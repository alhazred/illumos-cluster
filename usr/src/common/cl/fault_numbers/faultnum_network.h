/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FAULTNUM_NETWORK_H
#define	_FAULTNUM_NETWORK_H

#pragma ident	"@(#)faultnum_network.h	1.15	08/05/20 SMI"

#ifdef  __cplusplus
extern "C" {
#endif

/*
 * Fault point macro for the NETWORK fault point group.
 */

#ifdef FAULT_NET
#define	FAULTPT_NET(n, f)		FAULT_POINT((n), (f))
#else
#define	FAULTPT_NET(n, f)
#endif

/*
 * Fault numbers for the NETWORK fault point group.
 * Example:
 *
 *	#define	FAULTNUM_NET_XXX	(FAULTBASE_NET | 1)
 *
 * The naming convention is: FAULTNUM_NET_name
 * Where 'name' is the name of the operation.
 */

#define	FAULTNUM_NET_ATACH_MCOB_B	(FAULTBASE_NET | 0x0001)
#define	FAULTNUM_NET_ATACH_MCOB_A	(FAULTBASE_NET | 0x0002)
#define	FAULTNUM_NET_ATACH_MCOB_E	(FAULTBASE_NET | 0x0003)

#define	FAULTNUM_NET_REG_GIFN_B		(FAULTBASE_NET | 0x0004)
#define	FAULTNUM_NET_REG_GIFN_A		(FAULTBASE_NET | 0x0005)
#define	FAULTNUM_NET_REG_GIFN_E		(FAULTBASE_NET | 0x0006)

#define	FAULTNUM_NET_DEREG_GIFN_B	(FAULTBASE_NET | 0x0007)
#define	FAULTNUM_NET_DEREG_GIFN_A	(FAULTBASE_NET | 0x0008)
#define	FAULTNUM_NET_DEREG_GIFN_E	(FAULTBASE_NET | 0x0009)

#define	FAULTNUM_NET_REG_PXQO_B		(FAULTBASE_NET | 0x000a)
#define	FAULTNUM_NET_REG_PXQO_A		(FAULTBASE_NET | 0x000b)
#define	FAULTNUM_NET_REG_PXQO_E		(FAULTBASE_NET | 0x000c)

#define	FAULTNUM_NET_DEREG_PXYN_B	(FAULTBASE_NET | 0x000d)
#define	FAULTNUM_NET_DEREG_PXYN_A	(FAULTBASE_NET | 0x000e)
#define	FAULTNUM_NET_DEREG_PXYN_E	(FAULTBASE_NET | 0x000f)

#define	FAULTNUM_NET_ADD_GRP_B		(FAULTBASE_NET | 0x0010)
#define	FAULTNUM_NET_ADD_GRP_A		(FAULTBASE_NET | 0x0011)
#define	FAULTNUM_NET_ADD_GRP_E		(FAULTBASE_NET | 0x0012)

#define	FAULTNUM_NET_RM_GRP_B		(FAULTBASE_NET | 0x0013)
#define	FAULTNUM_NET_RM_GRP_A		(FAULTBASE_NET | 0x0014)
#define	FAULTNUM_NET_RM_GRP_E		(FAULTBASE_NET | 0x0015)

#define	FAULTNUM_NET_REG_INST_B		(FAULTBASE_NET | 0x0016)
#define	FAULTNUM_NET_REG_INST_A		(FAULTBASE_NET | 0x0017)
#define	FAULTNUM_NET_REG_INST_E		(FAULTBASE_NET | 0x0018)

#define	FAULTNUM_NET_DEREG_INST_B	(FAULTBASE_NET | 0x0019)
#define	FAULTNUM_NET_DEREG_INST_A	(FAULTBASE_NET | 0x001a)
#define	FAULTNUM_NET_DEREG_INST_E	(FAULTBASE_NET | 0x001b)

#define	FAULTNUM_NET_SET_PGIFN_B	(FAULTBASE_NET | 0x001c)
#define	FAULTNUM_NET_SET_PGIFN_A	(FAULTBASE_NET | 0x001d)
#define	FAULTNUM_NET_SET_PGIFN_E	(FAULTBASE_NET | 0x001e)

#define	FAULTNUM_NET_ATACH_GIFN_B	(FAULTBASE_NET | 0x001f)
#define	FAULTNUM_NET_ATACH_GIFN_A	(FAULTBASE_NET | 0x0020)
#define	FAULTNUM_NET_ATACH_GIFN_E	(FAULTBASE_NET | 0x0021)

#define	FAULTNUM_NET_ADD_SS_B		(FAULTBASE_NET | 0x0022)
#define	FAULTNUM_NET_ADD_SS_A		(FAULTBASE_NET | 0x0023)
#define	FAULTNUM_NET_ADD_SS_E		(FAULTBASE_NET | 0x0024)

#define	FAULTNUM_NET_RM_SS_B		(FAULTBASE_NET | 0x0025)
#define	FAULTNUM_NET_RM_SS_A		(FAULTBASE_NET | 0x0026)
#define	FAULTNUM_NET_RM_SS_E		(FAULTBASE_NET | 0x0027)

#define	FAULTNUM_NET_ADD_NODE_B		(FAULTBASE_NET | 0x0028)
#define	FAULTNUM_NET_ADD_NODE_A		(FAULTBASE_NET | 0x0029)

#define	FAULTNUM_NET_RM_NODE_B		(FAULTBASE_NET | 0x002a)
#define	FAULTNUM_NET_RM_NODE_A		(FAULTBASE_NET | 0x002b)

#define	FAULTNUM_NET_SET_DIST_B		(FAULTBASE_NET | 0x002c)
#define	FAULTNUM_NET_SET_DIST_A		(FAULTBASE_NET | 0x002d)
#define	FAULTNUM_NET_SET_DIST_E		(FAULTBASE_NET | 0x002e)

#define	FAULTNUM_NET_SET_WGHT_B		(FAULTBASE_NET | 0x002f)
#define	FAULTNUM_NET_SET_WGHT_A		(FAULTBASE_NET | 0x0030)
#define	FAULTNUM_NET_SET_WGHT_E		(FAULTBASE_NET | 0x0031)

#define	FAULTNUM_NET_REG_USRP_B		(FAULTBASE_NET | 0x0032)
#define	FAULTNUM_NET_REG_USRP_A		(FAULTBASE_NET | 0x0033)

#define	FAULTNUM_NET_SET_PARM_B		(FAULTBASE_NET | 0x0034)
#define	FAULTNUM_NET_SET_PARM_A		(FAULTBASE_NET | 0x0035)

#define	FAULTNUM_NET_CFG_ST_B		(FAULTBASE_NET | 0x0036)
#define	FAULTNUM_NET_CFG_ST_A		(FAULTBASE_NET | 0x0037)
#define	FAULTNUM_NET_CFG_ST_E		(FAULTBASE_NET | 0x0038)

#ifdef  __cplusplus
}
#endif

#endif	/* _FAULTNUM_NETWORK_H */
