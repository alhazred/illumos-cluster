/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FAULTNUM_VM_H
#define	_FAULTNUM_VM_H

#pragma ident	"@(#)faultnum_vm.h	1.7	08/05/20 SMI"

#ifdef  __cplusplus
extern "C" {
#endif

/*
 * Fault point macro for the VM fault point group.
 */

#ifdef FAULT_VM
#define	FAULTPT_VM(n, f)		FAULT_POINT((n), (f))
#else
#define	FAULTPT_VM(n, f)
#endif

/*
 * Fault numbers for the VM fault point group.
 * Example:
 *
 *	#define	FAULTNUM_LCA_REGISTER_CL_UCC	(FAULTBASE_VM | 1)
 *
 * Symbols for the CMM fault points encode their location in the following
 * manner:
 *
 *	FAULTNUM_CMM_file_function
 */

#define	FAULTNUM_VM_LCA_REGISTER_CL_UCC			(FAULTBASE_VM | 1)
#define	FAULTNUM_VM_CB_COORD_REGISTER_UCC_1		(FAULTBASE_VM | 2)
#define	FAULTNUM_VM_CB_COORD_REGISTER_UCC_2		(FAULTBASE_VM | 3)
#define	FAULTNUM_VM_LCA_BEFORE_DO_CB_TASK		(FAULTBASE_VM | 4)
#define	FAULTNUM_VM_LCA_CB_TASK_BEFORE_LAUNCH		(FAULTBASE_VM | 5)
#define	FAULTNUM_VM_LCA_CB_TASK_AFTER_LAUNCH		(FAULTBASE_VM | 6)
#define	FAULTNUM_VM_LCA_UNFINISHED_BTSTRP_CL_CB		(FAULTBASE_VM | 7)
#define	FAULTNUM_VM_LCA_UNFINISHED_CL_CB		(FAULTBASE_VM | 8)
#define	FAULTNUM_VM_LCA_CB_THREAD_DONE			(FAULTBASE_VM | 9)
#define	FAULTNUM_VM_LCA_NOTIFY_COORD_CB_TASK_DONE_1	(FAULTBASE_VM | 10)
#define	FAULTNUM_VM_LCA_NOTIFY_COORD_CB_TASK_DONE_2	(FAULTBASE_VM | 11)
#define	FAULTNUM_VM_LCA_NOTIFY_COORD_CB_TASK_DONE_3	(FAULTBASE_VM | 12)
#define	FAULTNUM_VM_LCA_NOTIFY_COORD_CB_TASK_DONE_4	(FAULTBASE_VM | 13)
#define	FAULTNUM_VM_CONSTRUCT_UCC_GROUP			(FAULTBASE_VM | 14)
#define	FAULTNUM_VM_CALCULATE_NEW_RVS_UVS		(FAULTBASE_VM | 15)
#define	FAULTNUM_VM_ALLOCATE_UPGD_COMMIT_TASK		(FAULTBASE_VM | 16)
#define	FAULTNUM_VM_UPGD_COMMIT_TASK_1_A		(FAULTBASE_VM | 17)
#define	FAULTNUM_VM_UPGD_COMMIT_TASK_1_B		(FAULTBASE_VM | 18)
#define	FAULTNUM_VM_UPGD_COMMIT_TASK_1_C		(FAULTBASE_VM | 19)
#define	FAULTNUM_VM_UPGD_COMMIT_TASK_2_A		(FAULTBASE_VM | 20)
#define	FAULTNUM_VM_UPGD_COMMIT_TASK_2_B		(FAULTBASE_VM | 21)
#define	FAULTNUM_VM_UPGD_COMMIT_TASK_2_C		(FAULTBASE_VM | 22)
#define	FAULTNUM_VM_UPGD_COMMIT_TASK_3_A		(FAULTBASE_VM | 23)
#define	FAULTNUM_VM_UPGD_COMMIT_TASK_3_B		(FAULTBASE_VM | 24)
#define	FAULTNUM_VM_UPGD_COMMIT_TASK_3_C		(FAULTBASE_VM | 25)
#define	FAULTNUM_VM_UPGD_COMMIT_TASK_3_D		(FAULTBASE_VM | 26)
#define	FAULTNUM_VM_UPGD_COMMIT_TASK_4_A		(FAULTBASE_VM | 27)
#define	FAULTNUM_VM_UPGD_COMMIT_TASK_4_B		(FAULTBASE_VM | 28)
#define	FAULTNUM_VM_UPGD_COMMIT_TASK_0_A		(FAULTBASE_VM | 29)
#define	FAULTNUM_VM_UPGD_COMMIT_TASK_0_B		(FAULTBASE_VM | 30)
#define	FAULTNUM_VM_CB_COORD_CB_TASK_DONE_1		(FAULTBASE_VM | 31)
#define	FAULTNUM_VM_CB_COORD_CB_TASK_DONE_2		(FAULTBASE_VM | 32)
#define	FAULTNUM_VM_CB_COORD_ITERATE_OVER_LCA		(FAULTBASE_VM | 33)
#define	FAULTNUM_VM_GET_NEXT_UCC_GROUP_1		(FAULTBASE_VM | 34)
#define	FAULTNUM_VM_THAW_SERVICES_1			(FAULTBASE_VM | 35)
#define	FAULTNUM_VM_THAW_SERVICES_2			(FAULTBASE_VM | 36)
#define	FAULTNUM_VM_FREEZE_SERVICES_1			(FAULTBASE_VM | 37)
#define	FAULTNUM_VM_FREEZE_SERVICES_2			(FAULTBASE_VM | 38)

#ifdef  __cplusplus
}
#endif

#endif	/* _FAULTNUM_VM_H */
