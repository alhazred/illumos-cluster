/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FAULT_NUMBERS_H
#define	_FAULT_NUMBERS_H

#pragma ident	"@(#)fault_numbers.h	1.37	08/06/03 SMI"

/*
 *  This file and the subsequent files included by it contain a list
 *  of fault numbers which can be used to set fault triggers and fault
 *  points.
 *
 *  Since the number of fault points, each containing a code that
 *  always checks if a trigger associated with its fault number
 *  exists, could potentially be large, it is desirable to have the
 *  ability to "turn on/off" selected fault points, because,
 *  otherwise, the performance of the system could be significantly
 *  affected.  Thus, we need the ability to divide fault points into
 *  groups and subgroups.
 *
 *  This file contains macros and switches, which when defined, allow
 *  us to "ifdef in/out" selected groups of fault points.  To turn on
 *  all fault points, define the FAULT_ALL macro, e.g. add
 *
 * 	-DFAULT_ALL
 *
 *  to the compiler flag.  To turn on only a certain group, add
 *  something like
 *
 * 	-DFAULT_GROUP
 *
 *  where "GROUP" is the name of the fault point group.  This group
 *  could, in turn, turn on other subgroups.  To turn off a particular
 *  subgroup of "GROUP" but leave other subgroups on, add
 *
 * 	-DFAULT_GROUP -DFAULT_SUBGROUP_OFF
 *
 *  where "SUBGROUP" is the name of the subgroup to be turned off.
 *  Similarly, if you want to turn on all fault points, _except_ one
 *  group, add
 *
 * 	-DFAULT_ALL -DFAULT_GROUP_OFF
 *
 *  The fault numbers belonging to each group themselves should be
 *  defined in separate files which are then included at the bottom of
 *  this file.
 *
 *  Below are instructions on how to add new groups/subgroups of fault
 *  numbers.  They are divided into several steps.
 *
 *  Finally, to use these fault numbers (and to use the Fault
 *  Injection framework) you don't need to include this file.  Simply
 *  #include <orb/fault/fault_injection.h>.
 *
 *  Good luck and have fun.
 *
 *
 */

#ifdef  __cplusplus
extern "C" {
#endif

#if !defined(_FAULT_INJECTION)
#undef FAULT_ALL
#endif

//
// STEP 1: define the base fault number for each fault point group/subgroup.
//
// Fault numbers are four bytes long.  The most significant two bytes are
// used as base fault numbers.  Base fault numbers help differentiate
// fault numbers belonging to one group of fault points from another.  The
// least significant two bytes can be used for the fault numbers themselves.
//
// To add a new group/subgroup of fault points, define the fault base number
// below, e.g.
//
//	#define	FAULTBASE_ORB		(1 << 16)
//	#define	FAULTBASE_HA_FRMWK	(2 << 16)
//	#define	FAULTBASE_PXFS		(3 << 16)
//
// Note, before you putback your changes to the gate, make sure you don't
// define a duplicate base number.
//
// Also, FAULTBASE_DEBUG below reserves base number 0.  It allows you to use
// fault injection for debugging your code.  This way, using the FAULT_POINT()
// macro, you can hardcode fault numbers (0-65535) without having to worry
// about duplicating others.  However, make sure that your final code
// doesn't use fault numbers within that range.
//

#define	FAULTBASE_DEBUG			(0 << 16)
#define	FAULTBASE_ORB			(1 << 16)
#define	FAULTBASE_HA_FRMWK		(2 << 16)
#define	FAULTBASE_TRANSPORT		(3 << 16)
#define	FAULTBASE_NET			(4 << 16)
#define	FAULTBASE_PXFS			(5 << 16)
#define	FAULTBASE_CMM			(6 << 16)
#define	FAULTBASE_ORPHAN		(7 << 16)
#define	FAULTBASE_CCR			(8 << 16)
#define	FAULTBASE_NS			(9 << 16)
#define	FAULTBASE_DCS			(10 << 16)
#define	FAULTBASE_RECONF		(11 << 16)
#define	FAULTBASE_UNREF			(12 << 16)
#define	FAULTBASE_RGM			(13 << 16)
#define	FAULTBASE_FLOW			(14 << 16)
#define	FAULTBASE_CLAPI			(15 << 16)
#define	FAULTBASE_DPM			(16 << 16)
#define	FAULTBASE_VM			(17 << 16)
#define	FAULTBASE_QDM			(18 << 16)

//
// STEP 2: force all fault points off when fault injection framework is not
//	   compiled in.
//
// For each group/subgroup you're adding, #undef its switch inside the
// #ifndef _FAULT_INJECTION block below.  E.g. to add group "FOO" and its
// subgroup "BAR", add the following:
//
//
//	#ifndef _FAULT_INJECTION
//		#undef FAULT_FOO
//		#undef FAULT_BAR
//	#endif
//

/* BEGIN CSTYLED */
#ifndef _FAULT_INJECTION
	#undef FAULT_ORB
	#undef FAULT_HA_FRMWK
	#undef FAULT_TRANSPORT
	#undef FAULT_NET
	#undef FAULT_PXFS
	#undef FAULT_CMM
	#undef FAULT_ORPHAN
	#undef FAULT_CCR
	#undef FAULT_NS
	#undef FAULT_DCS
	#undef FAULT_RECONF
	#undef FAULT_UNREF
	#undef FAULT_FLOW
	#undef FAULT_RGM
	#undef FAULT_CLAPI
	#undef FAULT_DPM
	#undef FAULT_VM
	#undef FAULT_QDM
#endif
/* END CSTYLED */

//
// STEP 3: define the group/subgroup hierarchy.
//
// The FAULT_ALL switch is reserved and used to turn on _all_ fault points.
// Define the "top" group switches inside the #ifdef FAULT_ALL block below.
// E.g. to define a new group called "FOO", add
//
//	#ifdef FAULT_ALL
//		#ifdef FAULT_FOO_OFF
//		#undef FAULT_FOO
//		#elif !defined(FAULT_FOO)
//		#define	FAULT_FOO
//		#endif
//	#endif
//
// Similary, to create "BAR" which is a subgroup of group "FOO", add
//
//	#ifdef FAULT_FOO
//		#ifdef FAULT_BAR_OFF
//		#undef FAULT_BAR
//		#elif !defined(FAULT_BAR)
//		#define	FAULT_BAR
//		#endif
//	#endif // FAULT_FOO
//

/* BEGIN CSTYLED */
#ifdef FAULT_ALL
	// ORB
	#ifdef FAULT_ORB_OFF
	#undef FAULT_ORB
	#elif !defined(FAULT_ORB)
	#define	FAULT_ORB
	#endif

	#ifdef FAULT_ORB
		// ORB::ORPHAN
		#ifdef FAULT_ORPHAN_OFF
		#undef FAULT_ORPHAN
		#elif !defined(FAULT_ORPHAN)
		#define	FAULT_ORPHAN
		#endif

		// ORB::RECONF
		#ifdef FAULT_RECONF_OFF
		#undef FAULT_RECONF
		#elif !defined(FAULT_RECONF)
		#define	FAULT_RECONF
		#endif

		// ORB::UNREF
		#ifdef FAULT_UNREF_OFF
		#undef FAULT_UNREF
		#elif !defined(FAULT_UNREF)
		#define	FAULT_UNREF
		#endif

		// ORB::FLOW
		#ifdef FAULT_FLOW_OFF
		#undef FAULT_FLOW
		#elif !defined(FAULT_FLOW)
		#define FAULT_FLOW
		#endif
	#endif

	// HA_FRMWK
	#ifdef FAULT_HA_FRMWK_OFF
	#undef FAULT_HA_FRMWK
	#elif !defined(FAULT_HA_FRMWK)
	#define	FAULT_HA_FRMWK
	#endif

	// TRANSPORT
	#ifdef FAULT_TRANSPORT_OFF
	#undef FAULT_TRANSPORT
	#elif !defined(FAULT_TRANSPORT)
	#define	FAULT_TRANSPORT
	#endif

	// NETWORK
	#ifdef FAULT_NET_OFF
	#undef FAULT_NET
	#elif !defined(FAULT_NET)
	#define	FAULT_NET
	#endif

	// PXFS
	#ifdef FAULT_PXFS_OFF
	#undef FAULT_PXFS
	#elif !defined(FAULT_PXFS)
	#define	FAULT_PXFS
	#endif

	// CMM
	#ifdef FAULT_CMM_OFF
	#undef FAULT_CMM
	#elif !defined(FAULT_CMM)
	#define FAULT_CMM
	#endif

	// CCR
	#ifdef FAULT_CCR_OFF
	#undef FAULT_CCR
	#elif !defined(FAULT_CCR)
	#define FAULT_CCR
	#endif

	// NAMESERVER
	#ifdef FAULT_NS_OFF
	#undef FAULT_NS
	#elif !defined(FAULT_NS)
	#define FAULT_NS
	#endif

	// DCS
	#ifdef FAULT_DCS_OFF
	#undef FAULT_DCS
	#elif !defined(FAULT_DCS)
	#define	FAULT_DCS
	#endif

	// RGM
	#ifdef FAULT_RGM_OFF
	#undef FAULT_RGM
	#elif !defined(FAULT_RGM)
	#define	FAULT_RGM
	#endif

	// CLAPI
	#ifdef FAULT_CLAPI_OFF
	#undef FAULT_CLAPI
	#elif !defined(FAULT_CLAPI)
	#define	FAULT_CLAPI
	#endif

	// DPM
	#ifdef FAULT_DPM_OFF
	#undef FAULT_DPM
	#elif !defined(FAULT_DPM)
	#define	FAULT_DPM
	#endif

	// VERSION MANAGER
	#ifdef FAULT_VM_OFF
	#undef FAULT_VM
	#elif !defined(FAULT_VM)
	#define FAULT_VM
	#endif

	//QDM
	#ifdef FAULT_QDM_OFF
	#undef FAULT_QDM
	#elif !defined(FAULT_QDM)
	#define FAULT_QDM
	#endif

#endif
/* END CSTYLED */

//
// STEP 4: create the group/subgroup "fault-number" include file and
//	   include it below.
//
// For example, for group "FOO" create an include file called "faultnum_foo.h"
// in the same directory as this file.  In that file, define the following
// items.  PLEASE DON'T DEFINE THEM IN THIS FILE.
//
//	1) A fault-point macro, for example:
//
//		#ifdef FAULT_FOO
//		#define	FAULTPT_FOO(n, f)	FAULT_POINT((n), (f))
//		#else
//		#define	FAULTPT_FOO(n, f)
//		#endif
//
//	   You must define FAULTPT_* macros to call FAULT_POINT().
//	   (For more information on FAULT_POINT(), see the file
//	   <orb/fault/fault_injection.h>.)
//
//	2) Define the fault numbers themselves using the group's fault base
//	   number, for example:
//
//		#define	FAULTNUM_FOO_MARSHAL	(FAULTBASE_FOO | 1)
//		#define	FAULTNUM_FOO_SEND	(FAULTBASE_FOO | 3)
//
// Include the file below, e.g.
//
//	#include <fault_numbers/faultnum_foo.h>
//

#include <fault_numbers/faultnum_orb.h>
#include <fault_numbers/faultnum_ha_frmwk.h>
#include <fault_numbers/faultnum_transport.h>
#include <fault_numbers/faultnum_network.h>
#include <fault_numbers/faultnum_pxfs.h>
#include <fault_numbers/faultnum_cmm.h>
#include <fault_numbers/faultnum_ns.h>
#include <fault_numbers/faultnum_orphan.h>
#include <fault_numbers/faultnum_ccr.h>
#include <fault_numbers/faultnum_dcs.h>
#include <fault_numbers/faultnum_reconf.h>
#include <fault_numbers/faultnum_unref.h>
#include <fault_numbers/faultnum_rgm.h>
#include <fault_numbers/faultnum_flowcontrol.h>
#include <fault_numbers/faultnum_clapi.h>
#include <fault_numbers/faultnum_dpm.h>
#include <fault_numbers/faultnum_vm.h>
#include <fault_numbers/faultnum_qdm.h>

//
// STEP 5: to define fault points in your code, do the following:
//
// 1) Include <orb/fault/fault_injection.h>.
//
//    Note, You don't need to include this file; it's automatically included
//    by <orb/fault/fault_injection.h>.
//
// 2) Use the "fault-point" macro, e.g.
//
//	FAULTPT_FOO(FAULTNUM_FOO_SEND, FaultFunctions::panic)
//
//    where FaultFunctions::panic() is a fault function that gets called when
//    there is a trigger for fault number FAULTNUM_FOO_SEND.
//
//    Note, the FaultFunctions class in <orb/fault_functions.h> provides
//    a set of common fault functions.  You don't need to include it in your
//    code; it's automatcally included by <orb/fault/fault_injection.h>
//
// 3) Or, you can define a local fault point by doing:
//
//	#if defined(FAULT_FOO)
//		void		*fault_argp;
//		uint32_t	fault_argsize;
//
//		if (fault_triggered(FAULTNUM_FOO_SEND, *fault_argp,
//					&fault_argsize) {
//			...	// do stuff when fault number
//			...	// FAULTNUM_FOO_SEND is triggered
//		}
//	#endif
//

#ifdef  __cplusplus
}
#endif

#endif	/* _FAULT_NUMBERS_H */
