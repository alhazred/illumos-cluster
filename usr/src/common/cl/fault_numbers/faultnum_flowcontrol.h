/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FAULTNUM_FLOWCONTROL_H
#define	_FAULTNUM_FLOWCONTROL_H

#pragma ident	"@(#)faultnum_flowcontrol.h	1.15	08/05/20 SMI"

#ifdef  __cplusplus
extern "C" {
#endif

/*
 * Fault point macro for the FLOWCONTROL fault point group.
 * Also refer to src/orb/fault_flowcontrol.h
 */

#ifdef	FAULT_FLOW
#define	FAULTPT_FLOWCONTROL(n, fp)		FAULT_POINT((n), (fp))
#else
#define	FAULTPT_FLOWCONTROL(n, fp)
#endif

#define	FAULTNUM_FLOWCONTROL_RESOURCE_ALLOCATOR_1	(FAULTBASE_FLOW | 0)
#define	FAULTNUM_FLOWCONTROL_NEED_MORE_RESOURCES_1	(FAULTBASE_FLOW | 1)
#define	FAULTNUM_FLOWCONTROL_GOT_THREADS		(FAULTBASE_FLOW | 2)
#define	FAULTNUM_FLOWCONTROL_SIGNAL_RELEASE		(FAULTBASE_FLOW | 3)
#define	FAULTNUM_FLOWCONTROL_PROCESS_RESOURCE_GRANT (FAULTBASE_FLOW | 4)
#define	FAULTNUM_FLOWCONTROL_PROCESS_RESOURCE_RECALL (FAULTBASE_FLOW | 5)
#define	FAULTNUM_FLOWCONTROL_CHECK_FOR_RESOURCE_CHANGES_1 (FAULTBASE_FLOW | 6)
#define	FAULTNUM_FLOWCONTROL_SIGNAL_STOP		(FAULTBASE_FLOW | 7)
#define	FAULTNUM_FLOWCONTROL_SIGNAL_ABORT		(FAULTBASE_FLOW | 8)
#define	FAULTNUM_FLOWCONTROL_SIGNAL_RESOURCE_ALLOCATOR_1 (FAULTBASE_FLOW | 9)
#define	FAULTNUM_FLOWCONTROL_SIGNAL_C_RESOURCE_CHANGES_1 (FAULTBASE_FLOW | 10)
#define	FAULTNUM_FLOWCONTROL_CHANGE_RESOURCE_POOL	(FAULTBASE_FLOW | 11)
#define	FAULTNUM_FLOWCONTROL_RESTORE_RESOURCE_POOL	(FAULTBASE_FLOW | 12)
#define	FAULTNUM_FLOWCONTROL_CHECK_RELEASE		(FAULTBASE_FLOW | 13)
#define	FAULTNUM_FLOWCONTROL_PROCESS_REQUEST_RESOURCES_1 (FAULTBASE_FLOW | 14)
#define	FAULTNUM_FLOWCONTROL_S_PRR_1			(FAULTBASE_FLOW | 15)
#define	FAULTNUM_FLOWCONTROL_DESTROY_THREADS		(FAULTBASE_FLOW | 16)
#define	FAULTNUM_FLOWCONTROL_SIGNAL_RELEASE_2		(FAULTBASE_FLOW | 17)
#define	FAULTNUM_FLOWCONTROL_PROCESS_MESSAGE		(FAULTBASE_FLOW | 18)

#ifdef  __cplusplus
}
#endif

#endif	/* _FAULTNUM_FLOWCONTROL_H */
