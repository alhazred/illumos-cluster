/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FAULTNUM_ORB_H
#define	_FAULTNUM_ORB_H

#pragma ident	"@(#)faultnum_orb.h	1.18	08/05/20 SMI"

#ifdef  __cplusplus
extern "C" {
#endif

/*
 * Fault point macro for the ORB fault point group.
 */

#ifdef FAULT_ORB
#define	FAULTPT_ORB(n, f)		FAULT_POINT((n), (f))
#define	FAULTPT_ENV_ORB(n, e, f)	FAULT_POINT_ENV((n), (e), (f))
#else
#define	FAULTPT_ORB(n, f)
#define	FAULTPT_ENV_ORB(n, e, f)
#endif

/*
 * Fault numbers for the ORB fault point group.
 */

#define	FAULTNUM_ORB_RXDOOR_EXCEPTION_REPLY_REMOTE	(FAULTBASE_ORB | 1)

#define	FAULTNUM_ORB_RETRY_UNSENT_INVO			(FAULTBASE_ORB | 2)
#define	FAULTNUM_ORB_RETRY_SENT_INVO			(FAULTBASE_ORB | 3)

#define	FAULTNUM_ORB_RETRY_UNSENT_REPLY			(FAULTBASE_ORB | 4)
#define	FAULTNUM_ORB_RETRY_SENT_REPLY			(FAULTBASE_ORB | 5)

#define	FAULTNUM_ORB_RETRY_UNSENT_REFJOB		(FAULTBASE_ORB | 6)
#define	FAULTNUM_ORB_RETRY_SENT_REFJOB			(FAULTBASE_ORB | 7)

#define	FAULTNUM_ORB_RETRY_UNSENT_CMM			(FAULTBASE_ORB | 8)
#define	FAULTNUM_ORB_RETRY_SENT_CMM			(FAULTBASE_ORB | 9)

#define	FAULTNUM_ORB_RETRY_UNSENT_SIGNAL		(FAULTBASE_ORB | 10)
#define	FAULTNUM_ORB_RETRY_SENT_SIGNAL			(FAULTBASE_ORB | 11)

#define	FAULTNUM_ORB_RETRY_UNSENT_CKPT			(FAULTBASE_ORB | 12)
#define	FAULTNUM_ORB_RETRY_SENT_CKPT			(FAULTBASE_ORB | 13)

#define	FAULTNUM_ORB_RETRY_UNSENT_FLOW			(FAULTBASE_ORB | 14)
#define	FAULTNUM_ORB_RETRY_SENT_FLOW			(FAULTBASE_ORB | 15)

#define	FAULTNUM_ORB_RETRY_UNSENT_ONEWAY		(FAULTBASE_ORB | 16)
#define	FAULTNUM_ORB_RETRY_SENT_ONEWAY			(FAULTBASE_ORB | 17)

#define	FAULTNUM_ORB_RETRY_UNSENT_FAULT			(FAULTBASE_ORB | 18)
#define	FAULTNUM_ORB_RETRY_SENT_FAULT			(FAULTBASE_ORB | 19)

#define	FAULTNUM_ORB_RETRY_UNSENT_MAYBE			(FAULTBASE_ORB | 20)
#define	FAULTNUM_ORB_RETRY_SENT_MAYBE			(FAULTBASE_ORB | 21)

#define	FAULTNUM_ORB_RETRY_UNSENT_VM			(FAULTBASE_ORB | 22)
#define	FAULTNUM_ORB_RETRY_SENT_VM			(FAULTBASE_ORB | 23)

#define	FAULTNUM_ORB_GET_DESCRIPTOR			(FAULTBASE_ORB | 24)

#define	FAULTNUM_ORB_SMARTPTR_SLEEP			(FAULTBASE_ORB | 25)

#ifdef  __cplusplus
}
#endif

#endif /* _FAULTNUM_ORB_H */
