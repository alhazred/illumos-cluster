/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FAULTNUM_NS_H
#define	_FAULTNUM_NS_H

#pragma ident	"@(#)faultnum_ns.h	1.9	08/05/20 SMI"

#ifdef  __cplusplus
extern "C" {
#endif

/*
 * Fault point macro for the NS fault point group.
 */

#ifdef FAULT_NS
#define	FAULTPT_NS(n, f)		FAULT_POINT((n), (f))
#else
#define	FAULTPT_NS(n, f)
#endif

/*
 * Fault numbers for the NS fault point group.
 * Example:
 *
 *	#define	FAULTNUM_NS_XXXX (FAULTBASE_NS | 1)
 *
 * The naming convention is: FAULTNUM_NS_name
 * Where 'name' is the name of the operation.
 */

#define	FAULTNUM_LOOKUP					(FAULTBASE_NS | 0x0001)
#define	FAULTNUM_NS_BIND				(FAULTBASE_NS | 0x0002)
#define	FAULTNUM_NS_BIND_BEFORE_STORE_BINDING		(FAULTBASE_NS | 0x0003)
#define	FAULTNUM_NS_BIND_AFTER_CKPT			(FAULTBASE_NS | 0x0004)

#define	FAULTNUM_NS_REBIND				(FAULTBASE_NS | 0x0005)
#define	FAULTNUM_NS_REBIND_BEFORE_STORE_BINDING		(FAULTBASE_NS | 0x0006)
#define	FAULTNUM_NS_REBIND_AFTER_CKPT			(FAULTBASE_NS | 0x0007)

#define	FAULTNUM_NS_BIND_CTX				(FAULTBASE_NS | 0x0008)
#define	FAULTNUM_NS_BIND_CTX_BEFORE_STORE_BINDING	(FAULTBASE_NS | 0x0009)
#define	FAULTNUM_NS_BIND_CTX_AFTER_CKPT			(FAULTBASE_NS | 0x000a)

#define	FAULTNUM_NS_REBIND_CTX				(FAULTBASE_NS | 0x000b)
#define	FAULTNUM_NS_REBIND_CTX_BEFORE_STORE_BINDING	(FAULTBASE_NS | 0x000c)
#define	FAULTNUM_NS_REBIND_CTX_AFTER_CKPT		(FAULTBASE_NS | 0x000d)

#define	FAULTNUM_NS_UNBIND				(FAULTBASE_NS | 0x000e)
#define	FAULTNUM_NS_UNBIND_BEFORE_DELETE_BINDING	(FAULTBASE_NS | 0x000f)
#define	FAULTNUM_NS_UNBIND_AFTER_CKPT			(FAULTBASE_NS | 0x0010)

#define	FAULTNUM_NS_BIND_NEWCTX				(FAULTBASE_NS | 0x0012)
#define	FAULTNUM_NS_BIND_NEWCTX_BEFORE_BIND_NEWCTX	(FAULTBASE_NS | 0x0013)
#define	FAULTNUM_NS_BIND_NEWCTX_AFTER_CKPT		(FAULTBASE_NS | 0x0014)

#define	FAULTNUM_NS_RESOLVE				(FAULTBASE_NS | 0x0015)

#define	FAULTNUM_NS_LIST				(FAULTBASE_NS | 0x0016)

#ifdef  __cplusplus
}
#endif

#endif	/* _FAULTNUM_NS_H */
