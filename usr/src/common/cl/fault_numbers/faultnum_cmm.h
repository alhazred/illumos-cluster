/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FAULTNUM_CMM_H
#define	_FAULTNUM_CMM_H

#pragma ident	"@(#)faultnum_cmm.h	1.17	09/03/06 SMI"

#ifdef  __cplusplus
extern "C" {
#endif

/*
 * Fault point macro for the CMM fault point group.
 *
 * FAULTPT_KCMM are the fault points for kernel/UNODE land CMM
 * FAULTPT_UCMM are the fault points for user land CMM
 */

#ifdef FAULT_CMM
#ifdef _KERNEL_ORB
#define	FAULTPT_KCMM(n, f)		FAULT_POINT((n), (f))
#define	FAULTPT_UCMM(n, f)
#else  // _KERNEL_ORB
#define	FAULTPT_UCMM(n, f)		FAULT_POINT((n), (f))
#define	FAULTPT_KCMM(n, f)
#endif
#else  // FAULT_CMM
#define	FAULTPT_KCMM(n, f)
#define	FAULTPT_UCMM(n, f)
#endif

/*
 * Fault numbers for the CMM fault point group.
 * Example:
 *
 *	#define	FAULTNUM_CMM_SEND	(FAULTBASE_CMM | 1)
 *
 * Symbols for the CMM fault points encode their location in the following
 * manner:
 *
 *	FAULTNUM_CMM_file_function
 */

#define	FAULTNUM_CMM_DROP_RANDOM_INCOMING_MSGS		(FAULTBASE_CMM | 1)
#define	FAULTNUM_CMM_DROP_RANDOM_OUTGOING_MSGS		(FAULTBASE_CMM | 2)

#define	FAULTNUM_CMM_AUTOMATON_RETURN_STATE_1		(FAULTBASE_CMM | 3)
#define	FAULTNUM_CMM_AUTOMATON_RETURN_STATE_2		(FAULTBASE_CMM | 4)
#define	FAULTNUM_CMM_AUTOMATON_BEGIN_STATE_1		(FAULTBASE_CMM | 5)
#define	FAULTNUM_CMM_AUTOMATON_BEGIN_STATE_2		(FAULTBASE_CMM | 6)
#define	FAULTNUM_CMM_AUTOMATON_STEP_STATE_1		(FAULTBASE_CMM | 7)
#define	FAULTNUM_CMM_AUTOMATON_STEP_STATE_2		(FAULTBASE_CMM | 8)

#define	FAULTNUM_CMM_CALLBACK_REGISTRY_RETURN_CALL	(FAULTBASE_CMM | 9)
#define	FAULTNUM_CMM_CALLBACK_REGISTRY_STEP_CALL	(FAULTBASE_CMM | 10)
#define	FAULTNUM_CMM_CALLBACK_REGISTRY_EXEC_CALLBACKS_1	(FAULTBASE_CMM | 11)
#define	FAULTNUM_CMM_CALLBACK_REGISTRY_EXEC_CALLBACKS_2	(FAULTBASE_CMM | 12)

#define	FAULTNUM_CMM_AUTOMATON_END_STATE		(FAULTBASE_CMM | 13)
#define	FAULTNUM_CMM_CONTROL_RECONFIGURE		(FAULTBASE_CMM | 14)

#define	FAULTNUM_CMM_STOP_ABORT				(FAULTBASE_CMM | 15)

#define	FAULTNUM_CMM_QUORUM_DEVICE_FAIL			(FAULTBASE_CMM | 16)
#define	FAULTNUM_CMM_QUORUM_DEVICE_DELAY		(FAULTBASE_CMM | 17)

#define	FAULTNUM_CMM_START_TRANS			(FAULTBASE_CMM | 18)
#define	FAULTNUM_CMM_STEP_TRANS				(FAULTBASE_CMM | 19)

#define	FAULTNUM_CMM_NODE_DEPART_HEARSAY_1		(FAULTBASE_CMM | 20)
#define	FAULTNUM_CMM_NODE_DEPART_HEARSAY_2		(FAULTBASE_CMM | 21)
#define	FAULTNUM_CMM_NODE_DEPART_HEARSAY_3		(FAULTBASE_CMM | 22)

#define	FAULTNUM_CMM_NODE_JOIN_HEARSAY_1		(FAULTBASE_CMM | 23)
#define	FAULTNUM_CMM_NODE_JOIN_HEARSAY_2		(FAULTBASE_CMM | 24)
#define	FAULTNUM_CMM_NODE_JOIN_HEARSAY_3		(FAULTBASE_CMM | 25)

#ifdef  __cplusplus
}
#endif

#endif	/* _FAULTNUM_CMM_H */
