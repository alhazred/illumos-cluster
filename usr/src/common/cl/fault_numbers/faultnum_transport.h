/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FAULTNUM_TRANSPORT_H
#define	_FAULTNUM_TRANSPORT_H

#pragma ident	"@(#)faultnum_transport.h	1.22	08/05/20 SMI"

#ifdef  __cplusplus
extern "C" {
#endif

/*
 * Fault point macro for the TRANSPORT fault point group.
 */

#ifdef FAULT_TRANSPORT
#define	FAULTPT_TRANSPORT(n, f)		FAULT_POINT((n), (f))
#define	FAULTPT_ENV_TRANSPORT(n, e, f)	FAULT_POINT_ENV((n), (e), (f))
#else
#define	FAULTPT_TRANSPORT(n, f)
#define	FAULTPT_ENV_TRANSPORT(n, e, f)
#endif

/*
 * Fault numbers for the TRANSPORT fault point group.
 */

#define	FAULTNUM_TRANSPORT_PATHEND_FAIL		(FAULTBASE_TRANSPORT | 1)
#define	FAULTNUM_TRANSPORT_STOP_PM_SEND		(FAULTBASE_TRANSPORT | 2)

#define	FAULTNUM_PM_DROP_ALL_INCOMING_MSGS	(FAULTBASE_TRANSPORT | 3)
#define	FAULTNUM_PM_DROP_ALL_OUTGOING_MSGS	(FAULTBASE_TRANSPORT | 4)

#define	FAULTNUM_PM_PANIC_IN_PATH_DOWN		(FAULTBASE_TRANSPORT | 5)

#define	FAULTNUM_TICK_DELAY_PANIC		(FAULTBASE_TRANSPORT | 6)

#define	FAULTNUM_TRANSPORT_DELAY_SEND_THREAD	(FAULTBASE_TRANSPORT | 7)
#define	FAULTNUM_PM_DELAY_CLOCK_THREAD		(FAULTBASE_TRANSPORT | 8)
#define	FAULTNUM_PM_HB_WRAP			(FAULTBASE_TRANSPORT | 9)
#define	FAULTNUM_HB_INTERMITTENT_FAILURE	(FAULTBASE_TRANSPORT | 10)

#ifdef  __cplusplus
}
#endif

#endif	/* _FAULTNUM_TRANSPORT_H */
