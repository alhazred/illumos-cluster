/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FAULTNUM_ORPHAN_H
#define	_FAULTNUM_ORPHAN_H

#pragma ident	"@(#)faultnum_orphan.h	1.9	08/05/20 SMI"

#ifdef  __cplusplus
extern "C" {
#endif

/*
 * Fault point macro for the ORPHAN fault point subgroup.
 */

#ifdef FAULT_ORPHAN
#define	FAULTPT_ORPHAN(n, f)		FAULT_POINT((n), (f))
#else
#define	FAULTPT_ORPHAN(n, f)
#endif

/*
 * Fault numbers for the ORPHAN fault point subgroup.
 *
 * A request is orphaned prior to the local rxdoor lookup in
 * rxdoor::handle_request_common().
 */

#define	FAULTNUM_ORPHAN_REQUEST_BEFORE_RXDOOR_LOOKUP	(FAULTBASE_ORPHAN | 1)

/*
 * A request is orphaned prior to the orphan message check in
 * rxdoor::handle_request_common().
 */

#define	FAULTNUM_ORPHAN_REQUEST_BEFORE_CHECK		(FAULTBASE_ORPHAN | 2)

/*
 * A request is orphaned after the orphan message check in
 * rxdoor::handle_request_common().
 */

#define	FAULTNUM_ORPHAN_REQUEST_AFTER_CHECK		(FAULTBASE_ORPHAN | 3)

/*
 * Reply messages are normally processed by an interrupt thread.  To allow
 * orphaned reply testing we must be able to suspend that thread while
 * waiting for the CMM to mark the server node dead.
 * This fault point (in orb_msg::deliver_message()) allows us to temporarily
 * force processing of reply messages to be deferred, i.e. hand them off to
 * non-interrupt threads.  Note, however, since this fault point is placed
 * before Fault Injection triggers are unmarshaled, it can be set only using
 * NodeTriggers.
 */

#define	FAULTNUM_ORPHAN_DEFER_REPLIES			(FAULTBASE_ORPHAN | 4)

/*
 * This fault point is used to turn off the FAULTNUM_ORPHAN_DEFER_REPLIES
 * NodeTriggers above.
 */

#define	FAULTNUM_ORPHAN_DEFER_REPLIES_OFF		(FAULTBASE_ORPHAN | 5)

/*
 * A reply is orphaned prior to the orphan message check in
 * outbound_invo_table::wakeup().
 */

#define	FAULTNUM_ORPHAN_REPLY_BEFORE_CHECK		(FAULTBASE_ORPHAN | 6)

/*
 * A reply is orphaned after the orphan message check in
 * outbound_invo_table::wakeup().
 */

#define	FAULTNUM_ORPHAN_REPLY_AFTER_CHECK		(FAULTBASE_ORPHAN | 7)

/*
 * A request/reply is orphaned in rxdoor_kit::unmarshal() before the
 * rxdoor lookup.
 */

#define	FAULTNUM_ORPHAN_UNMARSHAL_BEFORE_RXDOOR_LOOKUP	(FAULTBASE_ORPHAN | 8)

/*
 * A request/reply is orphaned in rxdoor_kit::unmarshal() before the
 * orphaned message check.
 */

#define	FAULTNUM_ORPHAN_UNMARSHAL_BEFORE_CHECK		(FAULTBASE_ORPHAN | 9)

/*
 * A request/reply is orphaned in rxdoor_kit::unmarshal() after the
 * orphaned message check.
 */

#define	FAULTNUM_ORPHAN_UNMARSHAL_AFTER_CHECK		(FAULTBASE_ORPHAN | 10)

#ifdef  __cplusplus
}
#endif

#endif	/* _FAULTNUM_ORPHAN_H */
