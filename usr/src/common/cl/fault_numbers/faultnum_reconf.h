/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FAULTNUM_RECONF_H
#define	_FAULTNUM_RECONF_H

#pragma ident	"@(#)faultnum_reconf.h	1.8	08/05/20 SMI"

#ifdef  __cplusplus
extern "C" {
#endif

/*
 * Fault point macro for the RECONF fault point subgroup.
 */

#ifdef FAULT_RECONF
#define	FAULTPT_RECONF(n, f)		FAULT_POINT((n), (f))
#else
#define	FAULTPT_RECONF(n, f)
#endif

/*
 * Fault numbers for the RECONF fault point subgroup.
 */

/*
 * Delay reference counting messages
 * This is a NodeTriggers fault.
 */

#define	FAULTNUM_RECONF_DEFER_CONFIRM		(FAULTBASE_RECONF | 1)
#define	FAULTNUM_RECONF_DEFER_ACK		(FAULTBASE_RECONF | 2)
#define	FAULTNUM_RECONF_DELAY_RECONNECT		(FAULTBASE_RECONF | 3)

#ifdef  __cplusplus
}
#endif

#endif	/* _FAULTNUM_RECONF_H */
