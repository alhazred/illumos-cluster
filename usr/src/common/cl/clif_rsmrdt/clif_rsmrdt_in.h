/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  clif_rsmrdt_in.h
 *
 */

#ifndef _CLIF_RSMRDT_IN_H
#define	_CLIF_RSMRDT_IN_H

#pragma ident	"@(#)clif_rsmrdt_in.h	1.8	08/05/20 SMI"

#include <sys/types.h>
#include <cplplrt/cplplrt.h>

extern "C" void *rsmrdt_add_adapter(const char *, int, uint64_t, int);
extern "C" boolean_t rsmrdt_remove_adapter(const char *, int);
extern "C" void *rsmrdt_add_path(const char *, int, nodeid_t, uint64_t, int,
	int);
extern "C" void rsmrdt_remove_path(nodeid_t, int, nodeid_t, int);
extern "C" boolean_t rsmrdt_path_up(const char *, int, nodeid_t, uint64_t);
extern "C" boolean_t rsmrdt_path_down(nodeid_t, int, nodeid_t, int);
extern "C" boolean_t rsmrdt_node_alive(nodeid_t);
extern "C" boolean_t rsmrdt_node_died(nodeid_t);
extern "C" void rsmrdt_set_my_nodeid(nodeid_t);
extern "C" void rsmrdt_purge_adapterpathlist(void);
extern "C" void rsmrdt_disconnect_node(nodeid_t);
extern "C" int rsmrdt_can_unload(void);

inline void
clifrsmrdt_path_monitor::shutdown()
{
	clifrsmrdt_shutdown_done++;
	clifrsmrdt_modunload_ok++;
	(void) clifrsmrdt_path_monitor::terminate();
}

// Static
inline clifrsmrdt_path_monitor &
clifrsmrdt_path_monitor::the(void)
{
	ASSERT(the_clifrsmrdt_path_monitor != NULL);
	return (*the_clifrsmrdt_path_monitor);

}

inline bool
clifrsmrdt_path_monitor::change_adapter(clconf_adapter_t *)
{
	return (false);
}

#endif	/* _CLIF_RSMRDT_IN_H */
