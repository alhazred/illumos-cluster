/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

//
// This code provides the C++ interface for RSMRDT driver to obtain
// callbacks from the Path Manager.  Calls to add_adapter build
// a list of adapters supported by RSMPI.  Calls to add_path build a list
// of paths pertaining to RSMRDT.  No path state is maintained here;
// path state transitions and processing is done in C code.
//

#pragma ident	"@(#)clif_rsmrdt.cc	1.15	08/05/20 SMI"

int clifrsmrdt_modunload_ok = 0;
int clifrsmrdt_shutdown_done = 0;

#include <sys/modctl.h>
#include <orb/invo/common.h>
#include <orb/transport/path_manager.h>
#include "clif_rsmrdt.h"

clifrsmrdt_path_monitor
*clifrsmrdt_path_monitor::the_clifrsmrdt_path_monitor = NULL;

/* inter-module dependencies */
char    _depends_on[] =  "misc/cl_runtime misc/cl_load misc/cl_orb "
	"misc/cl_haci misc/cl_quorum misc/cl_comm /drv/rsmrdt";

static struct modlmisc modlmisc = {
	&mod_miscops, "CLUSTER-RSMRDT Interface module v1.0",
};

static struct modlinkage modlinkage = {
#ifdef _LP64
	MODREV_1, { (void *) &modlmisc, NULL, NULL, NULL, NULL, NULL, NULL }
#else
	MODREV_1, { (void *) &modlmisc, NULL, NULL, NULL }
#endif
};


int
_init(void)
{
	int error;
	nodeid_t local_node_id;

	if ((cluster_bootflags & CLUSTER_BOOTED) == 0)
		return (EINVAL);

	if ((error = mod_install(&modlinkage)) != 0) {
		return (error);
	}

	/* Set the local node id */
	local_node_id = clconf_get_nodeid();

#ifdef NOTNOW
	os::printf("_init : local node id = %x\n", local_node_id);
#endif

	rsmrdt_set_my_nodeid(local_node_id);

	_cplpl_init();		/* C++ initialization */
	clifrsmrdt_path_monitor::initialize();
	return (0);
}

int
_fini(void)
{
	if (clifrsmrdt_modunload_ok == 0)
		return (EBUSY);

	if (clifrsmrdt_shutdown_done == 0) {
		if (clifrsmrdt_path_monitor::terminate() != 0)
			return (EBUSY);
	}

	_cplpl_fini();
	return (mod_remove(&modlinkage));
}

int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}

//
// CONSTRUCTOR
//
clifrsmrdt_path_monitor::clifrsmrdt_path_monitor()
{
#ifdef NOTNOW
	os::printf("clifrsmrdt_path_monitor constructor: this = %x\n", this);
#endif
}


//
// DESTRUCTOR
//
// clifrsmrdt_path_monitor::~clifrsmrdt_path_monitor()
// {
// #ifdef NOTNOW
//	os::printf("clifrsmrdt_path_monitor destructor: \n");
// #endif
// }


//
// Register with the cluster Path Manager
//
void
clifrsmrdt_path_monitor::initialize()
{
#ifdef NOTNOW
	os::printf("clifrsmrdt_path_monitor initialize: \n");
#endif
	ASSERT(the_clifrsmrdt_path_monitor == NULL);
	the_clifrsmrdt_path_monitor = new clifrsmrdt_path_monitor();
	ASSERT(the_clifrsmrdt_path_monitor != NULL);

	path_manager::the().add_client(path_manager::PM_OTHER,
	    the_clifrsmrdt_path_monitor);
}


//
// Un-register with the Path Manager callback list.  Then purge the
// adapter and path lists.  Then destroy the interface object
//
int
clifrsmrdt_path_monitor::terminate()
{
#ifdef NOTNOW
	os::printf("clifrsmrdt_path_monitor terminate: \n");
#endif
	if (rsmrdt_can_unload() != 0) {
		return (-1);
	}

	path_manager::the().remove_client(path_manager::PM_OTHER,
		the_clifrsmrdt_path_monitor);

	rsmrdt_purge_adapterpathlist();

	delete the_clifrsmrdt_path_monitor;
	the_clifrsmrdt_path_monitor = NULL;
	return (0);
}



//
// The rsmrdt_add_adapter routine returns TRUE if RSMPI supports the
// specified adapter.
//
bool
clifrsmrdt_path_monitor::add_adapter(clconf_adapter_t *adapter_p)
{
	const char *name;
	int instance;
	uint64_t adapter_hwaddr;
	const char *id = NULL;
	int adapterid;

	//
	// Extract adapter parameters from the database
	//

#ifdef NOTNOW
	os::printf("ADD ADAPT adp = %x\n", adapter_p);
#endif

	name = clconf_adapter_get_device_name(adapter_p);
	instance = clconf_adapter_get_device_instance(adapter_p);
	id = clconf_obj_get_property((clconf_obj_t *)adapter_p, "adapter_id");
	if (id == NULL)
		return (false);
	else
		adapter_hwaddr = (uint64_t)(int64_t)os::atoi(id);
	adapterid = clconf_obj_get_id((clconf_obj_t *)adapter_p);

	// An RSMPI supported adapter will return true
	if (rsmrdt_add_adapter(name, instance, adapter_hwaddr, adapterid)
		== NULL)
		return (false);
	else
		return (true);
}


//
// If the specified adapter was added to the list of supported adapters
// by add_adapter, then remove it from the list and call
// rsmrdt_remove_adapter for additional processing.
//
bool
clifrsmrdt_path_monitor::remove_adapter(clconf_adapter_t *adapter_p)
{
	const char *name;
	int instance;

	ASSERT(adapter_p != NULL);
#ifdef NOTNOW
	os::printf("clifrsmrdt_path_monitor remove_adapter %x\n", adapter_p);
#endif

	name = clconf_adapter_get_device_name(adapter_p);
	instance = clconf_adapter_get_device_instance(adapter_p);

	(void) rsmrdt_remove_adapter(name, instance);

	return (true);

}


bool
clifrsmrdt_path_monitor::add_path(clconf_path_t *path_p)
{
	nodeid_t		rem_nodeid;
	uint64_t		rem_adapter_hwaddr;
	int			rem_adapter_instance;
	clconf_cluster_t	*cl_cluster_p;
	clconf_adapter_t	*cl_adapter_p;
	const char 		*name;
	int 			instance;
	int			rem_adapterid;
	const char 		*id = NULL;

#ifdef NOTNOW
	os::printf("ADD PATH pathp = %x\n", path_p);
#endif

	// Get adapter, starting at the root of the cluster database
	cl_cluster_p = clconf_cluster_get_current();
	cl_adapter_p = clconf_path_get_adapter(path_p, CL_LOCAL, cl_cluster_p);
	name = clconf_adapter_get_device_name(cl_adapter_p);
	instance = clconf_adapter_get_device_instance(cl_adapter_p);

	//
	// Retrieve the remote node and remote adapter hardware address
	// from the cluster database
	rem_nodeid = clconf_path_get_remote_nodeid(path_p);
	cl_adapter_p = clconf_path_get_adapter(path_p, CL_REMOTE, cl_cluster_p);
	ASSERT(cl_adapter_p != NULL);

	id = clconf_obj_get_property((clconf_obj_t *)cl_adapter_p,
		"adapter_id");
	if (id == NULL) {
		clconf_obj_release((clconf_obj_t *)cl_cluster_p);
		return (false);
	} else {
		rem_adapter_hwaddr = (uint64_t)(int64_t)os::atoi(id);
	}

	rem_adapter_instance = clconf_adapter_get_device_instance(cl_adapter_p);

	clconf_obj_release((clconf_obj_t *)cl_cluster_p);

	rem_adapterid = clconf_path_get_adapterid(path_p, CL_REMOTE);

	//
	// RSMRDT driver path processing
	//
	if (rsmrdt_add_path(name, instance, rem_nodeid, rem_adapter_hwaddr,
			rem_adapter_instance, rem_adapterid) == NULL)
		return (false);
	else
		return (true);
}



//
//
bool
clifrsmrdt_path_monitor::remove_path(clconf_path_t *pth)
{
	nodeid_t	loc_nodeid, rem_nodeid;
	int 		loc_adapterid, rem_adapterid;

	ASSERT(pth != NULL);
#ifdef NOTNOW
	os::printf("Remove PATH pathp = %x\n", pth);
#endif
	loc_nodeid = clconf_get_local_nodeid();
	loc_adapterid = clconf_path_get_adapterid(pth, CL_LOCAL);
	rem_adapterid = clconf_path_get_adapterid(pth, CL_REMOTE);
	rem_nodeid = clconf_path_get_remote_nodeid(pth);

#ifdef NOTNOW
	os::printf("Remove PATH node loc(%d) rem(%d); adp loc(%d) rem (%d)\n",
		loc_nodeid, rem_nodeid, loc_adapterid, rem_adapterid);
#endif

	//
	// Call the RSMRDT driver remove path
	//
	rsmrdt_remove_path(loc_nodeid, loc_adapterid,
		rem_nodeid, rem_adapterid);

	return (true);
}

//
// This routine is called at clock ipl, so blocking is not allowed
//
bool
clifrsmrdt_path_monitor::path_up(clconf_path_t *pth)
{
	nodeid_t		rem_nodeid;
	uint64_t		rem_adapter_hwaddr;
	clconf_cluster_t	*cl_cluster_p;
	clconf_adapter_t	*cl_adapter_p;
	const char 		*name;
	int 			instance;
	const char		*id = NULL;

	ASSERT(pth != NULL);

#ifdef NOTNOW
	os::printf("PATH UP pathp = %x\n", pth);
#endif

	// Get adapter, starting at the root of the cluster database
	cl_cluster_p = clconf_cluster_get_current();
	cl_adapter_p = clconf_path_get_adapter(pth, CL_LOCAL, cl_cluster_p);
	name = clconf_adapter_get_device_name(cl_adapter_p);
	instance = clconf_adapter_get_device_instance(cl_adapter_p);

	//
	// Retrieve the remote node and remote adapter hardware address
	// from the cluster database
	rem_nodeid = clconf_path_get_remote_nodeid(pth);
	cl_adapter_p = clconf_path_get_adapter(pth, CL_REMOTE, cl_cluster_p);
	ASSERT(cl_adapter_p != NULL);

	id = clconf_obj_get_property((clconf_obj_t *)cl_adapter_p,
		"adapter_id");
	if (id == NULL) {
		clconf_obj_release((clconf_obj_t *)cl_cluster_p);
		return (false);
	} else {
		rem_adapter_hwaddr = (uint64_t)(int64_t)os::atoi(id);
	}

	clconf_obj_release((clconf_obj_t *)cl_cluster_p);

	if (rsmrdt_path_up(name, instance, rem_nodeid, rem_adapter_hwaddr)
		== 1)
		return (true);
	else
		return (false);
}


//
// Search the pathlist for pth; then call the driver pathdown routine
// with the adapter and path descriptor pointers (cookies).
// This routine is called at clock ipl, so blocking is not allowed
//
bool
clifrsmrdt_path_monitor::path_down(clconf_path_t *pth)
{
	nodeid_t	loc_nodeid, rem_nodeid;
	int 		loc_adapterid, rem_adapterid;

	ASSERT(pth != NULL);
#ifdef NOTNOW
	os::printf("PATH DOWN pathp = %x\n", pth);
#endif
	loc_nodeid = clconf_get_local_nodeid();
	loc_adapterid = clconf_path_get_adapterid(pth, CL_LOCAL);
	rem_adapterid = clconf_path_get_adapterid(pth, CL_REMOTE);
	rem_nodeid = clconf_path_get_remote_nodeid(pth);

#ifdef NOTNOW
	os::printf("Down PATH node loc(%d) rem(%d); adp loc(%d) rem (%d)\n",
		loc_nodeid, rem_nodeid, loc_adapterid, rem_adapterid);
#endif

	//
	// Call the RSMRDT driver path manager
	//
	(void) rsmrdt_path_down(loc_nodeid, loc_adapterid,
		rem_nodeid, rem_adapterid);

	return (true);
}

//
// disconnect_node is treated like a path_down for all paths pertaining
// to the node.  All elements of the pathlist are checked; when there is
// a node_id match, the rsmrdt_path_down routine is called in the driver
// path manager. disconnect_node is called at clock interrupt level so
// rsmrdt_path_down is called with the sleep flag set to NO_SLEEP.
bool
clifrsmrdt_path_monitor::disconnect_node(nodeid_t ndid, incarnation_num)
{

#ifdef NOTNOW
	os::printf("clifrsmrdt_path_monitor::disconnect_node = %x\n", ndid);
#endif
	rsmrdt_disconnect_node(ndid);
	return (true);
}

//
//  call the driver rsmrdt_node_alive
//
bool
clifrsmrdt_path_monitor::node_alive(nodeid_t node, incarnation_num)
{
	int ret = 0;

#ifdef NOTNOW
	os::printf("clifrsmrdt_path_monitor::node_alive = %x\n", node);
#endif
	ret = rsmrdt_node_alive(node);
	if (ret == 1)
		return (true);
	else
		return (false);
}


//
// All processing is done by disconnect_node
//
bool
clifrsmrdt_path_monitor::node_died(nodeid_t node, incarnation_num)
{
#ifdef NOTNOW
	os::printf("clifrsmrdt_path_monitor::node_died = %x\n", node);
#endif
	(void) rsmrdt_node_died(node);
	return (true);
}
