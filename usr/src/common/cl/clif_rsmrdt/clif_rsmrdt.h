/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _CLIF_RSMRDT_H
#define	_CLIF_RSMRDT_H

#pragma ident	"@(#)clif_rsmrdt.h	1.7	08/05/20 SMI"

#include <sys/list_def.h>
#include <orb/transport/pm_client.h>


// This is the class for the interface between the Path Manager (C++) and
// RSMRDT driver.
//
// It will register with the Path Manager for monitoring the node and path
// events and forward the events to C code in the Kernel Agent.
//

class clifrsmrdt_path_monitor : public pm_client {
public:
	bool add_adapter(clconf_adapter_t *);
	bool remove_adapter(clconf_adapter_t *);
	inline bool change_adapter(clconf_adapter_t *);
	bool add_path(clconf_path_t *);
	bool remove_path(clconf_path_t *);
	bool node_alive(nodeid_t, incarnation_num);
	bool node_died(nodeid_t, incarnation_num);
	bool path_up(clconf_path_t *);
	bool path_down(clconf_path_t *);
	bool disconnect_node(nodeid_t, incarnation_num);

	static void initialize();
	static int terminate();
	inline void shutdown();

	static clifrsmrdt_path_monitor &the();

	clifrsmrdt_path_monitor();
//	~clifrsmrdt_path_monitor();

private:

	static clifrsmrdt_path_monitor *the_clifrsmrdt_path_monitor;

	//
	// Disallow assignments and pass by value
	//
	clifrsmrdt_path_monitor(const clifrsmrdt_path_monitor &);
	clifrsmrdt_path_monitor &operator = (const clifrsmrdt_path_monitor &);

};


#include "clif_rsmrdt_in.h"

#endif	/* _CLIF_RSMRDT_H */
