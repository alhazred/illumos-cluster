/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CZNET_INTERNAL_H
#define	_CZNET_INTERNAL_H

#pragma ident	"@(#)cznet_internal.h	1.4	08/05/23 SMI"

#include <sys/list_def.h>
#include <sys/hashtable.h>
#include <sys/threadpool.h>
#include <nslib/ns.h>
#include <h/naming.h>
#include <h/ccr.h>
#include <ccr/data_server_impl.h>
#include <ccr/readonly_copy_impl.h>
#include <ccr/updatable_copy_impl.h>
#include <orb/infrastructure/orb.h>
#include <sys/clconf_int.h>
#include <sys/cladm_int.h>
#include <cmm/cmm_impl.h>

#ifndef _KERNEL
#include <scadmin/scconf.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif

#define	UINT_SIZE 32

// This is the class that's responsible for providing the private failover
// IP addresseses for the private failover IP address resources. The IP
// addresses are assigned from a pool of available IP addresses and a mapping
// is created between the IP address and the host name assigned to the
// resource.
//
class cznet_elem : public _SList::ListElem {
public:
	enum action_state { CZ_UNKNOWN = 0,
			CZ_ADD,
			CZ_EXISTS,
			CZ_REMOVE };

	cznet_elem(char *czname, char *subnet);
	~cznet_elem();

	action_state get_action();
	void set_action(action_state ac);
	int same(char *czname, char *subnet);
	char *get_czname();
	void set_czname(char *);
	char *get_subnet();
	void set_subnet(char *);
	uint32_t get_incn(nodeid_t);
	void set_incn(nodeid_t, uint32_t);

	cmm::membership_t czmbrshp;

private:
	char czname[CL_MAX_LEN];
	char subnet[CL_MAX_LEN];
	action_state action;
};

class cznet_map {

public:
	cznet_map();
	~cznet_map();

	SList<cznet_elem> & get_map_list();

	// Reads CCR and sets up list as above and sets up bitmask
	int cznet_map_populate(char *cz_name, char *subnet);
	int read_ccr_mappings();
	int is_changed();

	int initialize();
	void shutdown();

#ifndef _KERNEL
	int add_mapping_to_ccr(char *name, in_addr_t ipaddr,
		char *zonename, nodeid_t nodeid);
	int change_mapping_in_ccr(char *oldname, char *name,
		in_addr_t ipaddr, char *zonename, nodeid_t nodeid);
	int remove_mapping_from_ccr(char *name);

	int get_ip_address(char *hostname, char *zone, nodeid_t node);
	int change_privhostname(char *hostname, char *zone, nodeid_t node);
	char *get_privhostname(char *zone, nodeid_t node);
	int privhostname_exists(const char *hostname);
	int free_ip_address(char *zone, nodeid_t node);
#endif

private:
	SList<cznet_elem> *czelem_list;
	int current_version, cznet_map_changed;
};

/*
 * The CCR mapping table must be under /etc/cluster/ccr so that it
 * can be updated as any other CCR table on all the nodes of the cluster
 */
#define	CZNET_CCR_TABLE "cz_network_ccr"
#define	MAX_NAME_LEN 255

#endif	/* _CZNET_INTERNAL_H */
