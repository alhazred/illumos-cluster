//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cznet_common.cc	1.7	08/08/10 SMI"

#ifndef _KERNEL
#include <stropts.h>
#include <netdb.h>
#include <netinet/in.h>
#include <inet/tcp.h>
#include <sys/socket.h>
#endif

#include "cznet_internal.h"

cznet_elem::~cznet_elem()
{
}

cznet_elem::cznet_elem(char *z_name, char *snet) :
	_SList::ListElem(this)
{
	(void) os::strcpy(czname, z_name);
	(void) os::strcpy(subnet, snet);
	action = cznet_elem::CZ_UNKNOWN;
	for (nodeid_t i = 0; i <= NODEID_MAX; i++) {
		czmbrshp.members[i] = INCN_UNKNOWN;
	}
}


char *
cznet_elem::get_czname()
{
	char *name;
	name = new char[CL_MAX_LEN];
	(void) os::strcpy(name, czname);
	return (name);
}

void
cznet_elem::set_czname(char *zonename)
{
	if (czname != NULL)
		(void) os::strcpy(czname, zonename);
}

char *
cznet_elem::get_subnet()
{
	char *net;
	net = new char[CL_MAX_LEN];
	(void) os::strcpy(net, subnet);
	return (net);
}

void
cznet_elem::set_subnet(char *net)
{
	if (net != NULL)
		(void) os::strcpy(subnet, net);
}


cznet_elem::action_state
cznet_elem::get_action()
{
	return (action);
}

void
cznet_elem::set_action(cznet_elem::action_state ac)
{
	action = ac;
}

uint32_t cznet_elem::get_incn(nodeid_t nid) {
	return (czmbrshp.members[nid]);
}

void cznet_elem::set_incn(nodeid_t nid, uint32_t incn) {
	czmbrshp.members[nid] = incn;
}

int
cznet_elem::same(char *zn, char *sn)
{
	if (os::strcmp(czname, zn))
		return (0);
	if (os::strcmp(subnet, sn))
		return (0);
	return (1);
}

cznet_map::cznet_map()
{
	current_version = -1;
	cznet_map_changed = 1;
	czelem_list = NULL;
}

cznet_map::~cznet_map()
{
	czelem_list = NULL;
}

SList<cznet_elem>  &
cznet_map::get_map_list()
{
	ASSERT(czelem_list != NULL);
	return (*czelem_list);
}

int
cznet_map::initialize()
{
	czelem_list = new SList<cznet_elem>;
	if (czelem_list == NULL)
		return (ENOMEM);

	return (0);
}

void
cznet_map::shutdown()
{
	cznet_elem *p;
	if (czelem_list != NULL) {
		while ((p = czelem_list ->reapfirst()) != NULL) {
			delete p;
			p = NULL;
		}
		delete czelem_list;
		czelem_list = NULL;
	}
}


int
cznet_map::cznet_map_populate(char *czname, char *subnet)
{

	cznet_elem *cz_elem;

	// Obtain the element corresponding to the given IP address
	// from the free address list, set the hostname to the name
	// given as the parameter and then add the element to the
	// used list.
	SList<cznet_elem>::ListIterator
			iter(czelem_list);

	while ((cz_elem = iter.get_current()) != NULL) {
		if (cz_elem->same(czname, subnet)) {
			cz_elem->set_action(cznet_elem::CZ_EXISTS);
			return (0);
		}
		iter.advance();
	}
	cz_elem = new cznet_elem(czname, subnet);
	cz_elem->set_action(cznet_elem::CZ_ADD);
	czelem_list->append(cz_elem);

	return (0);
}

int
cznet_map::read_ccr_mappings()
{
	int new_version = -1, retry = 1;
	Environment e;
	ccr::readonly_table_var tabptr;
	ccr::element_seq_var elems;
	int num_elems;
	uint_t i;
	in_addr_t ipaddr;
	char *saddr, *snode;
	char *snet, *czname;
	nodeid_t node;
	char *dval, *lasts = NULL;
	CORBA::Exception *ex;
	cznet_elem *cz_elem = NULL;
	SList<cznet_elem>::ListIterator iter(czelem_list);
	ccr::directory_var ccr_ptr;

	cznet_map_changed = 1;

#ifndef _KERNEL
	// Check if ORB is initialized
	if (!(ORB::is_initialized())) {
		return (-1);
	}
#endif

	naming::naming_context_var ctxv = ns::local_nameserver();
	CORBA::Object_var obj = ctxv->resolve("ccr_directory", e);
	if (e.exception()) {
		e.clear();
		return (-1);
	}
	else
		ccr_ptr = ccr::directory::_narrow(obj);

	if (CORBA::is_nil(ccr_ptr))  {
		return (-1);
	}

	while (retry) {

		/* The table is created by the daemon if not already present */
		tabptr = ccr_ptr->lookup(CZNET_CCR_TABLE, e);
		if ((ex = e.exception()) != NULL) {
			return (-1);
		}

		new_version = tabptr->get_gennum(e);
		if ((ex = e.exception()) != NULL) {
			if (ccr::table_modified::_exnarrow(ex)) {
				// The table was modified since we
				// determined the number of elements
				// and version of the table.
				// Restart the process.
				e.clear();
				continue;
			} else {
				e.clear();
				return (-1);
			}
		}

		if ((new_version == current_version) && (new_version != -1)) {
			// We have already populated the used list with
			// the latest version of the table. Return success.
			cznet_map_changed = 0;
			return (0);
		}

		// Else, the table has been changed, so read it.
		cznet_map_changed = 1;

		// Initialize list elements
		iter.reinit(czelem_list);
		while ((cz_elem = iter.get_current()) != NULL) {
			cz_elem->set_action(
				cznet_elem::CZ_UNKNOWN);
			iter.advance();
		}

		// We need to read in the CCR table since this might be
		// the first time we are reading it or since the table
		// has changed since the last time we read it.
		num_elems = tabptr->get_num_elements(e);
		if ((ex = e.exception()) != NULL) {
			if (ccr::table_modified::_exnarrow(ex)) {
				// The table was modified since we
				// determined the number of elements
				// and version of the table.
				// Restart the process.
				e.clear();
				continue;
			} else {
				return (-1);
			}
		}

		if (num_elems > 0) {
			tabptr->atfirst(e);
			if ((ex = e.exception()) != NULL) {
				if (ccr::table_modified::_exnarrow(ex)) {
					// The table was modified since we
					// determined the number of elements
					// and version of the table.
					// Restart the process.
					e.clear();
					continue;
				} else {
					return (-1);
				}
			}

			tabptr->next_n_elements((uint32_t)num_elems, elems, e);
			if ((ex = e.exception()) != NULL) {
				if (ccr::table_modified:: _exnarrow(ex)) {
					// The table was modified since we
					// determined the number of elements
					// and version of the table.
					// Restart the process.
					e.clear();
					continue;
				} else {
					return (-1);
				}
			}

			// Populate the map.
			for (i = 0; i < elems->length(); i++) {
				czname = elems[i].key;
				snet = elems[i].data;
				(void) cznet_map_populate(czname, snet);
			}
		}
		retry = 0;
	}

	// Update the list so as to mark IP addresses which no longer
	// exist in the CCR for removal. Note any which should not
	// be marked for removal have already been marked as ADD or
	// EXISTS.
	iter.reinit(czelem_list);
	while ((cz_elem = iter.get_current()) != NULL) {
		if (cz_elem->get_action() == cznet_elem::CZ_UNKNOWN) {
			cz_elem->set_action(
				cznet_elem::CZ_REMOVE);
		}
		iter.advance();
	}

	current_version = new_version;
	return (0);
}

int
cznet_map::is_changed()
{
	return (cznet_map_changed);
}

#ifdef _KERNEL
clconf_errnum_t
get_cz_subnet(cz_subnet_t *cz_net_info)
{
	cznet_map the_cznet_map;
	cznet_elem *cz_elem = NULL;
	int err = 0;
	char *czname;

	if (the_cznet_map.initialize() != 0) {
		return (CL_MEMALLOC_FAIL);
	}

	// First read the CCR table
	if (the_cznet_map.read_ccr_mappings() != 0) {
		the_cznet_map.shutdown();
		return (CL_CCR_ERROR);
	}

	SList<cznet_elem>::ListIterator
	    iter(the_cznet_map.get_map_list());
	while ((cz_elem = iter.get_current()) != NULL) {

		czname = cz_elem->get_czname();

		if (strcmp(cz_net_info->name, czname) == 0) {
			os::strcpy(cz_net_info->sub_net, cz_elem->get_subnet());
			the_cznet_map.shutdown();
			return (CL_NOERROR);
		}
		iter.advance();
	}
	the_cznet_map.shutdown();
	return (CL_BADNAME);
}

#endif
