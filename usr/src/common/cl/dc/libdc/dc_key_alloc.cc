//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)dc_key_alloc.cc	1.11	08/05/20 SMI"

#include <sys/types.h>
#include <sys/sunddi.h>
#include <dc/sys/dc_key_alloc.h>
#include <dc/sys/dc_debug.h>

dc_key_alloc::dc_key_alloc()
{
	DCS_DBPRINTF_W(DCS_TRACE_LIBDC,
	    ("dc_key_alloc::dc_key_alloc().\n"));

	free_key = -1;
	max_key = 0;
	keys = (char **)NULL;
}

dc_key_alloc::~dc_key_alloc()
{
	DCS_DBPRINTF_R(DCS_TRACE_LIBDC,
	    ("dc_key_alloc::~dc_key_alloc(): destructor called!\n"));

	os::warning("Destructor called for dc_key_alloc ?? \n");
} /*lint !e1740 */

// Get a free number
int
dc_key_alloc::get_num(uint_t& suggested, const char *key)
{
	DCS_DBPRINTF_W(DCS_TRACE_LIBDC,
	    ("dc_key_alloc::get_num(%s).\n", key));

	int num = lookup_num(key);

	if (num == -1) {
		// the key has not been allocated before, do so now.
		return (alloc_num(suggested, key));
	}
	suggested = (uint_t)num;
	return (0);
}

int
dc_key_alloc::lookup_num(const char *key)
{
	DCS_DBPRINTF_W(DCS_TRACE_LIBDC,
	    ("dc_key_alloc::lookup_num(%s).\n", key));

	ASSERT(key && *key);

	for (int i = 0; i < max_key; i++) {
		if (keys[i] && strcmp(keys[i], key) == 0)
			return (i);
	}
	return (-1);
}

int
dc_key_alloc::alloc_num(uint_t& suggested, const char *key)
{
	DCS_DBPRINTF_W(DCS_TRACE_LIBDC,
	    ("dc_key_alloc::alloc_num(%s).\n", key));

	int i;

	ASSERT(key && *key);
	ASSERT(lookup_num(key) == -1);

	if ((suggested >= (uint_t)max_key) ||
	    (keys[suggested] == (char *)NULL)) {
		i = (int)suggested;
	} else {
		for (i = 0; i < max_key; i++) {
			if (keys[i] == 0)
				break;
		}
	}

	suggested = (uint_t)i;
	set_num(i, key);
	return (0);
}

// Set a number to be allocated.
void
dc_key_alloc::set_num(int num, const char *key)
{
	DCS_DBPRINTF_W(DCS_TRACE_LIBDC,
	    ("dc_key_alloc::set_num(%d, %s).\n", num, key));

	char **nkeysp;

	ASSERT(num >= 0 && key != (char *)NULL && *key != '\0');
	ASSERT(lookup_num(key) == -1 || num == lookup_num(key));

	if (num >= max_key) {
		int nmaxkeys = (num + num*1/4 + 1);

		if (nmaxkeys < 8)
			nmaxkeys = 8;
		/*lint -e737 */
		nkeysp = (char **)kmem_zalloc(nmaxkeys * sizeof (char *),
		    KM_SLEEP);
		if (keys) {
			bcopy(keys, nkeysp, max_key * sizeof (char *));
			kmem_free(keys, max_key * sizeof (char *));
		}
		/*lint +e737 */
		keys = nkeysp;
		max_key = nmaxkeys;
	}

	keys[num] = (char *)kmem_alloc(strlen(key)+1, KM_SLEEP);
	(void) strcpy(keys[num], key);
}

void
dc_key_alloc::reinitialize(int num_keys)
{
	DCS_DBPRINTF_W(DCS_TRACE_LIBDC,
	    ("dc_key_alloc::reinitialize(%d).\n", num_keys));

	remove_all();
	/*lint -e737 */
	keys = (char **)kmem_zalloc(num_keys * sizeof (char *),
	    KM_SLEEP);
	/*lint +e737 */
	max_key = num_keys;
}

void
dc_key_alloc::remove_all()
{
	DCS_DBPRINTF_W(DCS_TRACE_LIBDC,
	    ("dc_key_alloc::remove_all().\n"));

	if (keys) {
		for (int i = 0; i < max_key; i++) {
			if (keys[i] != (char *)NULL) {
				kmem_free(keys[i], strlen(keys[i])+1);
			}
		}
		kmem_free(keys, max_key * sizeof (char *)); //lint !e737
	}
	keys = (char **)NULL;
	max_key = 0;
	free_key = -1;
}

// Free a number

void
dc_key_alloc::free_num(int num)
{
	DCS_DBPRINTF_W(DCS_TRACE_LIBDC,
	    ("dc_key_alloc::free_num(%d).\n", num));

	ASSERT(0 <= num && num < max_key && keys[num] != (char *)NULL);

	kmem_free(keys[num], strlen(keys[num])+1);
	keys[num] = (char *)NULL;
}

int
dc_key_alloc::get_maxnum()
{
	return (max_key);
}

const char *
dc_key_alloc::lookup_key(int num)
{
	DCS_DBPRINTF_W(DCS_TRACE_LIBDC,
	    ("dc_key_alloc::lookup_key(%d).\n", num));

	ASSERT(num >= 0);

	if (num >= max_key)
		return (0);
	else
		return (keys[num]);
}
