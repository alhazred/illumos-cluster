//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)dc_contig_chunk.cc	1.18	08/05/20 SMI"

#include <sys/types.h>
#include <dc/sys/dc_contig_chunk.h>
#include <sys/os.h>
#include <sys/bitmap.h>
#include <dc/sys/dc_debug.h>


// Initialize the bit_list for dc_contig_chunk
dc_contig_chunk::dc_contig_chunk(int start)
{
	DCS_DBPRINTF_W(DCS_TRACE_LIBDC,
	    ("dc_contig_chunk::dc_contig_chunk(%d).\n", start));

	int i;

	free_bits =  (uint_t)DC_INT_MASK;
	for (i = 0; i < DC_CHUNK_SIZE; i++) {
		bit_list[i] = (uint_t)DC_INT_MASK;
	}
	start_value = start;
}

dc_contig_chunk::dc_contig_chunk(const repl_dc_data::dc_contig_chunk_info &info)
{
	DCS_DBPRINTF_W(DCS_TRACE_LIBDC,
	    ("dc_contig_chunk::dc_contig_chunk(%p).\n", &info));

	set_dc_chunk_info(info);
}

dc_contig_chunk::~dc_contig_chunk()
{
	DCS_DBPRINTF_W(DCS_TRACE_LIBDC,
	    ("dc_contig_chunk::~dc_contig_chunk().\n"));
}

// Check if the mcp has any free bits
int
dc_contig_chunk::free()
{
	DCS_DBPRINTF_W(DCS_TRACE_LIBDC,
	    ("dc_contig_chunk::free().\n"));

	if (free_bits & (uint_t)DC_INT_MASK)
		return (1);

	return (0);
}

// Get a number from this chunk.
uint_t
dc_contig_chunk::get_one()
{
	DCS_DBPRINTF_W(DCS_TRACE_LIBDC,
	    ("dc_contig_chunk::get_one().\n"));

	int freebit_index = 0;
	int ret_no;

	freebit_index = which_bit(free_bits);

	if (freebit_index) {
		freebit_index -= 1;
		ret_no = which_bit(bit_list[freebit_index]);
		ASSERT(ret_no);
		ret_no -= 1;
		bit_list[freebit_index] &= ~((uint_t)1 << ret_no);
		ret_no = chunk_bit_to_num((uint_t)ret_no, freebit_index);
		if (bit_list[freebit_index] == 0) {
			free_bits &= ~((uint_t)1 << freebit_index);
		}
		return ((uint_t)ret_no);
	}

	DCS_DBPRINTF_R(DCS_TRACE_LIBDC,
	    ("get_one(%p): freebit_index is 0.\n", this));

	return ((uint_t)DC_BAD_NUM);
}

// check if the given num is free
int
dc_contig_chunk::is_free(int num)
{
	DCS_DBPRINTF_W(DCS_TRACE_LIBDC,
	    ("dc_contig_chunk::is_free(%d).\n", num));

	int  freebit_index, chunkbit_index;

	ASSERT(num >= chunk_bit_to_num(0, 0));
	ASSERT(num <= chunk_bit_to_num(DC_CHUNK_MASK, DC_CHUNK_MASK));

	freebit_index = num_to_chunk_index(num);
	chunkbit_index = num_to_chunk_bit(num);

	if ((bit_list[freebit_index] & (1 << chunkbit_index)) != 0)
		return (1);

	return (0);
}

// Free the number in this chunk.
void
dc_contig_chunk::free_one(int num)
{
	DCS_DBPRINTF_W(DCS_TRACE_LIBDC,
	    ("dc_contig_chunk::free_one(%d).\n", num));
	int  freebit_index, chunkbit_index;

	ASSERT(num >= chunk_bit_to_num(0, 0));
	ASSERT(num <= chunk_bit_to_num(DC_CHUNK_MASK, DC_CHUNK_MASK));

	freebit_index = num_to_chunk_index(num);
	chunkbit_index = num_to_chunk_bit(num);

	free_bits |= (1 << freebit_index);

	ASSERT((bit_list[freebit_index] & (1 << chunkbit_index)) == 0);
	bit_list[freebit_index] |= (1 << chunkbit_index);
}

// Set a number to allocated in this chunk.
void
dc_contig_chunk::set_one(int num)
{
	DCS_DBPRINTF_W(DCS_TRACE_LIBDC,
	    ("dc_contig_chunk::set_one(%d).\n", num));

	int  freebit_index, chunkbit_index;

	ASSERT(num >= chunk_bit_to_num(0, 0));
	ASSERT(num <= chunk_bit_to_num(DC_CHUNK_MASK, DC_CHUNK_MASK));

	freebit_index = num_to_chunk_index(num);
	chunkbit_index = num_to_chunk_bit(num);
	bit_list[freebit_index] &= ~((uint_t)1 << chunkbit_index);
	if (bit_list[freebit_index] == 0) {
		free_bits &= ~((uint_t)1 << freebit_index);
	}
}

int
dc_contig_chunk::which_bit(uint_t a)
{
	DCS_DBPRINTF_W(DCS_TRACE_LIBDC,
	    ("dc_contig_chunk::which_bit(%x).\n", a));

	return (lowbit((ulong_t)a));
}

void
dc_contig_chunk::get_dc_chunk_info(repl_dc_data::dc_contig_chunk_info& info)
{
	DCS_DBPRINTF_W(DCS_TRACE_LIBDC,
	    ("dc_contig_chunk::get_dc_chunk_info(%p).\n", &info));

	info.bit_list.length(DC_CHUNK_SIZE);

	for (int i = 0; i < DC_CHUNK_SIZE; i++) {
		info.bit_list[(uint_t)i] = (uint_t)bit_list[(uint_t)i];
	}

	info.free_bits = free_bits;
	info.start_value = start_value;
}

void
dc_contig_chunk::set_dc_chunk_info(
    const repl_dc_data::dc_contig_chunk_info& info)
{
	DCS_DBPRINTF_W(DCS_TRACE_LIBDC,
	    ("dc_contig_chunk::set_dc_chunk_info(%p).\n", &info));

	ASSERT(info.bit_list.length() == DC_CHUNK_SIZE);

	for (int i = 0; i < DC_CHUNK_SIZE; i++) {
		bit_list[i] = (uint_t)info.bit_list[(uint_t)i];
	}

	free_bits = info.free_bits;
	start_value = info.start_value;
}
