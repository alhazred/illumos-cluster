//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)dc_lib.cc	1.18	08/05/20 SMI"

#include <dc/sys/dc_lib.h>
#include <nslib/ns.h>
#include <dc/sys/dc_debug.h>

const char *dclib::dcs_name = "DCS";

//
// Returns a "held" reference to the device configurator object for the
// cluster.  The caller is expected to release the reference.
//
mdc::dc_config_ptr
dclib::get_dcobj()
{
	DCS_DBPRINTF_W(DCS_TRACE_LIBDC,
	    ("dclib::get_dcobj().\n"));

	static mdc::dc_config_ptr dcobj = mdc::dc_config::_nil();

	if (CORBA::is_nil(dcobj)) {
		CORBA::Object_var obj;
		Environment	e;
		obj = ns::wait_resolve(dcs_name, e);
		if (e.exception()) {
			DCS_DBPRINTF_R(DCS_TRACE_LIBDC,
			    ("get_dcobj(): exception '%s' while calling "
			    "ns::wait_resolve(%s).\n",
			    e.exception()->_name(), dcs_name));
			e.clear();
			os::panic("DCS: Unable to resolve device "
			    "configurator from nameserver");
		}

		dcobj = mdc::dc_config::_narrow(obj);
		if (CORBA::is_nil(dcobj)) {
			DCS_DBPRINTF_R(DCS_TRACE_LIBDC,
			    ("get_dcobj(): CORBA::is_nil(dcobj).\n"));
			os::panic("DCS: Unable to resolve device "
			    "configurator from nameserver");
		}
	}

	return (dcobj);
}

//
// Returns a reference to the CCR directory object.  The caller is expected
// to release the reference.
//
ccr::directory_ptr
dclib::get_ccrobj()
{
	DCS_DBPRINTF_W(DCS_TRACE_LIBDC,
	    ("dclib::get_ccrobj().\n"));

	Environment env;

	// Lookup the CCR
	CORBA::Object_var obj = ns::wait_resolve_local("ccr_directory", env);
	if (env.exception()) {
		DCS_DBPRINTF_R(DCS_TRACE_LIBDC,
		    ("get_dcobj(): exception '%s' while calling "
		    "ns::wait_resolve_local(ccr_directory).\n",
		    env.exception()->_name()));
		env.clear();
		return (ccr::directory::_nil());
	}

	// Return the 'narrow'ed object, nil or not.
	return (ccr::directory::_narrow(obj));
}

//
// This function takes 'string' as input, and returns an array of records in
// 'fields', where a record is a substring of 'string' delimited by 'separator'.
// If there are more than 'maxfields' records in 'string', this function
// returns (-1), otherwise it returns the number of records returned in
// 'fields'.
// Note: This function will write all over 'string'.
//
int
dclib::parse_string(char *string, char separator, char **fields,
    uint_t maxfields)
{
	DCS_DBPRINTF_W(DCS_TRACE_LIBDC,
	    ("dclib::parse_string(%s, %c).\n", string, separator));

	ASSERT(maxfields > 0);
	ASSERT(string);
	ASSERT(fields);

	char *tmp;

	tmp = string;
	int token_no = 0;

	// Skip over spaces.
	while (*tmp && (*tmp == ' '))
		tmp++;

	if (*tmp == 0) {
		return (0);
	}

	fields[0] = tmp;
	token_no = 1;

	while (*tmp) {
		if (*tmp == separator) {
			// Mark the separator as end-of-string
			*tmp = 0;

			// Skip over spaces.
			do {
				tmp++;
			} while (*tmp && (*tmp == ' '));

			if (*tmp == 0) {
				// End-of-string, get out.
				break;
			}

			// Is there space in 'fields' for another token ?
			ASSERT(token_no <= maxfields); //lint !e574 !e737
			if (token_no == (int)maxfields) {
				//
				// Overflow in number of tokens, return an
				// error.
				//
				DCS_DBPRINTF_R(DCS_TRACE_LIBDC,
				    ("parse_string(): overflow: '%d'.\n",
				    maxfields));
				return (-1);
			}
			fields[token_no++] = tmp;
			continue;
		}
		tmp++;
	}

	return (token_no);
}

//
// Returns an updatable handle to the CCR table specified.
//
ccr::updatable_table_ptr
dclib::get_updatable_table(const char *table_name, bool creat)
{
	DCS_DBPRINTF_W(DCS_TRACE_LIBDC,
	    ("dclib::get_updatable_table(%s, %d).\n", table_name, creat));

	return ((ccr::updatable_table_ptr)get_ccr_table(table_name, creat,
	    true));
}

//
// Returns a handle to the CCR table specified.
//
ccr::readonly_table_ptr
dclib::get_readonly_table(const char *table_name, bool creat)
{
	DCS_DBPRINTF_W(DCS_TRACE_LIBDC,
	    ("dclib::get_readonly_table(%s, %d).\n", table_name, creat));

	return ((ccr::readonly_table_ptr)get_ccr_table(table_name, creat,
	    false));
}

//
// Worker function used by 'get_readonly_table' and 'get_updatable_table'.
//
void *
dclib::get_ccr_table(const char *table_name, bool creat,
    bool updatable)
{
	DCS_DBPRINTF_W(DCS_TRACE_LIBDC,
	    ("dclib::get_ccr_table(%s, %d, %d).\n",
	    table_name, creat, updatable));

	Environment env;
	CORBA::Exception *ex;

	// Lookup the CCR
	ccr::directory_var ccrp = dclib::get_ccrobj();
	if (CORBA::is_nil(ccrp)) {
		DCS_DBPRINTF_R(DCS_TRACE_LIBDC,
		    ("get_ccr_table(): CORBA::is_nil(ccrp).\n"));
		return (ccr::readonly_table::_nil());
	}

	void *table;

	// Attempt to get the table containing the list of services
	if (updatable) {
		table = ccrp->begin_transaction(table_name, env);
	} else {
		table = ccrp->lookup(table_name, env);
	}
	if ((ex = env.exception()) == NULL) {
		// No errors - return 'table' with a reference held.
		return (table);
	}

	ASSERT(ex != NULL);
	if (ccr::no_such_table::_exnarrow(ex) == NULL) {
		//
		// The exception is NOT the 'no_such_table' exception (which is
		// the only one we know how to handle).  Return an error to
		// the user.
		//
		if (updatable) {
			DCS_DBPRINTF_R(DCS_TRACE_LIBDC,
			    ("get_ccr_table(): exception '%s' while calling "
			    "%p->begin_transaction(%s).\n",
			    ex->_name(), ccrp, table_name));
		} else {
			DCS_DBPRINTF_R(DCS_TRACE_LIBDC,
			    ("get_ccr_table(): exception '%s' while calling "
			    "%p->lookup(%s).\n",
			    ex->_name(), ccrp, table_name));
		}
		RETURN_ON_EXCEPTION(env, "DCS: Error looking up services table",
		    ccr::readonly_table::_nil());
		ASSERT(0);
	}

	env.clear();

	//
	// Table does not exist - if we are not supposed to create it, just
	// return.
	//
	if (!creat) {
		return (ccr::readonly_table::_nil());
	}

	ccrp->create_table(table_name, env);
	if (env.exception()) {
		DCS_DBPRINTF_R(DCS_TRACE_LIBDC,
		    ("get_ccr_table(): exception '%s' while calling "
		    "%p->create_table(%s).\n",
		    env.exception()->_name(), ccrp, table_name));
	}
	RETURN_ON_EXCEPTION(env, "DCS: Unable to create CCR table",
	    ccr::readonly_table::_nil());

	//
	// No exception - lookup the table and return it.
	//
	if (updatable) {
		table = ccrp->begin_transaction(table_name, env);
		if (env.exception()) {
			DCS_DBPRINTF_R(DCS_TRACE_LIBDC,
			    ("get_ccr_table(): exception '%s' while calling "
			    "%p->begin_transaction(%s).\n",
			    ex->_name(), ccrp, table_name));
		}
	} else {
		table = ccrp->lookup(table_name, env);
		if (env.exception()) {
			DCS_DBPRINTF_R(DCS_TRACE_LIBDC,
			    ("get_ccr_table(): exception '%s' while calling "
			    "%p->lookup(%s).\n",
			    ex->_name(), ccrp, table_name));
		}
	}

	RETURN_ON_EXCEPTION(env, "DCS: Exception looking up newly created "
	    "services table", ccr::readonly_table::_nil());

	return (table);
}
