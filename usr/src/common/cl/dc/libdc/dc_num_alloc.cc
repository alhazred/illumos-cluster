//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)dc_num_alloc.cc	1.14	08/05/20 SMI"

#include <sys/types.h>
#include <dc/sys/dc_num_alloc.h>
#include <dc/sys/dc_debug.h>

dc_num_alloc::dc_num_alloc()
{
	DCS_DBPRINTF_W(DCS_TRACE_LIBDC, ("dc_num_alloc::dc_num_alloc().\n"));
}

dc_num_alloc::~dc_num_alloc()
{
	DCS_DBPRINTF_W(DCS_TRACE_LIBDC, ("dc_num_alloc::~dc_num_alloc().\n"));

	chunk_list.dispose();
}

// Get a free number
int
dc_num_alloc::get_num()
{
	DCS_DBPRINTF_W(DCS_TRACE_LIBDC, ("dc_num_alloc::get_num().\n"));

	dc_contig_chunk *mcp;

	mcp = find_free_mcp();

	return ((int)mcp->get_one());
}

// Returns 1 if num is free, 0 if not
int
dc_num_alloc::is_num_free(int num)
{
	DCS_DBPRINTF_W(DCS_TRACE_LIBDC,
	    ("dc_num_alloc::is_num_free(%d).\n", num));

	dc_contig_chunk *mcp;

	mcp  = find_mcp(num);
	return (mcp->is_free(num));
}

// Get the given number else return a new one
int
dc_num_alloc::get_given_num(int num)
{
	DCS_DBPRINTF_W(DCS_TRACE_LIBDC,
	    ("dc_num_alloc::get_given_num(%d).\n", num));

	dc_contig_chunk *mcp;

	mcp  = find_mcp(num);
	if (mcp->is_free(num)) {
		mcp->set_one(num);
		return (num);
	}
	mcp = find_free_mcp();
	return ((int)mcp->get_one());

}

// Set a number to be allocated.
void
dc_num_alloc::set_num(int num)
{
	DCS_DBPRINTF_W(DCS_TRACE_LIBDC,
	    ("dc_num_alloc::set_num(%d).\n", num));

	dc_contig_chunk *mcp;

	mcp = find_mcp(num);
	mcp->set_one(num);
}

// Free a number
void
dc_num_alloc::free_num(int num)
{
	DCS_DBPRINTF_W(DCS_TRACE_LIBDC,
	    ("dc_num_alloc::free_num(%d).\n", num));

	dc_contig_chunk *mcp;

	mcp = find_mcp(num);
	mcp->free_one(num);
}

// Initialize all allocations
void
dc_num_alloc::free_all()
{
	DCS_DBPRINTF_W(DCS_TRACE_LIBDC, ("dc_num_alloc::free_all().\n"));

	chunk_list.dispose();
}

// find an mcp that allocates num from start.
// The list is stored in a ascending order. we search for one
// and if we don't find one, allocate a new one and return.
dc_contig_chunk *
dc_num_alloc::find_mcp(int num)
{
	DCS_DBPRINTF_W(DCS_TRACE_LIBDC,
	    ("dc_num_alloc::find_mcp(%d).\n", num));

	dc_contig_chunk *mcp;
	int start;

	start = num_to_chunk_start(num);

	for (chunk_list.atfirst(); (mcp = chunk_list.get_current()) != NULL;
	    chunk_list.advance()) {
		if (mcp->get_start() > start)
			break;
		if (mcp->get_start() == start)
			return (mcp);
	}
	mcp = new dc_contig_chunk(start);
	chunk_list.insert_b(mcp);
	return (mcp);
}

// Finds a free mcp
dc_contig_chunk *
dc_num_alloc::find_free_mcp()
{
	DCS_DBPRINTF_W(DCS_TRACE_LIBDC,
	    ("dc_num_alloc::find_free_mcp().\n"));

	dc_contig_chunk *mcp, *prev_mcp;
	int start = 0;

	prev_mcp = NULL;

	// Loop to search free mcp or the first hole
	// that didn't have an mcp. We always allocate the lowest
	// possible free number.

	for (chunk_list.atfirst(); (mcp = chunk_list.get_current()) != NULL;
	    chunk_list.advance()) {
		if (prev_mcp &&
		    (mcp->get_start() != prev_mcp->get_start() +
			DC_CHUNK_NOS)) {
			start = prev_mcp->get_start() + DC_CHUNK_NOS;
			break;
		}
		prev_mcp = mcp;
		if (mcp->free())
			return (mcp);
		start = mcp->get_start() + DC_CHUNK_NOS;
	}

	mcp = new dc_contig_chunk(start);
	chunk_list.insert_b(mcp);
	return (mcp);
}
