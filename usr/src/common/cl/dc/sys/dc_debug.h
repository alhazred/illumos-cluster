/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_DC_DEBUG_H
#define	_DC_DEBUG_H

#pragma ident	"@(#)dc_debug.h	1.3	08/05/20 SMI"

#include <sys/dbg_printf.h>

#define	DCS_DBGBUF

#ifdef	DCS_DBGBUF
extern dbg_print_buf dcs_dbg;
#define	DCS_DBG(a)	dcs_dbg.dbprintf a
#define	DCS_DBG_CONT(a)	dcs_dbg.dbprintf_cont a
#else
#define	DCS_DBG(a)
#define	DCS_DBG_CONT(a)
#endif

#ifdef DEBUG
const int	DCS_WHITE   = 4;
const int	DCS_GREEN   = 3;
#endif
const int	DCS_AMBER   = 2;
const int	DCS_RED	    = 1;

//
// These options control which debug statements are enabled.
// This allows them to be controlled via the debugger.
// These constants are always defined in order to catch name conflicts.
//
const int	DCS_TRACE_DC_CONFIG			= 0x00000001;
const int	DCS_TRACE_DC_REPL_SERVER		= 0x00000002;
const int	DCS_TRACE_DC_MAJOR_STORE		= 0x00000004;
const int	DCS_TRACE_DC_MAPPER			= 0x00000008;
const int	DCS_TRACE_DC_SERVICE			= 0x00000010;
const int	DCS_TRACE_DC_SERVICES			= 0x00000020;
const int	DCS_TRACE_DC_STORAGE			= 0x00000040;
const int	DCS_TRACE_DC_TRANSACTION_STATES		= 0x00000080;
const int	DCS_TRACE_DC_DEVICE_REPLICA		= 0x00000100;
const int	DCS_TRACE_DEVICE_SERVICE_MGR		= 0x00000200;
const int	DCS_TRACE_DEVICE_SERVER			= 0x00000400;
const int	DCS_TRACE_LIBDC				= 0x00000800;

extern uint_t	dcs_trace_options;
extern int	dcs_trace_level;

//
// DCS_DBPRINTF_[W|G|A|R] - record dcs trace information.
// Inputs
//	option - enables trace if set in dcs_trace_options
//	args - The arguments to the print statements must be enclosed
//		with parenthesis, as in the following example:
//			("dcs action %d\n", action_number)
//
// Note that the trailing "else" statement in the macro removes
// ambiguity when the macro is nested in another "if" statement.
//
// Levels:
// - [W] White: enables calling stack printing (very verbose!);
// - [G] Green: enables notifications printing;
// - [A] Amber: enables warnings printing;
// - [R] Red:   enables fatal errors printing.
//
#ifdef DCS_DBGBUF

#ifdef DEBUG
#define	DCS_DBPRINTF_W(option, args)		\
	if ((option & dcs_trace_options) &&	\
	    (dcs_trace_level >= DCS_WHITE)) {	\
		DCS_DBG(args);			\
	} else

#define	DCS_DBPRINTF_G(option, args)		\
	if ((option & dcs_trace_options) &&	\
	    (dcs_trace_level >= DCS_GREEN)) {	\
		DCS_DBG(("GREEN: "));		\
		DCS_DBG_CONT(args);		\
	} else

#else	/* ! DEBUG */
#define	DCS_DBPRINTF_W(option, args)
#define	DCS_DBPRINTF_G(option, args)
#endif	/* DEBUG */

#define	DCS_DBPRINTF_A(option, args)		\
	if ((option & dcs_trace_options) &&	\
	    (dcs_trace_level >= DCS_AMBER)) {	\
		DCS_DBG(("AMBER: "));		\
		DCS_DBG_CONT(args);		\
	} else

#define	DCS_DBPRINTF_R(option, args)		\
	if (true) {				\
		DCS_DBG(("RED: "));		\
		DCS_DBG_CONT(args);		\
	} else

#else	/* ! DCS_DBGBUF */

#define	DCS_DBPRINTF_W(option, args)
#define	DCS_DBPRINTF_G(option, args)
#define	DCS_DBPRINTF_A(option, args)
#define	DCS_DBPRINTF_R(option, args)

#endif	/* DCS_DBGBUF */

#endif	/* _DC_DEBUG_H */
