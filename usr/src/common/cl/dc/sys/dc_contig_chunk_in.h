/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *  All Rights Reserved.
 *
 */

#ifndef _DC_CONTIG_CHUNK_IN_H
#define	_DC_CONTIG_CHUNK_IN_H

#pragma ident	"@(#)dc_contig_chunk_in.h	1.4	08/05/20 SMI"

inline int
dc_contig_chunk::get_start() {
	return (start_value);
}

inline int
dc_contig_chunk::chunk_bit_to_num(uint_t bitno, int index)
{
	return ((index << DC_CHUNK_SHIFT) + bitno + start_value);
}

inline int
dc_contig_chunk::num_to_chunk_index(int num)
{
	return ((num - start_value) >> DC_CHUNK_SHIFT);
}

inline int
dc_contig_chunk::num_to_chunk_bit(int num)
{
	return ((num - start_value) & DC_CHUNK_MASK);
}

#endif	/* _DC_CONTIG_CHUNK_IN_H */
