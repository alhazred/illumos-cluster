/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _DC_CONTIG_CHUNK_H
#define	_DC_CONTIG_CHUNK_H

#pragma ident	"@(#)dc_contig_chunk.h	1.13	08/05/20 SMI"

#include <sys/types.h>
#include <h/repl_dc_data.h>

#define	DC_BAD_NUM	-1

// Assumption is there are 32, 32-bit entries. The free_bits represents
// which one of those 32 entries have a free bit.
// Maximum it can allocate is MAXINT.

class dc_contig_chunk {
public:
	dc_contig_chunk(int);
	dc_contig_chunk(const repl_dc_data::dc_contig_chunk_info& info);
	~dc_contig_chunk();
	// returns whether any bit in the bit_list is free
	int free();
	// changes the state of one bit and returns the bit offset in
	uint_t get_one();
	int get_start();
	void free_one(int);
	void set_one(int);
	int is_free(int num);
	// finds which bit is first set in given integer
	static int which_bit(uint_t);

	void get_dc_chunk_info(repl_dc_data::dc_contig_chunk_info& info);

	void set_dc_chunk_info(const repl_dc_data::dc_contig_chunk_info& info);


private:

	enum {
		DC_CHUNK_SIZE = 32,
		DC_CHUNK_ENTSIZE = 32,
		DC_INT_MASK = 0xffffffff,
		DC_CHUNK_SHIFT = 5,
		DC_CHUNK_MASK = 0x1f
	};

	uint_t	free_bits;
	uint_t	bit_list[DC_CHUNK_SIZE];

	int chunk_bit_to_num(uint_t bitno, int index);
	int num_to_chunk_index(int num);
	int num_to_chunk_bit(int num);

	// Start value of the chunk.
	int	start_value;
};

#include <dc/sys/dc_contig_chunk_in.h>

#endif	/* _DC_CONTIG_CHUNK_H */
