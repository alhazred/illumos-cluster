/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _DC_LIB_H
#define	_DC_LIB_H

#pragma ident	"@(#)dc_lib.h	1.21	08/05/20 SMI"

#include <h/sol.h>
#include <sys/os.h>
#include <h/naming.h>
#include <h/replica.h>
#include <h/repl_pxfs.h>
#include <h/dc.h>
#include <h/ccr.h>

#define	DEV_SERVICE_NAME_PREFIX "device service"

#ifdef	DEBUG
#define	RETURN_ON_EXCEPTION(env, msg, ret_val) {	\
	if (env.exception()) {				\
		env.exception()->print_exception(msg);	\
		env.clear();				\
		return (ret_val);			\
	}						\
}
#else
#define	RETURN_ON_EXCEPTION(env, msg, ret_val) {	\
	if (env.exception()) {				\
		env.clear();				\
		os::warning(msg);			\
		return (ret_val);			\
	}						\
}
#endif	/* DEBUG */

class dclib {
public:
	//
	// Given the name of a service, this function returns the
	// name used to register this service with the replica
	// framework.
	// The caller needs to free the memory returned
	//
	static char *get_serviceid(const char *service_name);

	//
	// Returns the 'ha_mounter' object for this node.  This is used
	// to make upcalls to user space.
	//
	static repl_pxfs::ha_mounter_ptr get_mounter();

	//
	// Returns the device configurator object for the cluster.
	//
	static mdc::dc_config_ptr get_dcobj();

	//
	// Returns a reference to the CCR directory object.
	//
	static ccr::directory_ptr get_ccrobj();

	//
	// Returns a readonly handle to the CCR table specified.
	//
	static ccr::readonly_table_ptr get_readonly_table(
	    const char *table_name, bool create);

	//
	// Returns an updatable handle to the CCR table specified.
	//
	static ccr::updatable_table_ptr get_updatable_table(
	    const char *table_name, bool create);

	//
	// Implement strdup()
	//
	static char *strdup(const char *string);

	//
	// Parsing function to break up CCR strings into their
	// components.
	//
	static int parse_string(char *string, char separator,
	    char **fields, uint_t maxfields);

	//
	// There are various places in the dc code where we allocate
	// space to store a nodeid in string form - this enum defines
	// the length they all initialize to.
	//
	enum {
		NODEID_STRLEN = 5
	};

	static const char *dcs_name;

private:

	//
	// Worker function used by 'get_readable_table' and
	// 'get_updatable_table'.
	//
	static void *get_ccr_table(const char *table_name, bool create,
	    bool updatable);
};

#include <dc/sys/dc_lib_in.h>

#endif	/* _DC_LIB_H */
