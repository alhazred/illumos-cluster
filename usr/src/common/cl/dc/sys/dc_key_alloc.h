/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *  All Rights Reserved.
 *
 */

#ifndef _DC_KEY_ALLOC_H
#define	_DC_KEY_ALLOC_H

#pragma ident	"@(#)dc_key_alloc.h	1.8	08/05/20 SMI"

#include <sys/types.h>
#include <sys/os.h>
#include <sys/list_def.h>

//
// A generic class for allocating key in an integer space. This
// calls is useful for allocating instance numbers, major numbers
// and system call number. The idea is rather simple, you provide
// a key and the corresponding integer is looked or allocated, if
// one does not exist.
// Note: That there is not backing store provided for this class
// you must save the data and restore the contents of the class
// between boots. For dc_file_io class can be be used to do the
// file io needed for a backing store.
//

//
// get_num:
//	Return the integer corresponding to the given key.
//	A new number is allocated if a number is not currently
//	allocated.
//
// free_num:
//	Free the number associated with the particular key.
//
// set_num:
//	Set the number for a give key. This is used to quickly
//	populate the table from backing store.
//
// lookup_key:
//	Given a number return the key correponding to the number.
//	returns null is the number does not have a associated key.
//
// get_maxnum:
//	Return the maximum number of integers currently allocated.
//


class dc_key_alloc {
public:
	dc_key_alloc();
	virtual ~dc_key_alloc();
	int get_num(uint_t& suggested, const char *key);
	void free_num(int num);
	void set_num(int num, const char *key);
	const char *dc_key_alloc::lookup_key(int);
	int get_maxnum();
	void remove_all();
	void reinitialize(int num_keys);

protected:
	int free_key;
	int max_key;
	char **keys;
	int alloc_num(uint_t& suggested, const char *);
	int lookup_num(const char *);
};

#endif	/* _DC_KEY_ALLOC_H */
