/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _DC_NUM_ALLOC_H
#define	_DC_NUM_ALLOC_H

#pragma ident	"@(#)dc_num_alloc.h	1.15	08/05/20 SMI"

#include <sys/types.h>
#include <dc/sys/dc_contig_chunk.h>
#include <sys/os.h>
#include <sys/list_def.h>


// Implementation of the base class for allocating numbers.
// Allocators like minor allocators, instance allocators inherit
// from this.
// All inherited classes should implement its own locks and
// locks are not implemented in here.

class dc_num_alloc {
public:
	dc_num_alloc();
	virtual ~dc_num_alloc();
	// return 1 if num is free, 0 if not
	int is_num_free(int num);
	// allocate a free number
	int get_num();
	void free_num(int num);
	void free_all();
	void set_num(int num);
	// Try to get the given number
	int get_given_num(int num);

protected:
	// List of mapping chunk pointers. Each contiguous chunk
	// is a contiguous chunk of numbers starting from a particular
	// value.
	// CSTYLED
	SList<dc_contig_chunk> chunk_list;
	dc_contig_chunk *find_mcp(int start);
	dc_contig_chunk *find_free_mcp();
	// We define the constants inside enum.
	// Remember that enum's are "int" and express caution when
	// changing the values.
	// Each mapping chunk pointer has 1024 contiguous numbers
	// starting from a value given in its constructor.

	enum {
		DC_CHUNK_NOS = 1024,
		DC_CONTIG_MASK = 0x3ff,
		DC_CONTIG_SHIFT = 10
	};
	int num_to_chunk_start(int num)
	{
		return (num & ~DC_CONTIG_MASK);
	};

};



#endif	/* _DC_NUM_ALLOC_H */
