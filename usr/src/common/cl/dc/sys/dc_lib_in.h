/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  dc_lib_in.h
 *
 */

#ifndef _DC_LIB_IN_H
#define	_DC_LIB_IN_H

#pragma ident	"@(#)dc_lib_in.h	1.9	08/05/20 SMI"

inline char *
dclib::get_serviceid(const char *service_name)
{
	char *s_id = new char[strlen(DEV_SERVICE_NAME_PREFIX) +
	    strlen(service_name) + 2];

	os::sprintf(s_id, "%s %s", DEV_SERVICE_NAME_PREFIX, service_name);

	return (s_id);
}

inline char *
dclib::strdup(const char *string)
{
	return (os::strdup(string));
}

#endif	/* _DC_LIB_IN_H */
