/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _LIBDCS_H
#define	_LIBDCS_H

#pragma ident	"@(#)libdcs.h	1.29	08/05/20 SMI"

#include <sys/sysmacros.h>
#include <sys/clconf.h>
#include <sys/sc_syslog_msg.h>
#include <sys/sol_version.h>

#ifdef	__cplusplus
extern "C" {
#endif

/* tag for device fencing lock */
#define	COARSE_GRAIN_FENCING_LOCK "FENCE_LOCK"

/*
 * DCS_CCR_TABLE_PREFIX is prefix of the name of the CCR table where the dcs
 * configuration is stored.
 */
#define	DCS_CCR_TABLE_PREFIX "dcs_service_"

/*
 * DCS_SERVICE_NAME_MAX is the maximum allowed length for the name of a device
 * service name.
 */
#define	DCS_SERVICE_NAME_MAX MAXPATHLEN

/*
 * DCS_USER_PROGRAM_MAX is the maximum allowed length for the name of the
 * user program that handles state transitions of a device service.
 */
#define	DCS_USER_PROGRAM_MAX MAXPATHLEN

/*
 * DCS_GDEVS_MAX is the maximum number of dev_t ranges allowed to be placed in a
 * device service.
 */
#define	DCS_GDEVS_MAX 500

/*
 * DCS_DEFAULT_DESIRED_NUM_SECONDARIES is default value for desired number of
 * secondaries, if user does not supply one.
 */
#define	DCS_DEFAULT_DESIRED_NUM_SECONDARIES 1

/*
 * If dcs_startup_service() gets DCS_FREEZE_IN_PROGRESS exception from DCS,
 * it retries SWITCHOVER_TRIES-1 times before giving up.
 */
#define	SWITCHOVER_TRIES 30

/*
 * Duration of sleep between each retry from dcs_startup_service().
 */
#define	SLEEP_SEC 1

/*
 * This type defines the types of HA device services.
 */
typedef enum {
	TRANSPARENT_FAILOVER,	/* partial failure of the service is */
				/* transparent to its clients */
	EIO_FAILOVER		/* any failure of this service requires */
				/* clients to re-open active handles */
				/* to its components */
} dc_ha_type_t;

/*
 * This type defines the states that a HA device service replica can be in.
 */
typedef enum {
	PRIMARY,	/* Replica is the primary replica for this service */
	SECONDARY,	/* Replica is one of the secondary replicas for this */
			/* service. */
	SPARE,		/* Replica is a spare. */
	INACTIVE,	/* Replica is inactive i.e. either the node is down, */
			/* or the configuration is such that this replica */
			/* need not be made active */
	TRANSITION	/* Replica is in-between states */
} dc_replica_state_t;

/*
 * Define a replica by the node it runs on and it's priority.
 */
typedef struct {
	nodeid_t id;			/* The node this replica runs on */
	unsigned int preference;	/* A lower numbered 'preference' */
					/* indicates higher preference */
					/* Multiple nodes may have the same */
					/* value for 'preference'. */
} dc_replica_t;

/*
 * This structure holds a sequence of of 'dc_replica_t' structures.
 */
typedef struct {
	dc_replica_t *replicas;
	unsigned int count;
} dc_replica_seq_t;

/*
 * This structure holds the state of a replica identified by the node it runs
 */
typedef struct {
	nodeid_t id;
	dc_replica_state_t state;
} dc_replica_status_t;

/*
 * This structure holds a sequence of of 'dc_replica_status_t' instances.
 */
typedef struct {
	dc_replica_status_t *replicas;
	unsigned int count;
} dc_replica_status_seq_t;

/*
 * This structure holds a range of dev_t values.
 */
typedef struct {
	major_t maj;
	minor_t start_gmin;
	minor_t end_gmin;
} dc_gdev_range_t;

/*
 * This structure holds a sequence of device ranges.
 */
typedef struct {
	dc_gdev_range_t *ranges;
	unsigned int count;
} dc_gdev_range_seq_t;

/*
 * This structure holds a single property.
 */
typedef struct {
	char *name;
	char *value;
} dc_property_t;

/*
 * This structure holds a sequence of properties.
 */
typedef struct {
	dc_property_t *properties;
	unsigned int count;
} dc_property_seq_t;

/*
 * This structure holds a sequence of strings.
 */
typedef struct {
	char **strings;
	unsigned int count;
} dc_string_seq_t;

/*
 * Error codes
 */
typedef enum {
	DCS_SUCCESS = 0,		/* Success */
	DCS_ERR_NOT_CLUSTER,		/* Call is not being made from a node */
					/* booted as a cluster */
	DCS_ERR_SERVICE_CLASS_NAME,	/* Service class name in use, NULL */
	DCS_ERR_SERVICE_NAME,		/* Service name in use, NULL */
	DCS_ERR_USER_PROGRAM_NAME,	/* User program is NULL */
	DCS_ERR_NODE_ID,		/* Node id does not exist in system */
	DCS_ERR_DEVICE_INVAL,		/* dev_t value does not exist in the */
					/* system */
	DCS_ERR_DEVICE_BUSY,		/* another service is using this */
					/* dev_t value */
	DCS_ERR_SERVICE_INACTIVE,	/* service has not yet been started */
	DCS_ERR_SERVICE_BUSY,		/* service is busy */
	DCS_ERR_SERVICE_CLASS_BUSY,	/* Service class is in use */
	DCS_ERR_INVALID_STATE,		/* replica provider is in a invalid */
					/* state */
	DCS_ERR_REPLICA_FAILED,		/* replica provider failed to become */
					/* the primary */
	DCS_ERR_REPLICA_BUSY,		/* replica provider is busy */
	DCS_ERR_CCR_ACCESS,		/* Error accessing the CCR */
	DCS_ERR_NODE_BUSY,		/* Node is currently the primary */
	DCS_ERR_PROPERTY_NAME,		/* Invalid property name */
	DCS_ERR_COMM,			/* Communication error */
	DCS_ERR_NOMEM,			/* Unable to allocate memory */
	DCS_ERR_MAJOR_NUM,		/* Major number is invalid */
	DCS_ERR_NUMSEC,			/* Invalid desired num of secondaries */
	DCS_ERR_UNEXPECTED,		/* Unexpected error */
	DCS_FREEZE_IN_PROGRESS,		/* DCS primary cannot service request */
					/* because it is being frozen */
	DCS_ERR_VERSION			/* Running version does not support */
					/* method */
} dc_error_t;

/*
 * Convert a DCS error code to a string.
 */
const char *dcs_error_to_string(dc_error_t dc_err);

/*
 * This should be the first call made by a program that uses the libdcs
 * interfaces - if this call returns with a non-zero value, then we are not a
 * part of the cluster.
 */
int dcs_initialize(void);

/*
 * This call should be made before unloading the dcs library
 */
void dcs_shutdown(void);

/*
 * This call adds a service class to the DCS.
 */
dc_error_t dcs_create_service_class(
    const char *service_class,
    const char *user_program,
    dc_ha_type_t type);
/*
 * Possible return values:
 *  DCS_SUCCESS
 *  DCS_ERR_SERVICE_CLASS_NAME
 *  DCS_ERR_USER_PROGRAM_NAME
 *  DCS_ERR_CCR_ACCESS
 */

/*
 * This call sets the user program associated with a service class.
 */
dc_error_t dcs_set_service_class_user_program(
    const char *service_class,
    const char *user_program);
/*
 * Possible return values:
 *  DCS_SUCCESS
 *  DCS_ERR_SERVICE_CLASS_NAME
 *  DCS_ERR_USER_PROGRAM_NAME
 *  DCS_ERR_CCR_ACCESS
 */

/*
 * This call sets the HA type associated with a service class.
 */
dc_error_t dcs_set_service_class_ha_type(
    const char *service_class,
    dc_ha_type_t type);
/*
 * Possible return values:
 *  DCS_SUCCESS
 *  DCS_ERR_SERVICE_CLASS_NAME
 *  DCS_ERR_CCR_ACCESS
 */

/*
 * This call gets the parameters associated with a service class.
 * Memory is allocated for 'user_program' and should be freed using
 * 'dcs_free_string'.
 */
dc_error_t dcs_get_service_class_parameters(
    const char *service_class,
    char **user_program,
    dc_ha_type_t *type);
/*
 * Possible return values:
 *  DCS_SUCCESS
 *  DCS_ERR_SERVICE_CLASS_NAME
 *  DCS_ERR_CCR_ACCESS
 */

/*
 * This call remove a service class from the DCS configuration.
 */
dc_error_t dcs_remove_service_class(
    const char *service_class);
/*
 * Possible return values:
 *  DCS_SUCCESS
 *  DCS_ERR_SERVICE_CLASS_NAME
 *  DCS_ERR_SERVICE_CLASS_BUSY
 *  DCS_ERR_CCR_ACCESS
 */

/*
 * This call adds a ha device service to the device configurator persistent
 * storage.  If num_secondaries == 0 or num_secondaries > number of
 * nodes this service can run on, then num_secondaries is set to the maximum
 * possible.
 */
dc_error_t dcs_create_service(
    const char *service_name,
    const char *service_class,
    int enable_failback,
    const dc_replica_seq_t *nodes_seq,
    unsigned int num_secs,
    const dc_gdev_range_seq_t *gdev_range_seq,
    const dc_property_seq_t *property_seq);
/*
 * Possible return values:
 *  DCS_SUCCESS
 *  DCS_ERR_NOT_CLUSTER
 *  DCS_ERR_SERVICE_NAME
 *  DCS_ERR_SERVICE_CLASS_NAME
 *  DCS_ERR_NODE_ID
 *  DCS_ERR_DEVICE_INVAL
 *  DCS_ERR_DEVICE_BUSY
 *  DCS_ERR_PROPERTY_NAME
 *  DCS_ERR_CCR_ACCESS
 */

/*
 * This call will set the failback mode of this device service.  If
 * `enable_failback' is zero, this call will disable failback on the service,
 * otherwise it will enable failback.
 */
dc_error_t dcs_set_failback_mode(
    const char *service_name,
    int enable_failback);
/*
 * Possible return values:
 *  DCS_SUCCESS
 *  DCS_ERR_NOT_CLUSTER
 *  DCS_ERR_SERVICE_NAME
 *  DCS_ERR_CCR_ACCESS
 */

/*
 * Set the desired number of secondaries in a device service.
 */
dc_error_t dcs_set_active_secondaries(
    const char *service_name,
    unsigned int num_secs);
/*
 * Possible return values:
 *  DCS_SUCCESS
 *  DCS_ERR_NOT_CLUSTER
 *  DCS_ERR_SERVICE_NAME
 *  DCS_ERR_CCR_ACCESS
 */

/*
 * Add a replica to the service.
 */
dc_error_t dcs_add_node(
    const char *service_name,
    dc_replica_t *node);
/*
 * Possible return values:
 *  DCS_SUCCESS
 *  DCS_ERR_NOT_CLUSTER
 *  DCS_ERR_SERVICE_NAME
 *  DCS_ERR_NODE_ID
 *  DCS_ERR_CCR_ACCESS
 */

/*
 * Remove a replica from the service.
 */
dc_error_t dcs_remove_node(
    const char *service_name,
    nodeid_t id);
/*
 * Possible return values:
 *  DCS_SUCCESS
 *  DCS_ERR_NOT_CLUSTER
 *  DCS_ERR_SERVICE_NAME
 *  DCS_ERR_NODE_ID
 *  DCS_ERR_NODE_BUSY
 *  DCS_ERR_CCR_ACCESS
 */

/*
 * Add devices to this service.
 */
dc_error_t dcs_add_devices(
    const char *service_name,
    const dc_gdev_range_seq_t *gdev_range);
/*
 * Possible return values:
 *  DCS_SUCCESS
 *  DCS_ERR_NOT_CLUSTER
 *  DCS_ERR_SERVICE_NAME
 *  DCS_ERR_DEVICE_INVAL
 *  DCS_ERR_DEVICE_BUSY
 *  DCS_ERR_CCR_ACCESS
 */

/*
 * Removes a dev_t value from a service. The type of this device
 * will revert to what it was before it was added to this service.
 */
dc_error_t dcs_remove_devices(
    const char *service_name,
    const dc_gdev_range_seq_t *gdev_range);
/*
 * Possible return values:
 *  DCS_SUCCESS
 *  DCS_ERR_NOT_CLUSTER
 *  DCS_ERR_SERVICE_NAME
 *  DCS_ERR_DEVICE_INVAL
 *  DCS_ERR_DEVICE_BUSY
 *  DCS_ERR_CCR_ACCESS
 */

/*
 * Set the specified properties.  If this call returns with an error, then none
 * of the properties will have been set.  Existing properties will be
 * overwritten if they are of the same name as one of the properties being
 * specified here.
 */
dc_error_t dcs_set_properties(
    const char *service_name,
    const dc_property_seq_t *property_seq);
/*
 * Possible return values:
 *  DCS_ERR_SERVICE_CLASS_NAME
 *  DCS_ERR_PROPERTY_NAME
 *  DCS_ERR_CCR_ACCESS
 */

/*
 * Remove the specified properties from the device service.  If this call
 * returns with an error, then none of the properties specified will have been
 * removed.
 */
dc_error_t dcs_remove_properties(
    const char *service_name,
    const dc_string_seq_t *property_names);
/*
 * Possible return values:
 *  DCS_ERR_SERVICE_CLASS_NAME
 *  DCS_ERR_PROPERTY_NAME
 *  DCS_ERR_CCR_ACCESS
 */

/*
 * Get the value of the specified property.
 * Memory is allocated for 'property_value' and should be freed using
 * 'dcs_free_string'.
 */
dc_error_t dcs_get_property(
    const char *service_name,
    const char *property_name,
    char **property_value);
/*
 * Possible return values:
 *  DCS_ERR_SERVICE_CLASS_NAME
 *  DCS_ERR_PROPERTY_NAME
 *  DCS_ERR_CCR_ACCESS
 */

/*
 * Get the value of all the properties of the service.
 * Memory is allocated for 'properties' and should be freed using
 * 'dcs_free_properties'.
 */
dc_error_t dcs_get_all_properties(
    const char *service_name,
    dc_property_seq_t **property_seq);
/*
 * Possible return values:
 *  DCS_ERR_SERVICE_CLASS_NAME
 *  DCS_ERR_CCR_ACCESS
 */

/*
 * Remove a service definition.  This will only work if the service is not
 * currently active.
 */
dc_error_t dcs_remove_service(
    const char *service_name);
/*
 * Possible return values:
 *  DCS_SUCCESS
 *  DCS_ERR_NOT_CLUSTER
 *  DCS_ERR_SERVICE_NAME
 *  DCS_ERR_SERVICE_BUSY
 *  DCS_ERR_CCR_ACCESS
 */

/*
 * Switchover to a specific node.  If the nodeid parameter is 0, then we
 * switchover to the next available (in the order of preference specified
 * while creating the service) node. An issue with using nodeid=0 is
 * that calling dcs_switchover_service() doesn't guarantee that the service
 * won't end up back on the same node. To ensure a service is switched from
 * a specified node, then use dcs_switchover_service_from_node() instead.
 */
dc_error_t dcs_switchover_service(
    const char *service_name,
    nodeid_t id);
/*
 * Possible return values:
 *  DCS_SUCCESS
 *  DCS_ERR_NOT_CLUSTER
 *  DCS_ERR_SERVICE_INACTIVE
 *  DCS_ERR_NODE_ID
 *  DCS_ERR_INVALID_STATE
 *  DCS_ERR_REPLICA_FAILED
 *  DCS_ERR_CCR_ACCESS
 */

/*
 * Switchover a dcs service out of a specific node - from the node
 * with node id 'from_node_id' to any other node that can host the service.
 * There could be a race where the switchover could have been completed even
 * before we get here. This race is possible when we evacuate services from a
 * node. This is handled by checking if the devicegroup has already been moved
 * out of this node.
 */
dc_error_t dcs_switchover_service_from_node(
    const char *service_name,
    nodeid_t from_node_id);
/*
 * Possible return values:
 *  DCS_SUCCESS
 *  DCS_ERR_NOT_CLUSTER
 *  DCS_ERR_SERVICE_NAME
 *  DCS_ERR_SERVICE_INACTIVE
 *  DCS_ERR_INVALID_STATE
 */

/*
 * Startup a service with the specified node as the primary.  If "id" is 0, use
 * the configured parameters to do the startup.
 * If the service has already been started up, it will be switched over to node
 * "id", if the value of "id" is non-zero.
 */
dc_error_t dcs_startup_service(
    const char *service_name,
    nodeid_t id);
/*
 * Possible return values:
 *  DCS_SUCCESS
 *  DCS_ERR_NOT_CLUSTER
 *  DCS_ERR_SERVICE_INACTIVE
 *  DCS_ERR_NODE_ID
 *  DCS_ERR_INVALID_STATE
 *  DCS_ERR_REPLICA_FAILED
 *  DCS_ERR_CCR_ACCESS
 */

/*
 * Get all the service names from the DCS.
 * Memory is allocated for 'service_names' and should be freed using
 * 'dcs_free_string_seq'.
 */
dc_error_t dcs_get_service_names(
    dc_string_seq_t **service_names);
/*
 * Possible return values:
 *  DCS_SUCCESS
 *  DCS_ERR_NOT_CLUSTER
 *  DCS_ERR_CCR_ACCESS
 */

/*
 * Get all the service class names from the DCS.
 * Memory is allocated for 'service_names' and should be freed using
 * 'dcs_free_string_seq'.
 */
dc_error_t dcs_get_service_class_names(
    dc_string_seq_t **service_class_names);
/*
 * Possible return values:
 *  DCS_SUCCESS
 *  DCS_ERR_NOT_CLUSTER
 *  DCS_ERR_CCR_ACCESS
 */

/*
 * Get all the service names associated with the class 'service_class' from
 * the DCS.
 * Memory is allocated for 'service_names' and should be freed using
 * 'dcs_free_string_seq'.
 */
dc_error_t dcs_get_service_names_of_class(
    const char *service_class,
    dc_string_seq_t **service_names);
/*
 * Possible return values:
 *  DCS_SUCCESS
 *  DCS_ERR_NOT_CLUSTER
 *  DCS_ERR_SERVICE_CLASS_NAME
 *  DCS_ERR_CCR_ACCESS
 */

/*
 * Given a dev_t value, return the name of the service that the device belongs
 * to.
 */
dc_error_t dcs_get_service_name_by_dev_t(major_t maj, minor_t min,
    char **service_name);
/*
 * Possible return values:
 *  DCS_SUCCESS
 *  DCS_ERR_NOT_CLUSTER
 *  DCS_ERR_DEVICE_INVAL
 */

/*
 * Get the configuration of a device service.
 * Memory is allocated for 'service_class_name', 'nodes_seq' and
 * 'gdev_range_seq', and should be freed using 'dcs_free_string',
 * 'dcs_free_dc_replica_seq_t', and 'dcs_free_dc_gdev_seq_t' respectively.
 */
dc_error_t dcs_get_service_parameters(
    const char *service_name,
    char **service_class_name,
    int *failback_enabled,
    dc_replica_seq_t **nodes_seq,
    unsigned int *num_secs,
    dc_gdev_range_seq_t **gdev_range_seq,
    dc_property_seq_t **property_seq);
/*
 * Possible return values:
 *  DCS_SUCCESS
 *  DCS_ERR_NOT_CLUSTER
 *  DCS_ERR_SERVICE_NAME
 *  DCS_ERR_CCR_ACCESS
 */

/*
 * This call returns the active replicas of a service.  If there is no replica
 * returned in the active node list, then the service is currently inactive.
 */
dc_error_t dcs_get_service_status(
    const char *service_name,
    int *is_suspended,
    dc_replica_status_seq_t **active_nodes,
    sc_state_code_t *state,
    char *state_string);
/*
 * Possible return values:
 *  DCS_SUCCESS
 *  DCS_ERR_NOT_CLUSTER
 *  DCS_ERR_SERVICE_NAME
 */

/*
 * This call returns the services mastered by a given node.
 * Memory is allocated for 'service_names' and should be freed using
 * 'dcs_free_string_seq_t'.
 */
dc_error_t dcs_get_services_mastered_by_node(
    dc_string_seq_t **service_names,
    nodeid_t id);
/*
 * Possible return values:
 *  DCS_SUCCESS
 *  DCS_ERR_NOT_CLUSTER
 *  DCS_ERR_NODE_ID
 */

/*
 * This call will switchover all the services mastered by a given node.  If
 * the call is unable to switch one or more of these services, it returns the
 * 'DCS_ERR_INVALID_STATE' error, and a call to 'get_services_mastered_by_node'
 * should return the services not switchover over by this call.
 */
dc_error_t dcs_switchover_services_mastered_by_node(
    nodeid_t id);
/*
 * Possible return values:
 *  DCS_SUCCESS
 *  DCS_ERR_NOT_CLUSTER
 *  DCS_ERR_NODE_ID
 *  DCS_ERR_REPLICA_BUSY
 */

/*
 * This call will attempt to shutdown the named service, and if
 * 'suspend_service' is non-zero, it will also suspend the service.
 * If the service is already shutdown and/or suspended, no error will be
 * returned.
 */
dc_error_t dcs_shutdown_service(
    const char *service_name,
    int suspend_service);
/*
 * Possible return values:
 *  DCS_SUCCESS
 *  DCS_ERR_NOT_CLUSTER
 *  DCS_ERR_SERVICE_NAME
 *  DCS_ERR_SERVICE_BUSY
 *  DCS_ERR_CCR_ACCESS
 */

/*
 * Resume (un-suspend) the specified service.  This call will return success
 * even if the call is not currently suspended.
 */
dc_error_t dcs_resume_service(
    const char *service_name);
/*
 * Possible return values:
 *  DCS_SUCCESS
 *  DCS_ERR_NOT_CLUSTER
 *  DCS_ERR_SERVICE_NAME
 *  DCS_ERR_CCR_ACCESS
 */

/*
 * Disable the DSM on this node.  This call will return with an error if any
 * device service replicas are currently primaries on this node.
 */
dc_error_t dcs_disable_dsm(nodeid_t id);
/*
 * Possible return values:
 *  DCS_SUCCESS
 *  DCS_ERR_NOT_CLUSTER
 *  DCS_ERR_INVALID_STATE
 */

/*
 * Enable the DSM on this node.
 */
dc_error_t dcs_enable_dsm(nodeid_t id);
/*
 * Possible return values:
 *  DCS_SUCCESS
 *  DCS_ERR_NOT_CLUSTER
 */

/*
 * Change the priorities of a list of nodes.
 */
dc_error_t dcs_change_node_priority(char *service_name,
dc_replica_seq_t *nodes_seq);
/*
 * Possible return values:
 * DCS_SUCCESS
 * DCS_ERR_NOT_CLUSTER
 * DCS_ERR_SERVICE_NAME
 */

#if SOL_VERSION >= __s10
/*
 * Create global device links and nodes for a new SVM metadisk
 */
#if SOL_VERSION > __s10
dc_error_t dcs_create_device_nodes(
    dev_t maj,
    dc_property_seq_t **property_seq,
    const char *device_name);
#else /* !SOL_VERSION > __s10 */
dc_error_t dcs_create_device_nodes(dev_t maj, dc_property_seq_t **property_seq);
#endif /* !SOL_VERSION > __s10 */
#endif /* SOL_VERSION >= __s10 */

/*
 * Possible return values:
 *  DCS_SUCCESS
 *  DCS_ERR_NOT_CLUSTER
 *  DCS_ERR_DEVICE_INVAL
 */

/*
 * Routines called to free memory allocated by the calls above.
 */
void
dcs_free_string(char *string);

void
dcs_free_dc_replica_seq(dc_replica_seq_t *replicas);

void
dcs_free_dc_gdev_range_seq(dc_gdev_range_seq_t *gdev_range_seq);

void
dcs_free_dc_replica_status_seq(dc_replica_status_seq_t *active_replicas);

void
dcs_free_properties(dc_property_seq_t *property_seq);

void
dcs_free_string_seq(dc_string_seq_t *string_seq);

dc_error_t dcs_get_multiowner_status(nodeid_t nodeid, uint_t *flagp);

/* locking routines for device fencing */
int check_for_fencing(const char *lock_name, int timeout,
    unsigned int retry_interval);
int release_fencing_lock(const char *lock);
int grab_fencing_lock(const char *lock);

#ifdef	__cplusplus
}
#endif

#endif	/* _LIBDCS_H */
