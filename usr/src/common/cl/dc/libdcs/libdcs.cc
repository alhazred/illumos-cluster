/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)libdcs.cc	1.40	08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <sys/os.h>
#include <sys/sysmacros.h>
#include <orb/infrastructure/orb.h>
#include <orb/invo/common.h>
#include <orb/invo/corba.h>
#include <h/dc.h>
#include <nslib/ns.h>
#include <sys/rm_util.h>
#include <dc/sys/dc_lib.h>
#include <dc/libdcs/libdcs.h>
#include <cmm/cmm_ns.h>
#include <sys/sol_version.h>

/*
 * Helper function that initializes the ORB.
 */
int
dcs_initialize()
{
	return (ORB::initialize());
}

/*
 * Helper function that shuts down the ORB.
 */
void
dcs_shutdown()
{
	ORB::shutdown();
}

/*
 * Helper function that returns a reference to the dcs object.
 */
static mdc::dc_config_ptr
get_dcs_reference()
{
	CORBA::Object_var obj;
	Environment e;

	obj = ns::wait_resolve("DCS", e);
	if (e.exception() || CORBA::is_nil(obj)) {
		e.clear();
		return (mdc::dc_config::_nil());
	}

	return (mdc::dc_config::_narrow(obj));
}

/*
 * Helper function that returns a reference to a DSM object.
 */
static mdc::device_service_manager_ptr
get_dsm_reference(nodeid_t id)
{
	CORBA::Object_var obj;
	Environment e;
	char dsm_name[10];

	os::sprintf(dsm_name, "DSM.%d", (int)id);

	obj = ns::wait_resolve(dsm_name, e);
	if (e.exception() || CORBA::is_nil(obj)) {
		e.clear();
		return (mdc::device_service_manager::_nil());
	}

	return (mdc::device_service_manager::_narrow(obj));
}

/*
 * The following helper functions convert from one data type to another.
 */
static mdc::ha_dev_type
conv(dc_ha_type_t type)
{
	ASSERT((type == TRANSPARENT_FAILOVER) || (type == EIO_FAILOVER));

	if (type == TRANSPARENT_FAILOVER)
		return (mdc::TRANSPARENT_FAILOVER);
	else
		return (mdc::EIO_FAILOVER);
}

static dc_ha_type_t
conv(mdc::ha_dev_type type)
// LINTED Redefinition of symbol 'conv(int)'
{
	ASSERT((type == mdc::TRANSPARENT_FAILOVER) ||
	    (type == mdc::EIO_FAILOVER));

	if (type == mdc::TRANSPARENT_FAILOVER)
		return (TRANSPARENT_FAILOVER);
	else
		return (EIO_FAILOVER);
}

static mdc::node_preference_seq_t *
conv(const dc_replica_seq_t *replicas_seq)
{
	mdc::node_preference_seq_t *_nodes = new mdc::node_preference_seq_t;

	if (replicas_seq == NULL) {
		_nodes->length(0);
		return (_nodes);
	}

	_nodes->length(replicas_seq->count);
	for (uint_t i = 0; i < replicas_seq->count; i++) {
		(*_nodes)[i].id = (replicas_seq->replicas)[i].id;
		(*_nodes)[i].preference =
		    (replicas_seq->replicas)[i].preference;
	}

	return (_nodes);
}

static dc_replica_seq_t *
conv(mdc::node_preference_seq_t *nodes)
{
	dc_replica_seq_t *nodes_seq = new dc_replica_seq_t;

	if ((nodes == NULL) || (nodes->length() == 0)) {
		nodes_seq->count = 0;
		nodes_seq->replicas = NULL;
		return (nodes_seq);
	}

	nodes_seq->count = nodes->length();
	nodes_seq->replicas = new dc_replica_t[nodes_seq->count];
	for (uint_t i = 0; i < nodes_seq->count; i++) {
		(nodes_seq->replicas)[i].id = (*nodes)[i].id;
		(nodes_seq->replicas)[i].preference =
		    (*nodes)[i].preference;
	}

	return (nodes_seq);
}

static mdc::dev_range_seq_t *
conv(const dc_gdev_range_seq_t *gdev_range_seq)
{
	mdc::dev_range_seq_t *gdev_ranges = new mdc::dev_range_seq_t;

	uint_t i, gdev_ranges_count = 0;

	if (gdev_range_seq != NULL) {
		gdev_ranges_count = gdev_range_seq->count;
	}

	gdev_ranges->length(gdev_ranges_count);
	for (i = 0; i < gdev_ranges_count; i++) {
		(*gdev_ranges)[i].maj = (gdev_range_seq->ranges)[i].maj;
		(*gdev_ranges)[i].start_minor =
		    (gdev_range_seq->ranges)[i].start_gmin;
		(*gdev_ranges)[i].end_minor =
		    (gdev_range_seq->ranges)[i].end_gmin;
	}

	return (gdev_ranges);
}

static dc_gdev_range_seq_t *
conv(const mdc::dev_range_seq_t *gdev_ranges)
{
	dc_gdev_range_seq_t *gdev_range_seq = new dc_gdev_range_seq_t;

	if ((gdev_ranges == NULL) || (gdev_ranges->length() == 0)) {
		gdev_range_seq->count = 0;
		gdev_range_seq->ranges = NULL;
		return (gdev_range_seq);
	}

	gdev_range_seq->count = gdev_ranges->length();
	gdev_range_seq->ranges = new dc_gdev_range_t[gdev_range_seq->count];
	for (uint_t i = 0; i < gdev_range_seq->count; i++) {
		(gdev_range_seq->ranges)[i].maj =
		    (*gdev_ranges)[i].maj;
		(gdev_range_seq->ranges)[i].start_gmin =
		    (*gdev_ranges)[i].start_minor;
		(gdev_range_seq->ranges)[i].end_gmin =
		    (*gdev_ranges)[i].end_minor;
	}

	return (gdev_range_seq);
}

static mdc::service_property_seq_t *
conv(const dc_property_seq_t *property_seq)
{
	mdc::service_property_seq_t *properties =
	    new mdc::service_property_seq_t;

	uint_t i, properties_count = 0;

	if (property_seq != NULL) {
		properties_count = property_seq->count;
	}

	properties->length(properties_count);
	for (i = 0; i < properties_count; i++) {
		(*properties)[i].name = (const char *)
		    ((property_seq->properties)[i].name);
		(*properties)[i].value = (const char *)
		    (property_seq->properties)[i].value;
	}

	return (properties);
}

static dc_property_seq_t *
conv(const mdc::service_property_seq_t *properties)
{
	dc_property_seq_t *property_seq = new dc_property_seq_t;

	if ((properties == NULL) || (properties->length() == 0)) {
		property_seq->count = 0;
		property_seq->properties = NULL;
		return (property_seq);
	}

	property_seq->count = properties->length();
	property_seq->properties = new dc_property_t[property_seq->count];
	for (uint_t i = 0; i < property_seq->count; i++) {
		(property_seq->properties)[i].name = dclib::strdup(
		    (*properties)[i].name);
		(property_seq->properties)[i].value = dclib::strdup(
		    (*properties)[i].value);
	}

	return (property_seq);
}

static dc_replica_state_t
conv(replica::repl_prov_state state)
// LINTED Redefinition of symbol 'conv(int)'
{
	switch (state) {
	case replica::AS_DOWN:
		return (INACTIVE);
	case replica::AS_SPARE:
		return (SPARE);
	case replica::AS_SECONDARY:
		return (SECONDARY);
	case replica::AS_PRIMARY:
		return (PRIMARY);
	default:
		ASSERT(0);
	}
	os::warning("Invalid state (%d)\n", (int)state);
	return (INACTIVE);
}

static void
conv(const dc_string_seq_t *string_seq, sol::string_seq_t *&strings)
{
	strings = new sol::string_seq_t;
	unsigned int count = 0;

	if (string_seq != NULL) {
		count = string_seq->count;
	}

	strings->length(count);
	for (uint_t i = 0; i < count; i++) {
		(*strings)[i] = (const char *)((string_seq->strings)[i]);
	}
}

static dc_error_t
conv_alloc(sol::string_seq_t *strings, dc_string_seq_t *&string_seq)
{
	string_seq = new dc_string_seq_t;
	unsigned int count = strings->length();
	unsigned int neededlen, i;

	string_seq->count = count;
	if (count == 0) {
		string_seq->strings = NULL;
		return (DCS_SUCCESS);
	}

	string_seq->strings = new char *[count];

	if (string_seq->strings == NULL)
		return (DCS_ERR_NOMEM);

	//
	// We try to be efficient here by allocating all the memory in one
	// chunk.
	//

	// Calculate the amount of memory we need...
	for (i = 0, neededlen = 0; i < count; i++)
		neededlen +=
		    (unsigned int)strlen((const char *)((*strings)[i])) + 1;

	// ... allocate it...
	char *curr_ptr = new char[neededlen];

	if (curr_ptr == NULL)
		return (DCS_ERR_NOMEM);

	// ... and fill it up.
	char **curr_service_name = string_seq->strings;
	for (i = 0; i < count; i++, curr_service_name++) {
		//lint -e668
		(void) strcpy(curr_ptr, (const char *)((*strings)[i]));
		//lint +e668
		*curr_service_name = curr_ptr;
		curr_ptr += strlen((const char *)((*strings)[i])) + 1;
	}

	return (DCS_SUCCESS);
}

struct {
	int eval;
	char *str;
} errs[] = {{DCS_SUCCESS, "Success" },
	    //lint -e786
	    {DCS_ERR_NOT_CLUSTER, "Call is not being made from a node booted "
	    "as a cluster"},
	    //lint +e786
	    {DCS_ERR_SERVICE_CLASS_NAME, "Service class name invalid"},
	    {DCS_ERR_SERVICE_NAME, "Service name invalid"},
	    {DCS_ERR_USER_PROGRAM_NAME, "User program name invalid"},
	    {DCS_ERR_NODE_ID, "Node id does not exist in the cluster"},
	    {DCS_ERR_DEVICE_INVAL, "Invalid device"},
	    {DCS_ERR_DEVICE_BUSY, "Another service is using this dev_t value"},
	    {DCS_ERR_SERVICE_INACTIVE, "Service has not yet been started"},
	    {DCS_ERR_SERVICE_BUSY, "Service is busy"},
	    {DCS_ERR_SERVICE_CLASS_BUSY, "Service class is in use"},
	    {DCS_ERR_INVALID_STATE, "Node is in a invalid state"},
	    {DCS_ERR_REPLICA_FAILED, "Node failed to become the primary"},
	    {DCS_ERR_REPLICA_BUSY, "Node is busy"},
	    {DCS_ERR_CCR_ACCESS, "Error accessing the CCR"},
	    {DCS_ERR_NODE_BUSY, "Node is busy"},
	    {DCS_ERR_PROPERTY_NAME, "Property name is invalid"},
	    {DCS_ERR_COMM, "Communication error"},
	    {DCS_ERR_MAJOR_NUM,	"Major number is invalid"},
	    {DCS_ERR_NUMSEC, "Desired number of secondaries is invalid"},
	    {DCS_ERR_VERSION, "Cluster running downlevel version"},
	    {DCS_ERR_UNEXPECTED, "Unexpected error"}};

/*
 * Function that converts a dc_error_t to a meaningful string.
 */
const char *
dcs_error_to_string(dc_error_t dc_err)
{
	for (uint_t i = 0; i < (sizeof (errs)/sizeof (*errs)); i++) {
		if (dc_err == errs[i].eval) {
			return (errs[i].str);
		}
	}

	return ("Error code unknown");
}

/*
 * Helper function that converts any exception in the environment to
 * 'dc_error_t'.
 */
static dc_error_t
get_error_and_clear(Environment &e)
{
	CORBA::Exception *ex;
	mdc::dcs_error *ex_dcs;
	dc_error_t error = DCS_SUCCESS;

	if ((ex = e.exception()) != NULL) {
		if ((ex_dcs = mdc::dcs_error::_exnarrow(ex)) != NULL) {
			error = (dc_error_t)ex_dcs->error;
		} else if (CORBA::VERSION::_exnarrow(e.exception())
		    != NULL) {
			error = DCS_ERR_VERSION;
		} else {
			error = DCS_ERR_COMM;
		}
		e.clear();
	}

	return (error);
}

dc_error_t
dcs_create_service_class(
    const char *service_class,
    const char *user_program,
    dc_ha_type_t type)
{
	mdc::dc_config_var dcobj = get_dcs_reference();
	Environment e;
	mdc::ha_dev_type ha_type = conv(type);

	if (CORBA::is_nil(dcobj)) {
		return (DCS_ERR_NOT_CLUSTER);
	}

	if (service_class == NULL) {
		return (DCS_ERR_SERVICE_CLASS_NAME);
	}
	if (user_program == NULL) {
		return (DCS_ERR_USER_PROGRAM_NAME);
	}

	dcobj->create_service_class(service_class, user_program, ha_type, e);

	return (get_error_and_clear(e));
}

dc_error_t
dcs_set_service_class_user_program(
    const char *service_class,
    const char *user_program)
{
	mdc::dc_config_var dcobj = get_dcs_reference();
	Environment e;

	if (CORBA::is_nil(dcobj)) {
		return (DCS_ERR_NOT_CLUSTER);
	}

	if (service_class == NULL) {
		return (DCS_ERR_SERVICE_CLASS_NAME);
	}
	if (user_program == NULL) {
		return (DCS_ERR_USER_PROGRAM_NAME);
	}

	dcobj->set_service_class_user_program(service_class, user_program, e);

	return (get_error_and_clear(e));
}

dc_error_t
dcs_set_service_class_ha_type(
    const char *service_class,
    dc_ha_type_t type)
{
	mdc::dc_config_var dcobj = get_dcs_reference();
	Environment e;
	mdc::ha_dev_type ha_type = conv(type);

	if (CORBA::is_nil(dcobj)) {
		return (DCS_ERR_NOT_CLUSTER);
	}

	if (service_class == NULL) {
		return (DCS_ERR_SERVICE_CLASS_NAME);
	}

	dcobj->set_service_class_ha_type(service_class, ha_type, e);

	return (get_error_and_clear(e));
}

dc_error_t
dcs_get_service_class_parameters(
    const char *service_class,
    char **user_program,
    dc_ha_type_t *type)
{
	mdc::dc_config_var dcobj = get_dcs_reference();
	Environment e;
	CORBA::String_var _user_program;
	mdc::ha_dev_type ha_type;

	ASSERT(service_class && user_program && type);

	if (CORBA::is_nil(dcobj)) {
		return (DCS_ERR_NOT_CLUSTER);
	}

	if (service_class == NULL) {
		return (DCS_ERR_SERVICE_CLASS_NAME);
	}

	//lint -e1058
	dcobj->get_service_class_parameters(service_class, _user_program,
	    ha_type, e);
	//lint +e1058

	dc_error_t error = get_error_and_clear(e);
	if (error != DCS_SUCCESS) {
		return (error);
	}

	if (user_program != NULL) {
		*user_program = os::strdup((const char *)_user_program);
	}
	if (type != NULL) {
		*type = conv(ha_type);
	}

	return (DCS_SUCCESS);
}

dc_error_t
dcs_remove_service_class(
    const char *service_class)
{
	mdc::dc_config_var dcobj = get_dcs_reference();
	Environment e;

	if (CORBA::is_nil(dcobj)) {
		return (DCS_ERR_NOT_CLUSTER);
	}

	if (service_class == NULL) {
		return (DCS_ERR_SERVICE_CLASS_NAME);
	}

	dcobj->remove_service_class(service_class, e);

	return (get_error_and_clear(e));
}

/*
 * This call adds a ha device service to the device configurator persistent
 * storage.  If num_secondaries == 0, then num_secondaries is set to the
 * default value.
 */
dc_error_t
dcs_create_service(
    const char *service_name,
    const char *service_class,
    int enable_failback,
    const dc_replica_seq_t *nodes_seq,
    unsigned int num_secs,
    const dc_gdev_range_seq_t *gdev_range_seq,
    const dc_property_seq_t *property_seq)
{
	mdc::dc_config_var dcobj = get_dcs_reference();
	mdc::node_preference_seq_t *nodes;
	mdc::dev_range_seq_t *gdev_ranges;
	mdc::service_property_seq_t *properties;
	Environment e;

	if (CORBA::is_nil(dcobj)) {
		return (DCS_ERR_NOT_CLUSTER);
	}

	if (service_name == NULL) {
		return (DCS_ERR_SERVICE_NAME);
	}
	if (service_class == NULL) {
		return (DCS_ERR_SERVICE_CLASS_NAME);
	}

	bool bool_enable_failback = (enable_failback == 0 ? false : true);
	nodes = conv(nodes_seq);
	gdev_ranges = conv(gdev_range_seq);
	properties = conv(property_seq);

	dcobj->create_device_service(service_name, service_class,
	    bool_enable_failback, *nodes, num_secs, *gdev_ranges, *properties,
	    e);

	delete nodes;
	delete gdev_ranges;
	delete properties;

	return (get_error_and_clear(e));
}

dc_error_t dcs_set_properties(
    const char *service_name,
    const dc_property_seq_t *property_seq)
{
	mdc::dc_config_var dcobj = get_dcs_reference();
	mdc::service_property_seq_t *properties;
	Environment e;

	if (CORBA::is_nil(dcobj)) {
		return (DCS_ERR_NOT_CLUSTER);
	}

	if (service_name == NULL) {
		return (DCS_ERR_SERVICE_NAME);
	}

	properties = conv(property_seq);

	dcobj->set_properties(service_name, *properties, e);

	delete properties;

	return (get_error_and_clear(e));
}

dc_error_t dcs_remove_properties(
    const char *service_name,
    const dc_string_seq_t *property_names)
{
	mdc::dc_config_var dcobj = get_dcs_reference();
	sol::string_seq_t *properties;
	Environment e;

	if (CORBA::is_nil(dcobj)) {
		return (DCS_ERR_NOT_CLUSTER);
	}

	if (service_name == NULL) {
		return (DCS_ERR_SERVICE_NAME);
	}

	conv(property_names, properties);

	dcobj->remove_properties(service_name, *properties, e);

	delete properties;

	return (get_error_and_clear(e));
}

dc_error_t dcs_get_property(
    const char *service_name,
    const char *property_name,
    char **property_value)
{
	mdc::dc_config_var dcobj = get_dcs_reference();
	Environment e;
	CORBA::String_var value;

	ASSERT(property_value != NULL);

	if (CORBA::is_nil(dcobj)) {
		return (DCS_ERR_NOT_CLUSTER);
	}

	if (service_name == NULL) {
		return (DCS_ERR_SERVICE_NAME);
	}

	//lint -e1058
	dcobj->get_property(service_name, property_name, value, e);
	//lint +e1058

	dc_error_t error = get_error_and_clear(e);
	if (error != DCS_SUCCESS) {
		return (error);
	}

	*property_value = os::strdup((const char *)value);

	return (DCS_SUCCESS);
}

dc_error_t dcs_get_all_properties(
    const char *service_name,
    dc_property_seq_t **properties)
{
	mdc::dc_config_var dcobj = get_dcs_reference();
	Environment e;
	mdc::service_property_seq_t_var property_seq;

	if (CORBA::is_nil(dcobj)) {
		return (DCS_ERR_NOT_CLUSTER);
	}

	if (service_name == NULL) {
		return (DCS_ERR_SERVICE_NAME);
	}

	//lint -e1058
	dcobj->get_all_properties(service_name, property_seq, e);
	//lint +e1058

	dc_error_t error = get_error_and_clear(e);
	if (error != DCS_SUCCESS) {
		return (error);
	}

	*properties = conv(property_seq);

	return (DCS_SUCCESS);
}

/*
 * This call will set the failback mode of this device service.  If
 * `enable_failback' is zero, this call will disable failback on the service,
 * otherwise it will enable failback.
 */
dc_error_t
dcs_set_failback_mode(
    const char *service_name,
    int enable_failback)
{
	mdc::dc_config_var dcobj = get_dcs_reference();
	Environment e;

	if (CORBA::is_nil(dcobj)) {
		return (DCS_ERR_NOT_CLUSTER);
	}

	if (service_name == NULL) {
		return (DCS_ERR_SERVICE_NAME);
	}

	bool bool_enable_failback = (enable_failback == 0 ? false : true);

	dcobj->set_failback_mode(service_name, bool_enable_failback, e);

	return (get_error_and_clear(e));
}

/*
 * Set the number of active secondaries in a device service.
 */
dc_error_t
dcs_set_active_secondaries(
    const char *service_name,
    unsigned int num_secs)
{
	mdc::dc_config_var dcobj = get_dcs_reference();
	Environment e;

	if (CORBA::is_nil(dcobj)) {
		return (DCS_ERR_NOT_CLUSTER);
	}

	if (service_name == NULL) {
		return (DCS_ERR_SERVICE_NAME);
	}

	dcobj->set_active_secondaries(service_name, num_secs, e);

	return (get_error_and_clear(e));
}

/*
 * Add a replica to the service or change the preference of an existing
 * replica.
 */
dc_error_t
dcs_add_node(
    const char *service_name,
    dc_replica_t *repl)
{
	mdc::dc_config_var dcobj = get_dcs_reference();
	mdc::node_preference node;
	Environment e;

	if (CORBA::is_nil(dcobj)) {
		return (DCS_ERR_NOT_CLUSTER);
	}

	if (service_name == NULL) {
		return (DCS_ERR_SERVICE_NAME);
	}
	if (repl == NULL) {
		return (DCS_ERR_NODE_ID);
	}

	node.id = repl->id;
	node.preference = repl->preference;

	dcobj->add_node(service_name, node, e);

	return (get_error_and_clear(e));
}

/*
 * Add devices to this service.
 */
dc_error_t
dcs_add_devices(
    const char *service_name,
    const dc_gdev_range_seq_t *gdev_range_seq)
{
	mdc::dc_config_var dcobj = get_dcs_reference();
	mdc::dev_range_seq_t *gdev_ranges;
	Environment e;

	if (CORBA::is_nil(dcobj)) {
		return (DCS_ERR_NOT_CLUSTER);
	}

	if (service_name == NULL) {
		return (DCS_ERR_SERVICE_NAME);
	}

	gdev_ranges = conv(gdev_range_seq);
	if (gdev_ranges == NULL) {
		return (DCS_SUCCESS);
	}

	dcobj->add_devices(service_name, *gdev_ranges, e);

	delete (gdev_ranges);

	return (get_error_and_clear(e));
}

/*
 * Remove a replica from the service.
 */
dc_error_t
dcs_remove_node(
    const char *service_name,
    nodeid_t id)
{
	mdc::dc_config_var dcobj = get_dcs_reference();
	Environment e;

	if (CORBA::is_nil(dcobj)) {
		return (DCS_ERR_NOT_CLUSTER);
	}

	if (service_name == NULL) {
		return (DCS_ERR_SERVICE_NAME);
	}

	dcobj->remove_node(service_name, (sol::nodeid_t)id, e);

	return (get_error_and_clear(e));
}

/*
 * Removes a dev_t value from a service. The type of this device
 * will revert to what it was before it was added to this service.
 */
dc_error_t
dcs_remove_devices(
    const char *service_name,
    const dc_gdev_range_seq_t *gdev_range_seq)
{
	mdc::dc_config_var dcobj = get_dcs_reference();
	mdc::dev_range_seq_t *gdev_ranges;
	Environment e;

	if (CORBA::is_nil(dcobj)) {
		return (DCS_ERR_NOT_CLUSTER);
	}

	if (service_name == NULL) {
		return (DCS_ERR_SERVICE_NAME);
	}

	gdev_ranges = conv(gdev_range_seq);

	dcobj->remove_devices(service_name, *gdev_ranges, e);

	delete (gdev_ranges);

	return (get_error_and_clear(e));
}

/*
 * Remove a service definition.  This will only work if the service is not
 * currently active.
 */
dc_error_t
dcs_remove_service(
    const char *service_name)
{
	mdc::dc_config_var dcobj = get_dcs_reference();
	Environment e;

	if (CORBA::is_nil(dcobj)) {
		return (DCS_ERR_NOT_CLUSTER);
	}

	if (service_name == NULL) {
		return (DCS_ERR_SERVICE_NAME);
	}

	dcobj->remove_service(service_name, e);

	return (get_error_and_clear(e));
}


dc_error_t dcs_get_service_class_names(
    dc_string_seq_t **service_class_names)
{
	ASSERT(service_class_names);

	mdc::dc_config_var dcobj = get_dcs_reference();
	sol::string_seq_t_var names;
	Environment e;

	if (CORBA::is_nil(dcobj)) {
		return (DCS_ERR_NOT_CLUSTER);
	}

	//lint -e1058
	dcobj->get_service_class_names(names, e);
	//lint +e1058

	dc_error_t error = get_error_and_clear(e);
	if (error != DCS_SUCCESS) {
		return (error);
	}

	return (conv_alloc(names, *service_class_names));
}

dc_error_t
dcs_get_service_names_of_class(
    const char *service_class,
    dc_string_seq_t **service_names)
{
	ASSERT(service_class && service_names);

	mdc::dc_config_var dcobj = get_dcs_reference();
	sol::string_seq_t_var names;
	Environment e;

	if (CORBA::is_nil(dcobj)) {
		return (DCS_ERR_NOT_CLUSTER);
	}

	if (service_class == NULL) {
		return (DCS_ERR_SERVICE_CLASS_NAME);
	}

	//lint -e1058
	dcobj->get_service_names_of_class(service_class, names, e);
	//lint +e1058

	dc_error_t error = get_error_and_clear(e);
	if (error != DCS_SUCCESS) {
		return (error);
	}

	return (conv_alloc(names, *service_names));
}

/*
 * Get all the service names from the DCS
 */
dc_error_t
dcs_get_service_names(
    dc_string_seq_t **service_names)
{
	ASSERT(service_names);

	mdc::dc_config_var dcobj = get_dcs_reference();
	sol::string_seq_t_var names;
	Environment e;

	if (CORBA::is_nil(dcobj)) {
		return (DCS_ERR_NOT_CLUSTER);
	}

	//lint -e1058
	dcobj->get_service_names(names, e);
	//lint +e1058

	dc_error_t error = get_error_and_clear(e);
	if (error != DCS_SUCCESS) {
		return (error);
	}

	return (conv_alloc(names, *service_names));
}

/*
 * Given a dev_t value, return the name of the service that the device belongs
 * to.
 */
dc_error_t dcs_get_service_name_by_dev_t(major_t maj, minor_t min,
    char **service_name)
{
	mdc::dc_config_var dcobj = get_dcs_reference();
	CORBA::String_var _service_name;
	Environment e;

	ASSERT(service_name != NULL);

	if (CORBA::is_nil(dcobj)) {
		return (DCS_ERR_NOT_CLUSTER);
	}

	//lint -e1058
	dcobj->get_service_by_dev_t(maj, min, _service_name, e);
	//lint +e1058

	dc_error_t error = get_error_and_clear(e);
	if (error != DCS_SUCCESS) {
		return (error);
	}

	*service_name = os::strdup((const char *)_service_name);

	return (DCS_SUCCESS);
}

/*
 * This call will fill in `user_pgm', `nodes_seq' and `gdev_range_seq',
 * allocating memory for those parameters.  It is the responsibility of the
 * caller to free the memory associated with these parameters.
 */
dc_error_t
dcs_get_service_parameters(
    const char *service_name,
    char **service_class,
    int *failback_enabled,
    dc_replica_seq_t **nodes_seq,
    unsigned int *num_secs,
    dc_gdev_range_seq_t **gdev_range_seq,
    dc_property_seq_t **property_seq)
{
	mdc::dc_config_var dcobj = get_dcs_reference();
	Environment e;
	CORBA::String_var _service_class;
	mdc::node_preference_seq_t_var _nodes;
	mdc::dev_range_seq_t_var _gdev_ranges;
	mdc::service_property_seq_t_var _properties;
	bool _failback_enabled;
	unsigned int _num_secs;

	if (CORBA::is_nil(dcobj)) {
		return (DCS_ERR_NOT_CLUSTER);
	}

	if (service_name == NULL) {
		return (DCS_ERR_SERVICE_NAME);
	}

	//lint -e1058
	dcobj->get_service_parameters(service_name, _service_class,
	    _failback_enabled, _nodes, _num_secs, _gdev_ranges, _properties,
	    e);
	//lint +e1058

	dc_error_t error = get_error_and_clear(e);
	if (error != DCS_SUCCESS) {
		return (error);
	}

	if (failback_enabled != NULL) {
		*failback_enabled = (_failback_enabled ? 1 : 0);
	}
	if (service_class != NULL) {
		*service_class = os::strdup((const char *)_service_class);
	}
	if (nodes_seq != NULL) {
		*nodes_seq = conv(_nodes);
	}
	if (num_secs != NULL) {
		*num_secs = _num_secs;
	}
	if (gdev_range_seq != NULL) {
		*gdev_range_seq = conv(_gdev_ranges);
	}
	if (property_seq != NULL) {
		*property_seq = conv(_properties);
	}

	return (DCS_SUCCESS);
}

/*
 * Switchover to a specific node.  If the nodeid parameter is 0, then we
 * switchover to the next available (in the order of preference specified
 * while creating the service) node. An issue with using nodeid=0 is
 * that calling dcs_switchover_service() doesn't guarantee that the service
 * won't end up back on the same node. To ensure a service is switched from
 * a specified node, then use dcs_switchover_service_from_node() instead.
 */
dc_error_t
dcs_switchover_service(
    const char *service_name,
    nodeid_t id)
{
	Environment e;

	// Get the handle to the rma
	replica::rm_admin_var rm_ref = rm_util::get_rm();

	if (CORBA::is_nil(rm_ref)) {
		return (DCS_ERR_NOT_CLUSTER);
	}

	if (service_name == NULL) {
		return (DCS_ERR_SERVICE_NAME);
	}

	// Get the service id and provider id of the device service.
	CORBA::String_var s_id = dclib::get_serviceid(service_name);
	CORBA::String_var p_id = new char[dclib::NODEID_STRLEN];

	// Get a handle to the service admin. for this service
	replica::service_admin_var s_admin = rm_ref->get_control(s_id, e);
	if (e.exception()) {
		e.clear();
		return (DCS_ERR_SERVICE_INACTIVE);
	}

	// Get a list of the replica providers for this service
	replica::repl_prov_seq_var repl_provs;
	//lint -e1058
	s_admin->get_repl_provs(repl_provs, e);
	//lint +e1058
	if (e.exception()) {
		e.clear();
		return (DCS_ERR_SERVICE_INACTIVE);
	}

	(void) sprintf((char *)p_id, "%d", id);

	// Find the right provider to switchover
	for (uint_t i = 0; i < repl_provs.length(); i++) {
		if (id == 0) {
			// Switch to any provider that is currently a secondary
			if (repl_provs[i].curr_state == replica::AS_SECONDARY) {
				s_admin->change_repl_prov_status(
				    repl_provs[i].repl_prov_desc,
				    replica::SC_SET_PRIMARY, false, e);
				if (e.exception()) {
					//
					// The switchover failed - try another
					// replica.
					//
#ifdef	DEBUG
					e.exception()->print_exception("Error "
					    "trying to switchover: ");
#endif
					e.clear();
					continue;
				}
				return (DCS_SUCCESS);
			}
			continue;
		}
		// Match the provider id with the node id passed in
		if (strcmp(repl_provs[i].repl_prov_desc, p_id) == 0) {
			s_admin->change_repl_prov_status(
			    repl_provs[i].repl_prov_desc,
			    replica::SC_SET_PRIMARY, false, e);
			if (e.exception()) {
#ifdef	DEBUG
				e.exception()->print_exception("Error trying "
				    "to switchover: ");

#endif
				e.clear();
				return (DCS_ERR_REPLICA_FAILED);
			}
			return (DCS_SUCCESS);
		}
	}

	return (DCS_ERR_INVALID_STATE);
}

/*
 * Switchover a dcs service out of a specific node - from the node
 * with node id 'from_node_id' to any other node that can host the service.
 * There could be a race where the switchover could have been completed even
 * before we get here. This race is possible when we evacuate services from a
 * node. This is handled by checking if the devicegroup has already been moved
 * out of this node.
 */
dc_error_t
dcs_switchover_service_from_node(const char *service_name,
    nodeid_t from_node_id)
{
	Environment e;
	int service_changed;

	// Get the handle to the rma
	replica::rm_admin_var rm_ref = rm_util::get_rm();

	if (CORBA::is_nil(rm_ref)) {
		return (DCS_ERR_NOT_CLUSTER);
	}

	if (service_name == NULL) {
		return (DCS_ERR_SERVICE_NAME);
	}

	// Get the service id and provider id of the device service.
	CORBA::String_var s_id = dclib::get_serviceid(service_name);
	CORBA::String_var p_id = new char[dclib::NODEID_STRLEN];
	(void) sprintf((char *)p_id, "%d", from_node_id);

	//
	// We have to be told which node to switch out of
	//
	ASSERT(from_node_id != 0);

	do {
		service_changed = 0;

		// Get a handle to the service admin. for this service
		replica::service_admin_var s_admin =
		    rm_ref->get_control(s_id, e);
		if (e.exception()) {
			e.clear();
			return (DCS_ERR_SERVICE_INACTIVE);
		}

		// Get a list of the replica providers for this service
		replica::repl_prov_seq_var repl_provs;
		//lint -e1058
		s_admin->get_repl_provs(repl_provs, e);
		//lint +e1058
		if (e.exception()) {
			e.clear();
			return (DCS_ERR_SERVICE_INACTIVE);
		}

		for (uint_t i = 0; i < repl_provs.length(); i++) {
			if (repl_provs[i].curr_state == replica::AS_PRIMARY) {
				if (strcmp(repl_provs[i].repl_prov_desc,
				    p_id) != 0) {
					//
					// Already switched out of node
					// 'from_node_id'. Nothing needs to be
					// done here. Report success.
					//
					return (DCS_SUCCESS);
				}
				break;
			}
		}

		// Find the right provider to switchover
		for (uint_t i = 0; i < repl_provs.length(); i++) {
			// Switch to any provider that is currently a secondary
			if (repl_provs[i].curr_state == replica::AS_SECONDARY) {

				// Assert that we are switching out of
				// from_node_id
				ASSERT(strcmp(repl_provs[i].repl_prov_desc,
				    p_id) != 0);

				s_admin->change_repl_prov_status(
				    repl_provs[i].repl_prov_desc,
				    replica::SC_SET_PRIMARY, false, e);

				CORBA::Exception *ex;
				if ((ex = e.exception()) != NULL) {
					if (replica::service_admin::
					    service_changed::_exnarrow(ex) !=
					    NULL) {
						//
						// Our view of the service has
						// changed. We retry the whole
						// switchover process.
						//
						e.clear();
						service_changed = 1;
						break;
					} else {
						//
						// The switchover failed - try
						// another replica.
						//
#ifdef	DEBUG
						e.exception()->print_exception(
						    "Error trying to "
						    "switchover: ");
#endif
						e.clear();
					}
				} else {
					//
					// Switchover successful!
					//
					return (DCS_SUCCESS);
				}
			}
		}
	} while (service_changed);

	// We have failed to switchover. So we return an error.
	return (DCS_ERR_INVALID_STATE);
}

/*
 * Startup a service with the specified node as the primary.  If "id" is 0, use
 * the configured parameters to do the startup.
 * If the service has already been started up, it will be switched over to node
 * "id", if the value of "id" is non-zero.
 */
dc_error_t dcs_startup_service(
    const char *service_name,
    nodeid_t id)
{
	int num_tries = 0;
	dc_error_t error;

	if (service_name == NULL) {
		return (DCS_ERR_SERVICE_NAME);
	}

	mdc::dc_config_var dcobj = get_dcs_reference();
	Environment e;

	if (CORBA::is_nil(dcobj)) {
		return (DCS_ERR_NOT_CLUSTER);
	}

	while (num_tries < SWITCHOVER_TRIES) {
	    dcobj->cascaded_switch_device_service(service_name, id, e);
	    num_tries++;
	    error = get_error_and_clear(e);
	    //
	    // DCS_FREEZE_IN_PROGRESS error means DCS is being frozen.
	    // We sleep for SLEEP_SEC and retry SWITCHOVER_TRIES-1
	    // times before returning the error to the caller.
	    //
	    if (error == DCS_FREEZE_IN_PROGRESS &&
			num_tries < SWITCHOVER_TRIES) {
		os::usecsleep((os::usec_t)SLEEP_SEC*1000000);
	    } else {
		break;
	    }
	}

	return (error);
}

/*
 * This call returns the active replicas of a service.  If there is no replica
 * returned in the active node list, then the service is currently inactive.
 */
dc_error_t
dcs_get_service_status(
    const char *service_name,
    int	*is_suspended,
    dc_replica_status_seq_t **active_replicas,
    sc_state_code_t *state,
    char *)
{
	Environment	env;
	dc_error_t	error;
	bool		suspend_state = false;

	//
	// We should be able to get suspended status for non-replicated
	// service.
	//

	mdc::dc_config_var dcobj = get_dcs_reference();

	if (CORBA::is_nil(dcobj)) {
		return (DCS_ERR_NOT_CLUSTER);
	}

	if (service_name == NULL) {
		return (DCS_ERR_SERVICE_NAME);
	}

	dcobj->is_suspended(service_name, suspend_state, env);
	error = get_error_and_clear(env);

	if (error != DCS_SUCCESS) {
		return (error);
	}

	*is_suspended = (suspend_state ? 1 : 0);

	// Get the rma for this node
	replica::rm_admin_var rm_ref = rm_util::get_rm();

	if (CORBA::is_nil(rm_ref)) {
		return (DCS_ERR_NOT_CLUSTER);
	}

	// Initialize variables
	CORBA::String_var s_id = dclib::get_serviceid(service_name);

	// Get the service admin for this service
	replica::service_admin_var s_admin = rm_ref->get_control(s_id, env);
	if (env.exception()) {
		// Service is not active
		env.clear();
		*active_replicas = new dc_replica_status_seq_t;
		(*active_replicas)->count = 0;
		(*active_replicas)->replicas = NULL;
		*state = SC_STATE_OFFLINE;
		return (DCS_SUCCESS);
	}

	// Get the list of replica providers for this service
	replica::repl_prov_seq_var repl_provs;
	//lint -e1058
	s_admin->get_repl_provs(repl_provs, env);
	//lint +e1058
	if (env.exception()) {
		env.clear();
		return (DCS_ERR_COMM);
	}

	nodeid_t id;
	uint_t len = repl_provs.length();

	*active_replicas = new dc_replica_status_seq_t;
	(*active_replicas)->count = len;
	(*active_replicas)->replicas = new dc_replica_status_t[len];

	uint_t num_secondaries = 0;
	int primary_found = false;

	// Get the configured nodes for this service
	dc_replica_seq_t *nodes_seq;
	unsigned int configured_num_secs;

	error = dcs_get_service_parameters(service_name, NULL,
	    NULL, &nodes_seq, &configured_num_secs, NULL, NULL);

	if (error != DCS_SUCCESS) {
		return (error);
	}

	// Store the information in the required form
	for (uint_t i = 0; i < len; i++) {
		id = (uint_t)(atoi(repl_provs[i].repl_prov_desc));
		if (id == 0) {
			os::warning("Unexpected provider id %s for "
			    "service %s", (char *)repl_provs[i].repl_prov_desc,
			    (char *)s_id);
		}
		((*active_replicas)->replicas)[i].id = id;
		((*active_replicas)->replicas)[i].state =
		    conv(repl_provs[i].curr_state);
		switch (repl_provs[i].curr_state) {
		case replica::AS_SECONDARY:
			num_secondaries++;
			break;
		case replica::AS_PRIMARY:
			primary_found = true;
			break;
		case replica::AS_DOWN:
		case replica::AS_SPARE:
		default:
			break;
		}
	}

	//
	// If the configured number of secondaries for the service is zero, then
	// it should be set to the default value.
	//
	if (configured_num_secs == 0) {
		configured_num_secs = DCS_DEFAULT_DESIRED_NUM_SECONDARIES;
	}

	if (primary_found) {
		//
		// If there is a primary, then the service definitely exists,
		// and could either be in the 'SC_STATE_ONLINE' state or in the
		// 'SC_STATE_DEGRADED' state.
		//
		if (nodes_seq->count == 1) {
			// This device service has only one host configured.
			*state = SC_STATE_ONLINE;
		} else if (configured_num_secs > num_secondaries) {
			*state = SC_STATE_DEGRADED;
		} else {
			*state = SC_STATE_ONLINE;
		}
	} else {
		//
		// There is no primary - check to see if this is in the middle
		// of a transition, and if so, return 'SC_STATE_WAIT'.
		// Otherwise, this service is definitely 'SC_STATE_OFFLINE'.
		// XXX When we implement suspend/resume, we should also signal
		// the 'SC_STATE_FAULTED' state.
		//
		if (num_secondaries > 0) {
			*state = SC_STATE_WAIT;
		} else {
			*state = SC_STATE_OFFLINE;
		}
	}

	dcs_free_dc_replica_seq(nodes_seq);

	return (DCS_SUCCESS);
}

dc_error_t
dcs_get_services_mastered_by_node(
    char ***,
    nodeid_t)
{
	return (DCS_ERR_UNEXPECTED);
}

dc_error_t
dcs_switchover_services_mastered_by_node(
    nodeid_t)
{
	return (DCS_ERR_UNEXPECTED);
}

dc_error_t
dcs_shutdown_service(
    const char *service_name,
    int suspend)
{
	mdc::dc_config_var dcobj = get_dcs_reference();
	Environment e;
	bool bool_suspend = ((suspend == 0) ? false : true);

	if (CORBA::is_nil(dcobj)) {
		return (DCS_ERR_NOT_CLUSTER);
	}

	if (service_name == NULL) {
		return (DCS_ERR_SERVICE_NAME);
	}

	dcobj->shutdown_service(service_name, bool_suspend, e);

	return (get_error_and_clear(e));
}

dc_error_t
dcs_resume_service(
    const char *service_name)
{
	mdc::dc_config_var dcobj = get_dcs_reference();
	Environment e;

	if (CORBA::is_nil(dcobj)) {
		return (DCS_ERR_NOT_CLUSTER);
	}

	if (service_name == NULL) {
		return (DCS_ERR_SERVICE_NAME);
	}

	dcobj->resume_service(service_name, e);

	return (get_error_and_clear(e));
}

dc_error_t
dcs_disable_dsm(nodeid_t id)
{
	mdc::device_service_manager_ptr dsmobj = get_dsm_reference(id);
	Environment e;

	if (CORBA::is_nil(dsmobj)) {
		return (DCS_ERR_NOT_CLUSTER);
	}

	dsmobj->disable_dsm(e);

	return (get_error_and_clear(e));
}

dc_error_t
dcs_enable_dsm(nodeid_t id)
{
	mdc::device_service_manager_ptr dsmobj = get_dsm_reference(id);
	Environment e;

	if (CORBA::is_nil(dsmobj)) {
		return (DCS_ERR_NOT_CLUSTER);
	}

	dsmobj->enable_dsm(e);

	return (get_error_and_clear(e));
}

void
dcs_free_string(char *string)
{
	if (string)
		delete [] string;
}

void
dcs_free_dc_replica_seq(dc_replica_seq_t *nodes_seq)
{
	if (nodes_seq) {
		if (nodes_seq->replicas)
			delete [] nodes_seq->replicas;
		delete nodes_seq;
	}
}

void
dcs_free_dc_gdev_range_seq(dc_gdev_range_seq_t *gdev_range_seq)
{
	if (gdev_range_seq) {
		if (gdev_range_seq->ranges)
			delete [] gdev_range_seq->ranges;
		delete gdev_range_seq;
	}
}

void
dcs_free_dc_replica_status_seq(dc_replica_status_seq_t *active_replicas)
{
	if (active_replicas) {
		if (active_replicas->replicas)
			delete [] active_replicas->replicas;
		delete active_replicas;
	}
}

void
dcs_free_properties(dc_property_seq_t *property_seq)
{
	if (property_seq) {
		for (uint_t i = 0; i < property_seq->count; i++) {
			delete [] (property_seq->properties)[i].name;
			delete [] (property_seq->properties)[i].value;
		}
		if (property_seq->properties)
			delete [] property_seq->properties;
		delete property_seq;
	}
}

void
dcs_free_string_seq(dc_string_seq_t *string_seq)
{
	if (string_seq) {
		// We need to delete 0'th element since we are
		// doing malloc for all the strings as a single
		// array
		if (string_seq->strings) {
			delete [] string_seq->strings[0];
			delete [] string_seq->strings;
		}
		delete string_seq;
	}
}
//
// This method changes the priority of the list of nodes supplied
// for a service. It is called from the sysmgmt code when new nodes
// are added to a service. It calls change_nodes_priority on the
// device configurator object to get the job done.
//
dc_error_t
dcs_change_node_priority(char *service_name,
	dc_replica_seq_t *nodes_seq)
{
	mdc::node_preference_seq_t *nodes;
	Environment e;

	// Get a reference to the device configurator object.
	mdc::dc_config_var dcobj = get_dcs_reference();

	if (CORBA::is_nil(dcobj)) {
		return (DCS_ERR_NOT_CLUSTER);
	}

	if (service_name == NULL) {
		return (DCS_ERR_SERVICE_NAME);
	}

	nodes = conv(nodes_seq);

	// Invoke the corresponding method on the DCS primary.
	dcobj->change_nodes_priority(service_name, *nodes, e);

	return (get_error_and_clear(e));
}

/*
 * Determine if the specified node is SC_STATE_ONLINE or SC_STATE_OFFLINE.
 * This function follows the same behavior as scconf_ismember() and
 * will set the location pointed by "flagp" to non-zero if the
 * node is SC_STATE_ONLINE in the cluster. Otherwise, "flagp" will be set
 * to zero.
 *
 * The SC_STATE_ONLINE/SC_STATE_OFFLINE status is checked by calling into the
 * nameserver and getting the cluster status directly from ucmmd.
 */
dc_error_t
dcs_get_multiowner_status(nodeid_t nodeid, uint_t *flagp)
{
	cmm::cluster_state_t clust_state;
	const char *ucmm_name = "UCMM";
	Environment e;

	cmm::control_var ctrlv = cmm_ns::get_control(ucmm_name,
	    nodeid);
	if (CORBA::is_nil(ctrlv)) {
		// The node is not running ucmmd, thus SC_STATE_OFFLINE
		*flagp = 0;
		return (DCS_SUCCESS);
	}

	ctrlv->get_cluster_state(clust_state, e);
	if (e.exception()) {
		// SC_STATE_OFFLINE is assumed on error
		*flagp = 0;
		return (DCS_SUCCESS);
	}

	if (clust_state.members.members[nodeid] == INCN_UNKNOWN) {
		*flagp = 0;
	} else {
		*flagp = 1;
	}
	return (DCS_SUCCESS);
}


//
// Check for a device fencing lock, an indication that device fencing is in
// progress.  The lock is of the form <lock-tag>.<node-id>.  Only the <lock-tag>
// should be specified when checking for a lock, as this routine will cycle
// through all available node ids to see if any matching locks are held for any
// cluster nodes.
// The default timeout value (accessed by passing in a timeout value of -2) is
// set to the CMM timeout.  This allows modules which are dependent on fencing
// to continue on even if fencing has not yet completed, since the node which is
// being fenced should have brought itself down by this point.  If fencing
// completes before the CMM timeout, which should generally be the case, then
// dependent modules can continue onward before the errant node has killed
// itself, secure in the fact that the erant node has been fenced.
// Input parameters:
//	char *lock	lock tag, currently expected to be FENCE_NODE
//	int timeout    	how many seconds to wait for the lock to be released.
//			  0 indicates no wait, the call returns immediately,
//			  with the current fencing status.  -1 indicates
//			  infinite wait, do not return until no lock is held.
//			  -2 indicates use default timeout.
//	unsigned int retry_interval	how many seconds to wait between checks
//					  to see if the locks have been dropped
// Returns the following values:
//	0	no matching fencing locks being held
//	1	matching fencing locks still being held
//	-1	an error occurred, fencing status unknown
//	-2	an illegal parameter was passed in
//
int
check_for_fencing(const char *lock_name, int timeout,
    unsigned int retry_interval)
{
	unsigned int i;
	int rt;
	Environment e;
	CORBA::Exception *ex;
	const char *tempname;
	char *fence_lock;
	bool lock_cleared;
	clconf_cluster_t *cl;

	cl = clconf_cluster_get_current();

	if (lock_name == NULL)
		return (-2);

	if (timeout < -2)
		return (-2);

	if (retry_interval < 1)
		return (-2);

	// setup default timeout if specified
	if (timeout == -2)
		timeout = clconf_cluster_get_node_fenceoff_timeout(cl) / 1000;

	// setup infinite loop for synchronous call
	// disable retries for immediate return call
	if ((timeout == -1) || (timeout == 0))
		rt = timeout;
	else
		rt = timeout / (int)retry_interval;

	// Contact the nameserver
	naming::naming_context_var ctxv = ns::local_nameserver();
	ASSERT(!CORBA::is_nil(ctxv));

	fence_lock = new char [strlen(lock_name) + CL_MAX_LEN];
	if (fence_lock == NULL)
		return (-1);

	for (i = 0; i < NODEID_MAX + 1; i++) {
		tempname = clconf_cluster_get_nodename_by_nodeid(cl,
		    (nodeid_t)i);
		if (tempname == NULL)
			continue;

		(void) sprintf(fence_lock, "%s.%s", lock_name, tempname);
		(void) ctxv->resolve(fence_lock, e);
		ex = e.exception();
		if (ex != NULL) {
			// looking for a not_found exception
			if (naming::not_found::_exnarrow(ex) != NULL) {
				e.clear();
				continue;
			}
			e.clear();
		}

		lock_cleared = false;
		while (rt--) {
			// Fence lock is still held wait
			(void) sleep(retry_interval);
			(void) ctxv->resolve(fence_lock, e);
			ex = e.exception();
			if (ex != NULL) {
				// looking for a not_found exception
				if (naming::not_found::_exnarrow(ex) != NULL) {
					e.clear();
					lock_cleared = true;
					break;
				}
				e.clear();
			}
		}
		if (lock_cleared)
			continue;

		delete [] fence_lock;
		clconf_obj_release((clconf_obj_t *)cl);
		return (1);
	}

	delete [] fence_lock;
	clconf_obj_release((clconf_obj_t *)cl);
	return (0);
}

//
// Used to release a device fencing lock, to indicate that device fencing has
// completed.  The lock is of the form <lock-tag>.<node-id>.  Both parts must be
// specified when releasing the lock, i.e. FENCE_LOCK.phys-node-1
// Input parameters:
//	char *lock	lock name, currently expected to be FENCE_NODE.<node-id>
// Returns the following values:
//	0	the lock was successfully released
//	1	the lock was not being held
//	-1	an error occurred, fencing status unknown
//	-2	an illegal parameter was passed in
//
int
release_fencing_lock(const char *lock)
{
	Environment e;
	CORBA::Exception *ex;

	if (lock == NULL)
		return (-2);

	// Contact the nameserver
	naming::naming_context_var ctxv = ns::local_nameserver();
	ASSERT(!CORBA::is_nil(ctxv));
	ctxv->unbind(lock, e);
	ex = e.exception();
	if (ex != NULL) {
		if (naming::not_found::_exnarrow(ex) != NULL) {
			e.clear();
			return (1);
		}
		e.clear();
		return (-1);
	}
	return (0);
}

//
// Used to grab a device fencing lock, to indicate that device fencing or is in
// progress.  The lock is of the form <lock-tag>.<node-id>.  Both parts must be
// specified when grabbing the lock,  i.e. FENCE_LOCK.phys-node-1
// Input parameters:
//	char *lock	lock name, currently expected to be FENCE_NODE.<node-id>
// Returns the following values:
//	0	the lock was successfully grabbed
//	1	the lock was already held
//	-1	an error occurred, , fencing status unknown
//	-2	an illegal parameter was passed in
//
int
grab_fencing_lock(const char *lock)
{
	Environment e;
	CORBA::Exception *ex;

	if (lock == NULL)
		return (-2);

	// Contact the nameserver
	naming::naming_context_var ctxv = ns::local_nameserver();
	ASSERT(!CORBA::is_nil(ctxv));
	(void) ctxv->bind_new_context(lock, e);
	if ((ex = e.exception()) != NULL) {
		if (naming::already_bound::_exnarrow(ex) != NULL) {
			e.clear();
			return (1);
		} else {
			e.clear();
			return (-1);
		}
	}
	return (0);
}

#if SOL_VERSION >= __s10
//
// This method created global device links and nodes for new SVM disksets
// It calls create_device_nodes on the
// device configurator object to get the job done.
//
#if SOL_VERSION > __s10
dc_error_t
dcs_create_device_nodes(dev_t dev, dc_property_seq_t **properties,
    const char *device_name)
#else
dc_error_t
dcs_create_device_nodes(dev_t dev, dc_property_seq_t **properties)
#endif
{
	Environment e;

	sol::major_t major;
	sol::minor_t minor;
	mdc::service_property_seq_t_var property_seq;

	// Get a reference to the device configurator object.
	mdc::dc_config_var dcobj = get_dcs_reference();

	if (CORBA::is_nil(dcobj)) {
		return (DCS_ERR_NOT_CLUSTER);
	}

	major = getemajor(dev);
	minor = geteminor(dev);
	// Invoke the corresponding method on the DCS primary.
#if SOL_VERSION > __s10
	dcobj->create_device_nodes(major, minor, property_seq, device_name, e);
#else
	dcobj->create_device_nodes(major, minor, property_seq, e);
#endif

	dc_error_t error = get_error_and_clear(e);
	if (error != DCS_SUCCESS) {
		return (error);
	}

	*properties = conv(property_seq);
	return (DCS_SUCCESS);
}
#endif /* SOL_VERSION >= __s10 */
