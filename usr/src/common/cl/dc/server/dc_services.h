/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _DC_SERVICES_H
#define	_DC_SERVICES_H

#pragma ident	"@(#)dc_services.h	1.15	08/05/20 SMI"

#include <h/dc.h>
#include <dc/sys/dc_num_alloc.h>
#include <dc/server/dc_service.h>
#include <h/ccr.h>

#define	DC_SERVICE_KEYS_FILE "service_keys"

//
// Service classes are stored in the CCR in the form:
//    CCR key				CCR value
// <service_class_name>		<user_program_name:failover_type>
//
// where 'service_class_name' is the name of the service class (SUNWmd |
// DISK | TAPE), user_program_name is the name of the user level
// program that needs to be called during transitions, and 'failover_type' is
// either 'transparent' or 'eio'.
//

//
// There is one 'dc_services' object associated with one 'dc_config' object.
// The 'dc_services' object handles all the 'dc_service' objects - their
// initialization from persistent storage, the creation of new objects, and
// their deletion.
// It also serves as an allocator for device service ids.
//
class dc_services : public dc_num_alloc {

public:
	// Initialize device services.
	static void init();

	static void destroy();

	// Get the dc_services object
	static dc_services *get_services();

	dc_services();

	~dc_services();

	// Called when the dc_config object using this instance of dc_services
	// becomes a secondary.  Get rid of all state in this call.
	void cleanup();

	// Create a new service
	dc_error_t create_device_service(unsigned int service_id,
	    const char *service_nm,
	    dc_service_class *service_class,
	    bool failback_enabled,
	    const mdc::node_preference_seq_t &nodes,
	    unsigned int num_secs,
	    dc_minor_range_list *gdev_ranges_list,
	    const mdc::service_property_seq_t &properties);

	// Erase the service
	dc_error_t erase_service(dc_service *service);

	dc_error_t create_service_class(const char *service_class,
	    const char *user_program, mdc::ha_dev_type type);

	dc_error_t set_service_class_user_program(const char *service_class,
	    const char *user_program);

	dc_error_t set_service_class_ha_type(const char *service_class,
	    mdc::ha_dev_type type);

	dc_error_t get_service_class_parameters(const char *service_class,
	    CORBA::String_out user_program, mdc::ha_dev_type &type);

	dc_error_t remove_service_class(const char *service_class);

	// Get the names of all the service classes.
	void get_service_class_names(sol::string_seq_t_out &services);

	// Get the names of all the services belonging to a particular class.
	dc_error_t get_service_names_of_class(const char *service_name,
	    sol::string_seq_t_out &services);

	// Get the names of all the services
	void get_service_names(sol::string_seq_t_out &services);

	// Get service without acquiring any locks
	dc_service *resolve_service_nolocks(const char *service_nm);

	// Get the service with this id
	dc_service *resolve_service_from_id(unsigned int service_id,
	    int return_locked_service);

	// Get the service with this name
	dc_service *resolve_service(const char *service_nm,
	    int return_locked_service);

	// Allocate a unique device service id
	unsigned int allocate_service_id();

	// Free a device service id in memory.
	void free_service_id_in_memory(unsigned int num);

	// Free a device service id in memory and persistent store.
	dc_error_t free_service_id(unsigned int num);

	// Get all the active services that can run on node 'id'
	void get_active_services(dc_service_list &services, sol::nodeid_t id);

	// Get the 'dc_service_class' object specified.
	dc_service_class *find_service_class(const char *name);

	// Accessor functions for service_lock
	void wrlock();
	void rdlock();
	void unlock();

	enum {
		UNLOCKED = 1,
		RDLOCKED = 2,
		WRLOCKED = 3
	};

#ifdef DEBUG
	int lock_held();
#endif

private:

	//
	// Define the dc_services object.
	//
	static dc_services *the_services;

	static bool first_access;

	static const char *str_transparent;
	static const char *str_eio;
	static const char *service_keys_table_name;
	static const char *service_classes_table_name;

	static dc_service_class *alloc_service_class(const char *name,
	    const char *takeover_pgm, mdc::ha_dev_type type);

	static void free_service_class(dc_service_class *service_class);

	//
	// Helper function that both adds and removes service ids from the CCR
	// table.
	//
	dc_error_t update_service_id(unsigned int service_id, bool allocate);

	// Cached list of service classes
	dc_service_class_list service_class_list;

	// Cached list of services
	dc_service_list service_list;

	// This lock protects the service_list
	os::rwlock_t service_lock;

	// Read in all the configured service ids and names from persistent
	// storage into memory
	dc_error_t init_from_persistent();

	// Read in service classes from the CCR.
	dc_error_t init_service_classes();

	// Read in services from the CCR.
	dc_error_t init_services();

	typedef enum {
		add_element,
		remove_element,
		update_element
	} write_type_t;

	int conv(const char *string, mdc::ha_dev_type &type);
	void conv(mdc::ha_dev_type type, const char *&string);

	dc_error_t write_service_class(dc_service_class *service_class,
	    write_type_t type);
};

#include <dc/server/dc_services_in.h>

#endif	/* _DC_SERVICES_H */
