//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)dc_repl_server.cc	1.48	08/05/20 SMI"

#include <sys/os.h>
#include <dc/server/dc_mapper_impl.h>
#include <dc/server/dc_repl_server.h>
#include <dc/server/dc_config_impl.h>
#include <dc/sys/dc_lib.h>
#include <dc/server/dc_trans_states.h>

#include <dc/sys/dc_debug.h>

//
// The struct has an entry for each IDL interface which is being versioned. For
// a given VP major and minor version, we get the IDL version of those
// interfaces.
//
dc_ver_map_t	dc_vp_to_idl[DC_VP_MAX_MAJOR + 1][DC_VP_MAX_MINOR + 1] =
{
	{   { 0, 0, 0 },		// VP Version 0.0 defined for indexing
	    { 0, 0, 0 },		// VP Version 0.1   "      "     "
	    { 0, 0, 0 }  },		// VP Version 0.2   "      "     "
	{   { 0, 0, 0 },		// VP Version 1.0
	    { 1, 0, 0 },		// VP Version 1.1 Object Consolidation
	    { 1, 0, 0 }  }		// VP Version 1.2 Full RU Support
};

dc_repl_server *the_dc_repl_server = NULL;

dc_repl_server::dc_repl_server(const char *s_id, const char *p_id) :
	repl_server<repl_dc::repl_dc_ckpt>(s_id, p_id),
	_ckpt_proxy(nil)
{
	current_version.major_num = 0;
	current_version.minor_num = 0;
}

dc_repl_server::~dc_repl_server()
{
	delete dc_config_obj;
	dc_config_obj = NULL;
	_ckpt_proxy = nil;
}

//
// This is called from dc_startup() to create a replica for the DCS service.
//
void
dc_repl_server::initialize()
{
	Environment e;

	DCS_DBPRINTF_W(DCS_TRACE_DC_REPL_SERVER,
	    ("dc_repl_server::initialize().\n"));

	nodeid_t n = orb_conf::node_number();
	char tmp[dclib::NODEID_STRLEN];
	(void) sprintf(tmp, "%d", n);

	// Create a provider.
	the_dc_repl_server = new dc_repl_server("DCS", tmp);

	//
	// Register for Upgrade Callbacks.
	//

	// Get a pointer to the local version manager.
	version_manager::vm_admin_var vmgr_v = vm_util::get_vm(NODEID_UNKNOWN);
	if (CORBA::is_nil(vmgr_v)) {
		CL_PANIC(!"cl_dcs: Could not get vm_admin object");
	}

	char *svc_desc = "cl_dcs";
	char *vpname = "cl_dcs";

	// Build a UCC for support of version upgrade callbacks
	version_manager::ucc_seq_t ucc_seq(1, 1);
	ucc_seq[0].ucc_name = os::strdup(vpname);
	ucc_seq[0].vp_name = os::strdup(svc_desc);

	// Create a version upgrade callback object.
	cl_dcs_upgrade_callback *cbp =
	    new cl_dcs_upgrade_callback(*the_dc_repl_server);
	version_manager::upgrade_callback_var cb_v = cbp->get_objref();
	version_manager::vp_version_t	callback_limit;
	version_manager::vp_version_t	cur_version;

	//
	// Register the callback object with the Version Manager. The
	// tmp_version will be returned.  The version lock is not held
	// since the replica is not yet registered with the HA framework
	// so there can not be a call to become_primary. The current version
	// is returned regardless.
	//
	//
	// If the running version is less than the callback_limit
	// a callback will be registerd, otherwise a callback is not
	// registered (currently no way to tell).  The current running
	// version is returned regardless.
	//
	callback_limit.major_num = DC_VP_MAX_MAJOR;
	callback_limit.minor_num = DC_VP_MAX_MINOR;
	vmgr_v->register_upgrade_callbacks(ucc_seq, cb_v,
	    callback_limit, cur_version, e);
	if (e.exception()) {
		e.exception()->print_exception("cl_dcs: "
		    "Failed register_upgrade_callbacks()");
		e.clear();
		DCS_DBPRINTF_W(DCS_TRACE_DC_REPL_SERVER,
		    ("dc_repl_server::initialize: "
		    "Failed to register_upgrade_callbacks()\n"));
		return;
	}

	// Establish the current version in the provider
	the_dc_repl_server->set_init_version(cur_version);

	the_dc_repl_server->register_with_rm(1, true, e);
	if (e.exception()) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_REPL_SERVER,
		    ("initialize(dc_repl_server=%p): exception '%s' while "
		    "calling register_with_rm().\n", the_dc_repl_server,
		    e.exception()->_name()));

		cmn_err(CE_WARN,
		    "dc_repl_server: Exception calling register_with_rm()");
		e.clear();
	}

}

void
dc_repl_server::become_secondary(Environment &)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_REPL_SERVER,
	    ("dc_repl_server::become_secondary().\n"));

	dc_config_obj->convert_to_secondary();

	// Release our reference to the multi_ckpt_handler.
	CORBA::release(_ckpt_proxy);
	_ckpt_proxy = nil;
}

void
dc_repl_server::add_secondary(replica::checkpoint_ptr ckpt_obj,
    const char *, Environment &env)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_REPL_SERVER,
	    ("dc_repl_server::add_secondary(%p).\n", ckpt_obj));

	repl_dc::repl_dc_ckpt_var ckpt = repl_dc::repl_dc_ckpt::_narrow(
	    ckpt_obj);

	dc_config_obj->dump_state(ckpt, env);
	if (env.exception()) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_REPL_SERVER,
		    ("add_secondary(%p): exception '%s' while calling "
		    "dump_state().\n", this, env.exception()->_name()));
		env.clear();
	}
}

//
// The `become_primary' method may be called on a null secondary to start the
// service, or on an active secondary to perform a failover/switchover
//
void
dc_repl_server::become_primary(const replica::repl_name_seq &,
    Environment &env)
{
	CORBA::type_info_t *typ = NULL;

	version_lock.rdlock();

	DCS_DBPRINTF_W(DCS_TRACE_DC_REPL_SERVER,
	    ("dc_repl_server::become_primary(), current version is %d.%d.\n",
	    current_version.major_num, current_version.minor_num));

	// First, initialize the checkpoint proxy.
	typ = repl_dc::repl_dc_ckpt::_get_type_info(dc_vp_to_idl
	    [current_version.major_num][current_version.minor_num].dc_ckpt);
	ASSERT(typ != NULL);
	replica::checkpoint_var tmp_ckpt_v = set_checkpoint(typ);
	_ckpt_proxy = repl_dc::repl_dc_ckpt::_narrow(tmp_ckpt_v);

	ASSERT(!CORBA::is_nil(_ckpt_proxy));

	mdc::dc_config_var		dc_conf_v;
	naming::naming_context_var	ctx_v;
	Environment			rebind_env;
	typ = mdc::dc_config::_get_type_info(dc_vp_to_idl
	    [current_version.major_num][current_version.minor_num].dc_config);
	ASSERT(typ != NULL);

	if (dc_config_obj == NULL) {
		//
		// We need to create the dc_config object and
		// bind it with the name server.
		//
		dc_config_obj = new dc_config_impl(this);

		dc_conf_v = dc_config_obj->get_objref(typ);
		dc_config_obj->get_checkpoint()->
		    ckpt_dump_state(dc_conf_v, env);
	} else {
		dc_conf_v = dc_config_obj->get_objref(typ);
	}
	//
	// We will update the binding every time become_primary() is called.
	// This is necessary because it's possible that the version has been
	// upgraded while the DCS failover is taking place.
	//
	ctx_v = ns::root_nameserver();
	ASSERT(!CORBA::is_nil(ctx_v));

	ctx_v->rebind(dclib::dcs_name, dc_conf_v, rebind_env);
	version_lock.unlock();

	if (rebind_env.exception()) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_REPL_SERVER,
		    ("become_primary(%p): exception '%s' while "
		    "calling rebind().\n", this,
		    rebind_env.exception()->_name()));

		rebind_env.clear();
		env.exception(new replica::repl_prov_failed());

		// When returning exception, release the checkpoint.
		CORBA::release(_ckpt_proxy);
		_ckpt_proxy = nil;

		return;
	}

	dc_config_obj->convert_to_primary();
}

CORBA::Object_ptr
dc_repl_server::get_root_obj(Environment &)
{
	CORBA::type_info_t *typ = NULL;
	ASSERT(dc_config_obj != NULL);
	version_lock.rdlock();
	typ = mdc::dc_config::_get_type_info(dc_vp_to_idl
	    [current_version.major_num][current_version.minor_num].dc_config);
	version_lock.unlock();
	return (dc_config_obj->get_objref(typ));
}

void
dc_repl_server::remove_secondary(const char *, Environment &)
{
}

void
dc_repl_server::freeze_primary_prepare(Environment &)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_REPL_SERVER,
	    ("dc_repl_server::freeze_primary_prepare().\n"));

	dc_config_obj->prepare_for_freeze();
}

void
dc_repl_server::freeze_primary(Environment &)
{
}

void
dc_repl_server::unfreeze_primary(Environment &)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_REPL_SERVER,
	    ("dc_repl_server::unfreeze_primary().\n"));

	dc_config_obj->reset_freeze_prepare_request();
}

void
dc_repl_server::become_spare(Environment &)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_REPL_SERVER,
	    ("dc_repl_server::become_spare().\n"));

	delete dc_config_obj;
	dc_config_obj = NULL;
}

void
dc_repl_server::shutdown(Environment &env)
{
	DCS_DBPRINTF_A(DCS_TRACE_DC_REPL_SERVER,
	    ("dc_repl_server::shutdown(%p): device busy.\n", this));

	env.exception(new replica::service_busy);

	//
	// Since we a returning exception, we will remain primary,
	// so don't release _ckpt_proxy.
	//
}

//
// Set the initial version number.
//
void
dc_repl_server::set_init_version(const version_manager::vp_version_t &version)
{
	//
	// We could have had a callback between the time that we called
	// register_upgrade_callbacks() and the time that it returned and
	// set tmp_version. The callback may have set a newer version than
	// current version, so don't clobber it.
	//
	version_lock.wrlock();
	if (current_version.major_num < version.major_num ||
	    (current_version.major_num == version.major_num &&
	    current_version.minor_num < version.minor_num)) {
		current_version = version;
	}
	version_lock.unlock();
}

void
dc_repl_server::upgrade_callback(
	const version_manager::vp_version_t &version, Environment &e)
{
	CORBA::type_info_t		*typ;
	mdc::dc_config_var		dc_conf_v;
	naming::naming_context_var	ctx_v;
	Environment			rebind_env;

	//
	// Note that upgrade callbacks are not synchronized with
	// calls to become_primary(), add_secondary(), etc.
	// Getting this lock makes sure the replica state doesn't change.
	//
	version_lock.wrlock();
	if (current_version.major_num > version.major_num ||
	    (current_version.major_num == version.major_num &&
	    current_version.minor_num >= version.minor_num)) {
		// Version isn't changing so just return.
		version_lock.unlock();
		return;
	}

	current_version = version;
	DCS_DBPRINTF_R(DCS_TRACE_DC_REPL_SERVER,
	    ("upgrade_callback(%p): major = %d minor = %d ", this,
	    current_version.major_num, current_version.minor_num));

	if (CORBA::is_nil(_ckpt_proxy)) {
		// If this replica is a secondary, set the current_version.
		current_version = version;
		version_lock.unlock();
		return;
	}

	//
	// Switch the checkpoint interface to the new protocol. Save
	// the current _ckpt_proxy and release it after we get a new one
	//
	repl_dc::repl_dc_ckpt_ptr old_ckpt_p = _ckpt_proxy;

	typ = repl_dc::repl_dc_ckpt::_get_type_info(
	    dc_vp_to_idl[current_version.major_num][current_version.minor_num].
	    dc_ckpt);

	replica::checkpoint_var tmp_ckpt_v = set_checkpoint(typ);
	_ckpt_proxy = repl_dc::repl_dc_ckpt::_narrow(tmp_ckpt_v);
	ASSERT(!CORBA::is_nil(_ckpt_proxy));
	CORBA::release(old_ckpt_p);

	// Update the dc_config reference in the name server.
	typ = mdc::dc_config::_get_type_info(
	    dc_vp_to_idl[current_version.major_num][current_version.minor_num].
	    dc_config);
	ASSERT(typ != NULL);
	dc_conf_v = dc_config_obj->get_objref(typ);

	ctx_v = ns::root_nameserver();
	ASSERT(!CORBA::is_nil(ctx_v));

	ctx_v->rebind(dclib::dcs_name, dc_conf_v, rebind_env);
	version_lock.unlock();

	if (rebind_env.exception()) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_REPL_SERVER,
		    ("upgrade_callback(%p): exception '%s' while "
		    "calling rebind().\n", this,
		    rebind_env.exception()->_name()));

		rebind_env.clear();
	}

	// Upgrade the device server references held in the mapper list
	dc_config_obj->upgrade_device_references();
}

// dc_repl_server(repl_dc::repl_dc_ckpt::ckpt_secondary)
void
dc_repl_server::ckpt_dump_state(mdc::dc_config_ptr primary_obj, Environment &)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_REPL_SERVER,
	    ("dc_repl_server::ckpt_dump_state(%p).\n", primary_obj));

	if (dc_config_obj == NULL) {
		dc_config_obj = new dc_config_impl(primary_obj, this);
	}
}

// dc_repl_server(repl_dc::repl_dc_ckpt::ckpt_add_dc_mapper)
void
dc_repl_server::ckpt_add_dc_mapper(mdc::dc_mapper_ptr primary_obj,
    Environment &)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_REPL_SERVER,
	    ("dc_repl_server::ckpt_add_dc_mapper(%p).\n", primary_obj));

	dc_config_obj->ckpt_add_dc_mapper(primary_obj);
}

// dc_repl_server(repl_dc::repl_dc_ckpt::ckpt_set_dc_mapper)
void
dc_repl_server::ckpt_set_dc_mapper(mdc::dc_mapper_ptr primary_obj,
    uint32_t id, mdc::device_server_ptr dev_server, Environment &)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_REPL_SERVER,
	    ("dc_repl_server::ckpt_set_dc_mapper(%p, %d, %p).\n",
	    primary_obj, id, dev_server));

	dc_config_obj->ckpt_set_dc_mapper(primary_obj, id, dev_server);
}

// dc_repl_server(repl_dc::repl_dc_ckpt::ckpt_device_server)
void
dc_repl_server::ckpt_device_server(mdc::dc_mapper_ptr primary_obj,
    mdc::device_server_ptr dev_server, Environment &)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_REPL_SERVER,
	    ("dc_repl_server::ckpt_device_server(%p, %p).\n",
	    primary_obj, dev_server));

	dc_config_obj->ckpt_device_server(primary_obj, dev_server);
}

// dc_repl_server(repl_dc::repl_dc_ckpt::ckpt_remove_dc_mapper)
void
dc_repl_server::ckpt_remove_dc_mapper(mdc::dc_mapper_ptr mapper_obj,
    Environment &)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_REPL_SERVER,
	    ("dc_repl_server::ckpt_remove_dc_mapper(%p).\n", mapper_obj));

	dc_config_obj->ckpt_remove_dc_mapper(mapper_obj);
}

// dc_repl_server(repl_dc::repl_dc_ckpt::ckpt_register_dsm)
void
dc_repl_server::ckpt_register_dsm(mdc::device_service_manager_ptr dsm,
    mdc::device_server_ptr local_device_server, sol::nodeid_t node_id,
    Environment &)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_REPL_SERVER,
	    ("dc_repl_server::ckpt_register_dsm(%p, %p, %d).\n",
	    dsm, local_device_server, node_id));

	dc_config_obj->ckpt_register_dsm(dsm, local_device_server, node_id);
}

// dc_repl_server(repl_dc::repl_dc_ckpt::ckpt_unregister_dsm)
void
dc_repl_server::ckpt_unregister_dsm(mdc::device_service_manager_ptr dsm,
    Environment &)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_REPL_SERVER,
	    ("dc_repl_server::ckpt_unregister_dsm(%p).\n", dsm));

	dc_config_obj->ckpt_unregister_dsm(dsm);
}

// dc_repl_server(repl_dc::repl_dc_ckpt::ckpt_add_callback)
void
dc_repl_server::ckpt_add_callback(sol::major_t maj, sol::minor_t _min,
    fs::dc_callback_ptr cb_obj, mdc::dc_mapper_ptr mapper_obj,
    Environment &)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_REPL_SERVER,
	    ("dc_repl_server::ckpt_add_callback(%d, %d, %p, %p).\n",
	    maj, _min, cb_obj, mapper_obj));

	dc_config_obj->ckpt_add_callback(maj, _min, cb_obj, mapper_obj);
}

// dc_repl_server(repl_dc::repl_dc_ckpt::ckpt_remove_callback)
void
dc_repl_server::ckpt_remove_callback(sol::major_t maj, sol::minor_t _min,
    fs::dc_callback_ptr cb_obj, Environment &)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_REPL_SERVER,
	    ("dc_repl_server::ckpt_remove_callback(%d, %d, %p).\n",
	    maj, _min, cb_obj));

	dc_config_obj->ckpt_remove_callback(maj, _min, cb_obj);
}

// dc_repl_server(repl_dc::repl_dc_ckpt::ckpt_get_resv_lock)
void
dc_repl_server::ckpt_get_resv_lock(sol::nodeid_t id,
    mdc::reservation_client *resv_obj, sol::incarnation_num incarnation,
    Environment &)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_REPL_SERVER,
	    ("dc_repl_server::ckpt_get_resv_lock(%d, %p, %d).\n",
	    id, resv_obj, incarnation));

	dc_config_obj->ckpt_get_resv_lock(id, resv_obj, incarnation);
}

// dc_repl_server(repl_dc::repl_dc_ckpt::ckpt_release_resv_lock)
void
dc_repl_server::ckpt_release_resv_lock(mdc::reservation_client *resv_obj,
    Environment &)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_REPL_SERVER,
	    ("dc_repl_server::ckpt_release_resv_lock(%p).\n", resv_obj));

	dc_config_obj->ckpt_release_resv_lock(resv_obj);
}

// Checkpoint accessor function.
repl_dc::repl_dc_ckpt_ptr
dc_repl_server::get_checkpoint_repl_dc()
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_REPL_SERVER,
	    ("dc_repl_server::get_checkpoint_repl_dc().\n"));

	return (_ckpt_proxy);
}

cl_dcs_upgrade_callback::cl_dcs_upgrade_callback(dc_repl_server &dc_replica) :
    prov(dc_replica)
{
}

cl_dcs_upgrade_callback::~cl_dcs_upgrade_callback()
{
}

void
#ifdef DEBUG
cl_dcs_upgrade_callback::_unreferenced(unref_t cookie)
#else
cl_dcs_upgrade_callback::_unreferenced(unref_t)
#endif
{
	// This object does not support multiple 0->1 transitions.
	ASSERT(_last_unref(cookie));
	delete this;
}

void
cl_dcs_upgrade_callback::do_callback(const char *,
    const version_manager::vp_version_t &new_version, Environment &e)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_REPL_SERVER,
	    ("cl_dcs_upgrade_callback::do_callback().\n"));

	// Call the provider to update the version.
	prov.upgrade_callback(new_version, e);
}
