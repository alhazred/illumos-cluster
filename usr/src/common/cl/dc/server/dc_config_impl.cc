/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)dc_config_impl.cc	1.135	08/09/17 SMI"

#include <sys/os.h>
#include <sys/cl_events.h>
#include <sys/mc_probe.h>
#include <dc/server/dc_config_impl.h>
#include <dc/server/dc_mapper_impl.h>
#include <dc/server/dc_storage.h>
#include <sys/rm_util.h>

#include <dc/server/dc_repl_server.h>
#include <dc/server/dc_trans_states.h>
#include <dc/sys/dc_lib.h>
#include <dc/libdcs/libdcs.h>
#include <cmm/cmm_ns.h>

#include <dc/sys/dc_debug.h>
#include <sys/sol_version.h>

//
// The maximum number of metadevices in a set is currently limited
// to 8192. Must match define in {on}/usr/src/head/sdssc.h
//
#define	SDSSC_METADDEV_MAX	8192

// Global device configuration object
dc_config_impl *dc_config_obj = NULL;

//
// Primary constructor.
//
dc_config_impl::dc_config_impl(dc_repl_server *serverp) :
	mc_replica_of<mdc::dc_config>(serverp)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::dc_config_impl(%p): primary.\n", serverp));

	primary = true;
	dc_repl_serverp = serverp;
	switchovers_in_progress = 0;
	freeze_prepare_request = false;
}

//
// Secondary constructor.
//
dc_config_impl::dc_config_impl(mdc::dc_config_ptr obj,
    dc_repl_server *serverp) :
	mc_replica_of<mdc::dc_config>(obj)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::dc_config_impl(%p, %p): secondary.\n",
	    obj, serverp));

	primary = false;
	dc_repl_serverp = serverp;
	switchovers_in_progress = 0;
	freeze_prepare_request = false;
}

dc_config_impl::~dc_config_impl()
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::~dc_config_impl().\n"));

	mapper_list.dispose();
	dsm_list.dispose();
	callbacks.dispose();
	resv_locks.dispose();
	dc_repl_serverp = NULL;
}

//
// Object is bound to global name server. Unreferenced should never happen.
//
void
dc_config_impl::_unreferenced(unref_t)
{
	DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::_unreferenced(%p): unreferenced called!\n",
	    this));

	os::panic("unreferenced called for dc_config object\n");
}

//
// Called from dc_repl_server::become_primary().
//
void
dc_config_impl::convert_to_primary()
{
	dc_mapper_impl *dc_mapperp;
	unsigned int devid;

	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::convert_to_primary().\n"));

	primary = true;

	FAULTPT_DCS(FAULTNUM_DCS_CONVERT_PRIMARY_S_B,
		FaultFunctions::generic);

	//
	// Initialize the primary objects from persistent storage.
	//
	dc_storage::init();
	dc_services::init();

	dc_storage *dcstore = dc_storage::storage();
	dc_services *services = dc_services::get_services();
	dc_service *_service;
	services->rdlock();

	//
	// Mark all active services as active.
	//
	DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
	    ("convert_to_primary(%p): marking active services as active.\n",
	    this));
	for (mapper_list.atfirst();
	    (dc_mapperp = mapper_list.get_current()) != NULL;
	    mapper_list.advance()) {
		devid = dc_mapperp->get_devid();
		_service = services->resolve_service_from_id(devid,
		    dc_services::WRLOCKED);
		//
		// 'service' could be NULL if there is an error in the CCR
		// data.
		//
		if (_service != NULL) {
			_service->set_active(true);
			_service->unlock();
		}
	}

	FAULTPT_DCS(FAULTNUM_DCS_CONVERT_PRIMARY_S_A,
		FaultFunctions::generic);

	//
	// Insert all callback objects into the appropriate services.
	//
	DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
	    ("convert_to_primary(%p): inserting callback objects into "
	    "appropriate services.\n", this));
	dc_callback_info *tmp;
	for (callbacks.atfirst(); (tmp = callbacks.get_current()) != NULL;
	    callbacks.advance()) {
		bool found = dcstore->get_map(tmp->gmaj, tmp->gmin, &devid);
		if (!found) {
			//
			// This will happen only if there is an error in the
			// CCR data.
			//
			DCS_DBPRINTF_A(DCS_TRACE_DC_CONFIG,
			    ("convert_to_primary(%p): unable to find owner "
			    "for callback object %p (%d, %d)\n", this,
			    tmp, tmp->gmaj, tmp->gmin));
			os::warning("Unable to find owner for callback object "
			    "%p (%d, %d)\n", tmp, tmp->gmaj, tmp->gmin);
			continue;
		}
		_service = services->resolve_service_from_id(devid,
		    dc_services::UNLOCKED);
		ASSERT(_service);

		_service->add_callback_nocheck(tmp);
	}

	services->unlock();

	FAULTPT_DCS(FAULTNUM_DCS_CONVERT_PRIMARY_S_E,
		FaultFunctions::generic);

}

//
// Called from dc_repl_server::become_secondary().
//
void
dc_config_impl::convert_to_secondary()
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::convert_to_secondary().\n"));

	FAULTPT_DCS(FAULTNUM_DCS_CONVERT_SECONDARY_S_B,
		FaultFunctions::generic);

	//
	// Cleanup the primary objects.
	//
	dc_services::destroy();
	dc_storage::destroy();

	FAULTPT_DCS(FAULTNUM_DCS_CONVERT_SECONDARY_S_A,
		FaultFunctions::generic);

	ASSERT(primary);
	primary = false;

	FAULTPT_DCS(FAULTNUM_DCS_CONVERT_SECONDARY_S_E,
		FaultFunctions::generic);

}

//
// Return true if we are the primary.
//
bool
dc_config_impl::is_primary()
{
	return (primary);
}

//
// This call is made on the primary dc_config object to dump the current
// state to a newly joining secondary. We must do this with the correct
// idl protocol to support rolling upgrade.
//
// Note that there is currently no state checkpoint difference for
// the different versions. Therefore no race conditions matter here.
// if this changes we will need to add locking arround the current
// running version as commit callbacks can happen at any time.
// The only problem is if on non-upgraded node exists in the cluster
// it will panic if we send a checkpoint for the new dcs due to a tids
// mismatch.
//
void
dc_config_impl::dump_state(repl_dc::repl_dc_ckpt_ptr secondary_ckpt,
    Environment &env)
{
	dc_mapper_impl *dc_mapperp;
	dc_config_impl::dsm_info_t *dsmp;

	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::dump_state(%p).\n", secondary_ckpt));

	FAULTPT_DCS(FAULTNUM_DCS_NEWLY_JOINING_SECONDARY_S_B,
		FaultFunctions::generic);

	//
	// Get a reference to the current running version of dc_config_impl
	// object.
	//
	Environment	e;
	mdc::dc_config_var dc_config_ref =
	    (mdc::dc_config_ptr) dc_repl_serverp->get_root_obj(e);
	secondary_ckpt->ckpt_dump_state(dc_config_ref, env);
	if (env.exception()) {
		//
		// Exception while checkpointing a new secondary - there is
		// no need to continue with the checkpoints.
		//
		DCS_DBPRINTF_A(DCS_TRACE_DC_CONFIG,
		    ("dump_state(%p): exception '%s' while calling "
		    "ckpt_dump_state().\n", this,
		    env.exception()->_name()));
		return;
	}

	//
	// Locking is needed here because of the last_unref() check done in
	// the _unreferenced() implementations of dc_server and dc_mapper
	//
	list_lock.wrlock();

	// sync. secondary with all the dc_mapper's on the primary.
	for (mapper_list.atfirst();
	    (dc_mapperp = mapper_list.get_current()) != NULL;
	    mapper_list.advance()) {
		dc_mapperp->dump_state(secondary_ckpt, env);
		if (env.exception()) {
			list_lock.unlock();
			DCS_DBPRINTF_A(DCS_TRACE_DC_CONFIG,
			    ("dump_state(%p): exception '%s' while calling "
			    "dump_state().\n", this,
			    env.exception()->_name()));
			return;
		}
	}

	FAULTPT_DCS(FAULTNUM_DCS_NEWLY_JOINING_SECONDARY_S_O_LOCKED,
		FaultFunctions::generic);

	list_lock.unlock();

	// sync. secondary with all the dsm's registered with the primary.
	for (dsm_list.atfirst(); (dsmp = dsm_list.get_current()) != NULL;
	    dsm_list.advance()) {
		secondary_ckpt->ckpt_register_dsm(dsmp->dsm,
		    dsmp->local_device_server, dsmp->node_id, env);
		if (env.exception()) {
			//
			// Exception while checkpointing a new secondary - there
			// is no need to continue with the checkpoints.
			//
			DCS_DBPRINTF_A(DCS_TRACE_DC_CONFIG,
			    ("dump_state(%p): exception '%s' while calling "
			    "ckpt_register_dsm().\n", this,
			    env.exception()->_name()));
			return;
		}
	}

	FAULTPT_DCS(FAULTNUM_DCS_NEWLY_JOINING_SECONDARY_S_A,
		FaultFunctions::generic);

	// Checkpoint over all the 'dc_callback' objects.
	dc_callback_info *tmp;
	for (callbacks.atfirst(); (tmp = callbacks.get_current()) != NULL;
	    callbacks.advance()) {
		secondary_ckpt->ckpt_add_callback(tmp->gmaj,
		    tmp->gmin, tmp->cb_var, tmp->mapper_var, env);
		if (env.exception()) {
			//
			// Exception while checkpointing a new secondary -
			// there is no need to continue with the checkpoints.
			//
			DCS_DBPRINTF_A(DCS_TRACE_DC_CONFIG,
			    ("dump_state(%p): exception '%s' while calling "
			    "ckpt_add_callback().\n", this,
			    env.exception()->_name()));
			return;
		}
	}

	FAULTPT_DCS(FAULTNUM_DCS_NEWLY_JOINING_SECONDARY_S_E,
		FaultFunctions::generic);

	reservation_lock *tmp2;
	for (resv_locks.atfirst(); (tmp2 = resv_locks.get_current()) != NULL;
	    resv_locks.advance()) {
		get_checkpoint()->ckpt_get_resv_lock(tmp2->node,
		    tmp2->resv_obj, tmp2->incarnation, env);
		if (env.exception()) {
			//
			// Exception while checkpointing a new secondary -
			// there is no need to continue with the checkpoints.
			//
			DCS_DBPRINTF_A(DCS_TRACE_DC_CONFIG,
			    ("dump_state(%p): exception '%s' while calling "
			    "ckpt_get_resv_lock().\n", this,
			    env.exception()->_name()));
			return;
		}
	}
}

//
// 'get_device_server' is called to find the device server that owns the
// device.
// If the device being accessed is part of a HA service and the HA service is
// inactive, we attempt to start it up.
//
// dc_config_impl(mdc::dc_config::get_device_server)
void
dc_config_impl::cascaded_get_device_server(sol::major_t maj, sol::minor_t _min,
    sol::nodeid_t id, mdc::device_server_out dev_server, bool &is_ha,
    Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::cascaded_get_device_server"
	    "(%d, %d, %d, %p, %d).\n", maj, _min, id, dev_server, is_ha));
	// This device is part of a HA device service.
	is_ha = true;

	if (_freeze_in_progress(&_environment)) {
		DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
		    ("cascaded_get_device_server(%p): freeze in progress.\n",
		    this));
		return;
	}

	unsigned int device_id;
	dc_service *_service;
	bool found;

	dc_storage *dcstore = dc_storage::storage();
	dc_services *services = dc_services::get_services();

	if (maj >= (major_t)devcnt) {
		dev_server = mdc::device_server::_nil();
		_environment.exception(new sol::op_e(ENXIO));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("cascaded_get_device_server(%p): no such device.\n",
		    this));
		return;
	}

	services->rdlock();

	found = dcstore->get_map(maj, _min, &device_id);

	if (!found) {
		services->unlock();

		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("cascaded_get_device_server(%p): get_map() failed.\n",
		    this));

		dev_server = mdc::device_server::_nil();
		_environment.exception(new sol::op_e(ENXIO));
		return;
	}

	_service = services->resolve_service_from_id(device_id,
	    dc_services::RDLOCKED);
	ASSERT(_service != NULL);
	services->unlock();

	//
	// If the service is local then return back a NULL dev_server
	// and set is_ha to false. Note that no exception is raised.
	//
	if (_service->is_local_class()) {
		is_ha = false;
		dev_server = mdc::device_server::_nil();
		_service->unlock();
		DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
		    ("cascaded_get_device_server(%p): service is local.\n",
		    this));
		return;
	}

	dev_server = find_devobj(device_id);
	if (!CORBA::is_nil(dev_server)) {
		_service->unlock();
		DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
		    ("cascaded_get_device_server(%p): dev_server already"
		    " bound.\n", this));
		return;
	}

	// Try starting the service.
	_service->start_service_lock();

	if (_service->is_active()) {
		//
		// Someone started the service while we waited for the lock.
		//
		dev_server = find_devobj(device_id);
	} else if (_service->is_suspended()) {
		//
		// This service is suspended.  Provide local access so that
		// the device can be accessed in maintainence mode.
		//
		dev_server = get_local_device_server(id);
	} else {
		mdc::dc_mapper_var mapper_obj;
		dev_server = start_service(_service, id, false, false,
		    mapper_obj, _environment);
	}

	_service->start_service_unlock();
	_service->unlock();

	if (CORBA::is_nil(dev_server)) {
		_environment.exception(new sol::op_e(ENXIO));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("cascaded_get_device_server(%p): no device server "
		    "found.\n", this));
	}
}

//
// 'get_configured_nodes' is called by PXFS to find the HA properties of a
// device.
// If the device being accessed is part of a HA service and the HA service is
// inactive, we attempt to start it up.
//
// dc_config_impl(mdc::dc_config::get_configured_nodes)
void
dc_config_impl::cascaded_get_configured_nodes(sol::major_t maj,
    sol::minor_t _min, sol::nodeid_t id, fs::dc_callback_ptr cb_obj,
    bool &is_ha, CORBA::String_out ha_service_name, sol::nodeid_seq_t_out nodes,
    Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::cascaded_get_configured_nodes"
	    "(%d, %d, %d, %p, %d).\n", maj, _min, id, cb_obj, is_ha));

	if (_freeze_in_progress(&_environment)) {
		DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
		    ("cascaded_get_configured_nodes(%p): freeze in "
		    "progress.\n", this));
		return;
	}

	dc_storage *dcstore;
	dc_services *services;
	unsigned int device_id;
	bool found;
	dc_service *_service;
	sol::nodeid_seq_t *nodes_seq;

	if (maj >= (major_t)devcnt) {
		_environment.exception(new sol::op_e(ENXIO));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("cascaded_get_configured_nodes(%p): no such device.\n",
		    this));
		return;
	}

	dcstore = dc_storage::storage();
	services = dc_services::get_services();

	// Disallow removal of services.
	services->rdlock();

	found = dcstore->get_map(maj, _min, &device_id);

	if (!found) {
		services->unlock();

		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("cascaded_get_configured_nodes(%p): get_map() failed.\n",
		    this));

		_environment.exception(new sol::op_e(ENXIO));
		return;
	}

	ASSERT(found);

	_service = services->resolve_service_from_id(device_id,
	    dc_services::RDLOCKED);
	services->unlock();

	ASSERT(_service != NULL);

	nodes_seq = new sol::nodeid_seq_t;

	_service->start_service_lock();
	if (_service->is_suspended()) {
		DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
		    ("cascaded_get_configured_nodes(%p): service is not HA.\n",
		    this));
		// Mark service is non-HA.
		is_ha = false;
		ha_service_name = (char *)NULL;

		// Return the local node id.
		nodes_seq->length(1);
		(*(nodes_seq))[0] = id;
		nodes = nodes_seq;
	} else {
		DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
		    ("cascaded_get_configured_nodes(%p): service is HA.\n",
		    this));
		is_ha = true;

		char *service_name;
		_service->get_service_name(&service_name);
		ha_service_name = dclib::get_serviceid(service_name);
		delete [] service_name;

		_service->get_node_ids(*nodes_seq);
		nodes = nodes_seq;

		mdc::dc_mapper_var mapper_obj;

		if (_service->is_active()) {
			mapper_obj = find_mapper(_service->get_service_id());
			ASSERT(!CORBA::is_nil(mapper_obj));
		} else {
			// Startup the service.
			//lint -e529 dev_server not subsequently referenced
			mdc::device_server_var dev_server =
			    start_service(_service, id, false,
			    (CORBA::is_nil(cb_obj) ? false : true),
			    mapper_obj, _environment);
		}
			//lint +e529
		if (!CORBA::is_nil(cb_obj)) {
			ASSERT(_service->is_active());
			ASSERT(!CORBA::is_nil(mapper_obj));
			dc_callback_info *new_cb = new dc_callback_info(
			    maj, _min, cb_obj, mapper_obj);
			_service->add_callback(new_cb, _environment);
		}
	}
	_service->start_service_unlock();

	_service->unlock();
}

//
// IDL entry-point to startup and switchover the specified device service.
//
void
dc_config_impl::cascaded_switch_device_service(const char *service_name,
    sol::nodeid_t id, Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::cascaded_switch_device_service(%s, %d).\n",
	    service_name, id));

	if (!safe_to_switchover(_environment)) {
		DCS_DBPRINTF_A(DCS_TRACE_DC_CONFIG,
		    ("cascaded_switch_device_service(%p): not safe to "
		    "switchover.\n", this));
		return;
	}

	if (_freeze_in_progress(&_environment)) {
		notify_switchover_end();
		DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
		    ("cascaded_switch_device_service(%p): freeze in "
		    "progress.\n", this));
		return;
	}

	dc_services *services;
	dc_service *_service;

	services = dc_services::get_services();
	services->rdlock();
	_service = services->resolve_service(service_name,
	    dc_services::RDLOCKED);
	services->unlock();

	if (_service == NULL) {
		// Service with this name does not exist
		_environment.exception(new mdc::dcs_error(
		    DCS_ERR_SERVICE_NAME));
		notify_switchover_end();
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("cascaded_switch_device_service(%p): no such service.\n",
		    this));
		return;
	}

	_service->start_service_lock();
	if (_service->is_active()) {
		// Service is already active - switch it over.
		do_switchover(_service, id, false, _environment);
	} else {
		// Startup the service.
		mdc::dc_mapper_var mapper_obj;
		//lint -e529
		mdc::device_server_var dev_server = start_service(_service, id,
		    true, false, mapper_obj, _environment);
	}
	//lint +e529

	_service->start_service_unlock();
	_service->unlock();

	notify_switchover_end();
}

//
// Called on the secondary DCS to add a callback object to its list.
//
void
dc_config_impl::ckpt_add_callback(sol::major_t maj, sol::minor_t _min,
    fs::dc_callback_ptr cb_obj, mdc::dc_mapper_ptr mapper_obj)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::ckpt_add_callback(%d, %d, %p, %p).\n",
	    maj, _min, cb_obj, mapper_obj));

	//
	// See if this is the result of a retry.
	// Note that there is a rule implemented here that a particular
	// callback object may only register interest once for a particular
	// dev_t value.
	//
	dc_callback_info *tmp;
	for (callbacks.atfirst();
	    (tmp = callbacks.get_current()) != NULL;
	    callbacks.advance()) {
		if ((tmp->cb_var->_equiv(cb_obj)) && (tmp->gmaj == maj) &&
		    (tmp->gmin == _min)) {
			// Object already exists.
			DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
			    ("ckpt_add_callback(%p): object already exists.\n",
			    this));
			return;
		}
	}

	tmp = new dc_callback_info(maj, _min, cb_obj, mapper_obj);

	add_callback_internal(tmp);
}

//
// Function to add a callback object to the DCS.  Used by ::ckpt_add_callback,
// and by dc_service::add_callback().
//
void
dc_config_impl::add_callback_internal(dc_callback_info *tmp)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::add_callback_internal(%p).\n", tmp));

	callbacks.append(tmp);
}

//
// IDL call to unregister the callback object that was registered in
// 'get_configured_nodes'.
//
// dc_config_impl(mdc::dc_config::unregister_callback)
void
dc_config_impl::unregister_callback(sol::major_t maj, sol::minor_t _min,
    fs::dc_callback_ptr cb_obj, Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::unregister_callback(%d, %d, %p).\n",
	    maj, _min, cb_obj));

	if (maj >= (major_t)devcnt) {
		_environment.exception(new sol::op_e(ENXIO));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("unregister_callback(%p): no such device.\n", this));
		return;
	}

	dc_storage *dcstore = dc_storage::storage();
	dc_services *services = dc_services::get_services();

	// Disallow removal of services.
	services->rdlock();

	unsigned int device_id;
	bool found = dcstore->get_map(maj, _min, &device_id);

	if (!found) {
		services->unlock();

		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("unregister_callback(%p): get_map() failed.\n", this));

		_environment.exception(new sol::op_e(ENXIO));
		return;
	}

	ASSERT(found);

	dc_service *_service = services->resolve_service_from_id(device_id,
	    dc_services::WRLOCKED);
	services->unlock();

	ASSERT(_service != NULL);

	// Remove the callback object.
	_service->remove_callback(maj, _min, cb_obj, _environment);

	_service->unlock();
}

//
// Called on the secondary DCS to remove a callback object from the list.
//
void
dc_config_impl::ckpt_remove_callback(sol::major_t maj, sol::minor_t _min,
    fs::dc_callback_ptr cb_obj)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::ckpt_remove_callback(%d, %d, %p).\n",
	    maj, _min, cb_obj));
	remove_callback_internal(maj, _min, cb_obj);
}

//
// Function to remove the callback object from the DCS list - used by
// ::ckpt_remove_callback() and dc_service methods that remove a callback
// object.
//
void
dc_config_impl::remove_callback_internal(sol::major_t maj, sol::minor_t _min,
    fs::dc_callback_ptr cb_obj)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::remove_callback_internal(%d, %d, %p).\n",
	    maj, _min, cb_obj));

	dc_callback_info *tmp;
	Environment env;
	for (callbacks.atfirst();
	    (tmp = callbacks.get_current()) != NULL;
	    callbacks.advance()) {
		if ((tmp->cb_var->_equiv(cb_obj)) && (tmp->gmaj == maj) &&
		    (tmp->gmin == _min)) {
			//
			// Found the object - remove it.
			//
			(void) callbacks.erase(tmp);
			delete tmp;
			DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
			    ("remove_callback_internal(%p): callback "
			    "count=%d.\n", this, callbacks.count()));
			return;
		}
	}
}

//
// Create a new mapper object.  This call is because of a checkpoint
// on the secondary. Since ordering is done on the primary, no locking
// is needed.
//
void
dc_config_impl::ckpt_add_dc_mapper(const mdc::dc_mapper_ptr primary_obj)
{
	ASSERT(!CORBA::is_nil(primary_obj));

	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::ckpt_add_dc_mapper(%p).\n", primary_obj));

	mapper_list.prepend(new dc_mapper_impl(primary_obj));
}


//
// Set the devobj and devid of a mapper object and add it to the list.
// This call is because of a checkpoint on the secondary. Since ordering
// is done on the primary, no locking is needed.
//
void
dc_config_impl::ckpt_set_dc_mapper(const mdc::dc_mapper_ptr primary_obj,
    uint32_t id, mdc::device_server_ptr dev_server)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::ckpt_set_dc_mapper(%p, %d, %p).\n",
	    primary_obj, id, dev_server));

	ASSERT(!CORBA::is_nil(primary_obj));

	dc_mapper_impl *mapperp =
	    dc_mapper_impl::get_dc_mapper_impl(primary_obj);

	//
	// 'mapperp' will be NULL if this call comes from
	// dc_mapper::dump_state().
	//
	if (mapperp == NULL) {
		mapperp = new dc_mapper_impl(primary_obj);
		mapper_list.prepend(mapperp);
	}
	DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
	    ("ckpt_set_dc_mapper(%p): mapperp=%p.\n", this, mapperp));
	mapperp->set_devobj(dev_server);
	mapperp->set_devid(id);
}

//
// Constructor for dsm_info_t list element.
//
dc_config_impl::dsm_info_t::dsm_info_t(mdc::device_service_manager_ptr dsm_obj,
    mdc::device_server_ptr _local_device_server, sol::nodeid_t nid) :
	_SList::ListElem(this)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::dsm_info_t::dsm_info_t(%p, %p, %d).\n",
	    dsm_obj, _local_device_server, nid));

	dsm = mdc::device_service_manager::_duplicate(dsm_obj);
	local_device_server =
	    mdc::device_server::_duplicate(_local_device_server);
	node_id = nid;
}

//
// Add a new DSM to the list. Return true if it was newly added.
//
bool
dc_config_impl::add_dsm_to_list_locked(dc_config_impl::dsm_info_t *new_dsm)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::add_dsm_to_list_locked().\n"));

	ASSERT(dsm_list_lock.lock_held());

	dc_config_impl::dsm_info_t *tmp;

	//
	// A node may have gone down and come back up again.
	// Remove any old DSMs with the same node_id.
	//
	for (dsm_list.atfirst();
	    (tmp = dsm_list.get_current()) != NULL;
	    dsm_list.advance()) {

		if (tmp->node_id == new_dsm->node_id) {
			//
			// Found a dsm with the same node_id.
			// Either this is a retry on a new primary or this is
			// the result of a node rebooting.
			//
			if (tmp->dsm->_equiv(new_dsm->dsm)) {
				// This is a retry.
				DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
				    ("add_dsm_to_list_locked(%p): this is a "
				    "retry.\n", this));
				return (false);
			}

			//
			// The node was rebooted; remove the stale dsm.
			//
			DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
			    ("add_dsm_to_list_locked(%p): the node was "
			    "rebooted. removing the stale dsm.\n", this));
			(void) dsm_list.erase(tmp);
			delete tmp;

			//
			// There can be a maximum of one DSM entry with
			// the same node id on the list so there is no
			// need to look further.
			//
			break;
		}
	}

	dsm_list.append(new_dsm);

	return (true);
}

//
// Register a new node's DSM.
// We return a list of device_server replicas that need to be started on the
// node that is registering.
// This is an idempotent operation and does not need transactions.
//
// dc_config_impl(mdc::dc_config::register_dsm)
void
dc_config_impl::register_dsm(mdc::device_service_manager_ptr dsm,
    mdc::device_server_ptr local_device_server, sol::nodeid_t node_id,
    mdc::device_service_info_seq_out service_info_seq,
    Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::register_dsm(%p, %p, %d).\n",
	    dsm, local_device_server, node_id));

	FAULTPT_DCS(FAULTNUM_DCS_REGISTER_DSM_S_B,
		FaultFunctions::generic);

	dc_error_t err_val;

	//
	// Check if this node has the same major numbers <--> major name mapping
	// as us.
	//
	err_val = validate_dsm_major_numbers(dsm);
	if (err_val != 0) {
		_environment.exception(new mdc::dcs_error(err_val));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("register_dsm(%p): validate_dsm_major_numbers() "
		    "failed.\n", this));
		return;
	}

	//
	// Make a copy of the DSM object and try to add it to the list.
	//
	dc_config_impl::dsm_info_t *new_dsm = new dc_config_impl::dsm_info_t(
	    dsm, local_device_server, node_id);

	dsm_list_lock.wrlock();

	if (add_dsm_to_list_locked(new_dsm)) {
		// This is not a retry.
		get_checkpoint()->ckpt_register_dsm(dsm, local_device_server,
		    node_id, _environment);

		if (_environment.exception()) {
			// This means the node we are on is aborting.
			_environment.clear();
			dsm_list_lock.unlock();
			DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
			    ("register_dsm(%p): node is aborting.\n", this));
			return;
		}
	} else {
		// This is a retry so delete the unused list element.
		DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
		    ("register_dsm(%p): this is a retry.\n", this));
		delete new_dsm;
	}

	dsm_list_lock.unlock();

	mdc::device_service_info_seq *info_seq =
	    new mdc::device_service_info_seq;

	// Prevent services from being deleted.
	dc_services *services = dc_services::get_services();
	services->rdlock();

	dc_service_list service_list;

	// Get all the active services that this node is capable of hosting.
	services->get_active_services(service_list, node_id);

	info_seq->length(service_list.count());

	DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
	    ("register_dsm(%p): initial number of active services: '%d'.\n",
	    this, service_list.count()));

	dc_service *_service;
	char *tmp_str;
	dc_service_class *service_class;
	uint_t i = 0;

	FAULTPT_DCS(FAULTNUM_DCS_REGISTER_DSM_S_A,
		FaultFunctions::generic);

	while ((_service = service_list.reapfirst()) != NULL) {
		//
		// Synchronize with ::start_service()
		//
		_service->rdlock();
		_service->start_service_lock();
		if (!_service->is_active()) {
			//
			// The service became inactive between the call to
			// 'get_active_services' and now.
			//
			DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
			    ("register_dsm(%p): service '%d' became inactive "
			    "since call to get_active_services().\n",
				this, _service->get_service_id()));
			_service->start_service_unlock();
			_service->unlock();
			continue;
		}
		(*info_seq)[i].dev_id = _service->get_service_id();
		(*info_seq)[i].mapper = find_mapper((*info_seq)[i].dev_id);
		//
		// Active services must have a dc_mapper object associated
		// with them.
		//
		ASSERT(!CORBA::is_nil((*info_seq)[i].mapper));
		_service->get_service_name(&tmp_str);
		(*info_seq)[i].service_name = tmp_str;
		service_class = _service->get_service_class();
		(*info_seq)[i].user_pgm = service_class->get_user_program();
		(*info_seq)[i].priority = _service->get_priority(node_id);
		(*info_seq)[i].switchback_enabled =
		    _service->get_failback_mode();
		_service->start_service_unlock();
		_service->unlock();
		i++;
	}

	info_seq->length(i);

	services->unlock();

	service_info_seq = info_seq;

	FAULTPT_DCS(FAULTNUM_DCS_REGISTER_DSM_S_E,
		FaultFunctions::generic);

	DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
	    ("register_dsm(%p): final number of active services: '%d'.\n",
	    this, i));
}

//
// This IDL call is made by a device server from become_primary() when it
// thinks the DCS may currently have either a stale handle or no handle at all
// to it.
//
void
dc_config_impl::register_device_server(mdc::device_server_ptr dev_server,
    const char *service_name, Environment &_environment)
{
	ASSERT(!CORBA::is_nil(dev_server));

	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::register_device_server(%d, %s).\n",
	    dev_server, service_name));

	// Get the service startup lock for 'service_name'.
	dc_services *services = dc_services::get_services();
	services->rdlock();
	dc_service *_service = services->resolve_service(service_name,
	    dc_services::RDLOCKED);
	services->unlock();
	if (_service == NULL) {
		//
		// This means that the DCS switched/failed over between the
		// time the service became active, and the time this call
		// came in, and while reading the CCR from the new primary,
		// failed to find the service identified by 'service_name'.
		// This is a product of a serious CCR error, simply returning
		// will permit current open handles to the device to work, and
		// prevent any new access.  This seems like the best thing to
		// do.
		//
		os::warning("DCS: Could not find service %s.  Inconsistent "
		    "CCR information across nodes.", service_name);
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("register_device_server(%p): error detected in CCR.\n",
		    this));
		return;
	}

	//
	// Get the mapper for the service - it MUST exist if this call is
	// coming in.
	//
	unsigned int service_id = _service->get_service_id();
	mdc::dc_mapper_var mapper_obj = find_mapper(service_id);

	ASSERT(!CORBA::is_nil(mapper_obj));
	ASSERT(_service->is_active());

	dc_mapper_impl *mapperp = dc_mapper_impl::get_dc_mapper_impl(
	    mapper_obj);

	//
	// If the device server stored in the mapper object is not the same as
	// the device server passed in this call, then update the mapper
	// information.
	//
	mdc::device_server_var current_dev_server = mapperp->get_devobj();
	if ((CORBA::is_nil(current_dev_server)) ||
	    !(dev_server->_equiv(current_dev_server))) {
		DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
		    ("register_device_server(%p): updating mapper with new "
		    "device server.\n", this));
		mapperp->set_devobj(dev_server);
		get_checkpoint()->ckpt_device_server(mapper_obj,
		    dev_server, _environment);
		_environment.clear();
	}

	_service->unlock();
}

//
// Change the device server associated with a dc_mapper object.  Because this
// call is made on the secondary as a result of a checkpoint, no locking is
// required.
//
void
dc_config_impl::ckpt_device_server(const mdc::dc_mapper_ptr mapper_obj,
    mdc::device_server_ptr dev_server)
{
	ASSERT(!CORBA::is_nil(mapper_obj));
	ASSERT(!CORBA::is_nil(dev_server));

	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::ckpt_device_server(%p, %p).\n",
	    mapper_obj, dev_server));

	dc_mapper_impl *mapperp = dc_mapper_impl::get_dc_mapper_impl(
	    mapper_obj);
	ASSERT(mapperp != NULL);

	mapperp->set_devobj(dev_server);
}

//
// Get the local device service given a node id.
//
mdc::device_server_ptr
dc_config_impl::get_local_device_server(sol::nodeid_t id)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::get_local_device_server(%d).\n", id));

	dc_config_impl::dsm_info_t *tmp;
	mdc::device_server_ptr dev_server = mdc::device_server::_nil();

	dsm_list_lock.rdlock();
	for (dsm_list.atfirst();
	    (tmp = dsm_list.get_current()) != NULL;
	    dsm_list.advance()) {
		if (tmp->node_id == id) {
			dev_server = mdc::device_server::_duplicate(
			    tmp->local_device_server);
			break;
		}
	}
	dsm_list_lock.unlock();

	return (dev_server);
}

//
// Register a DSM on the secondary.
// No locking is needed - ordering is enforced by
// the ordering of checkpoints on the primary.
//
void
dc_config_impl::ckpt_register_dsm(mdc::device_service_manager_ptr dsm,
    mdc::device_server_ptr local_device_server, sol::nodeid_t node_id)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::ckpt_register_dsm(%p, %p, %d).\n",
	    dsm, local_device_server, node_id));

	dc_config_impl::dsm_info_t *tmp;

	//
	// A node may have gone down and come back up again.
	// Remove any old DSMs with the same node_id.
	// Also, the same DSM can be checkpointed twice if there is a
	// failover so don't add it if its there already.
	//
	for (dsm_list.atfirst();
	    (tmp = dsm_list.get_current()) != NULL;
	    dsm_list.advance()) {

		if (tmp->node_id == node_id) {
			//
			// Found a dsm with the same node_id.
			// Either this is a retry on a new primary or this is
			// the result of a node rebooting.
			//
			if (tmp->dsm->_equiv(dsm)) {
				// This is a retry.
				DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
				    ("ckpt_register_dsm(%p): this is a "
				    "retry.\n", this));
				return;
			}

			//
			// The node was rebooted; remove the stale dsm.
			//
			DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
			    ("ckpt_register_dsm(%p): this node was rebooted. "
			    "Removing the stale dsm.\n", this));
			(void) dsm_list.erase(tmp);
			delete tmp;

			//
			// There can be a maximum of one DSM entry with
			// the same node id on the list so there is no
			// need to look further.
			//
			break;
		}
	}

	dc_config_impl::dsm_info_t *new_dsm = new dc_config_impl::dsm_info_t(
	    dsm, local_device_server, node_id);
	dsm_list.append(new_dsm);
}

//
// Helper function that unregisters a DSM if an invocation on the DSM object
// results in a CORBA communication failure exception.
//
bool
dc_config_impl::remove_dsm_if_comm_error(mdc::device_service_manager_ptr dsm,
    CORBA::Exception *ex, Environment &env)
{
	ASSERT(!CORBA::is_nil(dsm));
	ASSERT(ex != NULL);

	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::remove_dsm_if_comm_error(%p).\n", dsm));

	if (CORBA::COMM_FAILURE::_exnarrow(ex) != NULL) {
		//
		// If the exception is CORBA communication failure,
		// it means the node has died and the DSM is no longer
		// a valid object so we remove it from the list.
		//
		dsm_list_lock.wrlock();
		dc_config_impl::dsm_info_t *dsm_info_p = find_dsm_info(dsm);
		if (dsm_info_p != NULL) {
			unregister_dsm(dsm_info_p, env);
		}
		dsm_list_lock.unlock();

		DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
		    ("remove_dsm_if_comm_error(%p): node has died.\n", this));
		return (true);
	}

	return (false);
}

//
// Unregister a DSM object.
// The call also checkpoints this removal to other replicas.
//
void
dc_config_impl::unregister_dsm(dc_config_impl::dsm_info_t *dsm_info_p,
    Environment &env)
{
	ASSERT(dsm_list_lock.lock_held());
	ASSERT(dsm_info_p != NULL);

	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::unregister_dsm(%p).\n", dsm_info_p));

	// Checkpoint the removal.
	get_checkpoint()->ckpt_unregister_dsm(dsm_info_p->dsm, env);
	if (env.exception()) {
		// This means the node we are on is aborting.
		env.clear();
		DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
		    ("unregister_dsm(%p): node is aborting.\n", this));
		return;
	}

	(void) dsm_list.erase(dsm_info_p);
	delete dsm_info_p;
}

//
// Remove a dsm from the secondary.  No locking is needed - ordering is
// enforced by the ordering of chackpoints on the primary.
//
void
dc_config_impl::ckpt_unregister_dsm(mdc::device_service_manager_ptr dsm)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::ckpt_unregister_dsm(%p).\n", dsm));

	dc_config_impl::dsm_info_t *tmp;
	for (dsm_list.atfirst();
	    (tmp = dsm_list.get_current()) != NULL;
	    dsm_list.advance()) {
		if (tmp->dsm->_equiv(dsm)) {
			DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
			    ("ckpt_unregister_dsm(%p): removing dsm for "
			    "nodeid %d.\n", this, tmp->node_id));
			(void) dsm_list.erase(tmp);
			delete tmp;
			return;
		}
	}
	// Unregister should not fail!
	ASSERT(0);
}

//
// Return the DSM object for a given node ID.
// The dsm_list_lock should be held.
//
mdc::device_service_manager_ptr
dc_config_impl::get_dsm_object(sol::nodeid_t node_id)
{
	ASSERT(dsm_list_lock.lock_held());

	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::get_dsm_object(%d).\n", node_id));

	dc_config_impl::dsm_info_t *tmp_dsm_info;

	IntrList<dc_config_impl::dsm_info_t, _SList>::ListIterator iterator(
	    dsm_list);
	while ((tmp_dsm_info = iterator.get_current()) != NULL) {
		if (tmp_dsm_info->node_id == node_id) {
			DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
			    ("get_dsm_object(%p): dsm object found.\n", this));
			return (mdc::device_service_manager::_duplicate(
			    tmp_dsm_info->dsm));
		}
		iterator.advance();
	}

	DCS_DBPRINTF_A(DCS_TRACE_DC_CONFIG,
	    ("get_dsm_object(%p): no dsm object found.\n", this));

	return (mdc::device_service_manager::_nil());
}

//
// Return the dsm_info_t pointer for the given DSM.
// Return NULL if the DSM is not found.
// The dsm_list_lock should be held.
//
//lint -e1038 type dsm_info_t not found, 'dc_config_impl::dsm_info_t' assumed
dc_config_impl::dsm_info_t *
//lint +e1038
dc_config_impl::find_dsm_info(mdc::device_service_manager_ptr dsm)
{
	ASSERT(dsm_list_lock.lock_held());

	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::find_dsm_info(dsm=%p).\n", dsm));

	dc_config_impl::dsm_info_t *tmp_dsm_info;

	IntrList<dc_config_impl::dsm_info_t, _SList>::ListIterator iterator(
	    dsm_list);
	while ((tmp_dsm_info = iterator.get_current()) != NULL) {
		if ((tmp_dsm_info->dsm)->_equiv(dsm)) {
			DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
			    ("find_dsm_info(%p): tmp_dsm_info=%p.\n",
			    this, tmp_dsm_info));
			return (tmp_dsm_info);
		}
		iterator.advance();
	}

	DCS_DBPRINTF_A(DCS_TRACE_DC_CONFIG,
	    ("find_dsm_info(%p): no dsm info found.\n", this));

	return (NULL);
}

//
// Return the dsm_info_t pointer for the given node ID.
// Return NULL if the DSM is not found.
// The dsm_list_lock should be held.
//
//lint -e1038 type dsm_info_t not found, 'dc_config_impl::dsm_info_t' assumed
dc_config_impl::dsm_info_t *
//lint +e1038
dc_config_impl::find_dsm_info(sol::nodeid_t node_id)
{
	ASSERT(dsm_list_lock.lock_held());

	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::find_dsm_info(node_id=%d).\n", node_id));

	dc_config_impl::dsm_info_t *tmp_dsm_info;

	IntrList<dc_config_impl::dsm_info_t, _SList>::ListIterator iterator(
	    dsm_list);
	while ((tmp_dsm_info = iterator.get_current()) != NULL) {
		if (tmp_dsm_info->node_id == node_id) {
			DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
			    ("find_dsm_info(%p): tmp_dsm_info=%p.\n",
			    this, tmp_dsm_info));
			return (tmp_dsm_info);
		}
		iterator.advance();
	}

	DCS_DBPRINTF_A(DCS_TRACE_DC_CONFIG,
	    ("find_dsm_info(%p): no dsm info found.\n", this));

	return (NULL);
}

//
// This operation starts up an HA device service on active DSMs.
// It is called from dc_server::get_configured_nodes() and from
// dc_server::get_device_server().
// Since the IDL call that starts up the service on DSMs is idempotent,
// so is this operation.
// Note that we set the service to be active after each successful call to
// dc_service::start_service. If a spare is being shutdown and at the same
// time another node is trying to become part of the service (or start a new
// incarnation of the service), it will have to wait for the replica framework
// to finish its cleanup work (it will not return from the dc_service::start_
// service call.). When the framework makes it the primary, the service would
// have been set to false. To avoid this situation, we set the service to be
// active again.
//
mdc::device_server_ptr
dc_config_impl::start_service(dc_service *_service, nodeid_t source_node_id,
    bool force_preferred_primary, bool force_active,
    mdc::dc_mapper_var &mapper_obj, Environment &env)
{
	uint_t i, start_service_error = 0;
	char *state_str = CL_DS_STARTING;

	ASSERT(_service->lock_held());

	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::start_service(%p, %d, %d, %d, %p).\n",
	    _service, source_node_id, force_preferred_primary,
	    force_active, mapper_obj));

	char *service_name = NULL;
	_service->get_service_name(&service_name);
	char *service_class = (_service->get_service_class())->name;

	DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
	    ("start_service(%p): service class/name '%s/%s'.\n",
	    this, service_class, service_name));

	(void) sc_publish_event(
		ESC_CLUSTER_DCS_STATE_CHANGE,
		CL_EVENT_PUB_DCS, CL_EVENT_SEV_INFO,
		CL_EVENT_INIT_OPERATOR, 0, 0, 0,
		CL_DS_NAME, SE_DATA_TYPE_STRING,
		service_name,
		CL_DS_SERVICE_CLASS, SE_DATA_TYPE_STRING,
		service_class,
		CL_OLD_STATE, SE_DATA_TYPE_STRING,
		"Offline",
		CL_NEW_STATE, SE_DATA_TYPE_STRING,
		state_str,
		NULL);

	mdc::node_preference_seq_t service_nodes;

	_service->get_nodes(service_nodes);
	uint_t node_count = service_nodes.length();

	if (node_count == 0) {
		if (!force_active) {
			delete [] service_name;
			DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
			    ("start_service(%p): no active node.\n", this));
			return (mdc::device_server::_nil());
		}
	}

	char *s_name = dclib::get_serviceid(service_name);

	//
	// List of 'dsm_info_t' structures for the nodes that can serve this
	// service.
	//
	IntrList<dc_config_impl::dsm_info_t, _SList> service_dsm_list;
	bool preference = false;

	//
	// This variable points to the 'dsm_info_t' structure corresponding to
	// the source node.
	//
	dc_config_impl::dsm_info_t *source_dsm_info = NULL;

	//
	// Sort the potential nodes by preference.  This is a tiny array, so
	// just bubble-sort.
	// Note that we have to do this even though the RM has preferences
	// because RM preferences are effective only during failover, not
	// initial startup.
	// A smaller number as the priority means a higher priority.

	struct mdc::node_preference tmp_node;
	for (i = 0; i < node_count-1; i++) {
		for (uint_t j = i+1; j < node_count; j++) {
			if (service_nodes[i].preference >
			    service_nodes[j].preference) {
				tmp_node = service_nodes[i];
				service_nodes[i] = service_nodes[j];
				service_nodes[j] = tmp_node;
			}
		}
	}

	// See if any preference is configured.
	if (node_count > 0) {
		if (service_nodes[0].preference ==
		    service_nodes[node_count-1].preference) {
			preference = false;
		} else {
			preference = true;
		}
	}

	//
	// If the device service does not contain any preferred node, then we
	// implement the policy of making the node that accessed the device
	// first the primary.
	//
	dc_config_impl::dsm_info_t *tmp_dsm_info;
	dsm_list_lock.rdlock();
	for (i = 0; i < node_count; i++) {
		mdc::device_service_manager_ptr tmp_dsm =
		    get_dsm_object(service_nodes[i].id);
		if (CORBA::is_nil(tmp_dsm)) {
			continue;
		}
		tmp_dsm_info = new dc_config_impl::dsm_info_t(tmp_dsm,
		    mdc::device_server::_nil(), service_nodes[i].id);
		//
		// If no preferences are configured, or if the dsm id passed in
		// to this function is meant to override configured
		// preferences, store a handle to the DSM of "source_node_id".
		//
		if ((force_preferred_primary || (!preference)) &&
		    (service_nodes[i].id == source_node_id)) {
			ASSERT(source_dsm_info == NULL);
			source_dsm_info = tmp_dsm_info;
		} else {
			service_dsm_list.append(tmp_dsm_info);
		}
	}

	if ((source_dsm_info == NULL) && (service_dsm_list.empty())) {
		// No nodes that could host this device service are alive.
		if (!force_active) {
			dsm_list_lock.unlock();
			DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
			    ("start_service(%p): no node is up.\n", this));
			delete [] s_name;
			delete [] service_name;
			return (mdc::device_server::_nil());
		}
	}

	//
	// Mark the service as active.  Now, if any other valid DSM is added to
	// the list via register_dsm(), this service will be started up on it.
	//
	_service->set_active(true);
	dsm_list_lock.unlock();

	//
	// Create a new mapper object which is created one-to-one with
	// device_server objects so we can tell if the device_server object
	// dies. There is also a chicken & egg problem since they both hold
	// references to eachother so we have to send a mapper reference to
	// the DSM so it can create a device_server and return it without
	// another IDL call from the DSM to us (which can cause HA service
	// dependency deadlocks).
	// If all goes well, we store the 'devid' value in the new 'dc_mapper'
	// object.  We will add the object to the list only at that time - it
	// is useless before that point.
	//
	unsigned int dev_id = _service->get_service_id();
	dc_mapper_impl *mapperp = new dc_mapper_impl(dc_repl_serverp, 0);
	mapper_obj = mapperp->get_objref();
	get_checkpoint()->ckpt_add_dc_mapper(mapper_obj, env);
	env.clear();

	//
	// We use a local environment for all nested invocations because we are
	// an HA object which makes it illegal to reuse our own environment.
	//
	Environment e;
	CORBA::Exception *ex;

	//
	// If there are no preferences configured in this device service, start
	// up the primary on the node that made this request.
	//
	if (source_dsm_info != NULL) {
		_service->start_service(source_dsm_info->dsm,
		    source_dsm_info->node_id, mapper_obj, e);
		_service->set_active(true);
		if ((ex = e.release_exception()) != NULL) {
			if (!remove_dsm_if_comm_error(source_dsm_info->dsm,
			    ex, env)) {
				ex->print_exception("Error calling"
				    " dsm::start_device_service\n");
				DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
				    ("start_service(%p): exception '%s' while "
				    "calling "
				    "dsm::start_device_service().\n",
				    this, e.exception()->_name()));
				start_service_error = 1;
			}
		}
	}

	//
	// Go through 'service_dsm_list', starting up the service on all the
	// nodes.
	//
	for (service_dsm_list.atfirst();
	    ((tmp_dsm_info = service_dsm_list.get_current()) != NULL);
	    service_dsm_list.advance()) {
		_service->start_service(tmp_dsm_info->dsm,
		    tmp_dsm_info->node_id, mapper_obj, e);
		_service->set_active(true);
		if ((ex = e.release_exception()) != NULL) {
			if (!remove_dsm_if_comm_error(tmp_dsm_info->dsm, ex,
			    env)) {
				ex->print_exception("Error calling"
				    " dsm::start_device_service");
				DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
				    ("start_service(%p): exception '%s' while "
				    "calling "
				    "dsm::start_device_service().\n",
				    this, e.exception()->_name()));
				start_service_error = 1;
			}
		}
	}

	replica::rm_admin_var rm_ref = rm_util::get_rm();
	ASSERT(!CORBA::is_nil(rm_ref));
	replica::service_admin_var controlp = rm_ref->get_control(s_name, e);
	mdc::device_server_ptr dev_server = mdc::device_server::_nil();

	if (e.exception()) {
		//
		// None of the nodes that host the service are a part of the
		// cluster.
		//
		e.clear();

		if (!force_active) {
			// The service is not active.
			_service->set_active(false);
			dev_id = 0;
			goto out;
		}
	} else {
		replica::repl_prov_seq_var repl_provs;
		bool found_it;

		controlp->get_repl_provs(repl_provs, e);
		if (e.exception()) {
			os::warning("DCS: get_repl_provs() exception\n");
			DCS_DBPRINTF_A(DCS_TRACE_DC_CONFIG,
			    ("start_service(%p): exception '%s' while calling "
			    "get_repl_provs().\n", this,
			    e.exception()->_name()));
			e.clear();
		}
		service_dsm_list.atfirst();
		while ((tmp_dsm_info = service_dsm_list.reapfirst()) != NULL) {
			found_it = false;
			for (uint_t ii = 0; ii < repl_provs.length(); ii++)
				if (os::atoi(repl_provs[ii].repl_prov_desc) ==
				    (int)tmp_dsm_info->node_id) {
					found_it = true;
					break;
				}

			if (found_it) {
				//
				// If the replica for a DSM on the list exists,
				// then replicas for DSMs on the list after this
				// DSM must exist because of the way the replica
				// framework works.
				//
				break;
			}

			_service->start_service(tmp_dsm_info->dsm,
			    tmp_dsm_info->node_id, mapper_obj, e);
			_service->set_active(true);
			if ((ex = e.release_exception()) != NULL) {
				if (!remove_dsm_if_comm_error(
				    tmp_dsm_info->dsm, ex, env)) {
					ex->print_exception(
					    "Error calling "
					    "dsm::start_device_service");
					DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
					    ("start_service(%p): exception "
					    "'%s' while calling dsm:"
					    ":start_device_service().\n",
					    this, e.exception()->_name()));
					start_service_error = 1;
				}
			}
		}

		//
		// Try to get the root object from the newly started
		// device_service.
		//
		controlp = rm_ref->get_control(s_name, e);
		CORBA::Object_var obj;
		if (e.exception() == NULL) {
			obj = controlp->get_root_obj(e);
		}
		if (e.exception()) {
			//
			// None of the nodes that host the service are
			// a part of the cluster.
			//
			e.clear();

			if (!force_active) {
				// The service is not active.
				_service->set_active(false);
				dev_id = 0;
				start_service_error = 1;
				DCS_DBPRINTF_A(DCS_TRACE_DC_CONFIG,
				    ("start_service(%p): all nodes hosting "
				    "the service are down.\n", this));
				goto out;
			}
		} else {
			dev_server = mdc::device_server::_narrow(obj);
			ASSERT(!CORBA::is_nil(dev_server));
		}
	}

	ASSERT(!CORBA::is_nil(dev_server) || (force_active));
	ASSERT(dev_id != 0);

	mapperp->set_devid(dev_id);
	if (!CORBA::is_nil(dev_server)) {
		mapperp->set_devobj(dev_server);
	}

out:
	// Cleanup.
	service_dsm_list.dispose();
	delete [] s_name;
	//
	// source_dsm_info can be NULL if a node, which is not part of
	// the service attempts to start it.
	//
	if (source_dsm_info != NULL) {
	    delete source_dsm_info;
	}

	//
	// Even if there was an error in starting up the service, we need to
	// add it to 'mapper_list' and keep it around as an HA object, so that
	// it gets checkpointed to joining secondaries.  See BugId 4286681.
	// Record the new mapper in our list.
	//
	list_lock.wrlock();
	mapper_list.prepend(mapperp);
	if (dev_id != 0) {
		get_checkpoint()->ckpt_set_dc_mapper(mapper_obj, dev_id,
		    dev_server, env);
		env.clear();
	}
	list_lock.unlock();

	if (start_service_error) {
		state_str = CL_DS_START_FAILED;
	} else {
		state_str = CL_DS_STARTED;
	}

	(void) sc_publish_event(
	ESC_CLUSTER_DCS_STATE_CHANGE,
		CL_EVENT_PUB_DCS, CL_EVENT_SEV_INFO,
		CL_EVENT_INIT_OPERATOR, 0, 0, 0,
		CL_DS_NAME, SE_DATA_TYPE_STRING,
		service_name,
		CL_DS_SERVICE_CLASS, SE_DATA_TYPE_STRING,
		service_class,
		CL_OLD_STATE, SE_DATA_TYPE_STRING,
		"Offline",
		CL_NEW_STATE, SE_DATA_TYPE_STRING,
		state_str,
		NULL);

	delete [] service_name;

	return (dev_server);
}

//
// IDL entry-point to create a new service class.
//
// dc_config_impl(mdc::dc_config::create_service_class)
void
dc_config_impl::create_service_class(const char *service_class,
    const char *user_program, mdc::ha_dev_type type,
    Environment &env)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::create_service_class(%s, %s, %d).\n",
	    service_class, user_program, type));

	dc_services *services = dc_services::get_services();

	services->wrlock();
	dc_error_t err_val = services->create_service_class(service_class,
	    user_program, type);
	services->unlock();

	if (err_val != DCS_SUCCESS) {
		env.exception(new mdc::dcs_error(err_val));
		DCS_DBPRINTF_A(DCS_TRACE_DC_CONFIG,
		    ("create_service_class(%p): create_service_class() "
		    "failed for service class '%s' with error '%d'.\n",
		    this, service_class, err_val));
	}
}

//
// IDL entry-point to set the user program of a service class.
//
// dc_config_impl(mdc::dc_config::set_service_class_user_program)
void
dc_config_impl::set_service_class_user_program(const char *service_class,
    const char *user_program, Environment &env)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::set_service_class_user_program(%s, %s).\n",
	    service_class, user_program));

	dc_services *services = dc_services::get_services();

	services->wrlock();
	dc_error_t err_val = services->set_service_class_user_program(
	    service_class, user_program);
	services->unlock();

	if (err_val != DCS_SUCCESS) {
		env.exception(new mdc::dcs_error(err_val));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("set_service_class_user_program(%p): "
		    "set_service_class_user_program() failed.\n", this));
	}
}

//
// IDL entry-point to set the HA type of a service class.
//
// dc_config_impl(mdc::dc_config::set_service_class_ha_type)
void
dc_config_impl::set_service_class_ha_type(const char *service_class,
    mdc::ha_dev_type type, Environment &env)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::set_service_class_ha_type(%s, %d).\n",
	    service_class, type));

	dc_services *services = dc_services::get_services();

	services->wrlock();
	dc_error_t err_val = services->set_service_class_ha_type(
	    service_class, type);
	services->unlock();

	if (err_val != DCS_SUCCESS) {
		env.exception(new mdc::dcs_error(err_val));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("set_service_class_ha_type(%p): "
		    "set_service_class_ha_type() failed.\n", this));
	}
}

//
// IDL entry point to get the list of service class names.
//
// dc_config_impl(mdc::dc_config::get_service_class_names)
void
dc_config_impl::get_service_class_names(sol::string_seq_t_out service_names,
    Environment &)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::get_service_class_names().\n"));

	dc_services *services = dc_services::get_services();
	services->rdlock();
	services->get_service_class_names(service_names);
	services->unlock();
}

//
// IDL entry point to get the parameters of the specified service class names.
//
// dc_config_impl(mdc::dc_config::get_service_class_parameters)
void
dc_config_impl::get_service_class_parameters(const char *service_class,
    CORBA::String_out user_program, mdc::ha_dev_type &type,
    Environment &env)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::get_service_class_parameters(%s).\n",
	    service_class));

	dc_services *services = dc_services::get_services();

	services->rdlock();
	dc_error_t err_val = services->get_service_class_parameters(
	    service_class, user_program, type);
	services->unlock();

	if (err_val != DCS_SUCCESS) {
		env.exception(new mdc::dcs_error(err_val));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("get_service_class_parameters(%p): "
		    "get_service_class_parameters() failed.\n", this));
	}
//lint -e1746 parameter 'user_program' could be made const reference
}
//lint +e1746

//
// IDL entry point to remove a service class.
//
// dc_config_impl(mdc::dc_config::remove_service_class)
void
dc_config_impl::remove_service_class(const char *service_class,
    Environment &env)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::remove_service_class(%s).\n",
	    service_class));

	dc_services *services = dc_services::get_services();

	services->wrlock();
	dc_error_t err_val = services->remove_service_class(service_class);
	services->unlock();

	if (err_val != DCS_SUCCESS) {
		env.exception(new mdc::dcs_error(err_val));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("remove_service_class(%p): remove_service_class() "
		    "failed.\n", this));
	}
}

//
// IDL entry point to set the specified properties of a device service.
//
// dc_config_impl(mdc::dc_config::set_properties)
void
dc_config_impl::set_properties(const char *service_name,
    const mdc::service_property_seq_t &properties,
    Environment &env)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::set_properties(%s, %p).\n",
	    service_name, properties));

	dc_services *services = dc_services::get_services();
	services->rdlock();

	dc_service *_service = services->resolve_service(service_name,
	    dc_services::WRLOCKED);

	services->unlock();

	if (_service == NULL) {
		// Service with this name does not exist
		env.exception(new mdc::dcs_error(DCS_ERR_SERVICE_NAME));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("set_properties(%p): service '%s' does not exist.\n",
		    this, service_name));
		return;
	}

	dc_error_t err_val = _service->set_properties(properties);
	_service->unlock();

	if (err_val != DCS_SUCCESS) {
		env.exception(new mdc::dcs_error(err_val));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("set_properties(%p): set_properties() failed.\n", this));
	}
}

//
// IDL entry point to remove the specified properties of a device service.
//
// dc_config_impl(mdc::dc_config::remove_properties)
void
dc_config_impl::remove_properties(const char *service_name,
    const sol::string_seq_t &property_names,
    Environment &env)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::remove_properties(%s, %p).\n",
	    service_name, property_names));

	dc_services *services = dc_services::get_services();
	services->rdlock();

	dc_service *_service = services->resolve_service(service_name,
	    dc_services::WRLOCKED);

	services->unlock();

	if (_service == NULL) {
		// Service with this name does not exist
		env.exception(new mdc::dcs_error(DCS_ERR_SERVICE_NAME));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("remove_properties(%p): service '%s' does not exist.\n",
		    this, service_name));
		return;
	}

	dc_error_t err_val = _service->remove_properties(property_names);
	_service->unlock();

	if (err_val != DCS_SUCCESS) {
		env.exception(new mdc::dcs_error(err_val));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("remove_properties(%p): remove_properties() failed.\n",
		    this));
	}
}

//
// IDL entry point to get the specified property value for a device service.
//
// dc_config_impl(mdc::dc_config::get_property)
void
dc_config_impl::get_property(const char *service_name,
    const char *property_name, CORBA::String_out property_value,
    Environment &env)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::get_property(%s, %s).\n",
	    service_name, property_name));

	dc_services *services = dc_services::get_services();
	services->rdlock();

	dc_service *_service = services->resolve_service(service_name,
	    dc_services::RDLOCKED);

	services->unlock();

	if (_service == NULL) {
		// Service with this name does not exist
		env.exception(new mdc::dcs_error(DCS_ERR_SERVICE_NAME));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("get_property(%p): service '%s' does not exist.\n",
		    this, service_name));
		return;
	}

	dc_error_t err_val = _service->get_property(property_name,
	    property_value);
	_service->unlock();

	if (err_val != DCS_SUCCESS) {
		env.exception(new mdc::dcs_error(err_val));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("get_property(%p): get_property() failed.\n", this));
	}
//lint -e1746 parameter 'property_value' could be made const reference
}
//lint +e1746

//
// IDL entry point to get all the properties of the specified device service.
//
// dc_config_impl(mdc::dc_config::get_all_properties)
void
dc_config_impl::get_all_properties(const char *service_name,
    mdc::service_property_seq_t_out properties,
    Environment &env)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::get_all_properties(%s).\n",
	    service_name));

	dc_services *services = dc_services::get_services();
	services->rdlock();

	dc_service *_service = services->resolve_service(service_name,
	    dc_services::RDLOCKED);

	services->unlock();

	if (_service == NULL) {
		// Service with this name does not exist
		env.exception(new mdc::dcs_error(DCS_ERR_SERVICE_NAME));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("get_all_properties(%p): service '%s' does not exist.\n",
		    this, service_name));
		return;
	}

	dc_error_t err_val = _service->get_all_properties(properties);
	_service->unlock();

	if (err_val != DCS_SUCCESS) {
		env.exception(new mdc::dcs_error(err_val));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("get_all_properties(%p): get_all_properties() failed.\n",
		    this));
	}
//lint -e1746 parameter 'property_value' could be made const reference
}
//lint +e1746

//
// IDL entry-point to get the service names of all services belonging to the
// specified class.
//
// dc_config_impl(mdc::dc_config::get_service_name_of_class)
void
dc_config_impl::get_service_names_of_class(const char *service_class,
    sol::string_seq_t_out service_names, Environment &env)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::get_service_names_of_class(%s).\n",
	    service_class));

	dc_services *services = dc_services::get_services();
	services->rdlock();
	dc_error_t err_val = services->get_service_names_of_class(
	    service_class, service_names);
	services->unlock();

	if (err_val != DCS_SUCCESS) {
		env.exception(new mdc::dcs_error(err_val));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("get_service_names_of_class(%p): "
		    "get_service_names_of_class() failed.\n", this));
	}
}

//
// IDL entry-point to get to a service name from a dev_t value.
//
void
dc_config_impl::get_service_by_dev_t(sol::major_t maj, sol::minor_t _min,
    CORBA::String_out service_name, Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::get_service_by_dev_t().\n"));

	bool found;
	unsigned int device_id;
	char *sname;

	dc_storage *dcstore = dc_storage::storage();
	dc_services *services = dc_services::get_services();

	// Lock down services to prevent deletions.
	services->rdlock();

	// Look for the specified devt
	found = dcstore->get_map(maj, _min, &device_id);
	if (!found) {
		services->unlock();
		_environment.exception(new mdc::dcs_error(
		    DCS_ERR_DEVICE_INVAL));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("get_service_by_dev_t(%p): get_map() failed.\n", this));
		return;
	}

	// Get the service with id "device_id"
	dc_service *_service = services->resolve_service_from_id(
	    device_id, dc_services::RDLOCKED);
	services->unlock();

	ASSERT(_service != NULL);
	_service->get_service_name(&sname);
	service_name = sname;
	_service->unlock();
}

//
// This is the idl entry point to create a new device service.  The procedure
// checks for validity of all the parameters passed in, allocates a service id,
// and writes out the service to persistent storage.
//
// dc_config_impl(mdc::dc_config::create_device_service)
void
dc_config_impl::create_device_service(const char *service_name,
    const char *service_class_name, bool failback_enabled,
    const mdc::node_preference_seq_t &nodes, uint32_t num_secondaries,
    const mdc::dev_range_seq_t &gdev_ranges,
    const mdc::service_property_seq_t &properties,
    Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::create_device_service(%s, %s, %d, %p, %d, %p, %p)"
	    ".\n", service_name, service_class_name, failback_enabled,
	    nodes, num_secondaries, gdev_ranges, properties));

	dc_services *services = dc_services::get_services();
	dc_storage *dcstore = dc_storage::storage();
	dc_error_t err_val;

	if (service_name == NULL) {
		_environment.exception(new mdc::dcs_error(
		    DCS_ERR_SERVICE_NAME));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("create_device_service(%p): service_name undefined.\n",
		    this));
		return;
	}

	err_val = validate_major_numbers(gdev_ranges);
	if (err_val != DCS_SUCCESS) {
		_environment.exception(new mdc::dcs_error(err_val));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("create_device_service(%p): validate_major_numbers() "
		    "failed.\n", this));
		return;
	}

	services->wrlock();

	if (services->resolve_service(service_name, dc_services::UNLOCKED)
	    != NULL) {
		// Service with this name already exists.
		services->unlock();
		_environment.exception(new mdc::dcs_error(
		    DCS_ERR_SERVICE_NAME));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("create_device_service(%p): service with this name ('%s')"
		    "already exists.\n", this, service_name));
		return;
	}

	dc_service_class *service_class = services->find_service_class(
	    service_class_name);
	if (service_class == NULL) {
		// No service class of this name exists.
		services->unlock();
		_environment.exception(new mdc::dcs_error(
		    DCS_ERR_SERVICE_CLASS_NAME));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("create_device_service(%p): no service class "
		    "associated with the service class name '%s'.\n",
		    this, service_class_name));
		return;
	}

	FAULTPT_DCS(FAULTNUM_DCS_CREATE_DEVICE_SERVICE_S_B,
		FaultFunctions::generic);

	// Verify that node ids are valid
	for (uint_t cnt = 0; cnt < nodes.length(); cnt++) {
		if (!orb_conf::node_configured(nodes[cnt].id)) {
			services->unlock();
			_environment.exception(new mdc::dcs_error(
			    DCS_ERR_NODE_ID));
			DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
			    ("create_device_service(%p): invalid node "
			    "id: %d.\n", this, nodes[cnt].id));
			return;
		}
	}

	//
	// Get a valid service id.  This allocation is done in memory and
	// will be written out to the CCR from within the
	// 'create_device_service' call.
	//
	unsigned int service_id = services->allocate_service_id();

	// See if the addition of these device ranges is valid.
	if ((err_val = dcstore->possible_mapping(add_mapping, gdev_ranges,
	    service_id)) != DCS_SUCCESS) {
		// Undo.
		services->free_service_id_in_memory(service_id);
		services->unlock();
		_environment.exception(new mdc::dcs_error(err_val));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("create_device_service(%p): service_id (%d) out of "
		    "range.\n", this, service_id));
		return;
	}

	// Configure the device ranges in memory.
	dc_minor_range_list *gdev_ranges_list = new dc_minor_range_list;
	err_val = dcstore->process_mapping(add_mapping, gdev_ranges,
	    service_id, gdev_ranges_list);
	ASSERT(err_val == DCS_SUCCESS);

	//
	// Write out to the CCR.  Note that we are giving up our "hold"
	// on 'gdev_ranges_list'.
	//
	err_val = services->create_device_service(service_id, service_name,
	    service_class, failback_enabled, nodes, num_secondaries,
	    gdev_ranges_list, properties);

	FAULTPT_DCS(FAULTNUM_DCS_CREATE_DEVICE_SERVICE_S_A,
		FaultFunctions::generic);

	if (err_val != DCS_SUCCESS) {
		os::warning("Error writing out service %d to the CCR.",
		    service_id);
		// Try to free the service id.
		(void) services->free_service_id(service_id);
		services->unlock();
		_environment.exception(new mdc::dcs_error(err_val));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("create_device_service(%p): create_device_service() "
		    "failed.\n", this));
		return;
	}

	(void) sc_publish_event(
		ESC_CLUSTER_DCS_CONFIG_CHANGE,
		CL_EVENT_PUB_DCS, CL_EVENT_SEV_INFO,
		CL_EVENT_INIT_OPERATOR, 0, 0, 0,
		CL_DS_NAME, SE_DATA_TYPE_STRING,
		service_name,
		CL_CONFIG_ACTION, SE_DATA_TYPE_UINT32,
		CL_EVENT_CONFIG_ADDED,
		CL_PROPERTY_NAME, SE_DATA_TYPE_STRING,
		"null",
		CL_PROPERTY_VALUE, SE_DATA_TYPE_STRING,
		"null",
		NULL);

	services->unlock();

	FAULTPT_DCS(FAULTNUM_DCS_CREATE_DEVICE_SERVICE_S_E,
		FaultFunctions::generic);

}

//
// This idl interface sets the failback mode of a service.  The operation is
// idempotent and needs no transactions.
//
// dc_config_impl(mdc::dc_config::set_failback_mode)
void
dc_config_impl::set_failback_mode(const char *service_name,
    bool failback_enabled, Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::set_failback_mode(%s, %d).\n",
	    service_name, failback_enabled));

	dc_services *services = dc_services::get_services();

	services->rdlock();

	dc_service *_service = services->resolve_service(service_name,
	    dc_services::WRLOCKED);

	services->unlock();

	if (_service == NULL) {
		// Service with this name does not exist
		_environment.exception(new mdc::dcs_error(
		    DCS_ERR_SERVICE_NAME));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("set_failback_mode(%p): service '%s' does not exist.\n",
		    this, service_name));
		return;
	}

	dc_error_t err_val = _service->set_failback_mode(failback_enabled);
	_service->unlock();

	if (err_val != DCS_SUCCESS) {
		_environment.exception(new mdc::dcs_error(err_val));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("set_failback_mode(%p): set_failback_mode() failed.\n",
		    this));
	} else {
		//
		// Publish the Device Service Property change event.
		//
		char failback_mode_str[16];
		if (failback_enabled) {
			os::sprintf(failback_mode_str, "%s", "true");
		} else {
			os::sprintf(failback_mode_str, "%s", "false");
		}
		(void) sc_publish_event(
			ESC_CLUSTER_DCS_CONFIG_CHANGE,
			CL_EVENT_PUB_DCS, CL_EVENT_SEV_INFO,
			CL_EVENT_INIT_OPERATOR, 0, 0, 0,
			CL_DS_NAME, SE_DATA_TYPE_STRING,
			service_name,
			CL_CONFIG_ACTION, SE_DATA_TYPE_UINT32,
			CL_EVENT_CONFIG_PROP_CHANGED,
			CL_PROPERTY_NAME, SE_DATA_TYPE_STRING,
			"Failback mode",
			CL_PROPERTY_VALUE, SE_DATA_TYPE_STRING,
			failback_mode_str,
			NULL);
	}
}

//
// IDL entry-point to set the desired number of secondaries of a service.
// This call writes the number to the the CCR, and if the service is active,
// also calls into the HA framework to notify it of this change. The
// operation is idempotent and needs no transactions.
//
// dc_config_impl(mdc::dc_config::set_active_secondaries)
void
dc_config_impl::set_active_secondaries(const char *service_name,
    unsigned int num_secondaries, Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::set_active_secondaries(%s, %d).\n",
	    service_name, num_secondaries));

	replica::rm_admin_var rm_ref;
	replica::service_admin_var svc_admin_ref;
	CORBA::Exception *ex;
	Environment e, env;
	char num_sec_str[16];

	dc_services *services = dc_services::get_services();

	services->rdlock();

	dc_service *_service = services->resolve_service(service_name,
	    dc_services::WRLOCKED);

	services->unlock();

	if (_service == NULL) {
		// Service with this name does not exist
		_environment.exception(new mdc::dcs_error(
		    DCS_ERR_SERVICE_NAME));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("set_active_secondaries(%p): service '%s' does not "
		    "exist.\n", this, service_name));
		return;
	}

	// Write the number to the CCR.
	dc_error_t err_val = _service->set_number_of_secondaries(
	    num_secondaries);

	if (err_val != DCS_SUCCESS) {
		_service->unlock();
		_environment.exception(new mdc::dcs_error(err_val));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("set_active_secondaries(%p): set_number_of_secondaries() "
		    "failed.\n", this));
		return;
	}

	//
	// Publish the event for Device Service property change.
	//
	os::sprintf(num_sec_str, "%d", num_secondaries);
	(void) sc_publish_event(
		ESC_CLUSTER_DCS_CONFIG_CHANGE,
		CL_EVENT_PUB_DCS, CL_EVENT_SEV_INFO,
		CL_EVENT_INIT_OPERATOR, 0, 0, 0,
		CL_DS_NAME, SE_DATA_TYPE_STRING,
		service_name,
		CL_CONFIG_ACTION, SE_DATA_TYPE_UINT32,
		CL_EVENT_CONFIG_PROP_CHANGED,
		CL_PROPERTY_NAME, SE_DATA_TYPE_STRING,
		"Number of secondaries",
		CL_PROPERTY_VALUE, SE_DATA_TYPE_STRING,
		num_sec_str,
		NULL);

	// If the service is not active, we return. Otherwise we inform
	// the replica framework of this change.
	if (!_service->is_active()) {
		DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
		    ("set_active_secondaries(%p): service '%s' is inactive.\n",
		    this, service_name));
		_service->unlock();
		return;
	}

	// Get the rma.
	rm_ref = rm_util::get_rm();
	ASSERT(!CORBA::is_nil(rm_ref));

	// Get the service admin object.
	char *s_name = dclib::get_serviceid(service_name);

	// We make three attempts to start the service if we get the
	// service_changed exception from the HA framework. We can get
	// this exception if the service has undergone a change and that
	// means our idea of the service is not correct.

	for (int num_retries = 0; num_retries < 3; num_retries++) {
	    svc_admin_ref = rm_ref->get_control(s_name, e);
	    if (e.exception()) {
		    _service->unlock();
		    delete [] s_name;
		    _environment.exception(new mdc::dcs_error(DCS_ERR_COMM));
		    DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
			("set_active_secondaries(%p): exception '%s' while "
			"calling get_control().\n", this,
			e.exception()->_name()));
		    return;
	    }

	    // 0 denotes default value
	    svc_admin_ref->change_desired_numsecondaries(
		((num_secondaries == 0) ? DCS_DEFAULT_DESIRED_NUM_SECONDARIES :
		num_secondaries), e);
	    if ((ex = e.exception()) != NULL) {
		if ((replica::invalid_numsecs::_exnarrow(ex)) != NULL) {
		    _environment.exception(new mdc::dcs_error(DCS_ERR_NUMSEC));
		    _service->unlock();
		    delete [] s_name;
		    DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
			("set_active_secondaries(%p): exception '%s' while "
			"calling change_desired_numsecondaries().\n",
			this, ex->_name()));
		    return;
		}

		if ((replica::service_admin::service_changed::_exnarrow(ex))
						!= NULL) {
			if (num_retries == 3) {
				_environment.exception(new mdc::dcs_error(
					DCS_ERR_SERVICE_BUSY));
				_service->unlock();
				delete [] s_name;
				DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
				    ("set_active_secondaries(%p): number of "
				    "retries reached.\n", this));
				return;
			}
		}
	    } else {
		_service->unlock();
		return;
	    }
	}
}

//
// IDL entry-point to add a node to the list of nodes that could serve the
// given device service.  The operation is idempotent and needs no transactions.
//
// dc_config_impl(mdc::dc_config::add_node)
void
dc_config_impl::add_node(const char *service_name,
    const mdc::node_preference &node, Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::add_node(%s, %p).\n", service_name, node));

	dc_services *services = dc_services::get_services();

	// Verify that node id is valid.
	if (!orb_conf::node_configured(node.id)) {
		_environment.exception(new mdc::dcs_error(DCS_ERR_NODE_ID));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("add_node(%p): node not configured.\n", this));
		return;
	}

	services->rdlock();

	dc_service *_service = services->resolve_service(service_name,
	    dc_services::WRLOCKED);

	services->unlock();

	if (_service == NULL) {
		// Service with this name does not exist.
		_environment.exception(new mdc::dcs_error(
		    DCS_ERR_SERVICE_NAME));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("add_node(%p): service '%s' does not exist.\n", this,
		    service_name));
		return;
	}

	dc_error_t err_val = _service->add_node(node);
	if (err_val != DCS_SUCCESS) {
		_service->unlock();
		_environment.exception(new mdc::dcs_error(err_val));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("add_node(%p): add_node() failed.\n", this));
		return;
	}

	//
	// Publish the Device Service property change event.
	//
	char *nodename = new char [CL_MAX_LEN + 1];
	clconf_get_nodename(node.id, nodename);
	(void) sc_publish_event(
		ESC_CLUSTER_DCS_CONFIG_CHANGE,
		CL_EVENT_PUB_DCS, CL_EVENT_SEV_INFO,
		CL_EVENT_INIT_OPERATOR, 0, 0, 0,
		CL_DS_NAME, SE_DATA_TYPE_STRING,
		service_name,
		CL_CONFIG_ACTION, SE_DATA_TYPE_UINT32,
		CL_EVENT_CONFIG_PROP_CHANGED,
		CL_PROPERTY_NAME, SE_DATA_TYPE_STRING,
		"Host Added",
		CL_PROPERTY_VALUE, SE_DATA_TYPE_STRING,
		nodename,
		NULL);

	delete [] nodename;

	if (!_service->is_active()) {
		_service->notify_callbacks(_environment);
		_service->unlock();
		DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
		    ("add_node(%p): service '%s' is not active.\n", this,
		    service_name));
		return;
	}

	dsm_list_lock.rdlock();
	mdc::device_service_manager_var current_dsm = get_dsm_object(node.id);
	dsm_list_lock.unlock();

	if (CORBA::is_nil(current_dsm)) {
		_service->notify_callbacks(_environment);
		_service->unlock();
		DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
		    ("add_node(%p): no current dsm.\n", this));
		return;
	}

	Environment e;
	unsigned int dev_id = _service->get_service_id();
	mdc::dc_mapper_var mapper_obj = find_mapper(dev_id);
	_service->start_service(current_dsm, node.id, mapper_obj, e);
	_service->notify_callbacks(_environment);
	_service->unlock();

	CORBA::Exception *ex;
	if ((ex = e.release_exception()) != NULL) {
		if (!remove_dsm_if_comm_error(current_dsm, ex, _environment)) {
			ex->print_exception("Unexpected exception");
			DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
			    ("add_node(%p): unexpected exception '%s'.\n",
			    this, ex->_name()));
		}
	}
}

//
// IDL call to remove the specified node from the service.
//
// dc_config_impl(mdc::dc_config::remove_node)
void
dc_config_impl::remove_node(const char *service_name, sol::nodeid_t node,
    Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::remove_node(%s, %d).\n",
	    service_name, node));

	dc_services *services = dc_services::get_services();

	services->rdlock();

	dc_service *_service = services->resolve_service(service_name,
	    dc_services::WRLOCKED);

	services->unlock();

	if (_service == NULL) {
		// Service with this name does not exist
		_environment.exception(new mdc::dcs_error(
		    DCS_ERR_SERVICE_NAME));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("remove_node(%p): service '%s' does not exist.\n",
		    this, service_name));
		return;
	}

	// If the node specified is not in this service, return success.
	if (!_service->is_node_in_service(node)) {
		_service->unlock();
		DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
		    ("remove_node(%p): node not part of service '%s'.\n",
		    this, service_name));
		return;
	}

	dc_error_t err_val = DCS_SUCCESS;

	if (_service->is_active()) {
		dsm_list_lock.rdlock();
		mdc::device_service_manager_ptr dsmp = get_dsm_object(node);
		dsm_list_lock.unlock();

		if (CORBA::is_nil(dsmp)) {
			//
			// This machine is currently down (or hasn't completed
			// booting).  Go ahead and remove the node from the
			// configuration.
			//
			DCS_DBPRINTF_A(DCS_TRACE_DC_CONFIG,
			    ("remove_node(%p): machine is down/hasn't "
			    "completed booting.\n", this));
			goto provider_down;
		}

		Environment e;
		CORBA::Exception *ex;

		// Shutdown the replica.
		dsmp->shutdown_replica(service_name, e);
		if ((ex = e.exception()) != NULL) {
			if (CORBA::SystemException::_exnarrow(ex) != NULL) {
				//
				// System exception - the DSM, and hence the
				// node, is dead.  Go ahead and remove the
				// node from the list of replicas.
				//
				DCS_DBPRINTF_A(DCS_TRACE_DC_CONFIG,
				    ("remove_node(%p): node has died.\n",
				    this));
			} else {
				mdc::dcs_error *ex_dcs =
				    mdc::dcs_error::_exnarrow(ex);
				if (ex_dcs == NULL) {
					//
					// The error is not a system exception,
					// or of type 'dcs_error'.  Hmm....
					//
					err_val = DCS_ERR_UNEXPECTED;
					DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
					    ("remove_node(%p): unexcepted "
					    "exception '%s'.\n", this,
					    ex->_name()));
				} else {
					err_val = (dc_error_t)(ex_dcs->error);
				}
			}
			e.clear();
		}
	}

provider_down:

	if (err_val == DCS_SUCCESS) {
		err_val = _service->remove_node(node);

		if ((err_val == DCS_SUCCESS) && (_service->is_active())) {
			_service->notify_callbacks(_environment);
		}
	}

	_service->unlock();

	if (err_val != DCS_SUCCESS) {
		_environment.exception(new mdc::dcs_error(err_val));
	} else {
		DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
		    ("remove_node(%p): publishing the device service property "
		    "change.\n", this));
		//
		// Publish the Device Service property change event.
		//
		char *nodename = new char [CL_MAX_LEN + 1];
		clconf_get_nodename(node, nodename);
		(void) sc_publish_event(
			ESC_CLUSTER_DCS_CONFIG_CHANGE,
			CL_EVENT_PUB_DCS, CL_EVENT_SEV_INFO,
			CL_EVENT_INIT_OPERATOR, 0, 0, 0,
			CL_DS_NAME, SE_DATA_TYPE_STRING,
			service_name,
			CL_CONFIG_ACTION, SE_DATA_TYPE_UINT32,
			CL_EVENT_CONFIG_PROP_CHANGED,
			CL_PROPERTY_NAME, SE_DATA_TYPE_STRING,
			"Host Removed",
			CL_PROPERTY_VALUE, SE_DATA_TYPE_STRING,
			nodename,
			NULL);

		delete [] nodename;
	}
}

//
// Add the specified devices to the service.
//
// dc_config_impl(mdc::dc_config::add_devices)
void
dc_config_impl::add_devices(const char *service_name,
    const mdc::dev_range_seq_t &gdev_ranges,
    Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::add_devices(%s, %p).\n", service_name,
	    gdev_ranges));

	dc_services *services = dc_services::get_services();
	dc_service *_service;
	unsigned int service_id;
	dc_error_t err_val;
	dc_minor_range_list *dev_range_list;
	dc_minor_range_list *stored_range_list;
	dc_storage *dcstore = dc_storage::storage();

	err_val = validate_major_numbers(gdev_ranges);
	if (err_val != DCS_SUCCESS) {
		_environment.exception(new mdc::dcs_error(err_val));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("add_devices(%p): validate_major_numbers() failed.\n",
		    this));
		return;
	}

	//
	// Any change to device configuration needs the services object
	// write-locked.
	//
	services->wrlock();

	_service = services->resolve_service(service_name,
	    dc_services::WRLOCKED);

	if (_service == NULL) {
		// Service with this name does not exist
		services->unlock();
		_environment.exception(new mdc::dcs_error(
		    DCS_ERR_SERVICE_NAME));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("add_devices(%p): service '%s' does not exist.\n",
		    this, service_name));
		return;
	}

	service_id = _service->get_service_id();

	// See if the addition of these device ranges is valid.
	if ((err_val = dcstore->possible_mapping(add_mapping, gdev_ranges,
	    service_id)) != DCS_SUCCESS) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("add_devices(%p): service_id (%d) out of range.\n",
		    this, service_id));
		goto out;
	}

	//
	// Store the current device ranges before 'process_mapping' in case of
	// an error.
	//
	dev_range_list = _service->get_devices_list();
	stored_range_list = copy_range_list(dev_range_list);

	// All device ranges are valid - configure them.
	err_val = dcstore->process_mapping(add_mapping, gdev_ranges,
	    service_id, _service->get_devices_list());
	ASSERT(err_val == DCS_SUCCESS);

	// Write out to the CCR.
	if ((err_val = _service->write_devices()) != DCS_SUCCESS) {
		//
		// Error writing out the data.  Restore it to the old state.
		//
		os::warning("Error writing out service %d to the CCR.",
		    service_id);
		_service->reinit_devices(stored_range_list);
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("add_devices(%p): write_devices() failed.\n", this));
		goto out;
	}

out:
	_service->unlock();
	services->unlock();

	if (err_val != DCS_SUCCESS) {
		_environment.exception(new mdc::dcs_error(err_val));
	}
}

//
// Helper function that returns a copy of a range list.
//
dc_minor_range_list *
dc_config_impl::copy_range_list(dc_minor_range_list *old_range_list)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::copy_range_list(%p).\n", old_range_list));

	dc_minor_range_list *new_range_list = new dc_minor_range_list;
	dc_minor_range *tmp_range;

	for (old_range_list->atfirst();
	    (tmp_range = old_range_list->get_current()) != NULL;
	    old_range_list->advance()) {
		new_range_list->append(new dc_minor_range(*tmp_range));
	}

	ASSERT(new_range_list->count() == old_range_list->count());

	return (new_range_list);
}

//
// Remove the specified devices from the service.
//
// dc_config_impl(mdc::dc_config::remove_devices)
void
dc_config_impl::remove_devices(const char *service_name,
    const mdc::dev_range_seq_t &gdev_ranges,
    Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::remove_devices(%s, %p).\n", service_name,
	    gdev_ranges));

	dc_services *services = dc_services::get_services();
	dc_service *_service;
	unsigned int service_id;
	dc_error_t err_val;
	dc_minor_range_list *dev_range_list;
	dc_minor_range_list *stored_range_list;
	dc_storage *dcstore = dc_storage::storage();

	//
	// Any change to device configuration needs the services object
	// write-locked.
	//
	services->wrlock();

	_service = services->resolve_service(service_name,
	    dc_services::WRLOCKED);

	if (_service == NULL) {
		// Service with this name does not exist
		services->unlock();
		_environment.exception(new mdc::dcs_error(
		    DCS_ERR_SERVICE_NAME));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("remove_devices(%p): service '%s' does not exist.\n",
		    this, service_name));
		return;
	}

	service_id = _service->get_service_id();

	//
	// See if PXFS has registered a callback object on any of the devices
	// that we are trying to remove from the service.
	//
	if (_service->callback_registered(&gdev_ranges, _environment)) {
		err_val = DCS_ERR_DEVICE_BUSY;
		DCS_DBPRINTF_A(DCS_TRACE_DC_CONFIG,
		    ("remove_devices(%p): callback still registered.\n",
		    this));
		goto out;
	}

	// See if the removal of these device ranges is valid.
	if ((err_val = dcstore->possible_mapping(remove_mapping, gdev_ranges,
	    service_id)) != DCS_SUCCESS) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("remove_devices(%p): service_if (%d) out of range.\n",
		    this, service_id));
		goto out;
	}

	//
	// Store the current device ranges before 'process_mapping' in case of
	// an error.
	//
	dev_range_list = _service->get_devices_list();
	stored_range_list = copy_range_list(dev_range_list);

	// Remove all the device maps.
	err_val = dcstore->process_mapping(remove_mapping, gdev_ranges,
	    service_id, _service->get_devices_list());
	ASSERT(err_val == DCS_SUCCESS);

	// Write this out to the CCR.
	if ((err_val = _service->write_devices()) != DCS_SUCCESS) {
		//
		// Error writing out the data.  Restore it to the old state.
		//
		os::warning("Error writing out service %d to the CCR.",
		    service_id);
		_service->reinit_devices(stored_range_list);
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("remove_devices(%p): write_devices() failed.\n", this));
		goto out;
	}

out:
	_service->unlock();
	services->unlock();

	if (err_val != DCS_SUCCESS) {
		_environment.exception(new mdc::dcs_error(err_val));
	}
}

//
// Remove the specified service.  This call is idempotent.
//
// dc_config_impl(mdc::dc_config::remove_service)
void
dc_config_impl::remove_service(const char *service_name,
    Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::remove_service(%s).\n", service_name));

	dc_services *services = dc_services::get_services();
	dc_storage *dcstore = dc_storage::storage();
	dc_error_t err_val;

	services->wrlock();

	dc_service *_service = services->resolve_service(service_name,
	    dc_services::WRLOCKED);

	if (_service == NULL) {
		//
		// Service with this name does not exist.  Return success.
		//
		services->unlock();
		DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
		    ("remove_service(%p): service '%s' does not exist.\n",
		    this, service_name));
		return;
	}

	//
	// If the service is busy, return an error.  We make a call to
	// callback_registered() so that all inactive callbacks get purged.
	//
	if (_service->callback_registered(NULL, _environment) ||
	    _service->is_active()) {
		_service->unlock();
		services->unlock();
		_environment.exception(new mdc::dcs_error(
		    DCS_ERR_SERVICE_BUSY));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("remove_service(%p): service '%s' is still in use.\n",
		    this, service_name));
		return;
	}

	FAULTPT_DCS(FAULTNUM_DCS_REMOVE_DEVICE_SERVICE_S_B,
		FaultFunctions::generic);

	mdc::dev_range_seq_t gdev_ranges;
	_service->get_devices(gdev_ranges);
	unsigned int service_id = _service->get_service_id();

	// Try erasing the CCR table
	if ((err_val = services->erase_service(_service)) == DCS_SUCCESS) {
		//
		// Remove the device ranges from memory.
		//
		err_val = dcstore->process_mapping(remove_mapping, gdev_ranges,
		    service_id, NULL);
		ASSERT(err_val == DCS_SUCCESS);
	} else {
		_service->unlock();
	}

	FAULTPT_DCS(FAULTNUM_DCS_REMOVE_DEVICE_SERVICE_S_A,
		FaultFunctions::generic);

	services->unlock();

	if (err_val != DCS_SUCCESS) {
		_environment.exception(new mdc::dcs_error(err_val));
	}

	(void) sc_publish_event(
		ESC_CLUSTER_DCS_CONFIG_CHANGE,
		CL_EVENT_PUB_DCS, CL_EVENT_SEV_INFO,
		CL_EVENT_INIT_OPERATOR, 0, 0, 0,
		CL_DS_NAME, SE_DATA_TYPE_STRING,
		service_name,
		CL_CONFIG_ACTION, SE_DATA_TYPE_UINT32,
		CL_EVENT_CONFIG_REMOVED,
		CL_PROPERTY_NAME, SE_DATA_TYPE_STRING,
		"null",
		CL_PROPERTY_VALUE, SE_DATA_TYPE_STRING,
		"null",
		NULL);

	FAULTPT_DCS(FAULTNUM_DCS_REMOVE_DEVICE_SERVICE_S_E,
		FaultFunctions::generic);
}

//
// This idl operation is idempotent and requires no transactions
//
void
dc_config_impl::get_service_names(sol::string_seq_t_out service_names,
    Environment &)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::get_service_names().\n"));

	dc_services *services = dc_services::get_services();
	services->rdlock();
	services->get_service_names(service_names);
	services->unlock();
}

//
// This idl operation is idempotent and requires no transactions
//
// dc_config_impl(mdc::dc_config::get_service_parameters)
void
dc_config_impl::get_service_parameters(const char *service_name,
    CORBA::String_out service_class_name, bool &failback_enabled,
    mdc::node_preference_seq_t_out nodes, uint32_t &num_secondaries,
    mdc::dev_range_seq_t_out gdev_ranges,
    mdc::service_property_seq_t_out properties,
    Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::get_service_parameters(%s).\n", service_name));

	dc_services *services = dc_services::get_services();

	services->rdlock();

	dc_service *_service = services->resolve_service(service_name,
	    dc_services::RDLOCKED);

	services->unlock();

	if (_service == NULL) {
		// Service with this name does not exist
		_environment.exception(new mdc::dcs_error(
		    DCS_ERR_SERVICE_NAME));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("get_service_parameters(%p): service '%s' does not "
		    "exist.\n", this, service_name));
		return;
	}

	// Process the methods that could return errors first.
	dc_error_t err_val = _service->get_all_properties(properties);
	if (err_val != DCS_SUCCESS) {
		_service->unlock();
		_environment.exception(new mdc::dcs_error(err_val));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("get_service_parameters(%p): get_all_properties(%p) "
		    "failed.\n", this, properties));
		return;
	}

	mdc::node_preference_seq_t *service_nodes =
	    new mdc::node_preference_seq_t;
	mdc::dev_range_seq_t *service_gdev_ranges = new mdc::dev_range_seq_t;

	dc_service_class *service_class = _service->get_service_class();
	ASSERT(service_class != NULL);
	service_class_name = dclib::strdup(service_class->name);

	_service->get_nodes(*service_nodes);
	failback_enabled = _service->get_failback_mode();
	_service->get_number_of_secondaries(num_secondaries);
	_service->get_devices(*service_gdev_ranges);

	_service->unlock();

	nodes = service_nodes;
	gdev_ranges = service_gdev_ranges;

//lint -e1746 parameter 'properties' could be made const reference
}
//lint +e1746


//
// Constructor for 'reservation_lock'.
//
reservation_lock::reservation_lock(sol::nodeid_t nd, sol::incarnation_num inc,
    mdc::reservation_client_ptr resv_obj_ptr)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("reservation_lock::reservation_lock(%d, %d, %p).\n",
	    nd, inc, resv_obj_ptr));

	node = nd;
	incarnation = inc;
	resv_obj = mdc::reservation_client::_duplicate(resv_obj_ptr);
}

//
// Destructor for 'reservation_lock''.
//
reservation_lock::~reservation_lock()
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("reservation_lock::~reservation_lock().\n"));

	// Nothing to do here.
}

void
dc_config_impl::ckpt_get_resv_lock(sol::nodeid_t id,
    mdc::reservation_client_ptr resv_obj, sol::incarnation_num incarnation)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::ckpt_get_resv_lock(%d, %p, %d).\n",
	    id, resv_obj, incarnation));

	reservation_lock *tmp;

	//
	// checkpoint the granting of a lock
	// not inherently idempotent, need to check for duplicates
	//
	for (resv_locks.atfirst(); (tmp = resv_locks.get_current()) != NULL;
	    resv_locks.advance()) {
		if (resv_obj->_equiv(tmp->resv_obj)) {
			// the lock is already here
			DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
			    ("ckpt_get_resv_lock(%p): lock already "
			    "present.\n", this));
			return;
		}
	}

	// this is a new checkpoint
	reservation_lock *newlock = new reservation_lock(id, incarnation,
	    resv_obj);
	resv_locks.append(newlock);
}

void
dc_config_impl::ckpt_release_resv_lock(mdc::reservation_client_ptr resv_obj)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::ckpt_release_resv_lock(%p).\n",
	    resv_obj));

	reservation_lock *tmp;

	//
	// checkpoint the releasing of a lock
	// is idempotent, retries will just not find the lock
	//
	for (resv_locks.atfirst(); (tmp = resv_locks.get_current()) != NULL;
	    resv_locks.advance()) {
		if (resv_obj->_equiv(tmp->resv_obj)) {
			(void) resv_locks.erase(tmp);
			delete tmp;
			break;
		}
	}
}

// dc_config_impl(mdc::dc_config::get_resv_lock)
void
dc_config_impl::get_resv_lock(sol::nodeid_t id,
    mdc::reservation_client_ptr resv_obj,
    cmm::cluster_state_t &request_state, Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::get_resv_lock(%d, %p).\n", id, resv_obj));

	reservation_lock *tmp;
	bool match = true;		// request memb == lock memb?
	bool grant_lock = false;	// are we gonna grant the lock?
	bool lock_error = false;	// error encountered in server?
	cmm::control_var ctrlp;		// to get cluster state
	Environment e;
	int i;
	cmm::cluster_state_t clust_state;
	static cmm::cluster_state_t lock_membership;

	// incarnation of the node requesting the lock.
	sol::incarnation_num request_incarnation;

	resv_lock_access.wrlock();

	// make idempotent, check for duplicate request
	for (resv_locks.atfirst(); (tmp = resv_locks.get_current()) != NULL;
	    resv_locks.advance())
		if (resv_obj->_equiv(tmp->resv_obj)) {
			// the lock is already here
			DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
			    ("get_resv_lock(%p): lock already present.\n",
			    this));
			grant_lock = true;
			goto outt;
		}

	// get cluster state
	ctrlp = cmm_ns::get_control();
	if (CORBA::is_nil(ctrlp)) {
		lock_error = true;
		_environment.exception(new sol::op_e(ECANCELED));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("get_resv_lock(%p): get_control() failed.\n", this));
		goto outt;
	}
	ctrlp->get_cluster_state(clust_state, e);
	if (e.exception()) {
		e.clear();
		lock_error = true;
		_environment.exception(new sol::op_e(ECANCELED));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("get_resv_lock(%p): get_cluster_state() failed.\n",
		    this));
		goto outt;
	}

	// fill in the membership data to return to the requesting node
	request_state.local_node_id = id;
	for (i = 0; i < NODEID_MAX + 1; i++)
		request_state.members.members[i] =
		    clust_state.members.members[i];

	//
	// Remove any old locks held by this node.
	// This is done by checking for a lock with the same nodeid but
	// different incarnation number as the lock being requested.
	//
	request_incarnation = clust_state.members.members[id];
	for (resv_locks.atfirst(); (tmp = resv_locks.get_current()) != NULL;
	    resv_locks.advance()) {
		if ((tmp->node == id) &&
		    (tmp->incarnation != request_incarnation)) {
			// found an old lock, remove it
			(void) resv_locks.erase(tmp);
			get_checkpoint()->ckpt_release_resv_lock(tmp->resv_obj,
			    _environment);
			if (_environment.exception()) {
				_environment.clear();
				lock_error = true;
				_environment.exception(
				    new sol::op_e(ECANCELED));
				DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
				    ("get_resv_lock(%p): exception '%s' "
				    "while calling "
				    "ckpt_release_resv_lock().\n", this,
				    _environment.exception()->_name()));
				goto outt;
			}
			delete tmp;
		}
	}

	if (resv_locks.count() == 0) {
		// no-one else is fencing, grant lock
		DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
		    ("get_resv_lock(%p): no-one else is fencing. "
		    "Granting lock.\n", this));

		lock_membership = clust_state;
		reservation_lock *newlock = new reservation_lock(id,
		    clust_state.members.members[id], resv_obj);
		resv_locks.append(newlock);
		get_checkpoint()->ckpt_get_resv_lock(id, resv_obj,
		    clust_state.members.members[id], _environment);
		if (_environment.exception()) {
			_environment.clear();
			lock_error = true;
			_environment.exception(new sol::op_e(ECANCELED));
			DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
			    ("get_resv_lock(%): exception '%s' while calling "
			    "ckpt_get_resv_lock().\n", this,
			    _environment.exception()->_name()));
		} else
			grant_lock = true;
		goto outt;
	}

	//
	// See if the requested lock has the same membership view as
	// the held locks.
	//
	for (i = 0; i < NODEID_MAX + 1; i++)
		if (clust_state.members.members[i] !=
		    lock_membership.members.members[i])
			match = false;

	if (match) {
		// Memberships match, grant lock.
		DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
		    ("get_resv_lock(%p): memberships match. Granting lock.\n",
		    this));
		reservation_lock *newlock = new reservation_lock(id,
		    clust_state.members.members[id], resv_obj);
		resv_locks.append(newlock);
		get_checkpoint()->ckpt_get_resv_lock(id, resv_obj,
		    clust_state.members.members[id], _environment);
		if (_environment.exception()) {
			_environment.clear();
			lock_error = true;
			_environment.exception(new sol::op_e(ECANCELED));
			DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
			    ("get_resv_lock(%p): exception '%s' while calling "
			    "ckpt_get_resv_lock().\n", this,
			    _environment.exception()->_name()));
		} else
			grant_lock = true;
		goto outt;
	}

	// See if there are any stale locks being held
	for (resv_locks.atfirst(); (tmp = resv_locks.get_current()) != NULL;
	    resv_locks.advance()) {
		tmp->resv_obj->is_alive(e);
		if (e.exception()) {
			// Do all exceptions mean the node or program have died?
			e.clear();
			DCS_DBPRINTF_A(DCS_TRACE_DC_CONFIG,
			    ("get_resv_lock(%p): found a stale resv_lock.\n",
			    this));
			//
			// This function will not return unless a node is up
			// or is really down.
			//
			bool safe;
			safe = ctrlp->safe_to_takeover_from(tmp->node, true, e);
			ASSERT(safe);

			// now we can release this node's stale lock
			DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
			    ("get_resv_lock(%p): releasing stale resv_lock.\n",
			    this));
			(void) resv_locks.erase(tmp);
			get_checkpoint()->ckpt_release_resv_lock(tmp->resv_obj,
			    _environment);
			if (_environment.exception()) {
				_environment.clear();
				lock_error = true;
				_environment.exception(
				    new sol::op_e(ECANCELED));
				DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
				    ("get_resv_lock(%p): exception '%s' while "
				    "calling ckpt_release_resv_lock().\n",
				    this,
				    _environment.exception()->_name()));
				goto outt;
			}
			delete tmp;
		}
	}

	if (resv_locks.count() == 0) {
		// all locks being held were stale
		DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
		    ("get_resv_lock(%p): all locks being held were stale.\n",
		    this));
		lock_membership = clust_state;
		reservation_lock *newlock = new reservation_lock(id,
		    clust_state.members.members[id], resv_obj);
		resv_locks.append(newlock);
		get_checkpoint()->ckpt_get_resv_lock(id, resv_obj,
		    clust_state.members.members[id], _environment);
		if (_environment.exception()) {
			_environment.clear();
			lock_error = true;
			_environment.exception(new sol::op_e(ECANCELED));
			DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
			    ("get_resv_lock(%p): exception '%s' while calling "
			    "ckpt_get_resv_lock().\n", this,
			    _environment.exception()->_name()));
		} else
			grant_lock = true;
		goto outt;
	}

outt:

	if ((!lock_error) && (!grant_lock)) {
		// lock denied, client should retry
		_environment.exception(new sol::op_e(EAGAIN));
		DCS_DBPRINTF_A(DCS_TRACE_DC_CONFIG,
		    ("get_resv_lock(%p): lock denied.\n", this));
	}

	resv_lock_access.unlock();
}

// dc_config_impl(mdc::dc_config::release_resv_lock)
void
dc_config_impl::release_resv_lock(mdc::reservation_client_ptr resv_obj,
    Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::release_resv_lock(%p).\n", resv_obj));

	reservation_lock *tmp;

	resv_lock_access.wrlock();

	for (resv_locks.atfirst(); (tmp = resv_locks.get_current()) != NULL;
	    resv_locks.advance())
		if (resv_obj->_equiv(tmp->resv_obj)) {
			(void) resv_locks.erase(tmp);
			get_checkpoint()->ckpt_release_resv_lock(resv_obj,
			    _environment);
			if (_environment.exception()) {
				_environment.clear();
				_environment.exception(
				    new sol::op_e(ECANCELED));
				DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
				    ("release_resv_lock(%p): exception '%s' "
				    "while calling "
				    "ckpt_release_resv_lock().\n", this,
				    _environment.exception()->_name()));
				break;
			}
			delete tmp;
			break;
		}

	resv_lock_access.unlock();
}


//
// This idl call shuts down the specified service and marks it as such in the
// service state.
// Service shutdown works as follows:
// * This call calls shutdown() on the service admin object for the service.
// This makes it to the current primary and if successful, returns back to this
// routine without any errors.  The RM will have cleaned up all secondaries
// before returning success to this call. We now release the reference to the
// 'device_server' object that is held in the 'dc_mapper_impl' object.
// * This triggers _unreferenced on the 'device_server' object.  The device
// server handles the 'unreferenced' call by detaching itself from the provider
// and by deleting itself.  This also results in releasing the final reference
// to the 'dc_mapper' object held by the DCS, and the provider object.  Hence,
// both these 'unreferenced' notifications get scheduled.
// * The primary 'dc_mapper' object cleans up in the unreferenced notification
// and deletes itself - this causes unreferenced to be delivered to all the
// secondaries.
// * The HA framework handles the unreferenced to the provider object and
// finally calls the destructor on it.
//
void
dc_config_impl::shutdown_service(const char *service_name,
    bool suspend, Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::shutdown_service(%s, %d).\n", service_name,
	    suspend));

	dc_services *services = dc_services::get_services();
	CORBA::Exception *ex;
	replica::rm_admin_var rm_ref;
	replica::service_admin_var svc_admin_ref;
	Environment e;
	char *new_state = CL_DS_STOPPING;

	//
	// First, resolve the service name to a dc_service object.
	//
	if (service_name == NULL) {
		_environment.exception(new mdc::dcs_error(
		    DCS_ERR_SERVICE_NAME));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("shutdown_service(%p): service_name is NULL.\n",
		    this));
		return;
	}

	services->rdlock();

	dc_service *_service = services->resolve_service(service_name,
	    dc_services::RDLOCKED);

	services->unlock();

	if (_service == NULL) {
		// Service with this name does not exist
		_environment.exception(new mdc::dcs_error(
		    DCS_ERR_SERVICE_NAME));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("shutdown_service(%p): service '%s' does not exist.\n",
		    this, service_name));
		return;
	}

	char *service_class = (_service->get_service_class())->name;
	if (suspend) {
		new_state = CL_DS_MAINT_MODE_STARTING;
	}
	(void) sc_publish_event(
		ESC_CLUSTER_DCS_STATE_CHANGE,
		CL_EVENT_PUB_DCS, CL_EVENT_SEV_INFO,
		CL_EVENT_INIT_OPERATOR, 0, 0, 0,
		CL_DS_NAME, SE_DATA_TYPE_STRING,
		service_name,
		CL_DS_SERVICE_CLASS, SE_DATA_TYPE_STRING,
		service_class,
		CL_OLD_STATE, SE_DATA_TYPE_STRING,
		"",
		CL_NEW_STATE, SE_DATA_TYPE_STRING,
		new_state,
		NULL);

	_service->start_service_lock();

	dc_error_t err_val = DCS_SUCCESS;

	// If the service is not active, return success.
	if (!_service->is_active()) {
		DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
		    ("shutdown_service(%p): service '%s' is not active.\n",
		    this, service_name));
		if (suspend) {
			err_val = _service->set_suspended(true);
		}
		_service->start_service_unlock();
		_service->unlock();

		if (err_val != DCS_SUCCESS) {
			_environment.exception(new mdc::dcs_error(err_val));
			DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
			    ("shutdown_service(%p): set_suspended() failed for "
			    "service '%s'.\n", this, service_name));
		}
		if (suspend) {
			new_state = CL_DS_MAINT_MODE;
		} else {
			new_state = CL_DS_STOPPED;
		}
		(void) sc_publish_event(
			ESC_CLUSTER_DCS_STATE_CHANGE,
			CL_EVENT_PUB_DCS, CL_EVENT_SEV_INFO,
			CL_EVENT_INIT_OPERATOR, 0, 0, 0,
			CL_DS_NAME, SE_DATA_TYPE_STRING,
			service_name,
			CL_DS_SERVICE_CLASS, SE_DATA_TYPE_STRING,
			service_class,
			CL_OLD_STATE, SE_DATA_TYPE_STRING,
			"Offline",
			CL_NEW_STATE, SE_DATA_TYPE_STRING,
			new_state,
			NULL);
		return;
	}

	//
	// See if PXFS has registered a callback object on any device in the
	// service, in which case we cannot shut it down because the mount
	// server may think the service is already active.
	//
	if (_service->callback_registered(NULL, _environment)) {
		_service->start_service_unlock();
		_service->unlock();
		_environment.exception(new mdc::dcs_error(
		    DCS_ERR_SERVICE_BUSY));
		if (suspend) {
		    new_state = CL_DS_MAINT_MODE_FAILED;
		} else {
		    new_state = CL_DS_STOP_FAILED;
		}
		(void) sc_publish_event(
			ESC_CLUSTER_DCS_STATE_CHANGE,
			CL_EVENT_PUB_DCS, CL_EVENT_SEV_INFO,
			CL_EVENT_INIT_OPERATOR, 0, 0, 0,
			CL_DS_NAME, SE_DATA_TYPE_STRING,
			service_name,
			CL_DS_SERVICE_CLASS, SE_DATA_TYPE_STRING,
			service_class,
			CL_OLD_STATE, SE_DATA_TYPE_STRING,
			"Online",
			CL_NEW_STATE, SE_DATA_TYPE_STRING,
			new_state,
			NULL);
		DCS_DBPRINTF_A(DCS_TRACE_DC_CONFIG,
		    ("shutdown_service(%p): shutdown aborted because a "
		    "callback is still registered for service '%s'.\n",
		    this, service_name));
		return;
	}

	FAULTPT_DCS(FAULTNUM_DCS_SHUTDOWN_DEVICE_SERVICE_S_B,
		FaultFunctions::generic);

	// Get the rma
	rm_ref = rm_util::get_rm();
	ASSERT(!CORBA::is_nil(rm_ref));

	// Get the service admin object
	char *s_name = dclib::get_serviceid(service_name);
	svc_admin_ref = rm_ref->get_control(s_name, e);
	delete [] s_name;
	if ((ex = e.exception()) != NULL) {
		if ((replica::unknown_service::_exnarrow(ex)) != NULL) {
			DCS_DBPRINTF_A(DCS_TRACE_DC_CONFIG,
			    ("shutdown_service(%p): exception '%s' "
			    "returned by get_control() for service "
			    "'%s'.\n", this, ex->_name(), service_name));
			//
			// We could get this exception if this call is a retry.
			// Continue processing.
			//
			e.clear();
			goto post_shutdown;
		}
		_service->start_service_unlock();
		_service->unlock();
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("shutdown_service(%p): exception '%s' returned by "
		    "get_control() for service '%s'.\n",
		    this, ex->_name(), service_name));
		e.clear();
		_environment.exception(new mdc::dcs_error(DCS_ERR_COMM));
		return;
	}

	// Invoke shutdown on the service
	svc_admin_ref->shutdown_service(false, e);

	// Fault point immediately after the shutdown returns.
	FAULTPT_DCS(FAULTNUM_DCS_SHUTDOWN_DEVICE_SERVICE_S_A,
		FaultFunctions::generic);

	if ((ex = e.exception()) != NULL) {
		if ((replica::deleted_service::_exnarrow(ex)) != NULL) {
			DCS_DBPRINTF_A(DCS_TRACE_DC_CONFIG,
			    ("shutdown_service(%p): service '%s' has been "
			    "deleted inbetween.\n", this, service_name));
			//
			// Some other agent deleted this service.  While this
			// is unlikely, it is possible, so continue with the
			// processing.
			//
			e.clear();
			//
			// Lock on _service will be released in post_shutdown.
			//
		} else {
			_service->start_service_unlock();
			_service->unlock();
			ASSERT(((replica::service_busy::_exnarrow(ex))
			    != NULL) || ((replica::depends_on::_exnarrow(ex))
			    != NULL));
			DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
			    ("shutdown_service(%p): exception '%s' returned by "
			    "shutdown_service() for service '%s'.\n",
			    this, ex->_name(), service_name));
			e.clear();
			_environment.exception(new mdc::dcs_error(
			    DCS_ERR_SERVICE_BUSY));
			if (suspend) {
			    new_state = CL_DS_MAINT_MODE_FAILED;
			} else {
			    new_state = CL_DS_STOP_FAILED;
			}
			(void) sc_publish_event(
				ESC_CLUSTER_DCS_STATE_CHANGE,
				CL_EVENT_PUB_DCS, CL_EVENT_SEV_INFO,
				CL_EVENT_INIT_OPERATOR, 0, 0, 0,
				CL_DS_NAME, SE_DATA_TYPE_STRING,
				service_name,
				CL_DS_SERVICE_CLASS, SE_DATA_TYPE_STRING,
				service_class,
				CL_OLD_STATE, SE_DATA_TYPE_STRING,
				"Online",
				CL_NEW_STATE, SE_DATA_TYPE_STRING,
				new_state,
				NULL);
			return;
		}
	}

post_shutdown:

	dc_mapper_impl *mapperp;
	unsigned int devid;

	_service->set_active(false);
	if (suspend) {
		err_val = _service->set_suspended(true);
	}
	devid = _service->get_service_id();

	//
	// Go through the list, and invalidate the 'dc_mapper' entry for this
	// device service.
	//
	list_lock.wrlock();
	for (mapper_list.atfirst();
	    (mapperp = mapper_list.get_current()) != NULL;
	    mapper_list.advance()) {
		if (mapperp->equals_devid(devid)) {
			//
			// Release the device_server reference from this mapper.
			// We can't remove the mapper object from the list
			// because we still need to checkpoint it to joining
			// secondaries.  See BugId 4298951.
			//
			mapperp->set_devobj(mdc::device_server::_nil());
			mapperp->set_devid(0);

			// Do the same on the secondaries.
			mdc::dc_mapper_var dc_mapper_obj =
			    mapperp->get_objref();
			get_checkpoint()->ckpt_set_dc_mapper(dc_mapper_obj, 0,
			    mdc::device_server::_nil(), _environment);
			// Nothing to do if there was an exception.
			_environment.clear();
			break;
		}
	}
	list_lock.unlock();

	_service->start_service_unlock();
	_service->unlock();

	if (err_val != DCS_SUCCESS) {
		_environment.exception(new mdc::dcs_error(err_val));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("shutdown_service(%p): set_suspended() failed for "
		    "service '%s'.\n", this, service_name));
	} else {
		if (suspend) {
			new_state = CL_DS_MAINT_MODE;
		} else {
			new_state = CL_DS_STOPPED;
		}
		(void) sc_publish_event(
			ESC_CLUSTER_DCS_STATE_CHANGE,
			CL_EVENT_PUB_DCS, CL_EVENT_SEV_INFO,
			CL_EVENT_INIT_OPERATOR, 0, 0, 0,
			CL_DS_NAME, SE_DATA_TYPE_STRING,
			service_name,
			CL_DS_SERVICE_CLASS, SE_DATA_TYPE_STRING,
			service_class,
			CL_OLD_STATE, SE_DATA_TYPE_STRING,
			"Online",
			CL_NEW_STATE, SE_DATA_TYPE_STRING,
			new_state,
			NULL);
	}

	FAULTPT_DCS(FAULTNUM_DCS_SHUTDOWN_DEVICE_SERVICE_S_E,
		FaultFunctions::generic);

}

//
// Called on the secondary from shutdown_service() on the primary.
//
void
dc_config_impl::ckpt_remove_dc_mapper(mdc::dc_mapper_ptr mapper_obj)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::ckpt_remove_dc_mapper(%p).\n", mapper_obj));

	dc_mapper_impl *mapperp =
	    dc_mapper_impl::get_dc_mapper_impl(mapper_obj);

	(void) mapper_list.erase(mapperp);
	mapperp->release_device_server();
}

//
// This idl entry point un-suspends a previously suspended service.
//
// dc_config_impl(mdc::dc_config::create_device_service)
void
dc_config_impl::resume_service(const char *service_name,
    Environment &env)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::resume_service(%s).\n", service_name));

	dc_services *services = dc_services::get_services();

	//
	// First, resolve the service name to a dc_service object.
	//
	if (service_name == NULL) {
		env.exception(new mdc::dcs_error(DCS_ERR_SERVICE_NAME));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("resume_service(%p): service_name is NULL.\n"));
		return;
	}

	services->rdlock();

	dc_service *_service = services->resolve_service(service_name,
	    dc_services::RDLOCKED);

	services->unlock();

	if (_service == NULL) {
		// Service with this name does not exist
		env.exception(new mdc::dcs_error(DCS_ERR_SERVICE_NAME));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("resume_service(%p): service '%s' does not exist.\n",
		    this, service_name));
		return;
	}

	_service->start_service_lock();
	dc_error_t err_val = _service->set_suspended(false);
	_service->start_service_unlock();

	_service->unlock();

	if (err_val != DCS_SUCCESS) {
		env.exception(new mdc::dcs_error(err_val));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("resume_service(%p): set_suspended() failed for "
		    "service '%s'.\n", this, service_name));
	}
}

//
// IDL entry-point invoked on the DCS by DSM's to switchback services if
// appropriate.
//
void
dc_config_impl::do_switchback(const char *service_name, sol::nodeid_t dsm_id,
    Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::do_switchback(%s, %d).\n",
	    service_name, dsm_id));


	if (!safe_to_switchover(_environment)) {
		DCS_DBPRINTF_A(DCS_TRACE_DC_CONFIG,
		    ("do_switchback(%p): not safe to switchover for service "
		    "'%s'.\n", this, service_name));
		return;
	}

	if (_freeze_in_progress(&_environment)) {
		notify_switchover_end();
		DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
		    ("do_switchback(%p): freeze in progress for service "
		    "'%s'.\n", this, service_name));
		return;
	}

	dc_services *services = dc_services::get_services();

	//
	// First, resolve the service name to a dc_service object.
	//
	if (service_name == NULL) {
		_environment.exception(new mdc::dcs_error(
		    DCS_ERR_SERVICE_NAME));
		notify_switchover_end();
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("do_switchback(%p): service_name is NULL.\n", this));
		return;
	}

	services->rdlock();
	//
	// Fault while a lock is held.  Does deadlock occur?
	//
	FAULTPT_DCS(FAULTNUM_DCS_DO_SWITCHBACK_S_B,
		FaultFunctions::generic);

	dc_service *_service = services->resolve_service(service_name,
	    dc_services::RDLOCKED);
	services->unlock();

	if (_service == NULL) {
		// Service with this name does not exist
		_environment.exception(new mdc::dcs_error(
		    DCS_ERR_SERVICE_NAME));
		notify_switchover_end();
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("do_switchback(%p): service '%s' does not exist.\n",
		    this, service_name));
		return;
	}

	_service->start_service_lock();
	do_switchover(_service, dsm_id, true, _environment);
	_service->start_service_unlock();
	_service->unlock();

	notify_switchover_end();
}

//
// Helper function to do switchovers/switchbacks of a particular device service.
// The service should be already activewhen this function is called.
//
void
dc_config_impl::do_switchover(dc_service *_service,
    sol::nodeid_t dsm_id, bool switchback, Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::do_switchover(%p, %d, %d).\n",
	    _service, dsm_id, switchback));

	MC_PROBE_0(dc_switchover_start, "clustering pxfs", "");

	char *new_state = CL_DS_SWITCHOVER_STARTED;
	char *service_name;
	_service->get_service_name(&service_name);

	char *service_class = (_service->get_service_class())->name;

	(void) sc_publish_event(
		ESC_CLUSTER_DCS_STATE_CHANGE,
		CL_EVENT_PUB_DCS, CL_EVENT_SEV_INFO,
		CL_EVENT_INIT_OPERATOR, 0, 0, 0,
		CL_DS_NAME, SE_DATA_TYPE_STRING,
		service_name,
		CL_DS_SERVICE_CLASS, SE_DATA_TYPE_STRING,
		service_class,
		CL_OLD_STATE, SE_DATA_TYPE_STRING,
		"Online",
		CL_NEW_STATE, SE_DATA_TYPE_STRING,
		new_state,
		CL_TIMESTAMP, SE_DATA_TYPE_TIME,
		os::gethrtime(),
		NULL);


	ASSERT(_service->lock_held());

	sol::nodeid_t id;
	uint_t i, len;
	replica::rm_admin_var rm_ref;
	char *s_id;
	replica::service_admin_var s_admin;
	uint_t dsm_priority = 0, priority = 0;
	Environment env;
	char p_id[dclib::NODEID_STRLEN];
	replica::repl_prov_seq_var repl_provs;

	// Get a handle to the RM.
	rm_ref = rm_util::get_rm();
	ASSERT(!CORBA::is_nil(rm_ref));

s_admin_service_changed_retry:
	// Get the service admin for this service.
	s_id = dclib::get_serviceid(service_name);
	s_admin = rm_ref->get_control(s_id, env);
	delete [] s_id;
	if (env.exception()) {
		// Service is not active - return.
		env.clear();
		_environment.exception(new mdc::dcs_error(
		    DCS_ERR_INVALID_STATE));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("do_switchover(%p): service '%s' is not active.\n",
		    this, service_name));
		delete [] service_name;
		return;
	}

	// Get the list of replica providers for this service.
	s_admin->get_repl_provs(repl_provs, env);
	if (env.exception()) {
		env.clear();
		_environment.exception(new mdc::dcs_error(
		    DCS_ERR_INVALID_STATE));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("do_switchover(%p): node has died for service '%s'.\n",
		    this, service_name));
		delete [] service_name;
		return;
	}
	len = repl_provs.length();

	FAULTPT_DCS(FAULTNUM_DCS_DO_SWITCHBACK_S_A,
		FaultFunctions::generic);

	if (dsm_id == 0) {
		ASSERT(!switchback);
		// Look for the secondary with the highest priority.
		for (i = 0; i < len; i++) {
			if (repl_provs[i].curr_state == replica::AS_SECONDARY) {
				id = (uint_t)os::atoi(
				    repl_provs[i].repl_prov_desc);
				ASSERT(id > 0);
				priority = _service->get_priority(id);
				// A smaller numerical value is interpreted as
				// higher priority.
				if ((dsm_id == 0) ||
				    (priority < dsm_priority)) {
					dsm_id = id;
					dsm_priority = priority;
				}
			}
		}
		if (dsm_id == 0) {
			//
			// Failed to find any secondary to switchover to.
			// Return an error to the user.
			//
			_environment.exception(new mdc::dcs_error(
			    DCS_ERR_INVALID_STATE));
			DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
			    ("do_switchover(%p): failed to find any "
			    "secondary to switchover to for service "
			    "'%s'.\n", this, service_name));
			delete [] service_name;
			return;
		}
	}

	ASSERT(dsm_id != 0);

	if (switchback) {
		// Look for the current primary.
		for (i = 0; i < len; i++) {
			if (repl_provs[i].curr_state == replica::AS_PRIMARY) {
				id = (uint_t)os::atoi(
				    repl_provs[i].repl_prov_desc);
				if (id == dsm_id) {
					//
					// 'dsm_id' is the current primary - the
					// switchover is unnecessary.
					//
					DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
					    ("do_switchover(%p): no "
					    "switchover necessary for "
					    "service '%s'.\n",
					    this, service_name));
					delete [] service_name;
					return;
				} else {
					//
					// Get the priority of the current
					// primary.
					//
					priority = _service->get_priority(id);
					break;
				}
			}
		}

		if (i > len) {
			// There is no primary - exit.
			_environment.exception(new mdc::dcs_error(
			    DCS_ERR_INVALID_STATE));
			DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
			    ("do_switchover(%p): no primary found for "
			    "service '%s'.\n", this, service_name));
			delete [] service_name;
			return;
		}

		// Is the priority of 'dsm_id' higher than the current primary ?
		dsm_priority = _service->get_priority(dsm_id);
		// XXX Changed this to take care of new priorities.
		if (dsm_priority >= priority) {
			//
			// The priority is lower than or equal to that of the
			// current primary.
			//
			DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
			    ("do_switchover(%p): switchover denied for "
			    "service '%s': priority is lower than "
			    "current primary.\n", this, service_name));
			delete [] service_name;
			return;
		}
	}

	// Change the provider for node 'dsm_id' to the primary state.
	(void) sprintf(p_id, "%d", dsm_id);
	s_admin->change_repl_prov_status((const char *)p_id,
	    replica::SC_SET_PRIMARY, false, env);
	CORBA::Exception *ex;
	if ((ex = env.exception()) != NULL) {
	    if (replica::service_admin::service_changed::_exnarrow(ex)
		    != NULL) {
		    env.clear();
		    DCS_DBPRINTF_A(DCS_TRACE_DC_CONFIG,
			("do_switchover(%p): 'service_changed' exception for "
			"service '%s'; retrying...\n", this, service_name));
		    goto s_admin_service_changed_retry;
	    }
	    DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		("do_switchover(%p): exception '%s' while calling "
		"change_repl_prov_status() for service '%s'.\n",
		this, ex->_name(), service_name));
	    if ((replica::failed_to_switchover::_exnarrow(ex) != NULL) ||
		(replica::invalid_repl_prov::_exnarrow(ex) != NULL)) {
		    env.clear();
		    _environment.exception(new mdc::dcs_error(
			DCS_ERR_REPLICA_FAILED));
	    } else {
		    //
		    // The switchover failed (maybe the calling node is dead?).
		    // Just return - errors should have been logged by the node
		    // if necessary.
		    //
		    env.clear();
		    _environment.exception(new mdc::dcs_error(
			DCS_ERR_INVALID_STATE));
	    }

		os::sc_syslog_msg *msg = NULL;

		msg = new os::sc_syslog_msg(SC_SYSLOG_DCS_DEVICE_SERVICE_TAG,
		    "DSM", NULL);
		//
		// SCMSGS
		// @explanation
		// There were problems while trying make this node primary
		// for this service.
		// @user_action
		// Retry the last service switchover operation.
		//
		(void) msg->log(SC_SYSLOG_WARNING, MESSAGE,
		    "Failed to set this node as primary for service '%s'.",
		    service_name);
		delete msg;

		new_state = CL_DS_SWITCHOVER_FAILED;
		(void) sc_publish_event(
		    ESC_CLUSTER_DCS_STATE_CHANGE,
		    CL_EVENT_PUB_DCS, CL_EVENT_SEV_CRITICAL,
		    CL_EVENT_INIT_OPERATOR, 0, 0, 0,
		    CL_DS_NAME, SE_DATA_TYPE_STRING,
		    service_name,
		    CL_DS_SERVICE_CLASS, SE_DATA_TYPE_STRING,
		    service_class,
		    CL_OLD_STATE, SE_DATA_TYPE_STRING,
		    "Online",
		    CL_NEW_STATE, SE_DATA_TYPE_STRING,
		    new_state,
		    CL_TIMESTAMP, SE_DATA_TYPE_TIME,
		    os::gethrtime(),
		    NULL);
		delete [] service_name;
		return;
	}

	new_state = CL_DS_SWITCHOVER_ENDED;
	(void) sc_publish_event(
		ESC_CLUSTER_DCS_STATE_CHANGE,
		CL_EVENT_PUB_DCS, CL_EVENT_SEV_INFO,
		CL_EVENT_INIT_OPERATOR, 0, 0, 0,
		CL_DS_NAME, SE_DATA_TYPE_STRING,
		service_name,
		CL_DS_SERVICE_CLASS, SE_DATA_TYPE_STRING,
		service_class,
		CL_OLD_STATE, SE_DATA_TYPE_STRING,
		"Online",
		CL_NEW_STATE, SE_DATA_TYPE_STRING,
		new_state,
		CL_TIMESTAMP, SE_DATA_TYPE_TIME,
		os::gethrtime(),
		NULL);

	delete [] service_name;

	FAULTPT_DCS(FAULTNUM_DCS_DO_SWITCHBACK_S_E,
		FaultFunctions::generic);

	MC_PROBE_0(dc_switchover_end, "clustering pxfs", "");
}

//
// Given a sequence of major numbers, fill in the corresponding major names
// for the major numbers in 'major_numbers'.
//
dc_error_t
dc_config_impl::fill_in_major_names(mdc::long_seq_t &major_numbers,
    sol::string_seq_t &major_names)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::fill_in_major_names(%p).\n", major_numbers));

	for (uint_t i = 0; i < major_numbers.length(); i++) {
		major_names[i] = (const char *)ddi_major_to_name(
		    major_numbers[i]);
		if (major_names[i] == (const char *)NULL) {
			DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
			    ("fill_in_major_names(%p): wrong major number "
			    "found.\n", this));
			return (DCS_ERR_MAJOR_NUM);
		}
	}

	return (DCS_SUCCESS);
}

//
// Check whether all the major numbers the DCS cares about are consistent
// with the DSM passed in to this function.
//
dc_error_t
dc_config_impl::validate_dsm_major_numbers(mdc::device_service_manager_ptr dsm)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::validate_dsm_major_numbers(%p).\n", dsm));

	dc_error_t err_val = DCS_SUCCESS;
	dc_storage *dcstore = dc_storage::storage();
	mdc::long_seq_t major_numbers;
	sol::string_seq_t *major_names;
	Environment env;
	CORBA::Exception *ex;
	uint_t i;

	// Get all the major numbers we care about from the store.
	dcstore->get_unique_major_numbers(major_numbers);
	if (major_numbers.length() == 0) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("validate_dsm_major_numbers(%p): no major numbers.\n",
		    this));
		return (DCS_SUCCESS);
	}

	// Get the major names for all the major numbers.
	major_names = new sol::string_seq_t(major_numbers.length(),
	    major_numbers.length());
	err_val = fill_in_major_names(major_numbers, *major_names);
	if (err_val != DCS_SUCCESS) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("validate_dsm_major_numbers(%p): fill_in_major_names() "
		    "failed.\n", this));
		goto out;
	}

	for (i = 0; i < major_numbers.length(); i++) {
		DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
		    ("validate_dsm_major_numbers(%p): %d %s <--> %d\n", this,
		    i, (const char *)((*major_names)[i]),
		    (int)(major_numbers[i])));
	}

	// Ask the DSM to confirm the mappings on its node.
	dsm->confirm_major_names(*major_names, major_numbers, env);
	if ((ex = env.exception()) != NULL) {
		if (CORBA::SystemException::_exnarrow(ex) == NULL) {
			err_val = DCS_ERR_MAJOR_NUM;
			DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
			    ("validate_dsm_major_numbers(%p): "
			    "confirm_major_names() failed.\n", this));
		}
		env.clear();
	}

out:
	delete major_names;
	return (err_val);
}

//
// Given a sequence of dev_t ranges, confirm that all the major numbers in this
// sequence that the DCS server hasn't seen before, map to the same driver name
// on all cluster nodes.
//
dc_error_t
dc_config_impl::validate_major_numbers(const mdc::dev_range_seq_t &gdev_ranges)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::validate_major_numbers(%p).\n", gdev_ranges));

	dsm_info_t *dsm_infop;
	IntrList<dsm_info_t, _SList>::ListIterator *iter;
	Environment env;
	CORBA::Exception *ex;
	dc_error_t err_val = DCS_SUCCESS;
	unsigned int gdev_range_len = gdev_ranges.length();
	dc_storage *dcstore = dc_storage::storage();

	mdc::long_seq_t *major_numbers = new mdc::long_seq_t(gdev_range_len);
	sol::string_seq_t *major_names = NULL;
	uint_t curr = 0;

	major_t maj;
	uint_t i, j;

	// Get the list of new, unique major numbers in 'major_numbers'.
	for (i = 0; i < gdev_range_len; i++) {
		maj = gdev_ranges[i].maj;
		if (dcstore->exists_major_number(maj)) {
			continue;
		}
		for (j = 0; j < curr; j++) {
			if ((*major_numbers)[j] == maj) {
				break;
			}
		}
		if (j == curr) {
			(*major_numbers)[curr++] = maj;
		}
	}

	if (curr == 0) {
		goto out;
	}

	//
	// Set the length of 'major_numbers' to the current number of filled-in
	// entries.
	//
	major_numbers->length(curr);

	// Get the driver names for all the major numbers.
	major_names = new sol::string_seq_t(curr, curr);
	err_val = fill_in_major_names(*major_numbers, *major_names);
	if (err_val != DCS_SUCCESS) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("validate_major_numbers(%p): fill_in_major_names() "
		    "failed.\n", this));
		goto out;
	}

	for (i = 0; i < major_numbers->length(); i++) {
		DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
		    ("validate_major_numbers(%p): %d %s <--> %d\n", this,
		    i, (const char *)((*major_names)[i]),
		    (int)((*major_numbers)[i])));
	}

	//
	// Now, walk through the list of DSMs, checking that these new (major
	// number <--> major name) mappings are the same on all nodes.
	//
	dsm_list_lock.rdlock();

	iter = new IntrList<dsm_info_t, _SList>::ListIterator(dsm_list);
	for (; (dsm_infop = iter->get_current()) != NULL; iter->advance()) {
		// Skip this node.
		if (dsm_infop->node_id == orb_conf::node_number()) {
			continue;
		}
		dsm_infop->dsm->confirm_major_names(*major_names,
		    *major_numbers, env);
		if ((ex = env.exception()) != NULL) {
			if (CORBA::SystemException::_exnarrow(ex) != NULL) {
				env.clear();
				DCS_DBPRINTF_A(DCS_TRACE_DC_CONFIG,
				    ("validate_major_numbers(%p): system "
				    "exception '%s' while calling "
				    "confirm_major_names().\n", this,
				    ex->_name()));
				continue;
			}
			DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
			    ("validate_major_numbers(%p): confirm_major_names()"
			    " failed.\n", this));
			env.clear();
			err_val = DCS_ERR_MAJOR_NUM;
			break;
		}
	}

	dsm_list_lock.unlock();

	delete iter;

out:
	delete major_numbers;
	delete major_names;
	return (err_val);
}

//
// Return the device_server object given a device server id.
//
mdc::device_server_ptr
dc_config_impl::find_devobj(const unsigned int devid)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::find_devobj(%d).\n", devid));

	dc_mapper_impl *mapperp;
	mdc::device_server_ptr devserver = mdc::device_server::_nil();

	list_lock.rdlock();

	dc_mapper_list::ListIterator iter(mapper_list);
	for (; (mapperp = iter.get_current()) != NULL; iter.advance()) {
		if (mapperp->equals_devid(devid)) {
			// Found it.
			devserver = mapperp->get_devobj();
			DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
			    ("find_devobj(%p, %d): devserver = %p.\n",
			    this, devid, devserver));
			break;
		}
	}

	list_lock.unlock();
	return (devserver);
}

//
// Return the dc_mapper object given a device server id.
//
mdc::dc_mapper_ptr
dc_config_impl::find_mapper(unsigned int devid)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::find_mapper(%d).\n", devid));

	dc_mapper_impl *mapperp;
	mdc::dc_mapper_ptr mapper_obj = mdc::dc_mapper::_nil();

	list_lock.rdlock();

	dc_mapper_list::ListIterator iter(mapper_list);
	for (; (mapperp = iter.get_current()) != NULL; iter.advance()) {
		if (mapperp->equals_devid(devid)) {
			// Found it.
			mapper_obj = mapperp->get_objref();
			DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
			    ("find_mapper(%p, %d): mapper_obj = %p.\n",
			    this, devid, mapper_obj));
			break;
		}
	}

	list_lock.unlock();
	return (mapper_obj);
}

//
// Grab the lock that protects the server and mapper lists.
//
void
dc_config_impl::lock_list()
{
	list_lock.wrlock();
}

//
// Release the lock that protects the server and mapper lists.
//
void
dc_config_impl::unlock_list()
{
	list_lock.unlock();
}

//
// Remove the mapper from the list of mappers
//
void
dc_config_impl::remove_mapper(dc_mapper_impl *mapperp)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::remove_mapper(%p).\n", mapperp));

	ASSERT(list_lock.lock_held());
	(void) mapper_list.erase(mapperp);
}

//
// Check whether dc_service is suspended.
//
void
dc_config_impl::is_suspended(const char *service_name, bool &suspended,
	Environment &_env)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::is_suspended(%s).\n", service_name));

	dc_services *services;
	dc_service *_service;

	services = dc_services::get_services();
	services->rdlock();
	_service = services->resolve_service(service_name,
	    dc_services::RDLOCKED);
	services->unlock();

	if (_service == NULL) {
		// Service under this name does not exist.
		// We don't need to report error in this case since
		// it will be taken care by caller of this function.
		_env.clear();
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("is_suspended(%p): service '%s' does not exist.\n",
		    this, service_name));
		return;
	}
	_service->start_service_lock();
	suspended = _service->is_suspended();
	_service->start_service_unlock();
	_service->unlock();
}

// Checkpoint accessor function.
repl_dc::repl_dc_ckpt_ptr
dc_config_impl::get_checkpoint()
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::get_checkpoint().\n"));

	return ((dc_repl_server*)(get_provider()))->get_checkpoint_repl_dc();
}


//
//
// Set the "active" flag of a service to false. This method is invoked
// by device_replica_impl::shutdown_spare(). Setting a flag of a dead
// service to false ensures that when the primary comes back up again,
// a new incarnation of the service is started, and all the providers,
// especially the spares, become part of the service.
// resolve_service_nolocks() is used to get a service without acquiring
// any lock on the corresponding dc_service object. This is done to
// ensure that we do not run into any deadlocks if the framework starts to
// cleanup the service and calls shutdown_spare() while the service was
// in use (e.g, being shutdown, switched over etc.). It will result in a
// callback into the DCS and could result in a deadlock. This can happen,
// for example, when a service is being switched over and the node to become
// the new primary dies between the time when we call change_repl_prov_status
// and when the framework starts to change the status.
//
void
dc_config_impl::set_service_inactive(const char *service_name,
		Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::set_service_inactive(%s).\n", service_name));

	dc_services *services = dc_services::get_services();
	services->rdlock();
	dc_service *_service = services->resolve_service_nolocks(
			service_name);
	services->unlock();
	if (_service == NULL) {
		// Service with this name does not exist
		_environment.exception(new mdc::dcs_error(
			DCS_ERR_SERVICE_NAME));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("set_service_inactive(%p): service '%s' does not exist.\n",
		    this, service_name));
		return;
	}

	_service->set_active(false);
}

//
// Set the "active" flag of a service to true. This method is invoked
// by device_service_mgr::start_service_worker_thread().
// Note that we set the service to be active after it is being started
// by DSM. If a spare is being shutdown and at the same time another
// node is trying to become part of the service, it will have to wait
// for the replica framework to finish its cleanup work. When the replica
// framework makes it the primary, the service would have been set to
// false. To avoid this situation, we set the service to be active again.
//
void
dc_config_impl::set_service_active(const char *service_name,
		    Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::set_service_active(%s).\n", service_name));

	dc_services *services = dc_services::get_services();
	services->rdlock();
	dc_service *_service = services->resolve_service_nolocks(
		    service_name);
	services->unlock();
	if (_service == NULL) {
		// Service with this name does not exist
		_environment.exception(new mdc::dcs_error(
		    DCS_ERR_SERVICE_NAME));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("set_service_active(%p): service '%s' does not exist.\n",
		    this, service_name));
		return;
	}

	_service->set_active(true);
}

//
// Changes priorities of the given list of nodes. This call only informs
// the replica framework of the change in priorities. Node priorities for
// this service in the dc_service object and the CCR are changed by the
// add_node call.
//
void
dc_config_impl::change_nodes_priority(const char *service_name,
	const mdc::node_preference_seq_t &nodes_seq,
	Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::change_nodes_priority(%s, %p).\n",
	    service_name, nodes_seq));

	char node_id_str[dclib::NODEID_STRLEN];
	uint_t i;
	replica::prov_priority_list *prov_list;

	dc_services *services = dc_services::get_services();
	services->rdlock();

	dc_service *_service = services->resolve_service(service_name,
	    dc_services::RDLOCKED);

	services->unlock();

	if (_service == NULL) {
		_environment.exception(new mdc::dcs_error(
		    DCS_ERR_SERVICE_NAME));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("change_nodes_priority(%p): service '%s' does not "
		    "exist.\n", this, service_name));
		return;
	}

	if (!_service->is_active()) {
		_service->unlock();
		// If the service is inactive we simply return. This is
		// because we have already written the new priorities to
		// the CCR via the add_node call.
		DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
		    ("change_nodes_priority(%p): service '%s' is inactive.\n",
		    this, service_name));
		return;
	}

	replica::rm_admin_var rm_ref = rm_util::get_rm();
	if (CORBA::is_nil(rm_ref)) {
		_service->unlock();
		_environment.exception(new mdc::dcs_error(
			DCS_ERR_NOT_CLUSTER));
		DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		    ("change_nodes_priority(%p): get_rm() failed for "
		    "service '%s'.\n", this, service_name));
		return;
	}

	// Get a handle to the service admin. for this service.
	Environment e;
	char *s_id = dclib::get_serviceid(service_name);
	replica::service_admin_var s_admin = rm_ref->get_control(s_id, e);
	delete [] s_id;
	if (e.exception()) {
	    e.clear();
	    _service->unlock();
	    _environment.exception(new mdc::dcs_error(
			DCS_ERR_NOT_CLUSTER));
	    DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
		("change_nodes_priority(%p): get_control() failed for "
		"service '%s'.\n", this, service_name));
	    return;
	}

	// Convert from dc_replica_t to replica::prov_priority_list.
	prov_list = new replica::prov_priority_list(nodes_seq.length());
	prov_list->length(nodes_seq.length());
	for (i = 0; i < nodes_seq.length(); i++) {
		(*prov_list)[i].priority = nodes_seq[i].preference;
		os::sprintf(node_id_str, "%d", nodes_seq[i].id);
		(*prov_list)[i].prov_desc = os::strdup(node_id_str);

	}

	CORBA::Exception *ex;
	s_admin->change_repl_prov_priority(*prov_list, e);
	// If at all we get an exception from the call above, it should
	// never be other than replica::deleted_service. The only other
	// exception that change_repl_prov_priority raises is the
	// invalid_repl_prov exception. That happens when, say, a repl
	// prov name was a null string. We expect libscconf to give us
	// valid strings.
	if ((ex = e.exception()) != NULL) {
	    e.clear();
	    ASSERT((replica::deleted_service::_exnarrow(ex)) == NULL);
	}

	_service->unlock();
}

//
// This is called on the DCS primary when a node joins the cluster
// and the Replica Framework tries to freeze the primary so that a
// new provider could be added. Before calling freeze_primary(), the
// framework calls freeze_primary_prepare(), which calls this method.
// This method does not return until all in-progress switchovers have
// completed.
//
void
dc_config_impl::prepare_for_freeze()
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::prepare_for_freeze().\n"));

	freeze_prepare_lock.lock();
	freeze_prepare_request = true;
	while (switchovers_in_progress > 0) {
		DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
		    ("prepare_for_freeze(%p): %ld switchovers in progress.\n",
		    this, switchovers_in_progress));
		freeze_prepare_cv.wait(&freeze_prepare_lock);
	}
	freeze_prepare_lock.unlock();
}

//
// This is called from dc_repl_server::unfreeze_primary() to indicate
// that DCS has been unfozen. This resets freeze_prepare_request to
// false, so that switchovers can proceed.
//
void
dc_config_impl::reset_freeze_prepare_request()
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::reset_freeze_prepare_request().\n"));
	freeze_prepare_lock.lock();
	freeze_prepare_request = false;
	freeze_prepare_lock.unlock();
}

//
// Called when a switchover request arrives at DCS. This could be an
// explicit switchover request, made using scswitch command, or could be
// an implicit one, for a service that has it 'switchback' property
// set to 'true'.
//
bool
dc_config_impl::safe_to_switchover(Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::safe_to_switchover().\n"));

	freeze_prepare_lock.lock();
	if (freeze_prepare_request) {
		_environment.exception(new mdc::dcs_error(
			DCS_FREEZE_IN_PROGRESS));
		freeze_prepare_lock.unlock();
		DCS_DBPRINTF_A(DCS_TRACE_DC_CONFIG,
		    ("safe_to_switchover(%p): freeze in progress.\n",
		    this));
		return (false);
	} else {
		switchovers_in_progress++;
		freeze_prepare_lock.unlock();
	}

	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("safe_to_switchover(%p): %ld switchovers in progress.\n",
	    this, switchovers_in_progress));
	return (true);
}

//
// This is called when a switchover has completed. It decrements the count
// of in-progress switchovers and notifies any waiting freeze requests so
// that they can make progress.
//
void
dc_config_impl::notify_switchover_end()
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_config_impl::notify_switchover_end().\n"));
	freeze_prepare_lock.lock();
	switchovers_in_progress--;
	if (switchovers_in_progress == 0) {
		DCS_DBPRINTF_G(DCS_TRACE_DC_CONFIG,
		    ("notify_switchover_end(%p): all in-progress switchovers "
		    "done.\n", this));
		freeze_prepare_cv.broadcast();
	}
	freeze_prepare_lock.unlock();
}

//
// Start up the dcs. The members coresponding to server side are
// initialised in dcops vector (used beween kernel and dcs).
// Called during modload time.
//
extern "C" void
dc_startup()
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_startup(): start.\n"));

	dc_repl_server::initialize();

	DCS_DBPRINTF_W(DCS_TRACE_DC_CONFIG,
	    ("dc_startup(): end.\n"));
}

//
// Call svm_syncnodes on all nodes.
//
#if SOL_VERSION > __s10
void
dc_config_impl::create_device_nodes(sol::major_t maj, sol::minor_t _min,
    mdc::service_property_seq_t_out disk_list, const char *device_name,
    Environment &_environment)
#else
void
dc_config_impl::create_device_nodes(sol::major_t maj, sol::minor_t _min,
    mdc::service_property_seq_t_out disk_list, Environment &_environment)
#endif
{
	Environment env;
	sol::nodeid_t n;
	bool found;
	unsigned int device_id;
	char prop_name[8];
	CORBA::String_var device_property;
#if SOL_VERSION <= __s10
	const char *device_name = "";
#else
	bool make_links = false;
#endif

	dc_storage *dcstore = dc_storage::storage();
	dc_services *services = dc_services::get_services();

	// Lock down services to prevent deletions.
	services->rdlock();

	// Find the service that this device is in.
	found = dcstore->get_map(maj, _min, &device_id);
	if (!found) {
		services->unlock();
		_environment.exception(new mdc::dcs_error(
		    DCS_ERR_DEVICE_INVAL));
		return;
	}
	// Get the service with id "device_id"
	dc_service *_service = services->resolve_service_from_id(
	    device_id, dc_services::WRLOCKED);
	services->unlock();

	ASSERT(_service != NULL);

	// name the property indicating existance of the metadevice
	// d_{metadevice number}
	(void) sprintf(prop_name, "d_%d", _min % SDSSC_METADDEV_MAX);

	// Check to see if the metadevice exists in the CCR. If it
	// does not, create links on all nodes, then add the property to
	// the CCR.
#if SOL_VERSION > __s10
	dc_error_t err_val = _service->test_and_register_name(prop_name,
	    device_name, &make_links);
	if ((err_val != DCS_SUCCESS) || make_links)
#else
	dc_error_t err_val = _service->get_property(prop_name,
	    device_property);
	if (err_val == DCS_ERR_PROPERTY_NAME)
#endif
	{
		for (n = 1; n <= NODEID_MAX; n++) {
			if (orb_conf::node_configured(n)) {
#if SOL_VERSION > __s10
				create_one_device_nodes(maj, _min, n,
								device_name);
#else
				create_one_device_nodes(maj, _min, n);
#endif
			}
		}
		mdc::service_property_seq_t *properties =
		    new mdc::service_property_seq_t;
		properties->length(1);
		(*properties)[0].name = (const char *)prop_name;
		(*properties)[0].value = device_name;
		err_val = _service->set_properties(*properties);
		delete properties;
	}
	if (err_val != DCS_SUCCESS) {
		_service->unlock();
		_environment.exception(new mdc::dcs_error(err_val));
		return;
	}

	// return the set of properties to the caller so that devfsadmd
	// can only make invocation on new metadevices, not existing ones.
	err_val = _service->get_all_properties(disk_list);
	if (err_val != DCS_SUCCESS) {
		_service->unlock();
		_environment.exception(new mdc::dcs_error(err_val));
		return;
	}
	_service->unlock();
}

//
// Helper function to cal svm_syncnode on 1 node.
//
#if SOL_VERSION > __s10
void
dc_config_impl::create_one_device_nodes(sol::major_t maj, sol::minor_t _min,
	sol::nodeid_t node, const char *device_name)
#else
void
dc_config_impl::create_one_device_nodes(sol::major_t maj, sol::minor_t _min,
	sol::nodeid_t node)
#endif
{
	Environment env;
	os::sc_syslog_msg *msg = NULL;

	CORBA::Object_var obj;
	repl_pxfs::ha_mounter_var mounter;
	char name[20];
	char command[256];
	naming::naming_context_var ctxp = ns::root_nameserver();

	//
	// Get the 'clexecd' object from the nameserver.
	//
	os::sprintf(name, "ha_mounter.%d", node);
	obj = ctxp->resolve(name, env);

	// Narrow the object down to type 'ha_mounter'.
	if (env.exception() == NULL) {
		mounter = repl_pxfs::ha_mounter::_narrow(obj);
	}

	if ((env.exception() != NULL) || CORBA::is_nil(mounter)) {
		//
		// Either there is no object in the nameserver bound with the
		// name 'ha_mounter.<nodeid>', or the object bound in the
		// nameserver with that name is not of type 'ha_mounter'.
		//
		msg = new os::sc_syslog_msg(SC_SYSLOG_DCS_DEVICE_SERVICE_TAG,
		    "DSM", NULL);
		//
		// SCMSGS
		// @explanation
		// There were problems making an upcall to run a user-level
		// program.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) msg->log(SC_SYSLOG_WARNING, MESSAGE, "Could not find "
		    "clexecd in nameserver.");
		delete msg;
		if (env.exception() != NULL) {
			env.clear();
		}
		return;
	}

	//
	// Execute the user program, in the foreground, syslog'ing any
	// output.
	//
#if SOL_VERSION > __s10
	os::sprintf(command, "/usr/cluster/lib/sc/SUNWmd_gdevs %d %d %s",
	    maj, _min, device_name);
#else
	os::sprintf(command, "/usr/cluster/lib/sc/SUNWmd_gdevs %d %d",
	    maj, _min);
#endif
	mounter->exec_program_with_opts((char *)command, false, true, true,
	    env);

	if (env.exception() != NULL) {
		// Failed to execute the program - print out a warning.
		msg = new os::sc_syslog_msg(SC_SYSLOG_DCS_DEVICE_SERVICE_TAG,
		    "DSM", NULL);
		//
		// SCMSGS
		// @explanation
		// There were problems making an upcall to run a user-level
		// program.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) msg->log(SC_SYSLOG_WARNING, MESSAGE, "Could not run %s."
		    "  User program did not execute cleanly.", (char *)command);
		delete msg;
		env.clear();
	}
}

//
// Walk the dc_mapper_list and upgrade the device server references.
//
void
dc_config_impl::upgrade_device_references()
{
	dc_mapper_impl *mapperp;

	CORBA::octet_seq_t	data;
	CORBA::object_seq_t	objs(1, 1);

	//
	// The following Environment is for the invocation that
	// obtains the updated object reference. This Environment must
	// not use a primary context.
	//
	Environment		_environment;

	// The following environment is for checkpointing to the secondary
	Environment		ckpt_environment;

	//
	// Create a primary context so the provider can send
	// checkpoints while the service is frozen.
	// XXX change the primary_ctx::invoke_env type.
	//
	primary_ctx ctx(NULL, primary_ctx::ADD_SECONDARY_CKPT,
	    ckpt_environment);

	list_lock.wrlock();

	dc_mapper_list::ListIterator iter(mapper_list);
	for (; (mapperp = iter.get_current()) != NULL; iter.advance()) {

		//
		// It is safe to do this as we are guaranteed to be called
		// only during a version upgrade callback.
		//
		mapperp->get_devobj()->_generic_method(data, objs,
		    _environment);

		if (_environment.exception()) {
			DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
			    ("upgrade_device_references(%p): exception"
			    "'%s' while calling _generic_method().\n",
			    this, _environment.exception()->_name()));
			_environment.clear();
		} else {
			mdc::device_server_var dev_server_obj =
			    mdc::device_server::_narrow(objs[0]);
			mdc::dc_mapper_var dc_mapper_obj =
			    mapperp->get_objref();

			mapperp->set_devobj(dev_server_obj);

			// Checkpoint to secondaries
			get_checkpoint()->ckpt_set_dc_mapper(
			    dc_mapper_obj, mapperp->get_devid(),
			    dev_server_obj, ckpt_environment);

			if (ckpt_environment.exception()) {
				DCS_DBPRINTF_R(DCS_TRACE_DC_CONFIG,
				    ("upgrade_device_references(%p): exception"
				    "'%s' while calling ckpt_set_dc_mapper()\n",
				    this, ckpt_environment.exception()->
				    _name()));
				ckpt_environment.clear();
			}
		}
	}
	list_lock.unlock();

	//
	// Show that the context is no longer there,
	// so that the destructor will see that everything is clean.
	//
	ckpt_environment.trans_ctxp = NULL;
}
