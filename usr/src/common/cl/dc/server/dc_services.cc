//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)dc_services.cc	1.33	08/05/20 SMI"

#include <dc/server/dc_services.h>
#include <dc/server/dc_service.h>
#include <dc/server/dc_config_impl.h>
#include <dc/sys/dc_lib.h>
#include <dc/libdcs/libdcs.h>
#include <h/ccr.h>

#include <dc/sys/dc_debug.h>

//
// Initialize static members.
//
dc_services *dc_services::the_services = NULL;
bool dc_services::first_access = true;
const char *dc_services::service_keys_table_name = "dcs_service_keys";
const char *dc_services::service_classes_table_name = "dcs_service_classes";
const char *dc_services::str_transparent = "transparent";
const char *dc_services::str_eio = "eio";

//
// Read in all the DCS configuration information into memory.  Called from
// dc_config::become_primary().
//
dc_error_t
dc_services::init_from_persistent()
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICES,
	    ("dc_services::init_from_persistent().\n"));

	dc_error_t err;

	// Get to an empty state
	cleanup();

	err = init_service_classes();
	if (err) {
		cleanup();
		return (err);
	}

	err = init_services();
	if (err) {
		cleanup();
		return (err);
	}

	return (DCS_SUCCESS);
}

//
// Helper functions to convert between types.
//
int
dc_services::conv(const char *string, mdc::ha_dev_type &type)
{
	ASSERT(string != NULL);

	if (strcmp(string, str_transparent) == 0) {
		type =  mdc::TRANSPARENT_FAILOVER;
		return (0);
	}
	if (strcmp(string, str_eio) == 0) {
		type = mdc::EIO_FAILOVER;
		return (0);
	}
	return (1);
}

void
dc_services::conv(mdc::ha_dev_type type, const char *&string)
{
	if (type == mdc::TRANSPARENT_FAILOVER) {
		string = str_transparent;
	} else {
		ASSERT(type == mdc::EIO_FAILOVER);
		string = str_eio;
	}
}

//
// Helper function that resolves a service class name to a 'dc_service_class'
// object.
//
dc_service_class *
dc_services::find_service_class(const char *name)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICES,
	    ("dc_services::find_service_class(%s).\n", name));

	dc_service_class *tmp;

	if (name == NULL) {
		return (NULL);
	}

	for (service_class_list.atfirst();
	    (tmp = service_class_list.get_current()) != NULL;
	    service_class_list.advance()) {
		if (strcmp(name, tmp->name) == 0) {
			return (tmp);
		}
	}

	return (NULL);
}

//
// Helper function called from init_from_persistent() to read in service classes
// from the CCR.
//
dc_error_t
dc_services::init_service_classes()
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICES,
	    ("dc_services::init_service_classes().\n"));

	ASSERT(lock_held());

	ccr::readonly_table_var table = dclib::get_readonly_table(
	    service_classes_table_name, false);
	if (CORBA::is_nil(table)) {
		return (DCS_ERR_CCR_ACCESS);
	}

	uint_t actual = 0;
	ccr::element_seq_var elems;
	Environment env;

	int count = table->get_num_elements(env);
	if (env.exception()) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICES,
		    ("init_service_classes(%p): exception '%s' while calling "
		    "%p->get_num_elements().\n", this,
		    env.exception()->_name(), table));
		RETURN_ON_EXCEPTION(env, "DCS: Error reading service classes "
		    "table", DCS_ERR_CCR_ACCESS);
	}

	ASSERT(count >= 0);

	if (count == 0) {
		os::warning("DCS: No device service classes are setup!\n");
		DCS_DBPRINTF_A(DCS_TRACE_DC_SERVICES,
		    ("init_service_classes(%p): no classes found in CCR.\n",
		    this));
		return (DCS_SUCCESS);
	}

	table->atfirst(env);
	if (env.exception()) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICES,
		    ("init_service_classes(%p): exception '%s' while calling "
		    "%p->atfirst().\n", this, env.exception()->_name(),
		    table));
		RETURN_ON_EXCEPTION(env, "DCS: Error reading service classes "
		    "table", DCS_ERR_CCR_ACCESS);
	}
	table->next_n_elements((uint32_t)count, elems, env);
	if (env.exception()) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICES,
		    ("init_service_classes(%p): exception '%s' while calling "
		    "%p->next_n_elements(%d, %p).\n", this,
		    env.exception()->_name(), table, count, elems));
		RETURN_ON_EXCEPTION(env, "DCS: Error reading service classes "
		    "table", DCS_ERR_CCR_ACCESS);
	}
	actual = elems->length();

	//
	// We now need to parse the data.  For a description of the CCR table
	// that stores service classes, see dc_services.h
	//

	char *fields[2];
	int out_count;
	mdc::ha_dev_type type;

	for (uint_t i = 0; i < actual; i++) {
		//
		// There should be two fields separated by ':' in the 'value'
		// part of the CCR record.  Separate them using 'parse_string'.
		//
		out_count = dclib::parse_string(elems[i].data, ':', fields,
		    2);

		//
		// If we got anything other than two fields, there is an error
		// in the CCR record.
		//
		if (out_count != 2) {
			DCS_DBPRINTF_A(DCS_TRACE_DC_SERVICES,
			    ("init_service_classes(%p): illegal value for key "
			    "'%s' in service classes file.\n",
			    this, (const char *)elems[i].key));
			os::warning("DCS: Illegal value for key %s in "
			    "service classes file", (const char *)elems[i].key);
			//
			// Try to process the other records, it is possible
			// (though unlikely) that this is not a seriuos problem.
			//
			continue;
		}

		//
		// If the second field cannot be converted to a failover type,
		// we have a problem.
		//
		if (conv(fields[1], type)) {
			DCS_DBPRINTF_A(DCS_TRACE_DC_SERVICES,
			    ("init_service_classes(%p): illegal value for key "
			    "'%s' in service classes file.\n",
			    this, (const char *)elems[i].key));
			os::warning("DCS: Illegal value for key %s in "
			    "service classes file", (const char *)elems[i].key);
			//
			// Try to process the other records, it is possible
			// (though unlikely) that this is not a serious problem.
			//
			continue;
		}

		//
		// If we've got to this point, we have a valid service class -
		// add this to the in-memory list.
		//
		service_class_list.append(new dc_service_class(elems[i].key,
		    fields[0], type));
	}

	return (DCS_SUCCESS);
}

//
// Helper function called from init_from_persistent() to read in services
// from the CCR.
//
dc_error_t
dc_services::init_services()
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICES,
	    ("dc_services::init_services().\n"));

	uint_t service_id;
	dc_service *_service;
	int count;
	Environment env;
	CORBA::Exception *ex;

	ASSERT(lock_held());

	ccr::readonly_table_var table = dclib::get_readonly_table(
	    service_keys_table_name, true);
	if (CORBA::is_nil(table)) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICES,
		    ("init_services(%p): table '%s' is null.\n", this,
		    service_keys_table_name));
		return (DCS_ERR_CCR_ACCESS);
	}

	uint_t actual = 0;
	ccr::element_seq_var elems;

	for (;;) {
		count = table->get_num_elements(env);
		if ((ex = env.exception()) != NULL) {
			if (ccr::table_modified::_exnarrow(ex) == NULL) {
				//
				// The error is NOT the 'table_modified'
				// exception, which is the only one we handle
				// here.  Return an error to the user.
				//
				DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICES,
				    ("init_services(%p): exception '%s' while "
				    "calling %p->get_num_elements().\n",
				    this, ex->_name(), table));
				RETURN_ON_EXCEPTION(env,
				    "DCS: Unexpected CCR error",
				    DCS_ERR_CCR_ACCESS);
				ASSERT(0);
			}
			// We have a stale handle to the table - try again.
			env.clear();
			table = dclib::get_readonly_table(
			    service_keys_table_name, false);
			if (CORBA::is_nil(table)) {
				DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
				    ("init_services(%p): table '%s' is null"
				    ".\n", this, service_keys_table_name));
				return (DCS_ERR_CCR_ACCESS);
			}
			continue;
		}

		ASSERT(count >= 0);

		if (count > 0) {
			table->atfirst(env);

			if ((ex = env.exception()) == NULL) {
				table->next_n_elements((uint32_t)count, elems,
				    env);
				actual = elems->length();
			}

			if ((ex = env.exception()) != NULL) {
				if (ccr::table_modified::_exnarrow(ex) ==
				    NULL) {
					//
					// The error is NOT the 'table_modified'
					// exception, which is the only one we
					// handle here.  Return an error to the
					// user.
					//
					DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICES,
					    ("init_services(%p): exception "
					    "'%s' while calling "
					    "%p->next_n_elements().\n",
					    this, ex->_name(), table));

					RETURN_ON_EXCEPTION(env,
					    "DCS: Unexpected CCR error",
					    DCS_ERR_CCR_ACCESS);
					ASSERT(0);
				}
				//
				// We have a stale handle to the table - try
				// again.
				//
				env.clear();
				table = dclib::get_readonly_table(
				    service_keys_table_name, false);
				if (CORBA::is_nil(table)) {
					DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICES,
					    ("init_services(%p): table '%s' "
					    "is null.\n", this,
					    service_keys_table_name));
					return (DCS_ERR_CCR_ACCESS);
				}
				continue;
			}
		}
		break;
	}

	ASSERT(actual == (uint_t)count);

	for (uint_t i = 0; i < (uint_t)count; i++) {
		service_id = (uint_t)os::atoi(elems[i].key);
		if (service_id < START_DEVID_VALUE) {
			DCS_DBPRINTF_A(DCS_TRACE_DC_SERVICES,
			    ("init_services(%p): ignoring illegal service "
			    "%d.\n", this, service_id));
			os::warning("DCS: Ignoring illegal service id %d",
			    service_id);
		} else if (resolve_service_from_id(service_id,
		    dc_services::UNLOCKED) != NULL) {
			DCS_DBPRINTF_A(DCS_TRACE_DC_SERVICES,
			    ("init_services(%p): service %d already read!.\n",
			    this, service_id));
		} else {
			// Mark this id as allocated
			set_num((int)service_id - START_DEVID_VALUE);

			//
			// Read in the service with this id from persistent
			// storage and cache it.
			//
			_service = dc_service::read_from_ccr(service_id);
			if (_service == NULL) {
				DCS_DBPRINTF_A(DCS_TRACE_DC_SERVICES,
				    ("init_services(%p): error initialising "
				    "service %d.\n", this, service_id));
				os::warning("DCS: Error initializing "
				    "service %d from file", service_id);
			} else {
				service_list.append(_service);
			}
		}
	}

	return (DCS_SUCCESS);
}

//
// Called when the dc_config object using this instance of dc_services
// becomes a secondary.  Get rid of all state in this call.
//
void
dc_services::cleanup()
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICES,
	    ("dc_services::cleanup().\n"));

	// Free all service id allocations
	free_all();

	service_list.dispose();
	service_class_list.dispose();
}

//
// Get the dc_services object
//
dc_services *
dc_services::get_services()
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICES,
	    ("dc_services::get_services().\n"));

	ASSERT(the_services != NULL);
	//
	// XXX Today, the DCS starts up before the CCR starts up read/write.
	// This means that we should postpone calling init_from_persistent()
	// (since it could create a CCR table).  When the DCS starts up after
	// the CCR, the init_from_persistent() call will go into
	// dc_config_impl::convert_to_primary()
	//
	if (first_access) {
		first_access = false;
		the_services->wrlock();
		(void) the_services->init_from_persistent();
		the_services->unlock();
	}
	return (the_services);
}

//
// Helper function that both adds and removes service ids from the CCR table.
//
dc_error_t
dc_services::update_service_id(unsigned int service_id, bool allocate)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICES,
	    ("dc_services::update_service_id(%d, %d).\n",
	    service_id, allocate));

	Environment env;

	// Get the table containing the list of services
	ccr::updatable_table_var table = dclib::get_updatable_table(
	    service_keys_table_name, false);
	if (CORBA::is_nil(table)) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICES,
		    ("update_service_id(%p): table '%s' is null.\n",
		    service_keys_table_name));
		return (DCS_ERR_CCR_ACCESS);
	}

	char service_id_str[5];

	(void) sprintf(service_id_str, "%d", service_id);

	if (allocate) {
		table->add_element(service_id_str, "", env);
		if (env.exception()) {
			DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICES,
			    ("update_service_id(%p): exception '%s' while "
			    "calling %p->add_element(%s).\n", this,
			    env.exception()->_name(), table,
			    service_id_str));
		}

	} else {
		table->remove_element(service_id_str, env);
		if (env.exception()) {
			DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICES,
			    ("update_service_id(%p): exception '%s' while "
			    "calling %p->remove_element(%s).\n", this,
			    env.exception()->_name(), table,
			    service_id_str));
		}
	}

	if (env.exception()) {
		os::warning("DCS: Error accessing services table");
		env.clear();
		table->abort_transaction(env);
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICES,
		    ("update_service_id(%p): exception '%s' while "
		    "calling %p->abort_transaction().\n", this,
		    env.exception()->_name(), table));
		return (DCS_ERR_CCR_ACCESS);
	}

	// If successful, commit the transaction and return.
	table->commit_transaction(env);
	if (env.exception()) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICES,
		    ("update_service_id(%p): exception '%s' while "
		    "calling %p->commit_transaction(%s).\n", this,
		    env.exception()->_name(), table));
		RETURN_ON_EXCEPTION(env, "DCS: Exception while commiting "
		    "transaction", DCS_ERR_CCR_ACCESS);
	}

	return (DCS_SUCCESS);
}

//
// Free a service id from memory and persistent storage.
//
dc_error_t
dc_services::free_service_id(unsigned int service_id)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICES,
	    ("dc_services::free_service_id(%d).\n", service_id));

	ASSERT(service_lock.lock_held());

	free_service_id_in_memory(service_id);

	return (update_service_id(service_id, false));
}

//
// Create a new service (in memory and in persistent storage)
//
dc_error_t
dc_services::create_device_service(unsigned int service_id,
    const char *service_name,
    dc_service_class *service_class,
    bool failback_enabled,
    const mdc::node_preference_seq_t &nodes,
    unsigned int num_secs,
    dc_minor_range_list *gdev_ranges_list,
    const mdc::service_property_seq_t &properties)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICES,
	    ("dc_services::create_device_service(%d, %s, %s, %d, %p, %d, %p, "
	    "%p).\n", service_id, service_name, service_class,
	    failback_enabled, nodes, num_secs, gdev_ranges_list, properties));

	dc_error_t err_val, err;
	dc_storage *dcstore = dc_storage::storage();

	ASSERT(service_lock.lock_held());

	//
	// First, write out this new service id to the CCR 'dcs_service_keys'
	// table.
	//
	err_val = update_service_id(service_id, true);
	if (err_val != DCS_SUCCESS) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICES,
		    ("create_device_service(%p): update_service_id(%d) "
		    "failed with error %d.\n", this, service_id, err_val));

		//
		// Remove the device ranges we just added.
		//
		dcstore->clear_mapping(service_id, gdev_ranges_list);
		//
		// Delete 'gdev_ranges_list' - this function is supposed to
		// own it.
		//
		delete gdev_ranges_list;
		return (err_val);
	}

	mdc::node_preference_seq_t *service_nodes =
	    new mdc::node_preference_seq_t(nodes);

	dc_service *_service = new dc_service(service_id, service_name,
	    service_class, failback_enabled, service_nodes, num_secs,
	    gdev_ranges_list, false);

	err_val = _service->write_to_ccr(dc_service::key_all, &properties);
	if (err_val != 0) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICES,
		    ("create_device_service(%p): write_to_ccr() failed with "
		    "error %d.\n", this, err_val));

		//
		// write_to_ccr() could fail for one of these two reasons:
		// 1. We could not get a table to update from the CCR.
		// 2. We got the table from the CCR, but write_internal()
		//    failed.
		// For case 2 above, we are left with an invalid
		// dcs_service_<service_id> table. There is no way of telling
		// case 1 from 2, so we try to clean up anyway. When the table
		// does not exist (for case 1), erase_ccr_table() call below
		// will return DCS_ERR_SERVICE_NAME. No error message is
		// printed in this case as the CCR is in the same state as
		// before.
		// For case 2, this will delete the dcs_service_<service_id>
		// table and its entry from the directory table. This will
		// keep the CCR sane.
		//
		if ((err = _service->erase_ccr_table()) != DCS_SUCCESS) {
		    if (err != DCS_ERR_SERVICE_NAME) {
			os::warning("CCR may be in an inconsistent state.\n");
		    }
		}
		//
		// Remove the device ranges we just added.
		//
		dcstore->clear_mapping(service_id, gdev_ranges_list);
		delete _service;
		return (err_val);
	}
	service_list.append(_service);

	return (DCS_SUCCESS);
}

//
// Erase this service (from memory and persistent storage)
//
dc_error_t
dc_services::erase_service(dc_service *_service)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICES,
	    ("dc_services::erase_service(%p).\n", _service));

	ASSERT(service_lock.lock_held());

	dc_error_t err_val = _service->erase_ccr_table();
	if (err_val != DCS_SUCCESS) {
		return (err_val);
	}

	unsigned int service_id = _service->get_service_id();

	(void) service_list.erase(_service);
	_service->unlock();

	delete _service;

	// Erase the service id from memory and persistent storage.
	return (free_service_id(service_id));
}

//
// Called by dc_config_impl to create a new service class.
//
dc_error_t
dc_services::create_service_class(const char *service_class_name,
    const char *user_program, mdc::ha_dev_type type)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICES,
	    ("dc_services::create_service_class(%s, %s, %d).\n",
	    service_class_name, user_program, type));

	ASSERT(service_lock.lock_held());

	//
	// Check to see if a class of this name already exists - if it does,
	// return an error.
	//
	dc_service_class *service_class =
	    find_service_class(service_class_name);
	if (service_class != NULL) {
		return (DCS_ERR_SERVICE_CLASS_NAME);
	}

	//
	// Since the call to the CCR to add a new record could fail, we try
	// that first.  Only if that call is successful do we add this class
	// to our in-memory state.
	//
	dc_service_class *new_class = new dc_service_class(service_class_name,
	    user_program, type);

	dc_error_t err_val = write_service_class(new_class, add_element);
	if (err_val == DCS_SUCCESS) {
		service_class_list.append(new_class);
	} else  {
		delete new_class;
	}

	return (err_val);
}

//
// Called by dc_config_impl to change the user program associated with a
// service class.
//
dc_error_t
dc_services::set_service_class_user_program(const char *service_class_name,
    const char *user_program)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICES,
	    ("dc_services::set_service_class_user_program(%s, %s).\n",
	    service_class_name, user_program));

	ASSERT(service_lock.lock_held());

	// Return an error if the service class does not exist.
	dc_service_class *service_class =
	    find_service_class(service_class_name);
	if (service_class == NULL) {
		return (DCS_ERR_SERVICE_CLASS_NAME);
	}

	//
	// Before doing an expensive CCR update operation, see if it is
	// necessary.
	//
	if (strcmp(service_class->takeover_program, user_program) == 0) {
		return (DCS_SUCCESS);
	}

	// Store the old user program in case of a CCR error.
	char *tmp = service_class->takeover_program;
	service_class->takeover_program = dclib::strdup(user_program);

	// Try writing out the service class.
	dc_error_t err_val = write_service_class(service_class, update_element);
	if (err_val == DCS_SUCCESS) {
		delete [] tmp;
	} else {
		// There was an error - revert to the older value.
		delete [] (service_class->takeover_program);
		service_class->takeover_program = tmp;
	}

	return (err_val);
}

dc_error_t
dc_services::set_service_class_ha_type(const char *service_class_name,
    mdc::ha_dev_type type)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICES,
	    ("dc_services::set_service_class_ha_type(%s, %d).\n",
	    service_class_name, type));

	ASSERT(service_lock.lock_held());

	// Return an error if the service class does not exist.
	dc_service_class *service_class =
	    find_service_class(service_class_name);
	if (service_class == NULL) {
		return (DCS_ERR_SERVICE_CLASS_NAME);
	}

	//
	// Before doing an expensive CCR update operation, see if it is
	// necessary.
	//
	if (type == service_class->type) {
		return (DCS_SUCCESS);
	}

	// Store the current value in case of a CCR error.
	mdc::ha_dev_type stored_type = service_class->type;
	service_class->type = type;

	// Try writing out the service class.
	dc_error_t err_val = write_service_class(service_class, update_element);
	if (err_val != DCS_SUCCESS) {
		// There was an error - revert to the old value.
		service_class->type = stored_type;
	}

	return (err_val);
}

dc_error_t
dc_services::get_service_class_parameters(const char *service_class_name,
    CORBA::String_out user_program, mdc::ha_dev_type &type)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICES,
	    ("dc_services::get_service_class_parameters(%s).\n",
	    service_class_name));

	ASSERT(service_lock.lock_held());

	// Return an error if there is no service class of this name.
	dc_service_class *service_class =
	    find_service_class(service_class_name);
	if (service_class == NULL) {
		return (DCS_ERR_SERVICE_CLASS_NAME);
	}

	// Return the relevant values to the caller.
	user_program = dclib::strdup(service_class->takeover_program);
	type = service_class->type;
	return (DCS_SUCCESS);
}

dc_error_t
dc_services::remove_service_class(const char *service_class_name)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICES,
	    ("dc_services::remove_service_class(%s).\n",
	    service_class_name));

	ASSERT(service_lock.lock_held());

	// Return an error if there is no service class of this name.
	dc_service_class *service_class =
	    find_service_class(service_class_name);
	if (service_class == NULL) {
		return (DCS_ERR_SERVICE_CLASS_NAME);
	}

	//
	// See if the service class is in use by any service.  Look at the
	// constructor/destructor of dc_service.
	//
	if (service_class->ref_count > 0) {
		return (DCS_ERR_SERVICE_CLASS_BUSY);
	}

	// Try updating the CCR table.
	dc_error_t err_val = write_service_class(service_class, remove_element);
	if (err_val != DCS_SUCCESS) {
		return (DCS_ERR_CCR_ACCESS);
	}

	// Success - update in-memory state.
	(void) service_class_list.erase(service_class);
	delete service_class;
	return (DCS_SUCCESS);
}

//
// Return the names of all the service classes.
//
void
dc_services::get_service_class_names(sol::string_seq_t_out &services)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICES,
	    ("dc_services::get_service_class_names().\n"));

	ASSERT(service_lock.lock_held());

	sol::string_seq_t *service_names = new sol::string_seq_t;
	dc_service_class *service_class;
	uint_t i;

	service_names->length(service_class_list.count());
	for (i = 0, service_class_list.atfirst();
	    ((service_class = service_class_list.get_current()) != NULL);
	    service_class_list.advance(), i++) {
		(*service_names)[i] = (const char *)(service_class->name);
	}

	services = service_names;
}

//
// Get the names of all the services belonging to the specified service class.
//
dc_error_t
dc_services::get_service_names_of_class(const char *service_class_name,
    sol::string_seq_t_out &service_names)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICES,
	    ("dc_services::get_service_names_of_class(%s).\n",
	    service_class_name));

	ASSERT(service_lock.lock_held());

	dc_service *_service;
	dc_service_class *service_class = find_service_class(
	    service_class_name);

	if (service_class == NULL) {
		return (DCS_ERR_SERVICE_CLASS_NAME);
	}

	uint_t count = service_class->ref_count;
	char **tmp = new char *[count];

	dc_service_list::ListIterator iterator(service_list);
	uint_t i = 0;
	while ((_service = iterator.get_current()) != NULL) {
		_service->rdlock();
		if (service_class == _service->get_service_class()) {
			_service->get_service_name(&(tmp[i]));
			i++;
		}
		_service->unlock();
		iterator.advance();
	};

	ASSERT(i == count);

	service_names = new sol::string_seq_t(count, count, tmp, true);
	return (DCS_SUCCESS);
}

//
// Get the names of all the services.
//
void
dc_services::get_service_names(sol::string_seq_t_out &service_names)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICES,
	    ("dc_services::get_service_names().\n"));

	ASSERT(service_lock.lock_held());

	dc_service *_service;

	uint_t count = service_list.count();
	char **tmp = new char *[count];

	dc_service_list::ListIterator iterator(service_list);
	uint_t i = 0;
	while ((_service = iterator.get_current()) != NULL) {
		_service->rdlock();
		_service->get_service_name(&(tmp[i]));
		_service->unlock();
		i++;
		iterator.advance();
	};

	ASSERT(i == count);

	service_names = new CORBA::StringSeq(count, count, tmp, true);
}

//
// Get the service with the given id.
//
dc_service *
dc_services::resolve_service_from_id(unsigned int service_id,
    int return_locked_service)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICES,
	    ("dc_services::resolve_service_from_id(%d, %d).\n",
	    service_id, return_locked_service));

	ASSERT(service_lock.lock_held());

	dc_service *_service;

	dc_service_list::ListIterator iterator(service_list);
	while ((_service = iterator.get_current()) != NULL) {
		_service->rdlock();
		if (_service->equals_service_id(service_id)) {
			if (return_locked_service == UNLOCKED) {
				_service->unlock();
			}
			if (return_locked_service == WRLOCKED) {
				_service->unlock();
				_service->wrlock();
				if (_service->equals_service_id(service_id)) {
					return (_service);
				} else {
					_service->unlock();
					return (NULL);
				}
			}
			return (_service);
		}
		_service->unlock();
		iterator.advance();
	}

	return (NULL);
}

//
// Get the service with the given name.
//
dc_service *
dc_services::resolve_service(const char *service_nm,
    int return_locked_service)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICES,
	    ("dc_services::resolve_service(%s, %d).\n",
	    service_nm, return_locked_service));

	ASSERT(service_lock.lock_held());

	dc_service *_service;

	dc_service_list::ListIterator iterator(service_list);
	while ((_service = iterator.get_current()) != NULL) {
		_service->rdlock();
		if (_service->equals_service_name(service_nm)) {
			if (return_locked_service == UNLOCKED) {
				_service->unlock();
			}
			if (return_locked_service == WRLOCKED) {
				_service->unlock();
				_service->wrlock();
				if (_service->equals_service_name(service_nm)) {
					return (_service);
				} else {
					_service->unlock();
					return (NULL);
				}
			}
			return (_service);
		}
		_service->unlock();
		iterator.advance();
	}

	return (NULL);
}

//
// Get all the active services that can run on node 'id'.  An active service
// is one that has been started at some point - current status does not matter.
// This function is used by the DCS to start up replicas of active services on
// the DSM of a node that is joining the cluster.
//
void
dc_services::get_active_services(dc_service_list &_service_list,
    sol::nodeid_t id)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICES,
	    ("dc_services::get_active_services(%d).\n", id));
	ASSERT(service_lock.lock_held());

	dc_service *_service;
	dc_service_list::ListIterator iterator(service_list);
	while ((_service = iterator.get_current()) != NULL) {
		_service->rdlock();
		if (_service->is_active()) {
			if (_service->is_node_in_service(id)) {
				_service_list.append(_service);
			}
		}
		_service->unlock();
		iterator.advance();
	}

}

//
// Helper function that adds, changes or removes a single service class from
// the CCR, depending on the value of 'write_type'.  This function is called
// from all the functions in dc_services that create or configure service
// classes.
//
dc_error_t
dc_services::write_service_class(dc_service_class *service_class,
    write_type_t write_type)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICES,
	    ("dc_services::write_service_class(%s, %d).\n",
	    service_class, write_type));

	Environment env;

	// Get the table containing the list of services
	ccr::updatable_table_var table = dclib::get_updatable_table(
	    service_classes_table_name, false);
	if (CORBA::is_nil(table)) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICES,
		    ("write_service_class(%p): table '%s' is null.\n",
		    this, service_classes_table_name));
		return (DCS_ERR_CCR_ACCESS);
	}

	size_t takeover_pgm_len = strlen(service_class->takeover_program);
	char *value = (char *)NULL;

	//
	// If we are removing a service class, then we do not need to compute
	// the string that is the 'data' part of the CCR record.
	//
	if (write_type != remove_element) {
		value = new char[takeover_pgm_len + strlen(str_transparent)
		    + 2];

		char *tmp = value;
		const char *failover_type;
		conv(service_class->type, failover_type);
		//lint -e668
		(void) strcpy(tmp, service_class->takeover_program);
		//lint +e668
		tmp += takeover_pgm_len;
		(void) strcpy(tmp, ":");
		tmp++;
		(void) strcpy(tmp, failover_type);
	}

	// Do the appropriate CCR operation.
	switch (write_type) {
	case add_element:
		table->add_element(service_class->name, value, env);
		break;
	case update_element:
		table->update_element(service_class->name, value, env);
		break;
	case remove_element:
		table->remove_element(service_class->name, env);
		break;
	default:
		ASSERT(0);
	}

	// Free up value, if it was allocated.
	delete [] value;

	//
	// Either abort the transaction or commit it, depending on how the
	// CCR operation returns.  If we abort the operation, the user will
	// see a 'DCS_ERR_CCR_ACCESS' error via the library routines.
	//

	if (env.exception()) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICES,
		    ("write_service_class(%p): exception '%s' while calling "
		    "%p->[write_type=%d].\n", this,
		    env.exception()->_name(), table, write_type));

		os::warning("DCS: Error updating service classes table");
		env.clear();
		table->abort_transaction(env);
		env.clear();
		return (DCS_ERR_CCR_ACCESS);
	}

	table->commit_transaction(env);
	if (env.exception()) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICES,
		    ("write_service_class(%p): exception '%s' while calling "
		    "%p->commit_transaction().\n", this,
		    env.exception()->_name(), table));

		os::warning("DCS: Error updating service classes table");
		env.clear();
		return (DCS_ERR_CCR_ACCESS);
	}

	return (DCS_SUCCESS);
}

//
// This is called from dc_config_impl::set_service_inactive(). It is only
// used when a spare is being shutdown. Trying to acquire locks when a spare
// is being shutdown can cause deadlocks in the DCS. This method is called
// only when a service is dead and the HA framework is cleaning it up. Hence,
// although we do not acquire locks explicitly, there is implicit locking
// because no transitions can take place on this service until the framework
// is done with its cleanup work.
//
dc_service *
dc_services::resolve_service_nolocks(const char *service_nm)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICES,
	    ("dc_services::resolve_service_nolocks(%s).\n",
	    service_nm));

	ASSERT(service_lock.lock_held());

	dc_service *_service;

	dc_service_list::ListIterator iterator(service_list);
	while ((_service = iterator.get_current()) != NULL) {
		if (_service->equals_service_name(service_nm)) {
			return (_service);
		}
		iterator.advance();
	}

	return (NULL);
}
