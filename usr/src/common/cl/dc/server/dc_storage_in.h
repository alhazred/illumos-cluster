/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _DC_STORAGE_IN_H
#define	_DC_STORAGE_IN_H

#pragma ident	"@(#)dc_storage_in.h	1.9	08/05/20 SMI"

inline void
dc_storage::init() {
	if (the_storage == NULL)
		the_storage = new dc_storage();
}

inline void
dc_storage::destroy() {
	ASSERT(the_storage != NULL);
	delete (the_storage);
	the_storage = NULL;
}

inline dc_storage *
dc_storage::storage() {
	ASSERT(the_storage != NULL);
	return (the_storage);
}

static inline int
dc_storage::maj_to_bucket(major_t maj)
{
	return (((int)maj) % MAX_HASH_BUCKET);
}

//
// Returns true if entries with this major number already exist in memory.
//
inline bool
dc_storage::exists_major_number(major_t maj)
{
	return (get_store(maj, false) != NULL);
}

#endif	/* _DC_STORAGE_IN_H */
