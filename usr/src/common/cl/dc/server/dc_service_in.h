/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _DC_SERVICE_IN_H
#define	_DC_SERVICE_IN_H

#pragma ident	"@(#)dc_service_in.h	1.23	08/05/20 SMI"

#include <dc/sys/dc_debug.h>

inline bool
dc_service::is_active()
{
	return (_active);
}

inline void
dc_service::get_service_name(char **buffer)
{
	ASSERT(access.lock_held());
	*buffer = dclib::strdup(service_name);
}

inline dc_service_class *
dc_service::get_service_class()
{
	return (_service_class);
}

inline bool
dc_service::is_local_class()
{
	ASSERT(access.lock_held());
	return (_service_class->local_class);
}

inline bool
dc_service::get_failback_mode()
{
	ASSERT(access.lock_held());
	return (_failback_enabled);
}

inline bool
dc_service::is_suspended()
{
	ASSERT(access.lock_held());
	return (_suspended);
}

inline void
dc_service::get_number_of_secondaries(unsigned int &num)
{
	ASSERT(access.lock_held());
	num = num_secondaries;
}

inline void
dc_service::get_nodes(mdc::node_preference_seq_t &nodes)
{
	ASSERT(access.lock_held());
	nodes = *service_nodes;
}

inline void
dc_service::get_node_ids(sol::nodeid_seq_t &node_ids)
{
	ASSERT(access.lock_held());
	int len = service_nodes->length();
	node_ids.length(len);
	for (int count = 0; count < len; count++) {
		node_ids[count] = (*service_nodes)[count].id;
	}
}

inline uint_t
dc_service::get_priority(nodeid_t id)
{
	ASSERT(access.lock_held());
	int len = service_nodes->length();
	for (int count = 0; count < len; count++) {
		if ((*service_nodes)[count].id == id) {
			return ((*service_nodes)[count].preference);
		}
	}

	ASSERT(0);
	return (0);
}

inline dc_minor_range_list *
dc_service::get_devices_list()
{
	ASSERT(access.lock_held());
	return (service_gdev_ranges);
}

inline bool
dc_service::equals_service_id(unsigned int sid)
{
	// No rdlock needed because service id does not change
	if (service_id == sid)
		return (true);
	else
		return (false);
}

inline bool
dc_service::equals_service_name(const char *service_nm)
{
	if (strcmp(service_name, service_nm) == 0)
		return (true);
	else
		return (false);
}

inline unsigned int
dc_service::get_service_id()
{
	return (service_id);
}

//
// Mark this service as active/inactive.
//
inline void
dc_service::set_active(bool active)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service::set_active(%d).\n", active));
	_active = active;
	DCS_DBPRINTF_G(DCS_TRACE_DC_SERVICE,
	    ("set_active(%p): service '%s' is being marked '%s'.\n",
	    this, service_name, (active == true) ? "active" : "inactive"));
}

inline void
dc_service::rdlock()
{
	access.rdlock();
}

inline void
dc_service::wrlock()
{
	//
	// First grab the mutex which serialize wrlock calls.
	//
	access_mutex.lock();
	//
	// try_wrlock() method can only succeed if *nobody* (reader or writer)
	// currently holds the lock. try_wrlock() will not block.
	//
	while (access.try_wrlock() == 0) {
		//
		// Lock is held. Block here until it is released.
		//
		access_cv.wait(&access_mutex);
	}
	//
	// We now have the writer's lock.
	//
	access_mutex.unlock();
}

inline void
dc_service::unlock()
{
	ASSERT(access.lock_held());
	ASSERT(!startup_lock.lock_held());
	//
	// Before unlocking the rw_lock grab the mutex protecting it.
	//
	access_mutex.lock();
	access.unlock();
	//
	// Notify any waiting writers.
	//
	access_cv.broadcast();
	access_mutex.unlock();
}

inline int
dc_service::start_service_try_lock()
{
	ASSERT(access.lock_held());
	return (startup_lock.try_lock());
}

inline void
dc_service::start_service_lock()
{
	ASSERT(access.lock_held());
	startup_lock.lock();
}

inline void
dc_service::start_service_unlock()
{
	ASSERT(access.lock_held());
	ASSERT(startup_lock.lock_held());
	startup_lock.unlock();
}

#ifdef DEBUG
inline int
dc_service::lock_held()
{
	return (access.lock_held());
}
#endif

#endif	/* _DC_SERVICE_IN_H */
