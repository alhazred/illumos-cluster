/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _DC_REPL_SERVER_H
#define	_DC_REPL_SERVER_H

#pragma ident	"@(#)dc_repl_server.h	1.29	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#include <h/dc.h>
#include <h/repl_dc.h>
#include <h/replica.h>
#include <orb/object/adapter.h>
#include <repl/service/replica_tmpl.h>
#include <sys/vm_util.h>

//
// This struct is used for building a table for supporting the mapping
// from version protocol spec file version number to the various IDL
// interface versions it represents.  The table will be a two dimensional
// array indexed by major/minor vp version.
//
typedef struct {		// idl interfaces
	int	dc_config;	// dc_config
	int	dc_mapper;	// dc_mapper
	int	dc_ckpt;	// repl_dc ckpt
} dc_ver_map_t;

//
// These are the current maximum indexes used for accessing the vp to idl
// version table.
//
const int	DC_VP_MAX_MAJOR = 1;
const int	DC_VP_MAX_MINOR = 2;

//
// This is the repl_server object for the DCS server side.
// It serves the dc_config object, all the dc_mapper objects, and all the
// dc_server objects.
//
class dc_repl_server : public repl_server<repl_dc::repl_dc_ckpt> {

public :
	dc_repl_server(const char *s_id, const char *p_id);
	~dc_repl_server();

	// repl_prov operations
	void become_secondary(Environment &);
	void add_secondary(replica::checkpoint_ptr, const char *,
	    Environment &);
	void remove_secondary(const char *, Environment &);
	void freeze_primary_prepare(Environment &);
	void freeze_primary(Environment &);
	void unfreeze_primary(Environment &);
	void shutdown(Environment &);
	CORBA::Object_ptr get_root_obj(Environment &);
	void become_primary(const replica::repl_name_seq &,
	    Environment &);
	void become_spare(Environment &);

	// Checkpoint operations
	void ckpt_dump_state(mdc::dc_config_ptr obj,
	    Environment &_environment);
	void ckpt_add_dc_mapper(mdc::dc_mapper_ptr primary_obj,
	    Environment &);
	void ckpt_set_dc_mapper(mdc::dc_mapper_ptr primary_obj,
	    uint32_t id, mdc::device_server_ptr dev_server,
	    Environment &);
	void ckpt_device_server(mdc::dc_mapper_ptr primary_obj,
	    mdc::device_server_ptr dev_server, Environment &);
	void ckpt_remove_dc_mapper(mdc::dc_mapper_ptr mapper_obj,
	    Environment &);
	void ckpt_register_dsm(mdc::device_service_manager_ptr dsm,
	    mdc::device_server_ptr local_device_server,
	    sol::nodeid_t node_id, Environment &);
	void ckpt_unregister_dsm(mdc::device_service_manager_ptr dsm,
	    Environment &);
	void ckpt_add_callback(sol::major_t maj, sol::minor_t min,
	    fs::dc_callback_ptr cb_obj, mdc::dc_mapper_ptr mapper_obj,
	    Environment &);
	void ckpt_remove_callback(sol::major_t maj, sol::minor_t min,
	    fs::dc_callback_ptr cb_obj, Environment &);
	void ckpt_get_resv_lock(sol::nodeid_t id,
	    mdc::reservation_client *resv_obj, sol::incarnation_num inc,
	    Environment &);
	void ckpt_release_resv_lock(mdc::reservation_client *resv_obj,
	    Environment &);
	static void initialize();

	// Checkpoint accessor function.
	repl_dc::repl_dc_ckpt_ptr	get_checkpoint_repl_dc();

	void upgrade_callback(const version_manager::vp_version_t &,
	    Environment &);

	// Set the initial version number.
	void set_init_version(const version_manager::vp_version_t &);

private:
	version_manager::vp_version_t	current_version;

	//
	// This lock protects 'current_version', _ckpt_proxy and
	// provides locking between upgrade callbacks and become_primary().
	// It needs to be a rwlock since we make checkpoint calls while
	// holding the lock.
	//
	os::rwlock_t	version_lock;

	// Checkpoint proxy,
	repl_dc::repl_dc_ckpt_ptr	_ckpt_proxy;
};

class cl_dcs_upgrade_callback :
	public McServerof<version_manager::upgrade_callback>
{
public:
	cl_dcs_upgrade_callback(dc_repl_server &);
	~cl_dcs_upgrade_callback();

	void _unreferenced(unref_t unref);

	void do_callback(const char *ucc_name,
	    const version_manager::vp_version_t &new_version,
	    Environment &);
private:
	dc_repl_server	&prov;
};

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif	/* _DC_REPL_SERVER_H */
