/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _DC_STORAGE_H
#define	_DC_STORAGE_H

#pragma ident	"@(#)dc_storage.h	1.12	08/05/20 SMI"

#include <sys/os.h>
#include <h/dc.h>
#include <dc/server/dc_major_store.h>

//
// Storage for global minor, local minor, device server id mappings.
// It also manages the transient storage for clone devices.
// Transient mappings are operated on by all functions that
// start with cache below.
// Each major no. has a file and cache list for storage, that is
// managed by the dc_major_store class. Actually this storage is used
// by both client and server.
//
extern class dc_storage *the_storage;

class dc_storage {
public:
	dc_storage();
	~dc_storage();

	static dc_storage *storage();
	static void init();
	static void destroy();

	//
	// Check for the validity of a change in the minor maps.
	//
	dc_error_t possible_mapping(modify_type_t type,
	    const mdc::dev_range_seq_t &gdev_ranges, unsigned int service_id);

	//
	// Add or remove the specified devices from the appropriate major
	// number maps.
	//
	dc_error_t process_mapping(modify_type_t type,
	    const mdc::dev_range_seq_t &gdev_ranges, unsigned int service_id,
	    dc_minor_range_list *service_range_list);

	//
	// Clears all the device mappings for a given service.
	//
	void clear_mapping(unsigned int service_id,
	    dc_minor_range_list *service_range_list);

	//
	// Adds the specified mappings to the appropriate 'dc_major_store'
	// objects.  These mappings are assumed to be good, so no validation
	// checking is required.
	//
	void add_mapping_nocheck(dc_minor_range_list *new_list);

	bool get_map(major_t maj, minor_t minor_num, unsigned int *device_id);

	//
	// Returns true if entries with this major number already exist in
	// memory.
	//
	bool exists_major_number(major_t maj);

	void get_unique_major_numbers(mdc::long_seq_t &major_numbers);

private:
	enum { MAX_HASH_BUCKET = 16 };
	dc_major_store *hashed_list[(int)MAX_HASH_BUCKET];
	os::mutex_t list_lock;
	static int maj_to_bucket(major_t maj);
	dc_major_store *get_store(major_t maj, bool create);
};

#include <dc/server/dc_storage_in.h>

#endif	/* _DC_STORAGE_H */
