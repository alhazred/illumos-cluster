//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)dc_service.cc	1.44	08/05/20 SMI"

#include <sys/os.h>
#include <dc/server/dc_service.h>
#include <dc/server/dc_services.h>
#include <dc/server/dc_config_impl.h>
#include <dc/sys/dc_lib.h>
#include <dc/libdcs/libdcs.h>
#include <dc/server/dc_storage.h>

#include <orb/fault/fault_injection.h>

#include <dc/sys/dc_debug.h>

//
// Initialize private constant strings.
//
const char *dc_service_class::str_local_class = "SUNWlocal";

const char *dc_service::str_yes = "yes";
const char *dc_service::str_no = "no";

const char *dc_service::key_str_service_name = "DCS_ServiceName";
const char *dc_service::key_str_service_class = "DCS_ServiceClass";
const char *dc_service::key_str_suspended = "DCS_Suspended";
const char *dc_service::key_str_failback_enabled = "DCS_FailbackEnabled";
const char *dc_service::key_str_num_secs = "DCS_NumberOfSecondaries";
const char *dc_service::key_str_nodes = "DCS_Nodes";
const char *dc_service::key_str_devices = "DCS_Devices";

const int dc_service::number_of_system_keys = 7;

const char *dc_service::service_table_prefix = "dcs_service_";

const int dc_service::node_record_max_len = 8;	// "(xx,yy) "
const int dc_service::node_record_min_len = 6;	// "(x,y) "

const int dc_service::gdev_record_max_len = 41;	// "(12d,12d-12d) "
const int dc_service::gdev_record_min_len = 8;	// "(x,y-z) "

#define	IN_BETWEEN(a, x, y)	((a >= x) && (a <= y))

//
// Constructor for 'dc_service_class'.
//
dc_service_class::dc_service_class(const char *class_name,
    const char *takeover_pgm, mdc::ha_dev_type ha_type)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service_class::dc_service_class(%s, %s, %d).\n",
	    class_name, takeover_pgm, ha_type));

	name = dclib::strdup(class_name);
	takeover_program = dclib::strdup(takeover_pgm);
	type = ha_type;
	ref_count = 0;

	//
	// If the device class is local then set the local_class
	// flag accordingly. This flag will define if we need to
	// setup device replicas for it or not.
	//
	if (strcmp(class_name, str_local_class) == 0)
		local_class = true;
	else
		local_class = false;
}

//
// Destructor for 'dc_service_class'.
//
dc_service_class::~dc_service_class()
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service_class::~dc_service_class().\n"));

	ASSERT(ref_count == 0);

	delete [] name;
	delete [] takeover_program;
}

//
// Helper function to get the user program to be run for services of
// this class in the form "takeover_program -C name".  It is upto the
// caller to free the memory allocated here.
//
char *
dc_service_class::get_user_program()
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service_class::get_user_program().\n"));

	char *tmp = new char[strlen(takeover_program) + strlen(name) + 10];
	//lint -e668
	(void) sprintf(tmp, "%s -C %s", takeover_program, name);
	//lint +e668

	DCS_DBPRINTF_G(DCS_TRACE_DC_SERVICE,
	    ("get_user_program(%p): command = '%s'.\n", this, tmp));

	return (tmp);
}

//
// Constructor for 'dc_callback_info'.
//
dc_callback_info::dc_callback_info(sol::major_t maj, sol::minor_t _min,
    fs::dc_callback_ptr cb_ptr, mdc::dc_mapper_ptr mapper_ptr)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_callback_info::dc_callback_info(%d, %d, %p, %p).\n",
	    maj, _min, cb_ptr, mapper_ptr));

	gmaj = maj;
	gmin = _min;
	cb_var = fs::dc_callback::_duplicate(cb_ptr);
	ASSERT(!CORBA::is_nil(cb_var));
	mapper_var = mdc::dc_mapper::_duplicate(mapper_ptr);
}

//
// Destructor for 'dc_callback_info'.
//
dc_callback_info::~dc_callback_info()
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_callback_info::~dc_callback_info().\n"));

	// Nothing to do here.
}

//
// Helper function to allocate and return a device range given all its
// components.
//
mdc::dev_range *
dc_service::alloc_range(major_t maj, minor_t start_minor, minor_t end_minor)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service::alloc_range(%d, %d, %d).\n",
	    maj, start_minor, end_minor));

	mdc::dev_range *tmp_range = new mdc::dev_range;

	tmp_range->maj = maj;
	tmp_range->start_minor = start_minor;
	tmp_range->end_minor = end_minor;

	return (tmp_range);
}

//
// Helper function that adds 'dev_range' to 'gdev_ranges' in sorted order.
//
void
dc_service::add_to_range_list(const mdc::dev_range &dev_range,
    mdc::dev_range_seq_t &gdev_ranges)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service::add_to_range_list(%p, %p).\n",
	    dev_range, gdev_ranges));

	static uint_t increment = 10;

	uint_t len = gdev_ranges.length();
	if (len == gdev_ranges.maximum()) {
		gdev_ranges.length(len + increment);
	}

	ASSERT(len < gdev_ranges.maximum());

	gdev_ranges.length(len+1);
	gdev_ranges[len] = dev_range;
}

//
// Constructor.
//
dc_service::dc_service(unsigned int sid,
    const char *service_nm,
    dc_service_class *service_class,
    bool failback_enabled,
    mdc::node_preference_seq_t *nodes,
    unsigned int num_secs,
    dc_minor_range_list *range_list,
    bool suspended)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service::dc_service(%d, %s, %p, %d, %p, %d, %p, %d).\n",
	    sid, service_nm, service_class, failback_enabled, nodes,
	    num_secs, range_list, suspended));

	service_name = dclib::strdup(service_nm);
	service_id = sid;
	_failback_enabled = failback_enabled;
	num_secondaries = num_secs;
	service_nodes = nodes;
	service_gdev_ranges = range_list;
	_suspended = suspended;
	_active = false;

	//
	// Increment the reference count on the service class - this will
	// prevent the service class from being removed.  We decrement this
	// reference count in the destructor of this object, which is called
	// when the service is being removed or this replica is no longer
	// the primary.
	//
	_service_class = service_class;
	_service_class->ref_count++;
}

//
// Destructor.
//
dc_service::~dc_service()
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE, ("dc_service::~dc_service().\n"));

	delete service_name;

	// We decrement the reference count from the constructor here
	_service_class->ref_count--;

	// Do this for lint.
	_service_class = NULL;
	delete _service_class;

	//
	// We empty the list here but do not free memory for the elements -
	// that is done in dc_major_store.
	//
	while (service_gdev_ranges->reapfirst()) {};
	delete service_gdev_ranges;

	//
	// We empty the list here but do not delete the elements - that is done
	// in dc_config_impl.
	//
	while (callbacks.reapfirst()) {};

	delete service_nodes;
}

//
// Helper function that returns the name of the CCR table for 'service_id'.
//
char *
dc_service::get_table_name(unsigned int _service_id)
{
	char *table_name;

	table_name = new char[10 + strlen(service_table_prefix) + 1];
	//lint -e668
	(void) sprintf(table_name, "%s%d", service_table_prefix, _service_id);
	//lint +e668

	return (table_name);
}

//
// Helper functions to convert between a bool and a string representing it
// in the CCR.
//
int
dc_service::conv_bool(const char *string, bool &b)
{
	ASSERT(string != NULL);

	//lint -e668
	if (strcmp(string, str_yes) == 0) {
	//lint +e668
		b = true;
		return (0);
	}
	//lint -e668
	if (strcmp(string, str_no) == 0) {
	//lint +e668
		b = false;
		return (0);
	}

	return (1);
}

int
dc_service::conv(bool b, const char *&string)
{
	if (b) {
		string = str_yes;
	} else {
		ASSERT(!b);
		string = str_no;
	}
	return (0);
}

//
// Helper functions to convert between an unsigned int and a string
// representing it in the CCR..
//
int
dc_service::conv_alloc(unsigned int i, char *&string)
{
	string = new char[10];
	//lint -e668
	(void) sprintf(string, "%d", i);
	//lint +e668
	return (0);
}

int
dc_service::conv_uint(const char *string, unsigned int &i)
{
	ASSERT(string != NULL);

	i = (uint_t)os::atoi(string);
	return (0);
}

//
// Helper functions to convert between a 'node_preference_seq_t' and a string
// representing it in the CCR..
//
int
dc_service::conv_alloc(const mdc::node_preference_seq_t &nodes, char *&string)
{
	char *tmp;
	uint_t count;
	uint_t length;
	char *pad = "";	// Padding between entries

	count = nodes.length();
	if (count == 0) {
		string = new char;
		*string = 0;
		return (0);
	}

	length = count * node_record_max_len + 1;
	string = new char[length];
	tmp = string;

	for (uint_t i = 0; i < count; i++) {
		//lint -e668
		(void) sprintf(tmp, "%s(%d,%d)", pad, nodes[i].id,
		    nodes[i].preference);
		//lint -e810
		tmp += strlen(tmp);
		//lint +e810
		//lint +e668
		pad = " ";
	}

	return (0);
}

int
dc_service::conv(char *string, mdc::node_preference_seq_t &nodes)
{
	ASSERT(string != NULL);

	//
	// The format that the node ids and preferences are stored in is:
	// "(id1,pref1) (id2,pref2) ...".
	// To parse this, we first break up the string by ")" and then by ",".
	//

	//lint -e668
	uint_t length = (uint_t)strlen(string);
	//lint +e668
	char **fields;

	uint_t in_count = 1 + (length/node_record_min_len);
	fields = new char *[in_count];

	int out_count = dclib::parse_string(string, ')', fields, in_count);
	if (out_count == -1) {
		delete [] fields;
		return (1);
	}

	ASSERT(out_count >= 0);

	nodes.length((uint_t)out_count);
	char *fields2[2];
	for (uint_t i = 0; i < (uint_t)out_count; i++) {
		//
		// If the node configuration string does not break up into
		// exactly two parts, return an error.
		//
		if (dclib::parse_string(fields[i], ',', fields2, 2) != 2) {
			delete [] fields;
			return (1);
		}

		// Skip over the '(' character to get the node id.
		if (conv_uint(fields2[0]+1, nodes[i].id)) {
			delete [] fields;
			return (1);
		}

		// Get the node preference
		if (conv_uint(fields2[1], nodes[i].preference)) {
			delete [] fields;
			return (1);
		}
	}

	delete [] fields;
	return (0);
}

//
// Helper functions to convert between a 'dc_minor_range_list' and a string
// representing it in the CCR..
//
int
dc_service::conv_alloc(dc_minor_range_list *gdev_ranges_list, char *&string)
{
	char *tmp;
	uint_t count;
	uint_t length;
	dc_minor_range *tmp_range;
	char *pad = "";	// Padding between entries
	char *major_name;

	count = gdev_ranges_list->count();

	if (count == 0) {
		string = new char;
		*string = 0;
		return (0);
	}

	length = count * gdev_record_max_len + 1;
	string = new char[length];
	tmp = string;

	dc_minor_range_list::ListIterator iterator(*gdev_ranges_list);

	for (; (tmp_range = iterator.get_current()) != NULL;
	    iterator.advance()) {
		//lint -e668
		if ((major_name = ddi_major_to_name(tmp_range->major_no))
		    != NULL) {
			(void) sprintf(tmp, "%s(%s,%d-%d)", pad,
			    major_name,
			    (int)(tmp_range->start_min),
			    (int)(tmp_range->end_min));
		} else {
			delete [] string;
			return (1);
		}
		//lint -e810
		tmp += strlen(tmp);
		//lint +e810
		//lint +e668
		pad = " ";
	}

	return (0);
}

int
dc_service::conv(char *string, mdc::dev_range_seq_t &gdev_ranges)
{
	ASSERT(string != NULL);

	//
	// The format that the dev_t values are stored in is:
	// "(major1,minor1-minor2) (major2,minor3-minor4) ...".
	// To parse this, we first break up the string by ')', then by ',',
	// and finally by '-'.
	//

	//lint -e668
	uint_t length = (uint_t)strlen(string);
	//lint +e668
	char **fields;
	int err = 0;

	mdc::dev_range tmp_range;

	uint_t in_count = 1 + (length/gdev_record_min_len);
	fields = new char *[in_count];

	//
	// Break up the string by ')' to get individual ranges pointed to by
	// 'fields'.
	//
	int out_count = dclib::parse_string(string, ')', fields, in_count);
	if (out_count == -1) {
		delete [] fields;
		return (1);
	}

	ASSERT(out_count >= 0);

	char *fields2[2];
	char *fields3[2];
	minor_t minor1, minor2;
	major_t	maj;

	for (uint_t i = 0; i < (uint_t)out_count; i++) {
		//
		// If the device range field does not break up into exactly two
		// parts, return an error.
		//
		if (dclib::parse_string(fields[i], ',', fields2, 2) != 2) {
			err = 1;
			goto out;
		}

		//
		// Skip over the '(' character to get the major number
		// XXX Check for a major number in the CCR file for backward
		// compatibility.
		//
		if (!os::ctype::is_digit(*(fields[0]+1))) {
			maj = ddi_name_to_major(fields2[0]+1);
			if (maj == (major_t)-1) {
				err = 1;
				goto out;
			}
		} else {
			if (conv_uint(fields2[0]+1, maj)) {
				err = 1;
				goto out;
			}
		}

		//
		// If the minor number record does not break up into exactly two
		// parts, return an error.
		//
		if (dclib::parse_string(fields2[1], '-', fields3, 2) != 2) {
			err = 1;
			goto out;
		}

		//
		// Otherwise, get the minor numbers stored in the string.
		//
		if (conv_uint(fields3[0], minor1)) {
			err = 1;
			goto out;
		}
		if (conv_uint(fields3[1], minor2)) {
			err = 1;
			goto out;
		}

		if (minor1 > minor2) {
			err = 1;
			goto out;
		}

		tmp_range.maj = maj;
		tmp_range.start_minor = minor1;
		tmp_range.end_minor = minor2;
		add_to_range_list(tmp_range, gdev_ranges);
	}

out:
	delete [] fields;
	return (err);
}

//
// Static function that takes in 'service_id' and attempts to read in
// configuration information about the service from the CCR.  If successful,
// this call returns an allocated 'dc_service' object, otherwise it returns
// NULL.
//
dc_service *
dc_service::read_from_ccr(unsigned int _service_id)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service::read_from_ccr(%d).\n", _service_id));

	CORBA::String_var _service_name;
	CORBA::String_var service_class_name;
	unsigned int num_secs;
	bool failback_enabled;
	bool suspended;
	ccr::readonly_table_var table;
	Environment env;
	int err;
	dc_error_t err_val;

	mdc::node_preference_seq_t *nodes = new mdc::node_preference_seq_t;

	// Lookup the CCR
	ccr::directory_var ccr_var = dclib::get_ccrobj();
	if (CORBA::is_nil(ccr_var)) {
		return (NULL);
	}

	char *tmp = get_table_name(_service_id);

	//
	// Lookup the table containing configuration information about this
	// service.
	//
	table = ccr_var->lookup(tmp, env);

	delete [] tmp;

	if (env.exception()) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
		    ("read_from_ccr(): exception '%s' while calling "
		    "%p->lookup().\n", env.exception()->_name(), ccr_var));
		RETURN_ON_EXCEPTION(env, "DCS: Error looking up services table",
		    NULL);
	}

	if (CORBA::is_nil(table)) {
		return (NULL);
	}

	// Read the name of the service
	_service_name = table->query_element(key_str_service_name, env);
	if (env.exception()) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
		    ("read_from_ccr(): exception '%s' while calling "
		    "%p->query_element(%s).\n", env.exception()->_name(),
		    key_str_service_name));
		RETURN_ON_EXCEPTION(env, "DCS: Error reading service name "
		    "from service table", NULL);
	}

	//
	// If a service with this name already exists, don't initialize this
	// one.
	//
	dc_services *services = dc_services::get_services();
	if (services->resolve_service(_service_name, dc_services::UNLOCKED)
	    != NULL) {
		DCS_DBPRINTF_A(DCS_TRACE_DC_SERVICE,
		    ("read_from_ccr(): service with name '%s' already "
		    "exist.\n", _service_name));

		os::warning("DCS: Duplicate service %s",
		    (const char *)_service_name);
		return (NULL);
	}

	// Read the name of the user program
	service_class_name = table->query_element(key_str_service_class, env);
	if (env.exception()) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
		    ("read_from_ccr(): exception '%s' while calling "
		    "%p->query_element(%s).\n", env.exception()->_name(),
		    table, key_str_service_class));
		RETURN_ON_EXCEPTION(env, "DCS: Error reading service class "
		    "from service table", NULL);
	}

	dc_service_class *service_class =
	    services->find_service_class(service_class_name);
	if (service_class == NULL) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
		    ("read_from_ccr(): service_class '%s' is NULL.\n",
		    service_class_name));
		os::warning("DCS: Invalid '%s' data in CCR table",
		    key_str_service_class);
		return (NULL);
	}

	// Read failback mode
	tmp = table->query_element(key_str_failback_enabled, env);
	if (env.exception()) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
		    ("read_from_ccr(): exception '%s' while calling "
		    "%p->query_element(%s).\n", env.exception()->_name(),
		    table, key_str_failback_enabled));
		RETURN_ON_EXCEPTION(env, "DCS: Error reading failback mode "
		    "from service table", NULL);
	}

	err = conv_bool((const char *)tmp, failback_enabled);
	delete [] tmp;
	if (err) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
		    ("read_from_ccr(): invalid data for '%s' in "
		    "CCR table.\n", key_str_failback_enabled));
		os::warning("DCS: Invalid '%s' data in CCR table",
		    key_str_failback_enabled);
		return (NULL);
	}

	// Read in the number of secondaries
	tmp = table->query_element(key_str_num_secs, env);
	if (env.exception()) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
		    ("read_from_ccr(): exception '%s' while calling "
		    "%p->query_element(%s).\n", env.exception()->_name(),
		    table, key_str_num_secs));
		RETURN_ON_EXCEPTION(env, "DCS: Error reading number of "
		    "secondaries from service table", NULL);
	}

	err = conv_uint((const char *)tmp, num_secs);
	delete [] tmp;
	if (err) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
		    ("read_from_ccr(): invalid data for '%s' in "
		    "CCR table.\n", key_str_num_secs));
		os::warning("DCS: Invalid '%s' data in CCR table",
		    key_str_num_secs);
		return (NULL);
	}

	// Read in the nodes
	tmp = table->query_element(key_str_nodes, env);
	if (env.exception()) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
		    ("read_from_ccr(): exception '%s' while calling "
		    "%p->query_element(%s).\n", env.exception()->_name(),
		    table, key_str_nodes));
		RETURN_ON_EXCEPTION(env, "DCS: Error reading nodes from "
		    "service table", NULL);
	}

	err = conv(tmp, *nodes);
	delete [] tmp;
	if (err) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
		    ("read_from_ccr(): invalid data for '%s' in "
		    "CCR table.\n", key_str_nodes));
		os::warning("DCS: Invalid '%s' data in CCR table",
		    key_str_nodes);
		return (NULL);
	}

	// Read suspended mode
	tmp = table->query_element(key_str_suspended, env);
	if (env.exception()) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
		    ("read_from_ccr(): exception '%s' while calling "
		    "%p->query_element(%s).\n", env.exception()->_name(),
		    table, key_str_suspended));
		RETURN_ON_EXCEPTION(env, "DCS: Error reading suspended mode "
		    "from service table", NULL);
	}

	err = conv_bool((const char *)tmp, suspended);
	delete [] tmp;
	if (err) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
		    ("read_from_ccr(): invalid data for '%s' in "
		    "CCR table.\n", key_str_suspended));
		os::warning("DCS: Invalid '%s' data in CCR table",
		    key_str_suspended);
		return (NULL);
	}

	// Read in the devices
	tmp = table->query_element(key_str_devices, env);
	if (env.exception()) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
		    ("read_from_ccr(): exception '%s' while calling "
		    "%p->query_element(%s).\n", env.exception()->_name(),
		    table, key_str_devices));
		RETURN_ON_EXCEPTION(env, "DCS: Error reading devices from "
		    "service table", NULL);
	}

	mdc::dev_range_seq_t gdev_ranges;
	err = conv(tmp, gdev_ranges);
	delete [] tmp;
	if (err) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
		    ("read_from_ccr(): invalid data for '%s' in "
		    "CCR table.\n", key_str_devices));
		os::warning("DCS: Invalid '%s' data in CCR table",
		    key_str_devices);
		return (NULL);
	}

	dc_storage *dcstore = dc_storage::storage();

	// Check to see that all the ranges read in from file are valid.
	if ((err_val = dcstore->possible_mapping(add_mapping, gdev_ranges,
	    _service_id)) != DCS_SUCCESS) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
		    ("read_from_ccr(): invalid data for '%s' in "
		    "CCR table.\n", key_str_devices));
		os::warning("DCS: Invalid '%s' data in CCR table",
		    key_str_devices);
		return (NULL);
	}

	dc_minor_range_list *gdev_ranges_list = new dc_minor_range_list;

	// All the ranges are valid - add them.
	err_val = dcstore->process_mapping(add_mapping, gdev_ranges,
	    _service_id, gdev_ranges_list);
	ASSERT(err_val == DCS_SUCCESS);

	dc_service *_service = new dc_service(_service_id, _service_name,
	    service_class, failback_enabled, nodes, num_secs,
	    gdev_ranges_list, suspended);

	return (_service);
}

//
// Initialize the device ranges that this service points to to 'new_list'.
//
void
dc_service::reinit_devices(dc_minor_range_list *new_list)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service::reinit_devices(%p).\n", new_list));

	ASSERT(new_list != NULL);

	dc_storage *dcstore = dc_storage::storage();

	// Clear the current mapping.
	dcstore->clear_mapping(service_id, service_gdev_ranges);

	// Delete 'service_gdev_ranges'...
	ASSERT(service_gdev_ranges->empty());
	delete service_gdev_ranges;

	// ... and reinitialize it.
	service_gdev_ranges = new_list;
	dcstore->add_mapping_nocheck(service_gdev_ranges);
}

//
// Get rid of this CCR table.
//
dc_error_t
dc_service::erase_ccr_table()
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service::erase_ccr_table().\n"));

	Environment env;
	CORBA::String_var table_name;

	// Lookup the CCR
	ccr::directory_var ccr_var = dclib::get_ccrobj();
	if (CORBA::is_nil(ccr_var)) {
		DCS_DBPRINTF_A(DCS_TRACE_DC_SERVICE,
		    ("erase_ccr_table(%p): get_ccrobj() failed.\n", this));

		return (DCS_ERR_CCR_ACCESS);
	}

	// Get the name of the CCR table associated with this service.
	table_name = get_table_name(service_id);

	//
	// If remove_table() raises no_such_table exception, we return
	// DCS_ERR_SERVICE_NAME. When dc_services::create_device_service()
	// calls erase_ccr_table() and the return value is
	// DCS_ERR_SERVICE_NAME, no error message is printed. This is because
	// the table was not created to begin with. Hence, CCR is in a
	// consistent state even though erase_ccr_table() raised an exception.
	//
	CORBA::Exception *ex;
	ccr_var->remove_table(table_name, env);
	if ((ex = env.exception()) != NULL) {
	    if (ccr::no_such_table::_exnarrow(ex) != NULL) {
		    DCS_DBPRINTF_A(DCS_TRACE_DC_SERVICE,
			("erase_ccr_table(%p): exception '%s' while calling "
			"remove_table(%s).\n", this, ex->_name(),
			(const char *) table_name));
		return (DCS_ERR_SERVICE_NAME);
	    } else {
		    DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
			("erase_ccr_table(%p): exception '%s' while calling "
			"remove_table(%s).\n", this, ex->_name(),
			(const char *) table_name));

		    RETURN_ON_EXCEPTION(env,
			"DCS: Error removing service table",
			DCS_ERR_CCR_ACCESS);
	    }
	}

	return (DCS_SUCCESS);
}

//
// Helper function that writes out either all or a subset of keys to the
// CCR table associated with this service.
//
dc_error_t
dc_service::write_to_ccr(write_type_t type,
    const mdc::service_property_seq_t *properties)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service::write_to_ccr(%d, %p).\n", type, properties));

	Environment env;
	CORBA::String_var table_name;
	dc_error_t err;
	ccr::updatable_table_var table;

#ifdef _FAULT_INJECTION
	if (fault_triggered(FAULTNUM_DCS_WRITECCR_S_B, NULL, NULL)) {
		DCS_DBPRINTF_G(DCS_TRACE_DC_SERVICE,
		    ("write_to_ccr(%p): fault_triggered returned true for "
		    "'%x'.\n", this, FAULTNUM_DCS_WRITECCR_S_B));
		return (DCS_ERR_CCR_ACCESS);
	}
#endif

	if (type == key_all) {
		//
		// This is the first time - we need to create this table.
		//
		table = get_updatable_table(true);
	} else {
		table = get_updatable_table(false);
	}

	if (CORBA::is_nil(table)) {
		DCS_DBPRINTF_A(DCS_TRACE_DC_SERVICE,
		    ("write_to_ccr(%p): table is null.\n", this));

		return (DCS_ERR_CCR_ACCESS);
	}

	err = write_internal(table, type, properties);

#ifdef _FAULT_INJECTION
	if (fault_triggered(FAULTNUM_DCS_WRITECCR_S_A, NULL, NULL)) {
		DCS_DBPRINTF_G(DCS_TRACE_DC_SERVICE,
		    ("write_to_ccr(%p): fault_triggered returned true for "
		    "'%x'.\n", this, FAULTNUM_DCS_WRITECCR_S_A));
		return (DCS_ERR_CCR_ACCESS);
	}
#endif

	if (err) {
		table->abort_transaction(env);
		if (env.exception()) {
			DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
			    ("write_to_ccr(%p): exception '%s' while calling "
			    "abort_transaction().\n", this,
			    env.exception()->_name()));
		}
	} else {
		table->commit_transaction(env);
		if (env.exception()) {
			DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
			    ("write_to_ccr(%p): exception '%s' while calling "
			    "abort_transaction().\n", this,
			    env.exception()->_name()));
		}
	}

	RETURN_ON_EXCEPTION(env, "DCS: Error accessing CCR table",
	    DCS_ERR_CCR_ACCESS);

#ifdef _FAULT_INJECTION
	if (fault_triggered(FAULTNUM_DCS_WRITECCR_S_E, NULL, NULL)) {
		DCS_DBPRINTF_G(DCS_TRACE_DC_SERVICE,
		    ("write_to_ccr(%p): fault_triggered returned true for "
		    "'%x'.\n", this, FAULTNUM_DCS_WRITECCR_S_E));
		return (DCS_ERR_CCR_ACCESS);
	}
#endif

	return (err);
}

//
// Helper function to add the element (key, data) to the element sequence
// 'elems' at the position specified by 'count'.  The function also increments
// 'count'.
//
void
dc_service::add_element(ccr::element_seq *elems, uint_t &count, const char *key,
    const char *data)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service::add_element(%p, %d, %s, %s).\n",
	    elems, count, key, data));

	// Confirm that we have enough space.
	ASSERT(elems->length() > count);

	(*elems)[count].key = key;
	(*elems)[count++].data = data;
}

//
// Helper routine called by 'write_table'.
//
dc_error_t
dc_service::write_internal(ccr::updatable_table_ptr table, write_type_t type,
    const mdc::service_property_seq_t *properties)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service::write_internal(%p, %d, %p).\n",
	    table, type, properties));

	ASSERT(!CORBA::is_nil(table));

	char *tmp;
	Environment env;
	uint_t i, count = 0, len = 0;
	ccr::element_seq *elems;
	CORBA::Exception *ex;
	dc_error_t err = DCS_SUCCESS;

	switch (type) {
	case key_all:
		count = number_of_system_keys;
		if (properties != NULL) {
			for (i = 0; i < properties->length(); i++) {
				if (!valid_property_name(
				    (*properties)[i].name)) {
					DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
					    ("write_internal(%p): invalid "
					    "property name '%s'.\n", this,
					    (*properties)[i].name));
					return (DCS_ERR_PROPERTY_NAME);
				}
			}
			count += properties->length();
		}

		elems = new ccr::element_seq(count, count);
		count = 0;

		add_element(elems, count, key_str_service_name, service_name);
		add_element(elems, count, key_str_service_class,
		    _service_class->name);

		if (conv(_failback_enabled, (const char *)tmp)) {
			delete elems;
			DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
			    ("write_internal(%p): conv() failed.\n", this));
			return (DCS_ERR_CCR_ACCESS);
		}
		add_element(elems, count, key_str_failback_enabled, tmp);

		if (conv(_suspended, (const char *)tmp)) {
			delete elems;
			DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
			    ("write_internal(%p): conv() failed.\n", this));
			return (DCS_ERR_CCR_ACCESS);
		}
		add_element(elems, count, key_str_suspended, tmp);

		if (conv_alloc(num_secondaries, tmp)) {
			delete elems;
			DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
			    ("write_internal(%p): conv_alloc() failed.\n",
			    this));
			return (DCS_ERR_CCR_ACCESS);
		}
		add_element(elems, count, key_str_num_secs, tmp);
		delete [] tmp;

		if (conv_alloc(*service_nodes, tmp)) {
			delete elems;
			DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
			    ("write_internal(%p): conv_alloc() failed.\n",
			    this));
			return (DCS_ERR_CCR_ACCESS);
		}
		add_element(elems, count, key_str_nodes, tmp);
		delete [] tmp;

		if (conv_alloc(service_gdev_ranges, tmp)) {
			delete elems;
			DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
			    ("write_internal(%p): conv_alloc() failed.\n",
			    this));
			return (DCS_ERR_CCR_ACCESS);
		}
		add_element(elems, count, key_str_devices, tmp);
		delete [] tmp;

		if (properties != NULL) {
			uint_t prop_len = properties->length();
			for (i = 0; i < prop_len; i++) {
				add_element(elems, count, (*properties)[i].name,
				    (*properties)[i].value);
			}
		}

		table->add_elements(*elems, env);
		if (env.exception()) {
			DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
			    ("write_internal(%p): exception '%s' while "
			    "calling add_elements(%p).\n", this,
			    env.exception()->_name(), elems));
		}
		delete elems;
		RETURN_ON_EXCEPTION(env, "DCS: Error writing elements to "
		    "service table", DCS_ERR_CCR_ACCESS);

		return (DCS_SUCCESS);

	case key_failback_enabled:
		if (conv(_failback_enabled, (const char *)tmp)) {
			DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
			    ("write_internal(%p): conv() failed.\n",
			    this));
			return (DCS_ERR_CCR_ACCESS);
		}
		table->update_element(key_str_failback_enabled, tmp, env);
		if (env.exception()) {
			DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
			    ("write_internal(%p): exception '%s' while "
			    "calling update_element(%s, %s).\n", this,
			    env.exception()->_name(),
			    key_str_failback_enabled, tmp));
		}
		RETURN_ON_EXCEPTION(env, "DCS: Error updating key in"
		    "service table", DCS_ERR_CCR_ACCESS);

		DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
		    ("} write_internal().\n"));
		return (DCS_SUCCESS);

	case key_suspended:
		if (conv(_suspended, (const char *)tmp)) {
			DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
			    ("write_internal(%p): conv() failed.\n", this));
			return (DCS_ERR_CCR_ACCESS);
		}
		table->update_element(key_str_suspended, tmp, env);
		if (env.exception()) {
			DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
			    ("write_internal(%p): exception '%s' while "
			    "calling update_element(%s, %s).\n", this,
			    env.exception()->_name(),
			    key_str_suspended, tmp));
		}
		RETURN_ON_EXCEPTION(env, "DCS: Error updating key in"
		    "service table", DCS_ERR_CCR_ACCESS);

		return (DCS_SUCCESS);

	case key_num_secs:
		if (conv_alloc(num_secondaries, tmp)) {
			DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
			    ("write_internal(%p): conv_alloc() failed.\n",
			    this));
			return (DCS_ERR_CCR_ACCESS);
		}
		table->update_element(key_str_num_secs, tmp, env);
		if (env.exception()) {
			DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
			    ("write_internal(%p): exception '%s' while "
			    "calling update_element(%s, %s).\n", this,
			    env.exception()->_name(),
			    key_str_num_secs, tmp));
		}
		delete [] tmp;
		RETURN_ON_EXCEPTION(env, "DCS: Error updating key in"
		    "service table", DCS_ERR_CCR_ACCESS);

		DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
		    ("} write_internal().\n"));
		return (DCS_SUCCESS);

	case key_nodes:
		if (conv_alloc(*service_nodes, tmp)) {
			DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
			    ("write_internal(%p): conv_alloc() failed.\n",
			    this));
			return (DCS_ERR_CCR_ACCESS);
		}

		table->update_element(key_str_nodes, tmp, env);
		if (env.exception()) {
			DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
			    ("write_internal(%p): exception '%s' while "
			    "calling update_element(%s, %s).\n", this,
			    env.exception()->_name(),
			    key_str_nodes, tmp));
		}
		delete [] tmp;
		RETURN_ON_EXCEPTION(env, "DCS: Error updating key in"
		    "service table", DCS_ERR_CCR_ACCESS);

		DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
		    ("} write_internal().\n"));
		return (DCS_SUCCESS);

	case key_devices:
		if (conv_alloc(service_gdev_ranges, tmp)) {
			DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
			    ("write_internal(%p): conv() failed.\n", this));
			return (DCS_ERR_CCR_ACCESS);
		}
		table->update_element(key_str_devices, (const char *)tmp, env);
		if (env.exception()) {
			DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
			    ("write_internal(%p): exception '%s' while "
			    "calling update_element(%s, %s).\n", this,
			    env.exception()->_name(),
			    key_str_devices, tmp));
		}
		delete [] tmp;
		RETURN_ON_EXCEPTION(env, "DCS: Error updating key in"
		    "service table", DCS_ERR_CCR_ACCESS);

		return (DCS_SUCCESS);

	case key_properties:
		if (properties == NULL) {
			DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
			    ("write_internal(%p): properties is NULL.\n",
			    this));
			return (DCS_ERR_UNEXPECTED);
		}
		//
		// Check for validity of the properties
		//
		for (i = 0; i < properties->length(); i++) {
			if (!valid_property_name(
			    (*properties)[i].name)) {
				DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
				    ("write_internal(%p): invalid property"
				    " name '%s'\n", this,
				    (*properties)[i].name));

				return (DCS_ERR_PROPERTY_NAME);
			}
		}
		count = properties->length();

		elems = new ccr::element_seq(count, count);
		count = 0;
		len = properties->length();
		for (i = 0; i < len; i++) {
			add_element(elems, count, (*properties)[i].name,
			    (*properties)[i].value);
		}

		table->add_elements(*elems, env);
		delete elems;
		if ((ex = env.exception()) != NULL) {
			if (ccr::key_exists::_exnarrow(ex) != NULL) {
				//
				// One of the keys already exist - Now
				// try to add serially
				//
				env.clear();
				for (i = 0; i < properties->length(); i++) {
					err = add_ccr_property(table,
					    (*properties)[i].name,
					    (*properties)[i].value);
					if (err != DCS_SUCCESS) {
						DCS_DBPRINTF_R(
						    DCS_TRACE_DC_SERVICE,
						    ("write_internal(%p): "
						    "add_ccr_property(%s) "
						    "failed.\n",
						    this, (*properties)[i].\
						    name));
						return (err);
					}
				}
				return (DCS_SUCCESS);
			}
			DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
			    ("write_internal(%p): exception '%s' while "
			    "calling add_elements().\n", this,
			    ex->_name()));
		}

		RETURN_ON_EXCEPTION(env,
		    "DCS: Error writing elements to service table",
		    DCS_ERR_CCR_ACCESS);

		return (DCS_SUCCESS);

	default:
		/* NOTREACHED */
		ASSERT(0);
	}

	/* NOTREACHED */
	ASSERT(0);

	DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
	    ("write_internal(%p): end of function reached!\n", this));

	// for lint : function dc_service::write_internal should return a value
	return (DCS_ERR_UNEXPECTED);
}

// Helper function to determine the validity of a property name.
bool
dc_service::valid_property_name(const char *name)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service::valid_property_name(%s).\n", name));

	//
	// Property names cannot begin with 'DCS_' - those names are reserved
	// for DCS use.
	//
	return (strncmp(name, "DCS_", (size_t)4) != 0);
}

//
// Helper function for writing out individual properties to the CCR.
//
dc_error_t
dc_service::add_ccr_property(ccr::updatable_table_ptr table, const char *name,
    const char *value)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service::add_ccr_property(%p, %s, %s).\n",
	    table, name, value));

	Environment env;
	CORBA::Exception *ex;

	// Check for validity.
	if (!valid_property_name(name)) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
		    ("add_ccr_property(): valid_property_name(%s) failed.\n",
		    this, name));
		return (DCS_ERR_PROPERTY_NAME);
	}

	//
	// Try adding the property to the table as a new element.
	//
	table->add_element(name, value, env);
	if ((ex = env.exception()) != NULL) {
		if (ccr::key_exists::_exnarrow(ex) != NULL) {
			//
			// This key already exists - use the 'update_element'
			// call to update the value.
			//
			env.clear();
			table->update_element(name, value, env);
		}
	}

	if (env.exception()) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
		    ("add_ccr_property(%p): exception '%s' while calling "
		    "%p->add_element(%s, %s).\n", this,
		    env.exception()->_name(), table, name, value));
		RETURN_ON_EXCEPTION(env, "DCS: Error updating key in service "
		    "table", DCS_ERR_CCR_ACCESS);
	}

	return (DCS_SUCCESS);
}

//
// Helper function for removing a property from the CCR.
//
dc_error_t
dc_service::remove_ccr_property(ccr::updatable_table_ptr table,
    const char *name)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service::remove_ccr_property(%p, %s).\n",
	    table, name));

	Environment env;
	CORBA::Exception *ex;

	// Check for validity.
	if (!valid_property_name(name)) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
		    ("remove_ccr_property(%p): invalid property name '%s'.\n",
		    this, name));

		return (DCS_ERR_PROPERTY_NAME);
	}

	//
	// Try removing the element from the table.
	//
	table->remove_element(name, env);
	if ((ex = env.exception()) != NULL) {
		if (ccr::no_such_key::_exnarrow(ex) != NULL) {
			// No such element exists - succeed silently.
			env.clear();
			DCS_DBPRINTF_G(DCS_TRACE_DC_SERVICE,
			    ("remove_ccr_property(%p): exception '%s' while "
			    "calling %p->remove_element(%s).\n",
			    this, ex->_name(), table, name));

			return (DCS_SUCCESS);
		}
		env.clear();
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
		    ("remove_ccr_property(%p): exception '%s' while calling "
		    "%p->remove_element(%s).\n", this, ex->_name(),
		    table, name));

		return (DCS_ERR_CCR_ACCESS);
	}

	return (DCS_SUCCESS);
}

//
// Interface to update 'failback_enabled'.
//
dc_error_t
dc_service::set_failback_mode(bool failback_enabled)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service::set_failback_mode(%d).\n", failback_enabled));

	ASSERT(access.lock_held());

	//lint -e731 Boolean argument to equal/not equal
	if (_failback_enabled == failback_enabled) {
	//lint +e731
		return (DCS_SUCCESS);
	}

	_failback_enabled = failback_enabled;
	dc_error_t err_val = write_to_ccr(key_failback_enabled);
	if (err_val != DCS_SUCCESS) {
		// Restore the previous value.
		_failback_enabled = !_failback_enabled;
	}
	return (err_val);
}

//
// Interface to update the 'suspended' state.
//
dc_error_t
dc_service::set_suspended(bool suspended)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service::set_suspended(%d).\n", suspended));

	ASSERT(access.lock_held());

	//lint -e731 Boolean argument to equal/not equal
	if (suspended == _suspended) {
	//lint +e731
		return (DCS_SUCCESS);
	}

	_suspended = suspended;
	dc_error_t err_val = write_to_ccr(key_suspended);
	if (err_val != DCS_SUCCESS) {
		// Restore the previous value.
		_suspended = !_suspended;
	}
	return (err_val);
}

//
// Interface to update 'num_secondaries'
//
dc_error_t
dc_service::set_number_of_secondaries(unsigned int num_secs)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service::set_number_of_secondaries(%d).\n", num_secs));

	ASSERT(access.lock_held());

	if (num_secondaries == num_secs) {
		return (DCS_SUCCESS);
	}

	unsigned int tmp_num_secs = num_secondaries;
	num_secondaries = num_secs;
	dc_error_t err_val = write_to_ccr(key_num_secs);
	if (err_val != DCS_SUCCESS) {
		// Restore the previous value.
		num_secondaries = tmp_num_secs;
	}
	return (err_val);
}

//
// Interface to update 'service_nodes'.  This call is used to add a new node
// and also to change the preference of an existing node.
//
dc_error_t
dc_service::add_node(const mdc::node_preference &node)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service::add_node(%p).\n", node));

	uint_t length;
	dc_error_t err_val;

	ASSERT(access.lock_held());
	length = service_nodes->length();
	for (uint_t i = 0; i < length; i++) {
		if ((*service_nodes)[i].id == node.id) {
			// Found the node.  Update the preference value.
			if ((*service_nodes)[i].preference == node.preference) {
				return (DCS_SUCCESS);
			}
			(*service_nodes)[i].preference = node.preference;
			err_val = write_to_ccr(key_nodes);
			return (err_val);
		}
	}
	// New node.  Add it to the list.
	(*service_nodes).length(length + 1);
	(*service_nodes)[length] = node;
	err_val = write_to_ccr(key_nodes);
	return (err_val);
}

//
// Interface to remove a node from 'service_nodes'.
//
dc_error_t
dc_service::remove_node(sol::nodeid_t node_id)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service::remove_node(%d).\n", node_id));

	mdc::node_preference_seq_t nodes;
	uint_t length;
	dc_error_t err_val;

	ASSERT(access.lock_held());
	length = service_nodes->length();
	for (uint_t i = 0; i < length; i++) {
		if ((*service_nodes)[i].id == node_id) {
			// Found it.  Remove.
			for (uint_t j = i; j < length-1; j++) {
				(*service_nodes)[j] = (*service_nodes)[j+1];
			}
			(*service_nodes).length(length - 1);
			err_val = write_to_ccr(key_nodes);
			return (err_val);
		}
	}

	// Could not find the node.  Just return.
	return (DCS_ERR_NODE_ID);
}

//
// Helper function that gets a handle to the CCR table associated with this
// service.
//
ccr::updatable_table_ptr
dc_service::get_updatable_table(bool _create)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service::get_updatable_table(%d).\n", _create));

	// Get the name of the CCR table associated with this service.
	CORBA::String_var table_name = get_table_name(service_id);

	//
	// Return the table containing the list of services, creating it if
	// necessary.
	//
	return (dclib::get_updatable_table(table_name, _create));
}

//
// Interface to add properties atomically to a device service.
// Note that properties are not cached - they are retrieved from the CCR
// everytime it is necessary to do so, so there is no update of in-memory
// state here.
//
dc_error_t
dc_service::set_properties(const mdc::service_property_seq_t &properties)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service::set_properties(%p).\n", properties));

	Environment env;
	dc_error_t err = DCS_SUCCESS;

	//
	// Get an updatable handle to the CCR table associated with this
	// service.
	//
	unsigned int count = properties.length();
	ccr::updatable_table_var table = get_updatable_table(false);

	//
	// Write all the properties in one update if possible.  We commit the
	// CCR transaction only if all properties were added successfully -
	// hence the atomicity.
	//
	if (count > 0)
		err = write_internal(table, key_properties, &properties);

	if (err != DCS_SUCCESS) {
		table->abort_transaction(env);
		env.clear();
		return (err);
	}

	table->commit_transaction(env);
	if (env.exception()) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
		    ("set_properties(%p): exception '%s' while calling "
		    "%p->commit_transaction().\n", this,
		    env.exception()->_name(), table));
		RETURN_ON_EXCEPTION(env, "DCS: Error accessing CCR table",
		    DCS_ERR_CCR_ACCESS);
	}

	return (DCS_SUCCESS);
}

//
// Interface to remove properties atomically from a device service.
//
dc_error_t
dc_service::remove_properties(const sol::string_seq_t &properties)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service::remove_properties(%p) {\n", properties));

	Environment env;
	CORBA::Exception *ex;
	dc_error_t err = DCS_SUCCESS;
	ccr::element_seq *elems;
	uint_t i, prop_len = 0;

	// Check for validity of the properties
	for (i = 0; i < properties.length(); i++) {
		if (!valid_property_name(properties[i])) {
			DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
			    ("remove_properties(%p): invalid property name "
			    "'%s'.\n", this, properties[i]));
			return (DCS_ERR_PROPERTY_NAME);
		}
	}

	unsigned int count = properties.length();
	elems = new ccr::element_seq(count, count);
	for (i = 0; i < count; i++) {
		add_element(elems, prop_len, properties[i], (const char *)"");
	}

	//
	// Get an updatable handle to the CCR table associated with this
	// service.
	//
	ccr::updatable_table_var table = get_updatable_table(false);

	//
	// Delete the properties in one update if possible. We commit the
	// CCR transaction only if all properties were added successfully -
	// hence the atomicity.
	//
	table->remove_elements(*elems, env);
	delete elems;
	if ((ex = env.exception()) != NULL) {
		if (ccr::no_such_key::_exnarrow(ex) != NULL) {
			//
			// One of the keys doesn't exist - Now
			// try to delete serially
			//
			for (i = 0; i < properties.length(); i++) {
				err = remove_ccr_property(table, properties[i]);
				if (err != DCS_SUCCESS)
					break;
			}
		} else {
			DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
			    ("remove_properties(%p): exception '%s' while "
			    "calling %p->remove_elements().\n",
			    this, ex->_name(), table));
			err = DCS_ERR_CCR_ACCESS;
		}
	}

	env.clear();
	if (err != DCS_SUCCESS) {
		table->abort_transaction(env);
		env.clear();
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
		    ("remove_properties(%p): transaction aborted on table "
		    "%p.\n", this, table));
		return (err);
	}

	table->commit_transaction(env);
	if (env.exception()) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
		    ("set_properties(%p): exception '%s' while calling "
		    "%p->commit_transaction().\n", this,
		    env.exception()->_name(), table));
		RETURN_ON_EXCEPTION(env, "DCS: Error accessing CCR table",
		    DCS_ERR_CCR_ACCESS);
	}

	return (DCS_SUCCESS);
}

//
// Interface to write 'service_gdev_ranges' out to the CCR.
//
dc_error_t
dc_service::write_devices()
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service::write_devices().\n"));
	ASSERT(access.lock_held());

	return (write_to_ccr(key_devices));
}

//
// Interface to get a particular property value from the CCR.
//
dc_error_t
dc_service::get_property(const char *property_name,
    CORBA::String_out property_value)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service::get_property(%s).\n", property_name));

	ASSERT(access.lock_held());
	char *value = NULL;
	Environment env;
	CORBA::Exception *ex;

	// Get a readonly handle to the CCR table for this service.

	char *tmp = get_table_name(service_id);
	ccr::readonly_table_var table = dclib::get_readonly_table(tmp, false);
	delete [] tmp;

	if (CORBA::is_nil(table)) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
		    ("get_property(%p): table is null.\n", this));
		return (DCS_ERR_CCR_ACCESS);
	}

	// Read the property value
	value = table->query_element(property_name, env);
	if ((ex = env.exception()) != NULL) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
		    ("get_property(%p): exception '%s' while calling "
		    "%p->query_element(%s).\n",
		    this, ex->_name(), table, property_name));

		if (ccr::no_such_key::_exnarrow(ex) != NULL) {
			env.clear();
			return (DCS_ERR_PROPERTY_NAME);
		}

		RETURN_ON_EXCEPTION(env, "DCS: Error reading service table",
		    DCS_ERR_CCR_ACCESS);
	}

	property_value = value;
	return (DCS_SUCCESS);
}

//
// Interface to get all existing properties for this service from the CCR.
//
dc_error_t
dc_service::get_all_properties(
    mdc::service_property_seq_t_out properties)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service::get_all_properties().\n"));

	ASSERT(access.lock_held());
	Environment env;

	// Get a readonly handle to the CCR table for this service.

	char *tmp = get_table_name(service_id);
	ccr::readonly_table_var table = dclib::get_readonly_table(tmp, false);
	delete [] tmp;

	if (CORBA::is_nil(table)) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
		    ("get_all_properties(%p): table is null.\n", this));

		return (DCS_ERR_CCR_ACCESS);
	}

	// Read in all the records of this table.
	uint32_t actual = 0;
	ccr::element_seq_var elems;

	//
	// cast to satisfy lint.  get_num_elements will return -1 on error,
	// but it also sets env, and we check this
	//
	unsigned int count = (unsigned int) table->get_num_elements(env);
	if (env.exception()) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
		    ("get_all_properties(%p): exception '%s' while calling "
		    "%p->get_num_elements().\n", this,
		    env.exception()->_name(), table));
		RETURN_ON_EXCEPTION(env, "DCS: Error reading service table",
		    DCS_ERR_CCR_ACCESS);
	}

	if (count > 0) {
		table->atfirst(env);
		if (env.exception()) {
			DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
			    ("get_all_properties(%p): exception '%s' while "
			    "calling %p->atfirst().\n", this,
			    env.exception()->_name(), table));
			RETURN_ON_EXCEPTION(env, "DCS: Error reading service "
			    "table", DCS_ERR_CCR_ACCESS);
		}

		table->next_n_elements((uint32_t)count, elems, env);
		if (env.exception()) {
			DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
			    ("get_all_properties(%p): exception '%s' while "
			    "calling %p->next_n_elements(%d, %p).\n",
			    this, env.exception()->_name(), table, count,
			    elems));
			RETURN_ON_EXCEPTION(env, "DCS: Error reading service "
			    "table", DCS_ERR_CCR_ACCESS);
		}
		actual = elems->length();
	}

	if (actual != count) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
		    ("get_all_properties(%p): actual (%d) != count (%d).\n",
		    this, actual, count));

		return (DCS_ERR_CCR_ACCESS);
	}

	//
	// We now need to initialize the property values.  We treat any key
	// value beginning with "DCS_" as a system property and do not return
	// it to the user.
	// Note that the set_properties() call will fail if any of the
	// properties specified begin with "DCS_".
	//

	mdc::service_property_seq_t *read_properties =
	    new mdc::service_property_seq_t;
	read_properties->length(actual - number_of_system_keys);
	uint_t properties_count = 0;
	for (uint_t i = 0; i < actual; i++) {
		if (strncmp("DCS_", elems[i].key, (size_t)4) == 0) {
			// This is a systen key - skip it.
			continue;
		}
		if (properties_count > (actual - number_of_system_keys - 1)) {
			// Too few system keys for this service.
			delete (read_properties);
			return (DCS_ERR_CCR_ACCESS);
		}
		(*read_properties)[properties_count].name =
		    (const char *)elems[i].key;
		(*read_properties)[properties_count].value =
		    (const char *)elems[i].data;
		properties_count++;
	}

	//
	// We reset the number of properties to the number initialized here.
	// This will take care of the paranoid case where a crazy system
	// administrator edits the CCR table for this service by hand and
	// adds a non-system property beginning with "DCS_".
	//
	read_properties->length(properties_count);

	properties = read_properties;

	return (DCS_SUCCESS);
}

#if SOL_VERSION > __s10
//
// Function to atomicly test if a SVM device name is registered, delete
// instances with duplicate device names, and register device name for
// an instance. It returns make_links which is true if a correct instance
// to device name is found.
//
dc_error_t
dc_service::test_and_register_name(const char *instance,
		const char *dev_name, bool *make_links)
{
	Environment env;
	dc_error_t error = DCS_SUCCESS;
	ccr::updatable_table_var wtable;
	ccr::readonly_table_var rtable;
	uint32_t actual = 0;
	ccr::element_seq_var elems;
	char *tmp;
	char *disk;
	bool updated = false;
	bool deleted = false;
	bool matched = false;
	CORBA::Exception *ex;

	// Get a writeable CCR handle and begin transaction.
	wtable = get_updatable_table(false);
	// Get a readonly CCR handle to read the existing properties.
	tmp = get_table_name(service_id);
	rtable = dclib::get_readonly_table(tmp, false);
	delete [] tmp;
	if (CORBA::is_nil(rtable)) {
		DCS_DBPRINTF_A(DCS_TRACE_DC_SERVICE,
		    ("test_and_register_name(%p): table is null.\n", this));
		return (DCS_ERR_CCR_ACCESS);
	}
	// Read the current CCR table into memory.
	unsigned count = (unsigned)rtable->get_num_elements(env);
	if (env.exception()) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
		    ("test_and_register_name(%p): exception '%s' while calling "
		    "%p->get_num_elements().\n", this,
		    env.exception()->_name(), rtable));
		RETURN_ON_EXCEPTION(env, "DCS: Error reading service table",
		    DCS_ERR_CCR_ACCESS);
	}
	if (count > 0) {
		rtable->atfirst(env);
		if (env.exception()) {
			DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
			    ("test_and_register_name(%p): exception '%s' while "
			    "calling %p->atfirst().\n", this,
			    env.exception()->_name(), rtable));
			RETURN_ON_EXCEPTION(env, "DCS: Error reading service "
			    "table", DCS_ERR_CCR_ACCESS);
		}
		rtable->next_n_elements((uint32_t)count, elems, env);
		if (env.exception()) {
			DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
			    ("test_and_register_name(%p): exception '%s' while "
			    "calling %p->next_n_elements(%d, %p).\n",
			    this, env.exception()->_name(), rtable, count,
			    elems));
			RETURN_ON_EXCEPTION(env, "DCS: Error reading service "
			    "table", DCS_ERR_CCR_ACCESS);
		}
		actual = elems->length();
	}
	// For some reason we did not read in all of the table.
	if (actual != count) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
		    ("test_and_register_name(%p): actual (%d) != count (%d).\n",
		    this, actual, count));
		return (DCS_ERR_CCR_ACCESS);
	}
	// Scan the CCR looking for matching names. Delete incorrect entries.
	for (uint_t i = 0; i < actual; i++) {
		if (strncmp("d_", elems[i].key, (size_t)2) != 0)
			continue;
		if (strcmp(dev_name, elems[i].data) == 0) {
			// Found matching device name.
			if (strcmp(instance, elems[i].key) == 0) {
				// Both device name and device instance are
				// correct.
				matched = true;
			} else {
				// Device name matches with wrong instance.
				wtable->remove_element(elems[i].key, env);
				if ((ex = env.exception()) != NULL) {
					env.clear();
					wtable->abort_transaction(env);
					if (env.exception()) {
						DCS_DBPRINTF_R(
						    DCS_TRACE_DC_SERVICE,
						    ("test_and_register_name"
						    "(%p): exception '%s' "
						    "while calling "
						    "abort_transaction().\n",
						    this,
						    env.exception()->_name()));
					}
					return (DCS_ERR_CCR_ACCESS);
				}
				deleted = true;
			}
		}
		if (!matched && (strcmp(instance, elems[i].key) == 0)) {
			// instance matches an existing key in the CCR but
			// has a different device name. Change the device name
			// and make sure we commit the change to the CCR.
			wtable->update_element(instance, dev_name, env);
			if ((ex = env.exception()) != NULL) {
				env.clear();
				wtable->abort_transaction(env);
				if (env.exception()) {
					DCS_DBPRINTF_R(
					    DCS_TRACE_DC_SERVICE,
					    ("test_and_register_name"
					    "(%p): exception '%s' "
					    "while calling "
					    "abort_transaction().\n",
					    this,
					    env.exception()->_name()));
				}
				return (DCS_ERR_CCR_ACCESS);
			}
			updated = true;
		}
	}
	if (!matched && !updated) {
		// This instance did not exist in the CCR. Add it.
		wtable->add_element(instance, dev_name, env);
		if ((ex = env.exception()) != NULL) {
			env.clear();
			wtable->abort_transaction(env);
			if (env.exception()) {
				DCS_DBPRINTF_R(
				    DCS_TRACE_DC_SERVICE,
				    ("test_and_register_name(%p): exception "
				    "'%s' while calling abort_transaction().\n",
				    this,
				    env.exception()->_name()));
			}
			return (DCS_ERR_CCR_ACCESS);
		}
		updated = true;
	}
	if (updated || deleted) {
		// We changed the CCR. Commit the changes.
		wtable->commit_transaction(env);
		if (env.exception()) {
			DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
			    ("test_and_register_name(%p): exception '%s' while "
			    "calling %p->commit_transaction().\n", this,
			    env.exception()->_name(), wtable));
			RETURN_ON_EXCEPTION(env, "DCS: Error accessing CCR "
			    "table",
			    DCS_ERR_CCR_ACCESS);
		}
	} else {
		// No change made to the CCR. Abort the transaction.
		wtable->abort_transaction(env);
		if (env.exception()) {
			DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE, ("test_and_"
			    "register_name(%p): exception '%s' while calling "
			    "abort_transaction().\n", this,
			    env.exception()->_name()));
		}
	}
	// Only return make_links = false if we have a good match in
	// the CCR and no bad matches.
	if (matched && !(updated || deleted))
		*make_links = false;
	else
		*make_links = true;
	return (DCS_SUCCESS);
}
#endif /* SOL_VERSION > __s10 */
//
// Accessor function.
//
void
dc_service::get_devices(mdc::dev_range_seq_t &gdev_ranges)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service::get_devices().\n"));

	ASSERT(access.lock_held());

	dc_minor_range *tmp_range;
	dc_minor_range_list::ListIterator iterator(*service_gdev_ranges);

	gdev_ranges.length(service_gdev_ranges->count());
	uint_t i = 0;
	for (; (tmp_range = iterator.get_current()) != NULL;
	    iterator.advance()) {
		gdev_ranges[i].maj = tmp_range->major_no;
		gdev_ranges[i].start_minor = tmp_range->start_min;
		gdev_ranges[i].end_minor = tmp_range->end_min;
		i++;
	}
}

//
// Returns true if 'id' is a possible host of this service.
//
bool
dc_service::is_node_in_service(sol::nodeid_t id)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service::is_node_in_service(%d).\n", id));

	ASSERT(access.lock_held());

	uint_t count = service_nodes->length();
	for (uint_t i = 0; i < count; i++) {
		if ((*service_nodes)[i].id == id) {
			return (true);
		}
	}

	return (false);
}

//
// Returns true if gdev is a part of this service.
//
bool
dc_service::is_device_in_service(sol::dev_t gdev)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service::is_device_in_service(%p).\n", gdev));

	ASSERT(access.lock_held());
	dc_minor_range *tmp_range;

	major_t maj = getmajor(gdev);
	minor_t _min = getminor(gdev);

	dc_minor_range_list::ListIterator iterator(*service_gdev_ranges);

	for (; (tmp_range = iterator.get_current()) != NULL;
	    iterator.advance()) {
		if ((tmp_range->major_no == maj) &&
		    IN_BETWEEN(_min, tmp_range->start_min,
		    tmp_range->end_min)) {
			return (true);
		}
	}

	return (false);
}

//
// Add a callback object to be notified of changes in nodes that host
// this device service.  'gdev' must be a part of this device service.
//
void
dc_service::add_callback(dc_callback_info *cb_info, Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service::add_callback(%p).\n", cb_info));

	ASSERT(access.lock_held());

	//
	// Check to see if this is the result of a retry.
	// Note that there is a rule implemented here that a particular
	// callback object may only register interest once for a particular
	// dev_t value.
	//
	dc_callback_info *tmp;
	for (callbacks.atfirst();
	    (tmp = callbacks.get_current()) != NULL;
	    callbacks.advance()) {
		if ((tmp->cb_var->_equiv(cb_info->cb_var)) &&
		    (tmp->gmaj == cb_info->gmaj) &&
		    (tmp->gmin == cb_info->gmin)) {
			// Object already exists.
			DCS_DBPRINTF_A(DCS_TRACE_DC_SERVICE,
			    ("add_callback(%p): callback already exists "
			    "(%d, %d).\n", this, tmp->gmaj, tmp->gmin));
			return;
		}
	}

	//
	// We store a reference to the 'dc_mapper' object in the callback
	// object so that even if all the nodes that host the device service
	// die, the 'dc_mapper' object will remain as long as callback
	// objects exists for this service.  See BugId 4203403 for why this
	// is required.
	//
	dc_config_obj->get_checkpoint()->ckpt_add_callback(cb_info->gmaj,
	    cb_info->gmin, cb_info->cb_var, cb_info->mapper_var, _environment);
	callbacks.append(cb_info);
	dc_config_obj->add_callback_internal(cb_info);
}

//
// Called from dc_config::convert_to_primary() to add callback objects
// onto the newly created dc_service object.
//
void
dc_service::add_callback_nocheck(dc_callback_info *cb_info)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service::add_callback_nocheck(%p).\n", cb_info));

	callbacks.append(cb_info);
}

//
// Remove callback object.
//
void
dc_service::remove_callback(sol::major_t gmaj, sol::minor_t gmin,
    fs::dc_callback_ptr cb_ptr, Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service::remove_callback(%d, %d, %p, service=%s).\n",
	    gmaj, gmin, cb_ptr, service_name));

	ASSERT(access.lock_held());

	dc_callback_info *tmp;
	Environment env;
	for (callbacks.atfirst();
	    (tmp = callbacks.get_current()) != NULL;
	    callbacks.advance()) {
		if ((tmp->cb_var->_equiv(cb_ptr)) && (tmp->gmaj == gmaj) &&
		    (tmp->gmin == gmin)) {
			//
			// Found the object - remove it.
			//
			dc_config_obj->get_checkpoint()->ckpt_remove_callback(
			    gmaj, gmin, cb_ptr, _environment);
			(void) callbacks.erase(tmp);
			dc_config_obj->remove_callback_internal(tmp->gmaj,
			    tmp->gmin, tmp->cb_var);
			DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
			    ("remove_callback(%p): object found.\n", this));
			return;
		}
	}

	DCS_DBPRINTF_A(DCS_TRACE_DC_SERVICE,
	    ("remove_callback(%p): object not found.\n", this));
}

//
// Helper function that returns true if 'gdev' is in one of the ranges
// specified by 'gdev_ranges'.
//
bool
dc_service::is_in_range(sol::dev_t gdev,
    const mdc::dev_range_seq_t &gdev_ranges)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service::is_in_range(%d, %p).\n", gdev, gdev_ranges));

	uint_t gdev_range_len = gdev_ranges.length();

	if (gdev_range_len == 0) {
		return (false);
	}

	major_t gmaj = getmajor(gdev);
	minor_t gmin = getminor(gdev);

	for (uint_t i = 0; i < gdev_range_len; i++) {
		if (gmaj != gdev_ranges[i].maj) {
			continue;
		}
		if ((gmin >= gdev_ranges[i].start_minor) &&
		    (gmin <= gdev_ranges[i].end_minor)) {
			return (true);
		}
	}

	// We did not find a match.
	return (false);
}

//
// Returns true if a valid callback object is registered for any device in
// 'gdev_ranges'.  "valid" here means a callback object that returns true when
// queried with the 'still_active' IDL call.  If 'gdev_ranges' is NULL, then we
// check for whether any valid callback object is registered for this service.
//
bool
dc_service::callback_registered(const mdc::dev_range_seq_t *gdev_ranges,
    Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service::callback_registered(%p, service=%s).\n",
	    gdev_ranges, service_name));

	ASSERT(access.lock_held());

	Environment env;
	bool cb_active;
	dc_callback_info *tmp;
	for (callbacks.atfirst(); (tmp = callbacks.get_current()) != NULL; ) {
		if ((gdev_ranges == NULL) || is_in_range(
		    makedevice(tmp->gmaj, tmp->gmin), *gdev_ranges)) {
			//
			// See if the callback object is still tracking this
			// device.
			//
			cb_active = tmp->cb_var->still_active(
			    makedevice(tmp->gmaj, tmp->gmin), env);
			if (!env.exception() && cb_active) {
				// A valid callback object is registered.
				DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
				    ("callback_registered(%p): found an "
				    "active/valid callback.\n", this));
				ASSERT(is_active());
				return (true);
			}
			//
			// The cb object is either dead or disinterested
			// in this device - remove it.
			//
			env.clear();
			DCS_DBPRINTF_A(DCS_TRACE_DC_SERVICE,
			    ("callback_registered(%p): found a dead or "
			    "inactive callback. Removing it.\n", this));
			dc_config_obj->get_checkpoint()->ckpt_remove_callback(
			    tmp->gmaj, tmp->gmin, tmp->cb_var, _environment);
			// Note that the erase does an implicit advance.
			(void) callbacks.erase(tmp);
			dc_config_obj->remove_callback_internal(tmp->gmaj,
			    tmp->gmin, tmp->cb_var);
		} else {
			callbacks.advance();
		}
	}

	// No valid callback object is registered.
	DCS_DBPRINTF_R(DCS_TRACE_DC_SERVICE,
	    ("callback_registered(%p): no valid callback found.\n", this));

	return (false);
}

//
// Notify all registered callback objects of a change in the list of
// nodes serving this device service.
//
void
dc_service::notify_callbacks(Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service::notify_callbacks(service=%s).\n",
	    service_name));

	ASSERT(access.lock_held());

	Environment env;
	dc_callback_info *tmp;
	sol::nodeid_seq_t nodes;
	uint_t length = service_nodes->length();
	nodes.length(length);
	for (uint_t i = 0; i < length; i++) {
		nodes[i] = (*service_nodes)[i].id;
	}

	for (callbacks.atfirst(); (tmp = callbacks.get_current()) != NULL; ) {
		tmp->cb_var->notify_change(makedevice(tmp->gmaj, tmp->gmin),
		    nodes, env);
		if (env.exception()) {
			//
			// The cb object is either dead or disinterested in
			// this device - remove it.
			//
			env.clear();
			DCS_DBPRINTF_A(DCS_TRACE_DC_SERVICE,
			    ("notify_callbacks(%p): found a dead or inactive "
			    "callback. Removing it.\n", this));
			dc_config_obj->get_checkpoint()->ckpt_remove_callback(
			    tmp->gmaj, tmp->gmin, tmp->cb_var, _environment);
			_environment.clear();
			// Note that the erase does an implicit advance.
			(void) callbacks.erase(tmp);
			dc_config_obj->remove_callback_internal(tmp->gmaj,
			    tmp->gmin, tmp->cb_var);
		} else {
			callbacks.advance();
		}
	}
}
// Start this service on the given DSM.  'id' is the node id of the DSM,
// and is used to get the priority of the replica to be started on that
// node.
void
dc_service::start_service(mdc::device_service_manager_ptr dsm,
    sol::nodeid_t id, mdc::dc_mapper_ptr mapper, Environment &env)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_SERVICE,
	    ("dc_service::start_service(%p, %d, %p).\n", dsm, id, mapper));

	ASSERT(access.lock_held());

	char *tmp = _service_class->get_user_program();

	char *tmp_secondaries;
	uint_t num_sec = 0;
	dc_error_t err = get_property("DCS_NumberOfSecondaries",
				tmp_secondaries);
	if (err == DCS_SUCCESS) {
		num_sec = (uint_t)os::atoi(tmp_secondaries);
	}

	dsm->start_device_service(service_id, service_name,
		tmp, get_priority(id), num_sec, mapper, env);

	delete [] tmp;
}
