/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _DC_MAPPER_IMPL_IN_H
#define	_DC_MAPPER_IMPL_IN_H

#pragma ident	"@(#)dc_mapper_impl_in.h	1.9	08/05/20 SMI"

//
// Return the id of the device service that this mapper is associated with.
//
inline unsigned int
dc_mapper_impl::get_devid()
{
	return (devid);
}

// Set the devid for this object.
inline void
dc_mapper_impl::set_devid(unsigned int id)
{
	devid = id;
}

//
// Compare the id passed in to the id of the device service that this mapper
// is associated with.  Return true if equal.
//
inline bool
dc_mapper_impl::equals_devid(unsigned int id)
{
	return ((devid == id) ? true : false);
}

//
// Return a new reference to the device server.
//
inline mdc::device_server_ptr
dc_mapper_impl::get_devobj() const
{
	return (mdc::device_server::_duplicate(dev_server));
}

//
// Set the device server object (used to complete construction).
//
inline void
dc_mapper_impl::set_devobj(mdc::device_server_ptr devobj)
{
	dev_server = mdc::device_server::_duplicate(devobj);
}

//
// Called from dc_config::shutdown_service() to release the reference to the
// 'device_server' object.
//
inline void
dc_mapper_impl::release_device_server()
{
	//
	// 'dev_server' is of type 'var', so assigning it to _nil will release
	// the previous reference.
	//
	dev_server = mdc::device_server::_nil();
}

//
// Static function used to get to the dc_mapper_impl object from a dc_mapper
// object reference.
//
inline dc_mapper_impl *
dc_mapper_impl::get_dc_mapper_impl(mdc::dc_mapper_ptr mapper)
{
	return ((dc_mapper_impl *) (mapper->_handler()->get_cookie()));
}

#endif	/* _DC_MAPPER_IMPL_IN_H */
