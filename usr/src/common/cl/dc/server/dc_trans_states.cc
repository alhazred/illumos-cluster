/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)dc_trans_states.cc	1.12	08/05/20 SMI"

#include <sys/os.h>
#include <orb/infrastructure/orb_conf.h>
#include <dc/server/dc_trans_states.h>

#include <dc/sys/dc_debug.h>

global_minor_state::global_minor_state(minor_t gmin)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_TRANSACTION_STATES,
	    ("global_minor_state::global_minor_state(%d).\n", gmin));

	gminor = gmin;
}

void
global_minor_state::get_data(minor_t *gmin)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_TRANSACTION_STATES,
	    ("global_minor_state::get_data().\n"));

	*gmin = gminor;
}

void
global_minor_state::register_new_state(minor_t gmin, Environment &env)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_TRANSACTION_STATES,
	    ("global_minor_state::register_new_state(%d).\n", gmin));

	// If this checkpoint is being sent while dumping state, don't
	// register state with the framework.
	if (secondary_ctx::extract_from(env) == NULL) {
		return;
	}

	global_minor_state *state = new global_minor_state(gmin);

	state->register_state(env);
	// The HA framework guarantees that no exception will be raised in
	// this call.
	ASSERT(!env.exception());
}

//
// If the environment variable passed in indicates that this call is a retry,
// then this function returns true.  It also returns the global minor number
// stored in the saved state variable.  If this call is not a retry,
// the function returns false, and the values in gmin is not touched.
//
bool
global_minor_state::retry(Environment &env, minor_t *gmin)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_TRANSACTION_STATES,
	    ("global_minor_state::retry().\n"));

	primary_ctx *ctxp = primary_ctx::extract_from(env);
	global_minor_state *state =
	    (global_minor_state *) ctxp->get_saved_state();

	if (state != NULL) {
		DCS_DBPRINTF_W(DCS_TRACE_DC_TRANSACTION_STATES,
		    ("retry(): this is a retry.\n"));
		// We are in a retry.
		state->get_data(gmin);
		return (true);
	}

	return (false);
}

void
global_minor_state::orphaned(Environment &)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_TRANSACTION_STATES,
	    ("global_minor_state::orphaned().\n"));

	// Client died.  Simply return, rollback will be handled by the
	// _unreferenced() call.
}

void
global_minor_state::commited()
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_TRANSACTION_STATES,
	    ("global_minor_state::commited().\n"));

	// We do not use this.
}

retry_state::retry_state()
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_TRANSACTION_STATES,
	    ("retry_state::retry_state().\n"));

	// Empty constructor.
}

void
retry_state::register_new_state(Environment &env)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_TRANSACTION_STATES,
	    ("retry_state::register_new_state().\n"));

	// If this checkpoint is being sent while dumping state, don't
	// register state with the framework.
	if (secondary_ctx::extract_from(env) == NULL) {
		return;
	}

	retry_state *state = new retry_state();

	state->register_state(env);
	// The HA framework guarantees that no exception will be raised in
	// this call.
	ASSERT(!env.exception());
}

// If the environment variable passed in indicates that this call is a retry,
// then this function returns true, otherwise it returns false.
bool
retry_state::retry(Environment &env)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_TRANSACTION_STATES,
	    ("retry_state::retry().\n"));

	primary_ctx *ctxp = primary_ctx::extract_from(env);

	if (ctxp->get_saved_state() != NULL) {
		DCS_DBPRINTF_W(DCS_TRACE_DC_TRANSACTION_STATES,
		    ("retry(): this is a retry.\n"));

		// We are in a retry.
		return (true);
	}

	return (false);
}

void
retry_state::orphaned(Environment &)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_TRANSACTION_STATES,
	    ("retry_state::orphaned().\n"));

	// Client died.  Simply return, rollback will be handled by the
	// _unreferenced() call.
}

void
retry_state::commited()
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_TRANSACTION_STATES,
	    ("retry_state::commited().\n"));

	// We do not use this.
}
