/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _DC_SERVICES_IN_H
#define	_DC_SERVICES_IN_H

#pragma ident	"@(#)dc_services_in.h	1.8	08/05/20 SMI"

//
// Empty constructor.
//
inline
dc_services::dc_services()
{
}

//
// Empty destructor.
//
inline
dc_services::~dc_services()
{
	cleanup();
}

//
// Static function to initialize device services.  This is called from
// the dc_config constructor.
//
inline void
dc_services::init()
{
	ASSERT(the_services == NULL);
	the_services = new dc_services;
}

inline void
dc_services::destroy()
{
	ASSERT(the_services != NULL);
	delete (the_services);
	the_services = NULL;
	first_access = true;
}

//
// Allocate a new service id.  Allocation is in-memory
//
inline unsigned int
dc_services::allocate_service_id()
{
	ASSERT(service_lock.lock_held());

	return (get_num() + START_DEVID_VALUE);
}

//
// Free a service id.  Freeing is in-memory
//
inline void
dc_services::free_service_id_in_memory(unsigned int service_id)
{
	ASSERT(service_lock.lock_held());

	free_num(service_id - START_DEVID_VALUE);
}

inline void
dc_services::wrlock()
{
	service_lock.wrlock();
}

inline void
dc_services::rdlock()
{
	service_lock.rdlock();
}

inline void
dc_services::unlock()
{
	service_lock.unlock();
}

#ifdef DEBUG
inline int
dc_services::lock_held()
{
	return (service_lock.lock_held());
}
#endif

#endif	/* _DC_SERVICES_IN_H */
