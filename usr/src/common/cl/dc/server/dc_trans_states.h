/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _DC_TRANS_STATES_H
#define	_DC_TRANS_STATES_H

#pragma ident	"@(#)dc_trans_states.h	1.10	08/05/20 SMI"

#include <sys/os.h>
#include <repl/service/transaction_state.h>
#include <repl/service/replica_handler.h>

//
// This state object is used by dc_mapper::map_minor().  The call allocates
// a minor number, and this state object is used to 'remember' what was
// allocated in case of a retry.
//
class global_minor_state : public transaction_state {

public:

	global_minor_state(minor_t gmin);

	void get_data(minor_t *gmin);

	//
	// Creates a new "global_minor_state" state object and registers
	// it with the ha framework.
	//
	static void register_new_state(minor_t gmin, Environment &);

	//
	// If the environment variable passed in indicates that
	// this call is a retry, then this function returns true.
	// It also returns the global minor number stored in the
	// saved state variable.  If this call is not a retry, the
	// function returns false, and the value in gmin is not touched.
	//
	static bool retry(Environment &env, minor_t *gmin);

	//
	// Called when state is unreferenced because client is dead.
	//
	void orphaned(Environment &);

	//
	// Called when transaction is commited on the primary i.e. when
	// the primary calls add_commit(), or when the framework knows
	// that the invocation has completed on the primary.
	//
	void commited();

private:

	minor_t gminor;

};

//
// Used by dc_mapper::unmap_minor(), dc_server::free_instance() and
// dc_server::devconfig_unlock().
// We just need this state object in order to know whether this
// call is a retry or not.  Hence, no data is stored here.
//
class retry_state : public transaction_state {

public:

	retry_state();

	static void register_new_state(Environment &env);

	//
	// If the environment variable passed in indicates that
	// this call is a retry, then this function returns true,
	// otherwise it returns false.
	//
	static bool retry(Environment &env);

	//
	// Called when state is unreferenced because client is dead.
	//
	void orphaned(Environment &);

	//
	// Called when transaction is commited on the primary i.e. when
	// the primary calls add_commit(), or when the framework knows
	// that the invocation has completed on the primary.
	//
	void commited();

};

#endif	/* _DC_TRANS_STATES_H */
