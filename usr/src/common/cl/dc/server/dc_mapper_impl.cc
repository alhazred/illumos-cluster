//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)dc_mapper_impl.cc	1.51	08/05/20 SMI"

#include <sys/os.h>
#include <dc/server/dc_mapper_impl.h>
#include <dc/server/dc_config_impl.h>
#include <sys/dc_ki.h>

#include <dc/sys/dc_lib.h>
#include <dc/server/dc_trans_states.h>
#include <dc/server/dc_repl_server.h>

#include <dc/sys/dc_debug.h>

//
// Constructor for the primary object.
//
dc_mapper_impl::dc_mapper_impl(dc_repl_server *serverp, unsigned int id) :
    mc_replica_of<mdc::dc_mapper>(serverp)
{
	construct(id);
}

//
// Constructor for the secondary object.
//
dc_mapper_impl::dc_mapper_impl(mdc::dc_mapper_ptr primary_obj) :
    mc_replica_of<mdc::dc_mapper>(primary_obj)
{
	construct(0);
	dev_server = mdc::device_server::_nil();
}

//
// Common code to construct the mapper object - called from both constructors.
//
void
dc_mapper_impl::construct(unsigned int id)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_MAPPER,
	    ("dc_mapper_impl::construct(%d).\n", id));
	devid = id;
	_handler()->set_cookie((void *)this);
}

//
// Destructor.  Cleanup state here - the destructor is called by
// dc_mapper::unref_handler() which is scheduled by the unreferenced call on
// this thread.
//
dc_mapper_impl::~dc_mapper_impl()
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_MAPPER,
	    ("dc_mapper_impl::~dc_mapper_impl().\n"));
}

//
// On the primary, references are being created in dc_config::dump_state(),
// and so we use the same lock as in there to see if this is the last unref
// that we will get.  If so, we delete this object.
// On the secondary, _last_unref() will always return true.
//
void
dc_mapper_impl::_unreferenced(unref_t arg)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_MAPPER,
	    ("dc_mapper_impl::_unreferenced(%d).\n", arg));

	if ((devid == 0) || (!dc_config_obj->is_primary())) {
		ASSERT(_last_unref(arg));
		dc_config_obj->lock_list();
		dc_config_obj->remove_mapper(this);
		dc_config_obj->unlock_list();
		DCS_DBPRINTF_G(DCS_TRACE_DC_MAPPER,
		    ("_unreferenced(%p): deleted mapper.\n", this));
		delete this;
		return;
	}

	ASSERT(dc_config_obj->is_primary());
	ASSERT(devid != 0);

	//
	// We need to mark the service as inactive, and remove the dc_mapper
	// object from the list in the dc_config object - get the appropriate
	// locks.
	//

	dc_services *services = dc_services::get_services();
	services->rdlock();
	dc_service *_service = services->resolve_service_from_id(
	    devid, dc_services::RDLOCKED);
	services->unlock();

	//
	// Note that 'service' could be NULL if there are CCR errors when the
	// DCS is switched over and the DCS is unable to read service
	// information about a service that had been started up on the
	// previous DCS primary.
	//
	if (_service != NULL) {
		if (!_service->start_service_try_lock()) {
			//
			// We can't get the service_lock - re-schedule the
			// unref notification.  See BugId 4284752 to see why
			// we can't block on the start_service lock.
			//
			_service->unlock();
			mdc::dc_mapper_var mapper_obj = get_objref();
			DCS_DBPRINTF_A(DCS_TRACE_DC_MAPPER,
			    ("_unreferenced(%p): couldn't get service "
			    "lock.\n", this));
			return;
		//lint -e529
		}
		//lint +e529
	}

	dc_config_obj->lock_list();

	if (_last_unref(arg)) {

		dc_config_obj->remove_mapper(this);
		dc_config_obj->unlock_list();

		if (_service != NULL) {
			//
			// If this unref() was previously tried on a primary
			// and partly processed, then it is possible that
			// there is another dc_mapper object for this service
			// in the dc_config list.  See bugid 4230849 for
			// details.
			//
			mdc::device_server_var obj =
			    dc_config_obj->find_devobj(devid);
			if (CORBA::is_nil(obj)) {
				// Mark the service as inactive.
				_service->set_active(false);
			} else {
				DCS_DBPRINTF_A(DCS_TRACE_DC_MAPPER,
				    ("_unreferenced(%p): found more mapper "
				    "objs.\n", this));
			}
			_service->start_service_unlock();
			_service->unlock();
		}

		FAULTPT_DCS(FAULTNUM_DCS_MAPPER_UNREF_S_O,
		    FaultFunctions::generic);

		DCS_DBPRINTF_G(DCS_TRACE_DC_MAPPER,
		    ("_unreferenced(%p): deleted mapper.\n", this));
		delete this;
		return;
	}

	dc_config_obj->unlock_list();
	if (_service != NULL) {
		_service->start_service_unlock();
		_service->unlock();
	}
}

//
// Called on the primary to add a secondary, or to checkpoint a new dc_mapper
// object to existing secondaries.
//
void
dc_mapper_impl::dump_state(repl_dc::repl_dc_ckpt_ptr secondary_ckpt,
    Environment &env)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_MAPPER,
	    ("dc_mapper_impl::dump_state().\n"));

	mdc::dc_mapper_var dc_mapper_obj = get_objref();

	secondary_ckpt->ckpt_set_dc_mapper(dc_mapper_obj, devid, dev_server,
	    env);
}

// Checkpoint accessor function.
repl_dc::repl_dc_ckpt_ptr
dc_mapper_impl::get_checkpoint()
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_MAPPER,
	    ("dc_mapper_impl::get_checkpoint().\n"));

	return ((dc_repl_server*)(get_provider()))->get_checkpoint_repl_dc();
}
