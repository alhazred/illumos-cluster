//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)dc_storage.cc	1.14	08/05/20 SMI"

#include <dc/server/dc_storage.h>
#include <dc/sys/dc_lib.h>

#include <dc/sys/dc_debug.h>

// These are the only majors we want dc_major_store objects created
// for.
#define	MAJORS_TO_CHECK 3
char *major_names_array[] = {"did", "md", "vxio"};

dc_storage *the_storage = NULL;

dc_storage::dc_storage() {
	DCS_DBPRINTF_W(DCS_TRACE_DC_STORAGE, ("dc_storage::dc_storage().\n"));

	for (int i = 0; i < MAX_HASH_BUCKET; i++)
		hashed_list[i] = NULL;
}

dc_storage::~dc_storage() {
	DCS_DBPRINTF_W(DCS_TRACE_DC_STORAGE, ("dc_storage::~dc_storage().\n"));

	dc_major_store *curr, *next;
	for (int i = 0; i < MAX_HASH_BUCKET; i++) {
		curr = hashed_list[i];
		while (curr) {
			next = curr->next;
			delete curr;
			curr = next;
		}
	}
}

void
dc_storage::get_unique_major_numbers(mdc::long_seq_t &major_numbers)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_STORAGE,
	    ("dc_storage::get_unique_major_numbers().\n"));

	uint_t pos = 0, incr = 10, len = 10;

	major_numbers.length(len);
	for (int i = 0; i < MAX_HASH_BUCKET; i++) {
		dc_major_store *curr = hashed_list[i];
		dc_major_store *tmp;
		for (tmp = curr; tmp; tmp = tmp->next) {
			if (pos >= len) {
				len += incr;
				major_numbers.length(len);
			}
			major_numbers[pos++] = tmp->get_major();
		}
	}

	major_numbers.length(pos);
}

//
// Get the 'dc_major_store' object associated with this major number.
// If '_create' is true, and no object exists with the major number, then
// create it.
//
dc_major_store *
dc_storage::get_store(major_t maj, bool _create)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_STORAGE,
	    ("dc_storage::get_store(%d, %d).\n", maj, _create));

	dc_major_store **dc_major_store_list = &hashed_list[maj_to_bucket(maj)];
	dc_major_store *tmp;
	int i;
	bool major_name_found = false;

	char *major_name = ddi_major_to_name(maj);
	if (major_name == NULL) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_STORAGE,
		    ("get_store(%p): ddi_major_to_name(%d) returned null.\n",
		    this, maj));
		return (NULL);
	}

	// Check if maj corresponds to one of did, md, or vxio.
	for (i = 0; i < MAJORS_TO_CHECK; i++) {
		if (strcmp(major_name, major_names_array[i]) == 0) {
			major_name_found = true;
			break;
		}
	}

	// Do not create an object for this major number as it is something
	// other than did, md, or vxio.
	if (!major_name_found) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_STORAGE,
		    ("get_store(%p): major name '%s' not found.\n",
		    this, major_name));
		return (NULL);
	}

	list_lock.lock();
	for (tmp = *dc_major_store_list; tmp; tmp = tmp->next) {
		if (tmp->get_major() == maj) {
			//
			// Note that entries never get deleted from this list,
			// so we can drop 'list_lock' before returning one
			// of the entries from the list.
			//
			list_lock.unlock();

			return (tmp);
		}
	}

	if (!_create) {
		list_lock.unlock();
		DCS_DBPRINTF_A(DCS_TRACE_DC_STORAGE,
		    ("get_store(%p): major '%p' not found and not created.\n",
		    major_name));

		return (NULL);
	}

	tmp = new dc_major_store(maj);
	tmp->next = *dc_major_store_list;
	*dc_major_store_list = tmp;
	list_lock.unlock();

	return (tmp);
}

//
// Check for the validity of a change in the minor maps.
//
dc_error_t
dc_storage::possible_mapping(modify_type_t type,
    const mdc::dev_range_seq_t &gdev_ranges, unsigned int service_id)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_STORAGE,
	    ("dc_storage::possible_mapping(%d, %p, %d).\n",
	    type, gdev_ranges, service_id));

	unsigned int i;
	unsigned int gdev_range_len;
	major_t maj;
	minor_t smin, emin;
	dc_error_t err_val;
	dc_major_store *tmp_major_object;

	ASSERT((type == add_mapping) || (type == remove_mapping));

	gdev_range_len = gdev_ranges.length();

	for (i = 0; i < gdev_range_len; i++) {
		maj = gdev_ranges[i].maj;
		smin = gdev_ranges[i].start_minor;
		emin = gdev_ranges[i].end_minor;

		// Check for validity of the major number.
		if (maj >= (major_t)devcnt) {
			DCS_DBPRINTF_R(DCS_TRACE_DC_STORAGE,
			    ("possible_mapping(%p): invalid major number "
			    "'%d' in '%p[%d]'.\n",
			    this, maj, gdev_ranges, i));

			return (DCS_ERR_DEVICE_INVAL);
		}

		// Check for validity of the device range.
		if (smin > emin) {
			DCS_DBPRINTF_R(DCS_TRACE_DC_STORAGE,
			    ("possible_mapping(%p): invalid device range "
			    "'[%d-%d]' in '%p[%d]'.\n",
			    this, smin, emin, gdev_ranges, i));
			return (DCS_ERR_DEVICE_INVAL);
		}

		// See if the modification is legal.
		// get_store can return NULL if the major number passed is
		// not did's, md's, or vxio's.
		if ((tmp_major_object = get_store(maj, true)) != NULL) {
			if ((err_val = tmp_major_object->possible_map(type,
			    smin, emin, service_id)) != DCS_SUCCESS) {
				DCS_DBPRINTF_R(DCS_TRACE_DC_STORAGE,
				    ("possible_mapping(%p): possible_map(%d, "
				    "%d, %d, %d) returned '%d'.\n",
				    this, type, smin, emin, service_id,
				    err_val));
				return (err_val);
			}
		} else {
			//
			// get_store with second argument as "true" returns
			// NULL only if the major number passed is not did's,
			// md's or vxio's.
			//
			DCS_DBPRINTF_R(DCS_TRACE_DC_STORAGE,
			    ("possible_mapping(%p): major '%d' not a did, md "
			    "or vxio.\n", this, maj));
			return (DCS_ERR_MAJOR_NUM);
		}
	}

	return (DCS_SUCCESS);
}

//
// Add or remove the specified devices from the appropriate major number maps.
//
dc_error_t
dc_storage::process_mapping(modify_type_t type,
    const mdc::dev_range_seq_t &gdev_ranges, unsigned int service_id,
    dc_minor_range_list *service_range_list)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_STORAGE,
	    ("dc_storage::process_mapping(%d, %p, %d, %p).\n",
	    type, gdev_ranges, service_id, service_range_list));

	unsigned int i;
	unsigned int gdev_range_len;
	major_t maj;
	minor_t smin, emin;
	dc_error_t err_val;
	dc_major_store *tmp_major_object;

	ASSERT((type == add_mapping) || (type == remove_mapping));

	gdev_range_len = gdev_ranges.length();

	if (gdev_range_len == 0) {
		DCS_DBPRINTF_G(DCS_TRACE_DC_STORAGE,
		    ("process_mapping(%p): gdev_range.length() is 0.\n",
		    this));
		return (DCS_SUCCESS);
	}

	for (i = 0; i < gdev_range_len; i++) {
		maj = gdev_ranges[i].maj;
		smin = gdev_ranges[i].start_minor;
		emin = gdev_ranges[i].end_minor;

		if (type == add_mapping) {
			if ((tmp_major_object = get_store(maj, true))
			    != NULL) {
				err_val = tmp_major_object->add_map(smin, emin,
				    service_id, service_range_list);
			} else {
				//
				// get_store() returned NULL, which means we
				// were trying to get a dc_major_store object
				// for something other than did, md, or vxio.
				//
				DCS_DBPRINTF_R(DCS_TRACE_DC_STORAGE,
				    ("process_mapping(%p): major '%d' not a "
				    "did, md or vxio.\n", this, maj));
				return (DCS_ERR_MAJOR_NUM);
			}
		} else {
			if ((tmp_major_object = get_store(maj, true))
			    != NULL) {
				err_val = tmp_major_object->remove_map(smin,
				    emin, service_id, service_range_list);
			} else {
				//
				// get_store() returned NULL, which means we
				// were trying to get a dc_major_store object
				// for something other than did, md, or vxio.
				//
				DCS_DBPRINTF_R(DCS_TRACE_DC_STORAGE,
				    ("process_mapping(%p): major '%d' not a "
				    "did, md or vxio.\n", this, maj));
				return (DCS_ERR_MAJOR_NUM);
			}
		}
		//
		// We must succeed, otherwise the previous call to
		// possible_map() would have returned an error.
		//
		ASSERT(err_val == DCS_SUCCESS);
	}

	return (DCS_SUCCESS);
}

//
// Adds the specified mappings to the appropriate 'dc_major_store'
// objects.  These mappings are assumed to be good, so no validation
// checking is required.
//
void
dc_storage::add_mapping_nocheck(dc_minor_range_list *service_range_list)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_STORAGE,
	    ("dc_storage::add_mapping_nocheck(%p).\n",
	    service_range_list));

	dc_major_store *tmp_major_object;

	ASSERT(service_range_list != NULL);

	dc_minor_range *tmp_range;

	//
	// The algorithm here is:
	// (i) Find the major number of the first device range in
	// 'service_range_list'.
	// (ii) Add all the device mappings for this service and major number.
	// (iii) Repeat steps (i) and (ii) till all done.
	//
	service_range_list->atfirst();
	while ((tmp_range = service_range_list->get_current()) != NULL) {
		tmp_major_object = get_store(tmp_range->major_no, true);
		// Added this ASSERT here as this method is void. If
		// get_store() returns NULL we cannot return an error code.
		// This assertion should not be hit anytime because the
		// mappings have been checked and found valid already.
		ASSERT(tmp_major_object != NULL);
		tmp_major_object->add_mapping_nocheck(service_range_list);
	}
}

//
// Clears all the device mappings for a given service.
//
void
dc_storage::clear_mapping(unsigned int service_id,
    dc_minor_range_list *service_range_list)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_STORAGE,
	    ("dc_storage::clear_mapping(%d, %p).\n",
	    service_id, service_range_list));

	dc_major_store *tmp_major_object;

	ASSERT(service_range_list != NULL);

	dc_minor_range *tmp_range;

	//
	// The algorithm here is:
	// (i) Find the major number of the first device range in
	// 'service_range_list'.
	// (ii) Clear all the device mappings for this service and major number.
	// (iii) Repeat steps (i) and (ii) till all done.
	//
	while (!service_range_list->empty()) {
		service_range_list->atfirst();
		tmp_range = service_range_list->get_current();
		ASSERT(tmp_range != NULL);
		tmp_major_object = get_store(tmp_range->major_no, true);
		// Added this ASSERT here because this method is void. If
		// get_store() returns NULL we cannot return an error code.
		// This assertion should not be hit anytime because we would
		// have been able to add a mapping only if it was valid.
		ASSERT(tmp_major_object != NULL);
		tmp_major_object->clear_mapping(service_id, service_range_list);
	}

	ASSERT(service_range_list->empty());
}

bool
dc_storage::get_map(major_t maj, minor_t minor_num, unsigned int *device_id)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_STORAGE,
	    ("dc_storage::get_map(%d, %d, %p).\n",
	    maj, minor_num, device_id));

	dc_major_store *tmp_major_object;

	// Check if get_store() returned NULL and avert a panic.
	if ((tmp_major_object = get_store(maj, true)) == NULL) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_STORAGE,
		    ("get_map(%p): major '%d' not a did, md or vxio.\n",
		    this, maj));
		return (false);
	}

	return (tmp_major_object->get_map(minor_num, device_id));
}
