/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _DC_SERVICE_H
#define	_DC_SERVICE_H

#pragma ident	"@(#)dc_service.h	1.29	08/05/20 SMI"

#include <h/dc.h>
#include <h/ccr.h>
#include <dc/sys/dc_num_alloc.h>
#include <dc/server/dc_storage.h>
#include <dc/libdcs/libdcs.h>
#include <dc/sys/dc_lib.h>

//
// This magic number defines the start value of the device service ids that will
// be handed out for HA device services
//
#define	START_DEVID_VALUE 1

//
// Services are stored in the CCR in a format that consists of property names
// and property values.  The property names that MUST exist in every valid
// device service table are {ServiceName, UserProgram, FailoverType,
// FailbackEnabled, NumberOfSecondaries, Nodes, Devices}.
// Additional properties may be present for different services.
// An example table is:
// <start example>
// 	DCS_ServiceName	dsk/d2
// 	DCS_ServiceClass	DISK
// 	DCS_FailbackEnabled	no
// 	DCS_Suspended	no
// 	DCS_NumberOfSecondaries	0
// 	DCS_Nodes	(6,0) (4,1)
// 	DCS_Devices	(149,64-71)
// <end example>
//

//
// Stores information about a service class.
//
class dc_service_class {
public:
	dc_service_class(const char *class_name, const char *user_program,
	    mdc::ha_dev_type ha_type);
	~dc_service_class();

	//
	// Helper function to get the user program to be run for services of
	// this class in the form "takeover_program -C name".  It is upto the
	// caller to free the memory allocated here.
	//
	char *get_user_program();

	// The variables are declared as public for convenience.
	char *name;
	char *takeover_program;
	mdc::ha_dev_type type;
	unsigned int ref_count;
	bool local_class;

private:
	static const char *str_local_class;
};

typedef SList<dc_service_class> dc_service_class_list;

//
// Stores data about a callback object.  Also holds a reference to the mapper
// associated with the service so that the service does not become inactive.
// Note that the variables are declared public for convenience.
//
class dc_callback_info {
public:
	dc_callback_info(sol::major_t maj,
	    sol::minor_t min, fs::dc_callback_ptr cb_ptr,
	    mdc::dc_mapper_ptr mapper_obj);
	~dc_callback_info();

	sol::major_t gmaj;
	sol::minor_t gmin;
	fs::dc_callback_var cb_var;
	mdc::dc_mapper_var mapper_var;
};

typedef SList<dc_callback_info> dc_callback_info_list;

class dc_service {

public:

	dc_service(unsigned int service_id,
	    const char *service_nm,
	    dc_service_class *service_class,
	    bool failback_enabled,
	    mdc::node_preference_seq_t *nodes,
	    unsigned int num_secs,
	    dc_minor_range_list *list,
	    bool suspended);

	~dc_service();

	// Read from file given the service id.
	static dc_service *read_from_ccr(unsigned int service_id);

	//
	// This enum is used to specify the keys that need to be written out
	// in the write_to_ccr() method.
	// XXX If the number of keys for a device service changes, make sure
	// that you change 'number_of_system_keys' as well.
	//
	typedef enum {
		key_all,
		key_service_class,
		key_failback_enabled,
		key_num_secs,
		key_nodes,
		key_devices,
		key_suspended,
		key_properties
	} write_type_t;

	// Write out to file.
	dc_error_t write_to_ccr(write_type_t type,
	    const mdc::service_property_seq_t *properties = NULL);

	dc_error_t erase_ccr_table();

	//
	// Allocates the required memory for buffer (using 'new')
	// and fills it with the name of the service.  It is up to
	// the caller to free this memory.
	//
	void get_service_name(char **buffer);

	//
	// Indicates whether this service is associated with
	// a local device class.
	//
	bool is_local_class();

	//
	// Accessor functions.
	//
	dc_service_class *get_service_class();
	bool get_failback_mode();
	void get_number_of_secondaries(unsigned int &num);
	void get_nodes(mdc::node_preference_seq_t &nodes);
	void get_node_ids(sol::nodeid_seq_t &node_ids);
	void get_devices(mdc::dev_range_seq_t &gdev_ranges);
	dc_minor_range_list *get_devices_list();
	unsigned int get_service_id();
	bool is_suspended();
	uint_t get_priority(nodeid_t id);
	dc_error_t get_property(const char *property_name,
	    CORBA::String_out property_value);
	dc_error_t get_all_properties(
	    mdc::service_property_seq_t_out properties);
	dc_error_t test_and_register_name(const char *instance,
	    const char *dev_name, bool *make_links);

	//
	// Functions that set the values of parameters - these functions
	// also write out to persistent storage, so errors are
	// possible.
	//
	dc_error_t set_failback_mode(bool failback_enabled);
	dc_error_t set_suspended(bool onoff);
	dc_error_t set_number_of_secondaries(unsigned int num_secs);
	dc_error_t add_node(const mdc::node_preference &node);
	dc_error_t remove_node(sol::nodeid_t node);
	dc_error_t write_devices();
	dc_error_t set_properties(const mdc::service_property_seq_t &);
	dc_error_t remove_properties(const sol::string_seq_t &);

	//
	// Initialize the device ranges that this service points to to
	// 'new_list'.
	//
	void reinit_devices(dc_minor_range_list *new_list);

	//
	// Helper function that adds 'dev_range' to 'gdev_ranges' in sorted
	// order.
	//
	static void add_to_range_list(const mdc::dev_range &dev_range,
	    mdc::dev_range_seq_t &gdev_ranges);

	// Returns true if equal.
	bool equals_service_id(unsigned int service_id);

	// Returns true if equal.
	bool equals_service_name(const char *service_nm);

	// Returns true if 'id' is a possible host of this service.
	bool is_node_in_service(sol::nodeid_t id);

	// Returns true if gdev is a part of this service.
	bool is_device_in_service(sol::dev_t gdev);

	//
	// Starts this service by calling into the given dsm.
	//
	void start_service(mdc::device_service_manager_ptr dsm,
	    sol::nodeid_t id, mdc::dc_mapper_ptr mapper, Environment &env);

	//
	// Indicates whether this service has ever been started.
	//
	bool is_active();

	//
	// Mark this service as active/inactive.
	//
	void set_active(bool _active);

	//
	// Add a callback object to be notified of changes in nodes that host
	// this device service.  'gdev' must be a part of this device service.
	//
	void add_callback(dc_callback_info *cb_info, Environment &);

	//
	// Called from dc_config::convert_to_primary() to add callback objects
	// onto the newly created dc_service object.
	//
	void add_callback_nocheck(dc_callback_info *cb_info);

	//
	// Remove callback object.
	//
	void remove_callback(sol::major_t maj, sol::minor_t min,
	    fs::dc_callback_ptr, Environment &);

	//
	// Notify all registered callback objects of a change in the list of
	// nodes serving this device service.
	//
	void notify_callbacks(Environment &);

	//
	// Returns true if a valid callback object is registered for any device
	// in 'gdev_ranges'.  "valid" here means a callback object that
	// returns true when queried with the 'still_active' IDL call.
	//
	bool callback_registered(const mdc::dev_range_seq_t *gdev_ranges,
	    Environment &);

	//
	// Helper function that returns the name of the CCR table for
	// 'service_id'.
	//
	static char *get_table_name(unsigned int service_id);

	void wrlock();
	void rdlock();
	void unlock();

	int start_service_try_lock();
	void start_service_lock();
	void start_service_unlock();

#ifdef DEBUG
	int lock_held();
#endif

private:

	static const char *str_yes;
	static const char *str_no;

	static const char *key_str_service_name;
	static const char *key_str_service_class;
	static const char *key_str_suspended;
	static const char *key_str_failback_enabled;
	static const char *key_str_num_secs;
	static const char *key_str_nodes;
	static const char *key_str_devices;
	static const int number_of_system_keys;

	static const char *service_table_prefix;

	static const int node_record_max_len;
	static const int node_record_min_len;

	static const int gdev_record_max_len;
	static const int gdev_record_min_len;

	static int conv(mdc::ha_dev_type type, char *&string);
	static int conv(const char *string, mdc::ha_dev_type &type);

	static int conv(bool b, const char *&string);
	static int conv_bool(const char *string, bool &b);

	static int conv_alloc(unsigned int i, char *&string);
	static int conv_uint(const char *string, unsigned int &i);

	static int conv_alloc(const mdc::node_preference_seq_t &nodes,
	    char *&string);
	static int conv(char *string, mdc::node_preference_seq_t &nodes);

	static int conv_alloc(dc_minor_range_list *gdev_ranges_list,
	    char *&string);
	static int conv(char *string, mdc::dev_range_seq_t &gdev_ranges);

	//
	// Helper function that gets a handle to the CCR table associated with
	// this service.
	//
	ccr::updatable_table_ptr get_updatable_table(bool create);

	// Helper functions for writing to the CCR.
	dc_error_t write_internal(ccr::updatable_table_ptr, write_type_t,
	    const mdc::service_property_seq_t *properties = NULL);
	void add_element(ccr::element_seq *elems, uint_t &count,
	    const char *key, const char *data);
	dc_error_t add_ccr_property(ccr::updatable_table_ptr table,
	    const char *name, const char *value);
	dc_error_t remove_ccr_property(ccr::updatable_table_ptr table,
	    const char *name);

	void add_property(const char *name, const char *value);
	void remove_property(const char *name);

	// Helper function to determine the validity of a property name.
	bool valid_property_name(const char *name);

	//
	// Helper function that returns a 'mdc::dev_range' structure given
	// its components.
	//
	static mdc::dev_range *alloc_range(major_t major, minor_t start_minor,
	    minor_t end_minor);

	//
	// Helper function that returns true if 'gdev' is in one of the ranges
	// specified by 'gdev_ranges'.
	//
	bool is_in_range(sol::dev_t gdev, const mdc::dev_range_seq_t
	    &gdev_ranges);

	// Pointer to the service class object for this service.
	dc_service_class *_service_class;

	// Protects the members.
	os::rwlock_t access;

	//
	// This mutex protects against deadlock on the access_lock.
	// See BugId 4255752 - Potential DCS deadlock during service startup.
	//
	os::mutex_t access_mutex;

	// This condition variable is associated with access_mutex above
	os::condvar_t access_cv;

	// Used to serialize attempts to start and shutdown this service.
	os::mutex_t startup_lock;

	//
	// List of callback objects registered on devices belonging to this
	// service.
	//
	dc_callback_info_list callbacks;

	// All the variables below go into persistent storage.
	char *service_name;
	dc_minor_range_list *service_gdev_ranges;
	mdc::node_preference_seq_t *service_nodes;
	unsigned int num_secondaries;
	unsigned int service_id;
	bool _failback_enabled;
	bool _suspended;

	// bool indicating whether this service was ever started.
	bool _active;
};

typedef	SList<class dc_service> dc_service_list;

#include <dc/server/dc_service_in.h>

#endif	/* _DC_SERVICE_H */
