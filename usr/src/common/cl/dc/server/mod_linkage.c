/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)mod_linkage.c	1.13	08/05/20 SMI"

#include <sys/modctl.h>
#include <cplplrt/cplplrt.h>

/* inter-module dependencies */
char _depends_on[] = "misc/cl_runtime misc/cl_load misc/cl_orb "
	"misc/cl_haci misc/cl_comm";

static struct modlmisc modlmisc = {
	&mod_miscops, "Major-Minor server code",
};

static struct modlinkage modlinkage = {
	MODREV_1, { (void *)&modlmisc, NULL, NULL, NULL }
};

extern void dc_startup();

int
_init(void)
{
	int	error;

	if ((error = mod_install(&modlinkage)) != 0) {
		return (error);
	}
	_cplpl_init();		/* C++ initialization */
	/*lint -e746 */
	dc_startup();		/* startup that registers the HA dcs object */
	/*lint +e746 */
	return (0);
}

int
_fini(void)
{
	return (-1);
}

int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}
