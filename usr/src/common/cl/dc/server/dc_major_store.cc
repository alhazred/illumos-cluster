/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)dc_major_store.cc	1.18	08/05/20 SMI"

#include <sys/os.h>
#include <dc/server/dc_major_store.h>
#include <dc/libdcs/libdcs.h>
#include <dc/sys/dc_lib.h>

#include <dc/sys/dc_debug.h>

#define	IN_BETWEEN(a, x, y)	((a >= x) && (a <= y))

//
// Constructor for 'dc_minor_range'.
//
dc_minor_range::dc_minor_range(major_t _major, minor_t _start_min,
    minor_t _end_min, unsigned int _device_id)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_MAJOR_STORE,
	    ("dc_minor_range::dc_minor_range(%d, %d, %d, %d).\n",
	    _major, _start_min, _end_min, _device_id));

	major_no = _major;
	start_min = _start_min;
	end_min = _end_min;
	device_id = _device_id;
}

//
// Copy constructor for 'dc_minor_range'.  We can use the default one, but
// writing one explicitly will make it easier to track this constructor if
// we make changes to this class.
//
dc_minor_range::dc_minor_range(dc_minor_range &range)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_MAJOR_STORE,
	    ("dc_minor_range::dc_minor_range(%p).\n", range));

	major_no = range.major_no;
	start_min = range.start_min;
	end_min = range.end_min;
	device_id = range.device_id;
}

//
// Destructor for 'dc_minor_range'.
//
dc_minor_range::~dc_minor_range()
{
	// Do nothing.
}

dc_major_store::dc_major_store(major_t maj) : major_no(maj)
{
	next = NULL;
}

dc_major_store::~dc_major_store()
{
	cleanup();
	next = NULL;
}

major_t
dc_major_store::get_major()
{
	return (major_no);
}

//
// This function cleans up the 'tmp_range' array, deleting all the values in it.
//
void
dc_major_store::cleanup()
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_MAJOR_STORE,
	    ("dc_major_store::cleanup().\n"));
	dc_minor_range *tmp_range;

	while ((tmp_range = range_list.reapfirst()) != NULL) {
		delete (tmp_range);
	}
}

//
// Clears all the device mappings for a given service.
//
void
dc_major_store::clear_mapping(unsigned int device_id,
    dc_minor_range_list *service_range_list)
{
	dc_minor_range *tmp_range;
	bool found;

	DCS_DBPRINTF_W(DCS_TRACE_DC_MAJOR_STORE,
	    ("dc_major_store::clear_mapping(%d, %p).\n",
	    device_id, service_range_list));

	ASSERT(service_range_list != NULL);

	if (service_range_list->empty()) {
		return;
	}

	//
	// Iterate through all the stored ranges, adding all entries for
	// this major number.  Use the fact that 'service_range_list' is
	// sorted by major number.
	//
	service_range_list->atfirst();
	while ((tmp_range = service_range_list->get_current()) != NULL) {
		// Sanity check.
		ASSERT(tmp_range->device_id == device_id);

		if (tmp_range->major_no < major_no) {
			service_range_list->advance();
			continue;
		}

		if (tmp_range->major_no > major_no) {
			return;
		}

		ASSERT(tmp_range->major_no == major_no);
		found = range_list.erase(tmp_range);
		ASSERT(found);
		// Note that 'erase' does an implicit 'advance'.
		(void) service_range_list->erase(tmp_range);
		delete tmp_range;
	}
}

//
// Adds the specified mappings to the appropriate 'dc_major_store'
// objects.  These mappings are assumed to be previously ordered, so no
// validation checking is required.
// This routine must leave the current pointer within 'service_range_list'
// after the last record for the major number 'major_no'.
//
void
dc_major_store::add_mapping_nocheck(dc_minor_range_list *service_range_list)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_MAJOR_STORE,
	    ("dc_major_store::add_mapping_nocheck(%p).\n",
	    service_range_list));

	dc_minor_range *range, *range2;

	ASSERT(service_range_list != NULL);

	if (service_range_list->empty()) {
		DCS_DBPRINTF_G(DCS_TRACE_DC_MAJOR_STORE,
		    ("add_mapping_nocheck(%p): list empty.\n", this));
		return;
	}

	//
	// Iterate through all the stored ranges, adding all entries for
	// this major number.  Use the fact that 'service_range_list' is
	// sorted by major number.
	//
	while ((range = service_range_list->get_current()) != NULL) {
		if (range->major_no > major_no) {
			DCS_DBPRINTF_G(DCS_TRACE_DC_MAJOR_STORE,
			    ("add_mapping_nocheck(%p): range->major_no (=%d)"
			    " > major_no (=%d).\n", this,
			    range->major_no, major_no));
			return;
		}

		//
		// The calling routine should have ensured that 'range->major'
		// will be >= 'major_no'.
		//
		ASSERT(range->major_no == major_no);

		//
		// Now, we need to add this to 'range_list'.  Note that
		// 'range_list' is sorted by minor number.
		//
		for (range_list.atfirst();
		    (range2 = range_list.get_current()) != NULL;
		    range_list.advance()) {
			if (range->end_min < range2->start_min) {
				DCS_DBPRINTF_W(DCS_TRACE_DC_MAJOR_STORE,
				    ("add_mapping_nocheck(%p): inserting "
				    "(%d, %d-%d) before (%d, %d).\n",
				    this,
				    range->major_no, range->start_min,
				    range->end_min, range2->start_min,
				    range2->end_min));
				//
				// Input range is on the left of the current
				// range.  Insert behind the current record
				// to maintain ordering.
				//
				range_list.insert_b(range);
				break;
			}

			//
			// Sanity checks.  If the input range is not on the
			// left of the current range, it MUST be entirely on
			// its right, without overlaps.
			//
			ASSERT(range->end_min > range2->end_min);
			ASSERT(range->start_min > range2->end_min);
		}

		if (range2 == NULL) {
			DCS_DBPRINTF_W(DCS_TRACE_DC_MAJOR_STORE,
			    ("add_mapping_nocheck(%p): appending "
			    "(%d, %d-%d).\n", this,
			    range->major_no, range->start_min,
			    range->end_min));
			//
			// Input range is to the right of every range in
			// 'range_list'.
			//
			range_list.append(range);
		}

		service_range_list->advance();
	}
}

//
// Returns false if any of the devices specified by the range belong to
// any service other than the specified one.
//
dc_error_t
dc_major_store::possible_map(modify_type_t type, minor_t smin, minor_t emin,
    unsigned int device_id)
{
	dc_minor_range *tmp_range;
	minor_t curr_smin, curr_emin;

	DCS_DBPRINTF_W(DCS_TRACE_DC_MAJOR_STORE,
	    ("dc_major_store::possible_map(%d, %d, %d, %d).\n",
	    type, smin, emin, device_id));

	ASSERT((type == add_mapping) || (type == remove_mapping));

	if (smin > emin) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_MAJOR_STORE,
		    ("possible_map(%p, %d): smin (=%d) > emin (=%d)\n",
		    this, device_id, smin, emin));
		return (DCS_ERR_DEVICE_INVAL);
	}

	for (range_list.atfirst();
	    (tmp_range = range_list.get_current()) != NULL;
	    range_list.advance()) {
		curr_smin = tmp_range->start_min;
		curr_emin = tmp_range->end_min;

		if (smin > curr_emin) {
			continue;
		}

		if (emin < curr_smin) {
			// We have gone past the list of stored ranges.
			if (type == add_mapping) {
				return (DCS_SUCCESS);
			} else {
				DCS_DBPRINTF_R(DCS_TRACE_DC_MAJOR_STORE,
				    ("possible_map(%p, %d): emin (=%d) < "
				    "curr_smin\n", this, device_id,
				    emin, curr_smin));
				return (DCS_ERR_DEVICE_INVAL);
			}
		}

		ASSERT((smin <= curr_emin) && (emin >= curr_smin));

		//
		// There is an overlap - check the device id values.
		//
		if (device_id != tmp_range->device_id) {
			DCS_DBPRINTF_R(DCS_TRACE_DC_MAJOR_STORE,
			    ("possible_map(%p, %d): device busy\n",
			    this, device_id));
			return (DCS_ERR_DEVICE_BUSY);
		}

		// If we are removing a range, the entire range should be here.
		if ((type == remove_mapping) &&
		    (!(IN_BETWEEN(smin, curr_smin, curr_emin) &&
		    IN_BETWEEN(emin, curr_smin, curr_emin)))) {
			DCS_DBPRINTF_R(DCS_TRACE_DC_MAJOR_STORE,
			    ("possible_map(%p, %d): not entire range\n",
			    this, device_id));
			return (DCS_ERR_DEVICE_INVAL);
		} else {
			return (DCS_SUCCESS);
		}
	}

	if (type == add_mapping) {
		return (DCS_SUCCESS);
	} else {
		DCS_DBPRINTF_R(DCS_TRACE_DC_MAJOR_STORE,
		    ("possible_map(%p, %d): type (%d) != add_mapping\n",
		    this, device_id, type));
		return (DCS_ERR_DEVICE_INVAL);
	}
}

//
// Returns true if a mapping exists for the minor number specified, and false
// if not.  If a mapping exists, then the device_id is returned in 'device_id'.
//
bool
dc_major_store::get_map(minor_t _min, unsigned int *device_id)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_MAJOR_STORE,
	    ("dc_major_store::get_map(%d, %p).\n", _min, device_id));

	dc_minor_range_list::ListIterator iterator(range_list);

	dc_minor_range *tmp_range;

	for (; (tmp_range = iterator.get_current()) != NULL;
	    iterator.advance()) {
		if (IN_BETWEEN(_min, tmp_range->start_min,
		    tmp_range->end_min)) {
			*device_id = tmp_range->device_id;
			return (true);
		}
		if (_min < tmp_range->start_min)
			return (false);
	}

	return (false);
}

//
// Insert entries into 'service_range_list' in a sorted manner.  The sorting is
// a two-level sort, first by major number and then by minor number.
//
void
dc_major_store::insert_sorted(dc_minor_range *new_range,
    dc_minor_range_list *service_range_list)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_MAJOR_STORE,
	    ("dc_major_store::insert_sorted(%p, %p).\n",
	    new_range, service_range_list));

	dc_minor_range *tmp_range;

	for (service_range_list->atfirst();
	    (tmp_range = service_range_list->get_current()) != NULL;
	    service_range_list->advance()) {

		if (tmp_range->major_no < new_range->major_no)
			continue;

		if (tmp_range->major_no > new_range->major_no) {
			service_range_list->insert_b(new_range);
			return;
		}

		ASSERT(tmp_range->major_no == new_range->major_no);

		if (tmp_range->start_min > new_range->start_min) {
			service_range_list->insert_b(new_range);
			return;
		}
	}

	service_range_list->append(new_range);
}

//
// Add the given mapping to the list of device ranges cached in this object.
// Also reflect any changes in 'service_range_list' to have the entries sorted
// by service id for quicker retrieval.
// This call is made only if 'possible_map' returns successfully, so this
// will always succeed.
//
dc_error_t
dc_major_store::add_map(minor_t start_min, minor_t end_min,
    unsigned int device_id, dc_minor_range_list *service_range_list)
{
	dc_minor_range *tmp_range, *next_range;
	unsigned int curr_device_id;
	minor_t curr_smin, curr_emin;
	Environment env;
	dc_minor_range *new_range;

	DCS_DBPRINTF_W(DCS_TRACE_DC_MAJOR_STORE,
	    ("dc_major_store::add_map(%d, %d, %d, %p).\n",
	    start_min, end_min, device_id, service_range_list));

	ASSERT(possible_map(add_mapping, start_min, end_min, device_id)
	    == DCS_SUCCESS);

	for (range_list.atfirst();
	    (tmp_range = range_list.get_current()) != NULL;
	    range_list.advance()) {
		curr_smin = tmp_range->start_min;
		curr_emin = tmp_range->end_min;
		curr_device_id = tmp_range->device_id;

		if ((start_min > curr_emin+1) ||
		    ((start_min == curr_emin+1) &&
		    (device_id != curr_device_id))) {
			//
			// Input range is on the right of the current range.
			//
			continue;
		}

		if ((end_min+1 < curr_smin) ||
		    ((end_min+1 == curr_smin) &&
		    (device_id != curr_device_id))) {
			//
			// Input range is on the left of the current range.
			// Write out a new entry.
			//
			new_range = new dc_minor_range(major_no, start_min,
			    end_min, device_id);
			range_list.insert_b(new_range);
			insert_sorted(new_range, service_range_list);
			break;
		}

		//
		// Input overlaps this range.  Some messy computing is required
		// to combine the input range with the list.
		//

		ASSERT(start_min <= curr_emin+1);
		ASSERT(end_min+1 >= curr_smin);

		//
		// Overlapping ranges MUST have the same device id, otherwise
		// the call to possible_map() would have returned an error.
		//
		ASSERT(device_id == curr_device_id);

		if (start_min < curr_smin) {
			curr_smin = tmp_range->start_min = start_min;
		}

		if (end_min <= curr_emin) {
			break;
		}

		range_list.advance();
		while ((next_range = range_list.get_current()) != NULL) {
			if ((next_range->start_min > end_min+1) ||
			    (next_range->device_id != curr_device_id)) {
				break;
			}

			if (next_range->end_min > end_min) {
				end_min = next_range->end_min;
			}

			range_list.advance();
			(void) range_list.erase(next_range);
			(void) service_range_list->erase(next_range);
			delete next_range;
		}

		tmp_range->end_min = end_min;
		break;
	}

	if (tmp_range == NULL) {
		//
		// Input was to the right of all ranges - append a new entry.
		//
		new_range = new dc_minor_range(major_no, start_min, end_min,
		    device_id);
		range_list.append(new_range);
		insert_sorted(new_range, service_range_list);
	}

	return (DCS_SUCCESS);
}

//
// Remove the given mapping from the list of device ranges cached in this
// object.  Also reflect any changes in 'service_range_list' to have the
// entries sorted by service id for quicker retrieval.
//
dc_error_t
dc_major_store::remove_map(minor_t start_min, minor_t end_min,
    unsigned int device_id, dc_minor_range_list *service_range_list)
{
	dc_minor_range *tmp_range;
	unsigned int curr_device_id;
	minor_t curr_smin, curr_emin;
	Environment env;
	dc_minor_range *new_range;
	bool found = false;

	DCS_DBPRINTF_W(DCS_TRACE_DC_MAJOR_STORE,
	    ("dc_major_store::remove_map(%d, %d, %d, %p).\n",
	    start_min, end_min, device_id, service_range_list));

	ASSERT(possible_map(remove_mapping, start_min, end_min, device_id)
	    == DCS_SUCCESS);

	dc_error_t err_val = DCS_SUCCESS;

	for (range_list.atfirst();
	    (tmp_range = range_list.get_current()) != NULL;
	    range_list.advance()) {
		curr_smin = tmp_range->start_min;
		curr_emin = tmp_range->end_min;
		curr_device_id = tmp_range->device_id;

		if (start_min > curr_emin) {
			//
			// Input range is on the right of the current range.
			//
			continue;
		}

		if (end_min < curr_smin) {
			//
			// Input range is on the left of us - break out.
			//
			break;
		}

		if ((start_min < curr_smin) || (end_min > curr_emin)) {
			//
			// Input range is not fully contained in the list -
			// break out.
			//
			break;
		}

		if (curr_device_id != device_id) {
			//
			// The device range is configured for some other device
			// id.
			//
			break;
		}

		// Found the range.
		found = true;

		if (start_min == curr_smin) {
			if (end_min == curr_emin) {
				(void) range_list.erase(tmp_range);
				if (service_range_list != NULL) {
					(void) service_range_list->erase(
					    tmp_range);
				}
				delete (tmp_range);
				break;
			}

			ASSERT(end_min < curr_emin);
			tmp_range->start_min = end_min+1;
			break;
		}

		ASSERT(start_min > curr_smin);

		tmp_range->end_min = start_min-1;
		if (end_min == curr_emin) {
			break;
		}

		ASSERT(end_min < curr_emin);

		new_range = new dc_minor_range(major_no, end_min+1, curr_emin,
		    curr_device_id);
		range_list.insert_a(new_range);
		if (service_range_list != NULL) {
			insert_sorted(new_range, service_range_list);
		}
		break;
	}

	if (!found) {
		err_val = DCS_ERR_DEVICE_INVAL;
		DCS_DBPRINTF_R(DCS_TRACE_DC_MAJOR_STORE,
		    ("remove_map(%p, %d): device not found.\n",
		    this, device_id));

	}

	return (err_val);
}
