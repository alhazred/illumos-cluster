/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _DC_MAJOR_STORE_H
#define	_DC_MAJOR_STORE_H

#pragma ident	"@(#)dc_major_store.h	1.11	08/05/20 SMI"

#include <sys/dc_ki.h>
#include <sys/os.h>
#include <sys/list_def.h>
#include <dc/libdcs/libdcs.h>
#include <h/ccr.h>

class dc_minor_range {
public:
	dc_minor_range(major_t major, minor_t start_min, minor_t end_min,
	    unsigned int device_id);
	dc_minor_range(dc_minor_range &range);
	~dc_minor_range();

	major_t major_no;
	minor_t start_min;
	minor_t end_min;
	unsigned int device_id;
};

typedef enum {
	add_mapping = 1,
	remove_mapping = 2
} modify_type_t;

typedef DList<dc_minor_range> dc_minor_range_list;

class dc_major_store {
public:
	dc_major_store(major_t);
	~dc_major_store();

	dc_error_t init();

	dc_error_t possible_map(modify_type_t map_type, minor_t smin,
	    minor_t emin, unsigned int device_id);
	bool get_map(minor_t min, unsigned int *device_id);
	dc_error_t add_map(minor_t start_min, minor_t end_min,
	    unsigned int device_id, dc_minor_range_list *service_range_list);
	dc_error_t remove_map(minor_t start_min, minor_t end_min,
	    unsigned int device_id, dc_minor_range_list *service_range_list);
	major_t get_major();

	//
	// Clears all the device mappings for a given service.
	//
	void clear_mapping(unsigned int service_id,
	    dc_minor_range_list *service_range_list);

	//
	// Adds the specified mappings to the appropriate 'dc_major_store'
	// objects.  These mappings are assumed to be good, so no validation
	// checking is required.
	//
	void add_mapping_nocheck(dc_minor_range_list *service_range_list);

	//
	// Pointer to the next 'dc_major_store' instance with the same hashed
	// value.
	//
	dc_major_store *next;

private:
	// Major number associated with this instance
	major_t major_no;

	// Stores the list of configured minor ranges for this major number.
	dc_minor_range_list range_list;

	// Helper function that disposes of 'range_list'
	void cleanup();

	static void insert_sorted(dc_minor_range *new_range,
	    dc_minor_range_list *service_range_list);
};

#endif	/* _DC_MAJOR_STORE_H */
