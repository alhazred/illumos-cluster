/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _DC_MAPPER_IMPL_H
#define	_DC_MAPPER_IMPL_H

#pragma ident	"@(#)dc_mapper_impl.h	1.28	08/05/20 SMI"

#include <h/dc.h>
#include <dc/server/dc_repl_server.h>

//
// Declaration for replicated dc_mapper object.
//
class dc_mapper_impl :
    public mc_replica_of<mdc::dc_mapper> {

public:
	// Constructor for the primary object.
	dc_mapper_impl(dc_repl_server *serverp, unsigned int id);

	// Constructor for the secondary object.
	dc_mapper_impl(mdc::dc_mapper_ptr primary_obj);

	~dc_mapper_impl();
	void _unreferenced(unref_t);

	static dc_mapper_impl *get_dc_mapper_impl(mdc::dc_mapper_ptr mapper);

	//
	// Called from dc_config::shutdown_service() to release the reference
	// to the 'device_server' object.
	//
	void release_device_server();

	// Return true if the given device ID matches our ID.
	bool equals_devid(unsigned int id);

	// Get the device id for this object.
	unsigned int get_devid();

	// Set the devid for this object.
	void set_devid(unsigned int id);

	// Return a new reference to the device server.
	mdc::device_server_ptr get_devobj() const;

	//
	// Set the device server object (used to complete construction).
	//
	void set_devobj(mdc::device_server_ptr devobj);

	static void init();

	//
	// Called by the dc_config object to dump the state of this object
	// to secondary DCS services.
	//
	void dump_state(repl_dc::repl_dc_ckpt_ptr secondary_ckpt,
	    Environment &env);

	// Checkpoint accesor function.
	repl_dc::repl_dc_ckpt_ptr	get_checkpoint();

private:
	// Common code for the constructors.
	void construct(unsigned int id);

	unsigned int devid;
	mdc::device_server_var dev_server;
};

typedef SList<dc_mapper_impl> dc_mapper_list;

#include <dc/server/dc_mapper_impl_in.h>

#endif	/* _DC_MAPPER_IMPL_H */
