/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _DC_CONFIG_IMPL_H
#define	_DC_CONFIG_IMPL_H

#pragma ident	"@(#)dc_config_impl.h	1.61	08/05/20 SMI"

#include <h/dc.h>
#include <h/naming.h>
#include <nslib/ns.h>
#include <sys/types.h>
#include <h/sol.h>
#include <orb/infrastructure/orb_conf.h>
#include <sys/list_def.h>
#include <dc/server/dc_mapper_impl.h>
#include <dc/server/dc_storage.h>

#include <dc/server/dc_repl_server.h>
#include <dc/server/dc_services.h>
#include <dc/libdcs/libdcs.h>
#include <sys/sol_version.h>

//
// Lock ordering:
//	dc_services->lock()
//		dc_service->lock()
//			dc_service->start_service_lock()
//				dc_config->dsm_list_lock()
//					dc_config->lock_list()
//

class reservation_lock {
public:
	reservation_lock(sol::nodeid_t nd, sol::incarnation_num inc,
	    mdc::reservation_client_ptr resv_obj_ptr);
	~reservation_lock();

	sol::nodeid_t node;
	sol::incarnation_num incarnation;
	mdc::reservation_client_var resv_obj;
};

typedef SList<reservation_lock> reservation_lock_list;

//
// Device Configurator Object. The object is bound to name server
// when the server module gets loaded.
//
// CSTYLED
class dc_config_impl :
    public mc_replica_of<mdc::dc_config> {

public:
	// Constructor for the primay object.
	dc_config_impl(dc_repl_server *serverp);

	// Constructor for the secondary object.
	dc_config_impl(mdc::dc_config_ptr obj, dc_repl_server *serverp);

	~dc_config_impl();
	void _unreferenced(unref_t);

	//
	// IDL definitions.
	//
	// mdc::dc_config::=
	void cascaded_get_configured_nodes(sol::major_t gmajor,
	    sol::minor_t gminor, sol::nodeid_t id, fs::dc_callback_ptr cb_obj,
	    bool &is_ha, CORBA::String_out ha_service_name,
	    sol::nodeid_seq_t_out nodes, Environment &_environment);
	void unregister_callback(sol::major_t major, sol::minor_t minor,
	    fs::dc_callback_ptr cb_obj, Environment &_environment);
	void cascaded_get_device_server(sol::major_t major, sol::minor_t gminor,
	    sol::nodeid_t id, mdc::device_server_out dev_server,
	    bool &is_ha, Environment &_environment);
	void register_dsm(mdc::device_service_manager_ptr dsm,
	    mdc::device_server_ptr local_device_server,
	    sol::nodeid_t node_id,
	    mdc::device_service_info_seq_out service_info_seq,
	    Environment &_environment);
	void register_device_server(mdc::device_server_ptr device_server,
	    const char *service_name, Environment &_environment);
	void create_service_class(const char *service_class,
	    const char *user_program, mdc::ha_dev_type ha_type,
	    Environment &_environment);
	void set_service_class_user_program(const char *service_class,
	    const char *user_program, Environment &_environment);
	void set_service_class_ha_type(const char *service_class,
	    mdc::ha_dev_type ha_type, Environment &_environment);
	void get_service_class_names(sol::string_seq_t_out services,
	    Environment &_environment);
	void get_service_class_parameters(const char *service_class,
	    CORBA::String_out user_program, mdc::ha_dev_type &ha_type,
	    Environment &_environment);
	void remove_service_class(const char *service_class,
	    Environment &_environment);
	void create_device_service(const char *service_name,
	    const char *service_class, bool failback_enabled,
	    const mdc::node_preference_seq_t &nodes, uint32_t num_secondaries,
	    const mdc::dev_range_seq_t &gdev_range_seq,
	    const mdc::service_property_seq_t &property_seq,
	    Environment &_environment);
	void set_failback_mode(const char *service_name,
	    bool failback_enabled, Environment &_environment);
	void set_active_secondaries(const char *service_name,
	    uint32_t num_secondaries, Environment &_environment);
	void add_node(const char *service_name,
	    const mdc::node_preference &node, Environment &_environment);
	void remove_node(const char *service_name, sol::nodeid_t node,
	    Environment &_environment);
	void add_devices(const char *service_name,
	    const mdc::dev_range_seq_t &gdev_range_seq,
	    Environment &_environment);
	void remove_devices(const char *service_name,
	    const mdc::dev_range_seq_t &gdev_range_seq,
	    Environment &_environment);
#if SOL_VERSION > __s10
	void create_device_nodes(sol::major_t maj, sol::minor_t minor,
	    mdc::service_property_seq_t_out disk_list, const char *device_name,
	    Environment &_environment);
#else
	void create_device_nodes(sol::major_t maj, sol::minor_t minor,
	    mdc::service_property_seq_t_out disk_list,
	    Environment &_environment);
#endif
	void set_properties(const char *service_name,
	    const mdc::service_property_seq_t &property_seq,
	    Environment &_environment);
	void shutdown_service(const char *service_name, bool suspend,
	    Environment &_environment);
	void resume_service(const char *service_name,
	    Environment &_environment);
	void remove_properties(const char *service_name,
	    const sol::string_seq_t &property_seq,
	    Environment &_environment);
	void get_property(const char *service_name,
	    const char *property_name, CORBA::String_out property_value,
	    Environment &_environment);
	void get_all_properties(const char *service_name,
	    mdc::service_property_seq_t_out property_seq,
	    Environment &_environment);
	void remove_service(const char *service_name,
	    Environment &_environment);
	void get_service_names_of_class(const char *service_class,
	    sol::string_seq_t_out services, Environment &_environment);
	void get_service_names(sol::string_seq_t_out services,
	    Environment &_environment);
	void get_service_parameters(const char *service_name,
	    CORBA::String_out service_class_name, bool &failback_enabled,
	    mdc::node_preference_seq_t_out nodes, uint32_t &num_secondaries,
	    mdc::dev_range_seq_t_out gdev_range_seq,
	    mdc::service_property_seq_t_out property_seq,
	    Environment &_environment);
	void get_service_by_dev_t(sol::major_t maj, sol::minor_t min,
	    CORBA::String_out service_name, Environment &_environment);
	void cascaded_switch_device_service(const char *service_name,
	    sol::nodeid_t id, Environment &_environment);
	void do_switchback(const char *service_name, sol::nodeid_t id,
	    Environment &_environment);
	void get_resv_lock(sol::nodeid_t id, mdc::reservation_client *resv_obj,
	    cmm::cluster_state_t &request_state, Environment &_environment);
	void release_resv_lock(mdc::reservation_client *resv_obj,
	    Environment &_environment);
	void is_suspended(const char *service_name, bool &suspended,
	    Environment &_environment);
	void set_service_inactive(const char *service_name,
	    Environment &_environment);
	void set_service_active(const char *service_name,
	    Environment &_environment);
	void change_nodes_priority(const char *service_name,
	    const mdc::node_preference_seq_t &nodes_seq,
		Environment &_environment);
	//

	// Disk reservation locking checkpoints.
	void ckpt_get_resv_lock(sol::nodeid_t id,
	    mdc::reservation_client *resv_obj,
	    sol::incarnation_num incarnation);
	void ckpt_release_resv_lock(mdc::reservation_client *resv_obj);

	// Dump state to a new secondary. Called on the primary.
	void ckpt_dump_state(const repl_dc_data::dc_minor_alloc_info &min_info,
	    const repl_dc_data::dc_storage_info &storage_info,
	    const repl_dc_data::dc_inst_alloc_info &inst_info);

	// Called from dc_repl_server::become_primary();
	void convert_to_primary();

	// Called from dc_repl_server::become_secondary();
	void convert_to_secondary();

	// Start up a service.  Return the started-up device server.
	mdc::device_server_ptr start_service(dc_service *service,
	    nodeid_t id, bool force_preferred_primary, bool force_active,
	    mdc::dc_mapper_var &mapper_obj, Environment &env);

	//
	// Called on the primary dc when a secondary is added.
	// This function brings the secondary in sync with the primary.
	//
	void dump_state(repl_dc::repl_dc_ckpt_ptr secondary_ckpt,
	    Environment &env);

	//
	// Functions called on the secondary DCS replica via the repl_dc_ckpt
	// interfaces of the same name.
	//
	void ckpt_add_dc_mapper(const mdc::dc_mapper_ptr primary_obj);
	void ckpt_set_dc_mapper(const mdc::dc_mapper_ptr primary_obj,
	    uint32_t id, mdc::device_server_ptr dev_server);
	void ckpt_device_server(const mdc::dc_mapper_ptr primary_obj,
	    mdc::device_server_ptr dev_server);
	void ckpt_remove_dc_mapper(mdc::dc_mapper_ptr mapper_obj);
	void ckpt_register_dsm(mdc::device_service_manager_ptr dsm,
	    mdc::device_server_ptr local_device_server,
	    sol::nodeid_t node_id);
	void ckpt_unregister_dsm(mdc::device_service_manager_ptr dsm);
	void ckpt_add_callback(sol::major_t maj, sol::minor_t min,
	    fs::dc_callback_ptr cb_obj, mdc::dc_mapper_ptr mapper_obj);
	void ckpt_remove_callback(sol::major_t maj, sol::minor_t min,
	    fs::dc_callback_ptr cb_obj);

	// Helper functions to add and remove callbacks.
	void add_callback_internal(dc_callback_info *db_info);
	void remove_callback_internal(sol::major_t maj, sol::minor_t min,
	    fs::dc_callback_ptr cb_obj);

	//
	// Called when a global minor is unmapped. Invalidates all
	// client's cache.
	//
	void invalidate_clients(major_t maj, minor_t gmin);

	// Find a device server from a given id.
	mdc::device_server_ptr find_devobj(const unsigned int devid);

	// Return a new reference to an existing mapper object for a given id.
	mdc::dc_mapper_ptr find_mapper(unsigned int devid);

	// Get the local device service given a node id.
	mdc::device_server_ptr get_local_device_server(sol::nodeid_t id);

	//
	// Locking routines called from the _unreferenced() implementations
	// of dc_server and dc_mapper before they call remove_server() and
	// remove_mapper().  Locking is not done within the remove_server()
	// and remove_mapper() calls because the lock is also used to
	// implement the _last_unref() protocol.
	//
	void lock_list();
	void unlock_list();

	//
	// Remove entry from the list, during unreferenced of
	// server and mapper objects.
	// These should be called with lock_list() held.
	//
	void remove_mapper(dc_mapper_impl *mapperp);

	bool is_primary();

	// List of device service managers.
	class dsm_info_t : public _SList::ListElem {
	public:
		dsm_info_t(mdc::device_service_manager_ptr dsm_obj,
		    mdc::device_server_ptr local_device_server,
		    sol::nodeid_t nid);

		// Public data members for convenience.
		mdc::device_service_manager_var dsm;
		mdc::device_server_var local_device_server;
		sol::nodeid_t node_id;
	};

	// Checkpoint accesor function.
	repl_dc::repl_dc_ckpt_ptr	get_checkpoint();

	//
	// Helper function to delay freezing of DCS primary until
	// all in-progress switchovers have completed.
	//
	void prepare_for_freeze();
	void reset_freeze_prepare_request();
	bool safe_to_switchover(Environment &_environment);
	void notify_switchover_end();

	void upgrade_device_references();

private:
	// Pointer to the replica for this HA object.
	dc_repl_server *dc_repl_serverp;

	bool primary;

	//
	//
	// rwlock protects device server object list.
	// The following rwlock protects both server and client lists also.
	//
	os::rwlock_t list_lock;

	// List of mappers identified by device server id.
	dc_mapper_list mapper_list;

	//
	// List of dc_callback objects.  This is stored in the DCS object
	// (as opposed to the 'dc_service' object) because the 'dc_service'
	// objects exist only on the primary.
	//
	dc_callback_info_list callbacks;

	// variables for supporting disk reservation locking
	reservation_lock_list resv_locks;
	os::rwlock_t resv_lock_access;		// control entry into lock code

	IntrList<dsm_info_t, _SList> dsm_list;

	//
	// Protects dsm_list.  While holding this lock, we should NEVER
	// try to hold the services->lock.
	//
	os::rwlock_t dsm_list_lock;

	//
	// Lock for accessing freeze_prepare_request.
	//
	os::mutex_t freeze_prepare_lock;

	//
	// Used to signal freeze_prepare_request() when all in-progress
	// switchovers have completed.
	//
	os::condvar_t freeze_prepare_cv;

	//
	// This is set when freeze_primary_prepare() is called on the DCS
	// primary.
	//
	bool freeze_prepare_request;

	//
	// Used to keep track of switchovers that need to finish before DCS
	// can be frozen.
	//
	uint_t switchovers_in_progress;

	//
	// A dsm may enter the dsm_list in one of two ways:
	// (1) via the dc_config::register_dsm() call on the primary, and
	// (2) via the dc_config::ckpt_register_dsm() call on the secondary.
	// When a node dies, the node's dsm dies with it.  We remove the dsm
	// from dsm_list in one of two ways:
	// (1) if the node comes back up, a new dsm calls register_dsm()
	//  with the same node_id.  At this point, we remove any previous
	//  dsm in dsm_list with the same node_id.
	// (2) if start_device_service() is called on a dsm in dsm_list
	//  and gets an ECOMM exception, the dsm is considered dead
	//  and is removed from the list.
	//

	//
	// This is a helper function called by register_dsm() to add a dsm to
	// 'dsm_list'.  If 'new_dsm' is already in the list, this function
	// returns false, otherwise it returns true.  If there is a dsm with
	// the same nodeid already in the list, this function replaces it
	// with the new one and returns true.
	//
	bool add_dsm_to_list_locked(dsm_info_t *new_dsm);

	//
	// Helper function to unregister a dsm if an invocation on the dsm
	// object results in a ECOMM exception.
	//
	bool remove_dsm_if_comm_error(mdc::device_service_manager_ptr dsm,
	    CORBA::Exception *ex, Environment &env);

	//
	// Called by register_dsm(), and by remove_dsm_if_error() when a dsm
	// needs to be removed from 'dsm_list'.  This call removes the dsm
	// passed in from 'dsm_list' and checkpoints this to the secondaries.
	//
	void unregister_dsm(dsm_info_t *dsm_info_p, Environment &env);

	//
	// Return the DSM object for a given node ID.
	// The dsm_list_lock should be held.
	//
	mdc::device_service_manager_ptr get_dsm_object(sol::nodeid_t);

	//
	// Return the dsm_info_t structure for the given DSM.
	// Return NULL if the DSM is not found.
	// The dsm_list_lock should be held.
	//
	dsm_info_t *find_dsm_info(mdc::device_service_manager_ptr dsm);

	//
	// Returns the dsm_info_t structure for the given node ID.
	// Return NULL if the DSM is not found.
	// The dsm_list_lock should be held.
	//
	dsm_info_t *find_dsm_info(sol::nodeid_t node_id);

	//
	// Helper function that returns a copy of a range list.
	//
	dc_minor_range_list *copy_range_list(dc_minor_range_list *);

	//
	// Helper function used to do switchbacks and switchovers.
	//
	void do_switchover(dc_service *service, sol::nodeid_t dsm_id,
	    bool switchback, Environment &_environment);

	dc_error_t fill_in_major_names(mdc::long_seq_t &major_numbers,
	    sol::string_seq_t &major_names);
	dc_error_t validate_major_numbers(const mdc::dev_range_seq_t &);
	dc_error_t validate_dsm_major_numbers(mdc::device_service_manager_ptr);

	//
	// Helper function to create nodes on 1 node
	//
#if SOL_VERSION > __s10
	void create_one_device_nodes(sol::major_t maj, sol::minor_t mind,
	    sol::nodeid_t node_id, const char *device_name);
#else
	void create_one_device_nodes(sol::major_t maj, sol::minor_t mind,
	    sol::nodeid_t node_id);
#endif
};

// Global device configurator object initialized during modload.
extern dc_config_impl *dc_config_obj;
// Global device configuration IDL version
extern version_manager::vp_version_t dcs_current_version;

#endif	/* _DC_CONFIG_IMPL_H */
