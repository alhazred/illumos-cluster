//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)unode_transport.cc	1.127	08/05/20 SMI"

#include <orb/fault/fault_injection.h>

#include <transports/unode/unode_transport.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/flow/resource.h>
#include <orb/msg/orb_msg.h>
#include <orb/msg/message_mgr.h>
#include <vm/vm_comm.h>
#include <fcntl.h>
#include <thread.h>
#include <pthread.h>
#include <unistd.h>
#include <stdio.h>
#include <orb/transport/path_manager.h>

// Retry time on getting EAGAIN on door_call
#define	UNODE_RETRY_EAGAIN	(os::usec_t)1000000	// 1 sec
#define	UNODE_PATH_INITIATE_TIMEOUT	(os::usec_t)500000	// 500 msec

#ifdef DEBUG
unode_stats_t unode_stats;
#endif

#ifdef UNODE_DBGBUF
dbg_print_buf	unode_debug(8192);
#endif

//
// unode_pathend methods
//

unode_pathend::unode_pathend(unode_transport *tr, clconf_path_t *p) :
	pe_priv_incn(INCN_UNKNOWN),
	dp(-1),
	pathend(tr, p)
{
	clconf_cluster_t *cl = clconf_cluster_get_current();
	// Get name of remote door from clconf_path
	clconf_adapter_t *ap = clconf_path_get_adapter(p, CL_REMOTE, cl);
	dname = os::strdup(
		clconf_obj_get_property((clconf_obj_t *)ap, "door_name"));
	clconf_obj_release((clconf_obj_t *)cl);
	// if door_name was NULL, it would be a configuration error
	ASSERT(dname != NULL);

	init_msg_buf_size = vm_comm::max_btstrp_np_string() +
	    sizeof (unode_init_msg);
	ASSERT(init_msg_buf_size > 0);
	init_msg_buf = (char *)malloc(init_msg_buf_size);
	ASSERT(init_msg_buf != NULL);

	pm_send_buf = new char[PM_MESSAGE_SIZE];
	ASSERT(pm_send_buf != NULL);
	pm_recv_buf = new char[PM_MESSAGE_SIZE];
	ASSERT(pm_recv_buf != NULL);

	// Initialize pm_msg
	pm_msg = new unode_pm_msg;
	ASSERT(pm_msg != NULL);
	pm_msg->header.msgtype = UNODE_PM_MSGTYPE;
	pm_msg->header.src = orb_conf::current_id_node();
#ifdef DEBUG
	pm_msg->header.dest.ndid = pe_node.ndid;
	pm_msg->header.dest.incn = INCN_UNKNOWN;
#endif
	// ASSERT that pm_msg->buf has 64-bit alignment
	ASSERT(((uintptr_t)(pm_msg->buf) & 0x7) == 0);

	UNODE_DBG(("New unode_pathend to node %u adapter %s\n",
	    pe_node.ndid, dname));
}

unode_pathend::~unode_pathend()
{
	UNODE_DBG(
	    ("Delete unode_pathend to node %u adapter %s\n",
	    pe_node.ndid, dname));
	ASSERT(get_state() == P_DELETED);
	if (dp >= 0) {
		// Neglect return value. Not much we can do if this fails
		(void) close(dp);
		dp = -1;
	}
	free(init_msg_buf);
	delete [] pm_send_buf;
	delete [] pm_recv_buf;
	delete pm_msg;
	delete [] dname;
}

// Can be in this routine only once. So can drop lock and use per-pathend
// variables
void
unode_pathend::pm_send_internal()
{
	ASSERT(lock_held());
	unlock();

	// Should not be sending PM message until initiate completes
	ASSERT(pm_msg->header.dest.incn != INCN_UNKNOWN);

	door_arg_t darg;

	atomic_copy_64((uint64_t *)pm_msg->buf, (uint64_t *)pm_send_buf);
#ifdef	USE_TICKSTATS
	send_tick_stat.update();		// update tick stats
#endif	// USE_TICKSTATS

	darg.data_ptr = (char *)pm_msg;
	darg.data_size = sizeof (unode_pm_msg);
	darg.desc_num = 0;
	darg.desc_ptr = NULL;
	darg.rbuf = NULL;
	darg.rsize = 0;

	// Assuming that pathend cannot get destructed or call
	// initiate while in this routine
	ASSERT(dp >= 0);
	int result = door_call(dp, &darg);
	if ((result != 0) && (errno == EBADF) &&
	    path_manager::the().is_enabled()) {
		UNODE_DBG(("Calling pathend fail for node %u "
			"adapter %u pm send failed\n", pe_node.ndid,
			local_adapter_id()));
		fail();
	}
	lock();
}

void
unode_pathend::initiate(char *&sendbuf, char *&recvbuf, incarnation_num &incn)
{
	ASSERT(lock_held());

	while ((get_state() == P_CONSTRUCTED) || (get_state() == P_INITERR)) {
		unlock();
		ASSERT(dp == -1);
		dp = open(dname, O_RDONLY);	// XX is O_RDONLY correct param
		if (dp < 0) {
			dp = -1;
			os::usecsleep(UNODE_PATH_INITIATE_TIMEOUT);
			lock();
			init_error();
			continue;
		}
		door_arg_t darg;
		unode_init_msg *init_msg = NULL;
		unode_init_msg *reply_msg = NULL;
		int result;

		init_msg = (unode_init_msg *)init_msg_buf;
		init_msg->header.msgtype = UNODE_INIT_MSGTYPE;
		init_msg->header.src = orb_conf::current_id_node();
#ifdef DEBUG
		init_msg->header.dest.ndid = pe_node.ndid;
		init_msg->header.dest.incn = pe_priv_incn;
#endif
		char *vbuf = init_msg_buf + sizeof (unode_init_msg);
		size_t vlen = init_msg_buf_size - sizeof (unode_init_msg);
		if (vm_comm::the().get_btstrp_np_string(vbuf, vlen) == NULL) {
			ASSERT(!"Version buffer overflow");
		};
		init_msg->version_len = (uint32_t)vlen;
		init_msg->version_compatible = 1;

		ASSERT(init_msg->version_len);
		darg.data_ptr = init_msg_buf;
		darg.data_size = sizeof (unode_init_msg) + vlen;
		darg.desc_num = 0;
		darg.desc_ptr = NULL;
		darg.rbuf = init_msg_buf;
		darg.rsize = init_msg_buf_size;

		UNODE_DBG(("Node %u: sending init message to %u\n",
		    orb_conf::local_nodeid(), pe_node.ndid));

		// Try to send init message on the door
		result = door_call(dp, &darg);

		// Expect reply in designated buffer.
		// XX Need to be more general and handly other cases?
		ASSERT((result != 0) || (darg.rbuf == init_msg_buf));

		reply_msg = (unode_init_msg *)init_msg_buf;

		UNODE_DBG(("Node %u: got init reply message %x from %u:%u "
		    "result %u:%u\n", orb_conf::local_nodeid(),
		    reply_msg->header.msgtype, reply_msg->header.src.ndid,
		    reply_msg->header.src.incn, result, errno));

		if ((result != 0) || (reply_msg->header.msgtype !=
		    UNODE_INITACCEPT_MSGTYPE)) {
			//
			// Remote side rejected request. Sleep and retry.
			//
			(void) close(dp);
			dp = -1;
			os::usecsleep(UNODE_PATH_INITIATE_TIMEOUT);
			lock();
			init_error();
			continue;
		}

		//
		// Remote side accepted request. Perform version compatibility
		// checks. But before that need to update path manager with
		// the new incarnation from the remote node.
		//
		if (!path_manager::the().update_node_incarnation(
		    reply_msg->header.src.ndid, reply_msg->header.src.incn)) {
			//
			// This incarnation has become stale. Must boot with
			// a newer incarnation to be allowed in.
			//
			(void) close(dp);
			dp = -1;
			os::usecsleep(UNODE_PATH_INITIATE_TIMEOUT);
			lock();
			init_error();
			continue;
		}
		if (!vm_comm::the().compatible_btstrp_np(
		    reply_msg->header.src.ndid, reply_msg->header.src.incn,
		    reply_msg->version_len,
		    ((char *)reply_msg) + sizeof (unode_init_msg),
		    reply_msg->version_compatible ? false : true)) {
			//
			// Remote version is incompatible. Sleep and retry
			//
			(void) close(dp);
			dp = -1;
			os::usecsleep(UNODE_PATH_INITIATE_TIMEOUT);
			lock();
			init_error();
			continue;
		}

		//
		// All checks passed!
		// Set pathend source node incarnation
		// There are 3 cases here
		// a) pe_priv_incn is UNKNOWN => we just completed our side
		// of the initiate protocol and this is the first time we know
		// the remote incn
		// b) pe_priv_incn is same as in repy_msg => the other side's
		// INIT request reached us earlier and we already set our
		// variable
		// c) pe_priv_incn is different from repl_msg => the other
		// side's INIT request completed but it rebooted before we
		// completed our side of the initate. It will retry.
		// Continue as in case a)
		//
		if (pe_priv_incn != reply_msg->header.src.incn) {
			pe_priv_incn = reply_msg->header.src.incn;
		}
		incn = pe_priv_incn;
#ifdef DEBUG
		pm_msg->header.dest.incn = incn;
#endif
		ASSERT(pe_node.ndid == reply_msg->header.src.ndid);
		ASSERT(ID_node::match(reply_msg->header.dest,
		    orb_conf::current_id_node()));

		lock();
		sendbuf = pm_send_buf;
		recvbuf = pm_recv_buf;
		// Zero out the recvbuf so PM does not see any stale messages
		bzero(recvbuf, (size_t)PM_MESSAGE_SIZE);
		UNODE_DBG(("Done pathend::initiate to node %u:%u on adp_id "
		    "%u state %d\n", pe_node.ndid, pe_priv_incn,
		    local_adapter_id(), get_state()));
		return;
	}
	UNODE_DBG(("Aborting pathend::initiate to node %u "
	    "on adp_id %u state %d\n", pe_node.ndid, local_adapter_id(),
	    get_state()));
}

void
unode_pathend::reinit()
{
	// This is called before the pathend recycles to P_CONSTRUCTED state
	// before calling initiate, set the internal state back to as it
	// was after the constructor ran
	if (dp >= 0) {
		(void) close(dp);
		dp = -1;
	}
	pe_priv_incn = INCN_UNKNOWN;
#ifdef DEBUG
	pm_msg->header.dest.incn = INCN_UNKNOWN;
#endif
}

void
unode_pathend::cleanup()
{
	// Nothing to do here. All cleanup happens on reinit, if any
}

endpoint *
unode_pathend::new_endpoint(transport *tr)
{
	return (new unode_endpoint((unode_transport *)tr, this));
}

void
unode_pathend::handle_pm_message(unode_pm_msg *msg)
{
	// Verify that we got a pm message from an incarnation that matches
	// If we receive PM messages before the local pathend initiate
	// hasnt yet completed but the remote side has, we drop them
	// However, we should not get a PM message from an older/newer incn
	if (msg->header.src.incn != pe_priv_incn) {
		ASSERT(pe_priv_incn == INCN_UNKNOWN);
		return;
	}

	atomic_copy_64((uint64_t *)pm_recv_buf, (uint64_t *)msg->buf);
#ifdef	USE_TICKSTATS
	recv_tick_stat.update();
#endif	// USE_TICKSTATS

}

void
unode_pathend::handle_init_message(unode_init_msg *msg,
    unode_init_msg *reply)
{
	// Accept the init message if either we havent completed our
	// init message yet or if the incarnation numbers match
	// Lock/unlock protect against multiple init messages (if any)
	lock();
	UNODE_DBG(
		("Node %u: handle init from %u:%u, local view %u:%u\n",
		orb_conf::local_nodeid(),
		msg->header.src.ndid, msg->header.src.incn,
		pe_node.ndid, pe_priv_incn));
	ASSERT(pe_node.ndid == msg->header.src.ndid);

	if (pe_priv_incn == INCN_UNKNOWN) {
		pe_priv_incn = msg->header.src.incn;
	}
	if (pe_priv_incn == msg->header.src.incn) {
		reply->header.msgtype = UNODE_INITACCEPT_MSGTYPE;
		unlock();
	} else {
		unlock();
		UNODE_DBG(("Calling pathend fail for node %u "
			"adapter %u incn mismatch\n", pe_node.ndid,
			local_adapter_id()));
		fail();
	}
}

//
// unode_endpoint methods
//

unode_endpoint::unode_endpoint(unode_transport *tr, unode_pathend *up) :
	dp(up->ddesc()),
	endpoint(tr, up)
{
}

unode_endpoint::~unode_endpoint()
{
	desc_lock.wrlock();		// destruct with lock held
	ASSERT((get_state() == E_DELETED) || (get_state() == E_CONSTRUCTED));
}

void
unode_endpoint::initiate()
{
	// Nothing more to initialize. Already done in constructor
}

// This routine is called in clock interrupt
// It is called with endpoint lock held and is important not to release the lock
// Use to wake up any blocked threads waiting for syncronous send acks to
// return exception
void
unode_endpoint::unblock()
{
	ASSERT(lock_held());
	// broadcast to wake up all waiting threads
	path_down_notify.signal();
}

void
unode_endpoint::cleanup()
{
	UNODE_DBG(("unode_endpoint::cleanup for node %u %u\n",
	    ep_node.ndid, ep_node.incn));
	ASSERT(lock_held());
	unlock();
	// Use the write lock to wait for all endpoints to get out of door_call
	desc_lock.wrlock();
	dp = -1;		// Stop using the door descriptor
	desc_lock.unlock();
	lock();

	// release hold on pathend as the endpoint now does not use any
	// more resources from the pathend.
	release_pathend();
}

// Push up all acked synchronous messages, if any
void
unode_endpoint::push()
{
	// Nothing to do, unode uses doors and does not do any early acks
}

void
unode_endpoint::send(Buf *b, bool syncflag, Environment *e)
{
	int result;
	door_arg_t darg;

#if defined(FAULT_TRANSPORT)
	fault_pathend_fail();			// try fail a pathend
#endif

retry:
	darg.data_ptr = (char *)b->head();
	darg.data_size = b->span();
	darg.desc_num = 0;
	darg.desc_ptr = NULL;
	darg.rbuf = NULL;
	darg.rsize = 0;

	// Hold read lock while accessing descriptor
	desc_lock.rdlock();
	// Checking state without holding endpoint lock. This is a hint anyway
	if ((get_state() != E_REGISTERED) || (dp < 0)) {
		e->system_exception(
			CORBA::RETRY_NEEDED(EBADF, CORBA::COMPLETED_NO));
		desc_lock.unlock();
		return;
	}

	result = door_call(dp, &darg);
	desc_lock.unlock();
	if (result != 0) {
		// Cache value of errno
		result = errno;
		if (result == EAGAIN) {
			// In case server is out of resources try again
			UNODE_DBG(
			    ("Could not send from %u:%u to %u:%u\n",
				orb_conf::local_nodeid(),
				orb_conf::local_incarnation(),
				ep_node.ndid, ep_node.incn));
			os::usecsleep(UNODE_RETRY_EAGAIN); // 1 sec
			goto retry;
		}

		// Any other error than EBADF implies message was malformed or
		// there was an error on the server side but the server
		// node is not dead.
		// EINTR implies this thread got a signal. A fix is to
		// have a different thread pick up all signals. For now
		// we return RETRY_NEEDED and let orb retry handle it
#ifdef UNODE_DBGBUF
		if ((get_state() == E_REGISTERED) && (result != EBADF)) {
			UNODE_DBG(
			    ("Unode transport: Dropping "
			    "message to %u:%u due to error %d\n",
			    ep_node.ndid, ep_node.incn, result));
		}
#endif

		// Wait for path_down notification in case door got EBADF
		// This avoids thrashing due to immediate retry by the ORB
		// For other transient errors (EINTR) should not wait
		if (syncflag && (result == EBADF)) {
			path_down_notify.wait();
			ASSERT(get_state() != E_REGISTERED);
		}

		// We discriminate on result to see whether it
		// was COMPLETED_NO or COMPLETED_MAYBE.
		CORBA::CompletionStatus completed;
		if ((result == EBADF) || (result == EINVAL)) {
			completed = CORBA::COMPLETED_NO;
		} else {
			completed = CORBA::COMPLETED_MAYBE;
		}

		// XX We may end up returning exception before
		// cleanup has been called for non-sync messages
		e->system_exception(CORBA::RETRY_NEEDED(
			    (uint_t)result, completed));
	}
}

void
unode_endpoint::add_resources(resources *resourcep)
{
	//
	// Reserve space for transport level msg header(s).
	//
	resourcep->add_send_header(sizeof (unode_orb_msg_header_t));
}

sendstream *
unode_endpoint::get_sendstream(resources *resourcep)
{
	return (new unode_sendstream(this, resourcep));
}


//
// unode_adapter methods
//

// static
void
unode_adapter::trans_door(void *arg, char *data, size_t dsize,
    door_desc_t *, uint_t)
{
	unode_adapter *up = (unode_adapter *)arg;

	// The server side of the transport is meant to simulate the other
	// transports, which hand their data up in the interrupt context,
	// so we set non_blocking status on the thread, and clear it before
	// calling door_return.
	os::envchk::set_nonblocking(os::envchk::NB_INTR);

	if (data == NULL) {
		// This is a dummy call made to force the bound door threads
		// to unbind and cause an unref to be delivered
		if (door_unbind() != 0) {
		    perror("door_unbind");
		}
		// Exit the thread
		thr_exit(NULL);
		/* NOTREACHED */
		ASSERT(0);
	}

#if 0
	// We do not user DOOR_UNREF yet. When we do, we should enable this
	// If received unref, then signal the remove_adapter code
	if (data == DOOR_UNREF_DATA) {
		up->unref_signal();
		os::envchk::clear_nonblocking(os::envchk::NB_INTR);
		(void) door_return(NULL, (size_t)0, NULL, 0);
		/* NOTREACHED */
		ASSERT(0);
	}
#endif

	unode_message_header_t *hdrp = (unode_message_header_t *)data;
//
//	UNODE_DBG(("Node %u: Received message %u from %u\n",
//		orb_conf::local_nodeid(), hdrp->msgtype, hdrp->src.ndid));
//
	switch (hdrp->msgtype) {
	case UNODE_PM_MSGTYPE :
		up->handle_pm_message((unode_pm_msg *)data);
		/* NOTREACHED */
		ASSERT(0);
		return;
	case UNODE_INIT_MSGTYPE :
		up->handle_init_message((unode_init_msg *)data);
		/* NOTREACHED */
		ASSERT(0);
		return;
	case UNODE_INITACCEPT_MSGTYPE :
		ASSERT(0);		// Cannot get these
		return;
	case UNODE_INITREJECT_MSGTYPE :
		ASSERT(0);		// Cannot get these
		return;
	default:
		// fall through for orb message
		// XXX Check against N_MSGTYPES
		break;
	}

	Buf *rb = new GWBuf((uint_t)dsize);
	// copy from data to heap
	// lint think "data" could be NULL - we checked it above
	bcopy(data, (char *)rb->head(), dsize);	//lint !e668
	ASSERT(dsize <= INT_MAX);
	rb->incspan((int)dsize);

	unode_orb_msg_header_t *mh = (unode_orb_msg_header_t *)rb->head();
//
//	UNODE_DBG(("Received Message from node %u(%u):M%u "
//	    "length %u\n", mh->header.src.ndid, mh->header.src.incn,
//	    mh->header.msgtype, dsize));
//

	// Need to guarantee that messages arrive only for intended incarnation
	ASSERT(ID_node::match(mh->header.dest, orb_conf::current_id_node()));

	// If needed we can put in a filter to verify that only messages
	// on valid pathends are delivered

	unode_recstream *dr;
	dr = new unode_recstream(rb, mh->header.src,
		(orb_msgtype)mh->header.msgtype);
	if (dr == NULL) {
		// The door transport does not handle memory failure failure
		// XXX can we use this as flow control and have caller retry?
		// Or can we return EAGAIN?
		os::panic("unode_transport out of memory to store message\n");
	} else {
		orb_msg::the().deliver_message(dr, os::NO_SLEEP,
		    mh->header.seq);
	}
	os::envchk::clear_nonblocking(os::envchk::NB_INTR);
	(void) door_return(NULL, (size_t)0, NULL, 0);
	/*NOTREACHED*/
	ASSERT(0);
}

void
unode_adapter::unode_door_thread_create(door_info_t *dip)
{
	if (dip == NULL) {
		// This is a request for a thread for the general pool
		(void) thr_create(NULL, (size_t)0,
		    unode_adapter::unode_trans_thread,
		    NULL, (long)(THR_BOUND | THR_DETACHED), NULL);
		return;
	}

	// Lint complains about the cast because di_data is a 64-bit quantity
	// This is safe because the same 32-bit quantity was passed to
	// door_create, which is returned here
	unode_adapter *up = (unode_adapter *)dip->di_data; //lint !e511

	// We do this without locks as it is OK
	if (up->door_thread_count >= MAX_UNODE_DOOR_THREADS) {
		return;
	}
	os::atomic_add_32(&(up->door_thread_count), 1);
	if (thr_create(NULL, (size_t)0, unode_adapter::unode_trans_thread,
	    (void *)up, (long)(THR_BOUND | THR_DETACHED), NULL)) {
		os::atomic_add_32(&(up->door_thread_count), -1);
	}
}

void
unode_adapter::handle_pm_message(unode_pm_msg *msg)
{
	pathend *pe;

	// Get the pathend object based of the remote nodeis and the
	// local (second arg = true) adapter id.
	pe = transp->get_pathend(get_adapter_id(), true, msg->header.src.ndid);
	if (pe != NULL) {
		((unode_pathend *)pe)->handle_pm_message(msg);
		pe->get_pathend_rele();	// Corresponding to get_pathend
	}
	(void) door_return(NULL, (size_t)0, NULL, 0);
	/*NOTREACHED*/
	ASSERT(0);
}

void
unode_adapter::handle_init_message(unode_init_msg *msg)
{
	nodeid_t	ndid;
	pathend		*pe;
	unode_pathend	*upe;
	unode_init_msg	*reply;
	char		*vbuf;
	size_t		vlen;

	ndid = msg->header.src.ndid;
	ASSERT(ndid <= NODEID_MAX);
	if (init_reply_bufs[ndid] == NULL) {
		init_reply_bufs[ndid] = (char *)malloc(sizeof (unode_init_msg) +
		    vm_comm::max_btstrp_np_string());
	}
	reply = (unode_init_msg *)init_reply_bufs[ndid];

	// Initialize reply to REJECT. pathend::handle_init_message will
	// set it appropriately on success.
	reply->header.msgtype = UNODE_INITREJECT_MSGTYPE;
	reply->header.src = orb_conf::current_id_node();
#ifdef DEBUG
	reply->header.dest = msg->header.src;
#endif

	// Stuff local version information in the reply
	vbuf = ((char *)reply) + sizeof (unode_init_msg);
	vlen = vm_comm::max_btstrp_np_string();
	if (vm_comm::the().get_btstrp_np_string(vbuf, vlen) == NULL) {
		ASSERT(!"Version buffer overflow");
	}
	ASSERT(vlen != 0);
	reply->version_len = vlen;
	reply->version_compatible = 1;

	if (!path_manager::the().update_node_incarnation(msg->header.src.ndid,
	    msg->header.src.incn)) {
		//
		// This incarnation has become stale. Must boot with
		// a newer incarnation to be allowed in.
		//
		UNODE_DBG(("Node %u(adapter %u): Stale incarnation"
		    " from %u:%u\n", orb_conf::local_nodeid(),
		    get_adapter_id(), msg->header.src.ndid,
		    msg->header.src.incn));

	} else if (!vm_comm::the().compatible_btstrp_np(msg->header.src.ndid,
	    msg->header.src.incn, msg->version_len,
	    ((char *)msg) + sizeof (unode_init_msg),
	    msg->version_compatible ? false : true)) {
		//
		// Incompatible version
		//
		reply->version_compatible = 0;
		UNODE_DBG(("Node %u:%u has incompatible version\n",
		    msg->header.src.ndid, msg->header.src.incn));

	} else if ((pe = transp->get_pathend(get_adapter_id(), true,
	    msg->header.src.ndid)) == NULL) {
		UNODE_DBG(("Node %u(adapter %u): no pathend in handle init"
		    " from %u:%u\n", orb_conf::local_nodeid(),
		    get_adapter_id(), msg->header.src.ndid,
		    msg->header.src.incn));
	} else {
		upe = (unode_pathend *)pe;
		upe->handle_init_message(msg, reply);
		pe->get_pathend_rele();	// Corresponding to get_pathend
	}

	(void) door_return((char *)reply, sizeof (unode_init_msg) + vlen,
	    NULL, 0);
	/*NOTREACHED*/
	ASSERT(!"Returned from door_return");
}

unode_adapter::unode_adapter(unode_transport *tr, clconf_adapter_t *ap) :
	ddesc(-1),
	door_thread_count(0),
	adapter(tr, ap)
{
	for (int i = 0; i <= NODEID_MAX; i++) {
		init_reply_bufs[i] = NULL;
	}

	dname = os::strdup(clconf_obj_get_property((clconf_obj_t *)ap,
		"door_name"));
	ASSERT(dname != NULL);

	UNODE_DBG(("add_adapter for node %u %s\n",
	    orb_conf::local_nodeid(), dname));

	// Remove a previous copy of the door file, if any
	(void) unlink(dname);

	// Use a private pool of server threads - to cap them
	ddesc = door_create(unode_adapter::trans_door, this, DOOR_PRIVATE);
	if (ddesc < 0) {
		os::printf("unode_transport door server failed to initialize.");
		return;
	}

	// Create the file to attach to
	int tfd = open(dname, O_CREAT|O_EXCL, 0666);
	if (tfd == -1) {
		os::warning("unode_transport: door %s already exists.", dname);
	} else {
		if (close(tfd) != 0) {
			os::warning("unode_transport: could not close fd just "
			    "created for %s errno %d\n", dname, errno);
		}
	}

	if (fattach(ddesc, dname) < 0) {
		os::printf("unode_transport couldn't attach door to %s", dname);
		// Not much else we can do if close fails
		(void) close(ddesc);
		ddesc = -1;
		if (unlink(dname) != 0) {
			os::warning("Unable to unlink %s, errno %d\n",
			    dname, errno);
		}
	}
}

unode_adapter::~unode_adapter()
{
	UNODE_DBG(("remove_adapter for node %u %s\n",
	    orb_conf::local_nodeid(), dname));

	for (int i = 0; i <= NODEID_MAX; i++) {
		if (init_reply_bufs[i] != NULL) {
			free(init_reply_bufs[i]);
		}
	}

	if (ddesc >= 0) {

		door_arg_t	darg;

		darg.data_ptr = NULL;
		darg.data_size = 0;
		darg.desc_num = 0;
		darg.rsize = 0;
		darg.desc_ptr = NULL;
		darg.rbuf = NULL;

		// Make dummy calls to the door to force all
		// bound threads to unbind
		for (int i = 0; i < MAX_UNODE_DOOR_THREADS; i++) {
			(void) door_call(ddesc, &darg);
		}

		// Revoke the door to prevent any more new invocations
		if (door_revoke(ddesc) != 0) {
			os::warning("unode_adapter unable to revoke"
			    " door %s, errno %d\n", dname, errno);
		}

		ddesc = -1;

#if	0
		// XXX The unref is not getting delivered.
		// For now we do not mark the door as of type DOOR_UNREF
		// Need to fix this
		// Wait for unreferenced on the door for 2 seconds
		os::systime to((os::usec_t)2000000);
		if (unref_notify.timedwait(to) == os::notify_t::WAITING) {
			os::warning("Did not get unref on adapter door %s\n",
				dname);
		}
#endif

		// fdetach and unlink from the file system name space
		if (fdetach(dname) != 0) {
			os::warning("unode_adapter::shutdown could not fdetach"
			    " %s, errno %d\n", dname, errno);
		}
		if (unlink(dname) != 0) {
			os::warning("unode_adapter unable to unlink"
			    " %s, errno %d\n", dname, errno);
		}
	}
	delete [] dname;
}

void *
unode_adapter::unode_trans_thread(void *arg)
{
	if (arg != NULL) {
		// This is a transport thread (not a control door thread)
		unode_adapter *up = (unode_adapter *)arg;
		while (up->ddesc < 0)
			yield();
		if (door_bind(up->ddesc) < 0) {
			perror("door_bind");
			thr_exit(NULL);
		}
	}
	if (pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL) != 0) {
		os::warning("unode_transport error in pthread_setcancel_state");
	}
	(void) door_return(NULL, (size_t)0, NULL, 0);
	/*NOTREACHED*/
	ASSERT(0);
	return (NULL);
}

//
// unode_transport methods
//

// Function : unode_transport(constructor)
// (the true defers sends in a separate thread)
//
unode_transport::unode_transport() :
	transport("unode_transport", true)
{
	// Handle transport door thread creation to limit number of threads
	// This is analogous to an interrupt thread in a real transport and
	// we allow 2 per adapter
	(void) door_server_create(unode_adapter::unode_door_thread_create);
}


// Function : ~transport (destructor)
//
unode_transport::~unode_transport()
{
}

pathend *
unode_transport::new_pathend(clconf_path_t *p)
{
	return (new unode_pathend(this, p));
}

adapter *
unode_transport::new_adapter(clconf_adapter_t *ap)
{
	return (new unode_adapter(this, ap));
}

//
// unode_sendstream methods
//

unode_sendstream::unode_sendstream(unode_endpoint *ep, resources *resourceptr) :
	sendstream(resourceptr, &MainBuf),
	MainBuf(resourceptr->get_env(), resourceptr->send_buffer_size()),
	endp(ep)
{
	endp->hold();

	uint_t hdr_size = MainBuf.alloc_chunk(resourceptr->send_header_size());
	ASSERT(hdr_size == resourceptr->send_header_size() ||
	    CORBA::WOULDBLOCK::_exnarrow(get_env()->exception()));
}

unode_sendstream::~unode_sendstream()
{
	MainBuf.dispose();
	endp->rele();
	endp = NULL;
}

ID_node &
unode_sendstream::get_dest_node()
{
	return (endp->rnode());
}

// send
// sendstream comes in with a hold on the endpoint. It is not released here to
// allow callers to cache the sendstream. Only the buffers may be released
// Callers should not do the release. Either the destructor for sendstream will.
void
unode_sendstream::send(bool need_seq, orb_seq_t &seq)
{
	Environment	*e = get_env();
	ASSERT(!e->exception());
	ASSERT(xdoorcount || XdoorTab.empty());
	uint_t mlen, xlen;

	ASSERT(seq == INVALID_ORBSEQ);

	{
		unode_orb_msg_header_t *hdrp = (unode_orb_msg_header_t *)
		    make_header(sizeof (unode_orb_msg_header_t));
		mlen = hdrp->mlen = MainBuf.span();
		xlen = hdrp->xlen = XdoorTab.span();
		hdrp->xdnum = xdoorcount;
		hdrp->offline_data = OffLineBufs.empty() ? 0 : 1;
		hdrp->header.msgtype = (uint_t)get_msgtype();
		hdrp->header.src = orb_conf::current_id_node();
		if (need_seq &&
		    !message_mgr::the().send_prepare(get_dest_node(),
		    seq)) {
			e->system_exception(CORBA::COMM_FAILURE(0,
			    CORBA::COMPLETED_NO));
			return;
		}
		hdrp->header.seq = seq;

#ifdef DEBUG
		hdrp->header.dest = get_dest_node();
#endif

		ASSERT(hdrp->mlen >= (uint_t)sizeof (unode_orb_msg_header_t));

//		UNODE_DBG(
//		    ("Sending msg: MainBuf(%u) XdoorTab(%u) (%u, %u)\n",
//		    hdrp->mlen, hdrp->xlen, hdrp->xdnum, hdrp->offline_data));

		// NOTE: Cannot use hdrp after this point
	}

	// Start forming the message region
	// Concatenate the various MarshalStreams

	// Re-integrate stats
	MainBuf.useregion(XdoorTab.region(), MarshalStream::CURSOR_ATEND);
	MainBuf.useregion(OffLineBufs, MarshalStream::CURSOR_ATEND);
	Buf *sb = MainBuf.coalesce_region().reap();

	UPDATE_UNODE_STATS(msg_sent, 1);

	//
	// In the world of multiple adaptors all messages except
	// nonblocking messages are treated as synchronous.
	//
	endp->send(sb, !e->is_nonblocking(), e);

	// In there is an XdoorTab we recreate it. The callers expects to keep
	// XdoorTab unmodifed for error recovery
	if (xdoorcount > 0) {
		// Delete the MainBuf part from sb
		ASSERT(mlen <= INT_MAX);
		sb->advance((int)mlen);
		// Delete the OffLineBufs part from sb
		sb->incspan((int)(sb->span() - xlen));
		// Restore the XdoorTab
		XdoorTab.usebuf(sb);
		ASSERT(XdoorTab.span() == xlen);
	} else {
		sb->done();
	}
}

//
// unode_recstream methods
//

unode_recstream::~unode_recstream()
{
	if (_buf != NULL) {
		_buf->done();
		_buf = NULL;
	}
}

ID_node &
unode_recstream::get_src_node()
{
	return (srcnode);
}

// Initialize recstream
bool
unode_recstream::initialize(os::mem_alloc_type)
{
	// splits up Buffer into MainBuf, XdoorTab, OfflineBufs

	unode_orb_msg_header_t mh;
	MainBuf.usebuf(_buf);
	MainBuf.read_header((void *)&mh, sizeof (mh));
	// Account for trimmed header
	ASSERT(mh.mlen >= (uint_t)sizeof (unode_orb_msg_header_t));
	mh.mlen -= (uint_t)sizeof (unode_orb_msg_header_t);

	xdoorcount = mh.xdnum;
	// XX This code assumes that _buf is still the first Buf in
	// MainBuf and accesses _buf directly. This is unclean and
	// error prone and works by accident
	if (xdoorcount > 0) {
		XdoorTab.usebuf(new nil_Buf(_buf->head() + mh.mlen,
						mh.xlen, mh.xlen));
	}
	if (mh.offline_data) {
		size_t offstart = mh.mlen + mh.xlen;
		ASSERT((uint_t)offstart <= _buf->span());
		uint_t offlen = _buf->span() - (uint_t)offstart;
		OffLineBufs.prepend(new nil_Buf(_buf->head() + offstart,
							offlen, offlen));
	}

	// MainBuf still has the rest of xdoor/offline data in it which we
	// not trim from the end. However, this is not a problem as the ORB
	// only reads whatever is part of MainBuf and does not read over the
	// limit.
	// We are also depending here on the Orb deleting/freeing XdoorTab
	// and OffLineBuf before MainBuf as XdoorTab and OffLineBufs are
	// pointing at data which is freed along with MainBuf

	_buf = NULL;
	return (true);
}

// Declare this extern C so we can dlopen with the unmangled name
extern "C" {

int
unode_init(void)
{
	// Just create a unode_transport structure and add it to PM
	path_manager::the().add_client(path_manager::PM_TRANSPORT,
	    new unode_transport());
	return (0);
}

}
