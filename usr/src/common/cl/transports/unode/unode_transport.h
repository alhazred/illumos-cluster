/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _UNODE_TRANSPORT_H
#define	_UNODE_TRANSPORT_H

#pragma ident	"@(#)unode_transport.h	1.64	08/05/20 SMI"

#include <orb/buffers/marshalstream.h>
#include <orb/transport/transport.h>
#include <sys/os.h>
#include <sys/file.h>
#include <sys/stream.h>
#include <sys/dbg_printf.h>
#include <door.h>

// TBD Need to add unode_debug comments all over
#if defined(DBGBUFS) && !defined(NO_UNODE_DBGBUF)
#define	UNODE_DBGBUF
#endif

#ifdef UNODE_DBGBUF
extern dbg_print_buf unode_debug;
#define	UNODE_DBG(a)	unode_debug.dbprintf a
#else
#define	UNODE_DBG(a)
#endif

// forware declarations...
class unode_transport;

#ifdef DEBUG
// TBD Need to decide what stats to implement and where they need to reside
class unode_stats_t {
public :
	uint_t msg_sent;
	uint_t xdoortab_msg;
};
extern unode_stats_t unode_stats;
#define	UPDATE_UNODE_STATS(f, v)	unode_stats.f += (v)
#else
#define	UPDATE_UNODE_STATS(f, v)
#endif

//
// Transport header for unode_sendstreams
//

// unode_orb_msg_header_t uses orb_msgtype for msgtype
// But other messages use very large numbers for private messages
static const uint_t UNODE_PM_MSGTYPE = 0xf0000001;
static const uint_t UNODE_INIT_MSGTYPE = 0xf0000002;
static const uint_t UNODE_INITACCEPT_MSGTYPE = 0xf0000003;
static const uint_t UNODE_INITREJECT_MSGTYPE = 0xf0000004;

typedef struct {
#ifdef MARSHAL_DEBUG
	// Make the first four fields similar for all unode messages
	uint32_t	marshal_debug;		// constant for validity check
						// must be the first data member
#endif
	uint_t	msgtype;   // the message type
	ID_node	src;	// Source ID_node
	orb_seq_t seq;	// the sequence number
#ifdef DEBUG
	ID_node	dest;	// Destination ID_node for debug purposes
#endif
} unode_message_header_t;

typedef struct {
	unode_message_header_t header;
	uint_t  mlen;		// length of main marshal buffer
	uint_t  xlen;		// length of the xdoor descriptions.
	uint_t  xdnum : 31;	// number of xdoors transported
	uint_t  offline_data : 1; // flag if off-line buffers transported.
} unode_orb_msg_header_t;

typedef struct {
	unode_message_header_t header;
	char	pad[64 - sizeof (unode_message_header_t)];
	// Pad to 64 bytes so buf will have proper alignment
	char	buf[PM_MESSAGE_SIZE];
} unode_pm_msg;

typedef struct {
	unode_message_header_t header;
	uint32_t	version_compatible;
	uint32_t	version_len;
	// This is followed by the actual version string
} unode_init_msg;

class unode_pathend : public pathend {
	friend class unode_endpoint;
public:
	unode_pathend(unode_transport *, clconf_path_t *);

	~unode_pathend();

	// Called in clock interrupt
	void pm_send_internal();

	// Return door descriptor (used by endpoint constructor)
	// This can only be called after pathend initiate completes
	// pathend does not change the descriptor till after all endpoints
	// have dropped references.
	int	ddesc() const;

	// Functions required to be implemented as part of pathend state machine
	// Called on add_path/path_cleanup to setup initial connection
	void	initiate(char *&send_buf, char *&rec_buf,
			    incarnation_num &incn);

	// Called on path_down after endpoints have been unregistered
	// But all endpoints may not have divorced from the path_end yet
	void	cleanup();

	// Called on path_down just before going back to P_CONSTRUCTED state
	// and calling initiate()
	void	reinit();

	// Function to create a new endpoint
	endpoint *new_endpoint(transport *);

	// Handle incoming path_manager message
	void handle_pm_message(unode_pm_msg *);

	// Handle incoming init message
	void handle_init_message(unode_init_msg *, unode_init_msg *);

private:
	// Copy of remote incn for init protocol and endpoint access
	incarnation_num	pe_priv_incn;

	// descriptor used to talk to distant node
	// We do not have a lock for this as the unode_pathend code
	// is single threaded by the generic pathend.
	int		dp;
	// Name of remote door, copied from clconf_path_t
	char		*dname;

	// Buffer for initial handshake
	char		*init_msg_buf;
	size_t		init_msg_buf_size;

	// For path manager send/receive
	// Allocated with new (to get 64 bit alignment)
	char		*pm_send_buf;
	char		*pm_recv_buf;

	// One copy always left initialized here.
	// Dynamically allocate to guarantee alignment
	unode_pm_msg	*pm_msg;

	// Disallow assignments and pass by value
	unode_pathend(const unode_pathend&);
	unode_pathend& operator = (unode_pathend&);
};

class unode_endpoint : public endpoint {
public:
	unode_endpoint(unode_transport *, unode_pathend *);

	~unode_endpoint();

	void	add_resources(resources *resourcep);
	sendstream	*get_sendstream(resources *resourcep);

	void send(Buf *, bool, Environment *);

	// Functions to implement as part of endpoint state machine
	void	unblock();
	void	initiate();
	void	push();
	void	cleanup();

	// accessor to generic endpoint's remote nodeid/incn info
	// used by unode_sendstream
	ID_node&	rnode();

private:
	// descriptor used to talk to distant node
	// It is a private copy of the pathend file descriptor
	int			dp;

	// Used to wait on path_down so that message send exceptions are

	// sent only after the path_down on a pathend has been received
	os::notify_t		path_down_notify;

	// rw lock for access to door descriptor
	os::rwlock_t		desc_lock;

	// Disallow assignments and pass by value
	unode_endpoint(const unode_endpoint&);
	unode_endpoint& operator = (unode_endpoint&);
};

//
// For unode sendstreams;
//
class unode_sendstream : public sendstream {

public:
	ID_node& get_dest_node();

	void send(bool, orb_seq_t &);

	unode_sendstream(unode_endpoint *, resources *);

protected:
	~unode_sendstream();
private:

	MarshalStream 		MainBuf;
	unode_endpoint		*endp;

	// Disallow assignments and pass by value
	unode_sendstream(const unode_sendstream&);
	unode_sendstream& operator = (unode_sendstream&);
};

//
// For unode recstreams;
//
class unode_recstream : public recstream {
public:
	ID_node		&get_src_node();

	bool		initialize(os::mem_alloc_type);

	unode_recstream(Buf *, ID_node &, orb_msgtype);

protected:
	~unode_recstream();

private:
	ID_node		srcnode;
	Buf		*_buf;

	// Disallow assignments and pass by value
	unode_recstream(const unode_recstream &);
	unode_recstream &operator = (unode_recstream &);
};

// unode_adapter
class unode_adapter : public adapter {
public:
	unode_adapter(unode_transport *, clconf_adapter_t *);

	~unode_adapter();

	static	void trans_door(void *, char *, size_t, door_desc_t *, uint_t);
	static	void unode_door_thread_create(door_info_t *);
	static	void *unode_trans_thread(void *);

	void	unref_signal();

	void handle_pm_message(unode_pm_msg *);
	void handle_init_message(unode_init_msg *);

	enum	{ MAX_UNODE_DOOR_THREADS = 2 };
private:
	int		ddesc;	// The door descriptor for this adapter
	uint_t		door_thread_count;
	os::notify_t	unref_notify;

	// Name of local door, copied from clconf_adapter_t
	char		*dname;

	// Buffers to prepare replies for intial handshake
	char		*init_reply_bufs[NODEID_MAX+1];

	// Disallow assignments and pass by value
	unode_adapter(const unode_adapter &);
	unode_adapter &operator = (unode_adapter &);
};

// unode_transport
class unode_transport : public transport {
public:
	unode_transport();
	~unode_transport();

private:
	pathend	*new_pathend(clconf_path_t *);
	adapter *new_adapter(clconf_adapter_t *);
	const char *transport_id();

	// Disallow assignments and pass by value
	unode_transport(const unode_transport&);
	unode_transport& operator = (unode_transport&);
};

#include <transports/unode/unode_transport_in.h>

#endif	/* _UNODE_TRANSPORT_H */
