/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  unode_transport_in.h
 *
 */

#ifndef _UNODE_TRANSPORT_IN_H
#define	_UNODE_TRANSPORT_IN_H

#pragma ident	"@(#)unode_transport_in.h	1.21	08/05/20 SMI"

inline int
unode_pathend::ddesc() const
{
	return (dp);
}

inline ID_node &
unode_endpoint::rnode()
{
	return (ep_node);
}

inline
unode_recstream::unode_recstream(Buf *rbuf, ID_node &src, orb_msgtype msgt) :
	recstream(msgt, invocation_mode::ONEWAY), // Default to oneway msg
	_buf(rbuf),
	srcnode(src)
{
}

inline const char *
unode_transport::transport_id()
{
	return ("doors");
}

inline void
unode_adapter::unref_signal()
{
	unref_notify.signal();
}

#endif	/* _UNODE_TRANSPORT_IN_H */
