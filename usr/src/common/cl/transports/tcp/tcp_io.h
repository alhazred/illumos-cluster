/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _TCP_IO_H
#define	_TCP_IO_H

#pragma ident	"@(#)tcp_io.h	1.16	08/05/20 SMI"


class tcp_endpoint;

//
// struct to send/receive reply io cookie during reply phase over the wire.
// cookie is the pointer to tcp_rio_client on the client and is what was
// received during the request
// sendstream_cookie is the pointer to the sendstream sending the reply
// This is used to do the ack protocol for replyios
// generation is the generation of the replyio used in put_offline_reply
// This is used to weed out old replyios in progress
//
struct rio_cookie {
	void	*cookie;
	void	*sendstream_cookie;
	uint_t	generation;
};

//
// struct used for tcpmod's internal state for replyio connections and
// also as argument to rio_callback
//
typedef struct tcpmod_replyio_info {
	struct rio_cookie	cookie;	// contents of initial rio_cookie recvd
	uint_t	size;		// total size of this replyio
	uint_t	offset;		// length transferred to tcp_transport so far
				// and what offset to receive next
	uint_t	connection;	// which connection arrived on
				// used to send ack back on same connection
} replyio_info_t;

class tcp_rio_client :
    public replyio_client_t, _DList::ListElem, public refcnt {
public:
	tcp_rio_client(struct buf *);

	virtual ~tcp_rio_client();

	void release();

	bool iodone(bool, uint_t);

	void rio_callback(mblk_t *, replyio_info_t *, tcp_endpoint *);

	void rio_do_copy(struct buf *, uint_t, uint_t, mblk_t *);

	static void tcp_rio_client_reaper(void *);

	void lock();
	void unlock();
	bool lock_held();
	os::systime duetime;
	bool is_deferred();
	void set_deferred();
private:
	uint_t		rio_len;
	uint_t		rio_len_copied;

	//
	// rio_copies_in_progress keeps track of the number of threads
	// that have been assigned rio data copy operations for this
	// rio. This counter serves two purposes:
	//
	// 1. Copy parallelization: multiple rio copies (from the same
	//    generation) are allowed to proceed in parallel. The thread
	//    doing the copy of the last segment waits (in iodone) until
	//    this counter drops to zero before proceeding with cleanup
	//    and sending an rio ack.
	//
	// 2. This counter is also used in preventing copies from multiple
	//    generations to run in parallel. A new generation that wishes
	//    to abort an older generation rio must wait (in iodone) until
	//    all copies from the older generation have finished. It then
	//    cleans up the rio client state information and sets it up
	//    for the new rio before relinquishing the lock.
	//
	// rio_cv is used to signal threads when interesting changes to
	// rio_copies_in_progress take place.
	//
	uint_t		rio_copies_in_progress;
	os::condvar_t	rio_cv;

	uint_t		conn;	// connection on which rio received
	tcp_endpoint	*tep;
	uint_t		reply_generation;
	void		*ack_sendstream_cookie;
	bool		processed_offset0;
	os::mutex_t	_lock;
	bool		deferred_processing;

	//
	// Disallow assignments and pass by value
	//
	tcp_rio_client(const tcp_rio_client &);
	tcp_rio_client &operator = (tcp_rio_client &);
};

//
// A task to serve tcp reply io optimization
//
class tcp_rio_task : public defer_task {

public:
	tcp_rio_task(mblk_t *, replyio_info_t *, tcp_endpoint *);

	void execute();

	~tcp_rio_task();

private:
	mblk_t		*mp;
	tcp_endpoint	*ep;
	replyio_info_t	rio;

	//
	// Disallow assignments and pass by value
	//
	tcp_rio_task(const tcp_rio_task &);
	tcp_rio_task &operator = (tcp_rio_task &);
};

#ifndef NOINLINES
#include <transports/tcp/tcp_io_in.h>
#endif  // _NOINLINES

#endif	/* _TCP_IO_H */
