/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)tcpmod.cc	1.70	08/05/20 SMI"

/*
 * This is the module pushed on top of each
 * tcp transport connection.
 */
#include <transports/tcp/tcp_transport.h>
#include <sys/tihdr.h>
#include <sys/modctl.h>
#include <orb/transport/hb_threadpool.h>

static int tcpmodopen(queue_t *q, dev_t *devp, int flag, int sflag,
			cred_t *crp);
static int tcpmodclose(queue_t *q, int flag, cred_t *crp);
static int tcpmodrput(queue_t *q, mblk_t *mp);
static int tcpmodwput(queue_t *q, mblk_t *mp);
static int tcpmodrsrv(queue_t *q);
static int tcpmodwsrv(queue_t *q);

#define	TCPMODID 	2223
#define	RETRY		1	/* timeout for retrying qbufcall */
#define	CALLBACK_RETRY	3	/* timeout for retrying failed callbacks */

// To enable some debug checks that are disabled due to their long running
// nature, uncomment the following line to define PARANOID
// #define	PARANOID

#ifdef PARANOID
#define	TCPMOD_STATE_DUMP(q, msg, mp, hp, do_assert)	\
	tcpmod_state_dump(q, msg, mp, hp, do_assert)
#else
#define	TCPMOD_STATE_DUMP(q, msg, mp, hp, do_assert)
#endif

static struct module_info tcpmod_rminfo = {
	TCPMODID, "cltcpint", 0, INFPSZ, 256 * 1024, 1
};

static struct module_info tcpmod_wminfo = {
	TCPMODID, "cltcpint", 0, INFPSZ, 256 * 1024, 1
};

static struct qinit tcpmodrinit = {
	(int (*)(void))tcpmodrput,	/* put procedure */
	(int (*)(void))tcpmodrsrv,	/* service procedure */
	(int (*)(void))tcpmodopen,	/* open procedure */
	(int (*)(void))tcpmodclose,	/* close procedure */
	NULL,			/* qadmin procedure */
	&tcpmod_rminfo,		/* module info structure */
	NULL,			/* module statistics structure */
	NULL,			/* r/w proc. */
	NULL,			/* info. procedure. */
	0			/* uio type for struio(). */
};

static struct qinit tcpmodwinit = {
	(int (*)(void))tcpmodwput,	/* put procedure */
	(int (*)(void))tcpmodwsrv,	/* service procedure */
	(int (*)(void))tcpmodopen,	/* open procedure */
	(int (*)(void))tcpmodclose,	/* close procedure */
	NULL,			/* qadmin procedure */
	&tcpmod_wminfo,		/* module info structure */
	NULL,			/* module statistics structure */
	NULL,			/* r/w proc. */
	NULL,			/* info. procedure. */
	0			/* uio type for struio(). */
};

struct streamtab tcpmodinfo = {
	&tcpmodrinit, &tcpmodwinit, NULL, NULL
};

static struct fmodsw fsw = {
	"cltcpint", &tcpmodinfo,
	(D_NEW | D_MP | D_MTPERQ | _D_QNEXTLESS)
};

/* inter-module dependencies */
char _depends_on[] = "misc/cl_runtime misc/cl_load misc/cl_orb "
	"misc/cl_haci misc/cl_quorum misc/cl_comm misc/cl_dlpitrans";

static struct modlstrmod modlstrmod = {
	&mod_strmodops, "protocol module for cluster tcp transport", &fsw
};

static struct modlinkage modlinkage = {
#ifdef _LP64
	MODREV_1, { (void *) &modlstrmod, NULL, NULL, NULL, NULL, NULL, NULL }
#else
	MODREV_1, { (void *) &modlstrmod, NULL, NULL, NULL }
#endif
};

//
// Module Private Structures
//
// struct tcpmod_private
//	conn_info has all the information about this connection.
//	It has the pointer to the object (pathend or endpoint)
//	and it also has the type (pathend or endpoint) so that
//	it can identify the object. It also has the connection
//	number.
//
//	_mp is the mblk pointer used to store the incoming datagrams.
//	The messages are stored in this field until the full datagram
//	is received.
//	_mp_tail points to the last mblk in the above mblk chain. We
//	keep track of this pointer separately so that we do not have to
//	traverse the entire mblk chain to add a new mblk to it,
//	particularly in the interrupt context.
//	_mp_size is the data size stored in the above mblk chain.
//	_mp_first_tail points to the last mblk that has been examined
//	so far as a possible candidate where the first message in the
//	mblk chain should end. Note that this mblk might or might not
//	have the end of the first message yet.
//	_mp_first_size is the size of the mblk chain that ends in
//	_mp_first_tail. Note that _mp_first_size can possibly be
//	larger than (or smaller than or equal to) the actual first
//	message size;
//	_mp_first_* is used to keep track of only non-rio messages as
//	rio messages are copied as received.hi without waiting for
//	the whole message to arrive.
//
//	bufcall() and timeout() are used to recover from memory
//	allocation errors.
//
//	rio_info for reply io optimization.
//
//	retry_tid:
//		We setup a timeout when the callback to tcp_transport
//		fails to accept a message - either due to endpoint not
//		in correct state or due to memory allocation failure.
//		This ensures that the queue is rescheduled after some time
//		to retry the delivery even if there is no other traffic on
//		the same connection. We schedule a timeout as doing
//		a qenable() after a putbq() will cause an infinite loop.
//
//
typedef struct tcpmod_private {
	struct tcpmod_conn_info conn_info;
	mblk_t *_mp;
	mblk_t *_mp_tail;
	size_t  _mp_size;
	mblk_t *_mp_first_tail;
	size_t  _mp_first_size;
	bufcall_id_t bufcall_id;
	timeout_id_t tid;
	timeout_id_t retry_tid;
	struct tcpmod_replyio_info rio_info;
} tcpmod_private_t;

static void tcp_reset(struct tcpmod_conn_info *, long, long);
static bool gather_header(mblk_t *, tcpmod_header_t *);
static mblk_t *fragment_msg(queue_t *, tcpmod_header_t *);
static int process_recv_msg(queue_t *, tcpmod_header_t *, mblk_t *, bool);
static void tcpmod_bufcall(void *);
static void tcpmod_timeout(void *);

static bool rio_gather_header(mblk_t *, tcpmod_rio_header_t *);
static int process_rio_other_msg(queue_t *q, tcpmod_header_t *);
static int tcpmod_rio_process_msg(queue_t *);
static void setup_bufcall(queue_t *, tcpmod_private_t *);

static void tcpmod_retry_timeout(void *);
static void set_retry_timeout(queue_t *, tcpmod_private_t *);

#include <cplplrt/cplplrt.h>

int
_init(void)
{
	int	error;

	if ((error = mod_install(&modlinkage)) != 0) {
		os::sc_syslog_msg tcp_syslog(SC_SYSLOG_TCP_TRANSPORT, "Tcp",
		    NULL);
		//
		// SCMSGS
		// @explanation
		// Streams module that intercepts private interconnect
		// communication could not be installed.
		// @user_action
		// Need a user action for this message.
		//
		(void) tcp_syslog.log(SC_SYSLOG_WARNING, MESSAGE,
		    "modinstall of tcpmod failed");
		return (error);
	}
	_cplpl_init();		/* C++ initialization */

	return (0);
}

/* when removing from kernel */
int
_fini(void)
{
	int	error;

	if ((error = mod_remove(&modlinkage)) != 0) {
		return (error);
	}
	_cplpl_fini();
	return (0);
}

int
_info(struct modinfo *modinfoPtr)
{
	return (mod_info(&modlinkage, modinfoPtr));
}

#ifdef PARANOID
static void
tcpmod_state_dump(queue_t	*q,
		char		*msg,
		mblk_t		*mp,
		tcpmod_header_t *hp,
		bool		do_assert)
{
	tcp_endpoint		*ep;
	tcp_pathend		*pe;
	tcpmod_private_t	*ip;
	uint_t			ndid;
	uint_t			adptid;
	uint_t			connection;
	char			*conn_tag;
	int			trace_option;

	ip = (tcpmod_private_t *)q->q_ptr;
	if (ip == NULL)
		return;
	if (ip->conn_info.type == TCP_ENDPOINT) {
		ep = (tcp_endpoint *) ip->conn_info.obj;
		if (ep == NULL)
			return;
		ndid = (uint_t)ep->get_rnode().ndid;
		adptid = (uint_t)ep->local_adapter_id();
		if (ip->conn_info.conn_type == TCP_REPLYIO_CONN)
			trace_option = TCPTR_TRACE_RIO_TCPMOD;
		else
			trace_option = TCPTR_TRACE_NRIO_TCPMOD;
		conn_tag = "EP";
	} else {
		pe = (tcp_pathend *) ip->conn_info.obj;
		trace_option = TCPTR_TRACE_PE_TCPMOD;
		if (pe == NULL)
			return;
		ndid = pe->node().ndid;
		adptid = (uint_t)pe->local_adapter_id();
		conn_tag = "PE";
	}
	connection = (uint_t)ip->conn_info.connection;

	ETCP_DBPRINTF(trace_option, ("%s%u:%u:%u SM: %s mp %p hp %p\n",
	    conn_tag, ndid, adptid, connection, msg, mp, hp));
	if (mp) {
		ETCP_DBPRINTF(trace_option, ("%s%u:%u:%u MP: size %d rptr %p"
		    " wptr %p.\n", conn_tag, ndid, adptid, connection,
		    MBLKL(mp), mp->b_rptr, mp->b_wptr));
	}
	if (hp) {
		ETCP_DBPRINTF(trace_option, ("%s%u:%u:%u HEADER: size %d"
		    " msgt %x cookie %p.\n", conn_tag, ndid, adptid,
		    connection, hp->size, hp->msgt, hp->notify_ptr));
	}
	ETCP_DBPRINTF(trace_option, ("%s%u:%u:%u MBLKCHAIN: mp %p tail %p"
	    " size %d ftail %p fsize %d.\n", conn_tag, ndid, adptid,
	    connection, ip->_mp, ip->_mp_tail, ip->_mp_size,
	    ip->_mp_first_tail, ip->_mp_first_size));
	if (ip->_mp) {
		ETCP_DBPRINTF(trace_option, ("%s%u:%u:%u FIRST MBLK: size %d"
		    " rptr %p wptr %p.\n", conn_tag, ndid, adptid, connection,
		    MBLKL(ip->_mp), ip->_mp->b_rptr, ip->_mp->b_wptr));
	}
	ETCP_DBPRINTF(trace_option, ("%s%u:%u:%u RIO: cookie %p ss %p gen %d"
	    " size %d off %d\n", conn_tag, ndid, adptid, connection,
	    ip->rio_info.cookie.cookie,
	    ip->rio_info.cookie.sendstream_cookie,
	    ip->rio_info.cookie.generation,
	    ip->rio_info.size, ip->rio_info.offset));
	if (do_assert) {
#ifdef PARANOID
		if (ip->_mp == NULL)
			ASSERT(ip->_mp_size == 0);
		else
			ASSERT(ip->_mp_size == msgdsize(ip->_mp));
#endif
	}
}
#endif // DEBUG


static inline bool
connect_error(mblk_t *mp, long *reasonp)
{
	union T_primitives *tpr;
	bool	err = true;

	tpr = (union T_primitives *)mp->b_rptr;
	switch (tpr->type) {
	case T_ERROR_ACK:
		// Report the primitive in error as the reason
		*reasonp = tpr->error_ack.PRIM_type;
		break;
	case T_DISCON_IND:
		// Report the disconnect reason
		*reasonp = tpr->discon_ind.DISCON_reason;
		break;
	case T_ORDREL_IND:
		*reasonp = 0;
		// No reason to report
		break;
	default:
		err = false;
		break;
	}
	return (err);
}

//
// void
// tcp_reset(struct tcpmod_conn_info *cip, int type, int reason)
//	Called whenever a tcp reset is received on a connection.
//
// Description:
// 	Called from tcpmod whenever an error on a connection occurs.
//	When an error occurs, T_error_ack is sent up.
//
//	This function is there mainly to catch the reset on endpoint
//	connections (if they happen because of a bug).
//
// Parameters:
//	cip : Pointer to information about the tcpmod tcp connection
//		that got reset.
//
void
tcp_reset(struct tcpmod_conn_info *cip, long type, long reason)
{
	if (cip->type == TCP_PATHEND) {
		TCP_DBPRINTF(("PE%u:%u:%u %p connection DROPPED, prim = %d"
		    " reason = %d.\n", ((tcp_pathend *) cip->obj)->node().ndid,
		    ((tcp_pathend *) cip->obj)->local_adapter_id(),
		    cip->connection, cip->obj, (int)type, (int)reason));
	} else {
		TCP_DBPRINTF(("EP%u:%u:%u %p connection DROPPED, prim = %d"
		    " reason = %d.\n",
		    ((tcp_endpoint *) cip->obj)->get_rnode().ndid,
		    ((tcp_endpoint *) cip->obj)->local_adapter_id(),
		    cip->connection, cip->obj, (int)type, (int)reason));

		// Endpoint connection was dropped, the path should be dropped
		// and reinitialized. Process_tcp_reset method does book
		// keeping and arranges for the path being dropped.

		((tcp_endpoint *) cip->obj)->process_tcp_reset();
	}
}


//
// static int
// tcpmodopen(queue_t *q, dev_t *, int, int, cred_t *)
//	open procedure for tcpmod module.
//
// Description:
//	Called when the module is pushed on to the connection.
//	It allocates a tcpmod_private structure.
//	It stores the pointer to the structure in the q_ptr field
//	of both the queues.
//	It enables the put and service procedures.
//
// Parameters:
//	queue_t	*q	- read side queue pointer.
//	Other parameters are unused?
//
// Returns:
//	0 on success and an error number on failure.
//
//
static int
tcpmodopen(queue_t *q, dev_t *, int, int, cred_t *)
{
	tcpmod_private_t	*infoptr;

	infoptr = (tcpmod_private_t *)kmem_zalloc(sizeof (tcpmod_private_t),
	    KM_NOSLEEP);
	if (infoptr == NULL) {
		os::sc_syslog_msg tcp_syslog(SC_SYSLOG_TCP_TRANSPORT, "Tcp",
		    NULL);
		//
		// SCMSGS
		// @explanation
		// Machine is out of memory.
		// @user_action
		// Need a user action for this message.
		//
		(void) tcp_syslog.log(SC_SYSLOG_WARNING, MESSAGE,
		    "tcpmodopen: Could not allocate private data");
		return (ENOMEM);
	}

	/* point queue pointer at private data */
	RD(q)->q_ptr = WR(q)->q_ptr = (char *)infoptr;

	qprocson(q);		/* enable put/srv routines */
	return (0);
}


//
// static int
// tcpmodclose(queue_t *q, int, cred_t *)
//	close procedure of the tcpmod module.
//
// Description:
//	This procedure is called when the connection (socket) is
//	closed. It disables the put and service procedure.
//	Frees all the messages, cancels timeouts and bufcalls,
//	and deallocates the private structure.
//
// Parameters:
//	queue_t	*q	- read side queue pointer
//	Other parametes unused?
//
//
static int
tcpmodclose(queue_t *q, int, cred_t *)
{
	tcpmod_private_t	*infoptr = (tcpmod_private_t *)q->q_ptr;
	mblk_t *mp;

	qprocsoff(q);		/* disable put/srv routines */

	/*
	 * XX if sending ack before receive_callback when handling checkpoint
	 * messages, then do not free pending rchain mblks, drain them
	 * calling receive_callback with KM_SLEEP flag
	 */

	/*
	 * free up any pending mblks in private data
	 */
	while ((mp = getq(RD(q))) != NULL) {
		freemsg(mp);
	}

	while ((mp = getq(WR(q))) != NULL) {
		freemsg(mp);
	}

	if (infoptr->_mp != NULL) {
		freemsg(infoptr->_mp);
	}

	if (infoptr->bufcall_id != 0) {
		qunbufcall(q, infoptr->bufcall_id);
	}

	if (infoptr->tid != 0) {
		(void) quntimeout(q, infoptr->tid);
	}

	if (infoptr->retry_tid != 0) {
		(void) quntimeout(q, infoptr->retry_tid);
	}

	/*
	 * free private data
	 */
	kmem_free(infoptr, sizeof (tcpmod_private_t));

	return (0);
}


//
//
// static int
// tcpmodrput(queue_t *q, mblk_t *mp)
//	Read Side Put Procedure.
//
// Description:
//	This is the read side put procedure of the tcpmod.
//	It processes all incoming messages. The datagrams
//	are collected and delivered to the higher layer.
//
//	The messages are delivered asynchronously and the
//	context switching at the streams head is avoided.
//
//	It processes M_PROTO (the M_DATA portion only) and
//	M_DATA messages. It passes all the other messages
//	to the streams head.
//
//	It simply places the message at the end of the
//	_mp in the private structure and enables the
//	read side service procedure. the service procedure
//	processes the data and sends the datagram up when
//	appropriate.
//
// Parameters:
//	queue_t *q	- pointer to read side queue
//	mblk_t	*mp	- pointer to message.
//
// See Also
//	tcpmodrsrv()
//
//
static int
tcpmodrput(queue_t *q, mblk_t *mp)
{
	tcpmod_private_t	*infoptr = (tcpmod_private_t *)q->q_ptr;
	mblk_t *tmp;
	mblk_t *mp_tail;
	long	t_type;
	long	t_reason;
	size_t	mp_size;
	size_t	mblk_size;

	switch (DB_TYPE(mp)) {

	case M_IOCTL:
		putnext(q, mp);
		break;

	case M_PROTO:
		t_type = ((union T_primitives *)mp->b_rptr)->type;
		if (t_type != T_DATA_IND) {
			/*
			 * Call tcp_reset in case of an error.
			 */
			if (connect_error(mp, &t_reason)) {
				if (infoptr->conn_info.obj) {
					/*
					 * make sure that tcp_reset() is
					 * executed in the context of this
					 * put procedure.
					 * put procedures are disabled
					 * before the endpoint goes away.
					 */
					tcp_reset(&infoptr->conn_info, t_type,
					    t_reason);
				}
			}
			putnext(q, mp);
			break;
		}
		tmp = mp->b_cont;
		freeb(mp);
		mp = tmp;
		ASSERT(DB_TYPE(mp) == M_DATA);
		//
		// fall through
		//
	case M_DATA: {
		//
		// We may receive empty mblks from tcp. We need to remove them.
		//
		while ((mp != NULL) && (MBLKL(mp) == 0)) {
			tmp = mp->b_cont;
			freeb(mp);
			mp = tmp;
		}
		//
		// Got an empty mblk chain from tcp!
		//
		if (mp == NULL) {
			return (0);
		}

		//
		// Add to the existing mblk chain.
		//
		TCPMOD_STATE_DUMP(q, "new mblk chain from tcp", mp, NULL, true);

		if (infoptr->_mp == NULL) {
			ASSERT(infoptr->_mp_tail == NULL);
			ASSERT(infoptr->_mp_size == 0);
			infoptr->_mp = mp;
		} else {
			ASSERT(infoptr->_mp_tail != NULL);
			ASSERT(infoptr->_mp_size > 0);
			infoptr->_mp_tail->b_cont = mp;
		}
		//
		// Compute the new tail and the new size.
		//
		// Pruning empty mblks along the way
		mp_tail = mp;
		mp_size = (size_t)MBLKL(mp);
		while ((tmp = mp_tail->b_cont) != NULL) {
			if ((mblk_size = (size_t)MBLKL(tmp)) == 0) {
				mp_tail->b_cont = tmp->b_cont;
				freeb(tmp);
			} else {
				mp_size += mblk_size;
				mp_tail = tmp;
			}
		}
		infoptr->_mp_tail  = mp_tail;
		infoptr->_mp_size += mp_size;

		TCPMOD_STATE_DUMP(q, "new mblk chain consumed", mp, NULL, true);

		// Call tcpmodrsv to process the pending message
		// We call this routine as the work to be done is exactly the
		// same as rsrv. We do need to process the messages here
		// as want to get the messages delivered in interrupt context
		// to the ORB. XX - Should have a separate routine that both
		// put and rsrv call.
		(void) tcpmodrsrv(q);
		break;
	}

	default:
		putnext(q, mp);
		break;
	}
	return (0);
}


//
// static bool
// gather_header(mblk_t *mp, tcpmod_header_t *header)
//	Gather the tcpmod header from a new message
//
// Description:
//	This routine gathers the tcpmod header from the
//	datagram message and copies it into the other variable.
//	It leaves the streams message unchanged.
//
// Parameters:
//	mblk_t		*mp	- message containing header
//	tcpmod_header_t *header - pointer to space allocated for header
//
// Returns:
//	true if the full header is copied.
//	false otherwise.
//
//
// See Also:
//	tcpmodrsrv().
//
static bool
gather_header(mblk_t *mp, tcpmod_header_t *header)
{
	size_t len;

	len = mp_to_buf(mp, (char *)header, sizeof (tcpmod_header_t));
	//
	// Check if sufficient data was received to
	// form a valid header.
	//
	if (len >= sizeof (tcpmod_header_t)) {
		ASSERT(header->debug == DEBUG_PATTERN);
		return (true);	// success
	}
	return (false);
}

//
// static mblk_t *
// fragment_msg(queue_t *q, tcpmod_header_t *_header)
//	Assemble a full datagram.
//
// Description:
//	Takes the mblk chain in infoptr->_mp
//	(infoptr is stored in q_ptr of both the queues).
//
//	Reads the tcpmod header into _header (if _mp is too small,
//	returns NULL)
//
//	If there is sufficient data in the mblk chain for a full message,
//	splits off the mblk chain into two parts and returns the pointer to
//	the first message which needs to be sent up to the ORB.
//
// 	Retains the rest of the mblks in infoptr->_mp
//
// 	If there is insufficient data or could not get memory for a dupb,
//	returns NULL and if necessary setup a qbufcall.
//
//	This routine can be called from either a qbufcall or from rput
//
// Parameters:
//	queue_t		*q		- read side queue pointer
//	tcpmod_header_t *_header	- pointer to tcpmod header.
//
//
// Returns:
//	pointer to a streams mblk if full datagram can be copied.
//	NULL otherwise.
//
//
static mblk_t *
fragment_msg(queue_t *q, tcpmod_header_t *_header)
{
	tcpmod_private_t	*infoptr = (tcpmod_private_t *)q->q_ptr;
	mblk_t *mp, *newmb;

#ifdef PARANOID
	//
	// The assert that used this has also been ifdef-ed out.
	//
	uint_t orig_size = (uint_t)infoptr->_mp_size;
#endif

	//
	// Copy the header onto the stack since
	// the header itself may arrive in multiple mblks
	//
	if (infoptr->_mp_size < sizeof (tcpmod_header_t)) {
		//
		// Insufficient bytes in infoptr->_mp for a header
		//
		return (NULL);
	}
	if (gather_header(infoptr->_mp, _header) == 0) {
		ASSERT(0);
	}

	TCPMOD_STATE_DUMP(q, "fragment_msg: header found", NULL, _header, true);
	//
	// We never send 0 byte messages, yet
	//
	ASSERT(_header->size != 0);

	//
	// Check if we already have a candidate first message chain
	// from an earlier call to this routine. If not, examine the
	// mblks that have arrived after we had looked for a complete
	// first message.
	//
	// If we do not have a partially constructed message at all,
	// initialize one first.
	//

	if (infoptr->_mp_first_tail == NULL) {
		ASSERT(infoptr->_mp_first_size == 0);
		infoptr->_mp_first_tail = infoptr->_mp;
		infoptr->_mp_first_size = (size_t)MBLKL(infoptr->_mp);
	}
	ASSERT(infoptr->_mp_first_size > 0);
	ASSERT(infoptr->_mp_first_size <= infoptr->_mp_size);

	if (infoptr->_mp_first_size < _header->size) {
		//
		// We do not yet have enough knowledge about the mblk chain
		// that will constitute the first message, need to scan any
		// new mblks that might have got added to the chain since we
		// last scanned it.
		//
		if (infoptr->_mp_first_size < infoptr->_mp_size) {
			//
			// New mblks have arrived.
			//
			ASSERT(infoptr->_mp_tail != infoptr->_mp_first_tail);
			ASSERT(infoptr->_mp_first_tail->b_cont != NULL);
			//
			// Check if all the new mblks can be consumed by the
			// first message.
			//
			if (infoptr->_mp_size <= _header->size) {
				//
				// All new mblks belong to the first message.
				//
				infoptr->_mp_first_tail = infoptr->_mp_tail;
				infoptr->_mp_first_size = infoptr->_mp_size;
			} else {
				//
				// We have to scan the list of the newly
				// arrived mblks to see where the first message
				// should end.
				//
				mblk_t	*new_tail;
				size_t	 new_size;

				new_size = infoptr->_mp_first_size;
				new_tail = infoptr->_mp_first_tail;
				for (mp = infoptr->_mp_first_tail->b_cont;
					mp != NULL; mp = mp->b_cont) {
					new_tail  = mp;
					new_size += (size_t)MBLKL(mp);
					if (new_size >= _header->size)
						break;
				}
				infoptr->_mp_first_size = new_size;
				infoptr->_mp_first_tail = new_tail;
			}
		}
		//
		// If we failed to receive enough mblks to
		// complete the first message, return. Try
		// again later when this routine gets called
		// next.
		//
		if (infoptr->_mp_first_size < _header->size) {
			TCPMOD_STATE_DUMP(q, "fragment_msg: complete msg has"
			    " not arrived yet", NULL, _header, true);
			return (NULL);
		}
	}

	TCPMOD_STATE_DUMP(q, "fragment_msg: complete msg arrived",
	    NULL, _header, true);
	//
	// At this point we have the entire first message between _mp
	// and _mp_first_tail. _mp_first_tail might have portions of
	// the next message appended, in which case we will have to
	// split the last mblk into two.
	//
	ASSERT(infoptr->_mp_first_size >= _header->size);

	//
	// If infoptr->_mp_first_size == _header->size,
	// we have a clean split.
	//
	if (infoptr->_mp_first_size == _header->size) {
		// Cache mp pointer to the first message
		mp = infoptr->_mp;

		// Point infoptr->_mp to the second message
		infoptr->_mp = infoptr->_mp_first_tail->b_cont;

		// Terminate the first message
		infoptr->_mp_first_tail->b_cont = NULL;

		TCPMOD_STATE_DUMP(q, "fragment_msg: clean split",
		    NULL, _header, false);
	} else {
		//
		// The mblk infoptr->_mp_first_tail spans two messages.
		// First, dup this particular mblk to new mblk.
		//
		newmb = dupb(infoptr->_mp_first_tail);

		if (newmb == NULL) {
			if (infoptr->conn_info.type == TCP_PATHEND) {
				ETCP_DBPRINTF(TCPTR_TRACE_PE_TCPMOD,
				    ("PE%u:%u:%u fragment_msg: dupb failed.\n",
				    ((tcp_pathend *) infoptr->conn_info.obj)
				    ->node().ndid,
				    ((tcp_pathend *) infoptr->conn_info.obj)
				    ->local_adapter_id(),
				    infoptr->conn_info.connection));
			} else {
				ETCP_DBPRINTF(TCPTR_TRACE_PE_TCPMOD,
				    ("EP%u:%u:%u fragment_msg: dupb failed.\n",
				    ((tcp_endpoint *) infoptr->conn_info.obj)
				    ->get_rnode().ndid,
				    ((tcp_endpoint *) infoptr->conn_info.obj)
				    ->local_adapter_id(),
				    infoptr->conn_info.connection));
			}
			setup_bufcall(q, infoptr);
			return (NULL);
		}

		//
		// Link newmb as head of remainder of chain
		//
		newmb->b_cont = infoptr->_mp_first_tail->b_cont;

		//
		// NULL terminate first message
		//
		infoptr->_mp_first_tail->b_cont = NULL;

		//
		// Point mp to the first message
		//
		mp = infoptr->_mp;

		//
		// Point infoptr->_mp to the second message
		//
		infoptr->_mp = newmb;

		//
		// If newmb is the only mblk left in the mblk chain
		// then it must also be the new tail.
		//
		if (newmb->b_cont == NULL)
			infoptr->_mp_tail = newmb;

		//
		// Adjust last mp's wptr of first message
		//
		infoptr->_mp_first_tail->b_wptr -=
			(infoptr->_mp_first_size - _header->size);

		//
		// Adjust first mp's rptr of remainder
		//
		newmb->b_rptr += (uint_t)MBLKL(newmb) -
			(infoptr->_mp_first_size - _header->size);

		ASSERT(infoptr->_mp_first_tail->b_wptr == newmb->b_rptr);

		TCPMOD_STATE_DUMP(q, "fragment_msg: newmb", NULL, _header,
		    false);
	}

	//
	// Update info about the remanining mblks in infoptr
	//
	infoptr->_mp_size -= _header->size;
	if (infoptr->_mp == NULL) {
		// No more messages left in the chain
		infoptr->_mp_tail = NULL;
		ASSERT(infoptr->_mp_size == 0);
	}

	//
	// Indicate that we do not have a partially "constructed"
	// first message any more.
	//
	infoptr->_mp_first_tail = NULL;
	infoptr->_mp_first_size = 0;

#ifdef PARANOID
	//
	// The following asserts have been ifdef-ed out as they might
	// involve chasing long mblk chains, something we cannot allow in
	// the interrupt context.
	//
	ASSERT(msgdsize(mp) == _header->size);
	ASSERT((msgdsize(mp) + msgdsize(infoptr->_mp)) == orig_size);
#endif

	TCPMOD_STATE_DUMP(q, "fragment_msg: returning", mp, _header, true);

	return (mp);
}


//
// static int
// process_recv_msg(queue_t *q, tcpmod_header_t *_header,
//					mblk_t *mp1, bool retry)
//	Process the received full datagram.
//
// Description:
//	This routine is called whenever a full datagram has been
//	received by tcpmod.
// 	The message received can be the initial transport message
//	for a pathend or endpoint connection,
//	an internal monitoring message for the pathend, or a
//	regular orb message (e.g. pxfs).
//
//	It delivers the message to the appropriate layer by
//	calling the correct function depending on the type of the
//	message.
//
//	If the function returns 0 (for regular messages) it queues the
//	entire datagram in its streams queue (infoptr->_mp becomes
//	available for new datagrams) and returns. The read side service
//	procedure will retry them later.
//
//	It also send an ack to the peer in the case of synchronous
//	messages (indicated by _header_ptr in tcpmod header being
//	non NULL).
//
// Parameters:
//	queue_t *q			- read side queue ptr.
//	tcpmod_header_t *_header	- tcpmod header
//	mblk_t *mp1			- datagram
//	bool retry			- indicates where to place the
//					  msg in the queue if delivery
//					  failed.
//
// Returns:
//	0 is the message has been successfully delivered.
//	-1 otherwise.
//
// See Also:
//	tcp_receive_callback() - function to deliver orb messages
//
//
static int
process_recv_msg(queue_t *q, tcpmod_header_t *_header, mblk_t *mp1, bool retry)
{
	tcpmod_private_t	*infoptr;
	tcptr_objtype		objtype;
	tcp_pathend		*pe = NULL;
	tcp_endpoint		*ep = NULL;
	mblk_t 			*ackmp = NULL;

	TCPMOD_STATE_DUMP(q, "process_recv_msg", mp1, _header, true);

	infoptr = (tcpmod_private_t *)q->q_ptr;
	ASSERT(infoptr != NULL);
	objtype = infoptr->conn_info.type;
	if (objtype == TCP_PATHEND) {
		pe = (tcp_pathend *)(infoptr->conn_info.obj);
		ASSERT(pe != NULL);
	} else {
		ASSERT(objtype == TCP_ENDPOINT);
		ep = (tcp_endpoint *)(infoptr->conn_info.obj);
		ASSERT(ep != NULL);
	}

#ifdef PARANOID
	//
	// The following assert has been ifdef-ed out as it might
	// involve chasing long mblk chains, something we cannot
	// allow in the interrupt context.
	//
	ASSERT(msgdsize(mp1) == _header->size);
#endif

	switch (_header->msgt) {
		case TCP_PM_INTERNAL :
			ASSERT(objtype == TCP_PATHEND);
			// Trim out the tcpmod_header
			if ((adjmsg(mp1, sizeof (tcpmod_header_t))) == 0) {
				freemsg(mp1);
				TCP_DBPRINTF(("process_recv_msg:adjmsg"
				    " failed\n"));
				return (0);
			}

			pe->pm_recv_internal(mp1);
			return (0);

		case TCP_PE_INITIAL_MSG:
			ASSERT(objtype == TCP_PATHEND);
			pe->recv_initial_msg(mp1);
			return (0);

		case TCP_EP_INITIAL_MSG:
			ep->recv_initial_msg(
			    infoptr->conn_info.connection, mp1);
			return (0);

		case TCP_KEEP_ACTIVE:
			//
			// The sole purpose of this message was to get some
			// traffic going on the TCP connection. That goal
			// has already been achieved. No more processing is
			// needed.
			//
			freemsg(mp1);
			return (0);

		case TCP_SYNC_ACK:
			// fall through
		case TCP_REPLYIO_ACK:
			ASSERT(objtype == TCP_ENDPOINT);
			ep->tcp_ack_callback(_header->notify_ptr,
				_header->msgt, infoptr->conn_info.connection);
			freemsg(mp1);
			return (0);

		case TCP_REPLYIO_MSG:
			// Should not be processing replyio messages in
			// this routine
			ASSERT(!"wrong message type in process_recv_msg");
			freemsg(mp1);
			return (0);

		default:
			ASSERT(objtype == TCP_ENDPOINT);
			break;
	}

	//
	// Preallocate mblk for reply in case of synchronous sends
	// Makes for simpler error handling
	//
	if (_header->notify_ptr != NULL) {
		ackmp = allocb(sizeof (tcpmod_header_t) + ep->get_wroff(),
		    BPRI_HI);
		if (ackmp == NULL) {
			//
			// Allocation failed, don't send the message to the ORB
			//
			if (retry) {
				(void) putbq(q, mp1);
				// Set timeout instead of qenable, qenable after
				// putbq causes an infinite loop
				set_retry_timeout(q, infoptr);
			} else {
				(void) putq(q, mp1);
				qenable(q);
			}
			return (-1);
		}
		ackmp->b_rptr += ep->get_wroff();
		ackmp->b_wptr = ackmp->b_rptr;
	}

	if ((ep->tcp_receive_callback(_header, mp1, os::NO_SLEEP)) == 0) {
		if (ackmp) {
			freemsg(ackmp);
		}
		if (retry) {
			(void) putbq(q, mp1);
			// Set timeout instead of qenable as qenable after
			// putbq causes an infinite loop
			set_retry_timeout(q, infoptr);
		} else {
			(void) putq(q, mp1);
			qenable(q);
		}
		return (-1);
	}

	//
	// Check if need to send an ACK for synchronous sends
	//
	if (_header->notify_ptr != NULL) {
		ETCP_DBPRINTF(TCPTR_TRACE_SYNC_ACK,
		    ("EP%u:%u:%u Sending sync ack for msgt %d notify ptr %p"
		    " msg size %d.\n", ep->get_rnode().ndid,
		    ep->local_adapter_id(), infoptr->conn_info.connection,
		    _header->msgt, _header->notify_ptr, _header->size));
		//
		// Adjust size of message - we are only sending the header
		// Set msgtype to indicate this is an ack to a synch. send
		//
		_header->size = (uint_t)sizeof (tcpmod_header_t);
		_header->msgt = TCP_SYNC_ACK;
		bcopy((char *)_header, ackmp->b_rptr, sizeof (*_header));
		ackmp->b_wptr += sizeof (tcpmod_header_t);
		putnext(OTHERQ(q), ackmp);
	}

	return (0);
}




//
//
// static int
// tcpmodrsrv(queue_t *q)
//	Read Side Service Procedure.
//
// Description:
//	This procedure processes received messages (the read side
//	put procedure simply queueus them). The tcpmod header is
//	gathered first. From the header, the size of the datagram
//	can be retrieved. From the information provided by the
//	header, the full datagram is collected. The datagram is
//	then delivered to the higher layers by calling the
//	appropriate functions.
//
//	The data is delivered directly avoiding context switches
//	at the streams head.
//
//	The put procedure always queues the incoming messages
//	in the _mp field of the private structure stored in
//	the q_ptr filed of the queue. The service procedure
//	assembles the datagram. If, for some reason, it is not
//	able to deliver the full datagram, it queues the message
//	containing the full datagram in its streams queue.
//
//	The algorithm is:
//		Check the streams queue for any undelivered full
//		datagrams.
//		If any found, try to deliver it now.
//		If it is not possible to deliver it, queue it back
//		and try to work on any new datagrams (queued in
//		the _mp field of the private structure).
//
//	The message received can be an ack for a synchronous message.
//	The acks contain only the tcpmod header. If an ack is
//	received, the corresponding process is signaled.
//
// Parameters:
//	queue_t *q - read side queue pointer.
//
// See Also:
//	gather_header()
//	fragment_msg()
//	process_recv_msg().
//
//	tcp_sendstream::send()
//	tcp_endpoint::send().
//
//
static int
tcpmodrsrv(queue_t *q)
{
	tcpmod_header_t		_header;
	mblk_t			*mp;
	tcpmod_private_t	*infoptr;

	infoptr = (tcpmod_private_t *)q->q_ptr;
	ASSERT(infoptr != NULL);

	// If there is retry timeout scheduled and we get more traffic on
	// the same connection, we may get called before the timeout
	// is scheduled. We remove the timeout here and it may get setup
	// again due to the callbacks we do here.
	if (infoptr->retry_tid != 0) {
		(void) quntimeout(q, infoptr->retry_tid);
	}

	// Process messages in getq first for both
	// normal and replyio connections.
	// replyio connections could have non-bulk data messages
	// that are queued up on allocation failures.
	while ((mp = getq(q)) != NULL) {
		//
		// Copy the header onto the stack,
		// accounting for multiple mblks
		//
		if (gather_header(mp, &_header) == 0) {
			ASSERT(0);
		}

		TCPMOD_STATE_DUMP(q, "processing message from getq",
		    mp, &_header, true);

		if (process_recv_msg(q, &_header, mp, true) == -1) {
			break;
		}
	}

	if (infoptr->conn_info.conn_type == TCP_REPLYIO_CONN) {
		ASSERT(infoptr->conn_info.type == TCP_ENDPOINT);
		//
		// process reply io messages while there is work to do
		//
		while (tcpmod_rio_process_msg(q) != -1) {
			// do nothing
		}
	} else {
		//
		// Fragment messages still left in infoptr->_mp
		//
		while ((mp = fragment_msg(q, &_header)) != NULL) {

			// Do not care for error return as failures do a putq
			(void) process_recv_msg(q, &_header, mp, false);
		}
	}
	return (0);
}

//
// Reply IO related routines
//

//
// Routine to read the replyio header from the mblk chain
//
static bool
rio_gather_header(mblk_t *mp, tcpmod_rio_header_t *rioh)
{
	size_t len;

	len = mp_to_buf(mp, (char *)rioh, sizeof (tcpmod_rio_header_t));
	//
	// Check if sufficient data was received to form a valid header.
	//
	if (len >= sizeof (tcpmod_rio_header_t)) {
		ASSERT(rioh->_header.debug == DEBUG_PATTERN);
		return (true);
	}
	return (false);
}

// Process a non replyio message received on the replyio connection
static int
process_rio_other_msg(queue_t *q, tcpmod_header_t *_header)
{
	mblk_t			*mp;
	tcpmod_private_t	*infoptr;

	//
	// Reset _mp_first_tail and _mp_first_size
	// These are optimizations used in the normal connections
	// which are not maintained correctly in rio connections
	// Resetting them will cause them to be recalculated
	//
	infoptr = (tcpmod_private_t *)q->q_ptr;
	infoptr->_mp_first_tail = NULL;
	infoptr->_mp_first_size = 0;

	TCPMOD_STATE_DUMP(q, "process_rio_other_msg: before fragment msg",
	    NULL, NULL, true);

	// See if there is enough bytes received for a full message
	// and read the header into _header
	mp = fragment_msg(q, _header);

	// reset these again for safety
	infoptr->_mp_first_tail = NULL;
	infoptr->_mp_first_size = 0;

	if (mp == NULL) {
		// Not able to extract message to process
		TCPMOD_STATE_DUMP(q, "process_rio_other_msg: could not"
		    " fragment msg", NULL, NULL, true);
		return (-1);
	}

	TCPMOD_STATE_DUMP(q, "process_rio_other_msg: extracted msg",
	    mp, NULL, true);

	// Verify that we are not processing a reply I/O message
	ASSERT(_header->msgt != TCP_REPLYIO_MSG);

	// do not check return value as process_recv_msg will
	// queue the message to be processed next time in tcpmodrsrv
	(void) process_recv_msg(q, _header, mp, false);
	return (0);
}

// Returns -1 if there is nothing left to process
//	Usually would have setup a timeout to reenable the queue on failures
// Returns 0 is there is more to process

static int
tcpmod_rio_process_msg(queue_t *q)
{
	tcpmod_private_t	*infoptr;
	mblk_t		*mp;
	mblk_t		*mp1;
	mblk_t		*newmb;
	uint_t		len;
	uint_t		riosize = (uint_t)sizeof (tcpmod_rio_header_t);
	uint_t		msize_to_copy;
	tcp_endpoint	*ep;
	uint_t		msglen;

	infoptr = (tcpmod_private_t *)q->q_ptr;
	ASSERT(infoptr != NULL);

	// Nothing to process
	if (infoptr->_mp == NULL) {
		return (-1);
	}

	//
	// Check if this is a new message. If so,
	// gather header and the remote cookie for reply io.
	// We use size == 0 for this check as we do not allow 0 length messages.
	//
	if (infoptr->rio_info.size == 0) {
		tcpmod_rio_header_t	_rioh;

		//
		// Copy the header onto the stack since
		// the header itself may arrive in multiple mblks
		//
		if (infoptr->_mp_size < riosize) {
			// Not enough bytes to read the header.
			// check if received a non replyio message
			return (process_rio_other_msg(q, &(_rioh._header)));
		}

		if (!rio_gather_header(infoptr->_mp, &_rioh)) {
			ASSERT(0);
		}

		// If received a non replyio message, process that if possible
		if (_rioh._header.msgt != TCP_REPLYIO_MSG) {
			return (process_rio_other_msg(q, &(_rioh._header)));
		}

		//
		// At least tcpmod header + cookie is there.
		// size is in the header. Set bytes copied to 0.
		//
		// structure copy

		TCPMOD_STATE_DUMP(q, "rio header located", NULL, &_rioh._header,
		    true);

		infoptr->rio_info.cookie = _rioh.rcookie;
		ASSERT(_rioh._header.size > riosize);
		infoptr->rio_info.size = _rioh._header.size - riosize;
		infoptr->rio_info.offset = 0;
		infoptr->rio_info.connection = infoptr->conn_info.connection;
		ASSERT(_rioh._header.msgt == TCP_REPLYIO_MSG);

		// Remove the header bytes from the message
		(void) adjmsg(infoptr->_mp, (ssize_t)riosize);

		//
		// Update message size _mp_size. Note that after return from
		// adjmsg, the first mblk is guaranteed to stay (maybe empty),
		// however, the only thing guaranteed about the other mblks
		// is that, a mblk would not have been removed from the chain
		// unless all preceding mblks except the first mblk have also
		// been removed. Hence updates to _mp_tail might be required.
		//
		infoptr->_mp_size -= (size_t)riosize;
		if (infoptr->_mp->b_cont == NULL)
			infoptr->_mp_tail = infoptr->_mp;

		//
		// If adjmsg has left an empty mblk at the head of the mblk
		// chain, free it.
		//
		if (MBLKL(infoptr->_mp) == 0) {
			mblk_t *freeit;

			freeit = infoptr->_mp;
			infoptr->_mp = freeit->b_cont;
			if (infoptr->_mp == NULL)
				infoptr->_mp_tail = NULL;
			freeb(freeit);
		}
		TCPMOD_STATE_DUMP(q, "rio header extracted", NULL,
		    &_rioh._header, true);
	}

	//
	// If there is no data in the rio message other than the header,
	// return. Do not call the callback yet.
	//
	msglen = (uint_t)infoptr->_mp_size;
	if (msglen == 0) {
		return (-1);
	}

	mp = infoptr->_mp;
	msize_to_copy = infoptr->rio_info.size - infoptr->rio_info.offset;

	// This has to be of type endpoint
	ASSERT(infoptr->conn_info.type == TCP_ENDPOINT);

	ep = (tcp_endpoint *)(infoptr->conn_info.obj);
	ASSERT(ep != NULL);

	ETCP_DBPRINTF(TCPTR_TRACE_RIO_DETAILS,
		("EP%u:%u:%u tcpmod_rio_process_msg: cookie %p msglen %d"
		" msize %d to_copy %d\n", ep->get_rnode().ndid,
		ep->local_adapter_id(), infoptr->conn_info.connection,
		infoptr->rio_info.cookie.cookie, msglen,
		infoptr->rio_info.size, msize_to_copy));

	TCPMOD_STATE_DUMP(q, "rio data found", NULL, NULL, true);

	//
	// if size of message is exactly what we need...
	// make room for a new message.
	// return message
	//
	if (msglen == msize_to_copy) {
		if (ep->tcp_rio_callback(mp, &infoptr->rio_info)) {
			infoptr->_mp = NULL;
			infoptr->_mp_tail = NULL;
			infoptr->_mp_size = 0;

			TCPMOD_STATE_DUMP(q, "rio data (exact match), copied",
			    NULL, NULL, true);

			bzero(&infoptr->rio_info, sizeof (tcpmod_replyio_info));
		} else {
			set_retry_timeout(q, infoptr);

			TCPMOD_STATE_DUMP(q, "rio data (exact match),"
			    " copy failed", NULL, NULL, true);
		}
		return (-1);
	}

	//
	// If the size is less than the message size,
	// return it and update the length copied.
	// In reply io, the messges are copied as and when
	// received to the (struct buf) there by reducing the need
	// to keep mblks for a long time.
	//
	if (msglen < msize_to_copy) {
		if (ep->tcp_rio_callback(mp, &infoptr->rio_info)) {
			infoptr->rio_info.offset += msglen;
			infoptr->_mp = NULL;
			infoptr->_mp_tail = NULL;
			infoptr->_mp_size = 0;

			TCPMOD_STATE_DUMP(q, "rio data (more needed), copied",
			    NULL, NULL, true);
		} else {
			set_retry_timeout(q, infoptr);

			TCPMOD_STATE_DUMP(q, "rio data (more needed),"
			    " copy failed", NULL, NULL, true);
		}
		return (-1);
	}

	//
	// The message contains all of the old message and
	// new message(s).
	// Extract the old message fully and return it.
	// Leave the new message in infoptr->_mp itself.
	// That will be processed next time.
	//
	ASSERT(msglen > msize_to_copy);

	// Set mp1 at the last mblk of the first message
	for (mp1 = mp, len = 0; mp1 != NULL; mp1 = mp1->b_cont) {
		ASSERT(len < msize_to_copy);

		// increment len by b_wptr - b_rptr
		len += (uint_t)MBLKL(mp1);

		// Check if got sufficient size message
		if (len >= msize_to_copy) {
			break;
		}
	}

	//
	// Cannot exhaust mblk chain without finding full message
	// and should have more mblks left
	//
	ASSERT(mp1 != NULL);

	//
	// mp1 now points at the last mblk of the first message
	// Now, if len == msize_to_copy, we have a clean split
	//
	if (len == msize_to_copy) {
		//
		// Point infoptr->_mp to the second message
		//
		infoptr->_mp = mp1->b_cont;
		infoptr->_mp_size -= len;

		// Terminate the first message
		mp1->b_cont = NULL;

#ifdef PARANOID
		ASSERT(msgdsize(mp) == msize_to_copy);
		ASSERT((msgdsize(mp) + msgdsize(infoptr->_mp)) == msglen);
#endif

		if (ep->tcp_rio_callback(mp, &infoptr->rio_info)) {
			TCPMOD_STATE_DUMP(q, "rio data (clean split), copied",
			    NULL, NULL, true);
			bzero(&infoptr->rio_info, sizeof (tcpmod_replyio_info));
			if (infoptr->_mp_size >= sizeof (tcpmod_header_t)) {
				//
				// Returning 0 will cause caller to call this
				// routine again so next message can be sent up.
				// Since replyio acks arrive on rio connections
				// as well, we check if we have enough data for
				// a tcpmod_header_t and not a bigger
				// tcpmod_rio_header_t which accompanies a rio
				// message.
				//
				return (0);
			}
		} else {
			linkb(mp, infoptr->_mp);
			infoptr->_mp = mp;
			infoptr->_mp_size += len;
			set_retry_timeout(q, infoptr);
			TCPMOD_STATE_DUMP(q, "rio data (clean split),"
			    " copy failed", NULL, NULL, true);
		}
		return (-1);
	}

	ASSERT(len > msize_to_copy);

	//
	// The mblk mp spans two messages.
	// First, dup this particular mblk to new mblk.
	//
	newmb = dupb(mp1);

	if (newmb == NULL) {
		ETCP_DBPRINTF(TCPTR_TRACE_RIO_TCPMOD,
		    ("EP%u:%u:%u tcp_rio_process_msg: dupb failed.\n",
		    ep->get_rnode().ndid, ep->local_adapter_id(),
		    infoptr->conn_info.connection));
		setup_bufcall(q, infoptr);
		return (-1);
	}

	//
	// Link newmb as head of remainder of chain
	//
	newmb->b_cont = mp1->b_cont;

	//
	// NULL terminate first message
	//
	mp1->b_cont = NULL;

	//
	// Point infoptr->_mp to the second message
	// Also indicate new message.
	//
	infoptr->_mp = newmb;

	//
	// If newmb is the only mblk left in the chain, _mp_tail
	// must also point to it.
	//
	if (newmb->b_cont == NULL)
		infoptr->_mp_tail = newmb;
	//
	// Length of remaining message
	//
	infoptr->_mp_size -= msize_to_copy;

	//
	// Adjust last mp's wptr of first message
	//
	mp1->b_wptr -= (len - msize_to_copy);

	//
	// Adjust first mp's rptr of remainder
	//
	newmb->b_rptr += (uint_t)MBLKL(newmb) - (len - msize_to_copy);

	ASSERT(mp1->b_wptr == newmb->b_rptr);

#ifdef PARANOID
	ASSERT(msgdsize(mp) == msize_to_copy);
	ASSERT(msgdsize(mp) + msgdsize(infoptr->_mp) == msglen);
#endif

	if (ep->tcp_rio_callback(mp, &infoptr->rio_info)) {
		TCPMOD_STATE_DUMP(q, "rio data (newmb), copied",
		    NULL, NULL, true);
		bzero(&infoptr->rio_info, sizeof (tcpmod_replyio_info));
		if (infoptr->_mp_size >= sizeof (tcpmod_header_t)) {
			//
			// Returning 0 will cause the caller to call this
			// routine again so next message can be sent up.
			// Since replyio acks arrive on rio connections
			// as well, we check if we have enough data for a
			// tcpmod_header_t and not a bigger tcpmod_rio_header_t
			// which accompanies a rio message.
			//
			return (0);
		}
	} else {
		linkb(mp, infoptr->_mp);
		infoptr->_mp = mp;
		infoptr->_mp_size += msize_to_copy;
		set_retry_timeout(q, infoptr);
		TCPMOD_STATE_DUMP(q, "rio data (newmb), copy failed",
		    NULL, NULL, true);
	}
	return (-1);
}

static void
setup_bufcall(queue_t *q, tcpmod_private_t *infoptr)
{
	if (infoptr->bufcall_id == 0) {
		// Couldn't allocate memory, qbufcall or can wait for next
		// incoming traffic.
		infoptr->bufcall_id = qbufcall(q, sizeof (mblk_t), BPRI_HI,
			tcpmod_bufcall, q);
		if ((infoptr->bufcall_id == 0) && (infoptr->tid == 0)) {
			// If qbufcall fails and we havent setup a timeout
			infoptr->tid = qtimeout(q, tcpmod_timeout, q,
			    (clock_t)RETRY);
			// Have to wait for next incoming traffic to trigger
			ASSERT(infoptr->tid != 0);
		}
	}
}

//
// static int
// tcpmod_wioctl(queue_t *wq, mblk_t *mp)
// 	Process write side ioctls for tcpmod.
//
// Description:
//	It processes the ioctl for tcpmod on the write side.
//	The ioctl currently recognized is CLIOCSTCPINTINFO.
//	This ioctl sets the connection information
//	(object type, object pointer, connection number).
//	The information is stored in the private structure.
//
//	All other messages are passed downstream.
//
// Parameters:
//	queue_t	*q	- write side queue pointer.
//	mblk_t	*mp	- pointer to mblk containing ioctl.
//
//
static int
tcpmod_wioctl(queue_t *wq, mblk_t *mp)
{
	struct	iocblk		*iocp;
	tcpmod_private_t	*infoptr;

	iocp = (struct iocblk *)mp->b_rptr;
	if (iocp->ioc_cmd ==  CLIOCSTCPINTINFO) {
		if ((mp->b_cont == NULL) ||
		    (msgdsize(mp->b_cont) < sizeof (struct tcpmod_conn_info))) {
			miocnak(wq, mp, 0, EINVAL);
			return (0);
		}
		infoptr = (tcpmod_private_t *)wq->q_ptr;
		bcopy(mp->b_cont->b_rptr, &(infoptr->conn_info),
		    sizeof (struct tcpmod_conn_info));
		miocack(wq, mp, 0, 0);
	} else {
		//
		// putnext any other ioctl.
		//
		putnext(wq, mp);
	}
	return (0);
}

//
// static int
// tcpmodwput(queue_t *q, mblk_t *mp)
//	tcpmod write side put procedure.
//
// Description:
//	One ioctl is recognized. All other messages are
//	passed downstream.
//
static int
tcpmodwput(queue_t *q, mblk_t *mp)
{

	if (DB_TYPE(mp) == M_IOCTL) {
		(void) tcpmod_wioctl(q, mp);
		return (0);
	}

	if (DB_TYPE(mp) >= QPCTL) {
		putnext(q, mp);
	} else if (canputnext(q)) {
		putnext(q, mp);
	} else {
		(void) putq(q, mp);
	}
	return (0);
}


//
// Write side service procedure.
//	All queued messages are passed down.
//
//
static int
tcpmodwsrv(queue_t *q)
{
	mblk_t *mp;

	while ((mp = getq(q)) != NULL) {
		if (DB_TYPE(mp) >= QPCTL) {
			putnext(q, mp);
			continue;
		}
		if (canputnext(q)) {
			putnext(q, mp);
		} else {
			(void) putbq(q, mp);
			break;
		}
	}
	return (0);
}

//
// Called when low on memory to schedule a bufcall().
//
static void
tcpmod_bufcall(void *arg)
{
	queue_t	*q = (queue_t *)arg;
	tcpmod_private_t *infoptr = (tcpmod_private_t *)(q->q_ptr);

	infoptr->bufcall_id = 0;
	if (infoptr->tid != 0) {
		// clear any timeout pending on the queue for bufcall
		(void) quntimeout(q, infoptr->tid);
		infoptr->tid = 0;
	}
	// enable q so that service procedures will run
	qenable(q);
}

//
// Try to resetup the bufcall again
//
static void
tcpmod_timeout(void *arg)
{
	queue_t	*q = (queue_t *)arg;
	tcpmod_private_t *infoptr = (tcpmod_private_t *)(q->q_ptr);

	infoptr->tid = 0;
	if (infoptr->bufcall_id != 0) {
		// Couldn't allocate memory, qbufcall or can wait for next
		// incoming traffic.
		infoptr->bufcall_id = qbufcall(q, sizeof (mblk_t), BPRI_HI,
			tcpmod_bufcall, q);
		if (infoptr->bufcall_id == 0) {
			// If qbufcall fails again
			infoptr->tid = qtimeout(q, tcpmod_timeout, q,
			    (clock_t)RETRY);
			// Have to wait for next incoming traffic to trigger
			ASSERT(infoptr->tid != 0);
		}
	}
}


//
// Routines used to reschedule the queue and cause the service
// procedure to run.
// we call this because endpoint is not registered or if there
// is a memory allocation failure in tcp_transport callback and hence
// the message cannot be delivered.
// Currently a timeout of 3 clock ticks are used.
//
static void
tcpmod_retry_timeout(void *arg)
{
	queue_t *q = (queue_t *)arg;
	tcpmod_private_t *infoptr = (tcpmod_private_t *)(q->q_ptr);

	infoptr->retry_tid = 0;

	// enable q to have the service procedures run
	qenable(q);
}

static void
set_retry_timeout(queue_t *q, tcpmod_private_t *infoptr)
{
	if (infoptr->retry_tid == 0) {
		infoptr->retry_tid = qtimeout(q, tcpmod_retry_timeout,
		    q, (clock_t)CALLBACK_RETRY);
		ASSERT(infoptr->retry_tid != 0);
	}
}
