//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)tcp_transport.cc	1.178	09/03/25 SMI"

#include <orb/ip/ifkconfig.h>
#include <orb/msg/message_mgr.h>
#include <orb/fault/fault_injection.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/infrastructure/clusterproc.h>
#include <vm/vm_comm.h>
#include <transports/tcp/tcp_transport.h>
#include <transports/tcp/arp_cache_monitor.h>
#include <sys/mc_probe.h>
#include <sys/callb.h>
#ifdef	_KERNEL
#include <sys/kstr.h>
#endif

extern "C" boolean_t cl_transport_panic_cb(void *, int);
// Tunable for panic notification which is ON by default
uint_t sc_panic_notify = 1;

//
// TCP TRANSPORT
//
// The tcp transport has two parts viz. control/monitor part called
// the "pathend", and the send/recv msgs part called the "endpoint".
//
// The pathend has one tcp connection. It is the first one to be
// created. If the pathend creation is successful and control messages
// can be exchanged with the peer node, the peer node is considered
// reachable and the endpoint is established.
//
// The endpoint has several connections (configurable). These connections
// are used to send/recv various messages from the higher layers
// (examples of higher layers are pxfs, global networking, cmm etc.).
// An endpoint is considered established if all the connections are
// successfully established and messages can be delivered to the
// other side (and received from the other side).
//
//
// Initialization:
//	Each path (consists of one pathend and one endpoint) is established
//	on a physical device (e.g. hme). The physical device (e.g. hme)
//	is configured with a private ip address and a netmask. The
//	configuration and plumbing of the physical devices under ip
//	is the same as that is done in the user space using ifconfig
//	command. In fact, the ifconfig command has been ported in to
//	the kernel space and is used by tcp transport.
//
//	The IP addresses are expected to be assigned from a private
//	address space (e.g. 172.16.0.0 class B network). Each path
//	has to be on a different logical ip network.
//
//	There are no other restrictions on the ip addresses.
//
// TCP Connection
//	The pathend and endpoint are made up of tcp connections.
//	The socket api is used to establish tcp connections.
//	The lower numbered nodes act as clients to higher numbered
//	nodes and establish active connections. The higher numbered
//	nodes listen and accept connections.
//
//	In order to achieve reliable delivery of datagrams and to avoid
//	a context switch at the streams head, a streams module
//	called "tcpmod" is pushed on top of each tcp connection.
//
//	This module collects the received datagram. Once a full
//	datagram is collected, it calls a designated function to
//	deliver the message directly to the higher layers.
//
//	Only after tcpmod has been pushed on to the connection,
//	datagrams can be collected and tcp transport can function.
//	The tcp transport makes use of messages to coordinate
//	the pushing of tcpmod on both the nodes.
//	The state of the endpoint is set to registered only after
//	both sides of a connection are ready with tcpmod.
//
// Servers for Listening and Accepting incoming connections:
//	On each node, there is one server for listening and accepting
//	pathend connections and one server for listening and accepting
//	endpoint connections. The servers hand over the connections to
//	the respective pathend/endpoint as the case may be.
//	The correct pathend/endpoint is determined from the ip address
//	and the cluster configuration database.
//
//	Two tcp ports for used for the servers. One for the pathend
//	and one for the endpoint.
//


//
// TCP Pathend
//	The pathend has one connection. This connection is used
//	to exchange control and monitoring messages.
//
//	The tcp connections has a timeout of 60 secs (or more).
//	When a node goes down and tries to rejoin, if the peer
//	node is in connect() call, it might take a long time to
//	establish a connection. In order to avoid this delay,
//	a udp handshake is provided. Once the udp message is
//	received, it is certain that the tcp connection will
//	go through without delay, as both nodes are up and ready.
//
//	In the rare case that if a (server) node goes down immediately
//	after the udp message exchange, client node could wait for
//	some time. But this case often is the result of a bug and
//	must be fixed.
//
//	The Algorithm/Protocol:
//
//	The Client:
//		(Lower Numbered Node)
//		Wait for udp message from server
//
//		udp msg received:
//			create a socket and push tcpmod
//
//			connect to server
//
//			exchange initial messages with the
//			 server to coordinate tcpmod pushing.
//
//			if (error)
//				go back to wait for udp msg
//			else
//				exchange incarnation number,
//				set up send/recv buffers
//				and declare pathend connected.
//
//
//	The Server:
//		(Higher Numbered Node)
//		Send a udp msg to client.
//
//		wait for client to connect (timeout 5 secs)
//
//		if (connection not received)
//			go back to send the udp msg again.
//
//		if (connection has arrived)
//			push tcpmod
//			exchange initial messages to coordinate
//			the pushing of tcpmod.
//
//			if (error)
//				go back to send the udp msg.
//
//			exchange incarnation number, set up
//			send/recv buffers and declare the
//			pathend connected.
//
//
//	Once the connection is established, the path manager
//	will start sending/receiving monitoring messages.
//	If a pathend is declared down, the path manager will
//	call cleanup() and will restart the connection establishment
//	procedure outlined above.
//
//	Note that cleanup is done after any error and state changes
//	are marked.
//


//
// TCP Endpoint
//	The tcp endpoint is set up similar to the pathend. The only
//	exception being, it has several connections.
//	The algorithm to establish the connections is the same as that
//	in pathend.
//
//	In the case of endpoint, the incarnation number is not exchanged.
//	The connections are used to send/recv messages (e.g.pxfs).
//
//	The number of connections and the nature of the messages
//	sent on each connection is configurable.
//
//	The Algorithm/Protocol:
//
//	The Client:
//		(Lower Numbered Node)
//		Wait for udp message from server
//
//		udp msg received:
//
//		   For each connection
//			create a socket and push tcpmod
//
//			connect to server
//
//			exchange initial messages with the
//			 server to coordinate tcpmod pushing.
//
//			if (error) {
//				clean up
//				go back to wait for udp msg
//			}
//
//			if (last connection) {
//				mark endpoint connected
//				return success to endpoint initiator.
//			}
//
//


//
//	The Server:
//		(Higher Numbered Node)
//		Send a udp msg to client.
//
//		wait for client to connect (timeout 5 secs)
//
//		if (connection not received)
//			go back to send the udp msg again.
//
//		Initial Connection has arrived:
//
//		   Repeat Until all connections are done:
//
//			Get the next connection
//			(waits for 5 secs for each connection)
//			(no blocking memory allocations are
//			 permitted until all connections are
//			 established. preallocate all memory
//			 needed to complete the connections).
//
//			push tcpmod
//
//			exchange initial messages to coordinate
//			the pushing of tcpmod.
//
//			if (error) {
//				cleanup()
//				go back to send the udp msg.
//			}
//
//			if (last connection) {
//				mark endpoint connected
//				return success to endpoint initiator.
//			}
//
//
//	Once all the connections are established, the endpoint is
//	registered. From this point onwards, messages can be sent
//	and received on each connection.
//


//
// TCP SEND
//	In order to send a message, the data has to be copied
//	to a streams message and the streams msg has to be sent on the
//	relevant connection using a putnext().
//
//	The tcp transport does not care about the inner formats
//	of the message. however, each message is preceded by a
//	tcpmod header. tcpmod needs the information contained
//	in the header (such as the size of the message) to correctly
//	deliver the message to the destination. On the receiving
//	end, the header is removed before delivering the message to the ORB.
//
//
// TCP RECEIVE
//	On the receiving side, tcpmod collects the datagram and
//	calls a procedure asynchronously to deliver the message to
//	the ORB. The message is not sent to the streams head.
//
//	However, socket control messages such as connect confirm are
//	sent to the streams head.
//


//
// tcpip header size left in front of the messages
// XX This should really be calculated dynamically but 72 seems like a
// safe number on our interconnects today
//
#define	TCPTR_TCPIP_HDR_SIZE	72

tcp_stats_t tcp_stats;

#ifdef PER_SENDSTREAM_CV
// Time to do a timedwait in sends before checking for endpoint alive
// when we use a per sendstream cv
// See comments in tcp_transport.h about this
static const os::usec_t TCPTR_ACK_WAIT = (os::usec_t)1000000;
#endif

// Time to sleep before retries if something fails during initiate
static const os::usec_t TCPTR_INITIATE_SLEEP = (os::usec_t)100000;


// Time to wait for initial TCP message synchronization
static const os::usec_t TCPTR_INITIAL_MSG_WAIT = (os::usec_t)10000000;

// Time to wait for initial socket connection on server side
static const os::usec_t TCPTR_TCPSOCKET_WAIT = (os::usec_t)5000000;

// Time interval to wait on client side between checking if the
// pathend/endpoint has changed state while we are waiting for the UDP
// handshake to complete.
static const os::usec_t TCPTR_UDPCLIENT_TIMEOUT = (os::usec_t)5000;

// Time to wait on server side for reply to the UDP message before restarting
// the handshake.
static const os::usec_t TCPTR_UDPSERVER_TIMEOUT = (os::usec_t)100000;

//
// Log a warning message if a pathend can not be established after repeated
// tries for this long. We do not log a message on first failed attempt to
// prevent spurious warnings when the machines are booting up and can not
// respond to each others requests immediately.
//
static const os::usec_t TCPTR_PATHEND_INITIATE_TIMEOUT = (os::usec_t)60000000;

//
// We only create 1 tcp_transport class. Dont allow the module to unload till
// this is deleted. XX Currently the shutdown interaction with generic transport
// is not implemented and we do not end up ever unloading the tcp_transport
//
tcp_transport *the_tcp_transport = NULL;

static int tcpmod_push(struct sonode *, tcptr_objtype, void *, uint_t);

//
// The following parameter determines what value to use for setting the
// TCP_ABORT_THRESHOLD and TCP_CONN_ABORT_THRESHOLD for pathend and endpoint
// connections. TCP_ABORT_THRESHOLD is the time that TCP waits for an
// outstanding ack; if no ack is received for that long, the connection is
// dropped. TCP_CONN_ABORT_THRESHOLD is the time TCP waits for a connection
// establishment to complete; if the connection could not be established
// in this time period, the connection attempt is abandoned.
//
// TCP uses the following default values for these parameters:
// TCP_ABORT_THRESHOLD : 8 min
// TCP_CONN_ABORT_THRESHOLD : 3 min
//
// These values are too large for the pathend/endpoint connections, because -
// (a) these connections are established only over single hop connections,
// (b) CMM and its clients, in many cases, expect transport to deliver
//	in a much more timely fashion; several of CMM aborts are smaller
//	than 2 minutes and we run the risk of the entire cluster panicing,
//	if either message delivery or connection establishment takes longer
//	than that.
// The solution is to use much smaller values for the above timeouts and to
// tear down and attempt to reestablish the paths when such problems are seen.
// However, the timeouts should not be made too aggressive, as that will land
// us back into the era of spurious path timeouts.
//
// Ideally, CMM should provide the transport with the message delivery time
// guarantees that it expects from the transport and the transport should set
// the times accordingly, or vice versa. But, until corresponding changes are
// made to the CMM, the following values will be used which are more in
// sync with the 2 min timeouts used by CMM. The values are /etc/system
// settable (some of the tests depend on it), however, this feature is not
// expected to be advertized. The units for both values is seconds.
//
// The TCP_ABORT_THRESHOLD value is not set for pathend connections, as the
// pathends have a faster mechanism (heartbeats) to catch faulty connections.
//
//
#define	MSECS_PER_SEC	1000
#define	MAX_EP_ABORT_THRESHOLD		(INT_MAX/MSECS_PER_SEC) // seconds
#define	DEFAULT_EP_ABORT_THRESHOLD	90	// seconds
#define	DEFAULT_CONN_ABORT_THRESHOLD	30	// seconds
int tcp_transport_ep_abort_threshold = DEFAULT_EP_ABORT_THRESHOLD;
int tcp_transport_conn_abort_threshold = DEFAULT_CONN_ABORT_THRESHOLD;

//
// set when timeouts are disabled and used to restore the value of
// tcp_transport_ep_abort_threshold when enabled again.
//
int tcp_transport_ep_abort_orig;

#define	USECS_IN_A_TICK	10000
#define	USECS_IN_A_SEC	1000000

// A flag to control whether rio acks should be used. They are off
// be default.
static int tcp_transport_use_rio_acks = 0;

//
// helper Routines
//

// There does not seem to be an soclose method in solaris
// instead the following VOP/VN operations are needed to properly close
// a socket.
// XX This should really be in base solaris
static
void
soclose(struct sonode *so)
{
#if	SOL_VERSION >= __s11
	(void) VOP_CLOSE(SOTOV(so), FREAD|FWRITE, 1, (offset_t)0, CRED(), NULL);
#else
	(void) VOP_CLOSE(SOTOV(so), FREAD|FWRITE, 1, (offset_t)0, CRED());
#endif
	VN_RELE(SOTOV(so));
}

//
// Transfers size bytes from mblk chain to contig buffer pointed to by bufp.
// This routine is used to avoid memory allocation for small messages.
// This is similar to msgpullup/pullupmsg but copies to a specified buffer
// Returns the number of bytes copied
//
size_t
mp_to_buf(mblk_t *bp, char *bufp, size_t size)
{
	size_t count = 0;
	size_t len;

	if (bufp == NULL) {
		return (0);
	}

	for (; (bp != NULL) && (count < size); bp = bp->b_cont) {
		ASSERT(bp->b_datap->db_type == M_DATA);
		ASSERT(bp->b_wptr >= bp->b_rptr);
		len = (size_t)MBLKL(bp);
		if ((count + len) > size) {
			len = size - count;
		}
		bcopy(bp->b_rptr, bufp, len);
		count += len;
		bufp += len;
	}
	ASSERT(count <= size);
	return (count);
}

//
// UDP Related routines
//
// A UDP message is sent and received to synchronize both the
// pathend and endpoint initiate() routines.
//
// In pathend, this serves to speed up the connection setup.
//
// In endpoint, it serves to make sure that connections are
// not established before the peer is ready.
//
// The following routines are copied from uts/common/fs/sockfs
// and modified to suit our purposes.
//

//
//
// static
// int
// tcptr_recvfrom(struct sonode *, char *, long, struct sockaddr *, socklen_t)
//
// Description:
//	This routine receives data from a udp socket.
//	It copies at most the specified number of bytes
//	into a buffer. It also copies the source address.
//
//	It uses sorecvmsg(), which is blocking. This routine
//	can be used after doing a poll and making sure that data
//	is there.
//
// Parameters:
//	struct sonode 	*so	- datagram socket
//	char 		*buffer	- buffer to copy data into
//	long		len	- length of buffer
//	struct sockaddr *name	- struct to copy source address into
//	socklen_t	namelen	- size of name above
//
// Returns:
//	number of bytes received on success
//	-1 on error.
//
static
int
tcptr_recvfrom(struct sonode *so, char *buffer, long len,
		struct sockaddr *name, socklen_t namelen)
{
	struct nmsghdr lmsg;
	struct uio auio;
	struct iovec aiov[1];

	aiov[0].iov_base = buffer;
	aiov[0].iov_len = len;
	auio.uio_loffset = 0;
	auio.uio_iov = aiov;
	auio.uio_iovcnt = 1;
	auio.uio_resid = len;
	auio.uio_segflg = UIO_SYSSPACE;
	auio.uio_limit = 0;
	auio.uio_fmode = 0;

	namelen = (socklen_t)sizeof (struct sockaddr);

	lmsg.msg_name = (char *)(name);
	lmsg.msg_namelen = namelen;
	lmsg.msg_controllen = 0;
	lmsg.msg_flags = 0;

#if	SOL_VERSION >= __s11
	ksocket_t	ksp;
	size_t		recv_size;

	lmsg.msg_iov = aiov;
	lmsg.msg_iovlen = 1;
	ksp = SOTOKS(so);

	if (ksocket_recvmsg(ksp, &lmsg, 0, &recv_size, CRED()) != 0) {
		return (-1);
	}
#else
	if (sorecvmsg(so, &lmsg, &auio) != 0) {
		return (-1);
	}
#endif
	if (lmsg.msg_namelen != 0) {
		bcopy(lmsg.msg_name, name, (size_t)(lmsg.msg_namelen));
		kmem_free(lmsg.msg_name, (size_t)(lmsg.msg_namelen));
	}

#if	SOL_VERSION >= __s11
	return ((int)recv_size);
#else
	return ((int)(len - auio.uio_resid));
#endif
}


//
// static
// int
// tcptr_sendto(struct sonode *so, void *buffer, long len, int flags,
//		struct sockaddr *name, socklen_t namelen, int *errorp)
//
// Description:
//	This routine is used to send data on a udp socket.
//	It sends the specified number of bytes to the specified
//	destination address. A datagram socket is used.
//
//	This uses kernel space.  Hence uio segflg must be UIO_SYSSPACE.
//
// Parameters:
//	struct sonode 	*so	- datagram socket
//	char 		*buffer	- buffer to get the data from
//	long		len	- length of buffer
//	int		flags	- XX Need explanation
//	struct sockaddr *name	- address of destination
//	socklen_t	namelen	- size of name above
//	int		*errorp - location to return error
//
// Returns:
//	number of bytes sent on success
//	-1 on error and sets errorp with errno
//
static
int
tcptr_sendto(struct sonode *so, void *buffer, long len, int flags,
    struct sockaddr *name, socklen_t namelen, int *errorp)
{
	struct nmsghdr lmsg;
	struct uio auio;
	struct iovec aiov[1];

	aiov[0].iov_base = (char *)buffer;
	aiov[0].iov_len = len;
	auio.uio_loffset = 0;
	auio.uio_iov = aiov;
	auio.uio_iovcnt = 1;
	auio.uio_resid = len;
	auio.uio_segflg = UIO_SYSSPACE;
	auio.uio_limit = 0;
	auio.uio_fmode = 0;

	lmsg.msg_name = (char *)name;
	lmsg.msg_namelen = namelen;
	lmsg.msg_control = NULL;
	lmsg.msg_controllen = 0;

	flags |= MSG_EOR;
	lmsg.msg_flags = flags;

#if	SOL_VERSION >= __s11
	ksocket_t	ksp;
	size_t		sent_size;
	ksp = SOTOKS(so);

	lmsg.msg_iov = aiov;
	lmsg.msg_iovlen = 1;
	*errorp = ksocket_sendmsg(ksp, &lmsg, flags, &sent_size, CRED());
#else
	*errorp = sosendmsg(so, &lmsg, &auio);
#endif
	if (*errorp != 0) {
		return (-1);
	}
#if	SOL_VERSION >= __s11
	return ((int)sent_size);
#else
	return ((int)(len - auio.uio_resid));
#endif
}

//
//
// static
// struct sonode *
// create_udp_socket(in_addr_t ipaddr, ushort_t port, int *errorp)
//
// Description:
//	This routine creates a udp socket (datagram). It takes the
//	ip address and the port number to bind the socket to.
//	It creates the socket and binds it to the specified address
//	and port. If it is not possible to create and bind the socket
//	it returns a NULL pointer. Otherwise it returns the pointer
//	to a struct sonode (represents socket inside the kernel).
//
//	If the port is 0, the system will pick one.
//
// Parameters:
//	in_addr_t ipaddr - ip address to bind the socket to
//	ushort_t port	 - port to bind to.
//	int *errorp	- pointer where any errno will be stored.
//
// Returns:
//	pointer to struct sonode on success
//	NULL on failure.
//
static
struct sonode *
create_udp_socket(in_addr_t ipaddr, ushort_t port, int *errorp)
{
	struct sonode *so;
	struct sockaddr_in sin;

	bzero(&sin, sizeof (sin));
	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = ipaddr;
	sin.sin_port = htons(port);

#if	SOL_VERSION >= __s11
	ksocket_t	ksp;

	*errorp = ksocket_socket(&ksp, AF_INET, SOCK_DGRAM, 0,
	    KSOCKET_SLEEP, CRED());
	if (*errorp) {
		TCP_DBPRINTF(("create_udp_socket: "
		"ksocket create failed with error = %d\n", *errorp));
		return (NULL);
	}

	*errorp = ksocket_bind(ksp, (struct sockaddr *)&sin,
	    (socklen_t)sizeof (sin), CRED());
	if (*errorp) {
		ksocket_close(ksp, CRED());
		TCP_DBPRINTF(("create_udp_socket: "
		    "ksocket_bind failed with error = %d\n", *errorp));
		return (NULL);
	}
	so = KSTOSO(ksp);
#else	// SOL_VERSION >= __s11
	vnode_t *avp;

	avp = solookup(AF_INET, SOCK_DGRAM, 0, NULL, errorp);
	if (avp == NULL) {
		TCP_DBPRINTF(("create_udp_socket: "
				"solookup failed with error = %d\n", *errorp));
		return (NULL);
	}

	so = socreate(avp, AF_INET, SOCK_DGRAM, 0, SOV_SOCKSTREAM, NULL,
		errorp);
	if (so == NULL) {
		TCP_DBPRINTF(("create_udp_socket: "
				"socreate failed with error %d\n", *errorp));
		return (NULL);
	}


	*errorp = sobind(so, (struct sockaddr *)&sin,
	    (socklen_t)sizeof (sin), 0, 0);
	if (*errorp) {
		soclose(so);
		TCP_DBPRINTF(("create_udp_socket: "
				"sobind failed with error = %d\n", *errorp));
		return (NULL);
	}
#endif	// SOL_VERSION >= __s11
	return (so);
}

//
//
// static
// void
// close_udp_socket(struct sonode *so)
//
// Description:
//	Closes the socket.
//
// Parameters:
//	struct sonode *so	- socket to be closed.
//
//
static
void
close_udp_socket(struct sonode *so)
{
#if	SOL_VERSION >= __s11
	ksocket_t ksp;
	ksp = SOTOKS(so);
	ksocket_close(ksp, CRED());
#else
	soclose(so);
#endif
}


//
// TCP Connection Setup:
// 	Allocate,  initialize and send a message to the
// 	other side as soon as the connection is established.
//
// 	The Server
//		Sends the initial message to the client.
//		Includes the connection number.
//		Client receives the message and sends a message+ack back.
//		It includes its connection number.
//		Both should match.
//
// 	Only after this handshake has been completed,
// 	both sides will be permitted to send/receive messages.
// 	tep_state is set to "connected" only after this exchange.
// 	If tep_state is not "connected", send() from higher layers will
// 	fail.
//

// in_addr_t
// get_peeraddr(struct sonode *so)
//
// Description:
//	get the remote address from the socket.
//	The corresponding socket ioctl is used.
//	The foreign address is stored in the socket structure
//	itself.
//
// Parameters:
//	struct sonode *so	- socket
//
// Returns:
//	the remote ip address
//
in_addr_t
get_peeraddr(struct sonode *so)
{
	int error;
	struct sockaddr_in *addr;

#if	SOL_VERSION >= __s11
	ksocket_t	ksp;
	socklen_t	addrlen;

	ksp = SOTOKS(so);
	addrlen = (socklen_t)sizeof (addr);
#endif
	//
	// Determine where the connection came from
	//
#if	SOL_VERSION >= __s11
	error = ksocket_getpeername(ksp, (struct sockaddr *)&addr,
	    &addrlen, CRED());
#else	// SOL_VERSION >== __s11
	error = sogetpeername(so);
#endif	// SOL_VERSION >= __s11
	if (error) {
		TCP_DBPRINTF(("Error in sogetpeername %d\n", error));
		return ((in_addr_t)-1);
	}

	//
	// Check to see if the socket is still connected. This check
	// is needed since the socket could get disconnected and
	// sogetpeername might still return success (bugid: 5029242).
	// Hence we call sogetpeername() again so that we are sure
	// that the socket is still connected.
	// RFE: 4427307 - this will provide a published way to use
	// networking from within the kernel.
	//
#if	SOL_VERSION >= __s11
	error = ksocket_getpeername(ksp, (struct sockaddr *)&addr,
	    &addrlen, CRED());
#else	// SOL_VERSION >= __s11
	error = sogetpeername(so);
	//
	// Determine the IP address of the peer
	//
	addr = (struct sockaddr_in *)so->so_faddr.soa_sa;
#endif	// SOL_VERSION >= __s11
	if (error) {
		TCP_DBPRINTF(("Error in sogetpeername %d\n", error));
		return ((in_addr_t)-1);
	}

	ASSERT(addr->sin_addr.s_addr != 0);
	return (addr->sin_addr.s_addr);
}


//
// TCP CONNECT Generic:
//

//
// static
// struct sonode *
// set_connect_client(in_addr_t remote_ip_addr, ushort_t port_num,
//	void *obj, tcptr_objtype type, uint_t connection, int *errorp)
//	Initiate and establish an active tcp connection to the
//	remote node.
//
// Description:
//	The nodes initiate connection to higher numbered connections.
//	They listen and accept connections from lower numbered nodes.
//	For example, node 1 will connect to node 2 and node 2 will
//	accept connections from node 1.
//
//	This routine is called to initiate and establish a connection
//	to a given node. The remote ip address is given.
//
//	All tcp transport streams have a module ('tcpmod") pushed
//	onto them. This module reconstructs the datagram and delivers
//	it to the higher layers. This avoids context switching and
//	enables async io. The tcpmod is given information about
//	the connection. The type of object (pathend or endpoint),
//	the object itself, and the connection number within the object
//	are given to the module using an ioctl.
//
//	A tcp socket is created, tcpmod is pushed on to the stream,
//	and the module is given the information about the object.
//	Then a connection is established to the remote node.
//
//
// Parameters:
//	in_addr_t remote_ip_addr	- remote ip address
//	ushort_t port_num		- remote port number
//	void 	*obj			- pointer to object
//	tcptr_objtype	type		- type of object
//					  (pathend or endpoint)
//	int	connection		- connection number within the
//					  object.
//	int	*errorp			- error code is returned in
//					  this pointer
//
// Returns:
//	pointer to struct sonode representing the connected socket
//	on success.
//	NULL on error.
//
static
struct sonode *
set_connect_client(in_addr_t remote_ip_addr,
			ushort_t port_num,
			void *obj,
			tcptr_objtype type,
			uint_t connection,
			int *errorp)
{
	struct sonode *so;
	struct sockaddr_in server;

	// Set the timeout to give up connection establishment to a
	// reasonable value.
	int cabort_timeout = tcp_transport_conn_abort_threshold *
	    MSECS_PER_SEC;

	// Set the protocol family, ip address and port for server
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = remote_ip_addr;
	server.sin_port = htons(port_num);

	*errorp = 0;

#if	SOL_VERSION >= __s11
	ksocket_t	ksp;

	*errorp = ksocket_socket(&ksp, AF_INET, SOCK_STREAM, 0,
	    KSOCKET_SLEEP, CRED());
	if (*errorp != 0) {
		TCP_DBPRINTF(("set_connect_client ksocket create failed"
		    " with error = %d\n", *errorp));
		return (NULL);
	}

	*errorp = ksocket_setsockopt(ksp, IPPROTO_TCP,
	    TCP_CONN_ABORT_THRESHOLD, (void *)&cabort_timeout,
	    (t_uscalar_t)sizeof (cabort_timeout), CRED());
	if (*errorp) {
		ksocket_close(ksp, CRED());
		TCP_DBPRINTF(("set_connect_client: Error in set cabort "
		    "timeout %d for connection %d\n", *errorp, connection));
		return (NULL);
	}
	so = KSTOSO(ksp);
	*errorp = tcpmod_push(so, type, obj, connection);
	if (*errorp != 0) {
		ksocket_close(ksp, CRED());
		TCP_DBPRINTF(("Failed to push tcpmod %d\n", *errorp));
		so = NULL;
		return (NULL);
	}

	*errorp = ksocket_connect(ksp, (struct sockaddr *)&server,
	    (socklen_t)sizeof (server), CRED());
	if (*errorp != 0) {
		TCP_DBPRINTF(("ksocket_connect returned with errno %d\n",
		    *errorp));
		ksocket_close(ksp, CRED());
		so = NULL;
	}
#else	// SOL_VERSION >= __s11
	vnode_t *avp;
	avp = solookup(AF_INET, SOCK_STREAM, 0, NULL, errorp);
	if (avp == NULL) {
		TCP_DBPRINTF(("solookup failed with error = %d\n", *errorp));
		return (NULL);
	}

	so = socreate(avp, AF_INET, SOCK_STREAM, 0, SOV_SOCKSTREAM, NULL,
		errorp);
	if (so == NULL) {
		TCP_DBPRINTF(("socreate failed with error %d\n", *errorp));
		return (NULL);
	}

	*errorp = sosetsockopt(so, IPPROTO_TCP, TCP_CONN_ABORT_THRESHOLD,
	    (void *)&cabort_timeout, (t_uscalar_t)sizeof (cabort_timeout));
	if (*errorp) {
		soclose(so);
		TCP_DBPRINTF(("set_connect_client: Error in set cabort "
		    "timeout %d for connection %d\n", *errorp, connection));
		return (NULL);
	}

	*errorp = tcpmod_push(so, type, obj, connection);
	if (*errorp != 0) {
		soclose(so);
		TCP_DBPRINTF(("Failed to push tcpmod %d\n", *errorp));
		return (NULL);
	}


	*errorp = soconnect(so, (struct sockaddr *)&server,
	    (socklen_t)sizeof (server), 0, 0);
	if (*errorp != 0) {
		TCP_DBPRINTF(("soconnect returned with errno %d\n", *errorp));
		soclose(so);
		so = NULL;
	}
#endif	// SOL_VERSION >= __s11
	return (so);
}


//
// int
// tcpmod_push(struct sonode *so, tcptr_objtype type, void *obj,
//	uint_t connection)
//	- Push the streams module on top of the given socket stream.
//
// Description:
//	Pushes tcpmod on top of the given socket stream and
//	gives the module the information about the connection.
//	The object type (i.e. pathend or endpoint) is given.
//	The pointer to the object and the connection number are also
//	given. The assumption is that tcpmod can do intelligent
//	things with this information. Also note that the put and
//	service procedures of the module are stopped before the
//	module is closed. The module is closed before the pathend
//	or endpoint goes away.
//
// Parameters:
//	struct sonode *so	- socket
//	tcptr_objtype  type	- type of object (i.e. pathend or endpoint)
//	void *obj		- pointer to object
//	int  connection		- connection number within the object
//
// Returns:
//	0 on success and error code on error.
//
int
tcpmod_push(struct sonode *so, tcptr_objtype type, void *obj, uint_t connection)
{
	struct strioctl	iocb;
	struct tcpmod_conn_info conn_info;

#if	SOL_VERSION >= __s11
	int32_t	error;

	error = 0;
	SOP_IOCTL(so, I_PUSH, (intptr_t)"cltcpint", FKIOCTL, CRED(),
	    &error);
#else
	int error;
	error = kstr_ioctl(SOTOV(so), I_PUSH, (intptr_t)"cltcpint");
#endif
	if (error) {
		return (error);
	}

	//
	// Let tcpmod know the endpoint, connection number and conn type
	//
	conn_info.obj = obj;
	conn_info.type = type;
	conn_info.connection = connection;
	if ((type == TCP_ENDPOINT) &&
	    ((tcp_endpoint *)obj)->is_rio_conn(connection)) {
		conn_info.conn_type = TCP_REPLYIO_CONN;
	} else {
		conn_info.conn_type = TCP_NORMAL_CONN;
	}

	iocb.ic_cmd = CLIOCSTCPINTINFO;
	iocb.ic_timout = 0;
	iocb.ic_len = (int)sizeof (struct tcpmod_conn_info);
	iocb.ic_dp = (char *)&conn_info;

#if	SOL_VERSION >= __s11
	error = 0;
	SOP_IOCTL(so, I_STR, (intptr_t)&iocb, FKIOCTL, CRED(), &error);
#else
	error = kstr_ioctl(SOTOV(so), I_STR, (intptr_t)&iocb);
#endif
	return (error);
}

//
// void
// send_initial_msg(struct sonode *so, uint_t connection, uint_t msgtype)
// 	Send a (tcp) message to the remote pathend/endpoint.
//
// Description:
//	Allocates, initializes and sends a message to the other
//	end. Called from both the client and server to do the
//	send.
//
//	The message sent includes the connection # and the incarnation #.
//
//	On the server side, the socket does an accept system call.
//	When the client connects, accept() creates a new socket
//	and sets all the information in the new socket.
//	The client connect succeeds long before the accept() returns
//	on the server. Hence, this explicit handshake is necessary
//	to ensure that both sides have pushed tcpmod and are ready to
//	receive messages. This message is used to synchronize the
//	server and the client nodes so that both are ready to send/recv
//	messages at about the same time.
//
//	In case or tcp endpoint the message is sent only to
//	synchronize the client/server connections, especially the pushing
//	of tcpmod on both sides (to receive the messages). The receive
//	call back routine rejects the message if the endpoint is not
//	in the proper state (E_REGISTERED). XXX This does not seem true in
//	recv_initial_msg. We do not reject it if not in E_REGISTERED.
//
// Parameters:
//	struct sonode *so	- socket
//	uint_t connection	- connection number
//	uint_t msgtype		- msgtype to use
//				(TCP_PE_INITIAL_MSG, TCP_EP_INITIAL_MSG)
//
// Return Value:
//	None
//

static void
send_initial_msg(queue_t *q, uint_t connection, int msgtype)
{
	mblk_t		*mp;
	struct init_msg	*infop;
	tcpmod_header_t *thp;

	mp = os::allocb((uint_t)sizeof (struct init_msg), BPRI_HI, os::SLEEP);
	mp->b_wptr = mp->b_rptr;

	infop = (struct init_msg *)mp->b_wptr;
	thp = &(infop->header);
	thp->msgt = msgtype;
	thp->size = (uint_t)sizeof (struct init_msg);
	thp->notify_ptr = NULL;
#ifdef DEBUG
	thp->src = orb_conf::current_id_node();
	thp->debug = DEBUG_PATTERN;
#endif

	infop->connection = connection;
	infop->src = orb_conf::current_id_node();
	mp->b_wptr += sizeof (struct init_msg);

	putnext(q, mp);
}

//
// int
// tcp_endpoint::tcp_receive_callback(tcpmod_header_t *hdrp, mblk_t *mp,
//			os::mem_alloc_type flag)
//
//	Method to receive datagrams from the tcp connection.
//
// Description:
//	The tcp endpoint class provides this method to receive
//	the full datagrams from tcpmod.
//	The source node, message type (ORB), sequence number
//	(maintained by a higher level layer), are extracted from
//	the tcpmod header.
//	mp contains the message to be delivered to the orb
//	mem_alloc_type indicates context in which this routine is called
//
//	It creates a tcp receive stream object and delivers the
//	message to the orb layer, which will in turn deliver it to
//	the appropriate application (e.g. pxfs)..
//
//	If the memory allocation (object creation) fails, the datagram
//	is returned to tcpmod. The tcpmod will queue the message
//	in its own queue and retry at a later time.
//
// Parameters:
//	tcpmod_header_t	*hdrp	- tcpmod header pointer (on stack)
//	mblk_t		*mp	- streams message containing the datagram
//	os::mem_alloc_type flag - whether called in a blocking context
//
// Returns:
//	1 if the message has been successfully delivered
//	0 otherwise.
//
int
tcp_endpoint::tcp_receive_callback(struct tcpmod_header_t *hdrp, mblk_t *mp,
	os::mem_alloc_type flag)
{
	tcp_recstream *dr;

	//
	// If endpoint is not registered with the endpoint registry
	// do not deliver the message yet. Let tcpmod retry later
	//
	if (!is_endpoint_registered()) {
		ETCP_DBPRINTF(TCPTR_TRACE_EP_ERR,
		    ("EP%u:%u:x tep::receive_callback: EP not registered"
		    " inc %u msg type %d mblk %x\n",
		    get_rnode().ndid, local_adapter_id(), get_rnode().incn,
		    hdrp->msgt, mp));
		return (0);
	}

	// We do not expect to get messages meant for any other incarnation
	// This is because we use TCP sockets and the incarnation number is
	// bound during connection setup
	ASSERT(ID_node::match(hdrp->src, get_rnode()));

	/*CSTYLED*/
	dr = new (flag) tcp_recstream(mp, get_rnode(), (orb_msgtype)hdrp->msgt,
	    this);
	if (dr == NULL)
		return (0);

	orb_msg::the().deliver_message(dr, flag, hdrp->seq);
	return (1);
}

// Function called by tcpmod when it receives an ack for a synchronous message
void
tcp_endpoint::tcp_ack_callback(void *ack_cookie, int ackt, uint_t conn)
{
	nodeid_t log_ndid;
	uint_t	log_adapter_id;
	int	log_ackt;

	tep_lock();
	if (!is_endpoint_registered()) {
		//
		// If endpoint is not registered, then sendstream may not be
		// there and will independently wake up and return
		//
		log_ndid = get_rnode().ndid;
		log_adapter_id = local_adapter_id();
		log_ackt = ackt;
		tep_unlock();
		ETCP_DBPRINTF(TCPTR_TRACE_EP_ERR,
		    ("EP%u:%u:x tep::ack_callback: EP not registered"
		    " ack type %d \n", log_ndid,
		    log_adapter_id, log_ackt));

		return;
	}

	switch (ackt) {
	case TCP_SYNC_ACK :
		((tcp_sendstream *)ack_cookie)->recvd_sync_ack(conn);
		break;
	case TCP_REPLYIO_ACK :
		((tcp_sendstream *)ack_cookie)->recvd_rio_ack(conn);
		break;
	default:
		ASSERT(!"Invalid ack message type");
		break;
	}
	tep_unlock();

}

//
// Set the  ACK timeout for each TCP connection
//
void
tcp_endpoint::set_timeouts(int threshold)
{
	int error;
	uint_t i;
	struct sonode *so;
	int abort_timeout = threshold * MSECS_PER_SEC;

	//
	// so_lock synchronizes with new connection formation
	// in tcp_endpoint::process_accept and also with
	// tcp_endpoint::_cleanup
	//
	so_lock.lock();
	for (i = 0; i < max_conns; i++) {
		so = so_ptrs[i];
		if (so != NULL) {
#if	SOL_VERSION >= __s11
			ksocket_t ksp = SOTOKS(so);
			error = ksocket_setsockopt(ksp, IPPROTO_TCP,
			    TCP_ABORT_THRESHOLD, (void *)&abort_timeout,
			    (t_uscalar_t)sizeof (abort_timeout), CRED());
#else	// SOL_VERSION >= __s11
			    error = sosetsockopt(so, IPPROTO_TCP,
			    TCP_ABORT_THRESHOLD, (void *)&abort_timeout,
			    (t_uscalar_t)sizeof (abort_timeout));
#endif	// SOL_VERSION >= __s11
			if (error) {
				TCP_DBPRINTF(("tcp_endpoint::change_timeouts:"
				    " Error in set abort timeout "
				    "%d for connection %d\n", error, i));
			}
		}
	}
	so_lock.unlock();
}

//
// Called by tcp_transport::timeouts_disable where the ACK timeout was
// set to the largest possible value.
//
void
tcp_endpoint::timeouts_disable()
{
	set_timeouts(tcp_transport_ep_abort_threshold);
	timeouts_disabled = true;
}

//
// Called by tcp_transport::timeouts_enable where the ACK timeout was set
// to the default value.
//
void
tcp_endpoint::timeouts_enable()
{
	set_timeouts(tcp_transport_ep_abort_threshold);
	timeouts_disabled = false;
}


//
// TCP PATHEND Routines
//
// tcp_pathend::tcp_pathend(tcp_transport *tp, clconf_path_t *pp)
//
//	Tcp Pathend Constructor
//
// Description:
//	This is the constructor for the tcp pathend class.
//	It inherits from the generic pathend class.
//	It initializes the tcp pathend state, port numbers
//	and other variables used to hold various information.
//
//	Each tcp pathend corresponds to a path.
//	Each path has 2 adapters (local and remote). The adapter has
//	several properties.
//
//	The properties that are of interest to us are ip address
//	and netmask. The constructor retrieves and stores both the
//	ip address and netmask for the local and remote adapters.
//
//	Messages to send and receive heart beat monitoring messages
//	are allocated and the static information (tcpmod header)
//	are initialized. This will avoid memory allocation during
//	the exchange of the messages.
//
//
// Parameters:
//	tcp_transport	*tp	- pointer to tcp transport object.
//	clconf_path_t	*pp	- path information as configured.
//
// See Also:
//	tcp_pathend class definition in tcp_transport.h
//
tcp_pathend::tcp_pathend(tcp_transport *tp, clconf_path_t *pp) :
	pathend(tp, pp),
	tpe_incn(INCN_UNKNOWN),
	do_esballoc(false),
	udp_send_buf(NULL),
	udp_send_buf_size(0),
	hs_seq(0),
	bad_udp_handshake_lbolt(0),
	tpe_state(TPE_RESET),
	udp_so(NULL),
	n_connections(0),
	first_err_time(0),
	last_warn_time(0),
	in_so_ptr(NULL),
	so_ptr(NULL),
	so_wq(NULL),
	tpe_msg_received(false),
	tpe_udp_msg_received(PE_TCPTR_UDPMSG_CANNOT_RECV),
	rcvd_nonintr_hb(false),
	rdconnp(NULL)
{
	clconf_cluster_t *cl;
	clconf_adapter_t *ap;
	const char *ipaddr;
	mblk_t		*mp;
	tcpmod_header_t *thp;
	const char *lazy_free;

	// Create a raw dlpi connection structure
	rdconnp = new raw_dlpi_conn(this, (size_t)PM_MESSAGE_SIZE);

	//
	// Take copy of ip addresses from clconf
	// This is in lieu of taking a hold on the clconf tree which
	// has the sideeffect of using up too much memory when we do
	// a lot of scconf updates
	//
	cl = clconf_cluster_get_current();

	// Local
	ap = clconf_path_get_adapter(pp, CL_LOCAL, cl);
	ipaddr = clconf_obj_get_property((clconf_obj_t *)ap, "ip_address");
	local_ipaddr = os::inet_addr(ipaddr);
	// We already check for valid ip_address strings in
	// the topology manager pmc_add_adapter
	ASSERT(local_ipaddr != (in_addr_t)(-1));

	// Get lazy_free flag for the local adapter from clconf.
	// lazy_free is a boolean, if it's set to 1, that means driver will
	// defer freeing of esballoc blocks as optimization.  This affects
	// whether esballoc can be used in Buf::get_mblk.  (See bug 4281279)
	// In such case, the boolean do_esballoc will be set to false.
	// If lazy_free is 0, do_esballoc will be set to true.
	lazy_free = clconf_obj_get_property((clconf_obj_t *)ap, "lazy_free");
	if (lazy_free != NULL)
		do_esballoc = !(uint_t)os::atoi(lazy_free);


	//
	// Allocate the buffer for preparing udp handshake messages.
	//
	udp_send_buf_size = sizeof (struct pathend_udp_info) +
	    MAX_MAC_ADDR_SIZE + vm_comm::max_btstrp_np_string();
	udp_send_buf = (char *)kmem_alloc(udp_send_buf_size, KM_SLEEP);

	// Tell the raw dlpi connection what adapter to use.
	rdconnp->set_adapter(ap);

	// Remote
	ap = clconf_path_get_adapter(pp, CL_REMOTE, cl);
	ipaddr = clconf_obj_get_property((clconf_obj_t *)ap, "ip_address");
	remote_ipaddr = os::inet_addr(ipaddr);
	// We already check for valid ip_address strings in
	// the topology manager pmc_add_adapter
	ASSERT(remote_ipaddr != (in_addr_t)(-1));

	clconf_obj_release((clconf_obj_t *)cl);

	//
	// Other initializations
	//

	// kmem_alloc guarantees at least 8 byte alignment
	sendbufp = (char *)kmem_alloc((size_t)PM_MESSAGE_SIZE, KM_SLEEP);
	recvbufp = (char *)kmem_alloc((size_t)PM_MESSAGE_SIZE, KM_SLEEP);
	recv_copy_bufp = (char *)kmem_alloc((size_t)PM_MESSAGE_SIZE, KM_SLEEP);

	send_copy_mp = os::allocb((uint_t)
	    (PM_MESSAGE_SIZE + sizeof (tcpmod_header_t) + TCPTR_TCPIP_HDR_SIZE),
	    BPRI_HI, os::SLEEP);
	send_copy_mp->b_rptr += TCPTR_TCPIP_HDR_SIZE;
	send_copy_mp->b_wptr = send_copy_mp->b_rptr;

	//
	// setup tcpmod header for pm send
	//
	mp = send_copy_mp;
	mp->b_wptr = mp->b_rptr;

	thp = (tcpmod_header_t *)mp->b_wptr;
	thp->msgt = TCP_PM_INTERNAL;
	thp->size = (uint_t)sizeof (tcpmod_header_t) + PM_MESSAGE_SIZE;
	thp->notify_ptr = NULL;
#ifdef DEBUG
	thp->src = orb_conf::current_id_node();
	thp->debug = DEBUG_PATTERN;
#endif

	mp->b_wptr += sizeof (tcpmod_header_t);

	// Create some extra threads for each pathend.
	// We create them in the pathend constructor instead of per endpoint
	// so we dont have to create/delete on each path down
	//
	get_transport()->create_rio_threads(TCP_RIO_THREADS_PER_EP);

	//
	// During Quantum-leap upgrade the transport version number in the
	// old software is incremented and placed in the CCR. After the
	// upgrade we read this from the CCR and initialize the version
	// number with the right value.
	//
	update_transport_version();

	UDP_DBPRINTF(("Transport version = %d\n", sc_transport_version));
}

// void
// tcp_pathend::delete_notify()
//
//	Called when a pathend is being deleted.
//
// Description:
//	This is called by the generic transport when a pathend
//	gets deleted.
// 	It resets the pathend state and wakes up the processes
//	waiting on events.
//
//	It is called with the lock held.
//	Must return with the lock held.
//
void
tcp_pathend::delete_notify()
{
	ASSERT(lock_held());
	unlock();

	tpe_lock();
	tpe_state = TPE_RESET;
	signal_tpe();
	signal_msg_received();
	tpe_unlock();

	lock();
}


//
// void
// tcp_pathend::_cleanup()
//	Internal cleanup method for pathend.
//
// Description:
//	This routine closes any open sockets. It resets state
//	and reinitializes all the variables.
//	This routine is called because of an error in initiate()
//	or the pathend is being closed.
//	This routine does the actual work.
//
void
tcp_pathend::_cleanup()
{
	struct sonode *so1, *so2;

	//
	// tcp_pathend does not use the TCP connections for heartbeat
	// communication any more. The connections can be safely torn
	// down and pointers nulled out right here (unlike in the case
	// of endpoint::_cleanup) as we don't have to worry about any
	// other thread having a reference to these pointers. In fact
	// eventually all TCP connection related code should be eliminated
	// from the tcp_pathend class.
	//
	tpe_lock();
	tpe_state = TPE_RESET;
	so_wq = NULL;
	n_connections = 0;
	tpe_msg_received = false;

	so1 = so_ptr;
	so_ptr = NULL;

	so2 = in_so_ptr;
	in_so_ptr = NULL;
	tpe_unlock();

#if	SOL_VERSION >= __s11
	ksocket_t	ksp;
#endif	// SOL_VERSION >= __s11
	// Avoid doing disconnect/close while holding lock.
	if (so1 != NULL) {
#if	SOL_VERSION >= __s11
		ksp = SOTOKS(so1);
		ksocket_close(ksp, CRED());
#else	// SOL_VERSION >= __s11
		(void) sodisconnect(so1, -1, 0);
		soclose(so1);
#endif	// SOL_VERSION >= __s11
	}
	if (so2 != NULL) {
#if	SOL_VERSION >= __s11
		ksp = SOTOKS(so2);
		ksocket_close(ksp, CRED());
#else	// SOL_VERSION >= __s11
		(void) sodisconnect(so2, -1, 0);
		soclose(so2);
#endif	// SOL_VERSION >= __s11
	}
}


//
// void
// tcp_pathend::cleanup()
//	cleanup the pathend.
//
// Description:
//	This is the cleanup() routine called by the generic transport.
//	Called after initiate() returns (when the pathend fails
//	or is being closed). Calls _cleanup() to get the work done.
//
//
void
tcp_pathend::cleanup()
{
	TCP_DBPRINTF(("Pathend cleanup node %d adapter %d rnode %d state %d\n",
	    orb_conf::local_nodeid(), local_adapter_id(), node().ndid,
	    get_state()));
	unlock();
	rdconnp->close();
	_cleanup();
	lock();
}

//
// void
// tcp_pathend::reinit()
//	Cleanup the remnants of the previous incarnation of this path.
//
// Description:
//	This routine is called by the generic transport when a pathend
//	is recycled and moves from the P_DRAINING state to P_CONSTRUCTED
//	state. In this routine the that was left open in cleanup is
//	called. Called with lock held. Should return with lock held.
//
void
tcp_pathend::reinit()
{
	TCP_DBPRINTF(("Pathend reinit node %d adapter %d rnode %d state %d\n",
	    orb_conf::local_nodeid(), local_adapter_id(), node().ndid,
	    get_state()));
}

//
// tcp_pathend::~tcp_pathend()
//	Destructor for tcp pathend class
//
// Description:
//	Called when the pathend goes away.
//	Set the state to close.  Disconnects and closes all
//	open sockets. Frees all messages.
//	Sets all variables to their initial value.
//
//
tcp_pathend::~tcp_pathend()
{
	if (rdconnp) {
		delete rdconnp;
		rdconnp = NULL;	//lint !e423
	}

	ASSERT(in_so_ptr == NULL);
	ASSERT(so_ptr == NULL);

	kmem_free(sendbufp, (size_t)PM_MESSAGE_SIZE);
	sendbufp = NULL;
	kmem_free(recvbufp, (size_t)PM_MESSAGE_SIZE);
	recvbufp = NULL;
	kmem_free(recv_copy_bufp, (size_t)PM_MESSAGE_SIZE);
	recv_copy_bufp = NULL;

	if (send_copy_mp != NULL) {
		freemsg(send_copy_mp);
	}
	send_copy_mp = NULL;

	if (udp_send_buf != NULL) {
		kmem_free(udp_send_buf, udp_send_buf_size);
	}
	udp_send_buf = NULL;
	udp_send_buf_size = 0;

	ASSERT(udp_so == NULL);

	// Uncreate the threads created in the constructor
	get_transport()->create_rio_threads(-TCP_RIO_THREADS_PER_EP);
}

//
//
// endpoint *
// tcp_pathend::new_endpoint(transport *trp)
//	Create a new endpoint for this pathend.
//
// Description:
//	This routine is called when the pathend creation is
//	successful and the other node is reachable via the
//	pathend connection.
//
//	Currently, only one endpoint per pathend is created.
//	The endpoint has several connections to carry several
//	different types of messages efficiently.
//
//	Creates a tcp endpoint object and returns the pointer to
//	the new object.
//
// Parameters:
//	transport	*trp - pointer to the generic transport object.
//
// Returns:
//	pointer to the newly created tcp endpoint on success.
//	NULL on error.
//
endpoint *
tcp_pathend::new_endpoint(transport *trp)
{
	return (new tcp_endpoint((tcp_transport *)trp, this));
}

int
tcp_pathend::udp_init()
{
	int error;

	udp_so = create_udp_socket(local_ipaddr, 0, &error);
	if (udp_so == NULL) {
		return (error);
	}

	udp_msg_lock();
	set_udp_msg_state(PE_TCPTR_UDPMSG_CAN_RECV_REQ);
	udp_msg_unlock();

	return (0);
}

void
tcp_pathend::udp_reset()
{
	udp_msg_lock();
	set_udp_msg_state(PE_TCPTR_UDPMSG_CAN_RECV_REQ);
	udp_msg_unlock();
}

void
tcp_pathend::udp_finish()
{
	udp_msg_lock();
	set_udp_msg_state(PE_TCPTR_UDPMSG_CANNOT_RECV);
	udp_msg_unlock();

	if (udp_so != NULL) {
		close_udp_socket(udp_so);
		udp_so = NULL;
	}
}

//
// tcp_pathend::udp_putq
//
// This is the method that processes incoming UDP handshake messages.
// Reply messages are also sent out if needed. The other method that
// sends out udp handshake messages is tcp_pathend::udp_server_initiate().
// The two methods must co-ordinate the use of the per pathend udp send
// buffer among themselves. The ACTIVE_REQUEST message is sent by the
// udp_server_initiate method. All other messages are sent by the udp_putq
// method.
//
// The tcp_transport object guarantees that there can be only one
// udp_putq call in progress at any point of time.
//
void
tcp_pathend::udp_putq(struct pathend_udp_info *imsg, struct sockaddr_in *sin,
	struct sonode *so)
{
	int	rlen;
	int	error;
	uchar_t	*lmacaddr = NULL;
	size_t	lmaclen = 0;
	int	compatible = 1;
	char	*send_ver_buf = NULL;
	size_t	send_ver_size = 0;
	struct	pathend_udp_info	*omsg = NULL;


	//
	// If we do not understand the header format in the incoming message,
	// reply with a header version info message. To avoid header version
	// info storm, send the header version info message only in response
	// to an ACTIVE_REQUEST message.
	//
	if (imsg->hdrver != PATHEND_UDP_INFO_VERSION) {
		UDP_DBPRINTF((" PE %p laid %d udp_putq message type %d with"
		    " unknown header version %d arrived.\n",
		    this, local_adapter_id(), imsg->msgtype, imsg->hdrver));

		if (imsg->msgtype != ACTIVE_REQUEST)
			return;

		udp_send_buf_lock.lock();

		omsg = (struct pathend_udp_info *)udp_send_buf;
		bzero(omsg, sizeof (struct pathend_udp_info));
		omsg->hdrver		= PATHEND_UDP_INFO_VERSION;
		omsg->hdrlen		=
			(uint32_t)sizeof (struct pathend_udp_info);
		omsg->msglen		= omsg->hdrlen;
		omsg->msgtype		= HANDSHAKE_VERSION_INFO;
		omsg->nodeid		= orb_conf::current_id_node().ndid;
		omsg->nodeinc		= orb_conf::current_id_node().incn;
		omsg->zero		= 0;
		omsg->sc_version	= SC_VERSION_STR;

		sin->sin_port = htons(UDP_PE_PORT);
		rlen = tcptr_sendto(so, (char *)omsg, (long)omsg->msglen,
		    0, (struct sockaddr *)sin, (socklen_t)sizeof (*sin),
		    &error);

		if (error != 0 || rlen != (int)omsg->msglen) {
			UDP_DBPRINTF(("PE %p laid %d udp_putq could not"
			    " send header info message\n", this,
			    local_adapter_id()));
		} else {
			UDP_DBPRINTF(("PE %p laid %d udp_putq send header"
			    " info version %d message sent\n", this,
			    local_adapter_id(), omsg->hdrver));
		}
		udp_send_buf_lock.unlock();
		return;
	}

	//
	// If the peer node does not understand our header format, we can not
	// help as we do not know of any other header format.
	//
	if (imsg->msgtype == HANDSHAKE_VERSION_INFO) {
		UDP_DBPRINTF((" PE %p laid %d udp_putq handshake info message"
		    " arrived version %d.\n", this, local_adapter_id(),
		    imsg->hdrver));
		return;
	}

	//
	// Make sure message arrived from the IP address that we associate
	// with the peer pathend. Sanity check.
	//
	if (sin->sin_addr.s_addr != remote_ipaddr) {
		UDP_DBPRINTF(("PE %p peer %d laid %d udp_putq remote"
		    " address 0x%x does not match expected 0x%x\n",
		    this, imsg->nodeid, local_adapter_id(),
		    sin->sin_addr.s_addr, remote_ipaddr));
		return;
	}

	//
	// Check that the message carries the message carries the nodeid
	// of the peer pathend. Sanity check.
	//
	if (imsg->nodeid != node().ndid) {
		UDP_DBPRINTF(("PE %p peer %d laid %d udp_putq remote"
		    " nodeid does not match expected %d\n",
		    this, imsg->nodeid, local_adapter_id(), node().ndid));
		return;
	}

	//
	// The UDP handshake packets can only be received if we are in the
	// PE_TCPTR_UDPMSG_CAN_RECV_REQ or PE_TCPTR_UDPMSG_CAN_RECV_RESP state.
	// So drop the packet if we are not in that state. If we have already
	// declared the UDP handshake complete and the remote node is retrying,
	// drop the message. Wait for the initiate process to start all over
	// again locally.
	//
	udp_msg_lock();
	if (get_udp_msg_state() != PE_TCPTR_UDPMSG_CAN_RECV_REQ &&
	    get_udp_msg_state() != PE_TCPTR_UDPMSG_CAN_RECV_RESP) {
		UDP_DBPRINTF(("PE %p peer %d laid %d udp_putq handshake"
		    " protocol needs to be restarted\n", this,
		    imsg->nodeid, local_adapter_id()));
		udp_msg_unlock();
		return;
	}
	udp_msg_unlock();

	// Log message arrival info
	switch (imsg->msgtype) {
	case ACTIVE_REQUEST:
		// Active request can arrive anytime
		UDP_DBPRINTF((" PE %p peer %d laid %d hdrver %d udp_putq"
		    " active request arrived seq %u.\n", this, imsg->nodeid,
		    local_adapter_id(), imsg->hdrver, imsg->hs_seq));
		break;
	case PASSIVE_RESPREQ:
		UDP_DBPRINTF((" PE %p peer %d laid %d hdrver %d udp_putq"
		    " passive resprep arrived seq %u.\n", this, imsg->nodeid,
		    local_adapter_id(), imsg->hdrver, imsg->hs_seq));
		//
		// Passive respreq can arrive only when we are in
		// PE_TCPTR_UDPMSG_CAN_RECV_RESP state
		//
		udp_msg_lock();
		if (get_udp_msg_state() != PE_TCPTR_UDPMSG_CAN_RECV_RESP) {
			UDP_DBPRINTF(("PE %p peer %d laid %d udp_putq"
			    " passive respreq drop\n", this,
			    imsg->nodeid, local_adapter_id()));
			udp_msg_unlock();
			return;
		}
		udp_msg_unlock();
		break;
	case ACTIVE_RESPONSE:
		//
		// Active response can arrive only when we are in
		// PE_TCPTR_UDPMSG_CAN_RECV_RESP state
		//
		UDP_DBPRINTF((" PE %p peer %d laid %d hdrver %d udp_putq"
		    " active response arrived seq %u.\n", this, imsg->nodeid,
		    local_adapter_id(), imsg->hdrver, imsg->hs_seq));
		udp_msg_lock();
		if (get_udp_msg_state() != PE_TCPTR_UDPMSG_CAN_RECV_RESP) {
			UDP_DBPRINTF(("PE %p peer %d laid %d udp_putq"
			    " active response drop\n", this,
			    imsg->nodeid, local_adapter_id()));
			udp_msg_unlock();
			return;
		}
		udp_msg_unlock();
		break;
	default:
		UDP_DBPRINTF((" PE %p peer %d laid %d hdrver %d udp_putq"
		    " unknown message arrived seq %u.\n", this, imsg->nodeid,
		    local_adapter_id(), imsg->hdrver, imsg->hs_seq));
		ASSERT(0);
		return;
	}


	//
	// Notify path manager about the current node incarnation. Path manager
	// will return with whether it is willing to accept a pathend from this
	// incarnation of the remote node. This step is necessary before we
	// call into the version manager for version verification.
	//
	if (imsg->msgtype == ACTIVE_REQUEST ||
	    imsg->msgtype == PASSIVE_RESPREQ) {
		if (!path_manager::the().update_node_incarnation(imsg->nodeid,
		    imsg->nodeinc)) {
			//
			// This incarnation has become stale. Must boot with
			// a newer incarnation to be allowed in.
			//
			UDP_DBPRINTF(("PE %p peer %d laid %d udp_putq stale"
			    " remote incarnation %d\n", this, imsg->nodeid,
			    local_adapter_id(), imsg->nodeinc));
			return;
		}
	}

	//
	// Discard stale message. Everytime an ACTIVE_REQUEST is sent
	// a new UDP hanshake sequence is started. We do not honor
	// messages belonging to other handshake sequences. Note that
	// this simple logic can allow an older ACTIVE_REQUEST messsage
	// arriving late to reset a handshake protocol. Unless packets
	// arrive out of order by default, this is not a problem.
	//
	if (imsg->msgtype == ACTIVE_REQUEST) {
		hs_seq = imsg->hs_seq;
	} else if (imsg->hs_seq != hs_seq) {
		UDP_DBPRINTF(("PE %p peer %d laid %d udp_putq sequence"
		    " number mismatch expected %u received %u\n",
		    this, imsg->nodeid, local_adapter_id(), hs_seq,
		    imsg->hs_seq));
		return;
	}

	//
	// Log the compatibility result from the remote machine.
	//
	if ((imsg->msgtype == PASSIVE_RESPREQ ||
	    imsg->msgtype == ACTIVE_RESPONSE) &&
	    !imsg->compatible) {
		UDP_DBPRINTF(("PE %p peer %d laid %d udp_putq version"
		    " mismatch\n", this, imsg->nodeid, local_adapter_id()));
	}

	//
	// If we need to send a reply (i.e., if this message has a request)
	// make sure we have enough data to send the reply.
	//
	if (imsg->msgtype == ACTIVE_REQUEST ||
	    imsg->msgtype == PASSIVE_RESPREQ) {
		if (rdconnp->get_local_addr(&lmacaddr, &lmaclen) != 0) {
			//
			// Local MAC address is not available yet, drop this
			// message so that the peer retries.
			//
			UDP_DBPRINTF(("PE %p peer %d laid %d udp_putq mac"
			    " address not available\n", this, imsg->nodeid,
			    local_adapter_id()));
			return;
		}

		send_ver_buf	= udp_send_buf +
		    sizeof (struct pathend_udp_info);
		send_ver_size	= udp_send_buf_size - MAX_MAC_ADDR_SIZE -
		    sizeof (struct pathend_udp_info);

		udp_send_buf_lock.lock();
		if (vm_comm::the().get_btstrp_np_string(send_ver_buf,
		    send_ver_size) == NULL) {

			UDP_DBPRINTF(("PE %p peer %d laid %d udp_putq version"
			    " buffer overflow\n",
			    this, imsg->nodeid, local_adapter_id()));

			ASSERT(0);

			udp_send_buf_lock.unlock();

			return;
		}
	}

	//
	// All checks have passed, we are ready to accept the message, i.e.,
	// now we can proceed to compatibility checks. Before that record
	// the fact that the message was accepted.
	//
	switch (imsg->msgtype) {
	case ACTIVE_REQUEST:
		UDP_DBPRINTF((" PE %p peer %d laid %d udp_putq active request"
		    " accepted seq %u.\n", this, imsg->nodeid,
		    local_adapter_id(), imsg->hs_seq));
		break;
	case PASSIVE_RESPREQ:
		UDP_DBPRINTF((" PE %p peer %d laid %d udp_putq passive resprep"
		    " accepted seq %u.\n", this, imsg->nodeid,
		    local_adapter_id(), imsg->hs_seq));
		break;
	case ACTIVE_RESPONSE:
		UDP_DBPRINTF((" PE %p peer %d laid %d udp_putq active response"
		    " accepted seq %u.\n", this, imsg->nodeid,
		    local_adapter_id(), imsg->hs_seq));
		break;
	default:
		ASSERT(0);
		break;
	}


	//
	// Now the compatibility check in case the message had a request part.
	//
	if (imsg->msgtype == ACTIVE_REQUEST ||
	    imsg->msgtype == PASSIVE_RESPREQ) {

		compatible = vm_comm::the().compatible_btstrp_np(
		    (nodeid_t)imsg->nodeid,
		    (incarnation_num)imsg->nodeinc,
		    (size_t)imsg->version_len,
		    ((char *)imsg) + imsg->version_offset,
		    imsg->compatible ? false : true);

		if (!compatible) {
			UDP_DBPRINTF(("PE %p peer %d laid %d udp_putq version"
			    " mismatch\n", this, imsg->nodeid,
			    local_adapter_id()));
		}
	}

	//
	// If this was a request message, a reply is needed.
	//
	if (imsg->msgtype == ACTIVE_REQUEST ||
	    imsg->msgtype == PASSIVE_RESPREQ) {

		omsg = (struct pathend_udp_info *)udp_send_buf;
		bzero(omsg, sizeof (struct pathend_udp_info));

		omsg->hdrver = PATHEND_UDP_INFO_VERSION;
		omsg->hdrlen = (uint32_t)sizeof (struct pathend_udp_info);

		if (imsg->msgtype == ACTIVE_REQUEST) {
			omsg->msgtype = PASSIVE_RESPREQ;
		} else {
			omsg->msgtype = ACTIVE_RESPONSE;
		}
		omsg->hs_seq		= hs_seq;
		omsg->nodeid		= orb_conf::current_id_node().ndid;
		omsg->nodeinc		= orb_conf::current_id_node().incn;
		omsg->compatible	= compatible ? 1 : 0;
		omsg->version_offset	= omsg->hdrlen;
		omsg->version_len	= (uint32_t)send_ver_size;
		omsg->zero		= 0;
		omsg->sc_version	= SC_VERSION_STR;

		//
		// Stuff in local mac address information if the compatibility
		// check was a success.
		//
		if (compatible) {
			omsg->mac_len	= (uint32_t)lmaclen;
			omsg->mac_offset = omsg->version_offset;
			omsg->mac_offset += omsg->version_len;
			ASSERT(lmacaddr != NULL);
			bcopy(lmacaddr, ((char *)omsg) + omsg->mac_offset,
			    lmaclen);
		}

		omsg->msglen = omsg->hdrlen + omsg->version_len + omsg->mac_len;

		//
		// Send the message.
		//
		sin->sin_port = htons(UDP_PE_PORT);
		rlen = tcptr_sendto(so, (char *)omsg, (long)omsg->msglen,
		    0, (struct sockaddr *)sin, (socklen_t)sizeof (*sin),
		    &error);

		if (error != 0 || rlen != (int)omsg->msglen) {
			UDP_DBPRINTF(("PE %p peer %d laid %d udp_putq could not"
			    " send message %d\n", this, omsg->nodeid,
			    local_adapter_id(), omsg->msgtype));
		} else {
			if (omsg->msgtype == PASSIVE_RESPREQ) {
				UDP_DBPRINTF(("PE %p peer %d laid %d udp_putq"
				    " passive respreq sent seq %u\n", this,
				    imsg->nodeid, local_adapter_id(),
				    imsg->hs_seq));
				// Now wait for active response
				udp_msg_lock();
				set_udp_msg_state(
				    PE_TCPTR_UDPMSG_CAN_RECV_RESP);
				udp_msg_unlock();
			} else if (omsg->msgtype == ACTIVE_RESPONSE) {
				UDP_DBPRINTF(("PE %p peer %d laid %d udp_putq"
				    " active response sent seq %u\n", this,
				    imsg->nodeid, local_adapter_id(),
				    imsg->hs_seq));
			}
		}
		udp_send_buf_lock.unlock();
	}

	//
	// If it was a response message and we are here, the UDP handshake
	// is complete from our point of view.
	//
	if ((imsg->msgtype == PASSIVE_RESPREQ ||
	    imsg->msgtype == ACTIVE_RESPONSE) &&
	    (compatible && imsg->compatible)) {
		udp_msg_lock();
		// Make sure we were actually waiting for a response
		// It could happen that the udp_getq in udp_server_initiate
		// can timeout by the time we get here and thus result in
		// udp_reset which sets the state to
		// PE_TCPTR_UDPMSG_CAN_RECV_REQ.
		if (get_udp_msg_state() != PE_TCPTR_UDPMSG_CAN_RECV_RESP) {
			UDP_DBPRINTF(("PE %p peer %d laid %d udp_putq"
			    " incorrect state. Handshake not complete\n", this,
			    imsg->nodeid, local_adapter_id()));
			udp_msg_unlock();
			return;
		}
		//
		// UDP handshake is complete
		//
		// Save the received remote mac address with the
		// rdconn object. This address will be used to send
		// raw dlpi messages.
		//
		rdconnp->set_remote_info(((unsigned char *)imsg) +
		    imsg->mac_offset, (size_t)imsg->mac_len,
		    imsg->nodeinc);

		//
		// Mark UDP handshake complete.
		//
		set_udp_msg_state(PE_TCPTR_UDPMSG_RECEIVED);
		udp_msg_signal();
		UDP_DBPRINTF(("PE %p peer %d laid %d udp handshake seq %u"
		    " complete.\n", this, imsg->nodeid, local_adapter_id(),
		    imsg->hs_seq));
		udp_msg_unlock();
	} else {
		UDP_DBPRINTF(("PE %p peer %d laid %d udp handshake seq %u"
		    " not complete compatible %d imsgcompat %d type %d.\n",
		    this, imsg->nodeid, local_adapter_id(), imsg->hs_seq,
		    compatible, imsg->compatible, imsg->msgtype));
	}
}

// Waits for udp message or timeout
// Returns 0 if udp state indicates message received, else returns ETIME
int
tcp_pathend::udp_getq(clock_t timval)
{
	udp_msg_lock();
	if (get_udp_msg_state() != PE_TCPTR_UDPMSG_RECEIVED) {
		os::systime timo(timval);
		(void) tpe_udp_msg_cv.timedwait(&tpe_udp_msg_lock, &timo);
		if (get_udp_msg_state() != PE_TCPTR_UDPMSG_RECEIVED) {
			udp_msg_unlock();
			return (ETIME);
		}
	}
	udp_msg_unlock();
	return (0);
}


//
// TCP PATHEND CONNECTION Establishment routines:
//
//
// int
// tcp_pathend::udp_client_initiate()
//	Initiate a udp rendezvous.
//
// Description:
//	TCP Connect has a timeout of 60 seconds.
//	And it is blocking. Hence, if a node goes down and comes back up,
//	sometimes it will take a long time (>30 secs) to establish
//	the connection again. In order to avoid this delay, a udp
//	handshake is performed. The udp handshake also serves as a
//	vehicle for initial version management negotiation.
//
//	The timeouts have been set to 5 secs. It means that within
//	timeout secs the connections will start (provided both the
//	nodes are up). timeout is configurable.
//
//	Wait for the udp msg to arrive periodically waking up to check pathend
//	state. The udp message is received by a central server common to all
//	pathends.
//
// Parameters:
//	None.
//
// Returns:
//	0 on success.
//	error code on error.
//
// See Also:
//	tcp_pathend::udp_server_initiate().
//
//
int
tcp_pathend::udp_client_initiate()
{
	int error;

	// While udp message has not arrived from server
	while ((error = udp_getq(TCPTR_UDPCLIENT_TIMEOUT)) == ETIME) {
		//
		// check pathend state if need to return earlier
		// if pathend state changes we return EPROTO instead of ETIME
		//
		lock();
		if ((get_state() != P_CONSTRUCTED) &&
		    (get_state() != P_INITERR)) {
			unlock();
			return (EPROTO);
		}
		unlock();
		tpe_update_pe_state(error);
	}
	return (error);
}

//
// int
// tcp_pathend::udp_server_initiate()
//	Initiate the udp handshake.
//
// Description:
//
//	The server node initiates the udp handshake from this call.
//
//	This routine is called by the server_initiate() routine,
//	which in turn is called by the initiate() routine.
//
int
tcp_pathend::udp_server_initiate()
{
	int		rlen;
	int		error;
	char		*version_buf;
	size_t		version_len;
	struct sockaddr_in	sin;
	struct pathend_udp_info	*info;

	// udp socket is in udp_so.
	ASSERT(udp_so != NULL);

	//
	// Obtain the local version information.
	//
	version_buf = udp_send_buf + sizeof (struct pathend_udp_info);
	version_len = udp_send_buf_size - MAX_MAC_ADDR_SIZE -
	    sizeof (struct pathend_udp_info);
	udp_send_buf_lock.lock();
	if (vm_comm::the().get_btstrp_np_string(version_buf, version_len)
	    == NULL) {
		UDP_DBPRINTF(("PE %p peer %d laid %d udp_putq version"
		    " buffer overflow\n",
		    this, node().ndid, local_adapter_id()));

		ASSERT(0);

		udp_send_buf_lock.unlock();

		return (EPROTO);
	}

	// Form the message
	info = (struct pathend_udp_info *)udp_send_buf;
	bzero(info, sizeof (struct pathend_udp_info));
	info->hdrver	= PATHEND_UDP_INFO_VERSION;
	info->hdrlen	= (uint32_t)sizeof (struct pathend_udp_info);
	info->msglen	= info->hdrlen + (uint32_t)version_len;
	info->msgtype	= ACTIVE_REQUEST;
	info->nodeid	= orb_conf::current_id_node().ndid;
	info->nodeinc	= orb_conf::current_id_node().incn;
	info->hs_seq	= ++hs_seq;
	info->compatible	= 1;
	info->version_offset	= info->hdrlen;
	info->version_len	= (uint32_t)version_len;
	info->zero		= 0;
	info->sc_version	= SC_VERSION_STR;

	//
	// If the message turned out to be smaller than the SC3.0 udp
	// handshake message, pad it to that length so that if the peer
	// happens to be running 3.0 or one its updates, it does not
	// outright reject this message but rather rejects it after
	// printing a useful message.
	//
	if (info->msglen < PATHEND_UDP_INFO_30_LENGTH) {
		bzero(udp_send_buf + info->msglen,
		    (size_t)(PATHEND_UDP_INFO_30_LENGTH - info->msglen));
		info->msglen = PATHEND_UDP_INFO_30_LENGTH;
	}

	// Send the message
	bzero(&sin, sizeof (sin));
	sin.sin_family = AF_INET;
	sin.sin_port = htons(UDP_PE_PORT);
	sin.sin_addr.s_addr = remote_ipaddr;
	rlen = tcptr_sendto(udp_so, info, (long)info->msglen, 0,
	    (struct sockaddr *)&sin, (socklen_t)sizeof (sin), &error);
	ASSERT((error != 0) || (rlen == (int)info->msglen));

	udp_send_buf_lock.unlock();

	if (error != 0) {
		UDP_DBPRINTF(("PE %p peer %d laid %d udp_server_initiate"
		    " could not send active request.\n", this, node().ndid,
		    local_adapter_id()));
		return (error);
	}

	UDP_DBPRINTF(("PE %p peer %d laid %d udp_server_initiate"
	    " active request sent seq %u.\n", this, node().ndid,
	    local_adapter_id(), info->hs_seq));

	// Set the state so that we can receive a response
	udp_msg_lock();
	set_udp_msg_state(PE_TCPTR_UDPMSG_CAN_RECV_RESP);
	udp_msg_unlock();

	//
	// Wait for reply from the client. Need to wait since reply
	// will carry version compatibility result and peer's MAC
	// address and incarnation.
	//

	if ((error = udp_getq(TCPTR_UDPSERVER_TIMEOUT)) == ETIME) {
		//
		// Check pathend state if need to return earlier. If pathend
		// state changes we return EPROTO instead of ETIME.
		//
		lock();
		if ((get_state() != P_CONSTRUCTED) &&
		    (get_state() != P_INITERR)) {
			unlock();
			return (EPROTO);
		}
		unlock();
	}
	return (error);
}

//
// void
// tcp_pathend::recv_initial_msg(mblk_t *bp)
//	Receive the initial message from the peer pathend.
//
// Description:
//	This routine is called by tcpmod read side put procedure
//	once it has the initial message from the peer pathend.
//	The tcpmod has been given the pathend object pointer.
//
//	This function sets the peer connection number and the
//	fact that the initial message has been received and
//	wakes up the initiate() routine which is waiting for this
//	message.
//
// Parameters:
//	mblk_t *bp	- message containing the initial message.
//
// Returns:
//	None. Wakes up the process waiting for this message, if needed.
//
// See Also:
//	send_initial_msg().
//	tcp_pathend::timed_wait_for_msg().
//
//
void
tcp_pathend::recv_initial_msg(mblk_t *bp)
{
	struct init_msg info;
	int logit = 0;
	uint_t n_conn = 0, conn = 0;

	ASSERT(bp != NULL);

	// Verify that the message is long enough
	ASSERT(msgdsize(bp) >= sizeof (struct init_msg));

	// Copy from mblks onto stack the constant part init_msg
	(void) mp_to_buf(bp, (char *)&info, sizeof (struct init_msg));

	freemsg(bp);

	TCP_DBPRINTF(("tpe::recv_initial_msg from %u:%u\n",
		info.src.ndid, info.src.incn));

	tpe_lock();
	// If the incoming connection matches what we are waiting for
	// then set the flag and signal the waiting thread
	// If not, neglect the incoming message and the waiting thread
	// will wake up and consider it a timeout and restart the connection
	// setups
	if (info.connection == n_connections) {
		set_incn(info.src.incn);
		set_msg_received();
	} else {
		logit = 1;
		n_conn = n_connections;
		conn = info.connection;
	}
	tpe_unlock();
	// log after dropping the lock
	if (logit)
		TCP_DBPRINTF(("connection mismtach local %d remote %d\n",
		    n_conn, conn));
}


//
// int
// tcp_pathend::timed_wait_for_msg()
//	Wait for the initial msg from the peer pathend.
//
// Description:
//	This routine waits for the initial message from the
//	remote pathend. It timeouts after TCPTR_INITIAL_MSG_WAIT
//	microsecs.
//
// Parameters:
//	None.
//
// Returns:
//	bool whether message has been received.
//
// See Also:
//	send_initial_msg().
//	tcp_pathend::recv_initial_msg().
//
bool
tcp_pathend::timed_wait_for_msg()
{
	tpe_lock();
	if (!has_msg_been_received()) {
		os::systime timo(TCPTR_INITIAL_MSG_WAIT);
		(void) tpe_msg_cv.timedwait(tpe_get_lock(), &timo);
	}
	tpe_unlock();

	return (has_msg_been_received());
}


//
// struct sonode *
// tcp_pathend::so_getq()
//	Get the socket connected from the client.
//
// Description:
//	This function is invoked by the server node to get the
//	connected socket from the client. It waits for a
//	timeout interval and returns the connected socket if
//	successful.  It returns NULL otherwise.
//
// Parameters:
//	None.
//
// Returns:
//	socket (struct sonode *) if successful. NULL otherwise.
//
// See Also:
//	tcp_pathend::so_putq().
//
//
struct sonode *
tcp_pathend::so_getq()
{
	struct sonode *so;
	tpe_lock();
	ASSERT(tpe_state == TPE_INITIATED);
	if (in_so_ptr == NULL) {
		os::systime timo((os::usec_t)TCPTR_TCPSOCKET_WAIT);
		(void) tpe_cv.timedwait(tpe_get_lock(), &timo);
	}
	so = in_so_ptr;
	in_so_ptr = NULL;
	tpe_unlock();
	return (so);
}


//
// int
// tcp_pathend::so_putq(struct sonode *so)
//	Place the connected socket in the queue and wakeup the
//	process waiting for it.
//
// Description:
//	This routine is called to place the connected socket
//	in the waiting pathend's queue. The waiting process
//	is signaled.
//
// 	socket is closed by the caller in the case of an error.
//
// Parameters:
//	struct sonode *so	- connected socket
//
// Returns:
//	0 on success and errno otherwise.
//
// See Also:
//	tcp_pathend::so_getq().
//
int
tcp_pathend::so_putq(struct sonode *so)
{
	tpe_lock();
	if ((tpe_state != TPE_INITIATED) ||
	    (in_so_ptr != NULL)) {
		tpe_unlock();
		return (EPROTO);
	}
	in_so_ptr = so;
	signal_tpe();
	tpe_unlock();

	return (0);
}


//
//
// int
// tcp_pathend::process_accept(struct sonode *so, bool server)
//	Process the connected sockets and make them reliable
//	datagram connections.
//
// Description:
//	This function is called both by the client and server
//	to process connected sockets.
//
//	It sets the MTU for the connection.
//	It also sets the write offset to leave room for headers.
//
//	It sets the TCP_NODELAY option so that any message will be
//	sent to the remote node immediately by TCP (otherwise TCP
//	tends to aggregate until sufficient size is reached).
//
//	It pushes tcpmod onto the connection (in the case of
//	server. In the case of client, it is pushed at the time
//	of socket creation. This is because of the way accept()
//	works). It also exchanges message with the peer. This helps
//	ensure that the a reliable datagram connection is fully
//	formed before higher layer messages are exchanged.
//
//	It verifies that the connection number matches with the peer.
//	It updates various information and in the case of the last
//	connection, updates the state.
//
//	Note that tcp_pathend has only one connection.
//	It can be thought of as the control/monitor connection.
//
//	Returns 0 on success and error code on error.
//
// Parameters:
//	struct sonode *so	- connected socket
//	bool server		- whether this is server.
//
// Returns:
//	0 on success. error code otherwise.
//
// See Also:
//	set_connect_client().
//
//
//
int
tcp_pathend::process_accept(struct sonode *so, bool server)
{
	int error;
	uint_t connection_no;
	tcp_pe_state log_tpe_state;

	TCP_DBPRINTF(("tpe: From node %d adapter %d connection# %d state %d\n",
		node().ndid, local_adapter_id(), n_connections + 1, tpe_state));

	tpe_lock();
	if (tpe_state != TPE_INITIATED) {
		log_tpe_state = tpe_state;
		tpe_unlock();
		//
		// Path may have been removed
		//
		TCP_DBPRINTF(("process_accept: Wrong state %d\n",
		    log_tpe_state));
		return (EPROTO);
	}
	connection_no = n_connections;
	tpe_unlock();

	// First TCP_NODELAY
	int binary = 1;
#if	SOL_VERSION >= __s11
	ksocket_t	ksp;
	ksp = SOTOKS(so);
	error = ksocket_setsockopt(ksp, IPPROTO_TCP, TCP_NODELAY,
	    (void *)&binary, (t_uscalar_t)sizeof (binary), CRED());
#else	// SOL_VERSION >= __s11
	error = sosetsockopt(so, IPPROTO_TCP, TCP_NODELAY, (void *)&binary,
		(t_uscalar_t)sizeof (binary));
#endif	// SOL_VERSION >= __s11
	if (error) {
		TCP_DBPRINTF(("tpe::process_accept: Error in set "
				"nodelay %d\n", error));
		return (error);
	}

	//
	// Push tcpmod for server. For client it happens before
	// soconnect().
	//

	if (server) {
		error = tcpmod_push(so, TCP_PATHEND, (void *)this,
		    connection_no);
		if (error != 0) {
			TCP_DBPRINTF(("Failed to push tcpmod %d\n", error));
			return (error);
		}
	}

	// Do the conversion from sonode to queue after doing tcpmod_push
	queue_t *q = WR((strvp2wq(SOTOV(so)))->q_next);
	ASSERT(q != NULL);

	//
	// Do the client/server handshake. client/server are mirror images
	// of each other.
	//
	if (server) {
		send_initial_msg(q, connection_no, TCP_PE_INITIAL_MSG);
		if (!timed_wait_for_msg()) {
			TCP_DBPRINTF(("tpe_process_accept: Server 2 failed\n"));
			return (ETIME);
		}
	} else {
		if (!timed_wait_for_msg()) {
			TCP_DBPRINTF(("tpe_process_accept: client 1 failed\n"));
			return (ETIME);
		}
		send_initial_msg(q, connection_no, TCP_PE_INITIAL_MSG);
	}

	tpe_lock();
	if (tpe_state != TPE_INITIATED) {
		tpe_unlock();
		return (EPROTO);
	}

	so_ptr = so;
	so_wq = q;
	ASSERT(connection_no == n_connections);
	n_connections++;
	tpe_state = TPE_CONNECTED;
	tpe_unlock();

	TCP_DBPRINTF(("tpe::process_accept:connections setup with "
			"node %d, so_wq %p\n", node().ndid, so_wq));

	return (0);
}


//
// TCP PATHEND INITIATE Routines:
//
//
//
// int
// tcp_pathend::client_initiate()
//	Initiate pathend connection as a client.
//
// Description:
//	This routine initiates a tcp connection to the remote pathend.
//
//	In order to make sure that the other side is up and running,
//	it calls udp_client_initiate(), which waits for the udp
//	handshake to complete.
//
//	It then calls set_connect_client() to initiate the tcp connection
//	to the server. The module "tcpmod" is pushed onto the socket
//	as soon as it is created.
//
//	Once the tcp connection is established, it calls
//	process_accept() to process the connection.
//
//	Note that only one connection is established for each
//	tcp pathend.
//
// Parameters:
//	None.
//
// Returns:
//	0 on success. error code otherwise.
//
// See Also:
//	tcp_pathend::udp_client_initiate(), set_connect_client(),
//	tcp_pathend::process_accept().
//
//
int
tcp_pathend::client_initiate()
{
	struct sonode *so;
	int	error;

	//
	// If remote nodeid is greater than your node number, be the client
	//
	ASSERT(orb_conf::local_nodeid() < node().ndid);

	if ((error = udp_client_initiate()) != 0) {
		return (error);
	}

	tpe_lock();
	tpe_state = TPE_INITIATED;
	tpe_unlock();

	TCP_DBPRINTF(("tpe::client_initiate:Trying to setup connections "
			"with node %d\n", node().ndid));

	//
	// call set_connect_client() until all the connections are
	// established.  A connection may
	// not be established because the client may be started
	// before the server. So, caller will to keep retrying until the
	// the connection is established
	// soconnect() sleeps and hence no explictit sleep is necessary here.
	//
	so = set_connect_client(remote_ipaddr,
	    TCP_PE_PORT, (void *)this, TCP_PATHEND, 0, &error);
	if (so == NULL)
		return (error);

	if ((error = process_accept(so, false)) != 0) {
#if	SOL_VERSION >= __s11
		ksocket_t ksp = SOTOKS(so);
		ksocket_close(ksp, CRED());
#else	// SOL_VERSION >= __s11
		(void) sodisconnect(so, -1, 0);
		soclose(so);
#endif	// SOL_VERSION >= __s11
	}
	return (error);
}


//
// int
// tcp_pathend::server_initiate()
//	Initiate connection from the server side.
//
// Description:
//	This function is called by initiate() in the case of
//	server nodes (higher numbered nodes) to start the
//	pathend connection.
//
//	It calls udp_server_initiate(), which initiates the
//	udp handshake and waits for it to complete.
//
//	It then waits for the tcp connection from the client.
//	When the client connects, it is queued to this routine
//	by the tcp_transport::start_server_pe() routine.
//	This function calls process_accept() to complete the
//	connection.
//
//	Note that so_ptr in this case is set by the so_putq().
//	so_ptr is initialized to NULL in the constructor.
//
// Parameters:
//	None.
//
// Returns:
//	0 on success. error code otherwise.
//
// See Also:
//	tcp_pathend::udp_server_initiate().
//	tcp_pathend::so_getq().
//	tcp_pathend::process_acccept().
//
int
tcp_pathend::server_initiate()
{
	struct sonode *so;
	int error;

	if ((error = udp_server_initiate()) != 0) {
		return (error);
	}

	tpe_lock();
	tpe_state = TPE_INITIATED;
	tpe_unlock();

	if ((so = so_getq()) == NULL) {
		return (ETIME);
	}

	if ((error = process_accept(so, true)) != 0) {
#if	SOL_VERSION >= __s11
		ksocket_t ksp = SOTOKS(so);
		ksocket_close(ksp, CRED());
#else	// SOL_VERSION >= __s11
		(void) sodisconnect(so, -1, 0);
		soclose(so);
#endif	// SOL_VERSION >= __s11
	}
	return (error);
}


//
//
// void
// tcp_pathend::initiate(char *&sbuf, char *&rbuf, incarnation_num &incn)
//	Initiate the tcp pathend connection to the remote node.
//
// Description:
//	Each tcp pathend object can be thought of as a control/monitor
//	channel between two nodes. The tcp_pathend::initiate() is the
//	first method called in tcp transport to establish a path
//	between 2 nodes.
//
//	If the remote node number is greater than this node number,
//	act as a client. Else act as a server.
//
//	udp socket is created to send message to the peer (if this is
//	a server). The socket is destroyed at the end.
//	NOTE: Make sure that udp_finish() is called corresponding
//		to the udp_init() before leaving this routine.
//
//	The incarnation number is exchanged.
//
//	The send and receive buffers for monitoring messages are
//	initialized.
//	This routine tries to establish the connection to the remote
//	node as long as the tcp pathend state is P_CONSTRUCTED or
//	P_INITERR.
//
//	Calls _cleanup() to handle any intermediate failures.
//
// Parameters:
//	char *&sbuf 		- send buf ptr reference
//	char *&rbuf 		- receive buf ptr reference
//	incarnation_num &incn	- incarnation number reference
//
// Returns:
//	None.
//
void
tcp_pathend::initiate(char *&sbuf, char *&rbuf, incarnation_num &incn)
{
	int error = 0;

	ASSERT(tpe_state == TPE_RESET);
	ASSERT(lock_held());
	ASSERT(get_state() == P_CONSTRUCTED);

	TCP_DBPRINTF(("In tpe_initiate on node %d adapter %d rnode %d "
		"state %d\n", orb_conf::local_nodeid(), local_adapter_id(),
		    node().ndid, get_state()));

	while ((get_state() == P_CONSTRUCTED) || (get_state() == P_INITERR)) {
		unlock();
		error = udp_init();
		tpe_update_pe_state(error);
		if (error == 0) {
			// Success. Break out of the udp init loop and
			// proceed with the rest of the initialization.
			lock();
			break;
		}
		// udp_init fails if a UDP socket cannot be
		// created. This is unlikely to be fixed by retrying
		// XX It would be better if we return
		// from initiate() in P_INITERR state to the
		// generic pathend state machine
		// Instead wait for delete_notify to signal a state change
		// due to the path being removed, i.e., we wait for admin
		// to remove the faulty path using scconf
		tpe_lock();
		tpe_cv.wait(tpe_get_lock());
		tpe_unlock();
		lock();
	}

	while ((get_state() == P_CONSTRUCTED) || (get_state() == P_INITERR)) {
		unlock();
		ASSERT(tpe_state == TPE_RESET);
		udp_reset();

		// Raw dlpi transport initialization. Open a "connection",
		// the dlpi device will be opened if need be.
		// raw_dlpi_conn::open calls are idempotent, thus we can
		// afford to call it in every iteration of this loop. Note
		// that tcp_pathend::_cleanup does not undo anything
		// done by this call to open.
		error = rdconnp->open(orb_conf::local_nodeid(),
		    orb_conf::local_incarnation(), node().ndid);

		if (error == 0) {
			if (orb_conf::local_nodeid() < node().ndid) {
				error = client_initiate();
			} else {  // Server
				error = server_initiate();
			}
		}
		if (error == 0) {
			tpe_update_pe_state(0);
			lock();
			sbuf = sendbufp;
			rbuf = recvbufp;
			incn = tpe_incn;

			//
			// Zero out the rbuf so PM does not see
			// any stale messages
			bzero(rbuf, (size_t)PM_MESSAGE_SIZE);
			break;
		}
		_cleanup();
		tpe_update_pe_state(error);
		lock();
	}

	// We can be here if
	// (i) all the connections above were opened successfully, or
	// (ii) the pathend is no longer in the P_CONSTRUCTED/P_INITERR state
	// In case (ii) we do not have to proceed with any more setup.

	if (get_state() == P_CONSTRUCTED || get_state() == P_INITERR) {
		// Set raw dlpi connection information. The remote MAC address
		// is now available, the following call will just store the
		// local and the remote nodeid and prepare a template mblk
		// based on this information.
		rdconnp->connect();
	}

	unlock();
	// Close udp socket
	udp_finish();
	lock();

	TCP_DBPRINTF(("Done tpe_initiate on node %d adapter %d rnode %d "
		"state %d\n", orb_conf::local_nodeid(), local_adapter_id(),
		    node().ndid, get_state()));
}

//
// void
// tcp_pathend::pm_recv_internal(mblk_t *bp)
//	Receive message from the transport and copy it into the
//	receive buffer.
//
// Description:
//	This function is called by tcpmod as soon as it receives
//	the internal monitoring message from the other end.
//	This function atomically copies the received message
//	into the receive buffer (initialized in initiate()).
//
// Parameters:
//	mblk_t *bp	- received streams message
//
// Returns:
//	None.
//	Atomically copies the received message into pathend receive
//	buffer.
//
// See Also:
//	tcp_pathend::pm_send_internal()
//
//
void
tcp_pathend::pm_recv_internal(mblk_t *bp)
{
	size_t len;

	ASSERT(bp != NULL);

	len = mp_to_buf(bp, recv_copy_bufp, (size_t)PM_MESSAGE_SIZE);
	freemsg(bp);
	ASSERT(len == PM_MESSAGE_SIZE);

	if (!pm_isnewer(recv_copy_bufp)) {
		// Stale heartbeat, drop it
		HB_DBG(this, ("R(%x) dropped stale(%016llx)\n",
		    curthread->t_pil, *((uint64_t *)recv_copy_bufp)));
		return;
	}
	atomic_copy_64((uint64_t *)recvbufp, (uint64_t *)recv_copy_bufp);
	UPDATE_TCP_STATS(hb_recvd, 1);
	if (!(curthread->t_flag & T_INTR_THREAD)) {
		UPDATE_TCP_STATS(non_intr_hb_recvd, 1);
		// Also syslog a message stating that this path is receiving
		// non interrupt heartbeats, unless one such message has
		// already been logged.
		if (!rcvd_nonintr_hb) {
			os::sc_syslog_msg msg("TCP TRANSPORT", "", NULL);
			//
			// SCMSGS
			// @explanation
			// Solaris Clustering requires network drivers to
			// deliver heartbeat messages in the interrupt
			// context. A heartbeat message has unexpectedly
			// arrived in non interrupt context.
			// @user_action
			// Check if the right version of the driver is in use.
			//
		    if (os::strstr(get_extname(), "vnet") != NULL) {
			PM_DBG(("Received non interrupt heartbeat on %s -"
				" path timeouts are likely.\n", get_extname()));
		    } else {
			(void) msg.log(SC_SYSLOG_WARNING, MESSAGE,
				"Received non interrupt heartbeat on %s -"
				" path timeouts are likely.", get_extname());
		    }
		}
		rcvd_nonintr_hb = true;
	}
	HB_DBG(this, ("R(%x) tcprecv(%016llx)\n", curthread->t_pil,
	    *((uint64_t *)recv_copy_bufp)));
#ifdef	USE_TICKSTATS
	recv_tick_stat.update();
#endif	// USE_TICKSTATS
}


//
//
// void
// tcp_pathend::pm_send_internal()
//	Send tcppathend internal monitoring message.
//
// Description:
//	This routine is called by the path manager to send
//	control/monitoring message to the remote node.
//
//	It copies the already allocated send message.
//
//	It atomically copies the message from the send buffer
//	and sends the message on the tcp pathend connection.
//	The tcpmod header and other static information are
//	initialized and kept in the pre-allocated message.
//
// Parameters:
//	None.
//
// Returns:
//	None.
//
// See Also:
//	tcp_pathend::tcp_pathend() - constructor.
//	tcp_pathend::pm_recv_internal().
//
void
tcp_pathend::pm_send_internal()
{
	ASSERT(lock_held());

	if (tpe_state == TPE_CONNECTED) {
		// Send the heartbeat only if the state is connected
		HB_DBG(this, ("S(%x) tcpsend(%016llx)\n", curthread->t_pil,
			*((uint64_t *)sendbufp)));
#ifdef	USE_TICKSTATS
		send_tick_stat.update();		// update tick stats
#endif
		os::hrtime_t	tstart, diff;
		tstart = os::gethrtime();
		uint64_t senddata;

		atomic_copy_64(&senddata, (uint64_t *)sendbufp);
		unlock();
		rdconnp->send((char *)&senddata);
		lock();
		diff = os::gethrtime() - tstart;
		if (diff > 10000000) {
			// print warning if longer than 10ms
			HB_DBG(this, ("S(%x) tcpsend delay %llu\n",
			    curthread->t_pil, diff));
		}
		HB_DBG(this, ("S(%x) tcpsend putnext done(%016llx)\n",
		    curthread->t_pil, *((uint64_t *)sendbufp)));
	} else {
		HB_DBG(this, ("S(%x) did not send(%016llx)\n",
			curthread->t_pil, *((uint64_t *)sendbufp)));
	}
}

//
// void tcp_pathend::tpe_update_pe_state(int)
//
// This routine is called by pathend initiate routines when they
// detect something wrong that should be brought to the operator's
// attention and also when the error condition clears up. The routine
// logs informative messages to syslog. It also changes the state of the
// pathend from P_CONSTRUCTED to P_INITERR if it is an error case. Must
// be called without lock.
//
// Returns: none
//
void
tcp_pathend::tpe_update_pe_state(int error)
{
	os::hrtime_t		curtime;
	os::sc_syslog_msg	msg("TCP TRANSPORT", "", NULL);

	ASSERT(!lock_held());
	if (error == 0) {
		//
		// We do not have to put the pathend back into the
		// P_CONSTRUCTED state, as a return from initiate will
		// move it P_UP anyway. Besides P_INITERR is treated
		// exactly the same as P_CONSTRUCTED for state transitions.
		//
		// No need to log anything either. A path online message
		// will be logged by the pathend state proxy when
		// we return from initiate.
		//
		last_warn_time = 0;
		first_err_time = 0;
		return;
	}

	//
	// Error condition. Put the pathend in the P_INITERR state so user
	// can take any appropriate action to fix the problem. Also log a
	// message if enough time has passed since the first error was
	// detected.
	//
	curtime = os::gethrtime();

	if (first_err_time == 0) {
		//
		// First time the error has been detected, give the pathend
		// some time to recover from error before changing the state
		// and/or logging a message.
		//
		first_err_time = curtime;
		return;
	}

	if ((curtime - first_err_time) / 1000 <
	    TCPTR_PATHEND_INITIATE_TIMEOUT) {
		//
		// Not enough time has elapsed since the error was detected.
		// Do not do anything yet.
		return;
	}

	//
	// The pathend could not recover within the stipulated timeout.
	// Put it in the INITERR state unless it is already in there.
	//
	lock();
	init_error();
	unlock();
	//
	// If we have not logged a message, log one.
	//
	if (last_warn_time == 0) {
		last_warn_time = curtime;
		//
		// SCMSGS
		// @explanation
		// Communication with another node could not be established
		// over the path.
		// @user_action
		// Any interconnect failure should be resolved, and/or the
		// failed node rebooted.
		//
		(void) msg.log(SC_SYSLOG_WARNING, MESSAGE,
				"Path %s initiation encountered errors,"
				" errno = %d. Remote node may be down or"
				" unreachable through this path.",
				get_extname(), error);
	}
}

//
// tcp_pathend::record_bad_udp_handshake
//
// This method is called when the transport receives a udp handshake
// message that has an unrecognized format. The method determines if
// it has been long enough since the last bad handshake message was
// received. This heuristic is used to determine whether this handshake
// message can potentially be from a rebooted peer. If the message arrives
// after a long duration, the method returns true which the caller treats
// as an indication that is should log a new warning.
//
bool
tcp_pathend::record_bad_udp_handshake()
{
	clock_t		current_lbolt = ddi_get_lbolt();
	bool		possibly_from_new_incarnation = false;

	//
	// UDP handshakes are tried often enough that a silence of
	// 30 seconds can be treated as a new reboot. It should be
	// safe to assume that a reboot takes at least 30 seconds.
	// If the assumption is wrong either way, the worst that
	// can happen is that either a spurious messages will get
	// logged, or a repeat message that should have been logged,
	// will be skipped.
	//
	if (current_lbolt - bad_udp_handshake_lbolt > 3000)
		possibly_from_new_incarnation = true;
	bad_udp_handshake_lbolt = current_lbolt;
	return (possibly_from_new_incarnation);
}

//
// TCP ADAPTER Routines
//
//
// tcp_adapter::tcp_adapter(tcp_transport *tp, clconf_adapter_t *ap) :
//					adapter(tp, ap)
//	tcp_adapter class constructor.
//
// Description:
//	There is no specific information/actions needed to be done in
//	tcp_adapter
//
// Parameters:
//	tcp_transport *tp	- pointer to tcp transport class object.
//	clconf_adapter_t *ap	- pointer to clconf adapter object.
//
// Returns:
//	None.
//
//
tcp_adapter::tcp_adapter(tcp_transport *tp, clconf_adapter_t *ap) :
	adapter(tp, ap)
{
	// Let the raw dlpi transport know of this adapter
	raw_dlpi::the().add_adapter(ap);
}


//
// tcp_adapter::~tcp_adapter()
//	tcp_adapter class destructor
//
// Parameters:
//	None.
//
// Returns:
//	None.
//
//
tcp_adapter::~tcp_adapter()
{
	// Remove this adapter from the list maintained by the raw
	// dlpi transport.
	raw_dlpi::the().remove_adapter(get_adapter_id());
}

//
// TCP ENDPOINT routines
//
//
// tcp_endpoint::tcp_endpoint(tcp_transport *tr, tcp_pathend *pe)
//
//	TCP Endpoint Constructor.
//
// Description:
//	Each path in tcp transport consists of a pathend
//	(for control and monitoring purposes) and an endpoint
//	(for sending/receiving higher layer messages e.g. pxfs).
//
//	The endpoint has several tcp connections associated
//	with it. Each tcp connection has a module (tcpmod)
//	which is used to reconstruct datagrams.
//
//	This routine is the constructor for the tcp endpoint.
//	It initializes the state, port numbers, number of
//	connections etc. It also initializes  queues for sockets
//	and the corresponding streams queue pointers.
//
//	XXX:
//	The way udp ports are handled will go away as an rfe
//	is fixed.
//
// Parameters:
//	tcp_transport *tr	- pointer to tcp transport object.
//	tcp_pathend   *pe	- pointer to the pathend object.
//
// Returns:
//	None.
//
//
tcp_endpoint::tcp_endpoint(tcp_transport *tr, tcp_pathend *pe) :
	endpoint(tr, pe),
	tep_state(TEP_RESET),
	timeouts_disabled(false),
	do_esballoc(false),
	n_connections(0),
	udp_so(NULL),
	tep_udp_msg_received(EP_TCPTR_UDPMSG_CANNOT_RECV),
	mtu_size(0),
	wroff(0),
	last_send_lbolt(0)
{
	int i;

	// set so_ptr/so_wstream to NULL
	for (i = 0; i < TCP_EP_MAX_CONNECTIONS; i++) {
		in_so_ptrs[i] = NULL;
		so_ptrs[i] = NULL;
		so_wqs[i] = NULL;
		so_rmsgs[i] = false;
	}

	//
	// Producer index and consumer index for incoming connections.
	//
	in_pindex = in_cindex = 0;

	local_ipaddr	= pe->get_local_ipaddr();
	remote_ipaddr	= pe->get_remote_ipaddr();
	do_esballoc	= pe->get_do_esballoc();

	// Assuming peer is running 3.1 or later
	use_rio_acks = tcp_transport_use_rio_acks ? true : false;
	max_conns = 4;
	curr_rio_index = rio_base = 2;
	ASSERT(max_conns <= TCP_EP_MAX_CONNECTIONS);
}


//
//
// tcp_endpoint::~tcp_endpoint()
//	tcp endpoint destructor.
//
// Description:
//	Resets state and other information.
//	Closes all tcp connections.
//
// Parameters:
//	None.
//
// Returns:
//	None.
//
// See Also:
//	tcp_endpoint constructor.
//
//
tcp_endpoint::~tcp_endpoint()
{
	uint_t i;

	for (i = 0; i < max_conns; i++) {
		so_wqs[i] = NULL;
		ASSERT(in_so_ptrs[i] == NULL);
		if (so_ptrs[i] != NULL) {
			soclose(so_ptrs[i]);
			so_ptrs[i] = NULL;
		}
	}

	ASSERT(pathendp == NULL);
	ASSERT(udp_so == NULL);
}


//
// tcp_endpoint::unblock()
//	Unblock the tcp_endpoint::initiate().
//
// Description:
//	This routine is called whenever an endpoint is going away
//	for any reason. This routine can signal any waiting process
//	so that initiate() can be terminaed immediately.
//
// Assertions:
//	speeds up the termination of initiate().
//	However, generic transport can call cleanup() only after initiate()
//	returns.
//
// Parameters:
//	None.
//
// Returns:
//	None.
//
//
void
tcp_endpoint::unblock()
{
	ASSERT(lock_held());
	unlock();

	tep_lock();
	// We should not be here in state INITIATED as there could be
	// threads blocked in initiate that we do not wake up here
	// If there are, then we can do a tep_cv.broadcast() here
	ASSERT(tep_state != TEP_INITIATED);
	tep_state = TEP_RESET;

#ifdef PER_SENDSTREAM_CV
	// In the case of per sendstream cv, each thread wakes up periodically
	// to check if the endpoint state has change and will notice that the
	// endpoint has been unregistered
#else
	// Wake up any threads waiting for synchronous sends/replyios
	ack_cv.broadcast();
#endif
	tep_unlock();
	lock();
}


//
// void
// tcp_endpoint::_cleanup()
// 	tcp endpoint Internal cleanup
//
// Description:
//	This is the internal cleanup routine for tcp endpoint.
//	This is called whenever an error occurs on a tcp endpoint
//	and needs cleaning up.
//
//	Resets the state. Closes/Disconnects the sockets.
//
// Parameters:
//	None.
//
// Returns:
//	None.
//
// See Also:
//	tcp_endpoint::cleanup().
//	tcp_endpoint::initiate().
//
//

void
tcp_endpoint::_cleanup()
{
	uint_t		i;
	endpoint_state	old_state;
	struct sonode	*so;

	//
	// synchronize with tcp_endpoint::set_timeouts
	//
	so_lock.lock();

	tep_lock();
	old_state = tep_state;
	tep_state = TEP_RESET;
	//
	// close all incoming connections.
	// disconnect all existing connections.
	// closing all existing connections is done in
	// ~tcp_endpoint as it will be called after all users (send)
	// are done with the connections.
	// Avoid doing the sodisconnect/soclose while holding lock
	//
#if	SOL_VERSION >= __s11
	ksocket_t	ksp;
#endif	// SOL_VERSION >= __s11
	for (i = 0; i < max_conns; i++) {

		so_rmsgs[i] = false;

		if (in_so_ptrs[i] != NULL) {
			so = in_so_ptrs[i];
			in_so_ptrs[i] = NULL;
			tep_unlock();
#if	SOL_VERSION >= __s11
			ksp = SOTOKS(so);
			ksocket_close(ksp, CRED());
#else	// SOL_VERSION >= __s11
			(void) sodisconnect(so, -1, 0);
			soclose(so);
#endif	// SOL_VERSION >= __s11
			tep_lock();
		}
		if (so_ptrs[i] != NULL) {
			tep_unlock();
#if	SOL_VERSION >= __s11
			ksp = SOTOKS(so_ptrs[i]);
			ksocket_shutdown(ksp, SHUT_RDWR, CRED());
#else	// SOL_VERSION >= __s11
			(void) sodisconnect(so_ptrs[i], -1, 0);
#endif	// SOL_VERSION >= __s11
			tep_lock();
		}
		//
		// The communication sockets have been disconnected. However,
		// unless _cleanup has been called midway through initiate
		// to abort the initiate, there could be pending sends.
		// Defer closing the sockets and nulling out the so_wqs
		// array to the tcp_endpoint destructor in not initiate
		// cases.
		//
		if (old_state == TEP_INITIATED) {
			if (so_ptrs[i] != NULL) {
				so = so_ptrs[i];
				so_ptrs[i] = NULL;
				so_wqs[i] = NULL;
				tep_unlock();
#if	SOL_VERSION >= __s11
				ksp = SOTOKS(so);
				ksocket_close(ksp, CRED());
#else	// SOL_VERSION >= __s11
				soclose(so);
#endif	// SOL_VERSION >= __s11
				tep_lock();
			}
		}
	}
	in_pindex = 0;
	in_cindex = 0;
	n_connections = 0;

	tep_unlock();
	so_lock.unlock();
}


//
// tcp_endpoint::cleanup()
//	cleanup after an initiate().
//
// Assertions:
//	Expects initiate() to have completed.
//
// Descriptions:
//	Calls _cleanup() to get the work done.
//	Also, releases pathend
//
// Parameters:
//	None.
//
// Returns:
//	None.
//
// See Also:
//	tcp_endpoint::_cleanup().
//
void
tcp_endpoint::cleanup()
{

	ASSERT(lock_held());
	unlock();
	// cleanup() may be called if the path is declared down from
	// initiate() state. In that case wake up any threads that
	// may be stuck waiting in tcp_endpoint. This could be a
	// thread initiate that called pathend::fail or a server side
	// thread waiting.
	tep_lock();
	signal_tep();
	tep_unlock();
	_cleanup();
	lock();
	release_pathend();
}

//
// tcp_endpoint::process_tcp_reset()
//
// This routine is called when tcpmod sees one of the tcp connections
// belonging to this endpoint go down.
//
void
tcp_endpoint::process_tcp_reset()
{
	//
	// If the endpoint is already reset, we might be receveing
	// a RST call for another connection. Ignore the request.
	//
	tep_lock();
	if (tep_state == TEP_RESET) {
		tep_unlock();
		return;
	}
	tep_unlock();

	// Tell the associated pathend to tear down the path and
	// reestablish it, so that we can recover from the dropped
	// connection.
	pathendp->mark_ep_faulted();

	// If we are in the middle of initiate, change the state to
	// TEP_RESET so that the initiate effort is abondoned.
	tep_lock();
	if (tep_state == TEP_INITIATED) {
		tep_state = TEP_RESET;
	}
	tep_unlock();
}

int
tcp_endpoint::udp_init()
{
	int error;

	udp_so = create_udp_socket(local_ipaddr, 0, &error);
	if (udp_so == NULL) {
		return (error);
	}

	udp_msg_lock();
	set_udp_msg_state(EP_TCPTR_UDPMSG_CAN_RECV);
	udp_msg_unlock();

	return (0);
}


void
tcp_endpoint::udp_finish()
{
	udp_msg_lock();
	set_udp_msg_state(EP_TCPTR_UDPMSG_CANNOT_RECV);
	udp_msg_unlock();

	if (udp_so != NULL) {
		close_udp_socket(udp_so);
		udp_so = NULL;
	}

}

void
tcp_endpoint::udp_putq(struct endpoint_udp_info *info, struct sockaddr_in *sin)
{

	if (sin->sin_addr.s_addr != remote_ipaddr) {
		TCP_DBPRINTF(("tep::udp_putq: Remote address 0x%x does not "
			"match 0x%x\n", sin->sin_addr.s_addr, remote_ipaddr));
		return;
	}

	if (!ID_node::match(info->node, get_rnode())) {
		TCP_DBPRINTF(("tep::udp_putq: Remote node %u:%u does not match "
		    "%u:%u\n", info->node.ndid, info->node.incn,
		    get_rnode().ndid, get_rnode().incn));
		return;
	}

	udp_msg_lock();
	if (get_udp_msg_state() == EP_TCPTR_UDPMSG_CAN_RECV) {
		set_udp_msg_state(EP_TCPTR_UDPMSG_RECEIVED);
		udp_msg_signal();
	}
	udp_msg_unlock();
}

// Waits for udp message or timeout
// Returns 0 if udp state indicates message received, else returns ETIME
int
tcp_endpoint::udp_getq(clock_t timval)
{
	udp_msg_lock();
	if (get_udp_msg_state() != EP_TCPTR_UDPMSG_RECEIVED) {
		os::systime timo(timval);
		(void) tep_udp_msg_cv.timedwait(&tep_udp_msg_lock, &timo);
		if (get_udp_msg_state() != EP_TCPTR_UDPMSG_RECEIVED) {
			udp_msg_unlock();
			return (ETIME);
		}
	}
	udp_msg_unlock();
	return (0);
}


//
// TCP ENDPOINT CONNECTION Establishment routines:
//
// int
// tcp_endpoint::udp_client_initiate()
//	Initiate a udp rendezvous.
//
// Description:
//	TCP Connect has a timeout of 60 seconds.
//	And it is blocking. Hence, if a node goes down and comes back up,
//	sometimes it will take a long time (>30 secs) to establish
//	the connection again. In order to avoid this delay, a udp
//	handshake is performed. The server (higher numbered node) sends
//	a udp message before checking for connection from the client.
//	The client (lower numbered node), waits for the udp message.
//	It connects only after it receives the udp message.
//
//	Waits for the udp message from the server,
//	waking up at regular intervals to check if endpoint state changed.
//
// Parameters:
//	None.
//
// Returns:
//	0 on success. error code on error.
//
// See Also:
//	tcp_endpoint::udp_server_initiate().
//
//
int
tcp_endpoint::udp_client_initiate()
{
	int error;

	// While udp message has not arrived from server
	if ((error = udp_getq(TCPTR_UDPCLIENT_TIMEOUT)) == ETIME) {
		// check endpoint state if need to return earlier
		// if endpoint state changes we return EPROTO instead of ETIME
		if (get_state() != E_CONSTRUCTED) {
			return (EPROTO);
		}
	}
	return (error);
}


//
// int
// tcp_endpoint::udp_server_initiate()
//	Send a udp message to the client to ask it to connect.
//
// Description:
//	The server node (higher numbered) sends a udp message
//	to the client. When the client receives the message,
//	it starts the tcp connection.
//
//	The server, after sending the message, waits for the
//	client to connect, for a specified time. If the client
//	does not connect within this period, it sends the message
//	again.
//
//	This routine is called by the server_initiate() routine,
//	which in turn is called by the initiate() routine.
//
int
tcp_endpoint::udp_server_initiate()
{
	struct sockaddr_in sin;
	struct endpoint_udp_info info;
	int rlen;
	char *ip = (char *)&info;
	int error;

	// udp_so contains the udp socket
	//
	ASSERT(udp_so != NULL);

	// Form the message
	//
	info.node = orb_conf::current_id_node();

	bzero(&sin, sizeof (sin));
	sin.sin_family = AF_INET;
	sin.sin_port = htons(UDP_EP_PORT);
	sin.sin_addr.s_addr = remote_ipaddr;

	//
	// Send the message
	//
	rlen = tcptr_sendto(udp_so, ip, (long)sizeof (info), 0,
	    (struct sockaddr *)&sin, (socklen_t)sizeof (sin), &error);
	ASSERT((error != 0) || (rlen == (int)sizeof (info)));

	return (0);
}

//
// void
// tcp_endpoint::recv_initial_msg(mblk_t *bp)
//	Receive the initial message from the peer endpoint.
//
// Description:
//	This routine is called by tcpmod read side put procedure
//	once it has the initial message from the peer endpoint.
//	The tcpmod has been given the endpoint object pointer.
//
//	This function sets the peer connection number, the
//	fact that the initial message has been received,  and
//	wakes up the initiate() process which is waiting for this
//	message.
//
//	Unlike the pathend, endpoint has multiple connections.
//	Hence, this routine is called with the connection number.
//	This routine will set the "initial msg received" for the
//	correct connection. The peer connection number can be
//	compared with the local connection number. Both should
//	match.
//
//
// Parameters:
//	int connection	- local connection number
//	mblk_t *bp	- message containing the initial message.
//
// Returns:
//	None. Wakes up the process waiting for this message.
//
// See Also:
//	send_initial_msg().
//	tcp_endpoint::timed_wait_for_msg().
//
// Notes:
//	tcp endpoint has multiple connections.
//
void
tcp_endpoint::recv_initial_msg(uint_t connection, mblk_t *bp)
{
	struct init_msg info;
	int logit = 0;
	uint_t n_conn = 0, conn = 0;

	ASSERT(bp != NULL);

	ASSERT(msgdsize(bp) >= sizeof (info));

	(void) mp_to_buf(bp, (char *)&info, sizeof (info));
	freemsg(bp);

	TCP_DBPRINTF(("tep::recv_initial_msg from %u:%u adapter %d "
	    "connection %d\n", info.src.ndid, info.src.incn,
	    local_adapter_id(), connection));

	if (!ID_node::match(info.src, get_rnode())) {
		TCP_DBPRINTF(("endpoint Received initial message from"
		    "mismatched incarnations %u:%u %u:%u\n",
		    get_rnode().ndid, get_rnode().incn,
		    info.src.ndid, info.src.incn));
		return;
	}

	tep_lock();
	// If the incoming connection matches what we are waiting for
	// then set the flag and signal the waiting thread
	// If not, neglect the incoming message and the waiting thread
	// will wake up and consider it a timeout and restart the connection
	// setups
	if (info.connection == n_connections) {
		set_msg_received(connection);
	} else {
		logit = 1;
		n_conn = n_connections;
		conn = info.connection;
	}
	tep_unlock();
	if (logit)
		TCP_DBPRINTF(("connection mismtach local %d remote %d\n",
		    n_conn, conn));
}


//
// int
// tcp_endpoint::timed_wait_for_msg(uint_t connection)
//	Wait for the initial msg from the peer endpoint.
//
// Description:
//	This routine waits for the initial message from the
//	remote endpoint. It timeouts after TCPTR_INITIAL_MSG_WAIT
//	microsecs.
//
//	When the initial msg for a particular connection arrives
//	from the peer, the connection is marked so.
//
// Parameters:
//	uint_t connection
//
// Returns:
//	if bool whether message has been received.
//
// See Also:
//	send_initial_msg().
//	tcp_endpoint::recv_initial_msg().
//
// Notes:
//	This routine only waits one time.
//
bool
tcp_endpoint::timed_wait_for_msg(uint_t conn)
{
	tep_lock();
	if (!has_msg_been_received(conn)) {
		os::systime timo(TCPTR_INITIAL_MSG_WAIT);
		(void) so_rmsg_cv[conn].timedwait(tep_get_lock(), &timo);
	}
	tep_unlock();

	return (has_msg_been_received(conn));
}

//
// struct sonode *
// tcp_endpoint::so_getq()
//	Get the connected socket from the client.
//
// Description:
//	This function is invoked by the server node to get the
//	connected socket from the client. It waits for a
//	specified amount of time and returns the connected
//	socket if successful. It returns NULL otherwise.
//
//	Since the endpoint has several connections, the incoming
//	connections are queued. The connections are returned in
//	the order they arrive, one at a time.
//
// Parameters:
//	None.
//
// Returns:
//	socket (struct sonode *) if successful. NULL otherwise.
//
// See Also:
//	tcp_endpoint::so_putq().
//
//
struct sonode *
tcp_endpoint::so_getq()
{
	struct sonode *so;

	tep_lock();
	if (tep_state != TEP_INITIATED ||
	    in_cindex == (int)max_conns) {
		tep_unlock();
		return (NULL);
	}
	so = in_so_ptrs[in_cindex];
	if (so == NULL) {
		os::systime timo((os::usec_t)TCPTR_TCPSOCKET_WAIT);
		(void) tep_cv.timedwait(tep_get_lock(), &timo);
		so = in_so_ptrs[in_cindex];
	}
	if (so != NULL) {
		in_so_ptrs[in_cindex] = NULL;
		in_cindex++;
	}
	tep_unlock();

	return (so);
}


//
// int
// tcp_endpoint::so_putq(struct sonode *so)
//	Place the connected socket in the queue and wakeup the
//	process waiting for it.
//
// Description:
//	This routine is called to place the connected socket
//	in the waiting pathend's queue. The waiting process
//	is signaled.
//
// 	socket is closed by the caller in the case of an error.
//
//	The endpoint has several connections (configurable).
//	The incoming connections are placed in a queue.
//	so_getq() will retrieve them.
//
// Parameters:
//	struct sonode *so	- connected socket
//
// Returns:
//	0 on success and errno otherwise.
//
// See Also:
//	tcp_endpoint::so_getq().
//
//
int
tcp_endpoint::so_putq(struct sonode *so)
{
	tep_lock();
	if ((tep_state != TEP_INITIATED) ||
	    (in_pindex == (int)max_conns) ||
	    (in_so_ptrs[in_pindex] != NULL)) {
		tep_unlock();
		return (EPROTO);
	}
	in_so_ptrs[in_pindex] = so;
	in_pindex++;
	signal_tep();
	tep_unlock();

	return (0);
}


//
// int
// tcp_endpoint::process_accept(struct sonode *so, bool server)
//	Process the connected sockets and make them reliable connections.
//
// Description:
//	This function is called both by the client and server
//	to process connected sockets.
//
//	It sets the MTU for the connection.
//	It also sets the write offset to leave room for headers.
//
//	It sets the TCP_NODELAY option so that any message will be
//	sent to the remote node immediately by TCP (otherwise TCP
//	tends to aggregate until sufficient size is reached).
//
//	It pushes tcpmod onto the connection (in the case of
//	server. In the case of client, it is pushed at the time
//	of socket creation. This is because of the way accept()
//	works). It also exchanges message with the peer. This helps
//	ensure that the a reliable datagram connection is fully
//	formed before higher layer messages are exchanged.
//
//	It verifies that the connection number matches with the peer.
//	It updates various information and in the case of the last
//	connection, updates the state.
//
//	Note that tcp_endpoint has several connections.
//	These connections are used to exchange data.
//	Common information is obtained during the first connection.
//	State is set to connected only if all of the connections
//	succeed.
//	The sockets (struct sonode *) and the streams queue pointers
//	are stored in arrays.
//	Maximum number of connections is configurable.
//	Each connection is associated with a particular type of orb msg.
//	This is configurable.
//
//	Note that if there is an error, the socket is not stored.
//	Nor is the tep_state set to connected.
//
//	Returns 0 on success and error code on error.
//
// Parameters:
//	struct sonode *so	- connected socket
//	bool server		- whether this is server.
//
// Returns:
//	0 on success. error code otherwise.
//
// See Also:
//	set_connect_client().
//
// Notes:
//	The error cases can occur if the remote node went down
//	in the middle of establishing connections. Currently,
//	if an error occurs (like node going down), the path manager
//	notices that and cleans up and reestablishes the pathend and
//	endpoint (i.e. path).
//
//
int
tcp_endpoint::process_accept(struct sonode *so, bool server)
{
	nodeid_t log_ndid = 0;
	incarnation_num log_incn = 0;
	uint_t log_adapter_id = 0;
	endpoint_state log_tep_state = TEP_RESET;

	TCP_DBPRINTF(("tep:process_accept from node %d adapter %d "
	    "connection# %d state %d\n",
	    get_rnode().ndid, local_adapter_id(), n_connections + 1,
	    tep_state));

	int error;
	uint_t connection_no;
	int log_connected = 0;

	tep_lock();
	if (tep_state != TEP_INITIATED) {
		tep_unlock();
		TCP_DBPRINTF(("tep::process_accept:State not INITIATED\n"));
		return (EPROTO);
	}
	connection_no = n_connections;
	tep_unlock();

#if	SOL_VERSION >= __s11
	ksocket_t	ksp = SOTOKS(so);
#endif	// SOL_VERSION >= __s11
	if (connection_no == 0) {
		//
		// First Connection.
		// Initialize mtu_size and wroff
		// All connections for a given path are assumed to have the
		// same MAX_SEG and wroff values.
		//
#if	SOL_VERSION >= __s11
		int optlen = sizeof (mtu_size);
		error = ksocket_getsockopt(ksp, IPPROTO_TCP,
		    TCP_MAXSEG, (void *)&mtu_size, &optlen, CRED());
#else	// SOL_VERSION >= __s11
		socklen_t optlen = (socklen_t)sizeof (mtu_size);
		error = sogetsockopt(so, IPPROTO_TCP, TCP_MAXSEG,
			(void *)&mtu_size, &optlen, 0);
#endif	// __SOL_VERSION >= __s11
		if (error) {
			TCP_DBPRINTF(("tep:process_accept: getsockopt "
					"failed. error %d\n", error));
			return (error);
		}

		//
		// XXX Need to intercept M_SETOPTS in tcpmod to get this value
		//
		wroff = TCPTR_TCPIP_HDR_SIZE;
	}

	//
	// First set TCP_NODELAY. We do not set NODELAY for replyio connections
	//
	int binary = 1;
#if	SOL_VERSION >= __s11
	error = ksocket_setsockopt(ksp, IPPROTO_TCP, TCP_NODELAY,
	    (void *)&binary, (t_uscalar_t)sizeof (binary), CRED());
#else	// SOL_VERSION >= __s11
	error = sosetsockopt(so, IPPROTO_TCP, TCP_NODELAY,
	    (void *)&binary, (t_uscalar_t)sizeof (binary));
#endif	// SOL_VERSION >= __s11
	if (error) {
		TCP_DBPRINTF(("tep::process_accept: Error in set nodelay "
		    "%d for connection %d\n", error, connection_no));
		return (error);
	}

	so_lock.lock(); // synchronize with tcp_endpoint::set_timeouts
	// Set the timeout to drop a tcp connection if acks could not be
	// received.
	int abort_timeout = tcp_transport_ep_abort_threshold * MSECS_PER_SEC;
#if	SOL_VERSION >= __s11
	error = ksocket_setsockopt(ksp, IPPROTO_TCP,
	    TCP_ABORT_THRESHOLD, (void *)&abort_timeout,
	    (t_uscalar_t)sizeof (abort_timeout), CRED());
#else	// SOL_VERSION >= __s11
	error = sosetsockopt(so, IPPROTO_TCP, TCP_ABORT_THRESHOLD,
	    (void *)&abort_timeout, (t_uscalar_t)sizeof (abort_timeout));
#endif	// SOL_VERSION >= __s11
	if (error) {
		TCP_DBPRINTF(("tep::process_accept: Error in set abort timeout "
		    "%d for connection %d\n", error, connection_no));
		so_lock.unlock();
		return (error);
	}

	//
	// Push tcpmod for server. For client it happens before
	// soconnect().
	//
	if (server) {
		error = tcpmod_push(so, TCP_ENDPOINT, this, connection_no);
		if (error != 0) {
			TCP_DBPRINTF(("Failed to push tcpmod %d\n", error));
			so_lock.unlock();
			return (error);
		}
	}

	// Do the conversion from sonode to queue after doing tcpmod_push
	queue_t *q = WR((strvp2wq(SOTOV(so)))->q_next);
	ASSERT(q != NULL);

	//
	// Do the client/server handshake. Client/server sides are mirror
	// images of each other.
	//
	if (server) {
		send_initial_msg(q, connection_no, TCP_EP_INITIAL_MSG);
		if (!timed_wait_for_msg(connection_no)) {
			TCP_DBPRINTF(("tep::process_accept: Server failed\n"));
			so_lock.unlock();
			return (ETIME);
		}
	} else {
		if (!timed_wait_for_msg(connection_no)) {
			TCP_DBPRINTF(("tep::process_accept: client failed\n"));
			so_lock.unlock();
			return (ETIME);
		}
		send_initial_msg(q, connection_no, TCP_EP_INITIAL_MSG);
	}

	tep_lock();
	if (tep_state != TEP_INITIATED) {
		tep_unlock();
		so_lock.unlock();
		TCP_DBPRINTF(("tep::process_accept: Endpoint Changed State. "
		    " State is %d\n", tep_state));
		return (EPROTO);
	}
	ASSERT(connection_no < max_conns);

	// The assert above justifies turning off the lint warnings on
	// the next two lines.
	so_ptrs[connection_no] = so;	//lint !e661
	so_wqs[connection_no] = q;	//lint !e661

	ASSERT(connection_no == n_connections);
	n_connections++;

	if (n_connections == max_conns) {
		tep_state = TEP_CONNECTED;
		log_ndid = get_rnode().ndid;
		log_incn = get_rnode().incn;
		log_adapter_id = local_adapter_id();
		log_tep_state = tep_state;
		log_connected = 1;
	}

	tep_unlock();
	so_lock.unlock();
	// log after dropping the lock
	if (log_connected)
		TCP_DBPRINTF(("tep::process_accept:connections setup with "
		    "node %u:%u adapter %d state %d\n",
		    log_ndid, log_incn,
		    log_adapter_id, log_tep_state));
	TCP_DBPRINTF(("tep: accepted from node %u adapter %d "
	    "connection# %d state %d so_wq %p\n",
	    get_rnode().ndid, local_adapter_id(), n_connections, tep_state, q));

	return (0);
}


//
// TCP ENDPOINT INITIATE Functions
//
//
// int
// tcp_endpoint::client_initiate()
//	Initiate endpoint connections as a client.
//
// Description:
//	This routine initiates connections to the remote endpoint.
//
//	It does this by calling udp_client_initiate(), which
//	waits for a udp message from the server. The arrival of an udp
//	message from the server means the server is ready for connection.
//
//	It then calls set_connect_client() to initiate the tcp connections
//	to the server. The module "tcpmod" is pushed onto the socket
//	as soon as it is created.
//
//	Once the tcp connection establishment succeeds, it calls
//	process_accept() to process the connection.
//
//	Note that there are several connections in tcp endpoint.
//	The endpoint is considered connected (and initiate() returns)
//	if and only if all the connections (number and nature of which
//	are configurable) are established successfully.
//
// Parameters:
//	None.
//
// Returns:
//	0 on success, error code otherwise.
//
// See Also:
//	tcp_endpoint::udp_client_initiate(), set_connect_client(),
//	tcp_endpoint::process_accept().
//
//
// Assertions:
//	Once initiate is called, cleanup() will be called.
//	cleanup() will be called after initiate() completes.
//	unblock() will speed up termination of initiate().
//
int
tcp_endpoint::client_initiate()
{
	struct sonode *so;
	int error;

	//
	// If remote nodeid is greater than your node number, be the client
	//
	ASSERT(orb_conf::local_nodeid() < get_rnode().ndid);

	if ((error = udp_client_initiate()) != 0) {
		return (error);
	}

	TCP_DBPRINTF(("tep::client initiate: connect to node %u:%u adapter %d"
	    "connection# %d\n", get_rnode().ndid, get_rnode().incn,
	    local_adapter_id(), n_connections));

	tep_lock();

	tep_state = TEP_INITIATED;
	//
	// call set_connect_client() until all the connections are
	// established.  A connection may
	// not be established because the client may be started
	// before the server. So, we have to keep retrying until all the
	// the connections is established or endpoint is cleanedup.
	// soconnect() sleeps and hence no explicit sleep is necessary.
	//
	while (tep_state == TEP_INITIATED) {
		tep_unlock();
		so = set_connect_client(remote_ipaddr, TCP_EP_PORT,
			(void *)this, TCP_ENDPOINT, n_connections, &error);
		// if so is NULL, it implies the network is not reachable,
		// return the error here.
		if (so == NULL) {
			return (error);
		}
		if ((error = process_accept(so, false)) != 0) {
#if	SOL_VERSION >= __s11
			ksocket_t	ksp;
			ksp = SOTOKS(so);
			ksocket_close(ksp, CRED());
#else	// SOL_VERSION >= __s11
			(void) sodisconnect(so, -1, 0);
			soclose(so);
#endif	// SOL_VERSION >= __s11
			return (error);
		}
		tep_lock();
	}
	tep_unlock();

	return (error);
}


//
// int
// tcp_endpoint::server_initiate()
//	Initiate connection from the server side.
//
// Description:
//	This function is called by initiate() in the case of
//	server nodes (higher numbered nodes) to initiate the
//	endpoint connections.
//
//	It calls udp_server_initiate(), which sends a udp
//	message to the client to inform that the server is
//	ready.
//	It then waits for the tcp connection from the client.
//	When the client connects, it is queued to this routine
//	by the tcp_transport::start_server_ep() routine.
//	It calls process_accept() to complete the connection.
//
//
// Parameters:
//	None.
//
// Returns:
//	0 on success. errno otherwise.
//
// See Also:
//	tcp_endpoint::udp_server_initiate().
//	tcp_endpoint::so_getq().
//	tcp_endpoint::process_acccept().
//
int
tcp_endpoint::server_initiate()
{
	int error;
	struct sonode *so;

	if ((error = udp_server_initiate()) != 0) {
		return (error);
	}

	tep_lock();
	tep_state = TEP_INITIATED;
	while (tep_state == TEP_INITIATED) {
		tep_unlock();
		if ((so = so_getq()) == NULL) {
			return (ETIME);
		}
		if ((error = process_accept(so, true)) != 0) {
#if	SOL_VERSION >= __s11
			ksocket_t	ksp;
			ksp = SOTOKS(so);
			ksocket_close(ksp, CRED());
#else	// SOL_VERSION >= __s11
			(void) sodisconnect(so, -1, 0);
			soclose(so);
#endif	// SOL_VERSION >= __s11
			return (error);
		}
		tep_lock();
	}
	tep_unlock();

	return (0);
}

//
// void
// tcp_endpoint::initiate()
//	Initiate the tcp endpoint connections to the remote node.
//
// Description:
//	tcp endpoint has several connections. These connections are
//	used to exchange data.
//
//	If the remote node number is greater than this node number,
//	act as a client. Else act as a server.
//
//	It calls the client_initiate() or the server initiate() as the
//	case may be. Initial udp messages are exchanged. Then
//	the tcp connections are established. tcpmod is pushed onto
//	each connection to make it a reliable datagram connection.
//
//	Calls _cleanup() to handle any intermediate failures.
//
//	udp_init() creates a udp socket.
//	udp_finish() closes it at the end.
//
//	NOTE: Make sure that udp_finish() is called corresponding to the
//		udp_init() before leaving this routine.
//
// Parameters:
//	None.
//
// Returns:
//	None.
//
void
tcp_endpoint::initiate()
{
	int error;
	bool pathend_failed = false;

	ASSERT(tep_state == TEP_RESET);

	ASSERT(lock_held());
	TCP_DBPRINTF(("In tep_initiate on node %d adapter %d rnode %d "
		"state %d\n", orb_conf::local_nodeid(), local_adapter_id(),
		    get_rnode().ndid, get_state()));

	unlock();

	// Checking state without holding lock, depends on consistent read.
	while (get_state() == E_CONSTRUCTED) {
		error = udp_init();
		if (error == 0) {
			// Success. Break out of the udp init loop and
			// proceed with the rest of the initialization.
			break;
		} else if ((error == EADDRINUSE) || (error == EADDRNOTAVAIL)) {
			//
			// Bind error (unrecoverable?) in initiate. Fail the
			// pathend so the error does not go unnoticed.
			//
			os::sc_syslog_msg msg("TCP TRANSPORT", "", NULL);
			//
			// SCMSGS
			// @explanation
			// Communication with another node could not be
			// established over the path.
			// @user_action
			// Any interconnect failure should be resolved, and/or
			// the failed node rebooted.
			//
			(void) msg.log(SC_SYSLOG_WARNING, MESSAGE,
				"Endpoint %s initialization error -"
				" errno = %d, failing associated pathend.",
				pathendp->get_extname(), error);
			pathendp->fail();
			pathend_failed = true;
			break;
		} else {
			// Transient error, sleep and then retry.
			os::usecsleep(TCPTR_INITIATE_SLEEP);
		}
	}

	//
	// If we forced a pathend failure, wait until the endpoint exits
	// the E_CONSTRUCTED state and then return. There if no provision
	// for initiate to return in the E_CONSTRUCTED state.
	// On calling pathend fail, generic endpoint state machine will move
	// from E_CONSTRUCTED->E_PUSHING->E_CLEANING and calls cleanup()
	// There we signal a cv what we wait on here to check for state change
	// XX There is already an ep_cv and a broadcast that happens on this
	// in the generic endpoint. If that can be made protected instead of
	// private, we could use it here instead of private cv.
	//
	if (pathend_failed) {
		// Checking state without holding endpoint lock,
		// depends on consistent read.
		tep_lock();
		while (get_state() == E_CONSTRUCTED) {
			tep_cv.wait(tep_get_lock());
		}
		tep_unlock();
		lock();
		return;
	}

	//
	// Add to list of endpoints in initiate state
	//
	get_transport()->add_tep(this);

	// Not using lock() while calling get_state(), at worst
	// we will do one more round of client/server initiate
	while (get_state() == E_CONSTRUCTED) {
		ASSERT(tep_state == TEP_RESET);
		if (orb_conf::local_nodeid() < get_rnode().ndid) {
			error = client_initiate();
		} else {
			error = server_initiate();
		}
		if (error == 0) {
			// initiate completed either successfully or
			// endpoint state has changed
			break;
		}
		_cleanup();
	}

	//
	// Remove from list of endpoints in initiate state
	//
	get_transport()->remove_tep(this);

	// Close udp socket
	udp_finish();

	lock();
	TCP_DBPRINTF(("Done tep_initiate on node %d adapter %d rnode %d "
		"state %d\n", orb_conf::local_nodeid(), local_adapter_id(),
		    get_rnode().ndid, get_state()));
}

//
// TCP ENDPOINT SEND/RECV routines:
//


uint_t
tcp_endpoint::choose_conn(int msgtype)
{
	uint_t	conn;

	switch (msgtype) {
	case TCP_REPLYIO_MSG:
		tep_lock();
		conn = curr_rio_index;
		if (++curr_rio_index == max_conns) {
		    curr_rio_index = rio_base;
		}
		tep_unlock();
		break;
	case CKPT_MSG:
		conn = 0;
		break;
	default:
		conn = 1;
		break;
	}

	return (conn);
}

//
// void
// tcp_endpoint::add_resources(resources *resourcep)
//	Add resources common to tcp endpoint connections.
//
// Description:
//	Sufficient space for lower layer protocol headers are added.
//
// Parameters:
//	resources *resourcep	- pointer to resources object.
//
// Returns:
//	None.
//
void
tcp_endpoint::add_resources(resources *resourcep)
{
	//
	// The tcp transport is actually in the middle of the protocols.
	// So we must reserve space for lower level protocol headers as
	// well as the upper layer header size that was passed to us.
	//
	resourcep->add_send_header((uint_t)(sizeof (tcp_message_header) +
		sizeof (tcpmod_header_t)));
}


//
// void
// tcp_endpoint::push()
//	Nothing to do here for tcp transport as transport does not ack
//	messages to sender till they have been delivered to the ORB
//
void
tcp_endpoint::push()
{
	ASSERT(lock_held());
	ASSERT(get_state() == E_PUSHING);
}

//
// sendstream *
// tcp_endpoint::get_sendstream(resources *resourcep)
//	Create a tcp sendstream object.
//
// Description:
// 	get_sendstream - validate that this node can communicate with the
//	destination node at the target incarnation number prior to
//	reserving a sendstream. Allocate a buffer with at least the
//	requested number of bytes. Header and Data must be word size multiples.
//	The start of data buffer cursor is advanced past the bytes
//	reserved for the headers.
//
// Parameters:
//	resources *resourcep	- pointer to resources object.
//
// Returns:
//	Pointer to a new tcp sendstream object if successful.
//	NULL otherwise.
//
//
sendstream *
tcp_endpoint::get_sendstream(resources *resourcep)
{
	Environment	*e = resourcep->get_env();
	tcp_sendstream	*se;

	/*CSTYLED*/
	se = new (e->nonblocking_type()) tcp_sendstream(this, resourcep);
	if (se == NULL) {
		ASSERT(e->is_nonblocking());
		// sendstream allocation failed.
		// Release endpoint and throw exception.
		e->system_exception(CORBA::WOULDBLOCK(
		    (uint_t)sizeof (tcp_sendstream), CORBA::COMPLETED_NO));
	} else if (CORBA::WOULDBLOCK::_exnarrow(e->exception())) {
		//
		// Only this specific exception prevents the creation
		// of the main buffer. Without a main buffer the sendstream
		// is useless. Other exceptions may be present in the case
		// of a reply message.
		//
		ASSERT(e->is_nonblocking());

		// Buffer allocation failed.  Get rid of sendstream
		// This releases the endpoint. se->done() will ensure
		// sendstream destruction, hence there is no memory leak
		// here.
		se->done();
		se = NULL; //lint !e423
	}
	return (se);
}


//
// void
// tcp_endpoint::send(orb_msgtype msgt,
// 			orb_seq_t seq,
//			mblk_t *mp,
//			uint_t len,
//			bool syncflag,
//			tcp_sendstream *se,
//			Environment *e)
//	tcp endpoint send.
//
// Description:
//	Fills up the tcpmod header, selects the right connection
//	for the msg type and sends the message downstream.
//	Checks the state of endpoint and if the state is not correct
//	returns an exception.
//
//	In case we have done any reply I/Os on this sendstream,
//	we wait for the acks to arrive before we send the message out
//
// Parameters:
// 	orb_msgtype msgt	- msg type
// 	orb_seq_t seq		- sequence number
//	mblk_t *mp		- pointer to mblk
//	uint_t len		- length of msg
//	bool syncflag		- synchronous or not
//	tcp_sendstream *se	- sendstream involved in doing the send
//	Environment *e		- environment
//
// Returns:
//	None.
//
// Notes:
// 	mp should be freed in case of RETRY_NEEDED exception
//	(if already passed to tcp, tcp is responsible for cleaning
//	up on exception). Currently there is no other kind of exception
//	generated, if not the recovery is more complicated and needs
//	to be done in sendstream::send syncflag indicates whether should
//	wait or not for ack from remote node.
//
void
tcp_endpoint::send(orb_msgtype msgt,
			orb_seq_t seq,
			mblk_t *mp,
			uint_t len,
			bool syncflag,
			tcp_sendstream *se,
			Environment *e)
{
	MC_PROBE_0(tcp_end_send_start, "clustering tcp_transport", "");

	queue_t *q;
	uint_t conn;

	// Verify that syncflag is not set for nonblocking sends
	ASSERT(!(syncflag && e->is_nonblocking()));

	ASSERT(mp != NULL);

	//
	// sendstream preallocates space for the header
	//
	ASSERT(MBLKHEAD(mp) >= (int)sizeof (tcpmod_header_t));
	mp->b_rptr -= sizeof (tcpmod_header_t);

	//
	// Note: If b_rptr is unaligned, we need to either align or use bcopy
	// notify_ptr is bcopy'd because 8-byte alignment is not guaranteed.
	//
	tcpmod_header_t *headerp = (tcpmod_header_t *)mp->b_rptr;

	headerp->seq = seq;
	headerp->size = len + (uint_t)sizeof (tcpmod_header_t);
	ASSERT(headerp->size == msgdsize(mp));
	headerp->msgt = msgt;
#ifdef DEBUG
	headerp->src = orb_conf::current_id_node();
	headerp->debug = DEBUG_PATTERN;
#endif

	//
	// Store the pointer to a sendstream in header to be used in waking up
	// synchronous sends and sending ACKs
	//
	void *notify_ptr = (syncflag) ? (void *)se : NULL;
	bcopy(&notify_ptr, &headerp->notify_ptr, sizeof (notify_ptr));

#if defined(FAULT_TRANSPORT)
	fault_pathend_fail();			// try fail a pathend
#endif

	// If replyio acks are in use, wait for them
	if (replyio_acks_used()) {
		// wait until any put offline replies have been acknowledged.
		// If endpoint/path fails, set exception in environment
		if (!se->wait_for_replyios()) {
			e->system_exception(
			    CORBA::RETRY_NEEDED(0, CORBA::COMPLETED_NO));
			freemsg(mp);
			MC_PROBE_0(tcp_end_send_end, "clustering tcp_transport",
			    "");
			return;
		}
	}
	MC_PROBE_0(tcp_end_send_waitdone, "clustering tcp_transport", "");

	//
	// The senstream might also have a preference for a connection as
	// it might want to batch offline data and main message on the same
	// connection.
	// If there are no preferred connections at all, choose one based
	// on message type.
	//
	conn = se->get_preferred_conn();
	if (conn == TCP_INVAL_CONN) {
		// If it is a replyio request, send the request on an rio
		// channel.
		if (se->is_replyio_req()) {
			ASSERT(msgt == INVO_MSG);
			conn = choose_conn(TCP_REPLYIO_MSG);
		} else {
			conn = choose_conn(msgt);
		}
	}
	ASSERT(conn < max_conns);
	q = get_queue(conn);

	// For synchronous messages set ack_pending to true
	if (syncflag) {
		se->set_ack_pending();

		ETCP_DBPRINTF(TCPTR_TRACE_SYNC_ACK,
		    ("EP%u:%u:%u sending synchronous message, size %d"
		    " notify ptr %p type %d.\n", get_rnode().ndid,
		    local_adapter_id(), conn, headerp->size,
		    notify_ptr, headerp->msgt));
	}

	//
	// Returning from putnext does not imply data left the
	// machine, data has been accepted by the transport and it will send it
	// if the remote node is reachable.
	//
	// We do not do a canput here. It is too late to flow control here
	// The ORB flow control has to stop marshaling earlier as now
	// there is nothing better to do if flow control fails, than to wait
	// We putnext directly into tcp and bypass tcpmod
	// If tcpmod needs to be involved we can replace putnext with a putq
	//
	ASSERT(q != NULL);
	putnext(q, mp);
	MC_PROBE_0(tcp_end_send_putnext, "clustering tcp_transport", "");

	//
	// Record the current time as the time the last message was
	// sent.
	//
	last_send_lbolt = ddi_get_lbolt();

	// For synchronous messages wait for ack to arrive before returning
	// If endpoint/path fails, set exception in environment
	if (syncflag) {
		if (!se->wait_for_sync_ack()) {
			e->system_exception(
				CORBA::RETRY_NEEDED(0, CORBA::COMPLETED_MAYBE));
		}
	}
	MC_PROBE_0(tcp_end_send_end, "clustering tcp_transport", "");
}

//
// tcp_endpoint::send_keep_active_message
//
// Sends a dummy TCP_KEEP_ACTIVE message if there has been no sends on this
// endpoint for a while.
//
void
tcp_endpoint::send_keep_active_message()
{
	mblk_t		*mp;
	queue_t		*q;
	tcpmod_header_t	*thp;
	os::usec_t	keep_active_interval;
	os::usec_t	time_since_last_send;

	//
	// Send a dummy message only if no other message has been sent
	// on this endpoint for an interval longer than
	// tcp_transport_ep_abort_threshold / 10.
	//
	time_since_last_send = (ddi_get_lbolt() - last_send_lbolt) *
	    USECS_IN_A_TICK;
	keep_active_interval = (os::usec_t)tcp_transport_ep_abort_threshold *
	    USECS_IN_A_SEC / 10;
	if (time_since_last_send < keep_active_interval) {
		return;
	}

	mp = os::allocb((uint_t)(sizeof (tcpmod_header_t) +
	    TCPTR_TCPIP_HDR_SIZE), BPRI_HI, os::NO_SLEEP);
	if (mp == NULL) {
		return;
	}
	mp->b_rptr += TCPTR_TCPIP_HDR_SIZE;
	mp->b_wptr = mp->b_rptr;

	thp = (tcpmod_header_t *)mp->b_wptr;


	thp->msgt = TCP_KEEP_ACTIVE;
	thp->size = (uint_t)sizeof (tcpmod_header_t);
	thp->notify_ptr = NULL;
#ifdef DEBUG
	thp->src = orb_conf::current_id_node();
	thp->debug = DEBUG_PATTERN;
#endif

	mp->b_wptr += sizeof (tcpmod_header_t);

	q = get_queue(choose_conn(TCP_KEEP_ACTIVE));
	ASSERT(q != NULL);
	putnext(q, mp);

	last_send_lbolt = ddi_get_lbolt();
}

//
// TCP TRANSPORT class routines
//

// Even though we use the prefer tcp_ in this code, we return the transport_id
// as dlpi as the transport type specified by scconf and external documentation
// is dlpi. The fact that we use tcp is an implementation detail!
const char *
tcp_transport::transport_id()
{
	return ("dlpi");
}

//
// Routines to receive udp messages for pathends and endpoints.
//
int
tcp_transport::start_udp_server_pe()
{
	struct sonode *so;
	struct sockaddr_in server;
	int error;
	int binary;
	in_addr_t ipaddr;
	struct sockaddr_in sin;
	struct pathend_udp_info *infop;
	int rlen;
	os::sc_syslog_msg msg("TCP TRANSPORT", "", NULL);
	char	ipbuf[16];

	TCP_DBPRINTF(("Starting udp server for pathend\n"));
#if	SOL_VERSION >= __s11
	ksocket_t	ksp;
#else	// SOL_VERSION >= __s11
	vnode_t *avp;
#endif	// SOL_VERSION >= __s11
retry:
	binary = 1;

	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(UDP_PE_PORT);
#if	SOL_VERSION >= __s11
	error = ksocket_socket(&ksp, AF_INET, SOCK_DGRAM, 0,
	    KSOCKET_SLEEP, CRED());
	if (error) {
		TCP_DBPRINTF(("tcp_transport::start_udp_server_pe: "
		"ksocket create failed with error = %d\n", error));
		return (error);
	}

	error = ksocket_setsockopt(ksp, SOL_SOCKET, SO_REUSEADDR,
	    (void *)&binary, (t_uscalar_t)sizeof (binary), CRED());
	if (error) {
		ksocket_close(ksp, CRED());
		TCP_DBPRINTF(("tcp_transport::start_udp_server_pe: "
		    " Error in set soreuseaddr %d\n", error));
		return (error);
	}

	error = ksocket_bind(ksp, (struct sockaddr *)&server,
	    (socklen_t)sizeof (server), CRED());
	if (error) {
		ksocket_close(ksp, CRED());
		TCP_DBPRINTF(("tcp_transport::start_udp_server_pe: "
		    "ksocket_bind failed with error = %d\n", error));
		return (error);
	}

	so = KSTOSO(ksp);
#else	// SOL_VERSION >= __s11
	avp = solookup(AF_INET, SOCK_DGRAM, 0, NULL, &error);
	if (avp == NULL) {
		TCP_DBPRINTF(("tcp_transport::start_udp_server_pe: "
		    "solookup failed with error = %d\n", error));
		return (error);
	}

	so = socreate(avp, AF_INET, SOCK_DGRAM, 0, SOV_SOCKSTREAM, NULL,
		&error);
	if (so == NULL) {
		TCP_DBPRINTF(("tcp_transport::start_udp_server_pe: "
		    "socreate failed with error = %d\n", error));
		return (error);
	}

	error = sosetsockopt(so, SOL_SOCKET, SO_REUSEADDR, (void *)&binary,
		(t_uscalar_t)sizeof (binary));
	if (error) {
		soclose(so);
		TCP_DBPRINTF(("tcp_transport::start_udp_server_pe: Error in set"
		    "soreuseaddr %d\n", error));
		return (error);
	}

	TCP_DBPRINTF(("Port number in udp server (PE) is %d\n", UDP_PE_PORT));

	error = sobind(so, (struct sockaddr *)&server,
		(socklen_t)sizeof (server), 0, 0);
	if (error) {
		soclose(so);
		TCP_DBPRINTF(("tcp_transport::start_udp_server_pe: "
		    "sobind failed with error = %d\n", error));
		return (error);
	}
#endif	// SOL_VERSION >= __s11
	//
	// Continue while transport is active
	//
	while (!is_shutdown) {
		bzero(pe_udp_recv_buf, sizeof (struct pathend_udp_info));
		bzero(&sin, sizeof (sin));

		rlen = tcptr_recvfrom(so, pe_udp_recv_buf,
		    (long)pe_udp_recv_buf_size, (struct sockaddr *)&sin,
		    (socklen_t)sizeof (sin));

		if (rlen == -1) {
			TCP_DBPRINTF(("tcp_transport::start_udp_server_pe: "
			    "sorecvmsg failed. error = %d\n", error));
#if	SOL_VERSION >= __s11
			ksocket_close(ksp, CRED());
#else	// SOL_VERSION >= __s11
			soclose(so);
#endif	// SOL_VERSION >= __s11
			goto retry;
		}

		infop = (struct pathend_udp_info *)pe_udp_recv_buf;

		ipaddr = sin.sin_addr.s_addr;
		(void) tcp_util::iptostr(ipaddr, ipbuf);

		tcp_pathend *tpe = get_tcp_pathend_for_ipaddr(ipaddr);
		if (tpe == NULL) {
			//
			// No Pathend.
			//
			TCP_DBPRINTF(("start_udp_server_pe: no pathend"
			    " for IP %s\n", ipbuf));
			continue;
		}

		//
		// Check if it could be a message from a node running 3.0 or
		// one of its updates. If yes, we will try to log a more helpful
		// message.
		//
		if ((rlen == PATHEND_UDP_INFO_30_LENGTH) &&
		    (infop->hdrver == PATHEND_UDP_INFO_30_REQUEST_VERSION ||
		    infop->hdrver == PATHEND_UDP_INFO_30_REPLY_VERSION)) {
			//
			// Record the current timestamp and log a message
			// if this handshake can potentially be from a
			// rebooting node.
			//
			if (tpe->record_bad_udp_handshake()) {
				//
				// SCMSGS
				// @explanation
				// Sun Cluster software at the local node
				// received an initial handshake message from
				// the remote node that is not running a
				// compatible version of the Sun Cluster
				// software.
				// @user_action
				// Make sure all nodes in the cluster are
				// running compatible versions of Sun Cluster
				// software.
				//
				(void) msg.log(SC_SYSLOG_WARNING, MESSAGE,
				    "Peer node %d attempted to contact us with"
				    " an invalid version message, source IP %s."
				    " Peer node may be running pre 3.1 Sun"
				    " Cluster software.", tpe->node().ndid,
				    ipbuf);
			}
			tpe->get_pathend_rele(); // corresponding to get above
			continue;
		}

		if (rlen < (int)PATHEND_UDP_INFO_MANDATORY_HEADER_LENGTH ||
		    rlen != (int)infop->msglen ||
		    infop->msglen < infop->hdrlen) {
			//
			// Record the current timestamp and log a message
			// if this handshake can potentially be from a
			// rebooting node.
			//
			if (tpe->record_bad_udp_handshake()) {
				//
				// SCMSGS
				// @explanation
				// Sun Cluster software at the local node
				// received an initial handshake message from
				// the remote node that is not running a
				// compatible version of the Sun Cluster
				// software.
				// @user_action
				// Make sure all nodes in the cluster are
				// running compatible versions of Sun Cluster
				// software.
				//
				(void) msg.log(SC_SYSLOG_WARNING, MESSAGE,
				    "Peer node %d attempted to contact us with"
				    " an invalid version message,"
				    " source IP %s.", tpe->node().ndid, ipbuf);
			}
			tpe->get_pathend_rele(); // corresponding to get above
			continue;
		}

		tpe->udp_putq(infop, &sin, so);
		tpe->get_pathend_rele();	// corresponding to get above
	}

	TCP_DBPRINTF(("start_udp_server_pe(): End\n"));
#if	SOL_VERSION >= __s11
	ksocket_close(ksp, CRED());
#else	// SOL_VERSION >= __s11
	soclose(so);
#endif	// SOL_VERSION >= __s11

	return (0);
}

int
tcp_transport::start_udp_server_ep()
{
	struct sonode *so;
	struct sockaddr_in server;
	int error;
	int binary;
	in_addr_t ipaddr;
	struct sockaddr_in sin;
	struct endpoint_udp_info info;
	int rlen;

	TCP_DBPRINTF(("Starting udp server for endpoint\n"));

#if	SOL_VERSION >= __s11
	ksocket_t	ksp;
#else	// SOL_VERSION >= __s11
	vnode_t *avp;
#endif	// SOL_VERSION >= __s11

retry:
	binary = 1;

	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(UDP_EP_PORT);
#if	SOL_VERSION >= __s11
	error = ksocket_socket(&ksp, AF_INET, SOCK_DGRAM, 0,
	    KSOCKET_SLEEP, CRED());
	if (error) {
		TCP_DBPRINTF(("tcp_transport::start_udp_server_ep: "
		    "ksocket create failed with error = %d\n", error));
		return (error);
	}
	error = ksocket_setsockopt(ksp, SOL_SOCKET, SO_REUSEADDR,
	    (void *)&binary, (t_uscalar_t)sizeof (binary), CRED());
	if (error) {
		ksocket_close(ksp, CRED());
		TCP_DBPRINTF(("tcp_transport::start_udp_server_ep: "
		    " Error in set soreuseaddr %d\n", error));
		return (error);
	}

	TCP_DBPRINTF(("Port number in udp server (EP) is %d\n", UDP_EP_PORT));

	error = ksocket_bind(ksp, (struct sockaddr *)&server,
	    (socklen_t)sizeof (server), CRED());
	if (error) {
		ksocket_close(ksp, CRED());
		TCP_DBPRINTF(("start_udp_server_ep: ksocket_bind failed "
		    " with error = %d\n", error));
		return (error);
	}

	so = KSTOSO(ksp);
#else	// SOL_VERSION >= __s11
	avp = solookup(AF_INET, SOCK_DGRAM, 0, NULL, &error);
	if (avp == NULL) {
		TCP_DBPRINTF(("tcp_transport::start_udp_server_ep: "
		    "solookup failed with error = %d\n", error));
		return (error);
	}

	so = socreate(avp, AF_INET, SOCK_DGRAM, 0, SOV_SOCKSTREAM, NULL,
		&error);
	if (so == NULL) {
		TCP_DBPRINTF(("tcp_transport::start_udp_server_ep: "
		    "socreate failed with error = %d\n", error));
		return (error);
	}

	error = sosetsockopt(so, SOL_SOCKET, SO_REUSEADDR, (void *)&binary,
		(t_uscalar_t)sizeof (binary));
	if (error) {
		soclose(so);
		TCP_DBPRINTF(("tcp_transport::start_udp_server_ep: Error in set"
		    "soreuseaddr %d\n", error));
		return (error);
	}

	TCP_DBPRINTF(("Port number in udp server (EP) is %d\n", UDP_EP_PORT));

	error = sobind(so, (struct sockaddr *)&server,
		(socklen_t)sizeof (server), 0, 0);
	if (error) {
		soclose(so);
		TCP_DBPRINTF(("start_udp_server_ep: sobind failed with error "
		    "= %d\n", error));
		return (error);
	}
#endif	// SOL_VERSION >= __s11
	//
	// Continue while transport is active
	//
	while (!is_shutdown) {
		bzero(&info, sizeof (info));
		bzero(&sin, sizeof (sin));
		rlen = tcptr_recvfrom(so, (char *)&info, (long)sizeof (info),
		    (struct sockaddr *)&sin, (socklen_t)sizeof (sin));
		if (rlen == -1) {
			TCP_DBPRINTF(("tcp_transport::start_udp_server_ep: "
			    "sorecvmsg failed. error = %d\n", error));
#if	SOL_VERSION >= __s11
			ksocket_close(ksp, CRED());
#else	// SOL_VERSION >= __s11
			soclose(so);
#endif	// SOL_VERSION >= __s11
			goto retry;
		}

		ASSERT(rlen == (int)sizeof (info));
		ipaddr = sin.sin_addr.s_addr;

		//
		// Get the endpoint for that node
		//
		tcp_endpoint *ep = get_tcp_endpoint_for_ipaddr(ipaddr);
		if (ep == NULL) {
			//
			// No Endpoint.
			//
			TCP_DBPRINTF(("start_udp_server_ep; No Endpoint\n"));
			continue;
		}
		ep->udp_putq(&info, &sin);
		ep->rele();
	}

	TCP_DBPRINTF(("start_udp_server_ep(): End\n"));
#if	SOL_VERSION >= __s11
	ksocket_close(ksp, CRED());
#else	// SOL_VERSION >= __s11
	soclose(so);
#endif	// SOL_VERSION >= __s11

	return (0);
}


//
// int
// tcp_transport::start_server_pe()
// 	Start the server that listens for TCP connections (pathend only).
//
// Description:
//	This function creates a server socket and listens and accepts
//	incoming connections for tcp pathends.
//	Once a connection is accepted, it finds the tcp pathend from
//	the remote ip address, the remote node, and the remote adapter id
//	(XXX: this is changing as more clconf routines are enhanced).
//	It then hands over the connection to the tcp pathend using
//	so_putq().
//
//	This is a method in the tcp transport class.
//
//	If an error occurs, this routine closes the connection.
//	This will trigger the other side to act.
//
// Parameters:
//	None.
//
// Returns:
//	None.
//
// See Also:
//	tcp_pathend::client_initiate(), tcp_pathend::so_getq(),
//	tcp_pathend::so_putq().
//
int
tcp_transport::start_server_pe()
{
	struct sonode *so, *newso;
	struct sockaddr_in server;
	int error;
	int binary;

	TCP_DBPRINTF(("Starting pathend server\n"));
#if	SOL_VERSION >= __s11
	ksocket_t		ksp, newksp;
	struct sockaddr_in	client;
	socklen_t		addrlen;
#else	// SOL_VERSION >= __s11
	vnode_t *avp;
#endif	// SOL_VERSION >= __s11
retry_accept:
	binary = 1;

	// Set the timeout to give up connection establishment to a
	// reasonable value.
	int cabort_timeout = tcp_transport_conn_abort_threshold *
	    MSECS_PER_SEC;

	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(TCP_PE_PORT);
#if	SOL_VERSION >= __s11
	error = ksocket_socket(&ksp, AF_INET, SOCK_STREAM, 0,
	    KSOCKET_SLEEP, CRED());
	if (error) {
		TCP_DBPRINTF(("ksocket create failed with error = %d\n",
		    error));
		return (error);
	}

	error = ksocket_setsockopt(ksp, SOL_SOCKET, SO_REUSEADDR,
	    (void *)&binary, (t_uscalar_t)sizeof (binary), CRED());
	if (error) {
		ksocket_close(ksp, CRED());
		TCP_DBPRINTF(("tcp_transport::start_server_pe: Error in set "
		    "soreuseaddr %d\n", error));
		return (error);
	}

	error = ksocket_setsockopt(ksp, IPPROTO_TCP,
	    TCP_CONN_ABORT_THRESHOLD, (void *)&cabort_timeout,
	    (t_uscalar_t)sizeof (cabort_timeout), CRED());
	if (error) {
		ksocket_close(ksp, CRED());
		TCP_DBPRINTF(("start_server_pe: Error in set cabort "
		    "timeout %d\n", error));
		return (error);
	}

	error = ksocket_bind(ksp, (struct sockaddr *)&server,
	    (socklen_t)sizeof (server), CRED());
	if (error) {
		ksocket_close(ksp, CRED());
		TCP_DBPRINTF(("ksocket_bind failed with error = %d\n", error));
		return (error);
	}

	error = ksocket_listen(ksp, TCP_BACKLOG_CONNECTIONS, CRED());
	if (error) {
		ksocket_close(ksp, CRED());
		TCP_DBPRINTF(("ksocket_listen failed with error %d\n", error));
		return (error);
	}

	addrlen = (socklen_t)sizeof (client);
#else	// SOL_VERSION >= __s11
	avp = solookup(AF_INET, SOCK_STREAM, 0, NULL, &error);
	if (avp == NULL) {
		TCP_DBPRINTF(("solookup failed with error = %d\n", error));
		return (error);
	}

	so = socreate(avp, AF_INET, SOCK_STREAM, 0, SOV_SOCKSTREAM, NULL,
		&error);
	if (so == NULL) {
		TCP_DBPRINTF(("socreate failed with error = %d\n", error));
		return (error);
	}

	error = sosetsockopt(so, SOL_SOCKET, SO_REUSEADDR, (void *)&binary,
		(t_uscalar_t)sizeof (binary));
	if (error) {
		soclose(so);
		TCP_DBPRINTF(("tcp_transport::start_server_pe: Error in set "
		    "soreuseaddr %d\n", error));
		return (error);
	}

	error = sosetsockopt(so, IPPROTO_TCP, TCP_CONN_ABORT_THRESHOLD,
	    (void *)&cabort_timeout, (t_uscalar_t)sizeof (cabort_timeout));
	if (error) {
		soclose(so);
		TCP_DBPRINTF(("start_server_pe: Error in set cabort "
		    "timeout %d\n", error));
		return (error);
	}

	TCP_DBPRINTF(("Port number in server is %d\n", TCP_PE_PORT));

	error = sobind(so, (struct sockaddr *)&server,
		(socklen_t)sizeof (server), TCP_BACKLOG_CONNECTIONS, 0);
	if (error) {
		soclose(so);
		TCP_DBPRINTF(("sobind failed with error = %d\n", error));
		return (error);
	}

	error = solisten(so, TCP_BACKLOG_CONNECTIONS);
	if (error) {
		soclose(so);
		TCP_DBPRINTF(("solisten failed with error %d\n", error));
		return (error);
	}
#endif
	// Continue while transport is active
	while (!is_shutdown) {
		newso = NULL;
#if	SOL_VERSION >= __s11
		error = ksocket_accept(ksp, (struct sockaddr *)&client,
		    &addrlen, &newksp, CRED());
		if (error) {
			TCP_DBPRINTF(("start_server_pe: Error in "
			    "ksocket_accept %d\n", error));
			if (error == EPROTO || error == ECONNABORTED) {
				// Peer closed connection before it was
				// accepted, ignore it.
				continue;
			}
			ksocket_close(ksp, CRED());
			goto retry_accept;
		}
		newso = KSTOSO(newksp);
		tcp_pathend *tpe =
		    get_tcp_pathend_for_ipaddr(client.sin_addr.s_addr);

		if (tpe == NULL) {
			// No Pathend.
			TCP_DBPRINTF(("start_server_pe; No Pathend\n"));
			ksocket_close(newksp, CRED());
			continue;
		}
		if (tpe->so_putq(newso) != 0) {
			// Cannot queue socket.
			// Must be in the wrong state.
			ksocket_close(newksp, CRED());
		}
		tpe->get_pathend_rele();
#else	// SOL_VERSION >= __s11
		error = soaccept(so, FREAD|FWRITE, &newso);
		if (error) {
			TCP_DBPRINTF(("start_server_pe: Error in "
					"soaccept %d\n", error));
			if (error == EPROTO || error == ECONNABORTED) {
				// Peer closed connection before it was
				// accepted, ignore it.
				continue;
			}
			soclose(so);
			goto retry_accept;
		}
		tcp_pathend *tpe = get_tcp_pathend(newso);
		if (tpe == NULL) {
			// No Pathend.
			TCP_DBPRINTF(("start_server_pe; No Pathend\n"));
			(void) sodisconnect(newso, -1, 0);
			soclose(newso);
			continue;
		}
		if (tpe->so_putq(newso) != 0) {
			// Cannot queue socket.
			// Must be in the wrong state.
			(void) sodisconnect(newso, -1, 0);
			soclose(newso);
		}
		tpe->get_pathend_rele();
#endif	// SOL_VERSION >= __s11
	}

	TCP_DBPRINTF(("start_server_pe(): End\n"));
#if	SOL_VERSION >= __s11
	ksocket_close(ksp, CRED());
#else	// SOL_VERSION >= __s11
	soclose(so);
#endif	// SOL_VERSION >= __s11

	return (0);
}


//
//
// int
// tcp_transport::start_server_ep()
// 	Start the server that listens for TCP connections (endpoint only).
//
// Description:
//	This function creates a server socket and listens and accepts
//	incoming connections for tcp endpoints.
//	Once a connection is accepted, it finds the tcp endpoint from
//	the remote ip address, the remote node, and the remote adapter id
//	(XXX: this is changing as more clconf routines are enhanced).
//	It then hands over the connection to the tcp endpoint using
//	so_putq().
//
//	This is a method in the tcp transport class.
//
//	If an error occurs, this routine closes the connection.
//	This will trigger the other side to act.
//
// Parameters:
//	None.
//
// Returns:
//	None.
//
// See Also:
//	tcp_endpoint::client_initiate(), tcp_endpoint::so_getq(),
//	tcp_endpoint::so_putq().
//
// Notes:
// 	Note that the port number is different for endpoints.
//
int
tcp_transport::start_server_ep()
{
	struct sonode *so, *newso;
	tcp_endpoint *ep;
	struct sockaddr_in server;
	int error;
	int binary;

	TCP_DBPRINTF(("Starting TCP Endpoint Server\n"));
#if	SOL_VERSION >= __s11
	ksocket_t		ksp, newksp;
	struct sockaddr_in	client;
	socklen_t		addrlen;
#else	// SOL_VERSION >= __s11
	vnode_t *avp;
#endif	// SOL_VERSION >= __s11
	binary = 1;

	// Set the timeout to give up connection establishment to a
	// reasonable value.
	int cabort_timeout = tcp_transport_conn_abort_threshold *
	    MSECS_PER_SEC;

	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(TCP_EP_PORT);
retry_accept:
#if	SOL_VERSION >= __s11
	error = ksocket_socket(&ksp, AF_INET, SOCK_STREAM, 0,
	    KSOCKET_SLEEP, CRED());
	if (error) {
		TCP_DBPRINTF(("ksocket_create failed with error = %d\n",
		    error));
		return (error);
	}

	error = ksocket_setsockopt(ksp, SOL_SOCKET, SO_REUSEADDR,
	    (void *)&binary, (t_uscalar_t)sizeof (binary), CRED());
	if (error) {
		ksocket_close(ksp, CRED());
		TCP_DBPRINTF(("tcp_transport::start_server_ep: Error in set "
		    "SO_REUSEADDR %d\n", error));
		return (error);
	}

	error = ksocket_setsockopt(ksp, IPPROTO_TCP, TCP_CONN_ABORT_THRESHOLD,
	    (void *)&cabort_timeout, (t_uscalar_t)sizeof (cabort_timeout),
	    CRED());
	if (error) {
		ksocket_close(ksp, CRED());
		TCP_DBPRINTF(("start_server_ep: Error in set cabort "
		    "timeout %d\n", error));
		return (error);
	}

	error = ksocket_bind(ksp, (struct sockaddr *)&server,
	    (socklen_t)sizeof (server), CRED());
	if (error) {
		ksocket_close(ksp, CRED());
		TCP_DBPRINTF(("ksocket_bind failed with error = %d\n", error));
		return (error);
	}

	error = ksocket_listen(ksp, TCP_BACKLOG_CONNECTIONS, CRED());
	if (error) {
		ksocket_close(ksp, CRED());
		TCP_DBPRINTF(("ksocket_listen failed with error %d\n", error));
		return (error);
	}

	addrlen = (socklen_t)sizeof (client);
#else	// SOL_VERSION >= __s11
	avp = solookup(AF_INET, SOCK_STREAM, 0, NULL, &error);
	if (avp == NULL) {
		TCP_DBPRINTF(("solookup failed with error = %d\n", error));
		return (error);
	}

	so = socreate(avp, AF_INET, SOCK_STREAM, 0, SOV_SOCKSTREAM, NULL,
		&error);
	if (so == NULL) {
		TCP_DBPRINTF(("socreate failed with error = %d\n", error));
		return (error);
	}

	error = sosetsockopt(so, SOL_SOCKET, SO_REUSEADDR, (void *)&binary,
		(t_uscalar_t)sizeof (binary));
	if (error) {
		soclose(so);
		TCP_DBPRINTF(("tcp_transport::start_server_ep: Error in set "
		    "soreuseaddr %d\n", error));
		return (error);
	}

	error = sosetsockopt(so, IPPROTO_TCP, TCP_CONN_ABORT_THRESHOLD,
	    (void *)&cabort_timeout, (t_uscalar_t)sizeof (cabort_timeout));
	if (error) {
		soclose(so);
		TCP_DBPRINTF(("start_server_ep: Error in set cabort "
		    "timeout %d\n", error));
		return (error);
	}

	TCP_DBPRINTF(("Port number in server is %d\n", TCP_EP_PORT));

	error = sobind(so, (struct sockaddr *)&server,
		(socklen_t)sizeof (server), TCP_BACKLOG_CONNECTIONS, 0);
	if (error) {
		soclose(so);
		TCP_DBPRINTF(("sobind failed with error = %d\n", error));
		return (error);
	}

	error = solisten(so, TCP_BACKLOG_CONNECTIONS);
	if (error) {
		soclose(so);
		TCP_DBPRINTF(("solisten failed with error %d\n", error));
		return (error);
	}
#endif	// SOL_VERSION >= __s11
	// Continue while transport is active
	while (!is_shutdown) {
		newso = NULL;
#if	SOL_VERSION >= __s11
		error = ksocket_accept(ksp, (struct sockaddr *)&client,
		    &addrlen, &newksp, CRED());
		if (error) {
			TCP_DBPRINTF(("start_server_ep: Error in "
			    "ksocket_accept %d\n", error));
			if (error == EPROTO || error == ECONNABORTED) {
				// Peer closed connection before it was
				// accepted, ignore it.
				continue;
			}
			ksocket_close(ksp, CRED());
			goto retry_accept;
		}

		newso = KSTOSO(newksp);
		//
		// Get the endpoint for that node
		//

		ep = get_tcp_endpoint_for_ipaddr(client.sin_addr.s_addr);
		if (ep == NULL) {
			// No Endpoint.
			TCP_DBPRINTF(("start_server_ep: No Endpoint\n"));
			ksocket_close(newksp, CRED());
			continue;
		}
		if (ep->so_putq(newso) != 0) {
			//
			// Cannot queue socket.
			// Must be in the wrong state.
			//

			ksocket_close(newksp, CRED());
		}
#else	// SOL_VERSION >= __s11
		error = soaccept(so, FREAD|FWRITE, &newso);
		if (error) {
			TCP_DBPRINTF(("start_server_ep: Error in "
					"soaccept %d\n", error));
			if (error == EPROTO || error == ECONNABORTED) {
				// Peer closed connection before it was
				// accepted, ignore it.
				continue;
			}
			soclose(so);
			goto retry_accept;
		}

		//
		// Get the endpoint for that node
		//
		ep = get_tcp_endpoint(newso);
		if (ep == NULL) {
			// No Endpoint.
			TCP_DBPRINTF(("start_server_ep: No Endpoint\n"));
			(void) sodisconnect(newso, -1, 0);
			soclose(newso);
			continue;
		}
		if (ep->so_putq(newso) != 0) {
			//
			// Cannot queue socket.
			// Must be in the wrong state.
			//
			(void) sodisconnect(newso, -1, 0);
			soclose(newso);
		}
#endif
		ep->rele();
		ep = NULL;
	}

	TCP_DBPRINTF(("start_server_ep(): End\n"));
#if	SOL_VERSION >= __s11
	ksocket_close(ksp, CRED());
#else	// SOL_VERSION >= __s11
	soclose(so);
#endif	// SOL_VERSION >= __s11

	return (0);
}



//
// adapter *
// tcp_transport::new_adapter(clconf_adapter_t *ap)
//	Create a New tcp adapter object.
//
//
adapter *
tcp_transport::new_adapter(clconf_adapter_t *ap)
{
	return (new tcp_adapter(this, ap));
}

//
// This returns with a pathend_hold on the pathend, caller is responsible
// for calling get_pathend_rele() when done.
//
tcp_pathend *
tcp_transport::get_tcp_pathend_for_ipaddr(in_addr_t ipaddr)
{
	int adapter_id;
	nodeid_t rnode_id;
	nodeid_t rn;
	tcp_pathend *tpe;

	if (ipaddr == (in_addr_t)-1)
		return (NULL);

	rn = clconf_cluster_get_nodeid_for_ipaddr(&rnode_id, &adapter_id,
		ipaddr);
	if ((rn == NODEID_UNKNOWN) || (!orb_conf::node_configured(rn))) {
		ETCP_DBPRINTF(TCPTR_TRACE_PE_CONNECT, ("Received a connect "
			"request from an unknown IP address 0x%x", ipaddr));
		return (NULL);
	}

	ASSERT(rnode_id == rn);

	// Get the pathend object based on the remote node id and the
	// remote (second arg = false indicates that) adapter id.
	tpe = (tcp_pathend *)get_pathend(adapter_id, false, rnode_id);
	if (tpe == NULL) {
		TCP_DBPRINTF(("No pathend for adapter_id %u rnode %u and"
		    " remote ipaddr 0x%x.", adapter_id, rnode_id, ipaddr));
	}

	return (tpe);
}

//
// This returns with a hold() on the endpoint. Caller should do a rele() when
// done with the endpoint
//
tcp_endpoint *
tcp_transport::get_tcp_endpoint_for_ipaddr(in_addr_t ipaddr)
{
	tcp_pathend	*tpe;
	tcp_endpoint	*tep;

	tpe = get_tcp_pathend_for_ipaddr(ipaddr);
	if (tpe == NULL)
		return (NULL);

	tep_list_lock.lock();
	DList<tcp_endpoint>::ListIterator iter(endp_list);
	while ((tep = iter.get_current()) != NULL) {
		if (tep->is_same_pathend(tpe)) {
			tep->hold();
			break;
		}
		iter.advance();
	}
	tep_list_lock.unlock();
	tpe->get_pathend_rele(); // corresponding to get_tcp_pathend_for_ipaddr
	return (tep);
}


//
// Override of pm_client virtual method. For each  endpoint
// the tcp_endpoint::timeouts_disable method is called
//
void
tcp_transport::timeouts_disable()
{
	endpoint	*ep;
	tcp_endpoint	*tep;
	nodeid_t	nid;
	IntrList<endpoint, _SList>::ListIterator	iter;

	// A maximum threshold is equivalent to disabling timeouts
	tcp_transport_ep_abort_orig = tcp_transport_ep_abort_threshold;
	tcp_transport_ep_abort_threshold = MAX_EP_ABORT_THRESHOLD;

	for (nid = 1; nid <= NODEID_MAX; nid++) {
		lock_node(nid);
		IntrList<endpoint, _SList> &ep_list = the_endpoints[nid];
		uint64_t current_ep_incn;

		// Store the current incarnation number of the
		// endpoint list. This is used later to check if
		// there were any changes to the endpoint list
		// during the time we release the lock on the list.
		current_ep_incn = the_endpoints_incn[nid];
		iter.reinit(ep_list);
		while ((ep = iter.get_current()) != NULL) {
			tep = (tcp_endpoint *)ep;
			iter.advance();
			if (tep->timeouts_are_disabled())
				continue;
			tep->hold();
			unlock_node(nid);
			ep->timeouts_disable();
			tep->rele();
			lock_node(nid);
			// Check if the endpoint list has changed
			// during the time we released the lock.
			// If changed, reinit the list iterator.
			if (current_ep_incn != the_endpoints_incn[nid]) {
				current_ep_incn = the_endpoints_incn[nid];
				iter.reinit(ep_list);
			}
		}
		unlock_node(nid);
	}
}

//
// Override of pm_client virtual method. For each endpoint
// the tcp_endpoint::timeouts_enable method is called
//
void
tcp_transport::timeouts_enable()
{
	endpoint	*ep;
	tcp_endpoint	*tep;
	nodeid_t	nid;
	IntrList<endpoint, _SList>::ListIterator	iter;

	// Restoring the original threshold is equivalent to enabling timeouts
	tcp_transport_ep_abort_threshold = tcp_transport_ep_abort_orig;

	for (nid = 1; nid <= NODEID_MAX; nid++) {
		lock_node(nid);
		IntrList<endpoint, _SList> &ep_list = the_endpoints[nid];
		uint64_t current_ep_incn;

		// Store the current incarnation number of the
		// endpoint list. This is used later to check if
		// there were any changes to the endpoint list
		// during the time we release the lock on the list.
		current_ep_incn = the_endpoints_incn[nid];
		iter.reinit(ep_list);
		while ((ep = iter.get_current()) != NULL) {
			tep = (tcp_endpoint *)ep;
			iter.advance();
			if (tep->timeouts_are_enabled())
				continue;
			tep->hold();
			unlock_node(nid);
			ep->timeouts_enable();
			tep->rele();
			lock_node(nid);
			// Check if the endpoint list has changed
			// during the time we released the lock.
			// If changed, reinit the list iterator.
			if (current_ep_incn != the_endpoints_incn[nid]) {
				current_ep_incn = the_endpoints_incn[nid];
				iter.reinit(ep_list);
			}
		}
		unlock_node(nid);
	}
	//
	// wake up the keep_active task(s)
	//
	task_lock.lock();
	task_cv.broadcast();
	task_lock.unlock();
}

//
// Function : transport(constructor)
//
// Purpose : to construct!
//
// Description:
//	Two server tasks are created. They listen for incoming
//	connections for the tcp pathend and the tcp endpoint
//	respectively.
//
//
tcp_transport::tcp_transport() :
	transport("tcp", true),
	is_shutdown(false),
	pe_udp_recv_buf(NULL),
	pe_udp_recv_buf_size(0),
	rio_threadpool(false, 0, "tcp transport replyio"),
	tcptr_threadpool(false, 5, "tcp transport listeners"),
	task_count(0)
{
	//
	// Allocate the buffer that will be used to receive pathend udp
	// handshake messages.
	//
	pe_udp_recv_buf_size = sizeof (struct pathend_udp_info) +
	    MAX_MAC_ADDR_SIZE + vm_comm::max_btstrp_np_string();
	pe_udp_recv_buf = (char *)kmem_alloc(pe_udp_recv_buf_size, KM_SLEEP);

	//
	// Post tasks to start servers which listen
	// for tcp/udp connections on pathend and endpoint.
	// We use tcptr_threadpool for these tasks as these
	// are long lived tasks which do not exit till the transport
	// is shutdown
	//
	post(new start_server_task(this, TCP_PATHEND, IPPROTO_TCP));
	post(new start_server_task(this, TCP_PATHEND, IPPROTO_UDP));

	post(new start_server_task(this, TCP_ENDPOINT, IPPROTO_TCP));
	post(new start_server_task(this, TCP_ENDPOINT, IPPROTO_UDP));

	post(new tcp_keep_active_task(this));

	//
	// Add the panic msg callback. Solaris will call this
	// function when a system panic occurs
	//
	callb_add(cl_transport_panic_cb, NULL, CB_CL_PANIC,
	    "Cluster panic notifier");
}


//
// tcp_transport::keep_active
//
// Attempts to send a "keep active" message on each tcp_endpoint periodically
// until its time to shutdown. The goal is to keep traffic flowing on the
// endpoints in absence of cluster infrastructure traffic so that any TCP/IP
// communication problems can be detected through TCP connection drops.
//
// The tcp_endpoint::send_keep_active_message method is called to actually send
// the message. That method is also responsible for ensuring that it does not
// send a "keep active" message on an endpoint that has already been active
// due to regular data flow.
//
// The frequency at which attempt is made to send the "keep active" message
// is tcp_transport_ep_abort_threshold / 10.
//
// This method is called from tcp_keep_active_task::execute.
//
void
tcp_transport::keep_active()
{
	nodeid_t	ndid;
	endpoint	*ep;
	tcp_endpoint	*tep;
	clock_t		start_lbolt;
	clock_t		end_lbolt;
	IntrList<endpoint, _SList>::ListIterator	iter;

	//
	// The transport object maintains a list of all its endpoints in an
	// array of per node lists. The array is indexed by nodeids.
	//
	for (ndid = 1; !is_shutdown; ndid = (ndid % NODEID_MAX) + 1) {
		start_lbolt = ddi_get_lbolt();
		node_locks[ndid].lock();
		IntrList<endpoint, _SList> &ep_list = the_endpoints[ndid];
		// Store the current incarnation number of the
		// endpoint list. This is used later to check if
		// there were any changes to the endpoint list
		// during the time we release the lock on the list.
		uint64_t current_ep_incn = the_endpoints_incn[ndid];
		iter.reinit(ep_list);
		while (!is_shutdown && (ep = iter.get_current()) != NULL) {
			tep = (tcp_endpoint *)ep;
			iter.advance();
			tep->tep_lock();
			if (!tep->is_endpoint_registered()) {
				tep->tep_unlock();
				continue;
			}
			tep->hold();
			tep->tep_unlock();
			node_locks[ndid].unlock();
			tep->send_keep_active_message();
			tep->rele();
			node_locks[ndid].lock();
			// Check if the endpoint list has changed
			// during the time we released the lock.
			// If changed, reinit the list iterator.
			if (current_ep_incn != the_endpoints_incn[ndid]) {
				current_ep_incn = the_endpoints_incn[ndid];
				iter.reinit(ep_list);
			}
		}
		node_locks[ndid].unlock();
		end_lbolt = ddi_get_lbolt();

		//
		// Sleep for one-tenth of the tcp_transport_ep_abort_threshold
		// interval at the end of every complete pass through all
		// endpoints. We don't want to be sending the keep active
		// message too often. At the same time we want to be responsive
		// enough in the case of failures.
		//
		if (!is_shutdown && ndid == NODEID_MAX) {
			os::usec_t	itertime;
			os::usec_t	sleepdur;
			sleepdur =
			    (os::usec_t)tcp_transport_ep_abort_threshold *
			    USECS_IN_A_SEC / 10;
			//
			// abort timeouts are disabled thru cmm_ctl by setting
			// tcp_transport_ep_abort_threshold to a large value.
			// In that case the sleepdur computation overflows.
			// Hence, must take care to ensure a long sleepdur.
			// The task is signaled when timeouts are enabled.
			//
			if (tcp_transport_ep_abort_threshold > sleepdur)
				sleepdur = INT_MAX;
			itertime = (end_lbolt - start_lbolt) *
			    USECS_IN_A_TICK;
			if (itertime < sleepdur) {
				os::systime timo(sleepdur - itertime);
				task_lock.lock();
				(void) task_cv.timedwait(&task_lock, &timo);
				task_lock.unlock();
			}
		}
	}

	// Let the shutdown thread know we have exited.
	task_lock.lock();
	ASSERT(task_count > 0);
	task_count--;
	if (task_count == 0)
		task_cv.broadcast();
	task_lock.unlock();
}

void
tcp_transport::shutdown()
{
	TCP_DBPRINTF(("tcp_transport: shutting down\n"));
	// Shutdown the threadpool used for connection listeners
	tcptr_threadpool.shutdown();
	// Shutdown the threadpool used for replyio
	rio_threadpool.shutdown();
	//
	// It is the responsibility of the common transport
	// to have cleaned up all pathends and adapters before
	// shutting down this transport.

	// Shutdown arp cache monitoring
	arp_cache_monitor::the().shutdown();

	ASSERT(!is_shutdown);
	is_shutdown = true;

	task_lock.lock();

	// Let any sleeping tasks know that we are shutting down.
	task_cv.broadcast();

	// Wait for all tasks to exit.
	while (task_count > 0) {
		task_cv.wait(&task_lock);
	}
	task_lock.unlock();

	delete this;
}


tcp_transport::~tcp_transport()
{
	// We do not do a clean shutdown yet (see shutdown)
	// We should not be here
	ASSERT(0);

	if (pe_udp_recv_buf != NULL) {
		kmem_free(pe_udp_recv_buf, pe_udp_recv_buf_size);
	}
	pe_udp_recv_buf = NULL;

	ASSERT(the_tcp_transport == this);
	the_tcp_transport = NULL;
}


//
// tcp_sendstream methods
//

//
// constructor - create an mblk based stream.
//
tcp_sendstream::tcp_MarshalStream::tcp_MarshalStream(Environment *e,
    uint_t byte_size, uint_t chunk_size, tcp_endpoint *ep) :
	MarshalStream(e, byte_size, chunk_size),
	endp(ep)
{
}

tcp_sendstream::~tcp_sendstream()
{
	MainBuf.dispose();
	if (endp->replyio_acks_used()) {
		// If there are any offline_pending for acks then we need to
		// wait for them here. This could happen if a send is aborted
		// partway through the marshalling process without actually
		// calling send.
		(void) wait_for_replyios();
	}
	// Either the endpoint has to have been unregistered,
	// or there should be no pending acks
	ASSERT(!endp->is_endpoint_registered() ||
		((offline_pending == 0) && !ack_pending));

	endp->rele();	// corresponds to hold in constructor
	endp = NULL;	// Make lint happy
}

//
// newbuf - tcp streams use mblk buffers.
//
Buf *
tcp_sendstream::tcp_MarshalStream::newbuf(uint_t nbytes)
{
	// endpoint mtu_size should have been initialized by now
	ASSERT(endp->get_mtu_size() != 0);

	// Limit nbytes to MTU size
	nbytes = MIN(nbytes, endp->get_mtu_size()) + //lint !e666
	    endp->get_wroff();

	Environment *e = get_env();
	os::mem_alloc_type flag = e->nonblocking_type();
	Buf *b;

	// Allocate an mblk of sufficient size
	mblk_t *mp = os::allocb(nbytes, BPRI_MED, flag);
	if (mp == NULL) {
		e->system_exception(CORBA::WOULDBLOCK(nbytes,
		    CORBA::COMPLETED_NO));
		return (NULL);
	}
	// Leave space for headers
	mp->b_rptr += endp->get_wroff();
	mp->b_wptr = mp->b_rptr;

	/*CSTYLED*/
	b = new (flag) MBlkBuf(mp, 0);
	if (b == NULL) {
		freeb(mp);
		e->system_exception(CORBA::WOULDBLOCK((uint_t)sizeof (MBlkBuf),
		    CORBA::COMPLETED_NO));
	}
	return (b);
}


//
// tcp_sendstream constructor
//	We allocate buffers of mtu_size (span() of each Buf is mtu_size)
//	so that TCP does not have to refragment. If the estimate for
//	the first buffer is too big (send_buffer_size) we cap it at
//	mtu_size
//
//	The first buffer of the marshal stream MainBuf
//	must be large enough to contain the message headers, because the
//	message headers cannot span buffers. See alloc_chunk call below
//
//
// XXX
// for the MIN macro lint gives the following warning:
// ../tcp_transport.cc:4432: Warning(666) [c:63]:
// Expression with side effects passed to repeated parameter 1 in macro MIN
// send_buffer_size should have been declared const
//
tcp_sendstream::tcp_sendstream(tcp_endpoint *ep, resources *new_resourcep) :
	sendstream(new_resourcep, &MainBuf),
	MainBuf(new_resourcep->get_env(),
	    MIN(new_resourcep->send_buffer_size(),
	    ep->get_mtu_size()), ep->get_mtu_size(), ep), //lint !e666
	endp(ep),
	prefer_conn(TCP_INVAL_CONN),
	replyio_ss(false),
	offline_pending(0),
	ack_pending(false),
	waiting_since(0)
{
	//
	// get a hold on the endpoint.
	//
	ep->hold();

	//
	// endpoint mtu_size should have been initialized by now
	// and be >= header_size
	//
	ASSERT(ep->get_mtu_size() >= resourcep->send_header_size());

	//
	// Allocate space for use by message headers
	//
	uint_t hdr_size;
	hdr_size = MainBuf.alloc_chunk(resourcep->send_header_size());
	ASSERT(hdr_size == resourcep->send_header_size() ||
	    CORBA::WOULDBLOCK::_exnarrow(get_env()->exception()));
}

ID_node &
tcp_sendstream::get_dest_node()
{
	return (endp->get_rnode());
}

//
// tcp_recstream methods
//

tcp_recstream::~tcp_recstream()
{
	if (_mp) {
		freemsg(_mp);
	}
}

ID_node &
tcp_recstream::get_src_node()
{
	return (src_node);
}

//
// tcp_recstream constructor -
// This method converts the lead mblk into a MBlkBuf structure.
//
tcp_recstream::tcp_recstream(mblk_t *mp, ID_node &src, orb_msgtype msgt,
    tcp_endpoint *endp) :
	recstream(msgt, invocation_mode::ONEWAY, endp), // Default oneway msg
	src_node(src),
	_mp(mp)
{
	// Indicate have not read message header
	mh.mlen = 0;

	ASSERT(msgdsize(_mp) > sizeof (tcpmod_header_t));

	//
	// Strip tcpmod_header_t. Neglect return value as it must succeed.
	//
	(void) adjmsg(_mp, sizeof (tcpmod_header_t));

	ASSERT(_mp != NULL);

	//
	// Purge any 0-length mblks
	//
	mblk_t	*first_mblkp;
	while (((first_mblkp = _mp) != NULL) && (MBLKL(first_mblkp) == 0)) {
		_mp = first_mblkp->b_cont;
		freeb(first_mblkp);
	}
	//
	// Perform non-blocking conversion of lead mblk to MBlkBuf
	// using pre-allocated MBlkBuf structure.
	// Any remaining unconverted mblk's remain in _mp
	//
	_mp = first_mblkp->b_cont;
	first_mblkp->b_cont = NULL;
	mblkbuf1.use_mblk(first_mblkp, (uint_t)MBLKL(first_mblkp));
	MainBuf.usebuf(&mblkbuf1);
}

//
// bool
// tcp_recstream::initialize(os::mem_alloc_type flag)
//	Initialize a tcp recstream.
//
// Description:
//	Any remaining mblk's are converted into MBlkBuf structures.
//	When present XdoorTab and Offline are split from MainBuf.
//
// Parameters:
//	os::mem_alloc_type flag		- sleep flag
//
// Returns:
//	false if initialize would have blocked and flag was os::NO_SLEEP
//	true otherwise.
//
bool
tcp_recstream::initialize(os::mem_alloc_type flag)
{
	MC_PROBE_0(tcp_recstream_init_start, "clustering tcp_transport", "");
	mblk_t *firstmp;

	//
	// If mh.mlen != 0, then it means this is a retry after the header was
	// read in and the mp to MBlkBuf conversion had succeeded.
	// _mp should be NULL in that case
	//
	ASSERT((mh.mlen == 0) || (_mp == NULL));

	// This method will attempt to convert any remaining
	// mblk's and load them into MainBuf.
	// The conversion operation can fail.
	// This method can be called multiple times until it succeeds.
	//
	// If there is an error in the middle we return with
	// converted Bufs in MainBuf and the rest in _mp
	//
	while ((firstmp = _mp) != NULL) {
		// Remove first mblk from chain
		_mp = _mp->b_cont;
		firstmp->b_cont = NULL;

		// catch any 0-length mblks
		if (MBLKL(firstmp) == 0) {
			freeb(firstmp);
			continue;
		}

		//
		// This MBlkBuf constructor does not fail except for
		// memory alloc failure. should return error and retry later
		//
		MBlkBuf *b;

		/*CSTYLED*/
		b = new (flag) MBlkBuf(firstmp, (uint_t)MBLKL(firstmp));
		if (b == NULL) {
			ASSERT(flag == os::NO_SLEEP);
			// restore _mp to original state
			firstmp->b_cont = _mp;
			_mp = firstmp;
			return (false);
		}
		ASSERT(b->base_address());
		MainBuf.usebuf(b);	// append to end of MainBuf
	}

	MainBuf.prepare();		// rewind cursor to beginning

	//
	// If this is a retry, then header may have been read in the
	// previous try and already stripped from the message,
	// we should use the old header and not try to read the header again
	//
	if (mh.mlen == 0) {
		MainBuf.read_header(&mh, sizeof (tcp_message_header));

		//
		// Adjust for trimmed header
		//
		ASSERT(mh.mlen >= (uint_t)sizeof (tcp_message_header));
		mh.mlen -= (uint_t)sizeof (tcp_message_header);
		ASSERT(mh.mlen != 0);		// do not support 0 len msgs

		xdoorcount = mh.xdnum;
	}

	//
	// The received data order is main-data (mandatory field),
	// xdoors (optional field), and offline-data (optional field).
	// The main-data includes all of the headers.
	// This section splits the three fields into separate streams:
	//	MainBuf, XdoorTab, OfflineBufs
	//
	if (xdoorcount) {
		//
		// Separate the main data from the optional fields.
		//
		if (!MainBuf.split_at_boundary(mh.mlen, XdoorTab, flag)) {
			ASSERT(flag == os::NO_SLEEP);
			//
			// All data still in MainBuf
			//
			return (false);
		}
		if (mh.offline_data && !XdoorTab.split_at_boundary(mh.xlen,
		    OffLineBufs, flag)) {
			ASSERT(flag == os::NO_SLEEP);
			MainBuf.useregion(XdoorTab.region());
			//
			// Leave all data in MainBuf
			//
			ASSERT(XdoorTab.empty());
			return (false);
		}
	} else if (mh.offline_data && !MainBuf.split_at_boundary(mh.mlen,
	    OffLineBufs, flag)) {
		ASSERT(flag == os::NO_SLEEP);
		//
		// All data still in MainBuf
		//
		return (false);
	}
	//
	// MainBuf and XdoorTab should now have cursors at start of regions
	//
	MC_PROBE_0(tcp_recstream_init_end, "clustering tcp_transport", "");
	return (true);
}

void
tcp_sendstream::recvd_rio_ack(uint_t conn)
{
	MC_PROBE_0(recvd_rio_ack, "clustering tcp_transport", "");

	ETCP_DBPRINTF(TCPTR_TRACE_RIO_MINIMAL,
	    ("EP%u:%u:%u received rio ack sendstream %p endp %p"
	    " offline_pending %d.\n", endp->get_rnode().ndid,
	    endp->local_adapter_id(), conn, this, endp, offline_pending));
	if (!endp->is_rio_conn(conn)) {
		ASSERT(!"Received rio ack on a non rio connection.");
	}
	ASSERT(endp->tep_lock_held());
	ASSERT(offline_pending > 0);
	offline_pending--;
	if (offline_pending == 0) {
#ifdef PER_SENDSTREAM_CV
		ack_cv.signal();
#else
		// Wake up all threads on endpoint
		endp->ack_cv.broadcast();
#endif
	}
}

void
tcp_sendstream::recvd_sync_ack(uint_t conn)
{
	MC_PROBE_0(recvd_sync_ack, "clustering tcp_transport", "");

	ETCP_DBPRINTF(TCPTR_TRACE_SYNC_ACK,
	    ("EP%u:%u:%u received sync ack sendstream %p endp %p"
	    " ack_pending %d.\n", endp->get_rnode().ndid,
	    endp->local_adapter_id(), conn, this,
	    endp, ack_pending));
	ASSERT(endp->tep_lock_held());
	ASSERT(ack_pending);
	ack_pending = false;
	waiting_since = 0;
#ifdef PER_SENDSTREAM_CV
	ack_cv.signal();
#else
	endp->ack_cv.broadcast();
#endif
}

//
// Wait for all the acks for pending offline replyios to be received
// or the endpoint is unregistered
// returns false if the endpoint is not registered
// returns true if the endpoint is registered and there are no pending replyios
// Note that this routine will return false even if there are no pending
// replyios if the endpoint is unregistered. So the return value from this
// routine can be used to check if the endpoint is unregistered also.
//
bool
tcp_sendstream::wait_for_replyios()
{
	// We can check this outside the lock as the thread that increments
	// offline_pending is the same thread that calls this routine
	// so we cannot be incrementing this at this point and if this
	// is 0, there cannot be any pending acks to decrement it
	if (offline_pending == 0) {
		return (endp->is_endpoint_registered());
	}
	endp->tep_lock();
	while (endp->is_endpoint_registered()) {
		if (offline_pending == 0) {
			endp->tep_unlock();
			return (true);
		}
#ifdef PER_SENDSTREAM_CV
		os::systime to(TCPTR_ACK_WAIT);
		(void) ack_cv.timedwait(endp->tep_get_lock(), &to);
#else
		endp->ack_cv.wait(endp->tep_get_lock());
#endif
	}
	endp->tep_unlock();
	return (false);
}

// wait_for_sync_ack waits for the ack from the receiver used for synchronous
// sends. tcp_endpoint::send is the caller and would have set the variable
// ack_pending in the sendstream before calling this
// returns true if there the ack was received
// returns false if the ack was not received and the endpoint is not registered
// Note that this is slightly different semantics from wait_for_replyios above
bool
tcp_sendstream::wait_for_sync_ack()
{
	//
	// For synchronous sends, tcpmod will send an ACK after
	// the message has been delivered to the orb_msg layer
	// at the receiver. The sender when receiving the ACK
	// will call recvd_sync_ack which will signal the cv
	// To handle a remote path failure before the ACK arrives
	// we exit in case the path is declared down
	//
	endp->tep_lock();

	// Record current time before starting the wait, used for debugging
	// lost sync ack scenarios.
	waiting_since = ddi_get_lbolt();

	while (ack_pending) {
		if (!endp->is_endpoint_registered()) {
			endp->tep_unlock();
			return (false);
		}
#ifdef PER_SENDSTREAM_CV
		os::systime to(TCPTR_ACK_WAIT);
		(void) ack_cv.timedwait(endp->tep_get_lock(), &to);
#else
		endp->ack_cv.wait(endp->tep_get_lock());
#endif
	}
	endp->tep_unlock();
	return (true);
}

//
// void
// tcp_sendstream::send(bool need_seq, orb_seq_t &seq)
//	tcp sendstream send.
//
// Description:
// 	sendstream comes in with a hold on the endpoint which was held
//	during the constructor.
//
//	After the sendstream was constructed in tcp_endpoint::get_sendstream
//	the ORB marshalling process would have constructed the message in
//	the sendstream.
//	MainBuf is of type tcp_sendstream::tcp_MarshalStream which allows
//		various methods to be overloaded. tcp_MarshalStream overloads
//		the newbuf method so we can allocate MBlkBuf (mblk) of the
//		right mtu_size with space for headers etc
//		in send(), we convert the MBlkBufs to a mblk chain.
//		Note that it is legal for the ORB to use different Buf types
//		than just MBlkBufs, so we always use the method get_mblk to
//		get the right behavior.
//
// 	XdoorTab needs to left unmodified as the reference counting code
//		depends on this for error recovery
//		So we copy the data in XdoorTab into mblks.
//		If there is space at the end of the last mblk in MainBuf
//		then we copy the contents of XdoorTab into that mblk
//		If there isnt sufficient space, we copy to a new mblk chain
//		and append to MainBuf's mblk chain.
//		XX We could explore a combination of the two mechanisms
//		We currently dont as the optimization works quite frequently
//
//	OffLineBufs contain bulk data that we did not want to copy to MainBuf
//		or will be read more efficiently by keeping them offline.
//		They are usually Bufs of different types (PpBuf, UioBuf, UBuf
//		are some examples). The Region class provides a get_mblk
//		function which is only used by the tcp_sendstream to convert
//		these Bufs into the corresponding mblk chain with the right
//		mtu_size and header space etc. These routines are all in
//		src/orb/Buf*
//		There are tricks we do with each Buf type and a lot of tuning
//		work is needed here. For some Buf types and for some sizes
//		copying the contents into an mblk chain is more efficient.
//		For others doing an esballoc is more efficient but requires
//		the caller to wait for the free function to be called.
//		For user bufs we have to copy before handing the data to tcp
//		as the stream head which usually does this translation is
//		shared across all threads in tcp_transport and cannot do this
//		copyin later.
//
//	Reply I/O sends.
//		For some invocations offline data is sent using
//		put_offline_reply/send_reply_buf to a preallocated memory
//		location on the receiver. tcp_transport implements these
//		by doing the send of a separate replyio connection and
//		waits for the acks to arrive before sending the rest of
//		the reply.
//		That is the wait_for_replyios() call in tcp_endpoint::send.
//		This ack protocol is needed so that when the sender receives
//		the reply message he can be sure that the message contents
//		have already been copied into the receiver destination memory
//		XX This ack protocol is currently costing close to a full
//		round trip (300us on 100Mbps ethernet) in latency and may
//		need to revisit the implementation. The alternative would
//		be to send the reply message along on the same connection
//		as the offline data and do the synchronization on the receiver
//
//	Sequence numbers:
//		In case the transport should return success only if it can
//		determine whether a synchronous	message got to the receiver or
//		for non-synchronous messages can guarantee that the
//		message will reach the receiver even if the path/endpoint fails
//		If it cannot do that, then it needs to use the message_mgr
//		to allocate a sequence number. This sequence number is used
//		by the ORB to resolve duplicates/delivery questions using an
//		alternate path in the event that the transport returns a
//		RETRY_NEEDED:COMPLETED_MAYBE exception.
//
// Exceptions raised:
//	Note that sendstream::send sets completion_status in exceptions
//	based on whether the message has been delivered to the receiver or not.
//	This is different from the way we use exceptions in other places where
//	the completion status refers to whether the skel had executed or not.
//	We do not use the same here as orb internal messages and reply messages
//	do not map well to the same semantic (among other reasons)
//
//		Any exceptions that happen during converting the sendstream
//		contents to mblks (alloc failures, copyin errors etc) are
//		returned to the caller with completion status COMPLETED_NO
//
//		If the transport knows that the remote node (the incarnation
//		was bound during sendstream creation to a particular endpoint)
//		incarnation is invalid (died/rebooted), then it can return
//		COMM_FAILURE exception
//
//		If the endpoint has changed state, or we hit other errors before
//		the message is actually given to tcp, then we return
//		RETRY_NEEDED::COMPLETED_NO exception
//
//		If the message has been handed off to tcp and the path times
//		out, the message is not guaranteed to arrive there.
//		We currently, wait for an end-to-end ack. If the ack does
//		not arrive we return RETRY_NEEDED::COMPLETED_MAYBE.
//		XX An RFE is being implemented to failover the connection
//		to a different adapter so we dont need the acks and we can
//		avoid the COMPLETED_MAYBE case and also avoid the sequence
//		number allocation.
//
// Parameters:
//	bool		need_seq	- whether the message needs a
//					  sequence number
//	orb_seq_t	&seq		- The sequence number allocated,
//					  or INVALID_ORBSEQ.
//
// See Also:
//	tcp_endpoint::send().
//	sendstream base class in src/orb/marshalstream.h
//
void
tcp_sendstream::send(bool need_seq, orb_seq_t &seq)
{
	MC_PROBE_0(tcp_send_start, "clustering tcp_transport", "");
	Environment	*e = get_env();
	uint_t		len, xlen, mlen;

	ASSERT(seq == INVALID_ORBSEQ);

	// The ORB handles any exceptions upto this point and there
	// should have been no exceptions in the environment, allowing
	// this routine to return errors that way
	ASSERT(!e->exception());

	//
	// If there are no xdoors, XdoorTab should be empty
	//
	ASSERT(xdoorcount || XdoorTab.empty());

	{
		//
		// Create header at the beginning of the main buffer
		// Space was reserved for this during the tcp_sendstream
		// constructor.
		//
		tcp_message_header *hdrp = (tcp_message_header *)make_header(
		    sizeof (tcp_message_header));
		mlen = hdrp->mlen = MainBuf.span();
		xlen = hdrp->xlen = XdoorTab.span();
		hdrp->xdnum = xdoorcount;
		hdrp->offline_data = OffLineBufs.empty() ? 0 : 1;

		ASSERT(hdrp->mlen >= (uint_t)sizeof (tcp_message_header));

		//
		// Calculate the total length of the message for later
		//
		len = mlen + xlen + OffLineBufs.span();

		//
		// This generates too many messages.
		// Enable them if debugging message
		// delivery interruption type problems.
		ETCP_DBPRINTF(TCPTR_TRACE_SEND,
		    ("EP%u:%u:x Sending M%d: MBuf(%d) XdoorTab(%d) (%d, %d)\n",
		    get_dest_node().ndid, endp->local_adapter_id(),
		    get_msgtype(), mlen, xlen, xdoorcount, hdrp->offline_data));

		// Note: Cannot use hdrp after this point
	}

	//
	// Concatenate the contents of the various MarshalStreams and
	// convert to mblks.
	// The order of the message is:
	// MainBuf (orb level headers are before the data), xdoors, offline
	//
	mblk_chain	mpchain;

	//
	// Note that we depend on Region2mblk/get_mblk methods to preserve
	// the reserved header space in at least the first mblk
	//
	mpchain.append_chain(MainBuf.Region2mblk(endp->get_wroff(),
	    endp->get_mtu_size(), endp->get_do_esballoc(), e));
	MC_PROBE_0(tcp_send_MainBuf, "clustering tcp_transport", "");

	ASSERT((mpchain.span() == mlen) || e->exception());

	//
	// Handle xdoortab only if there are no exceptions
	// Need to do a non-destructive copy
	//
	if (xdoorcount && !e->exception()) {
		UPDATE_TCP_STATS(xdoortab_msg, 1);
		mblk_t *tailmp = mpchain.last_mblk();
		ASSERT(tailmp != NULL);	// There is always a mainbuf

		//
		// If can piggyback xdoortab onto mainbuf, more efficient
		//
		if ((int)xlen <= MBLKTAIL(tailmp)) {
			UPDATE_TCP_STATS(xdoortab_msg_piggyback, 1);
			XdoorTab.region().copy_contents((char *)tailmp->b_wptr,
			    xlen, e);
			tailmp->b_wptr += xlen;
		} else {
			//
			// Unable to fit in tail, create new mblks and copy
			// XX We could potentially optimize further to copy
			// whatever we can above.
			// XX Need to investigate ways to avoid the copy
			// either using dupb or esballoc
			//
			mpchain.append_chain(XdoorTab.region().copy2mblk(
				endp->get_wroff(), endp->get_mtu_size(), e));
		}
	}
	ASSERT((mpchain.span() == (mlen + xlen)) || (e->exception()));
	MC_PROBE_0(tcp_send_XdoorTab, "clustering tcp_transport", "");

	// Convert any OffLineBufs contents into mblks
	// Region2mblk allocates mblks such that if the total length is
	// not a multiple of the mtu_size, the first mblk contains the
	// remainder (instead of the last). This is a heuristic that
	// exploits the knowledge that for most messages with OffLineBufs
	// the MainBuf is pretty small, hence the first 2 mblks in the
	// fully concatenated chain will actually fit in one ethernet
	// MTU. tcp/ip/ethernet drivers do this optimization (as of now)
	if (!OffLineBufs.empty() && !e->exception()) {
		UPDATE_TCP_STATS(offlinetab_msg, 1);
		mpchain.append_chain(OffLineBufs.Region2mblk(endp->get_wroff(),
		    endp->get_mtu_size(), endp->get_do_esballoc(), e));
	}

	if (e->exception()) {
		//
		// Free up any allocated mblks
		//
		mpchain.dispose();
		// Set completion status to be COMPLETED_NO explicitly
		e->sys_exception()->completed(CORBA::COMPLETED_NO);
		MC_PROBE_0(tcp_send_end, "clustering tcp_transport", "");
		return;
	}

	MC_PROBE_0(tcp_send_offline, "clustering tcp_transport", "");

	//
	// MainBuf and OfflineBufs should be empty as we did a destructive
	// copy above. XdoorTab, if exists, should not be empty.
	//
	ASSERT(MainBuf.empty());
	ASSERT(OffLineBufs.empty());
	ASSERT((xdoorcount == 0) || !XdoorTab.empty());
	ASSERT(len == mpchain.span());

	// We should not have any offline pending for nonblocking invocations
	ASSERT((offline_pending == 0) || !e->is_nonblocking());

	//
	// Allocate the sequence number from the message_mgr.  This is
	// done after copying to avoid using up unnecessary sequence numbers
	// and also to delay allocation till the last instant
	//

	if (need_seq &&
	    !message_mgr::the().send_prepare(get_dest_node(), seq)) {
		e->system_exception(CORBA::COMM_FAILURE(0,
		    CORBA::COMPLETED_NO));
		mpchain.dispose();
		MC_PROBE_0(tcp_send_end, "mc tcp", "");
		return;
	}

	//
	// make all blocking messages be synchronous.
	// In multiple adapter case we need to wait for ack before
	// we can return success. Else the ORB will not retry on failure.
	//
	endp->send(get_msgtype(), seq, mpchain.get_mblk_chain(), len,
	    !e->is_nonblocking(), this, e);

	//
	// endpoint::send or tcp frees mblks depending on where the
	// exception condition was discovered
	//

	UPDATE_TCP_STATS(msg_sent, 1);
	MC_PROBE_0(tcp_send_end, "clustering tcp_transport", "");
}

// task method called to start various listener threads
// XX There is a lot of code duplication in the routines called from here,
// We should parameterize and avoid the code duplication
void
start_server_task::execute()
{
	int	error = 0;
	int	last_error = 0;
	os::sc_syslog_msg msg("TCP TRANSPORT", "", NULL);

	//
	// Continue retrying until the server task is successful.
	//
	for (;;) {
		switch (objtype) {
		case TCP_PATHEND :
			if (server_type == IPPROTO_UDP) {
				error = transp->start_udp_server_pe();
			}
			if (server_type == IPPROTO_TCP) {
				error = transp->start_server_pe();
			}
			break;
		case TCP_ENDPOINT :
			if (server_type == IPPROTO_UDP) {
				error = transp->start_udp_server_ep();
			}
			if (server_type == IPPROTO_TCP) {
				error = transp->start_server_ep();
			}
			break;
		default :
			ASSERT((objtype == TCP_PATHEND) ||
			    (objtype == TCP_ENDPOINT));
			break;

		}
		if (error == 0) {
			//
			// Success.
			//
			break;
		}
		//
		// Server startup failed.
		// Syslog a message unless we have already logged
		// a similar message.
		//
		if ((error != last_error)) {
			//
			// SCMSGS
			// @explanation
			// TCP server to accept connections on the private
			// interconnect could not be started.
			// @user_action
			// Need a user action for this message.
			//
			(void) msg.log(SC_SYSLOG_WARNING, MESSAGE,
			    "%s %s server startup encountered"
			    " errors, errno = %d.",
			    (objtype == TCP_PATHEND) ? "Pathend" : "Endpoint",
			    server_type == IPPROTO_TCP ? "TCP" : "UDP", error);
			last_error = error;
		}
		//
		// Sleep and then retry
		//
		os::usecsleep(TCPTR_INITIATE_SLEEP);
	}

	transp->unpost();

	delete this;
}

//
// For the rest of the methods see tcp_transport_in.h
// They are all inline routines.
//

// Module linkage/_init/_fini routines for this module
extern "C" {
#include <sys/modctl.h>
#include <cplplrt/cplplrt.h>

/* inter-module dependencies */
char _depends_on[] = "misc/cl_runtime misc/cl_load misc/cl_orb "
	"misc/cl_haci misc/cl_quorum misc/cl_comm fs/specfs fs/sockfs";

static struct modlmisc modlmisc = {
	&mod_miscops, "ORB TCP Transport",
};

static struct modlinkage modlinkage = {
#ifdef _LP64
	MODREV_1, { (void *) &modlmisc, NULL, NULL, NULL, NULL, NULL, NULL }
#else
	MODREV_1, { (void *) &modlmisc, NULL, NULL, NULL }
#endif
};

int
_init(void)
{
	int error;

	if ((error = mod_install(&modlinkage)) != 0) {
		return (error);
	}
	_cplpl_init();		/* C++ initialization */

	//
	// XXX
	// lint gives the following error:
	// ../tcp_transport.cc:5021: Error(15) [c:45]:
	// operator new(unsigned int) redeclared
	// (Arg. no. 1: nominal) (location unknown)
	//
	the_tcp_transport = new tcp_transport; //lint !e15

	// Initialize the raw dlpi transport
	_raw_dlpi_init();

	// Initialize the arp cache monitor
	_arp_cache_monitor_init();

	// Add the transport as a pm_client to get a playback of adapter
	// and path additions and also to get future callbacks
	path_manager::the().add_client(path_manager::PM_TRANSPORT,
		the_tcp_transport);
	//
	// Create tcp_rio_client object reaper thread.
	//
	error = clnewlwp(tcp_rio_client::tcp_rio_client_reaper,
	    (void *)the_tcp_transport, 0, NULL, NULL);
	if (error != 0) {
		//
		// Log this event and indicate reaper is not running.
		//
		os::sc_syslog_msg tcp_msg(SC_SYSLOG_TCP_TRANSPORT,
		    "Tcp", NULL);
		//
		// SCMSGS
		// @explanation
		// The replyio subsystem was unable to create this thread.
		// This is caused by inadequate memory on the system.
		// @user_action
		// Add more memory to the system. If that does not resolve
		// the problem, contact your authorized Sun service provider
		// to determine whether a workaround or patch is available.
		//
		(void) tcp_msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "Tcp_transport could not create tcp_rio_client reaper\n");
		the_tcp_transport->reaper_not_avail();
	} else {
		the_tcp_transport->reaper_running();
	}
	return (0);
}

int
_fini(void)
{
	int error;

	/* If havent deleted the tcp_transport, return NULL */
	if (the_tcp_transport != NULL) {
		return (EBUSY);
	}

	if ((error = mod_remove(&modlinkage)) != 0) {
		return (error);
	}

	// Arp cache monitor fini
	_arp_cache_monitor_fini();

	_cplpl_fini();
	return (0);
}

int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}

}

// If inlines are disabled the methods need to be compiled
// into the library so we include the _in.h files while
// #defining inline to be nothing giving non-inline versions
// of the methods.

#ifdef NOINLINES
#define	inline
#include <transports/tcp/tcp_transport_in.h>
#undef inline		// in case code is added below that uses inline
#endif  // NOINLINES

//
// This is the callback registered with the panic subsystem.
// The callback will send a panic notification to all
// the other cluster memebers. Note that we need to send
// the notification to all the other nodes for the surviving
// members to start the reconfiguration immediately. Hence
// the limitation is that the improvement is lost with one
// lost panic notification message.
//
extern "C" boolean_t
cl_transport_panic_cb(void *arg, int code)
{
	if (sc_panic_notify == 1) {
		return (the_tcp_transport->panic_notify());
	}

	// Just return if panic notification is turned OFF
	return (B_TRUE);
}

// Entry point for the callback registered with panic subsystem
boolean_t
tcp_transport::panic_notify()
{
	tcp_pathend *tpe;
	int nid;

	//
	// Log to the console about the panic notification.
	// Note that we are not using sc_syslog_msg object
	// for this because the constructor of sc_syslog_msg
	// does a kmem_alloc() and we should not be doing
	// any memory allocations as part of the CB_CL_PANIC
	// callback
	//
	// SCMSGS
	// @explanation
	// The node is notifying the other remaining cluster nodes
	// that the node is going down due to a system panic.
	// @user_action
	// No action required.
	//
	printf(SC_SYSLOG_MESSAGE(
	    "Notifying cluster that this node is panicking\n"));

	// Send one message per node
	for (nid = 1; nid <= NODEID_MAX; nid++) {
		IntrList<pathend, _SList>::ListIterator iter(the_pathends[nid]);
		while ((tpe = (tcp_pathend *)iter.get_current()) != NULL) {
			if (tpe->panic_notify()) {
				break;
			}
			iter.advance();
		}
	}
	return (B_TRUE);
}

// Routine to send the panic message on this pathend
boolean_t
tcp_pathend::panic_notify()
{
	// Send only in CONNECTED state
	if (tpe_state == TPE_CONNECTED) {
		HB_DBG(this, ("Sending panic packet\n"));
		rdconnp->panic_notify();
		return (B_TRUE);
	} else {
		HB_DBG(this, ("Panic packet not sent\n"));
		return (B_FALSE);
	}
}
