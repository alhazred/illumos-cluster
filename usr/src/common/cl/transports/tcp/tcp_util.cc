/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)tcp_util.cc	1.7	08/05/20 SMI"

//
// TCP TRANSPORT utility routines
//

#include <sys/os.h>
#include <sys/param.h>
#include <transports/tcp/tcp_util.h>

#ifdef TCP_DBGBUF
uint32_t tcp_dbg_size = 65536;
uint32_t udp_dbg_size = 16384;
dbg_print_buf   tcp_debug(tcp_dbg_size);
dbg_print_buf   udp_debug(udp_dbg_size);
#endif

int tcptr_trace_options = (TCPTR_TRACE_EP_ERR | TCPTR_TRACE_RIO_MINIMAL);

#ifdef ETCP_DBGBUF
uint32_t etcp_dbg_size = 65536;
dbg_print_buf   etcp_debug(etcp_dbg_size, UNODE_DBGOUT_ALL, true);
#endif

//
// tcp_util::iptostr()
//
// Utility routine to convert an IP address into a printable string.
// Second argument is the user supplied buffer where the printable
// string will be stored. It is the user's responsibility to ensure
// that the buffer is big enough.
// Returns pointer to the passed buffer.
//
char *
tcp_util::iptostr(in_addr_t ipaddr, char *str)
{
	uint_t ipint;

	ipint = (uint_t)ipaddr;
	if (str == NULL)
		return (NULL);
	os::sprintf(str, "%u.%u.%u.%u", (ipint >> 24) & 0xff,
	    (ipint >> 16) & 0xff, (ipint >> 8) & 0xff, ipint & 0xff);
	return (str);
}

//
// tcp_util::mactostr()
//
// Utility routine to convert a MAC address into a printable string.
// Third argument is the user suppiled buffer where the printable
// string will be stored. It is the user's responsibility to ensure
// that the buffer is big enough.
// Returns pointer to the passed buffer.
//
char *
tcp_util::mactostr(uchar_t *macaddr, size_t len, char *str)
{
	uint_t i;

	if (str == NULL)
		return (NULL);
	ASSERT(len <= MAX_MAC_ADDR_SIZE);
	for (i = 0; i < len; i++) {
		os::sprintf(str + 3*i, "%02x:", (int)macaddr[i]);
	}
	str[i ? 3*i-1 : i] = '\0';
	return (str);
}

//
// tcp_util::macs_equal()
//
// Determines if two given mac addresses are equal
//
bool
tcp_util::macs_equal(uchar_t *mac1, uchar_t *mac2, size_t len)
{
	while (len--) {
		if (mac1[len] != mac2[len]) {
			return (false);
		}
	}
	return (true);
}
