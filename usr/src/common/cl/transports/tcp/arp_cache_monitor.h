/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ARP_CACHE_MONITOR_H
#define	_ARP_CACHE_MONITOR_H

#pragma ident	"@(#)arp_cache_monitor.h	1.5	08/05/20 SMI"

//
// The Arp Cache Monitor (ACM) is a part of the TCP (DLPI) Transport.
// Its charter is to monitor the arp cache at the cluster nodes to make
// sure that the arp entries corresponding to the private IP addresses
// used by the TCP transport stay sane. The need for this module arises
// mostly because by default the same set of private IP addresses are
// used by all clusters running suncluster 3.x. If someone miscables
// the private network adapters of a cluster to a public net to which
// other clusters are connected, all of those clusters run the risk of
// path downs due to polluted ARP entries. The arp cache monitor,
// monitors the arp caches at the local node based on the information
// provided to it by the transport.
//
// Whenever an adapter is opened (raw_dlpi_adapter::open) the local
// IP address and MAC address is registered with the ACM. Whenever a
// path is established with a remote node (raw_dlpi_conn::connect),
// the remote IP address and the remote MAC address are registered with
// the ACM. The ACM runs a background thread that wakes up periodically
// to verify the arp cache entries for the registered IP addresses. If
// it finds a polluted entry, it first logs a message to the syslog to
// notify the administrator of the problem and the attempts to take
// corrective action to keep the path from going down while the
// administrator works on the root cause of the problem. The polluted ARP
// entry for a remote IP address is simply deleted. We will depend on
// the ARP protocol to come up with the right entry. A polluted ARP
// entry for the local IP address is corrected. The need for monitoring
// local IP addresses will go away when the fix for kernel/tcp-ip bug
// #4363786 becomes available.
//

#include <netinet/in.h>
#include <sys/socketvar.h>
#include <sys/threadpool.h>
#include <sys/refcnt.h>
#include <transports/tcp/tcp_util.h>

// forward declaration
class arp_cache_monitor;

//
// The IP MAC class. One IP address, MAC address pair is stored for
// each IP address being monitored.
//
class acm_ipmac : public _SList::ListElem, public refcnt {
public:
	acm_ipmac(in_addr_t, uchar_t *, size_t, bool);
	in_addr_t get_ip();

	// Verify if this MAC address retrieved from the ARP cache
	// is the right MAC address
	bool verify_arp_mac(uchar_t *);

	// Retrieve ARP entry and check if it right, if not take
	// corrective action.
	void do_arp(struct sonode *);

private:
	// If it refers to a local IP
	bool local;

	// The IP address
	in_addr_t	ipaddr;

	// The real mac address and the mac address received from arp
	uchar_t real_mac[MAX_MAC_ADDR_SIZE];
	uchar_t arp_mac[MAX_MAC_ADDR_SIZE];

	// MAC address length
	size_t	mac_len;

	// Private destructor, called only through refcnt_unref
	~acm_ipmac();

	// Disallow assignments and pass by value
	acm_ipmac(const acm_ipmac &);
	acm_ipmac &operator = (acm_ipmac &);
};


// Task to monitor arp cache
class acm_defer_task : public defer_task {
public:
	acm_defer_task(arp_cache_monitor *);
	void execute();
	void task_done();

private:
	arp_cache_monitor *acmp;

	acm_defer_task(const acm_defer_task &);
	acm_defer_task &operator = (acm_defer_task &);
};

//
// The arp cache monitor class. There is only one object of this class.
//
class arp_cache_monitor {
public:
	arp_cache_monitor();
	~arp_cache_monitor();

	// Initialize allocates the arp cache monitor class
	static void initialize();

	// Return a reference to the arp cache monitor object. The raw dlpi
	// objects needs this refernce to register and unregister IP/MAC
	// addresses.
	static arp_cache_monitor& the();

	// Register an IP/MAC pair for monitoring
	void register_ipmac(in_addr_t, uchar_t *, size_t, bool);

	// Unregister an IP address
	void unregister_ipmac(in_addr_t);

	// The real monitoring is done by this method
	void run();

	// Shutdown
	void shutdown();

private:
	// Pointer to the arp cache monitor object
	static arp_cache_monitor *the_acmp;

	// List of IP-MAC pairs
	IntrList<acm_ipmac, _SList> ipmacs;

	// Iterator used to iterate over the list of ipmacs
	IntrList<acm_ipmac, _SList>::ListIterator   iter;

	// Mutex protecting internal data structures
	os::mutex_t acm_lock;

	// Condition variable for wait/notification
	os::condvar_t acm_cv;

	// Flags to coordinate operations between the monitoring
	// thread and the shutdown thread
	bool shutting_down;
	bool running;

	// The ARP cache monitor task
	acm_defer_task adt;

	// Prepare for monitoring
	struct sonode *prepare();

	// Cleanup after monitoring has been shut down
	void cleanup(struct sonode *);

	// Disallow assignments and pass by value
	arp_cache_monitor(const arp_cache_monitor &);
	arp_cache_monitor operator= (arp_cache_monitor &);
};

// Extern declarations for functions defined in arp_cache_monitor.cc that are
// called from tcp_transport.cc
extern void _arp_cache_monitor_init(void);
extern void _arp_cache_monitor_fini(void);

#endif	/* _ARP_CACHE_MONITOR_H */
