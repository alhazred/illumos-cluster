/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RAW_DLPI_H
#define	_RAW_DLPI_H

#pragma ident	"@(#)raw_dlpi.h	1.20	08/09/09 SMI"

//
// The raw dlpi transport used to exchange heartbeat messages among
// SC3.x cluster nodes over the private interconnect consists of two
// components:
//
//  1.	A component that is responsible for managing the raw dlpi
//	"connections" among the cluster nodes. This is implemented in
//	raw_dlpi.cc and gets compiled into the standard dlpi transport
//	module cl_dlpitrans.
//
//  2.	A streams module (cldlpihb) that gets pushed over the dlpi drivers
//	and intercepts heartbeat messages and forwards them to the correct
//	pathends. This module is implemented in raw_dlpi_str.cc.
//
// A raw_dlpi object keeps track of the internal data of the raw dlpi
// transport. This object is created along with the tcp_transport object when
// the cl_dlpitrans module is loaded. Everytime a new adapter gets added to
// the tcp_transport through a call to tcp_transport::new_adapter, an
// object of type raw_dlpi_adapter is also created and added to the linked
// list of adapters maintained by the raw_dlpi object. The raw_dlpi_adapter
// makes a copy of the device name, instance and the adapter id. The
// raw_dlpi_adapter gets deleted from the raw_dlpi adapter list and is
// destroyed when the corresponding tcp_adapter object is destroyed through
// tcp_adapter::~tcp_adapter.
//
// When a tcp_pathend is initiated, a channel to send heartbeat messages
// over raw dlpi is established between the two ends of the path. The
// tcp_pathend object contains a raw_dlpi_conn object that maintains
// information about this raw dlpi channel or "connection". The "connection"
// establishment takes place from tcp_pathend::initiate() in several steps.
//
//  1.	The dlpi driver corresponding to the local adapter is opened (using
//	a well known private SAP and the cldlpihb module is pushed on it. The
//	cldlpihb module is made aware of its adapter by sending it a pointer
//	to its adapter through an ioctl.
//
//  2.	The local MAC address is retrieved from the driver and stored with
//	the raw_dlpi_adapter object.
//
//  3.	MAC addresses are exchanged with the peer tcp_pathend using UDP
//	messages sent to well known ports. The existing tcp_pathend connection
//	establishment procedure already had this UDP message exchange step for
//	synchronization before TCP connections were established. The UDP
//	message exhange has been enhanced and adapted for the raw dlpi's
//	purpose. Two major changes have been made - (i) The UDP message format
//	(struct udp_info) has been changed to include a MAC address and
//	address length, and (ii) earlier only one node (tcp server) used to
//	send a (request) UDP message to the other node, in whose reponse the
//	other node (tcp client) would attempt to establish a TCP connection.
//	Now the UDP request message is explicity acked by the client. Both
//	nodes wait for this handshake to complete before proceeding any
//	further.
//
//  4.	Once the remote MAC address arrives, it is stored with the
//	raw_dlpi_conn object. Now we are ready to send and receive heartbeat
//	messages.
//
//  Each adapter can be connected to several cluster nodes on the same
//  private network. To uniquely identify the pathend an arriving heartbeat
//  message is meant for, the sender encapsulated its nodeid in the heartbeat
//  message. Each raw_dlpi_adapter object maintains an array of pointers to
//  all raw_dlpi_conn objects that are using this adapter. This array is
//  indexed by remote nodeids. The raw_dlpi_conn object is located by indexing
//  into the array using the sender's nodeid. The raw_dlpi_object maintains a
//  pointer to the container tcp_pathend object that is used to send the
//  heartbeat message up to the path_manager through the call to
//  tcp_pathend::pm_recv_internal().
//
//  The first time a raw_dlpi_conn is established over a raw_dlpi_adapter,
//  the corresponding device is opened. The raw_dlpi_adapter keeps track
//  of the number of raw_dlpi_conn objects associated with it. Subsequent
//  "connections" only increment this count. If a "connection" is closed, the
//  count is decremented. When the count goes down to zero the device is
//  closed. A raw_dlpi_conn is closed when the corresponding
//  tcp_pathend::cleanup is done.
//
//  Heartbeat path:
//  As earlier, the path manager makes a call to pathend::pm_send_internal
//  at the sender. Tcp_pathend::pm_send_internal gathers the heartbeat
//  tokens and calls raw_dlpi_conn::send which in turn calls
//  raw_dlpi_adapter::send. raw_dlpi_adapter::send hands over the heartbeat
//  message to the driver. No guarantees or acknowledgements are provided
//  about the delivery of the message at the other end of the path.
//
//  When the heartbeat message arrives at the receiver, the driver detects
//  the special SAP and sends the message upstream to the cldlpihb module.
//  The cldlpihb module's rput routine calls the corresponding
//  raw_dlpi_adapter's recv routine which uses the sender's nodeid to locate
//  the appropriate raw_dlpi_conn and passes the message to that object's
//  recv routine. The raw_dlpi_conn::recv calls the container tcp_pathend's
//  pm_recv_internal. The earlier incarnation of the pm_recv_internal used
//  to just overwrite a designated heartbeat receive are with the new tokens.
//  Since in the raw dlpi implementation, packets might (?) arrive out of
//  order, the pm_recv_internal calls a utility routine provided by the
//  path manager to determine if the newly received tokens are more current
//  before peforming the copy.
//
//  Since messages could arrive out of order, there is a need to guard against
//  messages originating in stale incarnations. During the "connection"
//  establishment phase, nodes exchange their current incarnation numbers
//  along with the MAC addresses. Each message carries the sender's nodeid
//  and its incarnation number. Only heartbeats originating in the agreed upon
//  incarnation are accepted.
//

#include <sys/dbg_printf.h>
#include <orb/ip/udi_util.h>
#include <orb/ip/vlan.h>
#include <orb/transport/transport.h>
#include <transports/tcp/tcp_util.h>

// Debug buffer stuff
#if defined(HACI_DBGBUFS) && !defined(NO_RAW_DLPI_DBGBUF)
#define	RAW_DLPI_DBGBUF
#endif
#ifdef RAW_DLPI_DBGBUF
extern dbg_print_buf    raw_dlpi_dbg;
#define	RAW_DLPI_DBG(f)	HACI_DEBUG(ENABLE_RAW_DLPI_DBG, raw_dlpi_dbg, f)
#else
#define	RAW_DLPI_DBG(f)
#endif

// Tracing heartbeats. Trace is on if the following macro
// is defined.
#ifndef RAW_DLPI_TRACE
#define	RAW_DLPI_TRACE
#endif

// The maximum mac addr size is a randomly chosen number. It works
// for the currently supported transports - Ethernet, SCI, IB. If a new
// transport is supported and it uses a larger mac addr,
// this constant must be updated.
// Note: IB has a 20-byte MAC address. Add 2 (for the sap) to get 22.
#define	MAX_MAC_ADDR_SIZE	22

// Again a randomly chosen SAP, 1 greater than that used by cl_net
#define	RAWDLPI_DEFAULT_SAP	0x0833

// The (estimated) maximum size of mac header that gets shipped with the
// data on the wire. This is only a performance optimization, a wrong value
// will not impact correctness. We leave this much space in the front on the
// data mblk empty before copying data. This is done so that the dlpi driver
// does not have to allocate another mblk for the header. Ethernet and SCI
// are happy with 32. If more transports are supported in the future, this
// number might need revision.
#define	MAX_MAC_HEADER_SIZE	32

#define	CLONE "clone"

// Name of the heartbeat interceptor streams module that gets pushed on
// the dlpi driver.
#define	RAWDLPI_STR_MODULE_NAME "cldlpihb"

// Name of the heartbeat sender streams module that gets pushed on
// the dlpi driver.
#define	HBSNDR_STR_MODULE_NAME "clhbsndr"

//
// Value of the special signature identifying when
// a node is notifying that it is panicking. Value is
// chosen to be something other than the value containing
// all zeroes or ones. This is because of the likelihood of
// the value to contain all zeroes or ones in case of h/w errors.
//
#define	PANIC_SIGNATURE	123456789

// Extern declarations for functions defined in raw_dlpi.cc that are called
// in tcp_transport.cc
extern void _raw_dlpi_init(void);
extern void _raw_dlpi_fini(void);

class tcp_pathend;
class raw_dlpi;
class raw_dlpi_adapter;
class raw_dlpi_conn_stats;
class raw_dlpi_perhb_stats;

//
// The raw_dlpi_conn represents a raw dlpi "connection" between two
// pathends over a dlpi adapter pair. Much like a UDP "connection", the
// main purpose of a raw_dlpi_conn is to hold the MAC address of the
// remote adapter. This "connection" is established during
// tcp_pathend::initiate(). The "connection" establishment mainly
// involves exchanging MAC addresses through UDP messages during
// tcp_pathend::initiate(). Each tcp_pathend has pointer to a heap
// allocated raw_dlpi_conn object as a member.
//
class raw_dlpi_conn {
public:
	raw_dlpi_conn(tcp_pathend *, size_t);
	~raw_dlpi_conn();

	// Store information about the local adapter to be used for
	// communication. Tcp_pathend::initiate() calls this routine
	// to enable the raw_dlpi_conn to identify the adapter to use.
	void set_adapter(clconf_adapter_t *);

	// Get the MAC address of the local adapter. This information
	// is needed by tcp_pathend so that it can exchange the MAC
	// address with its peer.
	int get_local_addr(uchar_t **, size_t *);

	// Once tcp_pathend::initiate() has obtained the MAC address of
	// the peer adapter, it informs the raw_dlpi_conn through a
	// call to set_remote_info.
	void set_remote_info(const uchar_t *, size_t, incarnation_num);

	// Open stores the local and remote nodeids and the local incarnation
	// number. Calls raw_dlpi_adapter::open() for opening the adapter
	// device. Called by tcp_pathend::initiate().
	int open(nodeid_t, incarnation_num, nodeid_t);

	// Connect makes the connection ready for sending and receiving
	// messages. Send preparations include creating a template mblk
	// chain that will be used to create the real heartbeat messages
	// to be sent. Recv preparations involve notifying the adapter
	// (through a call to raw_dlpi_adapter::store_connp) that the
	// "connection" is ready to accept heatbeats.
	void connect();

	// Send the heartbeat message of known length on this connection.
	// Called by tcp_pathend::pm_send_internal().
	void send(const char *);

	// Send the panic heartbeat message
	void panic_notify();

	// Routine called to send a received heartbeat message to the
	// path_manager through a call to tcp_pathend::pm_recv_internal()
	void recv(mblk_t *, incarnation_num);

	// Shutdown a connection. No new sends or receives will be allowed
	// on this connection. To be used before an imminent close.
	void shutdown();

	// Close the connection, called when a the path is being cleaned up.
	void close();

private:
	// State of the connection.
	enum {	// Object newly created
		RDCONN_NONE,

		// Corresponding adapter has been opened
		// but we don't know remote MAC addr yet
		RDCONN_OPEN,

		// Ready to send/recv
		RDCONN_CONNECTED,

		// Ongoing send/recv will be allowed to
		// to complete but no new ones will be initiated.
		RDCONN_SHUTDOWN,

		// Connection closed
		RDCONN_CLOSED
	} state;

	// Pointer to the local raw_dlpi_adapter structure that will be
	// used by this "connection".
	raw_dlpi_adapter *adapter;

	// Remote nodeid.
	nodeid_t   r_ndid;

	// Remote incarnation number. Only heartbeats originating in this
	// incarnation number will be accepted.
	incarnation_num r_inc;

	// Local nodeid.
	nodeid_t   l_ndid;

	// Local incarnation number. All heartbeats must carry this
	// incarnation number. The receiver will honor only heartbeats
	// originating from the prenegotiated incarnation exchanged during
	// connection establishment.
	incarnation_num l_inc;

	// Back pointer to the tcp_pathend we belong to. Used to identify
	// the pathend to which a received heartbeat message will be
	// delivered to.
	tcp_pathend *pathendp;

	// Length of the heartbeat message that will be sent over this
	// connection.
	size_t hblen;

	// A set of template mblk_t chains that will be duped to create
	// heartbeat messages on this connection. Stores precomputed
	// information that is the same in each heartbeat message sent.
	// We keep one template for each possible user priority value.
	mblk_t *send_mp_template[NUM_8021D_UPRI];

	// Number of sends and/or receives going on on this connection.
	// A connection cannot be closed unless this number is 0.
	uint_t pending_ops;

	// Send and recv statistics for debug purposes. Gets allocated
	// only if the RAW_DLPI_TRACE is defined, the pointer stays
	// NULL otherwise. Not ifdefed to keep the class size same
	// irrespective of whether tracing is on or off.
	raw_dlpi_conn_stats *statsp;

	// Whether registered with the arp cache monitor
	bool	acm_reg;

	// Packet drop statistics
	int nodata;	// Message too small
	int badinc;	// Message from bad incarnation
	int noconn;	// Connection has broken down

	// Lock protecting internal data structures and the associated
	// condition variable. This lock is needed by receives in the
	// interrupt context, hence we need to be careful about what is
	// done in its scope. It is ok to hold this lock for long durations
	// during connection setup and breakdown. But as long as the
	// connection is up, the lock should be held only for brief moments -
	// absolutely no blocking memory allocations while the lock is held
	// in the send/recv methods.
	os::mutex_t rdconn_lock;
	os::condvar_t rdconn_cv;

	// The remote macaddr. Length not stored here as it must be the
	// same as the local macaddr which is stored in the corresponding
	// adapter structure.
	uchar_t remote_addr[MAX_MAC_ADDR_SIZE];

	raw_dlpi_conn(const raw_dlpi_conn &);
	raw_dlpi_conn &operator = (raw_dlpi_conn &);
};

//
// The raw_dlpi_conn_stats class represents the debug statistics gathered
// per raw_dlpi_conn. The debug information consists of a log of the recently
// sent and received heartbeat tokens and the time stamps when they were sent
// or received. This information is gathered only when the RAW_DLPI_TRACE
// macro is defined. The stats are stored in two ring buffers - one for the
// sent messages and the other for the received messages. RAW_DLPI_TRACE_SIZE
// determines the size of each ring buffer.
//

#define	RAW_DLPI_TRACE_SIZE	32

class raw_dlpi_conn_stats {
public:
	raw_dlpi_conn_stats(size_t);
	~raw_dlpi_conn_stats();

	// Store the passed heartbeat token along with the current lbolt
	// in the send/recv ring buffers. The first argument tells
	// whether it is a receive or a send.
	void store(bool, char *, ushort_t);

private:
	// Size of send/recv ring buffer
	size_t size;

	// Recv ring buffer current index
	uint_t ri;

	// Recv ring buffer pointer
	raw_dlpi_perhb_stats *rp;

	// Mutex to protect recv ring buffer
	os::mutex_t rlock;

	// Send ring buffer current index
	uint_t si;

	// Send ring buffer pointer
	raw_dlpi_perhb_stats *sp;

	// Mutex to protect send ring buffer
	os::mutex_t slock;

	raw_dlpi_conn_stats(const raw_dlpi_conn_stats &);
	raw_dlpi_conn_stats &operator = (raw_dlpi_conn_stats &);
};


//
// The raw_dlpi_perhb_stats represents the information that is
// stored about each sent/recv heartbeat message. This information
// is stored in a ring buffer
//
class raw_dlpi_perhb_stats {
public:
	raw_dlpi_perhb_stats();
	~raw_dlpi_perhb_stats();

	// Store the passed heartbeat token along with the current lbolt
	void store(uint_t, char *, ushort_t);

private:
	// Driver queue count when this heartbeat was logged.
	ushort_t q_count;

	// Serial number
	ushort_t si;

	// Time stamp in lbolts
	uint32_t ts;

	// The heartbeat token
	uint64_t hb;

	raw_dlpi_perhb_stats(const raw_dlpi_conn_stats &);
	raw_dlpi_perhb_stats &operator = (raw_dlpi_conn_stats &);
};


//
// Class raw_dlpi_adapter represents a dlpi adapter that is used for
// heartbeat communication using raw dlpi messages.
//
class raw_dlpi_adapter : public _SList::ListElem {
public:
	raw_dlpi_adapter(char *, int, int, int, in_addr_t);
	~raw_dlpi_adapter();

	// Return the adapter id
	int get_id();

	// Return the device name
	const char *get_device();

	// Return the device instance
	int get_instance();

	// Return the vlan id
	int get_vlan_id();

	// Open the device using the raw dlpi sap.
	// If already open, increment the active connections count.
	int open();

	// Send a message that has been sent down by a raw_dlpi_conn
	int send(mblk_t *);

	// Pass a received heartbeat message up to the appropriate
	// connection.
	void recv(mblk_t *);

	// Shutdown a connection over this adapter. No new sends or
	// receives will be allowed for that connection through this
	// adapter. To be used before an imminent close.
	void shutdown(nodeid_t);

	// Close the device. If other connections have it open too, just
	// decrement the active connections count.
	void close();

	// Store information about a raw dlpi "connection" over this
	// adapter. The first argument is the remote nodeid and the
	// second argument is a pointer to the "connection". The adapter
	// stores pointers to all "connections" over it in an array
	// indexed by remote nodeids. A heartbeat message carries the
	// sender's nodeid which is used to index into this array to
	// determine the "connection" that should receive this heartbeat.
	// This routine is called by raw_dlpi_conn:connect().
	void store_connp(nodeid_t, raw_dlpi_conn *);

	// Get the raw_dlpi_conn pointer given the remote nodeid.
	raw_dlpi_conn *retrieve_connp(nodeid_t);

	// Get the MAC address of this adapter and the mac address length.
	// Tcp_pathend exchanges this information with its peer to establish
	// a raw dlpi "connection".
	void get_local_addr(uchar_t **, size_t *);

	// Return pointer to the cldlpihb module's write queue. Used for
	// extracting information from the driver for debugging purposes.
	queue_t *get_wq();

	// Check if fast path probe is available
	mblk_t *fast_path_probe(mblk_t *);

	void process_fast_path_ack(mblk_t *);

private:
	// Name of the device, e.g., hme
	char	*device;

	// Instance number of the device, e.g., 0 for hme0
	int	instance;

	// VLA-ID associated with this device
	int	vlan_id;

	// Adapter id of the device, comes from clconf_adapter_t.
	// Used as an unique identifier for the adapter.
	int	adp_id;

	// MAC address length in bytes.
	size_t	mac_len;

	// IP address to be hosted on this adapter, needed only to initiate
	// arp cache monitoring.
	in_addr_t ipaddr;

	// udi handle on the opened device.
	udi_handle_t uh;

	// Pointer to the write queue beneath the streams head. This
	// is the queue pointer we will use to send messages.
	queue_t *wq;

	// Total number of currently active connections. An adapter can not
	// be closed unless this number is zero.
	uint_t active_conns;

	// Whether fast path support is not available
	bool	no_fp;

	// Placeholder for the fast path probe ioctl (n)ack mp received
	// from the driver until the transport module can take a look
	// at it.
	mblk_t	*fast_path_ack_mp;

	// Mutex and condition variable to synchronize access to the above
	// (n)ack mp.
	os::mutex_t	fast_path_lock;
	os::condvar_t	fast_path_cv;

	// And now some statistics - hb messages dropped due to several
	// reasons
	int nodata;	// Messages dropped because too small
	int badsrc;	// Bad source node
	int noconn;	// Connection from given src not not found

	// Lock to protect internal data structures. This lock protects the
	// data structures only during the calls to open and close. We require
	// that other adapter methods be called only by agents that have
	// already opened the adapter and have not subsequently closed them.
	// It is guaranteed that an adapter's data structures will be in the
	// sane state and the device will be accessible as long as all agents
	// that have successfully opened the adapter have not closed it too.
	//
	// There is one exception to this rule. The adapter method
	// adapter::recv is called by the cldlpihb module which does not
	// explicitly open the adapter. However cldlpihb module exits only
	// if the adapter has already been successfully opened by someone and
	// and has not been closed, hence we are safe.
	os::mutex_t adapter_lock;

	// Local MAC address of the adapter.
	uchar_t	local_addr[MAX_MAC_ADDR_SIZE];

	// Array of raw_dlpi_conn pointers indexed by remote nodeids.
	// Used by cldlpihb to identify the right "connection" to
	// deliver the heartbeat to. Be careful before changing this
	// array indexed by nodeids architecture. The elements of the
	// array are modified by raw_dlpi_conn methods without acquiring
	// any locks as if they own their own slots.
	raw_dlpi_conn *conntab[NODEID_MAX+1];

	// Attach to the specific unit
	int dl_attach(udi_handle_t);

	// Bind to our sap
	int dl_bind(udi_handle_t);

	// Enable interrupt handling
	int dl_intr_mode(udi_handle_t);

	// Get the mac address info
	int dl_info(udi_handle_t);

	raw_dlpi_adapter(const raw_dlpi_adapter &);
	raw_dlpi_adapter &operator = (raw_dlpi_adapter &);
};

//
// The raw_dlpi class is mainly there to keep track of the adapters
// over which raw dlpi heartbeats are exchanged. There is only one
// object of this class which is created along with the tcp_transport
// object when the cl_dlpitrans module is loaded.
//
class raw_dlpi {
public:
	raw_dlpi();

	~raw_dlpi();

	// Initialize allocates the raw_dlpi structure
	static void initialize();

	// Return a reference to the raw_dlpi object. The tcp transport
	// needs this refernce to deposit and retrieve adapter information.
	static raw_dlpi& the();

	// Create a raw_dlpi_adapter object and put it in the list of
	// adapters maintained by the raw_dlpi object. Called by the
	// tcp_adapter constructor.
	void add_adapter(clconf_adapter_t *);

	// Delete a previously created raw_dlpi_adapter object and remove
	// it from the list maintained by the raw_dlpi object. Called by
	// tcp_adapter destructor. The agument is the adapter id.
	void remove_adapter(int);

	// Return a pointer to a raw_dlpi_adapter given the corresponding
	// clconf object.
	raw_dlpi_adapter *get_adapter(clconf_adapter_t *);

	// Return the sap to be used by the raw dlpi heartbeats
	ushort_t get_sap();

	// Shutdown
	static void shutdown();

private:
	// Pointer to the raw dlpi structure.
	static raw_dlpi *the_raw_dlpi_p;

	// SAP in use by the raw dlpi transport
	ushort_t sap;

	// List of raw_dlpi_adapters
	IntrList<raw_dlpi_adapter, _SList> adapters;

	// Mutex protecting internal data structures
	os::mutex_t rdlpi_lock;


	raw_dlpi(const raw_dlpi &);
	raw_dlpi &operator = (raw_dlpi &);
};

//
// Information sent by the raw dlpi transport to the cldlpihb streams
// module that gets the hb messages through CLIOCSRDINFO ioctl. The
// information sent includes a pointer to the raw_dlpi_adapter corresponding
// to adapter on which the cldlpihb module has been pushed. The cldlpihb
// module uses the adapter pointer to retrieve information about which
// pathend an arriving heartbeat should be going to.
//
typedef struct raw_dlpi_info_s {
	raw_dlpi_adapter *adapter;
} raw_dlpi_info_t;

// The heartbeat message header
typedef struct raw_dlpi_hb_hdr_s {
	nodeid_t	s_ndid;	// Sender's nodeid
	nodeid_t	r_ndid;	// Receivers's nodeid, currently unused
	incarnation_num s_incn;	// Sender's incarnation number
	incarnation_num panic;	// Panic signature
} raw_dlpi_hb_hdr_t;

#endif	/* _RAW_DLPI_H */
