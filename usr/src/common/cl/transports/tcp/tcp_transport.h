/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _TCP_TRANSPORT_H
#define	_TCP_TRANSPORT_H

#pragma ident	"@(#)tcp_transport.h	1.94	08/05/20 SMI"

#include <orb/transport/transport.h>
#include <orb/invo/invocation.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/stream.h>
#include <sys/strsubr.h>
#include <sys/strsun.h>
#include <sys/socketvar.h>
#include <sys/clconf_int.h>
#include <transports/common/transport_version.h>

// Headers related to replyio
#include <transports/tcp/tcp_io.h>

// Headers related to interaction with module pushed on top of tcp
#include <transports/tcp/tcpmod.h>

// Headers related to the raw dlpi transport management
#include <transports/tcp/raw_dlpi.h>

// TCP Transport utilities
#include <transports/tcp/tcp_util.h>

// forward declarations...
class tcp_transport;
class tcp_pathend;
class tcp_endpoint;
class tcp_sendstream;

//
// Notes of port usage:
// The pathend and endpoint are on different ports.
// This is done in order to make it easier to detect when
// a node has rebooted. Now, if more than one connection request
// is made for the pathend port, we know that the node has rebooted.
//
// all conenctions from all nodes are accepted by one central
// server and then dispatched to the specific pathend/endpoint.
// There are two tcp servers one for pathend and one for endpoint.
// There are two udp servers one for pathend and one for endpoint.
//
// We use port numbers 8059 thru 8062 in tcp transport.
//
// We do not have a reservation on these ports. If something else in the
// system uses these ports before us, tcp transport will not work.
// Usually we bind to these ports in rcS40bootcluster, but if we operate in a
// mixed transport environment and do not have any tcp transport adapters
// configured at boot, this may not work.
//
#define	TCP_PE_PORT		8059
#define	TCP_EP_PORT		8060
#define	UDP_PE_PORT		8061
#define	UDP_EP_PORT		8062

//
// Pending Connections.
#define	TCP_BACKLOG_CONNECTIONS	64

//
// Maximum Number of connections per path. Actual number of connections
// might vary depending on the peer version. First few connections are
// used for non rio messages. Remaining are used for rio messages.
//
#define	TCP_EP_MAX_CONNECTIONS		6
#define	TCP_INVAL_CONN		((uint_t)-1)	// Invalid connection

// Number of replyio threads per endpoint
// When using replyio, tcp_transport does not copy the data into the user
// buffer in interrupt context but instead defers the copies to threads
// in the rio_threadpool. We currently tune the number of threads to be
// a function of the number of endpoints created.
// TCP_RIO_THREADS_PER_EP defines how many we create per endpoint
// XX This number needs to be tuned based on the number of CPUs in the
// system and amount of idle time per rio_task.
#define	TCP_RIO_THREADS_PER_EP		1

//
// These are tcp_transports internal message types.
// We require that these do not conflict with the values in orb_msgtype
// XX We should use an enum for these
//
#define	TCP_PE_INITIAL_MSG	0xff00001

#define	TCP_EP_INITIAL_MSG	0xff00002

#define	TCP_PM_INTERNAL		0xff00003

#define	TCP_SYNC_ACK		0xff00004

#define	TCP_REPLYIO_MSG		0xff00005

#define	TCP_REPLYIO_ACK		0xff00006

#define	TCP_KEEP_ACTIVE		0xff00007

//
// Stats for tcp transport. XXX Needs to be expanded and made into a kstat
//
class tcp_stats_t {
public :
	uint_t mblkbuf_sent;
	uint_t esballoc_sent;
	uint_t mblk_sent;
	uint_t msg_sent;
	uint_t xdoortab_msg;
	uint_t xdoortab_msg_piggyback;
	uint_t offlinetab_msg;
	uint_t hb_recvd;
	uint_t non_intr_hb_recvd;
	uint_t mblk_recv_count[10];
	uint_t mblk_sent_count[10];
};
extern tcp_stats_t tcp_stats;
#define	UPDATE_TCP_STATS(f, v)	tcp_stats.f += (v)

//
// state variable used in tcp_pathend
//
enum pe_udp_msg_state {
			PE_TCPTR_UDPMSG_CANNOT_RECV,
			PE_TCPTR_UDPMSG_CAN_RECV_REQ,
			PE_TCPTR_UDPMSG_CAN_RECV_RESP,
			PE_TCPTR_UDPMSG_RECEIVED
		};

//
// state variable used in tcp_endpoint
//
enum ep_udp_msg_state {
			EP_TCPTR_UDPMSG_CANNOT_RECV,
			EP_TCPTR_UDPMSG_CAN_RECV,
			EP_TCPTR_UDPMSG_RECEIVED
		};


//
// UDP handshake message header exchanged during pathend initialization.
// The header is followed by optional data controlled by the various
// fields in the header. The primary purpose of this handshake is bootstrap
// version negotiation for suncluster software.
//
// The version negotitaion handshake proceeds as follows. Between the two
// nodes at the two ends of the path, the one with the higher nodeid assumes
// the "active" role whereas the other assumes a "passive" role. The active
// node obtains the local version information from the bootstrap vession
// manager and stuffs it in ACTIVE_REQUEST message and sends it to the
// passive node. When this message arrives at the passive node, the passive
// node calls into its version manager to validate the version string
// that it received from the peer. It then sends a PASSIVE_RESPREQ message
// to the peer. This message contains both the compatibilty check result
// from this node's point of view and a request to do compatibility check
// of this node's version string. [Both nodes must do compatibility checks
// on each other's version string as the results are not guaranteed to be
// symmetric.] Upon receiving the PASSIVE_RESPREQ message, the active node
// calls into the local version to validate the peer's version string and
// sends an ACTIVE_RESPONSE message back with the compatibility check.
//
// When a node becomes aware of a version incompatibilty, either though a
// response from its local version manager or as a result of receiving a
// compatibility check result from the peer, the path establishment process
// stops. If both the local version manager and the remote version manager
// ok the versions, then the path establishment process continues.
//
// The handshake protocol has timeouts built into it. If a timeout happens,
// the whole protocol is reinitiated. Every time a new ACTIVE_REQUEST message
// gets sent, a new handshake sequence number is generated. At any point of
// time there is only one valid handshake sequence number per path. Any stale
// UDP messages are dropped.
//
// Other than providing infrastructure for version negotiation, the UDP
// handshake protocol also serves two other purposes:
// (a) It provides a synchronization mechanism for pathend initiate.
// (b) It provides a mechanism to the two nodes to exchange the MAC addresses
//	at the two ends of the path. The MAC addresses are used for heartbeat
//	messages.
//
// The pathend_udp_info structure is the header of the very first messages
// exchanged between a pair of cluster nodes. It is through these messages
// that any two cluster nodes decide whether they have bare minimal software
// compatibility and whether it makes sense for them to set up higher level
// communication facilities between them. It is thus imperative that this
// header has a format that stays constant across 32/64 bit or debug/non-debug
// kernels. It is also desirable to keep the header format constant across
// releases. In the rare case when a release must change the header format,
// it must be prepared to be backward compatible and must handle messages
// with older headers. The first field in the header represents the header
// version. All future releases must retain the first size fields
// (header version, zero, nodeid, header length, message length, message type)
// and sc32 in the header as is. The four message types as well as the three
// way handshake protocol are invariants too. When a node receives a UDP
// handshake message with a (newer) header format that it does not recognize,
// it should reply back with a HANDSHAKE_VERSION_INFO message with information
// about the header version it understands in the message. The original sender
// is expected to restart the handshake with messages conforming to this header
// version.
//
// Sun Cluster 3.0 and and its update releases expects a null terminated version
// string starting at offset 44 in the udp handshake messages. We store the
// null terminated string "3.2" at offset 44 in the following header so that if
// this udp handshake message reaches a node running 3.0 or one of its updates,
// that node can gracefully reject this message. Future releases should also
// ensure that an appropriate null terminated string is always stored beginning
// at offset 44 in this header. SC 3.0 and updates also expect an integer value
// of zero at offset 4.
//
struct pathend_udp_info {
	uint32_t	hdrver;		// Header version
	uint32_t	zero;		// For the benefit of peers running
					// 3.0 or one of its updates. This field
					// must start at offset 4 in this
					// structure and must be set to 0.
	uint32_t	nodeid;		// Nodeid of the sending node
	uint32_t	hdrlen;		// Header length
	uint32_t	msglen;		// Length of this message
	uint32_t	msgtype;	// Message type, ACTIVE_REQUEST etc
	uint32_t	nodeinc;	// Current incarnation number of sender
	uint32_t	hs_seq;		// Current handshake sequence number
	uint32_t	compatible;	// In case of a PASSIVE_RESPREQ or
					// an ACTIVE_RESPONSE message whether
					// the peer found our previously sent
					// version string as compatible
	uint32_t	reserved[2];	// Reserved for future use
	uint32_t	sc_version;	// For the benefit of peers running
					// 3.0 or one of its updates. This field
					// must start at offset 44 in this
					// structure and must represent a null
					// terminated string.
	uint32_t	version_offset;	// Offset into the UDP message where
					// the version string starts
	uint32_t	version_len;	// Length of the version string
	uint32_t	mac_offset;	// Offset into the UDP message where
					// the MAC address starts
	uint32_t	mac_len;	// Length of the above MAC address
};

//
// Version of current pathend_udp_info header format
//
#define	PATHEND_UDP_INFO_VERSION	sc_transport_version

//
// This header must always be at least 48 bytes long. In addition, the sizes
// and offsets of the following fields must stay invariant across releases:
// hdrver, zero, hdrlen, msglen, msgtype, sc_version.
//
#define	PATHEND_UDP_INFO_MANDATORY_HEADER_LENGTH (12 * sizeof (uint32_t))

//
// Length of the corresponding udp_info structure in SC3.0 and its updates.
// We use this to intercept and reject messages from older releases while
// providing informative error messages.
//
#define	PATHEND_UDP_INFO_30_LENGTH	304

//
// The SC3.0 udp_info structure has a boolean as its first field indicating
// whether the the udp handshake message was a request message or a reply
// message. This was followed by an integer. Thus the first 4 bytes in a SC3.0
// udp handshake message will be one of the following two. These will appear as
// the header versions if the message were to be interpreted using the new
// header format. These two values are to be treated as invalid header versions
// for the new header format. Messages carrying these header versions will
// be treated as 3.0 messages.
//
#define	PATHEND_UDP_INFO_30_REQUEST_VERSION	0x0
#define	PATHEND_UDP_INFO_30_REPLY_VERSION	0x01000000

//
// This 32bit integer represents the string "3.2" that we assign to the
// sc_version field in the pathend_udp_info header.
//
#define	SC_VERSION_STR	(('3' << 24) | ('.' << 16) | ('2' << 8))

//
// Endpoint initiate has a UDP exchange protocol too. The chief purpose of
// this protocol is to make sure that both nodes get ready to establish
// TCP connections around the same time, so that the TCP connection attempts
// succeed the first time round so that long TCP connect timeouts can be
// avoided.
//
struct endpoint_udp_info {
	ID_node	node;
};

//
// Init message sent during TCP connection setup to synchronize the module
// pushing
//
struct init_msg {
	tcpmod_header_t	header;
	uint_t		connection;
	ID_node		src;
};

//
// When sending ORB messages that are marked synchronous, tcp_endpoint::send
// waits for an explicit end-to-end ack before returning.
// For reply io messages also we wait for acks for each send_reply_buf
// In waiting for the acks, we have two methods.
// a) We use a per-sendstream cv and do a timed wait in a loop on that cv
//	If all goes well, the ack arrives and wakes up the sendstream in
//	question.
//	If the path goes down, then after the timedwait times out the sendstream
//	checks the endpoint status and returns error
//	This option is got by defining PER_SENDSTREAM_CV below
// b) We use a per endpoint cv and do a cv wait
//	If all goes well, the ack arrives and we do a cv broadcast waking
//	up all sendstreams waiting for an ack.
//	If the path goes down, then after we again cv broadcast and on wake up
//	each sendstream checks the endpoint status and returns error
//
#define	PER_SENDSTREAM_CV

//
// TCP PathEnd:
//	Control/Monitoring connection for tcp transport.
//
// Description:
// 	A UDP socket is opened and messages are exchanged prior to
//	establishing a TCP connection. This avoids the time delay
//	associated with tcp connection timeout period (except when
//	the node goes down immeidately after the udp handshake).
//
//	Pathend has only one connection.
//
// State Changes:
//	Initially, the state is set to TPE_RESET.
//	If this node acts as a server, goes to TPE_INITIATED state
//	as soon as the connection request from the client succeeds.
//	If everything goes well, goes into TPE_CONNECTED state.
//	Else starts with TPE_RESET state.
//
//	If this node acts as a client, things are simple. XX Need to
//	expand on comments for client side
//
//	UDP handshake avoids any infinite chasing. XX Need explanation
//
class tcp_pathend : public pathend {

public:
	tcp_pathend(tcp_transport *, clconf_path_t *);

	~tcp_pathend();

	//
	// Internal state in tcp_pathend.
	// XX Would be nice if these states mapped to those in pathend
	// base class.
	//
	enum tcp_pe_state {	TPE_RESET,
				TPE_INITIATED,
				TPE_CONNECTED
			 };

	//
	// UDP handshake message types exchanged during version
	// compatibility checks. The message types and the three
	// way handshake protocol may not change across releases.
	//
	enum {
		HANDSHAKE_VERSION_INFO = 0,
		ACTIVE_REQUEST,
		PASSIVE_RESPREQ,
		ACTIVE_RESPONSE
	};

	// function to send a panic msg out
	boolean_t panic_notify();

	// accessor methods to lock protecting tcp_pathend internal variables
	void tpe_lock();
	void tpe_unlock();
	bool tpe_lock_held();
	os::mutex_t *tpe_get_lock();

	//
	// functions to recv and wait for initial udp message
	//
	int	udp_init();
	void	udp_reset();
	void	udp_finish();
	void	udp_putq(struct pathend_udp_info *, struct sockaddr_in *,
			struct sonode *);
	int	udp_getq(clock_t);

	// Please maintain the lock ordering between udp_msg_lock and
	// udp_send_buf_lock
	void	udp_msg_lock();
	void	udp_msg_unlock();
	void	udp_msg_signal();

	pe_udp_msg_state	get_udp_msg_state() const;
	void	set_udp_msg_state(pe_udp_msg_state);


	// functions to synchronize initial tcp message exchange
	int		so_putq(struct sonode *);
	struct sonode	*so_getq();
	void		signal_tpe();

	// accessor methods
	in_addr_t	get_local_ipaddr() const;

	in_addr_t	get_remote_ipaddr() const;

	tcp_transport *get_transport() const;

	bool		get_do_esballoc() const;

	// Function to wait for initial tcp msg from peer.
	void	recv_initial_msg(mblk_t *);
	bool	timed_wait_for_msg();

	// Method to complete connection setup and synchronization
	// after initial connection request
	int process_accept(struct sonode *, bool);

	// Mark the so_rmsgs array when the initial msg from peer
	// is received for a connection.
	// Also does a signal_msg_received()
	void	set_msg_received();

	// Check if initial tcp message has been received from peer.
	bool	has_msg_been_received();

	void	signal_msg_received();

	void	set_incn(incarnation_num);


	// Methods to send/receive heartbeat messages
	void	pm_send_internal();

	void	pm_recv_internal(mblk_t *);

	// Methods to record timestamp for bad udp handshake messages
	bool		record_bad_udp_handshake();

private:
	// Memory for heartbeats
	// We allocate two buffers (sendbufp and recvbufp) that store
	// the next heartbeat to be sent out and the last heartbeat
	// received respectively. These are shared with the path manager
	char		*sendbufp;
	char		*recvbufp;
	char		*recv_copy_bufp;
	mblk_t		*send_copy_mp;

	// Temporary storage for remote node's incarnation number while in
	// initiate(). This is because pathend::initiate takes the incarnation
	// as a return parameter and tcp_pathend should not update the
	// variable in base class pathend.
	incarnation_num	tpe_incn;

	// local and remote ip addresses converted from string to in_addr_t
	// during constructor
	in_addr_t	local_ipaddr;
	in_addr_t	remote_ipaddr;

	// do_esblloc is set according to "lazy_free" property read from
	// clconf for local adapter; if "lazy_free" is implemented in the
	// driver, do_esblloc is false, otherwise, do_esblloc is true
	bool		do_esballoc;

	//
	// Buffer to compose udp handshake messages to be sent and the
	// lock to synchronize access to it.
	// Please maintain the lock ordering between udp_msg_lock and
	// udp_send_buf_lock
	//
	char		*udp_send_buf;
	size_t		udp_send_buf_size;
	os::mutex_t	udp_send_buf_lock;

	//
	// The current udp handshake sequence number. The active node
	// increments this sequence number by one whenever it sends out
	// a new ACTIVE_REQUEST. The passive node sets this number to the
	// the sequence number carried in a received ACTIVE_REQUEST messages.
	// Other messages are accepted only if they match this sequence
	// number.
	//
	uint32_t	hs_seq;

	//
	// Lbolt when the most recent bad udp handshake message was received.
	// This is used to determine when to log a message indicating a bad
	// handshake message.
	//
	clock_t		bad_udp_handshake_lbolt;

	void initiate(char *&, char *&, incarnation_num &);

	int udp_client_initiate();

	int udp_server_initiate();

	int client_initiate();

	int server_initiate();

	void _cleanup();	// internal cleanup

	void cleanup();		// called when path goes down

	void reinit();		// called when pathend is recycled.

	void delete_notify();	// called when path is removed.

	// Called to create a new endpoint when the path is declared up
	endpoint *new_endpoint(transport *);

	// tcp_pathend specific state
	tcp_pe_state	tpe_state;

	// lock protecting most variables in tcp_pathend
	// XX Could use the lock in pathend without needing another one
	os::mutex_t	_tpe_lock;

	// UDP socket used for initial messaging before connection setup
	struct sonode	*udp_so;

	//
	// If the remote node connects, then obtain the connection
	// from the server task.
	// Currently only one connection is assumed. Since pathend
	// is not expected to have more than one connection, the code
	// is simplified. endpoint has several connections.
	//
	uint_t		n_connections;

	// First time an initiation error was seen on this pathend.
	os::hrtime_t	first_err_time;
	// Last time a warning was logged for this pathend.
	os::hrtime_t	last_warn_time;
	void		tpe_update_pe_state(int);

	os::condvar_t	tpe_cv;

	struct sonode	*in_so_ptr;	// temporary storage on server side

	// TCP socket for pathend.
	// and precalculated queue pointer from sonode.
	// set after TPE_CONNECTED state
	struct sonode	*so_ptr;
	queue_t		*so_wq;

	// bool/cv corresponding to initial tcp message receipt
	bool		tpe_msg_received;
	os::condvar_t	tpe_msg_cv;

	// state/cv/mutex corresponding to initial udp message exchange
	pe_udp_msg_state	tpe_udp_msg_received;
	os::condvar_t	tpe_udp_msg_cv;
	os::mutex_t	tpe_udp_msg_lock;

	// A flag that indicates whether a heartbeat has been received on
	// this path in the non interrupt context. SC3.0 requires drivers
	// to deliver heartbeat messages in the interrupt context. If this
	// flag is set, it indicates a problem with the network driver. The
	// flag was originally used for debugging, now enabled by default.
	bool		rcvd_nonintr_hb;

	// Pointer to the raw dlpi connection over which heartbeat messages
	// will be sent if the above flag is set.
	raw_dlpi_conn	*rdconnp;

	//
	// Disallow assignments and pass by value
	//
	tcp_pathend(const tcp_pathend &);
	tcp_pathend &operator = (tcp_pathend &);
};

//
// Transport header for tcp_sendstreams
// This follow tcpmod_header_t for all non-tcp transport internal messages
//
struct tcp_message_header {
#ifdef MARSHAL_DEBUG
	uint32_t	marshal_debug;	// constant for validity check
					// must be the first data member
#endif 	// MARSHAL_DEBUG
	uint_t  mlen;		// length of main marshal buffer
	uint_t  xlen;		// length of the xdoor descriptions.
	uint_t  xdnum : 31;	// number of xdoors transported
	uint_t  offline_data : 1; // flag if offline buffers transported
};

//
// tcp_endpoint
//	tcp endpoint class.
//
// Description:
//	tcp endpoint has several connections for data exchange.
//	The nature of the connections are the same as that of
//	the tcp pathend (i.e. reliable datagram connections over
//	tcp).
//
//	Since it has several connections, it has queues for incoming
//	sockets, completed connections, streams queue pointers, and
//	msg received bits.
//
// 	Inherit from generic transport's endpoint object
//
// State Changes:
//	From TEP_RESET in constructor to TEP_INITIATED in initiate().
//	Once all the connections are successfully established, it moves
//	to TEP_CONNECTED state.
//	Destructor moves it to closed state.
//
//
class tcp_endpoint : public endpoint {
public:
	tcp_endpoint(tcp_transport *, tcp_pathend *);

	~tcp_endpoint();

	// accessors
	tcp_transport *get_transport() const;

	void add_resources(resources *);

	sendstream *get_sendstream(resources *resourcep);

	enum endpoint_state { 	TEP_RESET,
				TEP_INITIATED,
				TEP_CONNECTED
			    };


	//
	// functions to recv and wait for initial udp message
	//
	int	udp_init();
	void	udp_finish();
	void	udp_putq(struct endpoint_udp_info *, struct sockaddr_in *);
	int	udp_getq(clock_t);

	void	udp_msg_lock();
	void	udp_msg_unlock();
	void	udp_msg_signal();

	ep_udp_msg_state	get_udp_msg_state() const;
	void	set_udp_msg_state(ep_udp_msg_state);

	// functions to synchronize initial tcp message exchange
	int		so_putq(struct sonode *);
	struct sonode	*so_getq();
	void		signal_tep();

	// send called from tcp_sendstream::send to form the tcpmod header
	// and send the message to tcp, waiting for any acks as needed
	void send(orb_msgtype, orb_seq_t, mblk_t *, uint_t,
			bool, tcp_sendstream *, Environment *);

	// Function called when tcpmod has a message ready to be delivered
	// callee will copy the contents (whatever is needed) from the header
	// by the time the callback returns
	// mem_alloc_type indicates whether the callback can block or not
	int tcp_receive_callback(struct tcpmod_header_t *, mblk_t *,
		os::mem_alloc_type);

	// Function called when tcpmod receives an ack for a synchronous send
	// or a reply io
	void tcp_ack_callback(void *, int, uint_t);

	//
	// Send a TCP_KEEP_ACTIVE message if needed. See comments accompanying
	// the tcp_keep_active_task class declaration for details.
	//
	void send_keep_active_message();

	// Check if endpoint is still registered
	bool is_endpoint_registered();

	// Check if the pathend argument corresponds to the pathend that
	// this endpoint is connected to
	// XX Could be a method on base class endpoint
	bool is_same_pathend(pathend *pe);

	// Accessor functions to get MTU size and write offset
	uint_t	get_mtu_size() const;
	uint_t 	get_wroff() const;

	bool    get_do_esballoc() const;

	//
	// Function to process, once a connection has been established
	//
	int	process_accept(struct sonode *so_acpt, bool);

	//
	// Function to wait for initial transport msg from peer.
	//
	void	recv_initial_msg(uint_t, mblk_t *);
	bool	timed_wait_for_msg(uint_t);

	//
	// Mark the so_rmsgs array when the initial msg from peer
	// is received for a connection.
	// Also does a signal
	//
	void	set_msg_received(uint_t);

	//
	// Check if a (transport) message has been received from peer.
	//
	bool	has_msg_been_received(uint_t);

	void	tep_lock();
	void	tep_unlock();
	bool	tep_lock_held();
	os::mutex_t *tep_get_lock();

	// ID_node of remote node/incn endpoint is setup with
	// XX This should be a method in base class endpoint
	ID_node &get_rnode();

	// Returns queue pointer corresponding to particular connection
	queue_t *get_queue(uint_t);

	// cv used for synchronizing ack receipt
	// See comments above regarding PER_SENDSTREAM_CV
#ifndef PER_SENDSTREAM_CV
	os::condvar_t		ack_cv;
#endif

	//
	// function called whenever a reply io msg is received by tcpmod.
	//
	int tcp_rio_callback(mblk_t *, replyio_info_t *);

	// Method to send reply I/O ack
	void tcp_rio_send_ack(void *cookie, uint_t conn);

	// Method called when tcpmod sees a connection going down
	void process_tcp_reset();

	// Method to map a orb message type to an endpoint tcp connection
	uint_t choose_conn(int);

	bool is_rio_conn(uint_t);

	// Method to determine if replyio ack reduction is on
	bool replyio_acks_used();

	// endpoint class virtual method override
	void	timeouts_disable();
	void	timeouts_enable();

	// For access to timeouts_disabled
	bool	timeouts_are_disabled();
	bool	timeouts_are_enabled();

private:
	// Methods called from generic transport state machine
	void	initiate();

	void	unblock();

	void	push();

	void	cleanup();


	// Internal methods
	void	_cleanup();

	int udp_client_initiate();

	int udp_server_initiate();

	int client_initiate();

	int server_initiate();

	// Protects updates to private members in tcp_endpoint
	os::mutex_t	_tep_lock;

	//
	// for support of timeouts_disable/enable
	// lock ordering is:  so_lock, _tep_lock
	//
	os::mutex_t	so_lock;

	void	set_timeouts(int);


	// tcp_endpoint specific state
	endpoint_state	tep_state;

	// special flag for retransmission timeouts disable/enable
	bool		timeouts_disabled;

	// Copy of values from pathend
	in_addr_t	local_ipaddr;
	in_addr_t	remote_ipaddr;

	bool		do_esballoc;

	//
	// n_connections indicate the number of connections established
	//
	uint_t  n_connections;

	//
	// index to determine the current connection number to
	// send the reply io
	//
	uint_t		curr_rio_index;

	// Socket used for initial UDP synchronization
	struct sonode	*udp_so;

	// XX Need comments explaining what these are for, or a forward
	// pointer to where the comments are
	struct sonode	*in_so_ptrs[TCP_EP_MAX_CONNECTIONS];
	int		in_pindex;
	int		in_cindex;

	os::condvar_t	tep_cv;

	// sockets and corresponding queue pointers for the different
	// TCP connections
	struct sonode	*so_ptrs[TCP_EP_MAX_CONNECTIONS];
	queue_t		*so_wqs[TCP_EP_MAX_CONNECTIONS];

	// Keeps track of connections for which initial TCP messages
	// have arrived.
	// XX Since connection setup is done in a serial fashion,
	// we could have used a single cv and a counter.
	bool		so_rmsgs[TCP_EP_MAX_CONNECTIONS];
	os::condvar_t	so_rmsg_cv[TCP_EP_MAX_CONNECTIONS];

	// state/cv/mutex corresponding to initial udp message exchange
	// XX This is common code to endpoint and could be extracted into
	// a separate class
	ep_udp_msg_state	tep_udp_msg_received;
	os::condvar_t	tep_udp_msg_cv;
	os::mutex_t	tep_udp_msg_lock;

	//
	// mtu_size is TCP segment size to use for this connection
	//	and is used in doing fragmentation before handing messages
	//	to TCP stack
	// wroff is the expected size of headers that we leave space for
	//	at the beginning of mblks, so that TCP stack need not
	//	allocate further mblks for headers
	//
	uint_t	mtu_size;
	uint_t 	wroff;

	// The following flags is used to tell the endpoints whether they
	// should be eliminating replyio acks or not.
	bool use_rio_acks;

	// Total number of connections to be established and the rio base
	// connection index among those.
	uint_t	max_conns;
	uint_t	rio_base;

	//
	// Last clock tick a message was sent on this endpoint.
	//
	clock_t	last_send_lbolt;

	//
	// Disallow assignments and pass by value
	//
	tcp_endpoint(const tcp_endpoint &);
	tcp_endpoint &operator = (tcp_endpoint &);
};


//
// For tcp sendstreams;
//
class tcp_sendstream : public sendstream {

public:
	//
	// tcp_MarshalStream - differs from its parent class in that it
	//	uses mblk's to hold data.
	//
	class tcp_MarshalStream : public MarshalStream {

	public:
		// constructor specifies both first Buf size and chunksize
		tcp_MarshalStream(Environment *e, uint_t byte_size,
		    uint_t chunk_size, tcp_endpoint *ep);

		//
		// Function to allocate a new buffer. does a new of MBlkBuf
		// leaving space for TCP headers
		//
		Buf *newbuf(uint_t nbytes);


	private:
		tcp_endpoint *endp;

	};

	// constructor
	tcp_sendstream(tcp_endpoint *ep, resources *new_resourcep);

	ID_node	&get_dest_node();

	void send(bool, orb_seq_t &);

	//
	// reply io optimization related routines.
	//
	replyio_client_t *add_reply_buffer(struct buf *, align_t);

	bool check_reply_buf_support(size_t, iotype_t, void *, optype_t);

	bool send_reply_buf(Buf *, replyio_server_t *);

	// routine to call when synchronous ack is recvd
	void recvd_sync_ack(uint_t);

	// routine to call when an reply io ack is recvd
	void recvd_rio_ack(uint_t);

	bool wait_for_replyios();

	bool wait_for_sync_ack();

	void set_ack_pending();

	void incr_offline_pending();

	uint_t get_preferred_conn();

	bool is_replyio_req();

protected:
	// destructor always called through sendstream::done()
	~tcp_sendstream();

private:
	tcp_MarshalStream	MainBuf;

	// Endpoint to be used to send data
	tcp_endpoint 		*endp;

	// Any preferred TCP connection within the said endpoint over which
	// the message needs to be sent. The preference comes into picture
	// when a RIO reply needs to  be sent on the same connection that
	// carried the associated offline data.
	uint_t			prefer_conn;

	// A flag to indicate whether this sendstream is meant for a replyio
	// request or reply. This will be used to ensure that a reply io
	// request travels over the RIO connections and that a RIO reply
	// goes over the same connection that carried the offline data.
	// This flag gets set in add_reply_buffer and in put_offline_reply..
	bool			replyio_ss;

	// # of offline replies pending
	uint_t			offline_pending;
	bool			ack_pending;
#ifdef PER_SENDSTREAM_CV
	os::condvar_t		ack_cv;
#endif
	// In case ack_pending is set, when did the wait for
	// ack start?
	clock_t		waiting_since;

	//
	// Disallow assignments and pass by value
	//
	tcp_sendstream(const tcp_sendstream &);
	tcp_sendstream &operator = (tcp_sendstream &);
};

//
// For tcp recstreams;
//
class tcp_recstream : public recstream {

public:
	// Constructor
	tcp_recstream(mblk_t *, ID_node &, orb_msgtype, tcp_endpoint *);

	ID_node		&get_src_node();

	bool initialize(os::mem_alloc_type);

	//
	// reply io optimization related routines.
	//
	replyio_server_t *get_reply_buffer();
	void	get_reply_buffer_cancel();

protected:
	// Always called through recstream::done()
	~tcp_recstream();

private:
	// tcp_transport does not get a hold on the endpoint on which the
	// message was received (like rsm_transport does). We store the
	// node/incn from which we received the message here.
	ID_node			src_node;

	// recstream::initialize can be called multiple times,
	// _mp store the list of unprocessed mblks
	mblk_t			*_mp;

	// Store a copy of the tcp_message_header, since recstream::initialize
	// may be called multiple times to complete the initialization, we
	// read the header the first time and remember it so later calls to
	// initialize can complete without reconstructing the header.
	tcp_message_header	mh;

	//
	// Preallocating MBlkBuf structures avoids memory allocation
	// problems when we want to convert the mblk's containing the
	// message headers into Buf structures that can be used in a recstream.
	// The leading mblks can be converted into Buf structures without
	// any further memory allocation operations.
	//
	// This also reduces the total number of memory allocation operations,
	// which should be a performance improvement overall. The size
	// of an MBlkBuf is small, so memory consumption is minor.
	//
	MBlkBuf		mblkbuf1;

	//
	// Disallow assignments and pass by value
	//
	tcp_recstream(const tcp_recstream &);
	tcp_recstream &operator = (tcp_recstream &);
};

#pragma enable_warn

//
// Information about adapter.
// Tcp transport has no specific information to store
//
class tcp_adapter : public adapter {

public:
	tcp_adapter(tcp_transport *, clconf_adapter_t *);

	~tcp_adapter();

private:
	// Disallow assignments and pass by value
	tcp_adapter(const tcp_adapter &);
	tcp_adapter &operator = (tcp_adapter &);
};


//
// tcp_transport
//	tcp transport class
//
// Description:
//	This can be thought of as the controlling class for
//	the tcp transport. It has methods for listening
//	and accepting connections. It delivers the connections
//	to the appropriate pathend or endpoint.
//
//	It also has housekeeping information about pathends,
//	endpoints, and adapters.
//
//	Anything common to all the tcp paths can be handled here.
//
//
class tcp_transport : public transport {
	friend class tcp_adapter;
public:
	tcp_transport();
	~tcp_transport();

	// function used by the callback registered with the panic subsystem
	boolean_t panic_notify();

	//
	// Function to add a tcp_endpoint to the list of endpoints in initiate
	// used in get_tcp_endpoint
	//
	void add_tep(tcp_endpoint *);
	void remove_tep(tcp_endpoint *);

	//
	// Start the server which listens for tcp connections
	// for the Path End.
	//
	int	start_server_pe();

	//
	// Start the server which listens for tcp connections
	// for the EndPoint.
	//
	int	start_server_ep();

	//
	// udp server routines.
	//
	int	start_udp_server_pe();
	int	start_udp_server_ep();

	//
	// The method to drive the TCP_KEEP_ACTIVE messages.
	//
	void	keep_active();

	// Call to either add or remove threads from the rio_threadpool
	void create_rio_threads(int);

	// Use to post a task to a rio_threadpool
	void rio_post(defer_task *);

	// pm_client virtual method override
	void timeouts_disable();
	void timeouts_enable();

	bool is_reaper_running();
	void reaper_running();
	void reaper_not_avail();

	bool	is_shutdown;

	//
	// Used to post a deferred task to the tcptr_threadpool. Also does
	// book keeping of number of tasks started.
	//
	void		post(defer_task *t);
	void		unpost();

private:
	const char *transport_id();

	pathend *new_pathend(clconf_path_t *);

	adapter *new_adapter(clconf_adapter_t *);

	tcp_pathend *get_tcp_pathend(struct sonode *so);

	tcp_endpoint *get_tcp_endpoint(struct sonode *so);

	tcp_pathend *get_tcp_pathend_for_ipaddr(in_addr_t);

	tcp_endpoint *get_tcp_endpoint_for_ipaddr(in_addr_t);

	void		shutdown();

	//
	// Buffer to receive pathend udp handshake messages. This
	// handshake is responsible for version negotiation. One buffer
	// is sufficient as one message is received and processed at
	// a time.
	//
	char		*pe_udp_recv_buf;
	size_t		pe_udp_recv_buf_size;

	//
	// List of tcp_endpoints in initiate state and lock
	// Used in get_tcp_endpoint function
	//
	DList<tcp_endpoint>	endp_list;
	os::mutex_t		tep_list_lock;

	// Threadpool for handling reply io bcopying
	threadpool		rio_threadpool;
	// Threadpool for listener tasks
	threadpool		tcptr_threadpool;

	//
	// Counter and associated mutex and cv for keeping track of
	// tasks created by the tcp transport. Used mostly for clean
	// shutdown.
	//
	int			task_count;
	os::mutex_t		task_lock;
	os::condvar_t		task_cv;
	bool			reaper_is_running;

	//
	// Disallow assignments and pass by value
	//
	tcp_transport(const tcp_transport &);
	tcp_transport &operator = (tcp_transport &);
};


//
// A task to start server
//
// One server for pathends and one server for endpoints are
// started. These servers listen for incoming connections.
//
class start_server_task : public defer_task {

public:
	start_server_task(tcp_transport *transp,
				tcptr_objtype objtype,
				int  server_type);

	void execute();

private:
	tcp_transport	*transp;
	tcptr_objtype 	objtype;
	int	server_type;	// protocol is stored here e.g. tcp, udp
				// uses the same constant used by IP
				// i.e IPROTO_TCP etc.

	// Disallow assignments and pass by value
	start_server_task(const start_server_task &);
	start_server_task &operator = (start_server_task &);
};

//
// The tcp_keep_active_task is used to send TCP_KEEP_ACTIVE messages
// periodically on idle endpoints. A single instance of this object is
// created when the tcp_transport object is created. The purpose of the
// TCP_KEEP_ACTIVE messages is to keep traffic flowing on idle TCP
// connections belonging to the TCP Transport. If traffic keeps flowing,
// any TCP/IP communication problems will be detected through endpoint
// TCP connection drops. This provides a way to detect TCP/IP communication
// problems on the cluster interconnect even when there is no cluster
// infrastructure traffic. It is important to detect such communication
// failure as user applications might rely on TCP/IP communication even
// when there is no cluster infrastructure traffic.
//
// Why don't we simply use the SO_KEEPALIVE option? There is no way to
// tune TCP's keep alive interval on a per socket basis. We can not use
// the default 2hrs timeout. At the same time setting the system wide
// timeout to our preferred value of ~90sec is not advisable either.
// Besides there is already a mechanism to catch TCP connection drops
// in the tcp transport when TCP level acks don't arrive for cluster
// infrastructure traffic. Piggybacking on that mechanism is the most
// natural way for detecting communication problems even in the absence
// of infrastructure traffic.
//
class tcp_keep_active_task : public defer_task {
public:
	tcp_keep_active_task(tcp_transport *);

	void execute();

private:
	tcp_transport *transp;
};

extern size_t mp_to_buf(mblk_t *bp, char *bufp, size_t size);
extern in_addr_t get_peeraddr(struct sonode *so);

#ifndef NOINLINES
#include <transports/tcp/tcp_transport_in.h>
#endif  // _NOINLINES

#endif	/* _TCP_TRANSPORT_H */
