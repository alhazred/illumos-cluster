/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _TCP_UTIL_H
#define	_TCP_UTIL_H

#pragma ident	"@(#)tcp_util.h	1.9	09/02/13 SMI"

#include <sys/types.h>
#include <netinet/in.h>
#include <orb/debug/haci_debug.h>

#define	KSTOSO(ks)	((struct sonode *)(ks))
#define	SOTOKS(so)	((ksocket_t)(uintptr_t)(so))

//
// Debug buffers for tcp transport. These buffers log connection
// establishment information. Per message logging goes to the
// extension buffer described below. The udp_debug buffer logs
// the initial UDP handshake activity. All other messages go to
// the tcp_debug buffer.
//
#if defined(HACI_DBGBUFS) && !defined(NO_TCP_DBGBUF)
#define	TCP_DBGBUF
#endif

#ifdef	TCP_DBGBUF
extern dbg_print_buf    tcp_debug;
extern dbg_print_buf    udp_debug;
#define	TCP_DBPRINTF(f) HACI_DEBUG(ENABLE_TCP_DBG, tcp_debug, f)
#define	UDP_DBPRINTF(f)	udp_debug.dbprintf f
#else
#define	TCP_DBPRINTF(f)
#define	UDP_DBPRINTF(f)
#endif

//
// Extension debug buffer for tcp transport. This buffer is used to
// print per message debugging information. This buffer has been
// split from tcp_debug so that the per message logs do not overrun
// the connection state log.
//
#if defined(HACI_DBGBUFS) && !defined(NO_ETCP_DBGBUF)
#define	ETCP_DBGBUF
#endif

extern int tcptr_trace_options;

const int	TCPTR_TRACE_RIO_TCPMOD	= 0x0001;
const int	TCPTR_TRACE_NRIO_TCPMOD	= 0x0002;
const int	TCPTR_TRACE_PE_TCPMOD	= 0x0004;
const int	TCPTR_TRACE_SEND	= 0x0008;
const int	TCPTR_TRACE_RIO_DETAILS	= 0x0010;
const int	TCPTR_TRACE_RIO_MINIMAL	= 0x0020;
const int	TCPTR_TRACE_SYNC_ACK	= 0x0040;
const int	TCPTR_TRACE_EP_ERR	= 0x0080;
const int	TCPTR_TRACE_PE_CONNECT	= 0x0100;

#ifdef	ETCP_DBGBUF
extern dbg_print_buf    etcp_debug;
#define	ETCP_DBPRINTF(option, f) if ((option) & tcptr_trace_options) \
	HACI_DEBUG(ENABLE_ETCP_DBG, etcp_debug, f)
#else
#define	ETCP_DBPRINTF(option, f)
#endif

// The maximum mac addr size is a randomly chosen number. It works
// for the currently supported transports - Ethernet, SCI, IB. If a new
// transport is supported and it uses a larger mac addr,
// this constant must be updated.
// Note: IB has a 20-byte MAC address. Add 2 (for the sap) to get 22.
#define	MAX_MAC_ADDR_SIZE	22

// The (estimated) maximum size of mac header that gets shipped with the
// data on the wire. This is only a performance optimization, a wrong value
// will not impact correctness. We leave this much space in the front on the
// data mblk empty before copying data. This is done so that the dlpi driver
// does not have to allocate another mblk for the header. Ethernet and SCI
// are happy with 32. If more transports are supported in the future, this
// number might need revision.
#define	MAX_MAC_HEADER_SIZE	32

// Utility routines
class tcp_util {
public:
	static char *iptostr(in_addr_t, char *);
	static char *mactostr(uchar_t *, size_t, char *);
	static bool macs_equal(uchar_t *, uchar_t *, size_t);
};
#endif	/* _TCP_UTIL_H */
