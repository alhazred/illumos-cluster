/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  tcp_transport_in.h
 *
 */

#ifndef _TCP_TRANSPORT_IN_H
#define	_TCP_TRANSPORT_IN_H

#pragma ident	"@(#)tcp_transport_in.h	1.42	08/05/20 SMI"

//
// Contains all the inline functions for all the tcp transport classes
// i.e pathend, endpoint, transport, adapter etc.
//

inline tcp_transport *
tcp_pathend::get_transport() const
{
	return ((tcp_transport *)transportp);
}

inline os::mutex_t *
tcp_pathend::tpe_get_lock()
{
	return (&_tpe_lock);
}

inline void
tcp_pathend::tpe_lock()
{
	tpe_get_lock()->lock();
}

inline bool
tcp_pathend::tpe_lock_held()
{
	return (tpe_get_lock()->lock_held());
}

inline void
tcp_pathend::tpe_unlock()
{
	tpe_get_lock()->unlock();
}

//
// Called on receiving the initial message on tcp_pathend to store
// the incarnation number of the remote node. This is stored here
// till pathend::initiate() returns this value to the generic pathend
inline void
tcp_pathend::set_incn(incarnation_num incn)
{
	ASSERT(tpe_lock_held());
	tpe_incn = incn;
}

// Verify whether the initial TCP message has been received
// not holding locks, so this is a hint but is guaranteed to read a single byte
// and will read a consistent value
inline bool
tcp_pathend::has_msg_been_received()
{
	return (tpe_msg_received);
}

// Called on receiving initial TCP message and when removing path
inline void
tcp_pathend::signal_msg_received()
{
	ASSERT(tpe_lock_held());
	tpe_msg_cv.signal();
}

// Call on receiving initial TCP message, sets flag to true and wakes up
// the thread in process_accept
inline void
tcp_pathend::set_msg_received()
{
	ASSERT(tpe_lock_held());
	ASSERT(!tpe_msg_received);
	tpe_msg_received = true;
	signal_msg_received();
}

inline void
tcp_pathend::signal_tpe()
{
	tpe_cv.signal();
}

inline pe_udp_msg_state
tcp_pathend::get_udp_msg_state() const
{
	return (tpe_udp_msg_received);
}

inline void
tcp_pathend::set_udp_msg_state(pe_udp_msg_state val)
{
	tpe_udp_msg_received = val;
}

inline void
tcp_pathend::udp_msg_lock()
{
	tpe_udp_msg_lock.lock();
}

inline void
tcp_pathend::udp_msg_unlock()
{
	tpe_udp_msg_lock.unlock();
}

inline void
tcp_pathend::udp_msg_signal()
{
	tpe_udp_msg_cv.signal();
}

inline in_addr_t
tcp_pathend::get_local_ipaddr() const
{
	return (local_ipaddr);
}

inline in_addr_t
tcp_pathend::get_remote_ipaddr() const
{
	return (remote_ipaddr);
}

inline bool
tcp_pathend::get_do_esballoc() const
{
	return (do_esballoc);
}

inline tcp_transport *
tcp_endpoint::get_transport() const
{
	return ((tcp_transport *)transportp);
}

// not holding locks, so this is a hint but is guaranteed to read a single word
// and will read a consistent value
inline bool
tcp_endpoint::is_endpoint_registered()
{
	return (get_state() == E_REGISTERED);
}

inline bool
tcp_endpoint::is_same_pathend(pathend *pe)
{
	return (pathendp == pe);
}

inline os::mutex_t *
tcp_endpoint::tep_get_lock()
{
	return (&_tep_lock);
}

inline void
tcp_endpoint::tep_lock()
{
	tep_get_lock()->lock();
}

inline void
tcp_endpoint::tep_unlock()
{
	tep_get_lock()->unlock();
}

inline bool
tcp_endpoint::tep_lock_held()
{
	return (tep_get_lock()->lock_held());
}


inline void
tcp_endpoint::set_msg_received(uint_t connection)
{
	ASSERT(tep_lock_held());
	ASSERT(!so_rmsgs[connection]);
	so_rmsgs[connection] = true;
	so_rmsg_cv[connection].signal();
}

// Not holding any locks, assuming safe atomic read
inline bool
tcp_endpoint::has_msg_been_received(uint_t connection)
{
	return (so_rmsgs[connection]);
}

inline void
tcp_endpoint::signal_tep()
{
	ASSERT(tep_lock_held());
	tep_cv.signal();
}

inline ep_udp_msg_state
tcp_endpoint::get_udp_msg_state() const
{
	return (tep_udp_msg_received);
}

inline void
tcp_endpoint::set_udp_msg_state(ep_udp_msg_state val)
{
	tep_udp_msg_received = val;
}

inline void
tcp_endpoint::udp_msg_lock()
{
	tep_udp_msg_lock.lock();
}

inline void
tcp_endpoint::udp_msg_unlock()
{
	tep_udp_msg_lock.unlock();
}

inline void
tcp_endpoint::udp_msg_signal()
{
	tep_udp_msg_cv.signal();
}

inline ID_node &
tcp_endpoint::get_rnode()
{
	return (ep_node);
}

inline queue_t *
tcp_endpoint::get_queue(uint_t conn)
{
	return (so_wqs[conn]);
}

// We assume that the last few connections are for replyio
// starting at TCP_RIO_CONN_BASE..TCP_RIO_CONN_BASE+TCP_MAX_REPLYIO_CONNECTIONS
inline bool
tcp_endpoint::is_rio_conn(uint_t conn)
{
	ASSERT(conn < max_conns);
	return (conn >= rio_base);
}

// Return the MTU for this endpoint
inline uint_t
tcp_endpoint::get_mtu_size() const
{
	return (mtu_size);
}

// Return the write offset (space to reserve for tcp/ip headers)
// for this endpoint
inline uint_t
tcp_endpoint::get_wroff() const
{
	return (wroff);
}

inline bool
tcp_endpoint::get_do_esballoc() const
{
	return (do_esballoc);
}


inline bool
tcp_endpoint::replyio_acks_used()
{
	return (use_rio_acks);
}

inline bool
tcp_endpoint::timeouts_are_disabled()
{
	return (timeouts_disabled ? true : false);
}

inline bool
tcp_endpoint::timeouts_are_enabled()
{
	return (timeouts_disabled ? false : true);
}

inline void
tcp_sendstream::set_ack_pending()
{
	// Setting this without holding locks as we have not sent the message
	// and we could not have anybody waiting for this or signalling yet
	ASSERT(!ack_pending);
	ack_pending = true;
}

inline void
tcp_sendstream::incr_offline_pending()
{
	endp->tep_lock();
	offline_pending++;
	ASSERT(offline_pending != 0);	// check wraparound
	endp->tep_unlock();
}

inline uint_t
tcp_sendstream::get_preferred_conn()
{
	return (prefer_conn);
}

inline bool
tcp_sendstream::is_replyio_req()
{
	return (replyio_ss);
}

inline bool
tcp_transport::is_reaper_running()
{
	return (reaper_is_running);
}

inline void
tcp_transport::reaper_running()
{
	reaper_is_running = true;
}

inline void
tcp_transport::reaper_not_avail()
{
	reaper_is_running = false;
}

inline pathend *
tcp_transport::new_pathend(clconf_path_t *pp)
{
	return (new tcp_pathend(this, pp));
}

inline void
tcp_transport::post(defer_task *t)
{
	tcptr_threadpool.defer_processing(t);
	task_lock.lock();
	task_count++;
	task_lock.unlock();
}

inline void
tcp_transport::unpost()
{
	task_lock.lock();
	ASSERT(task_count > 0);
	task_count--;
	if (task_count == 0)
		task_cv.broadcast();
	task_lock.unlock();
}

inline void
tcp_transport::rio_post(defer_task *t)
{
	rio_threadpool.defer_processing(t);
}

inline void
tcp_transport::add_tep(tcp_endpoint *ep)
{
	tep_list_lock.lock();
	ASSERT(!endp_list.exists(ep));
	endp_list.prepend(ep);
	tep_list_lock.unlock();
}

inline void
tcp_transport::remove_tep(tcp_endpoint *ep)
{
	tep_list_lock.lock();
	ASSERT(endp_list.exists(ep));
	endp_list.erase(ep);
	tep_list_lock.unlock();
}

inline tcp_pathend *
tcp_transport::get_tcp_pathend(struct sonode *newso)
{
	return (get_tcp_pathend_for_ipaddr(get_peeraddr(newso)));
}

inline tcp_endpoint *
tcp_transport::get_tcp_endpoint(struct sonode *newso)
{
	return (get_tcp_endpoint_for_ipaddr(get_peeraddr(newso)));
}

inline
start_server_task::start_server_task(tcp_transport *trp,
    tcptr_objtype objt, int proto) :
	transp(trp),
	objtype(objt),
	server_type(proto)
{
}

inline
tcp_keep_active_task::tcp_keep_active_task(tcp_transport *trp) :
	transp(trp)
{
}

inline void
tcp_keep_active_task::execute()
{
	transp->keep_active();
	delete this;
}

#endif	/* _TCP_TRANSPORT_IN_H */
