/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)arp_cache_monitor.cc	1.6	09/02/13 SMI"

#include <sys/systm.h>
#include <sys/file.h>
#include <sys/socket.h>
#include <sys/sockio.h>
#include <net/if_arp.h>
#include <orb/infrastructure/common_threadpool.h>
#include <transports/tcp/arp_cache_monitor.h>
#ifdef	_KERNEL
#include <sys/kstr.h>
#endif

// Pointer to the arp_cache_monitor object
arp_cache_monitor	*arp_cache_monitor::the_acmp = NULL;

//
// _arp_cache_monitor_init()
//
// Called from cl_dlpitrans' _init() routine when the module is
// loaded. Calls arp_cache_monitor::initialize to initialize the
// arp_cache_monitor object.
//
void
_arp_cache_monitor_init(void)
{
	arp_cache_monitor::initialize();
}

//
// _arp_cache_monitor_fini()
//
// Called from the cl_dlpitrans' _fini routine.
//
void
_arp_cache_monitor_fini(void)
{
}

//
// arp_cache_monitor constructor
//
arp_cache_monitor::arp_cache_monitor() :
	shutting_down(false),
	running(false),
	adt(this)
{
	iter.reinit(ipmacs);
	common_threadpool::the().defer_processing(&adt);
}

//
// arp_cache_monitor destructor
//
arp_cache_monitor::~arp_cache_monitor()
{
	ASSERT(ipmacs.empty());
}

//
// arp_cache_monitor::the()
//
// Returns reference to the arp cache monitor object
//
arp_cache_monitor&
arp_cache_monitor::the()
{
	return (*the_acmp);
}

//
// arp_cache_monitor::initialize()
//
// Creates the arp_cache_monitor object
//
void
arp_cache_monitor::initialize()
{
	the_acmp = new arp_cache_monitor();
}

//
// arp_cache_monitor::shutdown()
//
// Deletes the arp_cache_monitor object
//
void
arp_cache_monitor::shutdown()
{
	ASSERT(the_acmp);
	acm_lock.lock();
	shutting_down = true;
	acm_cv.signal();
	while (running) {
		acm_cv.wait(&acm_lock);
	}
	delete the_acmp;
	the_acmp = NULL;
}

//
// arp_cache_monitor::register_ipmac()
//
// Register an IP address for monitoring. The first parameter is the IP
// address to be monitored. The Following two parameters are the MAC address
// and the MAC address length the arp cache entry should have for this IP
// address. The last parameter specifies whether it is a local IP address.
// Recovery procedure is different for local and remore IP addresses.
//
// Called from raw_dlpi_adapter::open() for local IP
//    and from raw_dlpi_conn::connect() for remote IP
//
void
arp_cache_monitor::register_ipmac(in_addr_t ip, uchar_t *maca, size_t macl,
    bool lcl)
{
	acm_ipmac *ipmac = new acm_ipmac(ip, maca, macl, lcl);
	acm_lock.lock();
	ipmacs.append(ipmac);
	TCP_DBPRINTF(("IPMAC register 0x%x ip 0x%x count = %d\n",
	    ipmac, (int)ipmac->get_ip(), ipmacs.count()));
	iter.reinit(ipmacs);
	acm_cv.signal();
	acm_lock.unlock();
}

//
// arp_cache_monitor::unregister_ipmac()
//
// Unregister an IP address when no more monitoring is needed
//
// Called from raw_dlpi_adapter::close() for local IP
//    and from raw_dlpi_conn::close() for remote IP
//
void
arp_cache_monitor::unregister_ipmac(in_addr_t ip)
{
	acm_ipmac *ipmac;
	acm_lock.lock();
	IntrList<acm_ipmac, _SList>::ListIterator tmpiter(ipmacs);
	while ((ipmac = tmpiter.get_current()) != NULL) {
		if (ipmac->get_ip() == ip) {
			ASSERT(ipmacs.exists(ipmac));
			(void) ipmacs.erase(ipmac);
			break;
		}
		tmpiter.advance();
	}
	ASSERT(ipmac != NULL);
	TCP_DBPRINTF(("IPMAC unregister 0x%x ip 0x%x count = %d\n",
	    ipmac, (int)ipmac->get_ip(), ipmacs.count()));
	ipmac->rele();
	iter.reinit(ipmacs);
	acm_cv.signal();
	acm_lock.unlock();
}

//
// struct sonode *arp_cache_monitor::prepare()
//
// Set up infrastructure to access arp cache. Creates a datagram socket.
// Return sonode pointer on success, NULL on failure.
//
struct sonode *
arp_cache_monitor::prepare()
{
	int error = 0;
	struct sonode *so;

#if	SOL_VERSION >= __s11
	ksocket_t	ksp;

	error = ksocket_socket(&ksp, AF_INET, SOCK_DGRAM, 0,
	    KSOCKET_SLEEP, CRED());

	if (error != 0) {
		TCP_DBPRINTF(("acm: ksocket create failed, error = %d\n",
		    error));
		return (NULL);
	}

	so = KSTOSO(ksp);
#else
	vnode_t *vp;
	// Open a datagram socket vnode
	if ((vp = solookup(AF_INET, SOCK_DGRAM, 0, NULL, &error)) == NULL) {
		TCP_DBPRINTF(("acm: solookup failed, error = %d\n", error));
		return (NULL);
	}

	// Create a socket for sending ioctls
	if ((so = socreate(vp, AF_INET, SOCK_DGRAM, 0, SOV_SOCKSTREAM, NULL,
	    &error)) == NULL) {
		TCP_DBPRINTF(("acm: socreate failed, error = %d\n", error));
		return (NULL);
	}
#endif
	return (so);
}

//
// arp_cache_monitor::cleanup()
//
// Cleanup any infrastructure we needed to access arp cache. Basically
// closes the datagram socket.
//
void
arp_cache_monitor::cleanup(struct sonode *so)
{
	if (so) {
#if	SOL_VERSION >= __s11
		ksocket_t	ksp;

		ksp = SOTOKS(so);
		ksocket_close(ksp, CRED());
#else
		(void) VOP_CLOSE(SOTOV(so), FREAD|FWRITE, 1, (offset_t)0,
		    CRED());
		VN_RELE(SOTOV(so));
#endif
	}
}

//
// arp_cache_monitor::run()
//
// Loop through the registered IP addresses and monitor the ARP cache
// until told to shutdown.
//
void
arp_cache_monitor::run()
{
	acm_ipmac *ipmac;
	os::usec_t sleepdur;
	struct sonode *so;

	acm_lock.lock();
	running = true;
	acm_lock.unlock();
	so = prepare();
	acm_lock.lock();
	while (so != NULL && !shutting_down) {
		// If there is nothing to monitor, wait until there
		// is something to monitor
		if (ipmacs.count() == 0) {
			acm_cv.wait(&acm_lock);
		}

		// At least 5 secs between each iteration.
		sleepdur = 5000000;
		iter.reinit(ipmacs);
		while (!shutting_down && (ipmac = iter.get_current()) != NULL) {
			iter.advance();
			ipmac->hold();
			acm_lock.unlock();
			ipmac->do_arp(so);
			acm_lock.lock();
			ipmac->rele();

			if (!shutting_down) {
				// Sleep for 100 msecs between each query
				os::systime timo((os::usec_t)100000);
				sleepdur -= 100000;
				(void) acm_cv.timedwait(&acm_lock, &timo);
			}
		}
		// Sleep until next iteration
		if (!shutting_down && sleepdur > 0) {
			os::systime timo(sleepdur);
			(void) acm_cv.timedwait(&acm_lock, &timo);
		}
	}
	acm_lock.unlock();
	cleanup(so);
	acm_lock.lock();
	running = false;
	acm_cv.signal();
	acm_lock.unlock();
}

//
// acm_ipmac constructor
//
acm_ipmac::acm_ipmac(in_addr_t ip, uchar_t *maca, size_t macl, bool lcl) :
	_SList::ListElem(this),
	refcnt(1),
	local(lcl),
	ipaddr(ip),
	mac_len(macl)
{
	ASSERT(macl <= MAX_MAC_ADDR_SIZE);
	bcopy(maca, real_mac, macl);
	bcopy(maca, arp_mac, macl);
	TCP_DBPRINTF(("IPMAC created 0x%x\n", ipaddr));
}

//
// acm_ipmac destructor
//
acm_ipmac::~acm_ipmac()
{
	TCP_DBPRINTF(("IPMAC deleted 0x%x\n", ipaddr));
}

//
// acm_ipmac::get_ip()
//
// Return the IP address
//
in_addr_t
acm_ipmac::get_ip()
{
	return (ipaddr);
}

//
// acm_ipmac::do_arp()
//
// Queries the arp cache for the IP address, verifies if it is valid.
// Logs error message if invalid. Corrects the error.
// Called from arp_cache_monitor::run()
// Calls acm_ipmac::verify_arp_mac() for verification and error logging.
//
void
acm_ipmac::do_arp(struct sonode *so)
{
	struct arpreq arpr;
	struct strioctl iocb;
	struct sockaddr_in *sin;
	uchar_t *maca;
	int32_t	error = 0;

	ASSERT(so != NULL);

	// Prepare for query
	bzero(&arpr, sizeof (struct arpreq));
	arpr.arp_pa.sa_family = AF_INET;
	sin = (struct sockaddr_in *)&arpr.arp_pa;
	sin->sin_family = AF_INET;
	sin->sin_addr.s_addr = ipaddr;

	iocb.ic_cmd = SIOCGARP;
	iocb.ic_timout = 0;
	iocb.ic_len = (int)sizeof (struct arpreq);
	iocb.ic_dp = (char *)&arpr;

#if SOL_VERSION >= __s11
	SOP_IOCTL(so, I_STR, (intptr_t)&iocb, FKIOCTL, CRED(), &error);
	if ((error != 0) ||!(arpr.arp_flags & ATF_COM)) {
		return;
	}
#else
	// Query arp cache
	if (kstr_ioctl(SOTOV(so), I_STR, (intptr_t)&iocb) < 0 ||
	    !(arpr.arp_flags & ATF_COM)) {
		// Either the arp entry could not be extracted, or it
		// is not complete. In either case, ignore.
		return;
	}
#endif
	// Complete arp entry found
	maca = (uchar_t *)arpr.arp_ha.sa_data;
	if (verify_arp_mac(maca)) {
		// The ARP entry was found to be good, no more action is
		// required.
		return;
	}

	// A bad arp entry was found.
	if (local) {
		// The bad entry is for a local IP address. Once we are
		// running with a fix for kernel/tcp-ip bug #4363786, this
		// case should never be encountered. At that time, this
		// block should be replaced by an ASSERT(false);
		// For now, however, correct the arp entry.

		int oldflags = arpr.arp_flags;
		bzero(&arpr, sizeof (struct arpreq));
		arpr.arp_pa.sa_family = AF_INET;
		sin = (struct sockaddr_in *)&arpr.arp_pa;
		sin->sin_family = AF_INET;
		sin->sin_addr.s_addr = ipaddr;
		maca = (uchar_t *)arpr.arp_ha.sa_data;
		bcopy(real_mac, maca, mac_len);
		arpr.arp_flags = oldflags;

		iocb.ic_cmd = SIOCSARP;	//lint !e737
		iocb.ic_timout = 0;
		iocb.ic_len = (int)sizeof (struct arpreq);
		iocb.ic_dp = (char *)&arpr;

#if SOL_VERSION >= __s11
		SOP_IOCTL(so, I_STR, (intptr_t)&iocb, FKIOCTL, CRED(),
		    &error);
#else
		// Ignore errors as there is nothing better to do.
		(void) kstr_ioctl(SOTOV(so), I_STR, (intptr_t)&iocb);
#endif
	} else {
		// A bad entry for a remote address. Unfortunately ARP/IP
		// cannot help against such occurences. However, since we
		// are part of a cluster, we know for sure that this is
		// a bad arp entry. The administrator has already been
		// notified of the problem. We will try to avoid any path
		// downs in the meantime while the adminstraror fixes the
		// real problem by deleting the corrupt arp entry.

		arpr.arp_pa.sa_family = AF_INET;
		sin = (struct sockaddr_in *)&arpr.arp_pa;
		sin->sin_family = AF_INET;
		sin->sin_addr.s_addr = ipaddr;

		iocb.ic_cmd = SIOCDARP;	//lint !e737
		iocb.ic_timout = 0;
		iocb.ic_len = (int)sizeof (struct arpreq);
		iocb.ic_dp = (char *)&arpr;

#if SOL_VERSION >= __s11
		SOP_IOCTL(so, I_STR, (intptr_t)&iocb, FKIOCTL, CRED(),
		    &error);
#else
		// Ignore errors as there is nothing better to do.
		(void) kstr_ioctl(SOTOV(so), I_STR, (intptr_t)&iocb);
#endif
	}
}

//
// acm_ipmac::verify_arp_mac()
//
// Verify if the MAC address obtained from the arp cache is good for this
// IP address. Log a message if not.
// Returns true for good MAC addresses, false otherwise.
// Called fro acm_ipmac::do_arp()
//
bool
acm_ipmac::verify_arp_mac(uchar_t *maca)
{
	char macbuf[4*MAX_MAC_ADDR_SIZE];
	char ipbuf[16];

	os::sc_syslog_msg acm_syslog(SC_SYSLOG_TCP_TRANSPORT, "Tcp", NULL);

	if (tcp_util::macs_equal(maca, real_mac, mac_len)) {
		// Things are good
		bcopy(maca, arp_mac, mac_len);
		return (true);
	}

	// A wrong mac address has found its way into the ARP cache.
	// Log a warning unless it is the same old address for which
	// we have already logged a warning.

	(void) tcp_util::mactostr(maca, mac_len, macbuf);
	(void) tcp_util::iptostr(ipaddr, ipbuf);
	if (!tcp_util::macs_equal(maca, arp_mac, mac_len)) {
		bcopy(maca, arp_mac, mac_len);
		//
		// SCMSGS
		// @explanation
		// The transport at the local node detected an arp cache entry
		// that showed the specified MAC address for the above IP
		// address. The IP address is in use at this cluster on the
		// private network. However, the MAC address is a foreign MAC
		// address. A possible cause is that this machine received an
		// ARP request from another machine that does not belong to
		// this cluster, but hosts the same IP address using the above
		// MAC address on a network accessible from this machine. The
		// transport has temporarily corrected the problem by flushing
		// the offending arp cache entry. However, unless corrective
		// steps are taken, TCP/IP communication over the relevant
		// subnet of the private interconnect might break down, thus
		// causing path downs.
		// @user_action
		// Make sure that no machine outside this cluster hosts this
		// IP address on a network reachable from this cluster. If
		// there are other sunclusters sharing a public network with
		// this cluster, make sure that their private network adapters
		// are not miscabled to the public network. By default all
		// sunclusters use the same set of IP addresses on their
		// private networks.
		//
		(void) acm_syslog.log(SC_SYSLOG_WARNING, MESSAGE,
		    "TCPTR: Machine with MAC address %s is using cluster"
		    " private IP address %s on a network reachable from me."
		    " Path timeouts are likely.", macbuf, ipbuf);
	}
	return (false);
}

//
// acm_defer_task constructor
//
acm_defer_task::acm_defer_task(arp_cache_monitor *ap) :
	acmp(ap)
{
}

//
// acm_defer_task::execute()
//
// Calls acm_cache_monitor methods to the real job.
//
void
acm_defer_task::execute()
{
	acmp->run();
}

//
// acm_defer_task::task_done()
//
// This empty method is needed as the defer task was not directly allocated
// from heap and hence we do not want the default behavior of delete this
// on task done.
//
void
acm_defer_task::task_done()
{
}
