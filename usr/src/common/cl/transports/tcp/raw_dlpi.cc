/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)raw_dlpi.cc	1.37	09/02/18 SMI"

//
// The raw dlpi (heartbeat) transport
//
// This file implements the raw dlpi transport used for exchanging heartbeat
// messages. Please read comments in raw_dlpi.h for details.
//

#include <sys/types.h>
#include <sys/stream.h>
#include <sys/stropts.h>

#include <sys/errno.h>
#include <sys/ddi.h>
#include <sys/sunddi.h>
#include <sys/dlpi.h>

#include <sys/conf.h>
#include <sys/modctl.h>
#include <sys/utsname.h>
#include <sys/disp.h>
#include <sys/time.h>
#include <sys/file.h>
#include <sys/strsubr.h>
#include <sys/cl_net.h>
#include <orb/ip/vlan.h>
#include <orb/infrastructure/orb_conf.h>
#include <transports/tcp/raw_dlpi.h>
#include <transports/tcp/tcp_transport.h>
#include <transports/tcp/arp_cache_monitor.h>

#ifndef	DL_INTR_MODE_REQ
#define	DL_INTR_MODE_REQ	0x115 /* Request Rx processing in INTR mode */
typedef struct {
	t_uscalar_t	dl_primitive;
	t_uscalar_t	dl_sap;
	t_uscalar_t	dl_imode;	/* intr mode: 0 off  1 on */
} dl_intr_mode_req_t;
#endif

// The debug buffer
#ifdef RAW_DLPI_DBGBUF
dbg_print_buf    raw_dlpi_dbg(16384);
#endif

// Pointer to the raw_dlpi object.
raw_dlpi	*raw_dlpi::the_raw_dlpi_p = NULL;

// SAP to be used for raw dlpi communication. This value is copied
// into the raw_dlpi object when it is constructed at boot time and
// that copy is used for all communication. Hence resetting this value
// once the machine is up has no effect. It is declared a uint_t instead
// of a ushort_t for the ease of modification from /etc/system.
uint_t	raw_dlpi_sap = RAWDLPI_DEFAULT_SAP;

#ifdef DEBUG
//
// If the following flag is set, a node panics whenever it receives an
// ill formed message on the heartbeat channel. Ideally this should
// never happen. However, a particular pattern of ill formed messages
// have been seen occasionally while using sci adapters - bug #4346338
// has more details. This hook has been provided to help debug that
// problem. In the non debug kernel or when this flag is clear, ill
// formed messages are simply dropped.
//
int	raw_dlpi_badmsg_panic	= 0x0;
#endif

//
// _raw_dlpi_init()
//
// Called from cl_dlpitrans' _init() routine when the module is
// loaded. calls raw_dlpi::initialize to initialize the raw_dlpi
// object.
//
void
_raw_dlpi_init(void)
{
	raw_dlpi::initialize();
}

//
// _raw_dlpi_fini()
//
// Called from the cl_dlpitrans' _fini routine.
//
void
_raw_dlpi_fini(void)
{
	raw_dlpi::shutdown();
}

//
// raw_dlpi constructor
//
raw_dlpi::raw_dlpi() :
	sap((ushort_t)raw_dlpi_sap)
{
	RAW_DLPI_DBG(("Raw dlpi (%p) using sap 0x%x\n",
	    the_raw_dlpi_p, (int)sap));
}

//
// raw_dlpi destructor
//
raw_dlpi::~raw_dlpi()
{
	// All adapters must have been removed.
	ASSERT(adapters.empty());
}

//
// raw_dlpi::the()
//
// Returns reference to the raw dlpi object
//
raw_dlpi&
raw_dlpi::the()
{
	return (*the_raw_dlpi_p);
}

//
// raw_dlpi::initialize()
//
// Creates the raw_dlpi object
//
void
raw_dlpi::initialize()
{
	the_raw_dlpi_p = new raw_dlpi;
	RAW_DLPI_DBG(("Raw dlpi initialized (%p)\n", the_raw_dlpi_p));
}

//
// raw_dlpi::shutdown()
//
// Deletes the raw_dlpi object
//
void
raw_dlpi::shutdown()
{
	ASSERT(the_raw_dlpi_p);
	delete the_raw_dlpi_p;
	the_raw_dlpi_p = NULL;
	RAW_DLPI_DBG(("Raw dlpi shutdown\n"));
}

//
// raw_dlpi::add_adapter()
//
// Creates a new raw-dlpi_adapter object based on the specifications and
// adds it to the linked list maintained by the raw dlpi transport.
//
void
raw_dlpi::add_adapter(clconf_adapter_t *ap)
{
	raw_dlpi_adapter *adap;
	char *devname;
	int inst;
	int vlanid;
	int aid;
	in_addr_t ipaddr;

	devname = (char *)clconf_adapter_get_dlpi_device_name(ap);
	inst = clconf_adapter_get_device_instance(ap);
	vlanid = clconf_adapter_get_vlan_id(ap);
	aid = clconf_obj_get_id((clconf_obj_t *)ap);
	ipaddr = os::inet_addr(clconf_obj_get_property((clconf_obj_t *)ap,
	    "ip_address"));
	adap = new raw_dlpi_adapter(devname, inst, vlanid, aid, ipaddr);
	rdlpi_lock.lock();
	adapters.append(adap);
	RAW_DLPI_DBG(("Added adapter (%p) %s%d id %d\n",
	    adap, devname, inst, aid));
	rdlpi_lock.unlock();
}

//
// raw_dlpi::remove_adapter
//
// Removes the adapter identified by the adapter id from the linked
// list maintained by the raw dlpi transport.
//
void
raw_dlpi::remove_adapter(int aid)
{
	raw_dlpi_adapter *adap;

	RAW_DLPI_DBG(("Removing adapter aid %d\n", aid));
	rdlpi_lock.lock();
	IntrList<raw_dlpi_adapter, _SList>::ListIterator iter(adapters);
	while ((adap = iter.get_current()) != NULL) {
		if (adap->get_id() == aid) {
			ASSERT(adapters.exists(adap));
			(void) adapters.erase(adap);
			break;
		}
		iter.advance();
	}
	rdlpi_lock.unlock();
	RAW_DLPI_DBG(("Adapter (%p) %s%d.%d aid %d removed\n",
	    adap, adap->get_device(), adap->get_instance(),
	    adap->get_vlan_id(), aid));
	delete adap;
}

//
// raw_dlpi::get_adapter()
//
// Searches for the raw_dlpi_adapter corresponding to the clconf_adapter_t
// in the linked list maintained by the raw dlpi transport. A pointer
// to the object in the list is returned. If no matches were found, NULL is
// returned.
//
raw_dlpi_adapter *
raw_dlpi::get_adapter(clconf_adapter_t *ap)
{
	raw_dlpi_adapter *adap;
	char *devname;
	int inst;
	int vlanid;

	devname = (char *)clconf_adapter_get_dlpi_device_name(ap);
	inst = clconf_adapter_get_device_instance(ap);
	vlanid = clconf_adapter_get_vlan_id(ap);

	rdlpi_lock.lock();
	IntrList<raw_dlpi_adapter, _SList>::ListIterator iter(adapters);
	while ((adap = iter.get_current()) != NULL) {
		if (strcmp(adap->get_device(), devname) == 0 &&
		    adap->get_instance() == inst &&
		    adap->get_vlan_id() == vlanid) {
			ASSERT(adapters.exists(adap));
			break;
		}
		iter.advance();
	}
	rdlpi_lock.unlock();

	return (adap);
}

//
// raw_dlpi::get_sap()
//
// return the raw dlpi sap
//
ushort_t
raw_dlpi::get_sap()
{
	return (sap);
}

//
// raw_dlpi_adapter constructor
//
raw_dlpi_adapter::raw_dlpi_adapter(char *dev, int inst, int vlanid,
    int id, in_addr_t ipa) :
	_SList::ListElem(this),
	instance(inst),
	vlan_id(vlanid),
	adp_id(id),
	mac_len(0),
	ipaddr(ipa),
	uh(NULL),
	wq(0),
	active_conns(0),
	no_fp(false),
	fast_path_ack_mp(NULL),
	nodata(0),
	badsrc(0),
	noconn(0)
{
	int i;

	device = os::strdup(dev);
	for (i = 0; i <= NODEID_MAX; i++)
		conntab[i] = NULL;
	// Nodeid 0 is invalid. The 0th entry in the conntable is set
	// below to a signature value to recognize the beginning of the
	// conn table in crash dumps.
	conntab[0] = (raw_dlpi_conn *)0xa0b1c2d3;
}

//
// raw_dlpi_adapter destructor
//
raw_dlpi_adapter::~raw_dlpi_adapter()
{
	// Can't delete an adapter that is in use. What this means is that
	// all connections over an adapter must have been closed before it
	// can be deleted. In other words we require that all paths using
	// an adapter must have been removed before the adapter itself can
	// be removed.
	ASSERT(uh == NULL);
	ASSERT(active_conns == 0);
	ASSERT(mac_len == 0);
	delete [] device;
}

//
// raw_dlpi_adapter::get_id()
//
// Returns adapter id
//
int
raw_dlpi_adapter::get_id()
{
	return (adp_id);
}

//
// raw_dlpi_adapter::get_device()
//
// Returns device name
//
const char *
raw_dlpi_adapter::get_device()
{
	return (device);
}

//
// raw_dlpi_adapter::get_instance()
//
// Returns device instance
//
int
raw_dlpi_adapter::get_instance()
{
	return (instance);
}

//
// raw_dlpi_adapter::get_vlan_id()
//
// Returns vlan id
//
int
raw_dlpi_adapter::get_vlan_id()
{
	return (vlan_id);
}

//
// raw_dlpi_adapter::get_wq()
//
// Returns pointer to the cldlpihb module's write queue
//
queue_t *
raw_dlpi_adapter::get_wq()
{
	return (wq);
}

//
// raw_dlpi_adapter::open()
//
// Opens the device associated with the adapter using the raw dlpi
// sap for communication. Pushes the cldlpihb module before binding
// to the sap. The cldlpihd module intercepts all received packets.
//
int
raw_dlpi_adapter::open()
{
	int	error;
	udi_handle_t drv_uh;
	char	module_name[FMNAMESZ + 1];
	raw_dlpi_info_t	rdinfo;
	struct strioctl	iocb;

	adapter_lock.lock();
	if (uh) {
		// Already open, increment active_conns and return
		active_conns++;
		adapter_lock.unlock();
		return (0);
	}

	RAW_DLPI_DBG(("Opening adapter (%p) %s%d.%d\n",
	    this, device, instance, vlan_id));

#if SOL_VERSION >= __s11
	if ((error = udi_open_vop(device,
	    VLAN_PPA(instance, vlan_id), &drv_uh)) != 0) {
#else
	if ((error = udi_open_vop(device, -1, &drv_uh)) != 0) {
#endif
		RAW_DLPI_DBG(("Error in vop open 0x%x\n", error));
		adapter_lock.unlock();
		return (error);
	}

#if SOL_VERSION <= __s10
	// Attach to our instance
	if ((error = dl_attach(drv_uh)) != 0) {
		RAW_DLPI_DBG(("Attach failed error 0x%x\n", error));
		(void) udi_close(drv_uh);
		adapter_lock.unlock();
		return (error);
	}
#endif

	// Pop all autopush modules
	while ((error = udi_ioctl(drv_uh, I_LOOK,
			(intptr_t)module_name)) == 0) {
		RAW_DLPI_DBG(("Device contains module %s\n", module_name));
		if ((error = udi_ioctl(drv_uh, I_POP, (intptr_t)NULL)) != 0) {
			RAW_DLPI_DBG(("Pop failed error 0x%x\n", error));
			(void) udi_close(drv_uh);
			adapter_lock.unlock();
			return (error);
		}
	}

	//
	// Push the clhbsndr module. It is possible that the clhbsndr
	// module was already there (among other modules) on the stream
	// and we popped it off in the preceding step. We chose this
	// implementation to keep the code simple.
	//
	RAW_DLPI_DBG(("Pushing %s on adapter %s%d.%d\n",
	    HBSNDR_STR_MODULE_NAME,  device, instance, vlan_id));

	if ((error = udi_push(drv_uh, HBSNDR_STR_MODULE_NAME)) != 0) {
		RAW_DLPI_DBG(("Push %s failed %d\n",
		    HBSNDR_STR_MODULE_NAME, error));
		(void) udi_close(drv_uh);
		adapter_lock.unlock();
		return (error);
	}

	//
	// We will be doing our own ieee usr priority and mblk b_band management
	// and hence there is no need to send the CLIOCSPRIVBBAND ioctl to
	// clhbsndr.
	//

	// Push the cldlpihb module
	RAW_DLPI_DBG(("Pushing %s on adapter %s%d.%d\n",
	    RAWDLPI_STR_MODULE_NAME,  device, instance, vlan_id));

	if ((error = udi_push(drv_uh, RAWDLPI_STR_MODULE_NAME)) != 0) {
		RAW_DLPI_DBG(("Push %s failed %d\n",
		    RAWDLPI_STR_MODULE_NAME, error));
		(void) udi_close(drv_uh);
		adapter_lock.unlock();
		return (error);
	}

	// Let the cldlpihb module know of our adapter
	rdinfo.adapter = this;
	iocb.ic_cmd = CLIOCSRDINFO;
	iocb.ic_timout = 0;
	iocb.ic_len = (int)sizeof (raw_dlpi_info_t);
	iocb.ic_dp = (char *)&rdinfo;
	if ((error = udi_ioctl(drv_uh, I_STR, (intptr_t)&iocb)) != 0) {
		RAW_DLPI_DBG(("Ioctl %d failed error 0x%x\n",
		    CLIOCSRDINFO, error));
		(void) udi_close(drv_uh);
		adapter_lock.unlock();
		return (error);
	}

	// Bind to the raw dlpi sap
	if ((error = dl_bind(drv_uh)) != 0) {
		RAW_DLPI_DBG(("Bind (sap 0x%x) failed error 0x%x\n",
		    raw_dlpi::the().get_sap(), error));
		(void) udi_close(drv_uh);
		adapter_lock.unlock();
		return (error);
	}

	// Enable interrupt handling of all packets by ce.
	// For performance, ce can be setup to use taskq's
	// on the public network. But all packets
	// on the interconnect should be handled by interrupts to
	// guarantee high priority processing of heartbeats.
	// Note: This ioctl is currently supported by ce only.
	if (os::strcmp(device, "ce") == 0) {
		if ((error = dl_intr_mode(drv_uh)) != 0) {
			RAW_DLPI_DBG(("Enabling interrupt using "
			    "DL_INTR_MODE_REQ for %s failed: error %d\n",
			    device, error));
		}
	}

	// Get the Mac address using dl_info
	if ((error = dl_info(drv_uh)) != 0) {
		RAW_DLPI_DBG(("Could not get Mac address for (sap 0x%x),"
		    " error = 0x%x\n",
		    raw_dlpi::the().get_sap(), error));
		(void) udi_close(drv_uh);
		adapter_lock.unlock();
		return (error);
	}

	// Register the local IP address with the arp cache monitor
	arp_cache_monitor::the().register_ipmac(ipaddr, local_addr, mac_len,
	    true);

	uh = drv_uh;
	wq = WR((strvp2wq(UDI2VP(uh)))->q_next);
	active_conns++;

	RAW_DLPI_DBG(("Adapter (%p) open %s%d.%d successful uh %p wq %p\n",
	    this, device, instance, vlan_id, uh, wq));

	adapter_lock.unlock();

	return (0);
}

//
// raw_dlpi_adapter::shutdown()
//
// Shutdown prevent receives from the specified nodeid
//
void
raw_dlpi_adapter::shutdown(nodeid_t rndid)
{
	conntab[rndid] = NULL;
}

//
// raw_dlpi_adapter::close()
//
// "Close" the adapter from the point of view of a raw_dlpi_conn
// object. Actual device is closed only when all raw_dlpi_conns that
// have the adapter open have made their close calls.
//
void
raw_dlpi_adapter::close()
{
	int i;
	udi_handle_t tuh;

	adapter_lock.lock();
	if (uh == NULL) {
		adapter_lock.unlock();
		return;
	}

	ASSERT(wq);
	ASSERT(active_conns > 0);
	active_conns--;
	if (active_conns) {
		// Other connections are still open, return
		adapter_lock.unlock();
		return;
	}

	// Unregister the local IP address from the arp cache monitor
	arp_cache_monitor::the().unregister_ipmac(ipaddr);

	RAW_DLPI_DBG(("Closing adapter (%p) %s%d.%d\n",
	    this, device, instance, vlan_id));

	//
	// This was the last connection, need to close the device.
	// But before we do that make sure the connections array
	// is clean. Also, the lock must be dropped before the
	// device is closed as the lock is needed by the recv method
	// called by the cldlpihb::rput. Holding on to the lock
	// can cause deadlock as the close operation will want
	// every thread in the stream to leave the stream before
	// proceeding with the close.
	//
	for (i = 1; i <= NODEID_MAX; i++)
		ASSERT(conntab[i] == NULL);

	tuh = uh;
	uh = NULL;
	wq = NULL;
	bzero(local_addr, (size_t)MAX_MAC_ADDR_SIZE);
	mac_len = 0;

	RAW_DLPI_DBG(("Adapter (%p) %s%d.%d closed\n",
	    this, device, instance, vlan_id));

	adapter_lock.unlock();

	(void) udi_close(tuh);
}

//
// raw_dlpi_adapter::store_connp()
//
// Store a pointer to the raw_dlpi_conn object corresponding to the
// specified nodeid. It's an indication that the raw_dlpi_conn is
// ready to accept packets and will stay so until shutdown is called.
//
void
raw_dlpi_adapter::store_connp(nodeid_t rndid, raw_dlpi_conn *connp)
{
	ASSERT(uh);
	ASSERT(wq);
	ASSERT(active_conns > 0);
	ASSERT(conntab[rndid] == NULL);
	conntab[rndid] = connp;
}

//
// raw_dlpi_adapter::retrieve_connp()
//
// Return a pointer to the raw_dlpi_conn structure corresponding to
// the given nodeid. This is called on the receive path to deliver
// a heartbeat. The return value could be NULL if the connection was
// never established or it has since been closed.
//
raw_dlpi_conn *
raw_dlpi_adapter::retrieve_connp(nodeid_t rndid)
{
	return (conntab[rndid]);
}

//
// raw_dlpi_adapter::get_local_addr()
//
// Return a pointer to the local MAC address and the address size. The
// caller must not modify the contents at the returned address. The returned
// address pointer must not be freed. If any passed pointer is NULL, the
// corresponding information is not returned.
//
void
raw_dlpi_adapter::get_local_addr(uchar_t **addrp, size_t *addrlenp)
{
	ASSERT(uh);
	ASSERT(wq);
	ASSERT(mac_len);
	if (addrp)
		*addrp = local_addr;
	if (addrlenp)
		*addrlenp = mac_len;
}

//
// raw_dlpi_adapter::send()
//
// Send a heartbeat message over the adapter.
//
int
raw_dlpi_adapter::send(mblk_t *mp)
{
	ASSERT(uh);
	ASSERT(wq);
	ASSERT(active_conns > 0);
	ASSERT(mac_len);

	// The message is sent directly to the dlpi driver, the cldlpihb
	// module is not involved. Not doing flow control. Heartbeat
	// messages need to be handled with priority.
	putnext(wq, mp);

	return (0);
}

//
// raw_dlpi_adapter::recv()
//
// Forward a received heartbeat message to the appropriate connection. Called
// by cldlpihb module's rput.
//
void
raw_dlpi_adapter::recv(mblk_t *mp)
{
	raw_dlpi_conn		*connp;
	raw_dlpi_hb_hdr_t	hbhdr;

	// Extract the raw dlpi header information in a contiguous
	// area on the stack. Drop the packet if the message is not
	// big enough
	if (mp_to_buf(mp, (char *)&hbhdr, sizeof (raw_dlpi_hb_hdr_t)) <
	    sizeof (raw_dlpi_hb_hdr_t)) {
#ifdef DEBUG
		RAW_DLPI_DBG(("Adapter (%p) %s%d.%d dropping message too"
		    " small (len %d) to be a hb\n", this, device, instance,
		    vlan_id, msgdsize(mp)));
		if (raw_dlpi_badmsg_panic) {
			ASSERT(!"rawdlpistr_rput: msg too small");
		}
#endif
		freemsg(mp);
		nodata++;
		return;
	}

	// Drop if packet is from unknown source
	if (hbhdr.s_ndid < 1 || hbhdr.s_ndid > NODEID_MAX) {
#ifdef DEBUG
		RAW_DLPI_DBG(("Adapter (%p) %s%d.%d dropping (ill formed?)"
		    " message (len %d) from unknown src (%d)\n", this,
		    device, instance, vlan_id, msgdsize(mp), hbhdr.s_ndid));
		if (raw_dlpi_badmsg_panic) {
			ASSERT(!"rawdlpistr_rput: from unknow source");
		}
#endif
		freemsg(mp);
		badsrc++;
		return;
	}

	// Locate the connection based on the nodeid
	connp = retrieve_connp(hbhdr.s_ndid);

	if (connp == NULL) {
		// Either a connection was never opened at this end or
		// has since been closed. Drop the message.
		RAW_DLPI_DBG(("Adapter (%p) %s%d.%d dropping message (len %d)"
		    " from node %d\n", this, device, instance, vlan_id,
		    msgdsize(mp), hbhdr.s_ndid));
		freemsg(mp);
		noconn++;
		return;
	}

	//
	// Check if this is a panic packet. If it is a panic packet
	// call the path manager directly to notify this.
	//
	if (hbhdr.panic == PANIC_SIGNATURE) {
		path_manager::the().node_is_panicking(hbhdr.s_ndid,
		    hbhdr.s_incn);
		RAW_DLPI_DBG(("Adapter (%p) %s%d.%d received panic message from"
		    " node %d\n", this, device, instance, vlan_id,
		    hbhdr.s_ndid));
		freemsg(mp);
		return;
	}

	// Adjust the receive pointer and make the receive callback
	// for the connection. The receive callback will free the
	// message when it is done with it. Adjust message is bound to
	// succeed since mp_to_buf at the beginning of this method
	// succeeded, hence ignoring the return value.
	(void) adjmsg(mp, sizeof (raw_dlpi_hb_hdr_t));
	connp->recv(mp, hbhdr.s_incn);
}

//
// raw_dlpi_adapter::fast_path_probe()
//
// Probes for fast path support. In this succeeds heartbeat messages will
// be sent using the dlpi fast path (only M_DATA), else M_PROTO/M_DATA chain
// will be used. This call is made on behalf of a raw_dlpi_conn. The passed
// parameter is a copy of the unit data request that will be used to send
// heartbeats if the fast path probe failed. This method is responsible for
// freeing this mblk chain.
//
// Upon success a pointer to a mblk that contains the precomputed MAC header
// that need to be stuffed in front of the M_DATA message is returned. Upon
// failure NULL is returned.
//
mblk_t *
raw_dlpi_adapter::fast_path_probe(mblk_t *dlur_mp)
{
	mblk_t *mp, *tmpmp;
	struct  iocblk  *iocp;

	if (no_fp) {
		//
		// An earlier fast path probe has failed. Assume that the
		// device does not support fast path optimization.
		//
		freemsg(dlur_mp);
		return (NULL);
	}

	//
	// Allocate the ioctl mp and append the unit data request mp to the
	// ioctl mp.
	//
	if ((mp = mkiocb(DL_IOC_HDR_INFO)) == NULL) {
		freemsg(dlur_mp);
		return (NULL);
	}
	mp->b_cont = dlur_mp;

	// Set the ioctl data size
	iocp = (struct iocblk *)mp->b_rptr;
	iocp->ioc_count = msgdsize(mp->b_cont);

	//
	// Send the ioctl downstream to the device and wait for the (n)ack.
	// We acquire the adapter lock before sending the ioctl and release
	// it after receiving the (n)ack to make sure that that there is
	// only one DL_IOC_HDR_INFO ioctl pending at any time. This eliminates
	// the need to match ioctls with (n)acks.
	//
	adapter_lock.lock();

	ASSERT(fast_path_ack_mp == NULL);
	putnext(wq, mp);

	//
	// Wait for the reply.
	// The ioctl will go down to the device driver, the driver will
	// process it and send an ack or a nack back up. The (n)ack will
	// be intercepted by the cldlpihb module which will make a call
	// to raw_dlpi_adapter::process_fast_path_ack. We wait here
	// to be woken up by process_fast_path_ack.
	//
	fast_path_lock.lock();
	while (fast_path_ack_mp == NULL) {
		fast_path_cv.wait(&fast_path_lock);
	}
	mp = fast_path_ack_mp;
	fast_path_ack_mp = NULL;
	fast_path_lock.unlock();

	adapter_lock.unlock();

	//
	// At this point the ioctl ack/nack chain is in mp.
	//
	if (mp->b_datap->db_type == M_IOCNAK) {
		// Fast path is not supported
		no_fp = true;
		freemsg(mp);
		return (NULL);
	}

	//
	// It's an ack. First mp is the ioctl mp, next one if the unit data
	// request. The third one is the DLPI header response from the driver.
	//
	ASSERT(mp->b_datap->db_type == M_IOCACK);
	ASSERT(mp->b_cont != NULL && mp->b_cont->b_cont != NULL);

	// Get rid of the ioctl mp
	tmpmp = mp;
	mp = mp->b_cont;
	freeb(tmpmp);
	if (mp == NULL) {
		return (NULL);
	}

	// Get rid of the unit data request mp
	tmpmp = mp;
	mp = mp->b_cont;
	freeb(tmpmp);
	if (mp == NULL) {
		return (NULL);
	}

	//
	// At this point mp should have the desired M_DATA mblk with the
	// DLPI header in it.
	//
	ASSERT(mp->b_datap->db_type == M_DATA);

	return (mp);
}

//
// raw_dlpi_adapter::process_fast_path_ack
//
void
raw_dlpi_adapter::process_fast_path_ack(mblk_t *mp)
{
	fast_path_lock.lock();
	ASSERT(fast_path_ack_mp == NULL);
	fast_path_ack_mp = mp;
	fast_path_cv.signal();
	fast_path_lock.unlock();
}

//
// Attach to our adapter instance
//
// raw_dlpi_adapter::dl_attach
//
int
raw_dlpi_adapter::dl_attach(udi_handle_t drv_uh)
{
	dl_attach_req_t *attach_req;
	dl_error_ack_t *error_ack;
	union DL_primitives *dl_prim;
	mblk_t *mp;
	int error;

	if ((mp = allocb(sizeof (dl_attach_req_t), BPRI_MED)) == NULL) {
		return (ENOSR);
	}
	mp->b_datap->db_type = M_PROTO;
	mp->b_wptr += sizeof (dl_attach_req_t);

	attach_req = (dl_attach_req_t *)mp->b_rptr;
	attach_req->dl_primitive = DL_ATTACH_REQ;
	attach_req->dl_ppa = (t_uscalar_t)VLAN_PPA(instance, vlan_id);

	if ((error = udi_msg(drv_uh, mp, &mp, (timespec_t *)NULL)) != 0) {
		return (error);
	}

	dl_prim = (union DL_primitives *)mp->b_rptr;
	switch (dl_prim->dl_primitive) {
	case DL_OK_ACK:
		if ((size_t)(mp->b_wptr-mp->b_rptr) < sizeof (dl_ok_ack_t)) {
			RAW_DLPI_DBG(("dl_attach: DL_OK_ACK protocol"
			    "error\n"));
			break;
		}
		if (((dl_ok_ack_t *)dl_prim)->dl_correct_primitive !=
		    DL_ATTACH_REQ) {
			RAW_DLPI_DBG(("dl_attach: DL_OK_ACK rtnd prim %u\n",
			    ((dl_ok_ack_t *)dl_prim)->dl_correct_primitive));
			break;
		}
		freemsg(mp);
		return (0);

	case DL_ERROR_ACK:
		if ((size_t)(mp->b_wptr-mp->b_rptr) < sizeof (dl_error_ack_t)) {
			RAW_DLPI_DBG(("dl_attach: DL_ERROR_ACK protocol"
			    " error\n"));
			break;
		}

		error_ack = (dl_error_ack_t *)dl_prim;
		switch (error_ack->dl_errno) {
		case DL_BADPPA:
			RAW_DLPI_DBG(("dl_attach: DL_ERROR_ACK bad PPA\n"));
			break;

		case DL_ACCESS:
			RAW_DLPI_DBG(("dl_attach: DL_ERROR_ACK access"
			    "error\n"));
			break;
		default:
			RAW_DLPI_DBG(("dl_attach: DLPI error %u\n",
			    error_ack->dl_errno));
			break;
		}
		break;
	default:
		RAW_DLPI_DBG(("dl_attach: bad ACK header %u\n",
		    dl_prim->dl_primitive));
		break;
	}

	freemsg(mp);
	return (-1);
}

//
// raw_dlpi_adapter::dl_bind()
//
// Bind to the raw dlpi sap
//
int
raw_dlpi_adapter::dl_bind(udi_handle_t drv_uh)
{
	union DL_primitives *dl_prim;
	dl_bind_req_t	*bind_req;
	dl_error_ack_t	*error_ack;
	dl_bind_ack_t	*bind_ack;
	mblk_t		*mp;
	ushort_t	sap;
	int	error;

	sap = raw_dlpi::the().get_sap();

	if ((mp = allocb(sizeof (dl_bind_req_t), BPRI_MED)) == NULL) {
		return (ENOSR);
	}
	mp->b_datap->db_type = M_PROTO;

	bind_req = (dl_bind_req_t *)mp->b_wptr;
	mp->b_wptr += sizeof (dl_bind_req_t);
	bind_req->dl_primitive = DL_BIND_REQ;
	bind_req->dl_sap = sap;
	bind_req->dl_max_conind = 0;
	bind_req->dl_service_mode = DL_CLDLS;
	bind_req->dl_conn_mgmt = 0;
	bind_req->dl_xidtest_flg = 0;

	if ((error = udi_msg(drv_uh, mp, &mp, (timespec_t *)NULL)) != 0) {
		return (error);
	}

	dl_prim = (union DL_primitives *)mp->b_rptr;
	switch (dl_prim->dl_primitive) {
	case DL_BIND_ACK:
		if ((size_t)(mp->b_wptr-mp->b_rptr) < sizeof (dl_bind_ack_t)) {
			RAW_DLPI_DBG(("dl_bind: DL_BIND_ACK protocol"
			    " error\n"));
			break;
		}
		bind_ack = (dl_bind_ack_t *)dl_prim;
		if (bind_ack->dl_sap != sap) {
			RAW_DLPI_DBG(("dl_bind: DL_BIND_ACK bad sap %u\n",
			    ((dl_bind_ack_t *)dl_prim)->dl_sap));
			break;
		}

		if (bind_ack->dl_addr_length + bind_ack->dl_addr_offset >
		    (size_t)(mp->b_wptr-mp->b_rptr)) {
			RAW_DLPI_DBG(("dl_bind ack bad len %d\n",
			    bind_ack->dl_addr_length));
			break;
		}

		freemsg(mp);
		return (0);

	case DL_ERROR_ACK:
		if ((size_t)(mp->b_wptr-mp->b_rptr) < sizeof (dl_error_ack_t)) {
			RAW_DLPI_DBG(("dl_bind: DL_ERROR_ACK protocol"
			    " error\n"));
			break;
		}
		error_ack = (dl_error_ack_t *)dl_prim;
		RAW_DLPI_DBG(("dl_bind: DLPI error %u\n", error_ack->dl_errno));
		break;

	default:
		RAW_DLPI_DBG(("dl_bind: bad ACK header %u\n",
		    dl_prim->dl_primitive));
		break;
	}
	freemsg(mp);
	return (-1);
}

//
// raw_dlpi_adapter::dl_intr_mode()
//
// Enable interrupt handling of heartbeats
//
int
raw_dlpi_adapter::dl_intr_mode(udi_handle_t drv_uh)
{
	union DL_primitives *dl_prim;
	dl_intr_mode_req_t *intr_req;
	dl_error_ack_t	*error_ack;
	mblk_t		*mp;
	ushort_t	sap;
	int		error;

	sap = raw_dlpi::the().get_sap();

	if ((mp = allocb(sizeof (dl_intr_mode_req_t), BPRI_MED)) == NULL) {
		return (ENOSR);
	}
	mp->b_datap->db_type = M_PROTO;

	intr_req = (dl_intr_mode_req_t *)mp->b_wptr;
	mp->b_wptr += sizeof (dl_intr_mode_req_t);

	intr_req->dl_primitive = DL_INTR_MODE_REQ;
	intr_req->dl_sap = sap;
	intr_req->dl_imode = 1;

	if ((error = udi_msg(drv_uh, mp, &mp, (timespec_t *)NULL)) != 0) {
		return (error);
	}

	dl_prim = (union DL_primitives *)mp->b_rptr;
	switch (dl_prim->dl_primitive) {
	case DL_OK_ACK:
		RAW_DLPI_DBG(("dl_intr_mode: OK\n"));
		freemsg(mp);
		return (0);

	case DL_ERROR_ACK:
		if ((size_t)MBLKL(mp) < sizeof (dl_error_ack_t)) {
			RAW_DLPI_DBG(("dl_intr_mode: DL_ERROR_ACK protocol"
			    " error\n"));
			break;
		}
		error_ack = (dl_error_ack_t *)dl_prim;
		RAW_DLPI_DBG(("dl_intr_mode: DLPI error %u\n",
		    error_ack->dl_errno));
		break;

	default:
		RAW_DLPI_DBG(("dl_intr_mode: bad ACK header %u\n",
		    dl_prim->dl_primitive));
		break;
	}
	freemsg(mp);
	return (-1);
}


// Get the Mac address
int
raw_dlpi_adapter::dl_info(udi_handle_t iuh)
{
	dl_info_req_t *info_req;
	dl_error_ack_t *error_ack;
	dl_info_ack_t *info_ack;
	union DL_primitives *dl_prim;
	mblk_t *mp;
	int error;
	uchar_t *addrp;

	mac_len = 0;

	if ((mp = allocb(sizeof (dl_info_req_t), BPRI_MED)) == NULL) {
		return (ENOSR);
	}
	mp->b_datap->db_type = M_PROTO;

	info_req = (dl_info_req_t *)mp->b_wptr;
	mp->b_wptr += sizeof (dl_info_req_t);
	info_req->dl_primitive = DL_INFO_REQ;

	if ((error = udi_msg(iuh, mp, &mp, (timestruc_t *)NULL)) != 0) {
		return (error);
	}

	/*
	 * If the response message is not an ack for the DL_INFO_REQ,
	 * wait until the ack arrives. In the meanwhile drop all
	 * incoming messages.
	 */
	do {
		dl_prim = (union DL_primitives *)mp->b_rptr;
		if (mp->b_datap->db_type == M_PROTO ||
		    mp->b_datap->db_type == M_PCPROTO) {
			if (dl_prim->dl_primitive == DL_INFO_ACK ||
			    dl_prim->dl_primitive == DL_ERROR_ACK) {
				/* The response we have been waiting for */
				break;
			}
		}
		/*
		 * Drop this message, and get the next one from the queue
		 */
		RAW_DLPI_DBG(("dl_info: dropped msg db_type %d dl_prim %d\n",
			mp->b_datap->db_type, dl_prim->dl_primitive));
		freemsg(mp);
		mp = NULL;
		error = udi_msg(iuh, NULL, &mp, (timestruc_t *)NULL);
		if (error != 0) {
			return (error);
		}
	} while (true);


	dl_prim = (union DL_primitives *)mp->b_rptr;
	switch (dl_prim->dl_primitive) {
	case DL_INFO_ACK:
		if ((size_t)(mp->b_wptr-mp->b_rptr) < sizeof (dl_info_ack_t)) {
			RAW_DLPI_DBG(("dl_info: DL_INFO_ACK protocol"
			    " error\n"));
			break;
		}
		info_ack = (dl_info_ack_t *)dl_prim;

		if (info_ack->dl_addr_length > MAX_MAC_ADDR_SIZE) {
			RAW_DLPI_DBG(("huge address size %d \n",
			    info_ack->dl_addr_length));
			break;
		}

		if (info_ack->dl_addr_length + info_ack->dl_addr_offset >
		    (size_t)(mp->b_wptr-mp->b_rptr)) {
			RAW_DLPI_DBG(("dl_info ack bad len %d\n",
			    info_ack->dl_addr_length));
			break;
		}

		addrp = mp->b_rptr + info_ack->dl_addr_offset;

		//
		// Subtract the two bytes related to the sap
		// which is also unfortunately sent up.
		//

		mac_len = info_ack->dl_addr_length - sizeof (ushort_t);

		bcopy((uchar_t *)addrp, (uchar_t *)local_addr,
		    (size_t)info_ack->dl_addr_length);

		freemsg(mp);
		return (0);

	case DL_ERROR_ACK:
		if ((size_t)(mp->b_wptr-mp->b_rptr) < sizeof (dl_error_ack_t)) {
			RAW_DLPI_DBG(("dl_info: DL_ERROR_ACK protocol"
			    " error\n"));
			break;
		}
		error_ack = (dl_error_ack_t *)dl_prim;
		RAW_DLPI_DBG(("dl_info: DLPI error %u\n",
		    error_ack->dl_errno));
		break;

	default:
		RAW_DLPI_DBG(("dl_info: bad ACK header %u\n",
		    dl_prim->dl_primitive));
		break;
	}
	freemsg(mp);
	return (-1);
}

//
// raw_dlpi_conn constructor
//
raw_dlpi_conn::raw_dlpi_conn(tcp_pathend *pp, size_t len) :
	state(RDCONN_NONE),
	adapter(NULL),
	r_ndid(0),
	r_inc(0),
	l_ndid(0),
	l_inc(0),
	pathendp(pp),
	hblen(len),
	pending_ops(0),
	statsp(NULL),
	acm_reg(false),
	nodata(0),
	badinc(0),
	noconn(0)
{
	bzero(remote_addr, (size_t)MAX_MAC_ADDR_SIZE);
	for (int i = 0; i < NUM_8021D_UPRI; i++)
		send_mp_template[i] = NULL;
#ifdef RAW_DLPI_TRACE
	statsp = new raw_dlpi_conn_stats((size_t)RAW_DLPI_TRACE_SIZE);
#endif
}

//
// raw dlpi destructor
//
raw_dlpi_conn::~raw_dlpi_conn()
{
	ASSERT(state == RDCONN_CLOSED || state == RDCONN_NONE);
	ASSERT(pending_ops == 0);
	adapter = NULL;
	pathendp = NULL;
	for (int i = 0; i < NUM_8021D_UPRI; i++)
		send_mp_template[i] = NULL;
	state = RDCONN_NONE;
#ifdef RAW_DLPI_TRACE
	if (statsp) {
		delete statsp;
		statsp = NULL;
	}
#endif
}

//
// raw_dlpi_conn::set_adapter()
//
// Provide information to the raw_dlpi_conn object about what adapter,
// it is supposed to be using. Called by tcp_pathend::initiate().
//
void
raw_dlpi_conn::set_adapter(clconf_adapter_t *ap)
{
	rdconn_lock.lock();
	ASSERT(state == RDCONN_NONE || state == RDCONN_CLOSED);
	adapter = raw_dlpi::the().get_adapter(ap);
	ASSERT(adapter);
	rdconn_lock.unlock();
}

//
// raw_dlpi_conn::open()
//
// Open a raw_dlpi_conn. Calls raw_dlpi_adapter::open to open the adapter.
// Called by tcp_pathend::initiate().
// Arguments passed are the local nodeid, current local incarnation number
// and the remote nodeid. The local nodeid and the local incarnation number
// are to be sent with each outgoing message. Each incoming message must
// match the locally stored remote nodeid and remote incarnation number. The
// remote incarnation number will be set later by tcp_pathend::initiate().
//
int
raw_dlpi_conn::open(nodeid_t lndid, incarnation_num linc, nodeid_t rndid)
{
	int ret;

	rdconn_lock.lock();
	if (state == RDCONN_OPEN) {
		// Duplicate open
		rdconn_lock.unlock();
		return (0);
	}
	ASSERT(state == RDCONN_NONE || state == RDCONN_CLOSED);
	ASSERT(adapter);
	RAW_DLPI_DBG(("Connection (%p) node %d adapter (%p) %s%d.%d opening\n",
	    this, rndid, adapter, adapter->get_device(),
	    adapter->get_instance(), adapter->get_vlan_id()));
	ret = adapter->open();
	if (ret == 0) {
		state = RDCONN_OPEN;

		// Store away the remote and the local nodeid and the
		// local incarnation number
		r_ndid = rndid;
		l_ndid = lndid;
		l_inc  = linc;

		RAW_DLPI_DBG(("Connection (%p) open successful\n", this));
	}
	rdconn_lock.unlock();

	return (ret);
}

//
// raw_dlpi_conn::get_local_addr()
//
// Return a pointer to the local MAC address and the address size. Caller
// must not use the pointer to change the MAC address.
// Called by tcp_pathend::initiate() to exhange MAC addresses with its peer.
// If the connection has not been opened, local MAC address will not be
// available and this call will return with error.
//
int
raw_dlpi_conn::get_local_addr(uchar_t **addrp, size_t *addrlenp)
{
	rdconn_lock.lock();
	if (state != RDCONN_OPEN) {
		// We don't yet have the mac address
		rdconn_lock.unlock();
		return (1);
	}
	adapter->get_local_addr(addrp, addrlenp);
	rdconn_lock.unlock();
	return (0);
}

//
// raw_dlpi_conn::set_remote_info()
//
// Set information about the peer raw_dlpi_conn. Remote information includes
// remote MAC address, the address length and the remote incarnation number.
// Caled by tcp_pathend::initiate().
//
void
raw_dlpi_conn::set_remote_info(const uchar_t *addr, size_t addrlen,
				incarnation_num inc)
{
	uchar_t *laddrp;
	size_t laddrlen;

	rdconn_lock.lock();
	ASSERT(state == RDCONN_OPEN);

	// Verify that the local address and the remote address
	// are of the same length.
	adapter->get_local_addr(&laddrp, &laddrlen);
	ASSERT(addrlen == laddrlen);

	// Store away the remote address
	bcopy(addr, remote_addr, addrlen);

	// Store the remote incarnation number
	r_inc = inc;

	rdconn_lock.unlock();
}

//
// raw_dlpi_conn::connect()
//
// Tell the adapter about the open raw_dlpi_conn and create a template
// outgoing heartbeat. After return from this method the raw_dlpi_conn
// will be ready to send/receive heartbeats.
//
void
raw_dlpi_conn::connect(void)
{
	mblk_t	*mp, *fp_mp;
	size_t	mac_len;
	uchar_t *laddr;
	ushort_t sap;
	dl_unitdata_req_t *dl_udata;
	raw_dlpi_hb_hdr_t hbhdr;
	char macbuf[4*MAX_MAC_ADDR_SIZE];

	rdconn_lock.lock();
	ASSERT(state == RDCONN_OPEN);

	// Obtain the local address and the sap
	adapter->get_local_addr(&laddr, &mac_len);
	sap = raw_dlpi::the().get_sap();

	//
	// Now prepare template mblk chains that will be used to prepare
	// heartbeat messages. If the underlying device supports fast path,
	// a fast path template will be create. Else a unit data request
	// template will be created. We create one template for every possible
	// ieee user priority value.
	//
	// To query the adapter whether the underlying device supports DLPI
	// fast path we need to create a unit data request header and send it
	// to the device. If fast path is supported, the device will return
	// the corresponding fast path header.
	//
	// The Unit data request header consists of the unit data request
	// structure followed by the destination MAC address and the SAP.
	//

	for (int up = 0; up < NUM_8021D_UPRI; up++) {
		send_mp_template[up] = os::allocb(
		    (uint_t)(sizeof (dl_unitdata_req_t) + (size_t)mac_len +
		    sizeof (ushort_t)), BPRI_HI, os::SLEEP);
		mp = send_mp_template[up];

		// Stuff the unit data req
		mp->b_datap->db_type = M_PROTO;
		dl_udata = (dl_unitdata_req_t *)mp->b_wptr;
		dl_udata->dl_primitive = DL_UNITDATA_REQ;
		dl_udata->dl_dest_addr_offset =
		    (t_uscalar_t)sizeof (dl_unitdata_req_t);
		dl_udata->dl_priority.dl_min = (int)(MAX_8021D_UPRI - up);
		dl_udata->dl_priority.dl_max = (int)(MAX_8021D_UPRI - up);
		dl_udata->dl_dest_addr_length = (t_uscalar_t)(mac_len +
		    sizeof (ushort_t));
		mp->b_wptr += sizeof (dl_unitdata_req_t);

		// The MAC address and the sap
		bcopy(remote_addr, mp->b_wptr, mac_len);
		mp->b_wptr += mac_len;
		bcopy(&sap, mp->b_wptr, sizeof (ushort_t));
		mp->b_wptr += sizeof (ushort_t);

		//
		// Query the adapter whether fast path is supported. We send a
		// copy of the mblk chain that would have been freed upon return
		// from the fast_path_probe call.
		//
		fp_mp = adapter->fast_path_probe(copymsg(mp));

		if (fp_mp == NULL) {
			//
			// Fast path is not supported. Need to send an
			// M_PROTO/M_DATA chain with unit data request.
			// Allocate the data mblk, leave enough space empty in
			// the front of the mblk for the MAC header that will be
			// added by the dlpi driver.
			//
			mp = send_mp_template[up];
			mp->b_cont = os::allocb((uint_t)(MAX_MAC_HEADER_SIZE +
			    sizeof (hbhdr) + hblen), BPRI_HI, os::SLEEP);
			mp = mp->b_cont;
			mp->b_cont = NULL;
			mp->b_rptr += MAX_MAC_HEADER_SIZE;
			mp->b_wptr = mp->b_rptr;

			//
			// We will be sending unit data requests and the driver
			// should be using the dl_priority to determine the user
			// priority to be stuffed in the packet. To be doubly
			// sure, we set the b_band field of the M_DATA mblk to
			// the appropriate value as well.
			//
			mp->b_band = UPRI_TO_BBAND(up);

			RAW_DLPI_DBG(("Connect: Using non-fast path up %d"
			    " mp %p\n",	up, send_mp_template[up]));
		} else {
			//
			// Fast path is supported and enabled. Send only an
			// M_DATA message. The M_DATA message will contain the
			// DLPI header followed by the actual heartbeat message.
			// The mblk chain returned by the driver in response to
			// the fast path probe already has the M_DATA message
			// with the DLPI header. Allocate a new mblk big enough
			// to hold the DLPI header and the heartbeat message and
			// copy the known DLPI header to it. This will be the
			// new heartbeat template.
			//

			freemsg(send_mp_template[up]);
			send_mp_template[up] = os::allocb(
			    (uint_t)(sizeof (hbhdr) + hblen + msgdsize(fp_mp)),
			    BPRI_HI, os::SLEEP);

			for (mp = fp_mp; mp != NULL; mp = mp->b_cont) {
				bcopy(mp->b_rptr, send_mp_template[up]->b_wptr,
				    (size_t)MBLKL(mp));
				send_mp_template[up]->b_wptr += MBLKL(mp);
			}
			mp = send_mp_template[up];

			//
			// For some reason dl_priority passed in the unitdata
			// request if not taken into account by the vlan driver
			// when computing the fast path header. It relies on the
			// driver to recompute the user priority from the M_DATA
			// mblks when the packets are finally sent. Set the
			// b_band value to the right value if the driver did
			// not set it right in the fast path header reply.
			//
			mp->b_band = fp_mp->b_band ? fp_mp->b_band :
			    UPRI_TO_BBAND(up);

			freemsg(fp_mp);

			RAW_DLPI_DBG(("Connect: Using fast path up %d mp %p"
			    " size %d\n", up, send_mp_template[up],
			    MBLKL(send_mp_template[up])));
		}
		//
		// Prestore the heartbeat message header that will be used to
		// demultiplex at the other end. The actual data will be copied
		// when the actual send call is made. Member "panic" is used
		// to convey that a node is panicking. It is initialised to
		// 0 at this time and will be filled with the value of
		// PANIC_SIGNATURE when sending the panic notification.
		//
		hbhdr.s_ndid  = l_ndid;
		hbhdr.s_incn  = l_inc;
		hbhdr.r_ndid  = r_ndid;
		hbhdr.panic  = 0;
		bcopy(&hbhdr, mp->b_wptr, sizeof (raw_dlpi_hb_hdr_t));
		mp->b_wptr += sizeof (raw_dlpi_hb_hdr_t);
	}

	state = RDCONN_CONNECTED;

	RAW_DLPI_DBG(("Connection (%p) node %d adapter (%p) %s%d.%d"
	    " linc 0x%x rinc 0x%x established\n", this, r_ndid, adapter,
	    adapter->get_device(), adapter->get_instance(),
	    adapter->get_vlan_id(), l_inc, r_inc));
	RAW_DLPI_DBG(("Local MAC address %s\n",
	    tcp_util::mactostr(laddr, mac_len, macbuf)));
	RAW_DLPI_DBG(("Remote MAC address %s\n",
	    tcp_util::mactostr(remote_addr, mac_len, macbuf)));

	// Provide the remote IP and mac address information to the
	// arp cache monitor
	arp_cache_monitor::the().register_ipmac(pathendp->get_remote_ipaddr(),
	    remote_addr, mac_len, false);
	acm_reg = true;

	// Give the connection info to the adapter
	// Do it here after everything is setup to avoid
	// blocking interrupt processing of packets due to
	// various cv_wait()'s done earlier.
	adapter->store_connp(r_ndid, this);
	RAW_DLPI_DBG(("Connection (%p) attached\n", this));

	rdconn_lock.unlock();
}

//
// raw_dlpi_conn::panic_notify()
//
// Use the template heartbeat message ( normal heartbeat send does
// a copy of the template heartbeat message, but here we want to avoid
// doing any new memory allocations ) and use panic member of the
// heartbeat header to denote as a special panic message ( use PANIC_SIGNATURE
// for this purpose ). Message is not sent if the connection is
// not in the CONNECTED state. We do not touch pending_ops to
// avoid grabbing any locks during the panic callback.
//
void
raw_dlpi_conn::panic_notify()
{
	mblk_t *mp, *hbmp;
	raw_dlpi_hb_hdr_t hbhdr;
	if (state == RDCONN_CONNECTED) {
		mp = send_mp_template[SC_HB_USR_PRI];
		// The last mblk in the mblk chain will hold the heartbeat
		for (hbmp = mp; hbmp->b_cont != NULL; hbmp = hbmp->b_cont) {
		}

		// Adjust the write ptr to point to heartbeat header
		hbmp->b_wptr -= sizeof (raw_dlpi_hb_hdr_t);

		// Fill in the header
		hbhdr.s_ndid  = l_ndid;
		hbhdr.s_incn  = l_inc;
		hbhdr.r_ndid  = r_ndid;
		hbhdr.panic  = PANIC_SIGNATURE;
		bcopy(&hbhdr, mp->b_wptr, sizeof (raw_dlpi_hb_hdr_t));
		hbmp->b_wptr += sizeof (raw_dlpi_hb_hdr_t);

		// Set the mblk write pointer to the end of the message
		hbmp->b_wptr += hblen;

		RAW_DLPI_DBG(("panic_notify: Notifying about this node panic to"
		    " node %d adapter %s%d.%d\n", r_ndid, adapter->get_device(),
		    adapter->get_instance(), adapter->get_vlan_id()));
		(void) adapter->send(mp);
	}
}

//
// raw_dlpi_conn::send()
//
// Create a heartbeat message using the contents of the sendbufp and
// hand it over to the adapter for sending. Message is not sent if the
// connection is not in the CONNECTED state. The pending_ops count is
// incremented before the actual send and is decremented once the send
// completes. The increment and the decrement operations are protected
// by the lock but the actual send is performed outside the lock. The
// connection cannot be shutdown/closed as long as the pending_ops is
// nonzero. The lock is not held across the send as the same lock is
// required on the recv path in the interrupt context.
//
void
raw_dlpi_conn::send(const char *sendbufp)
{
	mblk_t *mp;
	mblk_t *hbmp;
	bool connected = false;

	rdconn_lock.lock();
	if (state == RDCONN_CONNECTED) {
		// State is connected, increment the pending operations
		// count.
		pending_ops++;
		connected = true;
	}
	rdconn_lock.unlock();

	// If not connected, do not send.
	if (!connected)
		return;

	// Construct the mblk chain by first duping the template and then
	// copying the heartbeat token into it.
	mp = copymsg(send_mp_template[SC_HB_USR_PRI]);
#ifdef FAULT_TRANSPORT
	{
		void	*fault_argp;
		uint32_t fault_argsize;

		//
		// Simulate an intermittent memory allocation failure
		// This is only a prototype fault. This fault
		// will need to be made more sophisticated once this
		// fault point is added to the automated tests.
		//
		if (fault_triggered(FAULTNUM_HB_INTERMITTENT_FAILURE,
		    &fault_argp, &fault_argsize) &&
		    ((ddi_get_lbolt() + (long)r_ndid) % 97 == 0)) {
			os::printf("Fault 0x%x node %d: intermittent"
			" heartbeat failure\n",
			FAULTNUM_HB_INTERMITTENT_FAILURE, r_ndid);
			freemsg(mp);
			mp = NULL;
		}
	}
#endif
	if (mp == NULL)	{	// Out of memory
		rdconn_lock.lock();
		pending_ops--;
		rdconn_lock.unlock();
		return;
	}

	// The last mblk in the mblk chain will hold the heartbeat
	for (hbmp = mp; hbmp->b_cont != NULL; hbmp = hbmp->b_cont) {
	}

	bcopy(sendbufp, hbmp->b_wptr, hblen);

#ifdef RAW_DLPI_TRACE
	// Log this heartbeat in the send ring buffer
	statsp->store(false, (char *)hbmp->b_wptr,
	    (ushort_t)(adapter->get_wq()->q_next->q_count));
#endif

	// Set the mblk write pointer to the end of the message
	hbmp->b_wptr += hblen;

	// Hand over the message to the adapter for sending.
	// Adapter::send is responsible for freeing the mblk.
	(void) adapter->send(mp);

	rdconn_lock.lock();
	// Decrement the pending operations count. If the count goes down
	// to zero and the connection is waiting for pending operations to
	// get over before it can close (i.e., state is RDCONN_SHUTDOWN),
	// send a signal to the waiting threads.
	pending_ops--;
	if (state == RDCONN_SHUTDOWN && pending_ops == 0) {
		rdconn_cv.broadcast();
	}
	rdconn_lock.unlock();
}

//
// raw_dlpi_conn::recv()
//
// Receive a heartbeat message from the adapter and forward it to the
// appropriate pathend through a call to pathend::pm_recv_internal().
// Message is accepted only if it is from the right incarnation and if
// the connection is in the RDCONN_CONNECTED state. This method is
// called in the network interrupt context.
//
// Pending_ops is incremented before the call to pm_recv_internal and
// is decremented after return from it. This is to prevent the connection
// from getting closed while a receive is going on. The increment and
// decrement operations done under the lock but the call to pm_recv_internal
// is outside the scope of the lock to reduce contetion with
// raw_dlpi_conn::send().
//
void
raw_dlpi_conn::recv(mblk_t *mp, incarnation_num inc)
{
	bool goodhb = false;

	rdconn_lock.lock();
	if (state == RDCONN_CONNECTED && r_inc == inc) {
		// State is connected, increment the pending operations
		// count.
		goodhb = true;
		pending_ops++;
	} else {
		// Gather packet drop statitics
		if (state != RDCONN_CONNECTED)
			noconn++;
		else {
			badinc++;
			RAW_DLPI_DBG(("Conn (%p) dropping message"
			    "(len %d) from node %d bad inc expected 0x%x"
			    "seen 0x%x\n", this, msgdsize(mp), r_ndid,
			    r_inc, inc));
		}
		freemsg(mp);
		mp = NULL;
	}
	rdconn_lock.unlock();

	if (!goodhb) {
		// Either not connected or message from a wrong incarnation
		return;
	}

#ifdef FAULT_TRANSPORT
	{
		void	*fault_argp;
		uint32_t fault_argsize;

		//
		// Simulate an intermittent packet corruption.
		// This is only a prototype fault. This fault
		// will need to be made more sophisticated once this
		// fault point is added to the automated tests.
		//
		if (fault_triggered(FAULTNUM_HB_INTERMITTENT_FAILURE,
		    &fault_argp, &fault_argsize) &&
		    ((ddi_get_lbolt() + (long)r_ndid) % 97 == 0)) {
			os::printf("Fault 0x%x node %d: intermittent"
			    " heartbeat failure\n",
			    FAULTNUM_HB_INTERMITTENT_FAILURE, r_ndid);
			mp->b_wptr = mp->b_rptr;
		}
	}
#endif

	// Check if we have enough data
	if (msgdsize(mp) < hblen) {
#ifdef DEBUG
		RAW_DLPI_DBG(("Conn (%p) dropping message too small (len %d)"
		    " to be a hb\n", this, msgdsize(mp)));
		if (raw_dlpi_badmsg_panic) {
			ASSERT(!"raw_dlpi_conn::recv: msg too small");
		}
#endif
		rdconn_lock.lock();
		pending_ops--;
		rdconn_lock.unlock();
		freemsg(mp);
		nodata++;
		return;
	}

#ifdef RAW_DLPI_TRACE
	statsp->store(true, (char *)mp->b_rptr,
	    (ushort_t)(RD(adapter->get_wq()->q_next)->q_count));
#endif

	// Pass the message to the path_manager through the pathend.
	// pm_recv_internal releases the mblk chain.
	pathendp->pm_recv_internal(mp);

	rdconn_lock.lock();
	// Decrement the pending operations count. If the count goes down
	// to zero and the connection is waiting for pending operations to
	// get over before it can close (i.e., state is RDCONN_SHUTDOWN),
	// send a signal to the waiting threads.
	pending_ops--;
	if (state == RDCONN_SHUTDOWN && pending_ops == 0) {
		rdconn_cv.broadcast();
	}
	rdconn_lock.unlock();
}

//
// raw_dlpi_conn::shutdown()
//
// Set the connection state to RDCONN_SHUTDOWN - no new sends or recvs
// will be initiated, ongoing operations can continue. Also pass this
// information down to our adapter through a call to
// raw_dlpi_adapter::shutdown().
//
void
raw_dlpi_conn::shutdown(void)
{
	rdconn_lock.lock();
	if (state == RDCONN_SHUTDOWN || state == RDCONN_CLOSED) {
		rdconn_lock.unlock();
		return;
	}
	RAW_DLPI_DBG(("Connection (%p) node %d adapter (%p) %s%d.%d shutdown\n",
	    this, r_ndid, adapter, adapter->get_device(),
	    adapter->get_instance(), adapter->get_vlan_id()));
	adapter->shutdown(r_ndid);
	state = RDCONN_SHUTDOWN;

	rdconn_lock.unlock();
}

//
// raw_dlpi_conn::close()
//
// Close a raw_dlpi_conn connection. Must wait for any ongoing send or
// recv operations to complete before proceeding.
//
void
raw_dlpi_conn::close(void)
{
	raw_dlpi_adapter *tadapter;

	rdconn_lock.lock();
	if (state == RDCONN_CLOSED) {
		// Duplicate close, return
		rdconn_lock.unlock();
		return;
	}

	// Shutdown so that no new operations are initiated.
	if (state != RDCONN_SHUTDOWN) {
		RAW_DLPI_DBG(("Connection (%p) node %d adapter (%p) %s%d.%d"
		    " shutdown\n", this, r_ndid, adapter,
		    adapter->get_device(), adapter->get_instance(),
		    adapter->get_vlan_id()));
		adapter->shutdown(r_ndid);
		state = RDCONN_SHUTDOWN;
	}

	// wait for pending operations to finish.
	while (pending_ops > 0) {
		rdconn_cv.wait(&rdconn_lock);
	}

	// If we lost the race to some other thread and the connection has
	// already been closed, return.
	if (state == RDCONN_CLOSED) {
		rdconn_lock.unlock();
		return;
	}

	// Ask the arp cache monitor to stop monitoring this connection
	if (acm_reg) {
		in_addr_t rem_ipaddr = pathendp->get_remote_ipaddr();
		arp_cache_monitor::the().unregister_ipmac(rem_ipaddr);
		acm_reg = false;
	}

	// Data structure cleanup
	bzero(remote_addr, (size_t)MAX_MAC_ADDR_SIZE);
	for (int i = 0; i < NUM_8021D_UPRI; i++) {
		if (send_mp_template[i]) {
			freemsg(send_mp_template[i]);
			send_mp_template[i] = NULL;
		}
	}
	r_ndid = 0;
	state = RDCONN_CLOSED;
	RAW_DLPI_DBG(("Connection (%p) node %d closed\n", this, r_ndid));
	tadapter = adapter;
	rdconn_lock.unlock();

	//
	// Close the adapter from our point of view. We close the adapter
	// after dropping the lock as adapter close might result in the
	// stream being closed (if we are the last connection left). The
	// rdconn_lock is needed by the recv method which is called by
	// cldlpihb::rput. A situation where rput is waiting for the
	// rdlock_conn lock which is held by this close method doing a
	// stream close must be avoided as that could result in a deadlock.
	//
	tadapter->close();
}

//
// raw_dlpi_conn_stats constructor.
//
raw_dlpi_conn_stats::raw_dlpi_conn_stats(size_t bufsize) :
	size(bufsize),
	ri(0),
	rp(NULL),
	si(0),
	sp(NULL)
{
	if (bufsize > 0) {
		rp = new raw_dlpi_perhb_stats[bufsize];
		sp = new raw_dlpi_perhb_stats[bufsize];
	}
}

//
// raw_dlpi_conn_stats destructor
//
raw_dlpi_conn_stats::~raw_dlpi_conn_stats()
{
	if (size > 0) {
		ASSERT(rp);
		delete [] rp;
		ASSERT(sp);
		delete [] sp;
	}
}

//
// raw_dlpi_conn_stats::store
//
// Store information about a particular heartbeat into the appropriate
// ring buffer.
//
void
raw_dlpi_conn_stats::store(bool isrecv, char *hbdata, ushort_t qc)
{
	if (isrecv) {
		rlock.lock();
		ASSERT(rp);
		ASSERT(ri < size);
		rp[ri].store(ri, hbdata, qc);
		ri = (uint_t)((ri + 1) % size);
		rlock.unlock();
	} else {
		slock.lock();
		ASSERT(sp);
		ASSERT(si < size);
		sp[si].store(si, hbdata, qc);
		si = (uint_t)((si + 1) % size);
		slock.unlock();
	}
}

//
// raw_dlpi_perhb_stats constructor
//
raw_dlpi_perhb_stats::raw_dlpi_perhb_stats() :
	q_count(0),
	si(0),
	ts(0),
	hb(0)
{
}

//
// raw_dlpi_perhb_stats destructor
//
raw_dlpi_perhb_stats::~raw_dlpi_perhb_stats()
{
}

//
// raw_dlpi_perhb_stats::store
//
// Store the particulars of one heartbeat
//
void
raw_dlpi_perhb_stats::store(uint_t i, char *hbdata, ushort_t qc)
{
	q_count = qc;
	si = (ushort_t)i;
	bcopy(hbdata, (char *)&hb, sizeof (uint64_t));
	// Timestamp is in lbolts
	ts = (uint32_t)ddi_get_lbolt();
}
