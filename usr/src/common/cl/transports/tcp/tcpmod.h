/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _TCPMOD_H
#define	_TCPMOD_H

#pragma ident	"@(#)tcpmod.h	1.29	08/05/20 SMI"

#include <sys/os.h>
#include <sys/cl_net.h>
#include <orb/msg/orb_msg.h>

//
// tcpmod header
//
// Description:
//	The following structure is the tcpmod header.
//	This header precedes all the messages sent via tcp
//	transport.
//
// 	notify_ptr is a cookie sent to the otherside which returns the
// 	FULL header, not just notify_ptr, without dereferencing it
//
//	We send the ID_node identifying the source in only DEBUG
//	mode as the initial connection setup establishes this and
//	is redundant in each subsequent message (save 8 bytes)
//
// 	pad is used for 32/64-bit safety.
//
//
struct tcpmod_header_t {
#ifdef DEBUG
#define	DEBUG_PATTERN	0x1abcdef1
	uint_t		debug;		// check for known pattern
	ID_node		src;		// source nodeid/incn
#endif
	uint_t		size;		// size of the message being sent
	int		msgt;		// message type
					// orb_msgtype + tcp_transport internal
	orb_seq_t	seq;		// orb sequence number
	void		*notify_ptr;	// cookie used for acks
};

typedef struct tcpmod_rio_header {
	tcpmod_header_t		_header;
	struct rio_cookie	rcookie;
} tcpmod_rio_header_t;

//
// The following structure is used as the argument to the
// connection info ioctl.
// This ioctl is used to inform the tcpmod of each connection
// about the object type, pointer to object, and the connection number.
//
// tcpmod stores this information in its private structure (pointed to
// by q->q_ptr) and uses it to make intelligent decisions about
// delivering messages to higher layers.
//
// conn_type says if this is reply io.
//

// Whether the pointer stored in tcpmod_conn_info is a TCP_PATHEND or
// a TCP_ENDPOINT
enum tcptr_objtype { TCP_PATHEND, TCP_ENDPOINT };

// Identify what kind of connection this is.
// XX This is redundant as tcp_endpoint::is_rio_conn(connection)
// has the same info
enum tcptr_conntype { TCP_NORMAL_CONN, TCP_REPLYIO_CONN	};

struct tcpmod_conn_info {
	void 	*obj;		// We do not use a pad for _LP64 as this
				// structure does not get sent over the wire
	tcptr_objtype	type;	// whether obj is pathend or endpoint
	uint_t	connection;
	tcptr_conntype	conn_type;
};

#endif	/* _TCPMOD_H */
