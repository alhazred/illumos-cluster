/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)raw_dlpi_str.cc	1.14	08/05/20 SMI"

//
// The raw dlpi steams module (cldlpihb).
//
// This module serves as the agent that intercepts heartbeat messages
// arriving over raw dlpi and passes them over to the path manager.
// This module gets pushed over the private interconnect dlpi devices
// which are opened using a private sap (see definition of raw_dlpi_sap
// in raw_dlpi.cc). Note that this module handles only arriving heartbeats.
// Heartbeat sends bypass this module and go directly to the dlpi driver.
// Please refer to raw_dlpi.h and raw_dlpi.cc for more details.
//

#include <sys/types.h>
#include <sys/stream.h>
#include <sys/stropts.h>

#include <sys/errno.h>
#include <sys/ddi.h>
#include <sys/sunddi.h>
#include <sys/dlpi.h>
#include <sys/strsun.h>

#include <sys/conf.h>
#include <sys/modctl.h>
#include <sys/cl_net.h>
#include <orb/infrastructure/orb_conf.h>
#include <transports/tcp/raw_dlpi.h>
#include <transports/tcp/tcp_transport.h>

char _depends_on[] = "misc/cl_runtime misc/cl_load misc/cl_orb "
	"misc/cl_haci misc/cl_quorum misc/cl_comm misc/cl_dlpitrans";

static int rawdlpistr_ropen(queue_t *, dev_t *, int, int, cred_t *);
static int rawdlpistr_rclose(queue_t *, int, cred_t *);
static int rawdlpistr_rput(queue_t *, mblk_t *);
static int rawdlpistr_wput(queue_t *, mblk_t *);
#ifdef RAWDLPI_STR_NEED_SRV
static int rawdlpistr_rsrv(queue_t *);
static int rawdlpistr_wsrv(queue_t *);
#endif

static struct module_info rawdlpistr_info = {
	0x1234,			// module id - random for now
	RAWDLPI_STR_MODULE_NAME, // module name - rawdlpistr
	0,			// min packet size
	INFPSZ,			// max packet size
	64*1024,		// hi-water mark
	16*1024			// lo-water mark
};

static struct qinit rawdlpistr_rint = {
	(int (*)(void))rawdlpistr_rput,	// put procedure
	NULL,				// service procedure
	(int (*)(void))rawdlpistr_ropen,	// open procedure
	(int (*)(void))rawdlpistr_rclose,	// close procedure
	NULL,			// qadmin procedure
	&rawdlpistr_info,	// module info structure
	NULL,			// module statistics structure
	NULL,			// r/w proc.
	NULL,			// info. procedure.
	0			// uio type for struio().
};

static struct qinit rawdlpistr_wint = {
	(int (*)(void))rawdlpistr_wput,	// put procedure
	NULL,			// service procedure
	NULL,			// open procedure
	NULL,			// close procedure
	NULL,			// qadmin procedure
	&rawdlpistr_info,	// module info structure
	NULL,			// module statistics structure
	NULL,			// r/w proc.
	NULL,			// info. procedure.
	0			// uio type for struio().
};

static struct streamtab rawdlpistr_stream = {
	&rawdlpistr_rint,	// read queue init structure
	&rawdlpistr_wint,	// write queue init structure
	NULL,		// mux read init structure
	NULL		// mux write init structure
};

static struct fmodsw rawdlpistr_fsw = {
	RAWDLPI_STR_MODULE_NAME,
	&rawdlpistr_stream,
	D_MP,
};

//
// Module linkage information for the kernel.
//

static struct modlstrmod modl_rawdlpistr = {
	&mod_strmodops, // Type of module.  This one is a Streams module
	"Raw dlpi based HB stream 'cldlpihb'",
	&rawdlpistr_fsw	// Module info
};

static struct modlinkage modlinkage = {
	MODREV_1,
	{ (void *)&modl_rawdlpistr, NULL, NULL, NULL }
};

//
// _init()
//
int
_init(void)
{
	int	error;

	if ((error = mod_install(&modlinkage)) != 0) {
		os::sc_syslog_msg rawdlpistr_syslog(SC_SYSLOG_TCP_TRANSPORT,
		    "Tcp", NULL);
		//
		// SCMSGS
		// @explanation
		// The streams module that intercepts heartbeat messages could
		// not be installed.
		// @user_action
		// Need a user action for this message.
		//
		(void) rawdlpistr_syslog.log(SC_SYSLOG_WARNING, MESSAGE,
		    "modinstall of cldlpihb failed");
		return (error);
	}

	return (error);
}

//
// _info()
//
int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}

//
// _fini()
//
int
_fini(void)
{
	int error;

	error = mod_remove(&modlinkage);

	return (error);
}

//
// Rawdlpistr private data
//
typedef struct rawdlpistr_data_s {
	// Pointer to the adapter over which this module has been pushed.
	// Used to deliver arriving heartbeats to correct "connections".
	raw_dlpi_adapter *adapterp;
} rawdlpistr_data_t;

//
// rawdlpistr_open()
//
// Read side open procedure
//
static int
rawdlpistr_ropen(queue_t *rqp, dev_t *, int, int sflag, cred_t *)
{
	if (rqp->q_ptr) {
		// This instance has already been opened.  Nothing to do.
		// Just return.
		return (0);
	}
	if (sflag != MODOPEN) {
		return (ENXIO);
	}
	// Allocate space for private data
	rqp->q_ptr = kmem_zalloc(sizeof (rawdlpistr_data_t), KM_SLEEP);

	// Turn our queue on ...
	qprocson(rqp);
	return (0);
}

//
// rawdlpistr_close()
//
// Read side close procedure
//
static int
rawdlpistr_rclose(queue_t *rqp, int, cred_t *)
{
	mblk_t *mp;

	// Disable put and service procedures on this queue
	qprocsoff(rqp);

	// Free up any queued messages
	while ((mp = getq((rqp))) != NULL) {
		freemsg(mp);
	}

	// Free private data
	if (rqp->q_ptr) {
		kmem_free(rqp->q_ptr, sizeof (rawdlpistr_data_t));
		rqp->q_ptr = NULL;
	}

	return (0);
}

//
// raw_dlpi_ioctl()
//
// Processes ioctl messages. Only the CLIOCSRDINFO ioctl is processed.
// The CLIOCSRDINFO brings information about the adapter above which
// this module has been pushed.
// Returns true if ioctl was processed, false otherwise.
//
static bool
raw_dlpi_ioctl(queue_t *wqp, mblk_t *mp)
{
	struct iocblk		*iocp;
	raw_dlpi_info_t		*iptr;
	rawdlpistr_data_t	*datap;

	iocp = (struct iocblk *)mp->b_rptr;
	if (iocp->ioc_cmd == CLIOCSRDINFO) {
		if (mp->b_cont == NULL ||
		    msgdsize(mp->b_cont) != sizeof (raw_dlpi_info_t)) {
			miocnak(wqp, mp, 0, EINVAL);
			return (true);
		}
		iptr = (raw_dlpi_info_t *)mp->b_cont->b_rptr;
		if (iptr == NULL) {
			miocnak(wqp, mp, 0, EINVAL);
			return (true);
		}
		datap = (rawdlpistr_data_t *)OTHERQ(wqp)->q_ptr;
		datap->adapterp = iptr->adapter;
		ASSERT(datap->adapterp);
		miocack(wqp, mp, 0, 0);
		return (true);
	} else {
		return (false);
	}
}

//
// rawdlpistr_wput()
//
// Write side put procedure, currently exists only for the ioctl
// message. No data is sent through this routine.
//
static int
rawdlpistr_wput(queue_t *wqp, mblk_t *mp)
{
	if (DB_TYPE(mp) == M_IOCTL) {
		if (raw_dlpi_ioctl(wqp, mp)) {
			return (0);
		}
	}
	//
	// Currently nothing passes through this routine that does not
	// satisfy the DB_TYPE(mp) >= QPCTL condition. Hence, we never
	// do a putq.
	//
	if (DB_TYPE(mp) >= QPCTL)
		putnext(wqp, mp);
	else if (canputnext(wqp))
		putnext(wqp, mp);
	else
		(void) putq(wqp, mp);
	return (0);
}

//
// rawdlpistr_rput()
//
// Receive side put procedure. M_DATA messages are processed in this
// module, rest are sent upstream.
//
static int
rawdlpistr_rput(queue_t *rqp, mblk_t *mp)
{
	rawdlpistr_data_t	*datap;
	bool			is_hb;
	bool			is_fp_ack;

	datap = (rawdlpistr_data_t *)rqp->q_ptr;
	ASSERT(datap);
	if (datap->adapterp == NULL) {
		RAW_DLPI_DBG(("rawdlpistr_rput: no adapter\n"));
		freemsg(mp);
		return (0);
	}

	//
	// First check if it is a data (heartbeat) message. The message
	// format will be different depending on whether fast path is in
	// use or not. In the case of fast path, heartbeat messages will
	// arrive in direct M_DATA mblks. Otherwise they will arrive in
	// M_PROTO/M_DATA DL_UNITDATA_IND mblk chains.
	//
	// Heartbeat messages will be intercepted and passed on for recv
	// processing directly. Other messages will be sent upstream to
	// the streams head.
	//
	is_hb = false;
	is_fp_ack = false;

	if (DB_TYPE(mp) == M_DATA) {
		// Fast path heartbeats
		is_hb = true;
	} else if (DB_TYPE(mp) == M_PROTO) {
		// Non fast path, heartbeats are in DL_UNITDATA_IND
		if (DL_UNITDATA_IND ==
		    ((union DL_primitives *)mp->b_rptr)->dl_primitive) {
			// Heartbeat message
			// Trim off the leading M_PROTO mblk
			mblk_t	*protomp = mp;
			mp = mp->b_cont;
			ASSERT(mp && DB_TYPE(mp) == M_DATA);
			freeb(protomp);

			// Indicate that it is a hb message
			is_hb = true;
		}
	} else if (DB_TYPE(mp) == M_IOCACK || DB_TYPE(mp) == M_IOCNAK) {
		if (((struct iocblk *)mp->b_rptr)->ioc_cmd ==
		    DL_IOC_HDR_INFO) {
			is_fp_ack = true;
		}
	}

	if (is_hb) {
		// Heartbeat message. Locate the adapter and call its recv
		// routine
		datap->adapterp->recv(mp);
	} else if (is_fp_ack) {
		// Send it to the adapters ack processing routine.
		datap->adapterp->process_fast_path_ack(mp);
	} else {
		// Other messages go upstream
		if (DB_TYPE(mp) == M_FLUSH) {
			if (*mp->b_rptr & FLUSHR) {
				if (*mp->b_rptr & FLUSHBAND) {
					flushband(rqp, FLUSHDATA,
					    *(mp->b_rptr + 1));
				} else {
					flushq(rqp, FLUSHDATA);
				}
			}
		}

		//
		// Currently no mblk that does not satisfy the condition
		// DB_TYPE(mp) >= QPCTL reaches this point. Hence we never
		// do a putq.
		//
		if (DB_TYPE(mp) >= QPCTL) {
			putnext(rqp, mp);
		} else if (canputnext(rqp)) {
			putnext(rqp, mp);
		} else {
			(void) putq(rqp, mp);
		}
	}
	return (0);
}

#ifdef RAWDLPI_STR_NEED_SRV
//
// rawdlpistr_wsrv()
//
// Write side service procedure. All queued messages are passed
// down. Strictly this routine is not needed as no message passes
// through this routine at this time.
//
static int
rawdlpistr_wsrv(queue_t *wqp)
{
	mblk_t *mp;

	while ((mp = getq(wqp)) != NULL) {
		if (DB_TYPE(mp) >= QPCTL) {
			putnext(wqp, mp);
			continue;
		}
		if (canputnext(wqp)) {
			putnext(wqp, mp);
		} else {
			(void) putbq(wqp, mp);
			break;
		}
	}
	return (0);
}

//
// rawdlpistr_rsrv()
//
// Receive side service procedure. Heartbeats don't go through this routine.
//
static int
rawdlpistr_rsrv(queue_t *rqp)
{
	mblk_t *mp;

	while ((mp = getq(rqp)) != NULL) {
		if (DB_TYPE(mp) >= QPCTL) {
			putnext(rqp, mp);
			continue;
		}
		if (canputnext(rqp)) {
			putnext(rqp, mp);
		} else {
			(void) putbq(rqp, mp);
			break;
		}
	}
	return (0);
}
#endif
