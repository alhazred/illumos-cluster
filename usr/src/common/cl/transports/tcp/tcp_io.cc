/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)tcp_io.cc	1.36	08/05/20 SMI"

#include <orb/infrastructure/orb_conf.h>
#include <transports/tcp/tcp_transport.h>
#include <sys/ddi.h>
#include <sys/mc_probe.h>
#include <sys/cl_assert.h>
#include <orb/transport/path_manager.h>

// flag for enabling the reply optimization feature
static bool tcp_reply_io_opt = true;

// length above which to enable reply optimization in tcp transport
// XX This needs to be tuned and maybe hardware specific
static uint_t TCP_MIN_RIO_LEN = 8192;


//
// length above which to enable server-pull in tcp transport
// XX This may be harware specific
//
static uint_t TCP_MIN_RWRITE_LEN = 32768;

//
// List of replyio_client_t's active.
// XX Should be in generic code
//
static IntrList<tcp_rio_client, _DList>	tcb_list;
static os::mutex_t	tcb_lock;

extern tcp_transport *the_tcp_transport;

//
// tcp_rio_client object reaper related data.
//
static os::mutex_t reaper_sleep_lock;
static os::condvar_t reaper_sleep_cv;
static os::usec_t reaper_sleep_time =
	(os::usec_t)30 * (MICROSEC / SEC);
static os::usec_t loiter_time =
	(os::usec_t)10 * (MICROSEC / SEC);

#define	SECOND_GENERATION	(2)

void
tcp_rio_client::tcp_rio_client_reaper(void *arg)
{
	tcp_rio_client *tcb;
	os::systime timo;
	uint32_t i;
	os::usec_t my_duetime;

	reaper_sleep_lock.lock();
	ETCP_DBPRINTF(TCPTR_TRACE_RIO_DETAILS,
	    ("Starting tcp_rio_client_reaper for %p\n",
	    arg));
	for (;;) {
		//
		// Determine loiter time for
		// tcp_rio_client objects with
		// deferred processing. This
		// timeout is based on the
		// maximum path timeout in the
		// system plus 10 per cent
		// safety margin.
		//
		// Reaper periodicity is just
		// maximum path timeout.
		//
		my_duetime = (os::usec_t)0;
		for (i = 1; i <= NODEID_MAX; i++) {
			os::usec_t detection_delay =
			    (os::usec_t)path_manager::
			    the().get_max_delay(i);
			if (detection_delay > my_duetime) {
				my_duetime = detection_delay;
			}
		}
		//
		// Timeout values are not yet setup right
		// after modload (start of reaper). So we
		// rather stick with default values.
		//
		if (my_duetime != (os::usec_t)0) {
			reaper_sleep_time =
			    my_duetime *
			    (os::usec_t)(MICROSEC / MILLISEC);
			my_duetime += my_duetime / (os::usec_t)10;
			loiter_time = my_duetime;
		}
		timo.setreltime(reaper_sleep_time);
		(void) reaper_sleep_cv.timedwait(
		    &reaper_sleep_lock,
		    &timo);
		//
		// Walk list of tcp_rio_clients and check
		// for elements with deferred processing
		// and due time expired.
		//
		tcb_lock.lock();
		tcb_list.atfirst();
		while ((tcb = tcb_list.get_current()) != NULL) {
			tcb_list.advance();
			if (tcb->is_deferred() &&
			    tcb->duetime.is_past()) {
				os::sc_syslog_msg tcp_msg(
				    SC_SYSLOG_TCP_TRANSPORT,
				    "Tcp", NULL);
				//
				// SCMSGS
				// @explanation
				// The replyio subsystem starts deferred
				// processing of a tcp_rio_client object.
				// @user_action
				// This message is for diagnostic purposes
				// only; no action required.
				//
				(void) tcp_msg.log(
				    SC_SYSLOG_NOTICE, MESSAGE,
				    "Reaping tcp_rio_client %p [%x]\n",
				    tcb, tcb->reply_generation);
				//
				// Unlock tcb_list to honour locking
				// protocol of tcp_rio_client destructor.
				// Reacquire tcb_lock and rewind iterator
				// after rele();
				//
				tcb_lock.unlock();
				tcb->rele();
				tcb_lock.lock();
				tcb_list.atfirst();
			}
		}
		tcb_lock.unlock();
	}
}

//
// This is called by tcpmod whenever data is available
// for a reply io msg. This routine is called in streams context and
// should not block.
//
// returns 0 if unable to store message
// returns 1 if able to process message
//
int
tcp_endpoint::tcp_rio_callback(mblk_t *mp, replyio_info_t *cp)
{
	MC_PROBE_0(tcp_rio_callback, "clustering tcp_transport", "");

	ETCP_DBPRINTF(TCPTR_TRACE_RIO_DETAILS,
		("EP%u:%u:%u rio_callback %p %p %u size %u offset %u\n",
		get_rnode().ndid, local_adapter_id(), cp->connection,
		cp->cookie.cookie, cp->cookie.sendstream_cookie,
		cp->cookie.generation, cp->size, cp->offset));

	// If the endpoint is not yet REGISTERED or has been unregistered
	// then ask tcpmod to retry
	if (!is_endpoint_registered()) {
		return (0);
	}

	// Allocate an rio task to process this
	defer_task *tp = new (os::NO_SLEEP) tcp_rio_task(mp, cp, this);
	if (tp == NULL) {
		// If unable to allocate memory, tcpmod will retry
		return (0);
	}

	// Post the task to the transport's threadpool
	get_transport()->rio_post(tp);
	return (1);
}

tcp_rio_task::tcp_rio_task(mblk_t *bp, replyio_info_t *rp, tcp_endpoint *tep) :
	mp(bp),
	ep(tep)
{
	ASSERT(mp != NULL);
	rio = *rp;	// structure copy
	ASSERT(tep->is_rio_conn(rio.connection));
	ep->hold();	// grab a hold which is released in the destructor

	// Find the replyio client and grab a hold on it
	tcp_rio_client *tcb = (tcp_rio_client *)(rio.cookie.cookie);
	tcb_lock.lock();
	//
	// XX If this check fails, it implies that we are still trying
	// to do replyio to a client Buf that has been deleted.
	// We need to abort the I/O in question.
	//
	if (!tcb_list.exists(tcb)) {
		os::panic("incoming reply io to a buf that has been deleted %p",
		    tcb);
	}
	tcb->hold();	// hold released in destructor
	tcb_lock.unlock();
}

tcp_rio_task::~tcp_rio_task()
{
	tcp_rio_client *tcb = (tcp_rio_client *)(rio.cookie.cookie);
	tcb->rele();	// corresponding to hold in constructor
	ep->rele();	// corresponding to hold in constructor
	if (mp != NULL) {
		freemsg(mp);	// We always freemsg in the task destructor
	}
}

void
tcp_rio_task::execute()
{
	tcp_rio_client *tcb = (tcp_rio_client *)(rio.cookie.cookie);
	tcb->rio_callback(mp, &rio, ep);
	delete this;
}

void
tcp_rio_client::rio_callback(mblk_t *mp, replyio_info_t *cp, tcp_endpoint *ep)
{
	MC_PROBE_0(rio_callback, "clustering tcp_transport", "");

	lock();

	bool newmsg = (cp->offset == 0);
	uint_t offset, msglen;

	ETCP_DBPRINTF(TCPTR_TRACE_RIO_DETAILS,
		("EP%u:%u:%u rio_callback trcp %p ackcp %p gen %u curgen %u\n",
		ep->get_rnode().ndid, ep->local_adapter_id(),
		cp->connection,	cp->cookie.cookie,
		cp->cookie.sendstream_cookie, cp->cookie.generation,
		reply_generation));

	if (cp->cookie.generation > reply_generation) {
		// This message belongs to a newer generation, abort
		// the ongoing rio through a call to iodone and clean
		// up stale rio_client state.
		if (iodone(true, cp->cookie.generation)) {
			// Abort was successful, start a new I/O
			ep->hold();	// grab hold for this endpoint
			ASSERT(tep == NULL);
			tep = ep;
			reply_generation = cp->cookie.generation;
			ASSERT(reply_generation != UINT_MAX); // wraparound
			ack_sendstream_cookie = cp->cookie.sendstream_cookie;
			rio_len = (uint_t)(cp->size);
			conn = cp->connection;
			ASSERT(rio_len != 0);
			rio_len_copied = 0;
			CL_PANIC(!processed_offset0);
		} else {
			// Abort was unsuccessful. It must be the case that
			// another message from our own generation has
			// already aborted the io in question, or a later
			// generation has superceded my generation. Note
			// that the lock can possibly be released in
			// iodone which might lead to this iodone failure.
			//
			// The former case will be handled normally later in
			// this routine - this is not the start of a new
			// generation but a part of the ongoing generation
			// and will be allowed to proceed normally. The latter
			// case will be handled by the next if clause that
			// will send an ack and return.

			ASSERT(cp->cookie.generation <= reply_generation);
		}
	}

	// The above and the below if clauses cannot be combined using
	// an else. The reply_generation could have changed in the call
	// to iodone.

	if (cp->cookie.generation < reply_generation) {
		// If this is an older generation than the I/O in progress
		// neglect this message
		unlock();

		// If we are going to neglect this message, and the
		// endpoint is not down, we should send an ack so the
		// remote side does not wait forever. We need to do this
		// only this is a newmsg so we send the ack only once
		if (newmsg && ep->is_endpoint_registered() &&
		    ep->replyio_acks_used()) {
			ep->tcp_rio_send_ack(cp->cookie.sendstream_cookie,
			    cp->connection);
		}
		return;
	}

	ASSERT(cp->cookie.generation == reply_generation);
	ASSERT(ack_sendstream_cookie == cp->cookie.sendstream_cookie);
	ASSERT(cp->connection == conn);	// part of message arrive on same conn
	ASSERT(tep == ep);

	offset = cp->offset;
	msglen = (uint_t)msgdsize(mp);
	ASSERT(msglen != 0);
	ASSERT((offset + msglen) <= rio_len);
	ETCP_DBPRINTF(TCPTR_TRACE_RIO_DETAILS,
		("EP%u:%u:%u rio: Copying %d for cookie %p at offset %u\n",
		ep->get_rnode().ndid, ep->local_adapter_id(),
		cp->connection, msglen, this, offset));

	// Set flag if processed offset 0, this is used in deciding
	// whether to send acks when aborting copies
	if (newmsg) {
		CL_PANIC(!processed_offset0);
		processed_offset0 = true;
	}

	// Update amount of bytes copied. Note that this update only
	// signifies that a copy thread has been started. See
	// tcp_rio_client::rio_do_copy for when the copy actually
	// gets over.
	ASSERT((rio_len_copied + msglen) <= rio_len);
	rio_len_copied += msglen;

	// Increment the number of threads doing the copy operation,
	// it gets decremented in rio_do_copy. If rio_copies_in_progress
	// comes down to zero after all copies have been started, the
	// copy is over.
	rio_copies_in_progress++;

	// Wakeup any threads that might be waiting for all copies to
	// get started.
	if (rio_len_copied == rio_len) {
		rio_cv.broadcast();
	}

	unlock();

	// copy mblks to buffer and call iodone if all copies
	// are over.
	rio_do_copy(bufptr, offset, msglen, mp);
}

//
// Copy mblk to struct buf and if io is done, send ack.
// The mblks are connected in a list by b_cont
// size bytes are copied from the mblks to the memory represented by bp
// starting at offset.
// Both the struct buf and the mblks are left unmodified
//
void
tcp_rio_client::rio_do_copy(struct buf *bp, uint_t offset,
    uint_t size, mblk_t *mp)
{
	ASSERT(size <= msgdsize(mp));
	ASSERT((offset + size) <= bp->b_bcount);
	if (size != 0) {
		// If already mapped we dont have to re bp_mapin
		if ((bp->b_flags & B_REMAPPED) != 0) {
			(void) mp_to_buf(mp, bp->b_un.b_addr + offset,
			    (size_t)size);
		} else {
			struct buf *cbp;

			// We clone and mapin only the portions of the buf
			// that we are
			// copying in this transfer
			cbp = bioclone(bp, (off_t)offset, (size_t)size,
			    (dev_t)0, (daddr_t)0, NULL, NULL, KM_SLEEP);
			bp_mapin(cbp);

			(void) mp_to_buf(mp, cbp->b_un.b_addr, (size_t)size);

			bp_mapout(cbp);
			biodone(cbp);
			freerbuf(cbp);
		}
	}

	lock();

	ASSERT(rio_copies_in_progress > 0);

	// If all copies have been started and if we are the one who
	// are responsible for copying the last segment, wait
	// until all copy threads have done their job and do the
	// cleanup.
	if (offset + size == rio_len) {
		// Io thread responsible for the last segment.
		// Wait for any copies for previous segments
		// that have not yet started to start and then
		// call iodone() to send ack and do cleanup.
		// Iodone waits until all pending copies are over.
		// The first argument to iodone below should be
		// false as this is not an abort case. Note that
		// at this point reply_generation is guaranteed
		// to be same as the generation we are part of
		// as when we acquired the lock rio_copies_in_progress
		// was > 0. If iodone fails, a higher generation has
		// aborted us and we can not do anything about it -
		// ignore the return value.

		while (rio_len_copied < rio_len) {
			rio_cv.wait(&_lock);
		}
		rio_copies_in_progress--;
		(void) iodone(false, reply_generation);

		rio_cv.broadcast();
	} else {
		// Io thread responsible for a segment other than
		// the last segment. Signal the waiting threads.
		// We signal whenever the number of copies in progress
		// drops down to 0 without checking whether all copy
		// threads have even been started or not. This is
		// to support rio aborts - iodone will be waiting
		// for this signal in those cases.

		rio_copies_in_progress--;
		if (rio_copies_in_progress == 0)
			rio_cv.broadcast();
	}

	unlock();
}


//
// Send the ack after reply io is completely done.
// That is, data has been copied to struct buf.
//
void
tcp_endpoint::tcp_rio_send_ack(void *scookie, uint_t conn)
{
	MC_PROBE_0(tcp_rio_send_ack_start, "clustering tcp_transport", "");

	mblk_t		*mp;
	tcpmod_header_t *thp;
	queue_t		*q;

	ETCP_DBPRINTF(TCPTR_TRACE_RIO_MINIMAL,
		("EP%u:%u:%u Sending rio ack for cookie %p ep %p\n",
		get_rnode().ndid, local_adapter_id(), conn, scookie, this));

	mp = os::allocb((uint_t)sizeof (tcpmod_header_t) + wroff, BPRI_HI,
	    os::SLEEP);
	mp->b_rptr += wroff;
	mp->b_wptr = mp->b_rptr;
	thp = (tcpmod_header_t *)mp->b_wptr;
	thp->msgt = TCP_REPLYIO_ACK;
	thp->size = (uint_t)sizeof (tcpmod_header_t);
	ASSERT(scookie != NULL);
	thp->notify_ptr = scookie;
#ifdef DEBUG
	thp->src = orb_conf::current_id_node();
	thp->debug = DEBUG_PATTERN;
#endif
	mp->b_wptr += sizeof (tcpmod_header_t);

	q = get_queue(conn);
	if (q != NULL) {	// could be NULL if endpoint is unregistered
		putnext(q, mp);
	} else {
		CL_PANIC(!is_endpoint_registered());
		freemsg(mp);
	}
	MC_PROBE_0(tcp_rio_send_ack_end, "clustering tcp_transport", "");
}

// Use to add/remove threads to the rio_threadpool
void
tcp_transport::create_rio_threads(int nthreads)
{
	int n = rio_threadpool.change_num_threads(nthreads);
	if (n != nthreads) {
		os::sc_syslog_msg tcp_msg(SC_SYSLOG_TCP_TRANSPORT, "Tcp", NULL);
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) tcp_msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "tcp_transport could not modify rio_threadpool"
		    "Asked for %d, got only %d\n", nthreads, n);
	}

}

//
// check_reply_buf_support and add_reply_buffer must be consistent.
// If it returns true, it is assumed that add_reply_buffer will
// succeed eventually.
//

bool
tcp_sendstream::check_reply_buf_support(size_t iosize, iotype_t, void *,
    optype_t op_type)
{
	if (((op_type == CL_OP_READ) && (iosize >= TCP_MIN_RIO_LEN)) ||
	    ((op_type == CL_OP_WRITE) && (iosize >= TCP_MIN_RWRITE_LEN))) {
		return (tcp_reply_io_opt);
	} else {
		return (false);
	}
}


//
//
// Buf *
// tcp_sendstream::add_reply_buffer(struct buf *bufp, align_t)
//	Request a reply from the server.
//
// Description:
//	This function is called by large i/o performers.
//	A request is made to the server to send the data.
//
//	The protocol is that the client that wants to add a reply buffer
//	first calls check_reply_buf_support() to see if reply I/O is supported.
//	If reply I/O is supported, and the client calls add_reply_buffer(),
//	it is guaranteed that add_reply_buffer() will eventually succeed.
//
//	Creates a new tcp_rio_client and returns the pointer to
//	it. The tcp_rio_client stores the struct buf pointer.
//	It is also sent as the cookie to the other side.
//
//
replyio_client_t *
tcp_sendstream::add_reply_buffer(struct buf *bufp, align_t)
{
	MC_PROBE_0(tcp_add_reply_buffer_start, "clustering tcp_transport", "");

	tcp_rio_client *rb;

	debug_put_word(M_REPLY);

	rb = new tcp_rio_client(bufp);

	//
	// remote cookie is the pointer to the replyio_client_t created.
	//
	rb->marshal_cookie(MainBuf);

	// Make a note in the sendstream that this is a request for a replyio
	// and hence should be sent on a replyio channel.
	replyio_ss = true;

	MC_PROBE_0(tcp_add_reply_buffer_end, "clustering tcp_transport", "");
	return (rb);
}

replyio_server_t *
tcp_recstream::get_reply_buffer()
{
	debug_get_word(M_REPLY);
	return (new replyio_server_t(MainBuf));
}

void
tcp_recstream::get_reply_buffer_cancel()
{
	debug_get_word(M_REPLY);
	replyio_server_t::unmarshal_cookie_cancel(MainBuf);
}

bool
tcp_sendstream::send_reply_buf(Buf *src, replyio_server_t *dest)
{
	tcpmod_rio_header_t	*thp;
	uint_t	src_size = src->span();
	mblk_t	*mp;
	mblk_t	*bp;
	queue_t	*q;
	Environment *e = get_env();

	//
	// Convert from Buf to mblks fragmenting for mtu_size
	// get_mblk is a destructive operation and src is freed
	//
	MC_PROBE_0(send_reply_buf_toget, "clustering tcp_transport", "");
	bp = src->get_mblk(endp->get_wroff(), endp->get_mtu_size(),
			endp->get_do_esballoc(), e);
	MC_PROBE_0(send_reply_buf_get, "clustering tcp_transport", "");
	if (e->exception()) {
		// If there is any exception during copy, they
		// are non-retryable and an exception is returned
		// to the PXFS client
		e->sys_exception()->completed(CORBA::COMPLETED_NO);
		return (true);
	}
	ASSERT(msgdsize(bp) == src_size);

	// Check if this sendstream already has a preference for an
	// endpoint connection (from a previous send_reply_buf). If
	// not choose a new one. Also store away this new connection
	// for any other following send_reply_buf or a send.

	if (prefer_conn == TCP_INVAL_CONN)
		prefer_conn = endp->choose_conn(TCP_REPLYIO_MSG);

	ETCP_DBPRINTF(TCPTR_TRACE_RIO_MINIMAL,
		("EP%u:%u:%u put_offline_reply for cookie %p ss %p size %u\n",
		get_dest_node().ndid, endp->local_adapter_id(), prefer_conn,
		dest->get_cookie(), this, src_size));

	// Allocate mblk for tcpmod_rio_header
	MC_PROBE_0(send_reply_buf_toalloc, "clustering tcp_transport", "");
	mp = os::allocb((uint_t)sizeof (tcpmod_rio_header_t) +
	    endp->get_wroff(), BPRI_HI, os::SLEEP);
	mp->b_rptr += endp->get_wroff();
	mp->b_wptr = mp->b_rptr;
	MC_PROBE_0(send_reply_buf_alloc, "clustering tcp_transport", "");

	thp = (tcpmod_rio_header_t *)mp->b_wptr;
	thp->_header.msgt = TCP_REPLYIO_MSG;
	thp->_header.size = (uint_t)sizeof (tcpmod_rio_header_t) + src_size;
	thp->_header.notify_ptr = NULL;
#ifdef DEBUG
	thp->_header.src = orb_conf::current_id_node();
	thp->_header.debug = DEBUG_PATTERN;
#endif
	thp->rcookie.cookie = dest->get_cookie();
	thp->rcookie.generation = dest->get_generation();
	thp->rcookie.sendstream_cookie = this;
	mp->b_wptr += sizeof (tcpmod_rio_header_t);

	mp->b_cont = bp;	// link mp to bp

	q = endp->get_queue(prefer_conn);
	if (q != NULL) {	// could be NULL if endpoint is unregistered
		// Increment number of offline pending waiting for replyio acks
		if (endp->replyio_acks_used()) {
			incr_offline_pending();
		}

		MC_PROBE_0(send_reply_buf_toputnext, "clustering tcp_transport",
		    "");
		putnext(q, mp);
		MC_PROBE_0(send_reply_buf_putnext, "clustering tcp_transport",
		    "");
	} else {
		CL_PANIC(!endp->is_endpoint_registered());
		e->system_exception(
		    CORBA::RETRY_NEEDED(0, CORBA::COMPLETED_NO));
		freemsg(mp);
	}

	return (true);
}

// tcp_rio_client inherits from refcnt which starts with a holdcount of 1
tcp_rio_client::tcp_rio_client(struct buf *bufp) :
	replyio_client_t(bufp),
	_DList::ListElem(this),
	duetime((os::usec_t)0),
	rio_len(0),
	rio_len_copied(0),
	rio_copies_in_progress(0),
	conn(0),
	tep(NULL),
	reply_generation(0),
	ack_sendstream_cookie(NULL),
	processed_offset0(false),
	deferred_processing(false)
{
	tcb_lock.lock();
	tcb_list.append(this);
	tcb_lock.unlock();
}


//
// tcp_rio_client::iodone()
//
// cleanup tcp_rio_client data structures and sends rio ack signifying
// either a successful rio or an aborted rio. The first argument indicates
// whether iodone has been called to abort an ongoing rio or to complete
// an rio. The second arument is the calling generation. Only a later
// generation can abort an rio initiated by an earlier generation. An
// rio can be successfully completed by the same generation.
//
// must be idempotent
//
bool
tcp_rio_client::iodone(bool abort, uint_t gen)
{
	ASSERT(lock_held());

	ETCP_DBPRINTF(TCPTR_TRACE_RIO_DETAILS,
		("EP%u:%u:%u iodone(%d) tccp %p ackcp %p rep_gen %u gen %u"
		" len %u copied %u\n", tep ? tep->get_rnode().ndid : 0,
		tep ? tep->local_adapter_id() : 0, conn,
		abort ? 1 : 0, this, ack_sendstream_cookie,
		reply_generation, gen, rio_len, rio_len_copied));

	//
	// Now wait until all copies that have already been started to get
	// over.

	while (rio_copies_in_progress > 0) {
		rio_cv.wait(&_lock);
	}

	if (abort) {
		// This iodone has been called to abort an ongoing rio.
		// The generation that is attempting to abort the
		// ongoing io must be larger than the current generation
		// to proceed with the abort.

		if (gen <= reply_generation) {
			// The io we are trying to abort has already been
			// aborted by either another message from our own
			// generation or by a message from a later
			// generation. No action is required.

			return (false);
		}
	} else {
		// This io call is to complete the current rio. The
		// passed generation must equal the reply generation to
		// allow completion.

		if (gen != reply_generation) {
			ASSERT(gen < reply_generation);
			// We lost the race and some later generation has
			// aborted us. Nothing left for us to do. The later
			// generation will call iodone to complete the rio
			// later. Note that an ack to confirm the abort
			// must have already been sent, hence no need to
			// so send an ack here.

			return (false);
		}
	}

	if (ack_sendstream_cookie != NULL) {
		// We do not have a non-NULL cookie when we
		// have not yet copied any data.
		ASSERT(rio_len_copied > 0);
		// Send ack only if processed offset0.
		// If we are calling iodone earlier, it means we are aborting
		// early. In this case we are guaranteed that the task with
		// offset 0 will arrive and will be rejected based on the
		// generation number and the ack will be sent then.
		if (processed_offset0) {
			ASSERT(rio_len >= rio_len_copied);
			if (tep->replyio_acks_used()) {
				tep->tcp_rio_send_ack(ack_sendstream_cookie,
				    conn);
			}
		}
		tep->rele();
		tep = NULL;
		ASSERT(rio_copies_in_progress == 0);
		ack_sendstream_cookie = NULL;
		processed_offset0 = false;
		conn = 0;
	} else {
		ASSERT(tep == NULL);
		ASSERT(rio_copies_in_progress == 0);
		ASSERT(conn == 0);
		CL_PANIC(!processed_offset0);
	}
	if (abort) {
		rio_len = rio_len_copied = 0;
	}
	return (true);
}

// Release the rio client. The boolean argument indicates whether it is an
// abortive release or a completion release.
void
tcp_rio_client::release()
{
	ETCP_DBPRINTF(TCPTR_TRACE_RIO_DETAILS,
		("EP%u:%u:%u gr(%d) trcp %p ackcp %p rep_gen %u len %u"
		"copied %u\n",	tep ? tep->get_rnode().ndid : 0,
		tep ? tep->local_adapter_id() : 0, conn,
		wait_for_copies ? 1 : 0, this, ack_sendstream_cookie,
		reply_generation, rio_len, rio_len_copied));

	lock();
	if (wait_for_copies) {
		// Wait for all copies to start. Iodone below will wait
		// for all copies to get over.
		while (rio_len == 0 || rio_len_copied < rio_len) {
			rio_cv.wait(&_lock);
		}
	}
	//
	// Now initiate an iodone to prevent any new replyio from happening.
	// We know in the completion case all copies have been started and
	// will finish. Moreover, we know that the relyio client has received
	// the REPLY message and hence retransmissions need not be accepted.
	//
	(void) iodone(true, UINT_MAX);
	//
	// Release object here if we had to deal just with generation one
	// RIO replies. Objects for RIO's with higher generation numbers
	// are released later to allow out-of-sequence RIO segments to
	// trickle in.
	//
	if (reply_generation >= SECOND_GENERATION &&
	    reply_generation < UINT_MAX &&
	    the_tcp_transport->is_reaper_running()) {
		//
		// Log this event since this is an indicator for excessive
		// system load.
		//
		set_deferred();
		duetime.setreltime((os::usec_t)loiter_time *
		    (MICROSEC / MILLISEC));
		os::sc_syslog_msg tcp_msg(SC_SYSLOG_TCP_TRANSPORT, "Tcp", NULL);
		//
		// SCMSGS
		// @explanation
		// The replyio subsystem detected an out-of-sequence arrival
		// of replyio segments.
		// @user_action
		// This message is for diagnostic purposes only; no action
		// required.
		//
		(void) tcp_msg.log(SC_SYSLOG_NOTICE, MESSAGE,
		    "Deferring tcp_rio_client %p with generation %x\n",
		    this, reply_generation);
		reply_generation = UINT_MAX;
		unlock();
	} else {
		reply_generation = UINT_MAX;	// prevent any more ios
		unlock();
		rele();	// corresponds to refcnt initialized to 1 in constructor
	}
}

tcp_rio_client::~tcp_rio_client()
{
	tcb_lock.lock();
	ASSERT(tcb_list.exists(this));
	(void) tcb_list.erase(this);
	tcb_lock.unlock();
	ASSERT(ack_sendstream_cookie == NULL);
	ASSERT(tep == NULL);
}

// If inlines are disabled the methods need to be compiled
// into the library so we include the _in.h files while
// #defining inline to be nothing giving non-inline versions
// of the methods.

#ifdef NOINLINES
#define	inline
#include <transports/tcp/tcp_io_in.h>
#undef inline		// in case code is added below that uses inline
#endif  // NOINLINES
