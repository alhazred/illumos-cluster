/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RSMT_ENDPOINT_H
#define	_RSMT_ENDPOINT_H

#pragma ident	"@(#)rsmt_endpoint.h	1.21	08/05/20 SMI"

#include <orb/transport/transport.h>

#include <transports/rsm/rsmt_pathend.h>
#include <transports/rsm/rsmt_msg.h>
#include <transports/rsm/rsmt_seg_mgr.h>
#include <transports/rsm/rsmt_rio_seg.h>
#include <transports/rsm/rsmt_bio.h>

class rsmt_endpoint : public endpoint {
public:
	friend class rsmt_sendstream;

	rsmt_endpoint(transport *, rsmt_pathend *,
		int local_adapter_id, nodeid_t remote_nodeid,
		int remote_adapter_id, rsm_addr_t remote_addr);
	~rsmt_endpoint();

	// inherited from endpoint that must be overridden

	void add_resources(resources *);
	sendstream *get_sendstream(resources *);
	void initiate();
	void unblock();
	//
	// The push method is called by the endpoint state machine when it
	// enters the E_PUSHING state to push any messages queued with the
	// endpoint object to the ORB before the endpoint object is destroyed.
	// The subsequent cleanup operation is expected to tear down the
	// endpoint object. An implicit assumption is made that once the
	// state machine has returned from push, no more messages can arrive
	// on the endpoint. However, the endpoint state machine provides no
	// primitive to tear down the communication infrastructure before the
	// call to push. Since the communication infrastructure is actually
	// torn down in the subsequent call to cleanup, messages can still
	// arrive after return from push. The RSM transport pushes messages
	// up to the ORB, even in the cleanup state. This will need to be
	// cleaned up once the generic endpoint state machine has been fixed
	// to provide a communication shutdown primitive before the call to
	// push.
	//
	void push();
	void cleanup();

	// end inherited

	rsmt_rio_seg_mgr	*get_rio_seg_mgr();

	rsmt_bio_mgr *get_bio_mgrp();

	endpoint_state rep_endpoint_state();

	// marks the pathend as faulted
	void	abort_endpoint();

private:
	rsmt_pathend	*rep_rpe;

	nodeid_t	rep_remote_nodeid;
	incarnation_num	rep_remote_incn;

	rsmt_seg_mgr	*rep_segmgrp;
	rsmt_bio_mgr	*bio_mgrp;
	rsmt_rio_seg_mgr *rio_smgrp;

	//
	// Prevent assignment and pass by value
	//
	rsmt_endpoint(const rsmt_endpoint &);
	rsmt_endpoint &operator = (const rsmt_endpoint &);

	// lint info
	rsmt_endpoint();
};

#endif	/* _RSMT_ENDPOINT_H */
