/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RSMT_BIO_SEG_H
#define	_RSMT_BIO_SEG_H

#pragma ident	"@(#)rsmt_bio_seg.h	1.13	08/05/20 SMI"

#include <sys/rsm/rsm_common.h>

#include "rsmt_seg_mgr.h"
#include "rsmt_msg.h"

// forward declarations
class rsmt_endpoint;
class rsmt_bio_mgr;
class rsmt_bio_seggrp;

//
// This file defines the segment groups that are in the bufferio manager
// as well as the send and receive segment classes. A segment group is
// esentially a collections of segment with similar characteristics. Each
// segment group comprises of receive segments that are implemented using
// rsm export segments and a bunch of send segments that are implemented
// using rsm import segments. The send segments in a segment group have a
// 1-to-1 relationship with receive segments on the remote endpoint and
// are connected to the receive segments. Marshalled ORB messages are sent
// by doing rsm_puts on the send segments. The sender of a message also
// sends an interrupt aka control message using cm_send to the remote node
// to indicate that there is a new message in the receive segment. These
// receive segments are subdivided into partitions so that it can
// carry multiple messages at the same time.
//
// The partitions in a segment are managed by using a directory - directory
// entry has the state of the partition. In a send segment there is a
// local directory which reflects the state of a partition. The receive
// segment has the directory which resides at the top of the segment, this
// directory is updated by the send segment via rsm_put to indicate the
// change of state of partitions.
//
// An ORB message can be either one partition or can span two contiguous
// partitions.
//


// BufferIO Directory flags
const uchar_t RSMT_BDFLGS_FIRST = 0x01; // first partition of the message

// State of the partition stored in the directory entries.
// RSMT_PARTITION_FREE: indicates that the partition can be used by the
// sender to send messages
// RSMT_PARTITION_INUSE: indicates that the partition has been reserved
// by the sender
// RSMT_PARTITION_WRDONE: indicates that the data has been transfered
// to the remote node - this state is updated in the local as well as the
// remote directory
// RSMT_PARTITION_ONLOAN: indicates the receiver is processing the message
// RSMT_PARTITION_CONSUMED: indicates the receiver has consumed the partition
// and it can be reused for a new message
const uchar_t RSMT_PARTITION_FREE = 0;
const uchar_t RSMT_PARTITION_INUSE = 1;
const uchar_t RSMT_PARTITION_WRDONE = 2;
const uchar_t RSMT_PARTITION_ONLOAN = 3;
const uchar_t RSMT_PARTITION_CONSUMED = 4;

// Used to reserve a particular send segment in a segment group
const uintptr_t	RSMT_BIO_SIDXRESVD = 1;

//
// Layout of a directory entry - its cacheline sized entry because we
// want the updates to the directory to be atomic. Additionally updates to
// remote directory entry is more efficient if it is cacheline sized, since
// it avoids the read-modify-write protocol for partial cacheline updates.
//
union rsmt_bio_dirent {
	uint64_t		pad[8];
	struct {
		hrtime_t	genrnum; // generation number of the partition
		uchar_t		state;	// partition state see RSMT_PARTITION_*
		uchar_t		flag;	// see RSMT_BDFLGS_*
		uint16_t	next;	// next partition where msg continues
	}		ent;
} rsmt_bio_dirent_t;

const int32_t RSMT_PUTRETRY_BACKOFF = 5; // put attempts before backingoff
const int32_t RSMT_GETRETRY_BACKOFF = 5; // get attempts before backingoff

// rsmt_bio_seg: bufferio generic segment
//
// This class represents a generic bufferio segment that is subdivided
// into partitions. It is inherited by the specific bufferio segment types
// - the receive and send segments. Each bufferio segment has a directory
// which containts state information for each of its partitions.
// Each bufferio segment has a segment-index associated with it, segment-index
// is a unique number within an instance of bufferio, it starts at 0.
// Each partition can be addressed using a partition-index, it starts at 0.
// Thus any partition within bufferio can be uniquely identified using
// 2-tuple <segment-index, partition-index>
//
class rsmt_bio_seg : public refcnt {
public:

	enum { // bufferio segment state
		RSMT_BIO_SEGNEW = 0, // new segment
		RSMT_BIO_SEGINIT, // initialized segment
		RSMT_BIO_SEGDEL // segment needs to be cleaned up
	};

	//
	// The constructor arguments are as follows
	//	seg_index - the segment index, a unique number associate with
	//	every segment in the bufferio component.
	//	num - the number of partitions in segment
	//	seggrp - pointer to the segment group to which this segment
	//	belongs to.
	//	segmgr - pointer to the segment manager
	//
	rsmt_bio_seg(int, int, rsmt_bio_seggrp *, rsmt_seg_mgr *);

	// Destructor
	~rsmt_bio_seg();

	// Do the setup eg. segment operations like connect/create etc. here
	// arg can be either in/out argument depending on how the subclass
	// chooses its implementation to be.
	virtual bool initiate(void *arg) = 0;

	// Cleanup the segment - disconnect/destroy it, called from
	// the rsmt_bio_seggrp::cleanup
	virtual	void cleanup() = 0;

	// This method returns a pointer to the directory that contains
	// information about each of its partition.
	virtual rsmt_bio_dirent	*get_dir() = 0;

	// This method returns a pointer to a directory entry for a given
	// partition-index.
	virtual rsmt_bio_dirent	*get_dirent(int) = 0;

	// Returns the offset in the segment of the directory entry
	// corresponding to the partition index
	off_t	get_diroffset(int);

	// Returns the total number of partitions in the bufferio segment
	int	get_numpartitions();

	// Returns the number of partititions needed to fit in a message
	// of size indicated by the argument
	int	get_numpartitions(size_t);

	// Returns the segment-index of the bufferio segment
	int	get_segindex();

	// Returns the size of the bufferio segment
	size_t	get_size();

	// Returns the size of the partition
	size_t	get_partitionsz();

	// Returns the state of the segment
	int get_state();
protected:
	// protects the bufferio segment fields
	os::mutex_t	s_lock;
	os::condvar_t	s_cv;

	// state of the segment
	int	state;

	// The segment-index of the bufferio segment - this a unique
	// number within an instance of bufferio and starts at 0
	int	seg_index;

	// The size of the total bufferio segment - multiple of PAGESIZE
	size_t	segsize;

	// The number of partitions in this segment
	// Note: this might be more than what is passed into the constructor
	// due to rounding-up of the segment length since it needs to be
	// a multiple of pagesize
	int	num_partitions;

	// size of each partition in this segment group
	size_t	partition_sz;

	// size of the directory rounded up to a cacheline size - this
	// essentially tells where the partitions start from
	size_t	dirsz;

	// A pointer to the segment group that this segment belongs to.
	rsmt_bio_seggrp	*seggrp;

	// A pointer to the segment manager object
	rsmt_seg_mgr *segmgr;
private:
	// Prevent assignments and pass by value
	rsmt_bio_seg(const rsmt_bio_seg&);
	rsmt_bio_seg &operator = (const rsmt_bio_seg&);

	// Pacify lint
	rsmt_bio_seg();
};

//
// rsmt_bio_rseg: bufferio receive segment
//
// This class represents a receive buffer to which the remote node
// writes. The receive buffer is implemented using an rsm export
// segment. The directory resides in the top of the segment and is
// used for bookkeeping. The size of the directory depends on
// the number of the partitions.
//
class rsmt_bio_rseg : public rsmt_bio_seg {
public:
	// Constructor
	//	seg_index - the segment index, a unique number associate with
	//		every segment in the bufferio component.
	//	num - the number of partitions in segment
	//	seggrp - pointer to the segment group to which this segment
	//		belongs to.
	//	segmgr - pointer to the segment manager
	rsmt_bio_rseg(int, int, rsmt_bio_seggrp *, rsmt_seg_mgr *);

	// Destructor
	~rsmt_bio_rseg();

	// Inherited methods

	//
	// This method creates the export rsm segment.
	// The outarg is an OUT argument - it is the connection token
	// corresponding to the export segment that is returned and is
	// sent in a control message to the peer node so that it can
	// connect to it.
	//
	bool initiate(void *outarg);

	// This method destroy the export segment
	void cleanup();

	// Returns a pointer to the directory embedded in the segment
	rsmt_bio_dirent	*get_dir();

	// Argument is a partition-index, it returns a pointer to the
	// directory entry corresponding to the partition-index.
	rsmt_bio_dirent	*get_dirent(int);

	// End inherited methods

	// Argument is a partition-index, it returns a pointer to the
	// start of the partition inside the exported memory corresponding to
	// the partition-index
	void	*partition(int);

	// push messages in the partitions to ORB, this is called
	// from the rsmt_bio_seggrp::push(). Return value is a completion
	// status. False indicates cleanup has started and more messages
	// are possible. Return value is true otherwise.
	bool		push();

	// release partition corresponding to the passed pointer, which
	// points to the data part of the message in the partition
	void	release_partitions(uchar_t *, uint_t);

	// The bio rseg object passes a pointer to the following
	// callback function to rsmt_seg_mgr::destroy_seg.
	//
	static void destroy_seg_done_callback(void *, rsmt_export_segment *);

private:
	// Set the destroy pending flag below to false.
	void    exseg_destroy_complete();

	// a pointer to the page-aligned memory which is exported
	// through the rsm segment
	void		*membuf;

	// pointer/length of the actual buffer that is allocated
	void		*raw_mem;
	size_t		raw_size;

	// export segment object
	rsmt_export_segment	*exseg;

	// If destroy_seg has been called on the exseg but the endpoint segment
	// manager is yet to complete the destroy.
	bool	exseg_destroy_pending;

	// Prevent assignments and pass by value
	rsmt_bio_rseg(const rsmt_bio_rseg&);
	rsmt_bio_rseg &operator = (const rsmt_bio_rseg&);

	// Pacify lint
	rsmt_bio_rseg();
};

//
// rsmt_bio_sseg: bufferio send segment
//
// This class represents a send buffer to which the local endpoint
// writes in order to transfer data. The send buffer is implemented
// using an rsm import segment. The send segment has a local copy of the
// directory which is used for doing the managing the partitions.
// The directory on remote peer resides in the top of the segment
// and is accessed via rsm_put/get operations.
//
class rsmt_bio_sseg : public rsmt_bio_seg {
public:
	// Constructor
	//	seg_index - the segment index, a unique number associate with
	//	every segment in the bufferio component.
	//	num - the number of partitions in segment
	//	seggrp - pointer to the segment group to which this segment
	//	belongs to.
	//	segmgr - pointer to the segment manager
	rsmt_bio_sseg(int, int, rsmt_bio_seggrp *, rsmt_seg_mgr *);

	// Destructor
	~rsmt_bio_sseg();

	// Inherited methods

	// This method does the import connect to the export segment
	// The inarg is an IN argument - it is a connection token
	// that is received from the exporting node in a control message.
	bool initiate(void *inarg);

	// This method does the import disconnect
	void cleanup();

	// Returns a pointer to the local copy of the directory.
	rsmt_bio_dirent	*get_dir();

	// Argument is a partition-index, it returns a pointer to the
	// directory entry in the local copy of the directory corresponding
	// to the partition-index.
	rsmt_bio_dirent	*get_dirent(int);

	// End inherited methods

	// Blocks till either initiate() completes or till timeout occurs
	// Returns false on timeout otherwise returns true
	bool wait_for_initiate();

	// Returns the offset into the import segment corresponding to the
	// partition-index.
	off_t partition(int);

	// Reserves one or more partition for a message of specified size.
	int alloc(size_t);

	// Frees up reserved partitions for a message of specified size
	// starting at the specified partition index
	void free(int, size_t);

	// Does the actual data transfer from rsmt_msg and Buf to the
	// partition index specified. If there is a failure exceptions are
	// stored in the environment and false is returned.
	bool xfer_data(int, rsmt_hdr_t *, Buf *, Environment *);

	// reclaim freed partitions by syncing local directory with the
	// partition-avail segment, returns the number of partitions
	// that were reclaimed.
	int reclaim();
private:
	// checks if the specified partitions have been updated for the
	// specified generation number
	int is_partition_updated(int, int, hrtime_t);

	// invoked when we want to backoff and retry the puts during data
	// transfer. This method returns either due to timeout or due the
	// state of the segment changing.
	void retry_timeout();

	// Local copy of the directory
	rsmt_bio_dirent	*local_dir;
	// measure of fragmentation
	int		frag_index;
	// number of partitions that are free
	int		free_partitions;
	// number of times alloc() failed
	uint32_t	alloc_failures;
	// hint about which partition might be free, used in alloc()
	int		free_pindex;

	// the generation number of the last time an update to the local_dir
	// from partition-avail segment was done
	hrtime_t	prev_genr;

	// save the connection token received in the SEG_CONNECT message
	rsmt_seg_conn_token_t conn_token;

	// import segment
	rsmt_import_segment	*imseg;

	// Prevent assignments and pass by value
	rsmt_bio_sseg(const rsmt_bio_sseg&);
	rsmt_bio_sseg &operator = (const rsmt_bio_sseg&);

	// Pacify lint
	rsmt_bio_sseg();
};

//
// rsmt_seggrp: segment group
//
// Each bufferio object has a set of segment groups. Segment groups
// contains export segments that are used as receive buffers. These
// export segments are partitioned into smaller buffers. The segment
// group also has import segments to which messages to be sent are
// written into using rsm_put. Hence the segment group is a bi-directional
// channel for exchanging messages.
//
class rsmt_bio_seggrp {
public:
	enum {
		// % of total partitions
		RSMT_PARTITION_LOTSFREE = 25
	};

	// Constructor
	// biomgr - pointer to the rsmt_bio_mgr
	// sgrpidx - seggrp number
	// partition_sz - size of each partition,
	// max_rpartitions - max number of partitions that each recv segment in
	// this segment group can have
	// rsindex - starting segment index for recv segments in this group
	// max_rsegs - max number of recv segments in this segment group
	rsmt_bio_seggrp(rsmt_bio_mgr *, int, size_t, int, int, int);

	// Destructor
	~rsmt_bio_seggrp();

	// This method is called during the processing of the PAVAIL_CONNECT
	// control message. The sseg information depends on the remote nodes
	// rseg, this information is carried in the control message.
	// ssindex - starting segment index for send segments in this group
	// max_spartitions - max number of partitions that each send segment in
	// this segment group can have
	// max_ssegs - max number of send segments in this segment group
	void init_ssegs(int, int, int);

	// This is called from the rsmt_bio_mgr::inititate_fini().
	// The segment group is prepopulated here, if the prepopulation
	// fails due to timeout or other reasons failure is returned.
	// initiate() is idempotent.
	bool initiate();

	// This is called when the endpoint is going down. All the
	// segment group resources are freed up at this point. The
	// cleanup_pre_sm call initiates the segment destruction, the
	// the cleanup_post_sm call frees up rest of the resources.
	void cleanup_pre_sm();
	void cleanup_post_sm();

	// Add a new bufferio send segment.
	// Argument num_partitions is the number of partitions desired
	// in the new segment. This call happens when an endpoint needs
	// to add more bufferio segments.
	rsmt_bio_sseg	*add_sseg(int);

	// Remove a bufferio send segment from the segment group
	void remove_sseg(rsmt_bio_sseg *);

	// Get the send segment corresponding to the given segment index,
	// places a hold on the send segment.
	rsmt_bio_sseg	*get_sseg(int);

	// Add a new bufferio receive segment.
	// Argument num_partitions is the number of partitions desired
	// in the new segment. This call happens in response to a
	// add_sseg on the remote end of the endpoint.
	rsmt_bio_rseg	*add_rseg(int, int);

	// Remove a bufferio receive segment from the segment group
	void remove_rseg(rsmt_bio_rseg *);

	// Get the receive segment corresponding to the given segment index,
	// places a hold on the send segment.
	rsmt_bio_rseg	*get_rseg(int);

	// free_resources is called when the endpoint wants to release
	// resources held by this segment group. The segment group does
	// a best effort free up
	void	free_resources();

	//
	// This method reserves partition(s) in the segment group for a
	// message. The size of the message is passed as the argument,
	// a pointer to rsmt_bio_sseg is returned in the second argument
	// and the partition-index of the first reserved partition
	// is returned in the last argument.
	bool reserve_partitions(size_t, rsmt_bio_sseg**, int *);

	// Wait till there are some free partitions available to carry
	// the marshalled message
	void	wait_for_freepartitions();

	// This method is called to push marshalled messages in the
	// partitions of this segment group to the ORB. Return value
	// is a completion status. False indicates cleanup has started
	// and more messages are possible. Return value is true otherwise.
	bool push();

	// Called after the message has been delivered to the ORB
	// and the partition(s) can be freed up and reused. The first
	// argument is the segment-index, the second arg is the
	// partition-index of the first partition and the third is the
	// number of partitions freed
	void	msg_consumed(int, int, int);

	// Reclaim consumed partitions and mark them as free
	void reclaim();

	// This method is called upon receiving the RECLAIM_PRTACK message.
	void reclaim_completed();

	// Returns the size of the partition
	size_t	get_partitionsz();

	// Returns the number of partitions required for given message size
	int	get_numpartitions(size_t);

	// Returns the max number recv segs that this segment group can have
	int get_rs_maxsegs();

	// Returns the maximum number of partitions per recv segment
	int get_rs_maxpartitions();

	// Returns the max number send segs that this segment group can have
	int get_ss_maxsegs();

	// Returns the maximum number of partitions per send segment
	int get_ss_maxpartitions();

	// Updates seggrp's free_partitions counter and wakeup threads
	// waiting for free partitions in this seggrp if signal is true
	// Used by the individual ssegs belonging to this seggrp
	void update_freepartitions(int delta, bool signal);

	// Returns the endpoint object stored in the biomgr
	rsmt_endpoint *get_endpoint();

	// return the generation number of corresponding to the send
	// segment specified by the index from the pavail segment.
	hrtime_t get_pavail_seginfo(int);

	// Return the array of generation numbers corresponding to the
	// partitions of the send segment specified by the index from the
	// pavail segment
	hrtime_t *get_pavail_partitioninfo(int);

private:
	// Pointer to the bufferio manager
	rsmt_bio_mgr	*biomgr;

	// Seggroup number - index of this seggroup in the biomgr seggrp array
	int	seggrp_index;

	// Size of each partition in this segment group
	size_t	partition_sz;

	// Starting segment-index of recv segments belonging to this group
	int	rs_startidx;

	// Maximum number of partitions per recv segments of this segment group
	int	rs_maxpartitions;

	// Maximum number of recv segments this segment group can have
	int	rs_maxsegs;

	// Starting segment-index of send segments belonging to this group
	int	ss_startidx;

	// Maximum number of partitions per send segments of this segment group
	int	ss_maxpartitions;

	// Maximum number of send segments this segment group can have
	int	ss_maxsegs;

	// Total number of partitions free in this segment group that can be
	// used by the sender.
	int	free_partitions;

	// Number of partitions that have been consumed and freed by the
	// receiver since the last update to the avail-segment. This is used
	// by the receiver to batch updates to the avail-segment
	int	consumed_partitions;

	// The threshold when the remote pavail segment is updated for
	// this segment group.
	int	partition_lotsfree;

	// Generation numbers for each receive segment of the last partition
	// that was consumed by the receiver
	hrtime_t *last_freed;

	// Number of elements in the last_freed array - this is essentially
	// rounded up value of rs_maxsegs so that size of last_freed is a
	// multiple of cacheline
	int	last_freed_nument;

	// Protects and serializes access to shadow_pavail
	// and updates to the remote pavail segment.
	// lock ordering -
	//	updavail_lock can be acquired with the sg_lock held
	//	sg_lock cannot be acquired with updavail_lock held
	os::mutex_t	updavail_lock;

	// The local copy of the generation numbers of partitions
	// of this seggrp. shadow_pavail is an array of pointers
	// to array of generation number
	hrtime_t **shadow_pavail;

	// Number of elements in per recv segment generation number array
	// pointed to by the entries in shadow_pavail. This is essentially
	// the rounded up value of rs_maxpartitions so that the size of the
	// generation number array is a multiple of cacheline.
	int	rs_shdw_pavail_nument;

	//
	// Flag indicates a RECLAIM_PRT message has been sent and an ACK
	// is pending
	//
	bool	reclaim_pending;

	// Pointer to the segment manager
	rsmt_seg_mgr	*segmgr;

	// Flag indicates whether the seggroup has been initiated
	bool initiated;

#ifdef	DEBUG
	// Counters - indicating the number of times 1 or 2 partitions were
	// used to carry messages
	uint64_t partitions_per_msg[2];
#endif

	// Locks to protect accesses to data members of the seggrp
	os::mutex_t	sg_lock;
	os::condvar_t	sg_cv;

	// An array of receive segment pointers
	rsmt_bio_rseg	**rsegs;

	// An array of send segments pointers
	rsmt_bio_sseg	**ssegs;

	// Lock that protects access to the send segment array and
	// receive segment array
	// lock ordering -
	//    seglist_lock can be acquired while sg_lock is held.
	//    sg_lock cannot be acquired while seglist_lock is held
	os::rwlock_t	seglist_lock;

	// Prevent assignments and pass by value
	rsmt_bio_seggrp(const rsmt_bio_seggrp&);
	rsmt_bio_seggrp &operator = (const rsmt_bio_seggrp&);

	// Pacify lint
	rsmt_bio_seggrp();

};

#include "rsmt_bio_seg_in.h"

#endif	/* _RSMT_BIO_SEG_H */
