/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RSMT_SEGMENT_H
#define	_RSMT_SEGMENT_H

#pragma ident	"@(#)rsmt_segment.h	1.18	08/05/20 SMI"

#include "rsmt_adapter.h"

//
// rsmt_seg: A RSM Transport Segment
//
// Each RSM Transport segment is "owned" by a transport path. The segment
// resides at the node (the exporter) at one end of the path and is imported
// by the node at the other end of the path. Note that there is exactly one
// importer for each segment. Segments belonging to a particular path are
// uniquely identified by their segment id and their type (export/import).
// The exported and imported segment objects are subclasses of this class.
//

// segment type
//
enum rsmt_seg_type {
		RSMT_SEG_EXPORT = 1,		// export segment
		RSMT_SEG_IMPORT = 2		// import segment
	};
//
// Constant for indicating unlimited segment resource
//
const size_t	RSMT_INFINITE_SEG_SIZE = (size_t)-1;

class rsmt_segment : public _SList::ListElem {
public:
	rsmt_segment(rsmt_adapter *seg_adp, size_t seg_size,
		rsm_memseg_id_t memseg_id, rsm_addr_t seg_remote_addr);

	// Read segment id, type and size
	rsm_memseg_id_t get_memseg_id();
	rsmt_seg_type get_seg_type();
	void set_seg_type(rsmt_seg_type type);
	size_t get_seg_size();

protected:
	size_t seg_size; 		// Size of the segment.
	rsm_memseg_id_t memseg_id; 	// The segment id
	rsm_addr_t seg_remote_addr; 	// Remote RSM address
	rsmt_seg_type seg_type;		// segment type
	rsm_controller_object_t seg_ctrl; // local copy rsmpi controller object

private:
	// Disallow assignments and pass by value
	rsmt_segment(const rsmt_segment&);
	rsmt_segment &operator = (const rsmt_segment&);

	// lint info
	rsmt_segment();
};

//
// Destruction of an RSMT export segment is an asynchronous event. Entities
// wishing to destroy (unpublish, unbind and delete) a segment makes a call
// to the rsmt_seg_mgr::destroy_seg method. This call returns with a guarantee
// that the segment has already been destroyed or will be destroyed sometime
// in the future. If the caller needs to know when the segment actually gets
// destroyed, it must pass a callback to the destroy_seg call. The following
// typedef defines the type of the callback. The callback is called with
// two parameters - (i) a void pointer that the destroy_seg caller passed
// to the call as a parameter, (ii) the pointer to the segment being
// destroyed.
//
class rsmt_export_segment;
typedef void rsmt_seg_destroy_callback_t(void *, rsmt_export_segment *);

//
// rsmt_export_segment: representation of a segment at the exporter
//
class rsmt_export_segment : public rsmt_segment {
public:
	// Create an export segment object by creating and publishing
	// a segment of specified size and optionally
	// bind memory at address mem_addr and of length mem_len
	rsmt_export_segment(rsmt_adapter *, size_t, rsm_memseg_id_t,
		rsm_addr_t, void *, size_t);

	// Export segment - create, publish with option bind and
	// unpublish and destroy
	int export_open(uint_t flags, rsm_resource_callback_t waitflag);
	int export_close();

	// Rebind and Unbind segments
	int bind(void *mem_addr, size_t mem_len,
		rsm_resource_callback_t waitflag);
	int rebind(void *mem_addr, size_t mem_len,
		rsm_resource_callback_t waitflag);
	int unbind();

	//
	// Set the callback that will be called from the segment destructor.
	// The first argument is a pointer to the callback function. The
	// second pointer is the argument that will be passed back to the
	// callback as the first parameter.
	//
	void	set_destroy_callback(rsmt_seg_destroy_callback_t *, void *);

	// return the size of memory bound to this segment
	size_t	get_mem_size();

	// whether the segment can be rebound or not
	bool is_rebindable();

private:
	rsm_memseg_export_handle_t ex_memseg;
	rsm_memory_local_t ex_memory;

	//
	// Any registered callback function that need be called when this
	// segment is destroyed.
	//
	rsmt_seg_destroy_callback_t	*destroy_callback;

	//
	// The argument to be passed to the above callback. The argument is
	// also set by the entity who wished to receive the callback when it
	// registers the callback.
	//
	void				*callback_param;

	//
	// Flag indicates whether this export seg can be rebound. This
	// depends on whether the backing memory passed at the time of
	// creation was large enough to cover the whole segment.
	//
	bool	rebindable;

	// Disallow assignments and pass by value
	rsmt_export_segment(const rsmt_export_segment&);
	rsmt_export_segment &operator = (const rsmt_export_segment&);

	// lint info
	rsmt_export_segment();
};

//
// rsmt_import_segment: representation of a segment at the importer
//
class rsmt_import_segment : public rsmt_segment {
public:
	// Create an import segment object
	rsmt_import_segment(rsmt_adapter *, size_t, rsm_memseg_id_t,
		rsm_addr_t);
	// Import segment - connect and disconnect
	int import_open();
	void import_close();

	// Read and Write operations
	// RSMPI drivers currently ignore the byte_swap parameter
	int get8(off_t offset,  uint8_t *datap, ulong_t rep_cnt);
	int get16(off_t offset, uint16_t *datap, ulong_t rep_cnt);
	int get32(off_t offset, uint32_t *datap, ulong_t rep_cnt);
	int get64(off_t offset, uint64_t *datap, ulong_t rep_cnt);
	int get(off_t offset, void *datap, size_t length);
	int put8(off_t offset,  uint8_t *datap, ulong_t rep_cnt);
	int put16(off_t offset, uint16_t *datap, ulong_t rep_cnt);
	int put32(off_t offset, uint32_t *datap, ulong_t rep_cnt);
	int put64(off_t offset, uint64_t *datap, ulong_t rep_cnt);
	int put(off_t offset, void *datap, size_t length);

	// Memory barrier operations
	int open_barrier(rsm_barrier_t *barrier);
	int order_barrier(rsm_barrier_t *barrier);
	int close_barrier(rsm_barrier_t *barrier);

private:
	rsm_memseg_import_handle_t im_memseg;

	// Disallow assignments and pass by value
	rsmt_import_segment(const rsmt_import_segment&);
	rsmt_import_segment &operator = (const rsmt_import_segment&);

	// lint info
	rsmt_import_segment();
};

#ifndef NOINLINES
#include "rsmt_segment_in.h"
#endif  // _NOINLINES

#endif /* _RSMT_SEGMENT_H */
