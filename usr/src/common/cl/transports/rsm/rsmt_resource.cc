//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rsmt_resource.cc	1.7	08/05/20 SMI"

#include "rsmt_resource.h"

// static members
rsmt_resource *rsmt_resource::the_rsmt_resource = NULL;

//
// Global resource management
rsmt_resource::rsmt_resource()
{
	memresc_maxadapter = 0;
	monitor_subscribed = false;
}

// instantiate the single instance of rsmt_resource
void
rsmt_resource::initialize()
{
	the_rsmt_resource = new rsmt_resource();
}

// cleanup on transport shutdown
void
rsmt_resource::shutdown()
{
	// unsubscribe our memory monitor callback function
	if (monitor_subscribed) {
#ifdef _KERNEL
		monitor::the().unsubscribe(rsmt_mem_monitor);
#endif
		monitor_subscribed = false;
	}
	delete the_rsmt_resource;
	the_rsmt_resource = NULL;
}

// return the rsmt_resource instance pointer
rsmt_resource &
rsmt_resource::the()
{
	return (*the_rsmt_resource);
}

// callback function to the memory monitor
void
rsmt_resource::rsmt_mem_monitor(monitor::system_state_t state)
{
//	ignore the state when the system has plenty of free memeory
	if (state == monitor::MEMORY_PLENTY)
		return;
	the().memory_resource_request(state);
}

// rsmt_resource_client registers for segment resource management support
void
rsmt_resource::register_resource_segment(int adapterid, rsmt_resource_client
	*clientp)
{
	rsmt_segresc_registry *resc_regp;
	rsmt_segresc_registry *rp;

	resc_regp = new rsmt_segresc_registry();
	segresc_cblock.lock();
	// check if the client has already registered
	segresc_cblist[adapterid].atfirst();
	while ((rp = segresc_cblist[adapterid].get_current()) != NULL) {
		segresc_cblist[adapterid].advance();
		if (clientp == rp->get_clientp()) {
			// client has already registered
			delete resc_regp;
			segresc_cblock.unlock();
			return;
		}
	}
	resc_regp->registry_register(clientp);
	segresc_cblist[adapterid].append(resc_regp);
	segresc_cblock.unlock();
}

// rsmt_resource_client unregisters its segment resource management support
void
rsmt_resource::unregister_resource_segment(int adapterid, rsmt_resource_client
	*clientp)
{
	rsmt_segresc_registry *rp;

	segresc_cblock.lock();
	segresc_cblist[adapterid].atfirst();
	while ((rp = segresc_cblist[adapterid].get_current()) != NULL) {
		segresc_cblist[adapterid].advance();
		if (clientp == rp->get_clientp()) {
			(void) segresc_cblist[adapterid].erase(rp);
			segresc_cblock.unlock();
			// call wil block if the resource registery is active
			// on the callback function
			rp->registry_unregister();
			delete rp;
			return;
		}
	}
	segresc_cblock.unlock();
}

// dynamic segment requests from rsmt_resource_client
void
rsmt_resource::segment_resource_request(int adapterid,
	rsmt_resource_client *clientp, size_t sz)
{
	rsmt_segresc_registry *rp;

	segresc_cblock.lock();
	// look for resource clients with the same adapterid
	segresc_cblist[adapterid].atfirst();
	while ((rp = segresc_cblist[adapterid].get_current()) != NULL) {
		segresc_cblist[adapterid].advance();

		// invokes resource clients other than the requesting one
		if (clientp != rp->get_clientp()) {
			rp->registry_segreq(sz);
		}
	}
	segresc_cblock.unlock();
}

// rsmt_resource_client registers for memory resource management support
void
rsmt_resource::register_resource_memory(int adapterid, rsmt_resource_client
	*clientp)
{
	rsmt_memresc_registry *resc_regp;
	rsmt_memresc_registry *rp;

	// allocate a new memory resource registry
	resc_regp = new rsmt_memresc_registry();
	memresc_cblock.lock();
	// check if the client has already registered
	memresc_cblist[adapterid].atfirst();
	while ((rp = memresc_cblist[adapterid].get_current()) != NULL) {
		memresc_cblist[adapterid].advance();
		if (clientp == rp->get_clientp()) {
			// client has already registered
			delete resc_regp;
			memresc_cblock.unlock();
			return;
		}
	}
	// update the memresc_maxadapter so that memory_resource_request
	// method does not need to go through all memresc_cblist's
	// to dispatch resource clients
	if (adapterid > memresc_maxadapter)
		memresc_maxadapter = adapterid;
	resc_regp->registry_register(clientp);
	memresc_cblist[adapterid].append(resc_regp);

	if (!monitor_subscribed) {
#ifdef _KERNEL
		// subscribe a rsmt global memory mgmt function to
		// the memory monitor
		monitor::the().subscribe(rsmt_mem_monitor);
#endif
		monitor_subscribed = true;
	}
	memresc_cblock.unlock();
}

// rsmt_resource_client unregisters for memory resource management support
void
rsmt_resource::unregister_resource_memory(int adapterid, rsmt_resource_client
	*clientp)
{
	rsmt_memresc_registry *rp;

	memresc_cblock.lock();
	memresc_cblist[adapterid].atfirst();
	while ((rp = memresc_cblist[adapterid].get_current()) != NULL) {
		memresc_cblist[adapterid].advance();
		if (clientp == rp->get_clientp()) {
			(void) memresc_cblist[adapterid].erase(rp);
			memresc_cblock.unlock();

			// call will block if the resource registery is active
			// on the callback function
			rp->registry_unregister();
			delete rp;
			return;
		}
	}
	memresc_cblock.unlock();
}

// invoking all resource clients to deallocate some memory based on the needs
// from the system state
void
rsmt_resource::memory_resource_request(monitor::system_state_t state)
{
	rsmt_memresc_registry *rp;
	int adapterid;

	memresc_cblock.lock();
	// calling all memory registries on all adapters
	for (adapterid = 0; adapterid <= memresc_maxadapter; adapterid++) {
		memresc_cblist[adapterid].atfirst();
		while ((rp = memresc_cblist[adapterid].get_current()) != NULL) {
			memresc_cblist[adapterid].advance();
				rp->registry_memreq(state);
		}
	}
	memresc_cblock.unlock();
}

//
// resource registry class constructor
rsmt_resc_registry::rsmt_resc_registry()
{
	resc_clientp = NULL;
	resc_flags = 0;
}

//
// resource registry class destructor
rsmt_resc_registry::~rsmt_resc_registry()
{
	resc_clientp = NULL;
	resc_flags = 0;
}

// retrieve the pointer to the corresponding rsmt_resource_client
rsmt_resource_client *
rsmt_resc_registry::get_clientp()
{
	return (resc_clientp);
}

// registering rsmt_resource_client to registry
void
rsmt_resc_registry::registry_register(rsmt_resource_client *clientp)
{
	resc_clientp = clientp;
}

// unregistering rsmt_resource_client to registry
void
rsmt_resc_registry::registry_unregister()
{
	resc_lock.lock();
	resc_flags |= UNREGISTERING;

	// wait if the registry is active in resource management
	while (resc_flags & ACTIVE_CB) {
		resc_cv.wait(&resc_lock);
	}
	resc_lock.unlock();
	resc_clientp = NULL;
}

// invoke the resource free functions
void
rsmt_resc_registry::execute()
{
	registry_resource_free();
}

//
// segment resource registry
rsmt_segresc_registry::rsmt_segresc_registry() :
	rsmt_resc_registry()
{
	resc_seg_reqsz = 0;
}

// start the defer task to handle the segment request
void
rsmt_segresc_registry::registry_segreq(size_t sz)
{
	resc_lock.lock();
	// check if callback function already activated
	if (resc_flags & ACTIVE_CB) {
		resc_lock.unlock();
		return;
	}
	resc_flags |= ACTIVE_CB;
	resc_seg_reqsz = sz;
	resc_lock.unlock();
	common_threadpool::the().defer_processing(this);
}

// invoke the resource client segment free method
void
rsmt_segresc_registry::registry_resource_free()
{
	resc_lock.lock();
	if ((resc_flags & UNREGISTERING) == 0) {
		resc_lock.unlock();
		resc_clientp->resource_segfree(resc_seg_reqsz);
		resc_lock.lock();
	}
	resc_flags &= ~ACTIVE_CB;
	resc_seg_reqsz = 0;
	if (resc_flags & UNREGISTERING)
		resc_cv.signal();
	resc_lock.unlock();
}

//
// memory resource registry
rsmt_memresc_registry::rsmt_memresc_registry() :
	rsmt_resc_registry()
{
	resc_mem_state = monitor::UNKNOWN;
}

// invoke the resource client memory free method
void
rsmt_memresc_registry::registry_resource_free()
{
	resc_lock.lock();
	if ((resc_flags & UNREGISTERING) == 0) {
		resc_lock.unlock();
		resc_clientp->resource_memfree(resc_mem_state);
		resc_lock.lock();
	}
	resc_flags &= ~ACTIVE_CB;
	resc_mem_state = monitor::UNKNOWN;
	if (resc_flags & UNREGISTERING)
		resc_cv.signal();
	resc_lock.unlock();
}

// start the defer task to handle the segment request
void
rsmt_memresc_registry::registry_memreq(monitor::system_state_t state)
{
	resc_lock.lock();
	// check if callback function already activated
	if (resc_flags & ACTIVE_CB) {
		resc_lock.unlock();
		return;
	}
	resc_flags |= ACTIVE_CB;
	resc_mem_state = state;
	resc_lock.unlock();
	common_threadpool::the().defer_processing(this);
}
