//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma	ident	"@(#)rsmt_bio_jumbo_seg.cc	1.12	08/05/20 SMI"

#include <sys/types.h>
#include <sys/threadpool.h>

#include <sys/rsm/rsm_common.h>

#include "rsmt_bio.h"
#include "rsmt_bio_jumbo_seg.h"
#include "rsmt_streams.h"

//
// Large messages those that cannot be sent using the partitioned segments
// are sent using "jumbo" segments. Jumbo segments are contained in
// jumbo segment groups and the message transfer is done by transferring
// one chunk of the message at a time.
//

//
// Constructor of the rsmt_bio_jumbo_seg
//
rsmt_bio_jumbo_seg::rsmt_bio_jumbo_seg(int sindex, size_t ssize,
    rsmt_bio_jumbo_seggrp *sgrp, rsmt_bio_mgr *bmgr):
	state(RSMT_BIO_JSEGNEW), segindex(sindex), segsize(ssize),
	seggrp(sgrp), biomgr(bmgr)
{
	ASSERT(biomgr);
	segmgr = biomgr->get_segmgr();
}

//
// Destructor
//
rsmt_bio_jumbo_seg::~rsmt_bio_jumbo_seg()
{
	state = 0;
	segindex = 0;
	segsize = 0;
	seggrp = NULL;
	segmgr = NULL;
	biomgr = NULL;
}

//
// Constructor - rsmt_bio_jumbo_rseg represents a jumbo receive segment.
// Receive segments are implemented as RSM export segments.
// sindex: segment index
// ssize: size of the RSM export segment
// sgrp: seggrp that this rseg belongs to
// bmgr: pointer to the bufferio mgr
//
rsmt_bio_jumbo_rseg::rsmt_bio_jumbo_rseg(int sindex, size_t ssize,
    rsmt_bio_jumbo_seggrp *sgrp, rsmt_bio_mgr *bmgr):
	rsmt_bio_jumbo_seg(sindex, ssize, sgrp, bmgr),
	membuf(NULL), raw_mem(NULL), raw_size(0),
	msgbufp(NULL), exseg(NULL)
{
	// zero out the msghdr
	bzero(&msghdr, sizeof (msghdr));

	RSMT_SEG_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_jumbo_rseg(%p) seggrp(%p) created\n", this, sgrp));
}

//
// Destructor
//
rsmt_bio_jumbo_rseg::~rsmt_bio_jumbo_rseg()
{
	RSMT_SEG_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_jumbo_rseg(%p) destructed\n", this));
	ASSERT(!msgbufp);
	msgbufp = NULL; // satisfy lint
	exseg = NULL;
	if (raw_mem) {
		free_chunk_aligned(raw_mem, raw_size);
	}
	raw_mem = NULL;
	raw_size = 0;
	membuf = NULL;
}

//
// Initiate a receive segment - this gets called from
// rsmt_bio_jumbo_seggrp::add_rseg(). This method allocates memory
// and creates an export segment using the allocated memory. It changes
// the state of the receive segment to JSEGINIT. It returns true if
// the operation is successful and the connection token of the export
// segment is returned in outarg, otherwise false is returned and
// outarg is set to RSMT_INVAL_SEG_CONN_TOKEN.
//
bool
rsmt_bio_jumbo_rseg::initiate(rsmt_seg_conn_token_t *conntok)
{
	rsmt_export_segment	*eseg;
	size_t  asize;
	size_t	rsize;
	void	*rmem;
	void	*segbuf;

	RSMT_SEG_DBG(RSMT_DBGL_INFO, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_jumbo_rseg(%p) initiate\n", this));

	*conntok = RSMT_INVAL_SEG_CONN_TOKEN;

	js_lock.lock();

	if (state == RSMT_BIO_JSEGINIT) {
		// return the connection token
		*conntok = segmgr->get_conn_token(exseg);
	} else if (state == RSMT_BIO_JSEGNEW) {
		js_lock.unlock();
		// alloc page aligned memory
		segbuf = alloc_chunk_aligned(segsize, (size_t)PAGESIZE,
		    &asize, &rmem, &rsize);
		ASSERT(segbuf);
		bzero(rmem, rsize);
		RSMT_SEG_DBG(RSMT_DBGL_INFO, RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_jumbo_rseg(%p) buf=%p,sz=0x%lx\n", this, segbuf,
			asize));

		// create the export segment and bind segbuf to it
		eseg = segmgr->create_seg(asize, RSMT_SMEX_CONNECT, segbuf,
		    asize, RSM_RESOURCE_SLEEP);

		if (!eseg) {
			free_chunk_aligned(rmem, rsize);
			RSMT_SEG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
			    ("rsmt_bio_jumbo_rseg(%p) create_seg failed\n",
				this));
			return (false);
		}

		js_lock.lock();
		segsize = asize;
		raw_mem = rmem;
		raw_size = rsize;
		membuf = segbuf;
		exseg = eseg;
		state = RSMT_BIO_JSEGINIT;
		// return the connection token
		*conntok = segmgr->get_conn_token(eseg);
	}
	js_lock.unlock();

	return (true);
}

//
// Cleanup of the receive segment - destroy the export segment.
// The backing store of the export segment is released later
// in the destructor
//
void
rsmt_bio_jumbo_rseg::cleanup()
{
	RSMT_SEG_DBG(RSMT_DBGL_INFO, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_jumbo_rseg(%p) cleanup\n", this));

	js_lock.lock();
	if (exseg) {
		segmgr->destroy_seg(exseg, 0, NULL, NULL);
	}
	exseg = NULL;
	if (msgbufp) {
		msgbufp->rele();
		msgbufp = NULL;
	}
	state = RSMT_BIO_JSEGDEL;
	js_cv.broadcast();
	js_lock.unlock();
}

//
// push() copies message from the jumbo receive segment into a Buf
// object corresponding to the message. After all the message chunks
// have been copied a recstream object is created using the Buf object
// and the message is delivered to ORB.
//
// off: offset in the message that this chunk maps to
// length: length of message data in this chunk
// firstchunk: indicates whether this the firstchunk, firstchunk has
// a message header preceding the data.
//
void
rsmt_bio_jumbo_rseg::push(off_t off, size_t length, bool firstchunk)
{
	rsmt_bio_msg_t chnkack_msg;
	rsmt_bio_jumbo_buf *tmp_msgbufp = NULL;
	Buf *bufp = NULL;
	bool lastchunk = false;
	rsmt_hdr_t tmp_msghdr;

	RSMT_MSG_DBG_D(RSMT_DBGL_INFO, RSMT_DBGF_BUFFERIO,
	    ("jumbo msg(%d) chunk recv off(0x%lx) len(0x%lx) firstchunk(%d)\n",
		segindex, off, length, firstchunk));
	js_lock.lock();
	if ((!membuf) || (state != RSMT_BIO_JSEGINIT)) {
		js_lock.unlock();
		RSMT_MSG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
		    ("jumbo msg(%d) push() membuf NULL or rseg not Inited\n"));
		return;
	}

	if (msgbufp) {
		ASSERT(!firstchunk);
		tmp_msgbufp = msgbufp;
		bufp = msgbufp->get_buf();   // places a hold on msgbufp
	}
	js_lock.unlock();

	if (firstchunk) {
		bcopy((char *)membuf, (char *)&msghdr, sizeof (msghdr));
		ASSERT(!bufp);
		// create a new buffer
		tmp_msgbufp = new rsmt_bio_jumbo_buf(msghdr.rm_total_len);
		ASSERT(tmp_msgbufp);
		ASSERT(off == 0);
		js_lock.lock();
		if (state != RSMT_BIO_JSEGINIT) { // path is going down
			js_lock.unlock();
			tmp_msgbufp->rele();	// delete msgbufp
			RSMT_MSG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
			    ("jumbo msg(%d) push() rseg not Inited\n"));
			return;
		}
		ASSERT(!msgbufp);
		msgbufp = tmp_msgbufp;
		bufp = msgbufp->get_buf(); // places a hold on msgbufp
		js_lock.unlock();
		bcopy((char *)membuf+sizeof (rsmt_padded_hdr),
		    (char *)bufp->head(), length);
	} else {
		ASSERT(bufp);
		bcopy((char *)membuf, (char *)bufp->head()+off, length);
	}
	// advance the tail in buf
	bufp->incspan((int)length);

	// send an ack - the ack is sent before the message is
	// delivered to the ORB because the send semantics for jumbo
	// msgs dictates that an ACK is required for send to complete
	// successfully. If we are unable to send the ACK, we drop
	// the message and the sender will report the failure.
	chnkack_msg.u.bmca_msg.flags = 0;
	chnkack_msg.u.bmca_msg.segindex = (ushort_t)segindex;

	if (bufp->span() == msghdr.rm_total_len) {
		// disassociate the GWBuf from the rsmt_bio_jumbo_buf and
		// release the hold on msgbuf placed during instantiation
		js_lock.lock();
		if (msgbufp) {
			(void) msgbufp->extract_buf();
			msgbufp->rele();
			msgbufp = NULL;
		}
		// make a local copy and before sending MSGCHUNKACK
		// since it msghdr will get reused by the next jumbo msg
		bcopy((char *)&msghdr, (char *)&tmp_msghdr, sizeof (msghdr));
		// zero out the msghdr
		bzero(&msghdr, sizeof (msghdr));
		js_lock.unlock();
		lastchunk = true;
	}

	if (!biomgr->cm_send(&chnkack_msg, sizeof (chnkack_msg),
		RSMT_BIO_MSGCHUNKACK, false)) {
		RSMT_MSG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
		    ("jumbo msg chunk ack(%d) cm_send failed\n",
			segindex));
		// path is going down, rele the hold on msgbufp. cleanup()
		// will also call rele causing the msgbufp to get deleted.
		tmp_msgbufp->rele();
		if (lastchunk) {
			// this message should not to be pushed to ORB
			// since in the absence of the CHUNKACK the sender is
			// going to return with COMPLETED_NO, we need to
			// destroy the GWBuf too.
			bufp->done();
		}
		return;
	}
	RSMT_MSG_DBG_D(RSMT_DBGL_INFO, RSMT_DBGF_BUFFERIO,
	    ("jumbo msg(%d) CHUNKACK sent\n", segindex));

	if (lastchunk) {
		// the entire message has been received push it to ORB

		// release hold placed due to get_buf();
		tmp_msgbufp->rele();

		rsmt_recstream *rs = new rsmt_recstream(&tmp_msghdr, bufp,
		    seggrp->get_endpoint());
		ASSERT(rs != NULL);
		orb_msg::the().deliver_message(rs, os::NO_SLEEP,
		    tmp_msghdr.rm_seq);
		RSMT_MSG_DBG_D(RSMT_DBGL_INFO, RSMT_DBGF_BUFFERIO,
		    ("jumbo msg(%d) pushed to ORB\n", segindex));
		return;
	}
	// release the hold placed by get_buf()
	tmp_msgbufp->rele();
}

//
// Constructor - rsmt_bio_jumbo_sseg represents a jumbo send segment.
// Send segments are implemented as RSM import segments. The size of
// of the send seg is determined during initiate since it depends on
// what the receiver could allocate.
// sindex: segment index
// sgrp: seggrp that this rseg belongs to
// bmgr: pointer to the bufferio mgr
//
rsmt_bio_jumbo_sseg::rsmt_bio_jumbo_sseg(int sindex,
    rsmt_bio_jumbo_seggrp *sgrp, rsmt_bio_mgr *bmgr):
	// size of the segment is determined during initiate
	rsmt_bio_jumbo_seg(sindex, (size_t)0, sgrp, bmgr),
	seg_inuse(false), ackrecvd(true), imseg(NULL)
{
	RSMT_SEG_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_jumbo_sseg(%p) seggrp(%p) constructed\n", this, sgrp));
}

//
// Destructor
//
rsmt_bio_jumbo_sseg::~rsmt_bio_jumbo_sseg()
{
	RSMT_SEG_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_jumbo_sseg(%p) destructed\n", this));

	// disconnect only when all refcnt goes to zero since threads
	// might hold reservations to partitions
	if (imseg) {
		segmgr->disconnect_seg(imseg);
	}
	imseg = NULL;
}

//
// This method gets called from rsmt_bio_jumbo_seggrp::add_sseg()
// Once this method successfully completes the segment is ready to be used
// for sending messages.
//
bool
rsmt_bio_jumbo_sseg::initiate(rsmt_seg_conn_token_t conntok)
{
	rsmt_import_segment	*iseg;

	RSMT_SEG_DBG(RSMT_DBGL_INFO, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_jumbo_sseg(%p) initiate\n", this));

	js_lock.lock();
	if (state == RSMT_BIO_JSEGINIT) { // initiate already done
		js_cv.broadcast();
		js_lock.unlock();
		return (true);
	}

	if (!imseg) {
		// connect to the export segment
		iseg = segmgr->connect_seg(conntok);
		if (!iseg) {
			RSMT_SEG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
			    ("rsmt_bio_jumbo_sseg(%p) connect failed\n", this));
			js_lock.unlock();
			return (false);
		}
		RSMT_SEG_DBG(RSMT_DBGL_VERBOSE, RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_jumbo_sseg(%p) connected(%p)\n", this, iseg));
		imseg = iseg;
	}
	// get the seg size
	segsize = imseg->get_seg_size();
	state = RSMT_BIO_JSEGINIT;
	js_cv.broadcast();
	js_lock.unlock();

	return (true);
}

//
// Cleanup of the jumbo send segment
//
void
rsmt_bio_jumbo_sseg::cleanup()
{
	RSMT_SEG_DBG(RSMT_DBGL_INFO, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_jumbo_sseg(%p) cleanup\n", this));

	js_lock.lock();
	state = RSMT_BIO_JSEGDEL;
	js_cv.broadcast();
	js_lock.unlock();
}

//
// This routine checks if the send segment is available to send a jumbo
// message, if it is, the segment is marked as "in use" and a hold is placed
// on the segment. Returns false if segment in already in use or it is not
// initialized.
//
bool
rsmt_bio_jumbo_sseg::reserve_segment()
{
	js_lock.lock();
	if (seg_inuse || (state != RSMT_BIO_JSEGINIT)) {
		// segment is already in use
		js_lock.unlock();
		return (false);
	}

	seg_inuse = true;
	js_lock.unlock();
	hold();

	return (true);
}

//
// This method marks the segment as free and releases the hold on it
//
void
rsmt_bio_jumbo_sseg::release_segment()
{
	js_lock.lock();
	seg_inuse = false;
	js_lock.unlock();
	rele();
}


//
// This routine does the message transfer of the large messages using the
// following protocol -
//	1) First chunk that is transferred using rsm_put contains the rsmt_hdr_t
//	followed by the data.
//	2) A RSMT_BIO_MSGCHUNK control message containing the offset, size of
//	data in the chunk is sent.
//	3) Wait for RSMT_BIO_MSGCHUNKDONE control message
//	4) Step 2 and 3 are repeated till the whole message gets transferred
// If data transfer fails we return false and exception is set in the
// environment.
//
bool
rsmt_bio_jumbo_sseg::xfer_data(rsmt_hdr_t *rhp, Buf *bp, Environment *e)
{
	int	chnkcnt = 0;
	size_t	chnksz;
	size_t	msg_size = bp->span();
	off_t	msg_offset;
	off_t	seg_offset;
	char	*data = (char *)bp->head();
	rsm_barrier_t	bar;
	rsmt_endpoint	*ep;
	rsmt_bio_msg_t	chnk_msg;
	rsmt_padded_hdr pad_hdr(rhp);

	RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_jumbo_sseg(%p) jumbo xfer_data start(%lu)\n",
		this, sizeof (pad_hdr) + msg_size));

	// Check if the buffer is sufficiently padded to be a multiple
	// of cacheline so that puts can safely use the rounded up value
	// of actual message length in put_data
	ASSERT((bp->length() % RSMT_CACHELINE) == 0);

	// rsm_put of rsmt_hdr - the message header
	if (!put_data((void *)&pad_hdr, (off_t)0, sizeof (pad_hdr), &bar)) {
		RSMT_MSG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_jumbo_sseg(%p) rsm_put(jumbo msg hdr) failed\n",
			this));
		e->system_exception(CORBA::RETRY_NEEDED(0,
		    CORBA::COMPLETED_NO));
		return (false);
	}

	seg_offset = (off_t)sizeof (pad_hdr);
	msg_offset = 0;

	do {
		// amount of actual data being transferred
		chnksz = (msg_size > (segsize - (size_t)seg_offset)) ?
		    (segsize - (size_t)seg_offset) : msg_size;
		ASSERT((chnksz > 0) && (chnksz <= segsize));

		if (!put_data((void *)data, seg_offset, chnksz, &bar)) {
			RSMT_MSG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
			    ("rsmt_bio_jumbo_sseg(%p) rsm_put jumbo msg "
				"chunk(%d) failed\n", this, chnkcnt));
			break;
		}

		// send the MSGCHUNKDATA message
		chnk_msg.u.bmc_msg.msg_offset = msg_offset;
		chnk_msg.u.bmc_msg.msg_len = chnksz;
		chnk_msg.u.bmc_msg.flags = 0;
		if (seg_offset > 0) {
			// data was written at an offset - its the first chunk
			chnk_msg.u.bmc_msg.flags |= RSMT_BIO_CHUNKFIRST;
		}
		chnk_msg.u.bmc_msg.segindex = (ushort_t)segindex;

		js_lock.lock();
		ackrecvd = false;
		js_lock.unlock();

		if (!biomgr->cm_send(&chnk_msg, sizeof (chnk_msg),
		    RSMT_BIO_MSGCHUNK, true)) {
			RSMT_MSG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
			    ("rsmt_bio_jumbo_sseg(%p) jumbo msg chunk(%d)"
				"cm_send failed\n", this, chnkcnt));
			break;
		}

		js_lock.lock();
		// wait for the ack or path to go down.
		while (!ackrecvd && (state == RSMT_BIO_JSEGINIT)) {
			js_cv.wait(&js_lock);
		}

		if (state != RSMT_BIO_JSEGINIT) {
			js_lock.unlock();
			e->system_exception(CORBA::RETRY_NEEDED(0,
			    CORBA::COMPLETED_NO));
			RSMT_MSG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
			    ("rsmt_bio_jumbo_sseg(%p) put_data: segment not "
				"inited\n", this));
			return (false);
		}
		js_lock.unlock();

		msg_offset += (off_t)chnksz;
		msg_size -= chnksz;
		seg_offset = 0; // second chunks starts from the top
				// of the segment
		data += chnksz;
		chnkcnt++;
	} while (msg_size > 0);

	if (msg_size == 0) {
		RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_jumbo_sseg(%p) jumbo xfer_data done\n", this));
		return (true);
	} else {
		e->system_exception(CORBA::RETRY_NEEDED(0,
		    CORBA::COMPLETED_NO));
		// take the endpoint down - this will cause the
		// the remote endpoint cleanup as well
		js_lock.lock();
		ep = seggrp->get_endpoint();
		if (ep && (state == RSMT_BIO_JSEGINIT)) {
			ep->abort_endpoint();
		}
		js_lock.unlock();
		RSMT_MSG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_jumbo_sseg(%p) jumbo xfer_data failed "
			"aborting endpoint(%p)\n", this, ep));
		return (false);
	}
}

void
rsmt_bio_jumbo_sseg::mark_ackrecvd()
{
	js_lock.lock();
	ASSERT(!ackrecvd);
	ackrecvd = true;
	js_cv.broadcast();
	js_lock.unlock();
	RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_jumbo_sseg(%p) jumbo ackrecvd, index(%d)\n",
		this, segindex));
}

//
// This routine uses rsm_put to transfer chunks of message to the remote node.
// data - points to the source buffer
// offset - indicates the offset into the segment where the data should
// be transferred.
// len - indicates the number of bytes that are being transferred
// bar - is a pointer to the barrier that is opened/closed around the put
//
bool
rsmt_bio_jumbo_sseg::put_data(void *data, off_t offset, size_t len,
    rsm_barrier_t *bar)
{
	int rc;
	int retrycnt = 0;
	bool openbarrier = true; // openbarrier needed?
	size_t put_len;

	//
	// Roundup len to be a multiple of cacheline so that the rsm_put
	// follows the optimal path. It is safe to do the rounding because
	//  - If data is the rsmt header it is padded and is a multiple
	//  of cacheline already.
	//  - If data is an intermediate chunk then len is same as segsize
	// which is always a multiple of PAGESIZE and hence also a multiple
	// of cacheline.
	//  - If data is the last chunk it is possible that its not a multiple
	// of cacheline. Since its the last chunk we can round it up to
	// the nearest multiple and do the put since the message buffer is
	// already padded during the Mainbuf creation.
	//
	put_len = ROUNDUP_CACHELINE(len);

	do {
		js_lock.lock();
		if (state != RSMT_BIO_JSEGINIT) {
			js_lock.unlock();
			// we have not xferred the data yet and the path is
			// going down
			RSMT_MSG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
			    ("rsmt_bio_jumbo_sseg(%p)  xfer_data:state(%d):"
				"!RSMT_BIO_JSEGINIT\n",	this, state));
			return (false);
		}
		js_lock.unlock();

		if (openbarrier &&
		    ((rc = imseg->open_barrier(bar)) != RSM_SUCCESS)) {
			// RSMERR_CONN_ABORTED is the only possible error
			ASSERT(rc == RSMERR_CONN_ABORTED);
			RSMT_MSG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
			    ("rsmt_bio_jumbo_sseg(%p) open_barrier err=%d\n",
				this, rc));
			return (false);
		}

		openbarrier = false;

		if ((rc = imseg->put(offset, data, put_len)) == RSM_SUCCESS) {
			rc = imseg->close_barrier(bar);
			ASSERT((rc != RSMERR_BAD_BARRIER_HNDL) &&
			    (rc != RSMERR_BARRIER_NOT_OPENED) &&
			    (rc != RSMERR_BAD_BARRIER_PTR));
			if (rc == RSM_SUCCESS) {
				return (true);
			} else { // close_barrier failed
				//  open barrier is needed during the retry
				openbarrier = true;
				RSMT_MSG_DBG(RSMT_DBGL_ERROR,
				    RSMT_DBGF_BUFFERIO,
				    ("rsmt_bio_jumbo_sseg(%p) close_barrier"
					" err=%d\n", this, rc));
			}
		} else { // rsm_put of the data failed
			ASSERT((rc != RSMERR_BAD_SEG_HNDL) ||
			    (rc != RSMERR_BAD_MEM_ALIGNMENT) ||
			    (rc != RSMERR_BAD_LENGTH) ||
			    (rc != RSMERR_PERM_DENIED));
			RSMT_MSG_DBG(RSMT_DBGL_ERROR,
			    RSMT_DBGF_BUFFERIO, ("rsmt_bio_jumbo_sseg(%p) "
				"rsm_put(data) err=%d\n", this, rc));
		}

		ASSERT(rc != RSM_SUCCESS);

		if (rc == RSMERR_CONN_ABORTED) { // permanent connection error
			return (false);
		} else if ((rc == RSMERR_BARRIER_FAILURE) ||
		    (rc == RSMERR_INSUFFICIENT_RESOURCES)) {// worth retrying
			retrycnt++;
			if (retrycnt >= RSMT_PUTRETRY_BACKOFF) {
				retry_timeout();
			}
		} else {
			ASSERT(0); // unexpected error
		}
	} while (true);
}

//
// This method implements a backoff mechanism for the retries of the put
// during the data transfer. This method returns either due timeout or due to
// change of state of the segment
//
void
rsmt_bio_jumbo_sseg::retry_timeout()
{
	os::systime to((os::usec_t)1000);    // 1 msec

	js_lock.lock();
	while (state == RSMT_BIO_JSEGINIT) {
		if (js_cv.timedwait(&js_lock, &to) == os::condvar_t::TIMEDOUT) {
			js_lock.unlock();
			RSMT_SEG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_BUFFERIO,
			    ("rsmt_bio_jumbo_sseg(%p) retry timedout\n", this));
			return;
		}
	}
	js_lock.unlock();
}

//
// This constructor defines the jumbo segment group.
// bmgr: the bufferio manager pointer
// size: Max segment size of receive/send segs
// nsegs: Max number of jumbo send or receive segments
//
rsmt_bio_jumbo_seggrp::rsmt_bio_jumbo_seggrp(rsmt_bio_mgr *bmgr,
    size_t size, int nsegs): biomgr(bmgr), seg_size(size),
    max_segs(nsegs), avlbl_segs(0), uninit_segs(nsegs), initiated(false)
{
	int i;

	// allocate and initialize the receive segment array
	rsegs = new (rsmt_bio_jumbo_rseg *[(uint32_t)nsegs]);
	ASSERT(rsegs);
	for (i = 0; i < nsegs; i++) {
		rsegs[i] = NULL;
	}

	// allocate and initialize the send segment array
	ssegs = new (rsmt_bio_jumbo_sseg *[(uint32_t)nsegs]);
	ASSERT(ssegs);
	for (i = 0; i < nsegs; i++) {
		ssegs[i] = NULL;
	}

	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_jumbo_seggrp(%p) biomgr(%p) created\n", this, bmgr));
}

//
// Destructor
//
rsmt_bio_jumbo_seggrp::~rsmt_bio_jumbo_seggrp()
{
	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_jumbo_seggrp(%p) destructed\n", this));

	if (rsegs) {
		delete [] rsegs;
	}

	if (ssegs) {
		delete [] ssegs;
	}

	biomgr = NULL;
	seg_size = 0;
	max_segs = 0;
	uninit_segs = 0;
}

//
// called from rsmt_bio_mgr::initiate_fini
//
bool
rsmt_bio_jumbo_seggrp::initiate()
{
	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_jumbo_seggrp(%p) initiate\n", this));

	initiated = true;
	return (true);
}

//
// Called from rsmt_bio_mgr::initiate_cleanup() and
// rsmt_bio_mgr::cleanup_init()
//
void
rsmt_bio_jumbo_seggrp::cleanup()
{
	int i;

	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_jumbo_seggrp(%p) cleanup\n", this));

	sg_lock.lock();
	initiated = false;
	sg_cv.broadcast();
	sg_lock.unlock();

	seglist_lock.wrlock();
	for (i = 0; i < max_segs; i++) {
		// cleanup the send segs and release the hold acquired
		// during their instantiation.
		if (ssegs[i]) {
			ssegs[i]->cleanup();
			ssegs[i]->rele();
			ssegs[i] = NULL;
		}
	}

	for (i = 0; i < max_segs; i++) {
		// cleanup the receive segs and release the hold acquired
		// during their instantiation.
		if (rsegs[i]) {
			rsegs[i]->cleanup();
			rsegs[i]->rele();
			rsegs[i] = NULL;
		}
	}
	seglist_lock.unlock();
}

//
// Currently a noop
//
bool
rsmt_bio_jumbo_seggrp::push()
{
	// messages are deemed complete only when the sender
	// receives the final ACK. so nothing needs to be done here.
	return (true);
}

//
// Called from rsmt_bio_mgr::send(), this method uses a send segment
// and transfers the jumbo message over it.
//
bool
rsmt_bio_jumbo_seggrp::send(rsmt_hdr_t *rhp, Buf *bp, Environment *e)
{
	bool	msgsent;
	size_t	tot_size = bp->span() + sizeof (rsmt_padded_hdr);
	rsmt_bio_jumbo_sseg *ss;

	// Locate a free send seg, if required create a new one.
	ss = find_sseg(tot_size);
	if (!ss) {
		// all attempts to get a send seg failed so return a
		// comm failure.
		e->system_exception(CORBA::RETRY_NEEDED(0,
		    CORBA::COMPLETED_NO));
		RSMT_SEG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_jumbo_seggrp(%p) no sseg giving up\n", this));
		return (false);
	}

	// transfer the message
	msgsent =  ss->xfer_data(rhp, bp, e);

	// marks the segment as free and releases the hold on it
	ss->release_segment();

	sg_lock.lock();
	avlbl_segs++; // one more send segment is available now.
	sg_cv.broadcast();
	sg_lock.unlock();

	RSMT_SEG_DBG_D(RSMT_DBGL_INFO, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_jumbo_seggrp(%p) msg(%ul) sent\n", this, tot_size));
	return (msgsent);
}

//
// free_resources is called when resource manager wants to reclaim
// segments - currently a noop
//
void
rsmt_bio_jumbo_seggrp::free_resources()
{
	// free available ssegs and signal the receiver so that it can
	// destroy the export segs.
}


//
// Get the endpoint to which this seggrp is associated with
//
rsmt_endpoint *
rsmt_bio_jumbo_seggrp::get_endpoint()
{
	return (biomgr->get_endpoint());
}

//
// Return the sseg for a given segment index - a hold is placed on the
// send segment returned
//
rsmt_bio_jumbo_sseg *
rsmt_bio_jumbo_seggrp::get_sseg(int segindex)
{
	rsmt_bio_jumbo_sseg *p = NULL;

	seglist_lock.rdlock();

	ASSERT(segindex < max_segs);
	if (ssegs && (segindex < max_segs)) {
		p = ssegs[segindex];
		if (p) {
			p->hold();
		}
	}
	seglist_lock.unlock();

	return (p);
}

//
// Return the rseg for a given segment index - a hold is placed on the
// recv segment returned
//
rsmt_bio_jumbo_rseg *
rsmt_bio_jumbo_seggrp::get_rseg(int segindex)
{
	rsmt_bio_jumbo_rseg *p = NULL;

	seglist_lock.rdlock();

	ASSERT(segindex < max_segs);
	if (rsegs && (segindex < max_segs)) {
		p = rsegs[segindex];
		if (p) {
			p->hold();
		}
	}
	seglist_lock.unlock();

	return (p);
}

//
// This routine is called while processing the RSMT_BIO_SEGRCREATE message
// with the jumbo flag set. A new receive segment is created and added to
// the rseg array. At this point a segment index is assigned to the segment,
// which along with the segment's connection token is passed back to the
// the sender.
//
// reqsize: requested segment size
//
void
rsmt_bio_jumbo_seggrp::add_rseg(size_t reqsize)
{
	int	i;
	bool	success = true;
	rsmt_seg_conn_token_t	conntok = RSMT_INVAL_SEG_CONN_TOKEN;
	rsmt_bio_msg_t		con_msg;
	rsmt_bio_jumbo_rseg	*rs = NULL;

	if (reqsize > seg_size) {
		// requested segment size is more than what can be supported
		RSMT_SEG_DBG(RSMT_DBGL_INFO, RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_jumbo_seggrp(%p) add_rseg req(%l) > seg(%l)\n",
			this, reqsize, seg_size));
		reqsize = seg_size;
	}

	seglist_lock.wrlock();
	for (i = 0; i < max_segs; i++) {
		if (!rsegs[i]) { // found a free entry in rsegs array
			rs = new rsmt_bio_jumbo_rseg(i, reqsize, this, biomgr);
			ASSERT(rs);
			rsegs[i] = rs;
			break;
		}
	}
	seglist_lock.unlock();

	// should not receive add_rseg requests if rsegs array is already full
	// since the sender knows about it
	ASSERT(i < max_segs);

	if (!rs || !rs->initiate(&conntok)) {
		success = false;
	}

	RSMT_SEG_DBG(RSMT_DBGL_INFO, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_jumbo_seggrp(%p) add_rseg(%d) conntok=0x%llx\n", this,
		i, conntok));

	//
	// send the RSMT_BIO_SEGCONNECT message, if initiate() fails
	// the conn_tok is an invalid token and the remote node will
	// see that and know that the RSMT_BIO_SEGCREATE was not
	// successful.
	//
	con_msg.u.bco_msg.tok = conntok;
	con_msg.u.bco_msg.flags = RSMT_BIO_CONNECTJUMBO;
	con_msg.u.bco_msg.nseggrp = (uchar_t)0;
	con_msg.u.bco_msg.segindex = (ushort_t)i;
	con_msg.u.bco_msg.partitions = (ushort_t)0;
	if (!biomgr->cm_send(&con_msg, sizeof (con_msg), RSMT_BIO_SEGCONNECT,
	    true)) {
		RSMT_SEG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_jumbo_seggrp(%p) add_rseg(%d) SEGCONNECT err\n",
			this, i));
		success = false;
	}

	if (!success) {
		seglist_lock.wrlock();
		rsegs[i] = NULL; // cleanup the receive seg and free array elem
		seglist_lock.unlock();
		if (rs) {
			rs->cleanup();
			rs->rele();
		}
	}
}


//
// This routine creates the sseg and inserts it into the sseg array in the
// jumbo seggrp. If the conntok is RSMT_INVAL_SEG_CONN_TOKEN it indicates
// that the create of the rseg failed on the remote node. This routine
// is called while processing the RSMT_BIO_SEGCONNECT message with the
// jumbo flag set.
//
// sindex: segment index
// conntok: connection token corresponding to the receive segment
//
void
rsmt_bio_jumbo_seggrp::add_sseg(int sindex, rsmt_seg_conn_token_t conntok)
{
	rsmt_bio_jumbo_sseg *ss;

	RSMT_SEG_DBG(RSMT_DBGL_INFO, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_jumbo_seggrp(%p) add_sseg(0x%llx)\n", this, conntok));

	if (conntok == RSMT_INVAL_SEG_CONN_TOKEN) {
		// the export create on the remote node failed
		sg_lock.lock();
		// uninit_segs was decremented in find_sseg before sending the
		// RSMT_MSG_SEGCREATE message, increment it since create failed
		uninit_segs++;
		sg_cv.broadcast();
		sg_lock.unlock();
		RSMT_SEG_DBG(RSMT_DBGL_INFO, RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_jumbo_seggrp(%p) add_sseg invalid conntok\n"));
		return;
	}

	seglist_lock.wrlock();
	ASSERT(ssegs[sindex] == NULL);
	ss = new rsmt_bio_jumbo_sseg(sindex, this, biomgr);
	ASSERT(ss);
	ssegs[sindex] = ss;
	seglist_lock.unlock();

	if (!ss->initiate(conntok)) {
		// only reason for initiate to fail is the export seg is gone
		seglist_lock.wrlock();
		ssegs[sindex] = NULL;
		seglist_lock.unlock();
		ss->rele();
		sg_lock.lock();
		uninit_segs++; // it was decremented in find_seg
		sg_cv.broadcast();
		sg_lock.unlock();
		RSMT_SEG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_jumbo_seggrp(%p) add_sseg(%p) initiate failed\n",
			this, ss));
		return;
	}

	sg_lock.lock();
	avlbl_segs++;	// one more sseg is available now
	sg_cv.broadcast();
	sg_lock.unlock();
}

//
// This routine sends a RSMT_BIO_SEGCREATE control message to the remote node
// to request creation of a receive segment in the jumbo seggrp.
//
bool
rsmt_bio_jumbo_seggrp::create_sseg(size_t msg_size)
{
	rsmt_bio_msg_t crt_msg;
	RSMT_SEG_DBG(RSMT_DBGL_INFO, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_jumbo_seggrp(%p) create_sseg(0x%lx)\n", this, msg_size));

	// create jumbo segment message - set the flag to indicate that
	crt_msg.u.bcr_msg.flags = RSMT_BIO_CREATEJUMBO;
	// requested seg size is the min of msg_size and max allowed seg_size
	crt_msg.u.bcr_msg.reqsize = (msg_size > seg_size) ? seg_size : msg_size;

	if (!biomgr->cm_send(&crt_msg, sizeof (crt_msg), RSMT_BIO_SEGCREATE,
	    true)) {
		RSMT_SEG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_jumbo_seggrp(%p) send(SEGCREATE) err\n", this));
		return (false);
	}

	return (true);
}

//
// find_sseg returns a send segment that can be used to send jumbo messages.
// It returns NULL if the seggrp is being cleaned up. If a preconnected
// and free segment exists that is returned otherwise creation of a new segment
// is requested provided more segments can be added to the seggrp.
//
rsmt_bio_jumbo_sseg *
rsmt_bio_jumbo_seggrp::find_sseg(size_t msg_size)
{
	int i;
	int retry_cnt = 5;
	rsmt_bio_jumbo_sseg *ss;

	RSMT_SEG_DBG_D(RSMT_DBGL_INFO, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_jumbo_seggrp(%p) find_sseg(0x%lx)\n", this, msg_size));

	do {
		sg_lock.lock();
		if (!initiated) {
			sg_lock.unlock();
			RSMT_SEG_DBG_D(RSMT_DBGL_INFO, RSMT_DBGF_BUFFERIO,
			    ("rsmt_bio_jumbo_seggrp(%p) find_sseg not init\n",
				this));
			return (NULL);
		}
		sg_lock.unlock();

		ss = NULL;
		seglist_lock.rdlock();
		for (i = 0; i < max_segs; i++) {
			if (ssegs[i] && ssegs[i]->reserve_segment()) {
				// reserve_segment places a hold on the segment
				ss = ssegs[i];
				break;
			}
		}
		seglist_lock.unlock();

		if (!ss) {
			sg_lock.lock();
			if (uninit_segs > 0) {
				// all existing segs are in use but we can
				// still add some more new segs
				uninit_segs--;
				sg_lock.unlock();
				if (!create_sseg(msg_size)) {
					sg_lock.lock();
					uninit_segs++; // undo the decr
					sg_lock.unlock();
					// attempt to create a new segment
					// failed start-over from the beginning
					continue;
				} else {
					sg_lock.lock();
				}
			}
			//
			// there are initialized ssegs and they are either in
			// use OR due to create_sseg more segs are being
			// initialized. Wait till something becomes available
			// OR if creation of new segs fails
			//
			while ((uninit_segs < max_segs) && (avlbl_segs == 0) &&
			    initiated) {
				sg_cv.wait(&sg_lock);
			}

			//
			// If the number of uninitialized segs is same as the
			// max_segs it indicates that jumbo segment could not
			// be created - this primarily happens due to resource
			// shortage at the remote endpoint. In these cases
			// retry a few times before giving up.
			//
			if (uninit_segs == max_segs) {
				retry_cnt--;
			}
			sg_lock.unlock();
			//  go to the start of the do-while loop.
		} else {
			sg_lock.lock();
			// consumed a send seg, decrement avlbl_segs counter
			avlbl_segs--;
			sg_lock.unlock();

			RSMT_SEG_DBG_D(RSMT_DBGL_INFO, RSMT_DBGF_BUFFERIO,
			    ("rsmt_bio_jumbo_seggrp(%p) find_sseg done(%p)\n",
				this, ss));
			return (ss);
		}
	} while (retry_cnt > 0);

	RSMT_SEG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_jumbo_seggrp(%p) find_sseg failed: resource shortage\n",
		this));
	return (NULL);
}


//
// GWBuf with a refcnt wrapper class
//
rsmt_bio_jumbo_buf::rsmt_bio_jumbo_buf(uint_t size)
{
	bufp = new GWBuf(size);
	ASSERT(bufp);
}

rsmt_bio_jumbo_buf::~rsmt_bio_jumbo_buf()
{
	ASSERT(verify_count(0));
	if (bufp) {
		bufp->done();
	}
}

// places a hold on rsmt_bio_jumbo_buf and returns the GWBuf
GWBuf *
rsmt_bio_jumbo_buf::get_buf()
{
	hold();
	return (bufp);
}

// called before the ownership of GWBuf is passed to the ORB
GWBuf *
rsmt_bio_jumbo_buf::extract_buf()
{
	GWBuf *tmp = bufp;
	ASSERT(tmp);
	// disassociate self from the embedded GWBuf
	bufp = NULL;
	return (tmp);
}
