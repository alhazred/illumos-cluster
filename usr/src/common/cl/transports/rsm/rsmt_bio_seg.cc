//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rsmt_bio_seg.cc	1.24	08/05/20 SMI"

#include <sys/types.h>
#include <sys/threadpool.h>

#include <sys/rsm/rsm_common.h>

#include "rsmt_bio.h"
#include "rsmt_bio_seg.h"
#include "rsmt_bio_buf.h"
#include "rsmt_streams.h"

// Threshold at which bufferio starts loaning up partitions
size_t RSMT_BIO_MIN_LOANSZ = 2*PAGESIZE;

//
// Generic bufferio segment class. The receive/send segments are subclasses
// of this class.
//
rsmt_bio_seg::rsmt_bio_seg(int segix, int npartition, rsmt_bio_seggrp *sgp,
    rsmt_seg_mgr *smgr):
	state(RSMT_BIO_SEGNEW),
	seg_index(segix),
	segsize(0),
	num_partitions(npartition),
	partition_sz(sgp->get_partitionsz()),
	seggrp(sgp),
	segmgr(smgr)
{
	ASSERT(num_partitions > 0);
	dirsz = (uint32_t)num_partitions*sizeof (rsmt_bio_dirent);
	// dirsz should be cacheline multiple to ensure partitions
	// start at cacheline aligned address
	ASSERT((dirsz & (RSMT_CACHELINE-1)) == 0);
}

rsmt_bio_seg::~rsmt_bio_seg()
{
	segmgr = NULL;
	seggrp = NULL;
	num_partitions = 0;
	seg_index = 0;
	state = 0;
	dirsz = 0;
	partition_sz = 0;
}

//
// receive segment - are implemented using rsm export segments. These
// segments have buffers where the messages are received from the
// remote node. rsmt_bio_rseg is a subclass of rsmt_bio_seg and implements
// the get_dir() and get_dirent(). The directory in a receive segment is
// embedded in the export segment.
//
rsmt_bio_rseg::rsmt_bio_rseg(int segix, int npartition, rsmt_bio_seggrp *sgp,
    rsmt_seg_mgr *smgr):
	rsmt_bio_seg(segix, npartition, sgp, smgr),
	membuf(NULL),
	raw_mem(NULL),
	raw_size(0),
	exseg(NULL),
	exseg_destroy_pending(false)
{
	RSMT_SEG_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_rseg(%p) seggrp(%p) segidx(%d) constructed\n", this,
		sgp, segix));
}

//
// The destroy_seg_done (static) callback. Gets called back by the endpoint
// segment manager when the export segment in question is unpublished and
// destroyed.
//
void
#if defined(DBGBUFS) && !defined(NO_RSMT_DBGBUF)
rsmt_bio_rseg::destroy_seg_done_callback(void *rsegp, rsmt_export_segment *eseg)
#else
rsmt_bio_rseg::destroy_seg_done_callback(void *rsegp, rsmt_export_segment *)
#endif
{
	RSMT_SEG_DBG(RSMT_DBGL_INFO, RSMT_DBGF_SEGMGR,
	    ("Segment destroy rseg %p callback eseg %p\n",
	    rsegp, eseg));
	((rsmt_bio_rseg *)rsegp)->exseg_destroy_complete();
	((rsmt_bio_rseg *)rsegp)->rele(); // Acquired when calling destroy_seg
}

//
// Gets called by the above static callback method. Records that the export
// segment associated with this rseg has been destroyed.
//
void
rsmt_bio_rseg::exseg_destroy_complete()
{
	exseg_destroy_pending = false;
}

//
// Destructor
//
rsmt_bio_rseg::~rsmt_bio_rseg()
{
	RSMT_SEG_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_rseg(%p) destructed\n", this));
	exseg = NULL;
	if (raw_mem) {
		free_chunk_aligned(raw_mem, raw_size);
	}
	raw_mem = NULL;
	raw_size = 0;
	membuf = NULL;
}

//
// Initiate a receive segment - this gets called from
// rsmt_bio_seggrp::add_rseg(). This method allocates memory
// and creates an export segment using the allocated memory. It changes
// the state of the receive segment to SEGINIT. It returns true if
// the operation is successful and the connection token of the export
// segment is returned in outarg, otherwise false is returned and
// outarg is set to RSMT_INVAL_SEG_CONN_TOKEN. Initiate is idempotent.
//
bool
rsmt_bio_rseg::initiate(void *outarg)
{
	rsmt_seg_conn_token_t	*conntok = (rsmt_seg_conn_token_t *)outarg;
	rsmt_export_segment	*eseg;
	size_t	seg_size;
	size_t  asize;
	size_t	rsize;
	void	*rmem;
	void	*segbuf;

	RSMT_SEG_DBG(RSMT_DBGL_INFO, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_rseg(%p) initiate\n", this));

	*conntok = RSMT_INVAL_SEG_CONN_TOKEN;

	s_lock.lock();

	if (state == RSMT_BIO_SEGINIT) {
		// return the connection token
		*conntok = segmgr->get_conn_token(exseg);
	} else if (state == RSMT_BIO_SEGNEW) {
		s_lock.unlock();
		seg_size = dirsz + (uint32_t)num_partitions*partition_sz;

		// alloc page aligned memory
		segbuf = alloc_chunk_aligned(seg_size, (size_t)PAGESIZE,
		    &asize, &rmem, &rsize);
		ASSERT(segbuf);
		bzero(rmem, rsize);
		RSMT_SEG_DBG(RSMT_DBGL_INFO, RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_rseg(%p) buf=%p,sz=0x%lx\n", this, segbuf,
			asize));

		// create the export segment and bind segbuf to it
		eseg = segmgr->create_seg(asize, RSMT_SMEX_CONNECT, segbuf,
		    asize, RSM_RESOURCE_SLEEP);

		if (!eseg) {
			free_chunk_aligned(rmem, rsize);
			RSMT_SEG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
			    ("rsmt_bio_rseg(%p) create_seg failed\n", this));
			return (false);
		}

		s_lock.lock();
		segsize = asize;
		raw_mem = rmem;
		raw_size = rsize;
		membuf = segbuf;
		exseg = eseg;
		state = RSMT_BIO_SEGINIT;
		// return the connection token
		*conntok = segmgr->get_conn_token(eseg);
	}
	s_lock.unlock();

	return (true);
}

//
// Cleanup of the receive segment - destroy the export segment.
// The backing store of the export segment is released later
// in the destructor
//
void
rsmt_bio_rseg::cleanup()
{
	rsmt_seg_destroy_callback_t *cbfp;
	cbfp = (rsmt_seg_destroy_callback_t *)destroy_seg_done_callback;

	RSMT_SEG_DBG(RSMT_DBGL_INFO, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_rseg(%p) cleanup\n", this));

	s_lock.lock();
	if (exseg) {
		exseg_destroy_pending = false;
		hold(); // Released in callback
		segmgr->destroy_seg(exseg, 0, cbfp, this);
	}
	exseg = NULL;
	state = RSMT_BIO_SEGDEL;
	s_cv.broadcast();
	s_lock.unlock();
}

//
// Get the start of the directory, in receive segments this is at the
// top of the memory bound to the rsm export segment.
//
rsmt_bio_dirent	*
rsmt_bio_rseg::get_dir()
{
	// return the aligned-address - that is where the directory starts
	return ((rsmt_bio_dirent *)membuf);
}

//
// Get the directory entry corresponding to a partition indicated by
// the partition index.
//
rsmt_bio_dirent *
rsmt_bio_rseg::get_dirent(int pindex)
{
	return ((rsmt_bio_dirent *)membuf + pindex);
}

//
// Deliver messages in the partitions to ORB, this is called from the
// rsmt_bio_seggrp::push()
// Iterate through all the partitions in the segment, in each iteration locate
// partitions in the write-done state (WRDONE) and push it to the ORB.
// Return value is a completion status. False indicates cleanup has started
// and more messages are possible. Return value is true otherwise.
// Return value is always true in non cleanup cases.
//
bool
rsmt_bio_rseg::push()
{
	rsmt_bio_dirent	*dir = get_dir();
	rsmt_hdr_t	hdr;
	uchar_t		*datap;
	Buf		*rb;
	int	curprt = 0;
	int	i;
	bool	allpushed = exseg_destroy_pending ? false : true;

	while (curprt < num_partitions) {
		i = curprt;
		s_lock.lock();

		// find partition that is in the WRDONE state and
		// is the first partition of the message

		if ((dir[i].ent.state != RSMT_PARTITION_WRDONE) ||
		    !(dir[i].ent.flag & RSMT_BDFLGS_FIRST)) {
			s_lock.unlock();
			curprt++;
			continue; // loop and look at the next direntry
		}

		if (dir[i].ent.next == 0) { // message in 1 partition
			ASSERT(dir[i].ent.state == RSMT_PARTITION_WRDONE);
			dir[i].ent.state = RSMT_PARTITION_ONLOAN;
			curprt++;
		} else { // message spans 2 partitions
			if (dir[i+1].ent.state != RSMT_PARTITION_WRDONE) {
				// the second half of the message has not yet
				// arrived - skip and continue
				s_lock.unlock();
				curprt += 2;
				continue;
			}
			ASSERT(dir[i+1].ent.next == 0);
			ASSERT(!(dir[i+1].ent.flag & RSMT_BDFLGS_FIRST));

			ASSERT(dir[i].ent.state == RSMT_PARTITION_WRDONE);
			ASSERT(dir[i+1].ent.state == RSMT_PARTITION_WRDONE);

			dir[i].ent.state = RSMT_PARTITION_ONLOAN;
			dir[i+1].ent.state = RSMT_PARTITION_ONLOAN;
			curprt += 2;
		}
		// acquire hold, released in release_partitions
		hold();
		s_lock.unlock();

		RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_rseg(%p) push(%d,%d)\n", this, i, curprt - i));

		// make a local copy of the header
		bcopy(partition(i), (char *)&hdr, sizeof (rsmt_padded_hdr));

		RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_rseg(%p) push seq(%u) cksum(%u:%u) \n", this,
			hdr.rm_seq, hdr.rm_cksum,
			rsmt_padded_hdr::checksum32((char *)partition(i) +
			    sizeof (rsmt_padded_hdr),
			    (size_t)hdr.rm_total_len)));

		if (hdr.rm_total_len < RSMT_BIO_MIN_LOANSZ) {
			// copy message out of the partition and release
			// the partition immediately
			datap = GWBuf::alloc_buffer(hdr.rm_total_len,
			    os::SLEEP);
			bcopy((char *)partition(i)+sizeof (rsmt_padded_hdr),
			    (char *)datap, (size_t)hdr.rm_total_len);
			release_partitions((uchar_t *)partition(i) +
			    sizeof (rsmt_padded_hdr), hdr.rm_total_len);
			// data is located on the heap hence Buf::HEAP.
			// We do not pass "this" since its not needed
			// when buffer is on the heap. More over it would be
			// wrong to pass it since the hold on rseg has been
			// released in release_partitions
			rb = new rsmt_bio_buf(NULL, datap, hdr.rm_total_len,
			    Buf::HEAP);
		} else {
			// This is a large message and we don't want to
			// take the hit of an extra bcopy. So the partition
			// is loaned up to ORB. The partition is released
			// when the rsmt_bio_buf gets destructed
			datap = (uchar_t *)partition(i) +
			    sizeof (rsmt_padded_hdr);
			// data is in a transport owned buffer hence
			// alloc_type is Buf::OTHER
			rb = new rsmt_bio_buf(this, datap, hdr.rm_total_len,
			    Buf::OTHER);
		}

		ASSERT(rb != NULL);

		rsmt_recstream *rs = new rsmt_recstream(&hdr, rb,
		    seggrp->get_endpoint());
		ASSERT(rs != NULL);

		// push the message up to the ORB
		orb_msg::the().deliver_message(rs, os::NO_SLEEP, hdr.rm_seq);
	}

	return (allpushed);
}

//
// Releases the partition that maps to the passed data pointer. The passed
// pointer should point to the data part of the message in the partition
// and size is used to determine the number of partitions being freed.
// This method gets called either immediately after the message is copied
// out of the partition or from the rsmt_bio_buf destructor if the partitioned
// was loaned up to the ORB.
//
void
rsmt_bio_rseg::release_partitions(uchar_t *p, uint_t size)
{
	rsmt_bio_dirent	*dir = get_dir();
	int numprt = get_numpartitions(size + sizeof (rsmt_padded_hdr));
	int pidx;

	// calculate the partition index
	// lint thinks partition_sz can be 0
	pidx = (int)(((uintptr_t)(p - sizeof (rsmt_padded_hdr)) -
	    (uintptr_t)partition(0)) / partition_sz); //lint !e414

	RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_BUFFERIO,
	    ("release partitions: datap(%p), %d:%d\n", p, pidx, numprt));

	// mark the partitions as CONSUMED since we have delivered
	// message to ORB
	s_lock.lock();
	ASSERT(dir[pidx].ent.state == RSMT_PARTITION_ONLOAN);
	dir[pidx].ent.state = RSMT_PARTITION_CONSUMED;
	if (numprt == 2) {
		ASSERT(dir[pidx+1].ent.state == RSMT_PARTITION_ONLOAN);
		dir[pidx+1].ent.state = RSMT_PARTITION_CONSUMED;
	}
	s_lock.unlock();

	// mark the partitions are free in the shadow pavail
	// and update remote pavail if required
	seggrp->msg_consumed(seg_index, pidx, numprt);

	// release hold acquired in push
	rele();
}

//
// send segments - are implemented using rsm import segments.
// rsmt_bio_sseg is a subclass of rsmt_bio_seg and implements, initiate(),
// get_dir() and get_dirent(). The send segment maintains a local copy of
// the directory, the directory which exists in the corresponding
// receive segment is accessed via remote put/get operations.
//
rsmt_bio_sseg::rsmt_bio_sseg(int segix, int npartition, rsmt_bio_seggrp *sgp,
    rsmt_seg_mgr *smgr):
	rsmt_bio_seg(segix, npartition, sgp, smgr),
	local_dir(NULL),
	frag_index(0),
	free_partitions(0),
	alloc_failures(0),
	free_pindex(0),
	prev_genr(0),
	conn_token(0),
	imseg(NULL)
{
	RSMT_SEG_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_sseg(%p) seggrp(%p) segidx(%d) constructed\n", this,
		sgp, segix));
}

//
// Destructor
//
rsmt_bio_sseg::~rsmt_bio_sseg()
{
	RSMT_SEG_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_sseg(%p) destructed\n", this));

	// disconnect only when all refcnt goes to zero since threads
	// might hold reservations to partitions
	if (imseg) {
		segmgr->disconnect_seg(imseg);
	}
	imseg = NULL;
	conn_token = 0;
	prev_genr = 0;
	alloc_failures = 0;
	free_partitions = 0;
	frag_index = 0;
	if (local_dir) {
		delete [] local_dir;
	}
	local_dir = NULL;
}

//
// This method gets called while processing the RSMT_BIO_SEGCONNECT message.
// The message carries the connection token that is passed IN as inarg.
// Once this method successfully completes the segment is ready to be used
// for sending messages. Initiate is idempotent.
//
bool
rsmt_bio_sseg::initiate(void *inarg)
{
	rsmt_import_segment	*iseg;
	rsmt_bio_dirent		*ldir;
	rsmt_seg_conn_token_t	*conntok = (rsmt_seg_conn_token_t *)inarg;

	RSMT_SEG_DBG(RSMT_DBGL_INFO, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_sseg(%p) initiate\n", this));

	s_lock.lock();
	if (state == RSMT_BIO_SEGINIT) { // initiate already done
		s_cv.broadcast();
		s_lock.unlock();
		return (true);
	}

	if (!local_dir) { // allocate and initialize the local directory
		ldir = new rsmt_bio_dirent[(uint32_t)num_partitions];
		ASSERT(ldir);
		for (int i = 0; i < num_partitions; i++) {
			ldir[i].ent.genrnum = 0;
			ldir[i].ent.state = RSMT_PARTITION_FREE;
			ldir[i].ent.flag = 0;
			ldir[i].ent.next = 0;
		}
		local_dir = ldir;
	}

	if (!imseg) {
		conn_token = *conntok;
		// connect to the export segment
		iseg = segmgr->connect_seg(*conntok);
		if (!iseg) {
			RSMT_SEG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
			    ("rsmt_bio_sseg(%p) connect failed\n", this));
			s_lock.unlock();
			return (false);
		}
		RSMT_SEG_DBG(RSMT_DBGL_VERBOSE, RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_sseg(%p) connected(%p)\n", this, iseg));
		imseg = iseg;
	}

	segsize = imseg->get_seg_size();
	// all partitions are free in the beginning
	free_partitions = num_partitions;
	prev_genr = 0; // genr number is 0 in the beginning
	state = RSMT_BIO_SEGINIT;
	s_cv.broadcast();
	s_lock.unlock();
	// update the number of free partitions in this seggrp due
	// to this new send segment and wakeup threads waiting for
	// free partitions
	seggrp->update_freepartitions(num_partitions, true);
	return (true);
}

//
// Cleanup of the send segment
//
void
rsmt_bio_sseg::cleanup()
{
	int cur_frprt;

	s_lock.lock();
	cur_frprt = free_partitions;
	free_partitions = 0;
	state = RSMT_BIO_SEGDEL;
	s_cv.broadcast();
	s_lock.unlock();
	// update freepartitions in the seggrp due to this segment
	seggrp->update_freepartitions(-cur_frprt, false);
}

//
// Get the start of the directory, in receive segments this is at the
// top of the memory bound to the rsm export segment.
//
rsmt_bio_dirent	*
rsmt_bio_sseg::get_dir()
{
	// return the aligned-address - that is where the directory starts
	return ((rsmt_bio_dirent *)&local_dir[0]);
}

//
// Get the directory entry corresponding to a partition indicated by
// the partition index.
//
rsmt_bio_dirent *
rsmt_bio_sseg::get_dirent(int pindex)
{
	return ((rsmt_bio_dirent *)&local_dir[pindex]);
}

//
// This method waits till the initiate() on rsmt_bio_sseg get called
// or it timesout.
//
bool
rsmt_bio_sseg::wait_for_initiate()
{
	os::systime to((os::usec_t)1000000);    // 1 sec

	s_lock.lock();
	while (state == RSMT_BIO_SEGNEW) {
		if (conn_token) { // SEGCONNECT message was received
			s_lock.unlock();
			if (initiate(&conn_token)) { // retry initiate
				s_lock.lock();
				break;
			} else {
				// initiate failed - receive segment must
				// have gone away - no point waiting.
				return (false);
			}
		}

		if (s_cv.timedwait(&s_lock, &to) == os::condvar_t::TIMEDOUT) {
			s_lock.unlock();
			RSMT_SEG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
			    ("rsmt_bio_sseg(%p) wait_for_initiate timedout\n",
				this));
			return (false);
		}
	}

	if (state == RSMT_BIO_SEGINIT) {
		s_lock.unlock();
		return (true);
	} else {
		s_lock.unlock();
		return (false);
	}
}

//
// Allocate partition(s) for a message of size msgsz and return
// the partition-index, -1 is returned if alloc fails.
// A message should either fit in 1 partition or 2 contiguos partitions
//
int
rsmt_bio_sseg::alloc(size_t msgsz)
{
	int	numprt = get_numpartitions(msgsz);
	int	cnt;
	int	i = 0;

	// alloc allows messages to span max 2 contiguous partitions
	ASSERT(numprt <= 2);

	s_lock.lock();
	if (state != RSMT_BIO_SEGINIT) {
		s_lock.unlock();
		return (false);
	}
	// locate a free partition
	if (numprt == 1) {
		// do a circular loop through the all partitions
		// starting at free_pindex
		for (cnt = 0; cnt < num_partitions; cnt++) {
			i = (free_pindex + cnt)%
			    num_partitions; //lint !e414
			if (local_dir[i].ent.state == RSMT_PARTITION_FREE) {
				// reserve the partition
				local_dir[i].ent.state =
				    RSMT_PARTITION_INUSE;
				free_pindex = 0; // reset free_pindex
				free_partitions--;
				break;
			}
		}
		if (cnt == num_partitions) { // no free partitions
			i = -1;
			alloc_failures++;
		}
	} else { // looking for 2 contiguous free partitions
		cnt = 0;
		// when looking for 2 contiguous free partitions
		// the last partition of the seg cannot be used to store
		// the first half of the message.
		// i points to the first of the two partitions and
		// it goes from
		// free_pindex -->num_partitions-2,0-->free_pindex-2
		//
		while (cnt < num_partitions-1) {
			i = (free_pindex + cnt)%(num_partitions-1);
			if (local_dir[i].ent.state == RSMT_PARTITION_FREE) {
				if (local_dir[i+1].ent.state ==
				    RSMT_PARTITION_FREE) {
					break;
				} else { // i+1 is not free skip it
					cnt += 2;
				}
			} else { // i is not free go to i+1
				cnt++;
			}
		}

		if (cnt == num_partitions-1) { // no free partition pair
			i = -1;
			alloc_failures++;
		} else {
			local_dir[i].ent.state = RSMT_PARTITION_INUSE;
			local_dir[i+1].ent.state = RSMT_PARTITION_INUSE;
			free_pindex = 0;
			free_partitions -= 2;
		}
	}

	if (i == -1) {
		s_lock.unlock();
	} else {
		hold(); // hold is released in rsmt_bio_mgr::send
		s_lock.unlock();
		seggrp->update_freepartitions(-numprt, false);
	}

	return (i);
}

//
// This method is called to undo the reservation done in alloc(), when
// the data transfer operation fails. Called from rsmt_bio_mgr::send()
//
void
rsmt_bio_sseg::free(int pindex, size_t msgsz)
{
	int numprt = get_numpartitions(msgsz);

	// messages can only span max 2 contiguous partitions
	ASSERT(numprt <= 2);

	// free up the directory entry
	s_lock.lock();
	local_dir[pindex].ent.genrnum = 0;
	local_dir[pindex].ent.state = RSMT_PARTITION_FREE;
	local_dir[pindex].ent.flag = 0;
	local_dir[pindex].ent.next = 0;
	if (numprt == 2) {
		local_dir[pindex+1].ent.genrnum = 0;
		local_dir[pindex+1].ent.state = RSMT_PARTITION_FREE;
		local_dir[pindex+1].ent.flag = 0;
		local_dir[pindex+1].ent.next = 0;
	}
	free_pindex = pindex;
	free_partitions += numprt;
	if (state == RSMT_BIO_SEGINIT) {
		s_lock.unlock();
		// update the seggrp only if segment is in SEGINIT state
		// wakeup threads waiting for free partitions
		seggrp->update_freepartitions(numprt, true);
	} else {
		s_lock.unlock();
	}
}

//
// This method is called from rsmt_bio_mgr::send() to do the data
// transfer. The partition index passed in refers to the partitions that
// are reserved prior to calling xfer_data().
//
// The data transfer is done in 3 steps
//	1) The data from the Buf structure as well as the directory entry is
//	transferred using rsm_put. Since there are no atomicity guarantees in
//	this step we transfer the directory entry but the state of the directory
//	entry is still RSMT_PARTITION_INUSE. Failure in this step means the
//	remote node is not going to see the message since the directory state
//	has not been updated yet.
//	2) The local directory state is updated and changed to WRDONE
//	3) The remote directory is updated with the data from the local
//	directory and now the state is set to WRDONE. If errors occur during
//	the remote directory update an attempt is made to read the directory
//	and check if the update has gone through. If the data read (genrnum)
//	as well as the state of the local directory matches, it means the
//	message has been delivered to the remote node.If the genrnum/state does
//	not match we can safely retry. Any failure in doing the read means
//	there is no way of knowing if the data update has happened and
//	COMPLETED_MAYBE is returned.
//
// If during step 1 or step 2 if persistent data xfer errors occur
// abort_endpoint() is invoked to bring down the path.
//
bool
rsmt_bio_sseg::xfer_data(int pindex, rsmt_hdr_t *rhp, Buf *bp, Environment *e)
{
	size_t	msg_size;
	off_t	pstart = partition(pindex);
	int	numprt;
	int	retrycnt = 0;
	bool	giveup = false;
	int	rc;
	hrtime_t	genrnum;
	rsm_barrier_t	bar;
	rsmt_endpoint	*ep;
	rsmt_padded_hdr	rph(rhp);

	// Check if the buffer is sufficiently padded to be a multiple
	// of cacheline so that puts can safely use the rounded up value
	// of bp->span()
	ASSERT((bp->length() % RSMT_CACHELINE) == 0);
	msg_size = ROUNDUP_CACHELINE(bp->span());

	numprt = get_numpartitions(sizeof (rsmt_padded_hdr) + msg_size);

	// There is a refcnt which was acquired during reserve_partition
	// guarding us so imseg can be used without holding s_lock.
	ASSERT(imseg != NULL);
	ASSERT(numprt <= 2);

	// update the local directory except for the state
	s_lock.lock();
	ASSERT(local_dir[pindex].ent.state == RSMT_PARTITION_INUSE);
	genrnum = os::gethrtime();
	local_dir[pindex].ent.genrnum = genrnum;
	local_dir[pindex].ent.flag |= RSMT_BDFLGS_FIRST;
	if (numprt == 1) {
		local_dir[pindex].ent.next = 0;
	} else { // msg spans two partitions
		ASSERT(local_dir[pindex+1].ent.state == RSMT_PARTITION_INUSE);
		local_dir[pindex].ent.next = (uint16_t)(pindex+1);
		local_dir[pindex+1].ent.genrnum = genrnum;
		local_dir[pindex+1].ent.flag = 0;
		local_dir[pindex+1].ent.next = 0;
	}
	s_lock.unlock();

	// Step I - put the message header and the message data.
	do {
		s_lock.lock();
		ASSERT(local_dir[pindex].ent.state == RSMT_PARTITION_INUSE);
		ASSERT(local_dir[pindex+numprt-1].ent.state ==
		    RSMT_PARTITION_INUSE);
		// check state to see if the sseg is still valid
		if (state != RSMT_BIO_SEGINIT) {
			s_lock.unlock();
			// we have not xferred the data yet and the path is
			// going down
			e->system_exception(CORBA::RETRY_NEEDED(0,
			    CORBA::COMPLETED_NO));
			RSMT_MSG_DBG_D(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
			    ("rsmt_bio_sseg(%p) xfer_data:state(%d):"
				"!RSMT_BIO_SEGINIT\n", this, state));
			return (false);
		}
		s_lock.unlock();

		if ((rc = imseg->open_barrier(&bar)) != RSM_SUCCESS) {
			// RSMERR_CONN_ABORTED is the only possible error
			ASSERT(rc == RSMERR_CONN_ABORTED);
			RSMT_MSG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
			    ("rsmt_bio_sseg(%p) open_barrier err=%d\n",
				this, rc));
			break;
		}

		RSMT_MSG_DBG_D(RSMT_DBGL_INFO, RSMT_DBGF_BUFFERIO,
		    ("xfer_data: hdr(0x%lx),data(0x%lx, %ld)\n", pstart,
			pstart + (off_t)sizeof (rph), msg_size));

		// rsm_put of rsmt_padded_hdr - the padded message header
		// rsm_put of data - the message data
		// rsm_put of dir entries- without the state update
		if ((rc = imseg->put(pstart, (void *)&rph, sizeof (rph))) ==
		    RSM_SUCCESS &&
		    (rc = imseg->put(pstart+(off_t)sizeof (rph),
			(void *)bp->head(), msg_size)) == RSM_SUCCESS &&
		    (rc = imseg->put(get_diroffset(pindex),
			&local_dir[pindex],
			(uint32_t)numprt*sizeof (rsmt_bio_dirent_t))) ==
		    RSM_SUCCESS) {
			rc = imseg->close_barrier(&bar);

			ASSERT((rc != RSMERR_BAD_BARRIER_HNDL) &&
			    (rc != RSMERR_BARRIER_NOT_OPENED) &&
			    (rc != RSMERR_BAD_BARRIER_PTR));

			if (rc == RSM_SUCCESS) {
				// go on to phase II
				break;
			} else { // close_barrier failed
				RSMT_MSG_DBG(RSMT_DBGL_INFO,
				    RSMT_DBGF_BUFFERIO,
				    ("rsmt_bio_sseg(%p) close_barrier err=%d\n",
					this, rc));
			}
		} else { // rsm_put of the data failed
				ASSERT((rc != RSMERR_BAD_SEG_HNDL) ||
				    (rc != RSMERR_BAD_MEM_ALIGNMENT) ||
				    (rc != RSMERR_BAD_LENGTH) ||
				    (rc != RSMERR_PERM_DENIED));
				RSMT_MSG_DBG(RSMT_DBGL_INFO,
				    RSMT_DBGF_BUFFERIO,
				    ("rsmt_bio_sseg(%p) rsm_put(data) err=%d\n",
					this, rc));
		}

		ASSERT(rc != RSM_SUCCESS);
		if (rc == RSMERR_CONN_ABORTED) { // permanent connection error
			break;
		} else if ((rc == RSMERR_BARRIER_FAILURE) ||
		    (rc == RSMERR_INSUFFICIENT_RESOURCES)) {// worth retrying
			retrycnt++;
			if (retrycnt >= RSMT_PUTRETRY_BACKOFF) {
				retry_timeout();
			}
		} else {
			ASSERT(0); // unexpected error
		}
	} while (true);

	if (rc != RSM_SUCCESS) { // data xfer failed
		// The data xfer failed inspite of multiple retries - path
		// seems to be in a bad state - bring it down and return
		// COMPLETED_NO so that ORB can try an alternative path.

		e->system_exception(CORBA::RETRY_NEEDED(0,
		    CORBA::COMPLETED_NO));
		s_lock.lock();
		ep = seggrp->get_endpoint();
		if (ep && (state == RSMT_BIO_SEGINIT)) {
			ep->abort_endpoint();
		}
		s_lock.unlock();
		RSMT_MSG_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_sseg(%p) put(data) err(%d:%d) "
			"abort endpoint(%p)\n", this, rc, retrycnt, ep));
		return (false);
	}

	// Step II - update the local directory state

	s_lock.lock();
	ASSERT(local_dir[pindex].ent.state == RSMT_PARTITION_INUSE);
	local_dir[pindex].ent.state = RSMT_PARTITION_WRDONE;
	if (numprt == 2) { // msg spans two partitions
		ASSERT(local_dir[pindex+1].ent.state == RSMT_PARTITION_INUSE);
		local_dir[pindex+1].ent.state = RSMT_PARTITION_WRDONE;
	}
	s_lock.unlock();

	// Step III - update the remote directory state.

	// update the remote directory entry - updates to remote directory can
	// only be retried if we know for sure that the previous update did
	// not happen. If there is any ambiguity we return a completion status
	// of COMPLETED_MAYBE since we cannot overwrite the directory that
	// the remote node might already have processed.
	retrycnt = 0;
	do {
		s_lock.lock();
		ASSERT(local_dir[pindex].ent.state == RSMT_PARTITION_WRDONE);
		ASSERT(local_dir[pindex+numprt-1].ent.state ==
		    RSMT_PARTITION_WRDONE);
		// check state to see if the sseg is still valid
		if (state != RSMT_BIO_SEGINIT) {
			s_lock.unlock();
			// we have not xferred the data yet and the path is
			// going down
			e->system_exception(CORBA::RETRY_NEEDED(0,
			    CORBA::COMPLETED_NO));
			RSMT_MSG_DBG_D(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
			    ("rsmt_bio_sseg(%p) xfer_data:state(%d):"
				"!RSMT_BIO_SEGINIT\n", this, state));
			return (false);
		}
		s_lock.unlock();

		if ((rc = imseg->open_barrier(&bar)) != RSM_SUCCESS) {
			// RSMERR_CONN_ABORTED is the only possible error
			ASSERT(rc == RSMERR_CONN_ABORTED);
			// the dirent has not been updated so msg will not
			// be seen by the remote node
			e->system_exception(CORBA::RETRY_NEEDED(0,
			    CORBA::COMPLETED_NO));
			RSMT_MSG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
			    ("rsmt_bio_sseg(%p) open_barrier(dirent) err=%d\n",
				this, rc));
			break;
		}

		// Although all we need to transfer is the directory state, it
		// is more efficient to transfer the whole directory since it
		// is multiple cacheline sized.
		rc = imseg->put(get_diroffset(pindex), &local_dir[pindex],
		    (uint32_t)numprt*sizeof (rsmt_bio_dirent_t));

		if (rc == RSM_SUCCESS) {
			if ((rc = imseg->close_barrier(&bar)) == RSM_SUCCESS) {
				return (true);
			} else { // close barrier failed
				ASSERT((rc != RSMERR_BAD_BARRIER_HNDL) &&
				    (rc != RSMERR_BARRIER_NOT_OPENED) &&
				    (rc != RSMERR_BAD_BARRIER_PTR));
				RSMT_MSG_DBG(RSMT_DBGL_ERROR,
				    RSMT_DBGF_BUFFERIO,
				    ("rsmt_bio_sseg(%p) close_barrier(dirent)"
					" err=%d\n", this, rc));
			}
		} else { // put failed
			RSMT_MSG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
			    ("rsmt_bio_sseg(%p) rsm_put(dirent) err=%d\n",
				this, rc));
		}

		ASSERT(rc != RSM_SUCCESS);

		if (rc == RSMERR_CONN_ABORTED) {
			// We can be here either due to put failure or barrier
			// failure, in either case there is no way of knowing
			// whether the put might have succeeded - the
			// message may already be on the remote node.
			// Can't do is_partition_updated() since CONN_ABORTED
			// indicates permanent error. We give up and let ORB
			// resolve it.
			e->system_exception(CORBA::RETRY_NEEDED(0,
			    CORBA::COMPLETED_MAYBE));
			return (false);
		} else if ((rc == RSMERR_BARRIER_FAILURE) ||
		    (rc == RSMERR_INSUFFICIENT_RESOURCES)) {
			RSMT_MSG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
			    ("rsmt_bio_sseg(%p) rsm_put(dirent) "
				"barrier err=%d\n", this, rc));
			// Lets us first try to do a read and check if
			// our update has made it to the remote node.
			switch (is_partition_updated(pindex, numprt, genrnum)) {
			case 0: // put succeeded - we are done
				return (true);
			case 1: // put didn't succeed - safely retry
				retrycnt++;
				if (retrycnt >= RSMT_PUTRETRY_BACKOFF) {
					retry_timeout();
				}
				break;
			case 2:	// don't know if put succeeded - maybe case
				e->system_exception(CORBA::RETRY_NEEDED(0,
				    CORBA::COMPLETED_MAYBE));
				giveup = true;
				break;
			default:
				ASSERT(0);
				break;
			}
		} else {
			ASSERT(0); // unknown errors
		}
	} while (!giveup);

	ASSERT(giveup);
	// all attempts of transferring data have been futile, the appropriate
	// exception has been set, lets mark the path down and return
	s_lock.lock();
	ep = seggrp->get_endpoint();
	if (ep && (state == RSMT_BIO_SEGINIT)) {
		ep->abort_endpoint();
	}
	s_lock.unlock();
	RSMT_MSG_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_sseg(%p) put(dir) failures abort endpoint(%p)\n", this,
		ep));
	return (false);
}

//
// reclaim() reads the partition-avail segment and updates the state
// of the directory entries. It returns the number of partitions that
// were reclaimed
//
int
rsmt_bio_sseg::reclaim()
{
	hrtime_t	*partitioninfo;
	hrtime_t	cur_genr;
	int		frprt = 0;
	int		i;

	s_lock.lock();
	// get the current generation number of the segment from
	// the pavail segment
	cur_genr = seggrp->get_pavail_seginfo(seg_index);
	partitioninfo = seggrp->get_pavail_partitioninfo(seg_index);

	// generation numbers monotonically increase
	if (cur_genr < prev_genr) { // for debugging
		RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
		    ("ERROR sidx=%d, cur=0x%llx, prev=0x%llx, pinfo(%p)\n",
			seg_index, cur_genr, prev_genr, partitioninfo));
		ASSERT(cur_genr >= prev_genr);
		s_lock.unlock();
		return (0);
	}

	// not in valid state or no new updates
	if ((state != RSMT_BIO_SEGINIT) || (cur_genr == prev_genr)) {
		s_lock.unlock();
		return (0);
	}

	// Iterate through the per-partition generation number and
	// look for partitions that have been freed - ie whose
	// generation number if greater than prev_genr and less
	// than-or-equal to the segments generation number
	for (i = 0; i < num_partitions; i++) {
		if ((partitioninfo[i] > prev_genr) &&
		    (partitioninfo[i] <= cur_genr)) {
			ASSERT(local_dir[i].ent.state ==
			    RSMT_PARTITION_WRDONE);
			// free the dir entry
			local_dir[i].ent.genrnum = 0;
			local_dir[i].ent.state = RSMT_PARTITION_FREE;
			local_dir[i].ent.flag = 0;
			local_dir[i].ent.next = 0;
			free_pindex = i;
			frprt++;
		}
	}

	RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_sseg(%p) reclaimed(%u)(0x%llx)\n", this, frprt,
		cur_genr));

	if (frprt == 0) {
		// no partitions were reclaimed
		s_lock.unlock();
		return (0);
	}

	prev_genr = cur_genr;
	free_partitions += frprt;
	s_lock.unlock();
	// update freepartitions and wakeup threads waiting for free partitions
	seggrp->update_freepartitions(frprt, true);

	return (frprt);
}

//
// Checks the state of remote direntries to validate if the put was
// successful. It checks the state of direntry specified by pindex and
// number of direntry to be checked is specified by numpart by doing
// a get and then comparing the genrnum.
//
// Returns:
//	0 - remote direntry was successfully updated
//	1 - remote direntry was successfully read but update was not
//	successful
//	2 - remote direntry could not be read - don't know if the update
//	was successful

int
rsmt_bio_sseg::is_partition_updated(int pindex, int numpart, hrtime_t genrnum)
{
	rsm_barrier_t	bar;
	rsmt_bio_dirent	dir[2];
	int		rc;
	uint32_t	retrycnt = 0;
	int		retval = 0;

	s_lock.lock();

	ASSERT((local_dir[pindex].ent.state == RSMT_PARTITION_WRDONE) &&
	    (local_dir[pindex+numpart-1].ent.state == RSMT_PARTITION_WRDONE));

	// some basic checks

	if (imseg == NULL) {
		s_lock.unlock();
		return (2);
	}
	s_lock.unlock();

	while (state == RSMT_BIO_SEGINIT) {
		if ((rc = imseg->open_barrier(&bar)) != RSM_SUCCESS) {
			RSMT_MSG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
			    ("open_barrier err=%d\n", rc));
			ASSERT(rc == RSMERR_CONN_ABORTED);
			retval = 2;
			break;
		}

#ifdef	SBUS_SCI
		// break up the dirent reads into multiple 64bit reads
		// since on sbus SCI 64-byte gets don't work
		ulong_t	repcnt = (uint32_t)numpart*sizeof (rsmt_bio_dirent_t)/
		    sizeof (uint64_t);
		// remote read of direntry
		rc = imseg->get64(get_diroffset(pindex), (uint64_t *)&dir[0],
		    repcnt);
#else
		// remote read of direntry
		rc = imseg->get(get_diroffset(pindex), (void *)&dir[0],
		    (uint32_t)numpart*sizeof (rsmt_bio_dirent_t));
#endif
		if ((rc == RSM_SUCCESS) &&
		    ((rc = imseg->close_barrier(&bar)) == RSM_SUCCESS)) {
			//
			// Get successful. Verify if remote directory was
			// updated.
			//
			if ((dir[0].ent.genrnum != genrnum) ||
			    (dir[0].ent.state == RSMT_PARTITION_INUSE)) {
				RSMT_MSG_DBG(RSMT_DBGL_INFO, RSMT_DBGF_BUFFERIO,
				    ("remote dir not updated\n"));
				retval = 1;
				break;
			}
			if ((numpart == 2) &&
			    (dir[1].ent.genrnum != genrnum) ||
			    (dir[1].ent.state == RSMT_PARTITION_INUSE)) {
				RSMT_MSG_DBG(RSMT_DBGL_INFO, RSMT_DBGF_BUFFERIO,
				    ("remote dir not updated\n"));
				retval = 1;
				break;
			}

			// Directory update was successful
			retval = 0;
			break;
		} else {
			// Get failed, retry on soft errors.

			RSMT_MSG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
			    ("remote dir get failed rc=%d\n", rc));

			if (rc == RSMERR_CONN_ABORTED) { // Unrecoverable
				retval = 2;
				break;
			} else if ((rc == RSMERR_BARRIER_FAILURE) ||
			    (rc == RSMERR_INSUFFICIENT_RESOURCES)) { // Retry
				retrycnt++;
				if (retrycnt >= RSMT_GETRETRY_BACKOFF) {
					retry_timeout();
				}
			} else { // Unexpected error
				ASSERT(0);
				retval = 2;
				break;
			}
		}
	}
	return (retval);
}

//
// This method implements a backoff mechanism for the retries of the put
// during the data transfer. This method returns either due timeout or due to
// change of state of the segment
//
void
rsmt_bio_sseg::retry_timeout()
{
	os::systime to((os::usec_t)1000);    // 1 msec

	s_lock.lock();
	while (state == RSMT_BIO_SEGINIT) {
		if (s_cv.timedwait(&s_lock, &to) == os::condvar_t::TIMEDOUT) {
			s_lock.unlock();
			RSMT_SEG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_BUFFERIO,
			    ("rsmt_bio_sseg(%p) retry timedout\n", this));
			return;
		}
	}
	s_lock.unlock();
}

//
// The bufferio receive and send segments of the same partition size are
// grouped together in segment groups. rsmt_bio_seggrp implements a segment
// group.
//
rsmt_bio_seggrp::rsmt_bio_seggrp(rsmt_bio_mgr *bmgr, int sgrpidx,
    size_t psize, int npartitions, int sindex, int nsegs):
	biomgr(bmgr),
	seggrp_index(sgrpidx),
	partition_sz(psize),
	rs_startidx(sindex),
	rs_maxpartitions(npartitions),
	rs_maxsegs(nsegs),
	ss_startidx(0),
	ss_maxpartitions(0),
	ss_maxsegs(0),
	free_partitions(0),
	consumed_partitions(0),
	partition_lotsfree(0),
	reclaim_pending(false),
	initiated(false),
	ssegs(NULL)
{
	int i;

	segmgr = biomgr->get_segmgr();
#ifdef	DEBUG
	// initiatize counters
	for (i = 0; i < 2; i++) {
		partitions_per_msg[i] = 0;
	}
#endif
	// rs_startidx should be a multiple of RSMT_GENRPERCACHELINE
	ASSERT(rs_startidx%RSMT_GENRPERCACHELINE == 0);

	// allocate and initialize the receive segment arrays
	rsegs = new (rsmt_bio_rseg *[(uint32_t)nsegs]);
	ASSERT(rsegs);
	for (i = 0; i < nsegs; i++) {
		rsegs[i] = NULL;
	}

	// number of entries in the generation number array that each element
	// of shadow_pavail points to is rounded up so that size of the
	// per receive segment genr number array is a multiple of cacheline
	// size. The size is same for all receive segments and does not depend
	// on the number of actual partitions.
	rs_shdw_pavail_nument = ROUNDUP(rs_maxpartitions,
	    RSMT_GENRPERCACHELINE);

	shadow_pavail = new (hrtime_t *[(uint32_t)nsegs]);
	ASSERT(shadow_pavail);
	for (i = 0; i < nsegs; i++) {
		shadow_pavail[i] = NULL;
	}
	// number of entries in the last_freed array for a seggrp is rounded
	// so that its size is a multiple of cacheline size.
	last_freed_nument = ROUNDUP(nsegs, RSMT_GENRPERCACHELINE);
	last_freed = new hrtime_t[(uint32_t)last_freed_nument];
	for (i = 0; i < last_freed_nument; i++) {
		last_freed[i] = 0;
	}
	RSMT_SEG_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_seggrp(%p) biomgr(%p) constructed\n", this, bmgr));
}

// Destructor
rsmt_bio_seggrp::~rsmt_bio_seggrp()
{
	RSMT_SEG_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_seggrp(%p) destructed\n", this));

	if (last_freed) {
		delete [] last_freed;
	}

	if (shadow_pavail) {
		for (int i = 0; i < rs_maxsegs; i++) {
			if (shadow_pavail[i]) {
				delete [] shadow_pavail[i];
			}
		}
		delete [] shadow_pavail;
	}
	if (rsegs) {
		delete [] rsegs;
	}
	if (ssegs) {
		delete [] ssegs;
	}
	biomgr = NULL;
	segmgr = NULL;
	partition_sz = 0;
	rs_startidx = 0;
	rs_maxpartitions = 0;
	rs_maxsegs = 0;
	ss_startidx = 0;
	ss_maxpartitions = 0;
	ss_maxsegs = 0;
	free_partitions = 0;
	consumed_partitions = 0;
	rs_shdw_pavail_nument = 0;
}


//
// Called during the processing of PAVAIL_CONNECT message. This method
// allocates the send segment list and prepopulates it with one send segment.
//
void
rsmt_bio_seggrp::init_ssegs(int start_index, int maxsegs, int maxpartitions)
{
	rsmt_bio_sseg	**tmp;
	rsmt_bio_sseg	*ssegp;

	// either these values should be zero or if we recv multiple
	// PAVAIL_CONNECT message it should be the same value each time.
	ASSERT((ss_maxpartitions == 0) ||
	    (ss_maxpartitions == maxpartitions));
	ASSERT((ss_startidx == 0) || (ss_startidx == start_index));
	ASSERT((ss_maxsegs == 0) || (ss_maxsegs == maxsegs));

	// start_index should be a multiple of RSMT_GENRPERCACHELINE
	ASSERT(start_index%RSMT_GENRPERCACHELINE == 0);

	sg_lock.lock();
	if (!ssegs) {		// allocate ssegs if it doesn't exist
		sg_lock.unlock();
		tmp = new (rsmt_bio_sseg *[(uint32_t)maxsegs]);
		ASSERT(tmp);
		for (int i = 1; i < maxsegs; i++) {
			tmp[i] = NULL;
		}
		// prepopulate the segment group
		// add the first send segment right here
		ssegp = new rsmt_bio_sseg(start_index, maxpartitions, this,
		    segmgr);
		ASSERT(ssegp);
		sg_lock.lock();
		ssegs = tmp;
		ssegs[0] = ssegp;
	}

	ss_startidx = start_index;
	ss_maxpartitions = maxpartitions;
	ss_maxsegs = maxsegs;
	sg_lock.unlock();

	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_seggrp(%p) init_ssegs sindx(%d),mxprt(%d),mxsegs(%d)\n",
		this, ss_startidx, ss_maxpartitions, ss_maxsegs));
	RSMT_PATH_DBG(RSMT_DBGL_VERBOSE, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_seggrp(%p) init_ssegs done\n", this));
}

//
// This gets called from the rsmt_bio_mgr::initiate_fini(). The seggrp
// is prepopulated with one receive segment. We also wait for the first
// send segment to be initiated. If inititate returns true it means that
// the seggrp has both receive and send segment ready to use.
//
bool
rsmt_bio_seggrp::initiate()
{
	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_seggrp(%p) initiate\n", this));

	sg_lock.lock();
	if (initiated) {
		sg_lock.unlock();
		return (true);
	}
	sg_lock.unlock();

	// add the first receive segment
	if (!add_rseg(rs_maxpartitions, rs_startidx)) {
		return (false);
	}

	// now wait for the prepopulated send segment to connect to the
	// receive segments on the remote node
	if (!ssegs[0]->wait_for_initiate()) {
		return (false);
	}

	sg_lock.lock();
	initiated = true;
	sg_lock.unlock();

	return (true);
}

//
// Cleanup the seggrp, called from the rsmt_bio_mgr::cleanup_fini_pre_sm().
// Starts the shutdown of segment communication infrastructure, but does
// not initiate destructing rsegs yet.
//
void
rsmt_bio_seggrp::cleanup_pre_sm()
{
	rsmt_bio_sseg *ssegp;
	rsmt_bio_rseg *rsegp;
	int i;

	sg_lock.lock();
	initiated = false;
	sg_cv.broadcast();
	sg_lock.unlock();

	seglist_lock.wrlock();

	for (i = 0; i < rs_maxsegs; i++) {
		if (rsegs[i]) {
			rsegp = rsegs[i];
			seglist_lock.unlock();
			// shouldn't hold seglist_lock during rseg cleanup
			// since it might need to acquire sg_lock.
			if (rsegp) {
				rsegp->cleanup();
			}
			seglist_lock.wrlock();
		}
	}

	for (i = 0; i < ss_maxsegs; i++) {
		if (ssegs[i]) {
			ssegp = ssegs[i];
			ssegs[i] = NULL;
			seglist_lock.unlock();
			// shouldn't hold seglist_lock during sseg cleanup
			// since it might need to acquire sg_lock.
			if (ssegp &&
			    (ssegp != (rsmt_bio_sseg *)RSMT_BIO_SIDXRESVD)) {
				ssegp->cleanup();
				ssegp->rele();
			}
			seglist_lock.wrlock();
		}
	}
	seglist_lock.unlock();
}

//
// Cleanup the seggrp, called from the rsmt_bio_mgr::cleanup_fini_post_sm().
// Release the rsegs.
//
void
rsmt_bio_seggrp::cleanup_post_sm()
{
	rsmt_bio_rseg *rsegp;
	int i;

	seglist_lock.wrlock();

	for (i = 0; i < rs_maxsegs; i++) {
		if (rsegs[i]) {
			rsegp = rsegs[i];
			rsegs[i] = NULL;
			seglist_lock.unlock();
			// shouldn't hold seglist_lock during rseg cleanup
			// since it might need to acquire sg_lock.
			if (rsegp) {
				rsegp->rele();
			}
			seglist_lock.wrlock();
		}
	}
	seglist_lock.unlock();
}

//
// add_sseg creates an rsmt_bio_sseg and adds it to the list of send
// segments in the segment group. This is called when we want to increase
// the number of the send segments in the segment group. Returns the pointer
// to the send segment if successful otherwise NULL is returned.
//
rsmt_bio_sseg *
rsmt_bio_seggrp::add_sseg(int npartitions)
{
	rsmt_bio_msg_t	crt_msg;
	rsmt_bio_sseg	*ssegp;
	int		i;

	if (npartitions > ss_maxpartitions) {
		RSMT_PATH_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_seggrp(%p) add_ssegs npartitions>>\n", this));
		return (NULL);
	}

	sg_lock.lock();
	// check state
	if (!initiated) {
		sg_lock.unlock();
		return (NULL);
	}
	// grab the write lock to ensure updates to sseg is atomic
	seglist_lock.wrlock();
	sg_lock.unlock();

	for (i = 0; i < ss_maxsegs; i++) {
		if (ssegs[i] == NULL) {
			ssegs[i] = (rsmt_bio_sseg *)RSMT_BIO_SIDXRESVD;
			break;
		}
	}

	seglist_lock.unlock();

	if (i == ss_maxsegs) { // reached the max number of segments
		return (NULL);
	}

	ssegp = new rsmt_bio_sseg(ss_startidx+i, npartitions, this, segmgr);
	ASSERT(ssegp);

	sg_lock.lock();
	seglist_lock.wrlock();

	if (!initiated) {
		seglist_lock.unlock();
		sg_lock.unlock();
		return (NULL);
	}
	ssegs[i] = ssegp;
	seglist_lock.unlock();
	sg_lock.unlock();

	// send a control message to the remote node so that a receive
	// segment corresponding to this send segment gets created.
	// this is a flow controlled message - since we need to
	// create an export segment in response to this message

	crt_msg.u.bcr_msg.flags = 0;
	crt_msg.u.bcr_msg.nseggrp = (uchar_t)seggrp_index;
	crt_msg.u.bcr_msg.segindex = (ushort_t)(ss_startidx+i);
	crt_msg.u.bcr_msg.partitions = (ushort_t)npartitions;

	if (!biomgr->cm_send(&crt_msg, sizeof (crt_msg), RSMT_BIO_SEGCREATE,
	    true)) {
		seglist_lock.wrlock();
		ssegs[i] = NULL;
		seglist_lock.unlock();
		ssegp->rele();
		RSMT_SEG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_seggrp(%p) sseg(%d) send(SEGCREATE) failed\n",
			this, ss_startidx+i));
		return (NULL);
	}

	RSMT_SEG_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_seggrp(%p) add_ssegs(%d) %p \n", this, ss_startidx+i,
		ssegp));

	return (ssegp);
}

//
// Remove the sseg from the list
//
void
rsmt_bio_seggrp::remove_sseg(rsmt_bio_sseg *ssegp)
{
	ASSERT(ssegp);

	if (!ssegp || (ssegp == (rsmt_bio_sseg *)RSMT_BIO_SIDXRESVD)) {
		return;
	}

	RSMT_SEG_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_seggrp(%p) remove_ssegs(%d) %p \n", this,
		ssegp->get_segindex(), ssegp));

	seglist_lock.wrlock();
	ssegs[ssegp->get_segindex()-ss_startidx] = NULL;
	seglist_lock.unlock();

	ssegp->cleanup();
	ssegp->rele();
}

//
// Return the sseg for a given segment index - a hold is placed on the
// send segment returned
//
rsmt_bio_sseg *
rsmt_bio_seggrp::get_sseg(int segindex)
{
	rsmt_bio_sseg *p = NULL;

	seglist_lock.rdlock();

	if (ssegs) {
		p = ssegs[segindex-ss_startidx];
		if (p) {
			p->hold();
		}
	}
	seglist_lock.unlock();

	return (p);
}

//
// Add a recv seg for the specified segindex, then call rsmt_bio_rseg::initiate
// Once this is done send the RSMT_BIO_SEGCONNECT to the remote node so that
// the send segment can connect this receive segment
//
rsmt_bio_rseg *
rsmt_bio_seggrp::add_rseg(int npartitions, int segindex)
{
	rsmt_bio_msg_t		con_msg;
	rsmt_seg_conn_token_t	conn_tok;
	rsmt_bio_rseg		*rsegp;
	int			sindex = segindex - rs_startidx;
	int			i;

	ASSERT(npartitions <= rs_maxpartitions);

	seglist_lock.rdlock();
	if (!rsegs[sindex]) {
		seglist_lock.unlock();
		rsegp = new rsmt_bio_rseg(segindex, npartitions, this, segmgr);
		ASSERT(rsegp);
	} else {
		rsegp = rsegs[sindex];
		seglist_lock.unlock();
	}

	if (rsegp->initiate(&conn_tok)) {
		// create of receive segment done
		sg_lock.lock();
		seglist_lock.rdlock();
		if (!rsegs[sindex]) { // rseg was just allocated
			rsegs[sindex] = rsegp;
			partition_lotsfree +=
			    (RSMT_PARTITION_LOTSFREE*npartitions/100);
			shadow_pavail[sindex] = new
			    hrtime_t[(uint32_t)rs_shdw_pavail_nument];
			for (i = 0; i < rs_shdw_pavail_nument; i++) {
				shadow_pavail[sindex][i] = 0;
			}
			ASSERT(shadow_pavail[sindex]);
		}
		seglist_lock.unlock();
		sg_lock.unlock();
	} else {
		seglist_lock.rdlock();
		// If this rseg was just allocated and is not
		// yet in the rseg list cleanup and return
		if (!rsegs[sindex]) {
			seglist_lock.unlock();
			rsegp->cleanup();
			rsegp->rele();
		} else {
			seglist_lock.unlock();
		}
		return (NULL);
	}

	// send the RSMT_BIO_SEGCONNECT message, if initiate() fails
	// the conn_tok is an invalid token and the remote node will
	// see that and know that the RSMT_BIO_SEGCREATE was not
	// successful.

	con_msg.u.bco_msg.tok = conn_tok;
	con_msg.u.bco_msg.flags = 0;
	con_msg.u.bco_msg.nseggrp = (uchar_t)seggrp_index;
	con_msg.u.bco_msg.segindex = (ushort_t)segindex;
	con_msg.u.bco_msg.partitions = (ushort_t)npartitions;

	if (!biomgr->cm_send(&con_msg, sizeof (con_msg), RSMT_BIO_SEGCONNECT,
	    true)) {
		seglist_lock.wrlock();
		rsegs[sindex] = NULL;
		seglist_lock.unlock();
		rsegp->rele();
		RSMT_SEG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_seggrp(%p) add_rseg(%d) send(SEGCONNECT)"
			" failed\n", this, segindex));
		return (NULL);
	}

	RSMT_SEG_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_seggrp(%p) add_rseg(%d) %p\n", this, segindex, rsegp));

	return (rsegp);
}

//
// Remove the rseg from the list
//
void
rsmt_bio_seggrp::remove_rseg(rsmt_bio_rseg *rsegp)
{
	ASSERT(rsegp);

	if (!rsegp) {
		return;
	}

	RSMT_SEG_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_seggrp(%p) remove_rseg(%d) %p\n", this,
		rsegp->get_segindex(), rsegp));

	seglist_lock.wrlock();
	rsegs[rsegp->get_segindex()-rs_startidx] = NULL;
	seglist_lock.unlock();

	rsegp->cleanup();

	//
	// XXX Add code to continue pushing any arriving messages on this
	// segment until the underlying export segment gets unpublished.
	// This must be done before before remove_rseg is put to use.
	// Currently this code is not in use.
	//

	rsegp->rele();
}

//
// Return the rseg for a given segment index - a hold is placed on the
// recv segment returned
//
rsmt_bio_rseg *
rsmt_bio_seggrp::get_rseg(int segindex)
{
	rsmt_bio_rseg *p = NULL;

	seglist_lock.rdlock();
	if ((p = rsegs[segindex-rs_startidx]) != NULL) {
		p->hold();
	}
	seglist_lock.unlock();

	return (p);
}

//
// free_resources is called when the endpoint wants to release
// resources held by this segment group. The segment group does
// a best effort free up
//
void
rsmt_bio_seggrp::free_resources()
{
}

//
// This method reserves partition(s) in the segment group for a
// message. The size of the message is passed as the argument,
// a pointer to rsmt_bio_sseg is returned in the second argument
// and the partition-index of the first reserved partition
// is returned in the last argument if successful.
//
bool
rsmt_bio_seggrp::reserve_partitions(size_t msgsz, rsmt_bio_sseg **segp,
    int *partition)
{
	int i;
	int numprt = get_numpartitions(msgsz);
	rsmt_bio_sseg *sendseg;

	*segp = NULL;
	*partition = 0;

	if (numprt > 2) {
		RSMT_MSG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_seggrp(%p) rsrv_prttions > 2 0x%lx\n", this,
			msgsz));
		return (false);
	}

	seglist_lock.rdlock();
	// search for free partitions in each send segment in the seggrp
	for (i = 0; i < ss_maxsegs; i++) {
		sendseg = ssegs[i];
		if (sendseg) {
			*partition = sendseg->alloc(msgsz);
			RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_BUFFERIO,
			    ("rsmt_bio_seggrp(%p) reserved partitions(%d)\n",
				this, *partition));
			if (*partition >= 0) {
				*segp = sendseg;
				seglist_lock.unlock();
#ifdef	DEBUG
				// update statistics counter
				atomic_add_64(&partitions_per_msg[numprt-1],
				    (int64_t)1);
#endif
				return (true);
			}
		}
	}
	seglist_lock.unlock();
	RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_seggrp(%p) reserve_partitions err\n", this));
	return (false);
}

//
// Block till some partitions become available
//
void
rsmt_bio_seggrp::wait_for_freepartitions()
{
	RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_seggrp(%p) wait_for_freepartitions(%d)\n", this,
		seggrp_index));
	sg_lock.lock();
	while ((free_partitions == 0) && initiated) {
		RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_seggrp(%p) waiting for freepartitions(%d)\n",
			this, free_partitions));
		sg_cv.wait(&sg_lock);
	}
	sg_lock.unlock();
}

//
// This method is called to push marshalled messages in the
// partitions of this segment group to the ORB.
// Return value is a completion status. False indicates cleanup has started
// and more messages are possible. Return value is true otherwise.
// Return value is always true in non cleanup cases.
//
bool
rsmt_bio_seggrp::push()
{
	bool	allpushed = true;

	// Loop through all the receive segments and push() messages
	// in each of them

	seglist_lock.rdlock();
	for (int i = 0; i < rs_maxsegs; i++) {
		if (rsegs[i]) {
			if (!rsegs[i]->push()) {
				allpushed = false;
			}
		}
	}
	seglist_lock.unlock();
	return (allpushed);
}


//
// Free partition(s) beginning at specified partition index. The arguments
// are the segment index, partition index and number of partitions to be
// freed. This is called on the receive side after the message has been
// delivered to the ORB.
//
void
rsmt_bio_seggrp::msg_consumed(int segindex, int pindex, int num_prt)
{
	int	sindex = segindex - rs_startidx;
	int	old_consumed_partitions;
	hrtime_t	now;

	RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_seggrp(%p) consumed(%d,%d)\n", this, pindex, num_prt));

	sg_lock.lock();
	consumed_partitions += num_prt;
	// hold this to protect shadow_pavail as well as serialize updates
	// to the remote pavail-segment
	updavail_lock.lock();
	now = os::gethrtime();
	// update shadow_pavail
	for (int i = 0; i < num_prt; i++) {
		shadow_pavail[sindex][pindex+i] = now;
	}

	last_freed[sindex] = now;

	// check the threshold and update remote pavail
	if ((consumed_partitions >= partition_lotsfree) &&
	    !reclaim_pending) {
		old_consumed_partitions = consumed_partitions;
		consumed_partitions = 0;
		reclaim_pending = true;
		sg_lock.unlock();
		if (!biomgr->update_pavail(seggrp_index, shadow_pavail,
		    rs_maxsegs,
		    (uint32_t)rs_shdw_pavail_nument*sizeof (hrtime_t),
		    rs_startidx, last_freed,
		    (uint32_t)last_freed_nument*sizeof (hrtime_t))) {
			// drop updavail_lock before acquiring sg_lock
			updavail_lock.unlock();
			sg_lock.lock();
			consumed_partitions += old_consumed_partitions;
			reclaim_pending = false;
			sg_lock.unlock();
		} else {
			updavail_lock.unlock();
		}
		return;
	}
	updavail_lock.unlock();
	sg_lock.unlock();
}

//
// reclaim consumed partitions and mark them as free
//
void
rsmt_bio_seggrp::reclaim()
{
	int	partitions_reclaimed = 0;
	rsmt_bio_msg_t	brpa_msg;

	seglist_lock.rdlock();
	for (int i = 0; i < ss_maxsegs; i++) {
		if (ssegs[i] && (ssegs[i] !=
		    (rsmt_bio_sseg *)RSMT_BIO_SIDXRESVD)) {
			partitions_reclaimed += ssegs[i]->reclaim();
		}
	}
	seglist_lock.unlock();

	ASSERT(partitions_reclaimed > 0);
	RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_seggrp(%p) sgrpid(%d) reclaimed(%d)\n", this,
		seggrp_index, partitions_reclaimed));
	brpa_msg.u.brpa_msg.seggrpindex = (uchar_t)seggrp_index;
	//
	// send a control message to indicate reclaim processing has been
	// completed
	//
	(void) biomgr->cm_send(&brpa_msg, sizeof (brpa_msg),
	    RSMT_BIO_RECLAIM_PRTACK, true);
}

//
// This method is called when the reclaim partition ack is received.
//
void
rsmt_bio_seggrp::reclaim_completed()
{
	int	old_consumed_partitions;

	sg_lock.lock();
	ASSERT(reclaim_pending);
	updavail_lock.lock();
	//
	// check if while the sender was reclaiming partitions more
	// partitions were freed in which case update remote pavail
	//
	if (consumed_partitions >= partition_lotsfree) {
		old_consumed_partitions = consumed_partitions;
		consumed_partitions = 0;
		sg_lock.unlock();
		if (!biomgr->update_pavail(seggrp_index, shadow_pavail,
		    rs_maxsegs,
		    (uint32_t)rs_shdw_pavail_nument*sizeof (hrtime_t),
		    rs_startidx, last_freed,
		    (uint32_t)last_freed_nument*sizeof (hrtime_t))) {
			// drop updavail_lock before acquiring sg_lock
			updavail_lock.unlock();
			sg_lock.lock();
			consumed_partitions += old_consumed_partitions;
			reclaim_pending = false;
			sg_lock.unlock();
		} else {
			updavail_lock.unlock();
		}
		return;
	}
	reclaim_pending = false;
	updavail_lock.unlock();
	sg_lock.unlock();
}

//
// Update the free_partitions counter and wake up threads waiting for
// free_partitions.
//
void
rsmt_bio_seggrp::update_freepartitions(int delta, bool signal_reqd)
{
	sg_lock.lock();
	free_partitions += delta;
	if (signal_reqd) {
		sg_cv.broadcast();
	}
	sg_lock.unlock();

}

//
// Get the endpoint to which this seggrp is associated with
//
rsmt_endpoint *
rsmt_bio_seggrp::get_endpoint()
{
	return (biomgr->get_endpoint());
}

//
// Get the generation number from the per-segment part of the local
// pavail-segment for the given segindex
//
hrtime_t
rsmt_bio_seggrp::get_pavail_seginfo(int	segindex)
{
	hrtime_t *seginfo = biomgr->get_pavail_seginfo();

	return (seginfo[segindex]);
}

//
// Get the generation number array from the per-partition part of the local
// pavail-segment for the given segindex
//
hrtime_t *
rsmt_bio_seggrp::get_pavail_partitioninfo(int segindex)
{
	hrtime_t	*partitioninfo;
	int		sindex = segindex - ss_startidx;

	partitioninfo = biomgr->get_pavail_partitioninfo(seggrp_index);

	partitioninfo = partitioninfo + sindex*ss_maxpartitions;

	return (partitioninfo);
}
