//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rsmt_cmf.cc	1.11	08/05/20 SMI"

#include <orb/infrastructure/common_threadpool.h>

#include "rsmt_cmf.h"

// static members of rsmt_cmf_mgr
rsmt_cmf_mgr *rsmt_cmf_mgr::the_rsmt_cmf_mgr = NULL;

//
// the cmf interrupt handler
//
extern "C" rsm_intr_hand_ret_t rsmt_cmf_intr_handler(
    rsm_controller_object_t *cntlp, rsm_intr_q_op_t operation,
    rsm_addr_t sender, void *data, size_t size, rsm_intr_hand_arg_t arg)
{
	rsmt_cmf_adapter *adp = (rsmt_cmf_adapter *)arg;

	// satisfy the compiler
	cntlp = cntlp;
	size = size;
	sender = sender;

	switch (operation) {
	case RSM_INTR_Q_OP_CREATE:
		// sendq create - noop
		break;
	case RSM_INTR_Q_OP_DESTROY:
		// sendq destroy - noop
		break;
	case RSM_INTR_Q_OP_RECEIVE:
		// receive a message
		ASSERT(data && adp);
		// deliver it to the adapter which
		// forwards it to the approp cmf_client
		adp->deliver(data);
		break;
	default:
		return (RSM_INTR_HAND_UNCLAIMED);
	}
	return (RSM_INTR_HAND_CLAIMED);
}

// defer task which redelivers pending messages
rsmt_cmf_defer_task::rsmt_cmf_defer_task(rsmt_cmf_mgr *mgrp):
	cmf_mgrp(mgrp)
{
}

void
rsmt_cmf_defer_task::execute()
{
	cmf_mgrp->redeliver();
}

//
// rsmt_cmf_adapter is instantiated by the rsmt_cmf_mgr, when the
// transport registers an adapter. It takes an adapter_id and a
// pointer to the rsm_controller_object_t which is obtained through
// rsm_get_controller(). Each adapter uses one rsm_intr_service_t number.
//
rsmt_cmf_adapter::rsmt_cmf_adapter(int adpid, rsm_controller_object_t *ctlr):
	cma_rsmadp_ctlr(ctlr),
	cma_adapter_id(adpid),
	cma_handler(rsmt_cmf_intr_handler),
	cma_clients_count(0),
	cma_retry_clients_count(0)
{
	int	rc;

	// init the clients array
	for (int i = 1; i <= NODEID_MAX; i++) {
		for (int j = 0; j < N_CMFCLIENTS; j++) {
			cma_clients[i][j] = NULL;
		}
	}


	// calculate the service number
	cma_rsmserv = rsmt_cmf_mgr::get_servicenum(adpid);

	// get the controller attributes and save it
	// XXXX: move this to rsmt_adapter class
	rc = rsm_get_controller_attr(ctlr->handle, &cma_rsmadp_attr);

	if (rc != RSM_SUCCESS) {
		RSMT_TRANS_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_CMF,
		    ("rsmt_cmf_adapter(%p): get_controller_attr failed=%d\n",
		    this, rc));
		ASSERT(0);
	}
}

rsmt_cmf_adapter::~rsmt_cmf_adapter()
{
	cma_rsmadp_ctlr = NULL;
	cma_rsmadp_attr	= NULL;
	cma_handler = NULL;
}

//
// A client registers itself on an adapter before it can start using
// the control message framework. After register_client is successful
// it can start receiving interrupts aka control messages.
//
void
rsmt_cmf_adapter::register_client(nodeid_t rem_node, cmf_clientid_t clientid,
    rsmt_cmf_client *clientp)
{
	ASSERT(clientp);

	cma_client_lock.wrlock();

	cma_clients[rem_node][clientid] = clientp;
	cma_clients_count++;

	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_CMF,
	    ("rsmt_cmf_adapter(%p) client=0x%p registered\n", this, clientp));

	cma_client_lock.unlock();
}

//
// unregister_client - detaches the specified client from the rsmt_cmf_adapter.
// After unregister_client succeeds it no longer can receive control messages.
//
void
rsmt_cmf_adapter::unregister_client(nodeid_t rem_node, rsmt_cmf_client *clientp)
{
	cmf_clientid_t	clientid;

	ASSERT(clientp);

	clientid = clientp->get_clientid();

	cma_client_lock.wrlock();

	ASSERT(cma_clients[rem_node][clientid] == clientp);
	cma_clients[rem_node][clientid] = NULL;
	cma_clients_count--;

	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_CMF,
	    ("rsmt_cmf_adapter(%p) client=0x%p unregistered\n", this, clientp));

	cma_client_lock.unlock();
}

//
// This method is called by rsmt_cmf_intr_handler - the interrupt handler.
// The control message is forwarded to the appropriate client based on
// the remote nodeid and the clientid.
//
void
rsmt_cmf_adapter::deliver(void *msgp)
{
	rsmt_cmhdr_t *hdrp = (rsmt_cmhdr_t *)msgp;
	rsmt_cmf_client *client;

	cma_client_lock.rdlock();
	client = cma_clients[hdrp->hdr.src][hdrp->hdr.clientid];
	if (client) {
		client->hold();
		cma_client_lock.unlock();
	} else { // stray message - drop it
		RSMT_MSG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_CMF,
		    ("rsmt_cmf_adapter(%p): deliver no such client\n", this));
		cma_client_lock.unlock();
		return;
	}

	client->cm_deliver(msgp);

	client->rele();
}

//
// This method is called from the defered task that tries to redeliver
// pending flow-controlled messages to the clients.
//
void
rsmt_cmf_adapter::redeliver()
{
	rsmt_cmf_client	*clientp;
	int i, j;

	// Nothing to redeliver
	if (cma_retry_clients_count == 0) {
		return;
	}

	cma_client_lock.rdlock();
	// run through the clients table and call redeliver for every client
	for (i = 1; i <= NODEID_MAX; i++) {
		for (j = 0; j < N_CMFCLIENTS; j++) {
			clientp = cma_clients[i][j];
			if (clientp != NULL) {
				(void) clientp->cm_redeliver();
			}
		}
	}
	cma_client_lock.unlock();
}

//
// The rsmt_cmf_mgr - this is a static object, there is one instance of
// this class and it is created by the rsmt_transport class.
//
rsmt_cmf_mgr::rsmt_cmf_mgr():
	cmmgr_shutdown(false),
	cmmgr_taskactive(false),
	cmmgr_retry_adapter_count(0)
{
	// init the adapters array
	for (int i = 1; i <= RSMT_MAX_ADAPTERS; i++) {
		cmmgr_adapters[i] = NULL;
	}

	cmmgr_retry_task = new rsmt_cmf_defer_task(this);

	// start off the retry_task
	common_threadpool::the().defer_processing(cmmgr_retry_task);

	RSMT_TRANS_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_CMF,
	    ("rsmt_cmf_mgr(%p): constructed\n", this));
}

rsmt_cmf_mgr::~rsmt_cmf_mgr()
{
	RSMT_TRANS_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_CMF,
	    ("rsmt_cmf_mgr(%p): destructed\n", this));

	delete cmmgr_retry_task;
}

//
// initialize the cmf manager, there is one manager on each node.
//
int
rsmt_cmf_mgr::initialize()
{
	// should be called only once
	ASSERT(the_rsmt_cmf_mgr == NULL);

	// instantiate the manager object
	the_rsmt_cmf_mgr = new rsmt_cmf_mgr();
	if (the_rsmt_cmf_mgr == NULL) {
		return (ENOMEM);
	}

	RSMT_TRANS_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_CMF,
	    ("rsmt_cmf_mgr: initialized successfully\n"));

	return (0);
}

//
// cleanup and shutdown the cmf manager
//
void
rsmt_cmf_mgr::shutdown()
{
	ASSERT(the_rsmt_cmf_mgr != NULL);

	// coordinate the shutdown with the retry task thread
	cmmgr_lock.lock();
	cmmgr_shutdown = true;
	cmmgr_cv.signal();
	while (cmmgr_taskactive) {
		cmmgr_cv.wait(&cmmgr_lock);
	}
	cmmgr_lock.unlock();

	delete the_rsmt_cmf_mgr;
	the_rsmt_cmf_mgr = NULL;

	RSMT_TRANS_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_CMF,
	    ("rsmt_cmf_mgr(%p): shutdown successfully\n", this));
}

//
// Called during the instantiation of rsmt_adapter, this happens
// in the rsmt_transport::new_adapter method.
// The interrupt handler is registered here.
//
void
rsmt_cmf_mgr::register_adapter(int adp_id, rsm_controller_object_t *ctlr)
{
	rsmt_cmf_adapter	*adp;
	int			rc;

	// validate the range of adp_id
	ASSERT(adp_id > 0 && adp_id <= RSMT_MAX_ADAPTERS);

	// create a new adapter object
	adp = new rsmt_cmf_adapter(adp_id, ctlr);

	cmmgr_lock.lock();
	ASSERT(cmmgr_adapters[adp_id] == NULL);

	cmmgr_adapters[adp_id] = adp; // add it to the adapters list
	RSMT_TRANS_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_CMF,
	    ("rsmt_cmf_mgr(%p): registered adp(%d) = %p\n", this, adp_id, adp));
	cmmgr_lock.unlock();

	// register the interrupt handler now - we don't want the handler
	// to get called before the adapter has been put in cmmgr_adapters
	// the handler_arg is a pointer to rsmt_cmf_adapter.
	// Last two arguments the sender list and its length are NULL, 0
	// all nodes are allowed to send messages to us.
	rc = RSM_REGISTER_HANDLER(*ctlr, adp->get_servicenum(),
		rsmt_cmf_intr_handler, (rsm_intr_hand_arg_t)adp,
		NULL, 0);

	RSMT_TRANS_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_CMF,
	    ("rsmt_cmf_mgr(%p):register_adapter(%p) handler for %d = %d\n",
		this, adp, adp->get_servicenum(), rc));

	ASSERT(rc == RSM_SUCCESS); // no errors should happen
}

//
// Called when a rsmt_adapter is deleted.
// The interrupt handler is unregistered here.
//
void
rsmt_cmf_mgr::unregister_adapter(int adp_id)
{
	rsmt_cmf_adapter	*adp;
	rsm_controller_object_t	*ctlr;

	ASSERT(adp_id > 0 && adp_id <= RSMT_MAX_ADAPTERS);

	cmmgr_lock.lock();

	adp = cmmgr_adapters[adp_id];
	ASSERT(adp != NULL);

	ctlr = adp->get_controller();

	// unregister the handler
	(void) RSM_UNREGISTER_HANDLER(*ctlr,
		adp->get_servicenum(), rsmt_cmf_intr_handler,
		(rsm_intr_hand_arg_t)adp);

	RSMT_TRANS_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_CMF,
	    ("rsmt_cmf_mgr(%p):unregister_adapter(%p) handler(%d)\n", this,
	    adp, adp->get_servicenum()));

	//
	// Release the refcnt, this release corresponds to the implicit
	// hold by the constructor. adapter gets deleted when refcnt comes
	// down to zero.
	//
	cmmgr_adapters[adp_id]->rele();

	cmmgr_adapters[adp_id] = NULL;
	RSMT_TRANS_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_CMF,
	    ("rsmt_cmf_mgr(%p): unregister adpid %d \n", this, adp_id));
	cmmgr_lock.unlock();
}

//
// place a hold on the adapter and return a pointer to the adapter
// for the requested adapter_id
//
rsmt_cmf_adapter *
rsmt_cmf_mgr::get_adapter(int adp_id)
{
	rsmt_cmf_adapter	*adp;

	ASSERT(adp_id > 0 && adp_id <= RSMT_MAX_ADAPTERS);

	cmmgr_lock.lock();

	adp = cmmgr_adapters[adp_id];
	if (adp != NULL) {
		adp->hold(); // place a hold on the adapter
	}

	cmmgr_lock.unlock();

	return (adp);
}

//
// The retry_task calls this routine when the defer task is posted onto
// the common threadpool. Loop through all adapters and call redeliver.
//
void
rsmt_cmf_mgr::redeliver()
{
	int		i;

	cmmgr_lock.lock();
	cmmgr_taskactive = true;

	while (!cmmgr_shutdown) {

		// XXX: need to do a conditional redelivery
		// if (cmmgr_retry_adapter_count > 0) {
		for (i = 1; i <= RSMT_MAX_ADAPTERS; i++) {
			if (cmmgr_adapters[i] != NULL) {
				cmmgr_adapters[i]->redeliver();
			}
		}
		cmmgr_lock.unlock();
		// XXX: Sleep for 100us - need to tune
		os::usecsleep((os::usec_t)100);
		cmmgr_lock.lock();
	}

	cmmgr_taskactive = false;
	cmmgr_cv.signal();
	cmmgr_lock.unlock();
}
