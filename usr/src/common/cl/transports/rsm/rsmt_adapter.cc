//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rsmt_adapter.cc	1.22	08/05/20 SMI"

#include "rsmt_util.h"
#include "rsmt_adapter.h"
#include "rsmt_cmf.h"

// Create an adpater object.  With the existing interface to
// new_adapter() (our caller) there is no way to return an error.
// If we find a problem we set rad_rsm_ctrl.handle to NULL and
// rsmt_pathend::initiate() checks for that and blocks waiting for
// the path to be deleted.
//
rsmt_adapter::rsmt_adapter(rsmt_transport *trp,
    clconf_adapter_t *cl_adapter) :
	adapter(trp, cl_adapter),
	attrp(NULL)
{
	const char *adpname;
	int rc;

	// Get the adapter name and device instance from clconf
	// for this adapter.
	//
	adpname = clconf_adapter_get_device_name(cl_adapter);
	ASSERT(adpname != NULL);
	rad_adpname = os::strdup(adpname);
	ASSERT(rad_adpname != NULL);

	//
	// rad_barriers_for_hb indicates whether it is safe to do open/close
	// barriers in pathend::pm_send_internal() which gets called in the
	// the high priority cyclic context by the heartbeat code.
	// rad_barriers_for_hb can be true only if:
	//	- open/close barriers do not block
	//	- timeouts for open/close barriers are fairly short in the
	//	order of millisecs
	//
	//

	//
	// ASSERT to ensure if a new RSM type adapter is added it
	// can support barriers in high priority cyclic context.
	//
	ASSERT(strcmp(rad_adpname, RSM_ADPNAME_SCI) == 0);
	rad_barriers_for_hb = true;

	rad_dev_inst = (uint_t)clconf_adapter_get_device_instance(cl_adapter);

	RSMT_TRANS_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_ADAPTER,
	    ("adapter name: %s, device instance: %u\n", adpname,
	    rad_dev_inst));

	rad_cmf_registered = false;

	// Get the controller from rsm for the name and instance we're
	// given and check that version numbers and capabilities match
	// this version of the transport.
	//
	rad_rsm_ctrl.handle = NULL;
	rc = rsm_get_controller(rad_adpname, rad_dev_inst, &rad_rsm_ctrl,
	    RSM_VERSION);
	if (rc != RSM_SUCCESS) {
		os::sc_syslog_msg msg("RSM TRANSPORT", "", NULL);
		//
		// SCMSGS
		// @explanation
		// This is a warning message from the RSM transport to
		// indicate that it cannot locate or get access to an expected
		// controller.
		// @user_action
		// This is a warning message as one of the controllers for the
		// private interconnect is unavailable. Users are encouraged
		// to run the controller specific diagnostic tests; reboot the
		// system if needed and if the problem persists, have the
		// controller replaced.
		//
		(void) msg.log(SC_SYSLOG_WARNING, MESSAGE,
			"RSM controller %s%u unavailable.",
			rad_adpname, rad_dev_inst);
		RSMT_TRANS_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_ADAPTER,
		    ("rsm_get_controller %s instance %u failed err = %d\n",
		    rad_adpname, rad_dev_inst, rc));
	} else if ((rc = rsm_get_controller_attr(rad_rsm_ctrl.handle,
	    &attrp)) != RSM_SUCCESS) {
		RSMT_TRANS_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_ADAPTER,
		    ("rsm_get_controller_attr failed, err = %d\n", rc));
	} else {
		// Save information that we'll use later when
		// initiating a connection.
		//

		// attr_page_size - 2^n indicates 2^nKB pagesize support
		// SCI does not return the right value.
		// XXX: Fix this once SCI returns correct value
		// rad_ctrl_pagesize = attrp->attr_page_size * 1024;

		rad_ctrl_pagesize = PAGESIZE;
		rad_ctrl_nodeid = attrp->attr_controller_addr;
		rad_ctrl_name = attrp->attr_name;

		ASSERT(rad_rsm_ctrl.handle != NULL);
		ASSERT(rad_rsm_ctrl.ops != NULL);
		// Put attribute info into debug buffer in case
		// it might be helpful for something.
		//
		RSMT_TRANS_DBG_D(RSMT_DBGL_ALWAYS, RSMT_DBGF_ENDPOINT,
		    ("name %s\n", attrp->attr_name));
		RSMT_TRANS_DBG_D(RSMT_DBGL_ALWAYS, RSMT_DBGF_ADAPTER,
		    ("controller_addr 0x%lx (%d)\n",
		    (ulong_t)rad_ctrl_nodeid, (ulong_t)rad_ctrl_nodeid));
		RSMT_TRANS_DBG_D(RSMT_DBGL_ALWAYS, RSMT_DBGF_ADAPTER,
		    ("direct_access_sizes 0x%x\n",
		    attrp->attr_direct_access_sizes));
		RSMT_TRANS_DBG_D(RSMT_DBGL_ALWAYS, RSMT_DBGF_ADAPTER,
		    ("mmu_protections %s\n", attrp->attr_mmu_protections ?
		    "true" : "false"));
		RSMT_TRANS_DBG_D(RSMT_DBGL_ALWAYS, RSMT_DBGF_ADAPTER,
		    ("page_size %lu\n",
		    (ulong_t)attrp->attr_page_size));
		RSMT_TRANS_DBG_D(RSMT_DBGL_ALWAYS, RSMT_DBGF_ADAPTER,
		    ("max_export_segment_size %lu\n",
		    (ulong_t)attrp->attr_max_export_segment_size));
		RSMT_TRANS_DBG_D(RSMT_DBGL_ALWAYS, RSMT_DBGF_ADAPTER,
		    ("max_export_segments %lu\n",
		    (ulong_t)attrp->attr_max_export_segments));
		RSMT_TRANS_DBG_D(RSMT_DBGL_ALWAYS, RSMT_DBGF_ADAPTER,
		    ("resource_callbacks %s\n",
		    attrp->attr_resource_callbacks ? "true" : "false"));

		// register the adapter with the control message framework
		// manager
		rsmt_cmf_mgr::the().register_adapter(get_adapter_id(),
		    &rad_rsm_ctrl);
		rad_cmf_registered = true;
		return;
	}

	// If we get here something failed.
	//
	if (rad_rsm_ctrl.handle != NULL) {
		rc = rsm_release_controller(rad_adpname, rad_dev_inst,
		    &rad_rsm_ctrl);
		ASSERT(rc == RSM_SUCCESS);
	}
	attrp = NULL;
	rad_ctrl_pagesize = 0;
	rad_ctrl_nodeid = 0;
	rad_ctrl_name = NULL;
	rad_rsm_ctrl.ops = NULL;
	rad_rsm_ctrl.handle = NULL;
}

rsmt_adapter::~rsmt_adapter()
{
	int rc;

	if (rad_cmf_registered) {
		rsmt_cmf_mgr::the().unregister_adapter(get_adapter_id());
	}

	if (rad_rsm_ctrl.handle != NULL) {
		rc = rsm_release_controller(rad_adpname, rad_dev_inst,
		    &rad_rsm_ctrl);
		if (rc != RSM_SUCCESS) {
			RSMT_TRANS_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_ADAPTER,
			    ("rsm_release_controller(\"%s\", %u) "
			    "failed with %d\n", rad_adpname, rad_dev_inst, rc));
		}
	}

	delete[] rad_adpname;

	attrp = NULL;
	rad_adpname = NULL;
	rad_ctrl_pagesize = 0;
	rad_ctrl_nodeid = 0;
	rad_ctrl_name = NULL;
	rad_rsm_ctrl.ops = NULL;
	rad_rsm_ctrl.handle = NULL;
}

//
// rsmt_adapter::get_max_segment_size
//
// Returns the maximum segment size that this adapter can support.
// This is a theoretical limit. Smaller segment allocation requests
// can also fail depending on dynamic system state.
//
size_t
rsmt_adapter::get_max_segment_size()
{
	if (attrp == NULL) {
		return (0);
	}
	return (attrp->attr_max_export_segment_size);
}

bool
rsmt_adapter::use_barriers_for_hb()
{
	return (rad_barriers_for_hb);
}
