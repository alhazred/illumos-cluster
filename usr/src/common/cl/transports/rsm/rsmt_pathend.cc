//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rsmt_pathend.cc	1.49	08/05/20 SMI"

#include <vm/vm_comm.h>
#include "rsmt_pathend.h"
#include "rsmt_endpoint.h"
#include "rsmt_util.h"
#include "rsmt_streams.h"

//
// Retry intervals used during pathend initiate upon failures during
// various operations.
//
#define	RSMT_PE_CREATE_RETRY_INTERVAL	((os::usec_t) 1000000)	// 1 sec
#define	RSMT_PE_PUBLISH_RETRY_INTERVAL	((os::usec_t) 1000000)	// 1 sec
#define	RSMT_PE_IMPORT_RETRY_INTERVAL	((os::usec_t) 1000000)	// 1 sec
#define	RSMT_PE_UNPUB_RETRY_INTERVAL	((os::usec_t) 10000)	// 10 msec
#define	RSMT_PE_GET_RETRY_INTERVAL	((os::usec_t) 10000)	// 10 msec

rsmt_pathend::rsmt_pathend(rsmt_transport *trp, clconf_path_t *pathp) :
	pathend(trp, pathp),
	remote_hb_offset(0)
{
	int local_adapter;
	clconf_cluster_t *cl;
	clconf_adapter_t *ap;
	rsm_addr_t config_local_adapter_id;

	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_PATHEND,
	    ("PE(%p) created\n", this));

	rpe_rtr = trp;
	rpe_rpd = NULL;

	rpe_have_seg = false;

	// initialize right here, so that even if return prematurely
	// cleanup doesn't see uninitialized fields.
	//
	rpe_remote_incn = 0;
	rpe_local_path_incn = 0;
	rpe_remote_path_incn = 0;
	rpe_rpd_rounded_size = 0;
	rpe_rpd_alloc = NULL;
	rpe_rpd_alloc_size = 0;
	rpe_ememseg = NULL;
	rpe_imemseg = NULL;
	rpe_memory.ms_memory.vr.vaddr = NULL;
	rpe_memory.ms_memory.vr.length = 0;
	rpe_memory.ms_memory.vr.as = NULL;
	rpe_wait_for_del = false;
	bzero(&rpe_send_buf, sizeof (rsmt_pathend_hbbuf_t));
	bzero(&rpe_recv_buf, sizeof (rsmt_pathend_hbbuf_t));

	// Find the adapter associated with this pathend.  We have
	// to do this in a roundabout way by asking the generic
	// transport.  If new_pathend() were a method on the adapter
	// class instead of the transport class, we would have all
	// the information ourselves.
	//
	local_adapter = local_adapter_id();
	rpe_adap = (rsmt_adapter *)(trp->get_adapter(local_adapter));
	ASSERT(rpe_adap != NULL);

	// If rsmt_adapter() couldn't find a contoller we can't form
	// a path so just return.  We'll block in initiate when this
	// happens.  This check will go away when new_adpater() can
	// return an error.
	//
	if (rpe_adap->rad_rsm_ctrl.handle == NULL) {
		RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_PATHEND,
		    ("PE(%p) could not locate rsm adapter for"
		    " adapter id %d\n", this, local_adapter));
		return;
	}

	// Find out whether we can use barriers to ensure ordering
	// in pm_send_internal
	rpe_barriers_for_hb = rpe_adap->use_barriers_for_hb();

	// Determine and save the the local and remote nodeid for
	// this path and the remote adapter id.  We will need this
	// info later.
	//
	rpe_local_nodeid = clconf_get_local_nodeid();
	rpe_remote_nodeid = clconf_path_get_remote_nodeid(pathp);

	ASSERT(rpe_local_nodeid >= 1 && rpe_local_nodeid <= RSMT_NODE_MAX);
	ASSERT(rpe_remote_nodeid >= 1 && rpe_remote_nodeid <= RSMT_NODE_MAX);

	cl = clconf_cluster_get_current();
	ap = clconf_path_get_adapter(pathp, CL_REMOTE, cl);
	rpe_remote_adapter_id = (uint_t)os::atoi(clconf_obj_get_property(
	    (clconf_obj *)ap, "adapter_id"));

	rpe_remote_adapter_instance = (uint_t)
	    clconf_adapter_get_device_instance(ap);

	ap = clconf_path_get_adapter(pathp, CL_LOCAL, cl);
	rpe_local_adapter_instance = (uint_t)
	    clconf_adapter_get_device_instance(ap);

	ASSERT(rpe_remote_adapter_instance <= RSMT_INST_MAX);
	ASSERT(rpe_local_adapter_instance <= RSMT_INST_MAX);

	// Check that the address of the local adapter given to
	// us by clconf matches the address the controller reports.
	// We have no way of reporting this however, so print a
	// a warning.
	//
	config_local_adapter_id = (uint_t)os::atoi(clconf_obj_get_property(
	    (clconf_obj *)ap, "adapter_id"));
	ASSERT(config_local_adapter_id == rpe_adap->rad_ctrl_nodeid);

	clconf_obj_release((clconf_obj_t *)cl);

	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_PATHEND,
	    ("PE(%p) created %d,%d -> %d,%d\n", this,
	    rpe_local_nodeid, rpe_local_adapter_instance,
	    rpe_remote_nodeid, rpe_remote_adapter_instance));

}

rsmt_pathend::~rsmt_pathend()
{
	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_PATHEND,
	    ("PE(%p) deleted %d,%d -> %d,%d\n", this,
	    rpe_local_nodeid, rpe_local_adapter_instance,
	    rpe_remote_nodeid, rpe_remote_adapter_instance));

	rpe_adap = NULL;

	rpe_rpd = NULL;
	rpe_imemseg = NULL;
	rpe_ememseg = NULL;
	rpe_rpd_alloc = NULL;
	rpe_rtr = NULL;
}

//
// Initiate the connection.
//
// This method performs three tasks:
// 1. It performs the essential handshake to establish a path so that
//    the nodes at both ends of the path know that the path can be
//    declared up.
// 2. It provides a vehicle for the version manager to exchange version
//    information for bootstrap node pair protocols. If version manager
//    detects an incompatibility at this level, path establishment process
//    is abandoned.
// 3. It exchanges information about the location of remote buffers that
//    should be used for heartbeat exchanges.
//
// The initiate process at each end of the path proceeds as follows.
// First the local node estimates the size of the segment it needs to
// export and then creates and exports the segment. Before exporting
// only the rsmt_pathend_data_t versions that this node understand are
// filled in in the rsmt_pathend_data. The valid flag is turned off.
// Next, attempts are made to connect to the similar segment exported
// by the peer. Once connection is established, the versions that the
// peer can support are read in and the local node determines the
// highest version that can be supported by both the local and the
// remote node. The appropriate version of rsmt_pathend_data is now
// filled in in the exported segment. This is followed by the buffer
// for incoming heartbeats and the local node pair version information
// obtained from the version manager. After all the data is written in,
// the valid flag is turned on.
//
// Next, we wait until the peer node turns the valid flag on in its
// exported segment. At that point, the remote data is read in and
// the local version manager is given an opportunity to determine if
// the remote node pair version capabilities are compatible with local
// node pair version capabilities. The compatible flag in the exported
// segment is updated accordingly. Next, we wait for the remote node
// to indicate whether it found the node pair versions we published
// compatible. If both nodes agree on compatibility, the pathend
// initiate is complete and the method returns after making a note of
// certain bookkeeping information such as location of remote heartbeat
// buffer, remote node incarnation number etc.
//
// If reads of remote data fail at any point or if an incompatibility
// is encountered, the whole process (except for segment creation and
// export) is started all over again with the hope that the remote
// node may have undergone some changes that will result in the path
// establishment process succeeding this time round.
//
void
rsmt_pathend::initiate(char *&send_buf_ref, char *&rec_buf_ref,
    incarnation_num &incn)
{
	int	rc;
	char	*vbuf;
	size_t	vlen;
	char	*version_buf;
	uint8_t	pe_data_ver;
	size_t	hdrlen;
	size_t	hblen;
	size_t	hbpad;
	pathend_state	state;
	rsmt_pathend_data_t	rdata;

	unlock();

	//
	// Assign a new path incarnation number to this incarnation
	// of the path based on current time.
	//
#ifdef _KERNEL
	timespec_t	tv;
	gethrestime(&tv);
#else
	struct timeval  tv;
	(void) gettimeofday(&tv, NULL);
#endif // _KERNEL
	if (tv.tv_sec == INCN_UNKNOWN)
		tv.tv_sec = 1;
	rpe_local_path_incn = (incarnation_num)tv.tv_sec;

	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_PATHEND,
	    ("PE(%p) initiate, local node incn %u path incn %u\n", this,
	    orb_conf::local_incarnation(), rpe_local_path_incn));

	//
	// If new_adapter fails it can't return an error so we
	// just sleep here hoping someone will remove the errant
	// path.  This will go away when new_adapter has its
	// interface fixed.
	//
	if (rpe_adap->rad_rsm_ctrl.handle == NULL) {
		RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_PATHEND,
		    ("PE(%p) no controller, blocking in initiate\n", this));
		lock();
		while (get_state() == P_CONSTRUCTED) {
			rpe_wait_for_del = true;
			unlock();
			rpe_mtx.lock();
			while (rpe_wait_for_del)
				rpe_cv.wait(&rpe_mtx);
			rpe_mtx.unlock();
			lock();
		}
		return;
	}

	//
	// To accurately estimate the size of the export segment that we
	// need, we need to determine if any padding is needed after the
	// rsmt_pathend_data header and before the beginning of the heartbeat
	// buffer. In the following we assume that the export segment will
	// be page aligned which implicitly implies RSMT_CACHELINE
	// alignment. Also if the local node is capable of supporting
	// multiple versions of rsmt_pathend_data_t, it must earmark
	// enough space for the largest possible data structure.
	//
	hdrlen	= sizeof (rsmt_pathend_data_t);
	hblen	= sizeof (rsmt_pathend_hbbuf_t);
	hbpad	= (RSMT_CACHELINE - (hdrlen & (RSMT_CACHELINE - 1))) &
			(RSMT_CACHELINE - 1);
	//
	// Get version information from the version manager. We pass
	// the first parameter as NULL to let the version manager
	// allocate space for the version buffer, since we don't know
	// it's size.
	//
	vlen = 0;
	vbuf = vm_comm::the().get_btstrp_np_string(NULL, vlen);
	ASSERT(vbuf != NULL && vlen != 0);

	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_PATHEND,
	    ("PE(%p) hdrlen %d hblen %d hbpad %d vlen %d\n",
	    this, hdrlen, hblen, hbpad, vlen));

	//
	// Create and publish the pathend shared segment.
	//
	rpe_create_exported_segment(hdrlen + hbpad + hblen + vlen);

	//
	// Try to establish contact and do version negotiation.
	//
	lock();
	while ((state = get_state()) == P_CONSTRUCTED) {
		unlock();

		// Import remote segment
		if (rpe_import_segment() != RSM_SUCCESS) {
			os::usecsleep(RSMT_PE_IMPORT_RETRY_INTERVAL);
			lock();
			continue;
		}

		//
		// Determine the highest version of the rsmt_pathend_data
		// structure format that we both understand.
		//
		pe_data_ver = rpe_negotiate_pe_data_version();

		RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_PATHEND,
		    ("PE(%p) Pathend data version = %d\n",
		    this, (int)pe_data_ver));

		if (pe_data_ver == 0) {
			goto disconnect_and_retry;
		}

		//
		// Now that we have agreed upon the rsmt_pathend_data's
		// format, let's make our information available to the
		// remote node.
		//
		rpe_populate_local_data(pe_data_ver, hbpad, vbuf, vlen);

		//
		// Read the rsmt_pathend_data structure from the peer.
		//
		RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_PATHEND,
		    ("PE(%p) Attempting to read remote data\n", this));

		bzero(&rdata, sizeof (rsmt_pathend_data_t));
		lock();
		rc = RSM_SUCCESS;
		while ((state = get_state()) == P_CONSTRUCTED) {
			unlock();
			if (((rc = rpe_get_remote_data((char *)&rdata,
			    (off_t)0, sizeof (rsmt_pathend_data_t))) !=
			    RSM_SUCCESS)) {
				lock();
				break;
			}
			if (rdata.flags & RSMT_PE_DATA_VALID) {
				lock();
				break;
			}
			os::usecsleep(RSMT_PE_GET_RETRY_INTERVAL);
			lock();
		}
		unlock();
		if (rc != RSM_SUCCESS) {
			// Could not read remote data.

			RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_PATHEND,
			    ("PE(%p) Failed to read remote data, rc = %d\n",
			    this, rc));

			goto disconnect_and_retry;
		}
		RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_PATHEND,
		    ("PE(%p) Remote data read successfully\n",
		    this));

		ASSERT(rdata.flags & RSMT_PE_DATA_VALID);

		//
		// Make sure data matches the format that we expect.
		//
		if (rdata.hdrver != pe_data_ver) {
			RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_PATHEND,
			    ("PE(%p) pathend data version mismatch,"
			    " expected %d found %d\n",
			    this, (int)pe_data_ver, (int)rdata.hdrver));

			goto disconnect_and_retry;
		}

		//
		// Notify path manager of the current version of the
		// remote node. This step must precede a call into the
		// version manager to determine compatibility.
		//
		if (!path_manager::the().update_node_incarnation(
		    rdata.ndid, rdata.incn)) {
			RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_PATHEND,
			    ("PE(%p) stale incarnation %d\n", this,
			    rdata.incn));
			goto disconnect_and_retry;
		}

		//
		// Now, get the remote node's bootstrap node pair protocol
		// versions that we need to forward to our version manager.
		//
		version_buf = new char[rdata.version_len];
		ASSERT(version_buf != NULL);
		if (((rc = rpe_get_remote_data(version_buf,
		    (off_t)rdata.version_offset, (size_t)rdata.version_len))
		    != RSM_SUCCESS)) {

			RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_PATHEND,
			    ("PE(%p) Failed to read remote version info,"
			    " rc = %d\n", this, rc));
			delete [] version_buf;
			goto disconnect_and_retry;
		}
		RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_PATHEND,
		    ("PE(%p) Remote version info read successfully,"
		    "remote path incn %u\n", this, rdata.path_incn));

		//
		// Make the path incarnation number we just read from the
		// remote node available to the remote node so that the
		// remote node knows that the compatibility verdict we are
		// going to publish below is based on the most recent
		// information read from the remote node..
		//
		rpe_rpd->path_incn_echo = rdata.path_incn;

		//
		// Call into version manager to determine bootstrap nodepair
		// protocol compatibility.
		//
		if (!vm_comm::the().compatible_btstrp_np(rdata.ndid,
		    rdata.incn, (size_t)rdata.version_len, version_buf,
		    rdata.flags & RSMT_NP_INCOMPATIBLE ? true : false)) {
			//
			// Not compatible! Make result available to peer.
			//
			RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_PATHEND,
			    ("PE(%p) Remote versions incompatible\n", this));

			rpe_rpd->flags |= RSMT_NP_INCOMPATIBLE;
			rpe_rpd->flags &= ~RSMT_NP_UNKNOWN;
			delete [] version_buf;
			goto disconnect_and_retry;
		}
		delete [] version_buf;

		//
		// Compatible, at least from our point of view. Make our
		// verdict available to the peer.
		//
		rpe_rpd->flags |= RSMT_NP_COMPATIBLE;
		rpe_rpd->flags &= ~RSMT_NP_UNKNOWN;

		//
		// Obtain the compatibility verdict reached by the peer.
		//
		RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_PATHEND,
		    ("PE(%p) Waiting for remote version compatibilily"
		    " result\n", this));

		lock();
		while ((state = get_state()) == P_CONSTRUCTED) {
			unlock();
			if (((rc = rpe_get_remote_data((char *)&rdata,
			    (off_t)0, sizeof (rsmt_pathend_data_t))) !=
			    RSM_SUCCESS) ||
			    !(rdata.flags & RSMT_NP_UNKNOWN)) {
				lock();
				break;
			}
			os::usecsleep(RSMT_PE_GET_RETRY_INTERVAL);
			lock();
		}
		unlock();
		if (rc != RSM_SUCCESS) {
			RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_PATHEND,
			    ("PE(%p) Error reading remote version"
			    " compatibilily result, rc = %d\n", this, rc));

			goto disconnect_and_retry;
		}

		//
		// See what the peer has to say?
		//
		if ((rdata.flags & RSMT_NP_COMPATIBLE) &&
		    (rdata.path_incn_echo == rpe_local_path_incn)) {
			//
			// Peer found us compatible too! We are all set.
			// Record some incarnation numbers and heartbeat
			// buffer pointers that represent this successful
			// handshake.
			//
			RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_PATHEND,
			    ("PE(%p) Remote node found us compatible\n",
			    this));

			lock();
			incn = rpe_remote_incn = rdata.incn;
			rpe_remote_path_incn = rdata.path_incn;
			//
			// clear up any stale token in the local_recv_buf
			// before starting a new incarnation of the path.
			//
			bzero(local_recv_buf, sizeof (local_recv_buf));
			rec_buf_ref = local_recv_buf;
			send_buf_ref = local_send_buf;
			remote_hb_offset = (off_t)rdata.hb_offset;

			// Done! Break out of the initiate loop.
			break;
		} else if (!(rdata.flags & RSMT_NP_COMPATIBLE)) {

			//
			// Remote node found us incompatible. Retry after a
			// while with the hope that the remote node reboots in
			// a mode where we can be compatible.
			//
			RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_PATHEND,
			    ("PE(%p) Remote node found us incompatible\n",
			    this));
		} else {
			//
			// Remote node does not have current information
			// about us.
			//
			RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_PATHEND,
			    ("PE(%p) Remote node's compatibility information"
			    " is stale - local path incn %u echoed incn %u \n",
			    this, rpe_local_path_incn, rdata.path_incn_echo));
		}

	disconnect_and_retry:
		rpe_rpd->flags = RSMT_NP_UNKNOWN;
		rpe_disconnect_segment();
		os::usecsleep(RSMT_PE_IMPORT_RETRY_INTERVAL);
		lock();
	}
	delete [] vbuf;

	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_PATHEND,
	    ("PE(%p) initiate done, state = %d\n", this, (int)state));
}

void
rsmt_pathend::pm_send_internal()
{
	rsm_barrier_t	bar;
	int	retry_cnt = 3;
	int	rc;

	// data to be sent is in local_send_buf which is embedded
	// in rpe_send_buf

	// we use the heartbeat token as its own chksum.
	os::atomic_copy_64((uint64_t *)rpe_send_buf.buf_chksum.chksum,
	    (uint64_t *)local_send_buf);

	do {
		rpe_open_barrier(&bar);

		// lint warns about null pointer deref in RSMT_PATHEND_OFFSET
		// macro
		rc = RSM_PUT(rpe_adap->rad_rsm_ctrl, rpe_imemseg,
		    remote_hb_offset, (void *)&rpe_send_buf,
		    (size_t)RSMT_CACHELINE); //lint !e413

		rc = rpe_close_barrier(&bar);
		if (!rc) {
			break;
		}

		retry_cnt--;

		HB_DBG(this, ("close_barrier using %s failed(%d) retry(%d)\n",
		    rpe_barriers_for_hb ? "Barrier" : "RSM_GET",
		    rc, retry_cnt));

	} while (retry_cnt);
}

void
rsmt_pathend::pm_get_internal()
{
	uint64_t tok;
	uint64_t tokck;
	rsmt_pathend_hbbuf_t	*hbbuf;

	hbbuf = ((rsmt_pathend_hbbuf_t *)
	    ((char *)rpe_rpd + rpe_rpd->hb_offset));

	// make a local copy of the heartbeat buffer
	tok = *(uint64_t *)hbbuf->buf_chksum.buf;
	tokck = *(uint64_t *)hbbuf->buf_chksum.chksum;

	//
	// There is no atomicity guarantee wrt rsm_put()
	// If the heartbeat buffer chksum (which is just a copy of the
	// heartbeat) is valid we copy it into the local_recv_buf.
	//

	if ((tok > 0) && (tok == tokck) && pm_isnewer(&tok)) {
		atomic_copy_64((uint64_t *)local_recv_buf, &tok);
	} else {
		HB_DBG(this, ("heartbeat invalid or stale:%llx:%llx\n", tok,
		    tokck));
	}
}

void
rsmt_pathend::cleanup()
{
	int rc;
	rsm_memseg_export_handle_t	eseg;
	rsm_memseg_import_handle_t	iseg;

	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_PATHEND,
	    ("PE(%p) cleanup\n", this));

	eseg = rpe_ememseg;
	rpe_ememseg = NULL;
	iseg = rpe_imemseg;
	rpe_imemseg = NULL;
	unlock();

	if (iseg != NULL) {
		rc = RSM_DISCONNECT(rpe_adap->rad_rsm_ctrl, iseg);
		ASSERT(rc == RSM_SUCCESS);
		if (rc != RSM_SUCCESS) {
			RSMT_SEG_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_PATHEND,
			    ("PE(%p) disconnect failed, rc %d\n", this, rc));
		}
	}

	if (eseg != NULL) {
		do {
			rc = RSM_UNPUBLISH(rpe_adap->rad_rsm_ctrl, eseg);
			if (rc == RSM_SUCCESS)
				break;
			if (rc == RSMERR_SEG_IN_USE) {
				os::usecsleep(RSMT_PE_UNPUB_RETRY_INTERVAL);
				RSMT_SEG_DBG(RSMT_DBGL_VERBOSE,
				    RSMT_DBGF_PATHEND,
				    ("PE(%p) unpublish failed rc %d\n", this,
					rc));
			}
		} while (rc == RSMERR_SEG_IN_USE);
		ASSERT(rc == RSM_SUCCESS);

		rc = RSM_SEG_DESTROY(rpe_adap->rad_rsm_ctrl, eseg);
		ASSERT(rc == RSM_SUCCESS);
	}

	lock();
	if (rpe_rpd_alloc) {
		free_chunk_aligned(rpe_rpd_alloc, rpe_rpd_alloc_size);
	}
	rpe_rpd = NULL;
	rpe_rpd_alloc = NULL;
	rpe_have_seg = false;
}

void
rsmt_pathend::delete_notify()
{
	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_PATHEND,
	    ("PE(%p) delete_notify\n", this));

	unlock();

	rpe_mtx.lock();
	rpe_wait_for_del = false;
	rpe_cv.broadcast();
	rpe_mtx.unlock();

	lock();
}

endpoint *
rsmt_pathend::new_endpoint(transport *tr)
{
	rsmt_endpoint *epp;

	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_PATHEND,
	    ("PE(%p) new_endpoint\n", this));

	epp = new rsmt_endpoint(tr, this, local_adapter_id(),
		rpe_remote_nodeid, remote_adapter_id(),
		rpe_remote_adapter_id);
	ASSERT(epp != NULL);
	return (epp);
}

void
rsmt_pathend::rpe_create_exported_segment(size_t dlen)
{
	int	rc;
	rsm_memseg_id_t	segid;
	pathend_state	state;

	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_PATHEND,
	    ("PE(%p) rpe_create_exported_segment\n", this));

	ASSERT(!rpe_have_seg);

	// Allocate a buffer that can be exported for the initiate
	// shared segment, verify alignment, clear for good measure.
	//
	rpe_rpd = (rsmt_pathend_data *)alloc_chunk_aligned(dlen,
	    rpe_adap->rad_ctrl_pagesize, &rpe_rpd_rounded_size,
	    &rpe_rpd_alloc, &rpe_rpd_alloc_size);
	bzero(rpe_rpd, dlen);

	//
	// Set the rsmt_pathend_data header version range that we can
	// work with. Since this implemnation is the very first implementation
	// of the RSM transport, both minimum and the maximum versions are
	// going to be the same. Also, indicate that the result of the node
	// pair version negotiation is not ready yet.
	//
	rpe_rpd->minver	= RSMT_PE_DATA_MIN_VER;
	rpe_rpd->maxver	= RSMT_PE_DATA_MAX_VER;
	rpe_rpd->flags	= RSMT_NP_UNKNOWN;

	// lint thinks we can't get to VADDR, but I think we can
	rpe_memory.ms_type = rsm_memory_local_t::RSM_MEM_VADDR;	//lint !e1061
	rpe_memory.ms_memory.vr.vaddr = rpe_rpd;
	rpe_memory.ms_memory.vr.length = rpe_rpd_rounded_size;
	rpe_memory.ms_memory.vr.as = NULL;

	lock();
	while ((state = get_state()) == P_CONSTRUCTED) {
		unlock();
		rc = RSM_SEG_CREATE(rpe_adap->rad_rsm_ctrl, &rpe_ememseg,
		    rpe_rpd_rounded_size, 0, &rpe_memory, NULL, NULL);
		if (rc == RSM_SUCCESS) {
			lock();
			break;
		}
		os::usecsleep(RSMT_PE_CREATE_RETRY_INTERVAL);
		lock();
	}
	unlock();
	if (state != P_CONSTRUCTED) {
		free_chunk_aligned(rpe_rpd_alloc, rpe_rpd_alloc_size);
		rpe_ememseg = NULL;
		rpe_rpd = NULL;
		rpe_rpd_alloc = NULL;
		rpe_rpd_alloc_size = 0;
		rpe_rpd_rounded_size = 0;
		return;
	}

	// We've created the segment, now make it visible to the
	// remote node by publishing it.
	//
	segid = RSM_CLUSTER_TRANSPORT_ID_BASE +
	    ((rpe_remote_nodeid << RSMT_INST_BITS) |
	    rpe_remote_adapter_instance);

	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_PATHEND,
	    ("PE(%p) export segid = 0x%x\n", this, segid));
	ASSERT(segid >= RSM_CLUSTER_TRANSPORT_ID_BASE &&
	    segid <= RSM_CLUSTER_TRANSPORT_ID_END);

	lock();
	while ((state = get_state()) == P_CONSTRUCTED) {
		unlock();
		rc = RSM_PUBLISH(rpe_adap->rad_rsm_ctrl, rpe_ememseg, NULL, 0,
		    segid, NULL, NULL);
		if (rc == RSM_SUCCESS) {
			lock();
			break;
		}
		os::usecsleep(RSMT_PE_PUBLISH_RETRY_INTERVAL);
		lock();
	}
	unlock();
	if (state != P_CONSTRUCTED) {
		rc = RSM_SEG_DESTROY(rpe_adap->rad_rsm_ctrl, rpe_ememseg);
		ASSERT(rc == RSM_SUCCESS);
		free_chunk_aligned(rpe_rpd_alloc, rpe_rpd_alloc_size);
		rpe_ememseg = NULL;
		rpe_rpd = NULL;
		rpe_rpd_alloc = NULL;
		rpe_rpd_alloc_size = 0;
		rpe_rpd_rounded_size = 0;
		return;
	}

	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_PATHEND,
	    ("PE(%p) published segid 0x%lx\n", this, (ulong_t)segid));

	rpe_have_seg = true;
}

int
rsmt_pathend::rpe_import_segment()
{
	int rc;
	rsm_memseg_id_t segid;

	segid = RSM_CLUSTER_TRANSPORT_ID_BASE +
	    ((rpe_local_nodeid << RSMT_INST_BITS) | rpe_local_adapter_instance);

	rc = RSM_CONNECT(rpe_adap->rad_rsm_ctrl,
	    rpe_remote_adapter_id, segid, &rpe_imemseg);

	if (rc) {
		RSMT_PATH_DBG(RSMT_DBGL_VERBOSE, RSMT_DBGF_PATHEND,
		    ("PE(%p) connect to segid 0x%lx failed=%d\n", this,
			(ulong_t)segid, rc));
	}

	return (rc);
}

void
rsmt_pathend::rpe_disconnect_segment()
{
	int	rc;

	if (rpe_imemseg != NULL) {
		rc = RSM_DISCONNECT(rpe_adap->rad_rsm_ctrl, rpe_imemseg);
		ASSERT(rc == RSM_SUCCESS);
		if (rc != RSM_SUCCESS) {
			RSMT_SEG_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_PATHEND,
			    ("PE(%p) disconnect failed rc %d\n", this, rc));
		}
	}

	rpe_imemseg = NULL;
}

void
rsmt_pathend::rpe_open_barrier(rsm_barrier_t *barp)
{
	if (rpe_barriers_for_hb) {
		(void) RSM_OPEN_BARRIER_REGION(rpe_adap->rad_rsm_ctrl,
		    rpe_imemseg, barp);
	}
}

int
rsmt_pathend::rpe_close_barrier(rsm_barrier_t *barp)
{
	if (rpe_barriers_for_hb) {
		return (RSM_CLOSE_BARRIER(rpe_adap->rad_rsm_ctrl, barp));
	} else {
		//
		// If barriers are heavyweight ops and they can
		// block for prolonged period, therefore they can't
		// be used in the context of the clock cyclic. So
		// a poor man's barrier is implemented using a RSM_GET
		// to ensure ordering and completion of the RSM_PUT
		// done in the pm_send_internal routine.
		//
		rsmt_pathend_hbbuf_t tmphb;

		// RSM_GET will flush out the RSM_PUT

		(void) RSM_GET(rpe_adap->rad_rsm_ctrl, rpe_imemseg,
		    remote_hb_offset, (void *)tmphb.buf,
		    (size_t)RSMT_CACHELINE); //lint !e413

		if (tmphb.buf_chksum.buf == 0 ||
		    tmphb.buf_chksum.buf != tmphb.buf_chksum.chksum) {
			// If an error occurs the pattern will be 0 -
			// interconnect guarantees it
			return (RSMERR_BARRIER_FAILURE);
		}
		return (RSM_SUCCESS);
	}
}

int
rsmt_pathend::rpe_get_remote_data(char *rbuf, off_t offset, size_t len)
{
	rsm_barrier_t	bar;
	int		rc;

	rc = RSM_OPEN_BARRIER_REGION(rpe_adap->rad_rsm_ctrl, rpe_imemseg,
		&bar);
	if (rc == RSM_SUCCESS) {
		rc = RSM_GET(rpe_adap->rad_rsm_ctrl, rpe_imemseg, offset,
			rbuf, len);
	}
	if (rc == RSM_SUCCESS) {
		rc = RSM_CLOSE_BARRIER(rpe_adap->rad_rsm_ctrl, &bar);
	}
	return (rc);
}

//
// Returns the highest rsmt_pathend_data version that is recognized
// by both nodes at the two ends of the path.
//
uint8_t
rsmt_pathend::rpe_negotiate_pe_data_version()
{
	rsmt_pathend_data rdata;

	if (rpe_get_remote_data((char *)&rdata, (off_t)0,
	    4 * sizeof (uint8_t)) != RSM_SUCCESS) {
		return (0);
	}
	if (rdata.minver > RSMT_PE_DATA_MAX_VER ||
	    rdata.maxver < RSMT_PE_DATA_MIN_VER) {
		return (0);
	} else if (rdata.maxver < RSMT_PE_DATA_MAX_VER) {
		return (rdata.maxver);
	} else {
		return (RSMT_PE_DATA_MAX_VER);
	}
}

//
// Fills in local data according to the passed rsmt_pathend_data version
// and then sets the valid flag on.
//
void
rsmt_pathend::rpe_populate_local_data(uint8_t pe_data_ver, size_t hbpad,
    char *vbuf, size_t vlen)
{
	rpe_rpd->hdrver	= pe_data_ver;
	rpe_rpd->ndid	= orb_conf::local_nodeid();
	rpe_rpd->incn	= orb_conf::local_incarnation();
	rpe_rpd->path_incn = rpe_local_path_incn;
	rpe_rpd->hb_offset = (uint32_t)(sizeof (rsmt_pathend_data_t) + hbpad);
	ASSERT((((uintptr_t)rpe_rpd + rpe_rpd->hb_offset) &
	    (uintptr_t)(RSMT_CACHELINE - 1)) == 0);
	rpe_rpd->hb_len = (uint32_t)sizeof (rsmt_pathend_hbbuf_t);
	rpe_rpd->version_offset	= rpe_rpd->hb_offset + rpe_rpd->hb_len;
	rpe_rpd->version_len	= (uint32_t)vlen;
	bcopy(vbuf, (char *)rpe_rpd + rpe_rpd->version_offset, vlen);

	ASSERT(rpe_rpd->flags == RSMT_NP_UNKNOWN);
	rpe_rpd->flags	|= RSMT_PE_DATA_VALID;
}
