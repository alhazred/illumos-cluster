/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RSMT_CMF_H
#define	_RSMT_CMF_H

#pragma ident	"@(#)rsmt_cmf.h	1.13	08/05/20 SMI"

#include <sys/types.h>
#include <sys/rsm/rsmpi.h>

#include <sys/refcnt.h>

#include "rsmt_cmf_client.h"
#include "rsmt_adapter.h"

#ifndef _KERNEL
#define	mem_alloc(bsize)	memalign(8, bsize) // double-word aligned
#define	mem_free(ptr, bsize)    free(ptr)
#else
#define	mem_alloc(bsize)	kmem_alloc(bsize, KM_SLEEP)
#define	mem_free(ptr, bsize)    kmem_free(ptr, bsize)
#endif

// forward declarations...
class rsmt_cmf_mgr;

//
// This file defines the control message framework used by the RSM transport.
// Control messages are sent using the interrupt queues provided by RSM.
// The class rsmt_cmf_client implements the control message operations.
// Components like segment mgr, replio, bufferio are subclasses of
// rsmt_cmf_client and hence has access to the control message operations.
// The RSM transport creates an instance of the class rsmt_cmf_mgr
// (rsm transport control message framework manager) when the transport
// is initialized. All rsm adapters are registered with the
// rsmt_cmf_mgr object. The rsmt_cmf_mgr object maintains a list of adapters
// (rsmt_cmf_adapter). Each adapter registers an interrupt handler.
//
// Control messages can be of two types - flow controlled or non flow
// controlled - for details look at rsmt_cmf_client class
//

//
// rsmt_cmf_adapter
//
// The rsmt_cmf_adapter object is reponsible for registering an interrupt
// handler that handles the incoming control messages. It always maintains
// a list of current clients, so that it can forward an incoming message
// to the appropriate client. The messages are demultiplexed based on
// sender-nodeid and clientid. THe rsmt_cmf_adapter object is created
// by the rsmt_cmf_mgr::register_adapter() interface.
//
class rsmt_cmf_adapter: public refcnt {
public:
	// arguments to the constructor.
	// adapter_id and pointer to rsm_controller_object_t
	rsmt_cmf_adapter(int, rsm_controller_object_t *);
	~rsmt_cmf_adapter();

	// rsmt_cmf_clients can register themselves by passing
	// the remote nodeid, clientid, pointer to itself
	// register_client enables the clients to start receiving
	// control messages. There can only be one client registered
	// for a specific remote node and clientid.
	void register_client(nodeid_t, cmf_clientid_t, rsmt_cmf_client *);

	// clients can unregister themselves by passing the remote nodeid
	// and pointer to itself.
	// unregister disassociates the client with the cmf_adapter
	void unregister_client(nodeid_t, rsmt_cmf_client *);

	// get the service number used by this adapter
	rsm_intr_service_t	get_servicenum() const;

	// get the controller object
	rsm_controller_object_t	*get_controller() const;

	// get the controller attributets
	rsm_controller_attr_t	*get_attr() const;

	// This method delivers the message to the appropriate client
	void deliver(void *);

	// This method is called by the retry_task that attempts to
	// redeliver a flow-controlled message to the clients.
	void redeliver();

	// This method is used by the rsmt_cmf_client to increment
	// the retry client count when it has pending messages
	// to be delivered.
	void incr_retryclientcnt();

	// This method is used by the rsmt_cmf_client to decrement
	// the retry client count when all pending messages have been
	// delivered.
	void decr_retryclientcnt();

private:
	// rsm_controller_object_t used in rsmpi calls
	rsm_controller_object_t	*cma_rsmadp_ctlr;

	// this rightfully belongs in the rsmt_adapter class
	rsm_controller_attr_t	*cma_rsmadp_attr;

	int	cma_adapter_id;

	rsm_intr_service_t cma_rsmserv;

	// the interrupt handler
	rsm_intr_hand_t cma_handler;

	// table of clients that have registered, nodeids start with 1
	rsmt_cmf_client *cma_clients[NODEID_MAX+1][N_CMFCLIENTS];

	// Number of clients
	uint_t cma_clients_count;

	// Number of clients that need message redelivery attempts
	uint_t cma_retry_clients_count;

	// protects accesses to the clients table, cma_clients_count
	os::rwlock_t	cma_client_lock;

	// Prevent assignment and pass by value
	rsmt_cmf_adapter(const rsmt_cmf_adapter &);
	rsmt_cmf_adapter &operator = (const rsmt_cmf_adapter &);

	// pacify lint
	rsmt_cmf_adapter();
};

//
// rsmt_cmf_defer_task: Task to attempt redelivery of failed
//	flow controlled messages.
//
// A single thread from the common threadpool is used to attempt redelivery
// of messages that could not be delivered the first time around. This task
// periodically retries to deliver the flow-controlled messages.
//
class rsmt_cmf_defer_task : public defer_task {
public:
	rsmt_cmf_defer_task(rsmt_cmf_mgr *);
	void execute();

private:
	rsmt_cmf_mgr *cmf_mgrp;

	// Prevent assignment and pass by value
	rsmt_cmf_defer_task(const rsmt_cmf_defer_task &);
	rsmt_cmf_defer_task &operator = (const rsmt_cmf_defer_task &);

	// Pacify lint
	rsmt_cmf_defer_task();
};

//
// rsm_cmf_mgr: The RSM Transport Control Message Framework Manager.
//
// There is one instance of this object at every node running the rsm
// transport. This object is created when the rsm transport is loaded.
// When adapters are added to the transport, the transport notifies the
// control message manager about their existence. The sole purpose
// of the control message manager is to keep track of the adapters,
// assign them service id, and let the clients know of the adapters
// when the clients query for them.
//
class rsmt_cmf_mgr {
public:

	// some class constants
	enum {
		// Base of the rsm_intr_service_t range available to rsm
		// transport. 0xc0 to 0xdf is the range reserved for rsmt
		// 0xd0, 0xd4 on SCI conflict with RSMAPI kernel agent
		RSMT_CMF_INTR_BASE = 0xc0
	};

	// Creates the rsmt_cmf_mgr object
	static int initialize();

	// Destroys the rsmt_cmf_mgr object
	void shutdown();

	// Returns reference to the rsmt_cmf_mgr object
	static rsmt_cmf_mgr &the();

	// Returns the interrupt service number for a given adapterid
	static rsm_intr_service_t   get_servicenum(int);

	// Add an adapter
	// arguments are the adapter id, pointer to the rsm controller object
	void register_adapter(int, rsm_controller_object_t *);

	// Remove an adapter given the adapter id.
	void unregister_adapter(int);

	// Returns pointer to the adapter object given the adapter id.
	// Places a hold on the adapter object.
	rsmt_cmf_adapter *get_adapter(int);

	// The message redelivery method
	void redeliver();
private:
	rsmt_cmf_mgr();
	~rsmt_cmf_mgr();

	static rsmt_cmf_mgr *the_rsmt_cmf_mgr;

	// An array of pointers to the adapter objects, indexed by the
	// adapter id. Adapter ids start with 1.
	rsmt_cmf_adapter *cmmgr_adapters[RSMT_MAX_ADAPTERS+1];

	// The task to retry delivery of failed messages
	rsmt_cmf_defer_task *cmmgr_retry_task;

	// flag used to coordinate retry_task's exit
	bool	cmmgr_shutdown;
	bool	cmmgr_taskactive;

	// Number of adapters that have retry messages pending.
	int	cmmgr_retry_adapter_count;

	os::condvar_t	cmmgr_cv;
	os::mutex_t	cmmgr_lock;

	// Prevent assignment and pass by value
	rsmt_cmf_mgr(const rsmt_cmf_mgr &);
	rsmt_cmf_mgr &operator = (const rsmt_cmf_mgr &);
};

#include "rsmt_cmf_in.h"

#endif /* _RSMT_CMF_H */
