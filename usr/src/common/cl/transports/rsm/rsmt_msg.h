/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RSMT_MSG_H
#define	_RSMT_MSG_H

#pragma ident	"@(#)rsmt_msg.h	1.12	08/05/20 SMI"

#include <orb/msg/orb_msg.h>
#include <orb/transport/path_manager.h>


//
// Info about message in partitions.
//
typedef struct {
	uint_t		rm_main_len;
	uint_t		rm_xdoor_len;
	uint_t		rm_xdoorcount;
	uint_t		rm_total_len;
	bool		rm_offline_data;
	uint_t		rm_offline_size;
	orb_msgtype	rm_msgtype;
	ID_node		rm_src;
	orb_seq_t	rm_seq;
#ifdef	DEBUG
	uint32_t	rm_cksum;
#endif
} rsmt_hdr_t;

//
// Create a 64-byte padded header to contain rsmt_hdr_t this ensures
//   - Header is 64-bytes.
//   - Doing 64-byte aligned and 64-byte sized writes are more efficient.
//
union rsmt_padded_hdr {
public:
	uchar_t		padc[RSMT_CACHELINE];
	uint64_t	padalign; // ensures double-word alignment
	// constructor
	rsmt_padded_hdr(const rsmt_hdr_t *);
#ifdef	DEBUG
	// calculate checksum
	static uint32_t checksum32(void *, size_t);
#endif
private:
	// Pacify lint
	rsmt_padded_hdr();
};

inline
rsmt_padded_hdr::rsmt_padded_hdr(const rsmt_hdr_t *hdrp)
{
	// lint clueless about unions, complains that padalign is uninitialized
	padalign = 0;
#ifdef	DEBUG
	for (uint32_t i = 0; i < sizeof (rsmt_padded_hdr); i++) {
		padc[i] = 0xab;
	}
#endif
	ASSERT(sizeof (*hdrp) <= sizeof (rsmt_padded_hdr));
	bcopy((char *)hdrp, padc, sizeof (*hdrp));
}

#ifdef	DEBUG
inline uint32_t
rsmt_padded_hdr::checksum32(void *cp_arg, size_t length)
{
	uchar_t *cp, *ep;
	uint32_t sum = 0;

	for (cp = (uchar_t *)cp_arg, ep = cp + length; cp < ep; cp++) {
		sum = ((sum >> 1) | (sum << 31)) + *cp;
	}
	return (sum);
}
#endif	// DEBUG

#endif	/* _RSMT_MSG_H */
