/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_RSMT_CMF_IN_H
#define	_RSMT_CMF_IN_H

#pragma ident	"@(#)rsmt_cmf_in.h	1.10	08/05/20 SMI"

inline rsm_intr_service_t
rsmt_cmf_adapter::get_servicenum() const
{
	return (cma_rsmserv);
}

inline rsm_controller_object_t *
rsmt_cmf_adapter::get_controller() const
{
	return (cma_rsmadp_ctlr);
}

inline rsm_controller_attr_t *
rsmt_cmf_adapter::get_attr() const
{
	// XXXX: this should come from the rsmt_adapter object directly
	return (cma_rsmadp_attr);
}

inline void
rsmt_cmf_adapter::incr_retryclientcnt()
{
	os::atomic_add_32(&cma_retry_clients_count, 1);
}

inline void
rsmt_cmf_adapter::decr_retryclientcnt()
{
	os::atomic_add_32(&cma_retry_clients_count, -1);
}

inline rsmt_cmf_mgr &
rsmt_cmf_mgr::the()
{
	ASSERT(the_rsmt_cmf_mgr != NULL);
	return (*the_rsmt_cmf_mgr);
}

//
// get service number mapping for a given adapter id
// service number = RSMT_CMF_INTR_BASE + adapter_id - 1
// NOTE:
// service numbers 0xD0 and 0xD4 conflict with RSMAPI kernel agent
// on SCI since the kernel agent uses 0x88+hwaddr to calculate its
// service number. On SCI the hwaddr of 0x48 and 0x4c are the ones
// that cause the conflict. So in rsm-transport we map 0xD0 to the last
// but one service number (0xDE) and 0xD4 to the last service number (0xDF)
// in the rsm-transport range.
//
inline rsm_intr_service_t
rsmt_cmf_mgr::get_servicenum(int adp_id)
{
	ASSERT(adp_id > 0 && adp_id <= RSMT_MAX_ADAPTERS);

	// adapter id starts from 1
	int servnum = RSMT_CMF_INTR_BASE + adp_id - 1;

	// map 0xD0 to 0xDE and 0xD4 to 0xDF
	if (servnum == 0xD0)
		// last but one service number in rsmt range
		servnum = RSMT_CMF_INTR_BASE + RSMT_MAX_ADAPTERS;
	else if (servnum == 0xD4)
		// last service number in rsmt range
		servnum = RSMT_CMF_INTR_BASE + RSMT_MAX_ADAPTERS + 1;

	return ((rsm_intr_service_t)servnum);
}

#endif	/* _RSMT_CMF_IN_H */
