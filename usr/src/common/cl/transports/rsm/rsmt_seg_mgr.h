/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RSMT_SEG_MGR_H
#define	_RSMT_SEG_MGR_H

#pragma ident	"@(#)rsmt_seg_mgr.h	1.33	08/05/20 SMI"

#include "rsmt_adapter.h"
#include "rsmt_segid_mgr.h"
#include "rsmt_segment.h"
#include "rsmt_cmf_client.h"

// Forward declarations
class rsmt_finalizer;

//
// rsmt_seg_mgr: The segment manager
//
// Each endpoint creates an instance of the segment manager object that
// manages segments on behalf of that endpoint.
//

// type definition of connection token
// opaque object to be passed by the exporter to the importer
// and used as an identification of the export segment
typedef uint64_t rsmt_seg_conn_token_t;
typedef rsmt_seg_conn_token_t *rsmt_seg_conn_tokenp_t;
#define	RSMT_INVAL_SEG_CONN_TOKEN ((rsmt_seg_conn_token_t)0)
typedef struct {
	rsmt_cmhdr_t	sm_hdr;
	rsmt_seg_conn_token_t	sm_token;
} rsmt_sm_msg_t;
#define	RSMT_SM_MSGSZ	(sizeof (rsmt_sm_msg_t))

#define	RSMT_SM_FLOWCONTROL	8

class rsmt_seg_mgr : public rsmt_cmf_client {
public:
	rsmt_seg_mgr(rsmt_transport *trp, rsmt_adapter *adp,
		int local_adapter_id, nodeid_t remote_nodeid,
		int remote_adapter_id, rsm_addr_t remote_addr);
	~rsmt_seg_mgr();

	// init and fini of segment manager
	void initiate_init();	// initialization I control message register
	bool initiate_fini();	// initialization II enable control message
	void initiate_cleanup(); // initialization cleanup on error
	void unblock();		// unblock control message unregister
	void cleanup_init();	// cleanup I control message unregister
	void cleanup_fini();   	// cleanup II disable control message and
				// internal cleanup

	// Create and publish a segment of the given size for export.
	// The flags argument is a set of export flags that specify whether the
	// segment needs to be connected to the peer and allow the RSM unbind
	// and rebind to happen.
	// The arguments mem_addr and mem_len specify the physical memory
	// to be bound to the segment. If mem_addr equals to NULL and the
	// mem_len equals to zero, the created segment will not have any backing
	// storage.
	// The waitflag arguments is of rsm_resource_callback_t type which
	// takes on the value of RSM_RESOURCE_SLEEP or RSM_RESOURCE_DONTWAIT.
	//
	// Return Values : A pointer to the rsmt_export_segment object.
	//	the function will be in blocking or nonblocking mode depends
	//	on the waitflag argument.
	//
	// Context : called from user or kernel.
	//
#define	RSMT_SMEX_CONNECT 0x01 	// connect to the peer
#define	RSMT_SMEX_UNBIND_REBIND 0x02 // unbind and rebind allowed
	rsmt_export_segment *create_seg(size_t size, uint32_t exflags,
	    void *mem_addr, size_t mem_len, rsm_resource_callback_t waitflag);

	// Destroy the export segment. Must be called from the endpoint
	// that initiated the create_seg(). Segment manager will synchronize
	// segment disconnection on the import endpoint before unpublishing
	// the segment. The second parameter is the callback method that will
	// be called when the export segment is unpublished and destoyed. The
	// third parameter is passed as is to the callback as the first
	// argument.
	//
	// Context : called from user, or kernel.
	//	nonblocking, in case the import segment has not been
	//	disconnected, the synchronization with the import peer
	//	will happen in the background.
	//
#define	RSMT_SMEX_NO_CONNECTIONS 0x01 // destroy segment that has no connections
	void destroy_seg(rsmt_export_segment *rsmt_segp, uint32_t flags,
	    rsmt_seg_destroy_callback_t *, void *);

	// An import endpoint uses connect_seg() to connect an exported segment.
	// The segment should have been created with peer connection option
	// by the export peer. The local endpoint uses rsmt_seg_conn_token_t
	// segment connection token argument to identify the segment.
	//
	// Return Values : A pointer to the rsmt_import_segment object.
	//
	// Context : called from user or kernel.
	//	the function will not block.
	//
	rsmt_import_segment *connect_seg(rsmt_seg_conn_token_t rsmt_conn_token);

	// Disconnect the import segment. Must be called from the endpoint
	// that initiated the connect_seg().
	//
	// Context : called from user, kernel.
	//	the function will not block.
	//
	void disconnect_seg(rsmt_import_segment *rsmt_segp);

	// Retrieve the connection token
	// Connection token is for export endpoint to hand over to the
	// import endpoint during the connect_seg call
	//
	rsmt_seg_conn_token_t get_conn_token(rsmt_segment *rsmt_segp);

	// Retrieve the memory segment id from the connection token
	//
	rsm_memseg_id_t get_memseg_id_by_token(rsmt_seg_conn_token_t
		conn_token);

	// inherited from the rsmt_cmf_client class
	//
	bool cm_handle(void *msgp, ushort_t msglen, int msgid, bool flowctrl);
	// end inherited

	// used by defer task to connect and destroy segments
	//
	void defer_connect_seg(rsmt_seg_conn_token_t rsmt_conn_token);
	void defer_destroy_seg(rsmt_seg_conn_token_t rsmt_conn_token);
	// used by finalizer task to destroy all outstanding segments
	//
	void finalizer();	// delay cleanup

private:
	rsmt_adapter *sm_adp;		// Pointer to the rsmt adapter object
	rsm_addr_t sm_remote_addr; 	// Remote RSM address
	rsmt_segid_mgr *sm_segidmp;	// Pointer to the segment id manager

#define	RSMT_SMST_ENABLE	0x0001
#define	RSMT_SMST_FINALIZER	0x0010
	int sm_state;			// states
	rsmt_finalizer *sm_fp;
	// count of segments whose unpublish failed even after retrying
	static uint32_t sm_unpublish_errcnt;

	// Retrieve the memory segment size from the connection token
	//
	size_t get_memseg_sz_by_token(rsmt_seg_conn_token_t conn_token);

	// Reap the destroy segment list
	//
	void reap_destroy_seglist();

	// mutex lock to protect the segment count and conditional variable
	// to wait for the last segment to be destroyed
	os::mutex_t sm_segcnt_lock;
	os::condvar_t sm_segcnt_cv;
	int sm_segcnt;		// number of outstanding created segments

	// mutex lock to protect the sm_import_seglist and sm_unclaimed_seglist
	os::mutex_t sm_import_lock;
	// List of newly import segments that are not connected
	// by the deferred task
	IntrList < rsmt_import_segment, _SList > sm_import_seglist;

	// List of newly connected segments that have not yet been claimed
	IntrList < rsmt_import_segment, _SList > sm_unclaimed_seglist;

	// mutex lock to protect the sm_disconnect_seglist
	os::mutex_t sm_disconnect_lock;
	// List of disconnected segments that are waiting for destroy
	IntrList < rsmt_export_segment, _SList > sm_disconnect_seglist;

	// List of to be destroyed segments that are waiting for disconnection
	IntrList < rsmt_export_segment, _SList > sm_destroy_seglist;

	// Disallow assignments and pass by value
	rsmt_seg_mgr(const rsmt_seg_mgr&);
	rsmt_seg_mgr &operator = (const rsmt_seg_mgr&);

	// lint info
	rsmt_seg_mgr();
};

//
// defer tasks to support segment manager
//
enum rsmt_connect_action_type {
	RSMT_CONN_ACTION_CONNECT = CMF_MSGID_BASE + 1,
	RSMT_CONN_ACTION_DISCONNECT
};

class rsmt_connect_defer_task : public defer_task {
public:
	rsmt_connect_defer_task(rsmt_seg_mgr *, rsmt_seg_conn_token_t,
		rsmt_connect_action_type, void *);

private:
	// Inherited methods from defer_task
	void execute();

	// The segment this defer task is working on
	rsmt_seg_mgr *dt_segmgrp;		// pointer to segment manager
	rsmt_seg_conn_token_t dt_token;		// connection token
	rsmt_connect_action_type dt_type;	// connection or disconnection
	void *dt_msgp;				// pointer to control message

	// Disallow assignments and pass by value
	rsmt_connect_defer_task(const rsmt_connect_defer_task&);
	rsmt_connect_defer_task &operator = (const rsmt_connect_defer_task&);

	// lint info
	rsmt_connect_defer_task();
};

class rsmt_finalizer : public defer_task {
public:
	rsmt_finalizer(rsmt_seg_mgr *);

private:
	// Inherited methods from defer_task
	void execute();

	// The endpoint segment manger this defer task is working on
	rsmt_seg_mgr *fdt_segmgrp;

	// Disallow assignments and pass by value
	rsmt_finalizer(const rsmt_finalizer&);
	rsmt_finalizer &operator = (const rsmt_finalizer&);

	// lint info
	rsmt_finalizer();
};

#endif /* _RSMT_SEG_MGR_H */
