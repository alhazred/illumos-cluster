/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_RSMT_CMF_CLIENT_IN_H
#define	_RSMT_CMF_CLIENT_IN_H

#pragma ident	"@(#)rsmt_cmf_client_in.h	1.6	08/05/20 SMI"

inline cmf_clientid_t
rsmt_cmf_client::get_clientid() const
{
	return (clientid);
}

inline char *
rsmt_cmf_client::hdr_to_data(char *p) const
{
	return (p + sizeof (rsmt_cmhdr_t));
}

inline char *
rsmt_cmf_client::data_to_hdr(char *p) const
{
	return (p - sizeof (rsmt_cmhdr_t));
}

inline bool
rsmt_cmf_client::reserved_msgid(int msgid) const
{
	return (((msgid >= 0) && (msgid < CMF_MSGID_BASE))? true : false);
}

#endif	/* _RSMT_CMF_CLIENT_IN_H */
