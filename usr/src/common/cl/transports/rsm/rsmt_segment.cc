//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rsmt_segment.cc	1.17	08/05/20 SMI"

#ifdef	_KERNEL
#include <vm/as.h>
#endif
#include "rsmt_util.h"
#include "rsmt_segment.h"

rsmt_segment::rsmt_segment(rsmt_adapter *adp, size_t size,
	rsm_memseg_id_t id, rsm_addr_t remote_addr) :
	_SList::ListElem(this)
{
	seg_size = size;
	memseg_id = id;
	seg_remote_addr = remote_addr;
	seg_ctrl = adp->rad_rsm_ctrl;		// structure copy
}

rsmt_export_segment::rsmt_export_segment(rsmt_adapter *adp, size_t size,
	rsm_memseg_id_t id, rsm_addr_t remote_addr, void *mem_addr,
	size_t mem_len) :
	rsmt_segment(adp, size, id, remote_addr),
	ex_memseg(NULL),
	destroy_callback((rsmt_seg_destroy_callback_t *)NULL),
	callback_param(NULL)
{
	ex_memory.ms_type = rsm_memory_local_t::RSM_MEM_VADDR;	//lint !e1061
#ifdef	_KERNEL
	ex_memory.ms_as = &kas;
#else
	ex_memory.ms_as = NULL;
#endif
	ex_memory.ms_vaddr = mem_addr;
	ex_memory.ms_length = mem_len;

	seg_type = RSMT_SEG_EXPORT;

	// If a partially bound or unbound segment is created it not rebindable
	// - this is an SCI/RSMPI requirement
	if (mem_len < size) {
		rebindable = false;
	} else {
		rebindable = true;
	}
}

//
// rsmt_export_segment::set_destroy_callback
//
// Stores information about the callback function that needs to be called
// when the segment is being destroyed. The first parameter is the callback
// function. The second parameter is a pointer that needs to be passed back
// to the callback as is as the first parameter. The second pointer to the
// callback with be the pointer to the segment being destroyed.
//
void
rsmt_export_segment::set_destroy_callback(rsmt_seg_destroy_callback_t *callb,
    void *callb_param)
{
	ASSERT(destroy_callback == (rsmt_seg_destroy_callback_t *)NULL);
	destroy_callback = callb;
	callback_param = callb_param;
}

int
rsmt_export_segment::export_open(uint_t flags, rsm_resource_callback_t waitflag)
{
	int rc;

	if (ex_memory.ms_vaddr && ex_memory.ms_length) {
		rc = RSM_SEG_CREATE(seg_ctrl, &ex_memseg,
			seg_size, flags, &ex_memory, waitflag, NULL);
	} else {
		rc = RSM_SEG_CREATE(seg_ctrl, &ex_memseg,
			seg_size, flags, NULL, waitflag, NULL);
	}

	if (rc != RSM_SUCCESS) {
		ASSERT(rc == RSMERR_INSUFFICIENT_RESOURCES);
		return (rc);
	}

	rc = RSM_PUBLISH(seg_ctrl, ex_memseg, NULL, 0, memseg_id,
		NULL, NULL);

	if (rc != RSM_SUCCESS) {
		(void) RSM_SEG_DESTROY(seg_ctrl, ex_memseg);
		ex_memseg = NULL;
	}

	return (rc);
}


int
rsmt_export_segment::export_close()
{
	int rc;
	rc = RSM_UNPUBLISH(seg_ctrl, ex_memseg);
	if (rc != RSM_SUCCESS) {
		ASSERT(rc == RSMERR_SEG_IN_USE);
		return (rc);
	}
	(void) RSM_SEG_DESTROY(seg_ctrl, ex_memseg);

	if (destroy_callback != (rsmt_seg_destroy_callback_t *)NULL) {
		(*destroy_callback)(callback_param, this);
	}

	return (RSM_SUCCESS);
}

int
rsmt_export_segment::unbind()
{
	int rc;

	rc = RSM_UNBIND(seg_ctrl, ex_memseg, (off_t)0,
		ex_memory.ms_length);

	if (rc == RSM_SUCCESS) {
		ex_memory.ms_vaddr = NULL;
		ex_memory.ms_length = 0;
	}

	return (rc);
}


int
rsmt_export_segment::bind(void *mem_addr, size_t mem_len,
	rsm_resource_callback_t waitflag)
{
	int rc;

	ex_memory.ms_vaddr = mem_addr;
	ex_memory.ms_length = mem_len;

	rc = RSM_BIND(seg_ctrl, ex_memseg, (off_t)0,
		&ex_memory, waitflag, NULL);

	if (rc != RSM_SUCCESS) {
		ex_memory.ms_vaddr = NULL;
		ex_memory.ms_length = 0;
	}

	return (rc);
}

int
rsmt_export_segment::rebind(void *mem_addr, size_t mem_len,
	rsm_resource_callback_t waitflag)
{
	int rc;
	void *maddr;
	size_t len;

	maddr = ex_memory.ms_vaddr;
	len = ex_memory.ms_length;

	ex_memory.ms_vaddr = mem_addr;
	ex_memory.ms_length = mem_len;

	rc = RSM_REBIND(seg_ctrl, ex_memseg, (off_t)0,
		&ex_memory, waitflag, NULL);

	if (rc == RSM_SUCCESS)
		return (rc);

	ex_memory.ms_vaddr = maddr;
	ex_memory.ms_length = len;

	return (rc);
}

rsmt_import_segment::rsmt_import_segment(rsmt_adapter *adp, size_t size,
	rsm_memseg_id_t id, rsm_addr_t remote_addr) :
	rsmt_segment(adp, size, id, remote_addr),
	im_memseg(NULL)
{
	seg_type = RSMT_SEG_IMPORT;
}

int
rsmt_import_segment::import_open()
{
	int rc;

	rc = RSM_CONNECT(seg_ctrl, seg_remote_addr, memseg_id, &im_memseg);
	if (rc != RSM_SUCCESS) {
		return (rc);
	}
	return (RSM_SET_BARRIER_MODE(seg_ctrl, im_memseg,
	    RSM_BARRIER_MODE_EXPLICIT));
}
