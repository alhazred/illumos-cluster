//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rsmt_rio.cc	1.12	08/05/20 SMI"

//
// This file implements the rsmt_rio_client and the rsmt_rio_server
// methods. In addition, this file also provides implementations for
// the replyio related methods in the rsmt_sendstream and the
// rsmt_recstream classes.
//

#include <transports/rsm/rsmt_util.h>
#include <transports/rsm/rsmt_transport.h>
#include <transports/rsm/rsmt_rio.h>
#include <sys/ddi.h>

//
// rsmt_sendstream::check_reply_buf_support
//
// Determines if reply optimization support is available for the given
// replyio parameters. RSMT transport cares only about the size of the
// transfer when evaluating replyio support.
//
// Returns true if support is available, false otherwise.
//
#ifdef _KERNEL
bool
rsmt_sendstream::check_reply_buf_support(size_t riosize, iotype_t, void *,
	optype_t)
{
	return (rss_rep->get_rio_seg_mgr()->valid_rio_size(riosize));
}
#else
bool
rsmt_sendstream::check_reply_buf_support(size_t, iotype_t, void *, optype_t)
{
	return (false);
}
#endif

//
// rsmt_sendstream::add_reply_buffer
//
// Sets up client side replyio infrastructure and marshals rsm transport
// specific information that will be needed by the server to perform the
// data transfer into the sendstream. A call to add_reply_buffer must
// succeed. Caller is responsible for having made a call to
// check_reply_buf_support beforehand to ensure that reply optmization
// is appropriate for the message transfer size.
//
replyio_client_t *
rsmt_sendstream::add_reply_buffer(struct buf *bufp, align_t)
{
#ifndef _KERNEL
	ASSERT(!"Replyio not supported in the unode environment.");
#endif
	// Debug tracing
	debug_put_word(M_REPLY);

	//
	// Create a replyio client object. Part of this process also binds
	// the memory area where the data is to be received to an exported
	// segment. Even if errors are encountered during segment creation,
	// we pretend that every thing went fine. The server will take care
	// of recovering from segment errors.
	//
	rsmt_rio_client *riocp = new rsmt_rio_client(bufp, rss_rep);

	// Marshal generic RIO information.
	riocp->marshal_cookie(rss_MainBuf);

	// Marshal RSM transport specific RIO information.
	riocp->marshal_rio_info(rss_MainBuf);

	RSMT_MSG_DBG(RSMT_DBGL_VERBOSE, RSMT_DBGF_REPLYIO,
	    ("add_reply_buffer ss %p bufp %p len %d : riocp %p cseg %p"
	    " segid 0x%x\n", this, bufp, bufp->b_bcount, riocp,
	    riocp->get_cseg(), riocp->get_cseg() ?
	    (int)riocp->get_cseg()->get_eseg()->get_memseg_id() : 0));

	return (riocp);
}

//
// rsmt_recstream::get_reply_buffer
//
// Check if the client has requested RIO data transfer. If yes return pointer
// to a rsmt_rio_server object encapsulating the details of the rio transfer.
//
replyio_server_t *
rsmt_recstream::get_reply_buffer()
{
	rsmt_rio_server *riosp = NULL;

	// Debug tracing
	debug_get_word(M_REPLY);

	// Set up server side replyio infrastructure
	riosp = new rsmt_rio_server(MainBuf, (rsmt_endpoint *)ep);

	RSMT_MSG_DBG(RSMT_DBGL_VERBOSE, RSMT_DBGF_REPLYIO,
	    ("get_reply_buffer rs %p riosp %p sseg %p segid 0x%x\n",
	    this, riosp, riosp->get_sseg(), riosp->get_sseg() ?
	    riosp->get_sseg()->get_iseg()->get_memseg_id() : 0));

	return (riosp);
}

//
// rsmt_recstream::get_reply_buffer_cancel
//
// This method is called to discard the replyio information from the recstream
// mainBuf when the server determines that it does not want to do a replyio
// transfer .
//
void
rsmt_recstream::get_reply_buffer_cancel()
{
	// Debug tracing
	debug_get_word(M_REPLY);

	rsmt_rio_server::unmarshal_rio_info_cancel(MainBuf);
}

//
// The following method does not need to be overridden for the
// RSM transport. The default implementation is fine.
//
// rsmt_sendstream::send_reply_buf(Buf *, replyio_server_t *)
//
// The get_offline_reply method might need overriding after the base
// replyio code gets cleaned up a little bit. Currently both replyio_client_t
// and rsmt_rio_client maintain a struct buf each, which is not very nice.
//
// rsmt_recstream::get_offline_reply(replyio_client_t *)
//

//
// rsmt_rio_client constructor
//
// This object is created in rsmt_sendstream::add_reply_buffer. Sendstream
// has a hold on the endpoint.
//
rsmt_rio_client::rsmt_rio_client(struct buf *bp, rsmt_endpoint *ep) :
	replyio_client_t(bp),
	endp(ep),
	cseg(NULL),
	bufp(NULL)
{
	endp->hold();

#ifdef _KERNEL
	//
	// Clone the passed buf struct and mapin. Although the parent class,
	// replyio_client_t, keeps a pointer to the buf structure describing
	// the destination, it does not keep the buf mapped in at all times.
	// RSM transport needs the buffer mapped in at all times.
	//
	bufp = bioclone(bp, (off_t)0, bufptr->b_bcount, (dev_t)0,
	    (daddr_t)0, NULL, NULL, KM_SLEEP);
	bp_mapin(bufp);
#endif
	//
	// Allocate a exported segment bound to the memory area specified
	// in the buf struct. This segment will be used to receive the
	// bulk data.
	//
	rsmt_rio_seg_mgr *rio_smgrp = ep->get_rio_seg_mgr();
	cseg = rio_smgrp->alloc_client_seg(bufp->b_un.b_addr, bufp->b_bcount);
}

//
// rsmt_rio_client destructor
//
rsmt_rio_client::~rsmt_rio_client()
{
	rsmt_rio_seg_mgr *rio_smgrp;

	if (cseg != NULL) {
		rio_smgrp = cseg->get_seg_pool()->get_rio_seg_mgr();

		rio_smgrp->release_client_seg(cseg);
	}

#ifdef _KERNEL
	// Release the buffer
	if (bufp != NULL) {
		bp_mapout(bufp);
		biodone(bufp);
		freerbuf(bufp);
		bufp = NULL;
	}
#endif
	endp->rele();	// Matches hold in the constructor
}

//
// rsmt_rio_client::marshal_rio_info
//
// Marshal information about the replyio operation that this replyio
// client object represents into the given marshalstream. This method
// is called from add_reply_buffer after the replyio client object has
// been successfully created. The marshalled information will be used
// by the peer replyio server to determine what this replyio operation
// consists of.
//
void
rsmt_rio_client::marshal_rio_info(MarshalStream &ms)
{
	rsmt_rio_info_t rioinfo;
	rsmt_seg_mgr	*smgrp;

	if (cseg) {
		smgrp = cseg->get_seg_pool()->get_seg_mgr();
		rioinfo.conn_token = smgrp->get_conn_token(cseg->get_eseg());
		rioinfo.offset = (off_t)((unsigned long long)bufp->b_un.b_addr
		    & PAGEOFFSET);
		rioinfo.size = bufp->b_bcount;

		RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_REPLYIO,
		    ("marshal_rio_info riocp %p rioinfo %p infosize %d"
		    " rio(off=%ld,size=%ld) ms %p\n",
		    this, &rioinfo, (int)sizeof (rsmt_rio_info_t),
		    rioinfo.offset, rioinfo.size, &ms));
	} else {
		rioinfo.conn_token = RSMT_INVAL_SEG_CONN_TOKEN;
		rioinfo.offset	= 0;
		rioinfo.size	= 0;
	}

	ms.put_bytes((void *)&rioinfo, sizeof (rsmt_rio_info_t), true);
}

//
// rsmt_rio_client::release
//
// This method is called to destroy the replyio client object and any replyio
// infrastructure setup on behalf of the object. It is called either (i) by
// get_offline_reply after the replyio data has been consumed, or (ii) by the
// initiator of the replyio when it determines it needs to abort the replyio
// operation.
//
void
rsmt_rio_client::release()
{
	rele();	// Corresponds to the hold in (refcnt) constructor
}

rsmt_rio_client_seg *
rsmt_rio_client::get_cseg()
{
	return (cseg);
}

//
// rsmt_rio_server constructor
//
// This object is created in rsmt_recstream::get_reply_buffer().
//
rsmt_rio_server::rsmt_rio_server(MarshalStream &ms, rsmt_endpoint *ep) :
	replyio_server_t(ms),
	endp(ep),
	sseg(NULL),
	offset(0),
	riosize(0)
{
	endp->hold();

	// Unmarshal the rio information sent by the replyio client and
	// setup the server side infrastructure for this replyio operation.
	unmarshal_rio_info(ms);
}

//
// rsmt_rio_server destructor
//
rsmt_rio_server::~rsmt_rio_server()
{
	if (sseg != NULL) {
		rsmt_rio_seg_mgr *rio_smgrp = sseg->get_rio_seg_mgr();

		rio_smgrp->release_server_seg(sseg);
		sseg = NULL;
	}

	endp->rele();	// Matches hold in constructor
}

//
// rsmt_rio_server::unmarshal_rio_info
//
// Unmarshals the rio information sent by the replyio client and creates
// and import segment corresponding to the export segment in the above
// rio information.
//
// This is called from the rsmt_rio_server constructor
//
void
rsmt_rio_server::unmarshal_rio_info(MarshalStream &ms)
{
	rsmt_rio_info_t rioinfo;
	rsmt_rio_seg_mgr *rio_smgrp = endp->get_rio_seg_mgr();

	ms.get_bytes((void *)&rioinfo, sizeof (rsmt_rio_info_t), true);
	if (rioinfo.conn_token != RSMT_INVAL_SEG_CONN_TOKEN) {
		offset = rioinfo.offset;
		riosize = rioinfo.size;
		sseg = rio_smgrp->claim_server_seg(&rioinfo.conn_token);
	}
}

//
// rsmt_ri_server::unmarshal_rio_info_cancel
//
// This called to unmarshal and discard the rio information that the relyio
// client sent when the replyio user at the server end decided that it does
// not want to continue with the replyio operation.
//
void
rsmt_rio_server::unmarshal_rio_info_cancel(MarshalStream &ms)
{
	rsmt_rio_info_t rioinfo;

	ms.get_bytes((void *)&rioinfo, sizeof (rsmt_rio_info_t), true);
}

//
// rsmt_rio_server::send_reply_buffer
//
// Actually send the replyio data.
//
replyio_server_t::send_reply_buf_result_t
rsmt_rio_server::send_reply_buf(Buf *bufp, sendstream *)
{
	int err1, err2, err3;
	bool success = false;

	RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_REPLYIO,
	    ("send_reply_buf riosp %p bufp %p len %d\n",
	    this, bufp, bufp->span()));

	//
	// Verify that the replyio infrastructure was actually set up.
	//
	if (sseg == NULL) {
		return (SEND_REPLY_BUF_FAILED);
	}

	//
	// Make sure the endpoint over which this replyio channel has been
	// setup is still available for data transfer. There is nothing that
	// prevents the endpoint from changing state after we have made this
	// check and before we actually attempt the data transfer or even
	// during the data transfer. However, this check provides means for
	// early error detection.
	//
	if (endp->get_state() != endpoint::E_REGISTERED) {
		return (SEND_REPLY_BUF_FAILED);
	}

	//
	// Sanity check. We should not be sending more than what the client
	// had asked for. If the bulkio server did not find enough data to
	// send, it is possible that we will send less than what the client
	// had asked for.
	//
	if (bufp->span() > riosize) {
		ASSERT(!"rio size mismatch");
		return (SEND_REPLY_BUF_FAILED);
	}

	//
	// Try to do a remote put operation. A put is not successful until
	// the barrier returns with success. Retry a couple of time if
	// barrier fails.
	//
	int put_attempts = 0;
	rsm_barrier_t barrier;
	rsmt_import_segment *iseg = sseg->get_iseg();

	RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_REPLYIO,
	    ("send_reply_buf riosp %p, attempting put\n", this));
	RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_REPLYIO,
	    ("iseg = %p offset = %d addr = %p size = %d\n",
	    iseg, (int)offset, bufp->head(), (int)riosize));

	while (put_attempts++ < 10) {
		err1 = err2 = err3 = 0;
		if ((err1 = iseg->open_barrier(&barrier)) == 0 &&
		    (err2 = iseg->put(offset, bufp->head(), riosize)) == 0 &&
		    (err3 = iseg->close_barrier(&barrier)) == 0) {
			success = true;
			break;
		}
	}

	if (!success) {
		RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_REPLYIO,
		    ("send_reply_buf riosp %p, put failed attempts %d"
		    " err = %d %d %d\n",
		    this, put_attempts, err1, err2, err3));
		RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_REPLYIO,
		    ("barrier = %llx %llx %llx %llx\n", barrier.comp[0],
		    barrier.comp[1], barrier.comp[2], barrier.comp[3]));
		// increment the replyio_failures counter
		os::atomic_add_32(&rsmt_rio_seg_mgr::replyio_failures, 1);
		return (SEND_REPLY_BUF_FAILED);
	}

	bufp->done();
	RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_REPLYIO,
	    ("send_reply_buf riosp %p, put successful\n", this));

	return (SEND_REPLY_BUF_SUCCESS);
}

rsmt_rio_server_seg *
rsmt_rio_server::get_sseg()
{
	return (sseg);
}
