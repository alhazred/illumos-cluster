//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma	ident	"@(#)rsmt_cmf_client.cc	1.20	08/05/20 SMI"

#include <sys/types.h>
#include <sys/threadpool.h>

#include "rsmt_cmf.h"
#include "rsmt_cmf_client.h"


//
// The following state diagram describes the state transitions in
// the cmf client class. Messages can be sent and received by the
// client only in the CMF_CLIENT_ENABLED state.
//
//		+------------->	CMF_CLIENT_DISABLED  <------------------+
//		|			|	^			|
//		|	cm_register()	|	| cm_unregister()	|
//		|			|	|			|
//		|		CMF_CLIENT_REGISTERED			|
//		|			|				|
//		|	cm_enable() 	|				|
//		|	(phase 1)	V				|
//		|		CMF_CLIENT_ACKPENDING ------------------+
//		|			|			cm_unregister()
//		|			|
//		|			| SYNC or SYNCACK recvd from peer
//		|			|
//		|		CMF_CLIENT_ACKRECVD --------------------+
//		|			|				|
//		|			|				|
//		|			|				|
//		|	cm_enable()	|				|
//		|	(phase 2)	V				|
//		|		CMF_CLIENT_ENABLED			|
//		|			|				|
//		|	cm_unregister()	|				|
//		|			V				|
//		+-------------- CMF_CLIENT_UNREGISTERED <---------------+
//		cm_disable()					cm_unregister()
//
// cm_enable() Phase 1 - creates sendq and alloc bufs
// cm_enable() Phase 2 - initiates credit handshake and waits till the
//			 credit handshake completes.
//
//


rsmt_cmf_client::rsmt_cmf_client(int local_adapterid, nodeid_t peer_nodeid,
    int peer_adapterid, rsm_addr_t peer_hwaddr, cmf_clientid_t client_id):
	nbufs(0), bufs(NULL), freebufs(NULL), retrybufs(NULL),
	clientid(client_id),
	peer_ndid(peer_nodeid),
	peer_rsma(peer_hwaddr),
	peer_adpid(peer_adapterid),
	cmf_sendq(NULL)
{
	rsm_controller_attr_t	*attrp;

	// initialize
	nlotsfree = 0;
	ff_index = 0;
	send_credit = 0;
	due_credit = 0;
	fr_index = 0;
	lr_index = nbufs - 1;

	retrycnt = 0;
	synchandler_posted = false;
	bzero(&syncmsg, sizeof (rsmt_cminfr_msg));

	// preallocate a defer task for handling the SYNC msg
	synchandler = new rsmt_cmf_procmsg_defer_task(this, &syncmsg);

	// acquires a hold on adapter
	adapterp = rsmt_cmf_mgr::the().get_adapter(local_adapterid);

	attrp = adapterp->get_attr();
	ASSERT(attrp);
	if (attrp->attr_intr_data_size_max > 0) {
		max_cm_size = (ushort_t)(attrp->attr_intr_data_size_max -
		    sizeof (rsmt_cmhdr_t));
		bufsize	= (uchar_t)attrp->attr_intr_data_size_max;
	} else { // SCI doesn't return valid values so use default
		max_cm_size = (ushort_t)(CMF_DFLT_MSGSIZE -
		    sizeof (rsmt_cmhdr_t));
		bufsize = (uchar_t)CMF_DFLT_MSGSIZE;
	}

	// XXXX: roundup bufsize to be aligned with attr_intr_data_align

	client_state = CMF_CLIENT_DISABLED;
}

// destructor
rsmt_cmf_client::~rsmt_cmf_client()
{
	delete synchandler;
	synchandler = NULL;

	// the buffers allocated for flow-control are deleted in the
	// destructor since there might be deferred tasks referring
	// to the buffers. The buffers get deleted when all holds
	// are released

	if (bufs) {
		mem_free(bufs, (size_t)nbufs*bufsize);
	}
	bufs = NULL;

	if (freebufs) {
		mem_free(freebufs, nbufs*sizeof (void *));
	}
	freebufs = NULL;

	if (retrybufs) {
		mem_free(retrybufs, nbufs*sizeof (void *));
	}
	retrybufs = NULL;

	cmf_sendq = NULL;

	// release the hold acquired by get_adapter() in the constructor
	adapterp->rele();
	adapterp = NULL;
}

//
// register the client with the rsmt_cmf_adapter
// The state changes from CMF_CLIENT_DISABLED to CMF_CLIENT_REGISTERED
//
void
rsmt_cmf_client::cm_register()
{
	// place a hold before registering with the adapter
	hold();

	adapterp->register_client(peer_ndid, clientid, this);

	client_lock.lock();

	// cm_register is valid only in DISABLED state
	ASSERT(client_state == CMF_CLIENT_DISABLED);

	// change state to REGISTERED
	client_state = CMF_CLIENT_REGISTERED;
	client_cv.broadcast();
	client_lock.unlock();

	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_CMF,
	    ("cm_register(%p) done\n", this));

}

//
// Does the necessary setup for sending/receiving control messages.
// A client calls cm_enable from its initiate_fini() routine till cm_enable
// succeeds. After cm_enable succeeds control messages can be exchanged.
// cm_enable has 2 phases
// phase I - create all the buffers necessary to handle flow-control messages,
//	create the sendq. The state of the client changes from
//	CMF_CLIENT_REGISTERED to CMF_CLIENT_ACKPENDING
// phase II - initiate the credit sync handshake protocol by sending a
//	CMF_MSGID_SYNC message.
// phase I is done only once and subsequent calls to cm_enable executes
// only phase II.
//
// The default window_size is defined by the constant CMF_WINDOWSZ
//
bool
rsmt_cmf_client::cm_enable(uchar_t window_size)
{
	int			rc;
	void			*tmpbufs = NULL;
	void			**tmpfreebufs = NULL;
	void			**tmpretrybufs = NULL;
	rsm_send_q_handle_t	tmpsq = NULL;
	rsmt_cmf_sendq		*tmp_cmfsq = NULL;
	rsm_controller_object_t *ctlp;
	rsm_intr_service_t	peer_service;
	rsmt_cminfr_msg		msg;
	os::systime		to((os::usec_t)100000);    // 100ms

	// cm_enable should be called only once and after cm_register
	client_lock.lock();
	if (client_state == CMF_CLIENT_REGISTERED) {
		client_lock.unlock();
		// allocate buffers for flow-control and initialize them
		// if client needs buffers for flow control
		if (window_size > 0) {
			tmpbufs = mem_alloc((size_t)window_size*bufsize);
			ASSERT(tmpbufs);
			bzero(tmpbufs, (size_t)window_size*bufsize);

			tmpfreebufs = (void **)mem_alloc(
				window_size*sizeof (void *));
			ASSERT(tmpfreebufs);
			// make freebuf elements point to elements of bufs
			for (int i = 0; i < window_size; i++) {
				tmpfreebufs[i] = ((char *)tmpbufs + i*bufsize);
			}

			tmpretrybufs = (void **)mem_alloc(
				window_size*sizeof (void *));
			ASSERT(tmpretrybufs);
			bzero(tmpretrybufs, window_size*sizeof (void *));
		}

		ctlp = adapterp->get_controller();
		peer_service = rsmt_cmf_mgr::get_servicenum(peer_adpid);

		// create the interrupt sendq
		// Its created with SEND_Q_NO_FENCE - hence the sendq is not
		// failfast.
		// Since we use DELIVER mode in rsm_send meaning that the send
		// returns only after the messages are enqueue on the receiver
		// we do not need queue fence for message ordering issues on
		// error.
		rc = RSM_SENDQ_CREATE(*ctlp, peer_rsma, peer_service,
		    CMF_INTR_PRI, (ulong_t)CMF_QUEUE_SIZE,
		    RSM_INTR_SEND_Q_NO_FENCE, RSM_RESOURCE_SLEEP, NULL, &tmpsq);

		RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_CMF,
		    ("cm_enable(%p) sendq_create=%d\n", this, rc));

		if (rc != RSM_SUCCESS) {
			mem_free(tmpbufs, (size_t)window_size*bufsize);
			mem_free(tmpfreebufs, window_size*sizeof (void *));
			mem_free(tmpretrybufs, window_size*sizeof (void *));
			return (false);
		}

		// create the refcntable version of sendq
		tmp_cmfsq = new rsmt_cmf_sendq(ctlp, tmpsq);

		client_lock.lock();

		// The state changed during the setup - so clean up and return
		if (client_state != CMF_CLIENT_REGISTERED) {
			client_lock.unlock();
			mem_free(tmpbufs, (size_t)window_size*bufsize);
			mem_free(tmpfreebufs, window_size*sizeof (void *));
			mem_free(tmpretrybufs, window_size*sizeof (void *));
			tmp_cmfsq->rele(); // destroy the sendq
			return (false);
		}

		nbufs = window_size;

		// nlotsfree is percentage (defined by CMF_BUF_LOTSFREE) of nbuf
		// note: nlotsfree can be potentially 0
		nlotsfree = (uchar_t)((nbufs*CMF_BUF_LOTSFREE)/100);

		// initialize buffer pointers
		bufs = tmpbufs;
		freebufs = tmpfreebufs;
		retrybufs = tmpretrybufs;
		cmf_sendq = tmp_cmfsq;	// save the cmf_sendq object
		ff_index = 0;
		// we start with 0 credits
		send_credit = 0;
		due_credit = 0;
		// init pointers for retrybufs - it is a circular buffer
		fr_index = 0;
		lr_index = nbufs - 1;
		retrycnt = 0;
		// move state to CMF_CLIENT_ACKPENDING and wait for the SYNCACK
		// message
		client_state = CMF_CLIENT_ACKPENDING;
		client_lock.unlock();
	} else if (client_state == CMF_CLIENT_ACKRECVD) {
		// recvd SYNCACK after cm_enabled had timedout and we had
		// returned to endpoint::initiate to check endpoint state
		client_state = CMF_CLIENT_ENABLED;
		// peers have the same window size - initialize send_credits
		send_credit = nbufs;
		client_cv.broadcast();
		client_lock.unlock();
		RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_CMF,
		    ("cm_enable(%p) enable complete\n", this));
		return (true);
	} else {
		client_lock.unlock();
	}

	ASSERT(client_state == CMF_CLIENT_ACKPENDING);
	// In SYNC message the credit_update field carries the window_size
	msg.hdr.hdr.credit_update = nbufs;
	// initiate the credit synchronization handshake - send SYNC message
	if (!cm_send_internal(&msg, sizeof (msg), CMF_MSGID_SYNC,
	    CMF_FLOWCONTROL)) {
		// SYNC send failed
		client_lock.lock();
		if (client_state == CMF_CLIENT_ACKRECVD) {
			// received a SYNC message from the peer and
			// already sent a SYNCACK - so we are done
			client_state = CMF_CLIENT_ENABLED;
			client_cv.broadcast();
			client_lock.unlock();
			RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_CMF,
			    ("cm_enable(%p) enable complete\n", this));
			return (true);
		} else if (client_state == CMF_CLIENT_ACKPENDING) {
			client_lock.unlock();
			RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_CMF,
			    ("cm_enable(%p) send SYNC failed\n", this));
			// send failed means that other end is not ready
			// Sleep for 1ms so that the cmf_client does not
			// retry in a tight loop.
			os::usecsleep((os::usec_t)1000);
			// return false and this will cause cmf client to
			// check endpoint state and then retry cm_enable.
			return (false);
		}
	}

	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_CMF,
	    ("cm_enable(%p) SYNC message sent\n", this));

	client_lock.lock();

	// wait for SYNCACK or SYNC to arrive from the peer, or
	// for the state to change due to clean up calls ie.
	// cm_unregister and cm_disable
	// timeout after 100ms
	while (client_state == CMF_CLIENT_ACKPENDING) {
		if (client_cv.timedwait(&client_lock, &to) ==
		    os::condvar_t::TIMEDOUT) {
			RSMT_PATH_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_CMF,
			    ("cm_enable(%p) SYNC timeout\n", this));
			break;
		}
	}

	// got woken up due to a state change or timeout - check state
	if (client_state == CMF_CLIENT_ACKRECVD) {
		client_state = CMF_CLIENT_ENABLED;
		// peers have the same window size - initialize send_credits
		send_credit = nbufs;
		client_cv.broadcast();
		client_lock.unlock();
		RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_CMF,
		    ("cm_enable(%p) enable complete\n", this));
		return (true);
	} else { // timeout
		client_lock.unlock();
		return (false);
	}
}

//
// unregister client from the adapter
// The state changes to CMF_CLIENT_UNREGISTERED or CMF_CLIENT_DISABLED
//
void
rsmt_cmf_client::cm_unregister()
{
	client_lock.lock();

	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_CMF,
	    ("cm_unregister(%p)\n", this));

	// already unregistered/disabled
	if ((client_state == CMF_CLIENT_UNREGISTERED) ||
	    (client_state == CMF_CLIENT_DISABLED)) {
		client_lock.unlock();
		return;
	}

	if (retrycnt > 0) { // pending messages exist
		// update the adapter since retry task need not attempt
		// redelivery of the pending messages for this client
		adapterp->decr_retryclientcnt();
	}

	if ((client_state == CMF_CLIENT_ENABLED) ||
	    (client_state == CMF_CLIENT_ACKRECVD)) {
		// change state to UNREGISTERED
		client_state = CMF_CLIENT_UNREGISTERED;
	} else {
		// client has not yet been ENABLED
		client_state = CMF_CLIENT_DISABLED;
	}
	client_cv.broadcast();
	client_lock.unlock();

	adapterp->unregister_client(peer_ndid, this);

	// release corresponding to the hold in cm_register
	rele();
}

//
// cm_disable destroys the sendq.
// The state of the client is changed to CMF_CLIENT_DISABLED
//
void
rsmt_cmf_client::cm_disable()
{
	rsmt_cmf_sendq		*tmp_cmfsq = NULL;

	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_CMF,
	    ("cm_disable(%p)\n", this));

	cm_unregister();

	client_lock.lock();

	if (cmf_sendq) {
		// keep a local reference before NULLing cmf_sendq
		tmp_cmfsq = cmf_sendq;
		cmf_sendq = NULL;
	}

	// XXX: do we need to make sure retry buffers are delivered
	// is there any point - if a particular message
	// delivery fails do we violate ordering and continue to
	// to deliver the other messages.

	client_state = CMF_CLIENT_DISABLED;
	client_cv.broadcast();
	client_lock.unlock();

	// release the hold and destroy the sendq
	if (tmp_cmfsq) {
		tmp_cmfsq->rele();
	}
}


//
// This method is called by the adapter in the interrupt context.
// If its a flow controlled message, it is copied into a buffer
// so that it can be accessed from a deferred task. If there are
// pending messages to be redelivered, attempt delivery of the old
// messages first to preserve message ordering. The redelivery also
// is done in the interrupt context. If the redelivery succeeds
// the current message is delivered to the client otherwise
// the current message is enqueued onto the retry list.
//
void
rsmt_cmf_client::cm_deliver(void *msgp)
{
	rsmt_cmhdr_t	*hdrp = (rsmt_cmhdr_t *)msgp;
	int		i;
	void		*cur_buf;
	bool		handled = false;
	bool		redelivered = true;
	rsmt_cmf_procmsg_defer_task	*msg_handler;

	client_lock.lock();

	// Message is accepted only if the state is ENABLED or
	// if its a synchronization message its accepted in ACKPENDING,
	// ACKRECVD and ENABLED state - otherwise they are dropped
	if ((client_state != CMF_CLIENT_ENABLED) &&
	    (hdrp->hdr.msgid != CMF_MSGID_SYNC) &&
	    (hdrp->hdr.msgid != CMF_MSGID_SYNCACK)) {
		client_lock.unlock();
		RSMT_MSG_DBG(RSMT_DBGL_VERBOSE, RSMT_DBGF_CMF,
		    ("cm_deliver(%p): msg dropped CMF !ENABLED\n", this));
		return;
	}

	if (hdrp->hdr.msgid == CMF_MSGID_SYNC) {
		// special handling of the SYNC message
		// copy message into special sync buffer
		bcopy(msgp, &syncmsg, (size_t)hdrp->hdr.size);
		if (synchandler_posted) {
			// synchandler defer task has already been scheduled
			// for a previous SYNC msg - don't post another one
			client_lock.unlock();
			RSMT_MSG_DBG(RSMT_DBGL_VERBOSE, RSMT_DBGF_CMF,
			    ("cm_deliver(%p): SYNC - handler already posted\n",
				this));
			return;
		}
		synchandler_posted = true;
		client_lock.unlock();
		// place a hold and rsmt_cmf_procmsg_defer_task
		// will release it
		hold();
		// post it on to the common_threadpool
		common_threadpool::the().defer_processing(synchandler);
		RSMT_MSG_DBG(RSMT_DBGL_VERBOSE, RSMT_DBGF_CMF,
		    ("cm_deliver(%p): SYNC msg posted\n", this));
		return;
	} else if (hdrp->hdr.msgid == CMF_MSGID_SYNCACK) {
		// special handling of the SYNCACK message
		client_lock.unlock();
		handled = cm_procmsg_internal(msgp);
		return;
	} else {
		// all other messages are accepted only in the ENABLED state
		ASSERT(client_state == CMF_CLIENT_ENABLED);
		client_lock.unlock();
	}

	if (hdrp->hdr.flags & CMF_MSGFLAG_FLOWCTRL) { // flow controlled message
		client_lock.lock();
		if (ff_index == CMF_FFINDEX_UNKNOWN) {
			// ff_index doesn't point to an available freebuf
			// search for one
			for (i = 0; i < nbufs; i++) {
				if (freebufs[i] != NULL) {
					ff_index = i;
					break;
				}
			}
		}
		// something's gotta be free - flow controllllll
		ASSERT(ff_index != CMF_FFINDEX_UNKNOWN);

		// copy message into buffer
		bcopy(msgp, freebufs[ff_index], (size_t)hdrp->hdr.size);

		RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_CMF,
		    ("cm_deliver(%p) freebuf[%d]=%p \n", this, ff_index,
		    freebufs[ff_index]));

		cur_buf = freebufs[ff_index];
		freebufs[ff_index] = NULL;	// this element is in use now
		ff_index = CMF_FFINDEX_UNKNOWN; // trash ff_index

		// now check if there are any pending deliveries.
		// attempt redelivery here in order to maintain FIFO
		// ordering

		if (retrycnt != 0) {
			client_lock.unlock();
			redelivered = cm_redeliver();
		} else {
			client_lock.unlock();
		}

		// hand the new message over to the client
		if (redelivered) {
			// place a hold before handing it to the threadpool
			// cm_free will release it
			hold();
			if (reserved_msgid(hdrp->hdr.msgid)) {
				// this is an infrastructure message, create
				// a defer_task to handle it
				msg_handler = new(os::NO_SLEEP)
				    rsmt_cmf_procmsg_defer_task(this, cur_buf);
				if (msg_handler) {
					// post it on to the common_threadpool
					common_threadpool::the().
					    defer_processing(msg_handler);
					handled = true;
				}
			} else {
				// pass it over to the client to deal with it
				handled = cm_handle(cur_buf,
					(ushort_t)hdrp->hdr.size,
					hdrp->hdr.msgid, CMF_FLOWCONTROL);
			}

			if (!handled) {
				// threadpool handoff failed - release the hold
				rele();
			}
		}

		if (!redelivered || !handled) { // redelivery or handle failed
			// put it in the retry circular queue
			client_lock.lock();
			// advance lr_index
			if (lr_index == nbufs - 1) {
				lr_index = 0;
			} else {
				lr_index++;
			}
			retrybufs[lr_index] = cur_buf;
			retrycnt++;
			if (retrycnt == 1) { // first msg in retrybuf
				// mark the adapter that the client needs
				// to receive the redeliver call from the
				// retry_task
				adapterp->incr_retryclientcnt();
			}
			client_lock.unlock();
		}
	} else { // non-flowcontrolled messages
		if (reserved_msgid(hdrp->hdr.msgid)) {
			// infrastructure message
			handled = cm_procmsg_internal(msgp);
		} else {
			handled = cm_handle(msgp, (ushort_t)hdrp->hdr.size,
			    hdrp->hdr.msgid, CMF_NOFLOWCONTROL);
		}
	}

	RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_CMF,
	    ("cm_deliver(%p) msg=%p handled=%d\n", this, (void *)hdrp,
		handled));
}

//
// This is called by the adapters redeliver method or by cm_deliver
// to deliver previously enqueued messages pointed to by retrybufs.
// Loops through the retrybufs and process them.
//
bool
rsmt_cmf_client::cm_redeliver()
{
	void				*msgp;
	rsmt_cmhdr_t			*hdrp;
	bool				redelivered = true;
	bool				handled;
	rsmt_cmf_procmsg_defer_task	*msg_handler;

	client_lock.lock();

	if (retrycnt == 0) { // nothing to redeliver
		client_lock.unlock();
		return (true);
	}

	while (retrycnt > 0) {
		msgp = retrybufs[fr_index];
		hdrp = (rsmt_cmhdr_t *)msgp;

		client_lock.unlock();
#ifdef	DEBUG_REDELIVER
		// debugging aid set redelivery flag so that message is
		// handled here
		hdrp->hdr.flags |= CMF_MSGFLAG_REDELIVER;
#endif
		RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_CMF,
		    ("cm_redeliver(%p) msg=%p\n", this,
			msgp));

		// grab a hold before handing off to threadpool
		// will be released in cm_free
		hold();

		if (reserved_msgid(hdrp->hdr.msgid)) {
			// this is an infrastructure message, create
			// a defer_task to handle it
			msg_handler = new(os::NO_SLEEP)
			    rsmt_cmf_procmsg_defer_task(this, msgp);
			if (msg_handler) {
				// post it on to the common_threadpool
				common_threadpool::the().
				    defer_processing(msg_handler);
				handled = true;
			} else {
				handled = false;
			}
		} else {
			handled = cm_handle(msgp, (ushort_t)hdrp->hdr.size,
			    hdrp->hdr.msgid, CMF_FLOWCONTROL);
		}

		if (!handled) {   // msg handling failed
			// drop the hold since threadpool handoff failed
			rele();
			// we should be holding the lock when we drop out
			// of the loop so grab it now
			client_lock.lock();
			break;
		}

		client_lock.lock();
		if (fr_index == nbufs - 1) {
			fr_index = 0;
		} else {
			fr_index++;
		}
		retrycnt--;
	}

	if (retrycnt > 0) { // redelivery failed
		redelivered = false;
	} else { // all pending messages redelivered
		// update the adapter to indicate that the retry list is empty
		adapterp->decr_retryclientcnt();
	}

	client_lock.unlock();
	return (redelivered);
}

//
// handlers of flow controlled messages need to call this in order to
// release the message buffer. This should be called in the deferred
// thread context.
//
void
rsmt_cmf_client::cm_free(void *msgp)
{
	int i;
	rsmt_cminfr_msg		creditmsg;

	client_lock.lock();

	for (i = 0; i < nbufs; i++) {
		if (freebufs[i] == NULL) { // found an unused freebuf
			freebufs[i] = msgp;
			if (ff_index == CMF_FFINDEX_UNKNOWN) {
				ff_index = i; // update ff_index
			}
			break;
		}
	}

	due_credit++;

	RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_CMF,
	    ("cm_free(%p) due_credit=%d, nlotsfree=%d\n",
		this, due_credit, nlotsfree));

	// number of free bufs exceeds nlotsfree send a credit message
	if (due_credit >= nlotsfree) {
		creditmsg.hdr.hdr.credit_update = due_credit;
		due_credit = 0;
		client_lock.unlock();

		if (!cm_send(&creditmsg, sizeof (creditmsg),
		    CMF_MSGID_CREDIT, CMF_NOFLOWCONTROL)) {
			// credit send failed, update due_credit
			RSMT_MSG_DBG(RSMT_DBGL_VERBOSE, RSMT_DBGF_CMF,
			    ("cm_free(%p) send credit err\n", this));
			client_lock.lock();
			due_credit += creditmsg.hdr.hdr.credit_update;
			client_lock.unlock();
		}
	} else {
		client_lock.unlock();
	}

	// release the hold acquired in cm_deliver/cm_redeliver
	rele();
}

//
// This method processes all the infrastructure messages. It can be
// called either in the interrupt context or in the defered thread
// context depending on the type of the message.
//
//
// Credit sync handshake protocol
//
// CMF_CLIENT_REGISTERED
//	|
//	|		CMF_MSGID_SYNC		CMF_CLIENT_ACKPENDING /
//	|	-------------------------->	CMF_CLIENT_ACKRECVD /
//	|					CMF_CLIENT_ENABLED
//	V						|
// CMF_CLIENT_ACKPENDING				|
//	|		CMF_MSGID_SYNCACK		|
//	|	<---------------------------		|
//	V						V
// CMF_CLIENT_ENABLED				CMF_CLIENT_ENABLED
//
bool
rsmt_cmf_client::cm_procmsg_internal(void *msgp)
{
	rsmt_cmhdr_t	*hdrp = (rsmt_cmhdr_t *)msgp;
	rsmt_cminfr_msg	local_syncmsg;
	rsmt_cmhdr_t	sendmsg;

	switch (hdrp->hdr.msgid) {
	case CMF_MSGID_SYNC: // flow-controlled - deferred task context
		RSMT_MSG_DBG(RSMT_DBGL_VERBOSE, RSMT_DBGF_CMF,
		    ("cm_procmsg_internal(%p) recv SYNC\n", this));
		client_lock.lock();

		// make a local copy of the sync message
		bcopy(msgp, (void *)&local_syncmsg, sizeof (rsmt_cminfr_msg));
		// If a new SYNC msg arrives the synchandler should be reposted
		// on the threadpool - set the flag for that
		synchandler_posted = false;

		if ((client_state == CMF_CLIENT_ACKPENDING) ||
		    (client_state == CMF_CLIENT_ACKRECVD) ||
		    (client_state == CMF_CLIENT_ENABLED)) {
			// Peers should have the same window size
			ASSERT(local_syncmsg.hdr.hdr.credit_update == nbufs);

			client_lock.unlock();
			// send a syncack message
			if (!cm_send_internal(&sendmsg, sizeof (sendmsg),
			    CMF_MSGID_SYNCACK, CMF_NOFLOWCONTROL)) {
				return (true);
			}

			client_lock.lock();
			// SYNC message in ACKPENDING state is considered
			// as an acknowlegment of the fact that the peer
			// is in the process of being enabled.
			// change state to ENABLED if we are still in
			// CMF_CLIENT_ACKPENDING
			if (client_state == CMF_CLIENT_ACKPENDING) {
				client_state = CMF_CLIENT_ACKRECVD;
				client_cv.broadcast();
			}
		} else { // SYNC message is dropped in all other states
			RSMT_MSG_DBG(RSMT_DBGL_VERBOSE, RSMT_DBGF_CMF,
			    ("cm_procmsg_internal(%p) SYNC dropped\n", this));
		}
		client_lock.unlock();
		break;
	case CMF_MSGID_SYNCACK: // not flow-controlled - interrupt context
		RSMT_MSG_DBG(RSMT_DBGL_VERBOSE, RSMT_DBGF_CMF,
		    ("cm_procmsg_internal(%p) recv SYNCACK\n", this));
		client_lock.lock();
		if (client_state == CMF_CLIENT_ACKPENDING) {
			client_state = CMF_CLIENT_ACKRECVD;
			client_cv.broadcast();
		} else { // SYNCACK is dropped in all other state
			RSMT_MSG_DBG(RSMT_DBGL_VERBOSE, RSMT_DBGF_CMF,
			    ("cm_procmsg_internal(%p) SYNCACK dropped\n",
				this));
		}
		client_lock.unlock();
		break;
	case CMF_MSGID_CREDIT: // not flow-controlled - interrupt context
		client_lock.lock();
		RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_CMF,
		    ("cm_procdmsg_internal(%p) recv CREDIT"
			" credit_update=%d, send_credit=%d\n",
			this, hdrp->hdr.credit_update, send_credit));
		if (client_state == CMF_CLIENT_ENABLED) {
			send_credit += hdrp->hdr.credit_update;
			client_cv.broadcast();
		} else {
			RSMT_MSG_DBG(RSMT_DBGL_VERBOSE, RSMT_DBGF_CMF,
			    ("cm_procmsg_internal(%p) CREDIT dropped\n", this));
		}
		client_lock.unlock();
		break;
	default:
		ASSERT(0);
		break;
	} // switch (hdrp->hdr.msgid)

	return (true);
}

//
// This method sends the message, msgp points to the message that needs
// to be sent, this message should have the header embedded in it.
// msglen is total length of the message including header. msgid is defined
// by the client and should be > CMF_MSGID_BASE for client defined messages.
// mode is either CMF_FLOWCONTROL or CMF_NOFLOWCONTROL.
//
// In order to send a message the client should be in CMF_CLIENT_ENABLED
// state. If a flow-controlled  message is being sent it will be sent if
// there is a send_credit available, otherwise the call blocks till credits
// become available.
//
//
bool
rsmt_cmf_client::cm_send(void *msgp, size_t msglen, int msgid, bool mode)
{
	rsmt_cmhdr_t		*hdrp = (rsmt_cmhdr_t *)msgp;
	rsm_controller_object_t *ctlp;
	rsm_send_q_handle_t	sendq;
	rsmt_cmf_sendq		*tmp_cmfsq = NULL;
	rsm_send_t		is;
	int			retry = CMF_SEND_RETRY;
	int			rc;

	client_lock.lock();

	if (mode) { // CMF_FLOWCONTROL
		// credit check - wait till credits become available
		// or the client_state changes
		while ((client_state == CMF_CLIENT_ENABLED) &&
		    (send_credit == 0)) {
			if (client_cv.wait_sig(&client_lock) ==
				os::condvar_t::SIGNALED) {
				client_lock.unlock();
				return (false);
			}
		}
	}

	if (client_state != CMF_CLIENT_ENABLED) {
			client_lock.unlock();
			return (false);
	}

	if (mode) { // CMF_FLOWCONTROL
		ASSERT(send_credit > 0); // got to have some credit here
		send_credit--; // save a buffer at the peer for this message
	}

	ASSERT(cmf_sendq); // in ENABLED state

	// grabs a hold on the sendq, so that we can safely drop the
	// lock and do the RSM_SEND
	sendq = cmf_sendq->get_sendq();
	// save a local reference so that even if cm_disable
	// is called after dropping the lock, we can do the rele()
	tmp_cmfsq = cmf_sendq;

	client_lock.unlock();

	ctlp = adapterp->get_controller();

	RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_CMF,
	    ("cm_send(%p) msgp(%p) msgid=%d\n", this, (void *)msgp, msgid));

	// fill the header
	hdrp->hdr.magic = 1;
	hdrp->hdr.src = (uchar_t)orb_conf::local_nodeid(); // get local node id
	hdrp->hdr.size = (uchar_t)msglen;
	if (msgid != CMF_MSGID_CREDIT) {
		// XXX: This might be unnecessary - this field currently is
		// used only for CMF_MSGID_CREDIT
		hdrp->hdr.credit_update = 0;
	}
	hdrp->hdr.flags = 0;
	if (mode) { // CMF_FLOWCONTROL)
		hdrp->hdr.flags |= CMF_MSGFLAG_FLOWCTRL;
	}
	hdrp->hdr.clientid = (ushort_t)clientid;
	hdrp->hdr.msgid = (ushort_t)msgid;

	// create the send structure
	// send mode is DELIVER and wait mode is SLEEP
	// this means RSM_SEND returns after the message has been successfully
	// posted on the remote node and since is_wait is 0 it will wait
	// forever
	is.is_data = msgp;
	is.is_size = msglen;
	is.is_flags = RSM_INTR_SEND_DELIVER | RSM_INTR_SEND_SLEEP;
	is.is_wait = 0;

	do {
		rc = RSM_SEND(*ctlp, sendq, &is, (rsm_barrier_t *)NULL);

		retry--;

		RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_CMF,
		    ("cm_send(%p) msgp(%p) send=%d\n", this, (void *)msgp, rc));

		ASSERT(rc != RSMERR_BAD_BARRIER_HNDL &&
		    rc != RSMERR_QUEUE_FENCE_UP);

		if ((rc == RSM_SUCCESS) || (rc == RSMERR_CONN_ABORTED) ||
		    (rc == RSMERR_TIMEOUT) ||
		    (rc == RSMERR_COMM_ERR_MAYBE_DELIVERED)) {
			// send succeeded or unrecoverable failure - no retry
			break;
		} else {
			// transient error - backoff and retry a little later
			if (retry == 0) {
				// XXX: Sleep for 1ms - need to tune
				os::usecsleep((os::usec_t)1000);
				// reset retry count and start again
				retry = CMF_SEND_RETRY;
			}
		}
		//
		// SCI driver may return RSMERR_BARRIER_FAILURE
		// instead of RSMERR_CONN_ABORTED when the peer node
		// goes down. Thus RSMERR_BARRIER_FAILURE may indicate
		// either a permanent or a transient error condition.
		// The while condition below will take us out of the loop
		// if the error condition is not a transient one.
		//
	} while (client_state == CMF_CLIENT_ENABLED);

	tmp_cmfsq->rele(); // release the hold acquired during get_sendq

	if ((rc != RSM_SUCCESS) && mode) { // CMF_FLOWCONTROL
		// free up the credit that was consumed for this message
		client_lock.lock();
		send_credit++;
		client_cv.broadcast();
		client_lock.unlock();
		return (false);
	}

	return (true);
}

//
// This is an send routine used by the framework when it wants
// to bypass credit checks, state check etc. - currently
// used to send the SYNC and SYNCACK messages
//
bool
rsmt_cmf_client::cm_send_internal(void *msgp, size_t msglen, int msgid,
bool mode)
{
	rsmt_cmhdr_t		*hdrp = (rsmt_cmhdr_t *)msgp;
	rsm_controller_object_t *ctlp;
	rsm_send_q_handle_t	sendq;
	rsmt_cmf_sendq		*tmp_cmfsq = NULL;
	rsm_send_t		is;
	int			retry = CMF_SEND_RETRY;
	int			rc;

	ctlp = adapterp->get_controller();

	RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_CMF,
	    ("cm_send_internal(%p) msglen=%d msgid=%d\n", this, msglen, msgid));

	client_lock.lock();
	if (cmf_sendq) {
		// grabs a hold on the sendq, so that we can safely drop the
		// lock and do the RSM_SEND
		sendq = cmf_sendq->get_sendq();
		// save a local reference so that even if cm_disable
		// is called after dropping the lock, we can do the rele()
		tmp_cmfsq = cmf_sendq;
		client_lock.unlock();
	} else {
		client_lock.unlock();
		return (false);
	}


	// fill the header
	hdrp->hdr.magic = 1;
	hdrp->hdr.src = (uchar_t)orb_conf::local_nodeid(); // get local node id
	hdrp->hdr.size = (uchar_t)msglen;
	hdrp->hdr.flags = 0;
	if (mode) { // CMF_FLOWCONTROL
		hdrp->hdr.flags |= CMF_MSGFLAG_FLOWCTRL;
	}
	hdrp->hdr.clientid = clientid;
	hdrp->hdr.msgid = (ushort_t)msgid;

	// create the send structure
	// send mode is DELIVER and wait mode is SLEEP
	// this means RSM_SEND returns after the message has been successfully
	// posted on the remote node and since is_wait is 0 it will wait
	// forever
	is.is_data = msgp;
	is.is_size = msglen;
	is.is_flags = RSM_INTR_SEND_DELIVER | RSM_INTR_SEND_SLEEP;
	is.is_wait = 0;

	do {
		rc = RSM_SEND(*ctlp, sendq, &is, (rsm_barrier_t *)NULL);

		retry--;

		if (rc != 0) {
			RSMT_MSG_DBG(RSMT_DBGL_VERBOSE, RSMT_DBGF_CMF,
			    ("cm_send_internal(%p) send=%d\n", this, rc));
		}

		ASSERT(rc != RSMERR_BAD_BARRIER_HNDL &&
		    rc != RSMERR_QUEUE_FENCE_UP);

		if ((rc == RSM_SUCCESS) || (rc == RSMERR_CONN_ABORTED) ||
		    (rc == RSMERR_TIMEOUT) ||
		    (rc == RSMERR_COMM_ERR_MAYBE_DELIVERED)) {
			// send succeeded or unrecoverable failure - no retry
			break;
		} else {
			// transient error - backoff and retry a little later
			if (retry == 0) {
				// XXX: Sleep for 1ms - need to tune
				os::usecsleep((os::usec_t)1000);
				// reset retry count and start again
				retry = CMF_SEND_RETRY;
			}
		}
		//
		// SCI driver may return RSMERR_BARRIER_FAILURE
		// instead of RSMERR_CONN_ABORTED when the peer node
		// goes down. Thus RSMERR_BARRIER_FAILURE may indicate
		// either a permanent or a transient error condition.
		// The while condition below will take us out of the loop
		// if the error condition is not a transient one.
		//
	} while (client_state != CMF_CLIENT_UNREGISTERED &&
		client_state != CMF_CLIENT_DISABLED);

	tmp_cmfsq->rele(); // release the hold acquired during get_sendq

	return ((rc == RSM_SUCCESS)? true : false);
}

//
// currently the two way send and reply is not implemented
//
bool
rsmt_cmf_client::cm_send_and_reply(void *, size_t, int, void *, size_t)
{
	return (false);
}

//
// currently the two way send and reply is not implemented
//
bool
rsmt_cmf_client::cm_reply(void *, void *)
{
	return (false);
}

//
// This defer task is used to process the flow-controlled infrastructure
// messages
//
rsmt_cmf_procmsg_defer_task::rsmt_cmf_procmsg_defer_task(
	rsmt_cmf_client *clientp, void *msgp):
	cmf_client(clientp),
	cmf_msgp(msgp)
{
}

void
rsmt_cmf_procmsg_defer_task::execute()
{
	rsmt_cmhdr_t *hdrp = (rsmt_cmhdr_t *)cmf_msgp;

	// handles only infrastructure messages
	if (!cmf_client->cm_procmsg_internal(cmf_msgp)) {
		RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_CMF,
		    ("cm_procmsg_internal(%p) failed\n", this));
	} else {
		// SYNC message is stored a special buffer so
		// call cm_free only for other message types
		if (hdrp->hdr.msgid == CMF_MSGID_SYNC) {
			// release the hold which normally happens in cm_free
			cmf_client->rele();
			// This is the sync handler return without
			// destroying it here.
			return;
		} else {
			// free up the buffer
			cmf_client->cm_free(cmf_msgp);
		}
	}

	// delete the defer_task object
	delete this;
}

//
// a refcnted version of the sendq
//
rsmt_cmf_sendq::rsmt_cmf_sendq(rsm_controller_object_t *cp,
	rsm_send_q_handle_t sq):
	sendq(sq),
	ctlp(cp)
{
	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_CMF,
	    ("rsmt_cmf_sendq(%p): constructed\n", this));
}

//
// Grabs a hold and returns the rsm sendq
//
rsm_send_q_handle_t
rsmt_cmf_sendq::get_sendq()
{
	hold();
	return (sendq);
}

//
// Destroys the sendq and deletes the rsmt_cmf_sendq object
// when the refcnt goes to zero.
//
void
rsmt_cmf_sendq::refcnt_unref()
{
	(void) RSM_SENDQ_DESTROY(*ctlp, sendq);

	RSMT_PATH_DBG(RSMT_DBGL_INFO, RSMT_DBGF_CMF,
	    ("rsmt_cmf_sendq(%p) sendq destroy(%p)\n", this, sendq));
	delete this;
}

// destructor
rsmt_cmf_sendq::~rsmt_cmf_sendq()
{
	RSMT_PATH_DBG(RSMT_DBGL_INFO, RSMT_DBGF_CMF,
	    ("rsmt_cmf_sendq(%p): destructed\n", this));
}
