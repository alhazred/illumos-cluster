/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RSMT_RESOURCE_H
#define	_RSMT_RESOURCE_H

#pragma ident	"@(#)rsmt_resource.h	1.7	08/05/20 SMI"

#include <orb/monitor/monitor.h>
#include "rsmt_transport.h"
#include "rsmt_resource_client.h"

// RSMT Resource registry is the base class that supports resource
// management. It has two derived classes rsmt_segresc_registry and
// rsmt_memresc_registry that supports segment and memory resource
// management respectively.
//
// It is also a subclass of defer_task such that all the resource
// policy can be exercised in a deferred thread. The registry_resource_free
// is the method that allows resources to be free up.
//
// The rsmt_resc_registry will connect up with a rsmt_resource_client
// through the registry_register/registry_unregister methods.
//
class rsmt_resc_registry : public defer_task {
public:
	// constructor and destructor
	rsmt_resc_registry();
	~rsmt_resc_registry();

	// retrieve the pointer to the corresponding rsmt_resource_client
	rsmt_resource_client *get_clientp();

	// virtual method to handle resource free
	virtual void registry_resource_free() = 0;

	// registry register and unregister
	void registry_register(rsmt_resource_client *);
	void registry_unregister();

protected:
	rsmt_resource_client *resc_clientp;

	enum {
		ACTIVE_CB = 0x01,	// callback active
		UNREGISTERING = 0x02	// unregister pending
	};

	uint_t 		resc_flags;	// execution state of the registry
	os::mutex_t 	resc_lock;	// protection lock and conditional
	os::condvar_t 	resc_cv;	// variable of the resc_flags

	// Inherited method from defer_task
	void execute();

private:
	// Disallow assignments and pass by value
	rsmt_resc_registry(const rsmt_resc_registry&);
	rsmt_resc_registry &operator = (const rsmt_resc_registry&);
};

// RSMT segresc registry is a derived class from rsmt_resc_registry
// that handle segment resource management.
class rsmt_segresc_registry : public rsmt_resc_registry {
public:
	rsmt_segresc_registry();

	// method that handles segment request from rsmt_resource manager
	// argument is
	// type size_t : maximium segment size
	void registry_segreq(size_t);
	// method that invokes the rsmt_resource_client segment free
	void registry_resource_free();

private:
	size_t	resc_seg_reqsz;

	// Disallow assignments and pass by value
	rsmt_segresc_registry(const rsmt_segresc_registry&);
	rsmt_segresc_registry &operator = (const rsmt_segresc_registry&);
};

// RSMT memresc registry is a derived class from rsmt_resc_registry
// that handle memory resource management.
class rsmt_memresc_registry : public rsmt_resc_registry {
public:
	rsmt_memresc_registry();

	// method that handles memory request from rsmt_resource manager
	// argument is
	// type monitor::system_state_t : system state from memory monitor
	void registry_memreq(monitor::system_state_t);
	// method that invokes the rsmt_resource_client memory free
	void registry_resource_free();

private:
	monitor::system_state_t resc_mem_state;

	// Disallow assignments and pass by value
	rsmt_memresc_registry(const rsmt_memresc_registry&);
	rsmt_memresc_registry &operator = (const rsmt_memresc_registry&);
};

// This is the RSMT global resource manager. It manages resources like
// memory and segment and provides the overall framework and flow control.
// It allows resource clients during runtime to request segment resources
// in advance in such a way that other resource clients that holding up
// a lot of unused segments could start to them free up.
//
// The global resource manager also subscribes to the memory monitor
// and dispatches the memory requests to the resource clients when
// the system is low in free memory.
class rsmt_resource {
public:
	// Creates the rsmt_resource object
	static void initialize();

	// Destroys the rsmt_resource object
	void shutdown();

	// Returns reference to the rsmt_resource object
	static rsmt_resource &the();

	// static : global memory free function for the memory monitor
	static void rsmt_mem_monitor(monitor::system_state_t state);

	// Register a resource segment from a client, arguments are :
	// type int : adapter id,
	// type rsmt_resource_client * : resource client instance
	void register_resource_segment(int, rsmt_resource_client *);

	// Unregister a resource segment from a client, argument are :
	// type int : adapter id,
	// type rsmt_resource_client * resource client instance
	void unregister_resource_segment(int, rsmt_resource_client *);

	// dynamic segment grow request, arguments are :
	// type int : adapter id,
	// type rsmt_resource_client * resource client instance
	// type size_t : segment size
	void segment_resource_request(int, rsmt_resource_client *, size_t);

	// Register a resource memory from a client, arguments are :
	// type int : adapter id,
	// type rsmt_resource_client * resource client instance
	void register_resource_memory(int, rsmt_resource_client *);

	// Unregister a resource memory from a client, argument are :
	// type int : adapter id,
	// type rsmt_resource_client * resource client instance
	void unregister_resource_memory(int, rsmt_resource_client *);

	// method that dispatches memory requests to the resource clients
	// type monitor::system_state_t : system free memory state
	void memory_resource_request(monitor::system_state_t);
private:
	// single instance of the rsmt resource
	static rsmt_resource *the_rsmt_resource;

	// mutex lock to protect the segresc_cblist
	os::mutex_t segresc_cblock;
	// List of the resource client callback functions
	SList<rsmt_segresc_registry> segresc_cblist[RSMT_MAX_ADAPTERS];

	// mutex lock to protect the memresc_cblist, memresc_maxadapter and
	// monitor_subscribed
	os::mutex_t memresc_cblock;
	// List of the resource client callback functions
	SList<rsmt_memresc_registry> memresc_cblist[RSMT_MAX_ADAPTERS];
	// max. adapter id registered
	int memresc_maxadapter;
	// memory monitor subscritpion state
	bool	monitor_subscribed;

	// Disallow assignments and pass by value
	rsmt_resource(const rsmt_resource&);
	rsmt_resource &operator = (const rsmt_resource&);

	rsmt_resource();
};
#endif /* _RSMT_RESOURCE_H */
