//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rsmt_endpoint.cc	1.58	08/05/20 SMI"

#include "rsmt_endpoint.h"
#include "rsmt_streams.h"
#include "rsmt_util.h"
#include "rsmt_segment.h"
#include "rsmt_seg_mgr.h"

//
// A very rudimentary form of resource management is currently implemented
// by limiting the total amount of memory an endpoint can export. This limit
// is applied to the bufferio segments in determining their size at the
// time segments get created in seggroups. Replyio segments in segment pools
// that are cacheable are also subject to this resource management.
//
// The following segments that are exempted from the resource management -
//	- pathend segment
//	- partition avail segment
//
// The defined limit is not a hard limit, at any instant the total amount
// of memory exported or imported can exceed the limit but it will be
// throttled back to below the limit in steady state
//

// Default total bio seg size limit is 32M
// Calculation based on 8 node cluster with 2 adapters using 512MB
size_t rsmt_total_bio_seg_size_per_endpoint = 32*1024*1024;
// Default total rio seg size limit is infinite - ie no restrictions
size_t rsmt_total_rio_seg_size_per_endpoint = RSMT_INFINITE_SEG_SIZE;

//
// The current default limit for SCI is 6MB - this figure is based on the fact
// that SCI adapters can import 4K(ATT entries)*64K(per ATT entry) = 256M, on
// an 8 node cluster if we want to limit rsm-transport to use about 15% of the
// importable memory it comes to about 6MB. On smaller clusters this can
// potentially be tuned to a larger number.
//
// The total space is shared between REPLYIO and BUFFERIO in the ratio of 2:1.
//

// 2MB for Bufferio on SCI
size_t rsmt_sci_total_bio_seg_size_per_endpoint = 2*1024*1024;
// 4MB for Replyio on SCI
size_t rsmt_sci_total_rio_seg_size_per_endpoint = 4*1024*1024;

//
// The bufferio and replyio limits for unode are as follows
//
// Bufferio in UNODE is limited to 1M per endpoint
size_t rsmt_unode_total_bio_seg_size_per_endpoint = 1*1024*1024;
// Replyio in UNODE is not exercised hence use the default -
// RSMT_INFINITE_SEG_SIZE.

//
// The following are parameters that are passed to the bio
// segment manager.
//
// RSMT_BIO_MAX_SEGMENTS	: maximum number of bio segments
// RSMT_BIO_MAX_JUMBO_SEGMENTS	: maximum number of jumbo segments
// RSMT_BIO_MAX_JUMBO_SEG_SIZE	: maximum size of the jumbo segment
//
const int RSMT_BIO_MAX_SEGMENTS		= 200;
const int RSMT_BIO_MAX_JUMBO_SEGMENTS	= 1;
const size_t RSMT_BIO_MAX_JUMBO_SEG_SIZE	= ((size_t)(256*1024));

rsmt_endpoint::rsmt_endpoint(transport *tr, rsmt_pathend *pep,
	int ladapter_id, nodeid_t remote_nodeid,
	int remote_adapter_id, rsm_addr_t remote_addr) :
	endpoint(tr, pep)
{
	size_t bio_total_seg_size;
	size_t rio_total_seg_size;

	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_ENDPOINT,
	    ("EP(%p) created\n", this));

	rep_rpe = pep;

	rep_remote_nodeid	= rep_rpe->rpe_remote_nodeid;
	rep_remote_incn		= rep_rpe->rpe_remote_incn;

	rep_segmgrp = new rsmt_seg_mgr((rsmt_transport *)tr,
	    rep_rpe->rpe_adap, ladapter_id, remote_nodeid,
	    remote_adapter_id, remote_addr);

	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_ENDPOINT,
	    ("EP(%p) seg_mgr(%p)\n", this, rep_segmgrp));

	// For sci use the specified limit
	if (strcmp(rep_rpe->rpe_adap->rad_ctrl_name, RSM_ADPNAME_SCI) == 0) {
		bio_total_seg_size = rsmt_sci_total_bio_seg_size_per_endpoint;
		rio_total_seg_size = rsmt_sci_total_rio_seg_size_per_endpoint;
	} else {
		// For other interconnects
#ifdef _KERNEL
		bio_total_seg_size = rsmt_total_bio_seg_size_per_endpoint;
#else // UNODE
		bio_total_seg_size = rsmt_unode_total_bio_seg_size_per_endpoint;
#endif
		rio_total_seg_size = rsmt_total_rio_seg_size_per_endpoint;
	}



	// Create the bufferio manager
	bio_mgrp = new rsmt_bio_mgr(this, rep_segmgrp, ladapter_id,
	    remote_nodeid, remote_adapter_id, remote_addr,
	    RSMT_BIO_MAX_SEGMENTS,
	    RSMT_BIO_MAX_SEGGRPS, // max segments per seggrp is 1 currently
	    bio_total_seg_size, // total seg size available to bio
	    RSMT_BIO_MAX_JUMBO_SEGMENTS,
	    RSMT_BIO_MAX_JUMBO_SEG_SIZE);

	// Create the replyio segment manager
	rio_smgrp = new rsmt_rio_seg_mgr(rep_segmgrp,
	    rep_rpe->rpe_adap->get_max_segment_size(), rio_total_seg_size,
	    ladapter_id, remote_nodeid, remote_adapter_id, remote_addr);
}

rsmt_endpoint::~rsmt_endpoint()
{
	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_ENDPOINT,
	    ("EP(%p) deleted\n", this));

	rep_rpe = NULL;

	// Release replio segment manager and common segment manager
	// and the bugfferio manager
	rio_smgrp->rele();
	bio_mgrp->rele();
	rep_segmgrp->rele();
	// lint is unaware of refcnt and thinks there is a memory leak
	bio_mgrp = NULL; //lint !e423
	rio_smgrp = NULL; //lint !e423
	rep_segmgrp = NULL; //lint !e423
}

void
rsmt_endpoint::add_resources(resources *)
{
	// oh, let's not do anything here, shall we?
}

sendstream *
rsmt_endpoint::get_sendstream(resources *resp)
{
	rsmt_sendstream *ssp;
	Environment *e = resp->get_env();
	os::mem_alloc_type mat;
	bool nonblocking;

	mat = e->nonblocking_type();
	nonblocking = (mat == os::NO_SLEEP);

	// lint thinks this form of new is "operator new(unsigned int, int)"
	// and conflicts with one of the new operators in kos_in.h.
	// Don't know that we can do anything because we don't determine
	// the type of the first arg.

	ssp = new(mat) rsmt_sendstream(resp,	//lint !e516
	    rep_remote_nodeid, rep_remote_incn, this, nonblocking);
	ASSERT(ssp != NULL);
	return (ssp);
}

void
rsmt_endpoint::initiate()
{
	endpoint_state	state;
	bool		success;

	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_ENDPOINT,
	    ("EP(%p) initiate\n", this));

	unlock();

	rep_segmgrp->initiate_init();

	while (1) {
		if (rep_segmgrp->initiate_fini())
			break;
		lock();
		state = get_state();
		if (state != E_CONSTRUCTED) {
			rep_segmgrp->initiate_cleanup();
			return;		// return under lock held
		}
		unlock();
	}
	//
	// Initialize the bufferio manager
	//
	bio_mgrp->initiate_init();
	while (true) {
		if (bio_mgrp->initiate_fini())
			break;
		lock();
		if (get_state() != E_CONSTRUCTED) {
			unlock();
			bio_mgrp->initiate_cleanup();
			rep_segmgrp->initiate_cleanup();
			lock();
			return;	// return with lock held
		}
		unlock();
	}

	//
	// Initialize the replyio segment manager
	//
	rio_smgrp->initiate_init();
	lock();
	success = false;
	while (get_state() == E_CONSTRUCTED && !success) {
		unlock();
		success = rio_smgrp->initiate_fini();
		lock();
	}
	if (get_state() != E_CONSTRUCTED) {
		unlock();
		rio_smgrp->initiate_cleanup();
		lock();
	}

	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_ENDPOINT,
	    ("EP(%p) initiate done, state = %d\n", this,
	    (int)get_state()));
}

// unblock is called when the pathend associated with the endpoint
// is declared down or removed.  unblock can be called from the
// clock interrupt.
//
void
rsmt_endpoint::unblock()
{
	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_ENDPOINT,
	    ("EP(%p) unblock\n", this));

	rio_smgrp->unblock();
	bio_mgrp->unblock();
	rep_segmgrp->unblock();
}

// After unblock(), push is called to push up to the orb any messages
// that we have already acknowledged to the sending side that we
// have received.
//
void
rsmt_endpoint::push()
{
	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_ENDPOINT,
	    ("EP (%p) push\n", this));

	// We don't acknowledge messages are received until
	// pushed up so we don't need to do anything here.
	bio_mgrp->push();
}

// cleanup is called after unblock() and push() and cleans
// up resources used for the endpoint.
//
void
rsmt_endpoint::cleanup()
{
	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_ENDPOINT,
	    ("EP(%p) cleanup\n", this));

	//
	// starts common segment manager cleanup first
	//
	rep_segmgrp->cleanup_init();

	//
	// Initiate the bufferio resource cleanup
	//
	bio_mgrp->cleanup_init();

	//
	// Trigger release of the replyio resources
	//
	rio_smgrp->cleanup_init();

	// cleanup_fini routines might block, drop the endpoint lock
	unlock();

	//
	// Cleanup operations that initiate segment destruction etc.
	//
	bio_mgrp->cleanup_fini_pre_sm();
	rio_smgrp->cleanup_fini_pre_sm();

	//
	// Segment manager final cleanup, The segment manager will be working
	// on destruction of all segments upon return from this call.
	//
	rep_segmgrp->cleanup_fini();

	//
	// Wrap up cleanup. Operations that need to wait for the segment
	// manager to have destroyed the segments.
	//
	bio_mgrp->cleanup_fini_post_sm();
	rio_smgrp->cleanup_fini_post_sm();

	lock();
	rep_rpe = NULL;

	//
	// Release our association with the pathend so that the underlying
	// path/adapter resources can be released (if needed).
	//
	release_pathend();
}

rsmt_bio_mgr *
rsmt_endpoint::get_bio_mgrp()
{
	return (bio_mgrp);
}

int rsmt_die1;

endpoint::endpoint_state
rsmt_endpoint::rep_endpoint_state()
{
	endpoint_state state;

	lock();
	state = get_state();
	unlock();
	return (state);
}

rsmt_rio_seg_mgr *
rsmt_endpoint::get_rio_seg_mgr()
{
	return (rio_smgrp);
}

//
// Mark the endpoint as faulted, this will cause the path to be brought down.
// This is done when repeated attempts to transfer data via rsm_put fails,
// since that is indicative of the path's poor health.
//
void
rsmt_endpoint::abort_endpoint()
{
	lock();
	if (rep_rpe) {
		rep_rpe->mark_ep_faulted();
	}
	unlock();
}
