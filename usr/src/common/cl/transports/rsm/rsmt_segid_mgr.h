/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RSMT_SEGID_MGR_H
#define	_RSMT_SEGID_MGR_H

#pragma ident	"@(#)rsmt_segid_mgr.h	1.11	08/05/20 SMI"

#include <sys/types.h>
#include <sys/rsm/rsmpi.h>
#include <sys/os.h>

#define	RSMT_SEGID_ALLOC_UNIT	(2 * 1024)
#define	BITS_PER_BYTE		(8)

//
// rsmt_segid_mgr : global segment id manager
//
// Manages segment id allocation for segments created by
// RSM Transport.
//
class rsmt_segid_mgr {
public:
	rsmt_segid_mgr(rsm_memseg_id_t base, uint32_t end);
	~rsmt_segid_mgr();

	// Allocate/free a memory segment id from the global segment manager
	rsm_memseg_id_t segid_alloc();
	void segid_free(rsm_memseg_id_t memseg_id);

	bool is_segid_valid(rsm_memseg_id_t memseg_id);
private:
	rsm_memseg_id_t sim_base;	// base memseg_id
	uint32_t sim_end;		// max memseg_id that can be used
	uint32_t sim_limit;		// memseg_id used so far
	uint32_t sim_pool_sz;		// size of segid_mgr allocation pool
	uchar_t *sim_pool;		// base pointer to allocation pool
	uchar_t *sim_pool_hint;		// hint pointer for quick access

	// mutex lock to synchronize alloc and free
	os::mutex_t	sim_pool_lock;

	// Disallow assignments and pass by value
	rsmt_segid_mgr(const rsmt_segid_mgr&);
	rsmt_segid_mgr &operator = (const rsmt_segid_mgr&);

	// lint info.
	rsmt_segid_mgr();
};
#endif /* _RSMT_SEGID_MGR_H */
