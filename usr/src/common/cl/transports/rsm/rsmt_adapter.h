/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RSMT_ADAPTER_H
#define	_RSMT_ADAPTER_H

#pragma ident	"@(#)rsmt_adapter.h	1.17	08/05/20 SMI"

#include <orb/transport/transport.h>
#include "rsmt_transport.h"

#define	RSM_ADPNAME_SCI "sci"
class rsmt_adapter : public adapter {
public:
	rsmt_adapter(rsmt_transport *, clconf_adapter_t *);
	~rsmt_adapter();

	//
	// Returns the maximum segment size that this adapter can support.
	// These is a theoretical limit. Segment allocation requests for
	// smaller than the max size can also fail depending on dynamic
	// system state.
	//
	size_t	get_max_segment_size();

	// Return the value of rad_barriers_for_hb
	bool	use_barriers_for_hb();

	rsm_controller_object_t	rad_rsm_ctrl;
	size_t			rad_ctrl_pagesize;
	rsm_addr_t		rad_ctrl_nodeid;
	const char		*rad_ctrl_name;

private:
	char		*rad_adpname;
	uint_t		rad_dev_inst;

	// indicates whether it is safe to use barriers to enforce ordering
	// in the clock cyclic context while sending heartbeat buffer in
	// the rsmt_pathend::pm_put_internal routine.
	bool		rad_barriers_for_hb;
	// indicates whether the adapter is registered with CMF
	bool		rad_cmf_registered;

	//
	// RSM controller attributes retrieved are retrieved from the
	// device driver and cached with the rsmt_adapter object in
	// the constructor.
	//
	rsm_controller_attr_t	*attrp;

	// Disallow copy constructor and assignment (lint warns
	// about having default methods of these when the constructor
	// uses new).
	//
	rsmt_adapter(const rsmt_adapter &);
	rsmt_adapter &operator = (const rsmt_adapter &);

	// Pacify lint
	rsmt_adapter();
};

#endif	/* _RSMT_ADAPTER_H */
