/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_RSMT_BIO_IN_H
#define	_RSMT_BIO_IN_H

#pragma ident	"@(#)rsmt_bio_in.h	1.7	08/05/20 SMI"

// Returns the total number of partitions in the bufferio segment
inline rsmt_seg_mgr *
rsmt_bio_mgr::get_segmgr()
{
	return (segmgr);
}

inline rsmt_endpoint *
rsmt_bio_mgr::get_endpoint()
{
	return (bio_rep);
}

inline hrtime_t *
rsmt_bio_mgr::get_pavail_seginfo()
{
	// the segment generation number area is at the top of the pavail
	// segment
	return ((hrtime_t *)aligned_mem);
}

inline hrtime_t *
rsmt_bio_mgr::get_pavail_partitioninfo(int sgindex)
{
	// skip the segment generation number area and then
	// all the seggrps before sgindex
	return ((hrtime_t *)partition_info + sgindex*RSMT_BIO_MAX_PARTITIONS);
}

//
// chunksize this determines the chunks in which MainBuf allocs memory.
// Map the send_buffer_size() in the sendstream constructor to an
// optimal chunksize based on partition sizes so as to minimize allocation
// of multiple chunks in MainBuf
//
inline uint32_t
rsmt_bio_mgr::get_chunksize(uint32_t nbytes)
{
	int sgidx;

	sgidx = get_seggrp((size_t)nbytes);

	if (sgidx == RSMT_BIO_MAX_SEGGRPS) {
		sgidx--;
	}

	return ((uint32_t)RSMT_BIO_PARTITIONS[sgidx].span_factor *
	    (uint32_t)RSMT_BIO_PARTITIONS[sgidx].partition_sz);
}


//
// Returns the most optimal segment group for a given message size, the message
// size includes the header. static method since get_chunksize uses it.
//
inline int
rsmt_bio_mgr::get_seggrp(size_t msgsz)
{
	int sgidx;

	// The smallest seggrp whose partition(s) can carry the message

	for (sgidx = 0; sgidx < RSMT_BIO_MAX_SEGGRPS; sgidx++) {
		if (RSMT_BIO_PARTITIONS[sgidx].partition_sz >= msgsz) {
			break;
		}
	}

	// Go one notch below if multiple partitions can fit the message
	if ((sgidx > 0) && ((uint32_t)RSMT_BIO_PARTITIONS[sgidx-1].span_factor*
		RSMT_BIO_PARTITIONS[sgidx-1].partition_sz >= msgsz)) {
		sgidx--;
	}

	return (sgidx);
}

#endif	/* _RSMT_BIO_IN_H */
