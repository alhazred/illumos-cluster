/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RSMT_SEGMENT_IN_H
#define	_RSMT_SEGMENT_IN_H

#pragma ident	"@(#)rsmt_segment_in.h	1.11	08/05/20 SMI"

inline rsm_memseg_id_t
rsmt_segment::get_memseg_id()
{
	return (memseg_id);
}

inline rsmt_seg_type
rsmt_segment::get_seg_type()
{
	return (seg_type);
}

inline void
rsmt_segment::set_seg_type(rsmt_seg_type type)
{
	seg_type = type;
}

inline size_t
rsmt_segment::get_seg_size()
{
	return (seg_size);
}

inline size_t
rsmt_export_segment::get_mem_size()
{
	return (ex_memory.ms_length);
}

inline bool
rsmt_export_segment::is_rebindable()
{
	return (rebindable);
}

inline void
rsmt_import_segment::import_close()
{
	(void) RSM_DISCONNECT(seg_ctrl, im_memseg);
}

inline int
rsmt_import_segment::put8(off_t offset, uint8_t *datap, ulong_t rep_cnt)
{
	return (RSM_PUT8(seg_ctrl,
		im_memseg, offset, datap, rep_cnt, (boolean_t)0));
}

inline int
rsmt_import_segment::put16(off_t offset, uint16_t *datap, ulong_t rep_cnt)
{
	return (RSM_PUT16(seg_ctrl,
		im_memseg, offset, datap, rep_cnt, (boolean_t)0));
}

inline int
rsmt_import_segment::put32(off_t offset, uint32_t *datap, ulong_t rep_cnt)
{
	return (RSM_PUT32(seg_ctrl,
		im_memseg, offset, datap, rep_cnt, (boolean_t)0));
}

inline int
rsmt_import_segment::put64(off_t offset, uint64_t *datap, ulong_t rep_cnt)
{
	return (RSM_PUT64(seg_ctrl,
		im_memseg, offset, datap, rep_cnt, (boolean_t)0));
}

inline int
rsmt_import_segment::put(off_t offset, void *datap, size_t length)
{
	return (RSM_PUT(seg_ctrl,
		im_memseg, offset, datap, length));
}

inline int
rsmt_import_segment::get8(off_t offset, uint8_t *datap, ulong_t rep_cnt)
{
	return (RSM_GET8(seg_ctrl,
		im_memseg, offset, datap, rep_cnt, (boolean_t)0));
}

inline int
rsmt_import_segment::get16(off_t offset, uint16_t *datap, ulong_t rep_cnt)
{
	return (RSM_GET16(seg_ctrl,
		im_memseg, offset, datap, rep_cnt, (boolean_t)0));
}

inline int
rsmt_import_segment::get32(off_t offset, uint32_t *datap, ulong_t rep_cnt)
{
	return (RSM_GET32(seg_ctrl,
		im_memseg, offset, datap, rep_cnt, (boolean_t)0));
}

inline int
rsmt_import_segment::get64(off_t offset, uint64_t *datap, ulong_t rep_cnt)
{
	return (RSM_GET64(seg_ctrl,
		im_memseg, offset, datap, rep_cnt, (boolean_t)0));
}

inline int
rsmt_import_segment::get(off_t offset, void *datap, size_t length)
{
	return (RSM_GET(seg_ctrl,
		im_memseg, offset, datap, length));
}

inline int
rsmt_import_segment::open_barrier(rsm_barrier_t *barrier)
{
	return (RSM_OPEN_BARRIER_REGION(seg_ctrl, im_memseg,
		barrier));
}

inline int
rsmt_import_segment::order_barrier(rsm_barrier_t *barrier)
{
	return (RSM_ORDER_BARRIER(seg_ctrl, barrier));
}

inline int
rsmt_import_segment::close_barrier(rsm_barrier_t *barrier)
{
	return (RSM_CLOSE_BARRIER(seg_ctrl, barrier));
}

#endif /* _RSMT_SEGMENT_IN_H */
