//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rsmt_rio_seg.cc	1.22	08/05/20 SMI"

#include <orb/infrastructure/common_threadpool.h>
#include <transports/rsm/rsmt_util.h>
#include <transports/rsm/rsmt_cmf.h>
#include <transports/rsm/rsmt_rio_seg.h>

extern uint_t rsmt_rio_max_tmem;

// RSM transport does not take the replyio path for data transfer if the
// transfer size is not at least this big.
size_t rsmt_rio_seg_mgr::RSMT_MIN_RIO_SIZE	= PAGESIZE;

// RSM transport does not create RIO segments smaller than this.
size_t rsmt_rio_seg_mgr::RSMT_MIN_RIO_SEG_SIZE	= PAGESIZE;

// RIO does not cache segments larger than this.
size_t rsmt_rio_seg_mgr::RSMT_MAX_RIO_CACHEABLE = rsmt_rio_max_tmem*PAGESIZE;

//
// If available_seg_size falls below this RIO segment scanner will try to
// dispose segments.
// The limit is set so that available_seg_size will always allow creation of
// at least 1 segment in the largest cacheable segment pool ie. same as
// rsmt_rio_seg_mgr::RSMT_MAX_RIO_CACHEABLE
//
size_t rsmt_rio_seg_mgr::RSMT_SEG_SIZE_LOW_THRESHOLD =
					rsmt_rio_max_tmem*PAGESIZE;

uint32_t rsmt_rio_seg_mgr::rebind_failures = 0;

uint32_t rsmt_rio_seg_mgr::replyio_failures = 0;

//
// rsmt_rio_seg_mgr::rsmt_rio_seg_mgr
//
// Replyio segment manager constructor. Each endpoint has its own private
// replyio segment manager.
//
rsmt_rio_seg_mgr::rsmt_rio_seg_mgr(rsmt_seg_mgr *smp, size_t maxss,
    size_t tot_segsz, int ladpid, nodeid_t rndid, int radpid,
    rsm_addr_t raddr) :
	rsmt_cmf_client(ladpid, rndid, radpid, raddr, CMFCLIENT_REPLIO),
	smgrp(smp),
	max_pools(0),
	seg_pools(NULL),
	min_seg_size(RSMT_MIN_RIO_SEG_SIZE),
	max_seg_size(maxss),
	total_seg_size(tot_segsz),
	available_seg_size(tot_segsz),
	claim_waiters(0),
	sends_in_progress(0),
	cleaning(false),
	scanner_active(false)
{
	uint_t	i;
	size_t	allocsize;

	ASSERT(min_seg_size <= max_seg_size);

	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_REPLYIO,
	    ("rio_seg_mgr %p created maxss %d ladpid %d rndid %d"
	    " radpid %d raddr %p\n", this, maxss, ladpid, rndid, radpid,
	    raddr));

	// calculate the threshold at which the scanner thread needs
	// to start scanning the segpools for resource management
	scanner_start_threshold = (total_seg_size*
	    (size_t)SCANNER_START_THRESHOLD)/100;
	if (scanner_start_threshold > RSMT_SEG_SIZE_LOW_THRESHOLD)
		scanner_start_threshold = RSMT_SEG_SIZE_LOW_THRESHOLD;

	//
	// Compute the number of segment pools. Segments in each segment pool
	// are twice as big as segments in the preceding segement pool. The
	// last segment pool is an exception in that segments there are of
	// the size max_seg_size which may be smaller than twice the size of
	// the segments in the preceding segment pool.
	//
	while ((min_seg_size << ++max_pools) < max_seg_size) {
	}

	// Create the segment pools
	seg_pools = new rsmt_rio_seg_pool[max_pools];

	// Initialize the segment pools
	for (i = 0, allocsize = min_seg_size; i < max_pools; i++) {
		seg_pools[i].initialize(this, smgrp, allocsize);
		if (i == max_pools - 1) {
			allocsize = max_seg_size;
		} else {
			allocsize = allocsize << 1;
		}
	}

	// create the scanner defer task if resource management is desired
	if (total_seg_size != RSMT_INFINITE_SEG_SIZE) {
		scannerdtp = new rsmt_rio_seg_scanner_dt(this);
	} else {
		scannerdtp = NULL;
	}

	//
	// Acquire a hold on the per endpoint segment manager. The RIO segment
	// manager will retain this hold for the life of the RIO segment
	// manager so that it can use the services of the endpoint segment
	// manager whenever it wishes to.
	//
	smgrp->hold();
}

//
// rsmt_rio_seg_mgr::~rsmt_rio_seg_mgr
//
// Replyio segment manager destructor.
//
rsmt_rio_seg_mgr::~rsmt_rio_seg_mgr()
{
	smgrp->rele();
	smgrp = NULL;

	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_REPLYIO,
	    ("rio_seg_mgr %p destroyed\n", this));

	// Destroy the segment pools
	delete [] seg_pools;
	seg_pools = NULL;
} //lint !e1740

//
// rsmt_rio_seg_mgr::initiate_init
//
// Called by endpoint::initiate once in the beginning. Guaranteed to succeed.
//
void
rsmt_rio_seg_mgr::initiate_init()
{
	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_REPLYIO,
	    ("rio_smgr %p initiate_init\n", this));

	cm_register();
	if (scannerdtp) {
		common_threadpool::the().defer_processing(scannerdtp);
	}
}

//
// rsmt_rio_seg_mgr::initiate_init
//
// Called by endpoint::initiate in a loop until successful.
//
bool
rsmt_rio_seg_mgr::initiate_fini()
{
	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_REPLYIO,
	    ("rio_smgr %p initiate_fini\n", this));

	return (cm_enable());
}

//
// rsmt_rio_seg_mgr::initiate_init
//
// Called by endpoint::initiate when aborting initiate due to state change.
//
void
rsmt_rio_seg_mgr::initiate_cleanup()
{
	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_REPLYIO,
	    ("rio_smgr %p initiate_cleanup\n", this));

	cm_unregister();
	cm_disable();
}

//
// rsmt_rio_seg_mgr::alloc_client_seg
//
// Called by the replyio client to create an exported segment bound to the
// rio memory area specified by the two parameters. A pointer to the exported
// segment is returned. Upon error, NULL is returned.
//
rsmt_rio_client_seg *
rsmt_rio_seg_mgr::alloc_client_seg(void *rioaddr, size_t riosize)
{
	rsmt_rio_client_seg *cseg = NULL;

	RSMT_SEG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_REPLYIO,
	    ("rio_mgr %p alloc_client_seg addr %p size %d\n",
	    this, rioaddr, (int)riosize));

	//
	// Only whole pages can be bound to a segment. Find the page address
	// given the rio address.
	//
	void *page_addr = (void *)((uintptr_t)rioaddr & PAGEMASK);

	// Now compute the minimum required segment size
	size_t newsize = (size_t)((uintptr_t)rioaddr & PAGEOFFSET) + riosize;

	// The minimum segment size must be rounded off to whole pages
	if (newsize & PAGEOFFSET) {
		newsize = (newsize & (size_t)PAGEMASK) + PAGESIZE;
	}

	RSMT_SEG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_REPLYIO,
	    ("rio_mgr %p alloc_client_seg page addr %p new size %d\n",
	    this, page_addr, (int)newsize));

	if (newsize > max_seg_size) {
		// Request for a segment larger than that can be supported.
		return (NULL);
	}

	// Find the best fit segment pool
	uint_t pool_index = get_pool_index(newsize);
	ASSERT(pool_index != (uint_t)-1);

	//
	// Best fit pool, cache search followed by create. Note that
	// this operation is still non blocking.
	//
	cseg = seg_pools[pool_index].alloc_client_seg(page_addr, newsize);

	RSMT_SEG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_REPLYIO,
	    ("rio_mgr %p alloc_client_seg returning cseg %p\n",
	    this, cseg));

	return (cseg);
}

//
// rsmt_rio_seg_mgr::claim_server_seg
//
// This method is called at the replyio server to claim a connected import
// segment from the replyio segment manager. Returns pointer to the import
// segment. Upon error, NULL will be returned.
//
rsmt_rio_server_seg *
rsmt_rio_seg_mgr::claim_server_seg(rsmt_seg_conn_token_t *segtokp)
{
	rsm_memseg_id_t		segment_id;
	rsmt_rio_server_seg	*sseg = NULL;
	rsmt_import_segment	*iseg = NULL;

	segment_id = smgrp->get_memseg_id_by_token(*segtokp);

	RSMT_SEG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_REPLYIO,
	    ("rio_mgr %p claim_server_seg segid %x\n", this, segment_id));

	sm_lock.lock();
	while (!cleaning && sseg == NULL) {
		//
		// Check if this segment is cached with us and is free.
		//
		sseg = sseg_free_hash.remove(segment_id, false);
		if (sseg != NULL) {
			RSMT_SEG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_REPLYIO,
			    ("rio_mgr %p claim_server_seg segid %x sseg %p"
			    " found\n", this, segment_id, sseg));
			break;
		}
		//
		// Segment was not found in the free cache. Maybe it exists but
		// has not yet been released by the previous operation.
		//
		sseg = sseg_inuse_hash.lookup(segment_id);
		if (sseg != NULL) {
			//
			// The segment exists but has not yet been released by
			// the previous operation yet. Need to wait for it to
			// become free.
			//
			RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_REPLYIO,
			    ("rio_mgr %p claim_server_seg segid %x sseg %p"
			    " still in use, waiting...\n", this, segment_id,
			    sseg));

			claim_waiters++;
			sm_cv.wait(&sm_lock);
			claim_waiters--;
			sseg = NULL;
		} else {
			// Brand new segment.
			break;
		}
	}
	sm_lock.unlock();

	if (sseg == NULL) {
		RSMT_SEG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_REPLYIO,
		    ("rio_mgr %p claim_server_seg segid %x not found\n",
		    this, segment_id));
	}

	if (!cleaning && sseg == NULL) {
		//
		// New segment. Ask the segment manager to connect to the
		// export side and return a new import seg handle.
		//
		iseg = smgrp->connect_seg(*segtokp);
		if (iseg != NULL) {
			sseg = new rsmt_rio_server_seg(iseg, this);
		}

		if (sseg != NULL) {
			RSMT_SEG_DBG(RSMT_DBGL_INFO, RSMT_DBGF_REPLYIO,
			    ("rio_mgr %p claim_server_seg new sseg %p"
			    " segid %x created\n", this, sseg, segment_id));
		} else {
			RSMT_SEG_DBG(RSMT_DBGL_INFO, RSMT_DBGF_REPLYIO,
			    ("rio_mgr %p claim_server_seg new sseg for"
			    " segid %x could not be created\n",
			    this, segment_id));
		}
	}

	//
	// Add the segment to the inuse list before returning. Segment will be
	// added to this list only if endpoint cleanup has not started yet.
	//
	if (sseg != NULL) {
		sm_lock.lock();
		if (cleaning) {
			RSMT_SEG_DBG(RSMT_DBGL_INFO, RSMT_DBGF_REPLYIO,
			    ("rio_mgr %p claim_server_seg sseg %p segid %x"
			    " disposed because cleaning\n", this, sseg,
			    segment_id));
			dispose_server_seg(sseg);
			sseg = NULL;
		} else {
			sseg_inuse_hash.add(sseg, segment_id, sseg->get_hent());
		}
		sm_lock.unlock();
	}

	return (sseg);
}

//
// rsmt_rio_seg_mgr::release_client_seg
//
// This method returns a replyio segment back to the replyio segment manager
// after it has been used. The replyio segment manager caches all used import
// segments with itself. The export segments are cached by respective segment
// pools.
//
void
rsmt_rio_seg_mgr::release_client_seg(rsmt_rio_client_seg *cseg)
{
	rsmt_export_segment *eseg = cseg->get_eseg();
	size_t	segsize = eseg->get_seg_size();
	uint_t	pool_index = get_pool_index(segsize);

	RSMT_SEG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_REPLYIO,
	    ("rio_mgr %p release_client_seg cseg %p segid %x\n",
	    this, cseg, eseg->get_memseg_id()));

	ASSERT(pool_index != (uint_t)-1);
	rsmt_rio_seg_pool& segpool = seg_pools[pool_index];
	segpool.release_client_seg(cseg);
}

//
// rsmt_rio_seg_mgr::release_server_seg
//
// The replyio code makes this call to release a import segment once the
// replyio operation is over. The segment can either be cached or freed.
//
void
rsmt_rio_seg_mgr::release_server_seg(rsmt_rio_server_seg *sseg)
{
	rsmt_import_segment	*iseg = sseg->get_iseg();
	rsm_memseg_id_t		segment_id = iseg->get_memseg_id();
	rsmt_rio_server_seg	*inuse_sseg = NULL;

	RSMT_SEG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_REPLYIO,
	    ("rio_mgr %p release_server_seg sseg %p segid %x ret %d\n",
	    this, sseg, segment_id, (int)sseg->is_retired()));

	sm_lock.lock();
	//
	// Remove the segment from the inuse list.
	//
	inuse_sseg = sseg_inuse_hash.remove(segment_id, false);
	ASSERT(inuse_sseg == sseg);

	//
	// Cache segments only if the replyio segment manager has not been
	// asked to start the cleanup operations in preparation for
	// destruction.
	//
	if (!cleaning && !sseg->is_retired()) {
		RSMT_SEG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_REPLYIO,
		    ("rio_mgr %p release_server_seg sseg %p added to cache\n",
		    this, sseg));

		sseg_free_hash.add(sseg, segment_id, sseg->get_hent());
		if (claim_waiters > 0) {
			//
			// Someone might be waiting for this very segment.
			//
			sm_cv.broadcast();
		}
	} else {
		RSMT_SEG_DBG(RSMT_DBGL_VERBOSE, RSMT_DBGF_REPLYIO,
		    ("rio_mgr %p release_server_seg sseg %p disposed\n",
		    this, sseg));

		dispose_server_seg(sseg);
	}
	sm_lock.unlock();
}

//
// rsmt_rio_seg_mgr::unblock
//
// Called from endpoint::unblock. This will allow the RIO segment manager
// from the next incarnation of the enpoint to be able to register with the
// control message framework while the current incarnation is working on
// going away. Makes the process of establishing the new incarnation quicker
// after a path down.
//
void
rsmt_rio_seg_mgr::unblock()
{
	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_REPLYIO,
	    ("rio_smgr %p unblock\n", this));

	cm_unregister();
}

//
// rsmt_rio_seg_mgr::cleanup_init
//
// This method is called from endpoint::cleanup to mark that endpoint
// cleanup has started.
//
void
rsmt_rio_seg_mgr::cleanup_init()
{
	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_REPLYIO,
	    ("rio_smgr %p cleanup_init\n", this));

	sm_lock.lock();
	cleaning = true;

	//
	// Wake up all those who have been waiting to claim a segment. That
	// segment is never going to become available.
	// Wake up scanner thread if it has waiting on the threshold.
	//
	if ((claim_waiters > 0) || (scanner_active)) {
		sm_cv.broadcast();
	}
	sm_lock.unlock();

	cm_unregister();
}

//
// rsmt_rio_seg_mgr::cleanup_fini_pre_sm
//
// This method is called from endpoint::cleanup to finish the cleanup
// operation that was started with the call to cleanup_init. This method
// frees all the cached replyio segments.
//
void
rsmt_rio_seg_mgr::cleanup_fini_pre_sm()
{
	uint_t	i;
	rsmt_rio_server_seg	*sseg;
	rsmt_import_segment	*iseg;

	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_REPLYIO,
	    ("rio_smgr %p cleanup_fini_pre_sm\n", this));

	sm_lock.lock();
	//
	// Wait for any pending control message sends to get over before
	// proceeding.
	//
	while (sends_in_progress > 0) {
		sm_cv.wait(&sm_lock);
	}
	sm_lock.unlock();

	//
	// From this point on, control message sends are disabled. It's safe
	// to call cm_disable.
	//
	cm_disable();

	// wait for the scanner thread to be shutdown
	sm_lock.lock();
	while (scanner_active) {
		sm_cv.wait(&sm_lock);
	}
	sm_lock.unlock();

	//
	// After the cleaning flag was set, no other thread could be accessing
	// the free segment hash table.
	//

	hashtable_t<rsmt_rio_server_seg *, rsm_memseg_id_t>::iterator
	    iter(sseg_free_hash);

	// Start disconnect on all the import segments cached with us.
	while ((sseg = iter.get_current()) != NULL) {
		iter.advance();
		iseg = sseg->get_iseg();
		(void) sseg_free_hash.remove(iseg->get_memseg_id(), false);
		dispose_server_seg(sseg);
	}

	// Ask all segment pools to cleanup the export segments that they
	// have cached.
	for (i = 0; i < max_pools; i++) {
		seg_pools[i].cleanup();
	}
}

//
// rsmt_rio_seg_mgr::cleanup_fini_post_sm
//
// This method is called from rsmt_endpoint::cleanup after the segment
// manager is done destroying all segments. This is a noop for now.
//
void
rsmt_rio_seg_mgr::cleanup_fini_post_sm()
{
	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_REPLYIO,
	    ("rio_smgr %p cleanup_fini_post_sm\n", this));
}

//
// rsmt_rio_seg_mgr::get_pool_index
//
// Given a segment size, returns the best fit segment pool index. If the
// given segment size is larger than the biggest segment possible, a -1 is
// returned.
//
uint_t
rsmt_rio_seg_mgr::get_pool_index(size_t segsize)
{
	uint_t	pool_index = 0;
	size_t	pool_seg_size = min_seg_size;

	while (segsize > pool_seg_size && pool_index < max_pools) {
		pool_index++;
		if (pool_index == max_pools - 1) {
			pool_seg_size = max_seg_size;
		} else {
			pool_seg_size <<= 1;
		}
	}
	if (pool_index == max_pools) {
		pool_index = (uint_t)-1;
	}
	return (pool_index);
}

//
// rsmt_rio_seg_mgr::seek_peer_release
//
// Sends a message to the peer to inform it that the given export segment is
// being retired and the peer should release the corresponding import segment.
//
void
rsmt_rio_seg_mgr::seek_peer_release(rsmt_rio_client_seg *cseg)
{
	rsmt_rio_ctrl_msg_t	ctrlmsg;
	bool	ok_to_send = false;

	bzero(&ctrlmsg, sizeof (rsmt_rio_ctrl_msg_t));
	ctrlmsg.conntok		= smgrp->get_conn_token(cseg->get_eseg());

	RSMT_SEG_DBG(RSMT_DBGL_VERBOSE, RSMT_DBGF_REPLYIO,
	    ("rio_mgr %p seek_peer_release cseg %p segid %x\n",
	    this, cseg, smgrp->get_memseg_id_by_token(ctrlmsg.conntok)));

	//
	// If the endpoint has started cleanup, no more messages should
	// be sent as either the control message mechanism either has already
	// been disabled or is waiting for sends already in progress to
	// complete before it can be disabled.
	//
	sm_lock.lock();
	if (!cleaning) {
		sends_in_progress++;
		ok_to_send = true;
	}
	sm_lock.unlock();

	if (ok_to_send) {
		if (!cm_send(&ctrlmsg, sizeof (rsmt_rio_ctrl_msg_t),
		    CMF_MSGID_BASE + 1, false)) {
			RSMT_SEG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_REPLYIO,
			    ("rio_mgr %p seek_peer_release cm_send failed\n",
			    this));
		}
		sm_lock.lock();
		sends_in_progress--;
		//
		// If the endpoint is in cleaning state, it is waiting for the
		// sends in progress count to drop to zero so that it can
		// disable the control message mechanism.
		//
		if (cleaning && sends_in_progress == 0) {
			sm_cv.broadcast();
		}
		sm_lock.unlock();
	} else {
		RSMT_SEG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_REPLYIO,
		    ("rio_mgr %p seek_peer_release cm_send aborted\n",
		    this));
	}
}

//
// Check is a segment of the specified size can be allocated, if yes deduct
// the size from the available seg size variable.
//
bool
rsmt_rio_seg_mgr::check_available_seg_size(size_t seg_size)
{
	if (available_seg_size == RSMT_INFINITE_SEG_SIZE)
		return (true);

	sm_lock.lock();
	if (available_seg_size >= seg_size) {
		available_seg_size -= seg_size;
		//
		// Wake up scanner thread so that it can check the threshold
		// and start scanning segment pools for resource management.
		//
		sm_cv.broadcast();
		sm_lock.unlock();
		return (true);
	}
	sm_lock.unlock();

	return (false);
}

//
// Segment of specified size is being disposed, update available_seg_size.
//
void
rsmt_rio_seg_mgr::freed_seg_size(size_t seg_size)
{
	if (total_seg_size == RSMT_INFINITE_SEG_SIZE) {
		return; // no resource management is being done

	}
	sm_lock.lock();
	available_seg_size += seg_size;
	ASSERT(available_seg_size <= total_seg_size);
	sm_lock.unlock();
}

//
// rsmt_rio_seg_mgr::seg_scanner
// This method is called by the rsmt_rio_seg_scanner_dt and does the resource
// management for the replyio segments. The resource management is done using a
// not recently used algorithm. The algorithm is implemented in a continous
// loop in two phases with sleeps interjected between the two phases.
//
//   phase I - Scan the client segments in the free list of segpools that
//	can cache segments and clear the recently_used flag of the segment.
//
//   phase II - If the available_seg_size falls below a certain threshold
//	free those segments in the segpool's free list that do not have their
//	recently_used flag set.
//
// The scanner thread becomes active only when the scanner_start_threshold
// is reached.
//
void
rsmt_rio_seg_mgr::seg_scanner()
{
	enum scanner_phases { PHASE_I = 1, PHASE_II };
	scanner_phases next_phase;

	RSMT_PATH_DBG(RSMT_DBGL_INFO, RSMT_DBGF_REPLYIO,
	    ("rsmt_rio_seg_mgr(%p) scanner thread starting\n", this));

	sm_lock.lock();
	scanner_active = true;
	next_phase = PHASE_I;

	while (!cleaning) {
		//
		// Wait if the scanner_start_threshold has not been reached.
		// This wait is done only before phase 1 because we always
		// want to do phase 1 and phase 2 in pairs.
		//
		while ((next_phase == PHASE_I) && !cleaning &&
		    (available_seg_size > scanner_start_threshold)) {
			sm_cv.wait(&sm_lock);
		}

		if (cleaning) { // we were woken up due to cleanup
			break;
		}
		sm_lock.unlock();

		switch (next_phase) {
		case PHASE_I:
			// phase I - clear the recently_used flag
			mark_phase();
			next_phase = PHASE_II;
			break;
		case PHASE_II:
			// phase II - cleanup segments
			cleanup_phase();
			next_phase = PHASE_I;
			break;
		}

		sm_lock.lock();
		if (cleaning) { // check if cleanup is in progress
			break;
		}
		sm_lock.unlock();

		// Sleep for 500us between phase I and II
		os::usecsleep((os::usec_t)500);
		sm_lock.lock();
	}
	scanner_active = false;
	sm_cv.signal();
	sm_lock.unlock();
	RSMT_PATH_DBG(RSMT_DBGL_INFO, RSMT_DBGF_REPLYIO,
	    ("rsmt_rio_seg_mgr(%p) scanner thread stopping\n", this));
}

//
// rsmt_rio_seg_mgr::cm_handle
//
// The control message handler. Handles the message sent by seek_peer_release.
//
bool
#ifdef DEBUG
rsmt_rio_seg_mgr::cm_handle(void *msgp, ushort_t msglen, int msgid, bool fc)
#else
rsmt_rio_seg_mgr::cm_handle(void *msgp, ushort_t, int, bool)
#endif
{
	rsm_memseg_id_t		segment_id;
	rsmt_rio_server_seg	*sseg;
	rsmt_rio_ctrl_msg_t	*ctrlmsgp;

	ASSERT(msglen == sizeof (rsmt_rio_ctrl_msg_t));
	ASSERT(msgid == CMF_MSGID_BASE + 1);
	ASSERT(!fc);

	ctrlmsgp = (rsmt_rio_ctrl_msg_t *)msgp;
	segment_id = smgrp->get_memseg_id_by_token(ctrlmsgp->conntok);

	RSMT_SEG_DBG(RSMT_DBGL_INFO, RSMT_DBGF_REPLYIO,
	    ("rio_smgr %p cm_handle release segid %x message arrived\n",
	    this, segment_id));

	sm_lock.lock();
	if (cleaning) {
		//
		// Endpoint cleaning has already started. Ignore this message.
		//
		sm_lock.unlock();

		RSMT_SEG_DBG(RSMT_DBGL_INFO, RSMT_DBGF_REPLYIO,
		    ("rio_smgr %p cm_handle release segid %x ignored\n",
		    this, segment_id));

		return (true);
	}
	//
	// First check if this segment is cached in the free segment hash.
	//
	sseg = sseg_free_hash.remove(segment_id, false);
	if (sseg == NULL) {
		//
		// Segment was not found in the free cache. Maybe it has not
		// been released by the previous operation yet.
		//
		RSMT_SEG_DBG(RSMT_DBGL_INFO, RSMT_DBGF_REPLYIO,
		    ("rio_smgr %p cm_handle segid %x not in free cache\n",
		    this, segment_id));

		sseg = sseg_inuse_hash.lookup(segment_id);
		if (sseg == NULL) {
			//
			// Segment was not found on the inuse hash either.
			// Either this segment was never successfully
			// created or has already been released due to path
			// down.
			//
			RSMT_SEG_DBG(RSMT_DBGL_INFO, RSMT_DBGF_REPLYIO,
			    ("rio_smgr %p cm_handle segid %x not in inuse"
			    " cache\n", this, segment_id));
		} else {
			RSMT_SEG_DBG(RSMT_DBGL_INFO, RSMT_DBGF_REPLYIO,
			    ("rio_smgr %p cm_handle segid %x found in inuse"
			    " cache sseg %p, retiring\n", this, segment_id,
			    sseg));

			ASSERT(!sseg->is_retired());
			sseg->retire();
		}
	} else {
		RSMT_SEG_DBG(RSMT_DBGL_INFO, RSMT_DBGF_REPLYIO,
		    ("rio_smgr %p cm_handle segid %x found in free"
		    " cache sseg %p, disposing\n", this, segment_id,
		    sseg));

		dispose_server_seg(sseg);
	}
	sm_lock.unlock();

	return (true);
}

//
// Implements phase I of the scanner algorithm
//
void
rsmt_rio_seg_mgr::mark_phase()
{
	// start from the biggest segpool first
	for (int i = (int)max_pools - 1; i >= 0; i--) {
		seg_pools[i].mark_phase();
	}
}

void
rsmt_rio_seg_mgr::cleanup_phase()
{
	// start from the biggest segpool first
	for (int i = (int)max_pools - 1; i >= 0; i--) {
		sm_lock.lock();
		if (available_seg_size <= RSMT_SEG_SIZE_LOW_THRESHOLD) {
			sm_lock.unlock();
			seg_pools[i].cleanup_phase();
		} else {
			sm_lock.unlock();
		}
	}
}

//
// rsmt_rio_seg_pool constructor
//
// A constructor that does not take any parameters must exist as the segment
// pools are allocated in arrays.
//
rsmt_rio_seg_pool::rsmt_rio_seg_pool() :
	allocsize(0),
	smgrp(NULL),
	rio_smgrp(NULL),
	cleaning(false),
	cacheable(false),
	resource_shortage(0)
{
	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_REPLYIO,
	    ("rio_spool %p created\n", this));
}

//
// rsmt_rio_seg_pool destructor
//
rsmt_rio_seg_pool::~rsmt_rio_seg_pool()
{
	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_REPLYIO,
	    ("rio_spool %p destroyed\n", this));

	ASSERT(free_list.empty());
	smgrp		= NULL;
	rio_smgrp	= NULL;
}

//
// rsmt_rio_seg_pool::initialize
//
// This method gets called to provide each segment pool with their individual
// personality. The first parameter is a pointer to the per endpoint segment
// manager. The second parameter is the size of the segments managed by this
// pool.
//
void
rsmt_rio_seg_pool::initialize(rsmt_rio_seg_mgr *riosmp, rsmt_seg_mgr *smp,
    size_t asize)
{
	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_REPLYIO,
	    ("rio_spool %p initialize rio_smgr %p alloc size %d\n",
	    this, riosmp, (int)asize));

	rio_smgrp	= riosmp;
	smgrp		= smp;
	allocsize	= asize;
	cacheable	= (allocsize == 0) ? false :
	    (allocsize <= (size_t)rsmt_rio_max_tmem*PAGESIZE);
}

//
// rsmt_rio_seg_pool::cleanup
//
// This method is called to free all the cached export segments when the
// endpoint is going down.
//
void
rsmt_rio_seg_pool::cleanup()
{
	rsmt_rio_client_seg	*cseg;

	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_REPLYIO,
	    ("rio_spool %p cleanup\n", this));

	pool_lock.lock();
	ASSERT(!cleaning);
	cleaning = true;
	pool_lock.unlock();

	//
	// From this point on, the cleanup thread owns the free list.
	//
	while ((cseg = free_list.reapfirst()) != NULL) {
		RSMT_SEG_DBG(RSMT_DBGL_INFO, RSMT_DBGF_REPLYIO,
		    ("rio_spool %p cleanup disposing cseg %p segid %x\n",
		    this, cseg, cseg->get_eseg()->get_memseg_id()));

		dispose_client_seg(cseg);

		if (cacheable) {
			// This is a cacheable segment and since it is in the
			// process of being disposed update the
			// available_seg_size counter. It's ok to update it
			// before the segment is actually freed since the
			// resource limit is not a hard limit.
			rio_smgrp->freed_seg_size(allocsize);
		}
	}
}

//
// rsmt_rio_seg_pool::alloc_client_seg
//
// This method is called to allocate a (export) segment from the segment pool
// at the replyio client. The two arguments specify the memory area the segment
// should be bound to. The return value is a pointer to an appropriate export
// (client) segment. Attempt is made to reuse to cached segment if one is
// found.
//
rsmt_rio_client_seg *
rsmt_rio_seg_pool::alloc_client_seg(void *memaddr, size_t memsize)
{
	rsmt_rio_client_seg	*cseg = NULL;
	rsmt_export_segment	*eseg = NULL;
	int	err;

	pool_lock.lock();

	RSMT_SEG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_REPLYIO,
	    ("rio_spool %p alloc_client_seg memaddr %p memsize %d"
	    " allocsize %d\n", this, memaddr, (int)memsize, (int)allocsize));

	if (cleaning) {
		pool_lock.unlock();

		RSMT_SEG_DBG(RSMT_DBGL_VERBOSE, RSMT_DBGF_REPLYIO,
		    ("rio_spool %p alloc_client_seg memaddr %p memsize %d"
		    " aborted.\n", this, memaddr, (int)memsize));

		return (NULL);
	}
	ASSERT(memsize <= allocsize);

	// Check if a preconnected segment is available
	cseg = free_list.reapfirst();
	pool_lock.unlock();

	if (cseg != NULL) {
		//
		// A preconnected segment was found in the free list.
		// Need to bind it to the memory area passed.
		//
		RSMT_SEG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_REPLYIO,
		    ("rio_spool %p alloc_client_seg cseg %p found free\n",
		    this, cseg));

		if ((err = cseg->rebind(memaddr, memsize)) == 0) {
			// Successful reuse of cached segment
			return (cseg);
		}
		// increment the rebind_failures counter
		os::atomic_add_32(&rsmt_rio_seg_mgr::rebind_failures, 1);

		//
		// Rebind failed. The chances of rebind succeeding again at
		// a later time are remote. Just destroy the segment.
		//
		RSMT_SEG_DBG(RSMT_DBGL_VERBOSE, RSMT_DBGF_REPLYIO,
		    ("rio_spool %p alloc_client_seg cseg %p rebind failed"
		    " eseg %p err %d, disposing\n",
		    this, cseg, cseg->get_eseg(), err));

		dispose_client_seg(cseg);

		if (cacheable) {
			// This is a cacheable segment and since it is in the
			// process of being disposed update the
			// available_seg_size counter. It's ok to update it
			// before the segment is actually freed since the
			// resource limit is not a hard limit.
			rio_smgrp->freed_seg_size(allocsize);
		}
		cseg = NULL;
	}
	ASSERT(cseg == NULL);

	// No cached segment found. Allocate a new segment.

	// If this is a cacheable segment, need to verify if resources
	// are available and reserve it.
	if (cacheable && !rio_smgrp->check_available_seg_size(allocsize)) {
		RSMT_SEG_DBG(RSMT_DBGL_INFO, RSMT_DBGF_REPLYIO,
		    ("rio_spool %p alloc_client_seg resource unavailable"
			" asize %d maddr %p msize %d\n",
			this, (int)allocsize, memaddr, (int)memsize));
		// increment the resource_shortage counter
		os::atomic_add_32(&resource_shortage, 1);
		return (NULL);
	}

	RSMT_SEG_DBG(RSMT_DBGL_INFO, RSMT_DBGF_REPLYIO,
	    ("rio_spool %p alloc_client_seg calling create seg with"
	    " asize %d maddr %p msize %d\n",
	    this, (int)allocsize, memaddr, (int)memsize));

	//
	// Do not use the RSMT_SMEX_CONNECT optimization while creating
	// segments since it results in a race condition between defer_connect
	// and disconnect_seg. The pre-connect optimization results in
	// the segment manager creating a defer_connect task which
	// sometimes gets scheduled much after replyio has claimed
	// the segment, done its data transfer and is in the process
	// of disconnecting the segment due to errors on replyio
	// client eg. rebind failure. If the defer_connect happens
	// just after the disconnect and before the replyio client's
	// attempts to unpublish the segment, the unpublish will
	// always fail since we now have a new connection to the
	// segment. This will result in segment leaks.
	//

	//
	// If the requested size is the not the same as the segment size
	// that is standard for this segment group, create the segment
	// bound to the trashmem and then rebind it to the desired memory.
	// This is so that the segment is initially created with the entire
	// segment backed up by real memory. This helps in rebind
	// success rate under the RSM interconnect.
	//
	rsmt_rio_trashmem	*tmem = NULL;

	if (memsize != allocsize) {
		tmem = rsmt_rio_trashmem_server::the().get_trashmem(allocsize);
		if (tmem == NULL) {
			RSMT_SEG_DBG_D(RSMT_DBGL_INFO, RSMT_DBGF_REPLYIO,
			    ("rio_spool %p trashmem allocation failed\n"));
		}
	}

	if (tmem != NULL) {
		eseg = smgrp->create_seg(allocsize, RSMT_SMEX_UNBIND_REBIND,
		    tmem->get_vaddr(), allocsize, NULL);
		RSMT_SEG_DBG_D(RSMT_DBGL_INFO, RSMT_DBGF_REPLYIO,
		    ("rio_spool %p create seg returned eseg %p\n", this, eseg));
		if (eseg != NULL) {
			err = eseg->rebind(memaddr, memsize, NULL);
			if (err != RSM_SUCCESS) {
				RSMT_SEG_DBG(RSMT_DBGL_INFO, RSMT_DBGF_REPLYIO,
				    ("rio_spool %p rebind of %p failed\n", this,
					eseg));
				// destroy the segment since rebind failed.
				// No connections were ever made to this
				// segment hence RSMT_SMEX_NO_CONNECTIONS flag
				// is used
				smgrp->destroy_seg(eseg,
				    RSMT_SMEX_NO_CONNECTIONS, NULL, NULL);
				eseg = NULL;
			}
		}
		rsmt_rio_trashmem_server::the().rele_trashmem(tmem);
	} else {
		//
		// It is possible that memsize < allocsize and in those cases
		// the segment will not be cached. Although from the point
		// of resource management such segments are not treated any
		// differently, ie. available_seg_size gets decremented.
		//
		eseg = smgrp->create_seg(allocsize, RSMT_SMEX_UNBIND_REBIND,
		    memaddr, (size_t)memsize, NULL);
		RSMT_SEG_DBG_D(RSMT_DBGL_INFO, RSMT_DBGF_REPLYIO,
		    ("rio_spool %p create seg returned eseg %p\n", this, eseg));
	}

	if (eseg != NULL) {
		cseg = new rsmt_rio_client_seg(eseg, this);
	}

	if ((cseg == NULL) && cacheable) {
		// This is a cacheable segment and since it is creation failed
		// update the available_seg_size counter.
		rio_smgrp->freed_seg_size(allocsize);
	}

	RSMT_SEG_DBG(RSMT_DBGL_INFO, RSMT_DBGF_REPLYIO,
	    ("rio_spool %p new cseg = %p\n", this, cseg));

	return (cseg);
}

//
// rsmt_rio_seg_pool::release_client_seg
//
// This method returns an export (client) segment back to the pool it
// was allocated from. The segment can either be cached or destroyed.
//
void
rsmt_rio_seg_pool::release_client_seg(rsmt_rio_client_seg *cseg)
{
	int	err;
	bool	cached = false;
	rsmt_export_segment	*eseg = cseg->get_eseg();

	RSMT_SEG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_REPLYIO,
	    ("rio_spool %p release_client_seg cseg %p eseg %p segid %x\n",
	    this, cseg, eseg, eseg->get_memseg_id()));

	ASSERT(eseg->get_seg_size() == allocsize);

	//
	// mark the segment as recently used before putting it back on
	// the free_list
	//
	cseg->set_recently_used(true);

	//
	// Add it to the free pool so that subsequent replyio operations can
	// reuse it. But before the segment is cached, rebind it to a trash
	// memory area so that we do not have "dangling pointers" from this
	// segment. In the following call to rebind the first NULL argument
	// indicates that the request is for binding to trash memory.
	//
	if ((err = cseg->rebind(NULL, allocsize)) == 0) {
		//
		// Rebind successful, need to add to the freelist.
		//
		pool_lock.lock();
		if (!cleaning) {
			//
			// Prepend to the free_list so that recently used
			// segments get selected over and over again.
			// This ensures that the not recently used algorithm
			// to free segments can find segments to disposed
			// based on the usage pattern of a particular segment
			// pool since the recently_used flag for the segments
			// at the tail of the list will not be set if a segpool
			// has more segments than it is using.
			//
			free_list.prepend(cseg);
			cached = true;
			RSMT_SEG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_REPLYIO,
			    ("rio_spool %p release_client_seg cseg %p added"
			    " to free list\n", this, cseg));
		}
		pool_lock.unlock();
	} else {
		// rebind failures to trashmem can be due to the fact that this
		// segpool is not cacheable. Increment the rebind_failures
		// counter only for cacheable segments which support rebind
		if ((cacheable) && (err != RSMERR_UNSUPPORTED_OPERATION)) {
			os::atomic_add_32(&rsmt_rio_seg_mgr::rebind_failures,
			    1);
		}
		RSMT_SEG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_REPLYIO,
		    ("rio_spool %p release_client_seg cseg %p rebind to"
			" trashmem failed err %d\n", this, cseg, err));
	}

	if (!cached) {
		//
		// The segment was not cached either because of a rebind
		// failure or because we are not caching segments any more.
		//
		RSMT_SEG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_REPLYIO,
		    ("rio_spool %p release_client_seg cseg %p disposing\n",
		    this, cseg));
		dispose_client_seg(cseg);

		if (cacheable) {
			// This is a cacheable segment and since it is in the
			// process of being disposed update the
			// available_seg_size counter. It's ok to update it
			// before the segment is actually freed since the
			// resource limit is not a hard limit.
			rio_smgrp->freed_seg_size(allocsize);
		}
	}
}

void
rsmt_rio_seg_pool::mark_phase()
{
	rsmt_rio_client_seg	*cseg;

	if (!cacheable) {
		// resource management is done only for cacheable segments
		return;
	}

	pool_lock.lock();
	free_list.atfirst();
	while ((cseg = free_list.get_current()) != NULL) {
		// clear the recently_used flag
		cseg->set_recently_used(false);
		free_list.advance();
	}
	pool_lock.unlock();
}

void
rsmt_rio_seg_pool::cleanup_phase()
{
	rsmt_rio_client_seg	*cseg;

	if (!cacheable) {
		// resource management is done only for cacheable segments
		return;
	}

	pool_lock.lock();
	free_list.atfirst();
	while ((cseg = free_list.get_current()) != NULL) {
		free_list.advance();
		if (!cseg->get_recently_used()) {
			// segment has not been recently used, remote it from
			// the free_list and schedule the destruction of the
			// segment.
			(void) free_list.erase(cseg);
			dispose_client_seg(cseg);
			RSMT_SEG_DBG(RSMT_DBGL_VERBOSE, RSMT_DBGF_REPLYIO,
			    ("rio_seg_pool %p cleanup_phase dispose seg(%p)\n",
				cseg));
			rio_smgrp->freed_seg_size(allocsize);
		}
	}
	pool_lock.unlock();
}
//
// rsmt_rio_cseg_dt::execute
//
// The defer task to handle the task of destroying an export segment at the
// replyio client.
//
void
rsmt_rio_cseg_dt::execute()
{
	rsmt_rio_seg_pool *spool = cseg->get_seg_pool();
	rsmt_rio_seg_mgr *rio_smgrp = spool->get_rio_seg_mgr();
	rsmt_seg_mgr	*smgrp = spool->get_seg_mgr();

	RSMT_SEG_DBG(RSMT_DBGL_VERBOSE, RSMT_DBGF_REPLYIO,
	    ("rio_cseg_dt %p execute spool %p sending release message"
	    " to peer, cseg %p segid %x\n",
	    this, spool, cseg, cseg->get_eseg()->get_memseg_id()));

	// Ask peer to release this segment
	rio_smgrp->seek_peer_release(cseg);

	RSMT_SEG_DBG(RSMT_DBGL_VERBOSE, RSMT_DBGF_REPLYIO,
	    ("rio_cseg_dt %p execute spool %p destroying cseg %p\n",
	    this, spool, cseg));

	// Destroy the export segment
	smgrp->destroy_seg(cseg->get_eseg(), 0, NULL, NULL);

	delete cseg;
	delete this;
}

//
// rsmt_rio_sseg_dt::execute
//
// The defer task to handle the task of destroying an import segment at the
// replyio server.
//
void
rsmt_rio_sseg_dt::execute()
{
	rsmt_rio_seg_mgr *rio_smgrp = sseg->get_rio_seg_mgr();
	rsmt_seg_mgr *smgrp = rio_smgrp->get_seg_mgr();

	RSMT_SEG_DBG(RSMT_DBGL_VERBOSE, RSMT_DBGF_REPLYIO,
	    ("rio_sseg_dt %p execute smgr %p disconnect sseg %p segid %x\n",
	    this, rio_smgrp, sseg, sseg->get_iseg()->get_memseg_id()));

	smgrp->disconnect_seg(sseg->get_iseg());

	delete sseg;
	delete this;
}

//
// rsmt_rio_client_seg::rebind
//
// Rebind the segment to the passed memory area. If the passed memory
// pointer is NULL, attempt is made to rebind the segment to a trash
// memory area. Any error encountered during rebind is returned.
//
int
rsmt_rio_client_seg::rebind(void *memptr, size_t sz)
{
	int	err = 0;

	// export segment cannot be rebound
	if (!eseg->is_rebindable())
		return (RSMERR_UNSUPPORTED_OPERATION);

	if (memptr != NULL) {
		err = eseg->rebind(memptr, sz, NULL);
		if (trashmem != NULL) {
			rsmt_rio_trashmem_server::the().rele_trashmem(trashmem);
			trashmem = NULL;
		}
		return (err);
	}

	//
	// This request is for rebinding the segment to the trash memory so
	// that the segment could be cached.
	//
	ASSERT(trashmem == NULL);
	trashmem = rsmt_rio_trashmem_server::the().get_trashmem(sz);
	if (trashmem == NULL) {
		return (ENOMEM);
	}

	err = eseg->rebind(trashmem->get_vaddr(), sz, NULL);
	if (err != 0) {
		rsmt_rio_trashmem_server::the().rele_trashmem(trashmem);
		trashmem = NULL;
	}
	return (err);
}
