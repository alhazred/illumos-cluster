/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_RSMT_BIO_BUF_IN_H
#define	_RSMT_BIO_BUF_IN_H

#pragma ident	"@(#)rsmt_bio_buf_in.h	1.5	08/05/20 SMI"

// Constructor
inline
rsmt_bio_buf::rsmt_bio_buf(rsmt_bio_rseg *rp, uchar_t *dp, uint_t len,
    Buf::alloc_type data_type) :
	nil_Buf(dp, len, len, Buf::HEAP, data_type),
	rseg(rp),
	datap(dp),
	size(len)
{
	// When data is on the heap rseg should not be passed
	if (data_type == Buf::HEAP) {
		ASSERT(rseg == NULL);
	}
}

// Destructor
inline
rsmt_bio_buf::~rsmt_bio_buf()
{
	// buffer is a loaned partition, release it now
	if (data_alloc == Buf::OTHER) {
		ASSERT(size > RSMT_BIO_MIN_LOANSZ && rseg);
		rseg->release_partitions(datap, size);
	}

	rseg = NULL;
	datap = NULL;
	size = 0;
}

#endif	/* _RSMT_BIO_BUF_IN_H */
