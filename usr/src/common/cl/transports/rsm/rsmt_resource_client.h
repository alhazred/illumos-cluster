/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RSMT_RESOURCE_CLIENT_H
#define	_RSMT_RESOURCE_CLIENT_H

#pragma ident	"@(#)rsmt_resource_client.h	1.5	08/05/20 SMI"

#include <orb/monitor/monitor.h>

//
// RSMT Resource Clients provides registration methods for segment
// and memory resource deallocation for bufferio and replyio.
// It also provides a method for bufferio and replyio to request
// segments to support their dynamic growth strategy.
//
class rsmt_resource_client {
public:
	// Constructor
	// Creates the rsmt_resource_client object
	// the argument is the local adapter id
	rsmt_resource_client(int);

	// Destructor
	virtual ~rsmt_resource_client();

	// default callback methods
	// virtual function type
	// each rsmt resource client may override one or both methods
	//
	// resource_segfree will be called back with
	// type size_t : max. segment size to be free
	virtual void resource_segfree(size_t);
	// resource_memfree will be called back with
	// type monitor::system_state_t : one of memory monitor states
	virtual void resource_memfree(monitor::system_state_t);

protected:
	// register and unregister segment resource deallocation
	void resource_segment_register();
	void resource_segment_unregister();

	// register and unregister memory resource deallocation
	void resource_memory_register();
	void resource_memory_unregister();

	// requesting segment resources from the global resource management
	// the argument is a hint of the size of segment that the resource
	// client will soon create
	// type size_t : maximium segment size
	void segment_request(size_t);

private:
	// local adapter id that the endpoint resource client belongs to
	int 	resource_adapter_id;

	// Disallow assignments and pass by value
	rsmt_resource_client(const rsmt_resource_client&);
	rsmt_resource_client &operator = (const rsmt_resource_client&);

	// lint info
	rsmt_resource_client();
};
#endif /* _RSMT_RESOURCE_CLIENT_H */
