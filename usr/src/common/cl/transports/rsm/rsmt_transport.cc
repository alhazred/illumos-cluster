//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rsmt_transport.cc	1.28	08/05/20 SMI"

#include <orb/transport/transport.h>

#include "rsmt_util.h"
#include "rsmt_transport.h"
#include "rsmt_pathend.h"
#include "rsmt_adapter.h"
#include "rsmt_cmf.h"
#include "rsmt_resource.h"
#include "rsmt_rio_trashmem.h"

// Constructor for rsmt_transport.  Called indirectly from creation
// of object as a result of unode_init() or kernel modload.  We
// provide our name although the transport apparently doesn't save
// this info as it has to call transport_id() later to find out
// our name.  We tell the transport base class that we don't need a
// deferred task for pm_send_internal since it is a no-op for us.
//
rsmt_transport::rsmt_transport() :
	transport("rsm", false)
{
	int rc;

	RSMT_TRANS_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_TRANSPORT,
	    ("rsmt_transport(%p)\n", this));

	//
	// the following debug messages ensure that the debug buffers are
	// pre-allocated in a non-interrupt context since it results
	// in "blocking" allocation of memory
	//
	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_DEFAULT,
	    ("DO NOT REMOVE THIS - INITIALIZES THE PATH DEBUG BUFFER\n"));
	RSMT_SEG_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_DEFAULT,
	    ("DO NOT REMOVE THIS - INITIALIZES THE SEG DEBUG BUFFER\n"));
	RSMT_MSG_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_DEFAULT,
	    ("DO NOT REMOVE THIS - INITIALIZES THE MSG DEBUG BUFFER\n"));


	// Some RSMPI layers want segment ids to be unique across the
	// machine, not just the controller, so we keep the value of the
	// last used segment id in the transport object.  RSMT_EP_SEG_BASE
	// comes from rsmt_pathend.h as it is the next segment id after
	// what is used for the pathend segments.
	//
	rtr_segidmp = new rsmt_segid_mgr(RSMT_EP_SEG_BASE,
		RSM_CLUSTER_TRANSPORT_ID_END);

	// initialize the singleton instance of resource framework
	rsmt_resource::initialize();

	// initialize the singleton instance of control message framework
	// manager
	rc = rsmt_cmf_mgr::initialize();
	if (rc) {
		RSMT_TRANS_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_TRANSPORT,
		    ("rsmt_transport() rsmt_cmf_mgr init failed= %d\n", rc));
	}

	//
	// Initialize the trash memory server
	//
	rsmt_rio_trashmem_server::initialize();
}

rsmt_transport::~rsmt_transport()
{
	rsmt_resource::the().shutdown();

	//
	// Shutdown the trash memory server
	//
	rsmt_rio_trashmem_server::shutdown();

	rsmt_cmf_mgr::the().shutdown();

	delete rtr_segidmp;
}

// The generic transport layer calls transport_id() to find out
// our name to see if the operation it is about to do applies to
// us.  Seems like this call shouldn't exist given we provided
// our name above.
//
const char *
rsmt_transport::transport_id()
{
	return ("rsm");
}

pathend *
rsmt_transport::new_pathend(clconf_path_t *path)
{
	rsmt_pathend *pep;

	pep = new rsmt_pathend(this, path);
	ASSERT(pep != NULL);
	RSMT_PATH_DBG(RSMT_DBGL_VERBOSE, RSMT_DBGF_PATHEND,
	    ("rsmt_transport::new_pathend(%p)\n", pep));

	return (pep);
}

adapter *
rsmt_transport::new_adapter(clconf_adapter_t *adp)
{
	rsmt_adapter *radp;

	radp = new rsmt_adapter(this, adp);
	ASSERT(radp != NULL);
	RSMT_PATH_DBG(RSMT_DBGL_VERBOSE, RSMT_DBGF_ADAPTER,
	    ("rsmt_transport::new_adapter(%p)\n", radp));

	// The new_adapter interface is lacking a way to indicate
	// an error has occurred and the caller of new_adpater
	// assumes the return value is non-NULL.  When this changes
	// in the future we will check if rsmt_adapter() was able
	// to get a controller and if it wasn't we'll probably
	// delete the object and return NULL.
	//
	if (radp->rad_rsm_ctrl.handle == NULL) {
		// delete radp;
		// radp = NULL;
	}

	return (radp);
}
