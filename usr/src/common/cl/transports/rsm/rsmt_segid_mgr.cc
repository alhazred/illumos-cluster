//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rsmt_segid_mgr.cc	1.16	08/05/20 SMI"

#include "rsmt_util.h"
#include "rsmt_segid_mgr.h"

//
// rsmt_segid_mgr : global segment id manager
//
// Manages segment id allocation for segments created by
// RSM Transport.
//

//
// rsmt_segid_mgr constructor
//
rsmt_segid_mgr::rsmt_segid_mgr(rsm_memseg_id_t base, uint32_t end)
{
	// memorize the max pool base and end
	sim_base = base;
	sim_end = end;

	// initialize allocation pool parameters and allocate the first pool
	sim_limit = base + RSMT_SEGID_ALLOC_UNIT - 1;
	sim_pool_sz = RSMT_SEGID_ALLOC_UNIT / BITS_PER_BYTE;
	sim_pool = (uchar_t *)operator new((size_t)sim_pool_sz,
		os::SLEEP, os::ZERO);
	ASSERT(sim_pool);

	// hint pointer is used for quick access to first free segment id
	sim_pool_hint = sim_pool;
	RSMT_TRANS_DBG(RSMT_DBGL_VERBOSE, RSMT_DBGF_SEGMGR,
	    ("rsmt_segid_mgr(%p) sim_pool(%p) sim_pool_sz(%d)\n", this,
		sim_pool, sim_pool_sz));
}

//
// rsmt_segid_mgr destructor
//
rsmt_segid_mgr::~rsmt_segid_mgr()
{
	sim_pool_hint = NULL;
	delete sim_pool;
}

rsm_memseg_id_t
rsmt_segid_mgr::segid_alloc()
{
	rsm_memseg_id_t memseg_id;
	uint32_t i;
	uchar_t *pp;
	uchar_t *pool;
	uint32_t pool_sz;

	// search for next available memseg_id
	//
	// use the hint point as the starting point and search byte by
	// byte in the segment id pool
	sim_pool_lock.lock();
	pp = sim_pool_hint;
	for (;;) {
		if (*pp != 0xff) {
			for (i = 0; i < BITS_PER_BYTE; i++) {
				if ((*pp & (1 << i)) == 0) {
					*pp |= (1 << i);
					break;
				}
			}
			sim_pool_hint = pp;
			memseg_id = sim_base + i +
				(uint32_t)((pp - sim_pool) * BITS_PER_BYTE);
			sim_pool_lock.unlock();
			return (memseg_id);
		}
		pp++;
		if (pp == &sim_pool[sim_pool_sz])
			pp = sim_pool;		// wrap back to the start
		if (pp == sim_pool_hint)
			break;			// running out of seg id
	}

	// check the max pool size has been reached
	if (sim_limit >= sim_end) {
		sim_pool_lock.unlock();
		return (0);
	}

	// expand the current pool by allocate a new pool with the
	// extended size
	pool_sz = sim_pool_sz + RSMT_SEGID_ALLOC_UNIT / BITS_PER_BYTE;
	pool = (uchar_t *)operator new((size_t)pool_sz, os::NO_SLEEP, os::ZERO);
	if (pool == NULL) {
		sim_pool_lock.unlock();
		return (0);
	}

	RSMT_TRANS_DBG(RSMT_DBGL_VERBOSE, RSMT_DBGF_SEGMGR,
	    ("rsmt_segid_mgr(%p) old pool(%p:%d) new pool(%p:%d)\n", this,
	    sim_pool, sim_pool_sz, pool, pool_sz));

	// transfer the content from current pool to the newly allocated
	// larger pool and after that, delete the old pool
	bcopy(sim_pool, pool, (size_t)sim_pool_sz);	//lint !e669
	delete sim_pool;
	sim_pool = pool;
	pp = &sim_pool[sim_pool_sz];
	*pp = 1;
	sim_pool_hint = pp;
	sim_pool_sz = pool_sz;
	memseg_id = (rsm_memseg_id_t)(sim_limit + 1);
	sim_limit += RSMT_SEGID_ALLOC_UNIT;
	RSMT_TRANS_DBG(RSMT_DBGL_VERBOSE, RSMT_DBGF_SEGMGR,
	    ("rsmt_segid_mgr(%p) sim_pool_hint(%p) memseg_id(%d)\n", this,
	    sim_pool_hint, memseg_id));
	sim_pool_lock.unlock();
	return (memseg_id);
}

//
// segid_free - return the memseg_id to the allocation pool
//
void
rsmt_segid_mgr::segid_free(rsm_memseg_id_t memseg_id)
{
	uint32_t idx;
	uint32_t offset;
	uchar_t mask;

	// use the memseg_id to calculate the bit mask location in the
	// allocation pool
	idx = (memseg_id - sim_base) / BITS_PER_BYTE;
	offset = memseg_id & (BITS_PER_BYTE - 1);
	mask = 1 << offset;
	sim_pool_lock.lock();
	sim_pool[idx] &= ~mask;
	sim_pool_hint = &sim_pool[idx];
	sim_pool_lock.unlock();
}

//
// is_segid_valid - perform range check for valid memseg_id
//
bool
rsmt_segid_mgr::is_segid_valid(rsm_memseg_id_t memseg_id)
{
	// input memseg_id must be within the allocation pool
	return (sim_base <= memseg_id && memseg_id <= sim_end);
}
