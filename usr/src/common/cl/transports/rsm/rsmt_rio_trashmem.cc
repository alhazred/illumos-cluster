//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rsmt_rio_trashmem.cc	1.9	08/05/20 SMI"

#include <sys/os.h>
#include <sys/refcnt.h>
#include <transports/rsm/rsmt_util.h>
#include <transports/rsm/rsmt_rio_trashmem.h>

//
// Flag to control whether the trashmem feature is enabled.
//
int rsmt_rio_use_trashmem = 1;

//
// Limits imposed on the the minimum and maximum possible sizes of the
// trashmem object by the RSM transport. Unit is PAGESIZE;
//
static const uint_t rsmt_rio_min_tmem = 16; // GFS does many 16 page transfers
uint_t rsmt_rio_max_tmem = 64;

//
// Pointer to the trashmem server object.
//
rsmt_rio_trashmem_server *rsmt_rio_trashmem_server::the_tmem_server = NULL;

//
// rsmt_rio_trashmem constructor.
//
// No trashmem is created in the constructor.
//
rsmt_rio_trashmem::rsmt_rio_trashmem() :
	vaddr(NULL),
	vsize(0),
	raw_vaddr(NULL),
	raw_vsize(0)
{
}

//
// rsmt_rio_trashmem destructor
//
rsmt_rio_trashmem::~rsmt_rio_trashmem()
{
	if (vaddr != NULL) {
		ASSERT(vsize != 0);
		free_chunk_aligned(raw_vaddr, raw_vsize);
		raw_vsize = 0;
	}
	vaddr = NULL;
	raw_vaddr = NULL;
}

//
// rsmt_rio_trashmem::initialize
//
// Method to assign trash memory to the trashmem object. The second
// parameter specifies whether the operation is allowed to block if no
// resources are available.
//
bool
rsmt_rio_trashmem::initialize(size_t sz, os::mem_alloc_type sleepflag)
{
	size_t	aligned_vsize; // on the stack since its not used later

	vaddr = alloc_chunk_aligned(sz, (size_t)PAGESIZE,
	    &aligned_vsize, &raw_vaddr, &raw_vsize, sleepflag);

	ASSERT(vaddr != NULL || sleepflag == os::NO_SLEEP);
	if (vaddr != NULL) {
		RSMT_TRANS_DBG(RSMT_DBGL_INFO, RSMT_DBGF_REPLYIO,
		    ("New trashmem object %p size %d server %p\n",
		    this, sz, &rsmt_rio_trashmem_server::the()));
		vsize = sz;
	}
	return ((vaddr == NULL) ? false : true);
}

//
// rsmt_rio_trashmem::get_vaddr
//
// Return the virtual memory address this trashmem object represents.
//
void *
rsmt_rio_trashmem::get_vaddr()
{
	return (vaddr);
}

//
// rsmt_rio_trashmem::get_vsize
//
// Return the size of the virtual memory this trashmem object represents.
//
size_t
rsmt_rio_trashmem::get_vsize()
{
	return (vsize);
}

//
// rsmt_rio_trashmem_server constructor
//
rsmt_rio_trashmem_server::rsmt_rio_trashmem_server() :
	tmem(NULL),
	cur_tmem_size(0)
{
	if (!rsmt_rio_use_trashmem) {
		return;
	}

	tmem = new rsmt_rio_trashmem();
	(void) tmem->initialize((size_t)(rsmt_rio_min_tmem * PAGESIZE),
	    os::SLEEP);
	cur_tmem_size = tmem->get_vsize();
}

//
// rsmt_rio_trashmem_server destructor
//
rsmt_rio_trashmem_server::~rsmt_rio_trashmem_server()
{
	if (tmem != NULL) {
		tmem->rele();
		tmem = NULL;	//lint !e423
	}
	cur_tmem_size = 0;
}

//
// rsmt_rio_trashmem_server::initialize
//
// Create the trashmem server object
//
void
rsmt_rio_trashmem_server::initialize()
{
	the_tmem_server = new rsmt_rio_trashmem_server();
}

//
// rsmt_rio_trashmem_server::shutdown
//
// Destroy the trashmem server object
//
void
rsmt_rio_trashmem_server::shutdown()
{
	delete the_tmem_server;
	the_tmem_server = NULL;
}

//
// rsmt_rio_trashmem_server::the()
//
// Return a refernce to the trashmem server object.
//
rsmt_rio_trashmem_server &
rsmt_rio_trashmem_server::the()
{
	return (*the_tmem_server);
}

//
// rsmt_rio_trashmeme_server::get_trashmem
//
// Get a pointer to a trashmem object that is at least as big as the
// passed size.
//
// If the current trashmem object can satisfy the request, a hold is
// placed on the object on behalf of the caller and a pointer to the
// trashmem object is returned. The hold will be released when the caller
// makes a subsequent rele_trashmem call.
//
// If the current trashmem object is too small, attempt is made to allocate
// a new trashmem object. If the attempt is successful, a hold is placed
// on the new object and a pointer to the new object is returned. The
// trashmem server releases its hold on the old trashmem object, so that it
// can be destructed when it has no more segments bound to it.
//
// If attempt to create a new trashmem object fails, NULL is returned. The
// caller won't be able to bind its unused segment to trash memory will
// likely have to release it instead of caching it for later use.
//
rsmt_rio_trashmem *
rsmt_rio_trashmem_server::get_trashmem(size_t sz)
{
	size_t			newsize = 0;
	rsmt_rio_trashmem	*newtmem = NULL;

	if (tmem == NULL) {
		return (NULL);
	}

	tmem_lock.lock();
	if (sz <= cur_tmem_size) {
		//
		// Existing trashmem object is good enough, acquire a hold
		// on behalf of the caller and return the pointer.
		//
		tmem->hold();
		tmem_lock.unlock();
		return (tmem);
	}
	if (sz > (size_t)(rsmt_rio_max_tmem * PAGESIZE)) {
		// Can't serve request for this big an object
		tmem_lock.unlock();
		return (NULL);
	}

	//
	// Existing trashmem object is too small, need to allocate a new
	// one. First, pick a good size.
	//
	newsize = cur_tmem_size;
	while (newsize < sz) {
		newsize = 2 * newsize;
	}
	if (newsize > (size_t)(rsmt_rio_max_tmem *PAGESIZE)) {
		newsize = (size_t)(rsmt_rio_max_tmem *PAGESIZE);
	}

	//
	// Non blocking allocation and initialization of the new object.
	//
	newtmem = new (os::NO_SLEEP) rsmt_rio_trashmem();
	if (newtmem == NULL) {
		tmem_lock.unlock();
		return (NULL);
	}
	if (!newtmem->initialize(newsize, os::NO_SLEEP)) {
		tmem_lock.unlock();
		newtmem->rele();
		return (NULL);
	}

	//
	// We have a new trashmem object. Release our hold on the old
	// trashmem object. This hold was acquired automatically when the
	// object was created. It will get destructed when everyone else
	// who has a hold on it also releases it.
	//
	tmem->rele();

	//
	// Make the new object the current object.
	//
	tmem = newtmem;	//lint !e423
	cur_tmem_size = newsize;

	//
	// Acquire a hold on the new object on behalf of the caller.
	//
	tmem->hold();

	tmem_lock.unlock();
	return (tmem);
}

//
// rsmt_rio_trashmem_server::rele_trashmem
//
// Release the hold on a trash memory object obtained via a prior call to
// get_trashmem.
//
void
rsmt_rio_trashmem_server::rele_trashmem(rsmt_rio_trashmem *tm)
{
	tm->rele();
}
