/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RSMT_CMF_CLIENT_H
#define	_RSMT_CMF_CLIENT_H

#pragma ident	"@(#)rsmt_cmf_client.h	1.19	08/05/20 SMI"

#include <sys/types.h>
#include <sys/rsm/rsmpi.h>

#include <sys/refcnt.h>

#include "rsmt_util.h"

// forward declarations...
class rsmt_cmf_adapter;
class rsmt_cmf_procmsg_defer_task;
class rsmt_cmf_sendq;

//
// This file defines the control message framework (cmf) client class.
// The class rsmt_cmf_client implements the control message operations.
// Each rsmt_cmf_client creates a sendq that it uses to send messages to
// its peer.
//
// Any component that uses cmf inherits the rsmt_cmf_client interface and
// hence has access to the control message operation. A cmf client first
// does the setup. The setup comprises of two phases and they are done
// in the following order
//	- cm_register()
//	- cm_enable()
//
// The tear-down of cmf client is also done in two phases and its done in
// the following order
//	- cm_unregister()
//	- cm_disabled()
//
// Control messages can be of two types - flow controlled or non flow
// controlled. Flow control management is done by the rsmt_cmf_client object.
// Incoming flow controlled messages are copied into a local buffer before
// the handler routine is called. Processing of flow controlled messages can
// be deferred by the client. For the non flow controlled messages, the
// interrupt handler is called without copying the payload. The non flow
// controlled messages must be handled in the (interrupt) context they are
// called. The rsmt_cmf_client object maintains a list of preallocated
// buffers that are uses for copying the flow controlled messages. A credit
// update scheme informs the sender how many messages can it send to the
// receiver at any time.
//


//
// Control message header. The control message framework prepends
// this header to every message sent by the client. Special care
// has been taken to keep the size of each field as small as
// possible so that almost all of the interrupt message payload
// can be made available to the client. A message with payload
// size = 0 is an explicit credit update message.
//
typedef struct {
	uchar_t	magic;	// Magic number for compatibility checks
	uchar_t	src;	// Nodeid of the source node
	uchar_t	size;	// Size of the message including the header
	uchar_t	credit_update;	// Any piggybacked credit updates
	uint32_t incn;	// incarnation number of the sending endpoint
	uint32_t cookie; // cookie used for 2-way message - TBD
	uchar_t flags;	// message flags defined by CMF_MSGFLAG_XXXXX
	uchar_t clientid; // clientid defined by cmf_clientid_t
	ushort_t msgid;	// messageid - 0x00-0x1f is reserved
} rsmt_cmhdr_info_t;

typedef union {
	uint64_t		foralignment;
	rsmt_cmhdr_info_t	hdr;
} rsmt_cmhdr_t;

//
// Following flags are currently defined.
//
const int CMF_MSGFLAG_FLOWCTRL = 0x01; // message is flow controlled
const int CMF_MSGFLAG_NEEDREPLY = 0x02; // a 2-way msg, is flow controlled
#ifdef	DEBUG_REDELIVER
const int CMF_MSGFLAG_REDELIVER = 0x80; // forces redelivery
#endif


//
// Each CMF client has an associated id that is used to demultiplex
// the message from a specific node to the corresponding client
//
typedef enum cmf_clientid {
		CMFCLIENT_SEGMGR = 0,
		CMFCLIENT_REPLIO,
		CMFCLIENT_BUFFERIO,
		N_CMFCLIENTS
} cmf_clientid_t;

//
// message ids 0x00-0x1f are reserved for CMF infrastructure
//
const int CMF_MSGID_BASE = 0x20;
const int CMF_MSGID_SYNC = 0x00;
const int CMF_MSGID_SYNCACK = 0x01;
const int CMF_MSGID_CREDIT = 0x02;
const int CMF_MSGID_REPLY = 0x03;

// mode of send - indicates the mode in cm_send() and cm_handle()
const bool CMF_FLOWCONTROL = true; // flow control needed
const bool CMF_NOFLOWCONTROL = false; // non flow control needed

// default flow control window size used in cm_enable
const uchar_t	CMF_WINDOWSZ = 64;

//
// The rsmt_cmf_client class implements the client interface in the
// the control message framework. Any component that needs to send/
// receive control messages via the rsm interrupt queue mechanism inherits
// this class and uses the protected methods for control message
// communication.
//
class rsmt_cmf_client : public refcnt {
public:
	// some class constants
	enum {
		CMF_FFINDEX_UNKNOWN = -1, // indicates ffindex is invalid
		CMF_INTR_PRI = 5, // priority of the interrupt send queue
		CMF_SEND_RETRY = 5, // # retries before backoff during rsm_send
		CMF_BUF_LOTSFREE = 25, // % nbuf that triggers credit msg
		CMF_DFLT_MSGSIZE = 56, // default msg size, used for SCI
		CMF_QUEUE_SIZE = 100, // qdepth of the interrupt send queue
		CMF_MAX_ACKSLOTS = 10 // max num of pending 2way requests
	};

	enum cmf_client_state_t {
		CMF_CLIENT_DISABLED,
		CMF_CLIENT_REGISTERED,
		CMF_CLIENT_ACKPENDING,
		CMF_CLIENT_ACKRECVD,
		CMF_CLIENT_ENABLED,
		CMF_CLIENT_UNREGISTERED
	};

	//
	// Constructor.
	//	Arguments are as follows
	//	local_adpid - local adapter id, available in the pathend class
	//	peer_nodeid - peer's node id, available in the pathend class
	//	peer_adpid - peer's adapter id, available in the pathend class
	//	peer_hwaddr - peer's rsm hwaddr, available in the pathend class
	//	client_id of the client
	//
	rsmt_cmf_client(int, nodeid_t, int, rsm_addr_t, cmf_clientid_t);

	// Destructor
	~rsmt_cmf_client();

	//
	// Adapters call cm_deliver to deliver a control message to the
	// client. This call performs some book keeping operations related
	// to flow control and then calls cm_handle to handle the incoming
	// control message. This function is called in the interrupt context.
	//
	void cm_deliver(void *msgp);

	//
	// Adapters call cm_redeliver to retry delivery of flow-controlled
	// messages that could not be delivered successfully.
	//
	bool cm_redeliver();

	// returns the clientid
	cmf_clientid_t	get_clientid() const;

	// this processes the cmf infrastructure messages. For flow-controlled
	// messages this gets called by the deferred task, otherwise
	// its called in the interrupt context. So implement the processing
	// of a message based on whether its flow-controlled or not.
	bool cm_procmsg_internal(void *msgp);

	//
	// Called by clients to return the memory used by a flow controlled
	// message to the free pool. This should not be called from the
	// interrupt context.
	//
	void cm_free(void *msgp);

	// Clients call cm_register to register the client with
	// the corresponding rsmt_cmf_adapter. At any point of time
	// only one incarnation of a particular client can be
	// registered.
	// NOTE: cm_register should be called before cm_enable.
	void cm_register();

	//
	// Clients call cm_enable to set up infrastructure for
	// control message communication like allocation of message
	// buffers etc.. eg. this is called during endpoint::initiate.
	// cm_enable blocks till the credit synchronization handshake
	// completes, guaranteeing that both peers are ready to exchange
	// control messages.
	// NOTE: cm_enable can be called only after cm_register.
	//
	// fctl_window is the flow control window size
	// fctl_windown defaults to CMF_WINDOWSZ
	//
	bool cm_enable(uchar_t fctl_window = CMF_WINDOWSZ);

	// Clients call cm_unregister to unregisters the client from
	// the rsmt_cmf_adapter. Once this is done a new incarnation
	// of the client is free to call cm_register. cm_unregister should
	// be the first step in the clean up processing. Once cm_unregister
	// is called the client can no longer send/receive control messages.
	// NOTE: cm_unregister should be called before cm_disable.
	void cm_unregister();

	//
	// Destroy control message communication infrastructure.
	// eg. called during endpoint::cleanup. This is the second
	// step in the cleanup processing of the cmf client. This step
	// free up all the rsmpi resources used by the framework and
	// completes the cleanup processing of the cmf_client
	// NOTE: cm_disable should be called after cm_unregister.
	void cm_disable();

	//
	// Send a control message to peer.
	// The client is responsible for allocating the message buffer
	// inclusive of the message header.
	// msgp - is a pointer to the message including the message header.
	// msglen - is the total length of message
	// mode indicates whether message is a flow controlled message
	// (CMF_FLOWCONTROL) or not (CMF_NOFLOWCONTROL).
	//
	bool cm_send(void *msgp, size_t msglen, int msgid, bool mode);

	//
	// Send a control message to peer and synchronously wait for a reply.
	// msgp points to the message, replyp points to the reply buffer
	// where reply is stored.
	// msglen and replylen are total lengths of the buffers
	// These messages are always flow controlled.
	//
	bool cm_send_and_reply(void *msgp, size_t msglen, int msgid,
	    void *replyp, size_t replylen);

	//
	// Send a reply message to peer. replyp points to the buffer
	// containing the request that was received and replyp points
	// to the reply buffer that is being sent to the requestor.
	// These messages are never flow controlled.
	//
	bool cm_reply(void *requestp, void *replyp);

protected:
	//
	// The message handler that the client must implement.
	// msgp is points to the control message including the message header.
	// The last argument indicates if the message is a flow controlled
	// message it is either CMF_FLOWCONTROL or CMF_NOFLOWCONTROL.
	//
	// A non flow controlled message must be handled in the context
	// cm_handle is called. There is no need to free the memory used
	// by the message. Processing of a flow controlled message can
	// be deferred.
	//
	// NOTE: RSMPI calls should not be made in the interrupt context
	//	hence if a handler needs to do so it should be done in a
	//	deferred manner.
	//
	//
	// However, once the processing is done, the message
	// must be freed by a call to cm_free.
	//
	// The return value is true if the message was successfully
	// handled, false otherwise. A non flow controlled message that
	// could not be handled successfully will be dropped by the system.
	// A flow controlled message that could not be handled successfully
	// (most likely due to lack of memory) will be saved and the control
	// message manager will attempt to redeliver it later. The redelivery
	// attempt can happen later either in the interrupt context or in
	// the context of the control message manager drainer thread.
	//
	virtual bool cm_handle(void *msgp, ushort_t msglen, int msgid,
		bool mode) = 0;

	//
	// A msg buffer is referred to by two pointers - a header pointer
	// and a data pointer. The header pointer points to the true
	// beginning of the buffer, whereas the data pointer points to
	// the location after the header. The following two utility
	// methods convert one to another. The client is always
	// presented with the data pointer.
	//
	char *hdr_to_data(char *hdrp) const;
	char *data_to_hdr(char *datap) const;

	// Maximum size of a control message available to the client.
	ushort_t max_cm_size;

private:
	// defines the layout of messages used by the infrastructure
	struct rsmt_cminfr_msg {
		rsmt_cmhdr_t	hdr;
	};

	// true if msgid is reserved for cmf infrastructure else false
	bool reserved_msgid(int msgid) const;

	// this is identical to cm_send except that it is used by the
	// framework to send messages, bypassing some state checks etc.
	bool cm_send_internal(void *msgp, size_t msglen, int msgid,
		bool flowctrl);

	// flag indicating whether synchandler has been posted to threadpool
	bool				synchandler_posted;
	// buffer that is used for the storing the CMF_MSGID_SYNC message
	// during credit synchronization handshake.
	rsmt_cminfr_msg			syncmsg;
	// A permanent defer task used for processing syncmsgs.
	rsmt_cmf_procmsg_defer_task	*synchandler;

	//
	// Each rsmt_cmf_client object maintains a list of preallocated
	// buffers that it uses for storing incoming flow controlled
	// messages. The following methods and instance variables manipulate
	// this buffer pool. The buffers are allocated out of one large
	// contiguous memory area.
	//
	// nbufs   : Total number of buffers in the pool
	// nlotsfree: Number of free bufs that triggers a credit message
	// bufsize : Size of each buffer = max_cm_size + sizeof(rsmt_cmhdr_t)
	// bufs    : Pointer to a memory area (nbufs * bufsize) in size
	// freebufs: An array with nbufs elements that holds pointers to
	//	buffers that are currently free.
	// ff_index: First free index - index of freebufs array element that
	//	has pointer to the first free buffer.
	// send_credit: the number of flow controlled messages that this
	//	client is allowed to send to its peer at this time.
	// due_credit: credit update that this client needs to send to
	//	its peer
	//
	uchar_t		nbufs;
	uchar_t		nlotsfree;
	uchar_t		bufsize;
	void		*bufs;
	void		**freebufs;
	int		ff_index;
	uchar_t		send_credit;
	uchar_t		due_credit;

	//
	// The rsmt_cmf_client object also maintains a list of buffers that
	// contain flow controlled messages that could not be handled so far.
	// These messages must be delivered before any newly arriving message
	// are delivered (FIFO).
	//
	// retrybufs:	An array with nbuf elements that is used to store
	//		pointers to messages that need redelivery.
	// fr_index:	First retry index - index of the retrybufs element
	//		that holds pointer to the first retry message.
	// lr_index:	Last retry index - index of the retrybufs element
	//		that holds pointer to the last retry message.
	// retrycnt:    Number of valid entries in retrybufs
	//
	void		**retrybufs;
	int		fr_index;
	int		lr_index;
	int		retrycnt;

	// The method to allocate a buffer out of the above buffer pool.
	void *cm_alloc();

	cmf_client_state_t	client_state;
	os::condvar_t		client_cv;
	os::mutex_t		client_lock;

	// Client id - identifies the type of client
	cmf_clientid_t	clientid;

	// Peer's nodeid
	nodeid_t	peer_ndid;

	// Peer's rsm address, computed using the peer nodeid
	rsm_addr_t	peer_rsma;

	// Peer's adapter id
	int	peer_adpid;

	// Sendq handle that will be used to send the control messages.
	rsmt_cmf_sendq		*cmf_sendq;

	// Pointer to the adapter object, this client is associated with.
	rsmt_cmf_adapter	*adapterp;

	// Prevent assignment and pass by value
	rsmt_cmf_client(const rsmt_cmf_client &);
	rsmt_cmf_client &operator = (const rsmt_cmf_client &);

	// Pacify lint
	rsmt_cmf_client();
};


//
// rsmt_cmf_procmsg_defer_task: Task to process flow controlled messages.
//
// When a flow controlled cmf infrastructure message needs to be processed
// this task is created.
//
class rsmt_cmf_procmsg_defer_task : public defer_task {
public:
	// takes a pointer to the rsmt_cmf_client object, a pointer to
	// the message buffer containing the message.
	rsmt_cmf_procmsg_defer_task(rsmt_cmf_client *, void  *);
	void execute();

private:
	rsmt_cmf_client *cmf_client;
	void		*cmf_msgp;

	// Prevent assignment and pass by value
	rsmt_cmf_procmsg_defer_task(const rsmt_cmf_procmsg_defer_task &);
	rsmt_cmf_procmsg_defer_task &operator = (const
	    rsmt_cmf_procmsg_defer_task &);

	// Pacify lint
	rsmt_cmf_procmsg_defer_task();
};

//
// This class is a wrapper around the rsm sendq to make it refcntable.
// The purpose is to be able to ensure that the sendq is valid
// during the rsm_send which is done without holding locks.
//
class rsmt_cmf_sendq : public refcnt {
public:
	rsmt_cmf_sendq(rsm_controller_object_t *, rsm_send_q_handle_t);
	rsm_send_q_handle_t	get_sendq();
protected:
	// destroys rsm sendq and deletes the object.
	void refcnt_unref();

	~rsmt_cmf_sendq();
private:
	rsm_send_q_handle_t	sendq;
	rsm_controller_object_t	*ctlp;

	// Prevent assignments and pass by value
	rsmt_cmf_sendq(const rsmt_cmf_sendq&);
	rsmt_cmf_sendq& operator = (const rsmt_cmf_sendq&);

	// Pacify lint
	rsmt_cmf_sendq();
};

#include "rsmt_cmf_client_in.h"

#endif /* _RSMT_CMF_CLIENT_H */
