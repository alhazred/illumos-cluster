/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RSMT_TRANSPORT_H
#define	_RSMT_TRANSPORT_H

#pragma ident	"@(#)rsmt_transport.h	1.22	08/05/20 SMI"

#include <orb/transport/transport.h>
#include <sys/rsm/rsmpi.h>
#include "rsmt_segid_mgr.h"

// Maximum number of adapters allowed. This is the same as
// the number of rsm_intr_service_t available to the transport.
#define	RSMT_MAX_ADAPTERS	30 // (0xDF-0xC0+1) - 2, D0,D4 are unusable

class rsmt_transport : public transport {
public:
	rsmt_transport();
	~rsmt_transport();

	// inherited from transport that must be overridden

	const char *transport_id();
	pathend *new_pathend(clconf_path_t *);
	adapter *new_adapter(clconf_adapter_t *);

	// end inherited

	rsmt_segid_mgr *rtr_get_segidmp();

private:
	rsmt_segid_mgr		*rtr_segidmp;

	// Disallow assignments and pass by value
	rsmt_transport(const rsmt_transport&);
	rsmt_transport &operator = (const rsmt_transport&);
};

#ifndef NOINLINES
#include "rsmt_transport_in.h"
#endif  // _NOINLINES

#endif	/* _RSMT_TRANSPORT_H */
