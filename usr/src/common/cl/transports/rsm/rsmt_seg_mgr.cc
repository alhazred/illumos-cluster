//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rsmt_seg_mgr.cc	1.44	08/05/20 SMI"

#include "rsmt_util.h"
#include "rsmt_transport.h"
#include "rsmt_segid_mgr.h"
#include "rsmt_seg_mgr.h"
#include "rsmt_segment.h"
#include "rsmt_cmf.h"

// static definitions
uint32_t rsmt_seg_mgr::sm_unpublish_errcnt = 0;

// segment manager construction
//
rsmt_seg_mgr::rsmt_seg_mgr(rsmt_transport *trp, rsmt_adapter *adp,
	int local_adapter_id, nodeid_t remote_nodeid,
	int remote_adapter_id, rsm_addr_t remote_addr) :
	rsmt_cmf_client(local_adapter_id, remote_nodeid,
		remote_adapter_id, remote_addr, CMFCLIENT_SEGMGR),
	sm_state(0),
	sm_fp(NULL)
{
	sm_adp = adp;
	sm_remote_addr = remote_addr;
	sm_segidmp = trp->rtr_get_segidmp();
	sm_fp = new rsmt_finalizer(this);
	sm_segcnt = 0;
}

// segment manager destruction
//
rsmt_seg_mgr::~rsmt_seg_mgr()
{
	RSMT_PATH_DBG_D(RSMT_DBGL_INFO, RSMT_DBGF_SEGMGR,
	    ("Segment manager destruction %p\n", this));

	sm_adp = NULL;
	sm_segidmp = NULL;
	if (sm_fp)
		delete sm_fp;
	sm_fp = NULL;
	sm_segcnt = 0;
}

// initialization function
//
void
rsmt_seg_mgr::initiate_init()
{
	// register control message
	cm_register();
}

bool
rsmt_seg_mgr::initiate_fini()
{
	// enable control message
	if (cm_enable(RSMT_SM_FLOWCONTROL)) {
		sm_state |= RSMT_SMST_ENABLE;
		return (true);
	}
	return (false);
}

void
rsmt_seg_mgr::initiate_cleanup()
{
	cm_unregister();
	if (sm_state & RSMT_SMST_ENABLE)
		cm_disable();
}

// destroy segments on the deferred destroy list
//
void
rsmt_seg_mgr::reap_destroy_seglist()
{
	rsmt_export_segment *exsegp;
	rsm_memseg_id_t memseg_id;
	int rc;

	// destroy segments on the to-be-destroyed segment list
	// lookup the destroyed segment list
	sm_disconnect_lock.lock();
	sm_destroy_seglist.atfirst();
	while ((exsegp = sm_destroy_seglist.get_current()) != NULL) {
		// destroy the segment
		rc = exsegp->export_close();
		RSMT_PATH_DBG_D(RSMT_DBGL_INFO, RSMT_DBGF_SEGMGR,
		    ("reap_destroy_seglist segp=%p rc=%d\n",
			exsegp, rc));
		// segment unpublish will fail if
		// the segment has not been disconnected
		sm_destroy_seglist.advance();
		if (rc == RSM_SUCCESS) {
			(void) sm_destroy_seglist.erase(exsegp);
			memseg_id = exsegp->get_memseg_id();
			delete exsegp;
			// free memory segment id
			sm_segidmp->segid_free(memseg_id);
			// decrement the refcnt on destroying seg obj
			sm_segcnt_lock.lock();
			sm_segcnt--;
			sm_segcnt_lock.unlock();
		}
	}
	sm_disconnect_lock.unlock();
}

// finalizer task to destroy all segments on the deferred destroy list
//
void
rsmt_seg_mgr::finalizer()
{
	RSMT_PATH_DBG(RSMT_DBGL_INFO, RSMT_DBGF_SEGMGR,
	    ("finalizer\n"));
	while (1) {
		sm_segcnt_lock.lock();
		if (sm_segcnt == 0)
			break;
		sm_segcnt_lock.unlock();
		reap_destroy_seglist();
		sm_segcnt_lock.lock();
		if (sm_segcnt == 0)
			break;
		sm_segcnt_lock.unlock();
		os::usecsleep(2000 * 1000L);
	}
	sm_state &= ~RSMT_SMST_FINALIZER;
	sm_segcnt_cv.signal();
	sm_segcnt_lock.unlock();
}

// unregister control message
// set internal state to disable
//
void
rsmt_seg_mgr::unblock()
{
	if (sm_state & RSMT_SMST_ENABLE) {
		cm_unregister();
		// no control messages received after this point
		sm_state &= ~RSMT_SMST_ENABLE;
	}
}

// unregister control message
// set internal state to disable
//
void
rsmt_seg_mgr::cleanup_init()
{
	if (sm_state & RSMT_SMST_ENABLE) {
		cm_unregister();
		// no control messages received after this point
		sm_state &= ~RSMT_SMST_ENABLE;
	}
}

// disable control message and internal cleanup
//
void
rsmt_seg_mgr::cleanup_fini()
{
	rsmt_import_segment *imsegp;

	sm_import_lock.lock();
	sm_disconnect_lock.lock();
	cm_disable();
	// no control messages sent after this point
	sm_disconnect_lock.unlock();

	// lookup the unclaimed segment list
	while ((imsegp = sm_unclaimed_seglist.reapfirst()) != NULL) {
		// disconnect import segments
		imsegp->import_close();
		delete imsegp;
		// decrement the refcnt on destroying seg obj
		sm_segcnt_lock.lock();
		sm_segcnt--;
		sm_segcnt_lock.unlock();
	}
	sm_import_lock.unlock();

	// unpublish export segments on the destroy list
	reap_destroy_seglist();

	sm_segcnt_lock.lock();
	if (sm_segcnt == 0) {
		sm_segcnt_lock.unlock();
		ASSERT(sm_import_seglist.empty());
		return;
	}
	// set flag - run finalizer
	sm_state |= RSMT_SMST_FINALIZER;
	sm_segcnt_lock.unlock();

	// start the finalizer thread to destroy
	// segments that have not been disconnected
	common_threadpool::the().defer_processing(sm_fp);
	// wait for the finalizer thread to destroy all segments
	sm_segcnt_lock.lock();
	while (sm_state & RSMT_SMST_FINALIZER) {
		sm_segcnt_cv.wait(&sm_segcnt_lock);
	}
	sm_segcnt_lock.unlock();
	ASSERT(sm_import_seglist.empty());
}


// create or export a segment
//
rsmt_export_segment *
rsmt_seg_mgr::create_seg(size_t seg_size, uint32_t exflags, void *mem_addr,
	size_t mem_len, rsm_resource_callback_t waitflag)
{
	rsmt_export_segment *segp;
	rsm_memseg_id_t memseg_id;
	uint_t export_flags;
	int rc;
	rsmt_sm_msg_t msg;
	rsmt_sm_msg_t *mp;

	// alloc mem seg id and export segment handle object
	memseg_id = sm_segidmp->segid_alloc();

	ASSERT(sm_segidmp->is_segid_valid(memseg_id));

	segp = new rsmt_export_segment(sm_adp, seg_size, memseg_id,
		sm_remote_addr, mem_addr, mem_len);

	if (exflags & RSMT_SMEX_UNBIND_REBIND)
		export_flags = RSM_ALLOW_UNBIND_REBIND;
	else
		export_flags = 0;

	// create and publish
	rc = segp->export_open(export_flags, waitflag);
	if (rc != RSM_SUCCESS) {
		delete segp;
		sm_segidmp->segid_free(memseg_id);
		RSMT_SEG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_SEGMGR,
		    ("export_open failed rc=%d\n", rc));
		return (NULL);
	}
	// increment the refcnt when creating a segment obj
	sm_segcnt_lock.lock();
	sm_segcnt++;
	sm_segcnt_lock.unlock();

	// check connection to peer for import
	if ((exflags & RSMT_SMEX_CONNECT) == 0) {
		return (segp);
	}

	// send control message to peer for import connect
	mp = &msg;
	mp->sm_token = get_conn_token(segp);
	(void) cm_send((void *)mp, RSMT_SM_MSGSZ, RSMT_CONN_ACTION_CONNECT,
		CMF_FLOWCONTROL);

	return (segp);
}

// destroy an export segment
//
void
rsmt_seg_mgr::destroy_seg(rsmt_export_segment *segp, uint32_t flags,
    rsmt_seg_destroy_callback_t *destroy_callback, void *callback_param)
{
	rsm_memseg_id_t memseg_id;
	rsmt_export_segment *disconn_segp;
	int rc;

	//
	// Save away the destroy callback, so that it will be called when
	// the segment in question actually gets destroyed.
	//
	if (destroy_callback != (rsmt_seg_destroy_callback_t *)NULL) {
		segp->set_destroy_callback(destroy_callback, callback_param);
	}

	memseg_id = segp->get_memseg_id();

	//
	// Destroy the segment right away - this is to clean up a segment that
	// no one has ever connected to therefore the whole disconnect_seglist
	// dance will not work and is not needed.
	//
	if (flags & RSMT_SMEX_NO_CONNECTIONS) {
		rc = segp->export_close();
		ASSERT(rc == RSM_SUCCESS);
		RSMT_SEG_DBG_D(RSMT_DBGL_INFO, RSMT_DBGF_SEGMGR,
		    ("destroy_seg export_close(%p)=%d immediate\n", segp, rc));

		sm_segidmp->segid_free(memseg_id);
		// decrement the refcnt on destroying seg obj
		sm_segcnt_lock.lock();
		sm_segcnt--;
		sm_segcnt_lock.unlock();
		delete segp;
		return;
	}

	sm_disconnect_lock.lock();
	// lookup the disconnected segment list
	sm_disconnect_seglist.atfirst();
	while ((disconn_segp = sm_disconnect_seglist.get_current()) != NULL) {
		sm_disconnect_seglist.advance();
		if (memseg_id == disconn_segp->get_memseg_id()) {
			(void) sm_disconnect_seglist.erase(disconn_segp);
			sm_disconnect_lock.unlock();

			// destroy the segment
			RSMT_SEG_DBG_D(RSMT_DBGL_INFO, RSMT_DBGF_SEGMGR,
			    ("destroy_seg - match\n"));

			delete disconn_segp;

			//
			// The segment is on the disconnect list meaning that
			// the disconnect message was recieved and now the
			// exporter is destroying the segment it. Although its
			// possible that the segment was added to the disconnect
			// list before the rsmpi driver sees the disconnect, in
			// which case export_close will fail with
			// RSMERR_SEG_IN_USE - so there is a need to retry.
			//
			int i = 0;
			// retry 10 times
			do {
				if ((rc = segp->export_close()) == RSM_SUCCESS)
					break;
			} while (++i < 10);

			RSMT_SEG_DBG(RSMT_DBGL_INFO, RSMT_DBGF_SEGMGR,
			    ("destroy_seg export_close(%p)=%d retry=%d\n",
				segp, rc, i));

			//
			// If all attempts to unpublish the segment fail,
			// put it on the destroy list to be destroyed later
			// when the endpoint is torn down. The code following
			// this while loop will accomplish that. The
			// sm_disconnect_lock needs to be acquired before
			// breaking out of the loop as code following the
			// loop expects that lock to be held no matter
			// what path was taken.
			//
			if (rc != RSM_SUCCESS) {
				os::atomic_add_32(&sm_unpublish_errcnt, 1);
				sm_disconnect_lock.lock();
				break;
			}

			sm_segidmp->segid_free(memseg_id);
			// decrement the refcnt on destroying seg obj
			sm_segcnt_lock.lock();
			sm_segcnt--;
			sm_segcnt_lock.unlock();
			delete segp;
			return;
		}
	}

	// check for enable state
	if (sm_state & 	RSMT_SMST_ENABLE) {
		// delay by adding to the to-be-destroyed segment list
		sm_destroy_seglist.append(segp);
		sm_disconnect_lock.unlock();
		return;
	}

	//
	// The segment was not found on the disconnect_seglist and the state
	// is not enabled hence no new control messages will be delivered
	// try to destroy the segment otherwise put it on the destroy_seglist
	// so that the endpoint cleanup will take care of the destruction
	//

	sm_disconnect_lock.unlock();
	RSMT_SEG_DBG_D(RSMT_DBGL_INFO, RSMT_DBGF_SEGMGR,
	    ("destroy_seg nomatch segp=%p\n", segp));
	rc = segp->export_close();
	if (rc == RSM_SUCCESS) {
		delete segp;
		sm_segidmp->segid_free(memseg_id);
		// decrement the refcnt on destroying seg obj
		sm_segcnt_lock.lock();
		sm_segcnt--;
		sm_segcnt_lock.unlock();
		return;
	}

	// defer the destroy segment until it is disconnected
	sm_disconnect_lock.lock();
	sm_destroy_seglist.append(segp);
	sm_disconnect_lock.unlock();
}

// segment destroy by defer task
//
void
rsmt_seg_mgr::defer_destroy_seg(rsmt_seg_conn_token_t token)
{
	rsm_memseg_id_t memseg_id;
	rsmt_export_segment *segp;
	int rc;

	memseg_id = get_memseg_id_by_token(token);
	sm_disconnect_lock.lock();
	// check for enable state
	if (!(sm_state & RSMT_SMST_ENABLE)) {
		// cleanup had happened
		sm_disconnect_lock.unlock();
		return;
	}

	// lookup the destroyed segment list
	sm_destroy_seglist.atfirst();
	while ((segp = sm_destroy_seglist.get_current()) != NULL) {
		sm_destroy_seglist.advance();
		if (memseg_id == segp->get_memseg_id()) {
			(void) sm_destroy_seglist.erase(segp);
			sm_disconnect_lock.unlock();

			// destroy the segment

			//
			// exporter has done the destroy_seg and we were waiting
			// for the importer to tell us about the disconnect.
			// We are here, that means we received the disconnect
			// message. Although its very possible that we got the
			// the disconnect msg before the rsmpi driver has seen
			// the disconnect so retry and export_close will
			// eventually succeed.
			//
			int i = 0;
			// retry 10 times
			do {
				if ((rc = segp->export_close()) == RSM_SUCCESS)
					break;
			} while (++i < 10);

			RSMT_SEG_DBG(RSMT_DBGL_INFO, RSMT_DBGF_SEGMGR,
			    ("defer_destroy_seg export_close(%p)=%d retry=%d\n",
				segp, rc, i));

			//
			// If all attempts to unpublish the segment fail,
			// put it back on the destroy list to be destroyed
			// later when the endpoint is torn down.
			//
			if (rc != RSM_SUCCESS) {
				os::atomic_add_32(&sm_unpublish_errcnt, 1);
				sm_disconnect_lock.lock();
				sm_destroy_seglist.append(segp);
				sm_disconnect_lock.unlock();
				return;
			}

			delete segp;
			sm_segidmp->segid_free(memseg_id);
			// decrement the refcnt on destroying seg obj
			sm_segcnt_lock.lock();
			sm_segcnt--;
			sm_segcnt_lock.unlock();
			return;
		}
	}

	// add to the disconnected segment list
	segp = new rsmt_export_segment(sm_adp, (size_t)0, memseg_id,
		sm_remote_addr, 0, (size_t)0);
	sm_disconnect_seglist.append(segp);
	sm_disconnect_lock.unlock();
}

// segment connection or import
//
rsmt_import_segment *
rsmt_seg_mgr::connect_seg(rsmt_seg_conn_token_t rsmt_conn_token)
{
	rsmt_import_segment *segp;
	rsm_memseg_id_t memseg_id;
	size_t sz;
	int rc;

	memseg_id = get_memseg_id_by_token(rsmt_conn_token);

	sm_import_lock.lock();
	// lookup the unclaimed segment list
	sm_unclaimed_seglist.atfirst();
	while ((segp = sm_unclaimed_seglist.get_current()) != NULL) {
		sm_unclaimed_seglist.advance();
		if (memseg_id == segp->get_memseg_id()) {
			(void) sm_unclaimed_seglist.erase(segp);
			sm_import_lock.unlock();
			RSMT_SEG_DBG_D(RSMT_DBGL_INFO, RSMT_DBGF_SEGMGR,
			    ("connect_seg : match %p\n", segp));
			return (segp);
		}
	}

	sz = get_memseg_sz_by_token(rsmt_conn_token);
	RSMT_SEG_DBG_D(RSMT_DBGL_INFO, RSMT_DBGF_SEGMGR,
	    ("connect_seg : con segid=0x%x sz=0x%x raddr=0x%llx\n",
		memseg_id, sz, sm_remote_addr));
	// create the import segment
	segp = new rsmt_import_segment(sm_adp, sz, memseg_id, sm_remote_addr);

	rc = segp->import_open();
	if (rc != RSM_SUCCESS) {
		sm_import_lock.unlock();
		delete segp;
		RSMT_SEG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_SEGMGR,
		    ("import_open failed rc=%d\n", rc));
		return (NULL);
	}
	// increment the refcnt when creating a segment obj
	sm_segcnt_lock.lock();
	sm_segcnt++;
	sm_segcnt_lock.unlock();

	if (sm_state & RSMT_SMST_ENABLE) {
		// add the new segment to the import segment list
		sm_import_seglist.append(segp);
	}
	sm_import_lock.unlock();

	return (segp);
}

// segment connection or import by defer task
//
void
rsmt_seg_mgr::defer_connect_seg(rsmt_seg_conn_token_t token)
{
	rsmt_import_segment *segp;
	rsm_memseg_id_t memseg_id;
	size_t sz;
	int rc;

	memseg_id = get_memseg_id_by_token(token);

	RSMT_SEG_DBG_D(RSMT_DBGL_INFO, RSMT_DBGF_SEGMGR,
	    ("defer connect_seg : segid = 0x%x\n", memseg_id));

	sm_import_lock.lock();
	if (!(sm_state & RSMT_SMST_ENABLE)) {
		// cleanup had happened
		sm_import_lock.unlock();
		return;
	}
	// lookup the import segment list
	sm_import_seglist.atfirst();
	while ((segp = sm_import_seglist.get_current()) != NULL) {
		sm_import_seglist.advance();
		if (memseg_id == segp->get_memseg_id()) {
			(void) sm_import_seglist.erase(segp);
			sm_import_lock.unlock();
			return;
		}
	}
	sz = get_memseg_sz_by_token(token);
	// create the import segment
	segp = new rsmt_import_segment(sm_adp, sz, memseg_id, sm_remote_addr);

	rc = segp->import_open();
	if (rc != RSM_SUCCESS) {
		// might have to record the token so that connect_seg()
		// will not put the new segment to the import segment list
		sm_import_lock.unlock();
		delete segp;
		return;
	}
	// increment the refcnt when creating a segment obj
	sm_segcnt_lock.lock();
	sm_segcnt++;
	sm_segcnt_lock.unlock();

	// add the new segment to the unclaimed segment list
	sm_unclaimed_seglist.append(segp);
	sm_import_lock.unlock();
}

// disconnect segment
//
void
rsmt_seg_mgr::disconnect_seg(rsmt_import_segment *segp)
{
	rsmt_import_segment *imsegp;
	rsmt_sm_msg_t msg;
	rsmt_sm_msg_t *mp;

	RSMT_SEG_DBG_D(RSMT_DBGL_INFO, RSMT_DBGF_SEGMGR,
	    ("disconnect_seg : seg = %p\n", segp));

	// disconnect segment
	segp->import_close();

	// send control message to peer for export destroy
	mp = &msg;
	mp->sm_token = get_conn_token(segp);
	(void) cm_send((void *)mp, RSMT_SM_MSGSZ, RSMT_CONN_ACTION_DISCONNECT,
		CMF_FLOWCONTROL);

	sm_import_lock.lock();
	// check import segment list in case the connect msg never came
	sm_import_seglist.atfirst();
	while ((imsegp = sm_import_seglist.get_current()) != NULL) {
		sm_import_seglist.advance();
		if (segp == imsegp) {
			(void) sm_import_seglist.erase(imsegp);
			break;
		}
	}
	sm_import_lock.unlock();

	delete segp;
	// decrement the refcnt on destroying seg obj
	sm_segcnt_lock.lock();
	sm_segcnt--;
	sm_segcnt_lock.unlock();
}

// get connection token from segment
//
rsmt_seg_conn_token_t
rsmt_seg_mgr::get_conn_token(rsmt_segment *rsmt_segp)
{
	rsmt_seg_conn_token_t t;

	//
	// Create the connection token by concatenating two 32 bit
	// entities - the segment size and the segment id into one
	// 64 bit token.
	//
	t = (rsmt_seg_conn_token_t)rsmt_segp->get_seg_size();
	t = (t << 32) | rsmt_segp->get_memseg_id();
	return (t);
}

// get mem seg id from connection token
//
rsm_memseg_id_t
rsmt_seg_mgr::get_memseg_id_by_token(rsmt_seg_conn_token_t conn_token)
{
	return ((rsm_memseg_id_t)conn_token);
}

// get segment size from connection token
//
size_t
rsmt_seg_mgr::get_memseg_sz_by_token(rsmt_seg_conn_token_t conn_token)
{
	//
	// The connection token is a 64 bit entity. The first 32 bits
	// represent the segment size.
	//
	return ((size_t)(conn_token >> 32));
}

// control message service handler
//
bool
rsmt_seg_mgr::cm_handle(void *msgp, ushort_t msglen, int msgid, bool flowctrl)
{
	char	*dp;
	rsmt_seg_conn_token_t token;
	rsmt_connect_defer_task *dt;
	os::mem_alloc_type flag = os::NO_SLEEP;

	msglen = msglen;
	flowctrl = flowctrl;

	// retrive the connection token
	dp = hdr_to_data((char *)msgp);
	token = *((rsmt_seg_conn_tokenp_t)dp);
	// spawn a defer task
	dt = new(flag) rsmt_connect_defer_task(this, token,
		(rsmt_connect_action_type) msgid, msgp);
	if (dt == NULL)
		return (false);
	common_threadpool::the().defer_processing(dt);
	return (true);
}

// connection type defer task constructor
//
rsmt_connect_defer_task::rsmt_connect_defer_task(rsmt_seg_mgr *smp,
	rsmt_seg_conn_token_t token, rsmt_connect_action_type type, void *msgp)
{
	dt_segmgrp = smp;
	dt_token = token;
	dt_type = type;
	dt_msgp = msgp;
}

// connection type defer task body
//
void
rsmt_connect_defer_task::execute()
{
	// free the message
	dt_segmgrp->cm_free(dt_msgp);

	// check for message type
	if (dt_type == RSMT_CONN_ACTION_CONNECT) {
		dt_segmgrp->defer_connect_seg(dt_token);
	} else if (dt_type == RSMT_CONN_ACTION_DISCONNECT) {
		dt_segmgrp->defer_destroy_seg(dt_token);
	} else {
		ASSERT((dt_type == RSMT_CONN_ACTION_CONNECT) ||
			(dt_type == RSMT_CONN_ACTION_DISCONNECT));
	}
	// delete the defer task object
	delete this;
}

// finalizer task constructor
//
rsmt_finalizer::rsmt_finalizer(rsmt_seg_mgr *smp)
{
	fdt_segmgrp = smp;
}

// finalizer task body
//
void
rsmt_finalizer::execute()
{
	fdt_segmgrp->finalizer();
}
