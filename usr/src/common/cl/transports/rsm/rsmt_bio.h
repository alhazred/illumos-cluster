/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RSMT_BIO_H
#define	_RSMT_BIO_H

#pragma ident	"@(#)rsmt_bio.h	1.17	08/05/20 SMI"

#include <sys/time.h>

#include "rsmt_cmf_client.h"
#include "rsmt_seg_mgr.h"
#include "rsmt_segment.h"
#include "rsmt_bio_seg.h"
#include "rsmt_bio_jumbo_seg.h"

#define	ROUNDUP(x, y)	((((x) + (y) - 1)/(y))*(y))

// forward reference
class rsmt_sendstream;

//
// This file implements the bufferio manager. The normal marshalled ORB
// messages are exchanged over the bufferio mechanism. Each rsm transport
// endpoint has a bufferio manager which manages all the bufferio resources.
// These bufferio resources are
//	- segment groups - each bufferio manager has an array of a fixed
//	number (RSMT_BIO_MAX_SEGGRPS) of segment groups. Each segment group
//	has segments that are subdivided into partitions that the transport
//	uses to carry messages. Segment group has export segments which serve
//	as receive buffers as well as import segments used for sending data.
//
//	- partition-avail segment - each bufferio manager creates and exports
//	a partition-avail segment, which is used by the remote endpoint to
//	update information about partitions state. After consuming the data
//	in the partition the receiver updates its generation number which
//	is a monotonically increasing number.
//
//	The following is the layout of the partition-avail segment.
//
//	+---------------------------------------------------------------+
//	| genr[0] | genr[1] | genr[2] | genr[3] | genr[4] ......^.......|
//	+-------------------------------------------------------|-------+
//	| Array of per send segment generation numbers indexed	|
//	| by seg_index						|
//	|						max_supported_segs
//	|	SEGINFO AREA					|
//	|							v
//	+---------------------------------------------------------------+
//	| Array of per partiton generation number for seggrp[0] ^
//	| First array is for partitions of seg_index 0, then	|
//	| seg_index 1 and so on				RSMT_BIO_MAX_PARTITIONS
//	|							|
//	|	PARTITIONINFO AREA for seggrp 0			|
//	|							v
//	+---------------------------------------------------------------+
//	| Array of per partition generation number for seggrp[1] and so on..
//	|
//	|	PARTITIONINFO AREA for seggrp 1
//
//	Note: In the SEGINFO AREA the space used by a seggrp is always a
//	multiple of RSMT_CACHELINE, this ensures atomicity of transactions.
//	For the same reasons the space used to store partition generation
//	numbers in the PARTITIONINFO AREA for each segment is also a
//	multiple of  RSMT_CACHELINE
//
//	The bufferio manager is a subclass of rsmt_cmf_client. Each endpoint
//	has a bufferio manager that it interfaces with for sending marshalled
//	ORB messages. The rsmt_sendstream uses the bufferio manager send()
//	method to send messages.
//

// Number of generation numbers that can be stored in a cacheline
const int RSMT_GENRPERCACHELINE = RSMT_CACHELINE / (int)sizeof (hrtime_t);

// Number of time puts are retried due to barrier failures
const int RSMT_BARRIER_RETRYCNT = 5;

// Max number of partitions per segment-group that bufferio supports
const int RSMT_BIO_MAX_PARTITIONS = (int)(2*PAGESIZE/sizeof (hrtime_t));

// Number of segment groups that bufferio supports
const int RSMT_BIO_MAX_SEGGRPS = 5;

// Max number of writes coalesced before sending an interrupt
const int RSMT_MAX_SENDACCUM = 30;

// CMF flow control window size for bufferio
const int RSMT_BIO_CMFWINDOW = 64;


// This struct defines the properties of a bufferio segment.
struct rsmt_seg_property {
	// size of each partition
	size_t	partition_sz;
	// # of contiguous partitions that can be used to carry a message
	// currently span_factor can either be 1 or 2.
	int	span_factor;
	// the weighting factor for the segment size calculation, this is used
	// to divide up the total rsm segment space available for bufferio
	// among segments.
	int	size_weight;
};

//
// Partition sizes of segment groups should be sorted in ascending order
//
const struct rsmt_seg_property RSMT_BIO_PARTITIONS[RSMT_BIO_MAX_SEGGRPS] = {
	// intended for checkpoint messages, span_factor=1 ensures that only
	// messages <= 256 go over this segment group
	{256, 1, 1},
	// this segment group is used for messages of size > 256, <= 1K bytes
	// since the span_factor=2
	{512, 2, 1},
	// this segment group is used for messages of size > 1K, <= 4K bytes
	{2*1024, 2, 1},
	// this segment group can take messages of size > 4K, <= 16K bytes
	// 8K sized bulkio messages are sent using this segment group
	// size_weight = 2 ensures that there are sufficient partitions in
	// this segment group for bulkio traffic (8KB messages)
	{8*1024, 2, 2},
	// this segment group can take messages of size > 16K, <= 128K bytes
	{64*1024, 2, 1}
};

//
// CMF message types used by bufferio
//
enum rsmt_bio_msgid {
	// request to connect to the partition-avail segment
	RSMT_BIO_PAVAIL_CONNECT = CMF_MSGID_BASE + 1,
	// request to create a bufferio receive segment
	RSMT_BIO_SEGCREATE,
	// request to connect to a bufferio segment
	RSMT_BIO_SEGCONNECT,
	// notification to receiver of marshalled messages
	RSMT_BIO_MSGWAITING,
	// sender's request to update partition-avail segment
	RSMT_BIO_UPD_PAVAIL,
	// notification to the receiver to process a chunk of the jumbo message
	RSMT_BIO_MSGCHUNK,
	// ack from receiver that chunk of the jumbo message has been processed
	RSMT_BIO_MSGCHUNKACK,
	// notification to reclaim partitions
	RSMT_BIO_RECLAIM_PRT,
	// ack to indicate reclaim processesing has been completed
	RSMT_BIO_RECLAIM_PRTACK
};

// This struct is embedded in the RSMT_BIO_PAVAIL_CONNECT message and
// carries info about a segment group.
typedef struct {
	ushort_t	maxsegs;
	ushort_t	maxpartitions;
} rsmt_seggrp_info_t;


//
// control message structures
//
typedef struct {
	rsmt_cmhdr_t		bio_hdr;
	union {
		struct {
			rsmt_seg_conn_token_t	tok;
			rsmt_seggrp_info_t	sgrpinfo[RSMT_BIO_MAX_SEGGRPS];
		} pac_msg; // RSMT_BIO_PAVAIL_CONNECT message
		struct {
			uchar_t			flags;
#define		RSMT_BIO_CREATEJUMBO	0x01
			uchar_t			nseggrp;
			ushort_t		segindex;
			ushort_t		partitions;
			size_t			reqsize; // requested segsize
		} bcr_msg; // RSMT_BIO_SEGCREATE message
		struct {
			rsmt_seg_conn_token_t	tok;
			uchar_t			flags;
#define		RSMT_BIO_CONNECTJUMBO	0x01
#define		RSMT_BIO_CONNECTERROR	0x02
			uchar_t			nseggrp;
			ushort_t		segindex;
			ushort_t		partitions;
		} bco_msg; // RSMT_BIO_SEGCONNECT message
		struct {
			ushort_t		msgpend[RSMT_BIO_MAX_SEGGRPS];
		} bmw_msg; // RSMT_BIO_MSGWAITING message
		struct {
		} upa_msg; // RSMT_BIO_UPD_PAVAIL message
		struct {
			off_t			msg_offset;
			size_t			msg_len;
			uchar_t			flags;
#define		RSMT_BIO_CHUNKFIRST	0x01
			ushort_t		segindex;
		} bmc_msg; // RSMT_BIO_MSGCHUNK message
		struct {
			uchar_t			flags;
			ushort_t		segindex;
		} bmca_msg; // RSMT_BIO_MSGCHUNKACK message
		struct {
			uchar_t			seggrpindex;
		} brp_msg; // RSMT_BIO_RECLAIM_PRT
		struct {
			uchar_t			seggrpindex;
		} brpa_msg; // RSMT_BIO_RECLAIM_PRTACK
	} u;
} rsmt_bio_msg_t;

//
// rsmt_bio_mgr is the bufferio manager (bio_mgr) that the manages
// various segment groups and is used to send marshalled ORB messages.
// This class is instantiated by the rsmt_endpoint.
// The biomgr provides interfaces for the rsmt_endpoint to initialize
// as well as cleanup. Each bufferio manager has a fixed number of segment
// groups that it supports. The bufferio manager has a maximum number of
// segments that it can create and export. This limit is passed as an
// argument when it the biomgr is instantiated based on system constraints.
// The maximum number of segments is equally divided over all the segment
// groups. Each segment group has a maximum number of partitions that can be
// supported - this is tied into number of entries for every segment group in
// the partition-avail segment.
class rsmt_bio_mgr : public rsmt_cmf_client {
public:
	// Constructor
	//	Pointer to the endpoint
	//	Pointer to the segment manager
	//	local_adpid - local adapter id
	//	peer_nodeid - peer's node id
	//	peer_adpid - peer's adapter id
	//	peer_hwaddr - peer's rsm hwaddr
	//	max_supported_segments - an upper limit on max_segments.
	//	max_segments - maximum number of segments
	//	max_jumbo_segs - maximum number of jumbo segments
	//	max_jumbo_seg_size - maximum size of a jumbo segment
	//  max_supported_segments is a static for a given interconnect type.
	//  max_segments and max_seg_size are a based on interconnect type,
	//  number of nodes in the cluster, memory on each node etc.
	rsmt_bio_mgr(rsmt_endpoint *, rsmt_seg_mgr *, int, nodeid_t, int,
	    rsm_addr_t, int, int, size_t, int, size_t);

	// Destructor
	~rsmt_bio_mgr();

	// Called from endpoint::initiate
	void initiate_init();
	bool initiate_fini();
	void initiate_cleanup();

	// Called from endpoint::unblock
	void unblock();

	// Called from endpoint::push
	void push();

	// Called from endpoint::cleanup
	void cleanup_init();
	void cleanup_fini_pre_sm();
	void cleanup_fini_post_sm();

	//
	// Called from the rsmt_bio_seggrp::msg_consumed() to do a remote
	// update of the pavail segment for all recv segments in this segment
	// group -
	// arguments are seggrp-index, shadow_pavail, # of rsegs, size of
	// per receive segment shadow_pavail array, start segment index,
	// per segment genr number array, size of the per segment genr number
	// array
	//
	bool update_pavail(int, hrtime_t **, int, size_t, int, hrtime_t *,
	    size_t);

	// Send that is called from the rsmt_sendstream::send
	bool send(rsmt_sendstream *, rsmt_hdr_t *, Buf *);

	// Inherited from the rsmt_cmf_client class
	bool cm_handle(void *, ushort_t, int, bool);
	// End inherited

	// Method to process control messages - called from cm_handle
	// or by the defer task processing the flow-controlled messages
	void processmsg(void *, ushort_t, int);

	// Return a pointer to the segment manager
	rsmt_seg_mgr *get_segmgr();

	// Return a pointer to the endpoint
	rsmt_endpoint *get_endpoint();

	// Return a pointer to the area in pavail segment that has
	// the per segment generation number
	hrtime_t *get_pavail_seginfo();

	// Return a pointer to the area in pavail segment that has
	// the per partition generation number for a specified segment
	// group index
	hrtime_t *get_pavail_partitioninfo(int);

	// Given a buffer size returns the amount of space that will be
	// used by bufferio to carry the message
	static uint32_t get_chunksize(uint32_t);

private:
	// Once iteration over all segment groups pushing arrived messages
	// to the ORB.
	bool	push_once();

	// Keep pushing messages up to the ORB until it is guaranteed that
	// no more messages can arrive using any of the segment groups
	// maintained by this bio object.
	void	push_all();

	// Pointer to the segment manager
	rsmt_seg_mgr	*segmgr;

	// Maximum number of export segments that can be supported by bufferio.
	// It defines an upper limit on the number of segments in bufferio,
	// therefore max_segments <= max_supported_segments.
	// This is a static property for a given interconnect type. This
	// is used during the creation of the pavail segment to predetermine
	// its layout - essentially the offset of the per-partition generation
	// number in the pavail segment.
	int	max_supported_segments;

	// Maximum number of export segments that bufferio can create
	// since there is a one to one mapping to an import segment
	// this limit is true for the import segments in the peer endpoint.
	// This is a limit determined based on system resources, interconnect
	// type, cluster size etc.
	int	max_segments;

	// total amount of rsm segment space that this instance of bufferio
	// can use
	size_t	total_segment_size;

	// amount of unused rsm segment space that is available for this
	// instance of bufferio
	size_t	available_segment_size;

	// Maximum size of jumbo segments
	size_t	max_jumbo_seg_size;

	// Maximum number of segments in jumbo seggrp
	int	max_jumbo_segments;

	// Maximum number of segment groups
	uint32_t	max_seggrps;

	// Indicates that the bufferio is shutting down
	bool	shutdown;

	// Indicates that bufferio has been initialized
	bool	bio_enabled;
	// Pointer to the endpoint associated with this instance of biomgr
	rsmt_endpoint	*bio_rep;

	// The partition-avail segment that keeps track of the state of the
	// partitions
	// Buffers used for availseg
	void		*aligned_mem;
	size_t		aligned_size;
	// raw unaligned buffer and its size
	void		*raw_mem;
	size_t		raw_size;

	// pointer into the local partition avail segment where the
	// generation numbers for partitions begin
	void		*partition_info;
	// offset into the remote partition avail segment where the
	// generation numbers for partitions begin
	off_t		remote_partition_info;

	// local and remote partition-avail segment
	rsmt_export_segment	*pavailseg;
	rsmt_import_segment	*remote_pavailseg;

	// Number of sends that are currently in progress. When this
	// goes to zero a notification interrupt is sent to the receiver.
	uint32_t	sends_in_progress;

	// Number of sends that have been coalesced
	uint32_t	sends_accumulated;

	// Number of pending messages in each seggrp - indicates
	// which segment groups have new messages in them, used
	// in the notification interrupt message
	uint32_t	pending_msgs[RSMT_BIO_MAX_SEGGRPS];

	// Array of segment group pointers
	rsmt_bio_seggrp	**seggrps;
	// Pointer to the jumbo seggrp
	rsmt_bio_jumbo_seggrp *jumbo_seggrp;

	// for synchronization
	os::mutex_t	bmgr_lock;
	os::condvar_t	bmgr_cv;

	// # of jumbo messages sent
	static uint32_t jumbo_msgcnt;

	// Returns the most optimal segment group for a given message size
	static inline int get_seggrp(size_t);

#ifdef	DEBUG
	// Updates the message count in the bucket corresponding to msgsize
	void update_msgcnt(size_t);
#endif

	// Prevent assignment and pass by value
	rsmt_bio_mgr(const rsmt_bio_mgr &);
	rsmt_bio_mgr &operator = (const rsmt_bio_mgr &);

	// pacify lint
	rsmt_bio_mgr();
};

//
// Defer task that is responsible for processing flow controlled messages
//
class rsmt_bio_procmsg_defer_task : public defer_task {
public:
	rsmt_bio_procmsg_defer_task(rsmt_bio_mgr *, void *, ushort_t, int);
	void execute();

private:
	rsmt_bio_mgr	*bio_mgr;
	void		*msgp;
	ushort_t	msglen;
	int		msgid;

	// Prevent assignment and pass by value
	rsmt_bio_procmsg_defer_task(const rsmt_bio_procmsg_defer_task &);
	rsmt_bio_procmsg_defer_task &operator =
	    (const rsmt_bio_procmsg_defer_task &);

	// pacify lint
	rsmt_bio_procmsg_defer_task();
};

#include "rsmt_bio_in.h"

#endif	/* _RSMT_BIO_H */
