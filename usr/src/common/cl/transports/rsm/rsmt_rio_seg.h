/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RSMT_RIO_SEG_H
#define	_RSMT_RIO_SEG_H

#pragma ident	"@(#)rsmt_rio_seg.h	1.13	08/05/20 SMI"

//
// This file defines the replyio segment management related classes. Each
// endpoint has a rsmt_rio_seg_mgr object that manages the segments that
// are used for data transfers using the replyio over that endpoint. To
// avoid incurring the overhead of creating/exporting segments and connecting
// to exported segments during each replyio operation, the rsmt_rio_seg_mgr
// object caches used segments and makes them available to subsequent replyio
// operations.
//
// The (export) segments are cached at the replyio client end through
// rsmt_rio_seg_pool objects. Each rsmt_rio_seg_pool objects caches segments
// of a particular size. The number of pools and they sizes they cache are
// determined at the time the endpoint is created. Each rsmt_rio_seg_pool
// objects maintains a linked list of free export segments that are represented
// by rsmt_rio_client_seg objects.
//
// The (import) segments are cached directly by the rsmt_rio_seg_mgr object.
// at the server end. A hash table of rsmt_rio_server_seg objects keyed
// by segment id is maintained by the rsmt_rio_seg_mgr object. When the
// replyio client sends a replyio request to the replyio server, the server
// used the segment id embedded in the request to retrieve the corresponding
// preconnected import segment from the hash table.
//

#include <sys/list_def.h>
#include <sys/hashtable.h>
#include <transports/rsm/rsmt_cmf_client.h>
#include <transports/rsm/rsmt_seg_mgr.h>
#include <transports/rsm/rsmt_rio_trashmem.h>

// Forward declarations
class rsmt_rio_seg_mgr;
class rsmt_rio_seg_pool;
class rsmt_rio_seg_scanner_dt;
class rsmt_rio_cseg_dt;
class rsmt_rio_sseg_dt;

//
// The rsmt_rio_client_seg class is a wrapper around the rsmt_export_segment
// class. This class provides infrastructure to preallocate resources that
// are needed for releasing/destroying the segment.
//
class rsmt_rio_client_seg : public _SList::ListElem {
public:
	rsmt_rio_client_seg(rsmt_export_segment *, rsmt_rio_seg_pool *);
	~rsmt_rio_client_seg();

	//
	// Accessor methods.
	//
	rsmt_export_segment	*get_eseg();
	rsmt_rio_seg_pool	*get_seg_pool();

	//
	// Extracts the task object from the server segment object, i.e., this
	// once this method is called the ownership of the defer task object
	// is transferred to the caller.
	//
	rsmt_rio_cseg_dt	*extract_task();

	//
	// Rebind this segment to the provided memory. If this method is
	// is called with a NULL memory pointer, the segment is bound to
	// trash memory.
	//
	int rebind(void *, size_t);

	//
	// get the value of recently_used flag
	//
	bool get_recently_used();

	//
	// Set the recently_used flag
	//
	void set_recently_used(bool);

private:
	//
	// The export segment this object represents.
	//
	rsmt_export_segment	*eseg;

	//
	// The trash memory this segment is bound to in case it is on the
	// free list. The trash memory pointer will be NULL if the segment
	// is not on the free list.
	//
	rsmt_rio_trashmem	*trashmem;

	//
	// Pointer to the segment pool this segment belongs to.
	//
	rsmt_rio_seg_pool	*spool;

	//
	// A defer task to perform the destroy and delete operation on this
	// segment when it is needed. The defer task is preallocated in the
	// constructor so that no memory allocations are needed when time comes
	// to release the segment.
	//
	rsmt_rio_cseg_dt	*dtp;

	//
	// Flag indicates that this segment was recently used.
	//
	bool	recently_used;

	//
	// Prevent assignment and pass by value
	//
	rsmt_rio_client_seg(const rsmt_rio_client_seg &);
	rsmt_rio_client_seg &operator = (rsmt_rio_client_seg &);
};

//
// The rsmt_rio_server_seg class is a wrapper around the rsmt_import_segment
// class. This class provides infrastructure for preallocating resources that
// are needed for releasing/disconnecting this segment.
//
class rsmt_rio_server_seg {
public:
	rsmt_rio_server_seg(rsmt_import_segment *, rsmt_rio_seg_mgr *);
	~rsmt_rio_server_seg();

	//
	// Accessor methods.
	//
	rsmt_import_segment	*get_iseg();
	base_hash_table::hash_entry	*get_hent();
	rsmt_rio_seg_mgr	*get_rio_seg_mgr();

	//
	// Extracts the task object from the server segment object, i.e., this
	// once this method is called the ownership of the defer task object
	// is transferred to the caller.
	//
	rsmt_rio_sseg_dt	*extract_task();

	//
	// Put the object in the retired state. See below for definition.
	//
	void			retire();

	//
	// Is the object in the retired state?
	//
	bool			is_retired();

private:
	//
	// The import segment this object represents.
	//
	rsmt_import_segment	*iseg;

	//
	// The hash table entry is preallocated in the constructor so that when
	// the segment needs to be added to the hash table for caching during
	// release, no memory allocation is needed.
	//
	base_hash_table::hash_entry	*hent;

	// A pointer to the rio segment manager
	rsmt_rio_seg_mgr	*rio_smgrp;

	//
	// A defer task to perform the disconnect and delete operation on this
	// segment when it is needed. The defer task is preallocated in the
	// constructor so that no memory allocations are needed when time comes
	// to release the segment.
	//
	rsmt_rio_sseg_dt	*dtp;

	//
	// When an export segment is ready to be destroyed, the peer import
	// segment becomes retired. This is a hint to the replyio system, that
	// this import segment should not be cached any more, rather a
	// disconnect followed by a delete is needed.
	//
	bool			retired;

	//
	// Prevent assignment and pass by value
	//
	rsmt_rio_server_seg(const rsmt_rio_server_seg &);
	rsmt_rio_server_seg &operator = (rsmt_rio_server_seg &);
};

//
// The rsmt_rio_seg_pool class provides a facility to cache and retrieve
// segments of the same size efficiently. Segments of the same size are
// stored together so that the first element in the list can be dequeued
// and returned on an allocation request without any list search. An
// rsmt_rio_seg_mgr object maintains an array of rsmt_rio_seg_pool objects.
//
class rsmt_rio_seg_pool {
public:
	rsmt_rio_seg_pool();
	~rsmt_rio_seg_pool();

	//
	// Segment pools are created in an array. Thus they must be created
	// with a constructor that takes no arguments. The initialize method
	// is called on each segment pool to provide each one of them with
	// their own identity (i.e., the size of the segments they will be
	// dealing with).
	//
	void initialize(rsmt_rio_seg_mgr *, rsmt_seg_mgr *, size_t);

	//
	// Allocate a replyio client segment. This method is called during
	// the creation of the rsmt_rio_client object. Returns pointer to a
	// cached segment if one is found. If not, requests the segment
	// manager to create one and returns pointer to the freshly created
	// segment. Call can block waiting for resources to become available.
	//
	rsmt_rio_client_seg *alloc_client_seg(void *, size_t);

	//
	// Release a replyio server segment. This is called when a
	// rsmt_rio_client object is destructed to release a
	// rsmt_rio_client_seg. The segment will either be cached for later
	// use or will be destroyed. This call is nonblocking.
	//
	void	release_client_seg(rsmt_rio_client_seg *);

	//
	// This method is called when the endpoint is going down to destroy
	// all the cached segments.
	//
	void	cleanup();

	rsmt_rio_seg_mgr	*get_rio_seg_mgr();

	rsmt_seg_mgr	*get_seg_mgr();

	//
	// This method scans the free_list and clears the recently_used flag
	//
	void	mark_phase();

	//
	// This method scans the free_list and disposes segments whose
	// recently_used flag is not set
	//
	void	cleanup_phase();
private:
	//
	// This is a convenience routine that notifies the peer that an
	// export segment is going away and then destroys the segment.
	//
	void	dispose_client_seg(rsmt_rio_client_seg *);

	// Size of the segments this pool caches.
	size_t		allocsize;

	// A pointer to the per endpoint segment manager that does the real
	// creation and destruction of segments.
	rsmt_seg_mgr	*smgrp;

	// A pointer to the per endpoint replyio segment manager
	rsmt_rio_seg_mgr	*rio_smgrp;

	// The list of all free cached export segments
	IntrList<rsmt_rio_client_seg, _SList> free_list;

	//
	// Whether we are in the middle of doing cleanup processing. After this
	// flag has been set any calls to release a segment will not result in
	// that segment being cached. Instead they will be destroyed
	// immediately.
	//
	bool		cleaning;

	//
	// flag indicates if the client segs in this segpool can potentially be
	// cached or not.
	//
	bool		cacheable;

	//
	// count of number of times alloc_client_seg failed due to shortage
	// of segment space resource
	//
	uint32_t	resource_shortage;

	// Lock to protect critical internal data.
	os::mutex_t	pool_lock;

	//
	// Prevent assignment and pass by value
	//
	rsmt_rio_seg_pool(const rsmt_rio_seg_pool &);
	rsmt_rio_seg_pool &operator = (rsmt_rio_seg_pool &);
};

//
// The rsmt_rio_seg_mgr object manages replyio segments on behalf on an
// endpoint. each ennpoint object has an instance of rsmt_rio_seg_mgr. All
// replyio segment allocation and release requests go though this object.
//
class rsmt_rio_seg_mgr : public rsmt_cmf_client {
public:
	enum {
		// percent of total_seg_size, scanner thread starts
		// scanning the segpools when available_seg_size below
		// this value.
		SCANNER_START_THRESHOLD = 25
	};

	rsmt_rio_seg_mgr(rsmt_seg_mgr *, size_t, size_t, int, nodeid_t, int,
	    rsm_addr_t);
	~rsmt_rio_seg_mgr();

	//
	// Called from endpoint::initiate
	//
	void initiate_init();
	bool initiate_fini();
	void initiate_cleanup();

	//
	// Called from endpoint::unblock
	//
	void	unblock();

	//
	// Called from endpoint::cleanup
	//
	void	cleanup_init();
	void	cleanup_fini_pre_sm();
	void	cleanup_fini_post_sm();

	//
	// Called to allocate an export segment at the replyio client. The
	// two parameters are the destination memory address for the data
	// transfer and the size of the data transfer.
	//
	rsmt_rio_client_seg	*alloc_client_seg(void *, size_t);

	//
	// Called at the replyio server to get a handle on the corresponding
	// import segment, given the rsmt_seg_conn_token_t that identifies
	// the export segment at the peer.
	//
	rsmt_rio_server_seg	*claim_server_seg(rsmt_seg_conn_token_t *);

	// Release (and cache) an export segment
	void	release_client_seg(rsmt_rio_client_seg *);

	// Release (and cache) an import segment.
	void	release_server_seg(rsmt_rio_server_seg *);

	rsmt_seg_mgr	*get_seg_mgr();

	//
	// This export segment is going away. Ask the peer to release the
	// corresponding import segment. After this call is made, it is
	// guaranteed that the peer will not be sent a reference to this
	// segment again.
	//
	void	seek_peer_release(rsmt_rio_client_seg *);

	//
	// Verify if it makes sense to do replyio optimization for a data
	// transfer of the given size.
	//
	bool	valid_rio_size(size_t);

	//
	// Check if a new client seg of the requested size can be allocated
	// - returns true if it can be otherwise returns false. The
	// available_seg_size value is also decremented if true is returned.
	// This is used as means of resource management and a return value of
	// true does not necessary mean the actual creation of the client seg
	// will succeed.
	//
	bool	check_available_seg_size(size_t);

	//
	// A cacheable client segment of specified size has been freed, update
	// available_seg_size
	void	freed_seg_size(size_t);

	//
	// This routine is called by the rsmt_rio_seg_scanner_dt and is
	// responsible for executing the phase I and phase II of the
	// segment resource management
	//
	void	seg_scanner();

	//
	// cm_handle is inherited from rsmt_cmf_client.
	//
	bool cm_handle(void *, ushort_t, int, bool);

	//
	// RSM transport does not support replyio optimization for data
	// tranfers smaller than this.
	//
	static size_t RSMT_MIN_RIO_SIZE;

	//
	// RSM transport does not allocate a replyio segment smaller than this.
	//
	static size_t RSMT_MIN_RIO_SEG_SIZE;

	//
	// RSM transport does not cache replyio segment larger than this,
	// this essentially maps to the max size of the trashmem memory.
	//
	static size_t RSMT_MAX_RIO_CACHEABLE;

	//
	// Low watermark which defines when the segment scanner should
	// start disposing segments. Expressed as a percentage of the
	// total_seg_size
	//
	static size_t RSMT_SEG_SIZE_LOW_THRESHOLD;

	//
	// Counter that keeps track of rebind failures
	//
	static uint32_t	rebind_failures;

	//
	// Counter that keeps track replyio failing over to bufferio
	//
	static uint32_t	replyio_failures;

private:
	//
	// This is a convenience routine that performs disconnect and delete.
	//
	void	dispose_server_seg(rsmt_rio_server_seg *);

	//
	// Given the size of the data transfer, returns an index into the
	// segment pool array. The index points to the segment pool that
	// handles best fit segments for this size. The first segment pool
	// is responsible for segments of the minimum segment size. The
	// subsequent pools manage segments that are twice as large as
	// the segments in the immediately preceding pool.
	//
	uint_t	get_pool_index(size_t);


	//
	// phase I of the segment resource management is done by this
	// routine which is called from the rsmt_rio_seg_scanner_dt task.
	// This routine scans the segment pools and clears the recently_used
	// flag of all the client segments in the free_list.
	//
	void	mark_phase();

	//
	// phase II of the segment resource management is done by this routine
	// which is called from the rsmt_rio_seg_scanner_dt task. This routine
	// checks if the the available_seg_size has fallen below a low
	// watermark, if so it tries to dispose client segments that have not
	// been recently_used.
	//
	void	cleanup_phase();

	//
	// A pointer to the per endpoint segment manager that does the real
	// segment creation/destruction.
	//
	rsmt_seg_mgr	*smgrp;

	// Total number of segment pools in this replyio segment manager
	uint_t		max_pools;

	// An array of replyio segment pools.
	rsmt_rio_seg_pool *seg_pools;

	// Minimum segment size used by this replyio segment manager.
	size_t		min_seg_size;

	//
	// Maximum segment size possible at this replyio segment manager.
	// This limit is imposed by the underlying device.
	//
	size_t		max_seg_size;

	//
	// Total segment size that replyio can allocate and export in the
	// cacheable segment pools
	//
	size_t	total_seg_size;

	//
	// Segment size out of the rio_total_seg_size still available for
	// use by replyio segment pools
	//
	size_t	available_seg_size;

	//
	// A hash table of cached import segments. The table is indexed by
	// the segment id of the corresponding export segments. The free
	// hash caches the free segments and the inuse hash caches the inuse
	// segments. The need to cache the inuse segments arises because the
	// replyio client can sometime recycle segments quicker than the
	// replyio server can release them. Under these circumstances, a
	// claim operation must wait until the corresponding import segment
	// is released.
	//
	hashtable_t<rsmt_rio_server_seg *, rsm_memseg_id_t> sseg_free_hash;

	hashtable_t<rsmt_rio_server_seg *, rsm_memseg_id_t> sseg_inuse_hash;

	//
	// Number of threads waiting in a claim operation. Use to determine
	// if a cv broadcast needs to be done when a segment is released.
	//
	int	claim_waiters;

	//
	// Number of sends in progress. The control message mechanism cannot
	// be disabled if a send operation is active.
	//
	int	sends_in_progress;

	//
	// Whether we are in the middle of doing cleanup processing. After this
	// flag has been set any calls to release a segment will not result in
	// that segment being cached. Instead they will be destroyed
	// immediately.
	//
	bool		cleaning;

	//
	// Whether the scanner defer task is active or not. If this is true
	// during shutdown the scanner thread needs to wait for the scanner
	// thread to stop before proceeding
	//
	bool		scanner_active;

	//
	// When the available_seg_size falls below this threshold the scanner
	// thread is woken up so that it can implement the resource management.
	//
	size_t		scanner_start_threshold;

	//
	// defer task that scans through segpools and disposes not recently
	// used segments
	//
	rsmt_rio_seg_scanner_dt	*scannerdtp;

	//
	// Mutex and condition variable for synchronization.
	//
	os::mutex_t	sm_lock;
	os::condvar_t	sm_cv;

	//
	// Prevent assignment and pass by value
	//
	rsmt_rio_seg_mgr(const rsmt_rio_seg_mgr &);
	rsmt_rio_seg_mgr &operator = (rsmt_rio_seg_mgr &);

};

//
// The rsmt_rio_seg_scanner_dt object is used to scan the segment pools for
// dormant rsmt_rio_client_seg and free them as a means of resource management.
//
class rsmt_rio_seg_scanner_dt : public defer_task {
public:
	rsmt_rio_seg_scanner_dt(rsmt_rio_seg_mgr *);
	~rsmt_rio_seg_scanner_dt();

	void	execute();

private:
	rsmt_rio_seg_mgr *rio_segmgrp;

	//
	// Prevent assignment and pass by value
	//
	rsmt_rio_seg_scanner_dt(const rsmt_rio_seg_scanner_dt &);
	rsmt_rio_seg_scanner_dt &operator = (rsmt_rio_seg_scanner_dt &);
};

//
// The rsmt_rio_cseg_dt object is used to execute a dispose operation on
// a client segment. The need for a defer task arises as the dispose operation
// may need to send a control message to the peer.
//
class rsmt_rio_cseg_dt : public defer_task {
public:
	rsmt_rio_cseg_dt(rsmt_rio_client_seg *);
	~rsmt_rio_cseg_dt();

	void	execute();

private:
	rsmt_rio_client_seg	*cseg;

	//
	// Prevent assignment and pass by value
	//
	rsmt_rio_cseg_dt(const rsmt_rio_cseg_dt &);
	rsmt_rio_cseg_dt &operator = (rsmt_rio_cseg_dt &);
};

//
// The rsmt_rio_sseg_dt object is used to execute a disconnect operation on
// a server segment. The need for a defer task arises as the trigger for
// disconnect can arrive in the interrupt context.
//
class rsmt_rio_sseg_dt : public defer_task {
public:
	rsmt_rio_sseg_dt(rsmt_rio_server_seg *);
	~rsmt_rio_sseg_dt();

	void	execute();

private:
	rsmt_rio_server_seg	*sseg;

	//
	// Prevent assignment and pass by value
	//
	rsmt_rio_sseg_dt(const rsmt_rio_sseg_dt &);
	rsmt_rio_sseg_dt &operator = (rsmt_rio_sseg_dt &);
};

//
// The rsmt_rio_ctrl_msg_t structure defines the format used by the replyio
// subsystem to exchange control messages. Currently only one type of control
// message is used by the replyio subsystem - a message that the exporter
// sends to the importer to release a cached segment.
//
typedef struct {
	rsmt_cmhdr_t		cmhdr;
	rsmt_seg_conn_token_t	conntok;
} rsmt_rio_ctrl_msg_t;

#include <transports/rsm/rsmt_rio_seg_in.h>

#endif /* _RSMT_RIO_SEG_H */
