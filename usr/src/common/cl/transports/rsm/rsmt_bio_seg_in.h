/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_RSMT_BIO_SEG_IN_H
#define	_RSMT_BIO_SEG_IN_H

#pragma ident	"@(#)rsmt_bio_seg_in.h	1.5	08/05/20 SMI"

// Returns the offset in the segment of the directory entry
// corresponding to the partition index
inline off_t
rsmt_bio_seg::get_diroffset(int pindex)
{
	return ((off_t)pindex*(off_t)sizeof (rsmt_bio_dirent_t));
}

// Returns the total number of partitions in the bufferio segment
inline int
rsmt_bio_seg::get_numpartitions()
{
	return (num_partitions);
}

// Returns the number of partititions needed to fit in a message
// of size indicated by the argument
inline int
rsmt_bio_seg::get_numpartitions(size_t msglen)
{
	// round-up msglen and divide by partition_sz
	return ((int)((msglen + partition_sz - 1) / partition_sz));
}

// Returns the segment-index of the bufferio segment
inline int
rsmt_bio_seg::get_segindex()
{
	return (seg_index);
}

// Returns the size of the bufferio segment
inline size_t
rsmt_bio_seg::get_size()
{
	return (segsize);
}

// Returns the size of the partition
inline size_t
rsmt_bio_seg::get_partitionsz()
{
	return (partition_sz);
}

// partition inside the exported memory.
inline void *
rsmt_bio_rseg::partition(int pindex)
{
	// skip the directory and index into the partition
	return ((void *)((char *)membuf + dirsz +
	    (uint32_t)pindex*partition_sz));
}

// Returns the offset into the import segment corresponding to the
// partition-index.
inline off_t
rsmt_bio_sseg::partition(int pindex)
{
	// skip the directory and index into the partition
	return ((off_t)dirsz + (off_t)pindex*(off_t)partition_sz);
}

// Maximum number of recv segments this segment group can have
inline int
rsmt_bio_seggrp::get_rs_maxsegs()
{
	return (rs_maxsegs);
}

// Maximum number of partitions per recv segment of this segment group
inline int
rsmt_bio_seggrp::get_rs_maxpartitions()
{
	return (rs_maxpartitions);
}
// Maximum number of send segments this segment group can have
inline int
rsmt_bio_seggrp::get_ss_maxsegs()
{
	return (ss_maxsegs);
}

// Maximum number of partitions per send segment of this segment group
inline int
rsmt_bio_seggrp::get_ss_maxpartitions()
{
	return (ss_maxpartitions);
}

inline size_t
rsmt_bio_seggrp::get_partitionsz()
{
	return (partition_sz);
}

// Returns the number of partititions needed to fit in a message
// of size indicated by the argument
inline int
rsmt_bio_seggrp::get_numpartitions(size_t msglen)
{
	// round-up msglen and divide by partition_sz
	return ((int)((msglen + partition_sz - 1) / partition_sz));
}

#endif	/* _RSMT_BIO_SEG_IN_H */
