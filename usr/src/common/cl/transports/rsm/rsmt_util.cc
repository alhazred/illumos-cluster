//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rsmt_util.cc	1.27	08/05/20 SMI"

// Utility routines and miscellaneous startup code for
// the RSM transport.

#ifdef _KERNEL
#include <sys/kmem.h>
#else
#include <unistd.h>
#include <stdlib.h>
#endif

#include "rsmt_util.h"
#include "rsmt_transport.h"

#if defined(DBGBUFS) && !defined(NO_RSMT_DBGBUF)
dbg_print_buf rsmt_trans_dbg(16 * 1024, UNODE_DBGOUT_ALL);
dbg_print_buf rsmt_path_dbg(32 * 1024, UNODE_DBGOUT_ALL);
dbg_print_buf rsmt_seg_dbg(32 * 1024, UNODE_DBGOUT_ALL);
dbg_print_buf rsmt_msg_dbg(64 * 1024, UNODE_DBGOUT_ALL);

uint_t		rsmt_dbg_level	= RSMT_DBGL_DEFAULT;
uint64_t	rsmt_dbg_flags	= RSMT_DBGF_DEFAULT;
#endif

// Allocate memory that starts and ends on a chunk boundary of at
// least "size" size.  The return value is the chunk aligned address.
// Also returned are the size rounded up to chunk_size, and the
// actual start and size of the allocation so it can later be
// freed (kmem_free needs both).
//
void *
alloc_chunk_aligned(size_t size, size_t chunk_size, size_t *rounded_size,
    void **actual_start, size_t *actual_size,
    os::mem_alloc_type flag)
{
#ifdef _KERNEL

	size_t bufsize;
	void *memptr;
	uintptr_t base;
	int alloc_mode;

	// the rounding up logic works only if chunk_size is a power of 2
	// but rsmt always passes PAGESIZE so just make sure it is PAGESIZE
	ASSERT(chunk_size == (size_t)PAGESIZE);


	alloc_mode = (flag == os::NO_SLEEP) ? KM_NOSLEEP : KM_SLEEP;
	// round up size to chunk multiple
	//
	// bufsize = (size + chunk_size - 1) / chunk_size * chunk_size;
	bufsize = (size + chunk_size - 1) & ~(chunk_size - 1);
	// in case I can't do math
	ASSERT((bufsize >= size) && (bufsize % chunk_size == 0));
	*rounded_size = bufsize;

	// add additional space so we can round up starting address
	//
	bufsize += chunk_size - 1;
	memptr = kmem_alloc(bufsize, alloc_mode);
	*actual_start = memptr;

	if (!memptr) {
		ASSERT(flag == os::NO_SLEEP);
		*actual_size = 0;
		*rounded_size = 0;
		return (NULL);
	}

	*actual_size = bufsize;
	base = (uintptr_t)memptr;
	// round up
	// base = (base + chunk_size - 1) / chunk_size * chunk_size;
	base = (base + chunk_size - 1) & ~(chunk_size - 1);
	ASSERT(base % chunk_size == 0);
	memptr = (void *)base;

	return (memptr);

#else	// !_KERNEL

	size_t bufsize;
	void *memptr;

	// Verify that rsmunode didn't lie to us about the pagesize
	// we know it uses.
	//
	ASSERT((long)chunk_size == sysconf(_SC_PAGESIZE));

	// bufsize = (size + chunk_size - 1) / chunk_size * chunk_size;
	bufsize = (size + chunk_size - 1) & ~(chunk_size - 1);
	// in case I can't do math
	ASSERT((bufsize >= size) && (bufsize % chunk_size == 0));
	*rounded_size = bufsize;
	*actual_size = bufsize;
	memptr = memalign(chunk_size, bufsize);
	*actual_start = memptr;

	if (!memptr && (flag == os::NO_SLEEP)) {
		*actual_size = 0;
		*rounded_size = 0;
		return (NULL);
	}

	// If memalign fails and flag was os::SLEEP the following, can't do
	// much, just die (this is only unode after all)

	ASSERT(memptr != NULL);
	return (memptr);

#endif	// !_KERNEL
}

void
free_chunk_aligned(void *p,
#ifdef _KERNEL
    size_t size)
#else
    size_t)
#endif
{
#ifdef _KERNEL
	kmem_free(p, size);
#else
	free(p);
#endif
}

#ifndef _KERNEL		// unode only code

// Code for unode so we can set various ints during runtime.

// Name and address of the int variable.
//
struct var_info {
	const char *name;
	int *addr;
};

// Macro so we don't have to type the same thing twice. :-)
//
#define	VAR(v)	{ #v, &v }

// List ints here as extern.
//
extern int rsmt_die1;

// List of all the ints we want to be able to modify.
//
static var_info var_list[] = {
	VAR(rsmt_die1),
	{ NULL, NULL }
};

// To set a particular int run unode_load giving rsm as the shared_obj
// and "rsmt_set" as the func and then a list of name-value pairs.
//
extern "C" int
rsmt_set(int argc, char **argv)
{
	int i;
	var_info *vip;

	argc--;		// argv[0] is "rsmt_set"
	argv++;

	while (argc >= 2) {
		for (vip = var_list; vip->name != NULL; vip++) {
			if (os::strcmp(argv[0], vip->name) == 0) {
				i = os::atoi(argv[1]);
				*vip->addr = i;
				os::printf("%s set to %d\n", vip->name,
				    *vip->addr);
				break;
			}
		}
		// Couldn't find variable.
		//
		if (vip->name == NULL) {
			os::printf("variable \"%s\" not in list\n");
			return (1);
		}

		argc -= 2;
		argv += 2;
	}

	return (0);
}

#endif	// !_KERNEL

static rsmt_transport *rsmt_transp;

static void
rsmt_start_transport()
{
	rsmt_transp = new rsmt_transport;
	ASSERT(rsmt_transp != NULL);
	path_manager::the().add_client(path_manager::PM_TRANSPORT, rsmt_transp);
}

#if 0	// can't do this yet

static void
rsmt_stop_transport()
{
	RSMT_TRANS_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_TRANSPORT,
	    ("rsmt_stop_transport\n"));

	if (rsmt_transp != NULL) {
		path_manager::the().remove_client(path_manager::PM_TRANSPORT,
		    rsmt_transp);
		rsmt_transp = NULL;
	}
}

#endif

// Initialization code for kernel module and unode.

#ifdef _KERNEL

#include <sys/modctl.h>
#include <sys/cmn_err.h>
#include <cplplrt/cplplrt.h>	// for _cplpl_init/_cplpl_fini

static struct modlmisc modlmisc = {
	&mod_miscops,
	"RSM-based cluster transport",
};

static struct modlinkage modlinkage = {
	MODREV_1,				// ml_rev
	{ &modlmisc, NULL, NULL, NULL },	// ml_linkage[]
};

char _depends_on[] =
	"misc/cl_runtime misc/cl_load misc/cl_orb "
	"misc/cl_haci misc/cl_quorum misc/cl_comm "
	"misc/rsmops";

int
_init()
{
	int error;

	error = mod_install(&modlinkage);
	if (error) {
		cmn_err(CE_WARN, "cl_rsmtrans mod_install failed");
		return (error);
	}

	_cplpl_init();	// C++ initialization

	RSMT_TRANS_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_TRANSPORT,
	    ("cl_rsmtrans _init\n"));

	rsmt_start_transport();

	return (0);
}

int
_fini()
{
	RSMT_TRANS_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_TRANSPORT,
	    ("cl_rsmtrans _fini\n"));

	return (EBUSY);

#if 0	// can't do this yet

	int error;

	rsmt_stop_transport();

	_cplpl_fini();	// C++ cleanup

	error = mod_remove(&modlinkage);
	if (error) {
		cmn_err(CE_WARN, "can't unload cl_rsmtrans module");
		return (error);
	}

	return (0);
#endif
}

int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}

#else	// !_KERNEL

extern "C" int
unode_init()
{
	RSMT_TRANS_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_TRANSPORT,
	    ("rsmt unode_init\n"));

	rsmt_start_transport();

	return (0);
}

#endif	// !_KERNEL
