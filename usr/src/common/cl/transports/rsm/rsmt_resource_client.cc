//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rsmt_resource_client.cc	1.7	08/05/20 SMI"

#include "rsmt_resource_client.h"
#include "rsmt_resource.h"

// Resource client constructor
rsmt_resource_client::rsmt_resource_client(int local_adapter_id)
{
	resource_adapter_id = local_adapter_id;
}

// Resource client destructor
rsmt_resource_client::~rsmt_resource_client()
{
	resource_adapter_id = 0;
}

// virtual method
// implement the policy of how and which segments will be deallocated
void
rsmt_resource_client::resource_segfree(size_t)
{
}

// virtual method
// implement the policy of how much memory will be deallocated
void
rsmt_resource_client::resource_memfree(monitor::system_state_t)
{
}

// register the segment management function to the global resource manager
void
rsmt_resource_client::resource_segment_register()
{
	rsmt_resource::the().register_resource_segment(resource_adapter_id,
		this);
}

// unregister the segment management function from the global resource manager
void
rsmt_resource_client::resource_segment_unregister()
{
	rsmt_resource::the().unregister_resource_segment(resource_adapter_id,
		this);
}

// register the memory management function to the global resource manager
void
rsmt_resource_client::resource_memory_register()
{
	rsmt_resource::the().register_resource_memory(resource_adapter_id,
		this);
}

// unregister the memory management function from the global resource manager
void
rsmt_resource_client::resource_memory_unregister()
{
	rsmt_resource::the().unregister_resource_memory(resource_adapter_id,
		this);
}

// requesting segment resources from the global resource management
void
rsmt_resource_client::segment_request(size_t sz)
{
	rsmt_resource::the().segment_resource_request(resource_adapter_id,
		this, sz);
}
