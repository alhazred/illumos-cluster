/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RSMT_RIO_TRASHMEM_H
#define	_RSMT_RIO_TRASHMEM_H

#pragma ident	"@(#)rsmt_rio_trashmem.h	1.7	08/05/20 SMI"

//
// The RSM Transport maintains a trash memory server that manages trash
// memory that segments that are not currently in use are kept bound to.
// Connected segments not in use cannot be left unbound as they run the
// risk of losing their assigned DVMA address range. Once, they lose the
// assigned DVMA ddress range, they can not reused.
//
// Everytime a segment needs to be bound to trash memory, a query to the
// trash memory server is made. The server returns a pointer to the trash
// memory. Ideally the trash memory server should maintain just one memory
// area to which all not-in-use segments are bound too. However, as bigger
// and bigger segments get created, there may be multiple trash memory
// areas in existence at the same time temporarily.
//
// The trash memory area returned by the trash memory server is virtual
// memory. Ideally the entire trash memory should be backed up by one single
// physical page frame. Investigationn is ongoing regarding whether the
// solaris virtual memory subsystem has any such interface that we can use.
// In the meanwhile the trash memory server returns real kernel memory.
//

//
// The trash memory class. Represents a contiguous virtual memory area. Every
// segment that is bound to this trash memory keeps a hold on it. The hold
// is released when the segment is unbound from the trash memory.
//
class rsmt_rio_trashmem : public refcnt {
public:
	rsmt_rio_trashmem();
	~rsmt_rio_trashmem();

	// Assign virtual memory to the trash memory object
	bool	initialize(size_t, os::mem_alloc_type);

	void	*get_vaddr();
	size_t	get_vsize();

private:
	// Start aligned virtual address
	void	*vaddr;

	// Size of trash virtual memory
	size_t	vsize;

	// address and size of the raw memory allocated
	void	*raw_vaddr;
	size_t	raw_vsize;

	//
	// Prevent assignment and pass by value
	//
	rsmt_rio_trashmem(const rsmt_rio_trashmem &);
	rsmt_rio_trashmem &operator = (const rsmt_rio_trashmem &);
};

//
// The trash memory server class. An instance of this class is created
// when the RSM transport object is created. The trashmem server object
// keeps one trashmem object at all times. When it receives a request
// for an amount of trash memory that can not be served by the current
// trash memory object, it creates a new trash memory object. The hold
// on the old trash memory object is released so that it can be freed
// when all segments that are bound it get unbound and release their
// hold on it.
//
class rsmt_rio_trashmem_server {
public:
	rsmt_rio_trashmem_server();
	~rsmt_rio_trashmem_server();

	static rsmt_rio_trashmem_server&	the();

	// Create the trashmem server object
	static void	initialize();

	// Destroy the trashmem server object
	static void	shutdown();

	rsmt_rio_trashmem	*get_trashmem(size_t);
	void			rele_trashmem(rsmt_rio_trashmem *);

private:
	// The current trashmem object
	rsmt_rio_trashmem	*tmem;

	// Size of the current trash memory
	size_t			cur_tmem_size;

	// Lock to synchronize access to the trashmem object
	os::mutex_t		tmem_lock;

	static rsmt_rio_trashmem_server *the_tmem_server;

	//
	// Prevent assignment and pass by value
	//
	rsmt_rio_trashmem_server(const rsmt_rio_trashmem_server &);
	rsmt_rio_trashmem_server &operator = (const rsmt_rio_trashmem_server &);
};

#endif	/* _RSMT_RIO_TRASHMEM_H */
