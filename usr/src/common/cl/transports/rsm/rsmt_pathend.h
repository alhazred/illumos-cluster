/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RSMT_PATHEND_H
#define	_RSMT_PATHEND_H

#pragma ident	"@(#)rsmt_pathend.h	1.27	08/05/20 SMI"

#include <orb/transport/transport.h>

#include "rsmt_transport.h"
#include "rsmt_adapter.h"
#include "rsmt_util.h"
#include "rsmt_seg_mgr.h"

// The initial shared memory segment used for pathends to
// rendevous is a well-known segment id based on node id
// and adapter device instance.  In the segment id space
// reserved for the RSM transport we use a portion for
// this purpose.  We currently use 8 bits each for node id
// and instance to support node ids and instances up to 255.
//
#define	RSMT_NODE_BITS	8
#define	RSMT_INST_BITS	8

// For asserts to make sure we don't use a segment id reserved
// for an endpoint.
//
#define	RSMT_NODE_MAX	((1 << RSMT_NODE_BITS) - 1)
#define	RSMT_INST_MAX	((1 << RSMT_INST_BITS) - 1)

// Above the space used for pathend segments is the space
// available to be assigned (on demand) for endpoints.  In
// this header file since it is the pathend's use that
// determines what is free for the endpoints.
//
#define	RSMT_EP_SEG_BASE	(RSM_CLUSTER_TRANSPORT_ID_BASE + \
	(1 << (RSMT_NODE_BITS + RSMT_INST_BITS)))

//
// sizeof this struct should be <= RSMT_CACHELINE
//
typedef struct {
	char		buf[PM_MESSAGE_SIZE];
	char		chksum[PM_MESSAGE_SIZE];
} rsmt_pad_hb_t;
//
// Heartbeat buffer, the hearbeat buffer is cacheline sized buffer so that
// puts can happen atomically.
//
typedef union {
	uint64_t	foralignment;
	char		filler[RSMT_CACHELINE];
	rsmt_pad_hb_t	buf_chksum;
	char		buf[PM_MESSAGE_SIZE];
} rsmt_pathend_hbbuf_t;

//
// A set of flags that are set in the exported rsmt_pathend_data segment
// at various points of time  during the node pair version negotiation
// phase. This must be a byte to guarantee atomicity between remote reads
// and writes.
//
// RSMT_PE_DATA_VALID	: The exported data segment has valid data. If this
//			  flag is not set then the only valid data is the
//			  header version range this node can support.
// RSMT_NP_UNKNOWN	: The exporting node has not yet determined if the
//			  node pair versions published by the peer are
//			  compatible with the local versions.
// RSMT_NP_INCOMPATIBLE	: The node pair versions are incompatible from this
//			  node's point of view.
// RSMT_NP_COMPATIBLE	: The node pair versions are compatible.
//
#define	RSMT_PE_DATA_VALID	((uint8_t)0x01)
#define	RSMT_NP_UNKNOWN		((uint8_t)0x10)
#define	RSMT_NP_INCOMPATIBLE	((uint8_t)0x20)
#define	RSMT_NP_COMPATIBLE	((uint8_t)0x40)

//
// The rsmt_pathend_data structure below is assigned a version. The follwoing
// numbers define the range of this structures version that the current
// software understands.
//
#define	RSMT_PE_DATA_MIN_VER	((uint8_t)1)
#define	RSMT_PE_DATA_MAX_VER	((uint8_t)1)

//
// The pathend uses an exported segment to share data with its peer. This
// segment is used for exchanging node pair version information during the
// path establishment phase. An area within the same segment is also used
// for exchanging heartbeats once the path is up and running. The structure
// rsmt_pathend_data defined below serves as the header of the data in
// this exported segment. The segment is organized as follows:
//
// ------------------------------
// |	rsmt_pathend_data	|
// |				|
// ------------------------------
// |	pad to ensure that the	|
// |	next item is aligned at	|
// |	RSMT_CACHELINE size.	|
// ------------------------------
// |				|
// |	heartbeat buffer	|
// |				|
// ------------------------------
// |				|
// |	node pair protocol	|
// |	version data		|
// ------------------------------
//
// The first four fields of the following data structure must stay
// invariant across all releases even if the rsmt_pathend_data structure
// is upgraded to a new version.
//
typedef struct rsmt_pathend_data {
	uint8_t		minver;		// Minimum and maximum
	uint8_t		maxver;		// rsmt_pathend_data versions known
					// to the local node.
	uint8_t		hdrver;		// The rsmt_pathend_data version
					// negotiated between the two nodes.
	uint8_t		flags;		// Flags, valid etc - see above.
	uint32_t	ndid;		// Nodeid of the exporting node
	uint32_t	incn;		// Incarnation number of the exporter
	uint32_t	path_incn;	// Path incarnation number of the
					// exporter - a path gets a new
					// incarnation number everytime it
					// initiates.
	uint32_t	path_incn_echo;	// Path incarnation number of the
					// peer echoed back.
	uint32_t	hb_offset;	// Offset where heartbeat buffer
					// starts in this segment.
	uint32_t	hb_len;		// Length of heartbeat buffer.
	uint32_t	version_offset;	// Offset where node pair version
					// data appears in this segment.
	uint32_t	version_len;	// Length of version data.
} rsmt_pathend_data_t;

class rsmt_pathend : public pathend {
public:
	friend class rsmt_endpoint;
	rsmt_pathend(rsmt_transport *, clconf_path_t *);
	~rsmt_pathend();

	// inherited from pathend that must be overridden

	void initiate(char *&send_buf_ref, char *&rec_buf_ref,
	    incarnation_num &incn);

	//
	// Since there are no atomicity guarantees, to ensure that
	// heartbeat passed to generic transport is valid and complete
	// pm_get_internal does preprocessing using a checksum on the
	// buffer which has been updated by the remote node via RSM_PUT.
	//
	void pm_get_internal();
	void pm_send_internal();
	void cleanup();
	void delete_notify();
	endpoint *new_endpoint(transport *tr);

	// end inherited

	void	rpe_create_exported_segment(size_t);
	int	rpe_import_segment();
	void	rpe_disconnect_segment();
	int	rpe_get_remote_data(char *, off_t, size_t);
	uint8_t	rpe_negotiate_pe_data_version();
	void	rpe_populate_local_data(uint8_t, size_t, char *, size_t);

	// public data

	nodeid_t	rpe_remote_nodeid;
	incarnation_num	rpe_remote_incn;
	rsmt_adapter	*rpe_adap;

	rsmt_pathend_data	*rpe_rpd;

	rsm_addr_t	rpe_remote_adapter_id;

private:
	rsmt_transport	*rpe_rtr;
	nodeid_t	rpe_local_nodeid;
	uint_t		rpe_local_adapter_instance;
	uint_t		rpe_remote_adapter_instance;

	//
	// A path gets a new incarnation number everytime it is constructed.
	// This is used to make sure that the pathend at one end does not
	// consider a handshake complete based on stale remote data.
	//
	incarnation_num	rpe_local_path_incn;
	incarnation_num	rpe_remote_path_incn;

	bool		rpe_have_seg;
	size_t		rpe_rpd_rounded_size;
	void		*rpe_rpd_alloc;
	size_t		rpe_rpd_alloc_size;

	rsm_memseg_export_handle_t	rpe_ememseg;
	rsm_memseg_import_handle_t	rpe_imemseg;
	rsm_memory_local_t		rpe_memory;

	bool		rpe_wait_for_del;
	os::mutex_t	rpe_mtx;
	os::condvar_t	rpe_cv;

	rsmt_pathend_hbbuf_t	rpe_send_buf;
#define	local_send_buf rpe_send_buf.buf_chksum.buf
	// rpe_recv_buf has just the heartbeat token not the begin/end patterns
	rsmt_pathend_hbbuf_t	rpe_recv_buf;
#define	local_recv_buf rpe_recv_buf.buf

	bool		rpe_barriers_for_hb;

	void		rpe_open_barrier(rsm_barrier_t *);
	int		rpe_close_barrier(rsm_barrier_t *);

	//
	// Offset into the pathend segment exported by the peer where
	// this node is expected to deposit the heartbeat tokens.
	//
	off_t		remote_hb_offset;

	rsmt_pathend();		// lint wants this
};

#endif /* _RSMT_PATHEND_H */
