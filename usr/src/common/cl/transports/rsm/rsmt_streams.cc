//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rsmt_streams.cc	1.29	08/05/20 SMI"

#include <orb/msg/message_mgr.h>

#include "rsmt_util.h"
#include "rsmt_streams.h"

//
// constructor
//
rsmt_sendstream::rsmt_MarshalStream::rsmt_MarshalStream(Environment *e,
    uint_t byte_size, uint_t chunk_size) :
	MarshalStream(e, byte_size, chunk_size)
{
}

//
// newbuf - rsmt streams create buffers whose buffers are multiple of
// RSMT_CACHELINE
//
Buf *
rsmt_sendstream::rsmt_MarshalStream::newbuf(uint_t nbytes)
{
	uint32_t length;
	// roundup nbytes to be a multiple of RSMT_CACHELINE
	length = (uint32_t)ROUNDUP_CACHELINE(nbytes);

	if (get_env()->is_nonblocking()) {
		uchar_t *datap = GWBuf::alloc_buffer(length, os::NO_SLEEP);
		if (datap == NULL) {
			get_env()->system_exception(CORBA::WOULDBLOCK(length,
			    CORBA::COMPLETED_NO));
			return (NULL);
		}
		/*CSTYLED*/
		Buf *b = new (os::NO_SLEEP) nil_Buf(datap, length, 0,
		    Buf::HEAP, Buf::HEAP);

		if (b == NULL) {
			delete [] datap;
			get_env()->system_exception(CORBA::WOULDBLOCK(
				(uint_t)sizeof (nil_Buf), CORBA::COMPLETED_NO));
		}
		return (b);
	}

	return (new GWBuf(length));

}

rsmt_sendstream::rsmt_sendstream(resources *res, nodeid_t rem_nodeid,
    incarnation_num rem_incn, rsmt_endpoint *ep, bool nonblocking) :
	sendstream(res, &rss_MainBuf),
	rss_MainBuf(res->get_env(), res->send_buffer_size(),
	rsmt_bio_mgr::get_chunksize(res->send_buffer_size())),
	rss_ID_node(rem_nodeid, rem_incn)
{
	uint_t actual;

	RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_SENDSTREAM,
	    ("rsmt_sendstream::rsmt_sendstream() %p header=%u buf=%u\n",
	    this, res->send_header_size(), res->send_buffer_size()));

	rss_rep = ep;
	rss_rep->hold();	// Released in destructor

	actual = rss_MainBuf.alloc_chunk(res->send_header_size());
	ASSERT(actual == res->send_header_size());

	rss_nonblocking = nonblocking;
}

rsmt_sendstream::~rsmt_sendstream()
{
	RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_SENDSTREAM,
	    ("rsmt_sendstream::~rsmt_sendstream() %p\n", this));

	rss_MainBuf.dispose();

	rss_rep->rele();	// Matches hold in constructor
	rss_rep = NULL;		// lint wants to see this
}

ID_node &
rsmt_sendstream::get_dest_node()
{
	return (rss_ID_node);
}

void
rsmt_sendstream::send(bool need_seq, orb_seq_t &seq)
{
	rsmt_hdr_t msg;
	Environment *e = get_env();

	RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_SENDSTREAM,
	    ("rsmt_sendstream::send() %p %s\n", this,
	    rss_nonblocking ? "nonblocking" : "blocking"));

	// If the endpoint has become unregistered don't do anything.
	//
	if (rss_rep->rep_endpoint_state() != endpoint::E_REGISTERED) {
		e->system_exception(CORBA::RETRY_NEEDED(ENOENT,
		    CORBA::COMPLETED_NO));
		return;
	}

	// get pointers to data and message info (in shared
	// remote memory segment)
	//
	// set message parameters
	//
	msg.rm_main_len = rss_MainBuf.span();
	msg.rm_xdoor_len = XdoorTab.span();
	msg.rm_xdoorcount = xdoorcount;
	msg.rm_offline_data = !OffLineBufs.empty();
	msg.rm_offline_size = OffLineBufs.span();
	RSMT_MSG_DBG(RSMT_DBGL_VERBOSE, RSMT_DBGF_SENDSTREAM,
	    ("send %p: main_len = %u, xdoor_len = %u, xdoorcount = %u, "
	    "offline_data = %u, offline_size = %u\n", this,
	    msg.rm_main_len, msg.rm_xdoor_len, msg.rm_xdoorcount,
	    msg.rm_offline_data, msg.rm_offline_size));
	msg.rm_msgtype = get_msgtype();
	msg.rm_src = orb_conf::current_id_node();

	// Make a copy of the XdoorTab
	Buf *saveXdoorTab = 0;
	if (msg.rm_xdoorcount > 0) {
		//
		// Call newbuf on existing rsm marshalstream. Use this
		// method so that it takes care of blocking and
		// non-blocking allocations.  The other alternative is
		// to repeat code from newbuf.
		//
		saveXdoorTab = rss_MainBuf.newbuf(msg.rm_xdoor_len);
		if (!saveXdoorTab) {
			ASSERT(e->exception());
			return;
		}
		XdoorTab.region().copy_contents((char *)saveXdoorTab->head(),
		    msg.rm_xdoor_len, e);
		ASSERT(!e->exception());
		// advance tail ptr to point to end of copied data
		saveXdoorTab->incspan((int)msg.rm_xdoor_len);
	}

	//
	// Get message data in a contiguous buffer for transmission.
	//
	rss_MainBuf.useregion(XdoorTab.region(), MarshalStream::CURSOR_ATEND);
	rss_MainBuf.useregion(OffLineBufs, MarshalStream::CURSOR_ATEND);
	//
	// We would not be here if there was an exception. We can
	// reuse the environment variable in the sendstream.
	//
	ASSERT(!e->exception());
	Buf *sb = rss_MainBuf.coalesce_region(e).reap();

	if (e->exception() != NULL) {
		RSMT_MSG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_SENDSTREAM,
		    ("coalesce_region returned exception. major=%d\n",
		    e->exception()->_major()));
		// Set completion status to be COMPLETED_NO explicitly
		e->sys_exception()->completed(CORBA::COMPLETED_NO);
		goto done;
	}
	msg.rm_total_len = sb->span();

	//
	// Get a sequence number from the ORB, if needed. The sequence
	// number is not used by the transport; ORB uses it for duplicate
	// detection. This really should be stuffed in by the ORB before
	// the call to sendstream::send. The apparent motivation for the
	// way things are is to make the duration a message hangs on to
	// a sequence number as short as possible since the ORB uses a
	// fairly small sequence number window for efficiency reasons.
	//
	//
	if (need_seq && !message_mgr::the().send_prepare(get_dest_node(),
	    seq)) {
		RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_SENDSTREAM,
		    ("rsmt_sendstream::send %p send_prepare problem\n",
		    this));
		e->system_exception(CORBA::COMM_FAILURE(0,
		    CORBA::COMPLETED_NO));
		goto done;
	}
	msg.rm_seq = seq;

#ifdef	DEBUG
	msg.rm_cksum = rsmt_padded_hdr::checksum32((void *)sb->head(),
	    (size_t)sb->span());
	RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_SENDSTREAM,
	    ("send %p: cksum=%u\n", this, msg.rm_cksum));
#endif
	RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_SENDSTREAM,
	    ("send %p: total_len = %u, span=%u, len=%u\n",
	    this, msg.rm_total_len, sb->span(), sb->length()));

	if (!rss_rep->get_bio_mgrp()->send(this, &msg, sb)) {
		// send failed - exception has been set in the env
		RSMT_MSG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_SENDSTREAM,
		    ("send %p: seq = %u failed\n", this, seq));
	}
	//
	// The caller expects the XdoorTab to be unmodified in case
	// error recovery is needed and it asserts so.  We are done
	// sending the message so stick the saved XdoorTab buffer back
	// into the sendstream if we created it. If coaslesce_region
	// returned exception, then sb == NULL so no need to call
	// done() on it.
	//
done:
	if (msg.rm_xdoorcount > 0) {
		ASSERT(XdoorTab.empty());
		XdoorTab.usebuf(saveXdoorTab);
		ASSERT(XdoorTab.span() == msg.rm_xdoor_len);
	}
	if (sb) {
		// sb == NULL if coalesce_region returned exception.
		sb->done();
	}
}

bool
rsmt_sendstream::get_nonblocking()
{
	return (rss_nonblocking);
}

// ----------------

rsmt_recstream::rsmt_recstream(rsmt_hdr_t *msg, Buf *rb, rsmt_endpoint *rep) :
	recstream(msg->rm_msgtype, invocation_mode::ONEWAY, rep)
{
	RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_RECSTREAM,
	    ("rsmt_recstream::rsmt_recstream() %p\n", this));

	rrs_src_node = msg->rm_src;
	rrs_bp = rb;
	rrs_main_len = msg->rm_main_len;
	rrs_xdoor_len = msg->rm_xdoor_len;
	rrs_total_len = msg->rm_total_len;
	rrs_offline_data = msg->rm_offline_data;
	rrs_msgtype = msg->rm_msgtype;
	rrs_seq = msg->rm_seq;

	RSMT_MSG_DBG(RSMT_DBGL_VERBOSE, RSMT_DBGF_RECSTREAM,
	    ("rsmt_recstream %p (seq %u): main_len = %u, xdoor_len = %u, "
	    "offline_data = %u, total_len = %u, offline_size = %u\n",
	    this, rrs_seq, rrs_main_len, rrs_xdoor_len, rrs_offline_data,
	    rrs_total_len, msg->rm_offline_size));
#ifdef	DEBUG
	RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_RECSTREAM,
	    ("rsmt_recstream %p (seq %u): rm_cksum = %u, cksum = %u\n",
	    this, rrs_seq, msg->rm_cksum, rsmt_padded_hdr::checksum32(
	    (void *)rb->head(), (size_t)rb->span())));
	// check for message corruption
	if (msg->rm_cksum != rsmt_padded_hdr::checksum32((void *)rb->head(),
	    (size_t)rb->span())) {
		RSMT_MSG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_RECSTREAM,
		    ("rsmt_recstream %p (seq %u): ERR CHECKSUM ERROR\n",
		    this, rrs_seq));
	}
	ASSERT(msg->rm_cksum == rsmt_padded_hdr::checksum32((void *)rb->head(),
	    (size_t)rb->span()));
#endif

	xdoorcount = msg->rm_xdoorcount;
}

rsmt_recstream::~rsmt_recstream()
{
	RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_RECSTREAM,
	    ("rsmt_recstream::~rsmt_recstream() %p\n", this));

	// rrs_bp gets appended to MainBuf and recstream->done() deletes it
	rrs_bp = NULL;		// lint wants to see this
}

bool
rsmt_recstream::initialize(os::mem_alloc_type)
{
	RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_RECSTREAM,
	    ("rsmt_recstream::initialize() %p \n", this));

	RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_RECSTREAM,
	    ("initialize: main_len = %u, xdoor_len = %u, total_len = %u\n",
	    rrs_main_len, rrs_xdoor_len, rrs_total_len));

	MainBuf.usebuf(rrs_bp);

	if (xdoorcount > 0) {
		XdoorTab.usebuf(new nil_Buf(rrs_bp->head() + rrs_main_len,
		    rrs_xdoor_len, rrs_xdoor_len));
	}

	if (rrs_offline_data) {
		uint_t offlen = rrs_total_len - (rrs_main_len + rrs_xdoor_len);
		RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_RECSTREAM,
		    ("initialize: offlen = %u\n", offlen));
		OffLineBufs.prepend(new nil_Buf(rrs_bp->head() +
		    rrs_main_len + rrs_xdoor_len, offlen, offlen));
	}

	RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_RECSTREAM,
	    ("MainBuf.span() = %u\n", MainBuf.span()));
	RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_RECSTREAM,
	    ("XdoorTab.span() = %u\n", XdoorTab.span()));
	RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_RECSTREAM,
	    ("OffLineBufs.span() = %u\n", OffLineBufs.span()));

	return (true);
}

ID_node &
rsmt_recstream::get_src_node()
{
	return (rrs_src_node);
}
