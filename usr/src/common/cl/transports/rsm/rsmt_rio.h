/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RSMT_RIO_H
#define	_RSMT_RIO_H

#pragma ident	"@(#)rsmt_rio.h	1.7	08/05/20 SMI"

//
// This file defines the rsmt_rio_client and rsmt_rio_server classes
// that are RSM transport specific derivatives of the base replyio support
// classes reply_client_t and reply_server_t respectively.
//

#include <sys/refcnt.h>
#include <transports/rsm/rsmt_segment.h>
#include <transports/rsm/rsmt_streams.h>

//
// The rsmt_rio_info_t structure encapsulates the rio transfer information
// that a replyio client sends to the replyio server.
//
typedef struct {
	// A token identifying the exported segment
	rsmt_seg_conn_token_t	conn_token;

	// Offset into the segment where data should go
	off_t	offset;

	// Size of the transfer
	size_t	size;
} rsmt_rio_info_t;

//
// rsmt_rio_client
//
// The RSM transport specific replyio_client_t
//
class rsmt_rio_client : public refcnt, public replyio_client_t {
public:
	rsmt_rio_client(struct buf *, rsmt_endpoint *);
	~rsmt_rio_client();

	// Inherited from replyio_client_t
	void release();

	//
	// marshal_rio_info is called to marshal RSM transport specific RIO
	// transfer information into the RIO request message during the call
	// to add_reply_buffer.
	//
	void marshal_rio_info(MarshalStream&);

	rsmt_rio_client_seg	*get_cseg();

private:
	// The endpoint on which the infrastructure for this rio has been
	// setup on.
	rsmt_endpoint		*endp;

	// The client side segment that will be used to receive the rio data.
	rsmt_rio_client_seg	*cseg;

	//
	// RSM transport needs to keep the IO area mapped at all times while
	// the segment stays exported. The base class replyio_client_t
	// maintains the buf pointer but does not guarantee that the IO area
	// will be mapped at all times as the other transports don't need it
	// to be. Thus we clone the buf structure and map it for our own use.
	//
	struct buf		*bufp;

	//
	// Prevent assignment and pass by value
	//
	rsmt_rio_client(const rsmt_rio_client &);
	rsmt_rio_client &operator = (rsmt_rio_client &);
};

//
// rsmt_rio_server
//
// The RSM transport specific replyio_server_t
//
class rsmt_rio_server : public replyio_server_t {
public:
	rsmt_rio_server(MarshalStream&, rsmt_endpoint *);
	~rsmt_rio_server();

	// Inherited from replyio_server_t
	send_reply_buf_result_t send_reply_buf(Buf *, sendstream *);

	//
	// Used to unmarshal the RSM transport specific RIO transfer
	// information from the RIO request message sent by the client.
	// The cancel method is used to extract and discard this information
	// when a decision has been made to drop the RIO operation.
	//
	void unmarshal_rio_info(MarshalStream&);
	static void unmarshal_rio_info_cancel(MarshalStream&);

	rsmt_rio_server_seg	*get_sseg();

private:
	// The endpoint on which the infrastructure for this rio has been
	// setup on.
	rsmt_endpoint		*endp;

	// The server side segment that will be used to transfer the rio data.
	rsmt_rio_server_seg	*sseg;

	// Offset into the segment where the data needs to be written.
	off_t			offset;

	// Size of the rio transfer.
	size_t			riosize;

	//
	// Prevent assignment and pass by value
	//
	rsmt_rio_server(const rsmt_rio_server &);
	rsmt_rio_server &operator = (rsmt_rio_server &);
};

#endif /* _RSMT_RIO_H */
