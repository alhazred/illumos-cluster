/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RSMT_STREAMS_H
#define	_RSMT_STREAMS_H

#pragma ident	"@(#)rsmt_streams.h	1.13	08/05/20 SMI"

#include <orb/buffers/marshalstream.h>

#include "rsmt_endpoint.h"

class rsmt_sendstream : public sendstream {
public:
	//
	// rsmt_MarshalStream - differs from its parent class in that it
	// allocates buffers whose size is a multiple of RSMT_CACHELINE.
	//
	class rsmt_MarshalStream : public MarshalStream {

	public:
		// constructor specifies both first Buf size and chunksize
		rsmt_MarshalStream(Environment *e, uint_t byte_size,
		    uint_t chunk_size);

		//
		// Function to allocate a new buffer.
		//
		Buf *newbuf(uint_t nbytes);
	private:
		// Pacify lint
		rsmt_MarshalStream();
	};

	rsmt_sendstream(resources *, nodeid_t rem_nodeid,
	    incarnation_num rem_incn, rsmt_endpoint *ep, bool nonblocking);
	~rsmt_sendstream();

	// inherited from sendstream that must be overridden

	ID_node &get_dest_node();
	void send(bool need_seq, orb_seq_t &);

	bool check_reply_buf_support(size_t, iotype_t, void *, optype_t);
	replyio_client_t *add_reply_buffer(struct buf *, align_t);
	// end inherited

	bool get_nonblocking();

	// lint wants to see these as public even though there are
	// accessors, but that makes sense as the accessors are
	// returning references to the objects, not copies, so
	// the caller of an accessor can mess with the data just as
	// if the data were public
	//
	rsmt_MarshalStream rss_MainBuf;
	ID_node rss_ID_node;

private:
	rsmt_endpoint	*rss_rep;
	bool		rss_nonblocking;

	rsmt_sendstream();	// lint wants this
};

class rsmt_recstream : public recstream {
public:
	rsmt_recstream(rsmt_hdr_t *, Buf *, rsmt_endpoint *);
	~rsmt_recstream();

	// inherited from recstream that must be overridden

	ID_node &get_src_node();
	bool initialize(os::mem_alloc_type);

	replyio_server_t *get_reply_buffer();
	void get_reply_buffer_cancel();
	// end inherited

	// lint wants to see these as public even though there are
	// accessors, but that makes sense as the accessors are
	// returning references to the objects, not copies, so
	// the caller of an accessor can mess with the data just as
	// if the data were public
	//
	ID_node rrs_src_node;

private:
	Buf *rrs_bp;

	uint_t rrs_main_len;
	uint_t rrs_xdoor_len;
	uint_t rrs_total_len;
	bool rrs_offline_data;
	orb_msgtype rrs_msgtype;
	orb_seq_t rrs_seq;

	rsmt_recstream();	// lint wants this
};

#endif	/* _RSMT_STREAMS_H */
