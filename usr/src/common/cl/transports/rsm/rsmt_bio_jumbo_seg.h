/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RSMT_BIO_JUMBO_SEG_H
#define	_RSMT_BIO_JUMBO_SEG_H

#pragma ident	"@(#)rsmt_bio_jumbo_seg.h	1.7	08/05/20 SMI"

#include <sys/rsm/rsm_common.h>

#include "rsmt_seg_mgr.h"
#include "rsmt_msg.h"

// forward declarations
class rsmt_bio_jumbo_seggrp;
class rsmt_bio_jumbo_buf;

//
// Typically bufferio is used for transferring small messages, but there
// are cases eg. during boot time, when replio fails on a certain path
// due to resources shortage or if the path goes down when bufferio needs
// to be able to transfer messages that are larger than what can be supported
// using partitioned segments (> 2*largest-partition-size).
//
// This file defines classes that are used for transferring large messages.
// The rsmt_bio_mgr creates a special segment group called the jumbo_seggrp
// which is a collection of jumbo receive (export) and send (import) segments.
// The large message is divided into smaller chunks depending on the size
// of send/receive segment being used for the message and the following
// protocol is used:
//
//	Sender							Receiver
//  RSM_PUT(first_chunk)
//			-----------RSMT_BIO_MSGCHUNK------>
//							Copy chunk into Buf
//			<----------RSMT_BIO_MSGCHUNKACK----
//  RSM_PUT(next_chunk)
//			-----------RSMT_BIO_MSGCHUNK------>
//							Copy chunk into Buf
//			<----------RSMT_BIO_MSGCHUNKACK----
//  ......
//  repeat this till the whole message is transferred.
//


//
// This class defines a generic jumbo segment and is subclassed to derive
// the receive and send segments. Jumbo segments are not partitioned.
//
class rsmt_bio_jumbo_seg : public refcnt {
public:
	enum { // bufferio segment state
		RSMT_BIO_JSEGNEW = 0, // new segment
		RSMT_BIO_JSEGINIT, // initialized segment
		RSMT_BIO_JSEGDEL // segment needs to be cleaned up
	};
	// Constructor
	//	seg_index - the segment index, a unique number associate with
	//		every segment in the bufferio component.
	//	seg_size - the size of the segment
	//	seggrp - pointer to the segment group to which this segment
	//		belongs to.
	//	biomgrp - pointer to the bufferio manager
	rsmt_bio_jumbo_seg(int, size_t, rsmt_bio_jumbo_seggrp *,
	    rsmt_bio_mgr *);

	// Destructor
	~rsmt_bio_jumbo_seg();
protected:
	// protects the bufferio segment fields
	os::mutex_t	js_lock;
	os::condvar_t	js_cv;

	// state of the segment
	int	state;
	// segment index
	int	segindex;
	// segment size
	size_t	segsize;
	// pointer to the jumbo segment group
	rsmt_bio_jumbo_seggrp *seggrp;
	// pointer to the segment manager
	rsmt_seg_mgr *segmgr;
	// pointer to the bio manager
	rsmt_bio_mgr *biomgr;
private:
	// Prevent assignments and pass by value
	rsmt_bio_jumbo_seg(const rsmt_bio_jumbo_seg&);
	rsmt_bio_jumbo_seg &operator = (const rsmt_bio_jumbo_seg&);

	// Pacify lint
	rsmt_bio_jumbo_seg();
};

//
// rsmt_bio_jumbo_rseg defines the jumbo receive segment, it is implemented
// using rsm export segments.
//
class rsmt_bio_jumbo_rseg : public rsmt_bio_jumbo_seg {
public:
	// Constructor
	//	seg_index - the segment index, a unique number associate with
	//		every segment in the bufferio component.
	//	seg_size - the size of the segment
	//	seggrp - pointer to the segment group to which this segment
	//		belongs to.
	//	biomgrp - pointer to the bufferio manager
	rsmt_bio_jumbo_rseg(int, size_t, rsmt_bio_jumbo_seggrp *,
	    rsmt_bio_mgr *);

	// Destructor
	~rsmt_bio_jumbo_rseg();

	// create the export segment, returns the connection token
	bool initiate(rsmt_seg_conn_token_t *);
	// destroy the export segment
	void cleanup();

	// copies msg from the export segment buffer to Buf and pushes it
	// the ORB when the message is completely received.
	void push(off_t, size_t, bool);
private:
	// a pointer to the page-aligned memory which is exported
	// through the rsm segment
	void	*membuf;
	// pointer/length of the actual buffer that is allocated
	void	*raw_mem;
	size_t	raw_size;
	// header of the message
	rsmt_hdr_t	msghdr;
	// Buffer where the message gets copied before being handed to ORB
	rsmt_bio_jumbo_buf *msgbufp;
	// export segment object
	rsmt_export_segment *exseg;

	// Prevent assignments and pass by value
	rsmt_bio_jumbo_rseg(const rsmt_bio_jumbo_rseg&);
	rsmt_bio_jumbo_rseg &operator = (const rsmt_bio_jumbo_rseg&);


	// Pacify lint
	rsmt_bio_jumbo_rseg();
};

class rsmt_bio_jumbo_sseg : public rsmt_bio_jumbo_seg {
public:
	// Constructor
	//	seg_index - the segment index, a unique number associate with
	//		every segment in the bufferio component.
	//	seggrp - pointer to the segment group to which this segment
	//		belongs to.
	//	biomgrp - pointer to the bufferio manager
	rsmt_bio_jumbo_sseg(int, rsmt_bio_jumbo_seggrp *, rsmt_bio_mgr *);

	// Destructor
	~rsmt_bio_jumbo_sseg();

	// connects to a receive segment using the connection token
	bool initiate(rsmt_seg_conn_token_t);
	// called from seggrp cleanup
	void cleanup();

	// mark the segment as in use
	bool reserve_segment();
	// mark the segment as free to use
	void release_segment();
	// Divides up the message into chunks and transfers it to the remote
	// node essentially implements the jumbo message protocol.
	bool xfer_data(rsmt_hdr_t *, Buf *, Environment *);
	// Marks that the MSGCHUNKACK has been recvd.
	void mark_ackrecvd();
private:
	// put message chunks using rsm_put
	bool put_data(void *, off_t, size_t, rsm_barrier_t *);
	// implements a trivial timeout routine
	void retry_timeout();
	// is the segment currently in use
	bool	seg_inuse;
	// has the ack to the RSMT_BIO_MSGCHUNK been recvd
	bool	ackrecvd;
	// import segment
	rsmt_import_segment *imseg;

	// Prevent assignments and pass by value
	rsmt_bio_jumbo_sseg(const rsmt_bio_jumbo_sseg&);
	rsmt_bio_jumbo_sseg &operator = (const rsmt_bio_jumbo_sseg&);

	// Pacify lint
	rsmt_bio_jumbo_sseg();
};

class rsmt_bio_jumbo_seggrp {
public:
	// Constructor
	// biomgr - pointer to the rsmt_bio_mgr
	// segsize - max segment size of the jumbo segments
	// max_rsegs - max number of recv segments in this segment group
	rsmt_bio_jumbo_seggrp(rsmt_bio_mgr *, size_t, int);

	// Destructor
	~rsmt_bio_jumbo_seggrp();

	// called from the rsmt_bio_mgr::initiate_fini
	bool initiate();
	// called from the rsmt_bio_mgr::cleanup
	void cleanup();
	// called from the rsmt_bio_mgr::push
	bool push();
	// called from rsmt_bio_mgr::send
	bool send(rsmt_hdr_t *, Buf *, Environment *);
	// free segments called for resource management
	void free_resources();

	// get the endpoint associated to this seggrp
	rsmt_endpoint *get_endpoint();
	// return the rseg corresponding to the requested segindex
	rsmt_bio_jumbo_rseg *get_rseg(int);
	// return the sseg corresponding to the requested segindex
	rsmt_bio_jumbo_sseg *get_sseg(int);
	// add a receive segment of min<requested size, seg_size>
	void add_rseg(size_t);
	// adds a send segment once the receive segment is ready
	void add_sseg(int, rsmt_seg_conn_token_t);
private:
	// request to create a new jumbo send segment
	bool create_sseg(size_t);
	// return a free jumbo send segment
	rsmt_bio_jumbo_sseg *find_sseg(size_t);
	// Pointer to the bufferio manager
	rsmt_bio_mgr	*biomgr;
	// Maximum segment size allowed
	size_t	seg_size;
	// Maximum number of segs that we can have in this seggrp
	int	max_segs;
	// Number of ssegs that are initialized and available
	int	avlbl_segs;
	// Number of new ssegs that can be added to this seggrp
	int	uninit_segs;
	// indicates whether or not the seggrp has been initialized
	bool	initiated;
	// Locks to protect accesses to data members of the seggrp
	os::mutex_t	sg_lock;
	os::condvar_t	sg_cv;

	// An array of receive segment pointers
	rsmt_bio_jumbo_rseg	**rsegs;
	// An array of send segments pointers
	rsmt_bio_jumbo_sseg	**ssegs;

	// Lock that protects access to the send segment array and
	// receive segment array
	// lock ordering -
	//    seglist_lock can be acquired while sg_lock is held.
	//    sg_lock cannot be acquired while seglist_lock is held
	os::rwlock_t	seglist_lock;

	// Prevent assignments and pass by value
	rsmt_bio_jumbo_seggrp(const rsmt_bio_jumbo_seggrp&);
	rsmt_bio_jumbo_seggrp &operator = (const rsmt_bio_jumbo_seggrp&);

	// Pacify lint
	rsmt_bio_jumbo_seggrp();
};

//
// A refcnt wrapper around GWBuf, this is used by the rsmt_bio_jumbo_rseg
// to manage the existence of GWBuf during the transfer of the large message.
// Since the ownership of GWBuf is transferred to ORB after the
// deliver_message call, a mechanism is needed to control the life of the
// GWBuf entirely by the transport till it is handed over to ORB.
// Note: GWBuf starts with a 0 refcnt.
//
class rsmt_bio_jumbo_buf : public refcnt {
public:
	// Constructor
	//  size - size of the GWBuf
	rsmt_bio_jumbo_buf(uint_t);
	// Destructor
	~rsmt_bio_jumbo_buf();
	// place a hold() on rsmt_bio_jumbo_buf and return the GWBuf pointer
	GWBuf *get_buf();
	// disassociates embedded GWBuf from the rsmt_bio_jumbo_buf
	GWBuf *extract_buf();
private:
	GWBuf *bufp;

	// Prevent assignments and pass by value
	rsmt_bio_jumbo_buf(const rsmt_bio_jumbo_buf&);
	rsmt_bio_jumbo_buf &operator = (const rsmt_bio_jumbo_buf&);

	// Pacify lint
	rsmt_bio_jumbo_buf();
};

#endif	/* _RSMT_BIO_JUMBO_SEG_H */
