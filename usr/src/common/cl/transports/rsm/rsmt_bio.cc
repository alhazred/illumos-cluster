//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma	ident	"@(#)rsmt_bio.cc	1.23	08/05/20 SMI"

#include <sys/types.h>
#include <sys/threadpool.h>

#include <sys/rsm/rsm_common.h>

#include "rsmt_streams.h"
#include "rsmt_bio.h"

// # of jumbo messages
uint32_t rsmt_bio_mgr::jumbo_msgcnt = 0;

#ifdef	DEBUG
// Histogram which tracks bufferio message sizes

// Number of entries in the rsmt_nummsg_cnt array
const int RSMT_MSGCNT_NUM_ENTRIES = 8;
// Message size (logarithmic) of the smallest bucket
const int RSMT_MSGCNT_MIN_MSGSIZE = 8; // min msgsize = 2^8 = 256
//
// # of messages of various sizes - 256, 512, 1024, .... , 16K, > 16K
// nummsg_cnt[0] msg_size =< 256 bytes (2^8),
// nummsg_cnt[1] 256 (2^8) < msg_size <= 512 bytes (2^9),
// and soon.
// The last entry is a catch all
// nummsg_cnt[RSMT_MSGCNT_NUM_ENTRIES-1]
//  2^(RSMT_MSGCNT_MIN_MSGSIZE+RSMT_MSGCNT_NUM_ENTRIES-2) bytes < msg_size
//
uint64_t rsmt_nummsg_cnt[RSMT_MSGCNT_NUM_ENTRIES] = {0};
#endif

rsmt_bio_mgr::rsmt_bio_mgr(rsmt_endpoint *rep, rsmt_seg_mgr *sgmgr, int ladpid,
    nodeid_t rndid, int radpid, rsm_addr_t raddr, int max_sup_segs,
    int max_segs, size_t total_seg_size, int max_jsegs, size_t max_jsgsz) :
	rsmt_cmf_client(ladpid, rndid, radpid, raddr, CMFCLIENT_BUFFERIO),
	segmgr(sgmgr),
	max_supported_segments(max_sup_segs),
	max_segments(max_segs),
	total_segment_size(total_seg_size),
	available_segment_size(total_seg_size),
	max_jumbo_seg_size(max_jsgsz),
	max_jumbo_segments(max_jsegs),
	shutdown(false),
	bio_enabled(false),
	bio_rep(rep),
	pavailseg(NULL),
	remote_pavailseg(NULL),
	sends_in_progress(0),
	sends_accumulated(0)
{
	size_t	partition_sz;
	size_t	seggenr_size;
	size_t	max_seg_size;
	int	max_partitions;
	int	num_segs[RSMT_BIO_MAX_SEGGRPS];
	int	extra_segs;
	int	start_index[RSMT_BIO_MAX_SEGGRPS] = {0};
	int	next_start_index;
	int	max_segs_per_seggrp;
	int	rounded_nsegs;
	int	rounded_nprt;
	size_t	pasize;
	int	total_weight;
	int	i;

	if (max_segments > max_supported_segments) {
		max_segments = max_supported_segments;
		RSMT_PATH_DBG(RSMT_DBGL_WARN, RSMT_DBGF_BUFFERIO,
		    ("max_segments(%d) > max_supported_segments using(%d)\n",
			max_segments, max_supported_segments));
	}

	// we have a fixed number of segment groups
	max_seggrps = RSMT_BIO_MAX_SEGGRPS;

	// divide up the max segments among the seggrps
	max_segs_per_seggrp = max_segments / RSMT_BIO_MAX_SEGGRPS;

	// if there are some extra segments left, spread it over the seggrps
	extra_segs = max_segments % RSMT_BIO_MAX_SEGGRPS;

	// allocate the array of seggrp pointers
	seggrps = new (rsmt_bio_seggrp *[RSMT_BIO_MAX_SEGGRPS]);
	ASSERT(seggrps);

	// initialize the start_index of the first seggrp to 0
	next_start_index = 0;
	total_weight = 0;

	// initialize the pending_msg array
	for (i = 0; i < RSMT_BIO_MAX_SEGGRPS; i++) {
		pending_msgs[i] = 0;
	}

	for (i = 0; i < RSMT_BIO_MAX_SEGGRPS; i++) {
		num_segs[i] = max_segs_per_seggrp;
		if (extra_segs-- > 0) {
			num_segs[i]++;
		}

		// ensure that the holes introduced in per-segment generation
		// number area in the pavail due to the above rounding up does
		// not cause an overflow
		ASSERT(next_start_index <= max_supported_segments);

		// start_index should be multiple of RSMT_GENRPERCACHELINE
		ASSERT(next_start_index%RSMT_GENRPERCACHELINE == 0);

		start_index[i] = next_start_index;

		//
		// Every seggrp's per-segment generation numbers starts
		// at cache aligned offset in the pavail segment as well as
		// it doesn't share a cacheline with another seggrp's
		// per-segment generation numbers.
		// This is guaranteed by assigning start_index which is
		// a multiple of RSMT_GENRPERCACHELINE.
		//
		rounded_nsegs = ROUNDUP(num_segs[i], RSMT_GENRPERCACHELINE);
		while (start_index[i] + rounded_nsegs >
		    max_supported_segments) {
			rounded_nsegs -= RSMT_GENRPERCACHELINE;
		}

		if (rounded_nsegs < num_segs[i]) {
			// we had to settle for a lower number of segments
			num_segs[i] = rounded_nsegs;
		}

		ASSERT(num_segs[i] > 0);

		// calculate start_index for next seggrp
		next_start_index = start_index[i] + rounded_nsegs;

		// calculate the sum of weights
		total_weight += num_segs[i]*RSMT_BIO_PARTITIONS[i].size_weight;
	}

	for (i = 0; i < RSMT_BIO_MAX_SEGGRPS; i++) {
		partition_sz = RSMT_BIO_PARTITIONS[i].partition_sz;

		// divide up the total segment space among the segments
		// proportional to their weights.
		max_seg_size = (total_segment_size *
		    (uint_t)RSMT_BIO_PARTITIONS[i].size_weight) /
		    (uint_t)total_weight;

		// round it down to multiple of pagesize
		max_seg_size &= (size_t)PAGEMASK;

		// max_seg_size is used to create segments - figure out
		// how many partitions we can have - each partition
		// has an associated directory entry.
		max_partitions = (int)(max_seg_size /
		    (partition_sz + sizeof (rsmt_bio_dirent_t)));

		// round up max_partitions so that the per-partition generation
		// numbers for each segment in a seggrp start at cache aligned
		// offset
		rounded_nprt = ROUNDUP(max_partitions, RSMT_GENRPERCACHELINE);

		while (num_segs[i]*rounded_nprt > RSMT_BIO_MAX_PARTITIONS) {
			// pavail-segment doesn't have enough space to
			// accomodate the rounded up max_partitions
			// try the next lower multiple of RSMT_GENRPERCACHELINE
			rounded_nprt -= RSMT_GENRPERCACHELINE;
		};

		if (rounded_nprt < max_partitions) {
			// we had to settle for a lower number of partitions
			max_partitions = rounded_nprt;
		}

		// seggrp should be able to accomodate atleast span_factor
		// number of partitions otherwise the seggrp selection
		// algorithm will break
		ASSERT(max_partitions >= RSMT_BIO_PARTITIONS[i].span_factor);

		// max_partitions and num_segs need not be multiples of
		// RSMT_GENRPERCACHELINE. We only guarantee that if they
		// are not, holes are left in the pavail-segment so that
		// cacheline alignment is ensured as well as cacheline
		// overlap is avoided.
		seggrps[i] = new rsmt_bio_seggrp(this, i, partition_sz,
		    max_partitions, start_index[i], num_segs[i]);

		ASSERT(available_segment_size >= max_seg_size);
		available_segment_size -= max_seg_size;

		ASSERT(seggrps[i]);
	}

	jumbo_seggrp = new rsmt_bio_jumbo_seggrp(this, max_jumbo_seg_size,
	    max_jumbo_segments);
	ASSERT(jumbo_seggrp);

	// At the top of the partition-avail segment there is a
	// generation number per segment. Following that is the
	// generation number per partition for each segment group

	// round up the space required for per-segment generation number
	// so that the per-partition generation numbers start at
	// cacheline aligned addresses

	seggenr_size = (size_t)ROUNDUP((uint_t)max_supported_segments*
	    sizeof (hrtime_t), (ushort_t)RSMT_CACHELINE);
	pasize = seggenr_size +
	    (uint32_t)(RSMT_BIO_MAX_SEGGRPS*RSMT_BIO_MAX_PARTITIONS*
		(short)sizeof (hrtime_t));

	// allocate pagesize aligned buffer for partition-avail segment
	aligned_mem = alloc_chunk_aligned(pasize, (size_t)PAGESIZE,
	    &aligned_size, &raw_mem, &raw_size);
	ASSERT(aligned_mem);
	bzero(raw_mem, raw_size);

	// skip space for per-segment generation number in partition-avail seg
	partition_info = (char *)aligned_mem + seggenr_size;
	remote_partition_info = (off_t)seggenr_size;

	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
		("rsmt_bio_mgr:(%d:%d --> %d:%d) is %p\n",
		    orb_conf::local_nodeid(), ladpid, rndid, radpid, this));
}

//
// Destructor
//
rsmt_bio_mgr::~rsmt_bio_mgr()
{
	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_mgr::destructed(%p)\n", this));

	for (int i = 0; i < RSMT_BIO_MAX_SEGGRPS; i++) {
		ASSERT(seggrps[i] != NULL);
		delete seggrps[i];
	}

	segmgr = NULL;
	max_segments = 0;
	max_jumbo_seg_size = 0;
	max_seggrps = 0;
	bio_rep = NULL;

	if (raw_mem) {
		free_chunk_aligned(raw_mem, raw_size);
	}

	aligned_mem = NULL;
	aligned_size = 0;
	raw_mem = NULL;
	raw_size = 0;
	partition_info = NULL;
	remote_partition_info = 0;
	pavailseg = NULL;
	remote_pavailseg = NULL;
	sends_in_progress = 0;
	sends_accumulated = 0;

	delete [] seggrps;
	delete jumbo_seggrp;
}

//
// Called from rsmt_endpoint::initiate()
//
void
rsmt_bio_mgr::initiate_init()
{
	cm_register();
}

//
// Called from rsmt_endpoint::initiate() - In this method if an action needs
// to wait it should be done with a timeout mechanism, this allows us to
// return to rsmt_endpoint::initiate() so that it can check the state of the
// endpoint.
//
bool
rsmt_bio_mgr::initiate_fini()
{
	rsmt_bio_msg_t	pac_msg;
	int		i;
	os::systime to((os::usec_t)1000000);    // 1 sec

	// don't really need to hold lock here since its single threaded
	// here
	if (!bio_enabled) {
		if (!cm_enable(RSMT_BIO_CMFWINDOW)) {
			RSMT_PATH_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
			    ("rsmt_bio_mgr::cm_enable(%p) failed\n", this));
			return (false);
		} else {
			bio_enabled = true;
		}
	}

	// create the partition avail segment
	// The layout of the partition-avail segment will be identical on
	// both ends of a path.
	if (!pavailseg) {
		pavailseg = segmgr->create_seg(aligned_size, RSMT_SMEX_CONNECT,
		    aligned_mem, aligned_size, RSM_RESOURCE_SLEEP);

		if (!pavailseg) {
			RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
			    ("rsmt_bio_mgr(%p) pavailseg create failed\n",
				this));
			return (false);
		}

		RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_mgr(%p) pavail=0x%p,sz=0x%lx\n", this,
			aligned_mem, aligned_size));

	}

	// stuff the connection token into the message
	pac_msg.u.pac_msg.tok = segmgr->get_conn_token(pavailseg);
	// stuff in the receive segment info for each seggrp
	// its used by sender to figure out the properties and
	// layout of the receive segment
	for (i = 0; i < RSMT_BIO_MAX_SEGGRPS; i++) {
		pac_msg.u.pac_msg.sgrpinfo[i].maxsegs =
		    (ushort_t)seggrps[i]->get_rs_maxsegs();
		pac_msg.u.pac_msg.sgrpinfo[i].maxpartitions =
		    (ushort_t)seggrps[i]->get_rs_maxpartitions();
	}

	// send a control message to the remote
	if (!cm_send(&pac_msg, sizeof (pac_msg), RSMT_BIO_PAVAIL_CONNECT,
	    true)) {
		RSMT_PATH_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_mgr(%p) PAVAIL_CONNECT send failed\n", this));
		return (false);
	}

	RSMT_PATH_DBG(RSMT_DBGL_VERBOSE, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_mgr(%p) PAVAIL_CONNECT sent\n", this));

	bmgr_lock.lock();
	// wait for the connect to the remote pavailseg to happen
	while (remote_pavailseg == NULL) {
		if (bmgr_cv.timedwait(&bmgr_lock, &to) ==
		    os::condvar_t::TIMEDOUT) {
			bmgr_lock.unlock();
			RSMT_PATH_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
			    ("rsmt_bio_mgr(%p) PAVAIL_CONNECT timeout\n",
				this));
			return (false);
		}
	}
	bmgr_lock.unlock();

	// partition-avail segment has been cross-imported - setup is done here

	// add one segment to each of the segment groups
	for (i = 0; i < RSMT_BIO_MAX_SEGGRPS; i++) {
		if (!seggrps[i]->initiate()) {
			RSMT_PATH_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
			    ("rsmt_bio_mgr(%p) seggrp(%d) initiate failed\n",
				this, i));
			return (false);
		}
	}

	// initialize the jumbo seggrp
	if (!jumbo_seggrp->initiate()) {
		RSMT_PATH_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_mgr(%p) jumbo seggrp initiate failed\n", this));
		return (false);
	}

	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_mgr(%p) initiate_fini done\n", this));

	return (true);
}

//
// Called from rsmt_endpoint::initiate() - to cleanup what was done
// in initiate_init and initiate_fini
//
void
rsmt_bio_mgr::initiate_cleanup()
{
	cm_unregister();
	if (bio_enabled) {
		cm_disable();
	}
	bio_enabled = false;

	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_mgr(%p) initiate_cleanup done\n", this));
}

//
// Called from rsmt_endpoint::unblock()
//
void
rsmt_bio_mgr::unblock()
{
	cm_unregister();

	bmgr_lock.lock();
	bio_enabled = false;
	bmgr_cv.broadcast();
	bmgr_lock.unlock();

	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_mgr(%p) unblock done\n", this));
}

//
// Called from rsmt_bio_mgr::push() and rsmt_bio_mgr::push_all.
// Makes one iteration through all segment groups and pushes all arrived
// messages to the ORB. Returns (completion status) false if the endpoint
// cleanup process has started and it is possible that more messages can
// arrive on this bio object during the cleanup process. Returns true
// otherwise. Return value is always true if cleanup has not started,
//
bool
rsmt_bio_mgr::push_once()
{
	bool	allpushed = true;

	RSMT_PATH_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_mgr(%p) push_once\n", this));

	for (int i = 0; i < RSMT_BIO_MAX_SEGGRPS; i++) {
		// push pending messages in the segment group to the ORB.
		if (!seggrps[i]->push()) {
			allpushed = false;
		}
		RSMT_PATH_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_mgr(%p) seggrp[%d]->push()\n", this, i));
	}

	if (!jumbo_seggrp->push()) {
		allpushed = false;
	}

	RSMT_PATH_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_mgr(%p) push_once allpushed = %d\n", this, allpushed));

	return (allpushed);
}

//
// Called from rsmt_bio_mgr::cleanup_fini_post_sm
//
// Pushes all messages arriving on the bio object. After return from this
// call no more messages can arrive on this bio object.
//
void
rsmt_bio_mgr::push_all()
{
	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_mgr(%p) push_all\n", this));

	while (!push_once()) {
		os::usecsleep((os::usec_t)10000);
	}

	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_mgr(%p) push_all done\n", this));
}

//
// Called from rsmt_endpoint::push(). Please see comments in rsmt_endpoint.h.
//
void
rsmt_bio_mgr::push()
{
	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_mgr(%p) push\n", this));

	(void) push_once();

	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_mgr(%p) push done\n", this));
}

//
// Called from rsmt_endpoint::cleanup()
//
// Starts the cleanup.
//
void
rsmt_bio_mgr::cleanup_init()
{
	cm_unregister();

	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_mgr(%p) cleanup_init done\n", this));
}

//
// Called from rsmt_endpoint::cleanup().
//
// Performs cleanup operations that can be done before the endpoint
// segment manager cleanup.
//
void
rsmt_bio_mgr::cleanup_fini_pre_sm()
{
	for (int i = 0; i < RSMT_BIO_MAX_SEGGRPS; i++) {
		seggrps[i]->cleanup_pre_sm();
	}

	jumbo_seggrp->cleanup();

	if (remote_pavailseg) {
		segmgr->disconnect_seg(remote_pavailseg);
		remote_pavailseg = NULL;
	}

	if (pavailseg) {
		segmgr->destroy_seg(pavailseg, 0, NULL, NULL);
		pavailseg = NULL;
	}

	cm_disable();

	bmgr_lock.lock();
	shutdown = true;
	bmgr_cv.broadcast();
	bmgr_lock.unlock();
	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_mgr(%p) cleanup_fini_pre_sm done\n", this));
}

//
// rsmt_bio_mgr::cleanup_fini_post_sm
//
// Called from rsmt_endpoint::cleanup().
//
// Performs cleanup operations that must be done after the endpoint
// segment manager cleanup. Before proceeding with the cleanup, waits
// for all arrived message and any messages that may arrive before the
// segment manager shuts down all communication channels. Please see
// comments in rsmt_endpoint.h.
//
void
rsmt_bio_mgr::cleanup_fini_post_sm()
{
	push_all();
	for (int i = 0; i < RSMT_BIO_MAX_SEGGRPS; i++) {
		seggrps[i]->cleanup_post_sm();
	}
	bio_rep = NULL;
	RSMT_PATH_DBG(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_mgr(%p) cleanup_fini_post_sm done\n", this));
}

//
// This method gets called from the rsmt_sendstream::send().
// The current algorithm is to look for a seggrp which can accomodate the
// message in 2 consecutive partitions. If there is nothing available in
// our seggrp of choice we very reluctantly look seggrps with larger
// partitions.
// If all seggrps are full, we go and wait till something becomes available in
// the first seggrp that was selected.
//
bool
rsmt_bio_mgr::send(rsmt_sendstream *sstream, rsmt_hdr_t *rhp, Buf *bp)
{
	Environment	*e = sstream->get_env();
	bool		rc = false;
	bool		needmsg = false;
	bool		nonblocking = sstream->get_nonblocking();
	size_t		msg_size = sizeof (rsmt_padded_hdr) + bp->span();
	rsmt_bio_sseg	*ssegp = NULL;
	rsmt_bio_msg_t	bmw_msg;
	int		sgidx;
	int		pindex = 0;
	int		i;

	bmgr_lock.lock();
	if (!bio_enabled) {
		bmgr_lock.unlock();
		e->system_exception(CORBA::RETRY_NEEDED(0,
		    CORBA::COMPLETED_NO));
		RSMT_MSG_DBG_D(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_mgr(%p) send message failed !bio_enabled\n",
			this));
		return (false);
	}
	bmgr_lock.unlock();

#ifdef DEBUG
	update_msgcnt(msg_size);
#endif
	// look for the most optimal seggrp to use
	sgidx = get_seggrp(msg_size);

	if (sgidx == RSMT_BIO_MAX_SEGGRPS) {
		// greater than what we can carry in the partitioned segments
		// Use the large message protocol here
		RSMT_MSG_DBG_D(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_mgr(%p) send message(%u) using jumbo segs\n",
			this, msg_size));
		os::atomic_add_32(&jumbo_msgcnt, 1);
		return (jumbo_seggrp->send(rhp, bp, e));
	}

	// sgidx is the most optimal segment group
	RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_mgr(%p) send message(%u) sgidx(%d)\n", this,
		msg_size, sgidx));
	do {
		bmgr_lock.lock();
		if (!bio_enabled) {
			bmgr_lock.unlock();
			e->system_exception(CORBA::RETRY_NEEDED(0,
			    CORBA::COMPLETED_NO));
			RSMT_MSG_DBG_D(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
			    ("rsmt_bio_mgr(%p) send message failed "
				"!bio_enabled\n", this));
			return (false);
		}
		bmgr_lock.unlock();
		// search for partitions starting from seggrp of choice
		for (i = sgidx; i < RSMT_BIO_MAX_SEGGRPS; i++) {
			if (seggrps[i]->reserve_partitions(msg_size, &ssegp,
			    &pindex)) {
				break;
			}
		}

		if (i == RSMT_BIO_MAX_SEGGRPS) {
			// all the seggrps are full
			if (nonblocking) {
				e->system_exception(CORBA::RETRY_NEEDED(0,
				    CORBA::COMPLETED_NO));
				RSMT_MSG_DBG_D(RSMT_DBGL_ERROR,
				    RSMT_DBGF_BUFFERIO,
				    ("rsmt_bio_mgr(%p) send message "
					"WOULDBLOCK\n", this));
				return (false);
			}

			// we block here in the seggrp of choice
			seggrps[sgidx]->wait_for_freepartitions();
		}
	} while (ssegp == NULL);

	// increment sends_in_progress to indicate a msg is being sent
	bmgr_lock.lock();
	sends_in_progress++;
	bmgr_lock.unlock();

	// do the data transfer now
	if (ssegp->xfer_data(pindex, rhp, bp, e)) {
		// make a note of the fact that seggrp[sgidx] has a new msg
		bmgr_lock.lock();
		pending_msgs[sgidx]++;
		bmgr_lock.unlock();
		rc = true;
	} else { // data transfer failed !!!!!!
		// free up the reserved partition
		ssegp->free(pindex, msg_size);
	}

	// release the hold acquired in rsmt_bio_sseg::alloc
	ssegp->rele();

	bmgr_lock.lock();
	// decrement the sends_in_progress
	sends_in_progress--;
	// interrupt coalescing  - if this is the last send need to send
	// an interrupt or if we have already coalesced enough messages
	// so as to reduce message latencies due to interrupt starvation
	if ((sends_in_progress == 0) ||
	    (sends_accumulated == RSMT_MAX_SENDACCUM)) {
		sends_accumulated = 0;
		// check which seggrps have pending messages
		for (i = 0; i < RSMT_BIO_MAX_SEGGRPS; i++) {
			bmw_msg.u.bmw_msg.msgpend[i] =
			    (ushort_t)pending_msgs[i];
			if (pending_msgs[i] > 0) {
				needmsg = true;
				pending_msgs[i] = 0;
			}
		}
		bmgr_lock.unlock();

		// send the interrupt notification

		if (needmsg && !cm_send(&bmw_msg, sizeof (bmw_msg),
		    RSMT_BIO_MSGWAITING, true)) {
			// what if cm_send fails and then there is nothing
			// else that follows - latency???
			bmgr_lock.lock();
			for (i = 0; i < RSMT_BIO_MAX_SEGGRPS; i++) {
				pending_msgs[i] +=
				    bmw_msg.u.bmw_msg.msgpend[sgidx];
			}
			bmgr_lock.unlock();
		}
	} else {
		sends_accumulated++;
		bmgr_lock.unlock();
	}

	RSMT_MSG_DBG_D(RSMT_DBGL_VERBOSE, RSMT_DBGF_BUFFERIO,
	    ("rsmt_bio_mgr(%p) send message %s\n", this, rc ? "done":"failed"));
	return (rc);
}

//
// Called from the rsmt_bio_seggrp::msg_consumed() to do a remote update
// of the pavail segment for a specific segment in the segment
// group.
//  sgrpidx - seggrp index
//  shdw_pavail - shadow_pavail of the seggrp (2-D array of genr numbers)
//  nsegs - # of segments ie entries in shadow_pavail
//  shdwsz - size of array pointed to by elements of shdw_pavail
//  start_sindex - starting segindex of the seggrp
//  last_freed - array of latest per-segment generation number
//  lf_szupdate - size of last_freed array
bool
rsmt_bio_mgr::update_pavail(int sgrpidx, hrtime_t **shdw_pavail, int nsegs,
    size_t shdwsz, int start_sindex, hrtime_t *last_freed, size_t lf_sz)
{
	off_t	seggrp_offset;
	off_t	offset;
	int	barretry = RSMT_BARRIER_RETRYCNT;
	int	rc;
	int	i;
	rsm_barrier_t	bar;
	rsmt_bio_msg_t	brp_msg;

	if (remote_pavailseg == NULL) {
		//
		// This is possible during the final push stage. Push that
		// happens during cleanup.
		//
		return (false);
	}

	// calculate the offset into remote_pavailseg where per-partition
	// info for the seggroup starts
	seggrp_offset = remote_partition_info +
	    (int)(sgrpidx*RSMT_BIO_MAX_PARTITIONS*(short)sizeof (hrtime_t));

	do {
		(void) remote_pavailseg->open_barrier(&bar);
		for (i = 0; i < nsegs; i++) { // loop through all segs
			if (shdw_pavail[i] == NULL) {
				continue;
			}
			// Ensure put size is cacheline multiple
			ASSERT(!(shdwsz & RSMT_CACHELINE_MASK));
			offset = seggrp_offset + (off_t)i*(off_t)shdwsz;
			rc = remote_pavailseg->put(offset,
			    (void *)shdw_pavail[i], shdwsz);
			RSMT_MSG_DBG_D(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
			    ("rsm_put(pinfo) off(0x%lx) d(%p) sz(%ld) err=%d\n",
				offset, shdw_pavail[i], shdwsz, rc));
		}
		rc = remote_pavailseg->order_barrier(&bar);
		ASSERT(rc != RSMERR_BARRIER_NOT_OPENED);

		// for ordering reasons the update of the per-segment
		// generation number is done after order_barrier

		// make sure put sizes are cacheline multiples - for
		// atomicity and performance.
		ASSERT(!(lf_sz & RSMT_CACHELINE_MASK));
		rc = remote_pavailseg->put(
			(off_t)start_sindex*(off_t)sizeof (hrtime_t),
			    last_freed, lf_sz);
		RSMT_MSG_DBG_D(RSMT_DBGL_ALWAYS, RSMT_DBGF_BUFFERIO,
		    ("rsm_put(sginfo) off(0x%lx) d(%p) sz(%ld) err=%d\n",
			(off_t)start_sindex*(off_t)sizeof (hrtime_t),
			last_freed, lf_sz, rc));

		rc = remote_pavailseg->close_barrier(&bar);

		ASSERT((rc != RSMERR_BAD_BARRIER_HNDL) &&
		    (rc != RSMERR_BARRIER_NOT_OPENED) &&
		    (rc != RSMERR_BAD_BARRIER_PTR));

		if ((rc == RSM_SUCCESS) || (rc == RSMERR_CONN_ABORTED)) {
			for (i = 0; i < nsegs; i++) { // loop through all segs
				RSMT_MSG_DBG_D(RSMT_DBGL_INFO,
				    RSMT_DBGF_BUFFERIO,
				    ("upd_pavail %d:%d @ 0x%llx\n", sgrpidx,
					i, last_freed[i]));
			}

			break;
		} else if (rc == RSMERR_BARRIER_FAILURE) {
			// retry put
			barretry--;
		}
	} while (barretry > 0);

	if (rc != RSM_SUCCESS) { // data xfer failed
		RSMT_MSG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_mgr(%p) rsm_put(upd_pavail) barrier=%d\n",
			this, rc));
		return (false);
	}

	brp_msg.u.brp_msg.seggrpindex = (uchar_t)sgrpidx;
	// send a control message to reclaim partititon
	if (!cm_send(&brp_msg, sizeof (brp_msg), RSMT_BIO_RECLAIM_PRT, true)) {
		RSMT_MSG_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_mgr(%p) send RECLAIM_PRT failed\n", this));
		return (false);
	}

	return (true);
}

//
// Inherited from rsmt_cmf_client. All the control messages specific
// to bufferio are handled here.
//
bool
rsmt_bio_mgr::cm_handle(void *msgp, ushort_t msglen, int msgid, bool fc)
{
	rsmt_bio_procmsg_defer_task	*msg_handler;

	if (fc) {
		msg_handler = new(os::NO_SLEEP)
		    rsmt_bio_procmsg_defer_task(this, msgp, msglen, msgid);
		if (msg_handler) {
			common_threadpool::the().defer_processing(msg_handler);
			return (true);
		} else {
			return (false);
		}
	} else { // non-flow controlled messages
		processmsg(msgp, msglen, msgid);
		return (true);
	}
}

//
// Process the control messages. This is called either in the interrupt
// context for non-flowcontrolled messages or from a
// rsmt_bio_procmsg_defer_task context.
//
void
rsmt_bio_mgr::processmsg(void *msgp, ushort_t, int msgid)
{
	rsmt_bio_msg_t		bmsg;
	rsmt_bio_sseg		*ssegp;
	rsmt_import_segment	*imseg;
	rsmt_bio_jumbo_sseg	*sseg;
	rsmt_bio_jumbo_rseg	*rseg;
	int			sgrpid;
	int			segindex;
	int			i;

	//
	// make a local copy so that msgp can be freed immediately for
	// flow-controlled messages
	//
	bcopy(msgp, (void *) &bmsg, sizeof (rsmt_bio_msg_t));

	switch (msgid) {
	case RSMT_BIO_PAVAIL_CONNECT: // flow controlled - defer task context
		// This is a request to connect to the pavail-segment created
		// at the remote endpoint

		// free the message buffer
		cm_free(msgp);

		RSMT_PATH_DBG(RSMT_DBGL_INFO, RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_mgr(%p) recv PAVAIL_CONNECT\n", this));

		bmgr_lock.lock();
		// received another PAVAIL_CONNECT message due to a timeout,
		// retry on the remote side, the prev PAVAIL_CONNECT had
		// succeeded so just cv_broadcast and return
		if (remote_pavailseg) {
			bmgr_cv.broadcast();
			bmgr_lock.unlock();
			break;
		}
		bmgr_lock.unlock();
		segindex = 0;	// starting-index for send segs in the seggrp
		for (i = 0; i < RSMT_BIO_MAX_SEGGRPS; i++) {
			// initialize the send segs for each seggrp
			seggrps[i]->init_ssegs(segindex,
			    (int)bmsg.u.pac_msg.sgrpinfo[i].maxsegs,
			    (int)bmsg.u.pac_msg.sgrpinfo[i].maxpartitions);
			// start-index is rounded up
			segindex +=
			    ROUNDUP((int)bmsg.u.pac_msg.sgrpinfo[i].maxsegs,
				    RSMT_GENRPERCACHELINE);
		}
		// connect to the remote pavail-segment
		imseg = segmgr->connect_seg(bmsg.u.pac_msg.tok);
		if (!imseg) {
			RSMT_PATH_DBG(RSMT_DBGL_ERROR, RSMT_DBGF_BUFFERIO,
			    ("rsmt_bio_mgr(%p) PAVAIL_CONNECT err\n", this));
			break;
		}
		RSMT_PATH_DBG(RSMT_DBGL_INFO, RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_mgr(%p) PAVAIL_CONNECTed(%p)\n", this, imseg));
		bmgr_lock.lock();
		remote_pavailseg = imseg;
		bmgr_cv.broadcast();
		bmgr_lock.unlock();
		break;
	case RSMT_BIO_SEGCREATE: // flow controlled - defer task context
		// This is a request to create a receive segment

		// free the message buffer
		cm_free(msgp);

		RSMT_SEG_DBG(RSMT_DBGL_INFO, RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_mgr(%p) recv SEGCREATE\n", this));

		if (bmsg.u.bcr_msg.flags & RSMT_BIO_CREATEJUMBO) {
			jumbo_seggrp->add_rseg(bmsg.u.bcr_msg.reqsize);
		} else {
		}
		break;
	case RSMT_BIO_SEGCONNECT: // flow controlled - defer task context
		// This is a request to connect to a receive segment created
		// at the remote endpoint

		// free the message buffer
		cm_free(msgp);

		RSMT_SEG_DBG(RSMT_DBGL_INFO, RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_mgr(%p) recv SEGCONNECT\n", this));

		// We have an ordering requirement on the processing of
		// the PAVAIL_CONNECT message - the ssegs need to be
		// created before we can process this message.
		// wait here till PAVAIL_CONNECT is processed or the endpoint
		// is brought down
		bmgr_lock.lock();
		while ((remote_pavailseg == NULL) && bio_enabled) {
			bmgr_cv.wait(&bmgr_lock);
		}

		if (!bio_enabled) {
			bmgr_lock.unlock();
			break;
		}
		bmgr_lock.unlock();

		segindex = (int)bmsg.u.bco_msg.segindex;
		if (bmsg.u.bco_msg.flags & RSMT_BIO_CONNECTJUMBO) {
			jumbo_seggrp->add_sseg(segindex, bmsg.u.bco_msg.tok);
		} else {
			sgrpid = (int)bmsg.u.bco_msg.nseggrp;
			if (seggrps[sgrpid]) {
				ssegp = seggrps[sgrpid]->get_sseg(segindex);
				RSMT_PATH_DBG(RSMT_DBGL_ALWAYS,
				    RSMT_DBGF_BUFFERIO,
				    ("rsmt_bio_mgr(%p) SEGCONNECT sgrpid=%d "
					"segindex=%d sseg(%p)\n", this, sgrpid,
					segindex, ssegp));
				if (ssegp) {
					// pass in the conn token that we got
					(void) ssegp->initiate(
						&bmsg.u.bco_msg.tok);
					// get_sseg acquires a hold
					ssegp->rele();
				}
			}
		}
		break;
	case RSMT_BIO_MSGWAITING: // flow controlled
		// This message is the notification interrupt - it indicates
		// that there are new messages in the receive segments that
		// need to be delivered to the ORB.

		// free the message buffer
		cm_free(msgp);

		bmgr_lock.lock();
		while ((remote_pavailseg == NULL) && bio_enabled) {
			bmgr_cv.wait(&bmgr_lock);
		}
		bmgr_lock.unlock();

		for (i = 0; i < RSMT_BIO_MAX_SEGGRPS; i++) {
			if (bmsg.u.bmw_msg.msgpend[i] > 0) {
				// some new messages in this segment group
				// push it to the ORB.
				(void) seggrps[i]->push();
			}
		}
		break;
	case RSMT_BIO_MSGCHUNK: // flow controlled
		// This message is the notification that a chunk of a jumbo
		// message has been copied to a jumbo receive segment

		// free the message buffer
		cm_free(msgp);

		RSMT_MSG_DBG_D(RSMT_DBGL_INFO, RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_mgr(%p) recv MSGCHUNK(%d)\n", this,
			bmsg.u.bmc_msg.segindex));

		rseg = jumbo_seggrp->get_rseg(bmsg.u.bmc_msg.segindex);
		if (rseg) {
			rseg->push(bmsg.u.bmc_msg.msg_offset,
			    bmsg.u.bmc_msg.msg_len,
			    bmsg.u.bmc_msg.flags & RSMT_BIO_CHUNKFIRST);
			rseg->rele();  // release hold acquired by get_rseg()
		}
		break;
	case RSMT_BIO_MSGCHUNKACK: // not flow controlled
		// This message is the ack for a chunk of a jumbo message
		// that has been received
		RSMT_MSG_DBG_D(RSMT_DBGL_INFO, RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_mgr(%p) recv MSGCHUNKACK(%d)\n", this,
			bmsg.u.bmca_msg.segindex));
		sseg = jumbo_seggrp->get_sseg(bmsg.u.bmca_msg.segindex);
		if (sseg) {
			sseg->mark_ackrecvd();
			sseg->rele(); // release hold acquired by get_sseg()
		}
		break;
	case RSMT_BIO_RECLAIM_PRT: // flow-controlled
		//
		// This message is a request to reclaim partitions for
		// segments in a specific segment group.
		//

		// free the message buffer
		cm_free(msgp);

		sgrpid = (int)bmsg.u.brp_msg.seggrpindex;
		RSMT_PATH_DBG_D(RSMT_DBGL_VERBOSE,
		    RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_mgr(%p) RECLAIM_PRT sgrpid=%d\n", this, sgrpid));

		if (seggrps[sgrpid]) {
			seggrps[sgrpid]->reclaim();
		}
		break;
	case RSMT_BIO_RECLAIM_PRTACK: // flow-controlled
		//
		// This message indicates that a prior request to reclaim
		// partitions has been completed.
		//

		// free the message buffer
		cm_free(msgp);

		sgrpid = (int)bmsg.u.brpa_msg.seggrpindex;
		RSMT_PATH_DBG_D(RSMT_DBGL_VERBOSE,
		    RSMT_DBGF_BUFFERIO,
		    ("rsmt_bio_mgr(%p) RECLAIM_PRTACK sgrpid=%d\n", this,
			sgrpid));
		if (seggrps[sgrpid]) {
			seggrps[sgrpid]->reclaim_completed();
		}
		break;
	default:
		ASSERT(0);
	}
}

#ifdef	DEBUG
void
rsmt_bio_mgr::update_msgcnt(size_t msgsize)
{
	int index;
	int numbits = (int)sizeof (size_t) * 8;
	size_t mask = (size_t)1 << (numbits - 1);

	// find the position of the most significant bit
	while ((msgsize & mask) == 0) {
		numbits--;
		msgsize = msgsize << 1;
	}

	if (msgsize << 1 == 0) {
		// msgsize is power of 2, goes to the lower bucket
		// bucket[0] <= 256, [1] <= 512 and so on
		numbits--;
	}

	if (numbits > (RSMT_MSGCNT_NUM_ENTRIES + RSMT_MSGCNT_MIN_MSGSIZE - 2))
		index = RSMT_MSGCNT_NUM_ENTRIES - 1;
	else
		index = numbits - RSMT_MSGCNT_MIN_MSGSIZE;

	// increase the appropriate counter
	os::atomic_add_64(&rsmt_nummsg_cnt[index], (int64_t)1);
}
#endif

//
// This defer task is used for processing flow controlled messages.
//
rsmt_bio_procmsg_defer_task::rsmt_bio_procmsg_defer_task(rsmt_bio_mgr *mgrp,
    void *msg, ushort_t len, int id):
	bio_mgr(mgrp),
	msgp(msg),
	msglen(len),
	msgid(id)
{
}

void
rsmt_bio_procmsg_defer_task::execute()
{
	// process the message
	bio_mgr->processmsg(msgp, msglen, msgid);

	// delete the defer_task object
	delete this;
}
