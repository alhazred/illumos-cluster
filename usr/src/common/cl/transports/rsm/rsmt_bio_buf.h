/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RSMT_BIO_BUF_H
#define	_RSMT_BIO_BUF_H

#pragma ident	"@(#)rsmt_bio_buf.h	1.5	08/05/20 SMI"


#include <orb/buffers/Buf.h>

#include "rsmt_bio_seg.h"

extern size_t RSMT_BIO_MIN_LOANSZ;

//
// This file defines an rsm-transport specific Buf type which is used
// to encapsulate data before it is passed on to the ORB. rsmt_bio_buf
// inherits from nil_Buf. The data buffer passed to rsmt_bio_buf can
// either be on HEAP (Buf::HEAP) or in rsmt_bio_rseg partitions (Buf::OTHER).
// The Buf::OTHER alloc_type is used when a partition is loaned up to the
// ORB in order to avoid an extra bcopy from the partition into the Buf.
// In this case when the rsmt_bio_buf gets destructed the partition is
// released. In the case of Buf::HEAP the data buffer is dynamically allocated
// and passed in the constructor and the destructor frees it up.
//

//
// Class that defines the rsmt_bio_buf
//
class rsmt_bio_buf : public nil_Buf {
public:
	//
	// Constructor:
	//   - takes pointer to the bufferio receive segment
	//   - pointer to the buffer that has the message data
	//   - size of the message
	//   - alloc_type of the buffer either Buf::HEAP or Buf::OTHER
	//
	rsmt_bio_buf(rsmt_bio_rseg *, uchar_t *, uint_t, Buf::alloc_type);

	// Destructor
	~rsmt_bio_buf();
private:
	// pointer to the bufferio receive segment where the message arrived
	rsmt_bio_rseg	*rseg;
	// pointer to the data buffer - either on heap or in a partition
	uchar_t		*datap;
	// size of the data buffer
	uint_t		size;

	// Disallow assignments and pass by value
	rsmt_bio_buf(const rsmt_bio_buf &);
	rsmt_bio_buf &operator = (const rsmt_bio_buf &);

	// pacify lint
	rsmt_bio_buf();
};

#include "rsmt_bio_buf_in.h"

#endif	/* _RSMT_BIO_BUF_H */
