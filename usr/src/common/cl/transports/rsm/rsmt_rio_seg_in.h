/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_RSMT_RIO_SEG_IN_H
#define	_RSMT_RIO_SEG_IN_H

#pragma ident	"@(#)rsmt_rio_seg_in.h	1.10	08/05/20 SMI"

inline
rsmt_rio_client_seg::rsmt_rio_client_seg(rsmt_export_segment *esp,
    rsmt_rio_seg_pool *sp) :
	_SList::ListElem(this),
	eseg(esp),
	trashmem(NULL),
	spool(sp),
	dtp(NULL),
	recently_used(true)
{
	dtp = new rsmt_rio_cseg_dt(this);
	spool->get_rio_seg_mgr()->hold();
}

inline
rsmt_rio_client_seg::~rsmt_rio_client_seg()
{
	spool->get_rio_seg_mgr()->rele();
	if (dtp != NULL) {
		delete dtp;
	}
}

inline rsmt_export_segment *
rsmt_rio_client_seg::get_eseg()
{
	return (eseg);
}

inline rsmt_rio_seg_pool *
rsmt_rio_client_seg::get_seg_pool()
{
	return (spool);
}

inline rsmt_rio_cseg_dt *
rsmt_rio_client_seg::extract_task()
{
	rsmt_rio_cseg_dt *thedtp = dtp;
	dtp = NULL;	// Ownership transferred to the caller
	return (thedtp);
}

inline bool
rsmt_rio_client_seg::get_recently_used()
{
	return (recently_used);
}

inline void
rsmt_rio_client_seg::set_recently_used(bool flag)
{
	recently_used = flag;
}

inline
rsmt_rio_server_seg::rsmt_rio_server_seg(rsmt_import_segment *isp,
    rsmt_rio_seg_mgr *rsmp) :
	iseg(isp),
	rio_smgrp(rsmp),
	dtp(NULL),
	retired(false)
{
	hent = new base_hash_table::hash_entry;
	dtp = new rsmt_rio_sseg_dt(this);
	rio_smgrp->hold();
}

inline
rsmt_rio_server_seg::~rsmt_rio_server_seg()
{
	rio_smgrp->rele();
	ASSERT(hent != NULL);
	delete hent;
	if (dtp != NULL) {
		delete dtp;
	}
}

inline rsmt_import_segment *
rsmt_rio_server_seg::get_iseg()
{
	return (iseg);
}

inline base_hash_table::hash_entry *
rsmt_rio_server_seg::get_hent()
{
	return (hent);
}

inline rsmt_rio_seg_mgr *
rsmt_rio_server_seg::get_rio_seg_mgr()
{
	return (rio_smgrp);
}

inline rsmt_rio_sseg_dt *
rsmt_rio_server_seg::extract_task()
{
	rsmt_rio_sseg_dt *thedtp = dtp;
	dtp = NULL;
	return (thedtp);
}

inline void
rsmt_rio_server_seg::retire()
{
	ASSERT(!retired);
	retired = true;
}

inline bool
rsmt_rio_server_seg::is_retired()
{
	return (retired);
}

inline rsmt_seg_mgr *
rsmt_rio_seg_mgr::get_seg_mgr()
{
	return (smgrp);
}

inline bool
rsmt_rio_seg_mgr::valid_rio_size(size_t riosize)
{
	return (RSMT_MIN_RIO_SIZE <= riosize && riosize <= max_seg_size);
}

inline void
rsmt_rio_seg_mgr::dispose_server_seg(rsmt_rio_server_seg *sseg)
{
	rsmt_rio_sseg_dt *dtp = sseg->extract_task();

	common_threadpool::the().defer_processing(dtp);
}

inline rsmt_rio_seg_mgr *
rsmt_rio_seg_pool::get_rio_seg_mgr()
{
	return (rio_smgrp);
}

inline rsmt_seg_mgr *
rsmt_rio_seg_pool::get_seg_mgr()
{
	return (smgrp);
}

inline void
rsmt_rio_seg_pool::dispose_client_seg(rsmt_rio_client_seg *cseg)
{
	rsmt_rio_cseg_dt *dtp = cseg->extract_task();

	common_threadpool::the().defer_processing(dtp);
}

inline
rsmt_rio_seg_scanner_dt::rsmt_rio_seg_scanner_dt(rsmt_rio_seg_mgr *sp) :
	rio_segmgrp(sp)
{
}

inline
rsmt_rio_seg_scanner_dt::~rsmt_rio_seg_scanner_dt()
{
}

inline void
rsmt_rio_seg_scanner_dt::execute()
{
	rio_segmgrp->seg_scanner();

	delete this;
}

inline
rsmt_rio_cseg_dt::rsmt_rio_cseg_dt(rsmt_rio_client_seg *cs) :
	cseg(cs)
{
}

inline
rsmt_rio_cseg_dt::~rsmt_rio_cseg_dt()
{
}

inline
rsmt_rio_sseg_dt::rsmt_rio_sseg_dt(rsmt_rio_server_seg *ss) :
	sseg(ss)
{
}

inline
rsmt_rio_sseg_dt::~rsmt_rio_sseg_dt()
{
}

#endif	/* _RSMT_RIO_SEG_IN_H */
