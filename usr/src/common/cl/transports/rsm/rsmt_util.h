/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RSMT_UTIL_H
#define	_RSMT_UTIL_H

#pragma ident	"@(#)rsmt_util.h	1.25	08/05/20 SMI"

#include <sys/os.h>
#include <sys/threadpool.h>
#include <sys/dbg_printf.h>
#include "rsmt_transport.h"

#if defined(DBGBUFS) && !defined(NO_RSMT_DBGBUF)
//
// RSM transport uses four debug buffers. The purpose of using multiple debug
// buffers is to prevent messages logged during frequent operations from
// overrunning useful messages logged by not so frequent events.
//
// The four debug buffers in the order of decreasing message frequency are
// as follows:
//
// rsmt_msg_dbg:	Debug messages logged per message transfer operation
// rsmt_seg_dbg:	Debug messages logged per segment operation
// rsmt_path_dbg:	Debug messages logged per path operation
// rsmt_trans_dbg:	Messages not covered by the above three buffers
//
extern dbg_print_buf rsmt_msg_dbg;
extern dbg_print_buf rsmt_seg_dbg;
extern dbg_print_buf rsmt_path_dbg;
extern dbg_print_buf rsmt_trans_dbg;

//
// The verbosity of messages logged to the RSM transport debug buffers
// can be controlled by the changing the rsmt_dbg_level variable. For the
// sake of simplicity logging to all four debug buffers is controlled by
// only one global variable.
//
// Some guidelines:
// a.	Anything that gets logged per message transfer is verbose.
// b.	Messages that get logged each iteration of a loop that is waiting
//	for some event to become true are verbose.
// c.	Path up and down events are considered rare (and error prone) enough
//	that messages pertaining to them should be logged always.
// d.	Segment events should normally be logged at the info or the warning
//	level depending on the event.
//
extern	uint_t		rsmt_dbg_level;

#define	RSMT_DBGL_ALWAYS	0	// Logged always
#define	RSMT_DBGL_ERROR		0	// Error messages
#define	RSMT_DBGL_WARN		1	// Warning messages
#define	RSMT_DBGL_INFO		2	// Informational messages
#define	RSMT_DBGL_VERBOSE	3	// Truly verbose

//
// Logging from various components of RSM transport can be selectively
// turned on or off by toggling the respective bits in the following
// bitmask. Note that besides providing component isolation, this
// facility also provides a way to control verbosity within a single
// component.
//
extern	uint64_t	rsmt_dbg_flags;

#define	RSMT_DBGF_TRANSPORT	((uint64_t)0x00000001)
#define	RSMT_DBGF_ADAPTER	((uint64_t)0x00000002)
#define	RSMT_DBGF_PATHEND	((uint64_t)0x00000010)
#define	RSMT_DBGF_ENDPOINT	((uint64_t)0x00000100)
#define	RSMT_DBGF_SENDSTREAM	((uint64_t)0x00000200)
#define	RSMT_DBGF_RECSTREAM	((uint64_t)0x00000400)
#define	RSMT_DBGF_CMF		((uint64_t)0x00001000)
#define	RSMT_DBGF_SEGMGR	((uint64_t)0x00010000)
#define	RSMT_DBGF_REPLYIO	((uint64_t)0x00100000)
#define	RSMT_DBGF_BUFFERIO	((uint64_t)0x01000000)

#ifdef DEBUG
#define	RSMT_DBGL_DEFAULT	RSMT_DBGL_VERBOSE
#else
#define	RSMT_DBGL_DEFAULT	RSMT_DBGL_WARN
#endif	// DEBUG
#define	RSMT_DBGF_DEFAULT	((uint64_t)-1)

#define	RSMT_GEN_DBG(level, type, dbgbuf, arg) \
	if (((level) <= rsmt_dbg_level) && ((type) & rsmt_dbg_flags)) \
		(dbgbuf).dbprintf arg

#define	RSMT_TRANS_DBG(level, type, arg) \
	RSMT_GEN_DBG(level, type, rsmt_trans_dbg, arg)

#define	RSMT_PATH_DBG(level, type, arg) \
	RSMT_GEN_DBG(level, type, rsmt_path_dbg, arg)

#define	RSMT_SEG_DBG(level, type, arg) \
	RSMT_GEN_DBG(level, type, rsmt_seg_dbg, arg)

#define	RSMT_MSG_DBG(level, type, arg) \
	RSMT_GEN_DBG(level, type, rsmt_msg_dbg, arg)
#else	// DBGBUFS
#define	RSMT_TRANS_DBG(level, type, arg)
#define	RSMT_PATH_DBG(level, type, arg)
#define	RSMT_SEG_DBG(level, type, arg)
#define	RSMT_MSG_DBG(level, type, arg)
#endif	// DBGBUFS

//
// Debug messages that should not even be compiled in non debug
// bits should be logged using the *_D macros.
//
#ifdef DEBUG
#define	RSMT_TRANS_DBG_D(level, type, arg) \
	RSMT_TRANS_DBG(level, type, arg)

#define	RSMT_PATH_DBG_D(level, type, arg) \
	RSMT_PATH_DBG(level, type, arg)

#define	RSMT_SEG_DBG_D(level, type, arg) \
	RSMT_SEG_DBG(level, type, arg)

#define	RSMT_MSG_DBG_D(level, type, arg) \
	RSMT_MSG_DBG(level, type, arg)
#else	// DEBUG
#define	RSMT_TRANS_DBG_D(level, type, arg)
#define	RSMT_PATH_DBG_D(level, type, arg)
#define	RSMT_SEG_DBG_D(level, type, arg)
#define	RSMT_MSG_DBG_D(level, type, arg)
#endif	// DEBUG

// Cacheline size is 64 bytes, RSM interconnects guarantee atomicity
// for writes which are cachline sized and offset in the import seg is
// cacheline aligned. To ensure atomicity of updates for directory entry
// as well as generation numbers, the layout of directory in receive segments
// and pavail segment makes sure that data is sufficiently padded so that
// puts are always multiple of cacheline. Also cacheline sized puts are
// more optimal.
const int RSMT_CACHELINE = 64;

const size_t RSMT_CACHELINE_MASK = RSMT_CACHELINE - 1;

#define	ROUNDUP_CACHELINE(b) ((b + RSMT_CACHELINE - 1) & ~RSMT_CACHELINE_MASK)

#define	RSMT_OFFSET(type, field) ((off_t)(&((type *)0)->field))

// Allocate memory that starts and ends on a chunk boundary of at
// least "size" size.  The return value is the chunk aligned address.
// Also returned are the size rounded up to chunk_size, and the
// actual start and size of the allocation.
//
void *alloc_chunk_aligned(size_t size, size_t chunk_size,
    size_t *rounded_size, void **actual_start, size_t *actual_size,
    os::mem_alloc_type = os::SLEEP);
void free_chunk_aligned(void *p, size_t size);

#endif	/* _RSMT_UTIL_H */
