/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  utcp_transport_in.h
 *
 */

#ifndef _UTCP_TRANSPORT_IN_H
#define	_UTCP_TRANSPORT_IN_H

#pragma ident	"@(#)utcp_transport_in.h	1.24	08/05/20 SMI"

//
// Contains all the inline functions for all the tcp transport classes
// i.e pathend, endpoint, transport, adapter etc.
//

inline utcp_transport *
utcp_pathend::get_transport() const
{
	return ((utcp_transport *)transportp);
}

inline os::mutex_t *
utcp_pathend::tpe_get_lock()
{
	return (&_tpe_lock);
}

inline void
utcp_pathend::tpe_lock()
{
	tpe_get_lock()->lock();
}

inline bool
utcp_pathend::tpe_lock_held()
{
	return (tpe_get_lock()->lock_held());
}

inline void
utcp_pathend::tpe_unlock()
{
	tpe_get_lock()->unlock();
}

//
// Called on receiving the initial message on tcp_pathend to store
// the incarnation number of the remote node. This is stored here
// till pathend::initiate() returns this value to the generic pathend
inline void
utcp_pathend::set_incn(incarnation_num incn)
{
	ASSERT(tpe_lock_held());
	tpe_incn = incn;
}

// Verify whether the initial TCP message has been received
// not holding locks, so this is a hint but is guaranteed to read a single byte
// and will read a consistent value
inline bool
utcp_pathend::has_msg_been_received()
{
	return (tpe_msg_received);
}

// Called on receiving initial TCP message and when removing path
inline void
utcp_pathend::signal_msg_received()
{
	ASSERT(tpe_lock_held());
	tpe_msg_cv.signal();
}

// Call on receiving initial TCP message, sets flag to true and wakes up
// the thread in process_accept
inline void
utcp_pathend::set_msg_received()
{
	ASSERT(tpe_lock_held());
	ASSERT(!tpe_msg_received);
	tpe_msg_received = true;
	signal_msg_received();
}

inline void
utcp_pathend::signal_tpe()
{
	tpe_cv.signal();
}

inline pe_udp_msg_state
utcp_pathend::get_udp_msg_state() const
{
	return (tpe_udp_msg_received);
}

inline void
utcp_pathend::set_udp_msg_state(pe_udp_msg_state val)
{
	tpe_udp_msg_received = val;
}

inline void
utcp_pathend::udp_msg_lock()
{
	tpe_udp_msg_lock.lock();
}

inline void
utcp_pathend::udp_msg_unlock()
{
	tpe_udp_msg_lock.unlock();
}

inline void
utcp_pathend::udp_msg_signal()
{
	tpe_udp_msg_cv.signal();
}

inline in_addr_t
utcp_pathend::get_local_ipaddr() const
{
	return (local_ipaddr);
}

inline in_addr_t
utcp_pathend::get_remote_ipaddr() const
{
	return (remote_ipaddr);
}

inline bool
utcp_pathend::get_do_esballoc() const
{
	return (do_esballoc);
}

inline utcp_transport *
utcp_endpoint::get_transport() const
{
	return ((utcp_transport *)transportp);
}

// not holding locks, so this is a hint but is guaranteed to read a single word
// and will read a consistent value
inline bool
utcp_endpoint::is_endpoint_registered()
{
	return (get_state() == E_REGISTERED);
}

inline bool
utcp_endpoint::is_same_pathend(pathend *pe)
{
	return (pathendp == pe);
}

inline os::mutex_t *
utcp_endpoint::tep_get_lock()
{
	return (&_tep_lock);
}

inline void
utcp_endpoint::tep_lock()
{
	tep_get_lock()->lock();
}

inline void
utcp_endpoint::tep_unlock()
{
	tep_get_lock()->unlock();
}

inline bool
utcp_endpoint::tep_lock_held()
{
	return (tep_get_lock()->lock_held());
}


inline void
utcp_endpoint::set_msg_received(uint_t connection)
{
	ASSERT(tep_lock_held());
	ASSERT(!so_rmsgs[connection]);
	so_rmsgs[connection] = true;
	so_rmsg_cv[connection].signal();
}

// Not holding any locks, assuming safe atomic read
inline bool
utcp_endpoint::has_msg_been_received(uint_t connection)
{
	return (so_rmsgs[connection]);
}

inline void
utcp_endpoint::signal_tep()
{
	ASSERT(tep_lock_held());
	tep_cv.signal();
}

inline ep_udp_msg_state
utcp_endpoint::get_udp_msg_state() const
{
	return (tep_udp_msg_received);
}

inline void
utcp_endpoint::set_udp_msg_state(ep_udp_msg_state val)
{
	tep_udp_msg_received = val;
}

inline void
utcp_endpoint::udp_msg_lock()
{
	tep_udp_msg_lock.lock();
}

inline void
utcp_endpoint::udp_msg_unlock()
{
	tep_udp_msg_lock.unlock();
}

inline void
utcp_endpoint::udp_msg_signal()
{
	tep_udp_msg_cv.signal();
}

inline ID_node &
utcp_endpoint::get_rnode()
{
	return (ep_node);
}

inline int
utcp_endpoint::get_queue(uint_t conn)
{
	return (so_ptrs[conn]);
}

// Return the MTU for this endpoint
inline uint_t
utcp_endpoint::get_mtu_size() const
{
	return (mtu_size);
}

// Return the write offset (space to reserve for tcp/ip headers)
// for this endpoint
inline uint_t
utcp_endpoint::get_wroff() const
{
	return (wroff);
}

inline bool
utcp_endpoint::get_do_esballoc() const
{
	return (do_esballoc);
}

inline bool
utcp_endpoint::timeouts_are_disabled()
{
	return (timeouts_disabled ? true : false);
}

inline bool
utcp_endpoint::timeouts_are_enabled()
{
	return (timeouts_disabled ? false : true);
}

inline void
utcp_sendstream::set_ack_pending()
{
	// Setting this without holding locks as we have not sent the message
	// and we could not have anybody waiting for this or signalling yet
	ASSERT(!ack_pending);
	ack_pending = true;
}

inline pathend *
utcp_transport::new_pathend(clconf_path_t *pp)
{
	return (new utcp_pathend(this, pp));
}

inline void
utcp_transport::post(defer_task *t)
{
	tcptr_threadpool.defer_processing(t);
	task_lock.lock();
	task_count++;
	task_lock.unlock();
}

inline void
utcp_transport::unpost()
{
	task_lock.lock();
	ASSERT(task_count > 0);
	task_count--;
	if (task_count == 0)
		task_cv.broadcast();
	task_lock.unlock();
}

inline void
utcp_transport::add_tep(utcp_endpoint *ep)
{
	tep_list_lock.lock();
	ASSERT(!endp_list.exists(ep));
	endp_list.prepend(ep);
	tep_list_lock.unlock();
}

inline void
utcp_transport::remove_tep(utcp_endpoint *ep)
{
	tep_list_lock.lock();
	ASSERT(endp_list.exists(ep));
	endp_list.erase(ep);
	tep_list_lock.unlock();
}

inline utcp_pathend *
utcp_transport::get_tcp_pathend(int newso)
{
	return (get_tcp_pathend_for_ipaddr(get_peeraddr(newso)));
}

inline utcp_endpoint *
utcp_transport::get_tcp_endpoint(int newso)
{
	return (get_tcp_endpoint_for_ipaddr(get_peeraddr(newso)));
}

inline
ustart_server_task::ustart_server_task(utcp_transport *trp,
    tcptr_objtype objt, int proto) :
	transp(trp),
	objtype(objt),
	server_type(proto)
{
}

inline
utcp_keep_active_task::utcp_keep_active_task(utcp_transport *trp) :
	transp(trp)
{
}

inline void
utcp_keep_active_task::execute()
{
	transp->keep_active();
	delete this;
}

#endif	/* _UTCP_TRANSPORT_IN_H */
