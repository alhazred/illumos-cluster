/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _UTCP_TRANSPORT_H
#define	_UTCP_TRANSPORT_H

#pragma ident	"@(#)utcp_transport.h	1.68	08/05/20 SMI"

/* Don't include this */
#define	_SYS_STRSUBR_H

class MBlkBuf {};

#include <orb/buffers/marshalstream.h>
#include <orb/transport/transport.h>
#include <transports/tcp/tcp_transport.h>
#include <transports/tcp/tcpmod.h>
#include <sys/os.h>
#include <sys/file.h>
#include <sys/stream.h>
#include <sys/dbg_printf.h>
#include <door.h>

// TBD Need to add utcp_debug comments all over
#if defined(DBGBUFS) && !defined(NO_UTCP_DBGBUF)
#define	UTCP_DBGBUF
#endif

#ifdef UTCP_DBGBUF
extern dbg_print_buf utcp_debug;
#define	TCP_DBPRINTF(a)	utcp_debug.dbprintf a
#else
#define	TCP_DBPRINTF(a)
#endif

#define	UPDATE_UTCP_STATS(f, v)

// forware declarations...
class utcp_transport;
class utcp_pathend;
class utcp_endpoint;
class utcp_sendstream;

// Tcp transport only supports upto 256 bytes of orb_conf::version_str length
#define	MAX_ORBVERSION_LEN 256

class utcp_pathend : public pathend {
	friend class utcp_endpoint;
public:
	utcp_pathend(utcp_transport *, clconf_path_t *);

	~utcp_pathend();

	//
	// Internal state in tcp_pathend.
	// XX Would be nice if these states mapped to those in pathend
	// base class.
	//
	enum tcp_pe_state {	TPE_RESET,
				TPE_INITIATED,
				TPE_CONNECTED
			 };

	//
	// UDP handshake message types exchanged during version
	// compatibility checks. The message types and the three
	// way handshake protocol may not change across releases.
	//
	enum {
		HANDSHAKE_VERSION_INFO = 0,
		ACTIVE_REQUEST,
		PASSIVE_RESPREQ,
		ACTIVE_RESPONSE
	};
	// accessor methods to lock protecting tcp_pathend internal variables
	void tpe_lock();
	void tpe_unlock();
	bool tpe_lock_held();
	os::mutex_t *tpe_get_lock();

	//
	// functions to recv and wait for initial udp message
	//
	int	udp_init();
	void	udp_reset();
	void	udp_finish();
	void	udp_putq(struct pathend_udp_info *, struct sockaddr_in *,
			int);
	int	udp_getq(clock_t);

	void	udp_msg_lock();
	void	udp_msg_unlock();
	void	udp_msg_signal();

	pe_udp_msg_state	get_udp_msg_state() const;
	void	set_udp_msg_state(pe_udp_msg_state);


	// functions to synchronize initial tcp message exchange
	int		so_putq(int);
	int		so_getq();
	void		signal_tpe();

	// accessor methods
	in_addr_t	get_local_ipaddr() const;

	in_addr_t	get_remote_ipaddr() const;

	utcp_transport *get_transport() const;

	bool		get_do_esballoc() const;

	// Function to wait for initial tcp msg from peer.
	void	recv_initial_msg(char *, size_t);
	bool	timed_wait_for_msg();

	// Method to complete connection setup and synchronization
	// after initial connection request
	int process_accept(int, bool);

	// Mark the so_rmsgs array when the initial msg from peer
	// is received for a connection.
	// Also does a signal_msg_received()
	void	set_msg_received();

	// Check if initial tcp message has been received from peer.
	bool	has_msg_been_received();

	void	signal_msg_received();

	void	set_incn(incarnation_num);


	// Methods to send/receive heartbeat messages
	void	pm_send_internal();

	void	pm_recv_internal(char *, size_t);

	// Methods to record timestamp for bad udp handshake messages
	bool		record_bad_udp_handshake();

private:
	// Memory for heartbeats
	// We allocate two buffers (sendbufp and recvbufp) that store
	// the next heartbeat to be sent out and the last heartbeat
	// received respectively. These are shared with the path manager
	char		*sendbufp;
	char		*recvbufp;
	char		*recv_copy_bufp;
	char		*send_copy_mp;

	// Temporary storage for remote node's incarnation number while in
	// initiate(). This is because pathend::initiate takes the incarnation
	// as a return parameter and tcp_pathend should not update the
	// variable in base class pathend.
	incarnation_num	tpe_incn;

	// local and remote ip addresses converted from string to in_addr_t
	// during constructor
	in_addr_t	local_ipaddr;
	in_addr_t	remote_ipaddr;

	// do_esblloc is set according to "lazy_free" property read from
	// clconf for local adapter; if "lazy_free" is implemented in the
	// driver, do_esblloc is false, otherwise, do_esblloc is true
	bool		do_esballoc;

	//
	// Buffer to compose udp handshake messages to be sent and the
	// lock to synchronize access to it.
	//
	char		*udp_send_buf;
	size_t		udp_send_buf_size;
	os::mutex_t	udp_send_buf_lock;

	//
	// The current udp handshake sequence number. The active node
	// increments this sequence number by one whenever it sends out
	// a new ACTIVE_REQUEST. The passive node sets this number to the
	// the sequence number carried in a received ACTIVE_REQUEST messages.
	// Other messages are accepted only if they match this sequence
	// number.
	//
	uint32_t	hs_seq;

	//
	// Lbolt when the most recent bad udp handshake message was received.
	// This is used to determine when to log a message indicating a bad
	// handshake message.
	//
	clock_t		bad_udp_handshake_lbolt;

	void initiate(char *&, char *&, incarnation_num &);

	int udp_client_initiate();

	int udp_server_initiate();

	int client_initiate();

	int server_initiate();

	void _cleanup();	// internal cleanup

	void cleanup();		// called when path goes down

	void reinit();		// called when pathend is recycled.

	void delete_notify();	// called when path is removed.

	// Called to create a new endpoint when the path is declared up
	endpoint *new_endpoint(transport *);

	// tcp_pathend specific state
	tcp_pe_state	tpe_state;

	// lock protecting most variables in tcp_pathend
	// XX Could use the lock in pathend without needing another one
	os::mutex_t	_tpe_lock;

	// UDP socket used for initial messaging before connection setup
	int		udp_so;

	//
	// If the remote node connects, then obtain the connection
	// from the server task.
	// Currently only one connection is assumed. Since pathend
	// is not expected to have more than one connection, the code
	// is simplified. endpoint has several connections.
	//
	uint_t		n_connections;

	// First time an initiation error was seen on this pathend.
	os::hrtime_t	first_err_time;
	// Last time a warning was logged for this pathend.
	os::hrtime_t	last_warn_time;
	void		tpe_update_pe_state(int);

	os::condvar_t	tpe_cv;

	int		in_so_ptr;	// temporary storage on server side

	// TCP socket for pathend.
	// and precalculated queue pointer from sonode.
	// set after TPE_CONNECTED state
	int		so_ptr;
#if 0
	queue_t		*so_wq;
#endif

	// bool/cv corresponding to initial tcp message receipt
	bool		tpe_msg_received;
	os::condvar_t	tpe_msg_cv;

	// state/cv/mutex corresponding to initial udp message exchange
	pe_udp_msg_state	tpe_udp_msg_received;
	os::condvar_t	tpe_udp_msg_cv;
	os::mutex_t	tpe_udp_msg_lock;

	// A flag that indicates whether a heartbeat has been received on
	// this path in the non interrupt context. SC3.0 requires drivers
	// to deliver heartbeat messages in the interrupt context. If this
	// flag is set, it indicates a problem with the network driver. The
	// flag is used solely for debugging.
	bool		rcvd_nonintr_hb;

	//
	// Disallow assignments and pass by value
	//
	utcp_pathend(const utcp_pathend &);
	utcp_pathend &operator = (utcp_pathend &);
};

class utcp_endpoint : public endpoint {
public:
	utcp_endpoint(utcp_transport *, utcp_pathend *);

	~utcp_endpoint();

	// accessors
	utcp_transport *get_transport() const;

	void add_resources(resources *);

	sendstream *get_sendstream(resources *resourcep);

	enum endpoint_state { 	TEP_RESET,
				TEP_INITIATED,
				TEP_CONNECTED
			    };


	//
	// functions to recv and wait for initial udp message
	//
	int	udp_init();
	void	udp_finish();
	void	udp_putq(struct endpoint_udp_info *, struct sockaddr_in *);
	int	udp_getq(clock_t);

	void	udp_msg_lock();
	void	udp_msg_unlock();
	void	udp_msg_signal();

	ep_udp_msg_state	get_udp_msg_state() const;
	void	set_udp_msg_state(ep_udp_msg_state);

	// functions to synchronize initial tcp message exchange
	int		so_putq(int);
	int 		so_getq();
	void		signal_tep();

	// send called from tcp_sendstream::send to form the tcpmod header
	// and send the message to tcp, waiting for any acks as needed
	void send(orb_msgtype, orb_seq_t, Buf *,
			bool, utcp_sendstream *, Environment *);

	// Function called when tcpmod has a message ready to be delivered
	// callee will copy the contents (whatever is needed) from the header
	// by the time the callback returns
	// mem_alloc_type indicates whether the callback can block or not
	int tcp_receive_callback(struct tcpmod_header_t *, char *,
		os::mem_alloc_type);

	// Function called when tcpmod receives an ack for a synchrnous send
	// or a reply io
	void tcp_ack_callback(void *, int, uint_t);

	//
	// Send a TCP_KEEP_ACTIVE message if needed. See comments accompanying
	// the tcp_keep_active_task class declaration for details.
	//
	void send_keep_active_message();

	// Check if endpoint is still registered
	bool is_endpoint_registered();

	// Check if the pathend argument corresponds to the pathend that
	// this endpoint is connected to
	// XX Could be a method on base class endpoint
	bool is_same_pathend(pathend *pe);

	// Accessor functions to get MTU size and write offset
	uint_t	get_mtu_size() const;
	uint_t 	get_wroff() const;

	bool    get_do_esballoc() const;

	//
	// Function to process, once a connection has been established
	//
	int	process_accept(int so_acpt, bool);

	//
	// Function to wait for initial transport msg from peer.
	//
	void	recv_initial_msg(uint_t, char *, size_t);
	bool	timed_wait_for_msg(uint_t);

	//
	// Mark the so_rmsgs array when the initial msg from peer
	// is received for a connection.
	// Also does a signal
	//
	void	set_msg_received(uint_t);

	//
	// Check if a (transport) message has been received from peer.
	//
	bool	has_msg_been_received(uint_t);

	void	tep_lock();
	void	tep_unlock();
	bool	tep_lock_held();
	os::mutex_t *tep_get_lock();

	// ID_node of remote node/incn endpoint is setup with
	// XX This should be a method in base class endpoint
	ID_node &get_rnode();

	// Returns socket corresponding to particular connection
	int get_queue(uint_t);

	// cv used for synchronizing ack receipt
	// See comments above regarding PER_SENDSTREAM_CV
#ifndef PER_SENDSTREAM_CV
	os::condvar_t		ack_cv;
#endif

	// Method called when tcpmod sees a connection going down
	void process_tcp_reset();

	// Method to map a orb message type to an endpoint tcp connection
	uint_t choose_conn(int);

	// endpoint class virtual method override
	void	timeouts_disable();
	void	timeouts_enable();

	// For access to timeouts_disabled
	bool	timeouts_are_disabled();
	bool	timeouts_are_enabled();

private:
	// Methods called from generic transport state machine
	void	initiate();

	void	unblock();

	void	push();

	void	cleanup();


	// Internal methods
	void	_cleanup();

	int udp_client_initiate();

	int udp_server_initiate();

	int client_initiate();

	int server_initiate();

	// Protects updates to private members in utcp_endpoint
	os::mutex_t	_tep_lock;

	//
	// for support of timeouts_disable/enable
	// lock ordering is:  so_lock, _tep_lock
	//
	os::mutex_t	so_lock;

	void	set_timeouts(int);


	// utcp_endpoint specific state
	endpoint_state	tep_state;

	// special flag for retransmission timeouts disable/enable
	bool		timeouts_disabled;

	// Copy of values from pathend
	in_addr_t	local_ipaddr;
	in_addr_t	remote_ipaddr;

	bool		do_esballoc;

	//
	// n_connections indicate the number of connections established
	//
	uint_t  n_connections;

	// Socket used for initial UDP synchronization
	int 		udp_so;

	// XX Need comments explaining what these are for, or a forward
	// pointer to where the comments are
	int 		in_so_ptrs[TCP_EP_MAX_CONNECTIONS];
	int		in_pindex;
	int		in_cindex;

	os::condvar_t	tep_cv;

	// sockets and corresponding queue pointers for the different
	// TCP connections
	int		so_ptrs[TCP_EP_MAX_CONNECTIONS];

	// Keeps track of connections for which initial TCP messages
	// have arrived.
	// XX Since connection setup is done in a serial fashion,
	// we could have used a single cv and a counter.
	bool		so_rmsgs[TCP_EP_MAX_CONNECTIONS];
	os::condvar_t	so_rmsg_cv[TCP_EP_MAX_CONNECTIONS];

	// state/cv/mutex corresponding to initial udp message exchange
	// XX This is common code to endpoint and could be extracted into
	// a separate class
	ep_udp_msg_state	tep_udp_msg_received;
	os::condvar_t	tep_udp_msg_cv;
	os::mutex_t	tep_udp_msg_lock;

	//
	// mtu_size is TCP segment size to use for this connection
	//	and is used in doing fragmentation before handing messages
	//	to TCP stack
	// wroff is the expected size of headers that we leave space for
	//	at the beginning of mblks, so that TCP stack need not
	//	allocate further mblks for headers
	//
	uint_t	mtu_size;
	uint_t 	wroff;

	// Total number of connections to be established and the rio base
	// connection index among those.
	uint_t	max_conns;

	//
	// Last clock tick a message was sent on this endpoint.
	//
	clock_t	last_send_lbolt;

	//
	// Disallow assignments and pass by value
	//
	utcp_endpoint(const utcp_endpoint &);
	utcp_endpoint &operator = (utcp_endpoint &);
};

//
// For utcp sendstreams;
//
class utcp_sendstream : public sendstream {

public:
	ID_node& get_dest_node();

	void send(bool, orb_seq_t &);

	utcp_sendstream(utcp_endpoint *, resources *);

	// routine to call when synchronous ack is recvd
	void recvd_sync_ack(uint_t);

	bool wait_for_sync_ack();

	void set_ack_pending();

	uint_t get_preferred_conn();

	bool			ack_pending;
protected:
	~utcp_sendstream();
private:

	MarshalStream 		MainBuf;
	utcp_endpoint		*endp;

#ifdef PER_SENDSTREAM_CV
	os::condvar_t		ack_cv;
#endif
	// In case ack_pending is set, when did the wait for
	// ack start?
	clock_t		waiting_since;

	// Disallow assignments and pass by value
	utcp_sendstream(const utcp_sendstream&);
	utcp_sendstream& operator = (utcp_sendstream&);
};

//
// For utcp recstreams;
//
class utcp_recstream : public recstream {
public:
	ID_node		&get_src_node();

	bool		initialize(os::mem_alloc_type);

	utcp_recstream(Buf *, ID_node &, orb_msgtype);

protected:
	~utcp_recstream();

private:
	ID_node		src_node;
	Buf		*_buf;

	// Disallow assignments and pass by value
	utcp_recstream(const utcp_recstream &);
	utcp_recstream &operator = (utcp_recstream &);
};

// utcp_adapter
class utcp_adapter : public adapter {
public:
	utcp_adapter(utcp_transport *, clconf_adapter_t *);

	~utcp_adapter();

	// Disallow assignments and pass by value
	utcp_adapter(const utcp_adapter &);
	utcp_adapter &operator = (utcp_adapter &);
};

// utcp_transport
class utcp_transport : public transport {
public:
	utcp_transport();
	~utcp_transport();

	//
	// Function to add a tcp_endpoint to the list of endpoints in initiate
	// used in get_tcp_endpoint
	//
	void add_tep(utcp_endpoint *);
	void remove_tep(utcp_endpoint *);

	//
	// Start the server which listens for tcp connections
	// for the Path End.
	//
	int	start_server_pe();

	//
	// Start the server which listens for tcp connections
	// for the EndPoint.
	//
	int	start_server_ep();

	//
	// udp server routines.
	//
	int	start_udp_server_pe();
	int	start_udp_server_ep();

	//
	// The method to drive the TCP_KEEP_ACTIVE messages.
	//
	void	keep_active();

	// pm_client virtual method override
	void timeouts_disable();
	void timeouts_enable();

	bool	is_shutdown;

	//
	// Used to post a deferred task to the tcptr_threadpool. Also does
	// book keeping of number of tasks started.
	//
	void		post(defer_task *t);
	void		unpost();

private:
	const char *transport_id();

	pathend *new_pathend(clconf_path_t *);

	adapter *new_adapter(clconf_adapter_t *);

	utcp_pathend *get_tcp_pathend(int so);

	utcp_endpoint *get_tcp_endpoint(int so);

	utcp_pathend *get_tcp_pathend_for_ipaddr(in_addr_t);

	utcp_endpoint *get_tcp_endpoint_for_ipaddr(in_addr_t);

	void		shutdown();

	//
	// Buffer to receive pathend udp handshake messages. This
	// handshake is responsible for version negotiation. One buffer
	// is sufficient as one message is received and processed at
	// a time.
	//
	char		*pe_udp_recv_buf;
	size_t		pe_udp_recv_buf_size;

	//
	// List of tcp_endpoints in initiate state and lock
	// Used in get_tcp_endpoint function
	//
	DList<utcp_endpoint>	endp_list;
	os::mutex_t		tep_list_lock;

	// Threadpool for listener tasks
	threadpool		tcptr_threadpool;

	//
	// Counter and associated mutex and cv for keeping track of
	// tasks created by the tcp transport. Used mostly for clean
	// shutdown.
	//
	int			task_count;
	os::mutex_t		task_lock;
	os::condvar_t		task_cv;

	// Disallow assignments and pass by value
	utcp_transport(const utcp_transport&);
	utcp_transport& operator = (utcp_transport&);
};


//
// A task to start server
//
// One server for pathends and one server for endpoints are
// started. These servers listen for incoming connections.
//
class ustart_server_task : public defer_task {

public:
	ustart_server_task(utcp_transport *transp,
				tcptr_objtype objtype,
				int  server_type);

	void execute();

private:
	utcp_transport	*transp;
	tcptr_objtype 	objtype;
	int	server_type;	// protocol is stored here e.g. tcp, udp
				// uses the same constant used by IP
				// i.e IPROTO_TCP etc.

	// Disallow assignments and pass by value
	ustart_server_task(const ustart_server_task &);
	ustart_server_task &operator = (ustart_server_task &);
};

//
// The tcp_keep_active_task is used to send TCP_KEEP_ACTIVE messages
// periodically on idle endpoints. A single instance of this object is
// created when the tcp_transport object is created. The purpose of the
// TCP_KEEP_ACTIVE messages is to keep traffic flowing on idle TCP
// connections belonging to the TCP Transport. If traffic keeps flowing,
// any TCP/IP communication problems will be detected through endpoint
// TCP connection drops. This provides a way to detect TCP/IP communication
// problems on the cluster interconnect even when there is no cluster
// infrastructure traffic. It is important to detect such communication
// failure as user applications might rely on TCP/IP communication even
// when there is no cluster infrastructure traffic.
//
// Why don't we simply use the SO_KEEPALIVE option? There is no way to
// tune TCP's keep alive interval on a per socket basis. We can not use
// the default 2hrs timeout. At the same time setting the system wide
// timeout to our preferred value of ~90sec is not advisable either.
// Besides there is already a mechanism to catch TCP connection drops
// in the tcp transport when TCP level acks don't arrive for cluster
// infrastructure traffic. Piggybacking on that mechanism is the most
// natural way for detecting communication problems even in the absence
// of infrastructure traffic.
//
class utcp_keep_active_task : public defer_task {
public:
	utcp_keep_active_task(utcp_transport *);

	void execute();

private:
	utcp_transport *transp;
};

extern in_addr_t get_peeraddr(int so);

#include <transports/utcp/utcp_transport_in.h>

#endif	/* _UTCP_TRANSPORT_H */
