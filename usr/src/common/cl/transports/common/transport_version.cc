//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
//  transport_version.cc
//

#pragma ident	"@(#)transport_version.cc	1.3	08/05/20 SMI"

#include <sys/sol_version.h>
#include <transports/common/transport_version.h>
#include <nslib/ns.h>
#include <h/ccr.h>

//
// In order to support Quantum Leap upgrade the pathend version number
// is changed. The version number is incremented by one during the
// upgrade to allow the two cluster partitions to co-exist
// and not talk to each other.
//

uint32_t sc_transport_version = 2;

//
// During Quantum Leap upgrade the transport version number in the
// old software is incremented and placed in the CCR. After the
// upgrade we read this from the CCR and initialize the version
// number with the right value.
// This function is called early during boot and the CCR lookup
// happens on the local copy of the CCR.
//
void
update_transport_version()
{
	naming::naming_context_var	ctxp_v;
	Environment			env;
	CORBA::Object_var		obj_v;
	CORBA::Exception		*exp;
	ccr::readonly_table_ptr		table_p;
	char				*version = NULL;
	uint32_t			sc_trans_version;
	ccr::directory_ptr		ccrdir_p;

	// Lookup the CCR directory.
	ctxp_v = ns::local_nameserver();

	obj_v = ctxp_v->resolve("ccr_directory", env);

	if ((exp = env.exception()) != NULL) {
		// Unrecoverable error. Panic the system.
		CL_PANIC(0);
	}

	ccrdir_p = ccr::directory::_narrow(obj_v);
	ASSERT(!CORBA::is_nil(ccrdir_p));

	table_p = ccrdir_p->lookup("infrastructure", env);

	if ((exp = env.exception()) != NULL) {
		// Unrecoverable error. Panic the system.
		CL_PANIC(0);
	}
	version = table_p->query_element(
	    "cluster.transport.sc_transport_version", env);

	if ((exp = env.exception()) != NULL) {
		if (ccr::no_such_key::_exnarrow(exp)) {
			CORBA::release(table_p);
			env.clear();
			return;
		} else {
			// Unrecoverable error. Panic the system.
			CL_PANIC(0);
		}
	}
	ASSERT(version != NULL);
	sc_trans_version = (uint32_t)os::atoi(version);
	ASSERT(sc_trans_version != 0);

	sc_transport_version = sc_trans_version;
}
