/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)kernel_sci.cc	1.6	08/05/20 SMI"

#include <tm/kernel_sci.h>


//
// Constructor for SCI device. clconfig_ops vector is looked up in the
// driver module and ops are initialized.
//
tm_sci_device::tm_sci_device(const char *dn) :
    tm_device(dn)
{
	clconfig_get_ops	func;
	int			version;

	bzero(&ops, sizeof (ops));
	func = (clconfig_get_ops)modlookup((char *)dn, "_clconfigop");
	if (func == NULL) {
		TM_DBG(("Entry %s is not defined in module %s\n", "_clconfigop",
		    dn));
		return;
	}
	if ((version = (*func)(CONFIG_CURRENT_VERSION, &ops, sizeof (ops), 1))
	    < 0 || CONFIG_GET_MAJOR(version) != CONFIG_MAJOR_VERSION) {
		TM_DBG(("Version mismatch for module %s, expected 0x%x, "
		    "got 0x%x\n", dn, CONFIG_CURRENT_VERSION, version));
		if (version > 0 && ops.config_fini != NULL)
			(ops.config_fini)(1);
		bzero(&ops, sizeof (ops));
	}
}


tm_sci_device::~tm_sci_device()
{
	bzero(&ops, sizeof (ops));
}

//
// Called before an SCI device is used for communication.
//
bool
tm_sci_device::setup_point(tm_point *pt)
{
#ifdef USE_LDI
	major_t			major;
	minor_t			minor;
	dev_t			devt;
	int			inst;
#endif
	int			ret;
	clconf_adapter_t	*cl_adp = pt->get_adp();
	clconfig_ops_t		*tm_ops = &ops;
	bool			rc = false;


	os::sc_syslog_msg tm_syslog(SC_SYSLOG_TRANSPORT_ADAPTER_TAG,
		"TM", NULL);
#ifdef USE_LDI
	// The driver attach scheme has changed in Solaris 10. The device needs
	// to be explicitly opened for the device to be attached. Hence the
	// hack  to open the device. The code to get the dev_t will have to
	// be moved sci into the config_ops structure.
	//

	major = ddi_name_to_major("sci");
	inst = clconf_adapter_get_device_instance(cl_adp);
	minor = 1 + inst * 8;
	devt = makedevice(major, minor);
	ldi_ident_from_dev(devt, &lid);

	if ((ret = ldi_open_by_dev(&devt, OTYP_CHR, FREAD, CRED(),
	    &lhp, lid)) != 0) {
		TM_DBG(("idi_open_by_dev returned %d (name is %s,"
		    "inst %d)\n", ret,
		    clconf_adapter_get_device_name(cl_adp), inst));
		ldi_ident_release(lid);
		return (rc);
	}
#endif

	if (tm_ops && tm_ops->config_add_adapter) {
		ret = (tm_ops->config_add_adapter)(
		    (clconfig_get_property_t)clconf_obj_get_property, cl_adp);
		if (ret == 0)
			rc = true;
		else {
			int instance;
			instance = clconf_adapter_get_device_instance(cl_adp);
			TM_DBG(("sci config add adapter returned %d\n", ret));
			//
			// SCMSGS
			// @explanation
			// The Sun Cluster Topology Manager (TM) has failed to
			// add the SCI adapter.
			// @user_action
			// Make sure that the SCI adapter is installed
			// correctly on the system or contact your authorized
			// Sun service provider for assistance.
			//
			(void) tm_syslog.log(SC_SYSLOG_WARNING, MESSAGE,
				"Failed to add the sci%d adapter\n",
				instance);
		}

	}
	return (rc);
}

//
// Called after an SCI device done using.
//
void
tm_sci_device::cleanup_point(tm_point *pt)
{
	clconfig_ops_t		*tm_ops = &ops;
	int			ret;
	clconf_adapter_t	*cl_adp = pt->get_adp();
	os::sc_syslog_msg tm_syslog(SC_SYSLOG_TRANSPORT_ADAPTER_TAG,
		"TM", NULL);

	if (tm_ops && tm_ops->config_remove_adapter) {
		ret = (tm_ops->config_remove_adapter)(
		    (clconfig_get_property_t)clconf_obj_get_property, cl_adp);
		if (ret != 0) {
			int instance;
			instance = clconf_adapter_get_device_instance(cl_adp);
			TM_DBG(("sci config remove adapter returned %d\n",
				ret));
			//
			// SCMSGS
			// @explanation
			// The Sun Cluster Topology Manager (TM) has failed to
			// remove the SCI adapter.
			// @user_action
			// Make sure that the SCI adapter is installed
			// correctly on the system or contact your authorized
			// Sun service provider for assistance.
			//
			(void) tm_syslog.log(SC_SYSLOG_WARNING, MESSAGE,
				"Failed to remove sci%d adapter\n", instance);
		}
	}
#ifdef USE_LDI
	ldi_ident_release(lid);
	ldi_close(lhp, FREAD, CRED());
#endif
}

//
// Called before a path is established between a pair of SCI adapters.
//
void
tm_sci_device::setup_path(clconf_path_t *cl_path, clconf_cluster_t *cl)
{
	clconfig_ops_t 	*tm_ops = &ops;
	clconf_adapter_t *s = clconf_path_get_adapter(cl_path, CL_LOCAL, cl);
	clconf_adapter_t *d = clconf_path_get_adapter(cl_path, CL_REMOTE, cl);
	os::sc_syslog_msg tm_syslog(SC_SYSLOG_TRANSPORT_ADAPTER_TAG,
		"TM", NULL);

	if (tm_ops && tm_ops->config_add_path) {
		int ret = (tm_ops->config_add_path)(
		    (clconfig_get_property_t)clconf_obj_get_property, s, d);
		if (ret != 0) {
			int instance;
			instance = clconf_adapter_get_device_instance(s);
			TM_DBG(("sci config_add_path returned %d\n", ret));
			//
			// SCMSGS
			// @explanation
			// The Sun Cluster Topology Manager (TM) has failed to
			// add or remove a path using the SCI adapter.
			// @user_action
			// Make sure that the SCI adapter is installed
			// correctly on the system. Also ensure that the
			// cables have been setup correctly. If required
			// contact your authorized Sun service provider for
			// assistance.
			//
			(void) tm_syslog.log(SC_SYSLOG_WARNING, MESSAGE,
				"Failed to configure sci%d adapter\n",
				instance);
		}
	}
}

//
// Called after a path is torn between a pair of SCI adapters.
//
void
tm_sci_device::cleanup_path(clconf_path_t *cl_path, clconf_cluster_t *cl)
{
	clconfig_ops_t 	*tm_ops = &ops;
	clconf_adapter_t *s = clconf_path_get_adapter(cl_path, CL_LOCAL, cl);
	clconf_adapter_t *d = clconf_path_get_adapter(cl_path, CL_REMOTE, cl);
	os::sc_syslog_msg tm_syslog(SC_SYSLOG_TRANSPORT_ADAPTER_TAG,
		"TM", NULL);

	if (tm_ops && tm_ops->config_remove_path) {
		int ret = (tm_ops->config_remove_path)(
		    (clconfig_get_property_t)clconf_obj_get_property, s, d);
		if (ret != 0) {
			int instance;
			instance = clconf_adapter_get_device_instance(s);
			TM_DBG(("sci config_remove_path returned %d\n", ret));
			(void) tm_syslog.log(SC_SYSLOG_WARNING, MESSAGE,
				"Failed to configure sci%d adapter\n",
				instance);
		}
	}
}
