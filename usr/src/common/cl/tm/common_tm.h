/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _COMMON_TM_H
#define	_COMMON_TM_H

#pragma ident	"@(#)common_tm.h	1.9	08/05/20 SMI"

#include <sys/os.h>
#include <sys/clconf_int.h>
#include <sys/list_def.h>
#include <sys/heartbeat.h>
#include <orb/debug/haci_debug.h>

#define	SCIDEVICE	"sci"

#ifdef _KERNEL_ORB
#if defined(HACI_DBGBUFS) && !defined(NO_TM_DBGBUF)
#define	TM_DBGBUF
#endif
#endif

#ifdef  TM_DBGBUF
extern dbg_print_buf tm_dbg;
#define	TM_DBG(a) HACI_DEBUG(ENABLE_TM_DBG, tm_dbg, a)
#else
#define	TM_DBG(a)
#endif

#ifdef _KERNEL
#define	STOINT(x)	stoi((char **)&x)
#else
#define	STOINT(x)	atoi(x)
#endif

//		Topology Manager Basics:

// The Topology manager is responsible for creating paths from the given
// topology, stored in the infrastructure file. Infrastructure file
// contains the topology information like the nodes in the cluster,
// adapters in each node, blackbox/junctions used to connect the adapters,
// and the cable information specifying which adapter is connected which
// adapter/junction.

// Topology Manager gets this infrastructure information from the clconf
// tree and first creates domains as follows:
//	1. a new domain for adapters connected directly to each other
//	   (point-to-point cable connections)
//	2. a new domain for each junction and the adapters connected to
//	   that junction.

// Once the domains are created, paths are created between the adapters
// in the same domain. See kernel topology manager and user topology
// manager header files for more information on the paths and domains.

extern bool is_same_property(const char *, const char *);

//
// tm_point is the TM representation of clconf adapter.
//
class tm_point {
public:
	tm_point(clconf_port_t *);
	virtual ~tm_point();

	// Gets the nodename
	const char 		*get_nodename();
	// Gets the adaptername
	const char 		*get_adpname();
	// Gets the device name
	const char 		*get_devname();

	// Returns true if this point and the point passed has argument
	// have the same node name and same adapter name
	bool			is_samename(tm_point *);

	// Compares the properties of the two adapters in the points
	// and retuns a bitmask of the properties that are different.
	uint_t			compare_properties(tm_point *);

	// Gets the pointer to the adapter
	clconf_adapter_t	*get_adp();
	// Gets the pointer to the port
	clconf_port_t		*get_port();

	// Gets the heartbeat quantum from the adapter pointer
	uint_t			get_hbq();
	// Gets the heartbeat timeout from the adapter pointer
	uint_t			get_hbtout();

	// Get point changes
	uint_t			get_changes();
	// set changes to new value
	void 			set_changes(uint_t);

	// Updates the point's adapter and port pointers with the
	// new point's adapter and port pointers
	void			update(tm_point *);

	// These are kept here since there is no better place to
	// keep them without unnecessarily complicating the source code
	// These three functions are needed for SCI
	virtual	void		calculate_new_adpid(uint_t *);
	virtual void		get_existing_adpid(uint_t *);
	virtual void		set_targets(clconf_port_t *);

	// Accessor functions
	uint_t			get_in_pathcount();
	void			inc_in_pathcount();
	void			dec_in_pathcount();
	void			set_in_pathcount(uint_t);

	uint_t			get_setup_pathcount();
	void			inc_setup_pathcount();
	void			dec_setup_pathcount();
protected:
	// in_path_count keeps track of number of paths created through
	// the point. The path count is incremented everytime the
	// tm_point is used as the src or dst of a path. Note that this
	// count does not refer to the number of paths that have been
	// setup using the point
	uint_t			in_path_count;

	// setup_path_count keeps track of the number of paths using
	// this tm_point that have been actually been setup using
	// setup_path.
	uint_t			setup_path_count;

	// Bit mask of the changes between old and new incarnation of the point
	uint_t			changes;

	clconf_port_t		*port;		// pointer to clconf port
	clconf_adapter_t	*adp;		// pointer to clconf adapter
	char 			*node_name;	// Node name
	char 			*adp_name;	// Adapter name
	char			*dev_name;	// Device name
	int			dev_inst;	// Device instance
};

//
// tm_domain is used by the TM to create paths. Each domain
// contains a list of points and paths are established between the
// points in the same domain.
// Also currently, only one adapter(or tm_point) from each node can
// in one domain. (This might change with InfiniBand)
//
class tm_domain {
public:
	tm_domain(clconf_obj_t *);
	virtual ~tm_domain();

	// Add a tm_point to the list of points.
	void 			insert_point(tm_point *);
	// Returns reference to the domains' point list.
	SList<tm_point>		&get_points();

	// Returns true if the blackbox exists in the domain
	bool			has_bb(clconf_obj_t *);
	// Adds a blackbox to the domain
	void			add_bb(clconf_obj_t *);

protected:
	// List of points in the domain
	SList<tm_point>		point_list;
	// List of blackboxes in the domain.
	SList<clconf_obj_t>	bb_list;
};

class tm_path {
public:
	enum { P_ADD = 0x1,	// Path is just added
	    P_DELETE = 0x2,	// Path should be deleted before any paths are
				// added
	    P_INVALID = 0x4,	// Path is invalid and should be deleted. It
				// usually a good idea to delete these paths
				// after any new paths are added
	    P_UP = 0x8 		// path is UP. used currently only by user TM
	};

	tm_path(tm_point *, tm_point *, char *);
	~tm_path();

	// returns true if path's name is made of the two points that
	// are passed in otherwise, returns false.
	bool			is_samename(tm_point *, tm_point *);

	// returns true if the destination and source nodes in the path
	// and the path that is passed in are same
	bool			is_samenodes(tm_path *);

	// returns a bit mask of properties that are different between
	// the path's source point and the first argument ORed with
	// the difference in the properties between the path's destination
	// point and the second argument
	uint_t			compare_properties(tm_point *, tm_point *);

	// returns the source point
	tm_point		*get_src();
	// returns the destination point
	tm_point		*get_dst();
	// returns the name of the path
	const char 		*get_name();

	// updates the source and destination points
	void			update(tm_point *, tm_point *);

	// routines to get/set path state
	void			set_state(uint_t);
	uint_t			get_state();
	bool			marked_remove();
	bool			is_up();

	// sets "changes" to the argument that is passed.
	bool			is_changed();

	// pointer to the clconf_path associated with this tm path
	clconf_path_t		*cl_path;

private:
	// Pointer to the source tm point
	tm_point		*src;
	// Pointer to the destination tm point
	tm_point		*dst;
	// Name of the path
	char			*name;
	// State of the path, one of the enumerated values above
	int			state;
};


// Common topology manager class - Abstract class
// (Made abstract explicitly so that no objects are created from this)
//
class topology_manager  {
public:
	topology_manager();
	virtual			~topology_manager();

protected:
	// creates a path between the two points
	void			create_path(tm_point *, tm_point *);

	// returns true if the changed properties would result in
	// re-establishing the path between the points by tearing
	// the path down first otherwise returns false.
	bool			needs_repath(uint_t);

	// Creates domains from the clconf tree that is passed in
	void			create_domains(clconf_cluster_t *);

	// Creates paths from the domains in the dom_list.
	void			create_paths();

	// Creates a new domain for the cable or adds the endpoints to an
	// existing domain. See the TM basics above for the rules which
	// would result in creating a new domain.
	void			setup_domain(clconf_cable_t *);

	// Returns pointer to the domain given the black box pointer
	tm_domain 		*get_domain_by_bb(clconf_obj_t *);

	// If blackboxes are connected to each other, this routine
	// puts them in the same domain, instead of creating a new one.
	// Currently scconf does not support connecting switch to switch.
	// Talk to John cummings for more details.
	void 			merge_switches(clconf_port_t *,
						clconf_port_t *);

	// Creates a point for the port if the port is an adapter port.
	// Otherwise returns NULL.
	tm_point 		*get_tm_point(clconf_port_t *, bool &,
						clconf_objtype_t &);

	// Searches all the paths for the point, if the point exists then
	// returns pointer to it, otherwise returns NULL
	tm_point		*get_point_from_paths(tm_point *);

	// Virtual functions
	// There is no reason for them to be either pure virtual functions
	// or not. Just made one of them as pure virtual to make sure that
	// no objects are created from this class.

	// Definition of path is slightly different for kernel TM and user TM
	// Kernel TM creates paths that are originating from the node which
	// the Kernel TM is running on, where as the user TM need to create
	// paths between all the nodes in the cluster.
	virtual	bool		valid_path(tm_point *, tm_point *) = 0;

	// path name is different in kernel and user TM. In fact kernel TM
	// does not even require path name, except for debug messages.
	// Kernel TM path is of form <node>:<adapter> - <node>:<adapter>
	// User TM path is of form
	// cluster.nodes.<node#>.adapters.<adp#>.nodes.<node#>,adapters.<adp#>
	virtual char		*make_pathname(tm_point *, tm_point *);

	// User TM creates points based on their device name
	virtual tm_point	*new_point(clconf_port_t *, const char *);

	// User TM creates its own domains.
	virtual tm_domain	*new_domain(clconf_obj_t *);

	SList<tm_domain>	dom_list; 	// list of domains
	SList<tm_path>		path_list;	// list of paths
};
#include <tm/common_tm_in.h>

#endif	/* _COMMON_TM_H */
