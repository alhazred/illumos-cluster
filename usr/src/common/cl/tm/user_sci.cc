//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident   "@(#)user_sci.cc 1.4     08/05/20 SMI"

#include <tm/user_tm.h>
#include <tm/user_sci.h>

//
// Constructor for sci point used by User TM
// sci point is needed to keep track of traget port ids and then use this
// target port id information while calculating the SCI adapter ids (RSM hard-
// ware addresses).
//
tm_sci_point::tm_sci_point(clconf_port_t *cl_port) :
    tm_point(cl_port)
{
	int nodeid = clconf_obj_get_id(clconf_obj_get_parent(
	    (clconf_obj_t*)adp));
	tgt_pid = (nodeid) ? (nodeid - 1) : 0;
}

tm_sci_point::~tm_sci_point()
{
}

//
// target pid of a point is the port name of the blackbox to which it is
// connected. If an adapter is directly connected to another adapter then
// the target pid is set to nodeid in the SCI constructor
//
void
tm_sci_point::set_targets(clconf_port_t *p)
{
	clconf_obj_t	*parent = clconf_obj_get_parent((clconf_obj_t *)p);
	const char *pname = clconf_obj_get_name((clconf_obj_t *)p);

	ASSERT(clconf_obj_get_objtype(parent) == CL_BLACKBOX);
	tgt_pid = (pname) ? STOINT(pname) : 0;
}

//
// Gets the existing adapter id of an adapter and sets the corresponding bit
// in the ass_aids , so that same adapter id is not assigned again when a
// new adapter id is assigned to a new adapter.
//
void
tm_sci_point::get_existing_adpid(uint_t *ass_aids)
{
	int port_id;
	const char *adp_idstr;
	uint_t aid;

	adp_idstr = clconf_obj_get_property((clconf_obj_t *)adp, "adapter_id");
	if (adp_idstr == NULL)
		return;
	aid = (uint_t)STOINT(adp_idstr);
	port_id = tgt_pid;
	if ((aid > 0) && (port_id >= 0) && (port_id < MAX_PORTS) &&
	    (ADP_ID_TO_PORT(aid) == port_id) &&
	    (is_entry_in_use(aid / 4, 8 * UINT_SIZE, ass_aids) == 0)) {
		if (set_entry_in_use(aid/4, 8 * UINT_SIZE, ass_aids) == 0)
			ASSERT(0);
	} else {
	    (void) clconf_obj_set_property((clconf_obj_t *)adp,
		"adapter_id", NULL);
	}
}

//
// Assigns the next available adapter id in the port range
//
void
tm_sci_point::calculate_new_adpid(uint_t *ass_aids)
{
	uint_t inx, inst, aid;
	char new_adp_id[20];
	int portid;
	const char *adp_idstr;

	adp_idstr = clconf_obj_get_property((clconf_obj_t *)adp, "adapter_id");
	if (adp_idstr != NULL)
		return;
	portid = tgt_pid;
	inx = inst = (uint_t)clconf_adapter_get_device_instance(adp) %
						MAX_PORT_RANGE;
	do {
		aid = CREATE_SCI_ADP_ID(portid, inx++);
		if (is_entry_in_use(aid / 4, 8 * UINT_SIZE, ass_aids) == 0)
				break;
		inx %= MAX_PORT_RANGE;
	} while (inx != inst);
	ASSERT(inx != inst);
	if (set_entry_in_use(aid / 4, 8 * UINT_SIZE, ass_aids) == 0)
		ASSERT(0);
	if (os::snprintf(new_adp_id, 20, "%ld", aid) >= 20)
		ASSERT(0);
	(void) clconf_obj_set_property((clconf_obj_t *)adp,
	    "adapter_id", (const char *)new_adp_id);
}
