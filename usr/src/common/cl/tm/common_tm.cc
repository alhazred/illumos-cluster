/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident	"@(#)common_tm.cc	1.8	08/05/20 SMI"

#include <tm/common_tm.h>

topology_manager::topology_manager()
{
}

topology_manager::~topology_manager()
{
}

//
// Creates domains from the clconf tree "cl".
// Only enabled cables are used to create domains.
//
void
topology_manager::create_domains(clconf_cluster_t *cl)
{
	clconf_iter_t		*iter;
	clconf_cable_t		*cl_cable;

	TM_DBG(("creating domains\n"));
	iter = clconf_cluster_get_cables(cl);
	for (; (cl_cable = (clconf_cable_t *)
	    clconf_iter_get_current(iter)) != NULL;
	    clconf_iter_advance(iter)) {
		if (!clconf_obj_enabled((clconf_obj_t *)cl_cable))
			continue;
		setup_domain(cl_cable);
	}
	clconf_iter_release(iter);
}

//
// Creates paths from the dom_list. For each domain in the dom_list,
// if any points make a valid path, then the path is created.
// Definition of valid path is different for kernel TM and user TM.
//
void
topology_manager::create_paths()
{
	SList<tm_domain>::ListIterator	d_iter(dom_list);
	tm_domain			*dom;
	tm_point			*pt1, *pt2;

	for (; (dom = d_iter.get_current()) != NULL; d_iter.advance()) {
	    SList<tm_point>::ListIterator	iter1(dom->get_points());

	    for (; (pt1 = iter1.get_current()) != NULL; iter1.advance()) {
		SList<tm_point>::ListIterator iter2(dom->get_points());

		for (; (pt2 = iter2.get_current()) != NULL; iter2.advance()) {
			if (!valid_path(pt1, pt2))
				continue;
			create_path(pt1, pt2);
		}
	    }
	}
}

//
// returns the point from the path_list which has the same nodename and
// adapter name as "pt".
//
tm_point *
topology_manager::get_point_from_paths(tm_point *pt)
{
	tm_path	*path;
	tm_point *point;

	for (path_list.atfirst(); (path = path_list.get_current()) != NULL;
	    path_list.advance()) {
		point = path->get_src();
		if (point->is_samename(pt))
			return (point);
		point = path->get_dst();
		if (point->is_samename(pt))
			return (point);
	}
	return (NULL);
}

// Determines if the changes cp should result in tearing down the path
// and re-establishing it.
bool
topology_manager::needs_repath(uint_t cp)
{
	if (cp & (AP_IP_ADDRESS|AP_NETMASK|AP_PORTS))
		return (true);
	return (false);
}

//
// Creates a path if one already does not exists between the points point1 and
// point2. If one already exists, then it checks to see if any of the
// properties of the adapters that make the path changed since the last
// time. If properties are not changed, then the src and dst points of the
// existing path are updated with the new points. If properties are changed
// then needs_repath() is called to check the change in properties would
// result in tearing down the path and re-establishing it. if not, "changes"
// member variable is set and src & dst are updated with the new points
// If it paths need to be torn down then they are marked for deletion.
//
void
topology_manager::create_path(tm_point *point1, tm_point *point2)
{
	tm_path		*path;
	uint_t		ch;
	tm_point	*pt1, *pt2;

	for (path_list.atfirst(); (path = path_list.get_current()) != NULL;
	    path_list.advance()) {
		if (path->is_samename(point1, point2)) {
			TM_DBG(("path %s\n", path->get_name()));
			ch = path->compare_properties(point1, point2);
			if (ch == AP_NONE) {
				TM_DBG(("\t\t... exists with same prps!!\n"));
				path->update(point1, point2);
				return;
			} else {
				TM_DBG(("\t\t... changed. changes = %x\n", ch));
				if (needs_repath(ch)) {
					path->set_state(tm_path::P_DELETE);
					break;
				} else {
					path->update(point1, point2);
					return;
				}
			}
		}
	}

	if ((pt1 = get_point_from_paths(point1)) != NULL) {
		pt1->update(point1);
	} else
		pt1 = point1;
	if ((pt2 = get_point_from_paths(point2)) != NULL) {
		pt2->update(point2);
	} else
		pt2 = point2;

	// make the path name before the path is created. Memory for
	// the name is allocated by make_pathname and tm_path destructor will
	// free the memory.
	// path name is different for kernel TM and user TM.
	char *pname = make_pathname(pt1, pt2);
	path_list.append(new tm_path(pt1, pt2, pname));
}

//
// This function makes up the default path name, which is of the format
// 		node1:adp1 - node2:adp1
//
char *
topology_manager::make_pathname(tm_point *s, tm_point *d)
{
	char *name;

	size_t size =  strlen(s->get_nodename()) + strlen(s->get_adpname()) +
		strlen(d->get_nodename()) + strlen(d->get_adpname()) +
		strlen(": - :") + 1;
	name = new char[size];
	os::sprintf(name, "%s:%s - %s:%s", s->get_nodename(), s->get_adpname(),
	    d->get_nodename(), d->get_adpname());
	return (name);
}

//
// creates a point for the port if the port is an adapter port and sets
// success to true. If the port is a blackbox port then returns NULL but
// sets the success to true. If any of the port/adapter/blackbox/node is
// is disabled then returns NULL and sets success to false.
// We do a lot of assertions here because, they should be true. verifications
// in this regard should be done in the userland before committing a new
// tree to the CCR.
//
tm_point *
topology_manager::get_tm_point(clconf_port_t *cl_cab_port,
    bool &success, clconf_objtype_t &type)
{
	const char		*port_name, *p_name, *gp_name, *dname;
	clconf_obj_t		*parent, *gparent;
	clconf_objtype_t	p_type;
	tm_point		*new_pt;

	success = false;
	if (!clconf_obj_enabled((clconf_obj_t *)cl_cab_port)) {
		return (NULL);
	}
	port_name = clconf_obj_get_name((clconf_obj_t *)cl_cab_port);
	ASSERT(port_name);
	parent = clconf_obj_get_parent((clconf_obj_t *)cl_cab_port);
	ASSERT(parent);
	p_type = clconf_obj_get_objtype(parent);
	if (!clconf_obj_enabled((clconf_obj_t *)parent)) {
		return (NULL);
	}
	// ASSERT((p_type == CL_ADAPTER) || (p_type == CL_BLACKBOX))
	p_name = clconf_obj_get_name(parent);
	ASSERT(p_name);
	type = p_type;
	if (p_type == CL_ADAPTER) {
		/* grandparent is clconf_node_t */
		gparent = clconf_obj_get_parent(parent);
		ASSERT(gparent);
		gp_name = clconf_obj_get_name(gparent);
		ASSERT(gp_name);
		if (!clconf_obj_enabled((clconf_obj_t *)gparent)) {
			return (NULL);
		}
		dname = clconf_adapter_get_device_name(
		    (clconf_adapter_t *)parent);
		if (dname == NULL)
			return (NULL);

		if ((new_pt = new_point(cl_cab_port, dname)) != NULL) {
			success = true;
		}
		return (new_pt);
	} else {
		success = true;
		return (NULL);
	}
}

//
// Merge switches by adding the second blackbox to the first domain
// and if there is already domain for the second blackbox then move all
// points in that domain to the first domain and free up the second domain.
//
void
topology_manager::merge_switches(clconf_port_t *p1, clconf_port_t *p2)
{
	tm_domain	*dom;
	clconf_obj_t	*parent1, *parent2;

	parent1 = clconf_obj_get_parent((clconf_obj_t *)p1);
	parent2 = clconf_obj_get_parent((clconf_obj_t *)p2);
	dom = get_domain_by_bb(parent1);
	if (dom == NULL) {
		dom = new_domain(parent1);
		dom_list.prepend(dom);
	}
	dom->add_bb(parent2);

	// If the second blackbox has a domain already, remove it.
	dom = get_domain_by_bb(parent2);
	if (dom != NULL) {
		(void) dom_list.erase(dom);
	}
}

//
// Creates points for the two endpoints of the cable and then creates
// a new domain or inserts into an existing domain based on the following rules
// 	1. If both the endpoints are adapters then a new domain is created
//	   and both the points are inserted into it.
//	2. If one of the endpoint is a blackbox then creates a new domain
//	   for the blckbox if one already does not exist and inserts the
//	   adapter point in to the domain
//	3. If both the endpoints are blackboxes then merges them.
//
void
topology_manager::setup_domain(clconf_cable_t *cl_cable)
{
	tm_point 		*pt1, *pt2;
	clconf_objtype_t	type1, type2;
	bool			success;
	tm_domain		*dom;
	clconf_port_t		*cl_cab_port1, *cl_cab_port2;
	clconf_obj_t		*parent;

	cl_cab_port1 = clconf_cable_get_endpoint(cl_cable, 1);
	cl_cab_port2 = clconf_cable_get_endpoint(cl_cable, 2);
	ASSERT(cl_cab_port1);
	ASSERT(cl_cab_port2);
	pt1 = get_tm_point(cl_cab_port1, success, type1);
	if (!success)
		return;
	pt2 = get_tm_point(cl_cab_port2, success, type2);
	if (!success) {
		if (pt1 != NULL)
			delete pt1;
		return;
	}
	if (type1 == CL_ADAPTER) {
		if (type2 == CL_ADAPTER) {
			dom = new_domain(NULL);
			dom_list.prepend(dom);
			dom->insert_point(pt1);
			dom->insert_point(pt2);
		} else {		// adp <-> bb
			parent = clconf_obj_get_parent(
			    (clconf_obj_t *)cl_cab_port2);
			pt1->set_targets(cl_cab_port2);
			dom = get_domain_by_bb(parent);
			if (dom == NULL) {
				dom = new_domain(parent);
				dom_list.prepend(dom);
			}
			dom->insert_point(pt1);
		}
	} else {
		if (type2 == CL_ADAPTER) {	// bb <-> adp
			parent = clconf_obj_get_parent(
			    (clconf_obj_t *)cl_cab_port1);
			pt2->set_targets(cl_cab_port1);
			dom = get_domain_by_bb(parent);
			if (dom == NULL) {
				dom = new_domain(parent);
				dom_list.prepend(dom);
			}
			dom->insert_point(pt2);
		} else {
			/*
			 * Currently, only supported blackbox is switch
			 */
			merge_switches(cl_cab_port1, cl_cab_port2);
		}
	}
}

//
// returns the domain which has the blackbox "bb"
//
tm_domain *
topology_manager::get_domain_by_bb(clconf_obj_t *bb)
{
	tm_domain	*dom;

	for (dom_list.atfirst(); (dom = dom_list.get_current()) != NULL;
	    dom_list.advance()) {
		if (dom->has_bb(bb))
			return (dom);
	}
	return (NULL);
}

tm_domain *
topology_manager::new_domain(clconf_obj_t *bb)
{
	return (new tm_domain(bb));
}

tm_point *
topology_manager::new_point(clconf_port_t *p, const char *)
{
	return (new tm_point(p));
}

//
// Constructor for tm_point
//
tm_point::tm_point(clconf_port_t *cl_port) :
    in_path_count(0),
    setup_path_count(0),
    changes(0),
    port(cl_port)
{
	clconf_obj_t *node;

	adp = (clconf_adapter_t *)
	    clconf_obj_get_parent((clconf_obj_t *)cl_port);
	ASSERT(adp != NULL);
	adp_name = os::strdup(clconf_obj_get_name((clconf_obj_t *)adp));
	node = clconf_obj_get_parent((clconf_obj_t *)adp);
	node_name = os::strdup(clconf_obj_get_name(node));
	dev_name = os::strdup(clconf_adapter_get_device_name(adp));
	dev_inst = clconf_adapter_get_device_instance(adp);
}

tm_point::~tm_point()
{
	ASSERT(in_path_count == 0);
	ASSERT(setup_path_count == 0);
	delete []node_name;
	delete []adp_name;
	delete []dev_name;
	port = NULL;
	adp = NULL;
}

bool
tm_point::is_samename(tm_point *pt)
{
	if ((os::strcmp(node_name, pt->get_nodename()) == 0) &&
	    (os::strcmp(adp_name, pt->get_adpname()) == 0)) {
		return (true);
	}
	return (false);
}

//
// returns the heartbeat quantum of the point(adapter)
//
uint_t
tm_point::get_hbq()
{
	const char	 *transp, *qval;
	char		*qstr;
	size_t		size;
	uint_t		ret = 0;

	transp = clconf_adapter_get_transport_type(adp);
	size = strlen(transp) + strlen("_heartbeat_quantum") + 1;
	qstr = new char[size];
	os::sprintf(qstr, "%s_heartbeat_quantum", transp);
	qval = clconf_obj_get_property((clconf_obj_t *)adp, qstr);
	delete []qstr;
	if (qval != NULL)
		ret = (uint_t)STOINT(qval);
	if (ret == 0)
		ret = HB_EMERGENCY_DEFAULT_QUANTUM;
	return (ret);
}

//
// returns the heartbeat timeout value of the point(adapter)
//
uint_t
tm_point::get_hbtout()
{
	const char	 *transp, *tval;
	char		*tstr;
	size_t		size;
	uint_t		ret = 0;

	transp = clconf_adapter_get_transport_type(adp);
	size = strlen(transp) + strlen("_heartbeat_timeout") + 1;
	tstr = new char[size];
	os::sprintf(tstr, "%s_heartbeat_timeout", transp);
	tval = clconf_obj_get_property((clconf_obj_t *)adp, tstr);
	delete []tstr;
	if (tval != NULL)
		ret = (uint_t)STOINT(tval);
	if (ret == 0)
		ret = HB_EMERGENCY_DEFAULT_TIMEOUT;
	return (ret);
}

//
// Compares the point's adapter with pt's adapter and returns the differences
// as a bitmask.
//
uint_t
tm_point::compare_properties(tm_point *pt)
{
	clconf_iter_t		*oiter, *niter;
	clconf_port_t		*old_port = NULL, *new_port = NULL;
	clconf_obj_t		*tmp_obj;
	const char		*old_property, *new_property, *prpt_name;
	uint_t			retval = AP_NONE;
	clconf_adapter_t 	*na = pt->adp;
	clconf_adapter_t	*oa = adp;

	if ((this == pt) || (oa == na)) {
		changes = AP_NONE;
		return (retval);
	}
	oiter = clconf_obj_get_properties((clconf_obj_t *)oa);
	if (oiter != NULL) {
		for (; (tmp_obj = clconf_iter_get_current(oiter)) != NULL;
		    clconf_iter_advance(oiter)) {
			prpt_name = clconf_obj_get_idstring(tmp_obj);
			old_property = clconf_obj_get_property((clconf_obj_t *)
			    oa, prpt_name);
			new_property = clconf_obj_get_property((clconf_obj_t *)
			    na, prpt_name);
			if (!is_same_property(old_property, new_property)) {
				retval |= clconf_get_pcode(prpt_name);
			}
		}
		clconf_iter_release(oiter);
	}

	niter = clconf_obj_get_properties((clconf_obj_t *)na);
	if (niter != NULL) {
		for (; (tmp_obj = clconf_iter_get_current(niter)) != NULL;
		    clconf_iter_advance(niter)) {
			prpt_name = clconf_obj_get_idstring(tmp_obj);
			old_property = clconf_obj_get_property((clconf_obj_t *)
			    oa, prpt_name);
			new_property = clconf_obj_get_property((clconf_obj_t *)
			    na, prpt_name);
			if (!is_same_property(old_property, new_property)) {
				retval |= clconf_get_pcode(prpt_name);
			}
		}
		clconf_iter_release(niter);
	}

	oiter = clconf_adapter_get_ports(oa);
	niter = clconf_adapter_get_ports(na);

	for ((old_port = (clconf_port_t *)clconf_iter_get_current(oiter)),
	    (new_port = (clconf_port_t *)clconf_iter_get_current(niter));
	    old_port && new_port;
	    clconf_iter_advance(oiter),
	    clconf_iter_advance(niter),
	    (old_port = (clconf_port_t *)clconf_iter_get_current(oiter)),
	    (new_port = (clconf_port_t *)clconf_iter_get_current(niter))) {
		/*
		 * If even one of the adp-ports has a different state,
		 * we need to remove the adapter and add the new one.
		 * Adapter has changed physically.  We take this
		 * approach because there isn't a one-to-one mapping
		 * between adp state and the states of its various ports.
		 */
	    if (strcmp(clconf_obj_get_name((clconf_obj_t *)old_port),
		clconf_obj_get_name((clconf_obj_t *)new_port)) != 0) {
			retval |= clconf_get_pcode("ports");
			break;
	    }
	    if (clconf_obj_enabled((clconf_obj_t *)old_port) !=
		clconf_obj_enabled((clconf_obj_t *)new_port)) {
			retval |= clconf_get_pcode("ports");
			break;
	    }
	}
	/*
	 * Number of ports in old and new adapters should be same
	 */
	if (old_port || new_port)
		retval |= clconf_get_pcode("ports");
	clconf_iter_release(oiter);
	clconf_iter_release(niter);

	changes = retval;
	return (retval);
}

// tm_domain constructor
tm_domain::tm_domain(clconf_obj_t *bb)
{
	TM_DBG(("created domain %p with bb=%p\n", this, (bb)?bb:0));
	if (bb != NULL)
		bb_list.append(bb);
}

tm_domain::~tm_domain(void)
{
	tm_point	*pt;

	// Delete only the invalid points.(points which are not part of any
	// path)
	while ((pt = point_list.reapfirst()) != NULL) {
		if (pt->get_in_pathcount() == 0)
			delete pt;
	}
	while (bb_list.reapfirst() != NULL)
		;
}

// tm_path constructor
// memory for the path name is pre-allocated, needs to be freed by the
// destructor
tm_path::tm_path(tm_point *s, tm_point *d, char *pname) :
    cl_path(NULL),
    src(s),
    dst(d),
    name(pname),
    state(P_ADD)
{
	// Increment pathcounts for src and dst tm_points.
	src->inc_in_pathcount();
	dst->inc_in_pathcount();
}

tm_path::~tm_path()
{
	delete []name;
	src->dec_in_pathcount();
	if (src->get_in_pathcount() == 0) {
		ASSERT(src->get_setup_pathcount() == 0);
		delete src;
	}
	dst->dec_in_pathcount();
	if (dst->get_in_pathcount() == 0) {
		ASSERT(dst->get_setup_pathcount() == 0);
		delete dst;
	}
	src = dst = NULL;
	cl_path = NULL;
}

bool
tm_path::is_samenodes(tm_path *pth)
{
	tm_point *s = pth->get_src();
	tm_point *d = pth->get_dst();

	if ((strcmp(src->get_nodename(), s->get_nodename()) == 0) &&
	    (strcmp(dst->get_nodename(), d->get_nodename()) == 0))
		return (true);
	return (false);
}

//
// This is not associated with any class. General util function.
// Return true - if old_property == new_property
// Return false - if old_property not equal to new_property
//

bool
is_same_property(const char *old_property, const char *new_property)
{
	if (old_property && new_property) {
	    if (strcmp(old_property, new_property)) {
		return (false);
	    }
	} else if (old_property || new_property) {
	    return (false);
	}
	return (true);
}
