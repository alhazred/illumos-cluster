/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  ommon_tm_in.h
 *
 */

#ifndef _COMMON_TM_IN_H
#define	_COMMON_TM_IN_H

#pragma ident	"@(#)common_tm_in.h	1.6	08/05/20 SMI"

inline void
tm_domain::insert_point(tm_point *pt)
{
	point_list.append(pt);
}

inline SList<tm_point> &
tm_domain::get_points()
{
	return (point_list);
}

// Returns true if "obj" is one of the bbs in the domain's bb list.
inline bool
tm_domain::has_bb(clconf_obj_t *obj)
{
	clconf_obj_t *bb;

	for (bb_list.atfirst(); (bb = bb_list.get_current()) != NULL;
	    bb_list.advance())
		if (bb == obj)
			return (true);
	return (false);
}

inline void
tm_domain::add_bb(clconf_obj_t *bb)
{
	bb_list.append(bb);
}

inline const char *
tm_point::get_nodename(void)
{
	return (node_name);
}

inline const char *
tm_point::get_adpname(void)
{
	return (adp_name);
}

inline const char *
tm_point::get_devname(void)
{
	return (dev_name);
}

inline uint_t
tm_point::get_changes()
{
	return (changes);
}

inline void
tm_point::set_changes(uint_t new_val)
{
	changes = new_val;
}

inline uint_t
tm_point::get_in_pathcount()
{
	return (in_path_count);
}

inline void
tm_point::set_in_pathcount(uint_t newval)
{
	in_path_count = newval;
}

inline void
tm_point::inc_in_pathcount()
{
	in_path_count++;
}

inline void
tm_point::dec_in_pathcount()
{
	in_path_count--;
}

inline uint_t
tm_point::get_setup_pathcount()
{
	return (setup_path_count);
}

inline void
tm_point::inc_setup_pathcount()
{
	setup_path_count++;
}

inline void
tm_point::dec_setup_pathcount()
{
	setup_path_count--;
}

inline bool
tm_path::is_samename(tm_point *s, tm_point *d)
{
	return ((src->is_samename(s)) && (dst->is_samename(d)));
}

inline uint_t
tm_path::compare_properties(tm_point *s, tm_point *d)
{
	if (src->get_changes() == 0)
		(void) src->compare_properties(s);
	if (dst->get_changes() == 0)
		(void) dst->compare_properties(d);
	return (src->get_changes() | dst->get_changes());
}

inline tm_point *
tm_path::get_src()
{
	return (src);
}

inline tm_point *
tm_path::get_dst()
{
	return (dst);
}

inline const char *
tm_path::get_name()
{
	return (name);
}

inline bool
tm_path::marked_remove()
{
	return ((state & (P_DELETE|P_INVALID)) != 0);
}

inline bool
tm_path::is_up()
{
	return ((state & P_UP) != 0);
}

inline bool
tm_path::is_changed()
{
	return (src->get_changes() | dst->get_changes());
}

inline void
tm_path::set_state(uint_t new_state)
{
	state = new_state;
}

inline uint_t
tm_path::get_state()
{
	return ((uint_t)state);
}

inline clconf_port_t *
tm_point::get_port()
{
	return (port);
}

inline clconf_adapter_t *
tm_point::get_adp()
{
	return (adp);
}

inline void
tm_path::update(tm_point *s, tm_point *d)
{
	src->update(s);
	dst->update(d);
	state &= ~P_INVALID;
}

inline void
tm_point::update(tm_point *new_pt)
{
	port = new_pt->port;
	adp = new_pt->adp;
}

inline void
tm_point::get_existing_adpid(uint_t *)
{
}

inline void
tm_point::calculate_new_adpid(uint_t *)
{
}

inline void
tm_point::set_targets(clconf_port_t *)
{
}

#endif	/* _COMMON_TM_IN_H */
