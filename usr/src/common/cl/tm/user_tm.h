/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _USER_TM_H
#define	_USER_TM_H

#pragma ident	"@(#)user_tm.h	1.6	08/05/20 SMI"

#include <sys/os.h>
#include <sys/param.h>
#include <sys/ethernet.h>
#include <netdb.h>
#include <sys/clconf_int.h>
#include <netinet/in.h>
#include <sys/heartbeat.h>
#include <sys/modctl.h>
#include <sys/cmn_err.h>
#include <sys/tm_int.h>
#include <sys/list_def.h>
#include <libintl.h>
#include <locale.h>

#ifndef _KERNEL_ORB
#define	DGETTEXT(x)	dgettext(TEXT_DOMAIN, x)
#else
#define	DGETTEXT(x)	x
#endif

#include <tm/common_tm.h>

#define	BYTE_SIZE	8
#define	UINT_SIZE	(int)(BYTE_SIZE * sizeof (uint_t))

extern uint_t get_net_and_host_bits(in_addr_t, uint_t *, uint_t *);
extern int is_entry_in_use(uint_t, uint_t, uint_t *);
extern int set_entry_in_use(uint_t, uint_t, uint_t *);
extern int get_next_entry(uint_t, uint_t, uint_t *, uint_t *);

//
// A derived class of generic domain class.
// Contains member data and functions to calculate IP addresses
//

class tm_user_domain : public tm_domain {
public:
	tm_user_domain(clconf_obj_t *);
	~tm_user_domain();
	void do_init(clconf_cluster_t *);
	void use_existing_ips(uint_t *, in_addr_t, in_addr_t, uint_t);
	int calculate_new_ips(in_addr_t, uint_t *, uint_t, uint_t);
	int calculate_ipaddr(tm_point *);

private:
	// Bit mask for keeping track of host nos.
	uint_t		*ip_host_nos;
	uint_t	max_ip_host_nos;
	uint_t	max_ip_entries;

	// Subnet number and netmask
	in_addr_t	subnet_netnum;
	in_addr_t	subnet_netmask;

	uint_t		subnet_no;
	uint_t		subnet_host_bits;
};

class user_tm : public topology_manager {
public:
	static	int initialize();
		void shutdown();

	static user_tm& the();

	// called by clconf_check_transition to verify the clconf tree
	// statically
	int verify(clconf_cluster_t *, char *, uint_t);

	// Does the dynamic verification of the clconf tree just before
	// the commit and returns without commiting if the dynamic path
	// verification shows node isolation.
	int ic_update(clconf_cluster_t *);

	// Change the IP address assignments for the cluster in the
	// non-cluster mode. This function cannot be used in the cluster
	// mode. The clconf_cluster_t argument gives the new clconf
	// tree with the private_net_number and private_netmask changed
	// to the new value.
	int modify_ip_address_range(clconf_cluster_t *);

private:
	user_tm();
	~user_tm();

	// does dynamic path verification
	int 		verify_path_status(clconf_cluster_t *);

	// strips of ip_address/netmask/adapter_id off clconf_adapter
	void 		cleanup_adapter(clconf_adapter_t *);
	void 		append_err(char *, uint_t, int *, int, const char *);

	// Checks to see if the heartbeat parameters are valid
	int		check_heartbeat_parameters(heartbeat_t *, char *,
			uint_t, int *);

	// Checks to see if the path between the two points is a valid
	// User TM path
	bool 		valid_path(tm_point *, tm_point *);

	// Calculates IP address/netmask and adapter_ids
	void 		calculate_pernode(clconf_cluster_t *);

	// Calculates IP address/netmask and adapter_ids
	int 		calculate_netinfo(clconf_cluster_t *);

	// Initializes the data structures for calculating netinfo.
	void 		do_init(clconf_cluster_t *);

	// Gets the states of the current paths in the path_list.
	void 		get_path_states();

	// called by get_path_states to set a path's state
	void 		set_path_state(const char *, int);

	// returns true if the path is removed it would be the last between
	// the pair nodes.
	bool 		is_last_path(tm_path *);

	tm_domain 	*new_domain(clconf_obj_t *);
	tm_point 	*new_point(clconf_port_t *, const char *);

	// Makes a path name of format
	// cluster.nodes.#.adapters.#.nodes.#.adapters.#
	char 		*make_pathname(tm_point *, tm_point *);

	// bitmask to keep track of subnet numbers
	uint_t 		*subnet_nos;
	uint_t	max_subnet_nos;
	uint_t	max_subnet_entries;

	// bitmask to keep track of adapter ids
	uint_t 		ass_aids[8];

	// default private network number
	in_addr_t 	def_netnum;
	// default private netmask
	in_addr_t 	def_netmask;

	// default private subnet netmask
	in_addr_t 	def_subnet_netmask;

	in_addr_t 	pernode_subnet_netnum;
	in_addr_t 	pernode_subnet_netmask;

	// Used for appending error
	uint_t 		err_off;

	// Lock to synchronize the_user_tm creation.
	static os::mutex_t 	user_tmlock;

	static user_tm 		*the_user_tm;
};

#include <tm/user_tm_in.h>
#endif	/* _USER_TM_H */
