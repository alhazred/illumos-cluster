/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _USER_SCI_H
#define	_USER_SCI_H

#pragma ident	"@(#)user_sci.h	1.4	08/05/20 SMI"

#include <tm/common_tm.h>

#define	MAX_PORT_RANGE	14
#define	PORT_RANGE	4
#define	MAX_PORTS	18

#define	CREATE_SCI_ADP_ID(port, prev_aid) \
	(int)((((uint_t)(port) << PORT_RANGE) + 2 + prev_aid) << 2)
#define	ADP_ID_TO_PORT(aid)	(int)((uint_t)(aid) >> (PORT_RANGE + 2))


class tm_sci_point : public tm_point {
public:
	tm_sci_point(clconf_port_t *);
	~tm_sci_point();
	void set_targets(clconf_port_t *);
	void get_existing_adpid(uint_t *);
	void calculate_new_adpid(uint_t *);
private:
	// Port id of switch port that this SCI adapter is connected to
	int tgt_pid;
};

#endif	/* _USER_SCI_H */
