//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)user_tm.cc	1.13	08/05/20 SMI"

#include <arpa/inet.h>
#include <tm/user_tm.h>
#include <orb/infrastructure/orb_conf.h>
#include <netinet/in.h>
#ifndef _KERNEL_ORB
#include <tm/user_sci.h>
#endif

user_tm *user_tm::the_user_tm = NULL;
os::mutex_t user_tm::user_tmlock;

//
// Called by clconf_lib_init
//
int
user_tm::initialize()
{
	user_tmlock.lock();
	if (the_user_tm != NULL) {
		user_tmlock.unlock();
		return (0);
	}
	the_user_tm = new user_tm();
	if (the_user_tm == NULL) {
		user_tmlock.unlock();
		return (ENOMEM);
	}
	user_tmlock.unlock();
	return (0);
}

void
user_tm::shutdown()
{
	delete the_user_tm;
	the_user_tm = NULL;
}

user_tm::user_tm()
{
	err_off = 0;

	def_netnum = 0;
	def_netmask = 0;
	def_subnet_netmask = 0;

	subnet_nos = NULL;
	max_subnet_nos = 0;
	max_subnet_entries = 0;

	pernode_subnet_netnum = 0;
	pernode_subnet_netmask = 0;

	bzero(ass_aids, sizeof (ass_aids));
}

//
// Initializes the data structures needed to calculate IP addresses
// and adapter ids.
void
user_tm::do_init(clconf_cluster_t *Cluster)
{
	const char 	*netmask, *ip_addr;
	uint_t		total_addrs, subnet_addrs;
	uint_t		sno;
	uint_t 		i;

	err_off = 0;

	if ((netmask = clconf_obj_get_property((clconf_obj_t *)Cluster,
	    "private_netmask")) == NULL ||
	    (def_netmask = os::inet_addr(netmask)) == 0 ||
	    get_net_and_host_bits(ntohl(def_netmask), NULL, NULL)
	    == 0) {
		def_netmask = os::inet_addr(CL_DEFAULT_NETMASK);
	} else
		def_netmask = os::inet_addr(netmask);
	def_netmask = ntohl(def_netmask);

	def_netnum = 0;
	if ((ip_addr = clconf_obj_get_property((clconf_obj_t *)Cluster,
	    "private_net_number")) == NULL ||
	    (def_netnum = os::inet_addr(ip_addr)) == 0 ||
	    ((ntohl(def_netnum) & def_netmask) != ntohl(def_netnum))) {
		if (def_netnum == 0)
			def_netnum = os::inet_addr(CL_DEFAULT_NET_NUMBER);
	} else
		def_netnum = os::inet_addr(ip_addr);
	def_netnum = ntohl(def_netnum);
	def_netnum &= def_netmask;

	if ((netmask = clconf_obj_get_property((clconf_obj_t *)Cluster,
	    "private_subnet_netmask")) == NULL ||
	    (def_subnet_netmask = os::inet_addr(netmask)) == 0 ||
		get_net_and_host_bits(ntohl(def_subnet_netmask), NULL, NULL)
	    == 0) {
		def_subnet_netmask = os::inet_addr(CL_DEFAULT_SUBNET_NETMASK);
	} else
		def_subnet_netmask = os::inet_addr(netmask);
	def_subnet_netmask = ntohl(def_subnet_netmask);

	// Find the maximum number of subnets based on the total
	// number of addresses and number of addresses in a
	// subnet and initialize related data structures
	total_addrs = (~def_netmask) + 1;
	subnet_addrs = (~def_subnet_netmask) + 1;
	max_subnet_nos = total_addrs / subnet_addrs;
	max_subnet_entries = (max_subnet_nos / UINT_SIZE) + 1;

	subnet_nos = new uint_t[max_subnet_entries];
	bzero(subnet_nos, sizeof (subnet_nos));
	for (i = 0; i < max_subnet_entries; i++)
		subnet_nos[i] = 0;

	// Cannot use subnet 0 and the last subnet. So, mark them as used.
	if (set_entry_in_use(0, max_subnet_nos, subnet_nos) == 0) {
		ASSERT(0);
	}
	if (set_entry_in_use(max_subnet_nos - 1, max_subnet_nos,
			subnet_nos) == 0) {
		ASSERT(0);
	}

	// Subnet T/2 upto subnet T/2 + T/4 is for logical per node
	// addresses. So, mark all these subnets as used.
	sno = max_subnet_nos/2;
	for (i = 0; i < max_subnet_nos/4; i++) {
		if (set_entry_in_use(sno+i, max_subnet_nos, subnet_nos) == 0) {
			ASSERT(0);
		}
	}

	bzero(ass_aids, sizeof (ass_aids));
}

user_tm::~user_tm()
{
	if (subnet_nos != NULL)
		delete [] subnet_nos;
}

//
// Gets the state of the paths by querying the component state proxy
// and then comparing the component name with the path name. When the path
// names match then the staet of the path is set to P_UP
//
void
user_tm::get_path_states()
{
	cladmin_component_state_list_t *pslist = NULL;
	uint_t i;

	pslist = cladmin_get_component_state_list();
	for (i = 0; i < pslist->listlen; i++) {
		if (strcmp(pslist->list[i].type,
		    SC_SYSLOG_TRANSPORT_PATH_TAG) == 0) {
			set_path_state(pslist->list[i].int_name,
			    pslist->list[i].state);
		}
	}
	cladmin_free_component_state_list(pslist);
}

//
// Name of the path in User TM is the internal pathname of path.
// (see transport pathend creation code for format of external path name
// and internal pathname). Internal pathname is the name returned through
// the query interface.
//
char *
user_tm::make_pathname(tm_point *s, tm_point *d)
{
	nodeid_t nodeid1, nodeid2;
	uint_t adpid1, adpid2;
	clconf_obj_t *node =
	    clconf_obj_get_parent((clconf_obj_t *)s->get_adp());
	nodeid1 = (nodeid_t)clconf_obj_get_id((clconf_obj_t *)node);
	adpid1 = (uint_t)clconf_obj_get_id((clconf_obj_t *)s->get_adp());
	node = clconf_obj_get_parent((clconf_obj_t *)d->get_adp());
	nodeid2 = (nodeid_t)clconf_obj_get_id((clconf_obj_t *)node);
	adpid2 = (uint_t)clconf_obj_get_id((clconf_obj_t *)d->get_adp());
	return (clconf_get_internal_pathname(nodeid1, adpid1, nodeid2, adpid2));
}

// find the path with pname in the pathlist and set its state to UP if
// the pstate is online
void
user_tm::set_path_state(const char *pname, int pstate)
{
	tm_path	*path;

	for (path_list.atfirst(); (path = path_list.get_current()) != NULL;
	    path_list.advance()) {
		if ((strcmp(path->get_name(), pname) == 0) &&
		    (pstate == SC_STATE_ONLINE)) {
			path->set_state(path->get_state() | tm_path::P_UP);
		}
	}
}

// Returns true if the path p is the last UP path in the list of paths
// between the source and destination nodes of path p.
bool
user_tm::is_last_path(tm_path *p)
{
	tm_path *path;
	SList<tm_path>::ListIterator	iter(path_list);

	for (; (path = iter.get_current()) != NULL; iter.advance()) {
		if ((!path->marked_remove()) && path->is_up() &&
		    (path->is_samenodes(p)))
			return (false);
	}
	return (true);
}

//
// "Cluster" points to the new clconf tree. verify_path_status function
// determines if it is safe to update the current clconf tree to new "Cluster"
// tree by checking the dynamic path status of the current paths and then
// returns zero if moving to the new clconf tree does not isolate any
// node from the cluster membership.
//
int
user_tm::verify_path_status(clconf_cluster_t *Cluster)
{
	tm_path *path;
	clconf_obj_t *adp;

	ASSERT(dom_list.empty());
	ASSERT(path_list.empty());
	create_domains(clconf_cluster_get_current());
	create_paths();
	dom_list.dispose();
	for (path_list.atfirst(); (path = path_list.get_current()) != NULL;
	    path_list.advance())
		path->set_state(tm_path::P_INVALID);

	// create domains from the new clconf tree
	create_domains(Cluster);
	// create paths from the new domain list. This would result in
	// the changes needed to paths.
	create_paths();
	// Get the state of each path in the path_list
	get_path_states();
	for (path_list.atfirst(); (path = path_list.get_current()) != NULL;
	    path_list.advance()) {
		if (path->is_up() && path->marked_remove() &&
		    is_last_path(path))
			return (1);
	}

	// Set an adapter property, temporarily, to identify the adapters
	// that needs to be cleared off of the ip address and adapter id
	for (path_list.atfirst(); (path = path_list.get_current()) != NULL;
	    path_list.advance()) {
		if (!path->marked_remove()) {
			adp = (clconf_obj_t *)path->get_src()->get_adp();
			(void) clconf_obj_set_property(adp, "in_path", "1");
			adp = (clconf_obj_t *)path->get_dst()->get_adp();
			(void) clconf_obj_set_property(adp, "in_path", "1");
		}
	}
	return (0);
}

//
// Does sanity checking on the clconf tree pointed by Cluster.
//
int
user_tm::verify(clconf_cluster_t *Cluster, char *err_buf, uint_t size)
{
	int			err = 0;
	char			tmp_buf[256], *bb_type;
	clconf_iter_t		*iter1, *iter2, *iter3;
	clconf_node_t		*node;
	clconf_adapter_t	*adp;
	clconf_port_t		*port, *port1, *port2;
	clconf_bb_t		*bb;
	clconf_cable_t		*cable;
	clconf_obj_t		*obj1, *obj2, *obj1_p, *obj2_p;
	clconf_objtype_t	obj1_type = CL_UNKNOWN_OBJ;
	clconf_objtype_t	obj2_type = CL_UNKNOWN_OBJ;
	int			obj1_id = 0, obj2_id = 0;
	int			obj1_p_id = 0, obj2_p_id = 0;
	int			cable_id = 0;
	const char		*hbq_str, *hbt_str, *transp;
	char			*hb_str;

	if ((err_buf == NULL) || (size == 0))
		return (TM_ERR_BUFF);

	err_off = 0;
	*err_buf = 0;
	if (Cluster == NULL) {
		append_err(err_buf, size, &err, TM_ERR_CLST,
		    DGETTEXT("Invalid Cluster object\n"));
		return (err);
	}
	/*
	 * Verify interconnect info
	 */

	/*
	 * Step 0 -
	 * Verify cluster wide heartbeat parameters if available.
	 */
	hbq_str = clconf_obj_get_property((clconf_obj_t *)Cluster,
		"transport_heartbeat_quantum");
	hbt_str = clconf_obj_get_property((clconf_obj_t *)Cluster,
		"transport_heartbeat_timeout");

	if (hbt_str != NULL && hbq_str != NULL) {
		heartbeat_t	hb;
		hb.quantum = (uint_t)STOINT(hbq_str);
		hb.timeout = (uint_t)STOINT(hbt_str);
		(void) check_heartbeat_parameters(&hb, err_buf,
						size, &err);
	}

	/*
	 * Step 1 -
	 * Run through the node and bb iterators and mark the existing
	 * ones.  These form the list of valid nodes/bb's/ports that
	 * can form a cable endpoint.
	 */

	/*
	 * Validate Node-Adapter-Port entries
	 */

	if ((iter1 = clconf_cluster_get_nodes(Cluster)) == NULL) {
	    append_err(err_buf, size, &err, TM_ERR_CLCF,
		DGETTEXT("No node defined in the cluster\n"));
	    return (err);
	}

	if (clconf_iter_get_count(iter1) <= 0) {
	    append_err(err_buf, size, &err, TM_ERR_NODE,
		DGETTEXT("No nodes in cluster\n"));
	    clconf_iter_release(iter1);
	    return (err);
	}

	/*
	 * XXX: Check for iterator being NULL before using it. Add more
	 * verbose error messges, proper error code, messaged ID and do
	 * what it takes to do proper internationalization.
	 */
	for (; (node = (clconf_node_t *)
	    clconf_iter_get_current(iter1)) != NULL;
	    clconf_iter_advance(iter1)) {

	    if (clconf_obj_get_name((clconf_obj_t *)node) == NULL) {
		append_err(err_buf, size, &err, TM_ERR_NAME,
		    DGETTEXT("Name for one or more node not defined\n"));
	    }
	    iter2 = clconf_node_get_adapters(node);

	    for (; (adp = (clconf_adapter_t *)
		clconf_iter_get_current(iter2)) != NULL;
		clconf_iter_advance(iter2)) {

		if (clconf_obj_get_name((clconf_obj_t *)adp) == NULL) {
			append_err(err_buf, size, &err, TM_ERR_NAME,
			    DGETTEXT("Name for one or more adapter not "
			    "defined\n"));
		}

		if (clconf_obj_get_property((clconf_obj_t *)adp,
		    "device_name") == NULL) {
			append_err(err_buf, size, &err, TM_ERR_NAME,
			    DGETTEXT("device_name not defined for "
			    "adapter(s)\n"));
		}

		if (clconf_obj_get_property((clconf_obj_t *)adp,
		    "device_instance") == NULL) {
			append_err(err_buf, size, &err, TM_ERR_NAME,
			    DGETTEXT("device_instance not defined for "
			    "adapter(s)\n"));
		}

		/* get transport type */
		transp = clconf_adapter_get_transport_type(adp);
		ASSERT(transp != NULL);
		/*
		 * make sure that both heartbeat parameters, if defined, are
		 * in range
		 */

		hb_str = new char[CL_MAX_LEN];
		if (os::snprintf(hb_str, (unsigned long)CL_MAX_LEN,
		    "%s_heartbeat_quantum", transp) >= CL_MAX_LEN)
			ASSERT(0);

		hbq_str = clconf_obj_get_property((clconf_obj_t *)adp, hb_str);

		if (os::snprintf(hb_str, (unsigned long)CL_MAX_LEN,
		    "%s_heartbeat_timeout", transp) >= CL_MAX_LEN)
			ASSERT(0);

		hbt_str = clconf_obj_get_property((clconf_obj_t *)adp, hb_str);

		delete []hb_str;

		if (hbt_str != NULL && hbq_str != NULL) {
			heartbeat_t	hb;
			hb.quantum = (uint_t)STOINT(hbq_str);
			hb.timeout = (uint_t)STOINT(hbt_str);
			(void) check_heartbeat_parameters(&hb, err_buf,
							size, &err);
		}

		iter3 = clconf_adapter_get_ports(adp);

		if (clconf_iter_get_count(iter3) > 1) {
			append_err(err_buf, size, &err, TM_ERR_ADPT,
			    DGETTEXT("Duplicate adapter usage! More than "
			    "one port per adapter is not supported.\n"));
			clconf_iter_release(iter3);
			clconf_iter_release(iter2);
			clconf_iter_release(iter1);
			return (err);
		}

		for (; (port = (clconf_port_t *)
		    clconf_iter_get_current(iter3)) != NULL;
		    clconf_iter_advance(iter3)) {
			if (clconf_obj_get_name((clconf_obj_t *)port) == NULL) {
				append_err(err_buf, size, &err, TM_ERR_NAME,
				    DGETTEXT("Name for one or more port not"
				    " defined\n"));
			}
		}
		clconf_iter_release(iter3);
	    }
	    clconf_iter_release(iter2);
	}
	clconf_iter_release(iter1);

	/*
	 * Validate Blackbox-Ports
	 */
	iter1 = clconf_cluster_get_bbs(Cluster);

	for (; (bb = (clconf_bb_t *)clconf_iter_get_current(iter1)) != NULL;
	    clconf_iter_advance(iter1)) {

	    if (clconf_obj_get_name((clconf_obj_t *)bb) == NULL) {
		append_err(err_buf, size, &err, TM_ERR_NAME,
		    DGETTEXT("Name for one or more blackbox not defined\n"));
	    }
	    if ((bb_type = (char *)clconf_obj_get_property((clconf_obj_t *)bb,
		"type")) == NULL || (strcmp(bb_type, "ring") != 0 &&
		strcmp(bb_type, "switch") != 0)) {
		append_err(err_buf, size, &err, TM_ERR_NAME,
		    DGETTEXT("type for one or more blackbox incorrect\n"));
	    }
	    iter2 = clconf_bb_get_ports(bb);

	    for (; (port = (clconf_port_t *)
		clconf_iter_get_current(iter2)) != NULL;
		clconf_iter_advance(iter2)) {
		if (clconf_obj_get_name((clconf_obj_t *)port) == NULL) {
			append_err(err_buf, size, &err, TM_ERR_NAME,
			    DGETTEXT("Name for one or more port not "
			    "defined\n"));
		}
	    }
	    clconf_iter_release(iter2);
	}
	clconf_iter_release(iter1);

	/*
	 * Step 2 -
	 * For each cable figure out -
	 *	If the end points valid (i.e. existing node-adp-port
	 *		or bb-port)?
	 */
	iter1 = clconf_cluster_get_cables(Cluster);

	for (; (cable = (clconf_cable_t *)
	    clconf_iter_get_current(iter1)) != NULL;
	    clconf_iter_advance(iter1)) {

	    cable_id = clconf_obj_get_id((clconf_obj_t *)cable);

	    // Endpoint 1
	    if ((port1 = clconf_cable_get_endpoint(cable, 1)) == NULL) {
		(void) snprintf(tmp_buf, sizeof (tmp_buf),
		    DGETTEXT("Cable %d: cable endpt 1 invalid\n"), cable_id);
		append_err(err_buf, size, &err, TM_ERR_EDPT, tmp_buf);
		continue;
	    }
	    obj1 = clconf_obj_get_parent((clconf_obj_t *)port1);
	    ASSERT(obj1);
	    obj1_type = clconf_obj_get_objtype(obj1);
	    obj1_id = clconf_obj_get_id(obj1);		/* adp or bb id */
	    if (obj1_type == CL_ADAPTER) {
		obj1_p = clconf_obj_get_parent(obj1);
		ASSERT(obj1_p);
		obj1_p_id = clconf_obj_get_id(obj1_p);	/* node id */
	    }

	    // Endpoint 2
	    if ((port2 = clconf_cable_get_endpoint(cable, 2)) == NULL) {
		(void) snprintf(tmp_buf, sizeof (tmp_buf),
		    DGETTEXT("Cable %d: cable endpt 2 invalid\n"), cable_id);
		append_err(err_buf, size, &err, TM_ERR_EDPT, tmp_buf);
		continue;
	    }
	    obj2 = clconf_obj_get_parent((clconf_obj_t *)port2);
	    ASSERT(obj2);
	    obj2_type = clconf_obj_get_objtype(obj2);
	    obj2_id = clconf_obj_get_id(obj2);		/* adp or bb id */
	    if (obj2_type == CL_ADAPTER) {
		obj2_p = clconf_obj_get_parent(obj2);
		ASSERT(obj2_p);
		obj2_p_id = clconf_obj_get_id(obj2_p);	/* node id */
	    }

	    if (((obj1_type != CL_BLACKBOX) && (obj1_type != CL_ADAPTER)) ||
		((obj2_type != CL_BLACKBOX) && (obj2_type != CL_ADAPTER))) {
		(void) snprintf(tmp_buf, sizeof (tmp_buf),
		    DGETTEXT("Invalid cable %d: Cable endpoints "
		    "should be one of - node:adapter or switch@port. "
		    "This cable has at least one end point which does not "
		    "belong to this class. (received - end1_type %d, "
		    "end2_type %d)\n"), cable_id, obj1_type, obj2_type);
		append_err(err_buf, size, &err, TM_ERR_EDPT, tmp_buf);
		continue;
	    }

	    if (obj1_type == CL_ADAPTER && obj2_type == CL_ADAPTER) {
		/* See if the two adapters are of the same type */
		if (!is_same_property(clconf_adapter_get_transport_type(
		    (clconf_adapter_t *)obj1),
		    clconf_adapter_get_transport_type(
		    (clconf_adapter_t *)obj2))) {
			(void) snprintf(tmp_buf, sizeof (tmp_buf),
			    DGETTEXT("Incompatible cable %d endpoints : "
			    "Node (%d), Adp (%d) is connected to Node (%d) "
			    "Adp (%d).  But they are of different transport "
			    "types\n"), cable_id, obj1_p_id, obj1_id,
			    obj2_p_id, obj2_id);
			append_err(err_buf, size, &err, TM_ERR_EDTR, tmp_buf);
		}

		/*
		 * If VLANs are enabled, verify that both adapters are
		 * configured to be in the same VLAN.
		 */
		if (clconf_adapter_get_vlan_id((clconf_adapter_t *)obj1) !=
		    clconf_adapter_get_vlan_id((clconf_adapter_t *)obj2)) {
			(void) snprintf(tmp_buf, sizeof (tmp_buf),
			    DGETTEXT("Incompatible cable %d endpoints : "
			    "Node (%d), Adp (%d) is connected to Node (%d) "
			    "Adp (%d).  But they are configured to be on"
			    " different VLANs\n"), cable_id, obj1_p_id,
			    obj1_id, obj2_p_id, obj2_id);
			append_err(err_buf, size, &err, TM_ERR_EDTR, tmp_buf);
		}
	    } else {
		/* One black box and one adapter */

		clconf_adapter_t	*adp_obj;
		clconf_bb_t		*bb_obj;
		const char		*adp_trtype;
		const char		*bb_trtype;
		int			adp_vlan_id;
		int			bb_vlan_id;

		if (obj1_type == CL_ADAPTER) {
			ASSERT(obj2_type == CL_BLACKBOX);
			adp_obj	= (clconf_adapter_t *)obj1;
			bb_obj	= (clconf_bb_t *)obj2;
		} else {
			ASSERT(obj1_type == CL_BLACKBOX);
			ASSERT(obj2_type == CL_ADAPTER);
			adp_obj	= (clconf_adapter_t *)obj2;
			bb_obj	= (clconf_bb_t *)obj1;
		}

		/*
		 * Black boxes do not have a defined transport type property.
		 * However, all adapters attached to a black box must have
		 * the same transport type property. To perform this check
		 * we insert a temporary transport type property to the black
		 * box object. When the first cable attached to a blackbox
		 * is found, the blackbox is assigned the transport type of
		 * the adapter at the other end of the cable. Subsequent
		 * adapters that are found attached to this blackbox must have
		 * the same transport type. The transport type property added
		 * to the blackboxes will be removed from the clconf tree at
		 * the end of this method.
		 */

		adp_trtype = clconf_adapter_get_transport_type(adp_obj);
		bb_trtype = clconf_bb_get_transport_type(bb_obj);

		if (bb_trtype == NULL) {
			clconf_bb_set_transport_type(bb_obj, adp_trtype);
		} else if (strcmp(adp_trtype, bb_trtype) != 0) {
			append_err(err_buf, size, &err, TM_ERR_EDTR,
			    DGETTEXT("All adapters connected to a switch"
			    " must be of the same transport type\n"));
		}

		/*
		 * Black boxes do not have a defined vlan id. However, all
		 * adapters connected to a black box must have the same
		 * vlan id. Exception - an adapter with no vlan id is allowed
		 * to be connected to a switch that has adapters with vlan ids
		 * connected to it. This it allow mixing of tagged and untagged
		 * switch ports in the same vlan. Mechanism to do this check
		 * is the similar to that used for transport_type above.
		 */
		adp_vlan_id = clconf_adapter_get_vlan_id(adp_obj);
		bb_vlan_id = clconf_bb_get_vlan_id(bb_obj);
		if (bb_vlan_id == -1) {
			clconf_bb_set_vlan_id(bb_obj, adp_vlan_id);
		} else if (bb_vlan_id != adp_vlan_id) {
			if (bb_vlan_id == 0 || adp_vlan_id == 0) {
				int vid = bb_vlan_id ? bb_vlan_id : adp_vlan_id;
				const char *sname = clconf_obj_get_name(
				    (clconf_obj_t *)bb_obj);
				(void) snprintf(tmp_buf, sizeof (tmp_buf),
				    "WARNING - Switch %s%shas both vlan tagged"
				    " (vlanid %d) and untagged adapters"
				    " connected to it. Assuming they are on"
				    " the same vlan.\n",
				    sname ? sname : "", sname ? " " : "", vid);
				append_err(err_buf, size, &err, 0,
				    DGETTEXT(tmp_buf));
			} else {
				append_err(err_buf, size, &err, TM_ERR_EDTR,
				    DGETTEXT("All adapters connected to a"
				    " switch must be in the same vlan.\n"));
			}
		}
	    }
	}
	clconf_iter_release(iter1);

	/*
	 * Step 3 -
	 * Delete the transport type and vlan id properties added to the
	 * blackboxes from the clconf tree above in step 2.
	 */
	iter1 = clconf_cluster_get_bbs(Cluster);
	for (; (bb = (clconf_bb_t *)clconf_iter_get_current(iter1)) != NULL;
	    clconf_iter_advance(iter1)) {
		clconf_bb_set_transport_type(bb, NULL);
		clconf_bb_set_vlan_id(bb, 0);
	}
	clconf_iter_release(iter1);

	return (err);
}

void
user_tm::append_err(char *buffer, uint_t sz, int *err, int err_code,
    const char *str)
{
	uint_t	size;

	*err |= err_code;

	/* get string length */
	if ((size = (uint_t)strxfrm(NULL, str, (size_t)0)) == 0) {
		*err |= TM_ERR_INTL;
		return;
	}

	if (sz <= (err_off + size)) {
		*err |= TM_ERR_INCP;
		return;
	}

	(void) sprintf((buffer + err_off), str);
	err_off += size;
}

int
user_tm::check_heartbeat_parameters(heartbeat_t *hb, char *err_buf,
    uint_t size, int *err)
{
	int	status = 0;
	char	tmp_buf[80];

	if (!HB_QUANTUM_IN_RANGE(hb)) {
		(void) sprintf(tmp_buf,
			"heartbeat quantum %u out of range %u - %u.\n",
			hb->quantum, HB_QUANTUM_MIN, HB_QUANTUM_MAX);
		append_err(err_buf, size, err, TM_ERR_BEAT, tmp_buf);
		status = -1;
	}

	if (!HB_TIMEOUT_IN_RANGE(hb)) {
		(void) sprintf(tmp_buf,
			"heartbeat timeout %u out of range %u - %u.\n",
			hb->timeout, HB_TIMEOUT_MIN(hb), HB_TIMEOUT_MAX(hb));
		append_err(err_buf, size, err, TM_ERR_BEAT, tmp_buf);
		status = -1;
	}

	return (status);
}

//
// Strips of ip_address/netmask/adapter_id properties of an adapter.
// This is called to remove those properties, which were earlier inserted
// when the adapter is enabled, because the adapter is no longer in use now.
//
void
user_tm::cleanup_adapter(clconf_adapter_t *cl_adp)
{
	clconf_obj_t *adp = (clconf_obj_t *)cl_adp;
	if (clconf_obj_get_property(adp, "ip_address") != NULL)
	    (void) clconf_obj_set_property(adp, "ip_address", NULL);
	if (clconf_obj_get_property(adp, "netmask") != NULL)
	    (void) clconf_obj_set_property(adp, "netmask", NULL);
	if (clconf_obj_get_property(adp, "adapter_id") != NULL)
	    (void) clconf_obj_set_property(adp, "adapter_id", NULL);
}

int
user_tm::ic_update(clconf_cluster_t *Cluster)
{
	clconf_iter_t *niter, *aiter;
	clconf_node_t *cl_node;
	clconf_obj_t *cl_adp;

	if (Cluster == NULL)
		return (1);

	// Check the dynamic path status and return 1 if the update will
	// result in node isolation

	if (verify_path_status(Cluster)) {
		dom_list.dispose();
		path_list.dispose();
		return (1);
	}

	do_init(Cluster);

	// Calculate per-node logical IP addresses. Do this only if
	// using flexible IP addressing. Otherwise the old default
	// pernode IP address range is used.
	if (flex_ip_address())
		calculate_pernode(Cluster);

	// Calculate IP addresses and SCI adapter ids
	if (calculate_netinfo(Cluster))
		return (1);

	niter = clconf_cluster_get_nodes(Cluster);
	for (; (cl_node = (clconf_node_t *)
	    clconf_iter_get_current(niter)) != NULL;
	    clconf_iter_advance(niter)) {
		aiter = clconf_node_get_adapters(cl_node);
		for (; (cl_adp = clconf_iter_get_current(aiter)) != NULL;
		    clconf_iter_advance(aiter)) {
			if (clconf_obj_get_property(cl_adp, "in_path") == NULL)
			    cleanup_adapter((clconf_adapter_t *)cl_adp);
			else {
			    (void) clconf_obj_set_property(cl_adp,
				"in_path", NULL);
			}
		}
		clconf_iter_release(aiter);
	}
	clconf_iter_release(niter);

	tm_domain *dom;
	tm_point *point;
	// Set path count to zero, so that all the points are deleted.
	while ((dom = dom_list.reapfirst()) != NULL) {
		SList<tm_point>::ListIterator iter(dom->get_points());
		for (; (point = iter.get_current()) != NULL; iter.advance())
			point->set_in_pathcount(0);
	}
	dom_list.dispose();
	path_list.dispose();
	return (0);
}

/*
 * This function updates the private IP address range and the individual
 * IP address assignments for the physical adapters and for the pernode
 * IP addresses based on the new IP address range provided. Note that
 * this function must be called only in non-cluster mode.
 */
int
user_tm::modify_ip_address_range(clconf_cluster_t *Cluster)
{
	tm_path *path;
	clconf_obj_t *adp;
	int bootflags = 0;

	/* Do not allow this in cluster mode */
	if ((os::cladm(CL_INITIALIZE, CL_GET_BOOTFLAG, &bootflags) != 0) ||
		(bootflags & CLUSTER_BOOTED)) {
		return (1);
	}

	if (Cluster == NULL)
		return (1);

	ASSERT(dom_list.empty());
	create_domains(Cluster);
	create_paths();

	// Mark the in_path property for the src and dest adapters
	// so that the IP address and netmask gets assigned when calling
	// calculate_netinfo.
	for (path_list.atfirst(); (path = path_list.get_current()) != NULL;
	    path_list.advance()) {
		adp = (clconf_obj_t *)path->get_src()->get_adp();
		(void) clconf_obj_set_property(adp, "in_path", "1");
		adp = (clconf_obj_t *)path->get_dst()->get_adp();
		(void) clconf_obj_set_property(adp, "in_path", "1");
	}

	do_init(Cluster);

	// Calculate per-node logical IP addresses.
	calculate_pernode(Cluster);

	// Calculate IP addresses and SCI adapter ids
	if (calculate_netinfo(Cluster))
		return (1);

	// Once the IP addresses are calculated, remove the in_path
	// property. Its no longer needed.
	for (path_list.atfirst(); (path = path_list.get_current()) != NULL;
	    path_list.advance()) {
		adp = (clconf_obj_t *)path->get_src()->get_adp();
		(void) clconf_obj_set_property(adp, "in_path", NULL);
		adp = (clconf_obj_t *)path->get_dst()->get_adp();
		(void) clconf_obj_set_property(adp, "in_path", NULL);
	}

	dom_list.dispose();
	path_list.dispose();
	return (0);
}

void
user_tm::calculate_pernode(clconf_cluster_t *Cluster)
{
	struct in_addr	in;
	uint_t		pernode_subnet_no;
	uint_t		pernode_addrs, num = 1;
	uint_t		pernode_subnet_host_bits;

	// Write the net number for the logical pernode IP addresses
	// to the CCR.

	pernode_addrs = (max_subnet_nos/4) * (~def_subnet_netmask + 1);
	pernode_subnet_netmask = (in_addr_t)0xFFFFFFFF;
	while (num < pernode_addrs) {
		num = num << 1;
		pernode_subnet_netmask = pernode_subnet_netmask << 1;
	}
	(void) get_net_and_host_bits(pernode_subnet_netmask, NULL,
		&pernode_subnet_host_bits);

	pernode_subnet_no = 2;
	pernode_subnet_netnum = def_netnum |
		(pernode_subnet_no << pernode_subnet_host_bits);

	in = inet_makeaddr(pernode_subnet_netnum, 0);
		(void) clconf_obj_set_property((clconf_obj_t *)Cluster,
			"private_user_net_number", inet_ntoa(in));
	in = inet_makeaddr(pernode_subnet_netmask, 0);
		(void) clconf_obj_set_property((clconf_obj_t *)Cluster,
			"private_user_netmask", inet_ntoa(in));
}

//
// Calculates the IP addresses and SCI adapter ids.
// Before calculating the new IP addresses, old IP addresses are stored
// in the bitmasks.
//
int
user_tm::calculate_netinfo(clconf_cluster_t *Cluster)
{
	tm_domain				*dom;
	tm_user_domain				*udom;
	tm_point	*point;
	int err = 0;

	for (dom_list.atfirst(); (dom = dom_list.get_current()) != NULL;
	    dom_list.advance()) {
		udom = (tm_user_domain *)dom;
		udom->do_init(Cluster);
		udom->use_existing_ips(subnet_nos, def_netnum,
		    def_netmask, max_subnet_nos);
		SList<tm_point>::ListIterator iter(dom->get_points());
		for (; (point = iter.get_current()) != NULL; iter.advance())
			point->get_existing_adpid(ass_aids);
	}
	for (dom_list.atfirst(); (dom = dom_list.get_current()) != NULL;
	    dom_list.advance()) {
		udom = (tm_user_domain *)dom;
		err = udom->calculate_new_ips(def_netnum, subnet_nos,
			max_subnet_nos, max_subnet_entries);
		if (err)
			return (1);
		SList<tm_point>::ListIterator iter(dom->get_points());
		for (; (point = iter.get_current()) != NULL; iter.advance()) {
			if (point->get_in_pathcount() != 0)
				point->calculate_new_adpid(ass_aids);
		}
	}
	return (0);
}

//
// Go through the list of points which are in use, and remember the IP
// addresses and subnet address that are in use.
//
void
tm_user_domain::use_existing_ips(uint_t *subnet_nos, in_addr_t def_netnum,
    in_addr_t def_netmask, uint_t max_subnet_nos)
{
	in_addr_t	in_netmask, in_ip;
	uint_t		sno, hno;
	tm_point	*pt;
	const char	*adp_ip_addr, *adp_netmask;

	for (point_list.atfirst(); (pt = point_list.get_current()) != NULL;
	    point_list.advance()) {
		clconf_adapter_t *cl_adp = pt->get_adp();
		adp_ip_addr = clconf_obj_get_property((clconf_obj_t *)cl_adp,
		    "ip_address");
		adp_netmask = clconf_obj_get_property((clconf_obj_t *)cl_adp,
		    "netmask");
		if ((adp_ip_addr == NULL) || (adp_netmask == NULL))
			continue;
		in_ip = inet_addr(adp_ip_addr);
		in_ip = ntohl(in_ip);
		in_netmask = inet_addr(adp_netmask);
		in_netmask = ntohl(in_netmask);
		ASSERT(in_ip > 0);
		ASSERT(in_netmask > 0);

		// Validate IP address and netmask given
		if ((in_ip & def_netmask) != def_netnum) {
			continue;
		}
		if (in_netmask != subnet_netmask)
			in_netmask = subnet_netmask;

		sno = (int)(((~def_netmask) & in_ip) >> subnet_host_bits);
		hno = (int)((~subnet_netmask) & in_ip);

		if (hno == 0 || sno == 0 ||
		    (subnet_no && (subnet_no != (uint_t)sno)))
			continue;

		if (subnet_no == 0) {
			if (is_entry_in_use(sno, max_subnet_nos, subnet_nos))
				continue;
			if (set_entry_in_use(sno, max_subnet_nos,
			    subnet_nos) == 0)
				ASSERT(0);
			subnet_no = sno;
			subnet_netnum = (def_netmask & in_ip) |
				(subnet_no << subnet_host_bits);
		}
		if (is_entry_in_use(hno, max_ip_host_nos, ip_host_nos))
			continue;
		if (set_entry_in_use(hno, max_ip_host_nos, ip_host_nos) == 0)
			ASSERT(0);
	}
}

//
// Calculate new subnet number(if needed) and new IP addresses for the
// points which are now part of paths.
//
int
tm_user_domain::calculate_new_ips(in_addr_t def_netnum, uint_t *subnet_nos,
	uint_t max_subnet_nos, uint_t max_subnet_entries)
{
	uint_t		sno = 0;
	tm_point	*pt;
	int err = 0;

	for (point_list.atfirst(); (pt = point_list.get_current()) != NULL;
	    point_list.advance()) {
		if (pt->get_in_pathcount() == 0) {
			continue;
		}
		if (subnet_no == 0) {
			if (get_next_entry(max_subnet_entries, max_subnet_nos,
				&sno, subnet_nos) == 0) {
				return (1);
			}
			if (set_entry_in_use(sno, max_subnet_nos,
			    subnet_nos) == 0)
				ASSERT(0);
			subnet_no = sno;
			subnet_netnum = def_netnum |
				(subnet_no << subnet_host_bits);
		}
		err = calculate_ipaddr(pt);
		if (err)
			return (1);
	}
	return (0);
}

int
tm_user_domain::calculate_ipaddr(tm_point *pt)
{
	const char 	*adp_ip_addr, *adp_netmask;
	uint_t		host_no;
	in_addr_t	in_ip, in_netmask;
	struct in_addr	in;
	clconf_adapter_t *adp = pt->get_adp();

	clconf_node_t *cl_node = (clconf_node_t *)
	    clconf_obj_get_parent((clconf_obj_t *)adp);
	ASSERT(cl_node != NULL);
	adp_ip_addr = clconf_obj_get_property((clconf_obj_t *)adp,
		"ip_address");
	adp_netmask = clconf_obj_get_property((clconf_obj_t *)adp, "netmask");
	if ((adp_ip_addr != NULL) && (adp_netmask != NULL)) {
		in_ip = inet_addr(adp_ip_addr);
		in_ip = ntohl(in_ip);
		in_netmask = inet_addr(adp_netmask);
		in_netmask = ntohl(in_netmask);

		if (((in_ip & subnet_netmask) == subnet_netnum) &&
			(in_netmask == subnet_netmask)) {
			return (0);
		}
	}

	host_no = (uint_t)clconf_obj_get_id((clconf_obj_t *)cl_node);
	if (is_entry_in_use(host_no, max_ip_host_nos, ip_host_nos) &&
		get_next_entry(max_ip_entries, max_ip_host_nos,
			&host_no, ip_host_nos) == 0) {
		return (1);
	}
	if (set_entry_in_use(host_no, max_ip_host_nos, ip_host_nos) == 0)
		ASSERT(0);

	in_ip = (uint_t)host_no;
	in = inet_makeaddr(subnet_netnum, in_ip);
	(void) clconf_obj_set_property((clconf_obj_t *)adp,
		"ip_address", inet_ntoa(in));

	in = inet_makeaddr(subnet_netmask, 0);
	(void) clconf_obj_set_property((clconf_obj_t *)adp,
			"netmask", inet_ntoa(in));

	return (0);
}

//
// valid path for user TM is a path from lower nodeid to higher node id.
// Be aware that user TM, unlike kernel TM, contains paths between two
// remote nodes too. This is needed for IP address calculation.
//
bool
user_tm::valid_path(tm_point *s, tm_point *d)
{
	clconf_obj_t *sn = clconf_obj_get_parent((clconf_obj_t *)s->get_adp());
	clconf_obj_t *dn = clconf_obj_get_parent((clconf_obj_t *)d->get_adp());
	nodeid_t snid = (nodeid_t)clconf_obj_get_id(sn);
	nodeid_t dnid = (nodeid_t)clconf_obj_get_id(dn);
	if (snid < dnid)
		return (true);
	return (false);
}

tm_domain *
user_tm::new_domain(clconf_obj_t *bb)
{
	return (tm_domain *)(new tm_user_domain(bb));
}

//
// Creates a point based on the device name
//
tm_point *
user_tm::new_point(clconf_port_t *p, const char *dname)
{
#ifndef _KERNEL_ORB
	if (strcmp(dname, "sci") == 0)
		return (new tm_sci_point(p));
	else
		return (new tm_point(p));
#else
	if (dname != NULL)
		return (new tm_point(p));
	return (NULL);
#endif
}

tm_user_domain::tm_user_domain(clconf_obj_t *bb) :
    subnet_no(0),
    tm_domain(bb)
{
	ip_host_nos = NULL;
	max_ip_host_nos = 0;
	max_ip_entries = 0;
	subnet_netnum = 0;
	subnet_netmask = 0;
	subnet_host_bits = 0;
}

void
tm_user_domain::do_init(clconf_cluster_t *Cluster)
{
	const char 	*netmask;

	// The max_ip_host_nos value must be decided based on
	// the paritition netmask stored
	// in the CCR, determined using the number of nodes
	// and adapters. And this value must be used to
	// determine the size of the ip_host_nos array.
	subnet_netmask = 0;
	if ((netmask = clconf_obj_get_property((clconf_obj_t *)Cluster,
		"private_subnet_netmask")) == NULL ||
		(subnet_netmask = os::inet_addr(netmask)) == 0 ||
		get_net_and_host_bits(ntohl(subnet_netmask), NULL, NULL)
		== 0) {
			subnet_netmask = os::inet_addr(
					CL_DEFAULT_SUBNET_NETMASK);
	} else
		subnet_netmask = os::inet_addr(netmask);

	subnet_netmask = ntohl(subnet_netmask);
	subnet_netnum = (in_addr_t)0;
	subnet_no = 0;

	max_ip_host_nos = (~subnet_netmask) + 1;
	max_ip_entries = (max_ip_host_nos / UINT_SIZE) + 1;

	ip_host_nos = new uint_t[max_ip_entries];
	bzero(ip_host_nos, sizeof (ip_host_nos));
	for (uint_t i = 0; i < max_ip_entries; i++)
		ip_host_nos[i] = 0;

	// We cannot use IP addresses with host id all 0s or 1s
	if (set_entry_in_use(0, max_ip_host_nos, ip_host_nos) == 0)
		ASSERT(0);
	if (set_entry_in_use(max_ip_host_nos - 1, max_ip_host_nos,
			ip_host_nos) == 0)
		ASSERT(0);

	// subnet_netmask is already in host order
	(void) get_net_and_host_bits(subnet_netmask, NULL,
		&subnet_host_bits);

}

tm_user_domain::~tm_user_domain()
{
	if (ip_host_nos != NULL)
		delete [] ip_host_nos;
}

//
// General utility functions
//
uint_t
get_net_and_host_bits(in_addr_t ip, uint_t *netp, uint_t *hostp)
{
	in_addr_t	tmp_ip = ip;
	uint_t		host_bits = 0, subnet_bits = 0, count[2] = {0, 0};

	for (subnet_bits = 0, host_bits = 0; tmp_ip; tmp_ip >>= 1)
		count[tmp_ip & 1]++;
	host_bits = count[0];
	subnet_bits = count[1];

	tmp_ip = ~((1 << host_bits) - (uint_t)1);
	tmp_ip &= (tmp_ip >> (UINT_SIZE - (subnet_bits + host_bits)));
	if (host_bits == 0 || subnet_bits == 0 || tmp_ip != ip) {
		return (0);
	}

	if (netp)
		*netp = subnet_bits;
	if (hostp)
		*hostp = host_bits;
	return (host_bits + subnet_bits);
}

int
is_entry_in_use(uint_t entry_number, uint_t max_number, uint_t *bitarray)
{
	uint_t	bitmask, words, bits;

	if (entry_number > max_number)
		return (1);


	bitmask = 1;
	bits = entry_number % UINT_SIZE;
	words = entry_number / UINT_SIZE;

	if (bitarray[words] & (bitmask << bits))
		return (1);
	return (0);
}

int
set_entry_in_use(uint_t entry_number, uint_t max_number, uint_t *bitarray)
{
	uint_t	bitmask, words, bits;

	if (entry_number > max_number)
		return (0);

	bitmask = 1;
	bits = entry_number % UINT_SIZE;
	words = entry_number / UINT_SIZE;

	if (bitarray[words] & (bitmask << bits))
		return (0);
	bitarray[words] |= (bitmask << bits);
	return (1);
}

int
get_next_entry(uint_t max_entries,
	uint_t max_number,
	uint_t *entry_number,
	uint_t *bitarray)
{
	uint_t	bitmask;
	uint_t	i, j, entry;
	int found = 0;

	bitmask = 1;

	for (i = 0, entry = 0; !found && i < max_entries; i++) {
		for (j = 0; j < UINT_SIZE; j++) {
			if (!(bitarray[i] & (bitmask << j))) {
				found = 1;
				break;
			}
			entry++;
		}
	}

	if (found && entry_number && entry < max_number) {
		*entry_number = entry;
		return (found);
	}
	else
		return (0);
}
