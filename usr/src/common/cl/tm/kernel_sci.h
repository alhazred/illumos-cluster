/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _KERNEL_SCI_H
#define	_KERNEL_SCI_H

#pragma ident	"@(#)kernel_sci.h	1.6	08/05/20 SMI"

#include <sys/sol_version.h>

#if SOL_VERSION < __s10
/* No Layered Driver Framework support */
#else
/* Enable all LDI related code */
#include	<sys/file.h>
#include	<sys/cred.h>
#include	<sys/sunddi.h>
#include	<sys/sunldi.h>
#include	<sys/sunldi_impl.h>
#include	<sys/conf.h>
#define	USE_LDI 1

#endif  /* SOL_VERSION */


#include	<sys/os.h>
#include 	<sys/clconf_int.h>

#ifdef  _cplusplus
extern "C" {
#endif

/*
 *	Backword compatibility must be retained if major version is same,
 *	i.e. only minor version should be increased.
 *	When compatibility is lost, increase the major version.
 */
#define	CONFIG_MINOR_VERSION	1
#define	CONFIG_MAJOR_VERSION	1
#define	CONFIG_GET_MAJOR(ver)	((uint_t)(ver) >> 8)
#define	CONFIG_GET_MINOR(ver)	((ver) & 0xff)
#define	CONFIG_MAKE_VERSION(maj, min)	(((maj) << 8) | ((min) & 0xff))
#define	CONFIG_CURRENT_VERSION	\
	CONFIG_MAKE_VERSION(CONFIG_MAJOR_VERSION, CONFIG_MINOR_VERSION)

typedef	void *clconfig_obj_t;
typedef const char *(*clconfig_get_property_t)(clconfig_obj_t, const char *);

typedef struct clconfig_ops {
	/* version of this OPS vector. */
	uint_t config_version;

	/* Non-zero argument releases the hold specified in _clconfigop() */
	void (*config_fini)(int);
	/*
	 * For next two functions, zero is success, non-zero is failure.
	 * Also clconf object for adapter is passed as argument
	 */
	int (*config_add_adapter)(clconfig_get_property_t, clconfig_obj_t);
	int (*config_remove_adapter)(clconfig_get_property_t, clconfig_obj_t);
	int (*config_add_path)(clconfig_get_property_t,
		clconfig_obj_t, clconfig_obj_t);
	int (*config_remove_path)(clconfig_get_property_t,
		clconfig_obj_t, clconfig_obj_t);
	int (*config_down_path)(clconfig_get_property_t,
		clconfig_obj_t, clconfig_obj_t);
} clconfig_ops_t;

/*
 *	Argument version is the requested version of clconfig_ops_t, return
 *	value is a version that is mutually (caller/callee) acceptable, -1 if
 *	no compatible version exists. if a compatible version is found and ops
 *	is not NULL, clconfig_ops_t structure is stored in this location.
 *
 *	Differing major numbers are considered incompatible. If requested
 *	minor version differs from what callee was compiled with, lower of
 *	the two version should be returned. If callee can't support requested
 *	minor version, -1 should be returned.
 *
 *	size argument gives the space allocated for the structure, so that
 *	we don't write beyond the structure boundry (multi-version support).
 *	Callee can return -1, is size argument is invalid or incompatible.
 *
 *	If hold argument is non-zero, if a compatible version is found and
 *	config_ops_t is returned, callee must prevent module from being
 *	unloaded until config_fini() entry point is called.
 */
typedef int (*clconfig_get_ops)(uint_t version, clconfig_ops_t *ops,
    size_t size, int hold);
extern int _clconfigop(uint_t, clconfig_ops_t *, size_t, int);


#ifdef  _cplusplus
}
#endif

#include <tm/kernel_tm.h>

class tm_sci_device : public tm_device {
public:
	tm_sci_device(const char *);
	~tm_sci_device();
	bool		setup_point(tm_point *);
	void		cleanup_point(tm_point *);
	void		setup_path(clconf_path_t *, clconf_cluster_t *);
	void		cleanup_path(clconf_path_t *, clconf_cluster_t *);

private:
	// SCI device config ops vector
	clconfig_ops_t		ops;
#ifdef USE_LDI
	ldi_ident_t		lid;
	ldi_handle_t		lhp;
#endif
};
#endif	/* _KERNEL_SCI_H */
