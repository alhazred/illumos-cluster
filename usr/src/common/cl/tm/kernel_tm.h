/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _KERNEL_TM_H
#define	_KERNEL_TM_H

#pragma ident	"@(#)kernel_tm.h	1.11	08/05/20 SMI"

#include <tm/common_tm.h>

#ifdef _KERNEL
#include <sys/modctl.h>
#include <sys/cmn_err.h>
#endif

#ifndef MAX
#define	MAX(a, b)	((a) < (b) ? (b) : (a))
#endif

extern "C" void kernel_tm_callback(void);

//
// tm_device class encapsulates interactions with the device driver. One
// instance of this class is created for each type of device.
//
class tm_device {
public:
	tm_device(const char *);
	virtual ~tm_device();

	// Returns the name of this device (e.g., hme, sci)
	const char *get_name();

	// The following four virtual functions are needed for SCI only.
	// Please see kernel_sci.h for details.
	virtual bool setup_point(tm_point *);
	virtual void cleanup_point(tm_point *);
	virtual void setup_path(clconf_path_t *, clconf_cluster_t *);
	virtual void cleanup_path(clconf_path_t *, clconf_cluster_t *);

protected:
	char *name;	// Name of the device
};

// Kernel Topology Manager class derived from the common topology manager
class kernel_tm : public topology_manager {
public:
	// Used by the ORB
	static	int initialize();
	static	void shutdown();

	static kernel_tm&	the();

	// Called by clconf when CCR infrastructure file is updated
	void tm_callback();

private:
	kernel_tm();
	~kernel_tm();

	// Updates paths based on their new state.
	void 			update_paths();

	// Sets up path by plumbing the adapter, if necessary and calling
	// the path manager.
	void 			setup_path(tm_path *);

	// Removes the path from the path manager and unplumbs the adapter
	// if it is the last path through the adapter.
	void 			destroy_path(tm_path *);

	// plumbs the adapter, loads the corresponding transport module
	// and informs path manager about the new adapter.
	bool 			setup_point(tm_point *);

	// unplumbs the adapter and informs path manager about the adapter
	// removal
	void 			destroy_point(tm_point *);

	// Determines if the two points make a valid kernel path
	bool 			valid_path(tm_point *, tm_point *);

	// Carries out any path or adapter properties changes
	void 			change_path(tm_path *);

	// returns the device with the name by searching the dev_list.
	tm_device 		*get_device_by_name(const char *);
	// Creates a new tm_device object if it does nto already exist
	tm_device		*create_device(const char *, bool);

	// Creates a new point
	tm_point		*new_point(clconf_port_t *, const char *);

	static kernel_tm 	*the_kernel_tm;

	// Name of the local node
	const char		*local_nodename;
	clconf_cluster_t	*cCluster;	// Current cluster
	clconf_cluster_t	*nCluster;	// new cluster
	SList<tm_device>	dev_list;	// List od devices

	// Lock to synchonize of TM operations. Currently only one
	// TM operation can be done at any time.
	os::mutex_t		tm_lock;
};

#include <tm/kernel_tm_in.h>
#endif	/* _KERNEL_TM_H */
