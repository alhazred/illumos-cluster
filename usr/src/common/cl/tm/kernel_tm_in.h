/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  kernel_tm_in.h
 *
 */

#ifndef _KERNEL_TM_IN_H
#define	_KERNEL_TM_IN_H

#pragma ident	"@(#)kernel_tm_in.h	1.4	08/05/20 SMI"

inline kernel_tm &
kernel_tm::the(void)
{
	ASSERT(the_kernel_tm != NULL);
	return (*the_kernel_tm);
}

inline const char *
tm_device::get_name()
{
	return (name);
}

// The following virtual functions have nothing to do for devices like
// ethernet.
inline bool
tm_device::setup_point(tm_point *)
{
	return (true);
}

inline void
tm_device::cleanup_point(tm_point *)
{
}

inline void
tm_device::setup_path(clconf_path_t *, clconf_cluster_t *)
{
}

inline void
tm_device::cleanup_path(clconf_path_t *, clconf_cluster_t *)
{
}

#endif	/* _KERNEL_TM_IN_H */
