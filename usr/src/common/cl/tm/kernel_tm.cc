//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)kernel_tm.cc	1.15	08/05/20 SMI"

#include <tm/kernel_tm.h>
#include <sys/clconf_int.h>
#ifdef _KERNEL
#include <sys/cl_assert.h>
#include <orb/ip/ifkconfig.h>
#include <tm/kernel_sci.h>
#else
#include <unode/unode.h>
#endif
#include <orb/transport/path_manager.h>
#include <clconf/clnode.h>

#ifdef  TM_DBGBUF
dbg_print_buf tm_dbg(8192);
#endif

kernel_tm *kernel_tm::the_kernel_tm = NULL;

int
kernel_tm::initialize()
{
	ASSERT(the_kernel_tm == NULL);
	TM_DBG(("Initializing kernel TM\n"));
	the_kernel_tm = new kernel_tm();
	if (the_kernel_tm == NULL)
		return (1);
	TM_DBG(("Initializing kernel TM Done\n"));
	return (0);
}

void
kernel_tm::shutdown()
{
	tm_path	*path;

	if (the_kernel_tm == NULL) {
		return;
	}
	the_kernel_tm->tm_lock.lock();
	cl_current_tree::the().set_oldroot_to_root();
	while ((path = the_kernel_tm->path_list.reapfirst()) != NULL) {
		// destroy path releases the memory allocated for the points
		the_kernel_tm->destroy_path(path);
	}
	the_kernel_tm->dev_list.dispose();
	delete the_kernel_tm;
	the_kernel_tm = NULL;
}

kernel_tm::kernel_tm()
{
	cCluster = clconf_cluster_get_current();
	nCluster = cCluster;
	ASSERT(cCluster != NULL);
	local_nodename =
	    os::strdup(clconf_cluster_get_nodename_by_nodeid(cCluster,
	    clconf_get_local_nodeid()));
	TM_DBG(("local nd name = %s\n", local_nodename));
	create_domains(cCluster);
	create_paths();
	// Dispose the domains since we don't need them any more.
	dom_list.dispose();

	update_paths();
	nCluster = NULL;
}

kernel_tm::~kernel_tm()
{
	if (cCluster)
		clconf_obj_release((clconf_obj_t *)cCluster);
}

//
// External interface for cl_haci
//
extern "C" void
kernel_tm_callback()
{
	kernel_tm::the().tm_callback();
}

//
// Only one callback can happen at any time.
//
void
kernel_tm::tm_callback(void)
{
	tm_path	*path;

	TM_DBG(("Kernel callback begin %s\n", local_nodename));
	tm_lock.lock();
	// get the new clconf tree and create domains
	nCluster = clconf_cluster_get_current();
	create_domains(nCluster);

	// Mark all the current paths as invalid
	for (path_list.atfirst(); (path = path_list.get_current()) != NULL;
	    path_list.advance()) {
		path->set_state(tm_path::P_INVALID);
		path->get_src()->set_changes(0);
		path->get_dst()->set_changes(0);
	}

	// Create any new paths and update old paths and delete any
	// obselete paths.
	create_paths();

	// Dispose the domains since we don't need them any more.
	dom_list.dispose();

	update_paths();
	if (cCluster)
		clconf_obj_release((clconf_obj_t *)cCluster);
	cCluster = nCluster;
	nCluster = NULL;
	tm_lock.unlock();
	TM_DBG(("Kernel callback end\n"));
}

tm_device *
#ifdef _KERNEL
kernel_tm::create_device(const char *dn, bool local)
#else
kernel_tm::create_device(const char *dn, bool)
#endif
{
	tm_device *dev	= NULL;

#ifdef _KERNEL
	//
	// First encounter with this device. First try to load the driver.
	// Don't create the device is the driver can not be loaded.
	// Attempt to load the driver is made only if the device is being
	// created on behalf of a local endpoint or if it is a non
	// ethernet device. Ethernet devices can be mixed and matched,
	// hence a driver need not be loaded unless a device is
	// encountered locally - in fact if the device is not present
	// locally the driver might not even be installed. Non ethernet
	// devices can not be mixed an matched, hence the driver should be
	// loaded. In fact SCI  requires that the driver be loaded
	// before the tm_*_device is created below. See kernel_sci.cc
	// for details on the requirement.
	//
	bool	is_sci	= false;
	os::sc_syslog_msg tm_syslog(SC_SYSLOG_TRANSPORT_ADAPTER_TAG,
	    "TM", NULL);

	if (strcmp(dn, SCIDEVICE) == 0) {
		is_sci = true;
	}

	if (local || is_sci) {
		if (modload("drv", (char *)dn) <= 0) {
			TM_DBG(("Could not load module %s\n", dn));
			//
			// SCMSGS
			// @explanation
			// Sun Cluster was not able to load the said network
			// driver module. Any private interconnect paths that
			// use an adapter of the corresponding type will not
			// be able to come up. If all private interconnect
			// adapters on this node are of this type, the node
			// will not be able to join the cluster at all.
			// @user_action
			// Install the appropriate network driver on this
			// node. If the node could not join the cluster, boot
			// the node in the non cluster mode and then install
			// the driver. Reboot the node after the driver has
			// been installed.
			//
			(void) tm_syslog.log(SC_SYSLOG_WARNING, MESSAGE,
			    "clcomm: failed to load driver module %s -"
			    " %s paths will not come up.", dn, dn);
			return (NULL);
		}
	}
	if (is_sci) {
		dev = new tm_sci_device(dn);
	} else {
		dev = new tm_device(dn);
	}
#else
	dev = new tm_device(dn);
#endif
	dev_list.append(dev);
	return (dev);
}

tm_device *
kernel_tm::get_device_by_name(const char *dev_name)
{
	tm_device				*dev;

	for (dev_list.atfirst(); (dev = dev_list.get_current()) != NULL;
	    dev_list.advance()) {
		if (strcmp(dev_name, dev->get_name()) == 0)
			return (dev);
	}

	return (NULL);
}

//
// update paths should always be called after create paths to carry out
// new states of the path
//
void
kernel_tm::update_paths()
{
	tm_path *path;

	// First see if there are any paths whose properties are changed.
	for (path_list.atfirst(); (path = path_list.get_current()) != NULL;
	    path_list.advance()) {
		if (path->is_changed()) {
			change_path(path);
		}
	}

	// Next destroy the paths which should be deleted before new path
	// between the same points can be brought up.
	for (path_list.atfirst(); (path = path_list.get_current()) != NULL; ) {
		if (path->get_state() & tm_path::P_DELETE) {
			// erase() does advance() too.
			(void) path_list.erase(path);
			destroy_path(path);
		} else
			path_list.advance();
	}

	// Bringup the new paths
	for (path_list.atfirst(); (path = path_list.get_current()) != NULL;
	    path_list.advance()) {
		if (path->get_state() & tm_path::P_ADD)
			setup_path(path);
	}

	// At the last destroy the paths which were in the old tree but
	// not in the new tree.
	for (path_list.atfirst(); (path = path_list.get_current()) != NULL; ) {
		if (path->get_state() & tm_path::P_INVALID) {
			(void) path_list.erase(path);
			destroy_path(path);
		} else
			path_list.advance();
	}
}

//
// setup_point plumbs the adapter associated with the point,
// loads the necessary transport module and finally calls path manager
// to add the adapter.
//
bool
kernel_tm::setup_point(tm_point *pt)
{
	const char *trtype;
	int device_instance, id;
	int vlanid;
	clconf_adapter_t	*adp = pt->get_adp();
	const char	*dname = pt->get_devname();
	tm_device *dev = get_device_by_name(dname);

	ASSERT(dev != NULL);

	trtype = clconf_adapter_get_transport_type((clconf_adapter_t *)adp);
	ASSERT(trtype != NULL);

	device_instance = clconf_adapter_get_device_instance(adp);
	vlanid = clconf_adapter_get_vlan_id(adp);

#ifdef _KERNEL
	in_addr_t 	ip_address, netmask;
	ifk_interface_t input;
	const char 	*ipaddr, *netmask_str;
	int 		error;
	const char *dlpi_dname = clconf_adapter_get_dlpi_device_name(adp);
	const char *vlan_dname = clconf_adapter_get_vlan_device_name(adp);

	os::sc_syslog_msg tm_syslog(SC_SYSLOG_TRANSPORT_ADAPTER_TAG,
	    "TM", NULL);

	if (!dev->setup_point(pt))
		return (false);

	// Get the ip address property from the clconf adapter object
	ipaddr = clconf_obj_get_property((clconf_obj_t *)adp, "ip_address");
	ASSERT(ipaddr != NULL);
	ip_address = os::inet_addr(ipaddr);
	ASSERT(ip_address != (in_addr_t)(-1));

	//
	// Get the netmask property from the clconf adapter object
	//
	netmask_str = clconf_obj_get_property((clconf_obj_t *)adp, "netmask");
	ASSERT(netmask_str != NULL);
	netmask = os::inet_addr(netmask_str);
	ASSERT(netmask != (in_addr_t)(-1));

	// modload() returns the module ID or -1.
	id = -1;
	if (os::strcmp(trtype, "dlpi") == 0) {
		id = modload("misc", "cl_dlpitrans");
	} else if (os::strcmp(trtype, "rsm") == 0) {
		id = modload("misc", "cl_rsmtrans");
	} else {
		//
		// SCMSGS
		// @explanation
		// The transport type used is not known to Solaris Clustering
		// @user_action
		// Need a user action for this message.
		//
		(void) tm_syslog.log(SC_SYSLOG_WARNING, MESSAGE,
		    "Unknown transport type: %s", trtype);
		dev->cleanup_point(pt);
		return (false);
	}

	if (id < 0) {
		//
		// SCMSGS
		// @explanation
		// Topology Manager could not load the specified transport
		// module. Paths configured with this transport will not come
		// up.
		// @user_action
		// Check if the transport modules exist with right permissions
		// in the right directories.
		//
		(void) tm_syslog.log(SC_SYSLOG_WARNING, MESSAGE,
		    "Could not load transport %s, paths configured"
		    " with this transport will not come up.", trtype);
		dev->cleanup_point(pt);
		return (false);
	}

	//
	// set IP Address and netmask
	// One IP address per adapter is assumed.
	//

	input.ifk_device_name = dlpi_dname;
	input.ifk_device_instance = device_instance;
	input.ifk_vlan_id = vlanid;
	input.ifk_vlan_device_name = vlan_dname;
	input.ifk_logical_unit = IFK_PHYSICAL;

	input.ifk_ipaddr.s_addr = ip_address;
	input.ifk_netmask.s_addr = netmask;

	if (ifkconfig(&input,
	    IFK_PLUMB|IFK_PRIVATE|IFK_IPADDR|IFK_NETMASK|IFK_UP, &error) != 0) {
		if (ifkconfig(&input, IFK_UNPLUMB, &error) != 0) {
			//
			// SCMSGS
			// @explanation
			// Topology Manager failed to plumb an adapter for
			// private network. A possible reason for plumb to
			// fail is that it is already plumbed. Solaris
			// Clustering tries to unplumb the adapter and plumb
			// it for private use but it could not unplumb the
			// adapter.
			// @user_action
			// Check if the adapter by that name exists.
			//
			(void) tm_syslog.log(SC_SYSLOG_WARNING, MESSAGE,
			    "Plumb failed. Tried to unplumb %s%d, "
			    "unplumb failed with rc %d", dlpi_dname,
			    device_instance, error);
			dev->cleanup_point(pt);
			return (false);
		}
		if (ifkconfig(&input,
		    IFK_PLUMB|IFK_PRIVATE|IFK_IPADDR|IFK_NETMASK|IFK_UP,
		    &error) != 0) {
			//
			// SCMSGS
			// @explanation
			// Topology Manager failed to plumb an adapter for
			// private network. A possible reason for plumb to
			// fail is that it is already plumbed. Solaris
			// clustering has successfully unplumbed the adapter
			// but failed while trying to plumb for private use.
			// @user_action
			// Need a user action for this message.
			//
			(void) tm_syslog.log(SC_SYSLOG_WARNING, MESSAGE,
			    "Unable to plumb even after unplumbing."
			    " rc = %d", error);
		    dev->cleanup_point(pt);
		    return (false);
		}
	}
#else
	// This is a unode cluster.

	// Skip setup of the adapters if the unode transport type is doors.
	if (os::strcmp(trtype, "doors") != 0) {
		in_addr_t 	ip_address, netmask;
		const char 	*ipaddr, *netmask_str;
		int 		error;
		char		buf[256];
		const char *dlpi_dname =
		    clconf_adapter_get_dlpi_device_name(adp);
		os::sc_syslog_msg tm_syslog(SC_SYSLOG_TRANSPORT_ADAPTER_TAG,
		    "TM", NULL);

		if (!dev->setup_point(pt))
			return (false);

		// Get the ip address property from the clconf adapter object
		ipaddr = clconf_obj_get_property((clconf_obj_t *)adp,
		    "ip_address");
		ASSERT(ipaddr != NULL);

		//
		// Get the netmask property from the clconf adapter object
		//
		netmask_str = clconf_obj_get_property((clconf_obj_t *)adp,
		    "netmask");
		ASSERT(netmask_str != NULL);

		//
		// set IP Address and netmask
		// One IP address per adapter is assumed.
		//
#ifdef linux
#define	IFCONFIG "/sbin/ifconfig %s%d plumb %s netmask %s up broadcast %s"
#define	IFUNCONFIG "/sbin/ifconfig %s%d down"
#else
#define	IFCONFIG \
	"/usr/sbin/ifconfig %s%d plumb %s netmask %s broadcast %s private up"
#define	IFUNCONFIG "/usr/sbin/ifconfig %s%d unplumb"
#endif

		char *broadaddr = "+";
#ifdef	linux
		struct in_addr	ba;
		ba.s_addr = (inet_addr(ipaddr) & inet_addr(netmask_str)) |
		    ~inet_addr(netmask_str);
		broadaddr = inet_ntoa(ba);
#endif	// linux
		sprintf(buf, IFCONFIG, dlpi_dname, device_instance, ipaddr,
		    netmask_str, broadaddr);

		(void) tm_syslog.log(SC_SYSLOG_WARNING, MESSAGE, buf);
		if ((error = system(buf)) != 0) {
			char buf2[256];
			sprintf(buf2, IFUNCONFIG, dlpi_dname,
			    device_instance);
			if ((error = system(buf2)) != 0) {
				(void) tm_syslog.log(SC_SYSLOG_WARNING, MESSAGE,
				    "Plumb failed. Tried to unplumb %s%d, "
				    "unplumb failed with rc %d", dlpi_dname,
				    device_instance, error);
				dev->cleanup_point(pt);
				return (false);
			}
			if ((error = system(buf)) != 0) {
				(void) tm_syslog.log(SC_SYSLOG_WARNING, MESSAGE,
				    "Unable to plumb even after unplumbing."
				    " rc = %d", error);
			    dev->cleanup_point(pt);
			    return (false);
			}
		}
	}

	id = unode_transport_modload(trtype);
	if (id < 0) {
		os::warning("loading transport %s failed\n", trtype);
		return (false);
	}
#endif

	// add_adapter() is called after plumbing
	(void) path_manager::the().add_adapter(adp);
	TM_DBG(("Added adapter: %p. device_name %s%d\n", adp, dname,
	    device_instance));
	return (true);
}

//
// Does the opposite of setup_point by calling path manager remove_adapter
// and then unplumbing the adapter
//
void
kernel_tm::destroy_point(tm_point *pt)
{
	int device_instance;
	int vlanid;
	clconf_adapter_t *adp = pt->get_adp();
	const char *dname = pt->get_devname();

	// remove_adapter() is called before unplumbing
	(void) path_manager::the().remove_adapter(adp);

	const char *dlpi_dname = clconf_adapter_get_dlpi_device_name(adp);

	// dlpi_name cannot be NULL as we verified in add_adapter
	// that this exists.
	ASSERT(dlpi_dname != NULL);

	device_instance = clconf_adapter_get_device_instance(adp);
	vlanid = clconf_adapter_get_vlan_id(adp);

#ifdef _KERNEL
	ifk_interface_t input;
	int error;
	os::sc_syslog_msg tm_syslog(SC_SYSLOG_TRANSPORT_ADAPTER_TAG,
	    "TM", NULL);

	input.ifk_device_name = dlpi_dname;
	input.ifk_device_instance = device_instance;
	input.ifk_vlan_id = vlanid;
	input.ifk_vlan_device_name = NULL;	// VLAN devname not needed
	input.ifk_logical_unit = IFK_PHYSICAL;

	if (ifkconfig(&input, IFK_DOWN, &error) != 0) {
		//
		// SCMSGS
		// @explanation
		// Topology Manager is done with using the adapter and failed
		// when tried to mark the interface down
		// @user_action
		// Need a user action for this message.
		//
		(void) tm_syslog.log(SC_SYSLOG_WARNING, MESSAGE,
		    "Unable to mark the interface %s%d down, rc %d", dlpi_dname,
		    device_instance, error);
	}

	if (ifkconfig(&input, IFK_UNPLUMB, &error) != 0) {
		//
		// SCMSGS
		// @explanation
		// Topology Manage is done with using the interface and failed
		// to unplumb the adapter.
		// @user_action
		// Need a user action for this message.
		//
		(void) tm_syslog.log(SC_SYSLOG_WARNING, MESSAGE,
		    "Unable to unplumb %s%d, rc %d", dlpi_dname,
		    device_instance, error);
	}
	tm_device *dev = get_device_by_name(dname);
	ASSERT(dev != NULL);
	dev->cleanup_point(pt);
#endif
	TM_DBG(("Removed adapter: %p. device_name %s%d\n", adp, dname,
	    device_instance));
}

//
// Sets up the source point if no paths were previously established
// using the adapter and then creates a clconf_path_t which is passed
// to path manager add_path.
//
void
kernel_tm::setup_path(tm_path *path)
{
	tm_point *src = path->get_src();
	tm_point *dst = path->get_dst();

	// The setup_pathcount is 0 when the first path using the
	// point is created but has not yet been setup. So, setup
	// the point.
	if (src->get_setup_pathcount() == 0) {
		if (!setup_point(src))
			return;
	}

	uint_t shbq = src->get_hbq();
	uint_t dhbq = dst->get_hbq();
	uint_t shbt = src->get_hbtout();
	uint_t dhbt = dst->get_hbtout();

	path->cl_path = clconf_path_create(src->get_port(), dst->get_port(),
	    MAX(shbq, dhbq), MAX(shbt, dhbt));

	tm_device *dev = get_device_by_name(src->get_devname());
	ASSERT(dev != NULL);
	dev->setup_path(path->cl_path, nCluster);
	clconf_cluster_add_path(path->cl_path);

	// Increment the setup_pathcount to indicate that now this
	// point belongs to a path which has been completely setup.
	src->inc_setup_pathcount();
	dst->inc_setup_pathcount();

	TM_DBG(("Path %s:%s -  %s:%s created!\n", src->get_nodename(),
	    src->get_adpname(), dst->get_nodename(), dst->get_adpname()));
}

//
// Does the opposite of setup_path
//
void
kernel_tm::destroy_path(tm_path *path)
{
	tm_point *src = path->get_src();
	tm_point *dst = path->get_dst();

	if (path->cl_path == NULL) {
		//
		// Corresponding setup_path did not do any setup.
		// There is nothing to destroy.
		//
		return;
	}

	src->dec_setup_pathcount();
	dst->dec_setup_pathcount();

	tm_device *dev = get_device_by_name(src->get_devname());
	dev->cleanup_path(path->cl_path, cCluster);

	// clconf_cluster_remove_path frees the cl_path structure too.
	clconf_cluster_remove_path(path->cl_path);

	// If the setup pathcount is 0, this means that there is currently
	// no path setup that is using the point. So, we can destroy
	// the point.
	if (src->get_setup_pathcount() == 0) {
		destroy_point(src);
	}

	TM_DBG(("%s destroyed!\n", path->get_name()));
	delete path;
}

//
// Calls path_manager appropriately depending on the properties that
// are changed.
//
void
kernel_tm::change_path(tm_path *path)
{
	uint_t		new_tmout, new_qtm, old_tmout, old_qtm;
	clconf_path_t	*new_path;
	clconf_path_t	*cl_path = path->cl_path;
	tm_point	*src = path->get_src();
	tm_point	*dst = path->get_dst();
	uint_t		schngs = src->get_changes();
	uint_t		changes = schngs | dst->get_changes();
	uint_t		shbq, dhbq, shbt, dhbt;

	if (cl_path == NULL) {
		//
		// Path was not setup during call to setup_path.
		// Can't change something that does not exist.
		//
		return;
	}

	old_qtm =  clconf_path_get_quantum(cl_path);
	old_tmout = clconf_path_get_timeout(cl_path);

	new_tmout = old_tmout;
	new_qtm = old_qtm;

	// Check if path related properties(timeout and quantum for now)
	// are changed.  If yes, call path manager to carry out the changes.
	if (changes & AP_HBT_TIMEOUT) {
		shbt = src->get_hbtout();
		dhbt = dst->get_hbtout();
		new_tmout = MAX(shbt, dhbt);
		TM_DBG(("new timeout = %d\n", new_tmout));
		if (new_tmout == old_tmout)
			changes &= ~AP_HBT_TIMEOUT;
	}
	if (changes & AP_HBT_QUANTUM) {
		shbq = src->get_hbq();
		dhbq = dst->get_hbq();
		new_qtm = MAX(shbq, dhbq);
		TM_DBG(("new quantum = %d\n", new_qtm));
		if (new_qtm == old_qtm)
			changes &= ~AP_HBT_QUANTUM;
	}

	if (changes & (AP_HBT_QUANTUM|AP_HBT_TIMEOUT)) {
		TM_DBG(("changing properties for %s %x\n", path->get_name(),
		    changes));
		new_path = clconf_path_create(src->get_port(), dst->get_port(),
			new_qtm, new_tmout);
		(void) path_manager::the().change_path(cl_path, new_path);
		clconf_path_release(new_path);
	}

	// If there are any adapter specific properties that are changed
	// then call path manager to change the adapter properties.
	// Change adapter properties apply to only the local adapters.
	if (src->get_changes() & (AP_BANDWIDTH|AP_NW_BANDWIDTH)) {
		TM_DBG(("changing properties for %s %x\n", src->get_adpname(),
		    changes));
		(void) path_manager::the().change_adapter(src->get_adp());
		schngs &= ~(AP_BANDWIDTH|AP_NW_BANDWIDTH);
		src->set_changes(schngs);
	}
}

tm_device::tm_device(const char *dname)
{
	name = os::strdup(dname);
}

tm_device::~tm_device()
{
	delete [] name;
}

//
// valid_path for kernel is the one where source point is local to the node
// in which kernel TM is running and destination point is not local.
//
bool
kernel_tm::valid_path(tm_point *s, tm_point *d)
{
	if ((strcmp(s->get_nodename(), local_nodename) == 0) &&
	    (strcmp(d->get_nodename(), local_nodename) != 0))
		return (true);
	return (false);
}

//
// Create a tm_point object given the clconf_port_t information and
// the device name. If this is the first point of its kind, the
// corresponding device object is also created. The device will not
// be created if the driver is not available. Failure to create the
// device object will also result in not creating the point object.
// Create_device is passed information about whether the device is
// local or not. It uses that information as an input in determining
// whether the driver needs to be loaded.
//
tm_point *
kernel_tm::new_point(clconf_port_t *p, const char *dname)
{
	tm_device	*dev = NULL;
	tm_point	*pt = NULL;

	if ((dev = get_device_by_name(dname)) == NULL) {
		clconf_obj_t	*adap = NULL;
		clconf_obj_t	*nodep = NULL;
		nodeid_t	ndid;
		bool		local;

		adap	= clconf_obj_get_parent((clconf_obj_t *)p);
		nodep	= clconf_obj_get_parent(adap);
		ndid	= (nodeid_t)clconf_obj_get_id(nodep);
		local	= (ndid == orb_conf::local_nodeid()) ? true : false;

		dev = create_device(dname, local);
	}
	if (dev == NULL) {
		// Driver not available
		return (NULL);
	}
#ifdef _KERNEL
	pt = new tm_point(p);
#else
	pt = new tm_point(p);
#endif
	return (pt);
}
