/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CL_ASSERT_H
#define	_CL_ASSERT_H

#pragma ident	"@(#)cl_assert.h	1.5	08/05/20 SMI"

/*
 * cl_assert.h. Defines ASSERT and CL_PANIC. CL_PANIC is the same
 * as ASSERT but also works in non-debug environment.
 */

#ifdef  __cplusplus
extern "C" {
#endif

#ifdef	_KERNEL
#include <sys/debug.h>

/* Definition for assfail is obtained from debug.h */
#ifdef DEBUG

/* We need to define our own ASSERT */
typedef int (*assert_func)(const char *, const char *, int);
extern assert_func assertfunc;

#undef ASSERT
#undef ASSERT64
#undef ASSERT32
#define	ASSERT(EX) ((void)((EX) || assertfunc(#EX, __FILE__, __LINE__)))

#ifdef _LP64
#define	ASSERT64(x)	ASSERT(x)
#define	ASSERT32(x)
#else	/* defined(_LP64) */
#define	ASSERT64(x)
#define	ASSERT32(x)	ASSERT(x)
#endif	/* defined(_LP64) */

#if defined(__STDC__)
#define	CL_PANIC(EX) ((void)((EX) || assertfunc(#EX, __FILE__, __LINE__)))
#else	/* defined(__STDC__) */
#define	CL_PANIC(EX) ((void)((EX) || assertfunc("EX", __FILE__, __LINE__)))
#endif	/* defined(__STDC__) */

#else	/* defined(DEBUG) */

#if defined(__STDC__)
#define	CL_PANIC(EX) ((void)((EX) || assfail(#EX, __FILE__, __LINE__)))
#else	/* defined(__STDC__) */
#define	CL_PANIC(EX) ((void)((EX) || assfail("EX", __FILE__, __LINE__)))
#endif	/* defined(__STDC__) */

#endif	/* defined(DEBUG) */

#else	/* defined(_KERNEL) */

int assertfail(const char *, const char *, int);
#undef	ASSERT

#ifdef	DEBUG

#ifdef _KERNEL_ORB

/* Define our own ASSERT using private assertfunc */
typedef int (*uassert_func)(const char *, const char *, int);
extern uassert_func assertfunc;
#define	ASSERT(EX) ((void)((EX) || assertfunc(#EX, __FILE__, __LINE__)))
#define	CL_PANIC(EX) ((void)((EX) || assertfunc(#EX, __FILE__, __LINE__)))

#else	/* _KERNEL_ORB */

#define	ASSERT(EX) ((void)((EX) || assertfail(#EX, __FILE__, __LINE__)))
#define	CL_PANIC(EX) ((void)((EX) || assertfail(#EX, __FILE__, __LINE__)))

#endif	/* defined(_KERNEL_ORB) */

#else	/* defined(DEBUG) */

#define	ASSERT(x)
#define	CL_PANIC(EX) ((void)((EX) || assertfail(#EX, __FILE__, __LINE__)))
#endif	/* defined(DEBUG) */

#endif	/* defined(_KERNEL) */

#ifdef  __cplusplus
}
#endif

#endif	/* _CL_ASSERT_H */
