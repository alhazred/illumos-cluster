/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _LIST_DEF_H
#define	_LIST_DEF_H

#pragma ident	"@(#)list_def.h	1.60	08/05/20 SMI"

#include <sys/os.h>

//
// This contains the generic list classes
//
// DO NOT USE THE _DList/_SList classes directly, they only provide base
// classes to share implementation.  Template classes DList, SList, IntrList
// provide the casting and the public interfaces.
//
// XXX Need comments/man pages on how to use the lists

// Note that the destructors in these classes are not declared virtual to
// avoid the overhead of creating template .o files and vtables.
// This has the side effect that if a class inherits from one of these, care
// must be taken to always call the destructor of the derived class and not
// the base class.
// If this starts to get confusing or has errors, we can use virtual destructors
//lint -esym(1512, _DList)
//lint -esym(1512, _SList)
//lint -esym(1512, SList*)
//lint -esym(1512, DList*)
//lint -esym(1512, IntrList*)
//lint -esym(1509, _DList)
//lint -esym(1509, _SList)
//lint -esym(1509, SList*)
//lint -esym(1509, DList*)
//lint -esym(1509, IntrList*)

// The list classes include an in-built iterator that maintains a current ptr
// In general, try to use the ListIterator classes instead of directly using
// the lists in-built iterator. The code is more readable that way.

// One of the common errors when using lists has been with using erase()
// erase removes an element from the list. In addition if the "current" pointer
// is at the element being removed it will advance the current pointer.
// So the right way to write a loop removing some elements from a list is
//	list.atfirst();
//	while ((p = list.get_current()) != NULL) {
//		list.advance();	// *** advance should be before erase()
//		if (some condition is true)
//			list.erase(p);
//	}
//
// To remove all elements from a list use reapfirst which does the erase better
//	while ((p = reapfirst()) != NULL) {
//		/* do something with p which is now not on the list */
//	}

// XX The usual convention for inline functions is to put them _in.h files
// However due to compiler bugs regarding templates, three categories of
// inlines are not working in _in.h and are included here
// 1) Workaround for Sun C++ complier 4.2 bug 4012797 for intel
//	constructors of member classes of a template must be defined inline.
// 2) Workaround for intel build due to bug 4256054
//	methods of member classes of a template must be defined inline.
// 3) methods that include in the argument list a member from a class that
//	is a parameter to the template
//	e.g. template <class A, class B>
//		method(A::footype)

//
// _DList - the class supports a doubly linked list of "ListElem"s.
// Do not use this class directly - use the template DList class below
// Or IntrList<T, _DList> below
//
class _DList {
public:
	class ListElem : public knewdel {
		friend class _DList;
	public:
		ListElem(void *e);

		void *elem() const;

		// Returns the next element in the list
		// Return NULL if the next element is NULL
		void *next_elem() const;

		// Used by the ListIterator only
		ListElem *next() const;

		// Used to set to NULL/ASSERT invariants when
		// adding/removing ListElems from lists
		// This ensures that a ListElem is on only 1 list at a time
		// and reduces the probability of list pointer corruption
		void ptr_assert();
		void ptr_reset();

	private:
		void * const _elem;
		ListElem *_next;
		ListElem *_prev;

		// Disallow assignments and pass by value
		ListElem(const ListElem&);
		ListElem& operator = (ListElem&);
	};

	// Constructor
	_DList();

	// Destructor
	~_DList();

	bool empty();
	void advance(void);
	void retreat(void);

	void split(_DList&);
	void backsplit(_DList&);
	void concat(_DList&);

	void atfirst(void);
	void atlast(void);

	uint_t count(void);

	// Returns _first, Used only in ListIterator. Dont use otherwise
	ListElem *first() const;

protected:
	// Append element to end of list leaving current position unchanged
	void _append(ListElem *le);

	// Place element at start of list.
	// Only if the current position was NULL, the new element become current
	void _prepend(ListElem *le);

	// Insert new element before current position
	void _insert_b(ListElem *le);

	// Insert new element after current position
	void _insert_a(ListElem *le);

	// This removes the list element pointed to from the list
	// Does not delete the ListElem itself
	bool _erase_listelem(ListElem *le);

	// This removes the list element containing _elem == t from the list
	// It also deletes the ListElem. Used in DList
	bool _erase_elem(void *t);

	// Extract first element from list regardless of current position
	// Semantics similar to erase
	void *_reapfirst(bool del);

	void *elem(void) const;

	// Return whether an element exists in the list
	// The bool argument indicates whether or not _current should be
	// updated
	bool _exists(void *t, bool update);

	// Iterator type 1 - requires a static callback function but a single
	//		call to list_iterate will suffice

	//
	// Callback function type.  First argument passed to the callback
	// function is a ListElem's entry.  Second argument is the
	// callback data (specified in call to iterate()).
	//
	typedef bool (*list_callback_t)(void *entryp, void *callback_data);

	//
	// Iterate through list, and for each ListElem found pass its entry
	// and callback_data to the callback function callbackf.  callbackf
	// function can stop the iteration before all ListElems have been
	// processed by returning false.  If the iteration stops in this
	// way, the function returns the element where it stopped,
	// otherwise it returns NULL.
	//
	// This function can be used when one or more iterations are needed
	// (in parallel or serially) without changing the state (_first,
	// _last, and _current) of the list.  Compare this to the other
	// iterator functions, e.g. atfirst(), advance(), etc.  However,
	// callers are responsible for ensuring that the state of the list
	// is not modified during the iteration, e.g. by using locks.
	//
	void *list_iterate(void *callbackf, void *callback_data);

	// Iterator type 2 - Instantiate a stack variable (or otherwise) of
	//	DList<T>::ListIterator or similar (see Region::span() in Buf.cc)
	//	Then use iter.get_current to get the current element which
	//	is initialized to _first. Use advance() to advance the iterator
	//	to the next element and use get_current()
	//	Caller needs to ensure that the list does not change while
	//	iterator is in scope.

private:
	ListElem *find_elem(void *t);

	// The del parameter controls whether the ListElem itself is deleted
	// or not. In IntrLists, the ListElems are embedded in some other
	// structure and should not be deleted as part of an erase operation
	// In DLists, the ListElems are created on add and should be deleted
	bool _erase_internal(ListElem *le, bool del);
	//
	// Member variables provide access to doubly linked list
	//
	ListElem * _first;
	ListElem * _last;
	ListElem * _current;

	// Disallow assignments and pass by value
	_DList(const _DList&);
	_DList& operator = (_DList&);
};

// _SList - Singly Linked List
// Do not use this class directly - use the template DList class below
// Or IntrList<T, _SList> below
class _SList {
public:
	class ListElem : public knewdel {
		friend class _SList;
	public:
		ListElem(void *e);

		void *elem() const;

		// Returns the next element in the list
		// Return NULL if the next element is NULL
		void *next_elem() const;

		// Used by the ListIterator only
		ListElem *next() const;

		// Used to set to NULL/ASSERT invariants when
		// adding/removing ListElems from lists
		// This ensures that a ListElem is on only 1 list at a time
		// and reduces the probability of list pointer corruption
		void ptr_assert();
		void ptr_reset();
	private:
		void * const _elem;
		ListElem *_next;

		// Disallow assignments and pass by value
		ListElem(const ListElem&);
		ListElem& operator = (ListElem&);
	};

	// Constructor
	_SList();

	// Destructor
	~_SList();

	bool empty();
	void advance(void);
	void retreat(void);

	void split(_SList&);
	void backsplit(_SList&);
	void concat(_SList&);

	void atfirst(void);
	void atlast(void);

	uint_t count(void);

	// Returns _first, Used only in ListIterator. Dont use otherwise
	ListElem *first() const;

protected:
	// Append element to end of list leaving current position unchanged
	void _append(ListElem *le);

	// Place element at start of list.
	// Only if the current position was NULL, the new element become current
	void _prepend(ListElem *le);

	// Insert new element before current position
	void _insert_b(ListElem *le);

	// Insert new element after current position
	void _insert_a(ListElem *le);

	// This removes the list element pointed to from the list
	// Does not delete the ListElem itself
	bool _erase_listelem(ListElem *le);

	// This removes the list element containing _elem == t from the list
	// It also deletes the ListElem. Used in SList where the ListElem
	// is created on each add
	bool _erase_elem(void *t);

	// Extract first element from list regardless of current position
	// Semantics similar to erase
	void *_reapfirst(bool del);

	void *elem(void) const;

	// Return whether an element exists in the list
	// The bool argument indicates whether or not _current should be
	// updated
	bool _exists(void *t, bool update);

	// Iterator type 1 - requires a static callback function but a single
	//		call to list_iterate will suffice

	//
	// Callback function type.  First argument passed to the callback
	// function is a ListElem's entry.  Second argument is the
	// callback data (specified in call to iterate()).
	//
	typedef bool (*list_callback_t)(void *entryp, void *callback_data);

	//
	// Iterate through list, and for each ListElem found pass its entry
	// and callback_data to the callback function callbackf.  callbackf
	// can stop the iteration before all ListElems have been processed
	// by returning false.  If the iteration stops in this way, the
	// function returns the element where it stopped, otherwise it
	// returns NULL.
	//
	// This function can be used when one or more iterations are needed
	// (in parallel or serially) without changing the state (_first,
	// _last, and _current) of the list.  Compare this to the other
	// iterator functions, e.g. atfirst(), advance(), etc.  However,
	// callers are responsible for ensuring that the state of the list
	// is not modified during the iteration, e.g. by using locks.
	//
	void *list_iterate(void *callbackf, void *callback_data);

	// Iterator type 2 - Instantiate a stack variable (or otherwise) of
	//	DList<T>::ListIterator or similar (see Region::span() in Buf.cc)
	//	Then use iter.get_current to get the current element which
	//	is initialized to _first. Use advance() to advance the iterator
	//	to the next element and use get_current()
	//	Caller needs to ensure that the list does not change while
	//	iterator is in scope.

private:
	ListElem *find_elem(void *t, ListElem **pe = NULL);
	ListElem *find_prev(ListElem *le);
	//
	// Member variables provide access to singly linked list
	//
	ListElem * _first;
	ListElem * _last;
	ListElem * _current;

	// Disallow assignments and pass by value
	_SList(const _SList&);
	_SList& operator = (_SList&);
};

//
// DList - support doubly linked lists.
// Template avoids casting
//
template<class T> class DList : public _DList {
public:
	class ListIterator {
	public:
		ListIterator() { _current = NULL; };
		ListIterator(_DList &listref) { reinit(listref); }
		ListIterator(_DList *listptr) { reinit(listptr); }

		// Use reinit to either restart the iterator or reuse an
		// iterator variable to iterator over a different list
		void reinit(_DList &listref) { _current = listref.first(); }
		void reinit(_DList *listptr) { _current = listptr->first(); }

		T *get_current() const {
		    return ((T *)(_current == NULL ? NULL : _current->elem()));
		}

		void advance() { _current = _current->next(); }

#ifdef DEBUGGER_PRINT
		T *mdb_get_current() const;
		void mdb_advance();
#endif

	private:
		_DList::ListElem *_current;
	};
	//
	// allocated is a default parameter used to indicate whether
	// caller is interested in knowing the status of dynamic memory
	// allocation.
	// allocated = NULL -- indicates caller is not interested.
	// allocated = any_other_value -- indicates caller is interested.
	// *allocated is set to true if allocation succeeds else
	// *allocated is set to false.
	//
	void	append(T *t, bool *allocated = NULL);
	void	append_nosleep(T *t, bool *allocated = NULL);
	void	prepend(T *t, bool *allocated = NULL);
	void	insert_b(T *t, bool *allocated = NULL);
	void	insert_a(T *t, bool *allocated = NULL);
	bool	erase(T *t);
	T	*reapfirst();

	// Return whether an element exists in the list
	// The bool argument indicates whether or not _current should be
	// updated
	bool exists(T *t, bool update = false);

	// Return the current element from the list
	T	*get_current(void) const;

	// NOTE: Call dispose only when all the elements were created with new
	void dispose();

	// remove and delete any ListElems that are on the list
	// Note: There is a potential for a memory leak if the elements stored
	// in the list need to be deleted/further processed. Caller needs
	// to make sure that the elements on the list are tracked somewhere
	// else also and are appropriately garbage collected
	void erase_list();

	//
	// Iterate through list, and for each ListElem found pass its entry
	// and callback_data to the callback function callbackf.  callbackf
	// can stop the iteration before all ListElems have been processed
	// by returning false.  If the iteration stops in this way, the
	// function returns the element where it stopped, otherwise it
	// returns NULL.
	//
	// This function can be used when one or more iterations are needed
	// (in parallel or serially) without changing the state (_first,
	// _last, and _current) of the list.  Compare this to the other
	// iterator functions, e.g. atfirst(), advance(), etc.  However,
	// callers are responsible for ensuring that the state of the list
	// is not modified during the iteration, e.g. by using locks.
	//
	T *iterate(bool (*callbackf)(T *, void *), void *callback_data);
};

// SList - supports singly linked lists
// template supports casting
template<class T> class SList : public _SList {
public:
	class ListIterator {
	public:
		ListIterator() { _current = NULL; };
		ListIterator(_SList &listref) { reinit(listref); }
		ListIterator(_SList *listptr) { reinit(listptr); }

		// Use reinit to either restart the iterator or reuse an
		// iterator variable to iterator over a different list
		void reinit(_SList &listref) { _current = listref.first(); }
		void reinit(_SList *listptr) { _current = listptr->first(); }

		T *get_current() const {
		    return ((T *)(_current == NULL ? NULL : _current->elem()));
		}

		void advance() { _current = _current->next(); }
#ifdef DEBUGGER_PRINT
		T *mdb_get_current() const;
		void mdb_advance();
#endif
	private:
		_SList::ListElem *_current;
	};

	//
	// allocated is a default parameter used to indicate whether
	// caller is interested in knowing the status of dynamic memory
	// allocation.
	// allocated = NULL		-- indicates caller is not interested.
	// allocated = any_other_value	-- indicates caller is interested.
	// *allocated is set to true if allocation succeeds else
	// *allocated is set to false.
	//
	void	append(T *t, bool *allocated = NULL);
	void	prepend(T *t, bool *allocated = NULL);
	void	insert_b(T *t, bool *allocated = NULL);
	void	insert_a(T *t, bool *allocated = NULL);
	bool	erase(T *t);
	T	*reapfirst();

	// Return whether an element exists in the list
	// The bool argument indicates whether or not _current should be
	// updated
	bool exists(T *t, bool update = false);

	// Return the current element from the list
	T	*get_current(void) const;

	// NOTE: Call dispose only when all the elements were created with new
	void dispose();

	// remove and delete any ListElems that are on the list
	// Note: There is a potential for a memory leak if the elements stored
	// in the list need to be deleted/further processed. Caller needs
	// to make sure that the elements on the list are tracked somewhere
	// else also and are appropriately garbage collected
	void erase_list();

	//
	// Iterate through list, and for each ListElem found pass its entry
	// and callback_data to the callback function callbackf.  callbackf
	// can stop the iteration before all ListElems have been processed
	// by returning false.  If the iteration stops in this way, the
	// function returns the element where it stopped, otherwise it
	// returns NULL.
	//
	// This function can be used when one or more iterations are needed
	// (in parallel or serially) without changing the state (_first,
	// _last, and _current) of the list.  Compare this to the other
	// iterator functions, e.g. atfirst(), advance(), etc.  However,
	// callers are responsible for ensuring that the state of the list
	// is not modified during the iteration, e.g. by using locks.
	//
	T *iterate(bool (*callbackf)(T *, void *), void *callback_data);
};

//
// IntrList
// Use this type of list when class T includes storage for the ListElem
// This avoids a new/delete in append/prepend/erase etc.
// Option a) class T should public inherit L::ListElem
// Option b) class T has an element of type L::ListElem and passes that in
//	append/prepend/insert/erase calls
//
template<class T, class L> class IntrList : public L {
public:
	class ListIterator {
	public:
		ListIterator() { _current = NULL; };
		ListIterator(L &listref) { reinit(listref); }
		ListIterator(L *listptr) { reinit(listptr); }

		// Use reinit to either restart the iterator or reuse an
		// iterator variable to iterator over a different list
		void reinit(L &listref) { _current = listref.first(); }
		void reinit(L *listptr) { _current = listptr->first(); }

		T *get_current() const {
		    return ((T *)(_current == NULL ? NULL : _current->elem()));
		}

		void advance()
		{
			ASSERT(_current != NULL);
			_current = _current->next();
		}

#ifdef DEBUGGER_PRINT
		void mdb_advance();
		T    *mdb_get_current() const;
#endif

	private:
		typename L::ListElem *_current;
	};

	void	append(typename L::ListElem *t)
	{
		L::_append(t);
	}

	void	prepend(typename L::ListElem *t)
	{
		L::_prepend(t);
	}

	void	insert_b(typename L::ListElem *t)
	{
		L::_insert_b(t);
	}

	void	insert_a(typename L::ListElem *t)
	{
		L::_insert_a(t);
	}

	bool	erase(typename L::ListElem *t)
	{
		return (L::_erase_listelem(t));
	}

	T	*reapfirst();

	// Return whether an element exists in the list
	// The bool argument indicates whether or not _current should be
	// updated
	bool exists(T *t, bool update = false)
	{
		return (L::_exists((void *)t, update));
	}

	bool exists(typename L::ListElem *t, bool update = false)
	{
		ASSERT(t != NULL);
		return (L::_exists(t->elem(), update));
	}

	// Return the current element from the list
	T	*get_current(void) const;

	// NOTE: Call dispose only when all the elements were created with new
	void dispose();

	// remove any ListElems that are on the list
	// Note: There is a potential for a memory leak if the elements stored
	// in the list need to be deleted/further processed. Caller needs
	// to make sure that the elements on the list are tracked somewhere
	// else also and are appropriately garbage collected
	// This is actually a noop for IntrLists, but is useful to catch leaks
	void erase_list();

	//
	// Iterate through list, and for each T found pass a pointer to it
	// and callback_data to the callback function callbackf.  callbackf
	// can stop the iteration before all T's have been processed by
	// returning false.  If the iteration stops in this way, the
	// function returns the element where it stopped, otherwise it
	// returns NULL.
	//
	// This function can be used when one or more iterations are needed
	// (in parallel or serially) without changing the state (_first,
	// _last, and _current) of the list.  Compare this to the other
	// iterator functions, e.g. atfirst(), advance(), etc.  However,
	// callers are responsible for ensuring that the state of the list
	// is not modified during the iteration, e.g. by using locks.
	//
	// Note, IntrList implicitly assumes that the _elem member of each
	// L::ListElem points to T (hint: see how the get_current works above)
	//
	T *iterate(bool (*callbackf)(T *, void *), void *callback_data);
};

#ifndef NOINLINES
#include <sys/list_def_in.h>
#endif  // _NOINLINES

#endif	/* _LIST_DEF_H */
