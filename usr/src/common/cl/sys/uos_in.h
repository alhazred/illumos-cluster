/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  uos_in.h
 *
 */

#ifndef _UOS_IN_H
#define	_UOS_IN_H

#pragma ident	"@(#)uos_in.h	1.25	08/05/20 SMI"

//
// These are the userland definitions of os.h inline functions.
//

inline os::threadid_t
os::threadid()
{
#ifdef linux
	return ((os::threadid_t) pthread_self());
#else
	return ((os::threadid_t) thr_self());
#endif
}

#ifdef linux
#include <pthread.h>
#include <signal.h>
#else
#include <thread.h>
#endif

inline int
os::thread::create(void *stackbase, size_t stacksize, void *(*func)(void *),
    void *arg, long flags, threadid_t *tidp)
{
#ifdef linux
	pthread_t tid;
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	if (flags && THR_DETACHED)
		pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	if (flags && THR_BOUND)
		pthread_attr_setscope(&attr, PTHREAD_SCOPE_SYSTEM);
	if (tidp == NULL)
		tidp = &tid;
	return (pthread_create(tidp, &attr, func, arg));
#else
	return (thr_create(stackbase, stacksize, func, arg, flags, tidp));
#endif
}

inline void
os::thread::exit(void *status)
{
#ifdef linux
	::pthread_exit(status);
#else
	::thr_exit(status);
#endif
}

inline int
os::thread::join(threadid_t tid, threadid_t *departed, void **status)
{
#ifdef linux
	return (::pthread_join(tid, status));
#else
	return (::thr_join(tid, departed, status));
#endif
}

inline void
os::thread::sigsetmask(int how, sigset_t *set, sigset_t *oset)
{
#ifdef linux
	sigprocmask(how, set, oset);
#else
	::thr_sigsetmask(how, set, oset);
#endif
}

inline int
os::thread::getconcurrency()
{
#ifdef linux
	return (0);
#else
	return (::thr_getconcurrency());
#endif
}

inline int
os::thread::setconcurrency(int val)
{
#ifdef linux
	return (0);
#else
	return (::thr_setconcurrency(val));
#endif
}

inline int
os::thread::main(void)
{
#ifdef linux
	return (0);
#else
	return (::thr_main());
#endif
}

#ifdef linux
#include <pwd.h>
#endif

inline struct passwd *
os::getpwuid(uid_t uid, struct passwd *pwd, char *buffer, int buflen)
{
#if defined(linux) || defined(_POSIX_PTHREAD_SEMANTICS)
	struct passwd *ret;
	if (::getpwuid_r(uid, pwd, buffer, buflen, &ret))
		return (NULL);
	return (ret);
#else
	return (::getpwuid_r(uid, pwd, buffer, buflen));
#endif
}

inline struct passwd *
os::getpwnam(const char *name, struct passwd *pwd, char *buffer, int buflen)
{
#if defined(linux) || defined(_POSIX_PTHREAD_SEMANTICS)
	struct passwd *ret;
	if (::getpwnam_r(name, pwd, buffer, buflen, &ret))
		return (NULL);
	return (ret);
#else
	return (::getpwnam_r(name, pwd, buffer, buflen));
#endif
}

#ifndef linux
#include <sys/systeminfo.h>
#else
#include <sys/utsname.h>
#endif

inline int
os::sysinfo(int command, char *buf, long count)
{
#ifdef linux
	struct utsname info;
	if (uname(&info) != 0)
		return (-1);
	switch (command) {
	case SI_RELEASE:
		strcpy(buf, info.release);
		break;
	case SI_HOSTNAME:
		strcpy(buf, info.nodename);
		break;
	default:
		ASSERT(0);
	}
	return (0);
#else
	return (::sysinfo(command, buf, count));
#endif
}

inline void *
/*CSTYLED*/
operator new(size_t len, os::mem_zero_type flag)
{
	void *p = operator new(len);
	if (flag == os::ZERO)
		::memset(p, 0, len);
	return (p);
}

inline void *
/*CSTYLED*/
operator new[](size_t len, os::mem_zero_type flag)
{
	void *p = operator new[] (len);
	if (flag == os::ZERO)
		::memset(p, 0, len);
	return (p);
}

inline void *
/*CSTYLED*/
operator new(size_t len, os::mem_alloc_type)
{
	return (operator new(len));
}

inline void *
/*CSTYLED*/
operator new[](size_t len, os::mem_alloc_type)
{
	return (operator new[] (len));
}

inline void *
/*CSTYLED*/
operator new(size_t len, os::mem_alloc_type, os::mem_zero_type zflag)
{
	void *p = operator new(len);
	if ((zflag == os::ZERO) && (p != NULL))
		::memset(p, 0, len);
	return (p);
}

inline void *
/*CSTYLED*/
operator new[](size_t len, os::mem_alloc_type, os::mem_zero_type zflag)
{
	void *p = operator new[] (len);
	if ((zflag == os::ZERO) && (p != NULL))
		::memset(p, 0, len);
	return (p);
}

//
// os::mutex_t
//
inline
os::mutex_t::mutex_t()
{
#ifdef linux
	::pthread_mutex_init(&_mutex, NULL);
#else
	::mutex_init(&_mutex, USYNC_THREAD, NULL);
#endif
	_owner = NO_THREAD;
}

inline
os::mutex_t::~mutex_t()
{
#ifdef linux
	::pthread_mutex_destroy(&_mutex);
#else
	::mutex_destroy(&_mutex);
#endif
}

inline void
os::mutex_t::lock()
{
	ASSERT(_owner != threadid());
#ifdef linux
	::pthread_mutex_lock(&_mutex);
#else
	::mutex_lock(&_mutex);
#endif
	_owner = os::threadid();
}

inline int
os::mutex_t::try_lock()
{
	//
	// mutex_trylock() returns an errno value while the kernel version
	// returns true/false for whether the lock was acquired/not acquired.
	// We convert the return value here to match the kernel.
	//
	ASSERT(_owner != os::threadid());
#ifdef linux
	int retval = (::pthread_mutex_trylock(&_mutex) == 0);
#else
	int retval = (::mutex_trylock(&_mutex) == 0);
#endif
	if (retval) {
		_owner = os::threadid();
	}
	return (retval);
}

inline void
os::mutex_t::unlock()
{
	ASSERT(_owner == os::threadid());
	_owner = NO_THREAD;
#ifdef linux
	::pthread_mutex_unlock(&_mutex);
#else
	::mutex_unlock(&_mutex);
#endif
}

inline void
os::mutex_t::set_owner()
{
	_owner = os::threadid();
}

inline int
os::mutex_t::lock_held()
{
	return (_owner == os::threadid());
}

inline int
os::mutex_t::lock_not_held()
{
	return (!(_owner == os::threadid()));
}

inline uint64_t
os::mutex_t::owner()
{
#ifdef linux
	return (0);
#else
	//
	// This assumes the current Solaris mutex implementation
	//
	return (_mutex.data);
#endif
}

//
// os::rwlock_t
//
inline
os::rwlock_t::rwlock_t()
{
#ifdef linux
	::pthread_rwlock_init(&_rwlock, NULL);
#else
	::rwlock_init(&_rwlock, USYNC_THREAD, NULL);
#endif
}

inline
os::rwlock_t::~rwlock_t()
{
#ifdef linux
	::pthread_rwlock_destroy(&_rwlock);
#else
	::rwlock_destroy(&_rwlock);
#endif
}

inline void
os::rwlock_t::rdlock()
{
#ifdef linux
	::pthread_rwlock_rdlock(&_rwlock);
#else
	::rw_rdlock(&_rwlock);
#endif
}

inline void
os::rwlock_t::wrlock()
{
#ifdef linux
	::pthread_rwlock_wrlock(&_rwlock);
#else
	::rw_wrlock(&_rwlock);
#endif
}

inline int
os::rwlock_t::try_rdlock()
{
#ifdef linux
	return (::pthread_rwlock_tryrdlock(&_rwlock) == 0);
#else
	return (::rw_tryrdlock(&_rwlock) == 0);
#endif
}

inline int
os::rwlock_t::try_wrlock()
{
#ifdef linux
	return (::pthread_rwlock_trywrlock(&_rwlock) == 0);
#else
	return (::rw_trywrlock(&_rwlock) == 0);
#endif
}

inline void
os::rwlock_t::unlock()
{
#ifdef linux
	::pthread_rwlock_unlock(&_rwlock);
#else
	::rw_unlock(&_rwlock);
#endif
}

inline int
os::rwlock_t::lock_held()
{
#ifdef linux
	return (read_held() || write_held());
#else
	return (RW_LOCK_HELD(&_rwlock));
#endif
}

inline int
os::rwlock_t::read_held()
{
#ifdef linux
	return (_rwlock.__rw_lock.__spinlock > 0);
#else
	return (RW_READ_HELD(&_rwlock));
#endif
}

#ifdef linux
extern "C" int gettid(void);
#endif

inline int
os::rwlock_t::write_held()
{
#ifdef linux
	//
	// This only checks that some thread has the lock held not
	// necessarily the caller. The problem is that the clone() ID
	// is stored here instead of the pthread_self() ID and there
	// is no easy way to get the clone() ID of the caller.
	// Also, this code depends on the implementation details
	// of pthread_rwlock_t and may break when a new version of Linux
	// is used.
	//
	return (_rwlock.__rw_pshared == gettid());
#else
	return (RW_WRITE_HELD(&_rwlock));
#endif
}

// The methods try_upgrade() and downgrade() are not supported in userland.
// There is no corresponding solaris system call to wrap around.

//
// os::sem_t
//
inline
os::sem_t::sem_t(uint_t initval)
{
#ifdef linux
	::sem_init(&_sem, 0, initval);
#else
	::sema_init(&_sem, initval, USYNC_THREAD, NULL);
#endif
}

inline
os::sem_t::sem_t()
{
#ifdef linux
	::sem_init(&_sem, 0, 0);
#else
	::sema_init(&_sem, 0, USYNC_THREAD, NULL);
#endif
}

inline
os::sem_t::~sem_t()
{
#ifdef linux
	::sem_destroy(&_sem);
#else
	::sema_destroy(&_sem);
#endif
}

inline void
os::sem_t::p()
{
	os::envchk::check_nonblocking(os::envchk::NB_INTR | os::envchk::NB_INVO,
	    "sema_wait");
#ifdef linux
	::sem_wait(&_sem);
#else
	::sema_wait(&_sem);
#endif
}

inline int
os::sem_t::tryp()
{
#ifdef linux
	return (::sem_trywait(&_sem));
#else
	return (::sema_trywait(&_sem));
#endif
}

inline void
os::sem_t::v()
{
#ifdef linux
	::sem_post(&_sem);
#else
	::sema_post(&_sem);
#endif
}

//
// os::tsd
//
inline
os::tsd::tsd(freef_t freef)
{
#ifdef linux
	if ((::pthread_key_create(&_key, freef)) == -1) {
#else
	if ((::thr_keycreate(&_key, freef)) == -1) {
#endif
		os::panic("os::tsd: thr_keycreate");
	}
}

inline
os::tsd::~tsd()
{
	// the userland thread library doesn't need a function to free the key
}

inline uintptr_t
os::tsd::get_tsd() const
{
	uintptr_t val;
#ifdef linux
	val = (uintptr_t)::pthread_getspecific(_key);
#else
	(void) ::thr_getspecific(_key, (void **)&val);
#endif
	return (val);
}

inline void
os::tsd::set_tsd(uintptr_t val)
{
#ifdef linux
	val = (uintptr_t)::pthread_setspecific(_key, (void *)val);
#else
	(void) ::thr_setspecific(_key, (void *)val);
#endif
}

//
// os::condvar_t
//
inline
os::condvar_t::condvar_t()
{
#ifdef linux
	::pthread_cond_init(&_cv, NULL);
#else
	::cond_init(&_cv, USYNC_THREAD, NULL);
#endif
#ifdef DEBUG
	num_waiting = 0;
	wait_lock = NULL;
#endif
}

inline
os::condvar_t::~condvar_t()
{
	ASSERT(num_waiting == 0);
#ifdef linux
	::pthread_cond_destroy(&_cv);
#else
	::cond_destroy(&_cv);
#endif
}

inline void
os::condvar_t::wait(os::mutex_t *lockp)
{
	os::envchk::check_nonblocking(os::envchk::NB_INVO, "cond_wait");
	debug_pre(lockp);
	lockp->_owner = NO_THREAD;
#ifdef linux
	::pthread_cond_wait(&_cv, &lockp->_mutex);
#else
	::cond_wait(&_cv, &lockp->_mutex);
#endif
	lockp->_owner = threadid();
	debug_post();
}

inline os::condvar_t::wait_result
os::condvar_t::wait_sig(os::mutex_t *lockp)
{
	wait_result result;

	os::envchk::check_nonblocking(os::envchk::NB_INTR | os::envchk::NB_INVO,
	    "cond_wait");
	debug_pre(lockp);
	lockp->_owner = NO_THREAD;
	// If EINTR, woke up by signal, return 0, else return 1
#ifdef linux
	if (::pthread_cond_wait(&_cv, &lockp->_mutex) == EINTR) {
#else
	if (::cond_wait(&_cv, &lockp->_mutex) == EINTR) {
#endif
		result = SIGNALED;
	} else {
		result = NORMAL;
	}
	lockp->_owner = threadid();
	debug_post();
	return (result);
}

inline os::condvar_t::wait_result
os::condvar_t::timedwait(os::mutex_t *lockp, systime *timeout)
{
	wait_result result;

	os::envchk::check_nonblocking(os::envchk::NB_INVO, "cond_timedwait");
	debug_pre(lockp);
	lockp->_owner = NO_THREAD;
#ifdef linux
	if (::pthread_cond_timedwait(&_cv, &lockp->_mutex, &timeout->_time)
	    == ETIME) {
#else
	if (::cond_timedwait(&_cv, &lockp->_mutex, &timeout->_time) == ETIME) {
#endif
		result = TIMEDOUT;
	} else {
		// Note that for userland, a signal or fork() could cause this
		// result.
		result = NORMAL;
	}
	lockp->_owner = threadid();
	debug_post();
	return (result);
}

inline os::condvar_t::wait_result
os::condvar_t::timedwait_sig(os::mutex_t *lockp, systime *timeout)
{
	wait_result result;

	os::envchk::check_nonblocking(os::envchk::NB_INTR | os::envchk::NB_INVO,
	    "cond_timedwait");
	debug_pre(lockp);
	lockp->_owner = NO_THREAD;
#ifdef linux
	switch (::pthread_cond_timedwait(&_cv, &lockp->_mutex,
	    &timeout->_time)) {
#else
	switch (::cond_timedwait(&_cv, &lockp->_mutex, &timeout->_time)) {
#endif
	case 0:
		result = NORMAL;
		break;
	case ETIME:
		result = TIMEDOUT;
		break;
	case EINTR:
		result = SIGNALED;
		break;
	}
	lockp->_owner = threadid();
	debug_post();
	return (result);
}

inline void
os::condvar_t::signal()
{
#ifdef linux
	::pthread_cond_signal(&_cv);
#else
	ASSERT(wait_lock == NULL || wait_lock->lock_held());
	::cond_signal(&_cv);
#endif
}

inline void
os::condvar_t::broadcast()
{
#ifdef linux
	::pthread_cond_broadcast(&_cv);
#else
	ASSERT(wait_lock == NULL || wait_lock->lock_held());
	::cond_broadcast(&_cv);
#endif
}

inline
os::rfile::rfile() :
	f(NULL)
{
}

inline
os::rfile::~rfile()
{
	if (f)
		close();
}

inline bool
os::rfile::open(char *pname)
{
	ASSERT(f == NULL);
	return ((f = ::fopen(pname, "r")) != NULL);
}

inline bool
os::rfile::isopen()
{
	return (f != NULL);
}

inline size_t
os::rfile::read(char *buf, size_t size, size_t nitems)
{
	ASSERT(f);
	return (::fread(buf, size, nitems, f));
}

inline int
os::rfile::fgetc()
{
	ASSERT(f);
	return (::fgetc(f));
}

inline int
os::rfile::ungetc(int c)
{
	ASSERT(f);
	return (::ungetc(c, f));
}

inline void
os::rfile::close()
{
	if (f)
		if (!::fclose(f));
			f = NULL;
}

inline
os::rdir::rdir() :
	d(NULL)
{
}

inline
os::rdir::~rdir()
{
	if (d)
		close();
}

inline bool
os::rdir::open(char *dname)
{
	ASSERT(d == NULL);
	return ((d = ::opendir(dname)) != NULL);
}

inline bool
os::rdir::isopen()
{
	return (d != NULL);
}

inline os::dirent_t *
os::rdir::read()
{
	ASSERT(d);
	return (::readdir(d));
}

inline void
os::rdir::close()
{
	if (d)
		if (!::closedir(d))
			d = NULL;
}

inline int
os::mkdirp(const char *path, mode_t mode)
{
	return (::mkdirp(path, mode));
}

inline void
os::systime::set(os::systime &other)
{
	_time.tv_sec = other._time.tv_sec;
	_time.tv_nsec = other._time.tv_nsec;
}

inline int
os::ctype::is_digit(int c)
{
	return (isdigit(c));
}

inline int
os::ctype::is_alpha(int c)
{
	return (isalpha(c));
}

inline int
os::ctype::is_xdigit(int c)
{
	return (isxdigit(c));
}

inline int
os::ctype::is_lower(int c)
{
	return (islower(c));
}

inline int
os::ctype::is_upper(int c)
{
	return (isupper(c));
}

inline int
os::ctype::is_space(int c)
{
	return (isspace(c));
}

inline int
os::atoi(const char *p)
{
	return (::atoi(p));
}

inline struct netent *
os::getnetbyname_r(const char *name, struct netent *result_buf,
    char *buffer, int *res)
{
	struct netent *result;

#ifdef linux
	if (::getnetbyname_r(name, result_buf, buffer,
	    (size_t)sizeof (buffer), &result, res) == -1) {
		result = (struct netent *)0;
	}
#else
	result = ::getnetbyname_r(name, result_buf, buffer,
	    (int)sizeof (buffer));
	res = &errno;
#endif
	return (result);
}

inline struct rpcent *
os::getrpcbyname_r(const char *name, struct rpcent *result_buf,
	char *buffer, int buflen)
{
	struct rpcent *result;
#ifdef linux
	if (::getrpcbyname_r(name, result_buf, buffer, (size_t)buflen,
	&result) == -1) {
		result = (struct rpcent *)0;
	}
#else
	result = ::getrpcbyname_r(name, result_buf, buffer, buflen);
#endif
	return (result);
}

#endif	/* _UOS_IN_H */
