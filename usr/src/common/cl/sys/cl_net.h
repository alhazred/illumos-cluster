/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_CL_NET_H
#define	_CL_NET_H

#pragma ident	"@(#)cl_net.h	1.4	08/05/20 SMI"

#include <sys/types.h>

/*
 * Global definitions used by Sun Cluster networking stack.
 */

/*
 * Ioctls used by Sun Cluster networking stack.
 */

#define	CLIOCBASE	0xff7f0000

/*
 * Used by TCP Transport to communicate adapter information
 * to the heartbeat interceptor module cldlpihb.
 */
#define	CLIOCSRDINFO		(CLIOCBASE+1)

/*
 * Used by TCP Transport to communicate connection information
 * to the cltcpint module pushed on TCP connections maintained
 * by the TCP Transport.
 */
#define	CLIOCSTCPINTINFO	(CLIOCBASE+2)

/*
 * Used by fpconf to let the clfpstr module know of the fp_adap
 * object that owns the stream the clfpstr module is pushed on.
 */
#define	CLIOCSFPADP		(CLIOCBASE+3)

/*
 * Used by various Sun Cluster components opening network devices
 * to tell the clhbsndr module pushed on those streams that it
 * has been pushed on a private network device and the messages
 * flowing through is should be tagged with a particular b_band.
 */
#define	CLIOCSPRIVBBAND		(CLIOCBASE+4)

typedef struct {
	uchar_t	bband;
} bband_info_t;

/*
 * Computes VLAN device instance number given physical device instance
 * number and VLAN id.
 */
#define	VLAN_PPA(inst, vid)	((inst) + 1000*(vid))

#endif	/* _CL_NET_H */
