/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  threadpool_in.h
 *
 */

#ifndef _THREADPOOL_IN_H
#define	_THREADPOOL_IN_H

#pragma ident	"@(#)threadpool_in.h	1.20	08/08/01 SMI"

//
// defer_task methods
//

inline
defer_task::defer_task() :
	_SList::ListElem(this)
{
}

//
// threadpool_worker_t methods
//

inline
threadpool_worker_t::threadpool_worker_t(threadpool *poolp, int gen) :
	_SList::ListElem(this),
	my_state(WORK),
	threadpoolp(poolp),
	my_gen(gen),
	my_taskp(NULL)
{
}

//
// threadpool methods
//

//
// total_number_threads - Returns the total number of threads in the threadpool
//
inline int
threadpool::total_number_threads()
{
	return (thr_total);
}

//
// number_idle_threads - Returns the number of idle threads in the threadpool
//
inline int
threadpool::number_idle_threads()
{
	return (thr_idle);
}

inline void
threadpool::lock()
{
	threadlock.lock();
}

inline void
threadpool::unlock()
{
	threadlock.unlock();
}

inline bool
threadpool::lock_held()
{
	return (threadlock.lock_held());
}

inline os::mutex_t *
threadpool::get_lock()
{
	return (&threadlock);
}

inline uint_t
threadpool::task_count()
{
	return (count);
}

inline void
threadpool::remove_pending()
{
	lock();
	pending.dispose();
	unlock();
}

//
// wakeup_worker - Wake up a sleeping worker thread.
// Must adjust the idle thread count before waking up the worker thread,
// because that thread is no longer idle as soon as its woken up.
//
inline void
threadpool::wakeup_worker(threadpool_worker_t *thrp)
{
	// Decrement number of idle threads
	ASSERT(thr_idle > 0);
	thr_idle--;
	ASSERT(thr_idle <= thr_total);

	// Wake worker thread
	thrp->threadcv.signal();
}

//
// fence_task methods
//

inline
fence_task::fence_task(bool flag) :
	on_heap(flag)
{
}

inline void
fence_task::wait()
{
	waitcv.wait();
}

//
// work_task methods
//

inline
work_task::work_task(task_handler mymethod, void *argp) :
	method(mymethod),
	arg(argp)
{
}

inline
work_task::work_task(work_task &todo) :
	method(todo.method),
	arg(todo.arg)
{
}

#endif	/* _THREADPOOL_IN_H */
