/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _THREADPOOL_H
#define	_THREADPOOL_H

#pragma ident	"@(#)threadpool.h	1.52	08/10/13 SMI"

#include <sys/list_def.h>
#include <limits.h>

#include <sys/dbg_printf.h>

#define	NO_THREADPOOL_DBGBUF

#if defined(DBGBUFS) && !defined(NO_THREADPOOL_DBGBUF)
#define	THREADPOOL_DBGBUF
#endif

#ifdef	THREADPOOL_DBGBUF
extern dbg_print_buf threadpool_dbg;
#define	THR_DBG(a) threadpool_dbg.dbprintf a
#else
#define	THR_DBG(a)
#endif

// Number of threads used for averaging
#define	MAX_THREADS_AVG	10

// forward declarations
class threadpool;

//
// The threadpool class is a utility to manage a pool of threads servicing a
// work queue.
//
// The threadpool can be statically allocated or embedded in some other class
// (e.g., the transport class embeds a bunch of threadpools) or dynamically
// allocated with new. The threadpool can be constructed without any
// arguments and later set the properties using set_dynamic_threads(),
// change_num_threads(), change_max_threads(), set_name() methods or
// can be constructed with the values for the properties.
// In either case, work items can be added to the work queue before setting the
// properties.
//
// The following properties of the threadpool can be modified:
//
// a) Automatic thread number adjustment is controlled by the
//	bool property (dynamic_threadpool). When the system detects a lot
//	of items in the work queue, the system will create more worker threads.
//	Conversely, when the system notes that a lot of worker threads are
//	idle, the system destroys worker threads.
//
//	XXX - the formula for calculating the number of idle threads suffers
//	from rounding. The system underestimates the number of idle threads.
//	The "average idle thread" formula must be changed, if accurate
//	accounting is needed for threadpools with small numbers of threads.
//
//	When automatic thread number adjustment is disabled, the number of
//	threads changes only in response to explicit commands.
//
// b) Number of threads (or lwps).
//	If dynamic_threadpool property is true - this is the preferred
//	steady-state number. if false, then this is the fixed number of
//	threads created.
//
// c) Max number of threads (or lwps)
//	When dynamic_threadpool property is true, we allow an unbounded number
//	of threads to be created due to temporary spikes in load (actually this
//	is limited by the amount of physical memory and kernel resources
//	available). Setting the max number of threads will bound the max number
//	of threads created.
//
// d) Name.
//	A name should be associated with the threadpool for debugging ease

//
// Work is added to the queue by calling defer_processing() method.
//	The element added should be of type inherited from defer_task.
//	The base class defer_task sets up the list linkage so that no
//	additional memory allocations are needed.
//

//
// The threadpool code will then call the execute() method for each element in
// the work queue as threads become available. The execute method is
// responsible for deletion of the element or passing it to some other entity.
// The execute method() is called within a kernel thread/lwp in non-interrupt
// context and can block.
// Remember, however, that if too many threads are blocked
// processing of the work queue will be blocked too and it may deadlock.
// The number of threads parameter should be tuned based on expected workload.
// (The dynamic threads feature helps but is not a guarantee against
// deadlock)
//

// Other miscellany:
//
// Block - block_processing() method will checkpoint the current state of the
//	task list and redirect all future tasks added to a waiting list
//	This will prevent any new tasks from getting executed.
//	Use quiesce(QBLOCK_EMPTY) or quiesce(QEMPTY) if need to wait
//	for the current	tasks to be finished executing.
//
// Unblock - unblock_processing() method will resume processing the tasks, if
//	this is the last unblock.
//
//	Block/unblock are nestable - If multiple blocks are done, corresponding
//		number of unblocks must be done and only the last unblock will
//		reenable task processing.
//
// Quiesce - This routine waits for the threadpool to process all pending work
//	currently in the queue are processed. Behaviour depends on fence_type:
//
//	QFENCE - Will insert a fence in the queue and return when all current
//		work queued is processed. It will continue to allow further
//		work to be added to the queue.
//		Multiple fences can be added to the queue, each waits for the
//		fence_task to reach the end of the queue
//		NOTE: XXX Current implementation only works for the case of
//		threadpool without dynamic threads and number of threads is 1.
//		This is ASSERTed in quiesce() and the implementation needs to
//		be fixed to support other cases as required.
//
//	QEMPTY - Will wait till all threads are idle and the work queue is empty
//		It will continue to allow work to be added to the queue
//		This is only a hint, as work can be added to the queue even
//		after returning from this routine.
//		Note that it may take while for this routine to return as
//		the current task processing is unbounded.
//
//	QBLOCK_EMPTY - combines a block_processing with quiesce QEMPTY
//		This will block further processing from happening and will
//		return when all current processing is completed.
//		Caller needs to call unblock_processing to resume processing
//		(See notes above about nesting of block/unblock calls)
//		Note that it may take while for this routine to return as
//		the current task processing is unbounded.
//
//	XX More types can be added as needed.
//
// Shutdown - The threadpool will do an immediate shutdown if the
//	shutdown() method is called. Any new tasks are discarded (task_done())
//	and pending tasks are discarded too.
//	Note that there will be a deadlock if the thread doing the shutdown is
//	a threadpool thread. e.g., if shutdown is done in an incoming invocation
//		or in an unref handler.
//	If a clean shutdown is required, do a quiesce(QBLOCK_EMPTY)
//
// Respawn - If the threadpool is initialized or constructed very early in the
//	boot process, kernel threads are used. If done later, kernel lwps are
//	used. The latter is preferred. A threadpool can be converted to using
//	kernel lwps by calling the respawn() method after the boot process has
//	made sufficient progress.
//	respawn() will handle existing work queue and new requests being added
//	while doing the respawn and wont drop any.
//	The above scenario does not occur anymore in our code, but the same
//	routine can be used to change the scheduling properties of the theadpool
//	which uses respawn to spawn new threads
//

//
// Example code:
//
// For most uses, there is a threadpool declared in orbthreads.h
// common_threadpool that can be used for most purposes. Skip to tasks
// section of the comments below if you are going to use the common_threadpool
//
// Create a threadpool (you can also embed threadpool in another structure)
//
// constructor for transport_impl sets the attributes
//
//    common_threadpool(true, 1, "Common_threadpool")
//
//	Asks for dynamic creation of threads with 1 as the steady state count
//
// When all done, in shutdown, or in destructor of embedded class
//	common_threadpool.shutdown();
//
// The way you add work to the threadpool is to create a work item of type
// derived from defer_task.
//
// class foo_task : public defer_task {
// public:
// 	foo_task(bar_type *bar);
// 	~foo_task();
//	void execute();
// private:
//	bar_type *bar;
// };
//
// Work items are added to the queue by:
//
//	common_threadpool.defer_processing(new foo_task(&foo_inst));
//
// The callback handler could be as follows:
//
// foo_task::execute() {
//	bar->call_some_method();
//	// OR do something with bar....
//	delete this;
// }
//
// If you are uncomfortable with above syntax, there is a class "task"
// defined at the bottom of this file that allows one to specify a static
// method.

//
// defer_task - Base class to inherit from for adding tasks to a threadpool
//
class defer_task : public _SList::ListElem {
public:
	//
	// The DEFAULT_TAG is used by those entities that do not care about
	// a tag.
	//
	enum { DEFAULT_TAG = 0 };

	defer_task();

	virtual ~defer_task();
	//
	// Method called when the task is scheduled to run
	// the callee is responsible for deleting the task when done
	// (or callee can make sure that someone else will delete it
	//
	virtual void execute() = 0;

	//
	// Method called when the threadpool decides to throw away a task
	// This happens only during shutdown
	// The default implementation does a delete this; which is what
	// most tasks do. So only tasks that are not dynamically allocated
	// (e.g., embedded in a different structure) need to implement this
	//
	virtual void task_done();

	virtual uint_t get_defer_task_tag();

	//
	// Returns an address that identifies a critical data structure
	// for the defer_task
	//
	virtual void *get_address_tag();

private:
	// disallowed - pass by value and assignment
	defer_task(const defer_task &);
	defer_task &operator = (defer_task &);
};

//
// transfer_task - orders the worker thread transfer to a new threadpool.
// This task is used exclusively by the threadpool framework.
// The transfer_task obtains access to the threadpool_worker_t
// through thread-specific data.
//
class transfer_task : public defer_task {
public:
	transfer_task(threadpool *new_threadpoolp);

	virtual ~transfer_task();

	// This method transfers the thread to a new threadpool
	virtual void	execute();

private:
	threadpool	*threadpoolp;
};

//
// fence_task
// This is the task used in implementing the threadpool::quiesce function
// It is here as an example implementation of tasks
//
class fence_task : public defer_task {
public:
	fence_task(bool = true);
	void wait();
	void execute();
	void task_done();
private:
	os::notify_t	waitcv;
	bool		on_heap;
};

//
// work_task
// NOTE: The class work_task is there for convenience. It requires use of
// static methods. If you prefer to avoid using static methods (a matter of
// programming style), then see the comments at the beginning of the file
// on how to create tasks.
//
// encapsulate a work_task to be performed by an offline thread
// This type of work_task allows a way to define tasks that call
// static methods (and take one argument).
// The execute method calls task_done() after the handler returns.
// The default implementation of task_done does a delete this - if the task was
// created with a new. If the work_task was not created with a new, the task
// would need to implement a task_done method - see comments in defer_task
// class.
//
class work_task : public defer_task {
public:
	// Signature of static method that can be used to form such tasks
	typedef void (*task_handler)(void *);

	work_task(task_handler, void *);
	work_task(work_task &);

	void execute();

protected:
	virtual ~work_task();
private:
	task_handler	method;
	void		*arg;

	// disallowed - pass by value and assignment
	work_task(const work_task &);
	work_task &operator = (work_task &);
};

//
// threadpool_worker_t - class manages a worker thread for a threadpool.
//
class threadpool_worker_t : public _SList::ListElem {
	friend class threadpool;
	friend class transfer_task;
public:
	threadpool_worker_t(threadpool *, int);

	// The main loop for the worker thread that dispatches the tasks
	void		deferred_task_handler();


private:
	// This identifies the possible states for the worker thread
	enum worker_state_t {
		WORK,		// Ready to do work
		SLEEP,		// Thread is sleeping
		SURPLUS,	// Thread is surplus and must terminate
		CHANGE		// A thread generation occurred
	};

	worker_state_t	my_state;

	// Identifies the threadpool for which this thread works.
	threadpool	*threadpoolp;

	// Thread generation number
	int		my_gen;

	// This will be the task to perform when woken up.
	defer_task	*my_taskp;

	// Worker thread waits behind here when idle
	os::condvar_t	threadcv;

	//
	// Thread-specific data holds pointer to the threadpool_worker_t object
	// for this worker thread.
	//
	static os::tsd	worker;

	// Disallow assignments and pass by value
	threadpool_worker_t(const threadpool_worker_t &);
	threadpool_worker_t &operator = (threadpool_worker_t &);
};

typedef IntrList<defer_task, _SList> task_list_t;

//
// threadpool - class used to describe one pool of worker threads
//
class threadpool {
	friend class threadpool_worker_t;
	friend class transfer_task;
public:
	enum fence_t { QFENCE, QEMPTY, QBLOCK_EMPTY };

	//
	// constructor
	// First arg is dynamic threadpool property.
	// Second arg is thr_desired.
	// Third arg is name for threadpool.
	// Fourth arg is thr_max.
	// Fifth arg is the stack size for threadpool threads. If 0
	// it uses the system default stack size, if specified
	// the threadpool will preallocate the memory for the stack. Note
	// if you specify the stacksize you need to make sure that your
	// thread won't grow its stack beyond the specified size. This arg
	// is only used for user land.
	//
	threadpool(bool = false, int = 0, const char *name = NULL,
	    int = INT_MAX, size_t stacksize = 0);

	virtual	~threadpool();

	//
	// Can be used to change/set fields in the threadpool and start threads.
	// Mostly used by constructor and callers using the default constructor.
	//
	int	modify(bool, int, const char *name);

	//
	// Change the default thread scheduling properties.
	// The thread priority and scheduling class are settable.
	// Set class to NULL to get default.
	//
	// Returns 0 on success and errno on failure (invalid class name).
	//
	int	set_sched_props(int, const char *cname = NULL);

	// wrapper to call deferred_task_handler
	static void	thread_handler(threadpool_worker_t *);

	// Method to add a task to the pending queue to be processed
	void	defer_processing(defer_task *, bool prepend = false);

	//
	// Please read comments in threadpool.cc on the implementation
	// Not all cases are handled yet.
	//
	void	quiesce(fence_t);

	//
	// Block processing new tasks
	// This puts all new tasks in a separate list that will be processed
	// when unblocked.
	// Every block_processing should be matched with an unblock_processing
	//
	void	block_processing();

	// Resumes processing tasks if this is the last unblock
	void	unblock_processing();

	// Shutdown the threadpool. Discards any pending work
	void	shutdown();

	//
	// Associate a name with the threadpool
	// Routine takes a copy of the name string
	//
	void	set_name(const char *);

	//
	// Change the nature of the threadpool
	// true => dynamic thread creation enabled
	// false => use static thread creation
	//
	void	set_dynamic_threads(bool);

	//
	// Change the number of server threads (increase or decrease)
	// Returns the change in value of thr_desired
	//
	int	change_num_threads(int);

	// Change the maximum cap on server threads (increase or decrease)
	bool	change_max_threads(int);

	//
	// Transfers the specified number of threads from the source
	// threadpool to this threadpool. This method enqueues tasks
	// that cause the thread transfers. Thus this method returns
	// before the transfer completes.
	//
	void	transfer_worker_threads(int num_transfers,
		    threadpool *from_poolp);

	//
	// task_count returns the number of tasks currently pending or
	// blocked.
	//
	uint_t	task_count();

	// Returns the total number of threads in the threadpool
	int	total_number_threads();

	// Remove any pending tasks
	void		remove_pending();

	//
	// Remove any pending defer_tasks with an address tag
	// that matches the specified address.
	//
	void remove_pending_with_address_tag(void *);

protected:
	// Returns the number of idle threads in the threadpool
	int	number_idle_threads();

	// Accessor methods for locking the threadpool
	void		lock();
	void		unlock();
	bool		lock_held();
	os::mutex_t	*get_lock();

private:
	// One worker thread is terminating
	void		worker_exit(threadpool_worker_t *thrp);

	// One worker thread has become idle
	void		worker_idle(threadpool_worker_t *thrp);

	// respawn causes the threads to exit and get restarted
	void		respawn();

	// Internal routine to create N new server threads
	int		add_server_thread(int);

	// Wake up idle worker threads because of thread generation change
	void		generation_change();

	// Wake up a sleeping worker thread
	void		wakeup_worker(threadpool_worker_t *thrp);

protected:
	// List of tasks waiting for a thread to be available
	task_list_t	pending;

	// List of tasks waiting for processing to be unblocked
	task_list_t	blocked_waiting;

private:
	// Name for debug ease
	char		*id;

	//
	// We use int instead of u_int as it avoids unnecessary casts, and
	// an int is large enough.
	//
	int		thr_total;		// Total spawned threads
	int		thr_idle;		// idle thread count
	int		thr_desired;		// desired thread count
	int		thr_max;		// max limit for thr_total
	int		avg_thr_idle;		// avg idle over last
						// MAX_THREADS_AVG reqs
	int		arr_thr_idle[MAX_THREADS_AVG];
						// last MAX_THREADS_AVG
						// idle values
	int		arr_index;		// index for arr_thr_idle
	int		sum_arr;		// sum of last MAX_THREADS_AVG
						// idle values
	int		thr_in_creation;	// In-progress thread creates

	// Identifies the generation number for worker threads
	int		generation;

	size_t		thr_stacksize;

	//
	// Scheduling properties of threads in this threadpool
	// the priority and scheduling class are settable.
	//
	int		thread_pri;
	char		*thread_clname;

	// True when this threadpool changes size automatically
	bool		dynamic_threadpool;


	// The number of threadpool "blocks" in effect.
	ushort_t	blocked_processing;

	//
	// Count of tasks waiting for worker thread processing.
	// Should be equal to pending.count() + blocked_waiting.count()
	//
	uint_t		count;

	os::condvar_t	threadidle;
	os::mutex_t	threadlock;

	//
	// Idle worker threads wait here.
	// Worker threads are interchangeable.
	// The queue uses LIFO ordering.
	//
	IntrList<threadpool_worker_t, _SList>	idle_list;

	// Disallow assignments and pass by value
	threadpool(const threadpool &);
	threadpool &operator = (threadpool &);
};

#ifndef NOINLINES
#include <sys/threadpool_in.h>
#endif  // _NOINLINES

#endif	/* _THREADPOOL_H */
