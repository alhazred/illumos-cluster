/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CL_COMM_SUPPORT_H
#define	_CL_COMM_SUPPORT_H

#pragma ident	"@(#)cl_comm_support.h	1.7	08/10/13 SMI"

#include <h/naming.h>
#include <h/replica.h>
#ifdef _KERNEL_ORB
#include <orb/transport/nil_endpoint.h>
#endif
#include <netinet/in.h>
#include <sys/clconf_int.h>
#include <cmm/cmm_impl.h>
#include <sys/sol_version.h>

//
// Originally, cl_comm contained most of the cluster kernel
// components in one very large kernel module. As a result
// of it's size, DTrace was unable to trace functions contained
// within cl_comm because the text size was greater than 2MB.
// In order to split up cl_comm into smaller more modular
// kernel modules it became necessary to utilize function
// pointers to resolve any undefined symbols that arose and
// to remove the circular dependencies among the new modules.
// Those function pointer are initialized in ORB::initialize()
// and the _init() function for cl_comm.
//
// The sections below all indicate the particular kernel
// module that first tries to access the particular function.
//

#ifdef _KERNEL_ORB
//
// Function accessed by cl_load
//
int (*clconfig_clmode_funcp)(int, void *);

int (*cl_orb_initialize_funcp)(void);
int (*cl_orb_is_initialized_funcp)(void);
#endif // _KERNEL_ORB

//
// Function accessed by cl_orb
//
naming::naming_context_ptr (*root_nameserver_funcp)(void);
naming::naming_context_ptr (*root_nameserver_root_funcp)(void);
naming::naming_context_ptr (*local_nameserver_funcp)(nodeid_t);
naming::naming_context_ptr (*local_nameserver_root_funcp)(nodeid_t);
replica::rm_admin_ptr (*get_rm_funcp)(void);
idlversion_ptr (*get_idlversion_impl_funcp)(nodeid_t);

#ifdef _KERNEL_ORB
void (*clconf_obj_release_funcp)(clconf_obj_t *);
clconf_obj_t *(*clconf_obj_get_child_funcp)(clconf_obj_t *,
    clconf_objtype_t, int);
clconf_cluster_t *(*clconf_cluster_get_current_funcp)(void);
int (*cluster_get_quorum_status_funcp)(quorum_status_t **);
sendstream *(*endpoint_reserve_resources_funcp)(resources *);
nil_recstream *(*make_recstream_funcp)(nil_sendstream *);
void (*vm_comm_handle_messages_funcp)(recstream *);
os::threadid_t (*get_transition_thread_id_funcp)(void);

#if defined(_FAULT_INJECTION)
nodeid_t (*rmm_primary_node_funcp)(void);
uint_t (*num_endpoints_funcp)(nodeid_t);
int (*fault_revoke_pathend_funcp)(int, nodeid_t);
void (*fi_do_reboot_funcp)(bool);
void (*fi_cmm_reboot_funcp)(uint_t, void *, uint32_t);
#endif // _FAULT_INJECTION

#if SOL_VERSION >= __s10 && !defined(UNODE)
int32_t (*zc_getidbyname_funcp)(const char *, uint32_t *);
#endif // (SOL_VERSION >= __s10) && !defined(UNODE)

#endif // _KERNEL_ORB

//
// Functions accessed by cl_haci
//
#ifdef _KERNEL
in_addr_t (*ipconf_get_physical_ip_funcp)(in_addr_t);
bool (*ipconf_is_initialized_funcp)(void);
#endif // _KERNEL
#ifdef _KERNEL_ORB
void (*kernel_tm_callback_funcp)(void);
bool (*pm_add_path_funcp)(clconf_path_t *);
bool (*pm_remove_path_funcp)(clconf_path_t *);
bool (*pm_enable_funcp)(void);
bool (*pm_disable_funcp)(void);
uint32_t (*pm_get_max_delay_funcp)(nodeid_t);
void (*pm_register_cmm_funcp)(cmm_impl *);
void (*pm_unregister_cmm_funcp)(cmm_impl *);
void (*cl_wait_until_orb_is_up_funcp)(void);
#endif // _KERNEL_ORB

#endif	/* _CL_COMM_SUPPORT_H */
