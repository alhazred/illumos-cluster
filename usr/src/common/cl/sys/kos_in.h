/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  kos_in.h
 *
 */

#ifndef _KOS_IN_H
#define	_KOS_IN_H

#pragma ident	"@(#)kos_in.h	1.30	08/05/20 SMI"

#include <sys/ksynch.h>

//
// These are the _KERNEL definitions of os.h inline functions.
//

inline os::threadid_t
os::threadid(void)
{
	return ((os::threadid_t) curthread);
}

inline os::threadid_t
os::thread::create(void *(*func)(void *))
{
	return (thread_create(NULL, (size_t)0, (void (*)())func,
	    NULL, (size_t)0, &p0, TS_RUN, 60));
}

inline void *
/*CSTYLED*/
operator new(galsize_t len, os::mem_zero_type flag)
{
	return (shared_new((size_t)len, os::SLEEP, flag));
}

inline void *
/*CSTYLED*/
operator new[](galsize_t len, os::mem_zero_type flag)
{
	return (shared_new((size_t)len, os::SLEEP, flag));
}

inline void *
/*CSTYLED*/
operator new(galsize_t len, os::mem_alloc_type flag)
{
	return (shared_new((size_t)len, flag, os::DONTZERO));
}

inline void *
/*CSTYLED*/
operator new[](galsize_t len, os::mem_alloc_type flag)
{
	return (shared_new((size_t)len, flag, os::DONTZERO));
}

inline void *
/*CSTYLED*/
operator new(galsize_t len, os::mem_alloc_type flag, os::mem_zero_type zflag)
{
	return (shared_new((size_t)len, flag, zflag));
}

inline void *
/*CSTYLED*/
operator new[](galsize_t len, os::mem_alloc_type flag, os::mem_zero_type zflag)
{
	return (shared_new((size_t)len, flag, zflag));
}

inline uint32_t
os::cas32(uint32_t *target, uint32_t cmp, uint32_t nv)
{
	return (::cas32(target, cmp, nv));
}

inline uint64_t
os::cas64(uint64_t *target, uint64_t cmp, uint64_t nv)
{
	return (::cas64(target, cmp, nv));
}

inline void *
os::casptr(void **target, void *cmp, void *nv)
{
	return (::casptr(target, cmp, nv));
}

//
// os::mutex_t
//
inline
os::mutex_t::mutex_t()
{
	::mutex_init(&_mutex, NULL, MUTEX_DEFAULT, NULL);
}

inline
os::mutex_t::~mutex_t()
{
	::mutex_destroy(&_mutex);
}

inline void
os::mutex_t::lock()
{
// It is OK to do blocking mutex_enter in interrupt context
// This debug check is only to verify (if needed) that we dont do it in
// unexpected places
#ifdef INTR_DEBUG2
	if (curthread->t_flag & T_INTR_THREAD) {
		if (::mutex_tryenter(&_mutex)) {
			return;
		}
		os::printf("mutex_tryenter in interrupt context failed"
		    " - doing mutex_enter\n");
		os::tracedump();
	}
#endif
	::mutex_enter(&_mutex);
}

inline int
os::mutex_t::try_lock()
{
	return (::mutex_tryenter(&_mutex));
}

inline void
os::mutex_t::unlock()
{
	::mutex_exit(&_mutex);
}

inline int
os::mutex_t::lock_held()
{
	return (::mutex_owned(&_mutex));
}

inline int
os::mutex_t::lock_not_held()
{
	return (panicstr || !::mutex_owned(&_mutex));
}

//
// os::rw_lock_t
//
inline
os::rwlock_t::rwlock_t()
{
	::rw_init(&_rwlock, NULL, RW_DEFAULT, NULL);
}

inline
os::rwlock_t::~rwlock_t()
{
	::rw_destroy(&_rwlock);
}

inline void
os::rwlock_t::rdlock()
{
// It is OK to do blocking rwlocks in interrupt context
// This debug check is only to verify (if needed) that we dont do it in
// unexpected places
#ifdef INTR_DEBUG2
	if (curthread->t_flag & T_INTR_THREAD) {
		if (::rw_tryenter(&_rwlock, RW_READER)) {
			return;
		}
		os::printf("rdlock in interrupt context failed"
		    " - doing rw_enter\n");
		os::tracedump();
	}
#endif
	::rw_enter(&_rwlock, RW_READER);
}

inline void
os::rwlock_t::wrlock()
{
// It is OK to do blocking rwlocks in interrupt context
// This debug check is only to verify (if needed) that we dont do it in
// unexpected places
#ifdef INTR_DEBUG2
	if (curthread->t_flag & T_INTR_THREAD) {
		if (::rw_tryenter(&_rwlock, RW_WRITER)) {
			return;
		}
		os::printf("wrlock in interrupt context failed"
		    " - doing rw_enter\n");
		os::tracedump();
	}
#endif
	::rw_enter(&_rwlock, RW_WRITER);
}

inline int
os::rwlock_t::try_rdlock()
{
	return (::rw_tryenter(&_rwlock, RW_READER));
}

inline int
os::rwlock_t::try_wrlock()
{
	return (::rw_tryenter(&_rwlock, RW_WRITER));
}

inline void
os::rwlock_t::unlock()
{
	::rw_exit(&_rwlock);
}

inline int
os::rwlock_t::try_upgrade()
{
	return (::rw_tryupgrade(&_rwlock));
}

inline void
os::rwlock_t::downgrade()
{
	::rw_downgrade(&_rwlock);
}

inline int
os::rwlock_t::lock_held()
{
	return (RW_LOCK_HELD(&_rwlock));
}

inline int
os::rwlock_t::read_held()
{
	return (RW_READ_HELD(&_rwlock));
}

inline int
os::rwlock_t::write_held()
{
	return (RW_WRITE_HELD(&_rwlock));
}

inline os::threadid_t
os::rwlock_t::rw_owner()
{
	return (::rw_owner(&_rwlock));
}

//
// os::sem_t
//
inline
os::sem_t::sem_t(uint_t initval)
{
	::sema_init(&_sem, initval, NULL, SEMA_DEFAULT, NULL);
}

inline
os::sem_t::sem_t()
{
	::sema_init(&_sem, 0, NULL, SEMA_DEFAULT, NULL);
}

inline
os::sem_t::~sem_t()
{
	::sema_destroy(&_sem);
}

inline void
os::sem_t::p()
{
	os::envchk::check_nonblocking(os::envchk::NB_INTR | os::envchk::NB_INVO,
	    "sema_p");
	::sema_p(&_sem);
}

inline int
os::sem_t::tryp()
{
	return (::sema_tryp(&_sem));
}

inline void
os::sem_t::v()
{
	::sema_v(&_sem);
}

//
// os::tsd
//
inline
os::tsd::tsd(freef_t freef)
{
	::tsd_create(&_key, freef);
}

inline
os::tsd::~tsd()
{
	tsd_destroy(&_key);
}

inline uintptr_t
os::tsd::get_tsd() const
{
	return ((uintptr_t)::tsd_get(_key));
}

inline void
os::tsd::set_tsd(uintptr_t val)
{
	(void) ::tsd_set(_key, (void *)val);
}


//
// os::condvar_t
//
inline
os::condvar_t::condvar_t()
{
	::cv_init(&_cv, NULL, CV_DEFAULT, NULL);
#ifdef DEBUG
	num_waiting = 0;
	wait_lock = NULL;
#endif
}

inline
os::condvar_t::~condvar_t()
{
	ASSERT(num_waiting == 0);
	::cv_destroy(&_cv);
}

inline void
os::condvar_t::wait(os::mutex_t *lockp)
{
	os::envchk::check_nonblocking(os::envchk::NB_INVO |
	    os::envchk::NB_INTR, "cv_wait");
	debug_pre(lockp);
	::cv_wait(&_cv, &lockp->_mutex);
	debug_post();
}

inline os::condvar_t::wait_result
os::condvar_t::wait_sig(os::mutex_t *lockp)
{
	int retval;
	wait_result result;

	os::envchk::check_nonblocking(os::envchk::NB_INTR | os::envchk::NB_INVO,
	    "cv_wait_sig");
	debug_pre(lockp);
	retval = ::cv_wait_sig(&_cv, &lockp->_mutex);
	ASSERT(retval >= -1);
	if (retval == 0) {
		result = SIGNALED;
	} else if (retval == -1) {
		result = TIMEDOUT;
	} else if (retval > 0) {
		result = NORMAL;
	}
	debug_post();
	return (result);
}

inline os::condvar_t::wait_result
os::condvar_t::timedwait(os::mutex_t *lockp, os::systime *timeout)
{
	clock_t retval;
	wait_result result;

	os::envchk::check_nonblocking(os::envchk::NB_INVO |
	    os::envchk::NB_INTR, "cv_timedwait");
	debug_pre(lockp);
	retval = ::cv_timedwait(&_cv, &lockp->_mutex, timeout->_time);
	ASSERT(retval >= -1);
	if (retval == 0) {
		result = SIGNALED;
	} else if (retval == -1) {
		result = TIMEDOUT;
	} else if (retval > 0) {
		result = NORMAL;
	}
	debug_post();
	return (result);
}

inline os::condvar_t::wait_result
os::condvar_t::timedwait_sig(os::mutex_t *lockp, os::systime *timeout)
{
	clock_t retval;
	wait_result result;

	os::envchk::check_nonblocking(os::envchk::NB_INTR | os::envchk::NB_INVO,
	    "cv_timedwait_sig");
	debug_pre(lockp);
	retval = ::cv_timedwait_sig(&_cv, &lockp->_mutex, timeout->_time);
	ASSERT(retval >= -1);
	if (retval == 0) {
		result = SIGNALED;
	} else if (retval == -1) {
		result = TIMEDOUT;
	} else if (retval > 0) {
		result = NORMAL;
	}
	debug_post();
	return (result);
}

inline void
os::condvar_t::signal()
{
	ASSERT(wait_lock == NULL || wait_lock->lock_held());
	::cv_signal(&_cv);
}

inline void
os::condvar_t::broadcast()
{
	ASSERT(wait_lock == NULL || wait_lock->lock_held());
	::cv_broadcast(&_cv);
}

inline
os::rfile::rfile() :
	f(NULL)
{
}

inline
os::rfile::~rfile()
{
	if (f)
		close();
}

inline bool
os::rfile::open(char *pname)
{
	return ((f = ::kobj_open_file(pname)) != NULL);
}

inline bool
os::rfile::isopen()
{
	return (f != NULL);
}

inline size_t
os::rfile::read(char *b, size_t size, size_t nitems)
{
	ASSERT(f);
	return ((size_t)::kobj_read_file(f, b, (unsigned)size,
	    (unsigned)nitems));
}

inline int
os::rfile::fgetc()
{
	ASSERT(f);
	return (kobj_getc(f));
}

// Note that this assumes we're putting back the same character
inline int
os::rfile::ungetc(int)
{
	ASSERT(f);
	return (kobj_ungetc(f));
}

inline void
os::rfile::close()
{
	if (f) {
		::kobj_close_file(f);
		f = NULL;
	}
}

inline
os::rdir::rdir() :
	dvp(NULL)
{
}

inline
os::rdir::~rdir()
{
	close();
}

inline bool
os::rdir::isopen()
{
	return (dvp != NULL);
}

inline bool
os::systime::is_before(os::systime &other)
{
	return (_time < other._time);
}

inline void
os::systime::set(os::systime &other)
{
	_time = other._time;
}

inline int
os::highbit(ulong_t w)
{
	return (::highbit(w));
}

inline int
os::lowbit(ulong_t w)
{
	return (::lowbit(w));
}


//
// Methods for character type derivation
//
inline int
os::ctype::is_digit(int c)
{
	return ((c >= '0') && (c <= '9'));
}

inline int
os::ctype::is_alpha(int c)
{
	return (
	    (c >= 'a' && c <= 'z') ||
	    (c >= 'A' && c <= 'Z'));
}

inline int
os::ctype::is_xdigit(int c)
{
	return (
	    (c >= '0' && c <= '9') ||
	    (c >= 'a' && c <= 'f') ||
	    (c >= 'A' && c <= 'F'));
}

inline int
os::ctype::is_lower(int c)
{
	return ((c >= 'a') && (c <= 'z'));
}

inline int
os::ctype::is_upper(int c)
{
	return ((c >= 'A') && (c <= 'Z'));
}

inline int
os::ctype::is_space(int c)
{
	return (
	    c == ' ' || c == '\t' || c == '\n' ||
	    c == '\r' || c == '\v' || c == '\f');
}

#endif	/* _KOS_IN_H */
