/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */


/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _VC_INT_H
#define	_VC_INT_H

#pragma ident	"@(#)vc_int.h	1.5	08/07/17 SMI"

#include <sys/sol_version.h>

/*
 * This file contains definitions related to zone clusters.
 */
#if (SOL_VERSION >= __s10)

/*
 * Define the minimum of the cluster id allocated to a zone cluster.
 * cluster id BASE_CLUSTER_ID == 0 is reserved for the global zone.
 * cluster id 1 is reserved for the use of the name server.
 * cluster id BASE_NONGLOBAL_ID == 2 is reserved for native non-global zones.
 */
#define	BASE_CLUSTER_ID		0
#define	BASE_NONGLOBAL_ID	2
#define	MIN_CLUSTER_ID		3

#endif // (SOL_VERSION >= __s10)

#endif /* _VC_INT_H */
