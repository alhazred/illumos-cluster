/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _CLADM_DEBUG_H
#define	_CLADM_DEBUG_H

#pragma ident	"@(#)cladm_debug.h	1.6	08/05/20 SMI"

#ifdef	_KERNEL
#include <sys/dbg_printf.h>
#endif

#ifdef	__cplusplus
extern "C" {
#endif

#ifdef	_KERNEL

#define	CLADM_DBGBUF

#ifdef	CLADM_DBGBUF
extern dbg_print_buf cladm_dbg;
#define	CLADM_DBG(a)	cladm_dbg.dbprintf a
#else
#define	CLADM_DBG(a)
#endif

const int	CLADM_TRACE_CLEXEC		= 0x00000001;
extern uint_t	cladm_trace_options;
extern int	cladm_trace_level;

const int	CLADM_GREEN = 3;
const int	CLADM_AMBER = 2;
const int	CLADM_RED = 1;

//
// CLADM_DBPRINTF - record cladm trace information.
// Inputs
//	option - enables trace if set in cladm_trace_options
//	level - Debug level, currently red, amber and green
//	args - The arguments to the print statements must be enclosed
//		with parenthesis, as in the following example:
//			("cladm action %d\n", action_number)
//
// Note that the trailing "else" statement in the macro removes
// ambiguity when the macro is nested in another "if" statement.
//
#ifdef CLADM_DBGBUF
#define	CLADM_DBPRINTF(option, level, args) \
if (((option & cladm_trace_options) &&\
	(level <= cladm_trace_level)) ||\
	(level == CLADM_RED)) {\
		CLADM_DBG(args); \
	} else
#else
#define	CLADM_DBPRINTF(option, level, args)
#endif	/* CLADM_DBGBUF */

#ifdef CLADM_DBGBUF
#define	CLEXEC_EXCEPTION(env, title, command) \
{ \
	CORBA::SystemException *sysex = \
		CORBA::SystemException::_exnarrow(env.exception()); \
	if (sysex != NULL) { \
		CLADM_DBPRINTF(CLADM_TRACE_CLEXEC, CLADM_RED, \
		    ("%s %s exception major %d minor %d completed %d\n", \
		    title, command, sysex->_major(), \
		    sysex->_minor(), sysex->completed())); \
	} \
}
#else
#define	CLEXEC_EXCEPTION(env, title, command)
#endif	/* CLADM_DBGBUF */


#else	/* !_KERNEL */


#define	CLADM_GREEN	3
#define	CLADM_AMBER	2
#define	CLADM_RED	1


#define	CLEXEC_EXCEPTION(env, title, command) \
{ \
	CORBA::SystemException *sysex = \
	    CORBA::SystemException::_exnarrow(env.exception()); \
	cladmin_log_t *etolog = \
	    (cladmin_log_t *)malloc(sizeof (cladmin_log_t)); \
	if (etolog != NULL) { \
		etolog->level = CLADM_RED; \
		if (sysex != NULL) { \
			(void) snprintf(etolog->str, CLADMIN_LOG_SIZE - 1, \
			    "%s %s exception major %d minor %d completed %d\n",\
			    title, command, sysex->_major(), \
			    sysex->_minor(), sysex->completed()); \
		} else { \
			(void) snprintf(etolog->str, CLADMIN_LOG_SIZE - 1, \
			    "%s %s NULL sysex\n",\
			    title, command); \
		} \
		(void) os::cladm(CL_CONFIG, CL_ADMIN_LOG, \
		    (void *)etolog); \
		free(etolog); \
	} \
}


#endif	/* _KERNEL */

#ifdef	__cplusplus
}
#endif

#endif	/* _CLADM_DEBUG_H */
