/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _MC_SEMA_IMPL_H
#define	_MC_SEMA_IMPL_H

#pragma ident	"@(#)mc_sema_impl.h	1.14	08/05/20 SMI"

#include	<sys/os.h>
#include	<orb/object/adapter.h>
#include	<sys/list_def.h>
#include	<h/mc_sema.h>


//
// NOTICE:
//
//	DONT User MC_SEMA unless you've talked to appropriate people.
//	This code was created for use in the fault injection tests.
//	Other uses of this code may lead to dependencies on non-shipping
//	code.  If you'd like to promote this module to shipping code
//	come talk to the right people.
//
//

#define	MAX_MC_SEMA		128

// The mc_sema server
//
// Clearinghouse for all operations on mc_sema objects
//
// See mc_sema.idl for comments
//
class mc_sema_server_impl : public McServerof<mc_sema::server> {
public:
	// Constructor
	mc_sema_server_impl();

	// IDL Members
	mc_sema::cookie	create_sema(Environment &);
	void		destroy(mc_sema::cookie, Environment &);

	void		init(uint32_t value, mc_sema::cookie, Environment &);

	bool		sema_p(mc_sema::sema_ptr, mc_sema::cookie,
				Environment &);

	void		sema_v(mc_sema::cookie, Environment &);

	void		no_op(mc_sema::cookie, Environment &);

	void		_unreferenced(unref_t);
private:
	typedef struct {
		uint32_t			count;
		bool				in_use;
		os::mutex_t			lck;
		SList<mc_sema::sema>		client_l;
	} sema_table_entry_t;

	// The static table
	sema_table_entry_t	sema_table[MAX_MC_SEMA];
};


// The end-user sema object on the client side
//
// See the mc_sema.idl for comments
//
class mc_sema_impl : public McServerof<mc_sema::sema> {
public:
	// Constructor
	// If you have a cookie to a mc_sema object
	// give it to the constructor
	// otherwise leave blank and init will allocate a new
	// object on the server
	mc_sema_impl(mc_sema::cookie id = 0);

	// IDL members
	// Init must be called on a mc_sema object before it can be used
	void	init(uint32_t value, Environment &);

	// destroy must be called in order to de-allocate the storage on the
	// server
	void	destroy(Environment &);

	void	sema_p(Environment &);
	void	sema_v(Environment &);

	void		wakeup(Environment &);
	mc_sema::cookie	get_cookie(Environment &);

	// unreferenced
	void	_unreferenced(unref_t);

	static int register_server();

	static int get_root_to_local();

private:
	int	create();

	//
	// This defines the state of this object.
	// The state is used to ensure that only one thread at a time on a node
	// can be actively requesting (P operation) the semaphore.
	// The state also ensures that a thread waiting a grant
	// from the server semaphore gets it before another thread
	// can grab the semaphore. Remember the client thread blocks
	// on the client node. The state does not have information
	// about whether any thread currently holds the semaphore.
	//
	// Here is the simple state transition diagram
	//
	//	--------------> NO_WAITER
	//	^		|
	//	|		|thread sends sema_p to server
	//	|		V
	//	|<------------- ONE_REQUESTER
	//	^ sema granted	|
	//	|		|sema_p blocked
	//	|		V
	//	|		ONE_WAITER
	//	|		|
	//	|		|server grants semaphore
	//	|		V
	//	|		ONE_PENDING
	//	|		|
	//	|<-------------- thread unblocked and has semaphore
	//
	typedef enum {NO_WAITER, // No thread trying to acquire semaphore
		ONE_REQUESTER,	// One thread has sent a request to server
		ONE_WAITER,	// The server told the client to wait
		ONE_PENDING}	// Server granted semaphore, client still asleep
			sema_state;

	sema_state		_state;

	mc_sema::cookie		_id;
	mc_sema::server_var	_server_ref;

	os::mutex_t		_lck;
	os::condvar_t		_cv;
};

#endif	/* _MC_SEMA_IMPL_H */
