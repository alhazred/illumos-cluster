/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _REFCNT_H
#define	_REFCNT_H

#pragma ident	"@(#)refcnt.h	1.13	08/05/20 SMI"

#include <sys/os.h>

//
// General purpose reference counting object.
// We dont use a lock for the refcnt but instead use the os::atomic*
//
class refcnt {
public:
	// reference counts usually start with one but we can construct
	// with any arbitrary count, if needed
	refcnt(uint_t cnt = 1);

	void hold();		// increment reference count
	void rele();		// decrement reference count and delete if zero

	// Returns true if _refcnt is equal to cnt.
	// This is a hint only as a hold/rele being done in parallel can
	// change the result before it returns
	// Useful mostly in ASSERTs before deleting to verify that the
	// count is a certain known value
	bool verify_count(uint_t cnt);
protected:
	// refcnt_unref can be overloaded if the child wants to do something
	// other than delete itself when the refcnt goes to zero.
	virtual	void refcnt_unref();
	//
	// Users of refcnt objects should use rele() to delete the
	// object. This is protected since objects which inherit from refcnt
	// still need to be able to destroy the object.
	//
	virtual ~refcnt();

private:
	uint_t		_refcnt;	// number of references

	// Disallow assignments and pass by value
	refcnt(const refcnt&);
	refcnt& operator = (refcnt&);
};

#include <sys/refcnt_in.h>

#endif	/* _REFCNT_H */
