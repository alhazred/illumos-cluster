/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _HASHTABLE_H
#define	_HASHTABLE_H

#pragma ident	"@(#)hashtable.h	1.28	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#include <sys/os.h>

//
// This file contains the base classes for a simple but fast hash table for
// storing/looking up uintptr_t sized values. NULL values cannot be stored.
// The value to store in the hash table should be of type castable to uintptr_t
//
// There are three hash table types for different key datatypes.
// hashtable_t -> uses key type castable to uint64_t
// string_hashtable_t -> uses const char * key type
// obj_hashtable_t -> uses CORBA::Object_ptr key type
//
// The code is structured such that the templates
// hashtable_t, string_hashtable_t, obj_hashtable_t, are the public interfaces
// The templates are there just for type checking and avoid casts in the callers
// and they use base_*hash_table classes for common code.
// XX the code needs some cleanup to separate out the base classes into a
// separate header file.
// Always use the templates and not the base classes.
//

// The constructor allows for a caller-specified hash table size.
// hash table size defaults to DEFAULT_HASHTABLE_SIZE (97 currently)

#define	DEFAULT_HASHTABLE_SIZE	97

// The constructor also allows for a caller-specified hash function.
// Defaults hash functions are used but callers are encouraged to provide one
// that could do a better job given context specific information
// The default hash functions are:
// hashtable_t -> default_hashfunc
// string_hashtable_t -> default_string_hashfunc
// obj_hashtable_t -> default_obj_hashfunc
//
// Note: when a new hashtable object is instantiated, caller is supposed
// to call is_constructed method before calling any other methods of the
// hashtable if is_constructed returns false then caller should delete the
// hashtable object immediately.
//

// The hash functions have the key and hash table size as argument and should
// return an index in the range (0, size -1).

// The destructor ASSERTs that the hash table is empty, caller is responsible
// for making the hash table contents empty before destroying it.

// Each hash table type provides functions to add/remove/lookup entries,
// count/empty methods for checking number of entries in the table,
// a dispose method to empty the table,
// and an iterator to go through all entries in the table.

// Locking
// The hash table implementations do not do any locking and expect the callers
// to provide necessary locking.

// add:
// All the hash table types support adding a key, value pair to the hash table.
// key,value are copied (for strings, strdup, and for objects, _duplicate) into
// the hash entry so that hash table has no dependency on the caller.
//
// Adding multiple values for same key: The hash table code does not
// explicitly check for duplicate keys or prevent such additions.
// However, there are no guarantees of which order the iterator will
// find the entries corresponding to the same key or which entry lookup()
// will return or which entry remove() will delete.
//
// Memory allocation impact: add does memory allocation to allocate the
// hash table entry and, if needed, to copy the key. Some callers may wish to
// avoid this allocation while holding critical locks.
// hashtable_t provides a version of add that takes a caller-allocated
// hash_entry_t (which could presumably be allocated without holding the lock
// or embedded in other data structures).
// XX string_hashtable_t and obj_hashtable_t currently do not provide such
// functionality

// remove:
// All hash table types support removing an entry based on the key.
// Returns the value that was stored associated with the key, if found.
// Returns 0/NULL if key is not found. This precludes adding 0/NULL valued
// entries to the hash table.
// See notes above regarding adding multiple entries in the table for the same
// key
// hashtable_t provides a bool argument to remove. The default is (true) to
// delete the hash_entry_t, i.e., assumes hash_entry was allocated with new.
// If it was not allocated with new, set the argument to false to skip the
// delete.
// This is useful if the hash entry was embedded in another data structure,
// similar to how IntrLists work.
// XX string and obj hashtable_t do not provide this functionality

// lookup:
// Returns the value that was stored associated with the key, if found.
// Returns 0/NULL if key is not found. This precludes adding 0/NULL valued
// entries to the hash table.
// See notes above regarding adding multiple entries in the table for the same
// key

// dispose:
// Does a remove of all entries in hash table.
// See notes in remove (above) on semantics of bool argument in hashtable_t

// iterator
// The hash tables provide an iterator template to scan all entries in the
// hash table.
// The iterator initializes when constructed to point at the first element,
// get_current() returns the value (not the key) for the current element
// advance moves the current pointer.
// get_current_key_and_bucket() returns not only the value, as in
// get_current() but also a pointer to the key and to the bucket number.
// This is useful for debugging.
// reinit moves the current pointer back to the first element
//
// Adding elements to the hash table in middle of an iteration can result in
// undefined behavior
// Removing elements in the middle of an iteration is supported. However,
// one should not remove the element pointed to by the iterator. The right
// way to remove a hash entry while using an iterator is to advance() before
// removing the current element. This is similar to how the list iterators work.

// Note that the destructors in these classes are not declared virtual to
// avoid the overhead of creating template .o files and vtables.
// This has the side effect that if a class inherits from one of these, care
// must be taken to always call the destructor of the derived class and not
// the base class.
// If this starts to get confusing or has errors, we can use virtual destructors
//lint -esym(1512, base_hash_table)
//lint -esym(1512, base_string_hash_table)
//lint -esym(1512, base_obj_hash_table)
//lint -esym(1512, hashtable_t*)
//lint -esym(1512, string_hashtable_t*)
//lint -esym(1512, obj_hashtable_t*)

// XX The usual convention for inline functions is to put them _in.h files
// However due to compiler bugs regarding templates, some
// inlines are not working in _in.h and are included here
// Workaround for intel build due to bug 4256054
//	methods of member classes of a template must be defined inline.
//

// EXAMPLES:
// 1.  hashtable_t class
//
//  a.  hashtable_t<sl_servicegrp_t *, network::service_sap_t*> *vanilla_ht;
//	vanilla_ht = new hashtable_t<sl_servicegrp_t *, network::service_sap_t*>
//	((uint_t)clnet_hashtable_size,
//	    vanilla_ht_hash_func,
//	    vanilla_ht_equal_func,
//	    vanilla_ht_free_func,
//	    vanilla_ht_dup_func);
//	Note that in this one the caller supplies all the arguements to the
//	constructor, overriding the defaults.
//	See src/cl_net/mcobj_impl.cc for more details.
//  b.   hashtable_t<gateway_handler *, door_id_t> gwm_table;
//	Note that this one takes all the defaults.
//

// The default hash function does mod (% operator) of hash table size
extern uint_t default_hashfunc(uint64_t key, uint_t hashtablesize);

// The default hash function to use for strings
extern uint_t default_string_hashfunc(const char *key, uint_t hashtablesize);

#include <orb/invo/corba.h>

// The default hash function to use for objects
extern uint_t default_obj_hashfunc(CORBA::Object_ptr key,
	uint_t hashtablesize);

//
// Default EQUALITY functions.
//
// The default equality function to use.
extern bool default_equalfunc(uint64_t, uint64_t);

//
// Default FREE functions.
//
// The default equality function to use.
extern void default_freefunc(uint64_t);

//
// Default DUPLICATE functions.  Duplicate the key.
//
// The default duplicate function to use.
extern uint64_t default_dupfunc(uint64_t);

class base_hash_table {
public:

	class iterator;
	friend class iterator;

	// destructor
	~base_hash_table();

	// Does a remove of all entries in the hash table
	// bool argument specifies whether dispose should delete the
	// hash entries. Pass in false if the entries added to the hash table
	// were not allocated through new. Caller is responsible for freeing
	// the memory in that case.
	void dispose(bool = true);

	// Returns whether hash table is empty or not
	bool empty();

	// Returns number of entries currently in hash table
	uint_t count() const;

	//
	// Returns the state of the dynamically allocated data-members
	// of hash table. Returns true if the allocation was successful
	// in the constructor. All users of hash table *should* call this
	// function to maintain data integrity. if this function returns
	// false then the hash table object must be deleted immediately,
	// else adding a element to this hashtable will result in unexpected
	// behaviour.
	//
	bool is_constructed() const;

	uint_t get_number_of_buckets() const;

	uint_t get_number_chained(uint_t bucket_index) const;

	// Return the size of the hash table (including elements) in bytes.
	uint_t get_size() const;

	// We make the structure of hash_entry be public because
	// there are cases where we need to allocate a hash entry
	// before calling add (so we avoid memory allocations while
	// holding a lock)
	struct hash_entry {
		uintptr_t he_entry;
		hash_entry *he_next;
		uint64_t he_key;
	};
	typedef struct hash_entry *hash_entry_ptr;

	typedef bool (*base_equal_key_function)(uint64_t, uint64_t);
	typedef void (*base_free_key_function)(uint64_t);
	typedef uint64_t (*base_dup_key_function)(uint64_t);

	// The iterator class. Use get_current to get current element.
	// Use advance to advance to the next element.

	// Use reinit to either reset the iterator to first element or to
	// use the interator for a different hash table

	// Note for doing removes while using the iterator
	// The iterator keeps a pointer to the current element. Caller should
	// do an advance() before deleting the current element returned by
	// get_current().
	class iterator {
	public:
		void reinit(base_hash_table *h);

		void advance();
	protected:
		// Internal methods called from the template
		iterator(base_hash_table *h);

		uintptr_t get_current() const;

		uintptr_t get_current_key_and_bucket(void **key,
		    uint_t *bucket_num) const;
	private:
		uint_t _index;
		hash_entry_ptr _current;
		base_hash_table *_table;

		// Disallow assignments and pass by value
		iterator(const iterator &);
		iterator & operator = (iterator &);
	};
protected:
	// Specify size of hash table
	// Specify a hash function that takes a uint64_t key as input and
	// returns an uint_t index. The implementation finds the bucket
	// for this key by doing table[index % size]
	base_hash_table(uint_t size, uint_t (*func)(uint64_t, uint_t),
	    base_equal_key_function eqfunc,  base_free_key_function frfunc,
	    base_dup_key_function dupfunc);

	// Internal methods to add/lookup/remove called from the template
	//
	// allocated is a default parameter used to indicate whether
	// caller is interested in knowing the status of dynamic memory
	// allocation.
	// allocated = NULL		-- indicates caller is not interested.
	// allocated = any_other_value	-- indicates caller is interested.
	// *allocated is set to true if memory allocation succeeds
	// else *allocated is set to false.
	//
	void _add(uintptr_t entry, uint64_t key, bool *allocated = NULL);

	// version of add that does not do memory allocation;
	// ptr points to an unused hash_entry.
	void _add(uintptr_t entry, uint64_t key, hash_entry_ptr ptr);
	uintptr_t _lookup(uint64_t key);
	uintptr_t _remove(uint64_t key, bool destroy);

private:
	hash_entry_ptr *hash(uint64_t key);

	hash_entry_ptr *hash_buckets;
	uint_t	hash_size;
	uint_t	(*hashfunc)(uint64_t, uint_t);
	uint_t	refcount;

	//
	// status indicates the state of dynamically allocated data-members.
	// status is true only if all the data-members are constructed
	// successfully in the constructor.
	//
	bool status;

	// Equality function compares two keys.  If identical, returns
	// true, else returns false. The default is to compare 64-bit
	// keys.
	base_equal_key_function	equalfunc;

	// Free storage function frees a key.  The default is to do
	// nothing since no storage was allocated for a 64-bit key.
	base_free_key_function freefunc;

	// Duplicate storage function allocates storage for a key.
	// The default is to do nothing since the caller doesn't need
	// storage allocated but will use the default 64-bit key.
	base_dup_key_function dupfunc;

	// Disallow assignments and pass by value
	base_hash_table(const base_hash_table &);
	base_hash_table & operator = (base_hash_table &);
};


class base_string_hash_table {
public:
	class iterator;
	friend class iterator;

	// destructor
	~base_string_hash_table();

	// Does a remove of all entries in the hash table
	void dispose();

	// Returns whether hash table is empty or not
	bool empty();

	// Returns number of entries currently in hash table
	uint_t count() const;

	//
	// Returns the state of the dynamically allocated data-members
	// of hash table. Returns true if the allocation was successful
	// in the constructor. All users of hash table *should* call this
	// function to maintain data integrity. if this function returns
	// false then the hash table object must be deleted immediately,
	// else adding a element to this hashtable will result in unexpected
	// behaviour.
	//
	bool is_constructed() const;

	struct hash_entry {
		uintptr_t he_entry;
		hash_entry *he_next;
		char *he_key;
	};
	typedef struct hash_entry *hash_entry_ptr;

	// The iterator class. Use get_current to get current element.
	// Use advance to advance to the next element.

	// Use reinit to either reset the iterator to first element or to
	// use the interator for a different hash table

	// Note for doing removes while using the iterator
	// The iterator keeps a pointer to the current element. Caller should
	// do an advance() before deleting the current element returned by
	// get_current().
	class iterator {
	public:
		void reinit(base_string_hash_table *h);

		void advance();
	protected:
		// Internal methods called from the template
		iterator(base_string_hash_table *h);

		uintptr_t get_current() const;

		uintptr_t get_current_key_and_bucket(char *&key,
		    uint_t &bucket_num) const;
	private:
		uint_t _index;
		hash_entry_ptr _current;
		base_string_hash_table *_table;

		// Disallow assignments and pass by value
		iterator(const iterator &);
		iterator & operator = (iterator &);
	};

protected:
	// Specify size of hash table
	// Specify a hash function that takes a const char * key as input and
	// returns an uint_t index. The implementation finds the bucket
	// for this key by doing table[index % size]
	base_string_hash_table(uint_t size,
	    uint_t (*func)(const char *, uint_t));

	// Internal methods to add/lookup/remove called from the template
	//
	// allocated is a default parameter used to indicate whether
	// caller is interested in knowing the status of dynamic memory.
	// allocation.
	// allocated = NULL		-- indicates caller is not interested.
	// allocated = any_other_value	-- indicates caller is interested
	// *allocated is set to true if memory allocation succeeds else
	// *allocated is set to false.
	//
	void _add(uintptr_t entry, const char *key, bool *allocated = NULL);
	uintptr_t _lookup(const char *key);
	uintptr_t _remove(const char *key);
private:
	hash_entry_ptr *hash(const char *key);

	hash_entry_ptr *hash_buckets;
	uint_t	hash_size;
	uint_t	(*hashfunc)(const char *, uint_t);
	uint_t	refcount;

	//
	// status indicates the state of dynamically allocated data-members
	// status is true only if all the data-members are constructed
	// successfully in the constructor.
	//
	bool status;

	// Disallow assignments and pass by value
	base_string_hash_table(const base_string_hash_table &);
	base_string_hash_table & operator = (base_string_hash_table &);

};

class base_obj_hash_table {
public:
	class iterator;
	friend class iterator;

	// destructor
	~base_obj_hash_table();

	// Does a remove of all entries in the hash table
	void dispose();

	// Returns whether hash table is empty or not
	bool empty();

	// Returns number of entries currently in hash table
	uint_t count() const;

	//
	// Returns the state of the dynamically allocated data-members
	// of hash table. Returns true if the allocation was successful
	// in the constructor. All users of hash table *should* call this
	// function to maintain data integrity. if this function returns
	// false then the hash table object must be deleted immediately
	// else adding a element to this hashtable will result in unexpected
	// behaviour.
	//
	bool is_constructed() const;

	struct hash_entry {
		uintptr_t he_entry;
		hash_entry *he_next;
		CORBA::Object_ptr he_key;
	};
	typedef struct hash_entry *hash_entry_ptr;

	// The iterator class. Use get_current to get current element.
	// Use advance to advance to the next element.

	// Use reinit to either reset the iterator to first element or to
	// use the interator for a different hash table

	// Note for doing removes while using the iterator
	// The iterator keeps a pointer to the current element. Caller should
	// do an advance() before deleting the current element returned by
	// get_current().
	class iterator {
	public:
		void reinit(base_obj_hash_table *h);

		void advance();
	protected:
		// Internal methods called from the template

		iterator(base_obj_hash_table *h);

		uintptr_t get_current() const;
	private:
		uint_t _index;
		hash_entry_ptr _current;
		base_obj_hash_table *_table;

		// Disallow assignments and pass by value
		iterator(const iterator &);
		iterator & operator = (iterator &);
	};

protected:
	// Specify size of hash table
	// Specify a hash function that takes a Object_ptr key as input and
	// returns an uint_t index. The implementation finds the bucket
	// for this key by doing table[index % size]
	base_obj_hash_table(uint_t size,
	    uint_t (*func)(CORBA::Object_ptr, uint_t));

	// Internal methods to add/lookup/remove called from the template
	//
	// allocated is a default parameter used to indicate whether
	// caller is interested in knowing the status of dynamic memory
	// allocation.
	// allocated = NULL		-- indicates caller is not interested.
	// allocated = any_other_value	-- indicates caller is interested.
	// *allocated is set to true, if memory allocation succeeds
	// else *allocated is set to false.
	//
	void _add(uintptr_t entry, CORBA::Object_ptr key,
	    bool *allocated = NULL);
	uintptr_t _lookup(CORBA::Object_ptr key);
	uintptr_t _remove(CORBA::Object_ptr key);

private:
	hash_entry_ptr *hash(CORBA::Object_ptr key);

	hash_entry_ptr *hash_buckets;
	uint_t	hash_size;
	uint_t	(*hashfunc)(CORBA::Object_ptr, uint_t);
	uint_t	refcount;
	//
	// status indicates the state of dynamically allocated data-members
	// status is true only if all the data-members are constructed
	// successfully in the constructor.
	//
	bool status;

	// Disallow assignments and pass by value
	base_obj_hash_table(const base_obj_hash_table &);
	base_obj_hash_table & operator = (base_obj_hash_table &);

};


// Template hashtable_t allows use of base_hash_table with better
// type checking and avoiding some casts in the code
// Class K should be castable to type uint64_t.
// Class T should be castable to type uintptr_t (usually a pointer)
// The template is clean and does only casts and should not be generating
// any .o template files.
// The constructor takes a configurable table size and hash function

template<class T, class K> class hashtable_t : public base_hash_table {
public:

	// Specify size of hash table
	// Specify a hash function that takes a uint64_t key as input and
	// returns an uint_t index. The implementation finds the bucket
	// for this key by doing table[index % size].
	// Specify an equality function that compares the keys of type K;
	// the meaning of equality depends on the type K.
	// Specify a free storage function that frees the key K if
	// it was allocated by the caller of "add" method; the default
	// is a no-op since it assumes that the caller is using the
	// standard 64-bit key.
	hashtable_t(
	    uint_t size = DEFAULT_HASHTABLE_SIZE,
	    uint_t (*func)(uint64_t, uint_t) = default_hashfunc,
	    base_equal_key_function equal = default_equalfunc,
	    base_free_key_function frfunc = default_freefunc,
	    base_dup_key_function dupfunc = default_dupfunc);

	// Add an entry to the hash table, allocates memory
	// Does not check if another entry with same key exists
	// Takes a copy of key
	//
	// allocated is a default parameter used to indicate whether
	// caller is interested in knowing the status of dynamic memory
	// allocation.
	// allocated = NULL		-- indicates caller is not interested.
	// allocated = any_other_value	-- indicates caller is interested.
	// *allocated is set to true if memory allocation succeeds else
	// *allocated is set to false.
	//
	void add(T value, K key, bool *allocated = NULL);

	// version of add that does not do memory allocation
	// Caller should allocate a base_hash_table::hash_entry or
	// provide a pointer to one already allocated.
	// This sometimes avoids doing memory allocations while holding locks.
	// The templates for the other hash table types do not provide this
	// functionality yet. We need to invent a way to preinitialize the
	// hash_entry instead of just allocating the entry as this does
	void add(T value, K key, hash_entry_ptr ptr);

	// Looks up in the hash table for key and returns the value
	// that was stored along with the key.
	// Returns 0 if key not found.
	T lookup(K key);

	// Remove the entry in the hash table that corresponds to the key
	// If found, returns the value that was stored along with the key
	// If not found, returns 0.
	// If the bool argument is set to true, the hash entry is deleted
	// if set to false, the caller is responsible for freeing the hash
	// entry
	T remove(K key, bool destroy = true);

	// The iterator class. Use get_current to get current element.
	// Use advance to advance to the next element.

	// Use reinit to either reset the iterator to first element or to
	// use the interator for a different hash table

	// Note for doing removes while using the iterator
	// The iterator keeps a pointer to the current element. Caller should
	// do an advance() before deleting the current element returned by
	// get_current().
	class iterator : public base_hash_table::iterator {
	public :
		iterator(hashtable_t<T, K> &h) :
		    base_hash_table::iterator(&h) { }

		iterator(hashtable_t<T, K> *h) :
		    base_hash_table::iterator(h) { }

		T get_current() const
		{
		    return ((T)base_hash_table::iterator::get_current());
		}

		//
		// Returns the current value, as well as the key and the
		// bucket number in the table. The caller must provide
		// the storage for out parameters "key" and "bucket_num."
		// This method is useful for debugging since it returns
		// some internal information.  Probably should not be
		// in conjunction with the standard "get_current"
		// defined above.
		//
		T get_current_key_and_bucket(K *key, uint_t *bucket_num) const
		{
		    return ((T)base_hash_table::iterator::
			get_current_key_and_bucket((void **)key,
			    (uint_t*)bucket_num));
		}
	};

private:
};


// Template string_hashtable_t allows use of base_string_hash_table with better
// type checking and avoiding some casts in the code
// Class T should be castable to type uintptr_t (usually a pointer)
// The template is clean and does only casts and should not be generating
// any .o template files.
// The constructor takes a configurable table size and hash function

template<class T> class string_hashtable_t : public base_string_hash_table {
public:
	// Specify size of hash table
	// Specify a hash function that takes a const char * key as input and
	// returns an uint_t index. The implementation finds the bucket
	// for this key by doing table[index % size]
	string_hashtable_t::string_hashtable_t(
	    uint_t size = DEFAULT_HASHTABLE_SIZE,
	    uint_t (*func)(const char *, uint_t) = default_string_hashfunc);

	// Add an entry to the hash table, allocates memory
	// Does not check if another entry with same key exists
	// Takes a copy of key
	//
	// allocated is a default parameter used to indicate whether
	// caller is interested in knowing the status of dynamic memory
	// allocation.
	// allocated = NULL		-- indicates caller is not interested.
	// allocated = any_other_value	-- indicates caller is interested.
	// *allocated is set to true if memory allocation succeeds else
	// *allocated is set to false.
	//
	void add(T value, const char *key, bool *allocated = NULL);

	// Looks up in the hash table for key and returns the value
	// that was stored along with the key.
	// Returns 0 if key not found.
	T lookup(const char *key);

	// Remove the entry in the hash table that corresponds to the key
	// If found, returns the value that was stored along with the key
	// If not found, returns 0.
	T remove(const char *key);

	// The iterator class. Use get_current to get current element.
	// Use advance to advance to the next element.

	// Use reinit to either reset the iterator to first element or to
	// use the interator for a different hash table

	// Note for doing removes while using the iterator
	// The iterator keeps a pointer to the current element. Caller should
	// do an advance() before deleting the current element returned by
	// get_current().
	class iterator : public base_string_hash_table::iterator {
	public :
		iterator(string_hashtable_t<T> &h) :
		    base_string_hash_table::iterator(&h)
			{ }

		iterator(string_hashtable_t<T> *h) :
		    base_string_hash_table::iterator(h)
			{ }

		T get_current() const
		{
		    return ((T)base_string_hash_table::iterator::get_current());
		}

		T get_current_key_and_bucket(char *&key,
		    uint_t &bucket_num) const
		{
			return ((T)base_string_hash_table::iterator::
			    get_current_key_and_bucket((char *)key,
			    bucket_num));
		}
	};
};

// Template orb_hashtable_t allows use of base_obj_hash_table with better
// type checking and avoiding some casts in the code
// Class T should be castable to type uintptr_t (usually a pointer)
// The template is clean and does only casts and should not be generating
// any .o template files.
// The constructor takes a configurable table size and hash function

template<class T> class obj_hashtable_t : public base_obj_hash_table {
public:
	// Specify size of hash table
	// Specify a hash function that takes a Object_ptr key as input and
	// returns an uint_t index. The implementation finds the bucket
	// for this key by doing table[index % size]
	obj_hashtable_t::obj_hashtable_t(
	    uint_t size = DEFAULT_HASHTABLE_SIZE,
	    uint_t (*func)(CORBA::Object_ptr, uint_t) = default_obj_hashfunc);

	// Add an entry to the hash table, allocates memory
	// Does not check if another entry with same key exists
	// Takes a copy of key
	//
	// allocated is a default parameter used to indicate whether
	// caller is interested in knowing the status of dynamic memory
	// allocation.
	// allocated = NULL		-- indicates caller is not interested.
	// allocated = any_other_value	-- indicates caller is interested.
	// *allocated is set to true,if memory allocation succeeds else
	// *allocated is set to false.
	//
	void add(T value, CORBA::Object_ptr key, bool *allocated = NULL);

	// Looks up in the hash table for key and returns the value
	// that was stored along with the key.
	// Returns 0 if key not found.
	T lookup(CORBA::Object_ptr key);

	// Remove the entry in the hash table that corresponds to the key
	// If found, returns the value that was stored along with the key
	// If not found, returns 0.
	T remove(CORBA::Object_ptr key);

	// The iterator class. Use get_current to get current element.
	// Use advance to advance to the next element.

	// Use reinit to either reset the iterator to first element or to
	// use the interator for a different hash table

	// Note for doing remove while using the iterator
	// The iterator keeps a pointer to the current element. Caller should
	// do an advance() before deleting the current element returned by
	// get_current().
	class iterator : public base_obj_hash_table::iterator {
	public :
		iterator(obj_hashtable_t<T> &h) :
		    base_obj_hash_table::iterator(&h)
			{ }

		iterator(obj_hashtable_t<T> *h) :
		    base_obj_hash_table::iterator(h)
			{ }

		T get_current() const
		{
		    return ((T)base_obj_hash_table::iterator::get_current());
		}
	};
};

#ifndef NOINLINES
#include <sys/hashtable_in.h>
#endif  // _NOINLINES

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif	/* _HASHTABLE_H */
