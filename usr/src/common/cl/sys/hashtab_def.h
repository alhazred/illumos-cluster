/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _HASHTAB_DEF_H
#define	_HASHTAB_DEF_H

#pragma ident	"@(#)hashtab_def.h	1.43	08/01/18 SMI"

// Hash Table Template definition
//
// This header file defines a hash table template.
// This code is derived from the Mach Packet Filter (MPF)
// implemented in C and distributed as part of the Mach 3.0
// distribution.
// It has been completely restructured in an object oriented manner
// and reimplemented in C++.
//

/*
 * Mach Operating System
 * Copyright (c) 1993 Carnegie Mellon University
 * All Rights Reserved.
 *
 * Permission to use, copy, modify and distribute this software and its
 * documentation is hereby granted, provided that both the copyright
 * notice and this permission notice appear in all copies of the
 * software, derivative works or modified versions, and any portions
 * thereof, and that both notices appear in supporting documentation.
 *
 * CARNEGIE MELLON ALLOWS FREE USE OF THIS SOFTWARE IN ITS "AS IS"
 * CONDITION.  CARNEGIE MELLON DISCLAIMS ANY LIABILITY OF ANY KIND FOR
 * ANY DAMAGES WHATSOEVER RESULTING FROM THE USE OF THIS SOFTWARE.
 *
 * Carnegie Mellon requests users of this software to return to
 *
 *  Software Distribution Coordinator  or  Software.Distribution@CS.CMU.EDU
 *  School of Computer Science
 *  Carnegie Mellon University
 *  Pittsburgh PA 15213-3890
 *
 * any improvements or extensions that they make and grant Carnegie Mellon
 * the rights to redistribute these changes.
 */
/*
 * Berkeley Packet Filter Definitions from Berkeley
 */

/*
 * Copyright (c) 1990-1991 The Regents of the University of California.
 * All rights reserved.
 *
 * This code is derived from the Stanford/CMU enet packet filter,
 * (net/enet.c) distributed as part of 4.3BSD, and code contributed
 * to Berkeley by Steven McCanne and Van Jacobson both of Lawrence
 * Berkeley Laboratory.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *      This product includes software developed by the University of
 *      California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *      @(#)bpf.h       7.1 (Berkeley) 5/7/91
 *
 * @(#) $Header: bpf.h,v 2.2 93/08/10 15:11:16 mrt Exp $ (LBL)
 */


#include <sys/types.h>
#include <sys/list_def.h>
#include <orb/invo/corba.h>

const uint_t	HASH_KEY_COUNT	= 5;
const uint_t	DEFAULT_HASHTAB_SIZE = 256;

class _ListHashTable {
public:
	class Iterator;
	friend class Iterator;

	uint_t refcount();

	void id(char n[]);
	void dumpTable();

	class hash_entry_t;	// forward decl for .cc file
	typedef IntrList<hash_entry_t, _DList> hash_list_t;

	class hash_entry_t : public _DList::ListElem {
		friend class _ListHashTable;
		friend class Iterator;
	private:
		void *_entry_ptr;
		uint_t keys[HASH_KEY_COUNT];

		hash_entry_t(void *t_ptr, uint_t nkeys, uint_t ekeys[]);
	};

	// The iterator class. Use get_current to get current element.
	// Use advance to advance to the next element.
	class Iterator {
	public:
		Iterator(_ListHashTable &);
		Iterator(_ListHashTable *);

		void *get_current();
		void advance();
	private:
		uint_t _index;
		_DList::ListElem *_current;
		uint_t _hashtab_size;
		hash_list_t *table;
	};
protected:
	// Only allow subclasses to instantiate.
	_ListHashTable(uint_t num_buckets, char n[] = NULL);
	virtual ~_ListHashTable();

	// Add an entry into the hash table with the given key(s).
	// The flag argument specifies how to allocate memory.
	// If successful, entry is returned; otherwise, NULL.
	void *_add(void *entry, uint_t nkeys, uint_t keys[],
		os::mem_alloc_type = os::SLEEP);

	// remove, lookup return the _entry ptr if found and NULL if not found.
	void *_remove(uint_t nkeys, uint_t keys[]);
	void *_lookup(uint_t nkeys, uint_t keys[]);

	//
	// Callback function type.  First argument passed to the callback
	// function is pointer to an element's entry.  Second argument is the
	// callback data (specified in call to iterate() or iterate_remove()).
	//
	typedef bool (*hash_callback_t)(void *entryp, void *callback_data);

	//
	// Iterate through table, and for each element found pass its entry
	// and callback_data to the callback function callbackf.  callbackf
	// can stop the iteration before all elements have been processed by
	// returning false.
	//
	// Callers are responsible for ensuring that the state of the hash
	// table is not modified during the iteration.
	//
	void hash_iterate(hash_callback_t callbackf, void *callback_data);

	//
	// Iterate through table, and for each element found remove it from
	// the table, and pass its entry and callback_data to the
	// callback function callbackf.  callbackf can stop the iteration
	// before all elements have been processed by returning false.
	//
	// Callers are responsible for ensuring that the state of the hash
	// table is not modified during the iteration.
	//
	void hash_iterate_remove(hash_callback_t callbackf,
			void *callback_data);

private:

	char		name[32];
	uint_t		maxkey_count;
	uint_t		ref_count;
	uint_t		hashtab_size;
	hash_list_t	*table;

	// elookup is private function that both lookup and remove call
	hash_entry_t *elookup(hash_list_t &, uint_t nkeys, uint_t keys[]);

	hash_list_t& hash(uint_t nkeys, uint_t keys[]);

	// Disallow assignments and pass by value
	_ListHashTable(const _ListHashTable&);
	_ListHashTable& operator = (_ListHashTable&);
};


//
// Hash table with fixed length keys.  Key length (in words) is provided as
// part of the template definition.  Maximum key length is HASH_KEY_COUNT.
//
template <class H, uint_t nkeys> class hash_table_t : public _ListHashTable {
public:
	hash_table_t(uint_t num_buckets = DEFAULT_HASHTAB_SIZE);

	class HashIterator : private Iterator {
	public:
		//
		// XXX	Workaround for Sun C++ complier 4.2 bug 4012797
		// 	for intel:
		//	constructors of member classes of a template must be
		//	defined inline.  Once this bug is fixed, these
		//	definitions can be moved to <sys/hashtab_def_in.h>.
		//
		HashIterator(_ListHashTable &h) : Iterator(h) {}
		HashIterator(_ListHashTable *h) : Iterator(h) {}

		//
		// XXX	Workaround for intel build problem described in the
		//	Comments section of bug 4256054.  Once this bug is
		//	fixed, these definitions can be moved to
		//	<sys/hashtab_def_in.h>.
		//
		H *get_current() { return ((H *)Iterator::get_current()); }
		void advance() { Iterator::advance(); }
	};

	// Add an entry to the hash table with the given key.  Only the
	// pointer is stored in the table, so the caller should make sure
	// the pointed-to object doesn't go away while still in the hash
	// table.  The flag argument specifies how to allocate memory.
	// If succesful, entry is returned; otherwise NULL.
	H *add(H *entry, uint_t keys[], os::mem_alloc_type = os::SLEEP);

	// remove, lookup return the entry ptr if found and NULL if not found.
	H *remove(uint_t keys[]);
	H *lookup(uint_t keys[]);

	//
	// Iterate through table, and for each element found pass its entry
	// and callback_data to the callback function callbackf.  callbackf
	// can stop the iteration before all elements have been processed by
	// returning false.
	//
	// Callers are responsible for ensuring that the state of the hash
	// table is not modified during the iteration.
	//
	void iterate(bool (*callbackf)(H *, void *), void *callback_data);

	//
	// Delete all hash elements and, optionally, their entries.
	// Pass false to the delete_entries parameter if you don't want delete
	// to be called on the entries (e.g. because they are NOT allocated
	// with new).
	//
	// Callers are responsible for ensuring that the state of the hash
	// table is not modified during dispose().
	//
	void dispose(bool delete_entries = true);

private:
	static bool _dispose_cb(void *entry, void *delete_entries);
};

#include <sys/hashtab_def_in.h>

#endif	/* _HASHTAB_DEF_H */
