/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SSM_H
#define	_SSM_H

#pragma ident	"@(#)ssm.h	1.37	08/05/20 SMI"

#include <netinet/in.h>
#include <sys/clconf.h>
#include <scha_ssm.h>

#ifdef	__cplusplus
extern "C" {
#endif

/*
 * Scalable Services Management (SSM) API
 *
 * This file defines the interfaces for the Scalable Services
 * Management. There are functions to add and remove scalable
 * services, add and remove service instances, and to check if
 * a particular service is a scalable service or not. A service
 * is defined uniquely by the 3-tuple <ip-address, protocol,
 * port number>. More functions will be added to this API as
 * and when deemed necessary.
 */

/*
 * Minimum value for client affinity timeout period.
 */
#define	SSM_MIN_STICKY_TIMEOUT	-1


/*
 * An IP address, port number and a protocol make up a service
 * To represent a IPV4 address, the ipaddr field must be a V4-mapped
 * address. Use the IN6_INADDR_TO_V4MAPPED() and IN6_IPADDR_TO_V4MAPPED()
 * macros in <netinet/in.h> for conversion to V4-mapped addresses.
 */
typedef struct {
	in6_addr_t	ipaddr;
	in_port_t	port;
	uint8_t		protocol;
} ssm_service_t;

/*
 * Structure to identify resources
 */

typedef struct {
	char resource[256];
} ssm_group_t;

/*
 * Create a new scalable service group.
 *
 * Requires:
 *	fields in ssm_group_t should be NULL terminated.
 * Effects:
 * 	This function informs cluster networking that a new scalable service
 * 	is being created.
 * 	Returns SCHA_SSM_SUCCESS on success.
 *
 * Parameters:
 *	g_name (IN) - structure containing the scalable service group name.
 * 	policy (IN) - the load balancing policy that should be used for
 *		 this service group.
 *
 * Exceptions:
 *	SCHA_SSM_NO_GROUP - returned if group is NULL.
 *	SCHA_SSM_GROUP_EXISTS - returned if the specified service group
 *			already exists.
 * 	SCHA_SSM_INVALID_POLICY - returned when the load balancing policy is
 * 			not one of the values defined in scha_ssm_policy_t.
 * 	SCHA_SSM_INTERNAL_ERROR - returned when there are internal problems
 *			with cluster networking.
 *	SCHA_SSM_MOD_NOTLOADED - returned when the primary pdt server is not
 *			found.
 */
scha_ssm_exceptions
ssm_create_scal_srv_grp(ssm_group_t *g_name, scha_ssm_policy_t policy);

/*
 * Add a scalable service.
 *
 * Requires:
 * 	fields in ssm_group_t should be NULL terminated.
 * Effects:
 * 	For each service, cluster networking maintains a list of nodes
 * 	on which the service can be run. This function informs cluster
 * 	networking that a new scalable service is being added to the specified
 * 	group.
 * 	Returns SCHA_SSM_SUCCESS on success or if the service already
 *	exists in the same service group - making the function idempotent.
 *
 * Parameters:
 *	g_name (IN) - structure containing the scalable service group name.
 * 	service (IN) - the scalable service being added.
 *
 * Exceptions:
 *	SCHA_SSM_NO_GROUP - returned if group is NULL.
 * 	SCHA_SSM_NO_SERVICE - returned if the service is NULL.
 * 	SCHA_SSM_SERVICE_CONFLICTS - returned if the service conflicts with an
 *              existing service in another service group.
 * 	SCHA_SSM_INVALID_PROTOCOL - returned when the protocol is not one
 *		of TCP/UDP.
 * 	SCHA_SSM_INVALID_ADDRESS - returned if the IP address is invalid.
 * 	SCHA_SSM_INVALID_PORTNUM - returned if the portnumber is < 0.
 * 	SCHA_SSM_INTERNAL_ERROR - returned when there are internal problems with
 *			     cluster networking.
 *      SCHA_SSM_MOD_NOTLOADED - returned when the primary pdt server is not
 *                      found.
 */
scha_ssm_exceptions
ssm_add_scal_service(ssm_group_t *g_name, ssm_service_t *service);

/*
 * Delete a scalable service group.
 *
 * Requires:
 *	fields in ssm_group_t should be NULL terminated.
 * Effects:
 * 	This function informs cluster networking to delete the specified
 *	scalable group.
 * 	Returns SCHA_SSM_SUCCESS on success.
 *
 * Parameters:
 *	g_name (IN) - structure containing the scalable service group name.
 *
 * Exceptions:
 *	SCHA_SSM_NO_GROUP - returned if group is NULL.
 * 	SCHA_SSM_INVALID_SERVICE_GROUP - returned if the specified service group
 *				doesnt exist.
 * 	SCHA_SSM_INTERNAL_ERROR - returned when there are internal problems with
 *			     cluster networking.
 *      SCHA_SSM_MOD_NOTLOADED - returned when the primary pdt server is not
 *                      found.
 */
scha_ssm_exceptions
ssm_del_scal_srv_grp(ssm_group_t *g_name);

/*
 * Remove the specified scalable service belonging to the specified service
 * group.
 *
 * Requires:
 * 	fields in ssm_group_t should be NULL terminated.
 * Effects:
 * 	This function informs cluster networking to remove the specified
 * 	scalable service belonging to the specified service group.
 * 	Returns SCHA_SSM_SUCCESS on success.
 *
 * Parameters:
 *	g_name (IN) - structure containing the scalable service group name.
 *	service (IN) - structure containing the specified service.
 *
 * Exceptions:
 *	SCHA_SSM_NO_GROUP - returned if group is NULL.
 * 	SCHA_SSM_INVALID_SERVICE_GROUP - returned if the specified service group
 *		doesnt exist.
 * 	SCHA_SSM_NO_SERVICE - returned if the service is NULL.
 *	SCHA_SSM_SERVICE_EXISTS - returned when the group is non-empty (i.e. at
 *		least one service exists).
 * 	SCHA_SSM_INVALID_PROTOCOL - returned when the protocol is not one
 *		of TCP/UDP.
 * 	SCHA_SSM_INVALID_ADDRESS - returned if the IP address is invalid.
 * 	SCHA_SSM_INVALID_PORTNUM - returned if the portnumber is < 0.
 * 	SCHA_SSM_INTERNAL_ERROR - returned when there are internal problems with
 *			     cluster networking.
 *      SCHA_SSM_MOD_NOTLOADED - returned when the primary pdt server is not
 *                      found.
 */
scha_ssm_exceptions
ssm_rem_scal_service(ssm_group_t *g_name, ssm_service_t *service);

/*
 * ssm_get_scal_services
 *
 * Effects:
 *	Return a list of scalable services configured with the given group.
 *
 * Parameters:
 *	g_name (IN) - structure containing the scalable service group name.
 *	svcs (OUT) - returns an array of scalable services
 *	cnt (OUT) - returns number of elements in the returned array
 *
 *	Memory associated with a returned array of non-zero elements must
 *	be freed by caller after use.
 *
 * Exceptions:
 *	SCHA_SSM_INVALID_SERVICE_GROUP
 *		Group doesn't exist
 *
 *	SCHA_SSM_NO_MEMORY
 *		Error allocating memory
 */

scha_ssm_exceptions
ssm_get_scal_services(ssm_group_t *g_name, ssm_service_t **svcs, int *cnt);


/*
 * Configure various parameters of a sticky service group.
 *
 * Requires:
 *      fields in ssm_group_t should be NULL terminated.
 * Effects:
 *      This function informs cluster networking to set the configuration
 *      for the specified service group. If the group is not sticky (that
 *      is, a load balancing policy not equal LB_STICKY or
 *      LB_STICKY_WILD), no action is taken and success is returned.
 *
 *      Returns SCHA_SSM_SUCCESS on success.
 *
 * Parameters:
 *      g_name (IN) - an existing scalable service group
 *      sticky_timeout (IN) - new affinity timeout value
 *      sticky_udp (IN) - should UDP affinity be configured?
 *      sticky_weak (IN) - should weak affinity be configured?
 *
 * Exceptions:
 *      SCHA_SSM_INVALID_SERVICE_GROUP - specified group does not exist
 *      SCHA_SSM_INVALID_STICKY_TIMEOUT - out of range timeout value
 */

scha_ssm_exceptions
ssm_config_sticky_srv_grp(ssm_group_t *g_name, int sticky_timeout,
	boolean_t sticky_udp, boolean_t sticky_weak,
	boolean_t generic_affinity);

scha_ssm_exceptions
ssm_config_rr_srv_grp(ssm_group_t *g_name, boolean_t rr_enabled,
    int conn_threshold);

/*
 * Gives information whether the weights on pdt are initialized/set.
 *
 * Requires:
 *      fields in ssm_group_t should be NULL terminated.
 * Effects:
 *	This function gives us information whether the Load_balancing_weights
 *	are set in the pdt server.
 *      Returns SCHA_SSM_SUCCESS on success.
 *
 * Parameters:
 *      g_name (IN) - an existing scalable service group
 *	result (OUT) - whether the weights are initialized in the pdt
 *	server or not.
 *
 * Exceptions:
 *      SCHA_SSM_INVALID_SERVICE_GROUP - specified group does not exist
 */
scha_ssm_exceptions
ssm_are_weights_set(ssm_group_t *g_name, boolean_t *result);

/*
 * Add a node to the nodelist for all services belonging to a service group.
 *
 * Requires:
 * 	fields in ssm_group_t should be NULL terminated.
 * Effects:
 * 	This function adds the specified node to the list of nodes for all
 * 	the services belonging to the specified service group. This is the
 * 	list of nodes on which the service can be run.
 * 	Returns SCHA_SSM_SUCCESS on success.
 *
 * Parameters:
 *	g_name (IN) - structure containing the scalable service group name.
 *	node (IN) - node number.
 *
 * Exceptions:
 *	SCHA_SSM_NO_GROUP - returned if group is NULL.
 * 	SCHA_SSM_INVALID_SERVICE_GROUP - returned if the specified service group
 *		doesnt exist.
 *	SCHA_SSM_INVALID_NODEID - returned when the node id is <= 0 or
 *		> NODEID_MAX
 *	SCHA_SSM_NODE_EXISTS - the node already exists in the service's
 *		config_list.
 *	SCHA_SSM_INTERNAL_ERROR - returned when we there are internal problems
 *      SCHA_SSM_MOD_NOTLOADED - returned when the primary pdt server is not
 *                      found.
 */
scha_ssm_exceptions
ssm_add_nodeid(ssm_group_t *g_name, nodeid_t node);

/*
 * Remove a node from the nodelist for all services belonging to a
 * resource_group
 *
 * Requires:
 * 	fields in ssm_group_t should be NULL terminated.
 * Effects:
 * 	This function removes a node from the node list of all the services
 * 	belonging to the specified service group.
 * 	Returns SCHA_SSM_SUCCESS on success.
 *
 * Paramaters:
 *	g_name (IN) - structure containing the scalable service group name.
 *	node (IN) - this node will be the removed from the nodelist of
 *		the service.
 *
 * Exceptions:
 *	SCHA_SSM_NO_GROUP - returned if group is NULL.
 * 	SCHA_SSM_INVALID_SERVICE_GROUP - returned if the specified service group
 *		doesnt exist.
 *	SCHA_SSM_INVALID_NODEID - returned when the node id is <= 0 or
 *		> NODEID_MAX
 *	SCHA_SSM_NO_NODEID - the node doesnt exist in the service's config_list.
 *	SCHA_SSM_INTERNAL_ERROR - returned when we there are internal problems
 *			     with cluster networking.
 *      SCHA_SSM_MOD_NOTLOADED - returned when the primary pdt server is not
 *                      found.
 */
scha_ssm_exceptions
ssm_remove_nodeid(ssm_group_t *g_name, nodeid_t node);

/*
 * Requires:
 * 	memory to be allocated for result.
 * Effects:
 * 	This function checks if a particular scalable service group
 * 	exists or not.
 *	Returns SCHA_SSM_SUCCESS on successful completion of the call.
 *
 * Parameters:
 *	g_name (IN) - the scalable service group in question.
 *	result (OUT) - if the service is a scalable service, result is
 *		set to 1, if not, it is set to 0.
 *
 * Exceptions:
 *	SCHA_SSM_NO_GROUP - returned if group is NULL.
 *	SCHA_SSM_INTERNAL_ERROR - returned when there are internal problems
 *			     with cluster networking.
 *      SCHA_SSM_MOD_NOTLOADED - returned when the primary pdt server is not
 *                      found.
 */
scha_ssm_exceptions
ssm_is_scalable_service_group(ssm_group_t *g_name, int *result);

/*
 * Requires:
 * 	memory to be allocated for result.
 * Effects:
 * 	This function checks if a particular service
 *	is a scalable service or not.
 *	Returns SCHA_SSM_SUCCESS on successful completion of the call.
 *
 * Parameters:
 *	service (IN) - the scalable service in question.
 *	result (OUT) - if the service is a scalable service, result is
 *		set to 1, if not, it is set to 0.
 *
 * Exceptions:
 *	SCHA_SSM_NO_SERVICE - returned if the service is NULL.
 *	SCHA_SSM_INVALID_PROTOCOL - returned if the protocol is not one
 *		of TCP/UDP.
 *	SCHA_SSM_INVALID_ADDRESS - returned if the IP address is invalid.
 *	SCHA_SSM_INVALID_PORTNUM - returned if the portnumber is < 0.
 *	SCHA_SSM_INTERNAL_ERROR - returned when there are internal problems
 *			     with cluster networking.
 *      SCHA_SSM_MOD_NOTLOADED - returned when the primary pdt server is not
 *                      found.
 */
scha_ssm_exceptions
ssm_is_scalable_service(ssm_service_t *service, int *result);

/*
 * Requires:
 * Effects:
 *	This assigns the specified IP address to the specified node and
 * 	detaches it from all other gif nodes. The port number in the service
 *	is ignored and the protocol only supports TCP or UDP.
 *
 * Parameters:
 *	service (IN) - structure containing the specified service.
 *	node (IN) - the node which is to be made the gif node.
 *
 * Exceptions:
 *	SCHA_SSM_INVALID_ADDRESS - returned if the IP address is invalid.
 *	SCHA_SSM_INVALID_NODEID - returned when the node id is <= 0 or
 *		> NODEID_MAX
 *	SCHA_SSM_INTERNAL_ERROR - returned when there are internal problems
 *			     with cluster networking.
 *      SCHA_SSM_MOD_NOTLOADED - returned when the primary pdt server is not
 *                      found.
 */
scha_ssm_exceptions
ssm_set_primary_gifnode(ssm_service_t *service, nodeid_t node);

/*
 * Requires:
 *	The ipaddr in the specified service should be a NULL terminated string
 * Effects:
 *	This assigns the specified IP address to the specified node. The group
 *	name is not used in this routine.
 * Parameters:
 * 	service (IN) - structure containing the specified service.
 *	g_name (IN) - the specified scalable service group name.
 *	node (IN) - the node to which the service is to be attached.
 *
 * Exceptions:
 *	SCHA_SSM_INVALID_ADDRESS - returned if the IP address is invalid.
 *	SCHA_SSM_INVALID_NODEID - returned when the node id is <= 0 or
 *		> NODEID_MAX
 *	SCHA_SSM_INTERNAL_ERROR - returned when there are internal problems
 *			     with cluster networking.
 *      SCHA_SSM_MOD_NOTLOADED - returned when the primary pdt server is not
 *                      found.
 */
scha_ssm_exceptions
ssm_attach_gifnode(ssm_group_t *g_name, ssm_service_t *service, nodeid_t node);

/*
 * Requires:
 *	The ipaddr in the specified service should be a NULL terminated string
 * Effects:
 *	This detaches the specified IP address from the specified node. The
 *	group name is not used in this routine.
 * Parameters:
 * 	service (IN) - structure containing the specified service.
 * 	node (IN) - the node from which the service is to be detached.
 *	g_name (IN) - the specified scalable service group name.
 *
 * Exceptions:
 *	SCHA_SSM_INVALID_ADDRESS - returned if the IP address is invalid.
 *	SCHA_SSM_INVALID_NODEID - returned when the node id is <= 0 or
 *		> NODEID_MAX
 *	SCHA_SSM_INTERNAL_ERROR - returned when there are internal problems
 *			     with cluster networking.
 *      SCHA_SSM_MOD_NOTLOADED - returned when the primary pdt server is not
 *                      found.
 */
scha_ssm_exceptions
ssm_detach_gifnode(ssm_group_t *g_name, ssm_service_t *service, nodeid_t node);


/*
 * Converts error codes to meaningful error messages and returns a
 * character pointer to the error message that can then be printed.
 *
 * Requires:
 *	 The error code to be converted as an input parameter.
 *
 * Effects:
 *	This returns a pointer to an error message that can be displayed.
 *
 * Parameters:
 *	The error code as an input parameter.
 *
 * Exceptions:
 *	A pointer to a message saying "unknown error" if the error code
 *	is not recognized.
 */
const char *
ssm_err_code_to_mesg(scha_ssm_exceptions ssm_err_code);

#ifdef	__cplusplus
}
#endif

#endif	/* _SSM_H */
