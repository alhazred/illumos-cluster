/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  sol_conv.h - Conversions between Solaris types and their IDL equivalents
 *
 */

#ifndef _SOL_CONV_H
#define	_SOL_CONV_H

#pragma ident	"@(#)sol_conv.h	1.30	08/05/20 SMI"

#include <sys/vfs.h>
#include <sys/vnode.h>
#include <sys/statvfs.h>
#include <sys/share.h>
#include <h/sol.h>
#include <sys/dc_ki.h>


//
// Conversions between vtype and sol::vtype_t
//

inline sol::vtype_t
conv(enum vtype vtype)
{
	return ((sol::vtype_t)vtype);
}

inline vtype
conv(sol::vtype_t ftype)
{
	return ((enum vtype)ftype);
}

//
// Conversions between vcexcl and sol::vcexcl_t
//

inline sol::vcexcl_t
conv(enum vcexcl vcexcl)
{
	return ((sol::vcexcl_t)vcexcl);
}

inline vcexcl
conv(sol::vcexcl_t fcexcl)
{
	return ((enum vcexcl)fcexcl);
}

//
// Conversions between vattr_t and sol::vattr_t
//
inline vattr &
conv(sol::vattr_t &fattr)
{
	return (*(vattr *)&fattr);
}

inline sol::vattr_t &
conv(vattr_t &vattr)
{
	return (*(sol::vattr_t *)&vattr);
}

inline const vattr &
conv(const sol::vattr_t &fattr)
{
	return (*(const vattr *)&fattr);
}

inline const sol::vattr_t &
conv(const vattr_t &vattr)
{
	return (*(const sol::vattr_t *)&vattr);
}

//
// Conversions between statvfs64_t and sol::statvfs64_t
//
inline statvfs64 &
conv(sol::statvfs64_t &fsstat)
{
	return (*(statvfs64 *)&fsstat);
}

inline sol::statvfs64_t &
conv(statvfs64_t &statvfs)
{
	return (*(sol::statvfs64_t *)&statvfs);
}

inline const statvfs64 &
conv(const sol::statvfs64_t &fsstat)
{
	return (*(const statvfs64 *)&fsstat);
}

inline const sol::statvfs64_t &
conv(const statvfs64_t &statvfs)
{
	return (*(const sol::statvfs64_t *)&statvfs);
}

//
// Conversions between flock64_t and sol::flock64_t
//
inline flock64 &
conv(sol::flock64_t &fl)
{
	return (*(flock64 *)&fl);
}

inline sol::flock64_t &
conv(flock64 &fl)
{
	return (*(sol::flock64_t *)&fl);
}

inline const flock64 &
conv(const sol::flock64_t &fl)
{
	return (*(const flock64 *)&fl);
}

inline const sol::flock64_t &
conv(const flock64 &fl)
{
	return (*(const sol::flock64_t *)&fl);
}

//
// Initialize sol::shrlock_t objects
//
typedef struct shrlock shrlock_t;

//
// Requires: "from_linfo" and "to_linfo" objects already exist (not NULL).
// Modifies: "to_linfo"
// Effects: Initialize fields of "to_linfo" with fields from
//  from "from_linfo."
//
inline void
shr_flatten(struct shrlock *from_linfo, sol::shrlock_t &to_linfo)
{
	//  Copy five fields from C struct to instance of IDL type
	to_linfo.access = from_linfo->s_access;
	to_linfo.deny = from_linfo->s_deny;
	to_linfo.sysid = from_linfo->s_sysid;
	to_linfo.pid = from_linfo->s_pid;
	to_linfo.own_len = from_linfo->s_own_len;

	// Initialize the sequence<octet> of length "len" with the "s_owner"
	// field of "from_linfo." We are using the actual byte sequence
	// from the "from_linfo" struct as the buffer of the sequence.
	// As long as the fourth argument is 'false' (don't release
	// buffer), we're okay.  Otherwise, we could allocate space and
	// bcopy the data.
	int len = to_linfo.own_len;
	to_linfo.owner.load(
		len,	/* maximum length of sequence */
		len,	/* actual length of sequence */
		(uint8_t *)from_linfo->s_owner, /* buffer */
		false);	/* do not release buffer when destroying struct */
}

//
// Requires: "from_linfo," "to_linfo" objects already exist (not NULL).
// Modifies: "to_linfo"
// Effects:  Initialize fields of "to_linfo" from "from_linfo."
//
inline void
shr_unflatten(sol::shrlock_t &from_linfo, struct shrlock *to_linfo)
{
	to_linfo->s_access = from_linfo.access;
	to_linfo->s_deny = from_linfo.deny;
	to_linfo->s_sysid = from_linfo.sysid;
	to_linfo->s_pid = from_linfo.pid;
	to_linfo->s_own_len = from_linfo.own_len;

	// Get the actual data from the owner's field of "from_linfo"
	uint8_t *shrdata = from_linfo.owner.buffer();
	// Now cast this data to caddr_t
	to_linfo->s_owner = (caddr_t)shrdata;
}


//
// Conversion between fsid_t and sol::fsid_t
//
inline fsid_t &
conv(sol::fsid_t &fsidref)
{
	return (*(fsid_t *)&fsidref);
}



//
// Requires: "to_info" and "from_info" already exist (not NULL).
// Modifies: "to_info"
// Effects: Copy four of the fields in "from_info" to "to_info."
//   This is done to give the effect of an in-out parameter.
//
inline void
shr_copyback(struct shrlock *from_info, sol::shrlock_t &to_info)
{
	to_info.access = from_info->s_access;
	to_info.deny = from_info->s_deny;
	to_info.sysid = from_info->s_sysid;
	to_info.pid = from_info->s_pid;
}


#ifdef _KERNEL
#include <sys/procset.h>

//
// Conversions between procset_t and sol::procset_t
// conv(to, from)
//

inline void
conv(procset_t &psp, const sol::procset_t &spsp)
{
	psp.p_op = (idop_t)spsp.p_op;
	psp.p_lidtype = (idtype_t)spsp.p_lidtype;
	psp.p_lid = spsp.p_lid;
	psp.p_ridtype = (idtype_t)spsp.p_ridtype;
	psp.p_rid = spsp.p_rid;
}

inline void
conv(sol::procset_t &spsp, const procset_t &psp)
{
	spsp.p_op = psp.p_op;
	spsp.p_lidtype = psp.p_lidtype;
	spsp.p_lid = psp.p_lid;
	spsp.p_ridtype = psp.p_ridtype;
	spsp.p_rid = psp.p_rid;
}

#include <sys/signal.h>

//
// Conversions between sigsend_t and sol::sigsend_t
// conv(to, from)
//

inline void
conv(sigsend_t &tsig, const sol::sigsend_t &fsig)
{
	tsig.sig = fsig.sig;
	tsig.perm = fsig.perm;
	tsig.checkperm = fsig.checkperm;
	tsig.sicode = fsig.sicode;
	tsig.value.sival_ptr = (void *)fsig.value;
}

inline void
conv(sol::sigsend_t &tsig, const sigsend_t &fsig)
{
	tsig.sig = fsig.sig;
	tsig.perm = fsig.perm;
	tsig.checkperm = fsig.checkperm;
	tsig.sicode = fsig.sicode;
	tsig.value = (int)((long)fsig.value.sival_ptr);
}
#endif

#endif	/* _SOL_CONV_H */
