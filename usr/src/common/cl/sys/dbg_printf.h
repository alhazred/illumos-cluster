/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _DBG_PRINTF_H
#define	_DBG_PRINTF_H

#pragma ident	"@(#)dbg_printf.h	1.28	08/05/20 SMI"

#if defined(DEBUG) && !defined(NO_DBGBUFS) && !defined(DBGBUFS)
#define	DBGBUFS
#endif

#ifdef	__cplusplus
#include <sys/os.h>
#include <sys/list_def.h>
extern "C" {
#else
#include <sys/param.h>
#endif

#ifdef	_KERNEL
#include <sys/varargs.h>
#else
#include <stdarg.h>
#endif

/*
 * How to instantiate and use debug buffers
 *
 * This file is intended to allow debug buffers to be written to from
 * C or C++ code.  Note that the C code is just an interface into the C++
 * code, so the cl_comm module (or whatever module the src/util code is
 * compiled into at the present) must be loaded for it to work.  To print
 * the contents of one of these buffers from kadb/adb, if the name of the
 * global object (in C++ the name of the dbg_print_buf object, and in C the
 * name of the dbgbuf_info_t object) was foo_dbg, you would execute the
 * command "*foo_dbg/s".  If you are running a unode cluster, the buffers
 * will be saved to a file in $UNODE_DIR/[clustername]/node-[nodeid]/, where
 * $UNODE_DIR defaults to /tmp.
 *
 * The debug print buffers must normally be statically allocated
 * because that is a requirement of the unode infrastructure.
 * The only dynamically allocated debug print buffers do not
 * live in unode clusters.
 *
 * To create and use a buffer from C++, you would do the following:
 *
 * In the .h file, put the following defines:
 *
 *   #include <sys/dbg_printf.h>
 *
 *   #if defined(DBGBUFS) && !defined(NO_FOO_DBGBUF)
 *   #define FOO_DBGBUF
 *   #endif
 *
 *   #ifdef FOO_DBGBUF
 *   extern dbg_print_buf foo_dbg;
 *   #define FOO_DBG(a) foo_dbg.dbprintf a
 *   #else
 *   #define FOO_DBG(a)
 *   #endif
 *
 * Then, in one .cc file, put these lines:
 *
 *   #ifdef FOO_DBGBUF
 *   dbg_print_buf foo_dbg(10240, UNODE_DBGOUT_?);
 *   #endif
 *
 * The first argument defines the size of the ring buffer used to store the
 * messages printed.
 *
 * The second argument only applies to unode clusters.  If you specify
 * UNODE_DBGOUT_ALL, the entire contents of the buffer, from when it was
 * first initialized, will be stored in a file.  If you use UNODE_DBGOUT_MMAP,
 * the ring buffer will be mmap-ed to a file, so you easily see the copy that
 * exists in the kernel.  If you use UNODE_DBGOUT_NONE, no file will be
 * created.  This argument defaults to UNODE_DBGOUT_ALL if not specified.
 *
 * Then, whenever you want to print a message, you would do this:
 * FOO_DBG(("The value is %d\n", 45));
 * Note the double parentheses.
 *
 * To create a buffer from C, you would do the following:
 *
 * In the .h file, put the following defines:
 *
 *   #include <sys/dbg_printf.h>
 *
 *   #if defined(DBGBUFS) && !defined(NO_FOO_DBGBUF)
 *   #define FOO_DBGBUF
 *   #endif
 *
 *   #ifdef FOO_DBGBUF
 *   extern dbgbuf_info_t foo_dbg;
 *   #define FOO_DBG(a)	print_to_dbgbuf a
 *   #else
 *   #define FOO_DBG(a)
 *   #endif
 *
 * The external declaration is so that you can print to this debug buffer from
 * any file that includes your .h.
 *
 * Then, in one .c file, put these lines:
 *
 *   #ifdef FOO_DBGBUF
 *   CREATE_DBGBUF(foo_dbg, 10240, UNODE_DBGOUT_?);
 *   #endif
 *
 * The first argument should be the name of the buffer.  The second should be
 * the size of the ring buffer stored in the kernel.  The third argument is
 * the UNODE_DBGOUT_? argument described above, except that there is no
 * default value, so you must specify one.
 *
 * Then, whenever you want to print a message, you would do this:
 * FOO_DBG((&foo_dbg, "The value is %d\n", 45));
 *
 * Because the C pre-processor uses parentheses in parsing, there is no way
 * to write a macro from C that avoids having to put the address of the
 * dbgbuf_info structure every time.
 *
 * If you want to destroy a debug buffer from C (if you are mod-unloading,
 * for instance) you would call destroy_dbgbuf(&foo_dbg);  If you print to
 * the buffer again, it will be re-allocated.
 */

/*
 * Compilation notes
 *
 * If you use the syntax suggested above, all dbgbufs will be turned on if
 * DEBUG is defined and NO_DBGBUFS is not defined.  To turn off the buffers
 * when compiling with DEBUG, define NO_DBGBUFS (on the compile line or
 * by changing this file).  If all dbgbufs are turned on, you can turn off
 * a specific buffer by defining NO_FOO_DBGBUF.  If all dbgbufs are turned
 * off, you can turn on a specific buffer by defining FOO_DBGBUF.  If you
 * do this, make sure the appropriate code is compiled into the cluster
 * code you are working with.
 */

/*
 * Unode debug buffers
 *
 * The default handling of debug buffers was described above.  There are
 * environmental variables that can be set to change the way those buffers
 * are handled.  See the comments and code in src/unode/unode_config* for
 * more information.
 */

typedef enum {
	UNODE_DBGOUT_ALL,	/* Output the entire file */
	UNODE_DBGOUT_MMAP,	/* MMAP the ring buffer to the file */
	UNODE_DBGOUT_NONE	/* Don't bother making the file */
} un_dbgout_t;

typedef struct {
	char *bptr;		/* Pointer to the actual string buffer */
	void *optr;		/* Pointer to the C++ dbg_print_buf object */
	uint_t bsize;		/* The size of the buffer, in bytes */
	un_dbgout_t uprint;	/* Enum for determining unode print opts */
} dbgbuf_info_t;

#define	CREATE_DBGBUF(a, b, c) dbgbuf_info_t a = { NULL, NULL, b, c }

void print_to_dbgbuf(dbgbuf_info_t *, char *fmt, ...);

void destroy_dbgbuf(dbgbuf_info_t *);

#ifdef	__cplusplus
}	// Matching the 'extern "C" {'

// global lock definition.
//
// This lock is used in print_to_dbgbuf function.
// print_to_dbgbuf is defined so that c programs can
// write to debug buffers.
//
extern os::mutex_t c_dbgbuf_lock;

//
// Class to accumulate debug messages in a round-robin fixed size buffer.
//

class dbg_print_buf {
#ifndef _KERNEL
	//
	// following friend class defines pthread_atfork handler functions.
	// They lock handler's when fork()/fork1() is called by user-land
	// process, to avoid fork-one safety problem.
	//
	friend class pthread_atfork_handler;
#endif // _KERNEL
public:
	dbg_print_buf(unsigned int size, un_dbgout_t output = UNODE_DBGOUT_ALL,
	    bool alloc_mem = false);
	dbg_print_buf(dbgbuf_info_t *);
	dbg_print_buf(const dbg_print_buf &dbuf);

	~dbg_print_buf();

	void dbprintf(char *, ...);

	// Continue printing at current location (without reprinting header)
	void dbprintf_cont(char *, ...);

	void dbprintf_va(bool, char *, va_list &);

	char *str();

	// dump the contents of the buffer
	void dump_str();

	// This method  erases all the debug buffer instances from the
	// dbg_print_buf_list. This is called from ORB::shutdown.
	//
	static void dbg_buf_erase_list();

private:
	void lock();
	void unlock();

	// Private method used to wraparound
	void update_tail(unsigned int pad);

	// Instantiate single instance of dbg_print_buf_list.
	static void dbg_buf_list_init();

	char *buf;		// start of buffer
	char *buf_end;		// Current pointer into buffer
	dbgbuf_info_t *c_ptr;	// If initialized from C, the C data structure
	const unsigned int buf_size;	// size of buffer in bytes
	os::mutex_t  lck;
	static const uint_t dbgpadlen;

#if defined(_KERNEL_ORB) && !defined(_KERNEL)
#include <stdio.h> // For FILE pointers
	// Whether we have checked with the symbol table for this buffer name
	bool	name_checked;
	un_dbgout_t print_mode;
	int	filep;		// descriptor for mmapped region, if mmaping
	FILE	*sfilep;	// pointer to STREAM for print, if printing all
	char	*mmap_buf;	// buf, for the mmaped region
	char	*mmap_buf_end;	// buf_end, for the mmaped region
#endif

	// Maintains a list of all the dbg_print_bufs instantiated
	// The list of dbg_print_bufs are locked in the pthread_atfork
	// handlers when fork() is called by the process.
	//
	static SList<dbg_print_buf> *dbg_print_buf_listp;

	static os::mutex_t dbgbuf_list_lock;

	// Disallow assignments
	dbg_print_buf &operator = (const dbg_print_buf &);
};

#include <sys/dbg_printf_in.h>
#endif	/* __cplusplus */

#endif	/* _DBG_PRINTF_H */
