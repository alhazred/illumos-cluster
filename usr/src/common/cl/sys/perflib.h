/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _PERFLIB_H
#define	_PERFLIB_H

#pragma ident	"@(#)perflib.h	1.12	08/05/20 SMI"

#define	CHUNK 256

#include <sys/os.h>
#include <sys/types.h>

class packer {
public:
	packer(int n) : head(0), size(n) {
		s = new char[size];
		s[0] = 0;
	}

	packer() : head(0), size(CHUNK) {
		s = new char[size];
		s[0] = 0;
	}

	~packer() { delete [] s; }
	packer& operator<<(int);
	packer& operator<<(long i) {
		return (*this << (int)i);
	}
	packer& operator<<(unsigned int);
	packer& operator<<(unsigned long i) {
		return (*this << (unsigned int)i);
	}
	packer& operator<<(longlong_t);
	packer& operator<<(u_longlong_t);
	packer& operator<<(float i) {
		return (*this << (double)i);
	}
	packer& operator<<(double);
	packer& operator<<(char *);

	// Return the current state of the string and zero out the internal
	// state. Caller is responsible for deleting the string returned.
	char *str();
private:
	size_t size;
	size_t head;
	char *s;
};

#endif	/* _PERFLIB_H */
