/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * os_in.h
 *
 */

#ifndef _OS_IN_H
#define	_OS_IN_H

#pragma ident	"@(#)os_in.h	1.18	08/05/20 SMI"

#include <sys/param.h>

// These are the inline functions which work in both the kernel and
// in userland.  Kernel only functions are in sys/kos_in.h, and userland
// only functions are in sys/uos_in.h

#ifndef	DEBUG

inline void
os::condvar_t::debug_pre(os::mutex_t *)
{
}

inline void
os::condvar_t::debug_post()
{
}

#endif	// DEBUG

inline os::hrtime_t
os::gethrtime()
{
	return (::gethrtime());
}

#ifndef linux
#include <sys/ddi.h>
#endif

inline int
os::drv_priv(cred_t *cr)
{
#ifdef _KERNEL
	return (::drv_priv(cr));
#else
	return (0); // XXX
#endif
}

inline int
os::drv_getparm(unsigned int parm, void *valuep)
{
#ifdef _KERNEL
	return (::drv_getparm(parm, valuep));
#else
	ASSERT(parm == LBOLT);
	switch (parm) {
	case LBOLT:
		struct timeval tv;
		(void) ::gettimeofday(&tv, NULL);
		*((clock_t *)valuep) = (tv.tv_sec * HZ) +
		    (tv.tv_usec * HZ / 1000000);
		return (0);
	default:
		ASSERT(0);
	}
	return (-1);
#endif
}

inline void
os::tracedump()
{
#ifdef linux
	ASSERT(0);
#else
	::symtracedump();
#endif // linux
}

//
// methods for atomic operations { see <sys/atomic.h> for details }
//
inline void
os::atomic_add_16(uint16_t *target, int16_t delta)
{
	::atomic_add_16(target, delta);
}

inline void
os::atomic_add_32(uint32_t *target, int32_t delta)
{
	::atomic_add_32(target, delta);
}

inline void
os::atomic_add_64(uint64_t *target, int64_t delta)
{
	::atomic_add_64(target, delta);
}

inline uint16_t
os::atomic_add_16_nv(uint16_t *target, int16_t delta)
{
	return (::atomic_add_16_nv(target, delta));
}

inline uint32_t
os::atomic_add_32_nv(uint32_t *target, int32_t delta)
{
	return (::atomic_add_32_nv(target, delta));
}

inline uint64_t
os::atomic_add_64_nv(uint64_t *target, int64_t delta)
{
	return (::atomic_add_64_nv(target, delta));
}

extern os::mutex_t c64lock;

inline void
os::atomic_copy_64(uint64_t *tgt, uint64_t *src)
{
	// Check alignment on processors that require it (see sys/isa_defs.h
	// for more info).  There is no define for the alignment of a
	// uint64_t but we know it is the same as a long long and assert
	// it is the same size.  Note that this assert is optimized away
	// on all current ISAs but wouldn't be for a new ISA that didn't
	// meet the assertion.
	//
#if _ALIGNMENT_REQUIRED == 1
	ASSERT(sizeof (uint64_t) == sizeof (long long));
	ASSERT(!((uintptr_t)tgt & _LONG_LONG_ALIGNMENT - 1) &&
	    !((uintptr_t)src & _LONG_LONG_ALIGNMENT - 1));
#endif

	c64lock.lock();
	*tgt = *src;
	c64lock.unlock();
}

inline
os::notify_t::notify_t() :
	flag(WAITING)
{
}

inline os::notify_t::wait_state
os::notify_t::get_state()
{
	return (flag);
}

inline
os::systime::systime(usec_t off)
{
	setreltime(off);
}

#ifndef INTR_DEBUG
// debug versions of these routines are in os_shared.cc
inline void
os::envchk::set_nonblocking(nbtype)
{
}

inline void
os::envchk::clear_nonblocking(nbtype)
{
}

inline bool
os::envchk::is_nonblocking(int)
{
	return (false);
}

inline void
os::envchk::check_nonblocking(int, char *)
{
}
#endif // INTR_DEBUG

#endif	/* _OS_IN_H */
