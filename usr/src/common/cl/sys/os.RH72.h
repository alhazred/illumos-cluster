/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _OS_RH72_H
#define	_OS_RH72_H

#pragma ident	"@(#)os.RH72.h	1.3	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef linux

#include <getopt.h>
#include <fcntl.h>

#ifndef size32_t
typedef unsigned int size32_t;
typedef int ssize32_t;
#endif

#ifndef _LONG_ALIGNMENT
#define	_LONG_ALIGNMENT 4
#endif

#ifndef cred_t
typedef int cred_t;
#endif

#ifndef THR_DETACHED
#define	THR_DETACHED	0x040
#define	THR_BOUND	0x001
#endif

#ifndef SI_HOSTNAME
#define	SI_HOSTNAME 2
#define	SI_RELEASE 3
#endif

/* don't want signed int8_t from stdint.h */

#include <stdint.h>
/*
#ifndef __int8_t_defined
#define	__int8_t_defined
typedef char			int8_t;
typedef short int		int16_t;
typedef int			int32_t;
typedef unsigned int		uint32_t;
#if __WORDSIZE == 64
typedef long int		int64_t;
#else
__extension__
typedef long long int		int64_t;
#endif
#endif
*/

#include <strings.h>
#include <libintl.h>
#include <sys/time.h>

/* pthreads */
#include <pthread.h>
#define	DEFAULTMUTEX PTHREAD_MUTEX_INITIALIZER
#define	mutex_t pthread_mutex_t
#define	mutex_lock(A) pthread_mutex_lock(A)
#define	mutex_unlock(A) pthread_mutex_unlock(A)
#define	mutex_destroy(A) pthread_mutex_destroy(A)

#ifndef SYS_NMLN
#include <sys/utsname.h>
#ifndef SYS_NMLN
#define	SYS_NMLN 65
#endif
#endif

	/* in_addr_t */

#include <netinet/in.h>
#include <sys/os_ipv6.h>

	/* netinet/in.h */

struct in_addr_solaris {
	union {
		struct { uint8_t s_b1, s_b2, s_b3, s_b4; } _S_un_b;
		struct { uint16_t s_w1, s_w2; } _S_un_w;
#if !defined(_XPG4_2) || defined(__EXTENSIONS__)
		uint32_t _S_addr;
#else
		in_addr_t _S_addr;
#endif /* !defined(_XPG4_2) || defined(__EXTENSIONS__) */
	} _S_un;
};

	/* time_t */
#include <time.h>
/*
 *      Definitions for commonly used resolutions.
 */
#define	SEC		1
#define	MILLISEC	1000
#define	MICROSEC	1000000
#define	NANOSEC		1000000000

typedef struct timespec timestruc_t;    /* definition per SVr4 */

#include <sys/types.h>

typedef unsigned char   uchar_t;
typedef unsigned short  ushort_t;
typedef unsigned int    uint_t;
typedef unsigned long   ulong_t;
typedef long long	longlong_t;
typedef unsigned long long	u_longlong_t;

typedef uint_t		minor_t;
typedef uint_t		major_t;

typedef u_longlong_t hrtime_t;

typedef uchar_t	mblk_t; /* Not used */

typedef void		*timeout_id_t;

typedef enum { B_FALSE, B_TRUE } boolean_t;

typedef short pri_t;

/* sys/param.h */
#define	MAXNAMELEN 255
#define	MAXPATHLEN 1024
#ifndef MAXHOSTNAMELEN
#define	MAXHOSTNAMELEN 512
#endif

/* mhd.h */

#define	MHIOC_RESV_KEY_SIZE	8
typedef struct mhioc_resv_key {
	uchar_t key[MHIOC_RESV_KEY_SIZE];
} mhioc_resv_key_t;

typedef struct mhioc_resv_desc {
	mhioc_resv_key_t	key;
	uint8_t			type;
	uint8_t			scope;
	uint32_t		scope_specific_addr;
} mhioc_resv_desc_t;

typedef struct mhioc_resv_desc_list {
	uint32_t		listsize;
	uint32_t		listlen;
	mhioc_resv_desc_t	*list;
} mhioc_resv_desc_list_t;

typedef struct mhioc_key_list {
	uint32_t		listsize;
	uint32_t		listlen;
	mhioc_resv_key_t	*list;
} mhioc_key_list_t;

#define	SCSI3_RESV_WRITEEXCLUSIVEREGISTRANTSONLY	5

/* cyclic.h */
typedef uintptr_t cyclic_id_t;

/* sysmacros.h */
#ifndef MIN
#define	MIN(a, b)	((a) < (b) ? (a) : (b))
#endif
#ifndef MAX
#define	MAX(a, b)	((a) < (b) ? (b) : (a))
#endif
#ifndef ABS
#define	ABS(a)	((a) < 0 ? -(a) : (a))
#endif

#define	roundup(x, y)   ((((x)+((y)-1))/(y))*(y))

/* cl_uevents.c */
#define	sc_publish_event(subclass, publisher, severity, initiator, ...) 0

/* ddi.h */

#define	LBOLT	4
#define	HZ	100

#define	drv_usectohz(x) ((x)*HZ/MICROSEC)
#define	drv_hztousec(x) ((x)*MICROSEC/HZ)

#ifndef SECSPERMIN
#define	SECSPERMIN 60
#endif

#ifndef processorid_t
/*
 * Type for processor name (CPU number).
 */
typedef int processorid_t;
#endif

#ifndef ipaddr_t
typedef uint32_t ipaddr_t;
#endif

#ifndef xdoor_id
typedef uint32_t xdoor_id;
#endif

#ifndef	PID_t
typedef	int64_t	PID_t;
#endif

#ifndef ASSERT
#include <assert.h>
#define	ASSERT(A) assert(A)
#endif

#ifndef strlcpy
#define	strlcpy(dst, src, dstsize) (strncpy(dst, src, \
	(strlen(src) >= dstsize) ? dstsize - 1 : strlen(src) + 1), \
	strlen(dst) + 1)
#endif

#ifndef strlcat
#define	strlcat(dst, src, dstsize) (strncat(dst, src, \
	(strlen(src) + strlen(dst) >= dstsize) ? dstsize - 1 - strlen(dst) : \
	strlen(dst) + 1), strlen(dst) + 1)
#endif

#endif

#ifdef __cplusplus
}
#endif

#endif /* _OS_RH72_H */
