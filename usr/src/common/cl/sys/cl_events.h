/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CL_EVENTS_H
#define	_CL_EVENTS_H

#pragma ident	"@(#)cl_events.h	1.9	08/05/27 SMI"


/*
 * cl_events.h - interfaces callable by kernel and user state
 *               routines to publish events via sysevent API.
 *
 */

#ifdef _KERNEL
#include <sys/types.h>
#include <sys/time.h>
#include <sys/sysevent.h>
#else
#include <libsysevent.h>
#endif /* _KERNEL */

#ifndef linux
#include <sys/sysevent/eventdefs.h>
#endif
#include <sys/cl_eventdefs.h>


#ifdef __cplusplus
extern "C" {
#endif

/*
 * sc_publish_event()
 *
 * Paramters:
 *
 *	subclass -
 *		a character string representing a cluster event subclass,
 *		as defined in cl_eventdefs.h
 *
 *	publisher -
 *		a character string representing an event publisher
 *		identifier.
 *
 *	severity -
 *		event severity, as defined in cl_eventdefs.h
 *
 *	initiator -
 *		event initiator identifier, as defined in cl_eventdefs.h
 *
 *  reserved-1 -
 *		must be zero
 *
 *  reserved-2 -
 *		must be zero
 *
 *  reserved-3 -
 *		must be zero
 *
 *	The rest of the argument list consists of a list of triples,
 *	terminated by a NULL. Each triple represents an event attribute
 *	and consists of an attribute name, value type and attribute value.
 *
 * Return Values:
 *
 *	Upon successful completion, the function return value is set to 0.
 *	If an error is detected, the return value is set as follows:
 *
 *  CL_EVENT_ENOMEM - not enough memory
 *  CL_EVENT_ECLADM - error during cladm() syscall
 *	CL_EVENT_EATTR  - cannot add event attribute
 *  CL_EVENT_EINVAL - invalid function parameter
 *  CL_EVENT_EPOST  - event publication error
 *
 */

#ifndef linux
cl_event_error_t sc_publish_event(char *subclass, char *publisher,
	cl_event_severity_t severity, cl_event_initiator_t initiator, ...);

/*
 * This interface supports event generation for a zone cluster from the
 * global zone.
 */
cl_event_error_t sc_publish_zc_event(char *zc_namep, char *subclass,
	char *publisher, cl_event_severity_t severity,
	cl_event_initiator_t initiator, ...);
#endif

#ifdef __cplusplus
}
#endif

#endif	/* _CL_EVENTS_H */
