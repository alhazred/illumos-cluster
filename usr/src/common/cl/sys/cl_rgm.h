/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CL_RGM_H
#define	_CL_RGM_H

#pragma ident	"@(#)cl_rgm.h	1.4	08/08/01 SMI"

#include <sys/systm.h>
#include <sys/door.h>

/*
 * The current users are rgmd, zonesd, syseventd. fed currently doesn't serve
 * non-global zones. pmf uses direct open of attached paths.
 */

/* #define	RGM_MAX_DOOR_PATHLEN		35 */
#define	RGM_MAX_DOOR_SYSCALL_CODE	3


#ifdef _KERNEL
#if SOL_VERSION >= __s10

int rgm_get_door_copy_args(void *arg, int *door_code, char *zonename);
int rgm_store_door_copy_args(
    void *arg, int *door_code, char *zonename, char *pathname);
void rgm_store_door(int door_code, door_handle_t h_in, char *zone_in);
door_handle_t rgm_get_door(int door_code, char *zone_in);


struct rgm_door_tuple {
			char *zonename;
			door_handle_t handle;
			};
struct rgm_door_info_t {
			bool use_default_key;
			/*
			hashtable_t<char *, door_handle_t handle> zone_hash;
			*/
			struct rgm_door_tuple *handles;
			};
#endif
#endif

#endif /* _CL_RGM_H */
