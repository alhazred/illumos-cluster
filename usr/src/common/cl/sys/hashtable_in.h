/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  hashtable_in.h
 *
 */

#ifndef _HASHTABLE_IN_H
#define	_HASHTABLE_IN_H

#pragma ident	"@(#)hashtable_in.h	1.17	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

inline bool
base_hash_table::empty()
{
	return (refcount == 0);
}

inline uint_t
base_hash_table::count() const
{
	return (refcount);
}

inline bool
base_hash_table::is_constructed() const
{
	return (status);
}

inline uint_t
base_hash_table::get_number_of_buckets() const
{
	return (hash_size);
}

inline uint_t
base_hash_table::get_number_chained(uint_t bucket_index) const
{
	uint_t i;
	hash_entry_ptr node;

	node = hash_buckets[bucket_index];
	i = 0;

	while (node != NULL) {
		node = node->he_next;
		i++;
	}
	return (i);
}

inline uint_t
base_hash_table::get_size() const
{
	// Compute number of buckets * size of each entry
	uint_t table_size = hash_size * (uint_t)sizeof (hash_entry_ptr);

	uint_t state_size = (uint_t)sizeof (*this);

	// Compute total size of all entries.
	uint_t total_entry_size = 0;
	hash_entry_ptr p;
	// For each bucket, traverse the chaine and compute the
	// size in bytes.
	for (uint_t i = 0; i < hash_size; i++) {
		p = hash_buckets[i];

		while (p != NULL) {
			// pp is pointer to whatever points to p
			total_entry_size = total_entry_size +
			    (uint_t)sizeof (*p);
			p = p->he_next;
		}
	}

	return (table_size + state_size + total_entry_size);
}

inline void
base_hash_table::_add(uintptr_t entry, uint64_t key, bool *allocated)
{
	hash_entry	*hash_elemp = new hash_entry;
	if (allocated != NULL) {
		// set memory allocation status
		*allocated = (hash_elemp != NULL);
	}
	if (hash_elemp != NULL) {
		_add(entry, key,  hash_elemp);
	}
}

inline
base_hash_table::iterator::iterator(base_hash_table *h)
{
	reinit(h);
}

inline uintptr_t
base_hash_table::iterator::get_current() const
{
	return ((_current) == NULL ? (uintptr_t)0 : _current->he_entry);
}

inline uintptr_t
base_hash_table::iterator::get_current_key_and_bucket(void **key,
    uint_t *bucket_num) const
{
	if (_current == NULL) {
		return ((uintptr_t)0);
	} else {
		*key = (void *)_current->he_key;
		*bucket_num = _index;
		return (_current->he_entry);
	}
}

inline bool
base_string_hash_table::empty()
{
	return (refcount == 0);
}

inline uint_t
base_string_hash_table::count() const
{
	return (refcount);
}

inline bool
base_string_hash_table::is_constructed() const
{
	return (status);
}

inline
base_string_hash_table::iterator::iterator(base_string_hash_table *h)
{
	reinit(h);
}

inline uintptr_t
base_string_hash_table::iterator::get_current() const
{
	return ((_current) == NULL ? (uintptr_t)0 : _current->he_entry);
}

inline uintptr_t
base_string_hash_table::iterator::get_current_key_and_bucket(char *&key,
    uint_t &bucket_num) const
{
	if (_current == NULL) {
		return ((uintptr_t)0);
	} else {
		key = (char *)_current->he_key;
		bucket_num = _index;
		return (_current->he_entry);
	}
}

inline bool
base_obj_hash_table::empty()
{
	return (refcount == 0);
}

inline uint_t
base_obj_hash_table::count() const
{
	return (refcount);
}

inline bool
base_obj_hash_table::is_constructed() const
{
	return (status);
}
inline
base_obj_hash_table::iterator::iterator(base_obj_hash_table *h)
{
	reinit(h);
}

inline uintptr_t
base_obj_hash_table::iterator::get_current() const
{
	return ((_current) == NULL ? (uintptr_t)0 : _current->he_entry);
}

template <class T, class K>
inline void
hashtable_t<T, K>::add(T value, K key, bool *allocated)
{
	_add((uintptr_t)value, (uint64_t)key, allocated);
}

template <class T, class K>
inline
hashtable_t<T, K>::hashtable_t(uint_t size, uint_t (*func)(uint64_t, uint_t),
    base_equal_key_function equal, base_free_key_function frfunc,
	base_dup_key_function dup_func) :
	base_hash_table(size, func, equal, frfunc, dup_func)
{
}

// version of add that does not do memory allocation;
template <class T, class K>
inline void
hashtable_t<T, K>::add(T value, K key, hash_entry_ptr ptr)
{
	_add((uintptr_t)value, (uint64_t)key, ptr);
}

// Looks up in the hash table for key and returns the value
// that was stored along with the key.
// Returns 0 if key not found.
template <class T, class K>
inline T
hashtable_t<T, K>::lookup(K key)
{
	return ((T)_lookup((uint64_t)key));
}

// Remove the entry in the hash table that corresponds to the key
// If found, returns the value that was stored along with the key
// If not found, returns 0.
// If the bool argument is set to true, the hash entry is deleted
// if set to false, the caller is responsible for freeing the hash entry
template <class T, class K>
inline T
hashtable_t<T, K>::remove(K key, bool destroy)
{
	return ((T)_remove((uint64_t)key, destroy));
}

template <class T>
inline
string_hashtable_t<T>::string_hashtable_t(uint_t size,
    uint_t (*func)(const char *, uint_t)) :
	base_string_hash_table(size, func)
{
}

// Add an entry to the hash table, allocates memory
// Does not check if another entry with same key exists
// Takes a copy of key
template <class T>
inline void
string_hashtable_t<T>::add(T value, const char *key, bool *allocated)
{
	_add((uintptr_t)value, key, allocated);
}

// Looks up in the hash table for key and returns the value
// that was stored along with the key.
// Returns 0 if key not found.
template <class T>
inline T
string_hashtable_t<T>::lookup(const char *key)
{
	return ((T)_lookup(key));
}

// Remove the entry in the hash table that corresponds to the key
// If found, returns the value that was stored along with the key
// If not found, returns 0.
template <class T>
inline T
string_hashtable_t<T>::remove(const char *key)
{
	return ((T)_remove(key));
}

template <class T>
inline
obj_hashtable_t<T>::obj_hashtable_t(uint_t size,
    uint_t (*func)(CORBA::Object_ptr, uint_t)) :
	base_obj_hash_table(size, func)
{
}

// Add an entry to the hash table, allocates memory
// Does not check if another entry with same key exists
// Takes a copy of key
template <class T>
inline void
obj_hashtable_t<T>::add(T value, CORBA::Object_ptr key, bool *allocated)
{
	_add((uintptr_t)value, key, allocated);
}

// Looks up in the hash table for key and returns the value
// that was stored along with the key.
// Returns 0 if key not found.
template <class T>
inline T
obj_hashtable_t<T>::lookup(CORBA::Object_ptr key)
{
	return ((T)_lookup(key));
}

// Remove the entry in the hash table that corresponds to the key
// If found, returns the value that was stored along with the key
// If not found, returns 0.
template <class T>
inline T
obj_hashtable_t<T>::remove(CORBA::Object_ptr key)
{
	return ((T)_remove(key));
}

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif	/* _HASHTABLE_IN_H */
