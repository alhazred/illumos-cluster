/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _TM_INT_H
#define	_TM_INT_H

#pragma ident	"@(#)tm_int.h	1.15	08/05/20 SMI"

#include <sys/clconf_int.h>

#ifdef  __cplusplus
extern "C" {
#endif

#ifdef _KERNEL_ORB

/*
 * Initialises TM.  It reads the exisisting CCR table and makes the
 * relevant add_path and add_adapter calls to the TCR.
 */
int tm_init(void);

/*
 * Shutdown TM.  Makes relevant remove_path and remove_adapter calls.
 */
void tm_fini(void);

/*
 * CCR callback handler in the TM
 */
void tm_callback(void);

#endif /* _KERNEL_ORB */

#ifndef _KERNEL

#define	TM_ERR_BUF_LEN	2048

#define	TM_ERR_BUFF	0x01	/* Invalid error buffer or buffer length */
#define	TM_ERR_CLST	0x02	/* Invalid clconf cluster object */
#define	TM_ERR_INTL	0x04	/* TM Library internal error - catch all */
#define	TM_ERR_INCP	0x08	/* Incomplete error string */
#define	TM_ERR_CABL	0x10	/* Cable related error */
#define	TM_ERR_NODE	0x20	/* Cluster node related error */
#define	TM_ERR_CLCF	0x40	/* Error in clconf routine */
#define	TM_ERR_EDPT	0x80	/* Invalid cable endpoint */
#define	TM_ERR_EDBB	0x100	/* Invalid blackbox cable endpoint */
#define	TM_ERR_EDND	0x200	/* Invalid node cable endpoint */
#define	TM_ERR_EDED	0x400	/* Error in cable endpoints */
#define	TM_ERR_EDTR	0x800	/* Incompatible endpts - trsprt type mismatch */
#define	TM_ERR_NAME	0x1000	/* Name of one or more objects missing */
#define	TM_ERR_BEAT	0x2000	/* Invalid heartbeat parameters */
#define	TM_ERR_ADPT	0x4000	/* Adapter related error */

/*
 * INPUT ARGS -
 *		1. new cluster tree to be verified.
 *		2. Pointer to a valid error buffer (max len = TM_ERR_BUF_LEN)
 *		3. Size of error buffer passed
 *
 * RETURN ARGS -
 *		1. cluster tree, may include - SCI adp id and IP address
 *		2. Error buffer with appropriate messages (in case of error)
 *
 * RETURN VALUE -
 *		One of the TM error codes defined above.
 */
int	tm_ic_verify(clconf_cluster_t *, char *, uint_t);

/*
 * Update the ip_addresses etc for the new cluster tree.
 */
void	tm_ic_update(clconf_cluster_t *);

#endif /* !_KERNEL */

#ifdef  __cplusplus
}
#endif

#endif	/* _TM_INT_H */
