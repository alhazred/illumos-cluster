/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * The following notice accompanied the original version of this file:
 *
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms are permitted
 * provided that this notice is preserved and that due credit is given
 * to the University of California at Berkeley. The name of the University
 * may not be used to endorse or promote products derived from this
 * software without specific prior written permission. This software
 * is provided ``as is'' without express or implied warranty.
 */

#ifndef _OS_IPV6_H
#define	_OS_IPV6_H

#pragma ident	"@(#)os_ipv6.h	1.4	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef linux

#ifndef in6_addr_t
typedef struct in6_addr in6_addr_t;
#endif

/*
 * Following extracted from Solaris 10 netinet/in.h and
 * converted, via the macros that immediately follow this
 * comment, for use with RedHat 3.0-AS.
 */

#define	_S6_un	in6_u
#define	_S6_u32	u6_addr32
#define	_S6_u8	u6_addr8
#define	_S6_u16	u6_addr16

/*
 * RFC 2553 specifies the following macros. Their type is defined
 * as "int" in the RFC but they only have boolean significance
 * (zero or non-zero). For the purposes of our comment notation,
 * we assume a hypothetical type "bool" defined as follows to
 * write the prototypes assumed for macros in our comments better.
 *
 * typedef int bool;
 */

/*
 * IN6 macros used to test for special IPv6 addresses
 * (Mostly from spec)
 *
 * bool  IN6_IS_ADDR_UNSPECIFIED (const struct in6_addr *);
 * bool  IN6_IS_ADDR_LOOPBACK    (const struct in6_addr *);
 * bool  IN6_IS_ADDR_MULTICAST   (const struct in6_addr *);
 * bool  IN6_IS_ADDR_LINKLOCAL   (const struct in6_addr *);
 * bool  IN6_IS_ADDR_SITELOCAL   (const struct in6_addr *);
 * bool  IN6_IS_ADDR_V4MAPPED    (const struct in6_addr *);
 * bool  IN6_IS_ADDR_V4MAPPED_ANY(const struct in6_addr *); -- Not from RFC2553
 * bool  IN6_IS_ADDR_V4COMPAT    (const struct in6_addr *);
 * bool  IN6_IS_ADDR_MC_RESERVED (const struct in6_addr *); -- Not from RFC2553
 * bool  IN6_IS_ADDR_MC_NODELOCAL(const struct in6_addr *);
 * bool  IN6_IS_ADDR_MC_LINKLOCAL(const struct in6_addr *);
 * bool  IN6_IS_ADDR_MC_SITELOCAL(const struct in6_addr *);
 * bool  IN6_IS_ADDR_MC_ORGLOCAL (const struct in6_addr *);
 * bool  IN6_IS_ADDR_MC_GLOBAL   (const struct in6_addr *);
 * bool  IN6_IS_ADDR_6TO4	 (const struct in6_addr *); -- Not from RFC2553
 * bool  IN6_ARE_6TO4_PREFIX_EQUAL(const struct in6_addr *,
 *	     const struct in6_addr *);			    -- Not from RFC2533
 */

#define	IN6_IS_ADDR_UNSPECIFIED(addr) \
	(((addr)->_S6_un._S6_u32[3] == 0) && \
	((addr)->_S6_un._S6_u32[2] == 0) && \
	((addr)->_S6_un._S6_u32[1] == 0) && \
	((addr)->_S6_un._S6_u32[0] == 0))

#ifdef _BIG_ENDIAN
#define	IN6_IS_ADDR_LOOPBACK(addr) \
	(((addr)->_S6_un._S6_u32[3] == 0x00000001) && \
	((addr)->_S6_un._S6_u32[2] == 0) && \
	((addr)->_S6_un._S6_u32[1] == 0) && \
	((addr)->_S6_un._S6_u32[0] == 0))
#else /* _BIG_ENDIAN */
#define	IN6_IS_ADDR_LOOPBACK(addr) \
	(((addr)->_S6_un._S6_u32[3] == 0x01000000) && \
	((addr)->_S6_un._S6_u32[2] == 0) && \
	((addr)->_S6_un._S6_u32[1] == 0) && \
	((addr)->_S6_un._S6_u32[0] == 0))
#endif /* _BIG_ENDIAN */

#ifdef _BIG_ENDIAN
#define	IN6_IS_ADDR_MULTICAST(addr) \
	(((addr)->_S6_un._S6_u32[0] & 0xff000000) == 0xff000000)
#else /* _BIG_ENDIAN */
#define	IN6_IS_ADDR_MULTICAST(addr) \
	(((addr)->_S6_un._S6_u32[0] & 0x000000ff) == 0x000000ff)
#endif /* _BIG_ENDIAN */

#ifdef _BIG_ENDIAN
#define	IN6_IS_ADDR_LINKLOCAL(addr) \
	(((addr)->_S6_un._S6_u32[0] & 0xffc00000) == 0xfe800000)
#else /* _BIG_ENDIAN */
#define	IN6_IS_ADDR_LINKLOCAL(addr) \
	(((addr)->_S6_un._S6_u32[0] & 0x0000c0ff) == 0x000080fe)
#endif /* _BIG_ENDIAN */

#ifdef _BIG_ENDIAN
#define	IN6_IS_ADDR_SITELOCAL(addr) \
	(((addr)->_S6_un._S6_u32[0] & 0xffc00000) == 0xfec00000)
#else /* _BIG_ENDIAN */
#define	IN6_IS_ADDR_SITELOCAL(addr) \
	(((addr)->_S6_un._S6_u32[0] & 0x0000c0ff) == 0x0000c0fe)
#endif /* _BIG_ENDIAN */

#ifdef _BIG_ENDIAN
#define	IN6_IS_ADDR_V4MAPPED(addr) \
	(((addr)->_S6_un._S6_u32[2] == 0x0000ffff) && \
	((addr)->_S6_un._S6_u32[1] == 0) && \
	((addr)->_S6_un._S6_u32[0] == 0))
#else  /* _BIG_ENDIAN */
#define	IN6_IS_ADDR_V4MAPPED(addr) \
	(((addr)->_S6_un._S6_u32[2] == 0xffff0000U) && \
	((addr)->_S6_un._S6_u32[1] == 0) && \
	((addr)->_S6_un._S6_u32[0] == 0))
#endif /* _BIG_ENDIAN */

/*
 * IN6_IS_ADDR_V4MAPPED - A IPv4 mapped INADDR_ANY
 * Note: This macro is currently NOT defined in RFC2553 specification
 * and not a standard macro that portable applications should use.
 */
#ifdef _BIG_ENDIAN
#define	IN6_IS_ADDR_V4MAPPED_ANY(addr) \
	(((addr)->_S6_un._S6_u32[3] == 0) && \
	((addr)->_S6_un._S6_u32[2] == 0x0000ffff) && \
	((addr)->_S6_un._S6_u32[1] == 0) && \
	((addr)->_S6_un._S6_u32[0] == 0))
#else  /* _BIG_ENDIAN */
#define	IN6_IS_ADDR_V4MAPPED_ANY(addr) \
	(((addr)->_S6_un._S6_u32[3] == 0) && \
	((addr)->_S6_un._S6_u32[2] == 0xffff0000U) && \
	((addr)->_S6_un._S6_u32[1] == 0) && \
	((addr)->_S6_un._S6_u32[0] == 0))
#endif /* _BIG_ENDIAN */

/* Exclude loopback and unspecified address */
#ifdef _BIG_ENDIAN
#define	IN6_IS_ADDR_V4COMPAT(addr) \
	(((addr)->_S6_un._S6_u32[2] == 0) && \
	((addr)->_S6_un._S6_u32[1] == 0) && \
	((addr)->_S6_un._S6_u32[0] == 0) && \
	!((addr)->_S6_un._S6_u32[3] == 0) && \
	!((addr)->_S6_un._S6_u32[3] == 0x00000001))

#else /* _BIG_ENDIAN */
#define	IN6_IS_ADDR_V4COMPAT(addr) \
	(((addr)->_S6_un._S6_u32[2] == 0) && \
	((addr)->_S6_un._S6_u32[1] == 0) && \
	((addr)->_S6_un._S6_u32[0] == 0) && \
	!((addr)->_S6_un._S6_u32[3] == 0) && \
	!((addr)->_S6_un._S6_u32[3] == 0x01000000))
#endif /* _BIG_ENDIAN */

/*
 * Note:
 * IN6_IS_ADDR_MC_RESERVED macro is currently NOT defined in RFC2553
 * specification and not a standard macro that portable applications
 * should use.
 */
#ifdef _BIG_ENDIAN
#define	IN6_IS_ADDR_MC_RESERVED(addr) \
	(((addr)->_S6_un._S6_u32[0] & 0xff0f0000) == 0xff000000)

#else  /* _BIG_ENDIAN */
#define	IN6_IS_ADDR_MC_RESERVED(addr) \
	(((addr)->_S6_un._S6_u32[0] & 0x00000fff) == 0x000000ff)
#endif /* _BIG_ENDIAN */

#ifdef _BIG_ENDIAN
#define	IN6_IS_ADDR_MC_NODELOCAL(addr) \
	(((addr)->_S6_un._S6_u32[0] & 0xff0f0000) == 0xff010000)
#else  /* _BIG_ENDIAN */
#define	IN6_IS_ADDR_MC_NODELOCAL(addr) \
	(((addr)->_S6_un._S6_u32[0] & 0x00000fff) == 0x000001ff)
#endif /* _BIG_ENDIAN */

#ifdef _BIG_ENDIAN
#define	IN6_IS_ADDR_MC_LINKLOCAL(addr) \
	(((addr)->_S6_un._S6_u32[0] & 0xff0f0000) == 0xff020000)
#else  /* _BIG_ENDIAN */
#define	IN6_IS_ADDR_MC_LINKLOCAL(addr) \
	(((addr)->_S6_un._S6_u32[0] & 0x00000fff) == 0x000002ff)
#endif /* _BIG_ENDIAN */

#ifdef _BIG_ENDIAN
#define	IN6_IS_ADDR_MC_SITELOCAL(addr) \
	(((addr)->_S6_un._S6_u32[0] & 0xff0f0000) == 0xff050000)
#else  /* _BIG_ENDIAN */
#define	IN6_IS_ADDR_MC_SITELOCAL(addr) \
	(((addr)->_S6_un._S6_u32[0] & 0x00000fff) == 0x000005ff)
#endif /* _BIG_ENDIAN */

#ifdef _BIG_ENDIAN
#define	IN6_IS_ADDR_MC_ORGLOCAL(addr) \
	(((addr)->_S6_un._S6_u32[0] & 0xff0f0000) == 0xff080000)
#else  /* _BIG_ENDIAN */
#define	IN6_IS_ADDR_MC_ORGLOCAL(addr) \
	(((addr)->_S6_un._S6_u32[0] & 0x00000fff) == 0x000008ff)
#endif /* _BIG_ENDIAN */

#ifdef _BIG_ENDIAN
#define	IN6_IS_ADDR_MC_GLOBAL(addr) \
	(((addr)->_S6_un._S6_u32[0] & 0xff0f0000) == 0xff0e0000)
#else /* _BIG_ENDIAN */
#define	IN6_IS_ADDR_MC_GLOBAL(addr) \
	(((addr)->_S6_un._S6_u32[0] & 0x00000fff) == 0x00000eff)
#endif /* _BIG_ENDIAN */

/*
 * Macros to a) test for 6to4 IPv6 address, and b) to test if two
 * 6to4 addresses have the same /48 prefix, and, hence, are from the
 * same 6to4 site.
 */

#ifdef _BIG_ENDIAN
#define	IN6_IS_ADDR_6TO4(addr) \
	(((addr)->_S6_un._S6_u32[0] & 0xffff0000) == 0x20020000)
#else /* _BIG_ENDIAN */
#define	IN6_IS_ADDR_6TO4(addr) \
	(((addr)->_S6_un._S6_u32[0] & 0x0000ffff) == 0x00000220)
#endif /* _BIG_ENDIAN */

#define	IN6_ARE_6TO4_PREFIX_EQUAL(addr1, addr2) \
	(((addr1)->_S6_un._S6_u32[0] == (addr2)->_S6_un._S6_u32[0]) && \
	((addr1)->_S6_un._S6_u8[4] == (addr2)->_S6_un._S6_u8[4]) && \
	((addr1)->_S6_un._S6_u8[5] == (addr2)->_S6_un._S6_u8[5]))

/*
 * Useful utility macros for operations with IPv6 addresses
 * Note: These macros are NOT defined in the RFC2553 or any other
 * standard specification and are not standard macros that portable
 * applications should use.
 */

/*
 * IN6_V4MAPPED_TO_INADDR
 * IN6_V4MAPPED_TO_IPADDR
 *	Assign a IPv4-Mapped IPv6 address to an IPv4 address.
 *	Note: These macros are NOT defined in RFC2553 or any other standard
 *	specification and are not macros that portable applications should
 *	use.
 *
 * void IN6_V4MAPPED_TO_INADDR(const in6_addr_t *v6, struct in_addr *v4);
 * void IN6_V4MAPPED_TO_IPADDR(const in6_addr_t *v6, ipaddr_t v4);
 *
 */
#define	IN6_V4MAPPED_TO_INADDR(v6, v4) \
	((v4)->s_addr = (v6)->_S6_un._S6_u32[3])
#define	IN6_V4MAPPED_TO_IPADDR(v6, v4) \
	((v4) = (v6)->_S6_un._S6_u32[3])

/*
 * IN6_INADDR_TO_V4MAPPED
 * IN6_IPADDR_TO_V4MAPPED
 *	Assign a IPv4 address address to an IPv6 address as a IPv4-mapped
 *	address.
 *	Note: These macros are NOT defined in RFC2553 or any other standard
 *	specification and are not macros that portable applications should
 *	use.
 *
 * void IN6_INADDR_TO_V4MAPPED(const struct in_addr *v4, in6_addr_t *v6);
 * void IN6_IPADDR_TO_V4MAPPED(const ipaddr_t v4, in6_addr_t *v6);
 *
 */
#ifdef _BIG_ENDIAN
#define	IN6_INADDR_TO_V4MAPPED(v4, v6) \
	((v6)->_S6_un._S6_u32[3] = (v4)->s_addr, \
	(v6)->_S6_un._S6_u32[2] = 0x0000ffff, \
	(v6)->_S6_un._S6_u32[1] = 0, \
	(v6)->_S6_un._S6_u32[0] = 0)
#define	IN6_IPADDR_TO_V4MAPPED(v4, v6) \
	((v6)->_S6_un._S6_u32[3] = (v4), \
	(v6)->_S6_un._S6_u32[2] = 0x0000ffff, \
	(v6)->_S6_un._S6_u32[1] = 0, \
	(v6)->_S6_un._S6_u32[0] = 0)
#else /* _BIG_ENDIAN */
#define	IN6_INADDR_TO_V4MAPPED(v4, v6) \
	((v6)->_S6_un._S6_u32[3] = (v4)->s_addr, \
	(v6)->_S6_un._S6_u32[2] = 0xffff0000U, \
	(v6)->_S6_un._S6_u32[1] = 0, \
	(v6)->_S6_un._S6_u32[0] = 0)
#define	IN6_IPADDR_TO_V4MAPPED(v4, v6) \
	((v6)->_S6_un._S6_u32[3] = (v4), \
	(v6)->_S6_un._S6_u32[2] = 0xffff0000U, \
	(v6)->_S6_un._S6_u32[1] = 0, \
	(v6)->_S6_un._S6_u32[0] = 0)
#endif /* _BIG_ENDIAN */

/*
 * IN6_6TO4_TO_V4ADDR
 *	Extract the embedded IPv4 address from the prefix to a 6to4 IPv6
 *      address.
 *	Note: This macro is NOT defined in RFC2553 or any other standard
 *	specification and is not a macro that portable applications should
 *	use.
 *	Note: we don't use the IPADDR form of the macro because we need
 *	to do a bytewise copy; the V4ADDR in the 6to4 address is not
 *	32-bit aligned.
 *
 * void IN6_6TO4_TO_V4ADDR(const in6_addr_t *v6, struct in_addr *v4);
 *
 */
#define	IN6_6TO4_TO_V4ADDR(v6, v4) \
	((v4)->_S_un._S_un_b.s_b1 = (v6)->_S6_un._S6_u8[2], \
	(v4)->_S_un._S_un_b.s_b2 = (v6)->_S6_un._S6_u8[3],  \
	(v4)->_S_un._S_un_b.s_b3 = (v6)->_S6_un._S6_u8[4],  \
	(v4)->_S_un._S_un_b.s_b4 = (v6)->_S6_un._S6_u8[5])

/*
 * IN6_ARE_ADDR_EQUAL (defined in RFC2292)
 *	 Compares if IPv6 addresses are equal.
 * Note: Compares in order of high likelyhood of a miss so we minimize
 * compares. (Current heuristic order, compare in reverse order of
 * uint32_t units)
 *
 * bool  IN6_ARE_ADDR_EQUAL(const struct in6_addr *,
 *			    const struct in6_addr *);
 */
#define	IN6_ARE_ADDR_EQUAL(addr1, addr2) \
	(((addr1)->_S6_un._S6_u32[3] == (addr2)->_S6_un._S6_u32[3]) && \
	((addr1)->_S6_un._S6_u32[2] == (addr2)->_S6_un._S6_u32[2]) && \
	((addr1)->_S6_un._S6_u32[1] == (addr2)->_S6_un._S6_u32[1]) && \
	((addr1)->_S6_un._S6_u32[0] == (addr2)->_S6_un._S6_u32[0]))

#endif /* !defined(_XPG4_2) || defined(__EXTENSIONS__) */

#ifdef __cplusplus
}
#endif

#endif /* _OS_IPV6_H */
