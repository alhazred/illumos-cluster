/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  refcnt_in.h
 *
 */

#ifndef _REFCNT_IN_H
#define	_REFCNT_IN_H

#pragma ident	"@(#)refcnt_in.h	1.11	08/05/20 SMI"

inline
refcnt::refcnt(uint_t cnt) :
    _refcnt(cnt)
{
}

//
// Increment reference count.
//
inline void
refcnt::hold()
{
	os::atomic_add_32(&_refcnt, 1);
	ASSERT(_refcnt != 0);	// check for wraparound
}

//
// Decrement reference count and delete if zero references are left.
//
inline void
refcnt::rele()
{
	ASSERT(_refcnt != 0);
	uint_t cnt = os::atomic_add_32_nv(&_refcnt, -1);
	if (cnt == 0) {
		// Call the unref method
		refcnt_unref();
	}
}

inline bool
refcnt::verify_count(uint_t cnt)
{
	// This is a hint
	return (_refcnt == cnt);
}

#endif	/* _REFCNT_IN_H */
