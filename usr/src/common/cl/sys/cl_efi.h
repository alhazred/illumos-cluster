/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_CL_EFI_H
#define	_CL_EFI_H

#pragma ident	"@(#)cl_efi.h	1.4	08/05/20 SMI"

#include <sys/sol_version.h>

#ifdef	__cplusplus
extern "C" {
#endif

#if SOL_VERSION >= __s9

/*
 * EFI disks support starts with s9u3,
 * SC uses currenlty a ref_proto not >= __s9u3,
 * when installed on a S9 < s9u3, ioctl DKIOCGGEOM
 * is used to check EFI disk support.
 */
#define	SC_EFI_SUPPORT

/*
 * SC uses currenlty a ref_proto not >= __s9u3
 * so define the few data we need in this case here.
 */
#if SOL_VERSION < __s9u3

#include <sys/dkio.h>

#define	EFI_RESERVED	{ 0x6a945a3b, 0x1dd2, 0x11b2, 0x99, 0xa6, \
			{ 0x08, 0x00, 0x20, 0x73, 0x66, 0x31 } }

#define	DKIOCPARTITION	(DKIOC|9)

struct uuid {
	uint32_t	time_low;
	int16_t		time_mid;
	uint16_t	time_hi_and_version;
	uint8_t		clock_seq_hi_and_reserved;
	uint8_t		clock_seq_low;
	uint8_t		node_addr[6];
};

struct partition64 {
	struct uuid	p_type;
	uint_t		p_partno;
	uint_t		p_resv1;
	diskaddr_t	p_start;
	diskaddr_t	p_size;
};


#else

#include <sys/efi_partition.h>

#endif

/*
 * PSARC2001-570: sectors 15-79 of EFI V_RESERVED
 * partition is the SC PGRE area.
 */
#define	SC_EFI_RESERVED_START	15

#endif // >= s9

#ifdef	__cplusplus
}
#endif

#endif /* _CL_EFI_H */
