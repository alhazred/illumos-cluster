/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * C++ kernel memory allocator that uses kmem_alloc. This allocator is more
 * space efficient than the default ::new because it does not require overhead
 * for storing the object size (since the compiler knows the object size
 * at the time that delete is called).
 *
 * Classes that want to use this allocator should publicly inherit
 * from knewdel.
 * Since this is only relevant for the kernel C++ runtime, In user land
 * inheriting from this class is a noop (but can avoid ifdefs)
 *
 * Note that class knewdel does not have a virtual destructor, adding one would
 * give up the space savings we achieved.
 * Using knewdel has the following side-effect if your code has the following
 * bug:
 *
 * If class B inherits from class A, and both have destructors. Class A's
 * destructor should be marked virtual if we expect to pass a pointer to class A
 * to the delete operator to delete an object of class B.
 * If we do not mark the destructor virtual, the destructor for B will not
 * be called (which is usually a bug)
 *
 * When using knewdel this bug has a disastrous side effect. If one allocates
 * an object of class B but does a delete (A *)ptr, then not only does the
 * destructor for B not get called, knewdel will not get the correct size
 * estimate from the compiler and the kernel is likely to panic. Compiler
 * will pass the size of class A to delete operator instead of size of class B
 * which was allocated.
 *
 * Thus, if you expect to be doing deletes on a base class pointer make sure
 * that the destructor for the base class is virtual.
 */

#ifndef _KNEWDEL_H
#define	_KNEWDEL_H

#pragma ident	"@(#)knewdel.h	1.16	08/05/20 SMI"

#include <sys/os.h>


class knewdel {
#ifdef _KERNEL
public:
	void * operator new(galsize_t, os::mem_alloc_type = os::SLEEP);
	void * operator new(galsize_t, os::mem_zero_type);
	void * operator new(galsize_t, os::mem_alloc_type, os::mem_zero_type);
	void operator delete(void *, galsize_t);
#endif // _KERNEL
};

#ifdef DEBUGGER_PRINT
#include <dbg_knewdel_in.h>
#else
#include <sys/knewdel_in.h>
#endif

#endif	/* _KNEWDEL_H */
