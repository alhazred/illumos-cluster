/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  nodeset_in.h
 *
 */

#ifndef _NODESET_IN_H
#define	_NODESET_IN_H

#pragma ident	"@(#)nodeset_in.h	1.14	08/05/20 SMI"

#include <sys/clconf_int.h>

inline
nodeset::nodeset() :
	_set(0)
{
	// We need to be able to fit the nodes in a longlong.
	ASSERT(NODEID_MAX <= 64);
}

inline
nodeset::nodeset(membership_bitmask_t membership) :
	_set(membership)
{
}

inline
nodeset::nodeset(const nodeset &ns) :
	_set(ns._set)
{
}

inline
nodeset::~nodeset()
{
}

inline bool
nodeset::equal(const nodeset &ns) const
{
	return (_set == ns._set);
}

inline bool
nodeset::superset(const nodeset &ns) const
{
	return ((_set & ns._set) == ns._set);
}

inline bool
nodeset::subset(const nodeset &ns) const
{
	return ((_set & ns._set) == _set);
}

inline nodeid_t
nodeset::next_node(nodeid_t c) const
{
	for (nodeid_t n = (c + 1); n <= NODEID_MAX; n++) {
		if (contains(n))
			return (n);
	}

	return (NODEID_UNKNOWN);
}

inline nodeid_t
nodeset::prev_node(nodeid_t c) const
{
	if (c == NODEID_UNKNOWN)
		return (c);

	for (nodeid_t n = (c - 1); n > NODEID_UNKNOWN; n--) {
		if (contains(n))
			return (n);
	}

	return (NODEID_UNKNOWN);
}

inline void
nodeset::add_node(const nodeid_t nodeid)
{
	_set |=  (1ULL << (nodeid - 1));
}

inline void
nodeset::set(const nodeset &ns)
{
	_set = ns._set;
}

inline void
nodeset::set(const membership_bitmask_t membership)
{
	_set = membership;
}

inline void
nodeset::remove_node(const nodeid_t nodeid)
{
	_set &= ~(1ULL << (nodeid - 1));
}

inline bool
nodeset::contains(const nodeid_t nodeid) const
{
	return (((_set & (1ULL << (nodeid - 1))) != 0) ? true : false);
}

inline void
nodeset::intersect(const nodeset &ns)
{
	_set &= ns._set;
}

inline void
nodeset::add_nodes(const nodeset &ns)
{
	_set |= ns._set;
}

inline void
nodeset::remove_nodes(const nodeset &ns)
{
	_set &= ~ns._set;
}

inline void
nodeset::empty()
{
	_set = 0;
}

inline bool
nodeset::is_empty() const
{
	return (_set == 0);
}

inline membership_bitmask_t
nodeset::bitmask() const
{
	return (_set);
}

#endif	/* _NODESET_IN_H */
