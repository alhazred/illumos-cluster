/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _DIDIO_H
#define	_DIDIO_H

#pragma ident	"@(#)didio.h	1.21	08/05/20 SMI"

/*
 * Dynamic MultiPathing Driver IOCTL Interface
 */


#include <sys/time.h>
#include <sys/param.h>

#ifdef	__cplusplus
extern "C" {
#endif

#define	SUBDEVICE_BITS	5

#define	DID_ASSIGN_INSTANCE	-1

#define	DID_MAX_MINORS		(1 << SUBDEVICE_BITS)
#define	DID_MAX_INSTANCE	8191
#define	DID_MIN_INSTANCE	1

/*
 * These ioctl numbers have been allocated and approved by .
 */
#define	IOCDID		('v' << 8)

/* "control" ioctls (i.e. only valid on unit 0) */
#define	IOCDID_INIT	(IOCDID|0)	/* intialize driver data */
#define	IOCDID_INITDONE	(IOCDID|1)	/* initialization complete */
#define	IOCDID_DISCOVER	(IOCDID|2)	/* discover sub-paths */
#define	IOCDID_INSTCNT	(IOCDID|3)	/* discover sub-paths */
#define	IOCDID_PATHCNT	(IOCDID|4)	/* discover sub-paths */
#define	IOCDID_INSTINFO	(IOCDID|5)	/* discover sub-paths */
#define	IOCDID_PATHINFO	(IOCDID|6)	/* discover sub-paths */
#define	IOCDID_STICK	(IOCDID|7)	/* prevent driver unload */
#define	IOCDID_UNSTICK	(IOCDID|8)	/* permit driver unload-paths */
#define	IOCDID_RMINST	(IOCDID|9)	/* remove an instance */
#define	IOCDID_INITTYPE (IOCDID|17)	/* initialize device type data */
#define	IOCDID_ISFIBRE	(IOCDID|18)	/* is this DID disk fibre-channel */
#define	IOCDID_FORCE_INIT	(IOCDID|19)	/* force driver data init */

/* "user" ioctls */
#define	IOCDIDPATHCOUNT	(IOCDID|10)	/* current number of subpaths */
#define	IOCDIDSTAT	(IOCDID|11)	/* subpath state */
#define	IOCDIDENABLE	(IOCDID|12)	/* enable subpath */
#define	IOCDIDDISABLE	(IOCDID|13)	/* disable subpath */
#define	IOCDIDPERF	(IOCDID|14)	/* return statistics */
#define	IOCDIDERR	(IOCDID|15)	/* return error records */
#define	IOCDIDDISCOVER	(IOCDID|16)	/* discover sub-paths */


/*
 * IODID_INIT :
 */
typedef struct did_init	{
	ddi_devid_t	device_id;
	int	instance;
	dev_t	devtlist[DID_MAX_MINORS];
	char	subpath[MAXPATHLEN];
	int	state;
	int	dev_num;
} did_init_t;

#if defined(_SYSCALL32)
typedef struct did_init32 {
	caddr32_t	device_id;
	int	instance;
	dev32_t	devtlist[DID_MAX_MINORS];
	char	subpath[MAXPATHLEN];
	int	state;
	int	dev_num;
} did_init32_t;
#endif  /* _SYSCALL32 */

/*
 * IOCDID_INITTYPE
 */
typedef struct did_init_type {
	int	dev_num;
	char	device_type[MAXPATHLEN];
	int	minor_count;
	char	minor_name[DID_MAX_MINORS][MAXNAMELEN];
	int	minor_type[DID_MAX_MINORS];
	minor_t minor_number[DID_MAX_MINORS];
	int	default_nodes;
	int	start_default_nodes;
	int	next_default_node;
	int	cur_max_instance;
} did_init_type_t;

#if defined(_SYSCALL32)
typedef struct did_init_type32 {
	int dev_num;
	char	device_type[MAXPATHLEN];
	int	minor_count;
	char	minor_name[DID_MAX_MINORS][MAXNAMELEN];
	int	minor_type[DID_MAX_MINORS];
	minor_t minor_number[DID_MAX_MINORS];
	int	default_nodes;
	int	start_default_nodes;
	int	next_default_node;
	int	cur_max_instance;
} did_init_type32_t;
#endif /* _SYSCALL32 */

/*
 * DEFINES AND STRUCTURE FOR IODIDSTAT
 */

#define	DID_DISABLED		0	/* values assumed by state */
#define	DID_ENABLED		1
#define	DID_BROKE		2
#define	DID_FAILOVER_ONLY	3

typedef struct iodidstat {
	int	subpath_number;
	ddi_devid_t device_id;
	char    subpath_name[MAXPATHLEN];
	int	state;
} iodidstat_t;


/*
 * DEFINES AND STRUCTURES FOR IOCDIDPERF
 */

#define	MINIMUM_DID	0	/* USED TO INDEX read_time/write_time */
#define	AVERAGE_DID	1
#define	MAXIMUM_DID	2

typedef struct iodidperf {
	int	number_of_outstanding_commands;
	int	read_io_operations_per_second;
	int	write_io_operations_per_second;
	int	read_time[3];
	int	write_time[3];
} iodidperf_t;


/*
 * STRUCTURE for IODIDERR
 */

typedef struct did_err {
	int	subpath_number;
	time_t  date_time_occurred;
	time_t  date_time_fixed;
} diderr_t[10];


#ifdef	__cplusplus
}
#endif

#endif	/* _DIDIO_H */
