/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CPR_STATE_H
#define	_CPR_STATE_H

#pragma ident	"@(#)cpr_state.h	1.4	08/05/20 SMI"

#include <sys/callb.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _KERNEL

extern void		set_cpr_state();
extern callb_cpr_t	*get_cpr_state();
extern void		clear_cpr_state();

#endif /* _KERNEL */

#ifdef __cplusplus
}
#endif

#endif	/* _CPR_STATE_H */
