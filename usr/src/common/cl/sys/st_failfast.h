/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ST_FAILFAST_H
#define	_ST_FAILFAST_H

#pragma ident	"@(#)st_failfast.h	1.10	08/05/20 SMI"

#ifdef	__cplusplus
extern "C" {
#endif

/*
 * st_ff_arm() and st_ff_disarm() manage a single proxied failfast
 * object in a single threaded process.  The descriptor for the failfast
 * proxy is stored in a global variable, and the application is assumed to
 * be single threaded.  Multithreaded applications wishing to use failfast
 * objects should link against the clcomm library and use the full failfast
 * interface.  The argument to st_ff_arm should be a character string that
 * identifies the application using the failfast (it will be truncated to 32
 * characters) These functions return error numbers corresponding to possible
 * errors accessing doors, or:
 *
 * EINVAL	_arm was called after _arm, or _disarm after _disarm, or
 * 		_disarm was called first.
 * E2BIG	_arm was called with a name string greater than 32 characters
 *
 * Note that proxy failfasts are only of the _unreferenced variety, they have
 * no "arm time".
 */

int st_ff_arm(char *);
int st_ff_disarm(void);

#ifdef	__cplusplus
}
#endif

#endif	/* _ST_FAILFAST_H */
