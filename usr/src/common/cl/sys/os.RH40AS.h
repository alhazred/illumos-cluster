/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _OS_RH40AS_H
#define	_OS_RH40AS_H

#pragma ident	"@(#)os.RH40AS.h	1.2	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef linux

#ifndef size32_t
typedef unsigned int size32_t;
typedef int ssize32_t;
#endif

#ifndef _LONG_ALIGNMENT
#define	_LONG_ALIGNMENT 4
#endif

#if __BYTE_ORDER == __LITTLE_ENDIAN
#define	_LITTLE_ENDIAN
#elif __BYTE_ORDER == __BIG_ENDIAN
#define	_BIG_ENDIAN
#endif

#ifndef cred_t
typedef int cred_t;
#endif

#ifndef THR_DETACHED
#define	THR_DETACHED	0x040
#define	THR_BOUND	0x001
#endif

#ifndef SI_HOSTNAME
#define	SI_HOSTNAME 2
#define	SI_RELEASE 3
#endif

#include <stdint.h>

#ifndef UINT16_MAX
#define	UINT16_MAX (65535)
#endif

#include <strings.h>
#include <libintl.h>
#include <sys/time.h>

/* pthreads */
#include <pthread.h>
#define	DEFAULTMUTEX PTHREAD_MUTEX_INITIALIZER
#define	mutex_t pthread_mutex_t
#define	mutex_init(A, B, C) pthread_mutex_init(A, C)
#define	mutex_lock(A) pthread_mutex_lock(A)
#define	mutex_unlock(A) pthread_mutex_unlock(A)
#define	mutex_destroy(A) pthread_mutex_destroy(A)
#define	thr_exit(A) pthread_exit(A)
typedef pthread_t thread_t;

#ifndef SYS_NMLN
#include <sys/utsname.h>
#ifndef SYS_NMLN
#define	SYS_NMLN 65
#endif
#endif

	/* in_addr_t */

#include <netinet/in.h>

	/* netinet/in.h */

struct in_addr_solaris {
	union {
		struct { uint8_t s_b1, s_b2, s_b3, s_b4; } _S_un_b;
		struct { uint16_t s_w1, s_w2; } _S_un_w;
#if !defined(_XPG4_2) || defined(__EXTENSIONS__)
		uint32_t _S_addr;
#else
		in_addr_t _S_addr;
#endif /* !defined(_XPG4_2) || defined(__EXTENSIONS__) */
	} _S_un;
};

	/* time_t */
#include <time.h>
/*
 *      Definitions for commonly used resolutions.
 */
#define	SEC		1
#define	MILLISEC	1000
#define	MICROSEC	1000000
#define	NANOSEC		1000000000

typedef struct timespec timestruc_t;    /* definition per SVr4 */

#include <sys/types.h>

typedef unsigned char   uchar_t;
typedef unsigned short  ushort_t;
typedef unsigned int    uint_t;
typedef unsigned long   ulong_t;
typedef long long	longlong_t;
typedef unsigned long long	u_longlong_t;

typedef uint_t		minor_t;
typedef uint_t		major_t;

typedef u_longlong_t hrtime_t;

typedef uchar_t	mblk_t; /* Not used */

typedef void		*timeout_id_t;

typedef enum { B_FALSE, B_TRUE } boolean_t;

typedef short pri_t;

#ifndef  __useconds_t_defined
typedef uint_t		useconds_t;	/* Time, in microseconds */
#endif

#include <fcntl.h>
typedef struct flock flock_t;

struct  netconfig {
	char		*nc_netid;	/* network identifier		*/
	unsigned int	nc_semantics;	/* defined below		*/
	unsigned int	nc_flag;	/* defined below		*/
	char		*nc_protofmly;	/* protocol family name		*/
	char		*nc_proto;	/* protocol name		*/
	char		*nc_device;	/* device name for network id	*/
	unsigned int	nc_nlookups;	/* # of entries in nc_lookups	*/
	char		**nc_lookups;	/* list of lookup directories	*/
	unsigned int	nc_unused[8];	/* borrowed for lockd etc.	*/
};

/* sys/param.h */
#define	MAXNAMELEN 255

/* netdb.h */
#ifndef MAXALIASES
#define	MAXALIASES	35
#endif

/* mhd.h */

#define	MHIOC_RESV_KEY_SIZE	8
typedef struct mhioc_resv_key {
	uchar_t key[MHIOC_RESV_KEY_SIZE];
} mhioc_resv_key_t;

typedef struct mhioc_resv_desc {
	mhioc_resv_key_t	key;
	uint8_t			type;
	uint8_t			scope;
	uint32_t		scope_specific_addr;
} mhioc_resv_desc_t;

typedef struct mhioc_resv_desc_list {
	uint32_t		listsize;
	uint32_t		listlen;
	mhioc_resv_desc_t	*list;
} mhioc_resv_desc_list_t;

typedef struct mhioc_key_list {
	uint32_t		listsize;
	uint32_t		listlen;
	mhioc_resv_key_t	*list;
} mhioc_key_list_t;

#define	SCSI3_RESV_WRITEEXCLUSIVEREGISTRANTSONLY	5

/* cyclic.h */
typedef uintptr_t cyclic_id_t;

/* cl_uevents.c */
#define	sc_publish_event(subclass, publisher, severity, initiator, ...) 0

/* ddi.h */

#define	LBOLT	4

#define	drv_usectohz(x) ((x)*HZ/MICROSEC)
#define	drv_hztousec(x) ((x)*MICROSEC/HZ)

#ifndef SECSPERMIN
#define	SECSPERMIN 60
#endif

#ifndef processorid_t
/*
 * Type for processor name (CPU number).
 */
typedef int processorid_t;
#endif

#ifndef ipaddr_t
typedef uint32_t ipaddr_t;
#endif

#ifndef xdoor_id
typedef uint32_t xdoor_id;
#endif

#ifndef	PID_t
typedef	int64_t	PID_t;
#endif

#ifndef ASSERT
#include <assert.h>
#define	ASSERT(A) assert(A)
#endif

#ifndef strlcpy
#define	strlcpy(dst, src, dstsize) (strncpy(dst, src, \
	(strlen(src) >= dstsize) ? dstsize - 1 : strlen(src) + 1), \
	strlen(dst) + 1)
#endif

#ifndef strlcat
#define	strlcat(dst, src, dstsize) (strncat(dst, src, \
	(strlen(src) + strlen(dst) >= dstsize) ? dstsize - 1 - strlen(dst) : \
	strlen(dst) + 1), strlen(dst) + 1)
#endif

#ifndef in6_addr_t
typedef struct in6_addr in6_addr_t;
#endif

#ifndef IN6_IS_ADDR_V4MAPPED_ANY
#ifdef _BIG_ENDIAN
#define	IN6_IS_ADDR_V4MAPPED_ANY(addr) \
	((((__const uint32_t *)(addr))[3] == 0) && \
	(((__const uint32_t *)(addr))[2] == 0x0000ffff) && \
	(((__const uint32_t *)(addr))[1] == 0) && \
	(((__const uint32_t *)(addr))[0] == 0))
#else  /* _BIG_ENDIAN */
#define	IN6_IS_ADDR_V4MAPPED_ANY(addr) \
	((((__const uint32_t *)(addr))[3] == 0) && \
	(((__const uint32_t *)(addr))[2] == 0xffff0000U) && \
	(((__const uint32_t *)(addr))[1] == 0) && \
	(((__const uint32_t *)(addr))[0] == 0))
#endif /* _BIG_ENDIAN */
#endif

/*
 * Useful utility macros for operations with IPv6 addresses
 * Note: These macros are NOT defined in the RFC2553 or any other
 * standard specification and are not standard macros that portable
 * applications should use.
 */

/*
 * IN6_V4MAPPED_TO_INADDR
 * IN6_V4MAPPED_TO_IPADDR
 *	Assign a IPv4-Mapped IPv6 address to an IPv4 address.
 *	Note: These macros are NOT defined in RFC2553 or any other standard
 *	specification and are not macros that portable applications should
 *	use.
 *
 * void IN6_V4MAPPED_TO_INADDR(const in6_addr_t *v6, struct in_addr *v4);
 * void IN6_V4MAPPED_TO_IPADDR(const in6_addr_t *v6, ipaddr_t v4);
 *
 */
#define	IN6_V4MAPPED_TO_INADDR(v6, v4) \
	((v4)->s_addr = (v6)->s6_addr32[3])
#define	IN6_V4MAPPED_TO_IPADDR(v6, v4) \
	((v4) = (v6)->s6_addr32[3])

/*
 * IN6_INADDR_TO_V4MAPPED
 * IN6_IPADDR_TO_V4MAPPED
 *	Assign a IPv4 address address to an IPv6 address as a IPv4-mapped
 *	address.
 *	Note: These macros are NOT defined in RFC2553 or any other standard
 *	specification and are not macros that portable applications should
 *	use.
 *
 * void IN6_INADDR_TO_V4MAPPED(const struct in_addr *v4, in6_addr_t *v6);
 * void IN6_IPADDR_TO_V4MAPPED(const ipaddr_t v4, in6_addr_t *v6);
 *
 */
#ifdef _BIG_ENDIAN
#define	IN6_INADDR_TO_V4MAPPED(v4, v6) \
	((v6)->s6_addr32[3] = (v4)->s_addr, \
	(v6)->s6_addr32[2] = 0x0000ffff, \
	(v6)->s6_addr32[1] = 0, \
	(v6)->s6_addr32[0] = 0)
#define	IN6_IPADDR_TO_V4MAPPED(v4, v6) \
	((v6)->s6_addr32[3] = (v4), \
	(v6)->s6_addr32[2] = 0x0000ffff, \
	(v6)->s6_addr32[1] = 0, \
	(v6)->s6_addr32[0] = 0)
#else /* _BIG_ENDIAN */
#define	IN6_INADDR_TO_V4MAPPED(v4, v6) \
	((v6)->s6_addr32[3] = (v4)->s_addr, \
	(v6)->s6_addr32[2] = 0xffff0000U, \
	(v6)->s6_addr32[1] = 0, \
	(v6)->s6_addr32[0] = 0)
#define	IN6_IPADDR_TO_V4MAPPED(v4, v6) \
	((v6)->s6_addr32[3] = (v4), \
	(v6)->s6_addr32[2] = 0xffff0000U, \
	(v6)->s6_addr32[1] = 0, \
	(v6)->s6_addr32[0] = 0)
#endif /* _BIG_ENDIAN */
#endif

#ifndef AI_DEFAULT
#define	AI_DEFAULT	(AI_V4MAPPED | AI_ADDRCONFIG)
#endif

//
// Redhat doesn't provide atomic_add_* primitives. We provide our own
// emulations in os_atomic.cc by using pragma weak. We need to declare them
// here to allow the code to compile.
//
extern uint16_t atomic_add_16_nv(uint16_t *, int16_t);
extern uint32_t atomic_add_32_nv(uint32_t *, int32_t);
extern uint64_t atomic_add_64_nv(uint64_t *, int64_t);
extern void atomic_add_16(uint16_t *, int16_t);
extern void atomic_add_32(uint32_t *, int32_t);
extern void atomic_add_64(uint64_t *, int64_t);
extern uint32_t cas32(uint32_t *, uint32_t, uint32_t);
extern uint64_t cas64(uint64_t *, uint64_t, uint64_t);
extern void *casptr(void *, void *, void *);

#ifdef __cplusplus
}
#endif

#endif /* _OS_RH40AS_H */
