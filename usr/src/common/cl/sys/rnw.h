/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RNW_H
#define	_RNW_H

#pragma ident	"@(#)rnw.h	1.8	08/05/20 SMI"

#include <sys/os.h>
#include <orb/invo/common.h>

// Class to support release_and_wait().  This allows you to release
// a reference for an object and wait until _unreferenced() is
// called for the object.  This is useful, for example, if you want
// to unload a kernel module but can't do so until you know its
// code will no longer be called.  release_and_wait() can be called
// by multiple threads simultaneously and all will block until
// _unreferenced() is called, which happens only when the last
// reference is released.  This code is designed to work only if
// there are no additional threads creating new references while
// other threads are waiting for unreferenced.  It is permissible,
// however, to create new references once all calls to
// release_and_wait() have returned and to use release_and_wait()
// for the new references.
//
// To use this you embed an "rnw" object in your object and call the
// rnw's release_and_wait() routine.  You also call rnw's
// rnw_unreferenced() from your object's _unreferenced() handler.
//
// Here is an example using a foo object which keeps a reference
// to the foo object in the foo implementation class.  The
// foo_server_impl class also defines its own release_and_wait so
// it can keep the rnw object private:
//
//	#include <sys/rnw.h>
//
//	class foo_server_impl : public McServerof<foo_mod::foo> {
//	public:
// 		void _unreferenced(unref_t cookie)
// 		{
//			ASSERT(_last_unref(cookie));
// 			foo_rnw.rnw_unreferenced();
// 		}
//
// 		void
// 		foo_release_and_wait()
// 		{
// 			foo_rnw.release_and_wait(foo_obj_ref);
// 		}
//
//	private:
// 		foo_mod::foo_ptr foo_obj_ref;
// 		rnw foo_rnw;
//	};
//
// Code that wants to release and wait as part of unloading a
// module would look like:
//
//	foo_server_impl *our_impl;
//
//	our_impl->foo_release_and_wait();
//	delete our_impl;
//	// continue with module unloading
//
// where our_impl, of course, has been set to an implementation of
// a foo server object, and foo_obj_ref has been set with foo's
// object reference (usually the result of our_impl->get_objref()).
// (The deletion of our_impl could also be at the end foo_server_impl's
// _unreferenced().)
//
// Note that your code must be correct with respect to oustanding
// references and calling release_and_wait() when it will not
// release the last reference will cause it to wait for
// _unreferenced() and if no one else is going to release the last
// reference, release_and_wait() will never return.

class rnw {
public:
	void
	release_and_wait(CORBA::Object_ptr obj)
	{
		rnw_received_unref = 0;

		CORBA::release(obj);

		rnw_unref_mutex.lock();
		while (!rnw_received_unref)
			rnw_unref_cv.wait(&rnw_unref_mutex);
		rnw_unref_mutex.unlock();
	}

	void
	rnw_unreferenced()
	{
		rnw_unref_mutex.lock();
		rnw_received_unref = 1;
		rnw_unref_cv.broadcast();
		rnw_unref_mutex.unlock();
	}

private:
	int		rnw_received_unref;
	os::mutex_t	rnw_unref_mutex;
	os::condvar_t	rnw_unref_cv;
};

#endif	/* _RNW_H */
