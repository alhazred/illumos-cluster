/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _HEARTBEAT_H
#define	_HEARTBEAT_H

#pragma ident	"@(#)heartbeat.h	1.10	08/05/20 SMI"

/*
 * Definitions for heartbeat parameters.
 *
 * This file is included by user land and kernel components, and
 * needs to work in both C and C++
 */

struct heartbeat_struc {
	uint_t		quantum;	/* time interval between heartbeats */
	uint_t		timeout;	/* heartbeat timeout value */
};

typedef struct heartbeat_struc heartbeat_t;

/*
 * These defaults are emergency defaults, and will only be used by the
 * tm when there is no entry in the CCR.  This should NEVER happen!
 *
 * CHANGING THE HB_EMERGENCY_DEFAULT_* PARAMETERS WILL NOT CHANGE THE
 * DEFAULT!  The default values should be already in the ccr, and are
 * determined in user land.
 */
#define	HB_EMERGENCY_DEFAULT_QUANTUM	1000		/* in milliseconds */
#define	HB_EMERGENCY_DEFAULT_TIMEOUT	10000		/* in milliseconds */

/*
 * The heartbeat processing is driven primarily by registration with the
 * Solaris cyclic subsystem with the cyclic "firing" at the rate of
 * PM_CYCLIC_HZ.  If for example PM_CYCLIC_HZ is set to 20, the pm
 * heartbeat processing occurs with a granularity of 50ms.  It does not
 * make sense to specify heartbeat parameters more accurately than
 * 1/PM_CYCLIC_HZ, eg. the quantum (in sec) should never be smaller than
 * 1/PM_CYCLIC_HZ. Note that unode will use PM_CYCLIC_HZ as 4, thus
 * having the granularity of heartbeat processing at 250 ms.
*/
#if !defined(_KERNEL_ORB) || defined(_KERNEL)
#define	PM_CYCLIC_HZ	20
#else
#define	PM_CYCLIC_HZ	4
#endif

/*
 * Minimum and maximum of quantum parameters, in milliseconds.
 */
#define	HB_QUANTUM_MIN	(2*(1000/PM_CYCLIC_HZ))	/* 100 ms minimum */
#define	HB_QUANTUM_MAX	10000			/* 10	s maximum */

/*
 * The timeout minimum requires the knowledge of the actual quantum.
 */
#define	HB_TIMEOUT_MIN(a) (5 * ((a)->quantum))	/* 500 ms minimum */
#define	HB_TIMEOUT_MAX(a) 60000 		/* 60   s maximum */

/*
 * Convenience macros to check for range.
 * Note that for the timeout check, the quantum parameters must also
 * be set properly.
 */
#define	HB_QUANTUM_IN_RANGE(a) \
		((((a)->quantum) >= HB_QUANTUM_MIN) && \
		(((a)->quantum) <= HB_QUANTUM_MAX))
#define	HB_TIMEOUT_IN_RANGE(a) \
		((((a)->timeout) >= HB_TIMEOUT_MIN(a)) && \
		(((a)->timeout) <= HB_TIMEOUT_MAX(a)))

#endif	/* _HEARTBEAT_H */
