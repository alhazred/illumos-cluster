/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_HBX_H
#define	_HBX_H

#pragma ident	"@(#)hbx.h	1.2	08/05/20 SMI"

#include  <sys/hbxctl.h>

#ifdef  __cplusplus
extern "C" {
#endif

/*
 * Internal hbxdrv types and macros
 */

/*
 * Heartbeat message field address offests
 * (from the start of the heartbeat message address) --------------------------
 */
#define	HBX_VERSION_OFFSET	0
#define	HBX_IN_OFFSET		HBX_VERSION_OFFSET + sizeof (hbx_version_t)
#define	HBX_DELAY_OFFSET	HBX_IN_OFFSET + sizeof (hbx_in_t)
#define	HBX_NODE_ID_OFFSET	HBX_DELAY_OFFSET + sizeof (hbx_delay_t)
#define	HBX_CLID_OFFSET		HBX_NODE_ID_OFFSET + sizeof (hbx_node_id_t)
#define	HBX_SN_OFFSET		HBX_CLID_OFFSET + sizeof (hbx_clid_t)
#define	HBX_DIGEST_OFFSET	HBX_SN_OFFSET + sizeof (hbx_sn_t)

/*
 * Heartbeat message sizes ----------------------------------------------------
 */
#define	HBX_HMAC_DIGEST_LEN	20
#define	HBX_MSG_SIZE		HBX_DIGEST_OFFSET + HBX_HMAC_DIGEST_LEN
#define	HBX_PAYLOAD_SIZE	HBX_DIGEST_OFFSET
#define	HBX_UDPHDR_SIZE		sizeof (struct udphdr)
#ifdef Solaris
#define	HBX_ETH_HDR_SIZE	sizeof (struct ether_header)
#define	HBX_IPHDR_SIZE		sizeof (ipha_t)
#endif
#ifdef linux
#define	HBX_ETH_HDR_SIZE	sizeof (struct ethhdr)
#define	HBX_IPHDR_SIZE		sizeof (struct iphdr)
#endif
#define	HBX_ALL_HDR_SIZE	HBX_IPHDR_SIZE + HBX_UDPHDR_SIZE

/*
 * Offset from the start of the heartbeat message packet to the start of the
 * heartbeat payload data (fastpath and non fastpath mode)
 */
#define	HBX_HB_OFFSET		HBX_ALL_HDR_SIZE
#define	HBX_FP_HB_OFFSET	HBX_ETH_HDR_SIZE + HBX_HB_OFFSET

/*
 * Heartbeat message buffer ---------------------------------------------------
 */
typedef uchar_t hbx_msg_t[HBX_MSG_SIZE];

/*
 * Heartbeat payload data type container (used internally) --------------------
 */
typedef struct hbx_hb_data_t {
	hbx_version_t	version;	/* heartbeat protocol version */
	hbx_in_t	in;		/* emitter incarnation number */
	hbx_delay_t	delay;		/* specified detection delay */
	hbx_node_id_t	node_id;	/* node id */
	hbx_clid_t	*clid_addr;	/* cluster identifier */
	hbx_sn_t	sn;		/* heartbeat msg sequence number */
} hbx_hb_data_t;

/*
 * Macros used to access heartbeat message fields -----------------------------
 */
#define	HBX_GET_VERSION_ADDR(a) (hbx_version_t *) \
	    ((uchar_t*)a + HBX_VERSION_OFFSET)
#define	HBX_GET_IN_ADDR(a) (hbx_in_t *) \
	    ((uchar_t*)a + HBX_IN_OFFSET)
#define	HBX_GET_DELAY_ADDR(a) (hbx_delay_t *) \
	    ((uchar_t*)a + HBX_DELAY_OFFSET)
#define	HBX_GET_NODE_ID_ADDR(a) (hbx_node_id_t *) \
	    ((uchar_t*)a + HBX_NODE_ID_OFFSET)
#define	HBX_GET_CLID_ADDR(a) (hbx_clid_t *) \
	    ((uchar_t*)a + HBX_CLID_OFFSET)
#define	HBX_GET_SN_ADDR(a) (hbx_sn_t*) \
	    ((uchar_t*)a + HBX_SN_OFFSET)
#define	HBX_GET_DIGEST_ADDR(a) (uchar_t *) \
	    ((uchar_t*)a + HBX_DIGEST_OFFSET)

/*
 * Macros used to access the heartbeat message start address ------------------
 * (different if eth fast path mode is set )
 */
#define	HBX_GET_HB_ADDR(a) (hbx_msg_t *) \
	    ((uchar_t *)a + HBX_HB_OFFSET)
#define	HBX_GET_FP_HB_ADDR(a) (hbx_msg_t *) \
	    ((uchar_t *)a + HBX_FP_HB_OFFSET)

/*
 * Heartbeat protocol (UDP/IP) ------------------------------------------------
 */
#define	HBX_PROTOCOL	17

/*
 * Transform an IPv4 address in a Ether multicast address ---------------------
 */
#if	(defined(linux) && defined(__BIG_ENDIAN)) || \
	(defined(Solaris) && defined(_BIG_ENDIAN))
#define	HBX_ETHER_MAP_IP_MULTICAST(ipaddr, ethaddr) \
{ \
	((uchar_t *)ethaddr)[0] = 0x01; \
	((uchar_t *)ethaddr)[1] = 0x00; \
	((uchar_t *)ethaddr)[2] = 0x5e; \
	((uchar_t *)ethaddr)[3] = ((uchar_t *)ipaddr)[1] & 0x7f; \
	((uchar_t *)ethaddr)[4] = ((uchar_t *)ipaddr)[2]; \
	((uchar_t *)ethaddr)[5] = ((uchar_t *)ipaddr)[3]; \
}
#else	/* __BIG_ENDIAN */
#define	HBX_ETHER_MAP_IP_MULTICAST(ipaddr, ethaddr) \
{ \
	((uchar_t *)ethaddr)[0] = 0x01; \
	((uchar_t *)ethaddr)[1] = 0x00; \
	((uchar_t *)ethaddr)[2] = 0x5e; \
	((uchar_t *)ethaddr)[3] = ((uchar_t *)ipaddr)[2] & 0x7f; \
	((uchar_t *)ethaddr)[4] = ((uchar_t *)ipaddr)[1]; \
	((uchar_t *)ethaddr)[5] = ((uchar_t *)ipaddr)[0]; \
}
#endif

#ifdef  __cplusplus
}
#endif

#endif	/* _HBX_H */
