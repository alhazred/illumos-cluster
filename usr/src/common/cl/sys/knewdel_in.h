/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  knewdel_in.h
 *
 */

#ifndef _KNEWDEL_IN_H
#define	_KNEWDEL_IN_H

#pragma ident	"@(#)knewdel_in.h	1.4	08/05/20 SMI"

#ifdef _KERNEL

// We do debug check to verify that we are not doing blocking kmem allocations
// in nonblocking contexts
// XX We would like to say NB_ALL here to also catch cases where
// we do blocking memory allocations in _unreferenced. However,
// due to bugid 4247115, we cannot yet.
#define	NB_CHECK_LEVEL	(os::envchk::NB_INTR | os::envchk::NB_INVO)

inline void *
knewdel::operator new(galsize_t size, os::mem_alloc_type flag)
{
	// Debug check to verify that we are not doing blocking kmem allocations
	// in nonblocking contexts
	os::envchk::check_nonblocking((flag == os::SLEEP) ? NB_CHECK_LEVEL : 0,
	    "blocking kmem_alloc");

	return (kmem_alloc((size_t)size, (int)flag));
}

inline void *
knewdel::operator new(galsize_t size, os::mem_zero_type zflag)
{
	// Debug check to verify that we are not doing blocking kmem allocations
	// in nonblocking contexts
	os::envchk::check_nonblocking(NB_CHECK_LEVEL, "blocking kmem_alloc");

	if (zflag == os::ZERO) {
		return (kmem_zalloc((size_t)size, KM_SLEEP));
	} else {
		return (kmem_alloc((size_t)size, KM_SLEEP));
	}
}

inline void *
knewdel::operator new(galsize_t size, os::mem_alloc_type flag,
	os::mem_zero_type zflag)
{
	// Debug check to verify that we are not doing blocking kmem allocations
	// in nonblocking contexts
	os::envchk::check_nonblocking((flag == os::SLEEP) ? NB_CHECK_LEVEL : 0,
	    "blocking kmem_alloc");

	if (zflag == os::ZERO) {
		return (kmem_zalloc((size_t)size, (int)flag));
	} else {
		return (kmem_alloc((size_t)size, (int)flag));
	}
}

inline void
knewdel::operator delete(void *ptr, galsize_t size)
{
	kmem_free(ptr, (size_t)size);
}

#endif  /* _KERNEL */

#endif	/* _KNEWDEL_IN_H */
