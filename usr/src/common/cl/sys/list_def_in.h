/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  list_def_in.h
 *
 */

#ifndef _LIST_DEF_IN_H
#define	_LIST_DEF_IN_H

#pragma ident	"@(#)list_def_in.h	1.32	08/05/20 SMI"

#if !defined(_KERNEL) && !defined(CLCOMM_COMPAT)
// This include supports ASSERTs in the destructors for _DList and _SList
#include <orb/infrastructure/orb.h>
#endif

inline
_DList::_DList() :
    _first(NULL),
    _last(NULL),
    _current(NULL)
{
}

inline bool
_DList::empty()
{
	return (_first == NULL);
}

inline
_DList::~_DList()
{
#if !defined(_KERNEL) && !defined(CLCOMM_COMPAT)
	ASSERT(empty() || ORB::is_shutdown());
#else
	ASSERT(empty());
#endif
}

inline _DList::ListElem *
_DList::first() const
{
	return (_first);
}

// advance
inline void
_DList::advance()
{
	if (_current != NULL) {
		_current = _current->_next;
	}
}

//
// retreat
// We allow _current to be set to NULL if retreating before the first element.
// This allows simple for (l.atlast(); l.get_current(); l.retreat()) loops.
//
inline void
_DList::retreat()
{
	if (_current != NULL) {
		_current = _current->_prev;
	}
}

// atfirst
inline void
_DList::atfirst()
{
	_current = _first;
}

// atlast
inline void
_DList::atlast()
{
	// Should _current be set to NULL OR _last?
	_current = _last;
}

// elem
inline void *
_DList::elem()
const
{
	return ((_current != NULL) ? _current->_elem : NULL);
}

// erase
// delete ListElem also
inline bool
_DList::_erase_elem(void *t)
{
	return (_erase_internal(find_elem(t), true));
}

// erase
// do not delete ListElem
inline bool
_DList::_erase_listelem(ListElem *le)
{
	return (_erase_internal(le, false));
}

// exists
inline bool
_DList::_exists(void *t, bool update)
{
	ListElem *le = find_elem(t);
	if (update)
		_current = le;
	return (le != NULL);
}

inline
_DList::ListElem::ListElem(void *e) :
    _elem(e),
    _next(NULL),
    _prev(NULL)
{
}

inline void *
_DList::ListElem::elem() const
{
	return (_elem);
}

inline _DList::ListElem *
_DList::ListElem::next() const
{
	return (_next);
}

inline void *
_DList::ListElem::next_elem() const
{
	return ((next() == NULL) ? NULL : next()->_elem);
}

inline void
_DList::ListElem::ptr_assert()
{
	ASSERT(_next == NULL);
	ASSERT(_prev == NULL);
}

inline void
_DList::ListElem::ptr_reset()
{
	_next = NULL;
	_prev = NULL;
}

inline
_SList::_SList() :
    _first(NULL),
    _last(NULL),
    _current(NULL)
{
}

inline bool
_SList::empty()
{
	return (_first == NULL);
}

inline
_SList::~_SList()
{
#if !defined(_KERNEL) && !defined(CLCOMM_COMPAT)
	ASSERT(empty() || ORB::is_shutdown());
#else
	ASSERT(empty());
#endif
}

inline _SList::ListElem *
_SList::first() const
{
	return (_first);
}


// advance
inline void
_SList::advance()
{
	if (_current != NULL) {
		_current = _current->_next;
	}
}

//
// retreat
// We allow _current to be set to NULL if retreating before the first element.
// This allows simple for (l.atlast(); l.get_current(); l.retreat()) loops.
//
inline void
_SList::retreat()
{
	if (_current != NULL) {
		_current = find_prev(_current);
	}
}

// atfirst
inline void
_SList::atfirst()
{
	_current = _first;
}

// atlast
inline void
_SList::atlast()
{
	// Should _current be set to NULL OR _last?
	_current = _last;
}

// elem
inline void *
_SList::elem()
const
{
	return ((_current != NULL) ? _current->_elem : NULL);
}

// exists
inline bool
_SList::_exists(void *t, bool update)
{
	ListElem *le = find_elem(t, NULL);
	if (update)
		_current = le;
	return (le != NULL);
}

inline
_SList::ListElem::ListElem(void *e) :
    _elem(e),
    _next(NULL)
{
}

inline void *
_SList::ListElem::elem() const
{
	return (_elem);
}

inline _SList::ListElem *
_SList::ListElem::next() const
{
	return (_next);
}

inline void *
_SList::ListElem::next_elem() const
{
	return ((next() == NULL) ? NULL : next()->_elem);
}

inline void
_SList::ListElem::ptr_assert()
{
	ASSERT(_next == NULL);
}

inline void
_SList::ListElem::ptr_reset()
{
	_next = NULL;
}

template <class T>
inline void
DList<T>::append(T *t, bool *allocated)
{
	ListElem *elem = new ListElem((void *)t);
	if (allocated != NULL) {
		// set memory allocation status
		*allocated = (elem != NULL);
	}
	if (elem != NULL) {
		_DList::_append(elem);
	}
}

template <class T>
inline void
DList<T>::append_nosleep(T *t, bool *allocated)
{
	ListElem *elem = new (os::NO_SLEEP) ListElem((void *)t);
	if (allocated != NULL) {
		// set memory allocation status
		*allocated = (elem != NULL);
	}
	if (elem != NULL) {
		_DList::_append(elem);
	}
}
template <class T>
inline void
DList<T>::prepend(T *t, bool *allocated)
{
	ListElem *elem = new ListElem((void *)t);
	if (allocated != NULL) {
		// set memory allocation status
		*allocated = (elem != NULL);
	}
	if (elem != NULL) {
		_DList::_prepend(elem);
	}
}

template <class T>
inline void
DList<T>::insert_b(T *t, bool *allocated)
{
	ListElem *elem = new ListElem((void *)t);
	if (allocated != NULL) {
		// set memory allocation status
		*allocated = (elem != NULL);
	}
	if (elem != NULL) {
		_DList::_insert_b(elem);
	}
}

template <class T>
inline void
DList<T>::insert_a(T *t, bool *allocated)
{
	ListElem *elem = new ListElem((void *)t);
	if (allocated != NULL) {
		// set memory allocation status
		*allocated = (elem != NULL);
	}
	if (elem != NULL) {
		_DList::_insert_a(elem);
	}
}

template <class T>
inline bool
DList<T>::erase(T *t)
{
	return (_DList::_erase_elem((void *)t));
}

template <class T>
inline T *
DList<T>::reapfirst()
{
	return ((T *)_DList::_reapfirst(true));
}

// Return whether an element exists in the list
// The bool argument indicates whether or not _current should be updated
template <class T>
inline bool
DList<T>::exists(T *t, bool update)
{
	return (_DList::_exists((void *)t, update));
}

// Return the current element from the list
template <class T>
inline T *
DList<T>::get_current(void) const
{
	return ((T *)_DList::elem());
}

// NOTE: Call dispose only when all the elements were created with new
template <class T>
inline void
DList<T>::dispose()
{
	T *ptr;
	while ((ptr = reapfirst()) != NULL) {
		delete ptr;
	}
}

// remove and delete any ListElems that are on the list
// Note: There is a potential for a memory leak if the elements stored
// in the list need to be deleted/further processed. Caller needs
// to make sure that the elements on the list are tracked somewhere
// else also and are appropriately garbage collected
template <class T>
inline void
DList<T>::erase_list()
{
	while (reapfirst() != NULL) {
		/* do nothing */
	}
}

//
// Iterate through list, and for each ListElem found pass its entry
// and callback_data to the callback function callbackf.  callbackf
// can stop the iteration before all ListElems have been processed
// by returning false.  If the iteration stops in this way, the
// function returns the element where it stopped, otherwise it
// returns NULL.
//
// This function can be used when one or more iterations are needed
// (in parallel or serially) without changing the state (_first,
// _last, and _current) of the list.  Compare this to the other
// iterator functions, e.g. atfirst(), advance(), etc.  However,
// callers are responsible for ensuring that the state of the list
// is not modified during the iteration, e.g. by using locks.
//
template <class T>
inline T *
DList<T>::iterate(bool (*callbackf)(T *, void *), void *callback_data)
{
	return ((T *)(_DList::list_iterate((_DList::list_callback_t)callbackf,
		    callback_data)));
}

template <class T>
inline void
SList<T>::append(T *t, bool *allocated)
{
	ListElem *elem = new ListElem((void *)t);
	if (allocated != NULL) {
		// set memory allocation status
		*allocated = (elem != NULL);
	}
	if (elem != NULL) {
		_SList::_append(elem);
	}
}

template <class T>
inline void
SList<T>::prepend(T *t, bool *allocated)
{
	ListElem *elem = new ListElem((void *)t);
	if (allocated != NULL) {
		// set memory allocation status
		*allocated = (elem != NULL);
	}
	if (elem != NULL) {
		_SList::_prepend(elem);
	}
}

template <class T>
inline void
SList<T>::insert_b(T *t, bool *allocated)
{
	ListElem *elem = new ListElem((void *)t);
	if (allocated != NULL) {
		// set memory allocation status
		*allocated = (elem != NULL);
	}
	if (elem != NULL) {
		_SList::_insert_b(elem);
	}
}
template <class T>
inline void
SList<T>::insert_a(T *t, bool *allocated)
{
	ListElem *elem = new ListElem((void *)t);
	if (allocated != NULL) {
		// set memory allocation status
		*allocated = (elem != NULL);
	}
	if (elem != NULL) {
		_SList::_insert_a(elem);
	}
}

template <class T>
inline bool
SList<T>::erase(T *t)
{
	return (_SList::_erase_elem((void *)t));
}

template <class T>
inline T *
SList<T>::reapfirst()
{
	return ((T *)_SList::_reapfirst(true));
}

// Return whether an element exists in the list
// The bool argument indicates whether or not _current should be
// updated
template <class T>
inline bool
SList<T>::exists(T *t, bool update)
{
	return (_SList::_exists((void *)t, update));
}

// Return the current element from the list
template <class T>
inline T *
SList<T>::get_current(void) const
{
	return ((T *)_SList::elem());
}

// NOTE: Call dispose only when all the elements were created with new
template <class T>
inline void
SList<T>::dispose()
{
	T *ptr;
	while ((ptr = reapfirst()) != NULL) {
		delete ptr;
	}
}

// remove and delete any ListElems that are on the list
// Note: There is a potential for a memory leak if the elements stored
// in the list need to be deleted/further processed. Caller needs
// to make sure that the elements on the list are tracked somewhere
// else also and are appropriately garbage collected
template <class T>
inline void
SList<T>::erase_list()
{
	while (reapfirst() != NULL) {
		/* do nothing */
	}
}

//
// Iterate through list, and for each ListElem found pass its entry
// and callback_data to the callback function callbackf.  callbackf
// can stop the iteration before all ListElems have been processed
// by returning false.  If the iteration stops in this way, the
// function returns the element where it stopped, otherwise it
// returns NULL.
//
// This function can be used when one or more iterations are needed
// (in parallel or serially) without changing the state (_first,
// _last, and _current) of the list.  Compare this to the other
// iterator functions, e.g. atfirst(), advance(), etc.  However,
// callers are responsible for ensuring that the state of the list
// is not modified during the iteration, e.g. by using locks.
//
template <class T>
inline T *
SList<T>::iterate(bool (*callbackf)(T *, void *), void *callback_data)
{
	return ((T *)
	    (_SList::list_iterate((_SList::list_callback_t) callbackf,
	    callback_data)));
}

template <class T, class L>
inline T *
IntrList<T, L>::reapfirst()
{
	return ((T *)L::_reapfirst(false));
}

// Return the current element from the list
template <class T, class L>
inline T *
IntrList<T, L>::get_current(void) const
{
	return ((T *)L::elem());
}

// NOTE: Call dispose only when all the elements were created with new
template <class T, class L>
inline void
IntrList<T, L>::dispose()
{
	T *ptr;
	while ((ptr = reapfirst()) != NULL) {
		delete ptr;
	}
}

// remove any ListElems that are on the list
// Note: There is a potential for a memory leak if the elements stored
// in the list need to be deleted/further processed. Caller needs
// to make sure that the elements on the list are tracked somewhere
// else also and are appropriately garbage collected
// This is actually a noop for IntrLists, but is useful to catch leaks
template <class T, class L>
inline void
IntrList<T, L>::erase_list()
{
	while (reapfirst() != NULL) {
		/* do nothing */
	}
}

//
// Iterate through list, and for each T found pass a pointer to it
// and callback_data to the callback function callbackf.  callbackf
// can stop the iteration before all T's have been processed by
// returning false.  If the iteration stops in this way, the
// function returns the element where it stopped, otherwise it
// returns NULL.
//
// This function can be used when one or more iterations are needed
// (in parallel or serially) without changing the state (_first,
// _last, and _current) of the list.  Compare this to the other
// iterator functions, e.g. atfirst(), advance(), etc.  However,
// callers are responsible for ensuring that the state of the list
// is not modified during the iteration, e.g. by using locks.
//
// Note, IntrList implicitly assumes that the _elem member of each
// L::ListElem points to T (hint: see how the get_current works above)
//
template <class T, class L>
inline T *
IntrList<T, L>::iterate(bool (*callbackf)(T *, void *), void *callback_data)
{
	return ((T *)(L::list_iterate(callbackf,
	    callback_data)));
}

#endif	/* _LIST_DEF_IN_H */
