/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CLADM_INT_H
#define	_CLADM_INT_H

#pragma ident	"@(#)cladm_int.h	1.36	08/07/14 SMI"

#include <sys/cladm.h>
#include <sys/sol_version.h>

#if (SOL_VERSION >= __s10)
#include <sys/zone.h>
#endif /* (SOL_VERSION >= __s10) */

#ifdef	__cplusplus
extern "C" {
#endif

/*
 * Define the cluster consolidation private parts of the _cladm() system call.
 */

/*
 * This interface is used internally to interpose on the _cladm system call
 * (see 4829869 Need interposition support for _cladm)
 */
extern int cladm(int, int, void *);

/*
 * Data structures for the various facilities.
 */

/*
 * Data structure for obtaining the cluster name.
 */
typedef struct clcluster_name {
	size_t		len;		/* length of name array in bytes */
	char		*name;		/* cluster name array */
} clcluster_name_t;

#if defined(_SYSCALL32)

typedef struct clcluster_name32 {
	size32_t	len;		/* length of name array in bytes */
	caddr32_t	name;		/* cluster name array */
} clcluster_name32_t;

#endif	/* defined(_SYSCALL32) */

/*
 * Data structure for obtaining a node's name.
 */
typedef struct clnode_name {
	nodeid_t	nodeid;		/* nodeid to get node name for */
	size_t		len;		/* length of name array in bytes */
	char		*name;		/* node name array */
} clnode_name_t;

#if defined(_SYSCALL32)

typedef struct clnode_name32 {
	nodeid_t	nodeid;		/* nodeid to get node name for */
	size32_t	len;		/* length of name array in bytes */
	caddr32_t	name;		/* node name array */
} clnode_name32_t;

#endif	/* defined(_SYSCALL32) */

#if (SOL_VERSION >= __s10)

typedef struct zcnode_name {
	char		zc_name[ZONENAME_MAX]; /* Zone cluster name */
	nodeid_t	nodeid;		/* nodeid to get the node name */
	size_t		len;		/* Length of the name array in bytes. */
	char		*namep;		/* node name array */
} zcnode_name_t;

#if defined(_SYSCALL32)

typedef struct zcnode_name32 {
	char		zc_name[ZONENAME_MAX]; /* Zone cluster name */
	nodeid_t	nodeid;		/* nodeid to get node name for */
	size32_t	len;		/* length of name array in bytes */
	caddr32_t	namep;		/* node name array */
} zcnode_name32_t;

#endif	/* defined(_SYSCALL32) */
/*
 * Data structure for obtaining a clusterized zone name.
 */
typedef struct cz_name {
	uint_t		clid;		/* cz id to get cz name for */
	size_t		len;		/* length of name array in bytes */
	char		*namep;		/* cz name array */
} cz_name_t;

#if defined(_SYSCALL32)

typedef struct cz_name32 {
	uint32_t	clid;		/* cz id to get cz name for */
	size32_t	len;		/* length of name array in bytes */
	caddr32_t	name;		/* cz name array */
} cz_name32_t;
#endif	/* defined(_SYSCALL32) */

/*
 * Data structure for obtaining a clusterized zone id.
 */
typedef struct cz_id_t {
	uint_t		clid;		/* cz id to get cz name for */
	char		name[ZONENAME_MAX];	/* cz name array */
} cz_id_t;

#if defined(_SYSCALL32)

typedef struct cz_id32 {
	uint32_t	clid;		/* cz id to get cz name for */
	char		name[ZONENAME_MAX];	/* cz name array */
} cz_id32_t;
#endif	/* defined(_SYSCALL32) */

/*
 * Data structure for obtaining a clusterized zone subnet.
 */
typedef struct cz_subnet {
	char		name[ZONENAME_MAX]; /* cz name to get the subnet for */
	char 		sub_net[INET6_ADDRSTRLEN]; /* cz subnet array */
} cz_subnet_t;

#if defined(_SYSCALL32)

typedef struct cz_subnet32 {
	char		name[ZONENAME_MAX]; /* cz name to get the subnet for */
	char		sub_net[INET6_ADDRSTRLEN];	/* cz subnet array */
} cz_subnet32_t;

#endif	/* defined(_SYSCALL32) */

#endif /* (SOL_VERSION >= __s10) */

/*
 * Data structure for returning the Solaris door file descriptor and
 * MD5 hash for the type ID of the interface.
 */
typedef struct cllocal_ns_data {
	int		filedesc;	/* Solaris door file descriptor */
	uint32_t	tid[4];		/* 128 bit MD5 hash for type ID */
} cllocal_ns_data_t;

/*
 * Data structure to a log string message in
 * the cladm_dbg kernel circular log buffer.
 */
#define	CLADMIN_LOG_SIZE	1024

typedef struct cladmin_log {
	int level;
	char str[CLADMIN_LOG_SIZE];
} cladmin_log_t;

#if defined(_SYSCALL32)
typedef struct cladmin_log32 {
	int level;
	char str[CLADMIN_LOG_SIZE];
} cladmin_log32_t;
#endif /* _SYSCALL32 */

/*
 * Command definitions for each of the facilities.
 * The type of the data pointer and the direction of the data transfer
 * is listed for each command.
 */

/*
 * CL_CONFIG facility commands.
 * Note that CL_NODEID (0) and CL_HIGHEST_NODEID (1) are defined in
 * <sys/cladm.h>.
 */
#define	CL_CLUSTER_ENABLE	5	/* Join/leave the cluster. */
					/* int - In */
#define	CL_GET_LOCAL_NS		6	/* Obsolete: use CL_GET_NS_DATA */
					/* int - Out */
#define	CL_GBLMNT_ENABLE	7	/* Enable/disable global mounts. */
					/* int - In */
#define	CL_GBLMNT_LOCK		8	/* Lock a device (for fsck/mounting). */
					/* string - In */
#define	CL_GBLMNT_UNLOCK	9	/* Unlock a device. */
					/* string - In */
#define	CL_UNMOUNT_DISABLE	10	/* Disables global unmounts from this */
					/* node. */
					/* Argument is unused */
#define	CL_GET_NS_DATA		11	/* Return info for local name server. */
					/* cllocal_ns_data_t * - Out */
#define	CL_SWITCHBACK_ENABLE	12	/* Start switchbacks . */
#define	CL_GET_CLUSTER_NAME	15	/* return the cluster's name. */
#define	CL_GET_NODE_NAME	16	/* return name of requested node. */
#define	CL_GET_CCR_ENTRY	17	/* given table and entry, get value */
#define	CL_GET_CLCONF_CHILD_STATS 18	/* Returns statistics about the */
					/* children (of a given type) of a */
					/* particular node in the kernel */
					/* clconf tree */
#define	CL_GET_VP_RUNNING_VERSION	19	/* Ret the proto's */
					/* version number */
#define	CL_RGM_GET_DOOR		20	/* get rgm related door handle */
#define	CL_RGM_STORE_DOOR	21	/* store rgm related door handles */
#define	CL_GET_CCR_PRIVIP	22	/* get privip ccr table values */
#define	CL_GET_SCSHUTDOWN	23	/* Is the cluster shutting down via */
					/* scshutdown(1M) */
#if (SOL_VERSION >= __s10)
#define	CL_GET_ZC_NAME		24	/* return the clusterized zone name */
#define	CL_GET_ZC_ID		25	/* return the clusterized zone Id */
#define	CL_GET_CZ_SUBNET	26	/* return the clusterized zone subnet */
#define	CL_GET_ZC_NODE_NAME	27	/* return the zone cluster node name */

#endif /* (SOL_VERSION >= __s10) */

/* Fault injection framework support */
#define	CL_FAULT_RPC		1000	/* Fault injection RPC. */
#define	CL_FAULT_GET_NODETRIG	1001	/* For user-land to get NodeTrigger. */
#define	CL_FAULT_DO_SETUP_WAIT	1002	/* Calls kernel-level */
					/* FaultFunctions::do_setup_wait(). */
#define	CL_FAULT_DO_WAIT	1003	/* Calls kernel-level */
					/* FaultFunctions::do_wait(). */
#define	CL_FAULT_WAIT		1004	/* Calls kernel-level */
					/* FaultFunctions::wait(). */
#define	CL_FAULT_SIGNAL		1005	/* Calls kernel-level */
					/* FaultFunctions::signal(). */
#define	CL_FAULT_REBOOT_ALL_NODES 1006	/* Calls kernel-level */
					/* FaultFunctions::reboot_all_nodes */
#define	CL_FAULT_RM_PRIMARY_NODE 1007	/* Calls kernel-level */
					/* FaultFunctions::rm_primary_node */
#define	CL_FAULT_FAILPATH	1008	/* Calls kernel-level */
					/* FaultFunctions::failpath */

/* orbadmin tool support */
#define	CL_ORBADMIN_STATS_MSG	2000	/* Read orb message statistics */
#define	CL_ORBADMIN_STATS_FLOW	2001	/* Read orb flow control statistics */
#define	CL_ORBADMIN_STATS_UNREF 2002	/* Read orb unref statistics */
#define	CL_ORBADMIN_FLOW_READ	2003	/* Read orb flow control settings */
#define	CL_ORBADMIN_FLOW_CHANGE	2004	/* Change orb flow control settings */
#define	CL_ORBADMIN_THREADS_MIN_READ 2005 /* Read server thread minimum value */
#define	CL_ORBADMIN_THREADS_MIN_CHANGE 2006 /* Change server thread minimum */
#define	CL_ORBADMIN_STATE_POOL	2007	/* Read orb resource_pool state */
#define	CL_ORBADMIN_STATE_BALANCER 2008	/* Read orb resource_balancer state */
#define	CL_ORBADMIN_STATE_UNREF	2009	/* Read orb unref_threadpool state */
#define	CL_ORBADMIN_STATE_REFCOUNT 2010	/* Read orb reference count state */

/* cladm debug support */
#define	CL_ADMIN_LOG	3000	/* log a string in cladm_dbg log buffer */

/* version manager get version call types */
#define	VM_NODE_OR_CLUSTER		1
#define	VM_NODEPAIR			2

#ifdef	__cplusplus
}
#endif

#endif	/* _CLADM_INT_H */
