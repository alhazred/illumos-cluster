/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  nodeset.h
 *
 * 	This class uses a bitmask to represent a set of nodes.
 *
 */

#ifndef _NODESET_H
#define	_NODESET_H

#pragma ident	"@(#)nodeset.h	1.25	08/05/20 SMI"

#include <sys/types.h>
#include <sys/boolean.h>
#include <sys/cl_assert.h>
#include <sys/clconf.h>
#ifdef	linux
#include <stdint.h>
#endif

//
// A bitmask to represent nodes.
// If this is changed, then the corresponding change should be made to
// quorum.idl
//
typedef uint64_t membership_bitmask_t;

class nodeset {
public:
	// By default the set is initialized to be empty.
	nodeset();

	// Initialize with a bitmask membership
	nodeset(membership_bitmask_t);
	// Initialize with another nodeset.
	nodeset(const nodeset &ns);
	~nodeset();

	// Remove all nodes.
	void	empty();

	// Is the set empty?
	bool	is_empty() const;
	bool	equal(const nodeset &ns) const;
	bool	contains(const nodeid_t nodeid) const;
	bool	superset(const nodeset &ns) const;
	bool	subset(const nodeset &ns) const;
	nodeid_t next_node(nodeid_t n) const;
	nodeid_t prev_node(nodeid_t n) const;
	void	add_node(const nodeid_t nodeid);
	void	add_nodes(const nodeset &ns);
	void	remove_node(const nodeid_t nodeid);
	void	remove_nodes(const nodeset &ns);
	void	intersect(const nodeset &ns);
	void	set(const nodeset& ns);
	void	set(const membership_bitmask_t);
	membership_bitmask_t bitmask() const;

private:
	membership_bitmask_t _set;

	//
	// Disallow assignment and comparison operators.
	//
	nodeset& operator = (const nodeset &ns);
	bool operator == (const nodeset &ns);
	bool operator != (const nodeset &ns);
};

#include <sys/nodeset_in.h>

#endif	/* _NODESET_H */
