/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_HBXCTL_H
#define	_HBXCTL_H

#pragma ident	"@(#)hbxctl.h	1.2	08/05/20 SMI"

#ifdef Solaris /* Solaris -------------------------------------------------- */
#include <sys/types.h>
#include <sys/time.h>
#include <netinet/in.h>

#endif /* Solaris ---------------------------------------------------------- */

#ifdef linux /* linux ------------------------------------------------------ */

#include <linux/types.h>
#include <linux/ioctl.h>

#endif /* linux ------------------------------------------------------------ */

#ifdef  __cplusplus
extern "C" {
#endif

/*
 * Common Basic types and constants for compatibility accross platforms
 */
#ifdef linux /* linux ------------------------------------------------------ */
typedef enum { B_FALSE, B_TRUE } boolean_t;
typedef uint32_t ipaddr_t;
typedef time_t hbx_time_t;
typedef unsigned char uchar_t;
#endif /* linux */

#ifdef Solaris /* Solaris -------------------------------------------------- */
typedef hrtime_t hbx_time_t;
#endif /* Solaris */

/*
 * Compatible types definitions
 */

/*
 * Heartbeat frequency types --------------------------------------------------
 */
typedef uint32_t hbx_delay_t;
#define	HBX_MAX_DELAY   10000 /* detection delay value max */
#define	HBX_MIN_DELAY   1000  /* detection delay value min */
#define	HBX_DEF_DELAY   5000  /* detection delay default value */

typedef uint32_t hbx_numhb_t; /* heartbeat # per detection delay */
#define	HBX_MIN_FREQ	300   /* minimum hb freq, in millisecs */
#define	HBX_DEF_FREQ	1000  /* default hb freq, in millisecs */

/*
 * Heartbeat identification types ---------------------------------------------
 */
typedef uint32_t	hbx_in_t;
typedef uint32_t	hbx_sn_t;
typedef uint32_t	hbx_node_id_t;
typedef uint32_t	hbx_clid_t;

typedef	uint16_t	hbx_version_t;
#define	HBX_PROTOCOL_VERSION	0x0100

/*
 * Heartbeat authentication types ---------------------------------------------
 */
#define	HBX_PKEY_LEN	64 /* private authentication key */
typedef char hbx_pkey_t[HBX_PKEY_LEN + 1];

/*
 * Heartbeat driver parameters container (used to set/get parameters) ---------
 */
#define	HBX_SYSLOG_PERIOD	3600	/* Dflt syslog logging period (sec) */
#define	HBX_UDP_DST_PORT	1515	/* Default hb UDP destination port */
#define	HBX_IP_TTL		1	/* Routing is disabled by default */

typedef struct hbx_params_t {
	/* Emission frequency parameters */
	hbx_delay_t	delay;
	hbx_numhb_t	numhb;
	/* Identification parameters */
	hbx_node_id_t	node_id;
	hbx_clid_t	clid;
	hbx_version_t	version;
	/* Authentication parameters */
	hbx_pkey_t	*pkey_addr;
	hbx_pkey_t	*alt_pkey_addr;
	/* UDP/IP parameters */
	uint16_t	udp_dst_port;
	uint8_t		ttl;
	/* Tunable parameters */
	boolean_t	debug_flag;
	hbx_delay_t	syslog_period;
} hbx_params_t;

/*
 * Device type (to reference network dev internally and w/ ioctl) -------------
 */
#ifdef Solaris
typedef uint64_t hbx_dev_t;
#endif
#ifdef linux
typedef struct net_device *hbx_dev_t;
#endif

/*
 * MAC address types ----------------------------------------------------------
 */
#define	HBX_MAC_MAX_LEN	16 /* mac address */
typedef uchar_t hbx_mac_addr_t[HBX_MAC_MAX_LEN];

/*
 * hbxdrv ioctl list
 *
 * Linux impl.  : only emit mode is supported
 * Solaris impl.: emit + rcv modes are supported
 */

#ifdef Solaris /* Solaris -------------------------------------------------- */
#define	HBX_IOC		0x80201ae0
#define	HBX_IO(b, o)	(int)(b + o)
#endif /* Solaris */
#ifdef linux /* linux ------------------------------------------------------ */
#define	HBX_IOC		0xAE
#define	HBX_IO(a, b)	_IO(a, b)
#endif /* linux */

#define	HBX_IOC_ADD_DEV			HBX_IO(HBX_IOC, 1) /* Linux only */
#define	HBX_IOC_DEL_DEV			HBX_IO(HBX_IOC, 2) /* Linux only */
#define	HBX_IOC_LIST_DEV		HBX_IO(HBX_IOC, 3)
#define	HBX_IOC_GET_DEV			HBX_IO(HBX_IOC, 4)
#define	HBX_IOC_SET_ID			HBX_IO(HBX_IOC, 5)
#define	HBX_IOC_EMIT_CTL		HBX_IO(HBX_IOC, 6)
#define	HBX_IOC_EMIT_SET_FREQ		HBX_IO(HBX_IOC, 7)
#define	HBX_IOC_EMIT_GET_FREQ		HBX_IO(HBX_IOC, 8)
#define	HBX_IOC_SET_AUTH		HBX_IO(HBX_IOC, 9)
#define	HBX_IOC_GET_AUTH		HBX_IO(HBX_IOC, 10)
#define	HBX_IOC_SET_IDENT		HBX_IO(HBX_IOC, 11)
#define	HBX_IOC_GET_IDENT		HBX_IO(HBX_IOC, 12)
#define	HBX_IOC_SET_DEBUG		HBX_IO(HBX_IOC, 13)
#define	HBX_IOC_SET_SYSLOG_PERIOD	HBX_IO(HBX_IOC, 14) /* rcv mode only */
#define	HBX_IOC_LIST_HOST		HBX_IO(HBX_IOC, 15) /* rcv mode only */
#define	HBX_IOC_GET_HOST		HBX_IO(HBX_IOC, 16) /* rcv mode only */
#define	HBX_IOC_DEL_HOST		HBX_IO(HBX_IOC, 17) /* rcv mode only */

#define	HBX_IOC_MAX_NR	17

static char *hbx_ioc[] = {
	"IOC_ADD_DEV",
	"IOC_DEL_DEV",
	"IOC_LIST_DEV",
	"IOC_GET_DEV",
	"IOC_SET_ID",
	"IOC_EMIT_CTL",
	"IOC_EMIT_SET_FREQ",
	"IOC_EMIT_GET_FREQ",
	"IOC_EMIT_SET_AUTH",
	"IOC_EMIT_GET_AUTH",
	"IOC_EMIT_SET_IDENT",
	"IOC_EMIT_GET_IDENT",
	"IOC_SET_DEBUG",
	"IOC_SET_SYSLOG_PERIOD",
	"IOC_LIST_HOST",
	"IOC_GET_HOST",
	"IOC_DEL_HOST"
};

/*
 * ioctl data structures definition
 */

/*
 * HBX_IOC_LIST_DEV ---------------------------------------------------------
 */
typedef struct hbx_ioc_list_dev_t {
	/*
	 * IN parameters
	 */
	uint32_t	dev_nb;	/* max dev in buffer area; can be NULL */
	/*
	 * OUT parameters
	 */
	uint32_t	real_dev_nb;	/* real number of devices id */
	hbx_dev_t	*dev;		/* buffer aera: list of devices id */
#if defined(Solaris) && !defined(_LP64)
	uint32_t pad;
#endif
} hbx_ioc_list_dev_t;

#if defined(Solaris) && defined(_KERNEL) && defined(_SYSCALL32)
/* The hbx_ioc_list_dev_s structure in a 32bit datamodel of a 64bit kernel */
typedef struct hbx_ioc_list_dev_32_s {
	/*
	 * IN parameters
	 */
	uint32_t	dev_nb;	/* max dev number in buffer area, can be NULL */
	/*
	 * OUT parameters
	 */
	uint32_t	real_dev_nb;	/* real number of devices id */
	uint32_t devp;			/* buffer area: list of devices id */
	uint32_t _pad;
} hbx_ioc_list_dev_32_t;
#endif

/*
 * HBX_IOC_GET_DEV ----------------------------------------------------------
 */

enum hbx_emit_state {
	HBX_EMIT_STATE_NONE = 0x0,
	HBX_EMIT_STATE_READY,
	HBX_EMIT_STATE_RUN
};

static char *hbx_emit_status[] = {
	"EMIT_NONE",
	"EMIT_READY",
	"EMIT_RUN"
};

enum hbx_mode_t {
	HBX_MODE_OFF = 0x0,
	HBX_RCV_MODE_ON,
	HBX_EMT_MODE_ON
};

static char *hbx_mode[] = {
	"MODE_OFF",
	"RCV_MODE_ON",
	"EMT_MODE_ON"
};

typedef struct hbx_ioc_get_dev_t {
	/*
	 * IN parameters
	 */
	hbx_dev_t	dev;	/* device id */
	/*
	 * OUT parameters
	 */
	uint32_t		id;		/* User id. cookie */
	uint32_t		host_nb;	/* current # of hosts */
	uint32_t		host_coll_nb;	/* current # of collisions */
	uint32_t		max_hosts;	/* max # of hosts for dev */
#ifdef Solaris
	uint32_t		mac_info;	/* dev MAC information avail. */
	uint32_t		mac_version;	/* dev MAC DLPI version */
	uint32_t		mac_ok;		/* dev MAC supported type ? */
	uint32_t		mac_type;	/* dev MAC type */
	uint32_t		mac_len;	/* dev MAC address lenght */
	uint32_t		mac_sap_len;	/* dev MAC sap address lenght */
	uint32_t		mac_sap_off;	/* dev MAC sap offset */
	hbx_mac_addr_t		mac_sap;	/* dev MAC sap address */
#endif
	hbx_mac_addr_t		mac_src;	/* heartbeat MAC src address */
	hbx_mac_addr_t		mac_dst;	/* heartbeat MAC dst address */
	hbx_mac_addr_t		mac_bca;	/* MAC bca or mca address */
	ipaddr_t		src;		/* heartbeat src IP address */
	ipaddr_t		dst;		/* heartbeat dst IP address */
	enum hbx_emit_state	state;		/* NONE, READY, RUN */
	enum hbx_mode_t		mode;		/* emit and/or rcv mode */
	hbx_in_t		in;		/* incarnation number */
} hbx_ioc_get_dev_t;

/*
 * HBX_IOC_SET_ID -----------------------------------------------------------
 */
typedef struct hbx_ioc_set_id_t {
	/*
	 * IN parameters
	 */
	hbx_dev_t	dev; /* device id */
	uint32_t	id;  /* user identification cookie */
#ifndef	_LP64
	uint32_t	_pad;
#endif
} hbx_ioc_set_id_t;

/*
 * HBX_IOC_EMIT_GET_FREQ - HBX_IOC_EMIT_SET_FREQ (emit mode only) ---------
 */
typedef struct hbx_ioc_freq_t {
	/*
	 * IN or OUT parameters
	 */
	hbx_delay_t delay; /* heartbeat detection delay, in milliseconds */
	hbx_numhb_t numhb; /* number of harbeats sent per detection delay */
	/*
	 * OUT parameters
	 */
	uint32_t	set;   /* frequency parameter set flag */
#ifndef	_LP64
	uint32_t	_pad;
#endif
} hbx_ioc_freq_t;

/*
 * HBX_IOC_EMIT_CTL ---------------------------------------------------------
 */

enum hbx_ioctl_emit_type {
	HBX_EMIT_CTL_SET_PARAMS = 0x0,
	HBX_EMIT_CTL_START,
	HBX_EMIT_CTL_STOP,
	HBX_EMIT_CTL_RESET
};

static char *hbx_ioctl_emit[] = {
	"EMIT_CTL_SET_PARAMS",
	"EMIT_CTL_START",
	"EMIT_CTL_STOP",
	"EMIT_CTL_RESET"
};

typedef struct hbx_ioc_emit_cmd_t {
	/* IN parameters */
	hbx_dev_t			dev;	/* device id */
	enum hbx_ioctl_emit_type	cmd;	/* command */
	/*
	 * The following IN parameters are relevant
	 * only for the EMIT_CTL_SET_PARAMS cmd
	 */
	ipaddr_t	src;	/* heartbeat IP src address */
	ipaddr_t	dst;	/* heartbeat IP dst address */
	enum hbx_mode_t	mode;	/* set the emit/rcv mode */
	uint16_t	udp_dst_port;	/* set udp msg dst port */
	uint8_t		ttl;	/* ip packaet ttl */
	hbx_in_t	in;	/* set up the in */
} hbx_ioc_emit_cmd_t;

/*
 * HBX_IOC_LIST_HOST (rcv mode only) ----------------------------------------
 */
typedef struct hbx_ioc_list_host_t {
	/*
	 * IN parameters
	 */
	hbx_dev_t	dev;		/* device id */
	uint32_t	host_nb;	/* max. hosts in buffer aera */
	/*
	 * OUT parameters
	 */
	uint32_t	real_host_nb;	/* real number of hosts */
	ipaddr_t	*host;		/* ist IPv4 hosts addr */
#if defined(Solaris) && !defined(_LP64)
	uint32_t	_pad;
#endif
} hbx_ioc_list_host_t;

#if defined(Solaris) && defined(_KERNEL) && defined(_SYSCALL32)
/* The hbx_ioc_list_host_t type in a 32bit datamodel of a 64bit kernel */
typedef struct {
	hbx_dev_t	dev;
	uint32_t	host_nb;
	uint32_t	real_host_nb;
	uint32_t	hostp;
	uint32_t	_pad;
} hbx_ioc_list_host_32_t;
#endif

/*
 * HBX_IOC_GET_HOST (rcv mode only) ----------------------------------------
 */

enum hbx_host_state {
	HOST_STATE_DOWN = 0,
	HOST_STATE_UP,
	HOST_STATE_ERROR
};

static char *hbx_host_status[] = {
	"HOST_STATE_DOWN",
	"HOST_STATE_UP",
	"HOST_STATE_ERROR"
};

typedef struct hbx_ioc_get_host_t {
	/*
	 * IN parameters
	 */
	hbx_dev_t		dev;	/* device id */
	ipaddr_t		host;	/* IPv4 host address */
	/*
	 * OUT parameters
	 */
	enum hbx_host_state	state;		/* host state */
	hbx_delay_t		delay;		/* detection delay, in msecs */
	hbx_node_id_t		node_id;	/* node id */
} hbx_ioc_get_host_t;

/*
 * HBX_IOC_DEL_HOST (rcv mode only) ----------------------------------------
 */
typedef struct hbx_ioc_del_host_t {
	/*
	 * IN parameters
	 */
	hbx_dev_t	dev;	/* device id */
	ipaddr_t	host;	/* IPv4 host address */
} hbx_ioc_del_host_t;

/*
 * HBX_IOC_GET_IDENT - HBX_IOC_SET_IDENT ---------------------------------
 */
typedef struct hbx_ioc_ident_t {
	/*
	 * IN or OUT parameters
	 */
	hbx_node_id_t	node_id;	/* node id */
	hbx_clid_t	clid;		/* cluster id */
	hbx_version_t	version;	/* heartbeat protocol version */
	/*
	 * OUT parameter
	 */
	uint32_t	set;		/* identity parameters set flag */
} hbx_ioc_ident_t;

/*
 * HBX_IOC_GET_AUTH - HBX_IOC_SET_AUTH ------------------------------------
 */

enum hbx_auth_cmd_t {
	HBX_AUTH_INIT_KEY = 0x0,
	HBX_AUTH_CHANGE_KEY,
	HBX_AUTH_COMMIT_KEY
};

static char *hbx_auth_cmd[] = {
	"AUTH_INIT_KEY",
	"AUTH_CHANGE_KEY",
	"AUTH_COMMIT_KEY"
};

typedef struct hbx_ioc_auth_t {
	/*
	 * IN parameters
	 */
	hbx_pkey_t		pkey;		/* committed priv auth. key */
	hbx_pkey_t		alt_pkey;	/* alternate priv auth. key */
	enum hbx_auth_cmd_t	cmd;		/* auth. sub-command */
	/*
	 * OUT parameter
	 */
	uint32_t		set;		/* auth. parameters set flag */
} hbx_ioc_auth_t;

/*
 * hbxdrv events data structure
 */

enum hbx_event_type {
	EVT_DEV_ADDED = 0x0,
	EVT_DEV_REMOVED,
	EVT_HOST_DETECTED,
	EVT_HOST_DELAY,
	EVT_HOST_DOWN,
	EVT_HOST_UP
};

static char *hbx_events[] = {
	"DEV_ADDED",
	"DEV_REMOVED",
	"HOST_DETECTED",	/* rcv mode only */
	"HOST_DELAY",		/* rcv mode only */
	"HOST_DOWN",		/* rcv mode only */
	"HOST_UP"		/* rcv mode only */
};

typedef struct hbx_event_t {
	hbx_dev_t		dev;	/* device id */
	hbx_time_t		time;	/* event sent time from the driver */
	uint32_t		id;	/* user identification cookie */
	enum hbx_event_type	type;	/* event type */
	/*
	 * fields 'src', 'host_state', 'delay' have no meaming
	 * for the following events type:
	 * EVT_DEV_ADDED, EVT_DEV_REMOVED, EVT_DEV_REMOVED
	 */
	ipaddr_t		src;	/* IPv4 host address */
	hbx_node_id_t		node_id;	/* node id */
	hbx_in_t		in;	/* host incarnation number */
	enum hbx_host_state	state;	/* host state */
	hbx_delay_t		delay;	/* detection delay, in milliseconds */
#ifndef	_LP64
	uint32_t		_pad;
#endif
} hbx_event_t;

#ifdef  __cplusplus
}
#endif

#endif	/* _HBXCTL_H */
