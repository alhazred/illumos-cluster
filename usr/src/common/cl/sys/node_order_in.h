/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  node_order_in.h
 *
 */

#ifndef _NODE_ORDER_IN_H
#define	_NODE_ORDER_IN_H

#pragma ident	"@(#)node_order_in.h	1.10	08/05/20 SMI"

inline void
node_order::set(nodeid_t nid)
{
	if (nid > 0 && nid <= NODEID_MAX)
		nodeid = nid;
	else
		nodeid = NODEID_UNKNOWN;
}

inline void
node_order::lower(void)
{
	ASSERT(valid());
	if (--nodeid == 0)
		nodeid = NODEID_UNKNOWN;
}

inline void
node_order::higher(void)
{
	ASSERT(valid());
	if (++nodeid > NODEID_MAX)
		nodeid = NODEID_UNKNOWN;
}

inline void
node_order::highest(void)
{
	nodeid = NODEID_MAX;
}

inline void
node_order::lowest(void)
{
	nodeid = 1;
}

inline nodeid_t
node_order::current(void)
{
	return (nodeid);
}

inline bool
node_order::valid(void)
{
	return (nodeid != NODEID_UNKNOWN);
}

// To test whether a given nodeid is valid
inline bool
node_order::valid(nodeid_t nid)
{
	set(nid);
	return (valid());
}

#endif	/* _NODE_ORDER_IN_H */
