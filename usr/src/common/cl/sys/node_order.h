/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _NODE_ORDER_H
#define	_NODE_ORDER_H

#pragma ident	"@(#)node_order.h	1.12	08/05/20 SMI"

#include <sys/clconf_int.h>
#include <orb/invo/common.h>

// The node_order class provides an encapsulated way of going through nodeids
// in a for loop, and also a compare function.

class node_order {
public:
	// Can be constructed with or without a nodeid argument
	node_order();
	node_order(nodeid_t);

	// Set the internal value to the argument if it is a valid nodeid.
	// If not, it will be set to NODEID_UNKNOWN
	void		set(nodeid_t);
	// Return the current value
	nodeid_t	current();

	// The next functions impose an ordering between valid nodeids.  It
	// should not be assumed that the ordering in the node_order
	// implementation is standard integer ordering.

	// lowest sets the internal value to the lowest of the valid nodeids,
	// according to the internal ordering
	void		lowest();

	// highest sets the internal value to the highest of the valid nodeids,
	// according to the internal ordering
	void		highest();

	// lower changes the internal value to the next lowest nodeid according
	// to the internal ordering, or NODEID_UNKNOWN if the current value
	// is the lowest.
	void		lower();

	// higher changes the internal value to the next higher nodeid according
	// to the internal ordering, or NODEID_UNKNOWN if the current value
	// is the highest.
	void		higher();

	// The static function returns -1 if the first value is higher, 1 if
	// the second is higher, and 0 if they are equal
	static int	cmp(nodeid_t, nodeid_t);

	// These functions return -1 if the supplied value is lower than the
	// current value, 1 if it is higher, and 0 if it is equal.  The
	// caller must ensure both the supplied and the internal value are
	// valid, as the functions will ASSERT if they aren't.
	int		cmp(nodeid_t);
	int		cmp(node_order &);

	// Returns true if the current or supplied value is not NODEID_UNKNOWN,
	// false otherwise
	bool		valid();
	bool		valid(nodeid_t);
private:
	nodeid_t	nodeid;

	node_order(const node_order &);
	node_order &operator = (node_order &);
};

#include <sys/node_order_in.h>

#endif	/* _NODE_ORDER_H */
