/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  hashtab_def_in.h
 *
 */

#ifndef _HASHTAB_DEF_IN_H
#define	_HASHTAB_DEF_IN_H

#pragma ident	"@(#)hashtab_def_in.h	1.10	06/05/24 SMI"

// Hash Table Template definition
//
// This header file defines a hash table template.
// This code is derived from the Mach Packet Filter (MPF)
// implemented in C and distributed as part of the Mach 3.0
// distribution.
// It has been completely restructured in an object oriented manner
// and reimplemented in C++.
//

/*
 * Mach Operating System
 * Copyright (c) 1993 Carnegie Mellon University
 * All Rights Reserved.
 *
 * Permission to use, copy, modify and distribute this software and its
 * documentation is hereby granted, provided that both the copyright
 * notice and this permission notice appear in all copies of the
 * software, derivative works or modified versions, and any portions
 * thereof, and that both notices appear in supporting documentation.
 *
 * CARNEGIE MELLON ALLOWS FREE USE OF THIS SOFTWARE IN ITS "AS IS"
 * CONDITION.  CARNEGIE MELLON DISCLAIMS ANY LIABILITY OF ANY KIND FOR
 * ANY DAMAGES WHATSOEVER RESULTING FROM THE USE OF THIS SOFTWARE.
 *
 * Carnegie Mellon requests users of this software to return to
 *
 *  Software Distribution Coordinator  or  Software.Distribution@CS.CMU.EDU
 *  School of Computer Science
 *  Carnegie Mellon University
 *  Pittsburgh PA 15213-3890
 *
 * any improvements or extensions that they make and grant Carnegie Mellon
 * the rights to redistribute these changes.
 */
/*
 * Berkeley Packet Filter Definitions from Berkeley
 */

/*
 * Copyright (c) 1990-1991 The Regents of the University of California.
 * All rights reserved.
 *
 * This code is derived from the Stanford/CMU enet packet filter,
 * (net/enet.c) distributed as part of 4.3BSD, and code contributed
 * to Berkeley by Steven McCanne and Van Jacobson both of Lawrence
 * Berkeley Laboratory.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *      This product includes software developed by the University of
 *      California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *      @(#)bpf.h       7.1 (Berkeley) 5/7/91
 *
 * @(#) $Header: bpf.h,v 2.2 93/08/10 15:11:16 mrt Exp $ (LBL)
 */


#ifdef DEBUG
#include <orb/infrastructure/orb.h>	// for ASSERT(ORB::is_shutdown())
#endif


//
// class _ListHashTable
//

inline
_ListHashTable::~_ListHashTable()
{
	ASSERT((refcount() == 0) || ORB::is_shutdown());
	delete [] table;
}

inline uint_t
_ListHashTable::refcount()
{
	return (ref_count);
}

//
// class _ListHashTable::hash_entry_t
//

inline
_ListHashTable::hash_entry_t::hash_entry_t(
	void *t_ptr, uint_t nkeys, uint_t ekeys[]) :
		_entry_ptr(t_ptr), _DList::ListElem(this)
{
	ASSERT(nkeys <= HASH_KEY_COUNT);
	uint_t index;

	for (index = 0; index < nkeys; index++) {
		keys[index] = ekeys[index];
	}

	for (; index < HASH_KEY_COUNT; index++) {
		keys[index] = 0;
	}
}

//
// class _ListHashTable::Iterator
//

inline
_ListHashTable::Iterator::Iterator(_ListHashTable &h) :
	_index(0), _current(h.table[0].first()),
	_hashtab_size(h.hashtab_size), table(h.table)
{
	if (_current == NULL)
		advance();
}

inline
_ListHashTable::Iterator::Iterator(_ListHashTable *h) :
	_index(0), _current(h->table[0].first()),
	_hashtab_size(h->hashtab_size), table(h->table)
{
	if (_current == NULL)
		advance();
}

inline void *
_ListHashTable::Iterator::get_current()
{
	return (_current == NULL ? NULL :
			((hash_entry_t *)_current)->_entry_ptr);
}


//
// template hash_table_t
//

template <class H, uint_t nkeys>
inline
hash_table_t<H, nkeys>::hash_table_t(uint_t num_buckets) :
		_ListHashTable(num_buckets)
{
}

template <class H, uint_t nkeys>
inline H *
hash_table_t<H, nkeys>::add(H *entry, uint_t keys[], os::mem_alloc_type flag)
{
	return ((H *) _ListHashTable::_add((void *) entry, nkeys, keys, flag));
}

template <class H, uint_t nkeys>
inline H *
hash_table_t<H, nkeys>::remove(uint_t keys[])
{
	return ((H *) _ListHashTable::_remove(nkeys, keys));
}

template <class H, uint_t nkeys>
inline H *
hash_table_t<H, nkeys>::lookup(uint_t keys[])
{
	return ((H *) _ListHashTable::_lookup(nkeys, keys));
}

template <class H, uint_t nkeys>
inline void
hash_table_t<H, nkeys>::iterate(
		bool (*callbackf)(H *, void *),
		void *callback_data)
{
	hash_iterate((hash_callback_t)callbackf, callback_data);
}

template <class H, uint_t nkeys>
inline void
hash_table_t<H, nkeys>::dispose(bool delete_entries)
{
	hash_iterate_remove(_dispose_cb, (void *) delete_entries);
}

// static
template <class H, uint_t nkeys>
inline bool
hash_table_t<H, nkeys>::_dispose_cb(void *entry, void *delete_entries)
{
	if (delete_entries) {
		delete ((H *) entry);
	}
	return (true);
}

#endif	/* _HASHTAB_DEF_IN_H */
