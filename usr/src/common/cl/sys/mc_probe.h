/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _MC_PROBE_H
#define	_MC_PROBE_H

#pragma ident	"@(#)mc_probe.h	1.13	08/05/20 SMI"

/*
 * MC_PROBE - provides a single place to control tnf probes within
 *	the solaris cluster software.
 *
 *	This file provides a macro to wrap the probes and thus avoid
 *	the clutter of "ifdef"s.
 *
 *	This file is included by both "c" and "c++" files.
 */

#ifdef	PROBES

/* Disable lint warning caused by tnf probes */
/* CSTYLED */
/*lint -esym(754, tnf_v_buf_?::probe_event) */
/* CSTYLED */
/*lint -esym(754, tnf_v_buf_?::time_delta) */
/* CSTYLED */
/*lint -esym(770, tnf_v_buf_?) */
/* CSTYLED */
/*lint -esym(631, tnf_v_buf_?) */
/* CSTYLED */
/*lint -esym(123, tnf_??*) */

#ifdef _KERNEL
#include <sys/tnf_probe.h>
#else
#include <tnf/probe.h>
#endif

#define	MC_PROBE_0(a, b, c) \
	TNF_PROBE_0(a, b, c)
#define	MC_PROBE_1(a, b, c, d, e, f) \
	TNF_PROBE_1(a, b, c, d, e, f)
#define	MC_PROBE_2(a, b, c, d, e, f, g, h, i) \
	TNF_PROBE_2(a, b, c, d, e, f, g, h, i)
#define	MC_PROBE_3(a, b, c, d, e, f, g, h, i, j, k, l) \
	TNF_PROBE_3(a, b, c, d, e, f, g, h, i, j, k, l)
#define	MC_PROBE_4(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o) \
	TNF_PROBE_4(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o)
#define	MC_PROBE_5(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r) \
	TNF_PROBE_5(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r)

#else	/* PROBES not defined */

#define	MC_PROBE_0(a, b, c)
#define	MC_PROBE_1(a, b, c, d, e, f)
#define	MC_PROBE_2(a, b, c, d, e, f, g, h, i)
#define	MC_PROBE_3(a, b, c, d, e, f, g, h, i, j, k, l)
#define	MC_PROBE_4(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o)
#define	MC_PROBE_5(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r)

#endif	/* PROBES */
#endif	/* _MC_PROBE_H */
