/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CL_EVENTS_INT_H
#define	_CL_EVENTS_INT_H

#pragma ident	"@(#)cl_events_int.h	1.4	08/05/20 SMI"

#include <stdio.h>
#include <thread.h>
#include <stddef.h>
#include <synch.h>
#include <sys/types.h>
#include <libsysevent.h>
#include <sys/sysevent.h>

#ifdef	__cplusplus
extern "C" {
#endif

sysevent_handle_t *cl_event_open_channel(char *endpoint);
int cl_event_close_channel(sysevent_handle_t *sysevent_hdl);
int cl_event_bind_channel(sysevent_handle_t *se_handle,
	int (*callback)(sysevent_t *ev));
void cl_event_unbind_channel(sysevent_handle_t *se_handle);

#ifdef	__cplusplus
}
#endif

#endif	/* _CL_EVENTS_INT_H */
