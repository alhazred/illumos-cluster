/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
*/

/*	Copyright (c) 1988 AT&T */
/*	  All Rights Reserved   */

#pragma ident   "@(#)strerror.c 1.6     08/05/20 SMI"

#ifdef _KERNEL_ORB

#include <stdio.h>
#include <sys/strerror.h>

/* copied from libc/port/gen/strerror.c */

extern const char _sys_errs[];
extern const int _sys_index[];
extern int _sys_num_err;

char *
strerror(int errnum)
{
	if (errnum < _sys_num_err && errnum >= 0)
		return ((char *)&_sys_errs[_sys_index[errnum]]);

	return (NULL);
}

#endif /* _KERNEL_ORB */
