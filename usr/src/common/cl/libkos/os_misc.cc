/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*	Copyright (c) 1984. 1988 AT&T	*/
/*	  All Rights Reserved	 */

/*
 * The following notice accompanied the original version of this file:
 *
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */


//
// Implementation of the C++ interface declared in os.h
//

#pragma ident	"@(#)os_misc.cc	1.76	08/12/17 SMI"

#include <sys/os.h>
#include <sys/varargs.h>
#include <sys/console.h>
#include <sys/cmn_err.h>
#include <sys/ddi.h>
#include <sys/sunddi.h>
#include <sys/varargs.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <sys/pathname.h>
#include <sys/sol_version.h>

//
// This file has a couple of lint overrides for Warning 40 when using va_start
// This is because our lint does not understand the compiler builtin variable
// __builtin_va_alist.
//

#define	_BUFSIZ 	1024
#define	_CHUNKSIZ	(4 * _BUFSIZ)

static sc_syslog_msg_status_t
sc_syslog_msg_log_no_args(
		sc_syslog_msg_handle_t	handle,
		int		priority,
		sc_event_type_t	event_type,
		const char	*message,
		va_list vp);

os::mutex_t	os::sleep_mutex;
os::condvar_t	os::sleep_cv;

/*
 * Internet address interpretation routine.
 * All the network library routines call this
 * routine to interpret entries in the data bases
 * which are expected to be an address.
 * The value returned is in network order.
 */
in_addr_t
os::inet_addr(const char *cp)
{
	register uint_t val, base;
	register char c;
	uint_t parts[4], *pp = parts;

	if (*cp == '\0')
		return ((uint_t)-1);  // disallow empty string in cp
again:
	/*
	 * Collect number up to ``.''.
	 * Only decimals are allowed.
	 */
	val = 0; base = 10;
	while ((c = *cp) != NULL) {
		if (os::ctype::is_digit(c)) {
			if ((uint_t)(uchar_t)(c - '0') >= base)
			    break;
			val = (val * base) + (uint_t)(uchar_t)(c - '0');
			cp++;
			continue;
		}
		break;
	}
	if (*cp == '.') {
		/*
		 * Internet format:
		 *	a.b.c.d
		 *	a.b.c	(with c treated as 16-bits)
		 *	a.b	(with b treated as 24 bits)
		 */
		if ((pp >= parts + 3) || (val > 0xff)) {
			return ((uint_t)-1);
		}
		*pp++ = val, cp++;
		goto again;
	}
	/*
	 * Check for trailing characters.
	 */
	if (*cp && !os::ctype::is_space(*cp)) {
		return ((uint_t)-1);
	}
	*pp++ = val;
	/*
	 * Concoct the address according to
	 * the number of parts specified.
	 */
	switch (pp - parts) {

	case 1:				/* a -- 32 bits */
		val = parts[0];
		break;

	case 2:				/* a.b -- 8.24 bits */
		if (parts[1] > 0xffffff)
		    return ((uint_t)-1);
		val = (parts[0] << 24) | (parts[1] & 0xffffff);
		break;

	case 3:				/* a.b.c -- 8.8.16 bits */
		if (parts[2] > 0xffff)
		    return ((uint_t)-1);
		val = (parts[0] << 24) | ((parts[1] & 0xff) << 16) |
			(parts[2] & 0xffff);
		break;

	case 4:				/* a.b.c.d -- 8.8.8.8 bits */
		if (parts[3] > 0xff)
		    return ((uint_t)-1);
		val = (parts[0] << 24) | ((parts[1] & 0xff) << 16) |
		    ((parts[2] & 0xff) << 8) | (parts[3] & 0xff);
		break;

	default:
		return ((uint_t)-1);
	}
	val = htonl(val);
	return (val);
}


void
os::printf(const char *fmt, ...)
{
	va_list adx;

	va_start(adx, fmt);		//lint !e40
	::console_vprintf(fmt, adx);
	va_end(adx);
}

int
os::snprintf(char *s, size_t n, const char *fmt, ...)
{
	// kernel version of vsnprintf/printf return size_t whereas user
	// land version returns int. We make both return int

	va_list adx;
	int ret;

	va_start(adx, fmt);		//lint !e40
	ret = (int)::vsnprintf(s, n, fmt, adx);
	va_end(adx);
	return (ret);
}

void
os::prom_printf(const char *fmt, ...)
{
	va_list adx;

	va_start(adx, fmt);		//lint !e40
	::console_vprintf(fmt, adx);
	va_end(adx);
}

void
os::panic(const char *fmt, ...)
{
	va_list adx;

	va_start(adx, fmt);		//lint !e40
	vcmn_err(CE_PANIC, fmt, adx);
	va_end(adx);
}

void
os::warning(const char *fmt, ...)
{
	va_list adx;

	va_start(adx, fmt);		//lint !e40

	printf("==> WARNING: ");
	::vprintf(fmt, adx);
	printf("\n");
	va_end(adx);
}

//
// The implementation of bsearch was copied from the sources of C library.
//
void *
os::bsearch(const void *ky, const void *bs, size_t nel, size_t size,
	int (*compar)(const void *, const void *))
{
	typedef char *POINTER;
	POINTER key = (char *)ky;
	POINTER base = (char *)bs;
	size_t two_size = size + size;
	POINTER last = base + size * (nel - 1); // last element in table

	while (last >= base) {
		POINTER p = base + size * ((size_t)(last - base)/two_size);
		int res = (*compar)(key, p);

		if (res == 0)
			return (p); // key found */
		if (res < 0)
			last = p - size;
		else
			base = p + size;
	}
	return (NULL);	// key not found */
}

void
os::usecsleep(os::usec_t sleeptime)
{
	os::systime			wake_time(sleeptime);
	//
	// We use cv_timedwait() instead of delay() because this must
	// be callable from real time threads and delay() uses
	// timeout() whereas cv_timedwait() uses
	// realtime_timeout(). Callouts scheduled by the timeout()
	// call are delivered by taskq threads in Solaris. The taskq
	// threads do not run in the real time scheduling class, so if
	// a realtime thread calls usecsleep() there is no guarantee
	// that it will be woken up. Since realtime_timeout() is not a
	// public interface, we use cv_timedwait().
	//
	sleep_mutex.lock();
	while (sleep_cv.timedwait(&sleep_mutex, &wake_time) !=
	    os::condvar_t::TIMEDOUT) {
		if (!wake_time.is_future()) {
			break;
		}
	}

	sleep_mutex.unlock();
}

void
os::systime::setreltime(os::usec_t offset)
{
	clock_t	now;
	clock_t	delta;

	(void) drv_getparm(LBOLT, (unsigned long *)&now);
	delta = (offset == 0) ? 0 : drv_usectohz(offset);
	_time = now + delta;
}

bool
os::systime::is_past()
{
	clock_t		now;

	(void) drv_getparm(LBOLT, (unsigned long *)&now);
	return ((_time < now) ? true : false);
}

bool
os::systime::is_future()
{
	clock_t		now;

	(void) drv_getparm(LBOLT, (unsigned long *)&now);
	return ((_time > now) ? true : false);
}

//
// os::tod::utc_to_tod
//
// Converts seconds since epoch (Jan 1, 1970) into year, month, day,
// hours, minutes, seconds. Code taken from usr/src/uts/common/os/timers.c.
// Code has been duplicated to avoid dependencies on the private
// symbol tod_lock that is needed for call to the original routine and is
// unnecessary in our context.
//
static int days_thru_month[64] = {
	0, 0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335, 366, 0, 0,
	0, 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365, 0, 0,
	0, 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365, 0, 0,
	0, 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365, 0, 0,
};

os::tod::todinfo_t
os::tod::utc_to_tod(time_t utc)
{
	long dse, day, month, year;
	os::tod::todinfo_t todi;

	if (utc < 0)			/* should never happen */
		utc = 0;

	dse = utc / 86400;		/* days since epoch */

	todi.tod_sec = (int)(utc % 60);
	todi.tod_min = (int)((utc % 3600) / 60);
	todi.tod_hour = (int)((utc % 86400) / 3600);
	todi.tod_dow = (int)((dse + 4) % 7 + 1); /* epoch was a Thursday */

	year = (int)(dse / 365 + 72); /* first guess - always a bit too large */
	do {
		year--;
		day = dse - 365 * (year - 70) - (int)((uint_t)(year - 69) >> 2);
	} while (day < 0);

	month = ((year & 3) << 4) + 1;
	while (day >= days_thru_month[month + 1])
		month++;

	todi.tod_day = (int)(day - days_thru_month[month] + 1);
	todi.tod_month = (int)(month & 15);
	todi.tod_year = (int)year;

	return (todi);
}

//
// This method is a supporting method for ctime_r below and has been
// copied as is from the userland implementation in
// usr/src/lib/libc/port/gen/ctime.c.
// Used to print numbers (day, hour, minute, second, year) into a date
// string. Each number is assumed to be one or two digits wide and is
// printed as a two digit number. This is a strange implementation -
// it does a modulo 100 operation on the number before printing.
// Fields that desire left padding by 0 rather than a space should
// be passed as value + 100.
//
char *
os::tod::ct_numb(char *cp, int n)
{
	cp++;
	if (n >= 10)
		*cp++ = (n / 10) % 10 + '0';
	else
		*cp++ = ' ';	/* Pad with blanks */
	*cp++ = n % 10 + '0';
	return (cp);
}

//
// This method is a supporting method for ctime_r below and has been
// adapted from the userland implementation of __posix_asctime_r
// in usr/src/lib/libc/port/gen/ctime.c. Used to convert
// the time of day into a date string. Time reported is GMT.
// Allocating space that is large enough to hold the date string is
// caller's responsibility. Returns pointer to the date string. A NULL
// pointer is returned if the buffer is too small.
//
char *
os::tod::asctime_r(const todinfo_t *t, char *cbuf, int buflen)
{
	char *cp;
	const char *ncp;
	const char *Date = "Day Mon 00 00:00:00 GMT 1900";
	const char *Day  = "SunMonTueWedThuFriSat";
	const char *Month = "JanFebMarAprMayJunJulAugSepOctNovDec";

	if (buflen < (int)os::strlen(Date) + 1) {
		return (NULL);
	}

	// Copy the template date string.
	cp = cbuf;
	for (ncp = Date; (*cp = *ncp) != NULL; cp++, ncp++) {
	}

	// Day of week; Sun is 1, Sat is 7
	ncp = Day + (3 * (t->tod_dow - 1));
	cp = cbuf;
	*cp++ = *ncp++;
	*cp++ = *ncp++;
	*cp++ = *ncp++;
	cp++;

	// Month; Jan is 1, Dec is 12
	ncp = Month + (3 * (t->tod_month - 1));
	*cp++ = *ncp++;
	*cp++ = *ncp++;
	*cp++ = *ncp++;

	// Day of month; right aligned, space padded
	cp = ct_numb(cp, t->tod_day);

	// Hour, minute and second; left padding by zeros is ensured by
	// adding 100 to the real value, see ct_numb for details.
	cp = ct_numb(cp, t->tod_hour + 100);
	cp = ct_numb(cp, t->tod_min + 100);
	cp = ct_numb(cp, t->tod_sec + 100);

	// Skip over the time zone substring GMT
	cp += 4;

	// Year; logic is convoluted because ct_numb can not handle
	// more than two digit numbers
	if (t->tod_year < 100) {
		// "19" already in buffer.
		cp += 2;
	} else if (t->tod_year < 8100) {
		cp = ct_numb(cp, (1900 + t->tod_year) / 100);
		cp--;
	} else {
		// Only 4-digit years are supported
		return (NULL);
	}
	(void) ct_numb(cp, t->tod_year + 100);
	return (cbuf);
}

//
// Coverts seconds since Jan 1, 1970 into a date string. The date
// string reports GMT time. Allocating buffer long enough to hold
// the date string is the caller's responsibility. Return value is
// NULL if the buffer is too small.
//
char *
os::tod::ctime_r(const time_t *clock, char *cbuf, int buflen)
{
	os::tod::todinfo_t	todi;

	if (clock == NULL || cbuf == NULL) {
		return (NULL);
	}
	todi = os::tod::utc_to_tod(*clock);
	return (asctime_r(&todi, cbuf, buflen));
}

//
// Derived from: usr/src/lib/libc/port/gen/atoi.c
//
int
os::atoi(const char *p)
{
	int n;
	int c, neg = 0;

	if (!os::ctype::is_digit((int)(c = *p))) {
		while (os::ctype::is_space(c)) {
			c = *++p;
		}
		switch (c) {
		case '-':
			neg++;
			/* FALLTHROUGH */
		case '+':
			c = *++p;
			break;
		default:
			break;
		}
		if (!os::ctype::is_digit(c))
			return (0);
	}
	for (n = '0' - c; os::ctype::is_digit(c = *++p); ) {
		n *= 10; /* two steps to avoid unnecessary overflow */
		n += '0' - c; /* accum neg to avoid surprises at MAX */
	}
	return (neg ? n : -n);
}

// dupb call from the ORB when not in streams framework.
// It waits till able to allocate memory using dupb
// If flag is os::NO_SLEEP, it will return NULL if unable to allocate
mblk_t *
os::dupb(mblk_t *mp, uint_t pri, os::mem_alloc_type flag)
{
	mblk_t *new_mp;

	while ((new_mp = ::dupb(mp)) == NULL) {
		if (flag == os::NO_SLEEP) {
			return (NULL);
		}
		os::envchk::check_nonblocking(
		    os::envchk::NB_INVO | os::envchk::NB_INTR, "dupb_mcorb");
		if (strwaitbuf(sizeof (mblk_t), (int)pri) != 0) {
			// Unable to queue to wait! sleep and retry
			// EINTR - how to abort? callers dont handle failure
			os::usecsleep((os::usec_t)100000);	// 100ms
		}
	}
	return (new_mp);
}

// allocb call from the ORB when not in streams framework.
// It waits till able to allocate memory using allocb_wait
// If flag is os::NO_SLEEP, it will return NULL if unable to allocate
mblk_t *
os::allocb(uint_t size, uint_t pri, os::mem_alloc_type flag)
{
	if (flag == os::NO_SLEEP)
		// LINTED: allocb takes signed argument
		return (::allocb((int)size, pri));
	else {
		os::envchk::check_nonblocking(
		    os::envchk::NB_INVO | os::envchk::NB_INTR, "allocb_mcorb");
		return (allocb_wait((size_t)size, pri, STR_NOSIG, NULL));
	}
}

//
// Convenience function to create a file. The creation mode is EXCL, which
// means that EEXIST is returned if the file already exists.
// Return 0 on success, non-zero on failure.
//
int
os::file_create(char *path)
{
	vnode_t *vp = NULL;
	struct vattr vattr;
	int error;

	vattr.va_type = VREG;
	vattr.va_mode = S_IRWXU; // rwx user (root)
	vattr.va_mask = AT_TYPE | AT_MODE;
	error = vn_create(path, UIO_SYSSPACE, &vattr, EXCL, 0, &vp,
	    CRCREAT, 0, 0);
	if (error != 0) {
		return (error);
	}
	VN_RELE(vp);
	return (0);
}

int
os::file_rename(char *from_path, char *to_path)
{
	int retval;

	retval = vn_rename(from_path, to_path, UIO_SYSSPACE);

	return (retval);
}

int
os::create_dir(char *dirpath)
{
	vnode_t *vp = NULL;
	struct vattr vattr;
	int error;
	mode_t dmode =  S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH;

	vattr.va_type = VDIR;
	vattr.va_mode = dmode;
	vattr.va_mask = AT_TYPE|AT_MODE;
	error = vn_create(dirpath, UIO_SYSSPACE, &vattr, EXCL, 0, &vp, CRMKDIR,
	    0, 0);
	if (error != 0) {
		return (error);
	}
	VN_RELE(vp);
	return (0);
}

int
os::rm_dir(char *dirpath)
{
	int retval;

	retval = vn_remove(dirpath, UIO_SYSSPACE, RMDIRECTORY);

	return (retval);
}

int
os::file_unlink(char *filename)
{
	int retval;

	retval = vn_remove(filename, UIO_SYSSPACE, RMFILE);

	return (retval);
}

int
os::file_link(char *from_path, char *to_path)
{
	int retval;

	retval = vn_remove(to_path, UIO_SYSSPACE, RMFILE);
	if ((retval != 0) && (retval != ENOENT)) {
		return (retval);
	}
	retval = vn_link(from_path, to_path, UIO_SYSSPACE);

	return (retval);
}

//
// file_copy
//
// Copies chunks of data from from_path to to_path.
// Overwrites to_path if one already exists.
//
int
os::file_copy(char *from_path, char *to_path)
{
	struct vnode	*from_vp = (struct vnode *)0;
	struct vnode	*to_vp = (struct vnode *)0;
	struct vattr	fattr;
	char		*tmp_buf;
	ssize_t		rresid, wresid, tmp_buf_size;
	offset_t	ofst;
	bool		chunks_left;
	int		cmode;
	int		retval;

	tmp_buf = (char *)
	    kmem_alloc(_CHUNKSIZ * sizeof (char), KM_SLEEP);
	tmp_buf_size = _CHUNKSIZ * sizeof (char);

	cmode = 0;	// Only relevant with FCREAT.

	if ((retval = vn_open(from_path, UIO_SYSSPACE, FREAD, cmode,
	    &from_vp, (enum create)0, 0)) != 0)
		goto done;

	//
	// Get the attributes of the from_path file for the cmode
	// of the vn_open for the to_path.
	//
	fattr.va_mask = AT_ALL;
#if	SOL_VERSION >= __s11
	if ((retval = VOP_GETATTR(from_vp, &fattr, 0, kcred, NULL)) != 0) {
#else
	if ((retval = VOP_GETATTR(from_vp, &fattr, 0, kcred)) != 0) {
#endif
	    goto done;
	}

	cmode = (int)fattr.va_mode;

	if ((retval = vn_open(to_path, UIO_SYSSPACE,
	    FCREAT | FWRITE | FTRUNC, cmode, &to_vp, CRCREAT, 0)) != 0)
		goto done;

	// Copy chunks of data from from_path to to_path.
	ofst = 0LL;
	chunks_left = true;
	while (chunks_left) {
		if ((retval = vn_rdwr(UIO_READ, from_vp, tmp_buf,
		    tmp_buf_size, ofst, UIO_SYSSPACE, 0,
		    (rlim64_t)RLIM_INFINITY, kcred, &rresid)) != 0)
			goto done;
		//
		// Amount read in = (tmp_buf_size - rresid).
		// If rresid is > zero, there is no more to read.
		//
		if (rresid > 0)
			chunks_left = false;

		if ((retval = vn_rdwr(UIO_WRITE, to_vp, tmp_buf,
		    (tmp_buf_size - rresid), ofst, UIO_SYSSPACE, FSYNC,
		    (rlim64_t)RLIM_INFINITY, kcred, &wresid)) != 0)
			goto done;
		//
		// Amount written out = (tmp_buf_size - rresid - wresid).
		// If wresid is not zero, the tmp_buf was not written
		// out entirely (ex if the FS was full). This reps an
		// error condition.
		//
		if (wresid != 0) {
			retval = EIO;
			goto done;
		}

		// If more chunks to read, then incr ofst by chunksize.
		if (chunks_left)
			ofst = ofst + tmp_buf_size;
	}

done:
	kmem_free((caddr_t)tmp_buf, _CHUNKSIZ * sizeof (char));
	//
	// No need to do a VOP_FSYNC for the to_vp since the
	// sync flag was specified for the vn_rdwr() call.
	//
	if (to_vp)
		vn_rele(to_vp);
	if (from_vp)
		vn_rele(from_vp);

	return (retval);
}

//
// Convenience function to check if the given file exists.
//
bool
os::file_exists(char *path)
{
	struct vnode *vp = NULL;
	int retval = 0;

	retval = vn_open(path, UIO_SYSSPACE, FREAD, 0, &vp, (enum create)0, 0);

	if (vp != NULL) {
		vn_rele(vp);
	}

	return (retval ? false : true);
}

//
// Open the directory 'dname'.
//
bool
os::rdir::open(char *dname)
{
	if (dvp != NULL)
		close();
	int err = lookupname(dname, UIO_SYSSPACE, FOLLOW, NULL, &dvp);
	if (err != 0)
		return (false);

	rawdirp = new char [maxlen];
	nextp = lastp = NULL;
	doff = (offset_t)0;

	return (true);
}

void
os::rdir::close()
{
	if (dvp != NULL) {
		VN_RELE(dvp);
		dvp = NULL;
		delete [] rawdirp;
		rawdirp = NULL;
	}
}

os::dirent_t *
os::rdir::read()
{
	//
	// If buffer is empty, read more data.
	//
	if (nextp >= lastp) {
		int	eof;
		uio_t	tuio;
		iovec_t	iov;

		tuio.uio_iov = &iov;
		tuio.uio_iovcnt = 1;
		tuio.uio_loffset = doff;
		tuio.uio_segflg = UIO_SYSSPACE;
		tuio.uio_llimit = (rlim64_t)RLIM_INFINITY;
		tuio.uio_fmode = FREAD;
		tuio.uio_resid = (ssize_t)maxlen;
		iov.iov_base = rawdirp;
		iov.iov_len = maxlen;

		// Read the directory contents.
#if	SOL_VERSION >= __s10
		VOP_RWLOCK(dvp, 0, NULL);
#else
		VOP_RWLOCK(dvp, 0);
#endif
#if	SOL_VERSION >= __s11
		int error = VOP_READDIR(dvp, &tuio, kcred, &eof, NULL, 0);
#else
		int error = VOP_READDIR(dvp, &tuio, kcred, &eof);
#endif
#if	SOL_VERSION >= __s10
		VOP_RWUNLOCK(dvp, 0, NULL);
#else
		VOP_RWUNLOCK(dvp, 0);
#endif

		if (error != 0) {
			return (NULL);
		}
		size_t cnt = (size_t)maxlen - (size_t)tuio.uio_resid;
		if (cnt < sizeof (struct dirent64)) {
			return (NULL);
		}
		nextp = (struct dirent64 *)rawdirp;
		lastp = (struct dirent64 *)(rawdirp + cnt);
		doff = tuio.uio_loffset;
	}

	struct dirent64 *dp = nextp;
	nextp = (struct dirent64 *)((char *)dp + dp->d_reclen);
	return (dp);
}

os::sc_syslog_msg::sc_syslog_msg(const char	*resource_type_specific_tag,
				const char	*resource_name,
				void	*)
{
	if (sc_syslog_msg_initialize(&msg_handle, resource_type_specific_tag,
				resource_name)
				!= SC_SYSLOG_MSG_STATUS_GOOD) {
		// XXX what to do?
	}
}

static os::mutex_t sc_syslog_msg_mutex;

sc_syslog_msg_status_t
os::sc_syslog_msg::log(int	priority,
		sc_event_type_t	event_type,
		const char	*format,
		...)
{
	va_list	ap;
	sc_syslog_msg_status_t status;

	va_start(ap, (void *)format);		//lint !e40
	status = sc_syslog_msg_log_no_args(msg_handle, priority, event_type,
				format, ap);
	va_end(ap);

	return (status);
}

os::sc_syslog_msg::~sc_syslog_msg()
{
	sc_syslog_msg_done(&msg_handle);
	msg_handle = NULL;		// for lint
}

/*
 * Following struct and static variables are used by
 * sc_syslog_msg implementation only.
 */
typedef struct {
	char resource_type_tag[_BUFSIZ];
	char resource_name[_BUFSIZ];
} msg_handle_struct;


extern "C" sc_syslog_msg_status_t
sc_syslog_msg_initialize(
			sc_syslog_msg_handle_t	*handle,
			const char	*type_specific_tag,
			const char	*name)
{
	msg_handle_struct *ihandle = NULL;

	*handle = NULL;
	if (name == NULL || type_specific_tag == NULL) {
		return (SC_SYSLOG_MSG_STATUS_ERROR);
	}

	if ((os::strlen(type_specific_tag) + os::strlen(name)) >= _BUFSIZ) {
		return (SC_SYSLOG_MSG_STATUS_ERROR);
	}

	ihandle = (msg_handle_struct *)kmem_zalloc(sizeof (msg_handle_struct),
							KM_NOSLEEP);
	if (ihandle == NULL) {
		/*
		 * Log error to syslog and return. NULL will be handled in
		 * sc_syslog_msg_log
		 */
		cmn_err(CE_WARN,
			"Out of memory (memory allocation failed):%s.%s",
			type_specific_tag, name);

		return (SC_SYSLOG_MSG_STATUS_ERROR);
	}

	(void) os::strcpy(ihandle->resource_type_tag, type_specific_tag);
	(void) os::strcpy(ihandle->resource_name, name);

	*handle = (sc_syslog_msg_handle_t)ihandle;

	return (SC_SYSLOG_MSG_STATUS_GOOD);
}

extern "C" sc_syslog_msg_status_t
sc_syslog_msg_log(
		sc_syslog_msg_handle_t	handle,
		int			priority,
		sc_event_type_t		event_type,
		const char		*format,
		...)
{
	va_list ap;
	sc_syslog_msg_status_t status;

	va_start(ap, (void *)format);		//lint !e40
	status = sc_syslog_msg_log_no_args(handle, priority, event_type,
						format, ap);
	va_end(ap);

	return (status);
}

static sc_syslog_msg_status_t
sc_syslog_msg_log_no_args(
		sc_syslog_msg_handle_t,
		int		priority,
		sc_event_type_t,
		const char	*format,
		va_list		ap)
{
	sc_syslog_msg_status_t status = SC_SYSLOG_MSG_STATUS_GOOD;

	// return if NULL or empty format string is passed
	if (!format || format[0] == '\0') {
		return (status);
	}

	/* log message */
	vcmn_err(priority, format, ap);
	return (status);
}

extern "C" void
sc_syslog_msg_done(sc_syslog_msg_handle_t *handle)
{
	msg_handle_struct *ihandle = (msg_handle_struct *)*handle;
	if (ihandle) {
		kmem_free(ihandle, sizeof (msg_handle_struct));
		*handle = NULL;
	}
}

// If inlines are disabled the methods need to be compiled
// into the library so we include the _in.h files while
// #defining inline to be nothing giving non-inline versions
// of the methods.

#ifdef NOINLINES
#define	inline
#include <sys/os_in.h>
#include <sys/kos_in.h>
#undef	inline		// in case code is added below that uses inline
#endif	// NOINLINES
