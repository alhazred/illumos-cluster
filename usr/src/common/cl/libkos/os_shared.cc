/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*	Copyright (c) 1988 AT&T	*/
/*	  All Rights Reserved	*/


//
// Implementation of the C++ interface declared in os.h
//

#pragma ident	"@(#)os_shared.cc	1.35	08/05/20 SMI"

#include <sys/os.h>

#ifdef _KERNEL
#include <sys/varargs.h>
#include <sys/promif.h>
#include <sys/cmn_err.h>
#include <sys/sunddi.h>
#else
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#endif

os::mutex_t c64lock;

size_t
os::strlen(const char *str)
{
	return (::strlen(str));
}

char *
os::strcpy(char *dst, const char *src)
{
	return (::strcpy(dst, src));
}

char *
os::strncpy(char *dst, const char *src, size_t n)
{
	return (::strncpy(dst, src, n));
}

char *
os::strdup(const char *string)
{
	if (string == NULL) {
		return (NULL);
	}
	char *s = new char[::strlen(string)+1];
	if (s == NULL) {
		return (NULL);
	}
	return (::strcpy(s, string));
}

int
os::strcmp(const char *s1, const char *s2)
{
	return (::strcmp(s1, s2));
}

int
os::strncmp(const char *s1, const char *s2, size_t n)
{
	return (::strncmp(s1, s2, n));
}

int
os::strncasecmp(const char *s1, const char *s2, size_t n)
{
	return (::strncasecmp(s1, s2, n));
}

//
// Copied entirely from libc.
//

size_t
os::strspn(const char *string, const char *charset)
{
	const char *p, *q;

	for (q = string; *q != '\0'; ++q) {
		for (p = charset; *p != '\0' && *p != *q; ++p)
			;
		if (*p == '\0')
			break;
	}
	return ((size_t)(q - string));
}

char *
os::strpbrk(const char *string, const char *brkset)
{
	const char *p;

	do {
		for (p = brkset; *p != '\0' && *p != *string; ++p)
			;
		if (*p != '\0')
			return ((char *)string);
	} while (*string++);

	return (NULL);
}

char *
os::strtok_r(char *string, const char *sepset, char **lasts)
{
	char	*q, *r;

	/* first or subsequent call */
	if (string == NULL)
		string = *lasts;

	if (string == NULL)		/* return if no tokens remaining */
		return (NULL);

	q = string + strspn(string, sepset);	/* skip leading separators */

	if (*q == '\0')		/* return if no tokens remaining */
		return (NULL);

	if ((r = strpbrk(q, sepset)) == NULL)	/* move past token */
		*lasts = NULL;	/* indicate this is last token */
	else {
		*r = '\0';
		*lasts = r + 1;
	}
	return (q);
}


char *
os::strstr(const char *as1, const char *as2)
{
	const char *s1, *s2;
	const char *tptr;
	char c;

	s1 = as1;
	s2 = as2;

	if (s2 == NULL || *s2 == '\0')
		return ((char *)s1);
	c = *s2;

	while (*s1)
		if (*s1++ == c) {
			tptr = s1;
			while ((c = *++s2) == *s1++ && c)
				;
			if (c == 0)
				return ((char *)tptr - 1);
			s1 = tptr;
			s2 = as2;
			c = *s2;
		}

	return (NULL);
}

void
os::sprintf(char *sbuf, const char *fmt, ...)
{
	va_list ap;

	// lint complains of builtin variable __builtin_va_alist
	va_start(ap, fmt);			/*lint !e40 */
	(void) vsprintf(sbuf, fmt, ap);
	va_end(ap);
}

#ifdef DEBUG

//
// reset - The condvar_t will no longer use the lock currently
// associated with it and the condvar_t is idle.
// This method resets the condvar_t state to its initial state.
// This method is intended to be equivalent to destroying the old condvar_t
// and creating a new one, without the constructor/destructor cost.
//
void
os::condvar_t::reset()
{
	ASSERT(num_waiting == 0);
	wait_lock = NULL;
}

//
// debug_pre - Called before wait.
//
void
os::condvar_t::debug_pre(os::mutex_t *lockp)
{
	if ((num_waiting == 0) && (wait_lock == NULL)) {
		// This is the first use of this condvar_t
		wait_lock = lockp;
	} else {
		// Only one lock can be used with this object.
		ASSERT(wait_lock == lockp);
	}
	ASSERT(wait_lock->lock_held());
	num_waiting++;
}

//
// debug_post - Called after wait.
// The wait_lock is not changed even if nobody is waiting.
//
void
os::condvar_t::debug_post()
{
	num_waiting--;
}

#endif // DEBUG


// The non INTR_DEBUG versions of these routines are in sys/os_in.h
#ifdef INTR_DEBUG

os::tsd os::envchk::env;

void
os::envchk::set_nonblocking(nbtype type)
{
	env.set_tsd(env.get_tsd() | (uintptr_t)(uint_t)type);
}

void
os::envchk::clear_nonblocking(nbtype type)
{
	env.set_tsd(env.get_tsd() & ~((uintptr_t)(uint_t)type));
}

bool
os::envchk::is_nonblocking(int mask)
{
	return (((env.get_tsd() & (uintptr_t)(uint_t)mask) != 0) ?
		true : false);
}

//
// Panic or print a warning if called from a context where blocking
// is not acceptable (interrupt thread, unreferenced thread, nonblocking
// invocation).  Used for debugging, similar to an ASSERT.
//
void
os::envchk::check_nonblocking(int mask, char *str)
{
	bool flags = is_nonblocking(mask);
#ifdef _KERNEL
	if ((curthread->t_flag & T_INTR_THREAD) && (mask & NB_INTR))
		flags = true;
#endif
	if (flags) {
		os::panic("Thread in non-blocking context performing %s", str);
	}
}

#endif // INTR_DEBUG

// Global private lock for all instances of notify_t
os::mutex_t os::notify_t::notify_lock;

//
// signal wakes up all waiters (using cv.broadcast) and sets the flag to DONE
// Any future waiters are immediately returned success
// All current waiters are woken by the broadcast
// If the class needs to be reused, it has to be explicitly reinitialized
// using reinit().

void
os::notify_t::signal()
{
	notify_lock.lock();
	ASSERT(flag == WAITING);	// Do not allow to signal twice
	flag = DONE;
	cv.broadcast();
	notify_lock.unlock();
}

//
// Wait for the flag to change to DONE (or !WAITING)
// os::notify_t semantics are such that it is only used once and
// once the flag changes to DONE it does not change.
// (see reinit below on how to reuse)
void
os::notify_t::wait()
{
	notify_lock.lock();
	while (flag == WAITING) {
		cv.wait(&notify_lock);
	}
	notify_lock.unlock();
}

// Similar to wait. See comments above
os::notify_t::wait_state
os::notify_t::timedwait(os::systime &to)
{
	notify_lock.lock();
	while (flag == WAITING) {
		if (cv.timedwait(&notify_lock, &to) ==
		    os::condvar_t::TIMEDOUT)
			break;
	}
	notify_lock.unlock();
	return (flag);
}


// use reinit to reinitialize the os::notify_t flag
// We wait for previous flag to be signalled before we reinitialize it.
// i.e., it implicitly includes a wait() semantic
//
// The caller is responsible to ensure that there will no
// no more new calls or calls happening in parallel to wait/timedwait after
// reinit that need to refer to the old version of the flag
//
int
os::notify_t::reinit(os::mem_alloc_type type)
{
	notify_lock.lock();
	// Wait for the signal on the old version
	while (flag == WAITING) {
		// If type is os::NO_SLEEP, return error
		if (type == os::NO_SLEEP) {
			notify_lock.unlock();
			return (0);
		}
		cv.wait(&notify_lock);
	}
	// Reinitialize
	flag = WAITING;
	notify_lock.unlock();
	return (1);
}

// Converts an integer to its string representation.
int
os::itoa(int x, char *buffer, uint_t base)
{
	char local[80];
	static const char digits[] = "0123456789abcdefghijklmnopqrstuvwxyz";

	uint_t n, rem, mod;

	n = (uint_t)sizeof (local) - 1;

	local[n] = 0;

	if ((base > 35) || (base == 0))
		return (0);

	if (x < 0) {
		rem = (uint_t)-x;
	} else {
		rem = (uint_t)x;
	}

	do {
		switch (base) {
		case 2:
			mod = rem & 1;
			rem = rem >> 1;
			break;
		case 10:
			mod = rem % 10;
			rem = rem / 10;
			break;

		case 16:
			mod = rem & 15;
			rem = rem >> 4;
			break;
		default:
			mod = rem % base;
			rem = rem / base;
			break;
		}
		local[--n] = digits[mod];
	} while (rem != 0);

	if (x < 0) {
		local[--n] = '-';
	}

	(void) os::strcpy(buffer, local + n);

	return ((int)(sizeof (local) - n - 1));
}
