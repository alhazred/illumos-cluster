/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)symtrace.cc	1.21	08/05/20 SMI"

#include <sys/types.h>
#include <sys/sysmacros.h>
#include <sys/systm.h>
#include <sys/stack.h>
#include <sys/os.h>
#include <sys/frame.h>

#if defined(i386) || defined(__i386) || defined(__amd64)

//
// The +p flag removes the bare platform define
// which cases problems including kobj.h ...
//
#ifndef i386
#define	i386
#endif

#define	flush_windows()
#define	SAVEDFP 3

#elif	defined(sparc) || defined(__sparc)

#define	SAVEDFP 1

#else

error - machine type not defined

#endif

#include <sys/kobj.h>

//
// symtraceback - prints the stack trace.
// The function begins by flushing all register data to the stack.
// Then the function walks down the stack one frame at a time,
// printing the symbolic name and offset as it goes.
//
extern "C"
void
symtraceback(caddr_t sp)
{
	register ulong_t	tospage;
	register struct frame	*fp;		// Current frame pointer
	register struct frame	*ofp;		// Prior frame pointer

	flush_windows();

	tospage = (ulong_t)btopr((ulong_t)sp);
	fp = (struct frame *)sp;

	while ((btopr((ulong_t)fp) == tospage) ||
		(btopr((ulong_t)fp) == (tospage + 1))) {

		ulong_t	offset;		// Offset into function
		char	*symname;	// Function symbolic name

		symname = kobj_getsymname((ulong_t)fp->fr_savpc, &offset);

		if (symname == NULL) {
			symname = "(unknown)";
			offset = (ulong_t)fp->fr_savpc;
		}

		os::printf("%s + 0x%lx \n", symname, offset);

		ofp = fp;

		fp = (struct frame *)((ulong_t)fp->fr_savfp + STACK_BIAS);

		if (fp == ofp) {
			os::printf("FP loop at %p", fp);
			break;
		}

		if (fp == NULL) {
			break;
		}
	}
}

//
// symtracedump - General system stack backtrace
// This uses "C" interface to match the user level version of this code.
//
extern "C" void
symtracedump(void)
{
	label_t l;

	(void) setjmp(&l);
	symtraceback((caddr_t)l.val[SAVEDFP] + STACK_BIAS);
}
