/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _PRIVIP_MAP_H
#define	_PRIVIP_MAP_H

#pragma ident	"@(#)privip_map.h	1.5	08/05/20 SMI"

#include "privip_data.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Definition of error codes returned by the scprivip api
 */

#define	SCPRIVIP_NOERR		0	/* no error was found */
#define	SCPRIVIP_EUNEXPECTED	1	/* internal or unexpected error */
#define	SCPRIVIP_EINUSE		2	/* hostname already exists */
#define	SCPRIVIP_EEXIST		3	/* zonename already configured */
#define	SCPRIVIP_ENOEXIST	4	/* zonename not configured */
#define	SCPRIVIP_NO_ADDR	5	/* IP address not available */
#define	SCPRIVIP_EAGAIN		6	/* Table modified, retry */

int scprivip_get_address(char *hostname, char *zone, nodeid_t node);
int scprivip_change_hostname(char *hostname, char *zone, nodeid_t node);
char * scprivip_get_hostname(char *zone, nodeid_t node);
int scprivip_hostname_exists(const char *hostname);
int scprivip_free_address(char *zone, nodeid_t node);

int scprivip_modify_ip();

#ifdef __cplusplus
}
#endif

#endif	/* _PRIVIP_MAP_H */
