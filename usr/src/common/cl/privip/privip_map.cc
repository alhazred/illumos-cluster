//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)privip_map.cc	1.6	08/05/20 SMI"

#ifndef _KERNEL
#include <stropts.h>
#include <netdb.h>
#include <netinet/in.h>
#include <inet/tcp.h>
#include <sys/socket.h>
#endif
#include <privip/privip_map_int.h>
#include <privip/privip_map.h>

privip_map_elem::privip_map_elem(in_addr_t addr, char *zname, nodeid_t node) :
	_SList::ListElem(this)
{
	os::sprintf(hostname, "clusternode%s-%s-priv", zonename, nodeid);
	ipaddr = addr;
	(void) os::strcpy(zonename, zname);
	nodeid = node;
	action = privip_map_elem::PRIVIP_UNKNOWN;
}

privip_map_elem::privip_map_elem(char *name,
	in_addr_t addr, char *zname, nodeid_t node) :
	_SList::ListElem(this)
{
	if (name != NULL)
		(void) os::strcpy(hostname, name);
	else
		os::sprintf(hostname, "clusternode%s-%s-priv",
			zonename, nodeid);
	ipaddr = addr;
	(void) os::strcpy(zonename, zname);
	nodeid = node;
	action = privip_map_elem::PRIVIP_UNKNOWN;
}

privip_map_elem::~privip_map_elem()
{
}

in_addr_t
privip_map_elem::get_addr()
{
	return (ipaddr);
}

void
privip_map_elem::set_addr(in_addr_t addr)
{
	ipaddr = addr;
}

char *
privip_map_elem::get_hostname()
{
	return (hostname);
}

void
privip_map_elem::set_hostname(char *name)
{
	if (name != NULL)
		(void) os::strcpy(hostname, name);
}

char *
privip_map_elem::get_zonename()
{
	char *name;
	name = new char [CL_MAX_LEN];
	(void) os::strcpy(name, zonename);
	return (name);
}

void
privip_map_elem::set_zonename(char *name)
{
	if (name != NULL)
		(void) os::strcpy(zonename, name);
}

nodeid_t
privip_map_elem::get_nodeid()
{
	return (nodeid);
}

void
privip_map_elem::set_nodeid(nodeid_t node)
{
	nodeid = node;
}

privip_map_elem::action_state
privip_map_elem::get_action()
{
	return (action);
}

void
privip_map_elem::set_action(privip_map_elem::action_state ac)
{
	action = ac;
}

int
privip_map_elem::same(in_addr_t addr, char *zname, nodeid_t node)
{
	if (ipaddr != addr)
		return (0);
	if (os::strcmp(zonename, zname))
		return (0);
	if (nodeid != node)
		return (0);
	return (1);
}

privip_map::privip_map()
{
	current_version = -1;
	privip_map_changed = 1;
	privip_map_list = NULL;

	max_entries = max_ip_addrs = 0;
	maxnodes = 0;
	ip_host_addrs = NULL;
	pernode_netnum = 0;
	pernode_netmask = 0;
}

privip_map::~privip_map()
{
	privip_map_list = NULL;
	ip_host_addrs = NULL;
}

SList<privip_map_elem>  &
privip_map::get_map_list()
{
	ASSERT(privip_map_list != NULL);
	return (*privip_map_list);
}

int
privip_map::initialize()
{
	uint_t i;
	const char *maxnodes_str;
	struct in_addr in;
	int nodes;

	privip_map_list = new SList<privip_map_elem>;
	if (privip_map_list == NULL)
		return (ENOMEM);

#ifndef _KERNEL
	if (clconf_lib_init() != 0) {
		return (-1);
	}
#endif
	clconf_cluster_t *cl = clconf_cluster_get_current();

	// Get the pernode IP address and the pernode netmask
	(void) clconf_get_user_network(&in);
	pernode_netnum = in._S_un._S_addr;
	(void) clconf_get_user_netmask(&in);
	pernode_netmask = in._S_un._S_addr;

	// Convert to host order for bitwise calculations
	pernode_netnum = ntohl(pernode_netnum);
	pernode_netmask = ntohl(pernode_netmask);

	maxnodes_str = clconf_obj_get_property((clconf_obj_t *)cl,
				"private_maxnodes");
	if (maxnodes_str != NULL) {
		nodes = os::atoi(maxnodes_str);
		if (nodes == -1) {
			maxnodes = (uint_t)NODEID_MAX;
		} else {
			maxnodes = (uint_t)nodes;
		}
	} else {
		maxnodes = (uint_t)NODEID_MAX;
	}

	max_ip_addrs = (~pernode_netmask + 1);
	max_entries = (max_ip_addrs/UINT_SIZE) + 1;

	ip_host_addrs = new uint_t[max_entries];
	if (ip_host_addrs == NULL) {
		delete privip_map_list;
		privip_map_list = NULL;
		return (ENOMEM);
	}

	bzero(ip_host_addrs, sizeof (ip_host_addrs));
	for (i = 0; i < max_entries; i++)
		ip_host_addrs[i] = 0;

	// Set entries for address 0 and all 1s to 1
	(void) set_hostno_in_use(0);
	(void) set_hostno_in_use(max_ip_addrs - 1);
	// Set entries starting for pernode IP addresses to 1.
	for (i = 1; i <= maxnodes; i++)
		(void) set_hostno_in_use(i);

	clconf_obj_release((clconf_obj_t *)cl);

	return (0);
}

void
privip_map::shutdown()
{
	privip_map_elem *p;
	if (privip_map_list != NULL) {
		while ((p = privip_map_list->reapfirst()) != NULL) {
			delete p;
			p = NULL;
		}
		delete privip_map_list;
		privip_map_list = NULL;
	}

	if (ip_host_addrs != NULL) {
		delete [] ip_host_addrs;
		ip_host_addrs = NULL;
	}
}

int
privip_map::privip_map_populate(char *hostname, in_addr_t addr,
		char *zname, nodeid_t node)
{
	privip_map_elem *privip_elem;

	// Obtain the element corresponding to the given IP address
	// from the free address list, set the hostname to the name
	// given as the parameter and then add the element to the
	// used list.
	SList<privip_map_elem>::ListIterator
					iter(privip_map_list);

	while ((privip_elem = iter.get_current()) != NULL) {
		if (privip_elem->same(addr, zname, node)) {
			privip_elem->set_action(privip_map_elem::PRIVIP_EXISTS);
			return (0);
		}
		iter.advance();
	}
	privip_elem = new privip_map_elem(hostname, addr, zname, node);
	privip_elem->set_action(privip_map_elem::PRIVIP_ADD);
	privip_map_list->append(privip_elem);

	return (0);
}

int
privip_map::read_ccr_mappings()
{
	int new_version = -1, retry = 1;
	Environment e;
	ccr::readonly_table_var tabptr;
	ccr::element_seq_var elems;
	int num_elems;
	uint_t i;
	in_addr_t ipaddr;
	char *saddr, *snode;
	char *hostname, *zname;
	nodeid_t node;
	char *dval, *lasts = NULL;
	CORBA::Exception *ex;
	privip_map_elem *privip_elem = NULL;
	SList<privip_map_elem>::ListIterator iter(privip_map_list);

	privip_map_changed = 1;

#ifndef _KERNEL
	// Check if ORB is initialized
	if (!(ORB::is_initialized())) {
		return (-1);
	}
#endif

	ccr::directory_var ccr_ptr = ccr::directory::_nil();

	naming::naming_context_var ctxp = ns::local_nameserver();
	CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
	if (e.exception()) {
		e.clear();
		return (-1);
	}
	else
		ccr_ptr = ccr::directory::_narrow(obj);

	if (CORBA::is_nil(ccr_ptr))  {
		return (-1);
	}

	while (retry) {

		if (reset_bitmask() != 0)
			return (-1);

		/* The table is created by the daemon if not already present */
		tabptr = ccr_ptr->lookup(PRIVIP_CCR_TABLE, e);
		if ((ex = e.exception()) != NULL) {
			return (-1);
		}

		new_version = tabptr->get_gennum(e);
		if ((ex = e.exception()) != NULL) {
			if (ccr::table_modified::_exnarrow(ex)) {
				// The table was modified since we
				// determined the number of elements
				// and version of the table.
				// Restart the process.
				e.clear();
				continue;
			} else {
				e.clear();
				return (-1);
			}
		}

		if ((new_version == current_version) && (new_version != -1)) {
			// We have already populated the used list with
			// the latest version of the table. Return success.
			privip_map_changed = 0;
			return (0);
		}

		// Else, the table has been changed, so read it.
		privip_map_changed = 1;

		// Initialize list elements
		iter.reinit(privip_map_list);
		while ((privip_elem = iter.get_current()) != NULL) {
			privip_elem->set_action(
				privip_map_elem::PRIVIP_UNKNOWN);
			iter.advance();
		}

		// We need to read in the CCR table since this might be
		// the first time we are reading it or since the table
		// has changed since the last time we read it.
		num_elems = tabptr->get_num_elements(e);
		if ((ex = e.exception()) != NULL) {
			if (ccr::table_modified::_exnarrow(ex)) {
				// The table was modified since we
				// determined the number of elements
				// and version of the table.
				// Restart the process.
				e.clear();
				continue;
			} else {
				return (-1);
			}
		}

		if (num_elems > 0) {
			tabptr->atfirst(e);
			if ((ex = e.exception()) != NULL) {
				if (ccr::table_modified::_exnarrow(ex)) {
					// The table was modified since we
					// determined the number of elements
					// and version of the table.
					// Restart the process.
					e.clear();
					continue;
				} else {
					return (-1);
				}
			}

			tabptr->next_n_elements((uint32_t)num_elems, elems, e);
			if ((ex = e.exception()) != NULL) {
				if (ccr::table_modified:: _exnarrow(ex)) {
					// The table was modified since we
					// determined the number of elements
					// and version of the table.
					// Restart the process.
					e.clear();
					continue;
				} else {
					return (-1);
				}
			}

			// Populate the map. Also set up the  bitmask to
			// indicate the addresses being used
			for (i = 0; i < elems->length(); i++) {
				hostname = elems[i].key;
				dval = elems[i].data;
				saddr = os::strtok_r(dval, "|", &lasts);
				ipaddr = os::inet_addr(saddr);
				ipaddr = ntohl(ipaddr);
				snode = os::strtok_r(NULL, "|", &lasts);
				node = (uint_t)os::atoi(snode);
				zname = os::strtok_r(NULL, "|", &lasts);

				(void) privip_map_populate(hostname,
					ipaddr, zname, node);
				(void) set_ipaddr_in_use(ipaddr);
			}
		}
		retry = 0;
	}

	// Update the list so as to mark IP addresses which no longer
	// exist in the CCR for removal. Note any which should not
	// be marked for removal have already been marked as ADD or
	// EXISTS.
	iter.reinit(privip_map_list);
	while ((privip_elem = iter.get_current()) != NULL) {
		if (privip_elem->get_action() ==
			privip_map_elem::PRIVIP_UNKNOWN) {
			privip_elem->set_action(
				privip_map_elem::PRIVIP_REMOVE);
		}
		iter.advance();
	}

	current_version = new_version;

	return (0);
}

int
privip_map::is_changed()
{
	return (privip_map_changed);
}


#ifdef _KERNEL

clconf_errnum_t
privip_get_ccr(uint_t *count, char *outbuf, size_t outbuf_len)
{
	privip_map the_privip_map;
	privip_map_elem *pmep;
	size_t size = 0;
	int i = 0;
	struct nss_privip *nss_table = (struct nss_privip *)outbuf;

	*count = 0;

	if (the_privip_map.initialize() != 0) {
		return (CL_MEMALLOC_FAIL);
	}

	// First read the CCR table
	if (the_privip_map.read_ccr_mappings() != 0) {
		the_privip_map.shutdown();
		return (CL_CCR_ERROR);
	}

	SList<privip_map_elem>::ListIterator
				iter(the_privip_map.get_map_list());
	while ((pmep = iter.get_current()) != NULL) {
		*count = (*count) + 1;
		iter.advance();
	}

	size = (sizeof (struct nss_privip)) * (*count);

	if (size > outbuf_len) {
		the_privip_map.shutdown();
		return (CL_BUF_TOO_SMALL);
	}

	iter.reinit(the_privip_map.get_map_list());
	i = 0;
	while ((pmep = iter.get_current()) != NULL) {
		(void) os::strcpy(nss_table[i].priv_hostname,
			pmep->get_hostname());
		nss_table[i].inaddr = pmep->get_addr();
		i++;
		iter.advance();
	}
	the_privip_map.shutdown();
	return (CL_NOERROR);
}

#else // _KERNEL

int
privip_map::get_ip_address(char *hostname, char *zname, nodeid_t node)
{
	privip_map_elem *pmep;
	in_addr_t addr;
	int retry = 1;
	int err = 0;

	if (initialize() != 0)
		return (SCPRIVIP_EUNEXPECTED);

	while (retry) {
		if (read_ccr_mappings() != 0) {
			shutdown();
			return (SCPRIVIP_EUNEXPECTED);
		}

		// First determine if the hostname is being used for any
		// privip address resource. Also check if the zone already
		// has a private hostname assigned to it. If so, return error.
		// Otherwise, get the first free IP address element available
		// from the bitmask. Assign the hostname to it and then
		// add the element to the list.
		// If there are no free IP addresses, return error.

		SList<privip_map_elem>::ListIterator
						iter(privip_map_list);
		while ((pmep = iter.get_current()) != NULL) {
			if ((!(os::strcmp(pmep->get_zonename(), zname))) &&
				(pmep->get_nodeid() == node)) {
				shutdown();
				return (SCPRIVIP_EEXIST);
			}
			if (!(os::strcmp(pmep->get_hostname(), hostname))) {
				// Host name is already being used.
				shutdown();
				return (SCPRIVIP_EINUSE);
			}
			iter.advance();
		}

		// Determine a free IP address from bitmask.
		if ((addr = get_free_ip_addr()) == 0) {
			shutdown();
			return (SCPRIVIP_NO_ADDR);
		}

		// Add this entry to the used list so that it will
		// get added to the CCR table via the add_mapping
		// method.
		if ((err = add_mapping_to_ccr(hostname, addr, zname, node))
			!= 0) {
			// If this fails, it means that there
			// is something wrong with the CCR
			// mapping table.

			// We may need to re-read it or return
			// error to the user.
			if (err == SCPRIVIP_EAGAIN) {
				continue;
			} else {
				shutdown();
				return (SCPRIVIP_EUNEXPECTED);
			}
		}

		// Else the mapping was successfully added to the CCR
		retry = 0;
	}

	shutdown();
	return (SCPRIVIP_NOERR);
}

int
privip_map::change_privhostname(char *hostname, char *zname, nodeid_t node)
{
	privip_map_elem *pmep;
	int retry = 1;
	int err = 0;
	char oldhostname[CL_MAX_LEN];
	in_addr_t addr = 0;

	if (initialize() != 0)
		return (SCPRIVIP_EUNEXPECTED);

	// First read the CCR table and determine if any entry exists
	// for the given hostnames
	while (retry) {
		if (read_ccr_mappings() != 0) {
			shutdown();
			return (SCPRIVIP_EUNEXPECTED);
		}

		// Check if the new hostname conflicts with an existing
		// hostname.
		SList<privip_map_elem>::ListIterator
						iter(privip_map_list);
		while ((pmep = iter.get_current()) != NULL) {
			if (!(os::strcmp(pmep->get_hostname(), hostname))) {
				// Host name is already being used.
				shutdown();
				return (SCPRIVIP_EINUSE);
			}
			iter.advance();
		}

		// Get the current hostname associated with the specified
		// zone and node.
		iter.reinit(privip_map_list);
		while ((pmep = iter.get_current()) != NULL) {
			if (!(os::strcmp(pmep->get_zonename(), zname)) &&
				(pmep->get_nodeid() == node)) {
				break;
			}
			iter.advance();
		}

		if (pmep == NULL) {
			shutdown();
			return (SCPRIVIP_ENOEXIST);
		}

		(void) os::strcpy(oldhostname, pmep->get_hostname());
		addr = pmep->get_addr();
		node = pmep->get_nodeid();
		(void) os::strcpy(zname, pmep->get_zonename());

		err = change_mapping_in_ccr(oldhostname, hostname,
						addr, zname, node);
		if (err != 0) {
			// An error occurred when accessing the CCR table
			// to change the host name to IP address mapping
			// associated with hostname.

			// We may need to re-read it or return
			// error to the user.
			if (err == SCPRIVIP_EAGAIN) {
				continue;
			} else {
				shutdown();
				return (SCPRIVIP_EUNEXPECTED);
			}
		}
		// Else the mapping was successfully changed in the CCR
		retry = 0;
	}

	shutdown();

	return (SCPRIVIP_NOERR);
}

char *
privip_map::get_privhostname(char *zname, nodeid_t node)
{
	privip_map_elem *pmep;
	char *hostname = NULL;

	if (initialize() != 0)
		return (NULL);

	// First read the CCR table and determine if any entry exists
	// for the given hostnames
	if (read_ccr_mappings() != 0) {
		shutdown();
		return (NULL);
	}

	// Check if the new hostname conflicts with an existing
	// hostname.
	SList<privip_map_elem>::ListIterator
					iter(privip_map_list);
	while ((pmep = iter.get_current()) != NULL) {
		if ((!(os::strcmp(pmep->get_zonename(), zname))) &&
			(pmep->get_nodeid() == node)) {
			hostname = new char [CL_MAX_LEN];
			(void) os::strcpy(hostname,
				pmep->get_hostname());
			break;
		}
		iter.advance();
	}

	if (pmep == NULL) {
		shutdown();
		return (NULL);
	}

	shutdown();
	return (hostname);
}

int
privip_map::privhostname_exists(const char *hostname)
{
	privip_map_elem *pmep;

	if (initialize() != 0)
		return (SCPRIVIP_EUNEXPECTED);

	// First read the CCR table and determine if any entry exists
	// for the given hostnames
	if (read_ccr_mappings() != 0) {
		shutdown();
		return (SCPRIVIP_EUNEXPECTED);
	}

	// Check if the new hostname conflicts with an existing
	// hostname.
	SList<privip_map_elem>::ListIterator
					iter(privip_map_list);
	while ((pmep = iter.get_current()) != NULL) {
		if (os::strcmp(pmep->get_hostname(), hostname) == 0) {
			shutdown();
			return (SCPRIVIP_EINUSE);
		}
		iter.advance();
	}

	shutdown();
	return (SCPRIVIP_NOERR);
}

int
privip_map::free_ip_address(char *zname, nodeid_t node)
{
	privip_map_elem *pmep;
	char name[CL_MAX_LEN];
	int retry = 1;
	int err = 0;

	if (initialize() != 0)
		return (SCPRIVIP_EUNEXPECTED);

	// First read the CCR table and determine if any entry exists
	// for the given hostname
	while (retry) {
		if (read_ccr_mappings() != 0) {
			shutdown();
			return (SCPRIVIP_EUNEXPECTED);
		}

		// First determine if the hostname is being used for any
		// privip address resource.
		SList<privip_map_elem>::ListIterator
						iter(privip_map_list);
		while ((pmep = iter.get_current()) != NULL) {
			if ((!(os::strcmp(pmep->get_zonename(), zname))) &&
				(pmep->get_nodeid() == node)) {
				break;
			}
			iter.advance();
		}

		if (pmep == NULL) {
			shutdown();
			return (SCPRIVIP_ENOEXIST);
		}

		(void) os::strcpy(name, pmep->get_hostname());

		// Remove the hostname to IP address mapping from CCR
		err = remove_mapping_from_ccr(name);
		if (err != 0) {
			// An error occurred when accessing the CCR table
			// to remove the host name to IP address mapping
			// associated with hostname.

			// We may need to re-read it or return
			// error to the user.
			if (err == SCPRIVIP_EAGAIN) {
				continue;
			} else {
				shutdown();
				return (SCPRIVIP_EUNEXPECTED);
			}
		}

		retry = 0;
	}

	shutdown();
	return (SCPRIVIP_NOERR);
}

// The following methods will interact with the CCR table which stores
// the host name to IP address mapping. Refer to rgm_ccrput.cc for
// this functionality.
int
privip_map::add_mapping_to_ccr(char *name, in_addr_t addr, char *zname,
			nodeid_t node)
{
	ccr::readonly_table_var tabptr;
	ccr::updatable_table_var transp = NULL;
	char tablename[MAX_NAME_LEN];
	char keyval[MAX_NAME_LEN], dataval[MAX_NAME_LEN];
	struct in_addr in;
	Environment e;
	CORBA::Exception *ex;
	int new_version = -1;

	// Check if ORB is initialized
	if (!(ORB::is_initialized())) {
		return (-1);
	}

	ccr::directory_var ccr_ptr = ccr::directory::_nil();

	naming::naming_context_var ctxp = ns::local_nameserver();
	CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
	if (e.exception())
		e.clear();
	else
		ccr_ptr = ccr::directory::_narrow(obj);

	if (CORBA::is_nil(ccr_ptr))
		return (-1);

	// We already have the ccr ptr. So we simply start a
	// transaction on the privip_map table.
	os::sprintf(tablename, "%s", PRIVIP_CCR_TABLE);
	transp = ccr_ptr->begin_transaction(tablename, e);
	if (e.exception()) {
		e.clear();
		return (-1);
	}

	tabptr = ccr_ptr->lookup(PRIVIP_CCR_TABLE, e);
	if ((ex = e.exception()) != NULL) {
		e.clear();
		transp->abort_transaction(e);
		if (e.exception())
			e.clear();
		return (-1);
	}

	new_version = tabptr->get_gennum(e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::table_modified::_exnarrow(ex)) {
			e.clear();
			transp->abort_transaction(e);
			if (e.exception())
				e.clear();
			return (SCPRIVIP_EAGAIN);
		} else {
			e.clear();
			transp->abort_transaction(e);
			if (e.exception())
				e.clear();
			return (-1);
		}
	}

	if ((new_version != current_version) && (new_version != -1)) {
		e.clear();
		transp->abort_transaction(e);
		if (e.exception())
			e.clear();
		return (SCPRIVIP_EAGAIN);
	}

	(void) strcpy(keyval, name);
	in = inet_makeaddr(addr, 0);
	os::sprintf(dataval, "%s|%d|%s", inet_ntoa(in), node, zname);

	transp->add_element(keyval, dataval, e);
	if ((ex = e.exception()) != NULL) {
			e.clear();
			transp->abort_transaction(e);
			if (e.exception())
				e.clear();
			return (-1);
	}

	// Commit the changes to the CCR table. This may result in a
	// exception, in which case, we need to re-read the table and
	// then try the write again.
	transp->commit_transaction(e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::table_modified::_exnarrow(ex)) {
			e.clear();
			transp->abort_transaction(e);
			if (e.exception())
				e.clear();
			return (SCPRIVIP_EAGAIN);
		} else {
			e.clear();
			transp->abort_transaction(e);
			if (e.exception())
				e.clear();
			return (-1);
		}
	}

	return (0);
}

int
privip_map::change_mapping_in_ccr(char *oldname, char *newname,
	in_addr_t addr, char *zname, nodeid_t node)
{
	ccr::readonly_table_var tabptr;
	ccr::updatable_table_var transp = NULL;
	char tablename[MAX_NAME_LEN];
	char keyval[MAX_NAME_LEN], dataval[MAX_NAME_LEN];
	struct in_addr in;
	Environment e;
	CORBA::Exception *ex;
	int new_version = -1;

	// Check if ORB is initialized
	if (!(ORB::is_initialized())) {
		return (-1);
	}

	ccr::directory_var ccr_ptr = ccr::directory::_nil();

	naming::naming_context_var ctxp = ns::local_nameserver();
	CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
	if (e.exception())
		e.clear();
	else
		ccr_ptr = ccr::directory::_narrow(obj);

	if (CORBA::is_nil(ccr_ptr))
		return (-1);

	// We already have the ccr ptr. So we simply start a
	// transaction on the privip_map table.
	os::sprintf(tablename, "%s", PRIVIP_CCR_TABLE);
	transp = ccr_ptr->begin_transaction(tablename, e);
	if (e.exception()) {
		e.clear();
		return (-1);
	}

	tabptr = ccr_ptr->lookup(PRIVIP_CCR_TABLE, e);
	if ((ex = e.exception()) != NULL) {
		e.clear();
		transp->abort_transaction(e);
		if (e.exception())
			e.clear();
		return (-1);
	}

	new_version = tabptr->get_gennum(e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::table_modified::_exnarrow(ex)) {
			e.clear();
			transp->abort_transaction(e);
			if (e.exception())
				e.clear();
			return (SCPRIVIP_EAGAIN);
		} else {
			e.clear();
			transp->abort_transaction(e);
			if (e.exception())
				e.clear();
			return (-1);
		}
	}

	if ((new_version != current_version) && (new_version != -1)) {
		e.clear();
		transp->abort_transaction(e);
		if (e.exception())
			e.clear();
		return (SCPRIVIP_EAGAIN);
	}

	// Remove the old hostname entry.
	transp->remove_element(oldname, e);
	if (e.exception()) {
		e.clear();
		transp->abort_transaction(e);
		if (e.exception())
			e.clear();
		return (-1);
	}

	(void) strcpy(keyval, newname);
	in = inet_makeaddr(addr, 0);
	os::sprintf(dataval, "%s|%d|%s", inet_ntoa(in), node, zname);
	transp->add_element(keyval, dataval, e);
	if ((ex = e.exception()) != NULL) {
			e.clear();
			transp->abort_transaction(e);
			if (e.exception())
				e.clear();
			return (-1);
	}

	// Commit the changes to the CCR table. This may result in a
	// exception, in which case, we need to re-read the table and
	// then try the write again.
	transp->commit_transaction(e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::table_modified::_exnarrow(ex)) {
			e.clear();
			transp->abort_transaction(e);
			if (e.exception())
				e.clear();
			return (SCPRIVIP_EAGAIN);
		} else {
			e.clear();
			transp->abort_transaction(e);
			if (e.exception())
				e.clear();
			return (-1);
		}
	}

	return (0);
}

int
privip_map::remove_mapping_from_ccr(char *name)
{
	ccr::readonly_table_var tabptr;
	ccr::updatable_table_var transp = NULL;
	char tablename[MAX_NAME_LEN];
	Environment e;
	CORBA::Exception *ex;
	int new_version = -1;

	// Check if ORB is initialized
	if (!(ORB::is_initialized())) {
		return (-1);
	}

	ccr::directory_var ccr_ptr = ccr::directory::_nil();

	naming::naming_context_var ctxp = ns::local_nameserver();
	CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
	if (e.exception())
		e.clear();
	else
		ccr_ptr = ccr::directory::_narrow(obj);

	if (CORBA::is_nil(ccr_ptr))
		return (-1);

	os::sprintf(tablename, "%s", PRIVIP_CCR_TABLE);
	transp = ccr_ptr->begin_transaction(tablename, e);
	if (e.exception()) {
		e.clear();
		return (-1);
	}

	tabptr = ccr_ptr->lookup(PRIVIP_CCR_TABLE, e);
	if ((ex = e.exception()) != NULL) {
		e.clear();
		transp->abort_transaction(e);
		if (e.exception())
			e.clear();
		return (-1);
	}

	new_version = tabptr->get_gennum(e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::table_modified::_exnarrow(ex)) {
			e.clear();
			transp->abort_transaction(e);
			if (e.exception())
				e.clear();
			return (SCPRIVIP_EAGAIN);
		} else {
			e.clear();
			transp->abort_transaction(e);
			if (e.exception())
				e.clear();
			return (-1);
		}
	}

	if ((new_version != current_version) && (new_version != -1)) {
		e.clear();
		transp->abort_transaction(e);
		if (e.exception())
			e.clear();
		return (SCPRIVIP_EAGAIN);
	}

	// Remove the entry identified by the hostname given as input
	// to this method.
	transp->remove_element(name, e);
	if (e.exception()) {
		e.clear();
		transp->abort_transaction(e);
		if (e.exception())
			e.clear();
		return (-1);
	}

	transp->commit_transaction(e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::table_modified::_exnarrow(ex)) {
			e.clear();
			transp->abort_transaction(e);
			if (e.exception())
				e.clear();
			return (SCPRIVIP_EAGAIN);
		} else {
			e.clear();
			transp->abort_transaction(e);
			if (e.exception())
				e.clear();
			return (-1);
		}
	}

	return (0);
}

#endif

int
privip_map::reset_bitmask()
{
	uint_t i;

	bzero(ip_host_addrs, sizeof (ip_host_addrs));
	for (i = 0; i < max_entries; i++)
		ip_host_addrs[i] = 0;

	// Set entries for address 0 and all 1s to 1
	(void) set_hostno_in_use(0);
	(void) set_hostno_in_use(max_ip_addrs - 1);
	// Set entries starting for pernode IP addresses to 1.
	for (i = 1; i <= maxnodes; i++)
		(void) set_hostno_in_use(i);

	return (0);
}

int
privip_map::is_ip_valid(in_addr_t addr)
{
	if ((addr & pernode_netmask) == pernode_netnum)
		return (1);
	return (0);
}

int
privip_map::set_hostno_in_use(uint_t host_no)
{
	uint_t bitmask, words, bits;

	bitmask = 1;
	words = host_no / UINT_SIZE;
	bits = host_no % UINT_SIZE;

	if (ip_host_addrs[words] & (bitmask << bits))
		return (-1);
	ip_host_addrs[words] |= (bitmask << bits);
	return (0);
}

int
privip_map::set_ipaddr_in_use(in_addr_t ipaddr)
{
	return (set_hostno_in_use(ipaddr & ~pernode_netmask));
}

in_addr_t
privip_map::get_free_ip_addr()
{
	uint_t bitmask = 1;
	uint_t i, j;
	int found = 0;
	uint_t hostno = 0;
	in_addr_t addr;

	for (i = 0; !found && i < max_entries; i++) {
		for (j = 0; j < UINT_SIZE; j++) {
			if (!(ip_host_addrs[i] & (bitmask << j))) {
				found = 1;
				break;
			}
			hostno++;
		}
	}

	if (found) {
		addr = pernode_netnum | hostno;
		return (addr);
	} else
		return (0);
}

#ifndef _KERNEL
extern "C"
{

/*
 * The following functions are used by the scconf libraries in order
 * to assign a private IP address for local zones, remove the private
 * IP address assigned to a local zone, and change the host name associated
 * with the private IP address for a local zone.
 */
int
scprivip_get_address(char *hostname, char *zname, nodeid_t node)
{
	privip_map the_privip_map;
	return (the_privip_map.get_ip_address(hostname, zname, node));
}

int
scprivip_change_hostname(char *hostname, char *zname, nodeid_t node)
{
	privip_map the_privip_map;
	return (the_privip_map.change_privhostname(hostname, zname, node));
}

int
scprivip_free_address(char *zname, nodeid_t node)
{
	privip_map the_privip_map;
	return (the_privip_map.free_ip_address(zname, node));
}

char *
scprivip_get_hostname(char *zname, nodeid_t node)
{
	privip_map the_privip_map;
	return (the_privip_map.get_privhostname(zname, node));
}

int
scprivip_hostname_exists(const char *hostname)
{
	privip_map the_privip_map;
	return (the_privip_map.privhostname_exists(hostname));
}

/*
 * The following function is called to update the IP addresses assigned
 * to local zones in case the cluster pernode IP address range has been
 * updated by using the Flexible IP addressing cli.
 */
int
scprivip_modify_ip()
{
	ccr::readonly_table_var tabptr;
	ccr::updatable_table_var transp = NULL;
	Environment e;
	CORBA::Exception *ex;
	uint_t num_elems = 0;
	uint_t i;
	ccr::element_seq_var elems;
	in_addr_t ipaddr;
	struct in_addr in;
	char *saddr, *snode, *zname;
	char *dval, *lasts = NULL;
	privip_map the_privip_map;
	int ret, retry = 1;
	int ip_addr_modified = 0;

	if (the_privip_map.initialize() != 0)
		return (NULL);

	// First read the CCR table. We are not using the read_ccr_mappings
	// method here since it updates the_privip_map and the bitmask. We
	// simply need to read the table contents and update them if
	// necessary.

	ccr::directory_var ccr_ptr = ccr::directory::_nil();

	naming::naming_context_var ctxp = ns::local_nameserver();
	CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
	if (e.exception()) {
		the_privip_map.shutdown();
		e.clear();
		return (-1);
	}
	else
		ccr_ptr = ccr::directory::_narrow(obj);

	if (CORBA::is_nil(ccr_ptr))  {
		the_privip_map.shutdown();
		return (-1);
	}

	while (retry) {

		tabptr = ccr_ptr->lookup(PRIVIP_CCR_TABLE, e);
		if ((ex = e.exception()) != NULL) {
			if (ccr::no_such_table::_exnarrow(ex)) {
				// The table does not exist. So need
				// to update any mappings.
				e.clear();
				the_privip_map.shutdown();
				return (0);
			} else {
				the_privip_map.shutdown();
				return (-1);
			}
		}

		num_elems = (uint_t)tabptr->get_num_elements(e);
		if ((ex = e.exception()) != NULL) {
			if (ccr::table_modified::_exnarrow(ex)) {
				e.clear();
				continue;
			} else {
				the_privip_map.shutdown();
				return (-1);
			}
		}

		if (num_elems > 0) {
			tabptr->atfirst(e);
			if ((ex = e.exception()) != NULL) {
				if (ccr::table_modified::_exnarrow(ex)) {
					e.clear();
					continue;
				} else {
					the_privip_map.shutdown();
					return (-1);
				}
			}

			tabptr->next_n_elements(num_elems, elems, e);
			if ((ex = e.exception()) != NULL) {
				if (ccr::table_modified:: _exnarrow(ex)) {
					e.clear();
					continue;
				} else {
					the_privip_map.shutdown();
					return (-1);
				}
			}
			num_elems = elems->length();
		}
		retry = 0;
	}

	// For each element in the CCR table, obtain the IP address and
	// check if it is within the cluster pernode IP address range. It
	// will be within the range as long as the cluster IP address
	// range has not been changed via the non-cluster mode Flexible
	// IP addressing cli. If the non-cluster mode flex-ip cli has
	// changed the IP address range, there will be a mismatch and we
	// need to update the private IP addresses assigned to the zones
	// to match the new IP address range.
	for (i = 0; i < num_elems; i++) {
		dval = elems[i].data;
		saddr = os::strtok_r(dval, "|", &lasts);
		ipaddr = os::inet_addr(saddr);
		ipaddr = ntohl(ipaddr);
		snode = os::strtok_r(NULL, "|", &lasts);
		zname = os::strtok_r(NULL, "|", &lasts);

		// Check IP address against the pernode subnet. If the
		// IP address lies within the pernode subnet, go to check
		// the next IP address. If the IP address does not lie
		// within the pernode subnet, update the ipaddr by getting
		// a valid IP address with get_free_ip_addr and write the
		// new IP address back to elems and mark as changed.
		ret = the_privip_map.is_ip_valid(ipaddr);

		if (ret == 0) {
			// The IP address needs to change.
			ipaddr = the_privip_map.get_free_ip_addr();
			if (ipaddr == 0) {
				// If no IP addresses are available,
				// just keep the old one.
				ipaddr = os::inet_addr(saddr);
				ipaddr = ntohl(ipaddr);
			} else {
				// Mark the new IP address as used
				(void) the_privip_map.set_ipaddr_in_use(
					ipaddr);
				ip_addr_modified = 1;
			}

			in = inet_makeaddr(ipaddr, 0);
			os::sprintf(elems[i].data, "%s|%s|%s",
			    inet_ntoa(in), snode, zname);

		}
	}

	// Since at least one IP address has been modified, we need to
	// rewrite the elements to the CCR table.
	if (ip_addr_modified) {
		transp = ccr_ptr->begin_transaction(PRIVIP_CCR_TABLE, e);
		if (e.exception()) {
			e.clear();
			the_privip_map.shutdown();
			return (-1);
		}

		// Remove all elements from privip_ccr
		transp->remove_all_elements(e);
		if ((ex = e.exception()) != NULL) {
			e.clear();
			transp->abort_transaction(e);
			if (e.exception())
				e.clear();
			the_privip_map.shutdown();
			return (-1);
		}

		// Add all new elements from the sequence above
		transp->add_elements(*elems, e);
		if ((ex = e.exception()) != NULL) {
			e.clear();
			transp->abort_transaction(e);
			if (e.exception())
				e.clear();
			the_privip_map.shutdown();
			return (-1);
		}

		// Commit the changes to the CCR table.
		transp->commit_transaction(e);
		if (e.exception()) {
			e.clear();
			transp->abort_transaction(e);
			if (e.exception())
				e.clear();
			the_privip_map.shutdown();
			return (-1);
		}
	}

	the_privip_map.shutdown();
	return (0);
}

} //extern "C"
#endif
