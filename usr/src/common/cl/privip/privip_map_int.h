/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _PRIVIP_MAP_INT_H
#define	_PRIVIP_MAP_INT_H

#pragma ident	"@(#)privip_map_int.h	1.4	08/05/20 SMI"

#include <sys/list_def.h>
#include <sys/hashtable.h>
#include <sys/threadpool.h>
#include <nslib/ns.h>
#include <h/naming.h>
#include <h/ccr.h>
#include <ccr/data_server_impl.h>
#include <ccr/readonly_copy_impl.h>
#include <ccr/updatable_copy_impl.h>
#include <orb/infrastructure/orb.h>
#include <sys/clconf_int.h>

#ifndef _KERNEL
#include <scadmin/scconf.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif

#define	UINT_SIZE 32

// This is the class that's responsible for providing the private failover
// IP addresseses for the private failover IP address resources. The IP
// addresses are assigned from a pool of available IP addresses and a mapping
// is created between the IP address and the host name assigned to the
// resource.
//
class privip_map_elem : public _SList::ListElem {
public:
	enum action_state { PRIVIP_UNKNOWN = 0,
			PRIVIP_ADD,
			PRIVIP_EXISTS,
			PRIVIP_REMOVE };

	privip_map_elem(in_addr_t addr, char *zonename, nodeid_t nodeid);
	privip_map_elem(char *name, in_addr_t addr,
		char *zonename, nodeid_t nodeid);
	~privip_map_elem();

	in_addr_t get_addr();
	void set_addr(in_addr_t addr);
	char *get_hostname();
	void set_hostname(char *name);
	char *get_zonename();
	nodeid_t get_nodeid();
	void set_zonename(char *name);
	void set_nodeid(nodeid_t nodeid);
	action_state get_action();
	void set_action(action_state ac);
	int same(in_addr_t addr, char *zone, nodeid_t node);

private:
	char hostname[CL_MAX_LEN];
	in_addr_t ipaddr;
	char zonename[CL_MAX_LEN];
	nodeid_t nodeid;
	action_state action;
};

class privip_map {

public:
	privip_map();
	~privip_map();

	SList<privip_map_elem> & get_map_list();

	// Reads CCR and sets up list as above and sets up bitmask
	int privip_map_populate(char *hostname, in_addr_t addr,
		char *zone, nodeid_t node);
	int read_ccr_mappings();
	int is_changed();

	int initialize();
	void shutdown();

	int reset_bitmask();
	int set_hostno_in_use(uint_t host_no);
	int set_ipaddr_in_use(in_addr_t ipaddr);
	in_addr_t get_free_ip_addr();
	int is_ip_valid(in_addr_t);

#ifndef _KERNEL
	int add_mapping_to_ccr(char *name, in_addr_t ipaddr,
		char *zonename, nodeid_t nodeid);
	int change_mapping_in_ccr(char *oldname, char *name,
		in_addr_t ipaddr, char *zonename, nodeid_t nodeid);
	int remove_mapping_from_ccr(char *name);

	int get_ip_address(char *hostname, char *zone, nodeid_t node);
	int change_privhostname(char *hostname, char *zone, nodeid_t node);
	char *get_privhostname(char *zone, nodeid_t node);
	int privhostname_exists(const char *hostname);
	int free_ip_address(char *zone, nodeid_t node);
#endif

private:
	SList<privip_map_elem> *privip_map_list;
	int current_version, privip_map_changed;
	uint_t max_ip_addrs, max_entries, *ip_host_addrs, maxnodes;
	in_addr_t pernode_netnum, pernode_netmask;
};

/*
 * The CCR mapping table must be under /etc/cluster/ccr so that it
 * can be updated as any other CCR table on all the nodes of the cluster
 */
#define	PRIVIP_CCR_TABLE "privip_ccr"
#define	MAX_NAME_LEN 255

#endif	/* _PRIVIP_MAP_INT_H */
