/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)dc_boot.c	1.60	08/05/20 SMI"

#include <sys/types.h>
#include <sys/kobj.h>
#include <sys/param.h>
#include <sys/errno.h>
#include <sys/cmn_err.h>
#include <sys/ddi.h>
#include <sys/sunddi.h>
#include <sys/autoconf.h>
#include <sys/param.h>
#include <sys/sysmacros.h>
#include <sys/bootconf.h>
#include <sys/reboot.h>
#include <sys/proc.h>
#include <sys/cladm.h>

/*
 * 6521154: SC build fails on nv_57 because of removal of definition "u"
 * from user.h
 */
#if SOL_VERSION >= __s11
#define	BUG_6521154
#endif

/*
 * Module linkage information for the kernel.
 */

static struct modlmisc modlmisc = {
	&mod_miscops, "DCS bootstrap module"
};

/*
 * Depending on 32 or 64-bit mode, ml_linkage is an array length 4 or 7.
 */
#ifndef _LP64

static struct modlinkage modlinkage = {MODREV_1, &modlmisc, NULL, NULL, NULL};

#else

static struct modlinkage modlinkage = {	MODREV_1, &modlmisc, NULL, NULL,
    NULL, NULL, NULL, NULL};

#endif

char _depends_on[] = "misc/cl_runtime";

int
_init(void)
{
	int	error = 0;

	if (modrootloaded)
		return (EINVAL);

	error = mod_install(&modlinkage);
	if (error)
		return (error);

	/*
	 * Invoking clconf_init() here to handle any /etc/cluster/nodeid file
	 * related errors. This change makes the clconf_init() in the
	 * startup_modules() routine in base solaris redundant. But
	 * we are keeping it for possible future use.
	 *
	 * If nodeid file does not exist or the nodeid is invalid or out of
	 * range, we will boot the system in non-cluster mode.
	 */

	clconf_init();

	/*
	 * Set the flag in the base kernel to indicate we are
	 * configured but not booting as a cluster.
	 */
	cluster_bootflags |= CLUSTER_CONFIGURED;
	if (!(boothowto & RB_NOBOOTCLUSTER)) {
		if (clconf_get_nodeid() == NODEID_UNKNOWN)
			cmn_err(CE_NOTE, "BOOTING IN NON CLUSTER MODE\n");
		else
			cluster_bootflags |= CLUSTER_BOOTED;
	}

	return (0);
}

int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}

/*
 * Minimum stack size for a cluster node (unless set in /etc/system).
 * Needs to be at least 12K for a 32-bit kernel and 24K for a 64-bit
 * kernel.
 */
#ifdef _LP64
int cl_min_stack_size = 0x6000;
#else
int cl_min_stack_size = 0x3000;
#endif

/*
 * Load drivers needed as part of modload().
 * This is called from modload() in the base kernel.
 */
int
clboot_modload(struct modctl *mp)
{
	return (0);
	/* need to suppress lint info about mp not being used */
} /*lint !e715 */

/*
 * Load modules into memory that are needed to mount root while the BOP
 * interface is still available.
 * This is called from loadrootmodules() in the base kernel.
 */
int
clboot_loadrootmodules()
{
	return (0);
}

/*
 * This is called from rootconf() in the base kernel.
 */
int
clboot_rootconf()
{
	return (0);
}

/*
 * This is called from mountroot() in the base kernel.
 */
void
clboot_mountroot()
{
}

/* This is used by the ORB. */
proc_t *proc_cluster;

/*
 * This is called from cluster_wrapper() in the base kernel.
 */

void
cluster()
{
	kthread_t *t = curthread;
	klwp_t *lwp = ttolwp(t);
	kcondvar_t cv;
	kmutex_t lock;
	int i;

	char *name = "cluster";

	proc_cluster = curproc;
	proc_cluster->p_cstime = 0;
	proc_cluster->p_stime = 0;
	proc_cluster->p_cutime = 0;
	proc_cluster->p_utime = 0;
	/* suppress access data overrun mesg from lint */
#ifdef BUG_6521154
	bcopy(name, PTOU(proc_cluster)->u_psargs,
	    strlen(name) + 1); /*lint !e419 */
	bcopy(name, PTOU(proc_cluster)->u_comm,
	    strlen(name)); /*lint !e419 */
#else
	bcopy(name, u.u_psargs, strlen(name) + 1); /*lint !e419 */
	bcopy(name, u.u_comm, strlen(name)); /*lint !e419 */
#endif

	/*
	 * Ignore signals so we don't have problems.
	 */
	mutex_enter(&proc_cluster->p_lock);
	for (i = SIGHUP; i <= SIGXFSZ; i++) {
		sigaddset(&proc_cluster->p_ignore, i);
	}
	mutex_exit(&proc_cluster->p_lock);

	mutex_init(&lock, NULL, MUTEX_ADAPTIVE, NULL);
	cv_init(&cv, NULL, CV_DEFAULT, NULL);

	/*
	 * Just wait for now.
	 * p_sig contains the list of pending signals, so delete the received
	 * signal from that set. Same for t_sig.
	 */
	mutex_enter(&lock);
	while (1) {
		if (cv_wait_sig(&cv, &lock) == 0) { /* signal received */

			if (lwp->lwp_cursig != 0) {

				mutex_enter(&proc_cluster->p_lock);

				if (lwp->lwp_cursig != 0) {
					if (lwp->lwp_cursig == SIGKILL) {
						proc_cluster->p_flag &=
						    ~SKILLED;
					}
					/*
					 * Delete this signal from
					 * list of pending signals.
					 */
					sigdelset(&proc_cluster->p_sig,
					    lwp->lwp_cursig);

					sigdelset(&t->t_sig,
					    lwp->lwp_cursig);

					lwp->lwp_cursig = 0;
					if (lwp->lwp_curinfo) {
						siginfofree(lwp->lwp_curinfo);
						lwp->lwp_curinfo = NULL;
					}
				}

				mutex_exit(&proc_cluster->p_lock);
			}
		} /* signal received */
	}
}
