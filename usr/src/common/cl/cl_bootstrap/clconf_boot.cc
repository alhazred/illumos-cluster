/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)clconf_boot.cc	1.50	08/05/20 SMI"

#include <sys/types.h>
#include <sys/cmn_err.h>
#include <sys/kobj.h>
#include <sys/systm.h>
#include <sys/kmem.h>
#include <sys/autoconf.h>
#include <sys/ddi.h>
#include <sys/sunddi.h>
#include <sys/sunddi.h>
#include <sys/clconf.h>
#include <sys/clconf_int.h>

/*
 * This file implements the interface to access the
 * configuration data needed in order to boot and form a cluster.
 */

#ifdef _KERNEL
typedef struct _buf kobj_t;
#define	KOBJ_NULL		((kobj_t *)-1)
#define	EOF			-1

#define	nodeid_filename "/etc/cluster/nodeid"
static nodeid_t mynodeid = NODEID_UNKNOWN;
static int clconf_initialized = 0;


/* Retiring soon. Don't use them anymore. */
/*
 * Initialize the cluster configuration (read nodeid file).
 * Note: this can be called from the base kernel.
 */
extern "C" void
clconf_init()
{
	kobj_t			*fp;
	int			ch;

	if (clconf_initialized == 1) {
		return;
	}
	clconf_initialized = 1;

	/* open the file for reading */
	if ((fp = kobj_open_file(nodeid_filename)) == KOBJ_NULL) {
		cmn_err(CE_NOTE, "Can't open %s\n", nodeid_filename);
		/*
		 * Since there is no config file, we are not
		 * configured or booting as a cluster. Return error.
		 */
		return;
	}
	while ((ch = kobj_getc(fp)) != EOF && ch != '\n') {
		if ((ch < '0') || (ch > '9')) {
			mynodeid = NODEID_UNKNOWN;
			break;
		}
		mynodeid = mynodeid * 10 + (uint_t)(ch - '0');
	}

	if ((mynodeid < 1) || (mynodeid > NODEID_MAX))
		mynodeid = NODEID_UNKNOWN;
	if (mynodeid == NODEID_UNKNOWN)
		cmn_err(CE_NOTE, "Invalid or Out of range nodeid\n");

	kobj_close_file(fp);
}

extern "C" nodeid_t
clconf_get_nodeid()
{
	return (mynodeid);
}

extern "C" nodeid_t
clconf_maximum_nodeid()
{
	return (NODEID_MAX);
}
#endif // _KERNEL
