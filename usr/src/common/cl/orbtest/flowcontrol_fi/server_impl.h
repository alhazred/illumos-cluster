/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ORBTEST_FLOWCONTROL_FI_SERVER_IMPL_H
#define	_ORBTEST_FLOWCONTROL_FI_SERVER_IMPL_H

#pragma ident	"@(#)server_impl.h	1.17	08/05/20 SMI"

//
// server_impl header file
//

#include <h/solobj.h>
#include <sys/os.h>
#include <sys/proc.h>
#include <orb/object/adapter.h>
#include <h/flowcontrol_fi.h>

#include <orbtest/flowcontrol_fi/impl_common.h>

//
// Test server implementation.
// The server runs on node 1 and clients on other nodes make various
// types of invocations.
//
class server_impl :
	public McServerof <flowcontrol_fi::server>,
	public impl_common {
public:
	server_impl();
	~server_impl();
	void	_unreferenced(unref_t);

	//
	// Initialize implementation object with the given name.
	// Returns: true if successful, false otherwise.
	//
	bool	init(char *strname);

	//
	// IDL Interfaces.
	//

	//
	// For clients to test simple two-way calls.  It adds dummy1 and
	// dummy2, and returns the total.
	//
	int32_t twoway_simple(int32_t dummy1, int32_t dummy2, Environment &);

	// For clients to test simple one-way calls.
	void    oneway_simple(int32_t dummy1, int32_t dummy2, Environment &);

	int32_t twoway_blocking(int32_t cv_num, int32_t dummy2, Environment &);
	void	oneway_blocking(int32_t dummy1, int32_t dummy2, Environment &);

	// A function that returns the resource pool info on the server.
	void	get_pool_info(int32_t &low, int32_t &moderate, int32_t &high,
	    int32_t &increment, Environment &);

	// A function that lowers the resource pool values
	void	setup_pool_values(int32_t &status, Environment &);

	// A function that restores the original resource pool values
	void	restore_pool_values(int32_t &status, Environment &);

	// For clients to tell this server that testing is done.
	void	done(Environment &e);
private:
	char			msg[128];	// used by Errmsg
	os::mutex_t		lck;
	os::condvar_t	_cv1;
	os::condvar_t	_cv2;

	// Variables set to allow resource pool values to be restored
	int		server_increment;
	int		server_low;
	int		server_moderate;
	int		server_high;
	int		server_minimum;
};

#endif	/* _ORBTEST_FLOWCONTROL_FI_SERVER_IMPL_H */
