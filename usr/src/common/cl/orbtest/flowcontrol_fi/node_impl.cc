/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)node_impl.cc	1.19	08/05/20 SMI"

//
// Node_impl
//

#include <sys/types.h>

#include <sys/os.h>
#include <h/solobj.h>
#include <nslib/ns.h>

#include <orbtest/flowcontrol_fi/node_impl.h>
#include <orbtest/flowcontrol_fi/faults/entry_support.h>
#include <orbtest/flowcontrol_fi/faults/node_death.h>
#include <orbtest/flowcontrol_fi/faults/single_invocations.h>
#include <orbtest/flowcontrol_fi/faults/node_grab_threads.h>
#include <orbtest/flowcontrol_fi/faults/node_wait.h>

node_impl::node_impl()
{
	(void) os::strcpy(msg, "");
	_status = FAIL_STATUS;
}	

node_impl::~node_impl()
{
	// Make sure _unreferenced() has been called.
	lock();
	if (! unref_called()) {
		// run_test_fi can catch this console message
		os::printf("ERROR: node_impl: destructor called before "
			"_unreferenced()\n");
	}
	unlock();

	os::printf("The node is being destroyed\n");
}

void
#ifdef DEBUG
node_impl::_unreferenced(unref_t cookie)
#else
node_impl::_unreferenced(unref_t)
#endif
{
	lock();
	ASSERT(_last_unref(cookie));
	unref_called(true);
	unlock();

	os::printf("The node has been unreferenced\n");
}

//
// Initialize implimentation object with the given name.
//
bool
node_impl::init(char *strname)
{
	flowcontrol_fi::node_ptr	tmpref_p;
	bool			retval;

	// Register with the name server.
	tmpref_p = get_objref();		// get temporary reference
	retval = register_obj(tmpref_p, strname);
	CORBA::release(tmpref_p);

	_status = FAIL_STATUS;

	return (retval);
}

//
// IDL interfaces.
//

void
node_impl::invoke_test(flowcontrol_fi::node_test_op test_num, Environment &)
{
	switch (test_num) {
	case flowcontrol_fi::GET_SERVER:
		server_v = get_server();
		break;
	case flowcontrol_fi::GET_THREADS:
		_status = node_wait_until_got_threads(0, NULL);
		break;
	case flowcontrol_fi::REBOOT:
		node_reboot(0, NULL);
		ASSERT(0);
		break;
	default:
		ASSERT(0);
	}
}

int32_t
node_impl::get_status(Environment &)
{
	return (_status);
}

//
// Clients invoke this when they're finished with this object.
//
void
node_impl::done(Environment &e)
{
	if (! unregister_obj()) {
		os::sprintf(msg, "ERROR: node_impl: Can't unregister "
		    "object\n");
		e.exception(new flowcontrol_fi::Errmsg(msg));
	}

	os::printf("Client called done\n");
}
