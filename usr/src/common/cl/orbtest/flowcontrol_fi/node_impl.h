/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ORBTEST_FLOWCONTROL_FI_NODE_IMPL_H
#define	_ORBTEST_FLOWCONTROL_FI_NODE_IMPL_H

#pragma ident	"@(#)node_impl.h	1.20	08/05/20 SMI"

//
// node_impl header file
//

#define	FAIL_STATUS 3

#include <h/solobj.h>
#include <sys/os.h>
#include <sys/proc.h>
#include <orb/object/adapter.h>
#include <h/flowcontrol_fi.h>

#include <orbtest/flowcontrol_fi/impl_common.h>

//
// Extra node implementation.
// Used to allow a client to perform various tasks on a remote node while
// holding resources on another node. For example, a client on node 2
// might make invocaitons, then call invoke_test on a node_impl on node
// 3 to do some remote task.
//
class node_impl : public McServerof <flowcontrol_fi::node>, public impl_common {
public:

	node_impl();
	~node_impl();
	void	_unreferenced(unref_t);

	//
	// Initialize implementation object with the given name.
	// Returns: true if successful, false otherwise.
	//
	bool	init(char *strname);

	//
	// IDL Interfaces.
	//

	// For client to tell node which test to run
	void	invoke_test(flowcontrol_fi::node_test_op test_num,
	    Environment &e);

	// For clients to reap status of invoke_test
	int32_t	get_status(Environment &e);

	// For clients to tell this server that testing is done.
	void	done(Environment &e);
private:
	int32_t 	_status;
	flowcontrol_fi::server_var server_v;
	char			msg[128];	// used by Errmsg
};

#endif /* _ORBTEST_FLOWCONTROL_FI_NODE_IMPL_H */
