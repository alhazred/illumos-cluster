/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)impl_common.cc	1.14	08/05/20 SMI"

//
// Support routines, inherited by server_impl and node_impl.
//

#if !defined(_KERNEL)
#include <unistd.h>
#endif

#include <sys/os.h>
#include <nslib/ns.h>
#include <orbtest/flowcontrol_fi/impl_common.h>

#if !defined(_FAULT_INJECTION)
//
// Print a failure message when Fault Injection is not compiled in.
// Returns:
//	A non-zero value.
//
int
no_fault_injection()
{
	os::printf("ERROR: Fault Injection not supported.\n");
	return (1);
}
#endif

//
// Class impl_common: common (non-exported) methods for implementation classes.
//
impl_common::impl_common()
{
	_name = NULL;
	_unref_called = false;
}

impl_common::~impl_common()
{
	delete[] _name;
}

//
// Register object with the name server with the given name.
// Returns:
//	True if successful, false otherwise.
//
bool
impl_common::register_obj(CORBA::Object_ptr objref, char *strname)
{
	Environment	e;
	CORBA::Exception	*exceptp;

	// Remember the given string name.
	delete [] _name;
	_name = new char[os::strlen(strname) + 1];
	if (_name == NULL) {
		os::printf("ERROR: register_obj(): can't allocate string\n");
		return (false);
	}
	(void) os::strcpy(_name, strname);

	// Get the root name server.
	naming::naming_context_var context_v = ns::root_nameserver();

	// Register implementation object with the name server.
	context_v->rebind(_name, objref, e);
	if ((exceptp = e.exception()) != NULL) {
		exceptp->print_exception("ERROR:");
		os::printf("ERROR: can't bind \"%s\"\n", _name);
		return (false);
	}

	return (true);		// success
}

//
// Unregister object from the name server.
// Returns:
// 	True if successful, false otherwise.
//
bool
impl_common::unregister_obj()
{
	Environment	e;
	CORBA::Exception	*exceptp;

	// Get the root name server.
	naming::naming_context_var context_v = ns::root_nameserver();

	// Unbind implementation object from name server.
	context_v->unbind(_name, e);
	if ((exceptp = e.exception()) != NULL) {
		exceptp->print_exception("ERROR:");
		os::printf("ERROR: can't unbind \"%s\"\n", _name);
		return (false);
	}

	return (true);
}

//
// Get an object reference with the given name from the name server.
// Returns:
//	The object reference if successful, _nil() otherwise.
//
CORBA::Object_ptr
impl_common::get_obj(char *strname)
{
	const uint32_t		retries = 30;
	CORBA::Object_ptr	obj_p;
	Environment		e;
	CORBA::Exception		*exceptp;
	uint32_t		i;

	// Get the root name server.
	naming::naming_context_var context_v = ns::root_nameserver();

	// Find implementation object from name server. Try several times
	// if it's not in the name server yet.
	for (i = 0; i < retries; ++i) {
		naming::not_found	*notfound;

		e.clear();		// in case previous loop had exception
		obj_p = context_v->resolve(strname, e);

		exceptp = e.exception();
		if (exceptp == NULL) {
			return (obj_p);		// success
		}

		notfound = naming::not_found::_exnarrow(exceptp);
		if (notfound != NULL) {
			// It's not in the name server yet; retry after 1 secs.
#if defined(_KERNEL)
			os::usecsleep(1 * 1000000);
#else
			(void) sleep(1);
#endif
		} else {
			break;
		}
	}

	exceptp->print_exception("ERROR:");
	os::printf("ERROR: can't resolve \"%s\"\n", strname);
	return (CORBA::Object::_nil());
}

//
// Suspend calling thread until _unreferenced() is called.
// Returns: true if _unreferenced() has been called.
//	false if timeout arg (in seconds) is nonzero and it
//	experies before _unreferenced() is called.
//
bool
impl_common::wait_until_unreferenced(uint32_t timeout /* = 0 */)
{
	bool 	retval = true;

	lock();
	if (timeout == 0) {
		while (! unref_called()) {
			_cv.wait(&_mutex);
		}
	} else {
		os::systime	systime;

		// setreltime() requires timeout in usecs.
		systime.setreltime(timeout * 1000000);
		while (! unref_called()) {
			// If timeout expires...
			if (_cv.timedwait(&_mutex, &systime) ==
			    os::condvar_t::TIMEDOUT) {
				retval = false;
				break;
			}
		}
	}
	unlock();
	return (retval);
}


//
// Has _unreferenced() been called?
// Caller must have this object locked prior to calling this routine.
//
bool
impl_common::unref_called()
{
	ASSERT(_mutex.lock_held() != 0);
	return (_unref_called);
}

//
// Mark that _unreferenced() has been called.
//
void
impl_common::unref_called(bool _unref)
{
	ASSERT(_mutex.lock_held() != 0);
	_unref_called = _unref;
	_cv.signal();		// signal wait_until_unreferenced()
}
