/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)server.cc	1.16	08/05/20 SMI"

//
// server support routine, used to init server program
//
#include <orbtest/flowcontrol_fi/server_impl.h>

// -----------------------------------------------------
// KERNEL server
// -----------------------------------------------------
#if defined(_KERNEL)

extern "C" {
	void _cplpl_init(void);
	void _cplpl_fini(void);
}
char _depends_on[] = "misc/cl_runtime misc/cl_load misc/cl_orb "
	"misc/cl_haci misc/cl_quorum misc/cl_comm";

extern struct mod_ops mod_miscops;

struct modlmisc modlmisc = {
	&mod_miscops, "flowcontrol FI test server"
};

struct modlinkage modlinkage = {
	MODREV_1, { (void *)&modlmisc, NULL }
};

static server_impl *serverp = NULL;

int
_init(void)
{
	int	error = 0;

	if ((error = mod_install(&modlinkage)) != 0) {
		return (error);
	}
	_cplpl_init();			// C++ initializtion

#if defined(_FAULT_INJECTION)
	serverp = new server_impl;
	if (! serverp->init(SERVER)) {
		delete serverp;
		error = ECANCELED;
	}
#else
	no_fault_injection();
	error = ENOTSUP;
#endif
	if (error) {
		_cplpl_fini();
		(void) mod_remove(&modlinkage);
	}

	return (error);
}

int
_fini(void)
{
#if defined(_FAULT_INJECTION)
	const uint32_t time_out = 180;	// wait timeout (in seconds)

	if (serverp != NULL) {
		if (! serverp->wait_until_unreferenced(time_out)) {
		    os::printf("FAIL: server didn't get unreferenced "
		    "after %d seconds\n", time_out);
			return (EBUSY);
		} else {
			delete serverp;
		}
	}
	_cplpl_fini();
#endif
	return (mod_remove(&modlinkage));
}

int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));

}

#endif // _KERNEL

// -----------------------------------------------------
// USER server
// -----------------------------------------------------
#if defined(_USER)

#include <signal.h>
#include <string.h>
#include <errno.h>

int
main()
{
#if defined(_FAULT_INJECTION)
	const uint32_t 	time_out = 180;		// wait timeout (in seconds)
	server_impl	*serverp;
	sigset_t	sig_set;
	int		ret, err;

	//
	// The fault injection driver will send a SIGTERM when trying to
	// unload this server. This thread will wait until SIGTERM is sent
	// and then wait until the server has been unreferenced before exiting.
	// We'll have all threads block SIGTERM to ensure that only this thread
	// receives SIGTERM.
	//
	(void) sigemptyset(&sig_set);
	if (sigaddset(&sig_set, SIGTERM) < 0) {
		os::printf("ERROR: Can't add SIGTERM to signal set (%s)\n",
		    strerror(errno));
		return (1);
	}
	if ((ret = thr_sigsetmask(SIG_BLOCK, &sig_set, NULL)) != 0) {
		os::printf("ERROR: Can't block SIGTERM (%s)\n",
			strerror(ret));
		return (1);
	}

	if ((err = ORB::initialize()) != 0) {
		os::printf("ERROR: Can't initialize ORB with error code %d\n",
		    err);
		return (1);
	}

	serverp = new server_impl;
	if (! serverp->init(SERVER)) {
		delete serverp;
		return (1);
	}

	// Wait until SIGTERM is sent.
	if ((ret = sigwait(&sig_set)) < 0) {
		os::printf("ERROR: Can't sigwait for SIGTERM (%s)\n",
			strerror(errno));
		delete serverp;
		return (1);
	}

	// Wait until server is unreferenced.
	if (!serverp->wait_until_unreferenced(time_out)) {
		os::printf("FAIL: server didn't get unreferenced "
			"after %d seconds\n", time_out);
		delete serverp;
		return (1);
	}

	delete serverp;
	return (0);
#else
	return (no_fault_injection());
#endif
}

#endif  // _USER

// -----------------------------------------------------
// UNODE server
// -----------------------------------------------------
#if defined(_UNODE)

int
unode_init()
{
#if defined(_FAULT_INJECTION)
	os::printf("flowcontrol FI test server loaded\n");
	return (0);
#else
	return (no_fault_injection());
#endif
}

int
flowcontrol_fi_server(int, char *[])
{
#if defined(_FAULT_INJECTION)
	server_impl	*serverp;

	serverp = new server_impl;
	if (! serverp->init(SERVER)) {
		delete serverp;
		return (1);
	}

	return (0);
#else
	return (no_fault_injection());
#endif
}

#endif // _UNODE
