#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)fault.data	1.21	08/05/20 SMI"
#

DEF_UPATH       /usr/cluster/orbtest/flowcontrol_fi
DEF_KPATH       /usr/cluster/orbtest/flowcontrol_fi/kernel:/kernel/misc
DEF_UFILE       flowcontrol_fi_client
DEF_KFILE       flowcontrol_fi_client
DEF_UNFILE      flowcontrol_fi_client

#
# NOTE: Not all tests are run by default! There are some tests which have
# versions that have the kernel and server loaded in different domains.
# Only one version of each test is run by default, to save time, but
# they can be reenabled if desired.
#

# TEST 1
# This test gets the server reference and makes a single, non-blocking
# invocation.
# user client -> user server

TEST	Simple flowcontrol test with 1 reference(user->user)
	SERVER		-n NODE_1 -m U -f flowcontrol_fi_server
	CLIENT		-n NODE_2 -m U simple_invoke_1
	CLIENT		-n NODE_2 -m U server_done 
	CLIENT		-n ALL_NODES -m K clear_all
	GROUP		simple-invoke
	SKIP		-r "See NOTE in fault.data" U UN K
	REQ_NODES	2	
END

# Test 2
# This test gets the server reference and makes a single, non-blocking
# invocation.
# kernel client -> kernel server

TEST	Simple flowcontrol test with 1 reference(kernel->kernel)
	SERVER		-n NODE_1 -m K -f flowcontrol_fi_server
	CLIENT		-n NODE_2 -m K simple_invoke_1
	CLIENT		-n NODE_2 -m K server_done 
	CLIENT		-n ALL_NODES -m K clear_all
	GROUP		simple-invoke
	REQ_NODES	2	
END

# TEST 3
# This test gets the server reference and makes a single, non-blocking
# invocation.
# user client -> kernel server

TEST	Simple flowcontrol test with 1 reference(user->kernel)
	SERVER		-n NODE_1 -m K -f flowcontrol_fi_server
	CLIENT		-n NODE_2 -m U simple_invoke_1
	CLIENT		-n NODE_2 -m U server_done 
	CLIENT		-n ALL_NODES -m K clear_all
	GROUP		simple-invoke
	REQ_NODES	2	
	SKIP		-r "See NOTE in fault.data" U UN K
END

# TEST 4
# This test gets the server reference and makes a single, non-blocking
# invocation.
# kernel client -> user server

TEST	Simple flowcontrol test with 1 reference(kernel->user)
	SERVER		-n NODE_1 -m U -f flowcontrol_fi_server
	CLIENT		-n NODE_2 -m K simple_invoke_1
	CLIENT		-n NODE_2 -m K server_done 
	CLIENT		-n ALL_NODES -m K clear_all
	GROUP		simple-invoke
	REQ_NODES	2	
	SKIP		-r "See NOTE in fault.data" U UN K
END

# TEST 5 - blocked_invoke_1
# This test makes a series of invocations which all block on the
# server side on a cv. When the node runs out of resources, it
# requests more from the server. When it receives more, it calls
# the routine got_threads. A fault point in got_threads releases
# the blocked invocations and allows them to complete.
#
# user client -> user server

TEST	Blocked invocation with 1 (or more) requests (user -> user)
	SERVER		-n NODE_1 -m U -f flowcontrol_fi_server
	CLIENT		-n NODE_2 -m U node_wait_until_got_threads 
	CLIENT		-n NODE_1 -m K release_all
	CLIENT		-n NODE_2 -m U server_done
	CLIENT		-n ALL_NODES -m K clear_all
	GROUP		blocked-invoke
	REQ_NODES	2
	SKIP		-r "See NOTE in fault.data" U UN K
END

# TEST 6
# This test makes a series of invocations which all block on the
# server side on a cv. When the node runs out of resources, it
# requests more from the server. When it receives more, it calls
# the routine got_threads. A fault point in got_threads releases
# the blocked invocations and allows them to complete.
#
# kernel client -> user server

TEST	Blocked invocation with 1 (or more) requests (kernel->user)
	SERVER		-n NODE_1 -m U -f flowcontrol_fi_server
	CLIENT		-n NODE_2 -m K node_wait_until_got_threads 
	CLIENT		-n NODE_1 -m K release_all
	CLIENT		-n NODE_2 -m U server_done
	CLIENT		-n ALL_NODES -m K clear_all
	GROUP		blocked-invoke
	REQ_NODES	2
END

# TEST 7
# This test makes a series of invocations which all block on the
# server side on a cv. When the node runs out of resources, it
# requests more from the server. When it receives more, it calls
# the routine got_threads. A fault point in got_threads releases
# the blocked invocations and allows them to complete.
#
# user client -> kernel server

TEST	Blocked invocation with 1 (or more) requests (user->kernel)
	SERVER		-n NODE_1 -m K -f flowcontrol_fi_server
	CLIENT		-n NODE_2 -m U node_wait_until_got_threads 
	CLIENT		-n NODE_1 -m K release_all
	CLIENT		-n NODE_2 -m U server_done
	CLIENT		-n ALL_NODES -m K clear_all
	GROUP		blocked-invoke
	REQ_NODES	2
	SKIP		-r "See NOTE in fault.data" U UN K
END

# TEST 8
# This test makes a series of invocations which all block on the
# server side on a cv. When the node runs out of resources, it
# requests more from the server. When it receives more, it calls
# the routine got_threads. A fault point in got_threads releases
# the blocked invocations and allows them to complete.
#
# kernel client -> kernel server

TEST	Blocked invocation with 1 (or more) requests (kernel -> kernel)
	SERVER		-n NODE_1 -m K -f flowcontrol_fi_server
	CLIENT		-n NODE_2 -m K node_wait_until_got_threads 
	CLIENT		-n NODE_1 -m K release_all
	CLIENT		-n NODE_2 -m K server_done
	CLIENT		-n ALL_NODES -m K clear_all
	GROUP		blocked-invoke
	REQ_NODES	2
	SKIP		-r "See NOTE in fault.data" U UN K
END

# TEST 9 
#
# First, this starts an external node that grabs the minimum
# number of resources
# This test issues a series of blocking invocations to the server.
# After the node runs out of resources, it requests more from the
# server, and gets more. This is repeated until the server returns
# a grant of 0 threads, signaling that all threads on the server
# are allocated. When the node receives this grant, it is processed
# in process_resource_grant. In this code is a fault point which
# releases the blocked invocations, allowing them to return.
#

TEST	Blocked invocation with rejected request (kernel -> kernel)
	SERVER		-n NODE_1 -m K -f flowcontrol_fi_server
	CLIENT		-n NODE_1 -m K change_resource_pool
	CLIENT		-n NODE_3 -m K node_grab_threads_until_got_threads 1
	CLIENT		-n NODE_2 -m K clear_all
	CLIENT		-n NODE_2 -m K node_wait_until_request_denied 
	CLIENT		-n NODE_1 -m K restore_resource_pool
	CLIENT		-n NODE_1 -m K release_all
	CLIENT		-n NODE_2 -m K server_done
	CLIENT		-n ALL_NODES -m K clear_all
	GROUP		denied-invoke
	REQ_NODES		3
END

# TEST 10
# Node 2 makes invocations until it has received a grant of threads
# from the server. It then unblocks all the invocations, resulting in
# many unused threads on the client.
# The program waits until the threads have been released back to the
# server
#
# user client -> user server

TEST	Blocked invocation with released resources (user -> user)
	SERVER		-n NODE_1 -m U -f flowcontrol_fi_server
	CLIENT		-n NODE_2 -m U node_wait_until_resource_release 
	CLIENT		-n NODE_1 -m K release_all
	CLIENT		-n NODE_2 -m U server_done
	CLIENT		-n ALL_NODES -m K clear_all
	GROUP		released-invocations	
	REQ_NODES	2
	SKIP		-r "See NOTE in fault.data" U UN K
END

# TEST 11
# Node 2 makes invocations until it has received a grant of threads
# from the server. It then unblocks all the invocations, resulting in
# many unused threads on the client.
# The program waits until the threads have been released back to the
# server
#
# user client -> kernel server

TEST	Blocked invocation with released resources (user->kernel)
	SERVER		-n NODE_1 -m K -f flowcontrol_fi_server
	CLIENT		-n NODE_2 -m U node_wait_until_resource_release 
	CLIENT		-n NODE_1 -m K release_all
	CLIENT		-n NODE_2 -m U server_done
	CLIENT		-n ALL_NODES -m K clear_all
	GROUP		released-invocations
	REQ_NODES	2
	SKIP		-r "See NOTE in fault.data" U UN K
END

# TEST 12
# Node 2 makes invocations until it has received a grant of threads
# from the server. It then unblocks all the invocations, resulting in
# many unused threads on the client.
# The program waits until the threads have been released back to the
# server
#
# kernel client -> user server

TEST	Blocked invocation with released resources (kernel->user)
	SERVER		-n NODE_1 -m U -f flowcontrol_fi_server
	CLIENT		-n NODE_2 -m K node_wait_until_resource_release 
	CLIENT		-n NODE_1 -m K release_all
	CLIENT		-n NODE_2 -m K server_done
	CLIENT		-n ALL_NODES -m K clear_all
	GROUP		released-invocations
	REQ_NODES	2
	SKIP		-r "See NOTE in fault.data" U UN K
END

# TEST 13
# Node 2 makes invocations until it has received a grant of threads
# from the server. It then unblocks all the invocations, resulting in
# many unused threads on the client.
# The program waits until the threads have been released back to the
# server
#
# kernel client -> kernel server

TEST	Blocked invocation with released resources (kernel -> kernel)
	SERVER		-n NODE_1 -m K -f flowcontrol_fi_server
	CLIENT		-n NODE_2 -m K node_wait_until_resource_release 
	CLIENT		-n NODE_1 -m K release_all
	CLIENT		-n NODE_2 -m K server_done
	CLIENT		-n ALL_NODES -m K clear_all
	GROUP		released-invocations
	REQ_NODES	2
END

# TEST 14
# Want to make sure that a node requesting additional resources causes
# a resource recall if there are no resources available
# Node_2 makes invocations until a resource request is denied. It then 
# returns, but continues to hold the invocations.
# Node_3 starts up, and asks server for resources. Server recalls 
# resources from Node_2. This triggers fault point in recall code which
# releases Node_2's invocations, thus allowing 3 to complete invocation.

TEST	New node causes resource recall (kernel -> kernel)
	SERVER		-n NODE_1 -m K -f flowcontrol_fi_server
	SERVER		-n NODE_3 -m K -f flowcontrol_fi_node
	CLIENT		-n NODE_1 -m K change_resource_pool
	CLIENT		-n NODE_2 -m K node_wait_until_release
	CLIENT		-n NODE_1 -m K restore_resource_pool
	CLIENT		-n NODE_1 -m K release_all
	CLIENT		-n NODE_2 -m K server_done
	CLIENT		-n NODE_2 -m K node_done
	CLIENT		-n ALL_NODES -m K clear_all
	GROUP		reboot-invoke
	REQ_NODES	3
END

# TEST 15
# Node 2 makes invocations until all resources on server are used. 
# At this point, Node 2 reboots. A new client is started on Node 2
# which attempts to make an invocation. This should succeed if all
# of Node 2's resources have been released.
#
TEST	Node reboot after resource exhaustion (kernel -> kernel )
	SERVER		-n NODE_1 -m K -f flowcontrol_fi_server
	CLIENT		-n NODE_1 -m K change_resource_pool
	CLIENT		-n NODE_2 -d NODE_2 -x -m K node_reboot_request_denied
	CLIENT		-n NODE_2 -m K node_wait_until_got_threads
	CLIENT		-n NODE_1 -m K restore_resource_pool
	CLIENT		-n NODE_1 -m K release_all
	CLIENT		-n NODE_2 -m K server_done
	CLIENT		-n ALL_NODES -m K clear_all
	GROUP		reboot-invoke
	REQ_NODES	2	
END

# TEST 16
#
# This is a repeat of Test 1, but with a oneway invocation instead
# of a twoway. The following tests must all be run on kernel level
# because oneway invocations are only valid in the kernel
#
TEST	Simple flowcontrol test with 1 oneway invocation(kernel->kernel)
	SERVER		-n NODE_1 -m K -f flowcontrol_fi_server
	CLIENT		-n NODE_2 -m K oneway_invoke
	CLIENT		-n NODE_1 -m K release_all
	CLIENT		-n NODE_2 -m K server_done 
	CLIENT		-n ALL_NODES -m K clear_all
	GROUP		simple-invoke
	REQ_NODES	2	
END

# TEST 17
#
# This is a repeat of  Test 8 using oneways, but it also checks that the
# client does not reuse resources that are held by the server for oneway
# requests until the server informs the client that the requests have 
# finished
#
TEST	Blocked oneway invocation with 1 (or more) requests (kernel->kernel)
	SERVER		-n NODE_1 -m K -f flowcontrol_fi_server
	CLIENT		-n NODE_2 -m K oneways_wait_until_got_threads 
	CLIENT		-n NODE_1 -m K release_all
	CLIENT		-n NODE_2 -m K server_done
	CLIENT		-n ALL_NODES -m K clear_all
	GROUP		blocked-invoke
	REQ_NODES	2
END

# TEST 18
#
# This is a repeat of test 13 with oneway invocations. It double-checks
# two things specific to oneways. One, that when the invocations complete,
# the client is informed that the resources are again available. Two, 
# that when the resources are unused, they will be released to the
# server
#
TEST	Blocked oneway invocation with released resources (kernel->kernel)
	SERVER		-n NODE_1 -m K -f flowcontrol_fi_server
	CLIENT		-n NODE_2 -m K oneways_wait_until_resource_release 
	CLIENT		-n NODE_1 -m K release_all
	CLIENT		-n NODE_2 -m K server_done
	CLIENT		-n ALL_NODES -m K clear_all
	GROUP		released-invocations	
	REQ_NODES	2
END

# Test 19
#
# This test checks that flow control works properly if the flow
# control management thread runs during a recall.
# Fist, node 2 gets the current resource policy. It then
# makes (high - inc/2) invocations. Then, node 3 makes a request.
# The server sends a recall to node 2, which should have
# inc/2 unused threads.
# The node does a short wait (to try to cause the resource
# pool thread to run in the interim). Then all of the invocations
# are released
#
TEST Node causes partial resource release (kernel -> kernel)
	SERVER		-n NODE_1 -m K -f flowcontrol_fi_server
	SERVER		-n NODE_3 -m K -f flowcontrol_fi_node
	CLIENT		-n NODE_1 -m K change_resource_pool
	CLIENT		-n NODE_2 -m K node_partial_release
	CLIENT		-n NODE_1 -m K release_all
	CLIENT		-n NODE_1 -m K restore_resource_pool
	CLIENT		-n NODE_2 -m K server_done
	CLIENT		-n NODE_2 -m K node_done
	CLIENT		-n ALL_NODES -m K clear_all
	GROUP		reboot-invoke
	REQ_NODES	3
END

# TEST 20
#
# This tests that when a node joins the cluster and there are
# resources available, it is allocated some by the server.
# The test sets a fault in the resource_balancer code path
# and then reboots node 3. When node 3 asks for resources and
# they are granted, the fault point is hit and we return pass.
#
TEST	Node joining when resources are available (kernel->kernel)
	SERVER		-n NODE_1 -m K -f flowcontrol_fi_server
	SERVER		-n NODE_3 -m K -f flowcontrol_fi_node
	CLIENT		-n NODE_2 -d NODE_3 -m K node_join_resources_available
	CLIENT		-n NODE_1 -m K release_all
	CLIENT		-n NODE_2 -m K server_done
	CLIENT		-n ALL_NODES -m K clear_all
	GROUP		joining_nodes
	REQ_NODES	3
END

# TEST 21
#
# This tests that when a node joins and resources are unavailable,
# the server recalls resources from another node.
# The test starts on node 2. It gets the current resource policy
# and makes high blocking invocations. It then reboots node 3.
# When node 3 rejoins, all of the server threads are given to 2. When
# the server issues the recall to node 2, the invocations are unblocked
# and return.
#
TEST	Node joining when resources are unavailable (kernel->kernel)
	SERVER		-n NODE_1 -m K -f flowcontrol_fi_server
	SERVER		-n NODE_3 -m K -f flowcontrol_fi_node
	CLIENT		-n NODE_2 -m K change_resource_pool
	CLIENT		-n NODE_2 -d NODE_3 -m K node_join_resources_unavailable
	CLIENT		-n NODE_2 -m K restore_resource_pool
	CLIENT		-n NODE_1 -m K release_all
	CLIENT		-n NODE_2 -m K server_done
	CLIENT		-n ALL_NODES -m K clear_all
	GROUP		joining_nodes
	REQ_NODES	3
END

# TEST 22
#
# This tests that when a node dies when there is an outstanding
# resource request, the resources are allocated to the requesting
# node.
# The test starts on node 2. It gets the current resource policy
# and makes 'high' invocations. It then reboots node 3. When node
# 2 gets node 3's released resources, the invocations are unblocked
# and return.
#
TEST	Node death while resources are needed (kernel->kernel)
	SERVER		-n NODE_1 -m K -f flowcontrol_fi_server
	SERVER		-n NODE_3 -m K -f flowcontrol_fi_node
	CLIENT		-n NODE_2 -m K change_resource_pool
	CLIENT		-n NODE_2 -d NODE_3 -m K node_die_resources_needed
	CLIENT		-n NODE_2 -m K restore_resource_pool
	CLIENT		-n NODE_1 -m K release_all
	CLIENT		-n NODE_2 -m K server_done
	CLIENT		-n ALL_NODES -m K clear_all
	GROUP		dying_nodes
	REQ_NODES	3
END

# TEST 23
#
# This test checks that if a node dies and the node's resources are
# unneeded, the resources are destroyed.
# The test starts on node 1, the server. Node 2 makes invocations
# until the server denies a request for more resources. The server
# sets a fault point in the code path for destroy threads, and reboots
# node 2. When the code path fault point is hit, the test returns.
#
TEST	Node death while resources are unneeded (kernel->kernel)
	SERVER		-n NODE_1 -m K -f flowcontrol_fi_server
	SERVER		-n NODE_2 -m K -f flowcontrol_fi_node
	CLIENT		-n NODE_1 -m K change_resource_pool
	CLIENT		-n NODE_2 -m K node_grab_threads_until_request_denied 1
	CLIENT		-n NODE_1 -d NODE_2 -m K node_die_resources_unneeded
	CLIENT		-n NODE_1 -m K release_all
	CLIENT		-n NODE_1 -m K server_done
	CLIENT		-n ALL_NODES -m K clear_all
	GROUP		dying_nodes
	REQ_NODES	2
END

# TEST 24
#
# First, node 2 makes invocations until it gets additional threads
# from the server once.
# Then, node 3 makes invocations until the server runs out of threads.
# Next, node 4 makes invocations until it requests more from the server.
# Now, the server recalls threads from node 3, which has the most. At
# this point, the invocations on node 2 are unblocked. The server should
# use these threads to grant to node 4. When node 4 gets threads, it
# unblocks all of the invocations.
#
TEST	Recall fulfilled by node other than recalled node (kernel->kernel)
	SERVER		-n NODE_1 -m K -f flowcontrol_fi_server
	CLIENT		-n NODE_1 -m K change_resource_pool 3
	CLIENT		-n NODE_2 -m K node_grab_threads_until_got_threads 1
	CLIENT		-n NODE_3 -m K clear_all
	CLIENT		-n NODE_3 -m K node_grab_threads_until_request_denied 2
	CLIENT		-n NODE_4 -m K clear_all
	CLIENT		-n NODE_4 -m K node_request_filled_by_other_node
	CLIENT		-n NODE_1 -m K restore_resource_pool
	CLIENT		-n NODE_1 -m K release_all
	CLIENT		-n NODE_2 -m K server_done
	CLIENT		-n ALL_NODES -m K clear_all
	GROUP		recall
	REQ_NODES	4
END

# TEST 25
#
# This tests that if a node releases threads and there is an
# outstanding request, the threads are granted to the node with
# the request.
# The test starts making invocations from node 2 until the node
# requests threads once from the server. Node 3 then requests
# threads until the server runs out. Now, the invocations from
# node 2 are unblocked, and node 2 releases those resources. When
# node 3 receives the released resources, it unblocks all the
# invocations, and the test returns.
#
TEST	Released threads sent to client with outstanding request (k->k)
	SERVER		-n NODE_1 -m K -f flowcontrol_fi_server
	CLIENT		-n NODE_1 -m K change_resource_pool
	CLIENT		-n NODE_2 -m K node_grab_threads_until_got_threads 1
	CLIENT		-n NODE_3 -m K clear_all
	CLIENT		-n NODE_3 -m K node_released_threads_needed 2
	CLIENT		-n NODE_1 -m K restore_resource_pool
	CLIENT		-n NODE_1 -m K release_all
	CLIENT		-n NODE_2 -m K server_done
	CLIENT		-n ALL_NODES -m K clear_all
	GROUP		released_threads
	REQ_NODES	3
END

#
# TEST 26
# This test makes a single blocking invocation from node 2, then
# kills node 2. When node 2 returns, it makes invocations in
# batches of increment threads until the thread that processed
# the first invocations is reused.
#
TEST	Node death, check that thread is reused (k->k)
	SERVER		-n NODE_1 -m K -f flowcontrol_fi_server
	CLIENT		-n NODE_1 -m K change_resource_pool
	CLIENT		-n NODE_2 -d NODE_2 -x -m K node_die_during_invo 1
	CLIENT		-n NODE_3 -m K node_grab_threads_until_got_threads 2
	CLIENT		-n NODE_2 -m K node_wait_until_thread_reused
	CLIENT		-n NODE_1 -m K release_all
	CLIENT		-n NODE_2 -m K server_done
	CLIENT		-n ALL_NODES -m K clear_all
	GROUP		node_death
	REQ_NODES	3
END
