/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)server_impl.cc	1.23	08/05/20 SMI"

//
// Server_impl
//

#include <sys/types.h>

#include <sys/os.h>
#include <orb/flow/resource_balancer.h>
#include <h/solobj.h>
#include <nslib/ns.h>

#include <orbtest/flowcontrol_fi/server_impl.h>

server_impl::server_impl()
{
	(void) os::strcpy(msg, "");
}	

server_impl::~server_impl()
{
	// Make sure _unreferenced() has been called.
	lock();
	if (! unref_called()) {
		// run_test_fi can catch this console message
		os::printf("ERROR: server_impl: destructor called before "
			"_unreferenced()\n");
	}
	unlock();

}

void
#ifdef DEBUG
server_impl::_unreferenced(unref_t cookie)
#else
server_impl::_unreferenced(unref_t)
#endif
{
	lock();
	ASSERT(_last_unref(cookie));
	unref_called(true);
	unlock();

	os::printf("The server has been unreferenced\n");
}

//
// Initialize implimentation object with the given name.
//
bool
server_impl::init(char *strname)
{
	flowcontrol_fi::server_ptr	tmpref_p;
	bool			retval;

	// Register with the name server.
	tmpref_p = get_objref();		// get temporary reference
	retval = register_obj(tmpref_p, strname);
	CORBA::release(tmpref_p);

	return (retval);
}

//
// IDL interfaces.
//

//
// For clients to test simple two-way calls.  It adds dummy1 and
// dummy2, and returns the total.
//
int32_t
server_impl::twoway_simple(int32_t dummy1, int32_t dummy2, Environment &)
{
	// Pause for a short while to add randomness
	os::hrtime_t	time = os::gethrtime();
	int	_random = (int)(time % 30);
	os::usecsleep(_random * 1000000);

	// Add the two arguments and return result.
	return (dummy1 + dummy2);
}

//
// twoway_blocking
// This call is invoked by the client to tie up resources for a
// controlled length of time. The general procedure is as follows:
// 1. Many calls to twoway_blocking
// 2. Cluster event causes RELEASE to be triggered
// 3. After some time, the first thread to wake from the _cv.timedwait
//    sees that the release has been triggered and broadcasts to the
//    others.
// 4. The other threads wake up, also broadcast, and return.
//
int32_t
server_impl::twoway_blocking(int32_t cv_num, int32_t dummy2, Environment &)
{
	//
	// cv_num specifies which cv to wait on, allowing some threads to
	// be released earlier than others. The only cv's currently
	// supported are 1 and 2.
	//
	os::condvar_t	*_cvp;
	int	fnum;

	if (cv_num == 1) {
		_cvp = &_cv1;
		fnum = FAULTNUM_FLOWCONTROL_SIGNAL_RELEASE;
	} else if (cv_num == 2) {
		_cvp = &_cv2;
		fnum = FAULTNUM_FLOWCONTROL_SIGNAL_RELEASE_2;
	} else {
		os::printf("FAIL: Tried to use an unknown cv\n");
		return (0);
	}

#if defined(_FAULT_INJECTION)
	os::systime to;
	int i = 0;
	int max_loop = 30;
	int timeout = 15;

	//
	// This loops up to max_loop times, each time checking to see if
	// the release has been triggered. This means that no additional
	// invocations are needed to broadcast on the cv, just a delay
	// until one of the existing invocations wakes and broadcasts
	//
	while (i < max_loop) {
		if (fault_triggered(fnum, NULL, 0)) {
			lck.lock();
			_cvp->broadcast();
			lck.unlock();
			break;
		} else if (fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT,
		    0, NULL)) {
			os::printf("FAIL: Abort causing invocation return\n");
			break;
		} else {
			// Set to wake the condition variable in timeout seconds
			to.setreltime(timeout * 1000000);
			lck.lock();
			_cvp->timedwait(&lck, &to);
			lck.unlock();
			i++;
		}
	}

	if (i == max_loop) {
		NodeTriggers::add(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT,
			NULL, 0, TRIGGER_ALL_NODES);
		os::printf("FAIL: Invocations timed out, i = %d\n", i);
		lck.lock();
		_cvp->broadcast();
		lck.unlock();
	}

	// Sleep a random amount of time before returning.
	os::hrtime_t	time = os::gethrtime();
	int	_random = (int)(time % 30);
	os::usecsleep(_random * 1000000);

#endif

	// Add the two arguments and return result.
	return (cv_num + dummy2);
}

//
// For clients to test simple one-way calls.
//
void
server_impl::oneway_simple(int32_t, int32_t, Environment &)
{
}

//
// For clients to test one-way calls that block on the server.
// Blocked invocations wait to see the RELEASE triggered.
//
void
server_impl::oneway_blocking(int32_t, int32_t, Environment &)
{
	os::condvar_t	*_cvp = &_cv1;

#if defined(_FAULT_INJECTION)
	os::systime to;
	int i = 0;
	int max_loop = 30;
	int timeout = 15;

	//
	// This loops up to max_loop times, each time checking to see if
	// the release has been triggered. This means that no additional
	// invocations are needed to broadcast on the cv, just a delay
	// until one of the existing invocations wakes and broadcasts
	//
	while (i < max_loop) {
		if (fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_RELEASE,
			NULL, 0)) {
			lck.lock();
			_cvp->broadcast();
			lck.unlock();
			break;
		} else if (fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT,
		    0, NULL)) {
			os::printf("FAIL: Abort causing invocation return\n");
			break;
		} else {
			to.setreltime(timeout * 1000000);
			lck.lock();
			_cvp->timedwait(&lck, &to);
			lck.unlock();
			i++;
		}
	}

	if (i == max_loop) {
		NodeTriggers::add(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT,
		    NULL, 0, TRIGGER_ALL_NODES);

		// Wake up the other waiting threads.
		lck.lock();
		_cvp->broadcast();
		lck.unlock();
	}

	// Sleep a random amount of time before returning.
	os::hrtime_t	time = os::gethrtime();
	int	_random = (int)(time % 30);
	os::usecsleep(_random * 1000000);
#endif

}

//
// A function for the client to query the server's resource pool
// information.
//
void
server_impl::get_pool_info(int32_t &low, int32_t &moderate, int32_t &high,
    int32_t &increment, Environment &)
{
#ifdef _KERNEL_ORB
	const resource_defs::resource_pool_t pool =
	    resource_defs::DEFAULT_POOL;	// the pool to query

	resource_balancer::the().get_policy(pool, low, moderate, high,
	    increment);
#else
	low = 0;
	moderate = 0;
	high = 0;
	increment = 0;
#endif // _KERNEL_ORB
}

//
// A function for the client to tell the server to reduce the server's
// resource pool values for the duration of the test. The agument
// returns 0 if successful, 1 otherwise.
//
void
server_impl::setup_pool_values(int32_t &status, Environment &)
{
#ifdef _KERNEL_ORB
	const resource_defs::resource_pool_t pool =
	    resource_defs::DEFAULT_POOL;	// the pool to query

	resource_balancer::the().get_policy(pool, server_low,
	    server_moderate, server_high, server_increment);

	int num_nodes = resource_balancer::the().number_nodes();
	server_minimum = resource_balancer::the().get_threads_min(pool);

	int	low, moderate, high, minimum;
	minimum = server_low;
	low = server_increment * reallocation_inc_multiple;
	moderate = low;
	high = num_nodes * (4 * server_increment);

	resource_balancer::the().set_threads_min(pool, minimum);
	if (resource_balancer::the().set_policy(pool, low, moderate,
	    high, server_increment, num_nodes)) {
		status = 0;
	} else {
		status = 1;
	}
#else
	status = 1;
#endif // _KERNEL_ORB
}

//
// A function for the client to tell the server to restore the server's
// resource pool values at the end of the test. The agument
// returns 0 if successful, 1 otherwise.
//
void
server_impl::restore_pool_values(int32_t &status, Environment &)
{
#ifdef _KERNEL_ORB
	const resource_defs::resource_pool_t pool =
	    resource_defs::DEFAULT_POOL;	// the pool to query

	int num_nodes = resource_balancer::the().number_nodes();

	if (!resource_balancer::the().set_policy(pool, server_low,
	    server_moderate, server_high, server_increment, num_nodes)) {
		status = 1;
		return;
	}

	resource_balancer::the().set_threads_min(pool, server_minimum);
	status = 0;
#else
	status = 1;
#endif // _KERNEL_ORB
}

//
// Clients invoke this when they're finished with this object.
//
void
server_impl::done(Environment &e)
{
	if (! unregister_obj()) {
		os::sprintf(msg, "ERROR: server_impl: Can't unregister "
		    "object\n");
		e.exception(new flowcontrol_fi::Errmsg(msg));
	}

	os::printf("Client called done\n");
}
