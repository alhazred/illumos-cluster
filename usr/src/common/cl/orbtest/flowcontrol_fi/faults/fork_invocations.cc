/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fork_invocations.cc	1.11	08/05/20 SMI"

#ifndef _KERNEL
#include <unistd.h>
#endif

#include <stdio.h>
#include <orbtest/flowcontrol_fi/server_impl.h>
#include <sys/thread.h>
#include <sys/proc.h>
#include <orbtest/flowcontrol_fi/faults/entry_support.h>

//
// fork_blocked_invoke
// This function repeatedly forks threads and passes them to the
// thread function which is passed as an argument. It stops either
// when there is an ABORT or a STOP signaled.
//
int
fork_blocked_invoke(int cv_num, void (*threadfn)(void *))
{
#if defined(_FAULT_INJECTION)
	int	rslt = 0;

	while ((!fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_STOP, NULL, 0)) &&
	    (!fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, NULL, 0))) {

#ifdef _KERNEL
		if (thread_create(NULL, NULL, (void(*)(void))threadfn,
		    (char *)&cv_num, 0, &p0, TS_RUN, 60) == NULL) {
			os::printf("Thread create failed for "
			    "blocked_invocation\n");
			NodeTriggers::add(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT,
			    NULL, 0, TRIGGER_ALL_NODES);
			rslt = 1;
		}
#else // USER
		if (thr_create(NULL, (size_t)0, (void *(*)(void *))threadfn,
		    (char *)&cv_num, THR_BOUND|THR_DETACHED, NULL)) {
			os::printf("Thread create failed for "
			    "blocked_invocation\n");
			NodeTriggers::add(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT,
			    NULL, 0, TRIGGER_ALL_NODES);
			rslt = 1;
		}
#endif // _KERNEL

		// Wait 1/100 second between invocations
		os::usecsleep(1 * 10000);
	}

	if (fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, NULL, 0)) {
		rslt = 1;
	}

	return (rslt);
#else
	cv_num, threadfn;		// To shut compiler warning

	return (no_fault_injection());
#endif
}

//
// fork_n_blocked_invoke
// This function repeatedly forks threads and starts them with the
// thread function which is passed in. It stops either when an ABORT
// is signalled, or when it has forked n threads, where n is the first
// argument passed.
//
int
fork_n_blocked_invoke(int num_invo, int cv_num, void (*threadfn)(void *))
{
	os::printf("*** Forking %d threads with blocked invocation.\n",
	    num_invo);
#if defined(_FAULT_INJECTION)
	int	rslt = 0;
	int count = 0;

	while ((!fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_STOP, NULL, 0)) &&
	    (!fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, NULL, 0)) &&
	    (count < num_invo)) {

		count++;

#ifdef _KERNEL
		if (thread_create(NULL, NULL, (void(*)(void))threadfn,
		    (char *)&cv_num, 0, &p0, TS_RUN, 60) == NULL) {
			os::printf("Thread create failed for "
			    "blocked_invocation\n");
			NodeTriggers::add(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT,
			    NULL, 0, TRIGGER_ALL_NODES);
			rslt = 1;
		}
#else // USER
		if (thr_create(NULL, (size_t)0, (void *(*)(void *))threadfn,
		    (char *)&cv_num, THR_BOUND|THR_DETACHED, NULL)) {
			os::printf("Thread create failed for "
			    "blocked_invocation\n");
			NodeTriggers::add(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT,
			    NULL, 0, TRIGGER_ALL_NODES);
			rslt = 1;
		}
#endif // _KERNEL

		// Wait 1/100 second between invocations
		os::usecsleep(1 * 10000);
	}

	if (fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, NULL, 0)) {
		rslt = 1;
	}

	return (rslt);
#else
	cv_num, threadfn;		// To shut compiler warning

	return (no_fault_injection());
#endif
}
