/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_FLOWCONTROL_FI_NODE_GRAB_THREADS_H
#define	_FLOWCONTROL_FI_NODE_GRAB_THREADS_H

#pragma ident	"@(#)node_grab_threads.h	1.9	08/05/20 SMI"

//
// Functions which grab threads until some
// fault point has been triggered. The
// functions never release the invocations
// themselves, so some other function must
// trigger the appropriate release.
//

#include <sys/os.h>
#include <orb/invo/common.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/fault/fault_injection.h>
#include <h/flowcontrol_fi.h>
#include <orbtest/flowcontrol_fi/impl_common.h>

//
// Test entry functions.
//

//
// A more specific version of the following. This test
// is a target of flowcontrol_fi_node's invoke_test. It
// assumes that another function has already set the STOP
// trigger.
//
int		grab_threads_until_stop(int argc, char *argv[]);

//
// This test sets the STOP trigger at the GOT_THREADS fault
// point, and forks invocations until it hits that fault
// point.
//
int 	node_grab_threads_until_got_threads(int argc, char *argv[]);

//
// This test sets the STOP trigger at the request denied fault
// point, and forks invocations until it hits that fault
// point.
//
int 	node_grab_threads_until_request_denied(int argc, char *argv[]);

#endif /* _FLOWCONTROL_FI_NODE_GRAB_THREADS_H */
