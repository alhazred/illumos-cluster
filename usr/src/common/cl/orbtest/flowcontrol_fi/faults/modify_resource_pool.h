/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_FLOWCONTROL_FI_MODIFY_RESOURCE_POOL_H
#define	_FLOWCONTROL_FI_MODIFY_RESOURCE_POOL_H

#pragma ident	"@(#)modify_resource_pool.h	1.10	08/05/20 SMI"

//
// Entry points for the functions used to
// change and restore the resource pool
// policies to make resource exhaustion
// easier.
//

#include <sys/os.h>
#include <orb/invo/common.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/fault/fault_injection.h>
#include <h/flowcontrol_fi.h>
#include <orbtest/flowcontrol_fi/impl_common.h>

//
// This is used to reduce the size of the resource
// pool's low, mod, high, and minimum values.
// It should be called from fault.data.
//
int 	change_resource_pool(int, char *[]);

//
// This is used to restore the original values
// of the resource policy. It should always
// be called in fault.data if change_resource_pool
// was used.
//
int		restore_resource_pool(int, char *[]);

#endif /* _FLOWCONTROL_FI_MODIFY_RESOURCE_POOL_H */
