/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)node_release.cc	1.14	08/05/20 SMI"

#ifndef _KERNEL
#include <unistd.h>
#endif

#include <stdio.h>
#include <orbtest/flowcontrol_fi/server_impl.h>
#include <sys/thread.h>
#include <sys/proc.h>
#include <orbtest/flowcontrol_fi/faults/node_release.h>
#include <orbtest/flowcontrol_fi/faults/thread_functions.h>
#include <orbtest/flowcontrol_fi/faults/entry_support.h>

//
// node_released_threads_needed
// Tests that threads are granted to a node with an outstanding
// request even if there wasn't a recall.
// Test makes invocations from node 2 until it requests and
// received a grant for additional threads. Then, node 3 makes
// invocations until it has a request for additional threads
// that is denied. Then, node 2 releases its invocations.
// (No recall is issued). Makes sure that node 3 gets additional
// threads.
//
int
node_released_threads_needed(int argc, char *argv[])
{
	os::printf("*** node_released_threads_needed\n");

#if defined(_FAULT_INJECTION)

	int	rslt = 0;

	if (argc != 2) {
		os::printf("FAIL: Not enough arguments\n");
		return (1);
	}
	int cv_num = os::atoi(argv[1]);

	//
	// Arm the trigger for PROCESS_RESOURCE_GRANT, so that we
	// stop making invocations when the server denies a request.
	//
	fault_flowcontrol::flowcontrol_arg_t fparg;
	fparg.op = fault_flowcontrol::SIGNAL_BOTH;
	fparg.cv_num = 1;
	NodeTriggers::add(FAULTNUM_FLOWCONTROL_PROCESS_RESOURCE_GRANT,
	    &fparg, (uint32_t)sizeof (fparg), TRIGGER_ALL_NODES);

	rslt = fork_blocked_invoke(cv_num, wait_until_release);

	//
	// Arm node trigger for GOT_THREADS, which will be hit after
	// the invocations from node 2 are released and granted to
	// this node.
	//
	fparg.cv_num = cv_num;
	fparg.op = fault_flowcontrol::SIGNAL_BOTH;
	NodeTriggers::add(FAULTNUM_FLOWCONTROL_GOT_THREADS, &fparg,
	    (uint32_t)sizeof (fparg), TRIGGER_THIS_NODE);

	//
	// Since fork_blocked_invoke stops when we get a grant of size 0,
	// the test is ready to release node 2's invocations, so signal
	// the release.
	//
	int fnum_node2;
	int fnum_node3;
	if (cv_num == 1) {
		fnum_node2 = FAULTNUM_FLOWCONTROL_SIGNAL_RELEASE_2;
		fnum_node3 = FAULTNUM_FLOWCONTROL_SIGNAL_RELEASE;
	} else {
		fnum_node2 = FAULTNUM_FLOWCONTROL_SIGNAL_RELEASE;
		fnum_node3 = FAULTNUM_FLOWCONTROL_SIGNAL_RELEASE_2;
	}
	NodeTriggers::add(fnum_node2, NULL, 0, TRIGGER_ALL_NODES);

	os::usecsleep(30 * 1000000);
	if (fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, NULL, 0)) {
		os::printf("FAIL: Invocations timed out without triggering "
		    "resource block\n");
		rslt = 1;
	} else if (fault_triggered(fnum_node3, NULL, 0)) {
		os::printf("PASS: Test successfully completed\n");
		rslt = 0;
	} else {
		os::printf("FAIL: Exited without signaling release\n");
		rslt = 1;
	}

	return (rslt);
#else
	argc, argv;	// To shut compiler warning
	return (no_fault_injection());
#endif
}

//
// node_request_filled_by_other_node
// Node 2 makes invocations until it gets additional threads.
// Node 3 makes invocations until the server runs out.
// Node 4 makes invocations until it asks for additional threads.
// Server recalls threads from node 3, but the threads are still
// blocked. Node 2 releases it's inovocations.
// Check that released resources are allocated to Node 4.
//
int
node_request_filled_by_other_node(int argc, char *argv[])
{
	os::printf("*** node_request_filled_by_other_node\n");
	argc, argv;

#if defined(_FAULT_INJECTION)

	int	rslt = 0;

	fault_flowcontrol::flowcontrol_arg_t fparg;

	//
	// Arm Node trigger to turn on FAULTNUM_FLOWCONTROL_GOT_THREADS
	// When server grants more resources, got_threads is called
	// This should signal condition variable 2, which is the one
	// the threads of the "hog" are waiting on.
	//
	fparg.op = fault_flowcontrol::SIGNAL_RELEASE;
	fparg.cv_num = 2;
	NodeTriggers::add(FAULTNUM_FLOWCONTROL_GOT_THREADS, &fparg,
	    (uint32_t)sizeof (fparg), TRIGGER_ALL_NODES);

	//
	// Now, arm node trigger in process_resource_recall, so that
	// when node_3 receives a recall, it will release the invocations
	// blocked from node 2. It will also cause this node to stop
	// issuing additional invocations
	//
	fparg.op = fault_flowcontrol::SIGNAL_BOTH;
	fparg.cv_num = 1;
	NodeTriggers::add(FAULTNUM_FLOWCONTROL_PROCESS_RESOURCE_RECALL,
	    &fparg, (uint32_t)sizeof (fparg), TRIGGER_ALL_NODES);

	rslt = fork_blocked_invoke(1, wait_until_release);

	os::usecsleep(30 * 1000000);
	if (fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, NULL, 0)) {
		os::printf("FAIL: Invocations timed out without triggering "
		    "resource block\n");
		rslt = 1;
	} else if (fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_RELEASE_2,
	    NULL, 0)) {
		os::printf("PASS: node_request_filled_by_other_node "
		    "successfully completed\n");
		rslt = 0;
	} else {
		os::printf("FAIL: Exited without release\n");
		rslt = 1;
	}

	return (rslt);
#else
	os::printf("FAIL: Fault injection not supported\n");
	return (no_fault_injection());
#endif

}

//
// This test sends a number of blocking two-way calls to the server, thus
// taking up a large number of resources from the resource pool.
// The client asks the server for more resources repeatedly. When the
// server responds with a resource grant of 0, the program exits, still
// holding the resources
// However, the client sets a fault point so that if the resources are
// ever recalled, the SIGNAL_RELEASE will be triggered
//

int
node_wait_until_release(int, char *[])
{
	os::printf("*** Node reboot wait until release.\n");

#if defined(_FAULT_INJECTION)

	int	rslt = 0;
	int 	i = 0;
	Environment e;

	fault_flowcontrol::flowcontrol_arg_t fparg;

	//
	// Arm Node trigger to stop issuing invocations when we have
	// a request to the server denied
	//
	fparg.op = fault_flowcontrol::SIGNAL_STOP;
	NodeTriggers::add(FAULTNUM_FLOWCONTROL_PROCESS_RESOURCE_GRANT,
	    &fparg, (uint32_t)sizeof (fparg), TRIGGER_THIS_NODE);

	//
	// Arm Node trigger to turn on FLOWCONTROL_PROCESS_RESOURCE_RECALL
	// fault_flowcontrol_signals for the release
	//
	fparg.op = fault_flowcontrol::SIGNAL_BOTH;
	NodeTriggers::add(FAULTNUM_FLOWCONTROL_PROCESS_RESOURCE_RECALL, &fparg,
	    (uint32_t)sizeof (fparg), TRIGGER_THIS_NODE);

	rslt = fork_blocked_invoke(1, wait_until_release);

	flowcontrol_fi::node_var	node_v;
	node_v = get_node();
	if (CORBA::is_nil(node_v)) {
		os::printf("FAIL: Could not get node reference\n");
		NodeTriggers::add(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, NULL,
		    0, TRIGGER_ALL_NODES);
		return (1);
	}

	//
	// We want the invoke test to stop when it sees
	// FAULTNUM_FLOWCONTROL_SIGNAL_STOP,
	// so we have to clear STOP before invoking the node
	//
	NodeTriggers::clear(FAULTNUM_FLOWCONTROL_PROCESS_RESOURCE_GRANT);
	NodeTriggers::clear(FAULTNUM_FLOWCONTROL_SIGNAL_STOP,
	    TRIGGER_ALL_NODES);

	flowcontrol_fi::node_test_op op = flowcontrol_fi::GET_THREADS;
	node_v->invoke_test(op, e);

	// Verify the call succeeded.
	if (check_exception(e.exception(), "FAIL:")) {
		os::printf("FAIL: invocation returned exception. "
		    "Expected to succeed\n");
		NodeTriggers::add(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, NULL, 0);
		rslt = 1;
	}

	if (fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, NULL, 0)) {
		os::printf("FAIL: Invocations timed out without triggering "
		    "resource block\n");
		rslt = 1;
	}
	else
	{
		int timeout_seconds = 15;
		int timeout_iterations = 40;
		i = 0;
		while (i < timeout_iterations) {
			if (node_v->get_status(e) == FAIL_STATUS) {
				os::usecsleep(timeout_seconds * 1000000);
				i++;
			} else {
				break;
			}
		}
		rslt += node_v->get_status(e);
		if (rslt == 0)
			os::printf("PASS: Test successfully completed\n");
		else
			os::printf("FAIL: Get_status returned %d\n", rslt);
	}

	return (rslt);
#else
	return (no_fault_injection());
#endif

}

//
// This tests the code path when a client uses many threads and then
// releases them. The client unblocks invocations when the server
// sends a grant for additional threads. The test checks that the
// resources are eventually released back to the server.
//

int
node_wait_until_resource_release(int, char *[])
{
	os::printf("*** Node wait until resource release\n");

#if defined(_FAULT_INJECTION)

	int	rslt = 0;
	int max_wait = 10;
	fault_flowcontrol::flowcontrol_arg_t fparg;

	//
	// Set up Node Trigger for got threads
	// When we get additional threads from the server, the fault
	// will trigger the cv release
	//
	fparg.op = fault_flowcontrol::SIGNAL_BOTH;
	NodeTriggers::add(FAULTNUM_FLOWCONTROL_GOT_THREADS, &fparg,
	    (uint32_t)sizeof (fparg), TRIGGER_THIS_NODE);

	//
	// check_for_resource_changes_1 is hit when the client has
	// resources to release to the server.
	//
	fparg.op = fault_flowcontrol::CHECK_FOR_RESOURCE_CHANGES_1;
	NodeTriggers::add(FAULTNUM_FLOWCONTROL_CHECK_FOR_RESOURCE_CHANGES_1,
	    &fparg, (uint32_t)sizeof (fparg), TRIGGER_THIS_NODE);

	//
	// check_release could also be hit when the client has
	// resources to release to the server.
	//
	fparg.op = fault_flowcontrol::CHECK_FOR_RESOURCE_CHANGES_1;
	NodeTriggers::add(FAULTNUM_FLOWCONTROL_CHECK_RELEASE,
	    &fparg, (uint32_t)sizeof (fparg), TRIGGER_THIS_NODE);

	rslt = fork_blocked_invoke(1, wait_until_release);

	if (fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, NULL, 0)) {
		os::printf("FAIL: Invocations timed out without triggering "
		    "resource block\n");
		rslt += 1;
	}

	//
	// We want to wait to make sure that the invocations are released.
	// Loop here until the faultpoint is triggered, and timeout and
	// abort after max_wait loops.
	//
	int i = 0;
	while (!fault_triggered(
	    FAULTNUM_FLOWCONTROL_CHECK_FOR_RESOURCE_CHANGES_1, NULL, 0)) {
		os::usecsleep(30 * 1000000);
		i++;
		if (i > max_wait) break;
	}

	if (i > max_wait) {
		os::printf("FAIL: Client never released threads to server\n");
		return (1);
	} else {
		os::printf("PASS: Client got additional threads "
		    "and then released them\n");
		rslt += 0;
	}

	return (rslt);
#else
	return (no_fault_injection());
#endif

}

//
// This test sends a number of blocking two-way calls to the server, thus
// taking up a large number of resources from the resource pool.
// The client asks the server for more resources repeatedly. When the
// server responds with a resource grant of 0, the program exits, still
// holding the resources
// However, the client sets a fault point so that if the resources are
// ever recalled, the SIGNAL_RELEASE will be triggered
//

int
node_partial_release(int, char *[])
{
	os::printf("*** Node partial release.\n");

#if defined(_FAULT_INJECTION)

	int	rslt = 0;
	int 	i = 0;
	Environment e;

	fault_flowcontrol::flowcontrol_arg_t fparg;

	//
	// Arm Node trigger to turn on FLOWCONTROL_PROCESS_RESOURCE_RECALL
	// fault_flowcontrol_signals for the release
	//
	fparg.op = fault_flowcontrol::SIGNAL_BOTH;
	NodeTriggers::add(FAULTNUM_FLOWCONTROL_PROCESS_RESOURCE_RECALL, &fparg,
	    (uint32_t)sizeof (fparg), TRIGGER_THIS_NODE);

	//
	// Ask the server what the maximum number of threads available is.
	// We do this in the fault_flowcontrol because that is loaded in
	// the kernel.
	//
	int high, mod, low, inc;
	flowcontrol_fi::server_var	server_v;
	server_v = get_server();
	if (CORBA::is_nil(server_v)) {
		os::printf("FAIL:Could not get server reference\n");
		return (1);
	}

	server_v->get_pool_info(low, mod, high, inc, e);

	// Verify the call succeeded.
	if (check_exception(e.exception(), "FAIL:")) {
		os::printf("FAIL: invocation returned exception. "
		    "Expected to succeed\n");
		return (1);
	}

	rslt = fork_n_blocked_invoke(high-low-(inc/2), 1, wait_until_release);

	flowcontrol_fi::node_var	node_v;
	node_v = get_node();
	if (CORBA::is_nil(node_v)) {
		os::printf("FAIL: Could not get node reference\n");
		NodeTriggers::add(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, NULL,
		    0, TRIGGER_ALL_NODES);
		return (1);
	}

	//
	// We want the invoke test to stop when it sees
	// FAULTNUM_FLOWCONTROL_SIGNAL_STOP,
	// so we have to clear STOP before invoking the node
	//
	NodeTriggers::clear(FAULTNUM_FLOWCONTROL_PROCESS_RESOURCE_GRANT);
	NodeTriggers::clear(FAULTNUM_FLOWCONTROL_SIGNAL_STOP,
	    TRIGGER_ALL_NODES);

	flowcontrol_fi::node_test_op	op = flowcontrol_fi::GET_THREADS;
	node_v->invoke_test(op, e);

	// Verify the call succeeded.
	if (check_exception(e.exception(), "FAIL:")) {
		os::printf("FAIL: invocation returned exception. "
		    "Expected to succeed\n");
		NodeTriggers::add(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, NULL, 0);
	}

	if (fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, NULL, 0)) {
		os::printf("FAIL: Invocations timed out without triggering "
		    "resource block\n");
		rslt = 1;
	} else {
		int timeout_seconds = 15;
		int timeout_iterations = 40;
		i = 0;
		while (i < timeout_iterations) {
			if (node_v->get_status(e) == FAIL_STATUS) {
				os::usecsleep(timeout_seconds * 1000000);
				i++;
			} else {
				break;
			}
		}
		rslt = node_v->get_status(e);
		if (rslt == 0)
			os::printf("PASS: Test successfully completed\n");
		else
			os::printf("FAIL: Get_result returned %d\n", rslt);
	}

	return (rslt);
#else
	return (no_fault_injection());
#endif
}
