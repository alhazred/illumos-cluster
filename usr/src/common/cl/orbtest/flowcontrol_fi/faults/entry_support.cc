/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)entry_support.cc	1.24	08/05/20 SMI"

//
// Entry function support routines.
//

#include <orb/infrastructure/orb_conf.h>
#include <orbtest/flowcontrol_fi/faults/entry_support.h>

//
// Check for exceptions.
// Returns :
//	True if there IS an exception, false otherwise.
//
bool
check_exception(CORBA::Exception *ex, char *prefix)
{
	flowcontrol_fi::Errmsg	*testex;

	if (ex == NULL) {
		return (false);		// no exception
	}

	if (testex = flowcontrol_fi::Errmsg::_exnarrow(ex)) {
		// Print the message shipped with the exception.
		os::printf(testex->msg);
	} else {
		ex->print_exception(prefix);
	}

	return (true);
}

//
// Get a reference to the server if it exists
// Returns:
//  Reference to the server if successful, _nil() otherwise.
//

flowcontrol_fi::server_ptr
get_server()
{
	CORBA::Object_var   obj_v;
	flowcontrol_fi::server_ptr  server_p;

	obj_v = impl_common::get_obj(SERVER);
	if (CORBA::is_nil(obj_v)) {
		os::printf("FAIL: Could not get CORBA object\n");
		return (flowcontrol_fi::server::_nil());
	}

	server_p = flowcontrol_fi::server::_narrow(obj_v);
	if (CORBA::is_nil(server_p)) {
		os::printf("ERROR: Can't narrow to flowcontrol_fi::server\n");
		return (flowcontrol_fi::server::_nil());
	}

	return (server_p);
}

//
// Get a reference to flowcontrol_fi::node from the name server.
// Returns :
//	Reference to the node if successful, _nil() otherwise.
//
flowcontrol_fi::node_ptr
get_node()
{
	CORBA::Object_var	obj_v;
	flowcontrol_fi::node_ptr	node_p;

	obj_v = impl_common::get_obj(NODE);
	if (CORBA::is_nil(obj_v)) {
		os::printf("FAIL: Could not get CORBA object\n");
		return (flowcontrol_fi::node::_nil());
	}

	node_p = flowcontrol_fi::node::_narrow(obj_v);
	if (CORBA::is_nil(node_p)) {
		os::printf("ERROR: Can't narrow to flowcontrol_fi::node\n");
		return (flowcontrol_fi::node::_nil());
	}
	return (node_p);
}

//
// Entry function to tell server test is finished so it can unbind itself
// from the name server and exit.
// Return:
//	0	SUCCESS
//	1	FAIL
//
int
server_done(int, char *[])
{
#if defined(_FAULT_INJECTION)
	flowcontrol_fi::server_var	server_v;
	Environment		e;

	// Get a reference to the server
	server_v = get_server();
	if (CORBA::is_nil(server_v)) {
		os::printf("ERROR: The server is already gone\n");
		return (1);
	}

	// Tell server we're done with test.
	server_v->done(e);
	if (check_exception(e.exception(), "ERROR:")) {
		os::printf("ERROR: can't invoke server->done()\n");
		return (1);
	}

	// Clear all fault points we may have used
	NodeTriggers::clear_all();

	os::printf("PASS: server done invoked\n");
	return (0);
#else
	return (no_fault_injection());
#endif
}

int
node_done(int, char *[])
{
#if defined(_FAULT_INJECTION)
	flowcontrol_fi::node_var	node_v;
	Environment		e;

	// Get a reference to thenode
	node_v = get_node();
	if (CORBA::is_nil(node_v)) {
		os::printf("ERROR: The node is already gone\n");
		return (1);
	}

	// Tell node we're done with test.
	node_v->done(e);
	if (check_exception(e.exception(), "ERROR:")) {
		os::printf("ERROR: can't invoke node->done()\n");
		return (1);
	}

	// Clear all fault points we may have used
	NodeTriggers::clear_all();

	os::printf("PASS: node done invoked\n");
	return (0);
#else
	return (no_fault_injection());
#endif
}

//
// Clears all NodeTriggers on the node on which it runs.
//
int
clear_all(int, char *[])
{
#if defined(_FAULT_INJECTION)

	NodeTriggers::clear_all();

	os::printf("PASS: All Triggers Cleared\n");
	return (0);
#else
	return (no_fault_injection());
#endif
}

//
// release_all
// This is called after each test to make sure that the invocations
// are released and that the servers can be unloaded.
//
int
release_all(int, char *[])
{
#if defined(_FAULT_INJECTION)
	NodeTriggers::add(FAULTNUM_FLOWCONTROL_SIGNAL_RELEASE, NULL,
	    0, TRIGGER_ALL_NODES);
	NodeTriggers::add(FAULTNUM_FLOWCONTROL_SIGNAL_RELEASE_2, NULL,
	    0, TRIGGER_ALL_NODES);

	os::printf("PASS: All invocations released\n");
	return (0);
#else
	return (no_fault_injection());
#endif
}
