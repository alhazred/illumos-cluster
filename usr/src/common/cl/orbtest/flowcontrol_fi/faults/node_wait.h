/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_FLOWCONTROL_FI_NODE_WAIT_H
#define	_FLOWCONTROL_FI_NODE_WAIT_H

#pragma ident	"@(#)node_wait.h	1.9	08/05/20 SMI"

//
// Entry points for tests that perform some action
// and then wait to see if particular code paths
// have been hit.
//

#include <sys/os.h>
#include <orb/invo/common.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/fault/fault_injection.h>
#include <h/flowcontrol_fi.h>
#include <orbtest/flowcontrol_fi/impl_common.h>

//
// Test forks invocations until it requests additional
// resources from the server. When it gets more
// resources, it unblocks the invocations and returns.
//
int		node_wait_until_got_threads(int, char *[]);

//
// Test forks invocations until it requests additional
// resources and the request is denied. This happens
// when the server has exhausted its resources.
//
int		node_wait_until_request_denied(int, char *[]);

//
// Test waits until a thread used by a previous function
// (node_die_during_invo) is reused by one of the invocations
// this function forks. It then unblocks the invocations and
// returns.
//
int 	node_wait_until_thread_reused(int argc, char *argv[]);

#endif /* _FLOWCONTROL_FI_NODE_WAIT_H */
