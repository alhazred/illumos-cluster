/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_FLOWCONTROL_FI_NODE_JOIN_H
#define	_FLOWCONTROL_FI_NODE_JOIN_H

#pragma ident	"@(#)node_join.h	1.10	08/05/20 SMI"

//
// Entry points for tests that deal with the flow of
// resources during the time that a node is joining
// (or rejoining) a cluster.
//

#include <sys/os.h>
#include <orb/invo/common.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/fault/fault_injection.h>
#include <h/flowcontrol_fi.h>
#include <orbtest/flowcontrol_fi/impl_common.h>

//
// Tests that a joining node gets resources if
// there are some available on the server.
//
int		node_join_resources_available(int, char *[]);

//
// Tests that if a node joins when there are no
// resources available, a resource recall is
// issued to the node holding the most resources.
//
int		node_join_resources_unavailable(int, char *[]);

#endif /* _FLOWCONTROL_FI_NODE_JOIN_H */
