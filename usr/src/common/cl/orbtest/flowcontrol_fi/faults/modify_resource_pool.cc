/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)modify_resource_pool.cc	1.11	08/05/20 SMI"

#ifndef _KERNEL
#include <unistd.h>
#endif

#include <stdio.h>
#include <sys/thread.h>
#include <sys/proc.h>
#include <orbtest/flowcontrol_fi/faults/modify_resource_pool.h>
#include <orbtest/flowcontrol_fi/faults/entry_support.h>

//
// change_resource_pool
//
// Sets up the fault point on the server which will reduce the size
// of the server's resource pool.
// Used in many tests to make resource exhaustion faster.
//

int
change_resource_pool(int, char *[])
{
	int32_t						ret;
	Environment					e;
	flowcontrol_fi::server_var	server_v;

	// Get a reference to the server
	server_v = get_server();
	if (CORBA::is_nil(server_v)) {
		os::printf("FAIL: Invocation could not get server refernce\n");
		return (1);
	}

	server_v->setup_pool_values(ret, e);
	if (ret) {
		os::printf("FAIL: Could not change resource pool values\n");
	} else {
		os::printf("PASS: Changed resource pool values on server\n");
	}

	return (ret);
}

//
// restore_resource_pool
//
// Hits the fault point on the server which will restore the size
// of the server's resource pool after a previous call to change_resoure
// pool. In that fault function, the fault point, RESTORE_RESOURCE_POOL
// was triggered and the argument structure set up with the original
// pool values.
//

int
restore_resource_pool(int, char *[])
{
	int32_t						ret;
	Environment					e;
	flowcontrol_fi::server_var	server_v;

	// Get a reference to the server
	server_v = get_server();
	if (CORBA::is_nil(server_v)) {
		os::printf("FAIL: Invocation could not get server refernce\n");
		return (1);
	}

	server_v->restore_pool_values(ret, e);
	if (ret) {
		os::printf("FAIL: Could not change resource pool values\n");
	} else {
		os::printf("PASS: Changed resource pool values on server\n");
	}

	return (ret);
}
