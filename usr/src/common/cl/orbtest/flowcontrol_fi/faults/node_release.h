/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_FLOWCONTROL_FI_NODE_RELEASE_H
#define	_FLOWCONTROL_FI_NODE_RELEASE_H

#pragma ident	"@(#)node_release.h	1.9	08/05/20 SMI"

//
// Entry point for tests which deal with what happens
// after resources are released to the server.
//

#include <sys/os.h>
#include <orb/invo/common.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/fault/fault_injection.h>
#include <h/flowcontrol_fi.h>
#include <orbtest/flowcontrol_fi/impl_common.h>

//
// Test makes invocations until node requests and is
// granted additional resources. Then, unblocks
// invocations and waits until the node sends a
// release back to server.
//
int		node_wait_until_resource_release(int, char *[]);

//
// Test makes invocations until the server's resources
// are exhausted. Then, another node makes invocations
// until it asks the server for more resources. When the
// server recalls the threads from this node, the invocations
// unblock and the test returns.
int		node_wait_until_release(int, char *[]);

//
// Test holds all resources, but is not using some. (less
// than a full increment). Checks that when a recall causes
// a release of these, there is no problem.
//
int 	node_partial_release(int, char *argv[]);

//
// Tests that if a node releases resources, and another node
// had an outstanding request, the released resources will
// be used to satisfy that request, even if this node was
// not issued a recall.
//
int 	node_request_filled_by_other_node(int argc, char *argv[]);

//
// Tests that if threads are released when there is an outstanding
// request that was denied, the threads are given to the denied
// node.
//
int 	node_released_threads_needed(int argc, char *argv[]);

#endif /* _FLOWCONTROL_FI_NODE_RELEASE_H */
