/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)node_join.cc	1.15	08/05/20 SMI"

#ifndef _KERNEL
#include <unistd.h>
#endif

#include <stdio.h>
#include <sys/thread.h>
#include <sys/proc.h>
#include <orb/flow/resource_balancer.h>
#include <orbtest/flowcontrol_fi/faults/node_join.h>
#include <orbtest/flowcontrol_fi/faults/thread_functions.h>
#include <orbtest/flowcontrol_fi/faults/entry_support.h>

//
// Node join resources available
//
// This test starts on node 2. It causes node 3 to reboot.
// When node 3 rejoins the cluster, there should be resources
// available, and it should hit the FLOWCONTROL_PROCESS_REQUEST_
// RESOURCES_1 faultpoint.
//

int
node_join_resources_available(int, char *[])
{
	os::printf("*** Node joining when resources are available.\n");

#if defined(_FAULT_INJECTION)

	int	rslt = 0;
	int 	i = 0;
	Environment e;
	int	num_wait = 20;

	fault_flowcontrol::flowcontrol_arg_t fparg;

	//
	// Arm trigger to send signal if we hit the process_resource_request
	// code path. Path is hit on server, so we must trigger all nodes.
	//
	fparg.op = fault_flowcontrol::PROCESS_REQUEST_RESOURCES_1;
	NodeTriggers::add(FAULTNUM_FLOWCONTROL_PROCESS_REQUEST_RESOURCES_1,
	    &fparg, (uint32_t)sizeof (fparg), TRIGGER_ALL_NODES);

	flowcontrol_fi::node_var	node_v;
	node_v = get_node();
	if (CORBA::is_nil(node_v)) {
		os::printf("FAIL: Could not get node reference\n");
		return (1);
	}

	// Tell the node to reboot itself
	flowcontrol_fi::node_test_op op = flowcontrol_fi::REBOOT;
	node_v->invoke_test(op, e);

	// Verify the call succeeded.
	if (check_exception(e.exception(), "FAIL:")) {
		os::printf("FAIL: invocation returned exception. "
		    "Expected to succeed\n");
		return (1);
	}

	while ((!fault_triggered(FAULTNUM_FLOWCONTROL_S_PRR_1, 0, NULL)) &&
	    (!fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, 0, NULL)) &&
	    (i < num_wait)) {
		os::usecsleep(30 * 1000000);
	}

	if (i == num_wait) {
		NodeTriggers::add(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, 0, 0,
		    TRIGGER_ALL_NODES);
		os::printf("FAIL: Never triggered signal\n");
		rslt += 1;
	} else {
		os::printf("PASS: Test successfully completed\n");
		rslt += 0;
	}

	return (rslt);
#else
	return (no_fault_injection());
#endif
}

//
// node_join_resources_unavailable
//
// This test starts on node 2. First, it makes requests for "high" threads.
// Then, it causes node 3 to reboot.
// When node 3 rejoins the cluster, there should be no resources
// available, and it should trigger a resource recall. The resource recall
// will cause the invocations to return and the test to pass.
//

int
node_join_resources_unavailable(int, char *[])
{
	os::printf("*** Node joining when resources are unavailable.\n");

#if defined(_FAULT_INJECTION)

	int	rslt = 0;
	int 	i = 0;
	Environment e;
	int num_wait = 30;

	fault_flowcontrol::flowcontrol_arg_t fparg;

	//
	// Arm trigger to send signal if we hit the process_resource_recall
	// code path. Path is hit on server, so we must trigger all nodes.
	//
	fparg.op = fault_flowcontrol::SIGNAL_RELEASE;
	NodeTriggers::add(FAULTNUM_FLOWCONTROL_PROCESS_RESOURCE_RECALL,
	    &fparg, (uint32_t)sizeof (fparg), TRIGGER_ALL_NODES);

	flowcontrol_fi::node_var	node_v;
	node_v = get_node();
	if (CORBA::is_nil(node_v)) {
		os::printf("FAIL: Could not get node reference\n");
		return (1);
	}

	//
	// Ask the server what the maximum number of threads available is.
	// We do this in the fault_flowcontrol because that is loaded in
	// the kernel.
	//
	int high, mod, low, inc;
	flowcontrol_fi::server_var	server_v;
	server_v = get_server();
	if (CORBA::is_nil(server_v)) {
		os::printf("FAIL:Could not get server reference\n");
		return (1);
	}

	server_v->get_pool_info(low, mod, high, inc, e);

	// Verify the call succeeded.
	if (check_exception(e.exception(), "FAIL:")) {
		os::printf("FAIL: invocation returned exception. "
		    "Expected to succeed\n");
		return (1);
	}

	rslt = fork_n_blocked_invoke(high + inc, 1, wait_until_release);

	// Tell the node to reboot itself
	flowcontrol_fi::node_test_op op = flowcontrol_fi::REBOOT;
	node_v->invoke_test(op, e);

	// Verify the call succeeded.
	if (check_exception(e.exception(), "FAIL:")) {
		os::printf("FAIL: invocation returned exception. "
		    "Expected to succeed\n");
		return (1);
	}

	while ((!fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, 0, NULL)) &&
	    (!fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_RELEASE, 0, NULL)) &&
		(i < num_wait)) {
		i++;
		os::usecsleep(10 * 10000000);
	}

	if (i == num_wait) {
		NodeTriggers::add(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, 0, 0,
		    TRIGGER_ALL_NODES);
		os::printf("FAIL: Test never released invocations, abort\n");
		return (1);
	}

	if (fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, 0, NULL)) {
		os::printf("FAIL: Test was aborted\n");
		return (1);
	} else {
		os::printf("PASS: Test successfully completed\n");
		rslt += 0;
	}

	return (rslt);
#else
	return (no_fault_injection());
#endif
}
