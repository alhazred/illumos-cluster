/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_FLOWCONTROL_FI_NODE_DEATH_H
#define	_FLOWCONTROL_FI_NODE_DEATH_H

#pragma ident	"@(#)node_death.h	1.10	08/05/20 SMI"

//
// Entry points for the various tests which either kill or
// reboot nodes
//

#include <sys/os.h>
#include <orb/invo/common.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/fault/fault_injection.h>
#include <h/flowcontrol_fi.h>
#include <orbtest/flowcontrol_fi/impl_common.h>

//
// This is used to test that resources held by a dead
// node are reallocated to another node if they are
// needed.
//
int		node_die_resources_needed(int, char *[]);

//
// This tests that resources held by a dead node are
// destroyed if there is no need for them.
//
int		node_die_resources_unneeded(int, char *[]);

//
// This tests that a thread processing the invocation
// of a dead node is reused after the node reconfiguration.
//
int 	node_die_during_invo(int argc, char *argv[]);

//
// This tests that a rebooting node releases its resources
// during node reconfiguration.
//
int		node_reboot_request_denied(int, char *[]);

//
// This is a simple helper function that reboots the current
// node. It is called as a CLIENT (in fault.data) and as a
// invocation for flowcontrol_fi_node
//
int		node_reboot(int, char*[]);

#endif /* _FLOWCONTROL_FI_NODE_DEATH_H */
