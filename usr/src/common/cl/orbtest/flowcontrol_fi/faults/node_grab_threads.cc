/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)node_grab_threads.cc	1.11	08/05/20 SMI"

#ifndef _KERNEL
#include <unistd.h>
#endif

#include <stdio.h>
#include <orbtest/flowcontrol_fi/server_impl.h>
#include <sys/thread.h>
#include <sys/proc.h>
#include <orbtest/flowcontrol_fi/faults/node_grab_threads.h>
#include <orbtest/flowcontrol_fi/faults/entry_support.h>

//
// node_grab_threads
// This is a helper function used by the various
// node_grab_threads_until_* functions. It takes
// two arguments. The first is a fault_point, and
// sets up the fault point to stop forking threads
// when that fault is hit. It then calls fork_blocked_invoke
// which makes the required number of invocations.
// The second argument is the number of the condition
// variable on which the invocations block. This is
// passed to fork_blocked_invoke.
// It returns without releasing the invocations.
//
int
node_grab_threads(int fnum, int cv_num)
{
#if defined(_FAULT_INJECTION)

	int	rslt = true;
	fault_flowcontrol::flowcontrol_arg_t fparg;

	//
	// Arm Node trigger to turn on fnum, the fault number
	// that we were passed. When the server hits that
	// code path, it will trigger SIGNAL_STOP, and
	// fork_blocked_invoke, which we call later, will stop
	// forking invocations.
	//
	fparg.op = fault_flowcontrol::SIGNAL_STOP;
	NodeTriggers::add(fnum, &fparg,
	    (uint32_t)sizeof (fparg), TRIGGER_ALL_NODES);

	rslt = fork_blocked_invoke(cv_num, wait_until_release);

	if (fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, NULL, 0)) {
		os::printf("FAIL: Invocations timed out without triggering "
		    "resource block\n");
		rslt = 1;
	} else {
		os::printf("PASS: node_grab_threads successfully completed\n");
		rslt = 0;
	}

	//
	// Clear the stop trigger so that others can run after this function
	// without leftover state.
	//
	NodeTriggers::clear(fnum, TRIGGER_ALL_NODES);

	return (rslt);
#else
	fnum, cv_num;		// To shut compiler warning
	return (no_fault_injection());
#endif
}

//
// node_grab_threads_until_got_threads
// This function calls node_grab_threads with the fault number
// for GOT_THREADS. So, when node_grab_threads returns, this
// client will have made enough invocations to have asked the
// server for more threads and received more threads. At this
// point, the function exits without releasing the invocations.
// The invocations must be released by another client. The
// function takes a single argument, which is the condition
// variable for the invocations to block on. See server_impl.cc
// for more information.
//
int
node_grab_threads_until_got_threads(int argc, char *argv[])
{
	os::printf("*** Node_grab_threads_until_got_threads.\n");
#if	defined(_FAULT_INJECTION)

	if (argc != 2) {
		os::printf("FAIL: Not given enough arguments\n");
		return (1);
	}

	int cv_num = os::atoi(argv[1]);
	return (node_grab_threads(FAULTNUM_FLOWCONTROL_GOT_THREADS,
	    cv_num));
#else
	argc, argv;	// To shut compiler warning
	return (no_fault_injection());
#endif // _FAULT_INJECTION);
}

//
// node_grab_threads_until_request_denied
// This function calls node_grab_threads with the fault number
// for PROCESS_RESOURCE_GRANT. So, when node_grab_threads returns, this
// client will have made enough invocations to have made a request
// to the server that the server has denied (send_grant of 0).
// At this point, the client returns without releasing the
// invocations. The invocations must be released by another client.
// The function takes a single integral argument which is the
// condition variable the invocations should block on. See
// server_impl.cc for more information.
//
int
node_grab_threads_until_request_denied(int argc, char *argv[])
{
	os::printf("*** Node_grab_threads_until_request_denied\n");
#if defined(_FAULT_INJECTION)

	if (argc != 2) {
		os::printf("FAIL: Not given enough arguments\n");
		return (1);
	}

	int cv_num = os::atoi(argv[1]);
	return (node_grab_threads(FAULTNUM_FLOWCONTROL_PROCESS_RESOURCE_GRANT,
	    cv_num));
#else
	argc, argv;	// To shut compiler warning
	return (no_fault_injection());
#endif // _FAULT_INJECTION);
}

//
// grab_threads_until_stop
//
// This function is invoked by a client calling invoke_test on a
// flowcontrol_fi node.
//
// It takes a single argument, which is a string containing a fault
// number. It triggers that fault number, giving it the op,
// SIGNAL_STOP. It then calls fork_blocked_invoke. This keeps forking
// threads until the triggered fault point is hit.
//

int
grab_threads_until_stop(int argc, char *argv[])
{
#if defined(_FAULT_INJECTION)

	int	rslt = 0;
	Environment e;

	if (argc != 1) {
		os::printf("FAIL: grab_threads_until_stop not given correct "
		    "number of arguments\n");
		return (1);
	}

	fault_flowcontrol::flowcontrol_arg_t fparg;

	//
	// Arm Node trigger to turn on the fault number that was passed to
	// the function. When the code path hits that fault, it will signal
	// for forked_blocked_invoke to stop forking threads.
	//
	fparg.op = fault_flowcontrol::SIGNAL_STOP;
	NodeTriggers::add(os::atoi(argv[0]), &fparg,
	    (uint32_t)sizeof (fparg), TRIGGER_THIS_NODE);

	rslt = fork_blocked_invoke(1, wait_until_release);

	return (rslt);
#else
	argc, argv;		// To shut compiler warning
	return (no_fault_injection());
#endif

}
