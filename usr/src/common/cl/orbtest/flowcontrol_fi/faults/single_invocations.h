/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_FLOWCONTROL_FI_SINGLE_INVOCATIONS_H
#define	_FLOWCONTROL_FI_SINGLE_INVOCATIONS_H

#pragma ident	"@(#)single_invocations.h	1.9	08/05/20 SMI"

//
// Entry points for the tests which only issue
// a single, usually non-blocking, invocation.
//

#include <sys/os.h>
#include <orb/invo/common.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/fault/fault_injection.h>
#include <h/flowcontrol_fi.h>
#include <orbtest/flowcontrol_fi/impl_common.h>

//
// Issues a single twoway invocation and checks to
// make sure that the node hit the resources_available
// code path.
//
int		simple_invoke_1(int, char *[]);

//
// Issues a single oneway invocation and checks to
// make sure that the node hit the resources_available
// code path.
//
int		oneway_invoke(int, char *[]);

#endif /* _FLOWCONTROL_FI_SINGLE_INVOCATIONS_H */
