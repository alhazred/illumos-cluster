/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)node_death.cc	1.18	08/05/20 SMI"

#ifndef _KERNEL
#include <unistd.h>
#endif

#include <stdio.h>
#include <sys/thread.h>
#include <sys/proc.h>
#include <orb/flow/resource_balancer.h>
#include <orbtest/flowcontrol_fi/faults/node_death.h>
#include <orbtest/flowcontrol_fi/faults/entry_support.h>

//
// node_die_resources_needed
//
// This tests starts on node 2. It queries the server for the maximum
// threads it can obtain. It then makes a number of invocations that
// exceeds that number of threads.
//
// At this point, the node will be making resource requests to the
// server periodically, and all will be denied.
//
// Now, the test sets the release fault trigger to be activated when
// the node receives additional resources. This should only happen
// when the other client node dies.
//
// Finally, the test reboots the other node. The server should grant
// additional resources, the invocations will be released, and then
// the test returns pass.
//

int
node_die_resources_needed(int, char *[])
{
	os::printf("*** Node death when resources are needed\n");

#if defined(_FAULT_INJECTION)

	int	rslt = 0;
	int i = 0;
	Environment e;
	int num_wait = 300;

	flowcontrol_fi::node_var	node_v;
	node_v = get_node();
	if (CORBA::is_nil(node_v)) {
		os::printf("FAIL: Could not get node reference\n");
		return (1);
	}

	//
	// Ask the server what the maximum number of threads available is.
	// We do this in the fault_flowcontrol because that is loaded in
	// the kernel.
	//
	int high, mod, low, inc;
	flowcontrol_fi::server_var	server_v;
	server_v = get_server();
	if (CORBA::is_nil(server_v)) {
		os::printf("FAIL:Could not get server reference\n");
		return (1);
	}

	server_v->get_pool_info(low, mod, high, inc, e);

	// Verify the call succeeded.
	if (check_exception(e.exception(), "FAIL:")) {
		os::printf("FAIL: invocation returned exception. "
		    "Expected to succeed\n");
		return (1);
	}

	rslt = fork_n_blocked_invoke(high + inc, 1, wait_until_release);

	//
	// Arm trigger to send signal if we hit the got_threads
	// code path.
	//
	fault_flowcontrol::flowcontrol_arg_t fparg;
	fparg.op = fault_flowcontrol::SIGNAL_RELEASE;
	NodeTriggers::add(FAULTNUM_FLOWCONTROL_GOT_THREADS,
	    &fparg, (uint32_t)sizeof (fparg), TRIGGER_THIS_NODE);

	//
	// Tell the other node to reboot itself
	//
	flowcontrol_fi::node_test_op	op = flowcontrol_fi::REBOOT;
	node_v->invoke_test(op, e);

	// Verify the call succeeded.
	if (check_exception(e.exception(), "FAIL:")) {
		os::printf("FAIL: invocation returned exception. "
		    "Expected to succeed\n");
		return (1);
	}

	while ((!fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, 0, NULL)) &&
	    (!fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_RELEASE, 0, NULL)) &&
		(i < num_wait)) {
		i++;
		os::usecsleep(30 * 1000000);
	}

	if (i == num_wait) {
		NodeTriggers::add(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, 0, 0,
		    TRIGGER_ALL_NODES);
		os::printf("FAIL: Test never released invocations, abort\n");
	}

	if (fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, 0, NULL)) {
		os::printf("FAIL: Test was aborted\n");
		rslt += 1;
	} else {
		os::printf("PASS: Test successfully completed\n");
		rslt += 0;
	}

	return (rslt);
#else
	return (no_fault_injection());
#endif
}

//
// node_die_resources_unneeded
//
// This test starts on node 1, which is also the server node.
//
// Before the test is run, node 2 has already acquired a large
// number of threads.
//
// Frist, it triggers the FLOWCONTROL_DESTROY_THREADS fault point.
// This is in the code path that destroys unneeded threads during
// node reconfiguration step 2.
//
// The test then causes node 2 to reboot. This should hit the code
// path and fault point described above. When it is hit, the fault
// point triggers the FLOWCONTROL_SIGNAL_RELEASE fault point.
//
// The test waits to see if the SIGNAL_RELEASE fault point is hit.
// If it is, the test returns pass, if it is not, the test returns
// fail.
//

int
node_die_resources_unneeded(int, char *[])
{
	os::printf("*** Node death when resources are unneeded\n");

#if defined(_FAULT_INJECTION)

	int	rslt = 0;
	int 	i = 0;
	Environment e;
	int num_wait = 30;

	fault_flowcontrol::flowcontrol_arg_t fparg;

	//
	// Arm trigger to send signal if we hit the destroy threads
	// fault point.
	//
	fparg.op = fault_flowcontrol::SIGNAL_RELEASE;
	NodeTriggers::add(FAULTNUM_FLOWCONTROL_DESTROY_THREADS,
	    &fparg, (uint32_t)sizeof (fparg), TRIGGER_THIS_NODE);

	flowcontrol_fi::node_var	node_v;
	node_v = get_node();
	if (CORBA::is_nil(node_v)) {
		os::printf("FAIL: Could not get node reference\n");
		return (1);
	}

	//
	// Tell the other node to reboot itself
	//
	flowcontrol_fi::node_test_op	op = flowcontrol_fi::REBOOT;
	node_v->invoke_test(op, e);

	// Verify the call succeeded.
	if (check_exception(e.exception(), "FAIL:")) {
		os::printf("FAIL: invocation returned exception. "
		    "Expected to succeed\n");
		return (1);
	}

	while ((!fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, 0, NULL)) &&
	    (!fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_RELEASE, 0, NULL)) &&
		(i < num_wait)) {
		i++;
		os::usecsleep(30 * 1000000);
	}

	if (i == num_wait) {
		NodeTriggers::add(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, 0, 0,
		    TRIGGER_ALL_NODES);
		os::printf("FAIL: Test never released invocations, abort\n");
	}

	if (fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, 0, NULL)) {
		os::printf("FAIL: Test was aborted\n");
		rslt += 1;
	} else {
		os::printf("PASS: Test successfully completed\n");
		rslt += 0;
	}

	return (rslt);
#else
	return (no_fault_injection());
#endif
}

//
// node_die_during_invo
// This tests the possibility that a node dies after an invocation
// has reached the server and been assigned to a server thread. The
// test first sets a trigger for a fault point on the invocation
// which will be hit when the invocation enters orb_msg::process_message
// after having been assigned to a thread. This will cause the node
// to be rebooted, will release the invocation, and will set a node
// trigger for the same fault point. Each time this fault point is hit,
// it will check to see if the thread processing the invocation is the
// same thread. When it is, SIGNAL_RELEASE will be triggered, which
// will be noticed by node_wait_until_thread_reused, which will be
// started on this node after the node reboots.
//
int
node_die_during_invo(int argc, char *argv[])
{
#if defined(_FAULT_INJECTION)
	os::printf("*** node_die_during_invo\n");

	if (argc != 2) {
		os::printf("FAIL: Incorrect number of arguments\n");
		return (1);
	}
	int cv_num = os::atoi(argv[1]);

	fork_n_blocked_invoke(1, cv_num, invo_death);

	// Now, reboot the node.
	node_reboot(0, NULL);

	ASSERT(1);
	return (0);
#else
	argc, argv;		// To shut compiler warning

	os::printf("FAIL: Fault injection not defined\n");
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

//
// This function reboots the node from which it is called.
//

int
node_reboot(int, char *[])
{
	os::printf("*** Node reboot.\n");

#if defined(_FAULT_INJECTION)

	FaultFunctions::wait_for_arg_t arg;

	// Cause the node to reboot
	arg.op = FaultFunctions::WAIT_FOR_DEAD;
	arg.nodeid = 0;

	FaultFunctions::reboot(0, &arg, (uint32_t)sizeof (arg));

	// Should never reach this point.
	ASSERT(0);
	return (1);
#else
	return (no_fault_injection());
#endif
}

//
// This test sends a number of blocking two-way calls to the server, thus
// taking up a large number of resources from the resource pool.
// The client asks the server for more resources repeatedly. When the
// server responds with a resource grant of 0, the program causes the node
// to reboot.
//
// This is used to test that resources are returned to the pool during
// node reconfiguration

int
node_reboot_request_denied(int, char *[])
{
	os::printf("*** Node reboot when request denied.\n");

#if defined(_FAULT_INJECTION)

	int	rslt = 0;
	int 	i = 0;
	Environment e;
	FaultFunctions::wait_for_arg_t arg;

	fault_flowcontrol::flowcontrol_arg_t fparg;

	//
	// Arm Node trigger to turn on FLOWCONTROL_NEED_MORE_RESOURCES_1
	// fault_flowcontrol_need_more_resources_1 signals for the forking
	// to stop when the node receives a resource grant of 0
	//
	fparg.op = fault_flowcontrol::SIGNAL_STOP;
	NodeTriggers::add(FAULTNUM_FLOWCONTROL_NEED_MORE_RESOURCES_1, &fparg,
	    (uint32_t)sizeof (fparg), TRIGGER_THIS_NODE);

	rslt = fork_blocked_invoke(1, wait_until_release);

	// Cause the node to reboot
	arg.op = FaultFunctions::WAIT_FOR_REJOIN;
	arg.nodeid = 0;

	os::printf("PASS: Grabbed all resources and rebooting.\n");
	FaultFunctions::reboot(0, &arg, (uint32_t)sizeof (arg));

	os::printf("FAIL: Reboot should never reach this point\n");
	return (1);
#else
	return (no_fault_injection());
#endif
}
