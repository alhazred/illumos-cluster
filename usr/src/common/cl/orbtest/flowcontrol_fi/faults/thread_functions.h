/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_FLOWCONTROL_FI_THREAD_FUNCTIONS_H
#define	_FLOWCONTROL_FI_THREAD_FUNCTIONS_H

#pragma ident	"@(#)thread_functions.h	1.9	08/05/20 SMI"

//
// This file contains the definitions for
// the various thread functions which are
// passed to the forking functions. These
// are the individual invocations to the
// server.
//

#include <sys/os.h>
#include <orb/invo/common.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/fault/fault_injection.h>
#include <h/flowcontrol_fi.h>
#include <orbtest/flowcontrol_fi/impl_common.h>

void simple_invocation(void *arg);
void invo_death(void *arg);
//
// This function gets a reference to the server, and
// then makes a single, blocking, twoway invocation.
// It takes a single argument, which is a pointer to
// an integer. The integer should be 0 or 1, specifying
// which condition variable on the server the thread
// should block on.
//
void wait_until_release(void *arg);

//
// This function gets a reference to the server, and
// then makes a single, blocking (on the server),
// oneway invocation. It does not take any arguments.
// It always sends "1" as the condition variable to
// block on.
//
void oneways_wait_until_release(void * arg);

#endif /* _FLOWCONTROL_FI_THREAD_FUNCTIONS_H */
