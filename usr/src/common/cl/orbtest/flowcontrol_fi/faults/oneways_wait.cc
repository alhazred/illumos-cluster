/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)oneways_wait.cc	1.10	08/05/20 SMI"

#ifndef _KERNEL
#include <unistd.h>
#endif

#include <stdio.h>
#include <orbtest/flowcontrol_fi/server_impl.h>
#include <sys/thread.h>
#include <sys/proc.h>
#include <orbtest/flowcontrol_fi/faults/oneways_wait.h>
#include <orbtest/flowcontrol_fi/faults/entry_support.h>

//
// Tests forks oneway invocations until it requests
// and is granted additional threads from the server.
//
int
oneways_wait_until_got_threads(int, char *[])
{
	os::printf("*** Client with blocked oneway invocation.\n");

#if defined(_FAULT_INJECTION)

	int	rslt = 0;

	fault_flowcontrol::flowcontrol_arg_t fparg;

	//
	// Arm Node trigger to turn on FAULTNUM_FLOWCONTROL_GOT_THREADS
	// When server grants more resources, got_threads is called
	// Fault function signals for the cv release
	//
	fparg.op = fault_flowcontrol::SIGNAL_BOTH;
	fparg.cv_num = 1;
	NodeTriggers::add(FAULTNUM_FLOWCONTROL_GOT_THREADS, &fparg,
	    (uint32_t)sizeof (fparg), TRIGGER_ALL_NODES);

	rslt = fork_blocked_invoke(1, oneways_wait_until_release);

	if (fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, NULL, 0)) {
		os::printf("FAIL: Invocations timed out without triggering "
		    "resource block\n");
		rslt = 1;
	} else if (fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_RELEASE,
	    NULL, 0)) {
		os::printf("PASS: Test successfully completed\n");
		rslt = 0;
	} else {
		os::printf("FAIL: Exited without releasing\n");
		rslt = 1;
	}

	//
	// Want to give invocations time to release to make sure we
	// don't interfere with the next test.
	//
	// os::usecsleep(60 * 1000000);

	return (rslt);
#else
	return (no_fault_injection());
#endif

}

//
// This test sends a number of blocking one-way calls to the server, thus
// taking up a large number of resources from the resource pool.
// The client asks the server for more resources repeatedly. When the
// server responds with a resource grant of 0, the cv is signalled, and
// all threads return
//

int
oneways_wait_until_request_denied(int, char *[])
{
	os::printf("*** Node wait on oneways until request denied.\n");

#if defined(_FAULT_INJECTION)

	int	rslt = 0;
	int 	i = 0;
	int max_minutes = 5;
	Environment e;

	fault_flowcontrol::flowcontrol_arg_t fparg;

	//
	// Arm Node trigger to turn on FLOWCONTROL_PROCESS_RESOURCE_GRANT
	// fault_flowcontrol_process_resource_grant signals for the release
	//
	fparg.op = fault_flowcontrol::SIGNAL_BOTH;
	NodeTriggers::add(FAULTNUM_FLOWCONTROL_PROCESS_RESOURCE_GRANT, &fparg,
	    (uint32_t)sizeof (fparg), TRIGGER_ALL_NODES);

	rslt = fork_blocked_invoke(1, oneways_wait_until_release);

	//
	// Want to wait until the invocations are released. Loop until
	// the release is triggered or we spend max_minutes.
	//
	while (i < max_minutes) {
		os::usecsleep(60 * 1000000);
		if (fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_RELEASE,
		    NULL, 0))
			break;
		i++;
	}

	if (i == max_minutes) {
		NodeTriggers::add(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, NULL, 0,
		    TRIGGER_ALL_NODES);
	}

	if (fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, NULL, 0)) {
		os::printf("FAIL: Invocations timed out without triggering "
		    "resource block\n");
		rslt = 1;
	} else {
		os::printf("PASS: Test successfully completed\n");
	}

	return (rslt);
#else
	return (no_fault_injection());
#endif

}

//
// This tests the code path when a client uses many threads and then
// releases them
//

int
oneways_wait_until_resource_release(int, char *[])
{
	os::printf("*** Client with blocked invocations. Continues to issue "
	    "requests until gets threads, then releases invocations. Does "
	    "not quit until client sends resources back to server.\n");

#if defined(_FAULT_INJECTION)

	int rslt = 0;
	int max_wait = 30;
	fault_flowcontrol::flowcontrol_arg_t fparg;

	//
	// Set up Node Trigger for got threads
	// When we get additional threads from the server, the fault
	// will trigger the condition variable release
	//
	fparg.op = fault_flowcontrol::SIGNAL_BOTH;
	NodeTriggers::add(FAULTNUM_FLOWCONTROL_GOT_THREADS, &fparg,
	    (uint32_t)sizeof (fparg), TRIGGER_THIS_NODE);

	//
	// check_for_resource_changes_1 is hit when the client has
	// resources to release to the server.
	//
	fparg.op = fault_flowcontrol::CHECK_FOR_RESOURCE_CHANGES_1;
	NodeTriggers::add(FAULTNUM_FLOWCONTROL_CHECK_FOR_RESOURCE_CHANGES_1,
	    &fparg, (uint32_t)sizeof (fparg), TRIGGER_THIS_NODE);

	//
	// check_release is hit when the client has
	// resources to release to the server.
	//
	fparg.op = fault_flowcontrol::CHECK_FOR_RESOURCE_CHANGES_1;
	NodeTriggers::add(FAULTNUM_FLOWCONTROL_CHECK_RELEASE,
	    &fparg, (uint32_t)sizeof (fparg), TRIGGER_THIS_NODE);

	rslt = fork_blocked_invoke(1, oneways_wait_until_release);

	if (fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, NULL, 0)) {
		os::printf("FAIL: Invocations timed out without triggering "
		    "resource block\n");
		rslt += 1;
	}

	//
	// Want the invocations to release to server to test release. Wait until
	// release occurrs or max_Wait intervals have passed.
	//
	int i = 0;
	while (!fault_triggered(
	    FAULTNUM_FLOWCONTROL_SIGNAL_C_RESOURCE_CHANGES_1, NULL, 0)) {
		os::usecsleep(30 * 1000000);
		i++;
		if (i > max_wait) break;
	}

	if (i > max_wait) {
		os::printf("FAIL: Client never released threads to server\n");
		rslt += 1;
	} else {
		os::printf("PASS: Client got additional threads and then "
		    "released them\n");
		rslt += 0;
	}

	NodeTriggers::clear_all();

	return (rslt);
#else
	return (no_fault_injection());
#endif
}
