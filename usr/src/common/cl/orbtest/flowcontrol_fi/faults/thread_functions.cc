/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)thread_functions.cc	1.10	08/05/20 SMI"

#ifndef _KERNEL
#include <unistd.h>
#endif

#include <stdio.h>
#include <orbtest/flowcontrol_fi/server_impl.h>
#include <sys/thread.h>
#include <sys/proc.h>
#include <orbtest/flowcontrol_fi/faults/thread_functions.h>
#include <orbtest/flowcontrol_fi/faults/entry_support.h>

void
invo_death(void *)
{
#if defined(_FAULT_INJECTION)
	const int32_t	arg1 = 1;
	const int32_t	arg2 = 2;
	const int32_t	expected = arg1+arg2;
	Environment		e;
	int32_t		ret;
	flowcontrol_fi::server_var	server_v;

	// Get a reference to the server
	server_v = get_server();
	if (CORBA::is_nil(server_v)) {
		os::printf("FAIL: Invocation could not get server refernce\n");
		return;
	}

	// Set up the PROCESS_MESSAGE trigger
	fault_flowcontrol::flowcontrol_arg_t fparg;
	fparg.op = fault_flowcontrol::PROCESS_MESSAGE;
	InvoTriggers::add(FAULTNUM_FLOWCONTROL_PROCESS_MESSAGE, &fparg,
	    (uint32_t)sizeof (fparg));

	ret = server_v->twoway_blocking(arg1, arg2, e);

	// Verify the call succeeded.
	if (check_exception(e.exception(), "FAIL:")) {
		os::printf("FAIL: invocation returned exception. "
		    "Expected to succeed\n");
		NodeTriggers::add(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, NULL, 0);
	} else if (ret != expected) {
		os::printf("FAIL: invocation succeeded by returned wrong "
		    "value.\n");
		NodeTriggers::add(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, NULL, 0);
	}
#endif
}

void
simple_invocation(void *)
{
#if defined(_FAULT_INJECTION)

	const int32_t	arg1 = 1;
	const int32_t	arg2 = 2;
	const int32_t	expected = arg1+arg2;
	Environment		e;
	int32_t		ret;
	flowcontrol_fi::server_var	server_v;

	// Get a reference to the server
	server_v = get_server();
	if (CORBA::is_nil(server_v)) {
		os::printf("FAIL: Invocation could not get server refernce\n");
		return;
	}

	ret = server_v->twoway_simple(arg1, arg2, e);

	// Verify the call succeeded.
	if (check_exception(e.exception(), "FAIL:")) {
		os::printf("FAIL: invocation returned exception. "
		    "Expected to succeed\n");
		NodeTriggers::add(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, NULL, 0);
	} else if (ret != expected) {
		os::printf("FAIL: invocation succeeded by returned wrong "
		    "value. Got %d, expected %d\n", ret, expected);
		NodeTriggers::add(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, NULL, 0);
	}
#else
	os::printf("FAIL: Fault injection not defined\n");
#endif
}

//
// wait_until_release
// This function is mostly passed to fork_*blocked_invoke as
// a thread function. It makes a single blocked invocation to
// the server, using an integer which is passed in arg, to
// be the condition variable on which the invocations are to
// block.
//
void
wait_until_release(void * arg)
{
#if defined(_FAULT_INJECTION)

	int cv_num = *(int *)arg;

	const int32_t	arg2 = 2;
	const int32_t	expected = cv_num+arg2;
	Environment		e;
	int32_t		ret;
	flowcontrol_fi::server_var	server_v;

	// Get a reference to the server
	server_v = get_server();
	if (CORBA::is_nil(server_v)) {
		os::printf("FAIL: Invocation could not get server refernce\n");
		return;
	}

	ret = server_v->twoway_blocking(cv_num, arg2, e);

	// Verify the call succeeded.
	if (check_exception(e.exception(), "FAIL:")) {
		os::printf("FAIL: invocation returned exception. "
		    "Expected to succeed\n");
		NodeTriggers::add(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, NULL, 0);
	} else if (ret != expected) {
		os::printf("FAIL: invocation succeeded by returned wrong "
		    "value. Got %d, expected %d\n", ret, expected);
		NodeTriggers::add(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, NULL, 0);
	}
#else
	os::printf("FAIL: Fault injection not defined\n");
	arg;	// To shut compiler warning
#endif
}

//
// oneways_wait_until_release
// This function is mostly passed to the fork_*blocked_invoke
// functions. It is the thread function which the blocked threads
// are given. It takes a single integral argument, which is the
// condition variable for the oneway invocations to block on.
//
void
oneways_wait_until_release(void *arg)
{
#if defined(_FAULT_INJECTION)

	int 			cv_num = *(int *)arg;
	const int32_t	arg2 = 2;
	const int32_t	expected = cv_num+arg2;
	Environment		e;
	flowcontrol_fi::server_var	server_v;

	// Get a reference to the server
	server_v = get_server();
	if (CORBA::is_nil(server_v)) {
		os::printf("FAIL: Invocation could not get server refernce\n");
		return;
	}

	//
	// Although the client doesn't block on the invocation, the
	// server thread does, tying up the flow control resource
	//

	server_v->oneway_blocking(cv_num, arg2, e);

	// Verify the call succeeded.
	if (check_exception(e.exception(), "FAIL:")) {
		os::printf("FAIL: invocation returned exception. "
		    "Expected to succeed\n");
		NodeTriggers::add(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, NULL, 0);
	}
	else
		os::printf("PASS: Successfull single call\n");
#else
	os::printf("FAIL: Fault injection not defined\n");
	arg;	// To shut compiler warning
#endif
}
