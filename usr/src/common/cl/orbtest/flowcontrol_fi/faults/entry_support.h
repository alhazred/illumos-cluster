/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ORBTEST_FLOWCONTROL_FI_FAULTS_ENTRY_SUPPORT_H
#define	_ORBTEST_FLOWCONTROL_FI_FAULTS_ENTRY_SUPPORT_H

#pragma ident	"@(#)entry_support.h	1.25	08/05/20 SMI"

//
// test entry support routines
//

#include <sys/os.h>
#include <orb/invo/common.h>
#include <nslib/ns.h>
#include <h/flowcontrol_fi.h>
#include <orb/flow/resource_balancer.h>
#include <orb/fault/fault_flowcontrol.h>
#include <orb/fault/fault_injection.h>
#include <orbtest/flowcontrol_fi/impl_common.h>
#include <orbtest/flowcontrol_fi/server_impl.h>
#include <orbtest/flowcontrol_fi/node_impl.h>
#include <orbtest/flowcontrol_fi/faults/thread_functions.h>

int	check(int, char *[]);

//
// Function takes a pointer to a thread function. It enters a while loop,
// creating threads with thread_function until SIGNAL_STOP is set. This
// is used mainly to create resource exhaustion
//
// The second is a version of the first which also stops if n threads
// have been forked.
//
int fork_blocked_invoke(int cv_num, void (*threadfn)(void *));

int fork_n_blocked_invoke(int n, int cv_num, void (*threadfn)(void *));

//
// This is a thread function which checks at the end to see if the
// got_threads faultpoint signal has been triggered
//
void wait_until_got_threads(void * argp);

//
// check for exceptions.
// Returns :
//	True if there IS an exception, false otherwise.
//
bool	check_exception(CORBA::Exception *ex, char *prefix);

//
// Returns a flowcontrol_fi::server_ptr for the loaded
// flowcontrol server
// Returns:
//  Reference to the server if successful, _nil() otherwise.
//
flowcontrol_fi::server_ptr  get_server();

//
// Get a reference to flowcontrol_fi::node from the name server.
// Returns:
//	Reference to the server if successful, _nil() otherwise.

flowcontrol_fi::node_ptr get_node();

//
// Tells the server that we have finished the tests.
// Must be called whenever the server is loaded.
//
int	server_done(int, char *[]);

//
// Tells the node that we have finished the tests.
// Must be called whenever the node is loaded.
//
int	node_done(int, char *[]);

//
// Clears all triggers set on the node this is loaded on.
// Should be called on each node before a test exits.
//
int	clear_all(int, char *[]);

//
// Sets both release triggers, allowing the condition
// variables to release the invocations.
//
int release_all(int, char *[]);

#endif	/* _ORBTEST_FLOWCONTROL_FI_FAULTS_ENTRY_SUPPORT_H */
