/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_FLOWCONTROL_FI_ONEWAYS_WAIT_H
#define	_FLOWCONTROL_FI_ONEWAYS_WAIT_H

#pragma ident	"@(#)oneways_wait.h	1.10	08/05/20 SMI"

//
// Entry points for test functions which fork blocked
// oneway invocations (invocations block on server, preventing
// return of resources) and then wait for certain events.
//

#include <sys/os.h>
#include <orb/invo/common.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/fault/fault_injection.h>
#include <h/flowcontrol_fi.h>
#include <orbtest/flowcontrol_fi/impl_common.h>

//
// Test forks oneway invocations until node requests
// and recieves additional resources.
//
int		oneways_wait_until_got_threads(int, char *[]);

//
// Test forks oneway invocations until node requests
// additional resources and server denies. Happens when
// server has exhausted resources.
//
int		oneways_wait_until_request_denied(int, char *[]);

//
// Test makes oneway invocations until node requests
// and receives resources. Then, invocations unblock, and
// node releases threads back to server.
//
int		oneways_wait_until_resource_release(int, char *[]);

#endif /* _FLOWCONTROL_FI_ONEWAYS_WAIT_H */
