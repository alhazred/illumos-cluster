/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)single_invocations.cc	1.10	08/05/20 SMI"

#if !defined(_KERNEL)
#include <unistd.h>
#endif

#include <stdio.h>
#include <orbtest/flowcontrol_fi/server_impl.h>
#include <orbtest/flowcontrol_fi/faults/single_invocations.h>
#include <orbtest/flowcontrol_fi/faults/entry_support.h>

//
// This file contains the tests which use only a single,
// usually non-blocking, invocation.
//

//
// Simple client with 1 invocation
//
int
simple_invoke_1(int, char *[])
{
	os::printf("*** Simple client with 1 invocation.\n");
#if defined(_FAULT_INJECTION)

	const int32_t	arg1 = 1;
	const int32_t	arg2 = 2;
	int32_t		ret;
	const int32_t	expected = arg1+arg2;
	int 		rslt = 0;

	fault_flowcontrol::flowcontrol_arg_t	farg;
	flowcontrol_fi::server_var	server_v;
	Environment		e;


	// Get a reference to the server.
	server_v = get_server();
	if (CORBA::is_nil(server_v)) {
		return (1);
	}

	//
	// Arm Invo trigger to turn on FAULTNUM_RESOURCE_ALLOCATOR_1
	// This fault point doesn't do anything important. It just sets
	// another fault so that we know it's been hit.
	//

	farg.op = fault_flowcontrol::RESOURCE_ALLOCATOR_1;
	InvoTriggers::add(FAULTNUM_FLOWCONTROL_RESOURCE_ALLOCATOR_1,
	    &farg, (uint32_t)sizeof (farg));

	// Invoke the server
	ret = server_v->twoway_simple(arg1, arg2, e);

	// Verify the call succeeded.
	if (check_exception(e.exception(), "FAIL:")) {
		os::printf("FAIL: invocation returned exception. "
		    "Expected to succeed\n");
		rslt += 1;
	} else if (ret != expected) {
		os::printf("FAIL: invocation succeeded by returned wrong "
		    "value. Got %d, expected %d\n", ret, expected);
		rslt += 1;
	} else if (!fault_triggered(
	    FAULTNUM_FLOWCONTROL_SIGNAL_RESOURCE_ALLOCATOR_1, NULL, 0)) {
		// When the flow hits the fault point, it triggers the above
		// signal fault
		os::printf("FAIL: invocation did not trigger resource availabe "
		    "fault.\n");
		rslt += 1;
	} else {
		os::printf("PASS: test completed\n");
		rslt += 0;
	}

	return (rslt);
#else
	return (no_fault_injection());
#endif
}

//
// Simple client with 1 one-way invocation
//
int
oneway_invoke(int, char *[])
{
	os::printf("*** Simple client with 1 one-way invocation.\n");
#if defined(_FAULT_INJECTION)

	const int32_t	arg1 = 1;
	const int32_t	arg2 = 2;
	const int32_t	expected = arg1+arg2;
	int 		rslt = 0;

	fault_flowcontrol::flowcontrol_arg_t	farg;
	Environment		e;

	flowcontrol_fi::server_var	server_v;

	// Get a reference to the server.
	server_v = get_server();
	if (CORBA::is_nil(server_v)) {
		return (1);
	}

	//
	// Arm Invo trigger to turn on FAULTNUM_RESOURCE_ALLOCATOR_1
	// This fault point doesn't do anything important. It just sets
	// another fault so that we know it's been hit.
	//
	farg.op = fault_flowcontrol::RESOURCE_ALLOCATOR_1;
	InvoTriggers::add(FAULTNUM_FLOWCONTROL_RESOURCE_ALLOCATOR_1,
	    &farg, (uint32_t)sizeof (farg));

	// Invoke the server
	server_v->oneway_simple(arg1, arg2, e);

	// Give the invocations time to go through and hit the fault
	os::usecsleep(30 * 1000000);

	// Verify the call succeeded.
	if (check_exception(e.exception(), "FAIL:")) {
		os::printf("FAIL: invocation returned exception. "
		    "Expected to succeed\n");
		rslt += 1;
	} else if (!fault_triggered(
	    FAULTNUM_FLOWCONTROL_SIGNAL_RESOURCE_ALLOCATOR_1, NULL, 0)) {
		// When the flow hits the fault point, it should trigger
		// the above signal fault point
		os::printf("FAIL: invocation did not trigger resource availabe "
		    "fault.\n");
		rslt += 1;
	} else {
		os::printf("PASS: test completed\n");
		rslt += 0;
	}

	return (rslt);
#else
	return (no_fault_injection());
#endif
}
