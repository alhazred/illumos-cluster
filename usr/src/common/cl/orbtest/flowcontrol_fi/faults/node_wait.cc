/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)node_wait.cc	1.14	08/05/20 SMI"

#ifndef _KERNEL
#include <unistd.h>
#endif

#include <stdio.h>
#include <orbtest/flowcontrol_fi/server_impl.h>
#include <sys/thread.h>
#include <sys/proc.h>
#include <orbtest/flowcontrol_fi/faults/node_wait.h>
#include <orbtest/flowcontrol_fi/faults/entry_support.h>
#include <orbtest/flowcontrol_fi/faults/thread_functions.h>

//
// Test forks invocations until it requests and receives additional
// threads from the server. It then unblocks the invocations
// and returns.
//
int
node_wait_until_got_threads(int, char *[])
{
	os::printf("*** Simple client with blocked invocation.\n");

#if defined(_FAULT_INJECTION)

	int	rslt = 0;

	fault_flowcontrol::flowcontrol_arg_t fparg;

	//
	// Arm Node trigger to turn on FAULTNUM_FLOWCONTROL_GOT_THREADS
	// When server grants more resources, got_threads is called
	// Fault function signals for the cv release
	//
	fparg.op = fault_flowcontrol::SIGNAL_BOTH;
	NodeTriggers::add(FAULTNUM_FLOWCONTROL_GOT_THREADS, &fparg,
	    (uint32_t)sizeof (fparg), TRIGGER_ALL_NODES);

	rslt = fork_blocked_invoke(1, wait_until_release);

	if (fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, NULL, 0)) {
		os::printf("FAIL: Invocations timed out without triggering "
		    "resource block\n");
		rslt = 1;
	} else if (fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_RELEASE,
	    NULL, 0)) {
		os::printf("PASS: Test successfully completed\n");
		rslt = 0;
	} else {
		os::printf("FAIL: Exited without releasing\n");
		rslt = 1;
	}

	return (rslt);
#else
	return (no_fault_injection());
#endif

}

//
// This test sends a number of blocking two-way calls to the server, thus
// taking up a large number of resources from the resource pool.
// The client asks the server for more resources repeatedly. When the
// server responds with a resource grant of 0, the cv is signalled, and
// all threads return
//

int
node_wait_until_request_denied(int, char *[])
{
#if defined(_FAULT_INJECTION)

	int	rslt = 0;
	int 	i = 0;
	int max_minutes = 5;
	Environment e;

	os::printf("*** Node wait until request denied.\n");

	fault_flowcontrol::flowcontrol_arg_t fparg;

	//
	// Arm Node trigger to turn on FLOWCONTROL_PROCESS_RESOURCE_GRANT
	// fault_flowcontrol_process_resource_grant signals for the release
	//
	fparg.op = fault_flowcontrol::SIGNAL_BOTH;
	fparg.cv_num = 1;
	NodeTriggers::add(FAULTNUM_FLOWCONTROL_PROCESS_RESOURCE_GRANT, &fparg,
	    (uint32_t)sizeof (fparg), TRIGGER_ALL_NODES);

	rslt = fork_blocked_invoke(1, wait_until_release);
	while (i < max_minutes) {
		//
		// We don't want to exit until the invocations have unblocked
		// so wait here, checking to see if the release has been
		// triggered. If it isn't after max_minutes, abort.
		//
		if (fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_RELEASE,
		    NULL, 0))
			break;
		os::usecsleep(60 * 1000000);
		i++;
	}

	if (i == max_minutes)
		NodeTriggers::add(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, NULL, 0,
		    TRIGGER_ALL_NODES);

	if (fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, NULL, 0)) {
		os::printf("FAIL: Invocations timed out without triggering "
		    "resource block\n");
		rslt = 1;
	} else {
		os::printf("PASS: Test successfully completed\n");
		rslt = 0;
	}

	return (rslt);
#else
	return (no_fault_injection());
#endif
}

//
// This tests that if a node made an invocation and then died,
// (see node_die_during_invo) that the thread that was
// processing the invocation is reused. This test forks
// invocations until one of them is processed by the thread.
//
int
node_wait_until_thread_reused(int, char *[])
{
	os::printf("*** node_wait_until_thread_reused\n");

#if defined(_FAULT_INJECTION)

	int	rslt = 0;
	Environment e;

	//
	// Ask the server what the maximum number of threads available is.
	// We do this in the fault_flowcontrol because that is loaded in
	// the kernel.
	//
	int high, mod, low, inc;
	flowcontrol_fi::server_var	server_v;
	server_v = get_server();
	if (CORBA::is_nil(server_v)) {
		os::printf("FAIL:Could not get server reference\n");
		return (1);
	}

	server_v->get_pool_info(low, mod, high, inc, e);

	// Verify the call succeeded.
	if (check_exception(e.exception(), "FAIL:")) {
		os::printf("FAIL: invocation returned exception. "
		    "Expected to succeed\n");
		return (1);
	}

	//
	// Now fork off high threads to make sure the threadpool won't
	// try to destroy the looked for thread due to the reboot.
	//
	fork_n_blocked_invoke(low+inc, 2, simple_invocation);

	//
	// Signal release on condition variable 1, so that the invocation
	// that was started before the node death will complete and
	// the thread will be released.
	//
	NodeTriggers::add(FAULTNUM_FLOWCONTROL_SIGNAL_RELEASE, NULL,
	    0, TRIGGER_ALL_NODES);

	fork_n_blocked_invoke(high, 2, wait_until_release);
	NodeTriggers::add(FAULTNUM_FLOWCONTROL_SIGNAL_RELEASE_2, NULL,
	    0, TRIGGER_ALL_NODES);

	if (fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_ABORT, NULL, 0)) {
		os::printf("FAIL: Invocations timed out without triggering "
		    "resource block\n");
		rslt = 1;
	} else if (fault_triggered(FAULTNUM_FLOWCONTROL_SIGNAL_STOP, NULL, 0)) {
		os::printf("PASS: Test successfully completed\n");
		rslt = 0;
	} else {
		os::printf("FAIL: Finished without releasing\n");
		rslt = 1;
	}

	return (rslt);
#else
	return (no_fault_injection());
#endif

}
