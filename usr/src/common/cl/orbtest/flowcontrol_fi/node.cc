/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)node.cc	1.17	08/05/20 SMI"

//
// node support routine, used to init extra node
//
#include <orbtest/flowcontrol_fi/node_impl.h>

// -----------------------------------------------------
// KERNEL server
// -----------------------------------------------------
#if defined(_KERNEL)

extern "C" {
	void _cplpl_init(void);
	void _cplpl_fini(void);
}
char _depends_on[] = "misc/cl_runtime misc/cl_load misc/cl_orb "
	"misc/cl_haci misc/cl_quorum misc/cl_comm";

extern struct mod_ops mod_miscops;

struct modlmisc modlmisc = {
	&mod_miscops, "flowcontrol FI extra node server"
};

struct modlinkage modlinkage = {
	MODREV_1, { (void *)&modlmisc, NULL }
};

static node_impl *nodep = NULL;

int
_init(void)
{
	int	error = 0;

	if ((error = mod_install(&modlinkage)) != 0) {
		return (error);
	}
	_cplpl_init();			// C++ initializtion

#if defined(_FAULT_INJECTION)
	nodep = new node_impl;
	if (! nodep->init(NODE)) {
		delete nodep;
		error = ECANCELED;
	}
#else
	no_fault_injection();
	error = ENOTSUP;
#endif
	if (error) {
		_cplpl_fini();
		(void) mod_remove(&modlinkage);
	}

	return (error);
}

int
_fini(void)
{
#if defined(_FAULT_INJECTION)
	const uint32_t time_out = 180;	// wait timeout (in seconds)

	if (nodep != NULL) {
		if (! nodep->wait_until_unreferenced(time_out)) {
			os::printf("FAIL: extra node didn't get unreferenced "
			"after %d seconds\n", time_out);
			return (EBUSY);
		} else {
			delete nodep;
		}
	}
	_cplpl_fini();
#endif
	return (mod_remove(&modlinkage));
}

int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));

}

#endif // _KERNEL

// -----------------------------------------------------
// USER server
// -----------------------------------------------------
#if defined(_USER)

#include <signal.h>
#include <string.h>
#include <errno.h>

int
main()
{
#if defined(_FAULT_INJECTION)
	const uint32_t 	time_out = 180;		// wait timeout (in seconds)
	node_impl	*nodep;
	sigset_t	sig_set;
	int		ret, err;

	//
	// The fault injection driver will send a SIGTERM when trying to
	// unload this server. This thread will wait until SIGTERM is sent
	// and then wait until the server has been unreferenced before exiting.
	// We'll have all threads block SIGTERM to ensure that only this thread
	// receives SIGTERM.
	//
	(void) sigemptyset(&sig_set);
	if (sigaddset(&sig_set, SIGTERM) < 0) {
		os::printf("ERROR: Can't add SIGTERM to signal set (%s)\n",
		    strerror(errno));
		return (1);
	}
	if ((ret = thr_sigsetmask(SIG_BLOCK, &sig_set, NULL)) != 0) {
		os::printf("ERROR: Can't block SIGTERM (%s)\n",
			strerror(ret));
		return (1);
	}

	if ((err = ORB::initialize()) != 0) {
		os::printf("ERROR: Can't initialize ORB with error code %d\n",
		    err);
		return (1);
	}

	nodep = new node_impl;
	if (! nodep->init(NODE)) {
		delete nodep;
		return (1);
	}

	// Wait until SIGTERM is sent.
	if ((ret = sigwait(&sig_set)) < 0) {
		os::printf("ERROR: Can't sigwait for SIGTERM (%s)\n",
			strerror(errno));
		delete nodep;
		return (1);
	}

	// Wait until server is unreferenced.
	if (!nodep->wait_until_unreferenced(time_out)) {
		os::printf("FAIL: extra node didn't get unreferenced "
			"after %d seconds\n", time_out);
		delete nodep;
		return (1);
	}

	delete nodep;
	return (0);
#else
	return (no_fault_injection());
#endif
}

#endif  // _USER

// -----------------------------------------------------
// UNODE server
// -----------------------------------------------------
#if defined(_UNODE)

int
unode_init()
{
#if defined(_FAULT_INJECTION)
	os::printf("flowcontrol FI extra node loaded\n");
	return (0);
#else
	return (no_fault_injection());
#endif
}

int
flowcontrol_fi_node(int, char *[])
{
#if defined(_FAULT_INJECTION)
	node_impl	*nodep;

	nodep = new node_impl;
	if (! nodep->init(NODE)) {
		delete nodep;
		return (1);
	}

	return (0);
#else
	return (no_fault_injection());
#endif
}

#endif // _UNODE
