/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)client.cc	1.17	08/05/20 SMI"

//
// client supporting flowcontrol_fi tests, used to call client test functions.
//

#include <orbtest/fi_support/fi_driver.h>
#include <orbtest/flowcontrol_fi/impl_common.h>
#include <orbtest/flowcontrol_fi/faults/node_wait.h>
#include <orbtest/flowcontrol_fi/faults/node_join.h>
#include <orbtest/flowcontrol_fi/faults/oneways_wait.h>
#include <orbtest/flowcontrol_fi/faults/entry_support.h>
#include <orbtest/flowcontrol_fi/faults/modify_resource_pool.h>
#include <orbtest/flowcontrol_fi/faults/node_death.h>
#include <orbtest/flowcontrol_fi/faults/node_grab_threads.h>
#include <orbtest/flowcontrol_fi/faults/node_release.h>
#include <orbtest/flowcontrol_fi/faults/single_invocations.h>

// Test entry functions.
fi_entry_t fi_entry[] = {
	ENTRY(simple_invoke_1),
	ENTRY(oneway_invoke),
	ENTRY(node_wait_until_resource_release),
	ENTRY(node_wait_until_got_threads),
	ENTRY(node_wait_until_request_denied),
	ENTRY(node_wait_until_release),
	ENTRY(node_reboot_request_denied),
	ENTRY(change_resource_pool),
	ENTRY(restore_resource_pool),
	ENTRY(oneways_wait_until_got_threads),
	ENTRY(oneways_wait_until_resource_release),
	ENTRY(oneways_wait_until_request_denied),
	ENTRY(node_partial_release),
	ENTRY(node_reboot),
	ENTRY(node_join_resources_available),
	ENTRY(node_join_resources_unavailable),
	ENTRY(node_die_resources_needed),
	ENTRY(node_die_resources_unneeded),
	ENTRY(node_grab_threads_until_got_threads),
	ENTRY(node_grab_threads_until_request_denied),
	ENTRY(node_request_filled_by_other_node),
	ENTRY(node_released_threads_needed),
	ENTRY(node_die_during_invo),
	ENTRY(node_wait_until_thread_reused),

	ENTRY(grab_threads_until_stop),

	ENTRY(server_done),
	ENTRY(node_done),
	ENTRY(clear_all),
	ENTRY(release_all),

	ENTRY(NULL)
};

// ---------------------------------------------------------
// KERNEL client
// ---------------------------------------------------------
#if defined(_KERNEL)

extern "C" {
	void _cplpl_init(void);
	void _cplpl_fini(void);
}

char _depends_on[] = "misc/cl_runtime misc/cl_load misc/cl_orb "
	"misc/cl_haci misc/cl_quorum misc/cl_comm misc/fi_driver";

extern struct mod_ops mod_miscops;

struct modlmisc modlmisc = {
	&mod_miscops, "flowcontrol FI test client"
};

struct modlinkage modlinkage = {
	MODREV_1, { (void *)&modlmisc, NULL }
};

int
_init(void)
{
	int	error = 0;

	if (error = mod_install(&modlinkage)) {
		return (error);
	}
	_cplpl_init();			// C++ initialization

#if defined(_FAULT_INJECTION)
	struct modctl	*mp;
	int		argc = 0;
	char		**argv = NULL;
	int		i, rslt;

	// Get module name so we know what test case to call
	if ((mp = mod_getctl(&modlinkage)) == NULL) {
		os::printf("ERROR: can't find modctl\n");
		_cplpl_fini();
		(void) mod_remove(&modlinkage);
		return (EIO);
	}

	// Parse module name to get arguments
	mod_name_to_args(mp->mod_modname, argc, argv);

	// Do the test.
	if ((rslt = do_fi_test(argc, argv, fi_entry)) != 0) {
		// Map test failures/errors to ECANCELEd.
		error = ECANCELED;
	}
	os::printf("	Result From Module: %d\n", rslt);

	// Clean up argument vectors allocated by mod_name_to+args().
	for (i = 0; i < argc; ++i) {
		delete[] argv[i];
	}
	delete[] argv;
#else
	no_fault_injection();
	error = ENOTSUP;
#endif

	if (error) {
		_cplpl_fini();
		(void) mod_remove(&modlinkage);
	}

	return (error);
}

int
_fini(void)
{
	_cplpl_fini();
	return (mod_remove(&modlinkage));
}

int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}

#endif // _KERNEL

// ---------------------------------------------------------
// USER client
// ---------------------------------------------------------
#if defined(_USER)
int
main(int argc, char *argv[])
{
#if defined(_FAULT_INJECTION)
	int	rslt, err;

	if ((err = ORB::initialize()) != 0) {
		os::printf("ERROR: Can't initialize orb with error code %d\n",
		    err);
		return (1);
	}

	// The fault Injection test driver passes the name of the
	// entry test function to execute and its argument.
	rslt = do_fi_test(argc, argv, fi_entry);
	return (rslt);
#else
	argc; argv;			// to shut compiler warning
	return (no_fault_injection());
#endif
}

#endif // _USER

// ----------------------------------------------------------
// UNODE client
// ----------------------------------------------------------
#if defined(_UNODE)

int
unode_init()
{
#if defined(_FAULT_INJECTION)
	os::printf("flowcontrol FI test client loaded\n");
	return (0);
#else
	return (no_fault_injection());
#endif
}

#endif // _UNODE
