/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CCR_CLNT_H
#define	_CCR_CLNT_H

#pragma ident	"@(#)ccr_clnt.h	1.21	08/06/04 SMI"

#include	<orb/invo/common.h>
#include	<orb/object/adapter.h>
#include	<h/ccr.h>

//
// Functions that act as clients for each test case
// They must be passed a pointer to a function that will activate the fault
// points for this test. And a pointer to a functions to de-activate the faults
// after the test has been run.  (These may be NULL).
// These functions are called immediate before and after the invocation that
// is being tested is called.
//

// Macro for the function to turn on fault injection
#define	FAULT_FPTR(func)	int (*func) (void *, int)

#define	SVC_NAME "ccr_server"

// ccr read test
extern int ccr_longelement(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void *,
	int,
	Environment &);

// ccr abort test
extern int ccr_abort(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void *,
	int,
	Environment &);

// ccr callback test
extern int ccr_callback(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void *,
	int,
	Environment &);

// ccr read test
extern int ccr_removeelement(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void *,
	int,
	Environment &);

// ccr read test
extern int ccr_nextelement(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void *,
	int,
	Environment &);

// ccr directory test
extern int ccr_directory(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void *,
	int,
	Environment &);

// ccr update
extern int ccr_updateelement(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void *,
	int,
	Environment &);

// ccr simple add
extern int ccr_simple_addelement(
	char *tablename,
	Environment &);

// ccr add
extern int ccr_addelement(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void *,
	int,
	Environment &);

// ccr add fi
extern int ccr_addele(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void *,
	int,
	Environment &);

// ccr recovery
extern int ccr_overwrite_with_reboot(
	void *,
	int,
	Environment &);

// ccr recovery
extern int ccr_overwrite_without_reboot(
	void *,
	int,
	Environment &);

// ccr recovery
extern int ccr_delete_without_reboot(
	void *,
	int,
	Environment &);

// ccr recovery
extern int ccr_preparedata(
	void *,
	int,
	Environment &);

// ccr recovery
extern int ccr_verifydata(
	void *,
	int,
	Environment &);

// ccr clearnup
// Attempts to clean up after error
extern void ccr_cleanup(
	void *,
	int);

//
// class implementing the callback interface
//
class callback_impl : public McServerof<ccr::callback> {
public:
	callback_impl();
	void did_update_zc(const char *, const char *, ccr::ccr_update_type,
	    Environment &);
	void did_recovery_zc(const char *, const char *, ccr::ccr_update_type,
	    Environment &);
	void did_update(const char *, ccr::ccr_update_type,
	    Environment &);
	void did_recovery(const char *, ccr::ccr_update_type,
	    Environment &);
	void _unreferenced(unref_t);
	bool callback_done;
	int num_elements_read;
};

#endif	/* _CCR_CLNT_H */
