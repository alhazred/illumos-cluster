/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)ccr_clnt.cc	1.38	08/10/27 SMI"

#if !defined(_KERNEL) && !defined(_UNODE)
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#endif

#include <sys/os.h>
#include <sys/cmn_err.h>
#include <h/naming.h>
#include <h/ccr.h>
#include <orb/invo/common.h>
#include <nslib/ns.h>
#include <h/dc.h>

#include <orbtest/repl_tests/repl_test_switch.h>
#include <orbtest/rm_probe/rm_snap.h>

#include <orbtest/ccr_fi/ccr_clnt.h>

#include <orbtest/ccr_fi/faults/entry_common.h>

// nodeid_t any_non_root();

//
// Function to arm faults
//
static void
arm_faults(
	FAULT_FPTR(on_fp),
	void *argp,
	int is_ha = 0)
{
	nodeid_t secondary_nodeid; // = any_non_root();
	ASSERT(argp != NULL);
	secondary_nodeid = *(nodeid_t *)argp;

	if (is_ha == 1) {
		rm_snap _rma_snapshot;
		if (!_rma_snapshot.refresh(SVC_NAME)) {
			os::prom_printf("Couldn't get rma snapshot"
				"(%s)\n", SVC_NAME);
			return;
		}

		Environment e;
		struct opt_t _opt;
		char prov_desc[10];
		os::sprintf(prov_desc, "%d", secondary_nodeid);
		_opt.new_state = replica::SC_SET_PRIMARY;
		_opt.prov_desc = prov_desc;
		_opt.service_desc = SVC_NAME;
		(void) trigger_switchover(&_opt, e);
		if (e.exception()) {
			e.exception()->print_exception("trigger_switchover");
		}
	}

	if (on_fp != NULL) {
		if (on_fp(&secondary_nodeid, (int)sizeof (nodeid_t)))
			os::prom_printf("WARNING: Arming faults failed\n");
		else
			os::prom_printf("Faults armed\n");
	} else {
		os::prom_printf("No Faults armed\n");
	}

}	

//
// Function to disarm faults
//
static void
disarm_faults(
	FAULT_FPTR(off_fp),
	void *argp = NULL)
{
	if (off_fp != NULL) {
		if (off_fp(argp, 0) != 0)
			os::prom_printf("WARNING: Disarming faults "
					"failed\n");
		else
			os::prom_printf("Fault disarmed.\n");
	}

}

//
// Helper function to do create_table s
//
int
do_creates(
	ccr::directory_var ccr_dir,
	char **argp,
	int nooftables,
	Environment &e)
{
	int i;

	for (i = 0; i < nooftables; i++) {
		char *tablename = argp[i];
		ccr_dir->create_table(tablename, e);
		if (e.exception()) {
			e.exception()->print_exception(
				"create_table failed");
			return (1);
		}
	}
	return (0);
}

//
// Helper function to do remove_table s
//
int
do_removes(
	ccr::directory_var ccr_dir,
	char **argp,
	int nooftables,
	Environment &e)
{
	os::printf("do_removes\n");
	int i;

	for (i = 0; i < nooftables; i++) {
		ccr_dir->remove_table(argp[i], e);
		if (e.exception()) {
			e.exception()->print_exception("ccr_update: "
						"Error in remove_table");
			return (1);
		}
	}
	return (0);
}

//
// The CCR cleanup.  This removes tables that may be left after an error.
//
void
ccr_cleanup(
	void *argarray,
	int arglen)
{
	os::printf("In cleanup\n");
	Environment e;
	int nooftables = arglen-2;

	char **argp = (char **)argarray;
	os::printf("Number of tables is %d, argp[0] is %s\n",
	    nooftables, argp[0]);
	argp += 2;

	while (nooftables > 0 && os::strcmp(argp[0], "seq") == 0 ||
	    os::strcmp(argp[0], "rep1") == 0 ||
	    os::strcmp(argp[0], "rep2") == 0 ||
	    os::strcmp(argp[0], "slp") == 0) {
		argp++;
		nooftables--;
	}

	naming::naming_context_var ctxp = ns::local_nameserver();
	CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
	if (e.exception()) {
		os::prom_printf("ccr_directory not found in name server\n");
		return;
	}
	ccr::directory_var ccr_dir = ccr::directory::_narrow(obj);

	os::printf("Cleanup removing tables\n");

	for (int i = 0; i < nooftables; i++) {
		os::printf("cleanup removing %s\n", argp[i]);
		ccr_dir->remove_table(argp[i], e);
		// Ignore exceptions
		e.clear();
	}
	os::printf("Finished cleanup\n");
}

//
// The CCR directory client
//
int
ccr_directory(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void *argarray,
	int arglen,
	Environment &e)
{
// #if defined(_KERNEL)

	int nooftables = arglen-2;

	char **argp = (char **)argarray;
	int id = os::atoi(argp[1]);
	nodeid_t nodeid = (nodeid_t)id;
	argp += 2;

	naming::naming_context_var ctxp = ns::local_nameserver();
	CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
	if (e.exception()) {
		os::prom_printf("ccr_directory not found in name server\n");
		return (1);
	}
	ccr::directory_var ccr_dir = ccr::directory::_narrow(obj);

	CORBA::StringSeq *tablenames;
	ccr_dir->get_table_names(tablenames, e);
	if (e.exception()) {
		e.exception()->print_exception("ccr_update: get_table_names"
					"failed");
		return (1);
	}

	int orig_num_tables = (int)tablenames->length();

	arm_faults(on_fp, &nodeid);
	if (do_creates(ccr_dir, argp, nooftables, e))
		return (1);
	disarm_faults(off_fp);

	ccr_dir->get_table_names(tablenames, e);
	if (e.exception()) {
		e.exception()->print_exception("ccr_update: get_table_names"
					"failed");
		return (1);
	}

	int num_tables = (int)tablenames->length();
	if (num_tables-orig_num_tables != nooftables) {
		os::prom_printf("Error: No. of tables created %d not equal to"
		    "no. of tables found %d\n", nooftables, num_tables);
		return (1);
	}

	if (do_removes(ccr_dir, argp, nooftables, e))
		return (1);

	return (0);
// #endif
}

// #if defined(_KERNEL)
//
// Helper fn. to do add_element s.
// If seq is set, add_elements will be used instead of add_element.
// If rep is set to 1 or 2, an entry will be repeated, which should
// generate an exception, which will be caught.  For rep=1, the repeated element
// will be added at the beginning, while for rep=2 the repeated element will be
// added as part of the group.
// If slp is set, there will be a sleep before the commit.
//
int
do_adds(
	ccr::directory_var ccr_dir,
	char **argp,
	int nooftables,
	int tablelen,
	int seq,
	int rep,
	int slp,
	Environment &e)
{
	int totalrows = 0;
	ccr::updatable_table_var upd_v;
	int i, j;

	for (i = 0; i < nooftables; i++) {
		bool got_exception = false;
		char *tablename = argp[i];
		char key[16];
		char val[16];
		ccr::element_seq eseq((unsigned int)tablelen,
		    (unsigned int)tablelen);
		upd_v = ccr_dir->begin_transaction(tablename, e);
		if (e.exception()) {
			e.exception()->print_exception("ccr_update");
			return (1);
		}
		if (rep == 1) {
			// This key will be repeated
			os::sprintf(key, "%d", totalrows+2);
			upd_v->add_element(key, "test", e);
			if (e.exception()) {
				e.exception()->print_exception(
				    "ccr_update");
				return (1);
			}
		}
		for (j = 0; j < tablelen; j++, totalrows++) {
			os::sprintf(key, "%d", totalrows);
			os::sprintf(val, "%d", j);
			if (rep == 2 && j == tablelen-2) {
				// Make the second last key
				// match an earlier one
				os::sprintf(key, "%d", totalrows-2);
			}
			if (seq == 0) {
				// Use add_element
				upd_v->add_element(key, val, e);
			} else {
				// Use add_elements
				// Const means eseq will make a copy
				eseq[j].key = (const char *)key;
				eseq[j].data = (const char *)val;
				if (j == tablelen-1) {
					upd_v->add_elements(eseq, e);
				}
			}
			if (rep && e.exception() &&
			    ccr::key_exists::_exnarrow(e.exception())) {
				// Exception expected
				os::printf("Got expected exception\n");
				e.clear();
				got_exception = true;
			} else if (e.exception()) {
				e.exception()->print_exception(
				    "ccr_update");
				return (1);
			}
		}
		if (slp) {
			os::printf("Sleeping for %d seconds\n", (int)slp);
			os::usecsleep(1000000*slp);
		}
		upd_v->commit_transaction(e);
		if (e.exception()) {
			e.exception()->print_exception("ccr_update");
			return (1);
		}
		if (rep && !got_exception) {
			// Didn't get expected exception
			// Throw an arbitrary exception
			os::printf("Didn't get expected key_exists exception");
//			workaround for memory leak in 4236225
//			e.exception(new ccr::no_such_key(""));
			e.exception(new ccr::no_such_key());
		}

		if (slp) {
			os::printf("Sleep for %d seconds\n", (int)slp/2);
			os::usecsleep(1000000*(slp/2));
		}
	}
	return (0);

}
// #endif

//
// Helper function to reads and verification.
//
int
do_reads(
	ccr::directory_var ccr_dir,
	char **argp,
	int nooftables,
	int tablelen,
	Environment &e)
{
	os::printf("do_reads\n");
	int i, j;
	int totalrows = 0;
	ccr::readonly_table_var rea_v;

	for (i = 0; i < nooftables; i++) {
		char *tablename = argp[i];
		rea_v = ccr_dir->lookup(tablename, e);
		if (e.exception()) {
			e.exception()->print_exception("readonly lookup");
			return (1);
		}

		for (j = 0; j < tablelen; j++, totalrows++) {
			char key[16];
			char exp_val[16];

			os::sprintf(key, "%d", totalrows);
			os::sprintf(exp_val, "%d", j);
			char *val = rea_v->query_element(key, e);
			if (e.exception()) {
				os::prom_printf("Exception in ccr_update:"
					"Error querying element %d of"
					"table %d", j, i);
				return (1);
			}
			if (os::strcmp(val, exp_val)) {
				os::prom_printf("ccr_update: Error querying "
					"element %d of table %d", j, i);
				return (1);
			}
			delete [] val;
		}
	}

	return (0);
}

//
// The simplest update client
// Always assumes that current table length is 10
//
int
ccr_simple_addelement(char *tablename, Environment &e)
{
	naming::naming_context_var ctxp = ns::local_nameserver();
	CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
	if (e.exception()) {
		os::prom_printf("ccr_directory not found in name server\n");
		return (1);
	}
	ccr::directory_var ccr_dir = ccr::directory::_narrow(obj);

	ccr::updatable_table_var upd_v;
	upd_v = ccr_dir->begin_transaction(tablename, e);
	if (e.exception()) {
		e.exception()->print_exception("ccr_update");
		return (1);
	}

	char	key[] = "10";
	upd_v->add_element(key, "10", e);
	if (e.exception()) {
		e.exception()->print_exception("ccr_update");
		return (1);
	}

	upd_v->commit_transaction(e);
	if (e.exception()) {
		e.exception()->print_exception("ccr_update");
		return (1);
	}
	return (0);
}

//
// The CCR simple update client - adds rows and queries to check them
// Argarray can include seq and/or rep1/rep2 and/or sleep
//
int
ccr_addelement(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void *argarray,
	int arglen,
	Environment &e)
{
// #if defined(_KERNEL)

	int tablelen = 10;
	int totalrows = 0;
	int nooftables = arglen-3;
	int seq = 0, rep = 0, slp = 0;

	char **argp = (char **)argarray;
	unsigned int where_faultpt = (unsigned int)os::atoi(argp[0]);
	int id = os::atoi(argp[1]);
	nodeid_t nodeid = (nodeid_t)id;
	// tablelen = os::atoi((const char *) *(argp+2));
	tablelen = os::atoi(argp[2]);
	argp += 3;

	// Handle special options
	if (os::strcmp(argp[0], "seq") == 0) {
		seq = 1;
		argp++;
		nooftables--;
	}
	if (os::strcmp(argp[0], "rep1") == 0) {
		rep = 1;
		argp++;
		nooftables--;
	}
	if (os::strcmp(argp[0], "rep2") == 0) {
		rep = 2;
		argp++;
		nooftables--;
	}
	if (os::strcmp(argp[0], "sleep") == 0) {
		// Sleep long enough for other node to go down
		// and come back up.

		// The unode version of the tests time out after
		// 180 secs, so the sleep interval should be diff
		// than in the kernel case. Setting it to 45.
#ifdef _KERNEL
		slp = 240;
#else
		slp = 45;
#endif
		argp++;
		nooftables--;
	}

	naming::naming_context_var ctxp = ns::local_nameserver();
	CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
	if (e.exception()) {
		os::prom_printf("ccr_directory not found in name server\n");
		return (1);
	}
	ccr::directory_var ccr_dir = ccr::directory::_narrow(obj);

	// Traverse table by table
	int i, j;

	int ha_flag = 0;

	if (where_faultpt) {
		ASSERT(on_fp != NULL && off_fp != NULL);
		if (where_faultpt % 2 == 0)
			ha_flag = 1;
		where_faultpt++;
		where_faultpt >>= 1;
	}

	if (where_faultpt == 1)
		arm_faults(on_fp, &nodeid, ha_flag);
	if (do_creates(ccr_dir, argp, nooftables, e))
		return (1);
	if (where_faultpt == 1)
		disarm_faults(off_fp, &nodeid);

	if (where_faultpt == 2)
		arm_faults(on_fp, &nodeid, ha_flag);
	(void) do_adds(ccr_dir, argp, nooftables, tablelen, seq, rep, slp, e);
	if (where_faultpt == 2)
		disarm_faults(off_fp, &nodeid);
	if (e.exception())
		return (1);

	if (where_faultpt < 3) {
		on_fp = NULL;
		off_fp = NULL;
	}

	// Verify data, unless this is a rep test.
	totalrows = 0;
	ccr::readonly_table_var rea_v;
	for (i = 0; rep == 0 && i < nooftables; i++) {
		char *tablename = argp[i];
		rea_v = ccr_dir->lookup(tablename, e);
		if (e.exception()) {
			e.exception()->print_exception("ccr_update");
			return (1);
		}

		for (j = 0; j < tablelen; j++, totalrows++) {
			char key[16];
			char exp_val[16];

			os::sprintf(key, "%d", totalrows);
			os::sprintf(exp_val, "%d", j);
			if (on_fp != NULL && i == 0 && j == 0)
				arm_faults(on_fp, &nodeid, ha_flag);
			char *val = rea_v->query_element(key, e);
			if (off_fp != NULL && i == 0 && j == 0)
				disarm_faults(off_fp, &nodeid);
			if (e.exception()) {
				os::prom_printf("Exception in ccr_update:"
					"Error querying element %d of"
					"table %d", j, i);
				return (1);
			}
			if (os::strcmp(val, exp_val)) {
				os::prom_printf("ccr_update: Error querying "
					"element %d of table %d", j, i);
				return (1);
			}
			delete [] val;
		}
	}

	if (do_removes(ccr_dir, argp, nooftables, e))
		return (1);

	return (0);
// #endif
}

//
// The CCR simple update client - adds/queries rows but more importantly
// verifies update_element
//
int
ccr_updateelement(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void *argarray,
	int arglen,
	Environment &e)
{
// #if defined(_KERNEL)
	on_fp = NULL;
	off_fp = NULL;
	int tablelen = 10;
	int totalrows = 0;
	int nooftables = arglen-3;

	char **argp = (char **)argarray;
	int id = os::atoi(argp[1]);
	nodeid_t nodeid = (nodeid_t)id;
	tablelen = os::atoi(argp[2]);
	argp += 3;

	naming::naming_context_var ctxp = ns::local_nameserver();
	CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
	if (e.exception()) {
		os::prom_printf("ccr_directory not found in name server\n");
		return (1);
	}
	ccr::directory_var ccr_dir = ccr::directory::_narrow(obj);

	// Traverse table by table

	int i, j;

	ccr::updatable_table_var upd_v;

	if (do_creates(ccr_dir, argp, nooftables, e))
		return (1);

	(void) do_adds(ccr_dir, argp, nooftables, tablelen, 0, 0, 0, e);
	if (e.exception())
		return (1);

	totalrows = 0;
	for (i = 0; i < nooftables; i++) {
		char *tablename = argp[i];

		upd_v = ccr_dir->begin_transaction(tablename, e);
		if (e.exception()) {
			e.exception()->print_exception("ccr_update");
			return (1);
		}
		for (j = 0; j < tablelen; j++, totalrows++) {
			char key[16];
			char val[16];
			os::sprintf(key, "%d", totalrows);
			os::sprintf(val, "%d", j+1);
			if (i == 0 && j == 0)
				arm_faults(on_fp, &nodeid);
			upd_v->update_element(key, val, e);
			if (i == 0 && j == 0)
				disarm_faults(off_fp);
			if (e.exception()) {
				e.exception()->print_exception("ccr_update");
				return (1);
			}
		}
		upd_v->commit_transaction(e);
		if (e.exception()) {
			e.exception()->print_exception("ccr_update");
			return (1);
		}
	}


	totalrows = 0;
	ccr::readonly_table_var rea_v;
	for (i = 0; i < nooftables; i++) {
		char *tablename = argp[i];
		rea_v = ccr_dir->lookup(tablename, e);
		if (e.exception()) {
			e.exception()->print_exception("ccr_update");
			return (1);
		}

		for (j = 0; j < tablelen; j++, totalrows++) {
			char key[16];
			char exp_val[16];

			os::sprintf(key, "%d", totalrows);
			os::sprintf(exp_val, "%d", j+1);
			char *val = rea_v->query_element(key, e);
			if (e.exception()) {
				os::prom_printf("Exception in ccr_update:"
					"Error querying element %d of"
					"table %d", j, i);
				return (1);
			}
			if (os::strcmp(val, exp_val)) {
				os::prom_printf("ccr_update: Error querying "
					"element %d of table %d", j, i);
				return (1);
			}
			delete [] val;
		}
	}

	if (do_removes(ccr_dir, argp, nooftables, e))
		return (1);

	return (0);

// #endif
}

//
// The CCR simple read client - calls next_element
//
int
ccr_nextelement(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void *argarray,
	int arglen,
	Environment &e)
{
	on_fp = NULL;
	off_fp = NULL;

	int tablelen = 10;
	int nooftables = arglen-2;

	tablelen = os::atoi((const char *) *((char **)argarray+1));
	char **argp = (char **)argarray + 2;

	naming::naming_context_var ctxp = ns::local_nameserver();
	CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
	if (e.exception()) {
		os::prom_printf("ccr_directory not found in name server\n");
		return (1);
	}
	ccr::directory_var ccr_dir = ccr::directory::_narrow(obj);

	int i;

	if (do_creates(ccr_dir, argp, nooftables, e))
		return (1);

	(void) do_adds(ccr_dir, argp, nooftables, tablelen, 0, 0, 0, e);
	if (e.exception())
		return (1);

	ccr::readonly_table_var rea_v;
	for (i = 0; i < nooftables; i++) {
		char *tablename = argp[i];
		rea_v = ccr_dir->lookup(tablename, e);
		if (e.exception()) {
			e.exception()->print_exception("ccr_update");
			return (1);
		}

		if ((int)rea_v->get_num_elements(e) != tablelen) {
			if (e.exception()) {
				os::prom_printf("get_num_elements failed\n");
				return (1);
			}
			os::prom_printf("Wrong no. of elements in table no."
				" %d", i);
			return (1);
		}
		if (e.exception()) {
			os::prom_printf("get_num_elements failed\n");
			return (1);
		}

		rea_v->atfirst(e);
		if (e.exception()) {
			e.exception()->print_exception("In atfirst");
			return (1);
		}

		int noofelems = 0;
		ccr::table_element *ret_elem;
		for (;;) {
			rea_v->next_element(ret_elem, e);
			if (e.exception()) {
				if (ccr::NoMoreElements::_exnarrow(
				    e.exception())) {
					// End of the table
					e.clear();
					break;
				} else {
					os::prom_printf("get_element failed\n");
					return (1);
				}
			}
			noofelems++;
			delete ret_elem;
		}
		if (tablelen != noofelems) {
			os::prom_printf("no. of elements returned %d not equal "
			    "to expected val %d\n", noofelems, tablelen);
			return (1);
		}
	}

	if (do_removes(ccr_dir, argp, nooftables, e))
		return (1);

	return (0);
} /*lint !e550 */

//
// The CCR remove_element and remove_elements ops.
//
int
ccr_removeelement(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void *argarray,
	int arglen,
	Environment &e)
{
// #if defined(_KERNEL)

	int tablelen = 10;
	int totalrows = 0;
	int nooftables = arglen-3;
	int seq = 0, slp = 0;

	char **argp = (char **)argarray;
	int where_faultpt = os::atoi(argp[0]);
	nodeid_t nodeid = (nodeid_t)os::atoi(argp[1]);
	tablelen = os::atoi(argp[2]);
	argp += 3;

	// Handle special options
	if (os::strcmp(argp[0], "seq") == 0) {
		seq = 1;
		argp++;
		nooftables--;
	}
	if (os::strcmp(argp[0], "sleep") == 0) {

		// The unode version of the tests time out after
		// 180 secs, so the sleep interval should be diff
		// than in the kernel case. Setting it to 45.
#ifdef _KERNEL
		slp = 240;
#else
		slp = 45;
#endif
		argp++;
		nooftables--;
	}

	naming::naming_context_var ctxp = ns::local_nameserver();
	CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
	if (e.exception()) {
		os::prom_printf("ccr_directory not found in name server\n");
		return (1);
	}
	ccr::directory_var ccr_dir = ccr::directory::_narrow(obj);

	int i, j;

	if (do_creates(ccr_dir, argp, nooftables, e))
		return (1);

	(void) do_adds(ccr_dir, argp, nooftables, tablelen, 0, 0, 0, e);
	if (e.exception())
		return (1);

	ccr::updatable_table_var upd_v;

	totalrows = 0;
	for (i = 0; i < nooftables; i++) {
		char *tablename = argp[i];
		upd_v = ccr_dir->begin_transaction(tablename, e);
		if (e.exception()) {
			e.exception()->print_exception("ccr_remove");
			return (1);
		}
		ccr::element_seq eseq((unsigned int)tablelen,
		    (unsigned int)tablelen);
		for (j = 0; j < tablelen; j++, totalrows++) {
			char key[16];
			os::sprintf(key, "%d", totalrows);
			if (i == 0 && j == 0) {
				if (where_faultpt == 1)
					arm_faults(on_fp, &nodeid);
				if (where_faultpt == 2)
					arm_faults(on_fp, &nodeid, 1);
			}
			if (seq) {
				// Collect up a sequence
				eseq[j].key = (const char *)key;
				if (j == tablelen-1) {
					// Send the sequence as a group
					os::printf("remove_elements\n");
					upd_v->remove_elements(eseq, e);
					if (i == 0) {
						disarm_faults(off_fp);
					}
				}
			} else {
				// Do removes one by one
				upd_v->remove_element(key, e);
				if (i == 0 && j == 0)
					disarm_faults(off_fp);
			}
			if (e.exception()) {
				e.exception()->print_exception(
				    "ccr_removeelement");
				return (1);
			}
		}
		if (slp) {
			// See what happens if we sleep before commit
			os::printf("Sleeping for %d seconds\n", (int)slp);
			os::usecsleep(1000000*slp);
		}
		upd_v->commit_transaction(e);
		if (e.exception()) {
			e.exception()->print_exception(
			    "ccr_removeelement");
			return (1);
		}
	}

	totalrows = 0;
	for (i = 0; i < nooftables; i++) {
		char *tablename = argp[i];
		upd_v = ccr_dir->begin_transaction(tablename, e);
		if (e.exception()) {
			e.exception()->print_exception(
			    "ccr_removeelement");
			return (1);
		}
		char key[16];
		char val[16];
		os::sprintf(key, "%d", totalrows);
		os::sprintf(val, "%d", 0);
		upd_v->add_element(key, val, e);
		if (e.exception()) {
			e.exception()->print_exception(
			    "ccr_removeelement");
			return (1);
		}
		upd_v->commit_transaction(e);
		if (e.exception()) {
			e.exception()->print_exception(
			    "ccr_removeelement");
			return (1);
		}

		totalrows += tablelen;
	}

	for (i = 0; i < nooftables; i++) {
		char *tablename = argp[i];
		upd_v = ccr_dir->begin_transaction(tablename, e);
		if (e.exception()) {
			e.exception()->print_exception(
			    "ccr_removeelement");
			return (1);
		}
		if (i == 0) {
			if (where_faultpt == 3)
				arm_faults(on_fp, &nodeid);
			if (where_faultpt == 4)
				arm_faults(on_fp, &nodeid, 1);
		}
		upd_v->remove_all_elements(e);
		if (i == 0)
			disarm_faults(off_fp);
		if (e.exception()) {
			e.exception()->print_exception(
			    "ccr_removeelement");
			return (1);
		}
		upd_v->commit_transaction(e);
		if (e.exception()) {
			e.exception()->print_exception(
			    "ccr_removeelement");
			return (1);
		}
	}

	if (do_removes(ccr_dir, argp, nooftables, e))
		return (1);

	return (0);
// #endif
}

//
// Checks abort_transaction.
//
int
ccr_abort(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void *argarray,
	int arglen,
	Environment &e)
{
// #if defined(_KERNEL)
	on_fp = NULL;
	off_fp = NULL;

	int tablelen = 10;
	int totalrows = 0;
	int nooftables = arglen-2;

	tablelen = os::atoi((const char *) *((char **)argarray+1));
	char **argp = (char **)argarray + 2;

	naming::naming_context_var ctxp = ns::local_nameserver();
	CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
	if (e.exception()) {
		os::prom_printf("ccr_directory not found in name server\n");
		return (1);
	}
	ccr::directory_var ccr_dir = ccr::directory::_narrow(obj);

	if (do_creates(ccr_dir, argp, nooftables, e))
		return (1);

	int i, j;

	totalrows = 0;
	ccr::updatable_table_var upd_v;
	for (i = 0; i < nooftables; i++) {
		char *tablename = argp[i];
		upd_v = ccr_dir->begin_transaction(tablename, e);
		if (e.exception()) {
			e.exception()->print_exception("ccr_update");
			return (1);
		}
		for (j = 0; j < tablelen; j++, totalrows++) {
			char key[16];
			char val[16];
			os::sprintf(key, "%d", totalrows);
			os::sprintf(val, "%d", j);
			upd_v->add_element(key, val, e);
			if (e.exception()) {
				e.exception()->print_exception("ccr_update");
				return (1);
			}
		}
		upd_v->commit_transaction(e);
		if (e.exception()) {
			e.exception()->print_exception("ccr_update");
			return (1);
		}

		upd_v = ccr_dir->begin_transaction(tablename, e);
		if (e.exception()) {
			e.exception()->print_exception("ccr_update");
			return (1);
		}
		upd_v->remove_all_elements(e);
		if (e.exception()) {
			e.exception()->print_exception("ccr_update");
			return (1);
		}
		upd_v->abort_transaction(e);
		if (e.exception()) {
			e.exception()->print_exception("ccr_update");
			return (1);
		}

		ccr::readonly_table_var rea_v;
		rea_v = ccr_dir->lookup(tablename, e);
		if (e.exception()) {
			e.exception()->print_exception("ccr_update");
			return (1);
		}
		if ((int)rea_v->get_num_elements(e) != tablelen) {
			os::prom_printf("Error in abort_transaction\n");
			return (1);
		}
		if (e.exception())
			e.clear();
	}

	if (do_removes(ccr_dir, argp, nooftables, e))
		return (1);

	return (0);
// #endif
} /*lint !e550 */

//
// Checks callback functionality
//
#define	CBTABLE "callback_test"
#define	num_cb 10

int
ccr_callback(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void *,
	int,
	Environment &e)
{
	on_fp = NULL;
	off_fp = NULL;
	// not used

	int tablelen = 10;
	int totalrows = 0;
	int j;

	naming::naming_context_var ctxp = ns::local_nameserver();
	CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
	if (e.exception()) {
		os::prom_printf("ccr_directory not found in name server\n");
		return (1);
	}
	ccr::directory_var ccr_dir = ccr::directory::_narrow(obj);

	ccr::readonly_table_var tab_v;

	// create table
	ccr_dir->create_table(CBTABLE, e);
	if (e.exception()) {
		os::prom_printf("error creating table %s\n", CBTABLE);
		return (1);
	}

	// create callback impl, get objref
	callback_impl *mycb[num_cb];
	ccr::callback_var cbptr[num_cb];
	for (j = 0; j < num_cb; j++) {
		mycb[j] = new callback_impl;
		cbptr[j] = mycb[j]->get_objref();
	}

	// register callback with table
	tab_v = ccr_dir->lookup(CBTABLE, e);
	if (e.exception()) {
		os::prom_printf("table %s not found\n", CBTABLE);
		return (1);
	}
	for (j = 0; j < num_cb; j++) {
		tab_v->register_callbacks(cbptr[j], e);
		if (e.exception()) {
			os::printf("register_callbacks failed\n");
			return (1);
		}
	}
	// register redundant objref - should get exception
	tab_v->register_callbacks(cbptr[0], e);
	if (!e.exception()) {
		os::printf("no exception in duplicate register\n");
		return (1);
	}
	e.clear();

	totalrows = 0;
	ccr::updatable_table_var upd_v;
	char *tablename = CBTABLE;
	upd_v = ccr_dir->begin_transaction(tablename, e);
	if (e.exception()) {
		e.exception()->print_exception("ccr_callback");
		return (1);
	}
	for (j = 0; j < tablelen; j++, totalrows++) {
		char key[16];
		char val[16];
		os::sprintf(key, "%d", totalrows);
		os::sprintf(val, "%d", j);
		upd_v->add_element(key, val, e);
		if (e.exception()) {
			e.exception()->print_exception("ccr_callback");
				return (1);
		}
	}
	upd_v->commit_transaction(e);
	if (e.exception()) {
		e.exception()->print_exception("ccr_callback");
		return (1);
	}

	// check callbacks succeeded and correct num elements read
	for (j = 0; j < num_cb; j++) {
		if (!mycb[j]->callback_done) {
			os::prom_printf("did_update callback not called\n");
			return (1);
		}
		if (mycb[j]->num_elements_read != tablelen) {
			os::prom_printf("did_update read wrong # of rows\n");
			return (1);
		}
	}

	// unregister callbacks
	tab_v = ccr_dir->lookup(CBTABLE, e);
	if (e.exception()) {
		os::prom_printf("table %s not found\n", CBTABLE);
		return (1);
	}

	// unregister callbacks
	for (j = 0; j < num_cb; j++) {
		tab_v->unregister_callbacks(cbptr[j], e);
		if (e.exception()) {
			os::prom_printf("unregister_callbacks failed\n");
			return (1);
		}
	}

	// unregister removed callback, should get exception
	tab_v->unregister_callbacks(cbptr[0], e);
	if (!e.exception()) {
		os::printf("no exception in duplicate unregister\n");
		return (1);
	}
	e.clear();

	ccr_dir->remove_table(CBTABLE, e);
	if (e.exception()) {
		os::prom_printf("error removing table %s\n", CBTABLE);
		return (1);
	}

	return (0);
} /*lint !e550 */

//
// callback methods
//
callback_impl::callback_impl():
	callback_done(false),
	num_elements_read(0)
{
}

void
callback_impl::did_update_zc(
    const char *,
    const char *,
    ccr::ccr_update_type,
    Environment &)
{
	ASSERT(0);
}

void
callback_impl::did_update(
    const char *tname,
    ccr::ccr_update_type,
    Environment &)
{
	ccr::directory_var ccr_dir;
	ccr::readonly_table_var tab_v;
	Environment e;

	// look up ccr directory in name server
	naming::naming_context_var ctxp = ns::local_nameserver();
	CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
	if (e.exception()) {
		e.exception()->print_exception("ccr_callback");
		e.clear();
		return;
	}
	ccr_dir = ccr::directory::_narrow(obj);

	// lookup table we got callback for
	tab_v = ccr_dir->lookup(tname, e);
	if (e.exception()) {
		e.exception()->print_exception("ccr_callback");
		e.clear();
		return;
	}
	num_elements_read = (int)tab_v->get_num_elements(e);
	if (e.exception()) {
		e.exception()->print_exception("ccr_callback");
		e.clear();
		return;
	}
	callback_done = true;
	e.clear();
}

void
callback_impl::did_recovery_zc(const char *, const char *,
    ccr::ccr_update_type, Environment &)
{
}

void
callback_impl::did_recovery(const char *, ccr::ccr_update_type,
    Environment &)
{
}

void
#ifdef DEBUG
callback_impl::_unreferenced(unref_t cookie)
#else
callback_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	delete this;
}

//
// Writes and reads long values. Over iterations, grows value length
// upto a specified param or a default if the param was not specified
//
int
ccr_longelement(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void *argarray,
	int arglen,
	Environment &e)
{
	on_fp = NULL;
	off_fp = NULL;

	int totalrows = 0;

	ASSERT(arglen == 2 || arglen == 3);

	char **argp = (char **)argarray + 1;

	naming::naming_context_var ctxp = ns::local_nameserver();
	CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
	if (e.exception()) {
		os::prom_printf("ccr_directory not found in name server\n");
		return (1);
	}
	ccr::directory_var ccr_dir = ccr::directory::_narrow(obj);

	int i, j;
	unsigned int valuelen = 16;
	unsigned int max_valuelen = 1024;
	if (arglen == 3)
		max_valuelen = (unsigned int)os::atoi((const char *) argp[1]);
	char *val = new char[(unsigned int)max_valuelen+1];

	totalrows = 0;
	ccr::updatable_table_var upd_v;
	char *tablename = *argp;
	ccr_dir->create_table(tablename, e);
	if (e.exception()) {
		e.exception()->print_exception("ccr_update");
		return (1);
	}
	upd_v = ccr_dir->begin_transaction(tablename, e);
	if (e.exception()) {
		e.exception()->print_exception("ccr_update");
		return (1);
	}
	while (valuelen <= max_valuelen) {
		char key[16];
		os::sprintf(key, "%d", totalrows);
		for (i = 0; i < (int)valuelen; i++)
			val[i] = 'A';
		val[i] = '\0';
		upd_v->add_element(key, val, e);
		if (e.exception()) {
			e.exception()->print_exception("ccr_update");
			return (1);
		}
		totalrows++;
		valuelen <<= 1;
	}
	upd_v->commit_transaction(e);
	if (e.exception()) {
		e.exception()->print_exception("ccr_update");
		return (1);
	}

	totalrows = 0;
	ccr::readonly_table_var rea_v;

	rea_v = ccr_dir->lookup(tablename, e);
	if (e.exception()) {
		e.exception()->print_exception("ccr_update");
		return (1);
	}

	valuelen = 8;
	for (j = 0; j < totalrows; j++) {
		char key[16];

		os::sprintf(key, "%d", j);
		for (i = 0; i < (int)valuelen; i++)
			val[i] = 'A';
		val[i] = '\0';
		char *rd_val = rea_v->query_element(key, e);
		if (e.exception()) {
			os::prom_printf("Exception in ccr_update:"
				"Error querying element %d of"
				"table %d", j, i);
			return (1);
		}
		if (os::strcmp(rd_val, val)) {
				os::prom_printf("ccr_update: Error querying "
					"element %d of table %d", j, i);
				return (1);
		}
		delete [] rd_val;

		valuelen <<= 1;
	}

	ccr_dir->remove_table(tablename, e);
	if (e.exception()) {
		e.exception()->print_exception("ccr_update: "
			"Error in remove_table");
		return (1);
	}

	delete [] val;
	return (0);
} /*lint !e550 */ 

/*
//
// Find the first non-root node
//
nodeid_t
any_non_root()
{
#if defined(_KERNEL) || defined(_UNODE)
	nodeid_t root_nodeid = clconf_get_root_nodeid(0);
	nodeid_t max_nodeid = NODEID_MAX;
	nodeid_t nodeid = 1;

	while (nodeid <= max_nodeid) {
		if (nodeid == root_nodeid) {
			nodeid++;
			continue;
		}
		if (orb_conf::node_configured(nodeid))
			return (nodeid);
		nodeid++;
	}
#endif

	return (0);
}

*/

/*
//
// The CCR simple update client - adds rows and queries to check them
//
int
ccr_addele(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void *argarray,
	int arglen,
	Environment &e)
{
// #if defined(_KERNEL)

	int tablelen = 10;
	int totalrows = 0;
	int nooftables = arglen-2;

	ASSERT(arglen >= 3);

	// nodeid_t id =
	// (nodeid_t)(os::atoi((const char *) *((char **)argarray+1)));
	//
	tablelen = os::atoi((const char *) *((char **)argarray+1));
	char **argp = (char **)argarray + 2;

	naming::naming_context_var ctxp = ns::local_nameserver();
	CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
	if (e.exception()) {
		os::prom_printf("ccr_directory not found in name server\n");
		return (1);
	}

	ccr::directory_var ccr_dir = ccr::directory::_narrow(obj);

	// Traverse table by table

	int i, j;

	if (do_creates(ccr_dir, argp, nooftables, e))
		return (1);

	arm_faults(on_fp);

	(void) do_adds(ccr_dir, argp, nooftables, tablelen, 0, 0, 0, e);
	if (e.exception())
		return (1);

	disarm_faults(off_fp);

	totalrows = 0;
	ccr::readonly_table_var rea_v;
	for (i = 0; i < nooftables; i++) {
		char *tablename = argp[i];
		rea_v = ccr_dir->lookup(tablename, e);
		if (e.exception()) {
			e.exception()->print_exception("ccr_update");
			return (1);
		}

		for (j = 0; j < tablelen; j++, totalrows++) {
			char key[16];
			char exp_val[16];

			os::sprintf(key, "%d", totalrows);
			os::sprintf(exp_val, "%d", j);
			char *val = rea_v->query_element(key, e);
			if (e.exception()) {
				os::prom_printf("Exception in ccr_update:"
					"Error querying element %d of"
					"table %d", j, i);
				return (1);
			}
			if (os::strcmp(val, exp_val)) {
				os::prom_printf("ccr_update: Error querying "
					"element %d of table %d", j, i);
				return (1);
			}
			delete [] val;
		}
	}

	if (do_removes(ccr_dir, argp, nooftables, e))
		return (1);

	return (0);
// #endif
}
*/

//
// Function creates a discrepancy in the ccr-image on the client node.
// That is, it puts data out-of-sync with respect to the rest of the
// cluster. Then, the function reboots the out-of-sync node.
//
#if defined(_KERNEL) || defined(_UNODE)
int
ccr_overwrite_with_reboot(
	void *,
	int,
	Environment &)
{
	return (0);
}
#else
int
ccr_overwrite_with_reboot(
	void *argarray,
	int,
	Environment &)
{
	char **argp = (char **)argarray + 1;

	char *tablename = argp[0];
	char filename[100] = "/etc/cluster/ccr/global/";

	(void) strcpy(filename+(int)strlen(filename), tablename);
	int fd = open(filename, O_WRONLY | O_TRUNC);
	(void) close(fd);

#if defined(_FAULT_INJECTION)
	FaultFunctions::wait_for_arg_t  wait_arg;

	wait_arg.op = FaultFunctions::WAIT_FOR_UNKNOWN;
	wait_arg.nodeid = 0;
	FaultFunctions::reboot(0, &wait_arg, (uint32_t)sizeof (wait_arg));
#endif

	return (0);
}
#endif

//
// Function creates a discrepancy in the ccr-image on the client node.
// That is, it puts data out-of-sync with respect to the rest of the
// cluster by zero'ing out the file given
//
#if defined(_KERNEL) || defined(_UNODE)
int
ccr_overwrite_without_reboot(
	void *,
	int,
	Environment &)
{
	return (0);
}
#else
int
ccr_overwrite_without_reboot(
	void *argarray,
	int,
	Environment &)
{
	os::printf("In ccr_overwrite_without_reboot\n");
	char **argp = (char **)argarray;

	char *tablename = argp[1];

	os::printf("argp[3] is %d\n", os::atoi(argp[3]));
	if (os::atoi(argp[3]) == 1) {
		char filename[100] = "/etc/cluster/ccr/global/";
		char backup_fname[150];
		(void) strcpy(filename+(int)strlen(filename), tablename);
		(void) strcpy(backup_fname, filename);
		(void) strcpy(backup_fname + (int)strlen(filename),
		    ".test.bak");
		os::printf("Filename is %s, backup is %s\n", filename,
		    backup_fname);
		(void) os::file_copy(filename, backup_fname);
		int fd = open(filename, O_WRONLY | O_TRUNC);
		(void) close(fd);
	} else {
		char backup_fname[100] = "/etc/cluster/ccr/global/";
		char filename[150];
		(void) strcpy(backup_fname+(int)strlen(backup_fname),
		    tablename);
		(void) strcpy(filename, backup_fname);
		(void) strcpy(filename + (int)strlen(backup_fname),
		    ".test.bak");
		os::printf("Filename is %s, backup is %s\n", filename,
		    backup_fname);
		(void) os::file_copy(filename, backup_fname);
		int fd = open(filename, O_WRONLY | O_TRUNC);
		(void) close(fd);
	}

	return (0);
}
#endif

//
// Function creates a discrepancy in the ccr-image on the client node.
// That is, it puts data out-of-sync with respect to the rest of the
// cluster by deleting the file given
//
#if defined(_KERNEL) || defined(_UNODE)
int
ccr_delete_without_reboot(
	void *,
	int,
	Environment &)
{
	return (0);
}
#else
int
ccr_delete_without_reboot(
	void *argarray,
	int,
	Environment &)
{
	os::printf("In ccr_delete_without_reboot\n");
	char **argp = (char **)argarray;

	char *tablename = argp[1];

	os::printf("argp[3] is %d\n", os::atoi(argp[3]));
	if (os::atoi(argp[3]) == 1) {
		char filename[100] = "/etc/cluster/ccr/global/";
		char backup_fname[150];
		(void) strcpy(filename+(int)strlen(filename), tablename);
		(void) strcpy(backup_fname, filename);
		(void) strcpy(backup_fname + (int)strlen(filename),
		    ".test.bak");
		os::printf("Filename is %s, backup is %s\n", filename,
		    backup_fname);
		(void) os::file_rename(filename, backup_fname);
	} else {
		char backup_fname[100] = "/etc/cluster/ccr/global/";
		char filename[150];
		(void) strcpy(backup_fname+(int)strlen(backup_fname),
		    tablename);
		(void) strcpy(filename, backup_fname);
		(void) strcpy(filename + (int)strlen(backup_fname),
		    ".test.bak");
		os::printf("Filename is %s, backup is %s\n", filename,
		    backup_fname);
		(void) os::file_rename(filename, backup_fname);
	}
	return (0);
}
#endif

//
// Function to create a table with data - it returns without removing the
// data.
//
int
ccr_preparedata(
	void *argarray,
	int arglen,
	Environment &e)
{
// #if defined(_KERNEL)
	int tablelen = 10;
	int nooftables = arglen-2;

	tablelen = os::atoi((const char *) *((char **)argarray+1));
	char **argp = (char **)argarray + 2;

	naming::naming_context_var ctxp = ns::local_nameserver();
	CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
	if (e.exception()) {
		os::prom_printf("ccr_directory not found in name server\n");
		return (1);
	}
	ccr::directory_var ccr_dir = ccr::directory::_narrow(obj);

	if (do_creates(ccr_dir, argp, nooftables, e))
		return (1);

	(void) do_adds(ccr_dir, argp, nooftables, tablelen, 0, 0, 0, e);
	if (e.exception())
		return (1);

// #endif
	return (0);
}

//
// Function to verify data.
//
int
ccr_verifydata(
	void *argarray,
	int arglen,
	Environment &e)
{
// #if defined(_KERNEL)
	int tablelen = 10;
	int nooftables = arglen-2;

	tablelen = os::atoi((const char *) *((char **)argarray+1));
	char **argp = (char **)argarray + 2;

	naming::naming_context_var ctxp = ns::local_nameserver();
	CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
	if (e.exception()) {
		os::prom_printf("ccr_directory not found in name server\n");
		return (1);
	}
	ccr::directory_var ccr_dir = ccr::directory::_narrow(obj);

	(void) do_reads(ccr_dir, argp, nooftables, tablelen, e);
	if (e.exception()) {
		os::printf("FAIL: Could not read table\n");
		return (1);
	}

	if (do_removes(ccr_dir, argp, nooftables, e)) {
		os::printf("FAIL: Could not remove table\n");
		return (1);
	}

// #endif
	return (0);
}
