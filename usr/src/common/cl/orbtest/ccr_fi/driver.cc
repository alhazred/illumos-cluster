/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)driver.cc	1.20	08/05/20 SMI"

#if defined(_KERNEL)
#include <sys/modctl.h>
#else
#include <orb/infrastructure/orb.h>
#endif

#include <orbtest/fi_support/fi_driver.h>

#include <orbtest/ccr_fi/faults/entry_ccr_clnt.h>

// List of test entry functions.
fi_entry_t fi_entry[] = {
	ENTRY(ccr_add_basic),
	ENTRY(ccr_update_basic),
	ENTRY(ccr_directory_updmeth),
	ENTRY(ccr_next_a),
	ENTRY(ccr_remove_basic),
	ENTRY(ccr_abort_basic),
	ENTRY(ccr_callback_basic),
	ENTRY(ccr_longvalues),
	ENTRY(ccr_add_updmeth),
	ENTRY(ccr_add_commupd),
	ENTRY(ccr_remove_updmeth),
	ENTRY(ccr_removeall_updmeth),
	ENTRY(ccr_remove_commupd),
	ENTRY(ccr_removeall_commupd),
	ENTRY(ccr_update_updmeth),
	ENTRY(ccr_recovery),
	ENTRY(ccr_setup_recovery_test),
	ENTRY(ccr_set_recovery_reboot),
	ENTRY(ccr_recovery_cleanup),
	ENTRY(ccr_node_reboot),
	ENTRY(ccr_check_and_fix_invalid),
	ENTRY(ccr_set_override),
	ENTRY(ccr_verify),
	ENTRY(ccr_absentnode),
	ENTRY(ccr_transaction_error),
	ENTRY(ccr_transaction),
	ENTRY(haccr_add_a),
	ENTRY(haccr_add_b),
	ENTRY(haccr_create_a),
	ENTRY(haccr_create_b),
	ENTRY(haccr_query_a),
	ENTRY(haccr_query_b),
	ENTRY(haccr_remove_a),
	ENTRY(haccr_remove_b),
	ENTRY(haccr_removeall_a),
	ENTRY(haccr_removeall_b),
	ENTRY(switch_ccr_tm_primary),
	ENTRY(NULL)
};


#if ! defined(_KERNEL)
int
main(int argc, char **argv)
{
	os::printf(" +++ Loading userland test driver\n");

	int	rslt;

	// Initialize orb.
	if (ORB::initialize() != 0) {
		os::printf("Failed to initialize orb\n");
		return (1);
	}

	rslt = do_fi_test(argc, argv, fi_entry);
	os::printf("    Result From Module: %d\n", rslt);
	return (rslt);
}

#else // ! _KERNEL

extern "C" {
	void _cplpl_init(void);
	void _cplpl_fini(void);
}

char _depends_on[] = "misc/cl_runtime misc/cl_load misc/cl_orb "
	"misc/cl_haci misc/cl_quorum misc/cl_comm misc/fi_driver";

// extern struct mod_ops mod_miscops;

struct modlmisc modlmisc = {
	&mod_miscops, "ccr fi test driver"
};

struct modlinkage modlinkage = {
	MODREV_1, { (void *)&modlmisc, NULL }
};

int
_init(void)
{
	os::printf(" +++ Loading kernel test driver\n");

	int		error = 0;
	struct modctl	*mp;
	int		argc, i;
	char		**argv;

	if (error = mod_install(&modlinkage)) {
		return (error);
	}
	_cplpl_init();				// C++ initialization

	// Get module name so we know what test case to call
	if ((mp = mod_getctl(&modlinkage)) == NULL) {
		os::printf("Can't find modctl\n");
		_cplpl_fini();
		(void) mod_remove(&modlinkage);
		return (EIO);
	}

	// Parse module name to get arguments
	mod_name_to_args(mp->mod_modname, argc, argv);

	// Do the test.
	error = do_fi_test(argc, argv, fi_entry);
	os::printf("    Result From do_fi_test: %d\n", error);

	// Clean up argument vectors allocated by mod_name_to_args().
	for (i = 0; i < argc; ++i) {
		delete[] argv[i];
	}
	delete[] argv;

	os::printf(" +++ Unloading kernel test driver in _init()\n");
	_cplpl_fini();
	error += mod_remove(&modlinkage);
	os::printf("    Result From Unloading Module: %d\n", error);

	return (error);
}

int
_fini(void)
{
	os::printf(" +++ Unloading kernel test driver in _fini()\n");

	return (0);
}

int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}

#endif // ! _KERNEL
