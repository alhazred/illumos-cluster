/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)entry_ccr_clnt.cc	1.29	08/05/27 SMI"

#include	<sys/os.h>
#include	<orb/invo/common.h>
#include	<orb/infrastructure/orb_conf.h>
#include	<orb/fault/fault_injection.h>
#include	<orbtest/ccr_fi/faults/entry_common.h>
#include	<orbtest/ccr_fi/ccr_clnt.h>
#include	<orbtest/ccr_fi/faults/fault_ccr_update_1_a.h>
#include	<ccr/fault_ccr.h>
#include	<sys/vnode.h>

#include	<orbtest/repl_tests/repl_test_switch.h>
#include	<orbtest/rm_probe/rm_snap.h>

#if	!defined(_KERNEL)
#include	<stdlib.h>
#include	<stdio.h>
#define	MAXLINE 256
#endif

//
// CCR AddElement with FI-update_method
//
int
ccr_add_updmeth(int arglen, char **argp)
{
	os::prom_printf("*** CCR AddElement Case UpdateMethodFI\n");

#if defined(_FAULT_INJECTION)
	Environment			e;
	int				rslt;

	*argp[0] = '3';
	*(argp[0]+1) = '\0';
	rslt = ccr_addelement(
		fault_ccr_update_1_a_on,
		fault_ccr_update_1_a_off,
		argp,
		arglen,
		e);
	if (e.exception() || rslt) {
		ccr_cleanup(argp, arglen);
	}
	if (e.exception()) {
		e.exception()->print_exception("ccr_add_updmeth");
		return (1);
	}

	return (rslt);

#else
	arglen = 0;
	argp = NULL;
	return (no_fault_injection());
#endif

}

//
// CCR AddElement with FI-commit_update
//
int
ccr_add_commupd(int arglen, char **argp)
{
	os::prom_printf("*** CCR AddElement Case CommitUpdateFI\n");

#if defined(_FAULT_INJECTION)
	Environment			e;
	int				rslt;

	*argp[0] = '3';
	*(argp[0]+1) = '\0';
	rslt = ccr_addelement(
		fault_ccr_update_1_e_on,
		fault_ccr_update_1_e_off,
		argp,
		arglen,
		e);
	if (e.exception() || rslt) {
		ccr_cleanup(argp, arglen);
	}
	if (e.exception()) {
		e.exception()->print_exception("ccr_add_commupd");
		return (1);
	}

	return (rslt);

#else
	arglen = 0;
	argp = NULL;
	return (no_fault_injection());
#endif

}

//
// CCR RemoveElement with FI-commit_update while removing 1 element
//
int
ccr_remove_commupd(int arglen, char **argp)
{
	os::prom_printf("*** CCR RemoveElement "
		"Case CommitUpdateFI_1_remove \n");

#if defined(_FAULT_INJECTION)
	Environment			e;
	int				rslt;

	*argp[0] = '1';
	*(argp[0]+1) = '\0';
	rslt = ccr_removeelement(
		fault_ccr_update_1_e_on,
		fault_ccr_update_1_e_off,
		argp,
		arglen,
		e);
	if (e.exception() || rslt) {
		ccr_cleanup(argp, arglen);
	}
	if (e.exception()) {
		e.exception()->print_exception("ccr_remove_commupd");
		return (1);
	}

	return (rslt);

#else
	arglen = 0;
	argp = NULL;
	return (no_fault_injection());
#endif

}

//
// CCR RemoveElement with FI-commit_update while removing all elements
//
int
ccr_removeall_commupd(int arglen, char **argp)
{
	os::prom_printf("*** CCR RemoveElement "
		"Case CommitUpdateFI_all_remove\n");

#if defined(_FAULT_INJECTION)
	Environment			e;
	int				rslt;

	*argp[0] = '3';
	*(argp[0]+1) = '\0';
	rslt = ccr_removeelement(
		fault_ccr_update_1_e_on,
		fault_ccr_update_1_e_off,
		argp,
		arglen,
		e);
	if (e.exception() || rslt) {
		ccr_cleanup(argp, arglen);
	}
	if (e.exception()) {
		e.exception()->print_exception("ccr_removeall_commupd");
		return (1);
	}

	return (rslt);

#else
	arglen = 0;
	argp = NULL;
	return (no_fault_injection());
#endif

}

//
// CCR Recovery Test Case - verification
//
int
ccr_verify(int arglen, char **argp)
{
	os::prom_printf("*** CCR Recovery basic - verify\n");

#if defined(_FAULT_INJECTION)

	Environment			e;
	int				rslt;

	rslt = ccr_verifydata(
		argp,
		arglen,
		e);
	if (e.exception() || rslt) {
		ccr_cleanup(argp, arglen);
	}
	if (e.exception())
		e.exception()->print_exception("ccr_recovery");
	return (rslt);
#else
	arglen = 0;
	argp = NULL;
	return (no_fault_injection());
#endif
}

//
// CCR Recovery - absent node
//
int
ccr_absentnode(int arglen, char **argp)
{
	os::prom_printf("*** CCR Recovery - absent node\n");

#if defined(_FAULT_INJECTION)

	Environment			e;
	int				rslt;

	int id = (int)os::atoi(argp[1]);
	FaultFunctions::wait_for_arg_t  wait_arg;

	wait_arg.op = FaultFunctions::WAIT_FOR_UNKNOWN;
	wait_arg.nodeid = (nodeid_t)id;
	FaultFunctions::reboot(0, &wait_arg, (uint32_t)sizeof (wait_arg));

	rslt = ccr_preparedata(
		argp+1,
		arglen-1,
		e);
	if (e.exception() || rslt) {
		ccr_cleanup(argp, arglen);
	}
	if (e.exception()) {
		e.exception()->print_exception("ccr_recovery");
		return (1);
	}

	return (rslt);
#else
	arglen = 0;
	argp = NULL;
	return (no_fault_injection());
#endif

}	

//
// CCR Recovery Test Case
//
int
ccr_recovery(int arglen, char **argp)
{
	os::prom_printf("*** CCR Recovery basic\n");

#if defined(_FAULT_INJECTION)

	Environment			e;
	int				rslt;

	if (arglen > 2) {
		rslt = ccr_preparedata(
			argp,
			arglen,
			e);
		if (e.exception() || rslt) {
			ccr_cleanup(argp, arglen);
		}
		if (e.exception()) {
			e.exception()->print_exception("ccr_recovery");
			return (1);
		}
		return (rslt);
	}
	rslt = ccr_overwrite_with_reboot(
		argp,
		arglen,
		e);
	if (e.exception() || rslt) {
		ccr_cleanup(argp, arglen);
	}
	if (e.exception()) {
		e.exception()->print_exception("ccr_recovery");
		return (1);
	}

	return (rslt);
#else
	arglen = 0;
	argp = NULL;
	return (no_fault_injection());
#endif

}

//
// ccr_recovery_cleanup
// Removes all triggers, both NodeTriggers and BootTriggers
// that were used during the test.
//
int
ccr_recovery_cleanup(int argc, char **)
{
#if	defined(_FAULT_INJECTION)

#if	defined(_KERNEL)
	if (argc > 1) {
		os::usecsleep(30 * 1000000);
	}
#else
	argc;	// To shut compiler warning
#endif

	NodeTriggers::clear_all();
	BootTriggers::clear_all();
	os::printf("PASS: Completed cleanup\n");
	return (0);
#else
	argc;	// To shut compiler warning
	return (no_fault_injection());
#endif
}

//
// CCR Additional Recovery Test Cases
// Setup out-of-sync data on current node.
// Name of the file to be overwritten should be passed.
//
// Usage:	ccr_setup_recovery_test tablename mode bak table
//	tablename	the name of the table to be corrupted
//	mode	0 means zero out the file
//			1 means delete the file
//	bak		1 means copy the file to .bak
//			0 means copy the .bak file to the tablefile
//	table	1 means directory (only currently supported option)
//
int
ccr_setup_recovery_test(int arglen, char **argp)
{
	os::prom_printf("*** CCR Setup Recovery Test\n");

#if defined(_FAULT_INJECTION)

	Environment			e;
	int				rslt;

	if (os::atoi(argp[2]) == 0) {
		rslt = ccr_overwrite_without_reboot(
			argp,
			arglen,
			e);
		if (e.exception() || rslt) {
			ccr_cleanup(argp, arglen);
		}
		if (e.exception()) {
			e.exception()->print_exception("ccr_recovery");
			return (1);
		}
	} else {
		rslt = ccr_delete_without_reboot(
			argp,
			arglen,
		    e);
		if (e.exception() || rslt) {
			ccr_cleanup(argp, arglen);
		}
		if (e.exception()) {
			e.exception()->print_exception("ccr_recovery");
			return (1);
		}
	}

	if (arglen == 5) {
		os::printf("Last argument is %d\n", argp[4]);
		int fnum = 0;

		int num = os::atoi(argp[4]);
		switch (num) {
		case 1:
			fnum = FAULTNUM_CCR_RESTORE_TABLE;
			break;
		default:
			os::printf("FAIL: Unknown fault point\n");
			return (1);
		}

		fault_ccr::ccr_arg_t	arg_t;
		arg_t.op = fault_ccr::RESTORE_TABLE;
		arg_t.table = fault_ccr::DIRECTORY;
		BootTriggers::add((unsigned int)fnum, &arg_t,
		    (unsigned)sizeof (arg_t), (int)TRIGGER_THIS_NODE);
	}

	if (rslt) {
		os::printf("FAIL: setup_recovery_test failed with value %d\n",
		    rslt);
	} else {
		os::printf("PASS: Completed setup_recovery_test.\n");
	}
	return (rslt);
#else
	arglen = 0;
	argp = NULL;
	return (no_fault_injection());
#endif
}

//
// ccr_check_and_fix_invalid
// Run after a table has been invalidated on the cluster. It checks
// to make sure /var/adm/messages has an invlid message for
// the tablename then fixes the problem using ccradm.
//
// Usage:	ccr_check_and_fix_invalid tablename
//
int
ccr_check_and_fix_invalid(int argc, char **argv)
{
#if	defined(_KERNEL)
	argc;		/*lint !e522 */
	argv;		/*lint !e522 */
	os::printf("FAIL: Must be run in user mode\n");
	return (1);
#else

#if	defined(_FAULT_INJECTION)
	if (argc != 2) {
		os::printf("FAIL: Usage is ccr_check_and_fix_invalid "
		    "tablename\n");
		return (1);
	}

	int		rslt = 0;
	char	cmd[256];

	//
	// Workaround for bugid 4374814. We must make sure that the
	// table exists to run ccradm.
	//
	sprintf(cmd, "touch /etc/cluster/ccr/global/%s", argv[1]);
	os::printf("Command is %s\n", cmd);
	rslt += system(cmd);
	os::printf("Result is %d\n", rslt);

	sprintf(cmd, "/usr/cluster/lib/sc/ccradm -r "
	    "/etc/cluster/ccr/global/%s" " -f "
	    "/etc/cluster/ccr/global/%s.test.bak", argv[1], argv[1]);
	os::printf("Command is %s\n", cmd);
	rslt += system(cmd);
	os::printf("Result is %d\n", rslt);

	if (!rslt)  {
		printf("PASS: Checked and fixed invalid table\n");
	} else {
		printf("FAIL: Could not find or fix invalid table\n");
	}
	return (rslt);
#else
	argc;		/*lint !e522 */
	argv;		/*lint !e522 */
	return (no_fault_injection());
#endif // _FAULT_INJECTION

#endif // _KERNEL
} /*lint !e550*/

//
// get_fault_num
// given an int, return the fault number to which it refers
//
int
get_fault_num(int num) {
	int fnum = 0;

	switch (num) {
		case 1:
			fnum = FAULTNUM_CCR_DO_RECOVERY_1;
			break;
		case 2:
			fnum = FAULTNUM_CCR_DO_RECOVERY_2;
			break;
		case 3:
			fnum = FAULTNUM_CCR_DO_RECOVERY_3;
			break;
		case 4:
			fnum = FAULTNUM_CCR_DO_RECOVERY_4;
			break;
		case 5:
			fnum = FAULTNUM_CCR_DO_RECOVERY_5;
			break;
		case 6:
			fnum = FAULTNUM_CCR_PROPAGATE_DATA_1;
			break;
		case 7:
			fnum = FAULTNUM_CCR_PROPAGATE_DATA_2;
			break;
		case 8:
			fnum = FAULTNUM_CCR_PROPAGATE_TABLE_1;
			break;
		case 9:
			fnum = FAULTNUM_CCR_PROPAGATE_TABLE_2;
			break;
		case 10:
			fnum = FAULTNUM_CCR_GET_CONSIS_IN_1;
			break;
		case 11:
			fnum = FAULTNUM_CCR_GET_CONSIS_IN_2;
			break;
		case 12:
			fnum = FAULTNUM_CCR_STARTUP_1;
			break;
		case 13:
			fnum = FAULTNUM_CCR_STARTUP_2;
			break;
		case 14:
			fnum = FAULTNUM_CCR_PREPARE_UPDATE_COPIES;
			break;
		case 15:
			fnum = FAULTNUM_CCR_COMMIT_UPDATE_COPIES;
			break;
		case 16:
			fnum = FAULTNUM_CCR_COMMIT_TRANSACTION_1;
			break;
		case 17:
			fnum = FAULTNUM_CCR_COMMIT_TRANSACTION_2;
			break;
		case 18:
			fnum = FAULTNUM_CCR_SYSTEM_ERROR_CLUSTER;
			break;
		default:
			os::printf("FAIL: Unknown fault number\n");
			return (0);
		}
	return (fnum);
}


//
// CCR do_recovery reboot setup
// Setups up test fault points for tests in which nodes
// are rebooted during do_recovery.
//
// Usage: ccr_set_recovery_reboot nodeid fnum b/n
//
// nodeid	0 if want to reboot other node
//			1 if want to reboot current node
// fnum		Number of the fault to be triggered
// b/n		0 if want to set BootTrigger
//			1 if want to set NodeTrigger
// t/a		0 if want to trigger this node
//			1 if want to trigger all nodes
// r/se		0 if want to reboot
//			1 if want system error
//
// A Note on the use of set_recovery_reboot:
//
// In many of the test cases, set_recovery_reboot is run on all nodes,
// and each node is told to set a trigger for itself. In this case, the
// test can cause either all nodes to reboot or just one, depending on
// where in the code the fault point is set.
//
// For instance, if the fault point is set in the TM code, then only the
// single node that is selected as TM will reboot. However, if the fault
// point is in code that is executed on all nodes, then all nodes will
// reboot.
//
int
ccr_set_recovery_reboot(int argc, char **argv)
{
	os::prom_printf("*** CCR Set do_recovery Reboot\n");

#if	defined(_FAULT_INJECTION)

	if (argc < 4) {
		os::printf("FAIL: Usage is ccr_set_recovery_reboot nodeid fnum"
		    " bn ta\n");
		return (1);
	}

	int num = os::atoi(argv[2]);
	int node_num = os::atoi(argv[1]);
	int bn = os::atoi(argv[3]);
	int ta = os::atoi(argv[4]);
	int rse = os::atoi(argv[5]);

	int fnum = get_fault_num(num);
	if (fnum == 0) {
		return (1);
	}

	fault_ccr::ccr_arg_t	arg_t;
	if (!rse) {
		arg_t.op = fault_ccr::REBOOT_NODE;
		if (node_num != 0) {
			arg_t.nodenum = orb_conf::local_nodeid();
		} else {
			arg_t.nodenum = (nodeid_t)node_num;
		}
	} else {
		arg_t.op = fault_ccr::SET_SYSTEM_ERROR;
		arg_t.nodenum = (nodeid_t)node_num;
	}

	if (!ta) {
		ta = (int)TRIGGER_THIS_NODE;
	} else {
		ta = TRIGGER_ALL_NODES;
	}

	if (!bn) {
		BootTriggers::add((unsigned int)fnum, &arg_t,
		    (unsigned)sizeof (arg_t), ta);
	} else {
		NodeTriggers::add((unsigned int)fnum, &arg_t,
		    (unsigned)sizeof (arg_t), ta);
	}

	os::printf("PASS: set_recovery_reboot completed successfully\n");
	return (0);
#else
	argc;		/*lint !e522 */
	argv;		/*lint !e522 */
	return (no_fault_injection());
#endif
}

//
// CCR reboot function
//
int
#if defined(_FAULT_INJECTION)
ccr_node_reboot(int, char **)
#else
ccr_node_reboot(int, char **argp)
#endif
{
	os::prom_printf("*** CCR Reboot\n");

#if defined(_FAULT_INJECTION)
	FaultFunctions::wait_for_arg_t  wait_arg;

	wait_arg.op = FaultFunctions::WAIT_FOR_DEAD;
	wait_arg.nodeid = 0;
	FaultFunctions::reboot(0, &wait_arg, (unsigned)sizeof (wait_arg));

	os::usecsleep(10 * 1000000);

	ASSERT(0);
	return (1);
#else
	argp = NULL;
	return (no_fault_injection());
#endif
}

//
// ccr_set_override
//
int
ccr_set_override(int argc, char **argv)
{
	os::printf("In ccr_set_override\n");
	if (argc != 2) {
		os::printf("FAIL: Usage is ccr_set_override tablename\n");
		return (1);
	}

#if	defined(_KERNEL)
	argc;		/*lint !e522 */
	argv;		/*lint !e522 */
	os::printf("FAIL: Must be run in user mode\n");
	return (1);
#else
	char	gennum[] = "ccr_gennum\t-2\n";
	char	*filename, *filename2;

	filename = new char[20+strlen(argv[1])];
	filename2 = new char[25+strlen(argv[1])];
	sprintf(filename, "/etc/cluster/ccr/global/%s", argv[1]);
	sprintf(filename2, "/etc/cluster/ccr/global/%s.over", argv[1]);

	FILE	*fptr = fopen(filename, "r");
	FILE	*fptr2 = fopen(filename2, "w");
	if (!fptr) {
		os::printf("FAIL: Could not open %s\n", filename);
		return (1);
	}
	if (!fptr2) {
		os::printf("FAIL: Could not open %s\n", filename2);
		return (1);
	}

	char	line[MAXLINE];
	fgets(line, MAXLINE, fptr);
	fputs(gennum, fptr2);

	while (fgets(line, MAXLINE, fptr)) {
		fputs(line, fptr2);
	}
	os::file_rename(filename2, filename);
	fclose(fptr);
	fclose(fptr2);

	//
	// Regenerate the checksum for the ccr table
	//
	int	rslt = 0;
	char	cmd[256];

	sprintf(cmd, "/usr/cluster/lib/sc/ccradm -F -i %s -o", filename);
	os::printf("Command is %s\n", cmd);
	rslt += system(cmd);
	os::printf("Result is %d\n", rslt);

	if (!rslt)  {
		os::printf("PASS: Set override bit to %s\n", filename);
	} else {
		os::printf("FAIL: Couldn't set override bit to %s\n", filename);
	}
	return (rslt);

#endif
} /*lint !e550 */

//
// ccr_transaction_error
// usage:	ccr_transaction_error fnum node type
//	fnum	12 for CCR_STARTUP_1
//			13 for CCR_STARTUP_2
//			14 for CCR_PREPARE_UPDATE_COPIES
//			15 for CCR_COMMIT_UPDATE_COPIES
//			16 for CCR_COMMIT_TRANSACTION_1
//			17 for CCR_COMMIT_TRANSACTION_2
//	node	1 for DS dies
//			2 for TM dies
//			3 for Client node dies
//	type	1 for reboot
//			2 for system error
//
int
ccr_transaction_error(int argc, char **argv)
{
	os::printf("In ccr_transaction_error\n");
#if	defined(_FAULT_INJECTION)
	if (argc != 4) {
		os::printf("FAIL: Usage is ccr_transaction_error fnum node "
		    "type\n");
		return (1);
	}
	int		rslt = 0;
	int		node = os::atoi(argv[2]);
	int		type = os::atoi(argv[3]);
	fault_ccr::ccr_arg_t		farg;

	int num = os::atoi(argv[1]);
	int fnum = get_fault_num(num);
	if (fnum == 0) {
		return (1);
	} else if ((num < 12) || (num > 17)) {
		os::printf("FAIL: Fault number out of range\n");
		return (1);
	}

	if (type == 1) {
		os::printf("Setting action to reboot\n");
		farg.op = fault_ccr::REBOOT_NODE;
		farg.nodenum = 0;
	} else {
		os::printf("Setting action for system error\n");
		farg.op = fault_ccr::SET_SYSTEM_ERROR;
		farg.nodenum = (unsigned int)node;
	}

	if (node == 1) {
		os::printf("Triggering this node\n");
		NodeTriggers::add((unsigned int)fnum, &farg,
		    (unsigned)sizeof (farg), (int)TRIGGER_THIS_NODE);
	} else {
		os::printf("Triggering all nodes\n");
		NodeTriggers::add((unsigned int)fnum, &farg,
		    (unsigned)sizeof (farg), TRIGGER_ALL_NODES);
	}

	os::printf("PASS: ccr_transaction_error set trigger\n");
	Environment	e;
	rslt = ccr_simple_addelement("recovery_test", e);

	return (rslt);
#else
	argc;		/*lint !e522 */
	argv;		/*lint !e522 */
	return (no_fault_injection());
#endif	// _FAULT_INJECTION
}

int
ccr_transaction(int, char **)
{
	os::printf("PASS: ccr_transaction\n");
	Environment e;
	int rslt = ccr_simple_addelement("recovery_test", e);
	if (rslt == 1) {
		if (ccr::system_error::_exnarrow(e.release_exception())
		    != NULL) {
			os::printf("PASS: Caused and survived system error\n");
			rslt = 0;
		} else {
			os::printf("FAIL: Unexpected exception\n");
		}
	}

	return (rslt);
}

//
// CCR Add-Basic Test Case
//
int
ccr_add_basic(int arglen, char **argp)
{
	os::prom_printf("*** CCR Adds Case Basic Functionality\n");

#if defined(_FAULT_INJECTION)
	Environment			e;
	int				rslt;

	*argp[0] = '0';
	*(argp[0]+1) = '\0';
	rslt = ccr_addelement(
		NULL,
		NULL,
		argp,
		arglen,
		e);
	if (e.exception() || rslt) {
		ccr_cleanup(argp, arglen);
	}
	if (e.exception()) {
		e.exception()->print_exception("ccr_add_basic");
		return (1);
	}

	return (rslt);

#else
	arglen = 0;
	argp = NULL;
	return (no_fault_injection());
#endif

}

//
// HA-CCR Add-A Test Case
//
int
haccr_add_a(int arglen, char **argp)
{
	os::prom_printf("*** HA-CCR Adds Case A\n");

#if defined(_FAULT_INJECTION)
	Environment			e;
	int				rslt;

	*argp[0] = '4';
	*(argp[0]+1) = '\0';
	rslt = ccr_addelement(
		fault_ccr_hatm_a_on,
		fault_ccr_hatm_a_off,
		argp,
		arglen,
		e);
	if (e.exception() || rslt) {
		ccr_cleanup(argp, arglen);
	}
	if (e.exception()) {
		e.exception()->print_exception("haccr_add_a");
		return (1);
	}

	return (rslt);

#else
	arglen = 0;
	argp = NULL;
	return (no_fault_injection());
#endif

}

//
// HA-CCR Add-A Test Case
//
int
haccr_add_b(int arglen, char **argp)
{
	os::prom_printf("*** HA-CCR Adds Case B\n");

#if defined(_FAULT_INJECTION)
	Environment			e;
	int				rslt;

	*argp[0] = '4';
	*(argp[0]+1) = '\0';
	rslt = ccr_addelement(
		fault_ccr_hatm_b_on,
		fault_ccr_hatm_b_off,
		argp,
		arglen,
		e);
	if (e.exception() || rslt) {
		ccr_cleanup(argp, arglen);
	}
	if (e.exception()) {
		e.exception()->print_exception("haccr_add_b");
		return (1);
	}

	return (rslt);

#else
	arglen = 0;
	argp = NULL;
	return (no_fault_injection());
#endif

}

//
// HA-CCR Create-A Test Case
//
int
haccr_create_a(int arglen, char **argp)
{
	os::prom_printf("*** HA-CCR CreateTables Case A\n");

#if defined(_FAULT_INJECTION)
	Environment			e;
	int				rslt;

	*argp[0] = '2';
	*(argp[0]+1) = '\0';
	rslt = ccr_addelement(
		fault_ccr_hatm_a_on,
		fault_ccr_hatm_a_off,
		argp,
		arglen,
		e);
	if (e.exception() || rslt) {
		ccr_cleanup(argp, arglen);
	}
	if (e.exception()) {
		e.exception()->print_exception("haccr_create_a");
		return (1);
	}

	return (rslt);

#else
	arglen = 0;
	argp = NULL;
	return (no_fault_injection());
#endif

}

//
// HA-CCR Create-B Test Case
//
int
haccr_create_b(int arglen, char **argp)
{
	os::prom_printf("*** HA-CCR CreateTables Case B\n");

#if defined(_FAULT_INJECTION)
	Environment			e;
	int				rslt;

	*argp[0] = '2';
	*(argp[0]+1) = '\0';
	rslt = ccr_addelement(
		fault_ccr_hatm_b_on,
		fault_ccr_hatm_b_off,
		argp,
		arglen,
		e);
	if (e.exception() || rslt) {
		ccr_cleanup(argp, arglen);
	}
	if (e.exception()) {
		e.exception()->print_exception("haccr_create_b");
		return (1);
	}

	return (rslt);

#else
	arglen = 0;
	argp = NULL;
	return (no_fault_injection());
#endif

}

//
// HA-CCR Query-A Test Case
//
int
haccr_query_a(int arglen, char **argp)
{
	os::prom_printf("*** HA-CCR Queries Case A\n");

#if defined(_FAULT_INJECTION)
	Environment			e;
	int				rslt;

	*argp[0] = '6';
	*(argp[0]+1) = '\0';
	rslt = ccr_addelement(
		fault_ccr_hatm_a_on,
		fault_ccr_hatm_a_off,
		argp,
		arglen,
		e);
	if (e.exception() || rslt) {
		ccr_cleanup(argp, arglen);
	}
	if (e.exception()) {
		e.exception()->print_exception("haccr_query_a");
		return (1);
	}

	return (rslt);

#else
	arglen = 0;
	argp = NULL;
	return (no_fault_injection());
#endif

}

//
// HA-CCR Query-B Test Case
//
int
haccr_query_b(int arglen, char **argp)
{
	os::prom_printf("*** HA-CCR Queries Case B\n");

#if defined(_FAULT_INJECTION)
	Environment			e;
	int				rslt;

	*argp[0] = '6';
	*(argp[0]+1) = '\0';
	rslt = ccr_addelement(
		fault_ccr_hatm_b_on,
		fault_ccr_hatm_b_off,
		argp,
		arglen,
		e);
	if (e.exception() || rslt) {
		ccr_cleanup(argp, arglen);
	}
	if (e.exception()) {
		e.exception()->print_exception("haccr_query_b");
		return (1);
	}

	return (rslt);

#else
	arglen = 0;
	argp = NULL;
	return (no_fault_injection());
#endif

}

//
// HA-CCR Remove-A Test Case
//
int
haccr_remove_a(int arglen, char **argp)
{
	os::prom_printf("*** HA-CCR Removes Case A\n");

#if defined(_FAULT_INJECTION)
	Environment			e;
	int				rslt;

	*argp[0] = '2';
	*(argp[0]+1) = '\0';
	rslt = ccr_removeelement(
		fault_ccr_hatm_a_on,
		fault_ccr_hatm_a_off,
		argp,
		arglen,
		e);
	if (e.exception() || rslt) {
		ccr_cleanup(argp, arglen);
	}
	if (e.exception()) {
		e.exception()->print_exception("haccr_remove_a");
		return (1);
	}

	return (rslt);

#else
	arglen = 0;
	argp = NULL;
	return (no_fault_injection());
#endif

}

//
// HA-CCR Remove-B Test Case
//
int
haccr_remove_b(int arglen, char **argp)
{
	os::prom_printf("*** HA-CCR Removes Case B\n");

#if defined(_FAULT_INJECTION)
	Environment			e;
	int				rslt;

	*argp[0] = '2';
	*(argp[0]+1) = '\0';
	rslt = ccr_removeelement(
		fault_ccr_hatm_b_on,
		fault_ccr_hatm_b_off,
		argp,
		arglen,
		e);
	if (e.exception() || rslt) {
		ccr_cleanup(argp, arglen);
	}
	if (e.exception()) {
		e.exception()->print_exception("haccr_remove_b");
		return (1);
	}

	return (rslt);

#else
	arglen = 0;
	argp = NULL;
	return (no_fault_injection());
#endif

}

//
// HA-CCR RemoveAll-A Test Case
//
int
haccr_removeall_a(int arglen, char **argp)
{
	os::prom_printf("*** HA-CCR RemoveAll Case A\n");

#if defined(_FAULT_INJECTION)
	Environment			e;
	int				rslt;

	*argp[0] = '4';
	*(argp[0]+1) = '\0';
	rslt = ccr_removeelement(
		fault_ccr_hatm_a_on,
		fault_ccr_hatm_a_off,
		argp,
		arglen,
		e);
	if (e.exception() || rslt) {
		ccr_cleanup(argp, arglen);
	}
	if (e.exception()) {
		e.exception()->print_exception("haccr_removeall_a");
		return (1);
	}

	return (rslt);

#else
	arglen = 0;
	argp = NULL;
	return (no_fault_injection());
#endif

}

//
// HA-CCR RemoveAll-B Test Case
//
int
haccr_removeall_b(int arglen, char **argp)
{
	os::prom_printf("*** HA-CCR RemoveAll Case B\n");

#if defined(_FAULT_INJECTION)
	Environment			e;
	int				rslt;

	*argp[0] = '4';
	*(argp[0]+1) = '\0';
	rslt = ccr_removeelement(
		fault_ccr_hatm_b_on,
		fault_ccr_hatm_b_off,
		argp,
		arglen,
		e);
	if (e.exception() || rslt) {
		ccr_cleanup(argp, arglen);
	}
	if (e.exception()) {
		e.exception()->print_exception("haccr_removeall_b");
		return (1);
	}

	return (rslt);

#else
	arglen = 0;
	argp = NULL;
	return (no_fault_injection());
#endif

}

//
// CCR Update-Basic Test Case
//
int
ccr_update_basic(int arglen, char **argp)
{
	os::prom_printf("*** CCR Updates Case BasicFunctionality\n");

#if defined(_FAULT_INJECTION)
	Environment			e;
	int				rslt;

	rslt = ccr_updateelement(
		NULL,
		NULL,
		argp,
		arglen,
		e);
	if (e.exception() || rslt) {
		ccr_cleanup(argp, arglen);
	}
	if (e.exception()) {
		e.exception()->print_exception("ccr_update_basic");
		return (1);
	}

	return (rslt);

#else
	arglen = 0;
	argp = NULL;
	return (no_fault_injection());
#endif

}

//
// CCR UpdateElement with FI-update_method
//
int
ccr_update_updmeth(int arglen, char **argp)
{
	os::prom_printf("*** CCR Updates Case CommitUpdateFI\n");

#if defined(_FAULT_INJECTION)
	Environment			e;
	int				rslt;

	rslt = ccr_updateelement(
		fault_ccr_update_1_d_on,
		fault_ccr_update_1_d_off,
		argp,
		arglen,
		e);
	if (e.exception() || rslt) {
		ccr_cleanup(argp, arglen);
	}
	if (e.exception()) {
		e.exception()->print_exception("ccr_update_updmeth");
		return (1);
	}

	return (rslt);

#else
	arglen = 0;
	argp = NULL;
	return (no_fault_injection());
#endif

}

//
// CCR Directory with FI-update_method
//
int
ccr_directory_updmeth(int arglen, char **argp)
{
	os::prom_printf("*** CCR Directory Case UpdateMethodFI\n");

#if defined(_FAULT_INJECTION)
	Environment			e;
	int				rslt;

	rslt = ccr_directory(
		fault_ccr_update_1_a_on,
		fault_ccr_update_1_a_off,
		argp,
		arglen,
		e);
	if (e.exception() || rslt) {
		ccr_cleanup(argp, arglen);
	}
	if (e.exception()) {
		e.exception()->print_exception("ccr_directory_updmeth");
		return (1);
	}

	return (rslt);

#else
	arglen = 0;
	argp = NULL;
	return (no_fault_injection());
#endif

}

//
// CCR Next-A Test Case
//
int
ccr_next_a(int arglen, char **argp)
{
	os::prom_printf("*** CCR NextElement Case A\n");

#if defined(_FAULT_INJECTION)
	Environment			e;
	int				rslt;

	rslt = ccr_nextelement(
		NULL,
		NULL,
		argp,
		arglen,
		e);
	if (e.exception() || rslt) {
		ccr_cleanup(argp, arglen);
	}
	if (e.exception()) {
		e.exception()->print_exception("ccr_next_a");
		return (1);
	}

	return (rslt);

#else
	arglen = 0;
	argp = NULL;
	return (no_fault_injection());
#endif

}

//
// CCR Remove-Basic Test Case
//
int
ccr_remove_basic(int arglen, char **argp)
{
	os::prom_printf("*** CCR Remove Case BasicFunctionality\n");

#if defined(_FAULT_INJECTION)
	Environment			e;
	int				rslt;

	*argp[0] = '0';
	*(argp[0]+1) = '\0';
	rslt = ccr_removeelement(
		NULL,
		NULL,
		argp,
		arglen,
		e);
	if (e.exception() || rslt) {
		ccr_cleanup(argp, arglen);
	}
	return (rslt);

#else
	arglen = 0;
	argp = NULL;
	return (no_fault_injection());
#endif

}

//
// CCR Remove with FI-update_method
//
int
ccr_remove_updmeth(int arglen, char **argp)
{
	os::prom_printf("*** CCR Remove Case UpdMethFI\n");

#if defined(_FAULT_INJECTION)
	Environment			e;
	int				rslt;

	*argp[0] = '1';
	*(argp[0]+1) = '\0';
	rslt = ccr_removeelement(
		fault_ccr_update_1_b_on,
		fault_ccr_update_1_b_off,
		argp,
		arglen,
		e);
	if (e.exception() || rslt) {
		ccr_cleanup(argp, arglen);
	}
	return (rslt);

#else
	arglen = 0;
	argp = NULL;
	return (no_fault_injection());
#endif

}

//
// CCR RemoveAll with FI-update_method
//
int
ccr_removeall_updmeth(int arglen, char **argp)
{
	os::prom_printf("*** CCR RemoveAll Case UpdateMethodFI\n");

#if defined(_FAULT_INJECTION)
	Environment			e;
	int				rslt;

	*argp[0] = '3';
	*(argp[0]+1) = '\0';
	rslt = ccr_removeelement(
		fault_ccr_update_1_c_on,
		fault_ccr_update_1_c_off,
		argp,
		arglen,
		e);
	if (e.exception() || rslt) {
		ccr_cleanup(argp, arglen);
	}
	return (rslt);

#else
	arglen = 0;
	argp = NULL;
	return (no_fault_injection());
#endif

}

//
// CCR Abort-Basic
//
int
ccr_abort_basic(int arglen, char **argp)
{
	os::prom_printf("*** CCR Abort Case BasicFunctionality \n");

#if defined(_FAULT_INJECTION)
	Environment			e;
	int				rslt;

	rslt = ccr_abort(
		NULL,
		NULL,
		argp,
		arglen,
		e);
	if (e.exception() || rslt) {
		ccr_cleanup(argp, arglen);
	}
	return (rslt);

#else
	arglen = 0;
	argp = NULL;
	return (no_fault_injection());
#endif

}

//
// CCR Callback-Basic
//
int
ccr_callback_basic(int arglen, char **argp)
{
	os::prom_printf("*** CCR Callback Case BasicFunctionality \n");

#if defined(_FAULT_INJECTION)
	Environment			e;
	int				rslt;

	rslt = ccr_callback(
		NULL,
		NULL,
		argp,
		arglen,
		e);
	if (e.exception() || rslt) {
		ccr_cleanup(argp, arglen);
	}
	return (rslt);

#else
	arglen = 0;
	argp = NULL;
	return (no_fault_injection());
#endif

}

//
// CCR LongValues
//
int
ccr_longvalues(int arglen, char **argp)
{
	os::prom_printf("*** CCR LongValues\n");

#if defined(_FAULT_INJECTION)
	Environment			e;
	int				rslt;

	rslt = ccr_longelement(
		NULL,
		NULL,
		argp,
		arglen,
		e);
	if (e.exception() || rslt) {
		ccr_cleanup(argp, arglen);
	}
	return (rslt);

#else
	arglen = 0;
	argp = NULL;
	return (no_fault_injection());
#endif

}

/*
 * This support function switch over ccr transcation manager primary
 * to the node where this support function is executed.
 *
 * For example:
 *	CLIENT          -n NODE_4 -m K switch_ccr_tm_primary
 *
 * In this case, ccr tm primary will be switched over to node 4
 */
int
switch_ccr_tm_primary(int arglen, char **argp)
{
	os::prom_printf("*** Switchover CCR TM primary\n");

	Environment	e;
	int		rslt;

	nodeid_t local_nodeid = orb_conf::local_nodeid();

	char	local_buf[10];
	os::sprintf(local_buf, "%d", local_nodeid);

	// Set up the switchover struct
	struct opt_t	_opt;

	_opt.new_state = replica::SC_SET_PRIMARY;
	_opt.prov_desc = local_buf;
	_opt.service_desc = "ccr_server";

	//
	// Do the switchover
	//
	rslt = trigger_switchover(&_opt, e);

	if (e.exception()) {
		e.exception()->print_exception("trigger_switchover");
	}

	return (rslt);
}
