/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ORBTEST_CCR_FI_FAULTS_ENTRY_COMMON_H
#define	_ORBTEST_CCR_FI_FAULTS_ENTRY_COMMON_H

#pragma ident	"@(#)entry_common.h	1.12	08/05/20 SMI"

#include <orb/invo/common.h>
#include <orb/fault/fault_functions.h>

#if defined(_FAULT_INJECTION) && defined(_KERNEL_ORB)
//
// Find the nth non-root node
//
nodeid_t non_root_node(int n);
#endif // _KERNEL_ORB

//
// get_primary
//
// Get the primary nodeid for the specified service
// arguments
//	service name
// returns
//	nodeid (or -1 if error)
// side effect
//	prints errors
nodeid_t get_primary(const char *svc_name);

//
// If fault injection is not turned on, print an error message and return 0.
//
#if !defined(_FAULT_INJECTION)
int	no_fault_injection();
#endif

#endif	/* _ORBTEST_CCR_FI_FAULTS_ENTRY_COMMON_H */
