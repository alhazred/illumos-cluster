/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ENTRY_CCR_CLNT_H
#define	_ENTRY_CCR_CLNT_H

#pragma ident	"@(#)entry_ccr_clnt.h	1.20	08/05/20 SMI"

#include <orbtest/ccr_fi/faults/entry_common.h>

int ccr_add_basic(int, char **);
int ccr_update_basic(int, char **);
int ccr_directory_updmeth(int, char **);
int ccr_next_a(int, char **);
int ccr_remove_basic(int, char **);
int ccr_abort_basic(int, char **);
int ccr_callback_basic(int, char **);
int ccr_longvalues(int, char **);

int ccr_add_updmeth(int, char **);
int ccr_add_commupd(int, char **);
int ccr_remove_commupd(int, char **);
int ccr_removeall_commupd(int, char **);
int ccr_remove_updmeth(int, char **);
int ccr_update_updmeth(int, char **);
int ccr_removeall_updmeth(int, char **);

int ccr_recovery(int, char **);
int ccr_recovery_cleanup(int, char **);
int ccr_setup_recovery_test(int, char **);
int ccr_set_recovery_reboot(int, char **);
int ccr_node_reboot(int, char **);
int ccr_check_and_fix_invalid(int, char **);
int ccr_set_override(int, char **);
int ccr_verify(int, char **);
int ccr_absentnode(int, char **);
int ccr_transaction_error(int, char **);
int ccr_transaction(int, char **);

int haccr_add_a(int, char **);
int haccr_add_b(int, char **);
int haccr_create_a(int, char **);
int haccr_create_b(int, char **);
int haccr_query_a(int, char **);
int haccr_query_b(int, char **);
int haccr_remove_a(int, char **);
int haccr_remove_b(int, char **);
int haccr_removeall_a(int, char **);
int haccr_removeall_b(int, char **);

//
// Support function
//
int switch_ccr_tm_primary(int, char **);

#endif	/* _ENTRY_CCR_CLNT_H */
