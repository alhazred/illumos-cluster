/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fault_ccr_update_1_a.cc	1.20	08/05/20 SMI"

//
// This file contains the on and off functions for the fault points for a
// specific group of fault scenarios.
//
// Refer to the README.faults file for a complete description of all
// scenarios
//

#include	<sys/os.h>
#include	<orb/fault/fault_injection.h>

int
fault_ccr_update_1_a_on(void *argp, int arglen)
{
	arglen = 0;

	FaultFunctions::wait_for_arg_t	wait_args;
	wait_args.op = FaultFunctions::WAIT_FOR_UNKNOWN;
	wait_args.nodeid = *(nodeid_t *)argp;
	os::printf("nodeid is %d\n", wait_args.nodeid);
	FaultFunctions::invo_trigger_add(
		FaultFunctions::REBOOT_SPECIFIED,
		FAULTNUM_CCR_ADDELEMENT_BEGIN_1,
		&wait_args, (uint32_t)sizeof (wait_args));

	os::warning("Armed fault: "
		"FAULTNUM_CCR_ADDELEMENT_BEGIN_1");

	return (0);
}

int
fault_ccr_update_1_a_off(void *, int)
{
	// Clear the invo triggers
	InvoTriggers::clear(FAULTNUM_CCR_ADDELEMENT_BEGIN_1);

	return (0);
}

int
fault_ccr_update_1_b_on(void *argp, int arglen)
{
	arglen = 0;

	FaultFunctions::wait_for_arg_t	wait_args;
	wait_args.op = FaultFunctions::WAIT_FOR_UNKNOWN;
	wait_args.nodeid = *(nodeid_t *)argp;

	FaultFunctions::invo_trigger_add(
		FaultFunctions::REBOOT_SPECIFIED,
		FAULTNUM_CCR_REMOVEELEMENT_BEGIN_1,
		&wait_args, (uint32_t)sizeof (wait_args));

	os::warning("Armed fault: "
		"FAULTNUM_CCR_REMOVEELEMENT_BEGIN_1");

	return (0);
}

int
fault_ccr_update_1_b_off(void *, int)
{
	// Clear the invo triggers
	InvoTriggers::clear(FAULTNUM_CCR_REMOVEELEMENT_BEGIN_1);

	return (0);
}

int
fault_ccr_update_1_c_on(void *argp, int arglen)
{
	arglen = 0;

	FaultFunctions::wait_for_arg_t	wait_args;
	wait_args.op = FaultFunctions::WAIT_FOR_UNKNOWN;
	wait_args.nodeid = *(nodeid_t *)argp;

	FaultFunctions::invo_trigger_add(
		FaultFunctions::REBOOT_SPECIFIED,
		FAULTNUM_CCR_REMOVEALLELEMENTS_BEGIN_1,
		&wait_args, (uint32_t)sizeof (wait_args));

	os::warning("Armed fault: "
		"FAULTNUM_CCR_REMOVEALLELEMENTS_BEGIN_1");

	return (0);
}

int
fault_ccr_update_1_c_off(void *, int)
{
	// Clear the invo triggers
	InvoTriggers::clear(FAULTNUM_CCR_REMOVEALLELEMENTS_BEGIN_1);

	return (0);
}

int
fault_ccr_update_1_d_on(void *argp, int arglen)
{
	arglen = 0;

	FaultFunctions::wait_for_arg_t	wait_args;
	wait_args.op = FaultFunctions::WAIT_FOR_UNKNOWN;
	wait_args.nodeid = *(nodeid_t *)argp;

	FaultFunctions::invo_trigger_add(
		FaultFunctions::REBOOT_SPECIFIED,
		FAULTNUM_CCR_UPDATEELEMENT_BEGIN_1,
		&wait_args, (uint32_t)sizeof (wait_args));

	os::warning("Armed fault: "
		"FAULTNUM_CCR_UPDATEELEMENT_BEGIN_1");

	return (0);
}

int
fault_ccr_update_1_d_off(void *, int)
{
	// Clear the invo triggers
	InvoTriggers::clear(FAULTNUM_CCR_UPDATEELEMENT_BEGIN_1);

	return (0);
}

int
fault_ccr_update_1_e_on(void *argp, int arglen)
{
	arglen = 0;

	FaultFunctions::wait_for_arg_t	wait_args;
	wait_args.op = FaultFunctions::WAIT_FOR_UNKNOWN;
	wait_args.nodeid = *(nodeid_t *)argp;

	FaultFunctions::invo_trigger_add(
		FaultFunctions::REBOOT_SPECIFIED,
		FAULTNUM_CCR_COMMITUPDATE_BEGIN_1,
		&wait_args, (uint32_t)sizeof (wait_args));

	os::warning("Armed fault: "
		"FAULTNUM_CCR_COMMITUPDATE_BEGIN_1");

	return (0);
}

int
fault_ccr_update_1_e_off(void *, int)
{
	// Clear the invo triggers
	InvoTriggers::clear(FAULTNUM_CCR_COMMITUPDATE_BEGIN_1);

	return (0);
}

int
fault_ccr_hatm_a_on(void *argp, int arglen)
{
	arglen = 0;

	FaultFunctions::wait_for_arg_t	wait_args;
	wait_args.op = FaultFunctions::WAIT_FOR_UNKNOWN;
	wait_args.nodeid = *(nodeid_t *)argp;

	// Add a node trigger
	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT,
		FAULTNUM_HA_FRMWK_CKPT_HANDLER_INVOKE_1,
		&wait_args, (uint32_t)sizeof (wait_args), wait_args.nodeid);

	os::warning("Armed fault: "
	    "FAULTNUM_HA_FRMWK_CKPT_HANDLER_INVOKE_1");

	return (0);
}

int
fault_ccr_hatm_a_off(void *, int)
{
	// Node Triggers are not cleared since node itself
	// down.
	// NodeTriggers::clear(FAULTNUM_HA_FRMWK_CKPT_HANDLER_INVOKE_1,
	//		    id);

	return (0);
}

int
fault_ccr_hatm_b_on(void *argp, int arglen)
{
	arglen = 0;

	FaultFunctions::wait_for_arg_t	wait_args;
	wait_args.op = FaultFunctions::WAIT_FOR_UNKNOWN;
	wait_args.nodeid = *(nodeid_t *)argp;

	// Add a node trigger
	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT,
		FAULTNUM_HA_FRMWK_CKPT_HANDLER_INVOKE_2,
		&wait_args, (uint32_t)sizeof (wait_args), wait_args.nodeid);

	os::warning("Armed fault: "
	    "FAULTNUM_HA_FRMWK_CKPT_HANDLER_INVOKE_2");

	return (0);
}

int
fault_ccr_hatm_b_off(void *, int)
{
	// Node Triggers are not cleared since
	// node itself went down.
	// NodeTriggers::clear(FAULTNUM_HA_FRMWK_CKPT_HANDLER_INVOKE_2,
	//	id);

	return (0);
}
