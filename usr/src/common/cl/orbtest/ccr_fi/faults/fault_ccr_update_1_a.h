/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FAULT_CCR_UPDATE_1_A_H
#define	_FAULT_CCR_UPDATE_1_A_H

#pragma ident	"@(#)fault_ccr_update_1_a.h	1.15	08/05/20 SMI"

//
// This file contains the on and off functions for the fault points for a
// specifc failt scenario.
//
// Refer to the README.faults file for a comeplete description of all
// scenarios
//

int fault_ccr_update_1_a_on(void *, int);
int fault_ccr_update_1_a_off(void *, int);
int fault_ccr_update_1_b_on(void *, int);
int fault_ccr_update_1_b_off(void *, int);
int fault_ccr_update_1_c_on(void *, int);
int fault_ccr_update_1_c_off(void *, int);
int fault_ccr_update_1_d_on(void *, int);
int fault_ccr_update_1_d_off(void *, int);
int fault_ccr_update_1_e_on(void *, int);
int fault_ccr_update_1_e_off(void *, int);

int fault_ccr_hatm_a_on(void *, int);
int fault_ccr_hatm_a_off(void *, int);
int fault_ccr_hatm_b_on(void *, int);
int fault_ccr_hatm_b_off(void *, int);

#endif	/* _FAULT_CCR_UPDATE_1_A_H */
