/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rmm_test_support.cc	1.11	08/05/20 SMI"

#include <sys/os.h>
#include <nslib/ns.h>
#include <orbtest/repl_sample/repl_sample_impl.h>

extern "C" {
repl_sample_prov *
new_repl_sample_prov(const char *svc_desc, const char *prov_desc)
{
	return (new repl_sample_prov(svc_desc, prov_desc));
}

void *
rmm_run_test(void *)
{
	Environment e;
	int32_t i, retval;
	int16_t nodenum;

	CORBA::Object_ptr test_obj = ns::wait_resolve_local("rmm_test", e);
	ASSERT(!e.exception());
	repl_sample::factory_ptr fp = repl_sample::factory::_narrow(test_obj);
	ASSERT(!CORBA::is_nil(fp));
	repl_sample::value_obj_ptr vop = fp->create_value_obj(e);
	ASSERT(!CORBA::is_nil(fp) && !e.exception());
	CORBA::release(fp);
	CORBA::release(test_obj);

	i = 0;
	vop->set_value(0, nodenum, e);
	ASSERT(!e.exception());

	//
	// Workaround for no return statement in loop below
	//
	if (0)
		return (0);

	for (;;) {
		i++;
		(void) vop->inc_value(nodenum, e);
		retval = vop->get_value(nodenum, e);
		ASSERT(!e.exception());
		ASSERT(i == retval);
		if (!(i % 100))
			os::printf("Completed %d iterations, provider on %d\n",
			    i, nodenum);
		os::usecsleep(100000);
	}
}

void
rmm_start_test(void)
{
#ifdef	_KERNEL
	if (thread_create(NULL, NULL, (void(*)())rmm_run_test, NULL, 0, &p0,
	    TS_RUN, 60) == NULL) {
		os::panic("thread_create failed\n");
	}
#else
	if (os::thread::create(NULL, (size_t)0, rmm_run_test, NULL, THR_BOUND,
	    NULL)) {
		os::panic("thr_create failed\n");
	}
#endif
}

}
