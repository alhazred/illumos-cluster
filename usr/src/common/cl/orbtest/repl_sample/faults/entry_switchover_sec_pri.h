/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ENTRY_SWITCHOVER_SEC_PRI_H
#define	_ENTRY_SWITCHOVER_SEC_PRI_H

#pragma ident	"@(#)entry_switchover_sec_pri.h	1.12	08/05/20 SMI"

#include <orbtest/repl_sample/faults/entry_common.h>

//
// Scenario 1
//
int switchover_sec_pri_1_a(int, char *[]);
int switchover_sec_pri_1_b(int, char *[]);
int switchover_sec_pri_1_c(int, char *[]);
int switchover_sec_pri_1_d(int, char *[]);
int switchover_sec_pri_1_e(int, char *[]);
int switchover_sec_pri_1_f(int, char *[]);
int switchover_sec_pri_1_g(int, char *[]);
int switchover_sec_pri_1_h(int, char *[]);
int switchover_sec_pri_1_i(int, char *[]);
int switchover_sec_pri_1_j(int, char *[]);
int switchover_sec_pri_1_k(int, char *[]);
int switchover_sec_pri_1_l(int, char *[]);
int switchover_sec_pri_1_m(int, char *[]);
int switchover_sec_pri_1_n(int, char *[]);
int switchover_sec_pri_1_o(int, char *[]);
int switchover_sec_pri_1_p(int, char *[]);
int switchover_sec_pri_1_q(int, char *[]);
int switchover_sec_pri_1_r(int, char *[]);
int switchover_sec_pri_1_s(int, char *[]);
int switchover_sec_pri_1_t(int, char *[]);

//
// Scenario 2
//
int switchover_sec_pri_2_a(int, char *[]);
int switchover_sec_pri_2_b(int, char *[]);
int switchover_sec_pri_2_c(int, char *[]);
int switchover_sec_pri_2_d(int, char *[]);
int switchover_sec_pri_2_e(int, char *[]);
int switchover_sec_pri_2_f(int, char *[]);
int switchover_sec_pri_2_g(int, char *[]);
int switchover_sec_pri_2_h(int, char *[]);
int switchover_sec_pri_2_i(int, char *[]);
int switchover_sec_pri_2_j(int, char *[]);
int switchover_sec_pri_2_k(int, char *[]);
int switchover_sec_pri_2_l(int, char *[]);
int switchover_sec_pri_2_m(int, char *[]);
int switchover_sec_pri_2_n(int, char *[]);
int switchover_sec_pri_2_o(int, char *[]);
int switchover_sec_pri_2_p(int, char *[]);
int switchover_sec_pri_2_q(int, char *[]);
int switchover_sec_pri_2_r(int, char *[]);
int switchover_sec_pri_2_s(int, char *[]);
int switchover_sec_pri_2_t(int, char *[]);

#endif	/* _ENTRY_SWITCHOVER_SEC_PRI_H */
