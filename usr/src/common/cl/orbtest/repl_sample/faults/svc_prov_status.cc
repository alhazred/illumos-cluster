/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)svc_prov_status.cc	1.10	08/05/20 SMI"

#include	<sys/os.h>
#include	<h/replica.h>
#include	<h/mc_sema.h>
#include	<orb/invo/common.h>
#include	<orb/infrastructure/orb_conf.h>
#include	<orbtest/repl_sample/repl_sample_clnt.h>
#include	<sys/mc_sema_impl.h>

#include	<orbtest/rm_probe/rm_snap.h>
#include	<orbtest/repl_sample/faults/entry_common.h>
#include	<orbtest/repl_sample/faults/svc_prov_status.h>

#include 	<orbtest/repl_tests/repl_test_switch.h>


int
verify_svc_up(int argc, char **argv)
{
#ifdef _FAULT_INJECTION

	int	i;
	char	**svc_name = new char *[10];

	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc < 2) {
		os::printf("Usage:\t%s <svc_name> [[svc_name] ...]\n",
			argv[0]);
		delete [] svc_name;
		return (1);
	}

	for (i = 0; i < argc-1; i++) {
		*(svc_name+i) = new char[os::strlen(argv[i+1])+1];
		os::sprintf(*(svc_name+i), "%s", argv[i+1]);
	}

	for (i = 0; i < argc-1; i++) {
		// Verify that service is up
		if (get_state(*(svc_name+i)) != replica::S_UP) {
			os::printf("+++ FAIL: service %s is not "
				"in S_UP state\n", *(svc_name+i));
			for (i = 0; i < argc-1; i++) {
				delete [] svc_name[i];
			}
			delete [] svc_name;
			return (1);
		} else {
			os::printf("PASS: service %s is in S_UP "
				"state as expected\n", *(svc_name+i));
		}
	}

	for (i = 0; i < argc-1; i++) {
		delete [] svc_name[i];
	}
	delete [] svc_name;

	return (0);

#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

int
verify_svc_down(int argc, char **argv)
{
#ifdef _FAULT_INJECTION

	int 	i;
	char	**svc_name = new char *[10];

	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc < 2) {
		os::printf("Usage:\t%s <svc_name> [[svc_name] ...]\n",
			argv[0]);
		delete [] svc_name;
		return (1);
	}

	for (i = 0; i < argc-1; i++) {
		*(svc_name+i) = new char[os::strlen(argv[i+1])+1];
		os::sprintf(*(svc_name+i), "%s", argv[i+1]);
	}

	for (i = 0; i < argc-1; i++) {
		// Verify that service is down
		if (get_state(*(svc_name+i)) != replica::S_DOWN) {
			os::printf("+++ FAIL: service %s is not "
				"in S_DOWN state\n", *(svc_name+i));
			for (i = 0; i < argc-1; i++) {
				delete [] svc_name[i];
			}
			delete [] svc_name;
			return (1);
		} else {
			os::printf("PASS: service %s is in S_DOWN "
				"state as expected\n", *(svc_name+i));
		}
	}

	for (i = 0; i < argc-1; i++) {
		delete [] svc_name[i];
	}
	delete [] svc_name;

	return (0);

#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

int
verify_prov_status(int argc, char **argv)
{
#ifdef _FAULT_INJECTION

	int retval = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}


	if (argc != 4) {
		os::printf("Usage:\t%s <svc_name> <prov_desc> "
			"<prov_state>\n", argv[0]);
		return (retval);
	}

	char *svc_name = new char[os::strlen(argv[1])+1];
	ASSERT(svc_name != NULL);
	os::sprintf(svc_name, "%s", argv[1]);

	if (get_state(svc_name) != replica::S_UP) {
		os::printf("+++ FAIL: service %s is not "
			"in S_UP state\n", svc_name);
		delete [] svc_name;
		return (retval);
	}

	char *prov_desc = new char[os::strlen(argv[2])+1];
	ASSERT(prov_desc != NULL);
	os::sprintf(prov_desc, "%s", argv[2]);

	// Create a snapshot object
	rm_snap				cur_snap;
	replica::repl_prov_state	prov_state;

	if (!cur_snap.refresh(svc_name))
		return (retval);

	if (os::strcmp(argv[3], "primary") == 0)
		prov_state = replica::AS_PRIMARY;
	else if (os::strcmp(argv[3], "secondary") == 0)
		prov_state = replica::AS_SECONDARY;
	else if (os::strcmp(argv[3], "spare") == 0)
		prov_state = replica::AS_SPARE;
	else {
		os::printf("ERROR: <prov_state> should be "
			"primary, secondary or spare only\n");
		delete [] svc_name;
		delete [] prov_desc;
		return (retval);
	}

	// Verify whether the provider is in expected state
	retval = cur_snap.verify_prov_state(prov_desc, prov_state);

	delete [] svc_name;
	delete [] prov_desc;

	return (retval);

#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}
