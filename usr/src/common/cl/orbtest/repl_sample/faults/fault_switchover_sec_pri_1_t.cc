/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fault_switchover_sec_pri_1_t.cc	1.19	08/05/20 SMI"

//
// This file contains the on and off functions for the fault points for a
// specific fault scenario.
//
// Refer to the README.faults file for a complete description of all
// scenarios
//

#include	<sys/os.h>
#include	<orb/infrastructure/orb_conf.h>
#include	<orb/fault/fault_injection.h>
#include	<orbtest/repl_sample/faults/entry_common.h>

int
fault_switchover_sec_pri_1_t_on(void *argp, int arglen)
{
	ASSERT(argp != NULL);
	ASSERT(arglen == sizeof (FaultFunctions::ha_dep_reboot_arg_t));

	FaultFunctions::node_trigger_add(FaultFunctions::HA_DEP_REBOOT,
		FAULTNUM_HA_FRMWK_RM_ROOT_OBJ_BCAST,
		argp, arglen,
		TRIGGER_ALL_NODES);

	os::warning("Armed fault: "
		"FAULTNUM_HA_FRMWK_RM_ROOT_OBJ_BCAST");
	return (0);
}

int
fault_switchover_sec_pri_1_t_off(void *, int)
{
	return (0);
}
