/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)entry_ru.cc	1.8	08/05/20 SMI"

#include	<orbtest/repl_sample/repl_sample_clnt.h>
#include	<sys/os.h>
#include	<orb/invo/common.h>
#include	<orbtest/rm_probe/rm_snap.h>
#include	<sys/rm_util.h>
#include	<orb/infrastructure/orb_conf.h>
#include 	<orb/fault/fault_injection.h>
#include	<orb/fault/fault_functions.h>
#include	<orbtest/repl_sample/faults/fault_ru.h>
#include	<orbtest/repl_sample/faults/entry_common.h>
#include	<orbtest/repl_tests/repl_test_switch.h>

//
// kill_svc_rm_primary
//
// Freeze service for upgrade
// Kill RM and service primary (they are co-located)
// After the RM recovers, make sure that the service is still frozen.
// Unfreeze the service.
//
static bool
service_state(char *svc, replica::service_state state)
{
	replica::service_state svc_state =
		    (replica::service_state)get_state(svc);
	return (svc_state == state);
}

static int
freeze_unfreeze(bool freeze, int argc, char **argv)
{
	int	i;

	if (argc < 2) {
		os::printf("Usage:\t%s <svc_name> [[svc_name] ...]\n",
			argv[0]);
		return (1);
	}

	// Freeze service
	replica::rm_admin_var rm_ref = rm_util::get_rm();

	// Get the reference to the replica framework
	rm_ref = rm_util::get_rm();

	if (CORBA::is_nil(rm_ref)) {
		os::printf("Could not get rm.\n");
		return (-1);
	}
	Environment e;
	if (freeze) {
		rm_ref->freeze_for_upgrade(argv[1], e);
		if (e.exception()) {
			os::printf("Freeze for upgrade %s returned "
			    "exception\n", argv[1]);
			return (1);
		} else {
			os::printf("%s frozen for upgrade\n", argv[1]);
		}
	} else {
		rm_ref->unfreeze_after_upgrade(argv[1], e);
		if (e.exception()) {
			os::printf("Unfreeze_after_upgrade %s returned "
			    "exception\n", argv[1]);
			return (1);
		} else {
			os::printf("%s Unfrozen\n", argv[1]);
		}
	}
	if (e.exception()) {
		e.exception()->print_exception("+++ Exception:");
		return (1);
	}
	return (0);
}

int
freeze_service(int argc, char **argv)
{
	return (freeze_unfreeze(true, argc, argv));
}

int
unfreeze_service(int argc, char **argv)
{
	return (freeze_unfreeze(false, argc, argv));
}


int
verify_svc_frozen_for_upgrade(int argc, char **argv)
{
#ifdef _FAULT_INJECTION
	int i;
	int rslt = 0;

	if (argc < 2) {
		os::printf("Usage:\t%s <svc_name> [[svc_name] ...]\n",
			argv[0]);
		return (1);
	}
	// Verify that service is frozen_for_upgrade
	for (i = 1; i < argc; i++) {
		replica::service_state svc_state =
			(replica::service_state)get_state(argv[i]);

		if ((svc_state != replica::S_FROZEN_FOR_UPGRADE_BY_REQ) &&
		    (svc_state != replica::S_FROZEN_FOR_UPGRADE)) {
			os::printf("+++ FAIL: service %s is not "
			    "in S_FROZEN_FOR_UPGRADE state\n", argv[i]);
			rslt = 1;
		}
	}
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

int
kill_svc_rm_primary(int argc, char **argv)
{
#ifdef _KERNEL_ORB
	os::printf("RU: Kill RM primary which is also the service primary\n");

#ifdef _FAULT_INJECTION

	int		rslt = 1;
	Environment	e;
	nodeid_t	rm_primary_nodeid, svc_primary_nodeid;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc != 2) {
		os::printf("Usage:\t%s <svc_name>\n", argv[0]);
		return (1);
	}

	// Set up the switchover struct
	struct opt_t	_opt;

	char	svc_name[50];
	os::sprintf(svc_name, "%s", argv[1]);
	_opt.service_desc = svc_name;

	if (get_state(_opt.service_desc) != replica::S_UP) {
		os::printf("+++ FAIL: service %s is not "
			"in S_UP state\n", _opt.service_desc);
		return (1);
	}

	rm_primary_nodeid = FaultFunctions::rm_primary_node();
	os::printf("Before: RM Primary is on %d\n", rm_primary_nodeid);

	char	prov[10];
	os::sprintf(prov, "%d", rm_primary_nodeid);
	_opt.prov_desc = prov;

	_opt.new_state = replica::SC_SET_PRIMARY;

	//
	// Do the switchover
	//
	rslt = trigger_switchover(&_opt, e);

	if (e.exception()) {
		e.exception()->print_exception("trigger_switchover");
	}

	if (rslt != 0) {
		return (rslt);
	}

	//
	// Make sure that ha_sample_server and RM primary are on same
	// node.
	//

	svc_primary_nodeid = get_primary(svc_name, true);
	rm_primary_nodeid = FaultFunctions::rm_primary_node();
	os::printf("After: RM Primary is on %d\n", rm_primary_nodeid);

	if (svc_primary_nodeid != rm_primary_nodeid) {
		os::printf("Service primary not same as RM primary. "
		    "Cannot run test.\n");
		return (1);
	}

	// Enable fault point
	fault_kill_svc_rm_primary_on((void *)&rm_primary_nodeid,
	    (uint32_t)sizeof (nodeid_t));

	return (0);
#else
	return (no_fault_injection());

#endif // _FAULT_INJECTION

#else
	os::printf("+++ FAIL: Test not supported in USER mode\n");
	return (1);
#endif // _KERNEL_ORB
}
