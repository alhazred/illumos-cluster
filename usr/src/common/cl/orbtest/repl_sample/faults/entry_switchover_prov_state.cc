/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)entry_switchover_prov_state.cc	1.11	08/05/20 SMI"

#include	<h/replica.h>
#include	<orb/fault/fault_injection.h>
#include	<orbtest/repl_sample/repl_sample_clnt.h>
#include	<sys/os.h>
#include	<orb/invo/common.h>
#include	<orbtest/rm_probe/rm_snap.h>

#include	<orbtest/repl_sample/faults/entry_common.h>
#include	<orbtest/repl_sample/faults/entry_switchover_prov_state.h>
#include	<orbtest/repl_sample/faults/faults_on_specific_service.h>

#include	<orbtest/repl_tests/repl_test_switch.h>

//
// Usage: functname <svc_name> <prov_desc> <new_state> [fault_node]
//	where <svc_name> = name of the service
//	<prov_desc> = nodeid of the provider
//	<new_state> = state after switchover
//	[fault_node] = node specified to be faulted (optional)
//

void
print_switchover_prov_state_usage(const char *functname)
{
	os::printf("Usage:\t%s <svc_name> <prov_desc> "
		"<new_state> [fault_node]\n", functname);
}

int
switchover_prov_state_no_fault(int argc, char **argv)
{
	os::printf("*** Switching over provider state without fault ***\n");

#ifdef _FAULT_INJECTION

	int 		rslt = 1;
	struct opt_t 	*p_opt;
	Environment 	e;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc != 4) {
		print_switchover_prov_state_usage(argv[0]);
		return (rslt);
	}

	// Allocate p_opt
	p_opt = new struct opt_t;

	p_opt->service_desc = new char[os::strlen(argv[1])+1];
	os::sprintf(p_opt->service_desc, "%s", argv[1]);

	if (get_state(p_opt->service_desc) != replica::S_UP) {
		os::printf("+++ FAIL: service %s is not "
			"in S_UP state\n", p_opt->service_desc);
		delete [] p_opt->service_desc;
		delete p_opt;
		return (rslt);
	}

	p_opt->prov_desc = new char[os::strlen(argv[2])+1];
	os::sprintf(p_opt->prov_desc, "%s", argv[2]);

	if (os::strcmp(argv[3], "primary") == 0)
		p_opt->new_state = replica::SC_SET_PRIMARY;
	else if (os::strcmp(argv[3], "secondary") == 0)
		p_opt->new_state = replica::SC_SET_SECONDARY;
	else if (os::strcmp(argv[3], "spare") == 0)
		p_opt->new_state = replica::SC_SET_SPARE;
	else {
		os::printf("ERROR: <prov_state> should be "
			"primary, secondary or spare only\n");
		delete [] p_opt->service_desc;
		delete [] p_opt->prov_desc;
		delete p_opt;
		return (rslt);
	}

	// No Fault
	rslt = switchover_prov_state(
		NULL, NULL, NULL, 0, p_opt, e);

	delete [] p_opt->service_desc;
	delete [] p_opt->prov_desc;
	delete p_opt;

	return (rslt);

#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
} //lint !e753

int
switchover_prov_state_fault_a(int argc, char **argv)
{
	os::printf("*** Switching over provider state with fault ***\n");

#ifdef _FAULT_INJECTION

	int 		rslt = 1;
	struct opt_t 	*p_opt;
	nodeid_t	fault_node;
	Environment 	e;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if ((argc < 4) || (argc > 5)) {
		print_switchover_prov_state_usage(argv[0]);
		return (rslt);
	}

	// Allocate p_opt
	p_opt = new struct opt_t;

	p_opt->service_desc = new char[os::strlen(argv[1])+1];
	os::sprintf(p_opt->service_desc, "%s", argv[1]);

	if (get_state(p_opt->service_desc) != replica::S_UP) {
		os::printf("+++ FAIL: service %s is not "
			"in S_UP state\n", p_opt->service_desc);
		delete [] p_opt->service_desc;
		delete p_opt;
		return (rslt);
	}

	p_opt->prov_desc = new char[os::strlen(argv[2])+1];
	os::sprintf(p_opt->prov_desc, "%s", argv[2]);

	if (os::strcmp(argv[3], "primary") == 0)
		p_opt->new_state = replica::SC_SET_PRIMARY;
	else if (os::strcmp(argv[3], "secondary") == 0)
		p_opt->new_state = replica::SC_SET_SECONDARY;
	else if (os::strcmp(argv[3], "spare") == 0)
		p_opt->new_state = replica::SC_SET_SPARE;
	else {
		os::printf("ERROR: <prov_state> should be "
			"primary, secondary or spare only\n");
		delete [] p_opt->service_desc;
		delete [] p_opt->prov_desc;
		delete p_opt;
		return (rslt);
	}

	if (argc == 5) {
		fault_node = (nodeid_t)os::atoi(argv[4]);
	} else {
		if ((fault_node = get_primary(p_opt->service_desc)) == 0)
			return (1);
	}

	os::printf(" ** Will fault node: %d\n", fault_node);

	// Populate fault arguments
	FaultFunctions::svc_reboot_arg_t	reboot_args;
	reboot_args.sid	 = get_sid(p_opt->service_desc);
	reboot_args.op	  = FaultFunctions::WAIT_FOR_UNKNOWN;
	reboot_args.nodeid	= fault_node;

	// Fault on provider to become secondary
	rslt = switchover_prov_state(
		fault_repl_prov_become_secondary_on,
		fault_repl_prov_become_secondary_off,
		&reboot_args, (int)sizeof (reboot_args),
		p_opt, e);

	delete [] p_opt->service_desc;
	delete [] p_opt->prov_desc;
	delete p_opt;

	return (rslt);

#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
} //lint !e753

int
switchover_prov_state_fault_b(int argc, char **argv)
{
	os::printf("*** Switching over provider state with fault ***\n");

#ifdef _FAULT_INJECTION

	int		rslt = 1;
	struct opt_t    *p_opt;
	nodeid_t	fault_node;
	Environment	e;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if ((argc < 4) || (argc > 5)) {
		print_switchover_prov_state_usage(argv[0]);
		return (rslt);
	}

	// Allocate p_opt
	p_opt = new struct opt_t;

	p_opt->service_desc = new char[os::strlen(argv[1])+1];
	os::sprintf(p_opt->service_desc, "%s", argv[1]);

	if (get_state(p_opt->service_desc) != replica::S_UP) {
		os::printf("+++ FAIL: service %s is not "
			"in S_UP state\n", p_opt->service_desc);
		delete [] p_opt->service_desc;
		delete p_opt;
		return (rslt);
	}

	p_opt->prov_desc = new char[os::strlen(argv[2])+1];
	os::sprintf(p_opt->prov_desc, "%s", argv[2]);

	if (os::strcmp(argv[3], "primary") == 0)
		p_opt->new_state = replica::SC_SET_PRIMARY;
	else if (os::strcmp(argv[3], "secondary") == 0)
		p_opt->new_state = replica::SC_SET_SECONDARY;
	else if (os::strcmp(argv[3], "spare") == 0)
		p_opt->new_state = replica::SC_SET_SPARE;
	else {
		os::printf("ERROR: <prov_state> should be "
			"primary, secondary or spare only\n");
		delete [] p_opt->service_desc;
		delete [] p_opt->prov_desc;
		delete p_opt;
		return (rslt);
	}

	if (argc == 5) {
		fault_node = (nodeid_t)os::atoi(argv[4]);
	} else {
		if ((fault_node = get_primary(p_opt->service_desc)) == 0)
			return (1);
	}

	os::printf(" ** Will fault node: %d\n", fault_node);

	// Populate fault arguments
	FaultFunctions::svc_reboot_arg_t	reboot_args;
	reboot_args.sid	 = get_sid(p_opt->service_desc);
	reboot_args.op	  = FaultFunctions::WAIT_FOR_UNKNOWN;
	reboot_args.nodeid	= fault_node;

	// Fault on provider to add secondary
	rslt = switchover_prov_state(
		fault_repl_prov_add_secondary_on,
		fault_repl_prov_add_secondary_off,
		&reboot_args, (int)sizeof (reboot_args),
		p_opt, e);

	delete [] p_opt->service_desc;
	delete [] p_opt->prov_desc;
	delete p_opt;

	return (rslt);

#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
} //lint !e753

int
switchover_prov_state_fault_c(int argc, char **argv)
{
	os::printf("*** Switching over provider state with fault ***\n");

#ifdef _FAULT_INJECTION

	int		rslt = 1;
	struct opt_t    *p_opt;
	nodeid_t	fault_node;
	Environment	e;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if ((argc < 4) || (argc > 5)) {
		print_switchover_prov_state_usage(argv[0]);
		return (rslt);
	}

	// Allocate p_opt
	p_opt = new struct opt_t;

	p_opt->service_desc = new char[os::strlen(argv[1])+1];
	os::sprintf(p_opt->service_desc, "%s", argv[1]);

	if (get_state(p_opt->service_desc) != replica::S_UP) {
		os::printf("+++ FAIL: service %s is not "
			"in S_UP state\n", p_opt->service_desc);
		delete [] p_opt->service_desc;
		delete p_opt;
		return (rslt);
	}

	p_opt->prov_desc = new char[os::strlen(argv[2])+1];
	os::sprintf(p_opt->prov_desc, "%s", argv[2]);

	if (os::strcmp(argv[3], "primary") == 0)
		p_opt->new_state = replica::SC_SET_PRIMARY;
	else if (os::strcmp(argv[3], "secondary") == 0)
		p_opt->new_state = replica::SC_SET_SECONDARY;
	else if (os::strcmp(argv[3], "spare") == 0)
		p_opt->new_state = replica::SC_SET_SPARE;
	else {
		os::printf("ERROR: <prov_state> should be "
			"primary, secondary or spare only\n");
		delete [] p_opt->service_desc;
		delete [] p_opt->prov_desc;
		delete p_opt;
		return (rslt);
	}

	if (argc == 5) {
		fault_node = (nodeid_t)os::atoi(argv[4]);
	} else {
		if ((fault_node = get_primary(p_opt->service_desc)) == 0)
			return (1);
	}

	os::printf(" ** Will fault node: %d\n", fault_node);

	// Populate fault arguments
	FaultFunctions::svc_reboot_arg_t	reboot_args;
	reboot_args.sid	 = get_sid(p_opt->service_desc);
	reboot_args.op	  = FaultFunctions::WAIT_FOR_UNKNOWN;
	reboot_args.nodeid	= fault_node;

	// Fault on provider to commit secondary
	rslt = switchover_prov_state(
		fault_repl_prov_commit_secondary_on,
		fault_repl_prov_commit_secondary_off,
		&reboot_args, (int)sizeof (reboot_args),
		p_opt, e);

	delete [] p_opt->service_desc;
	delete [] p_opt->prov_desc;
	delete p_opt;

	return (rslt);

#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
} //lint !e753

int
switchover_prov_state_fault_d(int argc, char **argv)
{
	os::printf("*** Switching over provider state with fault ***\n");

#ifdef _FAULT_INJECTION

	int		rslt = 1;
	struct opt_t    *p_opt;
	nodeid_t	fault_node;
	Environment	e;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if ((argc < 4) || (argc > 5)) {
		print_switchover_prov_state_usage(argv[0]);
		return (rslt);
	}

	// Allocate p_opt
	p_opt = new struct opt_t;

	p_opt->service_desc = new char[os::strlen(argv[1])+1];
	os::sprintf(p_opt->service_desc, "%s", argv[1]);

	if (get_state(p_opt->service_desc) != replica::S_UP) {
		os::printf("+++ FAIL: service %s is not "
			"in S_UP state\n", p_opt->service_desc);
		delete [] p_opt->service_desc;
		delete p_opt;
		return (rslt);
	}

	p_opt->prov_desc = new char[os::strlen(argv[2])+1];
	os::sprintf(p_opt->prov_desc, "%s", argv[2]);

	if (os::strcmp(argv[3], "primary") == 0)
		p_opt->new_state = replica::SC_SET_PRIMARY;
	else if (os::strcmp(argv[3], "secondary") == 0)
		p_opt->new_state = replica::SC_SET_SECONDARY;
	else if (os::strcmp(argv[3], "spare") == 0)
		p_opt->new_state = replica::SC_SET_SPARE;
	else {
		os::printf("ERROR: <prov_state> should be "
			"primary, secondary or spare only\n");
		delete [] p_opt->service_desc;
		delete [] p_opt->prov_desc;
		delete p_opt;
		return (rslt);
	}

	if (argc == 5) {
		fault_node = (nodeid_t)os::atoi(argv[4]);
	} else {
		if ((fault_node = get_primary(p_opt->service_desc)) == 0)
			return (1);
	}

	os::printf(" ** Will fault node: %d\n", fault_node);

	// Populate fault arguments
	FaultFunctions::svc_reboot_arg_t	reboot_args;
	reboot_args.sid	 = get_sid(p_opt->service_desc);
	reboot_args.op	  = FaultFunctions::WAIT_FOR_UNKNOWN;
	reboot_args.nodeid	= fault_node;

	// Fault on provider to remove secondary
	rslt = switchover_prov_state(
		fault_repl_prov_remove_secondary_on,
		fault_repl_prov_remove_secondary_off,
		&reboot_args, (int)sizeof (reboot_args),
		p_opt, e);

	delete [] p_opt->service_desc;
	delete [] p_opt->prov_desc;
	delete p_opt;

	return (rslt);

#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
} //lint !e753

int
switchover_prov_state_fault_e(int argc, char **argv)
{
	os::printf("*** Switching over provider state with fault ***\n");

#ifdef _FAULT_INJECTION

	int		rslt = 1;
	struct opt_t    *p_opt;
	nodeid_t	fault_node;
	Environment	e;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if ((argc < 4) || (argc > 5)) {
		print_switchover_prov_state_usage(argv[0]);
		return (rslt);
	}

	// Allocate p_opt
	p_opt = new struct opt_t;

	p_opt->service_desc = new char[os::strlen(argv[1])+1];
	os::sprintf(p_opt->service_desc, "%s", argv[1]);

	if (get_state(p_opt->service_desc) != replica::S_UP) {
		os::printf("+++ FAIL: service %s is not "
			"in S_UP state\n", p_opt->service_desc);
		delete [] p_opt->service_desc;
		delete p_opt;
		return (rslt);
	}

	p_opt->prov_desc = new char[os::strlen(argv[2])+1];
	os::sprintf(p_opt->prov_desc, "%s", argv[2]);

	if (os::strcmp(argv[3], "primary") == 0)
		p_opt->new_state = replica::SC_SET_PRIMARY;
	else if (os::strcmp(argv[3], "secondary") == 0)
		p_opt->new_state = replica::SC_SET_SECONDARY;
	else if (os::strcmp(argv[3], "spare") == 0)
		p_opt->new_state = replica::SC_SET_SPARE;
	else {
		os::printf("ERROR: <prov_state> should be "
			"primary, secondary or spare only\n");
		delete [] p_opt->service_desc;
		delete [] p_opt->prov_desc;
		delete p_opt;
		return (rslt);
	}

	if (argc == 5) {
		fault_node = (nodeid_t)os::atoi(argv[4]);
	} else {
		if ((fault_node = get_primary(p_opt->service_desc)) == 0)
			return (1);
	}

	os::printf(" ** Will fault node: %d\n", fault_node);

	// Populate fault arguments
	FaultFunctions::svc_reboot_arg_t	reboot_args;
	reboot_args.sid	 = get_sid(p_opt->service_desc);
	reboot_args.op	  = FaultFunctions::WAIT_FOR_UNKNOWN;
	reboot_args.nodeid	= fault_node;

	// Fault on provider to become primary
	rslt = switchover_prov_state(
		fault_repl_prov_become_primary_on,
		fault_repl_prov_become_primary_off,
		&reboot_args, (int)sizeof (reboot_args),
		p_opt, e);

	delete [] p_opt->service_desc;
	delete [] p_opt->prov_desc;
	delete p_opt;

	return (rslt);

#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
} //lint !e753

int
switchover_prov_state_fault_f(int argc, char **argv)
{
	os::printf("*** Switching over provider state with fault ***\n");

#ifdef _FAULT_INJECTION

	int		rslt = 1;
	struct opt_t    *p_opt;
	nodeid_t	fault_node;
	Environment	e;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if ((argc < 4) || (argc > 5)) {
		print_switchover_prov_state_usage(argv[0]);
		return (rslt);
	}

	// Allocate p_opt
	p_opt = new struct opt_t;

	p_opt->service_desc = new char[os::strlen(argv[1])+1];
	os::sprintf(p_opt->service_desc, "%s", argv[1]);

	if (get_state(p_opt->service_desc) != replica::S_UP) {
		os::printf("+++ FAIL: service %s is not "
			"in S_UP state\n", p_opt->service_desc);
		delete [] p_opt->service_desc;
		delete p_opt;
		return (rslt);
	}

	p_opt->prov_desc = new char[os::strlen(argv[2])+1];
	os::sprintf(p_opt->prov_desc, "%s", argv[2]);

	if (os::strcmp(argv[3], "primary") == 0)
		p_opt->new_state = replica::SC_SET_PRIMARY;
	else if (os::strcmp(argv[3], "secondary") == 0)
		p_opt->new_state = replica::SC_SET_SECONDARY;
	else if (os::strcmp(argv[3], "spare") == 0)
		p_opt->new_state = replica::SC_SET_SPARE;
	else {
		os::printf("ERROR: <prov_state> should be "
			"primary, secondary or spare only\n");
		delete [] p_opt->service_desc;
		delete [] p_opt->prov_desc;
		delete p_opt;
		return (rslt);
	}

	if (argc == 5) {
		fault_node = (nodeid_t)os::atoi(argv[4]);
	} else {
		if ((fault_node = get_primary(p_opt->service_desc)) == 0)
			return (1);
	}

	os::printf(" ** Will fault node: %d\n", fault_node);

	// Populate fault arguments
	FaultFunctions::svc_reboot_arg_t	reboot_args;
	reboot_args.sid	 = get_sid(p_opt->service_desc);
	reboot_args.op	  = FaultFunctions::WAIT_FOR_UNKNOWN;
	reboot_args.nodeid	= fault_node;

	// Fault on provider to become spare
	rslt = switchover_prov_state(
		fault_repl_prov_become_spare_on,
		fault_repl_prov_become_spare_off,
		&reboot_args, (int)sizeof (reboot_args),
		p_opt, e);

	delete [] p_opt->service_desc;
	delete [] p_opt->prov_desc;
	delete p_opt;

	return (rslt);

#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
} //lint !e753

int
switchover_prov_state_fault_g(int argc, char **argv)
{
	os::printf("*** Switching over provider state with fault ***\n");

#ifdef _FAULT_INJECTION

	int		rslt = 1;
	struct opt_t    *p_opt;
	nodeid_t	fault_node;
	Environment	e;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if ((argc < 4) || (argc > 5)) {
		print_switchover_prov_state_usage(argv[0]);
		return (rslt);
	}

	// Allocate p_opt
	p_opt = new struct opt_t;

	p_opt->service_desc = new char[os::strlen(argv[1])+1];
	os::sprintf(p_opt->service_desc, "%s", argv[1]);

	if (get_state(p_opt->service_desc) != replica::S_UP) {
		os::printf("+++ FAIL: service %s is not "
			"in S_UP state\n", p_opt->service_desc);
		delete [] p_opt->service_desc;
		delete p_opt;
		return (rslt);
	}

	p_opt->prov_desc = new char[os::strlen(argv[2])+1];
	os::sprintf(p_opt->prov_desc, "%s", argv[2]);

	if (os::strcmp(argv[3], "primary") == 0)
		p_opt->new_state = replica::SC_SET_PRIMARY;
	else if (os::strcmp(argv[3], "secondary") == 0)
		p_opt->new_state = replica::SC_SET_SECONDARY;
	else if (os::strcmp(argv[3], "spare") == 0)
		p_opt->new_state = replica::SC_SET_SPARE;
	else {
		os::printf("ERROR: <prov_state> should be "
			"primary, secondary or spare only\n");
		delete [] p_opt->service_desc;
		delete [] p_opt->prov_desc;
		delete p_opt;
		return (rslt);
	}

	if (argc == 5) {
		fault_node = (nodeid_t)os::atoi(argv[4]);
	} else {
		if ((fault_node = get_primary(p_opt->service_desc)) == 0)
			return (1);
	}

	os::printf(" ** Will fault node: %d\n", fault_node);

	// Populate fault arguments
	FaultFunctions::svc_reboot_arg_t	reboot_args;
	reboot_args.sid	 = get_sid(p_opt->service_desc);
	reboot_args.op	  = FaultFunctions::WAIT_FOR_UNKNOWN;
	reboot_args.nodeid	= fault_node;

	// Fault on provider
	rslt = switchover_prov_state(
		fault_repl_prov_switchover_on,
		fault_repl_prov_switchover_off,
		&reboot_args, (int)sizeof (reboot_args),
		p_opt, e);

	delete [] p_opt->service_desc;
	delete [] p_opt->prov_desc;
	delete p_opt;

	return (rslt);

#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
} //lint !e753
