/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)entry_stress.cc	1.17	08/05/20 SMI"

#include	<orbtest/repl_sample/repl_sample_clnt.h>
#include	<sys/os.h>
#include	<orb/invo/common.h>
#include	<orbtest/rm_probe/rm_snap.h>
#include	<orb/infrastructure/orb_conf.h>
#include	<orb/fault/fault_injection.h>
#include	<h/mc_sema.h>
#include	<sys/mc_sema_impl.h>

#include	<orbtest/repl_sample/faults/entry_stress_aux.h>

#include	<orbtest/repl_sample/faults/fault_stress_1.h>

//
// Stress 1 Test case
//
int
stress_1(int argc, char *argv[])
{
	os::printf("*** Stress Test Case Scenario 1\n");

	int	thread_multiplier = 1;

	if (argc > 1) {
		thread_multiplier = os::atoi(argv[1]);
		if (thread_multiplier == 0) {
			os::printf("*** Multiplier must be non-zero\n");
			thread_multiplier = 1;
		}
	}

	os::printf("*** Setting thread multiplier to %d\n", thread_multiplier);

#ifdef _FAULT_INJECTION

	// The arguments to pass to the on/off functions
	// Specified the cookie to the sync sema
	// and the node to wait for (to go unknown)
	sema_node_pair_t		arg;

	// A mc_sema object to synchronize all test threads
	mc_sema::sema_var		sema_ref;

	// Figure out which node we'd like to fault (Primary node)
	rm_snap				_rma_snapshot;
	nodeid_t			primary_nodeid;
	Environment			e;
	int				rslt;

	// Initialize the mc sema
	sema_ref = (new mc_sema_impl)->get_objref();
	ASSERT(!CORBA::is_nil(sema_ref));

	sema_ref->init(0, e);
	if (e.exception()) {
		e.exception()->print_exception("mc_sema::sema::init()");
		return (1);
	}

	// Get a snapshot of the rma
	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	// Get the primary node from the local rma
	if (_rma_snapshot.get_primary(primary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get primary nodeid\n");
		return (1);
	}

	// Fill in the details for the fault arguments
	arg.nodeid = primary_nodeid;
	arg.semaid = sema_ref->get_cookie(e);
	if (e.exception()) {
		e.exception()->print_exception("mc_sema::sema::get_cookie()");
		return (1);
	}

	os::printf("    Primary is node: %d\n", primary_nodeid);

#if 0
	// Note: This is ifdef'ed out since the FAULTNUM_PM_PANIC_IN_PATH_DOWN
	//	 fault point conflicts with the pathend_fail fault (when
	//	 turned on from arm_faults).  This code is left here in case
	//	 anybody needs to reenable it.

	// If the wrong node dies, panic in path_down
	uint64_t nodemask;
	nodemask = (uint64_t)1 << (primary_nodeid - 1);
	NodeTriggers::add(FAULTNUM_PM_PANIC_IN_PATH_DOWN, &nodemask,
	    sizeof (nodemask), TRIGGER_ALL_NODES);
#endif

	// Initialize the test status
	test_status.lck.lock();
	test_status.running_threads = 0;
	test_status.fail = 0;
	test_status.test_count = 0;
	test_status.lck.unlock();

	// Entry for each test threads
	//
	// Some entries will re-use the on/off functions for other
	// tests.  That is OK.  This is because the test uses the same fault
	// points but a different client.
	//
	for (int count = 0; count < thread_multiplier; count++) {
		start_test_thread(
		    "Simple Mini-Transaction (Scenario 1, Fault A)",
		    simple_mini_1, fault_stress_1_a_on, fault_stress_1_a_off,
		    &arg, (int)sizeof (arg));
		start_test_thread(
		    "Simple Mini-Transaction (Scenario 1, Fault B)",
		    simple_mini_1, fault_stress_1_b_on, fault_stress_1_b_off,
		    &arg, (int)sizeof (arg));
		start_test_thread(
		    "Simple Mini-Transaction (Scenario 1, Fault C)",
		    simple_mini_1, fault_stress_1_c_on, fault_stress_1_c_off,
		    &arg, (int)sizeof (arg));
		start_test_thread(
		    "Simple Mini-Transaction (Scenario 1, Fault D)",
		    simple_mini_1, fault_stress_1_d_on, fault_stress_1_d_off,
		    &arg, (int)sizeof (arg));
		start_test_thread(
		    "One-Phase Idempotent Mini-Transaction (Scenario 2, "
		    "Fault A)", one_phase_mini_1, fault_stress_1_a_on,
		    fault_stress_1_a_off, &arg, (int)sizeof (arg));
		start_test_thread(
		    "One-Phase Idempotent Mini-Transaction (Scenario 2, "
		    "Fault B)", one_phase_mini_1, fault_stress_1_b_on,
		    fault_stress_1_b_off, &arg, (int)sizeof (arg));
		start_test_thread(
		    "One-Phase Idempotent Mini-Transaction (Scenario 2, "
		    "Fault C)", one_phase_mini_1, fault_stress_1_e_on,
		    fault_stress_1_e_off, &arg, (int)sizeof (arg));
		start_test_thread(
		    "One-Phase Idempotent Mini-Transaction (Scenario 2, "
		    "Fault H)", one_phase_mini_1, fault_stress_1_d_on,
		    fault_stress_1_d_off, &arg, (int)sizeof (arg));
		start_test_thread(
		    "One-Phase Non-Idempotent Mini-Transaction (Scenario 2, "
		    "Fault A)", one_phase_mini_2, fault_stress_1_a_on,
		    fault_stress_1_a_off, &arg, (int)sizeof (arg));
		start_test_thread(
		    "One-Phase Non-Idempotent Mini-Transaction (Scenario 2, "
		    "Fault B)", one_phase_mini_2, fault_stress_1_b_on,
		    fault_stress_1_b_off, &arg, (int)sizeof (arg));
		start_test_thread(
		    "One-Phase Non-Idempotent Mini-Transaction (Scenario 2, "
		    "Fault C)", one_phase_mini_2, fault_stress_1_f_on,
		    fault_stress_1_f_off, &arg, (int)sizeof (arg));
		start_test_thread(
		    "One-Phase Non-Idempotent Mini-Transaction (Scenario 2, "
		    "Fault H)", one_phase_mini_2, fault_stress_1_d_on,
		    fault_stress_1_d_off, &arg, (int)sizeof (arg));
		start_test_thread(
		    "Two-Phase Mini-Transaction (Scenario 2, Fault B)",
		    two_phase_mini_1, fault_stress_1_g_on, fault_stress_1_g_off,
		    &arg, (int)sizeof (arg));
		start_test_thread(
		    "Two-Phase Mini-Transaction (Scenario 2, Fault G)",
		    two_phase_mini_1, fault_stress_1_h_on, fault_stress_1_h_off,
		    &arg, (int)sizeof (arg));
		start_test_thread(
		    "Two-Phase Mini-Transaction (Scenario 2, Fault N)",
		    two_phase_mini_1, fault_stress_1_i_on, fault_stress_1_i_off,
		    &arg, (int)sizeof (arg));
		start_test_thread(
		    "Two-Phase Mini-Transaction (Scenario 2, Fault O)",
		    two_phase_mini_1, fault_stress_1_j_on, fault_stress_1_j_off,
		    &arg, (int)sizeof (arg));
	} // for loop

	//
	// Synchronize with all the test threads
	//
	os::warning("Synchronizing (%d) test threads", test_status.test_count);
	for (int counter = 0; counter < test_status.test_count; counter++) {
		sema_ref->sema_p(e);
		if (e.exception()) {
			e.exception()->print_exception(
			    "mc_sema::sema::sema_p()");
			return (1);
		}
	}

	// At this point we've synched with all the threads reboot node 1
	FaultFunctions::wait_for_arg_t	reboot_args;
	reboot_args.nodeid	= primary_nodeid;
	reboot_args.op		= FaultFunctions::WAIT_FOR_UNKNOWN;

	FaultFunctions::reboot(0, &reboot_args, (uint32_t)sizeof (reboot_args));

	// Now wait for all the test threads to exit
	os::warning("Waiting for all test threads to exit...");
	test_status.lck.lock();
	while (test_status.running_threads != 0) {
		test_status.cv.wait(&(test_status.lck));
	}
	rslt = test_status.fail;
	test_status.lck.unlock();

#if 0
	// Note: This is ifdef'ed out since the FAULTNUM_PM_PANIC_IN_PATH_DOWN
	//	 fault point conflicts with the pathend_fail fault (when
	//	 turned on from arm_faults).  This code is left here in case
	//	 anybody needs to reenable it.
	NodeTriggers::clear(FAULTNUM_PM_PANIC_IN_PATH_DOWN, TRIGGER_ALL_NODES);
#endif

	return (rslt);
#else
	return (no_fault_injection());
#endif
}
