/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)entry_sec.cc	1.15	08/05/20 SMI"

#include	<orbtest/repl_sample/repl_sample_clnt.h>
#include	<sys/os.h>
#include	<orb/invo/common.h>
#include	<orb/infrastructure/orb_conf.h>
#include	<orb/fault/fault_injection.h>

#include	<orbtest/repl_sample/faults/entry_common.h>

#include	<orbtest/repl_sample/faults/fault_sec_1_e.h>
#include	<orbtest/repl_sample/faults/fault_sec_1_g.h>
#include	<orbtest/repl_sample/faults/fault_sec_1_i.h>
#include	<orbtest/repl_sample/faults/fault_sec_1_k.h>
#include	<orbtest/repl_sample/faults/fault_sec_1_l.h>
#include	<orbtest/repl_sample/faults/fault_sec_1_m.h>
#include	<orbtest/repl_sample/faults/fault_sec_1_o.h>
#include	<orbtest/repl_sample/faults/fault_sec_1_q.h>

#include	<orbtest/repl_sample/faults/fault_sec_2_b.h>

//
// Helper
//
#if defined(_KERNEL_ORB) && defined(_FAULT_INJECTION)
int
add_secondary_helper(FAULT_FPTR(f_on), FAULT_FPTR(f_off),
	nodeid_t fault_node, replica::service_state state, int sec_count);
#else
int
add_secondary_helper(void *, void *, nodeid_t, replica::service_state, int);
#endif // _KERNEL_ORB && _FAULT_INJECTION

#if defined(_FAULT_INJECTION)
int
remove_secondary_helper(FAULT_FPTR(f_on), FAULT_FPTR(f_off),
	nodeid_t fault_node, replica::service_state state, int sec_count);
#else
int
remove_secondary_helper(void *, void *, nodeid_t, replica::service_state, int);
#endif // _FAULT_INJECTION

//
// These are test cases for operations on the secondary providers, such
// as adding, removing, etc.
//

//
// Secondary 1-A Test case
//
// N/A


//
// Secondary 1-B Test case
//
// N/A


//
// Secondary 1-C Test case
//
// N/A


//
// Secondary 1-D Test case
//
int
sec_1_d(int, char *[])
{
	os::printf("*** Operation on Secondary Scenario 1 Fault D\n");

#ifdef _FAULT_INJECTION
	// Add a secondary
	return (add_secondary_helper(NULL, NULL, 0, replica::S_UP, 1));

#else
	return (no_fault_injection());
#endif
}


//
// Secondary 1-E Test case
//
int
sec_1_e(int, char *[])
{
	os::printf("*** Operation on Secondary Scenario 1 Fault E\n");

#ifdef _FAULT_INJECTION
	// Add a secondary
	// Fault before freeze_primary() (State 0)
	// Kill primary node (for svc)
	return (add_secondary_helper(fault_sec_1_e_on, fault_sec_1_e_off,
	    get_primary("ha_sample_server"), replica::S_DOWN, 0));

#else
	return (no_fault_injection());
#endif
}


//
// Secondary 1-F Test case
//
int
sec_1_f(int, char *[])
{
	os::printf("*** Operation on Secondary Scenario 1 Fault F\n");

#ifdef _FAULT_INJECTION
	// Add a secondary
	// Fault before freeze_primary() (State 0)
	// Kill secondary node (the one being added)
	// Re-use fault points from sec_1_e
	return (add_secondary_helper(fault_sec_1_e_on, fault_sec_1_e_off,
	    orb_conf::node_number(), replica::S_UP, 0));

#else
	return (no_fault_injection());
#endif
}


//
// Secondary 1-G Test case
//
int
sec_1_g(int, char *[])
{
	os::printf("*** Operation on Secondary Scenario 1 Fault G\n");

#ifdef _FAULT_INJECTION
	// Add a secondary
	// Fault before freeze_service() (State 1)
	// Kill primary node
	return (add_secondary_helper(fault_sec_1_g_on, fault_sec_1_g_off,
	    get_primary("ha_sample_server"), replica::S_DOWN, 0));

#else
	return (no_fault_injection());
#endif
}


//
// Secondary 1-H Test case
//
int
sec_1_h(int, char *[])
{
	os::printf("*** Operation on Secondary Scenario 1 Fault H\n");

#ifdef _FAULT_INJECTION
	// Add a secondary
	// Fault before freeze_service() (State 1)
	// Kill secondary node (being added)
	// Re-use the fault point from sec_1_g
	return (add_secondary_helper(fault_sec_1_g_on, fault_sec_1_g_off,
	    orb_conf::node_number(), replica::S_UP, 0));

#else
	return (no_fault_injection());
#endif
}


//
// Secondary 1-I Test case
//
int
sec_1_i(int, char *[])
{
	os::printf("*** Operation on Secondary Scenario 1 Fault I\n");

#ifdef _FAULT_INJECTION
	// Add a secondary
	// Fault before add_secondary() (State 2)
	// Kill primary node
	return (add_secondary_helper(fault_sec_1_i_on, fault_sec_1_i_off,
	    get_primary("ha_sample_server"), replica::S_DOWN, 0));

#else
	return (no_fault_injection());
#endif
}


//
// Secondary 1-J Test case
//
int
sec_1_j(int, char *[])
{
	os::printf("*** Operation on Secondary Scenario 1 Fault J\n");

#ifdef _FAULT_INJECTION
	// Add a secondary
	// Fault before add_secondary() (State 2)
	// Kill secondary node (the prov being added)
	// Re-use the fault point in sec_1_i
	return (add_secondary_helper(fault_sec_1_i_on, fault_sec_1_i_off,
	    orb_conf::node_number(), replica::S_DOWN, 0));

#else
	return (no_fault_injection());
#endif
}


//
// Secondary 1-K Test case
//
int
sec_1_k(int, char *[])
{
	os::printf("*** Operation on Secondary Scenario 1 Fault K\n");

#ifdef _FAULT_INJECTION
	// Add a secondary
	// Fault during add_secondary() (State 3)
	// Kill primary node
	return (add_secondary_helper(fault_sec_1_k_on, fault_sec_1_k_off,
	    get_primary("ha_sample_server"), replica::S_DOWN, 0));

#else
	return (no_fault_injection());
#endif
}


//
// Secondary 1-L Test case
//
int
sec_1_l(int, char *[])
{
	os::printf("*** Operation on Secondary Scenario 1 Fault L\n");

#ifdef _FAULT_INJECTION
	// Add a secondary
	// Fault during add_secondary() (State 3)
	// Kill primary node
	return (add_secondary_helper(fault_sec_1_l_on, fault_sec_1_l_off,
	    orb_conf::node_number(), replica::S_UP, 0));

#else
	return (no_fault_injection());
#endif
}


//
// Secondary 1-M Test case
//
int
sec_1_m(int, char *[])
{
	os::printf("*** Operation on Secondary Scenario 1 Fault M\n");

#ifdef _FAULT_INJECTION
	// Add a secondary
	// Fault before commit_secondary() (State 3)
	// Since the add_secondary failed, and we're not failing the RM
	// the secondary is allowed to take over the failed primary
	// Kill primary node
	if (add_secondary_helper(fault_sec_1_m_on, fault_sec_1_m_off,
	    get_primary("ha_sample_server"), replica::S_UP, 0) != 0) {
		os::printf("+++ FAIL: add_secondary_helper() failed\n");
		return (1);
	}

	Environment e;

	return (shutdown_service_nowork("ha_sample_server", e, false));
#else
	return (no_fault_injection());
#endif
}


//
// Secondary 1-N Test case
//
int
sec_1_n(int, char *[])
{
	os::printf("*** Operation on Secondary Scenario 1 Fault N\n");

#ifdef _FAULT_INJECTION
	// Add a secondary
	// Fault before commit_secondary() (State 3)
	// Kill secondary node (the one being added)
	return (add_secondary_helper(fault_sec_1_m_on, fault_sec_1_m_off,
	    orb_conf::node_number(), replica::S_UP, 0));

#else
	return (no_fault_injection());
#endif
}


//
// Secondary 1-O Test case
//
int
sec_1_o(int, char *[])
{
	os::printf("*** Operation on Secondary Scenario 1 Fault O\n");

#ifdef _FAULT_INJECTION
	// Add a secondary
	// Fault before unfreeze_primary() (State 2)
	// Kill primary node
	if (add_secondary_helper(fault_sec_1_o_on, fault_sec_1_o_off,
	    get_primary("ha_sample_server"), replica::S_UP, 0) != 0) {
		os::printf("+++ FAIL: add_secondary_helper() failed\n");
		return (1);
	}

	Environment e;

	return (shutdown_service_nowork("ha_sample_server", e, false));
#else
	return (no_fault_injection());
#endif
}


//
// Secondary 1-P Test case
//
int
sec_1_p(int, char *[])
{
	os::printf("*** Operation on Secondary Scenario 1 Fault P\n");

#ifdef _FAULT_INJECTION
	// Add a secondary
	// Fault before unfreeze_primary() (State 2)
	// Kill secondary node (the one being added)
	// Reuse the fualt points from sec_1_o
	return (add_secondary_helper(fault_sec_1_o_on, fault_sec_1_o_off,
	    orb_conf::node_number(), replica::S_UP, 0));

#else
	return (no_fault_injection());
#endif
}


//
// Secondary 1-Q Test case
//
int
sec_1_q(int, char *[])
{
	os::printf("*** Operation on Secondary Scenario 1 Fault Q\n");

#ifdef _FAULT_INJECTION
	// Add a secondary
	// Fault before unfreeze_service() (State 11)
	// Kill primary node
	if (add_secondary_helper(fault_sec_1_q_on, fault_sec_1_q_off,
	    get_primary("ha_sample_server"), replica::S_UP, 0) != 0) {
		os::printf("+++ FAIL: add_secondary_helper() failed\n");
		return (1);
	}
	Environment e;

	return (shutdown_service_nowork("ha_sample_server", e, false));
#else
	return (no_fault_injection());
#endif
}


//
// Secondary 1-R Test case
//
int
sec_1_r(int, char *[])
{
	os::printf("*** Operation on Secondary Scenario 1 Fault R\n");

#ifdef _FAULT_INJECTION
	// Add a secondary
	// Fault before unfreeze_service() (State 11)
	// Kill secondary node (the one being added)
	// Reuse the fault point from test sec_1_q
	return (add_secondary_helper(fault_sec_1_q_on, fault_sec_1_q_off,
	    orb_conf::node_number(), replica::S_UP, 0));

#else
	return (no_fault_injection());
#endif
}


//
// Secondary 2-A Test case
//
int
sec_2_a(int, char *[])
{
	os::printf("*** Operation on Secondary Scenario 2 Fault A\n");

#ifdef _FAULT_INJECTION
	// Remove a secondary
	return (remove_secondary_helper(NULL, NULL, 0, replica::S_UP, 0));

#else
	return (no_fault_injection());
#endif
}


//
// Secondary 2-B Test case
//
int
sec_2_b(int, char *[])
{
	os::printf("*** Operation on Secondary Scenario 2 Fault B\n");

#ifdef _FAULT_INJECTION
	// Remove a secondary
	// Fault before remove_secondary() (Step 0)
	// Kill primary node
	return (remove_secondary_helper(fault_sec_2_b_on, fault_sec_2_b_off,
	    get_primary("ha_sample_server"), replica::S_DOWN, 0));

#else
	return (no_fault_injection());
#endif
}


//
// Secondary 2-C Test case
//
int
sec_2_c(int, char *[])
{
	os::printf("*** Operation on Secondary Scenario 2 Fault C\n");

#ifdef _FAULT_INJECTION
	// Remove a secondary
	// Fault before remove_secondary() (Step 0)
	// Kill secondary (the one being removed)
	// Re-use the fault points from sec_2_b
	return (remove_secondary_helper(fault_sec_2_b_on, fault_sec_2_b_off,
	    get_secondary(1, "ha_sample_server"), replica::S_UP, 0));

#else
	return (no_fault_injection());
#endif
}


//
// Secondary 2-D Test case
//
int
sec_2_d(int, char *[])
{
	os::printf("*** Operation on Secondary Scenario 2 Fault D\n");

#ifdef _FAULT_INJECTION
	// Add a secondary
	return (add_secondary_helper(NULL, NULL, 0, replica::S_UP, 2));

#else
	return (no_fault_injection());
#endif
}


//
// Secondary 2-E Test case
//
int
sec_2_e(int, char *[])
{
	os::printf("*** Operation on Secondary Scenario 2 Fault E\n");

#ifdef _FAULT_INJECTION
	// Add a secondary
	// Fault before freeze_primary() (State 0)
	// Kill primary node (for svc)
	if (add_secondary_helper(fault_sec_1_e_on, fault_sec_1_e_off,
	    get_primary("ha_sample_server"), replica::S_UP, 1) != 0) {
		os::printf("+++ FAIL: add_secondary_helper() failed\n");
		return (1);
	}

	Environment e;
	return (shutdown_service_nowork("ha_sample_server", e, false));
#else
	return (no_fault_injection());
#endif
}


//
// Secondary 2-F Test case
//
int
sec_2_f(int, char *[])
{
	os::printf("*** Operation on Secondary Scenario 2 Fault F\n");

#ifdef _FAULT_INJECTION
	// Add a secondary
	// Fault before freeze_primary() (State 0)
	// Kill secondary node (the one being added)
	// Re-use fault points from sec_1_e
	return (add_secondary_helper(fault_sec_1_e_on, fault_sec_1_e_off,
	    orb_conf::node_number(), replica::S_UP, 1));

#else
	return (no_fault_injection());
#endif
}


//
// Secondary 2-G Test case
//
int
sec_2_g(int, char *[])
{
	os::printf("*** Operation on Secondary Scenario 2 Fault G\n");

#ifdef _FAULT_INJECTION
	// Add a secondary
	// Fault before freeze_service() (State 1)
	// Kill primary node
	if (add_secondary_helper(fault_sec_1_g_on, fault_sec_1_g_off,
	    get_primary("ha_sample_server"), replica::S_UP, 1) != 0) {
		os::printf("+++ FAIL: add_secondary_helper() failed\n");
		return (1);
	}

	Environment e;
	return (shutdown_service_nowork("ha_sample_server", e, false));
#else
	return (no_fault_injection());
#endif
}


//
// Secondary 2-H Test case
//
int
sec_2_h(int, char *[])
{
	os::printf("*** Operation on Secondary Scenario 2 Fault H\n");

#ifdef _FAULT_INJECTION
	// Add a secondary
	// Fault before freeze_service() (State 1)
	// Kill secondary node (being added)
	// Re-use the fault point from sec_1_g
	return (add_secondary_helper(fault_sec_1_g_on, fault_sec_1_g_off,
	    orb_conf::node_number(), replica::S_UP, 1));

#else
	return (no_fault_injection());
#endif
}


//
// Secondary 2-I Test case
//
int
sec_2_i(int, char *[])
{
	os::printf("*** Operation on Secondary Scenario 2 Fault I\n");

#ifdef _FAULT_INJECTION
	// Add a secondary
	// Fault before add_secondary() (State 2)
	// Kill primary node
	if (add_secondary_helper(fault_sec_1_i_on, fault_sec_1_i_off,
	    get_primary("ha_sample_server"), replica::S_UP, 1) != 0) {
		os::printf("+++ FAIL: add_secondary_helper() failed\n");
		return (1);
	}

	Environment e;
	return (shutdown_service_nowork("ha_sample_server", e, false));
#else
	return (no_fault_injection());
#endif
}


//
// Secondary 2-J Test case
//
int
sec_2_j(int, char *[])
{
	os::printf("*** Operation on Secondary Scenario 2 Fault J\n");

#ifdef _FAULT_INJECTION
	// Add a secondary
	// Fault before add_secondary() (State 2)
	// Kill secondary node (the prov being added)
	// Re-use the fault point in sec_1_i
	return (add_secondary_helper(fault_sec_1_i_on, fault_sec_1_i_off,
	    orb_conf::node_number(), replica::S_UP, 1));

#else
	return (no_fault_injection());
#endif
}


//
// Secondary 2-K Test case
//
int
sec_2_k(int, char *[])
{
	os::printf("*** Operation on Secondary Scenario 2 Fault K\n");

#ifdef _FAULT_INJECTION
	// Add a secondary
	// Fault during add_secondary() (State 3)
	// Kill primary node
	return (add_secondary_helper(fault_sec_1_k_on, fault_sec_1_k_off,
	    get_primary("ha_sample_server"), replica::S_UP, 1));

#else
	return (no_fault_injection());
#endif
}


//
// Secondary 2-L Test case
//
int
sec_2_l(int, char *[])
{
	os::printf("*** Operation on Secondary Scenario 2 Fault L\n");

#ifdef _FAULT_INJECTION
	// Add a secondary
	// Fault during add_secondary() (State 3)
	// Kill primary node
	return (add_secondary_helper(fault_sec_1_l_on, fault_sec_1_l_off,
	    orb_conf::node_number(), replica::S_UP, 1));

#else
	return (no_fault_injection());
#endif
}


//
// Secondary 2-M Test case
//
int
sec_2_m(int, char *[])
{
	os::printf("*** Operation on Secondary Scenario 2 Fault M\n");

#ifdef _FAULT_INJECTION
	// Add a secondary
	// Fault before commit_secondary() (State 3)
	// Since the add_secondary failed, and we're not failing the RM
	// the secondary is allowed to take over the failed primary
	// Kill primary node
	if (add_secondary_helper(fault_sec_1_m_on, fault_sec_1_m_off,
	    get_primary("ha_sample_server"), replica::S_UP, 1) != 0) {
		os::printf("+++ FAIL: add_secondary_helper() failed\n");
		return (1);
	}

	Environment e;
	return (shutdown_service_nowork("ha_sample_server", e, false));
#else
	return (no_fault_injection());
#endif
}


//
// Secondary 2-N Test case
//
int
sec_2_n(int, char *[])
{
	os::printf("*** Operation on Secondary Scenario 2 Fault N\n");

#ifdef _FAULT_INJECTION
	// Add a secondary
	// Fault before commit_secondary() (State 3)
	// Kill secondary node (the one being added)
	return (add_secondary_helper(fault_sec_1_m_on, fault_sec_1_m_off,
	    orb_conf::node_number(), replica::S_UP, 1));

#else
	return (no_fault_injection());
#endif
}


//
// Secondary 2-O Test case
//
int
sec_2_o(int, char *[])
{
	os::printf("*** Operation on Secondary Scenario 2 Fault O\n");

#ifdef _FAULT_INJECTION
	// Add a secondary
	// Fault before unfreeze_primary() (State 2)
	// Kill primary node
	if (add_secondary_helper(fault_sec_1_o_on, fault_sec_1_o_off,
	    get_primary("ha_sample_server"), replica::S_UP, 1) != 0) {
		os::printf("+++ FAIL: add_secondary_helper() failed\n");
		return (1);
	}

	Environment e;
	return (shutdown_service_nowork("ha_sample_server", e, false));
#else
	return (no_fault_injection());
#endif
}


//
// Secondary 2-P Test case
//
int
sec_2_p(int, char *[])
{
	os::printf("*** Operation on Secondary Scenario 2 Fault P\n");

#ifdef _FAULT_INJECTION
	// Add a secondary
	// Fault before unfreeze_primary() (State 2)
	// Kill secondary node (the one being added)
	// Reuse the fualt points from sec_1_o
	return (add_secondary_helper(fault_sec_1_o_on, fault_sec_1_o_off,
	    orb_conf::node_number(), replica::S_UP, 1));

#else
	return (no_fault_injection());
#endif
}


//
// Secondary 2-Q Test case
//
int
sec_2_q(int, char *[])
{
	os::printf("*** Operation on Secondary Scenario 2 Fault Q\n");

#ifdef _FAULT_INJECTION
	// Add a secondary
	// Fault before unfreeze_service() (State 11)
	// Kill primary node
	if (add_secondary_helper(fault_sec_1_q_on, fault_sec_1_q_off,
	    get_primary("ha_sample_server"), replica::S_UP, 1) != 0) {
		os::printf("+++ FAIL: add_secondary_helper() failed\n");
		return (1);
	}

	Environment e;
	return (shutdown_service_nowork("ha_sample_server", e, false));
#else
	return (no_fault_injection());
#endif
}


//
// Secondary 2-R Test case
//
int
sec_2_r(int, char *[])
{
	os::printf("*** Operation on Secondary Scenario 2 Fault R\n");

#ifdef _FAULT_INJECTION
	// Add a secondary
	// Fault before unfreeze_service() (State 11)
	// Kill secondary node (the one being added)
	// Reuse the fault point from test sec_1_q
	return (add_secondary_helper(fault_sec_1_q_on, fault_sec_1_q_off,
	    orb_conf::node_number(), replica::S_UP, 1));

#else
	return (no_fault_injection());
#endif
}


//
// Secondary 3-A Test case
//
int
sec_3_a(int, char *[])
{
	os::printf("*** Operation on Secondary Scenario 3 Fault A\n");

#ifdef _FAULT_INJECTION
	// Remove a secondary
	return (remove_secondary_helper(NULL, NULL, 0, replica::S_UP, 1));

#else
	return (no_fault_injection());
#endif
}


//
// Secondary 3-B Test case
//
int
sec_3_b(int, char *[])
{
	os::printf("*** Operation on Secondary Scenario 3 Fault B\n");

#ifdef _FAULT_INJECTION
	// Remove a secondary
	// Fault before remove_secondary() (Step 0)
	// Kill primary node
	// Re-use fault points from sec_2_b
	return (remove_secondary_helper(fault_sec_2_b_on, fault_sec_2_b_off,
	    get_primary("ha_sample_server"), replica::S_UP, 0));

#else
	return (no_fault_injection());
#endif
}


//
// Secondary 3-C Test case
//
int
sec_3_c(int, char *[])
{
	os::printf("*** Operation on Secondary Scenario 3 Fault C\n");

#ifdef _FAULT_INJECTION
	// Remove a secondary
	// Fault before remove_secondary() (Step 0)
	// Kill secondary (the one being removed)
	// Re-use the fault points from sec_2_b
	return (remove_secondary_helper(fault_sec_2_b_on, fault_sec_2_b_off,
	    get_secondary(1, "ha_sample_server"), replica::S_UP, 0));

#else
	return (no_fault_injection());
#endif
}



//
// Helper (Add Sec Helper)
//
// f_on		- Function pointer to turn faults on
// f_off	- Function pointer to turn faults off
// fault_node	- Nodeid of node to fault on
// state	- state that we expect to find the cluster on aftet test
// sec_count	- Count of the number of secondaries after test
//
#if defined(_KERNEL_ORB) && defined(_FAULT_INJECTION)
int
add_secondary_helper(FAULT_FPTR(f_on), FAULT_FPTR(f_off),
	nodeid_t fault_node, replica::service_state state, int sec_count)
#else
int
add_secondary_helper(void *, void *, nodeid_t, replica::service_state, int)
#endif // _KERNEL_ORB && _FAULT_INJECTION
{
#ifdef _FAULT_INJECTION

#ifdef _KERNEL_ORB
	//
	// Helper to add secondary tests
	//
	Environment	e;
	int		rslt;
	char		prov_desc[5];
	nodeid_t	primary;
	int		counter;

	// Verify that service is up
	if (get_state("ha_sample_server") != replica::S_UP) {
		os::printf("+++ FAIL: service is not in S_UP state\n");
		return (1);
	}

	if ((primary = get_primary("ha_sample_server")) == 0)
		return (1);

	os::printf(" ** Primary node is: %d\n", primary);
	os::printf(" ** Will fault node: %d\n", fault_node);

	// Do work on service prior to test
	(void) do_work("ha_sample_server");

	os::sprintf(prov_desc, "%d", orb_conf::node_number());

	// Populate fault arguments
	FaultFunctions::ha_dep_reboot_arg_t	reboot_args;
	reboot_args.sid		= get_sid("ha_sample_server");
	reboot_args.op		= FaultFunctions::WAIT_FOR_UNKNOWN;
	reboot_args.nodeid	= fault_node;

	rslt = register_with_rm(
	    f_on, f_off,
	    &reboot_args, (int)sizeof (reboot_args),
	    "ha_sample_server",
	    prov_desc,
	    e, 0, NULL);
	if (e.exception()) {
		e.exception()->print_exception("register_with_rm:");
		os::printf("+++ FAIL: Exception was not expected\n");
		return (1);
	}

	// Do work if the state is supposed to be UP
	if (state == replica::S_UP)
		(void) do_work("ha_sample_server");

	// Verify that service is at the expected state
	if (get_state("ha_sample_server") != state) {
		os::printf("+++ FAIL: service state (%d) is not in expected "
		    "state (%d)\n", get_state("ha_sample_server"), state);
		return (1);
	}

	if (state == replica::S_UP) {
		// Verify that we can get a primary out of this
		os::printf(" ** Verifying that a primary server exists\n");
		if (get_primary("ha_sample_server") == 0) {
			os::printf("+++ FAIL: Could not get primary nodeid"
			    " for service \"ha_sample_server\"\n");
			return (1);
		}
		// Verify that we can find as many secondaries as we specified
		os::printf(" ** Verifying that %d secondaries exist\n",
		    sec_count);
		for (counter = 0; ; counter++) {
			if (get_secondary(counter + 1, "ha_sample_server",
			    false) == 0) {
				break;
			} // getting all secondaries
		}

		if (counter != sec_count) {
			os::printf("+++ FAIL: Number of providers in cluster "
			    "%d does not match expected count %d.\n", counter,
			    sec_count);
			return (1);
		}
	}

	if (rslt)
		os::printf("+++ FAIL: Non-zero (%d) returned from client "
		    "function\n", rslt);
	else
		os::printf("--- PASS: Adding secondary was succesful\n");

	return (rslt);
#else
	os::printf("+++ FAIL: Test not supported in USER mode\n");
	return (1);

#endif // _KERNEL_ORB

#else
	return (no_fault_injection());
#endif
}

//
// Helper (Add Sec Helper)
//
// f_on		- Function pointer to turn faults on
// f_off	- Function pointer to turn faults off
// fault_node	- Nodeid of node to fault on
// state	- state that we expect to find the cluster on aftet test
// sec_count	- Count of the number of secondaries after test
//
#if defined(_FAULT_INJECTION)
int
remove_secondary_helper(FAULT_FPTR(f_on), FAULT_FPTR(f_off),
	nodeid_t fault_node, replica::service_state state, int sec_count)
#else
int
remove_secondary_helper(void *, void *, nodeid_t, replica::service_state, int)
#endif // _FAULT_INJECTION
{
#ifdef _FAULT_INJECTION
	//
	// Helper to add secondary tests
	//
	Environment	e;
	int		rslt;
	nodeid_t	primary;

	// Verify that service is up
	if (get_state("ha_sample_server") != replica::S_UP) {
		os::printf("+++ FAIL: service is not in S_UP state\n");
		return (1);
	}

	if ((primary = get_primary("ha_sample_server")) == 0)
		return (1);

	os::printf(" ** Primary node is: %d\n", primary);
	os::printf(" ** Will fault node: %d\n", fault_node);

	bool work;
	if (state == replica::S_UP)
		work = true;
	else
		work = false;

	// Populate fault arguments
	FaultFunctions::ha_dep_reboot_arg_t	reboot_args;
	reboot_args.sid		= get_sid("ha_sample_server");
	reboot_args.op		= FaultFunctions::WAIT_FOR_UNKNOWN;
	reboot_args.nodeid	= fault_node;

	rslt = change_sec_state(
	    f_on, f_off,
	    &reboot_args, (int)sizeof (reboot_args),
	    "ha_sample_server",
	    replica::SC_REMOVE_REPL_PROV,
	    e, work);
	if (e.exception()) {
		e.exception()->print_exception("change_sec_state:");
		os::printf("+++ FAIL: Exception was not expected\n");
		return (1);
	}

	// Verify that service is at the expected state
	if (get_state("ha_sample_server") != state) {
		os::printf("+++ FAIL: service state (%d) is not in expected "
		    "state (%d)\n", get_state("ha_sample_server"), state);
		return (1);
	}

	if (state == replica::S_UP) {
		// Verify that we can get a primary out of this
		os::printf(" ** Verifying that a primary server exists\n");
		if (get_primary("ha_sample_server") == 0) {
			os::printf("+++ FAIL: Could not get primary nodeid"
			    " for service \"ha_sample_server\"\n");
			return (1);
		}
		// Verify that we can find as many secondaries as we specified
		os::printf(" ** Verifying that %d secondaries exist\n",
		    sec_count);
		for (int counter = 1; counter <= sec_count; counter++) {
			if (get_secondary(counter, "ha_sample_server") == 0) {
				os::printf("+++ FAIL: Secondary #%d is missing"
				    "\n", counter);
				return (1);
			} // getting all secondaries
		}
	}

	if (rslt)
		os::printf("+++ FAIL: Non-zero (%d) returned from client "
		    "function\n", rslt);
	else
		os::printf("--- PASS: Removing secondary was succesful\n");

	return (rslt);

#else
	return (no_fault_injection());
#endif
}
