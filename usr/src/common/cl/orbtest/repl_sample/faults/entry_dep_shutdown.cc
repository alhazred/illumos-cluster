/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)entry_dep_shutdown.cc	1.10	08/05/20 SMI"

#include	<h/replica.h>
#include	<h/mc_sema.h>
#include	<sys/mc_sema_impl.h>
#include	<sys/os.h>
#include	<orb/invo/common.h>
#include	<orb/infrastructure/orb_conf.h>
#include	<orb/fault/fault_injection.h>
#include	<orbtest/repl_sample/repl_sample_clnt.h>

#include	<orbtest/rm_probe/rm_snap.h>
#include	<orbtest/repl_sample/faults/entry_common.h>
#include	<orbtest/repl_sample/faults/entry_dep_aux.h>
#include	<orbtest/repl_sample/faults/entry_dep_shutdown_aux.h>

//
// Dependency shutdown Scenario 1 Fault A
//
int
dep_shutdown_1_a(int, char **)
{
	os::printf("*** Dependency Scenario 1 Fault A\n");

#ifdef _FAULT_INJECTION
	// Shutdown dependent services in wrong order
	// order should be: top -> down
	// will try bottom first
	// DEP is: service_2 -> service_1

	Environment	e;
	int		rslt;

	if (get_state("service_1") != replica::S_UP)
		return (1);

	if (get_state("service_2") != replica::S_UP)
		return (1);

	rslt = shutdown_service(NULL, NULL, NULL, 0, "service_1", e);
	if (e.exception()) {
		if (!replica::depends_on::_exnarrow(e.exception())) {
			os::printf("+++ FAIL: Shutdown should have failed with"
			    "depends_on exception!\n");
			return (1);
		}
	} else {
		os::printf("+++ FAIL: Shutdown should have failed with"
		    "depends_on exception!\n");
		return (1);
	}

	if (get_state("service_1") != replica::S_UP)
		return (1);

	if (get_state("service_2") != replica::S_UP)
		return (1);

	os::printf("--- PASS: Shutdown failed due to dependencies\n");
	return (0);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Dependency shutdown Scenario 1 Fault B
//
int
dep_shutdown_1_b(int, char **)
{
	os::printf("*** Dependency Scenario 1 Fault B\n");

#ifdef _FAULT_INJECTION

#ifdef _KERNEL_ORB
	// While shutting down a service register a prov that depends on it
	// The shutdown should prevent the register from completing succesfuly

	if (get_state("service_1") != replica::S_UP)
		return (1);

	// Create structure to pass to new thread
	int			rslt;
	Environment		e;
	retval_t		retval;
	mc_sema::sema_var	mc_sema_ref;

	mc_sema_ref = (new mc_sema_impl)->get_objref();
	ASSERT(!CORBA::is_nil(mc_sema_ref));
	mc_sema_ref->init(0, e);
	if (e.exception()) {
		e.exception()->print_exception("mc_sema::init()");
		os::printf("+++ FAIL: Exception initializing mc_sema\n");
		return (1);
	}

	retval.exitcount = 0;
	retval.id = mc_sema_ref->get_cookie(e);
	if (e.exception()) {
		e.exception()->print_exception("mc_sema::get_cookie");
		os::printf("+++ FAIL: Exception getting mc_sema cookie\n");
		return (1);
	}
	ASSERT(retval.id != 0);

#ifdef _KERNEL
	if (thread_create(NULL, NULL,
	    (void (*)())dep_shutdown_aux_hang_on_shutdown, (caddr_t)&retval, 0,
	    &p0, TS_RUN, 60) == NULL) {
		os::printf("+++ FAIL: thread_create failed\n");
		return (1);
	}
#else
	if (thr_create(NULL, (size_t)0, dep_shutdown_aux_hang_on_shutdown,
	    &retval, THR_BOUND, NULL)) {
		os::printf("+++ FAIL: thread_create failed\n");
		return (1);
	}
#endif // _KERNEL

	// Do a sema_p on the mc_sema that will cause is to block until the
	// other thread reached the shutdown Fault Point.
	os::printf(" ** Waiting for thread to reach shutdown\n");
	mc_sema_ref->sema_p(e);
	if (e.exception()) {
		e.exception()->print_exception("mc_sema::sema_p");
		os::printf("+++ FAIL: Exception during sema_p on mc_sema\n");
		return (1);
	}
	os::printf(" ** Thread has reached shutdown\n");

	// Now register another provider on this node with a dependency on
	// service_1
	char	*deps[1]	= { "service_1" };
	char	prov_desc[5];

	os::sprintf(prov_desc, "%d", orb_conf::node_number());

	FaultFunctions::invo_trigger_add(FaultFunctions::SIGNAL,
	    FAULTNUM_HA_FRMWK_REPL_SERVICE_REGISTER_WITH_RM_1, NULL, 0);
	rslt = register_with_rm(NULL, NULL, NULL, 0, "ha_sample_server",
	    prov_desc, e, 1, deps);
	if (e.exception()) {
		if (replica::invalid_dependency::_exnarrow(e.exception())) {
			rslt = 0;
		} else {
			e.exception()->print_exception("register_with_rm");
			os::printf("+++ FAIL: Exception while registering"
			    " dependent prov.\n");
			return (1);
		}
	} else {
		os::printf("+++ FAIL: Registering a service while a shutdown"
		    " is in progress should fail\n");
		return (1);
	}

	// Now we need to sync back up with the exiting thread (that tried to
	// do a shutdown) .. this thread should have got an exception
	// and set its value to non-zero

	// Signal the other thread
	FaultFunctions::signal(0, NULL, 0);

	retval.lck.lock();
	while (retval.exitcount != 1) {
		retval.cv.wait(&retval.lck);
	}
	rslt = retval.value;
	if (retval.e.exception()) {
		retval.e.exception()->print_exception("shutdown:");
		os::printf("+++ FAIL: Shutdown of service_1 failed...\n");
		rslt = 1;
	}
	retval.lck.unlock();

	if (!rslt) {
		os::printf("--- PASS: Shutdown already in progress blocked"
		    " registration\n");
		return (rslt);
	}

	return (rslt);
#else
	os::printf("+++ FAIL: Test not supported in user-land\n");
	return (1);
#endif // _KERNEL_ORB

#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Dependency shutdown Scenario 1 Fault C
//
int
dep_shutdown_1_c(int, char **)
{
	os::printf("*** Dependency Scenario 1 Fault C\n");

#ifdef _FAULT_INJECTION
	// Attempt to remove a provider of a colocated service (bottom prov)
	// We will attempt to remove a bottom provider of a secondary node
	// This should fail

	Environment	e;
	int		rslt;

	if ((get_state("service_1") != replica::S_UP) ||
	    (get_state("service_2") != replica::S_UP))
		return (1);

	rslt = change_sec_state(NULL, NULL, NULL, 0, "service_1",
	    replica::SC_REMOVE_REPL_PROV, e);
	if (e.exception()) {
		if (!replica::depends_on::_exnarrow(e.exception())) {
			e.exception()->print_exception("change_sec_state");
			os::printf("+++ FAIL: Incorrect exception returned"
			    " while removing provider\n");
			return (1);
		} else {
			rslt = 0;
		}
	} else {
		os::printf("+++ FAIL: Removing a provider with dependencies"
		    " should have failed\n");
		return (1);
	}

	// Make sure that both services are still up
	if ((get_state("service_1") != replica::S_UP) ||
	    (get_state("service_2") != replica::S_UP))
		return (1);

	os::printf("--- PASS: Removing a provider with dependencies fails as"
	    " expected\n");
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}
