/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ENTRY_ONE_PHASE_NON_IDEMPOTENT_H
#define	_ENTRY_ONE_PHASE_NON_IDEMPOTENT_H

#pragma ident	"@(#)entry_one_phase_non_idempotent.h	1.10	08/05/20 SMI"

#include <orbtest/repl_sample/faults/entry_common.h>

int one_phase_non_idempotent_1_a(int, char *[]);
int one_phase_non_idempotent_1_b(int, char *[]);
int one_phase_non_idempotent_1_i(int, char *[]);
int one_phase_non_idempotent_2_a(int, char *[]);
int one_phase_non_idempotent_2_b(int, char *[]);
int one_phase_non_idempotent_2_c(int, char *[]);
int one_phase_non_idempotent_2_d(int, char *[]);
int one_phase_non_idempotent_2_e(int, char *[]);
int one_phase_non_idempotent_2_g(int, char *[]);
int one_phase_non_idempotent_2_h(int, char *[]);
int one_phase_non_idempotent_2_i(int, char *[]);
int one_phase_non_idempotent_3_a(int, char *[]);
int one_phase_non_idempotent_3_b(int, char *[]);
int one_phase_non_idempotent_3_c(int, char *[]);
int one_phase_non_idempotent_3_d(int, char *[]);
int one_phase_non_idempotent_3_e(int, char *[]);
int one_phase_non_idempotent_3_f(int, char *[]);
int one_phase_non_idempotent_3_g(int, char *[]);
int one_phase_non_idempotent_3_h(int, char *[]);
int one_phase_non_idempotent_3_i(int, char *[]);

#endif	/* _ENTRY_ONE_PHASE_NON_IDEMPOTENT_H */
