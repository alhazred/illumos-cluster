/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fault_invoke_1_a.cc	1.7	08/05/20 SMI"

//
// This file contains the on functions for the fault points for
// testing an invocation that tries to go remote, retries locally,
// and then finally retries successfully remotely.
//

#include	<sys/os.h>
#include	<orb/fault/fault_injection.h>
#include	<orb/fault/fault_functions.h>

int
fault_invoke_1_a_on(void *argp, int arglen)
{

	ASSERT(argp != NULL);
	ASSERT(arglen == sizeof (nodeid_t));

	// Arm triggers
	os::warning("Armed fault: FAULTNUM_HA_REMOTE_LOCAL");
	InvoTriggers::add(FAULTNUM_HA_FRMWK_REMOTE_LOCAL, NULL, 0);

	os::warning("Armed fault: FAULTNUM_HA_LOCAL_REMOTE");
	InvoTriggers::add(FAULTNUM_HA_FRMWK_LOCAL_REMOTE, NULL, 0);

	return (0);
}
