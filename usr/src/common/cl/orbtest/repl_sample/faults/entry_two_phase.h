/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ENTRY_TWO_PHASE_H
#define	_ENTRY_TWO_PHASE_H

#pragma ident	"@(#)entry_two_phase.h	1.11	08/05/20 SMI"

#include <orbtest/repl_sample/faults/entry_common.h>

// Scenario 1
int two_phase_1_a(int, char *[]);
int two_phase_1_q(int, char *[]);

// Scenario 2
int two_phase_2_a(int, char *[]);
int two_phase_2_b(int, char *[]);
int two_phase_2_c(int, char *[]);
int two_phase_2_d(int, char *[]);
int two_phase_2_e(int, char *[]);
int two_phase_2_f(int, char *[]);
int two_phase_2_g(int, char *[]);

int two_phase_2_h(int, char *[]);
int two_phase_2_m(int, char *[]);

int two_phase_2_n(int, char *[]);
int two_phase_2_o(int, char *[]);
int two_phase_2_p(int, char *[]);
int two_phase_2_q(int, char *[]);

#endif	/* _ENTRY_TWO_PHASE_H */
