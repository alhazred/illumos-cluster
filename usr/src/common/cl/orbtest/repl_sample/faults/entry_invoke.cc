/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)entry_invoke.cc	1.11	08/05/20 SMI"

#include	<orbtest/repl_sample/repl_sample_clnt.h>
#include	<sys/os.h>
#include	<orb/invo/common.h>
#include	<orbtest/rm_probe/rm_snap.h>
#include	<orb/infrastructure/orb_conf.h>
#include 	<orb/fault/fault_injection.h>
#include	<orb/fault/fault_functions.h>
#include	<orbtest/repl_sample/faults/fault_invoke_1_a.h>

//
// This test tries to perform a remote invocation that is retried locally,
// and then successfully retried remotely.
//
int
invoke_1_a(int, char *[])
{
	os::printf("*** Multiple retry - invoke remote, retry locally,"
	    " retry again remote\n");

#ifdef _FAULT_INJECTION
	// Figure out which node we'd like to fault (Primary node)
	rm_snap			_rma_snapshot;
	nodeid_t		primary_nodeid;
	Environment		e;
	int			returnvalue;

	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_primary(primary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get primary nodeid\n");
		return (1);
	}

	os::printf("    Primary is node: %d\n", primary_nodeid);

	returnvalue = (simple_mini_1(
	    fault_invoke_1_a_on,
	    NULL,	// Upon test completion all triggers are already cleared
	    &primary_nodeid,
	    (int)sizeof (primary_nodeid),
	    e,
	    "ha_sample_server",
	    false));	// Option says don't do a bunch of preliminary invos

	if (e.exception()) {
		e.exception()->print_exception(
		    "repl_sample::value_obj::get_value()");
		return (1);
	}

	return (returnvalue);

#else
	return (no_fault_injection());
#endif
}

//
// This test tries to perform a local invocation that is retried remotely,
// and then successfully retried locally.
//
int
invoke_1_b(int, char *[])
{
	os::printf("*** Multiple retry - invoke locally, retry remotely,"
	    " retry again locally\n");

#ifdef _FAULT_INJECTION
	// Figure out which node we'd like to fault (Primary node)
	rm_snap			_rma_snapshot;
	nodeid_t		primary_nodeid;
	Environment		e;
	int			returnvalue;

	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_primary(primary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get primary nodeid\n");
		return (1);
	}

	os::printf("    Primary is node: %d\n", primary_nodeid);

	returnvalue = (simple_mini_1(
	    fault_invoke_1_a_on,
	    NULL,	// Upon test completion all triggers are already cleared
	    &primary_nodeid,
	    (int)sizeof (primary_nodeid),
	    e,
	    "ha_sample_server",
	    false));	// Option says don't do a bunch of preliminary invos

	if (e.exception()) {
		e.exception()->print_exception(
		    "repl_sample::value_obj::get_value()");
		return (1);
	}

	return (returnvalue);

#else
	return (no_fault_injection());
#endif
}
