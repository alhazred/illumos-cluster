/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ENTRY_DEP_SHUTDOWN_AUX_H
#define	_ENTRY_DEP_SHUTDOWN_AUX_H

#pragma ident	"@(#)entry_dep_shutdown_aux.h	1.8	08/05/20 SMI"

#include <sys/os.h>
#include <h/mc_sema.h>

//
// Structure to pass to the threads created when we attempt to create
// a race condition while shutting down a service and registering a dependency
//
typedef struct {
	int		value;
	Environment	e;
	int		exitcount;
	os::mutex_t	lck;
	os::condvar_t	cv;

	mc_sema::cookie	id;
} retval_t;

//
// Thread starting point for the race condition test case (as above)
//
void *dep_shutdown_aux_hang_on_shutdown(void *);

#endif	/* _ENTRY_DEP_SHUTDOWN_AUX_H */
