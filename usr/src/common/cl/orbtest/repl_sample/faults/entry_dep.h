/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ENTRY_DEP_H
#define	_ENTRY_DEP_H

#pragma ident	"@(#)entry_dep.h	1.11	08/05/20 SMI"

int dep_1_a(int, char *[]);
int dep_1_b(int, char *[]);
int dep_1_c(int, char *[]);
int dep_1_d(int, char *[]);
int dep_1_e(int, char *[]);
int dep_1_f(int, char *[]);
int dep_1_g(int, char *[]);
int dep_1_h(int, char *[]);
int dep_1_i(int, char *[]);
int dep_1_j(int, char *[]);
int dep_1_k(int, char *[]);
int dep_1_l(int, char *[]);
int dep_1_m(int, char *[]);
int dep_1_n(int, char *[]);

int dep_2_a(int, char *[]);
int dep_2_b(int, char *[]);
int dep_2_d(int, char *[]);
int dep_2_g(int, char *[]);
int dep_2_h(int, char *[]);
int dep_2_j(int, char *[]);
int dep_2_m(int, char *[]);
int dep_2_n(int, char *[]);

int dep_3_a(int, char *[]);
int dep_3_b(int, char *[]);
int dep_3_d(int, char *[]);
int dep_3_g(int, char *[]);
int dep_3_h(int, char *[]);
int dep_3_j(int, char *[]);
int dep_3_m(int, char *[]);
int dep_3_n(int, char *[]);

int dep_4_a(int, char *[]);
int dep_4_b(int, char *[]);
int dep_4_d(int, char *[]);
int dep_4_g(int, char *[]);
int dep_4_h(int, char *[]);
int dep_4_j(int, char *[]);
int dep_4_m(int, char *[]);
int dep_4_n(int, char *[]);

int dep_5_a(int, char *[]);
int dep_5_b(int, char *[]);
int dep_5_d(int, char *[]);
int dep_5_g(int, char *[]);
int dep_5_h(int, char *[]);
int dep_5_j(int, char *[]);
int dep_5_m(int, char *[]);
int dep_5_n(int, char *[]);

#endif	/* _ENTRY_DEP_H */
