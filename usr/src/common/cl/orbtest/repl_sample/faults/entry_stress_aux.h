/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ENTRY_STRESS_AUX_H
#define	_ENTRY_STRESS_AUX_H

#pragma ident	"@(#)entry_stress_aux.h	1.11	08/05/20 SMI"

#include <sys/os.h>

#include <orbtest/repl_sample/repl_sample_clnt.h>
#include <orbtest/repl_sample/faults/entry_common.h>

// Helper structure for keeping track of each ttest thread
typedef struct {
	os::mutex_t	lck;
	os::condvar_t	cv;
	int		test_count;
	int		running_threads;
	int		fail;
} test_status_t;

//
// Helper structure for passing a mc_sema cookie and a nodeid
//
typedef struct {
	mc_sema::cookie	semaid;
	nodeid_t	nodeid;
} sema_node_pair_t;

//
// Helper structure for thread starting points
//
typedef struct {
	char	*test_name;
	int (*client_func)(FAULT_FPTR(a), FAULT_FPTR(b), void *, int,
	    Environment &, char *, bool);
	FAULT_FPTR(on_func);
	FAULT_FPTR(off_func);
	void	*data;
	int	data_len;
} test_arg_t;

// This function will start new threads for each test case
int start_test_thread(
	const char *,
	int(*)(FAULT_FPTR(a), FAULT_FPTR(b), void *, int, Environment &,
	    char *, bool),
	FAULT_FPTR(on),
	FAULT_FPTR(off),
	void *,
	int);

// This is the starting point for the test threads
void *test_thread_start_point(void *argp);

// Global test status
extern test_status_t	test_status;

#endif	/* _ENTRY_STRESS_AUX_H */
