/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)entry_shutdown.cc	1.19	08/05/20 SMI"

#include	<sys/os.h>
#include	<orb/invo/common.h>
#include	<orb/infrastructure/orb_conf.h>
#include	<orb/fault/fault_injection.h>
#include	<orbtest/rm_probe/rm_snap.h>

#ifdef _KERNEL_ORB
#include	<repl/rma/rma.h>
#endif // _KERNEL_ORB

#include	<orbtest/repl_sample/repl_sample_clnt.h>
#include	<orbtest/repl_sample/faults/fault_shutdown_1_b.h>

#include	<orbtest/repl_sample/faults/fault_shutdown_2_b.h>
#include	<orbtest/repl_sample/faults/fault_shutdown_2_c.h>
#include	<orbtest/repl_sample/faults/fault_shutdown_2_e.h>
#include	<orbtest/repl_sample/faults/fault_shutdown_4_a.h>

#include	<orbtest/repl_sample/faults/fault_ha_rm.h>

//
// Shutdown 1-A Test case
//
int
shutdown_1_a(int, char *[])
{
	os::printf("*** Shutdown Scenario 1 Fault A\n");

#ifdef _FAULT_INJECTION
	// No Fault in this case
	rm_snap		_rma_snapshot;
	Environment	e;
	int		rslt;

	if (get_state("ha_sample_server") != replica::S_UP)
		return (1);

	rslt = shutdown_service(NULL, NULL, NULL, 0, "ha_sample_server", e);
	if (e.exception()) {
		e.exception()->print_exception("shutdown_service:");
		os::printf("+++ FAIL: Exception during shutdown was not "
		    "expected\n");
		return (1);
	}

	os::printf(" ** Verifying that service was shutdown.\n");

	// Verify that the state of the service doesn't exist
	if (_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Service \"ha_sample_server\" still "
		    "exists, and is in state: %d\n", _rma_snapshot.get_state());
		return (1);
	}

	if (rslt == 0)
		os::printf("--- PASS: Service was shutdown\n");
	else
		os::printf("+++ FAIL: Shutdown service returned error\n");

	return (rslt);
#else
	return (no_fault_injection());
#endif
}

//
// Shutdown 1-B Test case
//
int
shutdown_1_b(int, char *[])
{
	os::printf("*** Shutdown Scenario 1 Fault B\n");

#ifdef _FAULT_INJECTION
	rm_snap		_rma_snapshot;
	nodeid_t	primary_nodeid;
	Environment	e;
	int		rslt;

	if (get_state("ha_sample_server") != replica::S_UP)
		return (1);

	primary_nodeid = get_primary("ha_sample_server");
	if (primary_nodeid == 0)
		return (1);

	os::printf("    Primary is node: %d\n", primary_nodeid);

	rslt = shutdown_service(
		fault_shutdown_1_b_on,
		fault_shutdown_1_b_off,
		&primary_nodeid,
		(int)sizeof (primary_nodeid),
		"ha_sample_server",
		e);
	if (e.exception()) {
		e.exception()->print_exception("shutdown_service:");
		os::printf("+++ FAIL: Exception during shutdown was not "
		    "expected\n");
		return (1);
	}

	os::printf(" ** Verifying that service was shutdown.\n");

	// Verify that the state of the service doesn't exist
	if (_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Service \"ha_sample_server\" still "
		    "exists, and is in state: %d\n", _rma_snapshot.get_state());
		return (1);
	}

	if (rslt == 0)
		os::printf("--- PASS: Service was shutdown\n");
	else
		os::printf("+++ FAIL: Shutdown service returned error\n");

	return (rslt);
#else
	return (no_fault_injection());
#endif
}

//
// Shutdown 2-A Test case
//
int
shutdown_2_a(int, char *[])
{
	os::printf("*** Shutdown Scenario 2 Fault A\n");

#ifdef _FAULT_INJECTION
	// No Fault in this case
	rm_snap		_rma_snapshot;
	Environment	e;
	int		rslt;

	if (get_state("ha_sample_server") != replica::S_UP)
		return (1);

	rslt = shutdown_service(NULL, NULL, NULL, 0, "ha_sample_server", e);
	if (e.exception()) {
		e.exception()->print_exception("shutdown_service:");
		os::printf("+++ FAIL: Exception during shutdown was not "
		    "expected\n");
		return (1);
	}

	os::printf(" ** Verifying that service was shutdown.\n");

	// Verify that the state of the service doesn't exist
	if (_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Service \"ha_sample_server\" still "
		    "exists, and is in state: %d\n", _rma_snapshot.get_state());
		return (1);
	}

	if (rslt == 0)
		os::printf("--- PASS: Service was shutdown\n");
	else
		os::printf("+++ FAIL: Shutdown service returned error\n");

	return (rslt);
#else
	return (no_fault_injection());
#endif
}

//
// Shutdown 2-B Test case
//
int
shutdown_2_b(int, char *[])
{
	os::printf("*** Shutdown Scenario 2 Fault B\n");

#ifdef _FAULT_INJECTION
	// Figure out which node we'd like to fault (Primary node)
	rm_snap		_rma_snapshot;
	nodeid_t	primary_nodeid;
	Environment	e;
	int		rslt;

	if (get_state("ha_sample_server") != replica::S_UP)
		return (1);

	primary_nodeid = get_primary("ha_sample_server");
	if (primary_nodeid == 0)
		return (1);

	os::printf("    Primary is node: %d\n", primary_nodeid);

	rslt = shutdown_service(
		fault_shutdown_2_b_on,
		fault_shutdown_2_b_off,
		&primary_nodeid,
		(int)sizeof (primary_nodeid),
		"ha_sample_server",
		e);
	if (e.exception()) {
		e.exception()->print_exception("shutdown_service:");
		os::printf("+++ FAIL: Exception during shutdown was not "
		    "expected\n");
		return (1);
	}

	os::printf(" ** Verifying that service was shutdown.\n");

	// Verify that the state of the service doesn't exist
	if (_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Service \"ha_sample_server\" still "
		    "exists, and is in state: %d\n", _rma_snapshot.get_state());
		return (1);
	}

	if (rslt == 0)
		os::printf("--- PASS: Service was shutdown\n");
	else
		os::printf("+++ FAIL: Shutdown service returned error\n");

	return (rslt);
#else
	return (no_fault_injection());
#endif
}

//
// Shutdown 2-C Test case
//
int
shutdown_2_c(int, char *[])
{
	os::printf("*** Shutdown Scenario 2 Fault C\n");

#ifdef _FAULT_INJECTION
	// Figure out which node we'd like to fault (Secondary node)
	rm_snap		_rma_snapshot;
	nodeid_t	secondary_nodeid;
	Environment	e;
	int		rslt;

	if (get_state("ha_sample_server") != replica::S_UP)
		return (1);

	secondary_nodeid = get_secondary(1, "ha_sample_server");
	if (secondary_nodeid == 0) {
		os::printf("+++ FAIL: Could not get secondary_nodeid\n");
		return (1);
	}

	os::printf("    Secondary is node: %d\n", secondary_nodeid);

	rslt = shutdown_service(
		fault_shutdown_2_c_on,
		fault_shutdown_2_c_off,
		&secondary_nodeid,
		(int)sizeof (secondary_nodeid),
		"ha_sample_server",
		e);
	if (e.exception()) {
		e.exception()->print_exception("shutdown_service:");
		os::printf("+++ FAIL: Exception during shutdown was not "
		    "expected\n");
		return (1);
	}

	os::printf(" ** Verifying that service was shutdown.\n");

	// Verify that the state of the service doesn't exist
	if (_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Service \"ha_sample_server\" still "
		    "exists, and is in state: %d\n", _rma_snapshot.get_state());
		return (1);
	}

	if (rslt == 0)
		os::printf("--- PASS: Service was shutdown\n");
	else
		os::printf("+++ FAIL: Shutdown service returned error\n");

	return (rslt);
#else
	return (no_fault_injection());
#endif
}

//
// Shutdown 2-E Test case
//
int
shutdown_2_e(int, char *[])
{
	os::printf("*** Shutdown Scenario 2 Fault E\n");

#ifdef _FAULT_INJECTION
	// Figure out which node we'd like to fault (Primary node)
	nodeid_t	primary_nodeid;
	Environment	e;
	int		rslt;

	if (get_state("ha_sample_server") != replica::S_UP)
		return (1);

	primary_nodeid = get_primary("ha_sample_server");
	if (primary_nodeid == 0)
		return (1);

	os::printf("    Primary is node: %d\n", primary_nodeid);

	rslt = change_sec_state(
		fault_shutdown_2_e_on,
		fault_shutdown_2_e_off,
		&primary_nodeid,
		(int)sizeof (primary_nodeid),
		"ha_sample_server",
		replica::SC_REMOVE_REPL_PROV,
		e,
		false);

	if (rslt == 0)
		os::printf("--- PASS: Prov was removed\n");

	if (get_state("ha_sample_server") != replica::S_DOWN)
		return (1);

	return (rslt);
#else
	return (no_fault_injection());
#endif
}

//
// Shutdown 3-A Test case
//
int
shutdown_3_a(int, char *[])
{
	os::printf("*** Shutdown Scenario 3 Fault A\n");

#ifdef _FAULT_INJECTION
	// No Fault in this case
	rm_snap		_rma_snapshot;
	Environment	e;
	int		rslt;

	if (get_state("ha_sample_server") != replica::S_UP)
		return (1);

	rslt = shutdown_service(NULL, NULL, NULL, 0, "ha_sample_server", e);
	if (e.exception()) {
		e.exception()->print_exception("shutdown_service:");
		os::printf("+++ FAIL: Exception during shutdown was not "
		    "expected\n");
		return (1);
	}

	os::printf(" ** Verifying that service was shutdown.\n");

	// Verify that the state of the service doesn't exist
	if (_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Service \"ha_sample_server\" still "
		    "exists, and is in state: %d\n", _rma_snapshot.get_state());
		return (1);
	}

	if (rslt == 0)
		os::printf("--- PASS: Service was shutdown\n");
	else
		os::printf("+++ FAIL: Shutdown service returned error\n");

	return (rslt);
#else
	return (no_fault_injection());
#endif
}

//
// Shutdown 3-B Test case
//
int
shutdown_3_b(int, char *[])
{
	os::printf("*** Shutdown Scenario 3 Fault B\n");

#ifdef _FAULT_INJECTION
	// Reuse the fault points from shutdown_2_b

	// Figure out which node we'd like to fault (Primary node)
	rm_snap		_rma_snapshot;
	nodeid_t	primary_nodeid;
	Environment	e;
	int		rslt;

	if (get_state("ha_sample_server") != replica::S_UP)
		return (1);

	primary_nodeid = get_primary("ha_sample_server");
	if (primary_nodeid == 0)
		return (1);

	os::printf("    Primary is node: %d\n", primary_nodeid);

	rslt = shutdown_service(
		fault_shutdown_2_b_on,
		fault_shutdown_2_b_off,
		&primary_nodeid,
		(int)sizeof (primary_nodeid),
		"ha_sample_server",
		e);
	if (e.exception()) {
		e.exception()->print_exception("shutdown_service:");
		os::printf("+++ FAIL: Exception during shutdown was not "
		    "expected\n");
		return (1);
	}

	os::printf(" ** Verifying that service was shutdown.\n");

	// Verify that the state of the service doesn't exist
	if (_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Service \"ha_sample_server\" still "
		    "exists, and is in state: %d\n", _rma_snapshot.get_state());
		return (1);
	}

	if (rslt == 0)
		os::printf("--- PASS: Service was shutdown\n");
	else
		os::printf("+++ FAIL: Shutdown service returned error\n");

	return (rslt);
#else
	return (no_fault_injection());
#endif
}

//
// Shutdown 3-C Test case
//
int
shutdown_3_c(int, char *[])
{
	os::printf("*** Shutdown Scenario 3 Fault C\n");

#ifdef _FAULT_INJECTION
	// Reuse the fault points from shutdown_2_c
	// Figure out which node we'd like to fault (Secondary node)
	rm_snap		_rma_snapshot;
	nodeid_t	secondary_nodeid;
	Environment	e;
	int		rslt;

	if (get_state("ha_sample_server") != replica::S_UP)
		return (1);

	secondary_nodeid = get_secondary(1, "ha_sample_server");
	if (secondary_nodeid == 0) {
		os::printf("+++ FAIL: Could not get secondary_nodeid\n");
		return (1);
	}

	os::printf("    Secondary is node: %d\n", secondary_nodeid);

	rslt = shutdown_service(
		fault_shutdown_2_c_on,
		fault_shutdown_2_c_off,
		&secondary_nodeid,
		(int)sizeof (secondary_nodeid),
		"ha_sample_server",
		e);
	if (e.exception()) {
		e.exception()->print_exception("shutdown_service:");
		os::printf("+++ FAIL: Exception during shutdown was not "
		    "expected\n");
		return (1);
	}

	os::printf(" ** Verifying that service was shutdown.\n");

	// Verify that the state of the service doesn't exist
	if (_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Service \"ha_sample_server\" still "
		    "exists, and is in state: %d\n", _rma_snapshot.get_state());
		return (1);
	}

	if (rslt == 0)
		os::printf("--- PASS: Service was shutdown\n");
	else
		os::printf("+++ FAIL: Shutdown service returned error\n");

	return (rslt);
#else
	return (no_fault_injection());
#endif
}

//
// Shutdown 3-D Test case
//
int
shutdown_3_d(int, char *[])
{
	os::printf("*** Shutdown Scenario 3 Fault D\n");

#ifdef _FAULT_INJECTION
	// Reuse the fault points from shutdown_2_c except we set the nodeid to
	// 0

	// Figure out which node we'd like to fault (Secondary node)
	rm_snap		_rma_snapshot;
	nodeid_t	nodeid;
	Environment	e;
	int		rslt;

	if (get_state("ha_sample_server") != replica::S_UP)
		return (1);

	nodeid = 0;

	rslt = shutdown_service(
		fault_shutdown_2_c_on,
		fault_shutdown_2_c_off,
		&nodeid,
		(int)sizeof (nodeid),
		"ha_sample_server",
		e);
	if (e.exception()) {
		e.exception()->print_exception("shutdown_service:");
		os::printf("+++ FAIL: Exception during shutdown was not "
		    "expected\n");
		return (1);
	}

	os::printf(" ** Verifying that service was shutdown.\n");

	// Verify that the state of the service doesn't exist
	if (_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Service \"ha_sample_server\" still "
		    "exists, and is in state: %d\n", _rma_snapshot.get_state());
		return (1);
	}

	if (rslt == 0)
		os::printf("--- PASS: Service was shutdown\n");
	else
		os::printf("+++ FAIL: Shutdown service returned error\n");

	return (rslt);
#else
	return (no_fault_injection());
#endif
}

//
// Shutdown 3-E Test case
//
int
shutdown_3_e(int, char *[])
{
	os::printf("*** Shutdown Scenario 3 Fault E\n");

#ifdef _FAULT_INJECTION
	// Reuse the fault points from shutdown_2_e

	// Figure out which node we'd like to fault (Primary node)
	nodeid_t	primary_nodeid;
	Environment	e;
	int		rslt;

	if (get_state("ha_sample_server") != replica::S_UP)
		return (1);

	primary_nodeid = get_primary("ha_sample_server");
	if (primary_nodeid == 0)
		return (1);

	os::printf("    Primary is node: %d\n", primary_nodeid);

	rslt = change_sec_state(
		fault_shutdown_2_e_on,
		fault_shutdown_2_e_off,
		&primary_nodeid,
		(int)sizeof (primary_nodeid),
		"ha_sample_server",
		replica::SC_REMOVE_REPL_PROV,
		e);

	if (rslt == 0)
		os::printf("--- PASS: Prov was removed\n");

	if (get_state("ha_sample_server") != replica::S_UP)
		return (1);

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// Shutdown 4-A Test case
//
int
shutdown_4_a(int, char *[])
{
	os::printf("*** Shutdown Scenario 4 Fault A\n");

#ifdef _FAULT_INJECTION

#ifdef _KERNEL_ORB
	//
	// This is a weird test
	// We will verify that the code left to clean up HA objects whose
	// unreference we are left waiting for, clean up correcltly.
	//
	// First we will do it without faults, then with a fault on the primary
	//
	// A prov can return succesfully from shutdown even if there are
	// outstanding references to its HA objects, if it knows that unref
	// to these objects will eventually come.
	// The rma will reap its data structures at a later time when it
	// finally receives the unreferenced callbacks for these HA objects
	//
	Environment		e;
	int			rslt;
	replica::service_num_t	sid;

	// First make sure that the service is up
	if (get_state("ha_sample_server") != replica::S_UP)
		return (1);

	// Get the service id
	if ((sid = get_sid("ha_sample_server")) == 0)
		return (1);

	// Next get a HA object from that service
	repl_sample::value_obj_ptr obj_ref = init_value_obj(
	    "ha_sample_server", 10);

	// Now shutdown that service
	rslt = shutdown_service(
		fault_shutdown_4_a_on,
		fault_shutdown_4_a_off,
		NULL, 0,
		"ha_sample_server",
		e);
	if (e.exception()) {
		e.exception()->print_exception("shutdown_service:");
		os::printf("+++ FAIL: Exception during shutdown was not "
		    "expected\n");
		return (1);
	}
	if (rslt) {
		os::printf("+++ FAIL: Error value: %d returned from shutdown\n",
		    rslt);
		return (1);
	}


	os::printf("    Verifying that RMA still has allocation for sid: %d\n",
	    sid);

	hxdoor_service *the_service;
	the_service = rma::the().lookup_service(sid);
	if (!the_service) {
		os::printf("+++ FAIL: RMA released service (%d) prior to all "
		    "unref's being delivered\n", sid);
		return (1);
	}
	the_service->unlock();

	os::printf(" ** Releasing HA object reference... will sleep waiting"
	    " for unref.\n");
	// Release the last HA object we hold
	CORBA::release(obj_ref);

	uint32_t	count = 1;

	// Cleanup thread wake up every 5 minutes
	uint32_t	max_count = 36;

	while (count <= max_count) {

	    // Sleep for some time waiting for unref to be processed.
	    os::usecsleep((os::usec_t)(10 * 1000000));

	    // Verify that the rma has released its allocation for
	    // this service
	    os::printf("    Verifying that RMA has released its allocation "
		"for sid: %d after %d secs\n", sid, count*10);
	    the_service = rma::the().lookup_service(sid);
	    if (the_service) {
		if (count == max_count) {
		    os::printf("+++ FAIL: RMA did not release service (%d) "
			"after final unref was delivered for %d secs\n",
			sid, max_count);
		    the_service->unlock();
		    return (1);
		}
		// Do nothing if count < max_count
		the_service->unlock();
	    } else {
		// RMA has released its allocation for the service
		// So assign "count" to "max_count" and get out
		count = max_count;
	    }

	    count++;
	}


	os::printf("--- PASS: RMA cleanup code was succesful\n");
	return (0);
#else
	os::printf("+++ FAIL: Test not supported from user-space\n");
	return (1);

#endif // _KERNEL_ORB

#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Shutdown 4-B Test case
//
int
shutdown_4_b(int, char *[])
{
	os::printf("*** Shutdown Scenario 4 Fault B\n");

#ifdef _FAULT_INJECTION

#ifdef _KERNEL_ORB
	//
	// This is a weird test
	// We will verify that the code left to clean up HA objects whose
	// unreference we are left waiting for, clean up correcltly.
	//
	// First we will do it without faults, then with a fault on the primary
	// (this is the case where we fault the primary after shutting the
	// service down)
	//
	// A prov can return succesfully from shutdown even if there are
	// outstanding references to its HA objects, if it knows that unref
	// to these objects will eventually come.
	// The rma will reap its data structures at a later time when it
	// finally receives the unreferenced callbacks for these HA objects
	//
	Environment		e;
	int			rslt;
	replica::service_num_t	sid;
	nodeid_t		primary_nodeid;

	// First make sure that the service is up
	if (get_state("ha_sample_server") != replica::S_UP)
		return (1);

	// Get nodeid of primary
	if ((primary_nodeid = get_primary("ha_sample_server")) == 0)
		return (1);

	// Get the service id
	if ((sid = get_sid("ha_sample_server")) == 0)
		return (1);

	// Next get a HA object from that service
	repl_sample::value_obj_ptr obj_ref = init_value_obj(
	    "ha_sample_server", 10);

	// Now shutdown that service
	rslt = shutdown_service(
		fault_shutdown_4_a_on,
		fault_shutdown_4_a_off,
		NULL, 0,
		"ha_sample_server",
		e);
	if (e.exception()) {
		e.exception()->print_exception("shutdown_service:");
		os::printf("+++ FAIL: Exception during shutdown was not "
		    "expected\n");
		return (1);
	}
	if (rslt) {
		os::printf("+++ FAIL: Error value: %d returned from shutdown\n",
		    rslt);
		return (1);
	}

	// Fail the old primary node
	FaultFunctions::wait_for_arg_t	wait_arg;
	wait_arg.op = FaultFunctions::WAIT_FOR_DEAD;
	wait_arg.nodeid = primary_nodeid;

	os::printf(" ** Killing node: %d\n", primary_nodeid);
	FaultFunctions::reboot(0, &wait_arg, (uint32_t)sizeof (wait_arg));

	os::printf("    Verifying that RMA still has allocation for sid: %d\n",
	    sid);
	hxdoor_service *the_service;
	the_service = rma::the().lookup_service(sid);
	if (!the_service) {
		os::printf("+++ FAIL: RMA released service (%d) prior to all "
		    "unref's being delivered\n", sid);
		return (1);
	}
	the_service->unlock();

	os::printf(" ** Releasing HA object reference... will sleep waiting"
	    " for unref.\n");
	// Release the last HA object we hold
	CORBA::release(obj_ref);

	uint32_t	count = 1;

	// Cleanup thread wake up every 5 minutes
	uint32_t	max_count = 36;

	while (count <= max_count) {

	    // Sleep for some time waiting for unref to be processed.
	    os::usecsleep((os::usec_t)(10 * 1000000));

	    // Verify that the rma has released its allocation for
	    // this service
	    os::printf("    Verifying that RMA has released its allocation "
		"for sid: %d after %d secs\n", sid, count*10);
	    the_service = rma::the().lookup_service(sid);
	    if (the_service) {
		if (count == max_count) {
		    os::printf("+++ FAIL: RMA did not release service (%d) "
			"after final unref was delivered for %d secs\n",
			sid, max_count);
		    the_service->unlock();
		    return (1);
		}
		// Do nothing if count < max_count
		the_service->unlock();
	    } else {
		// RMA has released its allocation for the service
		// So assign "count" to "max_count" and get out
		count = max_count;
	    }

	    count++;
	}


	os::printf("--- PASS: RMA cleanup code was succesful\n");
	return (0);
#else
	os::printf("+++ FAIL: Test not supported from user-space\n");
	return (1);

#endif // _KERNEL_ORB

#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

//
// Tests normal forced shutdown without any faults.
//
int
shutdown_forced_1(int, char *[])
{
	os::printf("*** Forced Shutdown Scenario 1\n");

#ifdef _FAULT_INJECTION

#ifdef _KERNEL_ORB
	Environment	e;
	int		ret = 0;

	// Hold a reference to an HA object in the service
	repl_sample::value_obj_ptr obj_ref_p = init_value_obj(
					"ha_sample_server", 0);
	if (CORBA::is_nil(obj_ref_p)) {
		os::printf("+++ FAIL: Could not get reference to "
		    "repl_sample::value_obj\n");
		return (1);
	}

	(void) shutdown_service(NULL, NULL, NULL, 0, "ha_sample_server",
	    e, true);
	if (e.exception()) {
		e.exception()->print_exception("shutdown_service:");
		os::printf("+++ FAIL: Exception during shutdown was not "
		    "expected\n");
		e.clear();
		ret = 1;
	}

	int16_t	nodenum = 0;

	//
	// Attempt to make invocation on the service that has just been
	// shutdown. This should fail with COMM_FAILURE
	//
	(void) obj_ref_p->get_value(nodenum, e);
	if (e.exception()) {
		e.exception()->print_exception(
		    "repl_sample::value_obj::get_value()");
		if (!CORBA::COMM_FAILURE::_exnarrow(e.exception())) {
			os::printf("+++ FAIL: Unexpected exception is returned."
			    " This is not expected after service shutdown.\n");
			ret = 1;
		}
		e.clear();
	} else {
		os::printf("+++ FAIL: No exception is returned. "
		    "This is not expected after service shutdown.\n");
		ret = 1;
	}

	//
	// now release the last reference to allow the service to be fully
	// removed.
	//
	CORBA::release(obj_ref_p);

	if (ret == 0) {
#if defined(_KERNEL_ORB) && !defined(_KERNEL)
		//
		// give periodic cleanup thread 15 seconds to cleanup.
		// We forced the periodic cleanup thread to wakes up every
		// 10 seconds in unode mode.
		//
		os::printf("sleep 15 seconds\n");
		os::usecsleep((os::usec_t)15000000);
#endif
		os::printf("--- PASS: service shutdown code was succesful\n");
	}
	return (ret);
#else
	os::printf("+++ FAIL: Test not supported from user-space\n");
	return (1);

#endif // _KERNEL_ORB

#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

// Service primary failure while forced_shutdown is in progress.
int
shutdown_forced_2(int, char *[])
{
	os::printf("*** Forced Shutdown Scenario 2\n");

#ifdef _FAULT_INJECTION

#ifdef _KERNEL_ORB
	Environment	e;
	int		ret = 0;
	repl_sample::value_obj_ptr obj_ref_p = init_value_obj(
					"ha_sample_server", 0);
	if (CORBA::is_nil(obj_ref_p)) {
		os::printf("+++ FAIL: Could not get reference to "
		    "repl_sample::value_obj\n");
		return (1);
	}

	nodeid_t	primary_nodeid = get_primary("ha_sample_server");
	if (primary_nodeid == 0) {
		os::printf("+++ FAIL: Could not find primary node id\n");
		return (1);
	}

	os::printf("    Primary is node: %d\n", primary_nodeid);
	(void) shutdown_service(
			fault_shutdown_1_b_on,
			fault_shutdown_1_b_off,
			&primary_nodeid,
			(int)sizeof (primary_nodeid),
			"ha_sample_server",
			e, true);
	if (e.exception()) {
		e.exception()->print_exception("shutdown_service:");
		os::printf("+++ FAIL: Exception during shutdown was not "
		    "expected\n");
		e.clear();
		ret = 1;
	}

	int16_t	nodenum = 0;

	os::printf("call get_value\n");
	(void) obj_ref_p->get_value(nodenum, e);
	if (e.exception()) {
		e.exception()->print_exception(
		    "repl_sample::value_obj::get_value()");
		if (!CORBA::COMM_FAILURE::_exnarrow(e.exception())) {
			os::printf("+++ FAIL: Unexpected exception is returned."
			    " This is not expected after service shutdown.\n");
			ret = 1;
		}
		e.clear();
	} else {
		os::printf("+++ FAIL: No exception is returned. "
		    "This is not expected after service shutdown.\n");
		ret = 1;
	}

	CORBA::release(obj_ref_p);

	if (ret == 0) {
#if defined(_KERNEL_ORB) && !defined(_KERNEL)
		//
		// give periodic cleanup thread 15 seconds to cleanup.
		// We forced the periodic cleanup thread to wakes up every
		// 10 seconds.
		//
		os::printf("sleep 15 seconds\n");
		os::usecsleep((os::usec_t)15000000);
#endif
		os::printf("--- PASS: service shutdown code was succesful\n");
	}

	return (ret);
#else
	os::printf("+++ FAIL: Test not supported from user-space\n");
	return (1);

#endif // _KERNEL_ORB

#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

// Service secondary failure while forced shutdown is in progress
int
shutdown_forced_3(int, char *[])
{
	os::printf("*** Forced Shutdown Scenario 4\n");

#ifdef _FAULT_INJECTION

#ifdef _KERNEL_ORB
	Environment	e;
	int	ret = 0;
	repl_sample::value_obj_ptr obj_ref_p = init_value_obj(
					"ha_sample_server", 0);
	if (CORBA::is_nil(obj_ref_p)) {
		os::printf("+++ FAIL: Could not get reference to "
		    "repl_sample::value_obj\n");
		return (1);
	}

	nodeid_t secondary_nodeid = get_secondary(1, "ha_sample_server");
	if (secondary_nodeid == 0) {
		os::printf("+++ FAIL: Could not get secondary_nodeid\n");
		return (1);
	}
	os::printf("    Secondary is node: %d\n", secondary_nodeid);

	(void) shutdown_service(
			fault_shutdown_1_b_on,
			fault_shutdown_1_b_off,
			&secondary_nodeid,
			(int)sizeof (secondary_nodeid),
			"ha_sample_server",
			e, true);
	if (e.exception()) {
		e.exception()->print_exception("shutdown_service:");
		os::printf("+++ FAIL: Exception during shutdown was not "
		    "expected\n");
		e.clear();
		ret = 1;
	}

	int16_t	nodenum = 0;

	os::printf("call get_value\n");
	(void) obj_ref_p->get_value(nodenum, e);
	if (e.exception()) {
		e.exception()->print_exception(
		    "repl_sample::value_obj::get_value()");
		if (!CORBA::COMM_FAILURE::_exnarrow(e.exception())) {
			os::printf("+++ FAIL: Unexpected exception is returned."
			    " This is not expected after service shutdown.\n");
			ret = 1;
		}
		e.clear();
	} else {
		os::printf("+++ FAIL: No exception is returned. "
		    "This is not expected after service shutdown.\n");
		ret = 1;
	}

	CORBA::release(obj_ref_p);

	if (ret == 0) {
#if defined(_KERNEL_ORB) && !defined(_KERNEL)
		//
		// give periodic cleanup thread 15 seconds to cleanup.
		// We forced the periodic cleanup thread to wakes up every
		// 10 seconds.
		//
		os::printf("sleep 15 seconds\n");
		os::usecsleep((os::usec_t)15000000);
#endif
		os::printf("--- PASS: service shutdown code was succesful\n");
	}
	return (ret);
#else
	os::printf("+++ FAIL: Test not supported from user-space\n");
	return (1);

#endif // _KERNEL_ORB

#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

// RM primary failure while forced shutdown is in progress
int
shutdown_forced_4(int, char *[])
{
	os::printf("*** Forced Shutdown Scenario 3\n");

#ifdef _FAULT_INJECTION

#ifdef _KERNEL_ORB
	Environment	e;
	int	ret = 0;
	repl_sample::value_obj_ptr obj_ref_p = init_value_obj(
					"ha_sample_server", 0);
	if (CORBA::is_nil(obj_ref_p)) {
		os::printf("+++ FAIL: Could not get reference to "
		    "repl_sample::value_obj\n");
		return (1);
	}

	FaultFunctions::ha_rm_reboot_arg_t reboot_arg;
	reboot_arg.nodeid = FaultFunctions::rm_primary_node();
	reboot_arg.op = FaultFunctions::WAIT_FOR_UNKNOWN;
	(void) os::strcpy(reboot_arg.fn_name, "shutdown_service");
	(void) shutdown_service(
			fault_ha_rm_shutdown_service_3_on,
			fault_ha_rm_shutdown_service_3_off,
			&reboot_arg,
			(int)sizeof (reboot_arg),
			"ha_sample_server",
			e, true);
	if (e.exception()) {
		e.exception()->print_exception("shutdown_service:");
		os::printf("+++ FAIL: Exception during shutdown was not "
		    "expected\n");
		e.clear();
		ret = 1;
	}

	int16_t	nodenum = 0;

	os::printf("call get_value\n");
	(void) obj_ref_p->get_value(nodenum, e);
	if (e.exception()) {
		e.exception()->print_exception(
		    "repl_sample::value_obj::get_value()");
		if (!CORBA::INV_OBJREF::_exnarrow(e.exception())) {
			os::printf("+++ FAIL: Unexpected exception is returned."
			    " This is not expected after service shutdown.\n");
			ret = 1;
		}
		e.clear();
	} else {
		os::printf("+++ FAIL: No exception is returned. "
		    "This is not expected after service shutdown.\n");
		ret = 1;
	}

	CORBA::release(obj_ref_p);

	if (ret == 0) {
#if defined(_KERNEL_ORB) && !defined(_KERNEL)
		//
		// give periodic cleanup thread 15 seconds to cleanup.
		// We forced the periodic cleanup thread to wakes up every
		// 10 seconds.
		//
		os::printf("sleep 15 seconds\n");
		os::usecsleep((os::usec_t)15000000);
#endif
		os::printf("--- PASS: service shutdown code was succesful\n");
	}
	return (ret);
#else
	os::printf("+++ FAIL: Test not supported from user-space\n");
	return (1);

#endif // _KERNEL_ORB

#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}
