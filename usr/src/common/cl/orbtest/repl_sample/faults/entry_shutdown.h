/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ENTRY_SHUTDOWN_H
#define	_ENTRY_SHUTDOWN_H

#pragma ident	"@(#)entry_shutdown.h	1.12	08/05/20 SMI"

#include <orbtest/repl_sample/faults/entry_common.h>

int shutdown_1_a(int, char *[]);
int shutdown_1_b(int, char *[]);

int shutdown_2_a(int, char *[]);
int shutdown_2_b(int, char *[]);
int shutdown_2_c(int, char *[]);
int shutdown_2_e(int, char *[]);

int shutdown_3_a(int, char *[]);
int shutdown_3_b(int, char *[]);
int shutdown_3_c(int, char *[]);
int shutdown_3_d(int, char *[]);
int shutdown_3_e(int, char *[]);

int shutdown_4_a(int, char *[]);
int shutdown_4_b(int, char *[]);

int shutdown_forced_1(int, char *[]);
int shutdown_forced_2(int, char *[]);
int shutdown_forced_3(int, char *[]);
int shutdown_forced_4(int, char *[]);

#endif	/* _ENTRY_SHUTDOWN_H */
