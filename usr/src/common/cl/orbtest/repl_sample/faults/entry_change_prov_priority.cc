/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)entry_change_prov_priority.cc	1.9	08/05/20 SMI"

#include	<h/replica.h>
#include	<orb/fault/fault_injection.h>
#include	<orbtest/repl_sample/repl_sample_clnt.h>
#include	<sys/os.h>
#include	<orb/invo/common.h>
#include	<orbtest/rm_probe/rm_snap.h>

#include	<orbtest/repl_sample/faults/entry_common.h>
#include	<orbtest/repl_sample/faults/entry_change_prov_priority.h>
#include	<orbtest/repl_sample/faults/faults_on_specific_service.h>

//
// Usage: functname <svc_name> <num_nodes> <pri_1> <pri_2> ... [fault_node]
//	where <svc_name> = name of the service
//	<num_nodes> = number of nodes in the cluster
//	<pri_1..n> = the corresponding priority of the node with such nodeid
//	[fault_node] = node specified to be faulted (optional)
//

void
print_change_prov_priority_usage(const char *functname)
{
	os::printf("Incorrect usage:\t%s\n", functname);
}

int
change_prov_priority_no_fault(int argc, char **argv)
{
	os::printf("*** Changing provider priority without fault ***\n");

#ifdef _FAULT_INJECTION

	int		i, rslt;
	int 		size_prov_desc = 5;
	Environment 	e;

	rslt = 1;

	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc < 3) {
		print_change_prov_priority_usage(argv[0]);
		return (rslt);
	}

	char *svc_name = new char[os::strlen(argv[1])+1];
	os::sprintf(svc_name, "%s", argv[1]);
	os::printf("service %s\n", svc_name);

	uint32_t num_nodes = (uint32_t)os::atoi(argv[2]);
	os::printf("num_nodes is %d\n", num_nodes);

	// If fault_node is specified, this routine will just ignore it
	// since it is not applicable.
	if (((uint32_t)argc < (num_nodes+3)) ||
		((uint32_t)argc > (num_nodes+5))) {
		print_change_prov_priority_usage(argv[0]);
		return (rslt);
	}

	// Verify that service is up
	if (get_state(svc_name) != replica::S_UP) {
		os::printf("+++ FAIL: service is not in S_UP state\n");
		return (1);
	}

	// Allocate an array of char pointer to point to prov description
	char **prov_desc = new char *[num_nodes]; //lint !e671

	replica::prov_priority_list prov_list(num_nodes, num_nodes);

	for (i = 0; i < (int)num_nodes; i++) {

		// Allocate enough memory to hold all the nodeid with
		// with double digit (support up to 64 nodes)
		*(prov_desc+i) = new char[size_prov_desc]; //lint !e737
		os::sprintf(*(prov_desc+i), "%d", i+1);
		os::printf("provider %s\n", *(prov_desc+i));
		//lint -e732
		prov_list[i].prov_desc = (const char *)(*(prov_desc+i));
		prov_list[i].priority = (uint32_t)os::atoi(argv[i+3]);
		//lint +e732
	}

	// No Fault
	rslt = change_repl_prov_priority(
		NULL, NULL, NULL, 0, svc_name, &prov_list, e);

	delete [] svc_name;

	for (i = 0; i < (int)num_nodes; i++) {
		delete [] (*(prov_desc+i));
	}
	delete [] prov_desc;

	return (rslt);

#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

int
change_prov_priority_fault_a(int argc, char **argv)
{
	os::printf("*** Changing provider priority with fault ***\n");

#ifdef _FAULT_INJECTION

	int		i, rslt;
	nodeid_t	fault_node;
	int		size_prov_desc = 5;
	Environment 	e;

	rslt = 1;

	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc < 3) {
		print_change_prov_priority_usage(argv[0]);
		return (rslt);
	}

	char *svc_name = new char[os::strlen(argv[1])+1];
	os::sprintf(svc_name, "%s", argv[1]);

	uint32_t num_nodes = (uint32_t)os::atoi(argv[2]);

	if (((uint32_t)argc < (num_nodes+3)) ||
		((uint32_t)argc > (num_nodes+5))) {
		print_change_prov_priority_usage(argv[0]);
		return (rslt);
	}

	// Verify that service is up
	if (get_state(svc_name) != replica::S_UP) {
		os::printf("+++ FAIL: service is not in S_UP state\n");
		return (1);
	}

	// Allocate an array of char pointer to point to prov description
	char **prov_desc = new char *[num_nodes]; //lint !e671

	replica::prov_priority_list prov_list(num_nodes, num_nodes);

	for (i = 0; i < (int)num_nodes; i++) {

		// Allocate enough memory to hold all the nodeid with
		// with double digit (support up to 64 nodes)
		*(prov_desc+i) = new char[size_prov_desc+1]; //lint !e737
		os::sprintf(*(prov_desc+i), "%d", i+1);
		//lint -e732
		prov_list[i].prov_desc = (const char *)(*(prov_desc+i));
		prov_list[i].priority = (uint32_t)os::atoi(argv[i+3]);
		//lint +e732
	}

	if ((uint32_t)argc == (num_nodes+4)) {
		fault_node = (nodeid_t)os::atoi(argv[(num_nodes+3)]);
	} else {
		if ((fault_node = get_primary(svc_name)) == 0)
			return (1);
	}

	// Fault the specified service if it is supplied; otherwise,
	// use the service for changing desired number of secondaries
	// as default
	char *fault_svc_name;
	if ((uint32_t)argc == (num_nodes+5)) {
		fault_svc_name = new char[os::strlen(argv[(num_nodes+4)])+1];
		os::sprintf(fault_svc_name, "%s", argv[(num_nodes+4)]);
	} else {
		fault_svc_name = svc_name;
	}

	os::printf(" ** Will fault service %s on node %d\n",
		fault_svc_name, fault_node);

	// Populate fault arguments
	FaultFunctions::svc_reboot_arg_t	reboot_args;
	reboot_args.sid	 = get_sid(fault_svc_name);
	reboot_args.op	  = FaultFunctions::WAIT_FOR_UNKNOWN;
	reboot_args.nodeid	= fault_node;

	// With Fault
	rslt = change_repl_prov_priority(
		fault_repl_prov_become_secondary_on,
		fault_repl_prov_become_secondary_off,
		&reboot_args, (int)sizeof (reboot_args),
		svc_name, &prov_list, e);

	delete [] svc_name;
	for (i = 0; i < (int)num_nodes; i++) {
		delete [] (*(prov_desc+i));
	}
	delete [] prov_desc;

	if ((uint32_t)argc == (num_nodes+5)) {
		delete [] fault_svc_name;
	}

	return (rslt);

#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

int
change_prov_priority_fault_b(int argc, char **argv)
{
	os::printf("*** Changing provider priority with fault ***\n");

#ifdef _FAULT_INJECTION

	int		i, rslt;
	nodeid_t	fault_node;
	int		size_prov_desc = 5;
	Environment 	e;

	rslt = 1;

	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc < 3) {
		print_change_prov_priority_usage(argv[0]);
		return (rslt);
	}

	char *svc_name = new char[os::strlen(argv[1])+1];
	os::sprintf(svc_name, "%s", argv[1]);

	uint32_t num_nodes = (uint32_t)os::atoi(argv[2]);

	if (((uint32_t)argc < (num_nodes+3)) ||
		((uint32_t)argc > (num_nodes+5))) {
		print_change_prov_priority_usage(argv[0]);
		return (rslt);
	}

	// Verify that service is up
	if (get_state(svc_name) != replica::S_UP) {
		os::printf("+++ FAIL: service is not in S_UP state\n");
		return (1);
	}

	// Allocate an array of char pointer to point to prov description
	char **prov_desc = new char *[num_nodes]; //lint !e671

	replica::prov_priority_list prov_list(num_nodes, num_nodes);

	for (i = 0; i < (int)num_nodes; i++) {

		// Allocate enough memory to hold all the nodeid with
		// with double digit (support up to 64 nodes)
		*(prov_desc+i) = new char[size_prov_desc+1]; //lint !e737
		os::sprintf(*(prov_desc+i), "%d", i+1);
		//lint -e732
		prov_list[i].prov_desc = (const char *)(*(prov_desc+i));
		prov_list[i].priority = (uint32_t)os::atoi(argv[i+3]);
		//lint +e732
	}

	if ((uint32_t)argc == (num_nodes+4)) {
		fault_node = (nodeid_t)os::atoi(argv[(num_nodes+3)]);
	} else {
		if ((fault_node = get_primary(svc_name)) == 0)
			return (1);
	}

	// Fault the specified service if it is supplied; otherwise,
	// use the service for changing desired number of secondaries
	// as default
	char *fault_svc_name;
	if ((uint32_t)argc == (num_nodes+5)) {
		fault_svc_name = new char[os::strlen(argv[(num_nodes+4)])+1];
		os::sprintf(fault_svc_name, "%s", argv[(num_nodes+4)]);
	} else {
		fault_svc_name = svc_name;
	}

	os::printf(" ** Will fault service %s on node %d\n",
		fault_svc_name, fault_node);

	// Populate fault arguments
	FaultFunctions::svc_reboot_arg_t	reboot_args;
	reboot_args.sid	= get_sid(fault_svc_name);
	reboot_args.op	= FaultFunctions::WAIT_FOR_UNKNOWN;
	reboot_args.nodeid	= fault_node;

	// With Fault
	rslt = change_repl_prov_priority(
		fault_repl_prov_add_secondary_on,
		fault_repl_prov_add_secondary_off,
		&reboot_args, (int)sizeof (reboot_args),
		svc_name, &prov_list, e);

	delete [] svc_name;
	for (i = 0; i < (int)num_nodes; i++) {
		delete [] (*(prov_desc+i));
	}
	delete [] prov_desc;

	if ((uint32_t)argc == (num_nodes+5)) {
		delete [] fault_svc_name;
	}

	return (rslt);

#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

int
change_prov_priority_fault_c(int argc, char **argv)
{
	os::printf("*** Changing provider priority with fault ***\n");

#ifdef _FAULT_INJECTION

	int		i, rslt;
	nodeid_t	fault_node;
	int		size_prov_desc = 5;
	Environment 	e;

	rslt = 1;

	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc < 3) {
		print_change_prov_priority_usage(argv[0]);
		return (rslt);
	}

	char *svc_name = new char[os::strlen(argv[1])+1];
	os::sprintf(svc_name, "%s", argv[1]);

	uint32_t num_nodes = (uint32_t)os::atoi(argv[2]);

	if (((uint32_t)argc < (num_nodes+3)) ||
		((uint32_t)argc > (num_nodes+5))) {
		print_change_prov_priority_usage(argv[0]);
		return (rslt);
	}

	// Verify that service is up
	if (get_state(svc_name) != replica::S_UP) {
		os::printf("+++ FAIL: service is not in S_UP state\n");
		return (1);
	}

	// Allocate an array of char pointer to point to prov description
	char **prov_desc = new char *[num_nodes]; //lint !e671

	replica::prov_priority_list prov_list(num_nodes, num_nodes);

	for (i = 0; i < (int)num_nodes; i++) {

		// Allocate enough memory to hold all the nodeid with
		// with double digit (support up to 64 nodes)
		*(prov_desc+i) = new char[size_prov_desc+1]; //lint !e737
		os::sprintf(*(prov_desc+i), "%d", i+1);
		//lint -e732
		prov_list[i].prov_desc = (const char *)(*(prov_desc+i));
		prov_list[i].priority = (uint32_t)os::atoi(argv[i+3]);
		//lint +e732
	}

	if ((uint32_t)argc == (num_nodes+4)) {
		fault_node = (nodeid_t)os::atoi(argv[(num_nodes+3)]);
	} else {
		if ((fault_node = get_primary(svc_name)) == 0)
			return (1);
	}

	// Fault the specified service if it is supplied; otherwise,
	// use the service for changing desired number of secondaries
	// as default
	char *fault_svc_name;
	if ((uint32_t)argc == (num_nodes+5)) {
		fault_svc_name = new char[os::strlen(argv[(num_nodes+4)])+1];
		os::sprintf(fault_svc_name, "%s", argv[(num_nodes+4)]);
	} else {
		fault_svc_name = svc_name;
	}

	os::printf(" ** Will fault service %s on node %d\n",
		fault_svc_name, fault_node);

	// Populate fault arguments
	FaultFunctions::svc_reboot_arg_t	reboot_args;
	reboot_args.sid	 = get_sid(fault_svc_name);
	reboot_args.op	  = FaultFunctions::WAIT_FOR_UNKNOWN;
	reboot_args.nodeid	= fault_node;

	// With Fault
	rslt = change_repl_prov_priority(
		fault_repl_prov_commit_secondary_on,
		fault_repl_prov_commit_secondary_off,
		&reboot_args, (int)sizeof (reboot_args),
		svc_name, &prov_list, e);

	delete [] svc_name;
	for (i = 0; i < (int)num_nodes; i++) {
		delete [] (*(prov_desc+i));
	}
	delete [] prov_desc;

	if ((uint32_t)argc == (num_nodes+5)) {
		delete [] fault_svc_name;
	}

	return (rslt);

#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

int
change_prov_priority_fault_d(int argc, char **argv)
{
	os::printf("*** Changing provider priority with fault ***\n");

#ifdef _FAULT_INJECTION

	int		i, rslt;
	nodeid_t	fault_node;
	int		size_prov_desc = 5;
	Environment 	e;

	rslt = 1;

	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc < 3) {
		print_change_prov_priority_usage(argv[0]);
		return (rslt);
	}

	char *svc_name = new char[os::strlen(argv[1])+1];
	os::sprintf(svc_name, "%s", argv[1]);

	uint32_t num_nodes = (uint32_t)os::atoi(argv[2]);

	if (((uint32_t)argc < (num_nodes+3)) ||
		((uint32_t)argc > (num_nodes+5))) {
		print_change_prov_priority_usage(argv[0]);
		return (rslt);
	}

	// Verify that service is up
	if (get_state(svc_name) != replica::S_UP) {
		os::printf("+++ FAIL: service is not in S_UP state\n");
		return (1);
	}

	// Allocate an array of char pointer to point to prov description
	char **prov_desc = new char *[num_nodes]; //lint !e671

	replica::prov_priority_list prov_list(num_nodes, num_nodes);

	for (i = 0; i < (int)num_nodes; i++) {

		// Allocate enough memory to hold all the nodeid with
		// with double digit (support up to 64 nodes)
		*(prov_desc+i) = new char[size_prov_desc+1]; //lint !e737
		os::sprintf(*(prov_desc+i), "%d", i+1);
		//lint -e732
		prov_list[i].prov_desc = (const char *)(*(prov_desc+i));
		prov_list[i].priority = (uint32_t)os::atoi(argv[i+3]);
		//lint +e732
	}

	if ((uint32_t)argc == (num_nodes+4)) {
		fault_node = (nodeid_t)os::atoi(argv[(num_nodes+3)]);
	} else {
		if ((fault_node = get_primary(svc_name)) == 0)
			return (1);
	}

	// Fault the specified service if it is supplied; otherwise,
	// use the service for changing desired number of secondaries
	// as default
	char *fault_svc_name;
	if ((uint32_t)argc == (num_nodes+5)) {
		fault_svc_name = new char[os::strlen(argv[(num_nodes+4)])+1];
		os::sprintf(fault_svc_name, "%s", argv[(num_nodes+4)]);
	} else {
		fault_svc_name = svc_name;
	}

	os::printf(" ** Will fault service %s on node %d\n",
		fault_svc_name, fault_node);

	// Populate fault arguments
	FaultFunctions::svc_reboot_arg_t	reboot_args;
	reboot_args.sid	 = get_sid(fault_svc_name);
	reboot_args.op	  = FaultFunctions::WAIT_FOR_UNKNOWN;
	reboot_args.nodeid	= fault_node;

	// With Fault
	rslt = change_repl_prov_priority(
		fault_repl_prov_remove_secondary_on,
		fault_repl_prov_remove_secondary_off,
		&reboot_args, (int)sizeof (reboot_args),
		svc_name, &prov_list, e);

	delete [] svc_name;
	for (i = 0; i < (int)num_nodes; i++) {
		delete [] (*(prov_desc+i));
	}
	delete [] prov_desc;

	if ((uint32_t)argc == (num_nodes+5)) {
		delete [] fault_svc_name;
	}

	return (rslt);

#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

int
change_prov_priority_fault_e(int argc, char **argv)
{
	os::printf("*** Changing provider priority with fault ***\n");

#ifdef _FAULT_INJECTION

	int		i, rslt;
	nodeid_t	fault_node;
	int		size_prov_desc = 5;
	Environment 	e;

	rslt = 1;

	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc < 3) {
		print_change_prov_priority_usage(argv[0]);
		return (rslt);
	}

	char *svc_name = new char[os::strlen(argv[1])+1];
	os::sprintf(svc_name, "%s", argv[1]);

	uint32_t num_nodes = (uint32_t)os::atoi(argv[2]);

	if (((uint32_t)argc < (num_nodes+3)) ||
		((uint32_t)argc > (num_nodes+5))) {
		print_change_prov_priority_usage(argv[0]);
		return (rslt);
	}

	// Verify that service is up
	if (get_state(svc_name) != replica::S_UP) {
		os::printf("+++ FAIL: service is not in S_UP state\n");
		return (1);
	}

	// Allocate an array of char pointer to point to prov description
	char **prov_desc = new char *[num_nodes]; //lint !e671

	replica::prov_priority_list prov_list(num_nodes, num_nodes);

	for (i = 0; i < (int)num_nodes; i++) {

		// Allocate enough memory to hold all the nodeid with
		// with double digit (support up to 64 nodes)
		*(prov_desc+i) = new char[size_prov_desc+1]; //lint !e737
		os::sprintf(*(prov_desc+i), "%d", i+1);
		//lint -e732
		prov_list[i].prov_desc = (const char *)(*(prov_desc+i));
		prov_list[i].priority = (uint32_t)os::atoi(argv[i+3]);
		//lint +e732
	}

	if ((uint32_t)argc == (num_nodes+4)) {
		fault_node = (nodeid_t)os::atoi(argv[(num_nodes+3)]);
	} else {
		if ((fault_node = get_primary(svc_name)) == 0)
			return (1);
	}

	// Fault the specified service if it is supplied; otherwise,
	// use the service for changing desired number of secondaries
	// as default
	char *fault_svc_name;
	if ((uint32_t)argc == (num_nodes+5)) {
		fault_svc_name = new char[os::strlen(argv[(num_nodes+4)])+1];
		os::sprintf(fault_svc_name, "%s", argv[(num_nodes+4)]);
	} else {
		fault_svc_name = svc_name;
	}

	os::printf(" ** Will fault service %s on node %d\n",
		fault_svc_name, fault_node);

	// Populate fault arguments
	FaultFunctions::svc_reboot_arg_t	reboot_args;
	reboot_args.sid	 = get_sid(fault_svc_name);
	reboot_args.op	  = FaultFunctions::WAIT_FOR_UNKNOWN;
	reboot_args.nodeid	= fault_node;

	// With Fault
	rslt = change_repl_prov_priority(
		fault_repl_prov_become_primary_on,
		fault_repl_prov_become_primary_off,
		&reboot_args, (int)sizeof (reboot_args),
		svc_name, &prov_list, e);

	delete [] svc_name;
	for (i = 0; i < (int)num_nodes; i++) {
		delete [] (*(prov_desc+i));
	}
	delete [] prov_desc;

	if ((uint32_t)argc == (num_nodes+5)) {
		delete [] fault_svc_name;
	}

	return (rslt);

#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

int
change_prov_priority_fault_f(int argc, char **argv)
{
	os::printf("*** Changing provider priority with fault ***\n");

#ifdef _FAULT_INJECTION

	int		i, rslt;
	nodeid_t	fault_node;
	int		size_prov_desc = 5;
	Environment 	e;

	rslt = 1;

	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc < 3) {
		print_change_prov_priority_usage(argv[0]);
		return (rslt);
	}

	char *svc_name = new char[os::strlen(argv[1])+1];
	os::sprintf(svc_name, "%s", argv[1]);

	uint32_t num_nodes = (uint32_t)os::atoi(argv[2]);

	if (((uint32_t)argc < (num_nodes+3)) ||
		((uint32_t)argc > (num_nodes+5))) {
		print_change_prov_priority_usage(argv[0]);
		return (rslt);
	}

	// Verify that service is up
	if (get_state(svc_name) != replica::S_UP) {
		os::printf("+++ FAIL: service is not in S_UP state\n");
		return (1);
	}

	// Allocate an array of char pointer to point to prov description
	char **prov_desc = new char *[num_nodes]; //lint !e671

	replica::prov_priority_list prov_list(num_nodes, num_nodes);

	for (i = 0; i < (int)num_nodes; i++) {

		// Allocate enough memory to hold all the nodeid with
		// with double digit (support up to 64 nodes)
		*(prov_desc+i) = new char[size_prov_desc+1]; //lint !e737
		os::sprintf(*(prov_desc+i), "%d", i+1);
		//lint -e732
		prov_list[i].prov_desc = (const char *)(*(prov_desc+i));
		prov_list[i].priority = (uint32_t)os::atoi(argv[i+3]);
		//lint +e732
	}

	if ((uint32_t)argc == (num_nodes+4)) {
		fault_node = (nodeid_t)os::atoi(argv[(num_nodes+3)]);
	} else {
		if ((fault_node = get_primary(svc_name)) == 0)
			return (1);
	}

	// Fault the specified service if it is supplied; otherwise,
	// use the service for changing desired number of secondaries
	// as default
	char *fault_svc_name;
	if ((uint32_t)argc == (num_nodes+5)) {
		fault_svc_name = new char[os::strlen(argv[(num_nodes+4)])+1];
		os::sprintf(fault_svc_name, "%s", argv[(num_nodes+4)]);
	} else {
		fault_svc_name = svc_name;
	}

	os::printf(" ** Will fault service %s on node %d\n",
		fault_svc_name, fault_node);

	// Populate fault arguments
	FaultFunctions::svc_reboot_arg_t	reboot_args;
	reboot_args.sid	 = get_sid(fault_svc_name);
	reboot_args.op	  = FaultFunctions::WAIT_FOR_UNKNOWN;
	reboot_args.nodeid	= fault_node;

	// With Fault
	rslt = change_repl_prov_priority(
		fault_repl_prov_become_spare_on,
		fault_repl_prov_become_spare_off,
		&reboot_args, (int)sizeof (reboot_args),
		svc_name, &prov_list, e);

	delete [] svc_name;
	for (i = 0; i < (int)num_nodes; i++) {
		delete [] (*(prov_desc+i));
	}
	delete [] prov_desc;

	if ((uint32_t)argc == (num_nodes+5)) {
		delete [] fault_svc_name;
	}

	return (rslt);

#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}
