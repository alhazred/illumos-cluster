/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fault_ru.cc	1.7	08/05/20 SMI"

//
// This file contains the on and off functions for the fault points for a
// specific fault scenario.
//

#include	<sys/os.h>
#include	<orb/fault/fault_injection.h>
#include	<orb/fault/fault_functions.h>

int
fault_kill_svc_rm_primary_on(void *argp, int arglen)
{
	ASSERT(argp != NULL);
	ASSERT(arglen == sizeof (nodeid_t));
	FaultFunctions::wait_for_arg_t	wait_arg;

	wait_arg.op	= FaultFunctions::WAIT_FOR_UNKNOWN;
	wait_arg.nodeid	= *(nodeid_t *)argp;

	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT_ONCE,
	    FAULTNUM_HA_FRMWK_SERVICE_FROZEN_FOR_UPGRADE,
	    (void *)&wait_arg,
	    (uint32_t)sizeof (FaultFunctions::wait_for_arg_t),
	    wait_arg.nodeid);

	os::warning("Armed fault: ",
		"FAULTNUM_HA_FRMWK_SERVICE_FROZEN_FOR_UPGRADE");
	return (0);
}


int
fault_kill_svc_rm_primary_off(void *, int)
{
	return (0);
}
