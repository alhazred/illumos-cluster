/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)entry_stress_aux.cc	1.14	08/05/20 SMI"

#include <sys/os.h>

#include <orbtest/repl_sample/faults/entry_stress_aux.h>


// Initialize global variables
test_status_t	test_status;


//
// Helper to start test threads
//
int
start_test_thread(
	const char *test_name,
	int (*client_func)(FAULT_FPTR(a), FAULT_FPTR(b), void *, int,
	    Environment &, char *, bool),
	FAULT_FPTR(on),
	FAULT_FPTR(off),
	void * arg_ptr,
	int arg_len)
{
	test_arg_t	*t_arg;

	test_status.test_count += 1;

	t_arg = new test_arg_t;
	t_arg->test_name = new char[os::strlen(test_name) + 1];
	os::sprintf(t_arg->test_name, "%s", test_name);
	t_arg->client_func = client_func;
	t_arg->on_func = on;
	t_arg->off_func = off;
	t_arg->data = arg_ptr;
	t_arg->data_len = arg_len;


#if defined(_KERNEL)
	if (thread_create(NULL, NULL, (void(*)())test_thread_start_point,
	    (caddr_t)t_arg, 0, &p0, TS_RUN, 60) == NULL) {
#else
	if (os::thread::create(NULL, (size_t)0, test_thread_start_point, t_arg,
	    THR_BOUND, NULL)) {
#endif
		os::printf("+++ FAIL: Thread Creation Failed.\n");
		return (1);
	}

	return (0);
}

//
// This is the starting point for the test threads
//
void *
test_thread_start_point(void *argp)
{
	Environment	e;
	int		rslt;
	test_arg_t	*t_arg = (test_arg_t *)argp;
	ASSERT(t_arg->test_name != NULL);

	// Up the global count of test threads
	test_status.lck.lock();
	test_status.running_threads += 1;
	// While holding the lock announce what we're doing
	os::printf("*** th %8x: %s\n", os::threadid(), t_arg->test_name);
	test_status.lck.unlock();

	// Run this test
	rslt = t_arg->client_func(t_arg->on_func, t_arg->off_func, t_arg->data,
	    t_arg->data_len, e, "ha_sample_server", true);
	if (e.exception()) {
		e.exception()->print_exception("Exception during test!");
	}

	// Reduce the thread count
	test_status.lck.lock();

	if (rslt != 0) {
		// Set the status to fail
		test_status.fail = 1;
	}

	test_status.running_threads -= 1;
	if (test_status.running_threads == 0) {
		// We are the last one broadcast a message
		test_status.cv.broadcast();
	}

	test_status.lck.unlock();

	return (NULL);
}
