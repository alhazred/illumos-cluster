/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fault_two_phase_1_q.cc	1.16	08/05/20 SMI"

//
// This file contains the on and off functions for the fault points for a
// specific fault scenario.
//
// Refer to the README.faults file for a complete description of all
// scenarios
//

#include	<sys/os.h>
#include	<sys/mc_sema_impl.h>
#include	<orb/fault/fault_injection.h>
#include	<orbtest/repl_sample/faults/entry_common.h>


int
fault_two_phase_1_q_on(void *argp, int arglen)
{
	ASSERT(argp != NULL);
	ASSERT(arglen == sizeof (cookie_pair_t));

	FaultFunctions::mc_sema_arg_t	sema_arg1;
	FaultFunctions::mc_sema_arg_t	sema_arg2;
	cookie_pair_t			*cookie_pair;

	cookie_pair = (cookie_pair_t *)argp;

	// Setup the first fault, which should be after add_commit
	// we want to do a sema_v on mc_sema1, and NO_WAIT
	sema_arg1.id		= cookie_pair->id1;
	sema_arg1.nodeid	= cookie_pair->nodeid;
	sema_arg1.wait.op	= FaultFunctions::NO_WAIT;

	// Setup the second fault, which should be a sema_p during send_reply
	// Make sure we do NO_WAIT after sema operation
	sema_arg2.id		= cookie_pair->id2;
	sema_arg2.nodeid	= cookie_pair->nodeid;
	sema_arg2.wait.op	= FaultFunctions::NO_WAIT;

	FaultFunctions::invo_trigger_add(
		FaultFunctions::SEMA_V_SPECIFIED,
		FAULTNUM_HA_FRMWK_REPLICA_TMPL_ADD_COMMIT,
		&sema_arg1, (uint32_t)sizeof (FaultFunctions::mc_sema_arg_t));

	FaultFunctions::invo_trigger_add(
		FaultFunctions::SEMA_P_SPECIFIED,
		FAULTNUM_HA_FRMWK_HXDOOR_SEND_REPLY,
		&sema_arg2, (uint32_t)sizeof (FaultFunctions::mc_sema_arg_t));

	os::warning("Armed fault: "
		"FAULTNUM_HA_FRMWK_REPLICA_TMPL_ADD_COMMIT");

	os::warning("Armed fault: "
		"FAULTNUM_HA_FRMWK_HXDOOR_SEND_REPLY");
	return (0);
}

int
fault_two_phase_1_q_off(void *, int)
{
	InvoTriggers::clear(FAULTNUM_HA_FRMWK_REPLICA_TMPL_ADD_COMMIT);
	InvoTriggers::clear(FAULTNUM_HA_FRMWK_HXDOOR_SEND_REPLY);
	return (0);
}
