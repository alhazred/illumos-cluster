/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FAULTS_ON_SPECIFIC_SERVICE_H
#define	_FAULTS_ON_SPECIFIC_SERVICE_H

#pragma ident	"@(#)faults_on_specific_service.h	1.11	08/05/20 SMI"

#include <orbtest/repl_sample/faults/entry_common.h>

//
// This file contains the on and off functions for the fault points for a
// specific fault scenario.
//
// Refer to the README.faults file for a complete description of all
// scenarios
//

int fault_repl_prov_become_secondary_on(void *, int);
int fault_repl_prov_become_secondary_off(void *, int);

int fault_repl_prov_add_secondary_on(void *, int);
int fault_repl_prov_add_secondary_off(void *, int);

int fault_repl_prov_commit_secondary_on(void *, int);
int fault_repl_prov_commit_secondary_off(void *, int);

int fault_repl_prov_remove_secondary_on(void *, int);
int fault_repl_prov_remove_secondary_off(void *, int);

int fault_repl_prov_become_primary_on(void *, int);
int fault_repl_prov_become_primary_off(void *, int);

int fault_repl_prov_become_spare_on(void *, int);
int fault_repl_prov_become_spare_off(void *, int);

int fault_repl_prov_switchover_on(void *, int);
int fault_repl_prov_switchover_off(void *, int);

#endif /* _FAULTS_ON_SPECIFIC_SERVICE_H */
