/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ENTRY_CHANGE_DESIRED_NUMSECS_H
#define	_ENTRY_CHANGE_DESIRED_NUMSECS_H

#pragma ident	"@(#)entry_change_desired_numsecs.h	1.9	08/05/20 SMI"

void print_change_desired_numsecs_usage(const char *[]);
int change_desired_numsecs_no_fault(int, char *[]);
int change_desired_numsecs_fault_a(int, char *[]);
int change_desired_numsecs_fault_b(int, char *[]);
int change_desired_numsecs_fault_c(int, char *[]);
int change_desired_numsecs_fault_d(int, char *[]);
int change_desired_numsecs_fault_e(int, char *[]);
int change_desired_numsecs_fault_f(int, char *[]);

#endif	/* _ENTRY_CHANGE_DESIRED_NUMSECS_H */
