/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)entry_dep_shutdown_aux.cc	1.9	08/05/20 SMI"

#include	<h/replica.h>
#include	<sys/os.h>
#include	<orb/invo/common.h>
#include	<orb/infrastructure/orb_conf.h>
#include	<orb/fault/fault_injection.h>
#include	<orbtest/repl_sample/repl_sample_clnt.h>

#include	<orbtest/rm_probe/rm_snap.h>
#include	<orbtest/repl_sample/faults/entry_dep_shutdown_aux.h>

#include	<orbtest/repl_sample/faults/fault_dep_shutdown_1_b.h>

#ifdef _FAULT_INJECTION

void *
dep_shutdown_aux_hang_on_shutdown(void *argp)
{
	// Shutdown while using a fault point to hang this thread
	Environment	e;
	retval_t	*retval	= (retval_t *)argp;
	int		rslt;

	ASSERT(retval->id != 0);

	os::printf(" ** Starting shutdown.. sema cookie is: %d\n", retval->id);

	retval->lck.lock();
	rslt = shutdown_service(
		fault_dep_shutdown_1_b_on,
		fault_dep_shutdown_1_b_off,
		&(retval->id),
		(int)sizeof (mc_sema::cookie),
		"service_1",
		retval->e);

	retval->value = rslt;
	retval->exitcount++;
	retval->cv.broadcast();
	retval->lck.unlock();

	return (NULL);
}

#endif // _FAULT_INJECTION
