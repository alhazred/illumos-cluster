/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fault_disconnect_clnt.cc	1.9	08/05/20 SMI"

#include	<sys/os.h>
#include	<orb/fault/fault_injection.h>
#include	<orb/fault/fault_functions.h>
#include 	<orbtest/repl_sample/faults/fault_disconnect_clnt.h>
//
// This file contains trigger arming functions for the fault points for
// holding up a reference to cause disconnect_client() to wait and
// later acking this so that disconnect_client() continues.
//

int
fault_invoke_drop_tr_info_on(void *argp, int arglen)
{

	ASSERT(argp != NULL);
	ASSERT(arglen == sizeof (FaultFunctions::generic_arg_t));

	os::warning("Armed fault: FAULTNUM_CMM_AUTOMATON_BEGIN_STATE_1");
	NodeTriggers::add(FAULTNUM_CMM_AUTOMATON_BEGIN_STATE_1, argp,
	    arglen, 1);

	os::warning("Armed fault: "
	    "FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_SHUTDOWN_W_O_"
	    "WAITING_FOR_UNREF");
	NodeTriggers::add(
	    FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_SHUTDOWN_W_O_WAITING_FOR_UNREF,
	    NULL, 0, TRIGGER_ALL_NODES);

	// Arm triggers
	os::warning("Armed fault: FAULTNUM_HA_FRMWK_HOLD_XDOOR_ACK");
	InvoTriggers::add(FAULTNUM_HA_FRMWK_HOLD_XDOOR_ACK, NULL, 0);

	return (0);
}

int
fault_invoke_kill_on(void *argp, int arglen)
{

	ASSERT(argp != NULL);
	ASSERT(arglen == sizeof (nodeid_t));

	os::warning("Armed fault: FAULTNUM_HA_FRMWK_KILL_NODE_2");
	NodeTriggers::add(FAULTNUM_HA_FRMWK_KILL_NODE_2, NULL, 0, 2);

	return (0);
}
