/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ENTRY_HA_RM_H
#define	_ENTRY_HA_RM_H

#pragma ident	"@(#)entry_ha_rm.h	1.12	08/05/20 SMI"

#include <orb/fault/fault_functions.h>
#include <orbtest/repl_sample/faults/entry_common.h>
#define	MAX_FN_NAME_LEN	256

int state_mach_basic(int, char **);
int state_mach_shutdown(int, char **);
int state_mach_addservice(int, char **);
int state_mach_connectprimary(int, char **);
int state_mach_markdown(int, char **);

int chkpt_register_service_1(int, char **);
int chkpt_add_rma_1(int, char **);
int chkpt_shutdown_1(int, char **);
int chkpt_register_repl_prov_1(int, char **);
int chkpt_get_control_1(int, char **);

int chkpt_register_service_2(int, char **);
int chkpt_add_rma_2(int, char **);
int chkpt_shutdown_2(int, char **);
int chkpt_register_repl_prov_2(int, char **);
int chkpt_get_control_2(int, char **);

int chkpt_shutdown_3(int, char **);
int chkpt_shutdown_4(int, char **);

#endif	/* _ENTRY_HA_RM_H */
