/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)entry_disconnect_clnt.cc	1.10	08/05/20 SMI"

#include	<orbtest/repl_sample/repl_sample_clnt.h>
#include	<sys/os.h>
#include	<orb/invo/common.h>
#include	<orbtest/rm_probe/rm_snap.h>
#include	<orb/infrastructure/orb_conf.h>
#include 	<orb/fault/fault_injection.h>
#include	<orb/fault/fault_functions.h>
#include	<orbtest/repl_sample/faults/fault_disconnect_clnt.h>

//
// Wrapper function that calls drop_tr_info() defined in
// repl_sample_clnt.cc
//
int
make_invo_drop_tr_info(int, char *[])
{
#ifdef _KERNEL_ORB
	os::printf("Test calling disconnect client()\n");

#ifdef _FAULT_INJECTION

	// Figure out which node we'd like to fault (Primary node)
	rm_snap					_rma_snapshot;
	nodeid_t				primary_nodeid;
	Environment				e;
	int					returnvalue;
	FaultFunctions::generic_arg_t		*fault_arg;
	uint32_t		fault_arg_size;

	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_primary(primary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get primary nodeid\n");
		return (1);
	}

	os::printf("    Primary is node: %d\n", primary_nodeid);

	// Set up fault arguments
	fault_arg = FaultFunctions::generic_arg_alloc(fault_arg_size,
	    FaultFunctions::SEND_HELD_TRINFO_ACK,
	    NULL,
	    0,
	    FaultFunctions::UNTIL,	// do_counting() must return true until
					// count value
	    3,				// Count of the times this fault point
					//  must be triggered before the fault
					//  code is called.
	    os::SLEEP);

	returnvalue = (drop_tr_info(
	    fault_invoke_drop_tr_info_on,
	    NULL,
	    fault_arg,
	    fault_arg_size,
	    e,
	    "ha_sample_server",
	    false));	// Option says don't do a bunch of preliminary invos

	if (e.exception()) {
		e.exception()->print_exception(
		    "repl_sample::value_obj::compare_obj_ref()");
		return (1);
	}
	return (returnvalue);

#else
	return (no_fault_injection());
#endif

#else
	os::printf("+++ FAIL: Test not supported in USER mode\n");
	return (1);
#endif // _KERNEL_ORB
}
//
// Wrapper function that calls kill_node() in repl_sample_clnt.cc
//
int
make_invo_to_kill(int, char *[])
{
#ifdef _KERNEL_ORB
	os::printf("invocation that kills a node\n");

#ifdef _FAULT_INJECTION

	// Figure out which node we'd like to fault (Primary node)
	rm_snap			_rma_snapshot;
	nodeid_t		primary_nodeid;
	Environment		e;
	int			returnvalue;

	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_primary(primary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get primary nodeid\n");
		return (1);
	}

	os::printf("    Primary is node: %d\n", primary_nodeid);

	returnvalue = (kill_node(
	    fault_invoke_kill_on,
	    NULL,	// Upon test completion all triggers are already cleared
	    &primary_nodeid,
	    (int)sizeof (primary_nodeid),
	    e,
	    "ha_sample_server",
	    false));	// Option says don't do a bunch of preliminary invos

	if (e.exception()) {
		e.exception()->print_exception(
		    "repl_sample::value_obj::compare_obj_ref()");
		return (1);
	}

	return (returnvalue);

#else
	return (no_fault_injection());
#endif

#else
	os::printf("+++ FAIL: Test not supported in USER mode\n");
	return (1);

#endif // _KERNEL_ORB
}
