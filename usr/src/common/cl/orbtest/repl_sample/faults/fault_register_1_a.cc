/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fault_register_1_a.cc	1.11	08/05/20 SMI"

//
// This file contains the on and off functions for the fault points for a
// specific fault scenario.
//
// Refer to the README.faults file for a complete description of all
// scenarios
//

#include	<sys/os.h>
#include	<h/mc_sema.h>
#include	<sys/mc_sema_impl.h>
#include	<orb/fault/fault_injection.h>

int
fault_register_1_a_on(void * argp, int arglen)
{
	ASSERT(argp != NULL);
	ASSERT(arglen == sizeof (nodeid_t));

	Environment			e;
	FaultFunctions::wait_for_arg_t	wait_arg;
	FaultFunctions::mc_sema_arg_t	sema_arg;
	mc_sema::sema_var		sema_ref;

	sema_ref = (new mc_sema_impl)->get_objref();
	ASSERT(!CORBA::is_nil(sema_ref));

	sema_ref->init(0, e);
	if (e.exception()) {
		e.exception()->print_exception("register_1_a: initializing "
		    "mc sema\n");
		return (1);
	}

	// Set up the arguments for this semaphore
	sema_arg.nodeid = 0;
	sema_arg.id = sema_ref->get_cookie(e);
	if (e.exception()) {
		e.exception()->print_exception("register_1_a: getting mc_sema "
		    "cookie\n");
		return (1);
	}
	// Set up the SEMA operation to do NO_WAIT
	sema_arg.wait.op = FaultFunctions::NO_WAIT;

	ASSERT(sema_arg.id != 0);

	//
	// We want to wait for the node to resurrect
	//
	wait_arg.op	= FaultFunctions::WAIT_FOR_UNKNOWN;
	wait_arg.nodeid	= *(nodeid_t *)argp;

	// Need a fault to reboot a node
	FaultFunctions::invo_trigger_add(FaultFunctions::REBOOT,
		FAULTNUM_HA_FRMWK_REPL_SERVICE_REGISTER_WITH_RM_1,
		&wait_arg, (uint32_t)sizeof (FaultFunctions::wait_for_arg_t));

	// Need a fault to do a SEMA_P (hang) the client thread until the
	// rebooted node does a add_rma on the RM  (Which will do a SEMA_P)
	FaultFunctions::invo_trigger_add(FaultFunctions::SEMA_P,
		FAULTNUM_HA_FRMWK_REPL_SERVICE_REGISTER_WITH_RM_2,
		&sema_arg, (uint32_t)sizeof (FaultFunctions::mc_sema_arg_t));

	// Need a fault to trigger a SEMA_V operation when the rebooted node
	// does a add_rma on the rm
	FaultFunctions::node_trigger_add(FaultFunctions::SEMA_V,
		FAULTNUM_HA_FRMWK_REPL_MGR_IMPL_ADD_RMA,
		&sema_arg, (uint32_t)sizeof (FaultFunctions::mc_sema_arg_t),
		TRIGGER_ALL_NODES);

	os::warning("Armed fault: "
		"FAULTNUM_HA_FRMWK_REPL_SERVICE_REGISTER_WITH_RM_1");
	os::warning("Armed fault: "
		"FAULTNUM_HA_FRMWK_REPL_SERVICE_REGISTER_WITH_RM_2");
	os::warning("Armed fault: "
		"FAULTNUM_HA_FRMWK_REPL_MGR_IMPL_ADD_RMA");

	return (0);
}


int
fault_register_1_a_off(void *, int)
{
	Environment	e;

	InvoTriggers::clear(
		FAULTNUM_HA_FRMWK_REPL_SERVICE_REGISTER_WITH_RM_1);

	InvoTriggers::clear(
		FAULTNUM_HA_FRMWK_REPL_SERVICE_REGISTER_WITH_RM_2);

	NodeTriggers::clear(
		FAULTNUM_HA_FRMWK_REPL_MGR_IMPL_ADD_RMA,
		TRIGGER_ALL_NODES);

	return (0);
}
