/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)entry_dep.cc	1.17	08/05/20 SMI"

#include	<h/replica.h>
#include	<orb/fault/fault_injection.h>
#include	<orbtest/repl_sample/repl_sample_clnt.h>
#include	<sys/os.h>
#include	<orb/invo/common.h>
#include	<orbtest/rm_probe/rm_snap.h>
#include	<orb/infrastructure/orb_conf.h>

#include	<orbtest/repl_sample/faults/entry_dep_aux.h>

#include	<orbtest/repl_sample/faults/fault_dep_1_a.h>
#include	<orbtest/repl_sample/faults/fault_dep_1_b.h>
#include	<orbtest/repl_sample/faults/fault_dep_1_c.h>
#include	<orbtest/repl_sample/faults/fault_dep_1_d.h>
#include	<orbtest/repl_sample/faults/fault_dep_1_e.h>
#include	<orbtest/repl_sample/faults/fault_dep_1_f.h>
#include	<orbtest/repl_sample/faults/fault_dep_1_g.h>
#include	<orbtest/repl_sample/faults/fault_dep_1_h.h>
#include	<orbtest/repl_sample/faults/fault_dep_1_i.h>
#include	<orbtest/repl_sample/faults/fault_dep_1_j.h>
#include	<orbtest/repl_sample/faults/fault_dep_1_k.h>
#include	<orbtest/repl_sample/faults/fault_dep_1_l.h>


//
// Dependency 1-A Test case
//
int
dep_1_a(int, char *[])
{
	os::printf("*** Dependency Scenario 1 Fault A\n");

#ifdef _FAULT_INJECTION
	//
	// No Faults
	// Baseline dependency switchover
	//
	svc_state_list_t	svc_state_list;
	int			rslt;

	svc_state_list.listlen = 2;
	svc_state_list.bottom_svc = 0;
	svc_state_list.top_svc = 1;
	svc_state_list.list = new svc_state_t[2];
	os::sprintf(svc_state_list.list[0].name, "service_1");
	os::sprintf(svc_state_list.list[1].name, "service_2");
	svc_state_list.list[0].state = replica::S_UP;
	svc_state_list.list[1].state = replica::S_UP;

	rslt = dep_switchover_helper(
	    fault_dep_1_a_on,
	    fault_dep_1_a_off,
	    0,
	    0,
	    true,
	    &svc_state_list);

	delete []svc_state_list.list;
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Dependency 1-B Test case
//
int
dep_1_b(int, char *[])
{
	os::printf("*** Dependency Scenario 1 Fault B\n");

#ifdef _FAULT_INJECTION

	svc_state_list_t	svc_state_list;
	int			rslt;

	svc_state_list.listlen = 2;
	svc_state_list.bottom_svc = 0;
	svc_state_list.top_svc = 1;
	svc_state_list.list = new svc_state_t[2];
	os::sprintf(svc_state_list.list[0].name, "service_1");
	os::sprintf(svc_state_list.list[1].name, "service_2");
	svc_state_list.list[0].state = replica::S_UP;
	svc_state_list.list[1].state = replica::S_UP;

	rslt = dep_switchover_helper(
	    fault_dep_1_b_on,
	    fault_dep_1_b_off,
	    get_sid("service_2"),
	    get_primary("service_1"),
	    true,
	    &svc_state_list);

	delete []svc_state_list.list;
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

//
// Dependency 1-C Test case
//
int
dep_1_c(int, char *[])
{
	os::printf("*** Dependency Scenario 1 Fault C\n");

#ifdef _FAULT_INJECTION

	svc_state_list_t	svc_state_list;
	int			rslt;

	svc_state_list.listlen = 2;
	svc_state_list.bottom_svc = 0;
	svc_state_list.top_svc = 1;
	svc_state_list.list = new svc_state_t[2];
	os::sprintf(svc_state_list.list[0].name, "service_1");
	os::sprintf(svc_state_list.list[1].name, "service_2");
	svc_state_list.list[0].state = replica::S_UP;
	svc_state_list.list[1].state = replica::S_UP;

	rslt = dep_switchover_helper(
	    fault_dep_1_c_on,
	    fault_dep_1_c_off,
	    get_sid("service_2"),
	    get_primary("service_1"),
	    true,
	    &svc_state_list);

	delete []svc_state_list.list;
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

//
// Dependency 1-D Test case
//
int
dep_1_d(int, char *[])
{
	os::printf("*** Dependency Scenario 1 Fault D\n");

#ifdef _FAULT_INJECTION

	svc_state_list_t	svc_state_list;
	int			rslt;

	svc_state_list.listlen = 2;
	svc_state_list.bottom_svc = 0;
	svc_state_list.top_svc = 1;
	svc_state_list.list = new svc_state_t[2];
	os::sprintf(svc_state_list.list[0].name, "service_1");
	os::sprintf(svc_state_list.list[1].name, "service_2");
	svc_state_list.list[0].state = replica::S_UP;
	svc_state_list.list[1].state = replica::S_UP;

	rslt = dep_switchover_helper(
	    fault_dep_1_d_on,
	    fault_dep_1_d_off,
	    get_sid("service_2"),
	    get_primary("service_1"),
	    true,
	    &svc_state_list);

	delete []svc_state_list.list;
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Dependency 1-E Test case
//
int
dep_1_e(int, char *[])
{
	os::printf("*** Dependency Scenario 1 Fault E\n");
#ifdef _FAULT_INJECTION

	svc_state_list_t	svc_state_list;
	int			rslt;

	svc_state_list.listlen = 2;
	svc_state_list.bottom_svc = 0;
	svc_state_list.top_svc = 1;
	svc_state_list.list = new svc_state_t[2];
	os::sprintf(svc_state_list.list[0].name, "service_1");
	os::sprintf(svc_state_list.list[1].name, "service_2");
	svc_state_list.list[0].state = replica::S_UP;
	svc_state_list.list[1].state = replica::S_UP;

	rslt = dep_switchover_helper(
	    fault_dep_1_e_on,
	    fault_dep_1_e_off,
	    get_sid("service_2"),
	    get_primary("service_1"),
	    true,
	    &svc_state_list);

	delete []svc_state_list.list;
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

//
// Dependency 1-F Test case
//
int
dep_1_f(int, char *[])
{
	os::printf("*** Dependency Scenario 1 Fault F\n");

#ifdef _FAULT_INJECTION

	svc_state_list_t	svc_state_list;
	int			rslt;

	svc_state_list.listlen = 2;
	svc_state_list.bottom_svc = 0;
	svc_state_list.top_svc = 1;
	svc_state_list.list = new svc_state_t[2];
	os::sprintf(svc_state_list.list[0].name, "service_1");
	os::sprintf(svc_state_list.list[1].name, "service_2");
	svc_state_list.list[0].state = replica::S_UP;
	svc_state_list.list[1].state = replica::S_UP;

	rslt = dep_switchover_helper(
	    fault_dep_1_f_on,
	    fault_dep_1_f_off,
	    get_sid("service_1"),
	    get_secondary(1, "service_1"),
	    false,
	    &svc_state_list);

	delete []svc_state_list.list;
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Dependency 1-G Test case
//
int
dep_1_g(int, char *[])
{
	os::printf("*** Dependency Scenario 1 Fault G\n");

#ifdef _FAULT_INJECTION

	svc_state_list_t	svc_state_list;
	int			rslt;

	svc_state_list.listlen = 2;
	svc_state_list.bottom_svc = 0;
	svc_state_list.top_svc = 1;
	svc_state_list.list = new svc_state_t[2];
	os::sprintf(svc_state_list.list[0].name, "service_1");
	os::sprintf(svc_state_list.list[1].name, "service_2");
	svc_state_list.list[0].state = replica::S_UP;
	svc_state_list.list[1].state = replica::S_UP;

	rslt = dep_switchover_helper(
	    fault_dep_1_g_on,
	    fault_dep_1_g_off,
	    get_sid("service_1"),
	    get_secondary(1, "service_1"),
	    false,
	    &svc_state_list);

	delete []svc_state_list.list;
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

//
// Dependency 1-H Test case
//
int
dep_1_h(int, char *[])
{
	os::printf("*** Dependency Scenario 1 Fault H\n");

#ifdef _FAULT_INJECTION

	svc_state_list_t	svc_state_list;
	int			rslt;

	svc_state_list.listlen = 2;
	svc_state_list.bottom_svc = 0;
	svc_state_list.top_svc = 1;
	svc_state_list.list = new svc_state_t[2];
	os::sprintf(svc_state_list.list[0].name, "service_1");
	os::sprintf(svc_state_list.list[1].name, "service_2");
	svc_state_list.list[0].state = replica::S_UP;
	svc_state_list.list[1].state = replica::S_UP;

	rslt = dep_switchover_helper(
	    fault_dep_1_h_on,
	    fault_dep_1_h_off,
	    get_sid("service_1"),
	    get_secondary(1, "service_1"),
	    false,
	    &svc_state_list);

	delete []svc_state_list.list;
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

//
// Dependency 1-I Test case
//
int
dep_1_i(int, char *[])
{
	os::printf("*** Dependency Scenario 1 Fault I\n");

#ifdef _FAULT_INJECTION

	svc_state_list_t	svc_state_list;
	int			rslt;

	svc_state_list.listlen = 2;
	svc_state_list.bottom_svc = 0;
	svc_state_list.top_svc = 1;
	svc_state_list.list = new svc_state_t[2];
	os::sprintf(svc_state_list.list[0].name, "service_1");
	os::sprintf(svc_state_list.list[1].name, "service_2");
	svc_state_list.list[0].state = replica::S_UP;
	svc_state_list.list[1].state = replica::S_UP;

	nodeid_t		sec_nodeid = get_secondary(1, "service_1");

	rslt = dep_switchover_helper(
	    fault_dep_1_i_on,
	    fault_dep_1_i_off,
	    get_sid("service_1"),
	    sec_nodeid, false,
	    &svc_state_list);

	delete []svc_state_list.list;
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

//
// Dependency 1-J Test case
//
int
dep_1_j(int, char *[])
{
	os::printf("*** Dependency Scenario 1 Fault J\n");

#ifdef _FAULT_INJECTION

	svc_state_list_t	svc_state_list;
	int			rslt;

	svc_state_list.listlen = 2;
	svc_state_list.bottom_svc = 0;
	svc_state_list.top_svc = 1;
	svc_state_list.list = new svc_state_t[2];
	os::sprintf(svc_state_list.list[0].name, "service_1");
	os::sprintf(svc_state_list.list[1].name, "service_2");
	svc_state_list.list[0].state = replica::S_UP;
	svc_state_list.list[1].state = replica::S_UP;

	rslt = dep_switchover_helper(
	    fault_dep_1_j_on,
	    fault_dep_1_j_off,
	    get_sid("service_1"),
	    get_secondary(1, "service_1"),
	    false,
	    &svc_state_list);

	delete []svc_state_list.list;
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

//
// Dependency 1-K Test case
//
int
dep_1_k(int, char *[])
{
	os::printf("*** Dependency Scenario 1 Fault K\n");

#ifdef _FAULT_INJECTION

	svc_state_list_t	svc_state_list;
	int			rslt;

	nodeid_t		primary_nodeid = get_primary("service_1");

	svc_state_list.listlen = 2;
	svc_state_list.bottom_svc = 0;
	svc_state_list.top_svc = 1;
	svc_state_list.list = new svc_state_t[2];
	os::sprintf(svc_state_list.list[0].name, "service_1");
	os::sprintf(svc_state_list.list[1].name, "service_2");
	svc_state_list.list[0].state = replica::S_UP;
	svc_state_list.list[1].state = replica::S_UP;

	rslt = dep_switchover_helper(
	    fault_dep_1_k_on,
	    fault_dep_1_k_off,
	    get_sid("service_1"),
	    primary_nodeid, true,
	    &svc_state_list, false);

	delete []svc_state_list.list;
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Dependency 1-L Test case
//
int
dep_1_l(int, char *[])
{
	os::printf("*** Dependency Scenario 1 Fault L\n");

#ifdef _FAULT_INJECTION

	svc_state_list_t	svc_state_list;
	int			rslt;

	svc_state_list.listlen = 2;
	svc_state_list.bottom_svc = 0;
	svc_state_list.top_svc = 1;
	svc_state_list.list = new svc_state_t[2];
	os::sprintf(svc_state_list.list[0].name, "service_1");
	os::sprintf(svc_state_list.list[1].name, "service_2");
	svc_state_list.list[0].state = replica::S_UP;
	svc_state_list.list[1].state = replica::S_UP;

	rslt = dep_switchover_helper(
	    fault_dep_1_l_on,
	    fault_dep_1_l_off,
	    get_sid("service_1"),
	    get_secondary(1, "service_1"),
	    false,
	    &svc_state_list);

	delete []svc_state_list.list;
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

//
// Dependency 1-M Test case
//
int
dep_1_m(int, char *[])
{
	os::printf("*** Dependency Scenario 1 Fault M\n");

#ifdef _FAULT_INJECTION
	nodeid_t	primary_nodeid;

	// Verify that the services are up
	if (get_state("service_1") != replica::S_UP) {
		os::printf("+++ FAIL: service_1 is not in S_UP state\n");
		return (1);
	}
	if (get_state("service_2") != replica::S_UP) {
		os::printf("+++ FAIL: service_2 is not in S_UP state\n");
		return (1);
	}
	primary_nodeid = get_primary("service_1");

	// Fail over the service by killing the primary node
	FaultFunctions::wait_for_arg_t	wait_arg;
	wait_arg.op	= FaultFunctions::WAIT_FOR_DEAD;
	wait_arg.nodeid	= get_primary("service_1");

	os::printf(" ** Killing node: %d\n", wait_arg.nodeid);
	FaultFunctions::reboot(0, &wait_arg, (uint32_t)sizeof (wait_arg));

	os::printf(" ** Waiting for reconfig to finish... ");
	while (get_primary("service_1") == primary_nodeid) {
		os::printf(".");
		os::usecsleep(1 * 1000000);
	}
	os::printf(" done\n");

	// Verify that service_1 is still up
	if (get_state("service_1") != replica::S_UP) {
		os::printf("+++ FAIL: service_1 is not in S_UP state\n");
		return (1);
	}

	// Verify that service_2 is still up
	if (get_state("service_2") != replica::S_UP) {
		os::printf("+++ FAIL: service_2 is not in S_UP state\n");
		return (1);
	}

	os::printf("--- PASS: Failover was succesful\n");

	return (0);
#else
	return (no_fault_injection());
#endif
}

//
// Dependency 1-N Test case
//
int
dep_1_n(int, char *[])
{
	os::printf("*** Dependency Scenario 1 Fault N\n");

#ifdef _FAULT_INJECTION
	nodeid_t	primary_nodeid;

	// Verify that the services are up
	if (get_state("service_1") != replica::S_UP) {
		os::printf("+++ FAIL: service_1 is not in S_UP state\n");
		return (1);
	}
	if (get_state("service_2") != replica::S_UP) {
		os::printf("+++ FAIL: service_2 is not in S_UP state\n");
		return (1);
	}
	primary_nodeid = get_primary("service_1");

	// We will fail over the service, but no secondaries will exist for
	// service_2 so it will go down

	// Fail over the service by killing the primary node
	FaultFunctions::wait_for_arg_t	wait_arg;
	wait_arg.op	= FaultFunctions::WAIT_FOR_DEAD;
	wait_arg.nodeid	= get_primary("service_1");

	os::printf(" ** Killing node: %d\n", wait_arg.nodeid);
	FaultFunctions::reboot(0, &wait_arg, (uint32_t)sizeof (wait_arg));

	os::printf(" ** Waiting for reconfig to finish... ");
	while (get_primary("service_1") == primary_nodeid) {
		os::printf(".");
		os::usecsleep(1 * 1000000);
	}
	os::printf(" done\n");

	// Verify that service_1 is still up
	if (get_state("service_1") != replica::S_UP) {
		os::printf("+++ FAIL: service_1 is not in S_UP state\n");
		return (1);
	}

	// Verify that service_2 is now down
	if (get_state("service_2") != replica::S_DOWN) {
		os::printf("+++ FAIL: service_2 is not in S_DOWN state\n");
		return (1);
	}

	os::printf("--- PASS: Failover was succesful\n");

	return (0);
#else
	return (no_fault_injection());
#endif
}


//
// Scenario 2
//
// service_3 depends on service_2 depends on service_1
//

//
// Dependency 2-A Test case
//
int
dep_2_a(int, char *[])
{
	os::printf("*** Dependency Scenario 2 Fault A\n");

#ifdef _FAULT_INJECTION
	//
	// No fault
	//
	svc_state_list_t	svc_state_list;
	int			rslt;

	svc_state_list.listlen = 3;
	svc_state_list.bottom_svc = 0;
	svc_state_list.top_svc = 2;
	svc_state_list.list = new svc_state_t[3];
	os::sprintf(svc_state_list.list[0].name, "service_1");
	os::sprintf(svc_state_list.list[1].name, "service_2");
	os::sprintf(svc_state_list.list[2].name, "service_3");
	svc_state_list.list[0].state = replica::S_UP;
	svc_state_list.list[1].state = replica::S_UP;
	svc_state_list.list[2].state = replica::S_UP;

	rslt = dep_switchover_helper(
	    fault_dep_1_a_on,
	    fault_dep_1_a_off,
	    0,
	    0,
	    true,
	    &svc_state_list);

	delete []svc_state_list.list;
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Dependency 2-B Test case
//
int
dep_2_b(int, char *[])
{
	os::printf("*** Dependency Scenario 2 Fault B\n");

#ifdef _FAULT_INJECTION

	svc_state_list_t	svc_state_list;
	int			rslt;

	svc_state_list.listlen = 3;
	svc_state_list.bottom_svc = 0;
	svc_state_list.top_svc = 2;
	svc_state_list.list = new svc_state_t[3];
	os::sprintf(svc_state_list.list[0].name, "service_1");
	os::sprintf(svc_state_list.list[1].name, "service_2");
	os::sprintf(svc_state_list.list[2].name, "service_3");
	svc_state_list.list[0].state = replica::S_UP;
	svc_state_list.list[1].state = replica::S_UP;
	svc_state_list.list[2].state = replica::S_UP;

	// By faultint on sid for service_2 that means that service_1
	// is still waiting for above and service_3 has moved on

	rslt = dep_switchover_helper(
	    fault_dep_1_b_on,
	    fault_dep_1_b_off,
	    get_sid("service_2"),
	    get_primary("service_1"),
	    true,
	    &svc_state_list);

	delete []svc_state_list.list;
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Dependency 2-C Test case
//
// Not Implemented


//
// Dependency 2-D Test case
//
int
dep_2_d(int, char *[])
{
	os::printf("*** Dependency Scenario 2 Fault D\n");

#ifdef _FAULT_INJECTION

	svc_state_list_t	svc_state_list;
	int			rslt;

	svc_state_list.listlen = 3;
	svc_state_list.bottom_svc = 0;
	svc_state_list.top_svc = 2;
	svc_state_list.list = new svc_state_t[3];
	os::sprintf(svc_state_list.list[0].name, "service_1");
	os::sprintf(svc_state_list.list[1].name, "service_2");
	os::sprintf(svc_state_list.list[2].name, "service_3");
	svc_state_list.list[0].state = replica::S_UP;
	svc_state_list.list[1].state = replica::S_UP;
	svc_state_list.list[2].state = replica::S_UP;

	// By faulting on sid service_2 that means that service_1 is still
	// waiting for service_2 to reach PD and service_3 has moved on

	rslt = dep_switchover_helper(
	    fault_dep_1_d_on,
	    fault_dep_1_d_off,
	    get_sid("service_2"),
	    get_primary("service_1"),
	    true,
	    &svc_state_list);

	delete []svc_state_list.list;
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

//
// Dependency 2-E Test case
//
// Not Implemented

//
// Dependency 2-F Test case
//
// Not Implemented

//
// Dependency 2-G Test case
//
int
dep_2_g(int, char *[])
{
	os::printf("*** Dependency Scenario 2 Fault G\n");

#ifdef _FAULT_INJECTION

	svc_state_list_t	svc_state_list;
	int			rslt;

	svc_state_list.listlen = 3;
	svc_state_list.bottom_svc = 0;
	svc_state_list.top_svc = 2;
	svc_state_list.list = new svc_state_t[3];
	os::sprintf(svc_state_list.list[0].name, "service_1");
	os::sprintf(svc_state_list.list[1].name, "service_2");
	os::sprintf(svc_state_list.list[2].name, "service_3");
	svc_state_list.list[0].state = replica::S_UP;
	svc_state_list.list[1].state = replica::S_UP;
	svc_state_list.list[2].state = replica::S_UP;

	//
	// By faulting on service_2, we force service_1 to have proceeded
	// and service_3 to be waiting for service_2
	//
	rslt = dep_switchover_helper(
	    fault_dep_1_g_on,
	    fault_dep_1_g_off,
	    get_sid("service_2"),
	    get_secondary(1, "service_1"),
	    false,
	    &svc_state_list);

	delete []svc_state_list.list;
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Dependency 2-H Test case
//
int
dep_2_h(int, char *[])
{
	os::printf("*** Dependency Scenario 2 Fault H\n");

#ifdef _FAULT_INJECTION

	svc_state_list_t	svc_state_list;
	int			rslt;

	svc_state_list.listlen = 3;
	svc_state_list.bottom_svc = 0;
	svc_state_list.top_svc = 2;
	svc_state_list.list = new svc_state_t[3];
	os::sprintf(svc_state_list.list[0].name, "service_1");
	os::sprintf(svc_state_list.list[1].name, "service_2");
	os::sprintf(svc_state_list.list[2].name, "service_3");
	svc_state_list.list[0].state = replica::S_UP;
	svc_state_list.list[1].state = replica::S_UP;
	svc_state_list.list[2].state = replica::S_UP;

	// By faulting of service_2 we force service_3 to be waiting
	// while service_1 has moved on

	rslt = dep_switchover_helper(
	    fault_dep_1_h_on,
	    fault_dep_1_h_off,
	    get_sid("service_2"),
	    get_secondary(1, "service_1"),
	    false,
	    &svc_state_list);

	delete []svc_state_list.list;
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Dependency 2-I Test case
//
// Not Implemented


//
// Dependency 2-J Test case
//
int
dep_2_j(int, char *[])
{
	os::printf("*** Dependency Scenario 2 Fault J\n");

#ifdef _FAULT_INJECTION

	svc_state_list_t	svc_state_list;
	int			rslt;

	svc_state_list.listlen = 3;
	svc_state_list.bottom_svc = 0;
	svc_state_list.top_svc = 2;
	svc_state_list.list = new svc_state_t[3];
	os::sprintf(svc_state_list.list[0].name, "service_1");
	os::sprintf(svc_state_list.list[1].name, "service_2");
	os::sprintf(svc_state_list.list[2].name, "service_3");
	svc_state_list.list[0].state = replica::S_UP;
	svc_state_list.list[1].state = replica::S_UP;
	svc_state_list.list[2].state = replica::S_UP;

	// By faulting on service_2 we force service_3 to still be waiting
	// while service_1 has moved on

	rslt = dep_switchover_helper(
	    fault_dep_1_j_on,
	    fault_dep_1_j_off,
	    get_sid("service_2"),
	    get_secondary(1, "service_1"),
	    false,
	    &svc_state_list);

	delete []svc_state_list.list;
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Dependency 2-M Test case
//
int
dep_2_m(int, char *[])
{
	os::printf("*** Dependency Scenario 2 Fault M\n");

#ifdef _FAULT_INJECTION
	nodeid_t	primary_nodeid;

	// Verify that the services are up
	if (get_state("service_1") != replica::S_UP) {
		os::printf("+++ FAIL: service_1 is not in S_UP state\n");
		return (1);
	}
	if (get_state("service_2") != replica::S_UP) {
		os::printf("+++ FAIL: service_2 is not in S_UP state\n");
		return (1);
	}
	if (get_state("service_3") != replica::S_UP) {
		os::printf("+++ FAIL: service_3 is not in S_UP state\n");
		return (1);
	}
	primary_nodeid = get_primary("service_1");

	// Fail over the service by killing the primary node
	FaultFunctions::wait_for_arg_t	wait_arg;
	wait_arg.op	= FaultFunctions::WAIT_FOR_DEAD;
	wait_arg.nodeid	= get_primary("service_1");

	os::printf(" ** Killing node: %d\n", wait_arg.nodeid);
	FaultFunctions::reboot(0, &wait_arg, (uint32_t)sizeof (wait_arg));

	os::printf(" ** Waiting for reconfig to finish... ");
	while (get_primary("service_1") == primary_nodeid) {
		os::printf(".");
		os::usecsleep(1 * 1000000);
	}
	os::printf(" done\n");

	// Verify that service_1 is still up
	if (get_state("service_1") != replica::S_UP) {
		os::printf("+++ FAIL: service_1 is not in S_UP state\n");
		return (1);
	}

	// Verify that service_2 is still up
	if (get_state("service_2") != replica::S_UP) {
		os::printf("+++ FAIL: service_2 is not in S_UP state\n");
		return (1);
	}

	// Verify that service_3 is still up
	if (get_state("service_3") != replica::S_UP) {
		os::printf("+++ FAIL: service_3 is not in S_UP state\n");
		return (1);
	}

	os::printf("--- PASS: Failover was succesful\n");

	return (0);
#else
	return (no_fault_injection());
#endif
}

//
// Dependency 2-N Test case
//
int
dep_2_n(int, char *[])
{
	os::printf("*** Dependency Scenario 2 Fault N\n");

#ifdef _FAULT_INJECTION
	nodeid_t	primary_nodeid;

	// Verify that the services are up
	if (get_state("service_1") != replica::S_UP) {
		os::printf("+++ FAIL: service_1 is not in S_UP state\n");
		return (1);
	}
	if (get_state("service_2") != replica::S_UP) {
		os::printf("+++ FAIL: service_2 is not in S_UP state\n");
		return (1);
	}
	if (get_state("service_3") != replica::S_UP) {
		os::printf("+++ FAIL: service_3 is not in S_UP state\n");
		return (1);
	}
	primary_nodeid = get_primary("service_1");

	// We will fail over the service, but no secondaries will exist for
	// service_2 so it will go down

	// Fail over the service by killing the primary node
	FaultFunctions::wait_for_arg_t	wait_arg;
	wait_arg.op	= FaultFunctions::WAIT_FOR_DEAD;
	wait_arg.nodeid	= get_primary("service_1");

	os::printf(" ** Killing node: %d\n", wait_arg.nodeid);
	FaultFunctions::reboot(0, &wait_arg, (uint32_t)sizeof (wait_arg));

	os::printf(" ** Waiting for reconfig to finish... ");
	while (get_primary("service_1") == primary_nodeid) {
		os::printf(".");
		os::usecsleep(1 * 1000000);
	}
	os::printf(" done\n");

	// Verify that service_1 is still up
	if (get_state("service_1") != replica::S_UP) {
		os::printf("+++ FAIL: service_1 is not in S_UP state\n");
		return (1);
	}

	// Verify that service_2 is still up
	if (get_state("service_2") != replica::S_UP) {
		os::printf("+++ FAIL: service_2 is not in S_UP state\n");
		return (1);
	}

	// Verify that service_3 is now down
	if (get_state("service_3") != replica::S_DOWN) {
		os::printf("+++ FAIL: service_3 is not in S_DOWN state\n");
		return (1);
	}

	os::printf("--- PASS: Failover was succesful\n");

	return (0);
#else
	return (no_fault_injection());
#endif
}


//
// Scenario 3
//
// service_3 depends on service_2 and service_1
// service_2 depends on service_1
// service_1
//

//
// Dependency 3-A Test case
//
int
dep_3_a(int, char *[])
{
	os::printf("*** Dependency Scenario 3 Fault A\n");

#ifdef _FAULT_INJECTION

	svc_state_list_t	svc_state_list;
	int			rslt;

	svc_state_list.listlen = 3;
	svc_state_list.bottom_svc = 0;
	svc_state_list.top_svc = 2;
	svc_state_list.list = new svc_state_t[3];
	os::sprintf(svc_state_list.list[0].name, "service_1");
	os::sprintf(svc_state_list.list[1].name, "service_2");
	os::sprintf(svc_state_list.list[2].name, "service_3");
	svc_state_list.list[0].state = replica::S_UP;
	svc_state_list.list[1].state = replica::S_UP;
	svc_state_list.list[2].state = replica::S_UP;

	rslt = dep_switchover_helper(
	    fault_dep_1_a_on,
	    fault_dep_1_a_off,
	    0,
	    0,
	    true,
	    &svc_state_list);

	delete []svc_state_list.list;
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Dependency 3-B Test case
//
int
dep_3_b(int, char *[])
{
	os::printf("*** Dependency Scenario 3 Fault B\n");

#ifdef _FAULT_INJECTION

	svc_state_list_t	svc_state_list;
	int			rslt;

	svc_state_list.listlen = 3;
	svc_state_list.bottom_svc = 0;
	svc_state_list.top_svc = 2;
	svc_state_list.list = new svc_state_t[3];
	os::sprintf(svc_state_list.list[0].name, "service_1");
	os::sprintf(svc_state_list.list[1].name, "service_2");
	os::sprintf(svc_state_list.list[2].name, "service_3");
	svc_state_list.list[0].state = replica::S_UP;
	svc_state_list.list[1].state = replica::S_UP;
	svc_state_list.list[2].state = replica::S_UP;

	// By faulting on sid for service_2 that means that service_1
	// is still waiting for above and service_3 has moved on

	rslt = dep_switchover_helper(
	    fault_dep_1_b_on,
	    fault_dep_1_b_off,
	    get_sid("service_2"),
	    get_primary("service_1"),
	    true,
	    &svc_state_list);

	delete []svc_state_list.list;
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Dependency 3-C Test case
//
// Not Implemented


//
// Dependency 3-D Test case
//
int
dep_3_d(int, char *[])
{
	os::printf("*** Dependency Scenario 3 Fault D\n");

#ifdef _FAULT_INJECTION

	svc_state_list_t	svc_state_list;
	int			rslt;

	svc_state_list.listlen = 3;
	svc_state_list.bottom_svc = 0;
	svc_state_list.top_svc = 2;
	svc_state_list.list = new svc_state_t[3];
	os::sprintf(svc_state_list.list[0].name, "service_1");
	os::sprintf(svc_state_list.list[1].name, "service_2");
	os::sprintf(svc_state_list.list[2].name, "service_3");
	svc_state_list.list[0].state = replica::S_UP;
	svc_state_list.list[1].state = replica::S_UP;
	svc_state_list.list[2].state = replica::S_UP;

	rslt = dep_switchover_helper(
	    fault_dep_1_d_on,
	    fault_dep_1_d_off,
	    get_sid("service_2"),
	    get_primary("service_1"),
	    true,
	    &svc_state_list);

	delete []svc_state_list.list;
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

//
// Dependency 3-E Test case
//
// Not Implemented

//
// Dependency 3-F Test case
//
// Not Implemented

//
// Dependency 3-G Test case
//
int
dep_3_g(int, char *[])
{
	os::printf("*** Dependency Scenario 3 Fault G\n");

#ifdef _FAULT_INJECTION

	svc_state_list_t	svc_state_list;
	int			rslt;

	svc_state_list.listlen = 3;
	svc_state_list.bottom_svc = 0;
	svc_state_list.top_svc = 2;
	svc_state_list.list = new svc_state_t[3];
	os::sprintf(svc_state_list.list[0].name, "service_1");
	os::sprintf(svc_state_list.list[1].name, "service_2");
	os::sprintf(svc_state_list.list[2].name, "service_3");
	svc_state_list.list[0].state = replica::S_UP;
	svc_state_list.list[1].state = replica::S_UP;
	svc_state_list.list[2].state = replica::S_UP;

	//
	// By faulting on service_2, we force service 1 to have proceeded
	// and service_3 to be waiting of service_2
	//
	rslt = dep_switchover_helper(
	    fault_dep_1_g_on,
	    fault_dep_1_g_off,
	    get_sid("service_2"),
	    get_secondary(1, "service_1"),
	    false,
	    &svc_state_list);

	delete []svc_state_list.list;
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Dependency 3-H Test case
//
int
dep_3_h(int, char *[])
{
	os::printf("*** Dependency Scenario 3 Fault H\n");

#ifdef _FAULT_INJECTION

	svc_state_list_t	svc_state_list;
	int			rslt;

	svc_state_list.listlen = 3;
	svc_state_list.bottom_svc = 0;
	svc_state_list.top_svc = 2;
	svc_state_list.list = new svc_state_t[3];
	os::sprintf(svc_state_list.list[0].name, "service_1");
	os::sprintf(svc_state_list.list[1].name, "service_2");
	os::sprintf(svc_state_list.list[2].name, "service_3");
	svc_state_list.list[0].state = replica::S_UP;
	svc_state_list.list[1].state = replica::S_UP;
	svc_state_list.list[2].state = replica::S_UP;

	rslt = dep_switchover_helper(
	    fault_dep_1_h_on,
	    fault_dep_1_h_off,
	    get_sid("service_2"),
	    get_secondary(1, "service_1"),
	    false,
	    &svc_state_list);

	delete []svc_state_list.list;
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Dependency 3-I Test case
//
// Not Implemented


//
// Dependency 3-J Test case
//
int
dep_3_j(int, char *[])
{
	os::printf("*** Dependency Scenario 3 Fault J\n");

#ifdef _FAULT_INJECTION

	svc_state_list_t	svc_state_list;
	int			rslt;

	svc_state_list.listlen = 3;
	svc_state_list.bottom_svc = 0;
	svc_state_list.top_svc = 2;
	svc_state_list.list = new svc_state_t[3];
	os::sprintf(svc_state_list.list[0].name, "service_1");
	os::sprintf(svc_state_list.list[1].name, "service_2");
	os::sprintf(svc_state_list.list[2].name, "service_3");
	svc_state_list.list[0].state = replica::S_UP;
	svc_state_list.list[1].state = replica::S_UP;
	svc_state_list.list[2].state = replica::S_UP;

	rslt = dep_switchover_helper(
	    fault_dep_1_j_on,
	    fault_dep_1_j_off,
	    get_sid("service_2"),
	    get_secondary(1, "service_1"),
	    false,
	    &svc_state_list);

	delete []svc_state_list.list;
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Dependency 3-M Test case
//
int
dep_3_m(int, char *[])
{
	os::printf("*** Dependency Scenario 3 Fault M\n");

#ifdef _FAULT_INJECTION
	nodeid_t	primary_nodeid;

	// Verify that the services are up
	if (get_state("service_1") != replica::S_UP) {
		os::printf("+++ FAIL: service_1 is not in S_UP state\n");
		return (1);
	}
	if (get_state("service_2") != replica::S_UP) {
		os::printf("+++ FAIL: service_2 is not in S_UP state\n");
		return (1);
	}
	if (get_state("service_3") != replica::S_UP) {
		os::printf("+++ FAIL: service_3 is not in S_UP state\n");
		return (1);
	}
	primary_nodeid = get_primary("service_1");

	// Fail over the service by killing the primary node
	FaultFunctions::wait_for_arg_t	wait_arg;
	wait_arg.op	= FaultFunctions::WAIT_FOR_DEAD;
	wait_arg.nodeid	= get_primary("service_1");

	os::printf(" ** Killing node: %d\n", wait_arg.nodeid);
	FaultFunctions::reboot(0, &wait_arg, (uint32_t)sizeof (wait_arg));

	os::printf(" ** Waiting for reconfig to finish... ");
	while (get_primary("service_1") == primary_nodeid) {
		os::printf(".");
		os::usecsleep(1 * 1000000);
	}
	os::printf(" done\n");

	// Verify that service_1 is still up
	if (get_state("service_1") != replica::S_UP) {
		os::printf("+++ FAIL: service_1 is not in S_UP state\n");
		return (1);
	}

	// Verify that service_2 is still up
	if (get_state("service_2") != replica::S_UP) {
		os::printf("+++ FAIL: service_2 is not in S_UP state\n");
		return (1);
	}

	// Verify that service_3 is still up
	if (get_state("service_3") != replica::S_UP) {
		os::printf("+++ FAIL: service_3 is not in S_UP state\n");
		return (1);
	}

	os::printf("--- PASS: Failover was succesful\n");

	return (0);
#else
	return (no_fault_injection());
#endif
}

//
// Dependency 3-N Test case
//
int
dep_3_n(int, char *[])
{
	os::printf("*** Dependency Scenario 3 Fault N\n");

#ifdef _FAULT_INJECTION
	nodeid_t	primary_nodeid;

	// Verify that the services are up
	if (get_state("service_1") != replica::S_UP) {
		os::printf("+++ FAIL: service_1 is not in S_UP state\n");
		return (1);
	}
	if (get_state("service_2") != replica::S_UP) {
		os::printf("+++ FAIL: service_2 is not in S_UP state\n");
		return (1);
	}
	if (get_state("service_3") != replica::S_UP) {
		os::printf("+++ FAIL: service_3 is not in S_UP state\n");
		return (1);
	}
	primary_nodeid = get_primary("service_1");

	// We will fail over the service, but no secondaries will exist for
	// service_2 so it will go down

	// Fail over the service by killing the primary node
	FaultFunctions::wait_for_arg_t	wait_arg;
	wait_arg.op	= FaultFunctions::WAIT_FOR_DEAD;
	wait_arg.nodeid	= get_primary("service_1");

	os::printf(" ** Killing node: %d\n", wait_arg.nodeid);
	FaultFunctions::reboot(0, &wait_arg, (uint32_t)sizeof (wait_arg));

	os::printf(" ** Waiting for reconfig to finish... ");
	while (get_primary("service_1") == primary_nodeid) {
		os::printf(".");
		os::usecsleep(1 * 1000000);
	}
	os::printf(" done\n");

	// Verify that service_1 is still up
	if (get_state("service_1") != replica::S_UP) {
		os::printf("+++ FAIL: service_1 is not in S_UP state\n");
		return (1);
	}

	// Verify that service_2 is still up
	if (get_state("service_2") != replica::S_UP) {
		os::printf("+++ FAIL: service_2 is not in S_UP state\n");
		return (1);
	}

	// Verify that service_3 is now down
	if (get_state("service_3") != replica::S_DOWN) {
		os::printf("+++ FAIL: service_3 is not in S_DOWN state\n");
		return (1);
	}

	os::printf("--- PASS: Failover was succesful\n");

	return (0);
#else
	return (no_fault_injection());
#endif
}


//
// Scenario 4
//
// service_3 depends on service_1
// service_2 depends on service_1
// service_1
//

//
// Dependency 4-A Test case
//
int
dep_4_a(int, char *[])
{
	os::printf("*** Dependency Scenario 4 Fault A\n");

#ifdef _FAULT_INJECTION
	svc_state_list_t	svc_state_list;
	int			rslt;

	svc_state_list.listlen = 3;
	svc_state_list.bottom_svc = 0;
	svc_state_list.top_svc = 2;
	svc_state_list.list = new svc_state_t[3];
	os::sprintf(svc_state_list.list[0].name, "service_1");
	os::sprintf(svc_state_list.list[1].name, "service_2");
	os::sprintf(svc_state_list.list[2].name, "service_3");
	svc_state_list.list[0].state = replica::S_UP;
	svc_state_list.list[1].state = replica::S_UP;
	svc_state_list.list[2].state = replica::S_UP;

	rslt = dep_switchover_helper(
	    fault_dep_1_a_on,
	    fault_dep_1_a_off,
	    0,
	    0,
	    true,
	    &svc_state_list);

	delete []svc_state_list.list;
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Dependency 4-B Test case
//
int
dep_4_b(int, char *[])
{
	os::printf("*** Dependency Scenario 4 Fault B\n");

#ifdef _FAULT_INJECTION

	svc_state_list_t	svc_state_list;
	int			rslt;

	svc_state_list.listlen = 3;
	svc_state_list.bottom_svc = 0;
	svc_state_list.top_svc = 2;
	svc_state_list.list = new svc_state_t[3];
	os::sprintf(svc_state_list.list[0].name, "service_1");
	os::sprintf(svc_state_list.list[1].name, "service_2");
	os::sprintf(svc_state_list.list[2].name, "service_3");
	svc_state_list.list[0].state = replica::S_UP;
	svc_state_list.list[1].state = replica::S_UP;
	svc_state_list.list[2].state = replica::S_UP;

	rslt = dep_switchover_helper(
	    fault_dep_1_b_on,
	    fault_dep_1_b_off,
	    get_sid("service_3"),
	    get_primary("service_1"),
	    true,
	    &svc_state_list);

	delete []svc_state_list.list;
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

//
// Dependency 4-C Test case
//
// Not Implmented

//
// Dependency 4-D Test case
//
int
dep_4_d(int, char *[])
{
	os::printf("*** Dependency Scenario 4 Fault D\n");

#ifdef _FAULT_INJECTION

	svc_state_list_t	svc_state_list;
	int			rslt;

	svc_state_list.listlen = 3;
	svc_state_list.bottom_svc = 0;
	svc_state_list.top_svc = 2;
	svc_state_list.list = new svc_state_t[3];
	os::sprintf(svc_state_list.list[0].name, "service_1");
	os::sprintf(svc_state_list.list[1].name, "service_2");
	os::sprintf(svc_state_list.list[2].name, "service_3");
	svc_state_list.list[0].state = replica::S_UP;
	svc_state_list.list[1].state = replica::S_UP;
	svc_state_list.list[2].state = replica::S_UP;

	rslt = dep_switchover_helper(
	    fault_dep_1_d_on,
	    fault_dep_1_d_off,
	    get_sid("service_3"),
	    get_primary("service_1"),
	    true,
	    &svc_state_list);

	delete []svc_state_list.list;
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Dependency 4-E Test case
//
// Not Implmented


//
// Dependency 4-F Test case
//
// Not Implmented


//
// Dependency 4-G Test case
//
int
dep_4_g(int, char *[])
{
	os::printf("*** Dependency Scenario 4 Fault G\n");

#ifdef _FAULT_INJECTION

	svc_state_list_t	svc_state_list;
	int			rslt;

	svc_state_list.listlen = 3;
	svc_state_list.bottom_svc = 0;
	svc_state_list.top_svc = 2;
	svc_state_list.list = new svc_state_t[3];
	os::sprintf(svc_state_list.list[0].name, "service_1");
	os::sprintf(svc_state_list.list[1].name, "service_2");
	os::sprintf(svc_state_list.list[2].name, "service_3");
	svc_state_list.list[0].state = replica::S_UP;
	svc_state_list.list[1].state = replica::S_UP;
	svc_state_list.list[2].state = replica::S_UP;

	rslt = dep_switchover_helper(
	    fault_dep_1_g_on,
	    fault_dep_1_g_off,
	    get_sid("service_1"),
	    get_secondary(1, "service_1"),
	    false,
	    &svc_state_list);

	delete []svc_state_list.list;
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Dependency 4-H Test case
//
int
dep_4_h(int, char *[])
{
	os::printf("*** Dependency Scenario 4 Fault H\n");

#ifdef _FAULT_INJECTION

	svc_state_list_t	svc_state_list;
	int			rslt;

	svc_state_list.listlen = 3;
	svc_state_list.bottom_svc = 0;
	svc_state_list.top_svc = 2;
	svc_state_list.list = new svc_state_t[3];
	os::sprintf(svc_state_list.list[0].name, "service_1");
	os::sprintf(svc_state_list.list[1].name, "service_2");
	os::sprintf(svc_state_list.list[2].name, "service_3");
	svc_state_list.list[0].state = replica::S_UP;
	svc_state_list.list[1].state = replica::S_UP;
	svc_state_list.list[2].state = replica::S_UP;

	rslt = dep_switchover_helper(
	    fault_dep_1_h_on,
	    fault_dep_1_h_off,
	    get_sid("service_1"),
	    get_secondary(1, "service_1"),
	    false,
	    &svc_state_list);

	delete []svc_state_list.list;
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Dependency 4-I Test case
//
// Not Implemented


//
// Dependency 4-J Test case
//
int
dep_4_j(int, char *[])
{
	os::printf("*** Dependency Scenario 4 Fault J\n");

#ifdef _FAULT_INJECTION

	svc_state_list_t	svc_state_list;
	int			rslt;

	svc_state_list.listlen = 3;
	svc_state_list.bottom_svc = 0;
	svc_state_list.top_svc = 2;
	svc_state_list.list = new svc_state_t[3];
	os::sprintf(svc_state_list.list[0].name, "service_1");
	os::sprintf(svc_state_list.list[1].name, "service_2");
	os::sprintf(svc_state_list.list[2].name, "service_3");
	svc_state_list.list[0].state = replica::S_UP;
	svc_state_list.list[1].state = replica::S_UP;
	svc_state_list.list[2].state = replica::S_UP;

	rslt = dep_switchover_helper(
	    fault_dep_1_j_on,
	    fault_dep_1_j_off,
	    get_sid("service_1"),
	    get_secondary(1, "service_1"),
	    false,
	    &svc_state_list);

	delete []svc_state_list.list;
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Dependency 4-M Test case
//
int
dep_4_m(int, char *[])
{
	os::printf("*** Dependency Scenario 4 Fault M\n");

#ifdef _FAULT_INJECTION
	nodeid_t	primary_nodeid;

	// Verify that the services are up
	if (get_state("service_1") != replica::S_UP) {
		os::printf("+++ FAIL: service_1 is not in S_UP state\n");
		return (1);
	}
	if (get_state("service_2") != replica::S_UP) {
		os::printf("+++ FAIL: service_2 is not in S_UP state\n");
		return (1);
	}
	if (get_state("service_3") != replica::S_UP) {
		os::printf("+++ FAIL: service_3 is not in S_UP state\n");
		return (1);
	}
	primary_nodeid = get_primary("service_1");

	// Fail over the service by killing the primary node
	FaultFunctions::wait_for_arg_t	wait_arg;
	wait_arg.op	= FaultFunctions::WAIT_FOR_DEAD;
	wait_arg.nodeid	= get_primary("service_1");

	os::printf(" ** Killing node: %d\n", wait_arg.nodeid);
	FaultFunctions::reboot(0, &wait_arg, (uint32_t)sizeof (wait_arg));

	os::printf(" ** Waiting for reconfig to finish... ");
	while (get_primary("service_1") == primary_nodeid) {
		os::printf(".");
		os::usecsleep(1 * 1000000);
	}
	os::printf(" done\n");

	// Verify that service_1 is still up
	if (get_state("service_1") != replica::S_UP) {
		os::printf("+++ FAIL: service_1 is not in S_UP state\n");
		return (1);
	}

	// Verify that service_2 is still up
	if (get_state("service_2") != replica::S_UP) {
		os::printf("+++ FAIL: service_2 is not in S_UP state\n");
		return (1);
	}

	// Verify that service_3 is still up
	if (get_state("service_3") != replica::S_UP) {
		os::printf("+++ FAIL: service_3 is not in S_UP state\n");
		return (1);
	}

	os::printf("--- PASS: Failover was succesful\n");

	return (0);
#else
	return (no_fault_injection());
#endif
}

//
// Dependency 4-N Test case
//
int
dep_4_n(int, char *[])
{
	os::printf("*** Dependency Scenario 4 Fault N\n");

#ifdef _FAULT_INJECTION
	nodeid_t	primary_nodeid;

	// Verify that the services are up
	if (get_state("service_1") != replica::S_UP) {
		os::printf("+++ FAIL: service_1 is not in S_UP state\n");
		return (1);
	}
	if (get_state("service_2") != replica::S_UP) {
		os::printf("+++ FAIL: service_2 is not in S_UP state\n");
		return (1);
	}
	if (get_state("service_3") != replica::S_UP) {
		os::printf("+++ FAIL: service_3 is not in S_UP state\n");
		return (1);
	}
	primary_nodeid = get_primary("service_1");

	// We will fail over the service, but there wont be another
	// node with the full 1,2,3 svc dependency, only 1,2 or 1,3 svc
	// dependencies.. who will survive?

	// Fail over the service by killing the primary node
	FaultFunctions::wait_for_arg_t	wait_arg;
	wait_arg.op	= FaultFunctions::WAIT_FOR_DEAD;
	wait_arg.nodeid	= get_primary("service_1");

	os::printf(" ** Killing node: %d\n", wait_arg.nodeid);
	FaultFunctions::reboot(0, &wait_arg, (uint32_t)sizeof (wait_arg));

	os::printf(" ** Waiting for reconfig to finish... ");
	while (get_primary("service_1") == primary_nodeid) {
		os::printf(".");
		os::usecsleep(1 * 1000000);
	}
	os::printf(" done\n");

	// Verify that service_1 is still up
	if (get_state("service_1") != replica::S_UP) {
		os::printf("+++ FAIL: service_1 is not in S_UP state\n");
		return (1);
	}

	// Verify that exactly one of services 2 and 3 is still up.
	replica::service_state s2_state =
	    (replica::service_state)get_state("service_2");
	replica::service_state s3_state =
	    (replica::service_state)get_state("service_3");
	if (s2_state == replica::S_DOWN && s3_state == replica::S_DOWN) {
		os::printf("+++ FAIL: service_2 and service_3 are both in "
		    "S_DOWN state\n");
		return (1);
	}
	if (s2_state == replica::S_UP && s3_state == replica::S_UP) {
		os::printf("+++ FAIL: service_2 and service_3 are both in "
		    "S_UP state\n");
		return (1);
	}

	os::printf("--- PASS: Failover was succesful\n");

	return (0);
#else
	return (no_fault_injection());
#endif
}


//
// Scenario 5
//
// service_3 depends on service_1 and service_2
// service_4 depends on service_2
// service_2
// service_1
//

//
// Dependency 5-A Test case
//
int
dep_5_a(int, char *[])
{
	os::printf("*** Dependency Scenario 5 Fault A\n");

#ifdef _FAULT_INJECTION
	svc_state_list_t	svc_state_list;
	int			rslt;

	svc_state_list.listlen = 4;
	svc_state_list.bottom_svc = 0;
	svc_state_list.top_svc = 3;
	svc_state_list.list = new svc_state_t[4];
	os::sprintf(svc_state_list.list[0].name, "service_1");
	os::sprintf(svc_state_list.list[1].name, "service_2");
	os::sprintf(svc_state_list.list[2].name, "service_3");
	os::sprintf(svc_state_list.list[3].name, "service_4");
	svc_state_list.list[0].state = replica::S_UP;
	svc_state_list.list[1].state = replica::S_UP;
	svc_state_list.list[2].state = replica::S_UP;
	svc_state_list.list[3].state = replica::S_UP;

	rslt = dep_switchover_helper(
	    fault_dep_1_a_on,
	    fault_dep_1_a_off,
	    0,
	    0,
	    true,
	    &svc_state_list);

	delete []svc_state_list.list;
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Dependency 5-B Test case
//
int
dep_5_b(int, char *[])
{
	os::printf("*** Dependency Scenario 5 Fault B\n");

#ifdef _FAULT_INJECTION

	svc_state_list_t	svc_state_list;
	int			rslt;

	svc_state_list.listlen = 4;
	svc_state_list.bottom_svc = 0;
	svc_state_list.top_svc = 3;
	svc_state_list.list = new svc_state_t[4];
	os::sprintf(svc_state_list.list[0].name, "service_1");
	os::sprintf(svc_state_list.list[1].name, "service_2");
	os::sprintf(svc_state_list.list[2].name, "service_3");
	os::sprintf(svc_state_list.list[3].name, "service_4");
	svc_state_list.list[0].state = replica::S_UP;
	svc_state_list.list[1].state = replica::S_UP;
	svc_state_list.list[2].state = replica::S_UP;
	svc_state_list.list[3].state = replica::S_UP;

	rslt = dep_switchover_helper(
	    fault_dep_1_b_on,
	    fault_dep_1_b_off,
	    get_sid("service_4"),
	    get_primary("service_1"),
	    true,
	    &svc_state_list);

	delete []svc_state_list.list;
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

//
// Dependency 5-C Test case
//
// Not Implmented

//
// Dependency 5-D Test case
//
int
dep_5_d(int, char *[])
{
	os::printf("*** Dependency Scenario 5 Fault D\n");

#ifdef _FAULT_INJECTION

	svc_state_list_t	svc_state_list;
	int			rslt;

	svc_state_list.listlen = 4;
	svc_state_list.bottom_svc = 0;
	svc_state_list.top_svc = 3;
	svc_state_list.list = new svc_state_t[4];
	os::sprintf(svc_state_list.list[0].name, "service_1");
	os::sprintf(svc_state_list.list[1].name, "service_2");
	os::sprintf(svc_state_list.list[2].name, "service_3");
	os::sprintf(svc_state_list.list[3].name, "service_4");
	svc_state_list.list[0].state = replica::S_UP;
	svc_state_list.list[1].state = replica::S_UP;
	svc_state_list.list[2].state = replica::S_UP;
	svc_state_list.list[3].state = replica::S_UP;

	rslt = dep_switchover_helper(
	    fault_dep_1_d_on,
	    fault_dep_1_d_off,
	    get_sid("service_4"),
	    get_primary("service_1"),
	    true,
	    &svc_state_list);

	delete []svc_state_list.list;
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Dependency 5-E Test case
//
// Not Implmented


//
// Dependency 5-F Test case
//
// Not Implmented


//
// Dependency 5-G Test case
//
int
dep_5_g(int, char *[])
{
	os::printf("*** Dependency Scenario 5 Fault G\n");

#ifdef _FAULT_INJECTION

	svc_state_list_t	svc_state_list;
	int			rslt;

	svc_state_list.listlen = 4;
	svc_state_list.bottom_svc = 0;
	svc_state_list.top_svc = 3;
	svc_state_list.list = new svc_state_t[4];
	os::sprintf(svc_state_list.list[0].name, "service_1");
	os::sprintf(svc_state_list.list[1].name, "service_2");
	os::sprintf(svc_state_list.list[2].name, "service_3");
	os::sprintf(svc_state_list.list[3].name, "service_4");
	svc_state_list.list[0].state = replica::S_UP;
	svc_state_list.list[1].state = replica::S_UP;
	svc_state_list.list[2].state = replica::S_UP;
	svc_state_list.list[3].state = replica::S_UP;

	rslt = dep_switchover_helper(
	    fault_dep_1_g_on,
	    fault_dep_1_g_off,
	    get_sid("service_2"),
	    get_secondary(1, "service_1"),
	    false,
	    &svc_state_list);

	delete []svc_state_list.list;
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Dependency 5-H Test case
//
int
dep_5_h(int, char *[])
{
	os::printf("*** Dependency Scenario 5 Fault H\n");

#ifdef _FAULT_INJECTION

	svc_state_list_t	svc_state_list;
	int			rslt;

	svc_state_list.listlen = 4;
	svc_state_list.bottom_svc = 0;
	svc_state_list.top_svc = 3;
	svc_state_list.list = new svc_state_t[4];
	os::sprintf(svc_state_list.list[0].name, "service_1");
	os::sprintf(svc_state_list.list[1].name, "service_2");
	os::sprintf(svc_state_list.list[2].name, "service_3");
	os::sprintf(svc_state_list.list[3].name, "service_4");
	svc_state_list.list[0].state = replica::S_UP;
	svc_state_list.list[1].state = replica::S_UP;
	svc_state_list.list[2].state = replica::S_UP;
	svc_state_list.list[3].state = replica::S_UP;

	rslt = dep_switchover_helper(
	    fault_dep_1_h_on,
	    fault_dep_1_h_off,
	    get_sid("service_2"),
	    get_secondary(1, "service_1"),
	    false,
	    &svc_state_list);

	delete []svc_state_list.list;
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Dependency 5-I Test case
//
// Not Implemented


//
// Dependency 5-J Test case
//
int
dep_5_j(int, char *[])
{
	os::printf("*** Dependency Scenario 5 Fault J\n");

#ifdef _FAULT_INJECTION

	svc_state_list_t	svc_state_list;
	int			rslt;

	svc_state_list.listlen = 4;
	svc_state_list.bottom_svc = 0;
	svc_state_list.top_svc = 3;
	svc_state_list.list = new svc_state_t[4];
	os::sprintf(svc_state_list.list[0].name, "service_1");
	os::sprintf(svc_state_list.list[1].name, "service_2");
	os::sprintf(svc_state_list.list[2].name, "service_3");
	os::sprintf(svc_state_list.list[3].name, "service_4");
	svc_state_list.list[0].state = replica::S_UP;
	svc_state_list.list[1].state = replica::S_UP;
	svc_state_list.list[2].state = replica::S_UP;
	svc_state_list.list[3].state = replica::S_UP;

	rslt = dep_switchover_helper(
	    fault_dep_1_j_on,
	    fault_dep_1_j_off,
	    get_sid("service_2"),
	    get_secondary(1, "service_1"),
	    false,
	    &svc_state_list);

	delete []svc_state_list.list;
	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Dependency 5-M Test case
//
int
dep_5_m(int, char *[])
{
	os::printf("*** Dependency Scenario 5 Fault M\n");

#ifdef _FAULT_INJECTION
	nodeid_t	primary_nodeid;

	// Verify that the services are up
	if (get_state("service_1") != replica::S_UP) {
		os::printf("+++ FAIL: service_1 is not in S_UP state\n");
		return (1);
	}
	if (get_state("service_2") != replica::S_UP) {
		os::printf("+++ FAIL: service_2 is not in S_UP state\n");
		return (1);
	}
	if (get_state("service_3") != replica::S_UP) {
		os::printf("+++ FAIL: service_3 is not in S_UP state\n");
		return (1);
	}
	if (get_state("service_4") != replica::S_UP) {
		os::printf("+++ FAIL: service_4 is not in S_UP state\n");
		return (1);
	}
	primary_nodeid = get_primary("service_1");

	// Fail over the service by killing the primary node
	FaultFunctions::wait_for_arg_t	wait_arg;
	wait_arg.op	= FaultFunctions::WAIT_FOR_DEAD;
	wait_arg.nodeid	= get_primary("service_1");

	os::printf(" ** Killing node: %d\n", wait_arg.nodeid);
	FaultFunctions::reboot(0, &wait_arg, (uint32_t)sizeof (wait_arg));

	os::printf(" ** Waiting for reconfig to finish... ");
	while (get_primary("service_1") == primary_nodeid) {
		os::printf(".");
		os::usecsleep(1 * 1000000);
	}
	os::printf(" done\n");

	// Verify that service_1 is still up
	if (get_state("service_1") != replica::S_UP) {
		os::printf("+++ FAIL: service_1 is not in S_UP state\n");
		return (1);
	}

	// Verify that service_2 is still up
	if (get_state("service_2") != replica::S_UP) {
		os::printf("+++ FAIL: service_2 is not in S_UP state\n");
		return (1);
	}

	// Verify that service_3 is still up
	if (get_state("service_3") != replica::S_UP) {
		os::printf("+++ FAIL: service_3 is not in S_UP state\n");
		return (1);
	}

	// Verify that service_4 is still up
	if (get_state("service_4") != replica::S_UP) {
		os::printf("+++ FAIL: service_4 is not in S_UP state\n");
		return (1);
	}

	os::printf("--- PASS: Failover was succesful\n");

	return (0);
#else
	return (no_fault_injection());
#endif
}

//
// Dependency 5-N Test case
//
int
dep_5_n(int, char *[])
{
	os::printf("*** Dependency Scenario 5 Fault N\n");

#ifdef _FAULT_INJECTION
	nodeid_t	primary_nodeid;

	// Verify that the services are up
	if (get_state("service_1") != replica::S_UP) {
		os::printf("+++ FAIL: service_1 is not in S_UP state\n");
		return (1);
	}
	if (get_state("service_2") != replica::S_UP) {
		os::printf("+++ FAIL: service_2 is not in S_UP state\n");
		return (1);
	}
	if (get_state("service_3") != replica::S_UP) {
		os::printf("+++ FAIL: service_3 is not in S_UP state\n");
		return (1);
	}
	if (get_state("service_4") != replica::S_UP) {
		os::printf("+++ FAIL: service_4 is not in S_UP state\n");
		return (1);
	}
	primary_nodeid = get_primary("service_1");

	// We will fail over the service, but there wont be another
	// node with the full 1,2,3,4 svc dependency, only 1,2,3 or 2,4 svc
	// dependencies.. who will survive?

	// Fail over the service by killing the primary node
	FaultFunctions::wait_for_arg_t	wait_arg;
	wait_arg.op	= FaultFunctions::WAIT_FOR_DEAD;
	wait_arg.nodeid	= get_primary("service_1");

	os::printf(" ** Killing node: %d\n", wait_arg.nodeid);
	FaultFunctions::reboot(0, &wait_arg, (uint32_t)sizeof (wait_arg));

	os::printf(" ** Waiting for reconfig to finish... ");
	while (get_primary("service_1") == primary_nodeid) {
		os::printf(".");
		os::usecsleep(1 * 1000000);
	}
	os::printf(" done\n");

	// XXX - Finish verification

	// Verify that service_1 is still up
	if (get_state("service_1") != replica::S_UP) {
		os::printf("+++ FAIL: service_1 is not in S_UP state\n");
		return (1);
	}

	// Verify that service_2 is still up
	if (get_state("service_2") != replica::S_UP) {
		os::printf("+++ FAIL: service_2 is not in S_UP state\n");
		return (1);
	}

	// Verify that exactly one of services 3 and 4 is still up
	replica::service_state s3_state =
	    (replica::service_state)get_state("service_3");
	replica::service_state s4_state =
	    (replica::service_state)get_state("service_4");
	if (s3_state == replica::S_DOWN) {
		os::printf("+++ FAIL: service_3 is in S_DOWN state\n");
		return (1);
	}
	if (s4_state == replica::S_DOWN) {
		os::printf("+++ FAIL: service_4 is in S_DOWN state\n");
		return (1);
	}

	os::printf("--- PASS: Failover was succesful\n");

	return (0);
#else
	return (no_fault_injection());
#endif
}
