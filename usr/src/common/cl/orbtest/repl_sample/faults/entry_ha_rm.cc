/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)entry_ha_rm.cc	1.18	08/05/20 SMI"

#include	<orbtest/repl_sample/repl_sample_clnt.h>
#include	<sys/os.h>
#include	<orb/invo/common.h>
#include	<orbtest/rm_probe/rm_snap.h>
#include	<orb/infrastructure/orb_conf.h>

#ifdef _KERNEL_ORB
#include	<orbtest/repl_sample/repl_sample_impl.h>
#endif

#include	<orbtest/repl_sample/faults/entry_ha_rm.h>
#include	<orbtest/repl_sample/faults/fault_ha_rm.h>

//
// (HA-RM) State Machine Basic Test case
//
// This concerns the fault point in the rm_repl_service::iterate_over fn.
// It expects as of now, exactly 1 arg to be compared by the fault
// fn. to the fn_name param in the iterate_over fn. This is the 'context'
// string, and could be one of the following:
//	.freeze_service_invos
//	.unfreeze_service_invos
//	.disconnect_clients
//
// The gist of what it does is making sure the primary RM faults while
// a change_sec_state is done - and, the primary faults iff (if and only if)
// the 'context' is correct.
//
int
state_mach_basic(int arglen, char **argp)
{
	os::printf("*** HA-RM State Machine Basic Test Case ***\n");

#ifdef _FAULT_INJECTION

#ifdef _KERNEL_ORB

	FaultFunctions::ha_rm_reboot_arg_t reboot_arg;
	int rslt;
	Environment e;

	ASSERT(arglen == 2);

	reboot_arg.nodeid = FaultFunctions::rm_primary_node();
	reboot_arg.op = FaultFunctions::WAIT_FOR_UNKNOWN;
	os::strcpy(reboot_arg.fn_name, argp[1]);
	reboot_arg.sid = get_sid("ha_sample_server");

	rslt = change_sec_state(
		fault_ha_rm_iterateover_on,
		fault_ha_rm_iterateover_off,
		&reboot_arg,
		(int)sizeof (reboot_arg),
		"ha_sample_server",
		replica::SC_SET_PRIMARY,
		e);

	if (e.exception()) {
		e.exception()->print_exception("change_sec_state()");
		os::printf("+++ FAIL: Exception was not expected\n");
		return (1);
	}

	return (rslt);

#else
	arglen = 0;
	argp = NULL;
	os::printf("+++ ERROR: Tests only supported in kernel mode testing\n");
	return (1);

#endif // _KERNEL_ORB
#else
	arglen = 0;
	argp = NULL;
	return (no_fault_injection());

#endif // _FAULT_INJECTION

}

//
// (HA-RM) State Machine connect_to_primary fault Test case
//
// Fault point is in connect_to_primary(). This point in code is hit
// by the primary RM when a change_sec_state (switchover) of the dummy
// service is initiated by the test.
//
int
state_mach_connectprimary(int, char **)
{
	os::printf("*** HA-RM State Machine ConnectPrimary Test Case ***\n");

#ifdef _FAULT_INJECTION

#ifdef _KERNEL_ORB
	FaultFunctions::ha_rm_reboot_arg_t reboot_arg;
	int rslt;
	Environment e;

	reboot_arg.nodeid = FaultFunctions::rm_primary_node();
	reboot_arg.op = FaultFunctions::WAIT_FOR_UNKNOWN;
	os::strcpy(reboot_arg.fn_name, "connect_to_primary");
	reboot_arg.sid = get_sid("ha_sample_server");

	rslt = change_sec_state(
		fault_ha_rm_connectprimary_on,
		fault_ha_rm_connectprimary_off,
		&reboot_arg,
		(int)sizeof (reboot_arg),
		"ha_sample_server",
		replica::SC_SET_PRIMARY,
		e);

	if (e.exception()) {
		e.exception()->print_exception("change_sec_state()");
		os::printf("+++ FAIL: Exception was not expected\n");
		return (1);
	}

	return (rslt);

#else

	os::printf("+++ ERROR: Tests only supported in kernel mode testing\n");
	return (1);

#endif // _KERNEL_ORB

#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

//
// (HA-RM) State Machine Shutdown Case - Fault Pt. in iterate_over
//
// Test case when the iterate_over() fault pt. is hit in the
// "shutdown_service" context. The test goes to the context by calling
// shutdown_service(), a wrapper around an identical named service_admin
// interface.
//
int
state_mach_shutdown(int, char **)
{
	os::printf("*** HA-RM Shutdown_Service Test Case ***\n");

#ifdef _FAULT_INJECTION

#ifdef _KERNEL_ORB

	FaultFunctions::ha_rm_reboot_arg_t reboot_arg;
	int rslt;
	Environment e;

	reboot_arg.nodeid = FaultFunctions::rm_primary_node();
	reboot_arg.op = FaultFunctions::WAIT_FOR_UNKNOWN;
	os::strcpy(reboot_arg.fn_name, "mark_down");
	reboot_arg.sid = get_sid("ha_sample_server");

	rslt = shutdown_service(
		fault_ha_rm_iterateover_on,
		fault_ha_rm_iterateover_off,
		&reboot_arg,
		(int)sizeof (reboot_arg),
		"ha_sample_server",
		e);

	if (e.exception()) {
		e.exception()->print_exception("shutdown_service");
		os::printf("+++ FAIL: Exception was not expected\n");
		return (1);
	}

	return (rslt);

#else

	os::printf("+++ ERROR: Tests only supported in kernel mode testing\n");
	return (1);

#endif // _KERNEL_ORB

#else
	return (no_fault_injection());
#endif
}

//
// (HA-RM) State Machine Shutdown Case - Fault Pt. in iterate_over
//
// Another test case pertaining to the iterate_over() fault pt. It covers
// the specific case of a "mark_down" context, which is arrived at if
// all the providers for a particular service die. In the test, we start
// with 1 provider - XXXX, is that too simplistic ?
//
int
state_mach_markdown(int, char **)
{
	os::printf("*** HA-RM Mark_Down Test Case ***\n");

#ifdef _FAULT_INJECTION

#ifdef _KERNEL_ORB

	FaultFunctions::ha_rm_reboot_arg_t reboot_arg;
	int rslt = 0;
	Environment e;

	nodeid_t primary_nodeid;
	primary_nodeid = get_primary("ha_sample_server", true);

	reboot_arg.nodeid = FaultFunctions::rm_primary_node();
	reboot_arg.op = FaultFunctions::WAIT_FOR_UNKNOWN;
	os::strcpy(reboot_arg.fn_name, "mark_down");
	reboot_arg.sid = get_sid("ha_sample_server");

	fault_ha_rm_iterateover_on(&reboot_arg, (int)sizeof (reboot_arg));

	FaultFunctions::wait_for_arg_t  wait_arg;

	wait_arg.op = FaultFunctions::WAIT_FOR_UNKNOWN;
	wait_arg.nodeid = primary_nodeid;
	FaultFunctions::reboot(0, &wait_arg, (uint32_t)sizeof (wait_arg));

	fault_ha_rm_iterateover_off(&reboot_arg, (int)sizeof (reboot_arg));
	return (rslt);
#else

	os::printf("+++ ERROR: Tests only supported in kernel mode testing\n");
	return (1);

#endif // _KERNEL_ORB

#else
	return (no_fault_injection());
#endif
}

// (HA-RM) Checkpoints RegisterService Case
//
//
int
chkpt_register_service_1(int, char **)
{
	os::printf("HA-RM ckpt_register_service Test Case 1 ***\n");

#ifdef _FAULT_INJECTION

#ifdef _KERNEL_ORB
	int rslt = 0;

	rslt = register_generic(
		fault_ha_rm_register_service_1_on,
		fault_ha_rm_register_service_1_off,
		NULL,
		0,
		"register_service");

	return (rslt);

#else
	os::printf("+++ ERROR: Tests only supported in kernel mode testing\n");
	return (1);
#endif // _KERNEL_ORB

#else
	return (no_fault_injection());
#endif
}

int
chkpt_register_service_2(int, char **)
{
	os::printf("HA-RM ckpt_register_service Test Case 2 ***\n");

#ifdef _FAULT_INJECTION

#ifdef _KERNEL_ORB
	int rslt = 0;

	rslt = register_generic(
		fault_ha_rm_register_service_2_on,
		fault_ha_rm_register_service_2_off,
		NULL,
		0,
		"register_service");

	return (rslt);

#else
	os::printf("+++ ERROR: Tests only supported in kernel mode testing\n");
	return (1);
#endif // _KERNEL_ORB

#else
	return (no_fault_injection());
#endif
}

// (HA-RM) Checkpoints AddRma Case
//
//
int
chkpt_add_rma_1(int, char **argp)
{
	os::printf("HA-RM ckpt_add_rma Test Case 1 ***\n");

#ifdef _FAULT_INJECTION

#ifdef _KERNEL_ORB
	FaultFunctions::ha_rm_reboot_arg_t reboot_arg;
	int rslt = 0;
	Environment e;
	nodeid_t primary = FaultFunctions::rm_primary_node();
	nodeid_t rma_to_add = (nodeid_t)os::atoi(argp[1]);

	reboot_arg.nodeid = primary;
	reboot_arg.op = FaultFunctions::WAIT_FOR_UNKNOWN;
	os::strcpy(reboot_arg.fn_name, "add_rma");

	fault_ha_rm_add_rma_1_on(&reboot_arg, (int)sizeof (reboot_arg));

	FaultFunctions::wait_for_arg_t wait_arg;
	wait_arg.op = FaultFunctions::WAIT_FOR_REJOIN;
	wait_arg.nodeid = rma_to_add;
	FaultFunctions::reboot(0, &wait_arg, (uint32_t)sizeof (wait_arg));

	// fault_ha_rm_add_rma_1_off(&reboot_arg, (int)sizeof (reboot_arg));

	rslt = change_sec_state(
		NULL,
		NULL,
		NULL,
		0,
		"ha_sample_server",
		replica::SC_SET_PRIMARY,
		e);

	if (e.exception()) {
		e.exception()->print_exception("change_sec_state()");
		os::printf("+++ FAIL: Exception was not expected\n");
		return (1);
	}

	return (rslt);

#else
	argp = NULL;
	os::printf("+++ ERROR: Tests only supported in kernel mode testing\n");
	return (1);
#endif // _KERNEL_ORB

#else
	argp = NULL;
	return (no_fault_injection());
#endif
}

// (HA-RM) Checkpoints AddRma Case
//
//
int
chkpt_add_rma_2(int, char **argp)
{
	os::printf("HA-RM ckpt_add_rma Test Case 2 ***\n");

#ifdef _FAULT_INJECTION

#ifdef _KERNEL_ORB
	FaultFunctions::ha_rm_reboot_arg_t reboot_arg;
	int rslt = 0;
	Environment e;
	nodeid_t primary = FaultFunctions::rm_primary_node();
	nodeid_t rma_to_add = (nodeid_t)os::atoi(argp[1]);

	reboot_arg.nodeid = primary;
	reboot_arg.op = FaultFunctions::WAIT_FOR_UNKNOWN;
	os::strcpy(reboot_arg.fn_name, "add_rma");

	fault_ha_rm_add_rma_2_on(&reboot_arg, (int)sizeof (reboot_arg));

	FaultFunctions::wait_for_arg_t wait_arg;
	wait_arg.op = FaultFunctions::WAIT_FOR_REJOIN;
	wait_arg.nodeid = rma_to_add;
	FaultFunctions::reboot(0, &wait_arg, (uint32_t)sizeof (wait_arg));

	// fault_ha_rm_add_rma_2_off(&reboot_arg, (int)sizeof (reboot_arg));

	rslt = change_sec_state(
		NULL,
		NULL,
		NULL,
		0,
		"ha_sample_server",
		replica::SC_SET_PRIMARY,
		e);

	if (e.exception()) {
		e.exception()->print_exception("change_sec_state()");
		os::printf("+++ FAIL: Exception was not expected\n");
		return (1);
	}

	return (rslt);

#else
	argp = NULL;
	os::printf("+++ ERROR: Tests only supported in kernel mode testing\n");
	return (1);
#endif // _KERNEL_ORB

#else
	argp = NULL;
	return (no_fault_injection());
#endif
}

// (HA-RM) Checkpoints RegisterRmaReplProv Case 1
//
//
int
chkpt_register_repl_prov_1(int, char **)
{
	os::printf("HA-RM ckpt_reg_rma_repl_prov Test Case 1 ***\n");

#ifdef _FAULT_INJECTION

#ifdef _KERNEL_ORB
	int rslt = 0;

	rslt = register_generic(
		fault_ha_rm_register_repl_prov_1_on,
		fault_ha_rm_register_repl_prov_1_off,
		NULL,
		0,
		"register_repl_prov");

	return (rslt);

#else
	os::printf("+++ ERROR: Tests only supported in kernel mode testing\n");
	return (1);
#endif // _KERNEL_ORB

#else
	return (no_fault_injection());
#endif
}

// (HA-RM) Checkpoints RegisterRmaReplProv Case 1
//
//
int
chkpt_register_repl_prov_2(int, char **)
{
	os::printf("HA-RM ckpt_reg_rma_repl_prov Test Case 2 ***\n");

#ifdef _FAULT_INJECTION

#ifdef _KERNEL_ORB
	int rslt = 0;

	rslt = register_generic(
		fault_ha_rm_register_repl_prov_2_on,
		fault_ha_rm_register_repl_prov_2_off,
		NULL,
		0,
		"register_repl_prov");

	return (rslt);

#else
	os::printf("+++ ERROR: Tests only supported in kernel mode testing\n");
	return (1);
#endif // _KERNEL_ORB

#else
	return (no_fault_injection());
#endif
}

// (HA-RM) Checkpoints GetControl Case
//
int
chkpt_get_control_1(int, char **)
{
	os::printf("*** HA-RM ckpt_get_control Test Case 1 ***\n");

#ifdef _FAULT_INJECTION

#ifdef _KERNEL_ORB

	FaultFunctions::ha_rm_reboot_arg_t reboot_arg;
	int rslt;
	Environment e;

	reboot_arg.nodeid = FaultFunctions::rm_primary_node();
	reboot_arg.op = FaultFunctions::WAIT_FOR_UNKNOWN;
	os::strcpy(reboot_arg.fn_name, "get_control");

	rslt = change_sec_state(
		fault_ha_rm_get_control_1_on,
		fault_ha_rm_get_control_1_off,
		&reboot_arg,
		(int)sizeof (reboot_arg),
		"ha_sample_server",
		replica::SC_SET_PRIMARY,
		e);

	if (e.exception()) {
		e.exception()->print_exception("change_sec_state()");
		os::printf("+++ FAIL: Exception was not expected\n");
		return (1);
	}

	return (rslt);

#else
	os::printf("+++ ERROR: Tests only supported in kernel mode testing\n");
	return (1);

#endif // _KERNEL_ORB
#else
	return (no_fault_injection());

#endif // _FAULT_INJECTION

}

// (HA-RM) Checkpoints GetControl Case
//
int
chkpt_get_control_2(int, char **)
{
	os::printf("*** HA-RM ckpt_get_control Test Case 2 ***\n");

#ifdef _FAULT_INJECTION

#ifdef _KERNEL_ORB

	FaultFunctions::ha_rm_reboot_arg_t reboot_arg;
	int rslt;
	Environment e;

	reboot_arg.nodeid = FaultFunctions::rm_primary_node();
	reboot_arg.op = FaultFunctions::WAIT_FOR_UNKNOWN;
	os::strcpy(reboot_arg.fn_name, "get_control");

	rslt = change_sec_state(
		fault_ha_rm_get_control_2_on,
		fault_ha_rm_get_control_2_off,
		&reboot_arg,
		(int)sizeof (reboot_arg),
		"ha_sample_server",
		replica::SC_SET_PRIMARY,
		e);

	if (e.exception()) {
		e.exception()->print_exception("change_sec_state()");
		os::printf("+++ FAIL: Exception was not expected\n");
		return (1);
	}

	return (rslt);

#else
	os::printf("+++ ERROR: Tests only supported in kernel mode testing\n");
	return (1);

#endif // _KERNEL_ORB
#else
	return (no_fault_injection());

#endif // _FAULT_INJECTION

}

// (HA-RM) Checkpoints ShutdownService Case
//
//
int
chkpt_shutdown_1(int, char **)
{
	os::printf("HA-RM ckpt_shutdown_service Test Case 2 ***\n");

#ifdef _FAULT_INJECTION

#ifdef _KERNEL_ORB
	FaultFunctions::ha_rm_reboot_arg_t reboot_arg;
	int rslt = 0;
	Environment e;

	reboot_arg.nodeid = FaultFunctions::rm_primary_node();
	reboot_arg.op = FaultFunctions::WAIT_FOR_UNKNOWN;
	os::strcpy(reboot_arg.fn_name, "shutdown_service");

	rslt = shutdown_service(
		fault_ha_rm_shutdown_service_2_on,
		fault_ha_rm_shutdown_service_2_off,
		&reboot_arg,
		(int)sizeof (reboot_arg),
		"ha_sample_server",
		e);

	if (e.exception()) {
		e.exception()->print_exception("shutdown_service");
		os::printf("+++ FAIL: exception was not expected\n");
		return (1);
	}

	return (rslt);

#else
	os::printf("+++ ERROR: Tests only supported in kernel mode testing\n");
	return (1);
#endif // _KERNEL_ORB

#else
	return (no_fault_injection());
#endif
}

// (HA-RM) Checkpoints ShutdownService Case
//
//
int
chkpt_shutdown_2(int, char **)
{
	os::printf("HA-RM ckpt_shutdown_service Test Case 2 ***\n");

#ifdef _FAULT_INJECTION

#ifdef _KERNEL_ORB
	FaultFunctions::ha_rm_reboot_arg_t reboot_arg;
	int rslt = 0;
	Environment e;

	reboot_arg.nodeid = FaultFunctions::rm_primary_node();
	reboot_arg.op = FaultFunctions::WAIT_FOR_UNKNOWN;
	os::strcpy(reboot_arg.fn_name, "shutdown_service");

	rslt = shutdown_service(
		fault_ha_rm_shutdown_service_2_on,
		fault_ha_rm_shutdown_service_2_off,
		&reboot_arg,
		(int)sizeof (reboot_arg),
		"ha_sample_server",
		e);

	if (e.exception()) {
		e.exception()->print_exception("shutdown_service");
		os::printf("+++ FAIL: exception was not expected\n");
		return (1);
	}

	return (rslt);

#else
	os::printf("+++ ERROR: Tests only supported in kernel mode testing\n");
	return (1);
#endif // _KERNEL_ORB

#else
	return (no_fault_injection());
#endif
}

//
// (HA-RM) State Machine AddService Case - Fault Pt. in iterate_over
//
// This reaches the "add_service" context in iterate_over() by creating
// a service, and then faults the primary.
//
int
state_mach_addservice(int, char **)
{
	os::printf("*** HA-RM Add_Service Test Case ***\n");

#ifdef _FAULT_INJECTION

#ifdef _KERNEL_ORB
	FaultFunctions::ha_rm_reboot_arg_t reboot_arg;
	int rslt = 0;
	Environment e;

	reboot_arg.nodeid = FaultFunctions::rm_primary_node();
	reboot_arg.op = FaultFunctions::WAIT_FOR_UNKNOWN;
	os::strcpy(reboot_arg.fn_name, "add_service");
	reboot_arg.sid = get_sid("ha_sample_server");

	fault_ha_rm_iterateover_on(&reboot_arg, (int)sizeof (reboot_arg));

	repl_sample_prov	*prov_ptr	= NULL;
	char			prov_desc[10];

	os::sprintf(prov_desc, "%d", orb_conf::node_number());
	prov_ptr = new repl_sample_prov("ha_sample_server",
	    (const char *)prov_desc);
	ASSERT(prov_ptr != NULL);
	prov_ptr->register_with_rm(1, NULL, NULL, true, e);

	if (e.exception()) {
		e.exception()->print_exception("add_service");
		os::printf("++ FAIL: exception was not expected\n");
		return (1);
	}

	fault_ha_rm_iterateover_off(&reboot_arg, (int)sizeof (reboot_arg));

	e.clear();

	rslt = shutdown_service_nowork("ha_sample_server", e, false);

	return (rslt);

#else

	os::printf("+++ ERROR: Tests only supported in kernel mode testing\n");
	return (1);

#endif // _KERNEL_ORB
#else
	return (no_fault_injection());
#endif
}

// (HA-RM) Checkpoints ShutdownService Case 3
//
//
int
chkpt_shutdown_3(int, char **)
{
	os::printf("HA-RM ckpt_shutdown_service Test Case 3 ***\n");

#ifdef _FAULT_INJECTION

#ifdef _KERNEL_ORB
	FaultFunctions::ha_rm_reboot_arg_t reboot_arg;
	int rslt = 0;
	Environment e;

	reboot_arg.nodeid = FaultFunctions::rm_primary_node();
	reboot_arg.op = FaultFunctions::WAIT_FOR_UNKNOWN;
	os::strcpy(reboot_arg.fn_name, "shutdown_service");

	rslt = shutdown_service(
		fault_ha_rm_shutdown_service_3_on,
		fault_ha_rm_shutdown_service_3_off,
		&reboot_arg,
		(int)sizeof (reboot_arg),
		"ha_sample_server",
		e);

	if (e.exception()) {
		e.exception()->print_exception("shutdown_service");
		os::printf("+++ FAIL: exception was not expected\n");
		return (1);
	}

	return (rslt);

#else
	os::printf("+++ ERROR: Tests only supported in kernel mode testing\n");
	return (1);
#endif // _KERNEL_ORB

#else
	return (no_fault_injection());
#endif
}

// (HA-RM) Checkpoints ShutdownService Case 4
//
//
int
chkpt_shutdown_4(int, char **)
{
	os::printf("HA-RM ckpt_shutdown_service Test Case 2 ***\n");

#ifdef _FAULT_INJECTION

#ifdef _KERNEL_ORB
	FaultFunctions::ha_rm_reboot_arg_t reboot_arg;
	int rslt = 0;
	Environment e;

	reboot_arg.nodeid = FaultFunctions::rm_primary_node();
	reboot_arg.op = FaultFunctions::WAIT_FOR_UNKNOWN;
	os::strcpy(reboot_arg.fn_name, "shutdown_service");

	rslt = shutdown_service(
		fault_ha_rm_shutdown_service_2_on,
		fault_ha_rm_shutdown_service_2_off,
		&reboot_arg,
		(int)sizeof (reboot_arg),
		"ha_sample_server",
		e);

	if (e.exception()) {
		e.exception()->print_exception("shutdown_service");
		os::printf("+++ FAIL: exception was not expected\n");
		return (1);
	}

	return (rslt);

#else
	os::printf("+++ ERROR: Tests only supported in kernel mode testing\n");
	return (1);
#endif // _KERNEL_ORB

#else
	return (no_fault_injection());
#endif
}
