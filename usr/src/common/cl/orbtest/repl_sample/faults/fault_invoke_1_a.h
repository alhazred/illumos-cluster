/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FAULT_INVOKE_1_A_H
#define	_FAULT_INVOKE_1_A_H

#pragma ident	"@(#)fault_invoke_1_a.h	1.8	08/05/20 SMI"

#include <orbtest/repl_sample/faults/entry_common.h>

//
// This file declares the trigger arming function for the fault points for a
// an invocation with multiple retries.
//

int fault_invoke_1_a_on(void *, int);

#endif	/* _FAULT_INVOKE_1_A_H */
