/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ENTRY_SWITCHOVER_PROV_STATE_H
#define	_ENTRY_SWITCHOVER_PROV_STATE_H

#pragma ident	"@(#)entry_switchover_prov_state.h	1.10	08/05/20 SMI"

void print_switchover_prov_state_usage(const char *[]);
int switchover_prov_state_no_fault(int, char *[]);
int switchover_prov_state_fault_a(int, char *[]);
int switchover_prov_state_fault_b(int, char *[]);
int switchover_prov_state_fault_c(int, char *[]);
int switchover_prov_state_fault_d(int, char *[]);
int switchover_prov_state_fault_e(int, char *[]);
int switchover_prov_state_fault_f(int, char *[]);
int switchover_prov_state_fault_g(int, char *[]);

#endif	/* _ENTRY_SWITCHOVER_PROV_STATE_H */
