/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fault_stress_1.cc	1.11	08/05/20 SMI"

//
// This file contains the on and off functions for the fault points for a
// specific fault scenario.
//
// Refer to the README.faults file for a complete description of all
// scenarios
//

#include	<sys/os.h>
#include	<orb/fault/fault_injection.h>
#include	<orbtest/repl_sample/faults/entry_stress_aux.h>


// Forward Definitions
int single_fp_specified_on(uint_t, void *, int);
int single_fp_once_on(uint_t, void *, int);
int single_fp_off(uint_t, void *, int);

//
// 1-A (Maps to Simple Mini 1-A)
//
int
fault_stress_1_a_on(void * argp, int arglen)
{
	single_fp_once_on(FAULTNUM_HA_FRMWK_HXDOOR_RESERVE_RESOURCES_1, argp,
	    arglen);
	return (0);
}
int
fault_stress_1_a_off(void * argp, int arglen)
{
	single_fp_off(FAULTNUM_HA_FRMWK_HXDOOR_RESERVE_RESOURCES_1, argp,
	    arglen);
	return (0);
}

//
// 1-B (Maps to Simple Mini 1-B)
//
int
fault_stress_1_b_on(void * argp, int arglen)
{
	single_fp_once_on(FAULTNUM_HA_FRMWK_HXDOOR_RESERVE_RESOURCES_2, argp,
	    arglen);
	return (0);
}
int
fault_stress_1_b_off(void * argp, int arglen)
{
	single_fp_off(FAULTNUM_HA_FRMWK_HXDOOR_RESERVE_RESOURCES_2, argp,
	    arglen);
	return (0);
}

//
// 1-C (Maps to Simple Mini 1-C)
//
int
fault_stress_1_c_on(void * argp, int arglen)
{
	single_fp_specified_on(FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_GET_VALUE,
	    argp, arglen);
	return (0);
}
int
fault_stress_1_c_off(void * argp, int arglen)
{
	single_fp_off(FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_GET_VALUE, argp,
	    arglen);
	return (0);
}


//
// 1-D (Maps to Simple Mini 1-D)
//
int
fault_stress_1_d_on(void * argp, int arglen)
{
	single_fp_specified_on(FAULTNUM_HA_FRMWK_HXDOOR_SEND_REPLY,
	    argp, arglen);
	return (0);
}
int
fault_stress_1_d_off(void * argp, int arglen)
{
	single_fp_off(FAULTNUM_HA_FRMWK_HXDOOR_SEND_REPLY, argp,
	    arglen);
	return (0);
}


//
// 1-E (Maps to One Phase Idempotent Mini 2-C)
//
int
fault_stress_1_e_on(void * argp, int arglen)
{
	single_fp_specified_on(FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_SET_VALUE,
	    argp, arglen);
	return (0);
}
int
fault_stress_1_e_off(void * argp, int arglen)
{
	single_fp_off(FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_SET_VALUE, argp,
	    arglen);
	return (0);
}


//
// 1-F (Maps to One Phase Non-Idempotent Mini 2-C)
//
int
fault_stress_1_f_on(void * argp, int arglen)
{
	single_fp_specified_on(FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_INC_VALUE,
	    argp, arglen);
	return (0);
}
int
fault_stress_1_f_off(void * argp, int arglen)
{
	single_fp_off(FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_INC_VALUE, argp,
	    arglen);
	return (0);
}


//
// 1-G (Maps to Two Phase Mini 2-B)
//
int
fault_stress_1_g_on(void * argp, int arglen)
{
	single_fp_specified_on(FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_INC_VALUE_2,
	    argp, arglen);
	return (0);
}
int
fault_stress_1_g_off(void * argp, int arglen)
{
	single_fp_off(FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_INC_VALUE_2, argp,
	    arglen);
	return (0);
}


//
// 1-H (Maps to Two Phase Mini 2-G)
//
int
fault_stress_1_h_on(void * argp, int arglen)
{
	single_fp_specified_on(FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_INC_VALUE_3,
	    argp, arglen);
	return (0);
}
int
fault_stress_1_h_off(void * argp, int arglen)
{
	single_fp_off(FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_INC_VALUE_3, argp,
	    arglen);
	return (0);
}


//
// 1-I (Maps to Two Phase Mini 2-N)
//
int
fault_stress_1_i_on(void * argp, int arglen)
{
	single_fp_specified_on(FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_SET_INC_VALUE,
	    argp, arglen);
	return (0);
}
int
fault_stress_1_i_off(void * argp, int arglen)
{
	single_fp_off(FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_SET_INC_VALUE, argp,
	    arglen);
	return (0);
}


//
// 1-J (Maps to Two Phase Mini 2-O)
//
int
fault_stress_1_j_on(void * argp, int arglen)
{
	single_fp_specified_on(FAULTNUM_HA_FRMWK_REPLICA_TMPL_ADD_COMMIT,
	    argp, arglen);
	return (0);
}
int
fault_stress_1_j_off(void * argp, int arglen)
{
	single_fp_off(FAULTNUM_HA_FRMWK_REPLICA_TMPL_ADD_COMMIT, argp,
	    arglen);
	return (0);
}


//
// Helper to turn on the fp using SEMA_V_ONCE as the op for the fault function
//
int
single_fp_once_on(uint_t faultnum, void *argp, int arglen)
{
	ASSERT(argp != NULL);
	ASSERT(arglen == sizeof (sema_node_pair_t));

	sema_node_pair_t		*in_arg;
	FaultFunctions::mc_sema_arg_t	sema_arg;

	in_arg = (sema_node_pair_t *)argp;

	sema_arg.id = in_arg->semaid;
	sema_arg.wait.op = FaultFunctions::WAIT_FOR_UNKNOWN;
	sema_arg.wait.nodeid = in_arg->nodeid;

	FaultFunctions::invo_trigger_add(FaultFunctions::SEMA_V_ONCE,
	    faultnum, &sema_arg,
	    (uint32_t)sizeof (FaultFunctions::mc_sema_arg_t));

	return (0);
}

//
// Helper to turn on the fp using SEMA_V_SPECIFIED as the op for the fault
// function
//
int
single_fp_specified_on(uint_t faultnum, void *argp, int arglen)
{
	ASSERT(argp != NULL);
	ASSERT(arglen == sizeof (sema_node_pair_t));

	sema_node_pair_t		*in_arg;
	FaultFunctions::mc_sema_arg_t	sema_arg;

	in_arg = (sema_node_pair_t *)argp;

	sema_arg.id = in_arg->semaid;
	sema_arg.nodeid = in_arg->nodeid;
	sema_arg.wait.op = FaultFunctions::WAIT_FOR_UNKNOWN;
	sema_arg.wait.nodeid = in_arg->nodeid;

	FaultFunctions::invo_trigger_add(FaultFunctions::SEMA_V_SPECIFIED,
	    faultnum, &sema_arg,
	    (uint32_t)sizeof (FaultFunctions::mc_sema_arg_t));

	return (0);
}

//
// Helper to turn off fp
//
int
single_fp_off(uint_t faultnum, void *, int)
{
	// Dont do anything since our fault is turned off by itself
	InvoTriggers::clear(faultnum);
	return (0);
}
