/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)faults_on_specific_service.cc	1.10	08/05/20 SMI"

//
// This file contains the on and off functions for the fault points for a
// specific fault scenario.
//
// Refer to the README.faults file for a complete description of all
// scenarios
//

#include	<sys/os.h>
#include	<orb/infrastructure/orb_conf.h>
#include	<orb/fault/fault_injection.h>

int
fault_repl_prov_become_secondary_on(void *argp, int arglen)
{
	ASSERT(argp != NULL);
	ASSERT((uint32_t)arglen == sizeof (FaultFunctions::svc_reboot_arg_t));

	FaultFunctions::node_trigger_add(FaultFunctions::SERVICE_REBOOT,
		FAULTNUM_HA_FRMWK_REPL_SERVICE_BECOME_SECONDARY,
		argp, (uint32_t)arglen,
		(uint32_t)TRIGGER_ALL_NODES);

	os::warning("Armed fault: "
		"FAULTNUM_HA_FRMWK_REPL_SERVICE_BECOME_SECONDARY");

	return (0);
}

int
fault_repl_prov_become_secondary_off(void *, int)
{
	NodeTriggers::clear(FAULTNUM_HA_FRMWK_REPL_SERVICE_BECOME_SECONDARY,
		TRIGGER_ALL_NODES);

	os::warning("Disarmed fault: "
		"FAULTNUM_HA_FRMWK_REPL_SERVICE_BECOME_SECONDARY");

	return (0);
}


int
fault_repl_prov_add_secondary_on(void *argp, int arglen)
{
	ASSERT(argp != NULL);
	ASSERT((uint32_t)arglen == sizeof (FaultFunctions::svc_reboot_arg_t));

	FaultFunctions::node_trigger_add(FaultFunctions::SERVICE_REBOOT,
		FAULTNUM_HA_FRMWK_REPL_SERVICE_ADD_SECONDARY,
		argp, (uint32_t)arglen,
		(uint32_t)TRIGGER_ALL_NODES);

	os::warning("Armed fault: "
		"FAULTNUM_HA_FRMWK_REPL_SERVICE_ADD_SECONDARY");

	return (0);
}

int
fault_repl_prov_add_secondary_off(void *, int)
{
	NodeTriggers::clear(FAULTNUM_HA_FRMWK_REPL_SERVICE_ADD_SECONDARY,
		TRIGGER_ALL_NODES);

	os::warning("Disarmed fault: "
		"FAULTNUM_HA_FRMWK_REPL_SERVICE_ADD_SECONDARY");

	return (0);
}


int
fault_repl_prov_commit_secondary_on(void *argp, int arglen)
{
	ASSERT(argp != NULL);
	ASSERT((uint32_t)arglen == sizeof (FaultFunctions::svc_reboot_arg_t));

	FaultFunctions::node_trigger_add(FaultFunctions::SERVICE_REBOOT,
		FAULTNUM_HA_FRMWK_REPL_SERVICE_COMMIT_SECONDARY,
		argp, (uint32_t)arglen,
		(uint32_t)TRIGGER_ALL_NODES);

	os::warning("Armed fault: "
		"FAULTNUM_HA_FRMWK_REPL_SERVICE_COMMIT_SECONDARY");

	return (0);
}

int
fault_repl_prov_commit_secondary_off(void *, int)
{
	NodeTriggers::clear(FAULTNUM_HA_FRMWK_REPL_SERVICE_COMMIT_SECONDARY,
		TRIGGER_ALL_NODES);

	os::warning("Disarmed fault: "
		"FAULTNUM_HA_FRMWK_REPL_SERVICE_COMMIT_SECONDARY");

	return (0);
}


int
fault_repl_prov_remove_secondary_on(void *argp, int arglen)
{
	ASSERT(argp != NULL);
	ASSERT((uint32_t)arglen == sizeof (FaultFunctions::svc_reboot_arg_t));

	FaultFunctions::node_trigger_add(FaultFunctions::SERVICE_REBOOT,
		FAULTNUM_HA_FRMWK_REPL_SERVICE_REMOVE_SECONDARY_2,
		argp, (uint32_t)arglen,
		(uint32_t)TRIGGER_ALL_NODES);

	os::warning("Armed fault: "
		"FAULTNUM_HA_FRMWK_REPL_SERVICE_REMOVE_SECONDARY_2");

	return (0);
}

int
fault_repl_prov_remove_secondary_off(void *, int)
{
	NodeTriggers::clear(FAULTNUM_HA_FRMWK_REPL_SERVICE_REMOVE_SECONDARY_2,
		TRIGGER_ALL_NODES);

	os::warning("Disarmed fault: "
		"FAULTNUM_HA_FRMWK_REPL_SERVICE_REMOVE_SECONDARY_2");

	return (0);
}


int
fault_repl_prov_become_primary_on(void *argp, int arglen)
{
	ASSERT(argp != NULL);
	ASSERT((uint32_t)arglen == sizeof (FaultFunctions::svc_reboot_arg_t));

	FaultFunctions::node_trigger_add(FaultFunctions::SERVICE_REBOOT,
		FAULTNUM_HA_FRMWK_REPL_SERVICE_BECOME_PRIMARY,
		argp, (uint32_t)arglen,
		(uint32_t)TRIGGER_ALL_NODES);

	os::warning("Armed fault: "
		"FAULTNUM_HA_FRMWK_REPL_SERVICE_BECOME_PRIMARY");

	return (0);
}

int
fault_repl_prov_become_primary_off(void *, int)
{
	NodeTriggers::clear(FAULTNUM_HA_FRMWK_REPL_SERVICE_BECOME_PRIMARY,
		TRIGGER_ALL_NODES);

	os::warning("Disarmed fault: "
		"FAULTNUM_HA_FRMWK_REPL_SERVICE_BECOME_PRIMARY");

	return (0);
}


int
fault_repl_prov_become_spare_on(void *argp, int arglen)
{
	ASSERT(argp != NULL);
	ASSERT((uint32_t)arglen == sizeof (FaultFunctions::svc_reboot_arg_t));

	FaultFunctions::node_trigger_add(FaultFunctions::SERVICE_REBOOT,
		FAULTNUM_HA_FRMWK_REPL_SERVICE_BECOME_SPARE_2,
		argp, (uint32_t)arglen,
		(uint32_t)TRIGGER_ALL_NODES);

	os::warning("Armed fault: "
		"FAULTNUM_HA_FRMWK_REPL_SERVICE_BECOME_SPARE_2");

	return (0);
}

int
fault_repl_prov_become_spare_off(void *, int)
{
	NodeTriggers::clear(FAULTNUM_HA_FRMWK_REPL_SERVICE_BECOME_SPARE_2,
		TRIGGER_ALL_NODES);

	os::warning("Disarmed fault: "
		"FAULTNUM_HA_FRMWK_REPL_SERVICE_BECOME_SPARE_2");

	return (0);
}

int
fault_repl_prov_switchover_on(void *argp, int arglen)
{
	ASSERT(argp != NULL);
	ASSERT((uint32_t)arglen == sizeof (FaultFunctions::svc_reboot_arg_t));

	FaultFunctions::node_trigger_add(FaultFunctions::SERVICE_REBOOT,
		FAULTNUM_HA_FRMWK_RM_SVC_FRZN_CLN_SWITCHOVER,
		argp, (uint32_t)arglen,
		(uint32_t)TRIGGER_ALL_NODES);

	os::warning("Armed fault: "
		"FAULTNUM_HA_FRMWK_RM_SVC_FRZN_CLN_SWITCHOVER");

	return (0);
}

int
fault_repl_prov_switchover_off(void *, int)
{
	NodeTriggers::clear(FAULTNUM_HA_FRMWK_RM_SVC_FRZN_CLN_SWITCHOVER,
		TRIGGER_ALL_NODES);

	os::warning("Disarmed fault: "
		"FAULTNUM_HA_FRMWK_RM_SVC_FRZN_CLN_SWITCHOVER");

	return (0);
}
