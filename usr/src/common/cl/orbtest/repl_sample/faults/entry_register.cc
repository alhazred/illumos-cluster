/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)entry_register.cc	1.16	08/05/20 SMI"

#include	<orbtest/repl_sample/repl_sample_clnt.h>
#include	<sys/os.h>
#include	<orb/invo/common.h>
#include	<orb/infrastructure/orb_conf.h>

#include	<orbtest/repl_sample/faults/fault_register_1_a.h>

//
// Register 1-A Test case
//
int
register_1_a(int, char *[])
{
	os::printf("*** Register Repl Prov Scenario 1 Fault A\n");

#ifdef _FAULT_INJECTION

#ifdef _KERNEL_ORB

	// This fault test case will simulate a joining rma during while a
	// registering service is in S_UNINIT state
	//
	// This is accomplished by rebooting a node while the
	// register_with_rm is hung after registering the service but before
	// registering the repl_prov
	//
	// We need to reboot the second non-root node
	//
	Environment	e;
	nodeid_t	nodeid;
	char		prov_desc[5];

	// We'll reboot the 2nd non_root node
	nodeid = non_root_node(2);

	if (nodeid == 0) {
		os::printf("+++ FAIL: Could not find the 2nd non-root node\n");
		return (1);
	}

	os::sprintf(prov_desc, "%d", orb_conf::node_number());

	os::printf("    Second non-root node is: %d\n", nodeid);

	int ret = register_with_rm(
	    fault_register_1_a_on,
	    fault_register_1_a_off,
	    &nodeid,
	    (int)sizeof (nodeid),
	    "ha_sample_server",
	    prov_desc,
	    e);

	if ((ret != 0) || e.exception()) {
		os::printf("+++ FAIL: register_with_rm failed\n");
		return (1);
	}

	return (shutdown_service_nowork("ha_sample_server", e, true));

#else
	os::printf("+++ FAIL: Test not supported in user-land\n");
	return (1);

#endif // _KERNEL_ORB

#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}
