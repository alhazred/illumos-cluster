/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FAULT_HA_RM_H
#define	_FAULT_HA_RM_H

#pragma ident	"@(#)fault_ha_rm.h	1.13	08/05/20 SMI"

#include <orbtest/repl_sample/faults/entry_common.h>

//
// This file contains the on and off functions for the fault points for a
// specific fault scenario.
//
// Refer to the README.faults file for a complete description of all
// scenarios
//

int fault_ha_rm_iterateover_on(void *, int);
int fault_ha_rm_iterateover_off(void *, int);
int fault_ha_rm_connectprimary_on(void *, int);
int fault_ha_rm_connectprimary_off(void *, int);

// int fault_ha_rm_registerservice_on(void *, int);
// int fault_ha_rm_registerservice_off(void *, int);

int fault_ha_rm_nodetriggers_on(int, void *, int, char *);
int fault_ha_rm_invotriggers_on(int, void *, int, char *);
int fault_ha_rm_invotriggers_off(int);
int fault_ha_rm_register_service_1_on(void *, int);
int fault_ha_rm_register_service_1_off(void *, int);
int fault_ha_rm_register_service_2_on(void *, int);
int fault_ha_rm_register_service_2_off(void *, int);
int fault_ha_rm_add_rma_1_on(void *, int);
int fault_ha_rm_add_rma_1_off(void *, int);
int fault_ha_rm_add_rma_2_on(void *, int);
int fault_ha_rm_add_rma_2_off(void *, int);

// int fault_ha_rm_addrma_on(void *, int);
// int fault_ha_rm_addrma_off(void *, int);
int fault_ha_rm_shutdown_service_1_on(void *, int);
int fault_ha_rm_shutdown_service_1_off(void *, int);
int fault_ha_rm_shutdown_service_2_on(void *, int);
int fault_ha_rm_shutdown_service_2_off(void *, int);
int fault_ha_rm_shutdown_service_3_on(void *, int);
int fault_ha_rm_shutdown_service_3_off(void *, int);
int fault_ha_rm_shutdown_service_4_on(void *, int);
int fault_ha_rm_shutdown_service_4_off(void *, int);
int fault_ha_rm_register_repl_prov_1_on(void *, int);
int fault_ha_rm_register_repl_prov_1_off(void *, int);
int fault_ha_rm_register_repl_prov_2_on(void *, int);
int fault_ha_rm_register_repl_prov_2_off(void *, int);
int fault_ha_rm_get_control_1_on(void *, int);
int fault_ha_rm_get_control_1_off(void *, int);
int fault_ha_rm_get_control_2_on(void *, int);
int fault_ha_rm_get_control_2_off(void *, int);

#endif	/* _FAULT_HA_RM_H */
