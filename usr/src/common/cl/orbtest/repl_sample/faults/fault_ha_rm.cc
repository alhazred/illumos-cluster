/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fault_ha_rm.cc	1.13	08/05/20 SMI"

//
// This file contains the on and off functions for the fault points for a
// specific fault scenario.
//
// Refer to the README.faults file for a complete description of all
// scenarios
//

#include	<sys/os.h>
#include	<sys/mc_sema_impl.h>
#include	<orb/fault/fault_injection.h>
#include	<orbtest/repl_sample/faults/entry_common.h>

#include	<orbtest/repl_sample/faults/entry_ha_rm.h>

int
fault_ha_rm_iterateover_on(void *argp, int arglen)
{
	ASSERT(argp != NULL);
	ASSERT(arglen == sizeof (FaultFunctions::ha_rm_reboot_arg_t));

	FaultFunctions::ha_rm_reboot_arg_t *reboot_args;
	reboot_args = (FaultFunctions::ha_rm_reboot_arg_t *)argp;

	nodeid_t primary_rm_nodeid = reboot_args->nodeid;
	NodeTriggers::add(FAULTNUM_RM_REPL_SERVICE_ITERATE_OVER,
		argp, arglen, primary_rm_nodeid);

	os::warning("Armed fault: "
		"FAULTNUM_RM_REPL_SERVICE_ITERATE_OVER");

	return (0);
}

int
fault_ha_rm_iterateover_off(void *, int)
{
	// Node Triggers do not need to be cleared
	return (0);
}

int
fault_ha_rm_connectprimary_on(void *argp, int arglen)
{
	ASSERT(argp != NULL);
	ASSERT(arglen == sizeof (FaultFunctions::ha_rm_reboot_arg_t));

	FaultFunctions::ha_rm_reboot_arg_t *reboot_args;
	reboot_args = (FaultFunctions::ha_rm_reboot_arg_t *)argp;

	nodeid_t primary_rm_nodeid = reboot_args->nodeid;
	NodeTriggers::add(FAULTNUM_RM_STATE_MACH_CONNECT_PRIMARY,
		argp, arglen, primary_rm_nodeid);

	os::warning("Armed fault: "
		"FAULTNUM_RM_STATE_MACH_CONNECT_PRIMARY");

	return (0);
}

int
fault_ha_rm_connectprimary_off(void *, int)
{
	// Node Triggers do not need to be cleared
	return (0);
}

int
fault_ha_rm_nodetriggers_on(
	uint_t fault_num,
	void *argp,
	int arglen,
	char *faultpt_str)
{
	ASSERT(argp != NULL);
	ASSERT(arglen == sizeof (FaultFunctions::ha_rm_reboot_arg_t));

	FaultFunctions::ha_rm_reboot_arg_t *reboot_args;
	reboot_args = (FaultFunctions::ha_rm_reboot_arg_t *)argp;

	nodeid_t primary_rm_nodeid = reboot_args->nodeid;
	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT_HA_RM,
		fault_num, argp, arglen, primary_rm_nodeid);

	os::warning("Armed fault: %s",
		(const char *)faultpt_str);

	return (0);
}

int
fault_ha_rm_invotriggers_on(
	uint_t fault_num,
	void *argp,
	int arglen,
	char *faultpt_str)
{
	ASSERT(argp != NULL);
	ASSERT(arglen == sizeof (FaultFunctions::ha_rm_reboot_arg_t));

	FaultFunctions::invo_trigger_add(FaultFunctions::REBOOT_HA_RM,
		fault_num, argp, arglen);

	os::warning("Armed fault %s",
		(const char *)faultpt_str);

	return (0);
}

int
fault_ha_rm_invotriggers_off(uint_t fault_num)
{
	InvoTriggers::clear(fault_num);
	return (0);
}

int
fault_ha_rm_register_service_1_on(void *argp, int arglen)
{
	int rslt = 0;

	rslt = fault_ha_rm_invotriggers_on(
		FAULTNUM_HA_RM_CKPT_REGISTER_SERVICE_1,
		argp,
		arglen,
		"FAULTNUM_HA_RM_CKPT_REGISTER_SERVICE_1");

	return (rslt);
}

int
fault_ha_rm_register_service_1_off(void *, int)
{
	fault_ha_rm_invotriggers_off(
		FAULTNUM_HA_RM_CKPT_REGISTER_SERVICE_1);
	return (0);
}

int
fault_ha_rm_register_service_2_on(void *argp, int arglen)
{
	int rslt = 0;

	rslt = fault_ha_rm_invotriggers_on(
		FAULTNUM_HA_RM_CKPT_REGISTER_SERVICE_2,
		argp,
		arglen,
		"FAULTNUM_HA_RM_CKPT_REGISTER_SERVICE_2");

	return (rslt);
}

int
fault_ha_rm_register_service_2_off(void *, int)
{
	fault_ha_rm_invotriggers_off(
		FAULTNUM_HA_RM_CKPT_REGISTER_SERVICE_2);
	return (0);
}

int
fault_ha_rm_add_rma_1_on(void *argp, int arglen)
{
	int rslt = 0;

	rslt = fault_ha_rm_nodetriggers_on(
		FAULTNUM_HA_RM_CKPT_ADD_RMA_1,
		argp,
		arglen,
		"FAULTNUM_HA_RM_CKPT_ADD_RMA_1");

	return (rslt);
}

int
fault_ha_rm_add_rma_1_off(void *, int)
{
	// Node triggers needn't be cleared

	return (0);
}

int
fault_ha_rm_add_rma_2_on(void *argp, int arglen)
{
	int rslt = 0;

	rslt = fault_ha_rm_nodetriggers_on(
		FAULTNUM_HA_RM_CKPT_ADD_RMA_2,
		argp,
		arglen,
		"FAULTNUM_HA_RM_CKPT_ADD_RMA_2");

	return (rslt);
}

int
fault_ha_rm_add_rma_2_off(void *, int)
{
	// Node triggers needn't be cleared

	return (0);
}

int
fault_ha_rm_shutdown_service_1_on(void *argp, int arglen)
{
	int rslt = 0;

	rslt = fault_ha_rm_invotriggers_on(
		FAULTNUM_HA_RM_CKPT_SHUTDOWN_SERVICE_1,
		argp,
		arglen,
		"FAULTNUM_HA_RM_CKPT_SHUTDOWN_SERVICE_1");

	return (rslt);
}

int
fault_ha_rm_shutdown_service_1_off(void *, int)
{
	fault_ha_rm_invotriggers_off(
		FAULTNUM_HA_RM_CKPT_SHUTDOWN_SERVICE_2);
	return (0);
}

int
fault_ha_rm_shutdown_service_2_on(void *argp, int arglen)
{
	int rslt = 0;

	rslt = fault_ha_rm_invotriggers_on(
		FAULTNUM_HA_RM_CKPT_SHUTDOWN_SERVICE_2,
		argp,
		arglen,
		"FAULTNUM_HA_RM_CKPT_SHUTDOWN_SERVICE_2");

	return (rslt);
}

int
fault_ha_rm_shutdown_service_2_off(void *, int)
{
	fault_ha_rm_invotriggers_off(
		FAULTNUM_HA_RM_CKPT_SHUTDOWN_SERVICE_2);
	return (0);
}

int
fault_ha_rm_shutdown_service_3_on(void *argp, int arglen)
{
	int rslt = 0;

	rslt = fault_ha_rm_invotriggers_on(
		FAULTNUM_HA_RM_CKPT_SHUTDOWN_SERVICE_3,
		argp,
		arglen,
		"FAULTNUM_HA_RM_CKPT_SHUTDOWN_SERVICE_3");

	return (rslt);
}

int
fault_ha_rm_shutdown_service_3_off(void *, int)
{
	fault_ha_rm_invotriggers_off(
		FAULTNUM_HA_RM_CKPT_SHUTDOWN_SERVICE_3);
	return (0);
}

int
fault_ha_rm_shutdown_service_4_on(void *argp, int arglen)
{
	int rslt = 0;

	rslt = fault_ha_rm_invotriggers_on(
		FAULTNUM_HA_RM_CKPT_SHUTDOWN_SERVICE_4,
		argp,
		arglen,
		"FAULTNUM_HA_RM_CKPT_SHUTDOWN_SERVICE_4");

	return (rslt);
}

int
fault_ha_rm_shutdown_service_4_off(void *, int)
{
	fault_ha_rm_invotriggers_off(
		FAULTNUM_HA_RM_CKPT_SHUTDOWN_SERVICE_4);
	return (0);
}

int
fault_ha_rm_get_control_1_on(void *argp, int arglen)
{
	int rslt = 0;

	rslt = fault_ha_rm_invotriggers_on(
		FAULTNUM_HA_RM_CKPT_GET_CONTROL_1,
		argp,
		arglen,
		"FAULTNUM_HA_RM_CKPT_GET_CONTROL_1");

	return (rslt);
}

int
fault_ha_rm_get_control_1_off(void *, int)
{
	fault_ha_rm_invotriggers_off(
		FAULTNUM_HA_RM_CKPT_GET_CONTROL_2);
	return (0);
}

int
fault_ha_rm_get_control_2_on(void *argp, int arglen)
{
	int rslt = 0;

	rslt = fault_ha_rm_invotriggers_on(
		FAULTNUM_HA_RM_CKPT_GET_CONTROL_2,
		argp,
		arglen,
		"FAULTNUM_HA_RM_CKPT_GET_CONTROL_2");

	return (rslt);
}

int
fault_ha_rm_get_control_2_off(void *, int)
{
	fault_ha_rm_invotriggers_off(
		FAULTNUM_HA_RM_CKPT_GET_CONTROL_2);
	return (0);
}

int
fault_ha_rm_register_repl_prov_1_on(void *argp, int arglen)
{
	int rslt = 0;

	rslt = fault_ha_rm_invotriggers_on(
		FAULTNUM_HA_RM_CKPT_REGISTER_REPL_PROV_1,
		argp,
		arglen,
		"FAULTNUM_HA_RM_CKPT_REGISTER_REPL_PROV_1");

	return (rslt);
}

int
fault_ha_rm_register_repl_prov_1_off(void *, int)
{
	fault_ha_rm_invotriggers_off(
		FAULTNUM_HA_RM_CKPT_REGISTER_REPL_PROV_1);
	return (0);
}

int
fault_ha_rm_register_repl_prov_2_on(void *argp, int arglen)
{
	int rslt = 0;

	rslt = fault_ha_rm_invotriggers_on(
		FAULTNUM_HA_RM_CKPT_REGISTER_REPL_PROV_2,
		argp,
		arglen,
		"FAULTNUM_HA_RM_CKPT_REGISTER_REPL_PROV_2");

	return (rslt);
}

int
fault_ha_rm_register_repl_prov_2_off(void *, int)
{
	fault_ha_rm_invotriggers_off(
		FAULTNUM_HA_RM_CKPT_REGISTER_REPL_PROV_2);
	return (0);
}
