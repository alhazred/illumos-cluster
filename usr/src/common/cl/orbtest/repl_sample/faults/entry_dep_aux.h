/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ENTRY_DEP_AUX_H
#define	_ENTRY_DEP_AUX_H

#pragma ident	"@(#)entry_dep_aux.h	1.16	08/05/20 SMI"

#include <orb/fault/fault_injection.h>
#include <h/replica.h>
#include <h/mc_sema.h>

#ifdef _FAULT_INJECTION

typedef struct {
	FaultFunctions::ha_dep_reboot_arg_t	reboot_arg;
	replica::service_num_t			top_sid;
	mc_sema::cookie				sema_id;
} dep_fault_arg_t;

//
// List cell for svc name and ending state
typedef struct {
	char			name[25];
	replica::service_state	state;
} svc_state_t;

//
// List for passing a number of services with their expected ending states
// also you can select which service will be top and which will be bottom
typedef struct {
	int		listlen;
	int		bottom_svc;
	int		top_svc;
	svc_state_t	*list;
} svc_state_list_t;

//
// Helper for fault test cases.  (A dep B)
//
// Arguments:
//	f_on		- pointer to function to add triggers
//	f_off		- pointer to function to remove triggers
//	sid		- id of service to fault on.
//	fault_nodeid	- nodeid of node to kill
//	should_switch	- Should the provs end up on a different node
//			  (post-test verification)
//	state_1		- State we expect for service_1 after test is complete
//	state_2		- State we expect for service_2 after test is complete
//	promote		- promote a secondary or demote a primary
//
// This helper function will do the following actions:
// 1 - Verify that service_1 and service_2 are on the same node to begin with
// 2 - Set the TSD with the sid of "service_1"
// 3 - Start a switchover on service_1
// 4 - Set the fault to trigger with the sid specfied (sid)
// 5 - Verify that the prov's are on new nodes
// 6 - Verify that both services are still up on same node
//
int dep_switchover_helper(FAULT_FPTR(f_on), FAULT_FPTR(f_off),
    replica::service_num_t sid, nodeid_t fault_nodeid, bool should_switch,
    svc_state_list_t *svc_state_list, bool promote = true);

#endif // _FAULT_INJECTION

#endif	/* _ENTRY_DEP_AUX_H */
