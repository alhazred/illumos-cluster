/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)entry_change_desired_numsecs.cc	1.9	08/05/20 SMI"

#include	<h/replica.h>
#include	<orb/fault/fault_injection.h>
#include	<orbtest/repl_sample/repl_sample_clnt.h>
#include	<sys/os.h>
#include	<orb/invo/common.h>
#include	<orbtest/rm_probe/rm_snap.h>

#include	<orbtest/repl_sample/faults/entry_common.h>
#include	<orbtest/repl_sample/faults/entry_change_desired_numsecs.h>
#include	<orbtest/repl_sample/faults/faults_on_specific_service.h>


//
// Usage: functname <svc_name> <desired_numsecs> [fault_node]
//	where <svc_name> = name of the service
//	<desired_numsecs> = desired number of secondaries
//	[fault_node] = node specified to be faulted (optional)
//

void
print_change_desired_numsecs_usage(const char *functname)
{
	os::printf("Incorrect usage:\t%s\n", functname);
}

int
change_desired_numsecs_no_fault(int argc, char **argv)
{
	os::printf("*** Changing desired numsecs without fault ***\n");

#ifdef _FAULT_INJECTION

	int rslt = 1;
	Environment e;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if ((argc < 3) || (argc > 5)) {
		print_change_desired_numsecs_usage(argv[0]);
		return (rslt);
	}

	char *svc_name = new char[os::strlen(argv[1])+1];
	os::sprintf(svc_name, "%s", argv[1]);

	uint32_t numsecs = (uint32_t)os::atoi(argv[2]);

	// No Fault
	rslt = change_desired_num_secs(
		NULL, NULL, NULL, 0, svc_name, numsecs, e);

	delete [] svc_name;
	return (rslt);

#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
} 

int
change_desired_numsecs_fault_a(int argc, char **argv)
{
	os::printf("*** Changing desired numsecs with fault ***\n");

#ifdef _FAULT_INJECTION

	int 		rslt;
	nodeid_t	fault_node;
	Environment 	e;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if ((argc < 3) || (argc > 5)) {
		print_change_desired_numsecs_usage(argv[0]);
		return (rslt);
	}

	char *svc_name = new char[os::strlen(argv[1])+1];
	os::sprintf(svc_name, "%s", argv[1]);

	uint32_t numsecs = (uint32_t)os::atoi(argv[2]);

	// Verify that service is up
	if (get_state(svc_name) != replica::S_UP) {
		os::printf("+++ FAIL: service is not in S_UP state\n");
		return (1);
	}

	if (argc == 4) {
		fault_node = (nodeid_t)os::atoi(argv[3]);
	} else {
		if ((fault_node = get_primary(svc_name)) == 0)
			return (1);
	}

	// Fault the specified service if it is supplied; otherwise,
	// use the service for changing desired number of secondaries
	// as default
	char *fault_svc_name;
	if (argc == 5) {
		fault_svc_name = new char[os::strlen(argv[4])+1];
		os::sprintf(fault_svc_name, "%s", argv[4]);
	} else {
		fault_svc_name = svc_name;
	}

	os::printf(" ** Will fault service %s on node %d\n",
		fault_svc_name, fault_node);

	// Populate fault arguments
	FaultFunctions::svc_reboot_arg_t	reboot_args;
	reboot_args.sid	 = get_sid(fault_svc_name);
	reboot_args.op	  = FaultFunctions::WAIT_FOR_UNKNOWN;
	reboot_args.nodeid	= fault_node;

	// Fault on provider to become secondary
	rslt = change_desired_num_secs(
		fault_repl_prov_become_secondary_on,
		fault_repl_prov_become_secondary_off,
		&reboot_args, (int)sizeof (reboot_args),
		svc_name, numsecs, e);

	delete [] svc_name;

	if (argc == 5) {
		delete [] fault_svc_name;
	}

	return (rslt);

#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

int
change_desired_numsecs_fault_b(int argc, char **argv)
{
	os::printf("*** Changing desired numsecs with fault ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;
	Environment 	e;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}


	if ((argc < 3) || (argc > 5)) {
		print_change_desired_numsecs_usage(argv[0]);
		return (rslt);
	}

	char *svc_name = new char[os::strlen(argv[1])+1];
	os::sprintf(svc_name, "%s", argv[1]);

	uint32_t numsecs = (uint32_t)os::atoi(argv[2]);

	// Verify that service is up
	if (get_state(svc_name) != replica::S_UP) {
		os::printf("+++ FAIL: service is not in S_UP state\n");
		return (1);
	}

	if (argc == 4) {
		fault_node = (nodeid_t)os::atoi(argv[3]);
	} else {
		if ((fault_node = get_primary(svc_name)) == 0)
			return (1);
	}

	// Fault the specified service if it is supplied; otherwise,
	// use the service for changing desired number of secondaries
	// as default
	char *fault_svc_name;
	if (argc == 5) {
		fault_svc_name = new char[os::strlen(argv[4])+1];
		os::sprintf(fault_svc_name, "%s", argv[4]);
	} else {
		fault_svc_name = svc_name;
	}

	os::printf(" ** Will fault service %s on node %d\n",
		fault_svc_name, fault_node);

	// Populate fault arguments
	FaultFunctions::svc_reboot_arg_t	reboot_args;
	reboot_args.sid	 = get_sid(fault_svc_name);
	reboot_args.op	  = FaultFunctions::WAIT_FOR_UNKNOWN;
	reboot_args.nodeid	= fault_node;

	// Fault on provider to add secondary
	rslt = change_desired_num_secs(
		fault_repl_prov_add_secondary_on,
		fault_repl_prov_add_secondary_off,
		&reboot_args, (int)sizeof (reboot_args),
		svc_name, numsecs, e);

	delete [] svc_name;

	if (argc == 5) {
		delete [] fault_svc_name;
	}

	return (rslt);

#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

int
change_desired_numsecs_fault_c(int argc, char **argv)
{
	os::printf("*** Changing desired numsecs with fault ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;
	Environment 	e;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if ((argc < 3) || (argc > 5)) {
		print_change_desired_numsecs_usage(argv[0]);
		return (rslt);
	}

	char *svc_name = new char[os::strlen(argv[1])+1];
	os::sprintf(svc_name, "%s", argv[1]);

	uint32_t numsecs = (uint32_t)os::atoi(argv[2]);

	// Verify that service is up
	if (get_state(svc_name) != replica::S_UP) {
		os::printf("+++ FAIL: service is not in S_UP state\n");
		return (1);
	}

	if (argc == 4) {
		fault_node = (nodeid_t)os::atoi(argv[3]);
	} else {
		if ((fault_node = get_primary(svc_name)) == 0)
			return (1);
	}

	// Fault the specified service if it is supplied; otherwise,
	// use the service for changing desired number of secondaries
	// as default
	char *fault_svc_name;
	if (argc == 5) {
		fault_svc_name = new char[os::strlen(argv[4])+1];
		os::sprintf(fault_svc_name, "%s", argv[4]);
	} else {
		fault_svc_name = svc_name;
	}

	os::printf(" ** Will fault service %s on node %d\n",
		fault_svc_name, fault_node);

	// Populate fault arguments
	FaultFunctions::svc_reboot_arg_t	reboot_args;
	reboot_args.sid	 = get_sid(fault_svc_name);
	reboot_args.op	  = FaultFunctions::WAIT_FOR_UNKNOWN;
	reboot_args.nodeid	= fault_node;

	// Fault on provider to add secondary
	rslt = change_desired_num_secs(
		fault_repl_prov_commit_secondary_on,
		fault_repl_prov_commit_secondary_off,
		&reboot_args, (int)sizeof (reboot_args),
		svc_name, numsecs, e);

	delete [] svc_name;

	if (argc == 5) {
		delete [] fault_svc_name;
	}

	return (rslt);

#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

int
change_desired_numsecs_fault_d(int argc, char **argv)
{
	os::printf("*** Changing desired numsecs with fault ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;
	Environment 	e;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if ((argc < 3) || (argc > 5)) {
		print_change_desired_numsecs_usage(argv[0]);
		return (rslt);
	}

	char *svc_name = new char[os::strlen(argv[1])+1];
	os::sprintf(svc_name, "%s", argv[1]);

	uint32_t numsecs = (uint32_t)os::atoi(argv[2]);

	// Verify that service is up
	if (get_state(svc_name) != replica::S_UP) {
		os::printf("+++ FAIL: service is not in S_UP state\n");
		return (1);
	}

	if (argc == 4) {
		fault_node = (nodeid_t)os::atoi(argv[3]);
	} else {
		if ((fault_node = get_primary(svc_name)) == 0)
			return (1);
	}

	// Fault the specified service if it is supplied; otherwise,
	// use the service for changing desired number of secondaries
	// as default
	char *fault_svc_name;
	if (argc == 5) {
		fault_svc_name = new char[os::strlen(argv[4])+1];
		os::sprintf(fault_svc_name, "%s", argv[4]);
	} else {
		fault_svc_name = svc_name;
	}

	os::printf(" ** Will fault service %s on node %d\n",
		fault_svc_name, fault_node);

	// Populate fault arguments
	FaultFunctions::svc_reboot_arg_t	reboot_args;
	reboot_args.sid	 = get_sid(fault_svc_name);
	reboot_args.op	  = FaultFunctions::WAIT_FOR_UNKNOWN;
	reboot_args.nodeid	= fault_node;

	// Fault on provider to add secondary
	rslt = change_desired_num_secs(
		fault_repl_prov_remove_secondary_on,
		fault_repl_prov_remove_secondary_off,
		&reboot_args, (int)sizeof (reboot_args),
		svc_name, numsecs, e);

	delete [] svc_name;

	if (argc == 5) {
		delete [] fault_svc_name;
	}

	return (rslt);

#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

int
change_desired_numsecs_fault_e(int argc, char **argv)
{
	os::printf("*** Changing desired numsecs with fault ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;
	Environment 	e;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if ((argc < 3) || (argc > 5)) {
		print_change_desired_numsecs_usage(argv[0]);
		return (rslt);
	}

	char *svc_name = new char[os::strlen(argv[1])+1];
	os::sprintf(svc_name, "%s", argv[1]);

	uint32_t numsecs = (uint32_t)os::atoi(argv[2]);

	// Verify that service is up
	if (get_state(svc_name) != replica::S_UP) {
		os::printf("+++ FAIL: service is not in S_UP state\n");
		return (1);
	}

	if (argc == 4) {
		fault_node = (nodeid_t)os::atoi(argv[3]);
	} else {
		if ((fault_node = get_primary(svc_name)) == 0)
			return (1);
	}

	// Fault the specified service if it is supplied; otherwise,
	// use the service for changing desired number of secondaries
	// as default
	char *fault_svc_name;
	if (argc == 5) {
		fault_svc_name = new char[os::strlen(argv[4])+1];
		os::sprintf(fault_svc_name, "%s", argv[4]);
	} else {
		fault_svc_name = svc_name;
	}

	os::printf(" ** Will fault service %s on node %d\n",
		fault_svc_name, fault_node);

	// Populate fault arguments
	FaultFunctions::svc_reboot_arg_t	reboot_args;
	reboot_args.sid	 = get_sid(fault_svc_name);
	reboot_args.op	  = FaultFunctions::WAIT_FOR_UNKNOWN;
	reboot_args.nodeid	= fault_node;

	// Fault on provider to add secondary
	rslt = change_desired_num_secs(
		fault_repl_prov_become_primary_on,
		fault_repl_prov_become_primary_off,
		&reboot_args, (int)sizeof (reboot_args),
		svc_name, numsecs, e);

	delete [] svc_name;

	if (argc == 5) {
		delete [] fault_svc_name;
	}

	return (rslt);

#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

int
change_desired_numsecs_fault_f(int argc, char **argv)
{
	os::printf("*** Changing desired numsecs with fault ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;
	Environment 	e;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if ((argc < 3) || (argc > 5)) {
		print_change_desired_numsecs_usage(argv[0]);
		return (rslt);
	}

	char *svc_name = new char[os::strlen(argv[1])+1];
	os::sprintf(svc_name, "%s", argv[1]);

	uint32_t numsecs = (uint32_t)os::atoi(argv[2]);

	// Verify that service is up
	if (get_state(svc_name) != replica::S_UP) {
		os::printf("+++ FAIL: service is not in S_UP state\n");
		return (1);
	}

	if (argc == 4) {
		fault_node = (nodeid_t)os::atoi(argv[3]);
	} else {
		if ((fault_node = get_primary(svc_name)) == 0)
			return (1);
	}

	// Fault the specified service if it is supplied; otherwise,
	// use the service for changing desired number of secondaries
	// as default
	char *fault_svc_name;
	if (argc == 5) {
		fault_svc_name = new char[os::strlen(argv[4])+1];
		os::sprintf(fault_svc_name, "%s", argv[4]);
	} else {
		fault_svc_name = svc_name;
	}

	os::printf(" ** Will fault service %s on node %d\n",
		fault_svc_name, fault_node);

	// Populate fault arguments
	FaultFunctions::svc_reboot_arg_t	reboot_args;
	reboot_args.sid	= get_sid(fault_svc_name);
	reboot_args.op	= FaultFunctions::WAIT_FOR_UNKNOWN;
	reboot_args.nodeid	= fault_node;

	// Fault on provider to add secondary
	rslt = change_desired_num_secs(
		fault_repl_prov_become_spare_on,
		fault_repl_prov_become_spare_off,
		&reboot_args, (int)sizeof (reboot_args),
		svc_name, numsecs, e);

	delete [] svc_name;

	if (argc == 5) {
		delete [] fault_svc_name;
	}

	return (rslt);

#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}
