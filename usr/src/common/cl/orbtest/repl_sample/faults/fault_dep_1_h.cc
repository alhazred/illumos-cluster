/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fault_dep_1_h.cc	1.14	08/05/20 SMI"

//
// This file contains the on and off functions for the fault points for a
// specific fault scenario.
//
// Refer to the README.faults file for a complete description of all
// scenarios
//

#include	<sys/os.h>
#include	<orb/fault/fault_injection.h>

#include	<orbtest/repl_sample/faults/entry_dep_aux.h>

int
fault_dep_1_h_on(void *argp, int arglen)
{
	ASSERT(argp != NULL);
	ASSERT(arglen == sizeof (dep_fault_arg_t));

	dep_fault_arg_t		*dep_argp = NULL;
	dep_argp = (dep_fault_arg_t *)argp;

	FaultFunctions::node_trigger_add(FaultFunctions::HA_DEP_REBOOT,
	    FAULTNUM_HA_FRMWK_RM_START_PRIMARY,
	    &(dep_argp->reboot_arg),
	    (uint32_t)sizeof (FaultFunctions::ha_dep_reboot_arg_t),
	    TRIGGER_ALL_NODES);

	//
	// Construct the arguments that need to be passed to
	// FaultFunctions::ha_dep_sema_v
	//
	// We need to set NO_WAIT
	// and the correct sid.
	//
	FaultFunctions::ha_dep_mc_sema_arg_t	ha_dep_sema_v_arg;
	ha_dep_sema_v_arg.sid			= dep_argp->top_sid;
	ha_dep_sema_v_arg.mc_sema_arg.id	= dep_argp->sema_id;
	ha_dep_sema_v_arg.mc_sema_arg.nodeid	= 0;
	ha_dep_sema_v_arg.mc_sema_arg.wait.op	= FaultFunctions::NO_WAIT;
	ha_dep_sema_v_arg.mc_sema_arg.wait.nodeid	= 0;

	FaultFunctions::node_trigger_add(FaultFunctions::HA_DEP_SEMA_V,
	    FAULTNUM_HA_FRMWK_RM_RECONFIG_EXIT,
	    &ha_dep_sema_v_arg,
	    (uint32_t)sizeof (ha_dep_sema_v_arg),
	    TRIGGER_ALL_NODES);

	os::warning("Armed fault: "
	    "FAULTNUM_HA_FRMWK_RM_START_PRIMARY");
	os::warning("Armed fault: "
	    "FAULTNUM_HA_FRMWK_RM_RECONFIG_EXIT");

	return (0);
}

int
fault_dep_1_h_off(void *, int)
{
	return (0);
}
