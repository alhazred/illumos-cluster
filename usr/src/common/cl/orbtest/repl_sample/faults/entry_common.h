/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ORBTEST_REPL_SAMPLE_FAULTS_ENTRY_COMMON_H
#define	_ORBTEST_REPL_SAMPLE_FAULTS_ENTRY_COMMON_H

#pragma ident	"@(#)entry_common.h	1.17	08/05/20 SMI"

#include <h/replica.h>
#include <h/mc_sema.h>
#include <orb/invo/common.h>

// Helper for faults that need two id's passed to turn them on or off
typedef struct {
	nodeid_t	first;
	nodeid_t	second;
} nodeid_pair_t;

// Helper to pass a nodeid and two mc_sema cookies to a fault on/off function
typedef struct {
	nodeid_t	nodeid;
	mc_sema::cookie	id1;
	mc_sema::cookie	id2;
} cookie_pair_t;

#if defined(_KERNEL_ORB) && defined(_FAULT_INJECTION)
//
// Find the nth non-root node
//
nodeid_t non_root_node(int n);
#endif // _KERNEL_ORB

//
// If fault injection is not turned on, print an error message and return 0.
//
#if !defined(_FAULT_INJECTION)
int	no_fault_injection();
#endif

//
// get_primary
//
// Get the primary nodeid for the specified service
// arguments
//	service name
//	print		- bool specifying wether to print error msgs.
// returns
//	nodeid (or -1 if error)
// side effect
//	prints errors
nodeid_t get_primary(const char *svc_name, bool print = true);

//
// get_secondary
//
// Get the nth secondary nodeid for the specified service
// arguments
//	service name
//	print		- bool specifying wether to print error msgs.
// returns
//	nodeid (or -1 if error)
// side effect
//	prints errors
nodeid_t get_secondary(int n, const char *svc_name, bool print = true);

//
// get_state
//
// Get the state of the specified service
// arguments
//	service_name
//	print		- bool specifying wether to print error msgs.
// returns
//	state
// side effect
//	prints errors
int get_state(const char *svc_name, bool print = true);

//
// get_sid
//
// Get the sid of the specified service
// arguments
//	service_name
//	print		- bool specifying wether to print error msgs.
// returns
//	sid
// side effect
//	prints errors
replica::service_num_t get_sid(const char *svc_name, bool print = true);

#endif	/* _ORBTEST_REPL_SAMPLE_FAULTS_ENTRY_COMMON_H */
