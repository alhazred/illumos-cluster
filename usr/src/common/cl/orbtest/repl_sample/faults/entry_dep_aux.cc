/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)entry_dep_aux.cc	1.14	08/05/20 SMI"

#include	<sys/os.h>
#include	<h/replica.h>
#include	<h/mc_sema.h>
#include	<orb/invo/common.h>
#include	<orb/infrastructure/orb_conf.h>
#include	<orb/fault/fault_injection.h>
#include	<orbtest/repl_sample/repl_sample_clnt.h>
#include	<sys/mc_sema_impl.h>

#include	<orbtest/rm_probe/rm_snap.h>
#include	<orbtest/repl_sample/faults/entry_common.h>
#include	<orbtest/repl_sample/faults/entry_dep_aux.h>

#if defined(_FAULT_INJECTION)
//
// Helper for fault test cases.
//
// Arguments:
//	f_on		- pointer to function to add triggers
//	f_off		- pointer to function to remove triggers
//	sid		- id of service to fault on.
//	fault_nodeid	- nodeid of node to kill
//	should_switch	- Should the provs end up on a different node
//			  (post-test verification)
//	state_1		- State we expect for bottom_svc after test is complete
//	state_2		- State we expect for top_svc after test is complete
//	promote		- promote a secondary or demote a primary
//
// This helper function will do the following actions:
// 1 - Verify that bottom_svc and top_svc are on the same node to begin with
// 2 - Start a switchover on bottom_svc
// 3 - Set the fault to trigger with the sid specfied (sid)
// 4 - Verify that the prov's are on new nodes
// 5 - Verify that both services are still up on same node
//
int
dep_switchover_helper(FAULT_FPTR(f_on), FAULT_FPTR(f_off),
    replica::service_num_t sid, nodeid_t fault_nodeid, bool should_switch,
    svc_state_list_t *svc_state_list, bool promote)
{
	// Figure out which node we'd like to fault (Primary node)
	nodeid_t	orig_primary_nodeid;
	nodeid_t	new_primary_nodeid;
	nodeid_t	*orig_primary;
	nodeid_t	*new_primary;
	char		*bottom_svc;
	char		*top_svc;
	int		svc_count;
	int		rslt;
	int		counter;
	Environment	e;

	ASSERT(svc_state_list != NULL);
	ASSERT(svc_state_list->listlen > 0);
	ASSERT(svc_state_list->bottom_svc >= 0);
	ASSERT(svc_state_list->top_svc >= 0);

	svc_count = svc_state_list->listlen;
	bottom_svc = svc_state_list->list[svc_state_list->bottom_svc].name;
	top_svc = svc_state_list->list[svc_state_list->top_svc].name;

	ASSERT(bottom_svc != NULL);
	ASSERT(top_svc != NULL);

	// Allocate an array of enough size to hold the nodeid's of each
	// service
	orig_primary = new nodeid_t[svc_count];
	for (counter = 0; counter < svc_count; counter++) {
		if ((orig_primary[counter] = get_primary(
		    svc_state_list->list[counter].name)) == 0) {
			delete []orig_primary;
			return (1);
		}
		os::printf("    Orig. %s primary is node: %d\n",
		    svc_state_list->list[counter].name,
		    orig_primary[counter]);
	}

	// Check that all provs are on the same node
	orig_primary_nodeid = orig_primary[0];
	for (counter = 1; counter < svc_count; counter++) {
		if (orig_primary[counter] != orig_primary_nodeid) {
			os::printf("+++ FAIL: All Provs are not on "
			    "same node\n");
			delete []orig_primary;
			return (1);
		}
	}

	// Create an mc_sema object and intiialize it to 0
	mc_sema::sema_var	mc_sema_ref;
	mc_sema_ref = (new mc_sema_impl)->get_objref();
	ASSERT(!CORBA::is_nil(mc_sema_ref));
	mc_sema_ref->init(0, e);
	if (e.exception()) {
		e.exception()->print_exception("mc_sema::init");
		os::printf("+++ FAIL: Exception was not expected\n");
		return (1);
	}

	// Populate fault arguments
	dep_fault_arg_t			fault_arg;
	fault_arg.reboot_arg.sid	= sid;
	fault_arg.reboot_arg.nodeid	= fault_nodeid;
	fault_arg.reboot_arg.op		= FaultFunctions::WAIT_FOR_UNKNOWN;
	fault_arg.top_sid		= get_sid(top_svc);
	fault_arg.sema_id		= mc_sema_ref->get_cookie(e);
	if (e.exception()) {
		e.exception()->print_exception("mc_sema::get_cookie");
		return (1);
	}
	ASSERT(fault_arg.sema_id != 0);

	// Check that we have a valid sid to fault
	if (fault_arg.top_sid == 0) {
		return (1);
	}

	if (promote) {
		rslt = change_sec_state(
		    f_on,
		    f_off,
		    &fault_arg,
		    (int)sizeof (fault_arg),
		    bottom_svc,
		    replica::SC_SET_PRIMARY,
		    e);
	} else {
		rslt = change_primary_state(
		    f_on,
		    f_off,
		    &fault_arg,
		    (int)sizeof (fault_arg),
		    bottom_svc,
		    replica::SC_SET_SECONDARY,
		    e);
	}

	if (e.exception()) {
		if ((replica::failed_to_switchover::_exnarrow(e.exception())
		    != NULL) && !should_switch) {
			rslt = 0;
			e.clear();
		} else {
			e.exception()->print_exception("change_sec_state()");
			os::printf("+++ FAIL: Exception was not expected\n");
			return (1);
		}
	}

	// Do a sema_p operation on the mc_sema we created, which will
	// synchronize with the fault point in the up state of the
	// HA RM
	//
	// Until the corresponding sema_v operation is done, we will
	// block here
	os::printf(" ** Will do a sema_p.  Waiting for top service to reach"
	    " service_up state\n");
	mc_sema_ref->sema_p(e);
	if (e.exception()) {
		e.exception()->print_exception("mc_sema::sema_p");
		os::printf("+++ FAIL: Exception was not expected\n");
		return (1);
	}
	os::printf(" ** Top level svc has reaced service_up state\n");

	// Destroy the mc_sema object, to release the resources
	// it has allocated on the mc_sema server
	mc_sema_ref->destroy(e);
	if (e.exception()) {
		e.exception()->print_exception("mc_sema::destroy");
		os::printf("+++ FAIL: Exception was not expected\n");
		return (1);
	}

	for (counter = 0; counter < svc_count; counter++) {
		if (get_state(svc_state_list->list[counter].name) !=
		    svc_state_list->list[counter].state) {
			os::printf("+++ FAIL: %s in state %d, should be %d\n",
			    svc_state_list->list[counter].name,
			    get_state(svc_state_list->list[counter].name),
			    svc_state_list->list[counter].state);
			delete []orig_primary;
			return (1);
		} // if state
	}

	// Allocate an array of enough size to hold the nodeid's of each
	// service
	new_primary = new nodeid_t[svc_count];
	for (counter = 0; counter < svc_count; counter++) {
		// Dont look up the primary if its not supposed to be up
		if (svc_state_list->list[counter].state != replica::S_UP)
			continue;

		if ((new_primary[counter] = get_primary(
		    svc_state_list->list[counter].name)) == 0) {
			delete []orig_primary;
			delete []new_primary;
			return (1);
		}
		os::printf("    New %s primary is node: %d\n",
		    svc_state_list->list[counter].name,
		    new_primary[counter]);
	}

	// Check that all provs are on the same node
	for (counter = 0; counter < svc_count; counter++) {
		// Dont compare if its not supposed to be up
		if (svc_state_list->list[counter].state != replica::S_UP)
			continue;

		new_primary_nodeid = new_primary[counter];
		counter++;
		break;
	}
	for (; counter < svc_count; counter++) {
		// Dont compare if its not supposed to be up
		if (svc_state_list->list[counter].state != replica::S_UP)
			continue;

		if (new_primary[counter] != new_primary_nodeid) {
			os::printf("+++ FAIL: All Provs did not end up on "
			    "same node\n");
			delete []orig_primary;
			delete []new_primary;
			return (1);
		}
	}

	// Verify that the provider has switched nodes (or hasn't)
	// if its still up
	for (counter = 0; counter < svc_count; counter++) {
		// Dont compare if its not supposed to be up
		if (svc_state_list->list[counter].state != replica::S_UP)
			continue;

		if (should_switch) {
			if (orig_primary[counter] == new_primary[counter]) {
				os::printf("+++ FAIL: Prov %s did not switch "
				    "nodes!\n",
				    svc_state_list->list[counter].name);
				delete []orig_primary;
				delete []new_primary;
				return (1);
			}
		} else {
			if (orig_primary[counter] != new_primary[counter]) {
				os::printf("+++ FAIL: Prov %s switched nodes "
				    "(it shouldn't)!\n",
				    svc_state_list->list[counter].name);
				delete []orig_primary;
				delete []new_primary;
				return (1);
			}
		} // should switch
	} // counter

	os::printf("--- PASS: Switchover was succesful\n");
	delete []orig_primary;
	delete []new_primary;
	return (0);
}

#endif // _FAULT_INJECTION
