/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)persist_store.cc	1.9	08/05/20 SMI"

#include 	<sys/os.h>
#include	<h/naming.h>
#include	<h/repl_sample.h>
#include	<nslib/ns.h>
#include	<orbtest/repl_sample/repl_sample_impl.h>

#include	<orbtest/repl_sample/persist_store.h>

int
place_store()
{
	os::printf("place_store()\n");

	Environment e;
	persistent_store_impl * storep = new persistent_store_impl;
	repl_sample::persistent_store_var store_ref = storep->get_objref();

	// Bind the persist store object to the root name server
	naming::naming_context_var ctxp = ns::root_nameserver();

	ASSERT(!CORBA::is_nil(ctxp));
	ctxp->rebind("ha sample persist store", store_ref, e);
	ASSERT(!e.exception());

	return (0);
}
