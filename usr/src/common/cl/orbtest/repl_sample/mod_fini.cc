/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)mod_fini.cc	1.6	08/05/20 SMI"

#include 	<sys/os.h>
#include	<h/naming.h>
#include	<orb/invo/common.h>
#include	<sys/rm_util.h>
#include	<nslib/ns.h>

#ifdef _KERNEL_ORB
#include	<orbtest/repl_sample/repl_sample_impl.h>
#include	<sys/modctl.h>
#endif

#include	<orbtest/repl_sample/repl_sample_clnt.h>
#include	<orbtest/repl_tests/repl_test_switch.h>
#include	<orbtest/rm_probe/rm_snap.h>
#include	<h/replica.h>
#include	<orb/fault/fault_injection.h>

#include	<orbtest/repl_sample/repl_sample_clnt.h>


void
mod_fini_wait()
{
#ifdef _KERNEL

#ifdef FAULT_INJECTION

	struct modinfo minfo;

	_info(&minfo);

	//
	// Make sure that the provider(s) created by this modules have
	// been unreferenced before unloading the module. Not doing so
	// causes the kernel to refer to unmapped addresses causing
	// panics.
	//
	for (; ; ) {
		SList<prov_in_mod_t>::ListIterator
			iter(repl_sample_prov::repl_sample_prov_list);

		prov_in_mod_t *pp;
		bool found_prov = false;

		repl_sample_prov::prov_list_lock.lock();

		while ((pp = iter.get_current()) != NULL) {
			if (pp->mod_id == minfo.mi_id) {
				found_prov = true;
				break;
			}
			iter.advance();
		}
		repl_sample_prov::prov_list_lock.unlock();

		if (!found_prov) {
			break;
		} else {
			os::printf("%s: Waiting for all repl_sample_provs "
			    "to be unreferenced.\n", minfo.mi_name);
			os::usecsleep(10 * 1000000); // 10s
		}
	}

#endif // FAULT_INJECTION
#endif // _KERNEL
}
