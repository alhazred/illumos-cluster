/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)driver.cc	1.49	08/05/20 SMI"

#if defined(_KERNEL)
#include <sys/modctl.h>
#include <orbtest/repl_sample/repl_sample_clnt.h>
#include <orbtest/repl_sample/mod_fini.h>

#else
#include <orb/infrastructure/orb.h>
#endif

#include <orbtest/fi_support/fi_driver.h>

#include <orbtest/repl_sample/faults/entry_basic.h>
#include <orbtest/repl_sample/faults/entry_one_phase_idempotent.h>
#include <orbtest/repl_sample/faults/entry_one_phase_non_idempotent.h>
#include <orbtest/repl_sample/faults/entry_two_phase.h>
#include <orbtest/repl_sample/faults/entry_switchover_sec_pri.h>
#include <orbtest/repl_sample/faults/entry_register.h>
#include <orbtest/repl_sample/faults/entry_stress.h>
#include <orbtest/repl_sample/faults/entry_dep.h>
#include <orbtest/repl_sample/faults/entry_dep_shutdown.h>
#include <orbtest/repl_sample/faults/entry_shutdown.h>
#include <orbtest/repl_sample/faults/entry_sec.h>
#include <orbtest/repl_sample/faults/entry_invoke.h>
#include <orbtest/repl_sample/faults/entry_disconnect_clnt.h>
#include <orbtest/repl_sample/faults/entry_ha_rm.h>
#include <orbtest/repl_sample/faults/entry_change_desired_numsecs.h>
#include <orbtest/repl_sample/faults/entry_change_prov_priority.h>
#include <orbtest/repl_sample/faults/entry_switchover_prov_state.h>
#include <orbtest/repl_sample/faults/svc_prov_status.h>
#include <orbtest/repl_sample/faults/entry_ru.h>

// List of test entry functions.
fi_entry_t fi_entry[] = {
	ENTRY(basic_1_a),
	ENTRY(basic_1_b),
	ENTRY(basic_1_c),
	ENTRY(basic_1_d),
	ENTRY(basic_1_e),
	ENTRY(basic_1_f),
	ENTRY(basic_2_a),
	ENTRY(basic_2_c),
	ENTRY(basic_3_a),
	ENTRY(basic_3_b),
	ENTRY(basic_3_c),
	ENTRY(basic_3_d),
	ENTRY(one_phase_idempotent_1_a),
	ENTRY(one_phase_idempotent_1_b),
	ENTRY(one_phase_idempotent_1_i),
	ENTRY(one_phase_idempotent_2_a),
	ENTRY(one_phase_idempotent_2_b),
	ENTRY(one_phase_idempotent_2_c),
	ENTRY(one_phase_idempotent_2_d),
	ENTRY(one_phase_idempotent_2_e),
	ENTRY(one_phase_idempotent_2_g),
	ENTRY(one_phase_idempotent_2_h),
	ENTRY(one_phase_idempotent_2_i),
	ENTRY(one_phase_idempotent_3_a),
	ENTRY(one_phase_idempotent_3_b),
	ENTRY(one_phase_idempotent_3_c),
	ENTRY(one_phase_idempotent_3_d),
	ENTRY(one_phase_idempotent_3_e),
	ENTRY(one_phase_idempotent_3_f),
	ENTRY(one_phase_idempotent_3_g),
	ENTRY(one_phase_idempotent_3_h),
	ENTRY(one_phase_idempotent_3_i),
	ENTRY(one_phase_non_idempotent_1_a),
	ENTRY(one_phase_non_idempotent_1_b),
	ENTRY(one_phase_non_idempotent_1_i),
	ENTRY(one_phase_non_idempotent_2_a),
	ENTRY(one_phase_non_idempotent_2_b),
	ENTRY(one_phase_non_idempotent_2_c),
	ENTRY(one_phase_non_idempotent_2_d),
	ENTRY(one_phase_non_idempotent_2_e),
	ENTRY(one_phase_non_idempotent_2_g),
	ENTRY(one_phase_non_idempotent_2_h),
	ENTRY(one_phase_non_idempotent_2_i),
	ENTRY(one_phase_non_idempotent_3_a),
	ENTRY(one_phase_non_idempotent_3_b),
	ENTRY(one_phase_non_idempotent_3_c),
	ENTRY(one_phase_non_idempotent_3_d),
	ENTRY(one_phase_non_idempotent_3_e),
	ENTRY(one_phase_non_idempotent_3_f),
	ENTRY(one_phase_non_idempotent_3_g),
	ENTRY(one_phase_non_idempotent_3_h),
	ENTRY(one_phase_non_idempotent_3_i),
	ENTRY(two_phase_1_a),
	ENTRY(two_phase_1_q),
	ENTRY(two_phase_2_a),
	ENTRY(two_phase_2_b),
	ENTRY(two_phase_2_c),
	ENTRY(two_phase_2_d),
	ENTRY(two_phase_2_e),
	ENTRY(two_phase_2_f),
	ENTRY(two_phase_2_g),
	ENTRY(two_phase_2_h),
	ENTRY(two_phase_2_m),
	ENTRY(two_phase_2_n),
	ENTRY(two_phase_2_o),
	ENTRY(two_phase_2_p),
	ENTRY(two_phase_2_q),
	ENTRY(switchover_sec_pri_1_a),
	ENTRY(switchover_sec_pri_1_b),
	ENTRY(switchover_sec_pri_1_c),
	ENTRY(switchover_sec_pri_1_d),
	ENTRY(switchover_sec_pri_1_e),
	ENTRY(switchover_sec_pri_1_f),
	ENTRY(switchover_sec_pri_1_g),
	ENTRY(switchover_sec_pri_1_h),
	ENTRY(switchover_sec_pri_1_i),
	ENTRY(switchover_sec_pri_1_j),
	ENTRY(switchover_sec_pri_1_k),
	ENTRY(switchover_sec_pri_1_l),
	ENTRY(switchover_sec_pri_1_m),
	ENTRY(switchover_sec_pri_1_n),
	ENTRY(switchover_sec_pri_1_o),
	ENTRY(switchover_sec_pri_1_p),
	ENTRY(switchover_sec_pri_1_q),
	ENTRY(switchover_sec_pri_1_r),
	ENTRY(switchover_sec_pri_1_s),
	ENTRY(switchover_sec_pri_1_t),
	ENTRY(switchover_sec_pri_2_a),
	ENTRY(switchover_sec_pri_2_b),
	ENTRY(switchover_sec_pri_2_c),
	ENTRY(switchover_sec_pri_2_d),
	ENTRY(switchover_sec_pri_2_e),
	ENTRY(switchover_sec_pri_2_f),
	ENTRY(switchover_sec_pri_2_g),
	ENTRY(switchover_sec_pri_2_h),
	ENTRY(switchover_sec_pri_2_i),
	ENTRY(switchover_sec_pri_2_j),
	ENTRY(switchover_sec_pri_2_k),
	ENTRY(switchover_sec_pri_2_l),
	ENTRY(switchover_sec_pri_2_m),
	ENTRY(switchover_sec_pri_2_n),
	ENTRY(switchover_sec_pri_2_o),
	ENTRY(switchover_sec_pri_2_p),
	ENTRY(switchover_sec_pri_2_q),
	ENTRY(switchover_sec_pri_2_r),
	ENTRY(switchover_sec_pri_2_s),
	ENTRY(switchover_sec_pri_2_t),
	ENTRY(register_1_a),
	ENTRY(stress_1),
	ENTRY(dep_1_a),
	ENTRY(dep_1_b),
	ENTRY(dep_1_c),
	ENTRY(dep_1_d),
	ENTRY(dep_1_e),
	ENTRY(dep_1_f),
	ENTRY(dep_1_g),
	ENTRY(dep_1_h),
	ENTRY(dep_1_i),
	ENTRY(dep_1_j),
	ENTRY(dep_1_k),
	ENTRY(dep_1_l),
	ENTRY(dep_1_m),
	ENTRY(dep_1_n),
	ENTRY(dep_2_a),
	ENTRY(dep_2_b),
	ENTRY(dep_2_d),
	ENTRY(dep_2_g),
	ENTRY(dep_2_h),
	ENTRY(dep_2_j),
	ENTRY(dep_2_m),
	ENTRY(dep_2_n),
	ENTRY(dep_3_a),
	ENTRY(dep_3_b),
	ENTRY(dep_3_d),
	ENTRY(dep_3_g),
	ENTRY(dep_3_h),
	ENTRY(dep_3_j),
	ENTRY(dep_3_m),
	ENTRY(dep_3_n),
	ENTRY(dep_4_a),
	ENTRY(dep_4_b),
	ENTRY(dep_4_d),
	ENTRY(dep_4_g),
	ENTRY(dep_4_h),
	ENTRY(dep_4_j),
	ENTRY(dep_4_m),
	ENTRY(dep_4_n),
	ENTRY(dep_5_a),
	ENTRY(dep_5_b),
	ENTRY(dep_5_d),
	ENTRY(dep_5_g),
	ENTRY(dep_5_h),
	ENTRY(dep_5_j),
	ENTRY(dep_5_m),
	ENTRY(dep_5_n),
	ENTRY(dep_shutdown_1_a),
	ENTRY(dep_shutdown_1_b),
	ENTRY(dep_shutdown_1_c),
	ENTRY(shutdown_1_a),
	ENTRY(shutdown_1_b),
	ENTRY(shutdown_2_a),
	ENTRY(shutdown_2_b),
	ENTRY(shutdown_2_c),
	ENTRY(shutdown_2_e),
	ENTRY(shutdown_3_a),
	ENTRY(shutdown_3_b),
	ENTRY(shutdown_3_c),
	ENTRY(shutdown_3_d),
	ENTRY(shutdown_3_e),
	ENTRY(shutdown_4_a),
	ENTRY(shutdown_4_b),
	ENTRY(shutdown_forced_1),
	ENTRY(shutdown_forced_2),
	ENTRY(shutdown_forced_3),
	ENTRY(shutdown_forced_4),
	ENTRY(sec_1_d),
	ENTRY(sec_1_e),
	ENTRY(sec_1_f),
	ENTRY(sec_1_g),
	ENTRY(sec_1_h),
	ENTRY(sec_1_i),
	ENTRY(sec_1_j),
	ENTRY(sec_1_k),
	ENTRY(sec_1_l),
	ENTRY(sec_1_m),
	ENTRY(sec_1_n),
	ENTRY(sec_1_o),
	ENTRY(sec_1_p),
	ENTRY(sec_1_q),
	ENTRY(sec_1_r),
	ENTRY(sec_2_a),
	ENTRY(sec_2_b),
	ENTRY(sec_2_c),
	ENTRY(sec_2_d),
	ENTRY(sec_2_e),
	ENTRY(sec_2_f),
	ENTRY(sec_2_g),
	ENTRY(sec_2_h),
	ENTRY(sec_2_i),
	ENTRY(sec_2_j),
	ENTRY(sec_2_k),
	ENTRY(sec_2_l),
	ENTRY(sec_2_m),
	ENTRY(sec_2_n),
	ENTRY(sec_2_o),
	ENTRY(sec_2_p),
	ENTRY(sec_2_q),
	ENTRY(sec_2_r),
	ENTRY(sec_3_a),
	ENTRY(sec_3_b),
	ENTRY(sec_3_c),
	ENTRY(state_mach_basic),
	ENTRY(state_mach_connectprimary),
	ENTRY(state_mach_shutdown),
	ENTRY(state_mach_markdown),
	ENTRY(state_mach_addservice),
	ENTRY(chkpt_register_service_1),
	ENTRY(chkpt_register_service_2),
	ENTRY(chkpt_add_rma_1),
	ENTRY(chkpt_add_rma_2),
	ENTRY(chkpt_shutdown_1),
	ENTRY(chkpt_shutdown_2),
	ENTRY(chkpt_register_repl_prov_1),
	ENTRY(chkpt_register_repl_prov_2),
	ENTRY(chkpt_get_control_1),
	ENTRY(chkpt_get_control_2),
	ENTRY(chkpt_shutdown_3),
	ENTRY(chkpt_shutdown_4),
	ENTRY(invoke_1_a),
	ENTRY(invoke_1_b),
	ENTRY(make_invo_drop_tr_info),
	ENTRY(make_invo_to_kill),
	ENTRY(change_desired_numsecs_no_fault),
	ENTRY(change_desired_numsecs_fault_a),
	ENTRY(change_desired_numsecs_fault_b),
	ENTRY(change_desired_numsecs_fault_c),
	ENTRY(change_desired_numsecs_fault_d),
	ENTRY(change_desired_numsecs_fault_e),
	ENTRY(change_desired_numsecs_fault_f),
	ENTRY(change_prov_priority_no_fault),
	ENTRY(change_prov_priority_fault_a),
	ENTRY(change_prov_priority_fault_b),
	ENTRY(change_prov_priority_fault_c),
	ENTRY(change_prov_priority_fault_d),
	ENTRY(change_prov_priority_fault_e),
	ENTRY(change_prov_priority_fault_f),
	ENTRY(switchover_prov_state_no_fault),
	ENTRY(switchover_prov_state_fault_a),
	ENTRY(switchover_prov_state_fault_b),
	ENTRY(switchover_prov_state_fault_c),
	ENTRY(switchover_prov_state_fault_d),
	ENTRY(switchover_prov_state_fault_e),
	ENTRY(switchover_prov_state_fault_f),
	ENTRY(switchover_prov_state_fault_g),
	ENTRY(verify_svc_up),
	ENTRY(verify_svc_down),
	ENTRY(verify_prov_status),
	ENTRY(verify_svc_frozen_for_upgrade),
	ENTRY(freeze_service),
	ENTRY(unfreeze_service),
	ENTRY(kill_svc_rm_primary),
	ENTRY(NULL)
};


#if ! defined(_KERNEL)
int
main(int argc, char **argv)
{
	int	rslt;

	// Initialize orb.
	if (ORB::initialize() != 0) {
		os::printf("Failed to initialize orb\n");
		return (1);
	}

	rslt = do_fi_test(argc, argv, fi_entry);
	return (rslt);
}

#else // ! _KERNEL

extern "C" {
	void _cplpl_init(void);
	void _cplpl_fini(void);
}

char _depends_on[] = "misc/cl_runtime misc/cl_load misc/cl_orb "
	"misc/cl_haci misc/cl_quorum misc/cl_comm "
	"misc/repl_sample_common misc/cl_dcs misc/fi_driver";

extern struct mod_ops mod_miscops;

struct modlmisc modlmisc = {
	&mod_miscops, "ha fault injection driver"
};

struct modlinkage modlinkage = {
	MODREV_1, { (void *)&modlmisc, NULL }
};

int
_init(void)
{
	int		error;
	struct modctl	*mp;
	int		argc, i;
	char		**argv;

	if (error = mod_install(&modlinkage)) {
		return (error);
	}
	_cplpl_init();				// C++ initialization

	// Get module name so we know what test case to call
	if ((mp = mod_getctl(&modlinkage)) == NULL) {
		os::printf("Can't find modctl\n");
		_cplpl_fini();
		mod_remove(&modlinkage);
		return (EIO);
	}

	// Parse module name to get arguments
	mod_name_to_args(mp->mod_modname, argc, argv);

	// Do the test.
	error = do_fi_test(argc, argv, fi_entry);
	os::printf("    Result From Module: %d\n", error);

	// Clean up argument vectors allocated by mod_name_to_args().
	for (i = 0; i < argc; ++i) {
		delete[] argv[i];
	}
	delete[] argv;
	return (error);
}

int
_fini(void)
{
	mod_fini_wait();
	_cplpl_fini();
	return (mod_remove(&modlinkage));
}

int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}

#endif // ! _KERNEL
