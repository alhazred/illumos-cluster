/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)mc_sema_server.cc	1.14	08/05/20 SMI"

#include 	<sys/os.h>
#include	<h/naming.h>
#include	<nslib/ns.h>
#include	<h/mc_sema.h>
#include	<sys/mc_sema_impl.h>

// Install mc_sema server on root name server
int
install_server()
{
	os::printf("Installing mc_sema server\n");

	Environment e;
	mc_sema::server_var	server_ref;
	server_ref = (new mc_sema_server_impl)->get_objref();
	ASSERT(!CORBA::is_nil(server_ref));

	naming::naming_context_ptr ctxp;

	// Bind the mc_sema server object to the root nameserver

	ctxp = ns::root_nameserver();
	ctxp->rebind("mc_sema_server", server_ref, e);
	CORBA::release(ctxp);
	ASSERT(!e.exception());

	// Also bind it to all the local name servers

	for (nodeid_t node = 1; node <= NODEID_MAX; node++) {
		ctxp = ns::local_nameserver(node);
		if (CORBA::is_nil(ctxp)) {
			continue;
		}
		ctxp->rebind("mc_sema_server", server_ref, e);
		CORBA::release(ctxp);

		if (e.exception()) {
			e.clear();
			continue;
		}
	}

	return (0);
}
