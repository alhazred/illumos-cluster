/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _HATYPES_IDLVEROBJ_IMPL_H
#define	_HATYPES_IDLVEROBJ_IMPL_H

#pragma ident	"@(#)idlverObj_impl.h	1.8	08/05/20 SMI"

#include <orb/invo/common.h>
#include <sys/os.h>

#ifdef ORBTEST_V1
#include <h/hatypetest_1.h>
#else
#include <h/hatypetest_0.h>
#endif

#include <repl/service/replica_tmpl.h>
#include <repl/service/transaction_state.h>
#include <orb/object/adapter.h>

#include <orbtest/hatypes/impl_common.h>
#include <orbtest/hatypes/hatypetest_prov.h>

#if defined(_KERNEL_ORB) && !defined(_KERNEL)
#define	_UNODE
#endif

class hatypetest_prov;

class minverObjA_impl :
    public mc_replica_of<hatypetest::minverObjA>,
    public impl_common {
public:

	// Constructor for primary.
	minverObjA_impl(hatypetest_prov *server);

	// Constructur for secondary.
	minverObjA_impl(hatypetest::minverObjA_ptr ref);

	~minverObjA_impl();

	void _unreferenced(unref_t);

	// Interface operations.
	int32_t	id(Environment &);
	short version(Environment &);
	char name(Environment &);
	void done(Environment &);
#ifdef ORBTEST_V1
	void setid(int32_t id, Environment &);
	void setname(const char *newname, Environment &);
	void _generic_method(CORBA::octet_seq_t &, CORBA::object_seq_t &,
	    Environment &);
#endif
	bool init();
	int32_t	idnum;

protected:
	bool init_called;
	bool done_called;
#ifdef ORBTEST_V1
	char myname[63];
#endif
};

#ifdef ORBTEST_V1
//
// Implementation object for version 1 of the minverObjB IDL interface
//
// Version 1 of the  minverObjB IDL interface inherits from version 1
// of the minverObjA IDL interface
//
class minverObjB_impl :
    public mc_replica_of<hatypetest::minverObjB>,
    public impl_common
{
public:
	// Constructor for primary.
	minverObjB_impl(hatypetest_prov *server);

	// Constructur for secondary.
	minverObjB_impl(hatypetest::minverObjB_ptr ref);

	~minverObjB_impl();

	void _unreferenced(unref_t);

	// Interface operations.
	int32_t	id(Environment &);
	char name(Environment &);
	void setid(int32_t id, Environment &);
	void setname(const char *newname, Environment &);
	short version(Environment &);
	void done(Environment &);

	bool init();
	int32_t	idnum;

protected:
	bool init_called;
	bool done_called;
	char myname[63];
};

//
// Implementation object for version 0 of the minverObjC IDL interface
//
// Version 0 of the  minverObjB IDL interface inherits from version 1
// of the minverObjA IDL interface
//
class minverObjC_impl :
    public mc_replica_of<hatypetest::minverObjC>,
    public impl_common
{
public:
	// Constructor for primary.
	minverObjC_impl(hatypetest_prov *server);

	// Constructur for secondary.
	minverObjC_impl(hatypetest::minverObjC_ptr ref);

	~minverObjC_impl();

	void _unreferenced(unref_t);

	// Interface operations.
	int32_t	id(Environment &);
	char name(Environment &);
	void setid(int32_t id, Environment &);
	void setname(const char *newname, Environment &);
	short version(Environment &);
	void done(Environment &);

	bool init();
	int32_t	idnum;

protected:
	bool init_called;
	bool done_called;
	char myname[63];
};

//
// Implementation object for version 0 of the combinedObjD IDL interface
//
// Version 0 of the combinedObjD IDL interface inherits from version 1
// of the minverObjA IDL interface and version 0 of the majverObjB
// interface.
//
class combinedObjD_impl :
    public mc_replica_of<hatypetest::combinedObjD>,
    public impl_common
{
public:
	// Constructor for primary.
	combinedObjD_impl(hatypetest_prov *server);

	// Constructur for secondary.
	combinedObjD_impl(CORBA::Object_ptr ref);

	~combinedObjD_impl();

	void _unreferenced(unref_t);

	// Interface operations.
	void getid(int32_t &id, Environment &);
	void getname(CORBA::String_out strbuf, Environment &);
	int32_t	id(Environment &);
	char name(Environment &);
	void setid(int32_t id, Environment &);
	void setname(const char *newname, Environment &);
	short version(Environment &);
	void done(Environment &);

	bool init();
	int32_t	idnum;

protected:
	bool init_called;
	bool done_called;
	char myname[63];
};

#endif	// ORBTEST_V1

#endif	/* _HATYPES_IDLVEROBJ_IMPL_H */
