/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)idlverObj_impl.cc	1.7	08/05/20 SMI"

#include <orbtest/hatypes/idlverObj_impl.h>

minverObjA_impl::minverObjA_impl(hatypetest_prov *server) :
    mc_replica_of<hatypetest::minverObjA>(server),
    idnum(1234)
{
	init_called = false;
	done_called = false;
}

minverObjA_impl::minverObjA_impl(hatypetest::minverObjA_ptr ref) :
    mc_replica_of<hatypetest::minverObjA>(ref),
    idnum(1234)
{
	init_called = false;
	done_called = false;
}

minverObjA_impl::~minverObjA_impl()
{
}

void
minverObjA_impl::_unreferenced(unref_t cookie)
{
	if (_last_unref(cookie))
		unref_called(true);
}


int32_t
minverObjA_impl::id(Environment &)
{
	return (idnum);
}


short
minverObjA_impl::version(Environment &)
{
#ifdef ORBTEST_V0
	return (0);
#else
	return (1);
#endif // ORBTEST_V0
}


char
minverObjA_impl::name(Environment &)
{
	return ('A');
}


//
// Initialize implementation object
//
// returns: true, if initialization succeeded
//			false, otherwise
bool
minverObjA_impl::init()
{
	os::printf("minverObjA_impl::init() called\n");

	// Ignore if called more than once.
	if (init_called)
		return (true);

	// - get reference to server object
	// - register server with the ORB name server
	// - release reference

#ifdef ORBTEST_V1
	// Create a reference for the version of the protocol we are running.
	hatypetest_prov *provp = (hatypetest_prov *)get_provider();
	CORBA::type_info_t *typeinfo =
	    hatypetest::minverObjA::_get_type_info(provp->service_version);

	hatypetest::minverObjA_var tmpref2 = get_objref(typeinfo);
	os::printf("Created proxy of type %s\n",
	    ((InterfaceDescriptor*)typeinfo)->get_name());
#else
	hatypetest::minverObjA_var tmpref2 = get_objref();
#endif // ORBTEST_V1

#if defined(_KERNEL_ORB)
	init_called = register_obj(tmpref2, Akserver_name);
	os::printf("minverObjA_impl::init(%s) %d called\n", Akserver_name,
	    (int)init_called);
#else
	init_called = register_obj(tmpref2, Auserver_name);
	os::printf("minverObjA_impl::init(%s) %d called\n", Auserver_name,
	    (int)init_called);
#endif

	return (init_called);
}

#ifdef ORBTEST_V1
void
minverObjA_impl::setid(int32_t id, Environment &)
{
	idnum = id;
}


void
minverObjA_impl::setname(const char *newname, Environment &)
{
	(void) os::strcpy(myname, newname);
}

void
minverObjA_impl::_generic_method(CORBA::octet_seq_t &data,
    CORBA::object_seq_t &objs, Environment &e)
{
	os::printf("minverObjA_impl::_generic_data() called\n");

	// The driver should have passed some data.
	if (data.length() != 8 ||
	    os::strcmp((const char *)data.buffer(), "driverX") != 0) {
		e.system_exception(CORBA::VERSION(0, CORBA::COMPLETED_NO));
		return;
	}
	// The driver should have passed a pointer to ourself.
	if (objs.length() != 1 || !_equiv(objs[0])) {
		e.system_exception(CORBA::VERSION(0, CORBA::COMPLETED_NO));
		return;
	}
	e;
	data.load(9, 9, (uint8_t *)"server_A", false);
}
#endif

//
// Test clients invoke this when they're finished with this object.
//
void
minverObjA_impl::done(Environment &)
{

	// Ignore if invoked more than once.
	if (done_called)
		return;

	// Unregister from the name server.
	if (unregister_obj()) {
		os::printf("minverObjA_impl::done() called\n");
		done_called = true;
	} else
		os::printf("minverObjA_impl::done unregister failed\n");
}


#ifdef ORBTEST_V1
// ------------------------------------------------------------------------
// minverObjB implementation
// ------------------------------------------------------------------------
minverObjB_impl::minverObjB_impl(hatypetest_prov *server) :
    mc_replica_of<hatypetest::minverObjB>(server),
    idnum(2345)
{
	init_called = false;
	done_called = false;
}

minverObjB_impl::minverObjB_impl(hatypetest::minverObjB_ptr ref) :
    mc_replica_of<hatypetest::minverObjB>(ref),
    idnum(1234)
{
	init_called = false;
	done_called = false;
}

minverObjB_impl::~minverObjB_impl()
{
}

void
minverObjB_impl::_unreferenced(unref_t cookie)
{
	if (_last_unref(cookie))
		unref_called(true);
}


short
minverObjB_impl::version(Environment &)
{
#ifdef ORBTEST_V0
	return (0);
#else
	return (1);
#endif // ORBTEST_V0
}


int32_t
minverObjB_impl::id(Environment &)
{
	return (idnum);
}



char
minverObjB_impl::name(Environment &)
{
	return ('B');
}

void
minverObjB_impl::setid(int32_t id, Environment &)
{
	idnum = id;
}


void
minverObjB_impl::setname(const char *newname, Environment &)
{
	(void) os::strcpy(myname, newname);
}

//
// Initialize implementation object
//
// returns: true, if initialization succeeded
// 		false, otherwise
bool
minverObjB_impl::init()
{
	hatypetest::minverObjB_var tmpref;

	// Ignore if called more than once.
	if (init_called)
		return (true);

	// - get reference to server object
	// - register server with the ORB name server
	// - release reference
	tmpref = get_objref();
#ifdef _KERNEL_ORB
	init_called = register_obj(tmpref, Bkserver_name);
#else
	init_called = register_obj(tmpref, Buserver_name);
#endif

	return (init_called);
}


//
// Test clients invoke this when they're finished with this object.
//
void
minverObjB_impl::done(Environment &)
{
	os::printf("minverObjB_impl::done() called\n");

	// Ignore if invoked more than once.
	if (done_called)
		return;

	// Unregister from the name server.
	if (unregister_obj())
		done_called = true;
}


// ------------------------------------------------------------------------
// minverObjC implementation
// ------------------------------------------------------------------------
minverObjC_impl::minverObjC_impl(hatypetest_prov *server) :
    mc_replica_of<hatypetest::minverObjC>(server),
    idnum(1234)
{
	init_called = false;
	done_called = false;
}

minverObjC_impl::minverObjC_impl(hatypetest::minverObjC_ptr ref) :
    mc_replica_of<hatypetest::minverObjC>(ref),
    idnum(1234)
{
	init_called = false;
	done_called = false;
}

minverObjC_impl::~minverObjC_impl()
{
}

void
minverObjC_impl::_unreferenced(unref_t cookie)
{
	if (_last_unref(cookie))
		unref_called(true);
}


short
minverObjC_impl::version(Environment &)
{
	return (0);
}

int32_t
minverObjC_impl::id(Environment &)
{
	return (idnum);
}



char
minverObjC_impl::name(Environment &)
{
	return ('C');
}

void
minverObjC_impl::setid(int32_t id, Environment &)
{
	idnum = id;
}


void
minverObjC_impl::setname(const char *newname, Environment &)
{
	(void) os::strcpy(myname, newname);
}


//
// Initialize implementation object
//
// returns: true, if initialization succeeded
// 		false, otherwise
bool
minverObjC_impl::init()
{
	hatypetest::minverObjC_var tmpref;

	// Ignore if called more than once.
	if (init_called)
		return (true);

	// - get reference to server object
	// - register server with the ORB name server
	// - release reference
	tmpref = get_objref();
#ifdef _KERNEL_ORB
	init_called = register_obj(tmpref, Ckserver_name);
#else
	init_called = register_obj(tmpref, Cuserver_name);
#endif

	return (init_called);
}


//
// Test clients invoke this when they're finished with this object.
//
void
minverObjC_impl::done(Environment &)
{
	os::printf("minverObjC_impl::done() called\n");

	// Ignore if invoked more than once.
	if (done_called)
		return;

	// Unregister from the name server.
	if (unregister_obj())
		done_called = true;
}


// ------------------------------------------------------------------------
// combinedObjD implementation
// ------------------------------------------------------------------------
combinedObjD_impl::combinedObjD_impl(hatypetest_prov *server) :
    mc_replica_of<hatypetest::combinedObjD>(server),
    idnum(3456)
{
	init_called = false;
	done_called = false;
}

combinedObjD_impl::combinedObjD_impl(CORBA::Object_ptr ref) :
    mc_replica_of<hatypetest::combinedObjD>(ref),
    idnum(1234)
{
	init_called = false;
	done_called = false;
}

combinedObjD_impl::~combinedObjD_impl()
{
}

void
combinedObjD_impl::_unreferenced(unref_t cookie)
{
	if (_last_unref(cookie))
		unref_called(true);
}


short
combinedObjD_impl::version(Environment &)
{
	return (0);
}

void
combinedObjD_impl::getid(int32_t &id, Environment &)
{
	id = idnum;
}

void
combinedObjD_impl::getname(CORBA::String_out strbuf, Environment &)
{
	strbuf = os::strdup(myname);
}

int32_t
combinedObjD_impl::id(Environment &)
{
	return (idnum);
}


char
combinedObjD_impl::name(Environment &)
{
	return ('D');
}

void
combinedObjD_impl::setid(int32_t id, Environment &)
{
	idnum = id;
}


void
combinedObjD_impl::setname(const char *newname, Environment &)
{
	os::strcpy(myname, newname);
}


//
// Initialize implementation object
//
// returns: true, if initialization succeeded
// 		false, otherwise
bool
combinedObjD_impl::init()
{
	hatypetest::combinedObjD_var tmpref;

	// Ignore if called more than once.
	if (init_called)
		return (true);

	// - get reference to server object
	// - register server with the ORB name server
	// - release reference
	tmpref = get_objref();
#ifdef _KERNEL_ORB
	init_called = register_obj(tmpref->_this_obj(), Dkserver_name);
#else
	init_called = register_obj(tmpref->_this_obj(), Duserver_name);
#endif

	return (init_called);
}


//
// Test clients invoke this when they're finished with this object.
//
void
combinedObjD_impl::done(Environment &)
{
	os::printf("combinedObjD_impl::done() called\n");

	// Ignore if invoked more than once.
	if (done_called)
		return;

	// Unregister from the name server.
	if (unregister_obj())
		done_called = true;
}

#endif // ORBTEST_V1
