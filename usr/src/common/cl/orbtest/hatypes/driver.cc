/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)driver.cc	1.12	08/05/20 SMI"

#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <signal.h>

#include <sys/os.h>
#include <sys/rm_util.h>
#include <nslib/ns.h>

#ifdef ORBTEST_V1
#include <h/hatypetest_1.h>
#else
#include <h/hatypetest_0.h>
#endif

#include <orbtest/hatypes/impl_common.h>

#include <h/network.h>

#if !defined(_UNODE)
#include <orb/infrastructure/orb.h>
#endif

static const char	prefix[] = "driver:";

// Ranges of number option arguments
enum Optarg_ranges {
	RANGE_ALL,			// all possible (long) numbers
	RANGE_GT0,			// > 0
	RANGE_GE0,			// >= 0
	RANGE_LT0,			// < 0
	RANGE_LE0			// >= 0
};

// Whether or not an option argument is optional
enum Optarg_optional {
	OPTARG_OPTIONAL,
	OPTARG_REQUIRED
};

// Connect which test client with which test server.
enum OptConn {
	CONN_U2K = (1 << 1),		// USER   client <-> KERNEL server
	CONN_K2K = (1 << 2),		// KERNEL client <-> KERNEL server
	CONN_ALL = (~0)			// test all connection combinations
};

// Which test to run.
const uint64_t
	TEST_NOARG		= (1ULL << 0),	// no-argument

	TEST_BOOLEAN		= (1ULL << 1),	// Boolean (bool)
	TEST_CHAR		= (1ULL << 2),	// Char (int8_t)
	TEST_OCTET		= (1ULL << 3),	// Octet (uint8_t)
	TEST_SHORT		= (1ULL << 4),	// Short (int16_t)
	TEST_USHORT		= (1ULL << 5),	// UShort (uint16_t)
	TEST_LONG		= (1ULL << 6),	// Long (int32_t)
	TEST_ULONG		= (1ULL << 7),	// ULong (uint32_t)
	TEST_LONGLONG		= (1ULL << 8),	// LongLong (int64_t)
	TEST_ULONGLONG		= (1ULL << 9),	// ULongLong (uint64_t)

	TEST_BSTRING 		= (1ULL << 10),	// Bounded string
	TEST_USTRING 		= (1ULL << 11),	// Unbounded string
	TEST_BSEQ		= (1ULL << 12),	// Bounded sequence
	TEST_USEQ		= (1ULL << 13),	// Unbounded sequence
	TEST_BSEQUSEQ		= (1ULL << 14),	// Bounded seq of
						// unbounded sequences
	TEST_USEQBSEQ		= (1ULL << 15),	// Unbounded sequence of
						// bounded sequences
	TEST_BSEQUSTR		= (1ULL << 16),	// Bounded sequence of
						// unbounded string
	TEST_USEQBSTR		= (1ULL << 17),	// Unbounded sequence of
						// bounded string

	TEST_FSTRUCT 		= (1ULL << 18),	// Fixed struct
	TEST_VSTRUCT 		= (1ULL << 19),	// Variable struct

	TEST_FUNION  		= (1ULL << 20),	// Fixed union
	TEST_VUNION  		= (1ULL << 21),	// Variable union

	TEST_OBJREF		= (1ULL << 22),	// Object reference
	TEST_OBJSEQ		= (1ULL << 23),	// Object reference sequence

	// TEST_REVOKE		= (1ULL << 24), Revoke operation

	TEST_ARRAYSHORT		= (1ULL << 27),	// Array of Short (int16_t)
	TEST_ARRAYOCTET		= (1ULL << 28),	// Array of UShort (uint8_t)
						// (2D)
	TEST_ARRAYFSTRUCT	= (1ULL << 29),	// Array of Fstruct
	TEST_ARRAYFUNION	= (1ULL << 30),	// Array of Funion

	TEST_EMPTYEXCEPTION	= (1ULL << 31),	// Empty Exception
	TEST_BASICEXCEPTION	= (1ULL << 32),	// Basic Exception
	TEST_COMPLEXEXCEPTION	= (1ULL << 33),	// Complex Exception
	TEST_STRINGEXCEPTION	= (1ULL << 34),	// String Exception

	TEST_OBJINHSIMPLE	= (1ULL << 35), // Single inheritance,
						// one level in ancestry tree.

	TEST_OBJINHMANY		= (1ULL << 36), // Single inheritance,
						// many level in ancestry tree.

	TEST_OBJINHMULTIPLE	= (1ULL << 37), // Multiple-inherited object

	TEST_BULKIO_UIO_IN	= (1ULL << 38), // Bulkio raw I/O write
	TEST_BULKIO_UIO_INOUT	= (1ULL << 39), // Bulkio raw I/O read
	TEST_BULKIO_PAGES_IN	= (1ULL << 40), // Bulkio pages write
	TEST_BULKIO_PAGES_INOUT	= (1ULL << 41), // Bulkio pages read

	TEST_CRED		= (1ULL << 42),	// Cred Object
	TEST_DATA_CONTAINER	= (1ULL << 43),	// Data Container Object

	TEST_SEQ_READ		= (1ULL << 44),	// Unounded sequence read
	TEST_SEQ_WRITE		= (1ULL << 45),	// Unbounded sequence write
	TEST_SEQ_RW		= (1ULL << 46),	// Unbounded sequence read/write
	TEST_IDL_VERSIONING	= (1ULL << 47),	// IDL Versioning Tests
						// 48 unused
	TEST_FSTRUCTSEQ		= (1ULL << 49), // Fixed Structure of Sequences
	TEST_NULLSEQ		= (1ULL << 50), // Null Sequence
	TEST_0LENSEQ		= (1ULL << 51), // Zero-length sequence

	// Run all tests if no specific ones are specified  except:
	//
	//	TEST_REVOKE	-- revoke test should not be included in the
	//			all case, because it can only be done once.
	//
	//	TEST_SEQ_READ/WRITE/RW - these tests exist to support
	//			performance comparisons. The functionality
	//			is already covered by unbounded sequence test.
	//
	TEST_ALL	= ~(TEST_SEQ_READ |
			    TEST_SEQ_WRITE |
			    TEST_SEQ_RW);


// Option variables
struct Options {
	Options();

	bool		done;	// invoke done() on test servers and clients
	uint32_t	conn;	// client/server combination
	uint64_t	test;	// which test to run

	// Which test(s) and connection(s) are actually specified.  Allows
	// to skip some tests unless specified on command line.
	uint64_t	test_specified;
	uint32_t	conn_specified;

	long	iter;		// how many times to run each test
	bool	noverify;	// set if verification off server side

	long	bstring_len;
	long	ustring_len;
	long	bseq_len;
	long	useq_len;
	long	seq_read_len;
	long	seq_write_len;
	long	seq_rw_len;
	long	bsequseq_len, bsequseq_max;
	long	useqbseq_len, useqbseq_max;
	long	bsequstr_len, bsequstr_max;
	long	useqbstr_len, useqbstr_max;
	long	vstruct_len;
	long	vunion_len;
	long	objseq_len;
	long	fstructseq_len, fstructseq_max;
	long	nullseq_len, nullseq_max;
	long	zerolenseq_len, zerolenseq_max;

	long    in_uio_len;
	long	inout_uio_len;
	long    in_pages_noof;
	long	inout_pages_noof;
	long	data_len;
};

static bool	parse_options(int argc, char *argv[], Options &opts);
static int	numarg(
			long &var,
			const char *optstr,
			int argc,
			char *argv[],
			int optarg_idx,
			Optarg_ranges range,
			Optarg_optional optional);
static bool	doit(Options &opts);
static bool	test(
			const char *client_name,
			const char *server_name,
			Options &opts);
static void	done(const char *name);
static void	print_stats(int64_t total_time, int32_t num_iter);


Options::Options()
{
	done = false;
	conn = 0;
	test = 0;
	iter = 1;
	bstring_len = 0;
	ustring_len = 1;
	bseq_len	= 10;
	useq_len	= 10;
	seq_read_len	= 10;
	seq_write_len	= 10;
	seq_rw_len	= 10;
	bsequseq_len = 10;
	bsequseq_max = 10;
	useqbseq_len = 10;
	useqbseq_max = 10;
	bsequstr_len = 10;
	bsequstr_max = 0;
	useqbstr_len = 10;
	useqbstr_max = 1;
	fstructseq_len = 10;
	fstructseq_max = 10;
	nullseq_len = 10;
	nullseq_max = 10;
	zerolenseq_len = 10;
	zerolenseq_max = 10;
	vstruct_len  = 0;
	vunion_len = 1;
	in_uio_len = 8192;
	inout_uio_len = 8192;
	in_pages_noof = 1;
	inout_pages_noof = 1;
	objseq_len = 10;
	data_len = 100;
	noverify = false;
	test_specified = 0;
	conn_specified = 0;
}

#if defined(_UNODE)
int
unode_init(void)
{
	os::printf("HA Types test module loaded\n");
	return (0);
}
#endif

int
#if defined(_UNODE)
driver(int argc, char *argv[])
#else
main(int argc, char *argv[])
#endif
{
	Options	opts;

	if (! parse_options(argc, argv, opts)) {
		return (1);
	}

#if !defined(_UNODE)
	(void) printf("DRIVER: Initializing user-land orb\n");
	if (ORB::initialize() != 0) {
		(void) fprintf(stderr, "ERROR: Can't initialize ORB\n");
		return (1);
	}
#endif

	if (! doit(opts)) {
		return (1);
	}

	return (0);
}


//
// Parse options
//
static bool
parse_options(int argc, char *argv[], Options &opts)
{
	int	ret;

	opts.conn = 0;
	opts.test = 0;
	opts.noverify = false;

	for (int idx = 1; idx < argc; ++idx) {
		const char	*arg = argv[idx];

		// -noverify
		//
		//	Prevent test server for performing verification.
		//
		if (strcmp(arg, "-noverify") == 0) {
			(void) printf("No verify (-noverify) option set\n");
			opts.noverify = true;
			continue;
		}

		// -i    <num_iter>
		// -iter <num_iter>
		//
		//	How many times to run the tests.
		//
		if (strcmp(arg, "-i") == 0 || strcmp(arg, "-iter") == 0) {
			if (numarg(opts.iter, arg, argc, argv, ++idx,
			    RANGE_GE0, OPTARG_REQUIRED) < 0) {
				return (false);
			}
			continue;
		}

#if !defined(_UNODE)
		// -u2k
		//
		//	Test USER client <-> KERNEL server.
		//
		if (strcmp(arg, "-u2k") == 0) {
			opts.conn |= CONN_U2K;
			continue;
		}

		// -k2k
		//
		//	Test KERNEL client <-> KERNEL server.
		//
		if (strcmp(arg, "-k2k") == 0) {
			opts.conn |= CONN_K2K;
			continue;
		}
#endif

		// -noarg
		//
		//	No-argument test.
		//
		if (strcmp(arg, "-noarg") == 0) {
			opts.test |= TEST_NOARG;
			continue;
		}

		// -bool
		// -boolean
		//
		//	Boolean (bool) test.
		//
		if (strcmp(arg, "-bool") == 0 || strcmp(arg, "-boolean") == 0) {
			opts.test |= TEST_BOOLEAN;
			continue;
		}

		// -char
		//
		//	Char (int8_t) test.
		//
		if (strcmp(arg, "-char") == 0) {
			opts.test |= TEST_CHAR;
			continue;
		}

		// -octet
		//
		//	Octet (uint8_t) test.
		//
		if (strcmp(arg, "-octet") == 0) {
			opts.test |= TEST_OCTET;
			continue;
		}

		// -short
		//
		//	Short (int16_t) test.
		//
		if (strcmp(arg, "-short") == 0) {
			opts.test |= TEST_SHORT;
			continue;
		}

		// -ushort
		//
		//	UShort (uint16_t) test.
		//
		if (strcmp(arg, "-ushort") == 0) {
			opts.test |= TEST_USHORT;
			continue;
		}

		// -long
		//
		//	Long (int32_t) test.
		//
		if (strcmp(arg, "-long") == 0) {
			opts.test |= TEST_LONG;
			continue;
		}

		// -ulong
		//
		//	ULong (uint32_t) test.
		//
		if (strcmp(arg, "-ulong") == 0) {
			opts.test |= TEST_ULONG;
			continue;
		}

		// -longlong
		//
		//	LongLong (int64_t) test.
		//
		if (strcmp(arg, "-longlong") == 0) {
			opts.test |= TEST_LONGLONG;
			continue;
		}

		// -ulonglong
		//
		//	ULongLong (uint64_t) test.
		//
		if (strcmp(arg, "-ulonglong") == 0) {
			opts.test |= TEST_ULONGLONG;
			continue;
		}

		// -basic
		//
		//	Run all basic types tests above.
		//
		if (strcmp(arg, "-basic") == 0) {
			opts.test |=
				TEST_BOOLEAN |
				TEST_CHAR |
				TEST_OCTET |
				TEST_SHORT |
				TEST_USHORT |
				TEST_LONG |
				TEST_ULONG |
				TEST_LONGLONG |
				TEST_ULONGLONG;
			continue;
		}

		// -bstring [string_len]
		//
		//	Bounded string test.
		//
		if (strcmp(arg, "-bstring") == 0) {
			ret = numarg(opts.bstring_len, arg, argc, argv,
					idx + 1, RANGE_GE0, OPTARG_OPTIONAL);
			if (ret < 0)
				return (false);
			else if (ret > 0)	// if got option argument
				++idx;
			opts.test |= TEST_BSTRING;
			continue;
		}

		// -ustring [string_len]
		//
		//	Unbounded string test.
		//
		if (strcmp(arg, "-ustring") == 0) {
			ret = numarg(opts.ustring_len, arg, argc, argv,
					idx + 1, RANGE_GE0, OPTARG_OPTIONAL);
			if (ret < 0)
				return (false);
			else if (ret > 0)	// if got option argument
				++idx;
			opts.test |= TEST_USTRING;
			continue;
		}

		// -bseq [seq_len]
		//
		//	Bounded sequence test.
		//
		if (strcmp(arg, "-bseq") == 0) {
			ret = numarg(opts.bseq_len, arg, argc, argv,
					idx + 1, RANGE_GE0, OPTARG_OPTIONAL);
			if (ret < 0) {
				return (false);
			} else if (ret > 0) {
				//  Got option argument
				++idx;
			}
			opts.test |= TEST_BSEQ;
			continue;
		}

		// -useq [seq_len]
		//
		//	Unbounded sequence test.
		//
		if (strcmp(arg, "-useq") == 0) {
			ret = numarg(opts.useq_len, arg, argc, argv,
					idx + 1, RANGE_GE0, OPTARG_OPTIONAL);
			if (ret < 0) {
				return (false);
			} else if (ret > 0) {
				// Got option argument
				++idx;
			}
			opts.test |= TEST_USEQ;
			continue;
		}

		// -seq_read [seq_len]
		//
		//	Unbounded sequence test with only one read argument.
		//
		if (strcmp(arg, "-seq_read") == 0) {
			ret = numarg(opts.seq_read_len, arg, argc, argv,
					idx + 1, RANGE_GE0, OPTARG_OPTIONAL);
			if (ret < 0) {
				return (false);
			} else if (ret > 0) {
				// Got option argument
				++idx;
			}
			opts.test |= TEST_SEQ_READ;
			continue;
		}

		// -seq_write [seq_len]
		//
		//	Unbounded sequence test with only one write argument.
		//
		if (strcmp(arg, "-seq_write") == 0) {
			ret = numarg(opts.seq_write_len, arg, argc, argv,
					idx + 1, RANGE_GE0, OPTARG_OPTIONAL);
			if (ret < 0) {
				return (false);
			} else if (ret > 0) {
				// Got option argument
				++idx;
			}
			opts.test |= TEST_SEQ_WRITE;
			continue;
		}

		// -seq_rw [seq_len]
		//
		//	Unbounded sequence test with one inout argument, which
		//	means there is only one argument in each direction.
		//
		if (strcmp(arg, "-seq_rw") == 0) {
			ret = numarg(opts.seq_rw_len, arg, argc, argv,
					idx + 1, RANGE_GE0, OPTARG_OPTIONAL);
			if (ret < 0) {
				return (false);
			} else if (ret > 0) {
				// Got option argument
				++idx;
			}
			opts.test |= TEST_SEQ_RW;
			continue;
		}

		// -bsequseq [bseq_len [useq_maxlen]]
		//
		//	Bounded sequence of unbounded sequences test.
		//
		if (strcmp(arg, "-bsequseq") == 0) {
			ret = numarg(opts.bsequseq_len, arg, argc, argv,
					idx + 1, RANGE_GE0, OPTARG_OPTIONAL);
			if (ret < 0)
				return (false);
			else if (ret > 0) {	// if got option argument
				++idx;

				// Get 2nd optional argument
				ret = numarg(opts.bsequseq_max, arg,
						argc, argv, idx + 1,
						RANGE_GE0, OPTARG_OPTIONAL);
				if (ret < 0)
					return (false);
				else if (ret > 0)
					++idx;
			}
			opts.test |= TEST_BSEQUSEQ;
			continue;
		}

		// -useqbseq [useq_len [bseq_maxlen]]
		//
		//	Unbounded sequence of bounded sequences test.
		//
		if (strcmp(arg, "-useqbseq") == 0) {
			ret = numarg(opts.useqbseq_len, arg, argc, argv,
					idx + 1, RANGE_GE0, OPTARG_OPTIONAL);
			if (ret < 0)
				return (false);
			else if (ret > 0) {	// if got option argument
				++idx;

				// Get 2nd optional argument
				ret = numarg(opts.useqbseq_max, arg,
						argc, argv, idx + 1,
						RANGE_GE0, OPTARG_OPTIONAL);
				if (ret < 0)
					return (false);
				else if (ret > 0)
					++idx;
			}
			opts.test |= TEST_USEQBSEQ;
			continue;
		}

		// -bsequstring [seq_len [string_maxlen]]
		//
		//	Bounded sequence of unbounded strings test.
		//
		if (strcmp(arg, "-bsequstring") == 0) {
			ret = numarg(opts.bsequstr_len, arg,
						argc, argv, idx + 1,
						RANGE_GE0, OPTARG_OPTIONAL);
			if (ret < 0)
				return (false);
			else if (ret > 0) {	// if got option argument
				++idx;

				// Get 2nd optional argument
				ret = numarg(opts.bsequstr_max, arg,
						argc, argv, idx + 1,
						RANGE_GE0, OPTARG_OPTIONAL);
				if (ret < 0)
					return (false);
				else if (ret > 0)
					++idx;
			}
			opts.test |= TEST_BSEQUSTR;
			continue;
		}

		// -useqbstring [seq_len [string_maxlen]]
		//
		//	Unbounded sequence of bounded strings test.
		//
		if (strcmp(arg, "-useqbstring") == 0) {
			ret = numarg(opts.useqbstr_len, arg, argc, argv,
					idx + 1, RANGE_GE0, OPTARG_OPTIONAL);
			if (ret < 0)
				return (false);
			else if (ret > 0) {	// if got option argument
				++idx;

				// Get 2nd optional argument
				ret = numarg(opts.useqbstr_max, arg,
						argc, argv, idx + 1,
						RANGE_GE0, OPTARG_OPTIONAL);
				if (ret < 0)
					return (false);
				else if (ret > 0)
					++idx;
			}
			opts.test |= TEST_USEQBSTR;
			continue;
		}

		// -fstruct
		//
		//	Fixed struct test.
		//
		if (strcmp(arg, "-fstruct") == 0) {
			opts.test |= TEST_FSTRUCT;
			continue;
		}

		// -vstruct [string_len]
		//
		//	Variable struct test.
		//
		if (strcmp(arg, "-vstruct") == 0) {
			ret = numarg(opts.vstruct_len, arg, argc, argv,
					idx + 1, RANGE_GE0, OPTARG_OPTIONAL);
			if (ret < 0)
				return (false);
			else if (ret > 0)	// if got option argument
				++idx;
			opts.test |= TEST_VSTRUCT;
			continue;
		}

		// -fstructseq
		//
		//	Seq Fixed struct test.
		//
		if (strcmp(arg, "-fstructseq") == 0) {
			ret = numarg(opts.fstructseq_len, arg, argc, argv,
					idx + 1, RANGE_GE0, OPTARG_OPTIONAL);
			if (ret < 0)
				return (false);
			else if (ret > 0) {	// if got option argument
				++idx;
				// Get 2nd optional argument
				ret = numarg(opts.fstructseq_max, arg,
						argc, argv, idx + 1,
						RANGE_GE0, OPTARG_OPTIONAL);
				if (ret < 0)
					return (false);
				else if (ret > 0)
					++idx;
			}
			opts.test |= TEST_FSTRUCTSEQ;
			continue;
		}

		// -nullseq
		if (strcmp(arg, "-nullseq") == 0) {
			ret = numarg(opts.nullseq_len, arg, argc, argv,
					idx + 1, RANGE_GE0, OPTARG_OPTIONAL);
			if (ret < 0)
				return (false);
			else if (ret > 0)	// if got option argument
				++idx;
			opts.test |= TEST_NULLSEQ;
			continue;
		}

		// -0lenseq
		if (strcmp(arg, "-0lenseq") == 0) {
			ret = numarg(opts.zerolenseq_len, arg, argc, argv,
					idx + 1, RANGE_GE0, OPTARG_OPTIONAL);
			if (ret < 0)
				return (false);
			else if (ret > 0)	// if got option argument
				++idx;
			opts.test |= TEST_0LENSEQ;
			continue;
		}

		// -funion
		//
		//	Fixed union test.
		//
		if (strcmp(arg, "-funion") == 0) {
			opts.test |= TEST_FUNION;
			continue;
		}

		// -vunion [string_len]
		//
		//	Variable union test.
		//
		if (strcmp(arg, "-vunion") == 0) {
			ret = numarg(opts.vunion_len, arg, argc, argv,
					idx + 1, RANGE_GE0, OPTARG_OPTIONAL);
			if (ret < 0)
				return (false);
			else if (ret > 0)	// if got option argument
				++idx;
			opts.test |= TEST_VUNION;
			continue;
		}

		// -obj
		// -objref
		//
		//	Object reference test.
		//
		if (strcmp(arg, "-obj") == 0 || strcmp(arg, "-objref") == 0) {
			opts.test |= TEST_OBJREF;
			continue;
		}

		// -objinhsimple
		//
		//	Object single inheritance test
		//	with one level in ancestry tree.
		//
		if (strcmp(arg, "-objinhsimple") == 0) {
			opts.test |= TEST_OBJINHSIMPLE;
			continue;
		}

		// -objinhmany
		//
		//	Object single inheritance test
		//	with many levels in ancestry tree.
		//
		if (strcmp(arg, "-objinhmany") == 0) {
			opts.test |= TEST_OBJINHMANY;
			continue;
		}

		// -objinhmultiple
		//
		//	Object multiple inheritance test.
		//
		if (strcmp(arg, "-objinhmultiple") == 0) {
			opts.test |= TEST_OBJINHMULTIPLE;
			continue;
		}

		// -objseq [seq_len]
		//
		//	Sequence of object references test.
		//
		if (strcmp(arg, "-objseq") == 0) {
			ret = numarg(opts.objseq_len, arg, argc, argv,
					idx + 1, RANGE_GE0, OPTARG_OPTIONAL);
			if (ret < 0)
				return (false);
			else if (ret > 0)	// if got option argument
				++idx;
			opts.test |= TEST_OBJSEQ;
			continue;
		}

		// -revoke
		//
		//	Revoke operation test.
		//
		// if (strcmp(arg, "-revoke") == 0) {
		//	opts.test |= TEST_REVOKE;
		//	continue;
		// }

		// -arrayshort
		//
		//	Array of Short (int16_t) test.
		//
		if (strcmp(arg, "-arrayshort") == 0) {
			opts.test |= TEST_ARRAYSHORT;
			continue;
		}

		// -arrayoctet
		//
		//	Array of Octet (uint8_t) test.
		//
		if (strcmp(arg, "-arrayoctet") == 0) {
			opts.test |= TEST_ARRAYOCTET;
			continue;
		}

		// -arrayfstruct
		//
		//	Array of fixed structs test.
		//
		if (strcmp(arg, "-arrayfstruct") == 0) {
			opts.test |= TEST_ARRAYFSTRUCT;
			continue;
		}

		// -arrayfunion
		//
		//	Array of fixed unions test.
		//
		if (strcmp(arg, "-arrayfunion") == 0) {
			opts.test |= TEST_ARRAYFUNION;
			continue;
		}

		// -array
		//
		//	Run all array tests above.
		//
		if (strcmp(arg, "-array") == 0) {
			opts.test |= TEST_ARRAYSHORT;
			opts.test |= TEST_ARRAYOCTET;
			opts.test |= TEST_ARRAYFSTRUCT;
			opts.test |= TEST_ARRAYFUNION;
			continue;
		}

		// -emptyexception
		//
		//	Empty Exception test.
		//
		if (strcmp(arg, "-emptyexception") == 0) {
			opts.test |= TEST_EMPTYEXCEPTION;
			continue;
		}

		// -basicexception
		//
		//	Basic Exception test.
		//
		if (strcmp(arg, "-basicexception") == 0) {
			opts.test |= TEST_BASICEXCEPTION;
			continue;
		}

		// -complexexception
		//
		//	Complex Exception test.
		//
		if (strcmp(arg, "-complexexception") == 0) {
			opts.test |= TEST_COMPLEXEXCEPTION;
			continue;
		}

		// -stringexception
		//
		//	String Exception test.
		//
		if (strcmp(arg, "-stringexception") == 0) {
			opts.test |= TEST_STRINGEXCEPTION;
			continue;
		}

//
// We rely on the bulkio fault injection tests to cover bulkio functionality.
// This code is left for those needing to do performance work,
// and they will have to select the pxfs version of the test to build.
//
#if 0
		//
		// -bulkio_uio_in [size-in-bytes]
		//
		if (strcmp(arg, "-bulkio_uio_in") == 0) {
			ret = numarg(opts.in_uio_len, arg, argc, argv,
					idx + 1, RANGE_GT0, OPTARG_OPTIONAL);
			if (ret < 0)
				return (false);
			else if (ret > 0) // if got option argument
				++idx;
			opts.test |= TEST_BULKIO_UIO_IN;
			continue;
		}

		//
		// -bulkio_uio_inout [size-in-bytes]
		//
		if (strcmp(arg, "-bulkio_uio_inout") == 0) {
			ret = numarg(opts.inout_uio_len, arg, argc, argv,
					idx + 1, RANGE_GT0, OPTARG_OPTIONAL);
			if (ret < 0)
				return (false);
			else if (ret > 0) // if got option argument
				++idx;
			opts.test |= TEST_BULKIO_UIO_INOUT;
			continue;
		}

		//
		// -bulkio_pages_in [number-of-pages]
		//
		if (strcmp(arg, "-bulkio_pages_in") == 0) {
			ret = numarg(opts.in_pages_noof, arg, argc, argv,
					idx + 1, RANGE_GT0, OPTARG_OPTIONAL);
			if (ret < 0)
				return (false);
			else if (ret > 0) // if got option argument
				++idx;
			opts.test |= TEST_BULKIO_PAGES_IN;
			continue;
		}

		//
		// -bulkio_pages_inout [number-of-pages]
		//
		if (strcmp(arg, "-bulkio_pages_inout") == 0) {
			ret = numarg(opts.inout_pages_noof, arg, argc, argv,
					idx + 1, RANGE_GT0, OPTARG_OPTIONAL);
			if (ret < 0)
				return (false);
			else if (ret > 0) // if got option argument
				++idx;
			opts.test |= TEST_BULKIO_PAGES_INOUT;
			continue;
		}
#endif

		// -exception
		//
		//	Run all Exception tests above.
		//
		if (strcmp(arg, "-exception") == 0) {
			opts.test |= TEST_EMPTYEXCEPTION;
			opts.test |= TEST_BASICEXCEPTION;
			opts.test |= TEST_COMPLEXEXCEPTION;
			opts.test |= TEST_STRINGEXCEPTION;
			continue;
		}

		// -cred
		//
		// Cred Object Test
		//

		if (strcmp(arg, "-cred") == 0) {
			opts.test |= TEST_CRED;
			continue;
		}


		// -data_container
		//
		// Data Container Object Test
		//
		if (strcmp(arg, "-data_container") == 0) {
			ret = numarg(opts.data_len, arg, argc, argv,
					idx + 1, RANGE_GT0, OPTARG_OPTIONAL);
			if (ret < 0)
				return (false);
			else if (ret > 0) // if got option argument
				++idx;
			opts.test |= TEST_DATA_CONTAINER;
			continue;
		}


		// -idlver
		//
		// IDL Versioning Tests
		//
		if (strcmp(arg, "-idlver") == 0) {
			opts.test |= TEST_IDL_VERSIONING;
			continue;
		}

		//
		// If user wants to invoke done() on all test servers/clients
		// (non-advertised option -- used by supporting shell scripts
		// only).
		if (strcmp(arg, "-done") == 0) {
			opts.done = true;
			continue;
		}

		// Unknown argument
		(void) fprintf(stderr, "ERROR: illegal argument: %s\n", arg);
		return (false);
	}

	// Which connection types are actually specified by user.
	opts.conn_specified = opts.conn;

	// If user didn't specify which test server(s) or test client(s)
	// then run all of them.
	if (opts.conn == 0) {
		opts.conn = (unsigned int) CONN_ALL;
	}

	// Which tests are actually specified by user.
	opts.test_specified = opts.test;

	// If user didn't specify which test(s) to run, then run all of them.
	if (opts.test == 0) {
		opts.test = TEST_ALL;	// run all tests
	}

	return (true);
}


//
// Get a number option argument.
// Returns:
//	-1 -- if an error occurs.
//	 0 -- the option argument is optional but it's not present.
//	 1 -- the option argument is succesfully obtained.
//
static int
numarg(
	long &var,		// area to store the option argument
	const char *optstr,
	int argc,
	char *argv[],
	int optarg_idx,		// index into argv to obtain the option arg
	Optarg_ranges range,
	Optarg_optional optional)
{
	char	*endp = "";
	long	val;

	if (optarg_idx >= argc) {
		if (optional == OPTARG_REQUIRED) {
			(void) fprintf(stderr, "ERROR: no option argument "
				"after %s\n", optstr);
			return (-1);
		}
		return (0);			// optional arg not present
	}

	// Convert option argument into number.
	val = strtol(argv[optarg_idx], &endp, 10);
	if (*endp != '\0')			// if not a number
	{
		if (optional == OPTARG_REQUIRED) {
			(void) fprintf(stderr, "ERROR: option argument after "
			    "%s must be a number\n", optstr);
			return (-1);
		}
		return (0);			// optional arg not present
	}

	// Verify the range of the opt arg is correct.
	switch (range) {
	case RANGE_ALL:			// all possible (long) numbers
		break;

	case RANGE_GT0:			// > 0
		if (! (val > 0)) {
			(void) fprintf(stderr, "ERROR: option argument after "
			    "%s must be a number > 0\n", optstr);
			return (-1);
		}
		break;

	case RANGE_GE0:			// >= 0
		if (! (val >= 0)) {
			(void) fprintf(stderr, "ERROR: option argument after "
			    "%s must be a number >= 0\n", optstr);
			return (-1);
		}
		break;

	case RANGE_LT0:			// < 0
		if (! (val < 0)) {
			(void) fprintf(stderr, "ERROR: option argument after "
			    "%s must be a number < 0\n", optstr);
			return (-1);
		}
		break;

	case RANGE_LE0:			// <= 0
		if (! (val <= 0)) {
			(void) fprintf(stderr, "ERROR: option argument after "
			    "%s must be a number < 0\n", optstr);
			return (-1);
		}
		break;

	default:
		(void) fprintf(stderr, "ERROR: invalid range specified: %d\n",
				range);
		return (-1);
	}

	var = val;
	// got the option argument
	return (1);
}


//
// The heart of the driver.
//
static bool
doit(Options &opts)
{
	(void) printf("DRIVER: doit called\n");
	if (opts.done) {
		// Invoke done() on all active test clients/servers, if any.
		const char *svc_desc;
#if defined(_UNODE)
		done(hatypes_unclient_name);
		done(svc_desc = hatypes_unserver_name);
#else
		//
		// Now that we're done with our tests - we'll tell the test
		// client and test server objects to unbind themselves from the
		// ORB nameserver.
		//
		if (opts.conn & (CONN_U2K)) {
			done(hatypes_uclient_name);
		}
		if (opts.conn & (CONN_K2K)) {
			done(hatypes_kclient_name);
		}
		done(svc_desc = hatypes_kserver_name);
#endif
		//
		// Shut down the HA service.
		//
		Environment e;
		replica::rm_admin_var rm_v = rm_util::get_rm();
		replica::service_admin_var sa =
		    rm_v->get_control(svc_desc, e);
		if (e.exception()) {
			(void) fprintf(stderr,
			    "ERROR: can't get service control object for %s\n",
			    svc_desc);
			e.clear();
		} else {
			sa->shutdown_service(false, e);
			if (e.exception()) {
				(void) fprintf(stderr,
				    "ERROR: can't shut down HA service %s\n",
				    svc_desc);
				e.clear();
			}
		}
		os::printf("DRIVER: DONE\n");
		return (true);
	}

	//
	// Else run tests.
	//
#if defined(_UNODE)
	(void) printf("DRIVER: UNODE client <-> UNODE server:\n");
	if (! test(hatypes_unclient_name, hatypes_unserver_name, opts)) {
		return (false);
	}
#else
	if (opts.conn & CONN_U2K) {
		(void) printf("DRIVER: USER client <-> KERNEL server:\n");
		if (! test(hatypes_uclient_name, hatypes_kserver_name, opts))
			return (false);
	}
	if (opts.conn & CONN_K2K) {
		(void) printf("DRIVER: KERNEL client <-> KERNEL server:\n");
		if (! test(hatypes_kclient_name, hatypes_kserver_name, opts))
			return (false);
	}
#endif

	return (true);
}


//
// Perform test(s) for a given client
//
static bool
test(const char *client_name, const char *server_name, Options &opts)
{
	Environment e1;
	hatypetest::server_var server_v;
	hatypetest::client_var client_v;
	CORBA::Object_var obj_v;
	int64_t			elapsed_time;
	bool			kclient = false;
	bool			kserver = false;

	(void) printf("DRIVER: test(...) called\n");

	//
	// Set some flags which can be used to check whether
	// the kernel-land client/server is running
	//
	if (strcmp(client_name, hatypes_kclient_name) == 0) {
		kclient = true;
	}
	if (strcmp(server_name, hatypes_kserver_name) == 0) {
		kserver = true;
	}

	//
	// Test Initialization
	//
	// - Get ref to test client obj from ORB nameserver
	// - Get ref to test server obj from ORB nameserver
	//
	(void) printf("DRIVER: Getting reference to client obj from "
	    "nameserver\n");
	obj_v = hatypes_get_obj(client_name);
	client_v = hatypetest::client::_narrow(obj_v);
	if (CORBA::is_nil(client_v)) {
		(void) printf("DRIVER: failed to get reference to test client "
		    "object\n");
		return (false);
	}

	(void) printf("DRIVER: Getting reference to server obj from "
	    "nameserver\n");
	obj_v = hatypes_get_obj(server_name);
	server_v = hatypetest::server::_narrow(obj_v);
	if (CORBA::is_nil(server_v)) {
		(void) printf("DRIVER: failed to get reference to test "
		    "server object\n");
		return (false);
	}

	//
	// Now we go through the conditional test case blocks.
	//

	if (opts.test & TEST_NOARG) {
		Environment	e;

		(void) printf("\tNo-argument test:\n");
		elapsed_time = client_v->test_noarg(server_v, (int)opts.iter,
		    opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
			print_stats(elapsed_time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_BOOLEAN) {
		Environment	e;

		(void) printf("\tBoolean test:\n");
		elapsed_time = client_v->test_boolean(server_v, (int)opts.iter,
		    opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
			print_stats(elapsed_time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_CHAR) {
		Environment	e;

		(void) printf("\tChar test:\n");
		elapsed_time = client_v->test_char(server_v, (int)opts.iter,
		    opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
			print_stats(elapsed_time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_OCTET) {
		Environment	e;

		(void) printf("\tOctet test:\n");
		elapsed_time = client_v->test_octet(server_v, (int)opts.iter,
		    opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
			print_stats(elapsed_time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_SHORT) {
		Environment	e;

		(void) printf("\tShort test:\n");
		elapsed_time = client_v->test_short(server_v, (int)opts.iter,
		    opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
			print_stats(elapsed_time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_USHORT) {
		Environment	e;

		(void) printf("\tUShort test:\n");
		elapsed_time = client_v->test_ushort(server_v, (int)opts.iter,
		    opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
			print_stats(elapsed_time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_LONG) {
		Environment	e;

		(void) printf("\tLong test:\n");
		elapsed_time = client_v->test_long(server_v, (int)opts.iter,
		    opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
			print_stats(elapsed_time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_ULONG) {
		Environment	e;

		(void) printf("\tULong test:\n");
		elapsed_time = client_v->test_ulong(server_v, (int)opts.iter,
		    opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
			print_stats(elapsed_time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_LONGLONG) {
		Environment	e;

		(void) printf("\tLongLong test:\n");
		elapsed_time = client_v->test_longlong(server_v, (int)opts.iter,
		    opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
			print_stats(elapsed_time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_ULONGLONG) {
		Environment	e;

		(void) printf("\tULongLong test:\n");
		elapsed_time = client_v->test_ulonglong(server_v,
		    (int)opts.iter, opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
			print_stats(elapsed_time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_BSTRING) {
		Environment	e;

		(void) printf("\tBounded string test: length %d\n",
				opts.bstring_len);
		elapsed_time = client_v->test_bstring(server_v, (int)opts.iter,
		    (uint32_t)opts.bstring_len, opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
			print_stats(elapsed_time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_USTRING) {
		Environment	e;

		(void) printf("\tUnbounded string test: length %d\n",
						opts.ustring_len);
		elapsed_time = client_v->test_ustring(server_v, (int)opts.iter,
		    (uint32_t)opts.ustring_len, opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
			print_stats(elapsed_time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_BSEQ) {
		Environment	e;

		(void) printf("\tBounded sequence test: length %d\n",
		    opts.bseq_len);
		elapsed_time = client_v->test_bseq(server_v, (int)opts.iter,
		    (uint32_t)opts.bseq_len, opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0) {
			print_stats(elapsed_time, (int32_t)opts.iter);
		}
	}

	if (opts.test & TEST_USEQ) {
		Environment	e;

		(void) printf("\tUnbounded sequence test: length %d\n",
		    opts.useq_len);
		elapsed_time = client_v->test_useq(server_v, (int)opts.iter,
		    (uint32_t)opts.useq_len, opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0) {
			print_stats(elapsed_time, (int32_t)opts.iter);
		}
	}

	if (opts.test & TEST_SEQ_READ) {
		Environment	e;

		(void) printf("\tUnbounded sequence read test: length %d\n",
						opts.seq_read_len);
		elapsed_time = client_v->test_seq_read(server_v, (int)opts.iter,
		    (uint32_t)opts.seq_read_len, opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0) {
			print_stats(elapsed_time, (int32_t)opts.iter);
		}
	}

	if (opts.test & TEST_SEQ_WRITE) {
		Environment	e;

		(void) printf("\tUnbounded sequence write test: length %d\n",
						opts.seq_write_len);
		elapsed_time = client_v->test_seq_write(server_v,
		    (int)opts.iter, (uint32_t)opts.seq_write_len,
		    opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0) {
			print_stats(elapsed_time, (int32_t)opts.iter);
		}
	}

	if (opts.test & TEST_SEQ_RW) {
		Environment	e;

		(void) printf("\tUnbounded sequence read/write test: "
				"length %d\n", opts.seq_rw_len);
		elapsed_time = client_v->test_seq_rw(server_v, (int)opts.iter,
		    (uint32_t)opts.seq_rw_len, opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0) {
			print_stats(elapsed_time, (int32_t)opts.iter);
		}
	}

	if (opts.test & TEST_BSEQUSEQ) {
		Environment	e;

		(void) printf("\tBounded sequence of unbounded sequences test:"
		    " length %d\n", opts.bsequseq_len);
		elapsed_time = client_v->test_bseq_useq(server_v,
		    (int)opts.iter, (uint32_t)opts.bsequseq_len,
		    (uint32_t)opts.bsequseq_max, opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
			print_stats(elapsed_time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_USEQBSEQ) {
		Environment	e;

		(void) printf("\tUnbounded sequence of bounded sequences test:"
		    " length %d\n", opts.useqbseq_len);
		elapsed_time = client_v->test_useq_bseq(server_v,
		    (int)opts.iter, (uint32_t)opts.useqbseq_len,
		    (uint32_t)opts.useqbseq_max, opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
			print_stats(elapsed_time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_BSEQUSTR) {
		Environment	e;

		(void) printf("\tBounded sequence of unbounded strings test:"
		    " length %d\n", opts.bsequstr_len);
		elapsed_time = client_v->test_bseq_ustring(server_v,
			(int)opts.iter, (uint32_t)opts.bsequstr_len,
			(uint32_t)opts.bsequstr_max, opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
			print_stats(elapsed_time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_USEQBSTR) {
		Environment	e;

		(void) printf("\tUnbounded sequence of bounded strings test:"
		    " length %d\n", opts.useqbstr_len);
		elapsed_time = client_v->test_useq_bstring(server_v,
			(int)opts.iter, (uint32_t)opts.useqbstr_len,
			(uint32_t)opts.useqbstr_max, opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
			print_stats(elapsed_time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_FSTRUCT) {
		Environment	e;

		(void) printf("\tFixed structure test:\n");
		elapsed_time = client_v->test_fstruct(server_v, (int)opts.iter,
		    opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
			print_stats(elapsed_time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_FSTRUCTSEQ) {
		Environment	e;

		(void) printf("\tSequence of Fixed structure test:\n");
		elapsed_time = client_v->test_fstructseq(server_v,
		    (int)opts.iter, (uint32_t)opts.fstructseq_len,
		    opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
			print_stats(elapsed_time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_NULLSEQ) {
		Environment	e;

		(void) printf("\tUninitialized/null out sequence:\n");
		elapsed_time = client_v->test_nullseq(server_v, (int)opts.iter,
		    (uint32_t)opts.nullseq_len, opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
			print_stats(elapsed_time, (int32_t)opts.iter);
	}
	if (opts.test & TEST_0LENSEQ) {
		Environment	e;
		(void) printf("\tOut Sequence of length 0:\n");
		elapsed_time = client_v->test_0lenseq(server_v, (int)opts.iter,
		    (uint32_t)opts.zerolenseq_len, opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
			print_stats(elapsed_time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_VSTRUCT) {
		Environment	e;

		(void) printf("\tVariable structure test:\n");
		elapsed_time = client_v->test_vstruct(server_v, (int)opts.iter,
		    (uint32_t)opts.vstruct_len, opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
			print_stats(elapsed_time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_FUNION) {
		Environment	e;

		(void) printf("\tFixed union test:\n");
		elapsed_time = client_v->test_funion(server_v, (int)opts.iter,
		    opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
			print_stats(elapsed_time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_VUNION) {
		Environment	e;

		(void) printf("\tVariable union test:\n");
		elapsed_time = client_v->test_vunion(server_v, (int)opts.iter,
		    (uint32_t)opts.vunion_len, opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
			print_stats(elapsed_time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_OBJREF) {
		Environment	e;

		(void) printf("\tObject reference test:\n");
		elapsed_time = client_v->test_obj(server_v, (int)opts.iter,
		    opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
			print_stats(elapsed_time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_OBJINHSIMPLE) {
		Environment	e;

		(void) printf("\tInherited object test:\n");
		elapsed_time = client_v->test_objinhsimple(server_v,
			(int)opts.iter, opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
			print_stats(elapsed_time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_OBJINHMANY) {
		Environment	e;

		(void) printf("\tInherited object (many levels) test:\n");
		elapsed_time = client_v->test_objinhmany(server_v,
		    (int)opts.iter, opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0) {
			print_stats(elapsed_time, opts.iter);
		}
	}

	if (opts.test & TEST_OBJINHMULTIPLE) {
		Environment	e;

		(void) printf("\tInherited object (multiple inheritance) "
				"test:\n");
		elapsed_time = client_v->test_objinhmultiple(server_v,
			(int)opts.iter, opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0) {
			print_stats(elapsed_time, (int32_t)opts.iter);
		}
	}

	if (opts.test & TEST_OBJSEQ) {
		Environment	e;

		(void) printf("\tObject reference sequence test:\n");
		elapsed_time = client_v->test_objseq(server_v, (int)opts.iter,
		    (uint32_t)opts.objseq_len, opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
			print_stats(elapsed_time, (int32_t)opts.iter);
	}

	// if (opts.test & TEST_REVOKE) {
	//	Environment e;

	//	printf("\tRevoke operation test:\n");
	//	elapsed_time = client_v->test_revoke(server_v, 1,
	//		opts.noverify, e);
	//	if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
	//		print_stats(elapsed_time, (int32_t)opts.iter);
	// }

	if (opts.test & TEST_ARRAYSHORT) {
		Environment	e;

		(void) printf("\tArray of Short (int16_t) test:\n");

		elapsed_time = client_v->test_arrayshort(server_v,
		    (int)opts.iter, opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
			print_stats(elapsed_time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_ARRAYOCTET) {
		Environment	e;

		(void) printf("\tArray of Octet (uint8_t) (2D) test:\n");

		elapsed_time = client_v->test_arrayoctet(server_v,
		    (int)opts.iter, opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
			print_stats(elapsed_time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_ARRAYFSTRUCT) {
		Environment	e;

		(void) printf("\tArray of Fstruct test:\n");

		elapsed_time = client_v->test_arrayfstruct(server_v,
			(int)opts.iter, opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
			print_stats(elapsed_time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_ARRAYFUNION) {
		Environment	e;

		(void) printf("\tArray of Funion test:\n");

		elapsed_time = client_v->test_arrayfunion(server_v,
			(int)opts.iter, opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
			print_stats(elapsed_time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_EMPTYEXCEPTION) {
		Environment	e;

		(void) printf("\tEmpty exception test:\n");

		elapsed_time = client_v->test_emptyexception(server_v,
			(int)opts.iter, opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
			print_stats(elapsed_time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_BASICEXCEPTION) {
		Environment	e;

		(void) printf("\tBasic exception test:\n");

		elapsed_time = client_v->test_basicexception(server_v,
			(int)opts.iter, opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
			print_stats(elapsed_time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_COMPLEXEXCEPTION) {
		Environment	e;

		(void) printf("\tComplex exception test:\n");

		elapsed_time = client_v->test_complexexception(server_v,
			(int)opts.iter, opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
			print_stats(elapsed_time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_STRINGEXCEPTION) {
		Environment	e;

		(void) printf("\tString exception test:\n");

		elapsed_time = client_v->test_stringexception(server_v,
			(int)opts.iter, opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
			print_stats(elapsed_time, (int32_t)opts.iter);
	}
#if 0
	if (opts.test & TEST_BULKIO_UIO_IN) {
		(void) printf("\tBulkio raw I/O write test: number bytes %d\n",
		    opts.in_uio_len);

		if (kclient && kserver) {
			Environment	e;

#if !defined(_UNODE)
			elapsed_time = client_v->test_bulkio_uio_in(server_v,
			    (int)opts.iter, (uint32_t)opts.in_uio_len,
			    opts.noverify, e);
#endif // _UNODE
			if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
				print_stats(elapsed_time, (int32_t)opts.iter);
		} else {
			(void) printf("\t\t***BULKIO TESTS ONLY SUPPORTED FOR "
			    "KERNEL --> KERNEL\n");
		}
	}

	if (opts.test & TEST_BULKIO_UIO_INOUT) {
		(void) printf("\tBulkio raw I/O read test: number bytes %d\n",
		    opts.inout_uio_len);

		if (kclient && kserver) {
			Environment	e;

#if !defined(_UNODE)
			elapsed_time = client_v->test_bulkio_uio_inout(server_v,
			    (int)opts.iter, (uint32_t)opts.inout_uio_len,
			    opts.noverify, e);
#endif // _UNODE
			if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
				print_stats(elapsed_time, (int32_t)opts.iter);
		} else {
			(void) printf("\t\t***BULKIO TESTS ONLY SUPPORTED FOR "
			    "KERNEL --> KERNEL\n");
		}
	}

	if (opts.test & TEST_BULKIO_PAGES_IN) {
		(void) printf("\tBulkio pages write test: number pages %d\n",
		    opts.in_pages_noof);

		if (kclient && kserver) {
			Environment	e;

#if !defined(_UNODE)
			elapsed_time = client_v->test_bulkio_pages_in(server_v,
			    (int)opts.iter, (uint32_t)opts.in_pages_noof,
			    opts.noverify, e);
#endif // _UNODE
			if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
				print_stats(elapsed_time, (int32_t)opts.iter);
		} else {
			(void) printf("\t\t***BULKIO TESTS ONLY SUPPORTED FOR "
			    "KERNEL --> KERNEL\n");
		}
	}
#endif  // 0
//
// This test causes the primary to blow the following assert
//
// panic[cpu0]/thread=716a6600: assertion failed: _fdbuf == NULL,
// file:
// /global/ws4/robj/sc31/usr/src/common/cl/pxfs/bulkio/bulkio_impl_pages.cc,
// line: 488
//
#if 0
	if (opts.test & TEST_BULKIO_PAGES_INOUT) {
		(void) printf("\tBulkio pages read test: number pages %d\n",
		    opts.inout_pages_noof);

		if (kclient && kserver) {
			Environment	e;

			elapsed_time =
			    client_v->test_bulkio_pages_inout(server_v,
				(int)opts.iter, (uint32_t)opts.inout_pages_noof,
				opts.noverify, e);
			if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
				print_stats(elapsed_time, (int32_t)opts.iter);
		} else {
			(void) printf("\t\t***BULKIO TESTS ONLY SUPPORTED FOR "
			    "KERNEL --> KERNEL\n");
		}
	}
#endif  // 0

	if (opts.test & TEST_CRED) {
		Environment	e;

		(void) printf("\tCred Object test:\n");

		if (kclient && kserver) {
			elapsed_time = client_v->test_cred(server_v,
				(int)opts.iter, opts.noverify, e);
			if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
				print_stats(elapsed_time, (int32_t)opts.iter);
		} else {
			(void) printf("\t\t***Cred test only supported for "
			    "kernel --> kernel\n");
		}
	}

	if (opts.test & TEST_DATA_CONTAINER) {
		Environment	e;

		(void) printf("\tData Container Object test:\n");

		elapsed_time = client_v->test_data_container(server_v,
			(int)opts.iter, (uint32_t)opts.data_len,
			opts.noverify, e);
		if (hatypes_env_ok(e, prefix) && elapsed_time >= 0)
			print_stats(elapsed_time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_IDL_VERSIONING) {
		Environment e;

		(void) printf("============================\n");
		(void) printf("Running IDL Versioning Tests\n");
		(void) printf("           Phase 1          \n");
		(void) printf("============================\n");

		bool ok = client_v->test_versioning_1(server_v, e);
		if (e.exception() || !ok) {
			return (false);
		}

		ok = server_v->test_get_old_ckpt_ref(e);
		if (e.exception() || !ok) {
			return (false);
		}

#ifdef ORBTEST_V1
		(void) printf("============================\n");
		(void) printf("    Running Upgrade Tests   \n");
		(void) printf("           Phase 2          \n");
		(void) printf("============================\n");
		server_v = client_v->test_upgrade(server_v, e);
		if (e.exception() || CORBA::is_nil(server_v)) {
			return (false);
		}

		(void) printf("============================\n");
		(void) printf("Running IDL Versioning Tests\n");
		(void) printf("           Phase 2          \n");
		(void) printf("============================\n");
		int16_t service_version = server_v->version(e);
		if (e.exception()) {
			return (false);
		}
		if (service_version == 1) {
			ok = client_v->test_versioning_2(server_v, e);
			if (e.exception() || !ok) {
				return (false);
			}
			ok = client_v->test_versioning_3(e);
			if (e.exception() || !ok) {
				return (false);
			}
		}

		ok = server_v->test_use_old_ckpt_ref(e);
		if (e.exception() || !ok) {
			return (false);
		}
#endif // ORBTEST_V1
	}

	return (true);
}


//
// Invoke the done() method on the impl object, if any, with the specified
// string name.
//
static void
done(const char *name)
{
	CORBA::Object_var	obj_v;

	(void) printf("DRIVER: done(\"%s\") called\n", name);

	// error messages suppressed, don't wait
	obj_v = hatypes_get_obj(name, true, false);

	if (! CORBA::is_nil(obj_v)) {
		Environment	e;
		hatypetest::common_var	common_v;

		common_v = hatypetest::common::_narrow(obj_v);
		common_v->done(e);
		if (e.exception()) {
			(void) fprintf(stderr,
			    "ERROR: can't invoke done on \"%s\"\n", name);
			e.exception()->print_exception("\t");
		}
	}
}

//
// Print statistics.
//
static void
print_stats(int64_t total_time, int32_t num_iter)
{
	total_time /= 1000;				// usec.

	(void) printf("\t\tTotal time         : %lld usec.\n", total_time);
	(void) printf("\t\tNo. of iterations  : %d\n", num_iter);
	(void) printf("\t\tAvg. time/iteration: %.2f usec.\n",
	    (double)total_time / num_iter);
}
