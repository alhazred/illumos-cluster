/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_upgrade.cc	1.7	08/05/20 SMI"

#include <nslib/ns.h>
#include "stdio.h"
#include <orbtest/hatypes/client_impl.h>

#if defined(_KERNEL) || defined(_KERNEL_ORB)
#include <orbtest/hatypes/server_impl.h>
#endif

#if defined(_KERNEL_ORB) && !defined(_KERNEL)
#define	_UNODE
#endif

hatypetest::server_ptr
client_impl::test_upgrade(
	hatypetest::server_ptr server_ref,
	Environment &)
{
	Environment e;
	naming::naming_context_var upgrade_root_ns;
	hatypetest::server_ptr	upgrade_server_p;
	CORBA::Object_var	upgrade_objp;
	const char *server_name;

#if defined(_UNODE)
	server_name = hatypes_unserver_name;
#else
	server_name = hatypes_kserver_name;
#endif

	//
	// Invoke the upgrade callback on the server. This should result in
	// a new reference being registered with the nameserver which is a V1
	// reference. The old reference should still work.
	//
	os::printf("Attempting to invoke upgrade_callback on server\n");
	server_ref->upgrade_callback(e);
	if (e.exception()) {
		os::printf("FAILED\n");
		os::printf("\tUpgrade Callback returned exception\n");
		e.exception()->print_exception("\t");
		return (hatypetest::server::_nil());
	}

	//
	// Assertion: Even after upgrade, server::_version should return 0 for
	// the old interface.
	//
	int version = -1;
	os::printf("Exercising server::_version on original ref\t\t\t");
	if ((version = hatypetest::server::_version(server_ref)) != 0) {
		os::printf("FAILED\n");
		os::printf("\tserver::_version returned %d, "
		    "was expecting 0\n", version);
		return (hatypetest::server::_nil());
	} else {
		os::printf("PASS\n");
	}

#ifdef ORBTEST_V1
	//
	// Assertion: Even after upgrade, the client should not be able to call
	// a version 1 method on the old reference.
	//
	os::printf("Exercising V1 method on old ref.\t\t\t\t");
	short id = server_ref->getid(e);
	if (e.exception()) {
		os::printf("PASS\n");
		e.exception()->print_exception("\t");
		e.clear();
	} else {
		os::printf("FAILED\n");
		os::printf("No exception returned from version 1 "
		    "invocation on old ref\n");
		return (hatypetest::server::_nil());
	}
#endif

	//
	// As part of the upgrade callback, we unregister the object reference
	// in the name. We should tests that the old reference works, but also
	// that the new reference works.
	//
	os::printf("Looking up hatypetest server in root nameserver\t\t\t");
	upgrade_root_ns = ns::root_nameserver();
	upgrade_objp = upgrade_root_ns->resolve(server_name, e);
	if (e.exception()) {
		os::printf("FAILED\n");
		os::printf("\tORB nameserver couldn't resolve "
		    "\"%s\"\n", server_name);
		e.exception()->print_exception("\t");
		return (hatypetest::server::_nil());
	} else if (CORBA::is_nil(upgrade_objp)) {
		os::printf("FAILED\n");
		os::printf("\tORB nameserver couldn't resolve "
		    "\"%s\", NIL returned\n", server_name);
		return (hatypetest::server::_nil());
	} else {
		os::printf("PASS\n");
	}

	//
	// Assertion: client should be able to narrow object to server.
	//
	os::printf("Narrow obj ref to type hatypetest::server\t\t\t");
	upgrade_server_p = hatypetest::server::_narrow(upgrade_objp);
	if (CORBA::is_nil(upgrade_server_p)) {
		os::printf("FAILED\n");
		os::printf("After upgrade callback, the server ref "
		    "is NULL\n");
		return (hatypetest::server::_nil());
	} else {
		os::printf("PASS\n");
	}

	//
	// Assertion: Should be able to get a different proxy for
	//		this interface.
	//
	os::printf("Test version 0 proxy object for server\t\t\t\t");

	// Get a proxy to the older interface.
	CORBA::type_info_t *tp = hatypetest::server::_get_type_info(0);
	upgrade_objp = CORBA::create_proxy_obj(tp,
	    upgrade_server_p->_handler());
	if (CORBA::is_nil(upgrade_objp)) {
		os::printf("FAILED_1\n");
		return (hatypetest::server::_nil());
	}
	hatypetest::server_var p0_v =
	    hatypetest::server::_narrow(upgrade_objp);
	if (CORBA::is_nil(p0_v)) {
		os::printf("FAILED_2\n");
		return (hatypetest::server::_nil());
	} else {
		os::printf("PASS\n");
	}

	//
	// Query the server for the current running version.
	//
	os::printf("Excercising server_impl::version on new ref\t\t\t");
	version = upgrade_server_p->version(e);
	if (e.exception()) {
		os::printf("FAILED\n");
		os::printf("\tserver_impl::version returned exception:");
		e.exception()->print_exception("\t");
		return (hatypetest::server::_nil());
	} else {
		os::printf("PASS\n");
	}

	//
	// Assertion: The server_impl::version and server::_version methods
	// should agree on the running version of the service.
	//
	os::printf("Exercising _version method on hatypetest::server "
	    "ref\t\t");

	int system_version = -1;
	if ((system_version = hatypetest::server::_version(upgrade_server_p))
	    != version) {
		os::printf("FAILED\n");
		os::printf("\tserver::_version returned %d, "
		    "was expecting %d\n", system_version, version);
		return (hatypetest::server::_nil());
	} else {
		os::printf("PASS\n");
	}
#ifdef ORBTEST_V1
	//
	// If we have a version 1 server, then try to invoke a version 1
	// method.
	//
	if (version == 1) {

		//
		// Assertion: If V1 client tries to invoke V1 interface on V1
		// server then the invocation should succceed and no exceptions
		// should be thrown.
		//
		short id_num = upgrade_server_p->getid(e);

		if (e.exception()) {
			os::printf("FAILED\n");
			e.exception()->print_exception("\t");
			return (hatypetest::server::_nil());
		} else if (id_num != 1234) {
			os::printf("FAILED\n");
			os::printf("\tgetid returned %h, was expecting "
			    "1234\n", id_num);
		} else {
			os::printf("PASS\n");
		}
	}
#endif

	return (upgrade_server_p);
}
