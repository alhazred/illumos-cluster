/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_ustring.cc	1.6	08/05/20 SMI"

//
// Unbounded string test
//

#include <orbtest/hatypes/client_impl.h>

#if defined(_KERNEL) || defined(_KERNEL_ORB)
#include <orbtest/hatypes/server_impl.h>
#endif


static bool set_in(hatypetest::Ustring &var, uint32_t len, const char *msgpfx,
    bool set = true);
static bool set_out(hatypetest::Ustring &var, uint32_t len, const char *msgpfx,
    bool set = true);
static bool set_io_i(hatypetest::Ustring &var, uint32_t len, const char *msgpfx,
    bool set = true);
static bool set_io_o(hatypetest::Ustring &var, uint32_t len, const char *msgpfx,
    bool set = true);
static bool set_ret(hatypetest::Ustring &var, uint32_t len, const char *msgpfx,
    bool set = true);

inline bool
verify_in(const hatypetest::Ustring v, uint32_t l, const char *p)
{
	return (set_in((hatypetest::Ustring) v, l, p, false));
}

inline bool
verify_out(const hatypetest::Ustring v, uint32_t l, const char *p)
{
	return (set_out((hatypetest::Ustring) v, l, p, false));
}

inline bool
verify_io_i(const hatypetest::Ustring v, uint32_t l, const char *p)
{
	return (set_io_i((hatypetest::Ustring) v, l, p, false));
}

inline bool
verify_io_o(const hatypetest::Ustring v, uint32_t l, const char *p)
{
	return (set_io_o((hatypetest::Ustring) v, l, p, false));
}

inline bool
verify_ret(const hatypetest::Ustring v, uint32_t l, const char *p)
{
	return (set_ret((hatypetest::Ustring) v, l, p, false));
}



//
// Test client interface.  Invoked by driver "programs" and, in turn,
// invokes the corresponding method on the test server side.
// Parameters:
//	server_ref  --	test server object to invoke.
//	num_repeat  --	how many times to run the test.
//	len	    --	length of string to test.
//	noverify    --	if true then server won't perform data verification.
// Returns:
//	Total test execution time.
//
int64_t
client_impl::test_ustring(
		hatypetest::server_ptr server_ref,
		int32_t num_repeat,
		uint32_t len,
		bool noverify,
		Environment &)
{
	// os::printf("%s test_ustring()\n", msg_prefix());
	os::hrtime_t	total_time = 0, start_time;

	if (noverify) {
		Environment	e3;

		server_ref->set_noverify(e3);
		if (! hatypes_env_ok(e3, msg_prefix())) {
			return (-1);
		}
	}

	for (; num_repeat > 0; --num_repeat) {
		Environment		e2;
		hatypetest::Ustring_var	in, out, inout, ret;

		// Prepare data to be passed to server.
		set_in(in.INOUT(), len, msg_prefix());
		set_io_i(inout.INOUT(), len, msg_prefix());

		// Invoke server.
		start_time = os::gethrtime();
		ret = server_ref->test_ustring(in, out, inout.INOUT(),
			len, e2);
		if (! hatypes_env_ok(e2, msg_prefix())) {
			total_time = -1;
			break;
		}
		total_time += os::gethrtime() - start_time;

		// Verify values returned by server.
		verify_out(out, len, msg_prefix());
		verify_io_o(inout, len, msg_prefix());
		verify_ret(ret, len, msg_prefix());
	}

	return (total_time);
}


//
// Test server interface.  Invoked by the test client.
//
#if defined(_KERNEL) || defined(_KERNEL_ORB)
hatypetest::Ustring
server_impl::test_ustring(
		const hatypetest::Ustring in,
		CORBA::String_out	out,
		hatypetest::Ustring	&inout,
		uint32_t len,
		Environment &)
{
	// os::printf("%s test_ustring()\n", msg_prefix());
	hatypetest::Ustring	ret = 0, newstr = 0;	// don't use _var!

	if (!verify_off) {
		// Verify values passed by client.
		verify_in(in, len, msg_prefix());
		verify_io_i(inout, len, msg_prefix());
	}

	// Prepare values to be returned to client.
	set_out(newstr, len, msg_prefix());
	out = newstr;
	set_io_o(inout, len, msg_prefix());
	set_ret(ret, len, msg_prefix());

	return (ret);
}
#endif


//
// Sets up (or verifies if "set" argument is false) 'in' parameter.
//
bool
set_in(hatypetest::Ustring &var, uint32_t len, const char *msgpfx, bool set)
{
	char		base = 1;
	uint32_t	i;

	if (set) {
		delete[] var;
		var = new char[len + 1];
		for (i = 0; i < len; ++i) {
			if (base == 0)
				base = 1;
			var[i] = base;
			base <<= 1;
		}
		var[i] = '\0';
		return (true);
	}

	// Verify data.
	for (i = 0; i < len; ++i) {
		if (base == 0)
			base = 1;
		if (var[i] == base) {
			base <<= 1;
			continue;
		}

		os::printf("FAIL: %s char at index %d of 'in' string:\n",
			msgpfx, i);
		os::printf("\texpected = 0x%x, actual = 0x%x\n", base, var[i]);
		return (false);
	}
	if (var[i] != '\0') {
		os::printf("FAIL: %s 'in' string not terminated at "
			"index %d:\n", msgpfx, i);
		return (false);
	}
	return (true);
}


//
// Sets up (or verifies if "set" argument is false) 'out' parameter.
//
bool
set_out(hatypetest::Ustring &var, uint32_t len, const char *msgpfx, bool set)
{
	char		base = ~0;
	uint32_t	i;

	if (set) {
		delete[] var;
		var = new char[len + 2];
		for (i = 0; i < len + 1; ++i) {
			if (base == 0)
				base = ~0;
			var[i] = base;
			base >>= 1;
		}
		var[i] = '\0';
		return (true);
	}
	// Did data  arrive
	if (NULL == (char *)var) {
		os::printf("FAIL: %s 'out' string null\n", msgpfx);
		return (false);
	}

	// Verify data.
	for (i = 0; i < len + 1; ++i) {
		if (base == 0)
			base = ~0;
		if (var[i] == base) {
			base >>= 1;
			continue;
		}

		os::printf("FAIL: %s char at index %d of 'out' string:\n",
			msgpfx, i);
		os::printf("\texpected = 0x%x, actual = 0x%x\n", base, var[i]);
		return (false);
	}
	if (var[i] != '\0') {
		os::printf("FAIL: %s 'out' string not terminated at "
			"index %d:\n", msgpfx, i);
		return (false);
	}
	return (true);
}


//
// Sets up (or verifies if "set" argument is false) incoming 'inout' parameter.
//
bool
set_io_i(hatypetest::Ustring &var, uint32_t len, const char *msgpfx, bool set)
{
	char		base = ~0;
	uint32_t	i;

	if (set) {
		delete[] var;
		var = new char[len + 1];
		for (i = 0; i < len; ++i) {
			if (base == 0)
				base = ~0;
			var[i] = base;
			base <<= 1;
		}
		var[i] = '\0';
		return (true);
	}

	// Verify data.
	for (i = 0; i < len; ++i) {
		if (base == 0)
			base = ~0;
		if (var[i] == base) {
			base <<= 1;
			continue;
		}

		os::printf("FAIL: %s char at index %d of 'inout' string:\n",
			msgpfx, i);
		os::printf("\texpected = 0x%x, actual = 0x%x\n", base, var[i]);
		return (false);
	}
	if (var[i] != '\0') {
		os::printf("FAIL: %s 'inout' string not terminated at "
			"index %d:\n", msgpfx, i);
		return (false);
	}
	return (true);
}


//
// Sets up (or verifies if "set" argument is false) outgoing 'inout' parameter.
//
bool
set_io_o(hatypetest::Ustring &var, uint32_t len, const char *msgpfx, bool set)
{
	char		base = 1;
	uint32_t	i;

	if (set) {
		delete[] var;
		var = new char[len + 3];
		for (i = 0; i < len + 2; ++i) {
			if (base == 0)
				base = ~0;
			var[i] = base;
			base >>= 1;
		}
		var[i] = '\0';
		return (true);
	}

	// Verify data.
	for (i = 0; i < len + 2; ++i) {
		if (base == 0)
			base = ~0;
		if (var[i] == base) {
			base >>= 1;
			continue;
		}

		os::printf("FAIL: %s char at index %d of 'inout' string:\n",
			msgpfx, i);
		os::printf("\texpected = 0x%x, actual = 0x%x\n", base, var[i]);
		return (false);
	}
	if (var[i] != '\0') {
		os::printf("FAIL: %s 'inout' string not terminated at "
			"index %d:\n", msgpfx, i);
		return (false);
	}
	return (true);
}


//
// Sets up (or verifies if "set" argument is false) returned value.
//
bool
set_ret(hatypetest::Ustring &var, uint32_t len, const char *msgpfx, bool set)
{
	char		base = 1;
	uint32_t	i;

	if (set) {
		delete[] var;
		var = new char[len + 1];
		for (i = 0; i < len; ++i) {
			var[i] = base;
			base = ~base;
		}
		var[i] = '\0';
		return (true);
	}

	// Verify data.
	for (i = 0; i < len; ++i) {
		if (var[i] == base) {
			base = ~base;
			continue;
		}

		os::printf("FAIL: %s char at index %d of returned string:\n",
			msgpfx, i);
		os::printf("\texpected = 0x%x, actual = 0x%x\n", base, var[i]);
		return (false);
	}
	if (var[i] != '\0') {
		os::printf("FAIL: %s returned string not terminated at "
			"index %d:\n", msgpfx, i);
		return (false);
	}
	return (true);
}
