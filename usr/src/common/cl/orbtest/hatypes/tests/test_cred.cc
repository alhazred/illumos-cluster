/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_cred.cc	1.9	08/05/20 SMI"

#ifdef ORBTEST_V1
#include <h/hatypetest_1.h>
#else
#include <h/hatypetest_0.h>
#endif

#include <h/solobj.h>
#include <orbtest/hatypes/client_impl.h>

#if defined(_KERNEL) || defined(_KERNEL_ORB)
#include <orbtest/hatypes/server_impl.h>
#endif

#include <sys/sol_version.h>
#if SOL_VERSION >= __s10
#include <sys/cred_impl.h>
#endif

const int IN = 1;
const int INOUT_IN = 2;
const int INOUT_OUT = 3;
const int OUT = 4;
const int RET = 5;

#if defined(_KERNEL)
#include <solobj/solobj_impl.h>

static bool
set(cred_t *cr, const char *msgpfx, const int type, bool set = true);

static bool
verify(cred_t *cr, const char *p, const int type)
{
	return (set(cr, p, type, false));
}

//
// Cred Object Test
//

//
// Test client interface.  Invoked by driver "programs" and, in turn,
// invokes the corresponding method on the test server side.
// Parameters:
//	server_ref  --	test server object to invoke.
//	num_repeat  --	how many times to run the test.
//	noverify    --	if true then server won't perform data verification.
// Returns:
//	Total test execution time.
//

int64_t
client_impl::test_cred(
		hatypetest::server_ptr server_ref,
		int32_t num_repeat,
		bool noverify,
		Environment &)
{
	os::hrtime_t	total_time = 0, start_time;

	if (noverify) {
		Environment	e3;

		server_ref->set_noverify(e3);
		if (! hatypes_env_ok(e3, msg_prefix())) {
			return (-1);
		}
	}

	for (; num_repeat > 0; --num_repeat) {
		Environment		e2;

		cred_t			cred_in, cred_io_in;
		solobj::cred_ptr	in, inout;
		solobj::cred_ptr	out = solobj::cred::_nil();
		solobj::cred_ptr	ret = solobj::cred::_nil();

		// Prepare cred objects to be passed to server.

		// Fill cred structure (cred_in) with known value
		// and convert it into cred object by calling conv().

		set(&cred_in, msg_prefix(), IN);
		in = solobj_impl::conv(&cred_in);


		// Fill cred structure (cred_io_in) with known value
		// and convert it into cred object by calling conv().

		set(&cred_io_in, msg_prefix(), INOUT_IN);
		inout = solobj_impl::conv(&cred_io_in);

		// Invoke server.

		start_time = os::gethrtime();

		ret = server_ref->test_cred(in, out, inout, e2);

		if (! hatypes_env_ok(e2, msg_prefix())) {
			total_time = -1;
			num_repeat = 0;		// end loop
		} else {

			total_time += os::gethrtime() - start_time;

			cred_t	*cred_out, *cred_io_out, *cred_ret;

			// Verify out, inout_out and ret parameters

			cred_io_out = solobj_impl::conv(inout);
			verify(cred_io_out, msg_prefix(), INOUT_OUT);

			cred_out = solobj_impl::conv(out);
			verify(cred_out, msg_prefix(), OUT);

			cred_ret = solobj_impl::conv(ret);
			verify(cred_ret, msg_prefix(), RET);

		}


		// Called by the client to free the memory allocated
		// for the 'out' parameter on the server side.

		server_ref->cred_crfree(e2);

		if (!hatypes_env_ok(e2, msg_prefix())) {
			os::printf("FAIL %s crfree() failed \n", msg_prefix());
		}

		CORBA::release(in);
		CORBA::release(inout);
		CORBA::release(out);
		CORBA::release(ret);
	}

	return (total_time);
}

//
// Test server interface.  Invoked by the test client.
//
#if defined(_KERNEL) || defined(_KERNEL_ORB)
solobj::cred_ptr
server_impl::test_cred(
		solobj::cred_ptr  in,
		solobj::cred_out  out,
		solobj::cred_ptr  &inout,
		Environment &)
{

	if (!verify_off) {

		// Verify object references from client.

		cred_t	*cred_tmp;

		cred_tmp = solobj_impl::conv(in);
		verify(cred_tmp, msg_prefix(), IN);
	}

	cred_t	*cred_io_out = solobj_impl::conv(inout);

	if (!verify_off) {
		verify(cred_io_out, msg_prefix(), INOUT_IN);
	}

	set(cred_io_out, msg_prefix(), INOUT_OUT);

	// Allocate a cred structure using crget() for 'out' parameter

	cr_out = crget();

	// Set cred structure to known value and convert into an object

	set(cr_out, msg_prefix(), OUT);
	out = solobj_impl::conv(cr_out);

	// duplicate in parameter to ret and return to the client.

	solobj::cred_ptr	ret = solobj::cred::_duplicate(in);
	cred_t	*cred_ret = solobj_impl::conv(ret);
	set(cred_ret, msg_prefix(), RET);

	return (ret);
}

void
server_impl::cred_crfree(Environment &)
{
	if (cr_out != NULL)
		crfree(cr_out);
}
#endif


//
// Sets up (or verifies if "set" argument is false) parameter.
//
bool
set(cred_t *var, const char *msgpfx, const int type, bool set)
#if SOL_VERSION >= __s10
{
	cred_t		*base = NULL;
	bool		failed = false;
	gid_t		grp;

	//
	// NOTE: In Solaris 10, the cred structure was made opaque, so
	// that none of the fields can be accessed directly, as in the
	// old code. Only accessor functions can be used. Further, two
	// new fields were added--cr_priv and cr_zone. The old test
	// code didn't initialize these fields, and using the functions
	// there's no way to do that. So we must use crget() to create
	// and initialize a cred structure.
	//
	base = crget();
	crsetresuid(base, type, type, type);
	crsetresgid(base, type, type, type);

	ASSERT(type >= 0);
	grp = (gid_t)type;
	crsetgroups(base, 1, &grp);

	if (set) {
		*var = *base; /* copy struct field-by-field */
		crfree(base); /* free allocated cred structure */
		return (true);
	}

	// Verify data.

	//
	// The test of cr_ref was removed because it's now considered
	// an internal field that outsiders need not know about and can't
	// be accessed anyway via any function.
	//
	if (crgetuid(var) != type) {
		os::printf("FAIL %s cr_uid expected = %d, actual = %d\n",
			msgpfx, type, crgetuid(var));
		failed = true;
	}
	if (crgetgid(var) != type) {
		os::printf("FAIL %s cr_gid expected = %d, actual = %d\n",
			msgpfx, type, crgetgid(var));
		failed = true;
	}
	if (crgetruid(var) != type) {
		os::printf("FAIL %s cr_ruid expected = %d, actual = %d\n",
			msgpfx, type, crgetruid(var));
		failed = true;
	}
	if (crgetrgid(var) != type) {
		os::printf("FAIL %s cr_rgid expected = %d, actual = %d\n",
			msgpfx, type, crgetrgid(var));
		failed = true;
	}

	if (crgetsuid(var) != type) {
		os::printf("FAIL %s cr_suid expected = %d, actual = %d\n",
			msgpfx, type, crgetsuid(var));
		failed = true;
	}

	if (crgetsgid(var) != type) {
		os::printf("FAIL %s cr_sgid expected = %d, actual = %d\n",
			msgpfx, type, crgetsgid(var));
		failed = true;
	}

	if (crgetngroups(var) != 1) {
		os::printf("FAIL %s cr_ngroups expected = 1, actual = %d\n",
			msgpfx, crgetngroups(var));
		failed = true;
	}

	const gid_t *groups = crgetgroups(var);

	if (groups[0] != type) {
		os::printf("FAIL %s cr_groups[0] expected = %d, actual = %d\n",
			msgpfx, type, groups[0]);
		failed = true;
	}

	crfree(base);
	return (!failed);
}
#else
{
	cred_t		base;
	bool		failed = false;

	base.cr_ref = 1;	/* reference count */
	base.cr_uid = type;	/* effective user id */
	base.cr_gid = type;	/* effective group id */
	base.cr_ruid = type;	/* real user id */
	base.cr_rgid = type;	/* real group id */
	base.cr_suid = type;	/* "saved" user id (from exec) */
	base.cr_sgid = type;	/* "saved" group id (from exec) */
	base.cr_ngroups = 1;	/* number of groups in cr_groups */
	base.cr_groups[0] = type;		/* supplementary group list */

	if (set) {
		*var = base;
		return (true);
	}

	// Verify data.

	if (var->cr_ref != 1) {
		os::printf("FAIL: %s cr_ref expected = 1, actual = %d\n",
			msgpfx, var->cr_ref);
		failed = true;
	}

	if (var->cr_uid != type) {
		os::printf("FAIL %s cr_uid expected = %d, actual = %d\n",
			msgpfx, type, var->cr_uid);
		failed = true;
	}
	if (var->cr_gid != type) {
		os::printf("FAIL %s cr_gid expected = %d, actual = %d\n",
			msgpfx, type, var->cr_gid);
		failed = true;
	}
	if (var->cr_ruid != type) {
		os::printf("FAIL %s cr_ruid expected = %d, actual = %d\n",
			msgpfx, type, var->cr_ruid);
		failed = true;
	}
	if (var->cr_rgid != type) {
		os::printf("FAIL %s cr_rgid expected = %d, actual = %d\n",
			msgpfx, type, var->cr_rgid);
		failed = true;
	}

	if (var->cr_suid != type) {
		os::printf("FAIL %s cr_suid expected = %d, actual = %d\n",
			msgpfx, type, var->cr_suid);
		failed = true;
	}

	if (var->cr_sgid != type) {
		os::printf("FAIL %s cr_sgid expected = %d, actual = %d\n",
			msgpfx, type, var->cr_sgid);
		failed = true;
	}

	if (var->cr_ngroups != 1) {
		os::printf("FAIL %s cr_ngroups expected = 1, actual = %d\n",
			msgpfx, var->cr_ngroups);
		failed = true;
	}

	if (var->cr_groups[0] != type) {
		os::printf("FAIL %s cr_groups[0] expected = %d, actual = %d\n",
			msgpfx, type, var->cr_groups[0]);
		failed = true;
	}

	return (!failed);
}
#endif

#else

int64_t
client_impl::test_cred(
		hatypetest::server_ptr,
		int32_t,
		bool,
		Environment &)
{
	return (0);
}


#if defined(_KERNEL) || defined(_KERNEL_ORB)
solobj::cred_ptr
server_impl::test_cred(
		solobj::cred_ptr,
		solobj::cred_out,
		solobj::cred_ptr &,
		Environment &)
{
	solobj::cred_ptr	ret = solobj::cred::_nil();

	return (ret);
}


void
server_impl::cred_crfree(Environment &)
{
}
#endif

#endif
