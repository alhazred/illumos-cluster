/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_fstruct.cc	1.6	08/05/20 SMI"

//
// Fixed structure test.  Because of compiler bug 4087749 we don't
// allow return of fixed-length structs and those checks have
// been removed but should be restored if struct returns are
// allowed again.
//

#include <orbtest/hatypes/client_impl.h>

#if defined(_KERNEL) || defined(_KERNEL_ORB)
#include <orbtest/hatypes/server_impl.h>
#endif


static bool	set_in(hatypetest::Fstruct &var, const char *msgpfx,
			bool set = true);
static bool	set_out(hatypetest::Fstruct &var, const char *msgpfx,
			bool set = true);
static bool	set_io_i(hatypetest::Fstruct &var, const char *msgpfx,
			bool set = true);
static bool	set_io_o(hatypetest::Fstruct &var, const char *msgpfx,
			bool set = true);

inline bool
verify_in(const hatypetest::Fstruct &v, const char *p)
{
	return (set_in((hatypetest::Fstruct &)v, p, false));
}

inline bool
verify_out(const hatypetest::Fstruct &v, const char *p)
{
	return (set_out((hatypetest::Fstruct &)v, p, false));
}

inline bool
verify_io_i(const hatypetest::Fstruct &v, const char *p)
{
	return (set_io_i((hatypetest::Fstruct &)v, p, false));
}

inline bool
verify_io_o(const hatypetest::Fstruct &v, const char *p)
{
	return (set_io_o((hatypetest::Fstruct &)v, p, false));
}

//
// Test client interface.  Invoked by driver "programs" and, in turn,
// invokes the corresponding method on the test server side.
// Parameters:
//	server_ref  --	test server object to invoke.
//	num_repeat  --	how many times to run the test.
//	noverify    --	if true then server won't perform data verification.
// Returns:
//	Total test execution time.
//
int64_t
client_impl::test_fstruct(
		hatypetest::server_ptr server_ref,
		int32_t num_repeat,
		bool noverify,
		Environment &)
{
	// os::printf("%s test_fstruct()\n", msg_prefix());
	os::hrtime_t	total_time = 0, start_time;

	if (noverify) {
		Environment	e3;

		server_ref->set_noverify(e3);
		if (! hatypes_env_ok(e3, msg_prefix())) {
			return (-1);
		}
	}

	for (; num_repeat > 0; --num_repeat) {
		Environment		e2;
		hatypetest::Fstruct	in, out, inout;

		// Prepare data to be passed to server.
		set_in(in, msg_prefix());
		set_io_i(inout, msg_prefix());

		// Invoke server.
		start_time = os::gethrtime();
		server_ref->test_fstruct(in, out, inout, e2);
		if (! hatypes_env_ok(e2, msg_prefix())) {
			total_time = -1;
			break;
		}
		total_time += os::gethrtime() - start_time;

		// Verify values returned by server.
		verify_out(out, msg_prefix());
		verify_io_o(inout, msg_prefix());
	}

	return (total_time);
}

//
// Test server interface.  Invoked by the test client.
//
#if defined(_KERNEL) || defined(_KERNEL_ORB)
void
server_impl::test_fstruct(
		const hatypetest::Fstruct &in,
		hatypetest::Fstruct &out,
		hatypetest::Fstruct &inout,
		Environment &)
{
	// os::printf("%s test_fstruct()\n", msg_prefix());

	if (!verify_off) {
		// Verify values passed by client.
		verify_in(in, msg_prefix());
		verify_io_i(inout, msg_prefix());
	}

	// Prepare values to be returned to client.
	set_out(out, msg_prefix());
	set_io_o(inout, msg_prefix());
}
#endif

//
// Sets up (or verifies if "set" argument is false) 'in' parameter.
//
bool
set_in(hatypetest::Fstruct &var, const char *msgpfx, bool set)
{
	hatypetest::Fstruct	base;
	bool			failed = false;

	base.m_1byte = 0xF7;
	base.m_2bytes = 0xeffe;
	base.m_4bytes = 0xfedcba98;
	base.m_8bytes = 0xfedcba9876543210;

	if (set) {
		var = base;
		return (true);
	}

	// Verify data.
	if (var.m_1byte != base.m_1byte) {
		os::printf("FAIL: %s 1-byte member of 'in' fixed struct:\n",
			msgpfx);
		os::printf("\texpected = 0x%x, actual = 0x%x\n",
			base.m_1byte, var.m_1byte);
		failed = true;
	}
	if (var.m_2bytes != base.m_2bytes) {
		os::printf("FAIL: %s 2-byte member of 'in' fixed struct:\n",
			msgpfx);
		os::printf("\texpected = 0x%hx, actual = 0x%hx\n",
			base.m_2bytes, var.m_2bytes);
		failed = true;
	}
	if (var.m_4bytes != base.m_4bytes) {
		os::printf("FAIL: %s 4-byte member of 'in' fixed struct:\n",
			msgpfx);
		os::printf("\texpected = 0x%lx, actual = 0x%lx\n",
			base.m_4bytes, var.m_4bytes);
		failed = true;
	}
	if (var.m_8bytes != base.m_8bytes) {
		os::printf("FAIL: %s 8-byte member of 'in' fixed struct:\n",
			msgpfx);
		os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
			base.m_8bytes, var.m_8bytes);
		failed = true;
	}

	return (!failed);
}

//
// Sets up (or verifies if "set" argument is false) 'out' parameter.
//
bool
set_out(hatypetest::Fstruct &var, const char *msgpfx, bool set)
{
	hatypetest::Fstruct	base;
	bool			failed = false;

	base.m_1byte = 0x3F;
	base.m_2bytes = 0x6666;
	base.m_4bytes = 0x12345678;
	base.m_8bytes = 0xca8642013579bdf;

	if (set) {
		var = base;
		return (true);
	}

	// Verify data.
	if (var.m_1byte != base.m_1byte) {
		os::printf("FAIL: %s 1-byte member of 'out' fixed struct:\n",
			msgpfx);
		os::printf("\texpected = 0x%x, actual = 0x%x\n",
			base.m_1byte, var.m_1byte);
		failed = true;
	}
	if (var.m_2bytes != base.m_2bytes) {
		os::printf("FAIL: %s 2-byte member of 'out' fixed struct:\n",
			msgpfx);
		os::printf("\texpected = 0x%hx, actual = 0x%hx\n",
			base.m_2bytes, var.m_2bytes);
		failed = true;
	}
	if (var.m_4bytes != base.m_4bytes) {
		os::printf("FAIL: %s 4-byte member of 'out' fixed struct:\n",
			msgpfx);
		os::printf("\texpected = 0x%lx, actual = 0x%lx\n",
			base.m_4bytes, var.m_4bytes);
		failed = true;
	}
	if (var.m_8bytes != base.m_8bytes) {
		os::printf("FAIL: %s 8-byte member of 'out' fixed struct:\n",
			msgpfx);
		os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
			base.m_8bytes, var.m_8bytes);
		failed = true;
	}

	return (!failed);
}

//
// Sets up (or verifies if "set" argument is false) incoming 'inout' parameter.
//
bool
set_io_i(hatypetest::Fstruct &var, const char *msgpfx, bool set)
{
	hatypetest::Fstruct	base;
	bool			failed = false;

	base.m_1byte = 0xF3;
	base.m_2bytes = 0x4444;
	base.m_4bytes = 0x98765432;
	base.m_8bytes = 0xfdb9753102468ac;

	if (set) {
		var = base;
		return (true);
	}

	// Verify data.
	if (var.m_1byte != base.m_1byte) {
		os::printf("FAIL: %s 1-byte member of 'inout' fixed struct:\n",
			msgpfx);
		os::printf("\texpected = 0x%x, actual = 0x%x\n",
			base.m_1byte, var.m_1byte);
		failed = true;
	}
	if (var.m_2bytes != base.m_2bytes) {
		os::printf("FAIL: %s 2-byte member of 'inout' fixed struct:\n",
			msgpfx);
		os::printf("\texpected = 0x%hx, actual = 0x%hx\n",
			base.m_2bytes, var.m_2bytes);
		failed = true;
	}
	if (var.m_4bytes != base.m_4bytes) {
		os::printf("FAIL: %s 4-byte member of 'inout' fixed struct:\n",
			msgpfx);
		os::printf("\texpected = 0x%lx, actual = 0x%lx\n",
			base.m_4bytes, var.m_4bytes);
		failed = true;
	}
	if (var.m_8bytes != base.m_8bytes) {
		os::printf("FAIL: %s 8-byte member of 'inout' fixed struct:\n",
			msgpfx);
		os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
			base.m_8bytes, var.m_8bytes);
		failed = true;
	}

	return (!failed);
}

//
// Sets up (or verifies if "set" argument is false) outgoing 'inout' parameter.
//
bool
set_io_o(hatypetest::Fstruct &var, const char *msgpfx, bool set)
{
	hatypetest::Fstruct	base;
	bool			failed = false;

	base.m_1byte = 0x7F;
	base.m_2bytes = 0xfeef;
	base.m_4bytes = 0x89abcdef;
	base.m_8bytes = 0x0123456789abcdef;

	if (set) {
		var = base;
		return (true);
	}

	// Verify data.
	if (var.m_1byte != base.m_1byte) {
		os::printf("FAIL: %s 1-byte member of 'inout' fixed struct:\n",
			msgpfx);
		os::printf("\texpected = 0x%x, actual = 0x%x\n",
			base.m_1byte, var.m_1byte);
		failed = true;
	}
	if (var.m_2bytes != base.m_2bytes) {
		os::printf("FAIL: %s 2-byte member of 'inout' fixed struct:\n",
			msgpfx);
		os::printf("\texpected = 0x%hx, actual = 0x%hx\n",
			base.m_2bytes, var.m_2bytes);
		failed = true;
	}
	if (var.m_4bytes != base.m_4bytes) {
		os::printf("FAIL: %s 4-byte member of 'inout' fixed struct:\n",
			msgpfx);
		os::printf("\texpected = 0x%lx, actual = 0x%lx\n",
			base.m_4bytes, var.m_4bytes);
		failed = true;
	}
	if (var.m_8bytes != base.m_8bytes) {
		os::printf("FAIL: %s 8-byte member of 'inout' fixed struct:\n",
			msgpfx);
		os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
			base.m_8bytes, var.m_8bytes);
		failed = true;
	}

	return (!failed);
}

//
// Sets up (or verifies if "set" argument is false) returned value.
//
bool
set_ret(hatypetest::Fstruct &var, const char *msgpfx, bool set)
{
	hatypetest::Fstruct	base;
	bool			failed = false;

	base.m_1byte = 0xAA;
	base.m_2bytes = 0xfedc;
	base.m_4bytes = 0x96f6e7a3;
	base.m_8bytes = 0xffffffffffffff00;

	if (set) {
		var = base;
		return (true);
	}

	// Verify data.
	if (var.m_1byte != base.m_1byte) {
		os::printf("FAIL: %s 1-byte member of returned "
			"fixed struct:\n", msgpfx);
		os::printf("\texpected = 0x%x, actual = 0x%x\n",
			base.m_1byte, var.m_1byte);
		failed = true;
	}
	if (var.m_2bytes != base.m_2bytes) {
		os::printf("FAIL: %s 2-byte member of returned "
			"fixed struct:\n", msgpfx);
		os::printf("\texpected = 0x%hx, actual = 0x%hx\n",
			base.m_2bytes, var.m_2bytes);
		failed = true;
	}
	if (var.m_4bytes != base.m_4bytes) {
		os::printf("FAIL: %s 4-byte member of returned "
			"fixed struct:\n", msgpfx);
		os::printf("\texpected = 0x%lx, actual = 0x%lx\n",
			base.m_4bytes, var.m_4bytes);
		failed = true;
	}
	if (var.m_8bytes != base.m_8bytes) {
		os::printf("FAIL: %s 8-byte member of returned "
			"fixed struct:\n", msgpfx);
		os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
			base.m_8bytes, var.m_8bytes);
		failed = true;
	}

	return (!failed);
}
