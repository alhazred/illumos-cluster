/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_data_container.cc	1.7	08/05/20 SMI"

#include <sys/os.h>

#ifdef ORBTEST_V1
#include <h/hatypetest_1.h>
#else
#include <h/hatypetest_0.h>
#endif

#include <orbtest/hatypes/client_impl.h>

#if defined(_KERNEL) || defined(_KERNEL_ORB)
#include <orbtest/hatypes/server_impl.h>
#endif

#include <nslib/data_container_impl.h>

//
// Data Container Object Test
//

const int IN = 1;
const int INOUT_IN = 2;
const int INOUT_OUT = 3;
const int OUT = 4;
const int RET = 5;

static bool
set(char *str, uint32_t len, const char *msgpfx, const int type,
		bool set = true);

static bool
verify(char *str, uint32_t len, const char *p, const int type)
{
	return (set(str, len, p, type, false));
}

//
// Test client interface.  Invoked by driver "programs" and, in turn,
// invokes the corresponding method on the test server side.
// Parameters:
//	server_ref  --	test server object to invoke.
//	num_repeat  --	how many times to run the test.
//	noverify    --	if true then server won't perform data verification.
// Returns:
//	Total test execution time.
//

int64_t
client_impl::test_data_container(
		hatypetest::server_ptr server_ref,
		int32_t num_repeat,
		uint32_t len,
		bool noverify,
		Environment &)
{
	os::hrtime_t	total_time = 0, start_time;

	if (noverify) {
		Environment	e3;

		server_ref->set_noverify(e3);
		if (! hatypes_env_ok(e3, msg_prefix())) {
			return (-1);
		}
	}

	for (; num_repeat > 0; --num_repeat) {
		Environment		e2;

		// Prepare in, and inout variables before passing
		// to the server

		char	*in_str = new char [len + 1];
		(void) set(in_str, len, msg_prefix(), IN);
		data_container_impl *in_data = new data_container_impl();
		data_container::data_ptr in = in_data->get_objref();
		in->set_data(in_str, e2);
		delete [] in_str;

		if (! hatypes_env_ok(e2, msg_prefix())) {
			CORBA::release(in);
			return (-1);
		}


		char	*io_str = new char [len + 1];

		(void) set(io_str, len, msg_prefix(), INOUT_IN);
		data_container_impl *io_data = new data_container_impl();
		data_container::data_ptr inout = io_data->get_objref();
		inout->set_data(io_str, e2);
		delete [] io_str;

		if (! hatypes_env_ok(e2, msg_prefix())) {
			CORBA::release(in);
			CORBA::release(inout);
			return (-1);
		}

		data_container::data_ptr out = data_container::data::_nil();
		data_container::data_ptr ret = data_container::data::_nil();

		// Invoke server.

		start_time = os::gethrtime();

		ret = server_ref->test_data_container(in, out, inout, len, e2);

		if (! hatypes_env_ok(e2, msg_prefix())) {
			total_time = -1;
			num_repeat = 0;		// end loop
		} else {

			total_time += os::gethrtime() - start_time;

			// verify out, inout(out) and return values

			char	*out_str, *ret_str, *io_out_str;

			out_str = out->obtain_data(e2);
			(void) verify(out_str, len, msg_prefix(), OUT);
			delete [] out_str;

			io_out_str = inout->obtain_data(e2);
			(void) verify(io_out_str, len, msg_prefix(), INOUT_OUT);
			delete [] io_out_str;

			ret_str = ret->obtain_data(e2);
			(void) verify(ret_str, len, msg_prefix(), RET);
			delete [] ret_str;
		}
		CORBA::release(in);
		CORBA::release(inout);
		CORBA::release(out);
		CORBA::release(ret);
	}

	return (total_time);
}

//
// Test server interface.  Invoked by the test client.
//
#if defined(_KERNEL) || defined(_KERNEL_ORB)
data_container::data_ptr
server_impl::test_data_container(
		data_container::data_ptr  in,
		data_container::data_out  out,
		data_container::data_ptr  &inout,
		uint32_t	length,
		Environment &e)
{

	Environment	e1;

	if (!verify_off) {

		// Verify object references from client.

		char	*in_str;
		in_str = in->obtain_data(e1);
		verify(in_str, length, msg_prefix(), IN);
		delete [] in_str;

		char	*io_str;
		io_str = inout->obtain_data(e1);
		verify(io_str, length, msg_prefix(), INOUT_IN);
		delete [] io_str;

	}

	// Prepare out, ret, and inout (out) string to be passed to client

	char	*io_out_str = new char [length + 1];
	set(io_out_str, length, msg_prefix(), INOUT_OUT);
	inout->set_data(io_out_str, e1);
	delete [] io_out_str;

	if (! hatypes_env_ok(e1, msg_prefix())) {
		e.exception(new data_container::invalid_data);
		return (data_container::data::_nil());
	}

	char	*out_str = new char [length + 1];
	set(out_str, length, msg_prefix(), OUT);
	data_container_impl *out_data = new data_container_impl();
	out_data->set_data(out_str, e1);
	out = out_data->get_objref();
	delete [] out_str;

	if (! hatypes_env_ok(e1, msg_prefix())) {
		e.exception(new data_container::invalid_data);
		return (data_container::data::_nil());
	}



	// duplicate in parameter to ret and return to the client.

	data_container::data_ptr ret = data_container::data::_duplicate(in);
	char	*ret_str = new char [length + 1];
	set(ret_str, length, msg_prefix(), RET);
	ret->set_data(ret_str, e1);
	delete [] ret_str;

	if (! hatypes_env_ok(e1, msg_prefix())) {
		e.exception(new data_container::invalid_data);
		return (data_container::data::_nil());
	}

	return (ret);
}
#endif

//
// Sets up (or verifies if "set" argument is false) parameter.
//
static bool
set(char *str, uint32_t len, const char *msgpfx, const int type, bool set)
{
	int	i;

	if (set) {

		for (i = 0; i < len; i++) {
			str[i] = type;
		}
		str[len] = 0;
		return (true);
	}

	for (i = 0; i < len; i++) {
		if (str[i] != type) {
			os::printf("FAIL: %s data container string "
				"has wrong data:\n", msgpfx);
			os::printf("\texpected = 0x%x, actual = 0x%x\n",
				type, str[i]);
			return (false);
		}
	}
	if (str[len] != 0) {
		os::printf("FAIL: %s data container string "
			"has wrong data:\n", msgpfx);
		os::printf("\texpected = 0, actual = 0x%x\n", str[i]);
		return (false);
	}

	return (true);
}
