/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_arrayfunion.cc	1.6	08/05/20 SMI"

#if defined(_KERNEL) || defined(_KERNEL_ORB)
#include <orbtest/hatypes/server_impl.h>
#endif
#include <orbtest/hatypes/client_impl.h>

//
// Forward declarations
//
static bool set_in(
	hatypetest::arrayFunion var,
	const char *msgpfx,
	bool set = true);

inline bool
verify_in(const hatypetest::arrayFunion v, const char *p)
{
	return (set_in((hatypetest::Funion (*)[10])v, p, false));
}


static bool set_out(
	hatypetest::arrayFunion var,
	const char *msgpfx,
	bool set = true);

inline bool
verify_out(hatypetest::arrayFunion v, const char *p)
{
	return (set_out(v, p, false));
}

static bool set_io_i(
	hatypetest::arrayFunion var,
	const char *msgpfx,
	bool set = true);

inline bool
verify_io_i(hatypetest::arrayFunion v, const char *p)
{
	return (set_io_i(v, p, false));
}

static bool set_io_o(
	hatypetest::arrayFunion var,
	const char *msgpfx,
	bool set = true);

inline bool
verify_io_o(hatypetest::arrayFunion v, const char *p)
{
	return (set_io_o(v, p, false));
}

#ifndef BUG_4086497
static bool set_ret(
	hatypetest::arrayFunion_slice *& var,
	const char *msgpfx,
	bool set = true);

inline bool
verify_ret(hatypetest::arrayFunion_slice *& v, const char *p)
{
	return (set_ret(v, p, false));
}
#endif	// ! BUG_4086497

//
// Array of Funion test (2x10 elements)
//
// server_impl::test_arrayfunion
//
#if defined(_KERNEL) || defined(_KERNEL_ORB)
#ifndef BUG_4086497
hatypetest::arrayFunion_slice *
#else
void
#endif	// ! BUG_4086497
server_impl::test_arrayfunion(
	const hatypetest::arrayFunion in,
	hatypetest::arrayFunion out,
	hatypetest::arrayFunion inout,
	Environment &)
{
	// os::printf("%s test_arrayfunion()\n", msg_prefix());

#ifndef BUG_4086497
	hatypetest::arrayFunion_slice * ret;
	ret = hatypetest::arrayFunion_alloc();

	if (ret == CORBAL::_nil) {
		os::printf("%s could not allocate return arrayFunion\n",
			msg_prefix());
		e.exception(new no_resources());
		return (CORBA::_nil);
	}
#endif	// ! BUG_4086497

	if (!verify_off) {
		// Verify values passed by client
		verify_in(in, msg_prefix());
		verify_io_i(inout, msg_prefix());
	}

	// Prepare values for return (populate)
	set_out(out, msg_prefix());
	set_io_o(inout, msg_prefix());

#ifndef BUG_4086497
	set_ret(ret, msg_prefix());
	return (ret);
#endif	// ! BUG_4086497
}
#endif

//
// client_impl::test_arrayfunion
//
int64_t
client_impl::test_arrayfunion(
	hatypetest::server_ptr server_ref,
	int32_t num_repeat,
	bool noverify,
	Environment &)
{
	// os::printf("%s test_arrayfunion()\n",msg_prefix());
	os::hrtime_t	total_time = 0, start_time;

	if (noverify) {
		Environment e2;

		server_ref->set_noverify(e2);
		if (! hatypes_env_ok(e2, msg_prefix())) {
			return (-1);
		}
	}

	hatypetest::arrayFunion * in, * out, * inout;

	// Allocate space for arrays dynamically to
	// avoid overflowing the stack with automatic variables
	// (Kernel stack is rather small)
	in = (hatypetest::arrayFunion *) new hatypetest::arrayFunion;
	out = (hatypetest::arrayFunion *) new hatypetest::arrayFunion;
	inout = (hatypetest::arrayFunion *) new hatypetest::arrayFunion;

	for (; num_repeat > 0; --num_repeat) {
		Environment e2;

#ifndef BUG_4086497
		arrayFunion_slice * ret;
#endif	// ! BUG_4086497

		// Prepare data to be passed to server.
		(void) set_in(*in, msg_prefix());
		(void) set_io_i(*inout, msg_prefix());

		// Invoke server.
		start_time = os::gethrtime();

#ifndef BUG_4086497
		ret = server_ref->test_arrayfunion(*in, *out, *inout, e2);
#else
		server_ref->test_arrayfunion(*in, *out, *inout, e2);
#endif	// ! BUG_4086497

		if (! hatypes_env_ok(e2, msg_prefix())) {
			total_time = -1;
			break;
		}

		total_time += os::gethrtime() - start_time;

		if (!noverify) {
			// Verify values returned by server.
			(void) verify_out(*out, msg_prefix());
			(void) verify_io_o(*inout, msg_prefix());

#ifndef BUG_4086497
			verify_ret (*ret, msg_prefix());
#endif	// ! BUG_4086497

		} // !noverify
	}

#ifndef BUG_4086497
	hatypetest::arrayFunion_free(ret);
#endif	// ! BUG_4086497

	// Release memory for arrays
	delete[] (in);
	delete[] (out);
	delete[] (inout);

	return (total_time);
}

//
// Common Functions
//
bool
set_in(hatypetest::arrayFunion var, const char *msgpfx, bool set)
{
	uint8_t			base_1byte	= 0xFF;
	uint16_t		base_2bytes	= 0x9999;
	uint32_t		base_4bytes	= 0x44444444;
	uint64_t 		base_8bytes	= 0x2222222222222222;
	hatypetest::FDiscrim	discrim		= hatypetest::f_1byte;
	int i, j;

	// Set data
	if (set) {
		for (i = 0; i < 2; ++i) {
			for (j = 0; j < 10; ++j) {
				switch (discrim) {
				case hatypetest::f_1byte:
					if (base_1byte == 0)
						base_1byte = 0xFF;
					var[i][j].m_1byte(base_1byte);

					base_1byte <<= 1;
					discrim = hatypetest::f_2bytes;
					break;

				case hatypetest::f_2bytes:
					if (base_2bytes == 0)
						base_2bytes = 0x9999;
					var[i][j].m_2bytes(base_2bytes);

					base_2bytes <<= 1;
					discrim = hatypetest::f_4bytes;
					break;

				case hatypetest::f_4bytes:
					if (base_4bytes == 0)
						base_4bytes = 0x44444444;

					var[i][j].m_4bytes(base_4bytes);

					base_4bytes <<= 1;
					discrim = hatypetest::f_8bytes;
					break;

				case hatypetest::f_8bytes:
					if (base_8bytes == 0)
						base_8bytes =
							0x2222222222222222;

					var[i][j].m_8bytes(base_8bytes);

					base_8bytes <<= 1;
					discrim = hatypetest::f_1byte;
					break;

				default:
					var[i][j]._d(discrim);
					break;

				} // switch (discrim)
			} // for j
		} // for i

		return (true);

	} // if (set)

	// Verify Data
	for (i = 0; i < 2; ++i) {
		for (j = 0; j < 10; ++j) {

			// Verify discrim value
			if (var[i][j]._d() != discrim) {
				os::printf("FAIL: %s discriminant of 'in' "
					"fixed union at index (%d,%d):\n",
					msgpfx, i, j);
				os::printf("\texpected = %d, actual = %d\n",
					discrim, var[i][j]._d());
				return (false);
			}

			switch (var[i][j]._d()) {
			case hatypetest::f_1byte:
				if (base_1byte == 0)
					base_1byte = 0xFF;
				if (var[i][j].m_1byte() != base_1byte) {
					os::printf("FAIL: %s 1-byte member of "
						"'in' fixed union at index "
						"(%d,%d):\n", msgpfx, i, j);
					os::printf("\texpected = 0x%x, actual "
						"= 0x%x\n", base_1byte,
						var[i][j].m_1byte());

					return (false);
				}

				base_1byte <<= 1;
				discrim = hatypetest::f_2bytes;
				break;

			case hatypetest::f_2bytes:
				if (base_2bytes == 0)
					base_2bytes = 0x9999;
				if (var[i][j].m_2bytes() != base_2bytes) {
					os::printf("FAIL: %s 2-bytes member of "
						"'in' fixed union at index "
						"(%d,%d):\n", msgpfx, i, j);
					os::printf("\texpected = 0x%hx, actual "
						"= 0x%hx\n", base_2bytes,
						var[i][j].m_2bytes());

					return (false);
				}

				base_2bytes <<= 1;
				discrim = hatypetest::f_4bytes;
				break;

			case hatypetest::f_4bytes:
				if (base_4bytes == 0)
					base_4bytes = 0x44444444;
				if (var[i][j].m_4bytes() != base_4bytes) {
					os::printf("FAIL: %s 4-bytes member of "
						"'in' fixed union at index "
						"(%d,%d):\n", msgpfx, i, j);
					os::printf("\texpected = 0x%lx, actual "
						"= 0x%lx\n", base_4bytes,
						var[i][j].m_4bytes());

					return (false);
				}

				base_4bytes <<= 1;
				discrim = hatypetest::f_8bytes;
				break;

			case hatypetest::f_8bytes:
				if (base_8bytes == 0)
					base_8bytes = 0x2222222222222222;
				if (var[i][j].m_8bytes() != base_8bytes) {
					os::printf("FAIL: %s 8-bytes member of "
						"'in' fixed union at index "
						"(%d,%d):\n", msgpfx, i, j);
					os::printf("\texpected = 0x%llx, actual"
						" = 0x%llx\n", base_8bytes,
						var[i][j].m_8bytes());

					return (false);
				}

				base_8bytes <<= 1;
				discrim = hatypetest::f_1byte;
				break;

			default:
				break;

			} // switch (discrim)
		} // for j
	} // for i

	return (true);
}

bool
set_out(hatypetest::arrayFunion var, const char *msgpfx, bool set)
{
	uint8_t			base_1byte	= 0xFF;
	uint16_t		base_2bytes	= 0x9999;
	uint32_t		base_4bytes	= 0x44444444;
	uint64_t 		base_8bytes	= 0x2222222222222222;
	hatypetest::FDiscrim	discrim		= hatypetest::f_1byte;
	int i, j;

	// Set data
	if (set) {
		for (i = 0; i < 2; ++i) {
			for (j = 0; j < 10; ++j) {
				switch (discrim) {
				case hatypetest::f_1byte:
					if (base_1byte == 0)
						base_1byte = 0xFF;
					var[i][j].m_1byte(base_1byte);

					base_1byte >>= 1;
					discrim = hatypetest::f_2bytes;
					break;

				case hatypetest::f_2bytes:
					if (base_2bytes == 0)
						base_2bytes = 0x9999;
					var[i][j].m_2bytes(base_2bytes);

					base_2bytes >>= 1;
					discrim = hatypetest::f_4bytes;
					break;

				case hatypetest::f_4bytes:
					if (base_4bytes == 0)
						base_4bytes = 0x44444444;

					var[i][j].m_4bytes(base_4bytes);

					base_4bytes >>= 1;
					discrim = hatypetest::f_8bytes;
					break;

				case hatypetest::f_8bytes:
					if (base_8bytes == 0)
						base_8bytes =
							0x2222222222222222;

					var[i][j].m_8bytes(base_8bytes);

					base_8bytes >>= 1;
					discrim = hatypetest::f_1byte;
					break;

				default:
					var[i][j]._d(discrim);
					break;

				} // switch (discrim)
			} // for j
		} // for i

		return (true);

	} // if (set)

	// Verify Data
	for (i = 0; i < 2; ++i) {
		for (j = 0; j < 10; ++j) {

			// Verify discrim value
			if (var[i][j]._d() != discrim) {
				os::printf("FAIL: %s discriminant of 'out' "
					"fixed union at index (%d,%d):\n",
					msgpfx, i, j);
				os::printf("\texpected = %d, actual = %d\n",
					discrim, var[i][j]._d());
				return (false);
			}

			switch (var[i][j]._d()) {
			case hatypetest::f_1byte:
				if (base_1byte == 0)
					base_1byte = 0xFF;
				if (var[i][j].m_1byte() != base_1byte) {
					os::printf("FAIL: %s 1-byte member of "
						"'out' fixed union at index "
						"(%d,%d):\n", msgpfx, i, j);
					os::printf("\texpected = 0x%x, actual "
						"= 0x%x\n", base_1byte,
						var[i][j].m_1byte());

					return (false);
				}

				base_1byte >>= 1;
				discrim = hatypetest::f_2bytes;
				break;

			case hatypetest::f_2bytes:
				if (base_2bytes == 0)
					base_2bytes = 0x9999;
				if (var[i][j].m_2bytes() != base_2bytes) {
					os::printf("FAIL: %s 2-bytes member of "
						"'out' fixed union at index "
						"(%d,%d):\n", msgpfx, i, j);
					os::printf("\texpected = 0x%hx, actual "
						"= 0x%hx\n", base_2bytes,
						var[i][j].m_2bytes());

					return (false);
				}

				base_2bytes >>= 1;
				discrim = hatypetest::f_4bytes;
				break;

			case hatypetest::f_4bytes:
				if (base_4bytes == 0)
					base_4bytes = 0x44444444;
				if (var[i][j].m_4bytes() != base_4bytes) {
					os::printf("FAIL: %s 4-bytes member of "
						"'out' fixed union at index "
						"(%d,%d):\n", msgpfx, i, j);
					os::printf("\texpected = 0x%lx, actual "
						"= 0x%lx\n", base_4bytes,
						var[i][j].m_4bytes());

					return (false);
				}

				base_4bytes >>= 1;
				discrim = hatypetest::f_8bytes;
				break;

			case hatypetest::f_8bytes:
				if (base_8bytes == 0)
					base_8bytes = 0x2222222222222222;
				if (var[i][j].m_8bytes() != base_8bytes) {
					os::printf("FAIL: %s 8-bytes member of "
						"'out' fixed union at index "
						"(%d,%d):\n", msgpfx, i, j);
					os::printf("\texpected = 0x%llx, actual"
						" = 0x%llx\n", base_8bytes,
						var[i][j].m_8bytes());

					return (false);
				}

				base_8bytes >>= 1;
				discrim = hatypetest::f_1byte;
				break;

			default:
				break;

			} // switch (discrim)
		} // for j
	} // for i

	return (true);
}


bool
set_io_i(hatypetest::arrayFunion var, const char *msgpfx, bool set)
{
	uint8_t			base_1byte	= ~0;
	uint16_t		base_2bytes	= ~0;
	uint32_t		base_4bytes	= ~0;
	uint64_t 		base_8bytes	= ~0;
	hatypetest::FDiscrim	discrim		= hatypetest::f_1byte;
	int i, j;

	// Set data
	if (set) {
		for (i = 0; i < 2; ++i) {
			for (j = 0; j < 10; ++j) {
				switch (discrim) {
				case hatypetest::f_1byte:
					if (base_1byte == 0)
						base_1byte = ~0;
					var[i][j].m_1byte(base_1byte);

					base_1byte >>= 1;
					discrim = hatypetest::f_2bytes;
					break;

				case hatypetest::f_2bytes:
					if (base_2bytes == 0)
						base_2bytes = ~0;
					var[i][j].m_2bytes(base_2bytes);

					base_2bytes >>= 1;
					discrim = hatypetest::f_4bytes;
					break;

				case hatypetest::f_4bytes:
					if (base_4bytes == 0)
						base_4bytes = ~0;

					var[i][j].m_4bytes(base_4bytes);

					base_4bytes >>= 1;
					discrim = hatypetest::f_8bytes;
					break;

				case hatypetest::f_8bytes:
					if (base_8bytes == 0)
						base_8bytes = ~0;

					var[i][j].m_8bytes(base_8bytes);

					base_8bytes >>= 1;
					discrim = hatypetest::f_1byte;
					break;

				default:
					var[i][j]._d(discrim);
					break;

				} // switch (discrim)
			} // for j
		} // for i

		return (true);

	} // if (set)

	// Verify Data
	for (i = 0; i < 2; ++i) {
		for (j = 0; j < 10; ++j) {

			// Verify discrim value
			if (var[i][j]._d() != discrim) {
				os::printf("FAIL: %s discriminant of 'io_i' "
					"fixed union at index (%d,%d):\n",
					msgpfx, i, j);
				os::printf("\texpected = %d, actual = %d\n",
					discrim, var[i][j]._d());
				return (false);
			}

			switch (var[i][j]._d()) {
			case hatypetest::f_1byte:
				if (base_1byte == 0)
					base_1byte = ~0;
				if (var[i][j].m_1byte() != base_1byte) {
					os::printf("FAIL: %s 1-byte member of "
						"'io_i' fixed union at index "
						"(%d,%d):\n", msgpfx, i, j);
					os::printf("\texpected = 0x%x, actual "
						"= 0x%x\n", base_1byte,
						var[i][j].m_1byte());

					return (false);
				}

				base_1byte >>= 1;
				discrim = hatypetest::f_2bytes;
				break;

			case hatypetest::f_2bytes:
				if (base_2bytes == 0)
					base_2bytes = ~0;
				if (var[i][j].m_2bytes() != base_2bytes) {
					os::printf("FAIL: %s 2-bytes member of "
						"'io_i' fixed union at index "
						"(%d,%d):\n", msgpfx, i, j);
					os::printf("\texpected = 0x%hx, actual "
						"= 0x%hx\n", base_2bytes,
						var[i][j].m_2bytes());

					return (false);
				}

				base_2bytes >>= 1;
				discrim = hatypetest::f_4bytes;
				break;

			case hatypetest::f_4bytes:
				if (base_4bytes == 0)
					base_4bytes = ~0;
				if (var[i][j].m_4bytes() != base_4bytes) {
					os::printf("FAIL: %s 4-bytes member of "
						"'io_i' fixed union at index "
						"(%d,%d):\n", msgpfx, i, j);
					os::printf("\texpected = 0x%lx, actual "
						"= 0x%lx\n", base_4bytes,
						var[i][j].m_4bytes());

					return (false);
				}

				base_4bytes >>= 1;
				discrim = hatypetest::f_8bytes;
				break;

			case hatypetest::f_8bytes:
				if (base_8bytes == 0)
					base_8bytes = ~0;
				if (var[i][j].m_8bytes() != base_8bytes) {
					os::printf("FAIL: %s 8-bytes member of "
						"'io_i' fixed union at index "
						"(%d,%d):\n", msgpfx, i, j);
					os::printf("\texpected = 0x%llx, actual"
						" = 0x%llx\n", base_8bytes,
						var[i][j].m_8bytes());

					return (false);
				}

				base_8bytes >>= 1;
				discrim = hatypetest::f_1byte;
				break;

			default:
				break;

			} // switch (discrim)
		} // for j
	} // for i

	return (true);
}


bool
set_io_o(hatypetest::arrayFunion var, const char *msgpfx, bool set)
{
	uint8_t			base_1byte	= ~0;
	uint16_t		base_2bytes	= ~0;
	uint32_t		base_4bytes	= ~0;
	uint64_t 		base_8bytes	= ~0;
	hatypetest::FDiscrim	discrim		= hatypetest::f_1byte;
	int i, j;

	// Set data
	if (set) {
		for (i = 0; i < 2; ++i) {
			for (j = 0; j < 10; ++j) {
				switch (discrim) {
				case hatypetest::f_1byte:
					if (base_1byte == 0)
						base_1byte = ~0;
					var[i][j].m_1byte(base_1byte);

					base_1byte <<= 1;
					discrim = hatypetest::f_2bytes;
					break;

				case hatypetest::f_2bytes:
					if (base_2bytes == 0)
						base_2bytes = ~0;
					var[i][j].m_2bytes(base_2bytes);

					base_2bytes <<= 1;
					discrim = hatypetest::f_4bytes;
					break;

				case hatypetest::f_4bytes:
					if (base_4bytes == 0)
						base_4bytes = ~0;
					var[i][j].m_4bytes(base_4bytes);

					base_4bytes <<= 1;
					discrim = hatypetest::f_8bytes;
					break;

				case hatypetest::f_8bytes:
					if (base_8bytes == 0)
						base_8bytes = ~0;
					var[i][j].m_8bytes(base_8bytes);

					base_8bytes <<= 1;
					discrim = hatypetest::f_1byte;
					break;

				default:
					break;

				} // switch (discrim)
			} // for j
		} // for i

		return (true);

	} // if (set)

	// Verify Data
	for (i = 0; i < 2; ++i) {
		for (j = 0; j < 10; ++j) {

			// Verify discrim value
			if (var[i][j]._d() != discrim) {
				os::printf("FAIL: %s discriminant of 'io_o' "
					"fixed union at index (%d,%d):\n",
					msgpfx, i, j);
				os::printf("\texpected = %d, actual = %d\n",
					discrim, var[i][j]._d());
				return (false);
			}

			switch (var[i][j]._d()) {
			case hatypetest::f_1byte:
				if (base_1byte == 0)
					base_1byte = ~0;
				if (var[i][j].m_1byte() != base_1byte) {
					os::printf("FAIL: %s 1-byte member of "
						"'io_o' fixed union at index "
						"(%d,%d):\n", msgpfx, i, j);
					os::printf("\texpected = 0x%x, actual "
						"= 0x%x\n", base_1byte,
						var[i][j].m_1byte());

					return (false);
				}

				base_1byte <<= 1;
				discrim = hatypetest::f_2bytes;
				break;

			case hatypetest::f_2bytes:
				if (base_2bytes == 0)
					base_2bytes = ~0;
				if (var[i][j].m_2bytes() != base_2bytes) {
					os::printf("FAIL: %s 2-bytes member of "
						"'io_o' fixed union at index "
						"(%d,%d):\n", msgpfx, i, j);
					os::printf("\texpected = 0x%hx, actual "
						"= 0x%hx\n", base_2bytes,
						var[i][j].m_2bytes());

					return (false);
				}

				base_2bytes <<= 1;
				discrim = hatypetest::f_4bytes;
				break;

			case hatypetest::f_4bytes:
				if (base_4bytes == 0)
					base_4bytes = ~0;
				if (var[i][j].m_4bytes() != base_4bytes) {
					os::printf("FAIL: %s 4-bytes member of "
						"'io_o' fixed union at index "
						"(%d,%d):\n", msgpfx, i, j);
					os::printf("\texpected = 0x%lx, actual "
						"= 0x%lx\n", base_4bytes,
						var[i][j].m_4bytes());

					return (false);
				}

				base_4bytes <<= 1;
				discrim = hatypetest::f_8bytes;
				break;

			case hatypetest::f_8bytes:
				if (base_8bytes == 0)
					base_8bytes = ~0;
				if (var[i][j].m_8bytes() != base_8bytes) {
					os::printf("FAIL: %s 8-bytes member of "
						"'io_o' fixed union at index "
						"(%d,%d):\n", msgpfx, i, j);
					os::printf("\texpected = 0x%llx, actual"
						" = 0x%llx\n", base_8bytes,
						var[i][j].m_8bytes());

					return (false);
				}

				base_8bytes <<= 1;
				discrim = hatypetest::f_1byte;
				break;

			default:
				break;

			} // switch (discrim)
		} // for j
	} // for i

	return (true);
}


#ifndef BUG_4086497
bool
set_ret(hatypetest::arrayFunion_slice *& var, const char *msgpfx, bool set)
{
	uint8_t			base_1byte	= ~0;
	uint16_t		base_2bytes	= ~0;
	uint32_t		base_4bytes	= ~0;
	uint64_t 		base_8bytes	= ~0;
	hatypetest::FDiscrim	discrim		= hatypetest::f_1byte;
	int i, j;

	// Set data
	if (set) {
		for (i = 0; i < 2; ++i) {
			for (j = 0; j < 10; ++j) {
				switch (discrim) {
				case hatypetest::f_1byte:
					var[i][j].m_1byte(base_1byte);

					base_1byte = ~base_1byte;
					discrim = hatypetest::f_2bytes;
					break;

				case hatypetest::f_2bytes:
					var[i][j].m_2bytes(base_2bytes);

					base_2bytes = ~base_2bytes;
					discrim = hatypetest::f_4bytes;
					break;

				case hatypetest::f_4bytes:
					var[i][j].m_4bytes(base_4bytes);

					base_4bytes = ~base_4bytes;
					discrim = hatypetest::f_8bytes;
					break;

				case hatypetest::f_8bytes:
					var[i][j].m_8bytes(base_8bytes);

					base_8bytes = ~base_8bytes;
					discrim = hatypetest::f_1byte;
					break;

				default:
					break;

				} // switch (discrim)
			} // for j
		} // for i

		return (true);

	} // if (set)

	// Verify Data
	for (i = 0; i < 2; ++i) {
		for (j = 0; j < 10; ++j) {

			// Verify discrim value
			if (var[i][j]._d() != discrim) {
				os::printf("FAIL: %s discriminant of 'ret' "
					"fixed union at index (%d,%d):\n",
					msgpfx, i, j);
				os::printf("\texpected = %d, actual = %d\n",
					discrim, var[i][j]._d());
				return (false);
			}

			switch (var[i][j]._d()) {
			case hatypetest::f_1byte:
				if (var[i][j].m_1byte() != base_1byte) {
					os::printf("FAIL: %s 1-byte member of "
						"'ret' fixed union at index "
						"(%d,%d):\n", msgpfx, i, j);
					os::printf("\texpected = 0x%x, actual "
						"= 0x%x\n", base_1byte,
						var[i][j].m_1byte());

					return (false);
				}

				base_1byte = ~base_1byte;
				discrim = hatypetest::f_2bytes;
				break;

			case hatypetest::f_2bytes:
				if (var[i][j].m_2bytes() != base_2bytes) {
					os::printf("FAIL: %s 2-bytes member of "
						"'ret' fixed union at index "
						"(%d,%d):\n", msgpfx, i, j);
					os::printf("\texpected = 0x%hx, actual "
						"= 0x%hx\n", base_2bytes,
						var[i][j].m_2bytes());

					return (false);
				}

				base_2bytes = ~base_2bytes;
				discrim = hatypetest::f_4bytes;
				break;

			case hatypetest::f_4bytes:
				if (var[i][j].m_4bytes() != base_4bytes) {
					os::printf("FAIL: %s 4-bytes member of "
						"'ret' fixed union at index "
						"(%d,%d):\n", msgpfx, i, j);
					os::printf("\texpected = 0x%lx, actual "
						"= 0x%lx\n", base_4bytes,
						var[i][j].m_4bytes());

					return (false);
				}

				base_4bytes = ~base_4bytes;
				discrim = hatypetest::f_8bytes;
				break;

			case hatypetest::f_8bytes:
				if (var[i][j].m_8bytes() != base_8bytes) {
					os::printf("FAIL: %s 8-bytes member of "
						"'ret' fixed union at index "
						"(%d,%d):\n", msgpfx, i, j);
					os::printf("\texpected = 0x%llx, actual"
						" = 0x%llx\n", base_8bytes,
						var[i][j].m_8bytes());

					return (false);
				}

				base_8bytes = ~base_8bytes;
				discrim = hatypetest::f_1byte;
				break;

			default:
				break;

			} // switch (discrim)
		} // for j
	} // for i

	return (true);
}
#endif	// ! BUG_4086497
