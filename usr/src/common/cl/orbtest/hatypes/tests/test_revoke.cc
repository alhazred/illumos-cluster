/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_revoke.cc	1.6	08/05/20 SMI"

//
// Revoke operation test
//

#include <orbtest/hatypes/client_impl.h>
#include <nslib/ns.h>

#if defined(_KERNEL) || defined(_KERNEL_ORB)
#include <orbtest/hatypes/server_impl.h>
#endif

//
// Test client interface.  Invoked by driver "programs" and, in turn,
// invokes the corresponding method on the test server side.
// Parameters:
//	server_ref  --	test server object to invoke.
//	num_repeat  --	(ignored)
//	noverify    --	if true then server won't perform data verification.
// Returns:
//	Total test execution time.
//
int64_t
client_impl::test_revoke(
		hatypetest::server_ptr server_ref,
		int32_t,
		bool noverify,
		Environment &)
{
	os::printf("%s test_revoke()\n", msg_prefix());
	CORBA::Object_ptr	objp;
	Environment e;
	naming::naming_context_var	root_ns;

	// Exception expected to be returned when an invocation is made
	// after the server is revoked.
	CORBA::INV_OBJREF	expected(EBADF, CORBA::COMPLETED_NO);

	Environment	e2;
	os::hrtime_t	total_time = 0, start_time;

	if (noverify) {
		server_ref->set_noverify(e2);
		if (! hatypes_env_ok(e2, msg_prefix())) {
			return (-1);
		}
	}

	root_ns = ns::root_nameserver();
	objp = root_ns->resolve("IDL Versioning test server A (kernel-land)",
				e);
	if (e.exception() || CORBA::is_nil(objp)) {
		(void) os::printf("FAILED\n");
		(void) os::printf("\tORB nameserver couldn't resolve "
		    "\"servername\"\n");
		e.exception()->print_exception("\t");
	}
	hatypetest::minverObjA_var	server_v;
	server_v = hatypetest::minverObjA::_narrow(objp);
	ASSERT(!CORBA::is_nil(server_v));

	start_time = os::gethrtime();
	server_ref->test_revoke(server_v, e2);
	if (! hatypes_env_ok(e2, msg_prefix())) {
		os::printf("%s FAIL: revoke failed\n", msg_prefix());
		return (-1);
	}
	total_time += os::gethrtime() - start_time;

	os::printf("%s revoke succeeded\n", msg_prefix());

	int version = server_v->version(e2);

/*
	// Now verify that revoke is in effect.
	// A no-argument invocation must fail with the expected exception.
	server_ref->test_noarg(e2);
*/
	if (hatypes_env_ok(e2, expected, msg_prefix())) {
		return (total_time);
	} else {
		return (-1);
	}
}

//
// Test server interface.  Invoked by the test client.
//
#if defined(_KERNEL) || defined(_KERNEL_ORB)
void
server_impl::test_revoke(hatypetest::minverObjA_ptr objref, Environment &)
{
	os::printf("%s test_revoke()\n", msg_prefix());

	Environment e;

	lck.lock();
	// Can only execute revoke once
	if (!(flags & (REVOKE | REVOKED))) {
		// Tell revoke thread to perform a revoke
		os::printf("%s Revoking server...\n", msg_prefix());
		flags |= REVOKE;

		obj_to_revoke = objref;
		revoke_start_cv.signal();
	} else {
		// Raise some exception
		// e.exception();
		os::warning("%s Revoke thread not created or has exited. "
			"Cannot execute test\n", msg_prefix());
	}
	lck.unlock();
}
#endif
