/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_bseq_useq.cc	1.6	08/05/20 SMI"

//
// Bounded sequence of unbounded sequences test
//

#include <orbtest/hatypes/client_impl.h>

#if defined(_KERNEL) || defined(_KERNEL_ORB)
#include <orbtest/hatypes/server_impl.h>
#endif


static bool	set_in(hatypetest::BseqUseq &var, uint32_t len,
			uint32_t maxlen, const char *msgpfx, bool set = true);

static bool	set_out(hatypetest::BseqUseq &var, uint32_t len,
			uint32_t maxlen, const char *msgpfx, bool set = true);

static bool	set_io_i(hatypetest::BseqUseq &var, uint32_t len,
			uint32_t maxlen, const char *msgpfx, bool set = true);

static bool	set_io_o(hatypetest::BseqUseq &var, uint32_t len,
			uint32_t maxlen, const char *msgpfx, bool set = true);

static bool	set_ret(hatypetest::BseqUseq &var, uint32_t len,
			uint32_t maxlen, const char *msgpfx, bool set = true);

inline bool
verify_in(const hatypetest::BseqUseq &v, uint32_t l, uint32_t m, const char *p)
{
	return (set_in((hatypetest::BseqUseq &)v, l, m, p, false));
}

inline bool
verify_out(const hatypetest::BseqUseq &v, uint32_t l, uint32_t m, const char *p)
{
	return (set_out((hatypetest::BseqUseq &)v, l, m, p, false));
}

inline bool
verify_io_i(const hatypetest::BseqUseq &v, uint32_t l, uint32_t m,
		const char *p)
{
	return (set_io_i((hatypetest::BseqUseq &)v, l, m, p, false));
}

inline bool
verify_io_o(const hatypetest::BseqUseq &v, uint32_t l, uint32_t m,
		const char *p)
{
	return (set_io_o((hatypetest::BseqUseq &)v, l, m, p, false));
}

inline bool
verify_ret(const hatypetest::BseqUseq &v, uint32_t l, uint32_t m, const char *p)
{
	return (set_ret((hatypetest::BseqUseq &)v, l, m, p, false));
}

//
// Test client interface.  Invoked by driver "programs" and, in turn,
// invokes the corresponding method on the test server side.
// Parameters:
//	server_ref  --	test server object to invoke.
//	num_repeat  --	how many times to run the test.
//	len	    --	length of the enclosing bounded sequence to test.
//	maxlen	    --	maximum length of the enclosed unbounded sequences.
//			Each sequence will be one element longer than the
//			previous starting with 1.  If maxlen is reached, then
//			the next sequence will restart with length 1.
//	noverify    --	if true then server won't perform data verification.
// Returns:
//	Total test execution time.
//
int64_t
client_impl::test_bseq_useq(
		hatypetest::server_ptr server_ref,
		int32_t num_repeat,
		uint32_t len,
		uint32_t maxlen,
		bool noverify,
		Environment &)
{
	// os::printf("%s test_bseq_useq()\n", msg_prefix());
	os::hrtime_t	total_time = 0, start_time;

	if (noverify) {
		Environment	e3;

		server_ref->set_noverify(e3);
		if (! hatypes_env_ok(e3, msg_prefix())) {
			return (-1);
		}
	}

	for (; num_repeat > 0; --num_repeat) {
		Environment		e2;
		hatypetest::BseqUseq	in, inout;
		hatypetest::BseqUseq_var	outp, retp;

		// Prepare data to be passed to server.
		(void) set_in(in, len, maxlen, msg_prefix());
		(void) set_io_i(inout, len, maxlen, msg_prefix());

		// Invoke server.
		start_time = os::gethrtime();
		retp = server_ref->test_bseq_useq(in, outp, inout, len, maxlen,
									e2);
		if (! hatypes_env_ok(e2, msg_prefix())) {
			total_time = -1;
			break;
		}
		total_time += os::gethrtime() - start_time;

		// Verify values returned by server.
		(void) verify_out(*outp, len, maxlen, msg_prefix());
		(void) verify_io_o(inout, len, maxlen, msg_prefix());
		(void) verify_ret(*retp, len, maxlen, msg_prefix());
	}

	return (total_time);
}

//
// Test server interface.  Invoked by the test client.
//
#if defined(_KERNEL) || defined(_KERNEL_ORB)
hatypetest::BseqUseq *
server_impl::test_bseq_useq(
		const hatypetest::BseqUseq &in,
		hatypetest::BseqUseq_out outp,
		hatypetest::BseqUseq &inout,
		uint32_t len,
		uint32_t maxlen,
		Environment &)
{
	// os::printf("%s test_bseq_useq()\n", msg_prefix());
	hatypetest::BseqUseq	*retp;		// don't use _var!

	if (!verify_off) {
		// Verify values passed by client.
		verify_in(in, len, maxlen, msg_prefix());
		verify_io_i(inout, len, maxlen, msg_prefix());
	}

	// Prepare values to be returned to client.
	outp = new hatypetest::BseqUseq;
	set_out(*outp, len, maxlen, msg_prefix());
	set_io_o(inout, len, maxlen, msg_prefix());
	retp = new hatypetest::BseqUseq;
	set_ret(*retp, len, maxlen, msg_prefix());

	return (retp);
}
#endif

//
// Sets up (or verifies if "set" argument is false) 'in' parameter.
//
bool
set_in(hatypetest::BseqUseq &var, uint32_t len, uint32_t maxlen,
		const char *msgpfx, bool set)
{
	bool		failed = false;
	uint32_t	inner, outer;
	// This way we don't have to know
	// the type of the sequence members
	hatypetest::Useq	base(1);

	base[0] = 0;

	if (set) {
		var.load(len, len, var.allocbuf(len), true);
		for (outer = 0; outer < var.length(); ++outer) {
			if (maxlen == 0) {
				var[outer].length(maxlen);
			} else {
				var[outer].length((outer % maxlen) + 1);
			}
			for (inner = 0; inner < var[outer].length(); ++inner) {
				var[outer][inner] = base[0];
				base[0] = (base[0] == 0) ? 1 : base[0] << 1;
			}
		}
		return (true);
	}

	// Verify data.
	if (var.maximum() != len) {
		os::printf("FAIL: %s maximum of 'in' bounded sequence:\n",
			msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			len, var.maximum());
		failed = true;
	}

	if (var.length() != len) {
		os::printf("FAIL: %s length of 'in' bounded sequence:\n",
			msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			len, var.length());
		return (false);
	}

	for (outer = 0; outer < var.length(); ++outer) {
		uint32_t	innerlen;

		if (maxlen == 0) {
			innerlen = maxlen;
		} else {
			innerlen = (outer % maxlen) + 1;
		}

		// Check inner sequences.
		if (var[outer].maximum() != innerlen) {
			os::printf("FAIL: %s maximum of unbounded sequence "
				"member at index %d of 'in' "
				"bounded sequence:\n", msgpfx, outer);
			os::printf("\texpected = %d, actual = %d\n",
				innerlen, var[outer].maximum());
			failed = true;
		}

		if (var[outer].length() != innerlen) {
			os::printf("FAIL: %s length of unbounded sequence "
				"member at index %d of 'in' "
				"bounded sequence:\n", msgpfx, outer);
			os::printf("\texpected = %d, actual = %d\n",
				innerlen, var[outer].length());
			failed = true;
			continue;
		}

		for (inner = 0; inner < var[outer].length(); ++inner) {
			if (var[outer][inner] == base[0]) {
				base[0] = (base[0] == 0) ? 1 : base[0] << 1;
				continue;
			}

			os::printf("FAIL: %s value at index %d of "
				"unbounded sequence member at index %d of "
				"'in' bounded sequence:\n",
				msgpfx, inner, outer);
			os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
				(uint64_t)base[0],
				(uint64_t)var[outer][inner]);
			failed = true;
			break;
		}
	}
	return (!failed);
}

//
// Sets up (or verifies if "set" argument is false) 'out' parameter.
//
bool
set_out(hatypetest::BseqUseq &var, uint32_t len, uint32_t maxlen,
		const char *msgpfx, bool set)
{
	bool		failed = false;
	uint32_t	inner, outer;
	// This way we don't have to know
	// the type of the sequence members
	hatypetest::Useq	base(1);
	base[0] = ~0;

	if (set) {
		var.load(len + 1, len, var.allocbuf(len + 1), true);
		for (outer = 0; outer < var.length(); ++outer) {
			if (maxlen == 0) {
				var[outer].length(maxlen);
			} else {
				var[outer].length((outer % maxlen) + 1);
			}
			for (inner = 0; inner < var[outer].length(); ++inner) {
				var[outer][inner] = base[0];
				base[0] = (base[0] == 0) ? ~0 : base[0] >> 1;
			}
		}
		return (true);
	}

	// Verify data.
	if (var.maximum() != len + 1) {
		os::printf("FAIL: %s maximum of 'out' bounded sequence:\n",
			msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			len + 1, var.maximum());
		failed = true;
	}

	if (var.length() != len) {
		os::printf("FAIL: %s length of 'out' bounded sequence:\n",
			msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			len, var.length());
		return (false);
	}

	for (outer = 0; outer < var.length(); ++outer) {
		uint32_t	innerlen;

		if (maxlen == 0) {
			innerlen = maxlen;
		} else {
			innerlen = (outer % maxlen) + 1;
		}

		// Check inner sequences.
		if (var[outer].maximum() != innerlen) {
			os::printf("FAIL: %s maximum of unbounded sequence "
				"member at index %d of 'out' "
				"bounded sequence:\n", msgpfx, outer);
			os::printf("\texpected = %d, actual = %d\n",
				innerlen, var[outer].maximum());
			failed = true;
		}

		if (var[outer].length() != innerlen) {
			os::printf("FAIL: %s length of unbounded sequence "
				"member at index %d of 'out' "
				"bounded sequence:\n", msgpfx, outer);
			os::printf("\texpected = %d, actual = %d\n",
				innerlen, var[outer].length());
			failed = true;
			continue;
		}

		for (inner = 0; inner < var[outer].length(); ++inner) {
			if (var[outer][inner] == base[0]) {
				base[0] = (base[0] == 0) ? ~0 : base[0] >> 1;
				continue;
			}

			os::printf("FAIL: %s value at index %d of "
				"unbounded sequence member at index %d of "
				"'out' bounded sequence:\n",
				msgpfx, inner, outer);
			os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
				(uint64_t)base[0],
				(uint64_t)var[outer][inner]);
			failed = true;
			break;
		}
	}
	return (!failed);
}

//
// Sets up (or verifies if "set" argument is false) incoming 'inout' parameter.
//
bool
set_io_i(hatypetest::BseqUseq &var, uint32_t len, uint32_t maxlen,
		const char *msgpfx, bool set)
{
	bool		failed = false;
	uint32_t	inner, outer;
	// This way we don't have to know
	// the type of the sequence members
	hatypetest::Useq	base(1);
	base[0] = ~0;

	if (set) {
		var.load(len, len, var.allocbuf(len), true);
		for (outer = 0; outer < var.length(); ++outer) {
			if (maxlen == 0) {
				var[outer].length(maxlen);
			} else {
				var[outer].length((outer % maxlen) + 1);
			}
			for (inner = 0; inner < var[outer].length(); ++inner) {
				var[outer][inner] = base[0];
				base[0] = (base[0] == 0) ? ~0 : base[0] << 1;
			}
		}
		return (true);
	}

	// Verify data.
	if (var.maximum() != len) {
		os::printf("FAIL: %s maximum of 'inout' bounded sequence:\n",
			msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			len, var.maximum());
		failed = true;
	}

	if (var.length() != len) {
		os::printf("FAIL: %s length of 'inout' bounded sequence:\n",
			msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			len, var.length());
		return (false);
	}

	for (outer = 0; outer < var.length(); ++outer) {
		uint32_t	innerlen;

		if (maxlen == 0) {
			innerlen = maxlen;
		} else {
			innerlen = (outer % maxlen) + 1;
		}

		// Check inner sequences.
		if (var[outer].maximum() != innerlen) {
			os::printf("FAIL: %s maximum of unbounded sequence "
				"member at index %d of 'inout' "
				"bounded sequence:\n", msgpfx, outer);
			os::printf("\texpected = %d, actual = %d\n",
				innerlen, var[outer].maximum());
			failed = true;
		}

		if (var[outer].length() != innerlen) {
			os::printf("FAIL: %s length of unbounded sequence "
				"member at index %d of 'inout' "
				"bounded sequence:\n", msgpfx, outer);
			os::printf("\texpected = %d, actual = %d\n",
				innerlen, var[outer].length());
			failed = true;
			continue;
		}

		for (inner = 0; inner < var[outer].length(); ++inner) {
			if (var[outer][inner] == base[0]) {
				base[0] = (base[0] == 0) ? ~0 : base[0] << 1;
				continue;
			}

			os::printf("FAIL: %s value at index %d of "
				"unbounded sequence member at index %d of "
				"'inout' bounded sequence:\n",
				msgpfx, inner, outer);
			os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
				(uint64_t)base[0],
				(uint64_t)var[outer][inner]);
			failed = true;
			break;
		}
	}
	return (!failed);
}

//
// Sets up (or verifies if "set" argument is false) outgoing 'inout' parameter.
//
bool
set_io_o(hatypetest::BseqUseq &var, uint32_t len, uint32_t maxlen,
		const char *msgpfx, bool set)
{
	bool		failed = false;
	uint32_t	inner, outer;
	// This way we don't have to know
	// the type of the sequence members
	hatypetest::Useq	base(1);
	base[0] = 0;

	if (set) {
		var.load(len + 1, len + 1, var.allocbuf(len + 1), true);
		for (outer = 0; outer < var.length(); ++outer) {
			if (maxlen == 0) {
				var[outer].length(maxlen);
			} else {
				var[outer].length((outer % maxlen) + 1);
			}
			for (inner = 0; inner < var[outer].length(); ++inner) {
				var[outer][inner] = base[0];
				base[0] = (base[0] == 0) ? ~0 : base[0] >> 1;
			}
		}
		return (true);
	}

	// Verify data.
	if (var.maximum() != len + 1) {
		os::printf("FAIL: %s maximum of 'inout' bounded sequence:\n",
			msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			len + 1, var.maximum());
		failed = true;
	}

	if (var.length() != len + 1) {
		os::printf("FAIL: %s length of 'inout' bounded sequence:\n",
			msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			len + 1, var.length());
		return (false);
	}

	for (outer = 0; outer < var.length(); ++outer) {
		uint32_t	innerlen;

		if (maxlen == 0) {
			innerlen = maxlen;
		} else {
			innerlen = (outer % maxlen) + 1;
		}

		// Check inner sequences.
		if (var[outer].maximum() != innerlen) {
			os::printf("FAIL: %s maximum of unbounded sequence "
				"member at index %d of 'inout' "
				"bounded sequence:\n", msgpfx, outer);
			os::printf("\texpected = %d, actual = %d\n",
				innerlen, var[outer].maximum());
			failed = true;
		}

		if (var[outer].length() != innerlen) {
			os::printf("FAIL: %s length of unbounded sequence "
				"member at index %d of 'inout' "
				"bounded sequence:\n", msgpfx, outer);
			os::printf("\texpected = %d, actual = %d\n",
				innerlen, var[outer].length());
			failed = true;
			continue;
		}

		for (inner = 0; inner < var[outer].length(); ++inner) {
			if (var[outer][inner] == base[0]) {
				base[0] = (base[0] == 0) ? ~0 : base[0] >> 1;
				continue;
			}

			os::printf("FAIL: %s value at index %d of "
				"unbounded sequence member at index %d of "
				"'inout' bounded sequence:\n",
				msgpfx, inner, outer);
			os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
				(uint64_t)base[0],
				(uint64_t)var[outer][inner]);
			failed = true;
			break;
		}
	}
	return (!failed);
}

//
// Sets up (or verifies if "set" argument is false) returned value.
//
bool
set_ret(hatypetest::BseqUseq &var, uint32_t len, uint32_t maxlen,
		const char *msgpfx, bool set)
{
	bool		failed = false;
	uint32_t	inner, outer;
	// This way we don't have to know
	// the type of the sequence members
	hatypetest::Useq	base(1);
	base[0] = 0;

	if (set) {
		var.load(len, len, var.allocbuf(len), true);
		for (outer = 0; outer < var.length(); ++outer) {
			if (maxlen == 0) {
				var[outer].length(maxlen);
			} else {
				var[outer].length((outer % maxlen) + 1);
			}
			for (inner = 0; inner < var[outer].length(); ++inner) {
				var[outer][inner] = base[0];
				base[0] = ~base[0];
			}
		}
		return (true);
	}

	// Verify data.
	if (var.maximum() != len) {
		os::printf("FAIL: %s maximum of returned bounded sequence:\n",
			msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			len, var.maximum());
		failed = true;
	}

	if (var.length() != len) {
		os::printf("FAIL: %s length of returned bounded sequence:\n",
			msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			len, var.length());
		return (false);
	}

	for (outer = 0; outer < var.length(); ++outer) {
		uint32_t	innerlen;

		if (maxlen == 0) {
			innerlen = maxlen;
		} else {
			innerlen = (outer % maxlen) + 1;
		}

		// Check inner sequences.
		if (var[outer].maximum() != innerlen) {
			os::printf("FAIL: %s maximum of unbounded sequence "
				"member at index %d of returned "
				"bounded sequence:\n", msgpfx, outer);
			os::printf("\texpected = %d, actual = %d\n",
				innerlen, var[outer].maximum());
			failed = true;
		}

		if (var[outer].length() != innerlen) {
			os::printf("FAIL: %s length of unbounded sequence "
				"member at index %d of returned "
				"bounded sequence:\n", msgpfx, outer);
			os::printf("\texpected = %d, actual = %d\n",
				innerlen, var[outer].length());
			failed = true;
			continue;
		}

		for (inner = 0; inner < var[outer].length(); ++inner) {
			if (var[outer][inner] == base[0]) {
				base[0] = ~base[0];
				continue;
			}

			os::printf("FAIL: %s value at index %d of "
				"unbounded sequence member at index %d of "
				"returned bounded sequence:\n",
				msgpfx, inner, outer);
			os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
				(uint64_t)base[0],
				(uint64_t)var[outer][inner]);
			failed = true;
			break;
		}
	}
	return (!failed);
}
