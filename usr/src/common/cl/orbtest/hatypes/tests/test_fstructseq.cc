/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_fstructseq.cc	1.6	08/05/20 SMI"

//
// Unbounded sequence of Fixed structure test
//

#include <orbtest/hatypes/client_impl.h>

#if defined(_KERNEL) || defined(_KERNEL_ORB)
#include <orbtest/hatypes/server_impl.h>
#endif

static bool	set_in(hatypetest::Fstructseq &var, uint32_t len,
		    const char *msgpfx,	bool set = true);

static bool	set_out(hatypetest::Fstructseq &var, uint32_t len,
		    const char *msgpfx,	bool set = true);

static bool	set_io_i(hatypetest::Fstructseq &var, uint32_t len,
		    const char *msgpfx,	bool set = true);

static bool	set_io_o(hatypetest::Fstructseq &var, uint32_t len,
		    const char *msgpfx,	bool set = true);

static bool	set_ret(hatypetest::Fstructseq &var, uint32_t len,
		    const char *msgpfx,	bool set = true);

inline bool
verify_in(const hatypetest::Fstructseq &v, uint32_t l, const char *p)
{
	return (set_in((hatypetest::Fstructseq &)v, l, p, false));
}

inline bool
verify_out(const hatypetest::Fstructseq &v, uint32_t l, const char *p)
{
	return (set_out((hatypetest::Fstructseq &)v, l, p, false));
}

inline bool
verify_io_i(const hatypetest::Fstructseq &v, uint32_t l, const char *p)
{
	return (set_io_i((hatypetest::Fstructseq &)v, l, p, false));
}

inline bool
verify_io_o(const hatypetest::Fstructseq &v, uint32_t l, const char *p)
{
	return (set_io_o((hatypetest::Fstructseq &)v, l, p, false));
}

inline bool
verify_ret(const hatypetest::Fstructseq &v, uint32_t l, const char *p)
{
	return (set_ret((hatypetest::Fstructseq &)v, l, p, false));
}

//
// Test client interface.  Invoked by driver "programs" and, in turn,
// invokes the corresponding method on the test server side.
// Parameters:
//	server_ref  --	test server object to invoke.
//	num_repeat  --	how many times to run the test.
//	len	    --	length of unbounded sequence to test.
//	noverify    --	if true then server won't perform data verification.
// Returns:
//	Total test execution time.
//
int64_t
client_impl::test_fstructseq(
		hatypetest::server_ptr server_ref,
		int32_t num_repeat,
		uint32_t len,
		bool noverify,
		Environment &)
{
	os::hrtime_t	total_time = 0, start_time;

	if (noverify) {
		Environment	e3;

		server_ref->set_noverify(e3);
		if (! hatypes_env_ok(e3, msg_prefix())) {
			return (-1);
		}
	}

	for (; num_repeat > 0; --num_repeat) {
		Environment		e2;
		hatypetest::Fstructseq		in, inout;
		hatypetest::Fstructseq_var	outp, retp;

		// Prepare data to be passed to server.
		set_in(in, len, msg_prefix());
		set_io_i(inout, len, msg_prefix());

		// Invoke server.
		start_time = os::gethrtime();
		retp = server_ref->test_fstructseq(in, outp, inout, len, e2);
		if (! hatypes_env_ok(e2, msg_prefix())) {
			total_time = -1;
			break;
		}
		total_time += os::gethrtime() - start_time;

		// Verify values returned by server.
		verify_out(*outp, len, msg_prefix());
		verify_io_o(inout, len, msg_prefix());
		verify_ret(*retp, len, msg_prefix());
	}

	return (total_time);
}

#if defined(_KERNEL) || defined(_KERNEL_ORB)
//
// Test server interface.  Invoked by the test client.
//
hatypetest::Fstructseq *
server_impl::test_fstructseq(
		const hatypetest::Fstructseq	&in,
		hatypetest::Fstructseq_out	outp,
		hatypetest::Fstructseq		&inout,
		uint32_t			len,
		Environment &)
{
	hatypetest::Fstructseq	*retp;		// don't use _var!

	// Verify values passed by client.
	if (!verify_off) {
		verify_in(in, len, msg_prefix());
		verify_io_i(inout, len, msg_prefix());
	}

	// Prepare values to be returned to client.
	outp = new hatypetest::Fstructseq;
	// Do not set out parameter

	set_io_o(inout, len, msg_prefix());
	retp = new hatypetest::Fstructseq;
	set_ret(*retp, len, msg_prefix());

	return (retp);
}
#endif

//
// Sets up (or verifies if "set" argument is false) 'in' parameter.
//
bool
set_in(hatypetest::Fstructseq &var, uint32_t len, const char *msgpfx, bool set)
{
	// This way we don't have to know
	// the type of the sequence members
	hatypetest::Fstructseq	base(1);

	base[0].m_1byte = (uint8_t)0x1;
	base[0].m_2bytes = (uint16_t)0x1;
	base[0].m_4bytes = (uint32_t)0x1;
	base[0].m_8bytes = (uint64_t)0x1;

	if (set) {
		var.load(len, len, var.allocbuf(len), true);
		for (uint32_t i = 0; i < var.length(); ++i) {
			var[i] = base[0];
		}
		return (true);
	}

	// Verify data.
	if (var.maximum() != len) {
		os::printf("FAIL: %s maximum of 'in' fstruct sequence:\n",
			msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			len, var.maximum());
	}

	if (var.length() != len) {
		os::printf("FAIL: %s length of 'in' fstruct sequence:\n",
			msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			len, var.length());
		return (false);
	}

	for (uint32_t i = 0; i < var.length(); ++i) {
		if ((var[i].m_1byte == base[0].m_1byte) &&
		    (var[i].m_2bytes == base[0].m_2bytes) &&
		    (var[i].m_4bytes == base[0].m_4bytes) &&
		    (var[i].m_8bytes == base[0].m_8bytes)) {
			continue;
		}

		os::printf("FAIL: %s value of 'in' fstruct sequence "
			"at index %d:\n", msgpfx, i);
		os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
			(uint64_t)base[0].m_8bytes, (uint64_t)var[i].m_8bytes);
		return (false);
	}

	return (true);
}

//
// Sets up (or verifies if "set" argument is false) 'out' parameter.
//
bool
set_out(hatypetest::Fstructseq &var, uint32_t len, const char *msgpfx, bool set)
{
	// This way we don't have to know
	// the type of the sequence members
	hatypetest::Fstructseq	base(1);

	base[0].m_1byte = (uint8_t)0x2;
	base[0].m_2bytes = (uint16_t)0x2;
	base[0].m_4bytes = (uint32_t)0x2;
	base[0].m_8bytes = (uint64_t)0x2;

	if (set) {
		var.load(len + 1, len, var.allocbuf(len + 1), true);
		for (uint32_t i = 0; i < var.length(); ++i) {
			var[i] = base[0];
		}
		return (true);
	}
	//
	// Just return because we do not set the out parameter.
	// Make sure length is set to 0.
	//
	if (var.length() != 0) {
		os::printf("FAIL: %s length of 'out' fstruct sequence:\n",
			msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			0, var.length());
		return (false);
	}

	return (true);
}

//
// Sets up (or verifies if "set" argument is false) incoming 'inout' parameter.
//
bool
set_io_i(hatypetest::Fstructseq &var,
	uint32_t len,
	const char *msgpfx,
	bool set)
{
	// This way we don't have to know
	// the type of the sequence members
	hatypetest::Fstructseq	base(1);

	base[0].m_1byte = (uint8_t)0x3;
	base[0].m_2bytes = (uint16_t)0x3;
	base[0].m_4bytes = (uint32_t)0x3;
	base[0].m_8bytes = (uint64_t)0x3;

	if (set) {
		var.load(len, len, var.allocbuf(len), true);
		for (uint32_t i = 0; i < var.length(); ++i) {
			var[i] = base[0];
		}
		return (true);
	}

	// Verify data.
	if (var.maximum() != len) {
		os::printf("FAIL: %s maximum of 'inout' "
			"fstruct sequence:\n", msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			len, var.maximum());
	}

	if (var.length() != len) {
		os::printf("FAIL: %s length of 'inout' "
			"fstruct sequence:\n", msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			len, var.length());
		return (false);
	}

	for (uint32_t i = 0; i < var.length(); ++i) {
		if ((var[i].m_1byte == base[0].m_1byte) &&
		    (var[i].m_2bytes == base[0].m_2bytes) &&
		    (var[i].m_4bytes == base[0].m_4bytes) &&
		    (var[i].m_8bytes == base[0].m_8bytes)) {
			continue;
		}

		os::printf("FAIL: %s value of 'inout' fstruct sequence "
			"at index %d:\n", msgpfx, i);
		os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
			(uint64_t)base[0].m_8bytes, (uint64_t)var[i].m_8bytes);
		return (false);
	}

	return (true);
}

//
// Sets up (or verifies if "set" argument is false) outgoing 'inout' parameter.
//
bool
set_io_o(hatypetest::Fstructseq &var,
	uint32_t len,
	const char *msgpfx,
	bool set)
{
	// This way we don't have to know
	// the type of the sequence members
	hatypetest::Fstructseq	base(1);

	base[0].m_1byte = (uint8_t)0x4;
	base[0].m_2bytes = (uint16_t)0x4;
	base[0].m_4bytes = (uint32_t)0x4;
	base[0].m_8bytes = (uint64_t)0x4;


	if (set) {
		var.load(len + 1, len + 1, var.allocbuf(len + 1), true);
		for (uint32_t i = 0; i < var.length(); ++i) {
			var[i] = base[0];
		}
		return (true);
	}

	// Verify data.
	if (var.maximum() != len + 1) {
		os::printf("FAIL: %s maximum of 'inout' "
			"fstruct sequence:\n", msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			len + 1, var.maximum());
	}

	if (var.length() != len + 1) {
		os::printf("FAIL: %s length of 'inout' "
			"fstruct sequence:\n", msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			len + 1, var.length());
		return (false);
	}

	for (uint32_t i = 0; i < var.length(); ++i) {
		if ((var[i].m_1byte == base[0].m_1byte) &&
		    (var[i].m_2bytes == base[0].m_2bytes) &&
		    (var[i].m_4bytes == base[0].m_4bytes) &&
		    (var[i].m_8bytes == base[0].m_8bytes)) {
			continue;
		}

		os::printf("FAIL: %s value of 'inout' fstruct sequence "
			"at index %d:\n", msgpfx, i);
		os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
			(uint64_t)base[0].m_8bytes, (uint64_t)var[i].m_8bytes);
		return (false);
	}

	return (true);
}

//
// Sets up (or verifies if "set" argument is false) returned value.
//
bool
set_ret(hatypetest::Fstructseq &var, uint32_t len, const char *msgpfx, bool set)
{
	// This way we don't have to know
	// the type of the sequence members
	hatypetest::Fstructseq	base(1);

	base[0].m_1byte = (uint8_t)0x5;
	base[0].m_2bytes = (uint16_t)0x5;
	base[0].m_4bytes = (uint32_t)0x5;
	base[0].m_8bytes = (uint64_t)0x5;

	if (set) {
		var.load(len, len, var.allocbuf(len), true);
		for (uint32_t i = 0; i < var.length(); ++i) {
			var[i] = base[0];
		}
		return (true);
	}

	// Verify data.
	if (var.maximum() != len) {
		os::printf("FAIL: %s maximum of returned "
			"fstruct sequence:\n", msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			len, var.maximum());
	}

	if (var.length() != len) {
		os::printf("FAIL: %s length of returned "
			"fstruct sequence:\n", msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			len, var.length());
		return (false);
	}

	for (uint32_t i = 0; i < var.length(); ++i) {
		if ((var[i].m_1byte == base[0].m_1byte) &&
		    (var[i].m_2bytes == base[0].m_2bytes) &&
		    (var[i].m_4bytes == base[0].m_4bytes) &&
		    (var[i].m_8bytes == base[0].m_8bytes)) {
			continue;
		}

		os::printf("FAIL: %s value of returned fstruct sequence at "
			"index %d:\n", msgpfx, i);
		os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
			(uint64_t)base[0].m_8bytes, (uint64_t)var[i].m_8bytes);
		return (false);
	}
	return (true);
}
