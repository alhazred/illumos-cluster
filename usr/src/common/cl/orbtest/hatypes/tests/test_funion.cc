/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_funion.cc	1.6	08/05/20 SMI"

//
// Fixed union test.  Because of compiler bug 4087749 we don't
// allow return of fixed-length unions and those checks have
// been removed but should be restored if union returns are
// allowed again.
//

#include <orbtest/hatypes/client_impl.h>

#if defined(_KERNEL) || defined(_KERNEL_ORB)
#include <orbtest/hatypes/server_impl.h>
#endif

static bool	set_in(hatypetest::Funion &var, hatypetest::FDiscrim discrim,
			const char *msgpfx, bool set = true);
static bool	set_out(hatypetest::Funion &var, hatypetest::FDiscrim discrim,
			const char *msgpfx, bool set = true);
static bool	set_io_i(hatypetest::Funion &var, hatypetest::FDiscrim discrim,
			const char *msgpfx, bool set = true);
static bool	set_io_o(hatypetest::Funion &var, hatypetest::FDiscrim discrim,
			const char *msgpfx, bool set = true);

inline bool
verify_in(const hatypetest::Funion &v, hatypetest::FDiscrim d, const char *p)
{
	return (set_in((hatypetest::Funion &)v, d, p, false));
}

inline bool
verify_out(const hatypetest::Funion &v, hatypetest::FDiscrim d, const char *p)
{
	return (set_out((hatypetest::Funion &)v, d, p, false));
}

inline bool
verify_io_i(const hatypetest::Funion &v, hatypetest::FDiscrim d, const char *p)
{
	return (set_io_i((hatypetest::Funion &)v, d, p, false));
}

inline bool
verify_io_o(const hatypetest::Funion &v, hatypetest::FDiscrim d, const char *p)
{
	return (set_io_o((hatypetest::Funion &)v, d, p, false));
}

//
// Test client interface.  Invoked by driver "programs" and, in turn,
// invokes the corresponding method on the test server side.
// Parameters:
//	server_ref  --	test server object to invoke.
//	num_repeat  --	how many times to run the test.
//	noverify    --	if true then server won't perform data verification.
// Returns:
//	Total test execution time.
//
int64_t
client_impl::test_funion(
		hatypetest::server_ptr server_ref,
		int32_t num_repeat,
		bool noverify,
		Environment &)
{
	// os::printf("%s test_funion()\n", msg_prefix());
	os::hrtime_t	total_time = 0, start_time;

	if (noverify) {
		Environment	e2;

		server_ref->set_noverify(e2);
		if (! hatypes_env_ok(e2, msg_prefix())) {
			return (-1);
		}
	}

	for (; num_repeat > 0; --num_repeat) {
		// For each possible union discriminant value
		for (int i = hatypetest::f_1byte; i < hatypetest::f_end; ++i) {
			Environment		e2;
			hatypetest::FDiscrim discrim = (hatypetest::FDiscrim) i;
			hatypetest::Funion	in, out, inout;

			// Prepare data to be passed to server.
			set_in(in, discrim, msg_prefix());
			set_io_i(inout, discrim, msg_prefix());

			// Invoke server.
			start_time = os::gethrtime();
			server_ref->test_funion(in, out, inout, discrim, e2);
			if (! hatypes_env_ok(e2, msg_prefix())) {
				total_time = -1;
				num_repeat = 0;		// end outer loop
				break;
			}
			total_time += os::gethrtime() - start_time;

			// Verify values returned by server.
			verify_out(out, discrim, msg_prefix());
			verify_io_o(inout, discrim, msg_prefix());
		}
	}

	return (total_time);
}

//
// Test server interface.  Invoked by the test client.
//
#if defined(_KERNEL) || defined(_KERNEL_ORB)
void
server_impl::test_funion(
		const hatypetest::Funion &in,
		hatypetest::Funion &out,
		hatypetest::Funion &inout,
		hatypetest::FDiscrim discrim,
		Environment &)
{
	// os::printf("%s test_funion()\n", msg_prefix());

	// Verify values passed by client.
	if (!verify_off) {
		verify_in(in, discrim, msg_prefix());
		verify_io_i(inout, discrim, msg_prefix());
	}

	// Prepare values to be returned to client.
	set_out(out, discrim, msg_prefix());
	set_io_o(inout, discrim, msg_prefix());
}
#endif

//
// Sets up (or verifies if "set" argument is false) 'in' parameter.
//
bool
set_in(hatypetest::Funion &var, hatypetest::FDiscrim discrim,
	const char *msgpfx, bool set)
{
	const uint8_t	base_1byte = 0xF7;
	const uint16_t	base_2bytes = 0xeffe;
	const uint32_t	base_4bytes = 0xfedcba98;
	const uint64_t	base_8bytes = 0xfedcba9876543210;
	bool		failed = false;

	if (set) {
		switch (discrim) {
		case hatypetest::f_1byte:
			var.m_1byte(base_1byte);
			break;

		case hatypetest::f_2bytes:
			var.m_2bytes(base_2bytes);
			break;

		case hatypetest::f_4bytes:
			var.m_4bytes(base_4bytes);
			break;

		case hatypetest::f_8bytes:
			var.m_8bytes(base_8bytes);
			break;

		default:
			var._d(discrim);
			break;
		}
		return (true);
	}

	// Verify data.
	if (var._d() != discrim) {
		os::printf("FAIL: %s discriminant of 'in' fixed union:\n",
			msgpfx);
		os::printf("\texpected = %d, actual = %d\n", discrim, var._d());
		return (false);
	}

	switch (var._d()) {
	case hatypetest::f_1byte:
		if (var.m_1byte() != base_1byte) {
			os::printf("FAIL: %s 1-byte member of 'in' fixed "
				"union:\n", msgpfx);
			os::printf("\texpected = 0x%x, actual = 0x%x\n",
				base_1byte, var.m_1byte());
			failed = true;
		}
		break;

	case hatypetest::f_2bytes:
		if (var.m_2bytes() != base_2bytes) {
			os::printf("FAIL: %s 2-byte member of 'in' fixed "
				"union:\n", msgpfx);
			os::printf("\texpected = 0x%hx, actual = 0x%hx\n",
				base_2bytes, var.m_2bytes());
			failed = true;
		}
		break;

	case hatypetest::f_4bytes:
		if (var.m_4bytes() != base_4bytes) {
			os::printf("FAIL: %s 4-byte member of 'in' fixed "
				"union:\n", msgpfx);
			os::printf("\texpected = 0x%lx, actual = 0x%lx\n",
				base_4bytes, var.m_4bytes());
			failed = true;
		}
		break;

	case hatypetest::f_8bytes:
		if (var.m_8bytes() != base_8bytes) {
			os::printf("FAIL: %s 8-byte member of 'in' fixed "
				"union:\n", msgpfx);
			os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
				base_8bytes, var.m_8bytes());
			failed = true;
		}
		break;

	default:
		break;
	}

	return (!failed);
}

//
// Sets up (or verifies if "set" argument is false) 'out' parameter.
//
bool
set_out(hatypetest::Funion &var, hatypetest::FDiscrim discrim,
	const char *msgpfx, bool set)
{
	const uint8_t		base_1byte = 0x3F;
	const uint16_t		base_2bytes = 0x6666;
	const uint32_t		base_4bytes = 0x12345678;
	const uint64_t		base_8bytes = 0xca8642013579bdf;
	hatypetest::FDiscrim	base_discrim =
					(hatypetest::FDiscrim) (discrim + 1);
	bool			failed = false;

	if (set) {
		switch (base_discrim) {
		case hatypetest::f_1byte:
			var.m_1byte(base_1byte);
			break;

		case hatypetest::f_2bytes:
			var.m_2bytes(base_2bytes);
			break;

		case hatypetest::f_4bytes:
			var.m_4bytes(base_4bytes);
			break;

		case hatypetest::f_8bytes:
			var.m_8bytes(base_8bytes);
			break;

		default:
			var._d(base_discrim);
			break;
		}
		return (true);
	}

	// Verify data.
	if (var._d() != base_discrim) {
		os::printf("FAIL: %s discriminant of 'out' fixed union:\n",
			msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			base_discrim, var._d());
		return (false);
	}

	switch (var._d()) {
	case hatypetest::f_1byte:
		if (var.m_1byte() != base_1byte) {
			os::printf("FAIL: %s 1-byte member of 'out' fixed "
				"union:\n", msgpfx);
			os::printf("\texpected = 0x%x, actual = 0x%x\n",
				base_1byte, var.m_1byte());
			failed = true;
		}
		break;

	case hatypetest::f_2bytes:
		if (var.m_2bytes() != base_2bytes) {
			os::printf("FAIL: %s 2-byte member of 'out' fixed "
				"union:\n", msgpfx);
			os::printf("\texpected = 0x%hx, actual = 0x%hx\n",
				base_2bytes, var.m_2bytes());
			failed = true;
		}
		break;

	case hatypetest::f_4bytes:
		if (var.m_4bytes() != base_4bytes) {
			os::printf("FAIL: %s 4-byte member of 'out' fixed "
				"union:\n", msgpfx);
			os::printf("\texpected = 0x%lx, actual = 0x%lx\n",
				base_4bytes, var.m_4bytes());
			failed = true;
		}
		break;

	case hatypetest::f_8bytes:
		if (var.m_8bytes() != base_8bytes) {
			os::printf("FAIL: %s 8-byte member of 'out' fixed "
				"union:\n", msgpfx);
			os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
				base_8bytes, var.m_8bytes());
			failed = true;
		}
		break;

	default:
		break;
	}

	return (!failed);
}

//
// Sets up (or verifies if "set" argument is false) incoming 'inout' parameter.
//
bool
set_io_i(hatypetest::Funion &var, hatypetest::FDiscrim discrim,
		const char *msgpfx, bool set)
{
	const uint8_t	base_1byte = 0xF3;
	const uint16_t	base_2bytes = 0x4444;
	const uint32_t	base_4bytes = 0x98765432;
	const uint64_t	base_8bytes = 0xfdb9753102468ac;
	bool		failed = false;

	if (set) {
		switch (discrim) {
		case hatypetest::f_1byte:
			var.m_1byte(base_1byte);
			break;

		case hatypetest::f_2bytes:
			var.m_2bytes(base_2bytes);
			break;

		case hatypetest::f_4bytes:
			var.m_4bytes(base_4bytes);
			break;

		case hatypetest::f_8bytes:
			var.m_8bytes(base_8bytes);
			break;

		default:
			var._d(discrim);
			break;
		}
		return (true);
	}

	// Verify data.
	if (var._d() != discrim) {
		os::printf("FAIL: %s discriminant of 'inout' fixed union:\n",
			msgpfx);
		os::printf("\texpected = %d, actual = %d\n", discrim, var._d());
		return (false);
	}

	switch (var._d()) {
	case hatypetest::f_1byte:
		if (var.m_1byte() != base_1byte) {
			os::printf("FAIL: %s 1-byte member of 'inout' fixed "
				"union:\n", msgpfx);
			os::printf("\texpected = 0x%x, actual = 0x%x\n",
				base_1byte, var.m_1byte());
			failed = true;
		}
		break;

	case hatypetest::f_2bytes:
		if (var.m_2bytes() != base_2bytes) {
			os::printf("FAIL: %s 2-byte member of 'inout' fixed "
				"union:\n", msgpfx);
			os::printf("\texpected = 0x%hx, actual = 0x%hx\n",
				base_2bytes, var.m_2bytes());
			failed = true;
		}
		break;

	case hatypetest::f_4bytes:
		if (var.m_4bytes() != base_4bytes) {
			os::printf("FAIL: %s 4-byte member of 'inout' fixed "
				"union:\n", msgpfx);
			os::printf("\texpected = 0x%lx, actual = 0x%lx\n",
				base_4bytes, var.m_4bytes());
			failed = true;
		}
		break;

	case hatypetest::f_8bytes:
		if (var.m_8bytes() != base_8bytes) {
			os::printf("FAIL: %s 8-byte member of 'inout' fixed "
				"union:\n", msgpfx);
			os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
				base_8bytes, var.m_8bytes());
			failed = true;
		}
		break;

	default:
		break;
	}

	return (!failed);
}

//
// Sets up (or verifies if "set" argument is false) outgoing 'inout' parameter.
//
bool
set_io_o(hatypetest::Funion &var, hatypetest::FDiscrim discrim,
		const char *msgpfx, bool set)
{
	const uint8_t		base_1byte = 0x7F;
	const uint16_t		base_2bytes = 0xfeef;
	const uint32_t		base_4bytes = 0x89abcdef;
	const uint64_t		base_8bytes = 0x0123456789abcdef;
	hatypetest::FDiscrim	base_discrim =
					(hatypetest::FDiscrim) (discrim - 1);
	bool			failed = false;

	if (set) {
		switch (base_discrim) {
		case hatypetest::f_1byte:
			var.m_1byte(base_1byte);
			break;

		case hatypetest::f_2bytes:
			var.m_2bytes(base_2bytes);
			break;

		case hatypetest::f_4bytes:
			var.m_4bytes(base_4bytes);
			break;

		case hatypetest::f_8bytes:
			var.m_8bytes(base_8bytes);
			break;

		default:
			var._d(base_discrim);
			break;
		}
		return (true);
	}

	// Verify data.
	if (var._d() != base_discrim) {
		os::printf("FAIL: %s discriminant of 'inout' fixed union:\n",
			msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			base_discrim, var._d());
		return (false);
	}

	switch (var._d()) {
	case hatypetest::f_1byte:
		if (var.m_1byte() != base_1byte) {
			os::printf("FAIL: %s 1-byte member of 'inout' fixed "
				"union:\n", msgpfx);
			os::printf("\texpected = 0x%x, actual = 0x%x\n",
				base_1byte, var.m_1byte());
			failed = true;
		}
		break;

	case hatypetest::f_2bytes:
		if (var.m_2bytes() != base_2bytes) {
			os::printf("FAIL: %s 2-byte member of 'inout' fixed "
				"union:\n", msgpfx);
			os::printf("\texpected = 0x%hx, actual = 0x%hx\n",
				base_2bytes, var.m_2bytes());
			failed = true;
		}
		break;

	case hatypetest::f_4bytes:
		if (var.m_4bytes() != base_4bytes) {
			os::printf("FAIL: %s 4-byte member of 'inout' fixed "
				"union:\n", msgpfx);
			os::printf("\texpected = 0x%lx, actual = 0x%lx\n",
				base_4bytes, var.m_4bytes());
			failed = true;
		}
		break;

	case hatypetest::f_8bytes:
		if (var.m_8bytes() != base_8bytes) {
			os::printf("FAIL: %s 8-byte member of 'inout' fixed "
				"union:\n", msgpfx);
			os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
				base_8bytes, var.m_8bytes());
			failed = true;
		}
		break;

	default:
		break;
	}

	return (!failed);
}

//
// Sets up (or verifies if "set" argument is false) returned value.
//
bool
set_ret(hatypetest::Funion &var, hatypetest::FDiscrim discrim,
		const char *msgpfx, bool set)
{
	const uint8_t	base_1byte = 0xAA;
	const uint16_t	base_2bytes = 0xfedc;
	const uint32_t	base_4bytes = 0x96f6e7a3;
	const uint64_t	base_8bytes = 0xffffffffffffff00;
	bool		failed = false;

	if (set) {
		switch (discrim) {
		case hatypetest::f_1byte:
			var.m_1byte(base_1byte);
			break;

		case hatypetest::f_2bytes:
			var.m_2bytes(base_2bytes);
			break;

		case hatypetest::f_4bytes:
			var.m_4bytes(base_4bytes);
			break;

		case hatypetest::f_8bytes:
			var.m_8bytes(base_8bytes);
			break;

		default:
			var._d(discrim);
			break;
		}
		return (true);
	}

	// Verify data.
	if (var._d() != discrim) {
		os::printf("FAIL: %s discriminant of returned fixed union:\n",
			msgpfx);
		os::printf("\texpected = %d, actual = %d\n", discrim, var._d());
		return (false);
	}

	switch (var._d()) {
	case hatypetest::f_1byte:
		if (var.m_1byte() != base_1byte) {
			os::printf("FAIL: %s 1-byte member of returned fixed "
				"union:\n", msgpfx);
			os::printf("\texpected = 0x%x, actual = 0x%x\n",
				base_1byte, var.m_1byte());
			failed = true;
		}
		break;

	case hatypetest::f_2bytes:
		if (var.m_2bytes() != base_2bytes) {
			os::printf("FAIL: %s 2-byte member of returned fixed "
				"union:\n", msgpfx);
			os::printf("\texpected = 0x%hx, actual = 0x%hx\n",
				base_2bytes, var.m_2bytes());
			failed = true;
		}
		break;

	case hatypetest::f_4bytes:
		if (var.m_4bytes() != base_4bytes) {
			os::printf("FAIL: %s 4-byte member of returned fixed "
				"union:\n", msgpfx);
			os::printf("\texpected = 0x%lx, actual = 0x%lx\n",
				base_4bytes, var.m_4bytes());
			failed = true;
		}
		break;

	case hatypetest::f_8bytes:
		if (var.m_8bytes() != base_8bytes) {
			os::printf("FAIL: %s 8-byte member of returned fixed "
				"union:\n", msgpfx);
			os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
				base_8bytes, var.m_8bytes());
			failed = true;
		}
		break;

	default:
		break;
	}

	return (!failed);
}
