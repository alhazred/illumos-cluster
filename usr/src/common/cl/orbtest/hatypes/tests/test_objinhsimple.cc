/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_objinhsimple.cc	1.6	08/05/20 SMI"

//
// Object reference test
//

#include <orbtest/hatypes/client_impl.h>

#if defined(_KERNEL) || defined(_KERNEL_ORB)
#include <orbtest/hatypes/server_impl.h>
#endif

static bool	verify(hatypetest::InhObjA_ptr objp, int32_t expected_id,
		    const char *paramtype, const char *msgpfx,
		    char expected_name);

static bool	local_narrow_test();

//
// Test object inheritance hierarchies:
//
//	hatypetest::InhObjA	hatypetest::Obj
//		^
//		|
//	hatypetest::InhObjB
//		^
//		|
//	hatypetest::InhObjC
//

//
// Local narrow test.
// Returns:
//	True if test passes.
//
bool
local_narrow_test()
{
	const uint32_t		timeout = 30;

	// NOTE: The following is NOT the recommended way to allocate object
	// implementations.  Object impls should be allocated in the heap
	// instead of on the stack.  We're doing it this way only for
	// convenience and because we don't do "delete this" in their
	// _unreferenced() methods.
	InhObjA_impl		A_obj;
	InhObjB_impl		B_obj;
	InhObjC_impl		C_obj;
	Obj_impl		Obj;

	hatypetest::InhObjA_ptr	A_ref;
	hatypetest::InhObjB_ptr	B_ref;
	hatypetest::InhObjC_ptr	C_ref;
	hatypetest::Obj_ptr	Obj_ref;
	CORBA::Object_ptr	objp;
	bool			retval = true;

	A_ref = A_obj.get_objref();
	B_ref = B_obj.get_objref();
	C_ref = C_obj.get_objref();
	Obj_ref = Obj.get_objref();

	//
	// hatypetest::InhObjA test:
	//
	//	Narrowing to InhObjA (self) should succeed.
	//	Narrowing to InhObjB, InhObjC and Obj should fail.
	//
	objp = hatypetest::InhObjA::_narrow(A_ref);
	if (is_not_nil(objp)) {
		CORBA::release(objp);				// pass
	} else {
		os::printf("FAIL: Object narrowed to self "
		    "returned nil (InhObjA --> InhObjA)\n");
		retval = false;
	}

	objp = hatypetest::InhObjB::_narrow(A_ref);
	if (is_not_nil(objp)) {
		os::printf("FAIL: Object narrowed to child class "
		    "returned non-nil (InhObjA --> InhObjB)\n");
		CORBA::release(objp);
		retval = false;
	}

	objp = hatypetest::InhObjC::_narrow(A_ref);
	if (is_not_nil(objp)) {
		os::printf("FAIL: Object narrowed to grandchild class "
		    "returned non-nil (InhObjA --> InhObjC)\n");
		CORBA::release(objp);
		retval = false;
	}

	objp = hatypetest::Obj::_narrow(A_ref);
	if (is_not_nil(objp)) {
		os::printf("FAIL: Object narrowed to unrelated class "
		    "returned non-nil (InhObjA --> Obj)\n");
		CORBA::release(objp);
		retval = false;
	}

	//
	// hatypetest::InhObjB test:
	//
	//	Narrowing to InhObjA and InhObjB should succeed.
	//	Narrowing to InhObjC and Obj should fail.
	//
	objp = hatypetest::InhObjA::_narrow(B_ref);
	if (is_not_nil(objp)) {
		CORBA::release(objp);				// pass
	} else {
		os::printf("FAIL: Object narrowed to parent class "
		    "returned nil (InhObjB --> InhObjA)\n");
		retval = false;
	}

	objp = hatypetest::InhObjB::_narrow(B_ref);
	if (is_not_nil(objp)) {
		CORBA::release(objp);				// pass
	} else {
		os::printf("FAIL: Object narrowed to self "
		    "returned nil (InhObjB --> InhObjB)\n");
		retval = false;
	}

	objp = hatypetest::InhObjC::_narrow(B_ref);
	if (is_not_nil(objp)) {
		os::printf("FAIL: Object narrowed to child class "
		    "returned non-nil (InhObjB --> InhObjC)\n");
		CORBA::release(objp);
		retval = false;
	}

	objp = hatypetest::Obj::_narrow(B_ref);
	if (is_not_nil(objp)) {
		os::printf("FAIL: Object narrowed to unrelated class "
		    "returned non-nil (InhObjB --> Obj)\n");
		CORBA::release(objp);
		retval = false;
	}

	//
	// hatypetest::InhObjC test
	//
	//	Narrowing to InhObjA, InhObjB and InhObjC should succeed.
	//	Narrowing to Obj should fail.
	//
	objp = hatypetest::InhObjA::_narrow(C_ref);
	if (is_not_nil(objp)) {
		CORBA::release(objp);				// pass
	} else {
		os::printf("FAIL: Object narrowed to grandparent class "
		    "returned nil (InhObjC --> InhObjA)\n");
		retval = false;
	}

	objp = hatypetest::InhObjB::_narrow(C_ref);
	if (is_not_nil(objp)) {
		CORBA::release(objp);				// pass
	} else {
		os::printf("FAIL: Object narrowed to parent class "
		    "returned nil (InhObjC --> InhObjB)\n");
		retval = false;
	}

	objp = hatypetest::InhObjC::_narrow(C_ref);
	if (is_not_nil(objp)) {
		CORBA::release(objp);				// pass
	} else {
		os::printf("FAIL: Object narrowed to self "
		    "returned nil (InhObjC --> InhObjC)\n");
		retval = false;
	}

	objp = hatypetest::Obj::_narrow(C_ref);
	if (is_not_nil(objp)) {
		os::printf("FAIL: Object narrowed to unrelated class "
		    "returned non-nil (InhObjC --> Obj)\n");
		CORBA::release(objp);
		retval = false;
	}

	//
	// hatypetest::Obj test
	//	Narrowing to InhObjA, InhObjB and InhObjC should fail.
	//
	objp = hatypetest::InhObjA::_narrow(Obj_ref);
	if (is_not_nil(objp)) {
		os::printf("FAIL: Object narrowed to unrelated class "
		    "returned non-nil (Obj --> InhObjA)\n");
		CORBA::release(objp);
		retval = false;
	}

	objp = hatypetest::InhObjB::_narrow(Obj_ref);
	if (is_not_nil(objp)) {
		os::printf("FAIL: Object narrowed to unrelated class "
		    "returned non-nil (Obj --> InhObjB)\n");
		CORBA::release(objp);
		retval = false;
	}

	objp = hatypetest::InhObjC::_narrow(Obj_ref);
	if (is_not_nil(objp)) {
		os::printf("FAIL: Object narrowed to unrelated class "
		    "returned non-nil (Obj --> InhObjC)\n");
		CORBA::release(objp);
		retval = false;
	}

	//
	// Cleanup
	//
	CORBA::release(A_ref);
	CORBA::release(B_ref);
	CORBA::release(C_ref);
	CORBA::release(Obj_ref);

	if (! A_obj.wait_until_unreferenced(timeout)) {
		retval = false;
	}
	if (! B_obj.wait_until_unreferenced(timeout)) {
		retval = false;
	}
	if (! C_obj.wait_until_unreferenced(timeout)) {
		retval = false;
	}
	if (! Obj.wait_until_unreferenced(timeout)) {
		retval = false;
	}

	return (retval);
}

//
// Test client interface.  Invoked by driver "programs" and, in turn,
// invokes the corresponding method on the test server side.
// Parameters:
//	server_ref  --	test server object to invoke.
//	num_repeat  --	how many times to run the test.
//	noverify    --	if true then server won't perform data verification.
// Returns:
//	Total test execution time.
//
int64_t
client_impl::test_objinhsimple(
    hatypetest::server_ptr server_ref,
    int32_t num_repeat,
    bool noverify,
    Environment &)
{
	// os::printf("%s test_objinhsimple()\n", msg_prefix());
	const uint32_t	timeout = 30;
	os::hrtime_t	total_time = 0, start_time;
	bool		failed = false;

	if (noverify) {
		Environment	e3;

		server_ref->set_noverify(e3);
		if (! hatypes_env_ok(e3, msg_prefix())) {
			return (-1);
		}
	}

	// Test local narrow first.
	if (! local_narrow_test()) {
		failed = true;
	}

	for (; !failed && num_repeat > 0; --num_repeat) {
		Environment		e2;

		// NOTE: The following is NOT the recommended way to allocate
		// object implementations.  Object impls should be allocated
		// in the heap instead of on the stack.  We're doing it this
		// way only for convenience and because we don't do
		// "delete this" in their _unreferenced() methods.
		InhObjB_impl		in_impl(hatypes_in_obj_id);
		InhObjA_impl		inout_impl(hatypes_inout_obj_id);

		hatypetest::InhObjB_ptr	in;
		hatypetest::InhObjA_ptr	inout;
		hatypetest::InhObjA_ptr	out = hatypetest::InhObjA::_nil();
		hatypetest::InhObjA_ptr	ret = hatypetest::InhObjA::_nil();

		// Prepare object references to be passed to server.
		in = in_impl.get_objref();
		inout = inout_impl.get_objref();

		// Invoke server.
		start_time = os::gethrtime();
		ret = server_ref->test_objinhsimple(in, out, inout, e2);
		total_time += os::gethrtime() - start_time;
		if (! hatypes_env_ok(e2, msg_prefix())) {
			failed = true;
		} else {
			CORBA::Object_var	objv;

			// Verify object references from server.
			if (! verify(out, hatypes_inout_obj_id, "'out'",
				msg_prefix(), 'A')) {

				failed = true;
			}
			if (! verify(inout, hatypes_in_obj_id, "'inout'",
				msg_prefix(), 'B')) {

				failed = true;
			}
			if (! verify(ret, hatypes_in_obj_id, "returned",
				msg_prefix(), 'B')) {

				failed = true;
			}

			// Do some more narrow checking for the out object
			// which is a remote object
			//
			// NOTE: _narrow() increments an object's reference
			// count but we don't need to do CORBA::release(objv)
			// below since objv is of type CORBA::Object_var, which
			// calls CORBA::release() everytime it's assigned a new
			// reference and when it goes out of scope.
			//
			objv = hatypetest::InhObjB::_narrow(out);
			if (is_not_nil(objv)) {
				os::printf("FAIL: Remote parent object "
				    "narrowed to child class (InhObjA --> "
				    "InhObjB)\n");
				failed = true;
			}

			objv = hatypetest::InhObjC::_narrow(out);
			if (is_not_nil(objv)) {
				os::printf("FAIL: Remote parent object "
				    "narrowed to child class (InhObjA --> "
				    "InhObjC)\n");
				failed = true;
			}

			objv = hatypetest::Obj::_narrow(out);
			if (is_not_nil(objv)) {
				os::printf("FAIL: Remote object narrowed to "
				    "unrelated class (InhObjA --> Obj)\n");
				failed = true;
			}
		}

		CORBA::release(in);
		CORBA::release(inout);
		CORBA::release(out);
		CORBA::release(ret);

		// Verify that _unreferenced() is (or will be) called.
		if (! in_impl.wait_until_unreferenced(timeout)) {
			failed = true;
		}
		if (! inout_impl.wait_until_unreferenced(timeout)) {
			failed = true;
		}
	}

	return (failed ? -1 : total_time);
}

//
// Test server interface.  Invoked by the test client.
//
#if defined(_KERNEL) || defined(_KERNEL_ORB)
hatypetest::InhObjA_ptr
server_impl::test_objinhsimple(
    hatypetest::InhObjA_ptr  in,
    hatypetest::InhObjA_out  out,
    hatypetest::InhObjA_ptr &inout,
    Environment &)
{
	// os::printf("%s test_objinhsimple()\n", msg_prefix());

	if (!verify_off) {
		// Verify object references from client.
		verify(in, hatypes_in_obj_id, "'in'", msg_prefix(), 'B');
		verify(inout, hatypes_inout_obj_id, "'inout'",
			msg_prefix(), 'A');
	}

	// Prepare object references for client.
	out = hatypetest::InhObjA::_duplicate(inout);	// copy inout to out

	CORBA::release(inout);
	inout = hatypetest::InhObjA::_duplicate(in);	// copy in into inout

	return (hatypetest::InhObjA::_duplicate(in));	// return in object
}
#endif

//
// Verifies that test object reference "objp" has the expected ID
// "expected_id".
//
bool
verify(hatypetest::InhObjA_ptr objp, int32_t expected_id, const char *paramtype,
    const char *msgpfx, char expected_name)
{
	int32_t		idnum;
	char		obj_name;
	Environment	e, env;

	if (CORBA::is_nil(objp)) {
		os::printf("FAIL: %s Simple inherited object reference %s "
		    "is NIL\n", paramtype, msgpfx);
		return (false);
	}

	// Check if the id is right.
	idnum = objp->id(e);
	if (e.exception()) {
		os::printf("FAIL: %s Simple inherited object reference had "
		    "exception in id()\n", msgpfx);
		e.clear();
		return (false);
	}
	if (idnum != expected_id) {
		os::printf("FAIL: %s %s Simple inherited object reference:\n",
		    paramtype, msgpfx);
		os::printf("\texpected id = %d, actual id = %d\n",
		    expected_id, idnum);
		return (false);
	}

	// Check if the name is as expected.
	obj_name = objp->name(env);
	if (env.exception()) {
		os::printf("FAIL: %s Simple inherited object reference had "
		    "exception in name()\n", msgpfx);
		env.clear();
		return (false);
	}
	if (obj_name != expected_name) {
		os::printf("FAIL %s %s Simple inherited object reference:\n",
		    paramtype, msgpfx);
		os::printf("\texpected name = %c, actual name = %c\n",
		    expected_name, obj_name);
		return (false);
	}

	return (true);
}
