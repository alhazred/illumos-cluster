/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_vunion.cc	1.6	08/05/20 SMI"

//
// Variable union test
//

#include <orbtest/hatypes/client_impl.h>

#if defined(_KERNEL) || defined(_KERNEL_ORB)
#include <orbtest/hatypes/server_impl.h>
#endif

static bool	set_in(hatypetest::Vunion &var, hatypetest::VDiscrim discrim,
			uint32_t len, hatypetest::Obj_ptr objref,
			const char *msgpfx, bool set = true);
static bool	set_out(hatypetest::Vunion &var, hatypetest::VDiscrim discrim,
			uint32_t len, hatypetest::Obj_ptr objref,
			const char *msgpfx, bool set = true);
static bool	set_io_i(hatypetest::Vunion &var, hatypetest::VDiscrim discrim,
			uint32_t len, hatypetest::Obj_ptr objref,
			const char *msgpfx, bool set = true);
static bool	set_io_o(hatypetest::Vunion &var, hatypetest::VDiscrim discrim,
			uint32_t len, hatypetest::Obj_ptr objref,
			const char *msgpfx, bool set = true);
static bool	set_ret(hatypetest::Vunion &var, hatypetest::VDiscrim discrim,
			uint32_t len, hatypetest::Obj_ptr objref,
			const char *msgpfx, bool set = true);

inline bool
verify_in(const hatypetest::Vunion &v, hatypetest::VDiscrim d, uint32_t l,
		hatypetest::Obj_ptr o, const char *p)
{
	return (set_in((hatypetest::Vunion &)v, d, l, o, p, false));
}

inline bool
verify_out(const hatypetest::Vunion &v, hatypetest::VDiscrim d, uint32_t l,
		hatypetest::Obj_ptr o, const char *p)
{
	return (set_out((hatypetest::Vunion &)v, d, l, o, p, false));
}

inline bool
verify_io_i(const hatypetest::Vunion &v, hatypetest::VDiscrim d, uint32_t l,
		hatypetest::Obj_ptr o, const char *p)
{
	return (set_io_i((hatypetest::Vunion &)v, d, l, o, p, false));
}

inline bool
verify_io_o(const hatypetest::Vunion &v, hatypetest::VDiscrim d, uint32_t l,
		hatypetest::Obj_ptr o, const char *p)
{
	return (set_io_o((hatypetest::Vunion &)v, d, l, o, p, false));
}

inline bool
verify_ret(const hatypetest::Vunion &v, hatypetest::VDiscrim d, uint32_t l,
		hatypetest::Obj_ptr o, const char *p)
{
	return (set_ret((hatypetest::Vunion &)v, d, l, o, p, false));
}


//
// Test client interface.  Invoked by driver "programs" and, in turn,
// invokes the corresponding method on the test server side.
// Parameters:
//	server_ref  --	test server object to invoke.
//	num_repeat  --	how many times to run the test.
//	len	    --	length of the union's sequence member to test.
//	noverify    --	if true then server won't perform data verification.
// Returns:
//	Total test execution time.
//
int64_t
client_impl::test_vunion(
		hatypetest::server_ptr server_ref,
		int32_t num_repeat,
		uint32_t len,
		bool noverify,
		Environment &)
{
	// os::printf("%s test_vunion()\n", msg_prefix());
	os::hrtime_t	total_time = 0, start_time;

	if (noverify) {
		Environment	e3;

		server_ref->set_noverify(e3);
		if (! hatypes_env_ok(e3, msg_prefix())) {
			return (-1);
		}
	}

	for (; num_repeat > 0; --num_repeat) {
		// For each possible union discriminant value
		for (int i = hatypetest::v_1byte; i < hatypetest::v_end; ++i) {
			Environment		e2;
			hatypetest::VDiscrim discrim = (hatypetest::VDiscrim) i;
			Obj_impl		obj_impl(hatypes_in_obj_id);
			hatypetest::Obj_ptr	objref = obj_impl.get_objref();

			//
			// Do test in a code block so we can verify that
			// object reference members of the Vunions's below are
			// released when the unions are destroyed.
			//
			{
				hatypetest::Vunion	in, inout;
				hatypetest::Vunion_var	outp, retp;

				// Prepare data to be passed to server.
				(void) set_in(in, discrim, len, objref,
						msg_prefix());
				(void) set_io_i(inout, discrim, len, objref,
						msg_prefix());

				// Invoke server.
				start_time = os::gethrtime();
				retp = server_ref->test_vunion(in, outp, inout,
						discrim, len, objref, e2);
				if (! hatypes_env_ok(e2, msg_prefix())) {
					total_time = -1;
					i = hatypetest::v_end; // end inner loop
					num_repeat = 0;	// end outer loop
				} else {
					total_time +=
						os::gethrtime() - start_time;

					// Verify values returned by server.
					(void) verify_out(*outp, discrim, len,
							objref, msg_prefix());
					(void) verify_io_o(inout, discrim, len,
							objref, msg_prefix());
					(void) verify_ret(*retp, discrim, len,
							objref, msg_prefix());
				}
			}

			// Verify that _unreferenced() is (or will be) called.
			CORBA::release(objref);
			(void) verify_unreferenced(&obj_impl, msg_prefix());
		}
	}

	return (total_time);
}

//
// Test server interface.  Invoked by the test client.
//
#if defined(_KERNEL) || defined(_KERNEL_ORB)
hatypetest::Vunion *
server_impl::test_vunion(
		const hatypetest::Vunion &in,
		hatypetest::Vunion_out outp,
		hatypetest::Vunion &inout,
		hatypetest::VDiscrim discrim,
		uint32_t len,
		hatypetest::Obj_ptr objref,
		Environment &)
{
	// os::printf("%s test_vunion()\n", msg_prefix());
	hatypetest::Vunion	*retp;		// don't use _var!

	if (!verify_off) {
		// Verify values passed by client.
		verify_in(in, discrim, len, objref, msg_prefix());
		verify_io_i(inout, discrim, len, objref, msg_prefix());
	}

	// Prepare values to be returned to client.
	outp = new hatypetest::Vunion;
	set_out(*outp, discrim, len, objref, msg_prefix());

	set_io_o(inout, discrim, len, objref, msg_prefix());

	retp = new hatypetest::Vunion;
	set_ret(*retp, discrim, len, objref, msg_prefix());

	return (retp);
}
#endif

//
// Sets up (or verifies if "set" argument is false) 'in' parameter.
//
bool
set_in(hatypetest::Vunion &var, hatypetest::VDiscrim discrim, uint32_t len,
		hatypetest::Obj_ptr objref, const char *msgpfx, bool set)
{
	const uint8_t	base_1byte = 0xF7;
	const uint16_t	base_2bytes = 0xeffe;
	const uint32_t	base_4bytes = 0xfedcba98;
	const uint64_t	base_8bytes = 0xfedcba9876543210;
	char		cbase = 1;
	uint32_t	i;
	bool		failed = false;

	if (set) {
		switch (discrim) {
		case hatypetest::v_1byte:
			var.m_1byte(base_1byte);
			break;

		case hatypetest::v_2bytes:
			var.m_2bytes(base_2bytes);
			break;

		case hatypetest::v_4bytes:
			var.m_4bytes(base_4bytes);
			break;

		case hatypetest::v_8bytes:
			var.m_8bytes(base_8bytes);
			break;

		case hatypetest::v_obj:
			var.m_obj(hatypetest::Obj::_duplicate(objref));
			break;

		default:
			var._d(discrim);
			var.m_str(new char[len + 1]);
			for (i = 0; i < len; ++i) {
				if (cbase == 0)
					cbase = 1;
				var.m_str()[i] = cbase;
				cbase <<= 1;
			}
			var.m_str()[i] = '\0';
			break;
		}
		return (true);
	}

	// Verify data.
	if (var._d() != discrim) {
		os::printf("FAIL: %s discriminant of 'in' "
			"variable union:\n", msgpfx);
		os::printf("\texpected = %d, actual = %d\n", discrim, var._d());
		return (false);
	}

	switch (var._d()) {
	case hatypetest::v_1byte:
		if (var.m_1byte() != base_1byte) {
			os::printf("FAIL: %s 1-byte member of 'in' "
				"variable union:\n", msgpfx);
			os::printf("\texpected = 0x%x, actual = 0x%x\n",
				base_1byte, var.m_1byte());
			failed = true;
		}
		break;

	case hatypetest::v_2bytes:
		if (var.m_2bytes() != base_2bytes) {
			os::printf("FAIL: %s 2-byte member of 'in' "
				"variable union:\n", msgpfx);
			os::printf("\texpected = 0x%hx, actual = 0x%hx\n",
				base_2bytes, var.m_2bytes());
			failed = true;
		}
		break;

	case hatypetest::v_4bytes:
		if (var.m_4bytes() != base_4bytes) {
			os::printf("FAIL: %s 4-byte member of 'in' "
				"variable union:\n", msgpfx);
			os::printf("\texpected = 0x%lx, actual = 0x%lx\n",
				base_4bytes, var.m_4bytes());
			failed = true;
		}
		break;

	case hatypetest::v_8bytes:
		if (var.m_8bytes() != base_8bytes) {
			os::printf("FAIL: %s 8-byte member of 'in' "
				"variable union:\n", msgpfx);
			os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
				base_8bytes, var.m_8bytes());
			failed = true;
		}
		break;

	case hatypetest::v_obj:
		if (CORBA::is_nil(var.m_obj())) {
			os::printf("FAIL: %s object reference member of "
				"'in' variable union is NIL\n", msgpfx);
			failed = true;
		} else {
			int32_t		act_id, exp_id;
			Environment	e;

			act_id = var.m_obj()->id(e);
			if (e.exception()) {
				os::printf("FAIL: %s object reference member of"
				    " 'in' variable union. exception in id()\n",
				    msgpfx);
				e.clear();
				failed = true;
			}
			exp_id = objref->id(e);
			if (e.exception()) {
				os::printf("FAIL: %s object reference had "
				    "exception in id()\n", msgpfx);
				e.clear();
				failed = true;
			}
			if (act_id != exp_id) {
				os::printf("FAIL: %s object reference member "
					"of 'in' variable union:\n",
					msgpfx);
				os::printf("\texpected id = %d, "
					"actual id = %d\n", exp_id, act_id);
				failed = true;
			}
		}
		break;

	default:
		for (i = 0; i < len; ++i) {
			if (cbase == 0)
				cbase = 1;
			if (var.m_str()[i] == cbase) {
				cbase <<= 1;
				continue;
			}

			os::printf("FAIL: %s char at index %d of string "
				"member of 'in' variable union:\n",
				msgpfx, i);
			os::printf("\texpected = 0x%x, actual = 0x%x\n",
				cbase, var.m_str()[i]);
			failed = true;
			break;
		}
		if (var.m_str()[i] != '\0') {
			os::printf("FAIL: %s string member of 'in' "
				"variable union not terminated at index %d:\n",
				msgpfx, i);
			failed = true;
		}
		break;
	}

	return (!failed);
}

//
// Sets up (or verifies if "set" argument is false) 'out' parameter.
//
bool
set_out(hatypetest::Vunion &var, hatypetest::VDiscrim discrim, uint32_t len,
		hatypetest::Obj_ptr objref, const char *msgpfx, bool set)
{
	const uint8_t		base_1byte = 0x3F;
	const uint16_t		base_2bytes = 0x6666;
	const uint32_t		base_4bytes = 0x12345678;
	const uint64_t		base_8bytes = 0xca8642013579bdf;
	hatypetest::VDiscrim	base_discrim =
					(hatypetest::VDiscrim) (discrim + 1);
	char			cbase = ~0;
	uint32_t		i;
	bool			failed = false;

	if (set) {
		switch (base_discrim) {
		case hatypetest::v_1byte:
			var.m_1byte(base_1byte);
			break;

		case hatypetest::v_2bytes:
			var.m_2bytes(base_2bytes);
			break;

		case hatypetest::v_4bytes:
			var.m_4bytes(base_4bytes);
			break;

		case hatypetest::v_8bytes:
			var.m_8bytes(base_8bytes);
			break;

		case hatypetest::v_obj:
			var.m_obj(hatypetest::Obj::_duplicate(objref));
			break;

		default:
			var._d(base_discrim);
			var.m_str(new char[len + 2]);
			for (i = 0; i < len + 1; ++i) {
				if (cbase == 0)
					cbase = ~0;
				var.m_str()[i] = cbase;
				cbase >>= 1;
			}
			var.m_str()[i] = '\0';
			break;
		}
		return (true);
	}

	// Verify data.
	if (var._d() != base_discrim) {
		os::printf("FAIL: %s discriminant of 'out' "
			"variable union:\n", msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			base_discrim, var._d());
		return (false);
	}

	switch (var._d()) {
	case hatypetest::v_1byte:
		if (var.m_1byte() != base_1byte) {
			os::printf("FAIL: %s 1-byte member of 'out' "
				"variable union:\n", msgpfx);
			os::printf("\texpected = 0x%x, actual = 0x%x\n",
				base_1byte, var.m_1byte());
			failed = true;
		}
		break;

	case hatypetest::v_2bytes:
		if (var.m_2bytes() != base_2bytes) {
			os::printf("FAIL: %s 2-byte member of 'out' "
				"variable union:\n", msgpfx);
			os::printf("\texpected = 0x%hx, actual = 0x%hx\n",
				base_2bytes, var.m_2bytes());
			failed = true;
		}
		break;

	case hatypetest::v_4bytes:
		if (var.m_4bytes() != base_4bytes) {
			os::printf("FAIL: %s 4-byte member of 'out' "
				"variable union:\n", msgpfx);
			os::printf("\texpected = 0x%lx, actual = 0x%lx\n",
				base_4bytes, var.m_4bytes());
			failed = true;
		}
		break;

	case hatypetest::v_8bytes:
		if (var.m_8bytes() != base_8bytes) {
			os::printf("FAIL: %s 8-byte member of 'out' "
				"variable union:\n", msgpfx);
			os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
				base_8bytes, var.m_8bytes());
			failed = true;
		}
		break;

	case hatypetest::v_obj:
		if (CORBA::is_nil(var.m_obj())) {
			os::printf("FAIL: %s object reference member of "
				"'out' variable union is NIL\n", msgpfx);
			failed = true;
		} else {
			int32_t		act_id, exp_id;
			Environment	e;

			act_id = var.m_obj()->id(e);
			if (e.exception()) {
				os::printf("FAIL: %s object reference member of"
				    " 'out' variable union exception in id()\n",
				    msgpfx);
				e.clear();
				failed = true;
			}
			exp_id = objref->id(e);
			if (e.exception()) {
				os::printf("FAIL: %s object reference had "
				    "exception in id()\n", msgpfx);
				e.clear();
				failed = true;
			}
			if (act_id != exp_id) {
				os::printf("FAIL: %s object reference member "
					"of 'out' variable union:\n",
					msgpfx);
				os::printf("\texpected id = %d, "
					"actual id = %d\n", exp_id, act_id);
				failed = true;
			}
		}
		break;

	default:
		for (i = 0; i < len + 1; ++i) {
			if (cbase == 0)
				cbase = ~0;
			if (var.m_str()[i] == cbase) {
				cbase >>= 1;
				continue;
			}

			os::printf("FAIL: %s char at index %d of string "
				"member of 'out' variable union:\n",
				msgpfx, i);
			os::printf("\texpected = 0x%x, actual = 0x%x\n",
				cbase, var.m_str()[i]);
			failed = true;
			break;
		}
		if (var.m_str()[i] != '\0') {
			os::printf("FAIL: %s string member of 'out' "
				"variable union not terminated at index %d:\n",
				msgpfx, i);
			failed = true;
		}
		break;
	}

	return (!failed);
}

//
// Sets up (or verifies if "set" argument is false) incoming 'inout' parameter.
//
bool
set_io_i(hatypetest::Vunion &var, hatypetest::VDiscrim discrim, uint32_t len,
		hatypetest::Obj_ptr objref, const char *msgpfx, bool set)
{
	const uint8_t	base_1byte = 0xF3;
	const uint16_t	base_2bytes = 0x4444;
	const uint32_t	base_4bytes = 0x98765432;
	const uint64_t	base_8bytes = 0xfdb9753102468ac;
	char		cbase = ~0;
	uint32_t	i;
	bool		failed = false;

	if (set) {
		switch (discrim) {
		case hatypetest::v_1byte:
			var.m_1byte(base_1byte);
			break;

		case hatypetest::v_2bytes:
			var.m_2bytes(base_2bytes);
			break;

		case hatypetest::v_4bytes:
			var.m_4bytes(base_4bytes);
			break;

		case hatypetest::v_8bytes:
			var.m_8bytes(base_8bytes);
			break;

		case hatypetest::v_obj:
			var.m_obj(hatypetest::Obj::_duplicate(objref));
			break;

		default:
			var._d(discrim);
			var.m_str(new char[len + 1]);
			for (i = 0; i < len; ++i) {
				if (cbase == 0)
					cbase = ~0;
				var.m_str()[i] = cbase;
				cbase <<= 1;
			}
			var.m_str()[i] = '\0';
			break;
		}
		return (true);
	}

	// Verify data.
	if (var._d() != discrim) {
		os::printf("FAIL: %s discriminant of 'inout' "
			"variable union:\n", msgpfx);
		os::printf("\texpected = %d, actual = %d\n", discrim, var._d());
		return (false);
	}

	switch (var._d()) {
	case hatypetest::v_1byte:
		if (var.m_1byte() != base_1byte) {
			os::printf("FAIL: %s 1-byte member of 'inout' "
				"variable union:\n", msgpfx);
			os::printf("\texpected = 0x%x, actual = 0x%x\n",
				base_1byte, var.m_1byte());
			failed = true;
		}
		break;

	case hatypetest::v_2bytes:
		if (var.m_2bytes() != base_2bytes) {
			os::printf("FAIL: %s 2-byte member of 'inout' "
				"variable union:\n", msgpfx);
			os::printf("\texpected = 0x%hx, actual = 0x%hx\n",
				base_2bytes, var.m_2bytes());
			failed = true;
		}
		break;

	case hatypetest::v_4bytes:
		if (var.m_4bytes() != base_4bytes) {
			os::printf("FAIL: %s 4-byte member of 'inout' "
				"variable union:\n", msgpfx);
			os::printf("\texpected = 0x%lx, actual = 0x%lx\n",
				base_4bytes, var.m_4bytes());
			failed = true;
		}
		break;

	case hatypetest::v_8bytes:
		if (var.m_8bytes() != base_8bytes) {
			os::printf("FAIL: %s 8-byte member of 'inout' "
				"variable union:\n", msgpfx);
			os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
				base_8bytes, var.m_8bytes());
			failed = true;
		}
		break;

	case hatypetest::v_obj:
		if (CORBA::is_nil(var.m_obj())) {
			os::printf("FAIL: %s object reference member of "
				"'inout' variable union is NIL\n", msgpfx);
			failed = true;
		} else {
			int32_t		act_id, exp_id;
			Environment	e;

			act_id = var.m_obj()->id(e);
			if (e.exception()) {
				os::printf("FAIL: %s object reference member of"
				    " 'inout' variable union exception in id\n",
				    msgpfx);
				e.clear();
				failed = true;
			}
			exp_id = objref->id(e);
			if (e.exception()) {
				os::printf("FAIL: %s object reference had "
				    "exception in id()\n", msgpfx);
				e.clear();
				failed = true;
			}
			if (act_id != exp_id) {
				os::printf("FAIL: %s object reference member "
					"of 'inout' variable union:\n",
					msgpfx);
				os::printf("\texpected id = %d, "
					"actual id = %d\n", exp_id, act_id);
				failed = true;
			}
		}
		break;

	default:
		for (i = 0; i < len; ++i) {
			if (cbase == 0) cbase = ~0;
			if (var.m_str()[i] == cbase) {
				cbase <<= 1;
				continue;
			}

			os::printf("FAIL: %s char at index %d of string "
				"member of 'inout' variable union:\n",
				msgpfx, i);
			os::printf("\texpected = 0x%x, actual = 0x%x\n",
				cbase, var.m_str()[i]);
			failed = true;
			break;
		}
		if (var.m_str()[i] != '\0') {
			os::printf("FAIL: %s string member of 'inout' "
				"variable union not terminated at index %d:\n",
				msgpfx, i);
			failed = true;
		}
		break;
	}

	return (!failed);
}

//
// Sets up (or verifies if "set" argument is false) outgoing 'inout' parameter.
//
bool
set_io_o(hatypetest::Vunion &var, hatypetest::VDiscrim discrim, uint32_t len,
		hatypetest::Obj_ptr objref, const char *msgpfx, bool set)
{
	const uint8_t		base_1byte = 0x7F;
	const uint16_t		base_2bytes = 0xfeef;
	const uint32_t		base_4bytes = 0x89abcdef;
	const uint64_t		base_8bytes = 0x0123456789abcdef;
	hatypetest::VDiscrim	base_discrim =
					(hatypetest::VDiscrim) (discrim - 1);
	char			cbase = 1;
	uint32_t		i;
	bool			failed = false;

	if (set) {
		switch (base_discrim) {
		case hatypetest::v_1byte:
			var.m_1byte(base_1byte);
			break;

		case hatypetest::v_2bytes:
			var.m_2bytes(base_2bytes);
			break;

		case hatypetest::v_4bytes:
			var.m_4bytes(base_4bytes);
			break;

		case hatypetest::v_8bytes:
			var.m_8bytes(base_8bytes);
			break;

		case hatypetest::v_obj:
			var.m_obj(hatypetest::Obj::_duplicate(objref));
			break;

		default:
			var._d(base_discrim);
			var.m_str(new char[len + 3]);
			for (i = 0; i < len + 2; ++i) {
				if (cbase == 0)
					cbase = ~0;
				var.m_str()[i] = cbase;
				cbase >>= 1;
			}
			var.m_str()[i] = '\0';
			break;
		}
		return (true);
	}

	// Verify data.
	if (var._d() != base_discrim) {
		os::printf("FAIL: %s discriminant of 'inout' "
			"variable union:\n", msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			base_discrim, var._d());
		return (false);
	}

	switch (var._d()) {
	case hatypetest::v_1byte:
		if (var.m_1byte() != base_1byte) {
			os::printf("FAIL: %s 1-byte member of 'inout' "
				"variable union:\n", msgpfx);
			os::printf("\texpected = 0x%x, actual = 0x%x\n",
				base_1byte, var.m_1byte());
			failed = true;
		}
		break;

	case hatypetest::v_2bytes:
		if (var.m_2bytes() != base_2bytes) {
			os::printf("FAIL: %s 2-byte member of 'inout' "
				"variable union:\n", msgpfx);
			os::printf("\texpected = 0x%hx, actual = 0x%hx\n",
				base_2bytes, var.m_2bytes());
			failed = true;
		}
		break;

	case hatypetest::v_4bytes:
		if (var.m_4bytes() != base_4bytes) {
			os::printf("FAIL: %s 4-byte member of 'inout' "
				"variable union:\n", msgpfx);
			os::printf("\texpected = 0x%lx, actual = 0x%lx\n",
				base_4bytes, var.m_4bytes());
			failed = true;
		}
		break;

	case hatypetest::v_8bytes:
		if (var.m_8bytes() != base_8bytes) {
			os::printf("FAIL: %s 8-byte member of 'inout' "
				"variable union:\n", msgpfx);
			os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
				base_8bytes, var.m_8bytes());
			failed = true;
		}
		break;

	case hatypetest::v_obj:
		if (CORBA::is_nil(var.m_obj())) {
			os::printf("FAIL: %s object reference member of "
				"'inout' variable union is NIL\n", msgpfx);
			failed = true;
		} else {
			int32_t		act_id, exp_id;
			Environment	e;

			act_id = var.m_obj()->id(e);
			if (e.exception()) {
				os::printf("FAIL: %s object reference member of"
				    " 'inout' variable union exception in id\n",
				    msgpfx);
				e.clear();
				failed = true;
			}
			exp_id = objref->id(e);
			if (e.exception()) {
				os::printf("FAIL: %s object reference had "
				    "exception in id()\n", msgpfx);
				e.clear();
				failed = true;
			}
			if (act_id != exp_id) {
				os::printf("FAIL: %s object reference member "
					"of 'inout' variable union:\n",
					msgpfx);
				os::printf("\texpected id = %d, "
					"actual id = %d\n", exp_id, act_id);
				failed = true;
			}
		}
		break;

	default:
		for (i = 0; i < len + 2; ++i) {
			if (cbase == 0)
				cbase = ~0;
			if (var.m_str()[i] == cbase) {
				cbase >>= 1;
				continue;
			}

			os::printf("FAIL: %s char at index %d of string "
				"member of 'inout' variable union:\n",
				msgpfx, i);
			os::printf("\texpected = 0x%x, actual = 0x%x\n",
				cbase, var.m_str()[i]);
			failed = true;
			break;
		}
		if (var.m_str()[i] != '\0') {
			os::printf("FAIL: %s string member of 'inout' "
				"variable union not terminated at index %d:\n",
				msgpfx, i);
			failed = true;
		}
		break;
	}

	return (!failed);
}


//
// Sets up (or verifies if "set" argument is false) returned value.
//
bool
set_ret(hatypetest::Vunion &var, hatypetest::VDiscrim discrim, uint32_t len,
		hatypetest::Obj_ptr objref, const char *msgpfx, bool set)
{
	const uint8_t	base_1byte = 0xAA;
	const uint16_t	base_2bytes = 0xfedc;
	const uint32_t	base_4bytes = 0x96f6e7a3;
	const uint64_t	base_8bytes = 0xffffffffffffff00;
	unsigned char		cbase = 1;
	uint32_t	i;
	bool		failed = false;

	if (set) {
		switch (discrim) {
		case hatypetest::v_1byte:
			var.m_1byte(base_1byte);
			break;

		case hatypetest::v_2bytes:
			var.m_2bytes(base_2bytes);
			break;

		case hatypetest::v_4bytes:
			var.m_4bytes(base_4bytes);
			break;

		case hatypetest::v_8bytes:
			var.m_8bytes(base_8bytes);
			break;

		case hatypetest::v_obj:
			var.m_obj(hatypetest::Obj::_duplicate(objref));
			break;

		default:
			var._d(discrim);
			var.m_str(new char[len + 1]);
			for (i = 0; i < len; ++i) {
				var.m_str()[i] = cbase;
				cbase = ~cbase;
			}
			var.m_str()[i] = '\0';
			break;
		}
		return (true);
	}

	// Verify data.
	if (var._d() != discrim) {
		os::printf("FAIL: %s discriminant of returned "
			"variable union:\n", msgpfx);
		os::printf("\texpected = %d, actual = %d\n", discrim, var._d());
		return (false);
	}

	switch (var._d()) {
	case hatypetest::v_1byte:
		if (var.m_1byte() != base_1byte) {
			os::printf("FAIL: %s 1-byte member of returned "
				"variable union:\n", msgpfx);
			os::printf("\texpected = 0x%x, actual = 0x%x\n",
				base_1byte, var.m_1byte());
			failed = true;
		}
		break;

	case hatypetest::v_2bytes:
		if (var.m_2bytes() != base_2bytes) {
			os::printf("FAIL: %s 2-byte member of returned "
				"variable union:\n", msgpfx);
			os::printf("\texpected = 0x%hx, actual = 0x%hx\n",
				base_2bytes, var.m_2bytes());
			failed = true;
		}
		break;

	case hatypetest::v_4bytes:
		if (var.m_4bytes() != base_4bytes) {
			os::printf("FAIL: %s 4-byte member of returned "
				"variable union:\n", msgpfx);
			os::printf("\texpected = 0x%lx, actual = 0x%lx\n",
				base_4bytes, var.m_4bytes());
			failed = true;
		}
		break;

	case hatypetest::v_8bytes:
		if (var.m_8bytes() != base_8bytes) {
			os::printf("FAIL: %s 8-byte member of returned "
				"variable union:\n", msgpfx);
			os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
				base_8bytes, var.m_8bytes());
			failed = true;
		}
		break;

	case hatypetest::v_obj:
		if (CORBA::is_nil(var.m_obj())) {
			os::printf("FAIL: %s object reference member of "
				"returned variable union is NIL\n", msgpfx);
			failed = true;
		} else {
			int32_t		act_id, exp_id;
			Environment	e;

			act_id = var.m_obj()->id(e);
			if (e.exception()) {
				os::printf("FAIL: %s object reference member of"
				    " return variable union exception in id\n",
				    msgpfx);
				e.clear();
				failed = true;
			}
			exp_id = objref->id(e);
			if (e.exception()) {
				os::printf("FAIL: %s object reference had "
				    "exception in id()\n", msgpfx);
				e.clear();
				failed = true;
			}
			if (act_id != exp_id) {
				os::printf("FAIL: %s object reference member "
					"of returned variable union:\n",
					msgpfx);
				os::printf("\texpected id = %d, "
					"actual id = %d\n", exp_id, act_id);
				failed = true;
			}
		}
		break;

	default:
		for (i = 0; i < len; ++i) {
			if (var.m_str()[i] == cbase) {
				cbase = ~cbase;
				continue;
			}

			os::printf("FAIL: %s char at index %d of string "
				"member of returned variable union:\n",
				msgpfx, i);
			os::printf("\texpected = 0x%x, actual = 0x%x\n",
				cbase, var.m_str()[i]);
			failed = true;
			break;
		}
		if (var.m_str()[i] != '\0') {
			os::printf("FAIL: %s string member of returned "
				"variable union not terminated at index %d:\n",
				msgpfx, i);
			failed = true;
		}
		break;
	}

	return (!failed);
}
