/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_longlong.cc	1.6	08/05/20 SMI"

//
// LongLong (int64_t) test
//

#include <orbtest/hatypes/client_impl.h>

#if defined(_KERNEL) || defined(_KERNEL_ORB)
#include <orbtest/hatypes/server_impl.h>
#endif


static bool	set_in(int64_t &var, const char *msgpfx,
			bool set = true);
static bool	set_out(int64_t &var, const char *msgpfx,
			bool set = true);
static bool	set_io_i(int64_t &var, const char *msgpfx,
			bool set = true);
static bool	set_io_o(int64_t &var, const char *msgpfx,
			bool set = true);
static bool	set_ret(int64_t &var, const char *msgpfx,
			bool set = true);

inline bool
verify_in(const int64_t &v, const char *p)
{
	return (set_in((int64_t &)v, p, false));
}

inline bool
verify_out(const int64_t &v, const char *p)
{
	return (set_out((int64_t &)v, p, false));
}

inline bool
verify_io_i(const int64_t &v, const char *p)
{
	return (set_io_i((int64_t &)v, p, false));
}

inline bool
verify_io_o(const int64_t &v, const char *p)
{
	return (set_io_o((int64_t &)v, p, false));
}

inline bool
verify_ret(const int64_t &v, const char *p)
{
	return (set_ret((int64_t &)v, p, false));
}


//
// Test client interface.  Invoked by driver "programs" and, in turn,
// invokes the corresponding method on the test server side.
// Parameters:
//	server_ref  --	test server object to invoke.
//	num_repeat  --	how many times to run the test.
//	noverify    --	if true then server won't perform data verification.
// Returns:
//	Total test execution time.
//
int64_t
client_impl::test_longlong(
		hatypetest::server_ptr server_ref,
		int32_t num_repeat,
		bool noverify,
		Environment &)
{
	// os::printf("%s test_longlong()\n", msg_prefix());
	os::hrtime_t	total_time = 0, start_time;

	if (noverify) {
		Environment	e2;

		server_ref->set_noverify(e2);
		if (! hatypes_env_ok(e2, msg_prefix())) {
			return (-1);
		}
	}

	for (; num_repeat > 0; --num_repeat) {
		Environment	e2;
		int64_t		in, out, inout, ret;

		// Prepare data to be passed to server.
		(void) set_in(in, msg_prefix());
		(void) set_io_i(inout, msg_prefix());

		// Invoke server.
		start_time = os::gethrtime();
		ret = server_ref->test_longlong(in, out, inout, e2);
		if (! hatypes_env_ok(e2, msg_prefix())) {
			total_time = -1;
			break;
		}
		total_time += os::gethrtime() - start_time;

		// Verify values returned by server.
		(void) verify_out(out, msg_prefix());
		(void) verify_io_o(inout, msg_prefix());
		(void) verify_ret(ret, msg_prefix());
	}

	return (total_time);
}


//
// Test server interface.  Invoked by the test client.
//
#if defined(_KERNEL) || defined(_KERNEL_ORB)
int64_t
server_impl::test_longlong(
		int64_t  in,
		int64_t &out,
		int64_t &inout,
		Environment &)
{
	// os::printf("%s test_longlong()\n", msg_prefix());
	int64_t	ret;

	// Verify values passed by client.
	if (!verify_off) {
		verify_in(in, msg_prefix());
		verify_io_i(inout, msg_prefix());
	}

	// Prepare values to be returned to client.
	set_out(out, msg_prefix());
	set_io_o(inout, msg_prefix());
	set_ret(ret, msg_prefix());

	return (ret);
}
#endif


//
// Sets up (or verifies if "set" argument is false) 'in' parameter.
//
bool
set_in(int64_t &var, const char *msgpfx, bool set)
{
	const int64_t	base = 0xfedcba9876543210;

	if (set) {
		var = base;
		return (true);
	}

	// Verify data.
	if (var == base)
		return (true);

	os::printf("FAIL: %s LongLong (int64_t) 'in' value:\n", msgpfx);
	os::printf("\texpected = 0x%llx, actual = 0x%llx\n", base, var);
	return (false);
}


//
// Sets up (or verifies if "set" argument is false) 'out' parameter.
//
bool
set_out(int64_t &var, const char *msgpfx, bool set)
{
	const int64_t	base = 0x0123456789abcdef;

	if (set) {
		var = base;
		return (true);
	}

	// Verify data.
	if (var == base)
		return (true);

	os::printf("FAIL: %s LongLong (int64_t) 'out' value:\n", msgpfx);
	os::printf("\texpected = 0x%llx, actual = 0x%llx\n", base, var);
	return (false);
}


//
// Sets up (or verifies if "set" argument is false) incoming 'inout' parameter.
//
bool
set_io_i(int64_t &var, const char *msgpfx, bool set)
{
	const int64_t	base = 0xfdb9753102468ac;

	if (set) {
		var = base;
		return (true);
	}

	// Verify data.
	if (var == base)
		return (true);

	os::printf("FAIL: %s LongLong (int64_t) 'inout' value:\n", msgpfx);
	os::printf("\texpected = 0x%llx, actual = 0x%llx\n", base, var);
	return (false);
}


//
// Sets up (or verifies if "set" argument is false) outgoing 'inout' parameter.
//
bool
set_io_o(int64_t &var, const char *msgpfx, bool set)
{
	const int64_t	base = 0xca8642013579bdf;

	if (set) {
		var = base;
		return (true);
	}

	// Verify data.
	if (var == base)
		return (true);

	os::printf("FAIL: %s LongLong (int64_t) 'inout' value:\n", msgpfx);
	os::printf("\texpected = 0x%llx, actual = 0x%llx\n", base, var);
	return (false);
}


//
// Sets up (or verifies if "set" argument is false) returned value.
//
bool
set_ret(int64_t &var, const char *msgpfx, bool set)
{
	const int64_t	base = 0xffffffffffffff00;

	if (set) {
		var = base;
		return (true);
	}

	// Verify data.
	if (var == base)
		return (true);

	os::printf("FAIL: %s LongLong (int64_t) returned value:\n", msgpfx);
	os::printf("\texpected = 0x%llx, actual = 0x%llx\n", base, var);
	return (false);
}
