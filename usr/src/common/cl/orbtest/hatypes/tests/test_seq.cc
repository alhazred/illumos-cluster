/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_seq.cc	1.6	08/05/20 SMI"

//
// Sequence read/write/read-write tests.
// These tests are primarily intended to support performance measurements.
//
// These tests were derived from the existing unbounded/bounded sequence tests.
//

#include <orbtest/hatypes/client_impl.h>

#if defined(_KERNEL) || defined(_KERNEL_ORB)
#include <orbtest/hatypes/server_impl.h>
#endif

static bool	set_in(hatypetest::UBseq &var, uint32_t len,
			const char *msgpfx, bool set = true);

static bool	set_out(hatypetest::UBseq &var, uint32_t len,
			const char *msgpfx, bool set);

static bool 	set_io_i(hatypetest::UBseq &var, uint32_t len,
			const char *msgpfx, bool set = true);

static bool	set_io_o(hatypetest::UBseq &var, uint32_t len,
			const char *msgpfx, bool set);

inline bool
verify_in(const hatypetest::UBseq &v, uint32_t l, const char *p)
{
	return (set_in((hatypetest::UBseq &)v, l, p, false));
}

inline bool
verify_out(const hatypetest::UBseq &v, uint32_t l, const char *p)
{
	return (set_out((hatypetest::UBseq &)v, l, p, false));
}

inline bool
verify_io_i(const hatypetest::UBseq &v, uint32_t l, const char *p)
{
	return (set_io_i((hatypetest::UBseq &)v, l, p, false));
}

inline bool
verify_io_o(const hatypetest::UBseq &v, uint32_t l, const char *p)
{
	return (set_io_o((hatypetest::UBseq &)v, l, p, false));
}

//
// Sets up (or verifies if "set" argument is false) 'in' parameter.
//
bool
set_in(hatypetest::UBseq &var, uint32_t len, const char *msgpfx, bool set)
{
	// This way we don't have to know
	// the type of the sequence members
	hatypetest::UBseq	base(1);

	base[0] = 0;

	if (set) {
		var.load(len, len, _FixedSeq_<uint8_t>::allocbuf(len), true);
		for (uint32_t i = 0; i < var.length(); ++i) {
			var[i] = base[0];
			base[0] = (base[0] == 0) ? 1 : (uint8_t)(base[0] << 1);
		}
		return (true);
	}

	// Verify data.
	if (var.maximum() != len) {
		os::printf("FAIL: %s maximum of 'in' unbounded sequence:\n",
			msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			len, var.maximum());
	}

	if (var.length() != len) {
		os::printf("FAIL: %s length of 'in' unbounded sequence:\n",
			msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			len, var.length());
		return (false);
	}

	for (uint32_t i = 0; i < var.length(); ++i) {
		if (var[i] == base[0]) {
			base[0] = (base[0] == 0) ? 1 : (uint8_t)(base[0] << 1);
			continue;
		}

		os::printf("FAIL: %s value of 'in' unbounded sequence "
			"at index %d:\n", msgpfx, i);
		os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
			(uint64_t)base[0], (uint64_t)var[i]);
		return (false);
	}
	return (true);
}

//
// Sets up (or verifies if "set" argument is false) 'out' parameter.
//
bool
set_out(hatypetest::UBseq &var, uint32_t len, const char *msgpfx, bool set)
{
	// This way we don't have to know
	// the type of the sequence members
	hatypetest::UBseq	base(1);

	base[0] = ~0;

	if (set) {
		// Load sequence with a special pattern of values.
		for (uint32_t i = 0; i < var.length(); ++i) {
			var[i] = base[0];
			base[0] = (base[0] == 0) ? ~0 : base[0] >> 1;
		}
		return (true);
	}

	// Verify data.
	if (var.maximum() != len + 1) {
		os::printf("FAIL: %s maximum of 'out' unbounded sequence:\n",
		    msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
		    len + 1, var.maximum());
	}

	if (var.length() != len) {
		os::printf("FAIL: %s length of 'out' unbounded sequence:\n",
		    msgpfx);
		os::printf("\texpected = %d, actual = %d\n", len, var.length());
		return (false);
	}

	for (uint32_t i = 0; i < var.length(); ++i) {
		if (var[i] == base[0]) {
			base[0] = (base[0] == 0) ? ~0 : base[0] >> 1;
		} else {
			os::printf("FAIL: %s value of 'out' unbounded sequence "
			    "at index %d:\n", msgpfx, i);
			os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
			    (uint64_t)base[0], (uint64_t)var[i]);
			return (false);
		}
	}
	return (true);
}

//
// Sets up (or verifies if "set" argument is false) incoming 'inout' parameter.
//
bool
set_io_i(hatypetest::UBseq &var, uint32_t len, const char *msgpfx, bool set)
{
	// This way we don't have to know
	// the type of the sequence members
	hatypetest::UBseq	base(1);

	base[0] = ~0;

	if (set) {
		var.load(len, len, _FixedSeq_<uint8_t>::allocbuf(len), true);
		for (uint32_t i = 0; i < var.length(); ++i) {
			var[i] = base[0];
			base[0] = (base[0] == 0) ? ~0 : (uint8_t)(base[0] << 1);
		}
		return (true);
	}

	// Verify data.
	if (var.maximum() != len) {
		os::printf("FAIL: %s maximum of 'inout' "
			"unbounded sequence:\n", msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			len, var.maximum());
	}

	if (var.length() != len) {
		os::printf("FAIL: %s length of 'inout' "
			"unbounded sequence:\n", msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			len, var.length());
		return (false);
	}

	for (uint32_t i = 0; i < var.length(); ++i) {
		if (var[i] == base[0]) {
			base[0] = (base[0] == 0) ? ~0 : (uint8_t)(base[0] << 1);
			continue;
		}

		os::printf("FAIL: %s value of 'inout' unbounded sequence "
			"at index %d:\n", msgpfx, i);
		os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
			(uint64_t)base[0], (uint64_t)var[i]);
		return (false);
	}
	return (true);
}

//
// Sets up (or verifies if "set" argument is false) outgoing 'inout' parameter.
//
bool
set_io_o(hatypetest::UBseq &var, uint32_t len, const char *msgpfx, bool set)
{
	// This way we don't have to know
	// the type of the sequence members
	hatypetest::UBseq	base(1);

	base[0] = 0;

	if (set) {
		var.load(len + 1, len + 1,
		    _FixedSeq_<uint8_t>::allocbuf(len + 1), true);
		for (uint32_t i = 0; i < var.length(); ++i) {
			var[i] = base[0];
			base[0] = (base[0] == 0) ? ~0 : base[0] >> 1;
		}
		return (true);
	}

	// Verify data.
	if (var.maximum() != len + 1) {
		os::printf("FAIL: %s maximum of 'inout' "
			"unbounded sequence:\n", msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			len + 1, var.maximum());
	}

	if (var.length() != len + 1) {
		os::printf("FAIL: %s length of 'inout' "
			"unbounded sequence:\n", msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			len + 1, var.length());
		return (false);
	}

	for (uint32_t i = 0; i < var.length(); ++i) {
		if (var[i] == base[0]) {
			base[0] = (base[0] == 0) ? ~0 : base[0] >> 1;
			continue;
		}

		os::printf("FAIL: %s value of 'inout' unbounded sequence "
			"at index %d:\n", msgpfx, i);
		os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
			(uint64_t)base[0], (uint64_t)var[i]);
		return (false);
	}
	return (true);
}

//
// Read Test client interface.  Invoked by driver "programs" and, in turn,
// invokes the corresponding method on the test server side.
// Parameters:
//	server_ref  --	test server object to invoke.
//	num_repeat  --	how many times to run the test.
//	len	    --	length of unbounded sequence to test.
//	noverify    --	if true then server won't perform data verification.
// Returns:
//	Total test execution time.
//
int64_t
client_impl::test_seq_read(
	hatypetest::server_ptr	server_ref,
	int32_t			num_repeat,
	uint32_t		len,
	bool			noverify,
	Environment		&)
{
	os::hrtime_t	total_time = 0;
	os::hrtime_t	start_time;

	if (noverify) {
		Environment	e3;

		server_ref->set_noverify(e3);
		if (!hatypes_env_ok(e3, msg_prefix())) {
			return (-1);
		}
	}

	for (; num_repeat > 0; --num_repeat) {
		Environment		e2;
		hatypetest::UBseq_var	outp;

		//
		// Invoke server sequence read
		//
		start_time = os::gethrtime();
		server_ref->test_seq_read(outp, len, e2);
		if (!hatypes_env_ok(e2, msg_prefix())) {
			total_time = -1;
			break;
		}
		total_time += os::gethrtime() - start_time;

		if (noverify) {
			//
			// Verification disabled.
			// Only check the sequence length.
			//
			if (outp->length() != len) {
				os::printf("FAIL: %s length of 'out' unbounded"
				    " sequence:\n", msg_prefix());
				os::printf("\texpected = %d, actual = %d\n",
				    len, outp->length());
			}
		} else {
			// Verify values returned by server.
			(void) verify_out(*outp, len, msg_prefix());
		}
	}
	return (total_time);
}

//
// Read Test server interface.  Invoked by the test client.
//
#if defined(_KERNEL) || defined(_KERNEL_ORB)
void
server_impl::test_seq_read(hatypetest::UBseq_out	outp, uint32_t len,
    Environment &)
{
	// Create a sequence of the desired length
	outp = new hatypetest::UBseq(len + 1, len);

	if (!verify_off) {
		// Load sequence with a special pattern
		(void) set_out(*outp, len, msg_prefix(), true);
	}
}
#endif


//
// Write Test client interface.  Invoked by driver "programs" and, in turn,
// invokes the corresponding method on the test server side.
// Parameters:
//	server_ref  --	test server object to invoke.
//	num_repeat  --	how many times to run the test.
//	len	    --	length of unbounded sequence to test.
//	noverify    --	if true then server won't perform data verification.
// Returns:
//	Total test execution time.
//
int64_t
client_impl::test_seq_write(
	hatypetest::server_ptr	server_ref,
	int32_t			num_repeat,
	uint32_t		len,
	bool			noverify,
	Environment		&)
{
	os::hrtime_t	total_time = 0;
	os::hrtime_t	start_time;

	if (noverify) {
		Environment	e3;

		server_ref->set_noverify(e3);
		if (!hatypes_env_ok(e3, msg_prefix())) {
			return (-1);
		}
	}

	for (; num_repeat > 0; --num_repeat) {
		Environment		e2;
		hatypetest::UBseq		in;

		// Prepare data to be passed to server.
		(void) set_in(in, len, msg_prefix());

		//
		// Invoke server sequence write
		//
		start_time = os::gethrtime();
		server_ref->test_seq_write(in, len, e2);
		if (!hatypes_env_ok(e2, msg_prefix())) {
			total_time = -1;
			break;
		}
		total_time += os::gethrtime() - start_time;
	}
	return (total_time);
}


//
// Test server interface.  Invoked by the test client.
//
#if defined(_KERNEL) || defined(_KERNEL_ORB)
void
server_impl::test_seq_write(const hatypetest::UBseq &in, uint32_t len,
    Environment &)
{
	if (!verify_off) {
		// Verify values passed by client.
		(void) verify_in(in, len, msg_prefix());
	}
}
#endif


//
// Read-Write Test client interface.  Invoked by driver "programs" and, in turn,
// invokes the corresponding method on the test server side.
// Parameters:
//	server_ref  --	test server object to invoke.
//	num_repeat  --	how many times to run the test.
//	len	    --	length of unbounded sequence to test.
//	noverify    --	if true then server won't perform data verification.
// Returns:
//	Total test execution time.
//
int64_t
client_impl::test_seq_rw(
	hatypetest::server_ptr	server_ref,
	int32_t			num_repeat,
	uint32_t		len,
	bool			noverify,
	Environment		&)
{
	os::hrtime_t	total_time = 0;
	os::hrtime_t	start_time;

	if (noverify) {
		Environment	e3;

		server_ref->set_noverify(e3);
		if (!hatypes_env_ok(e3, msg_prefix())) {
			return (-1);
		}
	}

	for (; num_repeat > 0; --num_repeat) {
		Environment		e2;
		hatypetest::UBseq		inout;

		// Prepare data to be passed to server.
		(void) set_io_i(inout, len, msg_prefix());

		// Invoke server.
		start_time = os::gethrtime();
		server_ref->test_seq_rw(inout, len, e2);
		if (!hatypes_env_ok(e2, msg_prefix())) {
			total_time = -1;
			break;
		}
		total_time += os::gethrtime() - start_time;

		if (noverify) {
			//
			// Verification disabled.
			// Only check the sequence length.
			// With verification disabled,
			// the server returns the same sequence to the client.
			//
			if (inout.length() != len) {
				os::printf("FAIL: %s length of 'inout' "
				    "unbounded sequence:\n", msg_prefix());
				os::printf("\texpected = %d, actual = %d\n",
				    len, inout.length());
			}
		} else {
			// Verify values returned by server.
			// When verification is enabled,
			// the server returns a sequence with a different
			// set of values from that sent by the client.
			//
			(void) verify_io_o(inout, len, msg_prefix());
		}
	}
	return (total_time);
}


//
// Read-Write Test server interface. Invoked by the test client.
//
// When verification is turned off,
// simply return the same sequence to the client.
//
#if defined(_KERNEL) || defined(_KERNEL_ORB)
void
server_impl::test_seq_rw(hatypetest::UBseq &inout, uint32_t len, Environment &)
{
	if (!verify_off) {
		// Verify values passed by client.
		(void) verify_io_i(inout, len, msg_prefix());

		// Prepare values to be returned to client.
		(void) set_io_o(inout, len, msg_prefix(), true);
	}
}
#endif
