/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_versioning.cc	1.7	08/05/20 SMI"

#include <nslib/ns.h>
#include "stdio.h"
#include <orbtest/hatypes/client_impl.h>

#if defined(_KERNEL) || defined(_KERNEL_ORB)
#include <orbtest/hatypes/server_impl.h>
#endif

#if defined(_KERNEL_ORB) && !defined(_KERNEL)
#define	_UNODE
#endif


//
// This test is expected to be run before an upgrade commit
// so the server should appear to be V0.
//
bool
client_impl::test_versioning_1(hatypetest::server_ptr server_ref, Environment &)
{
	Environment e;
	naming::naming_context_var root_ns;
	CORBA::Object_var obj_v;
	hatypetest::minverObjA_var serverA_v;

	//
	// Assertion: If server is either V0 or pretending to be V0,
	// then _version() should return 0.
	//
	os::printf("Exercising hatypetest::server::_version\t\t\t\t");
	int version = hatypetest::server::_version(server_ref);
	if (version != 0) {
		os::printf("FAILED\n");
		os::printf("\tserver::_version returned %d, "
		    "was expecting 0\n", version);
		return (false);
	} else {
		os::printf("PASS\n");
	}

	//
	// Try to invoke the version() method on the server_impl.
	// This should succeed and return service_version = 0.
	//
	os::printf("Exercising server_impl::version\t\t\t\t\t");
	int16_t service_version = server_ref->version(e);
	if (service_version != 0) {
		os::printf("FAILED\n");
		os::printf("\tserver_impl::version returned %d, "
		    "was expecting 0\n", version);
		return (false);
	} else if (e.exception()) {
		os::printf("FAILED\n");
		os::printf("\tserver_impl::version returned exception:");
		e.exception()->print_exception("\t");
		return (false);
	} else {
		os::printf("PASS\n");
	}

	//
	// Now we lookup one of our IDL versioning test objects.
	//
	os::printf("Looking up minverObjA server in root nameserver\t\t\t");
	root_ns = ns::root_nameserver();
	obj_v = root_ns->resolve(Akserver_name, e);
	if (e.exception() || CORBA::is_nil(obj_v)) {
		os::printf("FAILED\n");
		os::printf("\tORB nameserver couldn't resolve "
		    "\"%s\"\n", Akserver_name);
		e.exception()->print_exception("\t");
		return (false);
	} else {
		os::printf("PASS\n");
	}

	//
	// Assertion: Our client should be able to narrow object to minverObjA.
	//
	os::printf("Narrowing object ref to type minverObjA\t\t\t\t");
	serverA_v = hatypetest::minverObjA::_narrow(obj_v);
	if (CORBA::is_nil(serverA_v)) {
		os::printf("FAILED\n");
		return (false);
	} else
		os::printf("PASS\n");

	//
	// Assertion: If server is V0 then _version should return 0.
	//
	os::printf("Exercising _version method on minverObjA ref\t\t\t");
	if ((version = hatypetest::minverObjA::_version(serverA_v)) != 0) {
		os::printf("FAILED\n");
		os::printf("\tminverObjA::_version returned %d, "
		    "was expecting 0\n", version);
		return (false);
	} else {
		os::printf("PASS\n");
	}

#ifdef ORBTEST_V1
	//
	// Assertion: If V1 client tries to invoke V1 interface on V0 server,
	// 		then the UNSUPPORTED_VERSION exception should
	// 		get thrown.
	//
	os::printf("Invoking Version 1 interface on Version %d service\t\t",
	    service_version);
	serverA_v->setid(4321, e);
	if (e.exception()) {
		os::printf("PASS\n");
		e.exception()->print_exception("\t");
		e.clear();
	} else {
		os::printf("FAILED\n");
		return (false);
	}
#endif

	return (true);
}


#ifdef ORBTEST_V1
//
// This test is expected to be run after an upgrade commit
// so the server should appear to be V1.
//
bool
client_impl::test_versioning_2(hatypetest::server_ptr server_ref, Environment &)
{
	Environment e;
	naming::naming_context_var root_ns;
	CORBA::Object_var obj_v;
	hatypetest::minverObjA_var serverA_v;
	hatypetest::minverObjC_var serverC_v;

	//
	// Assertion: server should be V1 and _version() should return 1.
	//
	os::printf("Exercising hatypetest::server::_version\t\t\t\t");
	int version = hatypetest::server::_version(server_ref);
	if (version != 1) {
		os::printf("FAILED\n");
		os::printf("\tserver::_version returned %d, "
		    "was expecting 1\n", version);
		return (false);
	} else {
		os::printf("PASS\n");
	}

	//
	// Try to invoke the version() method on the server_impl.
	// This should succeed and return service_version = 1.
	//
	os::printf("Exercising server_impl::version\t\t\t\t\t");
	int16_t service_version = server_ref->version(e);
	if (service_version != 1) {
		os::printf("FAILED\n");
		os::printf("\tserver_impl::version returned %d, "
		    "was expecting 1\n", service_version);
		return (false);
	} else if (e.exception()) {
		os::printf("FAILED\n");
		os::printf("\tserver_impl::version returned exception:");
		e.exception()->print_exception("\t");
		return (false);
	} else {
		os::printf("PASS\n");
	}

	//
	// Try to invoke the getid() method on the server_impl.
	// This should succeed and create new objects for later tests.
	//
	os::printf("Exercising server_impl::getid\t\t\t\t\t");
	int16_t id = server_ref->getid(e);
	if (id != 1234) {
		os::printf("FAILED\n");
		os::printf("\tserver_impl::getid returned %d, "
		    "was expecting 1234\n", id);
		return (false);
	} else if (e.exception()) {
		os::printf("FAILED\n");
		os::printf("\tserver_impl::getid returned exception:");
		e.exception()->print_exception("\t");
		return (false);
	} else {
		os::printf("PASS\n");
	}

	//
	// Now we lookup one of our IDL versioning test objects.
	//
	os::printf("Looking up minverObjA server in root nameserver\t\t\t");
	root_ns = ns::root_nameserver();
	obj_v = root_ns->resolve(Akserver_name, e);
	if (e.exception() || CORBA::is_nil(obj_v)) {
		os::printf("FAILED\n");
		os::printf("\tORB nameserver couldn't resolve "
		    "\"%s\"\n", Akserver_name);
		e.exception()->print_exception("\t");
		return (false);
	} else {
		os::printf("PASS\n");
	}

	//
	// Assertion: Our client should be able to narrow object to minverObjA.
	//
	os::printf("Narrowing object ref to type minverObjA\t\t\t\t");
	serverA_v = hatypetest::minverObjA::_narrow(obj_v);
	if (CORBA::is_nil(serverA_v)) {
		os::printf("FAILED\n");
		return (false);
	} else
		os::printf("PASS\n");

	//
	// Assertion: If server is V1 then _version should return 1.
	//
	os::printf("Exercising _version method on minverObjA ref\t\t\t");
	if ((version = hatypetest::minverObjA::_version(serverA_v)) != 1) {
		os::printf("FAILED\n");
		os::printf("\tminverObjA::_version returned %d, "
		    "was expecting 1\n", version);
		return (false);
	} else {
		os::printf("PASS\n");
	}

	//
	// Assertion: If V1 client tries to invoke V1 interface on V1 server,
	// 		it should work.
	//
	os::printf("Invoking Version 1 interface on Version %d service\t\t",
	    service_version);
	serverA_v->setid(4321, e);
	if (e.exception()) {
		os::printf("FAILED\n");
		e.exception()->print_exception("\t");
		return (false);
	} else {
		os::printf("PASS\n");
	}

	//
	// Test getting a different proxy for this interface.
	//
	os::printf("Test version 0 proxy object minverObjA\t\t\t\t");

	// Get a proxy to the older interface.
	CORBA::type_info_t *tp =
	    hatypetest::minverObjA::_get_type_info(0);
	obj_v = CORBA::create_proxy_obj(tp, serverA_v->_handler());
	if (CORBA::is_nil(obj_v)) {
		os::printf("FAILED_1\n");
		return (false);
	}
	hatypetest::minverObjA_var p0_v =
	    hatypetest::minverObjA::_narrow(obj_v);
	if (CORBA::is_nil(p0_v)) {
		os::printf("FAILED_2\n");
		return (false);
	}

	// Call version 1 method.
	p0_v->setid(4321, e);
	if (e.exception()) {
		os::printf("PASS\n");
		e.exception()->print_exception("\t");
		e.clear();
	} else {
		os::printf("FAILED\n");
		return (false);
	}

	// Test generic method.
	os::printf("Test _generic_method() minverObjA\t\t\t\t");
	CORBA::octet_seq_t data(8, 8, (uint8_t *)"driverX", false);
	CORBA::object_seq_t objs(1, 1);
	objs[0] = CORBA::Object::_duplicate(serverA_v);
	p0_v->_generic_method(data, objs, e);

	if (e.exception()) {
		os::printf("FAILED_1\n");
		e.exception()->print_exception("\t");
		return (false);
	} else if (data.length() != 9 ||
	    os::strcmp((const char *)data.buffer(), "server_A") != 0) {
		os::printf("FAILED_2\n");
		os::printf("\tlen %d %s\n", data.length(),
		    data.buffer());
		return (false);
	} else {
		os::printf("PASS\n");
	}

	// Call generic method again to test version exception.
	os::printf("Test version exception minverObjA\t\t\t\t");
	p0_v->_generic_method(data, objs, e);

	if (e.exception()) {
		os::printf("PASS\n");
		e.exception()->print_exception("\t");
		e.clear();
	} else {
		os::printf("FAILED\n");
		return (false);
	}

	os::printf("Looking up minverObjC server in root nameserver\t\t\t");
	obj_v = root_ns->resolve(Ckserver_name, e);
	if (e.exception() || CORBA::is_nil(obj_v)) {
		os::printf("FAILED\n");
		os::printf("\tORB nameserver couldn't resolve "
		    "\"%s\"\n", Ckserver_name);
		e.exception()->print_exception("\t");
		return (false);
	} else {
		os::printf("PASS\n");
	}

	//
	// Assertion: Version 1 client should be able to narrow object
	// 		to minverObjC
	//
	os::printf("Narrowing object ref to type minverObjB\t\t\t\t");
	serverC_v = hatypetest::minverObjC::_narrow(obj_v);
	if (CORBA::is_nil(serverC_v)) {
		os::printf("FAILED\n");
		return (false);
	} else {
		os::printf("PASS\n");
	}

	//
	// Assertion: If client is V1 and this server implements V0 of
	// 		minverObjC then _version should return 0
	//
	os::printf("Exercising _version method on minverObjC ref\t\t\t");
	if ((version = hatypetest::minverObjC::_version(serverC_v)) != 0) {
		os::printf("FAILED\n");
		os::printf("\tminverObjC::_version returned %d, "
		    "was expecting 0\n", version);
		return (false);
	} else {
		os::printf("PASS\n");
	}

	//
	// Assertion: Since the minverObjC interface inherits from the v1
	// 		minverObjA interface, then we should be able to narrow
	// 		our minverObjC to a minverObjA reference
	//
	os::printf("Narrowing minverObjC object ref to type minverObjA\t\t");
	serverA_v = hatypetest::minverObjA::_narrow(serverC_v);
	if (CORBA::is_nil(serverA_v)) {
		os::printf("FAILED\n");
		return (false);
	} else {
		os::printf("PASS\n");
	}

	//
	// Assertion: If we run _version on our newly acquired minverObjA ref
	// 		it should return 1
	//
	os::printf("Exercising _version method on minverObjA ref\t\t\t");
	if ((version = hatypetest::minverObjA::_version(serverA_v)) != 1) {
		os::printf("FAILED\n");
		os::printf("\tminverObjA::_version returned %d, "
		    "was expecting 1\n", version);
		return (false);
	} else {
		os::printf("PASS\n");
	}

	return (true);
}

//
// This test is expected to be run after an upgrade commit
// so the server should appear to be V1.
//
bool
client_impl::test_versioning_3(Environment &)
{
	Environment e;
	hatypetest::combinedObjD_var serverD_v;
	hatypetest::minverObjA_var serverA_v;
	hatypetest::majverObjB_var serverB_v;
	naming::naming_context_var root_ns;
	CORBA::Object_var obj_v;

	os::printf("Looking up combinedObjD server in root nameserver\t\t");
	root_ns = ns::root_nameserver();
	obj_v = root_ns->resolve(Dkserver_name, e);
	if (e.exception() || CORBA::is_nil(obj_v)) {
		os::printf("FAILED\n");
		os::printf("\tORB nameserver couldn't resolve "
		    "\"%s\"\n", Dkserver_name);
		e.exception()->print_exception("\t");
		return (false);
	} else {
		os::printf("PASS\n");
	}

	//
	// Assertion: Version 1 client should NOT be able to narrow object to
	// 		server_only type combinedObjD
	//
	os::printf("Narrowing object ref to server_only type "
	    "combinedObjD\t\t");
	serverD_v = hatypetest::combinedObjD::_narrow(obj_v);
	if (CORBA::is_nil(serverD_v->_this_obj())) {
		os::printf("PASS\n");
	} else {
		os::printf("FAILED\n");
		return (false);
	}


	//
	// Assertion: Version 1 client should be able to narrow object to
	// 		type minverObjA.
	//
	os::printf("Narrowing object ref to type minverObjA\t\t\t\t");
	serverA_v = hatypetest::minverObjA::_narrow(obj_v);
	if (CORBA::is_nil(serverA_v)) {
		os::printf("FAILED\n");
		return (false);
	} else {
		os::printf("PASS\n");
	}
	//
	// Assertion: Version 1 client should be able to narrow object to
	// 		type majverObjB.
	//
	os::printf("Narrowing object ref to type majverObjB\t\t\t\t");
	serverB_v = hatypetest::majverObjB::_narrow(obj_v);
	if (CORBA::is_nil(serverB_v)) {
		os::printf("FAILED\n");
		return (false);
	} else {
		os::printf("PASS\n");
	}

	return (true);
}
#endif // ORBTEST_V1
