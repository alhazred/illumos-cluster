/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_vstruct.cc	1.6	08/05/20 SMI"

//
// Variable structure test
//

#include <orbtest/hatypes/client_impl.h>

#if defined(_KERNEL) || defined(_KERNEL_ORB)
#include <orbtest/hatypes/server_impl.h>
#endif

static bool	set_in(hatypetest::Vstruct &var, uint32_t len,
			hatypetest::Obj_ptr objref, const char *msgpfx,
			bool set = true);
static bool	set_out(hatypetest::Vstruct &var, uint32_t len,
			hatypetest::Obj_ptr objref, const char *msgpfx,
			bool set = true);
static bool	set_io_i(hatypetest::Vstruct &var, uint32_t len,
			hatypetest::Obj_ptr objref, const char *msgpfx,
			bool set = true);
static bool	set_io_o(hatypetest::Vstruct &var, uint32_t len,
			hatypetest::Obj_ptr objref, const char *msgpfx,
			bool set = true);
static bool	set_ret(hatypetest::Vstruct &var, uint32_t len,
			hatypetest::Obj_ptr objref, const char *msgpfx,
			bool set = true);

inline bool
verify_in(const hatypetest::Vstruct &v, uint32_t l, hatypetest::Obj_ptr o,
		const char *p)
{
	return (set_in((hatypetest::Vstruct &)v, l, o, p, false));
}

inline bool
verify_out(const hatypetest::Vstruct &v, uint32_t l, hatypetest::Obj_ptr o,
		const char *p)
{
	return (set_out((hatypetest::Vstruct &)v, l, o, p, false));
}

inline bool
verify_io_i(const hatypetest::Vstruct &v, uint32_t l, hatypetest::Obj_ptr o,
		const char *p)
{
	return (set_io_i((hatypetest::Vstruct &)v, l, o, p, false));
}

inline bool
verify_io_o(const hatypetest::Vstruct &v, uint32_t l, hatypetest::Obj_ptr o,
		const char *p)
{
	return (set_io_o((hatypetest::Vstruct &)v, l, o, p, false));
}

inline bool
verify_ret(const hatypetest::Vstruct &v, uint32_t l, hatypetest::Obj_ptr o,
		const char *p)
{
	return (set_ret((hatypetest::Vstruct &)v, l, o, p, false));
}

//
// Test client interface.  Invoked by driver "programs" and, in turn,
// invokes the corresponding method on the test server side.
// Parameters:
//	server_ref  --	test server object to invoke.
//	num_repeat  --	how many times to run the test.
//	len	    --	length of the structure's sequence member to test.
//	noverify    --	if true then server won't perform data verification.
// Returns:
//	Total test execution time.
//
int64_t
client_impl::test_vstruct(
		hatypetest::server_ptr server_ref,
		int32_t num_repeat,
		uint32_t len,
		bool noverify,
		Environment &)
{
	// os::printf("%s test_vstruct()\n", msg_prefix());
	os::hrtime_t	total_time = 0, start_time;

	if (noverify) {
		Environment	e3;

		server_ref->set_noverify(e3);
		if (! hatypes_env_ok(e3, msg_prefix())) {
			return (-1);
		}
	}

	for (; num_repeat > 0; --num_repeat) {
		Environment		e2;
		Obj_impl		obj_impl(hatypes_in_obj_id);
		hatypetest::Obj_ptr	objref = obj_impl.get_objref();

		//
		// Do test in a code block so we can verify that
		// object reference members of the Vstruct's below are
		// released when the structures are destroyed.
		//
		{
			hatypetest::Vstruct	in, inout;
			hatypetest::Vstruct_var	outp, retp;

			// Prepare data to be passed to server.
			(void) set_in(in, len, objref, msg_prefix());
			(void) set_io_i(inout, len, objref, msg_prefix());

			// Invoke server.
			start_time = os::gethrtime();
			retp = server_ref->test_vstruct(in, outp, inout,
							len, objref, e2);
			if (! hatypes_env_ok(e2, msg_prefix())) {
				total_time = -1;
				num_repeat = 0;		// end loop
			} else {
				total_time += os::gethrtime() - start_time;

				// Verify values returned by server.
				(void) verify_out(*outp, len, objref,
							msg_prefix());
				(void) verify_io_o(inout, len, objref,
							msg_prefix());
				(void) verify_ret(*retp, len, objref,
							msg_prefix());
			}
		}

		// Verify that _unreferenced() is (or will be) called.
		CORBA::release(objref);
		(void) verify_unreferenced(&obj_impl, msg_prefix());
	}

	return (total_time);
}


//
// Test server interface.  Invoked by the test client.
//
#if defined(_KERNEL) || defined(_KERNEL_ORB)
hatypetest::Vstruct *
server_impl::test_vstruct(
		const hatypetest::Vstruct &in,
		hatypetest::Vstruct_out outp,
		hatypetest::Vstruct &inout,
		uint32_t len,
		hatypetest::Obj_ptr objref,
		Environment &)
{
	// os::printf("%s test_vstruct()\n", msg_prefix());
	hatypetest::Vstruct	*retp;		// don't use _var!

	if (!verify_off) {
		// Verify values passed by client.
		verify_in(in, len, objref, msg_prefix());
		verify_io_i(inout, len, objref, msg_prefix());
	}

	// Prepare values to be returned to client.
	outp = new hatypetest::Vstruct;
	set_out(*outp, len, objref, msg_prefix());

	set_io_o(inout, len, objref, msg_prefix());

	retp = new hatypetest::Vstruct;
	set_ret(*retp, len, objref, msg_prefix());

	return (retp);
}
#endif


//
// Sets up (or verifies if "set" argument is false) 'in' parameter.
//
bool
set_in(hatypetest::Vstruct &var, uint32_t len, hatypetest::Obj_ptr objref,
		const char *msgpfx, bool set)
{
	const uint8_t	base_1byte = 0xF7;
	char		base_char = 1;
	uint32_t	i;
	bool		failed = false;

	if (set) {
		var.m_1byte = base_1byte;

		var.m_str = new char[len + 1];
		for (i = 0; i < len; ++i) {
			if (base_char == 0)
				base_char = 1;
			var.m_str[i] = base_char;
			base_char <<= 1;
		}
		var.m_str[i] = '\0';

		var.m_obj = hatypetest::Obj::_duplicate(objref);

		var.m_char_arr[0] = 'a';
		var.m_char_arr[1] = 'b';

		return (true);
	}

	// Verify 1-byte member.
	if (var.m_1byte != base_1byte) {
		os::printf("FAIL: %s 1-byte member of 'in' variable struct:\n",
			msgpfx);
		os::printf("\texpected = 0x%x, actual = 0x%x\n",
			base_1byte, var.m_1byte);
		failed = true;
	}

	// Verify string member.
	for (i = 0; i < len; ++i) {
		if (base_char == 0)
			base_char = 1;
		if (var.m_str[i] == base_char) {
			base_char <<= 1;
			continue;
		}

		os::printf("FAIL: %s char at index %d of string member of "
			"'in' variable struct:\n", msgpfx, i);
		os::printf("\texpected = 0x%x, actual = 0x%x\n",
			base_char, var.m_str[i]);
		failed = true;
		break;
	}
	if (var.m_str[i] != '\0') {
		os::printf("FAIL: %s string member of 'in' variable struct "
			"not terminated at index %d:\n", msgpfx, i);
		failed = true;
	}

	// Verify object reference member.
	if (CORBA::is_nil(var.m_obj)) {
		os::printf("FAIL: %s object reference member of 'in' "
			"variable struct is NIL\n", msgpfx);
		failed = true;
	} else {
		int32_t		act_id, exp_id;
		Environment	e;

		act_id = var.m_obj->id(e);
		if (e.exception()) {
			os::printf("FAIL: %s object reference member of "
			    "'in' variable struct had exception in id()\n",
			    msgpfx);
			e.clear();
			failed = true;
		}
		exp_id = objref->id(e);
		if (e.exception()) {
			os::printf("FAIL: %s object reference had exception "
			    "in id()\n", msgpfx);
			e.clear();
			failed = true;
		}
		if (act_id != exp_id) {
			os::printf("FAIL: %s object reference member of "
				"'in' variable struct:\n", msgpfx);
			os::printf("\texpected id = %d, actual id = %d\n",
				exp_id, act_id);
			failed = true;
		}
	}

	// Verify char array
	if (var.m_char_arr[0] != 'a' ||	var.m_char_arr[1] != 'b') {
		os::printf("FAIL: %s char array member of "
			"'in' variable struct:\n", msgpfx);
		os::printf("\texpected ab, actual %c%c\n",
			var.m_char_arr[0], var.m_char_arr[1]);
		failed = true;
	}

	return (!failed);
}


//
// Sets up (or verifies if "set" argument is false) 'out' parameter.
//
bool
set_out(hatypetest::Vstruct &var, uint32_t len, hatypetest::Obj_ptr objref,
		const char *msgpfx, bool set)
{
	const uint8_t	base_1byte = 0x7F;
	char		base_char = ~0;
	uint32_t	i;
	bool		failed = false;

	if (set) {
		var.m_1byte = base_1byte;

		var.m_str = new char[len + 2];
		for (i = 0; i < len + 1; ++i) {
			if (base_char == 0)
				base_char = ~0;
			var.m_str[i] = base_char;
			base_char >>= 1;
		}
		var.m_str[i] = '\0';

		var.m_obj = hatypetest::Obj::_duplicate(objref);

		var.m_char_arr[0] = 'c';
		var.m_char_arr[1] = 'd';

		return (true);
	}

	// Verify 1-byte member.
	if (var.m_1byte != base_1byte) {
		os::printf("FAIL: %s 1-byte member of 'out' "
			"variable struct:\n", msgpfx);
		os::printf("\texpected = 0x%x, actual = 0x%x\n",
			base_1byte, var.m_1byte);
		failed = true;
	}

	// Verify string member.
	for (i = 0; i < len + 1; ++i) {
		if (base_char == 0)
			base_char = ~0;
		if (var.m_str[i] == base_char) {
			base_char >>= 1;
			continue;
		}

		os::printf("FAIL: %s char at index %d of string member of "
			"'out' variable struct:\n", msgpfx, i);
		os::printf("\texpected = 0x%x, actual = 0x%x\n",
			base_char, var.m_str[i]);
		failed = true;
		break;
	}
	if (var.m_str[i] != '\0') {
		os::printf("FAIL: %s string member of 'out' variable struct "
			"not terminated at index %d:\n", msgpfx, i);
		failed = true;
	}

	// Verify object reference member.
	if (CORBA::is_nil(var.m_obj)) {
		os::printf("FAIL: %s object reference member of 'out' "
			"variable struct is NIL\n", msgpfx);
		failed = true;
	} else {
		int32_t		act_id, exp_id;
		Environment	e;

		act_id = var.m_obj->id(e);
		if (e.exception()) {
			os::printf("FAIL: %s object reference member of "
			    "'out' variable struct had exception in id()\n",
			    msgpfx);
			e.clear();
			failed = true;
		}
		exp_id = objref->id(e);
		if (e.exception()) {
			os::printf("FAIL: %s object reference had exception "
			    "in id()\n", msgpfx);
			e.clear();
			failed = true;
		}
		if (act_id != exp_id) {
			os::printf("FAIL: %s object reference member of "
				"'out' variable struct:\n", msgpfx);
			os::printf("\texpected id = %d, actual id = %d\n",
				exp_id, act_id);
			failed = true;
		}
	}

	// Verify char array
	if (var.m_char_arr[0] != 'c' ||	var.m_char_arr[1] != 'd') {
		os::printf("FAIL: %s char array member of "
			"'out' variable struct:\n", msgpfx);
		os::printf("\texpected cd, actual %c%c\n",
			var.m_char_arr[0], var.m_char_arr[1]);
		failed = true;
	}

	return (!failed);
}


//
// Sets up (or verifies if "set" argument is false) incoming 'inout' parameter.
//
bool
set_io_i(hatypetest::Vstruct &var, uint32_t len, hatypetest::Obj_ptr objref,
		const char *msgpfx, bool set)
{
	const uint8_t	base_1byte = 0xF3;
	char		base_char = ~0;
	uint32_t	i;
	bool		failed = false;

	if (set) {
		var.m_1byte = base_1byte;

		var.m_str = new char[len + 1];
		for (i = 0; i < len; ++i) {
			if (base_char == 0)
				base_char = ~0;
			var.m_str[i] = base_char;
			base_char <<= 1;
		}
		var.m_str[i] = '\0';

		var.m_obj = hatypetest::Obj::_duplicate(objref);

		var.m_char_arr[0] = 'e';
		var.m_char_arr[1] = 'f';

		return (true);
	}

	// Verify 1-byte member.
	if (var.m_1byte != base_1byte) {
		os::printf("FAIL: %s 1-byte member of 'inout' "
			"variable struct:\n", msgpfx);
		os::printf("\texpected = 0x%x, actual = 0x%x\n",
			base_1byte, var.m_1byte);
		failed = true;
	}

	// Verify string member.
	for (i = 0; i < len; ++i) {
		if (base_char == 0)
			base_char = ~0;
		if (var.m_str[i] == base_char) {
			base_char <<= 1;
			continue;
		}

		os::printf("FAIL: %s char at index %d of string member of "
			"'inout' variable struct:\n", msgpfx, i);
		os::printf("\texpected = 0x%x, actual = 0x%x\n",
			base_char, var.m_str[i]);
		failed = true;
		break;
	}
	if (var.m_str[i] != '\0') {
		os::printf("FAIL: %s string member of 'inout' "
			"variable struct not terminated at index %d:\n",
			msgpfx, i);
		failed = true;
	}

	// Verify object reference member.
	if (CORBA::is_nil(var.m_obj)) {
		os::printf("FAIL: %s object reference member of 'inout' "
			"variable struct is NIL\n", msgpfx);
		failed = true;
	} else {
		int32_t		act_id, exp_id;
		Environment	e;

		act_id = var.m_obj->id(e);
		if (e.exception()) {
			os::printf("FAIL: %s object reference member of "
			    "'inout' variable struct had exception in id()\n",
			    msgpfx);
			e.clear();
			failed = true;
		}
		exp_id = objref->id(e);
		if (e.exception()) {
			os::printf("FAIL: %s object reference had exception "
			    "in id()\n", msgpfx);
			e.clear();
			failed = true;
		}
		if (act_id != exp_id) {
			os::printf("FAIL: %s object reference member of "
				"'inout' variable struct:\n", msgpfx);
			os::printf("\texpected id = %d, actual id = %d\n",
				exp_id, act_id);
			failed = true;
		}
	}

	// Verify char array
	if (var.m_char_arr[0] != 'e' ||	var.m_char_arr[1] != 'f') {
		os::printf("FAIL: %s char array member of "
			"'inout' variable struct:\n", msgpfx);
		os::printf("\texpected ef, actual %c%c\n",
			var.m_char_arr[0], var.m_char_arr[1]);
		failed = true;
	}

	return (!failed);
}


//
// Sets up (or verifies if "set" argument is false) outgoing 'inout' parameter.
//
bool
set_io_o(hatypetest::Vstruct &var, uint32_t len, hatypetest::Obj_ptr objref,
		const char *msgpfx, bool set)
{
	const uint8_t	base_1byte = 0x3F;
	char		base_char = 1;
	uint32_t	i;
	bool		failed = false;

	if (set) {
		var.m_1byte = base_1byte;

		var.m_str = new char[len + 3];
		for (i = 0; i < len + 2; ++i) {
			if (base_char == 0)
				base_char = ~0;
			var.m_str[i] = base_char;
			base_char >>= 1;
		}
		var.m_str[i] = '\0';

		var.m_obj = hatypetest::Obj::_duplicate(objref);

		var.m_char_arr[0] = 'g';
		var.m_char_arr[1] = 'h';

		return (true);
	}

	// Verify 1-byte member.
	if (var.m_1byte != base_1byte) {
		os::printf("FAIL: %s 1-byte member of 'inout' "
			"variable struct:\n", msgpfx);
		os::printf("\texpected = 0x%x, actual = 0x%x\n",
			base_1byte, var.m_1byte);
		failed = true;
	}

	// Verify string member.
	for (i = 0; i < len + 2; ++i) {
		if (base_char == 0)
			base_char = ~0;
		if (var.m_str[i] == base_char) {
			base_char >>= 1;
			continue;
		}

		os::printf("FAIL: %s char at index %d of string member of "
			"'inout' variable struct:\n", msgpfx, i);
		os::printf("\texpected = 0x%x, actual = 0x%x\n",
			base_char, var.m_str[i]);
		failed = true;
		break;
	}
	if (var.m_str[i] != '\0') {
		os::printf("FAIL: %s string member of 'inout' "
			"variable struct not terminated at index %d:\n",
			msgpfx, i);
		failed = true;
	}

	// Verify object reference member.
	if (CORBA::is_nil(var.m_obj)) {
		os::printf("FAIL: %s object reference member of 'inout' "
			"variable struct is NIL\n", msgpfx);
		failed = true;
	} else {
		int32_t		act_id, exp_id;
		Environment	e;

		act_id = var.m_obj->id(e);
		if (e.exception()) {
			os::printf("FAIL: %s object reference member of "
			    "'inout' variable struct had exception in id()\n",
			    msgpfx);
			e.clear();
			failed = true;
		}
		exp_id = objref->id(e);
		if (e.exception()) {
			os::printf("FAIL: %s object reference had exception "
			    "in id()\n", msgpfx);
			e.clear();
			failed = true;
		}
		if (act_id != exp_id) {
			os::printf("FAIL: %s object reference member of "
				"'inout' variable struct:\n", msgpfx);
			os::printf("\texpected id = %d, actual id = %d\n",
				exp_id, act_id);
			failed = true;
		}
	}

	// Verify char array
	if (var.m_char_arr[0] != 'g' ||	var.m_char_arr[1] != 'h') {
		os::printf("FAIL: %s char array member of "
			"'inout' variable struct:\n", msgpfx);
		os::printf("\texpected gh, actual %c%c\n",
			var.m_char_arr[0], var.m_char_arr[1]);
		failed = true;
	}

	return (!failed);
}


//
// Sets up (or verifies if "set" argument is false) returned value.
//
bool
set_ret(hatypetest::Vstruct &var, uint32_t len, hatypetest::Obj_ptr objref,
		const char *msgpfx, bool set)
{
	const uint8_t	base_1byte = 0xAA;
	char		base_char = 1;
	uint32_t	i;
	bool		failed = false;

	if (set) {
		var.m_1byte = base_1byte;

		var.m_str = new char[len + 1];
		for (i = 0; i < len; ++i) {
			var.m_str[i] = base_char;
			base_char = ~base_char;
		}
		var.m_str[i] = '\0';

		var.m_obj = hatypetest::Obj::_duplicate(objref);

		var.m_char_arr[0] = 'i';
		var.m_char_arr[1] = 'j';

		return (true);
	}

	// Verify 1-byte member.
	if (var.m_1byte != base_1byte) {
		os::printf("FAIL: %s 1-byte member of returned "
			"variable struct:\n", msgpfx);
		os::printf("\texpected = 0x%x, actual = 0x%x\n",
			base_1byte, var.m_1byte);
		failed = true;
	}

	// Verify string member.
	for (i = 0; i < len; ++i) {
		if (var.m_str[i] == base_char) {
			base_char = ~base_char;
			continue;
		}

		os::printf("FAIL: %s char at index %d of string member of "
			"returned variable struct:\n", msgpfx, i);
		os::printf("\texpected = 0x%x, actual = 0x%x\n",
			base_char, var.m_str[i]);
		failed = true;
		break;
	}
	if (var.m_str[i] != '\0') {
		os::printf("FAIL: %s string member of returned "
			"variable struct not terminated at index %d:\n",
			msgpfx, i);
		failed = true;
	}

	// Verify object reference member.
	if (CORBA::is_nil(var.m_obj)) {
		os::printf("FAIL: %s object reference member of returned "
			"variable struct is NIL\n", msgpfx);
		failed = true;
	} else {
		int32_t		act_id, exp_id;
		Environment	e;

		act_id = var.m_obj->id(e);
		if (e.exception()) {
			os::printf("FAIL: %s object reference member of "
			    "returned variable struct had exception in id()\n",
			    msgpfx);
			e.clear();
			failed = true;
		}
		exp_id = objref->id(e);
		if (e.exception()) {
			os::printf("FAIL: %s object reference had exception "
			    "in id()\n", msgpfx);
			e.clear();
			failed = true;
		}
		if (act_id != exp_id) {
			os::printf("FAIL: %s object reference member of "
				"returned variable struct:\n", msgpfx);
			os::printf("\texpected id = %d, actual id = %d\n",
				exp_id, act_id);
			failed = true;
		}
	}

	// Verify char array
	if (var.m_char_arr[0] != 'i' ||	var.m_char_arr[1] != 'j') {
		os::printf("FAIL: %s char array member of "
			"returned variable struct:\n", msgpfx);
		os::printf("\texpected ij, actual %c%c\n",
			var.m_char_arr[0], var.m_char_arr[1]);
		failed = true;
	}

	return (!failed);
}
