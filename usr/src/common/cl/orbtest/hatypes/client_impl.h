/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _HATYPES_CLIENT_IMPL_H
#define	_HATYPES_CLIENT_IMPL_H

#pragma ident	"@(#)client_impl.h	1.9	08/05/20 SMI"

#ifdef ORBTEST_V1
#include <h/hatypetest_1.h>
#else
#include <h/hatypetest_0.h>
#endif

#include <orb/object/adapter.h>
#include <h/streams.h>
#include <orbtest/hatypes/impl_common.h>

//
// Test client implementation.
//
class client_impl : public McServerof<hatypetest::client>, public impl_common {
public:
	client_impl();
	~client_impl();
	void	_unreferenced(unref_t);

	// Initialize implementation object.
	// Returns: true if successful, false otherwise.
	bool	init();

	// Clients invoke this when they're finished with this object.
	void	done(Environment &e);

	//
	// These functions perform tests on the test client side.
	// Each is invoked by driver "programs" and, in turn, invokes
	// the corresponding method on the test server side.
	// Parameters:
	//	server_ref  --	test server object to invoke.
	//	repeat_num  --	how many times to run the test.
	//	len	    --	for string/sequence tests, specifies
	//			the length, in bytes (for strings,
	//			does not include the terminating NUL),
	//			to test.
	//	len, maxlen --	for sequence of strings/sequences tests,
	//			len specifies the length of the
	//			enclosing sequence, maxlen specifies the
	//			maximum length of the enclosed
	//			string/sequence (each will have length
	//			one longer than the previous).
	// Returns:
	//	Total test execution time.
	//
	bool test_versioning_1(
				hatypetest::server_ptr server_ref,
				Environment &e);
#ifdef ORBTEST_V1
	bool test_versioning_2(
				hatypetest::server_ptr server_ref,
				Environment &);
	bool test_versioning_3(Environment &);
#endif
	hatypetest::server_ptr test_upgrade(hatypetest::server_ptr server_ref,
				Environment &);

	int64_t	test_noarg(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);

	int64_t	test_boolean(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);
	int64_t	test_char(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);
	int64_t	test_octet(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);
	int64_t	test_short(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);
	int64_t	test_ushort(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);
	int64_t	test_long(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);
	int64_t	test_ulong(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);
	int64_t	test_longlong(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);
	int64_t	test_ulonglong(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);

	int64_t	test_bstring(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				bool noverify,
				Environment &e);
	int64_t	test_ustring(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				bool noverify,
				Environment &e);

	int64_t	test_bseq(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				bool noverify,
				Environment &e);
	int64_t	test_useq(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				bool noverify,
				Environment &e);

	int64_t	test_seq_read(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				bool noverify,
				Environment &e);

	int64_t	test_seq_write(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				bool noverify,
				Environment &e);

	int64_t	test_seq_rw(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				bool noverify,
				Environment &e);

	int64_t	test_bseq_useq(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				uint32_t maxlen,
				bool noverify,
				Environment &e);
	int64_t	test_useq_bseq(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				uint32_t maxlen,
				bool noverify,
				Environment &e);

	int64_t	test_bseq_ustring(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				uint32_t maxlen,
				bool noverify,
				Environment &e);
	int64_t	test_useq_bstring(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				uint32_t maxlen,
				bool noverify,
				Environment &e);

	int64_t	test_fstruct(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);
	int64_t	test_vstruct(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				bool noverify,
				Environment &e);

	int64_t	test_fstructseq(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				bool noverify,
				Environment &_environment);

	int64_t test_nullseq(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				bool noverify,
				Environment &_environment);

	int64_t test_0lenseq(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				bool noverify,
				Environment &_environment);

	int64_t	test_funion(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);

	int64_t	test_vunion(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				bool noverify,
				Environment &e);

	int64_t	test_obj(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);

	int64_t	test_objinhsimple(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);

	int64_t	test_objinhmany(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);

	int64_t	test_objinhmultiple(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);

	int64_t	test_objseq(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				bool noverify,
				Environment &e);

	// The time value is not important for revoke
	int64_t test_revoke(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);


	int64_t	test_arrayshort(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);

	int64_t	test_arrayoctet(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);

	int64_t	test_arrayfstruct(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);

	int64_t	test_arrayfunion(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);

	int64_t	test_emptyexception(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);

	int64_t	test_basicexception(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);

	int64_t	test_complexexception(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);

	int64_t	test_stringexception(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);

#if !defined(_UNODE)
	int64_t	test_bulkio_uio_in(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				bool noverify,
				Environment &e);

	int64_t	test_bulkio_uio_inout(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				bool noverify,
				Environment &e);

	int64_t	test_bulkio_pages_in(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t noofpages,
				bool noverify,
				Environment &e);

	int64_t	test_bulkio_pages_inout(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t noofpages,
				bool noverify,
				Environment &e);
#endif // _UNODE

	int64_t	test_cred(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);

	int64_t	test_data_container(
				hatypetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				bool noverify,
				Environment &e);

private:
	bool	done_called;
	bool	init_called;
};

#endif /* _HATYPES_CLIENT_IMPL_H */
