/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)khatypetest_server.cc	1.7	08/05/20 SMI"

#include <sys/types.h>
#include <sys/modctl.h>
#include <sys/errno.h>

#include <sys/os.h>
#include <nslib/ns.h>
#include <cplplrt/cplplrt.h>

#include <orbtest/hatypes/server_impl.h>

// inter-module dependencies
#ifdef ORBTEST_V1
char _depends_on[] = "misc/cl_runtime misc/cl_load misc/cl_orb "
	"misc/cl_haci misc/cl_quorum misc/cl_comm misc/khatypetest_v1";
#else
char _depends_on[] = "misc/cl_runtime misc/cl_laod misc/cl_orb "
	"misc/cl_haci misc/cl_quorum misc/cl_comm misc/khatypetest_v0";
#endif

extern struct mod_ops mod_miscops;

static struct modlmisc modlmisc = {
	&mod_miscops, "HA IDL Types Test Server",
};

static struct modlinkage modlinkage = {
	MODREV_1, { (void *)&modlmisc, NULL }
};

int
_init(void)
{
	int error;

	if ((error = mod_install(&modlinkage)) != 0) {
		return (error);
	}
	_cplpl_init();				// C++ initialization

	if (start_ha_service(hatypes_kserver_name)) {
		return (EINVAL);
	}

	os::printf("KSERVER: Service: \"%s\" READY\n", hatypes_kserver_name);
	return (0);
}

int
_fini(void)
{
	os::printf("KSERVER: Calling _fini()\n");
	_cplpl_fini();
	return (mod_remove(&modlinkage));
}

int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}
