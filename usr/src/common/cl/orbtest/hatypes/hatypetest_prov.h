/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _HATYPETEST_PROV_H
#define	_HATYPETEST_PROV_H

#pragma ident	"@(#)hatypetest_prov.h	1.10	08/05/20 SMI"

#include <sys/os.h>

#ifdef ORBTEST_V1
#include <h/hatypetest_1.h>
#else
#include <h/hatypetest_0.h>
#endif

#include <orb/object/adapter.h>
#include <h/data_container.h>
#include <h/repl_rm.h>
#include <h/streams.h>
#include <h/solobj.h>
#include <repl/service/replica_tmpl.h>
#include <repl/service/transaction_state.h>

// Forward Declarations
class server_impl;
class hatypetest_prov;

typedef struct prov_in_mod {
	hatypetest_prov *provp;
	int		 mod_id;
} prov_in_mod_t;

class hatypetest_prov : public repl_server<hatypetest::ckpt> {

public:
	hatypetest_prov(const char *svc_desc, const char *repl_desc);
	~hatypetest_prov();

	//
	// IDL Methods for replica::repl_prov methods
	//
	void become_secondary(Environment &e);
	void add_secondary(
		replica::checkpoint_ptr sec,
		const char *sec_name,
		Environment &e);
	void remove_secondary(const char *secondary_name, Environment &e);
	void freeze_primary_prepare(Environment &e);
	void freeze_primary(Environment &e);
	void unfreeze_primary(Environment &e);
	void become_primary(
		const replica::repl_name_seq &secondary_names,
		Environment &e);
	void become_spare(Environment &e);
	void shutdown(Environment &e);
	CORBA::Object_ptr get_root_obj(Environment &e);

	//
	// IDL methods for hatypetest::checkpoint
	//
	void ckpt_new_server(hatypetest::server_ptr p_server, Environment &e);
	void ckpt_init_called(bool new_value, Environment &);
	void ckpt_done_called(bool new_value, Environment &);
	void ckpt_verify_off(bool new_value, Environment &);

	// Checkpoint methods for object creation.
	void ckpt_create_minverObjA(hatypetest::minverObjA_ptr objref,
	    Environment &e);

#ifdef ORBTEST_V1
	void ckpt_create_minverObjB(hatypetest::minverObjB_ptr objref,
	    Environment &e);
	void ckpt_create_minverObjC(hatypetest::minverObjC_ptr objref,
	    Environment &e);
	void ckpt_create_combinedObjD(CORBA::Object_ptr objref,
	    Environment &e);
	void ckpt_service_version(int16_t new_version, Environment &);
#endif // ORBTEST_V1

	//
	// Callback to ensure done_called value change gets checkpointed
	// to any secondaries
	//
	void	done_called_changed(Environment &);

	//
	// Callback to ensure verify_off value change gets checkpointed
	// to any secondaries
	//
	void	verify_off_changed(Environment &);

	//
	// Upgrade callback method.  The server_impl object invokes this
	// to simulate the version manager telling the server object
	// that the running version of the cluster has changed.
	//
	void upgrade_callback(Environment &);

	//
	// Interface to be invoked during ORB::join_cluster to update the
	// local ns of a rejoining node
	//
	static int get_root_to_local();

	server_impl *serverp;
	hatypetest::server_var server_v;
	int16_t service_version;

	// Checkpoint accessor function.
	hatypetest::ckpt_ptr get_hatypetest_checkpoint();

private:
	// Checkpoint proxy.
	hatypetest::ckpt_var	_ckpt_proxy;

#ifdef ORBTEST_V1
	//
	// This lock protects 'service_version', _ckpt_proxy and
	// provides locking between upgrade callbacks and become_primary().
	// It needs to be a rwlock since we make checkpoint calls while
	// holding the lock.
	//
	os::rwlock_t version_lock;
#endif
};

#endif /* _HATYPETEST_PROV_H */
