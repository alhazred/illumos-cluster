/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)client_impl.cc	1.7	08/05/20 SMI"

#include <sys/types.h>

#if !defined(_KERNEL)
#include <unistd.h>
#endif

#include <sys/os.h>
#include <nslib/ns.h>

#include <orbtest/hatypes/client_impl.h>

client_impl::client_impl() :
	done_called(false), init_called(false)
{
#if defined(_KERNEL)
	msg_prefix("kernel client:");
#elif defined(_KERNEL_ORB)
	msg_prefix("unode client:");
#else
	msg_prefix("user client:");
#endif
}

client_impl::~client_impl()
{
	os::printf("client_impl::~client_impl()\n");
}

void
client_impl::_unreferenced(unref_t cookie)
{
	if (_last_unref(cookie))
		unref_called(true);
}

//
// Initialize the implementation object.
//
bool
client_impl::init()
{
	// Ignore if called more than once.
	if (init_called)
		return (true);

	// Register with the name server.
	hatypetest::client_var tmpref = get_objref();
#if defined(_KERNEL)
	init_called = register_obj(tmpref, hatypes_kclient_name);
#elif defined(_KERNEL_ORB)
	init_called = register_obj(tmpref, hatypes_unclient_name);
#else
	init_called = register_obj(tmpref, hatypes_uclient_name);
#endif
	return (init_called);
}

//
// Clients invoke this when they're finished with this object.
//
void
client_impl::done(Environment &)
{
	// Ignore if invoked more than once.
	if (done_called)
		return;

	// Unregister from the name server.
	if (unregister_obj())
		done_called = true;
}
