/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _IMPL_COMMON_H
#define	_IMPL_COMMON_H

#pragma ident	"@(#)impl_common.h	1.9	08/05/20 SMI"

#include <orb/invo/common.h>
#include <sys/os.h>

#ifdef ORBTEST_V1
#include <h/hatypetest_1.h>
#else
#include <h/hatypetest_0.h>
#endif

#include <orb/object/adapter.h>

#if defined(_KERNEL_ORB) && !defined(_KERNEL)
#define	_UNODE
#endif

//
// Names registered with the name server.
//
extern const char	hatypes_kserver_name[];		// kernel-land server
extern const char	hatypes_unserver_name[];	// unode server
extern const char	hatypes_uclient_name[];		// user-land client
extern const char	hatypes_kclient_name[];		// kernel-land client
extern const char	hatypes_unclient_name[];	// unode client

#ifdef ORBTEST_V1
extern const char	hatypes_kserver_v0_name[];	// massaged reference
#endif

const char Akserver_name[] = "IDL Versioning test server A (kernel-land)";
#ifdef ORBTEST_V1
const char Bkserver_name[]  = "IDL Versioning test server B (kernel-land)";
const char Ckserver_name[]  = "IDL Versioning test server C (kernel-land)";
const char Dkserver_name[]  = "IDL Versioning test server D (kernel-land)";
#endif

//
// For object reference testing.
//
extern const int32_t	hatypes_in_obj_id;
extern const int32_t	hatypes_inout_obj_id;

//
// Check Environment.
// Returns: true if no exception, false otherwise.
// Side Effects: prints the exception if there is one.
//
bool hatypes_env_ok(Environment &e, const char *msg_prefix);

//
// Verify that the Environment contains the expected exception.
// Returns: true if the expected exception exists, false otherwise.
// Side Effects: prints the exception if doesn't match the expected one.
//
bool hatypes_env_ok(Environment &e, CORBA::Exception &expected,
			const char *msg_prefix);

//
// Get an object reference with the specified string name from the name server.
// Returns: CORBA::Object::_nil() if the object isn't found and wait == false
//	or an error occurs.
//	If wait is true, then get_obj waits till the object is found in the
//		name server or an error occurs.
//
CORBA::Object_ptr hatypes_get_obj(const char *, bool quiet = false,
					bool wait = true);

//
// Common (non-exported) methods for implementation classes.
//
class impl_common {
public:
	//
	// Suspend calling thread until _unreferenced() is called.
	// Returns: true if _unreferenced() has been called.
	//	    false if timeout arg (in seconds) is nonzero and it
	//	    expires before _unreferenced() is called.
	//
	bool	wait_until_unreferenced(uint32_t timeout = 0);

	bool	unref_called() { return (unref_called_); }

	//
	// Register implementation object with the name server.
	// Returns: true if successful, false otherwise.
	//
	bool	register_obj(CORBA::Object_ptr objref, const char *name);

	//
	// Unregister implementation object from the name server.
	// Returns: true if successful, false otherwise.
	//
	bool	unregister_obj();

protected:
	// Protected so this class can be used only through derivation.
	impl_common();
	virtual ~impl_common();

	// Returns the string name registered with the name server.
	const char	*strname() { return (strname_); }

	void		msg_prefix(const char *s) { msg_prefix_ = s; }
	const char	*msg_prefix() { return (msg_prefix_); }

	void		unref_called(bool b);

private:
	const char	*msg_prefix_;
	bool		unref_called_;

	const char	*strname_;	// name to be registered with ns
	os::mutex_t	mutex;
	os::condvar_t	condvar;
};

//
// Object implementation used for object reference tests.
//
class Obj_impl : public McServerof<hatypetest::Obj>, public impl_common {
public:
	int32_t	idnum;

	Obj_impl() : idnum(0) {}
	Obj_impl(int32_t l) : idnum(l) {}
	~Obj_impl() {}

	void	_unreferenced(unref_t);
	void	set_id(int32_t l) { idnum = l; }

	// Interface operations.
	int32_t	id(Environment &) { return (idnum); }
};

class InhObjA_impl : public McServerof<hatypetest::InhObjA>,
			public impl_common {
public:
	int32_t	idnum;

	InhObjA_impl() : idnum(0) {}
	InhObjA_impl(int32_t l) : idnum(l) {}
	~InhObjA_impl() {}

	void	_unreferenced(unref_t);
	void 	set_id(int32_t l) { idnum = l; }

	// Interface operations.
	int32_t	id(Environment &) { return (idnum); }
	char	name(Environment &) { return ('A'); }
};

class InhObjB_impl : public McServerof<hatypetest::InhObjB>,
			public impl_common {
public:
	int32_t	idnum;

	InhObjB_impl() : idnum(0) {}
	InhObjB_impl(int32_t l) : idnum(l) {}
	~InhObjB_impl() {}

	void	_unreferenced(unref_t);
	void 	set_id(int32_t l) { idnum = l; }

	// Interface operations.
	int32_t	id(Environment &) { return (idnum); }
	char 	name(Environment &) { return ('B'); }
};

class InhObjC_impl : public McServerof<hatypetest::InhObjC>,
			public impl_common {
public:
	int32_t	idnum;

	InhObjC_impl() : idnum(0) {}
	InhObjC_impl(int32_t l) : idnum(l) {}
	~InhObjC_impl() {}

	void	_unreferenced(unref_t);
	void 	set_id(int32_t l) { idnum = l; }

	// Interface operations.
	int32_t	id(Environment &) { return (idnum); }
	char 	name(Environment &) { return ('C'); }
};

class InhObjD_impl : public McServerof<hatypetest::InhObjD>,
			public impl_common {
public:
	int32_t	idnum;

	InhObjD_impl() : idnum(0) {}
	InhObjD_impl(int32_t l) : idnum(l) {}
	~InhObjD_impl() {}

	void	_unreferenced(unref_t);
	void 	set_id(int32_t l) { idnum = l; }

	// Interface operations.
	int32_t	id(Environment &) { return (idnum); }
	char 	name(Environment &) { return ('D'); }
};

class InhObjE_impl : public McServerof<hatypetest::InhObjE>,
			public impl_common {
public:
	int32_t	idnum;

	InhObjE_impl() : idnum(0) {}
	InhObjE_impl(int32_t l) : idnum(l) {}
	~InhObjE_impl() {}

	void	_unreferenced(unref_t);
	void 	set_id(int32_t l) { idnum = l; }

	// Interface operations.
	int32_t	id(Environment &) { return (idnum); }
	char 	name(Environment &) { return ('E'); }
};

//
// Verifies that an object has been unreferenced.
//
bool verify_unreferenced(Obj_impl* obj_implp, const char *msgpfx);

#endif /* _IMPL_COMMON_H */
