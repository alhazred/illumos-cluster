/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _HATYPES_SERVER_IMPL_H
#define	_HATYPES_SERVER_IMPL_H

#pragma ident	"@(#)server_impl.h	1.9	08/05/20 SMI"

#include <sys/os.h>

#ifdef ORBTEST_V1
#include <h/hatypetest_1.h>
#else
#include <h/hatypetest_0.h>
#endif

#include <orb/object/adapter.h>
#include <h/data_container.h>
#include <h/repl_rm.h>
#include <h/streams.h>
#include <h/solobj.h>
#include <repl/service/replica_tmpl.h>
#include <repl/service/transaction_state.h>
#include <orbtest/hatypes/impl_common.h>
#include <orbtest/hatypes/idlverObj_impl.h>

//
// Test server implementation.
//
class server_impl : public mc_replica_of<hatypetest::server>,
			public impl_common {
public:
	server_impl(hatypetest_prov *server);
	server_impl(hatypetest::server_ptr ref, hatypetest_prov *server);
	~server_impl();

	void	_unreferenced(unref_t);

	int16_t	version(Environment &);

	// Initialize implementation object.
	// Returns: true if successful, false otherwise.
	bool	init();

	// Clients invoke this when they're finished with this object.
	void	done(Environment &e);

	// Set the no verify flag on the server side (associated with the
	// -noverify option
	void 	set_noverify(Environment &e);

	//
	// Upgrade callback method.  The driver program invokes this
	// to simulate the version manager telling the server object
	// that the running version of the clsuter has changed
	//
	void upgrade_callback(Environment &);

#ifdef ORBTEST_V1
	int16_t getid(Environment &);
#endif

	// Test method for no-argument invocation.
	void	test_noarg(Environment &e);

	//
	// Test methods for IDL basic types.
	// (There are no float and double tests since Solaris doesn't support
	// floating point arithmetic in kernel.)
	//
	bool test_boolean(bool in, bool &out, bool &inout, Environment &e);
	int8_t test_char(int8_t in, int8_t &out, int8_t &inout, Environment &e);
	uint8_t	test_octet(uint8_t in, uint8_t &out, uint8_t &inout,
				Environment &e);

	int16_t	test_short(int16_t in, int16_t &out, int16_t &inout,
				Environment &e);

	uint16_t test_ushort(uint16_t in, uint16_t &out, uint16_t &inout,
				Environment &e);

	int32_t	test_long(int32_t in, int32_t &out, int32_t &inout,
				Environment &e);

	uint32_t test_ulong(uint32_t in, uint32_t &out, uint32_t &inout,
				Environment &e);

	int64_t	test_longlong(int64_t in, int64_t &out, int64_t &inout,
				Environment &e);

	uint64_t test_ulonglong(uint64_t in, uint64_t &out, uint64_t &inout,
				Environment &e);

	//
	// Test methods for bounded (hatypetest::Bstring) and
	// unbounded (hatypetest::Ustring) strings.
	//
	hatypetest::Bstring	test_bstring(
					const hatypetest::Bstring &in,
					hatypetest::Bstring_out    out,
					hatypetest::Bstring &inout,
					uint32_t len,
					Environment &e);
	hatypetest::Ustring	test_ustring(
					const hatypetest::Ustring in,
					CORBA::String_out out,
					hatypetest::Ustring &inout,
					uint32_t len,
					Environment &e);

	//
	// Test methods for bounded and unbounded sequences.
	//
	hatypetest::Bseq	*test_bseq(
				const hatypetest::Bseq &in,
				hatypetest::Bseq_out outp,
				hatypetest::Bseq &inout,
				uint32_t len,
				Environment &e);
	hatypetest::Useq	*test_useq(
				const hatypetest::Useq &in,
				hatypetest::Useq_out outp,
				hatypetest::Useq &inout,
				uint32_t len,
				Environment &e);

	void		test_seq_read(
				hatypetest::UBseq_out outp,
				uint32_t len,
				Environment &e);

	void		test_seq_write(
				const hatypetest::UBseq &in,
				uint32_t len,
				Environment &e);

	void		test_seq_rw(
				hatypetest::UBseq &inout,
				uint32_t len,
				Environment &e);

	hatypetest::BseqUseq	*test_bseq_useq(
					const hatypetest::BseqUseq &in,
					hatypetest::BseqUseq_out outp,
					hatypetest::BseqUseq &inout,
					uint32_t len,
					uint32_t maxlen,
					Environment &e);
	hatypetest::UseqBseq	*test_useq_bseq(
					const hatypetest::UseqBseq &in,
					hatypetest::UseqBseq_out outp,
					hatypetest::UseqBseq &inout,
					uint32_t len,
					uint32_t maxlen,
					Environment &e);

	hatypetest::BseqUstring	*test_bseq_ustring(
					const hatypetest::BseqUstring &in,
					hatypetest::BseqUstring_out outp,
					hatypetest::BseqUstring &inout,
					uint32_t len,
					uint32_t maxlen,
					Environment &e);
	hatypetest::UseqBstring	*test_useq_bstring(
					const hatypetest::UseqBstring &in,
					hatypetest::UseqBstring_out outp,
					hatypetest::UseqBstring &inout,
					uint32_t len,
					uint32_t maxlen,
					Environment &e);

	//
	// Tests for fixed (hatypetest::Fstruct) and variable
	// (hatypetest::Vstruct) structures.
	//
	void			test_fstruct(
					const hatypetest::Fstruct &in,
					hatypetest::Fstruct &out,
					hatypetest::Fstruct &inout,
					Environment &e);
	hatypetest::Vstruct	*test_vstruct(
					const hatypetest::Vstruct &in,
					hatypetest::Vstruct_out outp,
					hatypetest::Vstruct &inout,
					uint32_t len,
					hatypetest::Obj_ptr objref,
					Environment &e);

	hatypetest::Fstructseq*	test_fstructseq(
					const hatypetest::Fstructseq &i,
					hatypetest::Fstructseq_out o,
					hatypetest::Fstructseq &io,
					uint32_t len,
					Environment &_environment);

	hatypetest::Useq	*test_nullseq(const hatypetest::Useq &i,
					hatypetest::Useq_out o,
					hatypetest::Useq &io,
					uint32_t len,
					Environment &_environment);
	hatypetest::Useq 	*test_0lenseq(const hatypetest::Useq &i,
					hatypetest::Useq_out o,
					hatypetest::Useq &io,
					uint32_t len,
					Environment &_environment);

	//
	// Tests for fixed (hatypetest::Funion) and variable
	// (hatypetest::Vunion) unions.
	//
	void			test_funion(
					const hatypetest::Funion &in,
					hatypetest::Funion &out,
					hatypetest::Funion &inout,
					hatypetest::FDiscrim discrim,
					Environment &e);
	hatypetest::Vunion	*test_vunion(
					const hatypetest::Vunion &in,
					hatypetest::Vunion_out outp,
					hatypetest::Vunion &inout,
					hatypetest::VDiscrim discrim,
					uint32_t len,
					hatypetest::Obj_ptr objref,
					Environment &e);

	//
	// Object reference tests.
	//
	hatypetest::Obj_ptr	test_obj(
					hatypetest::Obj_ptr  in,
					hatypetest::Obj_out  out,
					hatypetest::Obj_ptr &inout,
					Environment &e);

	hatypetest::InhObjA_ptr	test_objinhsimple(
					hatypetest::InhObjA_ptr  in,
					hatypetest::InhObjA_out  out,
					hatypetest::InhObjA_ptr &inout,
					Environment &e);

	hatypetest::InhObjA_ptr	test_objinhmany(
					hatypetest::InhObjA_ptr  in,
					hatypetest::InhObjA_out  out,
					hatypetest::InhObjA_ptr &inout,
					Environment &e);

	hatypetest::InhObjA_ptr	test_objinhmultiple(
					hatypetest::InhObjA_ptr  in,
					hatypetest::InhObjA_out  out,
					hatypetest::InhObjA_ptr &inout,
					Environment &e);

	hatypetest::Objseq	*test_objseq(
					const hatypetest::Objseq &in,
					hatypetest::Objseq_out outp,
					hatypetest::Objseq &inout,
					uint32_t len,
					Environment &e);

	// Test method for arrays
	void	test_arrayshort(
			const hatypetest::arrayShort in,
			hatypetest::arrayShort out,
			hatypetest::arrayShort inout,
			Environment &e);

	void	test_arrayoctet(
			const hatypetest::arrayOctet in,
			hatypetest::arrayOctet out,
			hatypetest::arrayOctet inout,
			Environment &e);

	void	test_arrayfstruct(
			const hatypetest::arrayFstruct in,
			hatypetest::arrayFstruct out,
			hatypetest::arrayFstruct inout,
			Environment &e);

	void	test_arrayfunion(
			const hatypetest::arrayFunion in,
			hatypetest::arrayFunion out,
			hatypetest::arrayFunion inout,
			Environment &e);

	void	test_emptyexception(
			Environment &e);

	void	test_basicexception(
			Environment &e);

	void	test_complexexception(
			hatypetest::Obj_ptr objref,
			uint32_t len,
			Environment &e);

	void	test_stringexception(
			Environment &e);

#if !defined(_UNODE)
	void	test_bulkio_uio_in(
			bulkio::in_uio_ptr in,
			uint32_t len,
			Environment &);

	void	test_bulkio_uio_inout(
			bulkio::inout_uio_ptr &inout,
			uint32_t len,
			Environment &);

	void	test_bulkio_pages_in(
			bulkio::in_pages_ptr in,
			uint32_t noofpages,
			Environment &);

	void	test_bulkio_pages_inout(
			bulkio::inout_pages_ptr &inout,
			uint32_t noofpages,
			Environment &);
#endif // _UNODE

	// Store client reference
	// void	store_client_ref(
	//		hatypetest::client_ptr client_ref,
	//		Environment &e);

	// Release client reference
	// void	release_client_ref(Environment &e);

	// Cred Object test

	solobj::cred_ptr	test_cred(
			solobj::cred_ptr in,
			solobj::cred_out out,
			solobj::cred_ptr &inout,
			Environment &e);

	// Called by the client to free the memory allocated
	// for the 'out' parameter on the server side.

	void	cred_crfree(Environment &e);

	// Data Container Object test

	data_container::data_ptr	test_data_container(
			data_container::data_ptr in,
			data_container::data_out out,
			data_container::data_ptr &inout,
			uint32_t	length,
			Environment &e);

	bool init_called;
	bool done_called;
	bool verify_off;

	minverObjA_impl	*idlverpA;
#ifdef ORBTEST_V1
	minverObjB_impl *idlverpB;
	minverObjC_impl *idlverpC;
	combinedObjD_impl *idlverpD;
#endif

	//
	// This stuff is for testing that we can use
	// an old checkpoint reference after the
	// upgrade commit has occured.
	//
	bool test_get_old_ckpt_ref(Environment &e);
	bool test_use_old_ckpt_ref(Environment &e);
	hatypetest::ckpt_var	test_ckpt_ref;

private:
	// The provider for which we are an object of
	hatypetest_prov	*my_provider;

	char    flags;

	os::mutex_t lck;	// protects flags and condition variables

	// Used for out parameter (cred object test)
	cred_t *cr_out;

	// Used to store client reference for oneway test
	hatypetest::client_ptr	clnt_ref;
};

int start_ha_service(const char *svc_desc);

#endif /* _HATYPES_SERVER_IMPL_H */
