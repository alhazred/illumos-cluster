/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)khatypetest.cc	1.7	08/05/20 SMI"

#include <sys/types.h>
#include <sys/modctl.h>
#include <sys/errno.h>

#include <sys/os.h>
#include <nslib/ns.h>

#include <orbtest/hatypes/server_impl.h>

//
// This module is a fake one.  Its purpose is to circumvent the way the
// top-level $CODEMGR_WS/src/Makefile builds kernel modules: each module
// is stripped out of its symbols which have been used by previous module(s)
// listed in the MODS variable.  The purpose is to avoid duplicate symbols
// in the kernel.
//
// There are two kernel modules in this set of tests: kserver and kclient.
// They share many templates/symbols causing only one module to be
// successfully built.  This module solves this problem.  It's compiled
// with all the necessary object files, while kserver and kclient are not.
// Both kserver and kclient, on the other hand, depend on this module
// which is automatically loaded by the kernel.
//
// This module must be placed in the /kernel/misc directory.
//


extern "C" {
	void _cplpl_init();
	void _cplpl_fini();
}


// inter-module dependencies

// pxfs needed for cred_object test

char _depends_on[] = "misc/cl_runtime misc/cl_load misc/cl_orb "
	"misc/cl_haci misc/cl_quorum misc/cl_comm misc/cl_dcs fs/pxfs";

extern struct mod_ops mod_miscops;

static struct modlmisc modlmisc = {
	&mod_miscops, "Common module for HA IDL types test server/client",
};

static struct modlinkage modlinkage = {
	MODREV_1, { (void *)&modlmisc, NULL }
};


int
_init(void)
{
	int	error;

	if (error = mod_install(&modlinkage)) {
		return (error);
	}
	_cplpl_init();				// C++ initialization

	return (0);
}


int
_fini(void)
{
	_cplpl_fini();
	return (mod_remove(&modlinkage));
}


int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}
