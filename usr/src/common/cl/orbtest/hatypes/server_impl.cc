/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)server_impl.cc	1.8	08/05/20 SMI"

#include <sys/types.h>

#if !defined(_KERNEL)
#include <stdlib.h>				// for exit()
#endif

#include <nslib/ns.h>

#include <orbtest/hatypes/server_impl.h>
#include <sys/vm_util.h>

static const char vp_name[] = "hatypetest";

//
// This is called from either the unode or kernel module to create and
// start a replica.
// Return true if replica failed to start.
//
int
start_ha_service(const char *svc_desc)
{
	Environment e;
	char prov_desc[10];

	os::sprintf(prov_desc, "%d", orb_conf::node_number());
	os::printf("HA Service: \"%s\" (%s) initialized\n",
	    svc_desc, prov_desc);

	// Create a replica provider.
	hatypetest_prov *prov_ptr =
	    new hatypetest_prov(svc_desc, (const char *)prov_desc);

	// Register the provider with the dependencies specified.
	replica::service_dependencies svc_deps(0, 0);
	replica::prov_dependencies prov_deps(0, 0);
	prov_ptr->register_with_rm(1, &svc_deps, &prov_deps, true, e);
	if (e.exception()) {
		e.exception()->print_exception("register_with_rm:");
		return (1);
	}

	return (0);
}

// ---------------------------------------------------------------------------
// hatypetest_prov implementation
// ---------------------------------------------------------------------------
hatypetest_prov::hatypetest_prov(const char *svc_desc, const char *repl_desc) :
	repl_server<hatypetest::ckpt>(svc_desc, repl_desc),
	serverp(NULL),
	service_version(0)
{
	os::printf("hatypetest_prov::hatypetest_prov()\n");
}


hatypetest_prov::~hatypetest_prov()
{
	os::printf("hatypetest_prov::~hatypetest_prov()\n");

	serverp = NULL;
}


//
// Convert from primary to secondary.
//
void
hatypetest_prov::become_secondary(Environment &e)
{
	os::printf("hatypetest_prov::become_secondary()\n");

	if (e.exception()) {
		e.exception()->print_exception("Exception in "
		    "hatypetest_prov::become_secondary\n");
		e.clear();
	}

	// Release the checkpoint interface pointer.
#ifdef ORBTEST_V1
	version_lock.wrlock();
#endif
	ASSERT(!CORBA::is_nil(_ckpt_proxy));
	_ckpt_proxy = hatypetest::ckpt::_nil();
	server_v = hatypetest::server::_nil();
#ifdef ORBTEST_V1
	version_lock.unlock();
#endif
}


void
hatypetest_prov::add_secondary(replica::checkpoint_ptr sec,
    const char *sec_name, Environment &e)
{
	os::printf("hatypetest_prov::add_secondary() %s\n", sec_name);

	//
	// Checkpoint the creation of the server object.
	//
	os::printf("Checkpointing creation of server object\n");
	hatypetest::ckpt_var my_ckpt = hatypetest::ckpt::_narrow(sec);
	ASSERT(!CORBA::is_nil(server_v));
	my_ckpt->ckpt_new_server(server_v, e);
	if (e.exception()) {
		os::printf("exception\n");
		e.clear();
	}

	my_ckpt->ckpt_init_called(serverp->init_called, e);
	if (e.exception()) {
		os::printf("exception\n");
		e.clear();
	}

	my_ckpt->ckpt_done_called(serverp->done_called, e);
	if (e.exception()) {
		os::printf("exception\n");
		e.clear();
	}

	my_ckpt->ckpt_verify_off(serverp->verify_off, e);
	if (e.exception()) {
		os::printf("exception\n");
		e.clear();
	}

#ifndef ORBTEST_V1
	hatypetest::minverObjA_var newA_ref = serverp->idlverpA->get_objref();
	my_ckpt->ckpt_create_minverObjA(newA_ref, e);
	if (e.exception()) {
		os::printf("exception\n");
		e.clear();
	}
#else
	version_lock.wrlock();
	CORBA::type_info_t *typ =
	    hatypetest::minverObjA::_get_type_info(service_version);
	hatypetest::minverObjA_var newA_ref =
	    serverp->idlverpA->get_objref(typ);
	my_ckpt->ckpt_create_minverObjA(newA_ref, e);
	if (e.exception()) {
		os::printf("exception\n");
		e.clear();
	}
	//
	// If we are running the new protocol, we need to dump state
	// for the new objects.
	//
	if (service_version != 0) {
		if (serverp->idlverpB != NULL) {
			hatypetest::minverObjB_var newB_ref =
			    serverp->idlverpB->get_objref();
			my_ckpt->ckpt_create_minverObjB(newB_ref, e);
			if (e.exception()) {
				os::printf("exception\n");
				e.clear();
			}
		}
		if (serverp->idlverpC != NULL) {
			hatypetest::minverObjC_var newC_ref =
			    serverp->idlverpC->get_objref();
			my_ckpt->ckpt_create_minverObjC(newC_ref, e);
			if (e.exception()) {
				os::printf("exception\n");
				e.clear();
			}
		}
		if (serverp->idlverpD != NULL) {
			hatypetest::combinedObjD_var newD_ref =
			    serverp->idlverpD->get_objref();
			my_ckpt->ckpt_create_combinedObjD(
			    newD_ref->_this_obj(), e);
			if (e.exception()) {
				os::printf("exception\n");
				e.clear();
			}
		}
		my_ckpt->ckpt_service_version(service_version, e);
		if (e.exception()) {
			os::printf("exception\n");
			e.clear();
		}
	}
	version_lock.unlock();
#endif
}


void
hatypetest_prov::remove_secondary(const char *secondary_name, Environment &e)
{
	os::printf("hatypetest_prov::remove_secondary() %s\n", secondary_name);
	if (e.exception()) {
		e.exception()->print_exception("Exception in "
		    "hatypetest_prov::remove_secondary\n");
		e.clear();
	}
}


void
hatypetest_prov::become_primary(const replica::repl_name_seq &secondary_names,
    Environment &e)
{
	if (e.exception()) {
		e.exception()->print_exception("Exception in "
		    "hatypetest_prov::become_primary\n");
		e.clear();
	}

	//
	// Setup our checkpoint object.
	//
#ifdef ORBTEST_V1
	version_lock.wrlock();
	os::printf("hatypetest_prov::become_primary() v%d\n",
	    service_version);
	CORBA::type_info_t *typ =
	    hatypetest::ckpt::_get_type_info(service_version);
	replica::checkpoint_var tmp_ckptp = set_checkpoint(typ);
#else
	os::printf("hatypetest_prov::become_primary()\n");
	replica::checkpoint_var tmp_ckptp =
	    set_checkpoint(hatypetest::ckpt::_get_type_info(0));
#endif
	_ckpt_proxy = hatypetest::ckpt::_narrow(tmp_ckptp);

	ASSERT(!CORBA::is_nil(_ckpt_proxy));

	//
	// If we were a secondary, there is no need to create a "first" server
	// object.
	// Locking for serverp is handled by the replica framework.
	//
	if (serverp != NULL) {
		//
		// XXX Note that a failure in become_primary after
		// creating and checkpointing serverp, could mean
		// that serverp->idlverpA is not initialized.
		//
		os::printf("Skipping server object creation\n");
#ifdef ORBTEST_V1
		typ = hatypetest::server::_get_type_info(service_version);
		server_v = serverp->get_objref(typ);
		version_lock.unlock();
#else
		server_v = serverp->get_objref();
#endif
		return;
	}
	//
	// Create the first hatypetest server object.
	//
	os::printf("Creating first hatypetest server object\n");
	serverp = new server_impl(this);
#ifdef ORBTEST_V1
	typ = hatypetest::server::_get_type_info(service_version);
	server_v = serverp->get_objref(typ);
#else
	server_v = serverp->get_objref();
#endif
	ASSERT(!CORBA::is_nil(server_v));

	//
	// Checkpoint the creation of the server object.
	//
	os::printf("Checkpointing creation of new server object\n");
	_ckpt_proxy->ckpt_new_server(server_v, e);

	// Initialize IDL versioning test objects.
	os::printf("SERVER: Initializing IDL versioning objects\n");

	serverp->idlverpA = new minverObjA_impl(this);
#ifdef ORBTEST_V1
	typ = hatypetest::minverObjA::_get_type_info(service_version);
	hatypetest::minverObjA_var newA_ref =
	    serverp->idlverpA->get_objref(typ);
#else
	hatypetest::minverObjA_var newA_ref = serverp->idlverpA->get_objref();
#endif
	_ckpt_proxy->ckpt_create_minverObjA(newA_ref, e);
	if (e.exception()) {
		os::printf("exception\n");
		e.clear();
	}

#ifdef ORBTEST_V1
	if (service_version != 0) {
		serverp->idlverpB = new minverObjB_impl(this);
		hatypetest::minverObjB_var newB_ref =
			serverp->idlverpB->get_objref();
		_ckpt_proxy->ckpt_create_minverObjB(newB_ref, e);
		if (e.exception()) {
			os::printf("exception\n");
			e.clear();
		}
		serverp->idlverpC = new minverObjC_impl(this);
		hatypetest::minverObjC_var newC_ref =
			serverp->idlverpC->get_objref();
		_ckpt_proxy->ckpt_create_minverObjC(newC_ref, e);
		if (e.exception()) {
			os::printf("exception\n");
			e.clear();
		}
		serverp->idlverpD = new combinedObjD_impl(this);
		hatypetest::combinedObjD_var newD_ref =
			serverp->idlverpD->get_objref();
		_ckpt_proxy->ckpt_create_combinedObjD(newD_ref->_this_obj(), e);
		if (e.exception()) {
			os::printf("exception\n");
			e.clear();
		}
	}
#endif // ORBTEST_V1

	if (! serverp->init_called) {
		serverp->init();
		_ckpt_proxy->ckpt_init_called(serverp->init_called, e);
	}
#ifdef ORBTEST_V1
	version_lock.unlock();
#endif
}

void
hatypetest_prov::freeze_primary_prepare(Environment &e)
{
}

void
hatypetest_prov::freeze_primary(Environment &e)
{
	os::printf("hatypetest_prov::freeze_primary()\n");
	if (e.exception()) {
		e.exception()->print_exception("Exception in "
			"hatypetest_prov::freeze_primary\n");
		e.clear();
	}
}

void
hatypetest_prov::unfreeze_primary(Environment &e)
{
	os::printf("hatypetest_prov::unfreeze_primary()\n");
	if (e.exception()) {
		e.exception()->print_exception("Exception in "
			"hatypetest_prov::unfreeze_primary\n");
		e.clear();
	}
}

void
hatypetest_prov::become_spare(Environment &e)
{
	os::printf("hatypetest_prov::become_spare()\n");

	// XXX No unreferenced() calls? Should be done by serverp delete.
	delete serverp;
	serverp = NULL;
}

void
hatypetest_prov::shutdown(Environment &e)
{
	os::printf("hatypetest_prov::shutdown()\n");
	if (!serverp->done_called) {
		e.exception(new replica::service_busy);
		return;
	}
	if (!serverp->wait_until_unreferenced(10)) {
		os::printf("hatypetest_prov::shutdown: server object "
		    "still referenced\n");
		e.exception(new replica::service_busy);
		return;
	}
	delete serverp;
	serverp = NULL;
#ifdef ORBTEST_V1
	version_lock.wrlock();
#endif
	_ckpt_proxy = hatypetest::ckpt::_nil();
#ifdef ORBTEST_V1
	version_lock.unlock();
#endif
	os::printf("hatypetest_prov::shutdown() DONE\n");
}


CORBA::Object_ptr
hatypetest_prov::get_root_obj(Environment &)
{
	ASSERT(serverp != NULL);

	return (hatypetest::server::_duplicate(server_v));
}


void
hatypetest_prov::done_called_changed(Environment &e)
{
	os::printf("hatypetest_prov::done_called_changed()\n");

	if (serverp->done_called)
		server_v = hatypetest::server::_nil();
	hatypetest::ckpt_var ckpt_v = get_hatypetest_checkpoint();
	ckpt_v->ckpt_done_called(serverp->done_called, e);
}


void
hatypetest_prov::verify_off_changed(Environment &e)
{
	os::printf("hatypetest_prov::verify_off_changed()\n");

	hatypetest::ckpt_var ckpt_v = get_hatypetest_checkpoint();
	ckpt_v->ckpt_verify_off(serverp->verify_off, e);
}


void
hatypetest_prov::ckpt_new_server(hatypetest::server_ptr p_server, Environment &)
{
	os::printf("hatypetest_prov::ckpt_new_server()\n");

	if (serverp == NULL) {
		os::printf("Creating shadow server object\n");
		serverp = new server_impl(p_server, this);
	}
}


void
hatypetest_prov::ckpt_init_called(bool new_value, Environment &)
{
	os::printf("hatypetest_prov::ckpt_init_called()\n");
	serverp->init_called = new_value;
}


void
hatypetest_prov::ckpt_done_called(bool new_value, Environment &)
{
	os::printf("hatypetest_prov::ckpt_done_called()\n");
	serverp->done_called = new_value;
}


void
hatypetest_prov::ckpt_verify_off(bool new_value, Environment &)
{
	os::printf("hatypetest_prov::ckpt_verify_off()\n");
	serverp->verify_off = new_value;
}


void
hatypetest_prov::ckpt_create_minverObjA(hatypetest::minverObjA_ptr objref,
					Environment &)
{
	os::printf("hatypetest_prov::ckpt_create_minverObjA()\n");
	// Create a shadow copy of the object, associate it with the reference.
	if (serverp->idlverpA == NULL)
		serverp->idlverpA = new minverObjA_impl(objref);
}


hatypetest::ckpt_ptr
hatypetest_prov::get_hatypetest_checkpoint()
{
#ifdef ORBTEST_V1
	version_lock.rdlock();
#endif
	hatypetest::ckpt_ptr ckptp = hatypetest::ckpt::_duplicate(_ckpt_proxy);
#ifdef ORBTEST_V1
	version_lock.unlock();
#endif
	return (ckptp);
}


#ifdef ORBTEST_V1
void
hatypetest_prov::ckpt_create_minverObjB(hatypetest::minverObjB_ptr objref,
					Environment &)
{
	os::printf("hatypetest_prov::ckpt_create_minverObjB()\n");
	// Create a shadow copy of the object, associate it with the reference.
	if (serverp->idlverpB == NULL)
		serverp->idlverpB = new minverObjB_impl(objref);
}


void
hatypetest_prov::ckpt_create_minverObjC(hatypetest::minverObjC_ptr objref,
					Environment &)
{
	os::printf("hatypetest_prov::ckpt_create_minverObjC()\n");
	// Create a shadow copy of the object, associate it with the reference.
	if (serverp->idlverpC == NULL)
		serverp->idlverpC = new minverObjC_impl(objref);
}


void
hatypetest_prov::ckpt_create_combinedObjD(CORBA::Object_ptr objref,
					Environment &)
{
	os::printf("hatypetest_prov::ckpt_create_minverObjD()\n");
	// Create a shadow copy of the object, associate it with the reference.
	if (serverp->idlverpD == NULL)
		serverp->idlverpD = new combinedObjD_impl(objref->_this_obj());
}


void
hatypetest_prov::ckpt_service_version(int16_t new_version, Environment &)
{
	os::printf("hatypetest_prov::ckpt_service_version()\n");
	version_lock.wrlock();
	service_version = new_version;
	version_lock.unlock();
}
#endif // ORBTEST_V1


//
// Note that upgrade callback functions are somewhat limited in the
// kinds of things they can do.
// XXX In particular, no HA checkpoints are allowed (which probably should
// be fixed although it works here since this is implemented using an IDL
// call to the server instead of an IDL call to a callback object).
//
void
hatypetest_prov::upgrade_callback(Environment &e)
{
	os::printf("hatypetest_prov::upgrade_callback()\n");

#ifdef ORBTEST_V1

	//
	// Return if already upgraded.
	//
	version_lock.wrlock();
	if (service_version == 1) {
		version_lock.unlock();
		return;
	}

	//
	// Assertion: An attempt here to invoke a v1 checkpoint method
	//		should result in an UNSUPPORTED_VERSION exception.
	// XXX Note that upgrade callbacks don't have a way to return
	// errors so this test can only be verified manually.
	//
	os::printf("Calling v1 checkpoint method prior to upgrade\t");
	_ckpt_proxy->ckpt_service_version(service_version, e);
	if (e.exception()) {
		os::printf("PASS\n");
		e.exception()->print_exception("\t");
		e.clear();
	} else {
		os::printf("FAIL\n");
	}

	//
	// Change over to version 1 checkpoint.
	//
	os::printf("Setting service_version to 1\n");
	service_version = 1;

	os::printf("Creating v1 hatypetest::ckpt object\n");
	replica::checkpoint_var tmp_ckptp =
	    set_checkpoint(hatypetest::ckpt::_get_type_info(service_version));

	os::printf("Narrowing v1 obj ref to hatypetest::ckpt\n");
	_ckpt_proxy = hatypetest::ckpt::_narrow(tmp_ckptp);
	ASSERT(!CORBA::is_nil(_ckpt_proxy));

	// Update the server reference.
	server_v = serverp->get_objref();
#ifdef _KERNEL
	(void) serverp->register_obj(server_v, hatypes_kserver_name);
#else
	(void) serverp->register_obj(server_v, hatypes_unserver_name);
#endif

	// Update the minverObjA reference.
	hatypetest::minverObjA_var tmprefA = serverp->idlverpA->get_objref();
	serverp->idlverpA->register_obj(tmprefA, Akserver_name);

	//
	// Checkpoint the change to service_version.
	//
	os::printf("Calling ckpt_service_version()\n");
	_ckpt_proxy->ckpt_service_version(service_version, e);
	if (e.exception()) {
		os::printf("exception\n");
		e.clear();
	}

	//
	// Checkpoint creation of version 1 specific objects.
	//
	serverp->idlverpB = new minverObjB_impl(this);
	hatypetest::minverObjB_var newB_ref = serverp->idlverpB->get_objref();
	serverp->idlverpB->init();
	os::printf("Calling ckpt_create_minverObjB()\n");
	_ckpt_proxy->ckpt_create_minverObjB(newB_ref, e);
	if (e.exception()) {
		os::printf("exception\n");
		e.clear();
	}
	serverp->idlverpC = new minverObjC_impl(this);
	hatypetest::minverObjC_var newC_ref = serverp->idlverpC->get_objref();
	serverp->idlverpC->init();
	os::printf("Calling ckpt_create_minverObjC()\n");
	_ckpt_proxy->ckpt_create_minverObjC(newC_ref, e);
	if (e.exception()) {
		os::printf("exception\n");
		e.clear();
	}
	serverp->idlverpD = new combinedObjD_impl(this);
	hatypetest::combinedObjD_var newD_ref = serverp->idlverpD->get_objref();
	serverp->idlverpD->init();
	os::printf("Calling ckpt_create_combinedObjD()\n");
	_ckpt_proxy->ckpt_create_combinedObjD(newD_ref->_this_obj(), e);
	if (e.exception()) {
		os::printf("exception\n");
		e.clear();
	}

	//
	// Unfreeze the HA service
	//
	version_lock.unlock();
#endif // ORBTEST_V1
}


//
// constructor - for primaries
//
server_impl::server_impl(hatypetest_prov *server) :
	mc_replica_of<hatypetest::server>(server),
	my_provider(server), init_called(false)
{
	os::printf("Calling primary server constructor\n");

	// by default we will do verification of results
	verify_off = false;
	done_called = false;
	idlverpA = NULL;
#ifdef ORBTEST_V1
	idlverpB = NULL;
	idlverpC = NULL;
	idlverpD = NULL;
#endif
}


//
// constructor - for secondaries
//
server_impl::server_impl(hatypetest::server_ptr ref, hatypetest_prov *server) :
	mc_replica_of<hatypetest::server>(ref),
	my_provider(server), init_called(false)
{
	os::printf("Calling secondary server constructor\n");
	verify_off = false;
	done_called = false;
	idlverpA = NULL;
#ifdef ORBTEST_V1
	idlverpB = NULL;
	idlverpC = NULL;
	idlverpD = NULL;
#endif
}


//
// destructor
//
// Should be in this routine only if the server was unreferenced
//
server_impl::~server_impl()
{
	os::printf("server_impl::~server_impl()\n");
	if (idlverpA != NULL) {
		(void) idlverpA->wait_until_unreferenced();
		delete idlverpA;
		idlverpA = NULL;
	}
#ifdef ORBTEST_V1
	os::printf("server_impl::~server_impl() B\n");
	if (idlverpB != NULL) {
		(void) idlverpB->wait_until_unreferenced();
		delete idlverpB;
		idlverpB = NULL;
	}
	os::printf("server_impl::~server_impl() C\n");
	if (idlverpC != NULL) {
		(void) idlverpC->wait_until_unreferenced();
		delete idlverpC;
		idlverpC = NULL;
	}
	os::printf("server_impl::~server_impl() D\n");
	if (idlverpD != NULL) {
		(void) idlverpD->wait_until_unreferenced();
		delete idlverpD;
		idlverpD = NULL;
	}
#endif
	os::printf("server_impl::~server_impl() done\n");
}

//
// version - should return the current version, not the compiled version.
//
int16_t
server_impl::version(Environment &)
{
#ifdef ORBTEST_V1
	return (my_provider->service_version);
#else
	return (0);
#endif
}

void
server_impl::_unreferenced(unref_t cookie)
{
	os::printf("server_impl::_unreferenced() called\n");
	if (_last_unref(cookie))
		unref_called(true);
}

//
// Initialize implementation object.
//
bool
server_impl::init()
{
	os::printf("server_impl::init() called\n");

	// If we already registered our object with the nameserver,
	// then there's no need to do it again.
	if (init_called)
		return (true);

#if defined(_KERNEL)
	init_called = register_obj(my_provider->server_v,
	    hatypes_kserver_name);
#elif defined(_KERNEL_ORB)
	init_called = register_obj(my_provider->server_v,
	    hatypes_unserver_name);
#endif

	idlverpA->init();
#ifdef ORBTEST_V1
	if (my_provider->service_version != 0) {
		idlverpB->init();
		idlverpC->init();
		idlverpD->init();
	}
#endif // ORBTEST_V1

	return (init_called);
}


//
// Clients invoke this when they're finished with this object.
//
void
server_impl::done(Environment &e)
{
	os::printf("server_impl::done() called\n");

	// Unregister from the name server.
	// XXX if the unregister is done on a node other that the one that
	// did the register, the name is NULL.
	if (unregister_obj()) {
		done_called = true;
		my_provider->done_called_changed(e);
	}

	Environment e2;
	idlverpA->done(e2);
#ifdef ORBTEST_V1
	if (my_provider->service_version > 0) {
		if (idlverpB != NULL)
			idlverpB->done(e2);
		if (idlverpC != NULL)
			idlverpC->done(e2);
		if (idlverpD != NULL)
			idlverpD->done(e2);
	}
#endif
}

//
// Sets the "no-verifcation" flag.  This is associated with the
// -noverify option
//
void
server_impl::set_noverify(Environment &e)
{
	verify_off = true;
	my_provider->verify_off_changed(e);
}

//
// This function should be called before the upgrade callback.
//
// It tests the following three pre-upgrade assertions:
//
// 1. We can obtain a reference to V0 checkpoint proxy
// 2. We can use that reference to call a V0 checkpoint method
// 3. An attempt to use the proxy ref to call a V1 checkpoint
//    method should fail
//
bool
server_impl::test_get_old_ckpt_ref(Environment &e)
{
	os::printf("Getting v0 checkpoint reference\t");
	test_ckpt_ref = my_provider->get_hatypetest_checkpoint();
	if (is_not_nil(test_ckpt_ref)) {
		os::printf("PASS\n");
	} else {
		os::printf("FAILED\n");
		return (false);
	}

	os::printf("Calling V0 interface method on V0 checkpoint reference\t");
	test_ckpt_ref->ckpt_init_called(init_called, e);
	if (e.exception()) {
		os::printf("FAILED\n");
		e.exception()->print_exception("\t");
		e.clear();
		return (false);
	} else {
		os::printf("PASS\n");
	}

#ifdef ORBTEST_V1
	os::printf("Calling V1 interface method on V0 checkpoint reference\t");
	test_ckpt_ref->ckpt_service_version(
	    my_provider->service_version, e);
	if (e.exception() && CORBA::VERSION::_exnarrow(e.exception())) {
		os::printf("PASS\n");
		e.exception()->print_exception("\t");
		e.clear();
	} else {
		os::printf("FAILED\n");
		return (false);
	}
#endif
	return (true);
}


bool
server_impl::test_use_old_ckpt_ref(Environment &e)
{
	//
	// server_impl::test_get_old_ckpt_ref() saved a duplicate reference
	// to the V0 checkpoint proxy in test_ckpt_ref so this reference
	// should still be a valid V0 reference even though upgrade_callback()
	// has set _ckpt_proxy to a V1 reference.
	//
	os::printf("Calling interface method on v0 checkpoint reference\t");
	test_ckpt_ref->ckpt_init_called(init_called, e);
	if (e.exception()) {
		os::printf("FAIL\n");
		e.exception()->print_exception("\t");
		return (false);
	} else {
		os::printf("PASS\n");
	}

#ifdef ORBTEST_V1
	os::printf("Calling V1 interface method on V0 checkpoint reference\t");
	test_ckpt_ref->ckpt_service_version(
	    my_provider->service_version, e);
	if (e.exception() && CORBA::VERSION::_exnarrow(e.exception())) {
		os::printf("PASS\n");
		e.exception()->print_exception("\t");
		e.clear();
	} else {
		os::printf("FAILED\n");
		return (false);
	}
#endif
	return (true);
}


#ifdef ORBTEST_V1
int16_t
server_impl::getid(Environment &)
{
	int16_t ret = 1234;
	os::printf("server_impl::getid() %h\n", ret);

	return (ret);
}
#endif // ORBTEST_V1

void
server_impl::upgrade_callback(Environment &e)
{
	// Invoke the provider callback to switch checkpoints.
	my_provider->upgrade_callback(e);
}
