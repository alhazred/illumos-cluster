/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_params_impl.cc	1.12	08/05/20 SMI"

#include <h/naming.h>
#include <nslib/ns.h>
#include <orbtest/dev_tests/test_params_impl.h>


test_params_impl::test_params_impl(int32_t num_clients)
{
	_num_clients = num_clients;

	_test_mode = orbtest::reuse_objects;
	_num_iters = 1;
	_num_threads = 1;
	_num_gets = 1;
	_num_invos = 1;
	_verbose = false;

	_got_unref = false;

	// Expect num_clients + one test server to call done().  We'll unbind
	// this server from the name server and exit when _done_count
	// reaches zero.
	_done_count = num_clients + 1;
}


void
#ifdef DEBUG
test_params_impl::_unreferenced(unref_t cookie)
#else
test_params_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));

	_unref_lck.lock();
	_got_unref = true;
	_unref_cv.signal();
	_unref_lck.unlock();
}


// advertize "this" in the name server
bool
test_params_impl::publish_params()
{
	naming::naming_context_var	ctxp = ns::root_nameserver();
	Environment			e;
	orbtest::test_params_var	ref = get_objref();

	ctxp->rebind(test_params_name, ref, e);
	if (e.exception()) {
		os::printf("test_params_impl: exception binding!\n");
		return (false);
	}

	return (true);
}


// wait for object to get unreferenced.
void
test_params_impl::release_params()
{
	_unref_lck.lock();
	while (!_got_unref) {
		_unref_cv.wait(&_unref_lck);
	}
	_unref_lck.unlock();
}


int32_t
test_params_impl::get_num_clients(Environment&)
{
	return (_num_clients);
}


orbtest::test_mode
test_params_impl::get_test_mode(Environment&)
{
	return (_test_mode);
}

void
test_params_impl::set_test_mode(orbtest::test_mode mode)
{
	_test_mode = mode;
}


int32_t
test_params_impl::get_num_iters(Environment&)
{
	return (_num_iters);
}

void
test_params_impl::set_num_iters(int32_t n_iters)
{
	_num_iters = n_iters;
}


int32_t
test_params_impl::get_num_threads(Environment&)
{
	return (_num_threads);
}

void
test_params_impl::set_num_threads(int32_t n_threads)
{
	_num_threads = n_threads;
}


int32_t
test_params_impl::get_num_gets(Environment&)
{
	return (_num_gets);
}

void
test_params_impl::set_num_gets(int32_t n_gets)
{
	_num_gets = n_gets;
}


int32_t
test_params_impl::get_num_invos(Environment&)
{
	return (_num_invos);
}

void
test_params_impl::set_num_invos(int32_t n_invos)
{
	_num_invos = n_invos;
}


bool
test_params_impl::get_verbose(Environment&)
{
	return (_verbose);
}

void
test_params_impl::set_verbose(bool v)
{
	_verbose = v;
}


void
test_params_impl::done(Environment &e)
{
	_done_lck.lock();

	// If all test clients and test server have called done() then
	// unbind from the name server.
	if (--_done_count == 0) {
		naming::naming_context_var	ctxp = ns::root_nameserver();

		ctxp->unbind(test_params_name, e);
		if (e.exception()) {
			e.exception()->print_exception(
						"test_params_impl::done");
		}
	}

	_done_lck.unlock();
}
