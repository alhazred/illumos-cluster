/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_mgr_test.cc	1.15	08/05/20 SMI"

#include <unistd.h>
#include <stdlib.h>

#include <sys/os.h>
#include <orbtest/dev_tests/test_mgr_impl.h>

// We setup the test object (test_mgr_impl) which reads the parameters
// and waits for clients to do their thing.
// When they are done wait_for_compeltion will return.

int
#if defined(_KERNEL_ORB) && !defined(_KERNEL)
mgr(int, char **)
#else
main_test(int, char **)
#endif
{
	test_mgr_impl	*smp = new test_mgr_impl();
	// register with name server and read parameters.
	if (!smp->setup()) {
		return (1);
	}

	// Wait for clients to finish their tests and unreferenced.
	smp->wait_for_completion();
	delete smp;

	return (0);
}
