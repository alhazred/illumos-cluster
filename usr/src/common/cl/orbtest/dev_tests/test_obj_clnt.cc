/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_obj_clnt.cc	1.35	08/05/20 SMI"

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

#include <thread.h>
#include <sys/errno.h>

#include <sys/os.h>

#include <h/orbtest.h>
#include <h/naming.h>
#include <nslib/ns.h>
#include <orbtest/dev_tests/test_common.h>

static int
opps(Environment& e)
{
	CORBA::Exception *ex;

	if ((ex = e.exception()) != NULL) {
		CORBA::SystemException *x;
		if ((x = CORBA::SystemException::_exnarrow(ex)) != NULL) {
			if (CORBA::COMM_FAILURE::_exnarrow(ex) != NULL) {
				os::printf("comm failure: node down."
				    " request %scompleted\n",
				    (x->completed() == CORBA::COMPLETED_YES) ?
					"was " :
					(x->completed() == CORBA::COMPLETED_NO)
					    ? "not " : "may be ");
			} else {
				os::printf("system Exception %d caught\n",
				    x->_major());
			}
		} else {
			os::printf("Exception caught\n");
		}
		return (-1);
	}
	return (0);
}

int
#if defined(_KERNEL_ORB) && !defined(_KERNEL)
clnt(int, char **)
#else
main_test(int, char **)
#endif
{
	orbtest::test_obj_ptr test_objp;
	int val, i;
	int retry;
	Environment e;
	int16_t not_used_in_this_test;

	// create a reference to the name server object
	naming::naming_context_var ctxp = ns::root_nameserver();

	do {
		retry = 0;
		CORBA::Object_var obj = ctxp->resolve(test_obj_name, e);
		if (e.exception()) {
			if (opps(e) == -1) {
				return (1);
			}
			e.clear();
		}

		test_objp = orbtest::test_obj::_narrow(obj);
		if (test_objp == NULL) {
			os::printf("can't narrow\n");
			return (1);
		}
	} while (retry);

	for (i = 0; i != 10; i++) {
		test_objp->put(i, not_used_in_this_test, e);
		if (opps(e) == -1) {
			return (1);
		}
		val = test_objp->get(e);
		if (opps(e) == -1) {
			return (1);
		}
	}

	CORBA::release(test_objp);

	// We want the object reference to go away because of the release not
	// because this process died.  For now sleep for a bit and hope that
	// is the case.  We need some thing better.
	sleep(5);
	return (0);
}
