/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_mgr_mod.cc	1.14	08/05/20 SMI"

#include <sys/modctl.h>
#include <sys/errno.h>

#include <sys/os.h>
#include <orbtest/dev_tests/test_mgr_impl.h>

extern "C" void _cplpl_init();
extern "C" void _cplpl_fini();

/* inter-module dependencies */
char _depends_on[] = "misc/cl_runtime misc/cl_load misc/cl_orb "
	"misc/cl_haci misc/cl_quorum misc/cl_comm";

extern struct mod_ops mod_miscops;

static struct modlmisc modlmisc = {
	&mod_miscops, "unreferenced test",
};

static struct modlinkage modlinkage = {
	MODREV_1, { (void *)&modlmisc, NULL }
};

test_mgr_impl	*smp = NULL;

int
_init(void)
{
	int	error;

	if (error = mod_install(&modlinkage)) {
		return (error);
	}
	_cplpl_init();		/* C++ initialization */

	// Create the test server.
	smp = new test_mgr_impl();

	// Register with name server and read parameters.
	if (!smp->setup()) {
		_cplpl_fini();
		mod_remove(&modlinkage);
		return (EIO);
	}

	return (0);
}

int
_fini(void)
{
	if (smp != NULL) {
		// Wait for clients to finish their tests and for unreferenced.
		smp->wait_for_completion();
		delete smp;
	}

		/* Cannot unload the Orb at this point in system development */

	_cplpl_fini();
	return (mod_remove(&modlinkage));
}

int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}
