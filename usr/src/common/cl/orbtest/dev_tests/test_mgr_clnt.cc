/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_mgr_clnt.cc	1.42	08/05/20 SMI"

#include <sys/errno.h>
#include <sys/time.h>

#include <sys/os.h>

#include <h/naming.h>
#include <nslib/ns.h>
#include <h/orbtest.h>
#include <orbtest/dev_tests/test_common.h>

static void * test_thread(void *arg);
orbtest::test_mgr_ptr	test_mgr_obj;

// Test parameters.
int	n_iters;
int	n_threads;
int	n_gets;
int	n_invos;
bool	verbose;

void	starttimeout(int timeout);

class wait_sync {
public:
	wait_sync() { }
	~wait_sync() { }

	void	wait_for_completion();
	void	notify_done();
	int	num_waits;

private:
	os::mutex_t	_lck;
	os::condvar_t	_cv;
};

wait_sync completion_wait;


void
wait_sync::wait_for_completion()
{
	_lck.lock();
	while (num_waits > 0)
		_cv.wait(&_lck);
	_lck.unlock();
}

void
wait_sync::notify_done()
{
	_lck.lock();
	num_waits--;
	_cv.signal();
	_lck.unlock();
}


int
test_mgr_clnt_test(int timeout)
{
	naming::naming_context_var	ctxp = ns::root_nameserver();
	orbtest::test_params_var	paramsp;
	Environment			e;
	CORBA::Exception		*ex;
	int				retval = false;
	int				i, j;

	//
	// Get reference to the test parameters server.
	//
	for (;;) {
		CORBA::Object_var	obj;

		obj = ctxp->resolve(test_params_name, e);
		if (ex = e.exception()) {
			if (naming::not_found::_exnarrow(ex)) {
				e.clear();
				continue;
			}
			ex->print_exception("test_mgr_clnt_test: "
				"looking up test_params");
			goto cleanup;
		}

		paramsp = orbtest::test_params::_narrow(obj);
		if (CORBA::is_nil(paramsp)) {
			os::printf("test_mgr_clnt_test: can't narrow "
				"test_params\n");
			goto cleanup;
		}

		break;
	}

	//
	// Get test parameters.
	//
	n_iters = paramsp->get_num_iters(e);
	if (ex = e.exception()) {
		ex->print_exception("test_mgr_clnt_test: "
			"paramsp->get_num_iters");
		goto cleanup;
	}

	n_threads = paramsp->get_num_threads(e);
	if (ex = e.exception()) {
		ex->print_exception("test_mgr_clnt_test: "
			"paramsp->get_num_threads");
		goto cleanup;
	}

	n_gets = paramsp->get_num_gets(e);
	if (ex = e.exception()) {
		ex->print_exception("test_mgr_clnt_test: "
			"paramsp->get_num_gets");
		goto cleanup;
	}

	n_invos = paramsp->get_num_invos(e);
	if (ex = e.exception()) {
		ex->print_exception("test_mgr_clnt_test: "
			"paramsp->get_num_invos");
		goto cleanup;
	}

	verbose = paramsp->get_verbose(e);
	if (ex = e.exception()) {
		ex->print_exception("test_mgr_clnt_test: "
			"paramsp->get_verbose");
		goto cleanup;
	}

	// Tell parameter server we're done with it.
	paramsp->done(e);
	if (ex = e.exception()) {
		ex->print_exception("test_mgr_clnt_test: paramsp->done");
		goto cleanup;
	}
	paramsp = nil;			// this will also do CORBA::release

	//
	// Get reference to the test server.
	//
	for (;;) {
		CORBA::Object_var	obj;

		obj = ctxp->resolve(test_mgr_name, e);
		if (ex = e.exception()) {
			if (naming::not_found::_exnarrow(ex)) {
				e.clear();
				continue;
			}
			ex->print_exception("test_mgr_clnt_test: "
				"looking up test_mgr");
			goto cleanup;
		}

		test_mgr_obj = orbtest::test_mgr::_narrow(obj);
		if (CORBA::is_nil(test_mgr_obj)) {
			os::printf("test_mgr_clnt_test: can't narrow "
				"test_mgr\n");
			goto cleanup;
		}

		// Register with the test server.  If this fails then we
		// probably have an object which is left dangling from
		// a previous run.
		test_mgr_obj->register_client(e);
		if (ex = e.exception()) {
			if (CORBA::COMM_FAILURE::_exnarrow(ex)) {
				os::printf("COMM_FAILURE invoking "
					"register_client()\n");
				CORBA::release(test_mgr_obj);
				e.clear();
				continue;
			} else {
				ex->print_exception("test_mgr_clnt_test: "
					"test_mgr_obj->register_client");
				goto cleanup;
			}
		}

		break;
	}

	//
	// Run the tests.
	//
	if (timeout != 0)
		starttimeout(timeout);

	for (i = 0; (timeout == 0) && (i < n_iters); i++) {
		completion_wait.num_waits = n_threads;

		for (j = 0; j != n_threads; j++) {
#ifdef	_KERNEL
			if (thread_create(NULL, NULL, (void(*)())test_thread,
			    (char *)j, 0, &p0, TS_RUN, 98) == NULL) {
				os::warning("%u thread_create failed\n", j);
				completion_wait.num_waits--;
			}
#else
			if (os::thread::create(NULL, (size_t)0, test_thread,
			    (void *)j, THR_BOUND, NULL)) {
				os::warning("%u thr_create failed\n", j);
				completion_wait.num_waits--;
			}
#endif	// _KERNEL
		}

		os::hrtime_t start = os::gethrtime();
		completion_wait.wait_for_completion();
		os::printf("iter %d done. Took %lld secs\n", i,
			(os::gethrtime() - start)/((os::hrtime_t) NANOSEC));
	}

	if (verbose) {
		os::printf("client: ");
	}
	os::printf("DONE\n");

	retval = true;					// success!

cleanup:
	CORBA::release(test_mgr_obj);
	test_mgr_obj = nil;

	// Tell parameter server we're done with it.
	if (is_not_nil(paramsp)) {
		e.clear();		// clear previous exceptions, if any
		paramsp->done(e);
		if (ex = e.exception()) {
			ex->print_exception("test_mgr_clnt_test: "
				"paramsp->done");
			return (false);
		}
	}

	return (retval);
}


static void *
test_timeout_thread(void *arg)
{
	intptr_t timeout = (intptr_t)arg;
	os::usecsleep((os::usec_t)timeout * MICROSEC);
	os::panic("injecting failure");
	return (NULL);
}


void
starttimeout(int timeout)
{
#ifdef	_KERNEL
	if (thread_create(NULL, NULL, (void(*)(void))test_timeout_thread,
	    (caddr_t)timeout, 0, &p0, TS_RUN, 61) == NULL) {
		os::panic("thread_create failed\n");
	}
#else
	if (os::thread::create(NULL, (size_t)0, test_timeout_thread,
	    (void *)timeout, THR_BOUND, NULL)) {
		os::panic("thr_create failed");
	}
#endif	// _KERNEL
}


static void *
test_thread(void *arg)
{
	Environment e;
	uint_t	tid = (uint_t)(uintptr_t)arg;
	int16_t	not_used_in_this_test;

	// if (verbose) {
	// 	os::printf("start thread %d\n", tid);
	// }

	for (int i = 0; i != n_gets; i++) {
		orbtest::test_obj_var test_obj = test_mgr_obj->get_test_obj(e);
		if (e.exception()) {
			e.exception()->print_exception("get_test_obj()");
			break;
		}
		for (int j = 0; j != n_invos; j++) {
			// we must put a unique number for this
			// process.
			test_obj->put(j, not_used_in_this_test, e);
			if (e.exception()) {
				e.exception()->print_exception("put()");
				break;
			}
			(void) test_obj->get(e);
			if (e.exception()) {
				e.exception()->print_exception("get()");
				break;
			}
		}
		if (e.exception())
			break;
	}

	// if (verbose) {
	//	os::printf("thread %d done\n", tid);
	// }

	completion_wait.notify_done();
	return (NULL);
}
