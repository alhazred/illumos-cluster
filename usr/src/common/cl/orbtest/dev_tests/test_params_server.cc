/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_params_server.cc	1.19	08/05/20 SMI"

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include <sys/os.h>
#include <orbtest/dev_tests/test_params_impl.h>


/* BEGIN CSTYLED */
static const char usage[] = "\
Usage: %s [options] <num_clients>\n\
\n\
Options:\n\
    -iter <num_iters>\n\
	Number of times each client should run the test.\n\
    -thread <num_threads>\n\
	Number of threads each client creates.\n\
    -get <num_gets>\n\
	Number of times each client thread should get test objects.\n\
    -invo <num_invos>\n\
	Number of put() and get() invocations each client thread makes\n\
	on the test objects.\n\
    -new\n\
	Tells test server to create a new test object implementation for\n\
	each client request for a test object.  Otherwise, only one\n\
	implementation will be created and the server will pass references\n\
	to it for each each client request.\n\
    -v\n\
	Verbose output.\n\
";
/* END CSTYLED */


static int	pos_optarg(int opt_index, int argc, char *argv[]);


//
// Command line contains test parameters.  We use a server to store the
// test parameters so we can provide them to kernel modules without the use
// of complicated schemes, e.g. using ioctl() calls.
//
int
#if defined(_KERNEL_ORB) && !defined(_KERNEL)
params_server(int argc, char **argv)
#else
main_test(int argc, char **argv)
#endif
{
	int32_t			n_clients;		// required arg
	orbtest::test_mode	test_mode = orbtest::reuse_objects;
	int32_t			n_iters = 1;
	int32_t			n_threads = 1;
	int32_t			n_gets = 1;
	int32_t			n_invos = 1;
	bool			verbose = false;
	int			idx;
	test_params_impl	*tpp;

	// Parse options.
	for (idx = 1; (idx < argc) && (argv[idx][0] == '-'); ++idx) {
		// "--" marks end of options.
		if (strcmp(argv[idx], "--") == 0) {
			++idx;
			break;
		}

		if (strncmp(argv[idx], "-ite", 4) == 0) {
			if ((n_iters = pos_optarg(idx++, argc, argv)) < 0) {
				return (1);
			}
			continue;
		}

		if (strncmp(argv[idx], "-thr", 4) == 0) {
			if ((n_threads = pos_optarg(idx++, argc, argv)) < 0) {
				return (1);
			}
			continue;
		}

		if (strncmp(argv[idx], "-get", 4) == 0) {
			if ((n_gets = pos_optarg(idx++, argc, argv)) < 0) {
				return (1);
			}
			continue;
		}

		if (strncmp(argv[idx], "-inv", 4) == 0) {
			if ((n_invos = pos_optarg(idx++, argc, argv)) < 0) {
				return (1);
			}
			continue;
		}

		if (strncmp(argv[idx], "-new", 4) == 0) {
			test_mode = orbtest::new_objects;
			continue;
		}

		if (strncmp(argv[idx], "-v", 2) == 0) {
			verbose = true;
			continue;
		}

		fprintf(stderr, "ERROR: option %s unknown\n", argv[idx]);
		fprintf(stderr, usage, argv[0]);
		return (1);
	}

	// Verify we have the right numbers of required arguments.
	if ((argc - idx) < 1) {
		fprintf(stderr, "ERROR: missing number of clients argument\n");
		fprintf(stderr, usage, argv[0]);
		return (1);
	}

	// Get required arguments.
	if ((n_clients = atoi(argv[idx++])) <= 0) {
		fprintf(stderr, "Number of clients must be > 0\n");
		return (1);
	}

	// Create parameter server.
	tpp = new test_params_impl(n_clients);

	// Set optional parameters.
	tpp->set_test_mode(test_mode);
	tpp->set_num_iters(n_iters);
	tpp->set_num_threads(n_threads);
	tpp->set_num_gets(n_gets);
	tpp->set_num_invos(n_invos);
	tpp->set_verbose(verbose);

	// Advertise server.
	tpp->publish_params();

	// Unbind from name server and wait for unreferenced.
	tpp->release_params();
	delete tpp;

	if (verbose) {
		os::printf("test_params: DONE\n");
	}

	return (0);
}


//
// Returns a positive option argument.
// Returns -1 if there are no more arguments or the option argument is <= 0.
//
// "opt_index" is the index into argv, pointing to the option string.
//
int
pos_optarg(int opt_index, int argc, char *argv[])
{
	int	numarg;
	int	numarg_index = opt_index + 1;

	if (argc <= numarg_index) {
		fprintf(stderr, "ERROR: option %s requires argument\n",
			argv[opt_index]);
		return (-1);
	}

	if ((numarg = atoi(argv[numarg_index])) <= 0) {
		fprintf(stderr, "ERROR: %s option argument must be > 0\n",
			argv[opt_index]);
		return (-1);
	}

	return (numarg);
}
