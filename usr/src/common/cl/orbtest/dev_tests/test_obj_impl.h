/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _TEST_OBJ_IMPL_H
#define	_TEST_OBJ_IMPL_H

#pragma ident	"@(#)test_obj_impl.h	1.23	08/05/20 SMI"

#include <h/orbtest.h>
#include <orb/object/adapter.h>
#include <orbtest/dev_tests/test_common.h>

/*CSTYLED*/
class test_obj_impl : public McServerof<orbtest::test_obj> {
public:
	static os::condvar_t 	done_cv;
	static os::mutex_t 	done_lock;
	static bool 		done;

	test_obj_impl() { _value = 0; };
	~test_obj_impl() {};
	virtual void	_unreferenced(unref_t);

	// IDL interfaces
	virtual void	put(int32_t i, int16_t &, Environment& _environment);
	virtual int32_t	get(Environment& _environment);

protected:
	int			_value;
	os::mutex_t 		_lck;
	void	_lock()		{ _lck.lock(); }
	void	_unlock()	{ _lck.unlock(); }
};

#endif	/* _TEST_OBJ_IMPL_H */
