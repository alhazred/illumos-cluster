/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_obj_bind.cc	1.27	08/05/20 SMI"

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

#include <thread.h>
#include <sys/errno.h>

#include <os.h>

#include <naming.h>
#include <nslib/ns.h>
#include <test_obj_impl.h>

static void * bind_thread(void *arg);

static naming::naming_context_ptr ctxp;


int
#if defined(_KERNEL_ORB) && !defined(_KERNEL)
bind(int argc, char **argv)
#else
main_test(int argc, char **argv)
#endif
{
	int	i, nthreads = 20;

	orbtest::test_obj_ptr obj1 = new test_obj_impl();

	// create a reference to the name server object
	ctxp = ns::root_nameserver();

	for (i = 0; i != nthreads; i++) {
		if (os::thread::create(NULL, (size_t)0, bind_thread, (void *)i,
		    THR_BOUND, NULL)) {
			os::panic("thr_create failed");
		}
	}

	os::printf("all threads created\n");
	for (i = 0; i != nthreads; i++) {
		thr_join(0, NULL, NULL);
	}
	CORBA::release(ctxp);
	os::printf("done\n");
	return (0);
}

static void *
bind_thread(void *arg)
{
	Environment e;
	Exception *ex;
	int	tid = (uint32_t)arg;

	orbtest::test_obj_ptr obj1 = new test_obj_impl();

	for (int i = 0; i != 100; i++) {
		char	buf[20];

		os::sprintf(buf, "x%d.%d\n", tid, i);
		// os::printf("binding %x to %s\n", obj1, buf);
		ctxp->bind(buf, obj1, e);
		if ((ex = e.exception()) != NULL) {
			if (CORBA::SystemException::_exnarrow(ex))
				os::printf("SystemException caught\n");
			else
				os::panic("x");
		}
		// os::printf("unbinding %s\n", buf);
		ctxp->unbind(buf, e);
		if ((ex = e.exception()) != NULL) {
			if (CORBA::SystemException::_exnarrow(ex))
				os::printf("SystemException caught\n");
			else
				os::panic("x");
		}
	}

	CORBA::release(obj1);

	return (NULL);
}

test_obj_impl::test_obj_impl()
{
	value = 0;
}

test_obj_impl::~test_obj_impl()
{
}

void
#ifdef DEBUG
test_obj_impl::_unreferenced(unref_t cookie)
#else
test_obj_impl::_unreferenced(unref_t)
#endif
{
	printf("unreferenced %x\n", this);

	ASSERT(_last_unref(cookie));
}

// test_obj_impl(orbtest::test_obj::put)
void test_obj_impl::put(int32_t i) {
	value = i;
}


// test_obj_impl(orbtest::test_obj::get)
int32_t test_obj_impl::get() {
	return (value);
}
