/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_obj_revoke.cc	1.29	08/05/20 SMI"

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

#include <thread.h>
#include <sys/errno.h>

#include <sys/os.h>

#include <h/naming.h>
#include <orbtest/dev_tests/test_obj_revoke.h>
#include <nslib/ns.h>

bool		test_obj_impl2::done = false;
os::condvar_t   test_obj_impl2::done_cv;
os::mutex_t	test_obj_impl2::done_lock;
bool 	test_obj_impl2::do_revoke = false;

int
#if defined(_KERNEL_ORB) && !defined(_KERNEL)
revoke(int, char **)
#else
main_test(int, char **)
#endif
{
	Environment e;

	test_obj_impl2 *implp = new test_obj_impl2;
	orbtest::test_obj_ptr obj1 = implp->get_objref();

	// create a reference to the name server object
	naming::naming_context_var ctxp = ns::root_nameserver();

	ctxp->rebind(test_obj_name, obj1, e);
	if (e.exception()) {
		CORBA::release(obj1);
		printf("SystemException caught\n");
		return (1);
	}
	CORBA::release(obj1);

	test_obj_impl2::done_lock.lock();
	while (!test_obj_impl2::do_revoke)
		test_obj_impl2::done_cv.wait(&test_obj_impl2::done_lock);
	test_obj_impl2::done_lock.unlock();

	implp->revoke();

	os::printf("sleeping again\n");
	os::usecsleep((os::usec_t)10*MICROSEC);
	os::printf("woke up\n");
	test_obj_impl2::done_lock.lock();
	while (!test_obj_impl2::done)
		test_obj_impl2::done_cv.wait(&test_obj_impl2::done_lock);
	test_obj_impl2::done_lock.unlock();

	delete implp;
	return (0);
}

void
#ifdef DEBUG
test_obj_impl2::_unreferenced(unref_t cookie)
#else
test_obj_impl2::_unreferenced(unref_t)
#endif
{
	os::printf("test_obj_impl2::_unreferenced %x!!\n", this);

	ASSERT(_last_unref(cookie));

	done_lock.lock();
	done = true;
	done_cv.signal();
	done_lock.unlock();
}

// test_obj_impl(orbtest::test_obj::put)
void test_obj_impl2::put(int32_t i, int16_t &, Environment&) {
	os::printf("sleeping\n");
	os::usecsleep((os::usec_t)10*MICROSEC);
	os::printf("woke up\n");
	value = i;
}


// test_obj_impl(orbtest::test_obj::get)
int32_t test_obj_impl2::get(Environment&)
{
	os::printf("triggering revoke on %x!!\n", this);
	done_lock.lock();
	do_revoke = true;
	done_cv.signal();
	done_lock.unlock();
	return (value);
}
