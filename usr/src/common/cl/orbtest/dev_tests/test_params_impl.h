/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _TEST_PARAMS_IMPL_H
#define	_TEST_PARAMS_IMPL_H

#pragma ident	"@(#)test_params_impl.h	1.13	08/05/20 SMI"

#include <h/orbtest.h>
#include <orb/object/adapter.h>
#include <orbtest/dev_tests/test_common.h>

/*CSTYLED*/
class test_params_impl : public McServerof<orbtest::test_params> {
public:
	test_params_impl(int32_t num_clients);

	void	_unreferenced(unref_t);

	// Set optional parameters.
	void	set_test_mode(orbtest::test_mode);
	void	set_num_iters(int32_t);
	void	set_num_threads(int32_t);
	void	set_num_gets(int32_t);
	void	set_num_invos(int32_t);
	void	set_verbose(bool);

	// advertize "this" in the name server
	bool	publish_params();

	// unbind from name server and wait for object to get unreferenced.
	void	release_params();

	//
	// IDL interfaces
	//

	// Test server parameters.
	int32_t			get_num_clients(Environment&);
	orbtest::test_mode	get_test_mode(Environment&);

	// Test client parameters.
	int32_t	get_num_iters(Environment&);
	int32_t	get_num_threads(Environment&);
	int32_t	get_num_gets(Environment&);
	int32_t	get_num_invos(Environment&);

	// Client and server parameters;
	bool	get_verbose(Environment&);

	// Each test client and test server must invoke this and release all
	// references to this server when it's finished with its tasks
	// so this server can unbind itself from the name server and exit.
	void	done(Environment&);

private:
	// Test server parameters.
	int32_t			_num_clients;
	orbtest::test_mode	_test_mode;

	// Test client parameters.
	int32_t	_num_iters;
	int32_t	_num_threads;
	int32_t	_num_gets;
	int32_t	_num_invos;

	// Client and server parameters.
	bool	_verbose;

	int32_t		_done_count;
	os::mutex_t	_done_lck;

	bool		_got_unref;
	os::condvar_t	_unref_cv;
	os::mutex_t	_unref_lck;
};

#endif	/* _TEST_PARAMS_IMPL_H */
