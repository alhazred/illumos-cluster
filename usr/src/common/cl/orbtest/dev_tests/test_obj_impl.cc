/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_obj_impl.cc	1.37	08/05/20 SMI"

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

#include <thread.h>
#include <sys/errno.h>

#include <sys/os.h>

#include <h/naming.h>
#include <nslib/ns.h>
#include <orbtest/dev_tests/test_obj_impl.h>

bool 	test_obj_impl::done = false;
os::condvar_t 	test_obj_impl::done_cv;
os::mutex_t 	test_obj_impl::done_lock;

int
#if defined(_KERNEL_ORB) && !defined(_KERNEL)
impl(int, char **)
#else
main_test(int, char **)
#endif
{
	Environment e;

	orbtest::test_obj_ptr obj1 = new test_obj_impl()->get_objref();

	// create a reference to the name server object
	naming::naming_context_var ctxp = ns::root_nameserver();

	ctxp->rebind(test_obj_name, obj1, e);
	if (e.exception()) {
		e.exception()->print_exception("dev_tests main\n");
	} else {
		CORBA::release(obj1);
	}
	test_obj_impl::done_lock.lock();
	while (!test_obj_impl::done)
		test_obj_impl::done_cv.wait(&test_obj_impl::done_lock);
	test_obj_impl::done_lock.unlock();

	return (0);
}


void
#ifdef DEBUG
test_obj_impl::_unreferenced(unref_t cookie)
#else
test_obj_impl::_unreferenced(unref_t)
#endif
{
	os::printf("test_obj_impl::unreferenced %x!!\n", this);

	ASSERT(_last_unref(cookie));

	delete this;
	done_lock.lock();
	done = true;
	done_cv.signal();
	done_lock.unlock();
}

// test_obj_impl(orbtest::test_obj::put)
void test_obj_impl::put(int32_t i, int16_t &, Environment&) {
	_value = i;
}


// test_obj_impl(orbtest::test_obj::get)
int32_t test_obj_impl::get(Environment&) {
	return (_value);
}
