/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)unode_reboot.cc	1.13	08/05/20 SMI"

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

#include <thread.h>
#include <sys/errno.h>
#include <h/naming.h>
#include <nslib/ns.h>

#include <unode/unode.h>
#include <sys/os.h>

#include <orb/fault/fault_injection.h>

// Condition variable used to make rebooting thread wait for request thread
// to have exited.
os::condvar_t	exit_cv;
os::mutex_t	exit_lock;
bool		exited = false;

extern void unode_reboot();

void *
do_reboot(void *)
{
	exit_lock.lock();

	while (!exited) {
		exit_cv.wait(&exit_lock);
	}

	// We should not return from this call
	reboot_unode_clean();
	ASSERT(false);

	exit_lock.unlock();
	return (NULL);
}


int
reboot(int, char **)
{
	os::printf("==> Got reboot request\n");

	exit_lock.lock();
	//
	// Do the reboot on another thread, so we can return on this thread
	//
	if (os::thread::create(NULL, (size_t)0, do_reboot, NULL, THR_BOUND,
	    NULL)) {
		os::printf("thr_create failed: could not reboot\n");
		return (1);
	}

	exited = true;
	exit_cv.broadcast();
	exit_lock.unlock();

	return (0);
}


void *
do_reboot_cluster(void *)
{
#if defined(_FAULT_INJECTION)
	exit_lock.lock();

	while (!exited) {
		exit_cv.wait(&exit_lock);
	}

	// We should not return from this call
	FaultFunctions::reboot_all_nodes();
	ASSERT(false);

	exit_lock.unlock();
#else
	os::warning("_FAULT_INJECTION not defined, reboot needs fault "
	    "injection");
#endif // _FAULT_INJECTION

	return (NULL);
}

int
reboot_cluster(int, char **)
{
	os::printf("==> Got reboot cluster request\n");

	exit_lock.lock();
	//
	// Do the reboot on another thread, so we can return on this thread
	//
	if (thr_create(NULL, (size_t)0, do_reboot_cluster, NULL, THR_BOUND,
	    NULL)) {
		os::printf("thr_create failed: could not reboot\n");
		return (1);
	}

	exited = true;
	exit_cv.broadcast();
	exit_lock.unlock();

	return (0);
}
