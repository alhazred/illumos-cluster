/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)unode_dump_csr.cc	1.7	08/05/20 SMI"

#include <sys/os.h>
#include <sys/clconf_int.h>

int
dump_csr(int, char **)
{
	cladmin_component_state_list_t	*state_list = NULL;
	cladmin_component_state_t	*state = NULL;

	state_list = cladmin_get_component_state_list();
	if (state_list != NULL) {
		for (int index = 0; index < state_list->listlen; index++) {
			state = &(state_list->list[index]);

			os::printf("Comp. Name: %s\n", state->int_name);
			os::printf("Comp. Type: %s\n", state->type);
			os::printf("State: %d (%s)\n\n", state->state,
			    state->state_string);

		}

		cladmin_free_component_state_list(state_list);
	}

	return (0);
}
