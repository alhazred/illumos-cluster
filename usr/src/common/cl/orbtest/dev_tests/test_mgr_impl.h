/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _TEST_MGR_IMPL_H
#define	_TEST_MGR_IMPL_H

#pragma ident	"@(#)test_mgr_impl.h	1.24	08/05/20 SMI"

#include <h/orbtest.h>
#include <orb/object/adapter.h>
#include <sys/os.h>
#include <orbtest/dev_tests/test_common.h>

class test_mgr_impl;

/*CSTYLED*/
class test_obj_impl1 : public McServerof<orbtest::test_obj> {
public:
	test_obj_impl1(test_mgr_impl* m);
	~test_obj_impl1();

	void	_unreferenced(unref_t);

	// IDL interface.
	void	put(int32_t i, int16_t &, Environment&);
	int32_t	get(Environment&);

	bool	has_reference();

	orbtest::test_obj_ptr	new_ref();

	void	mark_unrefd();

private:
	bool		_has_ref;
	int		_value;
	test_mgr_impl&	_mgr;

	test_obj_impl1();
};

/*CSTYLED*/
class test_mgr_impl : public McServerof<orbtest::test_mgr> {
public:
	test_mgr_impl();
	~test_mgr_impl();

	void	_unreferenced(unref_t);

	// IDL interface.
	orbtest::test_obj_ptr get_test_obj(Environment&);
	void	register_client(Environment&);

	// Interfaces used by managed objects (test_obj_impl1).
	//
	// A test object got _unreferenced();
	void	deal_with_unreferenced(test_obj_impl1 *test_objectp,
					unref_t cookie);

	// called by whatever is driving the test (modunload for kernel
	// or main for userland).
	bool	setup();
	void	wait_for_completion();

private:
	enum { UNREFERENCED = 0x01, DONE = 0x02 };

	// Test parameters.
	orbtest::test_mode	_test_mode;
	int32_t			_num_clients;
	bool			_verbose;

	void	_lock()		{ _lck.lock(); }
	void	_unlock()	{ _lck.unlock(); }

	os::mutex_t		_lck;
	os::condvar_t		_done_cv;
	char			_flags;

	/*CSTYLED*/
	DList<test_obj_impl1>	_object_list;

	// Number of objects in the list that have references.
	int32_t			_num_referenced;

	// If _test_mode == orbtest::reuse_objects, we'll count how many times
	// the test object gets _last_unref().  When this server is
	// unreferenced we'll verify that the test object has at least one
	// _last_unref().
	//
	// Note, it's possible to have more than one _last_unref() since there
	// is one client per node.  The client running on the same node as
	// this server may run very fast compared to other clients since all
	// invocations will be local.  Thus, this local client could finish
	// its tests and release all references to the test object before
	// other clients have a chance to start their tests.
	int32_t			_num_last_unref;
};

#endif	/* _TEST_MGR_IMPL_H */
