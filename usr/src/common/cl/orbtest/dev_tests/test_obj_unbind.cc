/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_obj_unbind.cc	1.23	08/05/20 SMI"

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

#include <thread.h>
#include <sys/errno.h>

#include <sys/os.h>

#include <h/naming.h>
#include <nslib/ns.h>
#include <h/orbtest.h>
#include <orbtest/dev_tests/test_common.h>

int
#if defined(_KERNEL_ORB) && !defined(_KERNEL)
unbind(int, char **)
#else
main_test(int, char **)
#endif
{
	Environment e;

	// create a reference to the name server object
	naming::naming_context_var ctxp = ns::root_nameserver();

	ctxp->unbind(test_obj_name, e);
	if (e.exception()) {
		printf("SystemException caught\n");
		return (1);
	}
	return (0);
}
