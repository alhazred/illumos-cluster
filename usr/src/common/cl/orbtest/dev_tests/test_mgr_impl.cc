/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_mgr_impl.cc	1.36	08/05/20 SMI"

#include <sys/os.h>

#include <h/naming.h>
#include <nslib/ns.h>
#include <orbtest/dev_tests/test_mgr_impl.h>


// This program expects to hear from _num_clients clients.
// The clients find the object in the name server.
// Each clients should do a put of an independent value.
// we keep track of how many have independent puts have been performed
// and then unbind the name from the name_server.  At this point the
// program should get unreferenced on the master object when all clients
// die or release the object. We can have the clients perform graceful
// or ungraceful releases of the object, the results should always be
// the same we exit when we get unreferenced on that object. This allows
// simple test scripts to be written which wait for this program to exit.
// and stranges errors or hangs would indicate a failure of the test.
// A number of multi threaded clients can stress the reference counting
// algorithms pretty well.


test_obj_impl1::test_obj_impl1(test_mgr_impl* m) : _mgr(*m)
{
	_value = 0;
	_has_ref = false;
}

test_obj_impl1::~test_obj_impl1()
{
	ASSERT(_has_ref == false);
}

void
test_obj_impl1::_unreferenced(unref_t cookie)
{
	_mgr.deal_with_unreferenced(this, cookie);
}

void
test_obj_impl1::put(int32_t i, int16_t &, Environment&)
{
	_value = i;
}

int32_t
test_obj_impl1::get(Environment&)
{
	return (_value);
}

bool
test_obj_impl1::has_reference()
{
	return (_has_ref);
}

orbtest::test_obj_ptr
test_obj_impl1::new_ref()
{
	_has_ref = true;
	return (get_objref());
}

void
test_obj_impl1::mark_unrefd()
{
	_has_ref = false;
}


test_mgr_impl::test_mgr_impl()
{
	_flags = 0;
	_num_referenced = 0;
	_num_last_unref = 0;
	_verbose = false;
}


test_mgr_impl::~test_mgr_impl()
{
	ASSERT(_num_referenced == 0);

	// If test_mode == orbtest::reuse_objects, verify that the test object
	// has received at least one _last_unref().
	if (_test_mode == orbtest::reuse_objects && !_object_list.empty() &&
	    _num_last_unref == 0) {
		test_obj_impl1	*objp;

		_object_list.atfirst();
		objp = _object_list.get_current();
		os::printf("FAIL: no last_unref for test object %x!!\n", objp);
	}

	_object_list.dispose();
}


void
#ifdef DEBUG
test_mgr_impl::_unreferenced(unref_t cookie)
#else
test_mgr_impl::_unreferenced(unref_t)
#endif
{
	if (_verbose) {
		os::printf("test_mgr: unreferenced %x!!\n", this);
	}

	ASSERT(_last_unref(cookie));

	_lock();
	_flags |= UNREFERENCED;
	if (_num_referenced == 0) {
		_flags |= DONE;
		_done_cv.signal();
	}
	_unlock();
}


void
test_mgr_impl::register_client(Environment&)
{
	Environment	e;
	bool		do_unbind = false;
	naming::naming_context_var	ctxp = ns::root_nameserver();

	_lock();

	if (_num_clients <= 0) {
		os::warning("test_mgr_impl::register: num_clients <= 0");
		_unlock();
		return;
	}

	if (--_num_clients == 0) {
		// All clients have registered. Unbind from the name
		// server so that we can get unreferenced on this
		// (test_mgr) object.
		do_unbind = true;
	}

	_unlock();

	if (do_unbind) {
		ctxp->unbind(test_mgr_name, e);
		e.clear();
	}
}


//
// If we are testing the 0->1 conversion, then we only keep one object
// on our list and keep passing out references to it. When that object
// gets unreferenced we delete it.
//
orbtest::test_obj_ptr
test_mgr_impl::get_test_obj(Environment&)
{
	test_obj_impl1		*test_objectp;
	orbtest::test_obj_ptr	newref;

	_lock();
	if (_test_mode == orbtest::new_objects || _object_list.empty()) {
		// allocate a new one
		test_objectp = new test_obj_impl1(this);
		_object_list.insert_a(test_objectp);
		_num_referenced++;
		if (_verbose) {
			os::printf("test_mgr: num referenced objs: %d\n",
				_num_referenced);
		}
	} else {
		_object_list.atfirst();
		test_objectp = _object_list.get_current();
		if (! test_objectp->has_reference()) {
			_num_referenced++;
			if (_verbose) {
				os::printf("test_mgr: num referenced objs: "
					"%d\n", _num_referenced);
			}
		}
	}
	newref = test_objectp->new_ref();
	_unlock();
	return (newref);
}


//
// Prepare for the test.
// 1 Look up the test parameters.
// 2 Bind to name server.
//
bool
test_mgr_impl::setup()
{
	naming::naming_context_var	ctxp = ns::root_nameserver();
	orbtest::test_params_var	paramsp;
	orbtest::test_mgr_var		test_mgrp;
	CORBA::Object_var		obj;
	Environment			e;
	CORBA::Exception		*ex;
	bool				retval = false;

	obj = ctxp->resolve(test_params_name, e);
	if (ex = e.exception()) {
		ex->print_exception("test_mgr_impl::setup: "
			"looking up test_params");
		goto cleanup;
	}

	paramsp = orbtest::test_params::_narrow(obj);
	if (CORBA::is_nil(paramsp)) {
		os::printf("test_mgr_impl::setup: can't narrow test_params\n");
		goto cleanup;
	}

	// Get test_mode parameter from the parameter server.
	_test_mode = paramsp->get_test_mode(e);
	if (ex = e.exception()) {
		ex->print_exception("test_mgr_impl::setup: "
			"paramsp->get_test_mode");
		goto cleanup;
	}

	// Get number of clients from the parameter server.
	_num_clients = paramsp->get_num_clients(e);
	if (ex = e.exception()) {
		ex->print_exception("test_mgr_impl::setup: "
			"paramsp->get_num_clients");
		goto cleanup;
	}

	// Get verbose flag from the parameter server.
	_verbose = paramsp->get_verbose(e);
	if (ex = e.exception()) {
		ex->print_exception("test_mgr_impl::setup: "
			"paramsp->get_verbose");
		goto cleanup;
	}

	// create and bind a reference to this object so clients can find it.
	test_mgrp = get_objref();
	ctxp->rebind(test_mgr_name, test_mgrp, e);
	if (ex = e.exception()) {
		ex->print_exception("test_mgr_impl::setup: can't bind "
			"test_mgr");
		goto cleanup;
	}

	retval = true;					// success!

cleanup:
	// Let parameters server know we're done.
	if (is_not_nil(paramsp)) {
		e.clear();		// clear previous exceptions, if any
		paramsp->done(e);
		if (ex = e.exception()) {
			ex->print_exception("test_mgr_impl::setup: "
				"paramsp->done");
			return (false);
		}
	}

	return (retval);
}


// One of the objects in the list got unreferenced. Depending on the test
// mode, we either delete it or we keep it around for reuse.
void
test_mgr_impl::deal_with_unreferenced(test_obj_impl1 *test_objectp,
	unref_t cookie)
{
	_lock();

	if (! test_objectp->_last_unref(cookie)) {
		if (_test_mode == orbtest::reuse_objects) {
			// Since there is only one test object for ALL client
			// requests, it's possible that the test object gets
			// unreferenced but another request comes in before
			// we reach this point.
			if (_verbose) {
				os::printf("test_mgr: _last_unref(%x) false\n",
					cookie);
			}
			_unlock();
			return;
		} else {
			// We should get a _last_unref() since there is one
			// test object per client request.
			os::printf("last_unref FAILED for test object %x!!\n",
				test_objectp);
		}
	}

	// It is really unreferenced.
	test_objectp->mark_unrefd();
	_num_last_unref++;
	_num_referenced--;

	if (_verbose) {
		os::printf("test_mgr: num referenced objs: %d\n",
			_num_referenced);
	}

	if (_test_mode == orbtest::new_objects) {
		_object_list.erase(test_objectp);
		delete test_objectp;
	}

	if (_num_referenced == 0 && (_flags & UNREFERENCED) != 0) {
		_flags |= DONE;
		_done_cv.signal();
	}

	_unlock();
}


void
test_mgr_impl::wait_for_completion()
{
	_lock();
	while ((_flags & DONE) == 0) {
		_done_cv.wait(&_lck);
	}
	_unlock();

	if (_verbose) {
		os::printf("test_mgr: DONE\n");
	}
}
