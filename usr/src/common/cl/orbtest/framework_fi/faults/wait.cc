/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)wait.cc	1.11	08/05/20 SMI"

#include <orbtest/framework_fi/faults/entries.h>

int
wait(int argc, char *argv[])
{
	if (argc != 2) {
		os::printf("FAIL: Only accepts a single argument\n");
		return (1);
	}

	int time = os::atoi(argv[1]);
	os::printf("Going to sleep for %d seconds\n", time);
	os::usecsleep(time * 1000000);

	os::printf("PASS: Finished sleep\n");
	return (0);
}
