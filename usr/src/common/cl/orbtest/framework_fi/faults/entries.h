/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ORBTEST_FRAMEWORK_FI_FAULTS_ENTRIES_H
#define	_ORBTEST_FRAMEWORK_FI_FAULTS_ENTRIES_H

#pragma ident	"@(#)entries.h	1.13	08/05/20 SMI"

//
// test entries
//

#include <sys/os.h>
#include <orb/invo/common.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/fault/fault_injection.h>
#include <orbtest/framework_fi/impl_common.h>

//
// Test entry functions.
//

int	trigger_fault(int argc, char *argv[]);
int clear_fault(int argc, char *argv[]);
int clear_all(int, char *[]);
int wait(int argc, char *argv[]);
int	node_reboot(int, char *[]);

#endif	/* _ORBTEST_FRAMEWORK_FI_FAULTS_ENTRIES_H */
