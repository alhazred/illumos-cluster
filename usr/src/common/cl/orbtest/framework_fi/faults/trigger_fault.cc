/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)trigger_fault.cc	1.17	08/05/20 SMI"

#if !defined(_KERNEL)
#include <unistd.h>
#endif

#include <orbtest/framework_fi/faults/entries.h>
#include <orb/fault/fault_injection.h>

//
// trigger_fault
// This is the main function to trigger fault points in the code.
// For command syntax, see fault.data file
//

int
trigger_fault(int argc, char *argv[])
{
#if defined(_FAULT_INJECTION)
	int fnum, offset;
	os::printf("*** Simple fault trigger\n");

	if (argc < 3) {
		os::printf("FAIL: Incomplete fault number specified.\n");
		return (1);
	}

	// The fault type (argv[1]) and offset (argv[2]) determine
	// the fault number. The numbers used for the byte shifts
	// below are from the fault_numbers.h file.
	offset = (int)os::atoi(argv[2]);
	if (os::strcmp(argv[1], "DEBUG") == 0) {
			os::printf(" * Fault is in FAULTBASE_DEBUG range\n");
			fnum = 0 << 16;
	} else if (os::strcmp(argv[1], "ORB") == 0) {
			os::printf(" * Fault is in FAULTBASE_ORB range\n");
			fnum = 1 << 16;
	} else if (os::strcmp(argv[1], "HA_FRMWRK") == 0) {
			os::printf(" * Fault is in FAULTBASE_HA_FRMWRK "
			    "range\n");
			fnum = 2 << 16;
	} else if (os::strcmp(argv[1], "TRANSPORT") == 0) {
			os::printf(" * Fault is in FAULTBASE_TRANSPORT "
			    "range\n");
			fnum = 3 << 16;
	} else if (os::strcmp(argv[1], "NET") == 0) {
			os::printf(" * Fault is in FAULTBASE_NET range\n");
			fnum = 4 << 16;
	} else if (os::strcmp(argv[1], "PXFS") == 0) {
			os::printf(" * Fault is in FAULTBASE_PXFS range\n");
			fnum = 5 << 16;
	} else if (os::strcmp(argv[1], "CMM") == 0) {
			os::printf(" * Fault is in FAULTBASE_CMM range\n");
			fnum = 6 << 16;
	} else if (os::strcmp(argv[1], "ORPHAN") == 0) {
			os::printf(" * Fault is in FAULTBASE_ORPHAN range\n");
			fnum = 7 << 16;
	} else if (os::strcmp(argv[1], "CCR") == 0) {
			os::printf(" * Fault is in FAULTBASE_CCR range\n");
			fnum = 8 << 16;
	} else if (os::strcmp(argv[1], "NS") == 0) {
			os::printf(" * Fault is in FAULTBASE_NS range\n");
			fnum = 9 << 16;
	} else if (os::strcmp(argv[1], "DCS") == 0) {
			os::printf(" * Fault is in FAULTBASE_DCS range\n");
			fnum = 10 << 16;
	} else if (os::strcmp(argv[1], "RECONF") == 0) {
			os::printf(" * Fault is in FAULTBASE_RECONF range\n");
			fnum = 11 << 16;
	} else if (os::strcmp(argv[1], "UNREF") == 0) {
			os::printf(" * Fault is in FAULTBASE_UNREF range\n");
			fnum = 12 << 16;
	} else {
			os::printf("Unknown fault range\n");
			return (1);
	}

	fnum += offset;
	os::printf(" ** Triggering fault number %d\n", fnum);

	if (argc == 3) {
		NodeTriggers::add(fnum, NULL, 0, TRIGGER_THIS_NODE);
		os::printf("PASS: Fault point triggered with no args\n");
		return (0);
	} else {

	// If argc > 3, there must be a set of arguments given.
	// First, find the name of the argument type, then create
	// an instance of that struct and fill in the fields with
	// other passed args.

	if (os::strcmp(argv[3], "wait_for_arg_t") == 0) {
		if (argc != 6) {
			os::printf("FAIL: Not enough args for ARGTYPE\n");
			return (1);
		}
		FaultFunctions::wait_for_arg_t arg_t;
		arg_t.op = (FaultFunctions::wait_for_op_t)os::atoi(argv[4]);
		arg_t.nodeid = (int)os::atoi(argv[5]);
		NodeTriggers::add(fnum, &arg_t, (uint32_t)sizeof (arg_t),
		    TRIGGER_THIS_NODE);
	} else if (os::strcmp(argv[3], "ha_rm_reboot_arg_t") == 0) {
		if (argc != 8) {
			os::printf("FAIL: Not enough args for ARGTYPE\n");
			return (1);
		}
		FaultFunctions::ha_rm_reboot_arg_t arg_t;
		arg_t.sid = (replica::service_num_t)os::atoi(argv[4]);
		arg_t.op = (FaultFunctions::wait_for_op_t)os::atoi(argv[5]);
		arg_t.nodeid = (nodeid_t)os::atoi(argv[6]);
		os::strcpy(arg_t.fn_name, argv[7]);
		NodeTriggers::add(fnum, &arg_t, (uint32_t)sizeof (arg_t),
		    TRIGGER_THIS_NODE);
	} else if (os::strcmp(argv[3], "cmm_reboot_arg_t") == 0) {
		if (argc != 7) {
			os::printf("FAIL: Not enough args for ARGTYPE\n");
		return (1);
		}
		FaultFunctions::cmm_reboot_arg_t arg_t;
		arg_t.step = os::atoi(argv[4]);
		arg_t.op = (FaultFunctions::wait_for_op_t)os::atoi(argv[5]);
		arg_t.nodeid = (nodeid_t)os::atoi(argv[6]);
		NodeTriggers::add(fnum, &arg_t, (uint32_t)sizeof (arg_t),
		    TRIGGER_THIS_NODE);
	} else if (os::strcmp(argv[3], "ha_dep_reboot_arg_t") == 0) {
		if (argc != 7) {
			os::printf("FAIL: Not enough args for ARGTYPE\n");
			return (1);
		}
		FaultFunctions::ha_dep_reboot_arg_t arg_t;
		arg_t.sid = (replica::service_num_t)os::atoi(argv[4]);
		arg_t.op = (FaultFunctions::wait_for_op_t)os::atoi(argv[5]);
		arg_t.nodeid = (nodeid_t)os::atoi(argv[6]);
		NodeTriggers::add(fnum, &arg_t, (uint32_t)sizeof (arg_t),
		    TRIGGER_THIS_NODE);
	} else if (os::strcmp(argv[3], "ha_rm_reboot_arg_t") == 0) {
		if (argc != 8) {
			os::printf("FAIL: Not enough args for ARGTYPE\n");
			return (1);
		}
		FaultFunctions::ha_rm_reboot_arg_t arg_t;
		arg_t.sid = (replica::service_num_t)os::atoi(argv[4]);
		arg_t.op = (FaultFunctions::wait_for_op_t)os::atoi(argv[5]);
		arg_t.nodeid = (nodeid_t)os::atoi(argv[6]);
		os::strcpy(arg_t.fn_name, argv[7]);
		NodeTriggers::add(fnum, &arg_t, (uint32_t)sizeof (arg_t),
		    TRIGGER_THIS_NODE);
	} else {
		os::printf("FAIL: Unknown ARGTYPE\n");
		return (1);
	}

	os::printf("PASS: Fault point successfully triggered with args\n");
	return (0);
	}

	// NOTREACHED
#else
	argc, argv;	// To shut compiler warning
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}
