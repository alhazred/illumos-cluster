/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)clear.cc	1.13	08/05/20 SMI"

#if !defined(_KERNEL)
#include <unistd.h>
#endif

#include <orbtest/framework_fi/faults/entries.h>
#include <orb/fault/fault_injection.h>

int
clear_fault(int argc, char *argv[])
{
#if defined(_FAULT_INJECTION)
	int fnum, offset;
	os::printf("*** Simple fault trigger clear\n");

	if (argc < 3) {
		os::printf("FAIL: Incomplete fault number specified.\n");
		return (1);
	}

	offset = (int)os::atoi(argv[2]);
	os::printf("We have an offset of %d\n", offset);
	if (os::strcmp(argv[1], "DEBUG") == 0) {
			os::printf(" * Fault is in FAULTBASE_DEBUG range\n");
			fnum = 0 << 16;
	} else if (os::strcmp(argv[1], "ORB") == 0) {
			os::printf(" * Fault is in FAULTBASE_ORB range\n");
			fnum = 1 << 16;
	} else if (os::strcmp(argv[1], "HA_FRMWRK") == 0) {
			os::printf(" * Fault is in FAULTBASE_HA_FRMWRK "
			    "range\n");
			fnum = 2 << 16;
	} else if (os::strcmp(argv[1], "TRANSPORT") == 0) {
			os::printf(" * Fault is in FAULTBASE_TRANSPORT "
			    "range\n");
			fnum = 3 << 16;
	} else if (os::strcmp(argv[1], "NET") == 0) {
			os::printf(" * Fault is in FAULTBASE_NET range\n");
			fnum = 4 << 16;
	} else if (os::strcmp(argv[1], "PXFS") == 0) {
			os::printf(" * Fault is in FAULTBASE_PXFS range\n");
			fnum = 5 << 16;
	} else if (os::strcmp(argv[1], "CMM") == 0) {
			os::printf(" * Fault is in FAULTBASE_CMM range\n");
			fnum = 6 << 16;
	} else if (os::strcmp(argv[1], "ORPHAN") == 0) {
			os::printf(" * Fault is in FAULTBASE_ORPHAN range\n");
			fnum = 7 << 16;
	} else if (os::strcmp(argv[1], "CCR") == 0) {
			os::printf(" * Fault is in FAULTBASE_CCR range\n");
			fnum = 8 << 16;
	} else if (os::strcmp(argv[1], "NS") == 0) {
			os::printf(" * Fault is in FAULTBASE_NS range\n");
			fnum = 9 << 16;
	} else if (os::strcmp(argv[1], "DCS") == 0) {
			os::printf(" * Fault is in FAULTBASE_DCS range\n");
			fnum = 10 << 16;
	} else if (os::strcmp(argv[1], "RECONF") == 0) {
			os::printf(" * Fault is in FAULTBASE_RECONF range\n");
			fnum = 11 << 16;
	} else if (os::strcmp(argv[1], "UNREF") == 0) {
			os::printf(" * Fault is in FAULTBASE_UNREF range\n");
			fnum = 12 << 16;
	} else {
			os::printf("Unknown fault range\n");
			return (1);
	}

	fnum += offset;
	os::printf("Final fault number %d\n", fnum);

	NodeTriggers::clear(fnum, TRIGGER_THIS_NODE);
	os::printf("PASS: Fault point cleared\n");
	return (0);
#else
	argc, argv;	// To shut compiler warning
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

int
clear_all(int, char*[])
{
#if defined(_FAULT_INJECTION)
	os::printf("Clearing all node triggers\n");

	NodeTriggers::clear_all(TRIGGER_THIS_NODE);
	os::printf("PASS: All fault point triggers cleared\n");
	return (0);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}
