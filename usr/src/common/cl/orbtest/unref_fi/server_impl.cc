/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)server_impl.cc	1.9	08/05/20 SMI"

//
// Server_imple
//

#include <sys/types.h>

#include <sys/os.h>
#include <nslib/ns.h>

#include <orbtest/unref_fi/server_impl.h>

server_impl::server_impl() :
    _testobjp(nil)
{
	(void) os::strcpy(msg, "");
}	

server_impl::~server_impl()
{
	if (_testobjp != nil) {
		delete  _testobjp;
	}

	// Make sure _unreferenced() has been called.
	lock();
	if (! unref_called()) {
		// run_test_fi can catch this console message
		os::printf("ERROR: server_impl: destructor called before "
		"_unreferenced()\n");
	}
	unlock();
}

void
#ifdef DEBUG
server_impl::_unreferenced(unref_t cookie)
#else
server_impl::_unreferenced(unref_t)
#endif
{
	lock();
	ASSERT(_last_unref(cookie));
	unref_called(true);
	unlock();
}

//
// Initialize implimentation object with the given name.
//
bool
server_impl::init(char *strname)
{
	unref_fi::server_ptr	tmpref;
	bool			retval;

	// Register with the name server.
	tmpref = get_objref();		// get temporary reference
	retval = register_obj(tmpref, strname);
	CORBA::release(tmpref);

	return (retval);
}

//
// IDL interfaces.
//

//
// For creating a new test object
//
void
server_impl::create_testobj(Environment &)
{
	if (_testobjp != nil) {
		return;		// the object exists
	}

	_testobjp = new testobj_impl;
	ASSERT(_testobjp != NULL);

	// Initialize test object.
	_testobjp->init();
}

//
// For deleting a test object
//
void
server_impl::del_testobj(Environment &)
{
	if (_testobjp == nil) {
		return;		// the testobj not exists
	}

	delete _testobjp;
	_testobjp = nil;

}

//
// get a testobj reference
//
unref_fi::testobj_ptr
server_impl::get_testobj(Environment &e)
{
	unref_fi::testobj_ptr	tmptestobjp;

	_testobjp->lock();
	tmptestobjp = _testobjp->get_newref();
	_testobjp->unlock();

	if (CORBA::is_nil(tmptestobjp)) {
		os::sprintf(msg, "ERROR: can't get reference of testobj.");
		e.exception(new unref_fi::Errmsg(msg));
	}

	return (tmptestobjp);
}

//
// verify that the _unreferenced(0 of the testobj has
// been called within "timeout" seconds.
//
void
server_impl::check_unref(uint32_t time_out, Environment &e)
{

	_testobjp->lock();

	// if the testobj hasn't been referenced at all,
	// then it is ok.
	if (! _testobjp->was_referenced()) {
		_testobjp->unlock();
	} else {
		// Must unlock since wait_until_unreferenced()
		// sets lock
		_testobjp->unlock();

		if (! _testobjp->wait_until_unreferenced(time_out)) {

			os::sprintf(msg, "ERROR: testobj did not get "
				"unreferenced after %d seconds\n", time_out);
			e.exception(new unref_fi::Errmsg(msg));
		}
	}
}

//
// Clients invoke this when they're finished with this object.
//
void
server_impl::done(Environment &e)
{
	if (! unregister_obj()) {
		os::sprintf(msg, "ERROR: server_impl: Can't unregister "
		    "object\n");
		e.exception(new unref_fi::Errmsg(msg));
	}
}
