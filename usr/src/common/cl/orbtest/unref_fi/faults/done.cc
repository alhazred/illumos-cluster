/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)done.cc	1.9	08/05/20 SMI"

#include <orbtest/unref_fi/faults/entries.h>
#include <orbtest/unref_fi/faults/entry_support.h>

//
// Entry function to tell server test is finished so it can unbind itself
// from the name server and exit.
// Return:
//	0	SUCCESS
//	1	FAIL
//
int
server_done(int, char *[])
{
#if defined(_FAULT_INJECTION)
	unref_fi::server_var	serverp;
	Environment		e;

	// Get a reference to the server.
	serverp = get_server();
	if (CORBA::is_nil(serverp)) {
		return (1);
	}

	// Tell server we're done with test.
	serverp->done(e);
	if (check_exception(e.exception(), "ERROR:")) {
		os::printf("ERROR: can't invoke server->done()\n");
		return (1);
	}

	os::printf("PASS: server done invoked\n");
	return (0);
#else
	return (no_fault_injection());
#endif
}
