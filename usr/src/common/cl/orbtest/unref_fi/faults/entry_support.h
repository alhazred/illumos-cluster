/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ORBTEST_UNREF_FI_FAULTS_ENTRY_SUPPORT_H
#define	_ORBTEST_UNREF_FI_FAULTS_ENTRY_SUPPORT_H

#pragma ident	"@(#)entry_support.h	1.13	08/05/20 SMI"

//
// test entry support routines
//

#include <sys/os.h>
#include <orb/invo/common.h>
#include <nslib/ns.h>
#include <h/unref_fi.h>
#include <orbtest/unref_fi/impl_common.h>
#include <orbtest/unref_fi/server_impl.h>

//
// check for exceptions.
// Returns :
//	True if there IS an exception, false otherwise.
//
bool	check_exception(CORBA::Exception *ex, char *prefix);

//
// Get a reference to unref_fi::server from the name server.
// Returns:
//	Reference to the server if successful, _nil() otherwise.
//
unref_fi::server_ptr get_server();

#endif	/* _ORBTEST_UNREF_FI_FAULTS_ENTRY_SUPPORT_H */
