/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)entry_support.cc	1.10	08/05/20 SMI"

//
// Entry function support routines.
//

#include <sys/os.h>
#include <nslib/ns.h>
#include <orbtest/unref_fi/faults/entry_support.h>

//
// Check for exceptions.
// Returns :
//	True if there IS an exception, false otherwise.
//
bool
check_exception(CORBA::Exception *ex, char *prefix)
{
	unref_fi::Errmsg	*testex;

	if (ex == NULL) {
		return (false);		// no exception
	}

	if (testex = unref_fi::Errmsg::_exnarrow(ex)) {
		// Print the message shipped with the exception.
		os::printf(testex->msg);
	} else {
		ex->print_exception(prefix);
	}

	return (true);
}

//
// Get a reference to unref_fi::server from the name server.
// Returns :
//	Reference to the server if successful, _nil() otherwise.
//
unref_fi::server_ptr
get_server()
{
	CORBA::Object_var	objp;
	unref_fi::server_ptr    serverp;

	objp = impl_common::get_obj(SERVER);
	if (CORBA::is_nil(objp)) {
		return (unref_fi::server::_nil());
	}

	serverp = unref_fi::server::_narrow(objp);
	if (CORBA::is_nil(serverp)) {
		os::printf("ERROR: Can't narrow to unref_fi::server\n");
		return (unref_fi::server::_nil());
	}

	return (serverp);
}
