/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)regobj_unref_paths.cc	1.14	08/05/20 SMI"

#if !defined(_KERNEL)
#include <unistd.h>
#endif

#include <orbtest/unref_fi/faults/entries.h>
#include <orbtest/unref_fi/faults/entry_support.h>

//
// Regular non-HA ORB object unreference paths
// 	NOTE: For more details about the defination of unreference paths,
//		Please refer to src/fault_numbers/faultnum_unref.h
//
// arg 1 is the test id, arg 2 is optional, which is to specify howmany
// references need to be created inside the test.
//
int
unref_regobj_path(int argc, char *argv[])
{

	// howmany references need to be created inside the test
	int	howmany = 1;

	if (argc == 3) {
		if ((howmany = os::atoi(argv[2])) <= 0) {
			os::printf("Argument 2 should be a integer "
			    "greater than 0.\n");
			    return (1);
		}
	}

	os::printf("*** regular object unreference path %s with %d "
	    "references.\n", argv[1], howmany);

#if defined(_FAULT_INJECTION)
	// get test id
	uint_t	faultnum, g_faultnum;
	if (argc < 2) {
		os::printf("Please specify unref path number.\n");
		return (1);
	} else {
		switch (os::atoi(argv[1])) {
		case 1:
			faultnum = FAULTNUM_UNREF_REGOBJ_PATH_1;
			g_faultnum = FAULTNUM_UNREF_SETUP_DELIVER_UNREFERENCED;
			break;
		case 2:
			faultnum = FAULTNUM_UNREF_REGOBJ_PATH_2;
			g_faultnum = FAULTNUM_UNREF_SETUP_DELIVER_UNREFERENCED;
			break;
		case 3:
			faultnum = FAULTNUM_UNREF_REGOBJ_PATH_3;
			g_faultnum = FAULTNUM_UNREF_SETUP_LAST_UNREF;
			break;
		case 4:
			faultnum = FAULTNUM_UNREF_REGOBJ_PATH_4;
			g_faultnum = FAULTNUM_UNREF_SETUP_LAST_UNREF;
			break;
		case 5:
			faultnum = FAULTNUM_UNREF_REGOBJ_PATH_5;
			g_faultnum = FAULTNUM_UNREF_SETUP_BEG_XDOOR_UNREF;
			break;
		case 6:
			faultnum = FAULTNUM_UNREF_REGOBJ_PATH_6;
			g_faultnum = FAULTNUM_UNREF_SETUP_BEG_XDOOR_UNREF;
			break;
		case 7:
			faultnum = FAULTNUM_UNREF_REGOBJ_PATH_7;
			g_faultnum = FAULTNUM_UNREF_SETUP_BEG_XDOOR_UNREF;
			break;
		default:
			os::printf("Unkown test number %s.\n", argv[1]);
			return (1);
		}
	}


	unref_fi::server_var 	server_v;
	Environment		e;
	unref_fi::testobj_ptr 	testobj_p;
	int			retval = 1;

	// Get a reference to the server.
	server_v = get_server();
	if (CORBA::is_nil(server_v)) {
		return (1);
	}

	// Let server create the test object
	server_v->create_testobj(e);
	if (check_exception(e.exception(), "ERROR:")) {
		os::printf("ERROR: server can't create the test object. \n");
		return (1);
	}

	// Get a reference to the testobj
	testobj_p = server_v->get_testobj(e);
	if (check_exception(e.exception(), "ERROR:")) {
		os::printf("ERROR: can't get reference to the testobj.\n");
		retval = 1;
		goto DONE;
	}

	//
	// Add global test trigger here, there are two triggers faultnum
	// and g_faultnum, g_faultnum is a test setup trigger.
	// The trigger needs to be set by server side because it will carry
	// testobj pointer information.
	//
	// Note: Currently g_faultnum is ignored. testobj_impl::set_trigger
	// creates its own unref_arg_t and sets the 'op' to be CREATE_NEW_REF.
	// This creates a number of references which are used by later
	// triggers to unref.
	//
	testobj_p->set_trigger(faultnum, g_faultnum, howmany, e);
	if (check_exception(e.exception(), "ERROR:")) {
		os::printf("ERROR: can't set global trigger to testobj.\n");
		retval = 1;
		CORBA::release(testobj_p);
		goto DONE;
	}

	// release the last reference
	CORBA::release(testobj_p);

	// check if unreference is called
	server_v->check_unref(200, e);
	if (check_exception(e.exception(), "ERROR:")) {
		os::printf("ERROR: Error get unreferenced.");
		retval = 1;
		goto DONE;
	}

	//
	// Verify the test result
	// If there's some error encountered, the server side will set
	// a global trigger to indicate the failure.
	//
	if (fault_triggered(FAULTNUM_UNREF_FAIL, NULL, NULL)) {
		os::printf("FAIL: regular object unreference path %s.\n",
		    argv[1]);
		retval = 1;
		NodeTriggers::clear(FAULTNUM_UNREF_FAIL, TRIGGER_ALL_NODES);
	} else {
		os::printf("PASS: regular object unrefernce path %s.\n",
		    argv[1]);
		retval = 0;
	}

	//
	// Clear test triggers in case they are not correctly cleared
	// at server side.
	//
DONE:	NodeTriggers::clear(g_faultnum);
	NodeTriggers::clear(faultnum);

	//
	// clear any exception so that the next invocation can use
	// environment variable
	//
	e.clear();

	// delete the testobj
	server_v->del_testobj(e);
	if (check_exception(e.exception(), "ERROR:")) {
		os::printf("ERROR: Error destroy testobj.\n");
		return (1);
	}

	return (retval);
#else
	return (no_fault_injection());
#endif
}
