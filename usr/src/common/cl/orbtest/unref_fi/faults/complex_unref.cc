/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)complex_unref.cc	1.11	08/05/20 SMI"

#if !defined(_KERNEL)
#include <unistd.h>
#endif

#include <orbtest/unref_fi/faults/entries.h>
#include <orbtest/unref_fi/faults/entry_support.h>

//
// Complex unref(with 0-1 transition) with 1 reference
//
int
complex_unref_1(int, char *[])
{
	os::printf("*** Complex unref(0-1 transition) with 1 reference.\n");

	unref_fi::server_var	serverp;
	Environment		e;
	unref_fi::testobj_ptr	testobjp;
	int			retval = 0;


	// Get a reference to the server.
	serverp = get_server();
	if (CORBA::is_nil(serverp)) {
		return (1);
	}

	// Let server create the test object
	serverp->create_testobj(e);
	if (check_exception(e.exception(), "ERROR:")) {
		os::printf("ERROR: server can't create the test objection.\n");
		return (1);
	}

	// get a reference to testobj
	testobjp = serverp->get_testobj(e);
	if (check_exception(e.exception(), "ERROR:")) {
		os::printf("ERROR: can't get reference to testobj");
		retval = 1;
		goto DONE;
	}

	// release the reference
	CORBA::release(testobjp);

	// check if unreference is called
	serverp->check_unref(200, e);
	if (check_exception(e.exception(), "ERROR:")) {
		os::printf("ERROR: Error get unreferenced.");
		retval = 1;
		goto DONE;
	}

	// get a reference to testobj (0-1)
	testobjp = serverp->get_testobj(e);
	if (check_exception(e.exception(), "ERROR:")) {
		os::printf("ERROR: can't get reference to testobj");
		retval = 1;
		goto DONE;
	}

	// release the reference
	CORBA::release(testobjp);

	// check if unreference is called
	serverp->check_unref(200, e);
	if (check_exception(e.exception(), "ERROR:")) {
		os::printf("ERROR: Error get unreferenced.");
		retval = 1;
		goto DONE;
	}

	// delete the testobj
DONE:	serverp->del_testobj(e);
	if (check_exception(e.exception(), "ERROR:")) {
		os::printf("ERROR: Error destroy testobj.");
		return (1);
	}

	if (retval == 0) {
		// If get here, the test passed
		os::printf("PASS: Complex unref with 1 reference \n");
	}
	return (retval);
}


//
// Complex unref(with 0-1 transition) with multi references
//
int
complex_unref_multi(int argc, char *argv[])
{
	unref_fi::testobj_ptr	*testobjp;
	int			howmany = 2;
	int			i, j;
	int			retval = 0;

	// Get argument
	if (argc == 2) {
		howmany = os::atoi(argv[1]);
		ASSERT(howmany > 0);
	}

	testobjp = new unref_fi::testobj_ptr[howmany];
	ASSERT(testobjp != NULL);

	os::printf("*** Complex unref(0-1 transition) with multi"
	    " references.\n");
	os::printf(" ** Set references counter to %d.\n", howmany);

	unref_fi::server_var	serverp;
	Environment		e;

	serverp = get_server();
	if (CORBA::is_nil(serverp)) {
		delete [] testobjp;
		return (1);
	}

	// Let server create the test object
	serverp->create_testobj(e);
	if (check_exception(e.exception(), "ERROR:")) {
		os::printf("ERROR: server can't create the test objection.\n");
		delete [] testobjp;
		return (1);
	}

	// get howmany references to testobj
	for (i = 0; i < howmany; i++) {
		testobjp[i] = serverp->get_testobj(e);
		if (check_exception(e.exception(), "ERROR:")) {
			os::printf("ERROR: can't get reference to testobj");

			// release other references
			for (j = 0; j < i; j++) {
				CORBA::release(testobjp[j]);
			}
			retval = 1;
			goto DONE;
		}
	}

	// release all the references
	for (i = 0; i < howmany; i++) {
		CORBA::release(testobjp[i]);
		testobjp[i] = nil;
	}

	// check if unreference is called
	serverp->check_unref(200, e);
	if (check_exception(e.exception(), "ERROR:")) {
		os::printf("ERROR: Error get unreferenced.");
		retval = 1;
		goto DONE;
	}

	// get howmany references to testobj (0-1)
	for (i = 0; i < howmany; i++) {
		testobjp[i] = serverp->get_testobj(e);
		if (check_exception(e.exception(), "ERROR:")) {
			os::printf("ERROR: can't get reference to testobj");

			// release other references
			for (j = 0; j < i; j++) {
				CORBA::release(testobjp[j]);
			}
			retval = 1;
			goto DONE;
		}
	}

	// release all the references
	for (i = 0; i < howmany; i++) {
		CORBA::release(testobjp[i]);
	}

	// check if unreference is called
	serverp->check_unref(200, e);
	if (check_exception(e.exception(), "ERROR:")) {
		os::printf("ERROR: Error get unreferenced.");
		retval = 1;
		goto DONE;
	}

DONE:	delete [] testobjp;
	// destroy the testobj
	serverp->del_testobj(e);
	if (check_exception(e.exception(), "ERROR:")) {
		os::printf("ERROR: Error destroy testobj.");
		return (1);
	}

	if (retval != 0) {
		return (retval);
	}

	// If get here, the test passed
	os::printf("PASS: Complex unref with multi references \n");
	return (0);
}
