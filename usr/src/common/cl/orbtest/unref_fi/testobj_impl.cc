/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)testobj_impl.cc	1.15	08/05/20 SMI"

//
// testobj_impl
//

#include <orbtest/unref_fi/testobj_impl.h>

//
// Used in src/orb/fault_unref.cc
// Used to store address of the handler for this test object.
// This address is used in fault_unref::generic_test_op() to
// identify if the test object is being unreferenced.
//
handler *test_obj_handlerp;

testobj_impl::testobj_impl() :
	_referenced(false)
{
}

testobj_impl::~testobj_impl()
{
	// If this had been referenced, then make
	// sure _unreferenced() has been called.
	lock();
	while (was_referenced() && ! unref_called()) {
		// run_test_fi can catch the console message
		os::printf("ERROR: testobj_impl: destructor called before "
		    "_unreferenced()\n");
	}

	// Reset test_obj_handlerp
	test_obj_handlerp = NULL;
	unlock();
}


void
testobj_impl::init()
{
	// Make sure only one test exists
	ASSERT(test_obj_handlerp == NULL);

	// Store away test_obj_handlerp
	test_obj_handlerp = _handler();
}

//
// Note, it's possible for each testobj to get re-referenced after
// it's been unreferenced.
//
void
testobj_impl::_unreferenced(unref_t cookie)
{
	lock();

	// Make sure delivery of _unreferenced() calls goes in the order of
	// of the object incarnations.
	ASSERT(unref_called() == false);

	// Mark flag whether this call is for the last reference.
	if (_last_unref(cookie)) {
		unref_called(true);
	}
	// unref_called(_last_unref(cookie));

	unlock();
}

//
// has this object been referenced at least once?
// Caller must have this object locked prior to calling
// this routine.
//
bool
testobj_impl::was_referenced()
{
	ASSERT(lock_held());
	return (_referenced);
}

//
// Return a new reference.
// Caller must have this object locked prior to calling this routine.
//
unref_fi::testobj_ptr
testobj_impl::get_newref()
{
	ASSERT(lock_held());
	_referenced = true;	// has been referenced at least once
	unref_called(false);		// reset flag
	return (get_objref());
}

//
// Interface
//

//
// Set test trigger. Since the trigger needs to carry testobj pointer
// information, it has to be set by server side.
//
// Each test has to set two triggers, one is for test setup and another is
// for test itself. Some tests share one setup trigger. The argument
// faultnum is the test trigger while the g_faultnum is the test setup trigger.
//
void
testobj_impl::set_trigger(uint_t faultnum, uint_t g_faultnum, int how_many,
    Environment &)
{
#if defined(_FAULT_INJECTION)

	//
	// arg and g_arg correspond to test trigger(faultnum) and setup
	// trigger(g_faultnum) respectively.
	//
	fault_unref::unref_arg_t	arg, g_arg;

	arg.numref = how_many;

	g_arg.numref = how_many;
	g_arg.op = fault_unref::CREATE_NEW_REF;

	switch (faultnum) {
	case FAULTNUM_UNREF_REGOBJ_PATH_1:
		arg.op = fault_unref::REGOBJ_UNREF_PATH_1;
		break;
	case FAULTNUM_UNREF_REGOBJ_PATH_2:
		arg.op = fault_unref::REGOBJ_UNREF_PATH_2;
		break;
	case FAULTNUM_UNREF_REGOBJ_PATH_3:
		arg.op = fault_unref::REGOBJ_UNREF_PATH_3;
		break;
	case FAULTNUM_UNREF_REGOBJ_PATH_4:
		arg.op = fault_unref::REGOBJ_UNREF_PATH_4;
		break;
	case FAULTNUM_UNREF_REGOBJ_PATH_5:
		arg.op = fault_unref::REGOBJ_UNREF_PATH_5;
		break;
	case FAULTNUM_UNREF_REGOBJ_PATH_6:
		arg.op = fault_unref::REGOBJ_UNREF_PATH_6;
		break;
	case FAULTNUM_UNREF_REGOBJ_PATH_7:
		arg.op = fault_unref::REGOBJ_UNREF_PATH_7;
		break;
	default:
		ASSERT(0);
	}

	NodeTriggers::add(faultnum, &arg, (uint32_t)sizeof (arg));
	NodeTriggers::add(g_faultnum, &g_arg, (uint32_t)sizeof (g_arg));
#else
	how_many = faultnum = g_faultnum = 0;	// shut off warning
	no_fault_injection();
#endif
}
