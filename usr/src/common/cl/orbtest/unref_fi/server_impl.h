/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ORBTEST_UNREF_FI_SERVER_IMPL_H
#define	_ORBTEST_UNREF_FI_SERVER_IMPL_H

#pragma ident	"@(#)server_impl.h	1.10	08/05/20 SMI"

//
// server_impl header file
//

#include <orb/object/adapter.h>
#include <h/unref_fi.h>

#include <orbtest/unref_fi/impl_common.h>
#include <orbtest/unref_fi/testobj_impl.h>

//
// Test server implementation.
//
class server_impl : public McServerof <unref_fi::server>, public impl_common {
	friend class testobj_impl;
public:
	server_impl();
	~server_impl();
	void	_unreferenced(unref_t);

	//
	// Initialize implementation object with the given name.
	// Returns: true if successful, false otherwise.
	//
	bool	init(char *strname);

	//
	// IDL Interfaces.
	//

	// For create a test obj
	void 	create_testobj(Environment &e);

	// For delete the created test obj
	void 	del_testobj(Environment &e);

	// Get a reference to the testobj
	unref_fi::testobj_ptr	get_testobj(Environment &e);

	// Check if the unref is called in testobj
	void	check_unref(uint32_t timeout, Environment &e);

	// For clients to tell this server that testing is done.
	void	done(Environment &e);
private:
	testobj_impl		*_testobjp;

	char			msg[128];	// used by Errmsg
};

#endif	/* _ORBTEST_UNREF_FI_SERVER_IMPL_H */
