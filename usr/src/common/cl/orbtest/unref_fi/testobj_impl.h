/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ORBTEST_UNREF_FI_TESTOBJ_IMPL_H
#define	_ORBTEST_UNREF_FI_TESTOBJ_IMPL_H

#pragma ident	"@(#)testobj_impl.h	1.11	08/05/20 SMI"

//
// Test object implementation. This is used by server_impl only
//

#include <orb/object/adapter.h>
#include <h/unref_fi.h>

#include <orbtest/unref_fi/impl_common.h>
#include <orbtest/unref_fi/server_impl.h>

class testobj_impl : public McServerof<unref_fi::testobj>, public impl_common {
	friend class server_impl;
public:
	void	_unreferenced(unref_t);

	// Returns a new reference.
	// Caller must have this object locked prior to calling this routine.
	unref_fi::testobj_ptr	get_newref();

	// Has this object been referenced at least once?
	// Caller must have this object locked prior to calling
	// this routine
	bool	was_referenced();

	// Initialize
	void	init();

	//
	// IDL Interfaces.
	//

	// To set the trigger
	void	set_trigger(uint_t faultnum, uint_t g_faultnum, int how_many,
		    Environment &e);

protected:
	// only server_impl can new this class
	testobj_impl();
	~testobj_impl();
private:

	bool		_referenced;	// true if this has been referenced
};

#endif	/* _ORBTEST_UNREF_FI_TESTOBJ_IMPL_H */
