/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rm_probe_unode.cc	1.11	08/05/20 SMI"

//
// rm_probe will probe the local rma about information about the rm
//

// Include Files
#include	<stdio.h>
#include	<string.h>

#include	<sys/os.h>
#include	<nslib/ns.h>

#include	<orbtest/rm_probe/rm_snap.h>
#include	<orbtest/rm_probe/rm_probe.h>

static const char	prefix[] = "rm_probe_unode:";

// Main entry point
int
rm_probe(int argc, char **argv)
{
	struct opt_t	*p_opt = NULL;
	int		retval;

	p_opt = get_options(argc, argv);
	if (p_opt == NULL) {
		return (1);
	}

	retval = do_probe(p_opt);
	delete(p_opt);
	return (retval);
}

//
// unode_init(void)
//
int
unode_init(void)
{
	os::printf("rm_probe module loaded\n");
	return (0);
}
