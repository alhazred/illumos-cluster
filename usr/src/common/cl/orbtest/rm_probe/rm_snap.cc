/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rm_snap.cc	1.33	08/05/20 SMI"

//
// rm_snap
//

// Include Files
#include	<sys/rm_util.h>
#include	<sys/os.h>
#include	<h/cmm.h>

#include	<orbtest/rm_probe/rm_snap.h>

static char	prefix[] = "rm_probe:";

static const char prov_state_str[7][15] = {
	"AS_DOWN",
	"AS_SPARE",
	"AS_SECONDARY",
	"AS_PRIMARY"
};

static const char service_state_str[4][15] = {
	"S_UP",
	"S_DOWN",
	"S_UNINIT",
	"S_UNSTABLE"
};

//
// Implementation of rm_snap
//

// Constructors

// Initialize
rm_snap::rm_snap()
{
	service_name = NULL;
	refreshed = false;
}

// Destructors
rm_snap::~rm_snap()
{
	delete [] (service_name);
}

// refresh(const char * svc_name)
// Wrapper for refreshing a new service
// Arguments: name of service
// Returns:
//	true if refresh was succesful
bool
rm_snap::refresh(const char *svc_name)
{
	// os::printf("rm_snap::refresh()\n");

	delete [] (service_name);
	service_name = NULL;

	if (svc_name != NULL) {
		// Copy the name
		service_name = new(char[os::strlen(svc_name)+1]);
		os::sprintf(service_name, "%s", svc_name);
		return (refresh());
	} else {
		return (refresh());
	}
}

// refresh()
// Does the actual work of refreshing the snapshot of the rm
// Arguments: none
// Returns:
//	true if refresh was succesful
bool
rm_snap::refresh()
{
	// os::printf("rm_snap::refresh()\n");

	svc = get_service(prefix);
	if (CORBA::is_nil(svc)) {
		os::printf("Could not find service.\n");
		return (false);
	}

	bool unstable = true;
	int count = 30; // at most wait 30 seconds for a service to be stable.
	while (count > 0 && unstable) {
		Environment e;
		replica::service_info *svinfo = NULL;
		svinfo = svc->get_service_info(e);
		if (e.exception()) {
			os::printf("Could not find service.\n");
			return (false);
		}
		if (svinfo == NULL) {
			os::printf("%s Could not get service info.\n",
			    prefix);
			return (false);
		}

		replica::service_state svstate = svinfo->s_state;
		delete svinfo;

		if (svstate == replica::S_UNSTABLE) {
			os::usecsleep(1000000);
			count--;
			continue;
		}
		if (get_repl_provs(prefix) != 0) {
			os::printf("Could not find providers for service.\n");
			return (false);
		}

		// There is a slight chance that the service becomes
		// unstable again between get_service_info and get_repl_provs.
		// We check the states we get in the repl_provs and if
		// they indicate that the service is unstable we do it
		// again.
		// Two cases for the service to be stable:
		// 1) there is one primary.
		// 2) all the replicas are in down state.
		int num_primary = 0;
		int num_sec = 0;
		int num_spare = 0;
		int num_down = 0;
		for (unsigned int i = 0; i < (repl_provs->length()); i++) {
			switch (repl_provs[i].curr_state) {
			case replica::AS_DOWN:
				num_down++;
				break;
			case replica::AS_SPARE:
				num_spare++;
				break;
			case replica::AS_SECONDARY:
				num_sec++;
				break;
			case replica::AS_PRIMARY:
				num_primary++;
				break;
			default:
				ASSERT(0);
				break;
			}
		}
		if (num_primary == 0 && num_down != repl_provs->length()) {
			os::usecsleep(1000000);
			count--;
			continue;
		}

		unstable = false;
	}

	refreshed = true;
	return (!unstable);
}

// get_service()
// Get the service_admin for a service
REPLICATION::service_admin_ptr
rm_snap::get_service(const char *msgpfx)
{
	if (service_name == NULL) {
		return (nil);
	}

	// os::printf("rm_snap::get_service(%s)\n", service_name);
	Environment e;

	replica::rm_admin_var	rm = rm_util::get_rm();
	ASSERT(!CORBA::is_nil(rm));

	REPLICATION::service_admin_var svc_ref =
		rm->get_control(service_name, e);
	if (e.exception()) {
		e.exception()->print_exception((char *)msgpfx);
		return (nil);
	}

	ASSERT(!CORBA::is_nil(svc_ref));
	return (REPLICATION::service_admin::_duplicate(svc_ref));
}

// get_repl_provs
// Arguments: A service_admin
// Returns: 0 upon success
int
rm_snap::get_repl_provs(
	const char *msgpfx)
{
	// os::printf("rm_snap::get_repl_provs()\n");

	Environment	e;

	// Get the repl_provs
	svc->get_repl_provs(repl_provs, e);

	if (e.exception()) {
		e.exception()->print_exception((char *)msgpfx);
		return (1);
	}
	return (0);
}

// find repl_prov()
// Arguments:	the description of the repl_prov we want
//		a sequence of repl_provs
// Returns:	the repl_prov we're after, of nil
//
// Finds teh repl_prov in the sequence of repl_prov that matches the desc.
REPLICATION::repl_prov_info *
rm_snap::find_repl_prov(const char *prov_desc)
{
	REPLICATION::repl_prov_info * info = NULL;

	if (prov_desc != NULL) {
		// os::printf("rm_snap::find_repl_prov(%s)\n", prov_desc);

		unsigned int i = 0;

		/* CSTYLED */
		for (i= 0; i < (repl_provs->length()); i++) {
			if (os::strcmp((char *)(repl_provs[i].repl_prov_desc),
				prov_desc) == 0) {
				info = &(repl_provs[i]);
			} // Match
		}
	}

	return (info);
}

// find primary_repl_prov()
// Arguments:	a sequence of repl_provs
// Returns:	the repl_prov we're after, or nil
//
// Finds the repl_prov in the sequence of repl_prov that is primary
REPLICATION::repl_prov_info *
rm_snap::find_primary_repl_prov()
{
	REPLICATION::repl_prov_info * info = NULL;

	// os::printf("rm_snap::find_primary_repl_prov()\n");

	/* CSTYLED */
	for (unsigned int i= 0; i < (repl_provs->length()); i++) {
		if (repl_provs[i].curr_state == REPLICATION::AS_PRIMARY)
			info = &(repl_provs[i]);
	}
	return (info);
}

// print_repl_prov_info()
// Arguments:	a pointer to the strucutre of the repl_prov_info
//
// prints the contents of the repl_prov_info to the screen
void
rm_snap::print_repl_prov_info(
	struct REPLICATION::repl_prov_info * info)
{
	if (info == NULL)
		return;

	os::printf(
		"rm_probe:\t%s\t%s\n",
		(char *)(info->repl_prov_desc),
		prov_state_str[info->curr_state - REPLICATION::AS_DOWN]);
}

// print_svc_info
//
// Arguments: pointer to a service_info structure
//
// Print to the screen the contents of a service_info struct
void
rm_snap::print_svc_info(REPLICATION::service_info * info)
{
	// os::printf("rm_snap::print_svc_info()\n");

	if (info == NULL)
		return;

	os::printf("name:\t%s\n", (char *)(info->service_name));
	os::printf("sid:\t%ld\n", info->sid);
	os::printf("state:\t%s\n",
		service_state_str[info->s_state - REPLICATION::S_UP]);
}

// print_prov
// Will print the snapshot information
// Argumetns: the description of a repl_prov
// Returns:
//	0 upon success
int
rm_snap::print_prov(const char *prov_desc)
{
	Environment e;
	REPLICATION::service_info * svc_info;
	unsigned int i;

	os::printf("rm_snap::print_prov()\n");

	if (!refreshed)
		return (false);

	svc_info = svc->get_service_info(e);
	if (e.exception()) {
		e.exception()->print_exception(prefix);
		return (false);
	}

	if (prov_desc == NULL) {
		// Print all the repl_prov_info's
		for (i = 0; i < repl_provs->length(); i++) {
			print_repl_prov_info(&(repl_provs[i]));
		}
	} else {
		// Only print the one we asked for
		print_repl_prov_info(find_repl_prov(prov_desc));
	}

	return (0);
}

// print_svc
// Will print the snapshot of a service if defined, otherwise all services
// Returns:
//	0 upon sucess
int
rm_snap::print_svc()
{
	// os::printf("rm_snap::print_svc()\n");

	if (CORBA::is_nil(svc)) {
		os::printf("Did not find a replicas in service\n");
		return (0);
	}

	Environment e;

	// print the service info
	print_svc_info(svc->get_service_info(e));
	if (e.exception()) {
		e.exception()->print_exception(prefix);
		return (1);
	}

	for (unsigned int i = 0; i < (repl_provs->length()); i++) {
		print_repl_prov_info(&(repl_provs[i]));
	}

	return (0);
}

// print_primary
// Will print the name of the primary repl_prov
// Returns:
//	0 upon success
int
rm_snap::print_primary()
{
	struct REPLICATION::repl_prov_info * p_info;

	p_info = find_primary_repl_prov();

	if (p_info == NULL) {
		os::printf("%s could not find repl_prov\n", prefix);
		return (1);
	}

	print_repl_prov_info(p_info);
	return (0);
}

// get_primary
// Will return the nodeid of the primary node
// Arguments:
//	a reference where to store the nodeid
// Returns:
//	0 upon success
int
rm_snap::get_primary(nodeid_t &nodeid)
{
	struct REPLICATION::repl_prov_info * p_info;

	p_info = find_primary_repl_prov();

	if (p_info == NULL) {
		os::printf("%s could not find repl_prov\n", prefix);
		return (1);
	}

	nodeid = os::atoi(p_info->repl_prov_desc);
	return (0);
}

// get_secondary
// Will return the nodeid of the nth secondary node
// Arguments
//	the number of secondary we're looking for, and a place to store the
//	nodeid
// Returns:
//	0 upon success
int
rm_snap::get_secondary(int n, nodeid_t &nodeid)
{
	// Secondary count
	int sec_count = 0;

	// Print all the repl_prov_info's
	for (unsigned int i = 0; i < repl_provs->length(); i++) {
		if (repl_provs[i].curr_state == REPLICATION::AS_SECONDARY) {
			sec_count++;
			if (sec_count == n) {
				nodeid = os::atoi(repl_provs[i].repl_prov_desc);
				return (0);
			}
		}
	}
	return (1);
}

// get_state
// Will return the state of the service
// Arguments
//	none
// Returns
//	the state of the service (replica::service_state)
int
rm_snap::get_state()
{
	if (CORBA::is_nil(svc)) {
		return (-1);
	}

	Environment e;
	REPLICATION::service_info *svc_info;

	svc_info = svc->get_service_info(e);
	if (e.exception()) {
		e.exception()->print_exception("replica::get_service_info()");
		return (-1);
	}

	return (svc_info->s_state);
}

// get_sid
// Will return the sid of the service
// Arguments
//	none
// Returns
//	the service id (0 on error)
REPLICATION::service_num_t
rm_snap::get_sid()
{
	if (CORBA::is_nil(svc)) {
		return (-1);
	}

	Environment e;
	REPLICATION::service_info *svc_info;

	svc_info = svc->get_service_info(e);
	if (e.exception()) {
		e.exception()->print_exception("replica::get_service_info()");
		return (-1);
	}

	return (svc_info->sid);
}

// Verify primary
// Will verify that the primary's match
// Returns:
//	0 if they match, otherwise 1
int
rm_snap::verify_prov_state(
	const char *prov_desc,
	REPLICATION::repl_prov_state target_state)
{
	struct REPLICATION::repl_prov_info * p_info;

	if (prov_desc == NULL)
		return (1);

	// os::printf("rm_snap::verify_primary(%s)\n", prov_desc);

	p_info = find_repl_prov(prov_desc);

	if (p_info == NULL) {
		os::printf("ERROR: could not find repl_prov %s\n", prov_desc);
		return (1);
	}

	if (p_info->curr_state != target_state) {
		os::printf("%s FAILED %s is not %s, BUT is %s\n",
		    prefix, prov_desc,
		    prov_state_str[target_state - REPLICATION::AS_DOWN],
		    prov_state_str[p_info->curr_state - REPLICATION::AS_DOWN]);
		return (1);
	} else {
		os::printf("%s SUCCESS %s is %s\n", prefix, prov_desc,
			prov_state_str[target_state - REPLICATION::AS_DOWN]);
		return (0);
	}

}

// get_primary_desc
// gets the description of the primary prov
char *
rm_snap::get_primary_desc()
{
	struct REPLICATION::repl_prov_info * p_info;

	p_info = find_primary_repl_prov();

	if (p_info == NULL) {
		os::printf("%s could not find repl_prov\n", prefix);
		return (NULL);
	}

	return (p_info->repl_prov_desc);
}
