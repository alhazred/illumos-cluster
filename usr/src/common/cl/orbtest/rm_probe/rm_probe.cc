/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rm_probe.cc	1.16	08/05/20 SMI"

#include	<sys/os.h>
#include	<h/replica.h>
#include	<stdlib.h>
#include	<stdio.h>
#include	<string.h>
#include	<unistd.h>
#include	<sys/time.h>

#include	<orbtest/rm_probe/rm_probe.h>
#include	<orbtest/rm_probe/rm_snap.h>

// Forward Declarations
void usage(const char *);
int do_printservice(struct opt_t *);
int do_printprov(struct opt_t *);
int do_findprimary(struct opt_t *);
int do_verifyprovstate(struct opt_t *);
int do_verifyswitchover(struct opt_t *);

// Do the probe action
int
do_probe(struct opt_t *p_opt)
{
	int retval;
	// os::printf("do_probe()\n");

	if (p_opt == NULL)
		return (1);

	// switch on the option
	switch (p_opt->opt_act) {
	case OPT_PRINTSERVICE :
		retval = do_printservice(p_opt);
		break;
	case OPT_PRINTPROV :
		retval = do_printprov(p_opt);
		break;
	case OPT_FINDPRIMARY :
		retval = do_findprimary(p_opt);
		break;
	case OPT_VERIFYPROVSTATE :
		retval = do_verifyprovstate(p_opt);
		break;
	case OPT_VERIFYSWITCHOVER :
		retval = do_verifyswitchover(p_opt);
		break;
	default :
		// Impossible option
		ASSERT(0);
		break;
	} // switch

	return (retval);
}

// Examine Command Line
// NULL returned on error
opt_t *
get_options(int argc, char ** argv)
{
	int arg;
	struct opt_t *p_opt;

	// Allocate p_opt
	p_opt = new struct opt_t;
	ASSERT(p_opt != NULL);

	p_opt->service_name = NULL;		// No Service default
	p_opt->prov_desc = NULL;		// No desc default
	p_opt->poll_interval = 1;		// 1 poll/sec time
	p_opt->timeout = 10;			// 10 sec timeout
	p_opt->target_state = REPLICATION::AS_PRIMARY;
	p_opt->target_prov_desc = NULL;		// No default target desc.
	p_opt->opt_act = OPT_PRINTSERVICE;	// Print the services avail

	// Examine the command line
	// Fix getopt so its re-entrant
	optind = 1;
	while ((arg = getopt(argc, argv, "p:t:d:s:")) != EOF) {
		switch (arg) {
		case 'd' :
			// Description of repl-prov
			p_opt->prov_desc = new char[os::strlen(optarg)+1];
			os::sprintf(p_opt->prov_desc, "%s", optarg);
			break;
		case 's' :
			// service name
			p_opt->service_name = new char[os::strlen(optarg)+1];
			os::sprintf(p_opt->service_name, "%s", optarg);
			break;
		case 'p' :
			// polling time (for polling services)
			p_opt->poll_interval = atoi(optarg);
			if (p_opt->poll_interval == 0) {
				os::printf("Poll-interval time must be > 0\n");
				delete(p_opt);
				return (NULL);
			}
			break;
		case 't' :
			// time-out time(in secs)
			p_opt->timeout = atoi(optarg);
			if (p_opt->timeout == 0) {
				os::printf("Timeout time must be > 0\n");
				delete(p_opt);
				return (NULL);
			}
			break;
		default	:
			usage(argv[0]);
			delete(p_opt);
			return (NULL);
		} // Switch
	} // while (commands)

	// Now we expect an action directive
	// Very crude method
	if (optind < argc) {
		if (os::strcmp(argv[optind], "print-service") == 0)
			p_opt->opt_act = OPT_PRINTSERVICE;
		else if (os::strcmp(argv[optind], "print-prov") == 0)
			p_opt->opt_act = OPT_PRINTPROV;
		else if (os::strcmp(argv[optind], "find-primary") == 0)
			p_opt->opt_act = OPT_FINDPRIMARY;
		else if (os::strcmp(argv[optind], "verify-prov-state") == 0) {
			p_opt->opt_act = OPT_VERIFYPROVSTATE;
			if (++optind < argc) {
				if (os::strcmp(argv[optind], "primary") == 0)
					p_opt->target_state =
						REPLICATION::AS_PRIMARY;
				else if (os::strcmp(argv[optind],
					"secondary") == 0)
					p_opt->target_state =
						REPLICATION::AS_SECONDARY;
				else if (os::strcmp(argv[optind],
					"down") == 0)
					p_opt->target_state =
						REPLICATION::AS_DOWN;
				else if (os::strcmp(argv[optind],
					"spare") == 0)
					p_opt->target_state =
						REPLICATION::AS_SPARE;
				else {
					os::printf("Invalid state to verify\n");
					usage(argv[0]);
					delete(p_opt);
					return (NULL);
				}
			} // argument for verify-prov-state
		} else if (os::strcmp(argv[optind], "verify-switchover") == 0) {
			p_opt->opt_act = OPT_VERIFYSWITCHOVER;
			if (++optind < argc) {
				p_opt->target_prov_desc = new char[strlen(
					argv[optind] + 1)];
				os::sprintf(p_opt->target_prov_desc,
					"%s", argv[optind]);
			}
		} else {
			os::printf("Invalid action defined.\n");
			usage(argv[0]);
			delete(p_opt);
			return (NULL);
		}
	}


	return (p_opt);
}

// print usage info
void
usage(const char *binname)
{
	os::printf("Usage:\t%s -s <svc_name> print-service\n", binname);
	os::printf("\t%s -s <svc_name> -d <prov_desc> print-prov\n", binname);
	os::printf("\t%s -s <svc_name> find-primary\n", binname);
	os::printf("\t%s -s <svc_name> -d <prov_desc> verify-prov-state"
		" <state>\n", binname);
	os::printf("\t%s -s <svc_name> -t <timeout> -p <poll-interval>"
		" verify-switchover\n", binname);
	os::printf("\n");
	os::printf("<prov-state> can be primary, secondary, spare, sync,"
		" freezing, frozen, or down\n");
	os::printf("<timeout> and <poll-interval> are in seconds.\n");
}

// Print a service
int
do_printservice(struct opt_t *p_opt)
{
	// os::printf("do_printservice()\n");

	// Create a snapshot object
	rm_snap		cur_snap;

	if (p_opt->service_name == NULL) {
		os::printf("You must specify a service name\n");
		return (1);
	}

	// Take a snapshot
	if (!cur_snap.refresh(p_opt->service_name))
		return (1);

	return (cur_snap.print_svc());
}

// Print a repl_prov
int
do_printprov(struct opt_t *p_opt)
{
	// os::printf("do_printprov()\n");

	// Create a snapshot object
	rm_snap		cur_snap;

	if (p_opt->service_name == NULL) {
		os::printf("You must specify a service name\n");
		return (1);
	}

	if (!cur_snap.refresh(p_opt->service_name))
		return (1);

	return (cur_snap.print_prov(p_opt->prov_desc));
}

// Find the primary prov in a service
int
do_findprimary(struct opt_t *p_opt)
{
	// os::printf("do_findprimary()\n");

	// Create a snapshot object
	rm_snap		cur_snap;

	if (p_opt->service_name == NULL) {
		os::printf("You must specify a service name\n");
		return (1);
	}

	if (!cur_snap.refresh(p_opt->service_name))
		return (1);

	return (cur_snap.print_primary());
}

// Verify that the primary prov
int
do_verifyprovstate(struct opt_t *p_opt)
{
	// os::printf("do_verifyprovstate()\n");

	// Create a snapshot object
	rm_snap		cur_snap;

	if (p_opt->service_name == NULL) {
		os::printf("You must specify a service name\n");
		return (1);
	}

	if (p_opt->prov_desc == NULL) {
		os::printf("You must specify a provider description\n");
		return (1);
	}

	if (!cur_snap.refresh(p_opt->service_name))
		return (1);

	return (cur_snap.verify_prov_state(p_opt->prov_desc,
		p_opt->target_state));
}

// Verify that a switchover occurred
// This must be called before forcing the switchover and backgrounded
int
do_verifyswitchover(struct opt_t *p_opt)
{
	// os::printf("do_verifyswichover()\n");

	// Create a snapshot object
	rm_snap		reference_snap;
	rm_snap		cur_snap;
	char		*reference_primary = NULL;
	char		*cur_primary = NULL;
	bool		switchover_detected = false;
	struct timeval	start;
	struct timeval	current;


	if (p_opt->service_name == NULL) {
		os::printf("You must specify a service name\n");
		return (1);
	}

	if (!reference_snap.refresh(p_opt->service_name))
		return (1);

	// get the reference primary_prov
	reference_primary = reference_snap.get_primary_desc();
	if (reference_primary == NULL)
		return (1);

	gettimeofday(&start, NULL);

	while (true) {
		// Take a snapshot
		if (!cur_snap.refresh(p_opt->service_name)) {
			return (1);
		}

		// Get the reference primary prov
		cur_primary = cur_snap.get_primary_desc();

		if (cur_primary != NULL) {
			// Compare
			if (os::strcmp(reference_primary, cur_primary) != 0) {
				switchover_detected = true;
				break;
			}
		}

		gettimeofday(&current, NULL);

		if ((current.tv_sec - start.tv_sec) > p_opt->timeout) {
			break;
		} else {
			sleep(p_opt->poll_interval);
		}
	}

	if (switchover_detected) {
		if (p_opt->target_prov_desc != NULL) {
			if (os::strcmp(p_opt->target_prov_desc, cur_primary)
				== 0) {
				os::printf("rm_probe: SUCCESS switchover from"
					" %s to %s detected\n",
					reference_primary, cur_primary);
				return (0);
			} else {
				os::printf("rm_probe: FAILED switchover from"
					" %s to %s detected (not %s)\n",
					reference_primary, cur_primary,
					p_opt->target_prov_desc);
				return (1);
			} // If we switchover to a expected target
		} else {
			os::printf("rm_probe: SUCCESS switchover from"
				" %s to %s detected\n",
				reference_primary, cur_primary);
			return (0);
		} // Did we ask for a specific switch
	} else {
		os::printf("rm_probe: FAILED switchover did not occur\n");
		return (1);
	}
}
