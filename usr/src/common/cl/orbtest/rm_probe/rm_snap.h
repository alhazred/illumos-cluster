/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RM_SNAP_H
#define	_RM_SNAP_H

#pragma ident	"@(#)rm_snap.h	1.21	08/05/20 SMI"

//
// rm_snap
//
// Object will gather snapshot information about the state of the replica
// manager
//

// Include Files
#include	<repl/service/repl_service.h>
#include	<h/cmm.h>

#define		REPLICATION	replica

class rm_snap {
public:
	// Constructor
	rm_snap();

	// Take a snapshot of the system
	bool refresh(const char *svc_name);

	// Get information
	int get_primary(nodeid_t &nodeid);
	int get_secondary(int num, nodeid_t &nodeid);
	int get_state();
	REPLICATION::service_num_t get_sid();
	char *get_primary_desc();


	// Print information
	int print_svc();
	int print_prov(const char *prov_desc = NULL);
	int print_primary();
	int verify_prov_state(
		const char *prov_desc,
		REPLICATION::repl_prov_state target_state);

	// Destructor
	~rm_snap();

protected:

private:
	// Actual Refresh
	bool refresh();

	// Get the service admin ptr
	REPLICATION::service_admin_ptr get_service(const char *);

	// Get the repl_provs (this is really a sequence of repl_prov info's
	int get_repl_provs(
		const char *msgpfx);

	// Find a repl_prov
	REPLICATION::repl_prov_info *find_repl_prov
		(const char *prov_desc);

	// Find the primary repl_prov
	REPLICATION::repl_prov_info *find_primary_repl_prov();

	// Print the information
	void print_svc_info(REPLICATION::service_info *svc_info);

	// Print the repl_prov info
	void print_repl_prov_info
		(REPLICATION::repl_prov_info *info);

	// Data members

	// Name of service we're shooting
	char *service_name;

	// Has been refreshed
	bool	refreshed;

	// Managed pointer to the rma admin ref.
	REPLICATION::rm_admin_var	rma;

	// Managed pointer to the service admin ref.
	REPLICATION::service_admin_var	svc;

	// Sequence of repl_provs
	REPLICATION::repl_prov_seq_var	repl_provs;
};

#endif	/* _RM_SNAP_H */
