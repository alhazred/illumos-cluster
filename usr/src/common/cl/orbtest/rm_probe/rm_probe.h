/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RM_PROBE_H
#define	_RM_PROBE_H

#pragma ident	"@(#)rm_probe.h	1.11	08/05/20 SMI"

#include	<sys/os.h>
#include	<orbtest/rm_probe/rm_snap.h>

// Action type
enum opt_act_t {
	OPT_PRINTSERVICE,
	OPT_PRINTPROV,
	OPT_FINDPRIMARY,
	OPT_VERIFYPROVSTATE,
	OPT_VERIFYSWITCHOVER
};

// Command line structure
struct opt_t {
	char				*service_name;
	char				*prov_desc;
	int				poll_interval;
	int				timeout;
	REPLICATION::repl_prov_state	target_state;
	char				*target_prov_desc;

	opt_act_t			opt_act;
};

// Examine Command line options
opt_t *get_options(int argc, char **argv);

// Carry out the defined action
int do_probe(struct opt_t *p_opt);

#endif	/* _RM_PROBE_H */
