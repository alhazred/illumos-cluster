/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_kproxy_client.cc	1.10	08/05/20 SMI"

#include <orbtest/test_kproxy/test_kproxy_aux.h>

#include <sys/os.h>
#include <h/naming.h>
#include <nslib/ns.h>
#include <h/test_kproxy.h>
#include <orb/infrastructure/orb.h>

//
// Usage
//
void
print_usage(const char *fname)
{
	os::printf("Usage: %s fname arg1, arg2, ..., argn\n\n", fname);
}

//
// The KProxy client will get a reference to the ORB KPROXY server on the local
// node.
//
// Then it will invoke the execute interface to run execute on the kernel
//
int
main(int argc, char *argv[])
{
	// Prepere the arguments to be passed to the KProxy server
	if (argc < 2) {
		print_usage(argv[0]);
		return (1);
	}

	if (ORB::initialize() != 0) {
		os::printf("ERROR: Cannot initialize ORB\n");
		return (1);
	}

	int				f_argc = argc - 1;
	char				*f_name = argv[1];
	test_kproxy::StringSeq		f_argv(f_argc, f_argc);

	for (int i = 1; i < argc; i++) {
		// Copy the argc into the sequence
		f_argv[i - 1] = (const char *)argv[i];
	}

	Environment			e;
	naming::naming_context_var	ctxp = ns::local_nameserver();
	// Find the KPROXY server (on local nameserver)
	CORBA::Object_var		obj_ref = ctxp->resolve(KPROXY_NAME, e);
	if (e.exception()) {
		e.exception()->print_exception("resolve");
		return (1);
	}

	// Narrow the object
	test_kproxy::server_var	sp = test_kproxy::server::_narrow(obj_ref);
	ASSERT(!CORBA::is_nil(sp));

	// Now invoke execute
	int16_t rslt = sp->execute(f_name, f_argv, true, e);
	if (e.exception()) {
		e.exception()->print_exception("test_kproxy::execute");
		return (1);
	}

	return (rslt);
}
