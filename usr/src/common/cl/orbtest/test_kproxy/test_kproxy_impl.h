/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _TEST_KPROXY_IMPL_H
#define	_TEST_KPROXY_IMPL_H

#pragma ident	"@(#)test_kproxy_impl.h	1.11	08/05/20 SMI"

#include <h/test_kproxy.h>
#include <sys/os.h>
#include <orb/object/adapter.h>

//
// Test Kernel Proxy
//
// This test ORB server will facilitate running kernel level tests from user
// space, by fielding requests from user level clients, to execute predefined
// tests, with arbitrary arguments.
//
// See interfaces/test_kproxy.idl for more info
//
class test_kproxy_impl : public McServerof<test_kproxy::server> {
public:
	// Constructor
	test_kproxy_impl() : McServerof<test_kproxy::server>() { };

	// Destructor
	~test_kproxy_impl() { };

	// IDL Members

	//
	// Execute tests
	// This routine will take a routine name and match it with a
	// routine defined in the impl object of test_kproxy::server
	// the definition will have a static function associated
	// with it that will be executed in kernel space.
	//
	// Arguments:
	//	fname		- name of function to run
	//	argv		- argument sequence
	//	wait_for_rslts	- do we want to wait for results
	//	e		- ORB environment
	//
	int16_t execute(const char *fname,
	    const test_kproxy::StringSeq &argv, bool wait_for_rslts,
	    Environment &e);

	void	_unreferenced(unref_t);
}; // class test_kproxy_impl

#endif	/* _TEST_KPROXY_IMPL_H */
