/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_kproxy_server_aux.cc	1.8	08/05/20 SMI"

#include <orbtest/test_kproxy/test_kproxy_server_aux.h>
#include <orbtest/test_kproxy/test_kproxy_aux.h>

#include <sys/os.h>
#include <nslib/ns.h>
#include <h/naming.h>
#include <h/test_kproxy.h>
#include <orbtest/test_kproxy/test_kproxy_impl.h>

// Initialization routine (install server)
int
kproxy_init()
{
	os::printf("Initializing the KProxy Test Server\n");

	Environment		e;
	test_kproxy::server_var	server_ref;
	server_ref = (new test_kproxy_impl)->get_objref();
	ASSERT(!CORBA::is_nil(server_ref));

	// Bind it into the name server (with a well known name)
	naming::naming_context_var ctxp = ns::local_nameserver();
	ASSERT(!CORBA::is_nil(ctxp));

	ctxp->rebind((const char *)KPROXY_NAME, server_ref, e);
	if (e.exception()) {
		e.exception()->print_exception("rebind");
		return (1);
	}

	return (0);
}

// Stop server
int
kproxy_fini()
{
	Environment	e;

	os::printf("Stopping the KProxy Test Server\n");

	naming::naming_context_var ctxp = ns::local_nameserver();
	ASSERT(!CORBA::is_nil(ctxp));

	// Unbind the server
	ctxp->unbind((const char *)KPROXY_NAME, e);
	if (e.exception()) {
		e.exception()->print_exception("unbind");
		return (1);
	}

	return (0);
}
