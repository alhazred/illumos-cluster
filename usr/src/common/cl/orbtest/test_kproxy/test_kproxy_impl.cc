/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_kproxy_impl.cc	1.10	08/05/20 SMI"

#include <orbtest/test_kproxy/test_kproxy_impl.h>
#include <sys/os.h>

//
// File SCOPE variable that maps the string of the function name
// to the function
//

#include <orbtest/repl_sample/repl_sample_impl_aux.h>
#include <orbtest/ha_stress/ha_stress_start.h>

// Convenience macro
#define	ENTRY(f)	{f, #f}

struct	_entry_t {
	int (*entry) (int argc, char *argv[]);		// Function
	char	*name;					// Name
};

_entry_t entry_list[] = {
	ENTRY(ha_sample_server),
	ENTRY(ha_stress_start_client),
	ENTRY(NULL)
};


void
#ifdef DEBUG
test_kproxy_impl::_unreferenced(unref_t cookie)
#else
test_kproxy_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	delete this;
}


//
// IDL Invocation Implementation
//
//
// Execute tests
// This routine will take a routine name and match it with a
// routine defined in the impl object of test_kproxy::server
// the definition will have a static function associated
// with it that will be executed in kernel space.
//
// Arguments:
//	fname		- name of function to run
//	argv		- argument sequence
//	wait_for_rslts	- do we want to wait for results
//	e		- ORB environment
//
int16_t
test_kproxy_impl::execute(const char *fname, const test_kproxy::StringSeq &argv,
	bool, Environment &e)
{
	bool	found		= false;
	char	**f_argv	= NULL;
	int	i, j, f_argc;
	size_t	len;
	int16_t	rslt;

	// Match the fname to the list of entries
	for (i = 0; entry_list[i].entry != NULL; i++) {
		if (os::strcmp(entry_list[i].name, fname) == 0) {
			found = true;
			break;
		}
	}

	// Did we find what we were looking for ?
	if (found) {
		// Generate a vector or args from the string sequence
		f_argv = new char*[argv.length()+1];
		f_argc = argv.length();

		// Copy the sequence of strings to the vector of strings
		for (j = 0; j < argv.length(); j++) {
			len = os::strlen(argv[j]);
			f_argv[j] = new char[len + 1];
			os::sprintf(f_argv[j], "%s", (const char *)argv[j]);
		}

		// Add a NULL terminator to the vector
		f_argv[argv.length()] = NULL;

		// Call the function we found
		rslt = entry_list[i].entry(f_argc, f_argv);
	} else {
		// Return exception in environment
		e.exception(new test_kproxy::fname_not_found);
	}

	return (rslt);
} // execute
