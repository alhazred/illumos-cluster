/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ORBTEST_RECONF_FI_TESTOBJ_IMPL_H
#define	_ORBTEST_RECONF_FI_TESTOBJ_IMPL_H

#pragma ident	"@(#)testobj_impl.h	1.9	08/05/20 SMI"

#include <orbtest/reconf_fi/support.h>

//
// Test object implementation.
//
/* CSTYLED */
class testobj_impl : public McServerof<orphan_fi::testobj>,
		public impl_common {
public:
	testobj_impl();
	~testobj_impl();
	void	_unreferenced(unref_t);

	// Initialize implementation object with the given name.
	// Returns: true if successful, false otherwise.
	bool	init(char *strname);

	//
	// IDL Interfaces.
	//

	// For testing that this object impl is still alive.  It
	// simply adds l1 and l2, and returns the total.
	int32_t	add(int32_t l1, int32_t l2, Environment &);

	// Get the node ID of this server.
	sol::nodeid_t	get_nodeid(Environment &);

	// For clients to tell this server that testing is done.
	void	done(Environment &);
};

#endif	/* _ORBTEST_RECONF_FI_TESTOBJ_IMPL_H */
