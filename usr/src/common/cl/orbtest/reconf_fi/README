#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)README	1.6	08/05/20 SMI"
#

                    Reconfiguration Fault Injection Tests
_______________________________________________________________________________

HOW TO RUN
==========
1) On real clusters.

	XXXXX NOTE: This test is not yet supported on real clusters.
	XXXXX The following will be the instructions when it is supported.

	To run the reconfiguration tests on a cluster do the following from
	a host NOT part of that cluster:

		cd <workspace>/src/orbtest/reconf_fi
		./run_test_fi -c <cluster_name>

	This will run all the tests on the cluster <cluster_name>.  Whenever
	possible, clients and servers will be run in user-land.

	To force clients and servers to run in kernel mode, do the following:

		cd <workspace>/src/orbtest/reconf_fi
		./run_test-fi -c <cluster_name> -k

	For more info on run_test_fi, enter

		./run_test_fi -?

2) On UNODE clusters.

	To run the reconfiguration tests on a UNODE cluster do the following:

		cd <workspace>/src/orbtest/reconf_fi
		./run_unode_fi

	For more info on run_unode_fi, enter

		./run_unode_fi -?

_______________________________________________________________________________

TEST DESCRIPTIONS
=================

The tests are specified in the file

	<workspace>/src/orbtest/reconf_fi/fault.data

Below is a description for each of the tests, including the strategy used.


-------------------------------------------------------------------------------
TEST 1

ASSERTION:
	During step 1 of reconfiguration nodes wait for confirmation messages
	for their REG messages associated with xdoors received from nodes which
	are now dead.
	When the confirmation messages come in the reconfiguration
	thread should wake up and the reconfiguration should complete.

TEST STRATEGY:
	Use a 3 node cluster.
	Put the name server primary on node 2
	Tell node 3 to defer sending confirm messages.
	Tell node 3 to create an object and bind it in the name server.
	The name server checkpoints this binding to nodes 1 and 3. This means
	that nodes 1 and 3 will receive a reference to the object on node 3
	and will register with it. Node 3 will not send a confirmation
	because it is deferring them.
	Kill node 2 (the sender of the references to nodes 1 and 3).
	Wait for node 1 to block in step 1 of the subsequent reconfiguration.
	Tell node 3 to start sending its confirm messages again.
	Verify that the reconfiguration thread on node 1 gets unblocked.

-------------------------------------------------------------------------------
TEST 2

ASSERTION:
	During step 1 of reconfiguration nodes wait for confirmation messages
	for their REG messages associated with xdoors received from nodes which
	are now dead.
	If a membership changes occurs before these confirm messages come
	in, then the thread should unblock and allow the next reconfiguration
	to execute. This tests the step1 cancellation code.

TEST STRATEGY:
	Use a 4 node cluster.
	Put the name server primary on node 2
	Tell node 3 to defer sending confirm messages.
	Tell node 3 to create an object and bind it in the name server.
	The name server checkpoints this binding to nodes 1 and 3. This means
	that nodes 1 and 3 will receive a reference to the object on node 3
	and will register with it. Node 3 will not send a confirmation
	because it is deferring them.
	Kill node 2 (the sender of the references to nodes 1 and 3).
	Wait for node 1 to block in step 1 of the subsequent reconfiguration.
	Kill node 4 forcing a new reconfiguration
	Wait for node 1 to unblock (because of the cancellation)
	Wait for node 1 to block again in the next reconfiguration.
	Tell node 3 to start sending its confirm messages again.
	Verify that the reconfiguration thread on node 1 gets unblocked.

-------------------------------------------------------------------------------
TEST 3

ASSERTION:
	During step 1 of reconfiguration nodes wait for confirmation messages
	for their REG messages associated with xdoors received from nodes which
	are now dead.
	If the node which should send the confirm then dies and never
	sends it, the reconfiguration thread should unblock and allow the
	next reconfiguration to occur.

TEST STRATEGY:
	Use a 3 node cluster.
	Put the name server primary on node 2
	Tell node 3 to defer sending confirm messages.
	Tell node 3 to create an object and bind it in the name server.
	The name server checkpoints this binding to nodes 1 and 3. This means
	that nodes 1 and 3 will receive a reference to the object on node 3
	and will register with it. Node 3 will not send a confirmation
	because it is deferring them.
	Kill node 2 (the sender of the references to nodes 1 and 3).
	Wait for node 1 to block in step 1 of the subsequent
	reconfiguration waiting for a confirmation from node 3.
	Kill node 3.
	Verify that the reconfiguration thread on node 1 gets unblocked.

-------------------------------------------------------------------------------
TEST 4

ASSERTION:
	During step 3 of a reconfiguration where the primary for an ha service
	has died, the hxdoor code waits for all marshals of that service's
	hxdoors to be acknowledged.
	When the acks come in, the reconfiguration thread should unblock and
	the reconfiguration should complete.

TEST STRATEGY:
	Use a 3 node cluster.
	Put the name server primary on node 1
	Tell node 3 to stop sending in ack messages for xdoors that it receives
	Start an ha service with a primary on node 2 and a secondary on node 1
	Have that service bind an object to the name server.
	The name server checkpoints this binding to nodes 2 and 3. This means
	that nodes 2 and 3 will receive a reference to the HA object.
	Node 3 will not send an ack for this hxdoor.
	Kill node 2, the primary of the ha service.
	Wait for the reconfiguration to block in step 3 on node 1, waiting
	for an ack for the hxdoor.
	Enable sending of acks on node 3.
	Wait for the reconfiguration to unblock on node 1.


-------------------------------------------------------------------------------
TEST 5

ASSERTION:
	During step 3 of a reconfiguration where the primary for an ha service
	has died, the hxdoor code waits for all marshals of that service's
	hxdoors to be acknowledged.
	If the node which received the marshals dies, it will never send the
	acks. In this case the reconfiguration step should unblock and a new
	reconfiguration should occur.

TEST STRATEGY:
	Use a 3 node cluster.
	Put the name server primary on node 1
	Tell node 3 to stop sending in ack messages for xdoors that it receives
	Start an ha service with a primary on node 2 and a secondary on node 1
	Have that service bind an object to the name server.
	The name server checkpoints this binding to nodes 2 and 3. This means
	that nodes 2 and 3 will receive a reference to the HA object.
	Node 3 will not send an ack for this hxdoor.
	Kill node 2, the primary of the ha service.
	Wait for the reconfiguration to block in step 3 on node 1, waiting
	for an ack for the hxdoor.
	Kill node 3
	Wait for the reconfiguration to unblock on node 1.

-------------------------------------------------------------------------------
TEST 6

ASSERTION:
	During step 3 of a reconfiguration where the primary for an ha
	service has died and the ha service was in the process of
	reconnecting to that new primary, the reconfiguration step blocks
	waiting for the reconnect to complete (this should happen
	quickly, since the reconnection consists of invocations to the
	now dead primary node).
	Once the reconnection completes, the reconfiguration should continue.

TEST STRATEGY:
	Use a 3 node cluster.
	Move all ha services to have their primary on node 1 - so that
	the fault injection point is guaranteed to be hit by our
	service (this could be enhanced to programmatically determine
	which service the fault applies to and make the fault point
	apply to just it).
	Start an ha service with a primary on node 2 and a secondary on node 3.
	Tell node 1 to block the next time it does a reconnect to a
	remote primary.
	Kill node 2 causing a failover to node 3. Node 1 will block when
	reconnecting to node 3.
	Wait for node 1 to be blocked in reconnect.
	Kill node 3 - the new primary.
	Wait for the reconfiguration to block on node 1 waiting for the
	reconnect to complete.
	Tell node 1 to unblock the reconnect.
	Wait for node 1's reconfiguration thread to unblock.

-------------------------------------------------------------------------------
TEST 7

ASSERTION:
	During step 3 of a reconfiguration where the primary for an ha
	service has died and the ha service was in the process of
	reconnecting to that new primary, the reconfiguration step blocks
	waiting for the reconnect to complete (this should happen
	quickly, since the reconnection consists of invocations to the
	now dead primary node).
	If another node dies while this is in progress, the reconfiguration
	thread should return, enabling the next reconfiguration.

TEST STRATEGY:
	Use a 4 node cluster.
	Move all ha services to have their primary on node 1 - so that
	the fault injection point is guaranteed to be hit by our
	service (this could be enhanced to programmatically determine
	which service the fault applies to and make the fault point
	apply to just it).
	Start an ha service with a primary on node 2 and a secondary on node 3.
	Tell node 1 to block the next time it does a reconnect to a
	remote primary.
	Kill node 2 causing a failover to node 3. Node 1 will block when
	reconnecting to node 3.
	Wait for node 1 to be blocked in reconnect.
	Kill node 3 - the new primary.
	Wait for the reconfiguration to block on node 1 waiting for the
	reconnect to complete.
	Kill node 4.
	Wait for node 1's reconfiguration thread to unblock.
	Tell node 1 to unblock the reconnect.

-------------------------------------------------------------------------------
