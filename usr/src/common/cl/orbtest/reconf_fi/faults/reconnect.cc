/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)reconnect.cc	1.8	08/05/20 SMI"

#include <orbtest/reconf_fi/faults/entries.h>
#include <orbtest/reconf_fi/faults/entry_support.h>
#include <repl/rma/rma.h>

//
// Entry function to tell node to enable fault to delay unref messages.
//
int
block_reconnect(int, char *[])
{
#if defined(_FAULT_INJECTION)
	NodeTriggers::add(FAULTNUM_RECONF_DELAY_RECONNECT, NULL, 0);
	return (0);
#else
	return (no_fault_injection());
#endif
}

//
// Entry function to tell node stop delaying unref messages.
//
int
unblock_reconnect(int, char *[])
{
#if defined(_FAULT_INJECTION)
	NodeTriggers::clear(FAULTNUM_RECONF_DELAY_RECONNECT);
	hxdoor_service::unblock_reconnect();
	return (0);
#else
	return (no_fault_injection());
#endif
}

//
// Entry function to tell node stop delaying unref messages.
//
int
wait_for_blocked_reconnect(int, char *[])
{
#if defined(_FAULT_INJECTION)
	hxdoor_service::wait_for_blocked_reconnect();
	return (0);
#else
	return (no_fault_injection());
#endif
}

#if defined(_FAULT_INJECTION)
int
refcount_wait_for_step3_blocked(int, char *[])
{
	rma::the().wait_for_step3_blocked();
	return (0);
}

int
refcount_wait_for_step3_unblocked(int, char *[])
{
	rma::the().wait_for_step3_unblocked();
	return (0);
}

#else

int
refcount_wait_for_step3_blocked(int, char *[])
{
	return (no_fault_injection());
}

int
refcount_wait_for_step3_unblocked(int, char *[])
{
	return (no_fault_injection());
}

#endif
