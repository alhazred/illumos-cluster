/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ORBTEST_RECONF_FI_FAULTS_ENTRIES_H
#define	_ORBTEST_RECONF_FI_FAULTS_ENTRIES_H

#pragma ident	"@(#)entries.h	1.11	08/05/20 SMI"

#include <sys/os.h>
#include <orb/invo/common.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/fault/fault_injection.h>
#include <h/orphan_fi.h>
#include <orbtest/reconf_fi/support.h>

//
// Test entry functions.
//

int	server_done(int, char *[]);
int	testobj_done(int, char *[]);

int	refcount_delay_confirm_enable(int, char *[]);
int	refcount_delay_confirm_disable(int, char *[]);
int	refcount_delay_ack_enable(int, char *[]);
int	refcount_delay_ack_disable(int, char *[]);
int	refcount_wait_for_step1_blocked(int, char *[]);
int	refcount_wait_for_step1_unblocked(int, char *[]);
int	refcount_wait_for_step3_blocked(int, char *[]);
int	refcount_wait_for_step3_unblocked(int, char *[]);
int	block_reconnect(int, char *[]);
int	unblock_reconnect(int, char *[]);
int	wait_for_blocked_reconnect(int, char *[]);

#endif	/* _ORBTEST_RECONF_FI_FAULTS_ENTRIES_H */
