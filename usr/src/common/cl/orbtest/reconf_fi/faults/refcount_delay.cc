/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)refcount_delay.cc	1.9	08/05/20 SMI"

#include <orbtest/reconf_fi/faults/entries.h>
#include <orbtest/reconf_fi/faults/entry_support.h>
#include <orb/refs/refcount.h>

//
// Entry function to tell node to enable fault to delay unref messages.
//
int
refcount_delay_confirm_enable(int, char *[])
{
#if defined(_FAULT_INJECTION)
	NodeTriggers::add(FAULTNUM_RECONF_DEFER_CONFIRM, NULL, 0);
	return (0);
#else
	return (no_fault_injection());
#endif
}

//
// Entry function to tell node stop delaying unref messages.
//
int
refcount_delay_confirm_disable(int, char *[])
{
#if defined(_FAULT_INJECTION)
	NodeTriggers::clear(FAULTNUM_RECONF_DEFER_CONFIRM);
	return (0);
#else
	return (no_fault_injection());
#endif
}

//
// Entry function to tell node to enable fault to delay unref messages.
//
int
refcount_delay_ack_enable(int, char *[])
{
#if defined(_FAULT_INJECTION)
	NodeTriggers::add(FAULTNUM_RECONF_DEFER_ACK, NULL, 0);
	return (0);
#else
	return (no_fault_injection());
#endif
}

//
// Entry function to tell node stop delaying unref messages.
//
int
refcount_delay_ack_disable(int, char *[])
{
#if defined(_FAULT_INJECTION)
	NodeTriggers::clear(FAULTNUM_RECONF_DEFER_ACK);
	return (0);
#else
	return (no_fault_injection());
#endif
}

#if defined(_FAULT_INJECTION)
int
refcount_wait_for_step1_blocked(int argc, char *argv[])
{
	if (argc != 2) {
		os::printf("ERROR: Wrong number of arguments\n");
		return (1);
	}
	nodeid_t nd = (nodeid_t)os::atoi(argv[1]);
	if (nd > NODEID_MAX || nd < 1) {
		os::printf("ERROR: bad nodeid %d\n", nd);
		return (1);
	}
	refcount::the().wait_for_step1_blocked(nd);
	return (0);
}

int
refcount_wait_for_step1_unblocked(int argc, char *argv[])
{
	if (argc != 2) {
		os::printf("ERROR: Wrong number of arguments\n");
		return (1);
	}
	nodeid_t nd = (nodeid_t)os::atoi(argv[1]);
	if (nd > NODEID_MAX || nd < 1) {
		os::printf("ERROR: bad nodeid %d\n", nd);
		return (1);
	}
	refcount::the().wait_for_step1_unblocked(nd);
	return (0);
}

#else

int
refcount_wait_for_step1_blocked(int, char *[])
{
	return (no_fault_injection());
}

int
refcount_wait_for_step1_unblocked(int, char *[])
{
	return (no_fault_injection());
}

#endif
