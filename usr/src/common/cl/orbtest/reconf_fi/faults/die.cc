/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)die.cc	1.7	08/05/20 SMI"

#include <orbtest/reconf_fi/faults/entries.h>
#include <unistd.h>


//
// Entry function to tell node to die. This only works in unode mode.
// We want to die and not reboot because we are testing the reconfiguration
// caused by the death of the node and the other supported ways of killing
// nodes by the framework wait for the node to reboot and rejoin the cluster.
// To achieve this we do an _exit() which is a bit hacky. When this is
// supported in a cleaner way by the framework we can clean it up.
//
int
die(int, char *[])
{
#if defined(_FAULT_INJECTION) && defined(_KERNEL_ORB) && !defined(_KERNEL)
	_exit(0);
	return (0);
#else
	return (no_fault_injection());
#endif
}
