/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)client.cc	1.7	08/05/20 SMI"

#include <orbtest/fi_support/fi_driver.h>
#include <orbtest/reconf_fi/support.h>
#include <orbtest/reconf_fi/faults/entries.h>

// Test entry functions.
fi_entry_t fi_entry[] = {

	ENTRY(server_done),
	ENTRY(testobj_done),

	ENTRY(refcount_delay_confirm_enable),
	ENTRY(refcount_delay_confirm_disable),
	ENTRY(refcount_delay_ack_enable),
	ENTRY(refcount_delay_ack_disable),
	ENTRY(refcount_wait_for_step1_blocked),
	ENTRY(refcount_wait_for_step1_unblocked),
	ENTRY(refcount_wait_for_step3_blocked),
	ENTRY(refcount_wait_for_step3_unblocked),
	ENTRY(block_reconnect),
	ENTRY(unblock_reconnect),
	ENTRY(wait_for_blocked_reconnect),

	ENTRY(NULL)
};

// ---------------------------------------------------------------------------
// KERNEL client
// ---------------------------------------------------------------------------
#if defined(_KERNEL)

extern "C" {
	void _cplpl_init(void);
	void _cplpl_fini(void);
}

char _depends_on[] = "misc/cl_runtime misc/cl_load misc/cl_orb "
	"misc/cl_haci misc/cl_quorum misc/cl_comm misc/fi_driver";

extern struct mod_ops mod_miscops;

struct modlmisc modlmisc = {
	&mod_miscops, "orphan FI test client"
};

struct modlinkage modlinkage = {
	MODREV_1, { (void *)&modlmisc, NULL }
};

int
_init(void)
{
	int	error = 0;

	if (error = mod_install(&modlinkage)) {
		return (error);
	}
	_cplpl_init();				// C++ initialization

#if defined(_FAULT_INJECTION)
	struct modctl	*mp;
	int		argc = 0;
	char		**argv = NULL;
	int		i, rslt;

	// Get module name so we know what test case to call
	if ((mp = mod_getctl(&modlinkage)) == NULL) {
		os::printf("ERROR: can't find modctl\n");
		_cplpl_fini();
		mod_remove(&modlinkage);
		return (EIO);
	}

	// Parse module name to get arguments
	mod_name_to_args(mp->mod_modname, argc, argv);

	// Do the test.
	if (rslt = do_fi_test(argc, argv, fi_entry)) {
		// Map test failures/errors to ECANCELED.
		error = ECANCELED;
	}
	os::printf("    Result From Module: %d\n", rslt);

	// Clean up argument vectors allocated by mod_name_to_args().
	for (i = 0; i < argc; ++i) {
		delete[] argv[i];
	}
	delete[] argv;
#else
	no_fault_injection();
	error = ENOTSUP;
#endif

	if (error) {
		_cplpl_fini();
		mod_remove(&modlinkage);
	}

	return (error);
}

int
_fini(void)
{
	_cplpl_fini();
	return (mod_remove(&modlinkage));
}

int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}

#endif // _KERNEL

// ---------------------------------------------------------------------------
// USER client
// ---------------------------------------------------------------------------
#if defined(_USER)

main(int argc, char *argv[])
{
#if defined(_FAULT_INJECTION)
	int	rslt;

	if (ORB::initialize() != 0) {
		os::printf("ERROR: Can't initialize orb\n");
		return (1);
	}

	// The Fault Injection test driver passes the name of the
	// entry test function to execute and its arguments.
	rslt = do_fi_test(argc, argv, fi_entry);
	return (rslt);
#else
	argc; argv;				// to shut compiler warning
	return (no_fault_injection());
#endif
}

#endif // _USER

// ---------------------------------------------------------------------------
// UNODE client
// ---------------------------------------------------------------------------
#if defined(_UNODE)

int
unode_init()
{
#if defined(_FAULT_INJECTION)
	os::printf("orphan FI test client loaded\n");
	return (0);
#else
	return (no_fault_injection());
#endif
}

#endif // _UNODE
