/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)testobj_impl.cc	1.8	08/05/20 SMI"

#include <orb/infrastructure/orb_conf.h>
#include <orbtest/reconf_fi/testobj_impl.h>

testobj_impl::testobj_impl()
{
}

testobj_impl::~testobj_impl()
{
	// Make sure _unreferenced() has been called.
	if (! is_unreferenced()) {
		os::printf("ERROR: testobj_impl: destructor called before "
			"_unreferenced()\n");
	}
}

void
#ifdef DEBUG
testobj_impl::_unreferenced(unref_t cookie)
#else
testobj_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	unref_called();
}

//
// Initialize implementation object with the given name.
//
bool
testobj_impl::init(char *strname)
{
	orphan_fi::testobj_ptr	tmpref;
	bool			retval;

	// Register with the name server.
	tmpref = get_objref();			// get temporary reference
	retval = register_obj(tmpref, strname);
	CORBA::release(tmpref);

	return (retval);
}

//
// IDL interfaces.
//

//
// For testing that this object impl is still alive.  It
// simply adds l1 and l2, and returns the total.
//
int32_t
testobj_impl::add(int32_t l1, int32_t l2, Environment &)
{
	return (l1 + l2);
}

//
// Returns the node ID where this object impl lives.
//
sol::nodeid_t
testobj_impl::get_nodeid(Environment &)
{
	return (orb_conf::node_number());
}

//
// For clients to tell that testing is done.
//
void
testobj_impl::done(Environment &)
{
	unregister_obj();
}
