/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _REPL_TEST_SWITCH_H
#define	_REPL_TEST_SWITCH_H

#pragma ident	"@(#)repl_test_switch.h	1.18	08/05/20 SMI"

#include <h/replica.h>

// This structure holds the information on the defined options
struct opt_t {
	replica::change_op	new_state;
	char			*prov_desc;
	char			*service_desc;
};

// This is the entry point to the work routine
extern int trigger_switchover(opt_t *p_opt);
extern int trigger_switchover(opt_t *p_opt, Environment &e);
extern int trigger_shutdown(char *service_desc);

// This is the routine that inspects the command line options
#ifndef _KERNEL
extern opt_t *get_switch_options(int argc, char **argv);
extern char *get_shutdown_options(int argc, char **argv);
#endif

#endif	/* _REPL_TEST_SWITCH_H */
