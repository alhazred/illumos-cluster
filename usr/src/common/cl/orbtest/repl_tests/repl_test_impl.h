/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _REPL_TEST_IMPL_H
#define	_REPL_TEST_IMPL_H

#pragma ident	"@(#)repl_test_impl.h	1.44	08/05/20 SMI"

#include <h/orbtest.h>
#include <repl/service/replica_tmpl.h>
#include <repl/service/transaction_state.h>

class test_server;

class trans_state_put: public transaction_state {
public:
	trans_state_put() : transaction_state(), data(0) { }
	~trans_state_put();

	void orphaned(Environment &);
	void committed();

	int32_t data;
};

/*CSTYLED*/
class test_server : public repl_server<orbtest::test_ckpt> {
public:
	/*CSTYLED*/
	test_server(const char * repl_desc) : repl_server<orbtest::test_ckpt>
	    ("ha test server", repl_desc) { }

	// Interface Operations
	void ckpt_test_mgr_obj(orbtest::test_mgr_ptr new_obj,
			Environment &);
	void ckpt_test_obj(orbtest::test_obj_ptr new_obj, Environment &);
	void ckpt_register(Environment &_environment);
	void ckpt_put(orbtest::test_obj_ptr put_obj, int32_t val,
			Environment &e);
	void become_secondary(Environment &e);
	void add_secondary(
		replica::checkpoint_ptr sec,
		const char *sec_name,
		Environment &e);
	void remove_secondary(const char *secondary_name,
		Environment &_environment);
	void freeze_primary_prepare(Environment &e);
	void freeze_primary(Environment &e);
	void unfreeze_primary(Environment &e);
	void become_primary(const replica::repl_name_seq &secondary_names,
		Environment &e);
	void become_spare(Environment &e);
	void shutdown(Environment &e);

	CORBA::Object_ptr get_root_obj(Environment &);

	static void initialize();

	static void switch_thread(void *);
	void startup(Environment &);

	// Checkpoint accessor function.
	orbtest::test_ckpt_ptr	get_checkpoint_orbtest();

private:
	orbtest::test_ckpt_ptr	_ckpt_proxy;
};

/*CSTYLED*/
class test_obj_repl_impl :
/*CSTYLED*/
    public mc_replica_of<orbtest::test_obj> {
public:
	test_obj_repl_impl(test_server *);
	test_obj_repl_impl(orbtest::test_obj_ptr);
	~test_obj_repl_impl();

	// IDL interfaces
	void	put(int32_t i, int16_t & repl_num, Environment &_environment);
	int32_t	get(Environment &_environment);

	void	_unreferenced(unref_t);

	// Utility function
	void update_val(int val) { lock(); value = val; unlock(); }

	// Checkpoint accessor function.
	orbtest::test_ckpt_ptr	get_checkpoint();

private:
	int	value;
	os::mutex_t lck;
	void	lock()		{ lck.lock(); }
	void	unlock()	{ lck.unlock(); }
};

/*CSTYLED*/
class test_mgr_repl_impl :
/*CSTYLED*/
    public mc_replica_of<orbtest::test_mgr> {
public:
	test_mgr_repl_impl(test_server *);
	test_mgr_repl_impl(test_server *, orbtest::test_mgr_ptr);
	~test_mgr_repl_impl();

	// IDL interfaces
	orbtest::test_obj_ptr get_test_obj(Environment &_environment);
	void register_client(Environment &_environment);

	void	_unreferenced(unref_t);

	void	lock()		{ lck.lock(); }
	void	unlock()	{ lck.unlock(); }

	// Checkpoint accessor function.
	orbtest::test_ckpt_ptr	get_checkpoint();

private:
	os::mutex_t lck;
	test_server *_serverp;
};

// The place where we store the global data for this test.
class test_data {
public:
	test_data();

	// Reset the data so that this module can be loaded again.
	// This is needed if we want to restart our service after
	// shutting it down on a node, for example.
	void reset();

	test_mgr_repl_impl *the_test_mgr_obj;
	orbtest::test_mgr_ptr test_mgr_ref;
	SList<test_obj_repl_impl> objlist;

	bool non_initialized;

#ifdef PANIC_GET_ROOT_OBJ
	bool first_time_get_root_obj;
#endif

};

#endif	/* _REPL_TEST_IMPL_H */
