/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)repl_test_switch_main.cc	1.19	08/05/20 SMI"

#include <unistd.h>
#include <stdlib.h>
#include <sys/os.h>
#include <orb/invo/common.h>
#include <orb/invo/corba.h>
#include <orb/infrastructure/orb.h>
#include <h/replica.h>

#include <orbtest/repl_tests/repl_test_switch.h>

int
main(int argc, char ** argv)
{
	if (ORB::initialize() != 0) {
		os::printf("failed to initialize orb\n");
		exit(1);
	}

	struct opt_t *p_opt;
	int retval;

	p_opt = get_switch_options(argc, argv);
	if (p_opt == NULL) {
		return (1);
	}

	retval = trigger_switchover(p_opt);
	delete [] p_opt->service_desc;
	delete [] p_opt->prov_desc;
	delete p_opt;

	return (retval);
}
