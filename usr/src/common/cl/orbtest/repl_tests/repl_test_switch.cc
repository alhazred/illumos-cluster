/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)repl_test_switch.cc	1.31	08/05/20 SMI"

#include <sys/os.h>
#include <h/naming.h>
#include <nslib/ns.h>
#include <h/orbtest.h>
#include <h/replica.h>


#include <orbtest/repl_tests/repl_test_switch.h>
#include <orbtest/repl_tests/repl_test_util.h>

static char prefix[] = "repl_test_switch";
static char name[] = "ha test server";


#ifndef	_KERNEL
#include <stdlib.h>
#include <stdio.h>
extern int switchover(int, char **);
#endif	// !_KERNEL


// Forward Definitions
bool random_secondary(
	replica::repl_prov_seq_var seq_ref,
	char *desc);
const char *change_op_to_name(replica::change_op op);
const char *prov_state_to_name(replica::repl_prov_state state);
void dump_provs(replica::repl_prov_seq_var seq_ref);
bool find_prov(
	replica::repl_prov_seq_var seq_ref,
	opt_t *p_opt,
	char *desc);
void usage(char *progname);


int
trigger_switchover(opt_t *p_opt)
{
	Environment	e;
	int		rslt;

	rslt = trigger_switchover(p_opt, e);
	if (e.exception()) {
		e.exception()->print_exception("trigger_switchover:");
	}

	return (rslt);
}

int
trigger_switchover(opt_t *p_opt, Environment &e)
{
	// os::printf("in trigger_switchover\n");
	char *service_desc;

	// os::printf("trigger_test:  before get_control\n");
	// Get control of a service
	if (p_opt->service_desc == NULL) {
		service_desc = "ha test server";
	} else {
		service_desc = p_opt->service_desc;
	}

	replica::service_admin_var sa
	    = get_service_admin_ref(prefix, service_desc, e);

	if (e.exception()) {
		e.exception()->print_exception
			("trigger test: get_service_admin_ref failed\n");
		return (1);
	}

	replica::repl_prov_seq_var	rp_list;

	// os::printf("trigger_test:  before get_repl_provs\n");
	sa->get_repl_provs(rp_list, e);
	if (e.exception()) {
		e.exception()->print_exception
			("trigger test: get_repl_provs failed\n");
		return (1);
	}

#ifndef _KERNEL
	os::printf("trigger_test:  got %d repl_provs from RM:  \n",
		rp_list->length());
#endif

	bool				found_prov = false;
	char prov_desc[10] = "";

	dump_provs(rp_list);
	if (p_opt->prov_desc == NULL) {
		found_prov = random_secondary(rp_list, prov_desc);
	} else {
		found_prov = find_prov(rp_list, p_opt, prov_desc);
	}

	if (!found_prov) {
		os::printf("trigger_test:  couldn't find repl_prov to "
		    "switch over to primary, exiting test\n");
		return (1);
	}

	os::printf("trigger_test: %s op for repl_prov `%s'\n",
		change_op_to_name(p_opt->new_state), prov_desc);

	bool ignore_change	= true;
	sa->change_repl_prov_status(prov_desc,
		p_opt->new_state,
		ignore_change,
		e);
	if (e.exception()) {
		e.exception()->print_exception(
		    "trigger test: change_repl_prov_status failed\n");
		return (1);
	}
	sa->get_repl_provs(rp_list, e);
	dump_provs(rp_list);

	return (0);
}


// random_secondary
// picks a random secondary from the list
bool
random_secondary(replica::repl_prov_seq_var seq_ref, char *desc)
{
	// Get the size of the the repl_prov list
	// And allocate an array of indices into the repl_prov list
	// so later we can randomly choose one of these
	uint32_t	sec_list_size = seq_ref->length();
	uint32_t	sec_index = 0;
	uint32_t	*sec_index_list = new uint32_t[sec_list_size];

	// Go through the list of repl_provs looking for secondaries
	for (uint32_t jindex = 0; jindex < seq_ref->length(); jindex++) {
		// Extract the state of the replica
		replica::repl_prov_state	state
			= seq_ref[jindex].curr_state;
		// Extract the description
		char *prov_desc = seq_ref[jindex].repl_prov_desc;

#ifndef _KERNEL
		os::printf("\t%s: , state `%d'\n", prov_desc, state);
#endif

		// Check the state of the replica provider
		if (state == replica::AS_SECONDARY) {
			// Add this index into the sec_index_list
			sec_index_list[sec_index] = jindex;
			sec_index++;
#ifndef _KERNEL
			os::printf("found secondary: %s\n", prov_desc);
#endif
		} // found a secondary
	} // for

	if (sec_index == 0) {
		delete [] sec_index_list;
		return (false);
	} else {
		// Figure out a random replica to switchover
		char *prov_desc = NULL;
		os::hrtime_t	time = os::gethrtime();
		int _random = (int)(time % sec_index);
		prov_desc = seq_ref[sec_index_list[_random]].repl_prov_desc;
		os::sprintf(desc, "%s", prov_desc);
	} // What is sec_index

	delete [] sec_index_list;
	return (true);
}

const char *
change_op_to_name(replica::change_op op)
{
	switch (op) {
		case replica::SC_SET_PRIMARY:
			return ("SC_SET_PRIMARY");
		case replica::SC_SET_SECONDARY:
			return ("SC_SET_SECONDARY");
		case replica::SC_SET_SPARE:
			return ("SC_SET_SPARE");
		case replica::SC_REMOVE_REPL_PROV:
			return ("SC_REMOVE_REPL_PROV");
	}
}

const char *
prov_state_to_name(replica::repl_prov_state state)
{
	switch (state) {
		case replica::AS_PRIMARY:
			return ("AS_PRIMARY");
		case replica::AS_SECONDARY:
			return ("AS_SECONDARY");
		case replica::AS_SPARE:
			return ("AS_SPARE");
		case replica::AS_DOWN:
			return ("AS_DOWN");
	}
}

// Dump the state of the providers.
void
#ifdef _KERNEL
dump_provs(replica::repl_prov_seq_var)
{
}
#else
dump_provs(replica::repl_prov_seq_var seq_ref)
{
	// Go through the list of repl_provs looking for the repl_prov
	for (uint32_t i = 0; i < seq_ref->length(); i++) {
		os::printf("\t%s: `%s'\n",
		    (char *)seq_ref[i].repl_prov_desc,
		    prov_state_to_name(seq_ref[i].curr_state));
	} // for
}
#endif

// Find a repl_prov
bool
find_prov(
	replica::repl_prov_seq_var seq_ref,
	opt_t *p_opt,
	char *desc)
{
	// Go through the list of repl_provs looking for the repl_prov
	for (uint32_t jindex = 0; jindex < seq_ref->length(); jindex++) {
		// Extract the state of the replica
		replica::repl_prov_state	state
			= seq_ref[jindex].curr_state;
		// Extract description
		char *prov_desc = seq_ref[jindex].repl_prov_desc;

		// Compare descriptions
		if (os::strcmp(prov_desc, p_opt->prov_desc) == 0) {
			// Found the prov we're looking for
			os::sprintf(desc, "%s", prov_desc);
			return (true);
		} // found the repl prov with desc
	} // for

	return (false);
}


#ifndef _KERNEL

// Entry points for UNODE

// These are done as a separate function due to the arguments
int
switchover(int argc, char **argv)
{
	int retval;
	struct opt_t *p_opt;

	p_opt = get_switch_options(argc, argv);
	if (p_opt == NULL)
		return (1);

	retval = trigger_switchover(p_opt);

	delete [] p_opt->prov_desc;
	delete p_opt;
	return (retval);
}

// Examine the command line
// In case of error NULL is returned
opt_t *
get_switch_options(int argc, char **argv)
{
	struct opt_t 	*p_opt;
	int		arg;

	// Allocate p_opt
	p_opt = new struct opt_t;

	// Set Default values
	p_opt->new_state =	replica::SC_SET_PRIMARY;
	p_opt->prov_desc =	NULL;
	p_opt->service_desc = 	NULL;

	// Examine the command line
	// Fix getopt so its re-entrant
	optind = 1;
	while ((arg = getopt(argc, argv, "?r:p:s:")) != EOF) {
		switch (arg) {
		case 'r' :
			// Service to change
			p_opt->service_desc = new char[os::strlen(optarg)+1];
			os::sprintf(p_opt->service_desc, "%s", optarg);
			break;
		case 'p' :
			// Description of prov
			p_opt->prov_desc = new char[os::strlen(optarg)+1];
			os::sprintf(p_opt->prov_desc, "%s", optarg);
			break;
		case 's' :
			// New state
			if (os::strcmp(optarg, "SC_SET_PRIMARY") == 0)
				p_opt->new_state =
					replica::SC_SET_PRIMARY;
			else if (os::strcmp(optarg, "SC_SET_SPARE") == 0)
				p_opt->new_state =
					replica::SC_SET_SPARE;
			else if (os::strcmp(optarg, "SC_SET_SECONDARY") == 0)
				p_opt->new_state =
					replica::SC_SET_SECONDARY;
			else if (os::strcmp(optarg, "SC_REMOVE_REPL_PROV") == 0)
				p_opt->new_state =
					replica::SC_REMOVE_REPL_PROV;
			else {
				os::printf("Invalid state change %s\n",
					optarg);
				delete(p_opt);
				usage(argv[0]);
				return (NULL);
			}
			break;
		default:
			usage(argv[0]);
			return (NULL);
		} // switch
	} // while (commands)

	return (p_opt);
}

// Usage printout
void
usage(char *progname)
{
	os::printf("\nUsage:\t%s -r <service name> [-p provider id]"
		" [-s new state]\n\n", progname);
	os::printf("Service name is the name of the HA service you'd like to "
		"switch\n");
	os::printf("Provider ID is the id of the replica\n");
	os::printf("New states can be:\n");
	os::printf("\tSC_SET_PRIMARY\n");
	os::printf("\tSC_SET_SECONDARY\n");
	os::printf("\tSC_SET_SPARE\n");
	os::printf("\tSC_REMOVE_REPL_PROV\n\n");
	os::printf("(*) If a service name (-r) is not specified, "
		"\"ha test server\" will be switched\n\n");
}

// These are done as a separate function due to the arguments
int
shutdown(int argc, char **argv)
{
	int retval;
	char	*service_desc = NULL;

	service_desc = get_shutdown_options(argc, argv);

	retval = trigger_shutdown(service_desc);

	delete [] service_desc;
	return (retval);
}

// Examine the command line
// In case of error NULL is returned
char *
get_shutdown_options(int argc, char **argv)
{
	char	*service_desc = NULL;
	int	arg;

	// Examine the command line
	// Fix getopt so its re-entrant
	optind = 1;
	while ((arg = getopt(argc, argv, "?s:")) != EOF) {
		switch (arg) {
		case 's' :
			// Service to change
			service_desc = new char[os::strlen(optarg)+1];
			os::strcpy(service_desc, optarg);
			break;
		default:
			usage(argv[0]);
			return (NULL);
		} // switch
	} // while (commands)

	return (service_desc);
}

int
trigger_shutdown(char *service_desc)
{
	// os::printf("in trigger_switchover\n");
	Environment e;

	// os::printf("trigger_test:  before get_control\n");
	// Get control of a service
	if (service_desc == NULL) {
		service_desc = "ha test server";
	}

	replica::service_admin_var sa
	    = get_service_admin_ref(prefix, service_desc, e);

	if (e.exception()) {
		e.exception()->print_exception
			("trigger test: get_service_admin_ref failed\n");
		return (1);
	}

	sa->shutdown_service(false, e);
	if (e.exception()) {
		e.exception()->print_exception
			("shutdown failed\n");
		return (1);
	}

	return (0);
}

#endif // !_KERNEL
