/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)repl_test_util.cc	1.9	08/05/20 SMI"

#include <sys/os.h>
#include <sys/rm_util.h>
#include <h/naming.h>
#include <h/replica.h>

replica::service_admin_ptr
get_service_admin_ref(const char *, const char *service_desc,
	CORBA::Environment &e)
{
	ASSERT(!e.exception());
	replica::rm_admin_var rm_ref = rm_util::get_rm();
	ASSERT(!CORBA::is_nil(rm_ref));

	return (rm_ref->get_control(service_desc, e));
}
