/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)repl_test_clnt.cc	1.40	08/05/20 SMI"

#include <sys/os.h>
#include <h/naming.h>
#include <nslib/ns.h>
#include <h/orbtest.h>
#include <sys/thread.h>

#if !defined(_UNODE) && !defined(_KERNEL)
#include <signal.h>
#include <stdlib.h>
#endif

#include <orbtest/repl_tests/repl_test_clnt.h>

#ifndef USE_NS
#include <orbtest/repl_tests/repl_test_util.h>
#endif

// Forward definition of handler
void EXIT_H(int);

// Initialize Global Variables
static int n_iters = 10;
static int n_threads = 2;

// Static global variable for this module that can be edited
// via the system(4) configuration file for verbosity
// control in kernel modules
static int quiet = 0;


// When to sleep - give up the cpu and print a message
#ifdef _KERNEL
static int break_iters = 200;
#else
static int break_iters = 2000;
#endif

os::mutex_t	lck;
os::condvar_t	done_cv;
bool		done = false;
int		active_threads = 0;	// Number of threads active

#ifdef USE_NS
static orbtest::test_mgr_ptr
get_test_mgr_ref_from_ns()
{
	CORBA::Environment	e;
	orbtest::test_mgr_ptr	test_mgr = nil;

	CORBA::Object_var obj = ns::wait_resolve("xyz", e);
	if (e.exception())
		e.exception()->print_exception("resolve");
	else {
		test_mgr = orbtest::test_mgr::_narrow(obj);
		if (CORBA::is_nil(test_mgr))
			os::printf("repl_test_client: narrow failed\n");
	}

	return (test_mgr);
}

#else
inline static void
handle_get_root_obj_exception(CORBA::Environment &e)
{
	if ((replica::uninitialized_service::_exnarrow(e.exception())) ||
	    (replica::failed_service::_exnarrow(e.exception())) ||
	    (CORBA::COMM_FAILURE::_exnarrow(e.exception()))) {
		e.exception()->print_exception("get_root_obj failure\n");
	} else {
		os::panic("replica client test got system exception "
		    "on get_root_obj\n");
	}
}

static orbtest::test_mgr_ptr
get_test_mgr_ref_from_rm()
{
	char prefix[]			= "repl_test_clnt";
	char service_desc[]		= "ha test server";
	orbtest::test_mgr_ptr test_mgr	= nil;

	CORBA::Environment		e;

	replica::service_admin_var sa
	    = get_service_admin_ref(prefix, service_desc, e);

	if (e.exception()) {
		e.exception()->print_exception(
		    "get_service_admin_ref failed\n");
	} else {

		//
		// new test for get_root_obj functionality
		//

		CORBA::Object_var obj = sa->get_root_obj(e);

		if (e.exception()) {
			handle_get_root_obj_exception(e);
		} else {
			// The general semantics for get_root_obj()
			// allow for nil returns without an exception.
			// But our test should return
			// an obj or an exception.
			//
			test_mgr = orbtest::test_mgr::_narrow(obj);
			if (CORBA::is_nil(test_mgr)) {
				os::printf("repl_test_client: narrow failed\n");
			}
		} // else got root obj
	} // else got sa

	return (test_mgr);
}
#endif /* USE_NS */

void
do_test(unsigned int thread_id)
{
	int	i;
	int	switches = 0;
	int16_t saved_repl_num = 0;
	int16_t repl_num = 0;

	orbtest::test_mgr_var test_mgr_ref
#ifdef USE_NS
	    = get_test_mgr_ref_from_ns();
#else
	    = get_test_mgr_ref_from_rm();
#endif /* USE_NS */

	if (CORBA::is_nil(test_mgr_ref)) {
		return;
	}

	Environment e;
	uint_t accum_iters = 0;
	while (!done) {
		orbtest::test_obj_var test_ref;
		test_ref = test_mgr_ref->get_test_obj(e);
		if (e.exception()) {
			e.exception()->print_exception(
			    "get_test_obj failed:");
			return;
		}
		for (i = 0; i != n_iters; i++) {
			test_ref->put(i, repl_num, e);
			if (e.exception()) {
				e.exception()->print_exception("put failed:");
				os::printf("\n%d invocations done\n", i);
				return;
			}

			if (repl_num != saved_repl_num) {
				if (saved_repl_num != 0) {
					switches++;
					if (!quiet) {
						os::printf("Changed server "
						    "replicas from: %d to: %d "
						    "(Thread: %d)\n",
						    saved_repl_num, repl_num,
						    thread_id);
					} // !quiet
				}
				saved_repl_num = repl_num;
			}

			if (done)
				break;
		}

		accum_iters += i;
		if (i != n_iters || accum_iters >= break_iters) {
			if (!quiet) {
				os::printf("%d invocations completed from "
				    "replica: %d (Thread %d)\n", accum_iters,
				    saved_repl_num, thread_id);
			}
			os::usecsleep(MICROSEC);
			accum_iters = 0;
		}
	}

	if (!quiet) {
		os::printf("%d server replica changes detected. (Thread: %d)\n",
		    switches, thread_id);
	}
}


// Thread entry point
static void *
test_thread(void *)
{
	lck.lock();
	active_threads++;
	lck.unlock();

#ifdef	_KERNEL
	do_test((unsigned int)os::threadid()->t_did);
#else
	do_test(os::threadid());
#endif

	lck.lock();
	active_threads--;
	if (active_threads == 0) {
		if (!quiet)
			os::printf("Signaling done_cv\n");
		done_cv.signal();
	}
	lck.unlock();

	return (NULL);
}


int
start_test_thread(int, char **)
{
	os::printf("+++ start_test_thread called \n");

#if !defined(_KERNEL) && !defined(_UNODE)
	if (signal(SIGINT, EXIT_H) == SIG_ERR) {
		os::panic("Could not setup signal catching...\n");
	}
#endif

	for (int i = 0; i < n_threads; i++) {
#ifdef _KERNEL
		if (thread_create(NULL, NULL, (void(*)(void))test_thread,
			NULL, 0, &p0, TS_RUN, 60) == NULL) {
			os::panic("thread_create failed\n");
		}
#else
		if (os::thread::create(NULL, (size_t)0, test_thread, NULL,
			THR_BOUND | THR_DETACHED, NULL)) {
			os::panic("thr_create failed\n");
		}
#endif
	}

	return (0);
}


// When we finish testing
// Call this function to make sure all the active threads are out of execution
int
finish(int, char **)
{
	done = 1;
	lck.lock();
	while (active_threads != 0) {
		done_cv.wait(&lck);
	}
	lck.unlock();

	return (0);
}

#if !defined(_UNODE) && !defined(_KERNEL)
static void *
finish(void *)
{
	finish(0, NULL);
	exit(0);

	return (0);
}

void
EXIT_H(int)
{
	if (os::thread::create(NULL, 0, finish, NULL,
		THR_BOUND | THR_DETACHED, NULL)) {
		os::panic("thr_create failed\n");
	}
}
#endif
