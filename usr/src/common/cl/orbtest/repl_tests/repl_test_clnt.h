/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _REPL_TEST_CLNT_H
#define	_REPL_TEST_CLNT_H

#pragma ident	"@(#)repl_test_clnt.h	1.12	08/05/20 SMI"

#include <sys/os.h>

#if defined(_KERNEL_ORB) && !defined(_KERNEL)
#define	_UNODE
#endif

extern	os::mutex_t	lck;		// Protect the condition variables
extern	os::condvar_t	done_cv;	// Signals the client is done
extern	bool		done;		// When we want to finish
extern	int		active_threads;	// Number of active threads

#endif	/* _REPL_TEST_CLNT_H */
