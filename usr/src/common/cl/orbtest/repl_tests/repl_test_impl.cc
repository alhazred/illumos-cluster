/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)repl_test_impl.cc	1.71	08/05/20 SMI"

#include <sys/os.h>
#include <sys/sunddi.h>
#include <h/naming.h>
#include <nslib/ns.h>
#include <h/replica.h>
#include <orbtest/repl_tests/repl_test_impl.h>

// The place we store global data.
test_data td;

// Global Module Variable that can be edited via the system(4)
// configuration file to turn off verbose output
int	quiet	= 0;

test_data::test_data() : the_test_mgr_obj(NULL), test_mgr_ref(nil),
    non_initialized(true)
{
#ifdef PANIC_GET_ROOT_OBJ
	first_time_get_root_obj = true;
#endif
}

void
test_data::reset()
{
	if (the_test_mgr_obj != NULL) {
		the_test_mgr_obj->lock();
		test_obj_repl_impl *obj = NULL;
		while ((obj = objlist.reapfirst()) != NULL) {
			delete obj;
		}
		ASSERT(objlist.empty());

		if (is_not_nil(test_mgr_ref)) {
			// Primary
			CORBA::release(test_mgr_ref);
			test_mgr_ref = nil;
		} else {
			// Secondary.
			delete the_test_mgr_obj;
		}
		the_test_mgr_obj = NULL;

		non_initialized = true;
	}
#ifdef PANIC_GET_ROOT_OBJ
	first_time_get_root_obj = true;
#endif
}

void
trans_state_put::orphaned(Environment &e)
{
	// os::printf("orphaned: %d\n", data);

	// Testing locks_recovered. It's all right not to make this call.
	locks_recovered();

	orbtest::test_ckpt_ptr sckpt = (orbtest::test_ckpt_ptr)
		get_checkpoint();
	sckpt->ckpt_put(NULL, data, e);
}

void
trans_state_put::committed()
{
	// os::printf("committed: %d\n", data);
}

trans_state_put::~trans_state_put()
{
	// os::printf("transaction state deleted: %d\n", data);
}

// IDL methods
void
test_server::become_secondary(Environment &)
{
	if (!quiet)
		os::printf("test_server::become_secondary\n");

	// We are changing from a primary to a secondary. Release the hold
	// on the root object.
	if (is_not_nil(td.test_mgr_ref)) {
		CORBA::release(td.test_mgr_ref);
		td.test_mgr_ref = nil;
	}

	// Release our reference to the multi_ckpt_handler.
	CORBA::release(_ckpt_proxy);
	_ckpt_proxy = nil;
}

void
test_server::add_secondary(replica::checkpoint_ptr sec, const char *sec_name,
    Environment &e)
{
	if (!quiet)
		os::printf("test_server::add_secondary: %s\n", sec_name);
	orbtest::test_ckpt_var ckpt = orbtest::test_ckpt::_narrow(sec);

	ASSERT(!CORBA::is_nil(ckpt));

	ASSERT(td.the_test_mgr_obj != NULL);
	orbtest::test_mgr_var ref = td.the_test_mgr_obj->get_objref();
	if (!quiet)
		os::printf("dumping state\n");
	ckpt->ckpt_test_mgr_obj(ref, e);
	if (e.exception())
		os::panic("exception in checkpoint");
	test_obj_repl_impl *obj;
	orbtest::test_obj_var test_ref;
	uint_t	i = 0;
	td.the_test_mgr_obj->lock();
	for (td.objlist.atfirst(); (obj = td.objlist.get_current()) != NULL;
	    td.objlist.advance(), i++) {
		test_ref = obj->get_objref();
		// XXX should checkpoint the value in the object
		ckpt->ckpt_test_obj(test_ref, e);
		if (e.exception())
			os::panic("exception in checkpoint");
	}
	td.the_test_mgr_obj->unlock();
	if (!quiet)
		os::printf("dumped %d objects\n", i);
}

void
test_server::remove_secondary(const char *sec_name, Environment &)
{
	if (!quiet)
		os::printf("test_server::remove_secondary: %s\n", sec_name);
}

void
test_server::become_primary(const replica::repl_name_seq &secondary_names,
    Environment &e)
{
	// First, initialize the checkpoint proxy.
	replica::checkpoint_var tmp_ckptp =
	    set_checkpoint(orbtest::test_ckpt::_get_type_info(0));
	_ckpt_proxy = orbtest::test_ckpt::_narrow(tmp_ckptp);

	ASSERT(!CORBA::is_nil(_ckpt_proxy));

	if (!quiet) {
		os::printf("test_server::become_primary. Secondaries are:\n");
		for (int i = 0; i < secondary_names.length(); i++) {
			os::printf("%s\n", (const char *)secondary_names[i]);
		}
	}
	if (td.non_initialized) {
		    td.non_initialized = false;
		    startup(e);
	} else {
		td.test_mgr_ref = td.the_test_mgr_obj->get_objref();
	}
}

void
test_server::become_spare(Environment &)
{
	if (!quiet)
		os::printf("test_server::become_spare\n");
	td.reset();
}

void
test_server::freeze_primary_prepare(Environment &)
{
}

void
test_server::freeze_primary(Environment &)
{
	if (!quiet)
		os::printf("test_server::freeze\n");
}

void
test_server::unfreeze_primary(Environment &)
{
	if (!quiet)
		os::printf("test_server::unfreeze\n");
}

// This is only called on primary.
void
test_server::shutdown(Environment &e)
{
	if (!quiet)
		os::printf("test_server::shutdown\n");
	if (td.objlist.empty()) {
		// ready to shutdown. Clean things up.
		td.reset();

		// Release our reference to the multi_ckpt_handler.
		CORBA::release(_ckpt_proxy);
		_ckpt_proxy = nil;
	} else {
		e.exception(new replica::service_busy);
	}
}

CORBA::Object_ptr
test_server::get_root_obj(Environment &)
{
	// The infrastructure guarantees that we're the primary if
	// this method is called on us.
	ASSERT(td.the_test_mgr_obj != nil);
	td.the_test_mgr_obj->lock();
	orbtest::test_mgr_ptr root_obj = td.the_test_mgr_obj->get_objref();
	td.the_test_mgr_obj->unlock();
	if (!quiet) {
		os::printf("test_server::get_root_obj returning "
		    "new reference `%x' to test_mgr\n", root_obj);
	}
	ASSERT(!CORBA::is_nil(root_obj));

#ifdef PANIC_GET_ROOT_OBJ
	if (td.first_time_get_root_obj) {
		os::panic("planned panic for first time get_root_obj()\n");
	}
#endif

	return (root_obj);
}

// Interface Operations
void
test_server::ckpt_test_mgr_obj(orbtest::test_mgr_ptr obj, Environment &)
{
	os::printf("ckpt_test_mgr_obj\n");
	// Create a shadow object for this object.
	td.the_test_mgr_obj = new test_mgr_repl_impl(this, obj);
	td.non_initialized = false;
#ifdef PANIC_GET_ROOT_OBJ
	td.first_time_get_root_obj = false;
#endif
}

void
test_server::ckpt_test_obj(orbtest::test_obj_ptr obj, Environment &)
{
	// os::printf("ckpt_test_obj\n");
	// Create a shadow object for this object.
	td.objlist.append(new test_obj_repl_impl(obj));
}

void
test_server::ckpt_register(Environment &)
{

}

void
test_server::ckpt_put(orbtest::test_obj_ptr o, int32_t l, Environment &e)
{
	secondary_ctx *ctxp = secondary_ctx::extract_from(e);
	trans_state_put *st = (trans_state_put *) ctxp->get_saved_state();

	if (!CORBA::is_nil(o)) {
		// os::printf("ckpt1: %d\n", l);
		// the first ckpt

		// ckpt from a retry
		if (st) {
			if (!quiet)
				os::printf("ckpt from a retry\n");
			return;
		}

		// A new transaction.
		// Update secondary copy
		test_obj_repl_impl *objp =
		    (test_obj_repl_impl *)o->_handler()->get_cookie();
		objp->update_val(l);

		// Create some state
		st = new trans_state_put();
		st->data = l;
		st->register_state(e);
		if (e.exception()) {
			os::printf("repl_test_impl: client died\n");
			e.clear();
		}
	} else {
		// os::printf("ckpt2: %d\n", l);
		// the second ckpt
		ASSERT(st);
		ASSERT(st->data == l);
	}
}

// Checkpoint accessor function.
orbtest::test_ckpt_ptr
test_server::get_checkpoint_orbtest()
{
	return (_ckpt_proxy);
}

test_obj_repl_impl::test_obj_repl_impl(test_server *serverp) :
/* CSTYLED */
    mc_replica_of<orbtest::test_obj>(serverp), value(0)
{

}

test_obj_repl_impl::test_obj_repl_impl(orbtest::test_obj_ptr obj) :
/* CSTYLED */
    mc_replica_of<orbtest::test_obj>(obj), value(0)
{

}

test_obj_repl_impl::~test_obj_repl_impl()
{
	// os::printf("test_obj_repl_impl deleted\n");
}

void
test_obj_repl_impl::put(int32_t l, int16_t & repl_num, Environment &e)
{
	// os::printf("put: %d\n", l);
	static int16_t node_num = 0;

	// If this is the first time put is called, figure out the
	// Node number we're on
	if (node_num == 0) {
		node_num = orb_conf::node_number();
	}

	repl_num = node_num;

	primary_ctx *ctxp = primary_ctx::extract_from(e);
	trans_state_put *st = (trans_state_put *) ctxp->get_saved_state();
	if (st) {
		if (!quiet)
			os::printf("found saved state\n");
		ASSERT(st->data == l);

		// don't send any more checkpoints once a transaction is
		// committed. Any checkpoints sent after commit may be
		// processed after the transaction_state is deleted.
		if (st->is_committed()) {
			return;
		}
		// Testing locks_recovered. It's all right not to make
		// this call.
		st->locks_recovered();
	}
	update_val(l);
	orbtest::test_ckpt_ptr sckpt = get_checkpoint();
	orbtest::test_obj_var objvar = get_objref();

	sckpt->ckpt_put(objvar, l, e);
	// os::usecsleep(MICROSEC);
	sckpt->ckpt_put(NULL, l, e);

	add_commit(e);
	// os::usecsleep(MICROSEC);
}

int32_t
test_obj_repl_impl::get(Environment &)
{
	return (0);
}

void
test_obj_repl_impl::_unreferenced(unref_t cookie)
{
	td.the_test_mgr_obj->lock();
	if (_last_unref(cookie)) {
		td.objlist.erase(this);
		delete this;
	}
	td.the_test_mgr_obj->unlock();
}

// Checkpoint accessor function.
orbtest::test_ckpt_ptr
test_obj_repl_impl::get_checkpoint()
{
	return ((test_server*)get_provider())->
	    get_checkpoint_orbtest();
}

test_mgr_repl_impl::test_mgr_repl_impl(test_server *serverp) :
/* CSTYLED */
    mc_replica_of<orbtest::test_mgr>(serverp),
    _serverp(serverp)
{

}

test_mgr_repl_impl::test_mgr_repl_impl(test_server *serverp,
    orbtest::test_mgr_ptr obj) :
/* CSTYLED */
    mc_replica_of<orbtest::test_mgr>(obj),
    _serverp(serverp)
{

}

test_mgr_repl_impl::~test_mgr_repl_impl()
{
	os::printf("mgr impl deleted\n");
}


// IDL interfaces
orbtest::test_obj_ptr
test_mgr_repl_impl::get_test_obj(Environment &e)
{
	test_obj_repl_impl *obj = new test_obj_repl_impl(_serverp);
	orbtest::test_obj_ptr ref = obj->get_objref();

	get_checkpoint()->ckpt_test_obj(ref, e);
	if (e.exception())
		os::panic("exception in checkpoint");
	lock();
	td.objlist.append(obj);
	unlock();

	return (ref);
}

void
test_mgr_repl_impl::register_client(Environment &)
{

}

void
test_mgr_repl_impl::_unreferenced(unref_t cookie)
{
	if (_last_unref(cookie))
		delete this;
}


void
test_server::startup(Environment &e)
{
	os::printf("repl_test_impl: starting the service!!!\n");
	td.the_test_mgr_obj = new test_mgr_repl_impl(this);
	td.test_mgr_ref = td.the_test_mgr_obj->get_objref();

	os::printf("checkpointing object\n");
	td.the_test_mgr_obj->get_checkpoint()->ckpt_test_mgr_obj(
	    td.test_mgr_ref, e);
	if (e.exception()) {
		os::panic("checkpoint exception");
	}
}

test_server *the_test_server = NULL;


void
test_server::initialize()
{
	Environment e;
	char desc[10];
	os::sprintf(desc, "%d", orb_conf::node_number());

	the_test_server = new test_server(desc);

	the_test_server->register_with_rm(1, true, e);
	if (e.exception()) {
		e.exception()->print_exception("failed to register:");
		e.clear();
	}

	// Fork off a test thread to trigger switchovers. This is done purely
	// as a test.

#ifdef	NOT_YET
#ifdef _KERNEL
	if (thread_create(NULL, NULL, (void(*)())test_server::switch_thread,
		NULL, 0, &p0, TS_RUN, 60) == NULL) {
		os::panic("thread_create failed\n");
	}
#else
	if (thr_create(NULL, (size_t)0, test_server::switch_thread, NULL,
	    THR_BOUND, NULL)) {
		os::panic("thr_create failed\n");
	}
#endif
#endif	// NOT_YET
}

void
test_server::switch_thread(void *)
{
	for (;;) {
		// sleep for a random time
#ifdef _KERNEL
		os::usecsleep(MICROSEC * (ddi_get_lbolt() % 32));
#else
		os::usecsleep(MICROSEC * 32);
#endif
		if (!quiet)
			os::printf("attempting to make this the primary\n");
		the_test_server->request_primary_mode();
	}
}

// Checkpoint accessor function.
orbtest::test_ckpt_ptr
test_mgr_repl_impl::get_checkpoint()
{
	return ((test_server*)get_provider())->
	    get_checkpoint_orbtest();
}
