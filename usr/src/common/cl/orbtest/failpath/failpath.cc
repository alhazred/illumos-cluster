/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)failpath.cc	1.11	08/05/20 SMI"

#include <sys/os.h>
#include <orb/invo/common.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/fault/fault_functions.h>


static void	usage(const char *progname);
static bool	str2num(const char *str, int32_t &result);
static bool	str2num(const char *str, uint32_t &result);


//
// Print usage information.
//
void
usage(const char *progname)
{
	os::printf(
"SYNOPSIS:\n"
"   %1$s [options...] <local_adapter_id> <remote_node_id>\n"
"\n"
"DESCRIPTION:\n"
"   Fail the path with the specified local adapter ID and remote node ID\n"
"\n"
"OPTIONS:\n"
"   -h|-?   Print this message.\n"
"   -f      Force the path to fail even if it's the only one to the\n"
"           specified node\n",
		progname);
}


#if defined(_KERNEL_ORB)
int
failpath(int argc, char *argv[])
#else
int
main(int argc, char *argv[])
#endif
{
#if defined(_FAULT_INJECTION)
	char				*progname;
	int				opt, err;
	FaultFunctions::failpath_t	spec;

	// Get the program's name.
	if (progname = strrchr(argv[0], '/')) {
		++progname;
	} else {
		progname = argv[0];
	}

	// Need to reset optind since in unode getopt() will use the same
	// variable across multiple invocations of this function (and possibly
	// among different shared libraries using getopt()).
	//
	// XXX This doesn't make getopt() MT-safe.
	optind = 1;

	// Parse options..
	while ((opt = getopt(argc, argv, "fh?")) != EOF) {
		switch (opt) {
		case 'f':
			// Force pathend to fail even it's the last one.
			spec.force = true;
			break;
		case 'h':
			usage(progname);
			return (0);
		case '?':
			if (optopt == opt) {
				// User really typed in -?.
				usage(progname);
				return (0);
			}
			return (1);		// error from getopt()
		}
	}

	if (argc - optind < 2) {
		os::printf("ERROR: Missing local adapter ID and/or "
			"remote node ID arguments\n");
		return (1);
	}

	// Get the local adapter ID argument
	if (! str2num(argv[optind++], spec.local_adapterid)) {
		return (1);
	}

	// Get the remote node ID argument
	if (! str2num(argv[optind++], spec.remote_nodeid)) {
		return (1);
	}

	// Verify remote node ID.
	if (spec.remote_nodeid == NODEID_UNKNOWN ||
	    spec.remote_nodeid > NODEID_MAX) {
		os::printf("ERROR: Invalid remote node ID: %d\n",
			spec.remote_nodeid);
		return (1);
	}

	if (spec.remote_nodeid == orb_conf::node_number()) {
		os::printf("ERROR: Node ID argument %d is the current node\n",
			spec.remote_nodeid);
		return (1);
	}

	if (! orb_conf::node_configured(spec.remote_nodeid)) {
		os::printf("ERROR: Remote node ID %d is not configured\n",
			spec.remote_nodeid);
		return (1);
	}

	// Fail the path.
	err = FaultFunctions::failpath(spec);
	switch (err) {
	case 0:
		break;					// success
	case EAGAIN:
		os::printf("ERROR: There is only one path to node ID %d.\n"
			"Use the -f option to force failure.\n",
			spec.remote_nodeid);
		return (1);
	case ECANCELED:
		os::printf("Path is already down\n");
		break;
	case ENOENT:
		os::printf("ERROR: Path doesn't exist\n");
		return (1);
	default:
		os::printf("ERROR: FaultFunctions::failpath: %s\n",
			strerror(err));
		return (1);
	}

	return (0);

#else // _FAULT_INJECTION

	argc, argv;				// shut compiler's warnings

	os::printf("Sorry, fault injection support is not compiled in\n");
	return (1);

#endif
}


//
// Translate a string into an int32_t.  The resulting number is returned in
// the parameter "result".
// Returns: true if successful, false otherwise.
//
bool
str2num(const char *str, int32_t &result)
{
	int32_t	tempres;
	char	*endp;

	errno = 0;
	tempres = (int32_t)strtol(str, &endp, 0);
	if (errno) {
		os::printf("ERROR: Can't convert '%s' to int32_t: %s\n",
			str, strerror(errno));
		return (false);
	}
	if (*endp != '\0') {
		os::printf("ERROR: Invalid int32_t string '%s'\n", str);
		return (false);
	}

	result = tempres;
	return (true);
}


//
// Translate a string into a uint32_t.  The resulting number is returned in
// the parameter "result".
// Returns: true if successful, false otherwise.
//
bool
str2num(const char *str, uint32_t &result)
{
	int32_t	tempres;
	char	*endp;

	// Convert to int32_t first so we can check for negative numbers.
	errno = 0;
	tempres = (int32_t)strtol(str, &endp, 0);
	if (errno) {
		os::printf("ERROR: Can't convert '%s' to uint32_t: %s\n",
			str, strerror(errno));
		return (false);
	}
	if (*endp != '\0') {
		os::printf("ERROR: Invalid uint32_t string '%s'\n", str);
		return (false);
	}
	if (tempres < 0) {
		os::printf("ERROR: uint32_t string '%s' must be >= 0\n", str);
		return (false);
	}

	result = (uint32_t)tempres;
	return (true);
}


#if defined(_KERNEL_ORB)
int
unode_init()
{
	os::printf("failpath module loaded\n");
	return (0);
}
#endif
