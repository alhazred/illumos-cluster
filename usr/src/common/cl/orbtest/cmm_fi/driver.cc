/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)driver.cc	1.19	09/03/06 SMI"

#if defined(_KERNEL)
#include <sys/modctl.h>
#endif

#include <orbtest/fi_support/fi_driver.h>

#include <orbtest/cmm_fi/faults/entry_reconfigure.h>
#include <orbtest/cmm_fi/faults/entry_quorum.h>

// List of test entry functions.
fi_entry_t fi_entry[] = {
	ENTRY(reconfigure_1_a),
	ENTRY(reconfigure_1_b),
	ENTRY(reconfigure_1_c),
	ENTRY(reconfigure_1_j),
	ENTRY(reconfigure_1_k),
	ENTRY(reconfigure_1_r),
	ENTRY(reconfigure_1_y),
	ENTRY(reconfigure_2_r),
	ENTRY(reconfigure_pre_sync_with_others),
	ENTRY(reconfigure_after_sync_with_others),
	ENTRY(reconfigure_pre_callbacks),
	ENTRY(reconfigure_after_callbacks),
	ENTRY(reconfigure_hang_pre_callbacks),
	ENTRY(quorum_1_a),
	ENTRY(quorum_1_b),
	ENTRY(quorum_1_c),
	ENTRY(quorum_1_d),
	ENTRY(quorum_1_e),
	ENTRY(quorum_1_f),
	ENTRY(quorum_1_g),
	ENTRY(quorum_1_h),
	ENTRY(quorum_1_i),
	ENTRY(quorum_1_j),
	ENTRY(quorum_2_a),
	ENTRY(quorum_2_b),
	ENTRY(quorum_2_c),
	ENTRY(quorum_2_d),
	ENTRY(quorum_2_e),
	ENTRY(quorum_2_f),
	ENTRY(quorum_2_g),
	ENTRY(quorum_2_h),
	ENTRY(quorum_3_a),
	ENTRY(quorum_3_b),
	ENTRY(quorum_3_c),
	ENTRY(quorum_3_d),
	ENTRY(quorum_3_e),
	ENTRY(quorum_3_f),
	ENTRY(quorum_3_g),
	ENTRY(quorum_3_h),
	ENTRY(quorum_4_a),
	ENTRY(quorum_4_b),
	ENTRY(quorum_4_c),
	ENTRY(quorum_4_d),
	ENTRY(quorum_4_e),
	ENTRY(quorum_4_f),
	ENTRY(quorum_4_g),
	ENTRY(quorum_4_h),
	ENTRY(quorum_4_i),
	ENTRY(quorum_4_j),
	ENTRY(quorum_4_k),
	ENTRY(quorum_4_l),
	ENTRY(quorum_4_m),
	ENTRY(node_depart_hearsay),
	ENTRY(node_join_hearsay),
	ENTRY(NULL)
};


#if ! defined(_KERNEL)
int
main(int argc, char **argv)
{
	int	rslt;

	// Initialize orb.
	if (ORB::initialize() != 0) {
		os::printf("Failed to initialize orb\n");
		return (1);
	}

	rslt = do_fi_test(argc, argv, fi_entry);
	return (rslt);
}

#else // ! _KERNEL

extern "C" {
	void _cplpl_init(void);
	void _cplpl_fini(void);
}

char _depends_on[] = "misc/cl_runtime misc/cl_load misc/cl_orb "
	"misc/cl_haci misc/cl_quorum misc/cl_comm misc/fi_driver";

struct modlmisc modlmisc = {
	&mod_miscops, "cmm fi test driver"
};

struct modlinkage modlinkage = {
#ifdef _LP64
	MODREV_1, { (void *) &modlmisc, NULL, NULL, NULL, NULL, NULL, NULL }
#else
	MODREV_1, { (void *) &modlmisc, NULL, NULL, NULL }
#endif
};

int
_init(void)
{
	int		error, catcherror;
	struct modctl	*mp;
	int		argc, i;
	char		**argv;

	error = mod_install(&modlinkage);

	if (error) {
		return (error);
	}
	_cplpl_init();				// C++ initialization

	// Get module name so we know what test case to call
	if ((mp = mod_getctl(&modlinkage)) == NULL) {
		os::printf("Can't find modctl\n");
		_cplpl_fini();
		catcherror = mod_remove(&modlinkage);
		return (EIO);
	}

	// Parse module name to get arguments
	mod_name_to_args(mp->mod_modname, argc, argv);

	// Do the test.
	error = do_fi_test(argc, argv, fi_entry);
	os::printf("    Result From Module: %d\n", error);

	// Clean up argument vectors allocated by mod_name_to_args().
	for (i = 0; i < argc; ++i) {
		delete[] argv[i];
	}
	delete[] argv;

	_cplpl_fini();
	catcherror = mod_remove(&modlinkage);

	if (catcherror)
		return (catcherror);
	else
		return (error);
}

int
_fini(void)
{
	return (0);
}

int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}

#endif // ! _KERNEL
