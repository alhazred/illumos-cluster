/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)entry_common.cc	1.20	08/05/20 SMI"

#include <orbtest/cmm_fi/faults/entry_common.h>

#include <sys/os.h>

//
// nodeids array for use by non_root_node fn.
// This data is here as part of the root-node removal
// process.
//
#if defined(_FAULT_INJECTION)

int num_of_nodeids = 0;
nodeid_t nodeids[NODEID_MAX];

#endif

//
// Fn. to store nodeid s passed by a test entry point
//
#if defined(_FAULT_INJECTION)

int
store_nodeids(int arglen, char **argv)
{
	num_of_nodeids = os::atoi(argv[arglen-1]);

	int index = arglen - 1 - num_of_nodeids;
	char **argp = argv + index;

	for (int i = 0; i < num_of_nodeids; i++)
		nodeids[i] = (nodeid_t)os::atoi(argp[i]);

	return (0);
}

#endif

//
// If fault injection is not turned on, print an error message and return 0.
//
#if !defined(_FAULT_INJECTION)
int
no_fault_injection()
{
	os::printf("+++ FAIL: Fault Injection Compilation Flags were not"
		"set!\n");
	return (0);
}
#endif

//
// New fns to find the nth non-root-node etc.
//

//
// non_root_node(n) really returns NODE_n+1_ID, and get_root_nodeid
// will return NODE_1_ID. NOTE that NODE_1 is what the FI driver assigns
// to the root node, and NODE_2..n to the other nodes.
//


#if defined(_FAULT_INJECTION) && defined(_KERNEL_ORB)

nodeid_t
non_root_node(int n)
{
	ASSERT(n < num_of_nodeids && n > 0);

	return (nodeids[n]);

#if 0
	// XXX This is the old version of non_root_node().  Kept here for
	// legacy's sake, but this whole function can be removed once the
	// concept of root node is gone.


	nodeid_t	root_nodeid;
	nodeid_t	highest_nodeid;
	nodeid_t	counter		= 0;

	root_nodeid = orb_conf::get_root_nodeid();

	// I'm going to do this really lame..
	// because there is no direct interface for what i want to do
	// I want to examine a list of all the nodeid's in the cluster, that
	// is not directly available without some major hoopla..
	// so instead i will create my own list of live nodes..  should be good
	// enough

	// Get the highest nodeid
	highest_nodeid = NODEID_MAX;

	while (n > 0) {
		// Increase counter
		counter++;

		// Have we looked too far?
		if (counter > highest_nodeid)
			return (0);

		// Ignore the root node
		if (counter == root_nodeid)
			continue;

		// Check if its a configured member of the cluster
		if (orb_conf::node_configured(counter)) {
			n--;
		}
	}

	return (counter);

#endif

}

#endif

#if defined(_FAULT_INJECTION) && defined(_KERNEL_ORB)

nodeid_t
get_root_nodeid()
{
	return (nodeids[0]);
}

#endif
