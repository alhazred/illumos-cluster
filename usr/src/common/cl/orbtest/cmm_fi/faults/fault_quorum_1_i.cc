/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fault_quorum_1_i.cc	1.9	08/09/03 SMI"

//
// This file contains the on and off functions for the fault points for a
// specific fault scenario.
//
// Refer to the README.faults file for a complete description of all
// scenarios
//

#include	<sys/os.h>
#include	<orb/fault/fault_injection.h>

extern int num_of_nodeids;
extern nodeid_t nodeids[];

int
fault_quorum_1_i_on(void *argp, int arglen)
{
	// Argument should be a FaultFunctions::mc_sema_arg_t structure
	ASSERT(argp != NULL);
	ASSERT(arglen == sizeof (np_list_t));

	np_list_t	*np_list = (np_list_t *)argp;
	// char		*path;
	nodeid_t	node;
	// int		len;
	int 		counter;

	// Add an invo trigger to make the client node wait for a reconfigure
	// to complete (by waiting on a semaphore

	FaultFunctions::invo_trigger_add(FaultFunctions::SCQ_WAIT,
	    FAULTNUM_CMM_CONTROL_RECONFIGURE,
	    NULL, 0);


	// Add a node trigger that will signal when the reconfig
	// is complete

	FaultFunctions::node_trigger_add(FaultFunctions::SIGNAL,
	    FAULTNUM_CMM_AUTOMATON_END_STATE,
	    NULL, 0, TRIGGER_THIS_NODE);

	os::warning("Armed fault: "
	    "FAULTNUM_CMM_CONTROL_RECONFIGURE");
	os::warning("Armed fault: "
	    "FAULTNUM_CMM_AUTOMATON_END_STATE");


	// Get the client node id and quorum path

	nodeid_t	this_node = orb_conf::node_number();
	char		*qd_path = get_qd_path(1, this_node);
	ASSERT(qd_path != NULL);

	// Set fault point so the client node cant talk to the QD
	NodeTriggers::add(FAULTNUM_CMM_QUORUM_DEVICE_FAIL,
		qd_path, (uint32_t)os::strlen(qd_path) + 1, this_node);
	os::warning("Armed fault: "
		"FAULTNUM_CMM_QUORUM_DEVICE_FAIL on node: %d", this_node);


	// Add trigger to stop the node from dumping core in case it aborts
	FaultFunctions::wait_for_arg_t  wait_args;
	wait_args.op = FaultFunctions::WAIT_FOR_DEAD;
	wait_args.nodeid = 0;

	// Add trigger to stop the node from dumping core in case of aborting
	// on all other nodes except the node under test
	for (counter = 0; counter < num_of_nodeids; counter++) {
	    if (nodeids[counter] != np_list->node_list[0]) {
		FaultFunctions::node_trigger_add(FaultFunctions::REBOOT,
		    FAULTNUM_CMM_STOP_ABORT, &wait_args,
		    (uint32_t)sizeof (wait_args),
		    nodeids[counter]);
		os::warning("Armed fault: "
		    "FAULTNUM_CMM_STOP_ABORT on node: %d", nodeids[counter]);
	    }
	}


	FaultFunctions::wait_for_arg_t  wait_args2;
	wait_args2.op = FaultFunctions::WAIT_FOR_REJOIN;

	// Kill a designated node
	for (counter = 0; counter < np_list->listlen; counter++) {
		node = np_list->node_list[counter];
		wait_args2.nodeid = node;
		os::printf("    The node being killed is: %d\n", node);
		FaultFunctions::reboot(0, &wait_args2,
		    (uint32_t)sizeof (wait_args2));
	}

	// I sleep here to give enough time to let the node die.  If the sleep
	// does not happen then the kernel module exits too quickly and thinks
	// that the node did not die

	os::usecsleep(15 * 1000000);

	return (0);
}

int
fault_quorum_1_i_off(void *argp, int arglen)
{
	// Argument should be a FaultFunctions::mc_sema_arg_t structure
	ASSERT(argp != NULL);
	ASSERT(arglen == sizeof (np_list_t));

	np_list_t	*np_list = (np_list_t *)argp;
	nodeid_t	node;
	int 		counter;

	// Clear the invo triggers

	InvoTriggers::clear(FAULTNUM_CMM_CONTROL_RECONFIGURE);
	NodeTriggers::clear(FAULTNUM_CMM_AUTOMATON_END_STATE);


	// Clear the fault
	// (For each node in the list)
	for (counter = 0; counter < np_list->listlen; counter++) {
		node = np_list->node_list[counter];
		NodeTriggers::clear(FAULTNUM_CMM_QUORUM_DEVICE_FAIL, node);
		NodeTriggers::clear(FAULTNUM_CMM_STOP_ABORT, node);
	}

	for (counter = 0; counter < num_of_nodeids; counter++) {
	    if (nodeids[counter] != np_list->node_list[0]) {
		NodeTriggers::clear(FAULTNUM_CMM_STOP_ABORT, nodeids[counter]);
	    }
	}

	return (0);
}
