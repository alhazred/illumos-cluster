/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FAULT_NODE_DEPART_HEARSAY_H
#define	_FAULT_NODE_DEPART_HEARSAY_H

#pragma ident	"@(#)fault_node_depart_hearsay.h	1.1	09/03/06 SMI"

//
// This file contains the on and off functions for the fault points for a
// specifc fault scenario.
//
// Refer to the README.faults file for a comeplete description of all
// scenarios
//

int fault_node_depart_hearsay_on(void *, int);
int fault_node_depart_hearsay_off(void *, int);

#endif	/* _FAULT_NODE_DEPART_HEARSAY_H */
