/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fault_quorum_1_a.cc	1.9	08/09/03 SMI"

//
// This file contains the on and off functions for the fault points for a
// specific fault scenario.
//
// Refer to the README.faults file for a complete description of all
// scenarios
//

#include	<sys/os.h>
#include	<orbtest/cmm_fi/faults/entry_quorum_aux.h>
#include	<orb/fault/fault_injection.h>

int
fault_quorum_1_a_on(void *argp, int arglen)
{
	// Argument should be a FaultFunctions::mc_sema_arg_t structure
	ASSERT(argp == NULL);
	ASSERT(arglen == 0);

	// Add an invo trigger to make the client node wait for a reconfigure
	// to complete (by waiting on a semaphore
	FaultFunctions::invo_trigger_add(FaultFunctions::SCQ_WAIT,
	    FAULTNUM_CMM_CONTROL_RECONFIGURE,
	    NULL, 0);

	// Add a node trigger that will signal when the reconfig
	// is complete
	FaultFunctions::node_trigger_add(FaultFunctions::SIGNAL,
	    FAULTNUM_CMM_AUTOMATON_END_STATE,
	    NULL, 0, TRIGGER_THIS_NODE);

	os::warning("Armed fault: "
	    "FAULTNUM_CMM_CONTROL_RECONFIGURE");

	os::warning("Armed fault: "
	    "FAULTNUM_CMM_AUTOMATON_END_STATE");

	return (0);
}

int
fault_quorum_1_a_off(void *, int)
{
	// Clear the invo triggers
	InvoTriggers::clear(FAULTNUM_CMM_CONTROL_RECONFIGURE);
	NodeTriggers::clear(FAULTNUM_CMM_AUTOMATON_END_STATE);

	return (0);
}
