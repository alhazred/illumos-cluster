/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)entry_quorum.cc	1.23	08/05/20 SMI"

#include	<sys/os.h>
#include	<sys/clconf.h>
#include	<orb/invo/common.h>
#include	<orb/infrastructure/orb_conf.h>
#include	<orbtest/cmm_fi/cmm_fi_clnt.h>
#include	<orb/fault/fault_injection.h>
#include	<orbtest/cmm_fi/faults/entry_common.h>

#include	<orbtest/cmm_fi/faults/entry_quorum_aux.h>

#include	<orbtest/cmm_fi/faults/fault_quorum_1_a.h>
#include	<orbtest/cmm_fi/faults/fault_quorum_1_b.h>
#include	<orbtest/cmm_fi/faults/fault_quorum_1_e.h>
#include	<orbtest/cmm_fi/faults/fault_quorum_1_i.h>

#include	<sys/quorum_int.h>

//
// CMM Quorum 1-A Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_1_a(int arglen, char *argv[])
#else
int
quorum_1_a(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 1 Fault A\n");

#ifdef _FAULT_INJECTION
	Environment	e;
	int		rslt;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	rslt = cmm_reconfigure(
		fault_quorum_1_a_on,
		fault_quorum_1_a_off,
		NULL,
		0,
		e);
	if (e.exception()) {
		e.exception()->print_exception("quorum_1_a");
		return (1);
	}
	if (rslt)
		return (rslt);

	// Verify that keys for both nodes exist in QD
	nodeid_list_t	node_list;
	node_list.listlen = 2;
	node_list.list = new nodeid_t[2];
	node_list.list[0] = orb_conf::get_root_nodeid();
	node_list.list[1] = non_root_node(1);

	rslt = verify_keys(1, &node_list);

	delete []node_list.list;

	return (rslt);
#else
	return (no_fault_injection());
#endif
}

//
// CMM Quorum 1-B Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_1_b(int arglen, char *argv[])
#else
int
quorum_1_b(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 1 Fault B\n");

#ifdef _FAULT_INJECTION
	Environment	e;
	int		rslt;
	np_list_t	np_list;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	// Get the 1st non-root node
	np_list.listlen = 1;
	np_list.node_list = new nodeid_t[1];
	np_list.path_list = new char *[1];
	np_list.node_list[0] = non_root_node(1);
	np_list.path_list[0] = get_qd_path(1, np_list.node_list[0]);

	os::printf("    First non-root node: %d\n", np_list.node_list[0]);

	rslt = cmm_reconfigure(
		fault_quorum_1_b_on,
		fault_quorum_1_b_off,
		&np_list,
		(int)sizeof (np_list),
		e);
	if (e.exception()) {
		e.exception()->print_exception("quorum_1_b");
		return (1);
	}
	if (rslt)
		return (rslt);

	// Verify that keys for both nodes exist in QD
	nodeid_list_t	node_list;
	node_list.listlen = 2;
	node_list.list = new nodeid_t[2];
	node_list.list[0] = orb_conf::get_root_nodeid();
	node_list.list[1] = non_root_node(1);

	rslt = verify_keys(1, &node_list);

	delete []node_list.list;
	delete []np_list.node_list;
	delete []np_list.path_list;

	return (rslt);
#else
	return (no_fault_injection());
#endif
}

//
// CMM Quorum 1-C Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_1_c(int arglen, char *argv[])
#else
int
quorum_1_c(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 1 Fault C\n");

#ifdef _FAULT_INJECTION
	// Reuse the fault point from quorum_1_b but this time set the nodeid
	// to the root node.  (It wont be able to comunicate with the QD)
	Environment	e;
	int		rslt;
	np_list_t	np_list;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	// Get the root node
	np_list.listlen = 1;
	np_list.node_list = new nodeid_t[1];
	np_list.path_list = new char *[1];
	np_list.node_list[0] = orb_conf::get_root_nodeid();
	np_list.path_list[0] = get_qd_path(1, np_list.node_list[0]);

	os::printf("    Root Nodeid: %d\n", np_list.node_list[0]);

	rslt = cmm_reconfigure(
		fault_quorum_1_b_on,
		fault_quorum_1_b_off,
		&np_list,
		(int)sizeof (np_list),
		e);
	if (e.exception()) {
		e.exception()->print_exception("quorum_1_c");
		return (1);
	}
	if (rslt)
		return (rslt);

	// Verify that keys for both nodes exist in QD
	nodeid_list_t	node_list;
	node_list.listlen = 2;
	node_list.list = new nodeid_t[2];
	node_list.list[0] = orb_conf::get_root_nodeid();
	node_list.list[1] = non_root_node(1);

	rslt = verify_keys(1, &node_list);

	delete []node_list.list;
	delete []np_list.node_list;
	delete []np_list.path_list;

	return (rslt);
#else
	return (no_fault_injection());
#endif
}

//
// CMM Quorum 1-D Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_1_d(int arglen, char *argv[])
#else
int
quorum_1_d(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 1 Fault D\n");

#ifdef _FAULT_INJECTION
	// Reuse the fault point from quorum_1_b but this time set the
	// node to ALL NODES, therefore the fualt point trigger will severe
	// the communications to the QD from all nodes.

	Environment	e;
	int		rslt;
	np_list_t	np_list;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	// Set PATH to NULL, and the nodeid to ALL NODES
	np_list.listlen = 1;
	np_list.node_list = new nodeid_t[1];
	np_list.path_list = new char *[1];
	np_list.node_list[0] = TRIGGER_ALL_NODES;
	np_list.path_list[0] = NULL;

	rslt = cmm_reconfigure(
		fault_quorum_1_b_on,
		fault_quorum_1_b_off,
		&np_list,
		(int)sizeof (np_list),
		e);
	if (e.exception()) {
		e.exception()->print_exception("quorum_1_d");
		return (1);
	}

	// Since we faulted the QD, no need to check keys (QD does not exist
	// anymore)
	delete []np_list.node_list;
	delete []np_list.path_list;

	return (rslt);
#else
	return (no_fault_injection());
#endif
}

//
// CMM Quorum 1-E Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_1_e(int arglen, char *argv[])
#else
int
quorum_1_e(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 1 Fault E\n");

#ifdef _FAULT_INJECTION
	Environment	e;
	int		rslt;
	np_list_t	np_list;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	// Get the 1st non-root node
	np_list.listlen = 1;
	np_list.node_list = new nodeid_t[1];
	np_list.path_list = new char *[1];
	np_list.node_list[0] = non_root_node(1);
	np_list.path_list[0] = get_qd_path(1, np_list.node_list[0]);

	os::printf("    First non-root node: %d\n", np_list.node_list[0]);

	rslt = cmm_reconfigure(
		fault_quorum_1_e_on,
		fault_quorum_1_e_off,
		&np_list,
		(int)sizeof (np_list),
		e);
	if (e.exception()) {
		e.exception()->print_exception("quorum_1_e");
		return (1);
	}

	os::usecsleep((os::usec_t)15 * 1000000);

	if (rslt)
		return (rslt);

	// Verify that keys for this node exist in QD
	nodeid_list_t	node_list;
	node_list.listlen = 1;
	node_list.list = new nodeid_t[1];
	node_list.list[0] = orb_conf::get_root_nodeid();

	rslt = verify_keys(1, &node_list);

	delete []node_list.list;
	delete []np_list.node_list;
	delete []np_list.path_list;

	return (rslt);
#else
	return (no_fault_injection());
#endif
}

//
// CMM Quorum 1-F Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_1_f(int arglen, char *argv[])
#else
int
quorum_1_f(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 1 Fault F\n");

#ifdef _FAULT_INJECTION
	// Reuse the fault point from quorum_1_f but this time set the
	// node to slow down to the root node.
	Environment	e;
	int		rslt;
	np_list_t	np_list;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	// Get the root node
	np_list.listlen = 1;
	np_list.node_list = new nodeid_t[1];
	np_list.path_list = new char *[1];
	np_list.node_list[0] = orb_conf::get_root_nodeid();
	np_list.path_list[0] = get_qd_path(1, np_list.node_list[0]);

	os::printf("    Root node: %d\n", np_list.node_list[0]);

	rslt = cmm_reconfigure(
		fault_quorum_1_e_on,
		fault_quorum_1_e_off,
		&np_list,
		(int)sizeof (np_list),
		e);
	if (e.exception()) {
		e.exception()->print_exception("quorum_1_e");
		return (1);
	}

	os::usecsleep((os::usec_t)15 * 1000000);

	if (rslt)
		return (rslt);

	// Verify that keys for this node exist in QD
	nodeid_list_t	node_list;
	node_list.listlen = 1;
	node_list.list = new nodeid_t[1];
	node_list.list[0] = non_root_node(1);

	rslt = verify_keys(1, &node_list);

	delete []node_list.list;
	delete []np_list.node_list;
	delete []np_list.path_list;

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// CMM Quorum 1-G Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_1_g(int arglen, char *argv[])
#else
int
quorum_1_g(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 1 Fault G\n");

#ifdef _FAULT_INJECTION
	Environment			e;
	int				rslt;
	FaultFunctions::wait_for_arg_t	wait_arg;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	wait_arg.op = FaultFunctions::WAIT_FOR_DEAD;
	wait_arg.nodeid = non_root_node(1);

	// Kill the first non-root node
	os::printf("    First non-root node is: %d\n", wait_arg.nodeid);
	FaultFunctions::reboot(0, &wait_arg, (uint32_t)sizeof (wait_arg));

	// Verify that only the keys for this node are left over
	nodeid_list_t	node_list;
	node_list.listlen = 1;
	node_list.list = new nodeid_t[1];
	node_list.list[0] = orb_conf::get_root_nodeid();

	rslt = verify_keys(1, &node_list);

	delete []node_list.list;

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// CMM Quorum 1-H Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_1_h(int arglen, char *argv[])
#else
int
quorum_1_h(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 1 Fault H\n");

#ifdef _FAULT_INJECTION
	Environment			e;
	int				rslt;
	FaultFunctions::wait_for_arg_t	wait_arg;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	wait_arg.op = FaultFunctions::WAIT_FOR_DEAD;
	wait_arg.nodeid = orb_conf::get_root_nodeid();

	// Kill the root node
	os::printf("    Root node is: %d\n", wait_arg.nodeid);
	FaultFunctions::reboot(0, &wait_arg, (uint32_t)sizeof (wait_arg));

	// Verify that only the keys for this node are left over
	nodeid_list_t	node_list;
	node_list.listlen = 1;
	node_list.list = new nodeid_t[1];
	node_list.list[0] = non_root_node(1);

	rslt = verify_keys(1, &node_list);

	delete []node_list.list;

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// CMM Quorum 1-I Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_1_i(int arglen, char **argv)
#else
int
quorum_1_i(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 1 Fault I\n");

#ifdef _FAULT_INJECTION
	Environment	e;
	int		rslt;
	np_list_t	np_list;

	if (store_nodeids(arglen, argv))
	    return (1);

	(void) wait_for_keys();

	// Get the 1st non-root node
	np_list.listlen = 1;
	np_list.node_list = new nodeid_t[1];
	np_list.path_list = new char *[1];
	np_list.node_list[0] = non_root_node(1);
	np_list.path_list[0] = get_qd_path(1, np_list.node_list[0]);

	os::printf("    First non-root node: %d\n", np_list.node_list[0]);

	rslt = cmm_reconfigure(
		fault_quorum_1_i_on,
		fault_quorum_1_i_off,
		&np_list,
		(int)sizeof (np_list),
		e);
	if (e.exception()) {
		e.exception()->print_exception("quorum_1_i");
		return (1);
	}
	if (rslt)
		return (rslt);

	// Verify that keys for this node exist in QD
/*
	nodeid_list_t   node_list;
	node_list.listlen = 1;
	node_list.list = new nodeid_t[1];
	// node_list.list[0] = orb_conf::get_root_nodeid();
	node_list.list[0] = non_root_node(1);

	rslt = verify_keys(1, &node_list);

	delete []node_list.list;
*/
	delete []np_list.node_list;
	delete []np_list.path_list;

	// return (rslt);
	return (0);
#else
	return (no_fault_injection());
#endif
}


//
// CMM Quorum 1-J Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_1_j(int arglen, char *argv[])
#else
int
quorum_1_j(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 1 Fault J\n");

#ifdef _FAULT_INJECTION

	// Reuse the fault point from quorum_1_i but this time set the nodeid
	// to the root node.  (It wont be able to comunicate with the QD)

	Environment	e;
	int		rslt;
	np_list_t	np_list;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	// Get the root node
	np_list.listlen = 1;
	np_list.node_list = new nodeid_t[1];
	np_list.path_list = new char *[1];
	np_list.node_list[0] = orb_conf::get_root_nodeid();
	np_list.path_list[0] = get_qd_path(1, np_list.node_list[0]);

	os::printf("    Root Nodeid: %d\n", np_list.node_list[0]);

	rslt = cmm_reconfigure(
		fault_quorum_1_i_on,
		fault_quorum_1_i_off,
		&np_list,
		(int)sizeof (np_list),
		e);

	if (e.exception()) {
		e.exception()->print_exception("quorum_1_j");
		return (1);
	}

	if (rslt)
		return (rslt);

	// Verify that keys for this node exist in QD
/*
	nodeid_list_t   node_list;
	node_list.listlen = 1;
	node_list.list = new nodeid_t[1];
	// node_list.list[0] = non_root_node(1);
	node_list.list[0] = orb_conf::get_root_nodeid();

	rslt = verify_keys(1, &node_list);

	delete []node_list.list;
*/
	delete []np_list.node_list;
	delete []np_list.path_list;

	// return (rslt);
	return (0);
#else
	return (no_fault_injection());
#endif
}

//
// CMM Quorum 2-A Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_2_a(int arglen, char *argv[])
#else
int
quorum_2_a(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 2 Fault A\n");

#ifdef _FAULT_INJECTION
	Environment	e;
	int		rslt;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	// Reuse the fault points from quorum_1_a

	rslt = cmm_reconfigure(
		fault_quorum_1_a_on,
		fault_quorum_1_a_off,
		NULL,
		0,
		e);
	if (e.exception()) {
		e.exception()->print_exception("quorum_2_a");
		return (1);
	}
	if (rslt)
		return (rslt);

	// Verify that keys for both nodes exist in QD
	nodeid_list_t	node_list;
	node_list.listlen = 3;
	node_list.list = new nodeid_t[3];
	node_list.list[0] = orb_conf::get_root_nodeid();
	node_list.list[1] = non_root_node(1);
	node_list.list[2] = non_root_node(2);

	rslt = verify_keys(1, &node_list);

	delete []node_list.list;

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// CMM Quorum 2-B Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_2_b(int arglen, char *argv[])
#else
int
quorum_2_b(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 2 Fault B\n");

#ifdef _FAULT_INJECTION
	Environment	e;
	int		rslt;
	np_list_t	np_list;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	// Get the 2nd non-root node
	np_list.listlen = 1;
	np_list.node_list = new nodeid_t[1];
	np_list.path_list = new char *[1];
	np_list.node_list[0] = non_root_node(1);
	np_list.path_list[0] = get_qd_path(1, np_list.node_list[0]);

	os::printf("    Second non-root node: %d\n", np_list.node_list[0]);

	// Reuse the fault points from quorum_1_b, but we've set a different
	// node to have a faulty QD connection

	rslt = cmm_reconfigure(
		fault_quorum_1_b_on,
		fault_quorum_1_b_off,
		&np_list,
		(int)sizeof (np_list),
		e);
	if (e.exception()) {
		e.exception()->print_exception("quorum_2_b");
		return (1);
	}
	if (rslt)
		return (rslt);

	// Verify that keys for both nodes exist in QD
	nodeid_list_t	node_list;
	node_list.listlen = 3;
	node_list.list = new nodeid_t[3];
	node_list.list[0] = orb_conf::get_root_nodeid();
	node_list.list[1] = non_root_node(1);
	node_list.list[2] = non_root_node(2);

	rslt = verify_keys(1, &node_list);

	delete []node_list.list;
	delete []np_list.node_list;
	delete []np_list.path_list;

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// CMM Quorum 2-C Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_2_c(int arglen, char *argv[])
#else
int
quorum_2_c(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 2 Fault C\n");

#ifdef _FAULT_INJECTION
	// Reuse the fault point from quorum_1_b but this time set the
	// node to ALL NODES, therefore the fualt point trigger will severe
	// the communications to the QD from all nodes.
	Environment	e;
	int		rslt;
	np_list_t	np_list;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	// Get the root node
	np_list.listlen = 1;
	np_list.node_list = new nodeid_t[1];
	np_list.path_list = new char *[1];
	np_list.node_list[0] = TRIGGER_ALL_NODES;
	np_list.path_list[0] = NULL;

	rslt = cmm_reconfigure(
		fault_quorum_1_b_on,
		fault_quorum_1_b_off,
		&np_list,
		(int)sizeof (np_list),
		e);
	if (e.exception()) {
		e.exception()->print_exception("quorum_2_c");
		return (1);
	}

	// Since we faulted the QD, no need to check keys
	delete []np_list.node_list;
	delete []np_list.path_list;

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// CMM Quorum 2-D Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_2_d(int arglen, char *argv[])
#else
int
quorum_2_d(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 2 Fault D\n");

#ifdef _FAULT_INJECTION
	Environment	e;
	int		rslt;
	np_list_t	np_list;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	// Get the 1st and 2nd non-root node
	np_list.listlen = 2;
	np_list.node_list = new nodeid_t[2];
	np_list.path_list = new char *[2];
	np_list.node_list[0] = non_root_node(1);
	np_list.node_list[1] = non_root_node(2);
	np_list.path_list[0] = get_qd_path(1, np_list.node_list[0]);
	np_list.path_list[1] = get_qd_path(1, np_list.node_list[1]);

	os::printf("    First non-root node: %d\n", np_list.node_list[0]);
	os::printf("    Second non-root node: %d\n", np_list.node_list[1]);

	// Reuse the fualt point from quorum_1_e with a different node_path
	// pair list

	rslt = cmm_reconfigure(
		fault_quorum_1_e_on,
		fault_quorum_1_e_off,
		&np_list,
		(int)sizeof (np_list),
		e);
	if (e.exception()) {
		e.exception()->print_exception("quorum_2_d");
		return (1);
	}

	os::usecsleep((os::usec_t)15 * 1000000);

	if (rslt)
		return (rslt);

	// Verify that keys for this node exist in QD
	nodeid_list_t	node_list;
	node_list.listlen = 1;
	node_list.list = new nodeid_t[1];
	node_list.list[0] = orb_conf::get_root_nodeid();

	rslt = verify_keys(1, &node_list);

	delete []node_list.list;
	delete []np_list.node_list;
	delete []np_list.path_list;

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// CMM Quorum 2-E Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_2_e(int arglen, char *argv[])
#else
int
quorum_2_e(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 2 Fault E\n");

#ifdef _FAULT_INJECTION
	Environment	e;
	int		rslt;
	np_list_t	np_list;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	// Get the 1st and 2nd non-root node
	// Get the root node (we'll isolate that node)
	np_list.listlen = 1;
	np_list.node_list = new nodeid_t[1];
	np_list.path_list = new char *[1];
	np_list.node_list[0] = orb_conf::get_root_nodeid();
	np_list.path_list[0] = get_qd_path(1, np_list.node_list[0]);

	os::printf("    Root node: %d\n", np_list.node_list[0]);

	// Reuse the fault point from quorum_1_e with a different node_path
	// pair list

	rslt = cmm_reconfigure(
		fault_quorum_1_e_on,
		fault_quorum_1_e_off,
		&np_list,
		(int)sizeof (np_list),
		e);
	if (e.exception()) {
		e.exception()->print_exception("quorum_2_e");
		return (1);
	}

	os::usecsleep((os::usec_t)15 * 1000000);

	if (rslt)
		return (rslt);

	// Verify that keys for this node exist in QD
	nodeid_list_t	node_list;
	node_list.listlen = 2;
	node_list.list = new nodeid_t[2];
	node_list.list[0] = non_root_node(1);
	node_list.list[1] = non_root_node(2);

	rslt = verify_keys(1, &node_list);

	delete []node_list.list;
	delete []np_list.node_list;
	delete []np_list.path_list;

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// CMM Quorum 2-F Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_2_f(int arglen, char *argv[])
#else
int
quorum_2_f(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 2 Fault F\n");

#ifdef _FAULT_INJECTION
	Environment			e;
	int				rslt;
	FaultFunctions::wait_for_arg_t	wait_arg;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	// Kill 2nd non-root node
	wait_arg.op = FaultFunctions::WAIT_FOR_DEAD;
	wait_arg.nodeid = non_root_node(2);

	os::printf("    Second non-root node is: %d\n", wait_arg.nodeid);
	FaultFunctions::reboot(0, &wait_arg, (uint32_t)sizeof (wait_arg));

	// Verify that only the keys for this node and the 1st non-root
	// node are left over
	nodeid_list_t	node_list;
	node_list.listlen = 2;
	node_list.list = new nodeid_t[2];
	node_list.list[0] = orb_conf::get_root_nodeid();
	node_list.list[1] = non_root_node(1);

	rslt = verify_keys(1, &node_list);

	delete []node_list.list;

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// CMM Quorum 2-G Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_2_g(int arglen, char *argv[])
#else
int
quorum_2_g(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 2 Fault G\n");

#ifdef _FAULT_INJECTION
	Environment			e;
	int				rslt;
	FaultFunctions::wait_for_arg_t	wait_arg;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	// Kill the root node
	wait_arg.op = FaultFunctions::WAIT_FOR_DEAD;
	wait_arg.nodeid = orb_conf::get_root_nodeid();

	os::printf("    Root node is: %d\n", wait_arg.nodeid);
	FaultFunctions::reboot(0, &wait_arg, (uint32_t)sizeof (wait_arg));

	// Verify that only the keys for this node and the 1st non-root
	// node are left over
	nodeid_list_t	node_list;
	node_list.listlen = 2;
	node_list.list = new nodeid_t[2];
	node_list.list[0] = non_root_node(1);
	node_list.list[1] = non_root_node(2);

	rslt = verify_keys(1, &node_list);

	delete []node_list.list;

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// CMM Quorum 2-H Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_2_h(int arglen, char *argv[])
#else
int
quorum_2_h(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 2 Fault H\n");

#ifdef _FAULT_INJECTION
	Environment			e;
	FaultFunctions::wait_for_arg_t	wait_arg;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	// Set fault point so we cant talk to the QD
	NodeTriggers::add(FAULTNUM_CMM_QUORUM_DEVICE_FAIL,
	    NULL, 0, TRIGGER_ALL_NODES);

	// Add trigger to stop the node from dumping core when aborting
	FaultFunctions::wait_for_arg_t  wait_arg2;
	wait_arg2.op = FaultFunctions::WAIT_FOR_UNKNOWN;
	wait_arg2.nodeid = 0;
	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT,
	    FAULTNUM_CMM_STOP_ABORT, &wait_arg2, (uint32_t)sizeof (wait_arg2),
	    TRIGGER_ALL_NODES);

	wait_arg.op = FaultFunctions::WAIT_FOR_DEAD;
	wait_arg.nodeid = orb_conf::get_root_nodeid();
	// Kill the Root node
	os::printf("    Root node is: %d\n", wait_arg.nodeid);
	FaultFunctions::reboot(0, &wait_arg, (uint32_t)sizeof (wait_arg));

	// XXX - We'll never make it this far.. since this node will be aborted
	// when it cant reach quorum .. (cant get the QD)
	ASSERT(false);

	return (0);
#else
	return (no_fault_injection());
#endif
}


//
// CMM Quorum 3-A Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_3_a(int arglen, char *argv[])
#else
int
quorum_3_a(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 3 Fault A\n");

#ifdef _FAULT_INJECTION
	Environment	e;
	int		rslt;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	// Reuse the fault points from quorum_1_a

	rslt = cmm_reconfigure(
		fault_quorum_1_a_on,
		fault_quorum_1_a_off,
		NULL,
		0,
		e);
	if (e.exception()) {
		e.exception()->print_exception("quorum_3_a");
		return (1);
	}
	if (rslt)
		return (rslt);

	// Verify that keys for both nodes exist in QD
	nodeid_list_t	node_list;
	node_list.listlen = 4;
	node_list.list = new nodeid_t[4];
	node_list.list[0] = orb_conf::get_root_nodeid();
	node_list.list[1] = non_root_node(1);
	node_list.list[2] = non_root_node(2);
	node_list.list[3] = non_root_node(3);

	rslt = verify_keys(1, &node_list);

	delete []node_list.list;

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// CMM Quorum 3-B Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_3_b(int arglen, char *argv[])
#else
int
quorum_3_b(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 3 Fault B\n");

#ifdef _FAULT_INJECTION
	Environment	e;
	int		rslt;
	np_list_t	np_list;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	// Get the 3rd non-root node
	np_list.listlen = 1;
	np_list.node_list = new nodeid_t[1];
	np_list.path_list = new char *[1];
	np_list.node_list[0] = non_root_node(3);
	np_list.path_list[0] = get_qd_path(1, np_list.node_list[0]);

	os::printf("    Third non-root node: %d\n", np_list.node_list[0]);

	// Reuse the fault points from quorum_1_b, but we've set a different
	// node to have a faulty QD connection

	rslt = cmm_reconfigure(
		fault_quorum_1_b_on,
		fault_quorum_1_b_off,
		&np_list,
		(int)sizeof (np_list),
		e);
	if (e.exception()) {
		e.exception()->print_exception("quorum_3_b");
		return (1);
	}
	if (rslt)
		return (rslt);

	// Verify that all keys exist in QD
	nodeid_list_t	node_list;
	node_list.listlen = 4;
	node_list.list = new nodeid_t[4];
	node_list.list[0] = orb_conf::get_root_nodeid();
	node_list.list[1] = non_root_node(1);
	node_list.list[2] = non_root_node(2);
	node_list.list[3] = non_root_node(3);

	rslt = verify_keys(1, &node_list);

	delete []node_list.list;
	delete []np_list.node_list;
	delete []np_list.path_list;

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// CMM Quorum 3-C Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_3_c(int arglen, char *argv[])
#else
int
quorum_3_c(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 3 Fault C\n");

#ifdef _FAULT_INJECTION
	// Reuse the fault point from quorum_1_b but this time set the
	// node to ALL NODES, therefore the fualt point trigger will severe
	// the communications to the QD from all nodes.
	Environment	e;
	int		rslt;
	np_list_t	np_list;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	// Get the root node
	np_list.listlen = 1;
	np_list.node_list = new nodeid_t[1];
	np_list.path_list = new char *[1];
	np_list.node_list[0] = TRIGGER_ALL_NODES;
	np_list.path_list[0] = NULL;

	rslt = cmm_reconfigure(
		fault_quorum_1_b_on,
		fault_quorum_1_b_off,
		&np_list,
		(int)sizeof (np_list),
		e);
	if (e.exception()) {
		e.exception()->print_exception("quorum_3_c");
		return (1);
	}

	// Since we faulted the QD, no need to check keys
	delete []np_list.node_list;
	delete []np_list.path_list;

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// CMM Quorum 3-D Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_3_d(int arglen, char *argv[])
#else
int
quorum_3_d(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 3 Fault D\n");

#ifdef _FAULT_INJECTION
	Environment	e;
	int		rslt;
	np_list_t	np_list;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	// Get the 2nd and 3rd non-root node
	np_list.listlen = 2;
	np_list.node_list = new nodeid_t[2];
	np_list.path_list = new char *[2];
	np_list.node_list[0] = non_root_node(2);
	np_list.node_list[1] = non_root_node(3);
	np_list.path_list[0] = get_qd_path(1, np_list.node_list[0]);
	np_list.path_list[1] = get_qd_path(1, np_list.node_list[1]);

	os::printf("    Second non-root node: %d\n", np_list.node_list[0]);
	os::printf("    Third non-root node: %d\n", np_list.node_list[1]);

	// Reuse the fualt point from quorum_1_e with a different node_path
	// pair list

	rslt = cmm_reconfigure(
		fault_quorum_1_e_on,
		fault_quorum_1_e_off,
		&np_list,
		(int)sizeof (np_list),
		e);
	if (e.exception()) {
		e.exception()->print_exception("quorum_3_d");
		return (1);
	}

	os::usecsleep((os::usec_t)15 * 1000000);

	if (rslt)
		return (rslt);

	// Verify that keys for this node exist in QD
	nodeid_list_t	node_list;
	node_list.listlen = 2;
	node_list.list = new nodeid_t[2];
	node_list.list[0] = orb_conf::get_root_nodeid();
	node_list.list[1] = non_root_node(1);

	rslt = verify_keys(1, &node_list);

	delete []node_list.list;
	delete []np_list.node_list;
	delete []np_list.path_list;

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// CMM Quorum 3-E Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_3_e(int arglen, char *argv[])
#else
int
quorum_3_e(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 3 Fault E\n");

#ifdef _FAULT_INJECTION
	Environment	e;
	int		rslt;
	np_list_t	np_list;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	// Get the root node (we'll isolate that node)
	// Get the 1st nonroot node
	np_list.listlen = 2;
	np_list.node_list = new nodeid_t[2];
	np_list.path_list = new char *[2];
	np_list.node_list[0] = orb_conf::get_root_nodeid();
	np_list.node_list[1] = non_root_node(1);
	np_list.path_list[0] = get_qd_path(1, np_list.node_list[0]);
	np_list.path_list[1] = get_qd_path(1, np_list.node_list[1]);

	os::printf("    Root node: %d\n", np_list.node_list[0]);
	os::printf("    1st Non-root node: %d\n", np_list.node_list[1]);

	// Reuse the fault point from quorum_1_e with a different node_path
	// pair list

	rslt = cmm_reconfigure(
		fault_quorum_1_e_on,
		fault_quorum_1_e_off,
		&np_list,
		(int)sizeof (np_list),
		e);
	if (e.exception()) {
		e.exception()->print_exception("quorum_2_d");
		return (1);
	}

	os::usecsleep((os::usec_t)15 * 1000000);

	if (rslt)
		return (rslt);

	// Verify that keys for this node exist in QD
	nodeid_list_t	node_list;
	node_list.listlen = 2;
	node_list.list = new nodeid_t[2];
	node_list.list[0] = non_root_node(2);
	node_list.list[1] = non_root_node(3);

	rslt = verify_keys(1, &node_list);

	delete []node_list.list;
	delete []np_list.node_list;
	delete []np_list.path_list;

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// CMM Quorum 3-F Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_3_f(int arglen, char *argv[])
#else
int
quorum_3_f(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 3 Fault F\n");

#ifdef _FAULT_INJECTION
	Environment			e;
	int				rslt;
	FaultFunctions::wait_for_arg_t	wait_arg;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	// Kill 2nd non-root node
	wait_arg.op = FaultFunctions::WAIT_FOR_DEAD;
	wait_arg.nodeid = non_root_node(2);

	os::printf("    Second non-root node is: %d\n", wait_arg.nodeid);
	FaultFunctions::reboot(0, &wait_arg, (uint32_t)sizeof (wait_arg));

	// Verify that only the keys for this node and the 1st non-root
	// and the 3rd non-root node node are left over
	nodeid_list_t	node_list;
	node_list.listlen = 3;
	node_list.list = new nodeid_t[3];
	node_list.list[0] = orb_conf::get_root_nodeid();
	node_list.list[1] = non_root_node(1);
	node_list.list[2] = non_root_node(3);

	rslt = verify_keys(1, &node_list);

	delete []node_list.list;

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// CMM Quorum 3-G Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_3_g(int arglen, char *argv[])
#else
int
quorum_3_g(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 3 Fault G\n");

#ifdef _FAULT_INJECTION
	Environment			e;
	int				rslt;
	FaultFunctions::wait_for_arg_t	wait_arg;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	// Kill the root node
	wait_arg.op = FaultFunctions::WAIT_FOR_DEAD;
	wait_arg.nodeid = orb_conf::get_root_nodeid();

	os::printf("    Root node is: %d\n", wait_arg.nodeid);
	FaultFunctions::reboot(0, &wait_arg, (uint32_t)sizeof (wait_arg));

	// Verify that only the keys for this node and the 1st non-root
	// node are left over
	nodeid_list_t	node_list;
	node_list.listlen = 3;
	node_list.list = new nodeid_t[3];
	node_list.list[0] = non_root_node(1);
	node_list.list[1] = non_root_node(2);
	node_list.list[2] = non_root_node(3);

	rslt = verify_keys(1, &node_list);

	delete []node_list.list;

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// CMM Quorum 3-H Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_3_h(int arglen, char *argv[])
#else
int
quorum_3_h(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 3 Fault H\n");

#ifdef _FAULT_INJECTION
	Environment			e;
	FaultFunctions::wait_for_arg_t	wait_arg;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	// Set fault point so we cant talk to the QD
	NodeTriggers::add(FAULTNUM_CMM_QUORUM_DEVICE_FAIL,
	    NULL, 0, TRIGGER_ALL_NODES);

	// Add trigger to stop the node from dumping core when aborting
	FaultFunctions::wait_for_arg_t  wait_arg2;
	wait_arg2.op = FaultFunctions::WAIT_FOR_UNKNOWN;
	wait_arg2.nodeid = 0;
	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT,
	    FAULTNUM_CMM_STOP_ABORT, &wait_arg2, (uint32_t)sizeof (wait_arg2),
	    TRIGGER_ALL_NODES);

	wait_arg.op = FaultFunctions::NO_WAIT;
	wait_arg.nodeid = orb_conf::get_root_nodeid();
	// Kill the first Root node
	os::printf("    Root node is: %d\n", wait_arg.nodeid);
	FaultFunctions::reboot(0, &wait_arg, (uint32_t)sizeof (wait_arg));

	// Kill the  1st non - root node
	wait_arg.op = FaultFunctions::WAIT_FOR_DEAD;
	wait_arg.nodeid = non_root_node(1);
	os::printf("    Root node is: %d\n", wait_arg.nodeid);
	FaultFunctions::reboot(0, &wait_arg, (uint32_t)sizeof (wait_arg));

	// XXX - We'll never make it this far.. since this node will be aborted
	// when it cant reach quorum .. (cant get the QD)
	ASSERT(false);

	return (0);
#else
	return (no_fault_injection());
#endif
}


//
// CMM Quorum 4-A Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_4_a(int arglen, char *argv[])
#else
int
quorum_4_a(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 4 Fault A\n");

#ifdef _FAULT_INJECTION
	Environment	e;
	int		rslt;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	// Reuse the fault point from quorum_4_a .. (this does no faults)
	// just a reconfigure

	rslt = cmm_reconfigure(
		fault_quorum_1_a_on,
		fault_quorum_1_a_off,
		NULL,
		0,
		e);
	if (e.exception()) {
		e.exception()->print_exception("quorum_4_a");
		return (1);
	}
	if (rslt)
		return (rslt);

	// Verify that keys for both nodes exist in QD
	nodeid_list_t	node_list;
	node_list.listlen = 2;
	node_list.list = new nodeid_t[2];

	// Verify keys of node 1 and 4 exist on QD1
	node_list.list[0] = orb_conf::get_root_nodeid();
	node_list.list[1] = non_root_node(3);
	rslt = verify_keys(1, &node_list);
	if (rslt) {
		os::printf("Keys of node 1 and 4 don't exist on QD1");
		delete []node_list.list;
		return (rslt);
	}
	os::printf("Keys of node 1 and 4 exist on QD1");

	// Verify keys of node 2 and 3 don't exist on QD1
	node_list.list[0] = non_root_node(1);
	node_list.list[1] = non_root_node(2);
	rslt = verify_keys(1, &node_list, true, false);
	if (rslt) {
		os::printf("Keys of node 2 and 3 exist on QD1 unexpectedly");
		delete []node_list.list;
		return (rslt);
	}
	os::printf("Keys of node 2 and 3 don't exist on QD1 as expected");


	// Verify keys of node 2 and 4 exist on QD2
	node_list.list[0] = non_root_node(1);
	node_list.list[1] = non_root_node(3);
	rslt = verify_keys(2, &node_list);
	if (rslt) {
		os::printf("Keys of node 2 and 4 don't exist on QD2");
		delete []node_list.list;
		return (rslt);
	}
	os::printf("Keys of node 2 and 4 exist on QD2");

	// Verify keys of node 1 and 3 don't exist on QD2
	node_list.list[0] = orb_conf::get_root_nodeid();
	node_list.list[1] = non_root_node(2);
	rslt = verify_keys(2, &node_list, true, false);
	if (rslt) {
		os::printf("Keys of node 1 and 3 exist on QD2 unexpectedly");
		delete []node_list.list;
		return (rslt);
	}
	os::printf("Keys of node 1 and 3 don't exist on QD2 as expected");

	// Verify keys of node 3 and 4 exist on QD3
	node_list.list[0] = non_root_node(2);
	node_list.list[1] = non_root_node(3);
	rslt = verify_keys(3, &node_list);
	if (rslt) {
		os::printf("Keys of node 3 and 4 don't exist on QD3");
		delete []node_list.list;
		return (rslt);
	}
	os::printf("Keys of node 3 and 4 exist on QD3");

	// Verify keys of node 1 and 2 don't exist on QD3
	node_list.list[0] = orb_conf::get_root_nodeid();
	node_list.list[1] = non_root_node(1);
	rslt = verify_keys(3, &node_list, true, false);
	if (rslt) {
		os::printf("Keys of node 1 and 2 exist on QD3 unexpectedly");
	}
	os::printf("Keys of node 1 and 2 don't exist on QD3 as expected");

	delete []node_list.list;

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// CMM Quorum 4-B Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_4_b(int arglen, char *argv[])
#else
int
quorum_4_b(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 4 Fault B\n");

#ifdef _FAULT_INJECTION
	// Reuse the fault point from quorum_1_b but this time set the
	// path to the 2 nodes that talk to quorum device 1
	Environment	e;
	int		rslt;
	np_list_t	np_list;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	// Get the root node
	np_list.listlen = 2;
	np_list.node_list = new nodeid_t[2];
	np_list.path_list = new char *[2];
	np_list.node_list[0] = orb_conf::get_root_nodeid();
	np_list.path_list[0] = get_qd_path(1, np_list.node_list[0]);
	np_list.node_list[1] = non_root_node(3);
	np_list.path_list[1] = get_qd_path(1, np_list.node_list[1]);

	rslt = cmm_reconfigure(
		fault_quorum_1_b_on,
		fault_quorum_1_b_off,
		&np_list,
		(int)sizeof (np_list),
		e);
	if (e.exception()) {
		e.exception()->print_exception("quorum_4_b");
		return (1);
	}

	delete []np_list.node_list;
	delete []np_list.path_list;

	if (rslt)
		return (rslt);

	// Verify that keys for both nodes exist in QD
	nodeid_list_t	node_list;
	node_list.listlen = 2;
	node_list.list = new nodeid_t[2];

	// Verify keys of node 2 and 4 exist on QD2
	node_list.list[0] = non_root_node(1);
	node_list.list[1] = non_root_node(3);
	rslt = verify_keys(2, &node_list);
	if (rslt) {
		os::printf("Keys of node 2 and 4 don't exist on QD2");
		delete []node_list.list;
		return (rslt);
	}
	os::printf("Keys of node 2 and 4 exist on QD2");

	// Verify keys of node 1 and 3 don't exist on QD2
	node_list.list[0] = orb_conf::get_root_nodeid();
	node_list.list[1] = non_root_node(2);
	rslt = verify_keys(2, &node_list, true, false);
	if (rslt) {
		os::printf("Keys of node 1 and 3 exist on QD2 unexpectedly");
		delete []node_list.list;
		return (rslt);
	}
	os::printf("Keys of node 1 and 3 don't exist on QD2 as expected");

	// Verify keys of node 3 and 4 exist on QD3
	node_list.list[0] = non_root_node(2);
	node_list.list[1] = non_root_node(3);
	rslt = verify_keys(3, &node_list);
	if (rslt) {
		os::printf("Keys of node 3 and 4 don't exist on QD3");
		delete []node_list.list;
		return (rslt);
	}
	os::printf("Keys of node 3 and 4 exist on QD3");

	// Verify keys of node 1 and 2 don't exist on QD3
	node_list.list[0] = orb_conf::get_root_nodeid();
	node_list.list[1] = non_root_node(1);
	rslt = verify_keys(3, &node_list, true, false);
	if (rslt) {
		os::printf("Keys of node 1 and 2 exist on QD3 unexpectedly");
	}
	os::printf("Keys of node 1 and 2 don't exist on QD3 as expected");

	delete []node_list.list;

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// CMM Quorum 4-C Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_4_c(int arglen, char *argv[])
#else
int
quorum_4_c(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 4 Fault C\n");

#ifdef _FAULT_INJECTION
	// Reuse the fault point from quorum_1_b but this time set the
	// path to the 2 nodes that talk to quorum device 2
	Environment	e;
	int		rslt;
	np_list_t	np_list;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	// Get the root node
	np_list.listlen = 2;
	np_list.node_list = new nodeid_t[2];
	np_list.path_list = new char *[2];
	np_list.node_list[0] = non_root_node(1);
	np_list.path_list[0] = get_qd_path(2, np_list.node_list[0]);
	np_list.node_list[1] = non_root_node(3);
	np_list.path_list[1] = get_qd_path(2, np_list.node_list[1]);

	rslt = cmm_reconfigure(
		fault_quorum_1_b_on,
		fault_quorum_1_b_off,
		&np_list,
		(int)sizeof (np_list),
		e);
	if (e.exception()) {
		e.exception()->print_exception("quorum_4_c");
		return (1);
	}

	delete []np_list.node_list;
	delete []np_list.path_list;

	if (rslt)
		return (rslt);

	// Verify that keys for both nodes exist in QD
	nodeid_list_t	node_list;
	node_list.listlen = 2;
	node_list.list = new nodeid_t[2];

	// Verify keys of node 1 and 4 exist on QD1
	node_list.list[0] = orb_conf::get_root_nodeid();
	node_list.list[1] = non_root_node(3);
	rslt = verify_keys(1, &node_list);
	if (rslt) {
		os::printf("Keys of node 1 and 4 don't exist on QD1");
		delete []node_list.list;
		return (rslt);
	}
	os::printf("Keys of node 1 and 4 exist on QD1");

	// Verify keys of node 2 and 3 don't exist on QD1
	node_list.list[0] = non_root_node(1);
	node_list.list[1] = non_root_node(2);
	rslt = verify_keys(1, &node_list, true, false);
	if (rslt) {
		os::printf("Keys of node 2 and 3 exist on QD1 unexpectedly");
		delete []node_list.list;
		return (rslt);
	}
	os::printf("Keys of node 2 and 3 don't exist on QD1 as expected");

	// Verify keys of node 3 and 4 exist on QD3
	node_list.list[0] = non_root_node(2);
	node_list.list[1] = non_root_node(3);
	rslt = verify_keys(3, &node_list);
	if (rslt) {
		os::printf("Keys of node 3 and 4 don't exist on QD3");
		delete []node_list.list;
		return (rslt);
	}
	os::printf("Keys of node 3 and 4 exist on QD3");

	// Verify keys of node 1 and 2 don't exist on QD3
	node_list.list[0] = orb_conf::get_root_nodeid();
	node_list.list[1] = non_root_node(1);
	rslt = verify_keys(3, &node_list, true, false);
	if (rslt) {
		os::printf("Keys of node 1 and 2 exist on QD3 unexpectedly");
	}
	os::printf("Keys of node 1 and 2 don't exist on QD3 as expected");

	delete []node_list.list;

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// CMM Quorum 4-D Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_4_d(int arglen, char *argv[])
#else
int
quorum_4_d(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 4 Fault D\n");

#ifdef _FAULT_INJECTION
	// Reuse the fault point from quorum_1_b but this time set the
	// path to the 2 nodes that talk to quorum device 3
	Environment	e;
	int		rslt;
	np_list_t	np_list;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	// Get the root node
	np_list.listlen = 2;
	np_list.node_list = new nodeid_t[2];
	np_list.path_list = new char *[2];
	np_list.node_list[0] = non_root_node(2);
	np_list.path_list[0] = get_qd_path(3, np_list.node_list[0]);
	np_list.node_list[1] = non_root_node(3);
	np_list.path_list[1] = get_qd_path(3, np_list.node_list[1]);

	rslt = cmm_reconfigure(
		fault_quorum_1_b_on,
		fault_quorum_1_b_off,
		&np_list,
		(int)sizeof (np_list),
		e);
	if (e.exception()) {
		e.exception()->print_exception("quorum_4_d");
		return (1);
	}

	delete []np_list.node_list;
	delete []np_list.path_list;

	if (rslt)
		return (rslt);

	// Verify that keys for both nodes exist in QD
	nodeid_list_t	node_list;
	node_list.listlen = 2;
	node_list.list = new nodeid_t[2];

	// Verify keys of node 1 and 4 exist on QD1
	node_list.list[0] = orb_conf::get_root_nodeid();
	node_list.list[1] = non_root_node(3);
	rslt = verify_keys(1, &node_list);
	if (rslt) {
		os::printf("Keys of node 1 and 4 don't exist on QD1");
		delete []node_list.list;
		return (rslt);
	}
	os::printf("Keys of node 1 and 4 exist on QD1");

	// Verify keys of node 2 and 3 don't exist on QD1
	node_list.list[0] = non_root_node(1);
	node_list.list[1] = non_root_node(2);
	rslt = verify_keys(1, &node_list, true, false);
	if (rslt) {
		os::printf("Keys of node 2 and 3 exist on QD1 unexpectedly");
		delete []node_list.list;
		return (rslt);
	}
	os::printf("Keys of node 2 and 3 don't exist on QD1 as expected");


	// Verify keys of node 2 and 4 exist on QD2
	node_list.list[0] = non_root_node(1);
	node_list.list[1] = non_root_node(3);
	rslt = verify_keys(2, &node_list);
	if (rslt) {
		os::printf("Keys of node 2 and 4 don't exist on QD2");
		delete []node_list.list;
		return (rslt);
	}
	os::printf("Keys of node 2 and 4 exist on QD2");

	// Verify keys of node 1 and 3 don't exist on QD2
	node_list.list[0] = orb_conf::get_root_nodeid();
	node_list.list[1] = non_root_node(2);
	rslt = verify_keys(2, &node_list, true, false);
	if (rslt) {
		os::printf("Keys of node 1 and 3 exist on QD2 unexpectedly");
		delete []node_list.list;
		return (rslt);
	}
	os::printf("Keys of node 1 and 3 don't exist on QD2 as expected");

	delete []node_list.list;

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// CMM Quorum 4-E Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_4_e(int arglen, char *argv[])
#else
int
quorum_4_e(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 4 Fault E\n");

#ifdef _FAULT_INJECTION
	// Reuse the fault point from quorum_1_b but this time set the
	// path to all the QD's from all NODES
	// path to the 2 nodes that talk to quorum device 3
	Environment	e;
	int		rslt;
	np_list_t	np_list;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	// Get the root node
	np_list.listlen = 1;
	np_list.node_list = new nodeid_t[1];
	np_list.path_list = new char *[1];
	np_list.node_list[0] = TRIGGER_ALL_NODES;
	np_list.path_list[0] = NULL;

	rslt = cmm_reconfigure(
		fault_quorum_1_b_on,
		fault_quorum_1_b_off,
		&np_list,
		(int)sizeof (np_list),
		e);
	if (e.exception()) {
		e.exception()->print_exception("quorum_4_e");
		return (1);
	}

	delete []np_list.node_list;
	delete []np_list.path_list;

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// CMM Quorum 4-F Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_4_f(int arglen, char *argv[])
#else
int
quorum_4_f(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 4 Fault F\n");

#ifdef _FAULT_INJECTION
	Environment	e;
	int		rslt, i;
	np_list_t	np_list;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	// Get the 1st, 2nd and 3rd non-root nodes
	np_list.listlen = 3;
	np_list.node_list = new nodeid_t[3];
	np_list.path_list = new char *[3];
	np_list.node_list[0] = orb_conf::get_root_nodeid();
	np_list.node_list[1] = non_root_node(1);
	np_list.node_list[2] = non_root_node(2);
	np_list.path_list[0] = get_qd_path(1, np_list.node_list[0]);
	np_list.path_list[1] = get_qd_path(2, np_list.node_list[1]);
	np_list.path_list[2] = get_qd_path(3, np_list.node_list[2]);

	os::printf("    First disconnected node: %d\n", np_list.node_list[0]);
	os::printf("    Second disconnected node: %d\n", np_list.node_list[1]);
	os::printf("    Third disconnected node: %d\n", np_list.node_list[2]);

	// Reuse the fault point from quorum_1_e with a different node_path
	// pair list

	rslt = cmm_reconfigure(
		fault_quorum_1_e_on,
		fault_quorum_1_e_off,
		&np_list,
		(int)sizeof (np_list),
		e);
	if (e.exception()) {
		e.exception()->print_exception("quorum_4_f");
		return (1);
	}

	os::usecsleep((os::usec_t)15 * 1000000);

	delete []np_list.node_list;
	delete []np_list.path_list;

	if (rslt)
		return (rslt);

	// Verify that the key for the surviving node exists in QD
	nodeid_list_t	node_list;
	node_list.listlen = 1;
	node_list.list = new nodeid_t[1];

	node_list.list[0] = non_root_node(3);

	for (i = 1; i <= 3; i++) {
		rslt = verify_keys(i, &node_list);
		if (rslt) {
			os::printf("Keys of node 4 don't exist on QD%d", i);
			delete []node_list.list;
			return (rslt);
		}
		os::printf("Keys of node 4 exist on QD%d", i);
	}

	delete []node_list.list;


	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// CMM Quorum 4-G Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_4_g(int arglen, char *argv[])
#else
int
quorum_4_g(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 4 Fault G\n");

#ifdef _FAULT_INJECTION
	Environment	e;
	int		rslt;
	np_list_t	np_list;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	// Get the root node
	np_list.listlen = 1;
	np_list.node_list = new nodeid_t[1];
	np_list.path_list = new char *[1];
	np_list.node_list[0] = non_root_node(3);
	np_list.path_list[0] = get_qd_path(1, np_list.node_list[0]);

	os::printf("    Disconnected N+1 node: %d\n", np_list.node_list[0]);

	// Reuse the fault point from quorum_1_e with a different node_path
	// pair list

	rslt = cmm_reconfigure(
		fault_quorum_1_e_on,
		fault_quorum_1_e_off,
		&np_list,
		(int)sizeof (np_list),
		e);
	if (e.exception()) {
		e.exception()->print_exception("quorum_4_g");
		return (1);
	}

	os::usecsleep((os::usec_t)15 * 1000000);

	delete []np_list.node_list;
	delete []np_list.path_list;


	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// CMM Quorum 4-H Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_4_h(int arglen, char *argv[])
#else
int
quorum_4_h(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 4 Fault H\n");

#ifdef _FAULT_INJECTION
	Environment			e;
	int				rslt;
	FaultFunctions::wait_for_arg_t	wait_arg;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	// Kill the first node
	wait_arg.op = FaultFunctions::WAIT_FOR_DEAD;
	wait_arg.nodeid = orb_conf::get_root_nodeid();

	os::printf("    First node is: %d\n", wait_arg.nodeid);
	FaultFunctions::reboot(0, &wait_arg, (uint32_t)sizeof (wait_arg));

	// Sleep here so other nodes can have enough time to preempt
	// the key of the killed node from the QD
	os::usecsleep((os::usec_t)15 * 1000000);

	// Verify that the keys of surviving nodes exist on the QDs.
	nodeid_list_t	node_list;

	// Only key from node 4 is expected on QD1
	node_list.listlen = 1;
	node_list.list = new nodeid_t[1];
	node_list.list[0] = non_root_node(3);
	rslt = verify_keys(1, &node_list);
	if (rslt) {
		os::printf("Key of node 4 is not found unexpectedly");
		delete []node_list.list;
		return (rslt);
	}
	os::printf("Key of node 4 is found");
	delete []node_list.list;

	// Keys of node 2 and 4 are expected on QD2
	node_list.listlen = 2;
	node_list.list = new nodeid_t[2];
	node_list.list[0] = non_root_node(1);
	node_list.list[1] = non_root_node(3);
	rslt = verify_keys(2, &node_list);
	if (rslt) {
		os::printf("Keys of node 2 or/and 4 are not found "
			"unexpectedly");
		delete []node_list.list;
		return (rslt);
	}
	os::printf("Keys of node 2 and 4 are found");
	delete []node_list.list;

	// Keys of node 3 and 4 are expected on QD3
	node_list.listlen = 2;
	node_list.list = new nodeid_t[2];
	node_list.list[0] = non_root_node(2);
	node_list.list[1] = non_root_node(3);
	rslt = verify_keys(3, &node_list);
	if (rslt) {
		os::printf("Keys of node 3 or/and 4 are not found "
			"unexpectedly");
		delete []node_list.list;
		return (rslt);
	}
	os::printf("Keys of node 3 and 4 are found");
	delete []node_list.list;


	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// CMM Quorum 4-I Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_4_i(int arglen, char *argv[])
#else
int
quorum_4_i(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 4 Fault I\n");

#ifdef _FAULT_INJECTION
	Environment			e;
	int				rslt;
	FaultFunctions::wait_for_arg_t	wait_arg;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	// Kill the second node
	wait_arg.op = FaultFunctions::WAIT_FOR_DEAD;
	wait_arg.nodeid = non_root_node(1);

	os::printf("    Second node is: %d\n", wait_arg.nodeid);
	FaultFunctions::reboot(0, &wait_arg, (uint32_t)sizeof (wait_arg));

	// Sleep here so other nodes can have enough time to preempt
	// the key of the killed node from the QD
	os::usecsleep((os::usec_t)15 * 1000000);

	// Verify that the keys of surviving nodes exist on the QDs.
	nodeid_list_t   node_list;

	// Keys from node 1 and 4 are expected on QD1
	node_list.listlen = 2;
	node_list.list = new nodeid_t[2];
	node_list.list[0] = orb_conf::get_root_nodeid();
	node_list.list[1] = non_root_node(3);
	rslt = verify_keys(1, &node_list);
	if (rslt) {
		os::printf("Key of node 1 or/and 4 are not found "
			"unexpectedly");
		delete []node_list.list;
		return (rslt);
	}
	os::printf("Key of node 1 and 4 are found");
	delete []node_list.list;

	// Only key of node 4 are expected on QD2
	node_list.listlen = 1;
	node_list.list = new nodeid_t[1];
	node_list.list[0] = non_root_node(3);
	rslt = verify_keys(2, &node_list);
	if (rslt) {
		os::printf("Key of node 4 is not found "
			"unexpectedly");
		delete []node_list.list;
		return (rslt);
	}
	os::printf("Key of node 4 is found");
	delete []node_list.list;

	// Keys of node 3 and 4 are expected on QD3
	node_list.listlen = 2;
	node_list.list = new nodeid_t[2];
	node_list.list[0] = non_root_node(2);
	node_list.list[1] = non_root_node(3);
	rslt = verify_keys(3, &node_list);
	if (rslt) {
		os::printf("Keys of node 3 or/and 4 are not found "
			"unexpectedly");
		delete []node_list.list;
		return (rslt);
	}
	os::printf("Keys of node 3 and 4 are found");
	delete []node_list.list;

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// CMM Quorum 4-J Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_4_j(int arglen, char *argv[])
#else
int
quorum_4_j(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 4 Fault J\n");

#ifdef _FAULT_INJECTION
	Environment			e;
	int				rslt;
	FaultFunctions::wait_for_arg_t	wait_arg;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	// Kill the third node
	wait_arg.op = FaultFunctions::WAIT_FOR_DEAD;
	wait_arg.nodeid = non_root_node(2);

	os::printf("    Third node is: %d\n", wait_arg.nodeid);
	FaultFunctions::reboot(0, &wait_arg, (uint32_t)sizeof (wait_arg));

	// Sleep here so other nodes can have enough time to preempt
	// the key of the killed node from the QD
	os::usecsleep((os::usec_t)15 * 1000000);

	// Verify that the keys of surviving nodes exist on the QDs.
	nodeid_list_t   node_list;

	// Keys from node 1 and 4 are expected on QD1
	node_list.listlen = 2;
	node_list.list = new nodeid_t[2];
	node_list.list[0] = orb_conf::get_root_nodeid();
	node_list.list[1] = non_root_node(3);
	rslt = verify_keys(1, &node_list);
	if (rslt) {
		os::printf("Key of node 1 or/and 4 are not found "
			"unexpectedly");
		delete []node_list.list;
		return (rslt);
	}
	os::printf("Key of node 1 and 4 are found");
	delete []node_list.list;

	// Keys of node 2 and 4 are expected on QD2
	node_list.listlen = 2;
	node_list.list = new nodeid_t[2];
	node_list.list[0] = non_root_node(1);
	node_list.list[1] = non_root_node(3);
	rslt = verify_keys(2, &node_list);
	if (rslt) {
		os::printf("Keys of node 2 or/and 4 are not found "
			"unexpectedly");
		delete []node_list.list;
		return (rslt);
	}
	os::printf("Keys of node 2 and 4 are found");
	delete []node_list.list;

	// Only key from node 4 is expected on QD3
	node_list.listlen = 1;
	node_list.list = new nodeid_t[1];
	node_list.list[0] = non_root_node(3);
	rslt = verify_keys(3, &node_list);
	if (rslt) {
		os::printf("Key of node 4 is not found unexpectedly");
		delete []node_list.list;
		return (rslt);
	}
	os::printf("Key of node 4 is found");
	delete []node_list.list;

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// CMM Quorum 4-K Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_4_k(int arglen, char *argv[])
#else
int
quorum_4_k(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 4 Fault K\n");

#ifdef _FAULT_INJECTION
	Environment			e;
	FaultFunctions::wait_for_arg_t	wait_arg;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	// Kill the 4th node (+1 node)
	wait_arg.op = FaultFunctions::WAIT_FOR_DEAD;
	wait_arg.nodeid = non_root_node(3);

	os::printf("    Fourth node is: %d\n", wait_arg.nodeid);
	FaultFunctions::reboot(0, &wait_arg, (uint32_t)sizeof (wait_arg));

	return (0);
#else
	return (no_fault_injection());
#endif
}


//
// CMM Quorum 4-L Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_4_l(int arglen, char *argv[])
#else
int
quorum_4_l(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 4 Fault L\n");

#ifdef _FAULT_INJECTION
	Environment			e;
	int				rslt;
	FaultFunctions::wait_for_arg_t	wait_arg;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	//  Kill 1st, 2nd and 3rd nodes
	// wait_arg.op = FaultFunctions::NO_WAIT;
	wait_arg.op = FaultFunctions::WAIT_FOR_DEAD;

	wait_arg.nodeid = orb_conf::get_root_nodeid();
	os::printf("    First node is: %d\n", wait_arg.nodeid);
	FaultFunctions::reboot(0, &wait_arg, (uint32_t)sizeof (wait_arg));

	wait_arg.nodeid = non_root_node(1);
	os::printf("    Second node is: %d\n", wait_arg.nodeid);
	FaultFunctions::reboot(0, &wait_arg, (uint32_t)sizeof (wait_arg));

	wait_arg.nodeid = non_root_node(2);
	os::printf("    Third node is: %d\n", wait_arg.nodeid);
	FaultFunctions::reboot(0, &wait_arg, (uint32_t)sizeof (wait_arg));

	// Sleep here so other nodes can have enough time to preempt
	// the key of the killed node from the QD
	// os::usecsleep(15 * 1000000);

	// Verify that the keys of surviving nodes exist on the QDs.
	nodeid_list_t   node_list;

	// Only key from node 4 is expected on QD1
	node_list.listlen = 1;
	node_list.list = new nodeid_t[1];
	node_list.list[0] = non_root_node(3);
	rslt = verify_keys(1, &node_list);
	if (rslt) {
		os::printf("Key of node 4 is not found unexpectedly");
		delete []node_list.list;
		return (rslt);
	}
	os::printf("Key of node 4 is found");
	delete []node_list.list;

	// Only key from node 4 is expected on QD2
	node_list.listlen = 1;
	node_list.list = new nodeid_t[1];
	node_list.list[0] = non_root_node(3);
	rslt = verify_keys(2, &node_list);
	if (rslt) {
		os::printf("Key of node 4 is not found unexpectedly");
		delete []node_list.list;
		return (rslt);
	}
	os::printf("Key of node 4 is found");
	delete []node_list.list;

	// Only key from node 4 is expected on QD3
	node_list.listlen = 1;
	node_list.list = new nodeid_t[1];
	node_list.list[0] = non_root_node(3);
	rslt = verify_keys(3, &node_list);
	if (rslt) {
		os::printf("Key of node 4 is not found unexpectedly");
		delete []node_list.list;
		return (rslt);
	}
	os::printf("Key of node 4 is found");
	delete []node_list.list;

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// CMM Quorum 4-M Test case
//
#if defined(_FAULT_INJECTION)
int
quorum_4_m(int arglen, char *argv[])
#else
int
quorum_4_m(int, char *[])
#endif
{
	os::printf("*** CMM Quorum Scenario 4 Fault E\n");

#ifdef _FAULT_INJECTION
	// Reuse the fault point from quorum_1_e but this time set the
	// path to all the QD's from all NODES
	// path to the 2 nodes that talk to quorum device 3
	// We'll also force a 2nd failure that will cause the entire cluster
	// to fail due to the fact that nonone can gain quorum
	// In this case QD's are down and node 3 will be down also
	// No one node should be able to gain quorum
	Environment			e;
	FaultFunctions::wait_for_arg_t	wait_arg;

	if (store_nodeids(arglen, argv))
		return (1);

	(void) wait_for_keys();

	// Add trigger to fail all QD's
	NodeTriggers::add(FAULTNUM_CMM_QUORUM_DEVICE_FAIL, NULL, 0,
	    TRIGGER_ALL_NODES);

	// Add trigger to stop the node from dumping core when aborting
	FaultFunctions::wait_for_arg_t  wait_arg2;
	wait_arg2.op = FaultFunctions::WAIT_FOR_DEAD;
	wait_arg2.nodeid = 0;
	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT,
	    FAULTNUM_CMM_STOP_ABORT, &wait_arg2, (uint32_t)sizeof (wait_arg2),
	    TRIGGER_ALL_NODES);

	// Kill root node
	// wait_arg.op = FaultFunctions::WAIT_FOR_REJOIN;
	wait_arg.op = FaultFunctions::WAIT_FOR_DEAD;
	wait_arg.nodeid = orb_conf::get_root_nodeid();
	os::printf("    Root node is: %d\n", wait_arg.nodeid);
	FaultFunctions::reboot(0, &wait_arg, (uint32_t)sizeof (wait_arg));

	// We shouldn't get this far because we should loose qourum
	ASSERT(false);

	return (0);
#else
	return (no_fault_injection());
#endif
}
