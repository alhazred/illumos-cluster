/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fault_quorum_1_b.cc	1.9	08/09/03 SMI"

//
// This file contains the on and off functions for the fault points for a
// specific fault scenario.
//
// Refer to the README.faults file for a complete description of all
// scenarios
//

#include	<sys/os.h>
#include	<orb/fault/fault_injection.h>

int
fault_quorum_1_b_on(void *argp, int arglen)
{
	// Argument should be a FaultFunctions::mc_sema_arg_t structure
	ASSERT(argp != NULL);
	ASSERT(arglen == sizeof (np_list_t));

	np_list_t	*np_list = (np_list_t *)argp;
	char		*path;
	nodeid_t	node;
	int		len;
	int		counter;

	// Add an invo trigger to make the client node wait for a reconfigure
	// to complete (by waiting on a semaphore
	FaultFunctions::invo_trigger_add(FaultFunctions::SCQ_WAIT,
	    FAULTNUM_CMM_CONTROL_RECONFIGURE,
	    NULL, 0);

	// Add a node trigger that will signal when the reconfig
	// is complete
	FaultFunctions::node_trigger_add(FaultFunctions::SIGNAL,
	    FAULTNUM_CMM_AUTOMATON_END_STATE,
	    NULL, 0, TRIGGER_THIS_NODE);

	// Add a node trigger that will fail the QD
	// (For each node in the list)
	for (counter = 0; counter < np_list->listlen; counter++) {
		node = np_list->node_list[counter];
		path = np_list->path_list[counter];
		if (path == NULL)
			len = 0;
		else
			len = (int)os::strlen(path) + 1;
		NodeTriggers::add(FAULTNUM_CMM_QUORUM_DEVICE_FAIL,
		    path, len, node);
	}

	os::warning("Armed fault: "
	    "FAULTNUM_CMM_CONTROL_RECONFIGURE");

	os::warning("Armed fault: "
	    "FAULTNUM_CMM_AUTOMATON_END_STATE");

	// Add a delay to the node specified (in CMM operation on QD)
	// (For each node in the list)
	for (counter = 0; counter < np_list->listlen; counter++) {
		node = np_list->node_list[counter];
		os::warning("Armed fault: "
		    "FAULTNUM_CMM_QUORUM_DEVICE_FAIL on node: %d", node);
	}

	return (0);
}

int
fault_quorum_1_b_off(void *argp, int arglen)
{
	// Argument should be a FaultFunctions::mc_sema_arg_t structure
	ASSERT(argp != NULL);
	ASSERT(arglen == sizeof (np_list_t));

	np_list_t	*np_list = (np_list_t *)argp;
	nodeid_t	node;
	int		counter;

	// Clear the invo triggers
	InvoTriggers::clear(FAULTNUM_CMM_CONTROL_RECONFIGURE);
	NodeTriggers::clear(FAULTNUM_CMM_AUTOMATON_END_STATE);

	// Clear the fault
	// (For each node in the list)
	for (counter = 0; counter < np_list->listlen; counter++) {
		node = np_list->node_list[counter];
		NodeTriggers::clear(FAULTNUM_CMM_QUORUM_DEVICE_FAIL, node);
	}

	return (0);
}
