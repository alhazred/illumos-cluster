/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ENTRY_RECONFIGURE_H
#define	_ENTRY_RECONFIGURE_H

#pragma ident	"@(#)entry_reconfigure.h	1.15	09/03/06 SMI"

#include <orbtest/cmm_fi/faults/entry_common.h>

int reconfigure_1_a(int, char *[]);
int reconfigure_1_b(int, char *[]);
int reconfigure_1_c(int, char *[]);
int reconfigure_1_j(int, char *[]);
int reconfigure_1_k(int, char *[]);
int reconfigure_1_r(int, char *[]);
int reconfigure_1_y(int, char *[]);

int reconfigure_2_r(int, char *[]);

int reconfigure_pre_sync_with_others(int, char *[]);
int reconfigure_after_sync_with_others(int, char *[]);
int reconfigure_pre_callbacks(int, char *[]);
int reconfigure_after_callbacks(int, char *[]);
int reconfigure_hang_pre_callbacks(int, char *[]);

int node_depart_hearsay(int, char *[]);
int node_join_hearsay(int, char *[]);
#endif	/* _ENTRY_RECONFIGURE_H */
