/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)entry_quorum_aux.cc	1.34	09/02/10 SMI"

#include	<sys/os.h>
#include	<h/quorum.h>

#include	<orbtest/cmm_fi/faults/entry_quorum_aux.h>
#include	<sys/quorum_int.h>
#include	<sys/strerror.h>
#include	<sys/nodeset.h>

#include	<nslib/ns.h>
#include	<quorum/common/quorum_util.h>

//
// Helper routine to get path to a QD give the index numbner of the QD
// as its defined in the quorum info file.  (For a give nodeid)
//
char *
get_qd_path(int index, nodeid_t node)
{
	quorum_table_t		*qt;
	char			*path = NULL;
	uint32_t		found = 0;
	uint32_t		qd_max;

	if (clconf_get_quorum_table(NULL /* current */, &qt) != 0)
		return (NULL);

	// Get number of QDs configured in quorum table
	qd_max = qt->quorum_devices.listlen;

	while ((index <= qd_max) && (found == 0)) {
		quorum_device_info_t	*qinfo;
		nodeset			nodes_with_paths;

		qinfo = &qt->quorum_devices.list[index - 1];
		nodes_with_paths.set(qinfo->nodes_with_configured_paths);
		if (nodes_with_paths.contains(node)) {
			// Fix for 4203711
			path = new char[os::strlen(qinfo->gdevname) + 1];
			ASSERT(path != NULL);
			(void) os::strcpy(path, qinfo->gdevname);
			found = 1;
		}

		// Not found yet. Increment index by 1 until qd_max
		index++;
	}

	clconf_release_quorum_table(qt);
	return (path);
}

//
// Helper to verify that the correct keys are in the QD
//
// The list of nodeid's is a list of the keys we expect to find in the QD
// NOTE that since we dont associate id's with QD's.  I cant do verification
// when we have multiple QD's.
int
verify_keys(int index, nodeid_list_t *node_list, bool print, bool foundkey)
{
	bool		use_pgre;
	uint64_t	sblkno;
	int		error;
	int		counter;

	if (print) {
		os::printf("    QD Key verification in progress...\n");
	}

	// Get the quorum table
	quorum_table_t	*qt;

	if (clconf_get_quorum_table(NULL /* current */, &qt) != 0)
		return (1);

	// Build the list of keys we're expecting to find from
	// the arguments passed in
	quorum::reservation_list_t	expected_key_list;
	expected_key_list.listsize = (unsigned int)node_list->listlen;
	expected_key_list.listlen = 0;
	expected_key_list.keylist.length(expected_key_list.listsize);

	// Find keys for each node
	nodeid_t	nodenum;
	for (counter = 0; counter < node_list->listlen; counter++) {
		for (int nc = 0; nc < ((int)qt->nodes.listlen); nc++) {
			nodenum = qt->nodes.list[nc].nodenum;
			if (nodenum == node_list->list[counter]) {
				bcopy(qt->nodes.list[nc].reservation_key.key,
				    expected_key_list.keylist[counter].key,
				    sizeof (quorum::registration_key_t));
				expected_key_list.listlen++;
				break;
			} // found what we're looking for
		} // qt->nodes.list
	} // node_list->list
	ASSERT(expected_key_list.listlen == ((unsigned int)node_list->listlen));

	// Find the path to the qd
	char			*path = NULL;
	quorum_device_info_t	*qinfo;
	nodeset			nodes_with_paths;

	ASSERT(index <= ((int)qt->quorum_devices.listlen));

	qinfo = &qt->quorum_devices.list[index - 1];
	nodes_with_paths.set(qinfo->nodes_with_configured_paths);
	if (nodes_with_paths.contains(orb_conf::node_number()))
		path = qinfo->gdevname;

	if (path == NULL) {
		// There is not path to this QD
		os::printf(" ** No path to QD: %d\n", index);
		clconf_release_quorum_table(qt);
		return (0);
	}

	// Get a reference to the device type registry.
	quorum::device_type_registry_var	type_registry_v;
	naming::naming_context_var		ctxp;
	Environment				e;
	ctxp = ns::local_nameserver();
	CORBA::Object_var   tr_v = ctxp->resolve("type_registry", e);
	if (e.exception()) {
		os::printf(" ** Could not resolve type registry\n");
		e.clear();
		return (1);
	}

	type_registry_v = quorum::device_type_registry::_narrow(tr_v);

	//
	// Get a reference to the quorum device type.
	//
	// If access_mode exists for the device, we use it;
	// else we use the type for the device
	//
	char *type_str = NULL;
	if (qinfo->access_mode != NULL) {
		type_str = qinfo->access_mode;
	} else {
		type_str = qinfo->type;
	}
	ASSERT(type_str != NULL);
	quorum::quorum_device_type_var	qd_v;
	qd_v = type_registry_v->get_quorum_device(type_str, e);
	if (e.exception()) {
		os::printf(" ** Error returned from get_quorum_device\n");
		e.clear();
		return (1);
	}

	// Open the QD device
	quorum::quorum_error_t	q_error = quorum::QD_SUCCESS;
	quorum::qd_property_seq_t	props;
	q_error = qd_v->quorum_open(path, false, props, e);
	if (e.exception()) {
		os::printf(" ** Exception returned from quorum open\n");
		e.clear();
		//
		// Call quorum_close() that will cleanup any complete
		// or partial actions done by quorum_open().
		// We ignore the return value, as we do not need to do anything
		// further after trying to close the quorum device object.
		//
		(void) qd_v->quorum_close(e);
		// quorum_close() does not raise any exceptions
		ASSERT(e.exception() == NULL);
		return (1);
	}
	if (q_error && (q_error != quorum::QD_EACCES)) {
		char *errormsg = strerror(EIO);
		if (errormsg == NULL) {
			errormsg = "Unidentified Error";
		}
		if (print) {
			os::printf("+++ FAIL: Error opening QD: %s "
			    "errno = %d (%s).\n", path, EIO, errormsg);
		}
		//
		// Call quorum_close() that will cleanup any complete
		// or partial actions done by quorum_open().
		// We ignore the return value, as we do not need to do anything
		// further after trying to close the quorum device object.
		//
		(void) qd_v->quorum_close(e);
		// quorum_close() does not raise any exceptions
		ASSERT(e.exception() == NULL);
		clconf_release_quorum_table(qt);
		return (1);
	}

	// Get the key list
	quorum::reservation_list_t	key_list;
	key_list.listsize = 16;
	key_list.listlen = 0;
	key_list.keylist.length(16);

	q_error = qd_v->quorum_read_keys(key_list, e);
	if (e.exception()) {
		os::printf("+++ FAIL: Exception returned from read_keys\n");
		e.clear();
		(void) qd_v->quorum_close(e);
		// quorum_close() does not raise any exceptions
		ASSERT(e.exception() == NULL);
		return (1);
	}
	if (q_error) {
		if (print) {
			os::printf("+++ FAIL: Error reading keys from QD: %s\n",
			    path);
		}
		q_error = qd_v->quorum_close(e);
		e.clear();
		clconf_release_quorum_table(qt);
		return (1);
	}
	q_error = qd_v->quorum_close(e);
	e.clear();

	// MUST initialize the boolean to false before looking for match keys
	bool	found = false;

	// Compare both key lists (they should be the same size!)
	// ASSERT(key_list.listlen == expected_key_list.listlen);
	for (counter = 0;
		counter < ((int)expected_key_list.listlen); counter++) {

		// If found, set the boolean to true.
		for (int kc = 0; kc < ((int)key_list.listlen); kc++) {
			if (match_idl_keys(&(key_list.keylist[kc]),
			    &(expected_key_list.keylist[counter]))) {
				found = true;
				break;
			}
		} // key_list.keylist

		// If not found, report keys not found.
		if (!found) {
			if (print) {
				os::printf("+++ Key (0x%llx) was not "
				    "found in QD\n",
				    expected_key_list.keylist[counter]);
			}

			// Return error ONLY if key is expected to be found
			if (foundkey) {
				os::printf("+++ FAIL: Key (0x%llx) was not "
				    "found in QD\n",
				    expected_key_list.keylist[counter]);
				clconf_release_quorum_table(qt);
				return (1);
			}
		}

		// Verify the key is not found if it is not expected
		if (!foundkey) {
			if (found) {
				if (print) {
					os::printf("+++ FAIL: Key (0x%llx) "
					    "was not expected to be found "
					    "in QD\n",
					    expected_key_list.keylist[counter]);
					clconf_release_quorum_table(qt);
					return (1);
				}
			}
		}

		found = false;
	} // expected_key_list.keylist

	if (print) {
		os::printf("--- PASS: Key Verification is complete\n");
	}

	clconf_release_quorum_table(qt);
	return (0);
}


//
// Helper to wait for all keys to appear on each QD
//
// This functions will go thorough each QD and make sura that the keys
// for every node that has a connection to this QD is on that QD
//
// CAUTION: may hang test if keys never appear
int
wait_for_keys()
{
	// Get the quorum table
	quorum_table_t	*qt;

	if (clconf_get_quorum_table(NULL /* current */, &qt) != 0)
		return (1);

	// Walk through each QD and verify keys
	for (int counter = 0;
		counter < ((int)qt->quorum_devices.listlen); counter++) {
		// Build a nodelist
		quorum_device_info_t	*qd_info;
		nodeid_list_t		node_list;
		int			node_count = 0;
		nodeset			nodes_with_paths;

		// Extact info on this QD
		qd_info = &(qt->quorum_devices.list[counter]);
		nodes_with_paths.set(qd_info->nodes_with_configured_paths);

		node_list.list = new nodeid_t[qt->nodes.listlen];

		// Go through each node that has a connection to this
		// QD and create a list of nodes to verify keys with
		for (nodeid_t nid = 1; nid <= qt->nodes.listlen; nid++) {
			if (nodes_with_paths.contains(nid)) {
				node_list.list[node_count] = nid;
				node_count++;
			}
		}
		node_list.listlen	= node_count;

		// Now verify keys (Spin on this)
		os::printf(" ** Verifying that keys exist on QD: %d "
		    "(Test may hang if keys dont appear)...", counter + 1);
		while (1) {
			if (verify_keys(counter + 1, &node_list, false) == 0)
				break;
			os::printf(".");
			os::usecsleep((os::usec_t)1 * 1000000);
		} // spinning
		os::printf(" done\n");

		// Delete structures
		node_list.listlen = 0;
		delete []node_list.list;
	}

	clconf_release_quorum_table(qt);
	return (0);
}
