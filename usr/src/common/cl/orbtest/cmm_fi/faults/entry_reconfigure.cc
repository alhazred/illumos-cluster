/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)entry_reconfigure.cc	1.20	09/03/06 SMI"

#include <sys/os.h>
#include <orb/invo/common.h>
#include <orb/infrastructure/orb_conf.h>
#include <orbtest/cmm_fi/cmm_fi_clnt.h>
#include <orb/fault/fault_injection.h>
#include <orbtest/cmm_fi/faults/entry_common.h>

#include <orbtest/cmm_fi/faults/fault_reconfigure_1_a.h>
#include <orbtest/cmm_fi/faults/fault_reconfigure_1_b.h>
#include <orbtest/cmm_fi/faults/fault_reconfigure_1_c.h>
#include <orbtest/cmm_fi/faults/fault_reconfigure_1_j.h>
#include <orbtest/cmm_fi/faults/fault_reconfigure_1_k.h>
#include <orbtest/cmm_fi/faults/fault_reconfigure_1_r.h>
#include <orbtest/cmm_fi/faults/fault_reconfigure_1_y.h>

#include <orbtest/cmm_fi/faults/fault_reconfigure_2_r.h>

#include <orbtest/cmm_fi/faults/fault_reconfigure_pre_sync_with_others.h>
#include <orbtest/cmm_fi/faults/fault_reconfigure_after_sync_with_others.h>
#include <orbtest/cmm_fi/faults/fault_reconfigure_pre_callbacks.h>
#include <orbtest/cmm_fi/faults/fault_reconfigure_after_callbacks.h>
#include <orbtest/cmm_fi/faults/fault_reconfigure_hang_pre_callbacks.h>

#include <orbtest/cmm_fi/faults/fault_node_depart_hearsay.h>
#include <orbtest/cmm_fi/faults/fault_node_join_hearsay.h>

// Forward Definitions

#if defined(_FAULT_INJECTION)
//
// Helper for non_step_fault_test_cases
//
int non_step_reconfigure(FAULT_FPTR(f_on), FAULT_FPTR(f_off));

//
// Helper for step_reconfigure fault test cases
//
int step_reconfigure(FAULT_FPTR(f_on), FAULT_FPTR(f_off), int32_t step);

//
// Helper for non_step_reconfigure sleep test cases
//
int non_step_reconfigure_sleep(FAULT_FPTR(f_on), FAULT_FPTR(f_off));

//
// Helper for step_reconfigure fault test cases
//
int step_reconfigure_sleep(FAULT_FPTR(f_on), FAULT_FPTR(f_off), int32_t step);

#else

int non_step_reconfigure(void *, void *);
int step_reconfigure(void *, void *, int32_t);
int non_step_reconfigure_sleep(void *, void *);
int step_reconfigure_sleep(void *, void *, int32_t);

#endif // _FAULT_INJECTION

//
// Fault will occur before sync with others (step routine)
//
#ifdef _FAULT_INJECTION
int
reconfigure_pre_sync_with_others(int argc, char *argv[])
{
	// Find out what the argument is.. should only be 1 argument
	// (the step number)
	int	step;

	if (argc < 2) {
		os::warning("Incorrect test arguments (reconfigure_pre_sync"
		    "_with_others expects 1 integer argument)\n");
		return (1);
	}

	// Call to put nodeids in a global variable
	if (store_nodeids(argc, argv))
		return (1);

	step = os::atoi(argv[1]);
	if (step == 0) {
		os::warning("Incorrect test arguments (1st arguments cannot be"
		    "0\n");
		return (1);
	}

	os::printf("*** CMM Reconfigure Pre-Sync-With-Others Step: %d\n", step);

	// In this test case we set step to be STEP - 1 of the desired
	// step we want to fault on, because the FAULT POINT is before the
	// step internal variable is updated
	return (step_reconfigure(
	    fault_reconfigure_pre_sync_with_others_on,
	    fault_reconfigure_pre_sync_with_others_off, step - 1));
}
#else
int
reconfigure_pre_sync_with_others(int, char *[])
{
	return (0);
}
#endif

//
// Fault will occur after sync with others (step routine)
//
#ifdef _FAULT_INJECTION
int
reconfigure_after_sync_with_others(int argc, char *argv[])
{
	// Find out what the argument is.. should only be 1 argument
	// (the step number)
	int	step;

	if (argc < 2) {
		os::warning("Incorrect test arguments (reconfigure_after_sync"
		    "_with_others expects 1 integer argument)\n");
		return (1);
	}

	// Call to put nodeids in a global variable
	if (store_nodeids(argc, argv))
		return (1);

	step = os::atoi(argv[1]);
	if (step == 0) {
		os::warning("Incorrect test arguments (1st arguments cannot be"
		    "0\n");
		return (1);
	}

	os::printf("*** CMM Reconfigure After-Sync-With-Others Step: %d\n",
	    step);

	return (step_reconfigure(
	    fault_reconfigure_after_sync_with_others_on,
	    fault_reconfigure_after_sync_with_others_off, step));
}
#else
int
reconfigure_after_sync_with_others(int, char *[])
{
	return (no_fault_injection());
}
#endif

//
// Fault will occur pre callbacks are done (step routine)
//
#ifdef _FAULT_INJECTION
int
reconfigure_pre_callbacks(int argc, char *argv[])
{
	// Find out what the argument is.. should only be 1 argument
	// (the step number)
	int	step;

	if (argc < 2) {
		os::warning("Incorrect test arguments (reconfigure_pre_"
		    "callbacks expects 1 integer argument)\n");
		return (1);
	}

	// Call to put nodeids in a global variable
	if (store_nodeids(argc, argv))
		return (1);

	step = os::atoi(argv[1]);
	if (step == 0) {
		os::warning("Incorrect test arguments (1st arguments cannot be"
		    "0\n");
		return (1);
	}

	os::printf("*** CMM Reconfigure Before-Callbacks Step: %d\n", step);

	return (step_reconfigure(
	    fault_reconfigure_pre_callbacks_on,
	    fault_reconfigure_pre_callbacks_off, step));
}
#else
int
reconfigure_pre_callbacks(int, char *[])
{
	return (no_fault_injection());
}
#endif

//
// Fault will occur after callbacks are done (step routine)
//
#ifdef _FAULT_INJECTION
int
reconfigure_after_callbacks(int argc, char *argv[])
{
	// Find out what the argument is.. should only be 1 argument
	// (the step number)
	int	step;

	if (argc < 2) {
		os::warning("Incorrect test arguments (reconfigure_pre_"
		    "callbacks expects 1 integer argument)\n");
		return (1);
	}

	// Call to put nodeids in a global variable
	if (store_nodeids(argc, argv))
		return (1);

	step = os::atoi(argv[1]);
	if (step == 0) {
		os::warning("Incorrect test arguments (1st arguments cannot be"
		    "0\n");
		return (1);
	}

	os::printf("*** CMM Reconfigure After-Callbacks Step: %d\n", step);

	return (step_reconfigure(
	    fault_reconfigure_after_callbacks_on,
	    fault_reconfigure_after_callbacks_off, step));
}
#else
int
reconfigure_after_callbacks(int, char *[])
{
	return (no_fault_injection());
}
#endif

//
// Hang callback thread prior to executing callbacks
//
#ifdef _FAULT_INJECTION
int
reconfigure_hang_pre_callbacks(int argc, char *argv[])
{
	// Find out what the argument is.. should only be 1 argument
	// (the step number)
	int	step;

	if (argc < 2) {
		os::warning("Incorrect test arguments (reconfigure_pre_"
		    "callbacks expects 1 integer argument)\n");
		return (1);
	}

	// Call to put nodeids in a global variable
	if (store_nodeids(argc, argv))
		return (1);

	step = os::atoi(argv[1]);
	if (step == 0) {
		os::warning("Incorrect test arguments (1st arguments cannot be"
		    "0\n");
		return (1);
	}

	os::printf("*** CMM Reconfigure Hang-Before-Callbacks Step: %d\n",
	    step);

	return (step_reconfigure_sleep(
	    fault_reconfigure_hang_pre_callbacks_on,
	    fault_reconfigure_hang_pre_callbacks_off, step));
}
#else
int
reconfigure_hang_pre_callbacks(int, char *[])
{
	return (no_fault_injection());
}
#endif


//
// Reconfigure 1-A Test case
//
int
reconfigure_1_a(int argc, char *argv[])
{
	os::printf("*** CMM Reconfigure Scenario 1 Fault A\n");

#ifdef _FAULT_INJECTION
	Environment			e;
	int				rslt;

	// Call to put nodeids in a global variable
	if (store_nodeids(argc, argv))
		return (1);

	rslt = cmm_reconfigure(
		fault_reconfigure_1_a_on,
		fault_reconfigure_1_a_off,
		NULL,
		0,
		e);
	if (e.exception()) {
		e.exception()->print_exception("reconfigure_1_a");
		return (1);
	}

	return (rslt);
#else
	argc, argv;				// shut compiler warning
	return (no_fault_injection());
#endif
}


//
// Reconfigure 1-B Test case
//
int
reconfigure_1_b(int argc, char *argv[])
{
	os::printf("*** CMM Reconfigure Scenario 1 Fault B\n");

#ifdef _FAULT_INJECTION
	// Call to put nodeids in a global variable
	if (store_nodeids(argc, argv))
		return (1);

	return (non_step_reconfigure(
	    fault_reconfigure_1_b_on, fault_reconfigure_1_b_off));
#else
	argc, argv;				// shut compiler warning
	return (no_fault_injection());
#endif
}


//
// Reconfigure 1-C Test case
//
int
reconfigure_1_c(int argc, char *argv[])
{
	os::printf("*** CMM Reconfigure Scenario 1 Fault C\n");

#ifdef _FAULT_INJECTION
	// Call to put nodeids in a global variable
	if (store_nodeids(argc, argv))
		return (1);

	return (non_step_reconfigure(
	    fault_reconfigure_1_c_on, fault_reconfigure_1_c_off));
#else
	argc, argv;				// shut compiler warning
	return (no_fault_injection());
#endif
}

//
// Reconfigure 1-J Test case
//
int
reconfigure_1_j(int argc, char *argv[])
{
	os::printf("*** CMM Reconfigure Scenario 1 Fault J\n");

#ifdef _FAULT_INJECTION
	// Call to put nodeids in a global variable
	if (store_nodeids(argc, argv))
		return (1);

	return (non_step_reconfigure(
	    fault_reconfigure_1_j_on, fault_reconfigure_1_j_off));
#else
	argc, argv;				// shut compiler warning
	return (no_fault_injection());
#endif
}


//
// Reconfigure 1-K Test case
//
int
reconfigure_1_k(int argc, char *argv[])
{
	os::printf("*** CMM Reconfigure Scenario 1 Fault K\n");

#ifdef _FAULT_INJECTION
	// Call to put nodeids in a global variable
	if (store_nodeids(argc, argv))
		return (1);

	return (non_step_reconfigure(
	    fault_reconfigure_1_k_on, fault_reconfigure_1_k_off));
#else
	argc, argv;				// shut compiler warning
	return (no_fault_injection());
#endif
}


//
// Reconfigure 1-R Test case
//
int
reconfigure_1_r(int argc, char *argv[])
{
	os::printf("*** CMM Reconfigure Scenario 1 Fault R\n");

#ifdef _FAULT_INJECTION
	// Call to put nodeids in a global variable
	if (store_nodeids(argc, argv))
		return (1);

	return (non_step_reconfigure(
	    fault_reconfigure_1_r_on, fault_reconfigure_1_r_off));
#else
	argc, argv;				// shut compiler warning
	return (no_fault_injection());
#endif
}


//
// Reconfigure 1-Y Test case
//
int
reconfigure_1_y(int argc, char *argv[])
{
	os::printf("*** CMM Reconfigure Scenario 1 Fault Y\n");

#ifdef _FAULT_INJECTION
	// Call to put nodeids in a global variable
	if (store_nodeids(argc, argv))
		return (1);

	return (non_step_reconfigure(
	    fault_reconfigure_1_y_on, fault_reconfigure_1_y_off));
#else
	argc, argv;				// shut compiler warning
	return (no_fault_injection());
#endif
}


//
// Reconfigure 2-R Test case
//
int
reconfigure_2_r(int argc, char *argv[])
{
	os::printf("*** CMM Reconfigure Scenario 2 Fault R\n");

#ifdef _FAULT_INJECTION
	// Call to put nodeids in a global variable
	if (store_nodeids(argc, argv))
		return (1);

	return (non_step_reconfigure_sleep(
	    fault_reconfigure_2_r_on, fault_reconfigure_2_r_off));
#else
	argc, argv;				// shut compiler warning
	return (no_fault_injection());
#endif
}

//
// "Node depart hearsay" fault scenario :
// Delay node_is_unreachable() action for node C on a node A
// until remote node B tells node A that node C has gone down
//
#ifdef _FAULT_INJECTION
int
node_depart_hearsay(int argc, char *argv[])
{
	// Call to put nodeids in a global variable
	if (store_nodeids(argc, argv)) {
		return (1);
	}

	os::printf("*** CMM Ignore Node Status Hearsay "
	    "(Scenario : node goes down)\n");

	//
	// We take the first non-root node as nodeB and second non-root node
	// as nodeC. nodeB will tell us about nodeC going down.
	// We reboot second non-root node (nodeC).
	// This is in accordance with what is mentioned in the fault.data
	// file for this test case.
	//
	nodeid_t nodeB = non_root_node(1);	// Node B
	nodeid_t nodeC = non_root_node(2);	// Node C to be rebooted

	//
	// Set up the arguments to pass to the on and off fault functions.
	// The arguments contain the node ids for node B and node C
	// that are employed in the test case.
	//
	FaultFunctions::delay_for_hearsay_arg_t args;
	args.nodeB = nodeB;
	args.nodeC = nodeC;

	os::printf("*** nodeB %d, nodeC %d\n", nodeB, nodeC);

	// Do the setup of fault triggers
	if (fault_node_depart_hearsay_on((void *)&args,
	    (int)sizeof (FaultFunctions::delay_for_hearsay_arg_t)) != 0) {
		os::printf("    WARNING: Arming faults failed\n");
	} else {
		os::printf("    All faults armed.\n");
	}

	// Reboot target node and wait for it to rejoin
	FaultFunctions::wait_for_arg_t wait_arg;
	wait_arg.op = FaultFunctions::WAIT_FOR_REJOIN;
	wait_arg.nodeid = nodeC;		// Node C to be rebooted
	FaultFunctions::reboot(
		0,				// Fault number to be ignored
		&wait_arg,			// Fault arguments
		(uint32_t)sizeof (wait_arg));	// Size of fault arguments

	//
	// If we reach here, it means node C
	// has rejoined membership after reboot.
	//

	// Disarm faults
	if (fault_node_depart_hearsay_off((void *)&args,
	    (int)sizeof (FaultFunctions::delay_for_hearsay_arg_t)) != 0) {
		os::printf("    WARNING: Disarming faults failed\n");
	} else {
		os::printf("    All faults disarmed.\n");
	}

	os::printf("--- PASS: Rejoin of Node %d completed\n", nodeC);
	return (0);
}
#else
int
node_depart_hearsay(int, char *[])
{
	return (no_fault_injection());
}
#endif

//
// "Node join hearsay" fault scenario :
// Delay node_is_reachable() action for node C on a node A,
// until remote node B tells node A that node C has come up
// with a new incarnation
//
#ifdef _FAULT_INJECTION
int
node_join_hearsay(int argc, char *argv[])
{
	// Call to put nodeids in a global variable
	if (store_nodeids(argc, argv)) {
		return (1);
	}

	os::printf("*** CMM Ignore Node Status Hearsay "
	    "(Scenario : node comes up)\n");

	//
	// We take the first non-root node as nodeB and second non-root node
	// as nodeC. nodeB will tell us about nodeC booting up with a new incn.
	// We reboot second non-root node (nodeC).
	// This is in accordance with what is mentioned in the fault.data
	// file for this test case.
	//
	nodeid_t nodeB = non_root_node(1);	// Node B
	nodeid_t nodeC = non_root_node(2);	// Node C to be rebooted

	//
	// Set up the arguments to pass to the on and off fault functions.
	// The arguments contain the node ids for node B and node C
	// that are employed in the test case.
	//
	FaultFunctions::delay_for_hearsay_arg_t args;
	args.nodeB = nodeB;
	args.nodeC = nodeC;

	os::printf("*** nodeB %d, nodeC %d\n", nodeB, nodeC);

	//
	// First setup wait for rejoin.
	// wait for rejoin semantics necessitate that the node,
	// which we should wait for, must be part of the membership
	// when we setup the wait.
	// NodeC is part of membership now, so set up the wait
	// for rejoin of NodeC.
	//
	FaultFunctions::wait_for_arg_t wait_arg_rejoin;
	wait_arg_rejoin.op = FaultFunctions::WAIT_FOR_REJOIN;
	wait_arg_rejoin.nodeid = nodeC;
	FaultFunctions::do_setup_wait(&wait_arg_rejoin);

	// Reboot target node and wait for it to be marked dead
	FaultFunctions::wait_for_arg_t wait_arg_dead;
	wait_arg_dead.op = FaultFunctions::WAIT_FOR_DEAD;
	wait_arg_dead.nodeid = nodeC;		// Node C to be rebooted
	FaultFunctions::reboot(
		0,				// Fault number to be ignored
		&wait_arg_dead,			// Fault arguments
		(uint32_t)sizeof (wait_arg_dead)); // Size of fault arguments

	//
	// If we reach here, it means node C has left membership.
	// It would be in the process of booting up.
	// Arm faults before it boots up, and then wait for nodeC to rejoin.
	// XXX There is a race here between arming faults and nodeC booting up.
	// XXX We do not want to arm the faults before nodeC goes down,
	// XXX and we want to arm faults just before nodeC boots up;
	// XXX hence the race. However, the race would not be the usual case,
	// XXX since reconfiguration for node C leaving membership
	// XXX would usually complete some time before node C's new incn starts
	// XXX coming up, and so we have a period where we can arm faults.
	//

	// Do the setup of fault triggers
	if (fault_node_join_hearsay_on((void *)&args,
	    (int)sizeof (FaultFunctions::delay_for_hearsay_arg_t)) != 0) {
		os::printf("    WARNING: Arming faults failed\n");
	} else {
		os::printf("    All faults armed.\n");
	}

	// Wait for node C to rejoin membership
	FaultFunctions::do_wait(&wait_arg_rejoin);

	//
	// If we reach here, it means node C
	// has rejoined membership after reboot.
	//

	// Disarm faults
	if (fault_node_join_hearsay_off((void *)&args,
	    (int)sizeof (FaultFunctions::delay_for_hearsay_arg_t)) != 0) {
		os::printf("    WARNING: Disarming faults failed\n");
	} else {
		os::printf("    All faults disarmed.\n");
	}

	os::printf("--- PASS: Rejoin of Node %d completed\n", nodeC);
	return (0);
}
#else
int
node_join_hearsay(int, char *[])
{
	return (no_fault_injection());
}
#endif

//
// Helper for non_step_reconfigure fault test cases
//
#ifdef _FAULT_INJECTION
int
non_step_reconfigure(FAULT_FPTR(f_on), FAULT_FPTR(f_off))
#else
int
non_step_reconfigure(void *, void *)
#endif // _FAULT_INJECTION
{
#ifdef _FAULT_INJECTION
	Environment			e;
	int				rslt;
	FaultFunctions::wait_for_arg_t	wait_arg;

	// Setup the wait arguments for the reboot
	wait_arg.op = FaultFunctions::WAIT_FOR_UNKNOWN;
	wait_arg.nodeid = non_root_node(1);

	rslt = cmm_reconfigure(
		f_on,
		f_off,
		&wait_arg,
		(int)sizeof (wait_arg),
		e);
	if (e.exception()) {
		e.exception()->print_exception("cmm_reconfigure");
		return (1);
	}

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// Helper for step_reconfigure fault test cases
//
#ifdef _FAULT_INJECTION
int
step_reconfigure(FAULT_FPTR(f_on), FAULT_FPTR(f_off), int32_t step)
#else
int
step_reconfigure(void *, void *, int32_t)
#endif
{
#ifdef _FAULT_INJECTION
	Environment				e;
	FaultFunctions::cmm_reboot_arg_t	cmm_reboot_arg;
	int					rslt;

	// Find the 1st non-root node and the set wait type
	cmm_reboot_arg.op = FaultFunctions::WAIT_FOR_UNKNOWN;
	cmm_reboot_arg.nodeid = non_root_node(1);

	// Set the step to fault on
	cmm_reboot_arg.step = step;

	rslt = cmm_reconfigure(
		f_on,
		f_off,
		&cmm_reboot_arg,
		(int)sizeof (cmm_reboot_arg),
		e);
	if (e.exception()) {
		e.exception()->print_exception("cmm_reconfigure");
		return (1);
	}

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// Helper for non_step_reconfigure sleep test cases
//
#ifdef _FAULT_INJECTION
int
non_step_reconfigure_sleep(FAULT_FPTR(f_on), FAULT_FPTR(f_off))
#else
int
non_step_reconfigure_sleep(void *, void *)
#endif
{
#ifdef _FAULT_INJECTION
	Environment			e;
	int				rslt;

	rslt = cmm_reconfigure(
		f_on,
		f_off,
		NULL,
		0,
		e);
	if (e.exception()) {
		e.exception()->print_exception("cmm_reconfigure");
		return (1);
	}

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// Helper for step_reconfigure fault test cases
//
#ifdef _FAULT_INJECTION
int
step_reconfigure_sleep(FAULT_FPTR(f_on), FAULT_FPTR(f_off), int32_t step)
#else
int
step_reconfigure_sleep(void *, void *, int32_t)
#endif
{
#ifdef _FAULT_INJECTION
	Environment				e;
	int					rslt;

	rslt = cmm_reconfigure(
		f_on,
		f_off,
		&step,
		(int)sizeof (step),
		e);
	if (e.exception()) {
		e.exception()->print_exception("cmm_reconfigure");
		return (1);
	}

	return (rslt);
#else
	return (no_fault_injection());
#endif
}
