/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ENTRY_QUORUM_H
#define	_ENTRY_QUORUM_H

#pragma ident	"@(#)entry_quorum.h	1.14	08/05/20 SMI"

#include <orbtest/cmm_fi/faults/entry_common.h>

int quorum_1_a(int, char *[]);
int quorum_1_b(int, char *[]);
int quorum_1_c(int, char *[]);
int quorum_1_d(int, char *[]);
int quorum_1_e(int, char *[]);
int quorum_1_f(int, char *[]);
int quorum_1_g(int, char *[]);
int quorum_1_h(int, char *[]);
int quorum_1_i(int, char *[]);
int quorum_1_j(int, char *[]);

int quorum_2_a(int, char *[]);
int quorum_2_b(int, char *[]);
int quorum_2_c(int, char *[]);
int quorum_2_d(int, char *[]);
int quorum_2_e(int, char *[]);
int quorum_2_f(int, char *[]);
int quorum_2_g(int, char *[]);
int quorum_2_h(int, char *[]);

int quorum_3_a(int, char *[]);
int quorum_3_b(int, char *[]);
int quorum_3_c(int, char *[]);
int quorum_3_d(int, char *[]);
int quorum_3_e(int, char *[]);
int quorum_3_f(int, char *[]);
int quorum_3_g(int, char *[]);
int quorum_3_h(int, char *[]);

int quorum_4_a(int, char *[]);
int quorum_4_b(int, char *[]);
int quorum_4_c(int, char *[]);
int quorum_4_d(int, char *[]);
int quorum_4_e(int, char *[]);
int quorum_4_f(int, char *[]);
int quorum_4_g(int, char *[]);
int quorum_4_h(int, char *[]);
int quorum_4_i(int, char *[]);
int quorum_4_j(int, char *[]);
int quorum_4_k(int, char *[]);
int quorum_4_l(int, char *[]);
int quorum_4_m(int, char *[]);

#endif	/* _ENTRY_QUORUM_H */
