/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/* CSTYLED */
#pragma ident	"@(#)fault_reconfigure_after_sync_with_others.cc	1.12	08/09/03 SMI"

//
// This file contains the on and off functions for the fault points for a
// specific fault scenario.
//
// Refer to the README.faults file for a complete description of all
// scenarios
//

#include	<sys/os.h>
#include	<orb/fault/fault_injection.h>
#include	<orbtest/cmm_fi/faults/entry_common.h>

int
fault_reconfigure_after_sync_with_others_on(void *argp, int arglen)
{
	// Arguments should be step_fault_arg_t
	ASSERT(argp != NULL);
	ASSERT(arglen == sizeof (FaultFunctions::cmm_reboot_arg_t));

	// Add an invo trigger to make the client node wait for a reconfigure
	// to complete (by waiting on a semaphore
	FaultFunctions::invo_trigger_add(FaultFunctions::SCQ_WAIT,
	    FAULTNUM_CMM_CONTROL_RECONFIGURE, NULL, 0);

	// Add a node trigger that will post to that mc_sema when the reconfig
	// is complete
	FaultFunctions::node_trigger_add(FaultFunctions::SIGNAL,
	    FAULTNUM_CMM_AUTOMATON_END_STATE, NULL, 0, TRIGGER_THIS_NODE);

	// Now add the trigger for this test case
	FaultFunctions::node_trigger_add(FaultFunctions::CMM_REBOOT,
	    FAULTNUM_CMM_AUTOMATON_STEP_STATE_2, argp, arglen,
	    TRIGGER_THIS_NODE);

	os::warning("Armed fault: "
	    "FAULTNUM_CMM_CONTROL_RECONFIGURE");

	os::warning("Armed fault: "
	    "FAULTNUM_CMM_AUTOMATON_END_STATE");

	os::warning("Armed fault: "
	    "FAULTNUM_CMM_AUTOMATON_STEP_STATE_2");

	return (0);
}

int
fault_reconfigure_after_sync_with_others_off(void *, int)
{
	// Clear the invo triggers
	InvoTriggers::clear(FAULTNUM_CMM_CONTROL_RECONFIGURE);
	NodeTriggers::clear(FAULTNUM_CMM_AUTOMATON_END_STATE);
	NodeTriggers::clear(FAULTNUM_CMM_AUTOMATON_STEP_STATE_2);

	return (0);
}
