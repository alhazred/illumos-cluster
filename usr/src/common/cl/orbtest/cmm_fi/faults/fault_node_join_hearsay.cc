/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fault_node_join_hearsay.cc	1.1	09/03/06 SMI"

//
// This file contains the on and off functions for the fault points for a
// specific fault scenario.
//
// Refer to the README.faults file for a complete description of all
// scenarios
//

#include	<sys/os.h>
#include	<h/mc_sema.h>
#include	<sys/mc_sema_impl.h>
#include	<orb/fault/fault_injection.h>

int
fault_node_join_hearsay_on(void *argp, int arglen)
{
	// Argument should be a FaultFunctions::delay_for_hearsay_arg_t
	ASSERT(argp != NULL);
	ASSERT(arglen == sizeof (FaultFunctions::delay_for_hearsay_arg_t));

	FaultFunctions::delay_for_hearsay_arg_t *hearsay_args =
	    (FaultFunctions::delay_for_hearsay_arg_t *)argp;

	//
	// Faults and flow of events
	// --------------------------
	// Scenario : Node C is rebooted.
	// Node A receives a node_is_reachable() for node C, but
	// the node_is_reachable() blocks until node B tells node A
	// that node C is coming up with a new incarnation.
	// Once node A hears it from node B,
	// node_is_reachable() does a sleep for some time
	// and then does its processing to signal the automaton
	// to do its reconfiguration. The automaton also needs to ensure
	// that it does not reconfigure until it is signalled by
	// node_is_reachable().
	// The fault setup code that calls this function to trigger
	// these faults waits until the reconfiguration is complete.
	//

	//
	// Set up the mc_sema that node_is_reachable() on local node (node A)
	// will use to wait for node B's message to node A telling
	// node C is coming up with a new incarnation.
	//
	Environment			e;
	FaultFunctions::mc_sema_arg_t	sema_arg;
	mc_sema::sema_var		sema_ref_v;

	sema_ref_v = (new mc_sema_impl)->get_objref();
	ASSERT(!CORBA::is_nil(sema_ref_v));

	sema_ref_v->init(0, e);
	if (e.exception()) {
		e.exception()->print_exception(
		    "fault_node_join_hearsay_on: initializing mc_sema\n");
		return (1);
	}

	//
	// Set up the arguments for this mc_sema.
	// We will use sema_p and sema_v operations that will not use
	// this nodeid member of the arg structure.
	//
	sema_arg.nodeid = 0;	// Ignored anyway
	sema_arg.id = sema_ref_v->get_cookie(e);
	if (e.exception()) {
		e.exception()->print_exception(
		    "fault_node_join_hearsay_on: getting mc_sema cookie\n");
	}
	ASSERT(sema_arg.id != 0);
	//
	// Set up NO_WAIT; we only wait as long as the mc_sema operation
	// makes us wait.
	//
	sema_arg.wait.op = FaultFunctions::NO_WAIT;

	//
	// Set up the mc_sema_with_data_arg_t structure to be used
	// for the fault operation
	//
	uint32_t total_size = 0;
	FaultFunctions::mc_sema_with_data_arg_t *sema_with_data_argp =
	    FaultFunctions::mc_sema_with_data_arg_alloc(
	    total_size,	// will be modified to store the total length
	    sema_arg,	// the mc_sema_arg structure
	    true,	// trigger once-only semantics
	    // data length to store in the struct
	    (uint32_t)sizeof (FaultFunctions::delay_for_hearsay_arg_t),
	    // actual data being stored in the struct
	    hearsay_args);

	//
	// Add a trigger to do a SEMA_P (block) in the node_is_reachable()
	// on local node (node A) until node B tells us that node C
	// has come up with a new incarnation
	//
	FaultFunctions::node_trigger_add(
	    FaultFunctions::SEMA_P_WITH_SHARED_DATA,	// fault operation
	    FAULTNUM_CMM_NODE_JOIN_HEARSAY_1,		// fault number
	    sema_with_data_argp,			// fault args
	    total_size,					// size of fault args
	    TRIGGER_THIS_NODE);				// trigger on local node

	//
	// Add a trigger to do a SEMA_V operation when
	// node B tells local node A that node C is up with a new incn
	//
	FaultFunctions::node_trigger_add(
	    FaultFunctions::SEMA_V_WITH_SHARED_DATA,	// fault operation
	    FAULTNUM_CMM_NODE_JOIN_HEARSAY_2,		// fault number
	    sema_with_data_argp,			// fault args
	    total_size,					// size of fault args
	    TRIGGER_THIS_NODE);				// trigger on local node

	//
	// Reset all sets in the event order sets in FaultFunctions;
	// specifying -1 resets all sets to 0.
	//
	FaultFunctions::reset_event_order_set(-1);

	//
	// Set up the fault argument for event ordering for :
	// - event 1 : node_is_reachable() on local node A for node C
	// finishes its work, after node B has told node A that
	// node C is up with a new incarnation
	// - event 2 : automaton on local node A does its reconfiguration
	// (we expect local node A not to go beyond begin state,
	// so we consider finishing begin state as event 2)
	//
	// The event ordering fault test will ensure that
	// event 1 happens before event 2.
	//
	// We will use the first array element of
	// FaultFunctions::event_order_sets to do our test.
	//

	// Fault argument for event 1
	FaultFunctions::event_order_arg_t ev1_order_arg;
	ev1_order_arg.index_into_event_order_sets = 0;	// First array element
	ev1_order_arg.order_of_event = 1;	// Numerical order of event 1

	// Fault argument for event 2
	FaultFunctions::event_order_arg_t ev2_order_arg;
	ev2_order_arg.index_into_event_order_sets = 0;	// First array element
	ev2_order_arg.order_of_event = 2;	// Numerical order of event 2

	// Add a trigger for event 1
	FaultFunctions::node_trigger_add(
	    FaultFunctions::CHECK_EVENT_ORDER,	// fault operation
	    FAULTNUM_CMM_NODE_JOIN_HEARSAY_3,	// fault number
	    &ev1_order_arg,			// fault args
	    // size of fault args
	    (uint32_t)sizeof (FaultFunctions::event_order_arg_t),
	    TRIGGER_THIS_NODE);			// trigger on local node

	// Add a trigger for event 2
	FaultFunctions::node_trigger_add(
	    FaultFunctions::CHECK_EVENT_ORDER,		// fault operation
	    FAULTNUM_CMM_AUTOMATON_BEGIN_STATE_2,	// fault number
	    &ev2_order_arg,				// fault args
	    // size of fault args
	    (uint32_t)sizeof (FaultFunctions::event_order_arg_t),
	    TRIGGER_THIS_NODE);				// trigger on local node

	os::warning("Armed fault : FAULTNUM_CMM_NODE_JOIN_HEARSAY_1");
	os::warning("Armed fault : FAULTNUM_CMM_NODE_JOIN_HEARSAY_2");
	os::warning("Armed fault : FAULTNUM_CMM_NODE_JOIN_HEARSAY_3");
	os::warning("Armed fault : FAULTNUM_CMM_AUTOMATON_BEGIN_STATE_2");

	return (0);
}

int
fault_node_join_hearsay_off(void *, int)
{
	// Clear the node triggers
	NodeTriggers::clear(FAULTNUM_CMM_NODE_JOIN_HEARSAY_1);
	NodeTriggers::clear(FAULTNUM_CMM_NODE_JOIN_HEARSAY_2);
	NodeTriggers::clear(FAULTNUM_CMM_NODE_JOIN_HEARSAY_3);
	NodeTriggers::clear(FAULTNUM_CMM_AUTOMATON_BEGIN_STATE_2);

	return (0);
}
