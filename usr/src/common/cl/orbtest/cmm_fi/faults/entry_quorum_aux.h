/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_ENTRY_QUORUM_AUX_H
#define	_ENTRY_QUORUM_AUX_H

#pragma ident	"@(#)entry_quorum_aux.h	1.14	08/05/20 SMI"

#include <orb/infrastructure/orb_conf.h>
#include <sys/clconf_int.h>

typedef struct {
	int		listlen;
	nodeid_t	*list;
} nodeid_list_t;

typedef struct {
	int		listlen;
	nodeid_t	*node_list;
	char		**path_list;
} np_list_t;

//
// Helper to get QD paths
//
char *get_qd_path(int index, nodeid_t node);

//
// Helper to verify keys in QD
//
int verify_keys(int index, nodeid_list_t *node_list, bool print = true,
    bool foundkey = true);

//
// Helper to poll and wait for all the keys to appear on the QD
//
int wait_for_keys();

#endif /* _ENTRY_QUORUM_AUX_H */
