/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)cmm_fi_clnt.cc	1.11	08/05/20 SMI"

#include 	<sys/os.h>
#include	<h/naming.h>
#include	<h/cmm.h>
#include	<orb/invo/common.h>
#include	<nslib/ns.h>
#include	<cmm/cmm_ns.h>
#include	<orbtest/cmm_fi/cmm_fi_clnt.h>

//
// Forward Defintions
//
// Helper Functions

//
// CMM Reconfig client
//
// This client will request a reconfigure from the CMM object
//
int
cmm_reconfigure(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void *argp,
	int arglen,
	Environment &e)
{
	cmm::control_var	cmm_control;

	os::printf("*** CMM Reconfigure\n");

	// Get the CMM control
	cmm_control = cmm_ns::get_control();
	if (e.exception()) {
		e.exception()->print_exception("cmm_reconfigure:");
		return (1);
	}

	ASSERT(!CORBA::is_nil(cmm_control));

	// Do the setup
	if (on_fp != NULL) {
		if (on_fp(argp, arglen) != 0)
			os::printf("    WARNING: Arming faults failed\n");
		else
			os::printf("    All faults armed.\n");
	} else {
		os::printf("    No Faults armed.\n");
	}

	//
	// CMM Reconfigure
	//
	cmm_control->reconfigure(e);

	// Disarm faults
	if (off_fp != NULL) {
		if (off_fp(argp, arglen) != 0)
			os::printf("    WARNING: Disarming faults failed\n");
		else
			os::printf("    All faults disarmed.\n");
	}

	if (e.exception()) {
		e.exception()->print_exception("cmm::control::reconfigure()");
		os::printf("+++ FAIL: Reconfigure returned exception\n");
		return (1);
	}

	// Compare to the expected value
	os::printf("--- PASS: Reconfigure completed without exception\n");
	return (0);
}
