/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CMM_FI_CLNT_H
#define	_CMM_FI_CLNT_H

#pragma ident	"@(#)cmm_fi_clnt.h	1.9	08/05/20 SMI"

#include	<orb/invo/common.h>

//
// Functions that act as clients for each test case
// They must be passed a pointer to a function that will activate the fault
// points for this test. And a pointer to a functions to de-activate the faults
// after the test has been run.  (These may be NULL).
// These functions are called immediate before and after the invocation that
// is being tested is called.
//

// Macro for the function to turn on fault injection
#define	FAULT_FPTR(func)	int (*func) (void *, int)

// CMM Reconfiguration
extern int cmm_reconfigure(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void *,
	int,
	Environment &);

#endif	/* _CMM_FI_CLNT_H */
