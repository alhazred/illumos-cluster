/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_st_ff.c	1.18	08/05/20 SMI"

#include <sys/st_failfast.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>

int
main(int argc, char**argv)
{
	int the_in;
	pid_t thepid = 0;

	if (argc < 2) {
		(void) printf("Usage: %s failfast_name\n", argv[0]);
		exit(1);
	}

	for (;;) {
		(void) printf("1) arm 2) disarm 3) fork process "
		    "4) kill forked process or 5) quit?\n> ");
		(void) scanf("%d", &the_in);
		switch (the_in) {
		case 1:
			(void) printf("Arm returned %d\n", st_ff_arm(argv[1]));
			break;
		case 2:
			(void) printf("Disarm returned %d\n", st_ff_disarm());
			break;
		case 3:
			if (thepid != 0) {
				(void) printf("Process %d already forked\n",
				    thepid);
			} else if ((thepid = fork()) != 0) {
				(void) printf("Forked process %d\n", thepid);
			} else {
				(void) execl("/usr/bin/sleep", "/usr/bin/sleep",
				    "100000");
			}
			break;
		case 4:
			if (thepid == 0) {
				(void) printf("No process forked\n");
			} else if (!kill(thepid, SIGKILL)) {
				(void) printf("Process %d killed\n", thepid);
				thepid = 0;
			} else {
				(void) printf("Process %d not killed\n",
				    thepid);
			}
			break;
		case 5:
			exit(0);
			break;
		default:
			break;
		}
	}
}
