/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ORBTEST_REF_STRESS_IMPL_COMMON_H
#define	_ORBTEST_REF_STRESS_IMPL_COMMON_H

#pragma ident	"@(#)impl_common.h	1.24	08/05/20 SMI"

#if !defined(_KERNEL)
#if defined(_KERNEL_ORB)
#define	_UNODE
#else
#define	_USER
#endif	// _KERNEL_ORB
#endif	// !_KERNEL

#include <sys/types.h>

#if defined(_UNODE) || defined(_USER)
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#endif

#include <sys/os.h>
#include <orb/invo/common.h>
#include <orb/object/adapter.h>
#include <nslib/ns.h>

#include <h/cmm.h>
#include <h/ref_stress.h>

static const uint32_t	init_seqmax = 100;	// initial sequence max size

// Convenience typedefs.
typedef ref_stress::idnum		idnum;

typedef ref_stress::testobj_ptr		testobj_ptr;
typedef ref_stress::testobj_var		testobj_var;

typedef ref_stress::server_ptr		server_ptr;
typedef ref_stress::server_var		server_var;

typedef ref_stress::client_ptr		client_ptr;
typedef ref_stress::client_var		client_var;

typedef ref_stress::test_param		test_param;
typedef ref_stress::test_param_seq	test_param_seq;

typedef ref_stress::rsrc_balancer	rsrc_balancer;
typedef ref_stress::rsrc_balancer_ptr	rsrc_balancer_ptr;
typedef ref_stress::rsrc_balancer_var	rsrc_balancer_var;

typedef ref_stress::Errmsg		Errmsg;		// exception class

#if defined(_UNODE)
int	unode_init(void);
#endif

//
// Functions to generate string names for name server registration
// and message prefixes, given some ID numbers.
//
inline char *
testobj_name(char *bufp, idnum server_id, idnum testobj_id)
{
#if defined(_KERNEL)
	os::sprintf(bufp, "testobj(%u,%u) (kernel)", server_id, testobj_id);
#elif defined(_UNODE)
	os::sprintf(bufp, "testobj(%u,%u) (unode)", server_id, testobj_id);
#else
	os::sprintf(bufp, "testobj(%u,%u) (user)", server_id, testobj_id);
#endif
	return (bufp);
}

inline char *
server_name(char *bufp, idnum server_id)
{
#if defined(_KERNEL)
	os::sprintf(bufp, "server(%u) (kernel)", server_id);
#elif defined(_UNODE)
	os::sprintf(bufp, "server(%u) (unode)", server_id);
#else
	os::sprintf(bufp, "server(%u) (user)", server_id);
#endif
	return (bufp);
}

inline char *
server_nsname(char *bufp, idnum server_id)
{
	os::sprintf(bufp, "ref_stress_server(%u)", server_id);
	return (bufp);
}

inline char *
thread_name(char *bufp, idnum client_id, idnum thread_id)
{
#if defined(_KERNEL)
	os::sprintf(bufp, "thread(%u,%u) (kernel)", client_id, thread_id);
#elif defined(_UNODE)
	os::sprintf(bufp, "thread(%u,%u) (unode)", client_id, thread_id);
#else
	os::sprintf(bufp, "thread(%u,%u) (user)", client_id, thread_id);
#endif
	return (bufp);
}

inline char *
client_name(char *bufp, idnum client_id)
{
#if defined(_KERNEL)
	os::sprintf(bufp, "client(%u) (kernel)", client_id);
#elif defined(_UNODE)
	os::sprintf(bufp, "client(%u) (unode)", client_id);
#else
	os::sprintf(bufp, "client(%u) (user)", client_id);
#endif
	return (bufp);
}

inline char *
client_nsname(char *bufp, idnum client_id)
{
	os::sprintf(bufp, "ref_stress_client(%u)", client_id);
	return (bufp);
}

inline char *
rsrc_balancer_name(char *bufp, nodeid_t nid)
{
	os::sprintf(bufp, "resource balancer interface (node %u)", nid);
	return (bufp);
}

inline char *
rsrc_balancer_nsname(char *bufp, nodeid_t nid)
{
	os::sprintf(bufp, "ref_stress_rsrc_balancer(%u)", nid);
	return (bufp);
}

//
// Convenience routines to narrow an exception.
// Returns: pointer to the exception, NULL if there no such exception.
//
inline CORBA::SystemException *
system_excp(Environment &e)
{
	return (CORBA::SystemException::_exnarrow(e.exception()));
}

inline Errmsg *
errmsg_excp(Environment &e)
{
	return (Errmsg::_exnarrow(e.exception()));
}

//lint -esym(1512, impl_common)
// The current design does not allow one to safely perform
// a deletion on a pointer to this base class if the pointer is really
// pointing to a derived class. Memory leaks would occur.
//
// Common (non-exported) methods for implementation classes.
//
class impl_common {
public:
	//
	// Suspend calling thread until _unreferenced() is called.
	// Returns: true if _unreferenced() has been called.
	//	    false if timeout arg (in seconds) is nonzero and it
	//	    expires before _unreferenced() is called.
	//
	bool	wait_until_unreferenced(uint32_t timeout = 0);

	//
	// Has _unreferenced() been called?
	// Caller must have this object locked prior to calling this routine.
	//
	bool	unref_called();

	// Get name registered with ns.
	const char	*nsname() { return _nsname; }

	// Get name.
	const char	*name() { return _name; }

	void	lock()		{ _mutex.lock(); }
	bool	lock_held()	{ return (_mutex.lock_held() != 0); }
	void	unlock()	{ _mutex.unlock(); }

protected:
	// Protected so this class can be used only through derivation.
	impl_common();
	~impl_common();

	//
	// Register object with the name server with the given name.
	// Returns: true if successful, false otherwise.
	//
	bool	register_obj(CORBA::Object_ptr objref, const char *strname);

	//
	// Unregister object from the name server.
	// Returns: true if successful, false otherwise.
	//
	bool	unregister_obj();

	//
	// Mark whether _unreferenced() has been called.
	// Caller must have this object locked prior to calling this routine.
	//
	void	unref_called(bool b);

	// Save name registered with ns.
	void	nsname(const char *str);

	// Set name.
	void	name(const char *str);

private:
	bool		_unref_called;
	bool		_obj_registered;
	char		*_nsname;
	char		*_name;
	os::mutex_t	_mutex;
	os::condvar_t	_cv;
};

//
// A common function used by client threads and testobj.  Performs the
// actual reference test.
//
// Side Effects: sets Environment exception and returns false when a
//		 failure/error occurs.  Returns true otherwise.
//
bool
do_test(
	const test_param_seq	&test_seq,
	const char		*msg_prefix,
	uint32_t		verbose_level,
	Environment		&e,
	bool			&reconfig_allowed);

#endif	/* _ORBTEST_REF_STRESS_IMPL_COMMON_H */
