/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _DRIVER_SUPPORT_H
#define	_DRIVER_SUPPORT_H

#pragma ident	"@(#)driver_support.h	1.20	08/05/20 SMI"

#include <h/ref_stress.h>
#include <orbtest/ref_stress/impl_common.h>
#include <orbtest/ref_stress/client_impl.h>
#include <orbtest/ref_stress/server_impl.h>

enum entity_t { CLIENT, SERVER, RSRC_BALANCER };

//
// The script's stdout and stderr (in unode mode, the node's stdout/stderr
// could be different from the script's).
//
extern FILE	*script_stdout;
extern FILE	*script_stderr;

class server_testobjs;			// fwd decl.
class client_threads;			// fwd decl.

//
// Some invocation parameter manipulators (for the >> operators)
//
class iter_manip {
public:
	iter_manip(uint32_t n) : _n(n) {}
	operator uint32_t() { return (_n); }	// to convert back to uint32_t

private:
	// Do not allow default constructor
	iter_manip();

	uint32_t	_n;
};

inline iter_manip
setiter(uint32_t num) { return (iter_manip(num)); }

struct testobj_descr {
	testobj_descr(idnum serv_id, idnum tobj_id) :
			server_id(serv_id), testobj_id(tobj_id) {}

	idnum	server_id;
	idnum	testobj_id;

private:
	// Do not allow default constructor
	testobj_descr();
};

//
// rsrc_bal
//	Represents the resource balancer interface (ref_stress::rsrc_balancer)
//	on a node.  Note, there can only be one interface per node.
//
class rsrc_bal {
public:
	rsrc_bal();
	~rsrc_bal();

	bool			init(idnum server_id, nodeid_t nid);
	rsrc_balancer_ptr	objref();
	nodeid_t		nodeid();

private:
	idnum			_server_id;	// corresponding server id
	nodeid_t		_nodeid;	// where it lives
	rsrc_balancer_ptr	_ref;
};

//
// server_testobjs
//
class server_info;				// fwd. decl

class server_testobjs {
	friend bool test(client_threads &cli, server_testobjs &srv);

public:
	server_testobjs();
	~server_testobjs();

	testobj_descr	operator()(idnum server_id, idnum testobj_id);
	uint32_t	num_servers() { return (_servers.length()); }

	//
	// Access to a node's (represented by the given server id)
	// resource balancer.
	// Return true if successful, false otherwise.
	// NOTE: There can only be one resource balancer per node.  If the
	//	 given server id >= number of nodes alive, that id will refer
	//	 to the same node as another one, i.e. modulo the number of
	//	 live nodes.
	//
	bool	get_threads_min(idnum server_id,
			resource_defs::resource_pool_t pool,
			int &minimum);

	bool	set_threads_min(idnum server_id,
			resource_defs::resource_pool_t pool,
			int minimum);

	bool	get_threads_total(idnum server_id,
			resource_defs::resource_pool_t pool,
			int &total);

	bool	get_policy(idnum server_id,
			resource_defs::resource_pool_t pool,
			int &low, int &moderate, int &high, int &increment);

	bool	set_policy(idnum server_id,
			resource_defs::resource_pool_t pool,
			int low, int moderate, int high, int increment,
			int number_nodes);

private:
	bool	_start();			// start all servers
	bool	_test();			// verify testobjs unreferenced
	bool	_end();				// end all servers

	_FixedSeq_<server_info*>	_servers;
};

class server_info {
	friend class server_testobjs;

public:
	uint32_t	num_testobjs()		{ return (_num_testobjs); }

	void		num_testobjs(uint32_t n) { _num_testobjs = n; }

private:
	server_info();
	~server_info();

	server_ptr	_ref;
	nodeid_t	_nodeid;
	uint32_t	_num_testobjs;
};

//
// client_threads
//
class client_info;				// fwd. decl
class thread_info;				// fwd. decl

class client_threads {
	friend bool test(client_threads &cli, server_testobjs &srv);

public:
	client_threads();
	~client_threads();

	thread_info&	operator()(idnum client_id, idnum thread_id);
	uint32_t	num_clients() { return (_clients.length()); }

	// If set to true, clients will be run one at a time, instead of in
	// parallel.  Useful for testing flow control.  By default, clients
	// are run in parallel.
	void		serial(bool s);

private:
	bool		_start();		// start all clients
	bool		_test();		// all clients perform tests
	bool		_wait_for_thread(thread_t);
	bool		_end();			// end all clients
	static void	*_test_thr(void *arg);

	bool		_serial;

	/* CSTYLED */
	_FixedSeq_<client_info*>	_clients;
};

class client_info {
	friend class client_threads;

public:
	uint32_t	num_threads() { return (_threads.length()); }

private:
	client_info();
	~client_info();

	client_ptr	_ref;
	nodeid_t	_nodeid;

	/* CSTYLED */
	_FixedSeq_<thread_info*>	_threads;
};

class thread_info {
	friend class client_threads;
	friend class client_info;

public:
	thread_info&	operator>>(testobj_descr &tobj_descr);
	thread_info&	operator>>(iter_manip &iter_m);
	uint32_t	num_tests() { return (_test_seq.length()); }

private:
	thread_info();
	~thread_info();

	test_param_seq	_test_seq;
	uint32_t	_default_iter;		// default # of times to invoke
};

// Perform setup, testing and cleanup, given references to server_testobjs
// and client_threads objects.
bool	test(client_threads &cli, server_testobjs &srv);

// After initialization completes, this returns the number of live nodes.
uint32_t	number_nodes_alive(void);

// Return a client/server ID modulo the number of nodes alive.
int	node_modulo(int number);

// Given a client/server id, returns a node id that can be used to run
// that client or server.  The basic algorithm is round-robin, i.e. modulo
// the number of live nodes.
nodeid_t	csid2nodeid(idnum id);

// Convenient routines to print to both the driver's and the script's
// stdout/stderr (if they're different).
// (In unode, the script's and driver's stdout/stderr could be different.)
int	stdout_printf(const char *fmt, ...);
int	stderr_printf(const char *fmt, ...);

// Convert a string argument into its equivalent integer.
// Returns the converted number in the parameter 'retnum'.
// Returns true if successful, false otherwise.
bool	convert_arg(char *arg, int &retnum);

//
// Since there is no easy way to start a process/module on a remote node (yet),
// we'll use an external shell script to do it.  The set of routines below
// handle the script.
//

// Initialize driver.
// Returns: true if successful, false otherwise.
bool	driver_init(void);

// Tell external script to start an entity on the node with node ID "nodeid"
// and with client/server id "id".
// Returns: true if successful, false otherwise.
bool	start(nodeid_t nodeid, entity_t entity, idnum id);

// Tell external script to end the entity with id "id".
// Returns: true if successful, false otherwise.
bool	end(nodeid_t nodeid, entity_t entity, idnum id);

// Tell external script testing is done.
void	done(void);

#endif	/* _DRIVER_SUPPORT_H */
