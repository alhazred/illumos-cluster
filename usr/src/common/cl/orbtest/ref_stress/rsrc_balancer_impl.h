/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RSRC_BALANCER_IMPL_H
#define	_RSRC_BALANCER_IMPL_H

#pragma ident	"@(#)rsrc_balancer_impl.h	1.18	08/05/20 SMI"

#include <orb/object/adapter.h>

#include <h/ref_stress.h>
#include <orbtest/ref_stress/impl_common.h>

//
// Resource balancer interface implementation.
//
class rsrc_balancer_impl : public McServerof<ref_stress::rsrc_balancer>,
	public impl_common
{
public:
	rsrc_balancer_impl();
	~rsrc_balancer_impl();		//lint !e1509

	bool	init();
	void	_unreferenced(unref_t);

	//
	// Interface Operations
	//
	int32_t	get_threads_min(int32_t pool, Environment &);

	void	set_threads_min(int32_t pool,
			int32_t minimum,
			Environment &);

	int32_t	get_threads_total(int32_t pool, Environment &);

	void	get_policy(int32_t pool,
			int32_t &low, int32_t &moderate,
			int32_t &high, int32_t &increment,
			Environment &);

	bool	set_policy(int32_t pool,
			int32_t low, int32_t moderate,
			int32_t high, int32_t increment,
			int32_t number_nodes, Environment &);

	void	set_verbose(uint32_t level, Environment &);

	void	done(Environment &);

private:
	uint32_t	_verbose_level;
};

#endif	/* _RSRC_BALANCER_IMPL_H */
