/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rsrc_balancer.cc	1.10	08/05/20 SMI"

#include <orbtest/ref_stress/rsrc_balancer_impl.h>

// ---------------------------------------------------------------------------
// KERNEL resource balancer interface
// ---------------------------------------------------------------------------
#if defined(_KERNEL)

#include <sys/systm.h>
#include <sys/modctl.h>
#include <sys/errno.h>


extern "C" {
	void _cplpl_init();
	void _cplpl_fini();
}

char _depends_on[] = "misc/cl_runtime misc/cl_load misc/cl_orb "
	"misc/cl_haci misc/cl_quorum misc/cl_comm";

extern struct mod_ops mod_miscops;

static struct modlmisc modlmisc = {
	&mod_miscops, "ref_stress rsrc_balancer",
};

static struct modlinkage modlinkage = {
	MODREV_1, {&modlmisc, NULL}
};

static rsrc_balancer_impl	*rsrc_balp = NULL;

int
_init(void)
{
	int		error;

	if (error = mod_install(&modlinkage)) {
		return (error);
	}
	_cplpl_init();				// C++ initialization

	// Create the resource balancer interface implementation.
	rsrc_balp =  new rsrc_balancer_impl;
	if (! rsrc_balp->init()) {
		delete rsrc_balp; rsrc_balp = NULL;
		_cplpl_fini();
		mod_remove(&modlinkage);
		return (EIO);
	}

	return (0);
}

int
_fini(void)
{
	// Return EBUSY if modunloaded before we get an unreferenced() call
	rsrc_balp->lock();
	if (! rsrc_balp->unref_called()) {
		// os::printf("%s still referenced\n", rsrc_balp->name());
		rsrc_balp->unlock();
		return (EBUSY);
	}
	rsrc_balp->unlock();

	delete rsrc_balp; rsrc_balp = NULL;

	_cplpl_fini();
	return (mod_remove(&modlinkage));
}

int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}

#endif // _KERNEL


// ---------------------------------------------------------------------------
// USER resource balancer interface (not allowed)
// ---------------------------------------------------------------------------
#if defined(_USER)
#error "Resource balancer interface not allowed in user space"
#endif // _USER


// ---------------------------------------------------------------------------
// UNODE resource balancer interface
// ---------------------------------------------------------------------------
#if defined(_UNODE)

#include <stdio.h>


int
unode_rsrc_balancer(int, char *[])
{
	rsrc_balancer_impl	*rsrc_balp;

	rsrc_balp = new rsrc_balancer_impl;
	if (! rsrc_balp->init()) {
		return (1);
	}

	rsrc_balp->wait_until_unreferenced();
	delete rsrc_balp;

	return (0);
}

#endif // _UNODE
