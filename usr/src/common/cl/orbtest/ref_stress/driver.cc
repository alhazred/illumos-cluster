/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)driver.cc	1.34	08/05/20 SMI"

#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

#include <sys/os.h>
#include <orb/invo/common.h>
#include <orb/flow/resource_balancer.h>
#include <nslib/ns.h>

#include <orbtest/ref_stress/driver_support.h>

//
// Default test parameters.
//
const int test3_default_cli = 20;	// # of client threads per node
const int test3_default_ser = 20;	// # of server objects per node
const int test3_default_iter = 1;	// # of iterations per invo

// Test 4 - default parameters
//
const int	test4_light_low		= 10;
const int	test4_light_moderate	= 20;
const int	test4_light_high	= 60;
const int	test4_light_thr		= 100;
const int	test4_light_iter	= 30;

const int	test4_heavy_low		= 10;
const int	test4_heavy_moderate	= 80;
const int	test4_heavy_high	= 550;
const int	test4_heavy_thr		= 600;
const int	test4_heavy_iter	= 80;

const int	test4_num_thr_floor	= 20;

//
// List available tests.
//
static void
list_tests()
{
	fprintf(stderr, "\n"
"TEST 1: Non-scaled test for local invocations only.\n"
"Parameters: None\n");

	fprintf(stderr, "\n"
"TEST 2: Non-scaled test for local and remote invocations.\n"
"Parameters: None\n");

	fprintf(stderr, "\n"
"TEST 3: Scaled test (scaled to the number of live nodes).\n"
"Parameters: [threads [objects [iters]]]\n"
"   threads - Number of client threads per node (default: %d)\n"
"   objects - Number of server objects per node (default: %d)\n"
"   iters   - Number of iterations per invocation (default: %d)\n",
		test3_default_cli, test3_default_ser, test3_default_iter);

	fprintf(stderr, "\n"
"TEST 4: Flow-control test.\n"
"Parameters: [light | heavy | <conditions>]\n");
	fprintf(stderr,
"   light - use conditions appropriate for light load test.\n"
"        This is the default case\n"
"   heavy - use conditions appropriate for heavy load test.\n"
"   <conditions> - specify actual condition parameters.\n"
"        The parameters are specified in the following order.\n"
"        Missing trailing parameters take on default values.\n");
	fprintf(stderr,
"   threads - Number of client threads per node (default: %d)\n"
"   iters   - Number of iterations per invocation (default: %d)\n",
		test4_heavy_thr, test4_heavy_iter);
	fprintf(stderr,
"   test_nodes - Number of nodes to use in the test\n"
"              Flow control policy values\n"
"   thread_low\n"
"   thread_moderate\n"
"   thread_high\n"
"              Default for the last 4 values is the existing system value\n");

	fprintf(stderr, "\n"
"Test 5: Unref Threadpool Test.\n"
"Parameters: None.\n");

	fprintf(stderr, "\n"
"By default (no -t option is specified) tests 1,2,3,4 and 5 are performed.\n");
}


#if !defined(UNODE_LIST_TESTS)


// ---------------------------------------------------------------------------
// TEST 1: Non-scaled test for local invocations only.
// Parameters: None
// ---------------------------------------------------------------------------
bool
test1(int = 1, char *[] = NULL)
{
	client_threads	t;
	server_testobjs	o;

	stdout_printf("-- TEST 1: non-scaled test: local invocations "
		"only\n");

	t(0, 0)	>> setiter(1) >> o(0, 1)
		>> setiter(2) >> o(0, 2)
		>> setiter(3) >> o(0, 3);

	t(0, 1)	>> setiter(1) >> o(0, 3)
		>> setiter(2) >> o(0, 2)
		>> setiter(3) >> o(0, 1);

	t(0, 2)	>> setiter(1) >> o(0, 1)
		>> setiter(2) >> o(0, 1)
		>> setiter(3) >> o(0, 1);

	return (test(t, o));
}


// ---------------------------------------------------------------------------
// TEST 2: Non-scaled test combining local and remote invocations.
// Parameters: None
// ---------------------------------------------------------------------------
bool
test2(int = 1, char *[] = NULL)
{
	client_threads	t;
	server_testobjs	o;

	stdout_printf("-- TEST 2: non-scaled test: local and remote "
		"invocations\n");

	// First client
	t(0, 0)	>> setiter(4) >> o(0, 1)
		>> setiter(4) >> o(0, 2)
		>> setiter(4) >> o(1, 0)
		>> setiter(4) >> o(1, 1)
		>> setiter(3) >> o(2, 0)
		>> setiter(3) >> o(0, 0);

	t(0, 1)	>> setiter(6) >> o(1, 3)
		>> setiter(6) >> o(1, 0)
		>> setiter(6) >> o(1, 1)
		>> setiter(4) >> o(0, 1);

	t(0, 2)	>> setiter(5) >> o(0, 3)
		>> setiter(5) >> o(0, 2)
		>> setiter(5) >> o(1, 0)
		>> setiter(4) >> o(1, 1)
		>> setiter(3) >> o(2, 0);

	// Second client
	t(1, 0)	>> setiter(4) >> o(0, 0)
		>> setiter(4) >> o(0, 2)
		>> setiter(4) >> o(1, 0)
		>> setiter(4) >> o(1, 1)
		>> setiter(3) >> o(2, 0)
		>> setiter(3) >> o(0, 0);

	t(1, 1)	>> setiter(6) >> o(1, 3)
		>> setiter(6) >> o(2, 0)
		>> setiter(6) >> o(1, 1)
		>> setiter(4) >> o(0, 1);

	return (test(t, o));
}


// ---------------------------------------------------------------------------
// TEST 3: Scaled test.
//
//	Places the specified number of clients on each node.
//	Each client invokes the specified number of servers in a chain
//	the specified number of times. Each invocation in the chain
//	involves a different server and a different object id.
//
// Parameters: [num_threads [num_objects [num_iters]]]
// ---------------------------------------------------------------------------
bool
test3(int argc = 1, char *argv[] = NULL)
{
	// Test parameters.
	int	num_cli = test3_default_cli;
	int	num_ser = test3_default_ser;
	int	num_iter = test3_default_iter;

	client_threads	cli;
	server_testobjs	ser;

	// Get 1st argument, if any.
	if (argc > 1) {
		if (! convert_arg(argv[1], num_cli)) {
			stderr_printf("ERROR: test 3: invalid number of "
				"client threads: %s\n", argv[1]);
			return (false);
		}
	}

	// Get 2nd argument, if any.
	if (argc > 2) {
		if (! convert_arg(argv[2], num_ser)) {
			stderr_printf("ERROR: test 3: invalid number of "
				"server objects : %s\n", argv[2]);
			return (false);
		}
	}

	// Get 3rd argument, if any.
	if (argc > 3) {
		if (! convert_arg(argv[3], num_iter)) {
			stderr_printf("ERROR: test 3: invalid number of "
				"iterations : %s\n", argv[3]);
			return (false);
		}
	}


	stdout_printf("-- TEST 3: scaled test:\n");
	stdout_printf("           %d client threads/node, "
		"%d server objects/node, %d iters\n",
		num_cli, num_ser, num_iter);


	// Per node loop
	for (int id = 0; id < number_nodes_alive(); id++) {

		// Per client thread loop
		for (int thr_id = 0; thr_id < num_cli; thr_id++) {

			// Each server will be on a different node
			// supporting a different object
			for (int obj_id = 0, ser_id = id + 1;
			    obj_id < num_ser;
			    obj_id++, ser_id++) {

				cli(node_modulo(id), thr_id) >>
				    setiter(num_iter) >>
				    ser(node_modulo(ser_id), obj_id);
			}
		}
	}

	return (test(cli, ser));
}


// ---------------------------------------------------------------------------
// set_test4_conditions
// Use command line arguments to set test conditions
//
bool
set_test4_conditions(int argc, char *argv[],
    int &num_thr, int &num_iter, int &test_nodes,
    int &new_low, int &new_moderate, int &new_high)
{
	if (argc == 1 || (argc == 2 && (strcmp(argv[1], "light") == 0))) {
		//
		// The user can execute the light test
		// either by specifying no arguments
		// or explicitly specifying the light test.
		//
		num_thr		= test4_light_thr;
		num_iter	= test4_light_iter;
		test_nodes	= -1;	// Use actual nodes in cluster
		new_low		= test4_light_low;
		new_moderate	= test4_light_moderate;
		new_high	= test4_light_high;
		return (true);
	}
	if (argc == 2 && strcmp(argv[1], "heavy") == 0) {
		//
		// Use the default settings for a heavy test
		// that exercises various flow control cases.
		//
		num_thr		= test4_heavy_thr;
		num_iter	= test4_heavy_iter;
		test_nodes	= -1;	// Use actual nodes in cluster
		new_low		= test4_heavy_low;
		new_moderate	= test4_heavy_moderate;
		new_high	= test4_heavy_high;
		return (true);
	}
	if (argc > 7) {
		stderr_printf("ERROR: invalid number of arguments. argc %d\n",
		    argc);
		return (false);
	}

	// Initialize to defaults
	num_thr		= test4_heavy_thr;
	num_iter	= test4_heavy_iter;
	test_nodes	= -1;
	new_low		= -1;
	new_moderate	= -1;
	new_high	= -1;

	if (argc < 2) {
		return (true);
	}
	if (!convert_arg(argv[1], num_thr)) {
		stderr_printf("ERROR: test 4: invalid number of client threads:"
		    " %s\n", argv[1]);
		return (false);
	}
	//
	// Ensure a minimum number of client threads
	//
	if (num_thr < test4_num_thr_floor) {
		stderr_printf("ERROR: test 4: min client threads is %d\n",
			test4_num_thr_floor);
		return (false);
	}

	if (argc < 3) {
		return (true);
	}
	if (!convert_arg(argv[2], num_iter) ||
	    num_iter <= 0) {
		stderr_printf("ERROR: test 4: invalid number of "
		    "iterations : %s\n", argv[2]);
		return (false);
	}

	if (argc < 4) {
		return (true);
	}
	if (!convert_arg(argv[3], test_nodes) ||
	    test_nodes == 0 || test_nodes == 1) {
		stderr_printf("ERROR: test 4: invalid number test nodes %s\n",
		    argv[6]);
		return (false);
	}
	if (test_nodes == 2) {
		stderr_printf("WARNING: test 4: should have min 3 nodes\n");
	}

	if (argc < 5) {
		return (true);
	}
	if (!convert_arg(argv[4], new_low)) {
		stderr_printf("ERROR: test 4: invalid thread_low value: %s\n",
		    argv[4]);
		return (false);
	}

	if (argc < 6) {
		return (true);
	}
	if (!convert_arg(argv[5], new_moderate)) {
		stderr_printf("ERROR: test 4: invalid thread_moderate value:"
		    " %s\n", argv[5]);
		return (false);
	}

	if (argc < 7) {
		return (true);
	}
	if (!convert_arg(argv[6], new_high)) {
		stderr_printf("ERROR: test 4: invalid thread_high value: %s\n",
		    argv[6]);
		return (false);
	}

	return (true);
}


// ---------------------------------------------------------------------------
// execute_complex_test - The initial conditions of tests 4 and 5 differ,
// but the execution is almost the same. This function performs the
// common portions of the test.
// ---------------------------------------------------------------------------
bool
execute_complex_test(int num_nodes_test,
    int new_low,
    int new_moderate,
    int new_high,
    idnum server_id,
    client_threads &cli, server_testobjs &ser)
{
	// the pool to use
	const resource_defs::resource_pool_t pool = resource_defs::DEFAULT_POOL;

	int	new_increment;
	int	total_threads;
	int	minimum_threads;
	int	old_low;
	int	old_moderate;
	int	old_high;
	int	old_increment;
	bool	result;

	int	num_nodes_cluster = number_nodes_alive();

	stdout_printf("   Number of nodes: in cluster %d used by test %d\n",
	    num_nodes_cluster, num_nodes_test);

	//
	// Determine current value for minimum server threads
	//
	if (! ser.get_threads_min(server_id, pool, minimum_threads)) {
		return (false);
	}

	//
	// Now throw away unused pre-allocated server threads.
	// The server will retain an increment number of threads
	// when we specify 1 thread. We want to force server to
	// create and destroy server threads.
	//
	if (! ser.set_threads_min(server_id, pool, 1)) {
		return (false);
	}

	//
	// Get current flow control parameters on the server node.
	//

	if (! ser.get_threads_total(server_id, pool, total_threads)) {
		return (false);
	}

	if (! ser.get_policy(server_id, pool, old_low, old_moderate, old_high,
	    old_increment)) {
		return (false);
	}
	//
	// A negative value for the policy value means use
	// the existing value.
	//
	if (new_low < 0) {
		new_low = old_low;
	}
	if (new_moderate < 0) {
		new_moderate = old_moderate;
	}
	if (new_high < 0) {
		new_high = old_high;
	}

	stdout_printf("   Current flow control parameters of node %d:\n",
		csid2nodeid(server_id));
	stdout_printf("\tTotal number of threads: %d\n", total_threads);
	stdout_printf("\tLow: %d, moderate: %d, high: %d, increment: %d\n",
		old_low, old_moderate, old_high, old_increment);

	if (new_high < total_threads) {
		// Make sure the new high doesn't exceed current # of threads.
		new_high = total_threads;
	}
	if (new_low < old_increment * reallocation_inc_multiple) {
		new_low = old_increment * reallocation_inc_multiple;
	}
	if (new_moderate < new_low + old_increment) {
		// Make sure the new moderate doesn't go below the new low
		new_moderate = new_low + old_increment;
	}
	if (new_high < new_moderate + old_increment) {
		// Make sure the new high doesn't go below the new moderate
		new_high = new_moderate + old_increment;
	}

	// XXX Currently we can't change the increment value.
	new_increment = old_increment;

	//
	// Set the new policy.
	//
	stdout_printf("   Setting new flow control parameters on node %d:\n",
		csid2nodeid(server_id));
	stdout_printf("\tLow: %d, moderate: %d, high: %d, increment: %d\n",
		new_low, new_moderate, new_high, new_increment);

	if (! ser.set_policy(server_id, pool, new_low, new_moderate, new_high,
	    new_increment, num_nodes_test)) {
		return (false);
	}

	//
	// Run the test.
	//
	result = test(cli, ser);

	//
	// Restore the old policy.
	//
	stdout_printf("   Restoring flow control parameters on node %d\n",
		csid2nodeid(server_id));

	if (! ser.set_policy(server_id, pool, old_low, old_moderate, old_high,
	    old_increment, num_nodes_cluster)) {
		return (false);
	}

	//
	// Now restore the original setting for the minimum number
	// of server threads.
	//
	if (! ser.set_threads_min(server_id, pool, minimum_threads)) {
		return (false);
	}

	return (result);
}


// ---------------------------------------------------------------------------
// TEST 4: Flow-control stress test.
//
//	One client on each node running one or more threads.  Each thread
//	makes an invocation to an object on a remote node several times.
//	The clients are run one at a time, thus causing the server to
//	allocate threads from one client node to another.
//
//	The test should exercise the condition where the client asks for
//	more thread than the server can provide.
//
//	This test is useful for testing a flow control scenario where a server
//	must allocate threads from one client node to another.
//
//	The system does not instantly create huge numbers of server threads.
//	Therefore we run many iterations in order to maintain a heavy load.
//	Eventually, the flow control software will reach its limit for
//	server threads. And this test wants to reach that level.
//
// NOTE: This test should run on a cluster of 3 or more nodes.
// ---------------------------------------------------------------------------
bool
test4(int argc = 1, char *argv[] = NULL)
{
	// Test parameters.
	int	num_thr;
	int	num_iter;
	int	num_nodes_test;
	int	new_low;
	int	new_moderate;
	int	new_high;

	// Where the server is
	const idnum	server_id = 0;
	int		num_nodes_cluster;

	client_threads	cli;
	server_testobjs	ser;

	//
	// Use command line arguments to set test condition
	//
	if (!set_test4_conditions(argc, argv,
	    num_thr, num_iter, num_nodes_test,
	    new_low, new_moderate, new_high)) {
		// Invalid test conditions specified
		return (false);
	}

	stdout_printf("-- TEST 4: flow-control test:\n");
	stdout_printf("           %d client threads/node, %d iters. "
		"Clients run one at a time\n", num_thr, num_iter);

	// Get number of alive nodes.
	num_nodes_cluster = number_nodes_alive();
	if (num_nodes_test < 0) {
		num_nodes_test = num_nodes_cluster;
	}

	//
	// Setup scenario.
	// Use server on one node and client threads on other nodes.
	//
	for (int node = 1; node < num_nodes_test; ++node) {
		for (int thread = 0; thread < num_thr; ++thread) {

			cli(node, thread) >>
			    setiter(num_iter) >>
			    ser(server_id, 0);
		}
	}
	cli.serial(true);			// clients run one at a time

	return (execute_complex_test(num_nodes_test,
	    new_low, new_moderate, new_high, server_id, cli, ser));
}


// ---------------------------------------------------------------------------
// TEST 5: Unref Threadpool test.
//
//	This test exercises the software that transfers software between
//	the unref threadpool and the general invocation server threadpool.
//
//	The test starts a large number of invocations to a large number of
//	test objects with the intent of using up the available server threads.
//
//	The invocations complete and drop the references to the objects.
//	This results in a lot of objects becoming unreferenced. A sleep
//	in the unreference method makes the unreference method take a long
//	time. The unref queue backs up. The system then transfers worker
//	threads from the general invocation server threadpool to
//	the unref_threadpool.
//
//	After the unreferences complete, the system sleeps for a little while
//	to exercise the code that returns the borrowed and now idle
//	unref worker threads.
//
// Parameters: None
// ---------------------------------------------------------------------------
bool
test5()
{
	// Test parameters.
	int	num_thr = 500;
	int	num_iter = 1;
	int	num_nodes_test = 1;
	int	new_low = 10;
	int	new_moderate = 20;
	int	new_high = 60;

	// Where the server is
	const idnum	server_id = 0;

	client_threads	cli;
	server_testobjs	ser;

	stdout_printf("-- TEST 5: Unref threadpool test:\n");
	stdout_printf("           %d client threads/node, %d iters. "
		"Clients run one at a time\n", num_thr, num_iter);

	//
	// Setup scenario.
	// Use server on one node and client threads on other nodes.
	//
	for (int thread = 0; thread < num_thr; ++thread) {

		cli(num_nodes_test, thread) >>
		    setiter(num_iter) >>
		    ser(server_id, thread);
	}
	cli.serial(true);			// clients run one at a time

	bool	result = execute_complex_test(num_nodes_test,
		    new_low, new_moderate, new_high, server_id, cli, ser);

	// Allow time for return of idle borrowed worker threads
	os::usecsleep(10000000);		// 10 seconds

	return (result);
}


//
// Driver main routine.
//
int
#if defined(_UNODE)
driver(int argc, char *argv[])
#else
main(int argc, char *argv[])
#endif
{
	bool	result = true;
	int	testnum;

	// If script just wants us to list available tests...
	if ((argc > 1) && (strcmp(argv[1], "-?") == 0)) {
		list_tests();
		return (0);
	}

	if (! driver_init()) {
		done();				// tell script we're done
		return (1);
	}

	if (argc == 1) {
		//
		// Tests to run by default.
		//
		result &= test1();	// non-scaled, local invos only
		result &= test2();	// non-scaled, local & remote invos
		result &= test3();	// scaled test
		result &= test4();	// flow control test
		result &= test5();	// Unref threadpool test
	} else {
		--argc;
		++argv;
		if (! convert_arg(argv[0], testnum)) {
			stderr_printf("ERROR: Invalid test number: %s\n",
				argv[0]);
			list_tests();
			done();			// tell script we're done
			return (1);
		}

		//
		// Run specific test.
		//
		switch (testnum) {
		case 1:
			// Non-scaled test, local invocations only.
			result &= test1(argc, argv);
			break;
		case 2:
			// Non-scaled test, local and remote invocations.
			result &= test2(argc, argv);
			break;
		case 3:
			// Scaled test.
			result &= test3(argc, argv);
			break;
		case 4:
			// Flow-control test.
			result &= test4(argc, argv);
			break;
		case 5:
			// Unref threadpool test.
			result &= test5();
			break;
		default:
			stderr_printf("ERROR: Invalid test number: %d\n",
				testnum);
			result = false;
			break;
		}
	}

	done();				// tell script we're done
	return (result ? 0 : 1);
}


#else // UNODE_LIST_TESTS

//
// This is a hackish way for the unode test script, run_unode_fi, to have
// the driver print the list of available tests without having to start
// a unode process first (which could be slow and expensive).
//
int
main()
{
	list_tests();
}

#endif // UNODE_LIST_TESTS
