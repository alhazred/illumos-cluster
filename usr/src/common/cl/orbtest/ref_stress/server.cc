/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)server.cc	1.21	08/05/20 SMI"

#include <sys/types.h>
#include <sys/os.h>
#include <orb/invo/common.h>

#include <orbtest/ref_stress/server_impl.h>

// ---------------------------------------------------------------------------
// KERNEL server
// ---------------------------------------------------------------------------
#if defined(_KERNEL)

#include <sys/systm.h>
#include <sys/modctl.h>
#include <sys/errno.h>


extern "C" {
	void _cplpl_init();
	void _cplpl_fini();
}

char _depends_on[] = "misc/cl_runtime misc/cl_load misc/cl_orb "
	"misc/cl_haci misc/cl_quorum misc/cl_comm";

extern struct mod_ops mod_miscops;

static struct modlmisc modlmisc = {
	&mod_miscops, "ref_stress server",
};

static struct modlinkage modlinkage = {
	MODREV_1, {&modlmisc, NULL}
};

static server_impl	*serverp = NULL;

int
_init(void)
{
	int		error;
	struct modctl	*mp;
	char		*ptr;
	idnum		server_id;
	unsigned	mult;

	if (error = mod_install(&modlinkage)) {
		return (error);
	}
	_cplpl_init();				// C++ initialization

	// Get module name so we can obtain the server id from it.
	if ((mp = mod_getctl(&modlinkage)) == NULL) {
		os::printf("Can't find modctl\n");
		_cplpl_fini();
		mod_remove(&modlinkage);
		return (EIO);
	}

	// Get server id from the end of the module name.
	ptr = mp->mod_modname;
	if ('0' <= *ptr && *ptr <= '9') {
		os::printf("Module name must begin with non-numeric\n");
		_cplpl_fini();
		mod_remove(&modlinkage);
		return (EINVAL);
	}

	while (*ptr != '\0')
		++ptr;
	--ptr;

	if (! ('0' <= *ptr && *ptr <= '9')) {
		os::printf("Module name must end with a number\n");
		_cplpl_fini();
		mod_remove(&modlinkage);
		return (EINVAL);
	}

	// Convert string server id number into numeric.
	server_id = 0;
	mult = 1;
	while ('0' <= *ptr && *ptr <= '9') {
		server_id += (*ptr - '0') * mult;
		mult *= 10;
		--ptr;
	}

	// Create the server implementation.
	serverp =  new server_impl(server_id);
	if (! serverp->init()) {
		delete serverp; serverp = NULL;
		_cplpl_fini();
		mod_remove(&modlinkage);
		return (EIO);
	}

	return (0);
}

int
_fini(void)
{
	// Return EBUSY if modunloaded before we get an unreferenced() call
	serverp->lock();
	if (! serverp->unref_called()) {
		// os::printf("%s still referenced\n", serverp->name());
		serverp->unlock();
		return (EBUSY);
	}
	serverp->unlock();

	delete serverp; serverp = NULL;

	_cplpl_fini();
	return (mod_remove(&modlinkage));
}

int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}

#endif // _KERNEL


// ---------------------------------------------------------------------------
// USER server
// ---------------------------------------------------------------------------
#if defined(_USER)

#include <stdio.h>
#include <orb/infrastructure/orb.h>

int
main(int argc, char *argv[])
{
	server_impl	*serverp;
	idnum		server_id;

	if (argc != 2) {
		fprintf(stderr, "Usage: %s <server_id>\n", argv[0]);
		return (1);
	}

	server_id = atol(argv[1]);

	if (ORB::initialize() != 0) {
		fprintf(stderr, "ERROR: Can't initialize ORB\n");
		return (1);
	}

	serverp = new server_impl(server_id);
	if (! serverp->init()) {
		return (1);
	}

	serverp->wait_until_unreferenced();
	delete serverp;

	return (0);
}

#endif // _USER


// ---------------------------------------------------------------------------
// UNODE server
// ---------------------------------------------------------------------------
#if defined(_UNODE)

#include <stdio.h>


int
server(int argc, char *argv[])
{
	server_impl	*serverp;
	idnum		server_id;

	if (argc != 2) {
		fprintf(stderr, "Usage: %s <server_id>\n", argv[0]);
		return (1);
	}

	server_id = (idnum)atol(argv[1]);

	serverp = new server_impl(server_id);
	if (! serverp->init()) {
		return (1);
	}

	serverp->wait_until_unreferenced();
	delete serverp;

	return (0);
}

#endif // _UNODE
