/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)driver_support.cc	1.40	08/05/20 SMI"

#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <signal.h>
#include <sys/stat.h>
#include <time.h>
#include <thread.h>
#include <errno.h>
#include <stdarg.h>

#include <sys/os.h>
#include <orb/invo/common.h>
#include <nslib/ns.h>
#include <orbtest/ref_stress/driver_support.h>

#if defined(_USER)
#include <orb/infrastructure/orb.h>
#endif

// The script's stdout and stderr (in unode mode, the node's stdout/stderr
// could be different from the script's).
FILE	*script_stdout = NULL;
FILE	*script_stderr = NULL;

static uint32_t	verbose_level = 0;
static bool	cmm_reconfigure = false;
static uint32_t	num_live_nodes = 0;
static nodeid_t	*live_nodeids = NULL;	// node ID's of the live nodes
static rsrc_bal	*balancers = NULL;	// array of rsrc balancer interfaces

// To communicate with the "run_test" or "run_unode" script.
static FILE	*fp_script2driver = NULL;	// script->driver (read)
static FILE	*fp_driver2script = NULL;	// driver->script (write)

static char	*entity2str(entity_t);



//
// rsrc_bal
//	Represents the resource balancer interface (ref_stress::rsrc_balancer)
//	on a node.
//
rsrc_bal::rsrc_bal()
{
	_server_id = 0;
	_nodeid = 0;
	_ref = rsrc_balancer::_nil();
}

rsrc_bal::~rsrc_bal()
{
	if (is_not_nil(_ref)) {
		// We have a reference. Make the reference go away.
		CORBA::release(_ref);

		// Tell external script to end to resource balancer interface.
		(void) end(_nodeid, RSRC_BALANCER, _server_id);
	}
}	//lint !e1740

bool
rsrc_bal::init(idnum server_id, nodeid_t nid)
{
	_server_id = server_id;
	_nodeid = nid;

	return (true);
}


//
// Returns a reference to the resource balancer interface.
// Returns a nil reference if an error occurs.
//
// NOTE: there can only be one balancer interface per node.
//
rsrc_balancer_ptr
rsrc_bal::objref()
{
	const int	num_retries = 20;

	// If we already got a reference to the balancer interface already.
	if (is_not_nil(_ref)) {
		return (_ref);
	}

	naming::naming_context_var context = ns::root_nameserver();
	Environment		e;
	CORBA::Exception	*ex = 0;
	CORBA::Object_var	objp;
	char			nsname[64];

	// Start the balancer interface on the node based on the id it's given.
	if (! start(_nodeid, RSRC_BALANCER, _server_id)) {
		return (rsrc_balancer::_nil());
	}

	//
	// Get reference to balancer interface object impl from the name
	// server.  Give it time to start and register its object impl: we'll
	// attempt num_retries times with one-second sleep in between.
	//
	(void) rsrc_balancer_nsname(nsname, _nodeid);
	int	trial;
	for (trial = 0; trial < num_retries; ++trial) {
		objp = context->resolve(nsname, e);
		ex = e.exception();
		if (! ex) {
			break;			// got object
		}
		if (! naming::not_found::_exnarrow(ex)) {
			ex->print_exception("driver:");
			(void) stderr_printf("ERROR: can't resolve \"%s\"\n",
			    nsname);
			return (rsrc_balancer::_nil());
		}
		(void) sleep(1);		// give it time
		e.clear();			// reset variable
	}
	if (ex) {
		(void) stderr_printf("ERROR: can't resolve \"%s\" after %d"
		    " seconds\n", nsname, trial);
		return (rsrc_balancer::_nil());
	}

	_ref = rsrc_balancer::_narrow(objp);

	// Tell it to unregister itself from the name server.
	_ref->done(e);
	e.clear();

	// Tell it the verbose level.
	_ref->set_verbose(verbose_level, e);
	if (e.exception()) {
		char	tmpbuf[64];

		e.exception()->print_exception("driver:");
		(void) stderr_printf("ERROR: can't set verbose level on %s\n",
			client_name(tmpbuf, _nodeid));
		return (rsrc_balancer::_nil());
	}

	return (_ref);
}


//
// Returns the node id where the corresponding resource balancer interface is.
//
nodeid_t
rsrc_bal::nodeid()
{
	return (_nodeid);
}



//
// server_testobjs
//
server_testobjs::server_testobjs() : _servers(init_seqmax)
{
}

server_testobjs::~server_testobjs()
{
	// Delete each server_info pointed to in the sequence.
	for (uint32_t i = 0; i < num_servers(); ++i) {
		delete _servers[i];
	}
}

testobj_descr
server_testobjs::operator() (idnum server_id, idnum testobj_id)
{
	uint32_t	nservers = num_servers();

	// If a new server info needs to be added...
	if (server_id >= nservers) {
		_servers.length(server_id + 1);

		// Add a server_info from indices nservers through server_id.
		for (uint32_t i = nservers; i <= server_id; ++i) {
			_servers[i] = new server_info;
		}
	}

	// Increment the number of test objects for the specified server
	// if it hasn't been referred to yet.
	if (testobj_id >= _servers[server_id]->num_testobjs()) {
		_servers[server_id]->num_testobjs(testobj_id + 1);
	}

	// Return a testobj_descr struct so thread_info::operator>> can get it.
	return (testobj_descr(server_id, testobj_id));
}


//
// Returns the minimum number of server threads for the specified resource pool
// of the node represented by the given server_id.
// Returns true if successful, false otherwise.
// NOTE: There can only be one resource balancer per node.  If the given
//	 server id >= number of nodes alive, that id will refer to the same
//	 node as another one, i.e. modulo the number of nodes.
//
bool
server_testobjs::get_threads_min(idnum server_id,
		resource_defs::resource_pool_t pool,
		int &minimum)
{
	int			bal_index;
	rsrc_balancer_ptr	obj_p;
	nodeid_t		nid;
	Environment		e;

	// Determine index to the balancers array (modulo the number of nodes).
	bal_index = (int)(server_id % num_live_nodes);

	nid = balancers[bal_index].nodeid();
	obj_p = balancers[bal_index].objref();
	if (CORBA::is_nil(obj_p)) {
		char	tmpbuf[64];

		(void) stderr_printf("ERROR: can't get reference to %s\n",
			rsrc_balancer_name(tmpbuf, nid));
		return (false);
	}

	minimum = obj_p->get_threads_min(pool, e);
	if (e.exception()) {
		Errmsg	*errp = errmsg_excp(e);
		char	tmpbuf[64];

		if (errp) {
			(void) stderr_printf(errp->msg);
		} else {
			e.exception()->print_exception("driver:");
			(void) stderr_printf("ERROR: can't get threads minimum"
			    " from %s\n", rsrc_balancer_name(tmpbuf, nid));
		}
		return (false);
	}

	return (true);
}


//
// Sets the minimum number of server threads for the specified resource pool
// of the node represented by the given server_id.
// Returns true if successful, false otherwise.
// NOTE: There can only be one resource balancer per node.  If the given
//	 server id >= number of nodes alive, that id will refer to the same
//	 node as another one, i.e. modulo the number of nodes.
//
bool
server_testobjs::set_threads_min(idnum server_id,
		resource_defs::resource_pool_t pool,
		int minimum)
{
	int			bal_index;
	rsrc_balancer_ptr	obj_p;
	nodeid_t		nid;
	Environment		e;

	// Determine index to the balancers array (modulo the number of nodes).
	bal_index = (int)(server_id % num_live_nodes);

	nid = balancers[bal_index].nodeid();
	obj_p = balancers[bal_index].objref();
	if (CORBA::is_nil(obj_p)) {
		char	tmpbuf[64];

		(void) stderr_printf("ERROR: can't get reference to %s\n",
			rsrc_balancer_name(tmpbuf, nid));
		return (false);
	}

	obj_p->set_threads_min(pool, minimum, e);
	if (e.exception()) {
		Errmsg	*errp = errmsg_excp(e);
		char	tmpbuf[64];

		if (errp) {
			(void) stderr_printf(errp->msg);
		} else {
			e.exception()->print_exception("driver:");
			(void) stderr_printf("ERROR: can't set threads minimum"
			    " from %s\n", rsrc_balancer_name(tmpbuf, nid));
		}
		return (false);
	}

	return (true);
}


//
// Returns the total number of server threads for the specified resource pool
// of the node represented by the given server_id.
// Returns true if successful, false otherwise.
// NOTE: There can only be one resource balancer per node.  If the given
//	 server id >= number of nodes alive, that id will refer to the same
//	 node as another one, i.e. modulo the number of
//
bool
server_testobjs::get_threads_total(idnum server_id,
		resource_defs::resource_pool_t pool,
		int &total)
{
	int			bal_index;
	rsrc_balancer_ptr	objp;
	nodeid_t		nid;
	Environment		e;

	// Determine index to the balancers array (modulo the number of nodes).
	bal_index = (int)(server_id % num_live_nodes);

	nid = balancers[bal_index].nodeid();
	objp = balancers[bal_index].objref();
	if (CORBA::is_nil(objp)) {
		char	tmpbuf[64];

		(void) stderr_printf("ERROR: can't get reference to %s\n",
			rsrc_balancer_name(tmpbuf, nid));
		return (false);
	}

	total = objp->get_threads_total(pool, e);
	if (e.exception()) {
		Errmsg	*errp = errmsg_excp(e);
		char	tmpbuf[64];

		if (errp) {
			(void) stderr_printf(errp->msg);
		} else {
			e.exception()->print_exception("driver:");
			(void) stderr_printf("ERROR: can't get threads total"
			    " from %s\n", rsrc_balancer_name(tmpbuf, nid));
		}
		return (false);
	}

	return (true);
}


//
// Returns the flow control policy parameters for the specified resource pool
// of the node represented by the given server_id.
// Returns true if successful, false otherwise.
// NOTE: There can only be one resource balancer per node.  If the given
//	 server id >= number of nodes alive, that id will refer to the same
//	 node as another one, i.e. modulo the number of
//
bool
server_testobjs::get_policy(idnum server_id,
		resource_defs::resource_pool_t pool,
		int &low, int &moderate, int &high, int &increment)
{
	int			bal_index;
	rsrc_balancer_ptr	objp;
	nodeid_t		nid;
	Environment		e;

	// Determine index to the balancers array (modulo the number of nodes).
	bal_index = (int)(server_id % num_live_nodes);

	nid = balancers[bal_index].nodeid();
	objp = balancers[bal_index].objref();
	if (CORBA::is_nil(objp)) {
		char	tmpbuf[64];

		(void) stderr_printf("ERROR: can't get reference to %s\n",
			rsrc_balancer_name(tmpbuf, nid));
		return (false);
	}

	objp->get_policy(pool, low, moderate, high, increment, e);
	if (e.exception()) {
		Errmsg	*errp = errmsg_excp(e);
		char	tmpbuf[64];

		if (errp) {
			(void) stderr_printf(errp->msg);
		} else {
			e.exception()->print_exception("driver:");
			(void) stderr_printf("ERROR: can't get policy"
			    " from %s\n", rsrc_balancer_name(tmpbuf, nid));
		}
		return (false);
	}

	return (true);
}


//
// Sets the flow control policy parameters for the specified resource pool
// of the node represented by the given server_id.
// Returns true if successful, false otherwise.
// NOTE: There can only be one resource balancer per node.  If the given
//	 server id >= number of nodes alive, that id will refer to the same
//	 node as another one, i.e. modulo the number of
//
bool
server_testobjs::set_policy(idnum server_id,
		resource_defs::resource_pool_t pool,
		int low, int moderate, int high, int increment,
		int number_nodes)
{
	int			bal_index;
	rsrc_balancer_ptr	objp;
	nodeid_t		nid;
	Environment		e;
	bool			result_succeeded;

	// Determine index to the balancers array (modulo the number of nodes).
	bal_index = (int)(server_id % num_live_nodes);

	nid = balancers[bal_index].nodeid();
	objp = balancers[bal_index].objref();
	if (CORBA::is_nil(objp)) {
		char	tmpbuf[64];

		(void) stderr_printf("ERROR: can't get reference to %s\n",
			rsrc_balancer_name(tmpbuf, nid));
		return (false);
	}

	result_succeeded = objp->set_policy(pool, low, moderate, high,
		increment, number_nodes, e);

	if (e.exception()) {
		Errmsg	*errp = errmsg_excp(e);
		char	tmpbuf[64];

		if (errp) {
			(void) stderr_printf(errp->msg);
		} else {
			e.exception()->print_exception("driver:");
			(void) stderr_printf("ERROR: can't set policy on %s\n",
				rsrc_balancer_name(tmpbuf, nid));
		}
		return (false);
	}

	if (!result_succeeded) {
		(void) stderr_printf("ERROR: set_policy reported failure\n");
		return (false);
	}

	return (true);
}


//
// Start all servers.
//
bool
server_testobjs::_start()
{
	const int	num_retries = 20;
	naming::naming_context_var context = ns::root_nameserver();

	for (idnum srvr_id = 0; srvr_id < num_servers(); ++srvr_id) {
		Environment		e;
		CORBA::Object_var	objp;
		CORBA::Exception	*ex = 0;
		char			nsname[64];

		// Determine which node this server is to run on.
		_servers[srvr_id]->_nodeid = csid2nodeid(srvr_id);

		// Start the server program/module
		(void) start(_servers[srvr_id]->_nodeid, SERVER, srvr_id);

		//
		// Get reference to server's object impl from the name server.
		// Give server time to start and register its object impl:
		// we'll attempt 'num_retries' times (arbitrary) with
		// one-second sleep in between.
		//
		(void) server_nsname(nsname, srvr_id);
		int	trial;
		for (trial = 0; trial < num_retries; ++trial) {
			objp = context->resolve(nsname, e);
			ex = e.exception();
			if (! ex) {
				break;			// got object
			}
			if (! naming::not_found::_exnarrow(ex)) {
				ex->print_exception("driver:");
				(void) stderr_printf("ERROR: can't resolve"
				    " \"%s\"\n", nsname);
				return (false);
			}
			(void) sleep(1);		// give it time
			e.clear();			// reset variable
		}
		if (ex) {
			(void) stderr_printf("ERROR: can't resolve \"%s\" "
				"after %d seconds\n", nsname, trial);
			return (false);
		}
		_servers[srvr_id]->_ref = ref_stress::server::_narrow(objp);

		// Tell server the verbose level.
		_servers[srvr_id]->_ref->set_verbose(verbose_level, e);
		if (e.exception()) {
			char	tmpbuf[64];

			e.exception()->print_exception("driver:");
			(void) stderr_printf("ERROR: can't set verbose level"
			    " on %s\n",	server_name(tmpbuf, srvr_id));
			return (false);
		}

		// Tell server to create objects, if any.
		_servers[srvr_id]->_ref->create_testobjs(
					_servers[srvr_id]->num_testobjs(), e);
		if (e.exception()) {
			Errmsg	*errp = errmsg_excp(e);

			if (errp) {
				(void) stderr_printf(errp->msg);
			} else {
				char	tmpbuf[64];

				e.exception()->print_exception("driver:");
				(void) stderr_printf("ERROR: can't create"
				    " objects on %s\n",
				    server_name(tmpbuf, srvr_id));
			}
			return (false);
		}
	}

	return (true);
}


//
// Have servers verify all their test objects have been unreferenced.
//
bool
server_testobjs::_test()
{
	//
	// Ordinary code waits for  the unreference to arrive
	// regardless as to how long that takes. Test programs want
	// to report test failures rather than wait forever.
	// The time required for unreference processing varies
	// significantly under normal conditions. So a large margin
	// is required to avoid false failure reporting.
	// When changing this value examine the ref_stress test with
	// the greatest amount of unreference processing, which is
	// currently test 5.
	//
	const uint32_t	timeout = 300;			// timeout in seconds
	bool		result = true;

	for (idnum srvr_id = 0; srvr_id < num_servers(); ++srvr_id) {
		Environment	e;

		_servers[srvr_id]->_ref->test_unrefs(timeout, e);
		if (e.exception()) {
			Errmsg	*errp = errmsg_excp(e);

			if (errp) {
				(void) stderr_printf(errp->msg);
			} else {
				char	tmpbuf[64];

				e.exception()->print_exception("driver:");
				(void) stderr_printf("ERROR: can't test unrefs"
				    " on %s\n", server_name(tmpbuf, srvr_id));
			}
			result = false;
		}
	}

	return (result);
}


//
// Done with all servers
//
bool
server_testobjs::_end()
{
	bool	result = true;

	for (idnum srvr_id = 0; srvr_id < num_servers(); ++srvr_id) {
		Environment	e;

		// In case server_testobjs::_start() failed.
		if (CORBA::is_nil(_servers[srvr_id]->_ref)) {
			continue;
		}

		_servers[srvr_id]->_ref->done(e);
		e.clear();	// we don't care about exception since this
				// only tells server to unregister from NS

		CORBA::release(_servers[srvr_id]->_ref);
		_servers[srvr_id]->_ref = ref_stress::server::_nil();

		result = result &&
		    end(_servers[srvr_id]->_nodeid, SERVER, srvr_id);
	}

	return (result);
}


//
// server_info
//
server_info::server_info()
{
	_ref = ref_stress::server::_nil();
	_nodeid = NODEID_UNKNOWN;
	_num_testobjs = 0;
}

server_info::~server_info()
{
	//
	// Somebody must have already released the reference
	//
}	//lint !e1540


//
// client_threads
//
client_threads::client_threads() : _clients(init_seqmax)
{
	_serial = false;		// by default, run clients in parallel
}

client_threads::~client_threads()
{
	// Delete each client_info pointed to in the sequence.
	for (uint32_t i = 0; i < num_clients(); ++i) {
		delete _clients[i];
	}
}

thread_info&
client_threads::operator() (idnum client_id, idnum thread_id)
{
	uint32_t	nclients, nthreads;

	// If a new client info needs to be added...
	nclients = num_clients();
	if (client_id >= nclients) {
		_clients.length(client_id + 1);

		// Add a client_info from indices nclients through client_id.
		for (uint32_t i = nclients; i <= client_id; ++i) {
			_clients[i] = new client_info;
		}
	}

	// If a new thread info needs to be added for the client...
	nthreads = _clients[client_id]->num_threads();
	if (thread_id >= nthreads) {
		_clients[client_id]->_threads.length(thread_id + 1);

		// Add a thread_info from indices nthreads through thread_id.
		for (uint32_t i = nthreads; i <= thread_id; ++i) {
			_clients[client_id]->_threads[i] = new thread_info;
		}
	}
	ASSERT(thread_id + 1 == _clients[client_id]->num_threads());

	return (*_clients[client_id]->_threads[thread_id]);
}


//
// If set to true, clients will be run one at a time, instead of in
// parallel.  Useful for testing flow control.  By default, clients
// are run in parallel.
//
void
client_threads::serial(bool s)
{
	_serial = s;
}


//
// Start all clients.
//
bool
client_threads::_start()
{
	const int	num_retries = 20;
	naming::naming_context_var context = ns::root_nameserver();
	uint32_t	num_cli = num_clients();
	uint32_t	num_thr;
	uint32_t	clnt_id;
	uint32_t	thrd_id;
	uint_t		reconf_client = 0;
	uint_t		reconf_thread = 0;
	bool		at_least_one_client = false;

	// If reconfiguration was selected choose client thread
	if (cmm_reconfigure) {
		reconf_client = (uint_t)random() % num_cli;
	}

	for (clnt_id = 0; clnt_id < num_cli; ++clnt_id) {
		Environment		e;
		CORBA::Exception	*ex = 0;
		CORBA::Object_var	objp;
		char			nsname[64];

		// Determine which node this client is to run on.
		_clients[clnt_id]->_nodeid = csid2nodeid(clnt_id);

		// Start the client program/module
		(void) start(_clients[clnt_id]->_nodeid, CLIENT, clnt_id);

		//
		// Get reference to client's object impl from the name server.
		// Give client time to start and register its object impl:
		// we'll attempt num_retries times with one-second sleep
		// in between.
		//
		(void) client_nsname(nsname, clnt_id);
		int	trial;
		for (trial = 0; trial < num_retries; ++trial) {
			objp = context->resolve(nsname, e);
			ex = e.exception();
			if (! ex) {
				break;			// got object
			}
			if (! naming::not_found::_exnarrow(ex)) {
				ex->print_exception("driver:");
				(void) stderr_printf("ERROR: can't resolve"
				    " \"%s\"\n", nsname);
				return (false);
			}
			(void) sleep(1);		// give it time
			e.clear();			// reset variable
		}
		if (ex) {
			(void) stderr_printf("ERROR: can't resolve \"%s\" "
			    "after %d seconds\n", nsname, trial);
			return (false);
		}
		_clients[clnt_id]->_ref = ref_stress::client::_narrow(objp);

		// Tell client the verbose level.
		_clients[clnt_id]->_ref->set_verbose(verbose_level, e);
		if (e.exception()) {
			char	tmpbuf[64];

			e.exception()->print_exception("driver:");
			(void) stderr_printf("ERROR: can't set verbose level on"
			    " %s\n", client_name(tmpbuf, clnt_id));
			return (false);
		}

		//
		// The test infrastructure currently expects a client on
		// every node, even if they do not do anything.
		// However, we do not tell the client to do anything when
		// it does not have any threads.
		//
		if (_clients[clnt_id]->num_threads() == 0) {
			// Some test scenarios do not create any client
			// threads on selected nodes.
			continue;
		}

		// Tell client to set up threads.
		_clients[clnt_id]->_ref->create_threads(
					_clients[clnt_id]->num_threads(), e);

		if (e.exception()) {
			Errmsg	*errp = errmsg_excp(e);

			if (errp) {
				(void) stderr_printf(errp->msg);
			} else {
				char	tmpbuf[64];

				e.exception()->print_exception("driver:");
				(void) stderr_printf("ERROR: can't set up "
				    "threads on %s\n",
				    client_name(tmpbuf, clnt_id));
			}
			return (false);
		}

		// Assign client the test sequences for each thread.
		num_thr = _clients[clnt_id]->num_threads();
		ASSERT(num_thr > 0);
		at_least_one_client = true;

		if (cmm_reconfigure) {
			reconf_thread = (uint_t)random() % num_thr;
		}

		for (thrd_id = 0; thrd_id < num_thr; ++thrd_id) {
			test_param_seq &test_seq =
				_clients[clnt_id]->_threads[thrd_id]->_test_seq;
			uint_t	num_tests = test_seq.length();

			//
			// If CMM reconfiguration option is set, is this the
			// thread which performs the reconfiguration.
			//
			if (cmm_reconfigure &&
			    clnt_id == reconf_client &&
			    thrd_id == reconf_thread) {
				// Now determine which iteration of which test
				// that starts the reconfiguration.
				uint_t	reconf_test = (uint_t)random() %
				    num_tests;

				test_seq[reconf_test].cmm_reconfigure =
				    (int)random() %
				    (int)test_seq[reconf_test].iter;
			}

			// Assign the test sequence to the thread.
			_clients[clnt_id]->_ref->set_test_seq(
							thrd_id, test_seq, e);
			if (e.exception()) {
				Errmsg	*errp = errmsg_excp(e);

				if (errp) {
					(void) stderr_printf(errp->msg);
				} else {
					char	tmpbuf[64];

					e.exception()->
						print_exception("driver:");
					(void) stderr_printf("ERROR: can't"
					    " assign test sequence to %s\n",
					    thread_name(tmpbuf,
						clnt_id, thrd_id));
				}
				return (false);
			}
		}
	}
	return (at_least_one_client);
}


//
// Have clients run the test.
//
bool
client_threads::_test()
{
	thread_t	*tids = new thread_t[num_clients()];
	uint_t		tids_idx;
	uint_t		i;
	int		err;
	bool		result = true;

	//
	// Use a separate thread for each client.  If these clients are to
	// run in parallel, then we'll start them altogether.  Otherwise,
	// we'll start them one by one.
	//
	for (tids_idx = 0; tids_idx < num_clients(); ++tids_idx) {
		err = thr_create(NULL, (size_t)0, _test_thr,
		    _clients[tids_idx]->_ref, (long)THR_SUSPENDED,
		    &tids[tids_idx]);
		if (err) {
			char	tmpbuf[64];

			(void) stderr_printf("ERROR: thr_create() failed to "
			    "create thread to test %s (%s)\n",
			    client_name(tmpbuf, tids_idx), strerror(err));
			break;
		}
	}

	// If thr_create() above failed ...
	if (tids_idx < num_clients()) {
		// Kill all the (suspended) threads created above.
		for (i = 0; i < tids_idx; ++i) {
			(void) thr_kill(tids[i], SIGTERM);
			(void) thr_continue(tids[i]);
			(void) thr_join(tids[i], NULL, NULL);
		}
		delete[] tids;
		return (false);
	}

	// If we're to run the clients in parallel, start all the threads and
	// then wait for all of them to finish.  Otherwise, start and wait
	// for them one at a time.
	if (_serial) {
		for (i = 0; i < tids_idx; ++i) {
			(void) thr_continue(tids[i]);	// start the client
			result = result && _wait_for_thread(tids[i]);
		}
	} else {
		for (i = 0; i < tids_idx; ++i) {
			(void) thr_continue(tids[i]);	// start the client
		}

		for (i = 0; i < tids_idx; ++i) {
			result = result && _wait_for_thread(tids[i]);
		}
	}

	delete[] tids;
	return (result);
}


//
// Wait for a client thread to finish.
//
bool
client_threads::_wait_for_thread(thread_t tid)
{
	Environment	*ep = NULL;
	Errmsg		*errp;
	bool		result = true;

	(void) thr_join(tid, NULL, (void **) &ep);
	if (ep && ep->exception()) {
		errp = errmsg_excp(*ep);
		if (errp) {
			(void) stderr_printf(errp->msg);
		} else {
			ep->exception()->print_exception("driver:");
			(void) stderr_printf("ERROR: failed to invoke a "
			    "client's test() method\n");
		}
		result = false;
	}
	delete ep;				// allocated by the thread
	return (result);
}


//
// client_threads::_test_thr
//
void *
client_threads::_test_thr(void *arg)
{
	client_ptr	clientp = (client_ptr) arg;
	Environment	*ep = new Environment;

	clientp->test(*ep);
	return (ep);
}


//
// Done with all clients
//
bool
client_threads::_end()
{
	bool	result = true;

	for (idnum clnt_id = 0; clnt_id < num_clients(); ++clnt_id) {
		Environment	e;

		// In case client_threads::_start() failed.
		if (CORBA::is_nil(_clients[clnt_id]->_ref)) {
			continue;
		}

		_clients[clnt_id]->_ref->done(e);
		e.clear();	// we don't care about exception since this
				// only tells client to unregister from NS

		CORBA::release(_clients[clnt_id]->_ref);
		_clients[clnt_id]->_ref = ref_stress::client::_nil();

		result = result &&
		    end(_clients[clnt_id]->_nodeid, CLIENT, clnt_id);
	}

	return (result);
}


//
// client_info
//
client_info::client_info() : _threads(init_seqmax)
{
	_ref = ref_stress::client::_nil();
	_nodeid = NODEID_UNKNOWN;
}

client_info::~client_info()
{
	// Delete each thread_info pointed to in the sequence.
	for (uint32_t i = 0; i < num_threads(); ++i) {
		delete _threads[i];
	}
	//
	// Somebody must have already released the reference
	//
}	//lint !e1740


//
// thread_info
//
thread_info::thread_info() : _test_seq(init_seqmax), _default_iter(1) {}

thread_info::~thread_info() {}

thread_info&
thread_info::operator>> (testobj_descr &tobj_descr)
{
	uint32_t	ntests = _test_seq.length();

	// Append an test_info at end of sequence.
	_test_seq.length(ntests + 1);
	_test_seq[ntests].server_id = tobj_descr.server_id;
	_test_seq[ntests].testobj_id = tobj_descr.testobj_id;
	_test_seq[ntests].iter = _default_iter;
	_test_seq[ntests].cmm_reconfigure = -1;

	return (*this);
}

thread_info&
thread_info::operator>> (iter_manip &iter_m)
{
	// Set the passed iteration number as subsequent default value.
	_default_iter = iter_m;
	return (*this);
}


//
// Perform setup, testing and cleanup, given references to server_testobjs
// and client_threads objects.
// Returns true if successful, false otherwise.
//
bool
test(client_threads &cli, server_testobjs &srv)
{
	bool	result = true;

	// If there are no servers or clients, then return.
	if (srv.num_servers() == 0 || cli.num_clients() == 0) {
		return (true);
	}

	if (srv._start() && cli._start()) {
		result = result && cli._test();
		result = result && srv._test();
	} else {
		result = false;
	}
	result = result && cli._end();
	result = result && srv._end();

	return (result);
}


//
// After initialization completes, this returns the number of live nodes.
//
uint32_t
number_nodes_alive()
{
	return (num_live_nodes);
}


//
// Return a client/server ID modulo the number of nodes alive.
//
int
node_modulo(int number)
{
	return (number % (int)num_live_nodes);
}


//
// Given a client/server id, returns a node id that can be used to run
// that client or server.  The basic algorithm is round-robin, i.e. modulo
// the number of live nodes.
//
nodeid_t
csid2nodeid(idnum id)
{
	return (live_nodeids[id % num_live_nodes]);
}


//
// Convenient routines to print to both the driver's and the script's
// stdout/stderr (if they're different).
// (In unode, the script's and driver's stdout/stderr could be different.)
//
int
stdout_printf(const char *fmt, ...)
{
	va_list	ap;
	int	ret;

	//
	// The macro va_start uses the symbol ___builtin_va_alist.
	// The compilation system is supposed to define this symbol,
	// and not the user. For more info refer to stdarg.h
	//
	va_start(ap, fmt);		//lint !e40
	ret = vfprintf(stdout, fmt, ap);
	if (script_stdout != stdout) {
		// Also print to script's stdout.
		ret = vfprintf(script_stdout, fmt, ap);
	}
	va_end(ap);
	return (ret);
}

int
stderr_printf(const char *fmt, ...)
{
	va_list	ap;
	int	ret;

	//
	// The macro va_start uses the symbol ___builtin_va_alist.
	// The compilation system is supposed to define this symbol,
	// and not the user. For more info refer to stdarg.h
	//
	va_start(ap, fmt);		//lint !e40

	ret = vfprintf(stderr, fmt, ap);
	if (script_stderr != stderr) {
		// Also print to script's stderr.
		ret = vfprintf(script_stderr, fmt, ap);
	}
	va_end(ap);
	return (ret);
}


//
// Convert a string argument into its equivalent integer.
// Returns the converted number in the parameter 'retnum'.
// Returns true if successful, false otherwise.
//
bool
convert_arg(char *arg, int &retnum)
{
	int	tmpnum;		// so retnum isn't changed if there's error
	char	*endp;

	errno = 0;
	tmpnum = (int)strtol(arg, &endp, 10);
	if (endp[0] != '\0' || errno != 0) {
		return (false);
	}
	retnum = tmpnum;
	return (true);
}


//
// Initialize driver.  Read in environment variables from script.
// Returns: true if successful, false otherwise.
//
// Note, below fdopen() is called in the following manner:
//
//	if (fp == NULL)
//		fp = fdopen(atoi(env), "r");
//
// This is especially important in unode mode: when the driver is executed
// multiple times it's executed within the same process.  So we must fdopen()
// only if we haven't done so (we can't call fclose() either since that will
// close the underlying fd).  Otherwise we'll keep allocating a STREAM for
// the same file descriptor over and over.  In user mode, the code won't
// make a difference.
//
bool
driver_init()
{
	char	*env, *endp;
	uint_t	i;

	// Turn off buffering of stdout and stderr.
	setbuf(stdout, NULL);
	setbuf(stderr, NULL);

	//
	// $REFSTRESS_FD_SCRIPT2DRIVER:
	//	fd number of the pipe to read control information from script.
	//
	env = getenv("REFSTRESS_FD_SCRIPT2DRIVER");
	ASSERT(env != NULL);
	if (fp_script2driver == NULL) {
		fp_script2driver = fdopen(atoi(env), "r");
	}
	ASSERT(fp_script2driver != NULL);
	setbuf(fp_script2driver, NULL);

	//
	// $REFSTRESS_FD_DRIVER2SCRIPT:
	//	fd number of the pipe to write control information to script.
	//
	env = getenv("REFSTRESS_FD_DRIVER2SCRIPT");
	ASSERT(env != NULL);
	if (fp_driver2script == NULL) {
		fp_driver2script = fdopen(atoi(env), "w");
	}
	ASSERT(fp_driver2script != NULL);
	setbuf(fp_driver2script, NULL);

	//
	// $REFSTRESS_FD_SCRIPT_STDOUT:
	//	fd number of the script's stdout (in unode mode, the node's
	//	stdout could be different from the script's).
	//
#if defined(_UNODE)
	env = getenv("REFSTRESS_FD_SCRIPT_STDOUT");
	ASSERT(env != NULL);
	if (script_stdout == NULL) {
		script_stdout = fdopen(atoi(env), "w");
	}
	ASSERT(script_stdout != NULL);
	setbuf(script_stdout, NULL);
#else
	script_stdout = stdout;			// just point to stdout
#endif

	//
	// $REFSTRESS_FD_SCRIPT_STDERR:
	//	fd number of the script's stderr (in unode mode, the node's
	//	stderr could be different from the script's).
	//
#if defined(_UNODE)
	env = getenv("REFSTRESS_FD_SCRIPT_STDERR");
	ASSERT(env != NULL);
	if (script_stderr == NULL) {
		script_stderr = fdopen(atoi(env), "w");
	}
	ASSERT(script_stderr != NULL);
	setbuf(script_stderr, NULL);
#else
	script_stderr = stderr;
#endif

	//
	// $REFSTRESS_OPT_NODES:
	//	Cluster information (number of live nodes and their node id's).
	//	Consists of a series of numbers: the first is number of nodes
	//	alive, followed by the node ID's of the nodes.
	//
	env = getenv("REFSTRESS_OPT_NODES");
	ASSERT(env != NULL);

	// Get number of live nodes.
	num_live_nodes = (uint32_t)strtoul(env, &endp, 10);

	// Check number of live nodes.
	if (num_live_nodes < 2) {
		(void) stderr_printf("ERROR: Insufficient number of live nodes:"
		    " %d\n", num_live_nodes);
		return (false);
	}

	// Get each node ID.
	delete [] live_nodeids;		// in case called multiple times
	live_nodeids = new nodeid_t[num_live_nodes];
	for (i = 0; i < num_live_nodes; ++i) {
		live_nodeids[i] = (nodeid_t)strtoul(endp, &endp, 10);
	}

	//
	// $REFSTRESS_OPT_CMM_RECONFIG:
	//	If nonzero, then force CMM reconfiguration at random points
	//	in time.
	//
	env = getenv("REFSTRESS_OPT_CMM_RECONFIG");
	ASSERT(env != NULL);
	cmm_reconfigure = (atoi(env) != 0);

	//
	// $REFSTRESS_OPT_VERBOSITY:
	//	Level of verbosity.
	//
	env = getenv("REFSTRESS_OPT_VERBOSITY");
	ASSERT(env != NULL);
	verbose_level = (uint32_t)atoi(env);

	// Allocate the resource balancer interface array (one per node).
	delete [] balancers;		// in case called multiple times
	balancers = new rsrc_bal[num_live_nodes];
	for (i = 0; i < num_live_nodes; ++i) {
		if (! balancers[i].init(i, csid2nodeid(i))) {
			(void) stderr_printf("ERROR: Can't initialize rsrc_bal"
			    " %d\n", i);
			return (false);
		}
	}

#if defined(_USER)
	if (ORB::initialize() != 0) {
		(void) stderr_printf("ERROR: Can't initialize ORB\n");
		return (false);
	}
#endif

	return (true);
}


//
// Tell script to start an entity on the node with node ID "nodeid" and
// with client/server id "id".
// Returns: true if successful, false otherwise.
//
bool
start(nodeid_t nodeid, entity_t entity, idnum id)
{
	char	char_buf[100];
	int	status;

	if (verbose_level >= 1) {
		(void) stdout_printf("Starting %s %d on node %d\n",
			entity2str(entity), id, nodeid);
	}

	(void) fprintf(fp_driver2script, "start %d %s %u\n",
	    nodeid, entity2str(entity), id);

	// Get status from the script.
	(void) fgets(char_buf, (int)(sizeof (char_buf)), fp_script2driver);
	status = atoi(char_buf);
	return (status == 0);
}


//
// Tell script to end an entity with client/server id "id".
// Returns: true if successful, false otherwise.
//
bool
end(nodeid_t nodeid, entity_t entity, idnum id)
{
	const int	num_retries = 60;
	const int	delay_unref_check = 2;

	char		char_buf[100];
	int		count, status;
	time_t		begin_time, end_time;

	if (verbose_level >= 1) {
		(void) stdout_printf("Stopping %s %d on node %d\n",
			entity2str(entity), id, nodeid);
	}

	// Try ending the entity for 'num_retries' times until it is ended
	// (as reported by the external script).  If it is still alive (i.e.
	// stil referenced) then ask the external script to terminate it
	// (for user-land and unode entity only).
	begin_time = time(NULL);
	for (count = 0; count < num_retries; ++count) {
		// Give the reference counting message some time to arive.
		(void) sleep(delay_unref_check);

		// Ask script to end it.
		(void) fprintf(fp_driver2script, "end %d %s %u\n",
			nodeid, entity2str(entity), id);

		// Get status from the script.
		(void) fgets(char_buf, (int)(sizeof (char_buf)),
		    fp_script2driver);
		status = atoi(char_buf);
		if (status == 0) {
			if (count > 0 && verbose_level >= 1) {
				(void) stdout_printf("   %s %d has stopped\n",
					entity2str(entity), id);
			}
			return (true);			// entity has ended
		}

		if (verbose_level >= 1) {
			(void) stdout_printf("   %s %d is still referenced. ",
				entity2str(entity), id);
			(void) stdout_printf("Trying to stop again...\n");
		}
	}
	end_time = time(NULL);

	(void) stderr_printf("ERROR: %s %d is still referenced after %d "
	    "seconds\n", entity2str(entity), id, end_time - begin_time);

	// Entity still alive: ask script to terminate it.
	(void) fprintf(fp_driver2script, "terminate %d %s %u\n",
		nodeid, entity2str(entity), id);

	(void) fgets(char_buf, (int)(sizeof (char_buf)), fp_script2driver);
	status = atoi(char_buf);
	return (status == 0);
}


//
// Tell external script testing is done and perform some clean up.
//
// Note, in unode it's especially important to reset some stuff, since
// multiple executing of the driver is done within the same process.  Also,
// don't fclose() any file streams, since that will close the underlying
// file descriptors for the driver's next run.
//
void
done()
{
	// Must delete the balancers first since their destructors may
	// use other information, e.g. live_nodeids.
	delete [] balancers;
	balancers = NULL;

	delete [] live_nodeids;
	live_nodeids = NULL;

	verbose_level = 0;
	cmm_reconfigure = false;
	num_live_nodes = 0;

	(void) fprintf(fp_driver2script, "done\n");
}


//
// Convert a entity_t into string.
//
static char *
entity2str(entity_t entity)
{
	switch (entity) {
	case CLIENT:		return ("client");
	case SERVER:		return ("server");
	case RSRC_BALANCER:	return ("rsrc_balancer");
	default:
		ASSERT(0);
		return ("unknown");
	}
}
