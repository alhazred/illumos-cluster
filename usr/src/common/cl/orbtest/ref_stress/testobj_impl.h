/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ORBTEST_REF_STRESS_TESTOBJ_IMPL_H
#define	_ORBTEST_REF_STRESS_TESTOBJ_IMPL_H

#pragma ident	"@(#)testobj_impl.h	1.19	08/05/20 SMI"

#include <orb/object/adapter.h>

#include <h/ref_stress.h>

#include <orbtest/ref_stress/impl_common.h>
#include <orbtest/ref_stress/server_impl.h>

//
// Server test object implementation
//
/* CSTYLED */
class testobj_impl : public McServerof<ref_stress::testobj>,
		public impl_common {
	friend class	server_impl;
public:
	bool		init(server_impl *server_implp, idnum testobj_id);
	void		_unreferenced(unref_t);

	// Returns a new reference.
	// Caller must have this object locked prior to calling this routine.
	testobj_ptr	get_newref();

	// Has this object been referenced at least once?
	// Caller must have this object locked prior to calling this routine.
	bool		was_referenced();

	//
	// Interface Operations.
	//
	void	test(const test_param_seq &test_seq, Environment &e);
	idnum	get_id(Environment &e);
	idnum	get_server_id(Environment &e);

private:
	// Only server_impl can create test objects.
	testobj_impl();
	testobj_impl(server_impl *server_implp, idnum testobj_id);

	~testobj_impl();		//lint !e1509

	idnum		_id;		// this test object's id number
	server_impl	*_server_implp;	// point back to the server impl
					// this object belongs to
	bool		_referenced;	// true if this has been referenced
	bool		_reconfig_allowed;	// Allowed when true
};

#endif	/* _ORBTEST_REF_STRESS_TESTOBJ_IMPL_H */
