/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)testobj_impl.cc	1.19	08/05/20 SMI"

#include <orbtest/ref_stress/testobj_impl.h>

//
// testobj_impl
//
testobj_impl::testobj_impl() : _server_implp(0), _id(0), _referenced(false) {}

testobj_impl::testobj_impl(server_impl *server_implp, idnum testobj_id)
{
	init(server_implp, testobj_id);
}

testobj_impl::~testobj_impl()
{
	// If this had been referenced, then make sure
	// _unreferenced() has been called.
	lock();
	if (was_referenced() && ! unref_called()) {
		os::printf("%s: ERROR: destructor called before "
			"_unreferenced()\n", name());
	}
	unlock();
}

bool
testobj_impl::init(server_impl *server_implp, idnum testobj_id)
{
	char	objname[64];

	_server_implp = server_implp;
	_id = testobj_id;

	name(testobj_name(objname, server_implp->_id, testobj_id));
	if (server_implp->_verbose_level >= 2) {
		os::printf("%s: created\n", name());
	}

	_reconfig_allowed = true;

	return (true);
}

//
// Note, it's possible for each testobj to get re-referenced after
// it's been unreferenced.
//
void
testobj_impl::_unreferenced(unref_t cookie)
{
#ifdef _KERNEL_ORB
	//
	// The user land orb does not vary the number of worker threads.
	// So there is no need to test this capability in user land.
	//
	// Sleep because want to force the system to use
	// more unreference threads. The intention is to exercise
	// the code that transfers threads between the unref_threadpool
	// and the server invocation threadpool.
	//
	os::usecsleep(1000000);		// 1 second sleep
#endif

	lock();

	// Make sure delivery of _unreferenced() calls goes in the order of
	// the object incarnations.  E.g., the following shouldn't occur:
	//
	//	Object gets referenced (1st incarnation).
	//	1st incarnation gets unreferenced.
	//	Object gets referenced (2nd incarnation).
	//	2nd incarnation gets unreferenced.
	//	Unreference of 2nd incarnation is delivered.
	//	Unreference of 1st incarnation is delivered.
	//
	// That is, the _unreferenced() call for the 2nd incarnation shouldn't
	// occur BEFORE the call for the 1st incarnation.  Otherwise the call
	// _last_unref(cookie) on the 1st incarnation's unreferenced() would
	// give an erroneous result (since the last reference -- the 2nd
	// incarnation -- has indeed been delivered).
	//
	ASSERT(unref_called() == false);

	// Mark flag whether this call is for the last reference.
	unref_called(_last_unref(cookie));

	unlock();
}

//
// Return a new reference.
// Caller must have this object locked prior to calling this routine.
//
testobj_ptr
testobj_impl::get_newref()
{
	ASSERT(lock_held());
	_referenced = true;		// has been referenced at least once
	unref_called(false);		// reset flag
	return (get_objref());
}

//
// Has this object been referenced at last once?
// Caller must have this object locked prior to calling this routine.
//
bool
testobj_impl::was_referenced()
{
	ASSERT(lock_held());
	return (_referenced);
}

void
testobj_impl::test(const test_param_seq &test_seq, Environment &e)
{
	if (_server_implp->_verbose_level >= 2) {
		os::printf("%s: invoked\n", name());
	}

	// Use a common routine to do the actual work.
	do_test(test_seq, name(), _server_implp->_verbose_level, e,
		_reconfig_allowed);
}

//
// Returns this test object's ID.
//
idnum
testobj_impl::get_id(Environment&)
{
	return (_id);
}

//
// Returns the ID of the server this test object belong to.
idnum
testobj_impl::get_server_id(Environment&)
{
	return (_server_implp->_id);
}
