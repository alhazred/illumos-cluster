/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ORBTEST_REF_STRESS_CLIENT_IMPL_H
#define	_ORBTEST_REF_STRESS_CLIENT_IMPL_H

#pragma ident	"@(#)client_impl.h	1.18	08/05/20 SMI"

#include <orb/object/adapter.h>

#include <h/ref_stress.h>
#include <orbtest/ref_stress/impl_common.h>

//
// Test thread
//
// Note: Only client_impl can use this class.
// Remember that default access is private.
//
class thread {
	friend class client_impl;

	thread();
	thread(client_impl* client_implp, idnum thread_id);
	~thread();

	bool	_init(client_impl* client_implp, idnum thread_id);
	void	_set_test_seq(const test_param_seq &test_seq)
			{ _test_seq = test_seq; }

	// Thread start function.  "arg" must point to instance of this class.
	static void	*_test(void *arg);

	const char	*_get_name() { return (_name); }

	idnum		_id;
	client_impl	*_client_implp;
	test_param_seq	_test_seq;
	char		*_name;
	Environment	_thr_env;
};

//
// Test client implementation.
//
/* CSTYLED */
class client_impl : public McServerof<ref_stress::client>, public impl_common {
	friend class thread;
public:
	client_impl(idnum client_id);
	~client_impl();			//lint !e1509

	bool	init();
	void	_unreferenced(unref_t);

	//
	// Interface Operations.
	//
	void	create_threads(uint32_t how_many, Environment &e);

	void	set_test_seq(
			idnum thread_id,
			const test_param_seq &test_seq,
			Environment &e);

	void	test(Environment &e);
	void	set_verbose(uint32_t level, Environment &e);
	void	done(Environment &e);

private:
	// Do not allow default constructor
	client_impl();

	idnum		_id;		// this client's id number
	uint32_t	_num_threads;	// # of threads to run during test
	thread		*_threads;
	uint32_t	_verbose_level;

#if defined(_KERNEL)
	os::sem_t	_thr_sema;
#endif
};

#endif	/* _ORBTEST_REF_STRESS_CLIENT_IMPL_H */
