/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)client_impl.cc	1.22	08/05/20 SMI"

#if defined(_UNODE) || defined(_USER)
#include <signal.h>
#endif

#include <orbtest/ref_stress/client_impl.h>


//
// Test thread
//
thread::thread() : _client_implp(0), _id(0), _name(0) {}

thread::thread(client_impl* client_implp, idnum thread_id) : _name(0)
{
	_init(client_implp, thread_id);
}

thread::~thread()
{
	delete[] _name;
}

bool
thread::_init(client_impl* client_implp, idnum thread_id)
{
	_client_implp = client_implp;
	_id = thread_id;

	// Set object name.
	delete[] _name;
	_name = new char[64];
	thread_name(_name, client_implp->_id, thread_id);

	if (client_implp->_verbose_level >= 2) {
		os::printf("%s: created\n", _get_name());
	}

	return (true);
}


//
// Thread start function.  "arg" points to an instance of class thread.
//
void *
thread::_test(void *arg)
{
	thread		*thisp = (thread *) arg;
	uint32_t	num_tests = thisp->_test_seq.length();
	naming::naming_context_var context = ns::root_nameserver();
	uint32_t	i;
	char		namebuf[64];
	bool		reconfig_allowed = true;

	if (num_tests == 0) {			// if no object to test
		goto done;
	}

	// First get references to test objects in the sequence.
	for (i = 0; i < num_tests; ++i) {
		Environment		tmpenv;
		CORBA::Object_var	objp;
		server_var		serverp;

		// Get the server object.
		server_nsname(namebuf, thisp->_test_seq[i].server_id);
		objp = context->resolve(namebuf, tmpenv);
		if (tmpenv.exception()) {
			char	msg[128];

			os::sprintf(msg, "%s: ERROR: can't resolve %s\n",
				thisp->_get_name(), namebuf);
			thisp->_thr_env.exception(new Errmsg(msg));
			goto cleanup;	// release objs already referenced
		}
		serverp = ref_stress::server::_narrow(objp);

		// Now get the testobj itself.
		thisp->_test_seq[i].testobjp =
			serverp->get_testobj(thisp->_test_seq[i].testobj_id,
				tmpenv);
		if (tmpenv.exception()) {
			char	msg[512];

			CORBA::SystemException	*sysex;
			if (sysex = system_excp(tmpenv)) {
				// Convert system exception to an Errmsg.
				testobj_name(namebuf,
					thisp->_test_seq[i].server_id,
					thisp->_test_seq[i].testobj_id);
				os::sprintf(msg, "%s: FAIL: can't get %s:\n"
					"\tSystemException: major %d, "
					"minor %d, completed %d\n",
					thisp->_get_name(), namebuf,
					sysex->_major(), sysex->_minor(),
					sysex->completed());
				thisp->_thr_env.exception(new Errmsg(msg));
				goto cleanup;
			}

			Errmsg	*errex;
			if (errex = errmsg_excp(tmpenv)) {
				// Just copy Errmsg from server.
				thisp->_thr_env.exception(new Errmsg(*errex));
				goto cleanup;
			}

			// Else, convert unknown exception to an Errmsg.
			CORBA::Exception *ex = tmpenv.exception();
			os::sprintf(msg, "%s: FAIL: can't get %s:\n"
				"\tException: %s major %d\n",
				thisp->_get_name(), namebuf,
				ex->_name() ? ex->_name() : "",
				ex->_major());
			thisp->_thr_env.exception(new Errmsg(msg));
			goto cleanup;
		}
	}

	// Call a common routine to perform the actual testing.
	do_test(thisp->_test_seq,
		thisp->_get_name(),
		thisp->_client_implp->_verbose_level,
		thisp->_thr_env,
		reconfig_allowed);

cleanup:
	// Release all references to test objects obtained above.
	for (i = 0; i < num_tests; ++i) {
		// testobjp is a _field - setting to nil will release any
		// existing references
		thisp->_test_seq[i].testobjp = ref_stress::testobj::_nil();
	}

done:
#if defined(_KERNEL)
	// Tell client_impl::test() that this thread is done.
	thisp->_client_implp->_thr_sema.v();
#endif
	return (NULL);
}



//
// Test client implementation.
//
client_impl::client_impl(idnum client_id) :
		_id(client_id),
		_num_threads(0),
		_threads(0),
		_verbose_level(0)
{
	char	objname[64];

	name(client_name(objname, client_id));
}


client_impl::~client_impl()
{
	delete[] _threads;

	// Make sure _unreferenced() has been called.
	lock();
	if (! unref_called()) {
		os::printf("%s: ERROR: destructor called before "
			"_unreferenced()\n", name());
	}
	unlock();
}


bool
client_impl::init()
{
	client_ptr	tmpref;
	bool		ret;
	char		strname[64];

	// Register with the name server.
	tmpref = get_objref();
	ret = register_obj(tmpref, client_nsname(strname, _id));
	CORBA::release(tmpref);
	return (ret);
}


void
#ifdef DEBUG
client_impl::_unreferenced(unref_t cookie)
#else
client_impl::_unreferenced(unref_t)
#endif
{
	lock();
	ASSERT(_last_unref(cookie));
	unref_called(true);
	unlock();
}


//
// Record "how_many" threads to be created when test starts.
//
void
client_impl::create_threads(uint32_t how_many, Environment &e)
{
	delete[] _threads;
	_threads = new thread[how_many];

	// Initialize thread information.
	for (uint32_t i = 0; i < how_many; ++i) {
		if (! _threads[i]._init(this, i)) {
			char	msg[128];

			os::sprintf(msg, "%s: ERROR: Can't initialize "
				"class thread # %d\n", name(), i);
			e.exception(new Errmsg(msg));

			delete[] _threads;
			_threads = 0;
			_num_threads = 0;
			return;
		}
	}

	_num_threads = how_many;
}


//
// Set test information sequence for thread with ID "thread_id".
//
void
client_impl::set_test_seq(
		idnum thread_id,
		const test_param_seq &test_seq,
		Environment&)
{
	_threads[thread_id]._set_test_seq(test_seq);
}


void
client_impl::test(Environment &e)
{
#if defined(_KERNEL)
	typedef	kthread_t *thread_t;
#endif
	thread_t	*tids = new thread_t[_num_threads];
	uint32_t	tids_idx, i;
	Errmsg		*errp = 0;

	// Create the threads.
	for (tids_idx = 0; tids_idx < _num_threads; ++tids_idx) {
#if defined(_KERNEL)
		tids[tids_idx] = thread_create(NULL, 0,
					(void (*)()) _threads[tids_idx]._test,
					(char *)&_threads[tids_idx], 0,
					&p0, TS_RUN, 97);

		// XXX	What to do when thread_create() fails and previous
		//	thread has run?  One way is to start the thread in
		//	suspended state, but how do we continue it without
		//	doing our own thread scheduling?
		//	For now, we'll just let the previous threads run.
		if (! tids[tids_idx]) {
			char	msg[128];

			os::sprintf(msg, "%s: ERROR: thread_create() "
				"failed to create thread %d\n",
				name(), tids_idx);
			errp = new Errmsg(msg);
			break;
		}
#else
		int err = thr_create(NULL, 0,
				_threads[tids_idx]._test, &_threads[tids_idx],
				THR_SUSPENDED | THR_BOUND | THR_NEW_LWP,
				&tids[tids_idx]);
		if (err) {
			char	msg[128];

			os::sprintf(msg, "%s: ERROR: thr_create() "
				"failed to create thread %d (%s)\n",
				name(), tids_idx, strerror(err));
			errp = new Errmsg(msg);
			break;
		}
#endif
	}

#if defined(_UNODE) || defined(_USER)
	// If any thread creation has failed ...
	if (tids_idx != _num_threads) {
		// Terminate all the (suspended) threads created above.
		for (i = 0; i < tids_idx; ++i) {
			thr_kill(tids[i], SIGTERM);
			thr_continue(tids[i]);
			thr_join(tids[i], NULL, NULL);
		}
		delete[] tids;
		e.exception(errp);
		return;
	}

	// Else, let the threads run.
	for (i = 0; i < tids_idx; ++i) {
		thr_continue(tids[i]);
	}
#endif

	// Wait until all threads are finished.
	for (i = 0; i < tids_idx; ++i) {
#if defined(_KERNEL)
		_thr_sema.p();
#else
		thr_join(tids[i], NULL, NULL);
#endif
	}

	// Copy error messages, if any, from each thread.
	for (i = 0; i < tids_idx; ++i) {
		Environment	&env = _threads[i]._thr_env;
		Errmsg		*thr_ex;

		// thread::_test() always set Errmsg when there's an exception.
		if (thr_ex = errmsg_excp(env)) {
			if (errp == NULL) {
				// Copy error message from thread.
				errp = new Errmsg(*thr_ex);
			} else {
				// Append error message from thread.
				size_t	oldlen = os::strlen(errp->msg);
				char	*newmsg = new char[oldlen +
						os::strlen(thr_ex->msg) + 1];

				os::strcpy(newmsg, errp->msg);
				os::strcpy(newmsg + oldlen, thr_ex->msg);
				errp->msg = newmsg;
			}
			env.clear();
		}
	}

	delete[] tids;
	if (errp) {
		e.exception(errp);
	}
}


//
// Set verbose level.
//
void
client_impl::set_verbose(uint32_t level, Environment&)
{
	_verbose_level = level;
}


void
client_impl::done(Environment &e)
{
	if (! unregister_obj()) {
		char	msg[128];

		os::sprintf(msg, "%s: ERROR: Can't unregister object\n",
			name());
		e.exception(new Errmsg(msg));
	}
}
