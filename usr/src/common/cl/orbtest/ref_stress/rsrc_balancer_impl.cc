/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rsrc_balancer_impl.cc	1.17	08/05/20 SMI"

#include <orb/infrastructure/orb_conf.h>
#include <orb/flow/resource_balancer.h>
#include <orbtest/ref_stress/rsrc_balancer_impl.h>


//
// rsrc_balancer_impl
//
rsrc_balancer_impl::rsrc_balancer_impl() : _verbose_level(0)
{
	char	objname[64];

	name(rsrc_balancer_name(objname, orb_conf::node_number()));
}


rsrc_balancer_impl::~rsrc_balancer_impl()
{
	// Make sure _unreferenced() has been called.
	lock();
	if (! unref_called()) {
		os::printf("%s: ERROR: destructor called before "
			"_unreferenced()\n", name());
	}
	unlock();
}


bool
rsrc_balancer_impl::init()
{
	rsrc_balancer_ptr	tmpref;
	bool 		ret;
	char			strname[64];

	// Register with the name server.
	tmpref = get_objref();
	ret = register_obj(tmpref,
		rsrc_balancer_nsname(strname, orb_conf::node_number()));
	CORBA::release(tmpref);
	return (ret);
}


void
#ifdef DEBUG
rsrc_balancer_impl::_unreferenced(unref_t cookie)
#else
rsrc_balancer_impl::_unreferenced(unref_t)
#endif
{
	lock();
	ASSERT(_last_unref(cookie));
	unref_called(true);
	unlock();
}


//
// Returns minimum number of server threads for the specified resource pool.
//
int32_t
rsrc_balancer_impl::get_threads_min(int32_t pool, Environment &)
{
	return (resource_balancer::the().get_threads_min(
	    (resource_defs::resource_pool_t) pool));
}


//
// Sets minimum number of server threads for the specified resource pool.
//
void
rsrc_balancer_impl::set_threads_min(int32_t pool, int32_t minimum,
    Environment &)
{
	resource_balancer::the().set_threads_min(
	    (resource_defs::resource_pool_t) pool, minimum);
}


//
// Returns total number of server threads for the specified resource pool.
//
int32_t
rsrc_balancer_impl::get_threads_total(int32_t pool, Environment &)
{
	int32_t	total;

	total = resource_balancer::the().get_threads_total(
				(resource_defs::resource_pool_t) pool);
	return (total);
}


//
// Returns the flow control policy parameters for the specified resource pool.
//
void
rsrc_balancer_impl::get_policy(int32_t pool, int32_t &low, int32_t &moderate,
		int32_t &high, int32_t &increment, Environment &)
{
	resource_balancer::the().get_policy(
				(resource_defs::resource_pool_t) pool,
				low, moderate, high, increment);
}


//
// Sets flow control policy parameters for the specified resource pool.
//
bool
rsrc_balancer_impl::set_policy(int32_t pool, int32_t low, int32_t moderate,
		int32_t high, int32_t increment, int32_t number_nodes,
		Environment &)
{
	return (resource_balancer::the().set_policy(
				(resource_defs::resource_pool_t) pool,
				low, moderate, high, increment, number_nodes));
}


//
// Set verbose level
//
void
rsrc_balancer_impl::set_verbose(uint32_t level, Environment&)
{
	_verbose_level = level;
}


//
// Tell this impl to unregister itself from the name server.
//
void
rsrc_balancer_impl::done(Environment &e)
{
	if (! unregister_obj()) {
		char	msg[128];

		os::sprintf(msg, "%s: ERROR: Can't unregister object\n",
			name());
		e.exception(new Errmsg(msg));
	}
}
