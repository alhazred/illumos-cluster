/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ORBTEST_REF_STRESS_SERVER_IMPL_H
#define	_ORBTEST_REF_STRESS_SERVER_IMPL_H

#pragma ident	"@(#)server_impl.h	1.18	08/05/20 SMI"

#include <orb/object/adapter.h>

#include <h/ref_stress.h>
#include <orbtest/ref_stress/impl_common.h>
#include <orbtest/ref_stress/testobj_impl.h>

//
// Test server implementation.
//
/* CSTYLED */
class server_impl : public McServerof<ref_stress::server>, public impl_common {
	friend class	testobj_impl;
public:
	server_impl(idnum server_id);
	~server_impl();			//lint !e1509

	bool	init();
	void	_unreferenced(unref_t);

	//
	// Interface Operations
	//
	void		create_testobjs(uint32_t how_many, Environment &e);
	testobj_ptr	get_testobj(idnum testobj_id, Environment &e);
	void		test_unrefs(uint32_t timeout, Environment &e);
	void		set_verbose(uint32_t level, Environment &e);
	void		done(Environment &e);

private:
	// Do not allow default constructor
	server_impl();

	idnum		_id;		// this server's id number
	uint32_t	_num_testobjs;	// # of test objects for this server
	testobj_impl	*_testobj_impls;
	uint32_t	_verbose_level;
};

#endif	/* _ORBTEST_REF_STRESS_SERVER_IMPL_H */
