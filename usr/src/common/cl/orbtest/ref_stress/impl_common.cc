/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)impl_common.cc	1.31	08/05/20 SMI"

#include <sys/types.h>

#include <sys/os.h>
#include <nslib/ns.h>
#include <cmm/cmm_ns.h>

#include <orbtest/ref_stress/impl_common.h>

#if defined(_UNODE)
int
unode_init()
{
	os::printf("Reference stress test module loaded\n");
	return (0);
}
#endif

//
// Class impl_common: common (non-exported) methods for implementation classes.
//
impl_common::impl_common() :
		_unref_called(false),
		_obj_registered(false),
		_nsname(0),
		_name(0) {}

impl_common::~impl_common()
{
	delete[] _nsname;
	delete[] _name;
}


//
// Register object with the name server with the given name.
// Returns: true if successful, false otherwise.
//
bool
impl_common::register_obj(CORBA::Object_ptr objref, const char *strname)
{
	Environment	e;
	CORBA::Exception	*ex;

	// Ignore if object is already registered.
	if (_obj_registered)
		return (true);

	// Save name to be registered with the name server.
	nsname(strname);

	// Get the root name server.
	naming::naming_context_var context = ns::root_nameserver();

	// Register implementation object with the name server.
	context->rebind(strname, objref, e);
	if (ex = system_excp(e)) {
		ex->print_exception((char *)name());
		os::printf("%s: ERROR: can't bind object\n", name());
		return (false);
	}

	_obj_registered = true;
	return (true);					// success
}


//
// Unregister object from the name server.
// Returns: true if successful, false otherwise.
//
bool
impl_common::unregister_obj()
{
	Environment	e;
	CORBA::Exception	*ex;

	// Ignore if object is already unregistered.
	if (! _obj_registered)
		return (true);

	// Get the root name server.
	naming::naming_context_var context = ns::root_nameserver();

	// Deregister implementation object from name server.
	context->unbind(nsname(), e);
	if (ex = system_excp(e)) {
		ex->print_exception((char *)name());
		os::printf("%s: ERROR: can't unbind object\n", name());
		return (false);
	}

	delete[] _nsname;
	_nsname = 0;

	_obj_registered = false;
	return (true);					// success
}


//
// Suspend calling thread until _unreferenced() is called.
// Returns: true if _unreferenced() has been called.
//	    false if timeout arg (in seconds) is nonzero and it
//	    expires before _unreferenced() is called.
//
bool
impl_common::wait_until_unreferenced(uint32_t timeout)
{
	bool	retval = true;

	_mutex.lock();
	if (timeout == 0) {
		while (! unref_called()) {
			_cv.wait(&_mutex);
		}
	} else {
		os::systime	systime;
		// setreltime() requires timeout in usecs.
		systime.setreltime(timeout * 1000000);
		while (! unref_called()) {
			// If timeout expires...
			if (_cv.timedwait(&_mutex, &systime) ==
			    os::condvar_t::TIMEDOUT) {
				retval = false;
				break;
			}
		}
	}
	_mutex.unlock();
	return (retval);
}


//
// Has _unreferenced() been called?
// Caller must have this object locked prior to calling this routine.
//
bool
impl_common::unref_called()
{
	ASSERT(_mutex.lock_held() != 0);
	return (_unref_called);
}


//
// Mark whether _unreferenced() has been called.
// Caller must have this object locked prior to calling this routine.
//
void
impl_common::unref_called(bool b)
{
	ASSERT(_mutex.lock_held() != 0);
	_unref_called = b;
	_cv.signal();		// signal wait_until_unreferenced()
}


//
// Save name registered with ns.
//
void
impl_common::nsname(const char *str)
{
	delete[] _nsname;
	_nsname = new char[os::strlen(str) + 1];
	os::strcpy(_nsname, str);
}


//
// Set name.
//
void
impl_common::name(const char *str)
{
	delete[] _name;
	_name = new char[os::strlen(str) + 1];
	os::strcpy(_name, str);
}



//
// invoke_errmsg - generate invocation error message.
// The error message is generated here to move the string buffers out
// of the remote invocation path.
//
void
invoke_errmsg(const char *msg_prefix, char *namebuf, int iter,
    Environment & tmpenv, Environment & e)
{
	char			tmpbuf[256];
	CORBA::SystemException *sysex;

	if (sysex = system_excp(tmpenv)) {
		// Convert system exception to an Errmsg.
		os::sprintf(tmpbuf, "%s: FAIL: invoke %s iter %d:\n"
		    "\tSystemException: major %d, minor %d, completed %d\n",
		    msg_prefix, namebuf, iter,
		    sysex->_major(), sysex->_minor(), sysex->completed());
		e.exception(new Errmsg(tmpbuf));
		return;
	}

	Errmsg	*errex;
	if (errex = errmsg_excp(tmpenv)) {
		size_t	oldlen = os::strlen(errex->msg);
		char	*newmsg;
		Errmsg	*newerr = new Errmsg();

		// Append our message as history of
		// invocations.
		os::sprintf(tmpbuf, "\t%s: iter %d\n", msg_prefix, iter);

		newmsg = new char[oldlen + os::strlen(tmpbuf)+ 1];
		os::strcpy(newmsg, errex->msg);
		os::strcpy(newmsg + oldlen, tmpbuf);

		newerr->msg = newmsg;
		e.exception(newerr);
		return;
	}

	// Else, convert unknown exception to an Errmsg.
	CORBA::Exception *ex = tmpenv.exception();
	os::sprintf(tmpbuf, "%s: FAIL: invoke %s iter %d:\n"
	    "\tException: %s major %d\n",
	    msg_prefix, namebuf, iter,
	    ex->_name() ? ex->_name() : "", ex->_major());
	e.exception(new Errmsg(tmpbuf));
}

//
// do_reconfigure - force a node reconfiguration.
//
bool
do_reconfigure(const char *msg_prefix, uint32_t verbose_level, Environment &e)
{
	char			tmpbuf[256];
	cmm::control_var	cmmp = cmm_ns::get_control();
	Environment		tmpenv;

	// Now force the reconfiguration.
	if (verbose_level >= 2) {
		os::printf("%s: forcing CMM reconfiguration\n", msg_prefix);
	}
	cmmp->reconfigure(tmpenv);
	if (tmpenv.exception()) {
		CORBA::SystemException	*sysex = system_excp(tmpenv);

		if (sysex) {
			os::sprintf(tmpbuf, "%s: ERROR: can't force "
			    "CMM reconfig:\n\tSystemException: major %d "
			    "minor %d completed %d\n",
			    msg_prefix, sysex->_major(), sysex->_minor(),
			    sysex->completed());
		} else {
			CORBA::Exception *ex = tmpenv.exception();

			os::sprintf(tmpbuf, "%s: ERROR: can't force "
			    "CMM reconfig:\n\tException: %s major %d\n",
			    msg_prefix,
			    ex->_name() ? ex->_name() : "",
			    ex->_major());
		}
		e.exception(new Errmsg(tmpbuf));
		return (false);
	}

	return (true);
}

//
// A common function used by client threads and testobj.  Performs the
// actual reference test.
//
// Side Effects: sets Environment exception and returns false when a
//		 failure/error occurs.  Returns true otherwise.
//
bool
do_test(
    const test_param_seq	&test_seq,
    const char			*msg_prefix,
    uint32_t			verbose_level,
    Environment			&e,
    bool			&reconfig_allowed)
{
	test_param_seq	seq2pass;
	uint32_t	num_tests = test_seq.length();
	char		namebuf[40];

	if (num_tests == 0) {			// if no object to test
		return (true);
	}

	// Point to the 2nd through last test params to be passed to the
	// next test object's test() method.
	seq2pass.load(num_tests - 1, num_tests - 1,
			(num_tests == 1) ? NULL : (test_param *) &test_seq[1],
			false);

	// Save name of testobj in 1st test param.
	testobj_name(namebuf, test_seq[0].server_id, test_seq[0].testobj_id);

	// Repeat "iter" times as described in the 1st test param.
	for (int i = 0; i < test_seq[0].iter; ++i) {
		testobj_var	testobjp;	// released at end of loop body
		Environment	tmpenv;

		// When the iteration matches perform a node reconfiguration
		if (reconfig_allowed && test_seq[0].cmm_reconfigure == i) {
			reconfig_allowed = false;
			if (!do_reconfigure(msg_prefix, verbose_level, e)) {
				return (false);
			}
		}

		// Get a duplicate of the test object in the 1st test param.
		// This will provide some stress (reference, release,
		// reference, release, ...) to the reference counting
		// mechanism since "testobjp" is of type testobj_var.
		testobjp = ref_stress::testobj::_duplicate(
							test_seq[0].testobjp);

		if (verbose_level >= 2) {
			os::printf("%s: invoking %s\n", msg_prefix, namebuf);
		}

		// Invoke test object, passing 2nd through last test params.
		testobjp->test(seq2pass, tmpenv);
		if (tmpenv.exception()) {
			invoke_errmsg(msg_prefix, namebuf, i, tmpenv, e);
			return (false);
		}
	}

	return (true);
}
