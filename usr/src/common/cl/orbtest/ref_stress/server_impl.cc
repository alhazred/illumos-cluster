/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)server_impl.cc	1.15	08/05/20 SMI"

#include <orbtest/ref_stress/server_impl.h>

//
// server_impl
//
server_impl::server_impl(idnum server_id) :
		_id(server_id),
		_num_testobjs(0),
		_testobj_impls(0),
		_verbose_level(0)
{
	char	objname[64];

	name(server_name(objname, server_id));
}

server_impl::~server_impl()
{
	delete[] _testobj_impls;

	// Make sure _unreferenced() has been called.
	lock();
	if (! unref_called()) {
		os::printf("%s: ERROR: destructor called before "
			"_unreferenced()\n", name());
	}
	unlock();
}

bool
server_impl::init()
{
	server_ptr	tmpref;
	bool 	ret;
	char		strname[64];

	// Register with the name server.
	tmpref = get_objref();
	ret = register_obj(tmpref, server_nsname(strname, _id));
	CORBA::release(tmpref);
	return (ret);
}

void
#ifdef DEBUG
server_impl::_unreferenced(unref_t cookie)
#else
server_impl::_unreferenced(unref_t)
#endif
{
	lock();
	ASSERT(_last_unref(cookie));
	unref_called(true);
	unlock();
}

//
// Create "how_many" testobj implementation's.
//
void
server_impl::create_testobjs(uint32_t how_many, Environment &e)
{
	delete[] _testobj_impls;
	_testobj_impls = new testobj_impl[how_many];

	// Initialize test objects.
	for (uint32_t i = 0; i < how_many; ++i) {
		if (! _testobj_impls[i].init(this, i)) {
			char	msg[128];

			os::sprintf(msg, "%s: ERROR: Can't initialize "
				"testobj # %d\n", name(), i);
			e.exception(new Errmsg(msg));

			delete[] _testobj_impls;
			_testobj_impls = 0;
			_num_testobjs = 0;
			return;
		}
	}

	_num_testobjs = how_many;
}

//
// Given a testobj ID, return a reference to the corresponding testobj.
//
testobj_ptr
server_impl::get_testobj(idnum testobj_id, Environment &e)
{
	// Make sure the testobj id is valid.
	if (testobj_id >= _num_testobjs) {
		char	msg[128];

		os::printf(msg, "%s: ERROR: invalid testobj (id %d) is "
			"requested, number of testobjs is %d\n",
			name(), testobj_id, _num_testobjs);
		e.exception(new Errmsg(msg));
		return (ref_stress::testobj::_nil());
	}

	testobj_ptr	testobjp;

	_testobj_impls[testobj_id].lock();
	testobjp = _testobj_impls[testobj_id].get_newref();
	_testobj_impls[testobj_id].unlock();

	return (testobjp);
}

//
// Verify that the _unreferenced() of each testobj has been called
// within "timeout" seconds.
//
void
server_impl::test_unrefs(uint32_t timeout, Environment &e)
{
	Errmsg	*errp = 0;

	for (uint32_t i = 0; i < _num_testobjs; ++i) {
		testobj_impl	&obj_impl = _testobj_impls[i];

		obj_impl.lock();

		// If testobj hasn't been referenced at all then it's ok.
		if (! obj_impl.was_referenced()) {
			obj_impl.unlock();
			continue;
		}

		// Must unlock since wait_until_unreferenced() sets lock.
		obj_impl.unlock();

		if (! obj_impl.wait_until_unreferenced(timeout)) {
			char	msg[128];

			os::sprintf(msg, "%s: FAIL: %s did not get "
				"unreferenced after %d seconds\n",
				name(), obj_impl.name(), timeout);

			if (errp == NULL) {
				errp = new Errmsg(msg);
			} else {
				// Append err message.
				size_t	oldlen = os::strlen(errp->msg);
				char	*newmsg = new char[oldlen +
							os::strlen(msg) + 1];

				os::strcpy(newmsg, errp->msg);
				os::strcpy(newmsg + oldlen, msg);

				errp->msg = newmsg;
			}
		}
	}

	if (errp) {
		e.exception(errp);
	}
}

//
// Set verbose level
//
void
server_impl::set_verbose(uint32_t level, Environment&)
{
	_verbose_level = level;
}

void
server_impl::done(Environment &e)
{
	if (! unregister_obj()) {
		char	msg[128];

		os::sprintf(msg, "%s: ERROR: Can't unregister object\n",
			name());
		e.exception(new Errmsg(msg));
	}
}
