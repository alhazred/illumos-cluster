/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _BASE_DEFS_H
#define	_BASE_DEFS_H

#pragma ident	"@(#)base_defs.h	1.16	08/05/20 SMI"

#include <stdio.h>
#include <orb/invo/common.h>
#include <orb/fault/fault_injection.h>
#include <sys/hashtable.h>

#if !defined(_KERNEL)
#if defined(_KERNEL_ORB)
#define	_UNODE
#else
#define	_USER
#endif	// _KERNEL_ORB
#endif	// !_KERNEL

// Whether a fault is to be armed/disarmed by default (i.e. when no arguments
// are given to arm_faults or disarm_faults).
enum default_t { DEFAULT, NOT_DEFAULT };

//
// Virtual base class arm_fault.
//

class arm_fault {
public:
	virtual void	usage() = 0;
	virtual bool	parse_options(const char *optstr) = 0;
	virtual bool	write_arg(FILE *) = 0;
	virtual void	arm(int nodeid) = 0;
	virtual void	disarm(int nodeid) = 0;

	uint_t		fault_num()		{ return (_fault_num); }
	const char	*name()			{ return (_name); }

	void		select()		{ _selected = true; }
	void		deselect()		{ _selected = false; }
	bool		is_selected()		{ return (_selected); }

	void		set_default(default_t d)	{ _default = d; }
	bool		is_default()	{ return (_default == DEFAULT); }
	const char	*default_yesno()
			    { return (_default == DEFAULT ? "yes" : "no"); }

	void		store_rcfile_line(char *line);
	const char	*get_rcfile_line()	{ return (_rcfile_line); }

	typedef string_hashtable_t<arm_fault *>	table_t;
	typedef table_t::iterator		table_iter_t;

	// Hash table of all faults we know about.
	static table_t	table;

	// Set to true by main() if this program is executed from rc file.
	static bool	execed_from_rcfile;

	virtual	~arm_fault();

protected:
	arm_fault(uint_t arg_fault_num, const char *arg_name, default_t);

	// Return true if the next option in optstr can be converted to the
	// desired type (or if there is no more option), false if error occurs.
	bool	parse_opt(const char *optstr, int &result);
	bool	parse_opt(const char *optstr, uint_t &result);
	bool	parse_opt(const char *optstr, char *&result);

	// Return true if there are still are still more options to process.
	bool	is_more_options()
		    { return (_next_opt != NULL && _next_opt[0] != '\0'); }

private:
	uint_t		_fault_num;
	char		*_name;
	const char	*_optstr;
	const char	*_next_opt;
	bool		_selected;
	default_t	_default;
	char		*_rcfile_line;
	// no default constructor, copy constructor, assignment
	arm_fault();
	arm_fault(const arm_fault &);
	arm_fault &operator = (const arm_fault &);
};

//
// Base class for faults which set SystemException.
//
class sysex_fault : public arm_fault {
public:
	bool	parse_options(const char *optstr);
	bool	write_arg(FILE *);
	void	arm(int nodeid);
	void	disarm(int nodeid);

protected:
	sysex_fault(
		uint_t				arg_fault_num,
		const char			*arg_name,
		default_t			arm_by_default,
		CORBA::SystemException		&ex,
		FaultFunctions::counter_when_t	arg_when,
		uint_t				arg_count,
		bool				arg_verbose = true)
		: arm_fault(arg_fault_num, arg_name, arm_by_default),
		    _sysex(ex), _when(arg_when), _count(arg_count),
		    _verbose(arg_verbose) {}

	FaultFunctions::counter_when_t when()		{ return (_when); }
	void	when(FaultFunctions::counter_when_t w)	{ _when = w; }

	uint_t	count()					{ return (_count); }
	void	count(uint_t l)				{ _count = l; }

	CORBA::SystemException	&sysex()		{ return (_sysex); }
	void	sysex(CORBA::SystemException &ex)	{ _sysex = ex; }

	void	verbose(bool b)			{ _verbose = b; }
	bool	verbose()				{ return (_verbose); }

private:
	CORBA::SystemException		_sysex;
	FaultFunctions::counter_when_t	_when;
	uint_t				_count;

	// If true, the set_sys_exception() fault function will print a warning
	// message prior to setting the exception.
	bool	_verbose;

	// no default constructor, copy constructor, assignment
	sysex_fault();
	sysex_fault(const sysex_fault &);
	sysex_fault &operator = (const sysex_fault &);
};

//
// Base class for retry-unsent-message faults.
// These faults set RETRY_NEEDED(0, COMPLETED_NO) system exception
// at random time.
//
class retry_unsent_fault : public sysex_fault {
public:
	bool	parse_options(const char *optstr);

protected:
	retry_unsent_fault(
		uint_t		arg_fault_num,
		const char	*arg_name,
		default_t	arm_by_default,
		uint_t		mtbf)
		: sysex_fault(arg_fault_num, arg_name, arm_by_default,
			CORBA::RETRY_NEEDED(0, CORBA::COMPLETED_NO),
			FaultFunctions::RANDOM, mtbf, false) {}
private:
	// no default constructor, copy constructor, assignment
	retry_unsent_fault();
	retry_unsent_fault(const retry_unsent_fault &);
	retry_unsent_fault &operator = (const retry_unsent_fault &);
};

//
// Base class for retry-sent-message faults.
// These faults set RETRY_NEEDED(0, COMPLETED_MAYBE) system exception
// at random time.
//
class retry_sent_fault : public sysex_fault {
public:
	bool	parse_options(const char *optstr);

protected:
	retry_sent_fault(
		uint_t		arg_fault_num,
		const char	*arg_name,
		default_t	arm_by_default,
		uint_t		mtbf)
		: sysex_fault(arg_fault_num, arg_name, arm_by_default,
			CORBA::RETRY_NEEDED(0, CORBA::COMPLETED_MAYBE),
			FaultFunctions::RANDOM, mtbf, false) {}
private:
	// no default constructor, copy constructor, assignment
	retry_sent_fault();
	retry_sent_fault(const retry_sent_fault &);
	retry_sent_fault &operator = (const retry_sent_fault &);
};

//
// Base class for simple vanilla fault point
//
class vanilla_fault : public arm_fault {
protected:
	vanilla_fault(uint_t arg_fault_num, const char *fault_name,
			default_t arm_by_default, uint32_t default_opt);
	virtual ~vanilla_fault() {};	// declare destructor virtual
	// derived class must only provide the usage
	virtual void	usage() = 0;
	// but it can still override any of the below functions
	bool	parse_options(const char *optstr);
	bool	write_arg(FILE *);
	void	arm(int nodeid);
	void	disarm(int nodeid);

	// a vanilla fault point has a single option
	uint32_t	_opt;
private:
	// no default constructor, copy constructor, assignment
	vanilla_fault();
	vanilla_fault(const vanilla_fault &);
	vanilla_fault &operator = (const vanilla_fault &);
};

#endif	/* _BASE_DEFS_H */
