/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)arm_faults.cc	1.24	08/05/20 SMI"

#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <limits.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <sys/os.h>
#include <orb/invo/common.h>

#if defined(_FAULT_INJECTION)

#include <orbtest/arm_faults/fault_defs.h>

enum ProgType { ARM, DISARM };

#ifdef	_USER
static const char	rcfile[] = "/etc/rc2.d/S99arm_faults";

// In the rc file, the list of faults follows this line.
static const char	begin_line[] = "aRM%#fAULtS@!stArTxOXHerE#%!";
#endif	// _USER

static char		*progname;
static ProgType		progtype;

static void	usage(void);
static void	fault_usage(const char *faultname = NULL);
static void	select_all(void);
static void	select_defaults(void);
static void	arm_selected(void);
static void	disarm_selected(void);
#ifdef	_USER
static bool	examine_rcfile(FILE *);
static bool	update_rcfile(FILE *);
#endif	// _USER

#if defined(_KERNEL_ORB)
static int
main_unode(int argc, char *argv[])
#else
int
main(int argc, char *argv[])
#endif
{
	// List of known faults.
	//
	// Note, having these on the stack is especially important in unode
	// since this shared library gets loaded only once and we need to
	// initialize these objects everytime this function is invoked.
	//
	retry_unsent_invo	_retry_unsent_invo;
	retry_sent_invo		_retry_sent_invo;
	retry_unsent_reply	_retry_unsent_reply;
	retry_sent_reply	_retry_sent_reply;
	retry_unsent_refjob	_retry_unsent_refjob;
	retry_sent_refjob	_retry_sent_refjob;
	retry_unsent_cmm	_retry_unsent_cmm;
	retry_sent_cmm		_retry_sent_cmm;
	retry_unsent_signal	_retry_unsent_signal;
	retry_sent_signal	_retry_sent_signal;
	retry_unsent_ckpt	_retry_unsent_ckpt;
	retry_sent_ckpt		_retry_sent_ckpt;
	retry_unsent_flow	_retry_unsent_flow;
	retry_sent_flow		_retry_sent_flow;
	retry_unsent_oneway	_retry_unsent_oneway;
	retry_sent_oneway	_retry_sent_oneway;
	retry_unsent_faultmsg	_retry_unsent_faultmsg;
	retry_sent_faultmsg	_retry_sent_faultmsg;
	retry_unsent_vmmsg	_retry_unsent_vmmsg;
	retry_sent_vmmsg	_retry_sent_vmmsg;
	pathend_fail		_pathend_fail;
	tick_delay_panic	_tick_delay_panic(NOT_DEFAULT);
	delay_send_thread	_delay_send_thread(NOT_DEFAULT);
	delay_clock_thread	_delay_clock_thread(NOT_DEFAULT);
	// stop_pm_send		_stop_pm_send;
	path_down_panic		_path_down_panic(NOT_DEFAULT);

	int		opt;
	bool		all_except = false;
	bool		print_fault_usage = false;
#if defined(_USER)
	int		err;
	int		rcfile_fd;
	FILE		*rcfile_fp = NULL;
	struct flock	flock;
#endif

	// Get the program's name.
	if ((progname = strrchr(argv[0], '/')) != NULL) {
		++progname;
	} else {
		progname = argv[0];
	}

	// Determine which program this is.
	if (strcmp(progname, "arm_faults") == 0) {
		progtype = ARM;
	} else if (strcmp(progname, "disarm_faults") == 0) {
		progtype = DISARM;
	} else {
		os::printf("ERROR: Program unrecognized: %s\n", progname);
		return (1);
	}

	// Need to reset optind since in unode getopt() will use the same
	// variable across multiple invocations of this function (and possibly
	// among different shared libraries using getopt()).
	//
	// XXX this is still MT-unsafe; there should be a MT-safe getopt().
	optind = 1;

	// Parse options..
	while ((opt = getopt(argc, argv, "hux?")) != EOF) {
		switch (opt) {
		case 'h':
			usage();
			return (0);
		case 'u':
			print_fault_usage = true;
			break;
		case 'x':
			all_except = true;
			break;
		case '?':
			if (optopt == opt) {
				// User really typed in -?.
				usage();
				return (0);
			}
			return (1);
		default:
			return (1);		// error from getopt()
		}
	}

	// If -x was specified, one or more faults must also be specified.
	if (all_except && optind >= argc) {
		os::printf("ERROR: One or more faults must be specified "
			"with the -x option\n");
		return (1);
	}

	// If user wants to print fault usage information...
	if (print_fault_usage) {
		if (optind >= argc) {
			// No more arguments, print all faults.
			fault_usage();
		} else if (all_except) {
			arm_fault		*faultp;
			arm_fault::table_iter_t	iter(arm_fault::table);

			// Print all faults except those specified.

			// First select all faults.
			select_all();

			// Then deselect those that are specified.
			for (; optind < argc; ++optind) {
				faultp = arm_fault::table.lookup(argv[optind]);
				if (faultp == NULL) {
					os::warning("%s doesn't match any "
						"known fault\n", argv[optind]);
				} else {
					faultp->deselect();
				}
			}

			// Print only those faults we've selected.
			for (; (faultp = iter.get_current()) != NULL;
			    iter.advance()) {
				if (faultp->is_selected()) {
					faultp->usage();
					os::printf("\n");
				}
			}
		} else {
			// Print each specified fault.
			for (; optind < argc; ++optind) {
				fault_usage(argv[optind]);
			}
		}
		return (0);
	}

	// If there are no more arguments, all if the -x option is specified,
	// select all default faults first.
	if (optind >= argc || all_except) {
		select_defaults();
	}

#if defined(_USER)
	if (optind < argc && strcmp(argv[optind], begin_line) == 0) {
		// A special argument: if it's equal to begin_line[] we know
		// this program is executed from the rc file.  We won't process
		// the file to prevent conflict with the shell that's also
		// reading the file.
		arm_fault::execed_from_rcfile = true;
		++optind;
	}
#endif

	// Parse the rest of the arguments.
	for (; optind < argc; ++optind) {
		char		*name;
		char		*options;
		char		*colon;
		arm_fault	*faultp;

		// Find the ':' delimiter, if any.
		if ((colon = strchr(argv[optind], ':')) != NULL) {
			*colon = '\0';
		}

		// Point to the fault name and the options (if any).
		name = argv[optind];
		options = colon ? (colon + 1) : (name + strlen(name));

		// Get object with the specified name from the arm_fault table.
		faultp = arm_fault::table.lookup(name);
		if (faultp == NULL) {
			os::printf("ERROR: %s doesn't match any known fault\n",
				name);
			return (1);
		}

		// Have the fault parse its options.
		if (! faultp->parse_options(options)) {
			return (1);
		}

		// Select this fault to be arm/disarmed unless -x is specified.
		if (all_except) {
			faultp->deselect();
		} else {
			faultp->select();
		}
	}

#if defined(_USER)
	if (err = ORB::initialize()) {
		os::printf("ERROR: Can't initialize ORB: %s\n", strerror(err));
		return (1);
	}
#endif

#if defined(_USER)
	// Open/create the rc file so faults can be recorded and remain
	// armed across reboots.
	// Note, we use open() below since fopen() doesn't allow us to open
	// the file with the equivalent of O_RDWR|O_CREAT (create the file
	// only if necessary without truncating it).
	rcfile_fd = open(rcfile, O_RDWR | O_CREAT,
			S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);
	if (rcfile_fd < 0) {
		os::printf("ERROR: Can't open \"%s\": %s\n",
			rcfile, strerror(errno));
		return (1);
	}

	// Use file lock to allow only one instance of this program to run
	// so the rc file won't get mangled.
	flock.l_type = F_WRLCK;
	flock.l_whence = SEEK_SET;
	flock.l_start = flock.l_len = 0;		// lock entire file
	if (fcntl(rcfile_fd, F_SETLK, &flock) < 0) {
		if (errno == EAGAIN) {
			os::printf("Another arm_faults/disarm_faults is "
				"currently running.  Try again later.\n");
			return (0);
		}
		os::printf("ERROR: Can't lock \"%s\": %s\n",
			rcfile, strerror(errno));
		return (1);
	}

	// Process the rc file only if this program is not executed from the
	// rc file, otherwise we'll nasty conflict with the shell that's
	// executing it (we still use the file lock for exclusivity, though).
	if (! arm_fault::execed_from_rcfile) {
		rcfile_fp = fdopen(rcfile_fd, "r+");
		if (rcfile_fp == NULL) {
			os::printf("ERROR: Can't create file stream: %s\n",
				strerror(errno));
			return (1);
		}

		// Examine the rc file to determine which faults are
		// currently armed.
		if (! examine_rcfile(rcfile_fp)) {
			return (1);
		}
	}
#endif // _USER

	// Arm or disarm selected faults depending on the name of this program.
	switch (progtype) {
	case ARM:
		arm_selected();
		break;
	case DISARM:
		disarm_selected();
		break;
	default:
		ASSERT(0);
		break;
	}

#if defined(_USER)
	// Update the rc file to reflect currently armed faults.
	if (! arm_fault::execed_from_rcfile) {
		if (! update_rcfile(rcfile_fp)) {
			return (1);
		}
	}
#endif // _USER

	return (0);
}

//
// Print usage information.
//
void
usage()
{
	os::printf(
"SYNOPSIS:\n"
"   arm_faults    [-?|-h] [-u] [-x] [<fault> ...]\n"
"   disarm_faults [-?|-h] [-u] [-x] [<fault> ...]\n"
"\n"
"DESCRIPTION:\n"
"   arm_faults arms one or more faults (Node Triggers) on the current node.\n"
"   disarm_faults disarms one or more faults set by 'arm_faults'.\n"
"\n"
"   Each <fault> argument denotes a fault to arm/disarm and is specified as\n"
"\n"
"       <name>[:<option>[,<option> ...]]\n"
"\n"
"   where <name> is the name for the fault.  If the fault accepts options,\n"
"   they can be specified by a ':' following the name and a comma-separated\n"
"   (no spaces) list of values.\n"
"\n"
"   If no <fault> argument is given, then faults which are marked to be\n"
"   armed/disarmed by default (see list below) will be armed/disarmed.\n"
"\n"
"OPTIONS:\n"
"   -?|-h   Print this message.\n"
"   -u      Print usage information of specific fault(s).\n"
"           If the -x option is also specified, then print usage information\n"
"           of all faults EXCEPT those specified.\n"
"           If no fault is specified, then print usage information of\n"
"           all faults.\n"
"   -x      Select all faults EXCEPT the specified ones.\n"
"           Note, with this option you must specify at least one fault.\n");

	os::printf("\nKNOWN FAULTS:\n\n");
	fault_usage();
}

//
// Print a fault's (or all faults' if parameter "faultname" is NULL)
// usage information.
//
void
fault_usage(const char *faultname /* = NULL */)
{
	arm_fault	*faultp;

	if (faultname == NULL) {
		arm_fault::table_iter_t	iter(arm_fault::table);

		// Print usage information of all known faults.
		for (; (faultp = iter.get_current()) != NULL; iter.advance()) {
			faultp->usage();
			os::printf("\n");
		}
	} else {
		// Print usage information of a specific fault.
		faultp = arm_fault::table.lookup(faultname);
		if (faultp == NULL) {
			os::warning("%s doesn't match any known fault\n",
				faultname);
		} else {
			faultp->usage();
		}
		os::printf("\n");
	}
}

//
// Select all faults (whether or not they are marked to be armed/disarmed
// by default).
//
void
select_all()
{
	arm_fault::table_iter_t	iter(arm_fault::table);
	arm_fault		*faultp;

	for (; (faultp = iter.get_current()) != NULL; iter.advance()) {
		faultp->select();
	}
}

//
// Select all faults which are marked to be armed/disarmed by default.
//
void
select_defaults()
{
	arm_fault::table_iter_t	iter(arm_fault::table);
	arm_fault		*faultp;

	for (; (faultp = iter.get_current()) != NULL; iter.advance()) {
		if (faultp->is_default()) {
			faultp->select();
		}
	}
}

//
// Arm all faults that have been selected
//
void
arm_selected()
{
	arm_fault::table_iter_t	iter(arm_fault::table);
	arm_fault		*faultp;

	for (; (faultp = iter.get_current()) != NULL; iter.advance()) {
		if (faultp->is_selected()) {
			os::printf("Arming %s\n", faultp->name());
			faultp->arm((int)TRIGGER_THIS_NODE);
		}
	}
}

//
// Disarm all faults that have been selected
//
void
disarm_selected()
{
	arm_fault::table_iter_t	iter(arm_fault::table);
	arm_fault		*faultp;

	for (; (faultp = iter.get_current()) != NULL; iter.advance()) {
		if (faultp->is_selected()) {
			os::printf("Disarming %s\n", faultp->name());
			faultp->disarm((int)TRIGGER_THIS_NODE);
		}
	}
}

//
// Examine the rc file to determine which faults are currently armed.
//
#if defined(_USER)
bool
examine_rcfile(FILE *fp)
{
	char	line[1024];
	bool	begin_found = false;

	rewind(fp);

	while (fgets(line, sizeof (line), fp)) {
		size_t	len = strlen(line);

		// Ignore empty lines.
		if (len == 0) {
			continue;
		}

		// Remove new line.
		if (line[len - 1] == '\n') {
			line[len - 1] = '\0';
		}

		// Read each line until we find one with begin_line[].
		if (! begin_found) {
			if (strcmp(begin_line, line) == 0) {
				begin_found = true;
			}
			continue;
		}

		char		*colon;
		arm_fault	*faultp;

		// Find the ':' delimiter, if any.
		if (colon = strchr(line, ':')) {
			*colon = '\0';
		}

		// If the line matches a known fault, have the fault remember
		// the line.
		if (faultp = arm_fault::table.lookup(line)) {
			if (colon) {
				*colon = ':';		// restore it first
			}
			faultp->store_rcfile_line(line);
		} else {
			os::warning("Fault %s in file \"%s\" doesn't match "
				"any known fault.  Ignored.\n", line, rcfile);
		}
	}

	if (ferror(fp)) {
		os::printf("ERROR: Can't read \"%s\": %s\n",
			rcfile, strerror(errno));
		return (false);
	}

	return (true);
}
#endif // _USER

//
// Update the rc file to reflect currently armed faults.
//
#if defined(_USER)
bool
update_rcfile(FILE *fp)
{
	const char	*execname;
	char		dirpath[PATH_MAX];

	// Get the directory where the executable of this program is.
	execname = getexecname();
	if (execname[0] != '/') {
		if (getcwd(dirpath, sizeof (dirpath)) == NULL) {
			os::printf("ERROR: Can't get cwd: %s\n",
				strerror(errno));
			return (false);
		}
	} else {
		const char	*last_slash;

		last_slash = strrchr(execname, '/');
		strncpy(dirpath, execname, last_slash - execname);
		dirpath[last_slash - execname] = '\0';
	}

	// Truncate the file first.
	// Note, ftruncate() is used below instead of freopen() so we can
	// maintain the lock on the rc file throughout the life of this
	// program.
	fflush(fp);
	if (ftruncate(fileno(fp), 0) < 0) {
		os::printf("ERROR: Can't truncate \"%s\": %s\n",
			rcfile, strerror(errno));
		return (false);
	}
	rewind(fp);

	// Write a shell script to execute arm_faults.
	fprintf(fp,
		"#!/sbin/sh\n"
		"#\n"
		"# Arm faults at boot time.\n"
		"#\n"
		"# DO NOT MODIFY THIS FILE.\n"
		"# IT'S AUTOMATICALLY GENERATED AND USED BY %1$s/arm_faults.\n"
		"# FOR MORE INFORMATION, ENTER:\n"
		"#\n"
		"#	%1$s/arm_faults -?\n"
		"#\n"
		"/usr/sbin/clinfo >/dev/null 2>&1 || exit 0\n"
		"test -x %1$s/arm_faults || exit 0\n"
		"xargs %1$s/arm_faults << EOF\n",
		dirpath);
	fprintf(fp, "%s\n", begin_line);
	fflush(fp);

	//
	// Write the faults (and its options) into the rcfile.
	//
	arm_fault::table_iter_t	iter(arm_fault::table);
	arm_fault		*faultp;
	int			num_written;
	const char		*rcfile_line;

	switch (progtype) {
	case ARM:
		// Write the fault only if it's selected by the user, OR
		// it was listed in the rc file before (it was armed).
		for (; faultp = iter.get_current(); iter.advance()) {
			if (faultp->is_selected()) {
				if (! faultp->write_arg(fp)) {
					return (false);
				}
				continue;
			}

			// If it was in the rc file before, write it as it was.
			if (rcfile_line = faultp->get_rcfile_line()) {
				fprintf(fp, "%s\n", rcfile_line);
			}
		}
		break;

	case DISARM:
		// Write the fault only if the user did NOT select it
		// AND it was listed in the rc file before (it was armed).
		num_written = 0;
		for (; faultp = iter.get_current(); iter.advance()) {
			if (faultp->is_selected()) {
				// User selected this fault to be disarmed.
				continue;
			}

			// If it was in the rc file before, write it as it was.
			if (rcfile_line = faultp->get_rcfile_line()) {
				fprintf(fp, "%s\n", rcfile_line);
				++num_written;
			}
		}

		// If no faults were written, unlink the file to prevent
		// arm_faults from arming all faults during next reboot.
		if (num_written == 0) {
			unlink(rcfile);
		}

		break;

	default:
		ASSERT(0);
		break;
	}
	fflush(fp);

	return (true);
}
#endif // _USER

#else // _FAULT_INJECTION

#if defined(_KERNEL_ORB)
static int
main_unode(int, char *argv[])
#else
int
main(int, char *argv[])
#endif
{
	os::printf("%s: sorry, fault injection not supported\n", argv[0]);
	return (1);
}

#endif // _FAULT_INJECTION

#if defined(_KERNEL_ORB)
static os::mutex_t	mutex;

int
unode_init()
{
	os::printf("arm_faults module loaded\n");
	return (0);
}

//
// Entry point for unode-version of arm_faults.
//
int
arm_faults(int argc, char *argv[])
{
	int	ret;

	mutex.lock();	// allow only one thread at a time to arm/disarm faults
	ret = main_unode(argc, argv);
	mutex.unlock();

	return (ret);
}

//
// Entry point for unode-version of disarm_faults.
//
int
disarm_faults(int argc, char *argv[])
{
	int	ret;

	mutex.lock();	// allow only one thread at a time to arm/disarm faults
	ret = main_unode(argc, argv);
	mutex.unlock();

	return (ret);
}
#endif // _KERNEL_ORB
