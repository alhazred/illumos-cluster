/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)base_defs.cc	1.17	08/05/20 SMI"

#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include <orbtest/arm_faults/base_defs.h>

// ----------------------------------------------------------------------------
// arm_fault
//	Virtual base class for all subclasses representing faults.
// ----------------------------------------------------------------------------

// Hash table of all faults we know about.

arm_fault::table_t	arm_fault::table(16);		// with 16 buckets

// Set to true by main() if this program is executed from rc file.

bool	arm_fault::execed_from_rcfile = false;

arm_fault::arm_fault(
	uint_t		arg_fault_num,
	const char	*arg_name,
	default_t	arm_by_default)
	: _fault_num(arg_fault_num), _name(NULL), _optstr(NULL),
	    _next_opt(NULL), _selected(false), _default(arm_by_default),
	    _rcfile_line(NULL)
{
	// Make sure this fault has not been registered before.
	ASSERT(table.lookup(arg_name) == NULL);

	// Copy the passed name in case it's not persistent.
	_name = new char[strlen(arg_name) + 1];
	ASSERT(_name != NULL);
	(void) strcpy(_name, arg_name);

	// Register fault in the hash table.
	table.add(this, _name);
}

arm_fault::~arm_fault()
{
	(void) table.remove(_name);		// remove object from hash table
	delete [] _name;
	delete [] _rcfile_line;
	_optstr = NULL;				// to make lint happy
	_next_opt = NULL;			// to make lint happy
}

//
// Copy and store the line from the rc file.
//

void
arm_fault::store_rcfile_line(char *line)
{
	delete [] _rcfile_line;
	_rcfile_line = new char[strlen(line) + 1];
	ASSERT(_rcfile_line != NULL);
	(void) strcpy(_rcfile_line, line);
}

// The parse_opt() methods return true if the next option in optstr can be
// converted to the desired type (or if there is no more option), false
// if error occurs.

bool
arm_fault::parse_opt(const char *optstr, int &result)
{
	int	tempres;
	char	*endp, *comma;
	bool	retval = false;

	// Remember option string if different from the previously stored one.
	if (_optstr != optstr) {
		_optstr = _next_opt = optstr;
	}

	// If there is no more option...
	if (_next_opt == NULL || _next_opt[0] == '\0') {
		return (true);
	}

	// Find the ',' delimiter, if any.
	if ((comma = (char *)strchr(_next_opt, ',')) != NULL) {
		*comma = '\0';
	}

	errno = 0;
	tempres = (int)strtol(_next_opt, &endp, 10);
	if (errno) {
		os::printf("ERROR: %s: Can't convert option '%s': %s\n",
			name(), _next_opt, strerror(errno));
	} else if (*endp != '\0') {
		os::printf("ERROR: %s: Invalid int option '%s'\n",
			name(), _next_opt);
	} else {
		_next_opt = endp + (comma ? 1 : 0);	// point to next option
		result = tempres;			// return result
		retval = true;
	}

	if (comma) {
		*comma = ',';				// restore the ','
	}
	return (retval);
}

bool
arm_fault::parse_opt(const char *optstr, uint_t &result)
{
	int	tempres;
	char	*endp, *comma;
	bool	retval = false;

	// Remember option string if different from the previously stored one.
	if (_optstr != optstr) {
		_optstr = _next_opt = optstr;
	}

	// If there is no more option...
	if (_next_opt == NULL || _next_opt[0] == '\0') {
		return (true);
	}

	// Find the ',' delimiter, if any.
	if ((comma = (char *)strchr(_next_opt, ',')) != NULL) {
		*comma = '\0';
	}

	errno = 0;
	tempres = (int)strtol(_next_opt, &endp, 10);
	if (errno) {
		os::printf("ERROR: %s: Can't convert option '%s': %s\n",
			name(), _next_opt, strerror(errno));
	} else if (*endp != '\0') {
		os::printf("ERROR: %s: Invalid uint option '%s'\n",
			name(), _next_opt);
	} else if (tempres < 0) {
		os::printf("ERROR: %s: option '%s' must be >= 0\n",
			name(), _next_opt);
	} else {
		_next_opt = endp + (comma ? 1 : 0);	// point to next option
		result = (uint_t)tempres;		// return result
		retval = true;
	}

	if (comma) {
		*comma = ',';				// restore the ','
	}
	return (retval);
}

bool
arm_fault::parse_opt(const char *optstr, char *&result)
{
	char	*comma;
	size_t	optlen;

	// Remember option string if different from the previously stored one.
	if (_optstr != optstr) {
		_optstr = _next_opt = optstr;
	}

	// If there is no more option...
	if (_next_opt == NULL || _next_opt[0] == '\0') {
		return (true);
	}

	// Find the ',' delimiter, if any.
	if ((comma = (char *)strchr(_next_opt, ',')) != NULL) {
		*comma = '\0';
	}

	// Allocate space and copy option to be returned to caller.
	optlen = strlen(_next_opt);
	result = new char[optlen + 1];
	ASSERT(result != NULL);
	(void) strcpy(result, _next_opt);

	_next_opt += optlen + (comma ? 1 : 0);		// point to next option

	if (comma) {
		*comma = ',';				// restore the ','
	}
	return (true);
}

// ----------------------------------------------------------------------------
// sysex_fault
//	Base class for faults which set SystemException.
// ----------------------------------------------------------------------------

bool
sysex_fault::parse_options(const char *optstr)
{
	uint_t	cnt = count();				// current count

	// Option: how many messages before the exception is set.
	if (! parse_opt(optstr, cnt)) {
		return (false);
	}
	count(cnt);					// change count
	return (true);
}

bool
sysex_fault::write_arg(FILE *fp)
{
	// Write full argument.
	if (fprintf(fp, "%s:%d\n", name(), count()) < 0) {
		os::printf("ERROR: Can't write argument: %s\n",
			strerror(errno));
		return (false);
	}
	return (true);
}

void
sysex_fault::arm(int nodeid)
{
	FaultFunctions::node_trigger_add(fault_num(), sysex(),
			    (nodeid_t)nodeid, when(), count(), verbose());
}

void
sysex_fault::disarm(int nodeid)
{
	NodeTriggers::clear(fault_num(), nodeid);
}

// ----------------------------------------------------------------------------
// retry_unsent_fault
//	Base class for retry-unsent-message faults.
//	These faults set RETRY_NEEDED(0, COMPLETED_NO) system exception
//	at random time.
// ----------------------------------------------------------------------------

bool
retry_unsent_fault::parse_options(const char *optstr)
{
	if (! sysex_fault::parse_options(optstr)) {
		return (false);
	}

	// Prevent infinite retries.
	if (count() < 2) {
		os::printf("ERROR: %s: option must be > 1\n", name());
		return (false);
	}

	return (true);
}

// ----------------------------------------------------------------------------
// retry_sent_fault
//	Base class for retry-sent-message faults.
//	These faults set RETRY_NEEDED(0, COMPLETED_MAYBE) system exception
//	at random time.
// ----------------------------------------------------------------------------

bool
retry_sent_fault::parse_options(const char *optstr)
{
	if (! sysex_fault::parse_options(optstr)) {
		return (false);
	}

	// Prevent infinite retries.
	if (count() < 2) {
		os::printf("ERROR: %s: option must be > 1\n", name());
		return (false);
	}

	return (true);
}

// ----------------------------------------------------------------------------
// vanilla_fault
//	Base class for simple fault points with one option
// ----------------------------------------------------------------------------

vanilla_fault::vanilla_fault(uint_t arg_fault_num, const char *fault_name,
    default_t arm_by_default, uint32_t default_opt) :
	arm_fault(arg_fault_num, fault_name, arm_by_default),
	_opt(default_opt)
{
}

bool
vanilla_fault::parse_options(const char *optstr)
{
	uint_t	opt = _opt;				// current option

	// single uint32_t option
	if (! parse_opt(optstr, opt)) {
		return (false);
	}
	_opt = opt;					// change option
	return (true);
}

bool
vanilla_fault::write_arg(FILE *fp)
{
	// Write full argument.
	if (fprintf(fp, "%s:%d\n", name(), _opt) < 0) {
		os::printf("ERROR: Can't write argument: %s\n",
			strerror(errno));
		return (false);
	}
	return (true);
}

void
vanilla_fault::arm(int nodeid)
{
	FaultFunctions::counter_t fault_arg(FaultFunctions::RANDOM, _opt);
	NodeTriggers::add(fault_num(), &fault_arg, (uint_t)sizeof (fault_arg),
			nodeid);
}

void
vanilla_fault::disarm(int nodeid)
{
	NodeTriggers::clear(fault_num(), nodeid);
}
