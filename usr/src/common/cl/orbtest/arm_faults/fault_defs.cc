/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fault_defs.cc	1.21	08/05/20 SMI"

#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include <orb/infrastructure/orb_conf.h>

#include <orbtest/arm_faults/fault_defs.h>

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_UNSENT_INVO
//
//	Force exception RETRY_NEEDED(0, COMPLETED_NO) on random
//	INVO_MSG BEFORE it is sent to the receiver.
// ----------------------------------------------------------------------------
const uint_t	retry_unsent_invo::default_mtbf = 20;

void
retry_unsent_invo::usage()
{
	os::printf(
"   %s[:<mtbf>]\n"
"      Force exception RETRY_NEEDED(0, COMPLETED_NO) on random INVO_MSG\n"
"      BEFORE it is sent to the receiver.\n"
"      Options:\n"
"         <mtbf> -- mean time between failure (default: %d)\n"
"      Armed/disarmed by default: %s\n",
		name(), default_mtbf, default_yesno());
}

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_SENT_INVO
//
//	Force exception RETRY_NEEDED(0, COMPLETED_MAYBE) on random INVO_MSG
//	AFTER it is sent to the receiver.
// ----------------------------------------------------------------------------
const uint_t	retry_sent_invo::default_mtbf = 20;

void
retry_sent_invo::usage()
{
	os::printf(
"   %s[:<mtbf>]\n"
"      Force exception RETRY_NEEDED(0, COMPLETED_MAYBE) on random INVO_MSG\n"
"      AFTER it is sent to the receiver.\n"
"      Options:\n"
"         <mtbf> -- mean time between failure (default: %d)\n"
"      Armed/disarmed by default: %s\n",
		name(), default_mtbf, default_yesno());
}

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_UNSENT_REPLY
//
//	Force exception RETRY_NEEDED(0, COMPLETED_NO) on random
//	REPLY_MSG BEFORE it is sent to the receiver.
// ----------------------------------------------------------------------------
const uint_t	retry_unsent_reply::default_mtbf = 20;

void
retry_unsent_reply::usage()
{
	os::printf(
"   %s[:<mtbf>]\n"
"      Force exception RETRY_NEEDED(0, COMPLETED_NO) on random REPLY_MSG\n"
"      BEFORE it is sent to the receiver.\n"
"      Options:\n"
"         <mtbf> -- mean time between failure (default: %d)\n"
"      Armed/disarmed by default: %s\n",
		name(), default_mtbf, default_yesno());
}


// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_SENT_REPLY
//
//	Force exception RETRY_NEEDED(0, COMPLETED_MAYBE) on random REPLY_MSG
//	AFTER it is sent to the receiver.
// ----------------------------------------------------------------------------
const uint_t	retry_sent_reply::default_mtbf = 20;

void
retry_sent_reply::usage()
{
	os::printf(
"   %s[:<mtbf>]\n"
"      Force exception RETRY_NEEDED(0, COMPLETED_MAYBE)\n"
"      on random REPLY_MSG AFTER it is sent to the receiver.\n"
"      Options:\n"
"         <mtbf> -- mean time between failure (default: %d)\n"
"      Armed/disarmed by default: %s\n",
		name(), default_mtbf, default_yesno());
}


// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_UNSENT_REFJOB
//
//	Force exception RETRY_NEEDED(0, COMPLETED_NO) on random
//	REFJOB_MSG BEFORE it is sent to the receiver.
// ----------------------------------------------------------------------------
const uint_t	retry_unsent_refjob::default_mtbf = 20;

void
retry_unsent_refjob::usage()
{
	os::printf(
"   %s[:<mtbf>]\n"
"      Force exception RETRY_NEEDED(0, COMPLETED_NO) on random REFJOB_MSG\n"
"      BEFORE it is sent to the receiver.\n"
"      Options:\n"
"         <mtbf> -- mean time between failure (default: %d)\n"
"      Armed/disarmed by default: %s\n",
		name(), default_mtbf, default_yesno());
}

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_SENT_REFJOB
//
//	Force exception RETRY_NEEDED(0, COMPLETED_MAYBE) on random REFJOB_MSG
//	AFTER it is sent to the receiver.
// ----------------------------------------------------------------------------
const uint_t	retry_sent_refjob::default_mtbf = 20;

void
retry_sent_refjob::usage()
{
	os::printf(
"   %s[:<mtbf>]\n"
"      Force exception RETRY_NEEDED(0, COMPLETED_MAYBE) on random REFJOB_MSG\n"
"      AFTER it is sent to the receiver.\n"
"      Options:\n"
"         <mtbf> -- mean time between failure (default: %d)\n"
"      Armed/disarmed by default: %s\n",
		name(), default_mtbf, default_yesno());
}

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_UNSENT_CMM
//
//	Force exception RETRY_NEEDED(0, COMPLETED_NO) on random
//	CMM_MSG BEFORE it is sent to the receiver.
// ----------------------------------------------------------------------------
const uint_t	retry_unsent_cmm::default_mtbf = 20;

void
retry_unsent_cmm::usage()
{
	os::printf(
"   %s[:<mtbf>]\n"
"      Force exception RETRY_NEEDED(0, COMPLETED_NO) on random CMM_MSG\n"
"      BEFORE it is sent to the receiver.\n"
"      Options:\n"
"         <mtbf> -- mean time between failure (default: %d)\n"
"      Armed/disarmed by default: %s\n",
		name(), default_mtbf, default_yesno());
}

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_SENT_CMM
//
//	Force exception RETRY_NEEDED(0, COMPLETED_MAYBE) on random CMM_MSG
//	AFTER it is sent to the receiver.
// ----------------------------------------------------------------------------
const uint_t	retry_sent_cmm::default_mtbf = 20;

void
retry_sent_cmm::usage()
{
	os::printf(
"   %s[:<mtbf>]\n"
"      Force exception RETRY_NEEDED(0, COMPLETED_MAYBE) on random CMM_MSG\n"
"      AFTER it is sent to the receiver.\n"
"      Options:\n"
"         <mtbf> -- mean time between failure (default: %d)\n"
"      Armed/disarmed by default: %s\n",
		name(), default_mtbf, default_yesno());
}

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_UNSENT_SIGNAL
//
//	Force exception RETRY_NEEDED(0, COMPLETED_NO) on random
//	SIGNAL_MSG BEFORE it is sent to the receiver.
// ----------------------------------------------------------------------------
const uint_t	retry_unsent_signal::default_mtbf = 20;

void
retry_unsent_signal::usage()
{
	os::printf(
"   %s[:<mtbf>]\n"
"      Force exception RETRY_NEEDED(0, COMPLETED_NO) on random SIGNAL_MSG\n"
"      BEFORE it is sent to the receiver.\n"
"      Options:\n"
"         <mtbf> -- mean time between failure (default: %d)\n"
"      Armed/disarmed by default: %s\n",
		name(), default_mtbf, default_yesno());
}

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_SENT_SIGNAL
//
//	Force exception RETRY_NEEDED(0, COMPLETED_MAYBE) on random SIGNAL_MSG
//	AFTER it is sent to the receiver.
// ----------------------------------------------------------------------------
const uint_t	retry_sent_signal::default_mtbf = 20;

void
retry_sent_signal::usage()
{
	os::printf(
"   %s[:<mtbf>]\n"
"      Force exception RETRY_NEEDED(0, COMPLETED_MAYBE) on random SIGNAL_MSG\n"
"      AFTER it is sent to the receiver.\n"
"      Options:\n"
"         <mtbf> -- mean time between failure (default: %d)\n"
"      Armed/disarmed by default: %s\n",
		name(), default_mtbf, default_yesno());
}

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_UNSENT_CKPT
//
//	Force exception RETRY_NEEDED(0, COMPLETED_NO) on random
//	CKPT_MSG BEFORE it is sent to the receiver.
// ----------------------------------------------------------------------------
const uint_t	retry_unsent_ckpt::default_mtbf = 20;

void
retry_unsent_ckpt::usage()
{
	os::printf(
"   %s[:<mtbf>]\n"
"      Force exception RETRY_NEEDED(0, COMPLETED_NO) on random CKPT_MSG\n"
"      BEFORE it is sent to the receiver.\n"
"      Options:\n"
"         <mtbf> -- mean time between failure (default: %d)\n"
"      Armed/disarmed by default: %s\n",
		name(), default_mtbf, default_yesno());
}

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_SENT_CKPT
//
//	Force exception RETRY_NEEDED(0, COMPLETED_MAYBE) on random CKPT_MSG
//	AFTER it is sent to the receiver.
// ----------------------------------------------------------------------------
const uint_t	retry_sent_ckpt::default_mtbf = 20;

void
retry_sent_ckpt::usage()
{
	os::printf(
"   %s[:<mtbf>]\n"
"      Force exception RETRY_NEEDED(0, COMPLETED_MAYBE) on random CKPT_MSG\n"
"      AFTER it is sent to the receiver.\n"
"      Options:\n"
"         <mtbf> -- mean time between failure (default: %d)\n"
"      Armed/disarmed by default: %s\n",
		name(), default_mtbf, default_yesno());
}

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_UNSENT_FLOW
//
//	Force exception RETRY_NEEDED(0, COMPLETED_NO) on random
//	FLOW_MSG BEFORE it is sent to the receiver.
// ----------------------------------------------------------------------------
const uint_t	retry_unsent_flow::default_mtbf = 20;

void
retry_unsent_flow::usage()
{
	os::printf(
"   %s[:<mtbf>]\n"
"      Force exception RETRY_NEEDED(0, COMPLETED_NO) on random FLOW_MSG\n"
"      BEFORE it is sent to the receiver.\n"
"      Options:\n"
"         <mtbf> -- mean time between failure (default: %d)\n"
"      Armed/disarmed by default: %s\n",
		name(), default_mtbf, default_yesno());
}

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_SENT_FLOW
//
//	Force exception RETRY_NEEDED(0, COMPLETED_MAYBE) on random FLOW_MSG
//	AFTER it is sent to the receiver.
// ----------------------------------------------------------------------------
const uint_t	retry_sent_flow::default_mtbf = 20;

void
retry_sent_flow::usage()
{
	os::printf(
"   %s[:<mtbf>]\n"
"      Force exception RETRY_NEEDED(0, COMPLETED_MAYBE) on random FLOW_MSG\n"
"      AFTER it is sent to the receiver.\n"
"      Options:\n"
"         <mtbf> -- mean time between failure (default: %d)\n"
"      Armed/disarmed by default: %s\n",
		name(), default_mtbf, default_yesno());
}

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_UNSENT_ONEWAY
//
//	Force exception RETRY_NEEDED(0, COMPLETED_NO) on random
//	INVO_ONEWAY_MSG BEFORE it is sent to the receiver.
// ----------------------------------------------------------------------------
const uint_t	retry_unsent_oneway::default_mtbf = 20;

void
retry_unsent_oneway::usage()
{
	os::printf(
"   %s[:<mtbf>]\n"
"      Force exception RETRY_NEEDED(0, COMPLETED_NO) on random\n"
"      INVO_ONEWAY_MSG BEFORE it is sent to the receiver.\n"
"      Options:\n"
"         <mtbf> -- mean time between failure (default: %d)\n"
"      Armed/disarmed by default: %s\n",
		name(), default_mtbf, default_yesno());
}

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_SENT_ONEWAY
//
//	Force exception RETRY_NEEDED(0, COMPLETED_MAYBE) on random
//	INVO_ONEWAY_MSG AFTER it is sent to the receiver.
// ----------------------------------------------------------------------------
const uint_t	retry_sent_oneway::default_mtbf = 20;

void
retry_sent_oneway::usage()
{
	os::printf(
"   %s[:<mtbf>]\n"
"      Force exception RETRY_NEEDED(0, COMPLETED_MAYBE) on random\n"
"      INVO_ONEWAY_MSG AFTER it is sent to the receiver.\n"
"      Options:\n"
"         <mtbf> -- mean time between failure (default: %d)\n"
"      Armed/disarmed by default: %s\n",
		name(), default_mtbf, default_yesno());
}

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_UNSENT_FAULT
//
//	Force exception RETRY_NEEDED(0, COMPLETED_NO) on random
//	FAULT_MSG BEFORE it is sent to the receiver.
// ----------------------------------------------------------------------------
const uint_t	retry_unsent_faultmsg::default_mtbf = 20;

void
retry_unsent_faultmsg::usage()
{
	os::printf(
"   %s[:<mtbf>]\n"
"      Force exception RETRY_NEEDED(0, COMPLETED_NO) on random FAULT_MSG\n"
"      BEFORE it is sent to the receiver.\n"
"      Options:\n"
"         <mtbf> -- mean time between failure (default: %d)\n"
"      Armed/disarmed by default: %s\n",
		name(), default_mtbf, default_yesno());
}

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_SENT_FAULT
//
//	Force exception RETRY_NEEDED(0, COMPLETED_MAYBE) on random FAULT_MSG
//	AFTER it is sent to the receiver.
// ----------------------------------------------------------------------------
const uint_t	retry_sent_faultmsg::default_mtbf = 20;

void
retry_sent_faultmsg::usage()
{
	os::printf(
"   %s[:<mtbf>]\n"
"      Force exception RETRY_NEEDED(0, COMPLETED_MAYBE) on random FAULT_MSG\n"
"      AFTER it is sent to the receiver.\n"
"      Options:\n"
"         <mtbf> -- mean time between failure (default: %d)\n"
"      Armed/disarmed by default: %s\n",
		name(), default_mtbf, default_yesno());
}

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_UNSENT_MAYBE
//
//	Force exception RETRY_NEEDED(0, COMPLETED_NO) on random
//	MAYBE_MSG BEFORE it is sent to the receiver.
// ----------------------------------------------------------------------------
const uint_t	retry_unsent_maybemsg::default_mtbf = 20;

void
retry_unsent_maybemsg::usage()
{
	os::printf(
"   %s[:<mtbf>]\n"
"      Force exception RETRY_NEEDED(0, COMPLETED_NO) on random MAYBE_MSG\n"
"      BEFORE it is sent to the receiver.\n"
"      Options:\n"
"         <mtbf> -- mean time between failure (default: %d)\n"
"      Armed/disarmed by default: %s\n",
		name(), default_mtbf, default_yesno());
}

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_SENT_MAYBE
//
//	Force exception RETRY_NEEDED(0, COMPLETED_MAYBE) on random MAYBE_MSG
//	AFTER it is sent to the receiver.
// ----------------------------------------------------------------------------
const uint_t	retry_sent_maybemsg::default_mtbf = 20;

void
retry_sent_maybemsg::usage()
{
	os::printf(
"   %s[:<mtbf>]\n"
"      Force exception RETRY_NEEDED(0, COMPLETED_MAYBE) on random MAYBE_MSG\n"
"      AFTER it is sent to the receiver.\n"
"      Options:\n"
"         <mtbf> -- mean time between failure (default: %d)\n"
"      Armed/disarmed by default: %s\n",
		name(), default_mtbf, default_yesno());
}

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_UNSENT_VM
//
//	Force exception RETRY_NEEDED(0, COMPLETED_NO) on random
//	VM_MSG BEFORE it is sent to the receiver.
// ----------------------------------------------------------------------------
const uint_t	retry_unsent_vmmsg::default_mtbf = 20;

void
retry_unsent_vmmsg::usage()
{
	os::printf(
"   %s[:<mtbf>]\n"
"      Force exception RETRY_NEEDED(0, COMPLETED_NO) on random VM_MSG\n"
"      BEFORE it is sent to the receiver.\n"
"      Options:\n"
"         <mtbf> -- mean time between failure (default: %d)\n"
"      Armed/disarmed by default: %s\n",
		name(), default_mtbf, default_yesno());
}

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_SENT_VM
//
//	Force exception RETRY_NEEDED(0, COMPLETED_MAYBE) on random VM_MSG
//	AFTER it is sent to the receiver.
// ----------------------------------------------------------------------------
const uint_t	retry_sent_vmmsg::default_mtbf = 20;

void
retry_sent_vmmsg::usage()
{
	os::printf(
"   %s[:<mtbf>]\n"
"      Force exception RETRY_NEEDED(0, COMPLETED_MAYBE) on random VM_MSG\n"
"      AFTER it is sent to the receiver.\n"
"      Options:\n"
"         <mtbf> -- mean time between failure (default: %d)\n"
"      Armed/disarmed by default: %s\n",
		name(), default_mtbf, default_yesno());
}

// ----------------------------------------------------------------------------
// FAULTNUM_TICK_DELAY_PANIC
//
//	Trigger panic if heartbeat ticks (clock, send, receive) are delayed
// ----------------------------------------------------------------------------

// by default 20% tolerance
const uint32_t	tick_delay_panic::default_max_percent_delay = 20;

void
tick_delay_panic::usage()
{
	os::printf(
		"   %s[:<max_percent_delay>]\n"
		"      Panic if any of the ticks is delayed by more than\n"
		"      <max_percent_delay> * {10 (clock), 1 (send), 1 (recv)\n"
		"      Options:\n"
		"        <max_percent_delay> -- max delay in %% of tick len.\n"
		"                                  default: %u%%\n"
		"      Armed/disarmed by default: %s\n",
		name(), _opt, default_yesno());
}

void
tick_delay_panic::arm(int nodeid)
{
	NodeTriggers::add(fault_num(), &_opt, (uint_t)sizeof (_opt), nodeid);
}

// ----------------------------------------------------------------------------
// FAULTNUM_TRANSPORT_PATHEND_FAIL
//
//	Force pathend::fail() at random time.
// ----------------------------------------------------------------------------
const uint_t	pathend_fail::default_mtbf = 23;

void
pathend_fail::usage()
{
	os::printf(
"   %s[:<mtbf>]\n"
"      Force pathend::fail() at random time.\n"
"      Options:\n"
"         <mtbf> -- mean time between failure (default: %d)\n"
"      Armed/disarmed by default: %s\n",
		name(), default_mtbf, default_yesno());
}

// ----------------------------------------------------------------------------
// FAULTNUM_TRANSPORT_STOP_PM_SEND
//
//	Stop calls to pathend::pm_send_internal() at random time.
// ----------------------------------------------------------------------------
const uint_t	stop_pm_send::default_mtbf = 20;

void
stop_pm_send::usage()
{
	os::printf(
"   %s[:<mtbf>]\n"
"      Stop calls to pathend::pm_send_internal() at random time until\n"
"      Options:\n"
"         <mtbf> -- mean time between failure (default: %d)\n"
"      Armed/disarmed by default: %s\n",
		name(), default_mtbf, default_yesno());
}

// ----------------------------------------------------------------------------
// FAULTNUM_PM_PANIC_IN_PATH_DOWN
//
//	Causes current node to panic (in path_record::drop_pathend()) if
//	the path to specific node(s) go down.
// ----------------------------------------------------------------------------
void
path_down_panic::usage()
{
	os::printf(
"   %1$s\n"
"   %1$s:any\n"
"   %1$s:<nodeid>[,<nodeid>...]\n"
"      Panic current node when a path to one of the specified nodes goes down\n"
"      Options:\n"
"         any\n"
"             Panic when *any* path goes down.\n"
"         <nodeid>[,<nodeid> ...]\n"
"             Panic when a path to one of the specified nodes goes down.\n"
"             If <nodeid> is negative, it means any node EXCEPT that node.\n"
"      If no option is specified, \"any\" is assumed.\n"
"      Armed/disarmed by default: %2$s\n",
		name(), default_yesno());
}

bool
path_down_panic::parse_options(const char *optstr)
{
	// If no option is specified, or if it's equal to "any",
	// then set fault point to cause panic when any path goes down.
	if (optstr == NULL || optstr[0] == '\0' || strcmp(optstr, "any") == 0) {
		_reverse_mask = 0xFFFFFFFFFFFFFFFFULL;	// set all bits
		return (true);
	}

	// For accumulating positive and negative nodeid's.
	uint64_t	positive_mask = 0;
	uint64_t	negative_mask = 0;

	// Parse the comma-separated list of nodeid's.
	do {
		int	nid = 0;
		int	abs_nid;

		if (! parse_opt(optstr, nid)) {
			return (false);		// error occured
		}

		abs_nid = abs(nid);

		// Verify node id.
		if (abs_nid < 1 || abs_nid > NODEID_MAX) {
			os::printf("ERROR: %s: Invalid node ID: %d\n",
				name(), abs_nid);
			return (false);
		}

		// Just in case...
		if (abs_nid > (int)(sizeof (_reverse_mask) * 8)) {
			os::printf("INTERNAL ERROR: Node ID %d "
				"> sizeof(reboot_mask)\n", abs_nid);
			return (false);
		}

		// Print warning if node isn't configured (unless this program
		// is executed from an rc file.)
		if (! orb_conf::node_configured((nodeid_t)abs_nid) &&
		    ! execed_from_rcfile) {
			os::warning("%s: Node ID %d is not configured\n",
				name(), abs_nid);
		}

		// Set the bit corresponding to the node id.
		if (nid > 0) {
			positive_mask |= (1ULL << (abs_nid - 1));
		} else {
			negative_mask |= (1ULL << (abs_nid - 1));
		}
	} while (is_more_options());

	// If there were no negative nodeid's (a negative nodeid means all
	// nodes EXCEPT that nodeid), then we'll simply set our reverse mask
	// to the positive mask.  Otherwise, we'll combine the positive
	// mask and the reverse of the negative mask.
	if (negative_mask == 0) {
		_reverse_mask = positive_mask;
	} else {
		_reverse_mask = positive_mask | ~negative_mask;
	}

	return (true);
}

bool
path_down_panic::write_arg(FILE *fp)
{
	if (fprintf(fp, name()) < 0) {
		os::printf("ERROR: Can't write argument: %s\n",
			strerror(errno));
		return (false);
	}

	if (_reverse_mask == 0xFFFFFFFFFFFFULL) {
		(void) fprintf(fp, ":any\n");
	} else {
		uint64_t	mask;
		nodeid_t	n;
		bool		first = true;

		for (mask = _reverse_mask, n = 1; mask != 0; mask >>= 1, ++n) {
			if (mask & 1) {
				(void) fprintf(fp, "%c%d",
					    (first ? ':' : ','), n);
				first = false;
			}
		}
		(void) fprintf(fp, "\n");
	}

	return (true);
}

void
path_down_panic::arm(int nodeid)
{
	uint64_t	fault_arg;

	// NOTE:
	//	The FAULTNUM_PM_PANIC_IN_PATH_DOWN fault point in
	//	path_record::drop_pathend() interprets a zero bit in the
	//	reboot mask (fault argument) to mean panic the current node
	//	if the path to the node corresponding to the bit position
	//	goes down.  If the bit is set, no panic is done.
	//
	//	However, to make bit calculation easier we did the opposite
	//	here.  We simply pass ~(_reverse_mask) as the fault argument.
	//
	fault_arg = ~(_reverse_mask);

	NodeTriggers::add(fault_num(), &fault_arg, (uint_t)sizeof (fault_arg),
			    nodeid);
}

void
path_down_panic::disarm(int nodeid)
{
	NodeTriggers::clear(fault_num(), nodeid);
}

// ----------------------------------------------------------------------------
// FAULTNUM_TRANSPORT_DELAY_SEND_THREAD
//
//	simulates a delayed send thread for the heartbeat messages
// ----------------------------------------------------------------------------
const uint_t	delay_send_thread::default_mtbf = 20;

void
delay_send_thread::usage()
{
	os::printf("   %s[:<mtbf>]\n"
	    "      Simulate a delayed heartbeat send thread\n"
	    "      Options:\n"
	    "         <mtbf> -- mean time between failure (default: %d)\n"
	    "      Armed/disarmed by default: %s\n",
		name(), default_mtbf, default_yesno());
}

// ----------------------------------------------------------------------------
// FAULTNUM_PM_DELAY_CLOCK_THREAD
//
//	simulates a delayed clock thread entry into the path manager
// ----------------------------------------------------------------------------
const uint_t	delay_clock_thread::default_mtbf = 20;

void
delay_clock_thread::usage()
{
	os::printf("   %s[:<mtbf>]\n"
	    "      Simulate a delayed clock thread\n"
	    "      Options:\n"
	    "         <mtbf> -- mean time between failure (default: %d)\n"
	    "      Armed/disarmed by default: %s\n",
		name(), default_mtbf, default_yesno());
}
