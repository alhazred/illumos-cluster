/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FAULT_DEFS_H
#define	_FAULT_DEFS_H

#pragma ident	"@(#)fault_defs.h	1.19	08/05/20 SMI"

#include <sys/types.h>
#include <sys/os.h>
#include <orb/invo/common.h>
#include <orb/fault/fault_injection.h>

#include <orbtest/arm_faults/base_defs.h>

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_UNSENT_INVO
//
//	Force exception RETRY_NEEDED(0, COMPLETED_NO) on random INVO_MSG
//	BEFORE it is sent to the receiver.
// ----------------------------------------------------------------------------
class retry_unsent_invo : public retry_unsent_fault {
public:
	retry_unsent_invo(default_t arm_by_default = DEFAULT)
		: retry_unsent_fault(FAULTNUM_ORB_RETRY_UNSENT_INVO,
			"retry_unsent_invo", arm_by_default, default_mtbf) {}
	void	usage();

	static const uint_t	default_mtbf;
};

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_SENT_INVO
//
//	Force exception RETRY_NEEDED(0, COMPLETED_MAYBE) on random INVO_MSG
//	AFTER it is sent to the receiver.
// ----------------------------------------------------------------------------
class retry_sent_invo : public retry_sent_fault {
public:
	retry_sent_invo(default_t arm_by_default = DEFAULT)
		: retry_sent_fault(FAULTNUM_ORB_RETRY_SENT_INVO,
			"retry_sent_invo", arm_by_default, default_mtbf) {}
	void	usage();

	static const uint_t	default_mtbf;
};

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_UNSENT_REPLY
//
//	Force exception RETRY_NEEDED(0, COMPLETED_NO) on random REPLY_MSG
//	BEFORE it is sent to the receiver.
// ----------------------------------------------------------------------------
class retry_unsent_reply : public retry_unsent_fault {
public:
	retry_unsent_reply(default_t arm_by_default = DEFAULT)
		: retry_unsent_fault(FAULTNUM_ORB_RETRY_UNSENT_REPLY,
			"retry_unsent_reply", arm_by_default, default_mtbf) {}
	void	usage();

	static const uint_t	default_mtbf;
};

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_SENT_REPLY
//
//	Force exception RETRY_NEEDED(0, COMPLETED_MAYBE) on random REPLY_MSG
//	AFTER it is sent to the receiver.
// ----------------------------------------------------------------------------
class retry_sent_reply : public retry_sent_fault {
public:
	retry_sent_reply(default_t arm_by_default = DEFAULT)
		: retry_sent_fault(FAULTNUM_ORB_RETRY_SENT_REPLY,
			"retry_sent_reply", arm_by_default, default_mtbf) {}
	void	usage();

	static const uint_t	default_mtbf;
};

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_UNSENT_REFJOB
//
//	Force exception RETRY_NEEDED(0, COMPLETED_NO) on random REFJOB_MSG
//	BEFORE it is sent to the receiver.
// ----------------------------------------------------------------------------
class retry_unsent_refjob : public retry_unsent_fault {
public:
	retry_unsent_refjob(default_t arm_by_default = DEFAULT)
		: retry_unsent_fault(FAULTNUM_ORB_RETRY_UNSENT_REFJOB,
			"retry_unsent_refjob", arm_by_default, default_mtbf)
		{}
	void	usage();

	static const uint_t	default_mtbf;
};

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_SENT_REFJOB
//
//	Force exception RETRY_NEEDED(0, COMPLETED_MAYBE) on random REFJOB_MSG
//	AFTER it is sent to the receiver.
// ----------------------------------------------------------------------------
class retry_sent_refjob : public retry_sent_fault {
public:
	retry_sent_refjob(default_t arm_by_default = DEFAULT)
		: retry_sent_fault(FAULTNUM_ORB_RETRY_SENT_REFJOB,
			"retry_sent_refjob", arm_by_default, default_mtbf) {}
	void	usage();

	static const uint_t	default_mtbf;
};

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_UNSENT_CMM
//
//	Force exception RETRY_NEEDED(0, COMPLETED_NO) on random CMM_MSG
//	BEFORE it is sent to the receiver.
// ----------------------------------------------------------------------------
class retry_unsent_cmm : public retry_unsent_fault {
public:
	retry_unsent_cmm(default_t arm_by_default = DEFAULT)
		: retry_unsent_fault(FAULTNUM_ORB_RETRY_UNSENT_CMM,
			"retry_unsent_cmm", arm_by_default, default_mtbf) {}
	void	usage();

	static const uint_t	default_mtbf;
};

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_SENT_CMM
//
//	Force exception RETRY_NEEDED(0, COMPLETED_MAYBE) on random CMM_MSG
//	AFTER it is sent to the receiver.
// ----------------------------------------------------------------------------
class retry_sent_cmm : public retry_sent_fault {
public:
	retry_sent_cmm(default_t arm_by_default = DEFAULT)
		: retry_sent_fault(FAULTNUM_ORB_RETRY_SENT_CMM,
			"retry_sent_cmm", arm_by_default, default_mtbf) {}
	void	usage();

	static const uint_t	default_mtbf;
};

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_UNSENT_SIGNAL
//
//	Force exception RETRY_NEEDED(0, COMPLETED_NO) on random SIGNAL_MSG
//	BEFORE it is sent to the receiver.
// ----------------------------------------------------------------------------
class retry_unsent_signal : public retry_unsent_fault {
public:
	retry_unsent_signal(default_t arm_by_default = DEFAULT)
		: retry_unsent_fault(FAULTNUM_ORB_RETRY_UNSENT_SIGNAL,
			"retry_unsent_signal", arm_by_default, default_mtbf)
		{}
	void	usage();

	static const uint_t	default_mtbf;
};

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_SENT_SIGNAL
//
//	Force exception RETRY_NEEDED(0, COMPLETED_MAYBE) on random SIGNAL_MSG
//	AFTER it is sent to the receiver.
// ----------------------------------------------------------------------------
class retry_sent_signal : public retry_sent_fault {
public:
	retry_sent_signal(default_t arm_by_default = DEFAULT)
		: retry_sent_fault(FAULTNUM_ORB_RETRY_SENT_SIGNAL,
			"retry_sent_signal", arm_by_default, default_mtbf) {}
	void	usage();

	static const uint_t	default_mtbf;
};


// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_UNSENT_CKPT
//
//	Force exception RETRY_NEEDED(0, COMPLETED_NO) on random CKPT_MSG
//	BEFORE it is sent to the receiver.
// ----------------------------------------------------------------------------
class retry_unsent_ckpt : public retry_unsent_fault {
public:
	retry_unsent_ckpt(default_t arm_by_default = DEFAULT)
		: retry_unsent_fault(FAULTNUM_ORB_RETRY_UNSENT_CKPT,
			"retry_unsent_ckpt", arm_by_default, default_mtbf) {}
	void	usage();

	static const uint_t	default_mtbf;
};

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_SENT_CKPT
//
//	Force exception RETRY_NEEDED(0, COMPLETED_MAYBE) on random CKPT_MSG
//	AFTER it is sent to the receiver.
// ----------------------------------------------------------------------------
class retry_sent_ckpt : public retry_sent_fault {
public:
	retry_sent_ckpt(default_t arm_by_default = DEFAULT)
		: retry_sent_fault(FAULTNUM_ORB_RETRY_SENT_CKPT,
			"retry_sent_ckpt", arm_by_default, default_mtbf) {}
	void	usage();

	static const uint_t	default_mtbf;
};

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_UNSENT_FLOW
//
//	Force exception RETRY_NEEDED(0, COMPLETED_NO) on random FLOW_MSG
//	BEFORE it is sent to the receiver.
// ----------------------------------------------------------------------------
class retry_unsent_flow : public retry_unsent_fault {
public:
	retry_unsent_flow(default_t arm_by_default = DEFAULT)
		: retry_unsent_fault(FAULTNUM_ORB_RETRY_UNSENT_FLOW,
			"retry_unsent_flow", arm_by_default, default_mtbf) {}
	void	usage();

	static const uint_t	default_mtbf;
};

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_SENT_FLOW
//
//	Force exception RETRY_NEEDED(0, COMPLETED_MAYBE) on random FLOW_MSG
//	AFTER it is sent to the receiver.
// ----------------------------------------------------------------------------
class retry_sent_flow : public retry_sent_fault {
public:
	retry_sent_flow(default_t arm_by_default = DEFAULT)
		: retry_sent_fault(FAULTNUM_ORB_RETRY_SENT_FLOW,
			"retry_sent_flow", arm_by_default, default_mtbf) {}
	void	usage();

	static const uint_t	default_mtbf;
};

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_UNSENT_ONEWAY
//
//	Force exception RETRY_NEEDED(0, COMPLETED_NO) on random
//	INVO_ONEWAY_MSG BEFORE it is sent to the receiver.
// ----------------------------------------------------------------------------
class retry_unsent_oneway : public retry_unsent_fault {
public:
	retry_unsent_oneway(default_t arm_by_default = DEFAULT)
		: retry_unsent_fault(FAULTNUM_ORB_RETRY_UNSENT_ONEWAY,
			"retry_unsent_oneway", arm_by_default, default_mtbf)
		{}
	void	usage();

	static const uint_t	default_mtbf;
};

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_SENT_ONEWAY
//
//	Force exception RETRY_NEEDED(0, COMPLETED_MAYBE) on random
//	INVO_ONEWAY_MSG AFTER it is sent to the receiver.
// ----------------------------------------------------------------------------
class retry_sent_oneway : public retry_sent_fault {
public:
	retry_sent_oneway(default_t arm_by_default = DEFAULT)
		: retry_sent_fault(FAULTNUM_ORB_RETRY_SENT_ONEWAY,
			"retry_sent_oneway", arm_by_default, default_mtbf) {}
	void	usage();

	static const uint_t	default_mtbf;
};

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_UNSENT_FAULT
//
//	Force exception RETRY_NEEDED(0, COMPLETED_NO) on random FAULT_MSG
//	BEFORE it is sent to the receiver.
// ----------------------------------------------------------------------------
class retry_unsent_faultmsg : public retry_unsent_fault {
public:
	retry_unsent_faultmsg(default_t arm_by_default = DEFAULT)
		: retry_unsent_fault(FAULTNUM_ORB_RETRY_UNSENT_FAULT,
			"retry_unsent_faultmsg", arm_by_default,
			default_mtbf)
		{}
	void	usage();

	static const uint_t	default_mtbf;
};

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_SENT_FAULT
//
//	Force exception RETRY_NEEDED(0, COMPLETED_MAYBE) on random FAULT_MSG
//	AFTER it is sent to the receiver.
// ----------------------------------------------------------------------------
class retry_sent_faultmsg : public retry_sent_fault {
public:
	retry_sent_faultmsg(default_t arm_by_default = DEFAULT)
		: retry_sent_fault(FAULTNUM_ORB_RETRY_SENT_FAULT,
			"retry_sent_faultmsg", arm_by_default, default_mtbf)
		{}
	void	usage();

	static const uint_t	default_mtbf;
};

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_UNSENT_MAYBE
//
//	Force exception RETRY_NEEDED(0, COMPLETED_NO) on random FAULT_MSG
//	BEFORE it is sent to the receiver.
// ----------------------------------------------------------------------------
class retry_unsent_maybemsg : public retry_unsent_fault {
public:
	retry_unsent_maybemsg(default_t arm_by_default = DEFAULT)
		: retry_unsent_fault(FAULTNUM_ORB_RETRY_UNSENT_MAYBE,
			"retry_unsent_maybemsg", arm_by_default,
			default_mtbf)
		{}
	void	usage();

	static const uint_t	default_mtbf;
};

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_SENT_MAYBE
//
//	Force exception RETRY_NEEDED(0, COMPLETED_MAYBE) on random MAYBE_MSG
//	AFTER it is sent to the receiver.
// ----------------------------------------------------------------------------
class retry_sent_maybemsg : public retry_sent_fault {
public:
	retry_sent_maybemsg(default_t arm_by_default = DEFAULT)
		: retry_sent_fault(FAULTNUM_ORB_RETRY_SENT_MAYBE,
			"retry_sent_maybemsg", arm_by_default, default_mtbf)
		{}
	void	usage();

	static const uint_t	default_mtbf;
};

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_UNSENT_VM
//
//	Force exception RETRY_NEEDED(0, COMPLETED_NO) on random FAULT_MSG
//	BEFORE it is sent to the receiver.
// ----------------------------------------------------------------------------
class retry_unsent_vmmsg : public retry_unsent_fault {
public:
	retry_unsent_vmmsg(default_t arm_by_default = DEFAULT)
		: retry_unsent_fault(FAULTNUM_ORB_RETRY_UNSENT_VM,
			"retry_unsent_vmmsg", arm_by_default,
			default_mtbf)
		{}
	void	usage();

	static const uint_t	default_mtbf;
};

// ----------------------------------------------------------------------------
// FAULTNUM_ORB_RETRY_SENT_VM
//
//	Force exception RETRY_NEEDED(0, COMPLETED_MAYBE) on random MAYBE_MSG
//	AFTER it is sent to the receiver.
// ----------------------------------------------------------------------------
class retry_sent_vmmsg : public retry_sent_fault {
public:
	retry_sent_vmmsg(default_t arm_by_default = DEFAULT)
		: retry_sent_fault(FAULTNUM_ORB_RETRY_SENT_VM,
			"retry_sent_vmmsg", arm_by_default, default_mtbf)
		{}
	void	usage();

	static const uint_t	default_mtbf;
};

// ----------------------------------------------------------------------------
// FAULTNUM_TICK_DELAY_PANIC
//
//	Trigger panic if heartbeat ticks (clock, send, receive) are delayed
// ----------------------------------------------------------------------------
class tick_delay_panic : public vanilla_fault {
public:
	tick_delay_panic(default_t arm_by_default = NOT_DEFAULT) :
	    vanilla_fault(FAULTNUM_TICK_DELAY_PANIC, "tick_delay_panic",
			arm_by_default, default_max_percent_delay)
		{};
	void usage();
	void arm(int nodeid);
private:
	static const uint32_t		default_max_percent_delay;
};

// ----------------------------------------------------------------------------
// FAULTNUM_TRANSPORT_DELAY_SEND_THREAD
//
//	simulates a delayed send thread for the heartbeat messages
// ----------------------------------------------------------------------------
class delay_send_thread : public vanilla_fault {
public:
	delay_send_thread(default_t arm_by_default = NOT_DEFAULT) :
	    vanilla_fault(FAULTNUM_TRANSPORT_DELAY_SEND_THREAD,
			"delay_send_thread", arm_by_default, default_mtbf)
		{};
	void usage();
private:
	static const uint32_t		default_mtbf;
};

// ----------------------------------------------------------------------------
// FAULTNUM_PM_DELAY_CLOCK_THREAD
//
//	simulates a delayed clock thread entry into the path manager
// ----------------------------------------------------------------------------
class delay_clock_thread : public vanilla_fault {
public:
	delay_clock_thread(default_t arm_by_default = NOT_DEFAULT) :
	    vanilla_fault(FAULTNUM_PM_DELAY_CLOCK_THREAD,
			"delay_clock_thread", arm_by_default, default_mtbf)
		{};
	void usage();
private:
	static const uint32_t		default_mtbf;
};

// ----------------------------------------------------------------------------
// FAULTNUM_TRANSPORT_PATHEND_FAIL
//
//	Force pathend::fail() at random time.
// ----------------------------------------------------------------------------
class pathend_fail : public vanilla_fault {
public:
	pathend_fail(default_t arm_by_default = NOT_DEFAULT) :
	    vanilla_fault(FAULTNUM_TRANSPORT_PATHEND_FAIL,
			"pathend_fail", arm_by_default, default_mtbf)
		{};
	void usage();
private:
	static const uint_t	default_mtbf;
};

// ----------------------------------------------------------------------------
// FAULTNUM_TRANSPORT_STOP_PM_SEND
//
//	Stop calls to pathend::pm_send_internal() at random time.
// ----------------------------------------------------------------------------
class stop_pm_send : public vanilla_fault {
public:
	stop_pm_send(default_t arm_by_default = DEFAULT) :
	    vanilla_fault(FAULTNUM_TRANSPORT_STOP_PM_SEND,
			    "stop_pm_send", arm_by_default, default_mtbf)
		{};
	void usage();
private:
	static const uint_t	default_mtbf;
};

// ----------------------------------------------------------------------------
// FAULTNUM_PM_PANIC_IN_PATH_DOWN
//
//	Causes current node to panic (in path_record::drop_pathend()) if
//	the path to specific node(s) go down.
// ----------------------------------------------------------------------------
class path_down_panic : public arm_fault {
public:
	path_down_panic(default_t arm_by_default = DEFAULT)
		: arm_fault(FAULTNUM_PM_PANIC_IN_PATH_DOWN,
					"path_down_panic", arm_by_default),
		    _reverse_mask(0xFFFFFFFFFFFFFFFFULL) // panic for any path
		{}

	void	usage();
	bool	parse_options(const char *optstr);
	bool	write_arg(FILE *);
	void	arm(int nodeid);
	void	disarm(int nodeid);

private:
	// NOTE:
	//	The FAULTNUM_PM_PANIC_IN_PATH_DOWN fault point in
	//	path_record::drop_pathend() interprets a zero bit in the
	//	reboot mask (fault argument) to mean panic the current node
	//	if the path to the node corresponding to the bit position
	//	goes down.  If the bit is set, no panic is done.
	//
	//	However, to make bit calculation easier we do the opposite
	//	here.  Then when it's time to arm the fault itself, we simply
	//	pass ~(_reverse_mask) as the fault argument.
	//
	uint64_t	_reverse_mask;
};

#endif	/* _FAULT_DEFS_H */
