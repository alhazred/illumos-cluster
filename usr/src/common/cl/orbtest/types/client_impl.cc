/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)client_impl.cc	1.31	08/05/20 SMI"

#include <sys/types.h>

#if !defined(_KERNEL)
#include <unistd.h>
#endif

#include <sys/os.h>
#include <nslib/ns.h>

#include <orbtest/types/client_impl.h>

client_impl::client_impl() : done_called(false), init_called(false)
{
#if defined(_KERNEL)
	msg_prefix("kernel client:");
#elif defined(_KERNEL_ORB)
	msg_prefix("unode client:");
#else
	msg_prefix("user client:");
#endif
}

client_impl::~client_impl()
{
}

void
#ifdef DEBUG
client_impl::_unreferenced(unref_t cookie)
#else
client_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	unref_called(true);
}

//
// Initialize implementation object
//
bool
client_impl::init()
{
	typetest::client_ptr	tmpref;
	bool			ret = false;

	// Ignore if called more than once.
	if (init_called)
		return (true);

	// Register with the name server.
	tmpref = get_objref();			// get temporary reference
#if defined(_KERNEL)
	ret = register_obj(tmpref, kclient_name);
#elif defined(_KERNEL_ORB)
	ret = register_obj(tmpref, unclient_name);
#else
	ret = register_obj(tmpref, uclient_name);
#endif
	CORBA::release(tmpref);

	init_called = ret;
	return (ret);
}

//
// Clients invoke this when they're finished with this object.
//
void
client_impl::done(Environment &)
{
	// Ignore if invoked more than once.
	if (done_called)
		return;

	// Unregister from the name server.
	if (unregister_obj())
		done_called = true;
}
