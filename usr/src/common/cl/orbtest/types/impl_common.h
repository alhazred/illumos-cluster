/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ORBTEST_TYPES_IMPL_COMMON_H
#define	_ORBTEST_TYPES_IMPL_COMMON_H

#pragma ident	"@(#)impl_common.h	1.40	08/05/20 SMI"

#include <orb/invo/common.h>
#include <sys/os.h>

#ifdef ORBTEST_V1
#include <h/typetest_1.h>
#else
#include <h/typetest_0.h>
#endif

#include <orb/object/adapter.h>

#if defined(_KERNEL_ORB) && !defined(_KERNEL)
#define	_UNODE
#endif

//
// Names registered with the name server.
//
extern const char	userver_name[];		// user-land server
extern const char	kserver_name[];		// kernel-land server
extern const char	unserver_name[];	// unode server
extern const char	uclient_name[];		// user-land client
extern const char	kclient_name[];		// kernel-land client
extern const char	unclient_name[];	// unode client
extern const char	uobj_name[];		// Obj instance

//
// For object reference testing.
//
extern const int32_t	in_obj_id;
extern const int32_t	inout_obj_id;

//
// Check Environment.
// Returns: true if no exception, false otherwise.
// Side Effects: prints the exception if there is one.
//
bool env_ok(Environment &e, const char *msg_prefix);

//
// Verify that the Environment contains the expected exception.
// Returns: true if the expected exception exists, false otherwise.
// Side Effects: prints the exception if doesn't match the expected one.
//
bool env_ok(Environment &e, CORBA::Exception &expected, const char *msg_prefix);

//
// Get an object reference with the specified string name from the name server.
// Returns: CORBA::Object::_nil() if the object isn't found and wait == false
//	or an error occurs.
//	If wait is true, then get_obj waits till the object is found in the
//		name server or an error occurs.
//
CORBA::Object_ptr get_obj(const char *, bool quiet = false, bool wait = true);

//
// Common (non-exported) methods for implementation classes.
//
class impl_common {
public:
	// Suspend calling thread until _unreferenced() is called.
	// Returns: true if _unreferenced() has been called.
	//	    false if timeout arg (in seconds) is nonzero and it
	//	    expires before _unreferenced() is called.
	bool	wait_until_unreferenced(uint32_t timeout = 0);

	bool	unref_called() { return (unref_called_); }

protected:
	// Protected so this class can be used only through derivation.
	impl_common();
	virtual ~impl_common();

	// Register implementation object with the name server.
	// Returns: true if successful, false otherwise.
	bool	register_obj(CORBA::Object_ptr objref, const char *name);

	// Unregister implementation object from the name server.
	// Returns: true if successful, false otherwise.
	bool	unregister_obj();

	// Returns the string name registered with the name server.
	const char	*strname() { return (strname_); }

	void		msg_prefix(const char *s) { msg_prefix_ = s; }
	const char	*msg_prefix() { return (msg_prefix_); }

	void		unref_called(bool b);

private:
	const char	*msg_prefix_;
	bool		unref_called_;

	const char	*strname_;	// name to be registered with ns
	os::mutex_t	mutex;
	os::condvar_t	condvar;
};

//
// Object implementation used for object reference tests.
//
class Obj_impl : public McServerof<typetest::Obj>, public impl_common {
public:
	int32_t	idnum;

	Obj_impl() : idnum(0), init_called(false), done_called(false) {}
	Obj_impl(int32_t l) : idnum(l), init_called(false), done_called(false)
	{}

	~Obj_impl() {}

	// Initialize implementation object.
	// Returns: true if successful, false otherwise.
	bool    init();
	// Clients invoke this when they're finished with this object.
	void    done(Environment &e);


	// sleep_period needs to be specified in seconds.
	void  call_sleep(uint16_t sleep_period, Environment &);

	void	_unreferenced(unref_t);
	void	set_id(int32_t l) { idnum = l; }

	// Interface operations.
	int32_t	id(Environment &) { return (idnum); }
private:
	bool    done_called;
	bool    init_called;
};

class InhObjA_impl : public McServerof<typetest::InhObjA>, public impl_common {
public:
	int32_t	idnum;

	InhObjA_impl() : idnum(0) {}
	InhObjA_impl(int32_t l) : idnum(l) {}
	~InhObjA_impl() {}

	void	_unreferenced(unref_t);
	void 	set_id(int32_t l) { idnum = l; }

	// Interface operations.
	int32_t	id(Environment &) { return (idnum); }
	char	name(Environment &) { return ('A'); }
};

class InhObjB_impl : public McServerof<typetest::InhObjB>, public impl_common {
public:
	int32_t	idnum;

	InhObjB_impl() : idnum(0) {}
	InhObjB_impl(int32_t l) : idnum(l) {}
	~InhObjB_impl() {}

	void	_unreferenced(unref_t);
	void 	set_id(int32_t l) { idnum = l; }

	// Interface operations.
	int32_t	id(Environment &) { return (idnum); }
	char 	name(Environment &) { return ('B'); }
};

class InhObjC_impl : public McServerof<typetest::InhObjC>, public impl_common {
public:
	int32_t	idnum;

	InhObjC_impl() : idnum(0) {}
	InhObjC_impl(int32_t l) : idnum(l) {}
	~InhObjC_impl() {}

	void	_unreferenced(unref_t);
	void 	set_id(int32_t l) { idnum = l; }

	// Interface operations.
	int32_t	id(Environment &) { return (idnum); }
	char 	name(Environment &) { return ('C'); }
};

class InhObjD_impl : public McServerof<typetest::InhObjD>, public impl_common {
public:
	int32_t	idnum;

	InhObjD_impl() : idnum(0) {}
	InhObjD_impl(int32_t l) : idnum(l) {}
	~InhObjD_impl() {}

	void	_unreferenced(unref_t);
	void 	set_id(int32_t l) { idnum = l; }

	// Interface operations.
	int32_t	id(Environment &) { return (idnum); }
	char 	name(Environment &) { return ('D'); }
};

class InhObjE_impl : public McServerof<typetest::InhObjE>, public impl_common {
public:
	int32_t	idnum;

	InhObjE_impl() : idnum(0) {}
	InhObjE_impl(int32_t l) : idnum(l) {}
	~InhObjE_impl() {}

	void	_unreferenced(unref_t);
	void 	set_id(int32_t l) { idnum = l; }

	// Interface operations.
	int32_t	id(Environment &) { return (idnum); }
	char 	name(Environment &) { return ('E'); }
};

//
// Verifies that an object has been unreferenced.
//
bool	verify_unreferenced(Obj_impl* obj_implp, const char *msgpfx);

#endif	/* _ORBTEST_TYPES_IMPL_COMMON_H */
