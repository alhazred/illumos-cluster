/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)impl_common.cc	1.44	08/05/20 SMI"

#include <sys/types.h>

#include <sys/os.h>
#include <nslib/ns.h>

#include <orbtest/types/impl_common.h>

//
// Names registered with the name server.
//
const char	userver_name[]	= "IDL types test server (user)";
const char	kserver_name[]	= "IDL types test server (kernel)";
const char	unserver_name[]	= "IDL types test server (unode)";
const char	uclient_name[]	= "IDL types test client (user)";
const char	kclient_name[]	= "IDL types test client (kernel)";
const char	unclient_name[]	= "IDL types test client (unode)";
const char	uobj_name[]	= "IDL types test Object";

//
// For object reference testing.
//
const int32_t	in_obj_id = 222;
const int32_t	inout_obj_id = 333;

//
// Check Environment.
// Returns: true if no exception, false otherwise.
// Side Effects: prints the exception if there is one.
//
bool
env_ok(CORBA::Environment &e, const char *msg_prefix)
{
	if (e.exception()) {
		e.exception()->print_exception((char *)msg_prefix);
		return (false);
	}
	return (true);
}

//
// Verify that the Environment contains the expected exception.
// Returns: true if the expected exception exists, false otherwise.
// Side Effects: prints the exception if doesn't match the expected one.
//
bool
env_ok(Environment &e, CORBA::Exception &expected, const char *msg_prefix)
{
	CORBA::Exception	*real_ex;
	CORBA::SystemException	*real_sysex, *expected_sysex;

	// Try to narrow the expected exception into SystemException.
	expected_sysex = CORBA::SystemException::_exnarrow(&expected);

	// Verify we got an exception.
	real_ex = e.exception();
	if (real_ex == NULL) {
		os::printf("%s FAIL: no exception returned. ", msg_prefix);
		if (expected_sysex != NULL) {
			os::printf("Expected %s(%d, %d)\n",
				expected._name(),
				expected_sysex->_minor(),
				expected_sysex->completed());
		} else {
			os::printf("Expected %s\n", expected._name());
		}

		return (false);
	}

	// Try to narrow the real exception into SystemException.
	real_sysex = CORBA::SystemException::_exnarrow(real_ex);

	// Verify we got the right exception.
	if (! real_ex->type_match(&expected)) {
		if (real_sysex != NULL) {
			os::printf("%s FAIL: got exception %s(%d, %d). ",
				msg_prefix, real_ex->_name(),
				real_sysex->_minor(),
				real_sysex->completed());
		} else {
			os::printf("%s FAIL: got exception %s. ",
				msg_prefix, real_ex->_name());
		}

		if (expected_sysex != NULL) {
			os::printf("Expected %s(%d, %d)\n",
				expected._name(),
				expected_sysex->_minor(),
				expected_sysex->completed());
		} else {
			os::printf("Expected %s\n", expected._name());
		}

		return (false);
	}

	// If system exception, verify its attributes.
	if (real_sysex != NULL && expected_sysex != NULL) {
		bool	failed = false;

		// Verify minor number.
		if (real_sysex->_minor() != expected_sysex->_minor()) {
			os::printf("%s FAIL: got %s minor %d. "
				"Expected minor %d\n",
				msg_prefix,
				real_sysex->_name(),
				real_sysex->_minor(),
				expected_sysex->_minor());
			failed = true;
		}

		// Verify completion status.
		if (real_sysex->completed() != expected_sysex->completed()) {
			os::printf("%s FAIL: got %s completion status %d. "
				"Expected status %d\n",
				msg_prefix,
				real_sysex->_name(),
				real_sysex->completed(),
				expected_sysex->completed());
			failed = true;
		}

		if (failed) {
			return (false);
		}
	}

	return (true);
}

//
// Get an object reference with the specified string name from the name server.
// Returns: CORBA::Object::_nil() if the object isn't found or an error occurs.
//
CORBA::Object_ptr
get_obj(const char *name, bool quiet /* =false */, bool wait /* true */)
{
	Environment		e;
	CORBA::Object_ptr	objp;

	if (wait) {
		objp = ns::wait_resolve(name, e);
	} else {
		naming::naming_context_var root_ns = ns::root_nameserver();
		objp = root_ns->resolve(name, e);
	}
	if (e.exception()) {
		if (! quiet) {
			os::printf("ERROR: can't resolve \"%s\"\n", name);
			e.exception()->print_exception("\t");
		}
		return (CORBA::Object::_nil());
	}

	return (objp);
}

//
// Class impl_common: common (non-exported) methods for implementation classes.
//

impl_common::impl_common() :
	msg_prefix_(""), unref_called_(false), strname_("")
{
}

// Destructor doesnt identify that msg_prefix has been freed somewhere else.
/*lint -save -e1540 */

impl_common::~impl_common()
{
}
/*lint -restore */

//
// Register implementation object with the name server.
// Returns: true if successful, false otherwise.
//
bool
impl_common::register_obj(CORBA::Object_ptr objref, const char *name)
{
	Environment	e;
	naming::naming_context_var	context = ns::root_nameserver();

	// Remember string name to be registered with the name server.
	strname_ = name;

	// Register implementation object with the name server
	context->rebind(strname_, objref, e);	// register
	if (! env_ok(e, msg_prefix())) {
		os::printf("%s ERROR: can't bind \"%s\"\n",
			msg_prefix(), strname_);
		return (false);
	}

	return (true);					// success
}

//
// Unregister implementation object from the name server.
// Returns: true if successful, false otherwise.
//
bool
impl_common::unregister_obj()
{
	Environment	e;
	naming::naming_context_var	context = ns::root_nameserver();

	// Deregister implementation object from name server.
	context->unbind(strname_, e);
	if (! env_ok(e, msg_prefix())) {
		os::printf("%s ERROR: can't unbind \"%s\"\n",
			msg_prefix(), strname_);
		return (false);
	}

	return (true);					// success
}

//
// Suspend calling thread until _unreferenced() is called.
// Returns: true if _unreferenced() has been called.
//	    false if timeout arg (in seconds) is nonzero and it
//	    expires before _unreferenced() is called.
//
bool
impl_common::wait_until_unreferenced(uint32_t timeout)
{
	bool	retval = true;

	mutex.lock();
	if (timeout == 0) {
		while (! unref_called())
			condvar.wait(&mutex);
	} else {
		os::systime	systime;
		// setreltime() requires timeout in usecs.
		systime.setreltime((long)timeout * 1000000);
		while (! unref_called()) {
			// If timeout expires...
			if (condvar.timedwait(&mutex, &systime) ==
						os::condvar_t::TIMEDOUT) {
				retval = false;
				break;
			}
		}
	}
	mutex.unlock();
	return (retval);
}

//
// Lock and set unref_called_.
//
void
impl_common::unref_called(bool b)
{
	mutex.lock();		// so wait_until_unreferenced() can wait
	unref_called_ = b;
	condvar.signal();	// signal wait_until_unreferenced()
	mutex.unlock();
}

//
// Verifies that an object has been unreferenced.
//
bool
verify_unreferenced(Obj_impl* obj_implp, const char *msgpfx)
{
	const uint32_t	timeout = 30;

	//
	// Wait until _unreferenced() is called on the implementation object.
	// Use a timeout waiting for the _unreferenced() so we can sort of
	// verify that object references are properly released as they
	// are passed around.  If the timeout expires before the
	// _unreferenced() call, print a failure message (after this
	// I don't know what will happen -- kernel modules may panic,
	// user level programs may core dump -- since the object
	// implementations will be destroyed with references still
	// dangling).  Make sure the timeout value is sufficiently large
	// since _unreferenced() is delivered asynchronously.
	//
	if (! obj_implp->wait_until_unreferenced(timeout)) {
		os::printf("FAIL: %s _unreferenced() (object id = %d) "
			"not called within %d seconds\n",
			msgpfx, obj_implp->idnum, timeout);
		return (false);
	}
	return (true);
}

//
// Class Obj_impl
//
void
#ifdef DEBUG
Obj_impl::_unreferenced(unref_t cookie)
#else
Obj_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	unref_called(true);
}

//
// Initialize implementation object
//
bool
Obj_impl::init()
{
	typetest::Obj_ptr	tmpref;
	bool			ret = false;

	// Ignore if called more than once.
	if (init_called) {
		return (true);
	}

	// Register with the name server.
	tmpref = get_objref();	// get temporary reference
	ret = register_obj(tmpref, uobj_name);
	CORBA::release(tmpref);

	init_called = ret;
	return (ret);
}

void
Obj_impl::call_sleep(uint16_t sleep_period, Environment &)
{
	os::usecsleep(1000000*sleep_period);
}

//
// Clients invoke this when they're finished with this object.
//
void
Obj_impl::done(Environment &)
{
	// Ignore if invoked more than once.
	if (done_called)
		return;
	// Unregister from the name server.
	if (unregister_obj()) {
		done_called = true;
	}
}

//
// Class InhObjA_impl
//
void
#ifdef DEBUG
InhObjA_impl::_unreferenced(unref_t cookie)
#else
InhObjA_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	unref_called(true);
}

//
// Class InhObjB_impl
//
void
#ifdef DEBUG
InhObjB_impl::_unreferenced(unref_t cookie)
#else
InhObjB_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	unref_called(true);
}

//
// Class InhObjC_impl
//
void
#ifdef DEBUG
InhObjC_impl::_unreferenced(unref_t cookie)
#else
InhObjC_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	unref_called(true);
}

//
// Class InhObjD_impl
//
void
#ifdef DEBUG
InhObjD_impl::_unreferenced(unref_t cookie)
#else
InhObjD_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	unref_called(true);
}

//
// Class InhObjE_impl
//
void
#ifdef DEBUG
InhObjE_impl::_unreferenced(unref_t cookie)
#else
InhObjE_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	unref_called(true);
}
