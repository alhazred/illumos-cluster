/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ORBTEST_TYPES_SERVER_IMPL_H
#define	_ORBTEST_TYPES_SERVER_IMPL_H

#pragma ident	"@(#)server_impl.h	1.48	08/05/20 SMI"

#ifdef ORBTEST_V1
#include <h/typetest_1.h>
#else
#include <h/typetest_0.h>
#endif

#include <orb/object/adapter.h>
#include <h/streams.h>
#include <h/solobj.h>
#include <h/data_container.h>
#include <orbtest/types/impl_common.h>
#include <orbtest/types/idlverObj_impl.h>

//
// Test server implementation.
//
class server_impl : public McServerof<typetest::server>, public impl_common {
public:
	server_impl();
	~server_impl();
	void	_unreferenced(unref_t);

	// Initialize implementation object.
	// Returns: true if successful, false otherwise.
	bool	init();

	// Clients invoke this when they're finished with this object.
	void	done(Environment &e);

	// Set the no verify flag on the server side (associated with the
	// -noverify option
	void 	set_noverify(Environment &e);

	// Test method for no-argument invocation.
	void	test_noarg(Environment &e);

	//
	// Test methods for IDL basic types.
	// (There are no float and double tests since Solaris doesn't support
	// floating point arithmetic in kernel.)
	//
	bool		test_boolean(
				bool  in,
				bool &out,
				bool &inout,
				Environment &e);
	int8_t		test_char(
				int8_t  in,
				int8_t &out,
				int8_t &inout,
				Environment &e);
	uint8_t		test_octet(
				uint8_t  in,
				uint8_t &out,
				uint8_t &inout,
				Environment &e);
	int16_t		test_short(
				int16_t  in,
				int16_t &out,
				int16_t &inout,
				Environment &e);
	uint16_t	test_ushort(
				uint16_t  in,
				uint16_t &out,
				uint16_t &inout,
				Environment &e);
	int32_t		test_long(
				int32_t  in,
				int32_t &out,
				int32_t &inout,
				Environment &e);
	uint32_t	test_ulong(
				uint32_t  in,
				uint32_t &out,
				uint32_t &inout,
				Environment &e);
	int64_t		test_longlong(
				int64_t  in,
				int64_t &out,
				int64_t &inout,
				Environment &e);
	uint64_t	test_ulonglong(
				uint64_t  in,
				uint64_t &out,
				uint64_t &inout,
				Environment &e);

	// Test methods for bounded (typetest::Bstring) and
	// unbounded (typetest::Ustring) strings.
	typetest::Bstring	test_bstring(
					const typetest::Bstring &in,
					typetest::Bstring_out    out,
					typetest::Bstring &inout,
					uint32_t len,
					Environment &e);
	typetest::Ustring	test_ustring(
					const typetest::Ustring in,
					CORBA::String_out out,
					typetest::Ustring &inout,
					uint32_t len,
					Environment &e);

	//
	// Test methods for bounded and unbounded sequences.
	//
	typetest::Bseq	*test_bseq(
				const typetest::Bseq &in,
				typetest::Bseq_out outp,
				typetest::Bseq &inout,
				uint32_t len,
				Environment &e);
	typetest::Useq	*test_useq(
				const typetest::Useq &in,
				typetest::Useq_out outp,
				typetest::Useq &inout,
				uint32_t len,
				Environment &e);

	void		test_seq_read(
				typetest::UBseq_out outp,
				uint32_t len,
				Environment &e);

	void		test_seq_write(
				const typetest::UBseq &in,
				uint32_t len,
				Environment &e);

	void		test_seq_rw(
				typetest::UBseq &inout,
				uint32_t len,
				Environment &e);

	typetest::BseqUseq	*test_bseq_useq(
					const typetest::BseqUseq &in,
					typetest::BseqUseq_out outp,
					typetest::BseqUseq &inout,
					uint32_t len,
					uint32_t maxlen,
					Environment &e);
	typetest::UseqBseq	*test_useq_bseq(
					const typetest::UseqBseq &in,
					typetest::UseqBseq_out outp,
					typetest::UseqBseq &inout,
					uint32_t len,
					uint32_t maxlen,
					Environment &e);

	typetest::BseqUstring	*test_bseq_ustring(
					const typetest::BseqUstring &in,
					typetest::BseqUstring_out outp,
					typetest::BseqUstring &inout,
					uint32_t len,
					uint32_t maxlen,
					Environment &e);
	typetest::UseqBstring	*test_useq_bstring(
					const typetest::UseqBstring &in,
					typetest::UseqBstring_out outp,
					typetest::UseqBstring &inout,
					uint32_t len,
					uint32_t maxlen,
					Environment &e);

	//
	// Tests for fixed (typetest::Fstruct) and variable (typetest::Vstruct)
	// structures.
	//
	void			test_fstruct(
					const typetest::Fstruct &in,
					typetest::Fstruct &out,
					typetest::Fstruct &inout,
					Environment &e);
	typetest::Vstruct	*test_vstruct(
					const typetest::Vstruct &in,
					typetest::Vstruct_out outp,
					typetest::Vstruct &inout,
					uint32_t len,
					typetest::Obj_ptr objref,
					Environment &e);

	typetest::Fstructseq*		test_fstructseq(
					const typetest::Fstructseq &i,
					typetest::Fstructseq_out o,
					typetest::Fstructseq &io,
					uint32_t len,
					Environment &_environment);

	typetest::Useq		*test_nullseq(const typetest::Useq &i,
					typetest::Useq_out o,
					typetest::Useq &io,
					uint32_t len,
					Environment &_environment);
	typetest::Useq 		*test_0lenseq(const typetest::Useq &i,
					typetest::Useq_out o,
					typetest::Useq &io,
					uint32_t len,
					Environment &_environment);
	//
	// Tests for fixed (typetest::Funion) and variable (typetest::Vunion)
	// unions.
	//
	void			test_funion(
					const typetest::Funion &in,
					typetest::Funion &out,
					typetest::Funion &inout,
					typetest::FDiscrim discrim,
					Environment &e);
	typetest::Vunion	*test_vunion(
					const typetest::Vunion &in,
					typetest::Vunion_out outp,
					typetest::Vunion &inout,
					typetest::VDiscrim discrim,
					uint32_t len,
					typetest::Obj_ptr objref,
					Environment &e);

	//
	// Object reference tests.
	//
	typetest::Obj_ptr	test_obj(
					typetest::Obj_ptr  in,
					typetest::Obj_out  out,
					typetest::Obj_ptr &inout,
					Environment &e);

	typetest::InhObjA_ptr	test_objinhsimple(
					typetest::InhObjA_ptr  in,
					typetest::InhObjA_out  out,
					typetest::InhObjA_ptr &inout,
					Environment &e);

	typetest::InhObjA_ptr	test_objinhmany(
					typetest::InhObjA_ptr  in,
					typetest::InhObjA_out  out,
					typetest::InhObjA_ptr &inout,
					Environment &e);

	typetest::InhObjA_ptr	test_objinhmultiple(
					typetest::InhObjA_ptr  in,
					typetest::InhObjA_out  out,
					typetest::InhObjA_ptr &inout,
					Environment &e);

	typetest::Objseq	*test_objseq(
					const typetest::Objseq &in,
					typetest::Objseq_out outp,
					typetest::Objseq &inout,
					uint32_t len,
					Environment &e);

	// Test method for revoke operation.
	void	test_revoke(Environment &e);

	// Test methods for oneway (blocking and nonblocking) invocations.
	//
	// The "client_ref" argument is a reference to the invoking
	// client.  The "flag_addr" argument is an address in the
	// client's address space.
	//
	// Note, non-blocking invocations don't allow passing object
	// references, so we'll pass the string name of the invoking
	// client instead.  The server will then use this name to get a
	// reference from the name server.
	//
	// After the client invokes the server, it waits on a flag
	// variable.  The server then invokes a "callback" method on
	// the client which sets the flag, to indicate that the
	// server has been successfully invoked.
	//
	void	test_oneway(
			uint64_t flag_addr,
			Environment &e);
	void	test_nonblocking(
			const char *client_name,
			uint64_t flag_addr,
			int32_t i1,
			int32_t i2,
			int32_t i3,
			int32_t i4,
			int32_t i5,
			Environment &e);

	// Test method for arrays
	void	test_arrayshort(
			const typetest::arrayShort in,
			typetest::arrayShort out,
			typetest::arrayShort inout,
			Environment &e);

	void	test_arrayoctet(
			const typetest::arrayOctet in,
			typetest::arrayOctet out,
			typetest::arrayOctet inout,
			Environment &e);

	void	test_arrayfstruct(
			const typetest::arrayFstruct in,
			typetest::arrayFstruct out,
			typetest::arrayFstruct inout,
			Environment &e);

	void	test_arrayfunion(
			const typetest::arrayFunion in,
			typetest::arrayFunion out,
			typetest::arrayFunion inout,
			Environment &e);

	void	test_emptyexception(
			Environment &e);

	void	test_basicexception(
			Environment &e);

	void	test_complexexception(
			typetest::Obj_ptr objref,
			uint32_t len,
			Environment &e);

	void	test_stringexception(
			Environment &e);

#if !defined(_UNODE)
	void	test_bulkio_uio_in(
			bulkio::in_uio_ptr in,
			uint32_t len,
			Environment &);

	void	test_bulkio_uio_inout(
			bulkio::inout_uio_ptr &inout,
			uint32_t len,
			Environment &);

	void	test_bulkio_pages_in(
			bulkio::in_pages_ptr in,
			uint32_t noofpages,
			Environment &);

	void	test_bulkio_pages_inout(
			bulkio::inout_pages_ptr &inout,
			uint32_t noofpages,
			Environment &);
#endif // _UNODE

	// Store client reference
	void	store_client_ref(
			typetest::client_ptr client_ref,
			Environment &e);

	// Release client reference
	void	release_client_ref(Environment &e);

	// Cred Object test

	solobj::cred_ptr	test_cred(
			solobj::cred_ptr in,
			solobj::cred_out out,
			solobj::cred_ptr &inout,
			Environment &e);

	// Called by the client to free the memory allocated
	// for the 'out' parameter on the server side.

	void	cred_crfree(Environment &e);

	// Data Container Object test

	data_container::data_ptr	test_data_container(
			data_container::data_ptr in,
			data_container::data_out out,
			data_container::data_ptr &inout,
			uint32_t	length,
			Environment &e);

private:
	bool	init_called;
	bool verify_off;

	minverObjA_impl	*idlverpA;
#ifdef ORBTEST_V1
	minverObjB_impl *idlverpB;
	minverObjC_impl *idlverpC;
	combinedObjD_impl *idlverpD;
#endif

	static void 	*revoke_thread(void *arg);
	void		revoke_operation();

	enum {	REVOKE =		0x1,	// Need to do Revoke
		REVOKED =		0x2};	// revoke thread died

	char	flags;

	os::mutex_t	lck;	// protects flags and condition variables

	// signals when revoke thread should run
	os::condvar_t	revoke_start_cv;

	// signals when revoke thread terminates
	os::condvar_t	revoke_done_cv;

	// Used for out parameter (cred object test)
	cred_t		*cr_out;

	// Used to store client reference for oneway test
	typetest::client_ptr	clnt_ref;
};

#endif	/* _ORBTEST_TYPES_SERVER_IMPL_H */
