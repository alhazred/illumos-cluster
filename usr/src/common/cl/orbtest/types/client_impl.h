/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ORBTEST_TYPES_CLIENT_IMPL_H
#define	_ORBTEST_TYPES_CLIENT_IMPL_H

#pragma ident	"@(#)client_impl.h	1.43	08/05/20 SMI"

#ifdef ORBTEST_V1
#include <h/typetest_1.h>
#else
#include <h/typetest_0.h>
#endif
#include <orb/object/adapter.h>
#include <h/streams.h>
#include <orbtest/types/impl_common.h>

//
// Test client implementation.
//
class client_impl : public McServerof<typetest::client>, public impl_common {
public:
	client_impl();
	~client_impl();
	void	_unreferenced(unref_t);

	// Initialize implementation object.
	// Returns: true if successful, false otherwise.
	bool	init();

	// Clients invoke this when they're finished with this object.
	void	done(Environment &e);

	//
	// These functions perform tests on the test client side.
	// Each is invoked by driver "programs" and, in turn, invokes
	// the corresponding method on the test server side.
	// Parameters:
	//	server_ref  --	test server object to invoke.
	//	repeat_num  --	how many times to run the test.
	//	len	    --	for string/sequence tests, specifies
	//			the length, in bytes (for strings,
	//			does not include the terminating NUL),
	//			to test.
	//	len, maxlen --	for sequence of strings/sequences tests,
	//			len specifies the length of the
	//			enclosing sequence, maxlen specifies the
	//			maximum length of the enclosed
	//			string/sequence (each will have length
	//			one longer than the previous).
	// Returns:
	//	Total test execution time.
	//
	int64_t	test_noarg(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);

	int64_t	test_boolean(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);
	int64_t	test_char(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);
	int64_t	test_octet(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);
	int64_t	test_short(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);
	int64_t	test_ushort(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);
	int64_t	test_long(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);
	int64_t	test_ulong(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);
	int64_t	test_longlong(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);
	int64_t	test_ulonglong(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);

	int64_t	test_bstring(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				bool noverify,
				Environment &e);
	int64_t	test_ustring(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				bool noverify,
				Environment &e);

	int64_t	test_bseq(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				bool noverify,
				Environment &e);
	int64_t	test_useq(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				bool noverify,
				Environment &e);

	int64_t	test_seq_read(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				bool noverify,
				Environment &e);

	int64_t	test_seq_write(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				bool noverify,
				Environment &e);

	int64_t	test_seq_rw(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				bool noverify,
				Environment &e);

	int64_t	test_bseq_useq(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				uint32_t maxlen,
				bool noverify,
				Environment &e);
	int64_t	test_useq_bseq(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				uint32_t maxlen,
				bool noverify,
				Environment &e);

	int64_t	test_bseq_ustring(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				uint32_t maxlen,
				bool noverify,
				Environment &e);
	int64_t	test_useq_bstring(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				uint32_t maxlen,
				bool noverify,
				Environment &e);

	int64_t	test_fstruct(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);
	int64_t	test_vstruct(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				bool noverify,
				Environment &e);

	int64_t	test_fstructseq(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				bool noverify,
				Environment &_environment);
	int64_t test_nullseq(typetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				bool noverify,
				Environment &_environment);

	int64_t test_0lenseq(typetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				bool noverify,
				Environment &_environment);

	int64_t	test_funion(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);
	int64_t	test_vunion(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				bool noverify,
				Environment &e);

	int64_t	test_obj(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);

	int64_t	test_objinhsimple(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);

	int64_t	test_objinhmany(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);

	int64_t	test_objinhmultiple(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);

	int64_t	test_objseq(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				bool noverify,
				Environment &e);

	// The time value is not important for revoke
	int64_t	test_revoke(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);

	int64_t	test_oneway(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);
	void	test_oneway_callback_verify(
				uint64_t flag_addr,
				Environment &e);
	void	test_oneway_callback_noverify(
				uint64_t flag_addr,
				Environment &e);

	int64_t	test_nonblocking(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);
	void	test_nonblocking_callback(
				uint64_t flag_addr,
				Environment &e);

	// Array testing

	int64_t	test_arrayshort(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);

	int64_t	test_arrayoctet(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);

	int64_t	test_arrayfstruct(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);

	int64_t	test_arrayfunion(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);

	int64_t	test_emptyexception(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);

	int64_t	test_basicexception(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);

	int64_t	test_complexexception(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);

	int64_t	test_stringexception(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);

#if !defined(_UNODE)
	int64_t	test_bulkio_uio_in(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				bool noverify,
				Environment &e);

	int64_t	test_bulkio_uio_inout(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				bool noverify,
				Environment &e);

	int64_t	test_bulkio_pages_in(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t noofpages,
				bool noverify,
				Environment &e);

	int64_t	test_bulkio_pages_inout(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t noofpages,
				bool noverify,
				Environment &e);
#endif // _UNODE

	int64_t	test_cred(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				bool noverify,
				Environment &e);

	int64_t	test_data_container(
				typetest::server_ptr server_ref,
				int32_t num_repeat,
				uint32_t len,
				bool noverify,
				Environment &e);

private:
	bool	done_called;
	bool	init_called;
};

#endif	/* _ORBTEST_TYPES_CLIENT_IMPL_H */
