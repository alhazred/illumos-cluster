/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)ktypetest_client.cc	1.15	08/05/20 SMI"

#include <sys/types.h>
#include <sys/modctl.h>
#include <sys/errno.h>

#include <sys/os.h>
#include <nslib/ns.h>

#include <orbtest/types/client_impl.h>

extern "C" {
	void _cplpl_init();
	void _cplpl_fini();
}

// inter-module dependencies
#ifdef ORBTEST_V0
char _depends_on[] = "misc/cl_runtime misc/cl_load misc/cl_orb "
	"misc/cl_haci misc/cl_quorum misc/cl_comm misc/ktypetest_v0";
#else
char _depends_on[] = "misc/cl_runtime misc/cl_load misc/cl_orb "
	"misc/cl_haci misc/cl_quorum misc/cl_comm misc/ktypetest_v1";
#endif

extern struct mod_ops mod_miscops;

static struct modlmisc modlmisc = {
	&mod_miscops, "IDL Types Test Client",
};

static struct modlinkage modlinkage = {
	MODREV_1, { (void *)&modlmisc, NULL }
};

static client_impl	*clientp = NULL;

int
_init(void)
{
	int	error;

	if (error = mod_install(&modlinkage)) {
		return (error);
	}
	_cplpl_init();				// C++ initialization

	clientp = new client_impl;
	if (! clientp->init()) {
		delete clientp; clientp = NULL;

		_cplpl_fini();
		mod_remove(&modlinkage);
		return (EIO);
	}

	os::printf("TEST CLIENT (kernel) READY\n");
	return (0);
}

int
_fini(void)
{
	// Return EBUSY if modunloaded before we get an unreferenced() call
	if (! clientp->unref_called()) {
		return (EBUSY);
	}

	delete clientp; clientp = NULL;

	_cplpl_fini();
	return (mod_remove(&modlinkage));
}

int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}
