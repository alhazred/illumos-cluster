/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)userver.cc	1.23	08/05/20 SMI"

#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>

#include <sys/os.h>
#include <nslib/ns.h>

#include <orbtest/types/server_impl.h>

#ifndef _UNODE
#include <orb/infrastructure/orb.h>
#endif

int
#ifdef _UNODE
server(int, char *[])
#else
main(int, char *[])
#endif
{
	server_impl	*serverp;

#ifndef _UNODE
	(void) fprintf(stdout, "userver: Initializing user-land ORB\n");
	if (ORB::initialize() != 0) {
		(void) fprintf(stderr, "ERROR: Can't initialize ORB\n");
		return (1);
	}
#endif

	serverp = new server_impl;
	(void) fprintf(stdout, "userver: Calling serverp->init()\n");
	if (! serverp->init()) {
		return (1);
	}
#ifdef _UNODE
	(void) fprintf(stdout, "TYPES TEST SERVER (unode) READY\n");
#else
	(void) fprintf(stdout, "TYPES TEST SERVER (userland) READY\n");
#endif

	(void) serverp->wait_until_unreferenced();
	return (0);
}
