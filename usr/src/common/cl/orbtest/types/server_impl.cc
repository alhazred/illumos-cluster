/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)server_impl.cc	1.40	08/05/20 SMI"

#include <sys/types.h>

#if !defined(_KERNEL)
#include <stdlib.h>				// for exit()
#endif

#include <sys/os.h>
#include <nslib/ns.h>

#include <orbtest/types/server_impl.h>

//
// constructor - create a thread to support revoke operations.
//
// will succeed even if thread creation fails for revoke test. Only the
// revoke test will fail to execute
//
server_impl::server_impl() : init_called(false), flags(0)
{
	// by default we will do verification of results
	verify_off = false;
#ifdef _KERNEL
	msg_prefix("kernel server:");

	if (thread_create(NULL, NULL, (void(*)())server_impl::revoke_thread,
	    (caddr_t)this, 0, &p0, TS_RUN, 60) == NULL) {
		os::warning("thread_create failed for revoke_thread");
		flags |= REVOKED;
	}
#else
#ifdef _KERNEL_ORB
	msg_prefix("unode server:");
#else
	msg_prefix("user server:");
#endif

	if (thr_create(NULL, (size_t)0, server_impl::revoke_thread,
	    (void *)this, THR_BOUND|THR_DETACHED, NULL)) {
		os::warning("Create thread failed for revoke_thread");
		flags |= REVOKED;
	}
#endif
}

//
// destructor - signals shutdown to the revoke_thread and
//	waits for that thead to terminate.
//
// Should be in this routine only if the server was unreferenced
server_impl::~server_impl()
{
	lck.lock();
	// Tell revoke thread to kill itself
	if (!(flags & REVOKED)) {
		flags |= REVOKE;
		revoke_start_cv.signal();
	}

	// Wait for revoke thread to die
	while (!(flags & REVOKED)) {
		revoke_done_cv.wait(&lck);
	}
	lck.unlock();

	idlverpA = NULL;
	cr_out = NULL;
	clnt_ref = NULL;
#ifdef ORBTEST_V1
	idlverpB = NULL;
	idlverpC = NULL;
	idlverpD = NULL;
#endif
}

//
// revoke_thread - entry place for thread that performs the revoke
//
void *
server_impl::revoke_thread(void * arg)
{
	// Convert arg to a pointer to the server object
	server_impl *my_serverp = (server_impl *)arg;

	my_serverp->revoke_operation();
	return (NULL);	// thread exits on return
}

//
// revoke_operation - perform revoke when requested and terminate thread
//
void
server_impl::revoke_operation()
{
	os::printf("SERVER: server_impl::revoke_operation() called\n");
	lck.lock();
	while (!(flags & REVOKE)) {
		revoke_start_cv.wait(&lck);
	}

	// Do revoked only once
	if (!(flags & REVOKED)) {
		// Revoke the object.
		revoke();

		// Unregister from the name server
		(void) unregister_obj();
	}

	// Show revoke done
	flags |= REVOKED;
	flags &= ~REVOKE;

	// Notify object destructor that thread is gone
	revoke_done_cv.signal();
	lck.unlock();
}

void
#ifdef DEBUG
server_impl::_unreferenced(unref_t cookie)
#else
server_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	unref_called(true);
}

//
// Initialize implementation object
//
bool
server_impl::init()
{
	typetest::server_ptr	tmpref;

	os::printf("SERVER: server_impl::init() called\n");
	// Ignore if called more than once.
	if (init_called)
		return (true);

	// Register self with the name server.
	tmpref = get_objref();			// get temporary reference
#if defined(_KERNEL)
	init_called = register_obj(tmpref, kserver_name);
#elif defined(_KERNEL_ORB)
	init_called = register_obj(tmpref, unserver_name);
#else
	init_called = register_obj(tmpref, userver_name);
#endif
	CORBA::release(tmpref);

	// Initialize IDL versioning test objects
	os::printf("SERVER: Initializing IDL versioning objects\n");

	idlverpA = new minverObjA_impl;
	os::printf("SERVER: Calling idlverpA->init()\n");
	if (! idlverpA->init()) {
		return (false);
	}
#ifdef ORBTEST_V1
	idlverpB = new minverObjB_impl;
	os::printf("SERVER: Calling idlverpB->init()\n");
	if (! idlverpB->init()) {
		return (false);
	}
	idlverpC = new minverObjC_impl;
	os::printf("SERVER: Calling idlverpC->init()\n");
	if (! idlverpC->init()) {
		return (false);
	}

	idlverpD = new combinedObjD_impl;
	os::printf("SERVER: Calling idlverpD->init()\n");
	if (! idlverpD->init()) {
		return (false);
	}
#endif // ORBTEST_V1

	return (init_called);
}

//
// Clients invoke this when they're finished with this object.
//
void
server_impl::done(Environment &)
{
	os::printf("SERVER: server_impl::done() called\n");
	lck.lock();
	// Can only execute done/revoke once
	if (!(flags & (REVOKE | REVOKED))) {
		// Tell revoke thread to perform a revoke
		flags |= REVOKE;
		revoke_start_cv.signal();
	}
	Environment e;
	idlverpA->done(e);
#ifdef ORBTEST_V1
	idlverpB->done(e);
	idlverpC->done(e);
	idlverpD->done(e);
#endif

	// We dont wait for the revoke to complete. This will allow the
	// done operation to be exectued from a remote node also
	// Otherwise revoke can block
	lck.unlock();
}

// Sets the "no-verifcation" flag.  This is associated with the
// -noverify option
void
server_impl::set_noverify(Environment &)
{
	verify_off = true;
}
