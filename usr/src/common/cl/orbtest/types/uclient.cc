/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)uclient.cc	1.24	08/05/20 SMI"

#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>

#include <sys/os.h>
#include <nslib/ns.h>

#include <orbtest/types/client_impl.h>
#include <orbtest/types/impl_common.h>

#ifndef _UNODE
#include <orb/infrastructure/orb.h>
#endif

int
#ifdef _UNODE
client(int, char *[])
#else
main(int, char *[])
#endif
{
	client_impl	*clientp;
	Obj_impl	*Objp;
	Environment	env;

#ifndef _UNODE
	if (ORB::initialize() != 0) {
		(void) fprintf(stderr, "ERROR: Can't initialize ORB\n");
		return (1);
	}
#endif

	clientp = new client_impl;
	if (! clientp->init()) {
		return (1);
	} else {
		// register Obj for test_fork support.
		Objp = new Obj_impl(in_obj_id);
		if (! Objp->init()) {
			return (1);
		}
	}

#ifdef _UNODE
	(void) fprintf(stdout, "TEST CLIENT (unode) READY\n");
#else
	(void) fprintf(stdout, "TEST CLIENT (user) READY\n");
#endif

	if (clientp->wait_until_unreferenced()) {
		// when the client is unreferenced.
		// Obj is also unregistered form the NS.
		Objp->done(env);
		(void) Objp->wait_until_unreferenced();
	}
	return (0);
}
