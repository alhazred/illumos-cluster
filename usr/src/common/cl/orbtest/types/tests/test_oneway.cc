/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_oneway.cc	1.18	08/05/20 SMI"

//
// Oneway test
//

#include <orbtest/types/client_impl.h>
#include <orbtest/types/server_impl.h>

class callback_flag {
public:
	os::hrtime_t	rtt;
	bool		acked;
	os::mutex_t	lck;
	os::condvar_t	cv;
};

void callback_func(callback_flag *flagp);

//
// Test client interface.  Invoked by driver "programs" and, in turn,
// invokes the corresponding method on the test server side.
// Parameters:
//	server_ref  --	test server object to invoke.
//	num_repeat  --	how many times to run the test.
//	noverify    --	if true then server won't perform data verification.
// Returns:
//	Total test execution time.
//
int64_t
client_impl::test_oneway(
		typetest::server_ptr server_ref,
		int32_t num_repeat,
		bool noverify,
		Environment &)
{
	// os::printf("%s test_oneway()\n", msg_prefix());

	const uint32_t		timeout = 60;		// in seconds

	os::hrtime_t		total_time = 0, start_time;
	callback_flag		*flagp = NULL;
	os::systime		systime;

	if (noverify) {
		Environment	e3;

		server_ref->set_noverify(e3);
		if (! env_ok(e3, msg_prefix())) {
			return (-1);
		}
	}

	// Create a flag so server can acknowledge that it's been invoked.
	flagp = new callback_flag;
	if (flagp == NULL) {
		os::printf("%s ERROR: can't allocate flag\n", msg_prefix());
		return (-1);
	}

	for (; num_repeat > 0; --num_repeat) {
		Environment	e2;

		flagp->acked = false;

		start_time = os::gethrtime();
		if (noverify)
			flagp->rtt = start_time;
		server_ref->test_oneway((uint64_t)flagp, e2);
		if (! env_ok(e2, msg_prefix())) {
			delete flagp;
			return (-1);
		}

		if (!noverify)
			total_time += os::gethrtime() - start_time;

		// Timed wait until the server sends an acknowledgment.
		systime.setreltime(timeout * 1000000);
		flagp->lck.lock();
		while (! flagp->acked) {
			// If timeout expires...
			if (flagp->cv.timedwait(&flagp->lck, &systime) ==
						os::condvar_t::TIMEDOUT) {
				os::printf("%s FAIL: no ack from server in %d "
					"seconds\n", msg_prefix(), timeout);
				flagp->lck.unlock();
				delete flagp;
				return (-1);
			}
		}
		flagp->lck.unlock();
		if (noverify)
			total_time += flagp->rtt;
	}

	delete flagp;
	return (total_time);
}

//
// Callback method invoked by the server to indicate that it's been
// successfully invoked. It's a two way invocation used in verify mode
// Parameters:
//	flag_addr  -- address of flag the client waits on.
// Returns:
//	None.
//

void
client_impl::test_oneway_callback_verify(
		uint64_t 		flag_addr,
		Environment &)
{
	callback_flag	*flagp = (callback_flag *)flag_addr;
	callback_func(flagp);
}

//
// Callback method invoked by the server to indicate that it's been
// successfully invoked. It's a one way invocation used in noverify mode
// to measure round trip time
// Parameters:
//	flag_addr  -- address of flag the client waits on.
// Returns:
//	None.
//

void
client_impl::test_oneway_callback_noverify(
		uint64_t 		flag_addr,
		Environment &)
{
	callback_flag	*flagp = (callback_flag *)flag_addr;
	flagp->rtt = os::gethrtime() - flagp->rtt;
	callback_func(flagp);
}

void
callback_func(callback_flag *flagp)
{
	flagp->lck.lock();
	flagp->acked = true;
	flagp->cv.signal();
	flagp->lck.unlock();
}

//
// Test server interface.  Invoked by the test client.
// Parameters:
//	flag_addr	-- address of a flag variable the client is waiting on.
//			   We'll pass it back to the client's callback method.
// Returns:
//	None.
//
void
server_impl::test_oneway(
		uint64_t flag_addr,
		Environment &)
{
	// os::printf("%s test_oneway()\n", msg_prefix());

	// Invoke the client's callback method to indicate that we've been
	// successfully invoked.

	typetest::client_var clientp = typetest::client::_duplicate(clnt_ref);

	// Nested invocation requires its own Environment
	Environment	env;

	if (verify_off) {
		// Make a one way invocation to acknowledge
		clientp->test_oneway_callback_noverify(flag_addr, env);
	} else {
		// Make a two way invocation to acknowledge
		clientp->test_oneway_callback_verify(flag_addr, env);
	}
	(void) env_ok(env, msg_prefix());
}

void
server_impl::store_client_ref(
		typetest::client_ptr	objref,
		Environment	&)
{
	clnt_ref = typetest::client::_duplicate(objref);
}

void
server_impl::release_client_ref(
		Environment	&)
{
	CORBA::release(clnt_ref);
}
