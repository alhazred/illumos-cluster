/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_stringexception.cc	1.8	08/05/20 SMI"

//
// Exception test
//

#include <orbtest/types/client_impl.h>
#include <orbtest/types/server_impl.h>

//
// Forward Definitions
//
static bool
set_exception(Environment &e, const char *msgpfx, bool set = true);

inline bool
verify_exception(Environment &e, const char *msgpfx)
{
	return (set_exception(e, msgpfx, false));
}


//
// Test client interface.  Invoked by driver "programs" and, in turn,
// invokes the corresponding method on the test server side.
// Parameters:
//	server_ref  --	test server object to invoke.
//	num_repeat  --	number of times that we'll execute test
//	noverify    --	(ignored)
// Returns:
//	Total test execution time.
//
int64_t
client_impl::test_stringexception(
		typetest::server_ptr server_ref,
		int32_t num_repeat,
		bool,
		Environment &)
{
	// os::printf ("%s test_stringexception()\n",msg_prefix());
	os::hrtime_t total_time = 0, start_time;

	int i;

	for (i = 0; i < num_repeat; ++i) {
		Environment e2;

		// Invoke server
		start_time = os::gethrtime();

		server_ref->test_stringexception(e2);
		total_time += os::gethrtime() - start_time;

		// Verify that the exception raised
		if (!verify_exception(e2, msg_prefix())) {
			total_time = -1;
			break;
		}

		// Clear the exception
		e2.clear();
	}

	return (total_time);
}


//
// Test server interface.  Invoked by the test client.
//
void
server_impl::test_stringexception(Environment &e)
{
	set_exception(e, msg_prefix());
}

//
// Set Exception
//
bool
set_exception(Environment &e, const char *msgpfx, bool set)
{
	CORBA::Exception		*p_ex;
	typetest::stringException	*p_ex_expected;
	bool				passed = true;
	char 				p[1024];
	const char 			*msg;

	// memset swas giving vague errors about func overloading
	for (int i = 0; i < 1023; i++)
		p[i] = 'a';
	p[1023] = '\0';
	msg = p;

	p_ex_expected = new typetest::stringException(msg);

	// Set exception
	if (set) {
		e.exception(p_ex_expected);
		return (true);
	}

	// Verify Exception
	{
		typetest::stringException *p_ex_actual;

		p_ex = e.exception();
		if (p_ex == NULL) {
			os::printf("%s string_exception not in environment\n",
				msgpfx);
			delete(p_ex_expected);
			return (false);
		}

		// There is an exception, verify that it ONLY narrows to
		// the expected type.

		// First the incorrect narrow
		if ((typetest::emptyException::_exnarrow(p_ex)) ||
		    (typetest::complexException::_exnarrow(p_ex)) ||
		    (CORBA::COMM_FAILURE::_exnarrow(p_ex))) {
			os::printf("FAIL: %s String exception narrowed to the "
			    "incorrect type.\n", msgpfx);
			delete(p_ex_expected);
			return (false);
		}

		if (!typetest::stringException::_exnarrow(p_ex)) {
			os::printf("%s exceptions do not match\n", msgpfx);
			os::printf("\texpected = 0x%x, actual = 0x%x\n",
				p_ex_expected->_major(), p_ex->_major());
			delete(p_ex_expected);
			return (passed);
		}

		p_ex_actual = typetest::stringException::_exnarrow(p_ex);

		if (strcmp(p_ex_actual->msg, p_ex_expected->msg) != 0) {
			os::printf("%s exception messages do not match\n",
				msgpfx);
			os::printf("\texpected = %s, actual = %s\n",
				(char *)p_ex_expected->msg,
				(char *)p_ex_actual->msg);
			passed = false;
		}

	} // Verification

	delete(p_ex_expected);
	return (passed);
}
