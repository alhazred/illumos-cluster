/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_complexexception.cc	1.19	08/05/20 SMI"

//
// Exception test
//

#include <orbtest/types/client_impl.h>
#include <orbtest/types/server_impl.h>

//
// Forward Definitions
//
static bool
set_exception(
	Environment &e,
	typetest::Obj_ptr,
	uint32_t len,
	const char *msgpfx,
	bool set = true);

inline bool
verify_exception(
	Environment &e,
	typetest::Obj_ptr o,
	uint32_t l,
	const char *m)
{
	return (set_exception(e, o, l, m, false));
}


//
// Test client interface.  Invoked by driver "programs" and, in turn,
// invokes the corresponding method on the test server side.
// Parameters:
//	server_ref  --	test server object to invoke.
//	num_repeat  --	number of times that we'll execute test
//	noverify    --	if true then no data verification will be performed
// Returns:
//	Total test execution time.
//
int64_t
client_impl::test_complexexception(
		typetest::server_ptr server_ref,
		int32_t num_repeat,
		bool noverify,
		Environment &)
{
	// os::printf ("%s test_complexexception()\n",msg_prefix());
	os::hrtime_t total_time = 0, start_time;

	int i;
	uint32_t len;

#ifndef BUG_4089645
	Obj_impl	obj_impl(in_obj_id);
#endif

	for (i = 0; i < num_repeat && total_time >= 0; ++i) {
		Environment e2;
		len = i;

		typetest::Obj_ptr	objref;

#ifndef BUG_4089645
		objref = obj_impl.get_objref();
#else
		objref = NULL;
#endif

		// Invoke server
		start_time = os::gethrtime();
		server_ref->test_complexexception(objref, len, e2);
		total_time += os::gethrtime() - start_time;

		// Verify exception raised by server.
		if (noverify) {
			e2.clear();		// verification not requested
		} else if (!verify_exception(e2, NULL, len, msg_prefix())) {
			total_time = -1;
		}

#ifndef BUG_4089645
		CORBA::release(objref);
		if (!noverify) {
			if (!verify_unreferenced(&obj_impl, msg_prefix())) {
				total_time = -1;
			}
		}
#endif
	}

	return (total_time);
}


//
// Test server interface.  Invoked by the test client.
//
void
server_impl::test_complexexception(typetest::Obj_ptr objref, uint32_t len,
		Environment &e)
{
	set_exception(e, objref, len, msg_prefix());
}


//
// Set Exception
//
bool
set_exception(Environment &e, typetest::Obj_ptr, uint32_t len,
		const char *msgpfx, bool set)
{
	typetest::complexException	*p_cex_expected;
	bool				passed = true;
	uint32_t 				i;

	char				baseChar = 1;

	p_cex_expected = new typetest::complexException;

	p_cex_expected->fixedStruct.m_1byte = ~0;
	p_cex_expected->fixedStruct.m_2bytes = 0;
	p_cex_expected->fixedStruct.m_4bytes = ~0;
	p_cex_expected->fixedStruct.m_8bytes = 0;

#ifndef BUG_4089645
	p_cex_expected->variableStruct.m_str = new char[len + 1];
	for (i = 0; i < len; ++i) {
		if (baseChar == 0)
			baseChar = 1;
		p_cex_expected->variableStruct.m_str[i] = baseChar;
		baseChar <<= 1;
	}
	p_cex_expected->variableStruct.m_str[i] = '\0';
	p_cex_expected->variableStruct.m_1byte = 0x0f;
	p_cex_expected->variableStruct.m_obj =
					typetest::Obj::_duplicate(objref);
#endif // ! BUG_4089645

	p_cex_expected->fixedUnion.m_8bytes(0x0f0f0f0f0f0f0f0f);

	p_cex_expected->variableUnion._d(typetest::v_str);
	p_cex_expected->variableUnion.m_str(new char[len+1]);
	for (i = 0; i < len; ++i) {
		if (baseChar == 0)
			baseChar = 1;
		p_cex_expected->variableUnion.m_str()[i] = baseChar;
		baseChar <<= 1;
	}
	p_cex_expected->variableUnion.m_str()[i] = '\0';
	baseChar = 1;

	// Set exception
	if (set) {
		e.exception(p_cex_expected);
		return (true);
	}

	CORBA::Exception		*p_ex;
	typetest::complexException	*p_cex_actual;

	p_ex = e.exception();
	if (p_ex == NULL) {
		os::printf("ERROR: %s complex_exception not in environment\n",
			msgpfx);
		delete(p_cex_expected);
		return (false);
	}

	// There is an exception verify that it ONLY narrows to the expected
	// type.

	// First the incorrect narrow
	if ((typetest::emptyException::_exnarrow(p_ex)) ||
	    (typetest::basicException::_exnarrow(p_ex)) ||
	    (CORBA::COMM_FAILURE::_exnarrow(p_ex))) {
		os::printf("FAIL: %s Complex exception narrowed to the "
		    "incorrect type.\n", msgpfx);
		return (false);
	}

	if (!typetest::complexException::_exnarrow(p_ex)) {
		os::printf("%s exceptions do not match\n", msgpfx);
		os::printf("\texpected = 0x%x, actual = 0x%x\n",
			p_cex_expected->_major(), p_ex->_major());
		delete(p_cex_expected);
		return (false);
	}

	p_cex_actual = typetest::complexException::_exnarrow(p_ex);

	// Verify Fstruct
	if (p_cex_actual->fixedStruct.m_1byte !=
					p_cex_expected->fixedStruct.m_1byte) {
		os::printf("%s exception fixedStruct.m_1byte does not "
			"match\n", msgpfx);
		os::printf("\texpected = 0x%x, actual = 0x%x\n",
			p_cex_expected->fixedStruct.m_1byte,
			p_cex_actual->fixedStruct.m_1byte);
		passed = false;
	}

	if (p_cex_actual->fixedStruct.m_2bytes !=
					p_cex_expected->fixedStruct.m_2bytes) {
		os::printf("%s exception fixedStruct.m_2bytes does not "
			"match\n", msgpfx);
		os::printf("\texpected = 0x%x, actual = 0x%x\n",
			p_cex_expected->fixedStruct.m_2bytes,
			p_cex_actual->fixedStruct.m_2bytes);
		passed = false;
	}

	if (p_cex_actual->fixedStruct.m_4bytes !=
					p_cex_expected->fixedStruct.m_4bytes) {
		os::printf("%s exception fixedStruct.m_4bytes does not "
			"match\n", msgpfx);
		os::printf("\texpected = 0x%x, actual = 0x%x\n",
			p_cex_expected->fixedStruct.m_4bytes,
			p_cex_actual->fixedStruct.m_4bytes);
		passed = false;
	}

	if (p_cex_actual->fixedStruct.m_8bytes !=
					p_cex_expected->fixedStruct.m_8bytes) {
		os::printf("%s exception fixedStruct.m_8bytes does not "
			"match\n", msgpfx);
		os::printf("\texpected = 0x%x, actual = 0x%x\n",
			p_cex_expected->fixedStruct.m_8bytes,
			p_cex_actual->fixedStruct.m_8bytes);
		passed = false;
	}

#ifndef BUG_4089645
	// Verify Vstruct
	for (i = 0; i < len; ++i) {
		if (baseChar == 0)
			baseChar = 1;
		if (p_cex_actual->variableStruct.m_str[i] != baseChar) {
			os::printf("%s exception variableStruct.m_str[%d] "
				"does not match\n", msgpfx, i);
			os::printf("\texpected = 0x%x, actual = 0x%x\n",
				baseChar,
				p_cex_actual->variableStruct.m_str[i]);
			passed = false;
			break;
		}

		baseChar <<= 1;
	}
	baseChar = 1;

	if (p_cex_actual->variableStruct.m_str[i] != '\0') {
		os::printf("FAIL: %s exception variableStruct.m_str "
			"is not null terminated at index %i\n",
			msgpfx, i);
		passed = false;
	}

	if (p_cex_actual->variableStruct.m_1byte !=
		p_cex_expected->variableStruct.m_1byte) {

		os::printf("%s exception variableStruct.m_1byte does "
			"not match\n", msgpfx);
		os::printf("\texpected = 0x%x, actual = 0x%x\n",
			p_cex_expected->variableStruct.m_1byte,
			p_cex_actual->variableStruct.m_1byte);
		passed = false;
	}

	if (CORBA::is_nil(p_cex_actual->variableStruct.m_obj)) {
		os::printf("FAIL: %s exception variableStruct.m_obj "
			"reference member is NIL\n", msgpfx);
		passed = false;
	} else {
		int32_t act_id, exp_id;
		Environment e2;

		act_id = p_cex_actual->variableStruct.m_obj->id(e2);
		if (e2.exception()) {
			os::printf("FAIL: %s exception "
				"variableStruct.m_obj reference member "
				"had exception in id()\n", msgpfx);

			// Make sure comparison below won't give false result.
			act_id = exp_id = 0;

			passed = false;
		} else {
			exp_id = objref->id(e2);
			if (e2.exception()) {
				os::printf("FAIL: %s object reference had "
					"exception in id()\n", msgpfx);

				// Make sure comparison below won't give
				// false result.
				act_id = exp_id = 0;

				passed = false;
			}
		}

		if (act_id != exp_id) {
			os::printf("FAIL: %s exception "
				"variableStruct.m_obj reference member id:\n",
				msgpfx);
			os::printf("\texpected id = %d, actual id = %d\n",
				exp_id, act_id);
			passed = false;
		}
	}
#endif // ! BUG_4089645

	// Verify Funion
	if (p_cex_actual->fixedUnion._d() != typetest::f_8bytes) {
		os::printf("FAIL: %s exception fixedUnion._d() does "
			"not match\n", msgpfx);
		os::printf("\texpected = 0x%x, actual = 0x%x\n",
			typetest::f_8bytes, p_cex_actual->fixedUnion._d());
		passed = false;
	} else if (p_cex_actual->fixedUnion.m_8bytes() !=
				p_cex_expected->fixedUnion.m_8bytes()) {
		os::printf("FAIL: %s exception fixedUnion.m_8bytes "
			"does not match\n", msgpfx);
		os::printf("\texpected = 0x%x, actual = 0x%x\n",
			p_cex_expected->fixedUnion.m_8bytes(),
			p_cex_actual->fixedUnion.m_8bytes());
		passed = false;
	}

	// Verify Vunion
	if (p_cex_actual->variableUnion._d() !=
					p_cex_expected->variableUnion._d()) {
		os::printf("FAIL: %s exception variableUnion._d() does "
			"not match\n", msgpfx);
		os::printf("\texpected = 0x%x, actual = 0x%x\n",
			p_cex_expected->variableUnion._d(),
			p_cex_actual->variableUnion._d());
		passed = false;
	} else {
		for (i = 0; i < len; ++i) {
			if (p_cex_actual->variableUnion.m_str()[i] !=
				p_cex_expected->variableUnion.m_str()[i]) {
				os::printf("%s exception "
					"variableUnion.m_str[%d] does not "
					"match\n", msgpfx, i);
				os::printf("\texpected = 0x%x, actual = 0x%x\n",
				    p_cex_expected->variableUnion.m_str()[i],
				    p_cex_actual->variableUnion.m_str()[i]);
				passed = false;
				break;
			}
		}
		if (p_cex_actual->variableUnion.m_str()[i] != '\0') {
			os::printf("FAIL: %s exception variableStruct.m_str "
				"is not null terminated at index %d\n",
				msgpfx, i);
			passed = false;
		}
	} // Correct discrim type

	delete(p_cex_expected);
	return (passed);
}
