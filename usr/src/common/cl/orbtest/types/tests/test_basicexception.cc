/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_basicexception.cc	1.17	08/05/20 SMI"

//
// Exception test
//

#include <orbtest/types/client_impl.h>
#include <orbtest/types/server_impl.h>

//
// Forward Definitions
//
static bool
set_exception(Environment &e, const char *msgpfx, bool set = true);

inline bool
verify_exception(Environment &e, const char *msgpfx)
{
	return (set_exception(e, msgpfx, false));
}

//
// Test client interface.  Invoked by driver "programs" and, in turn,
// invokes the corresponding method on the test server side.
// Parameters:
//	server_ref  --	test server object to invoke.
//	num_repeat  --	number of times that we'll execute test
//	noverify    --	(ignored)
// Returns:
//	Total test execution time.
//
int64_t
client_impl::test_basicexception(
		typetest::server_ptr server_ref,
		int32_t num_repeat,
		bool,
		Environment &)
{
	// os::printf ("%s test_basicexception()\n",msg_prefix());
	os::hrtime_t total_time = 0, start_time;

	int i;

	for (i = 0; i < num_repeat; ++i) {
		Environment e2;

		// Invoke server
		start_time = os::gethrtime();

		server_ref->test_basicexception(e2);
		total_time += os::gethrtime() - start_time;

		// Verify that the exception raised
		if (!verify_exception(e2, msg_prefix())) {
			total_time = -1;
			break;
		}

		// Clear the exception
		e2.clear();
	}

	return (total_time);
}

//
// Test server interface.  Invoked by the test client.
//
void
server_impl::test_basicexception(Environment &e)
{
	set_exception(e, msg_prefix());
}

//
// Set Exception
//
bool
set_exception(Environment &e, const char *msgpfx, bool set)
{
	CORBA::Exception		*p_ex;
	typetest::basicException	*p_bex_expected;
	bool				passed = true;

	p_bex_expected = new typetest::basicException(
		0xFF,
		0x8888,
		0x44444444,
		0x2222222222222222);

	// Set exception
	if (set) {
		e.exception(p_bex_expected);
		return (true);
	}

	// Verify Exception
	{
		typetest::basicException *p_bex_actual;

		p_ex = e.exception();
		if (p_ex == NULL) {
			os::printf("%s basic_exception not in environment\n",
				msgpfx);
			delete(p_bex_expected);
			return (false);
		}

		// There is an exception, verify that it ONLY narrows to
		// the expected type.

		// First the incorrect narrow
		if ((typetest::emptyException::_exnarrow(p_ex)) ||
		    (typetest::complexException::_exnarrow(p_ex)) ||
		    (CORBA::COMM_FAILURE::_exnarrow(p_ex))) {
			os::printf("FAIL: %s Basic exception narrowed to the "
			    "incorrect type.\n", msgpfx);
			return (false);
		}

		if (!typetest::basicException::_exnarrow(p_ex)) {
			os::printf("%s exceptions do not match\n", msgpfx);
			os::printf("\texpected = 0x%x, actual = 0x%x\n",
				p_bex_expected->_major(), p_ex->_major());
			delete(p_bex_expected);
			return (passed);
		}

		p_bex_actual = typetest::basicException::_exnarrow(p_ex);

		if (p_bex_actual->m_1byte != p_bex_expected->m_1byte) {
			os::printf("%s exception m_1byte does not match\n",
				msgpfx);
			os::printf("\texpected = 0x%x, actual = 0x%x\n",
				p_bex_expected->m_1byte,
				p_bex_actual->m_1byte);
			passed = false;
		}

		if (p_bex_actual->m_2bytes != p_bex_expected->m_2bytes) {
			os::printf("%s exception m_2byte does not match\n",
				msgpfx);
			os::printf("\texpected = 0x%x, actual = 0x%x\n",
				p_bex_expected->m_2bytes,
				p_bex_actual->m_2bytes);
			passed = false;
		}

		if (p_bex_actual->m_4bytes != p_bex_expected->m_4bytes) {
			os::printf("%s exception m_4byte does not match\n",
				msgpfx);
			os::printf("\texpected = 0x%x, actual = 0x%x\n",
				p_bex_expected->m_4bytes,
				p_bex_actual->m_4bytes);
			passed = false;
		}

		if (p_bex_actual->m_8bytes != p_bex_expected->m_8bytes) {
			os::printf("%s exception m_8byte does not match\n",
				msgpfx);
			os::printf("\texpected = 0x%x, actual = 0x%x\n",
				p_bex_expected->m_8bytes,
				p_bex_actual->m_8bytes);
			passed = false;
		}
	} // Verification

	delete(p_bex_expected);
	return (passed);
}
