/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_objseq.cc	1.15	08/05/20 SMI"

//
// Object reference sequence test
//

#include <orbtest/types/client_impl.h>
#include <orbtest/types/server_impl.h>


//
// Test client interface.  Invoked by driver "programs" and, in turn,
// invokes the corresponding method on the test server side.
// Parameters:
//	server_ref  --	test server object to invoke.
//	num_repeat  --	how many times to run the test.
//	len	    --	length of sequence (number of object references)
//			to test.
//	noverify    --	if true then server won't perform data verification.
// Returns:
//	Total test execution time.
//
// BUG: template _Ix_InterfaceSeq_ in orb/ix_tmpl.h, copy() member
// function won't compile: uses _buf instead of buf.
int64_t
client_impl::test_objseq(
		typetest::server_ptr server_ref,
		int32_t num_repeat,
		uint32_t len,
		bool noverify,
		Environment &e)
{
	// os::printf("%s test_objseq()\n", msg_prefix());
	os::hrtime_t	total_time = 0, start_time;
	const uint32_t	timeout = 30;
	int32_t		idnum, i;

	if (noverify) {
		Environment	e3;

		server_ref->set_noverify(e3);
		if (! env_ok(e3, msg_prefix())) {
			return (-1);
		}
	}

	for (; num_repeat > 0; --num_repeat) {
		Environment	e2;
		Obj_impl *in_impls = new Obj_impl[len];
		Obj_impl *inout_impls = new Obj_impl[len];
		typetest::Objseq *inp = new typetest::Objseq(len, len);
		typetest::Objseq *inoutp = new typetest::Objseq(len, len);
		typetest::Objseq *outp = 0, *retp = 0;

		// Prepare 'in' and 'inout' sequences for server
		for (i = 0; i < len; ++i) {
			in_impls[i].set_id(i);
			(*inp)[i] = in_impls[i].get_objref();

			inout_impls[i].set_id(len - i);
			(*inoutp)[i] = inout_impls[i].get_objref();
		}


		// Invoke server.
		start_time = os::gethrtime();
		retp = server_ref->test_objseq(*inp, outp, *inoutp, len, e2);
		if (! env_ok(e2, msg_prefix())) {
			total_time = -1;
			num_repeat = 0;			// end loop
		} else {
			total_time += os::gethrtime() - start_time;

			// Verify 'out' sequence from server; should be the
			// same as the 'inout' sequence above.
			if (outp->maximum() != len) {
				os::printf("FAIL: %s maximum of 'out' "
					"sequence:\n", msg_prefix());
				os::printf("\texpected = %d, actual = %d\n",
					len, outp->maximum());
			}
			if (outp->length() != len) {
				os::printf("FAIL: %s length of 'out' "
					"sequence:\n", msg_prefix());
				os::printf("\texpected = %d, actual = %d\n",
					len, outp->length());
			} else {
				for (i = 0; i < len; ++i) {
					idnum = (*outp)[i]->id(e2);
					e2.clear();
					if (idnum == len - i)
						continue;
					os::printf("FAIL: %s object reference "
						"at index %d of 'out' "
						"sequence:\n",
						msg_prefix(), i);
					os::printf("\texpected id = %d, "
						"actual id = %d\n",
						len - i, idnum);
					break;
				}
			}


			// Verify 'inout' sequence from server; should be
			// the same as the 'in' sequence above.
			if (inoutp->maximum() != len) {
				os::printf("FAIL: %s maximum of 'inout' "
					"sequence:\n", msg_prefix());
				os::printf("\texpected = %d, actual = %d\n",
					len, inoutp->maximum());
			}
			if (inoutp->length() != len) {
				os::printf("FAIL: %s length of 'inout' "
					"sequence:\n", msg_prefix());
				os::printf("\texpected = %d, actual = %d\n",
					len, inoutp->length());
			} else {
				for (i = 0; i < len; ++i) {
					idnum = (*inoutp)[i]->id(e2);
					e2.clear();
					if (idnum == i)
						continue;
					os::printf("FAIL: %s object reference "
						"at index %d of 'inout' "
						"sequence:\n",
						msg_prefix(), i);
					os::printf("\texpected id = %d, "
						"actual id = %d\n",
						i, idnum);
					break;
				}
			}


			// Verify returned sequence from server; should be
			// the same as the 'in' sequence above.
			if (retp->maximum() != len) {
				os::printf("FAIL: %s maximum of returned "
					"sequence:\n", msg_prefix());
				os::printf("\texpected = %d, actual = %d\n",
					len, retp->maximum());
			}
			if (retp->length() != len) {
				os::printf("FAIL: %s length of returned "
					"sequence:\n", msg_prefix());
				os::printf("\texpected = %d, actual = %d\n",
					len, retp->length());
			} else {
				for (i = 0; i < len; ++i) {
					idnum = (*retp)[i]->id(e2);
					e2.clear();
					if (idnum == i)
						continue;
					os::printf("FAIL: %s object reference "
						"at index %d of returned "
						"sequence:\n",
						msg_prefix(), i);
					os::printf("\texpected id = %d, "
						"actual id = %d\n",
						i, idnum);
					break;
				}
			}
		}

		// Release all objects.
		delete inp;
		delete outp;
		delete inoutp;
		delete retp;

		//
		// Wait until _unreferenced() is called on each of the
		// implementation objects.  Use a timeout waiting for the
		// _unreferenced() so we can sort of verify that object
		// references are properly released as they are passed
		// around.  If the timeout expires before the _unreferenced()
		// call, print a failure message (after this I don't know
		// what will happen -- kernel modules may panic, user level
		// programs may core dump -- since the object implementations
		// will be destroyed with references still dangling).  Make
		// sure the timeout value is sufficiently large since
		// _unreferenced() is delivered asynchronously.
		//
		for (i = 0; i < len; ++i) {
			if (! in_impls[i].wait_until_unreferenced(timeout)) {
				os::printf("FAIL: %s _unreferenced() (of "
					"in_impls[%d]) not called within "
					"%d seconds\n",
					msg_prefix(), i, timeout);
			}

			if (! inout_impls[i].wait_until_unreferenced(timeout)) {
				os::printf("FAIL: %s _unreferenced() (of "
					"inout_impls[%d]) not called within "
					"%d seconds\n",
					msg_prefix(), i, timeout);
			}
		}
		delete[] in_impls;
		delete[] inout_impls;
	}

	return (total_time);
}

//
// Test server interface.  Invoked by the test client.
//
typetest::Objseq *
server_impl::test_objseq(
	const typetest::Objseq	&in,
	typetest::Objseq_out	outp,
	typetest::Objseq	&inout,
	uint32_t		len,
	Environment		&)
{
	// os::printf("%s test_objseq()\n", msg_prefix());
	int32_t		idnum;
	int32_t		i;

	// A nested invocation requires its own Environment
	Environment	env;

	if (!verify_off) {
		// Verify 'in' sequence from client.
		if (in.maximum() != len) {
			os::printf("FAIL: %s maximum of 'in' sequence:\n",
				msg_prefix());
			os::printf("\texpected = %d, actual = %d\n",
				len, in.maximum());
		}
		if (in.length() != len) {
			os::printf("FAIL: %s length of 'in' sequence:\n",
				msg_prefix());
			os::printf("\texpected = %d, actual = %d\n",
				len, in.length());
		} else {
			for (i = 0; i < len; ++i) {

				idnum = in[i]->id(env);
				env.clear();

				if (idnum != i) {
					os::printf("FAIL: %s object reference "
						"at index %d of 'in' "
						"sequence:\n",
						msg_prefix(), i);
					os::printf("\texpected id = %d, "
						"actual id = %d\n", i, idnum);
					break;
				}
			}
		}


		// Verify 'inout' sequence from client.
		if (inout.maximum() != len) {
			os::printf("FAIL: %s maximum of 'inout' sequence:\n",
				msg_prefix());
			os::printf("\texpected = %d, actual = %d\n",
				len, inout.maximum());
		}
		if (inout.length() != len) {
			os::printf("FAIL: %s length of 'inout' sequence:\n",
				msg_prefix());
			os::printf("\texpected = %d, actual = %d\n",
				len, inout.length());
		} else {
			for (i = 0; i < len; ++i) {

				idnum = inout[i]->id(env);
				env.clear();

				if (idnum != len - i) {
					os::printf("FAIL: %s object reference "
						"at index %d of 'inout' "
						"sequence:\n",
						msg_prefix(), i);
					os::printf("\texpected id = %d, "
						"actual id = %d\n",
						len - i, idnum);
					break;
				}
			}
		}
	}

	// Prepare 'out' sequence for client.
	outp = new typetest::Objseq(inout);  	// copy inout to out

	// Prepare a new 'inout' sequence for client.
	inout = in;				// copy in into inout

	// Prepare and return a sequence to the client.
	return (new typetest::Objseq(in));	// copy in to returned object
}
