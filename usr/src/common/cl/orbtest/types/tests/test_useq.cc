/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_useq.cc	1.10	08/05/20 SMI"

//
// Unbounded sequence test
//

#include <orbtest/types/client_impl.h>
#include <orbtest/types/server_impl.h>


static bool	set_in(typetest::Useq &var, uint32_t len, const char *msgpfx,
			bool set = true);
static bool	set_out(typetest::Useq &var, uint32_t len, const char *msgpfx,
			bool set = true);
static bool	set_io_i(typetest::Useq &var, uint32_t len, const char *msgpfx,
			bool set = true);
static bool	set_io_o(typetest::Useq &var, uint32_t len, const char *msgpfx,
			bool set = true);
static bool	set_ret(typetest::Useq &var, uint32_t len, const char *msgpfx,
			bool set = true);

inline bool
verify_in(const typetest::Useq &v, uint32_t l, const char *p)
{
	return (set_in((typetest::Useq &)v, l, p, false));
}

inline bool
verify_out(const typetest::Useq &v, uint32_t l, const char *p)
{
	return (set_out((typetest::Useq &)v, l, p, false));
}

inline bool
verify_io_i(const typetest::Useq &v, uint32_t l, const char *p)
{
	return (set_io_i((typetest::Useq &)v, l, p, false));
}

inline bool
verify_io_o(const typetest::Useq &v, uint32_t l, const char *p)
{
	return (set_io_o((typetest::Useq &)v, l, p, false));
}

inline bool
verify_ret(const typetest::Useq &v, uint32_t l, const char *p)
{
	return (set_ret((typetest::Useq &)v, l, p, false));
}


//
// Test client interface.  Invoked by driver "programs" and, in turn,
// invokes the corresponding method on the test server side.
// Parameters:
//	server_ref  --	test server object to invoke.
//	num_repeat  --	how many times to run the test.
//	len	    --	length of unbounded sequence to test.
//	noverify    --	if true then server won't perform data verification.
// Returns:
//	Total test execution time.
//
int64_t
client_impl::test_useq(
		typetest::server_ptr server_ref,
		int32_t num_repeat,
		uint32_t len,
		bool noverify,
		Environment &)
{
	// os::printf("%s test_useq()\n", msg_prefix());
	os::hrtime_t	total_time = 0, start_time;

	if (noverify) {
		Environment	e3;

		server_ref->set_noverify(e3);
		if (! env_ok(e3, msg_prefix())) {
			return (-1);
		}
	}

	for (; num_repeat > 0; --num_repeat) {
		Environment		e2;
		typetest::Useq		in, inout;
		typetest::Useq_var	outp, retp;

		// Prepare data to be passed to server.
		set_in(in, len, msg_prefix());
		set_io_i(inout, len, msg_prefix());

		// Invoke server.
		start_time = os::gethrtime();
		retp = server_ref->test_useq(in, outp, inout, len, e2);
		if (! env_ok(e2, msg_prefix())) {
			total_time = -1;
			break;
		}
		total_time += os::gethrtime() - start_time;

		// Verify values returned by server.
		verify_out(*outp, len, msg_prefix());
		verify_io_o(inout, len, msg_prefix());
		verify_ret(*retp, len, msg_prefix());
	}

	return (total_time);
}


//
// Test server interface.  Invoked by the test client.
//
typetest::Useq *
server_impl::test_useq(
		const typetest::Useq	&in,
		typetest::Useq_out	outp,
		typetest::Useq		&inout,
		uint32_t len,
		Environment &)
{
	// os::printf("%s test_useq()\n", msg_prefix());
	typetest::Useq	*retp;		// don't use _var!

	// Verify values passed by client.
	if (!verify_off) {
		verify_in(in, len, msg_prefix());
		verify_io_i(inout, len, msg_prefix());
	}

	// Prepare values to be returned to client.
	outp = new typetest::Useq;
	set_out(*outp, len, msg_prefix());
	set_io_o(inout, len, msg_prefix());
	retp = new typetest::Useq;
	set_ret(*retp, len, msg_prefix());

	return (retp);
}


//
// Sets up (or verifies if "set" argument is false) 'in' parameter.
//
bool
set_in(typetest::Useq &var, uint32_t len, const char *msgpfx, bool set)
{
	typetest::Useq	base(1);	// this way we don't have to know
					// the type of the sequence members
	base[0] = 0;

	if (set) {
		var.load(len, len, var.allocbuf(len), true);
		for (uint32_t i = 0; i < var.length(); ++i) {
			var[i] = base[0];
			base[0] = (base[0] == 0) ? 1 : base[0] << 1;
		}
		return (true);
	}

	// Verify data.
	if (var.maximum() != len) {
		os::printf("FAIL: %s maximum of 'in' unbounded sequence:\n",
			msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			len, var.maximum());
	}

	if (var.length() != len) {
		os::printf("FAIL: %s length of 'in' unbounded sequence:\n",
			msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			len, var.length());
		return (false);
	}

	for (uint32_t i = 0; i < var.length(); ++i) {
		if (var[i] == base[0]) {
			base[0] = (base[0] == 0) ? 1 : base[0] << 1;
			continue;
		}

		os::printf("FAIL: %s value of 'in' unbounded sequence "
			"at index %d:\n", msgpfx, i);
		os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
			(uint64_t)base[0], (uint64_t)var[i]);
		return (false);
	}
	return (true);
}


//
// Sets up (or verifies if "set" argument is false) 'out' parameter.
//
bool
set_out(typetest::Useq &var, uint32_t len, const char *msgpfx, bool set)
{
	typetest::Useq	base(1);	// this way we don't have to know
					// the type of the sequence members
	base[0] = ~0;

	if (set) {
		var.load(len + 1, len, var.allocbuf(len + 1), true);
		for (uint32_t i = 0; i < var.length(); ++i) {
			var[i] = base[0];
			base[0] = (base[0] == 0) ? ~0 : base[0] >> 1;
		}
		return (true);
	}

	// Verify data.
	if (var.maximum() != len + 1) {
		os::printf("FAIL: %s maximum of 'out' unbounded sequence:\n",
			msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			len + 1, var.maximum());
	}

	if (var.length() != len) {
		os::printf("FAIL: %s length of 'out' unbounded sequence:\n",
			msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			len, var.length());
		return (false);
	}

	for (uint32_t i = 0; i < var.length(); ++i) {
		if (var[i] == base[0]) {
			base[0] = (base[0] == 0) ? ~0 : base[0] >> 1;
			continue;
		}

		os::printf("FAIL: %s value of 'out' unbounded sequence "
			"at index %d:\n", msgpfx, i);
		os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
			(uint64_t)base[0], (uint64_t)var[i]);
		return (false);
	}
	return (true);
}


//
// Sets up (or verifies if "set" argument is false) incoming 'inout' parameter.
//
bool
set_io_i(typetest::Useq &var, uint32_t len, const char *msgpfx, bool set)
{
	typetest::Useq	base(1);	// this way we don't have to know
					// the type of the sequence members
	base[0] = ~0;

	if (set) {
		var.load(len, len, var.allocbuf(len), true);
		for (uint32_t i = 0; i < var.length(); ++i) {
			var[i] = base[0];
			base[0] = (base[0] == 0) ? ~0 : base[0] << 1;
		}
		return (true);
	}

	// Verify data.
	if (var.maximum() != len) {
		os::printf("FAIL: %s maximum of 'inout' "
			"unbounded sequence:\n", msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			len, var.maximum());
	}

	if (var.length() != len) {
		os::printf("FAIL: %s length of 'inout' "
			"unbounded sequence:\n", msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			len, var.length());
		return (false);
	}

	for (uint32_t i = 0; i < var.length(); ++i) {
		if (var[i] == base[0]) {
			base[0] = (base[0] == 0) ? ~0 : base[0] << 1;
			continue;
		}

		os::printf("FAIL: %s value of 'inout' unbounded sequence "
			"at index %d:\n", msgpfx, i);
		os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
			(uint64_t)base[0], (uint64_t)var[i]);
		return (false);
	}
	return (true);
}


//
// Sets up (or verifies if "set" argument is false) outgoing 'inout' parameter.
//
bool
set_io_o(typetest::Useq &var, uint32_t len, const char *msgpfx, bool set)
{
	typetest::Useq	base(1);	// this way we don't have to know
					// the type of the sequence members
	base[0] = 0;

	if (set) {
		var.load(len + 1, len + 1, var.allocbuf(len + 1), true);
		for (uint32_t i = 0; i < var.length(); ++i) {
			var[i] = base[0];
			base[0] = (base[0] == 0) ? ~0 : base[0] >> 1;
		}
		return (true);
	}

	// Verify data.
	if (var.maximum() != len + 1) {
		os::printf("FAIL: %s maximum of 'inout' "
			"unbounded sequence:\n", msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			len + 1, var.maximum());
	}

	if (var.length() != len + 1) {
		os::printf("FAIL: %s length of 'inout' "
			"unbounded sequence:\n", msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			len + 1, var.length());
		return (false);
	}

	for (uint32_t i = 0; i < var.length(); ++i) {
		if (var[i] == base[0]) {
			base[0] = (base[0] == 0) ? ~0 : base[0] >> 1;
			continue;
		}

		os::printf("FAIL: %s value of 'inout' unbounded sequence "
			"at index %d:\n", msgpfx, i);
		os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
			(uint64_t)base[0], (uint64_t)var[i]);
		return (false);
	}
	return (true);
}


//
// Sets up (or verifies if "set" argument is false) returned value.
//
bool
set_ret(typetest::Useq &var, uint32_t len, const char *msgpfx, bool set)
{
	typetest::Useq	base(1);	// this way we don't have to know
					// the type of the sequence members
	base[0] = 0;

	if (set) {
		var.load(len, len, var.allocbuf(len), true);
		for (uint32_t i = 0; i < var.length(); ++i) {
			var[i] = base[0];
			base[0] = ~base[0];
		}
		return (true);
	}

	// Verify data.
	if (var.maximum() != len) {
		os::printf("FAIL: %s maximum of returned "
			"unbounded sequence:\n", msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			len, var.maximum());
	}

	if (var.length() != len) {
		os::printf("FAIL: %s length of returned "
			"unbounded sequence:\n", msgpfx);
		os::printf("\texpected = %d, actual = %d\n",
			len, var.length());
		return (false);
	}

	for (uint32_t i = 0; i < var.length(); ++i) {
		if (var[i] == base[0]) {
			base[0] = ~base[0];
			continue;
		}

		os::printf("FAIL: %s value of returned unbounded sequence at "
			"index %d:\n", msgpfx, i);
		os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
			(uint64_t)base[0], (uint64_t)var[i]);
		return (false);
	}
	return (true);
}
