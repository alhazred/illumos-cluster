/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_long.cc	1.10	08/05/20 SMI"

//
// Long (int32_t) test
//

#include <orbtest/types/client_impl.h>
#include <orbtest/types/server_impl.h>


static bool	set_in(int32_t &var, const char *msgpfx, bool set = true);
static bool	set_out(int32_t &var, const char *msgpfx, bool set = true);
static bool	set_io_i(int32_t &var, const char *msgpfx, bool set = true);
static bool	set_io_o(int32_t &var, const char *msgpfx, bool set = true);
static bool	set_ret(int32_t &var, const char *msgpfx, bool set = true);

inline bool
verify_in(const int32_t &v, const char *p)
{
	return (set_in((int32_t &)v, p, false));
}

inline bool
verify_out(const int32_t &v, const char *p)
{
	return (set_out((int32_t &)v, p, false));
}

inline bool
verify_io_i(const int32_t &v, const char *p)
{
	return (set_io_i((int32_t &)v, p, false));
}

inline bool
verify_io_o(const int32_t &v, const char *p)
{
	return (set_io_o((int32_t &)v, p, false));
}

inline bool
verify_ret(const int32_t &v, const char *p)
{
	return (set_ret((int32_t &)v, p, false));
}


//
// Test client interface.  Invoked by driver "programs" and, in turn,
// invokes the corresponding method on the test server side.
// Parameters:
//	server_ref  --	test server object to invoke.
//	num_repeat  --	how many times to run the test.
//	noverify    --	if true then server won't perform data verification.
// Returns:
//	Total test execution time.
//
int64_t
client_impl::test_long(
		typetest::server_ptr server_ref,
		int32_t num_repeat,
		bool noverify,
		Environment &)
{
	// os::printf("%s test_long()\n", msg_prefix());
	os::hrtime_t	total_time = 0, start_time;

	if (noverify) {
		Environment	e2;

		server_ref->set_noverify(e2);
		if (! env_ok(e2, msg_prefix())) {
			return (-1);
		}
	}

	for (; num_repeat > 0; --num_repeat) {
		Environment	e2;
		int32_t		in, out, inout, ret;

		// Prepare data to be passed to server.
		set_in(in, msg_prefix());
		set_io_i(inout, msg_prefix());

		// Invoke server.
		start_time = os::gethrtime();
		ret = server_ref->test_long(in, out, inout, e2);
		if (! env_ok(e2, msg_prefix())) {
			total_time = -1;
			break;
		}
		total_time += os::gethrtime() - start_time;

		// Verify values returned by server.
		verify_out(out, msg_prefix());
		verify_io_o(inout, msg_prefix());
		verify_ret(ret, msg_prefix());
	}

	return (total_time);
}


//
// Test server interface.  Invoked by the test client.
//
int32_t
server_impl::test_long(
		int32_t  in,
		int32_t &out,
		int32_t &inout,
		Environment &)
{
	// os::printf("%s test_long()\n", msg_prefix());
	int32_t	ret;

	// Verify values passed by client.
	if (!verify_off) {
		verify_in(in, msg_prefix());
		verify_io_i(inout, msg_prefix());
	}

	// Prepare values to be returned to client.
	set_out(out, msg_prefix());
	set_io_o(inout, msg_prefix());
	set_ret(ret, msg_prefix());

	return (ret);
}


//
// Sets up (or verifies if "set" argument is false) 'in' parameter.
//
bool
set_in(int32_t &var, const char *msgpfx, bool set)
{
	const int32_t	base = 0xfedcba98;

	if (set) {
		var = base;
		return (true);
	}

	// Verify data.
	if (var == base)
		return (true);

	os::printf("FAIL: %s Long (int32_t) 'in' value:\n", msgpfx);
	os::printf("\texpected = 0x%lx, actual = 0x%lx\n", base, var);
	return (false);
}


//
// Sets up (or verifies if "set" argument is false) 'out' parameter.
//
bool
set_out(int32_t &var, const char *msgpfx, bool set)
{
	const int32_t	base = 0x89abcdef;

	if (set) {
		var = base;
		return (true);
	}

	// Verify data.
	if (var == base)
		return (true);

	os::printf("FAIL: %s Long (int32_t) 'out' value:\n", msgpfx);
	os::printf("\texpected = 0x%lx, actual = 0x%lx\n", base, var);
	return (false);
}


//
// Sets up (or verifies if "set" argument is false) incoming 'inout' parameter.
//
bool
set_io_i(int32_t &var, const char *msgpfx, bool set)
{
	const int32_t	base = 0x98765432;

	if (set) {
		var = base;
		return (true);
	}

	// Verify data.
	if (var == base)
		return (true);

	os::printf("FAIL: %s Long (int32_t) 'inout' value:\n", msgpfx);
	os::printf("\texpected = 0x%lx, actual = 0x%lx\n", base, var);
	return (false);
}


//
// Sets up (or verifies if "set" argument is false) outgoing 'inout' parameter.
//
bool
set_io_o(int32_t &var, const char *msgpfx, bool set)
{
	const int32_t	base = 0x12345678;

	if (set) {
		var = base;
		return (true);
	}

	// Verify data.
	if (var == base)
		return (true);

	os::printf("FAIL: %s Long (int32_t) 'inout' value:\n", msgpfx);
	os::printf("\texpected = 0x%lx, actual = 0x%lx\n", base, var);
	return (false);
}


//
// Sets up (or verifies if "set" argument is false) returned value.
//
bool
set_ret(int32_t &var, const char *msgpfx, bool set)
{
	const int32_t	base = 0x96f6e7a3;

	if (set) {
		var = base;
		return (true);
	}

	// Verify data.
	if (var == base)
		return (true);

	os::printf("FAIL: %s Long (int32_t) returned value:\n", msgpfx);
	os::printf("\texpected = 0x%lx, actual = 0x%lx\n", base, var);
	return (false);
}
