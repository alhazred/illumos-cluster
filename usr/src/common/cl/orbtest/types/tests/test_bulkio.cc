/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_bulkio.cc	1.30	08/05/20 SMI"

//
// Bulkio Raw I/O Tests
//

#if !defined(_UNODE)

#include <stdio.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <sys/stream.h>
#ifdef ORBTEST_V1
#include <h/typetest_1.h>
#else
#include <h/typetest_0.h>
#endif
#include <orbtest/types/client_impl.h>
#include <orbtest/types/server_impl.h>

#if defined(_KERNEL)
#include <pxfs/bulkio/bulkio_impl.h>
#endif

static bulkio::in_uio_ptr set_in(uio_t *base, const char *msgpfx);
static bulkio::inout_uio_ptr set_inout(uio_t *base, const char *msgpfx);
static bool verify_in(bulkio::in_uio_ptr uioobj, uio_t *base,
		const char *msgpfx);
static bool verify_inout(bulkio::inout_uio_ptr uioobj, uio_t *base,
		const char *msgpfx);
static uio_t *set_uio(size_t len);
static void uio_delete(uio_t *uio_p);

//
// invoke_bulkio_uio_in - This actually calls the uio_in server routine
//
os::hrtime_t
invoke_bulkio_uio_in(
		typetest::server_ptr server_ref,
		uio_t *base,
		uint32_t len,
		const char *msgpfx)
{
	Environment		e2;
	os::hrtime_t		start_time;
	bulkio::in_uio_var	in;

	// Prepare data to be passed to server.
	in = set_in(base, msgpfx);
	ASSERT(!CORBA::is_nil(server_ref));

	// Invoke the server
	start_time = os::gethrtime();
	server_ref->test_bulkio_uio_in(in, len, e2);
	if (! env_ok(e2, msgpfx)) {
		return (-1);
	}
	return (os::gethrtime() - start_time);
}

//
// test_bulkio_uio_in - tests the "writes" operation.
// Test client interface.  Invoked by driver "programs" and, in turn,
// invokes the corresponding method on the test server side.
// Parameters:
//	server_ref  --	test server object to invoke.
//	num_repeat  --	how many times to run the test.
//	len	    --	length of data passed.
//	noverify    --	if true then server won't perform data verification.
// Returns:
//	Total test execution time when test succeeds.
//	-1 when test fails.
//
#if defined(_KERNEL)
int64_t
client_impl::test_bulkio_uio_in(
		typetest::server_ptr server_ref,
		int32_t num_repeat,
		uint32_t len,
		bool noverify,
		Environment &)
{
	uio_t		*base;
	os::hrtime_t	ret_time;
	os::hrtime_t	total_time = 0;

	base = set_uio((size_t)len);
	if (noverify) {
		Environment	e2;

		server_ref->set_noverify(e2);
		if (! env_ok(e2, msg_prefix())) {
			return (-1);
		}
	}

	for (; num_repeat > 0; --num_repeat) {
		ret_time = invoke_bulkio_uio_in(
				server_ref, base, len, msg_prefix());
		if (ret_time == -1) {
			break;
		}
		total_time += ret_time;
	}

	// free the stuff held by in
	uio_delete(base);
	return (total_time);
}
#else
int64_t
client_impl::test_bulkio_uio_in(
		typetest::server_ptr,
		int32_t,
		uint32_t,
		bool,
		Environment &)
{
	(void) printf("This test does nothing for user/unode cases\n");
	return (0);
}
#endif

//
// invoke_bulkio_uio_inout - This actually calls the server routine.
//
os::hrtime_t
invoke_bulkio_uio_inout(
		typetest::server_ptr	server_ref,
		uio_t			*base,
		uint32_t		len,
		const char		*msgpfx)
{
	Environment		e2;
	os::hrtime_t		start_time;
	os::hrtime_t		total_time;
	bulkio::inout_uio_var	inout;

	//
	// Prepare the inout object to be passed to server.
	// This will be 'filled' by the server and returned.
	//
	inout = set_inout(base, msgpfx);
	ASSERT(!CORBA::is_nil(server_ref));

	// Invoke the server
	start_time = os::gethrtime();
	server_ref->test_bulkio_uio_inout(inout.INOUT(), len, e2);
	if (! env_ok(e2, msgpfx)) {
		return (-1);
	}
	total_time = os::gethrtime() - start_time;

	// Verify values returned by server.
	if (!verify_inout(inout, base, msgpfx)) {
		return (-1);
	}

	return (total_time);
}

//
// test_bulkio_uio_inout - tests the "read" operation.
// Test client interface.  Invoked by driver "programs" and, in turn,
// invokes the corresponding method on the test server side.
// Parameters:
//	server_ref  --	test server object to invoke.
//	num_repeat  --	how many times to run the test.
//	len	    --	length of data passed.
//	noverify    --	if true then server won't perform data verification.
// Returns:
//	Total test execution time when test succeeds.
//	-1 when test fails.
//
#if defined(_KERNEL)
int64_t
client_impl::test_bulkio_uio_inout(
		typetest::server_ptr	server_ref,
		int32_t			num_repeat,
		uint32_t		len,
		bool			noverify,
		Environment		&)
{
	uio_t		*base;
	os::hrtime_t	ret_time;
	os::hrtime_t	total_time = 0;

	base = set_uio((size_t)len);
	if (noverify) {
		Environment	e2;

		server_ref->set_noverify(e2);
		if (! env_ok(e2, msg_prefix())) {
			return (-1);
		}
	}

	for (; num_repeat > 0; --num_repeat) {
		ret_time = invoke_bulkio_uio_inout(
				server_ref, base, len, msg_prefix());
		if (ret_time == -1) {
			break;
		}
		total_time += ret_time;
	}

	// free the stuff held by inout
	uio_delete(base);
	return (total_time);
}
#else
int64_t
client_impl::test_bulkio_uio_inout(
		typetest::server_ptr,
		int32_t,
		uint32_t,
		bool,
		Environment &)
{
	(void) printf("This test does nothing for user/unode cases\n");
	return (0);
}
#endif

//
// test_bulkio_uio_in - server side support for write operation test.
//
void
server_impl::test_bulkio_uio_in(
	bulkio::in_uio_ptr	in,
	uint32_t		len,
	Environment		&)
{
	if (!verify_off) {
		//
		// Create a uio structure containing data with
		// a specific data pattern.
		// This data structure will be compared against
		// the data structure actually received.
		//
		uio_t	*base = set_uio((size_t)len);

		//
		// Verify values passed by client against the
		// expected data pattern in structure "base".
		//
		verify_in(in, base, msg_prefix());

		// Destroy the data structure that was used for testing.
		uio_delete(base);
	}
}

//
// test_bulkio_uio_inout - server side support for read operation test.
//
#ifdef _KERNEL
void
server_impl::test_bulkio_uio_inout(
		bulkio::inout_uio_ptr	&inout,
		uint32_t		len,
		Environment		&)
{
	uio_t		*uiop;

	uiop = bulkio_impl::conv(inout);

	if (!verify_off) {
		//
		// Verify is enabled.
		//

		// Currently only support 1 I/O vector
		ASSERT(uiop->uio_iovcnt == 1);

		//
		// Write the data pattern,
		// which must match that expected by the client.
		//
		iovec_t		*iov = uiop->uio_iov;
		iov->iov_len = len;
		unsigned char	*buffer = (unsigned char *)iov->iov_base;
		for (int i = 0; i < len; i++) {
			buffer[i] = (unsigned char)(i % 8);
		}
	}
	// Adjust the uio structure
	uioskip(uiop, (size_t)len);
}
#else
void
server_impl::test_bulkio_uio_inout(bulkio::inout_uio_ptr &, uint32_t,
    Environment &)
{
	// This should never be called. The client_impl never makes the call.
	ASSERT(0);
}
#endif	// _KERNEL

//
// set_uio - creates and initializes a uio_t structure.
// bulkio uses this kind of data structure to locate the data
// buffers.
//
uio_t *
set_uio(size_t len)
{
	static uio_t	*res = NULL;
	unsigned char	*buffer;
	unsigned int	i;
	iovec_t		*iov;

	//
	// Write the data pattern
	// The data pattern is fixed
	//
	buffer = (unsigned char *) new unsigned char[len];
	for (i = 0; i < len; i++) {
		buffer[i] = (unsigned char) (i % 8);
	}

	// Set up the iovec_t
	iov = new iovec_t[1];
	iov->iov_base = (char *)buffer;
	iov->iov_len = len;

	// Create and initialize a new uio_t structure
	res = (uio_t *)new uio_t;
	res->uio_iov = iov;
	res->uio_iovcnt = 1;		// probably needs to be parameterized
	res->uio_segflg = UIO_SYSSPACE; // for now, NEEDS to be parameterized
	res->uio_resid = len;		// is total amount of data

	return (res);
}

//
// Compare data in the two uio structures. Return value is true if data chunks
// match exactly, false otherwise. Used to verify the received bulkio objects.
//
bool
uio_compare(uio_t *x, uio_t *y)
{
	int i, j, noof_buf, buf_len;

	//
	// If amount of data or no. of buffers differ, there is an obvious
	// mismatch.
	//
	if (x->uio_iovcnt != y->uio_iovcnt ||
		    x->uio_resid != y->uio_resid) {
		os::printf("uio_compare failed: no. of buffers\n");
		os::printf("or total data amount do not match\n");
		return (false);
	}

	noof_buf = x->uio_iovcnt;

	for (i = 0; i < noof_buf; i++) {
		//
		// if buffers no. i are of different length, there is
		// a  mismatch.
		//
		if (x->uio_iov[i].iov_len != y->uio_iov[i].iov_len) {
			os::printf("uio_compare failed: lengths different\n");
			os::printf("for buffers no. %d\n", i);
			return (false);
		}
		buf_len = (int)x->uio_iov[i].iov_len;
		unsigned char *buf1, *buf2;

		buf1 = (unsigned char *) (x->uio_iov[i].iov_base);
		buf2 = (unsigned char *) (y->uio_iov[i].iov_base);

		for (j = 0; j < buf_len; j++) {
			//
			// If the jth data values in the buffers do not
			//  match, there is a mismatch.
			//
			if (buf1[j] != buf2[j]) {
				os::printf("uio_compare failed:mismatch in\n");
				os::printf("value no. %d in buffer no. %d\n",
						j, i);
				return (false);
			}
		}
	}
	// Everything matches
	return (true);
}

//
// uio_delete - Free up the uio structure, io vectors and data buffers
//
void
uio_delete(uio_t *uiop)
{
	iovec_t	*iov = uiop->uio_iov;
	int	number = uiop->uio_iovcnt;
	for (int i = 0; i < number; i++) {
		if (iov[i].iov_len > 0) {
			delete[] iov[i].iov_base;
		}
	}
	if (number > 0) {
		delete[] iov;
	}
	delete uiop;
}

#if defined(_KERNEL)
//
// in_uio_ptr - Return a in_uio bulkio object to be sent to server for writes
//
bulkio::in_uio_ptr
set_in(uio_t *base, const char *)
{
	bulkio::in_uio_ptr uioobj = NULL;

	// Use the uio object to get the bulkio object
	uioobj = bulkio_impl::conv_in(base, base->uio_resid);

	return (uioobj);
}

//
// verify_in - Verify the received in_uio bulkio object
//
bool
verify_in(bulkio::in_uio_ptr uioobj, uio_t *base, const char *msgpfx)
{
	uio_t	*uiop;

	uiop = bulkio_impl::conv(uioobj);

	if (uio_compare(uiop, base) == true) {
		return (true);
	}
	os::printf("FAIL: %s Bulkio_uio 'in_uio'\n", msgpfx);
	return (false);
}

//
// set_inout - Prepare inout_uio object to be sent to server for a read.
//
bulkio::inout_uio_ptr
set_inout(uio_t *base, const char *)
{
	bulkio::inout_uio_ptr uioobj = NULL;

	uioobj = bulkio_impl::conv_inout(base, base->uio_resid);

	return (uioobj);
}

//
// verify_inout - Verify inout_uio read from server
//
bool
verify_inout(bulkio::inout_uio_ptr uioobj, uio_t *base, const char *msgpfx)
{
	uio_t	*uiop;

	uiop = bulkio_impl::conv(uioobj);

	if (uio_compare(uiop, base) == true) {
		return (true);
	}
	os::printf("FAIL: %s Bulkio_uio 'inout' value:\n", msgpfx);
	return (false);
}

#else

bool
verify_in(bulkio::in_uio_ptr, uio_t *, const char *)
{
	return (false);
}

bool
verify_inout(bulkio::inout_uio_ptr, uio_t *, const char *)
{
	return (false);
}

bulkio::inout_uio_ptr
set_inout(uio_t *, const char *)
{
	bulkio::inout_uio_ptr uioobj = NULL;

	return (uioobj);
}

bulkio::in_uio_ptr
set_in(uio_t *, const char *)
{
	bulkio::in_uio_ptr  uioobj = NULL;

	return (uioobj);
}

#endif
#endif // _UNODE
