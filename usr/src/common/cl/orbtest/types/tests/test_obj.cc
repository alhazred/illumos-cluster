/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_obj.cc	1.10	08/05/20 SMI"

//
// Object reference test
//

#include <orbtest/types/client_impl.h>
#include <orbtest/types/server_impl.h>


static bool	verify(typetest::Obj_ptr objp, int32_t expected_id,
			const char *paramtype, const char *msgpfx);


//
// Test client interface.  Invoked by driver "programs" and, in turn,
// invokes the corresponding method on the test server side.
// Parameters:
//	server_ref  --	test server object to invoke.
//	num_repeat  --	how many times to run the test.
//	noverify    --	if true then server won't perform data verification.
// Returns:
//	Total test execution time.
//
int64_t
client_impl::test_obj(
		typetest::server_ptr server_ref,
		int32_t num_repeat,
		bool noverify,
		Environment &)
{
	// os::printf("%s test_obj()\n", msg_prefix());
	os::hrtime_t	total_time = 0, start_time;

	if (noverify) {
		Environment	e3;

		server_ref->set_noverify(e3);
		if (! env_ok(e3, msg_prefix())) {
			return (-1);
		}
	}

	for (; num_repeat > 0; --num_repeat) {
		Environment		e2;
		Obj_impl		in_impl(in_obj_id);
		Obj_impl		inout_impl(inout_obj_id);
		typetest::Obj_ptr	in, inout;
		typetest::Obj_ptr	out = typetest::Obj::_nil();
		typetest::Obj_ptr	ret = typetest::Obj::_nil();

		// Prepare object references to be passed to server.
		in = in_impl.get_objref();
		inout = inout_impl.get_objref();

		// Invoke server.
		start_time = os::gethrtime();
		ret = server_ref->test_obj(in, out, inout, e2);
		if (! env_ok(e2, msg_prefix())) {
			total_time = -1;
			num_repeat = 0;		// end loop
		} else {
			total_time += os::gethrtime() - start_time;

			// Verify object references from server.
			verify(out, inout_obj_id, "'out'", msg_prefix());
			verify(inout, in_obj_id, "'inout'", msg_prefix());
			verify(ret, in_obj_id, "returned", msg_prefix());
		}

		CORBA::release(in);
		CORBA::release(inout);
		CORBA::release(out);
		CORBA::release(ret);

		// Verify that _unreferenced() is (or will be) called.
		verify_unreferenced(&in_impl, msg_prefix());
		verify_unreferenced(&inout_impl, msg_prefix());
	}

	return (total_time);
}


//
// Test server interface.  Invoked by the test client.
//
typetest::Obj_ptr
server_impl::test_obj(
		typetest::Obj_ptr  in,
		typetest::Obj_out  out,
		typetest::Obj_ptr &inout,
		Environment &)
{
	// os::printf("%s test_obj()\n", msg_prefix());

	if (!verify_off) {
		// Verify object references from client.
		verify(in, in_obj_id, "'in'", msg_prefix());
		verify(inout, inout_obj_id, "'inout'", msg_prefix());
	}

	// Prepare object references for client.
	out = typetest::Obj::_duplicate(inout);	// copy inout to out

	CORBA::release(inout);
	inout = typetest::Obj::_duplicate(in);	// copy in into inout

	return (typetest::Obj::_duplicate(in));	// return in object
}


//
// Verifies that test object reference "objp" has the expected ID
// "expected_id".
//
bool
verify(typetest::Obj_ptr objp, int32_t expected_id, const char *paramtype,
	const char *msgpfx)
{
	int32_t		idnum;
	Environment	e;

	if (CORBA::is_nil(objp)) {
		os::printf("FAIL: %s %s object reference is NIL\n",
			msgpfx, paramtype);
		return (false);
	}

	idnum = objp->id(e);
	if (e.exception()) {
		os::printf("FAIL: %s object reference had exception in id()\n",
			msgpfx);
		e.clear();
		return (false);
	}
	if (idnum != expected_id) {
		os::printf("FAIL: %s %s object reference:\n",
			paramtype, msgpfx);
		os::printf("\texpected id = %d, actual id = %d\n",
			expected_id, idnum);
		return (false);
	}

	return (true);
}
