/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_arrayfstruct.cc	1.18	08/05/20 SMI"

#include <orbtest/types/server_impl.h>
#include <orbtest/types/client_impl.h>

//
// Forward definitions
//
static bool set_in(
	typetest::arrayFstruct var,
	const char *msgpfx,
	bool set = true);

inline bool
verify_in(const typetest::arrayFstruct v, const char *p)
{
	return (set_in((typetest::Fstruct *)v, p, false));
}

static bool set_out(
	typetest::arrayFstruct var,
	const char *msgpfx,
	bool set = true);

inline bool
verify_out(typetest::arrayFstruct v, const char *p)
{
	return (set_out(v, p, false));
}

static bool set_io_i(
	typetest::arrayFstruct var,
	const char *msgpfx,
	bool set = true);

inline bool
verify_io_i(typetest::arrayFstruct v, const char *p)
{
	return (set_io_i(v, p, false));
}

static bool set_io_o(
	typetest::arrayFstruct var,
	const char *msgpfx,
	bool set = true);

inline bool
verify_io_o(typetest::arrayFstruct v, const char *p)
{
	return (set_io_o(v, p, false));
}

#ifndef BUG_4086497
static bool set_ret(
	typetest::arrayFstruct_slice *& var,
	const char *msgpfx,
	bool set = true);

inline bool
verify_ret(typetest::arrayFstruct_slice *& v, const char *p)
{
	return (set_ret(v, p, false));
}
#endif	// ! BUG_4086497

//
// Array of Fstruct (1 Elements)
//
// server_impl::test_arrayfstruct
//
#ifndef BUG_4086497
typetest::arrayFstruct_slice *
#else
void
#endif	// ! BUG_4086497
server_impl::test_arrayfstruct(
	const typetest::arrayFstruct in,
	typetest::arrayFstruct out,
	typetest::arrayFstruct inout,
	Environment &)
{
	// os::printf ("%s test_arrayfstruct()\n", msg_prefix());

#ifndef BUG_4086497
	typetest::arrayFstruct_slice * ret = 0;
	ret = typetest::arrayFstruct_alloc();

	if (ret == CORBA::_nil) {
		os::printf("%s could not allocate return arrayFstruct\n",
			msg_prefix());
		e.exception(new no_resources());
		return (CORBA::_nil);
	}
#endif	// ! BUG_4086497

	if (!verify_off) {
		// Verify values passed by client
		verify_in(in, msg_prefix());
		verify_io_i(inout, msg_prefix());
	}

	// Prepare values to be returned to client (populate)
	set_out(out, msg_prefix());
	set_io_o(inout, msg_prefix());

#ifndef BUG_4086497
	set_ret(ret, msg_prefix());
	return (ret);
#endif	// ! BUG_4086497
}

//
// client_impl::test_arrayfstruct
//
int64_t
client_impl::test_arrayfstruct(
	typetest::server_ptr server_ref,
	int32_t num_repeat,
	bool noverify,
	Environment &)
{
	// os::printf("%s test_arrayfstruct()\n",msg_prefix());
	os::hrtime_t	total_time = 0, start_time;

	if (noverify) {
		Environment e2;

		server_ref->set_noverify(e2);
		if (! env_ok(e2, msg_prefix())) {
			return (-1);
		}
	}

	typetest::arrayFstruct *in, *out, *inout;

	// Allocate dynamic arrays
	// This is dont so we dont cause a stack overflow
	// by allocating the arrays as automatic variables
	in = (typetest::arrayFstruct *) new typetest::arrayFstruct;
	out = (typetest::arrayFstruct *) new typetest::arrayFstruct;
	inout = (typetest::arrayFstruct *) new typetest::arrayFstruct;

	for (; num_repeat > 0; --num_repeat) {
		Environment e2;

#ifndef BUG_4086497
		arrayFstruct_slice * ret;
#endif	// ! BUG_4086497

		// Prepare data to be passed to server.
		set_in(*in, msg_prefix());
		set_io_i(*inout, msg_prefix());

		// Invoke server.
		start_time = os::gethrtime();

#ifndef BUG_4086497
		ret = server_ref->test_arrayfstruct(*in, *out, *inout, e2);
#else
		server_ref->test_arrayfstruct(*in, *out, *inout, e2);
#endif	// ! BUG_4086497

		if (! env_ok(e2, msg_prefix())) {
			total_time = -1;
			break;
		}

		total_time += os::gethrtime() - start_time;

		if (!noverify) {
			// Verify valies returned by server.
			verify_out(*out, msg_prefix());
			verify_io_o(*inout, msg_prefix());

#ifndef BUG_4086497
			verify_ret(ret, msg_prefix());
#endif // ! BUG_4086497
		} // !noverify

	}

#ifndef BUG_4086497
	typetest::arrayFstruct_free(ret);
#endif	// ! BUG_4086497

	// Delete the arrays
	delete[] in;
	delete[] out;
	delete[] inout;

	return (total_time);
}

//
// Common functions
//
bool
set_in(typetest::arrayFstruct var, const char *msgpfx, bool set)
{
	int i;
	int elements;
	typetest::Fstruct	base;

	base.m_1byte = 0xf7;
	base.m_2bytes = 0xabcd;
	base.m_4bytes = 0xfedcba98;
	base.m_8bytes = 0xfedcba9876543210;

	elements = (int)(sizeof (typetest::arrayFstruct) /
	    sizeof (typetest::Fstruct));


	// Set data
	if (set) {
		for (i = 0; i < elements; ++i) {
			if (base.m_1byte == 0)
				base.m_1byte = 0xF7;
			if (base.m_2bytes == 0)
				base.m_2bytes = 0xabcd;
			if (base.m_4bytes == 0)
				base.m_4bytes = 0xfedcba98;
			if (base.m_8bytes == 0)
				base.m_8bytes = 0xfedcba9876543210;

			var[i].m_1byte = base.m_1byte;
			var[i].m_2bytes = base.m_2bytes;
			var[i].m_4bytes = base.m_4bytes;
			var[i].m_8bytes = base.m_8bytes;

			base.m_1byte <<= 1;
			base.m_2bytes <<= 1;
			base.m_4bytes <<= 1;
			base.m_8bytes <<= 1;
		}

		return (true);
	}

	// Verify Data
	for (i = 0; i < elements; ++i) {
		if (base.m_1byte == 0)
			base.m_1byte = 0xF7;
		if (base.m_2bytes == 0)
			base.m_2bytes = 0xabcd;
		if (base.m_4bytes == 0)
			base.m_4bytes = 0xfedcba98;
		if (base.m_8bytes == 0)
			base.m_8bytes = 0xfedcba9876543210;

		if (var[i].m_1byte != base.m_1byte) {
			os::printf("FAIL: %s 1-byte member of 'in' fixed "
				"struct array at index %d :", msgpfx, i);
			os::printf("\texpected = 0x%x, actual = 0x%x\n",
				base.m_1byte, var[i].m_1byte);
			return (false);
		}
		if (var[i].m_2bytes != base.m_2bytes) {
			os::printf("FAIL: %s 2-byte member of 'in' fixed "
				"struct array at index %d :", msgpfx, i);
			os::printf("\texpected = 0x%hx, actual = 0x%hx\n",
				base.m_2bytes, var[i].m_2bytes);
			return (false);
		}
		if (var[i].m_4bytes != base.m_4bytes) {
			os::printf("FAIL: %s 4-byte member of 'in' fixed "
				"struct array at index %d :", msgpfx, i);
			os::printf("\texpected = 0x%lx, actual = 0x%lx\n",
				base.m_4bytes, var[i].m_4bytes);
			return (false);
		}
		if (var[i].m_8bytes != base.m_8bytes) {
			os::printf("FAIL: %s 8-byte member of 'in' fixed "
				"struct array at index %d :", msgpfx, i);
			os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
				base.m_8bytes, var[i].m_8bytes);
			return (false);
		}

		base.m_1byte <<= 1;
		base.m_2bytes <<= 1;
		base.m_4bytes <<= 1;
		base.m_8bytes <<= 1;
	}

	return (true);
}

//
// Array of Fstruct (1 elements)
//
bool
set_out(typetest::arrayFstruct var, const char *msgpfx, bool set)
{
	int i;
	int elements;
	typetest::Fstruct	base;

	base.m_1byte = 0xf7;
	base.m_2bytes = 0xabcd;
	base.m_4bytes = 0xfedcba98;
	base.m_8bytes = 0xfedcba9876543210;

	elements = (int)(sizeof (typetest::arrayFstruct) /
	    sizeof (typetest::Fstruct));

	// Set data
	if (set) {
		for (i = 0; i < elements; ++i) {
			if (base.m_1byte == 0)
				base.m_1byte = 0xF7;
			if (base.m_2bytes == 0)
				base.m_2bytes = 0xabcd;
			if (base.m_4bytes == 0)
				base.m_4bytes = 0xfedcba98;
			if (base.m_8bytes == 0)
				base.m_8bytes = 0xfedcba9876543210;

			var[i].m_1byte = base.m_1byte;
			var[i].m_2bytes = base.m_2bytes;
			var[i].m_4bytes = base.m_4bytes;
			var[i].m_8bytes = base.m_8bytes;

			base.m_1byte >>= 1;
			base.m_2bytes >>= 1;
			base.m_4bytes >>= 1;
			base.m_8bytes >>= 1;
		}

		return (true);
	}

	// Verify Data
	for (i = 0; i < elements; ++i) {
		if (base.m_1byte == 0)
			base.m_1byte = 0xF7;
		if (base.m_2bytes == 0)
			base.m_2bytes = 0xabcd;
		if (base.m_4bytes == 0)
			base.m_4bytes = 0xfedcba98;
		if (base.m_8bytes == 0)
			base.m_8bytes = 0xfedcba9876543210;

		if (var[i].m_1byte != base.m_1byte) {
			os::printf("FAIL: %s 1-byte member of 'out' fixed "
				"struct array at index %d :", msgpfx, i);
			os::printf("\texpected = 0x%x, actual = 0x%x\n",
				base.m_1byte, var[i].m_1byte);
			return (false);
		}
		if (var[i].m_2bytes != base.m_2bytes) {
			os::printf("FAIL: %s 2-byte member of 'out' fixed "
				"struct array at index %d :", msgpfx, i);
			os::printf("\texpected = 0x%hx, actual = 0x%hx\n",
				base.m_2bytes, var[i].m_2bytes);
			return (false);
		}
		if (var[i].m_4bytes != base.m_4bytes) {
			os::printf("FAIL: %s 4-byte member of 'out' fixed "
				"struct array at index %d :", msgpfx, i);
			os::printf("\texpected = 0x%lx, actual = 0x%lx\n",
				base.m_4bytes, var[i].m_4bytes);
			return (false);
		}
		if (var[i].m_8bytes != base.m_8bytes) {
			os::printf("FAIL: %s 8-byte member of 'out' fixed "
				"struct array at index %d :", msgpfx, i);
			os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
				base.m_8bytes, var[i].m_8bytes);
			return (false);
		}

		base.m_1byte >>= 1;
		base.m_2bytes >>= 1;
		base.m_4bytes >>= 1;
		base.m_8bytes >>= 1;
	}

	return (true);
}

//
// Array of Fstruct (1 elements)
//
bool
set_io_i(typetest::arrayFstruct var, const char *msgpfx, bool set)
{
	int i;
	int elements;
	typetest::Fstruct	base;

	base.m_1byte = ~0;
	base.m_2bytes = ~0;
	base.m_4bytes = ~0;
	base.m_8bytes = ~0;

	elements = (int)(sizeof (typetest::arrayFstruct) /
	    sizeof (typetest::Fstruct));

	// Set data
	if (set) {
		for (i = 0; i < elements; ++i) {
			if (base.m_1byte == 0)
				base.m_1byte = ~0;
			if (base.m_2bytes == 0)
				base.m_2bytes = ~0;
			if (base.m_4bytes == 0)
				base.m_4bytes = ~0;
			if (base.m_8bytes == 0)
				base.m_8bytes = ~0;

			var[i].m_1byte = base.m_1byte;
			var[i].m_2bytes = base.m_2bytes;
			var[i].m_4bytes = base.m_4bytes;
			var[i].m_8bytes = base.m_8bytes;

			base.m_1byte >>= 1;
			base.m_2bytes >>= 1;
			base.m_4bytes >>= 1;
			base.m_8bytes >>= 1;
		}

		return (true);
	}

	// Verify Data
	for (i = 0; i < elements; ++i) {
		if (base.m_1byte == 0)
			base.m_1byte = ~0;
		if (base.m_2bytes == 0)
			base.m_2bytes = ~0;
		if (base.m_4bytes == 0)
			base.m_4bytes = ~0;
		if (base.m_8bytes == 0)
			base.m_8bytes = ~0;

		if (var[i].m_1byte != base.m_1byte) {
			os::printf("FAIL: %s 1-byte member of 'io_i' fixed "
				"struct array at index %d :", msgpfx, i);
			os::printf("\texpected = 0x%x, actual = 0x%x\n",
				base.m_1byte, var[i].m_1byte);
			return (false);
		}
		if (var[i].m_2bytes != base.m_2bytes) {
			os::printf("FAIL: %s 2-byte member of 'io_i' fixed "
				"struct array at index %d :", msgpfx, i);
			os::printf("\texpected = 0x%hx, actual = 0x%hx\n",
				base.m_2bytes, var[i].m_2bytes);
			return (false);
		}
		if (var[i].m_4bytes != base.m_4bytes) {
			os::printf("FAIL: %s 4-byte member of 'io_i' fixed "
				"struct array at index %d :", msgpfx, i);
			os::printf("\texpected = 0x%lx, actual = 0x%lx\n",
				base.m_4bytes, var[i].m_4bytes);
			return (false);
		}
		if (var[i].m_8bytes != base.m_8bytes) {
			os::printf("FAIL: %s 8-byte member of 'io_i' fixed "
				"struct array at index %d :", msgpfx, i);
			os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
				base.m_8bytes, var[i].m_8bytes);
			return (false);
		}

		base.m_1byte >>= 1;
		base.m_2bytes >>= 1;
		base.m_4bytes >>= 1;
		base.m_8bytes >>= 1;
	}

	return (true);
}

//
// Array of Fstruct (1 elements)
//
bool
set_io_o(typetest::arrayFstruct var, const char *msgpfx, bool set)
{
	int i;
	int elements;

	typetest::Fstruct	base;

	base.m_1byte = ~0;
	base.m_2bytes = ~0;
	base.m_4bytes = ~0;
	base.m_8bytes = ~0;

	elements = (int)(sizeof (typetest::arrayFstruct) /
	    sizeof (typetest::Fstruct));

	// Set data
	if (set) {
		for (i = 0; i < elements; ++i) {
			if (base.m_1byte == 0)
				base.m_1byte = ~0;
			if (base.m_2bytes == 0)
				base.m_2bytes = ~0;
			if (base.m_4bytes == 0)
				base.m_4bytes = ~0;
			if (base.m_8bytes == 0)
				base.m_8bytes = ~0;

			var[i].m_1byte = base.m_1byte;
			var[i].m_2bytes = base.m_2bytes;
			var[i].m_4bytes = base.m_4bytes;
			var[i].m_8bytes = base.m_8bytes;

			base.m_1byte <<= 1;
			base.m_2bytes <<= 1;
			base.m_4bytes <<= 1;
			base.m_8bytes <<= 1;
		}

		return (true);
	}

	// Verify Data
	for (i = 0; i < elements; ++i) {
		if (base.m_1byte == 0)
			base.m_1byte = ~0;
		if (base.m_2bytes == 0)
			base.m_2bytes = ~0;
		if (base.m_4bytes == 0)
			base.m_4bytes = ~0;
		if (base.m_8bytes == 0)
			base.m_8bytes = ~0;

		if (var[i].m_1byte != base.m_1byte) {
			os::printf("FAIL: %s 1-byte member of 'io_o' fixed "
				"struct array at index %d :", msgpfx, i);
			os::printf("\texpected = 0x%x, actual = 0x%x\n",
				base.m_1byte, var[i].m_1byte);
			return (false);
		}
		if (var[i].m_2bytes != base.m_2bytes) {
			os::printf("FAIL: %s 2-byte member of 'io_o' fixed "
				"struct array at index %d :", msgpfx, i);
			os::printf("\texpected = 0x%hx, actual = 0x%hx\n",
				base.m_2bytes, var[i].m_2bytes);
			return (false);
		}
		if (var[i].m_4bytes != base.m_4bytes) {
			os::printf("FAIL: %s 4-byte member of 'io_o' fixed "
				"struct array at index %d :", msgpfx, i);
			os::printf("\texpected = 0x%lx, actual = 0x%lx\n",
				base.m_4bytes, var[i].m_4bytes);
			return (false);
		}
		if (var[i].m_8bytes != base.m_8bytes) {
			os::printf("FAIL: %s 8-byte member of 'io_o' fixed "
				"struct array at index %d :", msgpfx, i);
			os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
				base.m_8bytes, var[i].m_8bytes);
			return (false);
		}

		base.m_1byte <<= 1;
		base.m_2bytes <<= 1;
		base.m_4bytes <<= 1;
		base.m_8bytes <<= 1;
	}

	return (true);
}

//
// Array of Fstruct (1 elements)
//
#ifndef BUG_4086497
bool
set_ret(typetest::arrayFstruct_slice *& var, const char *msgpfx, bool set)
{
	int i;
	int elements;

	typetest::Fstruct	base;

	base.m_1byte = ~0;
	base.m_2bytes = ~0;
	base.m_4bytes = ~0;
	base.m_8bytes = ~0;

	elements = sizeof (typetest::arrayFstruct)/sizeof (typetest::Fstruct);

	// Set data
	if (set) {
		for (i = 0; i < elements; ++i) {
			var[i].m_1byte = base.m_1byte;
			var[i].m_2bytes = base.m_2bytes;
			var[i].m_4bytes = base.m_4bytes;
			var[i].m_8bytes = base.m_8bytes;

			base.m_1byte = ~base.m_1byte;
			base.m_2bytes = ~base.m_2bytes;
			base.m_4bytes = ~base.m_4bytes;
			base.m_8bytes = ~base.m_8bytes;
		}

		return (true);
	}

	// Verify Data
	for (i = 0; i < elements; ++i) {
		if (var[i].m_1byte != base.m_1byte) {
			os::printf("FAIL: %s 1-byte member of 'ret' fixed "
				"struct array at index %d :", msgpfx, i);
			os::printf("\texpected = 0x%x, actual = 0x%x\n",
				base.m_1byte, var[i].m_1byte);
			return (false);
		}
		if (var[i].m_2bytes != base.m_2bytes) {
			os::printf("FAIL: %s 2-byte member of 'ret' fixed "
				"struct array at index %d :", msgpfx, i);
			os::printf("\texpected = 0x%hx, actual = 0x%hx\n",
				base.m_2bytes, var[i].m_2bytes);
			return (false);
		}
		if (var[i].m_4bytes != base.m_4bytes) {
			os::printf("FAIL: %s 4-byte member of 'ret' fixed "
				"struct array at index %d :", msgpfx, i);
			os::printf("\texpected = 0x%lx, actual = 0x%lx\n",
				base.m_4bytes, var[i].m_4bytes);
			return (false);
		}
		if (var[i].m_8bytes != base.m_8bytes) {
			os::printf("FAIL: %s 8-byte member of 'ret' fixed "
				"struct array at index %d :", msgpfx, i);
			os::printf("\texpected = 0x%llx, actual = 0x%llx\n",
				base.m_8bytes, var[i].m_8bytes);
			return (false);
		}

		base.m_1byte = ~base.m_1byte;
		base.m_2bytes = ~base.m_2bytes;
		base.m_4bytes = ~base.m_4bytes;
		base.m_8bytes = ~base.m_8bytes;
	}

	return (true);
}
#endif	// ! BUG_4086497
