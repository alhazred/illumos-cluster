/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_fork.cc	1.14	08/05/20 SMI"

#include <stdio.h>
#include <thread.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/sol_version.h>
#include <errno.h>
#include <orb/invo/corba.h>
#include <orb/infrastructure/orb.h>
#include <orb/infrastructure/fork.h>
#include <sys/rm_util.h>
#include <h/naming.h>
#include <nslib/ns.h>
#include <orbtest/types/client_impl.h>
#include <orbtest/types/server_impl.h>
#include <orbtest/types/impl_common.h>
#include <orb/xdoor/solaris_xdoor.h>
#include <sys/os.h>
#include <dlfcn.h>

//
// Test cases for testing fork, fork1 and forkall support in ORB.
// ------------------------------------------------------------------
//		Test Case				Expected Result
// ------------------------------------------------------------------
// case 1: Orb initialized, fork executed in a		-- PASS
// single threaded client.

// case 2: Orb initialized, fork executed in a		-- PASS
// multi-threaded client with two threads and invocations
// are done on different typetest objects. These invocations are
// supposed to take place twice once each in parent and child.

// case 3: Orb initialized, fork executed in a		-- PASS
// multi-threaded client same as above case but has a extra
// thread which fork's a child and this fork is supposed
// be get blocked if there is any invocation in-progress.

// case 4: Orb server initialized, fork executed	-- FAIL
// fork is not supported in ORB servers as a result
// this case is supposed to fail.
//
// Note: fork defined in ORB would be invoked only when
// libclcomm is loaded before libC and libc. so during
// compilation  -lclcomm  should be specified before -lc and -lC.

// same set of tests can be executed for testing fork1 and forkall support.
// command line options:
//
//		test_fork 1  // To test fork support.
//
//		test_fork 2  // To test fork1 support.
//
//		test_fork 3  // To test vfork support.

//		test_fork 4  // To test forkall support.
//
// Brief Description of process flow:
//	test_fork program has the following helper functions:

// execute_fork_tests();
//	This function instantiates uclient and userver programs
//	in different processes. uclient creates client_impl and Obj
//	ORB server objects and userver creates server_impl server
//	object. uclient and userver registers server object with
//	the nameserver.
//
// single_threaded_client();
//	This function implements test case 1.
//
// MT_client1();
//	This function implements test case 2.
//
// MT_client2();
//	This function implements test case 3.
//
// invoke_testclient(func_param_t *func_paramp);
//	Through client_impl and Obj references makes a invocation
//	in both parent and child process.
//
// invoke_testserver(typetest::server_ptr *server_pp);
//	Using server_impl references makes a invocation
//	in both parent and child process.
//
//
// Note:
//
// when all the references are released, uclient and
// userver should have died, if it still exists then
// it means some references still exists and userver
// or uclient needs to be killed explicitly.
// In the script which executes test_fork, after
// termination of test_fork explicit check has to be made
// for uclient and userver, if these process still exist
// then it may indicate test_fork failure.
//
// sleep() function has been used in various test functions
// to wait for either thread to be launched or process to
// be invoked successfully. In future this can be changed to
// synchornization through other semantics like conditional
// variables.
//

const char	msg_prefix[]	= "fork support test:";

typedef pid_t (*fork_fn_ptr)(void);

typedef struct {
typetest::server_ptr	*server_pp;
typetest::client_ptr	*client_pp;
typetest::Obj_ptr	*Obj_pp;
uint16_t		sleep_period;
} func_param_t;

// helper functions
bool single_threaded_client(void);
bool MT_client1(void);
bool MT_client2(void);
bool invoke_testclient(func_param_t *func_paramp);
bool invoke_testserver(typetest::server_ptr *server_pp);
bool execute_fork_servers(void);
static void done(const char *name);
bool call_fork(void);
int verify_fork_resolution(char *fork_str);
int test_vfork(void);

fork_fn_ptr fork_ptr = NULL;

// Following synchronization primitives are used to
// synchronize between threads in MT_client2() function.
//
// invoke_testserver grabs sync_lck and wakes the thread
// waiting on sync_cv which is call_fork function.
// This is to ensure that fork is executed when a invocation
// is in progress.
//
os::mutex_t	sync_lck;
os::condvar_t	sync_cv;

bool thread_continue = false;
void
print_usage()
{
#ifndef	CONSISTENT_FORK // Solaris 10 and later releases.
	(void) fprintf(stdout, "\nUsage: test_fork FORK_TYPE"
	    "\n\t 1 for fork \n\t 2 for fork1 \n\t 3 for vfork\n");
#else
	(void) fprintf(stdout, "\nUsage: test_fork FORK_TYPE"
	    "\n\t 1 for fork \n\t 2 for fork1 \n\t 3 for vfork"
	    "\n\t 4 for forkall\n");
#endif  // CONSISTENT_FORK
}

int
main(int argc, char **argv)
{
	int ret_value = 0;
	int fork_case = 0;

	if (argc != 2) {
		print_usage();
		return (1);
	}

	fork_case = atoi(argv[1]);


	// select the appropriate fork function to call.
	switch (fork_case) {
		case 1: fork_ptr = fork;
			ret_value = verify_fork_resolution("fork");
			(void) fprintf(stdout, "\nTesting fork() support\n");
			break;

		case 2: fork_ptr = fork1;
			ret_value = verify_fork_resolution("fork1");
			(void) fprintf(stdout, "\nTesting fork1() support\n");
			break;

		case 3: fork_ptr = vfork;
			if (!(verify_fork_resolution("vfork"))) {
				ret_value = 1;
				//
				// in the case of vfork we just need to verify
				// that vfork isnt supported when ORB server is
				// initialized.
				(void) fprintf(stdout, "\nTesting vfork() "
				    "support\n");
				return (test_vfork());
			}
			break;

#ifdef	CONSISTENT_FORK // Solaris 10 and later releases.
		case 4: fork_ptr = forkall;
			ret_value = verify_fork_resolution("forkall");
			(void) fprintf(stdout, "\nTesting forkall() support\n");
			break;
#endif	// CONSISTENT_FORK

		default:
			print_usage();
			// invalid fork requested.
			return (1);

	}
	// if fork symbol doesnt resolve to libclcomm then test exits.
	if (ret_value) {
		(void) fprintf(stdout, "\ntest_fork Exiting\n");
		return (1);
	}
	// initialize ORB
	if (ORB::initialize(false) != 0) {
		(void) fprintf(stderr, "Failed to intitialize ORB.\n");
		// log failure
		return (1);
	}

	if (!execute_fork_servers()) {
		ret_value = 1;
	}
	// initialize ORB server.
	if (ORB::server_initialize() != 0) {
		(void) fprintf(stderr, "Failed to intitialize ORB server.");
		// log failure
		return (1);
	}

	// fork/fork1 should not be supported after ORB server
	// is initialized.
	pid_t child_pid;
	child_pid = (*fork_ptr)();
	if (child_pid < 0) {
		(void) fprintf(stdout, "\ntest case 4: Result = PASS");
		ret_value = 0;
	} else if (child_pid == 0) { // child process
		(void) fprintf(stdout, "\ntest case 4: Result = FAIL");
		ret_value = 1;
		exit(0);
	}
	return (ret_value);
}
//
// function single_threaded_client implements test case wherein
// the client is single threaded. This function gets some object
// references before fork is called and makes invocations using these
// references in parent and child. The references are released
// at the end of function. This functions make sure that the
// references are dup'ed in the child process and child is
// able to make invocations successfully.
//
bool
single_threaded_client()
{
	bool			ret_value = true;
	pid_t			child_pid;
	func_param_t		func_param;
	typetest::server_ptr    server_p;
	typetest::client_ptr    client_p;
	typetest::Obj_ptr	Obj_p;
	CORBA::Object_ptr	objp;

	// get client , server and Obj references.
	objp = get_obj(userver_name);
	if (CORBA::is_nil(objp)) {
		return (false);
	}
	server_p = typetest::server::_narrow(objp);
	CORBA::release(objp);

	objp = get_obj(uclient_name);
	if (CORBA::is_nil(objp)) {
		return (false);
	}
	client_p = typetest::client::_narrow(objp);
	CORBA::release(objp);

	objp = get_obj(uobj_name);
	if (CORBA::is_nil(objp)) {
		return (false);
	}
	Obj_p = typetest::Obj::_narrow(objp);
	CORBA::release(objp);

	func_param.server_pp = &server_p;
	func_param.client_pp = &client_p;
	func_param.Obj_pp = &Obj_p;
	func_param.sleep_period = 0;

	// call fork, make one invocation in parent and child
	// on separate objects. parent waits for the child to exit.
	//
	if ((child_pid = (*fork_ptr)()) < 0) {
		// fork failed, write result to a file.
		ret_value = false;
	} else if (child_pid > 0) { // parent process

		(void) invoke_testclient(&func_param);
		// wait for child to exit.
		while (wait((int *)0) != child_pid) {}
		CORBA::release(client_p);
		CORBA::release(server_p);
		CORBA::release(Obj_p);

	} else { // child process

		(void) invoke_testserver(func_param.server_pp);
		exit(0);
	}
	return (ret_value);
}
//
// execute_fork_servers starts uclient and userver in different processes.
// This function calls three functions which handle three
// test-cases for fork support.
//
bool
execute_fork_servers()
{
	bool ret_value = true;
	int 	error;
	int 	exec_client_error = 1;
	int 	exec_server_error = 1;

	// XXX needs to specify relative path's to uclient and userver.
	char *uclient_path = "/usr/cluster/orbtest/types/uclient0 >"
	    "/dev/console &";

	char *userver_path = "/usr/cluster/orbtest/types/userver0 >"
	    "/dev/console &";

	// check if uclient instance exists, if it does then kill
	// uclient instance.
	exec_client_error = system("pgrep uclient0 >/dev/null");
	exec_client_error = WEXITSTATUS((uint_t)exec_client_error);

	if (exec_client_error == 0) {
		(void) fprintf(stdout, "\nuclient0 exists, need to kill "
		    "in order to test fork support.");
		// kill uclient instance before starting another.
		error = system("pkill uclient0 >/dev/null");
	}

	// execute uclient in background.
	error = system(uclient_path);

	// system wasnt executed successfully.
	if (WEXITSTATUS((uint_t)error) != 0) {
		(void) fprintf(stderr, "FAILURE: system() could not execute "
		    "uclient0");
		return (false);
	}
	// check if userver instance exists, if it does then  kill
	// userver instance.
	exec_server_error = system("pgrep userver0 >/dev/null");
	exec_server_error = WEXITSTATUS((uint_t)exec_server_error);

	if (exec_server_error == 0) {
		(void) fprintf(stdout, "\nuserver0 exists, need to kill "
		    "in order to test fork support.");
		// kill userver instance before starting another.
		error = system("pkill userver0 >/dev/null");
	}

	// execute userver in background.
	error = system(userver_path);

	// system wasnt executed successfully.
	if (WEXITSTATUS((uint_t)error) != 0) {
		(void) fprintf(stderr, "FAILURE: system() could not execute "
		    "userver0");
		return (false);
	}
	// sleep so that uclient gets executed before getting
	// references of client_impl.
	(void) sleep(10);

	if (!(single_threaded_client())) {
		ret_value = false;
		(void) fprintf(stdout, "\ntest case 1: Result = FAIL");
	} else {
		(void) fprintf(stdout, "\ntest case 1: Result = PASS");
	}

	if (!(MT_client1())) {
		ret_value = false;
		(void) fprintf(stdout, "\ntest case 2: Result = FAIL");
	} else {
		(void) fprintf(stdout, "\ntest case 2: Result = PASS");
	}

	if (!(MT_client2())) {
		ret_value = false;
		(void) fprintf(stdout, "\ntest case 3: Result = FAIL");
	} else {
		(void) fprintf(stdout, "\ntest case 3: Result = PASS");
	}

	// unregister the client_impl object
	done(uclient_name);

	// unregister the server_impl object
	done(userver_name);

	return (ret_value);
}
// MT_client1 function creates two threads and forks a child process.
// the two threads make invocation on separate typetest objects.
// This function makes sure that the threads are replicated in the
// child process and all the references to typetest objects are released
// at the end of the function.
//
bool
MT_client1()
{
	Environment	e2;
	bool ret_value	= true;
	pid_t child_pid;
	thread_t thr1	= 0;
	thread_t thr2	= 0;
	typetest::server_ptr	server_p;
	typetest::client_ptr	client_p;
	typetest::Obj_ptr	Obj_p;
	CORBA::Object_ptr	objp;
	func_param_t		func_param;

	// Get test server
	objp = get_obj(userver_name);
	if (CORBA::is_nil(objp)) {
		return (false);
	}
	server_p = typetest::server::_narrow(objp);
	CORBA::release(objp);

	// Get test client
	objp = get_obj(uclient_name);
	if (CORBA::is_nil(objp)) {
		return (false);
	}
	client_p = typetest::client::_narrow(objp);
	CORBA::release(objp);

	// Get test Obj
	objp = get_obj(uobj_name);
	if (CORBA::is_nil(objp)) {
		return (false);
	}
	Obj_p = typetest::Obj::_narrow(objp);
	CORBA::release(objp);

	func_param.server_pp = &server_p;
	func_param.client_pp = &client_p;
	func_param.Obj_pp = &Obj_p;
	func_param.sleep_period = 0;

	// create two threads both of which invoke a server object.
	if (thr_create(NULL, 0,
	    (void *(*)(void *))invoke_testclient,
	    &func_param, NULL, &thr1) != 0) {
		(void) fprintf(stderr, "\nerror creating thread 1");
	}

	if (thr_create(NULL, 0,
	    (void *(*)(void *))invoke_testserver,
	    &server_p, NULL, &thr2) != 0) {
		(void) fprintf(stderr, "\nerror creating thread 2");
	}

	if ((child_pid = (*fork_ptr)()) < 0) {
		ret_value = false;
	} else if (child_pid > 0) { // parent process

		while (wait((int *)0) != child_pid) {}
		// other threads may get executed before releasing references.
		(void) thr_join(thr1, NULL, NULL);
		(void) thr_join(thr2, NULL, NULL);
		CORBA::release(server_p);
		CORBA::release(client_p);
		CORBA::release(Obj_p);

	} else { // child process
		// the two threads created will be replicated in the child.
		(void) thr_join(thr1, NULL, NULL);
		(void) thr_join(thr2, NULL, NULL);
		exit(0);
	}


	return (ret_value);
}
//
// MT_client2 : This function is similar to MT_client1
// function except that it has a third thread which forks when
// invocation is in progress, fork is blocked when invocation
// is in progress and continues when invocation is done.
//
bool
MT_client2()
{
	bool ret_value		=  true;
	thread_t thr1 = 0;
	thread_t thr2 = 0;
	thread_t thr3 = 0;
	typetest::server_ptr    server_p;
	typetest::client_ptr    client_p;
	CORBA::Object_ptr	objp;
	typetest::Obj_ptr	Obj_p;
	func_param_t		func_param;

	// Get test server
	objp = get_obj(userver_name);
	if (CORBA::is_nil(objp)) {
		return (false);
	}
	server_p = typetest::server::_narrow(objp);
	CORBA::release(objp);

	// Get test client
	objp = get_obj(uclient_name);
	if (CORBA::is_nil(objp)) {
		return (false);
	}
	client_p = typetest::client::_narrow(objp);
	CORBA::release(objp);

	// Get test Obj
	objp = get_obj(uobj_name);
	if (CORBA::is_nil(objp)) {
		return (false);
	}
	Obj_p = typetest::Obj::_narrow(objp);
	CORBA::release(objp);

	func_param.server_pp = &server_p;
	func_param.client_pp = &client_p;
	func_param.Obj_pp = &Obj_p;
	func_param.sleep_period = 10; // 10 secs

	// create three threads both of which invoke a server object.
	if (thr_create(NULL, 0,
	    (void *(*)(void *))invoke_testclient,
	    &func_param, NULL, &thr1) != 0) {
		(void) fprintf(stderr, "\nerror creating thread 1");
	}

	if (thr_create(NULL, 0,
	    (void *(*)(void *))invoke_testserver,
	    &server_p, NULL, &thr2) != 0) {
		(void) fprintf(stderr, "\nerror creating thread 2");
	}

	if (thr_create(NULL, 0,
	    (void *(*)(void *))call_fork, NULL,
	    NULL, &thr2) != 0) {
		(void) fprintf(stderr, "\nerror creating thread 3");
	}
	// wait for threads, as they may get executed after releasing
	// references.
	(void) thr_join(thr1, NULL, NULL);
	(void) thr_join(thr2, NULL, NULL);
	(void) thr_join(thr3, NULL, NULL);

	CORBA::release(server_p);
	CORBA::release(client_p);
	CORBA::release(Obj_p);

	return (ret_value);
}
// invoke_testserver makes a invocation to typeteet::server object.
bool
invoke_testserver(typetest::server_ptr* server_pp)
{
	Environment	env;

	// sleep for sometime so that this thread can be executed
	// child as well as  parent process. can use conditional
	// variable too for invoking this this thread.
	(void) sleep(10);

	if (!server_pp) {
		(void) fprintf(stderr, "\ninvalid params passed to "
		    "invoke_testserver");
		return (false);
	}


	bool		in, out, inout, ret;
	in = true;
	inout = true;

	// invoke server_impl.
	ret = (*server_pp)->test_boolean(in, out, inout, env);
	if (! env_ok(env, msg_prefix)) {
		(void) fprintf(stderr, "\ninvocation failure in "
		    "invoke_testserver");
	}
	env.clear();
	return (ret);
}
//
// invoke_testclient makes a invocation to typetest::client object
// and invocation to typetest::Obj object.
//
bool
invoke_testclient(func_param_t *func_paramp)
{
	bool			ret_value = true;
	Environment		env;
	uint16_t		sleep_period;

	if (func_paramp == NULL) {
		(void) fprintf(stderr, "\ninvalid params to invoke_testclient");
		return (false);
	}

	// sleep for sometime so that this thread can be executed in
	// child and parent process.
	//
	(void) sleep(10);


	typetest::server_ptr *server_pp = func_paramp->server_pp;
	typetest::client_ptr *client_pp = func_paramp->client_pp;
	typetest::Obj_ptr *Obj_pp = func_paramp->Obj_pp;
	sleep_period = func_paramp->sleep_period;

	// invoke the client_impl.
	ret_value = (bool) (*client_pp)->test_obj(*server_pp, 10, false, env);
	if (!env_ok(env, msg_prefix)) {
		(void) fprintf(stderr, "\nInvocation failure in "
		    "invoke_testclient");
	}
	env.clear();

	// make a IDL invocation.
	(void) (*Obj_pp)->id(env);
	if (! env_ok(env, msg_prefix)) {
		// exceptions raised, invocation failure.
		(void) fprintf(stderr, "\nidl invocation on Obj failed");
	}
	env.clear();

	thread_continue = true;

	// method sleep is invoked on the Obj so that
	// fork can be executed when invocation is in progress.
	//
	(*Obj_pp)->call_sleep(sleep_period, env);
	if (! env_ok(env, msg_prefix)) {
		(void) fprintf(stderr, "\nInvocation failure in "
		    "invoke_testclient");
	}
	env.clear();
	return (ret_value);

}

//
// Invoke the done() method on the impl object, if any, with the specified
// string name.
//
static void
done(const char *name)
{
	CORBA::Object_var	objp;
	objp = get_obj(name, true, false);

	if (!CORBA::is_nil(objp)) {
		Environment	e;
		typetest::common_var    commonp;
		commonp = typetest::common::_narrow(objp);
		commonp->done(e);
		if (e.exception()) {
			(void) fprintf(stderr, "ERROR: can't invoke done "
			    "on %s\n", name);
			e.exception()->print_exception("\t");
		}
	}
}

// returns true if fork succeeds.
// this is a separate function because it has to be executed
// in a separate thread.
bool
call_fork()
{
	pid_t child_pid;
	while (!thread_continue) {
		(void) sleep(1);
	}
	child_pid = (*fork_ptr)();
	if (child_pid < 0) {
		return (false);
	}
	return (true);
}

int
verify_fork_resolution(char *fork_str)
{
	void *tmp_fork_ptr = NULL;
	Dl_info dli;
	char *libclcomm_str = "/usr/cluster/lib/libclcomm.so.1";

	tmp_fork_ptr =  dlsym(RTLD_NEXT, fork_str);
	(void) dladdr(tmp_fork_ptr, &dli);
	if (strcmp(dli.dli_fname, libclcomm_str) != 0) {
		(void) fprintf(stdout, "\n%s symbol resolves to lib = %s",
		    fork_str, dli.dli_fname);
		(void) fprintf(stdout, "\nCheck library link order: "
		    "ldd prog_name");
		return (1); // resolves to incorrect library.
	}
	return (0);
}
int
test_vfork()
{
	int ret_value = 0;
	// initialize orb.
	if (ORB::initialize(false) != 0) {
		(void) fprintf(stderr, "Failed to intitialize ORB.\n");
		// log failure
		return (1);
	}
	// initialize ORB server.
	if (ORB::server_initialize() != 0) {
		(void) fprintf(stderr, "Failed to intitialize ORB server.");
		// log failure
		return (1);
	}
	(void) fprintf(stdout, "\nTo test if vfork is supported after"
	    " orb_server initialization");

	// vfork should not be supported after ORB server
	// is initialized.
	pid_t child_pid;
	child_pid = (*fork_ptr)();
	if (child_pid < 0) {
		(void) fprintf(stdout, "\ntest case 1: Result = PASS");
		ret_value = 0;
	} else if (child_pid == 0) { // child process
		(void) fprintf(stdout, "\ntest case 1: Result = FAIL");
		ret_value = 1;
		_exit(0);
	}
	return (ret_value);
}
