/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_nonblocking.cc	1.12	08/05/20 SMI"

//
// Oneway, nonblocking test
//

#include <orbtest/types/client_impl.h>
#include <orbtest/types/server_impl.h>

class callback_flag {
public:
	bool		acked;
	os::mutex_t	lck;
	os::condvar_t	cv;
};


//
// Test client interface.  Invoked by driver "programs" and, in turn,
// invokes the corresponding method on the test server side.
// Parameters:
//	server_ref  --	test server object to invoke.
//	num_repeat  --	how many times to run the test.
//	noverify    --	if true then server won't perform data verification.
// Returns:
//	Total test execution time.
//
int64_t
client_impl::test_nonblocking(
		typetest::server_ptr server_ref,
		int32_t num_repeat,
		bool noverify,
		Environment &)
{
	// os::printf("%s test_nonblocking()\n", msg_prefix());

	const uint32_t		timeout = 60;			// in seconds
	const uint32_t		num_retries = 10;
	const uint32_t		sleep_btw_retries = 500000;	// in usecs

	os::hrtime_t		total_time = 0, start_time;
	callback_flag		*flagp = NULL;
	int			retry;
	os::systime		systime;

	if (noverify) {
		Environment	e3;

		server_ref->set_noverify(e3);
		if (! env_ok(e3, msg_prefix())) {
			return (-1);
		}
	}

	// Create a flag so server can acknowledge that it's been invoked.
	flagp = new callback_flag;
	if (flagp == NULL) {
		os::printf("%s ERROR: can't allocate flag\n", msg_prefix());
		return (-1);
	}

	for (; num_repeat > 0; --num_repeat) {
		Environment	e2;

		flagp->acked = false;

		for (retry = 0; retry < num_retries; ++retry) {
			start_time = os::gethrtime();

			// Note, nonblocking invocations don't allow passing
			// object references so we'll pass in our name as
			// registered with the name server.
			server_ref->test_nonblocking(strname(),
					(uint64_t)flagp, 1, 2, 3, 4, 5, e2);

			if (CORBA::WOULDBLOCK::_exnarrow(e2.exception())) {
				// Sleep then retry.
				os::usecsleep(sleep_btw_retries);
				e2.clear();
				continue;
			}

			if (! env_ok(e2, msg_prefix())) {
				delete flagp;
				return (-1);
			}

			total_time += os::gethrtime() - start_time;
			break;
		}

		// If all retries were unsuccessful...
		if (retry == num_retries) {
			os::printf("%s FAIL: invocation would block after "
				"%d retries\n", msg_prefix(), num_retries);
			delete flagp;
			return (-1);
		}

		// Timed wait until the server sends an acknowledgment.
		systime.setreltime(timeout * 1000000);
		flagp->lck.lock();
		while (! flagp->acked) {
			// If timeout expires...
			if (flagp->cv.timedwait(&flagp->lck, &systime) ==
						os::condvar_t::TIMEDOUT) {
				os::printf("%s FAIL: no ack from server in %d "
					"seconds\n", msg_prefix(), timeout);
				flagp->lck.unlock();
				delete flagp;
				return (-1);
			}
		}
		flagp->lck.unlock();
	}

	delete flagp;
	return (total_time);
}


//
// Callback method invoked by the server to indicate that it's been
// successfully invoked.
// Parameters:
//	flag_addr -- address of flag the client waits on.
// Returns:
//	None.
//
void
client_impl::test_nonblocking_callback(uint64_t flag_addr, Environment &)
{
	callback_flag	*flagp = (callback_flag *)flag_addr;

	flagp->lck.lock();
	flagp->acked = true;
	flagp->cv.signal();
	flagp->lck.unlock();
}


//
// Test server interface.  Invoked by the test client.
// Parameters:
//	client_name	-- the string name of the invoking client as registered
//			   with the name server (nonblocking invocations don't
//			   allow passing object references).
//	flag_addr	-- address of a flag variable the client is waiting on.
//			   We'll pass it back to the client's callback method.
// Returns:
//	None.
//
void
server_impl::test_nonblocking(
		const char *client_name,
		uint64_t flag_addr,
		int32_t, int32_t, int32_t, int32_t, int32_t,
		Environment &e)
{
	// os::printf("%s test_nonblocking()\n", msg_prefix());

	CORBA::Object_var	objp;
	typetest::client_var	clientp;
	Environment e2;

	// Get reference to the invoking client.
	objp = get_obj(client_name);
	if (CORBA::is_nil(objp)) {
		os::printf("%s ERROR: can't get reference to \"%s\"\n",
			msg_prefix(), client_name);
		return;
	}
	clientp = typetest::client::_narrow(objp);

	// Invoke the client's callback method to indicate that we've been
	// successfully invoked.
	clientp->test_nonblocking_callback(flag_addr, e2);
	(void) env_ok(e2, msg_prefix());
}
