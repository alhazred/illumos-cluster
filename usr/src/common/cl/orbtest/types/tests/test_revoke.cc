/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_revoke.cc	1.14	08/05/20 SMI"

//
// Revoke operation test
//

#include <orbtest/types/client_impl.h>
#include <orbtest/types/server_impl.h>


//
// Test client interface.  Invoked by driver "programs" and, in turn,
// invokes the corresponding method on the test server side.
// Parameters:
//	server_ref  --	test server object to invoke.
//	num_repeat  --	(ignored)
//	noverify    --	if true then server won't perform data verification.
// Returns:
//	Total test execution time.
//
int64_t
client_impl::test_revoke(
		typetest::server_ptr server_ref,
		int32_t,
		bool noverify,
		Environment &)
{
	// os::printf("%s test_revoke()\n", msg_prefix());

	// Exception expected to be returned when an invocation is made
	// after the server is revoked.
	CORBA::INV_OBJREF	expected(EBADF, CORBA::COMPLETED_NO);

	Environment	e2;
	os::hrtime_t	total_time = 0, start_time;

	if (noverify) {
		server_ref->set_noverify(e2);
		if (! env_ok(e2, msg_prefix())) {
			return (-1);
		}
	}

	start_time = os::gethrtime();
	server_ref->test_revoke(e2);
	if (! env_ok(e2, msg_prefix())) {
		os::printf("%s FAIL: revoke failed\n", msg_prefix());
		return (-1);
	}
	total_time += os::gethrtime() - start_time;

	os::printf("%s revoke succeeded\n", msg_prefix());

	// Now verify that revoke is in effect.
	// A no-argument invocation must fail with the expected exception.
	server_ref->test_noarg(e2);
	if (env_ok(e2, expected, msg_prefix())) {
		return (total_time);
	} else {
		return (-1);
	}
}


//
// Test server interface.  Invoked by the test client.
//
void
server_impl::test_revoke(Environment &)
{
	// os::printf("%s test_revoke()\n", msg_prefix());

	lck.lock();
	// Can only execute revoke once
	if (!(flags & (REVOKE | REVOKED))) {
		// Tell revoke thread to perform a revoke
		os::printf("%s Revoking server...\n", msg_prefix());
		flags |= REVOKE;
		revoke_start_cv.signal();
	} else {
		// Raise some exception
		// e.exception();
		os::warning("%s Revoke thread not created or has exited. "
			"Cannot execute test\n", msg_prefix());
	}
	lck.unlock();
}
