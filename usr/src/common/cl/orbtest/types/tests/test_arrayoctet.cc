/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_arrayoctet.cc	1.18	08/05/20 SMI"

#include <orbtest/types/server_impl.h>
#include <orbtest/types/client_impl.h>

//
// Forward definitions
//
static bool set_in(
	typetest::arrayOctet var,
	const char *msgpfx,
	bool set = true);

inline bool
verify_in(const typetest::arrayOctet v, const char *p)
{
	return (set_in((uint8_t (*)[7])v, p, false));
}

static bool set_out(
	typetest::arrayOctet var,
	const char *msgpfx,
	bool set = true);

inline bool
verify_out(typetest::arrayOctet v, const char *p)
{
	return (set_out(v, p, false));
}

static bool set_io_i(
	typetest::arrayOctet var,
	const char *msgpfx,
	bool set = true);

inline bool
verify_io_i(typetest::arrayOctet v, const char *p)
{
	return (set_io_i(v, p, false));
}

static bool set_io_o(
	typetest::arrayOctet var,
	const char *msgpfx,
	bool set = true);

inline bool
verify_io_o(typetest::arrayOctet v, const char *p)
{
	return (set_io_o(v, p, false));
}

#ifndef BUG_4086497
static bool set_ret(
	typetest::arrayOctet_slice *& var,
	const char *msgpfx,
	bool set = true);

inline bool
verify_ret(typetest::arrayOctet_slice *& v, const char *p)
{
	return (set_ret(v, p, false));
}
#endif	// ! BUG_4086497

//
// 2D array of octet (9x7 Elements)
//
// server_impl::test_arrayoctet
//
#ifndef BUG_4086497
typetest::arrayOctet_slice *
#else
void
#endif	// ! BUG_4086497
server_impl::test_arrayoctet(
	const typetest::arrayOctet in,
	typetest::arrayOctet out,
	typetest::arrayOctet inout,
	Environment &)
{
	// os::printf ("%s test_arrayoctet()\n", msg_prefix());

#ifndef BUG_4086497
	typetest::arrayOctet_slice * ret = 0;
	ret = typetest::arrayOctet_alloc();

	if (ret == CORBA::_nil) {
		os::printf("%s could not allocate return arrayOctet\n",
			msg_prefix());
		e.exception(new no_resources());
		return (CORBA::_nil);
	}
#endif	// ! BUG_4086497

	if (!verify_off) {
		// Verify values passed by client
		verify_in(in, msg_prefix());
		verify_io_i(inout, msg_prefix());
	}

	// Prepare values to be returned to client (populate)
	set_out(out, msg_prefix());
	set_io_o(inout, msg_prefix());

#ifndef BUG_4086497
	set_ret(ret, msg_prefix());
	return (ret);
#endif	// ! BUG_4086497
}

//
// client_impl::test_arrayoctet
//
int64_t
client_impl::test_arrayoctet(
	typetest::server_ptr server_ref,
	int32_t num_repeat,
	bool noverify,
	Environment &)
{
	// os::printf("%s test_arrayoctet()\n",msg_prefix());
	os::hrtime_t	total_time = 0, start_time;

	if (noverify) {
		Environment e2;

		server_ref->set_noverify(e2);
		if (! env_ok(e2, msg_prefix())) {
			return (-1);
		}
	}

	typetest::arrayOctet *in, *out, *inout;

	// Allocate dynamic arrays
	// This is done so we dont cause a stack overflow by allocating the
	// arrays of the stack.
	in = (typetest::arrayOctet *) new typetest::arrayOctet;
	out = (typetest::arrayOctet *) new typetest::arrayOctet;
	inout = (typetest::arrayOctet *) new typetest::arrayOctet;

	for (; num_repeat > 0; --num_repeat) {
		Environment e2;

#ifndef BUG_4086497
		typetest::arrayOctet_slice * ret;
#endif	// ! BUG_4086497

		// Prepare data to be passed to server.
		set_in(*in, msg_prefix());
		set_io_i(*inout, msg_prefix());

		// Invoke server.
		start_time = os::gethrtime();

#ifndef BUG_4086497
		ret = server_ref->test_arrayoctet(*in, *out, *inout, e2);
#else
		server_ref->test_arrayoctet(*in, *out, *inout, e2);
#endif	// ! BUG_4086497

		if (! env_ok(e2, msg_prefix())) {
			total_time = -1;
			break;
		}

		// To calculate orb transfer times take the elapsed time
		// minus the server's execution time
		total_time += os::gethrtime() - start_time;

		if (!noverify) {
			// Verify valies returned by server.
			verify_out(*out, msg_prefix());
			verify_io_o(*inout, msg_prefix());

#ifndef BUG_4086497
			verify_ret(ret, msg_prefix());
#endif	// ! BUG_4086497

		} // !noverify
	}


#ifndef BUG_4086497
	typetest::arrayOctet_free(ret);
#endif	// ! BUG_4086497

	// Delete the arrays
	delete[] in;
	delete[] out;
	delete[] inout;

	return (total_time);
}

//
// Common functions
//
bool
set_in(typetest::arrayOctet var, const char *msgpfx, bool set)
{
	uint8_t base	= 0x01;
	int i, j;

	// Set data
	if (set) {
		for (i = 0; i < 9; ++i) {
			for (j = 0; j < 7; ++j) {
				if (base == 0)
					base = 0x01;
				var[i][j] = base;
				base <<= 1;
			}
		}
		return (true);
	}

	// Verify Data
	for (i = 0; i < 9; ++i) {
		for (j = 0; j < 7; ++j) {
			if (base == 0)
				base = 0x01;
			if (var[i][j] == base) {
				base <<= 1;
				continue;
			}

			os::printf("FAIL: %s octet at index (%d,%d) of 'in' "
				"array of octet :\n", msgpfx, i, j);
			os::printf("\texpected = 0x%x, actual = 0x%x\n", base,
				var[i][j]);
			return (false);
		}
	}

	return (true);
}

bool
set_out(typetest::arrayOctet var, const char *msgpfx, bool set)
{
	uint8_t base	= ~0;
	int i, j;

	// Set data
	if (set) {
		for (i = 0; i < 9; ++i) {
			for (j = 0; j < 7; ++j) {
				if (base == 0)
					base = ~0;
				var[i][j] = base;
				base >>= 1;
			}
		}
		return (true);
	}

	// Verify Data
	for (i = 0; i < 9; ++i) {
		for (j = 0; j < 7; ++j) {
			if (base == 0)
				base = ~0;
			if (var[i][j] == base) {
				base >>= 1;
				continue;
			}

			os::printf("FAIL: %s octet at index (%d,%d) of 'out' "
				"array of octet:\n", msgpfx, i, j);
			os::printf("\texpected = 0x%x, actual = 0x%x\n", base,
				var[i][j]);
			return (false);
		}
	}

	return (true);
}

bool
set_io_i(typetest::arrayOctet var, const char *msgpfx, bool set)
{
	uint8_t base	= 0x01;
	int i, j;

	// Set data
	if (set) {
		for (i = 0; i < 9; ++i) {
			for (j = 0; j < 7; ++j) {
				if (base == 0)
					base = ~0;
				var[i][j] = base;
				base <<= 1;
			}
		}
		return (true);
	}

	// Verify Data
	for (i = 0; i < 9; ++i) {
		for (j = 0; j < 7; ++j) {
			if (base == 0)
				base = ~0;
			if (var[i][j] == base) {
				base <<= 1;
				continue;
			}

			os::printf("FAIL: %s octet at index (%d,%d) of 'oi_i' "
				"array of octet:\n", msgpfx, i, j);
			os::printf("\texpected = 0x%x, actual = 0x%x\n", base,
				var[i][j]);
			return (false);
		}
	}

	return (true);
}

bool
set_io_o(typetest::arrayOctet var, const char *msgpfx, bool set)
{
	uint8_t base	= 0x01;
	int i, j;

	// Set data
	if (set) {
		for (i = 0; i < 9; ++i) {
			for (j = 0; j < 7; ++j) {
				if (base == 0)
					base = ~0;
				var[i][j] = base;
				base >>= 1;
			}
		}
		return (true);
	}

	// Verify Data
	for (i = 0; i < 9; ++i) {
		for (j = 0; j < 7; ++j) {
			if (base == 0)
				base = ~0;
			if (var[i][j] == base) {
				base >>= 1;
				continue;
			}

			os::printf("FAIL: %s octet at index (%d,%d) of 'io_o' "
				"array of octet:\n", msgpfx, i, j);
			os::printf("\texpected = 0x%x, actual = 0x%x\n", base,
				var[i][j]);
			return (false);
		}
	}

	return (true);
}

#ifndef BUG_4086497
bool
set_ret(typetest::arrayOctet_slice *& var, const char *msgpfx, bool set)
{
	uint8_t base	= 0x01;
	int i, j;

	// Set data
	if (set) {
		for (i = 0; i < 9; ++i) {
			for (j = 0; j < 7; ++j) {
				var[i][j] = base;
				base = ~base;
			}
		}
		return (true);
	}

	// Verify Data
	for (i = 0; i < 9; ++i) {
		for (j = 0; j < 7; ++j) {
			if (var[i][j] == base) {
				base = ~base;
				continue;
			}

			os::printf("FAIL: %s octet at index (%d,%d) of 'ret' "
				"array of octet:\n", msgpfx, i);
			os::printf("\texpected = 0x%x, actual = 0x%x\n", base,
				var[i][j]);
			return (false);
		}
	}

	return (true);
}

#endif	// ! BUG_4086497
