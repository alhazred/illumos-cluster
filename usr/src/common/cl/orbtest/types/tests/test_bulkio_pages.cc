/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_bulkio_pages.cc	1.44	08/05/20 SMI"

//
// Bulkio pages test
//

#if !defined(_UNODE)

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/vfs.h>
#include <fcntl.h>
#include <sys/uio.h>
#include <sys/stream.h>
#include <sys/sol_version.h>
#ifdef ORBTEST_V1
#include <h/typetest_1.h>
#else
#include <h/typetest_0.h>
#endif
#include <orbtest/types/client_impl.h>
#include <orbtest/types/server_impl.h>

#if SOL_VERSION >= __s10
#ifndef FSI
#define	FSI // PSARC 2001/679 Storage Infrastructure: File System Interfaces
#endif
#endif

#define	BULKIO_PAGES_RDWR 1

#if defined(_KERNEL)
#include <vm/page.h>
#include <sys/file.h>
#include <sys/fs_subr.h>
#include <sys/fs/ufs_fs.h>
#include <sys/fs/ufs_lockfs.h>
#include <sys/fs/ufs_filio.h>
#include <sys/fs/ufs_inode.h>
#include <sys/fs/ufs_fsdir.h>

#include <pxfs/bulkio/bulkio_impl.h>

static void destroy_pages(page_t *pp);
#endif	// _KERNEL

#if defined(BULKIO_PAGES_RDWR)
static bulkio::in_pages_ptr set_in(uint32_t noofpages, const char *msgpfx);

#if defined(_KERNEL)
static void set_in(page_t *pp, uint32_t len, int &num);
static void verify_in(page_t *pp);
#endif

static bulkio::inout_pages_ptr set_inout(uint32_t noofpages,
		const char *msgpfx);
static bool verify_in(bulkio::in_pages_ptr uioobj, uint32_t len,
		const char *msgpfx);
static bool verify_inout(bulkio::inout_pages_ptr uioobj, uint32_t len,
		const char *msgpfx);
// static void server_set_inout(bulkio::inout_pages_ptr uioobj, uint32_t len,
//		const char *msgpfx);
#endif	// BULKIO_PAGES_RDWR

#ifdef FSI
static struct vnode *bulkio_vp = NULL;
#else
static struct vnode bulkio_vnode;
static struct vnode *bulkio_vp = &bulkio_vnode;
#endif

static struct vnode *vp;
static struct vfs pgio_vfs;

static bulkio::inout_pages_var	my_inout_pages;

#if defined(_KERNEL)
static struct vnodeops bulkio_vnodeops;

// Omitted for now, will come back with writes
#if 0
struct vnodeops bulkio_vnodeops = {
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_nosys,
	fs_dispose,
	fs_nosys,
	fs_nosys,
	fs_nosys
};
#endif	// always disabled currently

#endif	// _KERNEL

#if defined(_KERNEL)
//
// invoke_bulkio_pages_inout - actually invokes the pages_inout server routine
//
os::hrtime_t
invoke_bulkio_pages_inout(
		typetest::server_ptr server_ref,
		page_t *pp,
		uint32_t noofpages,
		const char *msgpfx)
{
	Environment		e2;
	os::hrtime_t		start_time;
	// bulkio::inout_pages_var	inout;

	my_inout_pages = bulkio_impl::conv_inout(pp, 0,
	    (uint32_t)(noofpages * (PAGESIZE)), B_READ, PROT_ALL);

	ASSERT(!CORBA::is_nil(server_ref));

	// Invoke the server
	start_time = os::gethrtime();
	server_ref->test_bulkio_pages_inout(my_inout_pages.INOUT(),
						noofpages, e2);
	if (!env_ok(e2, msgpfx)) {
		return (-1);
	}
	return (os::gethrtime() - start_time);
}
#endif

//
// test_bulkio_pages_inout - page read operation test
// Test client interface.  Invoked by driver "programs" and, in turn,
// invokes the corresponding method on the test server side.
// Parameters:
//	server_ref  --	test server object to invoke.
//	num_repeat  --	how many times to run the test.
//	noverify    --	if true then server won't perform data verification.
// Returns:
//	Total test execution time.
//
#if defined(_KERNEL)
int64_t
client_impl::test_bulkio_pages_inout(
		typetest::server_ptr server_ref,
		int32_t num_repeat,
		uint32_t noofpages,
		bool noverify,
		Environment &)
{
	os::hrtime_t	total_time = 0;
	os::hrtime_t	ret_time;
	char		filename[] = "/f2-1000.txt";
	int		num = 0;

	if (noverify) {
		Environment	e2;

		server_ref->set_noverify(e2);
		if (! env_ok(e2, msg_prefix())) {
			return (-1);
		}
	}

#ifdef FSI
	if (bulkio_vp == NULL) {
		bulkio_vp = vn_alloc(KM_SLEEP);
	}

	VN_SET_VFS_TYPE_DEV(bulkio_vp, &pgio_vfs, VNON, 0);
#else
	VN_INIT(bulkio_vp, &pgio_vfs, VNON, 0);
#endif
	bulkio_vnodeops.vop_dispose = fs_dispose;
	bulkio_vp->v_op = &bulkio_vnodeops;
	bulkio_vp->v_data = NULL;

	// why is this line here? - we never reference it
	// bulkio::inout_pages_var inout;

	// Prepare data to be passed to server.

#ifdef BUG_4276255

	struct seg kseg;
	kseg.s_as = &kas;
	page_t *pp = page_create_va(bulkio_vp, 0,
				    noofpages*(PAGESIZE),
				    PG_EXCL, &kseg, (caddr_t)

#else
	page_t *pp = page_create_va(bulkio_vp, 0,
				    noofpages*(PAGESIZE),
				    PG_EXCL, kas, (caddr_t)

#endif
				    ((unsigned long long)bulkio_vp &
				    PAGEMASK));

	for (; num_repeat > 0; --num_repeat) {
		ret_time = invoke_bulkio_pages_inout(
				server_ref, pp, noofpages, msg_prefix());
		if (ret_time == -1)
			break;
		total_time += ret_time;
		if (!noverify)
			verify_in(pp);

		if (num_repeat > 1) {
			//
			// Make timing measurements easier.
			// Allow server time to finish cleanup from last
			// invocation before starting next invocation.
			//
			os::usecsleep((os::usec_t)100000);	// 100ms
		}
	}

	// release the pages that we were using
	os::printf("Calling bulkio_impl::release_pages()\n");
	bulkio_impl::release_pages(my_inout_pages);

	// Destroy the pages
	os::printf("Calling destroy_pages()\n");
	destroy_pages(pp);

	return (total_time);
}
#else
int64_t
client_impl::test_bulkio_pages_inout(
		typetest::server_ptr,
		int32_t,
		uint32_t,
		bool,
		Environment &)
{
	(void) printf("This test does nothing in unode/user case\n");
	return (0);
}
#endif	// _KERNEL

#if defined(_KERNEL)
//
// invoke_bulkio_pages_in - actually calls the pages_in server routine
//
os::hrtime_t
invoke_bulkio_pages_in(
		typetest::server_ptr	server_ref,
		page_t			*pp,
		uint32_t		noofpages,
		const char		*msgpfx)
{
	Environment		e2;
	os::hrtime_t		start_time;
	bulkio::in_pages_var	in;

	in = bulkio_impl::conv_in(pp, 0,
	    (uint32_t)(noofpages * (PAGESIZE)), B_WRITE);

	// Invoke the server
	ASSERT(!CORBA::is_nil(server_ref));
	start_time = os::gethrtime();
	server_ref->test_bulkio_pages_in(in, noofpages, e2);

	//
	// Wait for the PpBuf to get destructed before "in" test goes away.
	// The write operation is not complete until this wait completes.
	// Thus it is necessary to include the wait time in the total time.
	//
	bulkio_impl::wait_on_sema(in);

	if (!env_ok(e2, msgpfx)) {
		return (-1);
	}
	return (os::gethrtime() - start_time);
}
#endif	// _KERNEL

//
// test_bulkio_pages_in - page write test
// Test client interface.  Invoked by driver "programs" and, in turn,
// invokes the corresponding method on the test server side.
// Parameters:
//	server_ref  --	test server object to invoke.
//	num_repeat  --	how many times to run the test.
//	noverify    --	if true then server won't perform data verification.
// Returns:
//	Total test execution time.
//
#if defined(_KERNEL)
int64_t
client_impl::test_bulkio_pages_in(
		typetest::server_ptr server_ref,
		int32_t num_repeat,
		uint32_t noofpages,
		bool noverify,
		Environment &)
{
	os::hrtime_t	total_time = 0;
	os::hrtime_t	ret_time;
	char		filename[] = "/f2-1000.txt";
	int		num = 0;

	if (noverify) {
		Environment	e2;

		server_ref->set_noverify(e2);
		if (! env_ok(e2, msg_prefix())) {
			return (-1);
		}
	}

	// Prepare data to be passed to server.

#ifdef FSI

	if (bulkio_vp == NULL) {
		bulkio_vp = vn_alloc(KM_SLEEP);
	}
	VN_SET_VFS_TYPE_DEV(bulkio_vp, &pgio_vfs, VNON, 0);
#else
	VN_INIT(bulkio_vp, &pgio_vfs, VNON, 0);
#endif

	bulkio_vnodeops.vop_dispose = fs_dispose;
	bulkio_vp->v_op = &bulkio_vnodeops;
	bulkio_vp->v_data = NULL;

#ifdef BUG_4276255
	struct seg kseg;
	kseg.s_as = &kas;
	page_t *pp = page_create_va(bulkio_vp, 0,
					noofpages*(PAGESIZE),
					PG_EXCL, &kseg, (caddr_t)
#else
	page_t *pp = page_create_va(bulkio_vp, 0,
					noofpages*(PAGESIZE),
					PG_EXCL, &kas, (caddr_t)
#endif
					((unsigned long long)bulkio_vp &
					PAGEMASK));

	uint32_t i = 0;
	page_t *plist = pp;
	do {
		set_in(pp, (uint32_t)PAGESIZE, num);
		pp = pp->p_next;
	} while (pp != plist);

	for (; num_repeat > 0; --num_repeat) {
		ret_time = invoke_bulkio_pages_in(
				server_ref, pp, noofpages, msg_prefix());
		if (ret_time == -1)
			break;
		total_time += ret_time;
	}

	destroy_pages(pp);

	return (total_time);
}
#else
int64_t
client_impl::test_bulkio_pages_in(
		typetest::server_ptr,
		int32_t,
		uint32_t,
		bool,
		Environment &)
{
	(void) printf("This test does not work in unode/user case\n");
	return (0);
}
#endif	// _KERNEL

//
// Bulkio in_pages - writes
// Test server interface.  Invoked by the test client.
//
#if defined(_KERNEL)
void
server_impl::test_bulkio_pages_in(
		bulkio::in_pages_ptr in,
		uint32_t,
		Environment &)
#else
void
server_impl::test_bulkio_pages_in(
		bulkio::in_pages_ptr,
		uint32_t,
		Environment &)
#endif	// _KERNEL
{
#if defined(_KERNEL)
	if (!verify_off) {
		// Verify values passed by client.
		fdbuffer_t	*fdbuf;
		fdbuf = bulkio_impl::conv(in);


		// page_t *pp = bulkio_impl::conv(in);
		page_t *pp = fdbuf->fd_un.pages;
		verify_in(pp);
	}
#endif	// _KERNEL
}

//
// Bulkio inout_pages - reads
// Test server interface. Invoked by the test client.
//
#if defined(_KERNEL)
void
server_impl::test_bulkio_pages_inout(
		bulkio::inout_pages_ptr &inout,
		uint32_t,
		Environment &)
{
	if (verify_off) {
		return;
	}
	// Fill values to be returned to client.
	//
	uint32_t i = 0;
	int num = 0;
	fdbuffer_t	*fdbuf;
	fdbuf = bulkio_impl::conv(inout);

	page_t *pp = fdbuf->fd_un.pages;
	page_t *plist = pp;
	do {
		set_in(pp, (uint32_t)PAGESIZE, num);
		pp = pp->p_next;
	} while (pp != plist);

}
#else
void
server_impl::test_bulkio_pages_inout(
		bulkio::inout_pages_ptr &,
		uint32_t,
		Environment &)
{
}
#endif	// _KERNEL

//
// The following function is used to destroy pages. The code is similar to
// that of the VM function pvn_read_done, and simpler because it just destroys
// the pages.
//
#if defined(_KERNEL)
void
destroy_pages(page_t *plist)
{
	page_t *pp;

	while (plist != NULL) {
		pp = plist;
		page_sub(&plist, pp);
		page_io_unlock(pp);
		page_destroy(pp, 0);
	}
}
#endif	// _KERNEL

// The following are there only if reads/writes are implemented
#if defined(BULKIO_PAGES_RDWR)

//
// Does nothing for now - but will set data for the write
//
bulkio::in_pages_ptr
set_in(uint32_t, const char *)
{
	bulkio::in_pages_ptr uioobj = NULL;
	return (uioobj);
}

#if defined(_KERNEL)
void
set_in(page_t *pp, uint32_t len, int &num)
{
	uint32_t i;
	int *ptr;

	struct buf buffer;

	//
	// B_KERNBUF is obsoleted by S9
	//
#ifdef B_KERNBUF
	buffer.b_flags = B_KERNBUF | B_PAGEIO;
#else
	buffer.b_flags = B_PAGEIO;
#endif
	buffer.b_edev = 0;
	buffer.b_dev = 0;
	buffer.b_bcount = len;
	buffer.b_bufsize = len;
	buffer.b_pages = pp;
	buffer.b_un.b_addr = NULL;

	bp_mapin(&buffer);
	ptr = (int *)buffer.b_un.b_addr;
	len /= (uint32_t)sizeof (int);
	for (i = 0; i < len; i++)
		ptr[i] = num++;
	bp_mapout(&buffer);
}
#endif	// _KERNEL

// Does nothing for now - will verify the write
//
#if defined(_KERNEL)
void
verify_in(page_t *pp)
{
	page_t *plist = pp;

	uint32_t len = (uint32_t)PAGESIZE;
	uint32_t ilen = (uint32_t)(len / sizeof (int));
	struct buf buffer;
	int *ptr;
	uint32_t i;
	int num = 0;
	do {
	//
	// B_KERNBUF is obsoleted by S9
	//
#ifdef B_KERNBUF
		buffer.b_flags = B_KERNBUF | B_PAGEIO;
#else
		buffer.b_flags = B_PAGEIO;
#endif
		buffer.b_edev = 0;
		buffer.b_dev = 0;
		buffer.b_bcount = len;
		buffer.b_bufsize = len;
		buffer.b_pages = pp;
		buffer.b_un.b_addr = NULL;
		bp_mapin(&buffer);

		ptr = (int *)buffer.b_un.b_addr;
		for (i = 0; i < ilen; i++)
		if (ptr[i] != num++) {
			os::prom_printf("Int no. %d doesn't match %d\n",
					num, ptr[i]);
			os::prom_printf("FAIL: Bulkio_pages\n");
			bp_mapout(&buffer);
			return;
		}

		bp_mapout(&buffer);

		pp = pp->p_next;
	} while (plist != pp);
}

bool
verify_in(bulkio::in_pages_ptr pageobj, uint32_t noofpages,
	const char *msgpfx)
{
	fdbuffer_t *fdbuf;

	fdbuf = bulkio_impl::conv(pageobj);

	page_t *pp = fdbuf->fd_un.pages;

	uint32_t len = (uint32_t)(noofpages * PAGESIZE);
	uint32_t i;
	unsigned char *ptr;

	struct buf buffer;
	//
	// B_KERNBUF is obsoleted by S9
	//
#ifdef B_KERNBUF
	buffer.b_flags = B_KERNBUF | B_PAGEIO;
#else
	buffer.b_flags = B_PAGEIO;
#endif
	buffer.b_edev = 0;
	buffer.b_dev = 0;
	buffer.b_bcount = len;
	buffer.b_bufsize = len;
	buffer.b_pages = pp;
	buffer.b_un.b_addr = NULL;
	bp_mapin(&buffer);

	ptr = (unsigned char *)buffer.b_un.b_addr;
	for (i = 0; i < len; i++)
		if (ptr[i] != (unsigned char) (i % 8)) {
			os::prom_printf("Byte no. %d doesn't match\n", i);
			os::prom_printf("FAIL: %s Bulkio_pages\n",
					msgpfx);
		}

	bp_mapout(&buffer);
	return (true);
}
#else
bool
verify_in(bulkio::in_pages_ptr, uint32_t, const char *)
{
	return (false);
}
#endif	// _KERNEL

//
// Nothing happens, but this is intended to implement the server-side
// read.
//
// void
// server_set_inout(bulkio::inout_pages_ptr, uint32_t, const char *)
// {
// }

//
// Verify of the read.
//
bool
verify_inout(bulkio::inout_pages_ptr, uint32_t, const char *)
{
	return (true);
}
#endif	// BULKIO_PAGES_RDWR

#endif // _UNODE
