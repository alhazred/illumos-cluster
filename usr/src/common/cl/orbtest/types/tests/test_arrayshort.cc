/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_arrayshort.cc	1.20	08/05/20 SMI"

#include <orbtest/types/server_impl.h>
#include <orbtest/types/client_impl.h>

//
// Forward definitions
//
static bool set_in(
	typetest::arrayShort var,
	const char *msgpfx,
	bool set = true);

inline bool
verify_in(const typetest::arrayShort v, const char *p)
{
	return (set_in((int16_t *)v, p, false));
}

static bool set_out(
	typetest::arrayShort var,
	const char *msgpfx,
	bool set = true);

inline bool
verify_out(typetest::arrayShort v, const char *p)
{
	return (set_out(v, p, false));
}

static bool set_io_i(
	typetest::arrayShort var,
	const char *msgpfx,
	bool set = true);

inline bool
verify_io_i(typetest::arrayShort v, const char *p)
{
	return (set_io_i(v, p, false));
}

static bool set_io_o(
	typetest::arrayShort var,
	const char *msgpfx,
	bool set = true);

inline bool
verify_io_o(typetest::arrayShort v, const char *p)
{
	return (set_io_o(v, p, false));
}

#ifndef BUG_4086497
static bool set_ret(
	typetest::arrayShort_slice *& var,
	const char *msgpfx,
	bool set = true);

inline bool
verify_ret(typetest::arrayShort_slice *& v, const char *p)
{
	return (set_ret(v, p, false));
}
#endif	// ! BUG_4086497

//
// Array of Short (int16_t) test
//
// server_impl::test_arrayshort
//
#ifndef BUG_4086497
typetest::arraShort_slice *
#else
void
#endif	// ! BUG_4086497
server_impl::test_arrayshort(
	const typetest::arrayShort in,
	typetest::arrayShort out,
	typetest::arrayShort inout,
	Environment &)
{
	// os::printf("%s test_arrayshort()\n", msg_prefix());

#ifndef BUG_4086497
	typetest::arrayShort_slice * ret;
	ret = typetest::arrayShort_alloc();

	if (ret == CORBA::_nil) {
		os::printf("%s could not allocate return arrayShort\n",
			msg_prefix());
		e.exception(new no_resources());
		return (CORBAL::_nil);
	}
#endif	// ! BUG_4086497

	if (!verify_off) {
		// Verify values passed by client
		verify_in(in, msg_prefix());
		verify_io_i(inout, msg_prefix());
	}

	// Prepare values for return (populate)
	set_out(out, msg_prefix());
	set_io_o(inout, msg_prefix());

#ifndef BUG_4086497
	set_ret(ret, msg_prefix());

	return (ret);
#endif	// ! BUG_4086497
}

//
// client_impl::test_arrayshort
//
int64_t
client_impl::test_arrayshort(
	typetest::server_ptr server_ref,
	int32_t num_repeat,
	bool noverify,
	Environment &)
{
	// os::printf("%s test_arrayshort()\n",msg_prefix());
	os::hrtime_t	total_time = 0, start_time;

	if (noverify) {
		Environment e2;

		server_ref->set_noverify(e2);
		if (! env_ok(e2, msg_prefix())) {
			return (-1);
		}
	}

	typetest::arrayShort *in, *out, *inout;

	// Allocate the arrays on the heap to avoid stack overflow
	in = (typetest::arrayShort *) new typetest::arrayShort;
	out = (typetest::arrayShort *) new typetest::arrayShort;
	inout = (typetest::arrayShort *) new typetest::arrayShort;

	for (; num_repeat > 0; --num_repeat) {
		Environment e2;

#ifndef BUG_4086497
		arrayShort_slice * ret = 0;
#endif	// ! BUG_4086497

		// Prepare data to be passed to server.
		set_in(*in, msg_prefix());
		set_io_i(*inout, msg_prefix());

		// Invoke server.
		start_time = os::gethrtime();

#ifndef BUG_4086497
		ret = server_ref->test_arrayshort(*in, *out, *inout, e2);
#else
		server_ref->test_arrayshort(*in, *out, *inout, e2);
#endif	// ! BUG_4086497

		if (! env_ok(e2, msg_prefix())) {
			total_time = -1;
			break;
		}

		total_time += os::gethrtime() - start_time;

		if (!noverify) {
			// Verify values returned by server.
			verify_out(*out, msg_prefix());
			verify_io_o(*inout, msg_prefix());

#ifndef BUG_4086497
			verify_ret(ret, msg_prefix());
#endif	// ! BUG_4086497

		} // !noverify
	}

#ifndef BUG_4086497
	typetest::arrayShort_free(ret);
#endif	// ! BUG_4086497

	// Delete the arrays
	delete[] in;
	delete[] out;
	delete[] inout;

	return (total_time);
}

//
// Common Functions
//
bool
set_in(typetest::arrayShort var, const char *msgpfx, bool set)
{
	int16_t base	= 0x0001;
	int i;
	const int elements = (int)(sizeof (typetest::arrayShort) /
	    sizeof (int16_t));

	// Set data
	if (set) {
		for (i = 0; i < elements; ++i) {
			if (base == 0)
				base = 1;
			var[i] = base;
			base <<= 1;
		}
		return (true);
	}

	// Verify Data
	for (i = 0; i < elements; ++i) {
		if (base == 0)
			base = 1;
		if (var[i] == base) {
			base <<= 1;
			continue;
		}

		os::printf("FAIL: %s short at index %d of 'in' array of "
			"short:\n", msgpfx, i);
		os::printf("\texpected = 0x%hx, actual = 0x%hx\n",
			base, var[i]);
		return (false);
	}

	return (true);
}

bool
set_out(typetest::arrayShort var, const char *msgpfx, bool set)
{
	int16_t base	= ~0;
	int i;
	const int elements = (int)(sizeof (typetest::arrayShort) /
	    sizeof (int16_t));

	// Set data
	if (set) {
		for (i = 0; i < elements; ++i) {
			if (base == 0)
				base = ~0;
			var[i] = base;
			base >>= 1;
		}
		return (true);
	}

	// Verify Data
	for (i = 0; i < elements; ++i) {
		if (base == 0)
			base = ~0;
		if (var[i] == base) {
			base >>= 1;
			continue;
		}

		os::printf("FAIL: %s short at index %d of 'out' array of "
			"short:\n", msgpfx, i);
		os::printf("\texpected = 0x%hx, actual = 0x%hx\n",
			base, var[i]);
		return (false);
	}

	return (true);
}

bool
set_io_i(typetest::arrayShort var, const char *msgpfx, bool set)
{
	int16_t base	= 1;
	int i;
	const int elements = (int)(sizeof (typetest::arrayShort) /
	    sizeof (int16_t));

	// Set data
	if (set) {
		for (i = 0; i < elements; ++i) {
			if (base == 0)
				base = ~0;
			var[i] = base;
			base <<= 1;
		}
		return (true);
	}

	// Verify Data
	for (i = 0; i < elements; ++i) {
		if (base == 0)
			base = ~0;
		if (var[i] == base) {
			base <<= 1;
			continue;
		}

		os::printf("FAIL: %s short at index %d of 'io_i' array of "
			"short:\n", msgpfx, i);
		os::printf("\texpected = 0x%hx, actual = 0x%hx\n",
			base, var[i]);
		return (false);
	}

	return (true);
}

bool
set_io_o(typetest::arrayShort var, const char *msgpfx, bool set)
{
	int16_t base	= 1;
	int i;
	const int elements = (int)(sizeof (typetest::arrayShort) /
	    sizeof (int16_t));

	// Set data
	if (set) {
		for (i = 0; i < elements; ++i) {
			if (base == 0)
				base = ~0;
			var[i] = base;
			base >>= 1;
		}
		return (true);
	}

	// Verify Data
	for (i = 0; i < elements; ++i) {
		if (base == 0)
			base = ~0;
		if (var[i] == base) {
			base >>= 1;
			continue;
		}

		os::printf("FAIL: %s short at index %d of 'io_o' array of "
			"short:\n", msgpfx, i);
		os::printf("\texpected = 0x%hx, actual = 0x%hx\n",
			base, var[i]);
		return (false);
	}

	return (true);
}

#ifndef BUG_4086497
bool
set_ret(typetest::arrayShort_slice *& var, const char *msgpfx, bool set)
{
	int16_t base	= 0x0101;
	int i;
	const int elements =
		sizeof (typetest::arrayShort)/sizeof (int16_t);

	// Set data
	if (set) {
		for (i = 0; i < elements; ++i) {
			var[i] = base;
			base = ~base;
		}
		return (true);
	}

	// Verify Data
	for (i = 0; i < elements; ++i) {
		if (var[i] == base) {
			base = ~base;
			continue;
		}

		os::printf("FAIL: %s short at index %d of 'ret' array of "
			"short:\n", msgpfx, i);
		os::printf("\texpected = 0x%hx, actual = 0x%hx\n",
			base, var[i]);
		return (false);
	}

	return (true);
}

#endif	// ! BUG_4086497
