/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)test_objinhmany.cc	1.14	08/05/20 SMI"

//
// Object reference test
//

#include <orbtest/types/client_impl.h>
#include <orbtest/types/server_impl.h>


static bool	verify(typetest::InhObjA_ptr objp, int32_t expected_id,
			const char *paramtype, const char *msgpfx,
			char expected_name);


//
// test_objinhmany - tests object single inheritance
// with many levels in the ancestry tree.
//
// Test client interface.  Invoked by driver "programs" and, in turn,
// invokes the corresponding method on the test server side.
// Parameters:
//	server_ref  --	test server object to invoke.
//	num_repeat  --	how many times to run the test.
//	noverify    --	if true then server won't perform data verification.
// Returns:
//	Total test execution time.
//
int64_t
client_impl::test_objinhmany(
		typetest::server_ptr server_ref,
		int32_t num_repeat,
		bool noverify,
		Environment &)
{
	// os::printf("%s test_objinhmany()\n", msg_prefix());
	os::hrtime_t	total_time = 0, start_time;

	if (noverify) {
		Environment	e3;

		server_ref->set_noverify(e3);
		if (! env_ok(e3, msg_prefix())) {
			return (-1);
		}
	}

	for (; num_repeat > 0; --num_repeat) {
		Environment		e2;
		InhObjC_impl		in_impl(in_obj_id);
		InhObjA_impl		inout_impl(inout_obj_id);
		typetest::InhObjC_ptr	in;
		typetest::InhObjA_ptr	inout;
		typetest::InhObjA_ptr	out = typetest::InhObjA::_nil();
		typetest::InhObjA_ptr	ret = typetest::InhObjA::_nil();

		// Prepare object references to be passed to server.
		in = in_impl.get_objref();
		inout = inout_impl.get_objref();

		// Invoke server.
		start_time = os::gethrtime();
		ret = server_ref->test_objinhmany(in, out, inout, e2);
		if (! env_ok(e2, msg_prefix())) {
			total_time = -1;
			num_repeat = 0;		// end loop
		} else {
			total_time += os::gethrtime() - start_time;

			// Verify object references from server.
			verify(out, inout_obj_id, "'out'", msg_prefix(), 'A');
			verify(inout, in_obj_id, "'inout'", msg_prefix(), 'C');
			verify(ret, in_obj_id, "returned", msg_prefix(), 'C');
		}

		CORBA::release(in);
		CORBA::release(inout);
		CORBA::release(out);
		CORBA::release(ret);

		// Verify that _unreferenced() is (or will be) called.
		const uint32_t timeout = 30;
		InhObjC_impl *in_implp = &in_impl;
		in_implp->wait_until_unreferenced(timeout);
		InhObjA_impl *inout_implp = &inout_impl;
		inout_implp->wait_until_unreferenced(timeout);
	}

	return (total_time);
}


//
// test_objinhmany - tests object single inheritance
// with many levels in the ancestry tree.
//
// Test server interface.  Invoked by the test client.
//
typetest::InhObjA_ptr
server_impl::test_objinhmany(
		typetest::InhObjA_ptr  in,
		typetest::InhObjA_out  out,
		typetest::InhObjA_ptr &inout,
		Environment &)
{
	// os::printf("%s test_objinhmany()\n", msg_prefix());

	if (!verify_off) {
		// Verify object references from client.
		verify(in, in_obj_id, "'in'", msg_prefix(), 'C');
		verify(inout, inout_obj_id, "'inout'", msg_prefix(), 'A');
	}

	// Prepare object references for client.
	out = typetest::InhObjA::_duplicate(inout);	// copy inout to out

	CORBA::release(inout);
	inout = typetest::InhObjA::_duplicate(in);	// copy in into inout

	return (typetest::InhObjA::_duplicate(in));	// return in object
}


//
// Verifies that test object reference "objp" has the expected ID
// "expected_id".
//
bool
verify(typetest::InhObjA_ptr objp, int32_t expected_id, const char *paramtype,
	const char *msgpfx, char expected_name)
{
	int32_t		idnum;
	char		obj_name;
	Environment	e;

	if (CORBA::is_nil(objp)) {
		os::printf("FAIL: %s Many inherited object reference %s "
			"is NIL\n", msgpfx, paramtype);
		return (false);
	}

	// Check if the id is right.
	idnum = objp->id(e);
	if (e.exception()) {
		os::printf("FAIL: %s Many inherited object reference had "
			"exception in id()\n", msgpfx);
		e.clear();
		return (false);
	}
	if (idnum != expected_id) {
		os::printf("FAIL: %s %s Many inherited object reference:\n",
			paramtype, msgpfx);
		os::printf("\texpected id = %d, actual id = %d\n",
			expected_id, idnum);
		return (false);
	}

	// Check if the name is as expected.
	obj_name = objp->name(e);
	if (e.exception()) {
		os::printf("FAIL: %s Many inherited object reference had "
			"exception in name()\n", msgpfx);
		e.clear();
		return (false);
	}
	if (obj_name != expected_name) {
		os::printf("FAIL %s %s Many inherited object reference:\n",
			paramtype, msgpfx);
		os::printf("\texpected name = %c, actual name = %c\n",
			expected_name, obj_name);
		return (false);
	}

	return (true);
}
