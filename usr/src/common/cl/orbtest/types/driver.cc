/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)driver.cc	1.97	08/05/20 SMI"

#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <sys/os.h>
#include <nslib/ns.h>

#ifdef ORBTEST_V1
#include <h/typetest_1.h>
#else
#include <h/typetest_0.h>
#endif

#include <orbtest/types/impl_common.h>

#include <h/network.h>

#if !defined(_UNODE)
#include <orb/infrastructure/orb.h>
#endif

static const char prefix[] = "driver:";
static unsigned short server_version = 0;

//
// Globals
//
int iterations;
const char Auserver_name[] = "IDL Versioning test server A (user-land)";
const char Akserver_name[] = "IDL Versioning test server A (kernel-land)";
#ifdef ORBTEST_V1
const char Buserver_name[] = "IDL Versioning test server B (user-land)";
const char Bkserver_name[] = "IDL Versioning test server B (kernel-land)";
const char Cuserver_name[] = "IDL Versioning test server C (user-land)";
const char Ckserver_name[] = "IDL Versioning test server C (kernel-land)";
const char Duserver_name[] = "IDL Versioning test server D (user-land)";
const char Dkserver_name[] = "IDL Versioning test server D (kernel-land)";
#endif

const unsigned short USERLAND = 1;
const unsigned short KERNELLAND = 2;

// Ranges of number option arguments
enum Optarg_ranges {
	RANGE_ALL,			// all possible (long) numbers
	RANGE_GT0,			// > 0
	RANGE_GE0,			// >= 0
	RANGE_LT0,			// < 0
	RANGE_LE0			// >= 0
};

// Whether or not an option argument is optional
enum Optarg_optional {
	OPTARG_OPTIONAL,
	OPTARG_REQUIRED
};

// Connect which test client with which test server.
enum OptConn {
	CONN_UN2UN = (1 << 0),		// UNODE  client <-> UNODE server
	CONN_U2U = (1 << 1),		// USER   client <-> USER   server
	CONN_U2K = (1 << 2),		// USER   client <-> KERNEL server
	CONN_K2U = (1 << 3),		// KERNEL client <-> USER   server
	CONN_K2K = (1 << 4),		// KERNEL client <-> KERNEL server

	CONN_ALL = (~0)			// test all connection combinations
};

// Which test to run.
const uint64_t
	TEST_NOARG		= (1ULL << 0),	// no-argument

	TEST_BOOLEAN		= (1ULL << 1),	// Boolean (bool)
	TEST_CHAR		= (1ULL << 2),	// Char (int8_t)
	TEST_OCTET		= (1ULL << 3),	// Octet (uint8_t)
	TEST_SHORT		= (1ULL << 4),	// Short (int16_t)
	TEST_USHORT		= (1ULL << 5),	// UShort (uint16_t)
	TEST_LONG		= (1ULL << 6),	// Long (int32_t)
	TEST_ULONG		= (1ULL << 7),	// ULong (uint32_t)
	TEST_LONGLONG		= (1ULL << 8),	// LongLong (int64_t)
	TEST_ULONGLONG		= (1ULL << 9),	// ULongLong (uint64_t)

	TEST_BSTRING 		= (1ULL << 10),	// Bounded string
	TEST_USTRING 		= (1ULL << 11),	// Unbounded string
	TEST_BSEQ		= (1ULL << 12),	// Bounded sequence
	TEST_USEQ		= (1ULL << 13),	// Unbounded sequence
	TEST_BSEQUSEQ		= (1ULL << 14),	// Bounded seq of
						// unbounded sequences
	TEST_USEQBSEQ		= (1ULL << 15),	// Unbounded sequence of
						// bounded sequences
	TEST_BSEQUSTR		= (1ULL << 16),	// Bounded sequence of
						// unbounded string
	TEST_USEQBSTR		= (1ULL << 17),	// Unbounded sequence of
						// bounded string

	TEST_FSTRUCT 		= (1ULL << 18),	// Fixed struct
	TEST_VSTRUCT 		= (1ULL << 19),	// Variable struct

	TEST_FUNION  		= (1ULL << 20),	// Fixed union
	TEST_VUNION  		= (1ULL << 21),	// Variable union

	TEST_OBJREF		= (1ULL << 22),	// Object reference
	TEST_OBJSEQ		= (1ULL << 23),	// Object reference sequence

	TEST_REVOKE		= (1ULL << 24),	// Revoke operation

	TEST_ONEWAY		= (1ULL << 25),	// oneway
	TEST_NONBLOCKING	= (1ULL << 26),	// oneway nonblocking

	TEST_ARRAYSHORT		= (1ULL << 27),	// Array of Short (int16_t)
	TEST_ARRAYOCTET		= (1ULL << 28),	// Array of UShort (uint8_t)
						// (2D)
	TEST_ARRAYFSTRUCT	= (1ULL << 29),	// Array of Fstruct
	TEST_ARRAYFUNION	= (1ULL << 30),	// Array of Funion

	TEST_EMPTYEXCEPTION	= (1ULL << 31),	// Empty Exception
	TEST_BASICEXCEPTION	= (1ULL << 32),	// Basic Exception
	TEST_COMPLEXEXCEPTION	= (1ULL << 33),	// Complex Exception
	TEST_STRINGEXCEPTION	= (1ULL << 34),	// String Exception

	TEST_OBJINHSIMPLE	= (1ULL << 35), // Single inheritance,
						// one level in ancestry tree.

	TEST_OBJINHMANY		= (1ULL << 36), // Single inheritance,
						// many level in ancestry tree.

	TEST_OBJINHMULTIPLE	= (1ULL << 37), // Multiple-inherited object

	TEST_BULKIO_UIO_IN	= (1ULL << 38), // Bulkio raw I/O write
	TEST_BULKIO_UIO_INOUT	= (1ULL << 39), // Bulkio raw I/O read
	TEST_BULKIO_PAGES_IN	= (1ULL << 40), // Bulkio pages write
	TEST_BULKIO_PAGES_INOUT	= (1ULL << 41), // Bulkio pages read

	TEST_CRED		= (1ULL << 42),	// Cred Object
	TEST_DATA_CONTAINER	= (1ULL << 43),	// Data Container Object

	TEST_SEQ_READ		= (1ULL << 44),	// Unounded sequence read
	TEST_SEQ_WRITE		= (1ULL << 45),	// Unbounded sequence write
	TEST_SEQ_RW		= (1ULL << 46),	// Unbounded sequence read/write
	TEST_IDL_VERSIONING	= (1ULL << 47),	// IDL Versioning Tests
	TEST_FSTRUCTSEQ		= (1ULL << 48),
	TEST_NULLSEQ		= (1ULL << 49),
	TEST_0LENSEQ		= (1ULL << 50),

	// Run all tests if no specific ones are specified  except:
	//
	//	TEST_REVOKE	-- revoke test should not be included in the
	//			   all case, because it can only be done once.
	//
	//	TEST_SEQ_READ/WRITE/RW - these tests exist to support
	//			performance comparisons. The functionality
	//			is already covered by unbounded sequence test.
	//
	TEST_ALL	= ~(TEST_REVOKE |
			    TEST_SEQ_READ |
			    TEST_SEQ_WRITE |
			    TEST_SEQ_RW);

// Option variables
struct Options {
	Options();

	bool		done;	// invoke done() on test servers and clients
	uint32_t	conn;	// client/server combination
	uint64_t	test;	// which test to run

	// Which test(s) and connection(s) are actually specified.  Allows
	// to skip some tests unless specified on command line.
	uint64_t	test_specified;
	uint32_t	conn_specified;

	long	iter;		// how many times to run each test
	bool	noverify;	// set if verification off server side

	long	bstring_len;
	long	ustring_len;
	long	bseq_len;
	long	useq_len;
	long	seq_read_len;
	long	seq_write_len;
	long	seq_rw_len;
	long	bsequseq_len, bsequseq_max;
	long	useqbseq_len, useqbseq_max;
	long	fstructseq_len, fstructseq_max;
	long	nullseq_len, nullseq_max;
	long	zerolenseq_len, zerolenseq_max;
	long	bsequstr_len, bsequstr_max;
	long	useqbstr_len, useqbstr_max;
	long	vstruct_len;
	long	vunion_len;
	long	objseq_len;

	long    in_uio_len;
	long	inout_uio_len;
	long    in_pages_noof;
	long	inout_pages_noof;
	long	data_len;
};

static bool	parse_options(int argc, char *argv[], Options &opts);
static int	numarg(
			long &var,
			const char *optstr,
			int argc,
			char *argv[],
			int optarg_idx,
			Optarg_ranges range,
			Optarg_optional optional);
static bool	doit(Options &opts);
static bool	test(
			const char *client_name,
			const char *server_name,
			Options &opts);
static bool	do_versioning_tests1(unsigned short mode);
#ifdef ORBTEST_V1
static bool	do_versioning_tests2(unsigned short mode);
static bool	do_versioning_tests3(unsigned short mode);
#endif
static void	done(const char *name);
static void	print_stats(int64_t total_time, int32_t num_iter);

Options::Options()
{
	done = false;
	conn = 0;
	test = 0;
	iter = 1;
	bstring_len = 0;
	ustring_len = 1;
	bseq_len	= 10;
	useq_len	= 10;
	seq_read_len	= 10;
	seq_write_len	= 10;
	seq_rw_len	= 10;
	bsequseq_len = 10;
	bsequseq_max = 10;
	useqbseq_len = 10;
	useqbseq_max = 10;
	fstructseq_len = 10;
	fstructseq_max = 10;
	nullseq_len = 10;
	nullseq_max = 10;
	zerolenseq_len = 10;
	zerolenseq_max = 10;
	bsequstr_len = 10;
	bsequstr_max = 0;
	useqbstr_len = 10;
	useqbstr_max = 1;
	vstruct_len = 0;
	vunion_len = 1;
	in_uio_len = 8192;
	inout_uio_len = 8192;
	in_pages_noof = 1;
	inout_pages_noof = 1;
	objseq_len = 10;
	data_len = 100;
}

#if defined(_UNODE)
int
unode_init(void)
{
	os::printf("Types test module loaded\n");
	return (0);
}
#endif

int
#if defined(_UNODE)
driver(int argc, char *argv[])
#else
main(int argc, char *argv[])
#endif
{
	Options	opts;

	if (! parse_options(argc, argv, opts)) {
		return (1);
	}

#if !defined(_UNODE)
	if (ORB::initialize() != 0) {
		(void) fprintf(stderr, "ERROR: Can't initialize ORB\n");
		return (1);
	}
#endif

	if (! doit(opts)) {
		return (1);
	}

	return (0);
}

//
// Parse options
//
static bool
parse_options(int argc, char *argv[], Options &opts)
{
	int	ret;

	opts.conn = 0;
	opts.test = 0;
	opts.noverify = false;

	for (int idx = 1; idx < argc; ++idx) {
		const char	*arg = argv[idx];

		// -noverify
		//
		//	Prevent test server for performing verification.
		//
		if (strcmp(arg, "-noverify") == 0) {
			(void) printf("No verify (-noverify) option set\n");
			opts.noverify = true;
			continue;
		}

		// -i    <num_iter>
		// -iter <num_iter>
		//
		//	How many times to run the tests.
		//
		if (strcmp(arg, "-i") == 0 || strcmp(arg, "-iter") == 0) {
			if (numarg(opts.iter, arg, argc, argv, ++idx,
			    RANGE_GE0, OPTARG_REQUIRED) < 0) {
				return (false);
			}
			continue;
		}

#if !defined(_UNODE)
		// -u2u
		//
		//	Test USER client <-> USER server.
		//
		if (strcmp(arg, "-u2u") == 0) {
			opts.conn |= CONN_U2U;
			continue;
		}

		// -u2k
		//
		//	Test USER client <-> KERNEL server.
		//
		if (strcmp(arg, "-u2k") == 0) {
			opts.conn |= CONN_U2K;
			continue;
		}

		// -k2u
		//
		//	Test KERNEL client <-> USER server.
		//
		if (strcmp(arg, "-k2u") == 0) {
			opts.conn |= CONN_K2U;
			continue;
		}

		// -k2k
		//
		//	Test KERNEL client <-> KERNEL server.
		//
		if (strcmp(arg, "-k2k") == 0) {
			opts.conn |= CONN_K2K;
			continue;
		}
#endif

		// -noarg
		//
		//	No-argument test.
		//
		if (strcmp(arg, "-noarg") == 0) {
			opts.test |= TEST_NOARG;
			continue;
		}

		// -bool
		// -boolean
		//
		//	Boolean (bool) test.
		//
		if (strcmp(arg, "-bool") == 0 || strcmp(arg, "-boolean") == 0) {
			opts.test |= TEST_BOOLEAN;
			continue;
		}

		// -char
		//
		//	Char (int8_t) test.
		//
		if (strcmp(arg, "-char") == 0) {
			opts.test |= TEST_CHAR;
			continue;
		}

		// -octet
		//
		//	Octet (uint8_t) test.
		//
		if (strcmp(arg, "-octet") == 0) {
			opts.test |= TEST_OCTET;
			continue;
		}

		// -short
		//
		//	Short (int16_t) test.
		//
		if (strcmp(arg, "-short") == 0) {
			opts.test |= TEST_SHORT;
			continue;
		}

		// -ushort
		//
		//	UShort (uint16_t) test.
		//
		if (strcmp(arg, "-ushort") == 0) {
			opts.test |= TEST_USHORT;
			continue;
		}

		// -long
		//
		//	Long (int32_t) test.
		//
		if (strcmp(arg, "-long") == 0) {
			opts.test |= TEST_LONG;
			continue;
		}

		// -ulong
		//
		//	ULong (uint32_t) test.
		//
		if (strcmp(arg, "-ulong") == 0) {
			opts.test |= TEST_ULONG;
			continue;
		}

		// -longlong
		//
		//	LongLong (int64_t) test.
		//
		if (strcmp(arg, "-longlong") == 0) {
			opts.test |= TEST_LONGLONG;
			continue;
		}

		// -ulonglong
		//
		//	ULongLong (uint64_t) test.
		//
		if (strcmp(arg, "-ulonglong") == 0) {
			opts.test |= TEST_ULONGLONG;
			continue;
		}

		// -basic
		//
		//	Run all basic types tests above.
		//
		if (strcmp(arg, "-basic") == 0) {
			opts.test |=
				TEST_BOOLEAN |
				TEST_CHAR |
				TEST_OCTET |
				TEST_SHORT |
				TEST_USHORT |
				TEST_LONG |
				TEST_ULONG |
				TEST_LONGLONG |
				TEST_ULONGLONG;
			continue;
		}

		// -bstring [string_len]
		//
		//	Bounded string test.
		//
		if (strcmp(arg, "-bstring") == 0) {
			ret = numarg(opts.bstring_len, arg, argc, argv,
					idx + 1, RANGE_GE0, OPTARG_OPTIONAL);
			if (ret < 0)
				return (false);
			else if (ret > 0)	// if got option argument
				++idx;
			opts.test |= TEST_BSTRING;
			continue;
		}

		// -ustring [string_len]
		//
		//	Unbounded string test.
		//
		if (strcmp(arg, "-ustring") == 0) {
			ret = numarg(opts.ustring_len, arg, argc, argv,
					idx + 1, RANGE_GE0, OPTARG_OPTIONAL);
			if (ret < 0)
				return (false);
			else if (ret > 0)	// if got option argument
				++idx;
			opts.test |= TEST_USTRING;
			continue;
		}

		// -bseq [seq_len]
		//
		//	Bounded sequence test.
		//
		if (strcmp(arg, "-bseq") == 0) {
			ret = numarg(opts.bseq_len, arg, argc, argv,
					idx + 1, RANGE_GE0, OPTARG_OPTIONAL);
			if (ret < 0) {
				return (false);
			} else if (ret > 0) {
				//  Got option argument
				++idx;
			}
			opts.test |= TEST_BSEQ;
			continue;
		}

		// -useq [seq_len]
		//
		//	Unbounded sequence test.
		//
		if (strcmp(arg, "-useq") == 0) {
			ret = numarg(opts.useq_len, arg, argc, argv,
					idx + 1, RANGE_GE0, OPTARG_OPTIONAL);
			if (ret < 0) {
				return (false);
			} else if (ret > 0) {
				// Got option argument
				++idx;
			}
			opts.test |= TEST_USEQ;
			continue;
		}

		// -seq_read [seq_len]
		//
		//	Unbounded sequence test with only one read argument.
		//
		if (strcmp(arg, "-seq_read") == 0) {
			ret = numarg(opts.seq_read_len, arg, argc, argv,
					idx + 1, RANGE_GE0, OPTARG_OPTIONAL);
			if (ret < 0) {
				return (false);
			} else if (ret > 0) {
				// Got option argument
				++idx;
			}
			opts.test |= TEST_SEQ_READ;
			continue;
		}

		// -seq_write [seq_len]
		//
		//	Unbounded sequence test with only one write argument.
		//
		if (strcmp(arg, "-seq_write") == 0) {
			ret = numarg(opts.seq_write_len, arg, argc, argv,
					idx + 1, RANGE_GE0, OPTARG_OPTIONAL);
			if (ret < 0) {
				return (false);
			} else if (ret > 0) {
				// Got option argument
				++idx;
			}
			opts.test |= TEST_SEQ_WRITE;
			continue;
		}

		// -seq_rw [seq_len]
		//
		//	Unbounded sequence test with one inout argument, which
		//	means there is only one argument in each direction.
		//
		if (strcmp(arg, "-seq_rw") == 0) {
			ret = numarg(opts.seq_rw_len, arg, argc, argv,
					idx + 1, RANGE_GE0, OPTARG_OPTIONAL);
			if (ret < 0) {
				return (false);
			} else if (ret > 0) {
				// Got option argument
				++idx;
			}
			opts.test |= TEST_SEQ_RW;
			continue;
		}

		// -bsequseq [bseq_len [useq_maxlen]]
		//
		//	Bounded sequence of unbounded sequences test.
		//
		if (strcmp(arg, "-bsequseq") == 0) {
			ret = numarg(opts.bsequseq_len, arg, argc, argv,
					idx + 1, RANGE_GE0, OPTARG_OPTIONAL);
			if (ret < 0)
				return (false);
			else if (ret > 0) {	// if got option argument
				++idx;

				// Get 2nd optional argument
				ret = numarg(opts.bsequseq_max, arg,
						argc, argv, idx + 1,
						RANGE_GE0, OPTARG_OPTIONAL);
				if (ret < 0)
					return (false);
				else if (ret > 0)
					++idx;
			}
			opts.test |= TEST_BSEQUSEQ;
			continue;
		}

		// -useqbseq [useq_len [bseq_maxlen]]
		//
		//	Unbounded sequence of bounded sequences test.
		//
		if (strcmp(arg, "-useqbseq") == 0) {
			ret = numarg(opts.useqbseq_len, arg, argc, argv,
					idx + 1, RANGE_GE0, OPTARG_OPTIONAL);
			if (ret < 0)
				return (false);
			else if (ret > 0) {	// if got option argument
				++idx;

				// Get 2nd optional argument
				ret = numarg(opts.useqbseq_max, arg,
						argc, argv, idx + 1,
						RANGE_GE0, OPTARG_OPTIONAL);
				if (ret < 0)
					return (false);
				else if (ret > 0)
					++idx;
			}
			opts.test |= TEST_USEQBSEQ;
			continue;
		}

		// -bsequstring [seq_len [string_maxlen]]
		//
		//	Bounded sequence of unbounded strings test.
		//
		if (strcmp(arg, "-bsequstring") == 0) {
			ret = numarg(opts.bsequstr_len, arg,
						argc, argv, idx + 1,
						RANGE_GE0, OPTARG_OPTIONAL);
			if (ret < 0)
				return (false);
			else if (ret > 0) {	// if got option argument
				++idx;

				// Get 2nd optional argument
				ret = numarg(opts.bsequstr_max, arg,
						argc, argv, idx + 1,
						RANGE_GE0, OPTARG_OPTIONAL);
				if (ret < 0)
					return (false);
				else if (ret > 0)
					++idx;
			}
			opts.test |= TEST_BSEQUSTR;
			continue;
		}

		// -useqbstring [seq_len [string_maxlen]]
		//
		//	Unbounded sequence of bounded strings test.
		//
		if (strcmp(arg, "-useqbstring") == 0) {
			ret = numarg(opts.useqbstr_len, arg, argc, argv,
					idx + 1, RANGE_GE0, OPTARG_OPTIONAL);
			if (ret < 0)
				return (false);
			else if (ret > 0) {	// if got option argument
				++idx;

				// Get 2nd optional argument
				ret = numarg(opts.useqbstr_max, arg,
						argc, argv, idx + 1,
						RANGE_GE0, OPTARG_OPTIONAL);
				if (ret < 0)
					return (false);
				else if (ret > 0)
					++idx;
			}
			opts.test |= TEST_USEQBSTR;
			continue;
		}

		// -fstruct
		//
		//	Fixed struct test.
		//
		if (strcmp(arg, "-fstruct") == 0) {
			opts.test |= TEST_FSTRUCT;
			continue;
		}

		// -fstructseq
		//
		//	Seq Fixed struct test.
		//
		if (strcmp(arg, "-fstructseq") == 0) {
			ret = numarg(opts.fstructseq_len, arg, argc, argv,
					idx + 1, RANGE_GE0, OPTARG_OPTIONAL);
			if (ret < 0)
				return (false);
			else if (ret > 0) {	// if got option argument
				++idx;
				// Get 2nd optional argument
				ret = numarg(opts.fstructseq_max, arg,
						argc, argv, idx + 1,
						RANGE_GE0, OPTARG_OPTIONAL);
				if (ret < 0)
					return (false);
				else if (ret > 0)
					++idx;
			}
			opts.test |= TEST_FSTRUCTSEQ;
			continue;
		}
		// -nullseq
		if (strcmp(arg, "-nullseq") == 0) {
			ret = numarg(opts.nullseq_len, arg, argc, argv,
					idx + 1, RANGE_GE0, OPTARG_OPTIONAL);
			if (ret < 0)
				return (false);
			else if (ret > 0)	// if got option argument
				++idx;
			opts.test |= TEST_NULLSEQ;
			continue;
		}
		// -0lenseq
		if (strcmp(arg, "-0lenseq") == 0) {
			ret = numarg(opts.zerolenseq_len, arg, argc, argv,
					idx + 1, RANGE_GE0, OPTARG_OPTIONAL);
			if (ret < 0)
				return (false);
			else if (ret > 0)	// if got option argument
				++idx;
			opts.test |= TEST_0LENSEQ;
			continue;
		}
		// -vstruct [string_len]
		//
		//	Variable struct test.
		//
		if (strcmp(arg, "-vstruct") == 0) {
			ret = numarg(opts.vstruct_len, arg, argc, argv,
					idx + 1, RANGE_GE0, OPTARG_OPTIONAL);
			if (ret < 0)
				return (false);
			else if (ret > 0)	// if got option argument
				++idx;
			opts.test |= TEST_VSTRUCT;
			continue;
		}

		// -funion
		//
		//	Fixed union test.
		//
		if (strcmp(arg, "-funion") == 0) {
			opts.test |= TEST_FUNION;
			continue;
		}

		// -vunion [string_len]
		//
		//	Variable union test.
		//
		if (strcmp(arg, "-vunion") == 0) {
			ret = numarg(opts.vunion_len, arg, argc, argv,
					idx + 1, RANGE_GE0, OPTARG_OPTIONAL);
			if (ret < 0)
				return (false);
			else if (ret > 0)	// if got option argument
				++idx;
			opts.test |= TEST_VUNION;
			continue;
		}

		// -obj
		// -objref
		//
		//	Object reference test.
		//
		if (strcmp(arg, "-obj") == 0 || strcmp(arg, "-objref") == 0) {
			opts.test |= TEST_OBJREF;
			continue;
		}

		// -objinhsimple
		//
		//	Object single inheritance test
		//	with one level in ancestry tree.
		//
		if (strcmp(arg, "-objinhsimple") == 0) {
			opts.test |= TEST_OBJINHSIMPLE;
			continue;
		}

		// -objinhmany
		//
		//	Object single inheritance test
		//	with many levels in ancestry tree.
		//
		if (strcmp(arg, "-objinhmany") == 0) {
			opts.test |= TEST_OBJINHMANY;
			continue;
		}

		// -objinhmultiple
		//
		//	Object multiple inheritance test.
		//
		if (strcmp(arg, "-objinhmultiple") == 0) {
			opts.test |= TEST_OBJINHMULTIPLE;
			continue;
		}

		// -objseq [seq_len]
		//
		//	Sequence of object references test.
		//
		if (strcmp(arg, "-objseq") == 0) {
			ret = numarg(opts.objseq_len, arg, argc, argv,
					idx + 1, RANGE_GE0, OPTARG_OPTIONAL);
			if (ret < 0)
				return (false);
			else if (ret > 0)	// if got option argument
				++idx;
			opts.test |= TEST_OBJSEQ;
			continue;
		}

		// -revoke
		//
		//	Revoke operation test.
		//
		if (strcmp(arg, "-revoke") == 0) {
			opts.test |= TEST_REVOKE;
			continue;
		}

		// -oneway
		//
		//	One-way invocation test.
		//
		if (strcmp(arg, "-oneway") == 0) {
			opts.test |= TEST_ONEWAY;
			continue;
		}

		// -nonblocking
		//
		//	One-way, non-blocking invocation test.
		//
		if (strcmp(arg, "-nonblocking") == 0) {
			opts.test |= TEST_NONBLOCKING;
			continue;
		}

		// -arrayshort
		//
		//	Array of Short (int16_t) test.
		//
		if (strcmp(arg, "-arrayshort") == 0) {
			opts.test |= TEST_ARRAYSHORT;
			continue;
		}

		// -arrayoctet
		//
		//	Array of Octet (uint8_t) test.
		//
		if (strcmp(arg, "-arrayoctet") == 0) {
			opts.test |= TEST_ARRAYOCTET;
			continue;
		}

		// -arrayfstruct
		//
		//	Array of fixed structs test.
		//
		if (strcmp(arg, "-arrayfstruct") == 0) {
			opts.test |= TEST_ARRAYFSTRUCT;
			continue;
		}

		// -arrayfunion
		//
		//	Array of fixed unions test.
		//
		if (strcmp(arg, "-arrayfunion") == 0) {
			opts.test |= TEST_ARRAYFUNION;
			continue;
		}

		// -array
		//
		//	Run all array tests above.
		//
		if (strcmp(arg, "-array") == 0) {
			opts.test |= TEST_ARRAYSHORT;
			opts.test |= TEST_ARRAYOCTET;
			opts.test |= TEST_ARRAYFSTRUCT;
			opts.test |= TEST_ARRAYFUNION;
			continue;
		}

		// -emptyexception
		//
		//	Empty Exception test.
		//
		if (strcmp(arg, "-emptyexception") == 0) {
			opts.test |= TEST_EMPTYEXCEPTION;
			continue;
		}

		// -basicexception
		//
		//	Basic Exception test.
		//
		if (strcmp(arg, "-basicexception") == 0) {
			opts.test |= TEST_BASICEXCEPTION;
			continue;
		}

		// -complexexception
		//
		//	Complex Exception test.
		//
		if (strcmp(arg, "-complexexception") == 0) {
			opts.test |= TEST_COMPLEXEXCEPTION;
			continue;
		}

		// -stringexception
		//
		//	String Exception test.
		//
		if (strcmp(arg, "-stringexception") == 0) {
			opts.test |= TEST_STRINGEXCEPTION;
			continue;
		}

//
// We rely on the bulkio fault injection tests to cover bulkio functionality.
// This code is left for those needing to do performance work,
// and they will have to select the pxfs version of the test to build.
//
#if 0
		//
		// -bulkio_uio_in [size-in-bytes]
		//
		if (strcmp(arg, "-bulkio_uio_in") == 0) {
			ret = numarg(opts.in_uio_len, arg, argc, argv,
					idx + 1, RANGE_GT0, OPTARG_OPTIONAL);
			if (ret < 0)
				return (false);
			else if (ret > 0) // if got option argument
				++idx;
			opts.test |= TEST_BULKIO_UIO_IN;
			continue;
		}

		//
		// -bulkio_uio_inout [size-in-bytes]
		//
		if (strcmp(arg, "-bulkio_uio_inout") == 0) {
			ret = numarg(opts.inout_uio_len, arg, argc, argv,
					idx + 1, RANGE_GT0, OPTARG_OPTIONAL);
			if (ret < 0)
				return (false);
			else if (ret > 0) // if got option argument
				++idx;
			opts.test |= TEST_BULKIO_UIO_INOUT;
			continue;
		}

		//
		// -bulkio_pages_in [number-of-pages]
		//
		if (strcmp(arg, "-bulkio_pages_in") == 0) {
			ret = numarg(opts.in_pages_noof, arg, argc, argv,
					idx + 1, RANGE_GT0, OPTARG_OPTIONAL);
			if (ret < 0)
				return (false);
			else if (ret > 0) // if got option argument
				++idx;
			opts.test |= TEST_BULKIO_PAGES_IN;
			continue;
		}

		//
		// -bulkio_pages_inout [number-of-pages]
		//
		if (strcmp(arg, "-bulkio_pages_inout") == 0) {
			ret = numarg(opts.inout_pages_noof, arg, argc, argv,
					idx + 1, RANGE_GT0, OPTARG_OPTIONAL);
			if (ret < 0)
				return (false);
			else if (ret > 0) // if got option argument
				++idx;
			opts.test |= TEST_BULKIO_PAGES_INOUT;
			continue;
		}
#endif

		// -exception
		//
		//	Run all Exception tests above.
		//
		if (strcmp(arg, "-exception") == 0) {
			opts.test |= TEST_EMPTYEXCEPTION;
			opts.test |= TEST_BASICEXCEPTION;
			opts.test |= TEST_COMPLEXEXCEPTION;
			opts.test |= TEST_STRINGEXCEPTION;
			continue;
		}

		// -cred
		//
		// Cred Object Test
		//

		if (strcmp(arg, "-cred") == 0) {
			opts.test |= TEST_CRED;
			continue;
		}

		// -data_container
		//
		// Data Container Object Test
		//
		if (strcmp(arg, "-data_container") == 0) {
			ret = numarg(opts.data_len, arg, argc, argv,
					idx + 1, RANGE_GT0, OPTARG_OPTIONAL);
			if (ret < 0)
				return (false);
			else if (ret > 0) // if got option argument
				++idx;
			opts.test |= TEST_DATA_CONTAINER;
			continue;
		}

		// -idlver
		//
		// IDL Versioning Tests
		//
		if (strcmp(arg, "-idlver") == 0) {
			opts.test |= TEST_IDL_VERSIONING;
			continue;
		}

		// If user wants to invoke done() on all test servers/clients
		// (non-advertised option -- used by supporting shell scripts
		// only).
		if (strcmp(arg, "-done") == 0) {
			opts.done = true;
			continue;
		}

		// Unknown argument
		(void) fprintf(stderr, "ERROR: illegal argument: %s\n", arg);
		return (false);
	}

	// Which connection types are actually specified by user.
	opts.conn_specified = opts.conn;

	// If user didn't specify which test server(s) or test client(s)
	// then run all of them.
	if (opts.conn == 0) {
		opts.conn = CONN_ALL;	// use all servers/clients
	}

	// Which tests are actually specified by user.
	opts.test_specified = opts.test;

	// If user didn't specify which test(s) to run, then run all of them.
	if (opts.test == 0) {
		opts.test = TEST_ALL;	// run all tests
	}

	return (true);
}

//
// Get a number option argument.
// Returns:
//	-1 -- if an error occurs.
//	 0 -- the option argument is optional but it's not present.
//	 1 -- the option argument is succesfully obtained.
//
static int
numarg(
	long &var,		// area to store the option argument
	const char *optstr,
	int argc,
	char *argv[],
	int optarg_idx,		// index into argv to obtain the option arg
	Optarg_ranges range,
	Optarg_optional optional)
{
	char	*endp = "";
	long	val;

	if (optarg_idx >= argc) {
		if (optional == OPTARG_REQUIRED) {
			(void) fprintf(stderr, "ERROR: no option argument "
				"after %s\n", optstr);
			return (-1);
		}
		return (0);			// optional arg not present
	}

	// Convert option argument into number.
	val = strtol(argv[optarg_idx], &endp, 10);
	if (*endp != '\0')			// if not a number
	{
		if (optional == OPTARG_REQUIRED) {
			(void) fprintf(stderr, "ERROR: option argument "
				"after %s must be a number\n", optstr);
			return (-1);
		}
		return (0);			// optional arg not present
	}

	// Verify the range of the opt arg is correct.
	switch (range) {
	case RANGE_ALL:			// all possible (long) numbers
		break;

	case RANGE_GT0:			// > 0
		if (! (val > 0)) {
			(void) fprintf(stderr, "ERROR: option argument "
				"after %s must be a number > 0\n", optstr);
			return (-1);
		}
		break;

	case RANGE_GE0:			// >= 0
		if (! (val >= 0)) {
			(void) fprintf(stderr, "ERROR: option argument "
				"after %s must be a number >= 0\n", optstr);
			return (-1);
		}
		break;

	case RANGE_LT0:			// < 0
		if (! (val < 0)) {
			(void) fprintf(stderr, "ERROR: option argument "
				"after %s must be a number < 0\n", optstr);
			return (-1);
		}
		break;

	case RANGE_LE0:			// <= 0
		if (! (val <= 0)) {
			(void) fprintf(stderr, "ERROR: option argument "
				"after %s must be a number < 0\n", optstr);
			return (-1);
		}
		break;

	default:
		(void) fprintf(stderr, "ERROR: invalid range specified: %d\n",
			range);
		return (-1);
	}

	var = val;
	return (1);			// got the option argument
}

//
// The heart of the driver.
//
static bool
doit(Options &opts)
{
	if (opts.done) {
		// Invoke done() on all active test clients/servers, if any.
#if defined(_UNODE)
		done(unserver_name);
		done(unclient_name);
#else
		if (opts.conn & (CONN_U2U | CONN_K2U)) {
			done(userver_name);
		}
		if (opts.conn & (CONN_U2K | CONN_K2K)) {
			done(kserver_name);
		}
		if (opts.conn & (CONN_U2U | CONN_U2K)) {
			done(uclient_name);
		}
		if (opts.conn & (CONN_K2U | CONN_K2K)) {
			done(kclient_name);
		}
#endif
		return (true);
	}

	//
	// Else run tests.
	//
#if defined(_UNODE)
	if (opts.conn & CONN_UN2UN) {
		(void) printf("UNODE client <-> UNODE server:\n");
		if (! test(unclient_name, unserver_name, opts))
			return (false);
	}
#else
	if (opts.conn & CONN_U2U) {
		(void) printf("USER client <-> USER server:\n");
		if (! test(uclient_name, userver_name, opts))
			return (false);
	}

	if (opts.conn & CONN_U2K) {
		(void) printf("USER client <-> KERNEL server:\n");
		if (! test(uclient_name, kserver_name, opts))
			return (false);
	}

	if (opts.conn & CONN_K2U) {
		(void) printf("KERNEL client <-> USER server:\n");
		if (! test(kclient_name, userver_name, opts))
			return (false);
	}

	if (opts.conn & CONN_K2K) {
		(void) printf("KERNEL client <-> KERNEL server:\n");
		if (! test(kclient_name, kserver_name, opts))
			return (false);
	}
#endif

	return (true);
}

//
// Perform test(s) for a given client
//
static bool
test(const char *client_name, const char *server_name, Options &opts)
{
	typetest::server_var	serverp;
	typetest::client_var	clientp;
	CORBA::Object_var	objp;
	int64_t			time;
	bool			uclient = false;
	bool			kclient = false;
	bool			unclient = false;
	bool			userver = false;
	bool			kserver = false;
	bool			unserver = false;

	// Set some flags which can be used to check whether a particular
	// client/server is running
	if (strcmp(client_name, uclient_name) == 0) {
		uclient = true;				// USER client
	} else if (strcmp(client_name, kclient_name) == 0) {
		kclient = true;				// KERNEL client
	} else if (strcmp(client_name, unclient_name) == 0) {
		unclient = true;			// UNODE client
	}

	if (strcmp(server_name, userver_name) == 0) {
		userver = true;				// USER server
	} else if (strcmp(server_name, kserver_name) == 0) {
		kserver = true;				// KERNEL server
	} else if (strcmp(server_name, unserver_name) == 0) {
		unserver = true;			// UNODE server
	}

	// Get test client
	objp = get_obj(client_name);
	if (CORBA::is_nil(objp))
		return (false);
	clientp = typetest::client::_narrow(objp);
	if (!clientp) {
		(void) printf("\t Error:  clientp == null\n");
		return (false);
	}

	// Get test server
	objp = get_obj(server_name);
	if (CORBA::is_nil(objp))
		return (false);
	serverp = typetest::server::_narrow(objp);

	if (!serverp) {
		(void) printf("\t Error:  serverp == null\n");
		return (false);
	}
	//
	// Tell test client to run test with test server.
	//

	if (opts.test & TEST_NOARG) {
		Environment	e;

		(void) printf("\tNo-argument test:\n");
		time = clientp->test_noarg(serverp, (int)opts.iter,
		    opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0)
			print_stats(time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_BOOLEAN) {
		Environment	e;

		(void) printf("\tBoolean test:\n");
		time = clientp->test_boolean(serverp, (int)opts.iter,
		    opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0)
			print_stats(time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_CHAR) {
		Environment	e;

		(void) printf("\tChar test:\n");
		time = clientp->test_char(serverp, (int)opts.iter,
		    opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0)
			print_stats(time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_OCTET) {
		Environment	e;

		(void) printf("\tOctet test:\n");
		time = clientp->test_octet(serverp, (int)opts.iter,
		    opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0)
			print_stats(time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_SHORT) {
		Environment	e;

		(void) printf("\tShort test:\n");
		time = clientp->test_short(serverp, (int)opts.iter,
		    opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0)
			print_stats(time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_USHORT) {
		Environment	e;

		(void) printf("\tUShort test:\n");
		time = clientp->test_ushort(serverp, (int)opts.iter,
		    opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0)
			print_stats(time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_LONG) {
		Environment	e;

		(void) printf("\tLong test:\n");
		time = clientp->test_long(serverp, (int)opts.iter,
		    opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0)
			print_stats(time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_ULONG) {
		Environment	e;

		(void) printf("\tULong test:\n");
		time = clientp->test_ulong(serverp, (int)opts.iter,
		    opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0)
			print_stats(time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_LONGLONG) {
		Environment	e;

		(void) printf("\tLongLong test:\n");
		time = clientp->test_longlong(serverp, (int)opts.iter,
		    opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0)
			print_stats(time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_ULONGLONG) {
		Environment	e;

		(void) printf("\tULongLong test:\n");
		time = clientp->test_ulonglong(serverp, (int)opts.iter,
		    opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0)
			print_stats(time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_BSTRING) {
		Environment	e;

		(void) printf("\tBounded string test: length %d\n",
		    opts.bstring_len);
		time = clientp->test_bstring(serverp, (int)opts.iter,
		    (uint32_t)opts.bstring_len, opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0)
			print_stats(time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_USTRING) {
		Environment	e;

		(void) printf("\tUnbounded string test: length %d\n",
		    opts.ustring_len);
		time = clientp->test_ustring(serverp, (int)opts.iter,
		    (uint32_t)opts.ustring_len, opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0)
			print_stats(time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_BSEQ) {
		Environment	e;

		(void) printf("\tBounded sequence test: length %d\n",
		    opts.bseq_len);
		time = clientp->test_bseq(serverp, (int)opts.iter,
		    (uint32_t)opts.bseq_len, opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0) {
			print_stats(time, (int32_t)opts.iter);
		}
	}

	if (opts.test & TEST_USEQ) {
		Environment	e;

		(void) printf("\tUnbounded sequence test: length %d\n",
		    opts.useq_len);
		time = clientp->test_useq(serverp, (int)opts.iter,
		    (uint32_t)opts.useq_len, opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0) {
			print_stats(time, (int32_t)opts.iter);
		}
	}

	if (opts.test & TEST_SEQ_READ) {
		Environment	e;

		(void) printf("\tUnbounded sequence read test: length %d\n",
		    opts.seq_read_len);
		time = clientp->test_seq_read(serverp, (int)opts.iter,
		    (uint32_t)opts.seq_read_len, opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0) {
			print_stats(time, (int32_t)opts.iter);
		}
	}

	if (opts.test & TEST_SEQ_WRITE) {
		Environment	e;

		(void) printf("\tUnbounded sequence write test: length %d\n",
		    opts.seq_write_len);
		time = clientp->test_seq_write(serverp, (int)opts.iter,
		    (uint32_t)opts.seq_write_len, opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0) {
			print_stats(time, (int32_t)opts.iter);
		}
	}

	if (opts.test & TEST_SEQ_RW) {
		Environment	e;

		(void) printf("\tUnbounded sequence read/write test: "
			"length %d\n", opts.seq_rw_len);
		time = clientp->test_seq_rw(serverp, (int)opts.iter,
		    (uint32_t)opts.seq_rw_len, opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0) {
			print_stats(time, (int32_t)opts.iter);
		}
	}

	if (opts.test & TEST_BSEQUSEQ) {
		Environment	e;

		(void) printf("\tBounded sequence of unbounded sequences test:"
		    " length %d\n", opts.bsequseq_len);
		time = clientp->test_bseq_useq(serverp, (int)opts.iter,
		    (uint32_t)opts.bsequseq_len, (uint32_t)opts.bsequseq_max,
		    opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0)
			print_stats(time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_USEQBSEQ) {
		Environment	e;

		(void) printf("\tUnbounded sequence of bounded sequences test:"
		    " length %d\n", opts.useqbseq_len);
		time = clientp->test_useq_bseq(serverp, (int)opts.iter,
		    (uint32_t)opts.useqbseq_len, (uint32_t)opts.useqbseq_max,
		    opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0)
			print_stats(time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_BSEQUSTR) {
		Environment	e;

		(void) printf("\tBounded sequence of unbounded strings test:"
		    " length %d\n", opts.bsequstr_len);
		time = clientp->test_bseq_ustring(serverp, (int)opts.iter,
		    (uint32_t)opts.bsequstr_len, (uint32_t)opts.bsequstr_max,
		    opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0)
			print_stats(time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_USEQBSTR) {
		Environment	e;

		(void) printf("\tUnbounded sequence of bounded strings test:"
		    " length %d\n", opts.useqbstr_len);
		time = clientp->test_useq_bstring(serverp, (int)opts.iter,
		    (uint32_t)opts.useqbstr_len, (uint32_t)opts.useqbstr_max,
		    opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0)
			print_stats(time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_FSTRUCT) {
		Environment	e;

		(void) printf("\tFixed structure test:\n");

		time = clientp->test_fstruct(serverp, (int)opts.iter,
		    opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0)
			print_stats(time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_VSTRUCT) {
		Environment	e;

		printf("\tVariable structure test:\n");
		time = clientp->test_vstruct(serverp, (int)opts.iter,
		    (uint32_t)opts.vstruct_len, opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0)
			print_stats(time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_FSTRUCTSEQ) {
		Environment	e;

		(void) printf("\tSequence of Fixed structure test:\n");
		time = clientp->test_fstructseq(serverp, (int)opts.iter,
		    (uint32_t)opts.fstructseq_len, opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0)
			print_stats(time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_NULLSEQ) {
		Environment	e;

		(void) printf("\tUninitialized/null out sequence:\n");
		time = clientp->test_nullseq(serverp, (int)opts.iter,
		    (uint32_t)opts.nullseq_len, opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0)
			print_stats(time, (int32_t)opts.iter);
	}
	if (opts.test & TEST_0LENSEQ) {
		Environment	e;
		(void) printf("\tOut Sequence of length 0:\n");
		time = clientp->test_0lenseq(serverp, (int)opts.iter,
		    (uint32_t)opts.zerolenseq_len, opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0)
			print_stats(time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_FUNION) {
		Environment	e;

		(void) printf("\tFixed union test:\n");
		time = clientp->test_funion(serverp, (int)opts.iter,
		    opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0)
			print_stats(time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_VUNION) {
		Environment	e;

		(void) printf("\tVariable union test:\n");
		time = clientp->test_vunion(serverp, (int)opts.iter,
		    (uint32_t)opts.vunion_len, opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0)
			print_stats(time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_OBJREF) {
		Environment	e;

		(void) printf("\tObject reference test:\n");
		time = clientp->test_obj(serverp, (int)opts.iter,
		    opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0)
			print_stats(time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_OBJINHSIMPLE) {
		Environment	e;

		(void) printf("\tInherited object test:\n");
		time = clientp->test_objinhsimple(serverp, (int)opts.iter,
		    opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0)
			print_stats(time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_OBJINHMANY) {
		Environment	e;

		(void) printf("\tInherited object (many levels) test:\n");
		time = clientp->test_objinhmany(serverp, (int)opts.iter,
		    opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0) {
			print_stats(time, opts.iter);
		}
	}

	if (opts.test & TEST_OBJINHMULTIPLE) {
		Environment	e;

		(void) printf("\tInherited object (multiple inheritance) "
			"test:\n");
		time = clientp->test_objinhmultiple(serverp, (int)opts.iter,
		    opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0) {
			print_stats(time, (int32_t)opts.iter);
		}
	}

	if (opts.test & TEST_OBJSEQ) {
		Environment	e;

		(void) printf("\tObject reference sequence test:\n");
		time = clientp->test_objseq(serverp, (int)opts.iter,
		    (uint32_t)opts.objseq_len, opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0)
			print_stats(time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_REVOKE) {
		Environment	e;

		(void) printf("\tRevoke operation test:\n");
		time = clientp->test_revoke(serverp, 1, opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0)
			print_stats(time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_ONEWAY) {
		Environment	e;
		bool		test_specified = false;
		bool		conn_specified = false;

		(void) printf("\tOneway test:\n");

		// If -oneway option is specified.
		if (opts.test_specified & TEST_ONEWAY) {
			test_specified = true;
		}

		// If -u2u or -u2k is specified.
		if (opts.conn_specified & (CONN_U2U | CONN_U2K)) {
			conn_specified = true;
		}

		// XXX	Skip oneway test for user-land clients unless specified
		//	on command line.  Oneways are not supported for user
		//	clients yet.
		if (uclient && ! (test_specified && conn_specified)) {
			(void) printf("\t\t***ONEWAYS NOT SUPPORTED FOR USER "
			    "CLIENTS\n");
			(void) printf("\t\t***SPECIFY -oneway AND -u2u AND/OR "
			    "-u2k TO RUN THE TEST\n");
		} else if (opts.noverify && ! (kclient && kserver)) {
			(void) printf("\t\t***ONEWAYS ONLY SUPPORTED FOR "
			    "KERNEL->KERNEL FOR -noverify OPTION\n");
		} else {
			serverp->store_client_ref(clientp, e);
			if (! env_ok(e, prefix)) {
				(void) fprintf(stderr, "server invocation to "
				    "store client ref failed\n");
				return (false);
			}

			time = clientp->test_oneway(serverp,
			    (int)opts.iter, opts.noverify, e);
			if (env_ok(e, prefix) && time >= 0) {
				if (opts.noverify)
					(void) printf("\tOneway round trip "
						"time:\n");
				else
					(void) printf("\tOneway invocation "
						"time:\n");
				print_stats(time, (int32_t)opts.iter);
			} else if (e.exception()) {
				e.exception()->print_exception("Exception: ");
				e.clear();
			}

			serverp->release_client_ref(e);
			if (! env_ok(e, prefix)) {
				(void) fprintf(stderr, "server invocation to "
				    "release client ref failed\n");
			}
		}
	}

	if (opts.test & TEST_NONBLOCKING) {
		Environment	e;
		bool		test_specified = false;
		bool		conn_specified = false;

		(void) printf("\tNonblocking test:\n");

		// If -nonblocking option is specified.
		if (opts.test_specified & TEST_NONBLOCKING) {
			test_specified = true;
		}

		// If -u2u, -u2k or -k2u is specified.
		if (opts.conn_specified & (CONN_U2U | CONN_U2K | CONN_K2U)) {
			conn_specified = true;
		}

		// XXX	Skip nonblocking tests unless the -nonblocking option
		//	is specified.  Nonblocking semantics have changed
		//	from reliable to non-reliable.  Need to figure out
		//	how to handle this non-reliability in the test.
		//
		// XXX	Skip nonblocking tests for all client/server
		//	combinations except KERNEL<->KERNEL case, unless
		//	the -nonblocking option is specified AND the
		//	-u2k, -k2u and/or -u2u options are specified.
		if (! test_specified) {
			(void) printf("\t\t***NONBLOCKING CURRENTLY SKIPPED IN "
				"THE DEFAULT CASE\n");
			(void) printf("\t\t***SPECIFY -nonblocking TO RUN "
				"THE TEST\n");
		} else if ((uclient || userver) && ! conn_specified) {
			(void) printf("\t\t***NONBLOCKING NOT SUPPORTED FOR "
				"USER CLIENTS/SERVERS\n");
			(void) printf("\t\t***SPECIFY -nonblocking AND -u2u, "
				"-u2k, AND/OR -k2u\n");
			(void) printf("\t\t***TO RUN THE TEST\n");
		} else {
			time = clientp->test_nonblocking(serverp,
			    (int)opts.iter, opts.noverify, e);
			if (env_ok(e, prefix) && time >= 0)
				print_stats(time, (int32_t)opts.iter);
		}
	}

	if (opts.test & TEST_ARRAYSHORT) {
		Environment	e;

		(void) printf("\tArray of Short (int16_t) test:\n");

		time = clientp->test_arrayshort(serverp, (int)opts.iter,
		    opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0)
			print_stats(time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_ARRAYOCTET) {
		Environment	e;

		(void) printf("\tArray of Octet (uint8_t) (2D) test:\n");

		time = clientp->test_arrayoctet(serverp, (int)opts.iter,
		    opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0)
			print_stats(time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_ARRAYFSTRUCT) {
		Environment	e;

		(void) printf("\tArray of Fstruct test:\n");

		time = clientp->test_arrayfstruct(serverp, (int)opts.iter,
		    opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0)
			print_stats(time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_ARRAYFUNION) {
		Environment	e;

		(void) printf("\tArray of Funion test:\n");

		time = clientp->test_arrayfunion(serverp, (int)opts.iter,
		    opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0)
			print_stats(time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_EMPTYEXCEPTION) {
		Environment	e;

		(void) printf("\tEmpty exception test:\n");

		time = clientp->test_emptyexception(serverp, (int)opts.iter,
		    opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0)
			print_stats(time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_BASICEXCEPTION) {
		Environment	e;

		(void) printf("\tBasic exception test:\n");

		time = clientp->test_basicexception(serverp, (int)opts.iter,
		    opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0)
			print_stats(time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_COMPLEXEXCEPTION) {
		Environment	e;

		(void) printf("\tComplex exception test:\n");

		time = clientp->test_complexexception(serverp, (int)opts.iter,
		    opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0)
			print_stats(time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_STRINGEXCEPTION) {
		Environment	e;

		(void) printf("\tString exception test:\n");

		time = clientp->test_stringexception(serverp, (int)opts.iter,
		    opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0)
			print_stats(time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_BULKIO_UIO_IN) {
		(void) printf("\tBulkio raw I/O write test: number bytes %d\n",
		    opts.in_uio_len);

		if (kclient && kserver) {
			Environment	e;

#if !defined(_UNODE)
			time = clientp->test_bulkio_uio_in(serverp,
			    (int)opts.iter, (uint32_t)opts.in_uio_len,
			    opts.noverify, e);
#endif // _UNODE
			if (env_ok(e, prefix) && time >= 0)
				print_stats(time, (int32_t)opts.iter);
		} else {
			(void) printf("\t\t***BULKIO TESTS ONLY SUPPORTED FOR "
			    "KERNEL --> KERNEL\n");
		}
	}

	if (opts.test & TEST_BULKIO_UIO_INOUT) {
		(void) printf("\tBulkio raw I/O read test: number bytes %d\n",
		    opts.inout_uio_len);

		if (kclient && kserver) {
			Environment	e;

#if !defined(_UNODE)
			time = clientp->test_bulkio_uio_inout(serverp,
			    (int)opts.iter, (uint32_t)opts.inout_uio_len,
			    opts.noverify, e);
#endif // _UNODE
			if (env_ok(e, prefix) && time >= 0)
				print_stats(time, (int32_t)opts.iter);
		} else {
			(void) printf("\t\t***BULKIO TESTS ONLY SUPPORTED FOR "
			    "KERNEL --> KERNEL\n");
		}
	}

	if (opts.test & TEST_BULKIO_PAGES_IN) {
		(void) printf("\tBulkio pages write test: number pages %d\n",
		    opts.in_pages_noof);

		if (kclient && kserver) {
			Environment	e;

#if !defined(_UNODE)
			time = clientp->test_bulkio_pages_in(serverp,
			    (int)opts.iter, (uint32_t)opts.in_pages_noof,
			    opts.noverify, e);
#endif // _UNODE
			if (env_ok(e, prefix) && time >= 0)
				print_stats(time, (int32_t)opts.iter);
		} else {
			(void) printf("\t\t***BULKIO TESTS ONLY SUPPORTED FOR "
			    "KERNEL --> KERNEL\n");
		}
	}

	if (opts.test & TEST_BULKIO_PAGES_INOUT) {
		(void) printf("\tBulkio pages read test: number pages %d\n",
		    opts.inout_pages_noof);

		if (kclient && kserver) {
			Environment	e;

#if !defined(_UNODE)
			time = clientp->test_bulkio_pages_inout(serverp,
			    (int)opts.iter, (uint32_t)opts.inout_pages_noof,
			    opts.noverify, e);
#endif // _UNODE
			if (env_ok(e, prefix) && time >= 0)
				print_stats(time, (int32_t)opts.iter);
		} else {
			(void) printf("\t\t***BULKIO TESTS ONLY SUPPORTED FOR "
			    "KERNEL --> KERNEL\n");
		}
	}

	if (opts.test & TEST_CRED) {
		Environment	e;

		(void) printf("\tCred Object test:\n");

		if (kclient && kserver) {
			time = clientp->test_cred(serverp, (int)opts.iter,
			    opts.noverify, e);
			if (env_ok(e, prefix) && time >= 0)
				print_stats(time, (int32_t)opts.iter);
		} else {
			(void) printf("\t\t***Cred test only supported for "
			    "kernel --> kernel\n");
		}
	}

	if (opts.test & TEST_DATA_CONTAINER) {
		Environment	e;

		(void) printf("\tData Container Object test:\n");

		time = clientp->test_data_container(serverp, (int)opts.iter,
		    (uint32_t)opts.data_len, opts.noverify, e);
		if (env_ok(e, prefix) && time >= 0)
			print_stats(time, (int32_t)opts.iter);
	}

	if (opts.test & TEST_IDL_VERSIONING) {
		(void) printf("============================\n");
		(void) printf("Running IDL Versioning Tests\n");
		(void) printf("============================\n");
#if !defined(_UNODE)
		(void) printf("TEST MODE: driver <-> user-land ORB servers\n");
		if (!do_versioning_tests1(USERLAND)) {
			return (false);
		}
#ifdef ORBTEST_V1
		if (server_version == 1) {
			if (!do_versioning_tests2(USERLAND)) {
				return (false);
			}
		}
		if (server_version == 1) {
			if (!do_versioning_tests3(USERLAND)) {
				return (false);
			}
		}
#endif // ORBTEST_V1
#endif // _UNODE
		(void) printf("\nTEST MODE: driver <-> "
		    "kernel-land ORB servers\n");
		if (!do_versioning_tests1(KERNELLAND)) {
			return (false);
		}
#ifdef ORBTEST_V1
		if (server_version == 1) {
			if (!do_versioning_tests2(KERNELLAND)) {
				return (false);
			}
		}
		if (server_version == 1) {
			if (!do_versioning_tests3(KERNELLAND)) {
				return (false);
			}
		}
#endif // ORBTEST_V1
	}

	return (true);
}


#ifdef ORBTEST_V1
static bool
do_versioning_tests1(unsigned short mode)
{
	Environment	e;
	typetest::minverObjA_var server_v;
	naming::naming_context_var root_ns;
	CORBA::Object_var objp;

	if (mode == USERLAND) {
		(void) printf("Looking up user-land minverObjA server in "
		    "root nameserver\t");
		root_ns = ns::root_nameserver();
		objp = root_ns->resolve(Auserver_name, e);
		if (e.exception()) {
			(void) printf("FAILED\n");
			(void) printf("\tORB nameserver couldn't resolve "
			    "\"%s\"\n", Auserver_name);
			e.exception()->print_exception("\t");
			return (false);
		} else if (CORBA::is_nil(objp)) {
			(void) printf("FAILED\n");
			(void) printf("Nameserver returned nil reference\n");
			return (false);
		} else {
			(void) printf("PASS\n");
		}
	} else if (mode == KERNELLAND) {
		(void) printf("Looking up kernel-land minverObjA server "
		    "in root nameserver\t");
		root_ns = ns::root_nameserver();
		objp = root_ns->resolve(Akserver_name, e);
		if (e.exception()) {
			(void) printf("FAILED\n");
			(void) printf("\tORB nameserver couldn't resolve "
			    "\"%s\"\n", Akserver_name);
			e.exception()->print_exception("\t");
			return (false);
		} else if (CORBA::is_nil(objp)) {
			(void) printf("FAILED\n");
			(void) printf("Nameserver returned nil reference\n");
			return (false);
		} else {
			(void) printf("PASS\n");
		}
	}

	//
	// Assertion: Version 1 client should be able to narrow object
	// 		to minverObjA
	//
	(void) printf("Narrowing object ref to type minverObjA\t\t\t\t");
	server_v = typetest::minverObjA::_narrow(objp);
	if (CORBA::is_nil(server_v)) {
		(void) printf("FAILED\n");
		return (false);
	} else
		(void) printf("PASS\n");

	//
	// Use version interface on minverObjA to get actual version of this
	// object, for use in the following tests.
	//
	(void) printf("Exercising _version method on minverObjA ref\t\t\t");
	int version = -1;
	server_version = server_v->version(e);
	if (e.exception()) {
		(void) printf("FAILED\n");
		e.exception()->print_exception("\t");
		return (false);
	}

	//
	// Assertion: If client is V1 and server is V0 then _version should
	// 		return 0
	//
	if (server_version == 0) {
		if ((version = typetest::minverObjA::_version(server_v)) != 0) {
			(void) printf("FAILED\n");
			(void) printf("\tminverObjA::_version returned %d, "
			    "was expecting 0\n", version);
			return (false);
		} else {
			(void) printf("PASS\n");
		}
	//
	// Assertion: If client is V1 and server is V1 then _version should
	// 		return 1.
	//
	} else if (server_version == 1) {
		if ((version = typetest::minverObjA::_version(server_v)) != 1) {
			(void) printf("FAILED\n");
			(void) printf("\tminverObjA::_version returned %d, "
			    "was expecting 1\n", version);
			return (false);
		} else {
			(void) printf("PASS\n");
		}
	}

	(void) printf("Invoking Version 1 interface on Version %d server\t\t",
	    server_version);
	//
	// Assertion: If V1 client tries to invoke V1 interface on V0 server,
	// 		then the UNSUPPORTED_VERSION exception should
	// 		get thrown.
	//
	if (server_version == 0) {
		server_v->setid(4321, e);

		if (e.exception()) {
			(void) printf("PASS\n");
			e.exception()->print_exception("\t");
			e.clear();
		} else {
			(void) printf("FAILED\n");
			return (false);
		}
	//
	// Assertion: If V1 client tries to invoke V1 interface on V1 server,
	// 		then the invocation should succceed and no exceptions
	// 		should be thrown.
	//
	} else if (server_version == 1) {
		server_v->setid(4321, e);

		if (e.exception()) {
			(void) printf("FAILED\n");
			e.exception()->print_exception("\t");
			return (false);
		} else {
			(void) printf("PASS\n");
		}
	}

	//
	// Test getting a different proxy for this interface.
	//
	if (server_version == 1) {
		(void) printf("Test version 0 proxy object minverObjA\t\t\t\t");

		// Get a proxy to the older interface.
		CORBA::type_info_t *tp =
		    typetest::minverObjA::_get_type_info(0);
		objp = CORBA::create_proxy_obj(tp, server_v->_handler());
		if (CORBA::is_nil(objp)) {
			(void) printf("FAILED_1\n");
			return (false);
		}
		typetest::minverObjA_var p0_v =
		    typetest::minverObjA::_narrow(objp);
		if (CORBA::is_nil(p0_v)) {
			(void) printf("FAILED_2\n");
			return (false);
		}

		// Call version 1 method.
		p0_v->setid(4321, e);

		if (e.exception()) {
			(void) printf("PASS\n");
			e.exception()->print_exception("\t");
			e.clear();
		} else {
			(void) printf("FAILED\n");
			return (false);
		}

		// Test generic method.
		(void) printf("Test _generic_method() minverObjA\t\t\t\t");
		CORBA::octet_seq_t data(8, 8, (uint8_t *)"driverX", false);
		CORBA::object_seq_t objs(1, 1);
		objs[0] = CORBA::Object::_duplicate(server_v);
		p0_v->_generic_method(data, objs, e);

		if (e.exception()) {
			(void) printf("FAILED_1\n");
			e.exception()->print_exception("\t");
			return (false);
		} else if (data.length() != 9 ||
		    os::strcmp((const char *)data.buffer(), "server_A") != 0) {
			(void) printf("FAILED_2\n");
			(void) printf("\tlen %d %s\n", data.length(),
			    data.buffer());
			return (false);
		} else {
			(void) printf("PASS\n");
		}

		// Call generic method again to test version exception.
		(void) printf("Test version exception minverObjA\t\t\t\t");
		p0_v->_generic_method(data, objs, e);

		if (e.exception()) {
			(void) printf("PASS\n");
			e.exception()->print_exception("\t");
			e.clear();
		} else {
			(void) printf("FAILED\n");
			return (false);
		}
	}

	return (true);
}
#else
static bool
do_versioning_tests1(unsigned short mode)
{
	Environment	e;
	typetest::minverObjA_var server_v;
	naming::naming_context_var root_ns;
	CORBA::Object_var objp;

	if (mode == USERLAND) {
		(void) printf("Looking up user-land minverObjA server in "
		    "root nameserver\t");
		root_ns = ns::root_nameserver();
		objp = root_ns->resolve(Auserver_name, e);
		if (e.exception()) {
			(void) printf("FAILED\n");
			(void) printf("\tORB nameserver couldn't resolve "
			    "\"%s\"\n", Auserver_name);
			e.exception()->print_exception("\t");
			return (false);
		} else if (CORBA::is_nil(objp)) {
			(void) printf("FAILED\n");
			(void) printf("Nameserver returned nil reference\n");
			return (false);
		} else {
			(void) printf("PASS\n");
		}
	} else if (mode == KERNELLAND) {
		(void) printf("Looking up kernel-land minverObjA server in "
		    "root nameserver\t");
		root_ns = ns::root_nameserver();
		objp = root_ns->resolve(Akserver_name, e);
		if (e.exception()) {
			(void) printf("FAILED\n");
			(void) printf("\tORB nameserver couldn't resolve "
			    "\"%s\"\n", Akserver_name);
			e.exception()->print_exception("\t");
			return (false);
		} else if (CORBA::is_nil(objp)) {
			(void) printf("FAILED\n");
			(void) printf("Nameserver returned nil reference\n");
			return (false);
		} else {
			(void) printf("PASS\n");
		}
	}

	//
	// Assertion: Version 0 client should be able to narrow object
	// 		to minverObjA
	//
	(void) printf("Narrowing object ref to type minverObjA\t\t\t\t");
	server_v = typetest::minverObjA::_narrow(objp);
	if (CORBA::is_nil(server_v)) {
		(void) printf("FAILED\n");
		return (false);
	} else
		(void) printf("PASS\n");

	//
	// Assertion: If client is V0 then _version should return 0 regardless
	// 		of whether the server is V0 or V1.
	//
	(void) printf("Exercising _version method on minverObjA ref\t\t\t");
	int version = -1;
	if ((version = typetest::minverObjA::_version(server_v)) != 0) {
		(void) printf("FAILED\n");
		(void) printf("\tminverObjA::_version returned %d, "
		    "was expecting 0\n", version);
		return (false);
	} else {
		(void) printf("PASS\n");
	}

	return (true);
}
#endif

#ifdef ORBTEST_V1
static bool
do_versioning_tests2(unsigned short mode)
{
	Environment	e;
	typetest::minverObjC_var serverC_v;
	typetest::minverObjA_var serverA_v;
	naming::naming_context_var root_ns;
	CORBA::Object_var objp;


	if (mode == USERLAND) {
		(void) printf("Looking up user-land minverObjC server in "
		    "root nameserver\t");
		root_ns = ns::root_nameserver();
		objp = root_ns->resolve(Cuserver_name, e);
		if (e.exception()) {
			(void) printf("FAILED\n");
			(void) printf("\tORB nameserver couldn't resolve "
			    "\"%s\"\n", Cuserver_name);
			e.exception()->print_exception("\t");
			return (false);
		} else if (CORBA::is_nil(objp)) {
			(void) printf("FAILED\n");
			(void) printf("Nameserver returned nil reference\n");
			return (false);
		} else {
			(void) printf("PASS\n");
		}
	} else if (mode == KERNELLAND) {
		(void) printf("Looking up kernel-land minverObjC server "
		    "in root nameserver\t");
		root_ns = ns::root_nameserver();
		objp = root_ns->resolve(Ckserver_name, e);
		if (e.exception()) {
			(void) printf("FAILED\n");
			(void) printf("\tORB nameserver couldn't resolve "
			    "\"%s\"\n", Ckserver_name);
			e.exception()->print_exception("\t");
			return (false);
		} else if (CORBA::is_nil(objp)) {
			(void) printf("FAILED\n");
			(void) printf("Nameserver returned nil reference\n");
			return (false);
		} else {
			(void) printf("PASS\n");
		}
	}

	//
	// Assertion: Version 1 client should be able to narrow object
	// 		to minverObjC
	//
	(void) printf("Narrowing object ref to type minverObjB\t\t\t\t");
	serverC_v = typetest::minverObjC::_narrow(objp);
	if (CORBA::is_nil(serverC_v)) {
		(void) printf("FAILED\n");
		return (false);
	} else {
		(void) printf("PASS\n");
	}

	//
	// Assertion: If client is V1 and this server implements V0 of
	// 		minverObjC then _version should return 0
	//
	(void) printf("Exercising _version method on minverObjC ref\t\t\t");
	int version = -1;
	if ((version = typetest::minverObjC::_version(serverC_v)) != 0) {
		(void) printf("FAILED\n");
		(void) printf("\tminverObjC::_version returned %d, "
		    "was expecting 0\n", version);
		return (false);
	} else {
		(void) printf("PASS\n");
	}

	//
	// Assertion: Since the minverObjC interface inherits from the v1
	// 		minverObjA interface, then we should be able to narrow
	// 		our minverObjC to a minverObjA reference
	//
	(void) printf("Narrowing minverObjC object ref to type minverObjA\t\t");
	serverA_v = typetest::minverObjA::_narrow(serverC_v);
	if (CORBA::is_nil(serverA_v)) {
		(void) printf("FAILED\n");
		return (false);
	} else {
		(void) printf("PASS\n");
	}

	//
	// Assertion: If we run _version on our newly acquired minverObjA ref
	// 		it should return 1
	//
	(void) printf("Exercising _version method on minverObjA ref\t\t\t");
	version = -1;
	if ((version = typetest::minverObjA::_version(serverA_v)) != 1) {
		(void) printf("FAILED\n");
		(void) printf("\tminverObjA::_version returned %d, "
		    "was expecting 1\n", version);
		return (false);
	} else {
		(void) printf("PASS\n");
	}

	return (true);
}

static bool
do_versioning_tests3(unsigned short mode)
{
	Environment	e;
	typetest::combinedObjD_var serverD_p;
	typetest::minverObjA_var serverA_v;
	typetest::majverObjB_var serverB_v;
	naming::naming_context_var root_ns;
	CORBA::Object_var objp;

	if (mode == USERLAND) {
		(void) printf("Looking up user-land combinedObjD server in "
		    "root nameserver\t");
		root_ns = ns::root_nameserver();
		objp = root_ns->resolve(Duserver_name, e);
		if (e.exception()) {
			(void) printf("FAILED\n");
			(void) printf("\tORB nameserver couldn't resolve "
			    "\"%s\"\n", Duserver_name);
			e.exception()->print_exception("\t");
			return (false);
		} else if (CORBA::is_nil(objp)) {
			(void) printf("FAILED\n");
			(void) printf("Nameserver returned nil reference\n");
			return (false);
		} else {
			(void) printf("PASS\n");
		}
	} else if (mode == KERNELLAND) {
		(void) printf("Looking up kernel-land combinedObjD server in "
		    "root nameserver\t");
		root_ns = ns::root_nameserver();
		objp = root_ns->resolve(Dkserver_name, e);
		if (e.exception()) {
			(void) printf("FAILED\n");
			(void) printf("\tORB nameserver couldn't resolve "
			    "\"%s\"\n", Dkserver_name);
			e.exception()->print_exception("\t");
			return (false);
		} else if (CORBA::is_nil(objp)) {
			(void) printf("FAILED\n");
			(void) printf("Nameserver returned nil reference\n");
			return (false);
		} else {
			(void) printf("PASS\n");
		}
	}

	//
	// Assertion: Version 1 client should NOT be able to narrow object to
	// 		server_only type combinedObjD
	//
	(void) printf("Narrowing object ref to server_only type "
	    "combinedObjD\t\t");
	serverD_p = typetest::combinedObjD::_narrow(objp);
	if (CORBA::is_nil(serverD_p->_this_obj())) {
		(void) printf("PASS\n");
	} else {
		(void) printf("FAILED\n");
		return (false);
	}

	//
	// Assertion: Version 1 client should be able to narrow object to
	// 		type minverObjA
	//
	(void) printf("Narrowing object ref to type minverObjA\t\t\t\t");
	serverA_v = typetest::minverObjA::_narrow(objp);
	if (CORBA::is_nil(serverA_v)) {
		(void) printf("FAILED\n");
		return (false);
	} else {
		(void) printf("PASS\n");
	}
	//
	// Assertion: Version 1 client should be able to narrow object to
	// 		type majverObjB
	//
	(void) printf("Narrowing object ref to type majverObjB\t\t\t\t");
	serverB_v = typetest::majverObjB::_narrow(objp);
	if (CORBA::is_nil(serverB_v)) {
		(void) printf("FAILED\n");
		return (false);
	} else {
		(void) printf("PASS\n");
	}
	return (true);
}
#endif

//
// Invoke the done() method on the impl object, if any, with the specified
// string name.
//
static void
done(const char *name)
{
	CORBA::Object_var	objp;

	objp = get_obj(name, true, false);	// error messages suppressed
						// dont wait
	if (! CORBA::is_nil(objp)) {
		Environment		e;
		typetest::common_var	commonp;

		commonp = typetest::common::_narrow(objp);
		commonp->done(e);
		if (e.exception()) {
			fprintf(stderr, "ERROR: can't invoke done on \"%s\"\n",
			    name);
			e.exception()->print_exception("\t");
		}
	}
}

//
// Print statistics.
//
static void
print_stats(int64_t total_time, int32_t num_iter)
{
	total_time /= 1000;				// usec.

	printf("\t\tTotal time         : %lld usec.\n", total_time);
	printf("\t\tNo. of iterations  : %d\n", num_iter);
	printf("\t\tAvg. time/iteration: %.2f usec.\n",
	    (double)total_time / num_iter);
}
