/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)repl_sample_impl_v1.cc	1.8	08/05/20 SMI"

#include	<sys/modctl.h>
#include 	<sys/os.h>
#include	<sys/vm_util.h>
#include	<h/naming.h>
#include	<nslib/ns.h>
#include	<vm/vm_lca_impl.h>
#include	<orbtest/vm_upgd_cb/repl_sample_impl_v1.h>
#include	<orb/fault/fault_injection.h>

//
// We don't normally have that big node ids. So cast it to int16_t
// so lint does not complain.
//
int16_t			nodeid = (int16_t)orb_conf::node_number();

#if defined(_FAULT_INJECTION) && defined(_KERNEL_ORB) && !defined(_KERNEL)
//
// These are used to indicate that the serivce has completed its shutdown.
// When fault FAULTNUM_HA_FRMWK_KILL_NODE_2 is triggered, method
// Xdoor_manager::translate_in() waits on server_done_cv for shutdown to
// complete and exits when this condition variable is signalled.
//
extern os::mutex_t	server_done_mutex;
extern os::condvar_t	server_done_cv;
#endif

// Since factory_impl objects are themselves part of the service, they
// are only invoked on the primary
repl_sample::value_obj_ptr
factory_impl::create_value_obj(Environment &e)
{
	os::printf("factory_impl::create_value_obj()\n");

	// Allocate a new implementation, indicating that it is part of this
	// service
	repl_value_obj_impl * value_objp = new
		repl_value_obj_impl(my_provider);

	// Insert the object into a list of all objects in this service
	my_provider->list_lock.lock();
	my_provider->value_objlist.append(value_objp);
	my_provider->list_lock.unlock();

	my_provider->dump_obj_list();

	// Get a CORBA reference to the object
	// repl_sample::value_obj_ptr value_obj_ref = value_objp->get_objref();

	CORBA::type_info_t *typ = repl_sample::value_obj::_get_type_info(
	    my_provider->r_version.minor_num);
	repl_sample::value_obj_ptr value_obj_ref = value_objp->get_objref(typ);

	// Tell the secondaries about new object
	get_checkpoint()->ckpt_create_value_obj(value_obj_ref, e);

	// return a reference to the object
	return (value_obj_ref);
}


// This is the factory for persist objs
repl_sample::persist_obj_ptr
factory_impl::create_persist_obj(Environment &e)
{
	os::printf("factory_impl::create_persist_obj()\n");

	// Allocate a new impl, indicating that it is part of this service
	repl_persist_obj_impl * persist_objp = new
		repl_persist_obj_impl(my_provider);

	// insert object in list
	my_provider->list_lock.lock();
	my_provider->persist_objlist.append(persist_objp);
	my_provider->list_lock.unlock();

	my_provider->dump_obj_list();

	// Get a CORBA ref of the persistent object
	// repl_sample::persist_obj_ptr persist_obj_ref =
	//	persist_objp->get_objref();

	CORBA::type_info_t *typ = repl_sample::persist_obj::_get_type_info(
	    my_provider->r_version.minor_num);
	repl_sample::persist_obj_ptr persist_obj_ref =
	    persist_objp->get_objref(typ);

	// Tell secondaries about new object
	get_checkpoint()->ckpt_create_persist_obj(persist_obj_ref, e);

	// return a reference to the object
	return (persist_obj_ref);
}


// _unreferenced for factories
void
factory_impl::_unreferenced(unref_t cookie)
{
	os::printf("factory_impl::_unreferenced()\n");

	if (_last_unref(cookie)) {
		my_provider->factoryp = NULL;
		my_provider->initialized = false;
		my_provider->dump_obj_list();
		delete this;
	}
}

// Checkpoint accessor function.
repl_sample::ckpt_ptr
factory_impl::get_checkpoint()
{
	os::printf("factory_impl::get_checkpoint()\n");

	return ((repl_sample_prov*)(get_provider()))->
	    get_checkpoint_repl_sample();
}

#ifdef _KERNEL
// Static variables

SList<prov_in_mod_t> repl_sample_prov::repl_sample_prov_list;
os::mutex_t		repl_sample_prov::prov_list_lock;
extern struct modlinkage modlinkage;
#endif

repl_sample_prov::repl_sample_prov(const char *svc_desc,
	const char *repl_desc) :
		repl_server<repl_sample::ckpt> (svc_desc, repl_desc),
		factoryp(NULL),
		initialized(false),
		_ckpt_proxy(NULL),
		is_primary(false)
{
	svc_nam = new char[os::strlen(svc_desc) + 1];
	(void) os::strcpy(svc_nam, svc_desc);

	// Put the persist store ref in the local_ns after getting it
	// from the root_ns
	//
	(void) get_root_to_local();

	//
	// Initialize the running version of this repl_sample service
	// provider to version 1.0, since we try to simulate the
	// situation where both new and old service provider co-exist.
	// As new provider, it must be compatible with old provider.
	// Therefore, our running version is 1.0 at the beginning.
	//
	// For upgrade callback testing, we should query version manager
	// to get the current running version instead of assigning fixed
	// values here. Also we create another version variable to keep
	// trace of the highest version of this server can support. If
	// current running version is less than the highest version we
	// can support, we register upgrade callback here.
	//

	// ** NOTE **
	// The test make the assumption that the normal upgrade senario
	// has the running version changing from 1.0 -> 1.1
	//
	r_version.major_num = 1;
	r_version.minor_num = 0;

	vp_name = os::strdup("repl_sample");

	// Highest vp version we support
	highest_version.major_num = r_version.major_num;
	highest_version.minor_num = r_version.minor_num + 1;


#ifdef _KERNEL

	my_id = new prov_in_mod_t;
//	struct modinfo minfo;
//	(void) _info(&minfo);

	struct modctl	*mp;
	mp = mod_getctl(&modlinkage);
	if (mp == NULL) {
		os::panic("mod_getctl failed\n");
	}

	my_id->provp = this;
	my_id->mod_id = mp->mod_id;
//	my_id->mod_id = minfo.mi_id;

	prov_list_lock.lock();
	repl_sample_prov_list.append(my_id);
	prov_list_lock.unlock();

	os::printf("Created repl_sample_prov %p, mod_id %d, "
	    "version %d.%d\n", this, my_id->mod_id,
	    r_version.major_num, r_version.minor_num);
#else
	os::printf("Created repl_sample_prov version %d.%d\n",
	    r_version.major_num, r_version.minor_num);
#endif

	// Register upgrade callback, if necessary, during service
	// initialization.
	register_upgrade_callbacks();
	os::printf("Register upgrade callback called from "
	    "repl_sample_prov constructor\n");

}

repl_sample_prov::~repl_sample_prov()
{
	delete[] svc_nam;
	//
	// svc_nam is deleted above and factoryp would be deleted by
	// unreference. So suppress lint errors.
	//
	svc_nam = NULL; //lint -e1740
	factoryp = NULL; //lint -e1740

	os::printf("Deleting repl_sample_prov %p\n", this);

#ifdef _KERNEL
	prov_list_lock.lock();
	(void) repl_sample_prov_list.erase(my_id);
	prov_list_lock.unlock();
	delete my_id;
	my_id = NULL; // make lint happy
#endif

	_ckpt_proxy = NULL;
	delete [] vp_name;
}

// Implementation of the checkpoint methods
void
repl_sample_prov::ckpt_create_value_obj(
	repl_sample::value_obj_ptr obj,
	Environment &)
{
	os::printf("repl_sample_prov::ckpt_create_value_obj()\n");

	// Create a shadow copy of the object associating it with the reference
	// passed in
	repl_value_obj_impl * objp = new repl_value_obj_impl(obj, this);

	// Insert the object in a list of all value objects in this service
	list_lock.lock();
	value_objlist.append(objp);
	list_lock.unlock();
	dump_obj_list();
}

void
repl_sample_prov::ckpt_create_persist_obj(
	repl_sample::persist_obj_ptr obj,
	Environment &)
{
	os::printf("repl_sample_prov::ckpt_create_persist_obj()\n");

	// Use the replicated store
	repl_persist_obj_impl * objp =
		new repl_persist_obj_impl(obj, this);

	// Insert object into a list of persist objects in this service
	list_lock.lock();
	persist_objlist.append(objp);
	list_lock.unlock();
	dump_obj_list();
}

void
repl_sample_prov::ckpt_value_obj(
	repl_sample::value_obj_ptr obj,
	int32_t val,
	Environment &)
{
	os::printf("repl_sample_prov::ckpt_value_obj()\n");

	// Create a shadow cope of the object
	repl_value_obj_impl * objp = new repl_value_obj_impl(obj, this);

	// Set the value of this object to checkpointer value
	objp->_value(val);

	// Insert the object in a list of value objects
	list_lock.lock();
	value_objlist.append(objp);
	list_lock.unlock();
	dump_obj_list();
}

void
repl_sample_prov::ckpt_new_value_obj(
	repl_sample::value_obj_ptr obj,
	int32_t val,
	int32_t val_2,
	Environment &)
{
	os::printf("repl_sample_prov::ckpt_new_value_obj()\n");

	// Create a shadow cope of the object
	repl_value_obj_impl * objp = new repl_value_obj_impl(obj, this);

	// Set both values of this object to checkpointer values
	objp->_value(val);
	objp->_value_2(val_2);

	// Insert the object in a list of value objects
	list_lock.lock();
	value_objlist.append(objp);
	list_lock.unlock();
	dump_obj_list();
}

void
repl_sample_prov::ckpt_persist_obj(
	repl_sample::persist_obj_ptr obj,
	int32_t val,
	Environment &)
{
	os::printf("repl_sample_prov::ckpt_persist_obj()\n");

	repl_persist_obj_impl * objp =
		new repl_persist_obj_impl(obj, this);

	// Set the value of the replicated persistent object
	objp->_value(val);

	// Insert the object into a list of persistent objects in this service
	list_lock.lock();
	persist_objlist.append(objp);
	list_lock.unlock();
	dump_obj_list();
}

void
repl_sample_prov::ckpt_new_persist_obj(
	repl_sample::persist_obj_ptr obj,
	int32_t val,
	int32_t val_2,
	Environment &)
{
	os::printf("repl_sample_prov::ckpt_new_persist_obj()\n");

	repl_persist_obj_impl * objp =
		new repl_persist_obj_impl(obj, this);

	// Set both values of the replicated persistent object
	objp->_value(val);
	objp->_value_2(val_2);

	// Insert the object into a list of persistent objects in this service
	list_lock.lock();
	persist_objlist.append(objp);
	list_lock.unlock();
	dump_obj_list();
}
void
repl_sample_prov::ckpt_factory(
	repl_sample::factory_ptr obj,
	Environment &)
{
	os::printf("repl_sample_prov::ckpt_factory()\n");

	// Create a shadow copy and copy it to the global variable
	factoryp = new factory_impl(obj, this);

	// Mark this service as initialized
	initialized = true;

	os::printf("factoryp %p\n", factoryp);
}

void
repl_sample_prov::ckpt_set_value(
	repl_sample::value_obj_ptr obj,
	int32_t val,
	Environment &)
{
	os::printf("repl_sample_prov::ckpt_set_value(%x, %d)\n", obj, val);
	// Update the value of the object
	repl_value_obj_impl * objp =
		(repl_value_obj_impl *)obj->_handler()->get_cookie();
	ASSERT(objp != NULL);

	// Update the value
	objp->_value(val);

	// No state object is needed in this case
}

void
repl_sample_prov::ckpt_set_value_2(
	repl_sample::value_obj_ptr obj,
	int32_t val,
	Environment &)
{
	os::printf("repl_sample_prov::ckpt_set_value_2(%x, %d)\n",
	    obj, val);

	// Update both values of the object
	repl_value_obj_impl * objp =
		(repl_value_obj_impl *)obj->_handler()->get_cookie();
	ASSERT(objp != NULL);

	// Update the value_2
	objp->_value_2(val);

	// No state object is needed in this case
}

void
repl_sample_prov::ckpt_inc_value(
	repl_sample::value_obj_ptr obj,
	int32_t val,
	Environment &e)
{
	os::printf("repl_sample_prov::ckpt_inc_value()\n");

	// Update the value of the object
	repl_value_obj_impl * 	objp =
		(repl_value_obj_impl *)obj->_handler()->get_cookie();
	ASSERT(objp != NULL);

	// Set the value
	objp->_value(val);

	// Create a state object in case of retries
	value_inc_state * st = new value_inc_state(val);
	st->register_state(e);
	if (e.exception()) {
		e.exception()->print_exception("Failed to register_state:");
		e.clear();
	}
}

void
repl_sample_prov::ckpt_inc_value_2(
	repl_sample::value_obj_ptr obj,
	int32_t val,
	Environment &e)
{
	os::printf("repl_sample_prov::ckpt_inc_value_2()\n");

	// Update the value of the object
	repl_value_obj_impl * 	objp =
		(repl_value_obj_impl *)obj->_handler()->get_cookie();
	ASSERT(objp != NULL);

	// Set the value_2
	objp->_value_2(val);

	// Create a state object in case of retries
	value_inc_state * st = new value_inc_state(val);
	st->register_state(e);
	if (e.exception()) {
		e.exception()->print_exception("Failed to register_state:");
		e.clear();
	}
}

void
repl_sample_prov::ckpt_set_persist(
	repl_sample::persist_obj_ptr obj,
	int32_t val,
	Environment &)
{
	os::printf("repl_sample_prov::ckpt_set_persist()\n");
	os::printf("repl_sample_prov::ckpt_set_persist : obj %x, val %d\n",
		obj, val);

	// This is the checkpoint received when the cache value for a persisten
	// object needs to be updated

	repl_persist_obj_impl * objp =
		(repl_persist_obj_impl *)obj->_handler()->get_cookie();
	ASSERT(objp != NULL);

	// Set the value
	objp->_value(val);

	// We're done
	// No state object is needed in this case

}

void
repl_sample_prov::ckpt_set_persist_2(
	repl_sample::persist_obj_ptr obj,
	int32_t val,
	Environment &)
{
	os::printf("repl_sample_prov::ckpt_set_persist_2()\n");

	// This is the checkpoint received when the cache value for a persisten
	// object needs to be updated

	repl_persist_obj_impl * objp =
		(repl_persist_obj_impl *)obj->_handler()->get_cookie();
	ASSERT(objp != NULL);

	// Set both values
	objp->_value_2(val);

	// We're done
	// No state object is needed in this case

}

void
repl_sample_prov::ckpt_inc_persist_intent(
	repl_sample::persist_obj_ptr obj,
	int32_t val,
	Environment &e)
{
	os::printf("repl_sample_prov::ckpt_inc_persist_intent()\n");

	repl_sample::persist_obj_ptr obj_ref =
		repl_sample::persist_obj::_duplicate(obj);

	persist_inc_state * st = new persist_inc_state(obj_ref, val);
	st->register_state(e);
}

void
repl_sample_prov::ckpt_inc_persist_2_intent(
	repl_sample::persist_obj_ptr obj,
	int32_t val,
	Environment &e)
{
	os::printf("repl_sample_prov::ckpt_inc_persist_2_intent()\n");

	repl_sample::persist_obj_ptr obj_ref =
		repl_sample::persist_obj::_duplicate(obj);

	persist_2_inc_state * st =
		new persist_2_inc_state(obj_ref, val);
	st->register_state(e);
}

void
repl_sample_prov::ckpt_inc_persist_failure(
	Environment &e)
{
	os::printf("repl_sample_prov::ckpt_inc_persist_failure()\n");

	secondary_ctx * ctxs = secondary_ctx::extract_from(e);
	persist_inc_state * st;

	// Get the state
	st = (persist_inc_state *)ctxs->get_saved_state();
	ASSERT(st != NULL);
	st->my_progress = persist_inc_state::FAILED;
}

void
repl_sample_prov::ckpt_inc_persist_2_failure(
	Environment &e)
{
	os::printf("repl_sample_prov::ckpt_inc_persist_2_failure()\n");

	secondary_ctx * ctxs = secondary_ctx::extract_from(e);
	persist_2_inc_state * st;

	// Get the state
	st = (persist_2_inc_state *)ctxs->get_saved_state();
	ASSERT(st != NULL);
	st->my_progress = persist_2_inc_state::FAILED;
}


// The IDL Methods for replica::repl_prov
#ifdef FAULT_HA_FRMWK
void
repl_sample_prov::become_secondary(Environment &e)
#else
void
repl_sample_prov::become_secondary(Environment &)
#endif
{
	os::printf("repl_sample_prov::become_secondary()\n");

	CORBA::release(_ckpt_proxy);
	_ckpt_proxy = nil;
	is_primary = false;

#ifdef FAULT_HA_FRMWK
	// FAULT POINT
	// This fault point will cause become_primary to return an exception
	void		*fault_argp;
	uint32_t	fault_argsize;

	if (fault_triggered(
	    FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_BECOME_SECONDARY,
	    &fault_argp, &fault_argsize)) {
		os::printf("repl_sample_prov::become_secondary(): "
		    "FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_BECOME_SECONDARY\n");
		CORBA::Exception	*p_ex;

		// This fault will set some exception and return
		p_ex = new replica::repl_prov_failed;

		// Set the exception
		e.exception(p_ex);
		return;
	} // Fault Point
#endif
}

void
repl_sample_prov::add_secondary(
	replica::checkpoint_ptr sec,
	const char *,
	Environment &e)
{
	uint_t	i = 0;
	os::printf("repl_sample_prov::add_secondary()\n");

	repl_sample::ckpt_var ckpt_v = repl_sample::ckpt::_narrow(sec);

	ASSERT(!CORBA::is_nil(ckpt_v));

	// Dump the factory if it exists
	if (factoryp != NULL) {
		os::printf("Dumping state...\n");

		// repl_sample::factory_var factory_ref =
		//	factoryp->get_objref();
		os::printf("****r_version.minor_num = %d ****\n",
		    r_version.minor_num);
		CORBA::type_info_t *typ;
//		typ = repl_sample::factory::_get_type_info(
//		    r_version.minor_num);
//		repl_sample::factory_var factory_ref =
//		    factoryp->get_objref(typ);

		repl_sample::factory_var factory_ref =
		    factoryp->get_objref();

		os::printf("add_secs : ckpt_factory()\n");
		ckpt_v->ckpt_factory(factory_ref, e);
		if (e.exception()) {
			// XXX - I thought no exceptions could occur
			// but appears that in this case there can be
			// an exception
			// So just clear it (ignore it)
			e.exception()->print_exception(
			    "repl_sample::ckpt::ckpt_factory():");
			e.clear();
		}

		FAULTPT_HA_FRMWK(
		    FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_ADD_SECONDARY,
		    FaultFunctions::generic);

		// The value objects
		repl_value_obj_impl * value_objp;

		// The persist object
		repl_persist_obj_impl * persist_objp;

		int32_t value = -99;
		int32_t value_2 = -99;

		// Dump the value_objlist
		list_lock.lock();
		for (value_objlist.atfirst();
			(value_objp = value_objlist.get_current()) != NULL;
			value_objlist.advance(), i++) {

			os::printf("Dump value_objlist...\n");

			// Get a reference (CORBA) to the current object
			repl_sample::value_obj_var value_obj_ref;
			// value_obj_ref = value_objp->get_objref();

			typ = repl_sample::value_obj::_get_type_info(
			    r_version.minor_num);
			value_obj_ref = value_objp->get_objref(typ);

			// Get the value
			int16_t tmpval;		// tmp val
			value = value_objp->get_value(tmpval, e);

			// Retrieve value_2 from value_obj only in version 1
			if (r_version.minor_num == 1) {
				value_2 = value_objp->get_value_2(tmpval, e);
			}

			os::printf("value %d, value_2 %d\n", value, value_2);

			// Checkpoint differently based on current
			// running version
			if (r_version.minor_num == 1) {

			    // Checkpoint both two values in the object
			    os::printf("add_secs : ckpt_new_value_obj()\n");
			    ckpt_v->ckpt_new_value_obj(value_obj_ref, value,
				value_2, e);
			    if (e.exception()) {
				// XXX - I thought no exceptions could occur
				// but appears that in this case there can be
				// an exception
				// So just clear it (ignore it)
				e.exception()->print_exception(
				    "repl_sample::ckpt::ckpt_new_value_obj():");
				e.clear();
			    }
			} else if (r_version.minor_num == 0) {

			    // Checkpoint the value in the object
			    os::printf("add_secs : ckpt_value_obj()\n");
			    ckpt_v->ckpt_value_obj(value_obj_ref, value, e);
			    if (e.exception()) {
				// XXX - I thought no exceptions could occur
				// but appears that in this case there can be
				// an exception
				// So just clear it (ignore it)
				e.exception()->print_exception(
				    "repl_sample::ckpt::ckpt_value_obj():");
				e.clear();
			    }
			} else {
			    // Should not be here !!
			    ASSERT(0);
			}
		}

		// Dump the persist_objlist
		for (persist_objlist.atfirst();
			(persist_objp = persist_objlist.get_current()) != NULL;
			persist_objlist.advance(), i++) {

			os::printf("Dump persist_objlist...\n");

			// Get a reference (CORBA) to the current object
			repl_sample::persist_obj_var persist_obj_ref;
			// persist_obj_ref = persist_objp->get_objref();

			typ = repl_sample::persist_obj::_get_type_info(
			    r_version.minor_num);
			persist_obj_ref = persist_objp->get_objref(typ);

			// Get the value
			int16_t	tmpval;		// tmpval
			value = persist_objp->get_value(tmpval, e);

			// Retrieve value_2 from persist_obj only in version 1
			if (r_version.minor_num == 1) {
				value_2 = persist_objp->get_value_2(tmpval, e);
			}

			os::printf("value %d, value_2 %d\n", value, value_2);

			// Checkpoint differently based on current
			// running version
			if (r_version.minor_num == 1) {

			    // Checkpoint both two values in the object
			    os::printf("add_secs : ckpt_new_persist_obj()\n");
			    ckpt_v->ckpt_new_persist_obj(persist_obj_ref, value,
				value_2, e);
			    if (e.exception()) {
				// XXX - I thought no exceptions could occur
				// but appears that in this case there can be
				// an exception
				// So just clear it (ignore it)
				e.exception()->print_exception(
				    "repl_sample::ckpt::"
				    "ckpt_new_persist_obj():");
				e.clear();
			    }
			} else if (r_version.minor_num == 0) {

			    // Checkpoint the value in the object
			    os::printf("add_secs : ckpt_persist_obj()\n");
			    ckpt_v->ckpt_persist_obj(persist_obj_ref, value, e);
			    if (e.exception()) {
				// XXX - I thought no exceptions could occur
				// but appears that in this case there can be
				// an exception
				// So just clear it (ignore it)
				e.exception()->print_exception(
				    "repl_sample::ckpt::ckpt_persist_obj():");
				e.clear();
			    }
			} else {
			    // Should not be here !!
			    ASSERT(0);
			}
		}
		list_lock.unlock();
	}

	os::printf("Dumped %d objects\n", i);
}


void
repl_sample_prov::remove_secondary(
	const char *, Environment &)
{
	os::printf("repl_sample_prov::remove_secondary()\n");
}


void
repl_sample_prov::become_primary(const replica::repl_name_seq &, Environment &e)
{
	os::printf("repl_sample_prov::become_primary()\n");

	os::printf("become_primary : r_version.minor_num %d\n",
	    r_version.minor_num);
	os::printf("become_primary : Checkpoint proxy _ckpt_proxy %x\n",
	    _ckpt_proxy);
	os::printf("become_primary : repl_sample_prov %p\n", this);

	//
	// First, initialize the checkpoint proxy to the right type based
	// on current running version of this HA service. We might end up
	// using an older version of checkpoint interface to communicate
	// with other nodes to remain compatible even though we are
	// capable to support higher version.
	//
	replica::checkpoint_var tmp_ckptp =
	    set_checkpoint(repl_sample::ckpt::_get_type_info(
	    r_version.minor_num));
	_ckpt_proxy = repl_sample::ckpt::_narrow(tmp_ckptp);


	os::printf("become_primary : r_version.minor_num %d\n",
	    r_version.minor_num);
	os::printf("become_primary : Checkpoint proxy _ckpt_proxy %x\n",
	    _ckpt_proxy);
	os::printf("become_primary : repl_sample_prov %p\n", this);


//
// Comment out the v0 way to set checkpoint
//
//	replica::checkpoint_var tmp_ckptp =
//		set_checkpoint(repl_sample::ckpt::_get_type_info(0));
//	_ckpt_proxy = repl_sample::ckpt::_narrow(tmp_ckptp);

	ASSERT(!CORBA::is_nil(_ckpt_proxy));

	// Update the state to be primary.
	is_primary = true;

#ifdef FAULT_HA_FRMWK

	// This fault point will cause become_primary to return an exception
	if (fault_triggered(FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_BECOME_PRIMARY,
	    NULL, NULL)) {
		CORBA::Exception	*p_ex;

		// This fault will set some exception and return
		p_ex = new replica::repl_prov_failed;

		// When returning exception, release the checkpoint.
		CORBA::release(_ckpt_proxy);
		_ckpt_proxy = nil;

		// Set the exception
		e.exception(p_ex);
		return;
	} // Fault Point
#endif

	if (!initialized) {
		// Call start service to create the factory
		initialized = true;
		start_service(e);
		os::printf("become_primary : factory_impl %p\n", factoryp);

	}

#ifdef FAULT_HA_FRMWK
	repl_persist_obj_impl * persist_objp;

	for (persist_objlist.atfirst();
		(persist_objp = persist_objlist.get_current()) != NULL;
		persist_objlist.advance()) {
		Environment env;
		persist_objp->update_storage(env);

	}
#endif

}

CORBA::Object_ptr
repl_sample_prov::get_root_obj(Environment &)
{
	os::printf("repl_sample_prov::get_root_obj()\n");

	ASSERT(factoryp != NULL);
	return (factoryp->get_objref());

}

void
repl_sample_prov::become_spare(Environment &)
{
	os::printf("repl_sample_prov::become_spare()\n");


	repl_value_obj_impl * value_objp;
	repl_persist_obj_impl * persist_objp;

	list_lock.lock();
	while ((value_objp = value_objlist.reapfirst()) != NULL) {
		delete value_objp;
	}

	while ((persist_objp = persist_objlist.reapfirst()) != NULL) {
		delete persist_objp;
	}
	list_lock.unlock();

	dump_obj_list();

	delete factoryp;
	factoryp = NULL;  //lint !e423
	initialized = false;
}

void
repl_sample_prov::freeze_primary_prepare(Environment &)
{
	os::printf("repl_sample_prov::freeze_primary_prepare()\n");
}

void
repl_sample_prov::freeze_primary(Environment &)
{
	os::printf("repl_sample_prov::freeze_primary()\n");
}

void
repl_sample_prov::unfreeze_primary(Environment &)
{
	os::printf("repl_sample_prov::unfreeze_primary()\n");
}

void
repl_sample_prov::common_shutdown()
{
	os::printf("repl_sample_prov::common_shutdown()\n");

	char factory_name[50];
	os::sprintf(factory_name, "%s factory", svc_nam);

	// Nested invocation requires its own Environment
	Environment	env;

	// Unbind the factory in the name server
	naming::naming_context_var	ctxp = ns::root_nameserver();
	ctxp->unbind((const char *)factory_name, env);
	//
	// If the service should not wait for all references to go
	// away, i.e. wait_for_unref = 0, then the primary may have
	// unbound factory object before shutdown() is called on
	// secondaries.  Trying to unbind an unreferenced object from
	// nameserver will return a not_found exception on secondaries.
	//
	if (env.exception()) {
		os::printf("++ factory not found \n");
		ASSERT(naming::not_found::_exnarrow(env.exception()));
		env.clear();
	}
	factoryp = NULL; //lint !e423
	initialized = false;

#if defined(_FAULT_INJECTION) && defined(_KERNEL_ORB) && !defined(_KERNEL)
	server_done_mutex.lock();
	server_done_cv.signal();
	server_done_mutex.unlock();
#endif
}

uint32_t
repl_sample_prov::forced_shutdown(Environment &)
{
	os::printf("repl_sample_prov::forced_shutdown()\n");

	os::printf("++ factoryp = %p\n", factoryp);

	list_lock.lock();
	os::printf("Value Objlist: %d, Persist Objlist: %d\n",
	    value_objlist.count(), persist_objlist.count());
	list_lock.unlock();

	common_shutdown();

	// Release the reference to the checkpoint.
	CORBA::release(_ckpt_proxy);
	_ckpt_proxy = nil;

	// Update the state to be non-primary
	is_primary = false;

	return (0);
}

void
repl_sample_prov::shutdown(Environment &)
{
	os::printf("repl_sample_prov::shutdown()\n");

	os::printf("++ factoryp = %p\n", factoryp);

	int	wait_for_unref = 1;

#ifdef FAULT_HA_FRMWK
	if (fault_triggered(
	    FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_SHUTDOWN_W_O_WAITING_FOR_UNREF,
	    NULL, NULL)) {
		wait_for_unref = 0;
	}
#endif

	list_lock.lock();
	os::printf("Value Objlist: %d, Persist Objlist: %d\n",
		    value_objlist.count(), persist_objlist.count());
	list_lock.unlock();

	os::printf("Verifying that all HA objects have been unreferenced: ");

	while (wait_for_unref) {
		list_lock.lock();
		if (value_objlist.empty() && persist_objlist.empty()) {
			list_lock.unlock();
			break;
		}
		list_lock.unlock();

		// Spin while these lists arent empty
		os::printf(".");
		os::usecsleep(3L * 1000000);
		dump_obj_list();
	}
	os::printf(" DONE!\n");

	common_shutdown();

	CORBA::release(_ckpt_proxy);
	_ckpt_proxy = nil;

	is_primary = false;
}

void
repl_sample_prov::register_upgrade_callbacks()
{
	os::printf("repl_sample_prov::register_upgrade_callbacks()\n");

	os::printf("Register vp %s...\n", vp_name);

	Environment	e;

	// Get a pointer to the local version manager.
	version_manager::vm_admin_var vmgr_v = vm_util::get_vm(NODEID_UNKNOWN);

	version_lock.wrlock();

	repl_sample_callback *cbp = new repl_sample_callback(this);
	version_manager::upgrade_callback_var cb_v = cbp->get_objref();

	// VP repl_sample is a cluster vp. And we use the same ucc name
	// as the vp name.
	version_manager::ucc_seq_t ucc_seq(1, 1);

	ucc_seq[0].ucc_name = os::strdup(vp_name);
	ucc_seq[0].vp_name = os::strdup(vp_name);
	os::printf("Register %s upgrade callback with VM ...\n", vp_name);
	vmgr_v->register_upgrade_callbacks(ucc_seq, cb_v, highest_version,
	    r_version, e);
	if (e.exception())
	    vm_exception(e);

	os::printf("SUCCESS: registered vp %s\n", vp_name);


	//
	// Define running version and highest version for the following
	// test version protocols (Note: they are shared by all VPs.)
	//
	version_manager::vp_version_t	run_version, max_version;

	//
	// The following define the uccs of bootstrap-cluster vps.
	// And the ucc names have to match with their assoicated vp names.
	//


	//
	// btstrp_cl_vp_1 upgrade callback registration
	//
	ucc_seq[0].ucc_name = os::strdup("btstrp_cl_vp_1");
	ucc_seq[0].vp_name = os::strdup("btstrp_cl_vp_1");

	vmgr_v->get_running_version(ucc_seq[0].vp_name, run_version, e);
	if (e.exception())
		vm_exception(e);
	os::printf("%s running version is %d.%d\n",
		(const char *)ucc_seq[0].vp_name,
		run_version.major_num, run_version.minor_num);

	// There is a small window opportunity that running version of
	// the vp change after the previous running version query and
	// cause the upgrade callback registration to fail.
	max_version.major_num = run_version.major_num;
	max_version.minor_num = run_version.minor_num + 1;

	os::printf("%s highest supported version is %d.%d\n",
		(const char *)ucc_seq[0].vp_name,
		max_version.major_num, max_version.minor_num);

	os::printf("Register btstrp_cl_vp_1 upgrade callback with VM...\n");
	vmgr_v->register_upgrade_callbacks(ucc_seq, cb_v, max_version,
		run_version, e);
	if (e.exception())
		vm_exception(e);


	//
	// btstrp_cl_vp_2 upgrade callback registration
	//
	ucc_seq[0].ucc_name = os::strdup("btstrp_cl_vp_2");
	ucc_seq[0].vp_name = os::strdup("btstrp_cl_vp_2");

	vmgr_v->get_running_version(ucc_seq[0].vp_name, run_version, e);
	if (e.exception())
		vm_exception(e);
	os::printf("%s running version is %d.%d\n",
		(const char *)ucc_seq[0].vp_name,
		run_version.major_num, run_version.minor_num);

	// There is a small window opportunity that running version of
	// the vp change after the previous running version query and
	// cause the upgrade callback registration to fail.
	max_version.major_num = run_version.major_num;
	max_version.minor_num = run_version.minor_num + 1;

	os::printf("%s highest supported version is %d.%d\n",
		(const char *)ucc_seq[0].vp_name,
		max_version.major_num, max_version.minor_num);

	os::printf("Register btstrp_cl_vp_2 upgrade callback with VM...\n");
	vmgr_v->register_upgrade_callbacks(ucc_seq, cb_v, max_version,
		run_version, e);
	if (e.exception())
		vm_exception(e);


	//
	// btstrp_cl_vp_3 upgrade callback registration
	//
	ucc_seq[0].ucc_name = os::strdup("btstrp_cl_vp_3");
	ucc_seq[0].vp_name = os::strdup("btstrp_cl_vp_3");

	vmgr_v->get_running_version(ucc_seq[0].vp_name, run_version, e);
	if (e.exception())
		vm_exception(e);
	os::printf("%s running version is %d.%d\n",
		(const char *)ucc_seq[0].vp_name,
		run_version.major_num, run_version.minor_num);

	// There is a small window opportunity that running version of
	// the vp change after the previous running version query and
	// cause the upgrade callback registration to fail.
	max_version.major_num = run_version.major_num;
	max_version.minor_num = run_version.minor_num + 1;

	os::printf("%s highest supported version is %d.%d\n",
		(const char *)ucc_seq[0].vp_name,
		max_version.major_num, max_version.minor_num);

	os::printf("Register btstrp_cl_vp_3 upgrade callback with VM...\n");
	vmgr_v->register_upgrade_callbacks(ucc_seq, cb_v, max_version,
		run_version, e);
	if (e.exception())
		vm_exception(e);


	//
	// btstrp_cl_vp_4 upgrade callback registration
	//
	ucc_seq[0].ucc_name = os::strdup("btstrp_cl_vp_4");
	ucc_seq[0].vp_name = os::strdup("btstrp_cl_vp_4");

	vmgr_v->get_running_version(ucc_seq[0].vp_name, run_version, e);
	if (e.exception())
		vm_exception(e);
	os::printf("%s running version is %d.%d\n",
		(const char *)ucc_seq[0].vp_name,
		run_version.major_num, run_version.minor_num);

	// There is a small window opportunity that running version of
	// the vp change after the previous running version query and
	// cause the upgrade callback registration to fail.
	max_version.major_num = run_version.major_num;
	max_version.minor_num = run_version.minor_num + 1;

	os::printf("%s highest supported version is %d.%d\n",
		(const char *)ucc_seq[0].vp_name,
		max_version.major_num, max_version.minor_num);

	os::printf("Register btstrp_cl_vp_4 upgrade callback with VM...\n");
	vmgr_v->register_upgrade_callbacks(ucc_seq, cb_v, max_version,
		run_version, e);
	if (e.exception())
		vm_exception(e);


	//
	// btstrp_cl_vp_5 upgrade callback registration
	//
	ucc_seq[0].ucc_name = os::strdup("btstrp_cl_vp_5");
	ucc_seq[0].vp_name = os::strdup("btstrp_cl_vp_5");

	vmgr_v->get_running_version(ucc_seq[0].vp_name, run_version, e);
	if (e.exception())
		vm_exception(e);
	os::printf("%s running version is %d.%d\n",
		(const char *)ucc_seq[0].vp_name,
		run_version.major_num, run_version.minor_num);

	// There is a small window opportunity that running version of
	// the vp change after the previous running version query and
	// cause the upgrade callback registration to fail.
	max_version.major_num = run_version.major_num;
	max_version.minor_num = run_version.minor_num + 1;

	os::printf("%s highest supported version is %d.%d\n",
		(const char *)ucc_seq[0].vp_name,
		max_version.major_num, max_version.minor_num);

	os::printf("Register btstrp_cl_vp_5 upgrade callback with VM...\n");
	vmgr_v->register_upgrade_callbacks(ucc_seq, cb_v, max_version,
		run_version, e);
	if (e.exception())
		vm_exception(e);

	// Set to number of bootstrap-cluster upgrade callback registered
	// cbp->max_num_btstrp_cl = 5;

	os::printf("SUCCESS: bootstrap-cluster vps registration completed.\n");


	//
	// The following define the uccs for cluster vps.
	//
	char **tmp = new char *[1];
	char **tmp2 = new char *[1];
	char **tmp3 = new char *[1];
	char **tmp4 = new char *[1];

	version_manager::ucc_seq_t ucc_cl_seq(5, 5);

	ucc_cl_seq[0].ucc_name = os::strdup("cluster_vp_1");
	ucc_cl_seq[0].vp_name = os::strdup("non_btstrp_cl_vp");

	ucc_cl_seq[1].ucc_name = os::strdup("cluster_vp_2");
	ucc_cl_seq[1].vp_name = os::strdup("non_btstrp_cl_vp");
	tmp[0] = os::strdup("cluster_vp_1");
	version_manager::string_seq_t bseq_1(1, 1, tmp, true);
	ucc_cl_seq[1].upgrade_before = bseq_1;

	ucc_cl_seq[2].ucc_name = os::strdup("cluster_vp_3");
	ucc_cl_seq[2].vp_name = os::strdup("non_btstrp_cl_vp");
	tmp2[0] = os::strdup("cluster_vp_2");
	version_manager::string_seq_t bseq_2(1, 1, tmp2, true);
	ucc_cl_seq[2].upgrade_before = bseq_2;

	ucc_cl_seq[3].ucc_name = os::strdup("cluster_vp_4");
	ucc_cl_seq[3].vp_name = os::strdup("non_btstrp_cl_vp");
	tmp3[0] = os::strdup("cluster_vp_3");
	version_manager::string_seq_t bseq_3(1, 1, tmp3, true);
	ucc_cl_seq[3].upgrade_before = bseq_3;

	ucc_cl_seq[4].ucc_name = os::strdup("cluster_vp_5");
	ucc_cl_seq[4].vp_name = os::strdup("non_btstrp_cl_vp");
	tmp4[0] = os::strdup("cluster_vp_4");
	version_manager::string_seq_t bseq_4(1, 1, tmp4, true);
	ucc_cl_seq[4].upgrade_before = bseq_4;

	vmgr_v->get_running_version(ucc_cl_seq[0].vp_name, run_version, e);
	if (e.exception())
		vm_exception(e);
	os::printf("%s running version is %d.%d\n",
		(const char *)ucc_cl_seq[0].vp_name,
		run_version.major_num, run_version.minor_num);

	// There is a small window opportunity that running version of
	// the vp change after the previous running version query and
	// cause the upgrade callback registration to fail.
	max_version.major_num = run_version.major_num;
	max_version.minor_num = run_version.minor_num + 1;

	os::printf("%s highest supported version is %d.%d\n",
		(const char *)ucc_cl_seq[0].vp_name,
		max_version.major_num, max_version.minor_num);

	os::printf("Register cluster upgrade callback with VM...\n");
	// Register cluster vp upgrade callback
	vmgr_v->register_upgrade_callbacks(ucc_cl_seq, cb_v, max_version,
		run_version, e);
	if (e.exception())
		vm_exception(e);

	version_lock.unlock();


	os::printf("SUCCESS: registered non-bootstrap cluster vp "
		"'non_btstrp_cl_vp'\n");

}

void
repl_sample_prov::upgrade_callback(const version_manager::vp_version_t &v)
{
	os::printf("repl_sample_prov::upgrade_callback()\n");

	version_lock.wrlock();

	if (r_version.major_num == v.major_num &&
	    r_version.minor_num == v.minor_num) {
		os::printf("*** ATTENTION ***\n");
		os::printf("repl_sample_prov: running version is same as "
		    "new version being notified.\n");
		os::printf("running version %d.%d \n", r_version.major_num,
		    r_version.minor_num);
		os::printf("new version %d.%d \n", v.major_num, v.minor_num);
		version_lock.unlock();
		return;
	}

	os::printf("repl_sample_prov: version has been changed from %d.%d "
	    "to %d.%d. %s\n", r_version.major_num, r_version.minor_num,
	    v.major_num, v.minor_num, vp_name);

	ASSERT(v.major_num >= r_version.major_num);
	ASSERT(v.minor_num > r_version.minor_num);

	os::printf("Update repl_sample_prov running version...\n");
	r_version.major_num = v.major_num;
	r_version.minor_num = v.minor_num;
	os::printf("repl_sample_prov running version now is %d.%d\n",
	    r_version.major_num, r_version.minor_num);

	if (is_primary) {
		//
		// Keep the reference to the old ckpt_proxy first.
		// Note we don't want to do duplicate here.
		//
		os::printf("Save the old multicheckpoint proxy\n");
		replica::checkpoint_ptr old_ckptp = _ckpt_proxy;

		// Upgrade our multicheckpoint proxy
		// XX need use vp version -> idl version mapping
		os::printf("Upgrade multicheckpoint proxy in primary node\n");
		replica::checkpoint_var new_ckptp =
		    set_checkpoint(repl_sample::ckpt::_get_type_info(
		    r_version.minor_num));

		os::printf("Switch to new multicheckpoint proxy\n");
		_ckpt_proxy = repl_sample::ckpt::_narrow(new_ckptp);
		ASSERT(!CORBA::is_nil(_ckpt_proxy));
		os::printf("SUCCESS: upgraded multicheckpoint proxy\n");

		os::printf("Release old multicheckpoint proxy\n");
		CORBA::release(old_ckptp);
	}

	version_lock.unlock();

	// Unregister UCC if highest support version has reached.
	if (r_version.major_num == highest_version.major_num &&
	    r_version.minor_num == highest_version.minor_num) {
		os::printf("repl_sample_prov running version has reached "
		    "highest support version %d.%d \n",
		    highest_version.major_num, highest_version.minor_num);

		char **tmp = new char *[1];
		tmp[0] = os::strdup(vp_name);
		version_manager::string_seq_t ucc_name_seq(1, 1, tmp, true);

		os::printf("Unregister UCC ... %s\n", vp_name);
		// vm_lca_impl::the().unregister_uccs(ucc_name_seq);
		os::printf("SUCCESS: unregistered UCC ... %s\n", vp_name);
	}

}

// Miscelaneous methods
void
repl_sample_prov::start_service(Environment &e)
{
	os::printf("repl_sample_prov::start_service()\n");

	factoryp = new factory_impl(this);  //lint !e423

	char	factory_name[50];

	// Checkpoint the factory
	repl_sample::factory_var factory_ref = factoryp->get_objref();

	factoryp->get_checkpoint()->ckpt_factory(factory_ref, e);
	if (e.exception())
		os::panic("Exception in checkpoint");

	os::sprintf(factory_name, "%s factory", svc_nam);

	// Bind the factory in the name server
	Environment env;
	naming::naming_context_var ctxp = ns::root_nameserver();
	ctxp->rebind((const char *)factory_name, factory_ref, env);
	ASSERT(!env.exception());
}


// The Actual Object IDL members
// First the volatile object

// Get the value stored in the object
int32_t
repl_value_obj_impl::get_value(int16_t & nodenum, Environment &)
{
	os::printf("repl_value_obj_impl::get_value()\n");

	nodenum = nodeid;
	// os::printf("value_obj_impl::get_value()\n");
	// This is an example of a Basic mini-transaction
	// It is idempotent (we are only reporting a value)
	// It can be re-tried without any special protocol
	// No need to checkpoint
	int32_t rslt;

	// FAULT POINT
	// Generic fault point during the invocation.  Before any significant
	// processing can be completed.
	// Retries will trgger this fault again.
	FAULTPT_HA_FRMWK(
		FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_GET_VALUE,
		FaultFunctions::generic);

	lck.lock();
	rslt = _value();
	lck.unlock();

	os::printf("repl_value_obj_impl::get_value(%d)\n", rslt);

	return (rslt);
}

// Get the value_2 stored in the object
int32_t
repl_value_obj_impl::get_value_2(int16_t & nodenum, Environment &)
{
	os::printf("repl_value_obj_impl::get_value_2()\n");

	nodenum = nodeid;
	// os::printf("value_obj_impl::get_value_2()\n");
	// This is an example of a Basic mini-transaction
	// It is idempotent (we are only reporting a value)
	// It can be re-tried without any special protocol
	// No need to checkpoint
	int32_t rslt;

	// FAULT POINT
	// Generic fault point during the invocation.  Before any significant
	// processing can be completed.
	// Retries will trgger this fault again.
	FAULTPT_HA_FRMWK(
		FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_GET_VALUE,
		FaultFunctions::generic);

	lck.lock();
	rslt = _value_2();
	lck.unlock();

	os::printf("repl_value_obj_impl::get_value_2(%d)\n", rslt);

	return (rslt);
}

// Set the value stored in the object
void
repl_value_obj_impl::set_value(int32_t val, int16_t &nodenum, Environment &e)
{
	os::printf("repl_value_obj_impl::set_value(%d)\n", val);

	nodenum = nodeid;
	// os::printf("value_obj_impl::set_value(%d)\n", val);
	// This is an example of a 1-phase mini-transaction
	// It is idempotent (we can re-try without affecting outcome)

	// FAULT POINT
	// This generic fault point will be triggered during an invocation.
	// (This is a One phase idempotent mini transaction)
	FAULTPT_HA_FRMWK(
		FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_SET_VALUE,
		FaultFunctions::generic);

	lck.lock();
	_value(val);

	repl_sample::ckpt_ptr ckptp = get_checkpoint();
	// repl_sample::value_obj_var objvar = get_objref();

	CORBA::type_info_t *typ =
	    repl_sample::value_obj::_get_type_info(
	    my_provider->r_version.minor_num);
	repl_sample::value_obj_var objvar = get_objref(typ);

	ckptp->ckpt_set_value(objvar, val, e);
	if (e.exception())
		os::panic("Exception in checkpoint");
	lck.unlock();
}

// Set the value_2 stored in the object
void
repl_value_obj_impl::set_value_2(int32_t val, int16_t &nodenum, Environment &e)
{
	os::printf("repl_value_obj_impl::set_value_2(%d)\n", val);

	nodenum = nodeid;
	// os::printf("value_obj_impl::set_value_2(%d)\n", val);
	// This is an example of a 1-phase mini-transaction
	// It is idempotent (we can re-try without affecting outcome)

	// FAULT POINT
	// This generic fault point will be triggered during an invocation.
	// (This is a One phase idempotent mini transaction)
	FAULTPT_HA_FRMWK(
		FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_SET_VALUE,
		FaultFunctions::generic);

	lck.lock();
	_value_2(val);

	repl_sample::ckpt_ptr ckptp = get_checkpoint();
	// repl_sample::value_obj_var objvar = get_objref();

	CORBA::type_info_t *typ =
	    repl_sample::value_obj::_get_type_info(
	    my_provider->r_version.minor_num);
	repl_sample::value_obj_var objvar = get_objref(typ);

	os::printf("repl_value_obj_impl::set_value_2\n");
	os::printf("r_version.minor_num %d\n",
	    my_provider->r_version.minor_num);
	os::printf("typ %x\n", typ);
	os::printf("val %d\n", val);

	ckptp->ckpt_set_value_2(objvar, val, e);
	if (e.exception())
		os::panic("Exception in checkpoint");
	lck.unlock();
}

// Increase the value stored in the object
int32_t
repl_value_obj_impl::inc_value(int16_t &nodenum, Environment &e)
{
	os::printf("repl_value_obj_impl::inc_value()\n");

	nodenum = nodeid;
	// os::printf("value_obj_impl::inc_value()\n");
	// This is an example of a 1-phase non-indempotent mini-transaction
	// The service needs to keep the state of the value after it has been
	// increased.  Otherwise a retry might update twice.

	// FAULT POINT
	// Generic fault point during the invocation.  Before any processing
	// can take place.  (1-Phase non-idempotent)
	FAULTPT_HA_FRMWK(
		FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_INC_VALUE,
		FaultFunctions::generic);

	int32_t rslt;

	lck.lock();

	// Check for saved state
	primary_ctx * ctxp = primary_ctx::extract_from(e);
	value_inc_state * st = (value_inc_state *) ctxp->get_saved_state();

	if (st) {
		// Found saved state
		os::printf("Found saved state\n");
		// Use the saved state value to return
		rslt = st->value;
	} else {

		// Otherwise update value
		_value(_value() + 1);
		rslt = _value();

		// Checkpoint the update to the secondaries
		repl_sample::ckpt_ptr ckptp = get_checkpoint();
		// repl_sample::value_obj_var objvar = get_objref();

		CORBA::type_info_t *typ =
		    repl_sample::value_obj::_get_type_info(
		    my_provider->r_version.minor_num);
		repl_sample::value_obj_var objvar = get_objref(typ);

		ckptp->ckpt_inc_value(objvar, rslt, e);
		if (e.exception())
			os::panic("Exception in checkpoint ckpt_inc_value");
	}

	lck.unlock();
	return (rslt);
}

// Increase value_2 stored in the object
int32_t
repl_value_obj_impl::inc_value_2(int16_t &nodenum, Environment &e)
{
	os::printf("repl_value_obj_impl::inc_value_2()\n");

	nodenum = nodeid;
	// os::printf("value_obj_impl::inc_two_values()\n");
	// This is an example of a 1-phase non-indempotent mini-transaction
	// The service needs to keep the state of the value after it has been
	// increased.  Otherwise a retry might update twice.

	// FAULT POINT
	// Generic fault point during the invocation.  Before any processing
	// can take place.  (1-Phase non-idempotent)
	FAULTPT_HA_FRMWK(
		FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_INC_VALUE,
		FaultFunctions::generic);

	int32_t rslt;

	lck.lock();

	// Check for saved state
	primary_ctx * ctxp = primary_ctx::extract_from(e);
	value_inc_state * st = (value_inc_state *) ctxp->get_saved_state();

	if (st) {
		// Found saved state
		os::printf("Found saved state\n");
		// Use the saved state value to return
		rslt = st->value;
	} else {

		// Otherwise update value_2
		_value_2(_value_2() + 1);
		rslt = _value_2();

		// Checkpoint the update to the secondaries
		repl_sample::ckpt_ptr ckptp = get_checkpoint();
		// repl_sample::value_obj_var objvar = get_objref();

		CORBA::type_info_t *typ =
		    repl_sample::value_obj::_get_type_info(
		    my_provider->r_version.minor_num);
		repl_sample::value_obj_var objvar = get_objref(typ);

		ckptp->ckpt_inc_value_2(objvar, rslt, e);
		if (e.exception())
			os::panic("Exception in checkpoint ckpt_inc_value_2");

	}

	lck.unlock();
	return (rslt);
}


// Unreferenced
void
repl_value_obj_impl::_unreferenced(unref_t cookie)
{
	os::printf("repl_value_obj_impl::_unreferenced()\n");
	if (_last_unref(cookie)) {
		ASSERT(my_provider != NULL);

		my_provider->list_lock.lock();
		(void) my_provider->value_objlist.erase(this);
		my_provider->list_lock.unlock();
		my_provider->dump_obj_list();
		delete this;
	}
}

// Checkpoint accessor function.
repl_sample::ckpt_ptr
repl_value_obj_impl::get_checkpoint()
{
	os::printf("repl_value_obj_impl::get_checkpoint()\n");

	return ((repl_sample_prov*)(get_provider()))->
	    get_checkpoint_repl_sample();
}


// Constructor for persistent_obj
// Primary Constructor
repl_persist_obj_impl::repl_persist_obj_impl(repl_sample_prov *server) :
	/* CSTYLED */
	mc_replica_of<repl_sample::persist_obj> (server),
	my_provider(server)
{
	os::printf("repl_persist_obj_impl::repl_persist_obj_impl(1)\n");

	// Get a reference to the persisten storage object
	Environment e;
	naming::naming_context_var ctxp = ns::root_nameserver();
	CORBA::Object_var obj_ref =
		ctxp->resolve("ha sample persist store", e);
	if (e.exception()) {
		e.exception()->print_exception
			("Getting persistent object from root node");
		ASSERT(0);
	}

	// Make this object a smart reference to persisten_store
	storage = repl_sample::persistent_store::_narrow(obj_ref);

	// Set value and value_2 to 0
	value = 0;
	value_2 = 0;

	ASSERT(!CORBA::is_nil(storage));
}

// Secondary constructor
repl_persist_obj_impl::repl_persist_obj_impl(
    repl_sample::persist_obj_ptr ref,
    repl_sample_prov *server) :
	/* CSTYLED */
	mc_replica_of<repl_sample::persist_obj> (ref),
	my_provider(server)
{
	os::printf("repl_persist_obj_impl::repl_persist_obj_impl(2)\n");

	// Secondaries should not do any 'actual work'.
	// In this case, getting a ref to the persist_obj at this time
	// leads to deadlock (4195278). Solution is to move that
	// 'work' to become_primary.

	// Set value and value_2 to 0
	value = 0;
	value_2 = 0;

}

// unreferenced
void
repl_persist_obj_impl::_unreferenced(unref_t cookie)
{
	os::printf("repl_persist_obj_impl::_unreferenced()\n");
	if (_last_unref(cookie)) {
		ASSERT(my_provider != NULL);

		my_provider->list_lock.lock();
		(void) my_provider->persist_objlist.erase(this);
		my_provider->list_lock.unlock();
		my_provider->dump_obj_list();
		delete this;
	}
}


// Implementation for persisten object IDL Interface
int32_t
repl_persist_obj_impl::get_value(int16_t &nodenum, Environment &)
{
	nodenum = nodeid;
	os::printf("repl_persist_obj_impl::get_value()\n");
	// Simple Mini-transaction
	int32_t rslt;

	lck.lock();
	rslt = _value();
	lck.unlock();

	return (rslt);
}

// Implementation for persisten object IDL Interface
int32_t
repl_persist_obj_impl::get_value_2(int16_t &nodenum, Environment &)
{
	nodenum = nodeid;
	os::printf("repl_persist_obj_impl::get_value_2()\n");
	// Simple Mini-transaction
	int32_t rslt;

	lck.lock();
	rslt = _value_2();
	lck.unlock();

	return (rslt);
}

void
repl_persist_obj_impl::set_value(int32_t val, int16_t &nodenum, Environment &e)
{
	nodenum = nodeid;
	os::printf("repl_persist_obj_impl::set_value()\n");
	// this is idempotent 1-phase mini-transaction, because setting
	// the value twice on the persistent storage has no side-effects
	Environment tmp_env;

	lck.lock();

	// Update persistent storage
	storage->set_value(val, tmp_env);
	if (tmp_env.exception()) {
		// Return an exception to the caller
		CORBA::Exception	*p_ex;
		p_ex = new repl_sample::could_not_update;
		e.exception(p_ex);
		lck.unlock();
		return;
	} else {
		// Set the cached value
		_value(val);

		// Checkpoint to secondaries
		// repl_sample::persist_obj_var obj_ref = get_objref();

		CORBA::type_info_t *typ =
		    repl_sample::persist_obj::_get_type_info(
		    my_provider->r_version.minor_num);
		repl_sample::persist_obj_var obj_ref = get_objref(typ);

		os::printf("repl_persist_obj_impl::set_value\n");
		os::printf("r_version.minor_num %d\n",
			my_provider->r_version.minor_num);
		os::printf("typ %x\n", typ);
		os::printf("val %d\n", val);

		get_checkpoint()->ckpt_set_persist(obj_ref, val, e);
		if (e.exception()) {
			os::panic("Exception during checkpoint");
		}
	}

	lck.unlock();
}

void
repl_persist_obj_impl::set_value_2(int32_t val, int16_t &nodenum,
    Environment &e)
{
	nodenum = nodeid;
	os::printf("repl_persist_obj_impl::set_value_2()\n");
	// this is idempotent 1-phase mini-transaction, because setting
	// the value twice on the persistent storage has no side-effects
	Environment tmp_env;

	lck.lock();

	// Update persistent storage
	storage->set_value(val, tmp_env);
	if (tmp_env.exception()) {
		// Return an exception to the caller
		CORBA::Exception	*p_ex;
		p_ex = new repl_sample::could_not_update;
		e.exception(p_ex);
		lck.unlock();
		return;
	} else {
		// Set the cached value
		_value_2(val);

		// Checkpoint to secondaries
		// repl_sample::persist_obj_var obj_ref = get_objref();

		CORBA::type_info_t *typ =
		    repl_sample::persist_obj::_get_type_info(
		    my_provider->r_version.minor_num);
		repl_sample::persist_obj_var obj_ref = get_objref(typ);

		get_checkpoint()->ckpt_set_persist_2(obj_ref, val, e);
		if (e.exception()) {
			os::panic("Exception during checkpoint");
		}
	}

	lck.unlock();
}

//
// Note: The caller of this method doesn't really keep track of the
//	return value
//
int32_t
repl_persist_obj_impl::inc_value(int16_t &nodenum, Environment &e)
{
	nodenum = nodeid;
	os::printf("repl_persist_obj_impl::inc_value()\n");
	// This is a 2-phase mini-transaction
	// We must first checkpoint our intention to increment the persistent
	// storage, and then either checkpoint failure, or commit success.

	primary_ctx * ctxp = primary_ctx::extract_from(e);
	persist_inc_state * saved_state;
	int32_t rslt;

	// Check for saved state
	if ((saved_state = (persist_inc_state *)ctxp->get_saved_state()) ==
		NULL) {
		// Common case : this is the first time
		lck.lock();

		rslt = _value() + 1;

		// FAULT POINT
		// This generic fault point will trigger a fault prior to the
		// first checkpoint of a 2-phase mini-transaction
		FAULTPT_HA_FRMWK(
			FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_INC_VALUE_2,
			FaultFunctions::generic);

		// Get a CORBA reference
		// repl_sample::persist_obj_var obj_ref = get_objref();

		CORBA::type_info_t *typ =
		    repl_sample::persist_obj::_get_type_info(
		    my_provider->r_version.minor_num);
		repl_sample::persist_obj_var obj_ref = get_objref(typ);

		get_checkpoint()->ckpt_inc_persist_intent(obj_ref, rslt, e);
		if (e.exception()) {
			os::panic("Exception during checkpoint");
		}

		// FAULT POINT
		// This generic fault point will trigger a fault after the
		// first checkpoint of a 2-phase mini-transaction
		FAULTPT_HA_FRMWK(
			FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_INC_VALUE_3,
			FaultFunctions::generic);

		// Attempt to update persist store
		_set_inc_value(rslt, e);

		lck.unlock();
	} else {
		// This must be a re-try
		rslt = saved_state->handle_retry(e);
	}

	return (rslt);
}

//
// Note: The caller of this method doesn't really keep track of the
//	return value
//
int32_t
repl_persist_obj_impl::inc_value_2(int16_t &nodenum, Environment &e)
{
	nodenum = nodeid;
	os::printf("repl_persist_obj_impl::inc_value_2()\n");
	// This is a 2-phase mini-transaction
	// We must first checkpoint our intention to increment the persistent
	// storage, and then either checkpoint failure, or commit success.

	primary_ctx * ctxp = primary_ctx::extract_from(e);
	persist_2_inc_state * saved_state;
	int32_t rslt;

	// Check for saved state
	if ((saved_state = (persist_2_inc_state *)ctxp->get_saved_state()) ==
		NULL) {
		// Common case : this is the first time
		lck.lock();

		rslt = _value_2() + 1;

		// FAULT POINT
		// This generic fault point will trigger a fault prior to the
		// first checkpoint of a 2-phase mini-transaction
		FAULTPT_HA_FRMWK(
			FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_INC_VALUE_2,
			FaultFunctions::generic);

		// Get a CORBA reference
		// repl_sample::persist_obj_var obj_ref = get_objref();

		CORBA::type_info_t *typ =
		    repl_sample::persist_obj::_get_type_info(
		    my_provider->r_version.minor_num);
		repl_sample::persist_obj_var obj_ref = get_objref(typ);

		get_checkpoint()->ckpt_inc_persist_2_intent(obj_ref, rslt, e);
		if (e.exception()) {
			os::panic("Exception during checkpoint "
			    "ckpt_inc_persist_2_intent");
		}

		// FAULT POINT
		// This generic fault point will trigger a fault after the
		// first checkpoint of a 2-phase mini-transaction
		FAULTPT_HA_FRMWK(
			FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_INC_VALUE_3,
			FaultFunctions::generic);

		// Attempt to update persist store
		_set_inc_value_2(rslt, e);

		lck.unlock();
	} else {
		// This must be a re-try
		rslt = saved_state->handle_retry(e);
	}

	return (rslt);
}


// Implementation interface to set the inc value
void
repl_persist_obj_impl::_set_inc_value(int32_t val, Environment &e)
{
	os::printf("repl_persist_obj_impl::_set_inc_value()\n");

	Environment tmp_env;

	// Attempt to update persistent storage
	storage->set_value(val, tmp_env);

	// FAULT POINT
	// This fault point will allow the injection of a fault after the
	// action has taken place.  But before either a second checkpoint
	// or a commit is done.
	FAULTPT_HA_FRMWK(
		FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_SET_INC_VALUE,
		FaultFunctions::generic);

	if (tmp_env.exception()) {
		_ckpt_inc_failure(e);
		if (e.exception()) {
			os::panic("Exception during checkpoint");
		}

		CORBA::Exception	*p_ex;
		p_ex = new repl_sample::could_not_update;
		e.exception(p_ex);
	} else {
		// update local cache
		_value(val);
		add_commit(e);
	}
}

// Implementation interface to set the inc value_2
void
repl_persist_obj_impl::_set_inc_value_2(int32_t val, Environment &e)
{
	os::printf("repl_persist_obj_impl::_set_inc_value_2()\n");

	Environment tmp_env;

	// Attempt to update persistent storage
	storage->set_value(val, tmp_env);

	// FAULT POINT
	// This fault point will allow the injection of a fault after the
	// action has taken place.  But before either a second checkpoint
	// or a commit is done.
	FAULTPT_HA_FRMWK(
		FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_SET_INC_VALUE,
		FaultFunctions::generic);

	if (tmp_env.exception()) {
		_ckpt_inc_2_failure(e);
		if (e.exception()) {
			os::panic("Exception during checkpoint");
		}

		CORBA::Exception	*p_ex;
		p_ex = new repl_sample::could_not_update;
		e.exception(p_ex);
	} else {
		// update local cache
		_value_2(val);
		add_commit(e);
	}
}
// Implementation interface to signal a failed checkpoint
void
repl_persist_obj_impl::_ckpt_inc_failure(Environment &e)
{
	os::printf("repl_persist_obj_impl::_ckpt_inc_failure()\n");

	get_checkpoint()->ckpt_inc_persist_failure(e);
	if (e.exception())
		os::panic("Exception during checkpoint");
}

void
repl_persist_obj_impl::_ckpt_inc_2_failure(Environment &e)
{
	os::printf("repl_persist_obj_impl::_ckpt_inc_2_failure()\n");

	get_checkpoint()->ckpt_inc_persist_2_failure(e);
	if (e.exception())
		os::panic("Exception during checkpoint");
}

repl_sample::ckpt_ptr
repl_persist_obj_impl::get_checkpoint()
{
	os::printf("repl_persist_obj_impl::get_checkpoint()\n");

	return ((repl_sample_prov*)(get_provider()))->
	    get_checkpoint_repl_sample();
}

// Checkpoint accessor function.
repl_sample::ckpt_ptr
repl_sample_prov::get_checkpoint_repl_sample()
{
	os::printf("repl_sample_prov::get_checkpoint_repl_sample()\n");

	return (_ckpt_proxy);
}


// Interface to be invoked during ORB::join_cluster to update the
// local ns of a rejoining node
//
int
repl_sample_prov::get_root_to_local()
{
	os::printf("repl_sample_prov::get_root_to_local()\n");

	Environment e;
	naming::naming_context_ptr gctxp = ns::root_nameserver();
	repl_sample::persistent_store_var store_ref;
	CORBA::Object_var obj_ref;

	obj_ref = gctxp->resolve("ha sample persist store", e);
	CORBA::release(gctxp);
	if (!e.exception() && !CORBA::is_nil(obj_ref)) {
		naming::naming_context_ptr lctxp = ns::local_nameserver();

		store_ref = repl_sample::persistent_store::_narrow(obj_ref);
		lctxp->rebind("ha sample persist store", store_ref, e);
		CORBA::release(lctxp);
		ASSERT(!e.exception());
		return (0);
	}
	ASSERT(0);

	/* NOTREACHED */
	return (1);
}

//
// Implementation interface to get storage ref from the local ns
// Solution for bugid 4195278 - this also depends on stuff
// in files persist_store*
//
void
repl_persist_obj_impl::update_storage(Environment &e)
{
	os::printf("repl_persist_obj_impl::update_storage()\n");

	naming::naming_context_ptr ctxp = ns::local_nameserver();
	CORBA::Object_var obj_ref =
		ctxp->resolve("ha sample persist store", e);

	CORBA::release(ctxp);

	if (e.exception()) {
		e.exception()->print_exception("Getting persist store");
		ASSERT(0);
	}

	// Make this a ptr to the persist store
	storage = repl_sample::persistent_store::_narrow(obj_ref);
	ASSERT(!CORBA::is_nil(storage));
}

// Methods for the persistent state objct
void
persist_inc_state::committed()
{
	// This is called when the commit for a checkpoint is received
	// It is the SUCCESS of a 2-phase minitransaction
	os::printf("persist_inc_state::comitted()\n");

	// We can progress to COMMITTED
	my_progress = COMMITTED;

	// Set the local cache to the checkpointed value
	repl_persist_obj_impl * objp =
		(repl_persist_obj_impl *) obj_ref->_handler()->get_cookie();

	objp->_value(value);
}

// Helper function for retries
int32_t
persist_inc_state::handle_retry(Environment &e)
{
	os::printf("persist_inc_state::handle_retry()\n");

	// Use a local env.
	Environment tmp_env;

	int32_t retval = 0;
	int32_t persist_val = 0;

	// Get the underlying implementation object
	repl_persist_obj_impl * objp =
		(repl_persist_obj_impl *) obj_ref->_handler()->get_cookie();

	ASSERT(objp != NULL);

	switch (my_progress) {
	case STARTED:
		// We need to recover.  We should check and see if the previous
		// attempt at this operation succeeded
		persist_val = objp->_persist_value(tmp_env);
		if (tmp_env.exception()) {
			CORBA::Exception	*p_ex;
			p_ex = new repl_sample::could_not_update;
			e.exception(p_ex);
			objp->_ckpt_inc_failure(e);
			if (e.exception()) {
				os::panic("Exception during checkpoint\n");
			}
		} else {
			// Verify whether the previous operation succeeded
			if (persist_val == value) {
				// Operation succeded
				// Set the underlying value (local cache)
				objp->_value(value);
				retval = value;
			} else {
				// Retry the update
				objp->_set_inc_value(value, e);
				retval = value;
			}
		} // Exception
		break;
	case COMMITTED:
		// Just return what we had saved
		retval = value;
		break;
	case FAILED:
		// Return some exception
		CORBA::Exception	*p_ex;
		p_ex = new repl_sample::could_not_update;
		e.exception(p_ex);
		break;
	} // SWITCH

	return (retval);
}

// Methods for the persistent state objct
void
persist_2_inc_state::committed()
{
	// This is called when the commit for a checkpoint is received
	// It is the SUCCESS of a 2-phase minitransaction
	os::printf("persist_2_inc_state::comitted()\n");

	// We can progress to COMMITTED
	my_progress = COMMITTED;

	// Set the local cache to the checkpointed value
	repl_persist_obj_impl * objp =
		(repl_persist_obj_impl *) obj_ref->_handler()->get_cookie();

	objp->_value_2(value_2);
}

// Helper function for retries
int32_t
persist_2_inc_state::handle_retry(Environment &e)
{
	os::printf("persist_2_inc_state::handle_retry()\n");

	// Use a local env.
	Environment tmp_env;

	int32_t retval = 0;
	int32_t persist_val_2 = 0;

	// Get the underlying implementation object
	repl_persist_obj_impl * objp =
		(repl_persist_obj_impl *) obj_ref->_handler()->get_cookie();

	ASSERT(objp != NULL);

	switch (my_progress) {
	case STARTED:
		// We need to recover.  We should check and see if the previous
		// attempt at this operation succeeded
		persist_val_2 = objp->_persist_value(tmp_env);
		if (tmp_env.exception()) {
			CORBA::Exception	*p_ex;
			p_ex = new repl_sample::could_not_update;
			e.exception(p_ex);
			objp->_ckpt_inc_2_failure(e);
			if (e.exception()) {
				os::panic("Exception during checkpoint\n");
			}
		} else {
			// Verify whether the previous operation succeeded
			if (persist_val_2 == value_2) {
				// Operation succeded
				// Set the underlying value (local cache)
				objp->_value_2(value_2);
				retval = value_2;
			} else {
				// Retry the update
				objp->_set_inc_value_2(value_2, e);
				retval = value_2;
			}
		} // Exception
		break;
	case COMMITTED:
		// Just return what we had saved
		retval = value_2;
		break;
	case FAILED:
		// Return some exception
		CORBA::Exception	*p_ex;
		p_ex = new repl_sample::could_not_update;
		e.exception(p_ex);
		break;
	} // SWITCH

	return (retval);
}

// unreferenced
void
#ifdef DEBUG
persistent_store_impl::_unreferenced(unref_t cookie)
#else
persistent_store_impl::_unreferenced(unref_t)
#endif
{
	os::printf("persistent_store_impl::_unreferenced()\n");
	ASSERT(_last_unref(cookie));

	delete this;
}


// Methods of the persistent store object
#ifdef FAULT_HA_FRMWK
void
persistent_store_impl::set_value(int32_t val, Environment &e)
{
	os::printf("persistent_store_impl::set_value()\n");

	if (fault_triggered(
	    FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_PERSIST_STORE_SET_VALUE,
	    NULL, NULL)) {

		// This fault will set some exception and return
		CORBA::Exception	*p_ex;
		p_ex = new repl_sample::could_not_update;

		// Set the exception
		e.exception(p_ex);
		return;
	} // Fault Point
	value = val;
}
#else
void
persistent_store_impl::set_value(int32_t val, Environment &)
{
	os::printf("persistent_store_impl::set_value()\n");

	value = val;
}
#endif // FAULT_HA_FRMWK


/*

#ifdef FAULT_HA_FRMWK
void
persistent_store_impl::set_value_2(int32_t val, Environment &e)
{
	os::printf("persistent_store_impl::set_value_2()\n");

	if (fault_triggered(
	    FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_PERSIST_STORE_SET_VALUE,
	    NULL, NULL)) {

		// This fault will set some exception and return
		CORBA::Exception	*p_ex;
		p_ex = new repl_sample::could_not_update;

		// Set the exception
		e.exception(p_ex);
		return;
	} // Fault Point
	value_2 = val;
}
#else
void
persistent_store_impl::set_value_2(int32_t val, Environment &)
{
	os::printf("persistent_store_impl::set_value_2()\n");

	value_2 = val;
}

*/

repl_sample_callback::repl_sample_callback(repl_sample_prov *prov) :
    my_provider(prov),
    bs_cl_cb1(0),
    bs_cl_cb2(0),
    bs_cl_cb3(0),
    bs_cl_cb4(0),
    bs_cl_cb5(0),
    cl_cb1(0),
    cl_cb2(0),
    cl_cb3(0),
    cl_cb4(0),
    cl_cb5(0)
{
	os::printf("New repl_sample_callback %p\n", this);

	btstrp_cl_1_rv.major_num = 1;
	btstrp_cl_1_rv.minor_num = 0;
	btstrp_cl_2_rv.major_num = 1;
	btstrp_cl_2_rv.minor_num = 0;
	btstrp_cl_3_rv.major_num = 1;
	btstrp_cl_3_rv.minor_num = 0;
	btstrp_cl_4_rv.major_num = 1;
	btstrp_cl_4_rv.minor_num = 0;
	btstrp_cl_5_rv.major_num = 1;
	btstrp_cl_5_rv.minor_num = 0;
	non_btstrp_cl_rv.major_num = 1;
	non_btstrp_cl_rv.minor_num = 0;
	repl_sample_rv.major_num = 1;
	repl_sample_rv.minor_num = 0;
}

repl_sample_callback::~repl_sample_callback()
{
	os::printf("Delete repl_sample_callback %p\n", this);

	my_provider = NULL;
/*
	ASSERT(bs_cl_cb1 == 1);
	ASSERT(bs_cl_cb2 == 1);
	ASSERT(bs_cl_cb3 == 1);
	ASSERT(bs_cl_cb4 == 1);
	ASSERT(bs_cl_cb5 == 1);
	ASSERT(cl_cb1 == 1);
	ASSERT(cl_cb2 == 1);
	ASSERT(cl_cb3 == 1);
	ASSERT(cl_cb4 == 1);
	ASSERT(cl_cb5 == 1);
*/
}

void
#ifdef DEBUG
repl_sample_callback::_unreferenced(unref_t cookie)
#else
repl_sample_callback::_unreferenced(unref_t)
#endif
{
	os::printf("repl_sample_callback::_unreferenced() %p\n", this);

	// This object does not support multiple 0->1 transitions
	ASSERT(_last_unref(cookie));

	delete this;
}

void
repl_sample_callback::do_callback(const char *ucc_name,
    const version_manager::vp_version_t &new_version,
    Environment &)
{

	os::printf("repl_sample_callback::do_callback()\n");
	os::printf("New version is %d.%d\n",
		new_version.major_num, new_version.minor_num);

	// Note: this assert is correct if there is only one
	// ucc associated with repl_sample.
	// ASSERT(os::strcmp(ucc_name, "repl_sample") == 0);

	if (os::strcmp(ucc_name, "repl_sample") == 0) {
		os::printf("repl_sample callback received..\n");
		if (repl_sample_rv.major_num == new_version.major_num &&
		    repl_sample_rv.minor_num == new_version.minor_num) {
			os::printf("XXX Duplicate callback XXX\n");
			os::printf("repl_sample_rv running version stays at "
			    "%d.%d\n", repl_sample_rv.major_num,
			    repl_sample_rv.minor_num);
		} else if (repl_sample_rv.major_num <= new_version.major_num &&
		    repl_sample_rv.minor_num <= new_version.minor_num) {
			version_lock.lock();
			repl_sample_rv.major_num = new_version.major_num;
			repl_sample_rv.minor_num = new_version.minor_num;
			os::printf("repl_sample previous version = %d.%d\n",
			    repl_sample_rv.major_num, repl_sample_rv.minor_num);
			os::printf("repl_sample new version = %d.%d\n",
			    new_version.major_num, new_version.minor_num);
			//
			// XXX Do the actual callback
			//
			my_provider->upgrade_callback(new_version);
			version_lock.unlock();

			os::printf("repl_sample_rv running version upgrade to "
			    "%d.%d\n", repl_sample_rv.major_num,
			    repl_sample_rv.minor_num);
		} else {
		    // XXX New version should not be less than running version
		    ASSERT(0);
		}
	}


	//
	// Bootstrap-cluster vp upgrade callback accounting
	//
	if (os::strcmp(ucc_name, "btstrp_cl_vp_1") == 0) {
		os::printf("btstrp_cl_vp_1 callback received..\n");
		if (btstrp_cl_1_rv.major_num == new_version.major_num &&
		    btstrp_cl_1_rv.minor_num == new_version.minor_num) {
			os::printf("XXX Duplicate callback XXX\n");
			os::printf("btstrp_cl_vp_1 running version stays at "
			    "%d.%d\n", btstrp_cl_1_rv.major_num,
			    btstrp_cl_1_rv.minor_num);
		} else if (btstrp_cl_1_rv.major_num <= new_version.major_num &&
		    btstrp_cl_1_rv.minor_num <= new_version.minor_num) {
			version_lock.lock();
			btstrp_cl_1_rv.major_num = new_version.major_num;
			btstrp_cl_1_rv.minor_num = new_version.minor_num;
			version_lock.unlock();
			os::printf("btstrp_cl_vp_1 running version upgrade to "
			    "%d.%d\n", btstrp_cl_1_rv.major_num,
			    btstrp_cl_1_rv.minor_num);

			bs_cl_cb1 = 1;
			os::printf("bs_cl_cb1=%d, bs_cl_cb2=%d, bs_cl_cb3=%d, "
			    "bs_cl_cb4=%d, bs_cl_cb5=%d\n", bs_cl_cb1,
			    bs_cl_cb2, bs_cl_cb3, bs_cl_cb4, bs_cl_cb5);
/*
			char **tmp1 = new char *[1];
			tmp1[0] = os::strdup("btstrp_cl_vp_1");
			version_manager::string_seq_t ucc_1(1, 1, tmp1, true);
			os::printf("Unregister UCC ... btstrp_cl_vp_1\n");
			vm_lca_impl::the().unregister_uccs(ucc_1, env);
			os::printf("SUCCESS: unregistered UCC ... "
				"btstrp_cl_vp_1\n");
*/
		} else {
		    // XXX New version should not be less than running version
		    ASSERT(0);
		}
	}

	if (os::strcmp(ucc_name, "btstrp_cl_vp_2") == 0) {
		os::printf("btstrp_cl_vp_2 callback received..\n");
		if (btstrp_cl_2_rv.major_num == new_version.major_num &&
		    btstrp_cl_2_rv.minor_num == new_version.minor_num) {
			os::printf("XXX Duplicate callback XXX\n");
			os::printf("btstrp_cl_vp_2 running version stays at "
			    "%d.%d\n", btstrp_cl_2_rv.major_num,
			    btstrp_cl_2_rv.minor_num);
		} else if (btstrp_cl_2_rv.major_num <= new_version.major_num &&
		    btstrp_cl_2_rv.minor_num <= new_version.minor_num) {
			version_lock.lock();
			btstrp_cl_2_rv.major_num = new_version.major_num;
			btstrp_cl_2_rv.minor_num = new_version.minor_num;
			version_lock.unlock();
			os::printf("btstrp_cl_vp_2 running version upgrade to "
			    "%d.%d\n", btstrp_cl_2_rv.major_num,
			    btstrp_cl_2_rv.minor_num);

			bs_cl_cb2 = 1;
			os::printf("bs_cl_cb1=%d, bs_cl_cb2=%d, bs_cl_cb3=%d, "
			    "bs_cl_cb4=%d, bs_cl_cb5=%d\n", bs_cl_cb1,
			    bs_cl_cb2, bs_cl_cb3, bs_cl_cb4, bs_cl_cb5);
/*
			char **tmp2 = new char *[1];
			tmp2[0] = os::strdup("btstrp_cl_vp_2");
			version_manager::string_seq_t ucc_2(1, 1, tmp2, true);
			os::printf("Unregister UCC ... btstrp_cl_vp_2\n");
			// vm_lca_impl::the().unregister_uccs(ucc_2);
			os::printf("SUCCESS: unregistered UCC ... "
				"btstrp_cl_vp_2\n");
*/
		} else {
		    // XXX New version should not be less than running version
		    ASSERT(0);
		}
	}

	if (os::strcmp(ucc_name, "btstrp_cl_vp_3") == 0) {
		os::printf("btstrp_cl_vp_3 callback received..\n");
		if (btstrp_cl_3_rv.major_num == new_version.major_num &&
		    btstrp_cl_3_rv.minor_num == new_version.minor_num) {
			os::printf("XXX Duplicate callback XXX\n");
			os::printf("btstrp_cl_vp_3 running version stays at "
			    "%d.%d\n", btstrp_cl_3_rv.major_num,
			    btstrp_cl_3_rv.minor_num);
		} else if (btstrp_cl_3_rv.major_num <= new_version.major_num &&
		    btstrp_cl_3_rv.minor_num <= new_version.minor_num) {
			version_lock.lock();
			btstrp_cl_3_rv.major_num = new_version.major_num;
			btstrp_cl_3_rv.minor_num = new_version.minor_num;
			version_lock.unlock();
			os::printf("btstrp_cl_vp_3 running version upgrade to "
			    "%d.%d\n", btstrp_cl_3_rv.major_num,
			    btstrp_cl_3_rv.minor_num);

			bs_cl_cb3 = 1;
			os::printf("bs_cl_cb1=%d, bs_cl_cb2=%d, bs_cl_cb3=%d, "
			    "bs_cl_cb4=%d, bs_cl_cb5=%d\n", bs_cl_cb1,
			    bs_cl_cb2, bs_cl_cb3, bs_cl_cb4, bs_cl_cb5);
/*
			char **tmp3 = new char *[1];
			tmp3[0] = os::strdup("btstrp_cl_vp_3");
			version_manager::string_seq_t ucc_3(1, 1, tmp3, true);
			os::printf("Unregister UCC ... btstrp_cl_vp_3\n");
			// vm_lca_impl::the().unregister_uccs(ucc_3);
			os::printf("SUCCESS: unregistered UCC ... "
				"btstrp_cl_vp_3\n");
*/
		} else {
		    // XXX New version should not be less than running version
		    ASSERT(0);
		}
	}

	if (os::strcmp(ucc_name, "btstrp_cl_vp_4") == 0) {
		os::printf("btstrp_cl_vp_4 callback received..\n");
		if (btstrp_cl_4_rv.major_num == new_version.major_num &&
		    btstrp_cl_4_rv.minor_num == new_version.minor_num) {
			os::printf("XXX Duplicate callback XXX\n");
			os::printf("btstrp_cl_vp_4 running version stays at "
			    "%d.%d\n", btstrp_cl_4_rv.major_num,
			    btstrp_cl_4_rv.minor_num);
		} else if (btstrp_cl_4_rv.major_num <= new_version.major_num &&
		    btstrp_cl_4_rv.minor_num <= new_version.minor_num) {
			version_lock.lock();
			btstrp_cl_4_rv.major_num = new_version.major_num;
			btstrp_cl_4_rv.minor_num = new_version.minor_num;
			version_lock.unlock();
			os::printf("btstrp_cl_vp_4 running version upgrade to "
			    "%d.%d\n", btstrp_cl_4_rv.major_num,
			    btstrp_cl_4_rv.minor_num);

			bs_cl_cb4 = 1;
			os::printf("bs_cl_cb1=%d, bs_cl_cb2=%d, bs_cl_cb3=%d, "
			    "bs_cl_cb4=%d, bs_cl_cb5=%d\n", bs_cl_cb1,
			    bs_cl_cb2, bs_cl_cb3, bs_cl_cb4, bs_cl_cb5);
/*
			char **tmp4 = new char *[1];
			tmp4[0] = os::strdup("btstrp_cl_vp_4");
			version_manager::string_seq_t ucc_4(1, 1, tmp4, true);
			os::printf("Unregister UCC ... btstrp_cl_vp_4\n");
			// vm_lca_impl::the().unregister_uccs(ucc_4);
			os::printf("SUCCESS: unregistered UCC ... "
				"btstrp_cl_vp_4\n");
*/
		} else {
		    // XXX New version should not be less than running version
		    ASSERT(0);
		}
	}

	if (os::strcmp(ucc_name, "btstrp_cl_vp_5") == 0) {
		os::printf("btstrp_cl_vp_5 callback received..\n");
		if (btstrp_cl_5_rv.major_num == new_version.major_num &&
		    btstrp_cl_5_rv.minor_num == new_version.minor_num) {
			os::printf("XXX Duplicate callback XXX\n");
			os::printf("btstrp_cl_vp_5 running version stays at "
			    "%d.%d\n", btstrp_cl_5_rv.major_num,
			    btstrp_cl_5_rv.minor_num);
		} else if (btstrp_cl_5_rv.major_num <= new_version.major_num &&
		    btstrp_cl_5_rv.minor_num <= new_version.minor_num) {
			version_lock.lock();
			btstrp_cl_5_rv.major_num = new_version.major_num;
			btstrp_cl_5_rv.minor_num = new_version.minor_num;
			version_lock.unlock();
			os::printf("btstrp_cl_vp_5 running version upgrade to "
			    "%d.%d\n", btstrp_cl_5_rv.major_num,
			    btstrp_cl_5_rv.minor_num);

			bs_cl_cb5 = 1;
			os::printf("bs_cl_cb1=%d, bs_cl_cb2=%d, bs_cl_cb3=%d, "
			    "bs_cl_cb4=%d, bs_cl_cb5=%d\n", bs_cl_cb1,
			    bs_cl_cb2, bs_cl_cb3, bs_cl_cb4, bs_cl_cb5);
/*
			char **tmp5 = new char *[1];
			tmp5[0] = os::strdup("btstrp_cl_vp_5");
			version_manager::string_seq_t ucc_5(1, 1, tmp5, true);
			os::printf("Unregister UCC ... btstrp_cl_vp_5\n");
			// vm_lca_impl::the().unregister_uccs(ucc_5);
			os::printf("SUCCESS: unregistered UCC ... "
				"btstrp_cl_vp_5\n");
*/
		} else {
		    // XXX New version should not be less than running version
		    ASSERT(0);
		}
	}


	//
	// Cluster vp upgrade callback accounting
	//
	if (os::strcmp(ucc_name, "cluster_vp_1") == 0) {
		os::printf("cluster_vp_1 callback received..\n");
		if (non_btstrp_cl_rv.major_num == new_version.major_num &&
		    non_btstrp_cl_rv.minor_num == new_version.minor_num) {
			os::printf("XXX Duplicate callback XXX\n");
			os::printf("non_btstrp_cl_rv running version stays at "
			    "%d.%d\n", non_btstrp_cl_rv.major_num,
			    non_btstrp_cl_rv.minor_num);
		} else if
		    (non_btstrp_cl_rv.major_num <= new_version.major_num &&
		    non_btstrp_cl_rv.minor_num <= new_version.minor_num) {
			cl_cb1 = 1;
			os::printf("cl_cb1=%d, cl_cb2=%d, cl_cb3=%d, "
			    "cl_cb4=%d, cl_cb5=%d\n",
			    cl_cb1, cl_cb2, cl_cb3, cl_cb4, cl_cb5);
		} else {
		    // XXX New version should not be less than running version
		    ASSERT(0);
		}
	}

	if (os::strcmp(ucc_name, "cluster_vp_2") == 0) {
		os::printf("cluster_vp_2 callback received..\n");
		if (non_btstrp_cl_rv.major_num == new_version.major_num &&
		    non_btstrp_cl_rv.minor_num == new_version.minor_num) {
			os::printf("XXX Duplicate callback XXX\n");
			os::printf("non_btstrp_cl_rv running version stays at "
			    "%d.%d\n", non_btstrp_cl_rv.major_num,
			    non_btstrp_cl_rv.minor_num);
		} else if
		    (non_btstrp_cl_rv.major_num <= new_version.major_num &&
		    non_btstrp_cl_rv.minor_num <= new_version.minor_num) {
			cl_cb2 = 1;
			os::printf("cl_cb1=%d, cl_cb2=%d, cl_cb3=%d, "
			    "cl_cb4=%d, cl_cb5=%d\n",
			    cl_cb1, cl_cb2, cl_cb3, cl_cb4, cl_cb5);
		} else {
		    // XXX New version should not be less than running version
		    ASSERT(0);
		}
	}

	if (os::strcmp(ucc_name, "cluster_vp_3") == 0) {
		os::printf("cluster_vp_3 callback received..\n");
		if (non_btstrp_cl_rv.major_num == new_version.major_num &&
		    non_btstrp_cl_rv.minor_num == new_version.minor_num) {
			os::printf("XXX Duplicate callback XXX\n");
			os::printf("non_btstrp_cl_rv running version stays at "
			    "%d.%d\n", non_btstrp_cl_rv.major_num,
			    non_btstrp_cl_rv.minor_num);
		} else if
		    (non_btstrp_cl_rv.major_num <= new_version.major_num &&
		    non_btstrp_cl_rv.minor_num <= new_version.minor_num) {
			cl_cb3 = 1;
			os::printf("cl_cb1=%d, cl_cb2=%d, cl_cb3=%d, "
			    "cl_cb4=%d, cl_cb5=%d\n",
			    cl_cb1, cl_cb2, cl_cb3, cl_cb4, cl_cb5);
		} else {
		    // XXX New version should not be less than running version
		    ASSERT(0);
		}
	}

	if (os::strcmp(ucc_name, "cluster_vp_4") == 0) {
		os::printf("cluster_vp_4 callback received..\n");
		if (non_btstrp_cl_rv.major_num == new_version.major_num &&
		    non_btstrp_cl_rv.minor_num == new_version.minor_num) {
			os::printf("XXX Duplicate callback XXX\n");
			os::printf("non_btstrp_cl_rv running version stays at "
			    "%d.%d\n", non_btstrp_cl_rv.major_num,
			    non_btstrp_cl_rv.minor_num);
		} else if
		    (non_btstrp_cl_rv.major_num <= new_version.major_num &&
		    non_btstrp_cl_rv.minor_num <= new_version.minor_num) {
			cl_cb4 = 1;
			os::printf("cl_cb1=%d, cl_cb2=%d, cl_cb3=%d, "
			    "cl_cb4=%d, cl_cb5=%d\n",
			    cl_cb1, cl_cb2, cl_cb3, cl_cb4, cl_cb5);
		} else {
		    // XXX New version should not be less than running version
		    ASSERT(0);
		}
	}


	if (os::strcmp(ucc_name, "cluster_vp_5") == 0) {
		os::printf("cluster_vp_5 callback received..\n");
		if (non_btstrp_cl_rv.major_num == new_version.major_num &&
		    non_btstrp_cl_rv.minor_num == new_version.minor_num) {
			os::printf("XXX Duplicate callback XXX\n");
			os::printf("non_btstrp_cl_rv running version stays at "
			    "%d.%d\n", non_btstrp_cl_rv.major_num,
			    non_btstrp_cl_rv.minor_num);
		} else if
		    (non_btstrp_cl_rv.major_num <= new_version.major_num &&
		    non_btstrp_cl_rv.minor_num <= new_version.minor_num) {
			version_lock.lock();
			non_btstrp_cl_rv.major_num = new_version.major_num;
			non_btstrp_cl_rv.minor_num = new_version.minor_num;
			version_lock.unlock();

			os::printf("non_btstrp_cl_rv running version upgrade "
			    "to %d.%d\n", non_btstrp_cl_rv.major_num,
			    non_btstrp_cl_rv.minor_num);

			cl_cb5 = 1;
			os::printf("cl_cb1=%d, cl_cb2=%d, cl_cb3=%d, "
			    "cl_cb4=%d, cl_cb5=%d\n",
			    cl_cb1, cl_cb2, cl_cb3, cl_cb4, cl_cb5);
		} else {
		    // XXX New version should not be less than running version
		    ASSERT(0);
		}
	}


}

void
repl_sample_prov::dump_obj_list()
{
	SList<repl_value_obj_impl>::ListIterator v_iter(value_objlist);
	SList<repl_persist_obj_impl>::ListIterator p_iter(persist_objlist);

	repl_value_obj_impl *v_objp;
	repl_persist_obj_impl *p_objp;

	list_lock.lock();

	os::printf("Value Objlist: %d, Persist Objlist: %d\n",
		    value_objlist.count(), persist_objlist.count());

	os::printf("Dump value_objlist ...\n");
	while ((v_objp = v_iter.get_current()) != NULL) {
		os::printf("v_objp = %p\n", v_objp);
		v_iter.advance();
	}

	os::printf("Dump persist_objlist ...\n");
	while ((p_objp = p_iter.get_current()) != NULL) {
		os::printf("p_objp = %p\n", p_objp);
		p_iter.advance();
	}

	list_lock.unlock();
}

void
repl_sample_prov::vm_exception(Environment &e)
{
		if (e.exception()) {
			if (version_manager::not_found::
			    _exnarrow(e.exception())) {
				os::printf("Error: versioned protocol not "
				    "found\n");
			} else if (version_manager::wrong_mode::
			    _exnarrow(e.exception())) {
				os::printf("Error: versioned protocol is "
				    "node-pair mode, use other flag\n");
			} else if (version_manager::too_early::
			    _exnarrow(e.exception())) {
				os::printf("Error: Running version for not "
				    "yet set\n");
			} else if (version_manager::old_incarnation::
			    _exnarrow(e.exception())) {
				os::printf("Error: old_incarnation returned\n");
			} else if (version_manager::invalid_ucc::
			    _exnarrow(e.exception())) {
				os::printf("Error: Invalid ucc defined\n");
			} else if (version_manager::unknown_vp::
			    _exnarrow(e.exception())) {
				os::printf("Error: unknown vp defined\n");
			} else if (version_manager::ucc_redefined::
			    _exnarrow(e.exception())) {
				os::printf("Error: ucc_redefined\n");
			} else if (version_manager::invalid_ucc_dependencies::
			    _exnarrow(e.exception())) {
				os::printf("Error: invalid_ucc_dependencies\n");
//			} else if (version_manager::retry_needed::
//			    _exnarrow(e.exception())) {
//				os::printf("Error: retry_needed\n");
			} else {
				os::printf("Error: return system exception\n");
			}
		}
		e.clear();
}
