/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)entry_basic_v1.cc	1.6	08/05/20 SMI"

#include	<orbtest/vm_upgd_cb/repl_sample_clnt_v1.h>
#include	<sys/os.h>
#include	<orb/invo/common.h>
#include	<orbtest/rm_probe/rm_snap.h>

//
// Basic 1-A Test case
//
int
basic_1_a(int, char *[])
{
	os::printf("*** Basic Mini-Transaction Scenario \n");

#ifdef _FAULT_INJECTION
	// Figure out which node we'd like to fault (Primary node)
	rm_snap		_rma_snapshot;
	nodeid_t	primary_nodeid;
	Environment	e;

	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_primary(primary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get primary nodeid\n");
		return (1);
	}

	os::printf("    Primary is node: %d\n", primary_nodeid);

	return (simple_mini_1(
		NULL, NULL,
		&primary_nodeid,
		(int)sizeof (primary_nodeid),
		e));
#else
	return (no_fault_injection());
#endif
}
