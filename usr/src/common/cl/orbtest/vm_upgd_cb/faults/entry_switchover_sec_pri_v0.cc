/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)entry_switchover_sec_pri_v0.cc	1.6	08/05/20 SMI"

#include	<orbtest/vm_upgd_cb/repl_sample_clnt_v0.h>
#include	<sys/os.h>
#include	<orbtest/rm_probe/rm_snap.h>
#include	<orb/fault/fault_injection.h>

#include	<orbtest/vm_upgd_cb/faults/entry_common.h>

//
// Switchover helper
//
#ifdef _FAULT_INJECTION
int
switchover_helper(FAULT_FPTR(f_on), FAULT_FPTR(f_off), nodeid_t fault_node,
    bool wait_after_switch, bool should_switch);
#else
int
switchover_helper(void *, void *, nodeid_t, bool, bool);
#endif // _FAULT_INJECTION

//
// Switchover Sec -> Pri Test Case 1-A
//
int
switchover_sec_pri_1_a(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario \n");

#ifdef _FAULT_INJECTION
	// No Fault
	// Just a simple switchover
	return (switchover_helper(NULL, NULL, 0, false, true));
#else
	return (no_fault_injection());
#endif
}

//
// Switchover helper
//
#ifdef _FAULT_INJECTION
int
switchover_helper(FAULT_FPTR(f_on), FAULT_FPTR(f_off), nodeid_t fault_node,
    bool wait_after_switch, bool should_switch)
#else
int
switchover_helper(void *, void *, nodeid_t, bool, bool)
#endif // _FAULT_INJECTION
{
#ifdef _FAULT_INJECTION
	// Figure out which node we'd like to fault (Primary node)
	Environment	e;
	int		rslt;
	nodeid_t	start_primary;
	nodeid_t	end_primary;

	// Make sure that we're starting with a live service
	if (get_state("ha_sample_server") != replica::S_UP) {
		os::printf("+++ FAIL: Service \"ha_sample_server\" is not in"
		    " S_UP state (%d)\n", get_state("ha_sample_server"));
		return (1);
	}

	// Get the old primary
	if ((start_primary = get_primary("ha_sample_server")) == 0) {
		return (1);
	}

	os::printf("    Starting primary is node: %d\n", start_primary);
	os::printf("    Will fault node: %d\n", fault_node);

	// Popultate the fault arguments
	FaultFunctions::ha_dep_reboot_arg_t	reboot_args;
	reboot_args.sid		= get_sid("ha_sample_server");
	reboot_args.op		= FaultFunctions::WAIT_FOR_UNKNOWN;
	reboot_args.nodeid	= fault_node;

	rslt = change_sec_state(
	    f_on, f_off,
	    &reboot_args,
	    (int)sizeof (reboot_args),
	    "ha_sample_server",
	    replica::SC_SET_PRIMARY,
	    e);

	if (e.exception()) {
		if ((replica::failed_to_switchover::_exnarrow(e.exception())
		    != NULL) && !should_switch) {
			rslt = 0;
			e.clear();
		} else {
			e.exception()->print_exception("change_sec_state:");
			os::printf("+++ FAIL: Exception was not expected\n");
			return (1);
		}
	}

	if (rslt != 0) {
		os::printf("+++ FAIL: Error during switchover "
		    "(returned non-zero)");
		return (1);
	}

	if (wait_after_switch) {
		// Sleep for some time to give CMM time to reconfigure
		// 1,000,000 usecs in 1 sec
		// How about 30 seconds
		os::usecsleep((long)30 * 1000000);
	}

	// Make sure that we're ending with a live service
	if (get_state("ha_sample_server") != replica::S_UP) {
		os::printf("+++ FAIL: Service \"ha_sample_server\" is not in"
		    " S_UP state (%d)\n", get_state("ha_sample_server"));
		return (1);
	}

	if ((end_primary = get_primary("ha_sample_server")) == 0) {
		return (1);
	}

	os::printf("    Ending primary is: %d\n", end_primary);

	// Compare (they should be different)
	if (should_switch) {
		if (start_primary == end_primary) {
			os::printf("+++ FAIL: Switchover did not occur!?\n");
			return (1);
		}
	} else {
		if (start_primary != end_primary) {
			os::printf("+++ FAIL: Primary Prov's changed nodes "
			    "(they shouldn't have!)\n");
			return (1);
		}
	}

	os::printf("--- PASS: Switchover was completed.\n");
	return (0);
#else
	return (no_fault_injection());
#endif
}
