/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)entry_cb_registration_v1.cc	1.6	08/05/20 SMI"

// #include	<orbtest/vm_upgd_cb/repl_sample_clnt_v1.h>
#include	<sys/os.h>
#include	<orb/invo/common.h>
#include	<orbtest/rm_probe/rm_snap.h>
#include	<orb/infrastructure/orb_conf.h>
#include	<orb/fault/fault_injection.h>

#include	<orbtest/vm_upgd_cb/faults/entry_cb_registration_v1.h>
#include	<orbtest/vm_upgd_cb/faults/fault_cb_registration.h>


//
// Basic 1-A Test case
//
int
lca_register_cl_uccs_1(int argc, char **argv)
{
	os::printf("*** LCA register cl uccs fault 1 ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc > 2) {
		print_error_message(argv[0]);
		return (rslt);
	}

	if (argc == 1) {
		fault_node = orb_conf::local_nodeid();
	} else if (os::strcmp(argv[1], "rm_primary") == 0) {
		fault_node = FaultFunctions::rm_primary_node();
	} else {
		print_error_message(argv[0]);
		return (rslt);
	}

	os::printf("    Fault node: %d\n", fault_node);

	rslt = lca_register_cl_uccs(
		fault_lca_register_cl_uccs_1_on,
		fault_node, EVEN);

	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

int
lca_register_cl_uccs_2(int argc, char **argv)
{
	os::printf("*** LCA register cl uccs fault 2 ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc > 2) {
		print_error_message(argv[0]);
		return (rslt);
	}

	if (argc == 1) {
		fault_node = orb_conf::local_nodeid();
	} else if (os::strcmp(argv[1], "rm_primary") == 0) {
		fault_node = FaultFunctions::rm_primary_node();
	} else {
		print_error_message(argv[0]);
		return (rslt);
	}

	os::printf("    Fault node: %d\n", fault_node);

	rslt = lca_register_cl_uccs(
		fault_lca_register_cl_uccs_2_on,
		fault_node, ALWAYS);

	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

int
lca_register_cl_uccs_3(int argc, char **argv)
{
	os::printf("*** LCA register cl uccs fault 3 ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc > 2) {
		print_error_message(argv[0]);
		return (rslt);
	}

	if (argc == 1) {
		fault_node = orb_conf::local_nodeid();
	} else if (os::strcmp(argv[1], "rm_primary") == 0) {
		fault_node = FaultFunctions::rm_primary_node();
	} else {
		print_error_message(argv[0]);
		return (rslt);
	}

	os::printf("    Fault node: %d\n", fault_node);

	rslt = lca_register_cl_uccs(
		fault_lca_register_cl_uccs_3_on,
		fault_node, ALWAYS);

	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

int
lca_register_cl_uccs(
	FAULT_FPTR(on_fp),
	nodeid_t	fault_node,
	int		when_to_trigger)
{
	FaultFunctions::wait_for_arg_t	reboot_args;
	reboot_args.op		= FaultFunctions::WAIT_FOR_UNKNOWN;
	reboot_args.nodeid 	= fault_node;


	// Do the fault setup
	if (on_fp != NULL) {
		if (on_fp(&reboot_args, (int)sizeof (reboot_args),
		    when_to_trigger) != 0) {
			os::printf("    WARNING: Arming faults failed\n");
		}
		// Comment out later
		os::printf("    WARNING: Arming faults succeeded\n");
	} else {
		os::printf("    No Faults armed.\n");
	}

	return (0);
}
