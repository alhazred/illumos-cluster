/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)entry_one_phase_idempotent_v1.cc	1.6	08/05/20 SMI"

#include	<orbtest/vm_upgd_cb/repl_sample_clnt_v1.h>
#include	<sys/os.h>
#include	<orb/invo/common.h>
#include	<orbtest/rm_probe/rm_snap.h>

//
// One-Phase Idempotent Test Case 1-A
//
int
one_phase_idempotent_1_a(int, char *[])
{
	// We're just checking that a regular invocation with
	// checkpointing can succeed
	os::printf("*** One-Phase Idempotent Mini-Transaction Scenario \n");
	Environment	e;

	return (one_phase_mini_1(NULL, NULL, NULL, 0, e, "ha_sample_server",
		false));
}
