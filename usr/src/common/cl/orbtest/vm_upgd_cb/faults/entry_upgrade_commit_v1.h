/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ENTRY_UPGRADE_COMMIT_V1_H
#define	_ENTRY_UPGRADE_COMMIT_V1_H

#pragma ident	"@(#)entry_upgrade_commit_v1.h	1.6	08/05/20 SMI"

#include <orbtest/vm_upgd_cb/faults/entry_common.h>

// Macro for the function to turn on fault injection
#define	FAULT_FPTR(func)	int (*func) (void *, int, int)


int lca_do_callback_task_1(int, char *[]);
int lca_do_callback_task_2(int, char *[]);
int lca_do_callback_task_3(int, char *[]);
int lca_do_callback_task_4(int, char *[]);
int lca_do_callback_task_5(int, char *[]);
int lca_do_callback_task_6(int, char *[]);
int lca_do_callback_task_7(int, char *[]);
int lca_do_callback_task_8(int, char *[]);
int lca_do_callback_task_9(int, char *[]);
int lca_do_callback_task_10(int, char *[]);
// int lca_do_callback_task_11(int, char *[]);

int cb_coord_upgd_commit_1(int, char *[]);
int cb_coord_upgd_commit_2(int, char *[]);
int cb_coord_upgd_commit_3(int, char *[]);

int upgd_commit_task_exec_1(int, char *[]);
int upgd_commit_task_exec_2(int, char *[]);
int upgd_commit_task_exec_3(int, char *[]);
int upgd_commit_task_exec_4(int, char *[]);
int upgd_commit_task_exec_5(int, char *[]);
int upgd_commit_task_exec_6(int, char *[]);
int upgd_commit_task_exec_7(int, char *[]);
int upgd_commit_task_exec_8(int, char *[]);
int upgd_commit_task_exec_9(int, char *[]);
int upgd_commit_task_exec_10(int, char *[]);
int upgd_commit_task_exec_11(int, char *[]);

int cb_coord_cb_task_done_1(int, char *[]);
int cb_coord_cb_task_done_2(int, char *[]);

/*
int upgd_commit_task_retry_1(int, char *[]);
int upgd_commit_task_retry_2(int, char *[]);
int upgd_commit_task_retry_3(int, char *[]);
*/

int upgrade_commit_common(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	nodeid_t		fault_node,
	int	when_to_trigger,
	Environment		&e);
int do_commit(Environment &e);

#endif	/* _ENTRY_UPGRADE_COMMIT_V1_H */
