/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fault_upgrade_commit.cc	1.6	08/05/20 SMI"

//
// This file contains the on and off functions for the fault points for a
// specific fault scenario.
//
// Refer to the README.faults file for a complete description of all
// scenarios
//

#include	<sys/os.h>
#include	<orb/fault/fault_injection.h>
#include	<orb/fault/fault_functions.h>

int
fault_lca_do_callback_task_1_on(void * argp, int arglen, int when_to_trigger)
{
	ASSERT(argp != NULL);
	ASSERT((uint32_t)arglen == sizeof (FaultFunctions::wait_for_arg_t));

	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT_ONCE,
		FAULTNUM_VM_LCA_BEFORE_DO_CB_TASK,
		argp, (uint32_t)arglen,
		(uint32_t)TRIGGER_THIS_NODE,
		(FaultFunctions::counter_when_t)when_to_trigger);

	os::warning("Armed fault: "
		"FAULTNUM_VM_LCA_BEFORE_DO_CB_TASK");
	return (0);
}

int
fault_lca_do_callback_task_1_off(void *, int, int)
{
	return (0);
}


int
fault_lca_do_callback_task_2_on(void * argp, int arglen, int when_to_trigger)
{
	ASSERT(argp != NULL);
	ASSERT((uint32_t)arglen == sizeof (FaultFunctions::wait_for_arg_t));

	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT_ONCE,
		FAULTNUM_VM_LCA_CB_TASK_BEFORE_LAUNCH,
		argp, (uint32_t)arglen,
		(uint32_t)TRIGGER_THIS_NODE,
		(FaultFunctions::counter_when_t)when_to_trigger);

	os::warning("Armed fault: "
		"FAULTNUM_VM_LCA_CB_TASK_BEFORE_LAUNCH");
	return (0);
}

int
fault_lca_do_callback_task_2_off(void *, int, int)
{
	return (0);
}


int
fault_lca_do_callback_task_3_on(void * argp, int arglen, int when_to_trigger)
{
	ASSERT(argp != NULL);
	ASSERT((uint32_t)arglen == sizeof (FaultFunctions::wait_for_arg_t));

	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT_ONCE,
		FAULTNUM_VM_LCA_CB_TASK_AFTER_LAUNCH,
		argp, (uint32_t)arglen,
		(uint32_t)TRIGGER_THIS_NODE,
		(FaultFunctions::counter_when_t)when_to_trigger);

	os::warning("Armed fault: "
		"FAULTNUM_VM_LCA_CB_TASK_AFTER_LAUNCH");
	return (0);
}

int
fault_lca_do_callback_task_3_off(void *, int, int)
{
	return (0);
}


int
fault_lca_do_callback_task_4_on(void * argp, int arglen, int when_to_trigger)
{
	ASSERT(argp != NULL);
	ASSERT((uint32_t)arglen == sizeof (FaultFunctions::wait_for_arg_t));

	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT_ONCE,
		FAULTNUM_VM_LCA_UNFINISHED_BTSTRP_CL_CB,
		argp, (uint32_t)arglen,
		(uint32_t)TRIGGER_THIS_NODE,
		(FaultFunctions::counter_when_t)when_to_trigger);

	os::warning("Armed fault: "
		"FAULTNUM_VM_LCA_UNFINISHED_BTSTRP_CL_CB");
	return (0);
}

int
fault_lca_do_callback_task_4_off(void *, int, int)
{
	return (0);
}


int
fault_lca_do_callback_task_5_on(void * argp, int arglen, int when_to_trigger)
{
	ASSERT(argp != NULL);
	ASSERT((uint32_t)arglen == sizeof (FaultFunctions::wait_for_arg_t));

	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT_ONCE,
		FAULTNUM_VM_LCA_UNFINISHED_CL_CB,
		argp, (uint32_t)arglen,
		(uint32_t)TRIGGER_THIS_NODE,
		(FaultFunctions::counter_when_t)when_to_trigger);

	os::warning("Armed fault: "
		"FAULTNUM_VM_LCA_UNFINISHED_CL_CB");
	return (0);
}

int
fault_lca_do_callback_task_5_off(void *, int, int)
{
	return (0);
}


int
fault_lca_do_callback_task_6_on(void * argp, int arglen, int when_to_trigger)
{
	ASSERT(argp != NULL);
	ASSERT((uint32_t)arglen == sizeof (FaultFunctions::wait_for_arg_t));

	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT_ONCE,
		FAULTNUM_VM_LCA_CB_THREAD_DONE,
		argp, (uint32_t)arglen,
		(uint32_t)TRIGGER_THIS_NODE,
		(FaultFunctions::counter_when_t)when_to_trigger);

	os::warning("Armed fault: "
		"FAULTNUM_VM_LCA_CB_THREAD_DONE");
	return (0);
}

int
fault_lca_do_callback_task_6_off(void *, int, int)
{
	return (0);
}


int
fault_lca_do_callback_task_7_on(void * argp, int arglen, int when_to_trigger)
{
	ASSERT(argp != NULL);
	ASSERT((uint32_t)arglen == sizeof (FaultFunctions::wait_for_arg_t));

	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT_ONCE,
		FAULTNUM_VM_LCA_NOTIFY_COORD_CB_TASK_DONE_1,
		argp, (uint32_t)arglen,
		(uint32_t)TRIGGER_THIS_NODE,
		(FaultFunctions::counter_when_t)when_to_trigger);

	os::warning("Armed fault: "
		"FAULTNUM_VM_LCA_NOTIFY_COORD_CB_TASK_DONE_1");
	return (0);
}

int
fault_lca_do_callback_task_7_off(void *, int, int)
{
	return (0);
}


int
fault_lca_do_callback_task_8_on(void * argp, int arglen, int when_to_trigger)
{
	ASSERT(argp != NULL);
	ASSERT((uint32_t)arglen == sizeof (FaultFunctions::wait_for_arg_t));

	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT_ONCE,
		FAULTNUM_VM_LCA_NOTIFY_COORD_CB_TASK_DONE_2,
		argp, (uint32_t)arglen,
		(uint32_t)TRIGGER_THIS_NODE,
		(FaultFunctions::counter_when_t)when_to_trigger);

	os::warning("Armed fault: "
		"FAULTNUM_VM_LCA_NOTIFY_COORD_CB_TASK_DONE_2");
	return (0);
}

int
fault_lca_do_callback_task_8_off(void *, int, int)
{
	return (0);
}


int
fault_lca_do_callback_task_9_on(void * argp, int arglen, int when_to_trigger)
{
	ASSERT(argp != NULL);
	ASSERT((uint32_t)arglen == sizeof (FaultFunctions::wait_for_arg_t));

	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT_ONCE,
		FAULTNUM_VM_LCA_NOTIFY_COORD_CB_TASK_DONE_3,
		argp, (uint32_t)arglen,
		(uint32_t)TRIGGER_THIS_NODE,
		(FaultFunctions::counter_when_t)when_to_trigger);

	os::warning("Armed fault: "
		"FAULTNUM_VM_LCA_NOTIFY_COORD_CB_TASK_DONE_3");
	return (0);
}

int
fault_lca_do_callback_task_9_off(void *, int, int)
{
	return (0);
}


int
fault_lca_do_callback_task_10_on(void * argp, int arglen, int when_to_trigger)
{
	ASSERT(argp != NULL);
	ASSERT((uint32_t)arglen == sizeof (FaultFunctions::wait_for_arg_t));

	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT_ONCE,
		FAULTNUM_VM_LCA_NOTIFY_COORD_CB_TASK_DONE_4,
		argp, (uint32_t)arglen,
		(uint32_t)TRIGGER_THIS_NODE,
		(FaultFunctions::counter_when_t)when_to_trigger);

	os::warning("Armed fault: "
		"FAULTNUM_VM_LCA_NOTIFY_COORD_CB_TASK_DONE_4");
	return (0);
}

int
fault_lca_do_callback_task_10_off(void *, int, int)
{
	return (0);
}


//
// Fault points in cb_coordinator::upgrade_commit()
//


int
fault_cb_coord_upgd_commit_1_on(void * argp, int arglen, int when_to_trigger)
{
	ASSERT(argp != NULL);
	ASSERT((uint32_t)arglen == sizeof (FaultFunctions::wait_for_arg_t));

	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT_ONCE,
		FAULTNUM_VM_CONSTRUCT_UCC_GROUP,
		argp, (uint32_t)arglen,
		(uint32_t)TRIGGER_ALL_NODES,
		(FaultFunctions::counter_when_t)when_to_trigger);

	os::warning("Armed fault: "
		"FAULTNUM_VM_CONSTRUCT_UCC_GROUP");
	return (0);
}

int
fault_cb_coord_upgd_commit_1_off(void *, int, int)
{
	NodeTriggers::clear(FAULTNUM_VM_CONSTRUCT_UCC_GROUP,
		TRIGGER_ALL_NODES);

	os::warning("Disarmed fault: "
		"FAULTNUM_VM_CONSTRUCT_UCC_GROUP");

	return (0);
}


int
fault_cb_coord_upgd_commit_2_on(void * argp, int arglen, int when_to_trigger)
{
	ASSERT(argp != NULL);
	ASSERT((uint32_t)arglen == sizeof (FaultFunctions::wait_for_arg_t));

	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT_ONCE,
		FAULTNUM_VM_CALCULATE_NEW_RVS_UVS,
		argp, (uint32_t)arglen,
		(uint32_t)TRIGGER_ALL_NODES,
		(FaultFunctions::counter_when_t)when_to_trigger);

	os::warning("Armed fault: "
		"FAULTNUM_VM_CALCULATE_NEW_RVS_UVS");
	return (0);
}

int
fault_cb_coord_upgd_commit_2_off(void *, int, int)
{
	NodeTriggers::clear(FAULTNUM_VM_CALCULATE_NEW_RVS_UVS,
		TRIGGER_ALL_NODES);

	os::warning("Disarmed fault: "
		"FAULTNUM_VM_CALCULATE_NEW_RVS_UVS");

	return (0);
}


int
fault_cb_coord_upgd_commit_3_on(void * argp, int arglen, int when_to_trigger)
{
	ASSERT(argp != NULL);
	ASSERT((uint32_t)arglen == sizeof (FaultFunctions::wait_for_arg_t));

	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT_ONCE,
		FAULTNUM_VM_ALLOCATE_UPGD_COMMIT_TASK,
		argp, (uint32_t)arglen,
		(uint32_t)TRIGGER_ALL_NODES,
		(FaultFunctions::counter_when_t)when_to_trigger);

	os::warning("Armed fault: "
		"FAULTNUM_VM_ALLOCATE_UPGD_COMMIT_TASK");
	return (0);
}

int
fault_cb_coord_upgd_commit_3_off(void *, int, int)
{
	NodeTriggers::clear(FAULTNUM_VM_ALLOCATE_UPGD_COMMIT_TASK,
		TRIGGER_ALL_NODES);

	os::warning("Disarmed fault: "
		"FAULTNUM_VM_ALLOCATE_UPGD_COMMIT_TASK");

	return (0);
}

//
// Fault points in upgrade_commit_task::execute()
//

int
fault_upgd_commit_task_exec_1_on(void * argp, int arglen, int when_to_trigger)
{
	ASSERT(argp != NULL);
	ASSERT((uint32_t)arglen == sizeof (FaultFunctions::wait_for_arg_t));

	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT_ONCE,
		FAULTNUM_VM_UPGD_COMMIT_TASK_1_B,
		argp, (uint32_t)arglen,
		(uint32_t)TRIGGER_ALL_NODES,
		(FaultFunctions::counter_when_t)when_to_trigger);

	os::warning("Armed fault: "
		"FAULTNUM_VM_UPGD_COMMIT_TASK_1_B");
	return (0);
}

int
fault_upgd_commit_task_exec_1_off(void *, int, int)
{
	return (0);
}


int
fault_upgd_commit_task_exec_2_on(void * argp, int arglen, int when_to_trigger)
{
	ASSERT(argp != NULL);
	ASSERT((uint32_t)arglen == sizeof (FaultFunctions::wait_for_arg_t));

	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT_ONCE,
		FAULTNUM_VM_UPGD_COMMIT_TASK_1_C,
		argp, (uint32_t)arglen,
		(uint32_t)TRIGGER_ALL_NODES,
		(FaultFunctions::counter_when_t)when_to_trigger);

	os::warning("Armed fault: "
		"FAULTNUM_VM_UPGD_COMMIT_TASK_1_C");
	return (0);
}

int
fault_upgd_commit_task_exec_2_off(void *, int, int)
{
	return (0);
}


int
fault_upgd_commit_task_exec_3_on(void * argp, int arglen, int when_to_trigger)
{
	ASSERT(argp != NULL);
	ASSERT((uint32_t)arglen == sizeof (FaultFunctions::wait_for_arg_t));

	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT_ONCE,
		FAULTNUM_VM_UPGD_COMMIT_TASK_2_B,
		argp, (uint32_t)arglen,
		(uint32_t)TRIGGER_ALL_NODES,
		(FaultFunctions::counter_when_t)when_to_trigger);

	os::warning("Armed fault: "
		"FAULTNUM_VM_UPGD_COMMIT_TASK_2_B");
	return (0);
}

int
fault_upgd_commit_task_exec_3_off(void *, int, int)
{
	return (0);
}


int
fault_upgd_commit_task_exec_4_on(void * argp, int arglen, int when_to_trigger)
{
	ASSERT(argp != NULL);
	ASSERT((uint32_t)arglen == sizeof (FaultFunctions::wait_for_arg_t));

	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT_ONCE,
		FAULTNUM_VM_UPGD_COMMIT_TASK_2_C,
		argp, (uint32_t)arglen,
		(uint32_t)TRIGGER_ALL_NODES,
		(FaultFunctions::counter_when_t)when_to_trigger);

	os::warning("Armed fault: "
		"FAULTNUM_VM_UPGD_COMMIT_TASK_2_C");
	return (0);
}

int
fault_upgd_commit_task_exec_4_off(void *, int, int)
{
	return (0);
}


int
fault_upgd_commit_task_exec_5_on(void * argp, int arglen, int when_to_trigger)
{
	ASSERT(argp != NULL);
	ASSERT((uint32_t)arglen == sizeof (FaultFunctions::wait_for_arg_t));

	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT_ONCE,
		FAULTNUM_VM_UPGD_COMMIT_TASK_3_B,
		argp, (uint32_t)arglen,
		(uint32_t)TRIGGER_ALL_NODES,
		(FaultFunctions::counter_when_t)when_to_trigger);

	os::warning("Armed fault: "
		"FAULTNUM_VM_UPGD_COMMIT_TASK_3_B");
	return (0);
}

int
fault_upgd_commit_task_exec_5_off(void *, int, int)
{
	return (0);
}


int
fault_upgd_commit_task_exec_6_on(void * argp, int arglen, int when_to_trigger)
{
	ASSERT(argp != NULL);
	ASSERT((uint32_t)arglen == sizeof (FaultFunctions::wait_for_arg_t));

	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT_ONCE,
		FAULTNUM_VM_UPGD_COMMIT_TASK_3_C,
		argp, (uint32_t)arglen,
		(uint32_t)TRIGGER_ALL_NODES,
		(FaultFunctions::counter_when_t)when_to_trigger);

	os::warning("Armed fault: "
		"FAULTNUM_VM_UPGD_COMMIT_TASK_3_C");
	return (0);
}

int
fault_upgd_commit_task_exec_6_off(void *, int, int)
{
	return (0);
}


int
fault_upgd_commit_task_exec_7_on(void * argp, int arglen, int when_to_trigger)
{
	ASSERT(argp != NULL);
	ASSERT((uint32_t)arglen == sizeof (FaultFunctions::wait_for_arg_t));

	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT_ONCE,
		FAULTNUM_VM_UPGD_COMMIT_TASK_3_D,
		argp, (uint32_t)arglen,
		(uint32_t)TRIGGER_ALL_NODES,
		(FaultFunctions::counter_when_t)when_to_trigger);

	os::warning("Armed fault: "
		"FAULTNUM_VM_UPGD_COMMIT_TASK_3_D");
	return (0);
}

int
fault_upgd_commit_task_exec_7_off(void *, int, int)
{
	return (0);
}



int
fault_upgd_commit_task_exec_8_on(void * argp, int arglen, int when_to_trigger)
{
	ASSERT(argp != NULL);
	ASSERT((uint32_t)arglen == sizeof (FaultFunctions::wait_for_arg_t));

	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT_ONCE,
		FAULTNUM_VM_UPGD_COMMIT_TASK_4_A,
		argp, (uint32_t)arglen,
		(uint32_t)TRIGGER_ALL_NODES,
		(FaultFunctions::counter_when_t)when_to_trigger);

	os::warning("Armed fault: "
		"FAULTNUM_VM_UPGD_COMMIT_TASK_4_A");
	return (0);
}

int
fault_upgd_commit_task_exec_8_off(void *, int, int)
{
	return (0);
}


int
fault_upgd_commit_task_exec_9_on(void * argp, int arglen, int when_to_trigger)
{
	ASSERT(argp != NULL);
	ASSERT((uint32_t)arglen == sizeof (FaultFunctions::wait_for_arg_t));

	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT_ONCE,
		FAULTNUM_VM_UPGD_COMMIT_TASK_4_B,
		argp, (uint32_t)arglen,
		(uint32_t)TRIGGER_ALL_NODES,
		(FaultFunctions::counter_when_t)when_to_trigger);

	os::warning("Armed fault: "
		"FAULTNUM_VM_UPGD_COMMIT_TASK_4_B");
	return (0);
}

int
fault_upgd_commit_task_exec_9_off(void *, int, int)
{
	return (0);
}


int
fault_upgd_commit_task_exec_10_on(void * argp, int arglen, int when_to_trigger)
{
	ASSERT(argp != NULL);
	ASSERT((uint32_t)arglen == sizeof (FaultFunctions::wait_for_arg_t));

	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT_ONCE,
		FAULTNUM_VM_UPGD_COMMIT_TASK_0_A,
		argp, (uint32_t)arglen,
		(uint32_t)TRIGGER_ALL_NODES,
		(FaultFunctions::counter_when_t)when_to_trigger);

	os::warning("Armed fault: "
		"FAULTNUM_VM_UPGD_COMMIT_TASK_0_A");
	return (0);
}

int
fault_upgd_commit_task_exec_10_off(void *, int, int)
{
	return (0);
}


int
fault_upgd_commit_task_exec_11_on(void * argp, int arglen, int when_to_trigger)
{
	ASSERT(argp != NULL);
	ASSERT((uint32_t)arglen == sizeof (FaultFunctions::wait_for_arg_t));

	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT_ONCE,
		FAULTNUM_VM_UPGD_COMMIT_TASK_0_B,
		argp, (uint32_t)arglen,
		(uint32_t)TRIGGER_ALL_NODES,
		(FaultFunctions::counter_when_t)when_to_trigger);

	os::warning("Armed fault: "
		"FAULTNUM_VM_UPGD_COMMIT_TASK_0_B");
	return (0);
}

int
fault_upgd_commit_task_exec_11_off(void *, int, int)
{
	return (0);
}

//
// cb_coordinator::callback_task_done_on_one_lca()
//

int
fault_cb_coord_cb_task_done_1_on(void * argp, int arglen, int when_to_trigger)
{
	ASSERT(argp != NULL);
	ASSERT((uint32_t)arglen == sizeof (FaultFunctions::wait_for_arg_t));

	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT_ONCE,
		FAULTNUM_VM_CB_COORD_CB_TASK_DONE_1,
		argp, (uint32_t)arglen,
		(uint32_t)TRIGGER_ALL_NODES,
		(FaultFunctions::counter_when_t)when_to_trigger);

	os::warning("Armed fault: "
		"FAULTNUM_VM_CB_COORD_CB_TASK_DONE_1");
	return (0);
}

int
fault_cb_coord_cb_task_done_1_off(void *, int, int)
{
	return (0);
}



int
fault_cb_coord_cb_task_done_2_on(void * argp, int arglen, int when_to_trigger)
{
	ASSERT(argp != NULL);
	ASSERT((uint32_t)arglen == sizeof (FaultFunctions::wait_for_arg_t));

	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT_ONCE,
		FAULTNUM_VM_CB_COORD_CB_TASK_DONE_2,
		argp, (uint32_t)arglen,
		(uint32_t)TRIGGER_ALL_NODES,
		(FaultFunctions::counter_when_t)when_to_trigger);

	os::warning("Armed fault: "
		"FAULTNUM_VM_CB_COORD_CB_TASK_DONE_2");
	return (0);
}

int
fault_cb_coord_cb_task_done_2_off(void *, int, int)
{
	return (0);
}
