/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/* CSTYLED */
#pragma ident	"@(#)entry_one_phase_non_idempotent_v0.cc	1.7	08/05/20 SMI"

#include <orbtest/vm_upgd_cb/repl_sample_clnt_v0.h>
#include <sys/os.h>
#include <orb/invo/common.h>
#include <orbtest/rm_probe/rm_snap.h>

//
// One-Phase Non idempotent Test Case 1-A
//
int
one_phase_non_idempotent_1_a(int, char *[])
{
	os::printf("*** One-Phase Non-Idempotent Mini-Transaction Scenario \n");

	// We're just checking that a regular invocation with
	// checkpointing can succeed

	Environment	e;

	return (one_phase_mini_2(NULL, NULL, NULL, 0, e, "ha_sample_server",
		false));
}
