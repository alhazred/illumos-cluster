/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ENTRY_SWITCHOVER_SEC_PRI_V1_H
#define	_ENTRY_SWITCHOVER_SEC_PRI_V1_H

#pragma ident	"@(#)entry_switchover_sec_pri_v1.h	1.6	08/05/20 SMI"

#include <orbtest/vm_upgd_cb/faults/entry_common.h>

int switchover_sec_pri_1_a(int, char *[]);

#endif	/* _ENTRY_SWITCHOVER_SEC_PRI_V1_H */
