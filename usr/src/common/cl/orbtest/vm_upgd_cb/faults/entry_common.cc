/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)entry_common.cc	1.6	08/05/20 SMI"

#include <orbtest/vm_upgd_cb/faults/entry_common.h>

#include <sys/os.h>
#include <orb/infrastructure/orb_conf.h>

#include <orbtest/rm_probe/rm_snap.h>

//
// If fault injection is not turned on, print an error message and return 0.
//
#if !defined(_FAULT_INJECTION)
int
no_fault_injection()
{
	os::printf("+++ FAIL: Fault Injection Compilation Flags were not"
		"set!\n");
	return (0);
}
#endif

#if defined(_KERNEL_ORB) && defined(_FAULT_INJECTION)

//
// Find the nth non-root-node
//
nodeid_t
non_root_node(int n)
{
	nodeid_t	root_nodeid;
	nodeid_t	highest_nodeid;
	nodeid_t	counter		= 0;

	root_nodeid = orb_conf::get_root_nodeid();

	// I'm going to do this really lame..
	// because there is no direct interface for what i want to do
	// I want to examine a list of all the nodeid's in the cluster, that
	// is not directly available without some major hoopla..
	// so instead i will create my own list of live nodes..  should be good
	// enough

	// Get the highest nodeid
	highest_nodeid = NODEID_MAX;

	while (n > 0) {
		// Increase counter
		counter++;

		// Have we looked too far?
		if (counter > highest_nodeid)
			return (0);

		// Ignore the root node
		if (counter == root_nodeid)
			continue;

		// Check if its a configured member of the cluster
		if (orb_conf::node_configured(counter)) {
			n--;
		}
	}

	return (counter);
}

#endif // _KERNEL_ORB


//
// get_primary
//
// Get the primary nodeid for the specified service
// arguments
//	service name,
//	print		- a boolean specifiying wether to print error msgs
// returns
//	nodeid (or -1 if error)
// side effect
//	prints errors
nodeid_t
get_primary(const char *svc_name, bool print)
{
	rm_snap		_rma_snapshot;
	nodeid_t	primary;

	if (!_rma_snapshot.refresh(svc_name)) {
		if (print) {
			os::printf("+++ FAIL: Could not get rma snapshot "
			    "(%s)\n", svc_name);
		}
		return (0);
	}
	if (_rma_snapshot.get_primary(primary) != 0) {
		if (print) {
			os::printf("+++ FAIL: Could not get primary nodeid "
			    "(%s)\n", svc_name);
		}
		return (0);
	}
	if (primary == 0) {
		if (print) {
			os::printf("+++ FAIL: No primary exists for service "
			    "(%s)\n", svc_name);
		}
		return (0);
	}

	return (primary);
}


//
// get_secondary
//
// Get the nth secondary nodeid for the specified service
// arguments
//	service name
//	print		- a boolean specifiying wether to print error msgs
// returns
//	nodeid (or -1 if error)
// side effect
//	prints errors
nodeid_t
get_secondary(int n, const char *svc_name, bool print)
{
	rm_snap		_rma_snapshot;
	nodeid_t	secondary;

	if (!_rma_snapshot.refresh(svc_name)) {
		if (print) {
			os::printf("+++ FAIL: Could not get rma snapshot "
			    "(%s)\n", svc_name);
		}
		return (0);
	}
	if (_rma_snapshot.get_secondary(n, secondary) != 0) {
		if (print) {
			os::printf("+++ FAIL: Could not get secondary nodeid "
			    "(%s)\n", svc_name);
		}
		return (0);
	}
	if (secondary == 0) {
		if (print) {
			os::printf("+++ FAIL: Secondary #%d was not found for "
			    "(%s)\n", n, svc_name);
		}
		return (0);
	}

	return (secondary);
}



//
// get_state
//
// Get the state of the specified service
// arguments
//	service_name
//	print		- a boolean specifiying wether to print error msgs
// returns
//	state
// side effect
//	prints errors
int
get_state(const char *svc_name, bool print)
{
	rm_snap	_rma_snapshot;
	int	state;

	if (!_rma_snapshot.refresh(svc_name)) {
		if (print) {
			os::printf("+++ FAIL: Could not get rma snapshot "
			    "(%s)\n", svc_name);
		}
		return (-1);
	}

	state = _rma_snapshot.get_state();
	if (state == -1) {
		if (print) {
			os::printf("+++ FAIL: Could not get state (%s)\n",
			    svc_name);
		}
		return (-1);
	}

	return (state);
}


//
// get_sid
//
// Get the sid of the specified service
// arguments
//	service_name
//	print		- a boolean specifiying wether to print error msgs
// returns
//	sid
// side effect
//	prints errors
replica::service_num_t
get_sid(const char *svc_name, bool print)
{
	rm_snap	_rma_snapshot;
	replica::service_num_t	sid;

	if (!_rma_snapshot.refresh(svc_name)) {
		if (print) {
			os::printf("+++ FAIL: Could not get rma snapshot "
			    "(%s)\n", svc_name);
		}
		return (0);
	}

	sid = _rma_snapshot.get_sid();
	if (sid == 0) {
		if (print) {
			os::printf("+++ FAIL: Could not get sid (%s)\n",
			    svc_name);
		}
		return (0);
	}

	return (sid);
}

//
// Print error message
//
void
print_error_message(const char *functname)
{
	os::printf("Incorrect usage:\t%s\n", functname);
}
