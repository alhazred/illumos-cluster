/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FAULT_UPGRADE_COMMIT_H
#define	_FAULT_UPGRADE_COMMIT_H

#pragma ident	"@(#)fault_upgrade_commit.h	1.7	08/05/20 SMI"

#include <orbtest/vm_upgd_cb/faults/entry_common.h>

//
// This file contains the on and off functions for the fault points for a
// specific fault scenario.
//
// Refer to the README.faults file for a complete description of all
// scenarios
//

int fault_lca_do_callback_task_1_on(void *, int, int);
int fault_lca_do_callback_task_1_off(void *, int, int);
int fault_lca_do_callback_task_2_on(void *, int, int);
int fault_lca_do_callback_task_2_off(void *, int, int);
int fault_lca_do_callback_task_3_on(void *, int, int);
int fault_lca_do_callback_task_3_off(void *, int, int);
int fault_lca_do_callback_task_4_on(void *, int, int);
int fault_lca_do_callback_task_4_off(void *, int, int);
int fault_lca_do_callback_task_5_on(void *, int, int);
int fault_lca_do_callback_task_5_off(void *, int, int);
int fault_lca_do_callback_task_6_on(void *, int, int);
int fault_lca_do_callback_task_6_off(void *, int, int);
int fault_lca_do_callback_task_7_on(void *, int, int);
int fault_lca_do_callback_task_7_off(void *, int, int);
int fault_lca_do_callback_task_8_on(void *, int, int);
int fault_lca_do_callback_task_8_off(void *, int, int);
int fault_lca_do_callback_task_9_on(void *, int, int);
int fault_lca_do_callback_task_9_off(void *, int, int);
int fault_lca_do_callback_task_10_on(void *, int, int);
int fault_lca_do_callback_task_10_off(void *, int, int);
int fault_lca_do_callback_task_11_on(void *, int, int);
int fault_lca_do_callback_task_11_off(void *, int, int);

int fault_cb_coord_upgd_commit_1_on(void *, int, int);
int fault_cb_coord_upgd_commit_1_off(void *, int, int);
int fault_cb_coord_upgd_commit_2_on(void *, int, int);
int fault_cb_coord_upgd_commit_2_off(void *, int, int);
int fault_cb_coord_upgd_commit_3_on(void *, int, int);
int fault_cb_coord_upgd_commit_3_off(void *, int, int);

int fault_upgd_commit_task_exec_1_on(void *, int, int);
int fault_upgd_commit_task_exec_1_off(void *, int, int);
int fault_upgd_commit_task_exec_2_on(void *, int, int);
int fault_upgd_commit_task_exec_2_off(void *, int, int);
int fault_upgd_commit_task_exec_3_on(void *, int, int);
int fault_upgd_commit_task_exec_3_off(void *, int, int);
int fault_upgd_commit_task_exec_4_on(void *, int, int);
int fault_upgd_commit_task_exec_4_off(void *, int, int);
int fault_upgd_commit_task_exec_5_on(void *, int, int);
int fault_upgd_commit_task_exec_5_off(void *, int, int);
int fault_upgd_commit_task_exec_6_on(void *, int, int);
int fault_upgd_commit_task_exec_6_off(void *, int, int);
int fault_upgd_commit_task_exec_7_on(void *, int, int);
int fault_upgd_commit_task_exec_7_off(void *, int, int);
int fault_upgd_commit_task_exec_8_on(void *, int, int);
int fault_upgd_commit_task_exec_8_off(void *, int, int);
int fault_upgd_commit_task_exec_9_on(void *, int, int);
int fault_upgd_commit_task_exec_9_off(void *, int, int);
int fault_upgd_commit_task_exec_10_on(void *, int, int);
int fault_upgd_commit_task_exec_10_off(void *, int, int);
int fault_upgd_commit_task_exec_11_on(void *, int, int);
int fault_upgd_commit_task_exec_11_off(void *, int, int);

int fault_cb_coord_cb_task_done_1_on(void *, int, int);
int fault_cb_coord_cb_task_done_1_off(void *, int, int);
int fault_cb_coord_cb_task_done_2_on(void *, int, int);
int fault_cb_coord_cb_task_done_2_off(void *, int, int);

#endif	/* _FAULT_UPGRADE_COMMIT_H */
