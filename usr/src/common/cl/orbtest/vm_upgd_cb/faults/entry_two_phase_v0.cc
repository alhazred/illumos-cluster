/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)entry_two_phase_v0.cc	1.6	08/05/20 SMI"

#include	<sys/os.h>
#include	<h/mc_sema.h>
#include	<h/repl_sample_v0.h>
#include	<sys/mc_sema_impl.h>
#include	<orb/invo/common.h>
#include	<orb/fault/fault_functions.h>
#include	<orbtest/vm_upgd_cb/repl_sample_clnt_v0.h>
#include	<orbtest/rm_probe/rm_snap.h>
#include	<orbtest/vm_upgd_cb/faults/entry_two_phase_aux_v0.h>

//
// Two-Phase Test Case 1-A
//
int
two_phase_1_a(int, char *[])
{
	// We're just checking that a two phase invocation can succeed
	// without faults.
	os::printf("*** Two-Phase Mini-Transaction Scenario \n");

	Environment	e;
	return (two_phase_mini_1(NULL, NULL, NULL, 0, e, "ha_sample_server",
		false));
}
