/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ENTRY_LCA_REGISTRATION_V1_H
#define	_ENTRY_LCA_REGISTRATION_V1_H

#pragma ident	"@(#)entry_lca_registration_v1.h	1.6	08/05/20 SMI"

#include <orbtest/vm_upgd_cb/faults/entry_common.h>

// Macro for the function to turn on fault injection
#define	FAULT_FPTR(func)	int (*func) (void *, int, int)

int register_lca_1(int, char *[]);
int register_lca_2(int, char *[]);
int register_lca_3(int, char *[]);
int register_lca_4(int, char *[]);
int register_lca(FAULT_FPTR(on_fp), nodeid_t fault_node,
    int when_to_trigger);

#endif	/* _ENTRY_LCA_REGISTRATION_V1_H */
