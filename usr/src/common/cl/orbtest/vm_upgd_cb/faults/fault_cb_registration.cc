/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fault_cb_registration.cc	1.6	08/05/20 SMI"

//
// This file contains the on and off functions for the fault points for a
// specific fault scenario.
//
// Refer to the README.faults file for a complete description of all
// scenarios
//

#include	<sys/os.h>
#include	<orb/fault/fault_injection.h>
#include	<orb/fault/fault_functions.h>

int
fault_lca_register_cl_uccs_1_on(void * argp, int arglen, int when_to_trigger)
{
	ASSERT(argp != NULL);
	ASSERT((uint32_t)arglen == sizeof (FaultFunctions::wait_for_arg_t));

	FaultFunctions::boot_trigger_add(FaultFunctions::REBOOT_ONCE,
		FAULTNUM_VM_LCA_REGISTER_CL_UCC,
		argp, (uint32_t)arglen,
		(uint32_t)TRIGGER_THIS_NODE,
		(FaultFunctions::counter_when_t)when_to_trigger);

	os::warning("Armed fault: ",
		"FAULTNUM_VM_LCA_REGISTER_CL_UCC");
	return (0);
}


int
fault_lca_register_cl_uccs_2_on(void * argp, int arglen, int when_to_trigger)
{
	ASSERT(argp != NULL);
	ASSERT((uint32_t)arglen == sizeof (FaultFunctions::wait_for_arg_t));

	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT_ONCE,
		FAULTNUM_VM_CB_COORD_REGISTER_UCC_1,
		argp, (uint32_t)arglen,
		(uint32_t)TRIGGER_THIS_NODE,
		(FaultFunctions::counter_when_t)when_to_trigger);

	os::warning("Armed fault: ",
		"FAULTNUM_VM_CB_COORD_REGISTER_UCC_1");
	return (0);
}


int
fault_lca_register_cl_uccs_3_on(void * argp, int arglen, int when_to_trigger)
{
	ASSERT(argp != NULL);
	ASSERT((uint32_t)arglen == sizeof (FaultFunctions::wait_for_arg_t));

	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT_ONCE,
		FAULTNUM_VM_CB_COORD_REGISTER_UCC_2,
		argp, (uint32_t)arglen,
		(uint32_t)TRIGGER_THIS_NODE,
		(FaultFunctions::counter_when_t)when_to_trigger);

	os::warning("Armed fault: ",
		"FAULTNUM_VM_CB_COORD_REGISTER_UCC_2");
	return (0);
}
