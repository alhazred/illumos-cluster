/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)entry_upgrade_commit_v1.cc	1.8	08/05/20 SMI"

// #include	<orbtest/vm_upgd_cb/repl_sample_clnt_v1.h>
#include	<sys/os.h>
#include	<orb/invo/common.h>
#include	<orbtest/rm_probe/rm_snap.h>
#include	<orb/infrastructure/orb_conf.h>
#include	<orb/fault/fault_injection.h>
#include	<sys/vm_util.h>

#include	<orbtest/vm_upgd_cb/faults/entry_upgrade_commit_v1.h>
#include	<orbtest/vm_upgd_cb/faults/fault_upgrade_commit.h>


//
// Basic 1-A Test case
//
int
lca_do_callback_task_1(int argc, char **argv)
{
	os::printf("*** Fault RM primary while lca does callback task 1 ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;
	Environment	e;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc > 2) {
		print_error_message(argv[0]);
		return (rslt);
	}

	if (argc == 1) {
		fault_node = orb_conf::local_nodeid();
	} else if (os::strcmp(argv[1], "rm_primary") == 0) {
		fault_node = FaultFunctions::rm_primary_node();
	} else {
		print_error_message(argv[0]);
		return (rslt);
	}

	os::printf("    Fault node: %d\n", fault_node);

	rslt = upgrade_commit_common(
		fault_lca_do_callback_task_1_on,
		fault_lca_do_callback_task_1_off,
		fault_node, ALWAYS, e);

	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

//
// Basic 1-B Test case
//
int
lca_do_callback_task_2(int argc, char **argv)
{
	os::printf("*** Fault RM primary while lca does callback task 2 ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;
	Environment	e;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc > 2) {
		print_error_message(argv[0]);
		return (rslt);
	}

	if (argc == 1) {
		fault_node = orb_conf::local_nodeid();
	} else if (os::strcmp(argv[1], "rm_primary") == 0) {
		fault_node = FaultFunctions::rm_primary_node();
	} else {
		print_error_message(argv[0]);
		return (rslt);
	}

	os::printf("    Fault node: %d\n", fault_node);

	rslt = upgrade_commit_common(
		fault_lca_do_callback_task_2_on,
		fault_lca_do_callback_task_2_off,
		fault_node, ALWAYS, e);

	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

//
// Basic 1-C Test case
//
int
lca_do_callback_task_3(int argc, char **argv)
{
	os::printf("*** Fault RM primary while lca does callback task 3 ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;
	Environment	e;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc > 2) {
		print_error_message(argv[0]);
		return (rslt);
	}

	if (argc == 1) {
		fault_node = orb_conf::local_nodeid();
	} else if (os::strcmp(argv[1], "rm_primary") == 0) {
		fault_node = FaultFunctions::rm_primary_node();
	} else {
		print_error_message(argv[0]);
		return (rslt);
	}

	os::printf("    Fault node: %d\n", fault_node);

	rslt = upgrade_commit_common(
		fault_lca_do_callback_task_3_on,
		fault_lca_do_callback_task_3_off,
		fault_node, ALWAYS, e);

	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

//
// Basic 1-D Test case
//
int
lca_do_callback_task_4(int argc, char **argv)
{
	os::printf("*** Fault RM primary while lca does callback task 4 ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;
	Environment	e;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc > 2) {
		print_error_message(argv[0]);
		return (rslt);
	}

	if (argc == 1) {
		fault_node = orb_conf::local_nodeid();
	} else if (os::strcmp(argv[1], "rm_primary") == 0) {
		fault_node = FaultFunctions::rm_primary_node();
	} else {
		print_error_message(argv[0]);
		return (rslt);
	}

	os::printf("    Fault node: %d\n", fault_node);

	rslt = upgrade_commit_common(
		fault_lca_do_callback_task_4_on,
		fault_lca_do_callback_task_4_off,
		fault_node, ALWAYS, e);

	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

//
// Basic 1-E Test case
//
int
lca_do_callback_task_5(int argc, char **argv)
{
	os::printf("*** Fault RM primary while lca does callback task 5 ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;
	Environment	e;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc > 2) {
		print_error_message(argv[0]);
		return (rslt);
	}

	if (argc == 1) {
		fault_node = orb_conf::local_nodeid();
	} else if (os::strcmp(argv[1], "rm_primary") == 0) {
		fault_node = FaultFunctions::rm_primary_node();
	} else {
		print_error_message(argv[0]);
		return (rslt);
	}

	os::printf("    Fault node: %d\n", fault_node);

	rslt = upgrade_commit_common(
		fault_lca_do_callback_task_5_on,
		fault_lca_do_callback_task_5_off,
		fault_node, ALWAYS, e);

	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

//
// Basic 1-F Test case
//
int
lca_do_callback_task_6(int argc, char **argv)
{
	os::printf("*** Fault RM primary while lca does callback task 6 ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;
	Environment	e;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc > 2) {
		print_error_message(argv[0]);
		return (rslt);
	}

	if (argc == 1) {
		fault_node = orb_conf::local_nodeid();
	} else if (os::strcmp(argv[1], "rm_primary") == 0) {
		fault_node = FaultFunctions::rm_primary_node();
	} else {
		print_error_message(argv[0]);
		return (rslt);
	}

	os::printf("    Fault node: %d\n", fault_node);

	rslt = upgrade_commit_common(
		fault_lca_do_callback_task_6_on,
		fault_lca_do_callback_task_6_off,
		fault_node, ALWAYS, e);

	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

//
// Basic 1-G Test case
//
int
lca_do_callback_task_7(int argc, char **argv)
{
	os::printf("*** Fault RM primary while lca does callback task 7 ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;
	Environment	e;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc > 2) {
		print_error_message(argv[0]);
		return (rslt);
	}

	if (argc == 1) {
		fault_node = orb_conf::local_nodeid();
	} else if (os::strcmp(argv[1], "rm_primary") == 0) {
		fault_node = FaultFunctions::rm_primary_node();
	} else {
		print_error_message(argv[0]);
		return (rslt);
	}

	os::printf("    Fault node: %d\n", fault_node);

	rslt = upgrade_commit_common(
		fault_lca_do_callback_task_7_on,
		fault_lca_do_callback_task_7_off,
		fault_node, ALWAYS, e);

	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

//
// Basic 1-H Test case
//
int
lca_do_callback_task_8(int argc, char **argv)
{
	os::printf("*** Fault RM primary while lca does callback task 8 ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;
	Environment	e;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc > 2) {
		print_error_message(argv[0]);
		return (rslt);
	}

	if (argc == 1) {
		fault_node = orb_conf::local_nodeid();
	} else if (os::strcmp(argv[1], "rm_primary") == 0) {
		fault_node = FaultFunctions::rm_primary_node();
	} else {
		print_error_message(argv[0]);
		return (rslt);
	}

	os::printf("    Fault node: %d\n", fault_node);

	rslt = upgrade_commit_common(
		fault_lca_do_callback_task_8_on,
		fault_lca_do_callback_task_8_off,
		fault_node, ALWAYS, e);

	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Basic 1-H Test case
//
int
lca_do_callback_task_9(int argc, char **argv)
{
	os::printf("*** Fault RM primary while lca does callback task 9 ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;
	Environment	e;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc > 2) {
		print_error_message(argv[0]);
		return (rslt);
	}

	if (argc == 1) {
		fault_node = orb_conf::local_nodeid();
	} else if (os::strcmp(argv[1], "rm_primary") == 0) {
		fault_node = FaultFunctions::rm_primary_node();
	} else {
		print_error_message(argv[0]);
		return (rslt);
	}

	os::printf("    Fault node: %d\n", fault_node);

	rslt = upgrade_commit_common(
		fault_lca_do_callback_task_9_on,
		fault_lca_do_callback_task_9_off,
		fault_node, ALWAYS, e);

	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Basic 1-H Test case
//
int
lca_do_callback_task_10(int argc, char **argv)
{
	os::printf("*** Fault RM primary while lca does callback "
		"task 10 ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;
	Environment	e;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc > 2) {
		print_error_message(argv[0]);
		return (rslt);
	}

	if (argc == 1) {
		fault_node = orb_conf::local_nodeid();
	} else if (os::strcmp(argv[1], "rm_primary") == 0) {
		fault_node = FaultFunctions::rm_primary_node();
	} else {
		print_error_message(argv[0]);
		return (rslt);
	}

	os::printf("    Fault node: %d\n", fault_node);

	rslt = upgrade_commit_common(
		fault_lca_do_callback_task_10_on,
		fault_lca_do_callback_task_10_off,
		fault_node, ALWAYS, e);

	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

/*

//
// Basic 1-H Test case
//
int
lca_do_callback_task_11(int argc, char **argv)
{
	os::printf("*** Fault RM primary while lca does callback "
		"task 11 ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;
	Environment	e;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc > 2) {
		print_error_message(argv[0]);
		return (rslt);
	}

	if (argc == 1) {
		fault_node = orb_conf::local_nodeid();
	} else if (os::strcmp(argv[1], "rm_primary") == 0) {
		fault_node = FaultFunctions::rm_primary_node();
	} else {
		print_error_message(argv[0]);
		return (rslt);
	}

	os::printf("    Fault node: %d\n", fault_node);

	rslt = upgrade_commit_common(
		fault_lca_do_callback_task_11_on,
		fault_lca_do_callback_task_11_off,
		fault_node, ALWAYS, e);

	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

*/

//
// Fault senario in cb_coordinator::upgrade_commit()
//
int
cb_coord_upgd_commit_1(int argc, char **argv)
{
	os::printf("*** Fault RM primary while cb_coordinator primary "
		"do upgrade commit 1 ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;
	Environment	e;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc > 2) {
		print_error_message(argv[0]);
		return (rslt);
	}

	if (argc == 1) {
		fault_node = orb_conf::local_nodeid();
	} else if (os::strcmp(argv[1], "rm_primary") == 0) {
		fault_node = FaultFunctions::rm_primary_node();
	} else {
		print_error_message(argv[0]);
		return (rslt);
	}

	os::printf("    Fault node: %d\n", fault_node);

	rslt = upgrade_commit_common(
		fault_cb_coord_upgd_commit_1_on,
		fault_cb_coord_upgd_commit_1_off,
		fault_node, ALWAYS, e);

	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Fault senario in cb_coordinator::upgrade_commit()
//
int
cb_coord_upgd_commit_2(int argc, char **argv)
{
	os::printf("*** Fault RM primary while cb_coordinator primary "
		"do upgrade commit 2 ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;
	Environment	e;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc > 2) {
		print_error_message(argv[0]);
		return (rslt);
	}

	if (argc == 1) {
		fault_node = orb_conf::local_nodeid();
	} else if (os::strcmp(argv[1], "rm_primary") == 0) {
		fault_node = FaultFunctions::rm_primary_node();
	} else {
		print_error_message(argv[0]);
		return (rslt);
	}

	os::printf("    Fault node: %d\n", fault_node);

	rslt = upgrade_commit_common(
		fault_cb_coord_upgd_commit_2_on,
		fault_cb_coord_upgd_commit_2_off,
		fault_node, ALWAYS, e);

	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Fault senario in cb_coordinator::upgrade_commit()
//
int
cb_coord_upgd_commit_3(int argc, char **argv)
{
	os::printf("*** Fault RM primary while cb_coordinator primary "
		"do upgrade commit 3 ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;
	Environment	e;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc > 2) {
		print_error_message(argv[0]);
		return (rslt);
	}

	if (argc == 1) {
		fault_node = orb_conf::local_nodeid();
	} else if (os::strcmp(argv[1], "rm_primary") == 0) {
		fault_node = FaultFunctions::rm_primary_node();
	} else {
		print_error_message(argv[0]);
		return (rslt);
	}

	os::printf("    Fault node: %d\n", fault_node);

	rslt = upgrade_commit_common(
		fault_cb_coord_upgd_commit_3_on,
		fault_cb_coord_upgd_commit_3_off,
		fault_node, ALWAYS, e);

	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Fault senario in upgrade_commit_task::execute()
//
int
upgd_commit_task_exec_1(int argc, char **argv)
{
	os::printf("*** Fault RM primary while cb_coordinator primary "
		"execute upgrade commit task 1 ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;
	Environment	e;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc > 2) {
		print_error_message(argv[0]);
		return (rslt);
	}

	if (argc == 1) {
		fault_node = orb_conf::local_nodeid();
	} else if (os::strcmp(argv[1], "rm_primary") == 0) {
		fault_node = FaultFunctions::rm_primary_node();
	} else {
		print_error_message(argv[0]);
		return (rslt);
	}

	os::printf("    Fault node: %d\n", fault_node);

	rslt = upgrade_commit_common(
		fault_upgd_commit_task_exec_1_on,
		fault_upgd_commit_task_exec_1_off,
		fault_node, ALWAYS, e);

	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Fault senario in upgrade_commit_task::execute()
//
int
upgd_commit_task_exec_2(int argc, char **argv)
{
	os::printf("*** Fault RM primary while cb_coordinator primary "
		"execute upgrade commit task 2 ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;
	Environment	e;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc > 2) {
		print_error_message(argv[0]);
		return (rslt);
	}

	if (argc == 1) {
		fault_node = orb_conf::local_nodeid();
	} else if (os::strcmp(argv[1], "rm_primary") == 0) {
		fault_node = FaultFunctions::rm_primary_node();
	} else {
		print_error_message(argv[0]);
		return (rslt);
	}

	os::printf("    Fault node: %d\n", fault_node);

	rslt = upgrade_commit_common(
		fault_upgd_commit_task_exec_2_on,
		fault_upgd_commit_task_exec_2_off,
		fault_node, ALWAYS, e);

	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Fault senario in upgrade_commit_task::execute()
//
int
upgd_commit_task_exec_3(int argc, char **argv)
{
	os::printf("*** Fault RM primary while cb_coordinator primary "
		"execute upgrade commit task 3 ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;
	Environment	e;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc > 2) {
		print_error_message(argv[0]);
		return (rslt);
	}

	if (argc == 1) {
		fault_node = orb_conf::local_nodeid();
	} else if (os::strcmp(argv[1], "rm_primary") == 0) {
		fault_node = FaultFunctions::rm_primary_node();
	} else {
		print_error_message(argv[0]);
		return (rslt);
	}

	os::printf("    Fault node: %d\n", fault_node);

	rslt = upgrade_commit_common(
		fault_upgd_commit_task_exec_3_on,
		fault_upgd_commit_task_exec_3_off,
		fault_node, ALWAYS, e);

	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Fault senario in upgrade_commit_task::execute()
//
int
upgd_commit_task_exec_4(int argc, char **argv)
{
	os::printf("*** Fault RM primary while cb_coordinator primary "
		"execute upgrade commit task 4 ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;
	Environment	e;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc > 2) {
		print_error_message(argv[0]);
		return (rslt);
	}

	if (argc == 1) {
		fault_node = orb_conf::local_nodeid();
	} else if (os::strcmp(argv[1], "rm_primary") == 0) {
		fault_node = FaultFunctions::rm_primary_node();
	} else {
		print_error_message(argv[0]);
		return (rslt);
	}

	os::printf("    Fault node: %d\n", fault_node);

	rslt = upgrade_commit_common(
		fault_upgd_commit_task_exec_4_on,
		fault_upgd_commit_task_exec_4_off,
		fault_node, ALWAYS, e);

	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Fault senario in upgrade_commit_task::execute()
//
int
upgd_commit_task_exec_5(int argc, char **argv)
{
	os::printf("*** Fault RM primary while cb_coordinator primary "
		"execute upgrade commit task 5 ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;
	Environment	e;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc > 2) {
		print_error_message(argv[0]);
		return (rslt);
	}

	if (argc == 1) {
		fault_node = orb_conf::local_nodeid();
	} else if (os::strcmp(argv[1], "rm_primary") == 0) {
		fault_node = FaultFunctions::rm_primary_node();
	} else {
		print_error_message(argv[0]);
		return (rslt);
	}

	os::printf("    Fault node: %d\n", fault_node);

	rslt = upgrade_commit_common(
		fault_upgd_commit_task_exec_5_on,
		fault_upgd_commit_task_exec_5_off,
		fault_node, ALWAYS, e);

	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Fault senario in upgrade_commit_task::execute()
//
int
upgd_commit_task_exec_6(int argc, char **argv)
{
	os::printf("*** Fault RM primary while cb_coordinator primary "
		"execute upgrade commit task 6 ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;
	Environment	e;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc > 2) {
		print_error_message(argv[0]);
		return (rslt);
	}

	if (argc == 1) {
		fault_node = orb_conf::local_nodeid();
	} else if (os::strcmp(argv[1], "rm_primary") == 0) {
		fault_node = FaultFunctions::rm_primary_node();
	} else {
		print_error_message(argv[0]);
		return (rslt);
	}

	os::printf("    Fault node: %d\n", fault_node);

	rslt = upgrade_commit_common(
		fault_upgd_commit_task_exec_6_on,
		fault_upgd_commit_task_exec_6_off,
		fault_node, ALWAYS, e);

	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Fault senario in upgrade_commit_task::execute()
//
int
upgd_commit_task_exec_7(int argc, char **argv)
{
	os::printf("*** Fault RM primary while cb_coordinator primary "
		"execute upgrade commit task 7 ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;
	Environment	e;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc > 2) {
		print_error_message(argv[0]);
		return (rslt);
	}

	if (argc == 1) {
		fault_node = orb_conf::local_nodeid();
	} else if (os::strcmp(argv[1], "rm_primary") == 0) {
		fault_node = FaultFunctions::rm_primary_node();
	} else {
		print_error_message(argv[0]);
		return (rslt);
	}

	os::printf("    Fault node: %d\n", fault_node);

	rslt = upgrade_commit_common(
		fault_upgd_commit_task_exec_7_on,
		fault_upgd_commit_task_exec_7_off,
		fault_node, ALWAYS, e);

	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Fault senario in upgrade_commit_task::execute()
//
int
upgd_commit_task_exec_8(int argc, char **argv)
{
	os::printf("*** Fault RM primary while cb_coordinator primary "
		"execute upgrade commit task 8 ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;
	Environment	e;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc > 2) {
		print_error_message(argv[0]);
		return (rslt);
	}

	if (argc == 1) {
		fault_node = orb_conf::local_nodeid();
	} else if (os::strcmp(argv[1], "rm_primary") == 0) {
		fault_node = FaultFunctions::rm_primary_node();
	} else {
		print_error_message(argv[0]);
		return (rslt);
	}

	os::printf("    Fault node: %d\n", fault_node);

	rslt = upgrade_commit_common(
		fault_upgd_commit_task_exec_8_on,
		fault_upgd_commit_task_exec_8_off,
		fault_node, ALWAYS, e);

	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Fault senario in upgrade_commit_task::execute()
//
int
upgd_commit_task_exec_9(int argc, char **argv)
{
	os::printf("*** Fault RM primary while cb_coordinator primary "
		"execute upgrade commit task 9 ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;
	Environment	e;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc > 2) {
		print_error_message(argv[0]);
		return (rslt);
	}

	if (argc == 1) {
		fault_node = orb_conf::local_nodeid();
	} else if (os::strcmp(argv[1], "rm_primary") == 0) {
		fault_node = FaultFunctions::rm_primary_node();
	} else {
		print_error_message(argv[0]);
		return (rslt);
	}

	os::printf("    Fault node: %d\n", fault_node);

	rslt = upgrade_commit_common(
		fault_upgd_commit_task_exec_9_on,
		fault_upgd_commit_task_exec_9_off,
		fault_node, ALWAYS, e);

	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Fault senario in upgrade_commit_task::execute()
//
int
upgd_commit_task_exec_10(int argc, char **argv)
{
	os::printf("*** Fault RM primary while cb_coordinator primary "
		"execute upgrade commit task 10 ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;
	Environment	e;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc > 2) {
		print_error_message(argv[0]);
		return (rslt);
	}

	if (argc == 1) {
		fault_node = orb_conf::local_nodeid();
	} else if (os::strcmp(argv[1], "rm_primary") == 0) {
		fault_node = FaultFunctions::rm_primary_node();
	} else {
		print_error_message(argv[0]);
		return (rslt);
	}

	os::printf("    Fault node: %d\n", fault_node);

	rslt = upgrade_commit_common(
		fault_upgd_commit_task_exec_10_on,
		fault_upgd_commit_task_exec_10_off,
		fault_node, ALWAYS, e);

	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Fault senario in upgrade_commit_task::execute()
//
int
upgd_commit_task_exec_11(int argc, char **argv)
{
	os::printf("*** Fault RM primary while cb_coordinator primary "
		"execute upgrade commit task 11 ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;
	Environment	e;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc > 2) {
		print_error_message(argv[0]);
		return (rslt);
	}

	if (argc == 1) {
		fault_node = orb_conf::local_nodeid();
	} else if (os::strcmp(argv[1], "rm_primary") == 0) {
		fault_node = FaultFunctions::rm_primary_node();
	} else {
		print_error_message(argv[0]);
		return (rslt);
	}

	os::printf("    Fault node: %d\n", fault_node);

	rslt = upgrade_commit_common(
		fault_upgd_commit_task_exec_11_on,
		fault_upgd_commit_task_exec_11_off,
		fault_node, ALWAYS, e);

	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}





//
// Fault senario in cb_coordinator::callback_task_done_on_one_lca()
//
int
cb_coord_cb_task_done_1(int argc, char **argv)
{
	os::printf("*** Fault RM primary while cb_coordinator primary "
		"broadcast callback task done 1 ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;
	Environment	e;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc > 2) {
		print_error_message(argv[0]);
		return (rslt);
	}

	if (argc == 1) {
		fault_node = orb_conf::local_nodeid();
	} else if (os::strcmp(argv[1], "rm_primary") == 0) {
		fault_node = FaultFunctions::rm_primary_node();
	} else {
		print_error_message(argv[0]);
		return (rslt);
	}

	os::printf("    Fault node: %d\n", fault_node);

	rslt = upgrade_commit_common(
		fault_cb_coord_cb_task_done_1_on,
		fault_cb_coord_cb_task_done_1_off,
		fault_node, ALWAYS, e);

	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}



//
// Fault senario in cb_coordinator::callback_task_done_on_one_lca()
//
int
cb_coord_cb_task_done_2(int argc, char **argv)
{
	os::printf("*** Fault RM primary while cb_coordinator primary "
		"broadcast callback task done 2 ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;
	Environment	e;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc > 2) {
		print_error_message(argv[0]);
		return (rslt);
	}

	if (argc == 1) {
		fault_node = orb_conf::local_nodeid();
	} else if (os::strcmp(argv[1], "rm_primary") == 0) {
		fault_node = FaultFunctions::rm_primary_node();
	} else {
		print_error_message(argv[0]);
		return (rslt);
	}

	os::printf("    Fault node: %d\n", fault_node);

	rslt = upgrade_commit_common(
		fault_cb_coord_cb_task_done_2_on,
		fault_cb_coord_cb_task_done_2_off,
		fault_node, ALWAYS, e);

	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


/*

//
// Fault senario in upgrade_commit_task::execute()
//
int
upgd_commit_task_retry_1(int argc, char **argv)
{
	os::printf("*** Fault RM primary while new cb_coordinator primary "
		"retry upgrade commit task 1 ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;
	Environment	e;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc > 2) {
		print_error_message(argv[0]);
		return (rslt);
	}

	if (argc == 1) {
		fault_node = orb_conf::local_nodeid();
	} else if (os::strcmp(argv[1], "rm_primary") == 0) {
		fault_node = FaultFunctions::rm_primary_node();
	} else {
		print_error_message(argv[0]);
		return (rslt);
	}

	os::printf("    Fault node: %d\n", fault_node);

	rslt = upgrade_commit_common(
		fault_upgd_commit_task_retry_1_on,
		fault_upgd_commit_task_retry_1_off,
		fault_node, ALWAYS, e);

	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Fault senario in upgrade_commit_task::execute()
//
int
upgd_commit_task_retry_2(int argc, char **argv)
{
	os::printf("*** Fault RM primary while new cb_coordinator primary "
		"retry upgrade commit task 2 ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;
	Environment	e;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc > 2) {
		print_error_message(argv[0]);
		return (rslt);
	}

	if (argc == 1) {
		fault_node = orb_conf::local_nodeid();
	} else if (os::strcmp(argv[1], "rm_primary") == 0) {
		fault_node = FaultFunctions::rm_primary_node();
	} else {
		print_error_message(argv[0]);
		return (rslt);
	}

	os::printf("    Fault node: %d\n", fault_node);

	rslt = upgrade_commit_common(
		fault_upgd_commit_task_retry_2_on,
		fault_upgd_commit_task_retry_2_off,
		fault_node, ALWAYS, e);

	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}


//
// Fault senario in upgrade_commit_task::execute()
//
int
upgd_commit_task_retry_3(int argc, char **argv)
{
	os::printf("*** Fault RM primary while new cb_coordinator primary "
		"retry upgrade commit task 3 ***\n");

#ifdef _FAULT_INJECTION

	int		rslt;
	nodeid_t	fault_node;
	Environment	e;

	rslt = 1;

	int i;
	os::printf("Number of args is %d\n", argc);
	for (i = 0; i < argc; i++) {
		os::printf("argv %d is %s\n", i, argv[i]);
	}

	if (argc > 2) {
		print_error_message(argv[0]);
		return (rslt);
	}

	if (argc == 1) {
		fault_node = orb_conf::local_nodeid();
	} else if (os::strcmp(argv[1], "rm_primary") == 0) {
		fault_node = FaultFunctions::rm_primary_node();
	} else {
		print_error_message(argv[0]);
		return (rslt);
	}

	os::printf("    Fault node: %d\n", fault_node);

	rslt = upgrade_commit_common(
		fault_upgd_commit_task_retry_3_on,
		fault_upgd_commit_task_retry_3_off,
		fault_node, ALWAYS, e);

	return (rslt);
#else
	return (no_fault_injection());
#endif // _FAULT_INJECTION
}

*/

int
upgrade_commit_common(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	nodeid_t	fault_node,
	int		when_to_trigger,
	Environment	&e)
{
	FaultFunctions::wait_for_arg_t	reboot_args;
	// Fix for 4972416
	reboot_args.op		= FaultFunctions::NO_WAIT;
	// reboot_args.op	= FaultFunctions::WAIT_FOR_UNKNOWN;
	reboot_args.nodeid 	= fault_node;


	// Do the fault setup
	if (on_fp != NULL) {
		if (on_fp(&reboot_args, (int)sizeof (reboot_args),
		    when_to_trigger) != 0) {
			os::printf("    WARNING: Arming faults failed\n");
		}
		// Comment out later
		os::printf("    WARNING: Arming faults succeeded\n");
	} else {
		os::printf("    No Faults armed.\n");
	}

	int ret = 0;

	ret = do_commit(e);

	// Disarm faults
	if (off_fp != NULL) {
		if (off_fp(&reboot_args, (int)sizeof (reboot_args),
		    when_to_trigger) != 0) {
			os::printf("    WARNING: Disarming faults failed\n");
		}
		// Comment out later
		os::printf("    WARNING: Disarming faults succeeded\n");
	}

	return (ret);
}

int
do_commit(Environment &e)
{
	version_manager::vm_admin_var	vm_v;

	vm_v = vm_util::get_vm();
	if (CORBA::is_nil(vm_v)) {
		os::printf("ERROR: Could not find the version manager\n");
		return (1);
	}

	os::printf("+++ Issuing upgrade commit +++\n");
	os::usecsleep((long)2 * 100000);

	version_manager::vp_state state = version_manager::ALL_VERSIONS_MATCH;
	version_manager::membership_bitmask_t downrev_nodes = 0;

	vm_v->upgrade_commit(state, downrev_nodes, e);
	CORBA::Exception *expectp = e.exception();

	if (expectp == NULL) {
		if (downrev_nodes != 0) {
			os::printf("+++ Upgrade commit cannot be done because "
				"of mismatched versions on different nodes.\n");
			os::printf("+++ downrev_nodes %x +++\n", downrev_nodes);
			return (1);
		}
	} else if (expectp) {
		os::printf("+++ Upgrade commit returned unknown exception.");
		return (2);
	}

/*
	if (e.exception()) {
	    if (version_manager::can_not_upgrade::_exnarrow(e.exception())) {
		os::printf("ERROR: Could not commit upgrade.\n");
	    } else {
		os::printf("ERROR: Could not commit upgrade. "
		    "Unknown exception.\n");
	    }
	    return (1);
	}
*/
	return (0);
}
