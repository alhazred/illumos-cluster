/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)mod_fini_v0.cc	1.6	08/05/20 SMI"

#include 	<sys/os.h>
#include	<h/naming.h>
#include	<orb/invo/common.h>
#include	<sys/rm_util.h>
#include	<nslib/ns.h>

#ifdef _KERNEL_ORB
#include	<orbtest/vm_upgd_cb/repl_sample_impl_v0.h>
#include	<sys/modctl.h>
#endif

#include	<orbtest/repl_tests/repl_test_switch.h>
#include	<orbtest/rm_probe/rm_snap.h>
#include	<h/replica.h>
#include	<orb/fault/fault_injection.h>
#include	<orbtest/vm_upgd_cb/repl_sample_clnt_v0.h>


void
mod_fini_wait(int mod_id)
{
#ifdef _KERNEL

#ifdef _FAULT_INJECTION

	os::printf("+++ %d: Entering mod_fini\n", mod_id);

	//
	// Make sure that the provider(s) created by this modules have
	// been unreferenced before unloading the module. Not doing so
	// causes the kernel to refer to unmapped addresses causing
	// panics.
	//

	prov_in_mod_t *pp;
	bool found_prov;
	int time = 0;

	for (; ; ) {
		SList<prov_in_mod_t>::ListIterator
			iter(repl_sample_prov::repl_sample_prov_list);

		found_prov = false;

		repl_sample_prov::prov_list_lock.lock();

		if ((pp = iter.get_current()) != NULL) {
			os::printf("Found prov=%p, modid=%d\n", pp->provp,
			    pp->mod_id);
			found_prov = true;
		}
		repl_sample_prov::prov_list_lock.unlock();

		if (!found_prov) {
			break;
		} else {
			os::printf("%d: Waiting for all repl_sample_provs %p "
			    "to be unreferenced for %d secs.\n", mod_id,
			    pp->provp, time);
			os::usecsleep((long)10 * 1000000); // 10s
			time += 10;
		}
	}

	os::printf("+++ %d: Leaving mod_fini\n", mod_id);
#endif // _FAULT_INJECTION
#endif // _KERNEL
}
