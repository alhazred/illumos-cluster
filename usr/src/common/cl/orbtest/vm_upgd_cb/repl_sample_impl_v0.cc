/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)repl_sample_impl_v0.cc	1.8	08/05/20 SMI"

#include	<sys/modctl.h>
#include 	<sys/os.h>
#include	<h/naming.h>
#include	<nslib/ns.h>
#include	<orbtest/vm_upgd_cb/repl_sample_impl_v0.h>
#include	<orb/fault/fault_injection.h>

//
// We don't normally have that big node ids. So cast it to int16_t
// so lint does not complain.
//
int16_t			nodeid = (int16_t)orb_conf::node_number();

#if defined(_FAULT_INJECTION) && defined(_KERNEL_ORB) && !defined(_KERNEL)
//
// These are used to indicate that the serivce has completed its shutdown.
// When fault FAULTNUM_HA_FRMWK_KILL_NODE_2 is triggered, method
// Xdoor_manager::translate_in() waits on server_done_cv for shutdown to
// complete and exits when this condition variable is signalled.
//
extern os::mutex_t	server_done_mutex;
extern os::condvar_t	server_done_cv;
#endif

// Since factory_impl objects are themselves part of the service, they
// are only invoked on the primary
repl_sample::value_obj_ptr
factory_impl::create_value_obj(Environment &e)
{
	os::printf("factory_impl::create_value_obj()\n");

	// Allocate a new implementation, indicating that it is part of this
	// service
	repl_value_obj_impl * new_value_objp = new
		repl_value_obj_impl(my_provider);

	// Insert the object into a list of all objects in this service
	my_provider->list_lock.lock();
	my_provider->value_objlist.append(new_value_objp);
	my_provider->list_lock.unlock();

	// Get a CORBA reference to the object
	repl_sample::value_obj_ptr new_ref = new_value_objp->get_objref();
	// Tell the secondaries about new object
	get_checkpoint()->ckpt_create_value_obj(new_ref, e);
	// return a reference to the object
	return (new_ref);
}


// This is the factory for persist objs
repl_sample::persist_obj_ptr
factory_impl::create_persist_obj(Environment &e)
{
	os::printf("factory_impl::create_persist_obj()\n");

	// Allocate a new impl, indicating that it is part of this service
	repl_persist_obj_impl * objp = new
		repl_persist_obj_impl(my_provider);

	// insert object in list
	my_provider->list_lock.lock();
	my_provider->persist_objlist.append(objp);
	my_provider->list_lock.unlock();

	// Get a CORBA ref of the persistent object
	repl_sample::persist_obj_ptr new_ref = objp->get_objref();

	// Tell secondaries about new object
	get_checkpoint()->ckpt_create_persist_obj(new_ref, e);
	return (new_ref);
}


// _unreferenced for factories
void
factory_impl::_unreferenced(unref_t cookie)
{
	os::printf("factory_impl::_unreferenced()\n");

	if (_last_unref(cookie)) {
		my_provider->factoryp = NULL;
		my_provider->initialized = false;
		delete this;
	}
}

// Checkpoint accessor function.
repl_sample::ckpt_ptr
factory_impl::get_checkpoint()
{
	os::printf("factory_impl::get_checkpoint()\n");

	return ((repl_sample_prov*)(get_provider()))->
	    get_checkpoint_repl_sample();
}

#ifdef _KERNEL
// Static variables

SList<prov_in_mod_t> repl_sample_prov::repl_sample_prov_list;
os::mutex_t		repl_sample_prov::prov_list_lock;
#endif

repl_sample_prov::repl_sample_prov(const char *svc_desc,
	const char *repl_desc) :
		repl_server<repl_sample::ckpt> (svc_desc, repl_desc),
		factoryp(NULL),
		initialized(false),
		_ckpt_proxy(NULL)
{
	svc_nam = new char[os::strlen(svc_desc) + 1];
	(void) os::strcpy(svc_nam, svc_desc);

	// Put the persist store ref in the local_ns after getting it
	// from the root_ns
	//
	(void) get_root_to_local();


#ifdef _KERNEL

	my_id = new prov_in_mod_t;
	struct modinfo minfo;
	(void) _info(&minfo);

	my_id->provp = this;
	my_id->mod_id = minfo.mi_id;

	prov_list_lock.lock();
	repl_sample_prov_list.append(my_id);
	prov_list_lock.unlock();

	os::printf("Created repl_sample_prov %p, mod_id %d\n", this,
	    minfo.mi_id);
#endif

}

repl_sample_prov::~repl_sample_prov()
{
	delete[] svc_nam;
	//
	// svc_nam is deleted above and factoryp would be deleted by
	// unreference. So suppress lint errors.
	//
	svc_nam = NULL; //lint -e1740
	factoryp = NULL; //lint -e1740

	os::printf("Deleting repl_sample_prov %p\n", this);

#ifdef _KERNEL
	prov_list_lock.lock();
	(void) repl_sample_prov_list.erase(my_id);
	prov_list_lock.unlock();
	delete my_id;
	my_id = NULL; // make lint happy
#endif

	_ckpt_proxy = NULL;
}

// Implementation of the checkpoint methods
void
repl_sample_prov::ckpt_create_value_obj(
	repl_sample::value_obj_ptr obj,
	Environment &)
{
	os::printf("repl_sample_prov::ckpt_create_value_obj()\n");

	// Create a shadow copy of the object associating it with the reference
	// passed in
	repl_value_obj_impl * objp = new repl_value_obj_impl(obj, this);
	// Inset the object in a list of all value objects in this service
	list_lock.lock();
	value_objlist.append(objp);
	list_lock.unlock();
}

void
repl_sample_prov::ckpt_create_persist_obj(
	repl_sample::persist_obj_ptr obj,
	Environment &)
{
	os::printf("repl_sample_prov::ckpt_create_persist_obj()\n");

	// Use the replicated store
	repl_persist_obj_impl * objp =
		new repl_persist_obj_impl(obj, this);

	// Insert object into a list of persist objects in this service
	list_lock.lock();
	persist_objlist.append(objp);
	list_lock.unlock();
}

void
repl_sample_prov::ckpt_value_obj(
	repl_sample::value_obj_ptr obj,
	int32_t val,
	Environment &)
{
	os::printf("repl_sample_prov::ckpt_value_obj()\n");

	// Create a shadow cope of the object
	repl_value_obj_impl * objp = new repl_value_obj_impl(obj, this);
	// Set the value of this object to checkpointer value
	objp->_value(val);

	// Insert the object in a list of vallue objects
	list_lock.lock();
	value_objlist.append(objp);
	list_lock.unlock();
}

void
repl_sample_prov::ckpt_persist_obj(
	repl_sample::persist_obj_ptr obj,
	int32_t val,
	Environment &)
{
	os::printf("repl_sample_prov::ckpt_persist_obj()\n");

	repl_persist_obj_impl * objp =
		new repl_persist_obj_impl(obj, this);

	// Set the value of the replicated persistent object
	objp->_value(val);

	// Insert the object into a list of persistent objects in this service
	list_lock.lock();
	persist_objlist.append(objp);
	list_lock.unlock();
}

void
repl_sample_prov::ckpt_factory(
	repl_sample::factory_ptr obj,
	Environment &)
{
	os::printf("repl_sample_prov::ckpt_factory()\n");

	// Create a shadow copy and copy it to the global variable
	factoryp = new factory_impl(obj, this);

	// Mark this service as initialized
	initialized = true;
}

void
repl_sample_prov::ckpt_set_value(
	repl_sample::value_obj_ptr obj,
	int32_t val,
	Environment &)
{
	os::printf("repl_sample_prov::ckpt_set_value(%x, %d)\n", obj, val);
	// Update the value of the object
	repl_value_obj_impl * objp =
		(repl_value_obj_impl *)obj->_handler()->get_cookie();
	ASSERT(objp != NULL);

	// Update the value
	objp->_value(val);

	// No state object is needed in this case
}

void
repl_sample_prov::ckpt_inc_value(
	repl_sample::value_obj_ptr obj,
	int32_t val,
	Environment &e)
{
	os::printf("repl_sample_prov::ckpt_inc_value()\n");

	// Update the value of the object
	repl_value_obj_impl * 	objp =
		(repl_value_obj_impl *)obj->_handler()->get_cookie();
	ASSERT(objp != NULL);

	// Set the value
	objp->_value(val);

	// Create a state object in case of retries
	value_inc_state * st = new value_inc_state(val);
	st->register_state(e);
	if (e.exception()) {
		e.exception()->print_exception("Failed to register_state:");
		e.clear();
	}
}

void
repl_sample_prov::ckpt_set_persist(
	repl_sample::persist_obj_ptr obj,
	int32_t val,
	Environment &)
{
	os::printf("repl_sample_prov::ckpt_set_persist()\n");

	// This is the checkpoint received when the cache value for a persisten
	// object needs to be updated

	repl_persist_obj_impl * objp =
		(repl_persist_obj_impl *)obj->_handler()->get_cookie();
	ASSERT(objp != NULL);

	// Set the value
	objp->_value(val);

	// We're done
	// No state object is needed in this case

}

void
repl_sample_prov::ckpt_inc_persist_intent(
	repl_sample::persist_obj_ptr obj,
	int32_t val,
	Environment &e)
{
	os::printf("repl_sample_prov::ckpt_inc_persist_intent()\n");

	repl_sample::persist_obj_ptr obj_ref =
		repl_sample::persist_obj::_duplicate(obj);

	persist_inc_state * st = new persist_inc_state(obj_ref, val);
	st->register_state(e);
}

void
repl_sample_prov::ckpt_inc_persist_failure(
	Environment &e)
{
	os::printf("repl_sample_prov::ckpt_inc_persist_failure()\n");

	secondary_ctx * ctxs = secondary_ctx::extract_from(e);
	persist_inc_state * st;

	// Get the state
	st = (persist_inc_state *)ctxs->get_saved_state();
	ASSERT(st != NULL);
	st->my_progress = persist_inc_state::FAILED;
}


// The IDL Methods for replica::repl_prov
#ifdef FAULT_HA_FRMWK
void
repl_sample_prov::become_secondary(Environment &e)
#else
void
repl_sample_prov::become_secondary(Environment &)
#endif
{
	os::printf("repl_sample_prov::become_secondary()\n");

	CORBA::release(_ckpt_proxy);
	_ckpt_proxy = nil;

#ifdef FAULT_HA_FRMWK
	// FAULT POINT
	// This fault point will cause become_primary to return an exception
	void		*fault_argp;
	uint32_t	fault_argsize;

	if (fault_triggered(
	    FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_BECOME_SECONDARY,
	    &fault_argp, &fault_argsize)) {
		os::printf("repl_sample_prov::become_secondary(): "
		    "FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_BECOME_SECONDARY\n");
		CORBA::Exception	*p_ex;

		// This fault will set some exception and return
		p_ex = new replica::repl_prov_failed;

		// Set the exception
		e.exception(p_ex);
		return;
	} // Fault Point
#endif
}

void
repl_sample_prov::add_secondary(
	replica::checkpoint_ptr sec,
	const char *,
	Environment &e)
{
	uint_t	i = 0;
	os::printf("repl_sample_prov::add_secondary()\n");

	repl_sample::ckpt_var ckpt_v = repl_sample::ckpt::_narrow(sec);

	ASSERT(!CORBA::is_nil(ckpt_v));

	// Dump the factory if it exists
	if (factoryp != NULL) {
		os::printf("Dumping state...\n");

		repl_sample::factory_var factory_ref = factoryp->get_objref();
		ckpt_v->ckpt_factory(factory_ref, e);
		if (e.exception()) {
			// XXX - I thought no exceptions could occur
			// but appears that in this case there can be
			// an exception
			// So just clear it (ignore it)
			e.exception()->print_exception(
			    "repl_sample::ckpt::ckpt_factory():");
			e.clear();
		}

		FAULTPT_HA_FRMWK(
		    FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_ADD_SECONDARY,
		    FaultFunctions::generic);

		// The value objects
		repl_value_obj_impl * value_objp;

		// The persist object
		repl_persist_obj_impl * persist_objp;

		int32_t value;

		// Dump the value_objlist
		list_lock.lock();
		for (value_objlist.atfirst();
			(value_objp = value_objlist.get_current()) != NULL;
			value_objlist.advance(), i++) {

			// Get a reference (CORBA) to the current object
			repl_sample::value_obj_var value_obj_ref;
			value_obj_ref = value_objp->get_objref();
			// Get the value
			int16_t tmpval;		// tmp val
			value = value_objp->get_value(tmpval, e);

			// Checkpoint the value in the object
			ckpt_v->ckpt_value_obj(value_obj_ref, value, e);
			if (e.exception()) {
				// XXX - I thought no exceptions could occur
				// but appears that in this case there can be
				// an exception
				// So just clear it (ignore it)
				e.exception()->print_exception(
				    "repl_sample::ckpt::ckpt_value_obj():");
				e.clear();
			}
		}

		// Dump the persist_objlist
		for (persist_objlist.atfirst();
			(persist_objp = persist_objlist.get_current()) != NULL;
			persist_objlist.advance(), i++) {

			// Get a reference (CORBA) to the current object
			repl_sample::persist_obj_var persist_obj_ref;
			persist_obj_ref = persist_objp->get_objref();
			// Get the value
			int16_t	tmpval;		// tmpval
			value = persist_objp->get_value(tmpval, e);

			ckpt_v->ckpt_persist_obj(
				persist_obj_ref,
				value,
				e);
			if (e.exception()) {
				// XXX - I thought no exceptions could occur
				// but appears that in this case there can be
				// an exception
				// So just clear it (ignore it)
				e.exception()->print_exception(
				    "repl_sample::ckpt::ckpt_persist_obj():");
				e.clear();
			}
		}
		list_lock.unlock();
	}

	os::printf("Dumped %d objects\n", i);
}


void
repl_sample_prov::remove_secondary(
	const char *, Environment &)
{
	os::printf("repl_sample_prov::remove_secondary()\n");
}


void
repl_sample_prov::become_primary(const replica::repl_name_seq &, Environment &e)
{
	os::printf("repl_sample_prov::become_primary()\n");

	// First, initialize the checkpoint proxy.
	replica::checkpoint_var tmp_ckptp =
		set_checkpoint(repl_sample::ckpt::_get_type_info(0));
	_ckpt_proxy = repl_sample::ckpt::_narrow(tmp_ckptp);

	ASSERT(!CORBA::is_nil(_ckpt_proxy));

#ifdef FAULT_HA_FRMWK

	// This fault point will cause become_primary to return an exception
	if (fault_triggered(FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_BECOME_PRIMARY,
	    NULL, NULL)) {
		CORBA::Exception	*p_ex;

		// This fault will set some exception and return
		p_ex = new replica::repl_prov_failed;

		// When returning exception, release the checkpoint.
		CORBA::release(_ckpt_proxy);
		_ckpt_proxy = nil;

		// Set the exception
		e.exception(p_ex);
		return;
	} // Fault Point
#endif

	if (!initialized) {
		// Call start service to create the factory
		initialized = true;
		start_service(e);

	}

#ifdef FAULT_HA_FRMWK
	repl_persist_obj_impl * persist_objp;

	for (persist_objlist.atfirst();
		(persist_objp = persist_objlist.get_current()) != NULL;
		persist_objlist.advance()) {
		Environment env;
		persist_objp->update_storage(env);

	}
#endif

}

CORBA::Object_ptr
repl_sample_prov::get_root_obj(Environment &)
{
	os::printf("repl_sample_prov::get_root_obj()\n");

	ASSERT(factoryp != NULL);
	return (factoryp->get_objref());
}

void
repl_sample_prov::become_spare(Environment &)
{
	os::printf("repl_sample_prov::become_spare()\n");


	repl_value_obj_impl * value_objp;
	repl_persist_obj_impl * persist_objp;

	list_lock.lock();
	while ((value_objp = value_objlist.reapfirst()) != NULL) {
		delete value_objp;
	}

	while ((persist_objp = persist_objlist.reapfirst()) != NULL) {
		delete persist_objp;
	}
	list_lock.unlock();

	delete factoryp;
	factoryp = NULL;  //lint !e423
	initialized = false;
}

void
repl_sample_prov::freeze_primary_prepare(Environment &)
{
	os::printf("repl_sample_prov::freeze_primary_prepare()\n");
}

void
repl_sample_prov::freeze_primary(Environment &)
{
	os::printf("repl_sample_prov::freeze_primary()\n");
}

void
repl_sample_prov::unfreeze_primary(Environment &)
{
	os::printf("repl_sample_prov::unfreeze_primary()\n");
}

void
repl_sample_prov::common_shutdown()
{
	os::printf("repl_sample_prov::common_shutdown()\n");

	char factory_name[50];
	os::sprintf(factory_name, "%s factory", svc_nam);

	// Nested invocation requires its own Environment
	Environment	env;

	// Unbind the factory in the name server
	naming::naming_context_var	ctxp = ns::root_nameserver();
	ctxp->unbind((const char *)factory_name, env);
	//
	// If the service should not wait for all references to go
	// away, i.e. wait_for_unref = 0, then the primary may have
	// unbound factory object before shutdown() is called on
	// secondaries.  Trying to unbind an unreferenced object from
	// nameserver will return a not_found exception on secondaries.
	//
	if (env.exception()) {
		os::printf("++ factory not found \n");
		ASSERT(naming::not_found::_exnarrow(env.exception()));
		env.clear();
	}
	factoryp = NULL; //lint !e423
	initialized = false;

#if defined(_FAULT_INJECTION) && defined(_KERNEL_ORB) && !defined(_KERNEL)
	server_done_mutex.lock();
	server_done_cv.signal();
	server_done_mutex.unlock();
#endif
}

uint32_t
repl_sample_prov::forced_shutdown(Environment &)
{
	os::printf("repl_sample_prov::forced_shutdown()\n");

	os::printf("++ factoryp = %p\n", factoryp);

	list_lock.lock();
	os::printf("Value Objlist: %d, Persist Objlist: %d\n",
	    value_objlist.count(), persist_objlist.count());
	list_lock.unlock();

	common_shutdown();

	// Release the reference to the checkpoint.
	CORBA::release(_ckpt_proxy);
	_ckpt_proxy = nil;

	return (0);
}

void
repl_sample_prov::shutdown(Environment &)
{
	os::printf("repl_sample_prov::shutdown()\n");

	os::printf("++ factoryp = %p\n", factoryp);

	int	wait_for_unref = 1;

#ifdef FAULT_HA_FRMWK
	if (fault_triggered(
	    FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_SHUTDOWN_W_O_WAITING_FOR_UNREF,
	    NULL, NULL)) {
		wait_for_unref = 0;
	}
#endif

	list_lock.lock();
	os::printf("Value Objlist: %d, Persist Objlist: %d\n",
		    value_objlist.count(), persist_objlist.count());
	list_lock.unlock();

	os::printf("Verifying that all HA objects have been unreferenced: ");

	while (wait_for_unref) {
		list_lock.lock();
		if (value_objlist.empty() && persist_objlist.empty()) {
			list_lock.unlock();
			break;
		}
		list_lock.unlock();

		// Spin while these lists arent empty
		os::printf(".");
		os::usecsleep(3L * 1000000);
	}
	os::printf(" DONE!\n");

	common_shutdown();

	CORBA::release(_ckpt_proxy);
	_ckpt_proxy = nil;
}

// Miscelaneous methods
void
repl_sample_prov::start_service(Environment &e)
{
	os::printf("repl_sample_prov::start_service()\n");

	factoryp = new factory_impl(this);  //lint !e423

	char	factory_name[50];

	// Checkpoint the factory
	repl_sample::factory_var factory_ref = factoryp->get_objref();

	factoryp->get_checkpoint()->ckpt_factory(factory_ref, e);
	if (e.exception())
		os::panic("Exception in checkpoint");

	os::sprintf(factory_name, "%s factory", svc_nam);

	// Bind the factory in the name server
	Environment env;
	naming::naming_context_var ctxp = ns::root_nameserver();
	ctxp->rebind((const char *)factory_name, factory_ref, env);
	ASSERT(!env.exception());
}


// The Actual Object IDL members
// First the volatile object

// Get the value stored in the object
int32_t
repl_value_obj_impl::get_value(int16_t & nodenum, Environment &)
{
	os::printf("repl_value_obj_impl::get_value()\n");

	nodenum = nodeid;
	// os::printf("value_obj_impl::get_value()\n");
	// This is an example of a Basic mini-transaction
	// It is idempotent (we are only reporting a value)
	// It can be re-tried without any special protocol
	// No need to checkpoint
	int32_t rslt;

	// FAULT POINT
	// Generic fault point during the invocation.  Before any significant
	// processing can be completed.
	// Retries will trgger this fault again.
	FAULTPT_HA_FRMWK(
		FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_GET_VALUE,
		FaultFunctions::generic);

	lck.lock();
	rslt = _value();
	lck.unlock();

	return (rslt);
}

// Set the value stored in the object
void
repl_value_obj_impl::set_value(int32_t val, int16_t &nodenum, Environment &e)
{
	os::printf("repl_value_obj_impl::set_value()\n");

	nodenum = nodeid;
	// os::printf("value_obj_impl::set_value(%d)\n", val);
	// This is an example of a 1-phase mini-transaction
	// It is idempotent (we can re-try without affecting outcome)

	// FAULT POINT
	// This generic fault point will be triggered during an invocation.
	// (This is a One phase idempotent mini transaction)
	FAULTPT_HA_FRMWK(
		FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_SET_VALUE,
		FaultFunctions::generic);

	lck.lock();
	_value(val);

	repl_sample::ckpt_ptr ckptp = get_checkpoint();
	repl_sample::value_obj_var objvar = get_objref();

	ckptp->ckpt_set_value(objvar, val, e);
	if (e.exception())
		os::panic("Exception in checkpoint");
	lck.unlock();
}


// Increase the value stored in the object
int32_t
repl_value_obj_impl::inc_value(int16_t &nodenum, Environment &e)
{
	os::printf("repl_value_obj_impl::inc_value()\n");

	nodenum = nodeid;
	// os::printf("value_obj_impl::inc_value()\n");
	// This is an example of a 1-phase non-indempotent mini-transaction
	// The service needs to keep the state of the value after it has been
	// increased.  Otherwise a retry might update twice.

	// FAULT POINT
	// Generic fault point during the invocation.  Before any processing
	// can take place.  (1-Phase non-idempotent)
	FAULTPT_HA_FRMWK(
		FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_INC_VALUE,
		FaultFunctions::generic);

	int32_t rslt;

	lck.lock();

	// Check for saved state
	primary_ctx * ctxp = primary_ctx::extract_from(e);
	value_inc_state * st = (value_inc_state *) ctxp->get_saved_state();

	if (st) {
		// Found saved state
		os::printf("Found saved state\n");
		// Use the saved state value to return
		rslt = st->value;
	} else {

		// Otherwise update value
		_value(_value() + 1);
		rslt = _value();

		// Checkpoint the update to the secondaries
		repl_sample::ckpt_ptr ckptp = get_checkpoint();
		repl_sample::value_obj_var objvar = get_objref();

		ckptp->ckpt_inc_value(objvar, rslt, e);
		if (e.exception())
			os::panic("Exception in checkpoint");
	}

	lck.unlock();
	return (rslt);
}


// Unreferenced
void
repl_value_obj_impl::_unreferenced(unref_t cookie)
{
	os::printf("repl_value_obj_impl::_unreferenced()\n");
	if (_last_unref(cookie)) {
		ASSERT(my_provider != NULL);

		my_provider->list_lock.lock();
		(void) my_provider->value_objlist.erase(this);
		my_provider->list_lock.unlock();
		delete this;
	}
}

// Checkpoint accessor function.
repl_sample::ckpt_ptr
repl_value_obj_impl::get_checkpoint()
{
	os::printf("repl_value_obj_impl::get_checkpoint()\n");

	return ((repl_sample_prov*)(get_provider()))->
	    get_checkpoint_repl_sample();
}


// Constructor for persistent_obj
// Primary Constructor
repl_persist_obj_impl::repl_persist_obj_impl(repl_sample_prov *server) :
	/* CSTYLED */
	mc_replica_of<repl_sample::persist_obj> (server),
	my_provider(server)
{
	os::printf("repl_persist_obj_impl::repl_persist_obj_impl(1)\n");

	// Get a reference to the persisten storage object
	Environment e;
	naming::naming_context_var ctxp = ns::root_nameserver();
	CORBA::Object_var obj_ref =
		ctxp->resolve("ha sample persist store", e);
	if (e.exception()) {
		e.exception()->print_exception
			("Getting persistent object from root node");
		ASSERT(0);
	}

	// Make this object a smart reference to persisten_store
	storage = repl_sample::persistent_store::_narrow(obj_ref);

	// Set value to 0
	value = 0;

	ASSERT(!CORBA::is_nil(storage));
}

// Secondary constructor
repl_persist_obj_impl::repl_persist_obj_impl(
    repl_sample::persist_obj_ptr ref,
    repl_sample_prov *server) :
	/* CSTYLED */
	mc_replica_of<repl_sample::persist_obj> (ref),
	my_provider(server)
{
	os::printf("repl_persist_obj_impl::repl_persist_obj_impl(2)\n");

	// Secondaries should not do any 'actual work'.
	// In this case, getting a ref to the persist_obj at this time
	// leads to deadlock (4195278). Solution is to move that
	// 'work' to become_primary.

	// Set value to 0
	value = 0;

}

// unreferenced
void
repl_persist_obj_impl::_unreferenced(unref_t cookie)
{
	os::printf("repl_persist_obj_impl::_unreferenced()\n");

	if (_last_unref(cookie)) {
		ASSERT(my_provider != NULL);

		my_provider->list_lock.lock();
		(void) my_provider->persist_objlist.erase(this);
		my_provider->list_lock.unlock();
		delete this;
	}
}


// Implementation for persisten object IDL Interface
int32_t
repl_persist_obj_impl::get_value(int16_t &nodenum, Environment &)
{
	os::printf("repl_persist_obj_impl::get_value()\n");

	nodenum = nodeid;
	// os::printf("get_value()\n");
	// Simple Mini-transaction
	int32_t rslt;

	lck.lock();
	rslt = _value();
	lck.unlock();

	return (rslt);
}

void
repl_persist_obj_impl::set_value(int32_t val, int16_t &nodenum, Environment &e)
{
	os::printf("repl_persist_obj_impl::set_value()\n");

	nodenum = nodeid;
	// os::printf("set_value()\n");
	// this is idempotent 1-phase mini-transaction, because setting
	// the value twice on the persistent storage has no side-effects
	Environment tmp_env;

	lck.lock();

	// Update persistent storage
	storage->set_value(val, tmp_env);
	if (tmp_env.exception()) {
		// Return an exception to the caller
		CORBA::Exception	*p_ex;
		p_ex = new repl_sample::could_not_update;
		e.exception(p_ex);
		lck.unlock();
		return;
	} else {
		// Set the cached value
		_value(val);

		// Checkpoint to secondaries
		repl_sample::persist_obj_var obj_ref = get_objref();
		get_checkpoint()->ckpt_set_persist(obj_ref, val, e);
		if (e.exception()) {
			os::panic("Exception during checkpoint");
		}
	}

	lck.unlock();
}

int32_t
repl_persist_obj_impl::inc_value(int16_t &nodenum, Environment &e)
{
	os::printf("repl_persist_obj_impl::inc_value()\n");

	nodenum = nodeid;
	// os::printf("inc_value()\n");
	// This is a 2-phase mini-transaction
	// We must first checkpoint our intention to increment the persistent
	// storage, and then either checkpoint failure, or commit success.

	primary_ctx * ctxp = primary_ctx::extract_from(e);
	persist_inc_state * saved_state;
	int32_t rslt;

	// Check for saved state
	if ((saved_state = (persist_inc_state *)ctxp->get_saved_state()) ==
		NULL) {
		// Common case : this is the first time
		lck.lock();

		rslt = _value() + 1;

		// FAULT POINT
		// This generic fault point will trigger a fault prior to the
		// first checkpoint of a 2-phase mini-transaction
		FAULTPT_HA_FRMWK(
			FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_INC_VALUE_2,
			FaultFunctions::generic);

		// Get a CORBA reference
		repl_sample::persist_obj_var obj_ref = get_objref();
		get_checkpoint()->ckpt_inc_persist_intent(obj_ref, rslt, e);
		if (e.exception()) {
			os::panic("Exception during checkpoint");
		}

		// FAULT POINT
		// This generic fault point will trigger a fault after the
		// first checkpoint of a 2-phase mini-transaction
		FAULTPT_HA_FRMWK(
			FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_INC_VALUE_3,
			FaultFunctions::generic);

		// Attempt to update persist store
		_set_inc_value(rslt, e);

		lck.unlock();
	} else {
		// This must be a re-try
		rslt = saved_state->handle_retry(e);
	}

	return (rslt);
}


// Implementation interface to set the inc value
void
repl_persist_obj_impl::_set_inc_value(int32_t val, Environment &e)
{
	os::printf("repl_persist_obj_impl::_set_inc_value()\n");

	Environment tmp_env;

	// Attempt to update persistent storage
	storage->set_value(val, tmp_env);

	// FAULT POINT
	// This fault point will allow the injection of a fault after the
	// action has taken place.  But before either a second checkpoint
	// or a commit is done.
	FAULTPT_HA_FRMWK(
		FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_SET_INC_VALUE,
		FaultFunctions::generic);

	if (tmp_env.exception()) {
		_ckpt_inc_failure(e);
		if (e.exception()) {
			os::panic("Exception during checkpoint");
		}

		CORBA::Exception	*p_ex;
		p_ex = new repl_sample::could_not_update;
		e.exception(p_ex);
	} else {
		// update local cache
		_value(val);
		add_commit(e);
	}
}

// Implementation interface to signal a failed checkpoint
void
repl_persist_obj_impl::_ckpt_inc_failure(Environment &e)
{
	os::printf("repl_persist_obj_impl::_ckpt_inc_failure()\n");

	get_checkpoint()->ckpt_inc_persist_failure(e);
	if (e.exception())
		os::panic("Exception during checkpoint");
}

repl_sample::ckpt_ptr
repl_persist_obj_impl::get_checkpoint()
{
	os::printf("repl_persist_obj_impl::get_checkpoint()\n");

	return ((repl_sample_prov*)(get_provider()))->
	    get_checkpoint_repl_sample();
}

// Checkpoint accessor function.
repl_sample::ckpt_ptr
repl_sample_prov::get_checkpoint_repl_sample()
{
	os::printf("repl_sample_prov::get_checkpoint_repl_sample()\n");

	return (_ckpt_proxy);
}


// Interface to be invoked during ORB::join_cluster to update the
// local ns of a rejoining node
//
int
repl_sample_prov::get_root_to_local()
{
	os::printf("repl_sample_prov::get_root_to_local()\n");

	Environment e;
	naming::naming_context_ptr gctxp = ns::root_nameserver();
	repl_sample::persistent_store_var store_ref;
	CORBA::Object_var obj_ref;

	obj_ref = gctxp->resolve("ha sample persist store", e);
	CORBA::release(gctxp);
	if (!e.exception() && !CORBA::is_nil(obj_ref)) {
		naming::naming_context_ptr lctxp = ns::local_nameserver();

		store_ref = repl_sample::persistent_store::_narrow(obj_ref);
		lctxp->rebind("ha sample persist store", store_ref, e);
		CORBA::release(lctxp);
		ASSERT(!e.exception());
		return (0);
	}
	ASSERT(0);

	/* NOTREACHED */
	return (1);
}

//
// Implementation interface to get storage ref from the local ns
// Solution for bugid 4195278 - this also depends on stuff
// in files persist_store*
//
void
repl_persist_obj_impl::update_storage(Environment &e)
{
	os::printf("repl_persist_obj_impl::update_storage()\n");

	naming::naming_context_ptr ctxp = ns::local_nameserver();
	CORBA::Object_var obj_ref =
		ctxp->resolve("ha sample persist store", e);

	CORBA::release(ctxp);

	if (e.exception()) {
		e.exception()->print_exception("Getting persist store");
		ASSERT(0);
	}

	// Make this a ptr to the persist store
	storage = repl_sample::persistent_store::_narrow(obj_ref);
	ASSERT(!CORBA::is_nil(storage));
}

// Methods for the persistent state objct
void
persist_inc_state::committed()
{
	// This is called when the commit for a checkpoint is received
	// It is the SUCCESS of a 2-phase minitransaction
	os::printf("persist_inc_state::comitted()\n");

	// We can progress to COMMITTED
	my_progress = COMMITTED;

	// Set the local cache to the checkpointed value
	repl_persist_obj_impl * objp =
		(repl_persist_obj_impl *) obj_ref->_handler()->get_cookie();

	objp->_value(value);
}

// Helper function for retries
int32_t
persist_inc_state::handle_retry(Environment &e)
{
	os::printf("persist_inc_state::handle_retry()\n");

	// Use a local env.
	Environment tmp_env;

	int32_t retval = 0;
	int32_t persist_val = 0;

	// Get the underlying implementation object
	repl_persist_obj_impl * objp =
		(repl_persist_obj_impl *) obj_ref->_handler()->get_cookie();

	ASSERT(objp != NULL);

	switch (my_progress) {
	case STARTED:
		// We need to recover.  We should check and see if the previous
		// attempt at this operation succeeded
		persist_val = objp->_persist_value(tmp_env);
		if (tmp_env.exception()) {
			CORBA::Exception	*p_ex;
			p_ex = new repl_sample::could_not_update;
			e.exception(p_ex);
			objp->_ckpt_inc_failure(e);
			if (e.exception()) {
				os::panic("Exception during checkpoint\n");
			}
		} else {
			// Verify whether the previous operation succeeded
			if (persist_val == value) {
				// Operation succeded
				// Set the underlying value (local cache)
				objp->_value(value);
				retval = value;
			} else {
				// Retry the update
				objp->_set_inc_value(value, e);
				retval = value;
			}
		} // Exception
		break;
	case COMMITTED:
		// Just return what we had saved
		retval = value;
		break;
	case FAILED:
		// Return some exception
		CORBA::Exception	*p_ex;
		p_ex = new repl_sample::could_not_update;
		e.exception(p_ex);
		break;
	} // SWITCH

	return (retval);
}


// unreferenced
void
#ifdef DEBUG
persistent_store_impl::_unreferenced(unref_t cookie)
#else
persistent_store_impl::_unreferenced(unref_t)
#endif
{
	os::printf("persistent_store_impl::_unreferenced()\n");
	ASSERT(_last_unref(cookie));

	delete this;
}


// Methods of the persistent store object
#ifdef FAULT_HA_FRMWK
void
persistent_store_impl::set_value(int32_t val, Environment &e)
{
	os::printf("persistent_store_impl::set_value()\n");

	if (fault_triggered(
	    FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_PERSIST_STORE_SET_VALUE,
	    NULL, NULL)) {

		// This fault will set some exception and return
		CORBA::Exception	*p_ex;
		p_ex = new repl_sample::could_not_update;

		// Set the exception
		e.exception(p_ex);
		return;
	} // Fault Point
	value = val;
}
#else
void
persistent_store_impl::set_value(int32_t val, Environment &)
{
	os::printf("persistent_store_impl::set_value()\n");

	value = val;
}
#endif // FAULT_HA_FRMWK
