/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _REPL_SAMPLE_CLNT_V0_H
#define	_REPL_SAMPLE_CLNT_V0_H

#pragma ident	"@(#)repl_sample_clnt_v0.h	1.7	08/05/20 SMI"

#include	<orb/invo/common.h>
#include	<h/replica.h>
#include	<h/repl_sample_v0.h>

//
// These are the functions that drive a specific mini_transactions
// They must be passed a pointer to a function that will activate the fault
// points for this test. And a pointer to a functions to de-activate the faults
// after the test has been run.  (These may be NULL).
// These functions are called immediate before and after the invocation that
// is being tested is called.
//

// Macro for the function to turn on fault injection
#define	FAULT_FPTR(func)	int (*func) (void *, int)

// value_obj::get_value
extern int simple_mini_1(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void			*fault_argp,
	int			fault_arglen,
	Environment		&e,
	char			*svc_name = "ha_sample_server",
	bool			do_setup_work = true);

// value_obj::set_value (idempotent)
extern int one_phase_mini_1(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void			*fault_argp,
	int			fault_arglen,
	Environment		&e,
	char			*svc_name = "ha_sample_server",
	bool			do_setup_work = true);

// value_obj::inc_value (non-idempotent)
extern int one_phase_mini_2(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void			*fault_argp,
	int			fault_arglen,
	Environment		&e,
	char			*svc_name = "ha_sample_server",
	bool			do_setup_work = true);

// persist_obj::inc_value
extern int two_phase_mini_1(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void			*fault_argp,
	int			fault_arglen,
	Environment		&e,
	char			*svc_name = "ha_sample_server",
	bool			do_setup_work = true);

// change sec state
// Changes the state of the 1st secondary of the service
extern int change_sec_state(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void			*fault_argp,
	int			fault_arglen,
	char			*svc_desc,
	replica::change_op	new_state,
	Environment		&e,
	bool			do_setup_work = true);

// change primary state
// Changes the state of the primary of the service
extern int change_primary_state(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void			*argp,
	int			arglen,
	char			*service_name,
	replica::change_op	new_state,
	Environment		&e,
	bool			do_setup_work = true);

// change provider state
// Changes the state of the specified provider to the
// specified new state. The operation may or may not
// succeed depending on whether the request is valid
// for that particular provider at that moment
extern int change_provider_state(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void			*argp,
	int			arglen,
	char			*svc_desc,
	char			*prov_desc,
	replica::change_op	new_state,
	Environment		&e,
	bool			do_setup_work = true);

// Change desired number of secondaries
extern int change_desired_num_secs(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void			*argp,
	int			arglen,
	char			*service_name,
	uint32_t		num_secs,
	Environment		&e,
	bool			do_setup_work = true);

// Change the priority of providers
extern int change_repl_prov_priority(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void				*argp,
	int				arglen,
	char				*service_name,
	replica::prov_priority_list	*prov_list,
	Environment			&e,
	bool				do_setup_work = true);

// Switch the state of provider
extern int switchover_prov_state(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void			*argp,
	int			arglen,
	struct opt_t		*p_opt,
	Environment		&e,
	bool			do_setup_work = true);

// Shutdown service
extern int shutdown_service(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void			*fault_argp,
	int			fault_arglen,
	char			*svc_desc,
	Environment		&e,
	bool			is_forced_shutdown = false);

extern int shutdown_service_nowork(
	char			*svc_desc,
	Environment		&e,
	bool			is_forced_shutdown = false);

#if defined(_KERNEL_ORB)

// register_with_rm client
extern int register_with_rm(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void			*fault_argp,
	int			fault_arglen,
	char			*svc_desc,
	char			*prov_desc,
	Environment		&e,
	int			argc = 0,
	char			**argv = NULL);

extern int drop_tr_info(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void		*argp,
	int		arglen,
	Environment	&e,
	char		*svc_name,
	bool		do_setup_work);

extern int kill_node(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void		*argp,
	int		arglen,
	Environment	&e,
	char		*svc_name,
	bool		do_setup_work);

#endif // _KERNEL_ORB

//
// Helper functions to get a reference to an HA object
// from the specified service, and initialize it to a value
//
repl_sample::value_obj_ptr init_value_obj(char *svc_name, int32_t val);
repl_sample::persist_obj_ptr init_persist_obj(char *svc_name, int32_t val);

// Do work on a service
int do_work(char *);

#ifdef _FAULT_INJECTION
int
register_generic(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void *argp,
	int arglen,
	char *fn_name);
#endif

//
// get_control
// Gets the Service Admin interface for a specific service
//
// Arguments:
//	A string of the service name
//	A reference to the rma
// Returns:
//	A referernce to the service admin object
//	NULL in case of error
replica::service_admin_ptr
get_control(
	const char *service_desc,
	const replica::rm_admin_var &rma_ref,
	Environment &e);

#endif	/* _REPL_SAMPLE_CLNT_V0_H */
