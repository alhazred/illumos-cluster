/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)repl_sample_common_mod_v0.cc	1.6	08/05/20 SMI"

#include <sys/modctl.h>
#include <sys/errno.h>

#include <sys/os.h>
#include <cplplrt/cplplrt.h>

// #include <orbtest/vm_upgd_cb/mod_fini_v0.h>
#include <orbtest/vm_upgd_cb/repl_sample_impl_v0.h>

// extern "C" void _cplpl_init(void);
// extern "C" void _cplpl_fini(void);

/* inter-module dependencies */
char _depends_on[] = "misc/cl_runtime misc/cl_load misc/cl_orb "
	"misc/cl_haci misc/cl_quorum misc/cl_comm misc/cl_dcs";

// extern struct mod_ops mod_miscops;

static struct modlmisc modlmisc = {
	&mod_miscops, "repl_sample v0 replicated service",
};

static struct modlinkage modlinkage = {
//	MODREV_1, { (void *)&modlmisc, NULL }
	MODREV_1, { (void *)&modlmisc, NULL, NULL, NULL }
};

struct modctl	*mp;

int
_init(void)
{
	int	error;

	if ((error = mod_install(&modlinkage)) != 0) {
		return (error);
	}
	_cplpl_init();		/* C++ initialization */

	// Get module name so we know what server to run.
	if ((mp = mod_getctl(&modlinkage)) == NULL) {
		os::printf("Can't find modctl\n");
		error = mod_remove(&modlinkage);
		_cplpl_fini();
		return (EIO);
	}

	os::printf("Module repl_sample_common_mod_v0 loaded\n");

	return (0);
}

int
_fini(void)
{
	os::printf("Module repl_sample_common_mod_v0 unloaded\n");
	return (EBUSY);
/*
	return (0);
*/
}

int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}
