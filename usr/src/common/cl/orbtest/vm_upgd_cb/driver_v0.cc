/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)driver_v0.cc	1.6	08/05/20 SMI"

#if defined(_KERNEL)
#include <sys/modctl.h>
#include <orbtest/vm_upgd_cb/repl_sample_clnt_v0.h>
// #include <orbtest/vm_upgd_cb/mod_fini_v0.h>

#else
#include <orb/infrastructure/orb.h>
#endif

#include <cplplrt/cplplrt.h>

#include <orbtest/fi_support/fi_driver.h>

#include <orbtest/vm_upgd_cb/faults/entry_basic_v0.h>
#include <orbtest/vm_upgd_cb/faults/entry_one_phase_idempotent_v0.h>
#include <orbtest/vm_upgd_cb/faults/entry_one_phase_non_idempotent_v0.h>
#include <orbtest/vm_upgd_cb/faults/entry_two_phase_v0.h>
#include <orbtest/vm_upgd_cb/faults/entry_switchover_sec_pri_v0.h>

// List of test entry functions.
fi_entry_t fi_entry[] = {
	ENTRY(basic_1_a),
	ENTRY(one_phase_idempotent_1_a),
	ENTRY(one_phase_non_idempotent_1_a),
	ENTRY(two_phase_1_a),
	ENTRY(switchover_sec_pri_1_a),
	ENTRY(NULL)
};


#if ! defined(_KERNEL)
int
main(int argc, char **argv)
{
	int	rslt;

	// Initialize orb.
	if (ORB::initialize() != 0) {
		os::printf("Failed to initialize orb\n");
		return (1);
	}

	rslt = do_fi_test(argc, argv, fi_entry);
	return (rslt);
}

#else // ! _KERNEL

/*
extern "C" {
	void _cplpl_init(void);
	void _cplpl_fini(void);
}
*/

char _depends_on[] = "misc/cl_runtime misc/cl_load misc/cl_orb "
	"misc/cl_haci misc/cl_quorum misc/cl_comm "
	"misc/repl_sample_cb_common_v0 misc/cl_dcs misc/fi_driver";

// extern struct mod_ops mod_miscops;

struct modlmisc modlmisc = {
	&mod_miscops, "ha fault injection driver"
};

struct modlinkage modlinkage = {
//	MODREV_1, { (void *)&modlmisc, NULL }
	MODREV_1, { (void *)&modlmisc, NULL, NULL, NULL }
};

int
_init(void)
{
	int		error;
	struct modctl	*mp;
	int		argc, i;
	char		**argv;

	if ((error = mod_install(&modlinkage)) != 0) {
		return (error);
	}
	_cplpl_init();				// C++ initialization

	// Get module name so we know what test case to call
	if ((mp = mod_getctl(&modlinkage)) == NULL) {
		os::printf("Can't find modctl\n");
		error = mod_remove(&modlinkage);
		_cplpl_fini();
		return (EIO);
	}

	os::printf("Module driver_v0 loaded\n");

	// Parse module name to get arguments
	mod_name_to_args(mp->mod_modname, argc, argv);

	// Do the test.
	error = do_fi_test(argc, argv, fi_entry);
	os::printf("    Result From Module: %d\n", error);

	// Clean up argument vectors allocated by mod_name_to_args().
	for (i = 0; i < argc; ++i) {
		delete[] argv[i];
	}
	delete[] argv;
	return (error);
}

int
_fini(void)
{
	os::printf("Module driver_v0 unloaded\n");

	int r;
	r = mod_remove(&modlinkage);

	if (r == 0) {
		// Clean up.
		_cplpl_fini();
	}
	return (r);

/*
	// XXX Comment out. Should not wait for repl_sample_prov unref.
	// mod_fini_wait();
	_cplpl_fini();
	return (mod_remove(&modlinkage));
*/
}

int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}

#endif // ! _KERNEL
