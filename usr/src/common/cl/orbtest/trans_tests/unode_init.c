/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)unode_init.c	1.6	08/05/20 SMI"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/cladm.h>
#include <unode/unode_util.h>
#include <sys/clconf_int.h>
#include <scconf_private.h>

int
unode_init(void)
{
	printf("trans_tests loaded\n");
	return (0);
}

int
unode_cladm(int fac, int cmd, void *arg)
{
	int bflags = 0;
	switch (fac) {
		case CL_INITIALIZE:
			if (cmd == CL_GET_BOOTFLAG) {
				bflags = CLUSTER_CONFIGURED|CLUSTER_BOOTED;
				*(int *)arg = bflags;
				break;
			}
			printf("Error: invalid cmd to CL_CONFIG\n");
			return (1);
		case CL_CONFIG:
			if (cmd == CL_NODEID) {
			    *(nodeid_t *)arg = (nodeid_t)unode_node_number();
			    break;
			}
			printf("Error: invalid cmd to CL_CONFIG\n");
			return (1);
		default:
			printf("Error: invalid fac to unode_cladm\n");
			return (1);
	}
	return (0);
}


int
unode_make_door(clconf_node_t *cl_node, clconf_adapter_t *cl_adp)
{
	const char *adpname = clconf_obj_get_name((clconf_obj_t *)cl_adp);
	const char *ndname = clconf_obj_get_name((clconf_obj_t *)cl_node);
	int len = strlen(adpname) + 1;
	char *device_name = (char *)calloc(1, len);
	char *device_instance = (char *)calloc(1, len);
	const char *cname = unode_get_clustername();
	size_t size;
	char *drname = NULL;
	int ret = 0;

	if (device_name == NULL || device_instance == NULL) {
		ret = 1;
		goto cleanup;
	}
	if (conf_split_adaptername((char *)adpname, device_name,
	    device_instance)) {
		ret = 1;
		goto cleanup;
	}

	size = strlen(ndname)+strlen(device_instance)+
	    strlen("/tmp///transport.") + strlen(cname) + 1;
	drname = (char *)malloc(size);

	sprintf(drname, "/tmp/%s/%s/transport.%s", cname,
	    ndname, device_instance);
	if (clconf_obj_set_property((clconf_obj_t *)cl_adp, "door_name",
	    drname))
		ret = 1;
cleanup:
	if (device_name)
		free(device_name);
	if (device_instance)
		free(device_instance);
	if (drname)
		free(drname);
	return (ret);
}

int
unode_dummy()
{
	return (1);
}
