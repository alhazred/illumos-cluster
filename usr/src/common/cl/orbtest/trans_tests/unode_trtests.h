/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _UNODE_TRTESTS_H
#define	_UNODE_TRTESTS_H

#pragma ident	"@(#)unode_trtests.h	1.10	08/05/20 SMI"

#include <unode/unode_util.h>

int unode_make_door(clconf_node_t *, clconf_adapter_t *);
int unode_cladm(int, int, void *);
int unode_dummy();

#define	textdomain(x)			unode_dummy()
#define	bindtextdomain(a, b)		unode_dummy()
#define	setlocale(a, b)			unode_dummy()
#define	dgettext(a, b)			b
#undef SCCONF_CLPL_DIR
#define	SCCONF_CLPL_DIR			unode_get_clpldir()
#define	getuid()			0
#define	geteuid()			0
#define	setuid(x)			unode_dummy()
#define	_cladm(a, b, c)			unode_cladm(a, b, c)
#define	scstat_get_nodes(x) 		SCSTAT_ENOERR
#define	scstat_free_nodes(x)
#define	scstat_get_ds_status(a, b) 	SCSTAT_ENOERR
#define	scstat_free_ds_status(a)
#define	scconf_free_ds_config(a)
#define	scstat_get_quorum(a) 		SCSTAT_ENOERR
#define	scstat_free_quorum(a)
#define	scstat_get_rgs(a)		SCSTAT_ENOERR
#define	scstat_free_rgs(a)
#define	scstat_get_quorum_with_cached_config(a, b)	SCSTAT_ENOERR
#define	scstat_get_nodes_with_cached_config(a, b) 	SCSTAT_ENOERR
#define	scstat_get_ds_status_with_cached_config(a, b, c) SCSTAT_ENOERR

#define	scconf_add_quorum_device(a, b)		SCCONF_EUNEXPECTED
#define	scconf_add_quorum_device(a, b) 		SCCONF_EUNEXPECTED
#define	scconf_add_ds(a, b, c, d, e, f, g, h)	SCCONF_EUNEXPECTED
#define	scconf_set_secure_authtype(a) 		SCCONF_EUNEXPECTED
#define	scconf_maintstate_quorum_device(a) 	SCCONF_EUNEXPECTED
#define	scconf_reset_quorum_device(a) 		SCCONF_EUNEXPECTED
#define	scconf_maintstate_quorum_node(a) 	SCCONF_EUNEXPECTED
#define	scconf_get_quorum_installflag(a) 	SCCONF_EUNEXPECTED
#define	scconf_reset_quorum_node(a) 		SCCONF_EUNEXPECTED
#define	scconf_set_node_defaultvote(a, b)	SCCONF_EUNEXPECTED
#define	scconf_set_quorum_installflag() 	SCCONF_EUNEXPECTED
#define	scconf_reset_quorum() 			SCCONF_EUNEXPECTED
#define	scconf_change_ds(a, b, c, d, e, f, g)	SCCONF_EUNEXPECTED
#define	scconf_addto_secure_joinlist(a) 	SCCONF_EUNEXPECTED
#define	scconf_rm_quorum_device(a) 		SCCONF_EUNEXPECTED
#define	scconf_rm_ds(a, b, c, d, e) 		SCCONF_EUNEXPECTED
#define	scconf_get_ds_config(a, b, c)		SCCONF_EUNEXPECTED
#define	scconf_clear_secure_joinlist() 		SCCONF_EUNEXPECTED
#define	scconf_rmfrom_secure_joinlist(a) 	SCCONF_EUNEXPECTED
#define	rgm_free_rg(a)
#define	conf_get_secure_authtype(a, b) 		SCCONF_NOERR
#define	conf_get_secure_joinlist(a, b) 		SCCONF_NOERR
#define	conf_get_qdevs(a, b) 			SCCONF_NOERR
#define	conf_free_qdevs(a)
#define	did_delete_devices_by_node(a) 		0
#define	rgm_scrgadm_getrglist(a, b) 		\
	(*((scha_errmsg_t *)calloc(1, sizeof (scha_errmsg_t))))
#define	rgm_scrgadm_getrgconf(a, b) 		rgm_scrgadm_getrglist(a, b)

/*
 * This is a major hack. Whenever an adapter is added by scconf, it should
 * create a door for that adapter.
 */
#define	clconf_node_add_adapter(a, b) 	\
	clconf_node_add_adapter(a, b); 	\
	if (unode_make_door(a, b)) {	\
		rstatus = SCCONF_EUNEXPECTED; \
		goto cleanup;			\
	}

#endif	/* _UNODE_TRTESTS_H */
