/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ORBTEST_ORPHAN_FI_SUPPORT_H
#define	_ORBTEST_ORPHAN_FI_SUPPORT_H

#pragma ident	"@(#)support.h	1.10	08/05/20 SMI"

#if defined(_KERNEL)
#include <sys/modctl.h>
#endif

#include <sys/os.h>
#include <orb/invo/common.h>
#include <orb/object/adapter.h>
#include <nslib/ns.h>
#include <h/orphan_fi.h>

#if !defined(_KERNEL)
#if defined(_KERNEL_ORB)
#define	_UNODE
#else
#define	_USER
#endif	// _KERNEL_ORB
#endif	// !_KERNEL

// Names registered with the name server.
#define	SERVER	"orphan_fi server"
#define	TESTOBJ	"orphan_fi testobj"

#if !defined(_FAULT_INJECTION)
//
// Print a failure message when Fault Injection is not compiled in.
// Returns:
//	A non-zero value.
//
int	no_fault_injection();
#endif

//
// Get an object reference with the given name from the name server.
// Returns:
//	The object reference if successful, _nil() otherwise.
//
CORBA::Object_ptr get_obj(char *strname);

//
// Common (non-exported) methods for implementation classes.
//
class impl_common {
public:
	// Register object with the name server with the given name.
	// Returns:
	//	True if successful, false otherwise.
	bool	register_obj(CORBA::Object_ptr objref, char *strname);

	// Unregister the object from the name server.
	// Returns:
	//	True if successful, false otherwise.
	bool	unregister_obj();

	// Suspend calling thread until _unreferenced() is called.
	// Returns: true if _unreferenced() has been called.
	//	    false if timeout arg (in seconds) is nonzero and it
	//	    expires before _unreferenced() is called.
	bool	wait_until_unreferenced(uint32_t timeout = 0);

	// Has _unreferenced() been called?
	bool	is_unreferenced();

protected:
	// Protected so this class can be used only through derivation.
	impl_common();
	~impl_common();

	// Mark that _unreferenced() has been called.
	void	unref_called();

private:
	char		*_name;		// name registered with the name server
	bool		_unref_called;
	os::mutex_t	_mutex;
	os::condvar_t	_cv;
};

#endif	/* _ORBTEST_ORPHAN_FI_SUPPORT_H */
