/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ORBTEST_ORPHAN_FI_SERVER_IMPL_H
#define	_ORBTEST_ORPHAN_FI_SERVER_IMPL_H

#pragma ident	"@(#)server_impl.h	1.14	08/05/20 SMI"

#include <orbtest/orphan_fi/support.h>
#include <orbtest/orphan_fi/testobj_impl.h>

//
// Test server implementation.
//
/* CSTYLED */
class server_impl : public McServerof<orphan_fi::server>, public impl_common {
public:
	server_impl();
	~server_impl();
	void	_unreferenced(unref_t);

	// Initialize implementation object with the given name.
	// Returns: true if successful, false otherwise.
	bool	init(char *strname);

	//
	// IDL Interfaces.
	//

	// For clients to test simple two-way calls.  It adds l1 and
	// l2, and returns the total.  It also sets an internal flag
	// to indicate that the call has reached this server.
	int32_t	twoway_simple(int32_t l1, int32_t l2, Environment &);

	// For clients to test simple one-way calls.  It sets an
	// internal flag to indicate that the call has been made.
	void	oneway_simple(int32_t l1, int32_t l2, Environment &);

	// For clients to test two-way calls with one testobj passed
	// in.  It stores one duplicate and returns returns another
	// duplicate of the passed testobj.  It also sets an internal
	// flag to indicate that the call has been made.
	orphan_fi::testobj_ptr	twoway_one_obj(
					orphan_fi::testobj_ptr,
					Environment &);

	// For clients to test one-way calls with one testobj passed
	// in.  It sets an internal flag to indicate that the call
	// has been made.
	void	oneway_one_obj(orphan_fi::testobj_ptr, Environment &);

	// For clients to verify whether a test call has reached this server.
	void	reset_call_flag(Environment &);
	bool	was_called(Environment &);

	// For verifying whether the testobj passed to and saved
	// by *_one_obj() reached the server.
	void	clear_testobj(Environment &);
	void	test_testobj(Environment &);

	// Create a new server and return one reference to it.
	// The new server will not be registered with the name server.
	orphan_fi::server_ptr	create_server2(Environment &);

	// Wait until the server created by create_server2()
	// above gets unreferenced.  The timeout argument specifies
	// how long (in seconds) to wait for the unreferenced
	// (specify 0 for no time out).  Returns true if the testobj
	// gets unreferenced, false if the timeout expires.
	bool	wait_server2_unref(uint32_t timeout, Environment &);

	// Return whether the server created by create_server2()
	// above has been called or not.
	bool	was_server2_called(Environment &);

	// Returns the node ID where this object impl lives.
	sol::nodeid_t	get_nodeid(Environment &);

	// For clients to tell this server that testing is done.
	void	done(Environment &);

private:
	// For storing testobj references.
	orphan_fi::testobj_var	_testobjp;

	// For storing server impl created by create_server2_impl() above.
	server_impl	*_server2_implp;

	bool	_call_flag;
};

#endif	/* _ORBTEST_ORPHAN_FI_SERVER_IMPL_H */
