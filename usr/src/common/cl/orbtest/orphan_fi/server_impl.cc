/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)server_impl.cc	1.14	08/05/20 SMI"


#include <orb/infrastructure/orb_conf.h>
#include <orbtest/orphan_fi/server_impl.h>


server_impl::server_impl()
{
	_server2_implp = NULL;
	_call_flag = false;
}

server_impl::~server_impl()
{
	// Make sure _unreferenced() has been called.
	if (! is_unreferenced()) {
		os::printf("ERROR: server_impl: destructor called before "
			"_unreferenced()\n");
	}

	delete _server2_implp;
}

void
#ifdef DEBUG
server_impl::_unreferenced(unref_t cookie)
#else
server_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	unref_called();
}


//
// Initialize implementation object with the given name.
//
bool
server_impl::init(char *strname)
{
	orphan_fi::server_ptr	tmpref;
	bool			retval;

	// Register with the name server.
	tmpref = get_objref();			// get temporary reference
	retval = register_obj(tmpref, strname);
	CORBA::release(tmpref);

	return (retval);
}


//
// IDL interfaces.
//

//
// For clients to test simple two-way calls.  It adds l1 and
// l2, and returns the total.  It also sets an internal flag
// to indicate that the call has reached this server.
//
int32_t
server_impl::twoway_simple(int32_t l1, int32_t l2, Environment &)
{
	// Set flag to indicate that this call reached this server.
	_call_flag = true;

	// Add the two arguments and return result.
	return (l1 + l2);
}


//
// For clients to test simple one-way calls.  It sets an
// internal flag to indicate that the call has been made.
//
void
server_impl::oneway_simple(int32_t, int32_t, Environment &)
{
	// Set flag to indicate that this call reached this server.
	_call_flag = true;
}


//
// For clients to test two-way calls with one testobj passed
// in.  It stores one duplicate and returns returns another
// duplicate of the passed testobj.  It also sets an internal
// flag to indicate that the call has been made.
//
orphan_fi::testobj_ptr
server_impl::twoway_one_obj(orphan_fi::testobj_ptr obj, Environment &)
{
	// Store duplicate of obj.
	_testobjp = orphan_fi::testobj::_duplicate(obj);

	// Set flag to indicate that this call reached this server.
	_call_flag = true;

	// Return another duplicate of obj1.
	return (orphan_fi::testobj::_duplicate(obj));
}


//
// For clients to test one-way calls with one testobj passed
// in.  It sets an internal flag to indicate that the call
// has been made.
//
void
server_impl::oneway_one_obj(orphan_fi::testobj_ptr obj, Environment &)
{
	// Store duplicate of obj.
	_testobjp = orphan_fi::testobj::_duplicate(obj);

	// Set flag to indicate that this call reached this server.
	_call_flag = true;
}


//
// For verifying whether the testobj passed to and saved
// by *_one_obj() reached the server.
//
void
server_impl::clear_testobj(Environment &)
{
	_testobjp = orphan_fi::testobj::_nil();
}

void
server_impl::test_testobj(Environment &e)
{
	const int32_t		op1 = 3, op2 = 5;
	const int32_t		expected = op1 + op2;
	int32_t			result;
	char			msg[100];
	Environment		e2;
	CORBA::Exception	*ex;

	// Invoke the stored testobj.

	if (CORBA::is_nil(_testobjp)) {
		os::sprintf(msg, "FAIL: test object wasn't passed to" \
			"server\n");
		e.exception(new TestException(msg));
		return;
	}

	result = _testobjp->add(op1, op2, e2);
	if (ex = e2.exception()) {
		os::sprintf(msg, "FAIL: %s raised while invoking "
			"test object\n", ex->_name());
		e.exception(new TestException(msg));
		return;
	}

	if (result != expected) {
		os::sprintf(msg, "FAIL: test object returned %d. "
			"Expected %d\n", result, expected);
		e.exception(new TestException(msg));
		return;
	}
}


//
// Create a new server and return one reference to it.
// The new server will not be registered with the name server.
//
orphan_fi::server_ptr
server_impl::create_server2(Environment &)
{
	delete _server2_implp;

	if (_server2_implp = new server_impl) {
		return (_server2_implp->get_objref());
	} else {
		return (orphan_fi::server::_nil());
	}
}


//
// Wait until the server created by create_server2()
// above gets unreferenced.  The timeout argument specifies
// how long (in seconds) to wait for the unreferenced
// (specify 0 for no time out).  Returns true if the testobj
// gets unreferenced, false if the timeout expires.
//
bool
server_impl::wait_server2_unref(uint32_t timeout, Environment &)
{
	return (_server2_implp->wait_until_unreferenced(timeout));
}


//
// Return whether the server created by create_server2()
// above has been called or not.
//
bool
server_impl::was_server2_called(Environment &e)
{
	return (_server2_implp->was_called(e));
}


//
// Returns the node ID where this object impl lives.
//
sol::nodeid_t
server_impl::get_nodeid(Environment &)
{
	return (orb_conf::node_number());
}


//
// For clients to verify whether a test call has reached this server.
//
void
server_impl::reset_call_flag(Environment &)
{
	_call_flag = false;
}

bool
server_impl::was_called(Environment &)
{
	return (_call_flag);
}


//
// For clients to tell that testing is done.
//
void
server_impl::done(Environment &e)
{
	clear_testobj(e);		// release any stored testobj's
	unregister_obj();
}
