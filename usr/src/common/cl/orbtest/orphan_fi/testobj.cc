/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)testobj.cc	1.13	08/05/20 SMI"

#include <orbtest/orphan_fi/testobj_impl.h>


// ---------------------------------------------------------------------------
// KERNEL testobj
// ---------------------------------------------------------------------------
#if defined(_KERNEL)

extern "C" {
	void _cplpl_init(void);
	void _cplpl_fini(void);
}
char _depends_on[] = "misc/cl_runtime misc/cl_load misc/cl_orb "
	"misc/cl_haci misc/cl_quorum misc/cl_comm";

extern struct mod_ops mod_miscops;

struct modlmisc modlmisc = {
	&mod_miscops, "orphan FI testobj"
};

struct modlinkage modlinkage = {
	MODREV_1, { (void *)&modlmisc, NULL }
};

static testobj_impl *testobjp = NULL;

int
_init(void)
{
	int	error = 0;

	if (error = mod_install(&modlinkage)) {
		return (error);
	}
	_cplpl_init();				// C++ initialization

#if defined(_FAULT_INJECTION)
	testobjp = new testobj_impl;
	if (! testobjp->init(TESTOBJ)) {
		delete testobjp;
		error = ECANCELED;
	}
#else
	no_fault_injection();
	error = ENOTSUP;
#endif

	if (error) {
		_cplpl_fini();
		mod_remove(&modlinkage);
	}

	return (error);
}

int
_fini(void)
{
#if defined(_FAULT_INJECTION)
	const uint32_t	timeout = 180;		// wait timeout (in seconds)

	if (testobjp != NULL) {
		if (!testobjp->wait_until_unreferenced(timeout)) {
			os::printf("FAIL: testobj didn't get unreferenced "
				"after %d seconds\n", timeout);
			return (EBUSY);
		} else {
			delete testobjp;
		}
	}
	_cplpl_fini();
#endif
	return (mod_remove(&modlinkage));
}

int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}

#endif // _KERNEL


// ---------------------------------------------------------------------------
// USER testobj
// ---------------------------------------------------------------------------
#if defined(_USER)

#include <signal.h>
#include <errno.h>

int
main()
{
#if defined(_FAULT_INJECTION)
	const uint32_t	timeout = 180;		// wait timeout (in seconds)
	testobj_impl	*testobjp;
	sigset_t	sigset;
	int		ret;

	// The fault injection driver will send a SIGTERM when trying to
	// unload this server.  This thread will wait until SIGTERM is sent
	// and then wait until the testobj has been unreferenced before
	// exiting.  We'll have all threads block SIGTERM to ensure that
	// only this thread receives SIGTERM.
	sigemptyset(&sigset);
	if (sigaddset(&sigset, SIGTERM) < 0) {
		os::printf("ERROR: Can't add SIGTERM to signal set (%s)\n",
			strerror(errno));
		return (1);
	}
	if (ret = thr_sigsetmask(SIG_BLOCK, &sigset, NULL)) {
		os::printf("ERROR: Can't block SIGTERM (%s)\n",
			strerror(ret));
		return (1);
	}

	if (ORB::initialize() != 0) {
		os::printf("ERROR: Can't initialize ORB\n");
		return (1);
	}

	testobjp = new testobj_impl;
	if (! testobjp->init(TESTOBJ)) {
		delete testobjp;
		return (1);
	}

	// Wait until SIGTERM is sent.
	if ((ret = sigwait(&sigset)) < 0) {
		os::printf("ERROR: Can't sigwait for SIGTERM (%s)\n",
			strerror(errno));
		delete testobjp;
		return (1);
	}

	// Wait until testobj is unreferenced.
	if (!testobjp->wait_until_unreferenced(timeout)) {
		os::printf("FAIL: testobj didn't get unreferenced "
			"after %d seconds\n", timeout);
		delete testobjp;
		return (1);
	}

	delete testobjp;
	return (0);
#else
	return (no_fault_injection());
#endif
}

#endif // _USER


// ---------------------------------------------------------------------------
// UNODE testobj
// ---------------------------------------------------------------------------
#if defined(_UNODE)

int
unode_init()
{
#if defined(_FAULT_INJECTION)
	os::printf("orphan FI testobj loaded\n");
	return (0);
#else
	return (no_fault_injection());
#endif
}

int
orphan_fi_testobj(int, char *[])
{
#if defined(_FAULT_INJECTION)
	testobj_impl	*testobjp;

	testobjp = new testobj_impl;
	if (! testobjp->init(TESTOBJ)) {
		delete testobjp;
		return (1);
	}

	// testobjp->wait_until_unreferenced();
	// delete testobjp;

	return (0);
#else
	return (no_fault_injection());
#endif
}

#endif // _UNODE
