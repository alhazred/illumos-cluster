/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)reply_unmarshal_before_check.cc	1.12	08/05/20 SMI"

#if !defined(_KERNEL)
#include <unistd.h>
#endif

#include <orbtest/orphan_fi/faults/entries.h>
#include <orbtest/orphan_fi/faults/entry_support.h>

//
// Assertion:
//	When a reply message is orphaned BEFORE the orphaned message
//	check in rxdoor_kit::unmarshal(), then the client's invocation
//	shall return system exception COMM_FAILURE(0, COMPLETED_YES).
//
// Scenarios:
//	1. Server node is marked dead by CMM.
//
//	NOTE: we can't do a node-rejoin test for this because the fault point
//	is located in rxdoor_kit::unmarshal() after the
//	rxdoor_manager::the().lookup_rxdoor() call which locks the xdoor
//	bucket.  When the CMM is in its cluster reconfiguration mode, it will
//	also try to grab the lock, resulting in a CMM timeout and panic.
//
// Test Strategy:
//	1. Client sets an Invo trigger for fault number
//
//		FAULTNUM_ORPHAN_UNMARSHAL_BEFORE_CHECK
//
//	   which will reboot the server node.
//	2. Client makes a request to the server passing in an object reference
//	   whose implementation resides in a different node from the server.
//	   The server will return a duplicate of the passed reference.
//	3. When the reply message comes back and reaches
//	   rxdoor_kit::unmarshal(), the server node is rebooted and
//	   the thread is suspended until the CMM marks the node dead.
//	   The client's invocation should return exception
//
//		COMM_FAILURE(0, COMPLETED_YES)
//

//
// Server node dead.
//
int
reply_unmarshal_before_check_dead(int, char *[])
{
	os::printf("*** REPLY orphaned BEFORE unmarshal orphan check, "
		"server node DEAD\n");

#if defined(_FAULT_INJECTION)
	// Expected exception.
	CORBA::COMM_FAILURE		expected(0, CORBA::COMPLETED_YES);
	CORBA::COMM_FAILURE		*real_ex;

	orphan_fi::server_var		serverp;
	nodeid_t			server_nid;
	orphan_fi::testobj_var		testobjp;
	orphan_fi::testobj_var		ret_testobjp;
	FaultFunctions::wait_for_arg_t	reboot_arg;
	Environment			e;
	CORBA::Exception		*ex;
	int				rslt = 0;

	// Get a reference to the server.
	serverp = get_server();
	if (CORBA::is_nil(serverp)) {
		return (1);
	}

	// Get server's node ID.
	if (! get_nodeid(serverp, server_nid)) {
		return (1);
	}

	// Get a reference to the testobj.
	testobjp = get_testobj();
	if (CORBA::is_nil(testobjp)) {
		return (1);
	}

	// Arm Invo trigger to reboot server node during reply.
	reboot_arg.nodeid = server_nid;
	reboot_arg.op = FaultFunctions::WAIT_FOR_DEAD;

	FaultFunctions::invo_trigger_add(
		FaultFunctions::REBOOT_DIFFERENT,
		FAULTNUM_ORPHAN_UNMARSHAL_BEFORE_CHECK,
		&reboot_arg,
		(uint32_t)sizeof (reboot_arg));

	// Invoke the server.
	os::printf("    Making two-way invocation to server with "
		"one object ref...\n");
	ret_testobjp = serverp->twoway_one_obj(testobjp, e);

	// Verify we got an exception.
	ex = e.exception();
	if (ex == NULL) {
		os::printf("FAIL: no exception returned. "
			"Expected %s(%d, %d)\n",
			expected._name(), expected._minor(),
			expected.completed());
		rslt = 1;
		goto cleanup;
	}

	// Verify we got the right exception.
	if (! ex->type_match(&expected)) {
		os::printf("FAIL: got exception %s. Expected %s(%d, %d)\n",
			ex->_name(),
			expected._name(), expected._minor(),
			expected.completed());
		rslt = 1;
		goto cleanup;
	}

	// At this point we should get the right exception as indicated by
	// the type_match() test above.  We should be able to narrow "ex"
	// to the expected type, but we'll confirm it anyway, just in case.
	real_ex = expected._exnarrow(ex);
	if (real_ex == NULL) {
		os::printf("FAIL: got exception %s. Expected %s(%d, %d)\n",
			ex->_name(),
			expected._name(), expected._minor(),
			expected.completed());
		rslt = 1;
		goto cleanup;
	}

	// Verify minor number.
	if (real_ex->_minor() != expected._minor()) {
		os::printf("FAIL: got %s minor %d. Expected minor %d\n",
			expected._name(),
			real_ex->_minor(), expected._minor());
		rslt = 1;
		goto cleanup;
	}

	// Verify completion status.
	if (real_ex->completed() != expected.completed()) {
		os::printf("FAIL: got %s completion status %d. Expected "
			"status %d\n",
			expected._name(),
			real_ex->completed(), expected.completed());
		rslt = 1;
		goto cleanup;
	}

	// Else, we passed the test.  Yippee!
	os::printf("PASS: test completed\n");
	rslt = 0;

cleanup:
	// Clear all Invo triggers.
	InvoTriggers::clear_all();

	return (rslt);
#else
	return (no_fault_injection());
#endif
}
