/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)verify_request.cc	1.10	08/05/20 SMI"

#include <orbtest/orphan_fi/faults/entries.h>
#include <orbtest/orphan_fi/faults/entry_support.h>

//
// Entry function to verify that a previous request didn't reach the server.
//
int
verify_request_discarded(int, char *[])
{
#if defined(_FAULT_INJECTION)
	bool			server_called;
	orphan_fi::server_var	serverp;

	// Announce test.
	os::printf("    Verifying orphaned request was discarded\n");

	// Get a reference to the server.
	serverp = get_server();
	if (CORBA::is_nil(serverp)) {
		return (1);
	}

	if (! get_call_flag(serverp, server_called)) {
		return (1);
	}

	// Verify previous request didn't reach the server.
	if (server_called) {
		os::printf("FAIL: orphaned request reached server. "
			"Expected to be discarded.\n");
		return (1);
	}

	os::printf("PASS: test completed\n");
	return (0);
#else
	return (no_fault_injection());
#endif
}


//
// Entry function to verify that a previous request reached the server.
//
int
verify_request_succeeded(int, char *[])
{
#if defined(_FAULT_INJECTION)
	bool			server_called;
	orphan_fi::server_var	serverp;

	// Announce test.
	os::printf("    Verifying orphaned request reached server\n");

	// Get a reference to the server.
	serverp = get_server();
	if (CORBA::is_nil(serverp)) {
		return (1);
	}

	if (! get_call_flag(serverp, server_called)) {
		return (1);
	}

	// Verify previous request reached the server.
	if (! server_called) {
		os::printf("FAIL: orphaned request got discarded. "
			"Expected to reach the server.\n");
		return (1);
	}

	os::printf("PASS: test completed\n");
	return (0);
#else
	return (no_fault_injection());
#endif
}

//
// Entry function to verify first testobj reference passed to the server
// in a previous request.  This function invokes the server to, in turn,
// invoke the testobj.
//
//
int
verify_testobj(int, char *[])
{
#if defined(_FAULT_INJECTION)
	orphan_fi::server_var	serverp;
	Environment		e;

	// Announce test.
	os::printf("    Verifying server can invoke passed testobj\n");

	// Get a reference to the server.
	serverp = get_server();
	if (CORBA::is_nil(serverp)) {
		return (1);
	}

	// Ask the server to invoke the first stored testobj reference.
	serverp->test_testobj(e);
	if (check_exception(e.exception(), "ERROR:")) {
		return (1);
	}

	os::printf("PASS: test completed\n");
	return (0);
#else
	return (no_fault_injection());
#endif
}
