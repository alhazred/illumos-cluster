/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ORBTEST_ORPHAN_FI_FAULTS_ENTRY_SUPPORT_H
#define	_ORBTEST_ORPHAN_FI_FAULTS_ENTRY_SUPPORT_H

#pragma ident	"@(#)entry_support.h	1.13	08/05/20 SMI"

#include <sys/os.h>
#include <orb/invo/common.h>
#include <nslib/ns.h>
#include <h/orphan_fi.h>
#include <orbtest/orphan_fi/support.h>

//
// Check for exceptions.
// Returns :
//	True if there IS an exception, false otherwise.
//
bool	check_exception(CORBA::Exception *ex, char *prefix);

//
// Get a reference to orphan_fi::server from the name server.
// Returns:
//	Reference to the server if successful, _nil() otherwise.
//
orphan_fi::server_ptr get_server();

//
// Get a reference to orphan_fi::testobj from the name server.
// Returns:
//	Reference to the testobj if successful, _nil() otherwise.
//
orphan_fi::testobj_ptr get_testobj();

//
// Get the server's or a testobj's node ID.
// Returns:
//	The server's or testobj's node ID is returned in 'nid'.
//	Return value is true if successful, false otherwise.
//
bool	get_nodeid(orphan_fi::server_ptr, nodeid_t &nid);
bool	get_nodeid(orphan_fi::testobj_ptr, nodeid_t &nid);

//
// Reset server call flag.
// Returns:
//	True if successful, false otherwise.
//
bool	reset_call_flag(orphan_fi::server_ptr);

//
// Get value of server's call flag (whether it was called or not).
// Returns:
//	True/false returned in 'flag'.
//	Return value is true if successful, false otherwise.
//
bool	get_call_flag(orphan_fi::server_ptr, bool &flag);

//
// Tell server to clear its stored testobj references.
// Returns:
//	True if successful, false otherwise.
//
bool	clear_stored_testobjs(orphan_fi::server_ptr);

//
// Tell server/testobj that test is done.
// Returns:
//	True if successful, false otherwise.
//
bool	done(orphan_fi::server_ptr);
bool	done(orphan_fi::testobj_ptr);

#endif	/* _ORBTEST_ORPHAN_FI_FAULTS_ENTRY_SUPPORT_H */
