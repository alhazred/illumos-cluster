/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)reply_after_check.cc	1.12	08/05/20 SMI"

#include <orbtest/orphan_fi/faults/entries.h>
#include <orbtest/orphan_fi/faults/entry_support.h>

//
// Assertion:
//	When a reply message is orphaned AFTER the orphan message check in
//	(client) outbound_invo_table::wakeup(), then the reply shall reach
//	the client.
//
// Scenarios:
//	1. Server node is marked dead by CMM.
//
//	Note:	we can't do a node rejoin test for this assertion since
//		the fault point FAULTNUM_ORPHAN_REPLY_AFTER_CHECK in
//		outbound_invo_table::wakeup() occurs while the lock to
//		the table slot is still held.  The CMM reconfiguration
//		steps will try to grab this lock and a time-out will occur
//		since that lock is still being held by wakeup.
//
// Test Strategy:
//	1. Client sets a Node trigger for fault number
//
//		FAULTNUM_ORPHAN_DEFER_REPLIES
//
//	   on its node to temporarily disable use of interrupt threads for
//	   processing reply messages.  This will allow the thread to suspend
//	   itself waiting for the CMM to reach a certain state.
//	2. Client sets an Invo trigger for fault number
//
//		FAULTNUM_ORPHAN_REPLY_AFTER_CHECK
//
//	3. Client sets an Invo trigger for fault number
//
//		FAULTNUM_ORPHAN_DEFER_REPLIES_OFF
//
//	   This turns off the FAULTNUM_ORPHAN_DEFER_REPLIES NodeTriggers
//	   in orb_msg::deliver_message().  Without this, the CMM might time out
//	   in step 2 transition when the reply thread is suspended waiting
//	   for a node rejoin, because another reply message that is part of
//	   the step 2 transition gets deferred (REPLY_MSG has only one thread
//	   in its thread pool).
//	4. Client makes a two-way request to the server.
//	5. When the reply message comes back and reaches
//	   outbound_invo_table::wakeup(), the server node is rebooted and
//	   the thread is suspended until the CMM marks the node dead.
//	   The reply should continue to reach the client.
//	6. Client turns off the FAULTNUM_ORPHAN_DEFER_REPLIES Node trigger.
//

//
// Server node dead.
//
int
reply_after_check_dead(int, char *[])
{
	os::printf("*** REPLY orphaned AFTER orphan check, "
		"server node DEAD\n");

#if defined(_FAULT_INJECTION)
	const int32_t			arg1 = 1;
	const int32_t			arg2 = 2;
	int32_t				ret;
	const int32_t			expected = arg1 + arg2;

	orphan_fi::server_var		serverp;
	nodeid_t			server_nid;
	FaultFunctions::wait_for_arg_t	reboot_arg;
	Environment			e;
	int				rslt;

	// Get a reference to the server.
	serverp = get_server();
	if (CORBA::is_nil(serverp)) {
		return (1);
	}

	// Get server's node ID.
	if (! get_nodeid(serverp, server_nid)) {
		return (1);
	}

	// Arm Invo trigger to reboot server node during reply AFTER the
	// orphan message check.
	reboot_arg.nodeid = server_nid;
	reboot_arg.op = FaultFunctions::WAIT_FOR_DEAD;

	FaultFunctions::invo_trigger_add(
		FaultFunctions::REBOOT,
		FAULTNUM_ORPHAN_REPLY_AFTER_CHECK,
		&reboot_arg,
		(uint32_t)sizeof (reboot_arg));

	// Arm Invo trigger to turn off FAULTNUM_ORPHAN_DEFER_REPLIES when
	// the reply message reaches rxdoor::handle_reply().
	InvoTriggers::add(FAULTNUM_ORPHAN_DEFER_REPLIES_OFF, NULL, 0);

	// Arm Node trigger on current node to temporarily defer
	// reply message processing.
	NodeTriggers::add(FAULTNUM_ORPHAN_DEFER_REPLIES, NULL, 0);

	// Invoke the server.
	os::printf("    Making two-way invocation to server...\n");
	ret = serverp->twoway_simple(arg1, arg2, e);

	// Verify the call succeeded.
	if (check_exception(e.exception(), "FAIL:")) {
		os::printf("FAIL: invocation returned exception. "
			"Expected to succeed\n");
		rslt = 1;
	} else if (ret != expected) {
		os::printf("FAIL: invocation succeeded by returned wrong "
			"value. Got %d, expected %d\n", ret, expected);
		rslt = 1;
	} else {
		os::printf("PASS: test completed\n");
		rslt = 0;
	}

	// Clear the Node trigger on current node that temporarily defers
	// reply message processing.
	NodeTriggers::clear(FAULTNUM_ORPHAN_DEFER_REPLIES);

	// Clear all Invo triggers.
	InvoTriggers::clear_all();

	return (rslt);
#else
	return (no_fault_injection());
#endif
}
