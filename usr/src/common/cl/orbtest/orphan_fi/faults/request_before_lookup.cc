/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)request_before_lookup.cc	1.15	08/05/20 SMI"

#if !defined(_KERNEL)
#include <unistd.h>
#endif

#include <orbtest/orphan_fi/faults/entries.h>
#include <orbtest/orphan_fi/faults/entry_support.h>

//
// Assertion:
//	When the client has the only reference in the cluster to the server,
//	and the request message is orphaned and the server gets unreferenced
//	before the local rxdoor lookup in rxdoor::handle_request_common(),
//	then the request shall be rejected and not reach the server.
//
// Scenarios:
//	1. Two-way request
//	2. One-way request
//
// Test Strategy:
//	1. First client makes a request to first test server to create
//	   a new instance of a test server implementation and return to
//	   a reference to it.  Make sure that the client has the only
//	   reference to the second test server (e.g. don't register it
//	   with the name server).
//	2. First client sets an Invo trigger for fault number
//
//		FAULTNUM_ORPHAN_REQUEST_BEFORE_RXDOOR_LOOKUP
//
//	   which will reboot the current node.
//	3. First client invokes the 2nd test server.  When the request reaches
//	   rxdoor::handle_request_common() on the server side, the client's
//	   node is rebooted and the invocation thread on the server side
//	   is suspended until a signal (see FaultFunctions::signal()) is set.
//	4. Meanwhile, a second client does the following:
//
//		a. Invoke the 1st test server to wait until the 2nd server
//		   gets unreferenced.
//		b. Set an Invo trigger which will cause a fault point to
//		   call FaultFunctions::signal() to unblock the suspended
//		   thread which serviced the 1st client's invocation.
//		   Note, the same fault point as above can be used if
//		   it calls FaultFunctions::generic().
//		c. Make an invocation to the 1st test server to trigger
//		   the signal.
//		d. Sleep for awhile to let the server thread to unblock and
//		   continue processing.
//		e. Verify that the 2nd server did NOT receive the first
//		   client's invocation.
//


//
// Twoway request.
//
int
request_before_lookup_2way(int, char *[])
{
	os::printf("*** Two-way REQUEST orphaned before local rxdoor lookup, "
		"client has only server reference\n");

#if defined(_FAULT_INJECTION)
	orphan_fi::server_var		server1p, server2p;
	FaultFunctions::wait_for_arg_t	reboot_arg;
	Environment			e;

	// Get a reference to 1st server from the name server.
	server1p = get_server();
	if (CORBA::is_nil(server1p)) {
		return (1);
	}

	// Ask 1st server to create 2nd server (won't be registered with
	// the name server).
	server2p = server1p->create_server2(e);
	if (check_exception(e.exception(), "ERROR:")) {
		os::printf("ERROR: 1st server couldn't create 2nd server\n");
		return (1);
	}

	if (CORBA::is_nil(server2p)) {
		os::printf("ERROR: 1st server returned nil server reference\n");
		return (1);
	}

	// Tell 2nd server to reset the call flag.
	if (! reset_call_flag(server2p)) {
		return (1);
	}

	// Set up fault argument (reboot this node and then wait for signal
	// from FaultFunctions::signal()).
	reboot_arg.nodeid = orb_conf::node_number();
	reboot_arg.op = FaultFunctions::WAIT_FOR_LOCAL_SIGNAL;

	// Arm the trigger.
	FaultFunctions::invo_trigger_add(
		FaultFunctions::REBOOT,
		FAULTNUM_ORPHAN_REQUEST_BEFORE_RXDOOR_LOOKUP,
		&reboot_arg,
		(uint32_t)sizeof (reboot_arg));

	// Invoke the 2nd server.
	os::printf("    Making two-way invocation to 2nd server...\n");
	(void) server2p->twoway_simple(1, 2, e);
	InvoTriggers::clear_all();
	if (check_exception(e.exception(), "ERROR:")) {
		os::printf("ERROR: can't make two-way invocation to "
			"2nd server\n");
		return (1);
	}

	// We shouldn't get here since the server will reboot this node
	// in the middle of the invocation.
	os::printf("ERROR: 2nd server didn't reboot client node\n");
	return (1);
#else
	return (no_fault_injection());
#endif
}

//
// Oneway request.
//
int
request_before_lookup_1way(int, char *[])
{
	os::printf("*** One-way REQUEST orphaned before local rxdoor lookup, "
		"client has only server reference\n");

#if defined(_FAULT_INJECTION)
	orphan_fi::server_var		server1p, server2p;
	FaultFunctions::wait_for_arg_t	reboot_arg;
	Environment			e;

	// Get a reference to 1st server from the name server.
	server1p = get_server();
	if (CORBA::is_nil(server1p)) {
		return (1);
	}

	// Ask 1st server to create 2nd server (won't be registered with
	// the name server).
	server2p = server1p->create_server2(e);
	if (check_exception(e.exception(), "ERROR:")) {
		os::printf("ERROR: 1st server couldn't create 2nd server\n");
		return (1);
	}

	if (CORBA::is_nil(server2p)) {
		os::printf("ERROR: 1st server returned nil server reference\n");
		return (1);
	}

	// Tell 2nd server to reset the call flag.
	if (! reset_call_flag(server2p)) {
		return (1);
	}

	// Set up fault argument (reboot this node and then wait for signal
	// from FaultFunctions::signal()).
	reboot_arg.nodeid = orb_conf::node_number();
	reboot_arg.op = FaultFunctions::WAIT_FOR_LOCAL_SIGNAL;

	// Arm the trigger.
	FaultFunctions::invo_trigger_add(
		FaultFunctions::REBOOT,
		FAULTNUM_ORPHAN_REQUEST_BEFORE_RXDOOR_LOOKUP,
		&reboot_arg,
		(uint32_t)sizeof (reboot_arg));

	// Invoke the 2nd server.
	os::printf("    Making one-way invocation to 2nd server...\n");
	server2p->oneway_simple(1, 2, e);
	InvoTriggers::clear_all();
	if (check_exception(e.exception(), "ERROR:")) {
		os::printf("ERROR: can't make one-way invocation to "
			"2nd server\n");
		return (1);
	}

	// Since the above is a oneway call, it would have returned right
	// away.  To prevent the fault injection test driver from thinking
	// that this node didn't die, we'll sleep for a while.  If at the
	// end we're still alive, it's a test error.
#if defined(_KERNEL)
	os::usecsleep(180 * 1000000);		// sleep for 3 minutes
#else
	sleep(180);				// sleep for 3 minutes
#endif

	os::printf("ERROR: 2nd server didn't reboot client node\n");
	return (1);
#else
	return (no_fault_injection());
#endif
}

//
// Verify the invocation made by either entry function above didn't reach
// the 2nd server.
//
// Note: this entry function must be executed on the same node as the
//	 servers above.
//
int
verify_server2_request_discarded(int, char *[])
{
#if defined(_FAULT_INJECTION)
	const uint32_t		timeout = 180;		// 3-minute timeout
	const uint32_t		sleep_amt = 10;		// 10-second sleep

	orphan_fi::server_var	server1p;
	bool			server2_unrefed, server2_called;
	Environment		e;

	// Announce test.
	os::printf("    Verifying orphaned request to 2nd server was "
		"discarded\n");

	// Get a reference to the 1st server.
	server1p = get_server();
	if (CORBA::is_nil(server1p)) {
		return (1);
	}

	// Ask 1st server to wait until 2nd server is unreferenced.
	server2_unrefed = server1p->wait_server2_unref(timeout, e);
	if (check_exception(e.exception(), "ERROR:")) {
		os::printf("ERROR: can't invoke "
			"server->wait_server2_unref()\n");
		goto error;
	}

	// Verify 2nd server got unreferenced.
	if (! server2_unrefed) {
		os::printf("FAIL: 2nd server didn't get unreferenced after "
			"%d seconds", timeout);
		goto error;
	}

	// Arm an Invo trigger which will unblock the thread which serviced
	// the 1st client's invocation.
	// Note, we can use the same fault number
	// FAULTNUM_ORPHAN_REQUEST_BEFORE_RXDOOR_LOOKUP since the fault point
	// calls Faultfunctions::generic().
	FaultFunctions::invo_trigger_add(
		FaultFunctions::SIGNAL,
		FAULTNUM_ORPHAN_REQUEST_BEFORE_RXDOOR_LOOKUP,
		NULL, 0);

	// Invoke something on the 1st server to deliver the trigger.
	(void) server1p->get_nodeid(e);
	InvoTriggers::clear_all();
	if (check_exception(e.exception(), "ERROR:")) {
		os::printf("ERROR: can't invoke server to deliver signal\n");
		goto error;
	}

	// Give server thread time to unblock and continue processing.
	os::usecsleep(sleep_amt * 1000000);

	// Verify (via the 1st server) that the 2nd server didn't receive
	// the invocation.
	server2_called = server1p->was_server2_called(e);
	if (check_exception(e.exception(), "ERROR:")) {
		os::printf("ERROR: can't invoke "
			"server->was_server2_called()\n");
		return (1);
	}

	if (server2_called) {
		os::printf("FAIL: orphaned request reached 2nd server. "
			"Expected to be discarded.\n");
		return (1);
	}

	os::printf("PASS: test completed\n");
	return (0);

error:
	e.clear();			// clear previous exception

	// An error/failure has occurred.  Try to invoke something on the
	// 1st server to unblock the suspended thread so it won't hang
	// around forever.
	FaultFunctions::invo_trigger_add(
		FaultFunctions::SIGNAL,
		FAULTNUM_ORPHAN_REQUEST_BEFORE_RXDOOR_LOOKUP,
		NULL, 0);
	(void) server1p->get_nodeid(e);
	e.clear();			// don't care since only for clean up
	InvoTriggers::clear_all();
	return (1);
#else
	return (no_fault_injection());
#endif
}
