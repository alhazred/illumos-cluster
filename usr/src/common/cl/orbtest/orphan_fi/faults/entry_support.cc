/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)entry_support.cc	1.11	08/05/20 SMI"

//
// Entry function support routines.
//

#include <sys/os.h>
#include <nslib/ns.h>
#include <orbtest/orphan_fi/faults/entry_support.h>

//
// Check for exceptions.
// Returns :
//	True if there IS an exception, false otherwise.
//
bool
check_exception(CORBA::Exception *ex, char *prefix)
{
	orphan_fi::server::TestException	*testex;

	if (ex == NULL) {
		return (false);			// no exception
	}

	if (testex = orphan_fi::server::TestException::_exnarrow(ex)) {
		// Print the message shipped with the exception.
		os::printf(testex->msg);
	} else {
		ex->print_exception(prefix);
	}
	return (true);
}

//
// Get a reference to orphan_fi::server from the name server.
// Returns:
//	Reference to the server if successful, _nil() otherwise.
//
orphan_fi::server_ptr
get_server()
{
	CORBA::Object_var	objp;
	orphan_fi::server_ptr	serverp;

	objp = get_obj(SERVER);
	if (CORBA::is_nil(objp)) {
		return (orphan_fi::server::_nil());
	}

	serverp = orphan_fi::server::_narrow(objp);
	if (CORBA::is_nil(serverp)) {
		os::printf("ERROR: Can't narrow to orphan_fi::server\n");
		return (orphan_fi::server::_nil());
	}

	return (serverp);
}

//
// Get a reference to orphan_fi::testobj from the name server.
// Returns:
//	Reference to the testobj if successful, _nil() otherwise.
//
orphan_fi::testobj_ptr
get_testobj()
{
	CORBA::Object_var	objp;
	orphan_fi::testobj_ptr	testobjp;

	objp = get_obj(TESTOBJ);
	if (CORBA::is_nil(objp)) {
		return (orphan_fi::testobj::_nil());
	}

	testobjp = orphan_fi::testobj::_narrow(objp);
	if (CORBA::is_nil(testobjp)) {
		os::printf("ERROR: Can't narrow to orphan_fi::testobj\n");
		return (orphan_fi::testobj::_nil());
	}

	return (testobjp);
}

//
// Get the server's or a testobj's node ID.
// Returns:
//	The server's or testobj's node ID is returned in 'nid'.
//	Return value is true if successful, false otherwise.
//
bool
get_nodeid(orphan_fi::server_ptr serverp, nodeid_t &nid)
{
	Environment	e;

	if (CORBA::is_nil(serverp)) {
		os::printf("ERROR: get_nodeid(): nil server\n");
		return (false);
	}

	// Query server.
	nid = (nodeid_t)(serverp->get_nodeid(e));
	if (check_exception(e.exception(), "ERROR:")) {
		os::printf("ERROR: can't get server's node ID\n");
		return (false);
	}

	return (true);
}

bool
get_nodeid(orphan_fi::testobj_ptr testobjp, nodeid_t &nid)
{
	Environment	e;

	if (CORBA::is_nil(testobjp)) {
		os::printf("ERROR: get_nodeid(): nil testobj\n");
		return (false);
	}

	// Query the testobj impl.
	nid = (nodeid_t)(testobjp->get_nodeid(e));
	if (check_exception(e.exception(), "ERROR:")) {
		os::printf("ERROR: can't get testobj's node ID\n");
		return (false);
	}

	return (true);
}


//
// Reset server call flag.
// Returns:
//	True if successful, false otherwise.
//
bool
reset_call_flag(orphan_fi::server_ptr serverp)
{
	Environment	e;

	if (CORBA::is_nil(serverp)) {
		os::printf("ERROR: reset_call_flag(): nil server\n");
		return (false);
	}

	// Tell server to reset the call flag.
	serverp->reset_call_flag(e);
	if (check_exception(e.exception(), "ERROR:")) {
		os::printf("ERROR: can't reset server call flag\n");
		return (false);
	}

	return (true);
}


//
// Get value of server's call flag (whether it was called or not).
// Returns:
//	True/false returned in 'flag'.
//	Return value is true if successful, false otherwise.
//
bool
get_call_flag(orphan_fi::server_ptr serverp, bool &flag)
{
	Environment	e;

	if (CORBA::is_nil(serverp)) {
		os::printf("ERROR: get_call_flag(): nil server\n");
		return (false);
	}

	// Query server.
	flag = serverp->was_called(e);
	if (check_exception(e.exception(), "ERROR:")) {
		os::printf("ERROR: can't query server call flag\n");
		return (false);
	}

	return (true);
}


//
// Tell server to clear its stored testobj references.
// Returns:
//	True if successful, false otherwise.
//
bool
clear_stored_testobjs(orphan_fi::server_ptr serverp)
{
	Environment	e;

	if (CORBA::is_nil(serverp)) {
		os::printf("ERROR: clear_stored_testobjs(): nil server\n");
		return (false);
	}

	// Tell server to clear its stored testobj.
	serverp->clear_testobj(e);
	if (check_exception(e.exception(), "ERROR:")) {
		os::printf("ERROR: can't clear server testobj\n");
		return (false);
	}

	return (true);
}


//
// Tell server/testobj that test is done.
// Returns:
//	True if successful, false otherwise.
//
bool
done(orphan_fi::server_ptr serverp)
{
	Environment	e;

	if (CORBA::is_nil(serverp)) {
		os::printf("ERROR: done(): nil server\n");
		return (false);
	}

	// Tell server test is done.
	serverp->done(e);
	if (check_exception(e.exception(), "ERROR:")) {
		os::printf("ERROR: can't invoke server->done()\n");
		return (false);
	}

	return (true);
}

bool
done(orphan_fi::testobj_ptr testobjp)
{
	Environment	e;

	if (CORBA::is_nil(testobjp)) {
		os::printf("ERROR: done(): nil testobj\n");
		return (false);
	}

	// Tell testobj impl test is done.
	testobjp->done(e);
	if (check_exception(e.exception(), "ERROR:")) {
		os::printf("ERROR: can't invoke testobj->done()\n");
		return (false);
	}

	return (true);
}
