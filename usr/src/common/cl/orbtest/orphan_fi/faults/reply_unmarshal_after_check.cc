/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)reply_unmarshal_after_check.cc	1.13	08/05/20 SMI"

#if !defined(_KERNEL)
#include <unistd.h>
#endif

#include <orbtest/orphan_fi/faults/entries.h>
#include <orbtest/orphan_fi/faults/entry_support.h>

//
// Assertion:
//	When a reply message is orphaned AFTER the orphaned message
//	check in rxdoor_kit::unmarshal(), then the reply shall succeed
//	and reach the client.
//
// Scenarios:
//	1. Server node is marked dead by CMM.
//
//	NOTE: we can't do a node-rejoin test for this because the fault point
//	is located in rxdoor_kit::unmarshal() after orphaned message check
//	but before the lock to the rxdoor bucket (locked by the
//	rxdoor_manager::the().lookup_rxdoor() call) is released.  When the
//	CMM is in its cluster reconfiguration mode, it will also try to grab
//	the lock, resulting in a CMM timeout and panic.
//
// Test Strategy:
//	1. Client sets an Invo trigger for fault number
//
//		FAULTNUM_ORPHAN_UNMARSHAL_AFTER_CHECK
//
//	   which will reboot the server node.
//	2. Client makes a request to the server passing in an object reference
//	   whose implementation resides in a different node from the server.
//	   The server will return a duplicate of the passed object.
//	3. When the reply message comes back and reaches
//	   rxdoor_kit::unmarshal(), the server node is rebooted and
//	   the thread is suspended until the CMM marks the node dead.
//	   The reply should continue to reach the client.
//

//
// Server node dead.
//
int
reply_unmarshal_after_check_dead(int, char *[])
{
	os::printf("*** REPLY orphaned AFTER unmarshal orphan check, "
		"server node DEAD\n");

#if defined(_FAULT_INJECTION)
	const int32_t			op1 = 1;
	const int32_t			op2 = 2;
	const int32_t			expected = op1 + op2;
	int32_t				ret;

	orphan_fi::server_var		serverp;
	nodeid_t			server_nid;
	orphan_fi::testobj_var		testobjp;
	orphan_fi::testobj_var		ret_testobjp;
	FaultFunctions::wait_for_arg_t	reboot_arg;
	Environment			e;
	int				rslt = 0;

	// Get a reference to the server.
	serverp = get_server();
	if (CORBA::is_nil(serverp)) {
		return (1);
	}

	// Get server's node ID.
	if (! get_nodeid(serverp, server_nid)) {
		return (1);
	}

	// Get a reference to the testobj.
	testobjp = get_testobj();
	if (CORBA::is_nil(testobjp)) {
		return (1);
	}

	// Arm Invo trigger to reboot server node during reply.
	reboot_arg.nodeid = server_nid;
	reboot_arg.op = FaultFunctions::WAIT_FOR_DEAD;

	FaultFunctions::invo_trigger_add(
		FaultFunctions::REBOOT_DIFFERENT,
		FAULTNUM_ORPHAN_UNMARSHAL_AFTER_CHECK,
		&reboot_arg,
		(uint32_t)sizeof (reboot_arg));

	// Invoke the server.
	os::printf("    Making two-way invocation to server with "
		"one object ref...\n");
	ret_testobjp = serverp->twoway_one_obj(testobjp, e);

	// Verify the call succeeded.
	if (check_exception(e.exception(), "FAIL:")) {
		os::printf("FAIL: invocation returned exception. "
			"Expected to succeed\n");
		rslt = 1;
		goto cleanup;
	}
	e.clear();					// clear for next use

	// Verify we can invoke the returned testobj.
	if (CORBA::is_nil(ret_testobjp)) {
		os::printf("FAIL: invocation succeeded but returned a nil "
			"object reference\n");
		rslt = 1;
		goto cleanup;
	}

	ret = ret_testobjp->add(op1, op2, e);
	if (check_exception(e.exception(), "FAIL:")) {
		os::printf("FAIL: invocation to returned object raised "
			"exception. Expected to succeed\n");
		rslt = 1;
		goto cleanup;
	}

	if (ret != expected) {
		os::printf("FAIL: invocation to returned object returned "
			"wrong value. Got %d, expected %d\n", ret, expected);
		rslt = 1;
		goto cleanup;
	}

	os::printf("PASS: test completed\n");
	rslt = 0;

cleanup:
	// Clear all Invo triggers.
	InvoTriggers::clear_all();

	return (rslt);
#else
	return (no_fault_injection());
#endif
}
