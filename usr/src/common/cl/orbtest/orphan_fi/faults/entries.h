/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ORBTEST_ORPHAN_FI_FAULTS_ENTRIES_H
#define	_ORBTEST_ORPHAN_FI_FAULTS_ENTRIES_H

#pragma ident	"@(#)entries.h	1.12	08/05/20 SMI"

#include <sys/os.h>
#include <orb/invo/common.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/fault/fault_injection.h>
#include <h/orphan_fi.h>
#include <orbtest/orphan_fi/support.h>

//
// Test entry functions.
//
int	reply_after_check_dead(int, char *[]);

int	reply_before_check_dead(int, char *[]);
int	reply_before_check_rejoin(int, char *[]);

int	reply_unmarshal_after_check_dead(int, char *[]);

int	reply_unmarshal_before_check_dead(int, char *[]);

int	reply_unmarshal_before_lookup_dead(int, char *[]);
int	reply_unmarshal_before_lookup_rejoin(int, char *[]);

int	request_after_check_2way_dead(int, char *[]);
int	request_after_check_2way_rejoin(int, char *[]);
int	request_after_check_1way_dead(int, char *[]);
int	request_after_check_1way_rejoin(int, char *[]);

int	request_before_check_2way_dead(int, char *[]);
int	request_before_check_1way_dead(int, char *[]);

int	request_before_lookup_2way(int, char *[]);
int	request_before_lookup_1way(int, char *[]);
int	verify_server2_request_discarded(int, char *[]);

int	request_unmarshal_before_check_2way_dead(int, char *[]);
int	request_unmarshal_before_check_1way_dead(int, char *[]);

int	request_unmarshal_before_lookup_2way_dead(int, char *[]);
int	request_unmarshal_before_lookup_2way_rejoin(int, char *[]);
int	request_unmarshal_before_lookup_1way_dead(int, char *[]);
int	request_unmarshal_before_lookup_1way_rejoin(int, char *[]);

int	request_unmarshal_after_check_2way_dead(int, char *[]);
int	request_unmarshal_after_check_1way_dead(int, char *[]);

int	verify_request_discarded(int, char *[]);
int	verify_request_succeeded(int, char *[]);
int	verify_testobj(int, char *[]);

int	server_done(int, char *[]);
int	testobj_done(int, char *[]);

#endif	/* _ORBTEST_ORPHAN_FI_FAULTS_ENTRIES_H */
