/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)request_unmarshal_after_check.cc	1.11	08/05/20 SMI"

#if !defined(_KERNEL)
#include <unistd.h>
#endif

#include <orbtest/orphan_fi/faults/entries.h>
#include <orbtest/orphan_fi/faults/entry_support.h>


//
// Assertion:
//	When a request message is orphaned AFTER the orphaned message
//	check in rxdoor_kit::unmarshal(), then the request shall succeed
//	and reach the server.
//
// Scenarios:
//	1. Two-way request:
//		a) Client node is marked dead by the CMM.
//	2. One-way request:
//		a) Client node dead.
//
//	NOTE: we can't do a node-rejoin test for this because the fault point
//	is located in rxdoor_kit::unmarshal() after orphaned message check
//	but before the lock to the rxdoor bucket (locked by the
//	rxdoor_manager::the().lookup_rxdoor() call) is released.  When the
//	CMM is in its cluster reconfiguration mode, it will also try to grab
//	the lock, resulting in a CMM timeout and panic.
//
// Test Strategy:
//	1. First client sets an Invo trigger for fault number
//
//		FAULTNUM_ORPHAN_UNMARSHAL_AFTER_CHECK
//
//	   which will reboot the current node.
//	2. First client makes a request to the server passing in an
//	   object reference whose implementation resides in a different
//	   node from the client.
//	3. After the request reaches rxdoor_kit::unmarshal() on the server,
//	   the client's node is rebooted and the server thread is suspended
//	   until the CMM marks the node dead.  The request should succeed
//	   and reach the server.
//	4. After the node is rebooted, a second client queries the server to
//	   verify that the first client's request reached the server.
//


//
// Twoway request.  Client node dead.
//
int
request_unmarshal_after_check_2way_dead(int, char *[])
{
	os::printf("*** Two-way REQUEST orphaned AFTER unmarshal orphan "
		"check, client node DEAD\n");

#if defined(_FAULT_INJECTION)
	orphan_fi::server_var		serverp;
	orphan_fi::testobj_var		testobjp;
	orphan_fi::testobj_var		ret_testobjp;
	FaultFunctions::wait_for_arg_t	reboot_arg;
	Environment			e;

	// Get a reference to the server.
	serverp = get_server();
	if (CORBA::is_nil(serverp)) {
		return (1);
	}

	// Tell server to reset the call flag.
	if (! reset_call_flag(serverp)) {
		return (1);
	}

	// Tell server to clear its stored testobj references, if any.
	if (! clear_stored_testobjs(serverp)) {
		return (1);
	}

	// Get a reference to the testobj.
	testobjp = get_testobj();
	if (CORBA::is_nil(testobjp)) {
		return (1);
	}

	// Set up fault argument.
	reboot_arg.nodeid = orb_conf::node_number();
	reboot_arg.op = FaultFunctions::WAIT_FOR_DEAD;

	// Arm the trigger.
	FaultFunctions::invo_trigger_add(
		FaultFunctions::REBOOT_DIFFERENT,
		FAULTNUM_ORPHAN_UNMARSHAL_AFTER_CHECK,
		&reboot_arg,
		(uint32_t)sizeof (reboot_arg));

	// Invoke the server.
	os::printf("    Making two-way invocation to server with "
		"one object ref...\n");
	ret_testobjp = serverp->twoway_one_obj(testobjp, e);
	if (check_exception(e.exception(), "ERROR:")) {
		os::printf("ERROR: can't make two-way invocation to server\n");
		InvoTriggers::clear_all();
		return (1);
	}

	// We shouldn't get here since the server will reboot this node
	// in the middle of the invocation.
	os::printf("ERROR: server didn't reboot client node\n");
	InvoTriggers::clear_all();
	return (1);
#else
	return (no_fault_injection());
#endif
}


//
// Oneway request.  Client node dead.
//
int
request_unmarshal_after_check_1way_dead(int, char *[])
{
	os::printf("*** One-way REQUEST orphaned AFTER unmarshal orphan "
		"check, client node DEAD\n");

#if defined(_FAULT_INJECTION)
	orphan_fi::server_var		serverp;
	orphan_fi::testobj_var		testobjp;
	FaultFunctions::wait_for_arg_t	reboot_arg;
	Environment			e;

	// Get a reference to the server.
	serverp = get_server();
	if (CORBA::is_nil(serverp)) {
		return (1);
	}

	// Tell server to reset the call flag.
	if (! reset_call_flag(serverp)) {
		return (1);
	}

	// Tell server to clear its stored testobj references, if any.
	if (! clear_stored_testobjs(serverp)) {
		return (1);
	}

	// Get a reference to the testobj.
	testobjp = get_testobj();
	if (CORBA::is_nil(testobjp)) {
		return (1);
	}

	// Set up fault argument.
	reboot_arg.nodeid = orb_conf::node_number();
	reboot_arg.op = FaultFunctions::WAIT_FOR_DEAD;

	// Arm the trigger.
	FaultFunctions::invo_trigger_add(
		FaultFunctions::REBOOT_DIFFERENT,
		FAULTNUM_ORPHAN_UNMARSHAL_AFTER_CHECK,
		&reboot_arg,
		(uint32_t)sizeof (reboot_arg));

	// Invoke the server.
	os::printf("    Making one-way invocation to server with "
		"one object ref...\n");
	serverp->oneway_one_obj(testobjp, e);
	if (check_exception(e.exception(), "ERROR:")) {
		os::printf("ERROR: can't make one-way invocation to server\n");
		InvoTriggers::clear_all();
		return (1);
	}

	// Since the above is a oneway call, it would have returned right
	// away.  To prevent the fault injection test driver from thinking
	// that this node didn't die, we'll sleep for a while.  If at the
	// end we're still alive, it's a test error.
#if defined(_KERNEL)
	os::usecsleep(180 * 1000000);		// sleep for 3 minutes
#else
	sleep(180);				// sleep for 3 minutes
#endif

	os::printf("ERROR: server didn't reboot client node\n");
	InvoTriggers::clear_all();
	return (1);
#else
	return (no_fault_injection());
#endif
}
