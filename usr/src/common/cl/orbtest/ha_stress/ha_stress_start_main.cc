/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)ha_stress_start_main.cc	1.11	08/05/20 SMI"

//
// User level ha stress clients
//

#include <sys/os.h>
#include <orb/infrastructure/orb.h>
#include <orbtest/ha_stress/ha_stress_start.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>

//
// Main Function
//
int
main(int argc, char *argv[])
{
	// Detach from tty
	// Code lifted from some of bart's good work on the solaris commands
	// I even sacrificed the rabbit...
	close(0);
	close(1);
	close(2);

	switch (fork1()) {
	case (pid_t)-1:
		perror("fork1");
		return (-1);
	case 0:
		break;
	default:
		// Parent
		return (0);
	}

	setsid();
	(void) open("/dev/null", O_RDWR, 0);

	if (ORB::initialize() != 0) {
		os::printf("ERROR: Cannot initialize ORB\n");
		return (1);
	}

	return (ha_stress_start_client(argc, argv));
}
