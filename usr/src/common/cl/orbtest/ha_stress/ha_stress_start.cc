/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)ha_stress_start.cc	1.7	08/05/20 SMI"

//
// HA Stress Clients
//

#include <sys/os.h>
#include <nslib/ns.h>
#include <h/naming.h>
#include <h/ha_stress.h>

#include <orbtest/ha_stress/ha_stress_impl.h>
#include <orbtest/ha_stress/ha_stress_start.h>

#if !defined(_KERNEL_ORB)
#include <signal.h>
#endif

//
// Usage
//
void
usage_start(const char *fname)
{
	os::printf("Usage: %s svc_name client_name num_threads\n", fname);
}


//
// ha_stress_clients
//
// Entry point for All modes
// (Kernel, User, UNODE)
//
int
ha_stress_start_client(int argc, char *argv[])
{
	char			*svc_name	= NULL;
	char			*client_name	= NULL;
	int			num_threads	= 0;
	ha_stress::client_var	test_client_ref;


	// There must be 3 arguments
	if (argc < 4) {
		usage_start(argv[0]);
		return (1);
	}

	// Extract the service name and the number of threads
	svc_name = argv[1];
	client_name = argv[2];
	num_threads = os::atoi(argv[3]);

	// Number of threads has to greater than 0
	if (num_threads == 0) {
		os::printf("invalid num_threads (%s)\n", argv[3]);
		usage_start(argv[0]);
		return (1);
	}

	// Create a ha_stress::client object
	test_client_ref = (new ha_stress_impl)->get_objref();
	ASSERT(!CORBA::is_nil(test_client_ref));

	// Bind this client to the name server
	Environment			e;
	naming::naming_context_var	ctxp;

	ctxp = ns::root_nameserver();
	ASSERT(!CORBA::is_nil(ctxp));

	ctxp->rebind((const char *)client_name, test_client_ref, e);
	if (e.exception()) {
		e.exception()->print_exception("rebind");
		return (1);
	}

	// Now we start the tests
	test_client_ref->start(svc_name, num_threads, e);
	if (e.exception()) {
		e.exception()->print_exception("start");
		return (1);
	}

#if !defined(_KERNEL)
	// Now wait for the tests to complete
	test_client_ref->wait_for_done(e);
	if (e.exception()) {
		e.exception()->print_exception("start");
		return (1);
	}
#endif // !_KERNEL

	return (0);
}
