/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _HA_STRESS_IMPL_H
#define	_HA_STRESS_IMPL_H

#pragma ident	"@(#)ha_stress_impl.h	1.10	08/05/20 SMI"

#include <sys/os.h>
#include <orb/object/adapter.h>
#include <h/repl_sample.h>
#include <h/ha_stress.h>

//
// HA Stress Clients
//
// This ORB object is registered on the nameserver when the client is started
// all HA Stress clients will have the name ha_stress_ prefixed.
//
// These ORB objects can be used to stop the client, and get the results
// from the tests from it
//
class ha_stress_impl : public McServerof<ha_stress::client> {
public:
	class test_client {
	public:
		// Constructor
		test_client(const char *name, int16_t num);

		// Destructor
		~test_client();

		//
		// Start test
		//
		// Entry point for each test thread to start testing
		void start();

		//
		// Stop tests
		//
		// Will fill in the error vector (ends on a NULL)
		char **stop();

		//
		// Client Types
		//
		// Will return a string with a error message if there was an
		// error

		// Simple client
		// The simple client will do mini-transactions.
		// For each iteration of the simple mini-transaction, we will
		// get a reference to an HA object, and do simple
		// mini-transactions on it.
		//
		// Every XXX transaction we will discard the old HA object
		// and get a new one
		//
		char	*simple_client();

		// return the svc_name
		const char *svc_name();

	private:
		// Name of service we will be targeting
		char		*_svc_name;

		// Number of threads we'll start
		int16_t		num_threads;

		// Error vector
		char		**error_v;
		int		error_v_closed;
		os::mutex_t	error_v_lock;

		// Set when we're done testing
		int		done;
		os::mutex_t	done_lock;

		// Mechanism for keeping count of threads (kernel/user space)
		int		thread_count;
		int		active_thread_count;
		os::mutex_t	thread_count_lock;
		os::condvar_t	thread_count_cv;

		// Helper to get the factory of the repl_sample objects
		repl_sample::factory_ptr get_factory(Environment &e);

		// Helper to get a value obj from the repl sample factory
		repl_sample::value_obj_ptr get_value_obj(Environment &e);

		// Helper for format eror messages
		char *error_msg(const char *fmt, ...);

	}; // class test_client

	// Constructor
	ha_stress_impl() : p_client(NULL), done(0), num_threads(0),
		McServerof<ha_stress::client>() { }

	// Destructor
	~ha_stress_impl() { delete p_client; }


	// IDL Members

	//
	// Start tests
	// Will begin running tests against the specified svc, and the
	// specified number of threads
	//
	// Arguments:
	//	svc_name	- name of service
	//	num_threads	- number of test threads
	void start(const char *svc_name, int16_t num_threads, Environment &e);

	//
	// Stop tests
	// Will stop all threads on the test
	// Will return a sequence or error strings from the threads
	// If not error strings are returned, there were no errors
	//
	// Arguments:
	//	svc_name	- returns the name of service this was a client
	//			  for.
	//	errors		- Sequence of error strings
	void stop(CORBA::String_out svc_name, ha_stress::StringSeq_out errors,
	    Environment &e);

	//
	// Wait for done
	//
	// This invocation is to be used by the process that started
	// this server in user space ORB
	// so that it can wait for the server to be no longer in use
	void wait_for_done(Environment &e);

	// Unreferenced
	void	_unreferenced(unref_t);

	//
	// Implementation Specific
	//

	// Start point for threads
	static void *thread_start(void *arg);

private:
	// Pointer to test client object
	test_client	*p_client;
	int		num_threads;
	os::mutex_t	client_lock;

	// Set when we're done
	int		done;
	os::mutex_t	done_lock;
	os::condvar_t	done_cv;

}; // class ha_stress_impl

#endif	/* _HA_STRESS_IMPL_H */
