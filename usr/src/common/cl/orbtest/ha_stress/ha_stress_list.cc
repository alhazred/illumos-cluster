/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)ha_stress_list.cc	1.9	08/05/20 SMI"

//
// HA Stress Clients Stop
//

#include <sys/os.h>
#include <nslib/ns.h>
#include <h/naming.h>
#include <unistd.h>

//
// Usage
//
void
usage_list(const char *fname)
{
	os::printf("Usage: %s test_prefix [print]\n", fname);
}


//
// ha_stress_clients
//
// Entry point for All modes
// (Kernel, User, UNODE)
//
int
ha_stress_list_client(int argc, char *argv[])
{
	char	*prefix	= NULL;
	int	print_names = 0;
	unsigned int	index;

	// There must be 1 arguments
	if (argc < 2) {
		usage_list(argv[0]);
		return (1);
	}

	// Extract the prefix for all test clients
	// We'll still have to comb the list later and extract the tests
	// services and test clients
	prefix = argv[1];

	// If there is another arguments and its print, then print the names
	// of the clients in core
	if (argc > 2) {
		if (os::strcmp(argv[2], "print") == 0) {
			print_names = 1;
		}
	}

	// Get a list of names from the global name server
	Environment			e;
	naming::naming_context_var	ctxp;

	ctxp = ns::root_nameserver();
	ASSERT(!CORBA::is_nil(ctxp));

	// Get a list of bindings
	naming::binding_iterator_var	bi_ref;
	naming::binding_list_var	bl_ref;
	ctxp->list(128, bl_ref, bi_ref, e);
	if (e.exception()) {
		e.exception()->print_exception("list");
		return (1);
	}

	// Count the number of objects that match the name criteria
	// which is prefix.*.client.*
	char	crit[20];
	os::sprintf(crit, ".client.");

	int match_count = 0;
	for (index = 0; index < bl_ref->length(); index++) {
		if (strstr((const char *)(bl_ref[index].binding_name), crit) !=
		    NULL) {
			match_count++;
			if (print_names) {
				os::printf("%s\n",
				    (const char *)(bl_ref[index].binding_name));
			}
		}
	}

	// Check if there is more stuff to be read from the iterator
	while (!CORBA::is_nil(bi_ref)) {
		// Get the list
		bi_ref->next_n(128, bl_ref, e);
		if (e.exception()) {
			e.exception()->print_exception("next_n");
			return (1);
		}

		if (bl_ref->length() == 0) {
			break;
		}

		for (index = 0; index < bl_ref->length(); index++) {
			if (strstr((const char *)(bl_ref[index].binding_name),
			    crit) != NULL) {
				match_count++;
				if (print_names) {
					os::printf("%s\n", (const char *)
					    (bl_ref[index].binding_name));
				}
			}
		}
	}

	os::printf("%d\n", match_count);

	return (0);
}
