/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)ha_stress_stop.cc	1.7	08/05/20 SMI"

//
// HA Stress Clients Stop
//

#include <sys/os.h>
#include <nslib/ns.h>
#include <h/naming.h>
#include <h/ha_stress.h>
#include <unistd.h>

// Print constants
#define	PR_INFO		"INFO"
#define	PR_WARNING	"WARNING"
#define	PR_ERROR	"ERROR"

#define	PR_CLIENT	"client"


//
// Usage
//
void
usage_stop(const char *fname)
{
	os::printf("Usage: %s client_name\n", fname);
}


//
// ha_stress_clients
//
// Entry point for All modes
// (Kernel, User, UNODE)
//
int
ha_stress_stop_client(int argc, char *argv[])
{
	char			*client_name	= NULL;
	ha_stress::client_var	test_client_ref;


	// There must be 1 arguments
	if (argc < 2) {
		usage_stop(argv[0]);
		return (1);
	}

	// Extract the service name and the number of threads
	client_name = argv[1];

	// Bind this client to the name server
	Environment			e;
	naming::naming_context_var	ctxp;
	CORBA::Object_var		obj_ref;

	ctxp = ns::root_nameserver();
	ASSERT(!CORBA::is_nil(ctxp));

	// First resolve the test client object
	obj_ref = ctxp->resolve(client_name, e);
	if (e.exception()) {
		e.exception()->print_exception("resolve");
		return (1);
	}
	ASSERT(!CORBA::is_nil(obj_ref));

	// Cast it to the ha_stress::client
	test_client_ref = ha_stress::client::_narrow(obj_ref);
	ASSERT(!CORBA::is_nil(test_client_ref));

	// Now unbind this client test object from the nameserver
	ctxp->unbind((const char *)client_name, e);
	if (e.exception()) {
		e.exception()->print_exception("unbind");
		return (1);
	}

	// Now we stop the tests
	ha_stress::StringSeq_var		errors;
	char					*svc_name;

	test_client_ref->stop(svc_name, errors, e);
	if (e.exception()) {
		e.exception()->print_exception("stop");
		return (0);
	}

	// Print the errors
	for (unsigned int index = 0; index < errors.length(); index++) {
		os::printf("%s\n", (const char *)errors[index]);
	}

	// os::printf("XXX: stop_client completed\n");

	return (0);
}
