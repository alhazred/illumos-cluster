/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)ha_stress_impl.cc	1.14	08/05/20 SMI"

#include <nslib/ns.h>
#include <h/naming.h>
#include <h/replica.h>
#include <orbtest/ha_stress/ha_stress_impl.h>

#ifdef _KERNEL
#include <sys/varargs.h>
#include <sys/cmn_err.h>
#else
#include <thread.h>
#include <sched.h>
#include <stdio.h>
#include <stdarg.h>
#endif


//
// Test Clients
//

#define	MAX_SIMPLE_TRANSACTIONS		10
#define	NUM_CLIENT_TYPES		1
#define	KERNEL_INVO_SLEEP_TIME		1000000		// 1 second
#define	USER_INVO_SLEEP_TIME		250000		// .25 second

#define	ERROR_BUFFER_LEN		256

//
// Constructor
//
ha_stress_impl::test_client::test_client(const char *name, int16_t num) :
    _svc_name(os::strdup(name)), done(0), thread_count(0),
    active_thread_count(0), error_v(NULL), error_v_closed(1)
{
	ASSERT(name != NULL);

	num_threads = num;

	// Initialize the error string vector
	error_v = new char*[num];
	for (int16_t index = 0; index < num; index++) {
		error_v[index] = NULL;
	}

	// Set the error_vector open
	error_v_closed = 0;
}


//
// Destructor
//
ha_stress_impl::test_client::~test_client()
{
	int16_t index;
	delete []_svc_name;
	if (error_v != NULL) {
		for (index = 0; index < num_threads; index++) {
			if (error_v[index] != NULL) {
				delete [](error_v[index]);
			}
		}
		delete []error_v;
	}
}


//
// start tests
//
// Entry point for each client thread to start testing
void
ha_stress_impl::test_client::start()
{
	int	thr_number;
	char	*error = NULL;

	// Grab the done lock and examine the done variable
	done_lock.lock();
	if (!done) {
		// Update the thread_count
		thread_count_lock.lock();
		thread_count++;
		active_thread_count++;
		thr_number = thread_count;
		thread_count_lock.unlock();
	} else {
		done_lock.unlock();
		return;
	}
	done_lock.unlock();

	// Make sure we didn't spawn too many threads
	ASSERT(thr_number <= num_threads);

	// os::printf("XXX: Thread %d started...\n", thr_number);
	int	client_index = thr_number % NUM_CLIENT_TYPES;

	// Determine which client this thread should run
	switch (client_index) {
	case 0:
		// Simple Client
		error = simple_client();
		break;
	default:
		// Invalid
		ASSERT(false);
		break;
	}

/*
	if (error != NULL) {
		os::printf("XXX: Test thread returned with error: %s\n",
		    error);
	} else {
		os::printf("XXX: Test thread returned with no error\n");
	}
*/

	// If we got an error store it in the error vector
	error_v_lock.lock();
	if (!error_v_closed)
		error_v[thr_number - 1] = error;
	error_v_lock.unlock();

	// Update the thread_count
	thread_count_lock.lock();
	active_thread_count--;
	if (active_thread_count == 0)
		thread_count_cv.signal();
	thread_count_lock.unlock();
}

//
// Stop tests
//
// Will fill in the error vector and return it
char **
ha_stress_impl::test_client::stop()
{
	// Grab the done lock and set done so threads will begin to exit
	done_lock.lock();
	done = 1;
	done_lock.unlock();

	// os::printf("XXX: Waiting for (%d) test threads to finish\n",
	//    num_threads);

	// Wait for threads to exit
	thread_count_lock.lock();
	while (active_thread_count != 0) {
		thread_count_cv.wait(&thread_count_lock);
	}

	// os::printf("XXX: Done waiting\n");

	error_v_lock.lock();
	error_v_closed = 1;
	error_v_lock.unlock();

	// Now we will return a pointer to the error_vector
	return (error_v);
}


//
// simple client
//
// The simple client will do mini-transactions.
// For each iteration of the simple mini-transaction, we will get a reference
// to an HA object, and do simple mini-transactions on it.
//
// Every XXX transaction we will discard the old HA object and get a new one
//
char *
ha_stress_impl::test_client::simple_client()
{
	char				*error			= NULL;
	long				transaction_count	= 0;
	repl_sample::value_obj_var	obj_ref;
	Environment			e;
	int16_t				nodenum			= 0;
	int32_t				value			= 0;

	// os::printf("XXX: Simple client\n");

	for (;;) {
		done_lock.lock();
		if (done) {
			done_lock.unlock();
			break;
		}
		done_lock.unlock();

		// If we're at the top of the transaction count get a reference
		// to an HA Object
		if (transaction_count == 0) {
			// Create a reference to a value_obj
			obj_ref = get_value_obj(e);
			if (e.exception()) {
				error = error_msg("%s (%s)",
				    e.exception()->_name(), _svc_name);
				break;
			}

			if (CORBA::is_nil(obj_ref)) {
				error = error_msg("Could not get reference to"
				    " value_obj (%s)\n", _svc_name);
				break;
			}
		}

		// Set the value of the HA Object
		obj_ref->set_value((int)transaction_count, nodenum, e);
		if (e.exception()) {
			error = error_msg("%s (%s::set_value)",
			    e.exception()->_name(), _svc_name);
			break;
		}

		// Increment the value
		obj_ref->inc_value(nodenum, e);
		if (e.exception()) {
			error = error_msg("%s (%s::inc_value)",
			    e.exception()->_name(), _svc_name);
			break;
		}

		// Get the resulting value
		value = obj_ref->get_value(nodenum, e);
		if (e.exception()) {
			error = error_msg("%s (%s::get_value)",
			    e.exception()->_name(), _svc_name);
			break;
		}

		// Increment the transaction count
		transaction_count++;

		// Values should match
		if (transaction_count != value) {
			error = error_msg("count mismatch with obj "
			    "value (%l != %l) (%s)", transaction_count,
			    (long)value, _svc_name);
		}

		// Check if we've reached the MAX_SIMPLE_TRANSACTIONS
		if (transaction_count >= MAX_SIMPLE_TRANSACTIONS) {
			transaction_count = 0;
		}


#if defined(_KERNEL)
		os::usecsleep(KERNEL_INVO_SLEEP_TIME);
#else
		os::usecsleep(USER_INVO_SLEEP_TIME);
#endif

	}

	// os::printf("XXX: Exiting simple client...\n");

	return (error);
}


//
// error msg
// return buffer with error message in it.  (Static length buffer)
//
char *
ha_stress_impl::test_client::error_msg(const char *fmt, ...)
{
	va_list	ap;
	char	*rslt = new char[ERROR_BUFFER_LEN];
	ASSERT(rslt != NULL);

	va_start(ap, fmt);
	(void) ::vsnprintf(rslt, ERROR_BUFFER_LEN, fmt, ap);
	va_end(ap);

	return (rslt);
}


//
// Retrieve the factory from the nameserver
// Get the factory for a specific service
//
repl_sample::factory_ptr
ha_stress_impl::test_client::get_factory(Environment &e)
{
	// os::printf("get_factory()\n");

	// Get a factory name
	char	factory_name[25];
	os::sprintf(factory_name, "%s factory", _svc_name);

	naming::naming_context_var ctxp = ns::root_nameserver();
	CORBA::Object_var obj_ref = ctxp->resolve(factory_name, e);
	if (e.exception()) {
		// e.exception()->print_exception("get_factory()");
		return (nil);
	}

	// Make this a factory ptr, so we can return the new reference that
	// narrow returns.  (And we dont have to do a _duplicate)
	repl_sample::factory_ptr fp = repl_sample::factory::_narrow(obj_ref);

	ASSERT(!CORBA::is_nil(fp));

	return (fp);
}


//
// Create a value_obj
// Must get a factory, and then get a value_obj
//
repl_sample::value_obj_ptr
ha_stress_impl::test_client::get_value_obj(Environment &e)
{
	repl_sample::factory_var factory_ref = get_factory(e);
	if (CORBA::is_nil(factory_ref))
		return (nil);

	// Create a value object
	repl_sample::value_obj_ptr objp = factory_ref->create_value_obj(e);
	if (e.exception()) {
		// e.exception()->print_exception("get_value_obj()");
		return (nil);
	}

	return (objp);
}

//
// Return the _svc_name
//
const char *
ha_stress_impl::test_client::svc_name()
{
	ASSERT(_svc_name != NULL);

	return (_svc_name);
}


//
// HA Stress Impl
//

//
// _unreferenced
//
void
#ifdef DEBUG
ha_stress_impl::_unreferenced(unref_t cookie)
#else
ha_stress_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	delete this;
}

//
// Start tests
// Will begin running tests against the specified svc, and the
// specified number of threads
//
// Arguments:
//	svc_name	- name of service
//	num_threads	- number of test threads
void
ha_stress_impl::start(const char *svc_name, int16_t num, Environment &)
{
	ASSERT(svc_name != NULL);
	ASSERT(num != 0);

	client_lock.lock();
	ASSERT(p_client == NULL);

	// Save the number of threads
	num_threads = num;

	// Allocate a client object
	p_client = new test_client(svc_name, num_threads);

	// os::printf("XXX: Will create %d test threads\n", num_threads);

	// Now start new threads as specified
	for (int thr_count = 0; thr_count < num_threads; thr_count++) {
#ifdef _KERNEL
		if (thread_create(NULL, NULL,
		    (void(*)())ha_stress_impl::thread_start, (caddr_t)p_client,
		    0, &p0, TS_RUN, 60) == NULL) {
			return;
		}
#else
		if (os::thread::create(NULL, (size_t)0,
		    ha_stress_impl::thread_start, p_client, THR_BOUND, NULL)) {
			return;
		}
#endif
	}

	// os::printf("XXX: Started %d threads\n", num_threads);

	client_lock.unlock();
}


//
// Stop tests
// Will stop all threads on the test
// Will return a sequence or error strings from the threads
// If not error strings are returned, there were no errors
void
ha_stress_impl::stop(CORBA::String_out svc_name,
    ha_stress::StringSeq_out errors, Environment &e)
{
	// os::printf("XXX: ha_stress_impl::stop()\n");

	char		**error_v = NULL;

	client_lock.lock();
	if (p_client == NULL) {
		client_lock.unlock();
		e.exception(new ha_stress::already_stopped);
		return;
	}

	//
	// set the svc_name to the svc_name in the object
	//
	svc_name = os::strdup(p_client->svc_name());

	//
	// we need to get the error vector and copy it to the errors
	// string sequence
	//
	error_v = p_client->stop();
	errors = new ha_stress::StringSeq(num_threads);

	// os::printf("XXX: max: %d length: %d\n", (*errors).maximum(),
	//    (*errors).length());

	int seq_index = 0;

	if (error_v  != NULL) {
		for (int index = 0; index < num_threads; index++) {
			if (error_v[index] != NULL) {
				(*errors).length((*errors).length() + 1);
				(*errors)[seq_index] =
				    (const char *)error_v[index];
				seq_index++;
			}
		}
	}

	delete p_client;
	p_client = NULL;

	client_lock.unlock();

	done_lock.lock();
	done = 1;
	done_cv.signal();
	done_lock.unlock();
}


//
// Wait for done
//
// This invocation is to be used by the process that started
// this server in user space ORB
// so that it can wait for the server to be no longer in use
void
ha_stress_impl::wait_for_done(Environment &)
{
	done_lock.lock();
	while (!done)
		done_cv.wait(&done_lock);
	done_lock.unlock();
}


// Start point for threads
void *
ha_stress_impl::thread_start(void *arg)
{
	test_client	*tc	= (test_client *) arg;
	ASSERT(tc);

	tc->start();

	return (NULL);
}
