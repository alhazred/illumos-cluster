/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rm_primary_node.cc	1.9	08/05/20 SMI"

#include <orb/fault/fault_injection.h>

//
// Will exit with the nodeid of the HA RM Primary host
//
int
rm_primary_node(int, char **)
{
#ifdef _FAULT_INJECTION

#if defined(_KERNEL_ORB) && !defined(_KERNEL)
	return (FaultFunctions::rm_primary_node());
#else

	nodeid_t nodeid;

	nodeid = FaultFunctions::rm_primary_node();
	os::printf("HA_RM %d\n", nodeid);
	return (nodeid);
	// return (FaultFunctions::rm_primary_node());
#endif

#else
	return (0);
#endif
}
