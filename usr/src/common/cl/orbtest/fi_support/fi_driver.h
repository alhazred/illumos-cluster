/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FI_DRIVER_H
#define	_FI_DRIVER_H

#pragma ident	"@(#)fi_driver.h	1.9	08/05/20 SMI"

//
// Driver library for kernel and user-level fault-injection tests.
//
// Each test must define a NULL-terminated array of fi_entry_t structs.
// Each member of the array consists of a pointer to a test entry function
// and its string name of the function.  The ENTRY() macro defined in
// fi_driver.h can be used to simplify the construction of the array, e.g.
//
//	fi_entry_t fi_entry[] = {
//		ENTRY(entry_func1),
//		ENTRY(entry_func2),
//		ENTRY(NULL)
//	};
//
// Each test entry function must be of type:
//
//	int (*) (int argc, char *argv[])
//
// It must return 0 when successful, non-zero otherwise.
//
// User-level tests can simply call do_fi_test() with argc and argv from
// main(), and the fi_entry_t array to call an entry function, e.g.:
//
//	main(int argc, char *argv[])
//	{
//		...
//		result = do_fi_test(argc, argv, fi_entry);
//		return (result);
//	}
//
// Kernel test modules can call the mod_name_to_args() function first to
// convert entry function name and arguments imbedded in the module name to
// argc and argv[] and then call do_fi_test(), e.g.:
//
//	int
//	_init(void)
//	{
//		struct modctl	*mp;
//		int		argc;
//		char		**argv;
//
//		...
//
//		// Get module name.
//		if ((mp = mod_getctl(&modlinkage)) == NULL) {
//			_cplpl_fini();
//			mod_remove(&modlinkage);
//			return (EIO);
//		}
//
//		mod_name_to_args(mp->mod_modname, argc, argv);
//		error = do_fi_test(argc, argv, fi_entry);
//
//		// Clean up argument vectors allocated by mod_name_to_args().
//		for (int i = 0; i < argc; ++i) {
//			delete[] argv[i];
//		}
//		delete[] argv;
//
//		...
//	}
//

// Structure for specifying test entry functions.
struct fi_entry_t {
	int (*entry) (int argc, char *argv[]);
	char	*name;			// string name of entry function
};

// Convenience macro for listing entry functions in an fi_entry_t array.
#define	ENTRY(f)	{f, #f}

//
// Compare the specified test (argv[0]) against each string name in the
// entry_list[] array.  If there is a match, then call the corresponding
// entry function, passing argc and argv to it.  Note, to the entry function,
// argv[0] will be the its string name.
//
int	do_fi_test(int argc, char *argv[], fi_entry_t entry_list[]);

#if defined(_KERNEL)
//
// Parses a module name into a test entry point and arguments.
//
// Arguments:
//	modname	- The module name (needs to be parsed).
//	argc	- Out argument.  Store the argument count.
//	argv	- Out argument.  Store the argument vector (argv[0] will be
//		  the name of the test to run).
//
void	mod_name_to_args(char *modname, int &argc, char **&argv);
#endif

#endif	/* _FI_DRIVER_H */
