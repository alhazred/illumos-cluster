/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fi_driver.cc	1.10	08/05/20 SMI"

//
// Driver library for kernel and user-level fault-injection tests.
//
// Each test must define a NULL-terminated array of fi_entry_t structs.
// Each member of the array consists of a pointer to a test entry function
// and its string name of the function.  The ENTRY() macro defined in
// fi_driver.h can be used to simplify the construction of the array, e.g.
//
//	fi_entry_t fi_entry[] = {
//		ENTRY(entry_func1),
//		ENTRY(entry_func2),
//		ENTRY(NULL)
//	};
//
// Each test entry function must be of type:
//
//	int (*) (int argc, char *argv[])
//
// It must return 0 when successful, non-zero otherwise.
//
// User-level tests can simply call do_fi_test() with argc and argv from
// main(), and the fi_entry_t array to call an entry function, e.g.:
//
//	main(int argc, char *argv[])
//	{
//		...
//		result = do_fi_test(argc, argv, fi_entry);
//		return (result);
//	}
//
// Kernel test modules can call the mod_name_to_args() function first to
// convert entry function name and arguments imbedded in the module name to
// argc and argv[] and then call do_fi_test(), e.g.:
//
//	int
//	_init(void)
//	{
//		struct modctl	*mp;
//		int		argc;
//		char		**argv;
//
//		...
//
//		// Get module name.
//		if ((mp = mod_getctl(&modlinkage)) == NULL) {
//			_cplpl_fini();
//			mod_remove(&modlinkage);
//			return (EIO);
//		}
//
//		mod_name_to_args(mp->mod_modname, argc, argv);
//		error = do_fi_test(argc, argv, fi_entry);
//
//		// Clean up argument vectors allocated by mod_name_to_args().
//		for (int i = 0; i < argc; ++i) {
//			delete[] argv[i];
//		}
//		delete[] argv;
//
//		...
//	}
//

#if defined(_KERNEL)
#include <sys/modctl.h>
#else
#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#endif

#include <sys/os.h>
#include <sys/errno.h>
#include <orb/invo/common.h>
#include <orb/invo/corba.h>

#include "fi_driver.h"

//
// Compare the specified test (argv[0]) against each string name in the
// entry_list[] array.  If there is a match, then call the corresponding
// entry function, passing argc and argv to it.  Note, to the entry function,
// argv[0] will be the its string name.
//
int
do_fi_test(int argc, char *argv[], fi_entry_t entry_list[])
{
	char	*test;
	int	i, rslt;

	// The entry is hidden in the program name
	test = argv[0];

#if !defined(_KERNEL)
	char	*tmp;
	// Check wether its a full path or just filename
	tmp = strrchr(test, '/');
	if (tmp != NULL)
		test = tmp + 1;
#endif

	for (i = 0; entry_list[i].entry != NULL; ++i) {
		if (os::strcmp(entry_list[i].name, test) == 0) {
			rslt = entry_list[i].entry(argc, argv);
			return (rslt);
		}
	}

	// No test matched.
	os::printf("Test (%s) specified is invalid\n", test);
	return (1);
}

#if defined(_KERNEL)
//
// Helper to massage the string that the kernel will get into a test
// entry point and arguments (which have been stuffed in the module name)
//
// Arguments:
//	modname	- The module name (needs to be parsed).
//	argc	- Out argument.  Store the argument count.
//	argv	- Out argument.  Store the argument vector (argv[0] will be
//		  the name of the test to run).
//
void
mod_name_to_args(char *modname, int &argc, char **&argv)
{
	int	len = (int)os::strlen(modname);
	int	counter;
	char	*start;

	// Go through the name and find how many args there are
	argc = 1;
	for (counter = 0; counter < len; counter++) {
		if ((modname[counter] == '.') &&
		    (modname[counter + 1] != '\0')) {
			argc++;
		}
	}

	// Allocate space for these args
	argv = new char *[argc];
	ASSERT(argv);

	// Now copy the arguments
	start = modname;
	for (counter = 0; counter < argc; counter++) {
		int	arg_len = 0;

		// Figure out the lenght of this argument
		while ((start[arg_len] != '.') && (start[arg_len] != '\0')) {
			arg_len++;
		}

		argv[counter] = new char[arg_len + 1];
		ASSERT(argv[counter]);

		argv[counter][arg_len] = '\0';
		if (arg_len > 0) {
			os::strncpy(argv[counter], start, arg_len);
		}

		start += arg_len + 1;
	}
}
#endif // _KERNEL
