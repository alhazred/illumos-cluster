/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fi_driver_mod.cc	1.7	08/05/20 SMI"

//
// A simple kernel module to provide the fault injection test driver
// functions (see fi_driver.cc).
//

#include <sys/modctl.h>

extern "C" {
	void _cplpl_init(void);
	void _cplpl_fini(void);
}

char _depends_on[] = "misc/cl_runtime";

extern struct mod_ops mod_miscops;

static struct modlmisc modlmisc = {
	&mod_miscops, "fault injection test driver"
};

static struct modlinkage modlinkage = {
	MODREV_1, { (void *)&modlmisc, NULL }
};

int
_init(void)
{
	int	error;

	if (error = mod_install(&modlinkage)) {
		return (error);
	}
	_cplpl_init();				// C++ initialization.

	return (0);
}

int
_fini(void)
{
	_cplpl_fini();
	return (mod_remove(&modlinkage));
}

int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}
