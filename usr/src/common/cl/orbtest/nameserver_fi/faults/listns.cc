/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)listns.cc	1.13	08/05/20 SMI"

#include <orbtest/nameserver_fi/faults/entries.h>

//
// Assertion:
//
//	When a client performs list nameserver operation, and nameserver
//	primary dies during list operation, list operation from the client
//	should be successful. Verify by resolving the object bound from
//	the new nameserver primary.
//
// Test Strategy:
//
//	1. Using switchover, set up the nameserver primary to be a remote
//	   node (node other than the client node doing the list operation,
//	   because nameserver primary will be rebooted as part of list
//	   operation). Currently node number for nameserver primary is
//	   a command line argument (in fault.data file).
//
//	2. On the nameserver primary node, trigger add the fault point in
//	   the code path of list operation
//
//	3. From the client, list all contexts/objects under root nameserver
//	   and this should boot the nameserver primary (because of the
//	   trigger that was added).
//
//	4. Verify that list operation went thru successfully. list should
//	   not give any error messages.
//
//	5. Cleanup
//

static int list_context(naming::naming_context_ptr ctxp, int indent);
static void list_info(naming::naming_context_ptr ctxp,
	naming::binding_list_var bl, int indent);
static char *set_prefix(int indent);

int
listns_test(int argc, char *argv[])
{

#if defined(_FAULT_INJECTION)

	if (argc < 2) {
		os::printf("ERROR: Wrong number of arguments \n");
		return (1);
	}

	// First argument has the node number on which you
	// expect the 'nameserver primary' to be located.

	int id = (int)os::atoi(argv[1]);
	nodeid_t new_primary = (nodeid_t)id;


	//
	// Switchover 'repl_name_server' to the node specified
	// in fault.data file
	//

	if (switchover_nameserver_primary(new_primary)) {
		os::printf("FAIL: Couldn't switchover '%s' to '%d' \n",
			REPL_NS_SVC_NAME, new_primary);
		return (1);
	}

	// Add the trigger on nameserver primary. Fault function
	// is to reboot

	FaultFunctions::wait_for_arg_t	ns_reboot_arg;

	ns_reboot_arg.op = FaultFunctions::WAIT_FOR_UNKNOWN;
	ns_reboot_arg.nodeid = new_primary;

	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT_ONCE,
	    FAULTNUM_NS_LIST, &ns_reboot_arg, (uint32_t)sizeof (ns_reboot_arg),
	    new_primary);


	//
	// Get all the objects/contexts from the root nameserver
	//

	naming::naming_context_var ctxp = ns::root_nameserver();

	if (list_context(ctxp, -1) == 0) {
		os::printf("PASS: list test succeeded \n");
		return (0);
	} else {
		os::printf("FAIL: list test failed \n");
		return (1);
	}

#else
	argc, argv;	// shut compiler warning
	return (no_fault_injection());
#endif
}


//
// XXX - The following code was lifted from src/ns_util/listns.cc
// I haven't done any changes to Anil's code.

// List all objects in the specified context
//

int
list_context(naming::naming_context_ptr ctxp, int indent)
{
#if defined(_FAULT_INJECTION)
	Environment e;
	naming::binding_list_var bl;
	naming::binding_iterator_ptr iter;

	if (++indent > 8)
		return (0);

	ctxp->list(10, bl, iter, e);
	if (e.exception()) {
		e.clear();
		os::printf("ERROR: listns - error listing context\n");
		return (1);
	}

	if (bl->length() == 0)
		return (0);

	list_info(ctxp, bl, indent);

	if (!CORBA::is_nil(iter)) {
		for (;;) {
			iter->next_n(10, bl, e);
			if (e.exception()) {
				e.clear();
				break;
			}
			if (bl->length() == 0)
				break;

			list_info(ctxp, bl, indent);
		}
	}
	CORBA::release(iter);
	return (0);
#else
	ctxp, indent;	// shut compiler warning
	return (no_fault_injection());
#endif
}

//
// From src/ns_util/listns.cc
//

void
list_info(naming::naming_context_ptr ctxp, naming::binding_list_var bl,
    int indent)
{
#if defined(_FAULT_INJECTION)
	int len;
	char *prefix = NULL;
	Environment e;
	CORBA::Object_ptr obj;
	naming::naming_context_ptr cctxp;

	prefix = set_prefix(indent);

	len = bl->length();
	for (unsigned int i = 0; i < len; i++) {
		char *name = bl[i].binding_name;
		if (bl[i].bind_type == naming::ncontext) {
			os::printf("%sc  %s\n", prefix, name);
			obj = ctxp->resolve(name, e);
			if (e.exception()) {
				e.clear();
				os::printf("ERROR resolving ctx %s\n", name);
			}
			cctxp = naming::naming_context::_narrow(obj);
			CORBA::release(obj);
			list_context(cctxp, indent);
			CORBA::release(cctxp);
		} else {
			os::printf("%so  %s\n", prefix, name);
		}
	}
#else
	ctxp, bl, indent;	// shut compiler warning
#endif
}

// XXX - From src/ns_util/listns.cc

char *
set_prefix(int indent)
{
#if defined(_FAULT_INJECTION)
	char *prefix = NULL;

	prefix = new char[indent + 1];

	switch (indent) {
	case 1:
		strcpy(prefix, "\t");
		break;
	case 2:
		strcpy(prefix, "\t\t");
		break;
	case 3:
		strcpy(prefix, "\t\t\t");
		break;
	case 4:
		strcpy(prefix, "\t\t\t\t");
		break;
	case 5:
		strcpy(prefix, "\t\t\t\t\t");
		break;
	case 6:
		strcpy(prefix, "\t\t\t\t\t\t");
		break;
	case 7:
		strcpy(prefix, "\t\t\t\t\t\t\t");
		break;
	case 8:
		strcpy(prefix, "\t\t\t\t\t\t\t\t");
		break;
	default:
		break;
	}
	return (prefix);
#else
	indent;	// shut compiler warning
	return (NULL);
#endif
}
