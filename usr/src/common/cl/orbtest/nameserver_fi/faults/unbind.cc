/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)unbind.cc	1.12	08/05/20 SMI"

#include <orbtest/nameserver_fi/faults/entries.h>

#define	UNBIND_TEST_OBJ	"unbind_test_obj"

//
// Assertion:
//
//	When a client binds an object with the root nameserver, resolves
//	and then unbinds the object and if the nameserver primary dies
//	during unbind operation, unbind operation from the client should
//	be successful. Verify by resolving the object bound from the
//	new nameserver primary and resolve should fail.
//
// Test Strategy:
//
//	1. Using switchover, set up the nameserver primary to be a remote
//	   node (node other than the client node doing the unbind operation,
//	   because nameserver primary will be rebooted as part of unbind
//	   operation). Currently node number for nameserver primary is
//	   a command line argument (in fault.data file).
//
//	2. From the client, bind a new object to the root nameserver.
//
//	3. Verify that bind operation went thru successfully by
//	   resolving the object from the root nameserver.
//
//	4. On nameserver primary node, trigger add the fault point in
//	   the code path of unbind operation
//
//	5. unbind the object and this should reboot the nameserver primary.
//
//	6. Verify that unbind went thru successfully by resolving the
//	   object from the root nameserver again. This should fail.
//
//	7. Do the cleanup.


//
// unbind_test_1 tests the fault point FAULTNUM_NS_UNBIND
//

int
unbind_test_1(int argc, char *argv[])
{

#if defined(_FAULT_INJECTION)

	if (argc < 2) {
		os::printf("ERROR: Wrong number of arguments \n");
		return (1);
	}

	// First argument has the node number on which you
	// expect the 'nameserver primary' to be located.

	int id = (int)os::atoi(argv[1]);
	nodeid_t nameserver_primary = (nodeid_t)id;

	//
	// Perform the unbind_test (add trigger, do the bind,
	// resolve the object bound, unbind and cleanup
	//

	if (unbind_test(nameserver_primary, FAULTNUM_NS_UNBIND)) {
		os::printf("FAIL: unbind test failed \n");
		return (1);
	} else {
		os::printf("PASS: unbind test completed \n");
		return (0);
	}
#else
	argc, argv;	// shut compiler warning
	return (no_fault_injection());
#endif
}

//
// unbind_test_2 tests fault point FAULTNUM_NS_UNBIND_BEFORE_DELETE_BINDING
//

int
unbind_test_2(int argc, char *argv[])
{

#if defined(_FAULT_INJECTION)
	if (argc < 2) {
		os::printf("ERROR: Wrong number of arguments \n");
		return (1);
	}

	// First argument has the node number on which you
	// expect the 'nameserver primary' to be located.

	int id = (int)os::atoi(argv[1]);
	nodeid_t nameserver_primary = (nodeid_t)id;

	//
	// Perform the unbind test (add trigger, do the bind, resolve
	// the object bound, unbind and cleanup)
	//

	if (unbind_test(nameserver_primary,
		FAULTNUM_NS_UNBIND_BEFORE_DELETE_BINDING)) {
		os::printf("FAIL: unbind test failed \n");
		return (1);
	} else {
		os::printf("PASS: unbind test completed \n");
		return (0);
	}
#else
	argc, argv;	// shut compiler warning
	return (no_fault_injection());
#endif
}

//
// unbind_test_3 tests fault point FAULTNUM_NS_UNBIND_AFTER_CKPT
//

int
unbind_test_3(int argc, char *argv[])
{

#if defined(_FAULT_INJECTION)
	if (argc < 2) {
		os::printf("ERROR: Wrong number of arguments \n");
		return (1);
	}

	// First argument has the node number on which you
	// expect the 'nameserver primary' to be located.

	int id = (int)os::atoi(argv[1]);
	nodeid_t nameserver_primary = (nodeid_t)id;

	//
	// Perform the unbind_test (add trigger, do the bind,
	// resolve the object bound, unbind and cleanup
	//

	if (unbind_test(nameserver_primary, FAULTNUM_NS_UNBIND_AFTER_CKPT)) {
		os::printf("FAIL: unbind test failed \n");
		return (1);
	} else {
		os::printf("PASS: unbind test completed \n");
		return (0);
	}
#else
	argc, argv;	// shut compiler warning
	return (no_fault_injection());
#endif
}

//
// unbind_test performs the following:
//
//	1. Switchover the current nameserver primary to the new node
//	   (specified as the first argument)
//
//	2. Creates and binds a new object to the root nameserver
//
//	3. Resolve the object from the root nameserver to verify that
//	   bind operation went thru successfully.
//
//	4. Adds the trigger for the specified fault number (2nd argument)
//	   on the nameserver primary node
//
//	6. Unbinds the object and it reboots the nameserver primary.
//
//	7. Verify unbind went thru successfully by resolving the object
//	   and it should fail.
//
//

int
unbind_test(nodeid_t new_primary, int fault_num)
{
#if defined(_FAULT_INJECTION)
	//
	// Switchover 'repl_name_server' to the node specified
	// in the first argument
	//

	if (switchover_nameserver_primary(new_primary)) {
		os::printf("FAIL: Couldn't switchover '%s' to '%d' \n",
			REPL_NS_SVC_NAME, new_primary);
		return (1);
	}

	//
	// Create a new object and bind to the root nameserver
	//

	testobj_impl	*unbind_tobj = new testobj_impl;
	typetest::Obj_ptr objref = unbind_tobj->get_objref();

	unbind_object(UNBIND_TEST_OBJ);

	if (!bind_object(objref, UNBIND_TEST_OBJ)) {
		CORBA::release(objref);
		delete unbind_tobj;
		return (1);
	}

	CORBA::release(objref);

	// Verify that bind went thru successfully by trying
	// to resolve from the root nameserver

	CORBA::Object_ptr	objp;

	objp = resolve_object(UNBIND_TEST_OBJ);

	if (CORBA::is_nil(objp)) {
		unbind_object(UNBIND_TEST_OBJ);
		delete unbind_tobj;
		return (1);
	}

	CORBA::release(objp);


	// Add the trigger on nameserver primary. Fault function
	// is to reboot

	FaultFunctions::wait_for_arg_t	ns_reboot_arg;

	ns_reboot_arg.op = FaultFunctions::WAIT_FOR_UNKNOWN;
	ns_reboot_arg.nodeid = new_primary;

	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT_ONCE,
		fault_num, &ns_reboot_arg, (uint32_t)sizeof (ns_reboot_arg),
		new_primary);

	// Unbind the object from root nameserver. This should
	// reboot nameserver primary

	if (!unbind_object(UNBIND_TEST_OBJ)) {
		os::printf("ERROR: unbind '%s' failed \n", UNBIND_TEST_OBJ);
		delete unbind_tobj;
		return (1);
	}

	// Check that unbound has gone thru successfully by trying
	// to resolve. Resolve should fail

	objp = resolve_nonexisting_object(UNBIND_TEST_OBJ);

	if (!CORBA::is_nil(objp)) {
		CORBA::release(objp);
		delete unbind_tobj;
		os::printf("FAIL: resolve succeeded when expected to fail \n");
		return (1);
	}

	//
	// Wait till _unreferenced() has been called
	//

	if (!unbind_tobj->wait_until_unreferenced()) {
		os::printf("FAIL: testobj didn't get unereferenced \n");
		delete unbind_tobj;
		return (1);
	} else {
		delete unbind_tobj;
		return (0);
	}
#else
	new_primary, fault_num;	// shut compiler warning
	return (no_fault_injection());
#endif
}
