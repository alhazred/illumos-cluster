/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ORBTEST_NAMESERVER_FI_FAULTS_ENTRIES_H
#define	_ORBTEST_NAMESERVER_FI_FAULTS_ENTRIES_H

#pragma ident	"@(#)entries.h	1.14	08/05/20 SMI"

#if !defined(_KERNEL)
#include <unistd.h>
#endif

#if defined(_KERNEL)
#include <sys/modctl.h>
#endif

#include <sys/os.h>
#include <orb/invo/common.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/fault/fault_injection.h>
#include <orb/object/adapter.h>
#include <nslib/ns.h>
#include <nslib/naming_context_impl.h>
#include <h/dc.h>
#include <h/naming.h>
#include <h/typetest_0.h>
#include <orbtest/nameserver_fi/testobj_impl.h>
#include <orbtest/repl_tests/repl_test_switch.h>

#if !defined(_KERNEL)
#if defined(_KERNEL_ORB)
#define	_UNODE
#else
#define	_USER
#endif	// _KERNEL_ORB
#endif	// !_KERNEL

#if !defined(_FAULT_INJECTION)
//
// Print a failure message when Fault Injection is not compiled in.
// Returns:
//	A non-zero value.
//

int	no_fault_injection();
#endif

#define		REPL_NS_SVC_NAME	"repl_name_server"

//
// Test entry functions.
//

int	lookup_test(int, char *[]);
int	bind_test_1(int, char *[]);
int	bind_test_2(int, char *[]);
int	bind_test_3(int, char *[]);
int	rebind_test_1(int, char *[]);
int	rebind_test_2(int, char *[]);
int	rebind_test_3(int, char *[]);
int	bind_ctx_test_1(int, char *[]);
int	bind_ctx_test_2(int, char *[]);
int	bind_ctx_test_3(int, char *[]);
int	rebind_ctx_test_1(int, char *[]);
int	rebind_ctx_test_2(int, char *[]);
int	rebind_ctx_test_3(int, char *[]);
int	unbind_test_1(int, char *[]);
int	unbind_test_2(int, char *[]);
int	unbind_test_3(int, char *[]);
int	bind_newctx_test_1(int, char *[]);
int	bind_newctx_test_2(int, char *[]);
int	bind_newctx_test_3(int, char *[]);
int	resolve_test(int, char *[]);
int	listns_test(int, char *[]);

// Support functions

int switchover_nameserver_primary(nodeid_t);
int bind_test(nodeid_t, int);
int rebind_test(nodeid_t, int);
int unbind_test(nodeid_t, int);
int bind_ctx_test(nodeid_t, int);
int bind_newctx_test(nodeid_t, int);
int rebind_ctx_test(nodeid_t, int);
CORBA::Object_ptr resolve_object(char *);
CORBA::Object_ptr resolve_nonexisting_object(char *);
bool bind_object(CORBA::Object_ptr, char *);
bool bind_ctx(naming::naming_context_ptr, char *strname);
bool rebind_ctx(naming::naming_context_ptr, char *strname);
bool rebind_object(CORBA::Object_ptr, char *);
bool unbind_object(char *);
naming::naming_context_ptr bind_newcontext(char *);

#endif	/* _ORBTEST_NAMESERVER_FI_FAULTS_ENTRIES_H */
