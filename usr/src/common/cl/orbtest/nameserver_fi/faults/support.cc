/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)support.cc	1.12	08/05/20 SMI"

#include <orbtest/nameserver_fi/faults/entries.h>

#if !defined(_FAULT_INJECTION)
//
// Print a failure message when Fault Injection is not compiled in.
// Returns:
//	A non-zero value.
//
int
no_fault_injection()
{
	os::printf("ERROR: Fault Injection not supported.\n");
	return (1);
}
#endif

//
// This function switches over nameserver primary to the
// specified node
//

int
switchover_nameserver_primary(nodeid_t new_primary)
{
	// Switchover 'repl_name_server' primary to be on
	// the specified node

	Environment 	e;
	struct opt_t 	opt;
	char 		prov_desc[10];

	os::sprintf(prov_desc, "%d", new_primary);

	opt.new_state = replica::SC_SET_PRIMARY;
	opt.prov_desc = prov_desc;
	opt.service_desc = REPL_NS_SVC_NAME;
	trigger_switchover(&opt, e);
	if (e.exception()) {
		e.exception()->print_exception("trigger_switchover");
		return (1);
	}
	return (0);
}

//
// Get an object reference with the given name from the name server.
// Returns:
//	The object reference if successful, _nil() otherwise.
//

CORBA::Object_ptr
resolve_object(char *strname)
{
	const uint32_t		retries = 40;
	CORBA::Object_ptr	objp;
	Environment		e;
	CORBA::Exception	*ex;
	uint32_t		i;

	// Get the root name server.

	naming::naming_context_var context = ns::root_nameserver();

	// Find implementation object from name server.  Try several times
	// if it's not in the name server yet.

	for (i = 0; i < retries; ++i) {
		naming::not_found	*notfound;

		e.clear();		// in case previous loop had exception
		objp = context->resolve(strname, e);

		ex = e.exception();
		if (ex == NULL) {
			return (objp);				// success
		}

		notfound = naming::not_found::_exnarrow(ex);
		if (notfound != NULL) {
			// It's not in the name server yet; retry after 1 secs.
#if defined(_KERNEL)
			os::usecsleep(1 * 1000000);
#else
			sleep(1);
#endif
		} else {
			break;
		}
	}

	ex->print_exception("ERROR:");
	return (CORBA::Object::_nil());
}

//
// Bind object with the name server with the given name.
// Returns:
//	True if successful, false otherwise.
//

bool
bind_object(CORBA::Object_ptr objref, char *strname)
{
	Environment		e;
	CORBA::Exception	*ex;

	// Get the root name server.
	naming::naming_context_var context = ns::root_nameserver();

	// Register implementation object with the name server.

	context->bind(strname, objref, e);

	if (ex = e.exception()) {
		ex->print_exception("ERROR:");
		os::printf("ERROR: can't bind \"%s\"\n", strname);
		return (false);
	}

	return (true);					// success
}


//
// Unregister object from the name server.
// Returns:
//	True if successful, false otherwise.
//

bool
unbind_object(char *strname)
{
	Environment		e;
	CORBA::Exception	*ex;

	// Get the root name server.

	naming::naming_context_var context = ns::root_nameserver();

	// Unbind implementation object from name server.

	context->unbind(strname, e);

	if (ex = e.exception()) {
		ex->print_exception("unbind:");
		return (false);
	}

	return (true);					// success
}

//
// Rebind object to the name server.
// Returns:
//	True if successful, false otherwise.
//

bool
rebind_object(CORBA::Object_ptr objref, char *strname)
{
	Environment		e;
	CORBA::Exception	*ex;

	// Get the root name server.
	naming::naming_context_var context = ns::root_nameserver();

	// Register implementation object with the name server.

	context->rebind(strname, objref, e);

	if (ex = e.exception()) {
		ex->print_exception("ERROR:");
		os::printf("ERROR: can't rebind \"%s\"\n", strname);
		return (false);
	}

	return (true);					// success
}

//
// Bind a context to the name server.
// Returns:
//	True if successful, false otherwise.
//

bool
bind_ctx(naming::naming_context_ptr nc_obj, char *strname)
{
	Environment		e;
	CORBA::Exception	*ex;

	// Get the root name server.
	naming::naming_context_var context = ns::root_nameserver();

	// Register implementation object with the name server.

	context->bind_context(strname, nc_obj, e);

	if (ex = e.exception()) {
		ex->print_exception("ERROR:");
		os::printf("ERROR: can't bind \"%s\"\n", strname);
		return (false);
	}

	return (true);					// success
}

//
// Rebind a context to the name server.
// Returns:
//	True if successful, false otherwise.
//

bool
rebind_ctx(naming::naming_context_ptr nc_obj, char *strname)
{
	Environment		e;
	CORBA::Exception	*ex;

	// Get the root name server.
	naming::naming_context_var context = ns::root_nameserver();

	// Register implementation object with the name server.

	context->rebind_context(strname, nc_obj, e);

	if (ex = e.exception()) {
		ex->print_exception("ERROR:");
		os::printf("ERROR: can't rebind context \"%s\"\n", strname);
		return (false);
	}

	return (true);					// success
}

//
// resolve a nonexisting object  from the nameserver. This is called
// from unbind_object test. After unbind the object, resolve operation
// should rail.
//

CORBA::Object_ptr
resolve_nonexisting_object(char *strname)
{
	const uint32_t		retries = 40;
	CORBA::Object_ptr	objp;
	Environment		e;
	CORBA::Exception	*ex;
	uint32_t		i;

	// Get the root name server.

	naming::naming_context_var context = ns::root_nameserver();

	// Find implementation object from name server.  Try several times
	// if it's not in the name server yet.

	for (i = 0; i < retries; ++i) {
		naming::not_found	*notfound;

		e.clear();		// in case previous loop had exception
		objp = context->resolve(strname, e);

		ex = e.exception();
		if (ex == NULL) {
			return (objp);				// success
		}

		notfound = naming::not_found::_exnarrow(ex);
		if (notfound != NULL) {
			// It's not in the name server yet; retry after 1 secs.
#if defined(_KERNEL)
			os::usecsleep(1 * 1000000);
#else
			sleep(1);
#endif
		} else {
			break;
		}
	}

	return (CORBA::Object::_nil());
}

//
// Bind a context to the name server.
// Returns:
//	True if successful, false otherwise.
//

naming::naming_context_ptr
bind_newcontext(char *strname)
{
	Environment		e;
	CORBA::Exception	*ex;
	naming::naming_context_ptr	objp;

	// Get the root name server.
	naming::naming_context_var context = ns::root_nameserver();

	// Register implementation object with the name server.

	objp = context->bind_new_context(strname, e);

	if (ex = e.exception()) {
		ex->print_exception("ERROR:");
		os::printf("ERROR: can't bind new context \"%s\"\n", strname);
		return (naming::naming_context::_nil());
	}

	return (objp);					// success
}
