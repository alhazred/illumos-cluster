/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rebind.cc	1.12	08/05/20 SMI"

#include <orbtest/nameserver_fi/faults/entries.h>

#define	REBIND_TEST_OBJ	"rebind_test_obj"

//
// Assertion:
//
//	When a client rebinds an object with the root nameserver, and
//	nameserver primary dies during rebind operation, rebind operation
//	from the client should be successful. Verify by resolving
//	the object bound from the new nameserver primary.
//
// Test Strategy:
//
//	1. Using switchover, set up the nameserver primary to be a remote
//	   node (node other than the client node doing the rebind operation,
//	   because nameserver primary will be rebooted as part of rebind
//	   operation). Currently node number for nameserver primary is
//	   a command line argument (in fault.data file).
//
//	2. On the nameserver primary node, trigger add the fault point in
//	   the code path of rebind operation.
//
//	3. From the client, rebind a new object to the root nameserver and
//	   this should boot the nameserver primary (because of the trigger
//	   that was added).
//
//	4. Verify that rebind operation went thru successfully by
//	   resolving the object from the root nameserver.
//
//	5. Cleanup (unbind the object, Wait until _unreferenced() gets
//	   called...etc)
//
//


//
// rebind_test_1 tests the fault point FAULTNUM_NS_REBIND
//

int
rebind_test_1(int argc, char *argv[])
{

#if defined(_FAULT_INJECTION)

	if (argc < 2) {
		os::printf("ERROR: Wrong number of arguments \n");
		return (1);
	}

	// First argument has the node number on which you
	// expect the 'nameserver primary' to be located.

	int id = (int)os::atoi(argv[1]);
	nodeid_t nameserver_primary = (nodeid_t)id;

	//
	// Perform the rebind test (add trigger, do the rebind, resolve
	// the object bound, unbind and cleanup
	//

	if (rebind_test(nameserver_primary, FAULTNUM_NS_REBIND)) {
		os::printf("FAIL: rebind test failed \n");
		return (1);
	} else {
		os::printf("PASS: rebind test completed \n");
		return (0);
	}
#else
	argc, argv;	// shut compiler warning
	return (no_fault_injection());
#endif
}

//
// rebind_test_2 tests the fault point FAULTNUM_NS_REBIND_BEFORE_STORE_BINDING
//

int
rebind_test_2(int argc, char *argv[])
{

#if defined(_FAULT_INJECTION)

	if (argc < 2) {
		os::printf("ERROR: Wrong number of arguments \n");
		return (1);
	}

	// First argument has the node number on which you
	// expect the 'nameserver primary' to be located.

	int id = (int)os::atoi(argv[1]);
	nodeid_t nameserver_primary = (nodeid_t)id;


	//
	// Perform the rebind test (add trigger, do the rebind, resolve
	// the object bound, unbind and cleanup
	//

	if (rebind_test(nameserver_primary,
		FAULTNUM_NS_REBIND_BEFORE_STORE_BINDING)) {
		os::printf("FAIL: rebind test failed \n");
		return (1);
	} else {
		os::printf("PASS: rebind test completed \n");
		return (0);
	}
#else
	argc, argv;	// shut compiler warning
	return (no_fault_injection());
#endif
}

//
// rebind_test_3 tests the fault point FAULTNUM_NS_REBIND_AFTER_CKPT
//

int
rebind_test_3(int argc, char *argv[])
{

#if defined(_FAULT_INJECTION)

	if (argc < 2) {
		os::printf("ERROR: Wrong number of arguments \n");
		return (1);
	}

	// First argument has the node number on which you
	// expect the 'nameserver primary' to be located.

	int id = (int)os::atoi(argv[1]);
	nodeid_t nameserver_primary = (nodeid_t)id;


	//
	// Perform the rebind test (add trigger, do the rebind, resolve
	// the object bound, unbind and cleanup
	//

	if (rebind_test(nameserver_primary, FAULTNUM_NS_REBIND_AFTER_CKPT)) {
		os::printf("FAIL: rebind test failed \n");
		return (1);
	} else {
		os::printf("PASS: rebind test completed \n");
		return (0);
	}
#else
	argc, argv;	// shut compiler warning
	return (no_fault_injection());
#endif
}


//
// rebind_test performs the following:
//
//	1. Switchover the current nameserver primary to the new node
//	   (specified as the first argument)
//
//	2. Adds the trigger for the specified fault number (2nd argument)
//	   on the nameserver primary node
//
//	3. Creates and rebinds a new object to the root nameserver
//	   (nameserver primary reboots because of this)
//
//	4. Resolve the object from the root nameserver to verify that
//	   bind operation went thru successfully.
//
//	5. Unbind and cleanup
//
//

int
rebind_test(nodeid_t new_primary, int fault_num)
{
#if defined(_FAULT_INJECTION)
	//
	// Switchover 'repl_name_server' to the node specified
	// in the first argument
	//

	if (switchover_nameserver_primary(new_primary)) {
		os::printf("FAIL: Couldn't switchover '%s' to '%d' \n",
			REPL_NS_SVC_NAME, new_primary);
		return (1);
	}

	//
	// Create a new object and rebind to the root nameserver
	// This should be successful.
	//

	testobj_impl *rebind_tobj = new testobj_impl;
	typetest::Obj_ptr objref = rebind_tobj->get_objref();

	if (!rebind_object(objref, REBIND_TEST_OBJ)) {
		CORBA::release(objref);
		delete rebind_tobj;
		return (1);
	}

	// Add the trigger on nameserver primary. Fault function
	// is to reboot

	FaultFunctions::wait_for_arg_t	ns_reboot_arg;

	ns_reboot_arg.op = FaultFunctions::WAIT_FOR_UNKNOWN;
	ns_reboot_arg.nodeid = new_primary;

	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT_ONCE,
		fault_num, &ns_reboot_arg, (uint32_t)sizeof (ns_reboot_arg),
		new_primary);

	//
	// Rebind the same object again with the same name.
	// This should reboot the nameserver primary. But
	// rebind should not fail.
	//

	if (!rebind_object(objref, REBIND_TEST_OBJ)) {
		delete rebind_tobj;
		return (1);
	}

	CORBA::release(objref);

	// Verify that rebind went thru successfully by trying
	// to resolve the object from the root nameserver

	CORBA::Object_ptr	objp;

	objp = resolve_object(REBIND_TEST_OBJ);

	if (CORBA::is_nil(objp)) {
		unbind_object(REBIND_TEST_OBJ);
		delete rebind_tobj;
		return (1);
	}

	CORBA::release(objp);

	// Unbind the object from root nameserver

	if (!unbind_object(REBIND_TEST_OBJ)) {
		os::printf("ERROR: unbind '%s' failed \n", REBIND_TEST_OBJ);
		delete rebind_tobj;
		return (1);
	}

	//
	// Wait till _unreferenced() has been called
	//

	if (!rebind_tobj->wait_until_unreferenced()) {
		os::printf("FAIL: testobj didn't get unereferenced \n");
		delete rebind_tobj;
		return (1);
	} else {
		delete rebind_tobj;
		return (0);
	}
#else
	new_primary, fault_num;	// shut compiler warning
	return (no_fault_injection());
#endif
}
