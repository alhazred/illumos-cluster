/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)bind_newctx.cc	1.15	08/05/20 SMI"

#include <orbtest/nameserver_fi/faults/entries.h>

#define	BIND_NEWCTX_OBJ	"bind_newctx_obj"

//
// Assertion:
//
//	When a client binds a context with the root nameserver, and
//	nameserver primary dies during this operation, bind_new_context
//	operation from the client should be successful. Verify by resolving
//	the context from the new nameserver primary.
//
// Test Strategy:
//
//	1. Using switchover, set up the nameserver primary to be a remote
//	   node (node other than the client node doing the bind_newcontext
//	   operation, because nameserver primary will be rebooted as part
//	   of bind_newcontext operation). Currently node number for nameserver
//	   primary is a command line argument (in fault.data file).
//
//	2. On the nameserver primary node, trigger add the fault point
//	   in the code path of bind_new_context operation.
//
//	3. From the client, bind a context to the root nameserver and
//	   this should boot the nameserver primary (because of the trigger
//	   that was added).
//
//	4. Verify that bind_new_context went thru successfully by
//	   resolving the context from the root nameserver.
//
//	5. Cleanup (unbind the context ...etc)
//
//


//
// bind_newctx_test_1 tests the fault point FAULTNUM_NS_BIND_NEWCTX
//

int
bind_newctx_test_1(int argc, char *argv[])
{

#if defined(_FAULT_INJECTION)

	if (argc < 2) {
		os::printf("ERROR: Wrong number of arguments \n");
		return (1);
	}

	// First argument has the node number on which you
	// expect the 'nameserver primary' to be located.

	int id = (int)os::atoi(argv[1]);
	nodeid_t nameserver_primary = (nodeid_t)id;

	//
	// Perform the bind_newctx test (add trigger, do the bind, resolve
	// the object bound, unbind and cleanup
	//

	if (bind_newctx_test(nameserver_primary, FAULTNUM_NS_BIND_NEWCTX)) {
		os::printf("FAIL: bind_newcontext test failed \n");
		return (1);
	} else {
		os::printf("PASS: bind_newcontext test completed \n");
		return (0);
	}
#else
	argc, argv;	// shut compiler warning
	return (no_fault_injection());
#endif
}

//
// bind_newctx_test_2 tests the fault point
// FAULTNUM_NS_BIND_NEWCTX_BEFORE_BIND_NEWCTX
//

int
bind_newctx_test_2(int argc, char *argv[])
{

#if defined(_FAULT_INJECTION)

	if (argc < 2) {
		os::printf("ERROR: Wrong number of arguments \n");
		return (1);
	}

	// First argument has the node number on which you
	// expect the 'nameserver primary' to be located.

	int id = (int)os::atoi(argv[1]);
	nodeid_t nameserver_primary = (nodeid_t)id;

	//
	// Perform the bind_newctx_test (add trigger, do the bind, resolve
	// the object bound, unbind and cleanup
	//
	if (bind_newctx_test(nameserver_primary,
		FAULTNUM_NS_BIND_NEWCTX_BEFORE_BIND_NEWCTX)) {
		os::printf("FAIL: bind_newcontext test failed \n");
		return (1);
	} else {
		os::printf("PASS: bind_newcontext test completed \n");
		return (0);
	}
#else
	argc, argv;	// shut compiler warning
	return (no_fault_injection());
#endif
}

//
// bind_newctx_test_3 tests the fault point
// FAULTNUM_NS_BIND_NEWCTX_AFTER_CKPT
//

int
bind_newctx_test_3(int argc, char *argv[])
{

#if defined(_FAULT_INJECTION)
	if (argc < 2) {
		os::printf("ERROR: Wrong number of arguments \n");
		return (1);
	}

	// First argument has the node number on which you
	// expect the 'nameserver primary' to be located.

	int id = (int)os::atoi(argv[1]);
	nodeid_t nameserver_primary = (nodeid_t)id;

	//
	// Perform the bind_newctx_test (add trigger, do the bind, resolve
	// the object bound, unbind and cleanup
	//

	if (bind_newctx_test(nameserver_primary,
		FAULTNUM_NS_BIND_NEWCTX_AFTER_CKPT)) {
		os::printf("FAIL: bind_newcontext test failed \n");
		return (1);
	} else {
		os::printf("PASS: bind_newcontext test completed \n");
		return (0);
	}
#else
	argc, argv;	// shut compiler warning
	return (no_fault_injection());
#endif
}

//
// bind_newctx_test performs the following:
//
//	1. Switchover the current nameserver primary to the new node
//	   (specified as the first argument)
//
//	2. Adds the trigger for the specified fault number (2nd argument)
//	   on the nameserver primary node
//
//	3. Binds a new context to the root nameserver
//	   (nameserver primary reboots because of this)
//
//	4. Resolve the context from the root nameserver to verify that
//	   bind operation went thru successfully.
//
//	5. Unbind and cleanup
//


int
bind_newctx_test(nodeid_t new_primary, int fault_num)
{
#if defined(_FAULT_INJECTION)
	//
	// Switchover 'repl_name_server' to the node specified
	// in the first argument
	//

	if (switchover_nameserver_primary(new_primary)) {
		os::printf("FAIL: Couldn't switchover '%s' to '%d' \n",
			REPL_NS_SVC_NAME, new_primary);
		return (1);
	}

	// Add the trigger on nameserver primary. Fault function
	// is to reboot

	FaultFunctions::wait_for_arg_t	ns_reboot_arg;

	ns_reboot_arg.op = FaultFunctions::WAIT_FOR_UNKNOWN;
	ns_reboot_arg.nodeid = new_primary;

	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT_ONCE,
		fault_num, &ns_reboot_arg, (uint32_t)sizeof (ns_reboot_arg),
		new_primary);

	// Create a new context in the root nameserver

	// unbind the object just to make sure there is no
	// object with the same name

	unbind_object(BIND_NEWCTX_OBJ);

	naming::naming_context_ptr  nc_obj = bind_newcontext(BIND_NEWCTX_OBJ);

	if (CORBA::is_nil(nc_obj)) {
		return (1);
	}

os::printf("Done with bind_newcontext\n");

	// Verify that bind_newcontext went thru successfully by trying
	// to resolve from the root nameserver

	CORBA::Object_ptr	objp;

	objp = resolve_object(BIND_NEWCTX_OBJ);

os::printf("Done with resolve\n");

	if (CORBA::is_nil(objp)) {
		unbind_object(BIND_NEWCTX_OBJ);
		return (1);
	}

	CORBA::release(objp);

	// Unbind the object from root nameserver

	if (!unbind_object(BIND_NEWCTX_OBJ)) {
		os::printf("ERROR: unbind '%s' failed \n", BIND_NEWCTX_OBJ);
		return (1);
	}

os::printf("Done with unbind\n");

	return (0);

	// There is no wait_until_referenced for naming_context_impl.
	// So cannot wait until _unreferenced has been called
	//
#else
	new_primary, fault_num;		// shut compiler warning
	return (no_fault_injection());
#endif
}
