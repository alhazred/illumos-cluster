/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)lookup.cc	1.12	08/05/20 SMI"

#include <orbtest/nameserver_fi/faults/entries.h>

#define	LOOKUP_TEST_OBJ	"lookup_test_obj"

//
// Assertion:
//
//	When a client resolves an object from the root nameserver, and
//	nameserver primary dies during lookup (part of resolve), resolve
//	operation from the client should be successful.
//
// Test Strategy:
//
//	1. Using switchover, set up the nameserver primary to be a remote
//	   node (node other than the client node doing the lookup operation,
//	   because nameserver primary will be rebooted as part of lookup
//	   operation). Currently node number for nameserver primary is
//	   a command line argument (in fault.data file).
//
//	2. From a client, bind a new object to the root nameserver
//
//	3. On the nameserver primary node, trigger add the fault point in
//	   the code path of lookup operation.
//
//	4. From the client, resolve the object from the root nameserver.
//	   This should reboot the nameserver primary because of the fault
//	   point.  Resolve should be successful.
//
//	5. Cleanup (unbind the object, Wait until _unreferenced() gets
//	   called...etc)
//
//

int
lookup_test(int argc, char *argv[])
{

#if defined(_FAULT_INJECTION)
	if (argc < 2) {
		os::printf("ERROR: Wrong number of arguments \n");
		return (1);
	}

	// First argument has the node number on which you
	// expect the 'nameserver primary' to be located.

	int id = (int)os::atoi(argv[1]);

	nodeid_t new_primary = (nodeid_t)id;

	//
	// Switchover 'repl_name_server' to the node specified
	// in the first argument
	//

	if (switchover_nameserver_primary(new_primary)) {
		os::printf("FAIL: Couldn't switchover '%s' to '%d' \n",
			REPL_NS_SVC_NAME, new_primary);
		return (1);
	}

	//
	// Create a new object and bind to the root nameserver
	//


	testobj_impl *lookup_tobj = new testobj_impl;

	typetest::Obj_ptr objref = lookup_tobj->get_objref();

	if (!bind_object(objref, LOOKUP_TEST_OBJ)) {
		CORBA::release(objref);
		delete lookup_tobj;
		return (1);
	}

	CORBA::release(objref);

	// Add the trigger on nameserver primary. Fault function
	// is to reboot

	FaultFunctions::wait_for_arg_t	ns_reboot_arg;

	ns_reboot_arg.op = FaultFunctions::WAIT_FOR_UNKNOWN;
	ns_reboot_arg.nodeid = new_primary;

	FaultFunctions::node_trigger_add(FaultFunctions::REBOOT_ONCE,
	    FAULTNUM_LOOKUP, &ns_reboot_arg, (uint32_t)sizeof (ns_reboot_arg),
	    new_primary);

	// Resolve the object from the root nameserver. This should
	// reboot the nameserver primary.

	CORBA::Object_ptr	objp;

	objp = resolve_object(LOOKUP_TEST_OBJ);

	if (CORBA::is_nil(objp)) {
		unbind_object(LOOKUP_TEST_OBJ);
		delete lookup_tobj;
		return (1);
	}

	CORBA::release(objp);

	// Unbind the object from root nameserver

	if (!unbind_object(LOOKUP_TEST_OBJ)) {
		os::printf("ERROR: unbind '%s' failed \n", LOOKUP_TEST_OBJ);
		delete lookup_tobj;
		return (1);
	}

	//
	// Wait till _unreferenced() has been called
	//

	if (!lookup_tobj->wait_until_unreferenced()) {
		os::printf("FAIL: testobj didn't get unereferenced \n");
		os::printf("FAIL: lookup test failed \n");
		delete lookup_tobj;
		return (1);
	} else {
		os::printf("PASS: lookup test passed \n");
		delete lookup_tobj;
		return (0);
	}
#else
	argc, argv;	// shut compiler warning
	return (no_fault_injection());
#endif
}
