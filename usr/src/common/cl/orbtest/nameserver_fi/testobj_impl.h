/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ORBTEST_NAMESERVER_FI_TESTOBJ_IMPL_H
#define	_ORBTEST_NAMESERVER_FI_TESTOBJ_IMPL_H

#pragma ident	"@(#)testobj_impl.h	1.13	08/05/20 SMI"

#if defined(_KERNEL)
#include <sys/modctl.h>
#endif

#include <sys/os.h>
#include <orb/invo/common.h>
#include <orb/object/adapter.h>
#include <h/typetest_0.h>

#if !defined(_KERNEL)
#if defined(_KERNEL_ORB)
#define	_UNODE
#else
#define	_USER
#endif	// _KERNEL_ORB
#endif	// !_KERNEL

//
// Object implementation used for Nameserver FI tests
//
/* CSTYLED */
class testobj_impl : public McServerof<typetest::Obj> {

public:

	testobj_impl();
	~testobj_impl();

	void	_unreferenced(unref_t);
	void	set_id(int32_t l) { idnum = l; }

	// Interface operations.

	int32_t	id(Environment &) { return (idnum); }
	//
	// sleep_period needs to be specified in seconds.
	// This interface is need by /usr/cluster/orbtest/types/test_fork
	// code. Since the Rolling Upgrade project all idl methods
	// need to be implmented so a  empty method is being defined.
	//
	void    call_sleep(uint16_t sleep_period, Environment &)
	{
		os::usecsleep(sleep_period);
	}


	// Suspend calling thread until _unreferenced() is called.
	// Returns: true if _unreferenced() has been called.
	//	    false if timeout arg (in seconds) is nonzero and it
	//	    expires before _unreferenced() is called.

	bool	wait_until_unreferenced(uint32_t timeout = 0);

	// Has _unreferenced() been called?

	bool	is_unreferenced();


	// Mark that _unreferenced() has been called.

	void	unref_called();

private:
	int32_t		idnum;
	bool		_unref_called;
	os::mutex_t	_mutex;
	os::condvar_t	_cv;
};

#endif	/* _ORBTEST_NAMESERVER_FI_TESTOBJ_IMPL_H */
