/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)testobj_impl.cc	1.11	08/05/20 SMI"

#if !defined(_KERNEL)
#include <unistd.h>
#endif

#include <sys/os.h>
#include <nslib/ns.h>
#include <orbtest/nameserver_fi/testobj_impl.h>

testobj_impl::testobj_impl()
{
	_unref_called = false;
	idnum = 0;
}

testobj_impl::~testobj_impl()
{
	// Make sure _unreferenced() has been called.

	if (! is_unreferenced()) {
		os::printf("ERROR: testobj_impl: destructor called before "
			"_unreferenced()\n");
	}
}

//
// Suspend calling thread until _unreferenced() is called.
// Returns: true if _unreferenced() has been called.
//	    false if timeout arg (in seconds) is nonzero and it
//	    expires before _unreferenced() is called.
//

bool
testobj_impl::wait_until_unreferenced(uint32_t timeout /* = 0 */)
{
	bool	retval = true;

	_mutex.lock();
	if (timeout == 0) {
		while (! is_unreferenced()) {
			_cv.wait(&_mutex);
		}
	} else {
		os::systime	systime;

		// setreltime() requires timeout in usecs.
		systime.setreltime((long)timeout * 1000000);
		while (! is_unreferenced()) {
			// If timeout expires...
			if (_cv.timedwait(&_mutex, &systime) ==
			    os::condvar_t::TIMEDOUT) {
				retval = false;
				break;
			}
		}
	}
	_mutex.unlock();
	return (retval);
}

//
// Has _unreferenced() been called?
//
bool
testobj_impl::is_unreferenced()
{
	return (_unref_called);
}

//
// Mark that _unreferenced() has been called.
//
void
testobj_impl::unref_called()
{
	_mutex.lock();		// so wait_until_unreferenced() can wait
	_unref_called = true;
	_cv.signal();		// signal wait_until_unreferenced()
	_mutex.unlock();
}

void
#ifdef DEBUG
testobj_impl::_unreferenced(unref_t cookie)
#else
testobj_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	unref_called();
}
