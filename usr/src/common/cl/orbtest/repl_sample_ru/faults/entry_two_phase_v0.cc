/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)entry_two_phase_v0.cc	1.6	08/05/20 SMI"

#include	<sys/os.h>
#include	<h/mc_sema.h>
#include	<h/repl_sample_v0.h>
#include	<sys/mc_sema_impl.h>
#include	<orb/invo/common.h>
#include	<orb/fault/fault_functions.h>
#include	<orbtest/repl_sample_ru/repl_sample_clnt_v0.h>
#include	<orbtest/rm_probe/rm_snap.h>
#include	<orbtest/repl_sample_ru/faults/entry_two_phase_aux_v0.h>

#include	<orbtest/repl_sample_ru/faults/fault_two_phase_2_b.h>
#include	<orbtest/repl_sample_ru/faults/fault_two_phase_2_g.h>
#include	<orbtest/repl_sample_ru/faults/fault_two_phase_2_h.h>
#include	<orbtest/repl_sample_ru/faults/fault_two_phase_2_m.h>
#include	<orbtest/repl_sample_ru/faults/fault_two_phase_2_n.h>
#include	<orbtest/repl_sample_ru/faults/fault_two_phase_2_o.h>

#include	<orbtest/repl_sample_ru/faults/fault_one_phase_2_d.h>
#include	<orbtest/repl_sample_ru/faults/fault_one_phase_2_e.h>
#include	<orbtest/repl_sample_ru/faults/fault_one_phase_2_g.h>

//
// Two-Phase Test Case 1-A
//
int
two_phase_1_a(int, char *[])
{
	// We're just checking that a two phase invocation can succeed
	// without faults.
	os::printf("*** Two-Phase Mini-Transaction Scenario 1 Fault A\n");

	Environment	e;
	return (two_phase_mini_1(NULL, NULL, NULL, 0, e, "ha_sample_server",
		false));
}


//
// Two-Phase Test Case 1-Q
//
int
two_phase_1_q(int, char *[])
{
	os::printf("*** Two-Phase Mini-Transaction Scenario 1 Fault Q\n");

#ifdef _FAULT_INJECTION
	// We want to make sure that a invocation who is delayed in returning
	// to client will complete succesfully when the commit is sent to
	// the secondaries.

	retval_t	*retval	= NULL;
	int		r;
	Environment	e;
	mc_sema::sema_var	mc_sema1_ref;
	mc_sema::sema_var	mc_sema2_ref;

	mc_sema1_ref = (new mc_sema_impl)->get_objref();
	mc_sema2_ref = (new mc_sema_impl)->get_objref();

	ASSERT(!CORBA::is_nil(mc_sema1_ref));
	ASSERT(!CORBA::is_nil(mc_sema2_ref));

	// Allocate the retval object
	retval = new retval_t;
	ASSERT(retval != NULL);

	// Initialize both mc_sema objects
	mc_sema1_ref->init(0, e);
	if (e.exception()) {
		e.exception()->print_exception("mc_sema::init()");
		delete retval;
		return (1);
	}

	mc_sema2_ref->init(0, e);
	if (e.exception()) {
		e.exception()->print_exception("mc_sema::init()");
		delete retval;
		return (1);
	}

	// Reset the exitcount
	retval->exitcount = 0;

	retval->id1 = mc_sema1_ref->get_cookie(e);
	if (e.exception()) {
		e.exception()->print_exception("mc_sema::init()");
		delete retval;
		return (1);
	}

	retval->id2 = mc_sema2_ref->get_cookie(e);
	if (e.exception()) {
		e.exception()->print_exception("mc_sema::init()");
		delete retval;
		return (1);
	}

	ASSERT(retval->id1 != 0);
	ASSERT(retval->id2 != 0);

#if defined(_KERNEL)
	if (thread_create(NULL, (size_t)0,
	    (void(*)(void))two_phase_aux_hang_on_return,
	    (caddr_t)retval, (size_t)0, &p0, TS_RUN, 60) == NULL) {
		os::printf("+++ FAIL: thread_create failed\n");
		delete retval;
		return (1);
	}
#else
	if (thr_create(NULL, (size_t)0, two_phase_aux_hang_on_return,
	    retval, THR_BOUND, NULL)) {
		os::printf("+++ FAIL: thread_create failed\n");
		delete retval;
		return (1);
	}
#endif

	// Do a sema_p on mc_sema1 that will cause us to block until the other
	// invocation thread does sema_v after add_commit()
	mc_sema1_ref->sema_p(e);
	if (e.exception()) {
		e.exception()->print_exception("mc_sema::sema_p()");
		delete retval;
		return (1);
	}

	// Now this thread will run through a small one-phase mini-transaction
	// that will checkpoint therefore carrying with it the commit()
	os::printf("    Checkpoint invocation starting...\n");

	r = one_phase_mini_1(NULL, NULL, NULL, 0, e);

	// XXX - When we exit.. we should probably cancel the other thread
	if (r != 0) {
		delete retval;
		return (r);
	}

	if (e.exception()) {
		e.exception()->print_exception("one_phase_mini_1()");
		delete retval;
		return (1);
	}

	// At this point the commit has been flushed all that is needed now
	// is to do a sema_v on mc_sema2, which will allow the other thread to
	// continue
	mc_sema2_ref->sema_v(e);
	if (e.exception()) {
		e.exception()->print_exception("one_phase_mini_1()");
		delete retval;
		return (1);
	}

	// Wait for the other thread to finish before we stop this test
	retval->lck.lock();
	while (retval->exitcount != 1) {
		retval->cv.wait(&(retval->lck));
	}

	// Retrieve the return value
	r = retval->value;
	retval->lck.unlock();

	// Destroy the mc_sema objects
	mc_sema1_ref->destroy(e);
	if (e.exception()) {
		e.exception()->print_exception("mc_sema::destroy()");
	}

	mc_sema2_ref->destroy(e);
	if (e.exception()) {
		e.exception()->print_exception("mc_sema::destroy()");
	}

	delete retval;
	return (r);
#else
	return (no_fault_injection());
#endif	// _FAULT_INJECTION
}


//
// Two-Phase Test Case 2-A
//
int
two_phase_2_a(int, char *[])
{
	// We're just checking that a two phase invocation can succeed
	// without faults.
	os::printf("*** Two-Phase Mini-Transaction Scenario 2 Fault A\n");

	Environment	e;
	return (two_phase_mini_1(NULL, NULL, NULL, 0, e));
}

//
//
// Two-Phase Test Case 2-B
//
int
two_phase_2_b(int, char *[])
{
	os::printf("*** Two-Phase Mini-Transaction Scenario 2 Fault B\n");

#ifdef _FAULT_INJECTION
	// Use fault inside of service (prior to 1st checkpoint)

	// Figure out which node we'd like to fault (Primary node)
	rm_snap		_rma_snapshot;
	nodeid_t	primary_nodeid;
	Environment	e;

	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_primary(primary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get primary nodeid\n");
		return (1);
	}

	os::printf("    Primary is node: %d\n", primary_nodeid);

	return (two_phase_mini_1(
		fault_two_phase_2_b_on,
		fault_two_phase_2_b_off,
		&primary_nodeid,
		(int)sizeof (primary_nodeid),
		e,
		"ha_sample_server",
		false));
#else
	return (no_fault_injection());
#endif
}


//
//
// Two-Phase Test Case 2-C
//
int
two_phase_2_c(int, char *[])
{
	os::printf("*** Two-Phase Mini-Transaction Scenario 2 Fault C\n");

#ifdef _FAULT_INJECTION
	// Reuse the fault points for One-Phase 2-D
	// These faults are in the check point mechanism before the
	// checkpoint is delivered the secondary is killed

	// Figure out which node we'd like to fault (Secondary node)
	rm_snap		_rma_snapshot;
	nodeid_t	secondary_nodeid;
	int		rslt;
	Environment	e;

	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_secondary(1, secondary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get secondary nodeid\n");
		return (1);
	}

	os::printf("    Secondary is node: %d\n", secondary_nodeid);

	rslt = two_phase_mini_1(
		fault_one_phase_2_d_on,
		fault_one_phase_2_d_off,
		&secondary_nodeid,
		(int)sizeof (secondary_nodeid),
		e,
		"ha_sample_server",
		false);

	// Wait for CMM
	os::printf("    Waiting 45 secs for CMM reconfig\n");
	os::usecsleep((long)45 * 1000000);

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// Two-Phase Test Case 2-D
//
int
two_phase_2_d(int, char *[])
{
	os::printf("*** Two-Phase Mini-Transaction Scenario 2 Fault D\n");

#ifdef _FAULT_INJECTION
	// Reuse the fault points for One-Phase 2-E
	// These faults are in the check point mechanism before the
	// checkpoint is completed, the secondary dies

	// Figure out which node we'd like to fault (Secondary node)
	rm_snap		_rma_snapshot;
	nodeid_t	secondary_nodeid;
	int		rslt;
	Environment	e;

	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_secondary(1, secondary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get secondary nodeid\n");
		return (1);
	}

	os::printf("    Secondary is node: %d\n", secondary_nodeid);

	rslt = two_phase_mini_1(
		fault_one_phase_2_e_on,
		fault_one_phase_2_e_off,
		&secondary_nodeid,
		(int)sizeof (secondary_nodeid),
		e,
		"ha_sample_server",
		false);

	// Sleep for 45 seconds to allow the cmm to reconfigure
	os::printf("   Sleeping for 45 seoncds to allow CMM to reconfigure\n");
	os::usecsleep((long)45 * 1000000);

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// Two-Phase Test Case 2-E
//
int
two_phase_2_e(int, char *[])
{
	os::printf("*** Two-Phase Mini-Transaction Scenario 2 Fault E\n");

#ifdef _FAULT_INJECTION
	// Reuse the fault points for One-Phase 2-E (with nodeid set to 0
	// These faults are in the check point mechanism before the
	// checkpoint is completed, the secondary dies (all of them)

	Environment	e;
	nodeid_t	nodeid = 0;
	int		rslt;

	rslt = two_phase_mini_1(
		fault_one_phase_2_e_on,
		fault_one_phase_2_e_off,
		&nodeid,
		(int)sizeof (nodeid),
		e,
		"ha_sample_server",
		false);

	// Now wait for both secondaries to have gone dead
	os::printf("    Sleeping for 45 seconds to reconfigure cmm\n");
	os::usecsleep((long)45 * 1000000);

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// Two-Phase Test Case 2-F
//
int
two_phase_2_f(int, char *[])
{
	os::printf("*** Two-Phase Mini-Transaction Scenario 2 Fault F\n");

#ifdef _FAULT_INJECTION
	// Reuse the fault points for One-Phase 2-G
	// This fault are on the primary after the checkpoint returns
	// we kill the secondary.

	// Figure out which node we'd like to fault (Secondary node)
	rm_snap		_rma_snapshot;
	nodeid_t	secondary_nodeid;
	Environment	e;
	int		rslt;

	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_secondary(1, secondary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get secondary nodeid\n");
		return (1);
	}

	os::printf("    Secondary is node: %d\n", secondary_nodeid);

	rslt = two_phase_mini_1(
		fault_one_phase_2_g_on,
		fault_one_phase_2_g_off,
		&secondary_nodeid,
		(int)sizeof (secondary_nodeid),
		e,
		"ha_sample_server",
		false);

	// Sleep for 45 seconds to allow cmm to reconfigure
	os::printf("    Sleeping for 45 seconds to allow cmm to reconfigure\n");
	os::usecsleep((long)45 * 1000000);

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// Two-Phase Test Case 2-G
//
int
two_phase_2_g(int, char *[])
{
	os::printf("*** Two-Phase Mini-Transaction Scenario 2 Fault G\n");

#ifdef _FAULT_INJECTION
	// Kill primary after the 1st checkpoint
	// Fault points is in the service implemenetation

	// Figure out which node we'd like to fault (Primary node)
	rm_snap		_rma_snapshot;
	nodeid_t	primary_nodeid;
	Environment	e;

	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_primary(primary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get primary nodeid\n");
		return (1);
	}

	os::printf("    Primary is node: %d\n", primary_nodeid);

	return (two_phase_mini_1(
		fault_two_phase_2_g_on,
		fault_two_phase_2_g_off,
		&primary_nodeid,
		(int)sizeof (primary_nodeid),
		e,
		"ha_sample_server",
		false));
#else
	return (no_fault_injection());
#endif
}


//
// Two-Phase Test Case 2-H
//
int
two_phase_2_h(int, char *[])
{
	os::printf("*** Two-Phase Mini-Transaction Scenario 2 Fault H\n");

#ifdef _FAULT_INJECTION
	// Kill primary after the 1st checkpoint (after action has failed)
	// Fault points is in the service implemenetation

	// Figure out which node we'd like to fault (Primary node)
	rm_snap		_rma_snapshot;
	nodeid_t	primary_nodeid;
	Environment	e;
	int		rslt;

	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_primary(primary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get primary nodeid\n");
		return (1);
	}

	os::printf("    Primary is node: %d\n", primary_nodeid);

	rslt = two_phase_mini_1(
		fault_two_phase_2_h_on,
		fault_two_phase_2_h_off,
		&primary_nodeid,
		(int)sizeof (primary_nodeid),
		e,
		"ha_sample_server",
		false);

	// We expect a failure to return
	if (rslt != 0) {
		if (repl_sample::could_not_update::_exnarrow(e.exception())) {
			os::printf("--- PASS: Error was expected, Exception "
				"is ok\n");
			return (0);
		} else {
			os::printf("+++ FAIL: Error was expected, Incorrect "
				"exception detected, should have been: "
				"repl_sample::could_not_update\n");
			return (1);
		}
	} else {
		os::printf("+++ FAIL: Expected failure to be returned\n");
		return (1);
	}
#else
	return (no_fault_injection());
#endif
}


//
// Two-Phase Test Case 2-M
//
int
two_phase_2_m(int, char *[])
{
	os::printf("*** Two-Phase Mini-Transaction Scenario 2 Fault M\n");

#ifdef _FAULT_INJECTION
	// Kill primary after the 2nd checkpoint (after action has failed)
	// Fault point is in the send reply

	// Figure out which node we'd like to fault (Primary node)
	rm_snap		_rma_snapshot;
	nodeid_t	primary_nodeid;
	Environment	e;
	int		rslt;

	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_primary(primary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get primary nodeid\n");
		return (1);
	}

	os::printf("    Primary is node: %d\n", primary_nodeid);

	rslt = two_phase_mini_1(
		fault_two_phase_2_m_on,
		fault_two_phase_2_m_off,
		&primary_nodeid,
		(int)sizeof (primary_nodeid),
		e,
		"ha_sample_server",
		false);

	// We expect a failure to return
	if (rslt != 0) {
		if (repl_sample::could_not_update::_exnarrow(e.exception())) {
			os::printf("--- PASS: Error was expected, Exception "
				"is ok\n");
			return (0);
		} else {
			os::printf("+++ FAIL: Error was expected, Incorrect "
				"exception detected, should have been: "
				"repl_sample::could_not_update\n");
			return (1);
		}
	} else {
		os::printf("+++ FAIL: Expected failure to be returned\n");
		return (1);
	}
#else
	return (no_fault_injection());
#endif
}


//
// Two-Phase Test Case 2-N
//
int
two_phase_2_n(int, char *[])
{
	os::printf("*** Two-Phase Mini-Transaction Scenario 2 Fault N\n");

#ifdef _FAULT_INJECTION
	// Kill primary after the add_commit has been called

	// Figure out which node we'd like to fault (Primary node)
	rm_snap		_rma_snapshot;
	nodeid_t	primary_nodeid;
	Environment	e;

	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_primary(primary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get primary nodeid\n");
		return (1);
	}

	os::printf("    Primary is node: %d\n", primary_nodeid);

	return (two_phase_mini_1(
		fault_two_phase_2_n_on,
		fault_two_phase_2_n_off,
		&primary_nodeid,
		(int)sizeof (primary_nodeid),
		e,
		"ha_sample_server",
		false));
#else
	return (no_fault_injection());
#endif
}


//
//
// Two-Phase Test Case 2-O
//
int
two_phase_2_o(int, char *[])
{
	os::printf("*** Two-Phase Mini-Transaction Scenario 2 Fault O\n");

#ifdef _FAULT_INJECTION
	// Kill primary after the add_commit has been called

	// Figure out which node we'd like to fault (Primary node)
	rm_snap		_rma_snapshot;
	nodeid_t	primary_nodeid;
	Environment	e;

	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_primary(primary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get primary nodeid\n");
		return (1);
	}

	os::printf("    Primary is node: %d\n", primary_nodeid);

	return (two_phase_mini_1(
		fault_two_phase_2_o_on,
		fault_two_phase_2_o_off,
		&primary_nodeid,
		(int)sizeof (primary_nodeid),
		e,
		"ha_sample_server",
		false));
#else
	return (no_fault_injection());
#endif
}


//
// Two-Phase Test Case 2-P
//
int
two_phase_2_p(int, char *[])
{
	os::printf("*** Two-Phase Mini-Transaction Scenario 2 Fault P\n");

#ifdef _FAULT_INJECTION
	// We want to make sure that a invocation who is delayed in returning
	// to client will complete succesfully when the commit is sent to
	// the secondaries.

	// Reuse the fault points from 1-Q

	retval_t	*retval;
	int		r;
	Environment	e;
	mc_sema_impl		*mc_sema1;
	mc_sema_impl		*mc_sema2;

	mc_sema::sema_var	mc_sema1_ref;
	mc_sema::sema_var	mc_sema2_ref;

	mc_sema1 = new mc_sema_impl;
	mc_sema2 = new mc_sema_impl;

	mc_sema1_ref = mc_sema1->get_objref();
	mc_sema2_ref = mc_sema2->get_objref();

	// Allocate the retval object
	retval = new retval_t;

	// Initialize both mc_sema objects
	mc_sema1_ref->init(0, e);
	if (e.exception()) {
		e.exception()->print_exception("mc_sema::init()");
		delete retval;
		return (1);
	}

	mc_sema2_ref->init(0, e);
	if (e.exception()) {
		e.exception()->print_exception("mc_sema::init()");
		delete retval;
		return (1);
	}

	// Reset the exitcount
	retval->exitcount = 0;

	retval->id1 = mc_sema1_ref->get_cookie(e);
	if (e.exception()) {
		e.exception()->print_exception("mc_sema::init()");
		delete retval;
		return (1);
	}

	retval->id2 = mc_sema2_ref->get_cookie(e);
	if (e.exception()) {
		e.exception()->print_exception("mc_sema::init()");
		delete retval;
		return (1);
	}

	ASSERT(retval->id1 != 0);
	ASSERT(retval->id2 != 0);

#if defined(_KERNEL)
	if (thread_create(NULL, (size_t)0,
	    (void(*)(void))two_phase_aux_hang_on_return,
	    (caddr_t)retval, (size_t)0, &p0, TS_RUN, 60) == NULL) {
		os::printf("+++ FAIL: thread_create failed\n");
		delete retval;
		return (1);
	}
#else
	if (thr_create(NULL, (size_t)0, two_phase_aux_hang_on_return,
	    retval, THR_BOUND, NULL)) {
		os::printf("+++ FAIL: thread_create failed\n");
		delete retval;
		return (1);
	}
#endif

	// Do a sema_p on mc_sema1 that will cause us to block until the other
	// invocation thread does sema_v after add_commit()
	mc_sema1_ref->sema_p(e);
	if (e.exception()) {
		e.exception()->print_exception("mc_sema::sema_p()");
		delete retval;
		return (1);
	}

	// Now this thread will run through a small one-phase mini-transaction
	// that will checkpoint therefore carrying with it the commit()
	os::printf("    Checkpoint invocation starting...\n");

	r = one_phase_mini_1(NULL, NULL, NULL, 0, e);

	// XXX - When we exit.. we should probably cancel the other thread
	if (r != 0) {
		delete retval;
		return (r);
	}

	if (e.exception()) {
		e.exception()->print_exception("one_phase_mini_1()");
		delete retval;
		return (1);
	}

	// At this point the commit has been flushed all that is needed now
	// is to reboot the primary node.  It wont matter that we dont do
	// a sema_v on mc_sema2 (which would release the invoking thread)
	// since killing the primary will make that thread do a re-try.
	FaultFunctions::wait_for_arg_t	wait_arg;
	nodeid_t			primary_nodeid;
	rm_snap				rma_snapshot;

	if (!rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		delete retval;
		return (1);
	}

	if (rma_snapshot.get_primary(primary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get primary nodeid\n");
		delete retval;
		return (1);
	}

	wait_arg.op	= FaultFunctions::WAIT_FOR_UNKNOWN;
	wait_arg.nodeid	= primary_nodeid;

	// Reboot primary node
	FaultFunctions::reboot(0, &wait_arg, (uint32_t)sizeof (wait_arg));

	// Wait for the other thread to finish before we stop this test
	retval->lck.lock();
	while (retval->exitcount != 1) {
		retval->cv.wait(&(retval->lck));
	}

	// Retrieve the return value
	r = retval->value;
	retval->lck.unlock();

	// Destroy the mc_sema objects
	mc_sema1_ref->destroy(e);
	if (e.exception()) {
		e.exception()->print_exception("mc_sema::destroy()");
	}

	mc_sema2_ref->destroy(e);
	if (e.exception()) {
		e.exception()->print_exception("mc_sema::destroy()");
	}

	delete retval;
	return (r);
#else
	return (no_fault_injection());
#endif
}


//
// Two-Phase Test Case 2-Q
//
int
two_phase_2_q(int, char *[])
{
	os::printf("*** Two-Phase Mini-Transaction Scenario 2 Fault Q\n");

#ifdef _FAULT_INJECTION
	// We want to make sure that a invocation who is delayed in returning
	// to client will complete succesfully when the commit is sent to
	// the secondaries.

	// Reuse the fault points from 1-Q

	retval_t	*retval;
	int		r;
	Environment	e;
	mc_sema::sema_var	mc_sema1_ref;
	mc_sema::sema_var	mc_sema2_ref;

	mc_sema1_ref = (new mc_sema_impl)->get_objref();
	mc_sema2_ref = (new mc_sema_impl)->get_objref();
	ASSERT(!CORBA::is_nil(mc_sema1_ref));
	ASSERT(!CORBA::is_nil(mc_sema2_ref));

	// Allocate the retval object
	retval = new retval_t;

	// Initialize both mc_sema objects
	mc_sema1_ref->init(0, e);
	if (e.exception()) {
		e.exception()->print_exception("mc_sema::init()");
		delete retval;
		return (1);
	}

	mc_sema2_ref->init(0, e);
	if (e.exception()) {
		e.exception()->print_exception("mc_sema::init()");
		delete retval;
		return (1);
	}

	// Reset the exitcount
	retval->exitcount = 0;

	retval->id1 = mc_sema1_ref->get_cookie(e);
	if (e.exception()) {
		e.exception()->print_exception("mc_sema::init()");
		delete retval;
		return (1);
	}

	retval->id2 = mc_sema2_ref->get_cookie(e);
	if (e.exception()) {
		e.exception()->print_exception("mc_sema::init()");
		delete retval;
		return (1);
	}

#if defined(_KERNEL)
	if (thread_create(NULL, (size_t)0,
	    (void(*)(void))two_phase_aux_hang_on_return,
	    (caddr_t)retval, (size_t)0, &p0, TS_RUN, 60) == NULL) {
		os::printf("+++ FAIL: thread_create failed\n");
		delete retval;
		return (1);
	}
#else
	if (thr_create(NULL, (size_t)0, two_phase_aux_hang_on_return,
	    retval, THR_BOUND, NULL)) {
		os::printf("+++ FAIL: thread_create failed\n");
		delete retval;
		return (1);
	}
#endif

	// Do a sema_p on mc_sema1 that will cause us to block until the other
	// invocation thread does sema_v after add_commit()
	mc_sema1_ref->sema_p(e);
	if (e.exception()) {
		e.exception()->print_exception("mc_sema::sema_p()");
		delete retval;
		return (1);
	}

	// Now this thread will run through a small one-phase mini-transaction
	// that will checkpoint therefore carrying with it the commit()
	os::printf("    Checkpoint invocation starting...\n");

	r = one_phase_mini_1(NULL, NULL, NULL, 0, e);

	// XXX - When we exit.. we should probably cancel the other thread
	if (r != 0) {
		delete retval;
		return (r);
	}

	if (e.exception()) {
		e.exception()->print_exception("one_phase_mini_1()");
		delete retval;
		return (1);
	}

	// At this point the commit has been flushed all that is needed now
	// is to do a sema_v on mc_sema2, which will allow the other thread to
	// continue
	mc_sema2_ref->sema_v(e);
	if (e.exception()) {
		e.exception()->print_exception("one_phase_mini_1()");
		delete retval;
		return (1);
	}

	// Wait for the other thread to finish before we stop this test
	retval->lck.lock();
	while (retval->exitcount != 1) {
		retval->cv.wait(&(retval->lck));
	}

	// Retrieve the return value
	r = retval->value;
	retval->lck.unlock();

	// Destroy the mc_sema objects
	mc_sema1_ref->destroy(e);
	if (e.exception()) {
		e.exception()->print_exception("mc_sema::destroy()");
	}

	mc_sema2_ref->destroy(e);
	if (e.exception()) {
		e.exception()->print_exception("mc_sema::destroy()");
	}

	delete retval;
	return (r);
#else
	return (no_fault_injection());
#endif
}
