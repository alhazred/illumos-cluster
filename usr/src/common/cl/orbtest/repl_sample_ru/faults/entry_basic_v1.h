/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ENTRY_BASIC_V1_H
#define	_ENTRY_BASIC_V1_H

#pragma ident	"@(#)entry_basic_v1.h	1.7	08/05/20 SMI"

#include <orbtest/repl_sample_ru/faults/entry_common.h>

int basic_1_a(int, char *[]);
int basic_1_b(int, char *[]);
int basic_1_c(int, char *[]);
int basic_1_d(int, char *[]);
int basic_1_e(int, char *[]);
int basic_1_f(int, char *[]);
int basic_2_a(int, char *[]);
int basic_2_c(int, char *[]);
int basic_3_a(int, char *[]);
int basic_3_b(int, char *[]);
int basic_3_c(int, char *[]);
int basic_3_d(int, char *[]);

#endif	/* _ENTRY_BASIC_V1_H */
