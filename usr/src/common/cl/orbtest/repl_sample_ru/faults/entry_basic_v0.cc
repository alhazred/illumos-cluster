/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)entry_basic_v0.cc	1.6	08/05/20 SMI"

#include	<orbtest/repl_sample_ru/repl_sample_clnt_v0.h>
#include	<sys/os.h>
#include	<orb/invo/common.h>
#include	<orbtest/rm_probe/rm_snap.h>
#include	<orb/infrastructure/orb_conf.h>

#include	<orbtest/repl_sample_ru/faults/fault_basic_1_a.h>
#include	<orbtest/repl_sample_ru/faults/fault_basic_1_b.h>
#include	<orbtest/repl_sample_ru/faults/fault_basic_1_c.h>
#include	<orbtest/repl_sample_ru/faults/fault_basic_1_d.h>
#include	<orbtest/repl_sample_ru/faults/fault_basic_1_f.h>
#include	<orbtest/repl_sample_ru/faults/fault_basic_2_a.h>

//
// Basic 1-A Test case
//
int
basic_1_a(int, char *[])
{
	os::printf("*** Basic Mini-Transaction Scenario 1 Fault A\n");

#ifdef _FAULT_INJECTION
	// Figure out which node we'd like to fault (Primary node)
	rm_snap		_rma_snapshot;
	nodeid_t	primary_nodeid;
	Environment	e;

	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_primary(primary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get primary nodeid\n");
		return (1);
	}

	os::printf("    Primary is node: %d\n", primary_nodeid);

	return (simple_mini_1(
		fault_basic_1_a_on,
		fault_basic_1_a_off,
		&primary_nodeid,
		(int)sizeof (primary_nodeid),
		e));
#else
	return (no_fault_injection());
#endif
}


//
// Basic 1-B Test case
//
int
basic_1_b(int, char *[])
{
	os::printf("*** Basic Mini-Transaction Scenario 1 Fault B\n");

#ifdef _FAULT_INJECTION
	// Figure out which node we'd like to fault (Primary node)
	rm_snap		_rma_snapshot;
	nodeid_t	primary_nodeid;
	Environment	e;

	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_primary(primary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get primary nodeid\n");
		return (1);
	}

	os::printf("    Primary is node: %d\n", primary_nodeid);

	return (simple_mini_1(
		fault_basic_1_b_on,
		fault_basic_1_b_off,
		&primary_nodeid,
		(int)sizeof (primary_nodeid),
		e));
#else
	return (no_fault_injection());
#endif
}


//
// Basic 1-C Test case
//
int
basic_1_c(int, char *[])
{
	os::printf("*** Basic Mini-Transaction Scenario 1 Fault C\n");

#ifdef _FAULT_INJECTION
	// Figure out which node we'd like to fault (Primary node)
	rm_snap		_rma_snapshot;
	nodeid_t	primary_nodeid;
	Environment	e;

	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_primary(primary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get primary nodeid\n");
		return (1);
	}

	os::printf("    Primary is node: %d\n", primary_nodeid);

	return (simple_mini_1(
		fault_basic_1_c_on,
		fault_basic_1_c_off,
		&primary_nodeid,
		(int)sizeof (primary_nodeid),
		e));
#else
	return (no_fault_injection());
#endif
}


//
// Basic 1-D Test case
//
int
basic_1_d(int, char *[])
{
	os::printf("*** Basic Mini-Transaction Scenario 1 Fault D\n");

#ifdef _FAULT_INJECTION
	// Figure out which node we'd like to fault (Primary node)
	rm_snap		_rma_snapshot;
	nodeid_t	primary_nodeid;
	Environment	e;

	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_primary(primary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get primary nodeid\n");
		return (1);
	}

	os::printf("    Primary is node: %d\n", primary_nodeid);

	return (simple_mini_1(
		fault_basic_1_d_on,
		fault_basic_1_d_off,
		&primary_nodeid,
		(int)sizeof (primary_nodeid),
		e));
#else
	return (no_fault_injection());
#endif
}


//
// Basic 1-E Test case
//
int
basic_1_e(int, char *[])
{
	os::printf("*** Basic Mini-Transaction Scenario 1 Fault E\n");

#ifdef _FAULT_INJECTION
	int		retval;
	Environment	e;
	nodeid_t	nodeid;

	// Reuse the Fault points from basic 1-d

	// Set nodeid to 0
	nodeid = 0;

	retval = simple_mini_1(
		fault_basic_1_d_on,
		fault_basic_1_d_off,
		&nodeid,
		(int)sizeof (nodeid_t),
		e);

	// We expect a failure to come back
	if (retval == 1) {
		if (CORBA::COMM_FAILURE::_exnarrow(e.exception())) {
			os::printf("--- PASS: Error was expected, Exception "
				"is ok\n");
			return (0);
		} else {
			os::printf("+++ FAIL: Error was expected, Incorrect "
				"exceptoin detected, should have been: "
				"COMM_FAILURE\n");
			return (1);
		}
	} else {
		os::printf("+++ FAIL: Expected failure to be returned\n");
		return (1);
	}
#else
	return (no_fault_injection());
#endif
}


//
// Basic 1-F Test case
//
int
basic_1_f(int, char *[])
{
	os::printf("*** Basic Mini-Transaction Scenario 1 Fault F\n");

#ifdef _FAULT_INJECTION
	// Figure out our nodenumber, (We're the client)
	nodeid_t	client_nodeid = orb_conf::node_number();
	Environment	e;

	os::printf("    Client is node: %d\n", client_nodeid);

	return (simple_mini_1(
		fault_basic_1_f_on,
		fault_basic_1_f_off,
		&client_nodeid,
		(int)sizeof (client_nodeid),
		e));
#else
	return (no_fault_injection());
#endif
}


//
// Basic 2-A Test case
//
int
basic_2_a(int, char *[])
{
	os::printf("*** Basic Mini-Transaction Scenario 2 Fault A\n");

#ifdef _FAULT_INJECTION
	Environment	e;
	nodeid_t	nodeid = 0;

	return (simple_mini_1(
		fault_basic_2_a_on,
		fault_basic_2_a_off,
		&nodeid,
		(int)sizeof (nodeid_t),
		e));
#else
	return (no_fault_injection());
#endif
}


//
// Basic 2-C Test case
//
int
basic_2_c(int, char *[])
{
	os::printf("*** Basic Mini-Transaction Scenario 2 Fault C\n");

#ifdef _FAULT_INJECTION
	// Reuse the fault points from 1-C

	// Figure out which node we'd like to fault (Primary node)
	rm_snap		_rma_snapshot;
	nodeid_t	primary_nodeid;
	Environment	e;


	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_primary(primary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get primary nodeid\n");
		return (1);
	}

	os::printf("    Primary is node: %d\n", primary_nodeid);

	return (simple_mini_1(
		fault_basic_1_c_on,
		fault_basic_1_c_off,
		&primary_nodeid,
		(int)sizeof (primary_nodeid),
		e));
#else
	return (no_fault_injection());
#endif
}


//
// Basic 3-A Test case
//
int
basic_3_a(int, char *[])
{
	os::printf("*** Basic Mini-Transaction Scenario 3 Fault A\n");

#ifdef _FAULT_INJECTION
	// Reuse the faults from 1-A

	// Figure out which node we'd like to fault (Primary node)
	rm_snap		_rma_snapshot;
	nodeid_t	primary_nodeid;
	Environment	e;

	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_primary(primary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get primary nodeid\n");
		return (1);
	}

	os::printf("    Primary is node: %d\n", primary_nodeid);

	return (simple_mini_1(
		fault_basic_1_a_on,
		fault_basic_1_a_off,
		&primary_nodeid,
		(int)sizeof (primary_nodeid),
		e));
#else
	return (no_fault_injection());
#endif
}


//
// Basic 3-B Test case
//
int
basic_3_b(int, char *[])
{
	os::printf("*** Basic Mini-Transaction Scenario 3 Fault B\n");

#ifdef _FAULT_INJECTION
	// Reuse faults from 1-B

	// Figure out which node we'd like to fault (Primary node)
	rm_snap		_rma_snapshot;
	nodeid_t	primary_nodeid;
	Environment	e;

	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_primary(primary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get primary nodeid\n");
		return (1);
	}

	os::printf("    Primary is node: %d\n", primary_nodeid);

	return (simple_mini_1(
		fault_basic_1_b_on,
		fault_basic_1_b_off,
		&primary_nodeid,
		(int)sizeof (primary_nodeid),
		e));
#else
	return (no_fault_injection());
#endif
}


//
// Basic 3-C Test case
//
int
basic_3_c(int, char *[])
{
	os::printf("*** Basic Mini-Transaction Scenario 3 Fault C\n");

#ifdef _FAULT_INJECTION
	// Re-use the faults from 1-C

	// Figure out which node we'd like to fault (Primary node)
	rm_snap		_rma_snapshot;
	nodeid_t	primary_nodeid;
	Environment	e;

	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_primary(primary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get primary nodeid\n");
		return (1);
	}

	os::printf("    Primary is node: %d\n", primary_nodeid);

	return (simple_mini_1(
		fault_basic_1_c_on,
		fault_basic_1_c_off,
		&primary_nodeid,
		(int)sizeof (primary_nodeid),
		e));
#else
	return (no_fault_injection());
#endif
}


//
// Basic 3-D Test case
//
int
basic_3_d(int, char *[])
{
	os::printf("*** Basic Mini-Transaction Scenario 3 Fault D\n");

#ifdef _FAULT_INJECTION
	// Reuse faults from Basic 1-D

	// Figure out which node we'd like to fault (Primary node)
	rm_snap		_rma_snapshot;
	nodeid_t	primary_nodeid;
	Environment	e;

	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_primary(primary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get primary nodeid\n");
		return (1);
	}

	os::printf("    Primary is node: %d\n", primary_nodeid);

	return (simple_mini_1(
		fault_basic_1_d_on,
		fault_basic_1_d_off,
		&primary_nodeid,
		(int)sizeof (primary_nodeid),
		e));
#else
	return (no_fault_injection());
#endif
}
