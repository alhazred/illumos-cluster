/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)entry_one_phase_idempotent_v0.cc	1.6	08/05/20 SMI"

#include	<orbtest/repl_sample_ru/repl_sample_clnt_v0.h>
#include	<sys/os.h>
#include	<orb/invo/common.h>
#include	<orbtest/rm_probe/rm_snap.h>

#include	<orbtest/repl_sample_ru/faults/fault_basic_1_a.h>
#include	<orbtest/repl_sample_ru/faults/fault_basic_1_b.h>
#include	<orbtest/repl_sample_ru/faults/fault_basic_1_d.h>

#include	<orbtest/repl_sample_ru/faults/fault_one_phase_2_c.h>
#include	<orbtest/repl_sample_ru/faults/fault_one_phase_2_d.h>
#include	<orbtest/repl_sample_ru/faults/fault_one_phase_2_e.h>
#include	<orbtest/repl_sample_ru/faults/fault_one_phase_2_g.h>

//
// One-Phase Idempotent Test Case 1-A
//
int
one_phase_idempotent_1_a(int, char *[])
{
	// We're just checking that a regular invocation with
	// checkpointing can succeed
	os::printf("*** One-Phase Idempotent Mini-Transaction Scenario 1"
		" Fault A\n");
	Environment	e;

	return (one_phase_mini_1(NULL, NULL, NULL, 0, e, "ha_sample_server",
		false));
}


//
// One-Phase Idempotent Test Case 1-B
//
int
one_phase_idempotent_1_b(int, char *[])
{
	// We're just checking that a regular invocation with
	// checkpointing can succeed
	os::printf("*** One-Phase Idempotent Mini-Transaction Scenario 1"
		" Fault B\n");
	Environment	e;

	return (one_phase_mini_1(NULL, NULL, NULL, 0, e, "ha_sample_server",
		false));
}


//
// One-Phase Idempotent Test Case 1-I
//
int
one_phase_idempotent_1_i(int, char *[])
{
	os::printf("*** One-Phase Idempotent Mini-Transaction Scenario 1 "
		"Fault I\n");

#ifdef _FAULT_INJECTION
	int		retval;
	Environment	e;

	// Reuse the same fault point as Basic 1-D ... but set the
	// Set nodeid to 0
	nodeid_t	nodeid = 0;

	retval = one_phase_mini_1(
		fault_basic_1_d_on,
		fault_basic_1_d_off,
		&nodeid,
		(int)sizeof (nodeid_t),
		e,
		"ha_sample_server",
		false);

	// We expect a failure to come back
	if (retval == 1) {
		if (CORBA::COMM_FAILURE::_exnarrow(e.exception())) {
			os::printf("--- PASS: Error was expected, Exception "
				"is ok\n");
			return (0);
		} else {
			os::printf("+++ FAIL: Error was expected, Incorrect "
				"exceptoin detected, should have been: "
				"COMM_FAILURE\n");
			return (1);
		}
	} else {
		os::printf("+++ FAIL: Expected failure to be returned\n");
		return (1);
	}
#else
	return (no_fault_injection());
#endif
}


//
// One-Phase Idempotent Test Case 2-A
//
int
one_phase_idempotent_2_a(int, char *[])
{
	os::printf("*** One-Phase Idempotent Mini-Transaction Scenario 2"
		" Fault A\n");

#ifdef _FAULT_INJECTION
	// Reuse the faults from the Simple Mini-transaction 1A

	// Figure out which node we'd like to fault (Primary node)
	rm_snap		_rma_snapshot;
	nodeid_t	primary_nodeid;
	Environment	e;

	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_primary(primary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get primary nodeid\n");
		return (1);
	}

	os::printf("    Primary is node: %d\n", primary_nodeid);

	return (one_phase_mini_1(
		fault_basic_1_a_on,
		fault_basic_1_a_off,
		&primary_nodeid,
		(int)sizeof (primary_nodeid),
		e,
		"ha_sample_server",
		false));
#else
	return (no_fault_injection());
#endif
}


//
// One-Phase Idempotent Test Case 2-B
//
int
one_phase_idempotent_2_b(int, char *[])
{
	os::printf("*** One-Phase Idempotent Mini-Transaction Scenario 2"
		" Fault B\n");

#ifdef _FAULT_INJECTION
	// Re-use the faults from the Basic mini-transaction 1B

	// Figure out which node we'd like to fault (Primary node)
	rm_snap		_rma_snapshot;
	nodeid_t	primary_nodeid;
	Environment	e;

	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_primary(primary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get primary nodeid\n");
		return (1);
	}

	os::printf("    Primary is node: %d\n", primary_nodeid);

	return (one_phase_mini_1(
		fault_basic_1_b_on,
		fault_basic_1_b_off,
		&primary_nodeid,
		(int)sizeof (primary_nodeid),
		e,
		"ha_sample_server",
		false));
#else
	return (no_fault_injection());
#endif
}


//
// One-Phase Idempotent Test Case 2-C
//
int
one_phase_idempotent_2_c(int, char *[])
{
	os::printf("*** One-Phase Idempotent Mini-Transaction Scenario 2"
		" Fault C\n");

#ifdef _FAULT_INJECTION
	// Figure out which node we'd like to fault (Primary node)
	rm_snap		_rma_snapshot;
	nodeid_t	primary_nodeid;
	Environment	e;

	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_primary(primary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get primary nodeid\n");
		return (1);
	}

	os::printf("    Primary is node: %d\n", primary_nodeid);

	return (one_phase_mini_1(
		fault_one_phase_2_c_on,
		fault_one_phase_2_c_off,
		&primary_nodeid,
		(int)sizeof (primary_nodeid),
		e,
		"ha_sample_server",
		false));
#else
	return (no_fault_injection());
#endif
}


//
// One-Phase Idempotent Test Case 2-D
//
int
one_phase_idempotent_2_d(int, char *[])
{
	os::printf("*** One-Phase Idempotent Mini-Transaction Scenario 2"
		" Fault D\n");

#ifdef _FAULT_INJECTION
	// Figure out which node we'd like to fault (Secondary node)
	rm_snap		_rma_snapshot;
	nodeid_t	secondary_nodeid;
	Environment	e;

	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_secondary(1, secondary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get secondary nodeid\n");
		return (1);
	}

	os::printf("    Secondary is node: %d\n", secondary_nodeid);

	return (one_phase_mini_1(
		fault_one_phase_2_d_on,
		fault_one_phase_2_d_off,
		&secondary_nodeid,
		(int)sizeof (secondary_nodeid),
		e,
		"ha_sample_server",
		false));
#else
	return (no_fault_injection());
#endif
}


//
// One-Phase Idempotent Test Case 2-E
//
int
one_phase_idempotent_2_e(int, char *[])
{
	os::printf("*** One-Phase Idempotent Mini-Transaction Scenario 2"
		" Fault E\n");

#ifdef _FAULT_INJECTION
	// Figure out which node we'd like to fault (Secondary node)
	rm_snap		_rma_snapshot;
	nodeid_t	secondary_nodeid;
	int		rslt;
	Environment	e;

	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_secondary(1, secondary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get secondary nodeid\n");
		return (1);
	}

	os::printf("    Secondary is node: %d\n", secondary_nodeid);

	rslt = one_phase_mini_1(
		fault_one_phase_2_e_on,
		fault_one_phase_2_e_off,
		&secondary_nodeid,
		(int)sizeof (secondary_nodeid),
		e,
		"ha_sample_server",
		false);

	// Sleep waiting for CMM reconfig
	os::printf("   Sleeping for 45 seconds waiting for CMM reconfig\n");
	os::usecsleep((long)45 * 1000000);

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// One-Phase Idempotent Test Case 2-G
//
int
one_phase_idempotent_2_g(int, char *[])
{
	os::printf("*** One-Phase Idempotent Mini-Transaction Scenario 2"
		" Fault G\n");

#ifdef _FAULT_INJECTION
	// Figure out which node we'd like to fault (Secondary node)
	rm_snap		_rma_snapshot;
	nodeid_t	secondary_nodeid;
	Environment	e;
	int		rslt;

	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_secondary(1, secondary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get secondary nodeid\n");
		return (1);
	}

	os::printf("    Secondary is node: %d\n", secondary_nodeid);

	rslt = one_phase_mini_1(
		fault_one_phase_2_g_on,
		fault_one_phase_2_g_off,
		&secondary_nodeid,
		(int)sizeof (secondary_nodeid),
		e,
		"ha_sample_server",
		false);

	// Wait for CMM reconfig
	os::usecsleep((long)25 * 1000000);

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// One-Phase Idempotent Test Case 2-H
//
int
one_phase_idempotent_2_h(int, char *[])
{
	os::printf("*** One-Phase Idempotent Mini-Transaction Scenario 2"
		" Fault H\n");

#ifdef _FAULT_INJECTION
	// Reuse the fault points from Basic 1_D

	// Figure out which node we'd like to fault (Primary node)
	rm_snap		_rma_snapshot;
	nodeid_t	primary_nodeid;
	Environment	e;

	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_primary(primary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get primary nodeid\n");
		return (1);
	}

	os::printf("    Primary is node: %d\n", primary_nodeid);

	return (one_phase_mini_1(
		fault_basic_1_d_on,
		fault_basic_1_d_off,
		&primary_nodeid,
		(int)sizeof (primary_nodeid),
		e,
		"ha_sample_server",
		false));
#else
	return (no_fault_injection());
#endif
}


//
// One-Phase Idempotent Test Case 2-I
//
int
one_phase_idempotent_2_i(int, char *[])
{
	os::printf("*** One-Phase Idempotent Mini-Transaction Scenario 2 "
		"Fault I\n");

#ifdef _FAULT_INJECTION
	int		retval;
	Environment	e;

	// Reuse the same fault point as Basic 1-D ...
	// Set nodeid to 0

	nodeid_t	nodeid = 0;

	retval = one_phase_mini_1(
		fault_basic_1_d_on,
		fault_basic_1_d_off,
		&nodeid,
		(int)sizeof (nodeid_t),
		e,
		"ha_sample_server",
		false);

	// We expect a failure to come back
	if (retval == 1) {
		if (CORBA::COMM_FAILURE::_exnarrow(e.exception())) {
			os::printf("--- PASS: Error was expected, Exception "
				"is ok\n");
			return (0);
		} else {
			os::printf("+++ FAIL: Error was expected, Incorrect "
				"exceptoin detected, should have been: "
				"COMM_FAILURE\n");
			return (1);
		}
	} else {
		os::printf("+++ FAIL: Expected failure to be returned\n");
		return (1);
	}
#else
	return (no_fault_injection());
#endif
}


//
// One-Phase Idempotent Test Case 3-A
//
int
one_phase_idempotent_3_a(int, char *[])
{
	os::printf("*** One-Phase Idempotent Mini-Transaction Scenario 3"
		" Fault A\n");

#ifdef _FAULT_INJECTION
	// Reuse the faults from the Simple Mini-transaction 1A

	// Figure out which node we'd like to fault (Primary node)
	rm_snap		_rma_snapshot;
	nodeid_t	primary_nodeid;
	Environment	e;

	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_primary(primary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get primary nodeid\n");
		return (1);
	}

	os::printf("    Primary is node: %d\n", primary_nodeid);

	return (one_phase_mini_1(
		fault_basic_1_a_on,
		fault_basic_1_a_off,
		&primary_nodeid,
		(int)sizeof (primary_nodeid),
		e,
		"ha_sample_server",
		false));
#else
	return (no_fault_injection());
#endif
}


//
// One-Phase Idempotent Test Case 3-B
//
int
one_phase_idempotent_3_b(int, char *[])
{
	os::printf("*** One-Phase Idempotent Mini-Transaction Scenario 3"
		" Fault B\n");

#ifdef _FAULT_INJECTION
	// Re-use the faults from the Basic mini-transaction 1B

	// Figure out which node we'd like to fault (Primary node)
	rm_snap		_rma_snapshot;
	nodeid_t	primary_nodeid;
	Environment	e;

	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_primary(primary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get primary nodeid\n");
		return (1);
	}

	os::printf("    Primary is node: %d\n", primary_nodeid);

	return (one_phase_mini_1(
		fault_basic_1_b_on,
		fault_basic_1_b_off,
		&primary_nodeid,
		(int)sizeof (primary_nodeid),
		e,
		"ha_sample_server",
		false));
#else
	return (no_fault_injection());
#endif
}


//
// One-Phase Idempotent Test Case 3-C
//
int
one_phase_idempotent_3_c(int, char *[])
{
	os::printf("*** One-Phase Idempotent Mini-Transaction Scenario 3"
		" Fault C\n");

#ifdef _FAULT_INJECTION
	// Reuse faults from One Phase Idempotent 2-C

	// Figure out which node we'd like to fault (Primary node)
	rm_snap		_rma_snapshot;
	nodeid_t	primary_nodeid;
	Environment	e;

	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_primary(primary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get primary nodeid\n");
		return (1);
	}

	os::printf("    Primary is node: %d\n", primary_nodeid);

	return (one_phase_mini_1(
		fault_one_phase_2_c_on,
		fault_one_phase_2_c_off,
		&primary_nodeid,
		(int)sizeof (primary_nodeid),
		e,
		"ha_sample_server",
		false));
#else
	return (no_fault_injection());
#endif
}


//
// One-Phase Idempotent Test Case 3-D
//
int
one_phase_idempotent_3_d(int, char *[])
{
	os::printf("*** One-Phase Idempotent Mini-Transaction Scenario 3"
		" Fault D\n");

#ifdef _FAULT_INJECTION
	// Re use faults from One Phase Idempotent 2-D

	// Figure out which node we'd like to fault (Secondary node)
	rm_snap		_rma_snapshot;
	nodeid_t	secondary_nodeid;
	Environment	e;

	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_secondary(1, secondary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get secondary nodeid\n");
		return (1);
	}

	os::printf("    Secondary is node: %d\n", secondary_nodeid);

	return (one_phase_mini_1(
		fault_one_phase_2_d_on,
		fault_one_phase_2_d_off,
		&secondary_nodeid,
		(int)sizeof (secondary_nodeid),
		e,
		"ha_sample_server",
		false));
#else
	return (no_fault_injection());
#endif
}


//
// One-Phase Idempotent Test Case 3-E
//
int
one_phase_idempotent_3_e(int, char *[])
{
	os::printf("*** One-Phase Idempotent Mini-Transaction Scenario 3"
		" Fault E\n");

#ifdef _FAULT_INJECTION
	// Reuse faults from One Phase Idempotetnt 2-E

	// Figure out which node we'd like to fault (Secondary node)
	rm_snap		_rma_snapshot;
	nodeid_t	secondary_nodeid;
	int		rslt;
	Environment	e;

	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_secondary(1, secondary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get secondary nodeid\n");
		return (1);
	}

	os::printf("    Secondary is node: %d\n", secondary_nodeid);

	rslt = one_phase_mini_1(
		fault_one_phase_2_e_on,
		fault_one_phase_2_e_off,
		&secondary_nodeid,
		(int)sizeof (secondary_nodeid),
		e,
		"ha_sample_server",
		false);

	// Sleep waiting for CMM reconfig
	os::printf("    Sleeping for 45 secs waiting for CMM reconfig\n");
	os::usecsleep((long)45 * 1000000);

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// One-Phase Idempotent Test Case 3-F
//
int
one_phase_idempotent_3_f(int, char *[])
{
	os::printf("*** One-Phase Idempotent Mini-Transaction Scenario 3"
		" Fault F\n");

#ifdef _FAULT_INJECTION
	// Reuse faults from One-Phase Idempotent 2-E
	// Set nodeid to 0
	nodeid_t	nodeid = 0;
	Environment	e;
	int		rslt;

	rslt = one_phase_mini_1(
		fault_one_phase_2_e_on,
		fault_one_phase_2_e_off,
		&nodeid,
		(int)sizeof (nodeid_t),
		e,
		"ha_sample_server",
		false);

	// Sleep for 45 seconds waiting for CMM reconfig
	os::printf("    Sleep for 45 seconds waiting for CMM reconfig\n");
	os::usecsleep((long)45 * 1000000);

	return (rslt);
#else
	return (no_fault_injection());
#endif
}


//
// One-Phase Idempotent Test Case 3-G
//
int
one_phase_idempotent_3_g(int, char *[])
{
	os::printf("*** One-Phase Idempotent Mini-Transaction Scenario 3"
		" Fault G\n");

#ifdef _FAULT_INJECTION
	// Reuse Faults from One-Phase Idempotent 2-G

	// Figure out which node we'd like to fault (Secondary node)
	rm_snap		_rma_snapshot;
	nodeid_t	secondary_nodeid;
	Environment	e;

	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_secondary(1, secondary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get secondary nodeid\n");
		return (1);
	}

	os::printf("    Secondary is node: %d\n", secondary_nodeid);

	return (one_phase_mini_1(
		fault_one_phase_2_g_on,
		fault_one_phase_2_g_off,
		&secondary_nodeid,
		(int)sizeof (secondary_nodeid),
		e,
		"ha_sample_server",
		false));
#else
	return (no_fault_injection());
#endif
}


//
// One-Phase Idempotent Test Case 3-H
//
int
one_phase_idempotent_3_h(int, char *[])
{
	os::printf("*** One-Phase Idempotent Mini-Transaction Scenario 3"
		" Fault H\n");

#ifdef _FAULT_INJECTION
	// Reuse the fault points from Basic 1_D

	// Figure out which node we'd like to fault (Primary node)
	rm_snap		_rma_snapshot;
	nodeid_t	primary_nodeid;
	Environment	e;

	if (!_rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	if (_rma_snapshot.get_primary(primary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get primary nodeid\n");
		return (1);
	}

	os::printf("    Primary is node: %d\n", primary_nodeid);

	return (one_phase_mini_1(
		fault_basic_1_d_on,
		fault_basic_1_d_off,
		&primary_nodeid,
		(int)sizeof (primary_nodeid),
		e,
		"ha_sample_server",
		false));
#else
	return (no_fault_injection());
#endif
}


//
// One-Phase Idempotent Test Case 3-I
//
int
one_phase_idempotent_3_i(int, char *[])
{
	os::printf("*** One-Phase Idempotent Mini-Transaction Scenario 3 "
		"Fault I\n");

#ifdef _FAULT_INJECTION
	int		retval;
	Environment	e;

	// Reuse the same fault point as Basic 1-D ...
	// Set nodeid to 0
	nodeid_t	nodeid = 0;

	retval = one_phase_mini_1(
		fault_basic_1_d_on,
		fault_basic_1_d_off,
		&nodeid,
		(int)sizeof (nodeid_t),
		e,
		"ha_sample_server",
		false);

	// We expect a failure to come back
	if (retval == 1) {
		if (CORBA::COMM_FAILURE::_exnarrow(e.exception())) {
			os::printf("--- PASS: Error was expected, Exception "
				"is ok\n");
			return (0);
		} else {
			os::printf("+++ FAIL: Error was expected, Incorrect "
				"exceptoin detected, should have been: "
				"COMM_FAILURE\n");
			return (1);
		}
	} else {
		os::printf("+++ FAIL: Expected failure to be returned\n");
		return (1);
	}
#else
	return (no_fault_injection());
#endif
}
