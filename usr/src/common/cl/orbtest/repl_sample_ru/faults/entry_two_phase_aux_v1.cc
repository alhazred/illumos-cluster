/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)entry_two_phase_aux_v1.cc	1.6	08/05/20 SMI"

#include	<sys/os.h>
#include	<orb/invo/common.h>
#include	<orb/infrastructure/orb_conf.h>
#include	<orb/fault/fault_functions.h>
#include	<orbtest/repl_sample_ru/repl_sample_clnt_v1.h>
#include	<orbtest/rm_probe/rm_snap.h>

#include	<orbtest/repl_sample_ru/faults/entry_common.h>
#include	<orbtest/repl_sample_ru/faults/entry_two_phase_aux_v1.h>
#include	<orbtest/repl_sample_ru/faults/fault_two_phase_1_q.h>

//
// Auxiliary Function
//
// This function will setup a fault point that will hang this invocation
// before returning to the client.  The thread will hang until another
// thread does a sema_v on mc_sema2  (Since we'l do a sema_p on it from
// a fault point.)   After the add_commit we'll do a sema_v on mc_sema1
// to let the test thread know we've passed the add_commit() function.
//
// This functions is used to test the commit() of a 2-phase mini-transaction.
//
void *
two_phase_aux_hang_on_return(void *parg)
{
#ifdef _FAULT_INJECTION
	// Use the fault point for Two-Phase 1-Q... Which will cause us to
	// hang on the return.
	Environment	e;
	retval_t	*retval = (retval_t *)parg;
	int		r = 0;

	ASSERT(retval->id1 != 0);
	ASSERT(retval->id2 != 0);

	rm_snap		rma_snapshot;
	nodeid_t	primary_nodeid;
	cookie_pair_t	fault_arg;

	if (!rma_snapshot.refresh("ha_sample_server")) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		r = 1;
	}

	if (rma_snapshot.get_primary(primary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not get primary nodeid\n");
		r = 1;
	}

	// Setup the arguments for the on/off functions
	fault_arg.nodeid = primary_nodeid;

	fault_arg.id1 = retval->id1;
	fault_arg.id2 = retval->id2;

	if (r == 0) {
		os::printf("    Primary is node: %d\n", primary_nodeid);

		r = two_phase_mini_1(
			fault_two_phase_1_q_on,
			fault_two_phase_1_q_off,
			&fault_arg,
			(int)sizeof (fault_arg),
			e);
	}

	retval->lck.lock();
	retval->value = r;
	retval->exitcount++;
	retval->cv.broadcast();
	retval->lck.unlock();
#else
	parg = NULL;				// to shut compiler's warning
	os::printf("+++ FAIL: two_phase_aux_hang_on_return(): "
		"Fault Injection Compilation Flags were not set!\n");
#endif // _FAULT_INJECTION

#if defined(_KERNEL)
	thread_exit();
#else
	thr_exit(NULL);
#endif
	return (NULL);
}
