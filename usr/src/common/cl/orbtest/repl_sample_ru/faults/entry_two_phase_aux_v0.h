/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ENTRY_TWO_PHASE_AUX_V0_H
#define	_ENTRY_TWO_PHASE_AUX_V0_H

#pragma ident	"@(#)entry_two_phase_aux_v0.h	1.7	08/05/20 SMI"

#include	<sys/os.h>
#include	<h/mc_sema.h>
#include	<orbtest/repl_sample_ru/faults/entry_common.h>

// Helper structure for return values
typedef struct {
	int		value;
	int		exitcount;
	os::mutex_t	lck;
	os::condvar_t	cv;

	mc_sema::cookie	id1;
	mc_sema::cookie	id2;
} retval_t;

//
// Auxiliary Function
//
// This function will setup a fault point that will hang this invocation
// before returning to the client.  The thread will hang until another
// thread signals the FI Framework with the function FaulFunctions::signal().
// This functions is used to test the commit() of a 2-phase mini-transaction.
void * two_phase_aux_hang_on_return(void *);

#endif	/* _ENTRY_TWO_PHASE_AUX_V0_H */
