/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)entry_switchover_sec_pri_v1.cc	1.6	08/05/20 SMI"

#include	<orbtest/repl_sample_ru/repl_sample_clnt_v1.h>
#include	<sys/os.h>
#include	<orbtest/rm_probe/rm_snap.h>
#include	<orb/fault/fault_injection.h>

#include	<orbtest/repl_sample_ru/faults/entry_common.h>
#include	<orbtest/repl_sample_ru/faults/fault_switchover_sec_pri_1_b.h>
#include	<orbtest/repl_sample_ru/faults/fault_switchover_sec_pri_1_c.h>
#include	<orbtest/repl_sample_ru/faults/fault_switchover_sec_pri_1_d.h>
#include	<orbtest/repl_sample_ru/faults/fault_switchover_sec_pri_1_e.h>
#include	<orbtest/repl_sample_ru/faults/fault_switchover_sec_pri_1_f.h>
#include	<orbtest/repl_sample_ru/faults/fault_switchover_sec_pri_1_g.h>
#include	<orbtest/repl_sample_ru/faults/fault_switchover_sec_pri_1_n.h>
#include	<orbtest/repl_sample_ru/faults/fault_switchover_sec_pri_1_o.h>
#include	<orbtest/repl_sample_ru/faults/fault_switchover_sec_pri_1_p.h>
#include	<orbtest/repl_sample_ru/faults/fault_switchover_sec_pri_1_q.h>
#include	<orbtest/repl_sample_ru/faults/fault_switchover_sec_pri_1_r.h>
#include	<orbtest/repl_sample_ru/faults/fault_switchover_sec_pri_1_s.h>
#include	<orbtest/repl_sample_ru/faults/fault_switchover_sec_pri_1_t.h>



//
// Switchover helper
//
#ifdef _FAULT_INJECTION
int
switchover_helper(FAULT_FPTR(f_on), FAULT_FPTR(f_off), nodeid_t fault_node,
    bool wait_after_switch, bool should_switch);
#else
int
switchover_helper(void *, void *, nodeid_t, bool, bool);
#endif // _FAULT_INJECTION

//
// Switchover Sec -> Pri Test Case 1-A
//
int
switchover_sec_pri_1_a(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 1 Fault A\n");

#ifdef _FAULT_INJECTION
	// No Fault
	// Just a simple switchover
	return (switchover_helper(NULL, NULL, 0, false, true));
#else
	return (no_fault_injection());
#endif
}


//
// Switchover Sec -> Pri Test Case 1-B
//
int
switchover_sec_pri_1_b(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 1 Fault B\n");

#ifdef _FAULT_INJECTION
	return (switchover_helper(
	    fault_switchover_sec_pri_1_b_on,
	    fault_switchover_sec_pri_1_b_off,
	    get_primary("ha_sample_server"),
	    false, true));
#else
	return (no_fault_injection());
#endif
}


//
// Switchover Sec -> Pri Test Case 1-C
//
int
switchover_sec_pri_1_c(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 1 Fault C\n");

#ifdef _FAULT_INJECTION
	return (switchover_helper(
	    fault_switchover_sec_pri_1_c_on,
	    fault_switchover_sec_pri_1_c_off,
	    get_primary("ha_sample_server"),
	    false, true));
#else
	return (no_fault_injection());
#endif
}


//
// Switchover Sec -> Pri Test Case 1-D
//
int
switchover_sec_pri_1_d(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 1 Fault D\n");

#ifdef _FAULT_INJECTION
	return (switchover_helper(
	    fault_switchover_sec_pri_1_d_on,
	    fault_switchover_sec_pri_1_d_off,
	    get_primary("ha_sample_server"),
	    false, true));
#else
	return (no_fault_injection());
#endif
}

//
// Switchover Sec -> Pri Test Case 1-E
//
int
switchover_sec_pri_1_e(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 1 Fault E\n");

#ifdef _FAULT_INJECTION
	return (switchover_helper(
	    fault_switchover_sec_pri_1_e_on,
	    fault_switchover_sec_pri_1_e_off,
	    get_primary("ha_sample_server"),
	    false, true));
#else
	return (no_fault_injection());
#endif
}


//
// Switchover Sec -> Pri Test Case 1-F
//
int
switchover_sec_pri_1_f(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 1 Fault F\n");

#ifdef _FAULT_INJECTION
	return (switchover_helper(
	    fault_switchover_sec_pri_1_f_on,
	    fault_switchover_sec_pri_1_f_off,
	    get_primary("ha_sample_server"),
	    false, true));
#else
	return (no_fault_injection());
#endif
}


//
// Switchover Sec -> Pri Test Case 1-G
//
int
switchover_sec_pri_1_g(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 1 Fault G\n");

#ifdef _FAULT_INJECTION
	return (switchover_helper(
	    fault_switchover_sec_pri_1_g_on,
	    fault_switchover_sec_pri_1_g_off,
	    get_primary("ha_sample_server"),
	    false, true));
#else
	return (no_fault_injection());
#endif
}


//
// Switchover Sec -> Pri Test Case 1-H
//
int
switchover_sec_pri_1_h(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 1 Fault H\n");

#ifdef _FAULT_INJECTION
	// Reuse fault from Switchover Sec->Pri 1-B
	return (switchover_helper(
	    fault_switchover_sec_pri_1_b_on,
	    fault_switchover_sec_pri_1_b_off,
	    get_secondary(1, "ha_sample_server"),
	    false, false));
#else
	return (no_fault_injection());
#endif
}


//
// Switchover Sec -> Pri Test Case 1-I
//
int
switchover_sec_pri_1_i(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 1 Fault I\n");

#ifdef _FAULT_INJECTION
	// Reuse fault from Switchover Sec->Pri 1-C
	return (switchover_helper(
	    fault_switchover_sec_pri_1_c_on,
	    fault_switchover_sec_pri_1_c_off,
	    get_secondary(1, "ha_sample_server"),
	    false, false));
#else
	return (no_fault_injection());
#endif
}


//
// Switchover Sec -> Pri Test Case 1-J
//
int
switchover_sec_pri_1_j(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 1 Fault J\n");

#ifdef _FAULT_INJECTION
	// Reuse Faults from Switchover Sec -> Pri 1-D
	return (switchover_helper(
	    fault_switchover_sec_pri_1_d_on,
	    fault_switchover_sec_pri_1_d_off,
	    get_secondary(1, "ha_sample_server"),
	    false, false));
#else
	return (no_fault_injection());
#endif
}


//
// Switchover Sec -> Pri Test Case 1-K
//
int
switchover_sec_pri_1_k(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 1 Fault K\n");

#ifdef _FAULT_INJECTION
	// Reuse fault from Switchover Sec->Pri 1-E
	return (switchover_helper(
	    fault_switchover_sec_pri_1_e_on,
	    fault_switchover_sec_pri_1_e_off,
	    get_secondary(1, "ha_sample_server"),
	    false, false));
#else
	return (no_fault_injection());
#endif
}


//
// Switchover Sec -> Pri Test Case 1-L
//
int
switchover_sec_pri_1_l(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 1 Fault L\n");

#ifdef _FAULT_INJECTION
	// Reuse fault from Switchover Sec->Pri 1-F
	return (switchover_helper(
	    fault_switchover_sec_pri_1_f_on,
	    fault_switchover_sec_pri_1_f_off,
	    get_secondary(1, "ha_sample_server"),
	    false, false));
#else
	return (no_fault_injection());
#endif
}


//
// Switchover Sec -> Pri Test Case 1-M
//
int
switchover_sec_pri_1_m(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 1 Fault M\n");

#ifdef _FAULT_INJECTION
	// Reuse fault from Switchover Sec->Pri 1-G
	return (switchover_helper(
	    fault_switchover_sec_pri_1_g_on,
	    fault_switchover_sec_pri_1_g_off,
	    get_secondary(1, "ha_sample_server"),
	    false, false));
#else
	return (no_fault_injection());
#endif
}


//
// Switchover Sec -> Pri Test Case 1-N
//
int
switchover_sec_pri_1_n(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 1 Fault N\n");

#ifdef _FAULT_INJECTION
	return (switchover_helper(
	    fault_switchover_sec_pri_1_n_on,
	    fault_switchover_sec_pri_1_n_off,
	    get_secondary(1, "ha_sample_server"),
	    false, false));
#else
	return (no_fault_injection());
#endif
}


//
// Switchover Sec -> Pri Test Case 1-O
//
int
switchover_sec_pri_1_o(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 1 Fault O\n");

#ifdef _FAULT_INJECTION
	return (switchover_helper(
	    fault_switchover_sec_pri_1_o_on,
	    fault_switchover_sec_pri_1_o_off,
	    get_secondary(1, "ha_sample_server"),
	    false, false));
#else
	return (no_fault_injection());
#endif
}


//
// Switchover Sec -> Pri Test Case 1-P
//
int
switchover_sec_pri_1_p(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 1 Fault P\n");

#ifdef _FAULT_INJECTION
	return (switchover_helper(
	    fault_switchover_sec_pri_1_p_on,
	    fault_switchover_sec_pri_1_p_off,
	    get_secondary(1, "ha_sample_server"),
	    false, false));
#else
	return (no_fault_injection());
#endif
}


//
// Switchover Sec -> Pri Test Case 1-Q
//
int
switchover_sec_pri_1_q(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 1 Fault Q\n");

#ifdef _FAULT_INJECTION
	return (switchover_helper(
	    fault_switchover_sec_pri_1_q_on,
	    fault_switchover_sec_pri_1_q_off,
	    get_secondary(1, "ha_sample_server"),
	    false, false));
#else
	return (no_fault_injection());
#endif
}


//
// Switchover Sec -> Pri Test Case 1-R
//
int
switchover_sec_pri_1_r(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 1 Fault R\n");

#ifdef _FAULT_INJECTION
	return (switchover_helper(
	    fault_switchover_sec_pri_1_r_on,
	    fault_switchover_sec_pri_1_r_off,
	    get_secondary(1, "ha_sample_server"),
	    true, false));
#else
	return (no_fault_injection());
#endif
}

//
// Switchover Sec -> Pri Test Case 1-S
//
int
switchover_sec_pri_1_s(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 1 Fault S\n");

#ifdef _FAULT_INJECTION
	return (switchover_helper(
	    fault_switchover_sec_pri_1_s_on,
	    fault_switchover_sec_pri_1_s_off,
	    get_secondary(1, "ha_sample_server"),
	    true, false));
#else
	return (no_fault_injection());
#endif
}

//
// Switchover Sec -> Pri Test Case 1-T
//
int
switchover_sec_pri_1_t(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 1 Fault T\n");

#ifdef _FAULT_INJECTION
	return (switchover_helper(
	    fault_switchover_sec_pri_1_t_on,
	    fault_switchover_sec_pri_1_t_off,
	    get_secondary(1, "ha_sample_server"),
	    true, false));
#else
	return (no_fault_injection());
#endif
}

//
// SCENARIO 2
//
//
// Switchover Sec -> Pri Test Case 2-A
//
int
switchover_sec_pri_2_a(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 2 Fault A\n");

#ifdef _FAULT_INJECTION
	// No Fault
	// Just a simple switchover
	return (switchover_helper(NULL, NULL, 0, false, true));
#else
	return (no_fault_injection());
#endif
}


//
// Switchover Sec -> Pri Test Case 2-B
//
int
switchover_sec_pri_2_b(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 2 Fault B\n");

#ifdef _FAULT_INJECTION
	return (switchover_helper(
	    fault_switchover_sec_pri_1_b_on,
	    fault_switchover_sec_pri_1_b_off,
	    get_primary("ha_sample_server"),
	    false, true));
#else
	return (no_fault_injection());
#endif
}


//
// Switchover Sec -> Pri Test Case 2-C
//
int
switchover_sec_pri_2_c(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 2 Fault C\n");

#ifdef _FAULT_INJECTION
	return (switchover_helper(
	    fault_switchover_sec_pri_1_c_on,
	    fault_switchover_sec_pri_1_c_off,
	    get_primary("ha_sample_server"),
	    false, true));
#else
	return (no_fault_injection());
#endif
}


//
// Switchover Sec -> Pri Test Case 2-D
//
int
switchover_sec_pri_2_d(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 2 Fault D\n");

#ifdef _FAULT_INJECTION
	return (switchover_helper(
	    fault_switchover_sec_pri_1_d_on,
	    fault_switchover_sec_pri_1_d_off,
	    get_primary("ha_sample_server"),
	    false, true));
#else
	return (no_fault_injection());
#endif
}

//
// Switchover Sec -> Pri Test Case 2-E
//
int
switchover_sec_pri_2_e(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 2 Fault E\n");

#ifdef _FAULT_INJECTION
	return (switchover_helper(
	    fault_switchover_sec_pri_1_e_on,
	    fault_switchover_sec_pri_1_e_off,
	    get_primary("ha_sample_server"),
	    false, true));
#else
	return (no_fault_injection());
#endif
}


//
// Switchover Sec -> Pri Test Case 2-F
//
int
switchover_sec_pri_2_f(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 2 Fault F\n");

#ifdef _FAULT_INJECTION
	return (switchover_helper(
	    fault_switchover_sec_pri_1_f_on,
	    fault_switchover_sec_pri_1_f_off,
	    get_primary("ha_sample_server"),
	    false, true));
#else
	return (no_fault_injection());
#endif
}


//
// Switchover Sec -> Pri Test Case 2-G
//
int
switchover_sec_pri_2_g(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 2 Fault G\n");

#ifdef _FAULT_INJECTION
	return (switchover_helper(
	    fault_switchover_sec_pri_1_g_on,
	    fault_switchover_sec_pri_1_g_off,
	    get_primary("ha_sample_server"),
	    false, true));
#else
	return (no_fault_injection());
#endif
}


//
// Switchover Sec -> Pri Test Case 2-H
//
int
switchover_sec_pri_2_h(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 2 Fault H\n");

#ifdef _FAULT_INJECTION
	// Reuse fault from Switchover Sec->Pri 1-B
	return (switchover_helper(
	    fault_switchover_sec_pri_1_b_on,
	    fault_switchover_sec_pri_1_b_off,
	    get_secondary(1, "ha_sample_server"),
	    false, false));
#else
	return (no_fault_injection());
#endif
}


//
// Switchover Sec -> Pri Test Case 2-I
//
int
switchover_sec_pri_2_i(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 2 Fault I\n");

#ifdef _FAULT_INJECTION
	// Reuse fault from Switchover Sec->Pri 1-C
	return (switchover_helper(
	    fault_switchover_sec_pri_1_c_on,
	    fault_switchover_sec_pri_1_c_off,
	    get_secondary(1, "ha_sample_server"),
	    false, false));
#else
	return (no_fault_injection());
#endif
}


//
// Switchover Sec -> Pri Test Case 2-J
//
int
switchover_sec_pri_2_j(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 2 Fault J\n");

#ifdef _FAULT_INJECTION
	// Reuse Faults from Switchover Sec -> Pri 1-D
	return (switchover_helper(
	    fault_switchover_sec_pri_1_d_on,
	    fault_switchover_sec_pri_1_d_off,
	    get_secondary(1, "ha_sample_server"),
	    false, false));
#else
	return (no_fault_injection());
#endif
}


//
// Switchover Sec -> Pri Test Case 2-K
//
int
switchover_sec_pri_2_k(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 2 Fault K\n");

#ifdef _FAULT_INJECTION
	// Reuse fault from Switchover Sec->Pri 1-E
	return (switchover_helper(
	    fault_switchover_sec_pri_1_e_on,
	    fault_switchover_sec_pri_1_e_off,
	    get_secondary(1, "ha_sample_server"),
	    false, false));
#else
	return (no_fault_injection());
#endif
}


//
// Switchover Sec -> Pri Test Case 2-L
//
int
switchover_sec_pri_2_l(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 2 Fault L\n");

#ifdef _FAULT_INJECTION
	// Reuse fault from Switchover Sec->Pri 1-F
	return (switchover_helper(
	    fault_switchover_sec_pri_1_f_on,
	    fault_switchover_sec_pri_1_f_off,
	    get_secondary(1, "ha_sample_server"),
	    false, false));
#else
	return (no_fault_injection());
#endif
}


//
// Switchover Sec -> Pri Test Case 2-M
//
int
switchover_sec_pri_2_m(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 2 Fault M\n");

#ifdef _FAULT_INJECTION
	// Reuse fault from Switchover Sec->Pri 1-G
	return (switchover_helper(
	    fault_switchover_sec_pri_1_g_on,
	    fault_switchover_sec_pri_1_g_off,
	    get_secondary(1, "ha_sample_server"),
	    false, false));
#else
	return (no_fault_injection());
#endif
}


//
// Switchover Sec -> Pri Test Case 2-N
//
int
switchover_sec_pri_2_n(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 2 Fault N\n");

#ifdef _FAULT_INJECTION
	return (switchover_helper(
	    fault_switchover_sec_pri_1_n_on,
	    fault_switchover_sec_pri_1_n_off,
	    get_secondary(1, "ha_sample_server"),
	    false, false));
#else
	return (no_fault_injection());
#endif
}


//
// Switchover Sec -> Pri Test Case 2-O
//
int
switchover_sec_pri_2_o(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 2 Fault O\n");

#ifdef _FAULT_INJECTION
	return (switchover_helper(
	    fault_switchover_sec_pri_1_o_on,
	    fault_switchover_sec_pri_1_o_off,
	    get_secondary(1, "ha_sample_server"),
	    false, false));
#else
	return (no_fault_injection());
#endif
}


//
// Switchover Sec -> Pri Test Case 2-P
//
int
switchover_sec_pri_2_p(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 2 Fault P\n");

#ifdef _FAULT_INJECTION
	return (switchover_helper(
	    fault_switchover_sec_pri_1_p_on,
	    fault_switchover_sec_pri_1_p_off,
	    get_secondary(1, "ha_sample_server"),
	    false, false));
#else
	return (no_fault_injection());
#endif
}


//
// Switchover Sec -> Pri Test Case 2-Q
//
int
switchover_sec_pri_2_q(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 2 Fault Q\n");

#ifdef _FAULT_INJECTION
	return (switchover_helper(
	    fault_switchover_sec_pri_1_q_on,
	    fault_switchover_sec_pri_1_q_off,
	    get_secondary(1, "ha_sample_server"),
	    false, false));
#else
	return (no_fault_injection());
#endif
}


//
// Switchover Sec -> Pri Test Case 2-R
//
int
switchover_sec_pri_2_r(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 2 Fault R\n");

#ifdef _FAULT_INJECTION
	return (switchover_helper(
	    fault_switchover_sec_pri_1_r_on,
	    fault_switchover_sec_pri_1_r_off,
	    get_secondary(1, "ha_sample_server"),
	    true, false));
#else
	return (no_fault_injection());
#endif
}

//
// Switchover Sec -> Pri Test Case 2-S
//
int
switchover_sec_pri_2_s(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 2 Fault S\n");

#ifdef _FAULT_INJECTION
	return (switchover_helper(
	    fault_switchover_sec_pri_1_s_on,
	    fault_switchover_sec_pri_1_s_off,
	    get_secondary(1, "ha_sample_server"),
	    true, false));
#else
	return (no_fault_injection());
#endif
}

//
// Switchover Sec -> Pri Test Case 2-T
//
int
switchover_sec_pri_2_t(int, char *[])
{
	os::printf("*** Switchover Secondary --> Primary Scenario 2 Fault T\n");

#ifdef _FAULT_INJECTION
	return (switchover_helper(
	    fault_switchover_sec_pri_1_t_on,
	    fault_switchover_sec_pri_1_t_off,
	    get_secondary(1, "ha_sample_server"),
	    true, false));
#else
	return (no_fault_injection());
#endif
}


//
// Switchover helper
//
#ifdef _FAULT_INJECTION
int
switchover_helper(FAULT_FPTR(f_on), FAULT_FPTR(f_off), nodeid_t fault_node,
    bool wait_after_switch, bool should_switch)
#else
int
switchover_helper(void *, void *, nodeid_t, bool, bool)
#endif // _FAULT_INJECTION
{
#ifdef _FAULT_INJECTION
	// Figure out which node we'd like to fault (Primary node)
	Environment	e;
	int		rslt;
	nodeid_t	start_primary;
	nodeid_t	end_primary;

	// Make sure that we're starting with a live service
	if (get_state("ha_sample_server") != replica::S_UP) {
		os::printf("+++ FAIL: Service \"ha_sample_server\" is not in"
		    " S_UP state (%d)\n", get_state("ha_sample_server"));
		return (1);
	}

	// Get the old primary
	if ((start_primary = get_primary("ha_sample_server")) == 0) {
		return (1);
	}

	os::printf("    Starting primary is node: %d\n", start_primary);
	os::printf("    Will fault node: %d\n", fault_node);

	// Popultate the fault arguments
	FaultFunctions::ha_dep_reboot_arg_t	reboot_args;
	reboot_args.sid		= get_sid("ha_sample_server");
	reboot_args.op		= FaultFunctions::WAIT_FOR_UNKNOWN;
	reboot_args.nodeid	= fault_node;

	rslt = change_sec_state(
	    f_on, f_off,
	    &reboot_args,
	    (int)sizeof (reboot_args),
	    "ha_sample_server",
	    replica::SC_SET_PRIMARY,
	    e);

	if (e.exception()) {
		if ((replica::failed_to_switchover::_exnarrow(e.exception())
		    != NULL) && !should_switch) {
			rslt = 0;
			e.clear();
		} else {
			e.exception()->print_exception("change_sec_state:");
			os::printf("+++ FAIL: Exception was not expected\n");
			return (1);
		}
	}

	if (rslt != 0) {
		os::printf("+++ FAIL: Error during switchover "
		    "(returned non-zero)");
		return (1);
	}

	if (wait_after_switch) {
		// Sleep for some time to give CMM time to reconfigure
		// 1,000,000 usecs in 1 sec
		// How about 30 seconds
		os::usecsleep((long)30 * 1000000);
	}

	// Make sure that we're ending with a live service
	if (get_state("ha_sample_server") != replica::S_UP) {
		os::printf("+++ FAIL: Service \"ha_sample_server\" is not in"
		    " S_UP state (%d)\n", get_state("ha_sample_server"));
		return (1);
	}

	if ((end_primary = get_primary("ha_sample_server")) == 0) {
		return (1);
	}

	os::printf("    Ending primary is: %d\n", end_primary);

	// Compare (they should be different)
	if (should_switch) {
		if (start_primary == end_primary) {
			os::printf("+++ FAIL: Switchover did not occur!?\n");
			return (1);
		}
	} else {
		if (start_primary != end_primary) {
			os::printf("+++ FAIL: Primary Prov's changed nodes "
			    "(they shouldn't have!)\n");
			return (1);
		}
	}

	os::printf("--- PASS: Switchover was completed.\n");
	return (0);
#else
	return (no_fault_injection());
#endif
}
