/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _REPL_SAMPLE_IMPL_V1_H
#define	_REPL_SAMPLE_IMPL_V1_H

#pragma ident	"@(#)repl_sample_impl_v1.h	1.9	08/05/20 SMI"

#include	<sys/os.h>
#include	<h/solobj.h>
#include	<h/version_manager.h>
#include	<h/repl_sample_v1.h>
#include	<repl/service/replica_tmpl.h>
#include	<repl/service/transaction_state.h>

class factory_impl;
class repl_persist_obj_impl;
class repl_sample_prov;
class repl_value_obj_impl;

// The stat object for the inc on the value_obj (volatile storage)
// This state is used in 2-phase mini-transactions
class value_inc_state : public transaction_state {
public:
	value_inc_state(int32_t val) : transaction_state(), value(val) { }
	~value_inc_state() {}

	void orphaned(Environment &) {}
	// void completed() {}
	// void commited() {}

	int32_t	value;
};

// The stat object for the inc on the value_obj (volatile storage)
// This state is used in 2-phase mini-transactions
/*
class two_value_inc_state : public transaction_state {
public:
	two_value_inc_state(int32_t val, int32_t val_2) :
		transaction_state(), value(val), value_2(val_2) { }
	~two_value_inc_state() {}

	void orphaned(Environment &) {}
	// void completed() {}
	// void commited() {}

	int32_t	value;
	int32_t	value_2;
};
*/

// The state object for the inc on the persist_obj (persistent storage)
// This state is used in 2-phase mini-transaction
class persist_inc_state : public transaction_state {
public:
	persist_inc_state(repl_sample::persist_obj_ptr objp, int32_t val) :
		transaction_state(), obj_ref(objp), value(val)
	{ my_progress = STARTED; }

	void committed();

	// The service secondary could potentially receive orphan message
	// when both client and service primary both die at roughly the
	// same time; it is not necessary to have the service secondary
	// to panic always. It should be left to the implementor to specify
	// how to handle the orphaned message. Therefore, instead, it
	// change from os::panic("XXX - Orphaned ckpt received\n")
	// to os::printf("XXX Warning XXX - Orphaned ckpt received\n")

	void orphaned(Environment &)
	{ os::printf("XXX Warning XXX - Orphaned ckpt received\n"); }

	// Helper for use during retries
	int32_t handle_retry(Environment &e);

	// Ammount of progress that has been checkpointer
	enum progress { STARTED, COMMITTED, FAILED };

	int32_t				value;
	repl_sample::persist_obj_var	obj_ref;
	progress			my_progress;
};

// The state object for the inc on the persist_obj (persistent storage)
// This state is used in 2-phase mini-transaction
class persist_2_inc_state : public transaction_state {
public:
	persist_2_inc_state(repl_sample::persist_obj_ptr objp, int32_t val) :
		transaction_state(), obj_ref(objp), value_2(val)
	{ my_progress = STARTED; }

	void committed();

	// The service secondary could potentially receive orphan message
	// when both client and service primary both die at roughly the
	// same time; it is not necessary to have the service secondary
	// to panic always. It should be left to the implementor to specify
	// how to handle the orphaned message. Therefore, instead, it
	// change from os::panic("XXX - Orphaned ckpt received\n")
	// to os::printf("XXX Warning XXX - Orphaned ckpt received\n")

	void orphaned(Environment &)
	{ os::printf("XXX Warning XXX - Orphaned ckpt received\n"); }

	// Helper for use during retries
	int32_t handle_retry(Environment &e);

	// Ammount of progress that has been checkpointer
	enum progress { STARTED, COMMITTED, FAILED };

	int32_t				value_2;
	repl_sample::persist_obj_var	obj_ref;
	progress			my_progress;
};

// The replica provider server for this HA service.

//
// Struct to keep track of provider ptr and module id to which it
// belongs. We wait for our provider to be deleted before unloading
// the module.
//
typedef struct prov_in_mod {
	repl_sample_prov *provp;
	int		 mod_id;
} prov_in_mod_t;

/* CSTYLED */
class repl_sample_prov : public repl_server<repl_sample::ckpt> {
	friend class repl_value_obj_impl;
	friend class repl_persist_obj_impl;
	friend class factory_impl;
public:

	// Constructor repl_sample_prov
	repl_sample_prov(const char *svc_desc, const char *repl_desc);
	~repl_sample_prov();

	// IDL Methods for checkpoint interface
	void ckpt_create_value_obj(
		repl_sample::value_obj_ptr obj,
		Environment &);
/*
	void ckpt_create_value_obj_a(
		repl_sample::value_obj_a_ptr obj,
		Environment &);
	void ckpt_create_value_obj_b(
		repl_sample::value_obj_b_ptr obj,
		Environment &);
	void ckpt_create_maj_value_obj(
		repl_sample::maj_value_obj_ptr obj,
		Environment &);
	void ckpt_create_combined_value_obj(
		repl_sample::combined_value_obj_ptr obj,
		Environment &);
*/
	void ckpt_create_persist_obj(
		repl_sample::persist_obj_ptr,
		Environment &);
	void ckpt_set_value(
		repl_sample::value_obj_ptr,
		int32_t,
		Environment &);
	void ckpt_set_value_2(
		repl_sample::value_obj_ptr,
		int32_t,
		Environment &);
	void ckpt_inc_value(
		repl_sample::value_obj_ptr,
		int32_t,
		Environment &);
	void ckpt_inc_value_2(
		repl_sample::value_obj_ptr,
		int32_t,
		Environment &);
	void ckpt_set_persist(
		repl_sample::persist_obj_ptr,
		int32_t,
		Environment &);
	void ckpt_set_persist_2(
		repl_sample::persist_obj_ptr,
		int32_t,
		Environment &);
	void ckpt_inc_persist_intent(
		repl_sample::persist_obj_ptr,
		int32_t,
		Environment &);
	void ckpt_inc_persist_2_intent(
		repl_sample::persist_obj_ptr,
		int32_t,
		Environment &);
	void ckpt_inc_persist_failure(Environment &);
	void ckpt_inc_persist_2_failure(Environment &);
	void ckpt_factory(repl_sample::factory_ptr obj, Environment &e);

	// Used in the checkpoint of objects to new secondaries
	void ckpt_value_obj(
		repl_sample::value_obj_ptr obj,
		int32_t val,
		Environment &e);
	void ckpt_new_value_obj(
		repl_sample::value_obj_ptr obj,
		int32_t val,
		int32_t val_2,
		Environment &e);
/*
	void ckpt_value_obj_a(
		repl_sample::value_obj_a_ptr obj,
		int32_t val,
		Environment &e);
	void ckpt_value_obj_b(
		repl_sample::value_obj_b_ptr obj,
		int32_t val,
		Environment &e);
	void ckpt_maj_value_obj(
		repl_sample::maj_value_obj_ptr obj,
		int32_t val,
		Environment &e);
	void ckpt_combined_value_obj(
		repl_sample::combined_value_obj_ptr obj,
		int32_t val,
		Environment &e);
*/
	void ckpt_persist_obj(
		repl_sample::persist_obj_ptr obj,
		int32_t val,
		Environment &e);
	void ckpt_new_persist_obj(
		repl_sample::persist_obj_ptr obj,
		int32_t val,
		int32_t val_2,
		Environment &e);


	// IDL Methods for replica::repl_prov methods
	void become_secondary(Environment &e);
	void add_secondary(
		replica::checkpoint_ptr sec,
		const char *sec_name,
		Environment &);
	void remove_secondary(
		const char *secondary_name,
		Environment &);
	void freeze_primary_prepare(Environment &e);
	void freeze_primary(Environment &e);
	void unfreeze_primary(Environment &e);
	void become_primary(
		const replica::repl_name_seq &secondary_names,
		Environment &);
	CORBA::Object_ptr get_root_obj(Environment &);
	void become_spare(Environment &);
	uint32_t forced_shutdown(Environment &);
	void shutdown(Environment &);

	void upgrade_callback(const version_manager::vp_version_t &v);

	// Miscelanous methods for service work
	void start_service(Environment &);

	// Debugging
	void dump_obj_list();

	// Interface to be invoked during ORB::join_cluster to update the
	// local ns of a rejoining node
	//
	static int get_root_to_local();

	// For ease of use we make the list a public member
	// The list of objects currently in use
	/* CSTYLED */
	SList<repl_value_obj_impl> value_objlist;
	/* CSTYLED */
	SList<repl_persist_obj_impl> persist_objlist;

	// Need a lock for the lists
	os::mutex_t	list_lock;

	factory_impl 	*factoryp;
	bool		initialized;

#ifdef _KERNEL
	//
	// Keep a list of all providers created.
	// This is so that the test module can wait until the
	// provider is unreferenced before unloading the module.
	//
	static SList<prov_in_mod_t>	repl_sample_prov_list;
	static os::mutex_t		prov_list_lock;
	prov_in_mod_t			*my_id;
#endif

	// Protected by version_lock (Will be).
	// version_manager::vp_version_t	r_version;

	// Checkpoint accessor function.
	repl_sample::ckpt_ptr	get_checkpoint_repl_sample();

private:
	char		*svc_nam;
	void		common_shutdown();

	// Disallow Copy constructor and assignment operator.
	repl_sample_prov(const repl_sample_prov &);
	repl_sample_prov& operator = (repl_sample_prov &);

	// Checkpoint proxy.
	repl_sample::ckpt_ptr	_ckpt_proxy;

	// Lock that protects read/update of the running version.
	os::rwlock_t	version_lock;

	// Current running version
	version_manager::vp_version_t	r_version;

	// Highest version we support. This has to be hard coded and
	// can be initialized during construct time.
	version_manager::vp_version_t	highest_version;

	// The name of vp that we are associated with.
	char 	*vp_name;

	// Update running_version and register upgrade_callbacks if necessary
	void register_upgrade_callbacks();

	bool is_primary;
};

// The volatile object
class repl_value_obj_impl :
	/* CSTYLED */
	public mc_replica_of<repl_sample::value_obj> {
public:
	// Constructor for primary
	repl_value_obj_impl(repl_sample_prov *server):
	/* CSTYLED */
	    mc_replica_of<repl_sample::value_obj>
	(server), value(0), value_2(0), my_provider(server) { }

	// Constructor for secondary
	repl_value_obj_impl(repl_sample::value_obj_ptr ref,
			    repl_sample_prov *server) :
		mc_replica_of<repl_sample::value_obj> (ref),
		value(0), value_2(0), my_provider(server) {}

	// IDL Implementations
	int32_t get_value(int16_t &nodenum, Environment &e);
	void set_value(int32_t val, int16_t &nodenum, Environment &e);
	int32_t inc_value(int16_t &nodenum, Environment &e);

	int32_t get_value_2(int16_t &nodenum, Environment &e);
	void set_value_2(int32_t val, int16_t &nodenum, Environment &e);
	int32_t inc_value_2(int16_t &nodenum, Environment &e);

	// unreferenced
	void _unreferenced(unref_t);

	// Implementation method for updating value and value_2
	void _value(int32_t val) { value = val; }
	int32_t _value() { return (value); }

	void _value_2(int32_t val) { value_2 = val; }
	int32_t _value_2() { return (value_2); }

	// Checkpoint accessor function.
	repl_sample::ckpt_ptr	get_checkpoint();

private:
	// The provider for which we are an object of
	repl_sample_prov	*my_provider;

	int32_t value;
	int32_t value_2;

	// A lock to protect value
	os::mutex_t	lck;
};

// The persistant store interface
class persistent_store_impl :
	/* CSTYLED */
	public McServerof<repl_sample::persistent_store> {
public:
	// Constructor
	/* CSTYLED */
	persistent_store_impl() : McServerof<repl_sample::persistent_store>(),
		value(0) { }

	// IDL Members
	void set_value(int32_t val, Environment &);
	int32_t get_value(Environment &) { return (value); }

//	void set_value_2(int32_t val, Environment &);
//	int32_t get_value_2(Environment &) { return (value_2); }

	// unreferenced
	void _unreferenced(unref_t);
private:
	int32_t value;
//	int32_t value_2;
};

// The persistent object
class repl_persist_obj_impl :
	/* CSTYLED */
	public mc_replica_of<repl_sample::persist_obj> {
public:
	// Constructor for primary
	repl_persist_obj_impl(repl_sample_prov * server);

	// Constructor for secondary
	repl_persist_obj_impl(repl_sample::persist_obj_ptr ref,
	    repl_sample_prov *server);

	// IDL Implementations
	int32_t get_value(int16_t &, Environment &);
	void set_value(int32_t, int16_t &, Environment &);
	int32_t inc_value(int16_t &, Environment &);

	int32_t get_value_2(int16_t &, Environment &);
	void set_value_2(int32_t, int16_t &, Environment &);
	int32_t inc_value_2(int16_t &, Environment &);

	// unreferenced
	void _unreferenced(unref_t);

	// Implementation interface for cache of value
	int32_t _value() { return (value); }
	void _value(int32_t val) { value = val; }

	int32_t _value_2() { return (value_2); }
	void _value_2(int32_t val) { value_2 = val; }

	// Implementation interface to the persistent store
	int32_t _persist_value(Environment &e)
		{ return (storage->get_value(e)); }
	void _persist_value(int32_t val, Environment &e)
		{ storage->set_value(val, e); }
/*
	int32_t _persist_value_2(Environment &e)
		{ return (storage->get_value_2(e)); }
	void _persist_value_2(int32_t val, Environment &e)
		{ storage->set_value_2(val, e); }
*/
	// Implementation interface to set the inc value
	void _set_inc_value(int32_t, Environment &);
	void _set_inc_value_2(int32_t, Environment &);

	// Implementation interface to signal a failed checkpoint
	void _ckpt_inc_failure(Environment &);
	void _ckpt_inc_2_failure(Environment &);

	// Implementation interface to install the persistent
	// store ref (storage) by getting it from the local
	// name server
	void update_storage(Environment &);

	// Checkpoint accessor function.
	repl_sample::ckpt_ptr	get_checkpoint();

private:
	// The provider for which we are an object of
	repl_sample_prov	*my_provider;

	int32_t					value;
	int32_t					value_2;
	repl_sample::persistent_store_var	storage;
	os::mutex_t				lck;
};

// The Factory
class factory_impl :
	/* CSTYLED */
	public mc_replica_of <repl_sample::factory> {
public:
	// Primary constructor
	factory_impl(repl_sample_prov * server) :
	    /* CSTYLED */
	    mc_replica_of<repl_sample::factory>(server),
	    my_provider(server)
		{ }

	// Secondary Constructor
	factory_impl(repl_sample::factory_ptr ref, repl_sample_prov *server) :
	    /* CSTYLED */
	    mc_replica_of<repl_sample::factory>(ref),
	    my_provider(server)
		{ }

	// IDL Implmentations
	repl_sample::value_obj_ptr create_value_obj(Environment &e);
/*
	repl_sample::value_obj_a_ptr create_value_obj_a(Environment &e);
	repl_sample::value_obj_b_ptr create_value_obj_b(Environment &e);
	repl_sample::maj_value_obj_ptr create_maj_value_obj(Environment &e);
	repl_sample::combined_value_obj_ptr create_combined_value_obj(
	    Environment &e);
*/
	repl_sample::persist_obj_ptr create_persist_obj(
	    Environment &e);

	// unreferenced
	void _unreferenced(unref_t);

	// Checkpoint accessor function.
	repl_sample::ckpt_ptr	get_checkpoint();
private:
	// The provider for which we are a factory of
	repl_sample_prov	*my_provider;

};

class repl_sample_callback :
    public McServerof<version_manager::upgrade_callback> {
public:
	repl_sample_callback(repl_sample_prov *prov);
	~repl_sample_callback();

	//
	// The callback object can go away once all the associated uccs
	// have unregistered.
	//
	void _unreferenced(unref_t cookie);

	//
	// Actual callback method which will be invoked by the callback
	// framework during upgrade_commit.
	//
	void do_callback(const char *ucc_name,
	    const version_manager::vp_version_t &new_version,
	    Environment &e);

private:
	// Pointer to the actual object that needs to be upgraded.
	repl_sample_prov *my_provider;
};

#endif	/* _REPL_SAMPLE_IMPL_V1_H */
