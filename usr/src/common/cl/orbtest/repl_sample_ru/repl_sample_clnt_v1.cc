/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)repl_sample_clnt_v1.cc	1.7	08/05/20 SMI"

#include 	<sys/os.h>
#include	<h/naming.h>
#include	<orb/invo/common.h>
#include	<sys/rm_util.h>
#include	<nslib/ns.h>

#ifdef _KERNEL_ORB
#include	<orbtest/repl_sample_ru/repl_sample_impl_v1.h>
#include	<sys/modctl.h>
#endif

#include	<orbtest/repl_tests/repl_test_switch.h>
#include	<orbtest/rm_probe/rm_snap.h>
#include	<h/replica.h>
#include	<orb/fault/fault_injection.h>

#include	<orbtest/repl_sample_ru/repl_sample_clnt_v1.h>


// Forward Definitions
repl_sample::factory_ptr	get_factory(const char *factory_name);
repl_sample::value_obj_ptr	get_value_obj(char *svc_name);
repl_sample::persist_obj_ptr	get_persist_obj(char *svc_name);

//
// simple_mini_1 - execute a case of a simple mini transaction.
//
// Simple minitransactions are
//	1. idempotent,
//	2. have no state, and
//	3. make no updates.
//
int
simple_mini_1(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void		*argp,
	int		arglen,
	Environment	&e,
	char		*svc_name,
	bool		do_setup_work)
{
	os::printf("+++ repl_sample_clnt_v1 client +++\n");

	//
	// Before doing the simple mini-transaction, sometimes
	// we want to make the service do some work.
	//
	if (do_setup_work) {
		if (do_work(svc_name)) {
			return (1);
		}
	}
	const int32_t	EXPECTED_VALUE = 10;

	// Create a reference to a value_obj, and initialize it to 10
	repl_sample::value_obj_ptr	obj_ref_v = init_value_obj(svc_name,
	    EXPECTED_VALUE);
	if (CORBA::is_nil(obj_ref_v)) {
		os::printf("+++ FAIL: Could not get reference to "
		    "repl_sample::value_obj\n");
		return (1);
	}

	// Get the version number of the object reference
	int v_num = repl_sample::value_obj::_version(obj_ref_v);
	os::printf("+++ v_num = %d +++\n", v_num);

	// Arm the fault points needed for this test.
	if (on_fp != NULL) {
		if (on_fp(argp, arglen) != 0) {
			os::printf("    WARNING: Arming faults failed\n");
		}
	} else {
		os::printf("    No Faults armed.\n");
	}

	//
	// The Simple Mini-Transaction
	//
	int16_t	nodenum = 0;

	int32_t	value = obj_ref_v->get_value(nodenum, e);

	int32_t value_2 = -99;

	os::printf("+++ value %d value_2 %d +++\n", value, value_2);

	if (v_num == 1) {
		value_2 = obj_ref_v->get_value_2(nodenum, e);
	}

	os::printf("+++ value %d value_2 %d +++\n", value, value_2);

	// Disarm fault points for this test as needed.
	if (off_fp != NULL) {
		if (off_fp(argp, arglen) != 0) {
			os::printf("    WARNING: Disarming faults failed\n");
		}
	}
	if (e.exception()) {
		e.exception()->print_exception(
		    "repl_sample::value_obj::get_value()");
		CORBA::release(obj_ref_v);
		return (1);
	}

	// Compare the invocation result value with the expected value.
	if (value == EXPECTED_VALUE) {
		os::printf("--- PASS: Value returned was: %d from node %d\n",
		    value, nodenum);
	} else {
		os::printf("+++ FAIL: Value returned was: %d from node %d\n",
		    value, nodenum);
		CORBA::release(obj_ref_v);
		return (1);
	}

	if (v_num == 1) {
	    if (value_2 == (EXPECTED_VALUE+100)) {
		os::printf("--- PASS: Value_2 returned was: %d from node %d\n",
		    value_2, nodenum);
	    } else {
		os::printf("+++ FAIL: Value_2 returned was: %d from node %d\n",
		    value_2, nodenum);
		CORBA::release(obj_ref_v);
		return (1);
	    }
	}

	CORBA::release(obj_ref_v);
	return (0);
}

//
// This is the case of a 1-phase mini-transaction
//
int
one_phase_mini_1(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void *argp,
	int arglen,
	Environment &e,
	char *svc_name,
	bool do_setup_work)
{
	os::printf("+++ repl_sample_clnt_v1 client +++\n");

	// Before we do one_phase_mini_1 we want to make the service do
	// some work!
	if (do_setup_work) {
		if (do_work(svc_name))
			return (1);
	}

	// One phase mini transactions update some state.
	// There is only one checkpoint needed.
	// Could be idempotent, or not.

	const int32_t EXPECTED_VALUE = 20;

	// Create a reference to a value_obj, and initialize it to some value
	repl_sample::value_obj_ptr obj_ref = init_value_obj(svc_name, 0);
	if (CORBA::is_nil(obj_ref)) {
		os::printf("+++ FAIL: Could not get reference to "
			"repl_sample::value_obj\n");
		return (1);
	}

	// Get the version number of the object reference
	int v_num = repl_sample::value_obj::_version(obj_ref);
	os::printf("+++ v_num = %d +++\n", v_num);

	// Do the setup
	if (on_fp != NULL) {
		if (on_fp(argp, arglen) != 0)
			os::printf("    WARNING: Arming faults failed\n");
	} else {
		os::printf("    No Faults armed.\n");
	}

	//
	// The one-phase mini-transaction (idempotent)
	//
	int16_t		nodenum;
	obj_ref->set_value(EXPECTED_VALUE, nodenum, e);

	if (v_num == 1) {
		obj_ref->set_value_2(EXPECTED_VALUE+100, nodenum, e);
	}

	// Disarm faults
	if (off_fp != NULL)
		if (off_fp(argp, arglen) != 0)
			os::printf("    WARNING: Disarming faults failed\n");

	if (e.exception()) {
		e.exception()->print_exception
			("repl_sample::value_obj::set_value()");
		CORBA::release(obj_ref);
		return (1);
	}

	//
	// Verification
	//
	Environment	e2;
	int16_t		nodenum2;

	int32_t value = obj_ref->get_value(nodenum2, e2);
	if (e2.exception()) {
		e2.exception()->print_exception
			("repl_sample::value_obj::get_value()");
		CORBA::release(obj_ref);
		return (1);
	}

	// Compare to the expected value
	if (value == EXPECTED_VALUE) {
		os::printf("--- PASS: Value returned was: %d from node %d\n",
			value, nodenum);
	} else {
		os::printf("+++ FAIL: Value returned was: %d from node %d\n",
			value, nodenum);
		CORBA::release(obj_ref);
		return (1);
	}

	if (v_num == 1) {
	    int32_t value_2 = obj_ref->get_value_2(nodenum2, e2);
	    if (e2.exception()) {
		e2.exception()->print_exception
			("repl_sample::value_obj::get_value_2()");
		CORBA::release(obj_ref);
		return (1);
	    }

	    // Compare to the expected value
	    if (value_2 == EXPECTED_VALUE+100) {
		os::printf("--- PASS: Value_2 returned was: %d from node %d\n",
			value_2, nodenum);
	    } else {
		os::printf("+++ FAIL: Value_2 returned was: %d from node %d\n",
			value_2, nodenum);
		CORBA::release(obj_ref);
		return (1);
	    }
	}

	CORBA::release(obj_ref);
	return (0);
}

//
// This is the case of a 1-phase mini-transaction
//
int
one_phase_mini_2(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void *argp,
	int arglen,
	Environment &e,
	char *svc_name,
	bool do_setup_work)
{
	os::printf("+++ repl_sample_clnt_v1 client +++\n");

	// Before we do one_phase_mini_2 we want to make the service do
	// some work!
	if (do_setup_work) {
		if (do_work(svc_name))
			return (1);
	}

	// One phase mini-transaction (non-idempotent)
	const int32_t EXPECTED_VALUE = 21;

	repl_sample::value_obj_ptr obj_ref = init_value_obj(svc_name,
	    EXPECTED_VALUE - 1);
	if (CORBA::is_nil(obj_ref)) {
		os::printf("+++ FAIL: could not get reference to "
			"repl_sample::value_obj\n");
		return (1);
	}

	// Get the version number of the object reference
	int v_num = repl_sample::value_obj::_version(obj_ref);
	os::printf("+++ v_num = %d +++\n", v_num);

	// Do the setup
	if (on_fp != NULL) {
		if (on_fp(argp, arglen) != 0)
			os::printf("    WARNING: Arming faults failed\n");
	} else {
		os::printf("    No Faults armed.\n");
	}

	//
	// The one phase mini (non-indempotent)
	//
	int16_t		nodenum;
	(void) obj_ref->inc_value(nodenum, e);

	if (v_num == 1) {
	    (void) obj_ref->inc_value_2(nodenum, e);
	}

	// Disarm faults
	if (off_fp != NULL)
		if (off_fp(argp, arglen) != 0)
			os::printf("    WARNING: Disarming faults failed\n");

	if (e.exception()) {
		e.exception()->print_exception
			("repl_sample::value_obj::inc_value()");
		CORBA::release(obj_ref);
		return (1);
	}

	//
	// Verification
	//
	Environment	e2;
	int16_t		nodenum2;

	int32_t value = obj_ref->get_value(nodenum2, e2);
	if (e2.exception()) {
		e2.exception()->print_exception
			("repl_sample::value_obj::get_value()");
		CORBA::release(obj_ref);
		return (1);
	}

	// Compare to the expected value
	if (value == EXPECTED_VALUE) {
		os::printf("--- PASS: Value returned was: %d from node %d\n",
			value, nodenum);
	} else {
		os::printf("+++ FAIL: Value returned was: %d from node %d\n",
			value, nodenum);
		CORBA::release(obj_ref);
		return (1);
	}

	if (v_num == 1) {
	    int32_t value_2 = obj_ref->get_value_2(nodenum2, e2);
	    if (e2.exception()) {
		e2.exception()->print_exception
			("repl_sample::value_obj::get_value_2()");
		CORBA::release(obj_ref);
		return (1);
	    }

	    // Compare to the expected value
	    if (value_2 == EXPECTED_VALUE+100) {
		os::printf("--- PASS: Value_2 returned was: %d from node %d\n",
			value_2, nodenum);
	    } else {
		os::printf("+++ FAIL: Value_2 returned was: %d from node %d\n",
			value_2, nodenum);
		CORBA::release(obj_ref);
		return (1);
	    }
	}

	CORBA::release(obj_ref);
	return (0);
}


//
// This is the case of a 2-phase mini-transaction
// This client is single threaded.
//
int
two_phase_mini_1(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void *argp,
	int arglen,
	Environment &e,
	char *svc_name,
	bool do_setup_work)
{
	os::printf("+++ repl_sample_clnt_v1 client +++\n");

	// Before we do two_phase_mini_1 we want to make the service
	// do some work!
	if (do_setup_work) {
		if (do_work(svc_name))
			return (1);
	}

	// persist_obj::inc_value
	const int32_t EXPECTED_VALUE = 31;

	// Create a reference to a persist_obj, and initialize it to some value
	repl_sample::persist_obj_ptr obj_ref = init_persist_obj(svc_name,
	    EXPECTED_VALUE - 1);
	if (CORBA::is_nil(obj_ref)) {
		os::printf("ERROR: Could not get reference to "
			"repl_sample::persist_obj\n");
		os::printf("**NOTE** If persist_store_obj was located on "
			"one of the nodes that got rebooted, this error is "
			"expected. If not the case, it is a FAILURE.\n");
		return (0);
	}

	// Get the version number of the object reference
	int v_num = repl_sample::persist_obj::_version(obj_ref);
	os::printf("+++ v_num = %d +++\n", v_num);

	// Do the setup
	if (on_fp != NULL) {
		if (on_fp(argp, arglen) != 0)
			os::printf("    WARNING: Arming faults failed\n");
	} else {
		os::printf("    No Faults armed.\n");
	}

	//
	// The two-phase mini-transaction
	//
	int16_t		nodenum;
	(void) obj_ref->inc_value(nodenum, e);

	if (v_num == 1) {
	    (void) obj_ref->inc_value_2(nodenum, e);
	}

	// Disarm faults
	if (off_fp != NULL)
		if (off_fp(argp, arglen) != 0)
			os::printf("    WARNING: Disarming faults failed\n");

	if (e.exception()) {
		e.exception()->print_exception
			("repl_sample::persist_obj::inc_value()");
		CORBA::release(obj_ref);
		return (1);
	}

	//
	// Verification
	//
	int16_t		nodenum2;

	int32_t value = obj_ref->get_value(nodenum2, e);
	if (e.exception()) {
		e.exception()->print_exception
			("repl_sample::persist_obj::get_value()");
		CORBA::release(obj_ref);
		return (1);
	}

	// Compare to the expected value
	if (value == EXPECTED_VALUE) {
		os::printf("--- PASS: Value returned was: %d from node %d\n",
			value, nodenum);
	} else {
		os::printf("+++ FAIL: Value returned was: %d from node %d\n",
			value, nodenum);
		CORBA::release(obj_ref);
		return (1);
	}

	if (v_num == 1) {
	    int32_t value_2 = obj_ref->get_value_2(nodenum2, e);
	    if (e.exception()) {
		e.exception()->print_exception
			("repl_sample::persist_obj::get_value_2()");
		CORBA::release(obj_ref);
		return (1);
	    }

	    // Compare to the expected value
	    if (value_2 == EXPECTED_VALUE+100) {
		os::printf("--- PASS: Value_2 returned was: %d from node %d\n",
			value_2, nodenum);
	    } else {
		os::printf("+++ FAIL: Value_2 returned was: %d from node %d\n",
			value_2, nodenum);
		CORBA::release(obj_ref);
		return (1);
	    }
	}

	CORBA::release(obj_ref);
	return (0);
}

//
// HELPER ROUTINES
//

// Retrieve the factory from the nameserver
repl_sample::factory_ptr
get_factory(const char *factory_name)
{
	os::printf("get_factory()\n");

	Environment e;
	naming::naming_context_var ctxp = ns::root_nameserver();
	CORBA::Object_var obj_ref = ctxp->resolve(factory_name, e);
	if (e.exception()) {
		e.exception()->print_exception("get_factory()");
		return (nil);
	}

	// Make this a factory ptr, so we can return the new reference that
	// narrow returns.  (And we dont have to do a _duplicate)
	repl_sample::factory_ptr fp = repl_sample::factory::_narrow(obj_ref);

	ASSERT(!CORBA::is_nil(fp));

	return (fp);
}

// Create a value_obj
// Must get a factory, and then get a value_obj
repl_sample::value_obj_ptr
get_value_obj(char *svc_name)
{
	os::printf("get_value_obj()\n");

	Environment e;
	// Get a factory
	char	factory_name[50];
	os::sprintf(factory_name, "%s factory", svc_name);
	repl_sample::factory_var factory_ref = get_factory(factory_name);
	if (CORBA::is_nil(factory_ref))
		return (nil);

	// Create a value object
	repl_sample::value_obj_ptr objp = factory_ref->create_value_obj(e);
	if (e.exception()) {
		e.exception()->print_exception("get_value_obj()");
		return (nil);
	}

	return (objp);
}

// Create a persist_obj
// Must get a factory, and then get a persist_obj
repl_sample::persist_obj_ptr
get_persist_obj(char *svc_name)
{
	os::printf("get_persist_obj()\n");

	Environment e;
	// get a factory
	char	factory_name[50];
	os::sprintf(factory_name, "%s factory", svc_name);
	repl_sample::factory_var factory_ref = get_factory(factory_name);
	if (CORBA::is_nil(factory_ref))
		return (nil);

	// Create a value object
	repl_sample::persist_obj_ptr objp = factory_ref->create_persist_obj(e);
	if (e.exception()) {
		e.exception()->print_exception("get_persist_obj()");
		return (nil);
	}

	return (objp);
}

// Will create and initalize a value_obj to the passed in val
repl_sample::value_obj_ptr
init_value_obj(char *svc_name, int32_t val)
{
	os::printf("init_value_obj(%d)\n", val);

	Environment e;
	// Get a value object
	repl_sample::value_obj_ptr objp = get_value_obj(svc_name);
	if (CORBA::is_nil(objp)) {
		return (nil);
	}

	// Get the version number of the object reference
	int v_num = repl_sample::value_obj::_version(objp);

	os::printf("init_value_obj : v_num %d\n", v_num);
	os::printf("init_value_obj : val %d\n", val);

	// Set its value
	int16_t tmpval;
	objp->set_value(val, tmpval, e);
	if (e.exception()) {
		// e.exception()->print_exception("init_value_obj()");
		e.exception()->print_exception("init_value_obj() set_value");
		return (nil);
	}

	os::printf("init_value_obj : 1 : val %d, tmpval %d\n", val, tmpval);

	if (v_num == 1) {

	    os::printf("init_value_obj : 2 : val %d, tmpval %d\n", val, tmpval);

	    objp->set_value_2(val+100, tmpval, e);
	    if (e.exception()) {
		// e.exception()->print_exception("init_value_obj()");
		e.exception()->print_exception("init_value_obj() set_value_2");
		return (nil);
	    }
	}

	return (objp);
}

// Will create and initalize a persist_obj to the passed in val
repl_sample::persist_obj_ptr
init_persist_obj(char *svc_name, int32_t val)
{
	os::printf("init_persist_obj(%d)\n", val);

	Environment e;
	// Get a value object
	repl_sample::persist_obj_ptr objp = get_persist_obj(svc_name);
	if (CORBA::is_nil(objp)) {
		return (nil);
	}

	// Get the version number of the object reference
	int v_num = repl_sample::persist_obj::_version(objp);

	os::printf("init_persist_obj : v_num %d\n", v_num);
	os::printf("init_persist_obj : val %d\n", val);

	// Set its value
	int16_t tmpval;
	objp->set_value(val, tmpval, e);
	if (e.exception()) {
		// e.exception()->print_exception("init_persist_obj()");
		e.exception()->print_exception("init_persist_obj() set_value");
		return (nil);
	}

	os::printf("init_persist_obj : 1 : val %d, tmpval %d\n", val, tmpval);

	if (v_num == 1) {

	    os::printf("init_persist_obj : 2 : val %d, tmpval %d\n",
		val, tmpval);

	    objp->set_value_2(val+100, tmpval, e);
	    if (e.exception()) {
		// e.exception()->print_exception("init_persist_obj()");
		e.exception()->print_exception(
		    "init_persist_obj() set_value_2");
		return (nil);
	    }
	}

	return (objp);
}

//
// Change the state of the 1st secondary node
//
// Note that we check the location of the HA-RM
// If the location of the HA-RM changes during the test
// that means that it failed over, and the exception
// replica::invalid_repl_prov is expected.
//
int
change_sec_state(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void			*argp,
	int			arglen,
	char			*service_name,
	replica::change_op	new_state,
	Environment		&e,
	bool			do_setup_work)
{
	//
	// Before we do change_sec_state we want to make the service do some
	// work!
	//
	if (do_work(service_name)) {
		return (1);
	}
	ASSERT(service_name != NULL);

	// Find the secondary node
	rm_snap		_rma_snapshot;
	nodeid_t	_secondary_nodeid;

	if (!_rma_snapshot.refresh(service_name)) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	// Get the first secondary
	if (_rma_snapshot.get_secondary(1, _secondary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not find secondary nodeid\n");
		return (1);
	}

	char	action_str[4][25] = { "SC_SET_PRIMARY", "SC_SET_SPARE",
	    "SC_SET_SECONDARY", "SC_REMOVE_REPL_PROV" };

	os::printf(" ** Change state of prov in node %d with action %s\n",
	    _secondary_nodeid, action_str[new_state]);

	char	local_buf[10];
	os::sprintf(local_buf, "%d", _secondary_nodeid);

	// Set up the switchover struct
	struct opt_t	_opt;
	int		rslt;

	_opt.new_state = new_state;
	_opt.prov_desc = local_buf;
	_opt.service_desc = service_name;

	//
	// Find the location of the HA-RM prior to testing
	// KERNEL ONLY
	//
#ifdef _FAULT_INJECTION
	nodeid_t	primary_rm_host;
	primary_rm_host = FaultFunctions::rm_primary_node();
#endif

	// Do the fault setup
	if (on_fp != NULL) {
		if (on_fp(argp, arglen) != 0) {
			os::printf("    WARNING: Arming faults failed\n");
		}
	} else {
		os::printf("    No Faults armed.\n");
	}

	//
	// Do the switchover
	//
	rslt = trigger_switchover(&_opt, e);

	// Disarm faults
	if (off_fp != NULL) {
		if (off_fp(argp, arglen) != 0) {
			os::printf("    WARNING: Disarming faults failed\n");
		}
	}
	//
	// Check for exceptions from trigger_switchover
	//
#ifdef _FAULT_INJECTION
	if (primary_rm_host != FaultFunctions::rm_primary_node()) {
		// the only acceptable exception should be invalid_repl_prov
		if (e.exception()) {
			// If the exception is replica::invalid_repl_prov
			// then
			if ((replica::invalid_repl_prov::_exnarrow(
			    e.exception())) ||
			    (replica::service_admin::service_changed::_exnarrow(
			    e.exception())) ||
			    (replica::invalid_repl_prov_state::_exnarrow(
			    e.exception()))) {
				// Got an invalid_repl_prov exception
				os::warning("received replica"
				    " user exception, since HA-RM "
				    "was failed over, will ignore exception\n");
				e.clear();
				rslt = 0;
			}
			// Otherwise let the exception fall through
		}
	}
#endif
	//
	// There should be no exception
	// Let each test case print the error though in case they did
	// expect an exception
	//
	if (e.exception()) {
		e.exception()->print_exception("trigger_switchover");
		return (rslt);
	}

	// Verify that we can do work after the switchover
	if (do_setup_work) {
		if (do_work(service_name)) {
			return (1);
		}
	}

	return (rslt);
}

//
// Change the state of the primary provider
//
int
change_primary_state(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void			*argp,
	int			arglen,
	char			*service_name,
	replica::change_op	new_state,
	Environment		&e,
	bool			do_setup_work)
{
	//
	// Before we do change_primary_state we want to make the service do some
	// work!
	//
	if (do_work(service_name)) {
		return (1);
	}
	ASSERT(service_name != NULL);

	// Find the primary node
	rm_snap		_rma_snapshot;
	nodeid_t	_primary_nodeid;

	if (!_rma_snapshot.refresh(service_name)) {
		os::printf("+++ FAIL: Could not get rma snapshot\n");
		return (1);
	}

	// Get the primary
	if (_rma_snapshot.get_primary(_primary_nodeid) != 0) {
		os::printf("+++ FAIL: Could not find primary nodeid\n");
		return (1);
	}

	char	action_str[4][25] = { "SC_SET_PRIMARY", "SC_SET_SPARE",
	    "SC_SET_SECONDARY", "SC_REMOVE_REPL_PROV" };

	os::printf(" ** Change state of prov in node %d with action %s\n",
	    _primary_nodeid, action_str[new_state]);

	char	local_buf[10];
	os::sprintf(local_buf, "%d", _primary_nodeid);

	// Set up the switchover struct
	struct opt_t	_opt;
	int		rslt;

	_opt.new_state = new_state;
	_opt.prov_desc = local_buf;
	_opt.service_desc = service_name;

	// Do the fault setup
	if (on_fp != NULL) {
		if (on_fp(argp, arglen) != 0) {
			os::printf("    WARNING: Arming faults failed\n");
		}
	} else {
		os::printf("    No Faults armed.\n");
	}


	//
	// Do the switchover
	//
	rslt = trigger_switchover(&_opt, e);

	// Disarm faults
	if (off_fp != NULL) {
		if (off_fp(argp, arglen) != 0) {
			os::printf("    WARNING: Disarming faults failed\n");
		}
	}
	if (e.exception()) {
		e.exception()->print_exception("trigger_switchover");
	}

	// Verify that we can do work after the switchover
	if (do_setup_work) {
		if (do_work(service_name)) {
			return (1);
		}
	}

	return (rslt);
}

//
// Change desired number of secondaries
//
int
change_desired_num_secs(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void			*argp,
	int			arglen,
	char			*service_name,
	uint32_t		num_secs,
	Environment		&e,
	bool			do_setup_work)
{

	ASSERT(service_name != NULL);

	//
	// Before we do change_sec_state we want to make the service do some
	// work!
	//
	if (do_work(service_name)) {
		return (1);
	}

	replica::rm_admin_var		rm_ref;
	replica::service_admin_var	svc_admin_ref;

	// Get reference to RM
	os::printf("Attempt to get reference to RM\n");
	rm_ref = rm_util::get_rm();

	if (CORBA::is_nil(rm_ref)) {
		os::printf("ERROR: Could not get rm.\n");
		return (-1);
	}
	os::printf("Success: Got reference to RM\n");

	// Get the service control
	os::printf("Attempt to get the service control\n");
	svc_admin_ref = get_control(service_name, rm_ref, e);
	if (e.exception()) {
		if (replica::unknown_service::_exnarrow(e.exception())
		    != NULL) {
			os::printf("ERROR: Service (%s) is unknown\n",
			    service_name);
		} else {
			e.exception()->print_exception("Exception error");
		}

		return (-1);
	}

	if (CORBA::is_nil(svc_admin_ref)) {
		os::printf("ERROR: Could not get service admin object.\n");
		return (-1);
	}
	os::printf("Success: Got the service control\n");

	// Do the fault setup
	if (on_fp != NULL) {
		if (on_fp(argp, arglen) != 0) {
			os::printf("    WARNING: Arming faults failed\n");
		}
		// Comment out later
		os::printf("    WARNING: Arming faults succeeded\n");
	} else {
		os::printf("    No Faults armed.\n");
	}

	os::printf("Attempt to change desired number of secondaries\n");
	svc_admin_ref->change_desired_numsecondaries(num_secs, e);

	// Disarm faults
	if (off_fp != NULL) {
		if (off_fp(argp, arglen) != 0) {
			os::printf("    WARNING: Disarming faults failed\n");
		}
		// Comment out later
		os::printf("    WARNING: Disarming faults succeeded\n");
	}

	if (e.exception()) {
		e.exception()->print_exception("Exception error");
		return (-1);
	}

	uint32_t 		curr_num_secs;
	replica::service_info   *svc_info = NULL;

	svc_info = svc_admin_ref->get_service_info(e);
	if (e.exception())
		return (0);

	// Store the desired number of secondaries after the change
	curr_num_secs = svc_info->desired_numsecondaries;

	if (curr_num_secs != (uint32_t)num_secs) {
		os::printf(" ERROR: Setting desired_numsecondaries failed\n");
		return (-1);
	}
	os::printf("Success: Setting desired_numsecondaries succeeded\n");

	// Verify that we can do work after the switchover
	if (do_setup_work) {
		if (do_work(service_name)) {
			return (1);
		}
	}

	return (0);
}

//
// Change the priority of service providers
//
int
change_repl_prov_priority(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void				*argp,
	int				arglen,
	char				*service_name,
	replica::prov_priority_list	*prov_list,
	Environment			&e,
	bool				do_setup_work)
{

	ASSERT(service_name != NULL);

	//
	// Before we do change_sec_state we want to make the service do some
	// work!
	//
	if (do_work(service_name)) {
		return (1);
	}

	replica::rm_admin_var		rm_ref;
	replica::service_admin_var	svc_admin_ref;

	// Get reference to RM
	os::printf("Attempt to get reference to RM\n");
	rm_ref = rm_util::get_rm();

	if (CORBA::is_nil(rm_ref)) {
		os::printf("ERROR: Could not get rm.\n");
		return (-1);
	}
	os::printf("Success: Got reference to RM\n");

	// Get the service control
	os::printf("Attempt to get the service control\n");
	svc_admin_ref = get_control(service_name, rm_ref, e);
	if (e.exception()) {
		if (replica::unknown_service::_exnarrow(e.exception())
		    != NULL) {
			os::printf("ERROR: Service (%s) is unknown\n",
			    service_name);
		} else {
			e.exception()->print_exception("Exception error");
		}

		return (-1);
	}

	if (CORBA::is_nil(svc_admin_ref)) {
		os::printf("ERROR: Could not get service admin object.\n");
		return (-1);
	}
	os::printf("Success: Got the service control\n");

	// Do the fault setup
	if (on_fp != NULL) {
		if (on_fp(argp, arglen) != 0) {
			os::printf("    WARNING: Arming faults failed\n");
		}
		// Comment out later
		os::printf("    WARNING: Arming faults succeeded\n");
	} else {
		os::printf("    No Faults armed.\n");
	}

	os::printf("Attempt to change the priority of providers\n");
	svc_admin_ref->change_repl_prov_priority(*prov_list, e);

	// Disarm faults
	if (off_fp != NULL) {
		if (off_fp(argp, arglen) != 0) {
			os::printf("    WARNING: Disarming faults failed\n");
		}
		// Comment out later
		os::printf("    WARNING: Disarming faults succeeded\n");
	}

	if (e.exception()) {
		if (replica::invalid_repl_prov::_exnarrow(e.exception())
		    != NULL) {
			// invalid_repl_prov
			os::printf("ERROR: Replica(s) does not "
			    "match a registered replica for this "
			    "service\n");

		} else if (
		    replica::service_admin::service_changed::_exnarrow(
		    e.exception()) != NULL) {
			// service_changed
			os::printf("ERROR: Service changed state "
			    "during operation, verify state before "
			    "retrying operation\n");

		} else {
			e.exception()->print_exception("Exception error");
		}
		return (-1);

	} else {
		os::printf("priority change request was made.\n");
	}


	// Verify that we can do work after the switchover
	if (do_setup_work) {
		if (do_work(service_name)) {
			return (1);
		}
	}

	return (0);
}

//
// Switch the state of provider
//
int
switchover_prov_state(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void		*argp,
	int		arglen,
	struct opt_t	*p_opt,
	Environment	&e,
	bool		do_setup_work)
{
	int 	rslt;

	//
	// Before we do change_sec_state we want to make the service do some
	// work!
	//
	if (do_work(p_opt->service_desc)) {
		return (1);
	}

	// Do the fault setup
	if (on_fp != NULL) {
		if (on_fp(argp, arglen) != 0) {
			os::printf("    WARNING: Arming faults failed\n");
		}
		// Comment out later
		os::printf("    WARNING: Arming faults succeeded\n");
	} else {
		os::printf("    No Faults armed.\n");
	}

	os::printf("Attempt to switch the state of provider\n");
	rslt = trigger_switchover(p_opt);

	// Disarm faults
	if (off_fp != NULL) {
		if (off_fp(argp, arglen) != 0) {
			os::printf("    WARNING: Disarming faults failed\n");
		}
		// Comment out later
		os::printf("    WARNING: Disarming faults succeeded\n");
	}

	if (e.exception()) {
		e.exception()->print_exception("Exception error");
		return (-1);
	}

	if (rslt != 0)
		return (-1);

	// Verify that we can do work after the switchover
	if (do_setup_work) {
		if (do_work(p_opt->service_desc)) {
			return (1);
		}
	}

	return (0);
}

//
// Change the state of the 1st secondary node
//
int
shutdown_service(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void			*argp,
	int			arglen,
	char			*service_name,
	Environment		&e,
	bool			is_forced_shutdown)
{
	ASSERT(service_name != NULL);

	os::printf("*** Shutdown Service Client\n");
	os::printf(" ** Will shutdown the service: %s\n", service_name);

	// Do some work before shutdown is called
	if (do_work(service_name))
		return (1);

	// get rma
	replica::rm_admin_var rm_ref = rm_util::get_rm();

	if (CORBA::is_nil(rm_ref)) {
		os::printf("+++ FAIL: could not narrow object to rm\n");
		return (1);
	}

	// get control
	replica::service_admin_var	svc_admin_ref;
	svc_admin_ref = rm_ref->get_control(service_name, e);
	if (e.exception()) {
		e.exception()->print_exception("get_control");
		os::printf("+++ FAIL: Could not get control of service (%s)\n",
		    service_name);
		return (1);
	}
	if (CORBA::is_nil(svc_admin_ref)) {
		os::printf("+++ FAIL: Could not get control of service (%s)\n",
		    service_name);
		return (1);
	}

	// Do the fault setup
	if (on_fp != NULL) {
		if (on_fp(argp, arglen) != 0)
			os::printf("    WARNING: Arming faults failed\n");
	} else {
		os::printf("    No Faults armed.\n");
	}

	// shutdown service
	svc_admin_ref->shutdown_service(is_forced_shutdown, e);

	// Disarm faults
	if (off_fp != NULL)
		if (off_fp(argp, arglen) != 0)
			os::printf("    WARNING: Disarming faults failed\n");

	return (0);
}

//
// shutdown_service_nowork
//
// Simply calls shutdown on the given service
//
int
shutdown_service_nowork(
	char			*service_name,
	Environment		&e,
	bool			is_forced_shutdown)
{
	os::printf("+++ Shutting down service %s\n", service_name);

	// get rma
	replica::rm_admin_var rm_ref = rm_util::get_rm();

	// get control
	replica::service_admin_var	svc_admin_ref;
	svc_admin_ref = rm_ref->get_control(service_name, e);
	if (e.exception()) {
		e.exception()->print_exception("get_control");
		os::printf("+++ FAIL: Could not get control of service (%s)\n",
		    service_name);
		return (1);
	}
	if (CORBA::is_nil(svc_admin_ref)) {
		os::printf("+++ FAIL: Could not get control of service (%s)\n",
		    service_name);
		return (1);
	}
	// shutdown service
	svc_admin_ref->shutdown_service(is_forced_shutdown, e);

	if (e.exception()) {
		os::printf("+++ FAIL: shutdown_service_nowork failed for"
		    " service (%s)\n", service_name);
		return (1);
	}

	return (0);
}

#if defined(_KERNEL_ORB)
//
// Register provider
//
// This client will register a repl_prov
//
int
register_with_rm(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void		*argp,
	int 		arglen,
	char		*svc_desc,
	char		*prov_desc,
	Environment	&e,
	int		argc,
	char		**argv)

{
	ASSERT(svc_desc != NULL);
	ASSERT(prov_desc != NULL);
	repl_sample_prov	*prov_ptr	= NULL;

	os::printf("*** Register with RM: %s, %s\n", svc_desc, prov_desc);

	// The argc is a counter for the argv
	// The argv will contain service names for the dependencies of the prov
	// we're about to start
	replica::service_dependencies
		svc_deps((uint32_t)argc, (uint32_t)argc);
	replica::prov_dependencies
		prov_deps((uint32_t)argc, (uint32_t)argc);

	// Populate the dependencies
	for (int counter = 0; counter < argc; counter++) {
		ASSERT(argv[counter] != NULL);
		svc_deps[(uint32_t)counter] = (const char *)argv[counter];
		prov_deps[(uint32_t)counter].service =
		    (const char *)argv[counter];
		prov_deps[(uint32_t)counter].repl_prov_desc =
		    (const char *)prov_desc;
	}

	// Set up the provider
	prov_ptr = new repl_sample_prov((const char *)svc_desc,
	    (const char *)prov_desc);
	ASSERT(prov_ptr != NULL);

	// Do the fault setup
	if (on_fp != NULL) {
		if (on_fp(argp, arglen) != 0)
			os::printf("    WARNING: Arming faults failed\n");
	} else {
		os::printf("    No Faults armed.\n");
	}

	//
	// Register prov
	//
	prov_ptr->register_with_rm(1, &svc_deps, &prov_deps, true, e);

	// Disarm faults
	if (off_fp != NULL) {
		if (off_fp(argp, arglen) != 0)
			os::printf("    WARNING: Disarming faults failed\n");
	}

	// Examine the environment
	if (e.exception()) {
		e.exception()->print_exception
			("repl_sample::repl_prov::register_with_rm()");
		return (1);
	}

	return (0);
}

//
// disconnect_clnt
//
// Sets up faults points and makes invocation whose xdoor is not acked by
// node 1.
//
int
drop_tr_info(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void		*argp,
	int		arglen,
	Environment	&e,
	char		*svc_name,
	bool		do_setup_work)
{
	//
	// Before doing the simple mini-transaction, sometimes
	// we want to make the service do some work.
	//
	if (do_setup_work) {
		if (do_work(svc_name)) {
			return (1);
		}
	}
	const int32_t	EXPECTED_VALUE = 10;

	// Create a reference to a value_obj, and initialize it to 10
	repl_sample::value_obj_var	obj_ref_v = init_value_obj(svc_name,
	    EXPECTED_VALUE);
	if (CORBA::is_nil(obj_ref_v)) {
		os::printf("+++ FAIL: Could not get reference to "
		    "repl_sample::value_obj\n");
		return (1);
	}
	// Arm the fault points needed for this test.
	if (on_fp != NULL) {
		if (on_fp(argp, arglen) != 0) {
			os::printf("    WARNING: Arming faults failed\n");
		}
	} else {
		os::printf("    No Faults armed.\n");
	}
	//
	// Bind to the name server. This will checkpoint an object
	// reference to all nodes. Fault point to defer acks has been
	// set on node 2, so this objref will not be acked. So when
	// ha_sample_server is shutdown, disconnect_clients() will
	// block.
	//
	// Get the root name server.
	//
	naming::naming_context_var context = ns::root_nameserver();
	context->rebind("ha_sample_server", obj_ref_v, e);
	if (e.exception() != NULL) {
		os::printf("ERROR: can't bind ha_sample_server to"
		    " nameserver\n");
		return (1);
	}

	// Disarm fault points for this test as needed.
	if (off_fp != NULL) {
		if (off_fp(argp, arglen) != 0) {
			os::printf("    WARNING: Disarming faults failed\n");
		}
	}

	return (0);
}

//
// kill_node
//
//   Sets up fault point and makes invocation that causes node 2 to
//   exit.
//
int
kill_node(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void		*argp,
	int		arglen,
	Environment	&e,
	char		*svc_name,
	bool		do_setup_work)
{
	//
	// Before doing the simple mini-transaction, sometimes
	// we want to make the service do some work.
	//
	if (do_setup_work) {
		if (do_work(svc_name)) {
			return (1);
		}
	}

	const int32_t	EXPECTED_VALUE = 10;

	// Create a reference to a value_obj, and initialize it to 10
	repl_sample::value_obj_var	obj_ref_v = init_value_obj(svc_name,
	    EXPECTED_VALUE);
	if (CORBA::is_nil(obj_ref_v)) {
		os::printf("+++ FAIL: Could not get reference to "
		    "repl_sample::value_obj\n");
		return (1);
	}

	// Arm the fault points needed for this test.
	if (on_fp != NULL) {
		if (on_fp(argp, arglen) != 0) {
			os::printf("    WARNING: Arming faults failed\n");
		}
	} else {
		os::printf("    No Faults armed.\n");
	}

	int16_t		nodenum = 0;
	int32_t		value = obj_ref_v->get_value(nodenum, e);

	// Disarm fault points for this test as needed.
	if (off_fp != NULL) {
		if (off_fp(argp, arglen) != 0) {
			os::printf("    WARNING: Disarming faults failed\n");
		}
	}

	if (e.exception()) {
		e.exception()->print_exception(
		    "repl_sample::value_obj::get_value()");
		return (1);
	}

	// Compare the invocation result value with the expected value.
	if (value == EXPECTED_VALUE) {
		os::printf("--- PASS: Value returned was: %d from node %d\n",
		    value, nodenum);
		return (0);
	} else {
		os::printf("+++ FAIL: Value returned was: %d from node %d\n",
		    value, nodenum);
		return (1);
	}
}

#endif // _KERNEL_ORB


//
// do_work
// This function is used to create some mild work on the service before the
// actual test is executed.
//
int
do_work(char *svc_name)
{
	Environment	e;
	int		rslt;

	os::printf(" ** Making service do work (setup)...\n");
	rslt = simple_mini_1(NULL, NULL, NULL, 0, e, svc_name, false);
	if (e.exception()) {
		e.exception()->print_exception("simple_mini_1");
		return (1);
	}
	if (rslt) {
		return (rslt);
	}
	rslt = one_phase_mini_1(NULL, NULL, NULL, 0, e, svc_name, false);
	if (e.exception()) {
		e.exception()->print_exception("one_phase_mini_1");
		return (1);
	}
	if (rslt) {
		return (rslt);
	}
	rslt = one_phase_mini_2(NULL, NULL, NULL, 0, e, svc_name, false);
	if (e.exception()) {
		e.exception()->print_exception("one_phase_mini_2");
		return (1);
	}
	if (rslt) {
		return (rslt);
	}
	rslt = two_phase_mini_1(NULL, NULL, NULL, 0, e, svc_name, false);
	if (e.exception()) {
		e.exception()->print_exception("two_phase_mini_1");
		return (1);
	}
	if (rslt) {
		return (rslt);
	}
	os::printf(" ** Done making service do work\n");
	return (0);
}

#if defined(_KERNEL_ORB)

#ifdef _FAULT_INJECTION

int
register_generic(
	FAULT_FPTR(on_fp),
	FAULT_FPTR(off_fp),
	void *,
	int,
	char *fn_name)
{
	FaultFunctions::ha_rm_reboot_arg_t reboot_arg;
	int rslt = 0;
	Environment e;

	reboot_arg.nodeid = FaultFunctions::rm_primary_node();
	reboot_arg.op = FaultFunctions::WAIT_FOR_UNKNOWN;
	(void) os::strcpy(reboot_arg.fn_name, fn_name);

	if (on_fp(&reboot_arg, (int)sizeof (reboot_arg)) != 0) {
		os::printf("    WARNING: Arming faults failed\n");
	}

	repl_sample_prov	*prov_ptr	= NULL;
	char			prov_desc[10];

	os::sprintf(prov_desc, "%d", orb_conf::node_number());
	prov_ptr = new repl_sample_prov("ha_sample_server",
	    (const char *)prov_desc);
	ASSERT(prov_ptr != NULL);
	prov_ptr->register_with_rm(1, NULL, NULL, true, e);

	if (e.exception()) {
		e.exception()->print_exception("register_with_rm");
		os::printf("+++ FAIL: exception was not expected\n");
		return (1);
	}

	if (off_fp(&reboot_arg, (int)sizeof (reboot_arg)) != 0) {
		os::printf("    WARNING: Disarming faults failed\n");
	}

	return (rslt);
}
#endif // _FAULT_INJECTION

#endif // _KERNEL_ORB

//
// get_control
// Gets the Service Admin interface for a specific service
// This routine is copied over from repl_lib.cc in replctl
//
// Arguments:
//	A string of the service name
//	A reference to the rma
// Returns:
//	A referernce to the service admin object
//	NULL in case of error
replica::service_admin_ptr
get_control(
	const char *service_desc,
	const replica::rm_admin_var &rma_ref,
	Environment &e)
{
	replica::service_admin_var	svc_admin_ref;

	ASSERT(service_desc != NULL);
	ASSERT(!CORBA::is_nil(rma_ref));

	// Get control
	svc_admin_ref = rma_ref->get_control(service_desc, e);
	if (e.exception())
		return (nil);

	if (CORBA::is_nil(svc_admin_ref))
		return (nil);

	return (replica::service_admin::_duplicate(svc_admin_ref));
}
