/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)repl_sample_impl_aux_v0.cc	1.6	08/05/20 SMI"

#include <orbtest/repl_sample_ru/repl_sample_impl_v0.h>
#include <orb/infrastructure/orb_conf.h>

//
// This is the UNODE entry point.  And a function that gets called from
// the kernel module
//
// Within the arguments to this functions are the name of the service
// and the dependecies of this service (who we depend on)
// If there are no arguments.  The name of the service will default to
// repl_sample_server
//

int
ha_sample_server_v0(int argc, char *argv[])
{
	Environment e;
	repl_sample_prov	*prov_ptr;

	// Exmaine the command line
	//
	// The arguments (if any) should be in this order:
	//
	// 0	- The name of the entry point (repl_sample_server)
	// 1	- The name of the service
	// 2..	- The name of a dependency
	//
	char *svc_desc;
	int	svc_deps_num = argc - 2;
	if (svc_deps_num < 1)
		svc_deps_num = 0;

	replica::service_dependencies
		svc_deps((uint32_t)svc_deps_num, (uint32_t)svc_deps_num);
	replica::prov_dependencies
		prov_deps((uint32_t)svc_deps_num, (uint32_t)svc_deps_num);

	// Set the description of the current prov we're starting to the
	// node number
	char prov_desc[10];
	os::sprintf(prov_desc, "%d", orb_conf::node_number());

	if (argc > 1) {
		svc_desc = argv[1];
		for (int counter = 2; counter < argc; counter++) {
			svc_deps[(uint32_t)counter - 2] =
			    (const char *)argv[counter];
			prov_deps[(uint32_t)counter - 2].service =
			    (const char *)argv[counter];
			prov_deps[(uint32_t)counter - 2].repl_prov_desc =
			    (const char *)prov_desc;
		}
	} else  {
		svc_desc = "ha_sample_server";
	}
	ASSERT(svc_desc != NULL);

	os::printf("Service: \"%s\" (%s) initialized\n", svc_desc, prov_desc);
	if (svc_deps.length() > 0) {
		for (uint32_t counter = 0; counter < svc_deps.length();
		    counter++) {
			if ((char *)svc_deps[counter] != NULL)
				os::printf("\tsvc_deps: \"%s\"\n",
				    (char *)svc_deps[counter]);
		}
	}
	if (prov_deps.length() > 0) {
		for (uint32_t counter = 0; counter < prov_deps.length();
		    counter++) {
			if ((char *)prov_deps[counter].service != NULL)
				os::printf("\tprov_deps: \"%s\", \"%s\"\n",
				    (char *)prov_deps[counter].service,
				    (char *)prov_deps[counter].repl_prov_desc);
		}
	}

	// Create a replica provider with the description node_#
	prov_ptr = new repl_sample_prov((const char *)svc_desc,
	    (const char *)prov_desc);

	// Register the prov with the dependencies specified
	prov_ptr->register_with_rm(1, &svc_deps, &prov_deps, true, e);
	if (e.exception()) {
		e.exception()->print_exception("Failed to register:");
		return (1);
	}

	return (0);
}
