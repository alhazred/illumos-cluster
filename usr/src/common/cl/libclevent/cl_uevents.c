/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)cl_uevents.c	1.19	09/01/09 SMI"

#include <stdio.h>
#include <sys/sol_version.h>
#include <sys/sc_syslog_msg.h>
#include <syslog.h>

#ifndef _KERNEL_ORB
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
#include <sys/cladm_int.h>
#include <sys/clconf_int.h>
#endif /* ! _KERNEL_ORB */

#include <sys/types.h>
#include <sys/time.h>
#include <sys/cl_eventdefs.h>
#include <libsysevent.h>
#include <netdb.h>

#if SOL_VERSION >= __s10
#include <rgm/security.h>
#include <sc_event_proxy/sc_event_proxy.h>
#include <sc_event_proxy_impl.h>
#include <door.h>
#include <sys/mman.h>
#include <string.h>
#include <sys/vc_int.h>
#endif

#ifndef _KERNEL_ORB

/*
 * The following values are cached and
 * need not be retrieved everytime
 */
static char clid[256];
static char clname[256];
#if SOL_VERSION >= __s10
static char zone_name[ZONENAME_MAX];
static char nodename[MAXHOSTNAMELEN + 1 + ZONENAME_MAX];
#else
static char nodename[MAXHOSTNAMELEN + 1];
#endif


/* check for cluster info cache. value of 1 means cached */
static int clinfo_cache_status = 0;

/*
 * The cst_events defines the event types that cluster would like
 * CST to care about. The event list is terminated by a NULL element.
 */
char *cst_events[] = {
	ESC_CLUSTER_NODE_CONFIG_CHANGE,
	ESC_CLUSTER_NODE_STATE_CHANGE,
	ESC_CLUSTER_MEMBERSHIP,
	ESC_CLUSTER_R_STATE,
	ESC_CLUSTER_RG_STATE,
	ESC_CLUSTER_RG_REMAINING_OFFLINE,
	ESC_CLUSTER_RG_GIVEOVER_DEFERRED,
	ESC_CLUSTER_RG_NODE_REBOOTED,
	ESC_CLUSTER_FM_R_STATUS_CHANGE,
	ESC_CLUSTER_R_CONFIG_CHANGE,
	ESC_CLUSTER_RG_CONFIG_CHANGE,
	ESC_CLUSTER_RG_PRIMARIES_CHANGING,
	ESC_CLUSTER_FM_R_RESTARTING,
	NULL
};


/*
 * This structure contains the subscriber name and pointer to
 * the associated event list.
 */
typedef struct subscription_map {
	char *subscriber;
	char **eventlist;
} subscription_map;


/*
 * The subscription map contains the association of subscriber
 * to event type. This list can be expanded to include other
 * subscribers and their subscribed events.
 */
static subscription_map event_map[] = {
	{ "CST", (char **)cst_events },
	{ NULL, NULL }
};

void sc_free_subscription(char **);
int sc_get_subscription(char *, char ***);

#if SOL_VERSION >= __s10
/*
 * This function is the client function to the event proxy server
 * running in the global zone. Here we make a door call to the
 * server which logs the sysevent for us in the global zone.
 */
cl_event_error_t
sc_publish_event_proxy(const char *ev_subclass, const char *ev_publisher,
    nvlist_t *attr_list)
{
	int serverfd = -1;
	door_arg_t door_arg;
	char *arg_buf = NULL;
	char *attr_buf = NULL;
	XDR xdrs;
	size_t arg_size;
	size_t attr_size;
	int retval = 0;
	proxy_event_data event_data = {0};
	sc_syslog_msg_handle_t sys_handle;

	if (nvlist_size(attr_list, &attr_size, NV_ENCODE_XDR)) {
		retval = CL_EVENT_EPOST;
		/* CSTYLED */
		goto proxy_end; /*lint !e801 */
	}

	attr_buf = (char *)calloc(1, attr_size);
	if (attr_buf == NULL) {
		retval = ENOMEM;
		/* CSTYLED */
		goto proxy_end; /*lint !e801 */
	}
	if (nvlist_pack(attr_list, &attr_buf, &attr_size,
	    NV_ENCODE_XDR, 0)) {
		retval = CL_EVENT_EPOST;
		/* CSTYLED */
		goto proxy_end; /*lint !e801 */
	}

	/* Open the server door */
	if ((serverfd = get_door_syscall(RGM_SYSEVENT_PROXY_CODE,
	    GLOBAL_ZONENAME)) == -1) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_SC_EVENT_PROXY_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * An internal error has occurred in the inter-process
		 * communication between Sun Cluster processes. Related error
		 * messages might be found near this one in the syslog output.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    SYSTEXT("Unable to open door server."));
		sc_syslog_msg_done(&sys_handle);
		retval = CL_EVENT_EPOST;
		/* CSTYLED */
		goto proxy_end; /*lint !e801 */
	}

	/* Populate the event_data struct */

	event_data.subclass = (char *)ev_subclass;
	event_data.publisher = (char *)ev_publisher;
	event_data.buf_size = attr_size;

	/* Calculate size of buffer needed to encode data */
	arg_size = proxy_event_data_xdr_sizeof(event_data) +
	    EVENT_PROXY_PADDING_SPACE + attr_size;

	arg_buf = (char *)calloc(1, arg_size);
	if (arg_buf == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_SC_EVENT_PROXY_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * An internal error has occurred in the inter-process
		 * communication between Sun Cluster processes. Related error
		 * messages might be found near this one in the syslog output.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    SYSTEXT("Unable to allocate memory"));
		sc_syslog_msg_done(&sys_handle);
		retval = ENOMEM;
		/* CSTYLED */
		goto proxy_end; /*lint !e801 */
	}

	/* Initialize the XDR Stream for Encoding data */
	xdrs.x_ops = NULL;
	xdrmem_create(&xdrs, arg_buf, (uint_t)arg_size, XDR_ENCODE);
	if (xdrs.x_ops == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_SC_EVENT_PROXY_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * An internal error has occurred in the inter-process
		 * communication between Sun Cluster processes. Related error
		 * messages might be found near this one in the syslog output.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    SYSTEXT("Unable to initialize XDR stream"));
		sc_syslog_msg_done(&sys_handle);
		retval = CL_EVENT_EPOST;
		/* CSTYLED */
		goto proxy_end; /*lint !e801 */
	}

	/*
	 * Marshall the arguments.
	 * XDR Encode the data to be sent to the
	 * door server in the global zone.
	 */
	if (!xdr_proxy_event_data(&xdrs, &event_data)) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_SC_EVENT_PROXY_TAG, "");
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    SYSTEXT("XDR Error while encoding arguments."));
		sc_syslog_msg_done(&sys_handle);
		retval = CL_EVENT_EPOST;
		xdr_destroy(&xdrs);
		/* CSTYLED */
		goto proxy_end; /*lint !e801 */
	}

	/*
	 * Set up the arguments to the door call.
	 * Copy the data in the attr_buf at the
	 * end of the arg_buf keeping a certain
	 * padding space in between. This padding is
	 * needed as the xdr encoded data might use
	 * some extra space beyond the xdr_getpos
	 * point.
	 * [(xdr_encoded_data)(padding)(attr_list)]
	 * One important thing to note here is that, we
	 * do not encode the attr_list while sending it
	 * across the door. The reason is that attr_list
	 * is already nvlist packed using XDR encoding.
	 * So, we cannot reencode and decode it. Therefore,
	 * we dont encode it again and send it across
	 * by copying it after the arg_buf with the padding
	 * space in between.
	 */
	(void) memcpy(arg_buf + xdr_getpos(&xdrs)
	    + EVENT_PROXY_PADDING_SPACE,
	    attr_buf, attr_size);

	door_arg.data_ptr = arg_buf;
	door_arg.data_size = arg_size;
	door_arg.desc_ptr = NULL;
	door_arg.desc_num = 0;
	door_arg.rbuf = NULL;
	door_arg.rsize = 0;

	/* Make door call to global zone to generate the sysevent */
	if (make_door_call(serverfd, &door_arg) != 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_SC_EVENT_PROXY_TAG, "");
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    SYSTEXT("Unable to contact sc_zonesd."));
		sc_syslog_msg_done(&sys_handle);
		retval = CL_EVENT_EPOST;
		xdr_destroy(&xdrs);
		/* CSTYLED */
		goto proxy_end; /*lint !e801 */
	}
	if (strcmp(door_arg.rbuf, "SUCCESS") != 0) {
		retval = CL_EVENT_EPOST;
	}
	/*
	 * Free any memory that was allocated by door
	 * to copy return arguments
	 */
	(void) munmap(door_arg.rbuf, door_arg.rsize);
	xdr_destroy(&xdrs);

proxy_end:

	if (attr_buf != NULL) {
		free(attr_buf);
	}
	if (arg_buf != NULL) {
		free(arg_buf);
	}
	if (serverfd != -1) {
		(void) close(serverfd);
	}
	return (retval);
}

#endif /* SOL_VERSION >= __s10 */

#if SOL_VERSION >= __s10
cl_event_error_t
sc_publish_event_common(char *subclassp, char *publisherp,
    cl_event_severity_t severity, cl_event_initiator_t initiator,
    char *clidp, char *clnamep, char *nodenamep, char *zonenamep,
    va_list ap)
#else
cl_event_error_t
sc_publish_event_common(char *subclassp, char *publisherp,
    cl_event_severity_t severity, cl_event_initiator_t initiator,
    char *clidp, char *clnamep, char *nodenamep, va_list ap)
#endif /* SOL_VERSION >= __s10 */
{

	int retval = 0;

	char *ev_subclassp;
	char *ev_publisherp;
	uint32_t ev_severity = 0;
	uint32_t ev_initiator = 0;

	char *value_namep;
	data_type_t value_type;

	sysevent_id_t eid;
	nvlist_t *attr_listp;

	struct timeval tod;

	int err;

	/* Allocate event attribute list */

	if (nvlist_alloc(&attr_listp, NV_UNIQUE_NAME_TYPE, 0) != 0) {
		return (CL_EVENT_ENOMEM);
	}

	/* Process the argument list */

	ev_subclassp = subclassp;
	ev_publisherp = publisherp;
	ev_severity = (uint32_t)severity;
	ev_initiator = (uint32_t)initiator;

	/* Skip over reserved fields that are ignored for now */
	/* CSTYLED */
	/*lint -e718 -e746 */
	(void) va_arg(ap, int);
	(void) va_arg(ap, int);
	(void) va_arg(ap, int);
	/* CSTYLED */
	/*lint +e718 +e746 */

	/*
	 * Add required event attributes:
	 *  - Cluster ID
	 *  - Cluster Name
	 *  - Node Name
	 *  - Time Stamp
	 *  - Event Version
	 *  - Severity
	 *  - Initiator
	 */

	if (nvlist_add_string(attr_listp, CL_CLUSTER_ID, clidp) != 0) {
		retval = CL_EVENT_EATTR;
		/* CSTYLED */
		goto event_exit; /*lint !e801 */
	}

	if (nvlist_add_string(attr_listp, CL_CLUSTER_NAME, clnamep) != 0) {
		retval = CL_EVENT_EATTR;
		/* CSTYLED */
		goto event_exit; /*lint !e801 */
	}

	if (nvlist_add_string(attr_listp, CL_EVENT_NODE, nodenamep) != 0) {
		retval = CL_EVENT_EATTR;
		/* CSTYLED */
		goto event_exit; /*lint !e801 */
	}

	if (nvlist_add_uint32(attr_listp, CL_EVENT_VERSION,
			CL_EVENT_VERSION_NUMBER) != 0) {
		retval = CL_EVENT_EATTR;
		/* CSTYLED */
		goto event_exit; /*lint !e801 */
	}

	(void) gettimeofday(&tod, (void *) NULL);

	if (nvlist_add_uint32(attr_listp, CL_EVENT_TS_SEC,
			(uint32_t)tod.tv_sec) != 0) {
		retval = CL_EVENT_EATTR;
		/* CSTYLED */
		goto event_exit; /*lint !e801 */
	}

	if (nvlist_add_uint32(attr_listp, CL_EVENT_TS_USEC,
			(uint32_t)tod.tv_usec) != 0) {
		retval = CL_EVENT_EATTR;
		/* CSTYLED */
		goto event_exit; /*lint !e801 */
	}


	/* Add initiator id and severity values */

	if (nvlist_add_uint32(attr_listp, CL_EVENT_SEVERITY,
			ev_severity) != 0) {
		retval = CL_EVENT_EATTR;
		/* CSTYLED */
		goto event_exit; /*lint !e801 */
	}

	if (nvlist_add_uint32(attr_listp, CL_EVENT_INITIATOR,
			ev_initiator) != 0) {
		retval = CL_EVENT_EATTR;
		/* CSTYLED */
		goto event_exit; /*lint !e801 */
	}

	/*
	 * Process event attributes. Each attribute specification is
	 * in a form of a triple, consisting of a value name, value
	 * type and the actual value. Processing is terminated when
	 * we encounter a NULL attribute.
	 *
	 */

	/* CSTYLED */
	/*lint -e516 */
	while (1) {

		value_namep = va_arg(ap, char *);
		if (value_namep == NULL)
			break;

		value_type = va_arg(ap, data_type_t);
		if (value_type == NULL)
			break;

		switch (value_type) {
		case SE_DATA_TYPE_BYTE : {
			uchar_t value;
			value = va_arg(ap, uchar_t);
			err = nvlist_add_byte(attr_listp, value_namep, value);
			if (err) {
				retval = CL_EVENT_EATTR;
				/* CSTYLED */
				goto event_exit; /*lint !e801 */
			}
			break;
		}
		case SE_DATA_TYPE_INT16 : {
			int16_t value;
			value = va_arg(ap, int16_t);
			err = nvlist_add_int16(attr_listp, value_namep, value);
			if (err) {
				retval = CL_EVENT_EATTR;
				/* CSTYLED */
				goto event_exit; /*lint !e801 */
			}
			break;
		}
		case SE_DATA_TYPE_UINT16 : {
			uint16_t  value;
			value = va_arg(ap, uint16_t);
			err = nvlist_add_uint16(attr_listp, value_namep, value);
			if (err) {
				retval = CL_EVENT_EATTR;
				/* CSTYLED */
				goto event_exit; /*lint !e801 */
			}
			break;
		}
		case SE_DATA_TYPE_INT32 : {
			int32_t value;
			value = va_arg(ap, int32_t);
			err = nvlist_add_int32(attr_listp, value_namep, value);
			if (err) {
				retval = CL_EVENT_EATTR;
				/* CSTYLED */
				goto event_exit; /*lint !e801 */
			}
			break;
		}
		case SE_DATA_TYPE_UINT32 : {
			uint32_t  value;
			value = va_arg(ap, uint32_t);
			err = nvlist_add_uint32(attr_listp, value_namep, value);
			if (err) {
				retval = CL_EVENT_EATTR;
				/* CSTYLED */
				goto event_exit; /*lint !e801 */
			}
			break;
		}
		case SE_DATA_TYPE_INT64 : {
			int64_t value;
			value = va_arg(ap, int64_t);
			err = nvlist_add_int64(attr_listp, value_namep, value);
			if (err) {
				retval = CL_EVENT_EATTR;
				/* CSTYLED */
				goto event_exit; /*lint !e801 */
			}
			break;
		}
		case SE_DATA_TYPE_UINT64 : {
			uint64_t  value;
			value = va_arg(ap, uint64_t);
			err = nvlist_add_uint64(attr_listp, value_namep, value);
			if (err) {
				retval = CL_EVENT_EATTR;
				/* CSTYLED */
				goto event_exit; /*lint !e801 */
			}
			break;
		}
		case SE_DATA_TYPE_STRING : {
			char *value;
			value = va_arg(ap, char *);
			err = nvlist_add_string(attr_listp, value_namep, value);
			if (err) {
				retval = CL_EVENT_EATTR;
				/* CSTYLED */
				goto event_exit; /*lint !e801 */
			}
			break;
		}
		case SE_DATA_TYPE_BYTES : {
			uchar_t *value;
			value = va_arg(ap, uchar_t *);
			err = nvlist_add_byte_array(attr_listp,
				value_namep, value, (uint_t)sizeof (value));
			if (err) {
				retval = CL_EVENT_EATTR;
				/* CSTYLED */
				goto event_exit; /*lint !e801 */
			}
			break;
		}
		case SE_DATA_TYPE_TIME : {
			int64_t value;
			value = va_arg(ap, int64_t);
			err = nvlist_add_int64(attr_listp, value_namep, value);
			if (err) {
				retval = CL_EVENT_EATTR;
				/* CSTYLED */
				goto event_exit; /*lint !e801 */
			}
			break;
		}
		case DATA_TYPE_UNKNOWN:
		case DATA_TYPE_BOOLEAN:
		case DATA_TYPE_INT16_ARRAY:
		case DATA_TYPE_UINT16_ARRAY:
		case DATA_TYPE_INT32_ARRAY:
		case DATA_TYPE_UINT32_ARRAY:
		case DATA_TYPE_INT64_ARRAY:
		case DATA_TYPE_UINT64_ARRAY:
		case DATA_TYPE_STRING_ARRAY:
		default:
			retval = CL_EVENT_EINVAL;
			/* CSTYLED */
			goto event_exit; /*lint !e801 */
		}
	}
	/* CSTYLED */
	/*lint +e516 */

	/*
	 * Post event...
	 */
#if SOL_VERSION >= __s10
	if (strcmp(zonenamep, GLOBAL_ZONENAME) == 0) {
		if (sysevent_post_event(EC_CLUSTER, ev_subclassp,
		    SUNW_VENDOR, ev_publisherp, attr_listp, &eid) != 0) {
			retval = CL_EVENT_EPOST;
		}
	} else {
		retval = sc_publish_event_proxy(ev_subclassp, ev_publisherp,
		    attr_listp);
	}
#else
	if (sysevent_post_event(EC_CLUSTER, ev_subclassp,
	    SUNW_VENDOR, ev_publisherp, attr_listp, &eid) != 0) {
		retval = CL_EVENT_EPOST;
	}
#endif /* SOL_VERSION >= __s10 */
	event_exit:
		if (attr_listp) {
			nvlist_free(attr_listp);
		}
	return (retval);
}


/*
 * Common code called by sc_publish_event and sc_publish_zc_event interfaces.
 */
/* VARARGS4 */
/* ARGSUSED */
cl_event_error_t
sc_publish_event_int(char *subclass, char *publisher,
	cl_event_severity_t severity, cl_event_initiator_t initiator,
	va_list ap)
{
	int retval = 0;

	clcluster_name_t clname_args;
	clnode_name_t nodename_args;
	get_ccr_entry_t ccr_entry_args;
	char *table_name = "infrastructure";
	char *entry_name = "cluster.properties.cluster_id";
	sc_syslog_msg_handle_t sys_handle;

	nodeid_t nid;
	size_t publen;
	int err;


	/* Validate function parameters */

	if ((subclass == NULL) || (strlen(subclass) > MAX_SUBCLASS_LEN)) {
		return (CL_EVENT_EINVAL);
	}

	publen = strlen(publisher) + strlen(SUNW_VENDOR) +
		strlen(SE_USR_PUB) + 1;

	if ((publisher == NULL) || (publen > MAX_PUB_LEN)) {
		return (CL_EVENT_EINVAL);
	}

	if ((severity < 0) || (initiator < 0)) {
		return (CL_EVENT_EINVAL);
	}

	/*
	 * The cluster information can be cached. It is sufficient to retrieve
	 * it only once.
	 */

	if (clinfo_cache_status == 0) {
	/* cache the cluster info: cluster id, cluster name,  nodename */

		/* Get cluster id */
		ccr_entry_args.table_name = table_name;
		ccr_entry_args.table_name_len = strlen(table_name) + 1;
		ccr_entry_args.entry_name = entry_name;
		ccr_entry_args.entry_name_len = strlen(entry_name) + 1;
		ccr_entry_args.buf = clid;
		ccr_entry_args.buf_len = sizeof (clid);

		if (cladm(CL_CONFIG, CL_GET_CCR_ENTRY, &ccr_entry_args)) {
			return (CL_EVENT_ECLADM);
		}

		/* Get cluster name */
		clname_args.name = clname;
		clname_args.len = sizeof (clname);

		if (cladm(CL_CONFIG, CL_GET_CLUSTER_NAME, &clname_args)) {
			return (CL_EVENT_ECLADM);
		}

		/* Get nodename */
		if (cladm(CL_CONFIG, CL_NODEID, &nid)) {
			return (CL_EVENT_ECLADM);
		}

		nodename_args.nodeid = nid;
		nodename_args.name = nodename;
		nodename_args.len = sizeof (nodename);

		if (cladm(CL_CONFIG, CL_GET_NODE_NAME, &nodename_args)) {
			return (CL_EVENT_ECLADM);
		}

#if SOL_VERSION >= __s10
		/*
		 * Retrieve the zonename
		 */
		if (getzonenamebyid(getzoneid(), zone_name,
		    sizeof (zone_name)) < 0) {
			/* CSTYLED */
			err = errno; /*lint !e746 */
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_SC_EVENT_PROXY_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * getzonenamebyid failed for the specified reason.
			 * @user_action
			 * Save a copy of the /var/adm/messages files on all
			 * nodes. Contact your authorized Sun service provider
			 * for assistance in diagnosing the problem.
			 */
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    SYSTEXT("getzonenamebyid failed: %s"),
			    strerror(err));
			sc_syslog_msg_done(&sys_handle);
			return (CL_EVENT_EPOST);
		}

		if (strcmp(zone_name, GLOBAL_ZONENAME) != 0) {
			(void) strlcat(nodename, ":", sizeof (nodename));
			(void) strlcat(nodename, zone_name, sizeof (nodename));
		}
#endif
		clinfo_cache_status = 1;
	}
#if SOL_VERSION >= __s10
	retval = sc_publish_event_common(subclass, publisher, severity,
		    initiator, clid, clname, nodename, zone_name, ap);
#else
	retval = sc_publish_event_common(subclass, publisher, severity,
		    initiator, clid, clname, nodename, ap);
#endif /* SOL_VERSION >= __s10 */

	return (retval);
}
#endif /* ! _KERNEL_ORB */


/*
 * Interface to publish an event from the base cluster.
 */
/* VARARGS4 */
/* ARGSUSED */
cl_event_error_t
sc_publish_event(char *subclass, char *publisher,
	cl_event_severity_t severity, cl_event_initiator_t initiator, ...)
{
#ifndef _KERNEL_ORB
	va_list ap;
	int retval;
	/* CSTYLED */
	va_start(ap, initiator); /*lint !e40 */
	retval = sc_publish_event_int(subclass, publisher, severity,
	    initiator, ap);
	va_end(ap);
	return (retval);
#else
	return (0);
#endif /* _KERNEL_ORB */
}

#if SOL_VERSION >= __s10

/*
 * Interface to publish an event related to a zone cluster from the base
 * cluster.
 */
/* VARARGS4 */
/* ARGSUSED */
cl_event_error_t
sc_publish_zc_event(char *zc_namep, char *subclassp, char *publisherp,
	cl_event_severity_t severity, cl_event_initiator_t initiator, ...)
{
#ifndef _KERNEL_ORB

	va_list ap;
	int retval = 0;

	sc_syslog_msg_handle_t sys_handle;

	nodeid_t nid;
	size_t publen;
	int err;

	cz_id_t czid;
	zcnode_name_t zcnode_name;

	/*
	 * If we are dealing with the global zone, we will invoke
	 * the interface to publish global zone events.
	 */
	if (strcmp(zc_namep, "global") == 0) {
		/* CSTYLED */
		va_start(ap, initiator); /*lint !e40 */
		retval = sc_publish_event_int(subclassp, publisherp, severity,
		    initiator, ap);
		va_end(ap);
		return (retval);
	}

	/* Validate function parameters */

	if ((subclassp == NULL) || (strlen(subclassp) > MAX_SUBCLASS_LEN)) {
		return (CL_EVENT_EINVAL);
	}

	publen = strlen(publisherp) + strlen(SUNW_VENDOR) +
		strlen(SE_USR_PUB) + 1;

	if ((publisherp == NULL) || (publen > MAX_PUB_LEN)) {
		return (CL_EVENT_EINVAL);
	}

	if ((severity < 0) || (initiator < 0)) {
		return (CL_EVENT_EINVAL);
	}

	czid.clid = 0;
	(void) strcpy(czid.name, zc_namep);

	if (_cladm(CL_CONFIG, CL_GET_ZC_ID, &czid)) {
		return (CL_EVENT_ECLADM);
	}
	/*
	 * Validate that the zone cluster name that is specified and is
	 * configured in the system.
	 */
	if (czid.clid < MIN_CLUSTER_ID) {
		return (CL_EVENT_EINVAL);
	}

	/*
	 * This interface overwrites any previous state that may have
	 * been cached. We reset the cache here.
	 */
	clinfo_cache_status = 0;

	(void) sprintf(clid, "%d", czid.clid);

	/*
	 * Get the cluster name. For a zone cluster, cluster name is
	 * same as the zone name.
	 */
	(void) strcpy(clname, zc_namep);

	/* Get nodename */

	if (cladm(CL_CONFIG, CL_NODEID, &nid)) {
		return (CL_EVENT_ECLADM);
	}

	(void) strcpy(zcnode_name.zc_name, zc_namep);

	zcnode_name.nodeid = nid;
	zcnode_name.namep = nodename;
	zcnode_name.len = sizeof (nodename);

	if (cladm(CL_CONFIG, CL_GET_ZC_NODE_NAME, &zcnode_name)) {
		return (CL_EVENT_ECLADM);
	}
	/*
	 * Retrieve the zonename
	 */
	if (getzonenamebyid(getzoneid(), zone_name,
	    sizeof (zone_name)) < 0) {
		/* CSTYLED */
		err = errno; /*lint !e746 */
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_SC_EVENT_PROXY_TAG, "");

		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    SYSTEXT("getzonenamebyid failed: %s"),
		    strerror(err));
		sc_syslog_msg_done(&sys_handle);
		return (CL_EVENT_EPOST);
	}
	/* CSTYLED */
	va_start(ap, initiator); /*lint !e40 */

	retval = sc_publish_event_common(subclassp, publisherp, severity,
		    initiator, clid, clname, nodename, zone_name, ap);

	va_end(ap);
	return (retval);
#else
	return (0);

#endif /* ! _KERNEL_ORB */
}

#endif /* SOL_VERSION >= __s10 */

#ifndef _KERNEL_ORB
/*
 * sc_get_subscription
 *
 * This function will accept a character string to identify
 * a particular subscriber and then allocate an array of strings
 * (i.e. event types) associated with that subscriber and return
 * them to the caller. The array of event types will be NULL
 * terminated. The caller will be responsible for calling
 * sc_free_subscription() to free the allocated memory.
 *
 * Possible Return values:
 *	0	- Success
 *	EINVAL	- Invalid subscriber or events
 *	ENOMEM	- Not enough memory
 */
int
sc_get_subscription(char *subscriber, char **subscription[])
{
	char **event;
	subscription_map *sub;
	uint32_t nevents = 0;
	uint32_t i;

	if (subscriber == NULL) {
		return (EINVAL);
	}

	/*
	 * Find the subscriber in the event_map. Currently only
	 * a limited number of subscribers are recognized.
	 */
	for (sub = event_map; sub->subscriber != NULL; sub++) {
		if (strcmp(sub->subscriber, subscriber) == 0) {
			break;
		}
	}
	if (sub->subscriber == NULL) {
		return (EINVAL);
	}

	/*
	 * Determine how many events are associated with this subscriber
	 * and allocate the appropriate sufficient memory.
	 */
	for (event = sub->eventlist; *event != NULL; event++) {
		nevents++;
	}

	if (nevents > 0) {
		if ((*subscription = (char **)calloc(
		    (size_t)(nevents + 1), sizeof (char *))) == NULL) {
			return (ENOMEM);
		}
		for (i = 0; i < nevents; i++) {
			if (((*subscription)[i] = strdup(sub->eventlist[i]))
			    == NULL) {
				sc_free_subscription(*subscription);
				return (ENOMEM);
			}
		}
		(*subscription)[i] = NULL;
	} else {
		/* No events found for this subscriber */
		return (EINVAL);
	}

	return (0);
}

/*
 * Free all memory associated with the event list.
 */
void
sc_free_subscription(char **ev_list)
{
	int i = 0;

	if (ev_list == NULL) {
		return;
	}

	while (ev_list[i] != NULL) {
		free(ev_list[i]);
		i++;
	}
	free(ev_list);
}
#endif /* ! _KERNEL_ORB */
