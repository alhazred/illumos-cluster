/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident   "@(#)cl_event_utils.c 1.6 08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <door.h>
#include <string.h>
#include <strings.h>
#include <synch.h>
#include <pthread.h>
#include <thread.h>
#include <libnvpair.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/modctl.h>
#include <sys/sysevent.h>
#include <sys/sysevent_impl.h>

#include <sys/cl_events_int.h>

/*
 * The following functions are copies of the project private interfaces
 * provided by Solaris libsysevent library. They are here because Solaris
 * is unwilling to expose the project private interfaces in libsysevent.
 * These functions are only needed while Sun Cluster 3.x is supported on
 * Solaris 8, and should be removed whenever Solaris 8 support is no longer
 * necessary.
 *
 */

#define	dprint	if (debug) (void) printf
static int debug = 0;

/*
 * cl_door_service - generic event delivery service routine.  This routine
 *		is called in response to a door call to post an event.
 *
 */

static void
cl_door_service(void *cookie, char *args, size_t alen,
    door_desc_t *ddp, uint_t ndid)
{
	door_cookie_t *cook = (door_cookie_t *)cookie;
	sysevent_t *ev;
	int	ret;

	if (args == NULL || alen == (size_t)0) {
		return;
	}

	/*
	 * Allocate and copy the event buffer into the subscriber's
	 * address space
	 */
	ev = (sysevent_t *)calloc((size_t)1, alen);
	if (ev == NULL) {
		ret = ENOMEM;
		goto return_from_door;
	}
	bcopy(args, ev, alen);

	/*
	 * door_func now has a private copy of the event and may proceed
	 * to consume the event.
	 *
	 * The return value is provided to the client via a door_return
	 * call.
	 *
	 */
	ret = (*cook->door_func)(ev);

	free(ev);

return_from_door:

	bcopy(&ret, args, sizeof (int32_t));
	(void) door_return(args, sizeof (int32_t), NULL, 0);
} /*lint !e715 */

/*
 * Establish an event channel through which an event publisher may post
 * events or an event subscriber may receive events.
 */
sysevent_handle_t *
cl_event_open_channel(char *channel)
{
	int fd;
	sysevent_handle_t *cl_event_hp;


	if (strlen(channel) + 1 > MAXPATHLEN) {
		errno = EINVAL; /*lint !e746 */
		return (NULL);
	}

	cl_event_hp = (sysevent_handle_t *)malloc(sizeof (sysevent_handle_t));
	if (cl_event_hp == NULL) {
		errno = ENOMEM;
		return (NULL);
	}

	cl_event_hp->bound = 0;
	cl_event_hp->result = 0;
	cl_event_hp->service_door = -1;
	(void) mutex_init(&cl_event_hp->lock, USYNC_THREAD, NULL);
	cl_event_hp->door_name = strdup(channel);
	if (cl_event_hp->door_name == NULL) {
		(void) mutex_destroy(&cl_event_hp->lock);
		free(cl_event_hp);
		return (NULL);
	}

	/*
	 * Create the client door file if one does not exist.
	 */
	fd = open(cl_event_hp->door_name, O_CREAT|O_RDWR, S_IREAD|S_IWRITE);
	if ((fd == -1) && (errno != EEXIST)) {
		(void) mutex_destroy(&cl_event_hp->lock);
		free(cl_event_hp->door_name);
		free(cl_event_hp);
		return (NULL);
	}

	cl_event_hp->service_fd = fd;

	return (cl_event_hp);
}

/*
 * cl_event_close_channel - Clean up resources associated with a previously
 *				opened sysevent channel
 */
int
cl_event_close_channel(sysevent_handle_t *cl_event_hp)
{

	if (cl_event_hp == NULL) {
		errno = EINVAL;
		return (-1);
	}

	if (cl_event_hp->bound)
		cl_event_unbind_channel(cl_event_hp);

	(void) close(cl_event_hp->service_fd);
	(void) mutex_destroy(&cl_event_hp->lock);
	free(cl_event_hp->door_name);
	free(cl_event_hp);

	return (0);
}

/*
 * cl_event_bind_channel - Bind an event receiver to an event channel
 */
int
cl_event_bind_channel(sysevent_handle_t *se_handle,
	int (*func)(sysevent_t *ev))
{
	door_cookie_t *cookie;

	if (se_handle == NULL || func == NULL) {
		errno = EINVAL;
		return (-1);
	}

	(void) mutex_lock(&se_handle->lock);
	if (se_handle->bound) {
		errno = EINVAL;
		(void) mutex_unlock(&se_handle->lock);
		return (-1);
	}

	/* Allocate door associated cookie */
	if ((cookie = (door_cookie_t *)malloc(sizeof (*cookie))) == NULL) {
		errno = ENOMEM;
		(void) mutex_unlock(&se_handle->lock);
		return (-1);
	}
	cookie->door_func = func;

	/*
	 * Create the sysevent door service for this client.
	 * syseventd will use this door service to propagate
	 * events to the client.
	 */
	if ((se_handle->service_door = door_create(cl_door_service, cookie, 0))
	    == -1) {
		dprint("sysevent_bind_channel: door create failed: "
			"%s\n", strerror(errno));
		free(cookie);
		(void) mutex_unlock(&se_handle->lock);
		return (-1);
	}

	(void) fdetach(se_handle->door_name);
	if (fattach(se_handle->service_door, se_handle->door_name) != 0) {
		dprint("sysevent_bind_subscriber: door attaching failed: "
			"%s\n", strerror(errno));
		(void) door_revoke(se_handle->service_door);
		free(cookie);
		(void) mutex_unlock(&se_handle->lock);
		return (-1);
	}

	se_handle->cookie = cookie;
	se_handle->bound = 1;

	(void) mutex_unlock(&se_handle->lock);

	return (0);
}

/*
 * cl_event_unbind_channel - Unbind an event receiver from an event channel
 */
void
cl_event_unbind_channel(sysevent_handle_t *cl_event_hp)
{
	if (cl_event_hp == NULL)
		return;

	(void) mutex_lock(&cl_event_hp->lock);
	if (cl_event_hp->bound == 0) {
		(void) mutex_unlock(&cl_event_hp->lock);
		return;
	}

	(void) fdetach(cl_event_hp->door_name);
	(void) door_revoke(cl_event_hp->service_door);
	free(cl_event_hp->cookie);
	(void) mutex_unlock(&cl_event_hp->lock);
}
