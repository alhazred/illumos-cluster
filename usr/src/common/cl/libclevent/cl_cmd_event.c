/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)cl_cmd_event.c	1.6	08/05/20 SMI"

#include <stdio.h>
#include <strings.h>
#include <libgen.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <ctype.h>
#include <pwd.h>

#include <sys/cl_cmd_event.h>
#include <sys/cl_auth.h>

static int arg_num;
static char** args;
static int *exit_val;
static char *cmd_opt;
static int init_called = 0;
static cl_event_error_t cl_ev_err;

#define	PWD_BUF_SIZE	1024

/*
 * quote(char *)
 * Input:
 * cmd_opt	  - this is a pointer to an array containing the parameters
 *                  passed to the program
 * Action : the function quotes the parameters.
 *
 * Return : Returns the quoted string.
 */
char*
quote(char *cmd_option) {
	char *quotedoption = NULL;
	size_t len = 0;

	if (cmd_option != NULL) {
		len = strlen(cmd_option);
		if ((*(cmd_option) == '-') &&
			(len == 2) &&
			(isalpha(*(cmd_option+1)))) {
			quotedoption = (char*) malloc(sizeof (char) * (len+1));
			if (quotedoption != NULL) {
				strcpy(quotedoption, cmd_option);
			}
		} else {
			quotedoption = (char*) malloc(sizeof (char) * (len+3));
			if (quotedoption != NULL) {
				strcpy(quotedoption, "\"");
				strcat(quotedoption, cmd_option);
				strcat(quotedoption, "\"");
			}
		}
	}
	return (quotedoption);
}

/*
 * cl_cmd_event_init(int, char**, int*)
 *
 * Input:
 *       argc      - this is the number of arguments passed to
 *                   the program
 *       argv      - this is a pointer to a pointer to an array
 *                   containing the parameters passed to a program
 *       exit_code - this is the exit value that the program returns.
 *
 * Action: This function initializes this library.
 *
 * Return: none
 */
void
cl_cmd_event_init(int argc, char** argv, int *exit_code) {

	int i = 0;
	size_t len = 0;
	char *quoted_cmdopt;

	arg_num = argc;
	args = argv;
	exit_val = exit_code;
	cl_ev_err = 100;

	for (i = 1; i < arg_num; ++i) {
		len += (strlen(*(args + i)) + 3);
	}

	if (len == 0) {
		len = 1;
	}
	cmd_opt = (char*) malloc(sizeof (char) * len);
	(void) memset(cmd_opt, 0, len);

	for (i = 1; i < arg_num; ++i) {
		quoted_cmdopt = quote(*(args+i));
		if (quoted_cmdopt != NULL) {
			(void) strcat(cmd_opt, quoted_cmdopt);
			if (i != (arg_num - 1)) {
				(void) strcat(cmd_opt, " ");
			}
		}
	}
	init_called = 1;
}

static void
generate_event(char *type) {

	struct passwd pwd;

	char pwd_buff[PWD_BUF_SIZE];
	char loginname[PWD_BUF_SIZE];

	uid_t uid;

	uid = cl_auth_get_initial_uid();

	if (getpwuid_r(uid, &pwd, pwd_buff, PWD_BUF_SIZE) != NULL) {
		strcpy(loginname, pwd.pw_name);
	} else {
		strcpy(loginname, "");
	}

	/*
	 * Suppress lint warning about ANSI limit of
	 * 31 'function arguments'
	 */
	/*lint -e793 */
	cl_ev_err = sc_publish_event(ESC_CLUSTER_CMD,
	    type,
	    CL_EVENT_SEV_INFO,
	    CL_EVENT_INIT_OPERATOR,
	    0, 0, 0,
	    CL_EVENT_CMD_UID, SE_DATA_TYPE_UINT32, uid,
	    CL_EVENT_CMD_EUID, SE_DATA_TYPE_UINT32, geteuid(),
	    CL_EVENT_CMD_PID, SE_DATA_TYPE_UINT32, getpid(),
	    CL_EVENT_CMD_PPID, SE_DATA_TYPE_UINT32, getppid(),
	    CL_EVENT_CMD_LOGIN, SE_DATA_TYPE_STRING, loginname,
	    CL_EVENT_CMD_TTY_STDIN, SE_DATA_TYPE_STRING, ttyname(0),
	    CL_EVENT_CMD_TTY_STDOUT, SE_DATA_TYPE_STRING, ttyname(1),
	    CL_EVENT_CMD_NAME, SE_DATA_TYPE_STRING, basename(*args),
	    CL_EVENT_CMD_OPT, SE_DATA_TYPE_STRING, cmd_opt,
	    CL_EVENT_CMD_RES, SE_DATA_TYPE_INT32, *exit_val,
	    NULL);
	/*lint +e793 */
}

static void
generate_event_1(char *type) {

	struct passwd pwd;

	char pwd_buff[PWD_BUF_SIZE];
	char loginname[PWD_BUF_SIZE];

	uid_t uid;

	uid = cl_auth_get_initial_uid();

	if (getpwuid_r((uid_t)uid, &pwd, pwd_buff, PWD_BUF_SIZE) != NULL) {
		strcpy(loginname, pwd.pw_name);
	} else {
		strcpy(loginname, "user not found");
	}

	/*
	 * Suppress lint warning about ANSI limit of
	 * 31 'function arguments'
	 */
	/*lint -e793 */
	cl_ev_err = sc_publish_event(ESC_CLUSTER_CMD,
	    type,
	    CL_EVENT_SEV_INFO,
	    CL_EVENT_INIT_OPERATOR,
	    0, 0, 0,
	    CL_EVENT_CMD_UID, SE_DATA_TYPE_UINT32, uid,
	    CL_EVENT_CMD_EUID, SE_DATA_TYPE_UINT32, geteuid(),
	    CL_EVENT_CMD_PID, SE_DATA_TYPE_UINT32, getpid(),
	    CL_EVENT_CMD_PPID, SE_DATA_TYPE_UINT32, getppid(),
	    CL_EVENT_CMD_LOGIN, SE_DATA_TYPE_STRING, loginname,
	    CL_EVENT_CMD_TTY_STDIN, SE_DATA_TYPE_STRING, "?",
	    CL_EVENT_CMD_TTY_STDOUT, SE_DATA_TYPE_STRING, "?",
	    CL_EVENT_CMD_NAME, SE_DATA_TYPE_STRING, basename(*args),
	    CL_EVENT_CMD_OPT, SE_DATA_TYPE_STRING, cmd_opt,
	    CL_EVENT_CMD_RES, SE_DATA_TYPE_INT32, *exit_val,
	    NULL);
	/*lint +e793 */
}

/*
 * cl_cmd_event_gen_end_event()
 *
 * Input: none
 *
 * Action: Calls sc_publish_event() via internal functions
 * to generate an event. It verifies that cl_cmd_event_init()
 * has been already called. So cl_cmd_event_init() should be
 * called prior to the first use of this function.
 * This function is typically registered with atexit()
 * for generating an event at the completion of a CLI.
 *
 * Return: none.
 */
void
cl_cmd_event_gen_end_event(void) {

	if (init_called) {
		if (ttyname(0) && ttyname(1)) {
			generate_event(CL_EVENT_PUB_CMD_END);
		} else {
			generate_event_1(CL_EVENT_PUB_CMD_END);
		}
	} else {
		cl_ev_err = 5;
	}
}

/*
 * cl_cmd_event_gen_start_event()
 *
 * Input: none
 *
 * Action: Calls sc_publish_event() via internal functions
 * to generate an event. It verifies that cl_cmd_event_init()
 * has been already called. So cl_cmd_event_init() should be
 * called prior to the first use of this function.
 * This function is typically used for generating an event at
 * the start of a CLI.
 *
 * Return: cl_event_error_t
 */
cl_event_error_t
cl_cmd_event_gen_start_event(void) {

	if (init_called) {
		if (ttyname(0) && ttyname(1)) {
			generate_event(CL_EVENT_PUB_CMD_START);
		} else {
			generate_event_1(CL_EVENT_PUB_CMD_START);
		}
	} else {
		cl_ev_err = 5;
	}

	return (cl_ev_err);
}
