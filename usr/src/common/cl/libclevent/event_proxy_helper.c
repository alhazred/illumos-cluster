/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)event_proxy_helper.c	1.2	08/05/20 SMI"

#include "sc_event_proxy/sc_event_proxy.h"
#include <string.h>
#include <rgm/security.h>

/*
 * How we calculate size for xdr encoding:
 * ---------------------------------------
 * The size calculation that we do here has to calculate
 * the size of the structure members and also the size of
 * the data that is pointed to by the pointers if any
 * in the structure.
 * For eg:
 * struct abc {
 *      int a;
 *      char *ch;
 * }
 * in this case we will find the size of the struct abc
 * which will give us the size needed for storing the
 * elements that is an int and a char pointer. And also
 * we will need to add the size of the string pointed to
 * by ch.
 * In addition to this, xdr encodes string in the following
 * way:
 * [integer(for size of the string) + string]
 * So, for any string say char *ch = {"example"}
 * The size needed will be :
 * sizeof (ch) + sizeof (int) + safe_strlen(ch)
 * Here the sizeof (int) is taken for storing the sizeof
 * the string.
 * We also use the RNDUP macro to round up the number to the
 * nearest word boundary. And the use of safe_strlen takes
 * care of NULL strings and also takes into account the space
 * needed for the null terminator, which strlen doesnt.
 */

size_t
proxy_event_data_xdr_sizeof(proxy_event_data event_data)
{
	return (RNDUP(sizeof (proxy_event_data)) +
	    RNDUP(safe_strlen(event_data.subclass)) +
	    RNDUP(sizeof (int)) +
	    RNDUP(safe_strlen(event_data.publisher)) +
	    RNDUP(sizeof (int)));
}
