/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)comm.cc	1.7	08/07/14 SMI"

#include <sys/cladm_int.h>
#include <sys/systm.h>
#include <sys/errno.h>
#include <sys/sol_version.h>
#include <sys/cred.h>
#include <sys/vnode.h>
#include <sys/modctl.h>

#if (SOL_VERSION >= __s10)
#include <sys/policy.h>
#include <sys/priv.h>
#ifdef _KERNEL
#include <sys/zone.h>
#endif // _KERNEL
#endif

#ifdef _KERNEL
#include <sys/file.h>
#include <sys/cladm_debug.h>
#include <sys/cl_rgm.h>
#include <sys/sunddi.h>
#endif

#include <sys/ddi.h>
#include <sys/door.h>
#include <sys/pathname.h>
#include <orb/fault/fault_rpc.h>
#include <orb/debug/orb_stats_mgr.h>
#include <orb/flow/resource_balancer.h>
#include <orb/refs/unref_stats.h>
#include <orb/debug/orbadmin_state.h>
#include <sys/vm_util.h>
#include <cmm/cmm_impl.h>

#include <sys/clconf_int.h>
#include <sys/kmem.h>

/*
 * cladm(2) - cluster administation system call.
 * This replaces the stub defined in the base kernel.
 */
#ifndef _KERNEL
#include <stdio.h>
#include <sys/cl_assert.h>
#include <unode/systm.h>
#include <errno.h>
#include <door.h>
#define	set_errno(e) (errno = e, -1)
#define	CRED() (NULL)
#define	closeandsetf(f, y) close(f)
#endif


/*
 * There are a couple of lines where we do a lint override as follows :
 * if (get_udatamodel() == DATAMODEL_NATIVE) {	lint !e774
 *
 * This is because in IPL32 get_udatamodel() is a #define constant and lint
 * warns about the if statement always evaluating to true
 */

/*
 * There are a couple of lines with lint override for lint error 507
 * This is in code that should be removed by FCS, bugid 4216877
 * If this code is not removed by FCS, then this code should be fixed
 */

/*
 * These routines are defined in the pxfs module and defined here as pointers
 * to avoid module dependency loops.
 */
int (*pxfs_mount_client_enable_ptr)(int cmd, int fd);
int (*pxfs_mount_client_lock_ptr)(int cmd, struct pathname *devpnp);
void (*pxfs_disable_unmounts_ptr)(void);

extern int copyin_clcluster_name(void *, clcluster_name_t *);
extern int copyout_clcluster_name(clcluster_name_t *, void *);
extern int copyin_clnode_name(void *uaddr, clnode_name_t *namep);
extern int copyout_clnode_name(clnode_name_t *namep, void *uaddr);

#ifdef _KERNEL
extern int copyin_clconf_get_ccr_entry(void *uaddr, get_ccr_entry_t *argp);
extern int copyout_clconf_get_ccr_entry(get_ccr_entry_t *argp, void *uaddr);
extern int copyin_cladmin_log(void *, cladmin_log_t **);
#if (SOL_VERSION >= __s10)
extern clconf_errnum_t privip_get_ccr(uint_t *, char *, size_t);
extern int copyin_privip_get_ccr(void *uaddr, get_ccr_privip_t *argp);
extern int copyout_privip_get_ccr(get_ccr_privip_t *argp, void *uaddr);
extern int copyin_cz_subnet(const void *uaddr, cz_subnet_t *subnetp);
extern int copyout_cz_subnet(const cz_subnet_t *subnetp, void *uaddr);
extern clconf_errnum_t get_cz_subnet(cz_subnet_t *);
#endif
#endif

extern int copyout_get_scshutdown_state(bool *argp, void *uaddr);

#if (SOL_VERSION >= __s10)
extern int copyin_zcnode_name(const void *uaddr, zcnode_name_t *namep);
extern int copyout_zcnode_name(const zcnode_name_t *namep, void *uaddr);
extern int copyin_cz_name(const void *uaddr, cz_name_t *namep);
extern int copyout_cz_name(const cz_name_t *namep, void *uaddr);
extern int copyin_cz_id(const void *uaddr, cz_id_t *czidp);
extern int copyout_cz_id(const cz_id_t *czidp, void *uaddr);
#endif // (SOL_VERSION >= __s10)

extern int copyin_clconf_get_clconf_child_stats(void *uaddr,
	get_clconf_child_stats_t *argp);
extern int copyout_clconf_get_clconf_child_stats(get_clconf_child_stats_t *argp,
	void *uaddr);

extern int copyin_clconf_get_proto_vers_num(void *uaddr,
	get_clconf_proto_vers_num_t *argp);
extern int copyout_clconf_get_proto_vers_num(get_clconf_proto_vers_num_t *argp,
	void *uaddr);

#ifdef _KERNEL
	extern "C" door_handle_t gateway_get_local_ns(void);

#if SOL_VERSION >= __s10
	struct rgm_door_info_t rgm_door_info[RGM_MAX_DOOR_SYSCALL_CODE + 1]
	    = {0};
#endif

#else
	extern "C" int gateway_get_local_ns(void);
#endif
extern "C" void get_local_ns_typeid(uint32_t *);
extern int clconfig(int, void *);

//
// Solaris looks specifically for cladmin when loading cl_comm. It
// is for this reason that the entry point is provided in this module.
//
extern "C" int
cladmin(int fac, int cmd, void *arg)
{
	int error;

	switch (fac) {
	case CL_CONFIG:
		error = clconfig(cmd, arg);
		break;

	default:
		error = EINVAL;
		break;
	}

	/*
	 * lint error is about a loss of sign from int to unsigned when passing
	 * error to set_error()
	 */
	ASSERT(error >= 0);
	return (error ? (int)set_errno(error) : 0);	/*lint !e732 */
}

//
// clconfig_clmode implements the parts of the clconfig that need
// the cluster to be initialized, and in particular, the various
// cluster modules to be loaded.
//
extern "C" int
clconfig_clmode(int cmd, void *arg)
{
	int ret = 0;
	clcluster_name_t clustname;
	clnode_name_t nodename;
#if (SOL_VERSION >= __s10) && !defined(UNODE)
	cz_name_t		czname;
	zcnode_name_t		zcnode_name;
	cz_id_t			czid;
	cz_subnet_t		cznet_arg;
	char 			*clnamep = NULL;
	zoneid_t		zoneid;
	zone_t			*zonep = NULL;
#endif // (SOL_VERSION >= __s10) && !defined(UNODE)
	cllocal_ns_data_t ns_data;
	const char *cp;
	int fd;
#ifdef _KERNEL
	struct file *fp;
	vnode_t *vp;
	char path[MAXPATHLEN];

#if (SOL_VERSION >= __s10)
	char zonename[ZONENAME_MAX];
#else
	char zonename[7]; // "global"
#endif
	int door_code;
#else
	door_info_t dinfo;
	extern int cluster_bootflags;
#endif // _KERNEL

	clconf_cluster_t *cl;
	clconf_node_t *nd;
	get_clconf_child_stats_t clconf_child_stats_arg;
#ifdef _KERNEL
	get_ccr_entry_t ccr_entry_arg;
#if (SOL_VERSION >= __s10)
	get_ccr_privip_t ccr_privip_arg;
#endif
	struct pathname lookpn;
	char *gdev_prefix;
#endif
	get_clconf_proto_vers_num_t clconf_proto_vers_arg;
	version_manager::vm_admin_ptr vm_adm_p;
	version_manager::vp_version_t vpv;
	nodeid_t remote_node;
	Environment e;
	CORBA::Exception *ex;
	bool is_scshutdown_arg;

	switch (cmd) {
	case CL_GBLMNT_ENABLE:
#ifdef _KERNEL
		/*
		 * Enable or disable participation in the cluster-wide
		 * global file system name space according to the value of
		 * the int pointed at by arg.
		 * Return 0 on success or an errno value on failure.
		 */
		ret = os::drv_priv(CRED());
		if (ret != 0)
			break;
		if (copyin(arg, &fd, sizeof (int)) != 0) {
			ret = EFAULT;
			break;
		}
		/* Check to be sure modules are loaded. */
		if (pxfs_mount_client_enable_ptr == NULL) {
			if (modload("fs", "pxfs") == -1 ||
			    pxfs_mount_client_enable_ptr == NULL) {
				ret = EINVAL;
				break;
			}
		}
		if (fd != 0) {
			/*
			 * Start up device services and
			 * then join the global name space.
			 */
			ret = (*pxfs_mount_client_enable_ptr)(cmd, fd);
		} else {
			/* Widthdraw from the global name space. */
			ret = ENOTSUP;
		}
#else
		//
		// Global devices not supported in UNODE
		//
		ret = ENOTSUP;
#endif
		break;

	case CL_SWITCHBACK_ENABLE:
#ifdef _KERNEL
		/*
		 * Initiate switchbacks.
		 */

		/* Check to be sure modules are loaded. */
		if (pxfs_mount_client_enable_ptr == NULL) {
			ret = EINVAL;
			break;
		}

		ret = (*pxfs_mount_client_enable_ptr)(cmd, 0);
#else
		//
		// PXFS not supported in UNODE
		//
		ret = ENOTSUP;
#endif
		break;

	case CL_GBLMNT_LOCK:
	case CL_GBLMNT_UNLOCK:
#ifdef _KERNEL
		/*
		 * This call is used by the boot RC scripts to notify
		 * the mount service that a device should be locked
		 * so only one node is allowed to fsck and mount it.
		 * Return 0 on success or an errno value on failure.
		 */
		ret = os::drv_priv(CRED());
		if (ret != 0)
			break;

		/* Check to be sure modules did load OK. */
		if (pxfs_mount_client_lock_ptr == NULL) {
			ret = EINVAL;
			break;
		}

		/* Read the device pathname into the kernel. */
		ret = pn_get((char *)arg, UIO_USERSPACE, &lookpn);
		if (ret != 0)
			break;

		ret = (*pxfs_mount_client_lock_ptr)(cmd, &lookpn);

		pn_free(&lookpn);
#else
		//
		// Global devices not supported in UNODE
		//
		ret = ENOTSUP;
#endif
		break;

	case CL_UNMOUNT_DISABLE:
#ifdef _KERNEL
		/*
		 * There is just a single writer to 'pxfs_disable_unmounts_ptr'
		 * (during modload of kernel/fs/pxfs), so we don't need locking.
		 */
		if (pxfs_disable_unmounts_ptr == NULL) {
			ret = EINVAL;
			break;
		}

		(*pxfs_disable_unmounts_ptr)();
		ret = 0;
#else
		//
		// PXFS not supported in UNODE
		//
		ret = ENOTSUP;
#endif
		break;

	case CL_RGM_STORE_DOOR:
		ret = EINVAL;
#ifdef _KERNEL
#if (SOL_VERSION >= __s10)
		ret = rgm_store_door_copy_args(arg, &door_code, zonename, path);
		if (ret != 0) {
			break;
		}
		door_handle_t h;
		// this gives us the vnode for the path, not the door.
		ret = door_ki_open(path, &h);
		if (ret == 0) {
			rgm_store_door(door_code, h, zonename);
		}
		//
		// Once handle is create, the value is never changed. Hence we
		// avoid any possibility of race condition with clients. Hence
		// no locking necessary.
		// Currently, the vnode is permanently held in the system till
		// it is brought down, even if the server creating it has
		// died. The node will anyway come down very soon due
		// to failfast.
		//
		// Alternately, We can have another syscall, to release the
		// vnode (may be using door_ki_rele/door_ki_close) or simply
		// VN_RELE, and other related code. If that is ever done, we
		// will need to implement locking for accessing the handles
		// in the CL_RGM_GET_DOOR call.
		//
#endif
#endif
		break;
	case CL_RGM_GET_DOOR:
		ret = EINVAL;
#ifdef _KERNEL
#if (SOL_VERSION >= __s10)
		ret = rgm_get_door_copy_args(arg, &door_code, zonename);
		if (ret != 0)
			break;

		fp = (file_t *)rgm_get_door(door_code, zonename);
		if (fp == NULL) {
			// program is not yet registered.
			ret = ESRCH;
			break;
		}
		vp = fp->f_vnode;
		//
		// call VN_HOLD because fassign doesn't bump up the reference,
		// however later closef on fd will call VN_RELE
		//
		VN_HOLD(vp);

		if ((ret = fassign(&vp, FREAD, &fd)) != 0) {
			VN_RELE(vp);
			break;
		}
		ret = ddi_copyout(&fd, arg, sizeof (int), 0);
		if (ret != 0) {
			(void) closeandsetf(fd, NULL);
			ret = EFAULT;
			break;
		}
#endif
#endif
		break;

	case CL_GET_LOCAL_NS:
	case CL_GET_NS_DATA:
		/*
		 * One should not be able to get a pointer to the nameserver
		 * if non-root. This is the primary way we prevent
		 * unprivileged use of the ORB's object framework
		 */

#ifdef _KERNEL
#if (SOL_VERSION >= __s10)
		ret = priv_policy(CRED(),
			    priv_getbyname(PRIV_SYS_ADMIN, 0), B_FALSE,
			    EPERM, NULL);
#else
		ret = os::drv_priv(CRED());
#endif // SOL_VERSION >= __s10

		if (ret != 0)
			break;
		fp = (file_t *)gateway_get_local_ns();
		if (fp == NULL) {
			ret = ESRCH;
			break;
		}
		if ((fd = ufalloc(0)) == -1) {
			(void) closef(fp);
			ret = EMFILE;
			break;
		}
		setf(fd, fp);
#else
		if ((fd = gateway_get_local_ns()) == NULL) {
			ret = ESRCH;
			break;
		}
#endif
		ns_data.filedesc = fd;
#ifndef _KERNEL
		if (door_info(fd, &dinfo) < 0) {
			perror("door_info");
			ASSERT(0);
		}
#endif
		get_local_ns_typeid(ns_data.tid);
		if (cmd == CL_GET_LOCAL_NS) {
			ret = copyout(&fd, arg, sizeof (int));
		} else {
			ret = copyout(&ns_data, arg, sizeof (ns_data));
		}
		if (ret != 0) {
			/* Reset fd and close fp. */
			(void) closeandsetf(fd, NULL);
			ret = EFAULT;
			break;
		}
		break;

	case CL_GET_CLUSTER_NAME:
		if (copyin_clcluster_name(arg, &clustname)) {
			ret = EFAULT;
			break;
		}

		cl = clconf_cluster_get_current();
		cp = clconf_obj_get_name((clconf_obj_t *)cl);
		if (cp == NULL) {
			clustname.len = 0;
		} else if (copyoutstr((char *)cp, clustname.name,
		    clustname.len, &clustname.len)) {
			clconf_obj_release((clconf_obj_t *)cl);
			ret = EFAULT;
			break;
		}
		if (copyout_clcluster_name(&clustname, arg)) {
			ret = EFAULT;
		}
		clconf_obj_release((clconf_obj_t *)cl);

		break;

	case CL_GET_NODE_NAME:
		if (copyin_clnode_name(arg, &nodename)) {
			ret = EFAULT;
			break;
		}

		if (nodename.nodeid == NODEID_UNKNOWN ||
		    nodename.nodeid > NODEID_MAX) {
			ret = EINVAL;
			break;
		}

		cl = clconf_cluster_get_current();
		if (cl == NULL) {
			ret = EFAULT;
			break;
		}

		nd = (clconf_node_t *)clconf_obj_get_child((clconf_obj_t *)cl,
		    CL_NODE, (int)nodename.nodeid);
		cp = clconf_obj_get_name((clconf_obj_t *)nd);
		if (cp == NULL) {
			nodename.len = 0;
#ifdef _KERNEL
		} else if (copyoutstr((char *)cp, nodename.name,
		    nodename.len, &nodename.len)) {
			clconf_obj_release((clconf_obj_t *)cl);
			ret = EFAULT;
			break;
		}
#else
		} else {
			if (strlen(cp) < nodename.len) {
				nodename.len = strlen(cp);
			}
			(void) strncpy(nodename.name, cp, nodename.len + 1);
		}
#endif
		if (copyout_clnode_name(&nodename, arg)) {
			ret = EFAULT;
		}

		clconf_obj_release((clconf_obj_t *)cl);

		break;

	case CL_GET_CCR_ENTRY:
		//
		// Here we call a clconf interface to read an entry from
		// the CCR. The clconf interface does IDL invocations on
		// the directory object to read the entry.
		// Since an IDL invocation has the cluster id info,
		// so we do not need to add an extra check here.
		// The IDL invocation to CCR directory would read
		// the CCR table corresponding to the current cluster.
		//
#ifdef _KERNEL
		if (copyin_clconf_get_ccr_entry(arg, &ccr_entry_arg)) {
			ret = EFAULT;
			break;
		}

		ccr_entry_arg.rslt = clconf_get_ccr_entry(
		    ccr_entry_arg.table_name, ccr_entry_arg.entry_name,
		    ccr_entry_arg.buf, ccr_entry_arg.buf_len);

		if (copyout_clconf_get_ccr_entry(&ccr_entry_arg, arg)) {
			ret = EFAULT;
			break;
		}
#else
		//
		// XXX CCR not supported?
		//
		ret = ENOTSUP;
#endif
		break;

	case CL_GET_CCR_PRIVIP:
#if defined(_KERNEL) && (SOL_VERSION >= __s10)
		if (copyin_privip_get_ccr(arg, &ccr_privip_arg)) {
			ret = EFAULT;
			break;
		}

		ccr_privip_arg.rslt = privip_get_ccr(&(ccr_privip_arg.count),
		    ccr_privip_arg.buf, ccr_privip_arg.buf_len);

		if (copyout_privip_get_ccr(&ccr_privip_arg, arg)) {
			ret = EFAULT;
			break;
		}
#else
		//
		// XXX CCR not supported?
		//
		ret = ENOTSUP;
#endif
		break;

	case CL_GET_SCSHUTDOWN:

		is_scshutdown_arg = cmm_impl::the().get_cluster_shutdown();

		if (copyout_get_scshutdown_state(&is_scshutdown_arg, arg)) {
			ret = EFAULT;
		}

		break;

	case CL_GET_CLCONF_CHILD_STATS:
		if (copyin_clconf_get_clconf_child_stats(arg,
		    &clconf_child_stats_arg)) {
			ret = EFAULT;
			break;
		}

		clconf_child_stats_arg.rslt = clconf_get_clconf_child_stats(
		    clconf_child_stats_arg.path_len,
		    clconf_child_stats_arg.path_node_types,
		    clconf_child_stats_arg.path_node_indices,
		    clconf_child_stats_arg.child_type,
		    &clconf_child_stats_arg.child_count,
		    &clconf_child_stats_arg.max_child_index);

		if (copyout_clconf_get_clconf_child_stats(
		    &clconf_child_stats_arg, arg)) {
			ret = EFAULT;
			break;
		}
		break;

	case CL_GET_VP_RUNNING_VERSION:

		if (copyin_clconf_get_proto_vers_num(arg,
		    &clconf_proto_vers_arg)) {
			ret = EFAULT;
			break;
		}

		/* get vm_admin object */
		vm_adm_p = vm_util::get_vm();
		if (CORBA::is_nil(vm_adm_p)) {
			/* Something has gone wrong; return error; */
			ret = EAGAIN;
			break;
		}

		remote_node = clconf_proto_vers_arg.nodeid;

		/* Retrieve protocol version depending on type */
		if (clconf_proto_vers_arg.mode == VM_NODE_OR_CLUSTER) {
			vm_adm_p->get_running_version(
			    clconf_proto_vers_arg.vp_name, vpv, e);
		} else if (clconf_proto_vers_arg.mode == VM_NODEPAIR) {
			vm_adm_p->get_nodepair_running_version(
			    clconf_proto_vers_arg.vp_name, remote_node,
			    INCN_UNKNOWN, vpv, e);
		} else {
			ret = EINVAL;
			break;
		}
		if ((ex = e.exception()) != NULL) {
			/* Something has gone wrong; return error; */
			if (version_manager::old_incarnation::_exnarrow(ex))
				ret = ESTALE;
			else if (version_manager::not_found::_exnarrow(ex))
				ret = ENOENT;
			else if	(version_manager::wrong_mode::_exnarrow(ex))
				ret = EINVAL;
			else if	(version_manager::too_early::_exnarrow(ex))
				ret = EAGAIN;
			else ASSERT(0);	/* should never come here */
			break;
		}

		/* copy the major and minor number to the output struct */
		clconf_proto_vers_arg.maj_num = vpv.major_num;
		clconf_proto_vers_arg.min_num = vpv.minor_num;

		if (copyout_clconf_get_proto_vers_num(&clconf_proto_vers_arg,
		    arg)) {
			ret = EFAULT;
			break;
		}
		break;

	case CL_GDEV_PREFIX:
#ifdef _KERNEL
		gdev_prefix = (char *)kmem_alloc((size_t)MAXPATHLEN,
		    KM_SLEEP);
		(void) sprintf(gdev_prefix, "/global/.devices/node@%d",
		    clconf_get_local_nodeid());
		if (copyoutstr(gdev_prefix, (char *)arg,
		    (size_t)MAXPATHLEN, NULL))
			ret = EFAULT;
		kmem_free(gdev_prefix, (size_t)MAXPATHLEN);
#else
		//
		// Global devices not supported in UNODE
		//
		ret = ENOTSUP;
#endif
		break;

#ifdef _FAULT_INJECTION
	case CL_FAULT_RPC:
	case CL_FAULT_GET_NODETRIG:
	case CL_FAULT_DO_SETUP_WAIT:
	case CL_FAULT_DO_WAIT:
	case CL_FAULT_WAIT:
	case CL_FAULT_SIGNAL:
	case CL_FAULT_REBOOT_ALL_NODES:
	case CL_FAULT_RM_PRIMARY_NODE:
	case CL_FAULT_FAILPATH:
		/*
		 * Fault injection framework support.
		 * Pass work to cl_fault_injection().
		 */
		ret = cl_fault_injection(cmd, arg);
		break;
#endif
#if (SOL_VERSION >= __s10)
	case CL_GET_ZC_NODE_NAME:
	/* Get the zone cluster node name given the zone cluster node id */
#ifdef	_KERNEL
		if (copyin_zcnode_name(arg, &zcnode_name)) {
			ret = EFAULT;
			break;
		}
		zoneid = getzoneid();
		zonep = zone_find_by_id(zoneid);

		if ((zoneid != 0) &&
		    os::strcmp(zonep->zone_name, zcnode_name.zc_name) != 0) {
				//
				// Trying to access zone cluster
				// information from a zone that does not
				// belong to the zone cluster.
				// We fail this attempt here.
				//
				zone_rele(zonep);
				ret = EACCES;
				break;
		}
		cl = clconf_cluster_get_vc_current(zcnode_name.zc_name);
		if (cl == NULL) {
			ret = EFAULT;
			zone_rele(zonep);
			break;
		}
		nd = (clconf_node_t *)clconf_obj_get_child((clconf_obj_t *)cl,
		    CL_NODE, (int)zcnode_name.nodeid);
		cp = clconf_obj_get_name((clconf_obj_t *)nd);
		if (cp == NULL) {
			// lookup failed.
			zcnode_name.len = 0;
		} else if (copyoutstr((char *)cp, zcnode_name.namep,
		    zcnode_name.len, &zcnode_name.len)) {
				clconf_obj_release((clconf_obj_t *)cl);
				zone_rele(zonep);
				ret = EFAULT;
				break;
		}
		if (copyout_zcnode_name(&zcnode_name, arg)) {
			ret = EFAULT;
		}
		zone_rele(zonep);
		clconf_obj_release((clconf_obj_t *)cl);
		break;
#else
		//
		// clusterized zone is not supported in UNODE
		//
		ret = ENOTSUP;
		break;
#endif /* _KERNEL */

	/* Get the clusterized zone name from the cluster id. */
	case CL_GET_ZC_NAME:
#ifdef _KERNEL
		if (copyin_cz_name(arg, &czname)) {
			ret = EFAULT;
			break;
		}
		zoneid = getzoneid();
		zonep = zone_find_by_id(zoneid);
		ret = clconf_get_cluster_name(czname.clid, &clnamep);
		if (ret != 0) {
			// lookup failed.
			czname.len = 0;
		} else {
			if ((zoneid != 0) &&
			    os::strcmp(zonep->zone_name, clnamep) != 0) {
				//
				// Trying to access cluster-wide zone
				// information from a zone that does not
				// belong to the cluster-wide zone.
				// We fail this attempt here.
				//
				delete [] clnamep;
				zone_rele(zonep);
				ret = EACCES;
				break;
			}
			if (copyoutstr(clnamep, czname.namep, czname.len,
			    &czname.len)) {
				delete [] clnamep;
				zone_rele(zonep);
				ret = EFAULT;
				break;
			}
		}
		if (copyout_cz_name(&czname, arg)) {
			ret = EFAULT;
		}
		if (clnamep != NULL) {
			delete [] clnamep;
		}
		zone_rele(zonep);
#else
		//
		// clusterized zone is not supported in UNODE
		//
		ret = ENOTSUP;
#endif // _KERNEL
		break;

	/* Get the clusterized zone id from the name. */
	case CL_GET_ZC_ID:
#ifdef _KERNEL
		if (copyin_cz_id(arg, &czid)) {
			ret = EFAULT;
			break;
		}
		ret = clconf_get_cluster_id(czid.name, &czid.clid);

		if (copyout_cz_id(&czid, arg)) {
			ret = EFAULT;
		}
#else
		//
		// clusterized zone is not supported in UNODE
		//
		ret = ENOTSUP;
#endif // _KERNEL
		break;

	/* Get the clusterized zone subnet from the name. */
	case CL_GET_CZ_SUBNET:
#ifdef _KERNEL

		if (copyin_cz_subnet(arg, &cznet_arg)) {
			ret = EFAULT;
			break;
		}
		ret = get_cz_subnet(&cznet_arg);

		if (ret != 0) {
			ret = EINVAL;
			break;
		}

		if (copyout_cz_subnet(&cznet_arg, arg)) {
			ret = EFAULT;
			break;
		}
#else
		//
		// clusterized zone is not supported in UNODE
		//
		ret = ENOTSUP;
#endif // _KERNEL
		break;
#endif // (SOL_VERSION >= __s10)

	/* Read orb message statistics */
	case CL_ORBADMIN_STATS_MSG:
		ret = cl_read_orb_stats_msg(arg);
		break;

	/* Read orb flow control statistics */
	case CL_ORBADMIN_STATS_FLOW:
		ret = cl_read_orb_stats_flow(arg);
		break;

	/* Read orb unref statistics */
	case CL_ORBADMIN_STATS_UNREF:
		ret = cl_read_orb_stats_unref(arg);
		break;

	/* Read flow control settings */
	case CL_ORBADMIN_FLOW_READ:
		ret = cl_read_flow_settings(arg);
		break;

	/* Change flow control settings */
	case CL_ORBADMIN_FLOW_CHANGE:
		ret = cl_change_flow_settings(arg);
		break;

	/* Read flow control server threads minimum setting */
	case CL_ORBADMIN_THREADS_MIN_READ:
		ret = cl_read_threads_min(arg);
		break;

	/* Change flow control server threads minimum setting */
	case CL_ORBADMIN_THREADS_MIN_CHANGE:
		ret = cl_change_threads_min(arg);
		break;

	/*  Read orb resource_pool state */
	case CL_ORBADMIN_STATE_POOL:
		ret = cl_read_orb_state_pool(arg);
		break;

	/*  Read orb resource_balancer state */
	case CL_ORBADMIN_STATE_BALANCER:
		ret = cl_read_orb_state_balancer(arg);
		break;

	/*  Read orb unref_threadpool state */
	case CL_ORBADMIN_STATE_UNREF:
		ret = cl_read_orb_state_unref(arg);
		break;

	/*  Read orb reference count state */
	case CL_ORBADMIN_STATE_REFCOUNT:
		ret = cl_read_orb_state_refcount(arg);
		break;

	default:
		ret = EINVAL;
	}

	return (ret);
}
