/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)cladmin.cc	1.87	08/08/01 SMI"

#include <sys/cladm_int.h>
#include <sys/systm.h>
#include <sys/errno.h>
#include <sys/sol_version.h>
#include <sys/cred.h>
#include <sys/vnode.h>
#include <sys/modctl.h>
#include <sys/cl_comm_support.h>

#if (SOL_VERSION >= __s10)
#include <sys/policy.h>
#include <sys/priv.h>
#endif

#ifdef _KERNEL
#include <sys/file.h>
#include <sys/cladm_debug.h>
#endif

#include <sys/pathname.h>
#include <sys/clconf_int.h>
#include <sys/kmem.h>

/*
 * There are a couple of lines where we do a lint override as follows :
 * if (get_udatamodel() == DATAMODEL_NATIVE) {	lint !e774
 *
 * This is because in IPL32 get_udatamodel() is a #define constant and lint
 * warns about the if statement always evaluating to true
 */

/*
 * There are a couple of lines with lint override for lint error 507
 * This is in code that should be removed by FCS, bugid 4216877
 * If this code is not removed by FCS, then this code should be fixed
 */

/*
 * These routines are defined in the pxfs module and defined here as pointers
 * to avoid module dependency loops.
 */
int (*pxfs_mount_client_enable_ptr)(int cmd, int fd);
int (*pxfs_mount_client_lock_ptr)(int cmd, struct pathname *devpnp);
void (*pxfs_disable_unmounts_ptr)(void);

int clconfig(int cmd, void *arg);

int copyin_clcluster_name(void *, clcluster_name_t *);
int copyout_clcluster_name(clcluster_name_t *, void *);

int copyin_clnode_name(void *uaddr, clnode_name_t *namep);
int copyout_clnode_name(clnode_name_t *namep, void *uaddr);

#ifdef _KERNEL
int copyin_clconf_get_ccr_entry(void *uaddr, get_ccr_entry_t *argp);
int copyout_clconf_get_ccr_entry(get_ccr_entry_t *argp, void *uaddr);
static int copyin_cladmin_log(void *, cladmin_log_t **);

#if (SOL_VERSION >= __s10)
int copyin_privip_get_ccr(void *uaddr, get_ccr_privip_t *argp);
int copyout_privip_get_ccr(get_ccr_privip_t *argp, void *uaddr);
int copyin_zcnode_name(const void *uaddr, zcnode_name_t *namep);
int copyout_zcnode_name(zcnode_name_t *namep, void *uaddr);
int copyin_cz_name(const void *uaddr, cz_name_t *namep);
int copyout_cz_name(const cz_name_t *namep, void *uaddr);
int copyin_cz_id(const void *uaddr, cz_id_t *czidp);
int copyout_cz_id(const cz_id_t *czidp, void *uaddr);
int copyin_cz_subnet(const void *uaddr, cz_subnet_t *subnetp);
int copyout_cz_subnet(const cz_subnet_t *subnetp, void *uaddr);
#endif // (SOL_VERSION >= __s10)
#endif // _KERNEL

int copyout_get_scshutdown_state(bool *argp, void *uaddr);

int copyin_clconf_get_clconf_child_stats(void *uaddr,
	get_clconf_child_stats_t *argp);
int copyout_clconf_get_clconf_child_stats(get_clconf_child_stats_t *argp,
	void *uaddr);

int copyin_clconf_get_proto_vers_num(void *uaddr,
	get_clconf_proto_vers_num_t *argp);
int copyout_clconf_get_proto_vers_num(get_clconf_proto_vers_num_t *argp,
	void *uaddr);

/*
 * cladm(2) - cluster administation system call.
 * This replaces the stub defined in the base kernel.
 */
#ifndef _KERNEL
#include <stdio.h>
#include <sys/cl_assert.h>
#include <unode/systm.h>
#include <errno.h>
#include <door.h>
#define	CRED() (NULL)
#endif

/*
 * Handle the CL_CONFIG facility commands for getting & setting
 * cluster configuration data.
 * Return 0 on success, errno value if error.
 */
int
clconfig(int cmd, void *arg)
{
	int ret = 0;
	nodeid_t nid;
	int fd;
#ifndef _KERNEL
	extern int cluster_bootflags;
#endif

	/*
	 * If we are not booted as a cluster, pretend the cladm() system call
	 * doesn't exist.
	 * XXX This code is here so we can allow the cl_comm module to be
	 * loaded without returning an error to workaround bug 4192014.
	 */
	if ((cluster_bootflags & CLUSTER_BOOTED) == 0) {
		return (ENOSYS);
	}

	/*
	 * CL_ADMIN_LOG can be called before orb is initialized by the
	 * userland caller since logging does not use any orb operation.
	 */
	if (cmd == CL_ADMIN_LOG) {
#ifdef _KERNEL
		cladmin_log_t *logp;
		if (copyin_cladmin_log(arg, &logp) != 0) {
			kmem_free(logp, sizeof (cladmin_log_t));
			return (EFAULT);
		}
		CLADM_DBPRINTF(CLADM_TRACE_CLEXEC, logp->level, (logp->str));
		kmem_free(logp, sizeof (cladmin_log_t));
		return (0);
#else
		return (ENOTSUP);
#endif
	}

	/*
	 * Return error if ORB isn't initialized and operation requires
	 * ORB to be initialized.
	 */
	if (cmd != CL_HIGHEST_NODEID && cmd != CL_CLUSTER_ENABLE &&
	    !cl_orb_is_initialized_funcp()) {
		return (EINVAL);
	}

	switch (cmd) {
	case CL_HIGHEST_NODEID:
		/*
		 * Always return NODEID_MAX. This allows user programs that
		 * do not link with the orb to get at this constant.
		 */
		nid = NODEID_MAX;
		if (copyout(&nid, arg, sizeof (nid)))
			ret = EFAULT;
		break;

	case CL_CLUSTER_ENABLE:
		/* Only root is allowed to join/leave the cluster. */
		ret = os::drv_priv(CRED());
		if (ret != 0)
			break;
		if (copyin(arg, &fd, sizeof (int)) != 0) {
			ret = EFAULT;
			break;
		}

		//
		// Load any modules that are required for
		// orb initialization, i.e. cl_orb, cl_haci,
		// cl_quroum, cl_comm.
		//
#ifdef _KERNEL
		if (modload("misc", "cl_load") == -1) {
			ret = EINVAL;
			break;
		}
		if (modload("misc", "cl_orb") == -1) {
			ret = EINVAL;
			break;
		}
		if (modload("misc", "cl_haci") == -1) {
			ret = EINVAL;
			break;
		}
		if (modload("misc", "cl_quorum") == -1) {
			ret = EINVAL;
			break;
		}
		if (modload("misc", "cl_comm") == -1) {
			ret = EINVAL;
			break;
		}
#endif

		if (fd != 0) {
			/* Join the cluster (startup). */
			ret = cl_orb_initialize_funcp();
			if (ret != 0)
				break;
#ifdef _KERNEL
			if (modload("misc", "cl_dcs") == -1) {
				ret = EINVAL;
				break;
			}
#endif
		} else {
			/* Widthdraw from the cluster (shutdown). */
			ret = ENOTSUP;
		}
		break;

	default:
		//
		// Everything else must be done later in the boot process
		// and handled by clconfig_clmode().
		ret = clconfig_clmode_funcp(cmd, arg);
	}

	return (ret);
}

int
#ifdef _KERNEL
copyin_clcluster_name(void *uaddr, clcluster_name_t *namep)
#else
copyin_clcluster_name(void *, clcluster_name_t *)
#endif
{
#ifdef _KERNEL
	if (get_udatamodel() == DATAMODEL_NATIVE) {	/*lint !e774 */
		return (copyin(uaddr, namep, sizeof (*namep)));
	} else {
		clcluster_name32_t name32;
		int err;

		if ((err = copyin(uaddr, &name32, sizeof (name32))) != 0) {
			return (err);
		}
		namep->len = (size_t)name32.len;
		namep->name = (char *)name32.name;
		return (0);
	}
#else
	ASSERT(0);
	return (0);
#endif
}

int
#ifdef _KERNEL
copyout_clcluster_name(clcluster_name_t *namep, void *uaddr)
#else
copyout_clcluster_name(clcluster_name_t *, void *)
#endif
{
#ifdef _KERNEL
	if (get_udatamodel() == DATAMODEL_NATIVE) {	/*lint !e774 */
		return (copyout(namep, uaddr, sizeof (*namep)));
	} else {
		clcluster_name32_t name32;

		name32.len = (size32_t)namep->len;
		name32.name =
		    (caddr32_t)(unsigned long)namep->name;	/*lint !e507 */
		return (copyout(&name32, uaddr, sizeof (name32)));
	}
#else
	ASSERT(0);
	return (0);
#endif
}

int
copyin_clnode_name(void *uaddr, clnode_name_t *namep)
{
#ifdef _KERNEL
	if (get_udatamodel() == DATAMODEL_NATIVE) {	/*lint !e774 */
		return (copyin(uaddr, namep, sizeof (*namep)));
	} else {
		clnode_name32_t name32;
		int err;

		if ((err = copyin(uaddr, &name32, sizeof (name32))) != 0) {
			return (err);
		}
		namep->nodeid = name32.nodeid;
		namep->len = name32.len;
		namep->name = (char *)name32.name;
		return (0);
	}
#else
	namep->nodeid = ((clnode_name_t *)uaddr)->nodeid;
	namep->len = ((clnode_name_t *)uaddr)->len;
	namep->name = (char *)&(((clnode_name_t *)uaddr)->name);
	return (0);
#endif
}

int
copyout_clnode_name(clnode_name_t *namep, void *uaddr)
{
#ifdef _KERNEL
	if (get_udatamodel() == DATAMODEL_NATIVE) {	/*lint !e774 */
		return (copyout(namep, uaddr, sizeof (*namep)));
	} else {
		clnode_name32_t name32;

		name32.nodeid = namep->nodeid;
		name32.len = (size32_t)namep->len;
		name32.name =
		    (caddr32_t)(unsigned long)namep->name;	/*lint !e507 */
		return (copyout(&name32, uaddr, sizeof (name32)));
	}
#else
	((clnode_name_t *)uaddr)->nodeid = namep->nodeid;
	((clnode_name_t *)uaddr)->len = (size32_t)namep->len;
	return (0);
#endif
}

#ifdef _KERNEL
int
copyin_clconf_get_ccr_entry(void *uaddr, get_ccr_entry_t *argp)
{
	int	err;

	if (get_udatamodel() == DATAMODEL_NATIVE) {	/*lint !e774 */
		get_ccr_entry_t	ccr_entry;
		err = copyin(uaddr, &ccr_entry, sizeof (ccr_entry));
		if (err != 0) {
			return (err);
		}

		argp->table_name_len = ccr_entry.table_name_len;
		argp->table_name = (char *)kmem_alloc(argp->table_name_len,
		    KM_SLEEP);
		if ((err = copyin(ccr_entry.table_name, argp->table_name,
		    argp->table_name_len)) != 0) {
			kmem_free(argp->table_name, argp->table_name_len);
			return (err);
		}

		argp->entry_name_len = ccr_entry.entry_name_len;
		argp->entry_name =
		    (char *)kmem_alloc(argp->entry_name_len, KM_SLEEP);
		if ((err = copyin(ccr_entry.entry_name, argp->entry_name,
		    argp->entry_name_len)) != 0) {
			kmem_free(argp->table_name, argp->table_name_len);
			kmem_free(argp->entry_name, argp->entry_name_len);
			return (err);
		}

		argp->buf_len = ccr_entry.buf_len;
		argp->buf = (char *)kmem_alloc(argp->buf_len, KM_SLEEP);
	} else {
		get_ccr_entry32_t	ccr_entry32;

		err = copyin(uaddr, &ccr_entry32, sizeof (ccr_entry32));
		if (err != 0) {
			return (err);
		}

		argp->table_name_len = ccr_entry32.table_name_len;
		argp->table_name = (char *)kmem_alloc(argp->table_name_len,
		    KM_SLEEP);
		if ((err = copyin((char *)ccr_entry32.table_name,
		    argp->table_name, argp->table_name_len)) != 0) {
			kmem_free(argp->table_name, argp->table_name_len);
			return (err);
		}

		argp->entry_name_len = ccr_entry32.entry_name_len;
		argp->entry_name = (char *)kmem_alloc(argp->entry_name_len,
		    KM_SLEEP);
		if ((err = copyin((char *)ccr_entry32.entry_name,
		    argp->entry_name, argp->entry_name_len)) != 0) {
			kmem_free(argp->table_name, argp->table_name_len);
			kmem_free(argp->entry_name, argp->entry_name_len);
			return (err);
		}

		argp->buf_len = ccr_entry32.buf_len;
		argp->buf = (char *)kmem_alloc(argp->buf_len, KM_SLEEP);
	}

	return (0);
}

int
copyout_clconf_get_ccr_entry(get_ccr_entry_t *argp, void *uaddr)
{
	int err;

	if (get_udatamodel() == DATAMODEL_NATIVE) {	/*lint !e774 */
		get_ccr_entry_t	ccr_entry;
		err = copyin(uaddr, &ccr_entry, sizeof (ccr_entry));
		kmem_free(argp->table_name, argp->table_name_len);
		kmem_free(argp->entry_name, argp->entry_name_len);

		if (err != 0) {
			kmem_free(argp->buf, argp->buf_len);
			return (err);
		}

		err = copyout(argp->buf, ccr_entry.buf, argp->buf_len);
		kmem_free(argp->buf, argp->buf_len);

		if (err != 0) {
			return (err);
		}
		ccr_entry.rslt = argp->rslt;

		err = copyout(&ccr_entry, uaddr, sizeof (ccr_entry));
		if (err != 0) {
			return (err);
		}
	} else {
		get_ccr_entry32_t	ccr_entry32;
		err = copyin(uaddr, &ccr_entry32, sizeof (ccr_entry32));

		kmem_free(argp->table_name, argp->table_name_len);
		kmem_free(argp->entry_name, argp->entry_name_len);

		if (err != 0) {
			kmem_free(argp->buf, argp->buf_len);
			return (err);
		}

		err = copyout(argp->buf, (char *)ccr_entry32.buf,
		    argp->buf_len);
		kmem_free(argp->buf, argp->buf_len);

		if (err != 0) {
			return (err);
		}

		ccr_entry32.rslt = argp->rslt;

		err = copyout(&ccr_entry32, uaddr, sizeof (ccr_entry32));
		if (err != 0) {
			return (err);
		}
	}

	return (0);
}


#if (SOL_VERSION >= __s10)
int
copyin_privip_get_ccr(void *uaddr, get_ccr_privip_t *argp)
{
	int	err;

	if (get_udatamodel() == DATAMODEL_NATIVE) {	/*lint !e774 */
		get_ccr_privip_t	ccr_privip;
		err = copyin(uaddr, &ccr_privip, sizeof (ccr_privip));
		if (err != 0) {
			return (err);
		}
		argp->count = ccr_privip.count;
		argp->buf_len = ccr_privip.buf_len;
		argp->buf = (char *)kmem_alloc(argp->buf_len, KM_SLEEP);
	} else {
		get_ccr_privip32_t	ccr_privip32;
		err = copyin(uaddr, &ccr_privip32, sizeof (ccr_privip32));
		if (err != 0) {
			return (err);
		}
		argp->count = ccr_privip32.count;
		argp->buf_len = ccr_privip32.buf_len;
		argp->buf = (char *)kmem_alloc(argp->buf_len, KM_SLEEP);
	}
	return (0);
}

int
copyout_privip_get_ccr(get_ccr_privip_t *argp, void *uaddr)
{
	int err;

	if (get_udatamodel() == DATAMODEL_NATIVE) {	/*lint !e774 */
		get_ccr_privip_t	ccr_privip;
		err = copyin(uaddr, &ccr_privip, sizeof (ccr_privip));
		if (err != 0) {
			kmem_free(argp->buf, argp->buf_len);
			return (err);
		}
		err = copyout(argp->buf, ccr_privip.buf, argp->buf_len);
		kmem_free(argp->buf, argp->buf_len);
		if (err != 0) {
			return (err);
		}
		ccr_privip.rslt = argp->rslt;
		ccr_privip.count = argp->count;
		err = copyout(&ccr_privip, uaddr, sizeof (ccr_privip));
		if (err != 0) {
			return (err);
		}
	} else {
		get_ccr_privip32_t	ccr_privip32;
		err = copyin(uaddr, &ccr_privip32, sizeof (ccr_privip32));
		if (err != 0) {
			kmem_free(argp->buf, argp->buf_len);
			return (err);
		}
		err = copyout(argp->buf, (char *)ccr_privip32.buf,
		    argp->buf_len);
		kmem_free(argp->buf, argp->buf_len);
		if (err != 0) {
			return (err);
		}
		ccr_privip32.rslt = argp->rslt;
		ccr_privip32.count = argp->count;
		err = copyout(&ccr_privip32, uaddr, sizeof (ccr_privip32));
		if (err != 0) {
			return (err);
		}
	}
	return (0);
}


int
copyin_zcnode_name(const void *uaddr, zcnode_name_t *zcnode_namep)
{
	if (get_udatamodel() == DATAMODEL_NATIVE) {	/*lint !e774 */
		return (copyin(uaddr, zcnode_namep, sizeof (*zcnode_namep)));
	} else {
		zcnode_name32_t name32;
		int err;

		if ((err = copyin(uaddr, &name32, sizeof (name32))) != 0) {
			return (err);
		}
		os::strcpy(zcnode_namep->zc_name, name32.zc_name);
		zcnode_namep->nodeid = name32.nodeid;
		zcnode_namep->len = name32.len;
		zcnode_namep->namep = (char *)name32.namep;
		return (0);
	}
}

int
copyout_zcnode_name(const zcnode_name_t *namep, void *uaddr)
{
	if (get_udatamodel() == DATAMODEL_NATIVE) {	/*lint !e774 */
		return (copyout(namep, uaddr, sizeof (*namep)));
	} else {
		zcnode_name32_t name32;

		os::strcpy(name32.zc_name, namep->zc_name);
		name32.nodeid = namep->nodeid;
		name32.len = (size32_t)namep->len;
		name32.namep =
		    (caddr32_t)(unsigned long)namep->namep;	/*lint !e507 */
		return (copyout(&name32, uaddr, sizeof (name32)));
	}
}

int
copyin_cz_name(const void *uaddr, cz_name_t *cznamep)
{
	if (get_udatamodel() == DATAMODEL_NATIVE) {	/*lint !e774 */
		return (copyin(uaddr, cznamep, sizeof (*cznamep)));
	} else {
		cz_name32_t name32;
		int err;

		if ((err = copyin(uaddr, &name32, sizeof (name32))) != 0) {
			return (err);
		}
		cznamep->clid = name32.clid;
		cznamep->len = name32.len;
		cznamep->namep = (char *)name32.name;
		return (0);
	}
}

int
copyout_cz_name(const cz_name_t *cznamep, void *uaddr)
{
	if (get_udatamodel() == DATAMODEL_NATIVE) {	/*lint !e774 */
		return (copyout(cznamep, uaddr, sizeof (*cznamep)));
	} else {
		cz_name32_t name32;

		name32.clid = cznamep->clid;
		name32.len = (size32_t)cznamep->len;
		name32.name =
		    (caddr32_t)(unsigned long)cznamep->namep;	/*lint !e507 */
		return (copyout(&name32, uaddr, sizeof (name32)));
	}
}

int
copyin_cz_id(const void *uaddr, cz_id_t *czidp)
{
	if (get_udatamodel() == DATAMODEL_NATIVE) {	/*lint !e774 */
		return (copyin(uaddr, czidp, sizeof (*czidp)));
	} else {
		cz_id32_t czid32;
		int err;

		if ((err = copyin(uaddr, &czid32, sizeof (czid32))) != 0) {
			return (err);
		}
		czidp->clid = czid32.clid;
		(void) os::strcpy(czidp->name, czid32.name);
		return (0);
	}
}

int
copyout_cz_id(const cz_id_t *czidp, void *uaddr)
{
	if (get_udatamodel() == DATAMODEL_NATIVE) {	/*lint !e774 */
		return (copyout(czidp, uaddr, sizeof (*czidp)));
	} else {
		cz_id32_t czid32;

		czid32.clid = czidp->clid;
		(void) os::strcpy(czid32.name, czidp->name);
		return (copyout(&czid32, uaddr, sizeof (czid32)));
	}
}

int
copyin_cz_subnet(const void *uaddr, cz_subnet_t *cznetp)
{
	int err;

	if (get_udatamodel() == DATAMODEL_NATIVE) { /*lint !e774 */
		return (copyin(uaddr, cznetp, sizeof (*cznetp)));
	} else {
		cz_subnet32_t sub_net32;

		if ((err =
		    copyin(uaddr, &sub_net32, sizeof (sub_net32))) != 0) {
			return (err);
		}
		os::strcpy(cznetp->name, sub_net32.name);
		return (0);
	}
}

int
copyout_cz_subnet(const cz_subnet_t *cznetp, void *uaddr)
{
	int err;

	if (get_udatamodel() == DATAMODEL_NATIVE) { /*lint !e774 */
		return (copyout(cznetp, uaddr, sizeof (*cznetp)));
	} else {
		cz_subnet32_t net;
		uint32_t len;

		(void) os::strcpy(net.name, cznetp->name);
		(void) os::strcpy(net.sub_net, cznetp->sub_net);

		err = copyout(&net, uaddr, sizeof (net));
		if (err != 0) {
			return (err);
		}
	}
	return (0);
}

#endif // (SOL_VERSION >= __s10)
#endif // _KERNEL

int
copyout_get_scshutdown_state(bool *argp, void *uaddr)
{
	int err;

	err = copyout(argp, uaddr, sizeof (bool));

	return (err);
}

int
copyin_clconf_get_clconf_child_stats(void *uaddr,
    get_clconf_child_stats_t *argp)
{
	int	err;

	err = copyin(uaddr, argp, sizeof (get_clconf_child_stats_t));

	return (err);
}

int
copyout_clconf_get_clconf_child_stats(get_clconf_child_stats_t *argp,
    void *uaddr)
{
	int err;

	err = copyout(argp, uaddr, sizeof (get_clconf_child_stats_t));

	return (err);
}

int
copyin_clconf_get_proto_vers_num(void *uaddr,
    get_clconf_proto_vers_num_t *argp)
{
	int	err;

	err = copyin(uaddr, argp, sizeof (get_clconf_proto_vers_num_t));

	return (err);
}

int
copyout_clconf_get_proto_vers_num(get_clconf_proto_vers_num_t *argp,
    void *uaddr)
{
	int err;

	err = copyout(argp, uaddr, sizeof (get_clconf_proto_vers_num_t));

	return (err);
}

#ifdef _KERNEL
static int
copyin_cladmin_log(void *uaddr, cladmin_log_t **logp)
{
	*logp = (cladmin_log_t *)
		kmem_alloc(sizeof (cladmin_log_t), KM_SLEEP);
	if (get_udatamodel() == DATAMODEL_NATIVE) {	/*lint !e774 */
		return (copyin(uaddr, *logp, sizeof (cladmin_log_t)));
	} else {
		int err;
		cladmin_log32_t *log32p = (cladmin_log32_t *)
			kmem_alloc(sizeof (cladmin_log32_t), KM_SLEEP);
		if ((err = copyin(uaddr,
		    log32p, sizeof (cladmin_log32_t))) != 0) {
			kmem_free(log32p, sizeof (cladmin_log32_t));
			return (err);
		}
		(*logp)->level = log32p->level;
		(void) strncpy((*logp)->str,
		    log32p->str, sizeof (log32p->str));
		kmem_free(log32p, sizeof (cladmin_log32_t));
		return (0);
	}
}
#endif
