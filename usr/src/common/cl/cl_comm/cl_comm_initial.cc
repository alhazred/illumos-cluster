//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cl_comm_initial.cc	1.4	09/02/13 SMI"

#include <orb/invo/common.h>
#include <sys/modctl.h>
#include <orb/object/schema.h>
#include <orb/transport/nil_endpoint.h>
#include <orb/msg/orb_msg.h>
#include <cplplrt/cplplrt.h>
#include <sys/cladm.h>
#include <orb/infrastructure/orb_conf.h>
#include <cmm/cmm_ns.h>
#include <vm/vm_comm.h>
#include <sys/cl_comm_support.h>

#if defined(_FAULT_INJECTION)
#include <orb/fault/fault_functions.h>
#endif

extern "C" int cl_orb_initialize(void);
extern "C" int cl_orb_is_initialized(void);

/* inter-module dependencies */
#if SOL_VERSION >= __s11
char _depends_on[] = "misc/cl_runtime misc/cl_bootstrap misc/cl_load "
	"misc/cl_orb misc/cl_haci misc/cl_quorum strmod/clhbsndr "
	"drv/clprivnet drv/ip fs/sockfs misc/ksocket";
#else
char _depends_on[] = "misc/cl_runtime misc/cl_bootstrap misc/cl_load "
	"misc/cl_orb misc/cl_haci misc/cl_quorum strmod/clhbsndr "
	"drv/clprivnet drv/ip fs/sockfs";
#endif

static struct modlmisc modlmisc = {
	&mod_miscops, "Sun Cluster communication support",
};

static struct modlinkage modlinkage = {
#ifdef _LP64
	MODREV_1, { (void *) &modlmisc, NULL, NULL, NULL, NULL, NULL, NULL }
#else
	MODREV_1, { (void *) &modlmisc, NULL, NULL, NULL }
#endif
};

extern "C" int
_init(void)
{
	int	error;
	sc_syslog_msg_handle_t	handle;

	// fails if ENOMEM or unable to acquire mutex lock, both of which
	// are rare possibilities.
	if (sc_syslog_msg_initialize(&handle, SC_SYSLOG_FRAMEWORK_TAG,
	    SC_SYSLOG_FRAMEWORK_RSRC) != SC_SYSLOG_MSG_STATUS_GOOD) {
		return (ENOMEM);
	}

	if ((error = mod_install(&modlinkage)) != 0) {
		//
		// SCMSGS
		// @explanation
		// The loading of the cl_comm module failed.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm: error loading kernel module: %d", error);
		sc_syslog_msg_done(&handle);
		return (error);
	}

	sc_syslog_msg_done(&handle);
	_cplpl_init();		/* C++ initialization */

	//
	// We initialize these two function pointers here as they
	// must be resolved as soon as the module loads. The remaining
	// function pointers will be resolved once the orb is initialized.
	//
	// The following function pointers are called from the cl_load
	// module and when the cluster is started.
	//
	cl_orb_initialize_funcp = cl_orb_initialize;
	cl_orb_is_initialized_funcp = cl_orb_is_initialized;

	return (0);
}

extern "C" int
_fini(void)
{
	// Don't let the module unload
	return (EBUSY);
}

extern "C" int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}
