/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)cl_comm_fault_functions.cc	1.4	08/05/20 SMI"

#include <sys/os.h>
#include <sys/cladm_int.h>
#include <sys/clconf.h>
#include <orb/invo/common.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/buffers/marshalstream.h>
#include <orb/fault/fault_injection.h>
#include <orb/fault/fault_functions.h>
#include <orb/fault/fault_rpc.h>
#include <errno.h>

#include <h/replica.h>
#include <h/naming.h>
#include <nslib/ns.h>
#include <nslib/naming_context_impl.h>
#include <nslib/data_container_impl.h>
#include <sys/mc_sema_impl.h>
#include <sys/rm_util.h>

#include <cmm/cmm_ns.h>

#if defined(_KERNEL_ORB) && !defined(_KERNEL)
#define	UNODE
#else
#undef	UNODE
#endif

#if defined(_KERNEL_ORB)
#include <orb/member/members.h>
#include <repl/rmm/rmm.h>
#include <orb/transport/path_manager.h>
#include <orb/transport/endpoint_registry.h>
#include <orb/xdoor/translate_mgr.h>
#endif

#if defined(_KERNEL)
#include <sys/uadmin.h>
#include <sys/vfs.h>
#include <sys/dumphdr.h>
#include <sys/callb.h>
#include <sys/sunddi.h>

extern ksema_t fsflush_sema;

#elif defined(_KERNEL_ORB)
#include <signal.h>
#include <stdio.h>
#include <unode/unode.h>
#endif

#if defined(_KERNEL_ORB)
//
// External interface for cl_orb
//
extern "C" void fi_cmm_reboot(uint_t fault_num, void *fault_argp,
    uint32_t fault_arglen)
{
	FaultFunctions::cmm_reboot(fault_num, fault_argp, fault_arglen);
}

//
// CMM reboot
//
// This fault point will reboot a remote / local node when the
// step of the local node matches the step specified in the fault
// arguments
// Fault Argument:
//	A cmm_reboot_arg_t structure that specifies the node to reboot
//	and the step to do it in.  (and a wait type)
//
void
FaultFunctions::cmm_reboot(uint_t fault_num, void *fault_argp,
		uint32_t fault_arglen)
{
	ASSERT(fault_argp != NULL);
	ASSERT(fault_arglen == sizeof (cmm_reboot_arg_t));

	cmm_reboot_arg_t	*cmm_argp;
	wait_for_arg_t		wait_arg;
	uint32_t		step, curstep;
	nodeid_t		nodeid;

	// Extract the cmm_reboot_arg_t structure from the fault argument
	cmm_argp = (cmm_reboot_arg_t *)fault_argp;

	if (cmm_argp->nodeid == 0) {
		cmm_argp->nodeid = orb_conf::local_nodeid();
	}

	step = cmm_argp->step;
	nodeid = cmm_argp->nodeid;

	if (! get_current_cmm_step(curstep)) {
		return;			// get_current_cmm_step() failed
	}

	// If the current step isn't the desired one, we're done.
	if (curstep != step) {
		return;
	}

	wait_arg.op = cmm_argp->op;
	wait_arg.nodeid = nodeid;

	// Setup the wait_for_X
	do_setup_wait(&wait_arg);

	// turn off the fault before first (Global and Environment)
	// No need to copy the wait arguments (we already did)
	NodeTriggers::clear(fault_num, TRIGGER_ALL_NODES);
	InvoTriggers::clear(fault_num);

	if (fault_rpc::send_fault(fault_rpc::NODE_REBOOT, nodeid, fault_num)) {
		// Wait for unknown
		do_wait(&wait_arg);
	}
}
#endif // _KERNEL_ORB

//
// get_current_cmm_step
// Returns the current cmm step on this node in the argument.
// Returns true if successful, false othersie.
//
#if defined(_KERNEL_ORB)
bool
FaultFunctions::get_current_cmm_step(uint32_t &current_step)
{
	cmm::cluster_state_t	cmmstate;
	Environment		e;

	cmm::control_var cmm_cntl_v = cmm_ns::get_control();
	cmm_cntl_v->get_cluster_state(cmmstate, e);
	if (e.exception()) {
		e.exception()->print_exception(
			"cmm::control::get_cluster_state");
		return (false);
	}

	current_step = cmmstate.curr_step;
	return (true);
}
#endif // _KERNEL_ORB

//
// External interface for cl_orb
//
#if defined(_KERNEL_ORB)
extern "C" void
fi_do_reboot(bool clean)
{
	FaultFunctions::_do_reboot(clean);
}
#endif // _KERNEL_ORB

//
// Called when a node reboot (fault injection rpc) message is received.
//
#if defined(_KERNEL_ORB)
void
FaultFunctions::_do_reboot(bool clean)
{
#if defined(_KERNEL)

#if (SOL_VERSION >= __s10) && !defined(UNODE)

	Environment env;
	ff::failfast_admin_var ff_admin_v =
	    cmm_ns::get_ff_admin(orb_conf::local_nodeid());

	ASSERT(!CORBA::is_nil(ff_admin_v));

	// Disable node level failfast control on the local node
	ff_admin_v->set_node_level_failfast_control(false, env);

	ASSERT(!env.exception());

#else
	extern bool failfast_disabled;

	// We are having problems with failfasts occuring here. We disable
	// them for now.
	failfast_disabled = true;
#endif

#if SOL_VERSION < __s10
	// XXX - The following code was taken from syscall/uadmin.c
	//
	// Code terminates all processes except for the kernel daemons
	// and syncs the filesystems before shutting down
	// This should help us avoid the TRAP if we call mdboot without
	// doing cleanup.
	//
	// This is neccesary because we cant just call mdboot without
	// sending SIGKILL to all the non-kernel-daemon processes.
	// Otherwise we might get preemted, and one of these process could
	// attempt to access resources that have already been freed, and we'd
	// end up trapping.
	//
	// Some of the code has been commented out because it was releasing
	// resources for this thread.  Yet since we're a kernel thread that
	// does not have a user-level context, we dont have these resources.
	// And cant give them up.
	//

	register struct proc *p;
	// struct vnode *exec_vp;

	/*
	 * Release (almost) all of our own resources.
	 */
/*
	// This code has been removed because it does not apply to the kernel
	// threads in clustering
	p = ttoproc(curthread);
	// exitlwps(0);
	mutex_enter(&p->p_lock);
	p->p_flag |= SNOWAIT;
	sigfillset(&p->p_ignore);
	curthread->t_lwp->lwp_cursig = 0;
	if (p->p_exec) {
		exec_vp = p->p_exec;
		p->p_exec = NULLVP;
		mutex_exit(&p->p_lock);
		VN_RELE(exec_vp);
	} else {
		mutex_exit(&p->p_lock);
	}
	closeall(1);
	relvm();
*/

	/*
	 * Kill all processes except kernel daemons and ourself.
	 * Make a first pass to stop all processes so they won't
	 * be trying to restart children as we kill them.
	 */
	mutex_enter(&pidlock);
	for (p = practive; p != NULL; p = p->p_next) {
		if (p->p_exec != NULLVP &&	/* kernel daemons */
		    p->p_as != &kas &&
		    p->p_stat != SZOMB) {
			mutex_enter(&p->p_lock);
			p->p_flag |= SNOWAIT;
			sigtoproc(p, NULL, SIGSTOP);
			mutex_exit(&p->p_lock);
		}
	}
	p = practive;
	while (p != NULL) {
		if (p->p_exec != NULLVP &&	/* kernel daemons */
		    p->p_as != &kas &&
		    p->p_stat != SIDL &&
		    p->p_stat != SZOMB) {
			mutex_enter(&p->p_lock);
			// lint complains about shift in sigword() macro
			if (sigismember(&p->p_sig,
			    SIGKILL)) {		//lint !e572 !e778
				mutex_exit(&p->p_lock);
				p = p->p_next;
			} else {
				sigtoproc(p, NULL, SIGKILL);
				mutex_exit(&p->p_lock);
				/* wait one second. */
				(void) cv_timedwait(&p->p_srwchan_cv,
				    &pidlock, ddi_get_lbolt() +
				    drv_usectohz((clock_t)1000000));
				p = practive;
			}
		} else {
			p = p->p_next;
		}
	}

	mutex_exit(&pidlock);

/*
	// Removed because we cant release these resources from the clustering
	// kernel threads
	VN_RELE(u.u_cdir);
	if (u.u_rdir)
		VN_RELE(u.u_rdir);

	u.u_cdir = rootdir;
	u.u_rdir = NULL;
*/
#endif

	/*
	 * Allow fsflush to finish running and then prevent it
	 * from ever running again so that vfs_unmountall() and
	 * vfs_syncall() can acquire the vfs locks they need.
	 */
	::sema_p(&fsflush_sema);

	// We wont unmount all the fs because of a deadlock problem with
	// some cmm tests and pxfs
	//
	// The problem is that unmount and sync will cause pxfs to do IDL
	// invocations, which in the middle of CMM tests, will deadlock
	// if IDL invocations are frozen
	//
	// UPDATE: only do these if we want a clean reboot
	if (clean) {
		vfs_unmountall();
		(void) VFS_MOUNTROOT(rootvfs, ROOT_UNMOUNT);
		vfs_syncall();
	}

	if (clean) {
		(void) callb_execute_class(CB_CL_UADMIN, 0);
	}

	dump_messages();

	// Reboot this node
	// We can call directly into mdboot since we already took care of the
	// preprocessing that uadmin goes through for A_SHUTDOWN
#if SOL_VERSION >= __s11
	mdboot(A_REBOOT, AD_BOOT, NULL, B_FALSE);
#else
	mdboot(A_REBOOT, AD_BOOT, NULL);
#endif // SOL_VERSION >= __s11

#else
	if (clean)
		reboot_unode_clean();
	else
		reboot_unode();
#endif // _KERNEL
}
#endif // _KERNEL_ORB
