/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)cl_load_initial.cc	1.4	08/05/20 SMI"


#include <sys/modctl.h>
#include <sys/errno.h>
#include <cplplrt/cplplrt.h>
#include <sys/sc_syslog_msg.h>
#include <sys/os.h>

/*
 * The module definition for cl_load.
 */

/*
 * cl_load's module definition _init(), _mod_info() and _fini().
 */

/* inter-module dependencies */
char _depends_on[] = "misc/cl_runtime misc/cl_bootstrap";

static struct modlmisc modlmisc = {
	&mod_miscops, "Sun Cluster cladmin support",
};

static struct modlinkage modlinkage = {
#ifdef _LP64
	MODREV_1, { (void *) &modlmisc, NULL, NULL, NULL, NULL, NULL, NULL }
#else
	MODREV_1, { (void *) &modlmisc, NULL, NULL, NULL }
#endif
};

extern "C" int
_init(void)
{
	int	error;
	sc_syslog_msg_handle_t	handle;

	if ((error = mod_install(&modlinkage)) != 0) {
		//
		// SCMSGS
		// @explanation
		// The loading of the cl_load module failed.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, SC_SYSLOG_WARNING, MESSAGE,
		    "cl_load: error loading kernel module: %d", error);
		sc_syslog_msg_done(&handle);
		return (error);
	}

	_cplpl_init();		/* C++ initialization */

	return (0);
}

extern "C" int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}

extern"C" int
_fini(void)
{
	return (EBUSY);
}
