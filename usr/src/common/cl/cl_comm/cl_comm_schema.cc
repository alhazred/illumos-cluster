//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cl_comm_schema.cc	1.4	08/05/20 SMI"

#include <limits.h>
#include <orb/object/schema.h>
#include <orb/object/adapter.h>
#include <nslib/ns.h>
#include <h/version.h>
#include <h/version_manager.h>
#include <sys/vm_util.h>

#include <orb/infrastructure/orb.h>
#include <orb/object/idlversion_impl.h>

//
// Return a reference to the interface version object on the given node.
//
extern "C" idlversion_ptr
get_idlversion_impl(nodeid_t n)
{
	naming::naming_context_var ctxp;
	CORBA::Object_var cobj;
	char name[24]; // "idlversion_NNNNN\0"
	Environment e;
	static os::mutex_t llock;

	// This assert catches calls before ORB::initialize() has been called.
	ASSERT((ORB::is_initialized()) || (ORB::is_initializing()));

	//
	// If we're looking for the interface version object on another node,
	// handle that case here.
	//
	if ((n != NODEID_UNKNOWN) && (n != orb_conf::local_nodeid())) {

#ifdef _KERNEL_ORB
		//
		// Check the current running version of idlversion
		// object. In version 1.0, the reference is obtained
		// from the nameserver. However, there are bootstrap
		// issues with this approach. So in version 1.1 the
		// the idl version object created at a well known
		// xdoor id. The advantage is that we know how to get
		// references to such objects and do not have to
		// lookup the nameserver. See bug 4889625.
		//
		version_manager::vm_admin_var vm_v = vm_util::get_vm();
		version_manager::vp_version_t idlver;
		Environment env;

		vm_v->get_nodepair_running_version("Idl_Schema_Cache", n,
		    INCN_UNKNOWN, idlver, env);

		if (!env.exception() && (idlver.minor_num != 0)) {
			InterfaceDescriptor *infp = (InterfaceDescriptor *)
			    idlversion::_get_type_info(0);
			return (new Anchor<idlversion_stub>(n,
			    XDOOR_SCHEMA_CACHE, infp));
		}
		//
		// If global nameserver on this node has been
		// initialized, lookup the reference to idlversion
		// object there. If it has not, then ask the local
		// nameserver on the node that sent us this reference.
		//
		if (ns::is_root_ns_initialized()) {
			ctxp = ns::root_nameserver();
		} else {
			//
			// XXX It is quite inefficient to lookup
			// reference to the idlversion object on the
			// sending node's local name server. But this
			// will happen only in the case where this
			// node is joining a cluster, global
			// nameserver is in process of adding this
			// node as secondary and dumpstate checkpoints
			// contain a type that is unknown to this
			// node. At all other times, reference to
			// idlversion object on the sending node
			// should be available in the root_nameserver.
			// Note that if we are here then the sending
			// node is running version 0 of the idlversion
			// object where the object is not created at a
			// fixed xdoor id. So we have to look up in
			// the local nameserver on that node
			//
			InterfaceDescriptor *infp = (InterfaceDescriptor *)
				naming::naming_context::_get_type_info(0);

			ctxp = new Anchor<naming::naming_context_stub>(n,
			    XDOOR_LOCAL_NS, infp);
		}

#else
		//
		// In userland, the root nameserver will be initialized before
		// any user process start up.
		//
		ctxp = ns::root_nameserver();
#endif
		if (CORBA::is_nil(ctxp)) {
			return (idlversion::_nil());
		}
		(void) os::snprintf(name, sizeof (name),
		    idlversion_template, n);
		cobj = ctxp->resolve(name, e);
		if (e.exception()) {
			e.clear();
			return (idlversion::_nil());
		}
		return (idlversion::_narrow(cobj));
	}
	//
	// Looking for idlversion object on the local node.
	//
#ifdef _KERNEL_ORB
	//
	// If this is in the kernel, then the idlversion object is
	// created during ORB::initialize().
	//
	ASSERT(!CORBA::is_nil(idlversion_impl::self_p));
	return (idlversion::_duplicate(idlversion_impl::self_p));
#else
	//
	// Userland ORB: Get a reference to idlversion object in the
	// kernel from the local nameserver.
	//
	static idlversion_ptr local_svr_p = idlversion::_nil();

	if (CORBA::is_nil(local_svr_p)) {
		//
		// Use a lock on the outside chance that there are
		// two threads racing to initialize the server.
		// In the normal case (when local_svr isn't nil),
		// we use the fast path and avoid this locking.
		//
		llock.lock();
		if (CORBA::is_nil(local_svr_p)) {
			ctxp = ns::local_nameserver();
			if (CORBA::is_nil(ctxp)) {
				llock.unlock();
				return (idlversion::_nil());
			}
			(void) os::snprintf(name, sizeof (name),
			    idlversion_template, orb_conf::local_nodeid());
			cobj = ctxp->resolve(name, e);
			if (e.exception()) {
				llock.unlock();
				return (idlversion::_nil());
			}
			local_svr_p = idlversion::_narrow(cobj);
		}
		llock.unlock();
	}
	return (idlversion::_duplicate(local_svr_p));
#endif
}
