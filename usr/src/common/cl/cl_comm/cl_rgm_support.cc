//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cl_rgm_support.cc	1.4	08/08/01 SMI"

#include <sys/cl_rgm.h>
#include <sys/cladm_int.h>
#include <sys/errno.h>
#include <sys/sunddi.h>
#include <sys/os.h>
#include <sys/param.h>


#ifdef _KERNEL
#if SOL_VERSION >= __s10
extern struct rgm_door_info_t rgm_door_info[];
int
rgm_get_door_copy_args(void *arg, int *door_code, char *zonename) {
	void *tmp;
	if (ddi_copyin(arg, door_code, sizeof (int), 0) != 0) {
		return (EFAULT);
	}
	if (*door_code < 0 || *door_code > RGM_MAX_DOOR_SYSCALL_CODE) {
		return (EINVAL);
	}
#if (SOL_VERSION >= __s10)
	tmp = (void *)((char *)arg + sizeof (int));
	if (ddi_copyin(tmp, zonename, ZONENAME_MAX, 0) != 0) {
		return (EFAULT);
	}
	zonename[ZONENAME_MAX-1] = 0;
#else
	strcpy(zonename, "global");
#endif
	return (0);
}

int
rgm_store_door_copy_args(
    void *arg, int *door_code, char *zonename, char *pathname) {

	void *tmp;

	if (ddi_copyin(arg, door_code, sizeof (int), 0) != 0) {
		return (EFAULT);
	}
	if (*door_code < 0 || *door_code > RGM_MAX_DOOR_SYSCALL_CODE) {
		return (EINVAL);
	}
	tmp = (void *)((char *)arg + sizeof (int));
#if (SOL_VERSION >= __s10)
	if (ddi_copyin(tmp, zonename, ZONENAME_MAX, 0) != 0) {
		return (EFAULT);
	}
	zonename[ZONENAME_MAX -1] = 0;
	tmp = (void *)((char *)tmp + ZONENAME_MAX);
#else
	strcpy(zonename, "global");
#endif
	if (ddi_copyin(tmp, pathname, MAXPATHLEN, 0) != 0) {
		return (EFAULT);
	}
	// ensure that the string is null-terminated.
	pathname[MAXPATHLEN-1] = 0;
	return (0);
}

door_handle_t rgm_get_door(int door_code, char *zone_in) {
	// assume door code is valid.
	// introduce permission checks for zone boundary
	int i;
	char *zone;

	struct rgm_door_info_t *door_info = rgm_door_info + door_code;

	if (door_info->handles == NULL)
		return (NULL);
	if (door_info->use_default_key == true)
		zone = "global";
	else
		zone = zone_in;

	for (i = 0; door_info->handles[i].zonename != NULL; i++) {
		if (strcmp(zone, door_info->handles[i].zonename) == 0) {
			return (door_info->handles[i].handle);
		}
	}

	return (NULL);
}

void rgm_store_door(int door_code, door_handle_t h_in, char *zone_in) {
	// assume door code is valid.
	// locking needs to be implemented
	int i;
	void *tmp;
	struct rgm_door_info_t *door_info = rgm_door_info + door_code;
	if (door_info->handles == NULL) {
		door_info->handles =
		    (rgm_door_tuple *)kmem_alloc(
		    (size_t)2 * sizeof (rgm_door_tuple), KM_SLEEP);
		door_info->handles[0].zonename =  os::strdup(zone_in);
		door_info->handles[0].handle = h_in;
		door_info->handles[1].zonename =  NULL;
		return;
	}
	for (i = 0; door_info->handles[i].zonename != NULL; i++) {
		if (strcmp(zone_in, door_info->handles[i].zonename) == 0) {
			// entry exist. overwrite
			door_info->handles[i].handle = h_in;
			// what about freeing the vnode
			return;
		}
	}
	tmp =  kmem_alloc((i+2) * sizeof (rgm_door_tuple), KM_SLEEP);
	bcopy(rgm_door_info->handles, tmp, i * sizeof (rgm_door_tuple));
	// os::warning("i=%d:size:%d:Try freeing:%p,%d",i,
	// 	sizeof(rgm_door_tuple),
	//	rgm_door_info->handles, (i+1) * sizeof(rgm_door_tuple));
	kmem_free(rgm_door_info->handles, (i+1) * sizeof (rgm_door_tuple));
	door_info->handles = (struct rgm_door_tuple *)tmp;
	door_info->handles[i].zonename =  os::strdup(zone_in);
	door_info->handles[i].handle = h_in;
	door_info->handles[i+1].zonename = NULL;
}

#endif
#endif
