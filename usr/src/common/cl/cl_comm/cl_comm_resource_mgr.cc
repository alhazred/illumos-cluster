//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cl_comm_resource_mgr.cc	1.2	08/05/20 SMI"

#include <orb/member/members.h>
#include <orb/flow/resource_mgr.h>
#include <orb/flow/resource_pool.h>
#include <orb/flow/resource_balancer.h>
#include <orb/flow/resource_done.h>
#include <orb/debug/orb_trace.h>
#include <orb/msg/orb_msg.h>
#include <orb/infrastructure/orb.h>
#include <orb/infrastructure/orb_conf.h>

//
// Class resource_mgr methods
//

//
// node_reconfiguration - a node reconfiguration has occurred.
// This method cleans up all of the data structures supporting flow control
// on this node.
//
// static
void
resource_mgr::node_reconfiguration()
{
	//
	// Clean up flow control information for all nodes in the cluster.
	//
	// The system currently allows a node to go straight from
	// one incarnation number to a new incarnation number without
	// this code ever seeing the intervening node dead state.
	//
	resource_balancer::the().node_reconfiguration();

	//
	// Clean up the data structures for each node/pool combination
	//
	for (nodeid_t node = 1; node <= NODEID_MAX; node++) {
		for (int pool = 0; pool < resource_defs::NUMBER_POOLS; pool++) {
			resource_done::select_resource_done(node,
			    (resource_defs::resource_pool_t)pool)->
			    init(node, (resource_defs::resource_pool_t)pool);
			resource_pool::select_resource_pool(node,
			    (resource_defs::resource_pool_t)pool)->
			    init(node, (resource_defs::resource_pool_t)pool);
		}
	}
}
