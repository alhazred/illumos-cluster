//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cl_comm_resource_pool.cc	1.2	08/05/20 SMI"


#include <orb/flow/resource_pool.h>
#include <orb/flow/resource.h>
#include <orb/member/members.h>
#include <orb/debug/orb_trace.h>
#include <orb/flow/resource_blocker.h>
#include <orb/infrastructure/orb_conf.h>
#include <sys/os.h>
#include <orb/debug/orb_stats_mgr.h>
#include <orb/debug/orbadmin_state.h>
#include <orb/fault/fault_injection.h>

#if defined(_KERNEL_ORB) && !defined(_KERNEL)
#include <unode/systm.h>
#endif

//
// init - there are three situations.
// There was no change in the current incarnation number, do nothing.
// The node is dead, set resources appropriate for dead node.
// The node is alive with a new incarnation number, initialize data structure.
//
void
resource_pool::init(nodeid_t node, resource_defs::resource_pool_t new_pool)
{
	Environment	e;

	lck.lock();

	// The pool value does not change once it has been set.
	ASSERT(pool == resource_defs::NUMBER_POOLS ||
	    pool == new_pool);
	pool = new_pool;

#ifdef DRC_DEBUG
	if (server_node.incn != INCN_UNKNOWN &&
	    !orb_conf::is_local_incn(server_node)) {
		ORB_DBPRINTF(ORB_TRACE_RECONFIG,
		    ("resource_pool(%d.%3d,%d) total %d free %d need %d\n",
		    server_node.ndid, (server_node.incn % 1000), pool,
		    thread_total, thread_free, thread_needed));
	}
#endif	// DRC_DEBUG

	incarnation_num		new_incn = members::the().incn(node);
	if (server_node.incn == new_incn) {
		// No change of state. Do nothing
		lck.unlock();
		return;
	}

	if (orb_conf::local_nodeid() == node) {
		// Set it to maximum supported version for consistency
		vp_version.major_num = 2;
		vp_version.minor_num = 0;
	} else {
		vm_util::get_vm()->get_nodepair_running_version("resource_mgr",
		    node, new_incn, vp_version, e);

		if (e.exception()) {
			//
			// Something has gone wrong; but we will try to operate
			// in version 1.0 mode
			//
#ifdef DEBUG
			(e.exception())->print_exception("resource_mgr:"
			    " vm_adm exception: ");
#endif // DEBUG
			vp_version.major_num = 1;
			vp_version.minor_num = 0;
		}
	}

	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("resource_mgr(server node %d maj %3d min %d) \n", node,
	    vp_version.major_num, vp_version.minor_num));

	// Associate the resource pool with the current server.
	// The node value does not change once set, while the incarnation
	// number can change.
	ASSERT(server_node.ndid == NODEID_UNKNOWN ||
	    server_node.ndid == node);
	server_node.ndid = node;
	server_node.incn = new_incn;

	// The old node incarnation is gone.
	node_death_cleanup();

	if (new_incn == INCN_UNKNOWN) {
		// Node currently dead
		lck.unlock();
		return;
	}
	// The node must be alive with a new incarnation number.

	// The resource pool is empty and must be initialized.
	ASSERT(pending_list.empty());
	ASSERT(thread_needed == 0);
	ASSERT(thread_total == 0);
	thread_free = 0;
	thread_free_min = 0;

	// The system should already disallow requests for additional
	// resources.
	//
	ASSERT(!allow_request_additional);

	// Message requests will block until the server grants resources

	lck.unlock();
}
