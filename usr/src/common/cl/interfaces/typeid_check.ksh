#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)typeid_check.ksh	1.6	08/05/20 SMI"
#
# The IDL compiler generates a type ID for each IDL interface (using MD5
# algorithm) based on its signature.  If an interface uses a type,
# e.g. size_t, whose basic types are different in 64-bit and 32-bit, then
# a different type ID will be generated for each case.  This could result in
# a kernel panic when, for instance, a 32-bit user application makes an
# invocation to a 64-bit kernel since the kernel doesn't know about
# the 32-bit type ID.
#
# This script verifies that type IDs generated for 32-bit user code are the
# same as 64-bit kernel.  It does this by comparing each schema file in the
# src/K64/schema directory against the corresponding one in src/U/schema.
#
# For certain IDL interfaces, however, it's ok to have different type IDs
# because, for example, they are only used in kernel-to-kernel invocations.
# The file
#
#	diff.list
#
# located in the same directory as this script, contains the names of such
# interfaces.  This script will exclude these interfaces from being compared.
#
# As a further sanity check, this script also compares schema files in
# src/U/schema against those in src/K32/schema (32-bit kernel).  These files
# should have the same type IDs.
#
# Possible exit values:
#
#	0	- success, no type ID mismatch found.
#	1	- failure, at least one interface has mismatched type ID.
#	2	- an error has occured.
#
unalias cd ls				# make sure user doesn't have these

PROGNAME=$0
PROGDIR=$(dirname $0)		# directory where this program resides
cd $PROGDIR				# do it from this script's directory

# The top-level src directory.
SRCDIR=${SRC}/common/cl/interfaces

# Exit value to use when encountering errors.
ERR_EXIT=2


#
# Filter a schema file to get only those lines with type IDs.  The lines
# are also sorted to ease diff'ing.
#
function tid_lines # <schema_file>
{
	fgrep '_Ix__tid' $1 | sort
}


#
# Diff two schema files.
#
function diff_files # <file1> <file2>
{
	diff -b <(tid_lines $1) <(tid_lines $2)
}


#
# Diff each schema file in src/$1/schema against its corresponding one
# in src/$2/schema.  The third argument, if exists, is the name of the
# file which contains names of schema files to be excluded from being
# compared (ok to have different type IDs).
#
function do_diff # <dir1> <dir2> [ok_file]
{
	typeset dir1=$1
	typeset dir2=$2
	typeset ok_file=$3
	typeset dir1_full dir1_partial dir1_files
	typeset dir2_full dir2_partial dir2_files
	typeset extra_files
	typeset interfaces
	typeset file item
	integer diff_found

	# Make sure we got at least 2 args.
	if (( $# < 2 ))
	then
		print -u2 "ERROR: do_diff(): Wrong # of args"
		exit $ERR_EXIT
	fi

	# Full pathnames to the schema directories.
	dir1_full=$SRCDIR/$dir1/schema
	dir2_full=$SRCDIR/$dir2/schema

	# Partial pathnames to the schema directories (for printing).
	dir1_partial=common/cl/interfaces/$dir1/schema
	dir2_partial=common/cl/interfaces/$dir2/schema

	# Get list of schema files.
	dir1_files=$(cd $dir1_full; ls -1 *_types.cc 2>/dev/null)
	dir2_files=$(cd $dir2_full; ls -1 *_types.cc 2>/dev/null)

	# Verify that there are files in the directories.
	if [[ -z "$dir1_files" ]]
	then
		print -u2 "ERROR: No schema files in $dir1_partial"
		exit $ERR_EXIT
	fi

	if [[ -z "$dir2_files" ]]
	then
		print -u2 "ERROR: No schema files in $dir2_partial"
		exit $ERR_EXIT
	fi

	# Verify that the schema directories contain the same set of files.
	extra_files=$(comm -23 <(echo "$dir1_files") <(echo "$dir2_files"))
	if [[ -n "$extra_files" ]]
	then
		print -u2 "ERROR: The following files exist only in" \
			"$dir1_partial:\n"
		echo "$extra_files" | sed 's;^;	;' >&2
		exit $ERR_EXIT
	fi

	extra_files=$(comm -13 <(echo "$dir1_files") <(echo "$dir2_files"))
	if [[ -n "$extra_files" ]]
	then
		print -u2 "ERROR: The following files exist only in" \
			"$dir2_partial:\n"
		echo "$extra_files" | sed 's;^;	;' >&2
		exit $ERR_EXIT
	fi

	# Compare each schema file.
	diff_found=0
	for file in $dir1_files
	do
		# Compare the files and get the names of interfaces that
		# have different type IDs.
		interfaces=$(diff_files $dir1_full/$file $dir2_full/$file |
			sed -n '/^</s/^.*_Ix__tid(\([^\)]\{1,\}\)).*/\1/p')

		# Do next file if there are no different type IDs.
		[[ -z "$interfaces" ]] && continue 

		# If there is an "ok" file, filter out those interfaces listed
		# in the file.
		if [[ -n "$ok_file" && -f "$ok_file" ]]
		then
			interfaces=$(echo "$interfaces" | fgrep -vf \
			  <(nawk 'NF > 0 && $1 !~ /^#/ {print $1}' "$ok_file"))
		fi

		# If there are no interfaces left, do next file.
		[[ -z "$interfaces" ]] && continue 

		if (( ! diff_found ))
		then
			print "\
Each schema file below has different type IDs for the following interfaces
between the versions in $dir1_partial and $dir2_partial:
"
			diff_found=1
		fi

		echo "    $file:"
		echo "$interfaces" | sed 's;^;        ;'
		echo
	done

	# If there were differences, return 1, else 0.
	(( diff_found == 0 ))
	return $?
}


RET=0							# exit value

# Compare 32-bit user against 64-bit kernel.  The file "diff.list" contains
# names of interfaces which are ok to have different type IDs.
do_diff 32 64 diff.list || RET=1

exit $RET
