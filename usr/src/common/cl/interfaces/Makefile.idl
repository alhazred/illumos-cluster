#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)Makefile.idl	1.135	08/07/22 SMI"
#
# usr/src/common/cl/interfaces/Makefile.idl
#

#
# List of all IDL interface files
#

DCS_IDL_FILES = \
	dc.idl	\
	repl_dc.idl	\
	repl_dc_data.idl	\
	solobj.idl

PXFS_IDL_FILES = \
	bulkio.idl		\
	px_aio.idl		\
	pxfs.idl		\
	pxfs_v1.idl		\
	repl_pxfs.idl		\
	repl_pxfs_v1.idl

# See below for adding IXINCLUDES rule if adding a new .idl file

CCR_IDL_FILES =			\
	ccr.idl			\
	ccr_data.idl		\
	ccr_trans.idl		\
	component_state.idl

CMM_IDL_FILES =			\
	cmm.idl			\
	membership.idl		\
	ff.idl			\
	remote_exec.idl

EVENTS_IDL_FILES =		\
	clevent.idl

COMM_IDL_FILES = \
	sol.idl			\
	network.idl		\
	quantum_leap.idl	\
	ifconfig_proxy.idl	\
	ns_test.idl		\
	pnm_mod.idl		\
	rtreg_proxy.idl

GLOBAL_NETWORKING_IDL_FILES =	\
	streams.idl

HAFRMWK_IDL_FILES =		\
	repl_rm.idl		\
	replica_int.idl		\
	replica.idl

NS_IDL_FILES =			\
	naming.idl		\
	repl_ns.idl

ORB_IDL_FILES =			\
	addrspc.idl		\
	data_container.idl	\
	orbmsg.idl		\
	perf.idl

QUORUM_IDL_FILES =		\
	dpm.idl			\
	quorum.idl

RGM_IDL_FILES =			\
	rgm.idl

VM_IDL_FILES =			\
	version_manager.idl	\
	version.idl

TEST_IDL_FILES =		\
	ha_stress.idl		\
	flowcontrol_fi.idl	\
	mc_sema.idl		\
	orbtest.idl		\
	orphan_fi.idl		\
	ref_stress.idl		\
	test_kproxy.idl		\
	unref_fi.idl	

IDL_FILES =			\
	$(CCR_IDL_FILES)	\
	$(CLCONF_IDL_FILES)	\
	$(CMM_IDL_FILES)	\
	$(DCS_IDL_FILES)	\
	$(EVENTS_IDL_FILES)	\
	$(COMM_IDL_FILES)	\
	$(GLOBAL_NETWORKING_IDL_FILES)	\
	$(HAFRMWK_IDL_FILES)	\
	$(NS_IDL_FILES)		\
	$(ORB_IDL_FILES)	\
	$(PXFS_IDL_FILES)	\
	$(QUORUM_IDL_FILES)	\
	$(RGM_IDL_FILES)	\
	$(VM_IDL_FILES)		\
	$(TEST_IDL_FILES)

#
# These files are compiled by the IDL compiler but the output files are
# not linked into the ORB.
#
ALL_IDL_FILES =			\
	corba_impl.idl 		\
	typetest_0.idl		\
	typetest_1.idl		\
	hatypetest_0.idl	\
	hatypetest_1.idl	\
	repl_sample.idl		\
	repl_sample_v0.idl	\
	repl_sample_v1.idl	\
	$(IDL_FILES)		

# List of subdirectories to create for storing the IDL compiler output.
IDL_SUBDIRS = h schema skels stubs tids

#
# Specify the location of the IDL compiler.
#
IX = $(SRC)/common/cl/interfaces/ixx/ixx

#
# IXCLEAN puts IX output through a script to make it cstyle-clean
#
IXCLEAN = $(SRC)/common/cl/interfaces/ixx/ixxclean

IXCPP = /usr/lib/cpp

IXFLAGS = -env

IXINCLUDES = -i '<orb/invo/invo.h>' -i '<h/solobj.h>'

IXSTUBINCLUDES = -stubinclude '<orb/invo/invocation.h>' \
	-stubinclude '<sys/mc_probe.h>' \
	-stubinclude '<orb/invo/argfuncs.h>'

IXDEFINES += $(DSOL_VERSION)

# Added for bug 4950424
$(POST_S9_BUILD)IXDEFINES += -DBUG_4954775

GENINCLUDES=$(SRC)/common/cl/interfaces/genincludes

# The base directory for C/C++ header files
CCINCLUDES += -I$(SRC)/common/cl

