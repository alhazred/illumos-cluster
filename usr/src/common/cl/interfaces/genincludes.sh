#! /bin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# ident	"@(#)genincludes.sh	1.5	08/05/20 SMI"
#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#
# Generate additional include files for idl generated headers.  These
# files are passed onto ixx as additional arguments so that the header
# files are populated with the additional include files that are needed.
#

#
# The 'sed' stage of the pipeline matches all input lines that #include
# an XXX.idl file, and for each such match, outputs "-i <h/XXX.h>"
# The 'grep' stage suppresses any output line, of the 'sed' stage,
# that matches the string "<h/solobj.h>"
# Even if the pipeline does not get any match, the 'exit 0' returns success,
# so that 'dmake' does not fail by complaining about
# the failure of the pipeline.
#
sed -n -e 's,^#include[ 	]*"\([^"]*\)\.idl".*,-i <h/\1.h>,p' "$@" | \
grep -v '<h/solobj\.h>'
exit 0
