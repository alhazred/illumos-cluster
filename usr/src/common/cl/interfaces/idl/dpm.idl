//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms. 
//
// dpm.idl - Solaris Clustering Disk Path Monitor interfaces
//
// ident	"@(#)dpm.idl	1.9	08/05/20 SMI"
//

#ifndef _dpm_idl
#define	_dpm_idl

module disk_path_monitor {

        interface dpm;
	interface dpm_policy;

        exception dpm_error {
            unsigned long error;
        };

        struct dpm_disk_t {
            string disk_path;
        };

        struct dpm_disk_status_t {
            string	disk_path;
	    long	status;
        };

        typedef sequence<dpm_disk_t> dpm_disk_seq;

        typedef sequence<dpm_disk_status_t> dpm_disk_status_seq;

        interface dpm {

                void dpm_change_monitoring_status(
                    inout dpm_disk_status_seq monitor_list,
                    inout dpm_disk_status_seq unmonitor_list)
		raises (dpm_error);

                void dpm_change_monitoring_status_all(
                    in long what)
		raises (dpm_error);

                void dpm_get_monitoring_status(
		    inout dpm_disk_status_seq disk_list,
		    in char do_probe)
		raises (dpm_error);

                void dpm_get_monitoring_status_all(
                    out dpm_disk_status_seq all_disk_list)
		raises (dpm_error);

                void dpm_get_failed_disk_status(
                    out dpm_disk_status_seq falied_disk_list)
		raises (dpm_error);

		void dpm_remove_disks(
		    inout dpm_disk_status_seq disk_list)
		raises (dpm_error);

		boolean is_alive();
        };

	// All we need to know is that someone has a reference to this object
	interface dpm_policy {
	};
};

#endif /* _dpm_idl */
