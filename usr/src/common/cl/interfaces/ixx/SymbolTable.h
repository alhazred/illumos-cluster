/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _SYMBOLTABLE_H
#define	_SYMBOLTABLE_H

#pragma ident	"@(#)SymbolTable.h	1.5	06/05/24 SMI"

/*
 * Copyright (c) 1992-1993 Silicon Graphics, Inc.
 * Copyright (c) 1993 Fujitsu, Ltd.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Silicon Graphics and Fujitsu may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Silicon Graphics and Fujitsu.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */


#include "types.h"
#include "expr.h"

// Forward declarations
class Symbol;
struct Scope;
class SymbolMap;

class SymbolTable {
	friend class Resolver;
public:
	SymbolTable();
	~SymbolTable();

	static SymbolTable	*the()	{ return (the_symbol_tablep); }

	Scope	*enter_scope(String *name);
	Scope	*scope();
	void	leave_scope();

	void	bind(String *, Symbol *);
	void	bind_in_scope(Scope *, String *, Symbol *);
	Symbol	*resolve(String *);
	Symbol	*resolve_in_scope(Scope *, String *);

	Symbol	*void_type();
	Symbol	*oneway_type();
	Symbol	*boolean_type();
	Symbol	*char_type();
	Symbol	*octet_type();
	Symbol	*short_type();
	Symbol	*ushort_type();
	Symbol	*long_type();
	Symbol	*ulong_type();
	Symbol	*longlong_type();
	Symbol	*ulonglong_type();
	Symbol	*float_type();
	Symbol	*double_type();
	Symbol	*string_type();

protected:

	Scope		*scope_;
	SymbolMap	*map_;
	Symbol		*void_;
	Symbol		*oneway_;
	Symbol		*boolean_;
	Symbol		*char_;
	Symbol		*octet_;
	Symbol		*short_;
	Symbol		*ushort_;
	Symbol		*long_;
	Symbol		*ulong_;
	Symbol		*longlong_;
	Symbol		*ulonglong_;
	Symbol		*float_;
	Symbol		*double_;
	Symbol		*string_;

private:
	static SymbolTable	*the_symbol_tablep;

};

inline Symbol *SymbolTable::void_type()		{ return void_; }
inline Symbol *SymbolTable::oneway_type()	{ return oneway_; }
inline Symbol *SymbolTable::boolean_type()	{ return boolean_; }
inline Symbol *SymbolTable::char_type()		{ return char_; }
inline Symbol *SymbolTable::octet_type()	{ return octet_; }
inline Symbol *SymbolTable::short_type()	{ return short_; }
inline Symbol *SymbolTable::ushort_type()	{ return ushort_; }
inline Symbol *SymbolTable::long_type()		{ return long_; }
inline Symbol *SymbolTable::ulong_type()	{ return ulong_; }
inline Symbol *SymbolTable::longlong_type()	{ return longlong_; }
inline Symbol *SymbolTable::ulonglong_type()	{ return ulonglong_; }
inline Symbol *SymbolTable::float_type()	{ return float_; }
inline Symbol *SymbolTable::double_type()	{ return double_; }
inline Symbol *SymbolTable::string_type()	{ return string_; }

#endif	/* _SYMBOLTABLE_H */
