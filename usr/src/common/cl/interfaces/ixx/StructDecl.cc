//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

/*
 * Copyright (c) 1992-1993 Silicon Graphics, Inc.
 * Copyright (c) 1993 Fujitsu, Ltd.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Silicon Graphics and Fujitsu may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Silicon Graphics and Fujitsu.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

#pragma ident	"@(#)StructDecl.cc	1.10	06/05/24 SMI"

#include "generator.h"
#include "StructDecl.h"
#include "Symbol.h"
#include "Resolver.h"
#include "StructMember.h"
#include "InterfaceDef.h"
#include "Declarator.h"
#include "SymbolTable.h"

//
// Class StructDecl Methods
//

StructDecl::StructDecl(SourcePosition *p, Identifier *ident,
    ExprList *members) :
	ExprImpl(p),
	ident_(ident),
	members_(members)
{
}

StructDecl::~StructDecl()
{
}

bool
StructDecl::compute_varying()
{
	if (members_ != nil) {
		for (ListItr(ExprList) i(*members_); i.more(); i.next()) {
			if (i.cur()->varying()) {
				return (true);
			}
		}
	}
	return (false);
}

//
// Perform semantic checks and define symbols.
//
void
StructDecl::resolve(Resolver *r)
{
	Symbol *s = r->new_symbol(ident_);
	symbol_ = s;
	s->struct_tag(this);
	block_ = r->enter_scope(ident_);
	resolve_list(members_, r);
	r->leave_scope();
	r->symbol(s);
	s->hash()->add_list(members_);
}

//
// Generate header file declarations.
//
bool
StructDecl::generate(Generator *g)
{
	String *s = ident_->string();

	g->emit("struct %I {", s);
	if (members_ != nil) {
		g->emit("\n%i");
		if (varying()) {
			g->emit("%I();\n", s);
			g->emit("%I(const %I &);\n", s);
			g->emit("~%I();\n", s);
			g->emit("%I &operator = (const %I &);\n\n", s);
		}
		generate_list(members_, &Expr::generate, g, ";\n");
		g->emit(";\n%u");
	} else {
		g->emit(" ");
	}
	g->emit("};\n");

	if (varying()) {
		g->emit("typedef _T_out_<%I, %I *> %I_out;\n", s);
	}
	g->emit("typedef _T_var_<%I, %I *> %I_var", s);
	return (true);
}

bool
StructDecl::generate_name(Generator *g)
{
	g->emit("%I", ident_->string());
	return (true);
}

bool
StructDecl::generate_type_name(Generator *g)
{
	g->add_operation_param("struct");
	g->add_operation_param(ident_->string()->string());
	return (generate_list(members_, &Expr::generate_type_name, g));
}

bool
StructDecl::generate_extern_stubs(Generator *g)
{
	bool b = false;
	if (!symbol_->declared_stub(g->file_mask())) {
		symbol_->declare_stub(g->file_mask());
		g->emit("extern void _%Y_put(%< &, void *);\n", nil, this);
		g->emit("extern void _%Y_get(%< &, void *);\n", nil, this);
		if (varying()) {
			g->emit("extern void _%Y_release(void *);\n",
			    nil, this);
		}
		if (ExprValueAnyobjs()) {
			g->emit("extern void _%Y_unput(void *);\n", nil, this);
		}
		g->emit("extern MarshalInfo_complex _%Y_ArgMarshalFuncs;\n",
		    nil, this);
		b = true;
	}
	return (b);
}

//
// Generate any functions needed to support this type
// at marshal time, such as put, get, etc.
//
bool
StructDecl::generate_stub(Generator *g)
{
	g->enter_scope(block_);
	generate_list(members_, &Expr::generate_stub, g, "\n");
	g->leave_scope();

	//
	// Output extern declarations for the elements that need them.
	// (i.e., those not output from the above tree walk)
	//
	if (generate_list(members_, &Expr::generate_extern_stubs, g)) {
		g->emit("\n");
	}

	String *stringp = ident_->string();

	g->emit("//\n"
	    "// Marshal time support functions for %F\n"
	    "//\n\n", nil, this);

	//
	// Generate marshal function.
	//
	g->emit("void\n"
	    "_%Y_put(%< &_b, void *voidp)\n"
	    "{\n%i"
	    "%F *structp = (%F *)voidp;\n", nil, this);
	generate_list(members_, &Expr::generate_marshal, g);
	g->emit("%u}\n\n");

	//
	// Generate unmarshal function.
	//
	g->emit("void\n"
	    "_%Y_get(%< &_b, void *voidp)\n"
	    "{%i\n"
	    "%F *structp = (%F *)voidp;\n", nil, this);
	generate_list(members_, &Expr::generate_unmarshal, g);
	g->emit("%u}\n\n");

	if (varying()) {
		// Generate default constructor.
		g->need_sep(true);
		g->emit("%Q::%I() %i", stringp);
		put_member_list(g, &StructDecl::put_ctor_member);
		g->emit("%u\n{\n}\n\n", stringp);

		// Generate copy constructor.
		g->need_sep(true);
		g->emit("%Q::%I(const %I &__s) %i", stringp);
		put_member_list(g, &StructDecl::put_copy_ctor_hdr_member);
		g->emit("%u\n{\n%i");
		put_member_list(g, &StructDecl::put_copy_ctor_body_member);
		g->emit("%u}\n\n");

		// Generate destructor.
		g->emit("%Q::~%I()\n{\n%i", stringp);
		put_member_list(g, &StructDecl::put_dtor_member);
		g->emit("%u}\n\n");

		g->emit("%Q &\n"
		    "%Q::operator = (const %I &__s)\n"
		    "{\n%i", stringp);
		put_member_list(g, &StructDecl::put_asg_member);
		g->emit("return (*this);\n");
		g->emit("%u}\n\n");

		//
		// Generate make function.
		//
		g->emit("void *\n"
		    "_%Y_makef()\n"
		    "{%i\nreturn (new %F);\n"
		    "%u}\n\n", nil, this);

		//
		// Generate free function.
		//
		g->emit("void\n"
		    "_%Y_freef(void *voidp)\n"
		    "{%i\n"
		    "%F *structp = (%F *)voidp;\n"
		    "delete structp;\n"
		    "%u}\n\n", nil, this);

		//
		// Generate release function.
		//
		g->emit("void\n"
		    "_%Y_release(void *voidp)\n"
		    "{\n%i"
		    "%F *structp = (%F *)voidp;\n", nil, this);
		generate_list(members_, &Expr::generate_release, g);
		g->emit("%u}\n\n");
	}

	//
	// Generate marshal roll back function,
	// which is only required if the struct can possibly
	// contain object references.
	//
	if (ExprValueAnyobjs()) {
		g->emit("void\n"
		    "_%Y_unput(void *voidp)\n"
		    "{\n%i"
		    "%F *structp = (%F *)voidp;\n", nil, this);
		generate_list(members_, &Expr::generate_marshal_rollback, g);
		g->emit("%u}\n\n");
	}

	//
	// Create a structure containing references to all
	// of the marshal time support functions.
	//
	g->emit("MarshalInfo_complex _%Y_ArgMarshalFuncs = {\n%i"
	    "&_%Y_put,\n"
	    "&_%Y_get,\n", nil, this);
	if (varying()) {
		g->emit("&_%Y_makef,\n"
		    "&_%Y_freef,\n"
		    "&_%Y_release,\n", nil, this);
	} else {
		g->emit("NULL,\n"
		    "NULL,\n"
		    "NULL,\n");
	}
	if (ExprValueAnyobjs()) {
		g->emit("&_%Y_unput,\n", nil, this);
	} else {
		g->emit("NULL,\n");	// no unput method
	}
	g->emit("NULL\n");		// no marshal_size method
	g->emit("%u};\n");

	symbol_->declare_stub(g->file_mask());
	return (true);
}

void
StructDecl::put_member_list(Generator *g,
    void (StructDecl::*func)(Generator *, Expr *type, Expr *e),
    const char *sep)
{
	bool need_sep = false;
	for (ListItr(ExprList) e(*members_); e.more(); e.next()) {
		StructMember *m = e.cur()->symbol()->struct_member();
		// valid struct => m != nil
		for (ListItr(ExprList) d(*m->declarators()); d.more();
		    d.next()) {
			if (need_sep) {
				g->emit(sep);
			}
			(this->*func)(g, m->type(), d.cur());
			need_sep = sep != nil;
		}
	}
}

void
StructDecl::put_ctor_member(Generator *g, Expr *type, Expr *e)
{
	if (type->string_type()) {
		if (g->need_sep(false)) {
			g->emit(":\n");
		} else {
			g->emit(",\n");
		}
		g->emit("%N((char *)NULL)", nil, e);
	} else if (type->interface_type()) {
		if (g->need_sep(false)) {
			g->emit(":\n");
		} else {
			g->emit(",\n");
		}
		g->emit("%N(", nil, e);
		bool b = g->interface_is_ref(false);
		g->emit("%F::_nil())", nil, type);
		g->interface_is_ref(b);
	}
}

void
StructDecl::put_copy_ctor_hdr_member(Generator *g, Expr *, Expr *e)
{
	if (e->actual_type()->tag() != Symbol::sym_array) {
		if (g->need_sep(false)) {
			g->emit(":\n");
		} else {
			g->emit(",\n");
		}
		g->emit("%N(__s.%N)", nil, e);
	}
}

void
StructDecl::put_copy_ctor_body_member(Generator *g, Expr *, Expr *e)
{
	Symbol *s = e->actual_type();
	if (s->tag() == Symbol::sym_array) {
		Declarator *d = s->array();
		Symbol *t = d->element_type()->actual_type();
		bool is_ptr = (t->tag() == Symbol::sym_interface ||
		    t == SymbolTable::the()->string_type());
		ExprList *subs = d->subscripts();
		g->emit_array_loop_start(subs);
		long n = subs->count();
		g->emit("%N", nil, e);
		g->emit_array_loop_indices(n);
		g->emit(" = __s.%N", nil, e);
		g->emit_array_loop_indices(n);
		g->emit(";\n");
		g->emit_array_loop_finish(n);
	}
}

void
StructDecl::put_dtor_member(Generator *g, Expr *type, Expr *e)
{
	if (type->string_type()) {
		g->emit("delete [] (char *)%N;\n", nil, e);
	} else if (type->interface_type()) {
		g->emit("CORBA::release(");
		bool b = g->interface_is_ref(false);
		type->actual_type()->interface()->put_cast_up(g);
		// You need the "" here to not confuse SCCS!
		g->emit("(%F""%p)", nil, type);
		g->interface_is_ref(b);
		g->emit("%N);\n", nil, e);
	}
}

void
StructDecl::put_asg_member(Generator *g, Expr *, Expr *e)
{
	Symbol *s = e->actual_type();
	if (s->tag() == Symbol::sym_array) {
		Declarator *d = s->array();
		ExprList *subs = d->subscripts();
		g->emit_array_loop_start(subs);
		long n = subs->count();
		g->emit("%N", nil, e);
		g->emit_array_loop_indices(n);
		g->emit(" = __s.%N", nil, e);
		g->emit_array_loop_indices(n);
		g->emit(";\n");
		g->emit_array_loop_finish(n);
	} else {
		g->emit("%N = __s.%N;\n", nil, e);
	}
}
