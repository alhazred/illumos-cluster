//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

/*
 * Copyright (c) 1992-1993 Silicon Graphics, Inc.
 * Copyright (c) 1993 Fujitsu, Ltd.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Silicon Graphics and Fujitsu may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Silicon Graphics and Fujitsu.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

#pragma ident	"@(#)UnionDecl.cc	1.11	06/05/24 SMI"

#include "generator.h"
#include "UnionDecl.h"
#include "Symbol.h"
#include "Resolver.h"
#include "UnionMember.h"
#include "SymbolTable.h"

//
// Class UnionDecl Methods
//

UnionDecl::UnionDecl(SourcePosition *p, Identifier *ident, Expr *type,
    CaseList *cases) :
	ExprImpl(p),
	ident_(ident),
	type_(type),
	cases_(cases)
{
}

UnionDecl::~UnionDecl()
{
}

bool
UnionDecl::compute_varying()
{
	for (ListItr(CaseList) i(*cases_); i.more(); i.next()) {
		if (i.cur()->varying()) {
			return (true);
		}
	}
	return (false);
}

void
UnionDecl::resolve(Resolver *r)
{
	Symbol *s = r->new_symbol(ident_);
	symbol_ = s;
	switch_type_ = switch_type(r);
	if (switch_type_ != nil) {
		case_table_ = new CaseTable(20);
		default_label_ = nil;
		s->union_tag(this);
		r->push_context(s);
		//
		// Create two scopes because generated C++ has extra scope:
		//	struct { ... union { ... } }
		//
		r->enter_scope(ident_);
		block_ = SymbolTable::the()->
		    enter_scope(new String("_Ix_union"));
		for (ListItr(CaseList) c(*cases_); c.more(); c.next()) {
			c.cur()->resolve(r);
		}
		r->leave_scope();
		r->leave_scope();
		r->symbol(s);
		r->pop_context();
		check_cases(r);
		delete case_table_;
		case_table_ = nil;
	}
}

Symbol *
UnionDecl::switch_type(Resolver *r)
{
	r->push_context();
	type_->resolve(r);
	Symbol *type_sym = r->context()->out_symbol;
	r->pop_context();
	if (type_sym == nil) {
		return (nil);
	}
	SymbolTable *t = SymbolTable::the();
	if (type_sym != t->long_type() && type_sym != t->short_type() &&
	    type_sym != t->ulong_type() && type_sym != t->ushort_type() &&
	    type_sym != t->char_type() && type_sym != t->boolean_type() &&
	    type_sym->enum_tag() == nil) {
		r->error(type_, "Bad switch type for union");
		return (nil);
	}
	return (type_sym);
}

void
UnionDecl::check_cases(Resolver *r)
{
	unsigned long count = default_label_ ? 1 : 0;
	for (TableIterator(CaseTable) i(*case_table_); i.more(); i.next()) {
		++count;
	}
	unsigned long max = ~0; // long or unsigned long
	SymbolTable *t = SymbolTable::the();
	if (switch_type_ == t->boolean_type()) {
		max = 2;
	} else if (switch_type_ == t->short_type() ||
	    switch_type_ == t->ushort_type()) {
		max = 1 << 16;
	} else if (switch_type_ == t->char_type()) {
		max = 1 << 8;
	} else {
		EnumDecl *enum_decl = switch_type_->enum_tag();
		if (enum_decl != nil) {
			max = enum_decl->enums();
		}
	}
	if (count > max) {
		r->error(type_, "Maximum number of case labels exceeded");
	}
}

bool
UnionDecl::generate(Generator *g)
{
	String *stringp = ident_->string();

	g->emit("class %I {\npublic:\n%i", stringp);
	g->emit("%E _d_;\n", nil, type_);
	g->emit("union _Ix_union {\n%i");
	for (ListItr(CaseList) c(*cases_); c.more(); c.next()) {
		c.cur()->generate(g);
		g->emit(";\n");
	}
	bool vary = varying();
	if (vary) {
		g->emit("void *__init_;\n");
	}
	g->emit("%u} _u;\n");

	if (vary) {
		g->emit("\nvoid _free();\n\n");
		g->emit("%I();\n", stringp);
		g->emit("%I(const %I &__u) { *this = __u; }\n", stringp);
		g->emit("~%I();\n", stringp);
		g->emit("%I &operator = (const %I &);\n", stringp);
	}
	g->emit("\nconst %F _d() const { return (_d_); }\n", nil, type_);
	g->emit("void _d(%F);\n", nil, type_);
	for (ListItr(CaseList) i(*cases_); i.more(); i.next()) {
		g->emit("\n");
		generate_access_hdr(g, i.cur());
	}
	g->emit("%u};\n");

	if (vary) {
		g->emit("typedef _T_out_<%I, %I *> %I_out;\n", stringp);
		g->emit("typedef _T_var_<%I, %I *> %I_var", stringp);
	} else {
		g->emit("typedef %I %I_var", stringp);
	}
	return (true);
}

void
UnionDecl::generate_access_hdr(Generator *g, CaseElement *c)
{
	UnionMember *u = c->element();
	Expr *t = u->type();
	Expr *e = u->declarators()->item(0);
	bool v = t->varying();
	bool addr = t->addr_type();
	bool is_string = !addr && t->string_type();

	// const get
	if (addr) {
		g->emit("const ");
	}
	g->emit("%F ", nil, t);
	if (addr) {
		g->emit("&");
	}
	g->emit("%E() const { return (", nil, e);
	if (v && !is_string && !t->interface_type()) {
		g->emit("*");
	}
	g->emit("_u.%E); }\n", nil, e);

	// non-const get (aggregates only)
	if (addr) {
		g->emit("%F", nil, t);
		g->emit(" &%E() { return (", nil, e);
		if (v && !is_string && !t->interface_type()) {
			g->emit("*");
		}
		g->emit("_u.%E); }\n", nil, e);
	}

	// set
	g->emit("void %E(", nil, e);
	if (addr) {
		g->emit("const ");
	}
	g->emit("%F", nil, t);
	if (addr) {
		g->emit(" &");
	}
	g->emit(");\n");
}

bool
UnionDecl::generate_name(Generator *g)
{
	g->emit("%I", ident_->string());
	return (true);
}

bool
UnionDecl::generate_type_name(Generator *g)
{
	g->add_operation_param("union");
	g->add_operation_param(ident_->string()->string());
	for (ListItr(CaseList) i(*cases_); i.more(); i.next()) {
		(void) i.cur()->generate_type_name(g);
	}
	return (false);
}

bool
UnionDecl::generate_extern_stubs(Generator *g)
{
	bool b = false;
	if (!symbol_->declared_stub(g->file_mask())) {
		symbol_->declare_stub(g->file_mask());
		g->emit("extern void _%Y_put(%< &, void *);\n", nil, this);
		g->emit("extern void _%Y_get(%< &, void *);\n", nil, this);
		if (varying()) {
			g->emit("extern void _%Y_release(void *);\n",
			    nil, this);
		}
		if (ExprValueAnyobjs()) {
			g->emit("extern void _%Y_unput(void *);\n", nil, this);
		}
		g->emit("extern MarshalInfo_complex _%Y_ArgMarshalFuncs;\n",
		    nil, this);
		b = true;
	}
	return (b);
}

//
// Generate any functions needed to support this type
// at marshal time, such as put, get, etc.
//
bool
UnionDecl::generate_stub(Generator *g)
{
	g->enter_scope(block_);
	for (ListItr(CaseList) c(*cases_); c.more(); c.next()) {
		c.cur()->element()->generate_stub(g);
	}
	g->leave_scope();

	//
	// Output extern declarations for the elements that need them.
	// (i.e., those not output from the above tree walk)
	//
	bool b = type_->generate_extern_stubs(g);
	for (ListItr(CaseList) e(*cases_); e.more(); e.next()) {
		b |= e.cur()->element()->generate_extern_stubs(g);
	}
	if (b) {
		g->emit("\n");
	}

	String *stringp = ident_->string();

	g->emit("void\n"
	    "%:%I::_d(%X _nd)\n"
	    "{\n%i", stringp, type_);

	bool vary = varying();
	if (vary) {
		g->emit("_free();\n");
	}
	g->emit("_d_ = _nd;\n");
	g->emit("%u}\n\n");

	for (ListItr(CaseList) i(*cases_); i.more(); i.next()) {
		generate_access_impl(g, i.cur());
	}

	g->emit("//\n"
	    "// Marshal time support functions for %F\n"
	    "//\n\n", nil, this);

	//
	// Generate marshal function.
	//
	g->emit("void\n"
	    "_%Y_put(%< &_b, void *voidp)\n"
	    "{\n%i"
	    "%F *unionp = (%F *)voidp;\n", nil, this);
	g->emit_put(type_, "unionp->_d()", type_);
	g->emit("switch (unionp->_d()) {\n");
	for (ListItr(CaseList) j(*cases_); j.more(); j.next()) {
		j.cur()->generate_copy(&Expr::generate_marshal, g);
	}
	g->emit("}\n"
	    "%u}\n\n");

	//
	// Generate unmarshal function.
	//
	g->emit("void\n"
	    "_%_%I_get(%< &_b, void *voidp)\n"
	    "{\n%i"
	    "%F *unionp = (%F *)voidp;\n",
	    stringp, this);
	g->emit("unionp->_d(");
	Symbol *symbolp = type_->actual_type();
	switch (symbolp->tag()) {
	case Symbol::sym_enum:
		g->emit("%F(_b.get_long())", nil, type_);
		break;
	case Symbol::sym_typedef:
		g->emit("_b.get_%I()", symbolp->type_name()->str());
		break;
	default:
		// shouldn't happen
		g->handler()->internal("bad union tag type");
		break;
	}
	g->emit(");\nswitch (unionp->_d()) {\n");
	for (ListItr(CaseList) u(*cases_); u.more(); u.next()) {
		u.cur()->generate_copy(&Expr::generate_unmarshal, g);
	}
	g->emit("}\n%u"
	    "}\n\n");

	// Determine whether the size of this data entity varies.
	if (vary) {
		g->emit("%:%I::%I()\n"
		    "{%i\n"
		    "_u.__init_ = 0;\n"
		    "%u}\n\n", stringp);

		g->emit("%:%I::~%I()\n"
		    "{%i\n"
		    "_free();\n"
		    "%u}\n\n", stringp);

		g->emit("%:%I &\n"
		    "%:%I::operator = (const %I &_this)\n"
		    "{\n%i", stringp);
		g->emit("switch (_this._d()) {\n");
		for (ListItr(CaseList) k(*cases_); k.more(); k.next()) {
			generate_assign(g, k.cur());
		}
		g->emit("}\n"
		    "return (*this);\n"
		    "%u}\n\n");

		g->emit("void\n"
		    "%:%I::_free()\n"
		    "{\n%i", stringp);
		g->emit("switch (_d()) {\n");
		for (ListItr(CaseList) l(*cases_); l.more(); l.next()) {
			generate_free(g, l.cur());
		}
		g->emit("}\n"
		    "%u}\n\n");

		//
		// Generate make function.
		//
		g->emit("void *\n"
		    "_%Y_makef()\n"
		    "{%i\n"
		    "return (new %F);\n"
		    "%u}\n\n", nil, this);

		//
		// Generate free function.
		//
		g->emit("void\n"
		    "_%Y_freef(void *voidp)\n"
		    "{%i\n"
		    "%F *unionp = (%F *)voidp;\n"
		    "delete unionp;\n"
		    "%u}\n\n", nil, this);

		//
		// Generate release function.
		//
		g->emit("void\n"
		    "_%Y_release(void *voidp)\n"
		    "{\n%i"
		    "%F *unionp = (%F *)voidp;\n",
		    nil, this);
		g->emit("switch (unionp->_d()) {\n");
		for (ListItr(CaseList) m(*cases_); m.more(); m.next()) {
			m.cur()->generate_copy(&Expr::generate_release, g);
		}
		g->emit("}\n"
		    "%u}\n\n");
	}

	//
	// Generate marshal roll back function,
	// which is only required if the union can possibly
	// contain object references.
	//
	if (ExprValueAnyobjs()) {
		g->emit("void\n"
		    "_%Y_unput(void *voidp)\n"
		    "{\n%i"
		    "%F *unionp = (%F *)voidp;\n", nil, this);
		g->emit("switch (unionp->_d()) {\n");
		for (ListItr(CaseList) ii(*cases_); ii.more(); ii.next()) {
			ii.cur()->
			    generate_copy(&Expr::generate_marshal_rollback, g);
		}
		g->emit("}\n"
		    "%u}\n\n");
	}

	//
	// Create a structure containing references to all
	// of the marshal time support functions.
	//
	g->emit("MarshalInfo_complex _%Y_ArgMarshalFuncs = {\n%i"
	    "&_%Y_put,\n"
	    "&_%Y_get,\n", nil, this);
	if (vary) {
		g->emit("&_%Y_makef,\n"
		    "&_%Y_freef,\n"
		    "&_%Y_release,\n", nil, this);
	} else {
		g->emit("NULL,\n"
		    "NULL,\n"
		    "NULL,\n");
	}
	if (ExprValueAnyobjs()) {
		g->emit("&_%Y_unput,\n", nil, this);
	} else {
		g->emit("NULL,\n");	// no unput method
	}
	g->emit("NULL\n");		// no marshal_size method
	g->emit("%u};\n");

	symbol_->declare_stub(g->file_mask());
	return (true);
}

//
// Generate the set function for a given union member (case element).
//
void
UnionDecl::generate_access_impl(Generator *g, CaseElement *casep)
{
	String		*stringp = ident_->string();
	UnionMember	*umemberp = casep->element();
	Expr		*typep = umemberp->type();
	Expr		*exprp = umemberp->declarators()->item(0);

	g->emit("void\n"
	    "%:%I::%E(", stringp, exprp);
	bool	addr = typep->addr_type();
	if (addr) {
		g->emit("const ");
	}
	g->emit("%F ", nil, typep);
	if (addr) {
		g->emit("&");
	}
	g->emit("_v)\n"
	    "{\n%i", nil, typep);
	generate_set_tag(g, casep);
	// XXX Should duplicate string and possibly duplicate interface ref.
	if (typep->varying() && !typep->string_type() &&
	    !typep->interface_type()) {
		g->emit("_u.%E = new ", nil, exprp);
		g->emit("%F(_v);\n", nil, typep);
	} else {
		g->emit("_u.%E = _v;\n", nil, exprp);
	}
	g->emit("%u}\n\n");
}

void
UnionDecl::generate_set_tag(Generator *g, CaseElement *c)
{
	ExprList *list = c->labels();
	if (list->count() == 1) {
		Expr *e = list->item(0);
		if (e != default_label_) {
			g->emit("_d(%E);\n", nil, e);
		}
	}
}

void
UnionDecl::generate_free(Generator *g, CaseElement *c)
{
	UnionMember *u = c->element();
	Expr *t = u->type();
	Expr *e = u->declarators()->item(0);
	for (ListItr(ExprList) i(*c->labels()); i.more(); i.next()) {
		i.cur()->generate_marshal(g);
		if (t->interface_type()) {
			g->emit("%iCORBA::release(_u.%E);\nbreak;\n%u", nil, e);
		} else if (t->string_type()) {
			g->emit("%idelete [] _u.%E;\nbreak;\n%u", nil, e);
		} else if (t->varying()) {
			g->emit("%idelete _u.%E;\nbreak;\n%u", nil, e);
		} else {
			g->emit("%ibreak;\n%u", nil, e);
		}
	}
}

void
UnionDecl::generate_assign(Generator *g, CaseElement *c)
{
	ExprList *list = c->labels();
	for (ListItr(ExprList) i(*list); i.more(); i.next()) {
		i.cur()->generate_marshal(g);
	}
	g->emit("%i");
	if (list->count() != 1 || list->item(0) == default_label_) {
		g->emit("_d(_this._d());\n");
	}
	g->emit("%E(_this.%E());\n", nil, c->element()->declarators()->item(0));
	g->emit("break;\n%u");
}
