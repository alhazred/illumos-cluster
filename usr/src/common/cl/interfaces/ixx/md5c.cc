/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright (c) 2001 by Sun Microsystems, Inc.
 * All rights reserved.
 */
#pragma ident	"@(#)md5c.cc	1.9	08/05/20 SMI"

#ifdef linux
#include <sys/types.h>
#include <string.h>
#define	PROTOTYPES 1
extern "C" {
#include <md5global.h>
#include <md5.h>
}
void md5_calc(unsigned char *, unsigned char *, unsigned int);
#else
#include <strings.h>
#include <md5.h>
#endif

/*
 * Digests a string and place the results in 'digest'.
 */
void
MDString(const char *string, unsigned char *digest)
{
	MD5_CTX context;
	unsigned int len = (unsigned int)strlen(string);

	MD5Init(&context);
	MD5Update(&context, (unsigned char *)string, len);
	MD5Final(digest, &context);
}
