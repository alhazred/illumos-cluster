/*
 *  Copyright 2005 Sun Microsystems, Inc.  All rights reserved.
 *  Use is subject to license terms.
 *
 */

#ifndef _GENERATOR_H
#define	_GENERATOR_H

#pragma ident	"@(#)generator.h	1.31	05/10/27 SMI"

/*
 * Copyright (c) 1992-1993 Silicon Graphics, Inc.
 * Copyright (c) 1993 Fujitsu, Ltd.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Silicon Graphics and Fujitsu may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Silicon Graphics and Fujitsu.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */


#include "err.h"
#include "expr-impl.h"
#include "list.h"

#include <stdio.h>

#ifndef ADB_MACRO
// the following lint comment disables a warning caused bugs in flexelint
/*lint -save -e404 */
#include <sstream>
/*lint -restore */
#endif

declarePtrList(StringList, String)

class Generator {
public:
	Generator(ErrorHandler *, const ConfigInfo &);
	virtual ~Generator();

	enum ParamFlags {
		emit_actuals = 0x1,
		emit_formals = 0x2,
		emit_env = 0x4,
		emit_body = 0x8,
		emit_env_actuals = 0x5,
		emit_env_formals = 0x6,
		emit_env_formals_body = 0xe
	};

	struct IdInfo {
		Expr	*name;
	};

	struct IdTable {
		IdInfo	*info;
		IdInfo	*cur;
		long	size;
		long	free;
	};

	ErrorHandler *handler();
	bool has_request();
	bool is_pure();
	bool is_pure(bool);
	bool has_env();
	bool has_copy_outs();
	FILE *stubfile();
	FILE *serverfile();
	FILE *schemafile();
	FILE *tidfile();
	long file_mask();
	bool qualify();
	bool formals();
	bool concat();
	bool concat(bool);
	void counter(long n);
	long counter();
	void count(long delta);

	bool set_source(SourcePosition *);
	bool is_source();
	bool was_source();
	bool need_ifndef();

	bool begin_file(FILE *);
	void end_file();

	bool interface_is_ref(bool);
	bool need_sep(bool);
	bool op_body(bool);
	bool is_op_body();
	bool array_decl(bool);
	bool is_array_decl();

	void push_prefix(String *);
	void pop_prefix();
	String *prefix();

	void enter_scope(Scope *);
	Scope *scope();
	void leave_scope();

	void emit_tid(unsigned char *type, String *name);
	void add_id_to_table(Expr *name);
	void emit_schema_id_table();

	bool aggr_decl(Expr *);
	bool need_extern(Expr *);

	void emit(const char *format, String * = nil, Expr * = nil);
	void emit_str(const char *, long length);
	void emit_substr(const char *, long length);
	void emit_tab(long n);
	void emit_include_newline();
	void emit_flush(const char *p, const char *start);
	void emit_format(int ch, String *, Expr *);
	void emit_expr_scope(Expr *e);
	void copy(const char *);

	void emit_char(long);
	void emit_chars_length(const char *, long length);
	void emit_integer(long);
	void emit_hexinteger(long);
	void emit_float(double);
	void emit_declarator_ident(Identifier *);
	void emit_op(Opcode);
	void emit_filename(String *);
	void emit_ifndef(String *);
	void emit_endif(String *);
	void emit_file_tag(const char *);
	void emit_include(String *);
	void emit_includes();
	void emit_stub_includes(String *);
	void emit_server_includes(String *);
	void emit_param_list(ExprList *params, ParamFlags flags);
	void emit_param_decls(ExprList *params, ParamFlags flags);
	void emit_env_param(ParamFlags flags);
	void unused_params(char *);
	bool is_unused_param(const char *str, long length);
	long emit_class_tid(Expr *expr_ptr, long index, long version);
	void emit_class_no_methods_tid(Expr *expr_ptr, long version);
	void emit_class_func(Expr *expr_ptr);
	void emit_func_extern(Expr *expr_ptr);

	void emit_type_to_func_info(Expr *expr_ptr);
	void emit_offset_info(String *name, Expr *expr_ptr);

	bool emit_scope(Scope *);
	bool emit_scope_only(Scope *);
	bool emit_name_only(Scope *);

	void emit_put(Expr *type, const char *format, Expr *value);
	void emit_array_loop_start(ExprList *subscripts);
	void emit_array_loop_indices(long nsubscripts);
	void emit_array_loop_finish(long nsubscripts);
	void emit_get(Expr *type, const char *format, Expr *value);
	void emit_release(Expr *type, const char *format, Expr *value);
	void emit_unput(Expr *type, const char *format, Expr *value);
	void emit_edit_warning(String *);
	void flush();

	// Operations that manipulate the typeid signature for an interface
	char *get_type_string();
	char *get_module_string();
	void start_module(const char *s);
	void start_interface(const char *s, long v);
	void add_interface_parent(const char *s, long v);
	void add_operation(const char *s);
	void start_operation(const char *s);
	void add_operation_param(const char *s);
	void add_operation_attribute(int i);

	// Start a typeid signature for an exception
	void start_exception(char *separator_p, Expr *expr_p,
	    Identifier *ident_p);

private:
	ErrorHandler	*handler_;
	FILE		*out_;
	long		mask_;
	FILE		*stubfile_;
	FILE		*serverfile_;
	FILE		*schemafile_;
	FILE		*tidfile_;
	StringList	*symbol_id_start_;
	StringList	*symbol_id_end_;
	bool		generate_include_;
	const char	*inclpath_;
	long		inclpath_length_;
	const char	*inclext_;
	long		inclext_length_;
	StringList	*includes_;
	StringList	*stub_includes_;
	StringList	*server_includes_;
	const char	*filename_;
	const char	*prefix_;
	long		prefix_length_;
	bool		purevirtual_;
	bool		pass_env_;
	bool		copy_outs_;
	bool		source_;
	bool		was_source_;
	bool		need_ifndef_;
	bool		qualify_;
	bool		concat_;
	bool		need_sep_;
	bool		ref_;
	bool		body_;
	bool		subscripts_;
	bool		formals_;
	long		counter_;
	long		indent_;
	long		save_indent_;
	long		column_;
	long		save_column_;
	bool		include_newline_;
	String		*unused_params_;
	Scope		*scope_;
	Scope		*scope_stack_[5];
	int		scope_stackp;
	String		*impl_is_from_;
	StringList	*prefixes_;
	StringList	*files_;
	Generator::IdTable	id_table_;
	Generator::IdInfo	static_info_[100];

	// TypeId information
	// NOTE: as of 2005.10.12, the current version of macrogen
	// cannot deal with the stabs generated by std::string.
	// so we substitute generic pointer variables for when this
	// header is processed by the macrogen pipeline.
#ifndef ADB_MACRO
	std::ostringstream &type_stream;
	std::string &module_prefix;
#else
	char *type_stream;
	char *module_prefix;
#endif

	long length(const char *);
	FILE *open_output(const char *);
	void emit_include_list(StringList *);
	void emit_include_filename();
	void emit_include_substr(bool path, const char *str, long length);
	void emit_tr_filename(const char *start, const char *end);
	void append_type(const char *s);
	bool scope_get_chars(char *separator_p, Scope *scope_p);
};

inline ErrorHandler* Generator::handler() { return handler_; }
inline long Generator::file_mask() { return mask_; }
inline FILE *Generator::stubfile() { return stubfile_; }
inline FILE *Generator::serverfile() { return serverfile_; }
inline FILE *Generator::schemafile() { return schemafile_; }
inline FILE *Generator::tidfile() { return tidfile_; }
inline bool Generator::is_pure() { return purevirtual_; }
inline bool Generator::has_env() { return pass_env_; }
inline bool Generator::has_copy_outs() { return copy_outs_; }
inline bool Generator::qualify() { return qualify_; }
inline bool Generator::formals() { return formals_; }
inline bool Generator::concat() { return concat_; }
inline void Generator::counter(long n) { counter_ = n; }
inline long Generator::counter() { return counter_; }
inline void Generator::count(long delta) { counter_ += delta; }
inline bool Generator::is_source() { return source_; }
inline bool Generator::was_source() { return was_source_; }
inline bool Generator::need_ifndef() { return need_ifndef_; }
inline Scope *Generator::scope() { return scope_; }

#endif	/* _GENERATOR_H */
