//
// Copyright 2005 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

/*
 * Copyright (c) 1992-1993 Silicon Graphics, Inc.
 * Copyright (c) 1993 Fujitsu, Ltd.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Silicon Graphics and Fujitsu may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Silicon Graphics and Fujitsu.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

#pragma ident	"@(#)generator.cc	1.54	05/10/27 SMI"

//
// Generate code
//

#include "generator.h"
#include "tokendefs.h"
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include "Symbol.h"
#include "InterfaceDef.h"
#include "Operation.h"
#include "StructDecl.h"
#include "StructMember.h"
#include "UnionDecl.h"
#include "UnionMember.h"
#include "SequenceDecl.h"
#include "Declarator.h"
#include "SymbolTable.h"
#include "main.h"

implementPtrList(StringList, String)

//
// Generate a random initial symbol id
//

#include <math.h>
#include <time.h>
#include <sys/types.h>

#ifdef WIN32
#include <search.h>
#endif

#include <assert.h>

#if !defined(__sgi)
#define	srandom srand
#define	random rand
#endif

static char default_start_id[9];
static char default_end_id[9];

static void
create_random_ids()
{
	srandom((int)time(0));
	long r = (random() & 0x00ffff00) | 0x11000000;
	sprintf(default_start_id, "%08x", r);
	sprintf(default_end_id, "%08x", r + 0xff);
}

static char tab[] = "    ";
static const long right_margin = 65;

Generator::Generator(ErrorHandler *h, const ConfigInfo &i)
	: type_stream(*new std::ostringstream),
	    module_prefix(*new std::string)
{
	handler_ = h;
	out_ = stdout;
	mask_ = 0;
	generate_include_ = true;
	symbol_id_start_ = new StringList;
	symbol_id_end_ = new StringList;
	inclpath_ = i.inclpath;
	inclpath_length_ = length(inclpath_);
	inclext_ = i.inclext;
	inclext_length_ = length(inclext_);
	includes_ = i.includes;
	stub_includes_ = i.stub_includes;
	//
	// XXX The following is not a bug. We would need to fix Makefiles
	// to use -serverinclude instead of -stubinclude.
	//
	server_includes_ = i.stub_includes;
	filename_ = i.filename;
	stubfile_ = open_output(i.stubfile);
	serverfile_ = open_output(i.serverfile);
	schemafile_ = open_output(i.schemafile);
	tidfile_ = open_output(i.tidfile);
	prefix_ = i.prefix;
	prefix_length_ = length(prefix_);
	pass_env_ = i.pass_env;
	copy_outs_ = i.copy_outs;
	purevirtual_ = false;
	source_ = true;
	was_source_ = true;
	need_ifndef_ = true;
	qualify_ = true;
	concat_ = false;
	ref_ = true;
	need_sep_ = false;
	body_ = true;
	subscripts_ = true;
	formals_ = true;
	counter_ = 0;
	indent_ = 0;
	column_ = 0;
	include_newline_ = false;
	unused_params_ = nil;
	scope_ = nil;
	prefixes_ = new StringList;
	files_ = new StringList;
	id_table_.info = static_info_;
	id_table_.cur = id_table_.info;
	id_table_.size = sizeof (static_info_) / sizeof (static_info_[0]);
	id_table_.free = id_table_.size;
}

Generator::~Generator()
{
	delete prefixes_;
	delete files_;
	delete &type_stream;
	delete &module_prefix;
}

long
Generator::length(const char *s)
{
	return (s == nil ? 0 : strlen(s));
}

FILE *
Generator::open_output(const char *name)
{
	FILE *f = nil;
	if (name != nil) {
		f = fopen(name, "w");
		if (f == nil) {
			handler_->begin_unrecoverable();
			handler_->put_chars("Can't write output file '");
			handler_->put_chars(name);
			handler_->put_chars("'");
			handler_->end();
		}
	}
	return (f);
}

//
// Handle a possible change in source file input (preprocessor line directive).
//
bool
Generator::set_source(SourcePosition *p)
{
	if (filename_ != nil) {
		was_source_ = source_;
		// Note: file name comparison is using strcmp()!
		source_ = (p == nil || *p->filename() == filename_);
	}
	return (source_);
}

bool
Generator::begin_file(FILE *f)
{
	if (f != nil) {
		fflush(out_);
		out_ = f;
		if (out_ == stubfile_) {
			mask_ = 1;
		} else if (out_ == serverfile_) {
			mask_ = 2;
		} else if (out_ == schemafile_) {
			mask_ = 4;
		}
		assert(indent_ == 0);
		save_indent_ = indent_;
		save_column_ = column_;
		indent_ = 0;
		column_ = 0;
		return (true);
	}
	return (false);
}

void
Generator::end_file()
{
	fflush(out_);
	out_ = stdout;
	mask_ = 0;
	indent_ = save_indent_;
	column_ = save_column_;
}

bool
Generator::interface_is_ref(bool b)
{
	bool prev = ref_;
	ref_ = b;
	return (prev);
}

bool
Generator::need_sep(bool b)
{
	bool prev = need_sep_;
	need_sep_ = b;
	return (prev);
}

bool
Generator::op_body(bool b)
{
	bool prev = body_;
	body_ = b;
	return (prev);
}

bool
Generator::is_op_body()
{
	return (body_);
}

bool
Generator::array_decl(bool b)
{
	bool prev = subscripts_;
	subscripts_ = b;
	return (prev);
}

bool
Generator::is_array_decl()
{
	return (subscripts_);
}

bool
Generator::is_pure(bool b)
{
	bool prev = purevirtual_;
	purevirtual_ = b;
	return (prev);
}

bool
Generator::concat(bool b)
{
	bool prev = concat_;
	concat_ = b;
	return (prev);
}

void
Generator::push_prefix(String *s)
{
	prefixes_->prepend(s);
}

void
Generator::pop_prefix()
{
	if (prefixes_->count() > 0) {
		prefixes_->remove(0);
	}
}

String *
Generator::prefix()
{
	String *s = nil;
	if (prefixes_->count() > 0) {
		s = prefixes_->item(0);
	}
	return (s);
}

void
Generator::enter_scope(Scope *s)
{
	assert(scope_stackp < 5);
	scope_stack_[scope_stackp++] = scope_;
	scope_ = s;
}

void
Generator::leave_scope()
{
	assert(scope_stackp > 0);
	scope_ = scope_stack_[--scope_stackp];
}

//
// emit_tid -
// Write out the definition of a symbol id.
// Uses MD5 encoding for the type id.
//
void
Generator::emit_tid(unsigned char *id, String *name)
{
	int	i;
	int	j;

	// 128bit digest / (bits per int) / (bytes per int) = ints per digest
	static const int md5_len = 128 / 8 / sizeof (int);

	emit("uint32_t _Ix__tid(%_%I)[] = {\n%i", name);
	for (i = 0; i < md5_len; i++) {
		emit("0x");
		if (column_ == 0) {
			emit_include_newline();
			emit_tab(indent_);
		}
		for (j = 0; j < sizeof (int); j++) {
			fprintf(out_, "%02x", id[j]);
			column_ += 2;
		}
		if (i < (md5_len - 1)) {
			emit("U,\n");
		}
		id += sizeof (int);
	}
	emit("U\n%u};\n\n");
}

//
// Add an id to a table for later generation as part of schema initialization.
//
void
Generator::add_id_to_table(Expr *name)
{
	Generator::IdTable *t = &id_table_;
	if (t->free == 0) {
		long n = t->size + 100;
		Generator::IdInfo *new_info = new Generator::IdInfo[n];
		for (long i = t->size - 1; i >= 0; i--) {
			new_info[i] = t->info[i];
		}
		if (t->info != static_info_) {
			delete [] t->info;
		}
		t->info = new_info;
		t->cur = t->info + t->size;
		t->free = n - t->size;
		t->size = n;
	}
	Generator::IdInfo *i = t->cur;
	i->name = name;
	t->cur = i + 1;
	t->free -= 1;
}

//
// emit_schema_id_table - creates the data structures needed to
// specify the Interface (or equivalently class) definitions for the
// operational system.
//
void
Generator::emit_schema_id_table()
{
	Generator::IdTable	*tablep = &id_table_;
	Generator::IdInfo	*infop = tablep->info;
	long			used = tablep->size - tablep->free;

	//
	// Generate a list of all Interface (or equivalently class) definitions
	// that occurred in the corresponding ".idl" file.
	//
	if (used > 0) {
		emit("static InterfaceDescriptor *_ix_schema[] = {\n%i");
		for (long i = used; i > 0; i--, infop++) {
			InterfaceDef *ifp = infop->name->symbol()->interface();
			if (ifp == nil) {
				emit("&_Ix__type(%Y),\n", nil, infop->name);
				continue;
			}
			for (long v = 0; v <= ifp->version(); v++) {
				emit("&_Ix__type(%Y_", nil, infop->name);
				emit_integer(v);
				emit("),\n");
			}
		}
		emit("NULL\n%u};\n\n");

		emit("static OxInitSchema _ix_init_schema(_ix_schema);"
		    "\t//lint !e1502\n");
	}
}

//
// Check if the given expression is the declaration of an aggregate type,
// i.e., a StructDecl, UnionDecl, SequenceDecl, or array declarator.
// Since we don't have runtime type information on the expressions
// themselves, the way we do this is get the symbol for the expression and
// check if it's expression is the same as the given one (and therefore
// not a typedef).
//

bool
Generator::aggr_decl(Expr *t)
{
	Expr *e;
	Symbol *s = t->symbol();
	switch (s->tag()) {
	case Symbol::sym_array:
		e = s->array();
		break;
	case Symbol::sym_struct:
		e = s->struct_tag();
		break;
	case Symbol::sym_union:
		e = s->union_tag();
		break;
	case Symbol::sym_sequence:
		e = t;
		break;
	default:
		e = nil;
		break;
	}
	return (e == t);
}

bool
Generator::need_extern(Expr *e)
{
	bool b = true;
	Symbol *s = e->symbol();
	if (s != nil) {
		if (s->declared()) {
			b = false;
		} else {
			s->declared(true);
		}
	}
	return (b);
}

void
Generator::emit(const char *format, String *s, Expr *e)
{
	const char *start = format;
	const char *p;

	for (p = start; *p != '\0'; p++) {
		if (*p == '\n') {
			if (p > start) {
				emit_substr(start, p - start);
			}
			putc('\n', out_);
			column_ = 0;
			start = p + 1;
		} else if (*p == '%') {
			if (p > start) {
				emit_substr(start, p - start);
			}
			++p;
			emit_format(*p, s, e);
			start = p + 1;
		}
	}
	if (p > start) {
		emit_substr(start, p - start);
	}
}

void
Generator::emit_str(const char *s, long length)
{
	if (column_ == 0) {
		emit_include_newline();
		emit_tab(indent_);
	}
	fputs(s, out_);
	column_ += length;
}

void
Generator::emit_substr(const char *s, long length)
{
	if (column_ == 0) {
		emit_include_newline();
		emit_tab(indent_);
	}
	fprintf(out_, "%.*s", length, s);
	column_ += length;
}

void
Generator::emit_tab(long n)
{
	for (long i = 0; i < n; i++) {
#if 0
		fputs(tab, out_);
		column_ += sizeof (tab) - 1;
#else
		fputc('\t', out_);
		column_ += 8 - (column_ % 8);
#endif
	}
}

//
// Emit a newline if one is pending, such as from a recent #include.
//

void
Generator::emit_include_newline()
{
	if (include_newline_) {
		putc('\n', out_);
		include_newline_ = false;
	}
}

void
Generator::emit_flush(const char *p, const char *start)
{
	long n = p - start;
	if (n != 0) {
		emit_substr(start, n);
	}
}

void
Generator::append_type(const char *s)
{
	type_stream << s << "::";
}

void
Generator::start_module(const char *s)
{
	type_stream.str("");
	append_type("Sun Fresco IDL");
	append_type(s);
	module_prefix = type_stream.str();
}

void
Generator::start_interface(const char *s, long v)
{
	// Reset interface name and version.
	type_stream.str("");
	type_stream << module_prefix << s << "_" << v << "::";
}

void
Generator::add_interface_parent(const char *s, long v)
{
	type_stream << s << "_" << v << "@@";
}

void
Generator::add_operation(const char *s)
{
	type_stream << s;
}

//
// This is called during the rewrite phase to create a type string
// for just the operation.
//
void
Generator::start_operation(const char *s)
{
	type_stream.str("");
	type_stream << s << "##";
}

//
// Add a type paramter to the current operation signature.
//
void
Generator::add_operation_param(const char *s)
{
	type_stream << s << "//";
}

//
// Add a specfication to the current operation signature.
//
void
Generator::add_operation_attribute(int i)
{
	type_stream << "<" << i << ">";
}

//
// Return the constructed module string (no interfaces/operations).
// The caller should free() the returned value when done.
//
char *
Generator::get_module_string()
{
	return (strdup((module_prefix + "++Module_ID++").c_str()));
}

//
// Return the constructed type string name.
// The caller should free() the returned value when done.
//
char *
Generator::get_type_string()
{
	//
	// must call strdup here since the return value of .str()
	// is only valid within this scope.
	//
	return (strdup((char *)(type_stream.str().c_str())));
}

//
// Interpret a format specification, potentially using
// a given string and expression for substitution.
//
// Formats:
//
//    %^ - name prefix if defined
//    %p - suffix for an object pointer type
//    %r - suffix for a managed object pointer type
//    %i - increment tab indentation level
//    %u - decrement tab indentation level
//    %b - discretionary line break or space
//    %o - white space if in the body of an operation
//    %I - given string
//    %P - given string, adding ptr suffix if top-level interface type
//    %R - given string as object reference, adding ptr suffix
//    %Q - short-hand for %:%I
//    %E - generate given expression
//    %; - scope of expression using ::
//    %- - scope of expression using _
//    %X - fully-qualified expression
//    %Y - fully-qualified expression using _ instead of ::
//    %T - like %Y except void emits void instead of CORBA_void
//    %N - generate type expression's name
//    %F - generate type expression's fully-qualified name
//    %A - attribute parameter name for given (type) expression
//    %B - marshal buffer type
//    %* - emit "*" if the expression type should be a pointer result
//    %, - emit ", " if envclass is defined
//    %e - environment formal parameter (if envclass is defined)
//    %f - environment formal parameter for body (if envclass is defined)
//    %a - environment actual parameter (if envclass is defined)
//    %~ - name of current scope
//    %S - current scope with _ as separator
//    %: - current scope with :: as separator and trailer
//    %_ - current scope with _ as separator and trailer
//    %? - scope operator (::) or name separator (_)
//    %m - emit scope only
//    %n - emit name only
//    %. - scope specification (scope::) if prefix is defined
//    %= - output " = 0" if pure virtual
//    %# - put # in column 1 regardless of current indent setting
//    %> - invocation class
//    %< - Service class
//    %W - Write Marshal Stream class
//    %G - Read Marshal Stream class
//    %v - version number
//
void
Generator::emit_format(int ch, String *s, Expr *e)
{
	String *str;
	Symbol *sym;
	const char *fmt;
	bool b;

	switch (ch) {
	case '^':
		if (prefix_ != nil) {
			emit_substr(prefix_, prefix_length_);
		}
		break;
	case 'p':
		emit_str("_ptr", 4);
		break;
	case 'P':
		if (ref_ && e->interface_type()) {
			sym = e->symbol();
			if (sym != nil && sym->is_top_level()) {
				emit("%^");
			}
			emit("%I""%p", s);
		} else {
			emit("%I", s);
		}
		break;
	case 'r':
		emit_str("_var", 4);
		break;
	case 'i':
		++indent_;
		break;
	case 'u':
		assert(indent_ > 0);
		--indent_;
		break;
	case 'b':
		if (column_ > right_margin) {
			putc('\n', out_);
			column_ = 0;
		} else {
			putc(' ', out_);
			column_++;
		}
		break;
	case 'I':
		emit_substr(s->string(), s->length());
		break;
	case 'R':
		if (prefix_ != nil) {
			emit("%I_ptr", s);
		} else {
			emit("%I""%p", s);
		}
		break;
	case 'Q':
		emit("%:%I", s);
		break;
	case 'E':
		e->generate(this);
		break;
	case ';':
		emit_expr_scope(e);
		break;
	case '-':
		b = concat_;
		concat_ = true;
		emit_expr_scope(e);
		concat_ = b;
		break;
	case 'X':
		emit_expr_scope(e);
		b = qualify_;
		qualify_ = false;
		e->generate(this);
		qualify_ = b;
		break;
	case 'Y':
		b = concat_;
		concat_ = true;
		sym = e->symbol();
		if (sym != nil && sym->is_builtin_type()) {
			fmt = "CORBA_%F";
		} else {
			fmt = "%^%F";
		}
		emit(fmt, nil, e);
		concat_ = b;
		break;
	case 'T':
		b = concat_;
		concat_ = true;
		sym = e->symbol();
		if (sym == SymbolTable::the()->void_type()) {
			fmt = "%F";
		} else if (sym != nil && sym->is_builtin_type()) {
			fmt = "CORBA_%F";
		} else {
			fmt = "%^%F";
		}
		emit(fmt, nil, e);
		concat_ = b;
		break;
	case 'N':
		e->generate_name(this);
		break;
	case 'F':
		emit_expr_scope(e);
		b = qualify_;
		qualify_ = false;
		e->generate_name(this);
		qualify_ = b;
		break;
	case 'A':
		emit_str("p_", 2);
		e->generate(this);
		break;
	case 'B':
		emit("service");
		break;
	case '*':
		if (!copy_outs_ && e->varying()) {
			sym = e->actual_type();
			if (sym != nil &&
			    sym != SymbolTable::the()->string_type() &&
			    sym->tag() != Symbol::sym_interface &&
			    sym->tag() != Symbol::sym_string) {
				emit_str("*", 1);
			}
		}
		break;
	case ',':
		if (has_env()) {
			emit(", ");
		}
		break;
	case 'e':
		if (has_env()) {
			emit_param_decls(nil, emit_env_formals);
		}
		break;
	case 'f':
		if (has_env()) {
			emit_param_decls(nil, emit_env_formals_body);
		}
		break;
	case 'a':
		if (has_env()) {
			emit_param_decls(nil, emit_env_actuals);
		}
		break;
	case '~':
		emit_scope(scope_);
		break;
	case 'm':
		emit_scope_only(scope_);
		break;
	case 'n':
		emit_name_only(scope_);
		break;
	case 'S':
		b = concat_;
		concat_ = true;
		emit("%~");
		concat_ = b;
		break;
	case ':':
		if (emit_scope(scope_)) {
			emit("%?");
		}
		break;
	case '_':
		b = concat_;
		concat_ = true;
		emit("%^%:");
		concat_ = b;
		break;
	case '?':
		if (concat_) {
			emit_str("_", 1);
		} else {
			emit_str("::", 2);
		}
		break;
	case '.':
		str = prefix();
		if (str != nil) {
			emit_substr(str->string(), str->length());
			emit_str("::", 2);
		}
		break;
	case '=':
		if (is_pure()) {
			emit(" = 0");
		}
		break;
	case '#':
		// Follow Sun coding style and don't indent CPP directives.
		putc('#', out_);
		column_ += 1;
		break;
	case '%':
		emit_str("%", 1);
		break;
	case '>':
		emit("invocation ");
		break;
	case '<':
		emit("service");
		break;
	case 'W':
		emit("_Mc_WMarshalStream ");
		break;
	case 'G':
		emit("_Mc_RMarshalStream ");
		break;
	case 'v':
		char buf[10];
		sprintf(buf, "_%d", e->version());
		emit(buf);
		break;
	default:
		emit_str("%", 1);
		putc(ch, out_);
		column_ += 1;
		break;
	}
}

void
Generator::emit_expr_scope(Expr *e)
{
	Symbol *sym = e->symbol();
	if (sym != nil && emit_scope(sym->scope())) {
		emit("%?");
	}
}

void
Generator::copy(const char *s)
{
	fputs(s, out_);
	column_ = 0;
}

void
Generator::emit_char(long c)
{
	putc(int(c), out_);
}

void
Generator::emit_chars_length(const char *s, long length)
{
	emit_substr(s, length);
}

void
Generator::emit_integer(long n)
{
	char buf[100];
	sprintf(buf, "%ld", n);
	emit_str(buf, strlen(buf));
}

void
Generator::emit_hexinteger(long n)
{
	char buf[100];
	sprintf(buf, "0x%lx", n);
	emit_str(buf, strlen(buf));
}

void
Generator::emit_float(double d)
{
	char buf[100];
	sprintf(buf, "%g", d);
	emit_str(buf, strlen(buf));
}

void
Generator::emit_declarator_ident(Identifier *i)
{
	if (i != nil && body_) {
		String *s = i->string();
		if (is_unused_param(s->string(), s->length())) {
			return;
		}
		emit("%I", s);
	}
}

void
Generator::emit_op(Opcode op)
{
	char single_char_op[2];
	const char *opname;
	switch (op) {
	case LSHIFT:
		opname = "<<";
		break;
	case RSHIFT:
		opname = ">>";
		break;
	default:
		if (op >= ' ' && op <= '~') {
			single_char_op[0] = char(op);
			single_char_op[1] = '\0';
			opname = single_char_op;
		} else {
			opname = "???";
		}
		break;
	}
	fputs(opname, out_);
	column_ += strlen(opname);
}

//
// Convert /A/B/.../C/D.idl to C_D_h
//

void
Generator::emit_filename(String *s)
{
	const char *start = s->string();
	String::Index n = s->length();
	const char *p;
	const char *end = start + n - 1;
	for (p = end; p > start; p--) {
		if (*p == '.') {
			end = p;
			break;
		}
	}
	long slash = 0;
	for (; p > start; p--) {
		if (*p == '/') {
			if (slash == 1) {
				++p;
				break;
			}
			++slash;
		}
	}
	emit_tr_filename(p, end);
	emit_tr_filename(inclext_, inclext_ + inclext_length_);
}

void
Generator::emit_tr_filename(const char *start, const char *end)
{
	for (const char *p = start; p < end; p++) {
		int ch = *p;
		if (!isalnum(ch)) {
			ch = '_';
		}
		putc(ch, out_);
	}
}

void
Generator::emit_ifndef(String *filename)
{
	emit("%#ifndef ");
	emit_filename(filename);
	emit("\n%#define\t");
	emit_filename(filename);
	emit("\n\n");
	files_->prepend(new CopyString(*filename));
	need_ifndef_ = false;
}

void
Generator::emit_endif(String *filename)
{
	if (files_->count() > 0) {
		String *current = files_->item(0);
		if (*current != *filename) {
			emit("%#endif // ");
			emit_filename(filename);
			emit("\n");
			delete current;
			files_->remove(0);
		}
	}
}

//
// Generate a global definition for cfront so that
// it generates unique per-file symbols.
//

void
Generator::emit_file_tag(const char *s)
{
	emit("_Ix_FileId(");
	emit(s);
	emit("%I)\n\n", symbol_id_start_->item(0));
}

void
Generator::emit_include(String *name)
{
	if (inclpath_ != nil) {
		include_newline_ = false;
		emit("%#include \"");
		const char *start = name->string();
		String::Index n = name->length();
		// assume n > 0 ==> end >= start
		const char *end = start + n - 1;
		const char *p;
		for (p = start; ; p++) {
			if (p > end) {
				for (; p > start; p--) {
					if (*p == '/') {
						start = p + 1;
						break;
					}
				}
				emit_str(inclpath_, inclpath_length_);
				break;
			}
			const char *s1, *s2;
			for (s1 = p, s2 = inclpath_; *s1 == *s2; s1++, s2++)
				;
			if (*s2 == '\0') {
				start = p;
				break;
			}
		}
		for (p = end; ; p--) {
			if (p == start) {
				emit_str(start, end - start);
				break;
			}
			if (*p == '.') {
				emit_substr(start, p - start);
				emit_str(inclext_, inclext_length_);
				break;
			}
		}
		emit("\"\n");
		include_newline_ = true;
	}
}

void
Generator::emit_includes()
{
	if (generate_include_) {
		StringList *list = includes_;
		if (list != nil && list->count() > 0) {
			emit_include_list(list);
		}
		generate_include_ = false;
	}
}

void
Generator::emit_stub_includes(String *s)
{
	emit_edit_warning(s);
	StringList *list = stub_includes_;
	if (list != nil && list->count() > 0) {
		emit_include_list(list);
	} else {
		emit_include_filename();
		const char *name = "Ox/stub";
		emit_include_substr(true, name, strlen(name));
		include_newline_ = false;
		emit("\n");
	}
}

void
Generator::emit_server_includes(String *s)
{
	emit_edit_warning(s);
	StringList *list = server_includes_;
	if (list != nil && list->count() > 0) {
		emit_include_list(list);
	} else {
		emit_include_filename();
		const char *name = "Ox/server";
		emit_include_substr(true, name, strlen(name));
		include_newline_ = false;
		emit("\n");
	}
}

void
Generator::emit_include_list(StringList *list)
{
	include_newline_ = false;
	for (ListItr(StringList) i(*list); i.more(); i.next()) {
		emit("%#include %I\n", i.cur());
	}
	include_newline_ = true;
}

void
Generator::emit_include_filename()
{
	if (filename_ != nil && filename_[0] != '\0') {
		const char *p;
		for (p = filename_; *p != '\0'; p++)
			;
		for (const char *q = p - 1; q > filename_; q--) {
			if (*q == '.') {
				p = q;
				break;
			}
		}
		emit_include_substr(false, filename_, p - filename_);
	}
}

void
Generator::emit_include_substr(bool path, const char *s, long length)
{
	include_newline_ = false;
	emit("%#include \"");
	if (path && inclpath_ != nil) {
		emit_str(inclpath_, inclpath_length_);
	}
	emit_substr(s, length);
	emit_str(inclext_, inclext_length_);
	emit("\"\n");
	include_newline_ = true;
}

void
Generator::emit_param_list(ExprList *params, ParamFlags flags)
{
	emit("(");
	emit_param_decls(params, flags);
	emit(")");
}

void
Generator::emit_param_decls(ExprList *params, ParamFlags flags)
{
	bool env = has_env() && (flags & emit_env) != 0;
	bool formals = (flags & emit_formals) != 0;

	if (params != nil) {
		bool b = formals_;
		formals_ = formals;
		ExprImpl::generate_list(params, &Expr::generate, this, ", ");
		formals_ = b;
	}
	if (env) {
		if (params != nil) {
			emit(", ");
		}
		emit_env_param(flags);
	}
}

void
Generator::emit_env_param(ParamFlags flags)
{
	if ((flags & emit_formals) != 0) {
		emit("Environment &");
	}
	if (!is_unused_param("_environment", 12)) {
		emit("_environment");
	}
}

void
Generator::unused_params(char *p)
{
	delete unused_params_;
	unused_params_ = (p == nil) ? nil : new CopyString(p);
}

bool
Generator::is_unused_param(const char *str, long length)
{
	if (unused_params_ != nil) {
		const char *p = unused_params_->string();
		const char *start;
		while (*p != '\0') {
			for (; *p != '\0' && !isalpha(*p) && *p != '_'; p++)
				;
			start = p;
			for (; isalnum(*p) || *p == '_'; p++)
				;
			if (p - start == length &&
			    strncmp(start, str, int(length)) == 0) {
				return (true);
			}
		}
	}
	return (false);
}

bool
Generator::emit_scope(Scope *s)
{
	bool b = false;
	if (s != nil) {
		if (s->outer != nil && s->outer->name != nil) {
			emit_scope(s->outer);
			emit("%?");
			b = true;
		}
		if (s->name != nil) {
			emit("%I", s->name);
			b = true;
		}
	}
	return (b);
}

//
// emit_scope_only - print the scope information without the name
//
bool
Generator::emit_scope_only(Scope *scopep)
{
	bool output = false;
	if (scopep != nil) {
		if (scopep->outer != nil && scopep->outer->name != nil) {
			emit_scope(scopep->outer);
			output = true;
		}
	}
	return (output);
}

//
// emit_name_only - print the name information without the scope
//
bool
Generator::emit_name_only(Scope *scopep)
{
	bool output = false;
	if (scopep != nil) {
		if (scopep->name != nil) {
			emit("%I", scopep->name);
			output = true;
		}
	}
	return (output);
}

void
Generator::emit_array_loop_start(ExprList *subscripts)
{
	long index = 0;
	for (ListItr(ExprList) e(*subscripts); e.more(); e.next()) {
		emit("for (int _i");
		emit_integer(index);
		emit(" = 0; _i");
		emit_integer(index);
		emit(" < %E; _i", nil, e.cur());
		emit_integer(index);
		emit("++) {\n%i");
		++index;
	}
}

void
Generator::emit_array_loop_indices(long nsubscripts)
{
	for (long index = 0; index < nsubscripts; index++) {
		emit("[_i");
		emit_integer(index);
		emit("]");
	}
}

void
Generator::emit_array_loop_finish(long nsubscripts)
{
	for (long index = 0; index < nsubscripts; index++) {
		emit("%u}\n");
	}
}

void
Generator::emit_put(Expr *type, const char *format, Expr *value)
{
	bool	b;
	Symbol	*symbolp = value->actual_type();
	switch (symbolp->tag()) {
	case Symbol::sym_array:
		b = array_decl(false);
		emit("_%Y_put(_b, &", nil, symbolp->array());
		emit(format, nil, value);
		emit(");\n");
		array_decl(b);
		return;
	case Symbol::sym_enum:
		emit("_b.put_long(");
		break;
	case Symbol::sym_interface:
		emit("_b.put_object(");
		if (symbolp->interface()->put_cast_up(this)) {
			emit("(%F""%p)", nil, symbolp->interface());
		}
		break;
	case Symbol::sym_sequence:
		if (symbolp->sequence_type()->type()->string_type()) {
			emit("CORBA::StringSeq::_put(_b, &");
		} else {
			emit("_%Y_put(_b, &", nil, symbolp->sequence_type());
		}
		break;
	case Symbol::sym_string:
		emit("_b.put_string(");
		break;
	case Symbol::sym_typedef:
		// builtin type
		emit("_b.put_%I(", symbolp->type_name()->str());
		break;
	case Symbol::sym_struct:
	case Symbol::sym_union:
		emit("_%Y_put(_b, &", nil, type);
		break;
	case Symbol::sym_unknown:
	case Symbol::sym_module:
	case Symbol::sym_parameter:
	case Symbol::sym_exception:
	case Symbol::sym_constant:
	case Symbol::sym_enum_value:
		assert(0);
		break;
	default:
		emit("_%Y_put(_b, ", nil, type);
		break;
	}
	emit(format, nil, value);
	emit(");\n");
}

void
Generator::emit_get(Expr *type, const char *format, Expr *value)
{
	bool	b;
	Symbol	*symbolp = value->actual_type();
	switch (symbolp->tag()) {
	case Symbol::sym_array:
		b = array_decl(false);
		emit("_%Y_get(_b, &", nil, symbolp->array());
		emit(format, nil, value);
		emit(");\n");
		array_decl(b);
		break;
	case Symbol::sym_enum:
		emit(format, nil, value);
		emit(" = %F(_b.get_long());\n", nil, type);
		break;
	case Symbol::sym_interface:
		emit(format, nil, value);
		b = interface_is_ref(false);
		emit(" = (%F""%p)_b.get_object(",
		    nil, type);
		emit("&_%Y_ArgMarshalFuncs);\n",
		    nil, symbolp->interface());
		interface_is_ref(b);
		break;
	case Symbol::sym_sequence: {
			SequenceDecl *seq = symbolp->sequence_type();
			if (seq->type()->string_type()) {
				emit("CORBA::StringSeq::_get(_b, &");
			} else {
				emit("_%Y_get(_b, &", nil, seq);
			}
			emit(format, nil, value);
			emit(");\n");
			break;
		}
	case Symbol::sym_string:
		emit(format, nil, value);
		emit(" = _b.get_string();\n");
		break;
	case Symbol::sym_typedef:
		// builtin type
		emit(format, nil, value);
		emit(" = _b.get_%I();\n", symbolp->type_name()->str());
		break;
	case Symbol::sym_struct:
	case Symbol::sym_union:
		emit("_%Y_get(_b, &", nil, type);
		emit(format, nil, value);
		emit(");\n");
		break;
	case Symbol::sym_unknown:
	case Symbol::sym_module:
	case Symbol::sym_parameter:
	case Symbol::sym_exception:
	case Symbol::sym_constant:
	case Symbol::sym_enum_value:
		assert(0);
		break;
	default:
		emit("_%Y_get(_b, ", nil, type);
		emit(format, nil, value);
		emit(");\n");
		break;
	}
}

//
// emit_release - generate a function call that releases the specified item.
// Note: items that do not vary do not require a "release" operation.
//
void
Generator::emit_release(Expr *type, const char *format, Expr *value)
{
	bool	b;
	Symbol	*symbolp = value->actual_type();
	switch (symbolp->tag()) {
	case Symbol::sym_array:
		if (symbolp->varying()) {
			//
			// Only arrays of variable size elements
			// require this operation.
			//
			b = array_decl(false);
			emit("_%Y_release(&", nil, symbolp->array());
			emit(format, nil, value);
			emit(");\n");
			array_decl(b);
		}
		break;

	case Symbol::sym_enum:
		break;

	case Symbol::sym_interface:
		emit(format, nil, value);
		b = interface_is_ref(false);
		emit(" = %F::_nil();\n", nil, type);
		interface_is_ref(b);
		break;

	case Symbol::sym_sequence: {
			SequenceDecl *seq = symbolp->sequence_type();
			if (seq->type()->string_type()) {
				emit("CORBA::StringSeq::_releasef(&");
			} else {
				emit("_%Y_release(&", nil, seq);
			}
			emit(format, nil, value);
			emit(");\n");
			break;
		}

	case Symbol::sym_string:
		// Fall thru
	case Symbol::sym_typedef:
		if (value->string_type()) {
			emit(format, nil, value);
			emit(" = (char *)NULL;\n");
		}
		break;

	case Symbol::sym_unknown:
	case Symbol::sym_module:
	case Symbol::sym_parameter:
	case Symbol::sym_exception:
	case Symbol::sym_constant:
	case Symbol::sym_enum_value:
		assert(0);
		break;

	default:
		if (symbolp->varying()) {
			emit("_%Y_release(&", nil, type);
			emit(format, nil, value);
			emit(");\n");
		}
		break;
	}
}

//
// emit_unput - generate a marshal rollback function for the specified item.
// Items that can not possibly contain an object reference
// do nothing for marshal rollback, and hence have no such function.
//
void
Generator::emit_unput(Expr *type, const char *format, Expr *value)
{
	if (!type->ExprValueAnyobjs()) {
		// This item can not possibly contain any object references.
		return;
	}
	// This item can contain object references

	Symbol	*symbolp = value->actual_type();
	switch (symbolp->tag()) {
	case Symbol::sym_array:
		{
			bool	array_dec = array_decl(false);
			emit("_%Y_unput(&", nil, symbolp->array());
			emit(format, nil, value);
			emit(");\n");
			array_decl(array_dec);
		}
		break;

	case Symbol::sym_interface:
		emit("service::unput_object(");
		if (symbolp->interface()->put_cast_up(this)) {
			emit("(%F""%p)", nil, symbolp->interface());
		}
		emit(format, nil, value);
		emit(");\n");
		break;

	case Symbol::sym_sequence:
		emit("_%Y_unput(&", nil, symbolp->sequence_type());
		emit(format, nil, value);
		emit(");\n");
		break;

	case Symbol::sym_union:
	case Symbol::sym_struct:
		emit("_%Y_unput(&", nil, type);
		emit(format, nil, value);
		emit(");\n");
		break;

	case Symbol::sym_union_member:
	case Symbol::sym_member:
	case Symbol::sym_enum:
	case Symbol::sym_string:
	case Symbol::sym_typedef:
	case Symbol::sym_unknown:
	case Symbol::sym_module:
	case Symbol::sym_operation:
	case Symbol::sym_parameter:
	case Symbol::sym_exception:
	case Symbol::sym_constant:
	case Symbol::sym_enum_value:
		assert(0);
		break;
	}
}

void
Generator::emit_edit_warning(String *stringp)
{
	emit("/* DO NOT EDIT -- Automatically generated");
	if (stringp != nil) {
		emit(" */\n/* from %I", stringp);
	}
	emit(" */\n\n");
}

void
Generator::flush()
{
	long n = files_->count();
	for (long i = 0; i < n; i++) {
		fprintf(out_, "\n#endif\n");
	}
	fflush(out_);
	if (stubfile_ != nil) {
		fclose(stubfile_);
	}
	if (serverfile_ != nil) {
		fclose(serverfile_);
	}
}

//
// scope_get_chars - recursively traverse the scoping levels.
// The resulting character string will begin with the outermost level
// and end with the innermost level.
// Returns whether a valid scope was found.
//
bool
Generator::scope_get_chars(char *separator_p, Scope *scope_p)
{
	bool found_scope = false;
	if (scope_p != nil) {
		if (scope_p->outer != nil &&
		    scope_p->outer->name != nil) {
			// Process enclosing scope first.
			(void) scope_get_chars(separator_p, scope_p->outer);

			// Append separator.
			type_stream << separator_p;

			found_scope = true;
		}
		if (scope_p->name != nil) {
			// Append name.
			type_stream << scope_p->name->string();

			found_scope = true;
		}
	}
	return (found_scope);
}

//
// scope_make_chars - produce a typeid string for an exception.
//
void
Generator::start_exception(char *separator_p, Expr *expr_p,
    Identifier *ident_p)
{
	type_stream.str("");
	String	*prefix_p = prefix();
	if (prefix_p != nil) {
		// Initialize the string with current prefix.
		type_stream << prefix_p->string();
	}

	// Recursively walk through the scoping levels.
	Symbol	*symbol_p = expr_p->symbol();
	if (symbol_p != nil &&
	    scope_get_chars(separator_p, symbol_p->scope())) {
		// Found nesting levels, append separator.
		type_stream << separator_p;
	}

	//
	// Now add the name of this entity.
	// This only works with those things having a valid
	// identification string.
	//
	type_stream
			<< (char *)ident_p->string()->string()
			<< "++Exception++";
}
