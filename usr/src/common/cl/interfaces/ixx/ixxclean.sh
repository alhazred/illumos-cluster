#!/bin/nawk -f
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 1997-2002 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)ixxclean.sh	1.9	08/05/20 SMI"

# This filter cleans up the output of ixx to make it cstyle-clean.
# It handles the prototypes and proxies in .h files.

# Process everything in an automatically generated file
/DO NOT EDIT -- Automatically generate/ {
	processfile = 1;
}

# Just print an unprocessed line
(processfile != 1) {
	print $0;
}

# Print blank lines
/^$/ {
	print $0;
}

# For a processed line, we first fix some problems and then wrap it to
# the line length
(processfile == 1) {
	# These specify the margin, indentation for first line and remainder
	# The first line can be "margin" characters and will be preceded by
	# the string "indent".  The remaining lines can be "margin_rem"
	# characters and will be preceded by the string "indent_rem".
	margin = 78
	indent = "";
	margin_rem = 74;
	indent_rem = "    ";

	#If we're processing the whole file, don't change indentations
	if (!processfile) {
		# If there's a curly brace on the end, split it off and print
		# it later.
		brace = 0;
		if ($0 ~ "{$") {
			brace = 1;
			sub(" *{","");
		}

		# If the line starts with 4 or 8 indentation, strip this off.
		# The first line will be indented with a tab and the remainder
		# tab+4
		if ($0 ~ "^    " || $0 ~ "^\t") {
			sub("^[ \t]*","");
			margin = 70;
			indent = "\t";
			margin_rem = 66;
			indent_rem = "\t    ";
		}
	}

	# Convert "char * foo" to "char *foo"
	gsub("char \\* ","char *");

	# Split the output line
	while (length($0)>margin) {

		lastchar = -1;

		# end is the potential last character of the line
		# We search backwards from end for a suitable breaking point.
		end = length($0);
		if (end>margin) {
			end = margin
		}

		# Find an instance of ", " to break at
		# 5 is an arbitrary stopping point
		for (i=end-1;i>5;i--) {
			if (substr($0,i,2) == ", ") {
				lastchar = i;
				break;
			}
		}
		# If no commas, then break at a space
		if (lastchar == -1) {
			for (i=end;i>5;i--) {
				if (substr($0,i,1) == " ") {
					lastchar = i-1;
					break;
				}
			}
		}
		if (lastchar == -1) {
			# Just print the line, since it can't be broken
			print indent $0;
			next
		}

		# lastchar is the last character we want to keep in the line.
		left = substr($0,1,lastchar);
		print indent left;
		$0 = substr($0,lastchar+2);

		# Use the margin and indentation specified for the remainder
		# of the line
		margin = margin_rem;
		indent = indent_rem;
	}

	# Print anything remaining
	if ($0 != "") {
		print indent $0;
	}

	# Print on its own line the brace we stripped off earlier
	if (brace==1) {
		print "{";
	}
}
