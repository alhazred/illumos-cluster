//
// Copyright 1998-2002 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

/*
 * Copyright (c) 1992-1993 Silicon Graphics, Inc.
 * Copyright (c) 1993 Fujitsu, Ltd.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Silicon Graphics and Fujitsu may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Silicon Graphics and Fujitsu.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

#pragma ident	"@(#)main.cc	1.19	02/05/22 SMI"

//
// Main program for IDL compiler
//

#include "err.h"
#include "expr.h"
#include "exprkit.h"
#include "scanner.h"
#include "list.h"
#include <stdio.h>
#include "SymbolTable.h"
#include "exprkit.h"

#ifdef WIN32
#include <io.h>
#define	access _access
#define	R_OK 4
#define	W_OK 2
#else
#include <unistd.h>
#endif	// WIN32

#if defined(AIXV3)
extern "C" int yyparse();
#else
extern int yyparse();
#endif

extern ExprList	*yyparse_root;

#if defined(YYDEBUG)
extern int yydebug;
#endif

extern bool	debug_size;

//
// type_debug - when true, this flag turns on a trace that outputs
//		each Typeid value to stderr.
//
//	This value can be set true by specifying 'T' as a parameter to
//	the command line "-debug" argument.
//
bool	type_debug = false;

declarePtrList(StringList, String)

class App {
public:
	App() {}

	int		main(int argc, char **argv);
private:
	unsigned long	curheapsize();
	void		init();
	void		get_args(int, char **);
	const char	*get_next_arg(long &i, int argc, char **argv);

	void		get_file_arg(const char	*&name, int mode, long &i,
			    int argc, char **argv);

	void		run();
	void		parse();
	void		resolve(Resolver *);
	void		rewrite(Generator *);
	void		generate(Generator *);
	void		finish();
	void		stage(const char *);

	void		missing(const char *arg);
	void		bad_arg(const char *arg);
	void		bad_access(const char *filename);

	ExprKit		*exprs_;
	Expr		*root_;
	ExprList	*defs_;
	ConfigInfo	gen_;
	ErrorHandler	*errors_;
	bool		verbose_;
	bool		scanning_only_;
	bool		resolving_;
	bool		generating_;
	bool		ucase_;
	unsigned long	heapstart_;
};

int
main(int argc, char **argv)
{
	App	a;

	return (a.main(argc, argv));
}

int
App::main(int argc, char **argv)
{
	init();
	get_args(argc, argv);
	if (errors_->count() == 0) {
		run();
	}
	finish();
	return (errors_->count());
}

void
App::init()
{
	defs_ = nil;
	gen_.filename = nil;
	gen_.stubfile = nil;
	gen_.serverfile = nil;
	gen_.schemafile = nil;
	gen_.tidfile = nil;
	gen_.inclpath = nil;
	gen_.inclext = ".h";
	gen_.includes = new StringList;
	gen_.stub_includes = new StringList;
	gen_.server_includes = new StringList;
	gen_.prefix = nil;
	gen_.pass_env = false;
	gen_.copy_outs = false;

	ErrorHandlerKit *errkit = new ErrorHandlerKit;
	errors_ = errkit->handler();
	delete errkit;

	verbose_ = false;
	scanning_only_ = false;
	resolving_ = true;
	generating_ = true;
	ucase_ = false;
	heapstart_ = 0;
	heapstart_ = curheapsize();
	String::case_sensitive(true);
	stage("init");

	// This contains all symbols which do not belong to any module
	add_module_name("");
}

//
// get_args - process the IDL compiler command line arguments
//
void
App::get_args(int argc, char **argv)
{
	String	arg;
	for (long i = 1; i < argc; i++) {
		arg = argv[i];

		if (arg == "-schema") {
			//
			// File name to output schema code into.
			// Default is no output.
			//
			get_file_arg(gen_.schemafile, 0, i, argc, argv);

		} else if (arg == "-path") {
			//
			// Prepend argument to #include filenames generated.
			// Note that no separator is added so '-path foo'
			// will generate "#include 'fooOx/stub.h"'
			// This option is overridden by -stubinclude and
			// -serverinclude.
			//
			gen_.inclpath = get_next_arg(i, argc, argv);

		} else if (arg == "-inclext") {
			gen_.inclext = get_next_arg(i, argc, argv);

		} else if (arg == "-i" || arg == "-include") {
			gen_.includes->append(new String(get_next_arg(i, argc,
			    argv)));

		} else if (arg == "-stubinclude") {
			//
			// Output #include arg to <stubfile> and <schemafile>
			// Note that no separator is added so
			// '-stubinclude foo'
			// will generate '#include foo'
			// Default is "Ox/stub.h"
			//
			gen_.stub_includes->append(
			    new String(get_next_arg(i, argc, argv)));

		} else if (arg == "-serverinclude") {
			//
			// Output #include arg to <serverfile>
			// Note that no separator is added so
			// '-serverinclude foo'
			// will generate '#include foo'
			// Default is "Ox/server.h"
			//
			gen_.server_includes->append(
			    new String(get_next_arg(i, argc, argv)));

		} else if (arg == "-stubfile") {
			//
			// File name to output stub (client) code into.
			// Default is no output.
			//
			get_file_arg(gen_.stubfile, 0, i, argc, argv);

		} else if (arg == "-tidfile") {
			//
			// File name to output type ID into.
			// Default is no output.
			// Note you must specify the -schemafile option.
			// This is useful for checking that previous
			// versions of interfaces haven't been changed
			// accidentally when making IDL changes.
			//
			get_file_arg(gen_.tidfile, 0, i, argc, argv);

		} else if (arg == "-serverfile") {
			//
			// File name to output skeleton (server) code into.
			// Default is no output.
			//
			get_file_arg(gen_.serverfile, 0, i, argc, argv);

		} else if (arg == "-f" || arg == "-file") {
			//
			// Specify the IDL input file name for error reporting.
			// Input will still be read from stdin.
			// Also, will output '#include "filename.h"'
			// if no -stubinclude or -serverinclude options
			// are specified.
			//
			gen_.filename = get_next_arg(i, argc, argv);

		} else if (arg == "-topfile") {
			// Override -file option.
			char	*s = new char[1];
			s[0] = '\0';
			gen_.filename = s;

		} else if (arg == "-p" || arg == "-prefix") {
			gen_.prefix = get_next_arg(i, argc, argv);

		} else if (arg == "-env") {
			// Add an Environment argument to all methods.
			gen_.pass_env = true;

		} else if (arg == "-copy_outs") {
			gen_.copy_outs = true;

		} else if (arg == "-cs") {
			//
			// Allow identifiers which only differ in
			// capitalization.  This is non CORBA.
			//
			ucase_ = true;

		} else if (arg == "-debug") {
			const char *debugflags = get_next_arg(i, argc, argv);
			for (const char *p = debugflags; *p != '\0'; p++) {
				switch (*p) {
#if defined(YYDEBUG)
				case 'y':
					yydebug = 1;
					break;
#endif
				case 's':
					// Just scan the input file.
					scanning_only_ = true;
					break;
				case 'v':
					verbose_ = true;
					break;
				case 'p':
					resolving_ = false;
					generating_ = false;
					break;
				case 'r':
					resolving_ = true;
					generating_ = false;
					break;
				case 'T':
					// Output information about
					// generation of type id's
					//
					type_debug = true;
					break;
				case 'c':
					resolving_ = true;
					generating_ = true;
				default:
					errors_->begin_unrecoverable();
					errors_->put_chars("Unknown debug"
					    " flag '");
					errors_->put_chars(p);
					errors_->put_chars("'");
					errors_->end();
				}
			}
		} else {
			bad_arg(argv[i]);
		}
	}
}

const char *
App::get_next_arg(long &i, int argc, char **argv)
{
	long next_i = i + 1;
	if (next_i >= argc) {
		missing(argv[i]);
		return (nil);
	}
	i = next_i;
	return (argv[i]);
}

void
App::get_file_arg(const char *&name, int mode, long &i, int argc, char **argv)
{
	if (name != nil) {
		errors_->begin_unrecoverable();
		errors_->put_chars("Only one '");
		errors_->put_chars(argv[i]);
		errors_->put_chars("' allowed");
		errors_->end();
	} else {
		name = get_next_arg(i, argc, argv);
		if (mode != 0 && name != nil && access(name, mode) < 0) {
			bad_access(name);
		}
	}
}

void
App::run()
{
	ScannerKit	*scanners = new ScannerKit;
	if (scanning_only_) {
		Scanner	*s = scanners->make_scanner(nil, errors_, ucase_);
		for (TokenType t = s->get_token(); t != 0; t = s->get_token()) {
			s->print_token(t);
			printf("\n");
		}
	} else {
		exprs_ = new ExprKit(errors_);

		// The one and only SymbolTable is accessible through a static
		// method on the class.
		new SymbolTable;

		Resolver	*r = exprs_->resolver(gen_);
		Generator	*g = exprs_->generator(gen_);
		scanners->make_scanner(nil, errors_, ucase_);
		parse();
		resolve(r);

		if (debug_size) {
			//
			// Print out the contents of the symbol table
			// with size information.
			//
			print_module_list();
		}

		rewrite(g);
		generate(g);
		delete exprs_;
	}
	delete scanners;
}

void
App::parse()
{
	stage("parsing");
	yyparse();
	if (errors_->count() == 0) {
		if (defs_ == nil) {
			defs_ = yyparse_root;
		} else {
			for (ListItr(ExprList) i(*yyparse_root);
			    i.more();
			    i.next()) {
				defs_->append(i.cur());
			}
			delete yyparse_root;
		}
	}
}

void
App::resolve(Resolver *r)
{
	if (errors_->count() == 0 && resolving_) {
		stage("resolving");
		root_ = exprs_->root(defs_);
		root_->resolve(r);
	}
}

//
// Convert tree from single Expr for all versions of an interface to
// separate Exprs for each version.
//
void
App::rewrite(Generator *g)
{
	if (errors_->count() == 0 && generating_) {
		stage("rewriting");
		root_->rewrite(g, nil);
	}
}

void
App::generate(Generator *g)
{
	if (errors_->count() == 0 && generating_) {
		stage("generating");
		root_->generate(g);
	}
}

void
App::finish()
{
	stage("finish");
}

#if !defined(AIXV3)
extern "C" {
	void	*sbrk(int);
}
#endif

unsigned long
App::curheapsize()
{
#ifdef WIN32
	// don't know what to use on Win32
	return (0);
#else
	return ((((unsigned long)sbrk(0) + 1023) >> 10) - heapstart_);
#endif
}

void
App::stage(const char *s)
{
	if (verbose_) {
		errors_->begin_comment();
		errors_->put_chars(s);
		errors_->put_chars("(");
		errors_->put_integer(curheapsize());
		errors_->put_chars("k)");
		errors_->end();
	}
}

void
App::bad_arg(const char *arg)
{
	errors_->begin_unrecoverable();
	errors_->put_chars("Unexpected argument ");
	errors_->put_chars(arg);
	errors_->end();
}

void
App::missing(const char *arg)
{
	errors_->begin_unrecoverable();
	errors_->put_chars("Expected argument after ");
	errors_->put_chars(arg);
	errors_->end();
}

void
App::bad_access(const char *filename)
{
	errors_->begin_unrecoverable();
	errors_->put_chars("Can't access \"");
	errors_->put_chars(filename);
	errors_->put_chars("\"");
	errors_->end();
}
