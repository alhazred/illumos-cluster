//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

/*
 * Copyright (c) 1992-1993 Silicon Graphics, Inc.
 * Copyright (c) 1993 Fujitsu, Ltd.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Silicon Graphics and Fujitsu may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Silicon Graphics and Fujitsu.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

#pragma ident	"@(#)StructMember.cc	1.8	06/05/24 SMI"

#include "generator.h"
#include "StructMember.h"
#include "Symbol.h"
#include "Resolver.h"

//
// Class StructMember Methods
//

StructMember::StructMember(SourcePosition *p, Expr *type, ExprList *decls) :
	ExprImpl(p),
	type_(type),
	declarators_(decls)
{
}

StructMember::~StructMember()
{
}

bool StructMember::compute_varying()
{
	return (type_->varying());
}

void
StructMember::resolve(Resolver *r)
{
	type_->resolve(r);
	Symbol *s = new Symbol(r->scope());
	s->struct_member(this);
	symbol_ = s;
	r->push_context(s, type_);
	resolve_list(declarators_, r);
	r->pop_context();
	s->hash()->add_list(declarators_);
}

bool
StructMember::generate(Generator *g)
{
	if (!put_member_type(g)) {
		g->emit(g->aggr_decl(type_) ? "%E;\n%N " : "%E ", nil, type_);
	}
	generate_list(declarators_, &Expr::generate, g, ", ");
	return (true);
}

//
// put_member_type - outputs the member type if it is a string or
// or class (or interface).
// Returns true when this method outputs type information.
//
bool
StructMember::put_member_type(Generator *g)
{
	bool output_type = false;

	if (type_->string_type()) {
		g->emit("CORBA::String_field ");
		output_type = true;
	} else if (type_->interface_type()) {
		bool ref = g->interface_is_ref(false);
		g->emit("%F_field ", nil, type_);
		g->interface_is_ref(ref);
		output_type = true;
	}
	return (output_type);
}

bool
StructMember::generate_extern_stubs(Generator *g)
{
	return (type_->generate_extern_stubs(g) |
	    generate_list(declarators_, &Expr::generate_extern_stubs, g));
}

bool
StructMember::generate_stub(Generator *g)
{
	return (type_->generate_stub(g) |
	    generate_list(declarators_, &Expr::generate_stub, g));
}

bool
StructMember::generate_marshal(Generator *g)
{
	for (ListItr(ExprList) i(*declarators_); i.more(); i.next()) {
		g->emit_put(type_, "structp->%E", i.cur());
	}
	return (true);
}

bool
StructMember::generate_unmarshal(Generator *g)
{
	for (ListItr(ExprList) i(*declarators_); i.more(); i.next()) {
		g->emit_get(type_, "structp->%E", i.cur());
	}
	return (true);
}

bool
StructMember::generate_release(Generator *g)
{
	for (ListItr(ExprList) i(*declarators_); i.more(); i.next()) {
		g->emit_release(type_, "structp->%E", i.cur());
	}
	return (true);
}

bool
StructMember::generate_marshal_rollback(Generator *g)
{
	for (ListItr(ExprList) i(*declarators_); i.more(); i.next()) {
		g->emit_unput(type_, "structp->%E", i.cur());
	}
	return (true);
}

bool
StructMember::generate_type_name(Generator *g)
{
	return (generate_list(declarators_, &Expr::generate_type_name, g));
}
