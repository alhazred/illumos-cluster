/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _UNIONDECL_H
#define	_UNIONDECL_H

#pragma ident	"@(#)UnionDecl.h	1.6	06/05/24 SMI"

/*
 * Copyright (c) 1992-1993 Silicon Graphics, Inc.
 * Copyright (c) 1993 Fujitsu, Ltd.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Silicon Graphics and Fujitsu may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Silicon Graphics and Fujitsu.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */


#include "expr-impl.h"

class CaseTable;
class CaseList;

//
// This file supports unions
//
class UnionDecl : public ExprImpl {
public:
	UnionDecl(SourcePosition *, Identifier *, Expr *, CaseList *);
	~UnionDecl();

	void	resolve(Resolver *);
	bool	generate(Generator *);

	bool generate_name(Generator *);
	bool generate_type_name(Generator *);
	bool generate_extern_stubs(Generator *);
	bool generate_stub(Generator *);

	Symbol		*switch_type()	{ return switch_type_; }
	CaseTable	*cases()	{ return case_table_; }
	CaseList	*caselist()	{ return cases_; }
	bool		default_label() { return default_label_ != nil; }
	void		default_label(Expr* e) { default_label_ = e; }
	Identifier	*ident()	{ return ident_; }

protected:
	bool compute_varying();

	Symbol	*switch_type(Resolver *);
	void	check_cases(Resolver *);
	void	generate_access_hdr(Generator *, CaseElement *);
	void	generate_access_impl(Generator *, CaseElement *);
	void	generate_set_tag(Generator *, CaseElement *);
	void	generate_free(Generator *, CaseElement *);
	void	generate_assign(Generator *, CaseElement *);

	Identifier	*ident_;		// Name of union
	Expr		*type_;			// Type of switch discriminator
	CaseList	*cases_;		// parsed case list
	Scope		*block_;		// Scope including union/switch
	Symbol		*switch_type_;
	CaseTable	*case_table_;
	Expr		*default_label_;
};

#endif	/* _UNIONDECL_H */
