//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

/*
 * Copyright (c) 1992-1993 Silicon Graphics, Inc.
 * Copyright (c) 1993 Fujitsu, Ltd.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Silicon Graphics and Fujitsu may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Silicon Graphics and Fujitsu.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

#pragma ident	"@(#)Symbol.cc	1.12	06/05/24 SMI"

#include "Symbol.h"
#include "generator.h"
#include "Parameter.h"
#include "InterfaceDef.h"
#include "Module.h"
#include "StructDecl.h"
#include "StructMember.h"
#include "UnionDecl.h"
#include "UnionMember.h"
#include "Declarator.h"

//
// class Symbol methods
//

Symbol::Symbol(Scope *s)
{
	tag_ = sym_unknown;
	scope_ = s;
	declared_ = false;
	declared_stub_ = 0;
	hash_.init();
	value_.interface_ = nil;
}

Symbol::Symbol(const Symbol &s)
{
	tag_ = s.tag_;
	scope_ = s.scope_;
	declared_ = false;
	value_.interface_ = s.value_.interface_;
}

Symbol::~Symbol()
{
}

void
Symbol::scope(Scope *s)
{
	scope_ = s;
}

Scope *
Symbol::inner_scope()
{
	switch (tag_) {
	case sym_module:
		return (value_.module_->block());
	case sym_interface:
		return (value_.interface_->block());
	}
	return (nil);
}

Symbol *
Symbol::actual_type()
{
	Symbol *s = this;
	for (;;) {
		Expr *t;
		switch (s->tag_) {
		case Symbol::sym_typedef:
			t = s->type_name()->type();
			break;
		case Symbol::sym_member:
			t = s->struct_member()->type();
			break;
		case Symbol::sym_union_member:
			t = s->union_member()->type();
			break;
		case Symbol::sym_parameter:
			t = s->parameter()->type();
			break;
		default:
			t = nil;
			break;
		}
		if (t == nil) {
			break;
		}
		Symbol *ns = t->symbol();
		if (ns == nil) {
			break;
		}
		s = ns;
	}
	return (s);
}

//
// type_desc - return the argument type for this symbol
//
invo_args::arg_type
Symbol::type_desc()
{
	invo_args::arg_type	argtype;

	switch (tag()) {
	case Symbol::sym_enum:
		argtype = invo_args::t_long;
		break;
	case Symbol::sym_string:
		argtype = invo_args::t_string;
		break;
	case Symbol::sym_typedef:
		argtype = type_name()->kind();
		break;
	case Symbol::sym_interface:
		argtype = interface()->kind();
		break;
	default:
		argtype = invo_args::t_addr;
		break;
	}
	return (argtype);
}

void
Symbol::copy_value(Symbol* s)
{
	if (s != nil) {
		tag_ = s->tag_;
		scope_ = s->scope_;
		value_ = s->value_;
	}
}

void
Symbol::module(Module* m)
{
	tag_ = sym_module;
	value_.module_ = m;
}

Module *
Symbol::module()
{
	return (tag_ == sym_module ? value_.module_ : nil);
}

void
Symbol::interface(InterfaceDef* i)
{
	tag_ = sym_interface;
	value_.interface_ = i;
}

InterfaceDef*
Symbol::interface()
{
	return (tag_ == sym_interface ? value_.interface_ : nil);
}

void
Symbol::type_name(TypeName* t)
{
	tag_ = sym_typedef;
	value_.type_ = t;
}

TypeName *
Symbol::type_name()
{
	return (tag_ == sym_typedef ? value_.type_ : nil);
}

void
Symbol::constant(Constant* c)
{
	tag_ = sym_constant;
	value_.constant_ = c;
}

Constant *
Symbol::constant()
{
	return (tag_ == sym_constant ? value_.constant_ : nil);
}

void
Symbol::operation(Operation* o)
{
	tag_ = sym_operation;
	value_.operation_ = o;
}

Operation *
Symbol::operation()
{
	return (tag_ == sym_operation ? value_.operation_ : nil);
}

void
Symbol::parameter(Parameter* p)
{
	tag_ = sym_parameter;
	value_.parameter_ = p;
}

Parameter *
Symbol::parameter()
{
	return (tag_ == sym_parameter ? value_.parameter_ : nil);
}

void
Symbol::array(Declarator* d)
{
	tag_ = sym_array;
	value_.array_ = d;
}

Declarator *
Symbol::array()
{
	return (tag_ == sym_array ? value_.array_ : nil);
}

void
Symbol::struct_tag(StructDecl* s)
{
	tag_ = sym_struct;
	value_.struct_tag_ = s;
}

StructDecl *
Symbol::struct_tag()
{
	return (tag_ == sym_struct ? value_.struct_tag_ : nil);
}

void
Symbol::struct_member(StructMember* m)
{
	tag_ = sym_member;
	value_.struct_member_ = m;
}

StructMember *
Symbol::struct_member()
{
	return (tag_ == sym_member ? value_.struct_member_ : nil);
}

void
Symbol::union_tag(UnionDecl* u)
{
	tag_ = sym_union;
	value_.union_tag_ = u;
}

UnionDecl *
Symbol::union_tag()
{
	return (tag_ == sym_union ? value_.union_tag_ : nil);
}

void
Symbol::union_member(UnionMember* m)
{
	tag_ = sym_union_member;
	value_.union_member_ = m;
}

UnionMember *
Symbol::union_member()
{
	return (tag_ == sym_union_member ? value_.union_member_ : nil);
}

void
Symbol::enum_tag(EnumDecl* e)
{
	tag_ = sym_enum;
	value_.enum_tag_ = e;
}

EnumDecl *
Symbol::enum_tag()
{
	return (tag_ == sym_enum ? value_.enum_tag_ : nil);
}

void
Symbol::enum_value_tag(Enumerator* e)
{
	tag_ = sym_enum_value;
	value_.enum_value_tag_ = e;
}

Enumerator *
Symbol::enum_value_tag()
{
	return (tag_ == sym_enum_value ? value_.enum_value_tag_ : nil);
}

void
Symbol::except_type(ExceptDecl* e)
{
	tag_ = sym_exception;
	value_.except_type_ = e;
}

ExceptDecl *
Symbol::except_type()
{
	return (tag_ == sym_exception ? value_.except_type_ : nil);
}

void
Symbol::sequence_type(SequenceDecl* s)
{
	tag_ = sym_sequence;
	value_.sequence_type_ = s;
}

SequenceDecl *
Symbol::sequence_type()
{
	return (tag_ == sym_sequence ? value_.sequence_type_ : nil);
}

void
Symbol::string_type(StringDecl *s)
{
	tag_ = sym_string;
	value_.string_type_ = s;
}

StringDecl *
Symbol::string_type()
{
	return (tag_ == sym_string ? value_.string_type_ : nil);
}

Expr *
Symbol::expr_type()
{
	return (tag_ != sym_unknown ? value_.any_type_ : nil);
}

//
// We want to know whether an aggregate type contains
// pointers that might need freeing.
//

bool
Symbol::varying()
{
	Expr *e = nil;
	switch (tag_) {
	case Symbol::sym_typedef:
		e = value_.type_;
		break;
	case Symbol::sym_struct:
		e = value_.struct_tag_;
		break;
	case Symbol::sym_union:
		e = value_.union_tag_;
		break;
	case Symbol::sym_array:
		e = value_.array_->element_type();
		break;
	case Symbol::sym_sequence:
	case Symbol::sym_string:
	case Symbol::sym_interface:
		return (true);
	}
	return (e != nil && e->varying());
}
