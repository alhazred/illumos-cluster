//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

/*
 * Copyright (c) 1992-1993 Silicon Graphics, Inc.
 * Copyright (c) 1993 Fujitsu, Ltd.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Silicon Graphics and Fujitsu may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Silicon Graphics and Fujitsu.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

#pragma ident	"@(#)SymbolTable.cc	1.5	06/05/24 SMI"

#include "SymbolTable.h"
#include "expr-impl.h"
#include "table2.h"
#include "Symbol.h"

inline unsigned long
key_to_hash(String& s)
{
	return (s.hash());
}

declareTable2(SymbolMap, Scope*, String, Symbol*)
implementTable2(SymbolMap, Scope*, String, Symbol*)

//
// class SymbolTable methods
//

SymbolTable	*SymbolTable::the_symbol_tablep = nil;

SymbolTable::SymbolTable()
{
	map_ = new SymbolMap(500);
	scope_ = nil;
	leave_scope();

	// Only allow one active symbol table in the system
	if (SymbolTable::the_symbol_tablep != nil) {
		delete SymbolTable::the_symbol_tablep;
	}

	// Make the symbol table globally available
	the_symbol_tablep = this;
}

SymbolTable::~SymbolTable()
{
	for (Table2Iterator(SymbolMap) i(*map_); i.more(); i.next()) {
		Symbol* s = i.cur_value();
		delete s;
	}
	delete map_;
}

Scope *
SymbolTable::enter_scope(String* name)
{
	Scope* s = new Scope;
	s->name = name;
	s->outer = scope_;
	s->id = 0;
	s->except_index = 0;
	scope_ = s;
	return (s);
}

Scope *
SymbolTable::scope()
{
	return (scope_);
}

void
SymbolTable::leave_scope()
{
	Scope* s = scope_;
	if (s != nil) {
		scope_ = s->outer;
	}
}

void
SymbolTable::bind(String* str, Symbol* sym)
{
	bind_in_scope(scope_, str, sym);
}

void
SymbolTable::bind_in_scope(Scope* s, String* str, Symbol* sym)
{
	sym->scope(s);
	map_->insert(s, *str, sym);
}

Symbol *
SymbolTable::resolve(String* str)
{
	return (resolve_in_scope(scope_, str));
}

Symbol *
SymbolTable::resolve_in_scope(Scope* s, String* str)
{
	Symbol* sym;
	for (Scope* b = s; b != nil; b = b->outer) {
		if (map_->find(sym, b, *str)) {
			return (sym);
		}
	}
	return (nil);
}
