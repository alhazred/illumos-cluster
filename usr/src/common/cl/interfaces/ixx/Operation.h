/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _OPERATION_H
#define	_OPERATION_H

#pragma ident	"@(#)Operation.h	1.14	06/05/24 SMI"

/*
 * Copyright (c) 1992-1993 Silicon Graphics, Inc.
 * Copyright (c) 1993 Fujitsu, Ltd.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Silicon Graphics and Fujitsu may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Silicon Graphics and Fujitsu.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */


#include <orb/invo/invo_args.h>
#include "expr.h"
#include "list.h"

//
// Operation - specifies information about a method (or operation)
// on an interface (or object);
//
class Operation : public ExprImpl {
public:
	Operation(SourcePosition *p, Identifier *ident, long version,
	    Expr *type, ExprList *params, ExprList *exceptions,
	    long attributes, ExprList *context, long pool);
	~Operation();

	void	resolve(Resolver *);
	bool	generate(Generator *);

	Identifier	*ident()	{ return ident_; }
	long		index()		{ return index_; }

	bool		generate_name(Generator *);
	bool		generate_stub(Generator *);
	bool		generate_server(Generator *);
	bool		generate_receive_table(Generator *);
	bool		generate_type_name(Generator *);

	void		set_index(long n);
	void		emit_stub_hdr(Generator *);
	void		emit_wrapper_stub(Generator *);
	void		emit_versionex_stub(Generator *);

protected:
	void		check_oneway(Resolver *);
	bool		has_marshal_funcs(Generator *);
	void		generate_param_marshal(Generator *);
	void		generate_param_value(Generator *);

	Identifier	*ident_;	// method name
	long		index_;		// method number (index)
	Expr		*type_;		// method return type
	ExprList	*params_;	// method parameter list
	ExprList	*exceptions_;	// method can raise these exceptions
	ExprList	*context_;	//
	InterfaceDef	*interface_;	//
	Scope		*block_;	//
	long		pool_;		// method can use special resource pool
	long		attributes_;	// invocation semantics combined
	bool		oneway_;	//	oneway
	bool		nonblocking_;	//	nonblocking
	bool		unreliable_;	//	unreliable

	// Uncomment if checkpoint is needed
	// bool		checkpoint_;

private:
	static void	generate_marshal_func(Generator *, Expr *,
			    invo_args::arg_mode);

	static void	generate_arg(Generator *, long, const char *);

	static bool	has_marshal(Generator *, Expr *);

	static void	generate_result_value(Generator *, Expr *);

	static void	generate_receive_asg(Generator *, Expr *type);

	void		determine_argument_marshal_order(Generator *);

	void		generate_arg_desc_arg_sizes(Generator *, long nparams,
			    invo_args::arg_type	return_argtype);

	void		generate_ArgMarshal(Generator *, long nparams);

	void		generate_arg_info(Generator *);

	void		generate_stub_code(Generator *, long nparams,
			    invo_args::arg_type	return_argtype);

	// Generate debug code to check for illegal exceptions in stub
	void		operation_exception_validation(Generator *);

	void		generate_param_desc(Generator *, long nparams,
			    arg_sizes &argsizes);

	void		generate_receive_addresses(Generator *);

	void		check_marshal_size_attribute(Resolver *resolverp);
};

#endif	/* _OPERATION_H */
