/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _RESOLVER_H
#define	_RESOLVER_H

#pragma ident	"@(#)Resolver.h	1.8	06/05/24 SMI"

/*
 * Copyright (c) 1992-1993 Silicon Graphics, Inc.
 * Copyright (c) 1993 Fujitsu, Ltd.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Silicon Graphics and Fujitsu may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Silicon Graphics and Fujitsu.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */


#include "list.h"
#include "table.h"
#include "expr.h"
#include "SymbolTable.h"

//
// Resolve names and do type-checking
//

struct Context {
    Symbol* in_symbol;
    Symbol* out_symbol;
    Expr* type_expr;
};

declareList(ContextList, Context)

declareTable(CaseTable, long, long)

class Resolver {
public:
	Resolver(ExprKit *, ErrorHandler *);
	virtual ~Resolver();

	ExprKit		*expr_kit();
	Scope		*enter_scope(Identifier *);
	Scope		*scope();
	void		leave_scope();
	void		push_context(Context *);
	void		push_context(Symbol *, Expr * = nil);
	void		push_context();
	Context		*context();
	void		symbol(Symbol *);
	void		pop_context();
	void		bind(Identifier *, Symbol *);
	Symbol		*resolve(Identifier *);
	Symbol		*new_symbol(Identifier *);
	ErrorHandler	*handler();
	void		report_undefined(bool);
	void		undefined(Expr *, String *);
	void		undefined_scope(Expr *, Symbol *);
	void		redefined(Identifier *);
	void		symerr(Expr *, String *, const char *message);
	void		error(Expr *, const char *message);
	void		warning(Expr *, const char *message);
private:
	void		init_types();

	Symbol		*builtin_type(const char *name, const char *mapping,
			    invo_args::arg_type kind, bool enter = true);

	ExprKit		*exprs_;
	ErrorHandler	*handler_;
	bool		report_undefined_;
	ContextList	*contexts_;

	Symbol		*object_;
	Symbol		*true_;
	Symbol		*false_;
};

inline ExprKit *
Resolver::expr_kit()
{
	return (exprs_);
}

inline void
Resolver::report_undefined(bool b)
{
	report_undefined_ = b;
}

inline Scope *
Resolver::enter_scope(Identifier *ident)
{
	return (SymbolTable::the()->enter_scope(ident->string()));
}

inline Scope *
Resolver::scope()
{
	return (SymbolTable::the()->scope());
}

inline void
Resolver::leave_scope()
{
	SymbolTable::the()->leave_scope();
}

#endif	/* _RESOLVER_H */
