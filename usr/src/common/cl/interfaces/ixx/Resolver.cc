//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

/*
 * Copyright (c) 1992-1993 Silicon Graphics, Inc.
 * Copyright (c) 1993 Fujitsu, Ltd.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Silicon Graphics and Fujitsu may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Silicon Graphics and Fujitsu.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

#pragma ident	"@(#)Resolver.cc	1.30	06/05/24 SMI"

//
// Resolve names and do type-checking
//

#include "err.h"
#include "expr-impl.h"
#include "Resolver.h"
#include <stdlib.h>
#include <stdio.h>
#include <orb/invo/invo_args.h>
#include "Symbol.h"
#include "Operation.h"
#include "InterfaceDef.h"
#include "Module.h"
#include "SequenceDecl.h"
#include "UnionDecl.h"
#include "UnionMember.h"
#include "Declarator.h"

implementList(ContextList, Context)

implementTable(CaseTable, long, long)

// class Resolver

Resolver::Resolver(ExprKit *exprs, ErrorHandler *handler) :
    exprs_(exprs), handler_(handler), report_undefined_(true)
{
	contexts_ = new ContextList(3);
	SymbolTable::the()->enter_scope(nil);
	init_types();
}

Resolver::~Resolver()
{
	handler_->destroy();
	delete contexts_;
}

void
Resolver::init_types()
{
	SymbolTable *symboltablep = SymbolTable::the();
	String *obj = new String("Object");
	ExprList *defs = exprs_->exprlist();
	Expr *i = exprs_->forward_interface(exprs_->ident(obj));
	defs->append(i);
	Expr *m = exprs_->module(exprs_->ident(new String("CORBA")), defs);
	m->resolve(this);

	//
	// Note: if any new builtin types are added which are not fixed
	// size, update the code in TypeName::compute_varying.
	//
	symboltablep->void_ = builtin_type("void", "void", invo_args::t_void);
	symboltablep->oneway_ = builtin_type("oneway", "void",
	    invo_args::t_void, false);
	symboltablep->boolean_ = builtin_type("boolean", "bool",
	    invo_args::t_boolean);
	symboltablep->char_ = builtin_type("char", "int8_t", invo_args::t_char);
	symboltablep->octet_ = builtin_type("octet", "uint8_t",
	    invo_args::t_octet);
	symboltablep->short_ = builtin_type("short", "int16_t",
	    invo_args::t_short);
	symboltablep->ushort_ = builtin_type("unsigned_short", "uint16_t",
	    invo_args::t_unsigned_short, false);
	symboltablep->long_ =
	    builtin_type("long", "int32_t", invo_args::t_long);
	symboltablep->ulong_ = builtin_type("unsigned_long", "uint32_t",
	    invo_args::t_unsigned_long, false);
	symboltablep->longlong_ = builtin_type("longlong", "int64_t",
	    invo_args::t_longlong);
	symboltablep->ulonglong_ = builtin_type("unsigned_longlong",
	    "uint64_t", invo_args::t_unsigned_longlong, false);
#ifdef ORBFLOAT
	symboltablep->float_ = builtin_type("float", "float",
	    invo_args::t_float);
	symboltablep->double_ = builtin_type("double", "double",
	    invo_args::t_double);
#endif
	symboltablep->string_ = builtin_type("string", "String",
	    invo_args::t_string);

	true_ = builtin_type("TRUE", "TRUE", invo_args::t_boolean);
	false_ = builtin_type("FALSE", "FALSE", invo_args::t_boolean);
}

void
Resolver::push_context(Context *c)
{
	contexts_->prepend(*c);
}

void
Resolver::push_context(Symbol *s, Expr *t)
{
	Context c;
	c.in_symbol = s;
	c.out_symbol = nil;
	c.type_expr = t;
	contexts_->prepend(c);
}

void
Resolver::push_context()
{
	Context c;
	c.in_symbol = nil;
	c.out_symbol = nil;
	c.type_expr = nil;
	contexts_->prepend(c);
}

Context *
Resolver::context()
{
	Context *c;
	if (contexts_->count() == 0) {
		c = nil;
	} else {
		c = &contexts_->item_ref(0);
	}
	return (c);
}

void
Resolver::symbol(Symbol *s)
{
	if (contexts_->count() != 0) {
		contexts_->item_ref(0).out_symbol = s;
	}
}

void
Resolver::pop_context()
{
	ContextList *list = contexts_;
	if (list->count() != 0) {
		Symbol *s = list->item_ref(0).out_symbol;
		list->remove(0);
		if (list->count() != 0) {
			list->item_ref(0).out_symbol = s;
		}
	}
}

void
Resolver::bind(Identifier *ident, Symbol *s)
{
	SymbolTable::the()->bind(ident->string(), s);
}

Symbol *
Resolver::resolve(Identifier *ident)
{
	return (SymbolTable::the()->resolve(ident->string()));
}

Symbol *
Resolver::new_symbol(Identifier *ident)
{
	Scope *block = scope();
	Symbol *s = resolve(ident);
	if (s == nil || s->scope() != block) {
		s = new Symbol(block);
		bind(ident, s);
	} else {
		redefined(ident);
	}
	return (s);
}

ErrorHandler *
Resolver::handler()
{
	return (handler_);
}

void
Resolver::undefined(Expr *e, String *s)
{
	if (report_undefined_) {
		symerr(e, s, "undefined");
	}
}

void
Resolver::undefined_scope(Expr *e, Symbol *)
{
	if (report_undefined_) {
		error(e, "Accessor is not a scope");
	}
}

void
Resolver::redefined(Identifier *i)
{
	symerr(i, i->string(), "redefined");
}

void
Resolver::symerr(Expr *e, String *s, const char *message)
{
	ErrorHandler *err = handler_;
	e->set_source_position(err);
	err->begin_error();
	err->put_chars("Symbol \"");
	err->put_string(*s);
	err->put_chars("\" ");
	err->put_chars(message);
	err->end();
}

void
Resolver::error(Expr *e, const char *message)
{
	ErrorHandler *err = handler_;
	e->set_source_position(err);
	err->error(message);
}

void
Resolver::warning(Expr *e, const char *message)
{
	ErrorHandler *err = handler_;
	e->set_source_position(err);
	err->warning(message);
}

Symbol *
Resolver::builtin_type(const char *name, const char *mapping,
    invo_args::arg_type kind, bool enter)
{
	String *u = new String(name);
	TypeName *t = new TypeName(handler_->position(), u,
	    new String(mapping), kind);
	Symbol *s = new Symbol(scope());
	s->type_name(t);
	s->hash()->copy_string(u);
	if (enter) {
		SymbolTable::the()->bind(u, s);
	}
	return (s);
}

//
// Create resolver.
//
Resolver *
ExprKit::resolver(const ConfigInfo&)
{
	return (new Resolver(this, impl_->handler()));
}

//
// Compute hash codes for detecting schema changes.
//
// These hashing functions are pretty random.  Perhaps we should use
// something like the Modula-3 fingerprints, but I can't find
// the reference that describes how to compute those.
//
void
HashCode::init()
{
	for (int i = 0; i < sizeof (hash_) / sizeof (hash_[0]); i++) {
		hash_[i] = 0;
	}
}

void
HashCode::copy(const HashCode& h)
{
	for (int i = 0; i < sizeof (hash_) / sizeof (hash_[0]); i++) {
		hash_[i] = h.hash_[i];
	}
}

void
HashCode::copy_string(String *s)
{
	const char *p = s->string();
	const char *q = &p[s->length() - 1];
	unsigned long v0 = 0, v1 = 0;
	while (p < q) {
		v0 = (v0 << 1) ^ *p;
		p++;
		v1 = (v1 << 1) ^ *p;
		p++;
	}
	if (p <= q) {
		v0 = (v0 << 1) ^ *p;
	}
	unsigned long t0 = v0 >> 10, t1 = v1 >> 10;
	hash_[0] = (v0 ^ (t0 ^ (t0 >> 10)));
	hash_[1] = (v1 ^ (t1 ^ (t1 >> 10)));
}

void
HashCode::copy_ident(Identifier *i)
{
	copy_string(i->string());
}

void
HashCode::add(const HashCode& h)
{
	unsigned long u0 = hash_[0], u1 = hash_[1];
	unsigned long v0 = h.hash_[0], v1 = h.hash_[1];
	hash_[0] = (u0 << 1) ^ v0;
	hash_[1] = (u1 << 1) ^ v1;
}

void
HashCode::add_string(String *s)
{
	HashCode h;
	h.copy_string(s);
	add(h);
}

void
HashCode::add_type(Expr *e)
{
	Symbol *s = e->symbol();
	if (s != nil) {
		if (s->tag() == Symbol::sym_interface) {
			add(s->interface()->info()->hash);
		} else {
			add(*s->hash());
		}
	}
}

void
HashCode::add_list(ExprList *list)
{
	if (list != nil) {
		for (ListItr(ExprList) i(*list); i.more(); i.next()) {
			add_type(i.cur());
		}
	}
}

bool
HashCode::match(String *s)
{
	if (hash_[0] == 0 && hash_[1] == 0) {
		return (true);
	}
	unsigned long v0 = 0, v1 = 0;
	sscanf(s->string(), "%08x%08x", &v1, &v0);
	return (hash_[0] == v0 && hash_[1] == v1);
}

//
// Resolve operations for different kinds of expressions.
//
void
ExprImpl::resolve(Resolver *)
{
}

void
ExprImpl::resolve_list(ExprList *list, Resolver *r)
{
	if (list != nil) {
		for (ListItr(ExprList) i(*list); i.more(); i.next()) {
			i.cur()->resolve(r);
		}
	}
}

void
RootExpr::resolve(Resolver *r)
{
	resolve_list(defs_, r);
}

void
Accessor::resolve(Resolver *r)
{
	Symbol *sym;
	r->push_context();
	qualifier_->resolve(r);
	sym = r->context()->out_symbol;
	r->pop_context();
	if (sym != nil) {
		Scope *s = sym->inner_scope();
		if (s == nil) {
			r->undefined_scope(this, sym);
		} else {
			symbol_ =
			    SymbolTable::the()->resolve_in_scope(s, string_);
			if (symbol_ == nil) {
				r->undefined(this, string_);
			} else {
				r->symbol(symbol_);
			}
		}
	}
}

//
// Still need to check here that the constant's value is appropriate
// for the declared type.
//
void
Constant::resolve(Resolver *r)
{
	Symbol *s = r->new_symbol(ident_);
	s->constant(this);
	symbol_ = s;
	type_->resolve(r);
	value_->resolve(r);
}

void
Unary::resolve(Resolver *r)
{
	expr_->resolve(r);
}

void
Binary::resolve(Resolver *r)
{
	left_->resolve(r);
	right_->resolve(r);
}

void
TypeName::resolve(Resolver *r)
{
	symbol_ = new Symbol(r->scope());
	symbol_->type_name(this);
	r->push_context(symbol_);
	type_->resolve(r);
	Context *c = r->context();
	Symbol *s = c->out_symbol;
	seq_ = false;
	if (s != nil) {
		symbol_->hash()->copy(*s->hash());
		if (s->tag() == Symbol::sym_sequence &&
		    declarators_->count() == 1) {
			Declarator *d = declarators_->item(0)->declarator();
			if (d != nil && d->subscripts() == nil) {
				seq_ = true;
				s->sequence_type()->name(d);
			}
		}
	}
	c->in_symbol = symbol_;
	c->type_expr = type_;
	resolve_list(declarators_, r);
	r->pop_context();
}

void
UnsignedType::resolve(Resolver *r)
{
	r->push_context();
	type_->resolve(r);
	Symbol *type = r->context()->out_symbol;
	SymbolTable *t = SymbolTable::the();
	if (type == t->short_type()) {
		symbol_ = t->ushort_type();
	} else if (type == t->long_type()) {
		symbol_ = t->ulong_type();
	} else if (type == t->longlong_type()) {
		symbol_ = t->ulonglong_type();
	} else {
		r->error(type_, "Bad type for unsigned");
	}
	r->symbol(symbol_);
	r->pop_context();
}

void
CaseElement::resolve(Resolver *r)
{
	resolve_list(labels_, r);
	r->push_context();
	element_->resolve(r);
	symbol_ = r->context()->out_symbol;
	r->pop_context();
}

void
CaseLabel::resolve(Resolver *r)
{
	r->push_context(r->context());
	value_->resolve(r);
	symbol_ = r->context()->out_symbol;
	r->pop_context();
	r->symbol(symbol_);
}

void
DefaultLabel::resolve(Resolver *r)
{
	Context *context = r->context();
	if (context != nil && context->in_symbol != nil) {
		UnionDecl *union_decl = context->in_symbol->union_tag();
		if (union_decl != nil) {
			if (!union_decl->default_label()) {
				union_decl->default_label(this);
			} else {
				r->error(this,
				    "Multiple default labels in switch");
			}
		} else {
			r->error(this, "Default label is not inside union");
		}
	}
}

void
EnumDecl::resolve(Resolver *r)
{
	Symbol *s = r->new_symbol(ident_);
	symbol_ = s;
	s->enum_tag(this);
	r->push_context(s);
	resolve_list(members_, r);
	r->pop_context();
	s->hash()->add_list(members_);
}

void
Enumerator::resolve(Resolver *r)
{
	Symbol *s = r->new_symbol(ident_);
	symbol_ = s;
	s->enum_value_tag(this);
	s->hash()->copy_ident(ident_);
	EnumDecl *enum_decl = r->context()->in_symbol->enum_tag();
	decl_ = enum_decl;
	value_ = enum_decl->assign_value();
}

void
IdentifierImpl::resolve(Resolver *r)
{
	symbol_ = r->resolve(this);
	if (symbol_ == nil) {
		r->undefined(this, value_);
	} else {
		Context *c = r->context();
		if (c != nil) {
			Symbol *s = c->in_symbol;
			if (s != nil) {
				Symbol::Tag t = symbol_->tag();
				switch (s->tag()) {
				case Symbol::sym_union:
					if (t == Symbol::sym_enum_value) {
						check(r, s->union_tag(),
						    symbol_->enum_value_tag());
					}
					break;
				case Symbol::sym_operation:
					//
					// Assume we are resolving the raises
					// clause of an operation.
					//
					if (t != Symbol::sym_exception) {
						r->symerr(this, value_,
						    "is not an exception");
					}
					break;
				}
			}
		}
		InterfaceDef *infp = symbol_->interface();
		if (infp != nil) {
			if (infp->info()->server_only) {
				r->symerr(this, value_,
				    "server only interfaces not allowed");
			}
		}
	}
	r->symbol(symbol_);
}

void
IdentifierImpl::check(Resolver *r, UnionDecl *union_decl,
    Enumerator *enumerator)
{
	EnumDecl *enum_decl = union_decl->switch_type()->enum_tag();
	if (enum_decl != nil && enum_decl != enumerator->decl()) {
		r->error(this, "Enumerator not declared in enumeration");
		return;
	}
	long value = enumerator->value();
	CaseTable *cases = union_decl->cases();
	long existing;
	if (cases->find(existing, value)) {
		r->error(this, "Case label not unique in union");
	} else {
		cases->insert(value, value);
	}
}

void
boolLiteral::resolve(Resolver *r)
{
	Context *context = r->context();
	if (context != nil && context->in_symbol != nil) {
		UnionDecl *union_decl = context->in_symbol->union_tag();
		if (union_decl != nil) {
			if (union_decl->switch_type() !=
			    SymbolTable::the()->boolean_type()) {
				r->error(this,
				    "bool case label not in boolean switch");
			} else {
				CaseTable *cases = union_decl->cases();
				long existing;
				if (cases->find(existing, value_)) {
					r->error(this,
					    "Case label not unique in union");
				} else {
					cases->insert(value_, value_);
				}
			}
		}
	}
}

void
IntegerLiteral::resolve(Resolver *r)
{
	Context *context = r->context();
	if (context && context->in_symbol) {
		UnionDecl *union_decl = context->in_symbol->union_tag();
		if (union_decl != nil) {
			check(r, union_decl->switch_type());
			CaseTable *cases = union_decl->cases();
			long existing;
			if (cases->find(existing, value_)) {
				r->error(this,
				    "Case label not unique in union");
			} else {
				cases->insert(value_, value_);
			}
		}
	}
}

void
IntegerLiteral::check(Resolver *r, Symbol *switch_type)
{
	long max, min;
	bool check_range = false;
	SymbolTable *t = SymbolTable::the();
	if (switch_type == t->short_type()) {
		min = -(1 << 15);
		max = (1 << 15) - 1;
		check_range = true;
	} else if (switch_type == t->ushort_type()) {
		min = 0;
		max = (1 << 16) - 1;
		check_range = true;
	} else if (switch_type == t->boolean_type()) {
		r->error(this, "bool switch cannot take integer cases");
	} else if (switch_type == t->char_type()) {
		r->error(this, "Char switch cannot take integer cases");
	}
	if (check_range && (value_ < min || value_ > max)) {
		r->error(this, "Case label value out of range for switch type");
	}
}

void
FloatLiteral::resolve(Resolver *r)
{
	Context *context = r->context();
	if (context != nil && context->in_symbol != nil) {
		UnionDecl *union_decl = context->in_symbol->union_tag();
		if (union_decl != nil) {
			r->error(this, "Float literal as case label");
		}
	}
}

void
StringLiteral::resolve(Resolver *r)
{
	Context *context = r->context();
	if (context != nil && context->in_symbol != nil) {
		UnionDecl *union_decl = context->in_symbol->union_tag();
		if (union_decl != nil) {
			r->error(this, "String literal as case label");
		}
	}
}

void
CharLiteral::resolve(Resolver *r)
{
	Context *context = r->context();
	if (context != nil && context->in_symbol != nil) {
		UnionDecl *union_decl = context->in_symbol->union_tag();
		if (union_decl != nil) {
			if (union_decl->switch_type() !=
			    SymbolTable::the()->char_type()) {
				r->error(this,
				    "Character literal not in "
				    "char switch type");
				return;
			}
			CaseTable *cases = union_decl->cases();
			long existing;
			if (cases->find(existing, value_)) {
				r->error(this,
				    "Case label not unique in union");
			} else {
				cases->insert(value_, value_);
			}
		}
	}
}
