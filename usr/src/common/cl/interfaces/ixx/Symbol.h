/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _SYMBOL_H
#define	_SYMBOL_H

#pragma ident	"@(#)Symbol.h	1.10	06/05/24 SMI"

/*
 * Copyright (c) 1992-1993 Silicon Graphics, Inc.
 * Copyright (c) 1993 Fujitsu, Ltd.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Silicon Graphics and Fujitsu may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Silicon Graphics and Fujitsu.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */


#include "expr-impl.h"

// Forward declarations
struct Scope;
struct ExceptDecl;

class Symbol {
public:
	enum Tag {
		sym_unknown,
		sym_module, sym_interface, sym_typedef, sym_constant,
		sym_operation, sym_parameter, sym_exception,
		sym_array,
		sym_struct, sym_member,
		sym_union, sym_union_member,
		sym_enum,	// This type is implemented by invo_args::t_long
		sym_enum_value,
		sym_sequence,
		sym_string
	};

	Symbol(Scope*);
	Symbol(const Symbol&);
	virtual ~Symbol();

	Tag		tag()			{ return tag_; }
	void		scope(Scope*);
	Scope		*scope()		{ return scope_; }
	Scope		*inner_scope();
	bool		is_top_level()		{ return scope_->name == nil; }
	bool		is_builtin_type();

	Symbol		*actual_type();
	invo_args::arg_type	type_desc();
	bool		varying();

	void		declared(bool b)	{ declared_ = b; }
	bool		declared()		{ return declared_; }
	void		declare_stub(long n)	{ declared_stub_ |= n; }
	bool		declared_stub(long n)
				{ return (declared_stub_ & n) != 0; }

	HashCode	*hash()			{ return &hash_; }

	void		copy_value(Symbol *);

	void		module(Module *);
	Module		*module();

	void		interface(InterfaceDef *);
	InterfaceDef	*interface();

	void		type_name(TypeName *);
	TypeName	*type_name();

	void		constant(Constant *);
	Constant	*constant();

	void		operation(Operation *);
	Operation	*operation();

	void		parameter(Parameter *);
	Parameter	*parameter();

	void		array(Declarator *);
	Declarator	*array();

	void		struct_tag(StructDecl *);
	StructDecl	*struct_tag();

	void		struct_member(StructMember *);
	StructMember	*struct_member();

	void		union_tag(UnionDecl *);
	UnionDecl	*union_tag();

	void		union_member(UnionMember *);
	UnionMember	*union_member();

	void		enum_tag(EnumDecl *);
	EnumDecl	*enum_tag();

	void		enum_value_tag(Enumerator *);
	Enumerator	*enum_value_tag();

	void		except_type(ExceptDecl *);
	ExceptDecl	*except_type();

	void		sequence_type(SequenceDecl *);
	SequenceDecl	*sequence_type();

	void string_type(StringDecl *);
	StringDecl	*string_type();

	Expr		*expr_type();

protected:
	Tag tag_;
	Scope		*scope_;
	bool		declared_;
	long		declared_stub_;
	HashCode	hash_;
	union {
		Module		*module_;
		InterfaceDef	*interface_;
		TypeName	*type_;
		Constant	*constant_;
		Operation	*operation_;
		Parameter	*parameter_;
		Declarator	*array_;
		StructDecl	*struct_tag_;
		StructMember	*struct_member_;
		UnionDecl	*union_tag_;
		UnionMember	*union_member_;
		EnumDecl	*enum_tag_;
		Enumerator	*enum_value_tag_;
		ExceptDecl	*except_type_;
		SequenceDecl	*sequence_type_;
		StringDecl	*string_type_;
		Expr		*any_type_;
	} value_;
};

inline bool
Symbol::is_builtin_type()
{
	return (tag_ == Symbol::sym_typedef && value_.type_->type() == nil);
}

#endif	/* _SYMBOL_H */
