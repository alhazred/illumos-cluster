//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

/*
 * Copyright (c) 1992-1993 Silicon Graphics, Inc.
 * Copyright (c) 1993 Fujitsu, Ltd.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Silicon Graphics and Fujitsu may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Silicon Graphics and Fujitsu.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

#pragma ident	"@(#)InterfaceDef.cc	1.18	06/05/24 SMI"

//
// Class InterfaceDef Methods
//

#include <assert.h>
#include "generator.h"
#include "InterfaceDef.h"
#include "Operation.h"
#include "main.h"
#include "Resolver.h"
#include "Symbol.h"

//
// One operation is reserved for narrow.
// The number must fit within uint16_t (2 bytes).
//
#define	MAX_METHODS 65535

//
// The following methods came from expr.cc
//

InterfaceDef::InterfaceDef(SourcePosition *pos, bool forward, bool server_only,
    Identifier *ident, ExprList *supertypes, ExprList *defs) :
	ExprImpl(pos),
	forward_(forward),
	server_only_(server_only),
	ident_(ident),
	supertypes_(supertypes),
	parents_(nil),
	defs_(defs),
	info_(nil)
{
}

InterfaceDef::~InterfaceDef()
{
}

//
// Output code for the header file.
//
bool
InterfaceDef::generate(Generator *g)
{
	String *s = ident_->string();
	if (!info_->generated_decl) {
		// Normal case for versioned interface definition.
		g->emit("%#ifndef _interface_%_%I\n"
		    "%#define\t_interface_%_%I\n"
		    "_Ix_Forward(%I)\n"
		    "%#endif\n\n", s);
		info_->generated_decl = true;
	}
	if (!forward_) {
		put_hdr(g);
		info_->generated_body = true;
	}
	return (false);
}

//
// put_hdr - outputs the class definition to the header file.
//
void
InterfaceDef::put_hdr(Generator *g)
{
	String *s = ident_->string();
	bool save;

	g->emit("%#pragma disable_warn\n\n");
	if (parents_ != nil) {
		g->emit("class %I : public ", s);
		save = g->interface_is_ref(false);
		generate_list(parents_, &Expr::generate, g, ", public ");
		(void) g->interface_is_ref(save);
	} else {
		g->emit("class %I : public CORBA::Object", s);
	}
	g->emit(" {\npublic:\n%i");

	g->emit("// CORBA Interface Operations\n");
	if (info_->multiple_inheritance) {
		g->emit("virtual handler *_handler() = 0;\n");
		g->emit("virtual InterfaceDescriptor *_deep_type() {\n%i"
		    "return (_interface_descriptor());\n%u"
		    "}\n");

		g->emit("CORBA::Object_ptr _this_obj() {\n%i"
		    "return (");
		put_cast_up(g);
		g->emit("this);\n%u"
		    "}\n");
	}
	g->emit("virtual void *_this_ptr();\n");
	g->emit("virtual InterfaceDescriptor *_interface_descriptor();\n");
	g->emit("static int _version(CORBA::Object_ptr obj);\n");
	g->emit("static CORBA::type_info_t *_get_type_info(int v);\n");
	g->emit("static %R _narrow(CORBA::Object_ptr obj);\n", s);
	g->emit("static %R _duplicate(%R obj);\n", s);
	g->emit("static %R _nil() { return (NULL); }\n\n", s);
	g->emit("// Interface Operations\n");

	g->enter_scope(info_->block);
	save = g->is_pure(true);
	generate_list(defs_, &Expr::generate, g, ";\n", ";\n");
	(void) g->is_pure(save);

	//
	// The following table is used to keep track of method prototypes
	// output so we don't duplicate a function inherited from
	// multiple interfaces.
	//
	char **op_table = new char *[info_->max_methods];
	long cnt = 0;

	// Output prototype for version exception functions.
	emit_versionex_hdr(g, op_table, cnt, version(), true);
	g->leave_scope();

	g->emit("%u\nprotected:\n%i"
	    "%I() {}\n"
	    "~%I() {}\n"
	    "%u};\n\n", s);

	// Output stub class.
	g->emit("class %I_stub : public %I", s);
	if (parents_ != nil) {
		save = g->interface_is_ref(false);
		for (ListItr(ExprList) i(*parents_); i.more();
		    i.next()) {
			ParentImpl *p = (ParentImpl *)i.cur();
			g->emit(", public %E_stub", nil, p);
		}
		(void) g->interface_is_ref(save);
	} else {
		g->emit(", public CORBA::Object_stub");
	}
	g->emit(" {\npublic:\n%i"
	    "virtual handler *_handler() = 0;\n"
	    "virtual InterfaceDescriptor *_deep_type() = 0;\n");

	g->emit("CORBA::Object_ptr _this_obj() {\n%i"
	    "return (%I::_this_obj());\n%u"
	    "}\n", s);

	cnt = 0;
	g->enter_scope(info_->block);
	emit_methods_hdr(g, op_table, cnt, version());
	g->emit("virtual void _generic_method(CORBA::octet_seq_t &data, "
	    "CORBA::object_seq_t &objs,\n    Environment &_environment);\n");
	g->leave_scope();

	g->emit("%u\nprotected:\n%i"
	    "%I_stub() {}\n"
	    "~%I_stub() {}\n"
	    "%u};\n\n", s);
	delete [] op_table;

	g->emit("%#pragma enable_warn\n");
}

//
// Emit function prototype declarations for the inherited methods which
// this interface does not support.
//
void
InterfaceDef::emit_versionex_hdr(Generator *g, char **op_table, long &cnt,
    long v, bool incl)
{
	if (defs_ != nil && (!incl || v < version())) {
		// Check for methods not in this version of the interface.
		for (ListItr(ExprList) i(*defs_); i.more(); i.next()) {
			Symbol *sym = i.cur()->symbol();
			Operation *op;
			if (sym != nil && (op = sym->operation()) != nil) {
				if (incl && op->version() <= v)
					continue;
				//
				// Check to see if we have output the same
				// function signature before.
				//
				if (seen_method(op, op_table, cnt))
					continue;
				if (op->generate(g))
					g->emit(";\n");
			}
		}
	}

	// Determine number of parent classes.
	if (parents_ == nil) {
		// No parent classes.
		return;
	}

	// Process all ancestor classes.
	long nparents = parents_->count();
	for (long parent_index = 0; parent_index < nparents; parent_index++) {
		// Should be a ParentImpl.
		ParentImpl *p = (ParentImpl *)parents_->item(parent_index);

		// We need to check the all ancestors this version allows.
		p->interface()->emit_versionex_hdr(g, op_table, cnt,
		    p->parent_version(), p->version() <= v);
	}
}

//
// Emit function prototype declarations for the stub methods which
// this interface supports.
// The tricky part here is that if C inherits from A and B which both
// define a foo() method, we need to generate one stub if both foo()s
// have the same signature and two foo() stubs if they differ.
//
void
InterfaceDef::emit_methods_hdr(Generator *g, char **op_table, long &cnt, long v)
{
	if (defs_ != nil) {
		// Check for methods in this version of the interface.
		for (ListItr(ExprList) i(*defs_); i.more(); i.next()) {
			Symbol *sym = i.cur()->symbol();
			Operation *op;
			if (sym != nil && (op = sym->operation()) != nil) {
				if (op->version() > v)
					continue;
				//
				// Check to see if we have output the same
				// function signature before.
				//
				if (seen_method(op, op_table, cnt))
					continue;
				if (op->generate(g))
					g->emit(";\n");
			}
		}
	}

	// Determine number of parent classes.
	if (parents_ == nil) {
		// No parent classes.
		return;
	}

	// Process all ancestor classes.
	long nparents = parents_->count();
	for (long parent_index = 0; parent_index < nparents; parent_index++) {
		// Should be a ParentImpl.
		ParentImpl *p = (ParentImpl *)parents_->item(parent_index);

		// We need to check the all ancestors this version allows.
		if (p->version() <= v)
			p->interface()->emit_methods_hdr(g, op_table, cnt,
			    p->parent_version());
	}
}

//
// Output client side stub code (e.g., xxx_stub.cc).
//
bool
InterfaceDef::generate_stub(Generator *g)
{
	// Skip forward references.
	if (forward_)
		return (false);

	info_->declared_stub = true;
	String *s = ident_->string();

	//
	// Output code for _this_ptr().
	//
	g->emit(
	    "void *\n"
	    "%Q::_this_ptr()\n"
	    "{\n%i"
	    "return (this);\n%u"
	    "}\n\n", s);

	//
	// Generate the method to create a proxy.
	//
	g->emit("void *\n"
	    "_Ix_CreateProxy(%_%I)(handler *handlerp, "
	    "InterfaceDescriptor *infp)\n"
	    "{\n%i"
	    "return (new proxy<%Q_stub>(handlerp, infp));\n%u"
	    "}\n\n", s);

	long ver = version_;
	for (long v = 0; v <= ver; v++) {
		version_ = v;
		g->emit("_Ix_extern__tid(%_%I""%v);\n", s, this);
	}
	version_ = ver;
	g->emit("\n");

	//
	// Generate the structure that provides the information
	// needed to process this object at marshal time.
	//
	if (!info_->server_only) {
		g->emit("MarshalInfo_object _%Y_ArgMarshalFuncs = {%i\n"
		    "&_Ix_CreateProxy(%Y),\n"
		    "_Ix__tid(%Y_0)\n"
		    "%u};\n\n", s, this);
	}

	//
	// Output code for returning a pointer to the interface descriptor.
	//
	g->emit("extern InterfaceDescriptor _Ix__type(%_%I""%v);\n\n"
	    "InterfaceDescriptor *\n"
	    "%Q::_interface_descriptor()\n"
	    "{%i\n"
	    "return (&_Ix__type(%_%I""%v));\n"
	    "%u}\n\n", s, this);

	// Return the version number for this interface.
	g->emit("int\n"
	    "%Q::_version(CORBA::Object_ptr object_p)\n"
	    "{\n%i"
	    "if (CORBA::is_nil(object_p))\n%i"
	    "return (-1);\n%u"
	    "return (object_p->_deep_type()->get_version(_Ix__tid(%_%I_0)));\n"
	    "%u}\n\n", s, this);

	// Return the type_info_t for the requested version.
	if (!info_->server_only) {
		g->emit("CORBA::type_info_t *\n"
		    "%Q::_get_type_info(int v)\n"
		    "{\n%i"
		    "return (_Ix__type(%_%I""%v).get_type_info(v));\n"
		    "%u}\n\n", s, this);
	} else {
		//
		// Server only interfaces are always version 1.0
		// and similar to narrow, should not return any useful info.
		//
		g->emit("CORBA::type_info_t *\n"
		    "%Q::_get_type_info(int)\n"
		    "{\n%i"
		    "return (NULL);\n"
		    "%u}\n\n", s);
	}

	//
	// Narrow method - there are two versions:
	// regular or server only.
	//
	if (info_->server_only) {
		g->emit("%:%R\n"
		    "%Q::_narrow(CORBA::Object_ptr object_p)\n"
		    "{\n%i"
		    "return (_nil());\n%u"
		    "}\n\n", s);
	} else {
		g->emit("%:%R\n"
		    "%Q::_narrow(CORBA::Object_ptr object_p)\n"
		    "{\n%i"
		    "return (CORBA::is_nil(object_p) ? _nil() :\n"
		    "    (%R)object_p->_handler()->narrow(object_p,\n%i"
		    "_Ix__tid(%_%I_0),\n"
		    "&_Ix_CreateProxy(%_%I)));\n%u%u"
		    "}\n\n", s);
	}

	//
	// Duplicate method.
	//
	g->emit("%:%R\n"
	    "%Q::_duplicate(%R object_p)\n"
	    "{\n%i", s);
	if (info_->multiple_inheritance) {
		// Multiple inheritance
		g->emit("CORBA::Object_ptr obj_p = ");
		put_cast_up(g);
		g->emit("object_p;\n"
		    "return (CORBA::is_nil(obj_p) ? _nil() :\n"
		    "    (%R)obj_p->_handler()->duplicate(obj_p,\n%i", s);
	} else {
		// Single inheritance
		g->emit("return (CORBA::is_nil(object_p) ? _nil() :\n"
		    "    (%R)object_p->_handler()->duplicate(\n%i"
		    "object_p, ", s);
	}
	g->emit("_Ix__tid(%_%I_0)));\n%u%u"
	    "}\n", s);

	if (defs_ != nil) {
		g->emit("\n");
		if (!info_->declared_corba_tid) {
			info_->declared_corba_tid = true;
			g->emit("extern InterfaceDescriptor "
			    "_Ix__type(CORBA_Object);\n\n");
		}
		g->enter_scope(info_->block);
		generate_list(defs_, &Expr::generate_stub, g, "\n");

		// Output initialization code for exception TID schema table.
		if (info_->block->except_index > 0) {
			put_except_list(defs_, g);
		}
		g->leave_scope();
	}

	//
	// The following table is used to keep track of stub methods
	// output so we don't duplicate a function inherited from
	// multiple interfaces.
	//
	char **op_table = new char *[info_->max_methods];
	long cnt = 0;

	// Output stub methods.
	g->enter_scope(info_->block);
	emit_versionex_stubs(g, op_table, cnt, version(), true);
	cnt = 0;
	emit_methods_stubs(g, op_table, cnt, version(), false);

	//
	// Output delegation routines for CORBA::Object stub methods.
	//
	g->emit("\nvoid\n"
	    "%~_stub::_generic_method(CORBA::octet_seq_t &data, "
		"CORBA::object_seq_t &objs,\n"
	    "    Environment &_environment)\n"
	    "{\n%i");
	if (!info_->multiple_inheritance) {
		g->emit("CORBA::Object_stub");
	} else {
		ParentImpl *p = (ParentImpl *)parents_->item(0);
		g->emit("%I_stub", p->interface()->string());
	}
	g->emit("::_generic_method(data, objs, _environment);\n"
	    "%u}\n");
	g->leave_scope();

	delete [] op_table;

	return (true);
}

//
// Emit version exception routines for the methods
// this interface does not support.
//
void
InterfaceDef::emit_versionex_stubs(Generator *g, char **op_table, long &cnt,
    long v, bool incl)
{
	if (defs_ != nil && (!incl || v < version())) {
		// Check for methods not in this version of the interface.
		for (ListItr(ExprList) i(*defs_); i.more(); i.next()) {
			Symbol *sym = i.cur()->symbol();
			Operation *op;
			if (sym != nil && (op = sym->operation()) != nil) {
				if (incl && op->version() <= v)
					continue;
				//
				// Check to see if we have output the same
				// function signature before.
				//
				if (seen_method(op, op_table, cnt))
					continue;
				g->emit("\n");
				op->emit_versionex_stub(g);
			}
		}
	}

	// Determine number of parent classes.
	if (parents_ == nil) {
		// No parent classes.
		return;
	}

	// Process all ancestor classes.
	long nparents = parents_->count();
	for (long parent_index = 0; parent_index < nparents; parent_index++) {
		// Should be a ParentImpl.
		ParentImpl *p = (ParentImpl *)parents_->item(parent_index);

		// We need to check the all ancestors this version allows.
		p->interface()->emit_versionex_stubs(g, op_table, cnt,
		    p->parent_version(), p->version() <= v);
	}
}

//
// Emit stub delegation routines for the methods which
// this interface inherits.
//
void
InterfaceDef::emit_methods_stubs(Generator *g, char **op_table, long &cnt,
    long v, bool incl)
{
	if (incl && defs_ != nil) {
		// Check for methods in this version of the interface.
		for (ListItr(ExprList) i(*defs_); i.more(); i.next()) {
			Symbol *sym = i.cur()->symbol();
			Operation *op;
			if (sym != nil && (op = sym->operation()) != nil) {
				if (op->version() > v)
					continue;
				//
				// Check to see if we have output the same
				// function signature before.
				//
				if (seen_method(op, op_table, cnt))
					continue;
				g->emit("\n");
				op->emit_wrapper_stub(g);
			}
		}
	}

	// Determine number of parent classes.
	if (parents_ == nil) {
		// No parent classes.
		return;
	}

	// Process all ancestor classes.
	long nparents = parents_->count();
	for (long parent_index = 0; parent_index < nparents; parent_index++) {
		// Should be a ParentImpl.
		ParentImpl *p = (ParentImpl *)parents_->item(parent_index);

		// We need to check the all ancestors this version allows.
		if (p->version() <= v)
			p->interface()->emit_methods_stubs(g, op_table, cnt,
			    p->parent_version(), true);
	}
}

//
// Output the server side code to perform a method call.
//
bool
InterfaceDef::generate_server(Generator *g)
{
	if (forward_ || !info_->has_methods || !info_->generated_body ||
	    info_->server_only) {
		// no operations!
		return (false);
	}

	g->enter_scope(info_->block);
	generate_list(defs_, &Expr::generate_server, g);
	g->leave_scope();

	String	*s = ident_->string();
	for (long v = 0; v <= version(); v++) {
		if (info_->vinfo[v].nops == 0) {
			continue;
		}
		g->emit("InterfaceDescriptor::ReceiverFunc _%_%I_", s);
		g->emit_integer(v);
		g->emit("_receive_table[", s);
		g->emit_integer(info_->vinfo[v].nops);
		g->emit("] = {\n%i");
		g->enter_scope(info_->block);
		for (long i = 0; ; ) {
			info_->vinfo[v].ops[i].op->generate_receive_table(g);
			if (++i >= info_->vinfo[v].nops) {
				break;
			}
			g->emit(",\n");
		}
		g->leave_scope();
		g->emit("\n%u};\n");
	}
	return (true);
}

//
// Output code for the schema file (e.g., xxx_types.cc).
//
bool
InterfaceDef::generate_schema(Generator *g)
{
	// Don't output schema for forward declarations.
	if (info_ == nil || forward_)
		return (false);

	//
	// This type id is recorded for schema initialization.
	// We use this object since it has an entry in the
	// symbol table so emit("%F") will work.
	//
	g->add_id_to_table(this);

	for (long v = 0; v <= version(); v++) {
		emit_schema(g, v);
	}

	return (false);
}

//
// Output an InterfaceDescriptor structure for the given version of this
// interface.
//
void
InterfaceDef::emit_schema(Generator *g, long v)
{
	String *namep = ident_->string();

	// Start the type string name for this interface.
	g->start_interface(namep->string(), v);

	// Output inherited interface names and versions.
	if (supertypes_ != nil) {
		for (ListItr(ExprList) i(*supertypes_); i.more();
		    i.next()) {
			ParentImpl *p = (ParentImpl *)i.cur();
			if (p->version() != v) {
				continue;
			}
			// This is the inherited interface name.
			String *s = p->interface()->string();
			assert(s != nil);
			g->add_interface_parent(s->string(),
			    p->parent_version());
		}
	}

	// Add interface operation strings.
	for (long i = 0; i < info_->vinfo[v].nops; i++) {
		g->add_operation(info_->vinfo[v].ops[i].type_string);
	}

	//
	// O.K. Now generate the type id signature for this interface.
	//
	generate_typeid(g, v);

	//
	// Generate the index to receive function table.
	//
	bool b = g->interface_is_ref(false);

	//
	// Check check if this version of the interface has any methods and
	// generate "extern" declarations for the receiver function tables.
	// The next table requires these extern declarations.
	// This can result in duplicate extern declarations when
	// an IDL file declares multiple objects sharing an ancestor,
	// but that does not hurt.
	//
	bool has_methods = has_any_methods(g, v);
	if (has_methods) {
		g->emit("static InterfaceDescriptor::ReceiverTable "
		    "_Ix_index_func(%_%I_", namep);
		g->emit_integer(v);
		g->emit(")[] = {\n%i");
		//
		// Output list of receiver table entries for this class and
		// all parent classes.
		//
		emit_class_func(g, v);
		g->emit("%u};\n\n");
	}

	// Output any needed extern TID declarations.
	emit_tid_extern(g, v);

	//
	// Generate the object typeid to index table.
	//
	g->emit("static InterfaceDescriptor::VersionTable "
	    "_Ix_type_index(%_%I_", namep);
	g->emit_integer(v);
	g->emit(")[] = {\n%i");
	//
	// Output list of this class and all parent classes,
	// that have methods.
	//
	long inf_index = emit_class_tid(g, v, 0);
	// Null Terminate the list of classes having methods.
	g->emit("{ NULL, 0 },\n");

	// Output list of all parent classes that have no methods.
	(void) emit_class_no_methods_tid(g, v, inf_index);
	// Null Terminate the list of classes having no methods.
	g->emit("{ NULL, 0 }\n%u};\n\n");

	//
	// Multiple parents means multiple inheritance,
	// and thus the system needs offsets for different parents.
	// All versions of the interface could share the same offset table
	// since we only create one proxy for any version of an
	// interface but we would need a separate type to index table.
	// Since this isn't used much, it didn't seem worth the trouble.
	//
	if (info_->multiple_inheritance) {
		g->emit("_Ix_BeginOffsetList(%_%I_", namep);
		g->emit_integer(v);
		g->emit(")\n%i");
		emit_offset_info(g, namep, v, true);
		emit_offset_info(g, namep, v, false);
		g->emit("%u_Ix_EndOffsetList()\n\n");
	}

	//
	// Note: all versions use the highest "known" proxy create function
	// so that when an object is unmarshalled, all methods can be called
	// and return a version exception if the deep type doesn't
	// support that method.
	//
	if (v == 0) {
		g->emit("extern void *_Ix_CreateProxy(%_%I)(handler *, "
		    "InterfaceDescriptor *);\n\n", namep);
	}

	//
	// Generate the InterfaceDescriptor object definition.
	//
	g->emit("InterfaceDescriptor _Ix__type(%_%I_", namep);
	g->emit_integer(v);
	g->emit(")(\n%i"
	    "\"%Q_", namep);
	g->emit_integer(v);
	// CSTYLED
	g->emit("\",\n"
	    "_Ix__tid(%_%I_", namep);
	g->emit_integer(v);
	g->emit("),\n"
	    "_Ix__tid(%_%I_0),\n", namep);
	g->emit_integer(v);
	if (info_->multiple_inheritance) {
		g->emit(",\n_Ix__offsets(%_%I_", namep);
		g->emit_integer(v);
		g->emit("),\n");
	} else {
		g->emit(",\nNULL,\n");
	}
	g->emit("_Ix_type_index(%_%I_", namep);
	g->emit_integer(v);
	g->emit("),\n");
	if (has_methods) {
		g->emit("_Ix_index_func(%_%I_", namep);
		g->emit_integer(v);
		g->emit("),\n");
	} else {
		g->emit("NULL,\n");
	}
	g->emit("&_Ix_CreateProxy(%_%I));\n\n%u", namep);

	(void) g->interface_is_ref(b);
}

//
// Generate the Typeid for this interface.
//
void
InterfaceDef::generate_typeid(Generator *g, long v)
{
	// Run MD5 on the string
	char *s = g->get_type_string();
	unsigned char digest[16];
	MDString(s, digest);

	// Output the object type id.
	info_->vinfo[v].emitted_tid = true;
	char *buf = new char [strlen(ident_->string()->string()) + 20];
	sprintf(buf, "%s_%ld", ident_->string()->string(), v);
	String str(buf);
	g->emit_tid(digest, &str);
	delete [] buf;

	if (g->tidfile() != nil) {
		fprintf(g->tidfile(), "TID: %s\n", s);
		fprintf(g->tidfile(), "MD5: ");
		for (int i = 0; i < 16; i++)
			fprintf(g->tidfile(), "%02x", digest[i]);
		fprintf(g->tidfile(), "\n");
	}
	if (type_debug == true) {
		fprintf(stderr, "TID digest input: %s\n", s);
		fprintf(stderr, "MD5: ");
		for (int i = 0; i < 16; i++)
			fprintf(stderr, "%02x", digest[i]);
		fprintf(stderr, "\n");
	}
	free(s);
}

//
// Recursively visit this class and all of its ancestor classes.
// If the class has methods, output an extern declaration for the
// receiver function and return true.
//
bool
InterfaceDef::has_any_methods(Generator *g, long v)
{
	bool b = false;

	// Test current version of the interface for methods.
	if (info_->vinfo[v].nops > 0) {
		if (!info_->vinfo[v].declared_rfunc) {
			info_->vinfo[v].declared_rfunc = true;
			g->emit("extern InterfaceDescriptor::ReceiverFunc _");
			bool save = g->concat(true);
			g->emit_scope(info_->block);
			(void) g->concat(save);
			g->emit("_");
			g->emit_integer(v);
			g->emit("_receive_table[];\n");
		}
		b = true;
	}

	// Determine number of parent classes.
	if (supertypes_ == nil) {
		// No parent classes.
		return (b);
	}

	// Process all ancestor classes.
	long nparents = supertypes_->count();
	for (long parent_index = 0; parent_index < nparents; parent_index++) {
		// Should be a ParentImpl.
		ParentImpl *p = (ParentImpl *)supertypes_->item(parent_index);

		// We need to check the all ancestors this version allows.
		if (p->version() <= v &&
		    p->interface()->has_any_methods(g, p->parent_version()))
			b = true;
	}

	return (b);
}

//
// Recursively visit this class and all of its ancestor classes.
// If the class has methods, then output the class name TID and index.
//
long
InterfaceDef::emit_class_tid(Generator *g, long v, long index)
{
	for (long i = v; i >= 0; i--) {
		// Test interface for methods.
		if (info_->vinfo[i].nops > 0) {
			g->emit("{ _Ix__tid(");
			bool b = g->concat(true);
			g->emit_scope(info_->block);
			(void) g->concat(b);
			g->emit("_");
			g->emit_integer(i);
			g->emit("), ");
			g->emit_integer(index);
			g->emit(" },\n");

			index++;
		}
	}

	// Determine number of parent classes.
	if (supertypes_ == nil) {
		// No parent classes.
		return (index);
	}

	// Process all ancestor classes.
	long nparents = supertypes_->count();
	for (long parent_index = 0; parent_index < nparents; parent_index++) {
		// Should be a ParentImpl.
		ParentImpl *p = (ParentImpl *)supertypes_->item(parent_index);

		if (p->version() <= v)
			index = p->interface()->emit_class_tid(g,
			    p->parent_version(), index);
	}
	return (index);
}

//
// emit_class_no_methods_tid - recursively visit this class and all of its
// ancestor classes. If the class has no methods,
// then output the class name with the "_tid" suffix.
// The emit function has only a limited number of parameters, which
// explains why the suffix is hard coded.
//
long
InterfaceDef::emit_class_no_methods_tid(Generator *g, long v, long index)
{
	for (long i = v; i >= 0; i--) {
		// Test interface for no methods.
		if (info_->vinfo[i].nops == 0) {
			g->emit("{ _Ix__tid(");
			bool b = g->concat(true);
			g->emit_scope(info_->block);
			(void) g->concat(b);
			g->emit("_");
			g->emit_integer(i);
			g->emit("), ");
			g->emit_integer(index);
			g->emit(" },\n");

			index++;
		}
	}

	// Determine number of parent classes.
	if (supertypes_ == nil) {
		// No parent classes.
		return (index);
	}

	// Process all ancestor classes.
	long nparents = supertypes_->count();
	for (long parent_index = 0; parent_index < nparents; parent_index++) {
		// Should be a ParentImpl.
		ParentImpl *p = (ParentImpl *)supertypes_->item(parent_index);

		if (p->version() <= v)
			index = p->interface()->emit_class_no_methods_tid(g,
			    p->parent_version(), index);
	}
	return (index);
}

//
// emit_class_func - recursively visit this class and all of its
// ancestor classes. If the class has methods, then output a table entry.
//
void
InterfaceDef::emit_class_func(Generator *g, long v)
{
	for (long i = v; i >= 0; i--) {
		//
		// Output a table entry if there are any methods
		// for this version.
		//
		if (info_->vinfo[i].nops > 0) {
			char buf[24];
			sprintf(buf, "{ %ld, _", info_->vinfo[i].nops);
			g->emit(buf);
			bool b = g->concat(true);
			g->emit_scope(info_->block);
			(void) g->concat(b);
			g->emit("_");
			g->emit_integer(i);
			g->emit("_receive_table },\n");
		}
	}

	// Determine number of parent classes.
	if (supertypes_ == nil) {
		// No parent classes.
		return;
	}

	// Process all ancestor classes.
	long nparents = supertypes_->count();
	for (long parent_index = 0; parent_index < nparents; parent_index++) {
		// Should be a ParentImpl.
		ParentImpl *p = (ParentImpl *)supertypes_->item(parent_index);

		if (p->version() <= v)
			p->interface()->emit_class_func(g, p->parent_version());
	}
}

//
// emit_tid_extern - recursively visit this class and all of its
// ancestor classes. Output an extern declaration for the object type id.
// Skip the extern declaration for classes that emitted a type id or
// if we have already output an extern declaration.
//
void
InterfaceDef::emit_tid_extern(Generator *g, long v)
{
	// Test whether the class has emitted a type id.
	if (!info_->vinfo[v].emitted_tid) {
		info_->vinfo[v].emitted_tid = true;
		g->emit("_Ix_extern__tid(");
		bool b = g->concat(true);
		g->emit_scope(info_->block);
		(void) g->concat(b);
		g->emit("_");
		g->emit_integer(v);
		g->emit(");\n");
	}

	// Determine number of parent classes.
	if (supertypes_ == nil) {
		// No parent classes.
		return;
	}

	// Process all ancestor classes.
	long nparents = supertypes_->count();
	for (long parent_index = 0; parent_index < nparents; parent_index++) {
		// Should be a ParentImpl.
		ParentImpl *p = (ParentImpl *)supertypes_->item(parent_index);

		if (p->version() <= v)
			p->interface()->emit_tid_extern(g, p->parent_version());
	}
}

//
// Emit an array of offsets which can be used to "cast" an object pointer
// to one of its parent classes.
// This array is indexed by the index value used to describe the parent
// interface (i.e., the typeid to index table).
//
void
InterfaceDef::emit_offset_info(Generator *g, String *namep, long v,
    bool methods)
{
	for (long i = v; i >= 0; i--) {
		// Test interface for methods.
		if (methods && info_->vinfo[i].nops > 0 ||
		    !methods && info_->vinfo[i].nops == 0) {
			g->emit("_Ix_OffsetEntry(%:%I, ", namep);
			g->emit_scope(info_->block);
			g->emit("),\n");
		}
	}

	// Determine number of parent classes.
	if (supertypes_ == nil) {
		// No parent classes.
		return;
	}

	// Process all ancestor classes.
	long nparents = supertypes_->count();
	for (long parent_index = 0; parent_index < nparents; parent_index++) {
		// Should be a ParentImpl.
		ParentImpl *p = (ParentImpl *)supertypes_->item(parent_index);

		if (p->version() <= v)
			p->interface()->emit_offset_info(g, namep,
			    p->parent_version(), methods);
	}
}

//
// Generate a cast to the current interface's ancestors.
// The cast does NOT include this interface, as the exact format of
// that may vary depending on the current scope.
//
bool
InterfaceDef::put_cast_up(Generator *g)
{
	bool b = false;
	if (parents_ != nil) {
		ParentImpl *p = (ParentImpl *)parents_->item(0);
		InterfaceDef *interfacep = p->interface();
		if (interfacep != nil) {
			b = interfacep->put_cast_up(g);
		}
		if (parents_->count() > 1) {
			g->emit("(%F""%p)", nil, interfacep);
			b = true;
		}
	}
	return (b);
}

bool
InterfaceDef::generate_name(Generator *g)
{
	g->emit("%I", ident_->string());
	return (true);
}

//
// This is called when generating the type ID string for an interface
// parameter of a method.
//
bool
InterfaceDef::generate_type_name(Generator *g)
{
	g->add_operation_param("interface");
	g->add_operation_param(ident_->string()->string());
	return (false);
}

//
// This is called if an interface method returns an interface object
// or has an interface object as a parameter.
//
bool
InterfaceDef::generate_extern_stubs(Generator *g)
{
	if (!info()->declared_stub) {
		info()->declared_stub = true;
		g->emit("extern MarshalInfo_object _%Y_ArgMarshalFuncs;\n",
		    nil, this);
		return (true);
	}
	return (false);
}

//
// The following methods came from Resolver.cc
//

void
InterfaceDef::resolve(Resolver *resolverp)
{
	InterfaceDef	*interfacep;
	bool		new_symbol = false;
	Symbol		*symbolp = resolverp->resolve(ident_);
	if (symbolp == nil) {
		new_symbol = true;
		symbolp = new Symbol(resolverp->scope());
		symbolp->hash()->copy_ident(ident_);
		resolverp->bind(ident_, symbolp);
		interfacep = nil;
	} else {
		interfacep = symbolp->interface();
	}
	symbol_ = symbolp;
	if (interfacep == nil) {
		//
		// This is a new interface (or object).
		//
		assert(info_ == nil);

		info_ = new InterfaceInfo;
		info_->block = nil;
		info_->server_only = server_only_;
		info_->multiple_inheritance = false;
		info_->has_methods = false;
		info_->declared_stub = false;
		info_->declared_corba_tid = false;
		info_->generated_decl = false;
		info_->generated_body = false;
		info_->nvinfo = 0;
		info_->vinfo = nil;
		info_->max_params = 0;
		info_->max_methods = -1;
		info_->hash.copy(*symbolp->hash());
	} else {
		info_ = interfacep->info_;
		if (server_only_)
			info_->server_only = true;
	}
	if (forward_) {
		//
		// This is a forward declaration.
		//
		if (new_symbol) {
			symbolp->interface(this);
		} else if (interfacep == nil) {
			resolverp->redefined(ident_);
		}
	} else {
		//
		// This is a regular interface declaration.
		//
		if (!new_symbol) {
			if (interfacep == nil || !interfacep->forward_) {
				resolverp->redefined(ident_);
			}
		}
		//
		// Server only interfaces should inherit from two or more
		// interfaces and not have any local definitions.
		// There will be a class defined in the header file
		// but no stubs or skels so you can't narrow to a server
		// only interface.
		//
		if (info_->server_only) {
			if (supertypes_ == nil || supertypes_->count() < 2) {
				resolverp->error(ident_,
				    "Server only interfaces "
				    "should inherit from 2 or more interfaces");
			}
			if (defs_ != nil) {
				resolverp->error(ident_,
				    "Server only interfaces "
				    "should have no definitions");
			}
		}

		if (supertypes_ != nil) {
			resolve_list(supertypes_, resolverp);

			//
			// Find the highest version of this interface.
			// Also, check for same interface inherited
			// multiple times.
			// Build a list "parents_" which contains only the
			// highest version of a given ancestor.
			//
			for (ListItr(ExprList) i(*supertypes_); i.more();
			    i.next()) {
				// Should be a ParentImpl.
				ParentImpl *p = (ParentImpl *)i.cur();
				long v = p->version();
				if (version_ < v)
					version_ = v;

				ParentImpl *pp = inherits(p);
				if (pp != nil) {
					if (pp->version() == p->version() ||
					    pp->parent_version() ==
					    p->parent_version()) {
						resolverp->error(p,
						    "Same interface is "
						    "inherited more than once");
					} else if (pp->parent_version() >
					    p->parent_version()) {
						resolverp->error(p,
						    "Inherited interface "
						    "version regression");
						p = pp;
					}
				}
				if (parents_ == nil) {
					parents_ = new ExprList;
				}
				parents_->append(p);
			}
		}
		symbolp->interface(this);

		resolverp->push_context(symbolp);
		info_->block = resolverp->enter_scope(ident_);
		if (defs_ != nil) {
			resolve_list(defs_, resolverp);
			symbolp->hash()->add_list(defs_);
			// Find the highest version of this interface.
			for (ListItr(ExprList) i(*defs_); i.more(); i.next()) {
				Expr *e = i.cur();
				Symbol *s = e->symbol();
				Operation *op;
				if (s != nil && (op = s->operation()) != nil) {
					info_->has_methods = true;
					long v = op->version();
					if (version_ < v)
						version_ = v;
				}
			}
		}
		resolverp->leave_scope();
		resolverp->pop_context();
	}
}

//
// Search the list of non-duplicated ancestor interfaces for "parent"
// and return the pointer if found.
//
ParentImpl *
InterfaceDef::inherits(ParentImpl *parent)
{
	if (parents_ != nil) {
		for (ListUpdater(ExprList) i(*parents_); i.more(); i.next()) {
			// Should be a ParentImpl.
			ParentImpl *p = (ParentImpl *)i.cur();

			// Note: this is not a simple pointer comparison.
			if (p->string() == parent->string() ||
			    p->symbol() == parent->symbol()) {
				i.remove_cur();
				return (p);
			}
		}
	}
	return (nil);
}

//
// Search the list of non-duplicated ancestor interfaces for "inf"
// and return its index (off_index).
//
long
InterfaceDef::off_index(InterfaceDef *inf)
{
	if (parents_ != nil) {
		// Ancestors start in offset table at index one.
		long off = 1;
		for (ListItr(ExprList) i(*parents_); i.more(); i.next()) {
			// Should be a ParentImpl.
			ParentImpl *p = (ParentImpl *)i.cur();

			if (p->interface() == inf) {
				return (off);
			}
			off++;
		}
	}
	// off_index zero is for the interface itself.
	return (0);
}

//
// Return true if this interface or one of its ancestors has multiple
// inheritance.
//
bool
InterfaceDef::multiple_inherits()
{
	if (parents_ != nil) {
		if (parents_->count() > 1) {
			return (true);
		}
		ParentImpl *p = (ParentImpl *)parents_->item(0);
		return (p->interface()->multiple_inherits());
	}
	return (false);
}

int
InterfaceDef::opinfo_sort(const void *a, const void *b)
{
	const OpInfo *ap = (const OpInfo *)a, *bp = (const OpInfo *)b;
	return (strcmp(ap->type_string, bp->type_string));
}

VersionInfo::VersionInfo()
{
	nops = 0;
	ops = nil;
	declared_rfunc = false;
	emitted_tid = false;
}

//
// This pass is done after resolve() has been called for all Expr's
// in the parse tree.  We sort the methods based on TID strings and
// assign cooresponding method numbers before any code is generated.
// We also check for forward interface declarations which don't have
// a definition someplace.
//
void
InterfaceDef::rewrite(Generator *g, ListUpdater(ExprList) *)
{
	ErrorHandler *err = g->handler();

	if (forward_) {
		//
		// Check to be sure forward declarations have a definition.
		//
		if (info_->block == nil) {
			set_source_position(err);
			err->error("Interface is undefined");
		}
		return;
	}

	//
	// Keep a table of the methods for each version of the interface
	// so we can generate the receiver function table(s).
	//
	long v;
	Operation *op;

	if (supertypes_ != nil) {
		for (ListItr(ExprList) i(*supertypes_); i.more();  i.next()) {
			ParentImpl *p = (ParentImpl *)i.cur();
			InterfaceDef *inf = p->symbol()->interface();

			// Make sure parent interface is defined.
			if (inf->info()->block == nil) {
				p->set_source_position(err);
				err->error("Inherited interface is not "
				    "defined");
			}
			//
			// Make sure interface doesn't inherit from a
			// version which doesn't exist.
			//
			if (p->parent_version() > inf->version()) {
				p->set_source_position(err);
				err->error("Inherited interface version is not "
				    "defined");
			} else
				p->interface(inf);
		}
		info_->multiple_inheritance = multiple_inherits();
	}

	// Define an array of structures, one for each version.
	info_->nvinfo = version_ + 1;
	info_->vinfo = new VersionInfo [version_ + 1];

	if (defs_ != nil) {
		//
		// Count the number of operations in each version of the
		// interface.
		//
		for (ListItr(ExprList) i(*defs_); i.more();  i.next()) {
			op = i.cur()->symbol()->operation();
			if (op != nil) {
				assert(op->version() <= version_);
				info_->vinfo[op->version()].nops++;
			}
		}

		// Allocate an array to save the TID strings.
		for (v = 0; v <= version_; v++) {
			//
			// Check whether exceeded max number of operations
			// on an object.
			//
			if (info_->vinfo[v].nops > MAX_METHODS) {
				set_source_position(err);
				err->error("Maximum number of operations "
				    "in an interface exceeded");
			}
			if (info_->vinfo[v].nops != 0) {
				info_->vinfo[v].ops =
				    new OpInfo [info_->vinfo[v].nops];
				//
				// Reset counter so we can use it to
				// save strings.
				//
				info_->vinfo[v].nops = 0;
			} else {
				info_->vinfo[v].ops = nil;
			}
		}

		// Now put definitions into the correct interface object.
		for (ListItr(ExprList) d(*defs_); d.more();  d.next()) {
			//
			// For operations, generate the type ID string and
			// save it.
			//
			op = d.cur()->symbol()->operation();
			if (op != nil) {
				g->enter_scope(info_->block);
				op->generate_type_name(g);
				g->leave_scope();
				v = op->version();
				info_->vinfo[v].ops[
				    info_->vinfo[v].nops].op = op;
				info_->vinfo[v].ops[
				    info_->vinfo[v].nops++].type_string =
					g->get_type_string();
			}
		}

		//
		// Now we can sort the list of operation TID strings and
		// set the method numbers so the interface TID string and
		// method numbers will match. The sorting allows methods to
		// be listed in any order in the IDL file.
		// This order must also be used to generate the receive_table[]
		// since it is indexed by method number.
		//
		for (v = 0; v <= version(); v++) {
			qsort(info_->vinfo[v].ops, info_->vinfo[v].nops,
			    sizeof (OpInfo), opinfo_sort);
			for (long j = 0; j < info_->vinfo[v].nops; j++) {
				info_->vinfo[v].ops[j].op->set_index(j);
			}
		}
	}

	long cnt = 0;
	cnt_methods(cnt);
	info_->max_methods = cnt;
}

//
// Count the total number of methods an interface can call.
//
void
InterfaceDef::cnt_methods(long &cnt)
{
	// Optimization: If we have already counted this interface, return it.
	if (info_->max_methods >= 0) {
		cnt += info_->max_methods;
		return;
	}

	for (long v = 0; v < info_->nvinfo; v++) {
		cnt += info_->vinfo[v].nops;
	}

	// Determine number of parent classes.
	if (parents_ == nil) {
		// No parent classes.
		return;
	}

	// Process all ancestor classes.
	long nparents = parents_->count();
	for (long parent_index = 0; parent_index < nparents; parent_index++) {
		// Should be a ParentImpl.
		ParentImpl *p = (ParentImpl *)parents_->item(parent_index);

		// We need to check the all ancestors this version allows.
		p->interface()->cnt_methods(cnt);
	}
}

//
// Return true/false if we have output the same function signature before.
// If we haven't seen it, add it to the table.
//
bool
InterfaceDef::seen_method(Operation *op, char **op_table, long &cnt)
{
	char *s = info_->vinfo[op->version()].ops[op->index()].type_string;

	for (long i = 0; i < cnt; i++) {
		if (strcmp(op_table[i], s) == 0) {
			return (true);
		}
	}
	op_table[cnt++] = s;
	return (false);
}

//
// ParentImpl - defines an inherited interface.
//

ParentImpl::ParentImpl(SourcePosition *pos, Expr *name, long version,
    long parent_version, InterfaceDef *inf) :
	ExprImpl(pos),
	name_(name),
	parent_version_(parent_version),
	interface_(inf)
{
	version_ = version;
}

ParentImpl::~ParentImpl()
{
}

void
ParentImpl::resolve(Resolver *resolverp)
{
	name_->resolve(resolverp);

	//
	// Check that supertype is an interface
	// and save its pointer for later in rewrite().
	//
	Symbol *s = symbol();
	if (s == nil || (interface_ = s->interface()) == nil) {
		resolverp->symerr(this, string(), "is not an interface");
	}
}

bool
ParentImpl::generate(Generator *g)
{
	return (name_->generate(g));
}
