/*
 *  Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 *  Use is subject to license terms.
 */

/*
 * Copyright (c) 1992-1993 Silicon Graphics, Inc.
 * Copyright (c) 1993 Fujitsu, Ltd.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Silicon Graphics and Fujitsu may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Silicon Graphics and Fujitsu.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

/*
 * IDL grammar
 *
 * Have to use slash-star comments instead of slash-slash comments for yacc.
 */
/* CSTYLED */
%{

#pragma ident	"@(#)gram.y	1.24	08/01/09 SMI"

#include "exprkit.h"
#include "scanner.h"
#include <stdio.h>
#include <orb/invo/invo_args.h>
#if defined(YYDEBUG)
#include <stdio.h>
#endif
#include <string.h>

#ifdef sun
/* workaround for Sun bug */
#include <malloc.h>
#endif

/*
 * Global data needed for communication between a yacc parser and
 * the rest of the program.
 */

Scanner		*yyparse_scanner;
ExprKit		*yyparse_exprkit;
ExprList	*yyparse_root;

char *interface_name = "";	// this string has the current interface name
char *module_name = "";		// this string has the current module name

/*
 * It would be better if yylex and yyerror were inline functions,
 * but some versions of yacc will generate prototypes or other
 * potentially confusing information unless they are macros.
 *
 * On the other hand, IBM AIXV3 requires that they be functions ...
 */

#if defined(AIXV3)
extern "C" {
    int yylex() { return yyparse_scanner->get_token(); }
    void yyerror(char *msg) { yyparse_scanner->error(msg); }
}
#else
#define	yylex() (yyparse_scanner->get_token())
#define	yyerror(msg) (yyparse_scanner->error(msg))
#endif

static ExprList *
definition_list(Expr *def)
{
	ExprList *s = yyparse_exprkit->exprlist();
	s->append(def);
	return (s);
}

static ExprList *
append(ExprList *list, Expr *element)
{
	ExprList *e = list;
	if (e == nil) {
		e = yyparse_exprkit->exprlist();
	}
	e->append(element);
	return (e);
}

static ExprList *
concat(ExprList *list1, ExprList *list2)
{
	ExprList* e = list1;
	if (e == nil) {
		e = list2;
	} else if (list2 != nil) {
		for (ListItr(ExprList) i(*list2); i.more(); i.next()) {
			e->append(i.cur());
		}
	}
	return (e);
}

static CaseList *
case_append(CaseList *list, CaseElement *element)
{
	CaseList *e = list;
	if (e == nil) {
		e = yyparse_exprkit->caselist();
	}
	e->append(element);
	return (e);
}

inline Expr *
ident(UniqueString *s)
{
	//
	// Store the string in the list of names in order
	// to find the scoped name.
	//
	Expr *i = yyparse_exprkit->ident(s);
	i->ExprValue.strings[i->ExprValue.numstr++] = strdup(s->string());
	return (i);
}

inline Expr *
actual_interface(UniqueString *s, ExprList *supertypes, ExprList *defs)
{
	return (yyparse_exprkit->actual_interface(ident(s), supertypes, defs));
}

inline Expr *
forward_interface(UniqueString *s)
{
	return (yyparse_exprkit->forward_interface(ident(s)));
}

inline Expr *
parent_interface(Expr *e, long version, long parent_version)
{
	return (yyparse_exprkit->parent_interface(e, version, parent_version));
}

inline Expr *
module(UniqueString *s, ExprList *defs)
{
	return (yyparse_exprkit->module(ident(s), defs));
}

inline Expr *
srcpos(SourcePosition *p)
{
	return (yyparse_exprkit->position(p));
}

inline Expr *
server_only(UniqueString *s)
{
	return (yyparse_exprkit->server_only(ident(s)));
}

inline Expr *
global(UniqueString *s)
{
	Expr *e = yyparse_exprkit->scoped(nil, s);

	e->ExprValue.strings[e->ExprValue.numstr++] = strdup("");
	e->ExprValue.strings[e->ExprValue.numstr++] = strdup(s->string());

	return (e);
}

inline Expr *
scoped(Expr *scope, UniqueString *s)
{

	if (scope->ExprValue.numstr > 2) {
		// Cant have more than 2 strings
		fprintf(stderr, "Too many strings!!!!!\n");
	}
	Expr *e = yyparse_exprkit->scoped(scope, s);

	for (int i = 0; i < scope->ExprValue.numstr; i++) {
		e->ExprValue.strings[e->ExprValue.numstr++] =
		    strdup(scope->ExprValue.strings[i]);
	}
	e->ExprValue.strings[e->ExprValue.numstr++] = strdup(s->string());

	return (e);
}

inline Expr *
const_dcl(Expr *ident, Expr *type, Expr *expr)
{
	return (yyparse_exprkit->constant(ident, type, expr));
}

inline Expr *
boolcon(bool value)
{
	return (yyparse_exprkit->boolean_literal(value));
}

inline Expr *
intcon(long value)
{
	return (yyparse_exprkit->integer_literal(value));
}

inline Expr *
charcon(long value)
{
	return (yyparse_exprkit->char_literal(value));
}

inline Expr *
floatcon(double value)
{
	return (yyparse_exprkit->float_literal(value));
}

inline Expr *
stringcon(String *value)
{
	return (yyparse_exprkit->string_literal(value));
}

inline Expr *
unary(Opcode op, Expr *expr)
{
	return (yyparse_exprkit->unary(op, expr));
}

inline Expr *
binary(Opcode op, Expr *left, Expr *right)
{
	return (yyparse_exprkit->binary(op, left, right));
}

inline Expr *
typedef_decl(Expr *type, ExprList *declarator_list)
{
	return (yyparse_exprkit->type_name(type, declarator_list));
}

inline Expr *
name_type(Expr *name)
{
	return (name_decl(name));
}

//
// Must turn a reference Object internally to CORBA::Object so
// that the generated code is correctly scoped.
//
inline Expr *
object_type()
{
	Expr	*e = yyparse_exprkit->scoped(
	    yyparse_exprkit->ident(new String("CORBA")),
	    new UniqueString("Object"));
	e->ExprValue.any_objs = true;
	e->ExprValueObjtref(1);
	return (e);
}

//
// unsigned_type - This type must be one of the various integer types.
//
inline Expr *
unsigned_type(Expr *type)
{
	//
	// Get the base type.
	//
	Expr	*x = name_decl(type);

	Expr	*e = yyparse_exprkit->unsigned_type(type);
	e->ExprValue.any_objs = x->ExprValue.any_objs;
	e->ExprValueSize(x->ExprValueSize());
	e->ExprValueObjtref(x->ExprValueObjtref());
	e->ExprValueUnknown(x->ExprValueUnknown());
	return (e);
}

inline Expr *
long_type()
{
	Expr	*e = yyparse_exprkit->ident(new String("long"));
	e->ExprValueSize(4);
	return (e);
}

inline Expr *
ulong_type()
{
	Expr	*e = yyparse_exprkit->unsigned_type(long_type());
	e->ExprValueSize(4);
	return (e);
}

inline Expr *
long_long_type()
{
	Expr	*e = yyparse_exprkit->ident(new String("longlong"));
	e->ExprValueSize(8);
	return (e);

}

inline Expr *
ulong_long_type()
{
	Expr	*e = yyparse_exprkit->unsigned_type(long_long_type());
	e->ExprValueSize(8);
	return (e);
}

static ExprList *
declarator_list(Expr *declarator)
{
	ExprList	 *s = yyparse_exprkit->exprlist();
	s->append(declarator);
	return (s);
}

inline Expr *
declarator_decl(Expr *ident, ExprList *opt_subscript_list)
{
	return (yyparse_exprkit->declarator_decl(ident, opt_subscript_list));
}

inline Expr *
struct_type(Expr *ident, ExprList *member_list)
{
	return (yyparse_exprkit->struct_decl(ident, member_list));
}

inline Expr *
struct_member(Expr *type, ExprList *declarator_list)
{
	return (yyparse_exprkit->struct_member(type, declarator_list));
}

inline Expr *
union_type(Expr *ident, Expr *type, CaseList *case_list)
{
	return (yyparse_exprkit->union_decl(ident, type, case_list));
}

static CaseList *
case_list(CaseElement *case_stmt)
{
	CaseList *s = yyparse_exprkit->caselist();
	s->append(case_stmt);
	return (s);
}

inline CaseElement *
case_element(ExprList *case_label_list, UnionMember *element)
{
	return (yyparse_exprkit->case_element(case_label_list, element));
}

static ExprList *
case_label_list(Expr *case_label)
{
	ExprList *s = yyparse_exprkit->exprlist();
	s->append(case_label);
	return (s);
}

inline Expr *
case_label(Expr *value)
{
	return (yyparse_exprkit->case_label(value));
}

inline Expr *
default_label()
{
	return (yyparse_exprkit->default_label());
}

inline UnionMember *
union_member(Expr *type, Expr *declarator)
{
	return (yyparse_exprkit->union_member(type, declarator));
}

inline Expr *
enum_type(Expr *ident, ExprList *name_list)
{
	return (yyparse_exprkit->enum_decl(ident, name_list));
}

inline Expr *
enumerator(UniqueString *s)
{
	return (yyparse_exprkit->enumerator(ident(s)));
}

inline Expr *
sequence_type(Expr *type, Expr *opt_length)
{
	return (yyparse_exprkit->sequence_decl(type, opt_length));
}

inline Expr *
string_type(Expr *opt_length)
{
	return (yyparse_exprkit->string_decl(opt_length));
}

inline Expr *
except_dcl(Expr *ident, ExprList *member_list)
{
	return (yyparse_exprkit->except_decl(ident, member_list));
}

/*
 * This method processes a method (or operation) declaration on
 * an interface (or object).
 */
inline Expr *
op(long pool, long attr, Expr *type, UniqueString *method_name,
    long version, ExprList *params, ExprList *raises, ExprList *context)
{
	return (yyparse_exprkit->operation(ident(method_name), version,
	    type, params, raises, attr, context, pool));
}

static ExprList *
param_list(Expr *param)
{
	ExprList *s = yyparse_exprkit->exprlist();
	s->append(param);
	return (s);
}

/*
 * This method processes a declaration for a parameter.
 */
inline Expr *
param(invo_args::arg_size_info size_type, invo_args::arg_mode attribute,
    Expr *type, Expr *ident, ExprList *subscripts)
{
	return (yyparse_exprkit->parameter(size_type, attribute, type,
	    ident, subscripts));
}

static ExprList *
string_list(Expr *str)
{
	ExprList	*s = yyparse_exprkit->exprlist();
	s->append(str);
	return (s);
}

%}

%token
	/*
	 * Keywords in alphabetical order.
	 * Add CHECKPOINT if we decide to use it.
	 * Galileo does not support CORBA "attribute" or "readonly".
	 * These keywords have been retained to prevent their use
	 * in case the Galileo project ever decides to support them.
	 */
	ATTRIBUTE
	CASE CONST CONTEXT
	DEFAULT
	ENUM EXCEPTION
	FALSE
	IN INOUT INTERFACE
	LONG
	MARSHAL_SIZE MODULE NONBLOCKING
	OBJECT ONEWAY OPERATOR OUT
	RAISES READONLY RECONFIG
	SEQUENCE STRING_TOKEN STRUCT SWITCH
	TRUE TYPEDEF
	UNION UNRELIABLE UNSIGNED

	/* include other C++ keywords to avoid generating confusing code */
	ASM AUTO BREAK CLASS CONTINUE DELETE DO ELSE EXTERN
	FOR FRIEND GOTO IF INLINE NEW
	PRIVATE PROTECTED PUBLIC
	REGISTER RETURN
	SIGNED SIZEOF
	/* CSTYLED */
	STATIC
	TEMPLATE THIS
	VIRTUAL VOLATILE WHILE

	/* other tokens */
	INTCON CHARCON FLOATCON IDENT STRING
	SCOPE LSHIFT RSHIFT
	SRCPOS PRAGMA SERVER_ONLY

	/* other C++ tokens */
	ELLIPSES INCR DECR ARROW LE GE EQ NE AND OR

/*
 * This use of forward referencing the types (e.g., "class String")
 * isn't really correct, as they will appear "nested" inside the
 * yacc-generated struct.  However, there doesn't seem to be any other
 * way to get this to work.
 */
%union {
	bool boolean_;
	long long_;
	double double_;
	String *string_;
	UniqueString *ustring_;
	Expr *identifier_;
	Expr *expr_;
	ExprList *exprlist_;
	CaseList *caselist_;
	CaseElement *case_;
	UnionMember *umember_;
	SourcePosition *position_;
	/* ParamTag */ unsigned long param_;
	/* Opcode */ unsigned long opcode_;
};

/*
 * An unused example of a boolean variable
 * %type <boolean_> READONLY opt_readonly
 */
%type <long_> INTCON CHARCON opt_op_attr opt_pool opt_marshal_size version
%type <double_> FLOATCON
%type <string_> STRING
%type <ustring_> IDENT SERVER_ONLY
%type <expr_>
	definition inheritance_name name export const_dcl expr type_dcl
	type simple_type constr_type declarator
	struct_type member union_type switch_type case_label
	enum_type sequence_type opt_sequence_length
	string_type opt_string_length
	except_dcl op_dcl param

%type <exprlist_>
	definition_list
	opt_inheritance inheritance_list name_list export_list
	declarator_list opt_subscript_list member_list case_label_list enum_list
	opt_params params opt_raises opt_context string_list

%type <ustring_> interface_ident module_ident

%type <caselist_>
	case_list

%type <case_>
	case

%type <umember_>
	element

%type <position_>
	SRCPOS

%type <param_>
	param_attribute

%type <opcode_>
	LSHIFT RSHIFT
	'(' '[' '.' '+' '-' '*' '/' '%' '!' '~'
	',' '=' '?' ':' '|' '^' '&' '<' '>'

/* operator precedences */
%left lowPrec
%left '{'
%right ')'
%left ','
%right '='
%right '?' ':'
%left '|'
%left '^'
%left '&'
%left LSHIFT RSHIFT
%left '+' '-'
%left '*' '/' '%'
%right UNARY SIZEOF CAST '~'
%left '(' '['
%left SCOPE
%left highPrec

%%

start:
	definition_list			{ yyparse_root = $1; }
;

definition_list:
	definition			{ $$ = definition_list($1); }
|	definition_list definition	{ $$ = append($1, $2); }
;

definition:
	type_dcl ';'			{ $$ = $1; }
| const_dcl ';'				{ $$ = $1; }
| except_dcl ';'			{ $$ = $1; }

| INTERFACE interface_ident opt_inheritance
	'{' export_list '}' ';'		{
					$$ = actual_interface($2, $3, $5);

					// reset interface_name to null
					interface_name = strdup("");
					}

| INTERFACE interface_ident ';'		{
					$$ = forward_interface($2);

					// reset interface_name to null
					interface_name = strdup("");
					}

| MODULE module_ident
	'{' definition_list '}' ';'	{
					$$ = module($2, $4);

					// reset module_name to null
					module_name = strdup("");
					}

| SRCPOS				{ $$ = srcpos($1); }
| SERVER_ONLY				{ $$ = server_only($1); }
;

/*
 * interface_ident and module_ident are separate productions, because this is
 * the only way we can store the names of modules and interfaces before they are
 * used.
 */

interface_ident:
	IDENT		{
				interface_name = strdup((char *)$1->string());
				add_interface_name(module_name,	interface_name);
				$$ = $1;
			}
;

module_ident:
	IDENT		{
				module_name = strdup((char *)$1->string());
				add_module_name(module_name);
				$$ = $1;
			}
;

opt_inheritance:
	/* empty */			{ $$ = nil; }
| ':' inheritance_list			{ $$ = $2; }
;

inheritance_list:
	inheritance_name		{ $$ = append(nil, $1); }
| inheritance_list ',' inheritance_name	{ $$ = append($1, $3); }
;

inheritance_name:
	name				{ $$ = parent_interface($1, 0, 0); }
| name '<' INTCON '>'			{ $$ = parent_interface($1, $3, 0); }
| name '<' INTCON ':' INTCON '>'	{ $$ = parent_interface($1, $3, $5); }
;

name_list:
	name				{ $$ = append(nil, $1); }
| name_list ',' name			{ $$ = append($1, $3); }
;

name:
	IDENT				{ $$ = ident($1); }
| SCOPE IDENT				{ $$ = global($2); }
| name SCOPE IDENT			{ $$ = scoped($1, $3); }
;

export_list:
	/* empty */			{ $$ = nil; }
| export_list export			{ $$ = append($1, $2); }
;

export:
	type_dcl ';'			{  $$ = $1; }
| const_dcl ';'				{  $$ = $1; }
| except_dcl ';'			{  $$ = $1; }
| op_dcl ';'				{  $$ = $1; }
;

const_dcl:
	CONST simple_type IDENT '=' expr	{ $$ = const_dcl(ident($3), $2,
						    $5); }
;

expr:
	name			{ $$ = $1; }
| FALSE				{ $$ = boolcon(false); }
| TRUE				{ $$ = boolcon(true); }
| INTCON			{ $$ = intcon($1); }
| CHARCON			{ $$ = charcon($1); }
| FLOATCON			{ $$ = floatcon($1); }
| STRING			{ $$ = stringcon($1); }
| '+' expr %prec UNARY		{ $$ = unary('+', $2); }
| '-' expr %prec UNARY		{ $$ = unary('-', $2); }
| expr '+' expr			{ $$ = binary('+', $1, $3); }
| expr '-' expr			{ $$ = binary('-', $1, $3); }
| expr '*' expr			{ $$ = binary('*', $1, $3); }
| expr '/' expr			{ $$ = binary('/', $1, $3); }
| expr '%' expr			{ $$ = binary('%', $1, $3); }
| expr LSHIFT expr		{ $$ = binary(LSHIFT, $1, $3); }
| expr RSHIFT expr		{ $$ = binary(RSHIFT, $1, $3); }
| expr '&' expr			{ $$ = binary('&', $1, $3); }
| expr '|' expr			{ $$ = binary('|', $1, $3); }
| expr '^' expr			{ $$ = binary('^', $1, $3); }
| '~' expr %prec UNARY		{ $$ = unary('~', $2); }
| '(' expr ')'			{ $$ = $2; }
;

type_dcl:
	TYPEDEF type declarator_list	{ $$ = typedef_decl($2, $3); }
| struct_type				{ $$ = $1; }
| union_type				{ $$ = $1; }
| enum_type				{ $$ = $1; }
;

type:
	simple_type		{ $$ = $1; }
| constr_type			{ $$ = $1; }
;

simple_type:
	name			{ $$ = name_type($1); }
| OBJECT			{ $$ = object_type(); }
| UNSIGNED LONG LONG		{ $$ = ulong_long_type(); }
| UNSIGNED name			{ $$ = unsigned_type($2); }
| LONG				{ $$ = long_type(); }
| UNSIGNED LONG			{ $$ = ulong_type(); }
| LONG LONG			{ $$ = long_long_type(); }
| sequence_type			{ $$ = $1; }
| string_type			{ $$ = $1; }
;

constr_type:
	struct_type		{ $$ = $1; }
| union_type			{ $$ = $1; }
| enum_type			{ $$ = $1; }
;

declarator_list:
	declarator			{ $$ = declarator_list($1); }
| declarator_list ',' declarator	{ $$ = append($1, $3); }
;

declarator:
	IDENT opt_subscript_list	{ $$ = declarator_decl(ident($1), $2); }
;

opt_subscript_list:
	/* empty */			{ $$ = nil; }
| opt_subscript_list '[' expr ']'	{ $$ = append($1, $3); }
;

struct_type:
	STRUCT IDENT '{' member_list '}'	{ $$ = struct_type(ident($2),
						    $4); }
;

member_list:
	/* empty */			{ $$ = nil; }
| member_list member			{ $$ = append($1, $2); }
;

member:
	type declarator_list ';'	{ $$ = struct_member($1, $2); }
;

union_type:
	UNION IDENT
	SWITCH '(' switch_type ')'
	'{' case_list '}'		{ $$ = union_type(ident($2), $5, $8); }
;

switch_type:
	name			{ $$ = $1; }
| enum_type			{ $$ = $1; }
| LONG				{ $$ = long_type(); }
| UNSIGNED LONG			{ $$ = ulong_type(); }
| UNSIGNED name			{ $$ = unsigned_type($2); }
| LONG LONG			{ $$ = long_long_type(); }
| UNSIGNED LONG LONG		{ $$ = ulong_long_type(); }
;

case_list:
	case			{ $$ = case_list($1); }
| case_list case		{ $$ = case_append($1, $2); }
;

case:
	case_label_list element ';'	{ $$ = case_element($1, $2); }
;

case_label_list:
	case_label			{ $$ = case_label_list($1); }
| case_label_list case_label		{ $$ = append($1, $2); }
;

case_label:
	CASE expr ':'		{ $$ = case_label($2); }
| DEFAULT ':'			{ $$ = default_label(); }
;

element:
	type declarator		{ $$ = union_member($1, $2); }
;

enum_type:
	ENUM IDENT '{' enum_list '}'	{ $$ = enum_type(ident($2), $4); }
;

enum_list:
	IDENT				{ $$ = append(nil, enumerator($1)); }
| enum_list ',' IDENT			{ $$ = append($1, enumerator($3)); }
;

sequence_type:
	SEQUENCE '<' simple_type
	opt_sequence_length '>'		{ $$ = sequence_type($3, $4); }
;

opt_sequence_length:
	/* empty */			{ $$ = nil; }
| ',' expr				{ $$ = $2; }
;

string_type:
	STRING_TOKEN opt_string_length	{ $$ = string_type($2); }
;

opt_string_length:
	/* empty */		{ $$ = nil; }
| '<' expr '>'			{ $$ = $2; }
;

except_dcl:
	EXCEPTION IDENT '{' member_list '}'	{ $$ =
						    except_dcl(ident($2), $4); }
;

op_dcl:
	opt_pool
	opt_op_attr
	simple_type
	IDENT version '(' opt_params ')'
	opt_raises opt_context	{ $$ = op($1, $2, $3, $4, $5, $7, $9, $10); }
;

opt_pool:
	/*
	 * The method definition can optionally declare that this operation
	 * belongs to a special collection of operations, such as those
	 * supporting node reconfiguration activity.
	 */
	/* empty */				{ $$ = DEFAULT_OPTION; }
|	RECONFIG				{ $$ = RECONFIG_OPTION; }
;

opt_op_attr:
	/*
	 * The values returned here are checked in resolve.cc to determine
	 * the semantic properties of the invocation.
	 */
	/* empty */				{ $$ = TWO_WAY; }

/* 	Add CHECKPOINT and set $$ to CHECK_POINT if checkpoint is used */
|	ONEWAY					{ $$ = ONE_WAY; }
|	ONEWAY NONBLOCKING UNRELIABLE 		{ $$ = ONE_WAY_NB_UR; }
|	ONEWAY UNRELIABLE NONBLOCKING 		{ $$ = ONE_WAY_NB_UR; }
|	ONEWAY UNRELIABLE			{ $$ = ONE_WAY_UR; }
;

version:
	/* empty */		{ $$ = 0; }
| '<' INTCON '>'		{ $$ = $2; }
;

opt_params:
	/* empty */		{ $$ = nil; }
| params			{ $$ = $1; }
;

params:
	param			{ $$ = param_list($1); }
| params ',' param		{ $$ = append($1, $3); }
;

/*
 * The extra rule for parameters is to allow missing names
 * to be caught as a semantic rather than syntactic error.
 */
param:
	opt_marshal_size
	param_attribute
	simple_type
	IDENT
	opt_subscript_list	{ $$ = param((invo_args::arg_size_info)$1,
				    (invo_args::arg_mode)$2, $3, ident($4), $5);
				}
|	opt_marshal_size
	param_attribute
	simple_type		{ $$ = param((invo_args::arg_size_info)$1,
				    (invo_args::arg_mode)$2, $3, nil, nil);
				}
;

/*
 * The size of some types cannot be determined at compile time.
 * Do not want to compute the size at marshal time for all arguments
 * whose size is not known at compile time.
 * Therefore we tag those arguments that will have their size computed
 * at marshal time.
 */
opt_marshal_size:
	/* empty */		{ $$ = invo_args::size_unknown; }
|	MARSHAL_SIZE		{ $$ = invo_args::size_marshal_time; }
;

/*
 * As with parameter names, we have an extra rule for a missing
 * parameter attribute so that we can give a message that is more
 * meaningful than "syntax error."
 */
param_attribute:
	/* empty */		{ $$ = invo_args::err_param; }
| IN				{ $$ = invo_args::in_param; }
| OUT				{ $$ = invo_args::out_param; }
| INOUT				{ $$ = invo_args::inout_param; }
;

opt_raises:
	/* empty */		{ $$ = nil; }
| RAISES '(' name_list ')'	{ $$ = $3; }
;

opt_context:
	/* empty */		{ $$ = nil; }
| CONTEXT '(' string_list ')'	{ $$ = $3; }
;

string_list:
	STRING			{ $$ = string_list(stringcon($1)); }
| string_list ',' STRING	{ $$ = append($1, stringcon($3)); }
;
