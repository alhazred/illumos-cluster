//
// Copyright 1998-2002 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

/*
 * Copyright (c) 1992-1993 Silicon Graphics, Inc.
 * Copyright (c) 1993 Fujitsu, Ltd.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Silicon Graphics and Fujitsu may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Silicon Graphics and Fujitsu.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

#pragma ident	"@(#)expr.cc	1.20	02/05/22 SMI"

//
// Expr - node in parse graph
//

#include "generator.h"
#include "Symbol.h"
#include "SymbolTable.h"

//
// class Expr methods
//

Expr::Expr()
{
}

Expr::~Expr()
{
}

bool
Expr::addr_type()
{
	Symbol	*symbolp = actual_type();
	if (symbolp != nil) {
		switch (symbolp->tag()) {
		case Symbol::sym_string:
		case Symbol::sym_struct:
		case Symbol::sym_sequence:
		case Symbol::sym_union:
			return (true);
		}
	}
	return (false);
}

bool
Expr::void_type()
{
	return (actual_type() == SymbolTable::the()->void_type());
}

bool
Expr::string_type()
{
	Symbol	*symbolp = actual_type();
	return (symbolp->tag() == Symbol::sym_string ||
	    symbolp == SymbolTable::the()->string_type());
}

bool
Expr::interface_type()
{
	Symbol	*symbolp = actual_type();
	return (symbolp != nil && symbolp->tag() == Symbol::sym_interface);
}

//
// actual_type - return a symbol pointer to the actual type for this expr
//
Symbol *
Expr::actual_type()
{
	Symbol	*symbolp = symbol();
	if (symbolp != nil) {
		symbolp = symbolp->actual_type();
	}
	return (symbolp);
}
