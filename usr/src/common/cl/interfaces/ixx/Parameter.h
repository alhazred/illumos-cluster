/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _PARAMETER_H
#define	_PARAMETER_H

#pragma ident	"@(#)Parameter.h	1.9	06/05/24 SMI"

/*
 * Copyright (c) 1992-1993 Silicon Graphics, Inc.
 * Copyright (c) 1993 Fujitsu, Ltd.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Silicon Graphics and Fujitsu may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Silicon Graphics and Fujitsu.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */


#include <orb/invo/invo_args.h>
#include "expr.h"
#include "list.h"

class Parameter : public ExprImpl {
public:
	Parameter(SourcePosition *pos, invo_args::arg_size_info size_type,
	    invo_args::arg_mode attr, Expr *type, Identifier *ident,
	    ExprList *subscripts);

	~Parameter();

	int			get_marshal_position()
					{ return (marshal_position_); }
	void			set_marshal_position(int position)
					{ marshal_position_ = position; }
	invo_args::arg_size_info size_type()	{ return size_type_; }
	invo_args::arg_mode	attr()		{ return attr_; }
	Expr			*type()		{ return type_; }
	Declarator		*declarator()	{ return declarator_; }

	void		resolve(Resolver *);
	bool		generate(Generator *);

	bool		generate_stub(Generator *);
	bool		generate_extern_stubs(Generator *);
	bool		generate_type_name(Generator *);

	void		process_arg_desc(Generator *g, arg_sizes &argsizes);

protected:
	void		put_array_param(Generator *);
	void		put_string_param(Generator *);
	void		put_objref_param(Generator *);
	void		put_param(Generator *);

	int				marshal_position_;
	invo_args::arg_size_info	size_type_;
	invo_args::arg_mode		attr_;
	Expr				*type_;
	Declarator			*declarator_;
};

#endif	/* _PARAMETER_H */
