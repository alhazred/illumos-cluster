//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

/*
 * Copyright (c) 1992-1993 Silicon Graphics, Inc.
 * Copyright (c) 1993 Fujitsu, Ltd.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Silicon Graphics and Fujitsu may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Silicon Graphics and Fujitsu.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

#pragma ident	"@(#)UnionMember.cc	1.8	06/05/24 SMI"

#include "generator.h"
#include "UnionMember.h"

//
// Class UnionMember Methods
//

UnionMember::UnionMember(SourcePosition *p, Expr *type, ExprList *members) :
	StructMember(p, type, members)
{
}

UnionMember::~UnionMember()
{
}

bool
UnionMember::generate(Generator *g)
{
	Expr *e = declarators_->item(0);
	if (type_->string_type())
		g->emit("%E", nil, type_);
	else {
		g->emit(g->aggr_decl(type_) ? "%E;\n%N " : "%E ", nil, type_);
		if (type_->varying() && !type_->interface_type()) {
			g->emit("*");
		}
	}
	g->emit("%E", nil, e);
	return (true);
}

bool
UnionMember::generate_marshal(Generator *g)
{
	Expr *exprp = declarators_->item(0);
	if (type_->varying() && !type_->string_type() &&
	    !type_->interface_type()) {
		g->emit("if (unionp->_u.%E != NULL) {\n%i", nil, exprp);
	}
	g->emit_put(type_, "unionp->%E()", exprp);

	if (type_->varying() && !type_->string_type() &&
	    !type_->interface_type()) {
		g->emit("%u}\n");
	}
	return (true);
}

bool
UnionMember::generate_unmarshal(Generator *g)
{
	Expr *exprp = declarators_->item(0);
	if (type_->varying() && !type_->string_type() &&
	    !type_->interface_type()) {
		g->emit("unionp->_u.%E = ", nil, exprp);
		g->emit("new %F;\n", nil, type_);

		g->emit("if (unionp->_u.%E != NULL) {\n%i", nil, exprp);

		g->emit_get(type_, "*unionp->_u.%E", exprp);

		g->emit("%u}\n");
	} else {
		g->emit_get(type_, "unionp->_u.%E", exprp);
	}
	return (true);
}

bool
UnionMember::generate_release(Generator *g)
{
	Expr *exprp = declarators_->item(0);
	if (type_->interface_type()) {
		// For objref, need to do a CORBA release explicitly as unions
		// do not declare the members are *_field
		//
		// XX Could we call InterfaceDef::put_release?
		g->emit("CORBA::release(unionp->_u.%E);\n", nil, exprp);
		g->emit_release(type_, "unionp->_u.%E", exprp);
		return (true);
	} else if (type_->string_type()) {
		// For string, need to do a delete explicitly as unions
		// do not declare the members are *_field
		g->emit("delete [] unionp->_u.%E;\n", nil, exprp);
		g->emit_release(type_, "unionp->_u.%E", exprp);
		return (true);
	} else if (type_->varying()) {
		g->emit("if (unionp->_u.%E != NULL) {\n%i", nil, exprp);
		g->emit_release(type_, "*unionp->_u.%E", exprp);
		g->emit("%u}\n");
		return (true);
	}
	return (false);
}

bool
UnionMember::generate_marshal_rollback(Generator *g)
{
	if (!ExprValueAnyobjs()) {
		// This item can not possibly contain any object references.
		return (false);
	}
	// This item can contain object references

	Expr	*exprp = declarators_->item(0);
	if (type_->interface_type()) {
		g->emit_unput(type_, "unionp->_u.%E", exprp);
	} else {
		g->emit("if (unionp->_u.%E != NULL) {\n%i", nil, exprp);
		g->emit_unput(type_, "unionp->%E()", exprp);
		g->emit("%u}\n");
	}
	return (true);
}
