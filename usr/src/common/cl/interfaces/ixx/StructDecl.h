/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _STRUCTDECL_H
#define	_STRUCTDECL_H

#pragma ident	"@(#)StructDecl.h	1.6	06/05/24 SMI"

/*
 * Copyright (c) 1992-1993 Silicon Graphics, Inc.
 * Copyright (c) 1993 Fujitsu, Ltd.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Silicon Graphics and Fujitsu may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Silicon Graphics and Fujitsu.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */


#include "expr.h"
#include "list.h"

//
// This file supports structs
//
class StructDecl : public ExprImpl {
public:
	StructDecl(SourcePosition *, Identifier	*, ExprList *);
	~StructDecl();

	void		resolve(Resolver *);
	bool		generate(Generator *);

	Identifier	*ident()	{ return ident_; }
	ExprList	*members()	{ return members_; }

	bool		generate_name(Generator *);
	bool		generate_type_name(Generator *);
	bool		generate_extern_stubs(Generator *);
	bool		generate_stub(Generator *);

protected:
	bool		compute_varying();

	void		put_member_list(Generator*,
			    void (StructDecl::*func)(Generator *, Expr *type,
				Expr *e),
			    const char *sep = nil);

	void		put_ctor_member(Generator*, Expr* type, Expr* e);
	void		put_copy_ctor_hdr_member(Generator*, Expr *type,
			    Expr* e);
	void		put_copy_ctor_body_member(Generator*, Expr *type,
			    Expr* e);
	void		put_dtor_member(Generator*, Expr *type, Expr* e);
	void		put_asg_member(Generator*, Expr *type, Expr* e);

	Identifier	*ident_;
	ExprList	*members_;
	Scope		*block_;
};

#endif	/* _STRUCTDECL_H */
