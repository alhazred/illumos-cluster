//
// Copyright 1995-2002 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

/*
 * Copyright (c) 1992-1993 Silicon Graphics, Inc.
 * Copyright (c) 1993 Fujitsu, Ltd.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Silicon Graphics and Fujitsu may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Silicon Graphics and Fujitsu.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

#pragma ident	"@(#)gen-stubs.cc	1.45	02/05/22 SMI"

//
// Generate code
//

#include "generator.h"
#include "expr.h"
#include <orb/invo/invo_args.h>
#include <assert.h>
#include "Symbol.h"
#include "ExceptDecl.h"
#include "InterfaceDef.h"
#include "UnionMember.h"

bool
ExprImpl::generate_extern_stubs(Generator *)
{
	return (false);
}

bool
ExprImpl::generate_stub(Generator *)
{
	return (false);
}

bool
ExprImpl::generate_marshal(Generator *)
{
	return (false);
}

bool
ExprImpl::generate_unmarshal(Generator *)
{
	return (false);
}

bool
ExprImpl::generate_release(Generator *)
{
	return (false);
}

bool
ExprImpl::generate_marshal_rollback(Generator *)
{
	return (false);
}

bool
IdentifierImpl::generate_extern_stubs(Generator *g)
{
	return (symbol()->expr_type()->generate_extern_stubs(g));
}

bool
IdentifierImpl::generate_stub(Generator *)
{
	return (false);
}

bool
IdentifierImpl::generate_marshal(Generator *g)
{
	return (generate(g));
}

bool
IdentifierImpl::generate_unmarshal(Generator *g)
{
	return (generate(g));
}

bool
IdentifierImpl::generate_release(Generator *g)
{
	return (generate(g));
}

bool
IdentifierImpl::generate_marshal_rollback(Generator *g)
{
	return (generate(g));
}

bool
Accessor::generate_extern_stubs(Generator *g)
{
	return (symbol()->expr_type()->generate_extern_stubs(g));
}

//
// Generate extern decls for stubs for data types.
//

bool
TypeName::generate_extern_stubs(Generator *g)
{
	return ((type_ != nil && type_->generate_extern_stubs(g)) |
	    generate_list(declarators_, &Expr::generate_extern_stubs, g));
}

bool
TypeName::generate_stub(Generator *g)
{
	return ((type_ != nil && type_->generate_stub(g)) |
	    generate_list(declarators_, &Expr::generate_stub, g));
}

//
// put_except_list - creates a NULL terminated list of exception descriptors
// for this interface or module. Generates code that loads
// exception descriptor information. This method is only called when exceptions
// are known to be present.
//
bool
ExprImpl::put_except_list(ExprList *defs, Generator *g)
{
	//
	// Create a NULL terminated list of exception descriptors
	//
	g->emit("ExceptDescriptor *_%S_excepts[] = {\n%i");
	for (ListItr(ExprList) i(*defs); i.more(); i.next()) {
		Symbol *symbolp = i.cur()->symbol();
		if (symbolp != nil && symbolp->tag() == Symbol::sym_exception) {
			g->emit("&_%_%I_type,\n",
			symbolp->except_type()->ident()->string());
		}
	}
	g->emit("NULL\n%u};\n");

	//
	// Generate code to cause the exception descriptor information
	// to be loaded.
	//
	g->emit("static OxInitExcept _%S_excepts_data(_%S_excepts);\n");

	return (true);
}

bool
Constant::generate_stub(Generator *g)
{
	bool b = false;

	//
	// Output code to initialize constant if definition in header file
	// was "nested".
	//
	Scope *scopep = symbol_->scope();
	if (scopep != nil && scopep->name != nil) {
		g->emit("const %F %:%I = ", ident_->string(), type_);
		g->emit("%X;\n", nil, value_);
		b = true;
	}
	return (b);
}

bool
CaseElement::generate_copy(bool(Expr::*func)(Generator *), Generator *g)
{
	generate_list(labels_, func, g);
	g->emit("%i");
	(element_->*func)(g);
	g->emit("break;\n%u");
	return (true);
}

bool
CaseElement::generate_extern_stubs(Generator *g)
{
	return (element_->generate_extern_stubs(g));
}

bool
CaseLabel::label(Generator *g)
{
	g->emit("case %X:\n", nil, value_);
	return (true);
}

bool
DefaultLabel::generate_marshal(Generator *g)
{
	return (label(g));
}

bool
DefaultLabel::generate_unmarshal(Generator *g)
{
	return (label(g));
}

bool
DefaultLabel::generate_release(Generator *g)
{
	return (label(g));
}

bool
DefaultLabel::generate_marshal_rollback(Generator *g)
{
	return (label(g));
}

bool
DefaultLabel::label(Generator *g)
{
	g->emit("default:\n");
	return (true);
}

bool
ExprImpl::generate_server(Generator *)
{
	return (false);
}

bool
ExprImpl::generate_receive_table(Generator *)
{
	return (false);
}

bool
ExprImpl::generate_schema(Generator *)
{
	return (false);
}
