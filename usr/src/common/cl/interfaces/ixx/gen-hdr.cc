//
// Copyright 1998-2002 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

/*
 * Copyright (c) 1992-1993 Silicon Graphics, Inc.
 * Copyright (c) 1993 Fujitsu, Ltd.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Silicon Graphics and Fujitsu may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Silicon Graphics and Fujitsu.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

#pragma ident	"@(#)gen-hdr.cc	1.39	02/06/19 SMI"

//
// Generate header file
//

#include <string.h>
#include "generator.h"
#include "types.h"
#include <orb/invo/invo_args.h>
#include <assert.h>
#include "Symbol.h"
#include "Operation.h"
#include "Parameter.h"
#include "Module.h"
#include "InterfaceDef.h"
#include "ExceptDecl.h"
#include "UnionDecl.h"
#include "UnionMember.h"
#include "SequenceDecl.h"
#include "Declarator.h"
#include "SymbolTable.h"

//
// Rewrite the whole tree.
//
void
RootExpr::rewrite(Generator *g, ListUpdater(ExprList) *)
{
	rewrite_list(g, defs_);
}

//
// This is the top level routine for generating all the output files
// after the IDL input file has been parsed (syntax checked and
// expression tree built) and resolved (symantics checked).
//
bool
RootExpr::generate(Generator *g)
{
	SourcePosition *p = g->handler()->position();
	String *s = nil;

	if (p != nil) {
		s = p->filename();
	}
	g->emit_edit_warning(s);
	g->need_sep(false);

	// Output header file to stdout.
	generate_list(defs_, &Expr::generate, g, ";\n", ";\n");

	// Output *_stub.cc to stub file (if specified on command line).
	if (g->begin_file(g->stubfile())) {
		g->emit_stub_includes(s);
		g->need_sep(false);
		bool b = generate_list(defs_, &Expr::generate_stub, g, "\n");
		// Output initialization code for exception TID schema table.
		if (defs_ != nil &&
		    SymbolTable::the()->scope()->except_index > 0) {
			if (b) {
				g->emit("\n");
			}
			put_except_list(defs_, g);
		}
		g->end_file();
	}
	// Output *_skel.cc to server file (if specified on command line).
	if (g->begin_file(g->serverfile())) {
		g->emit_server_includes(s);
		put_file(&Expr::generate_server, g);
	}
	// Output *_types.cc to schema file (if specified on command line).
	if (g->begin_file(g->schemafile())) {
		g->emit_stub_includes(s);
		put_file(&Expr::generate_schema, g);

		g->begin_file(g->schemafile());
		g->emit_schema_id_table();
		g->end_file();
	}
	g->flush();
	return (false);
}

void
RootExpr::put_file(
	// CSTYLED
	bool (Expr::*func)(Generator *),
	Generator *g)
{
	g->need_sep(false);
	generate_list(defs_, func, g, "\n");
	g->end_file();
}

bool
Accessor::generate(Generator *g)
{
	if (g->qualify()) {
		bool b = g->interface_is_ref(false);
		g->emit("%E""%?", nil, qualifier_);
		g->interface_is_ref(b);
	}
	g->emit("%P", string_, this);
	return (true);
}

//
// This supports environments where applications cannot access library data
// directly. Generate _both_ the #define for a constant and the
// static const member.  The #define has an extra leading underscore
// to avoid name conflicts.
//
bool
Constant::generate(Generator *g)
{
	String *s = ident_->string();

	Scope *scopep = symbol_->scope();
	bool nested = (scopep != nil && scopep->name != nil);
	if (nested) {
		g->emit("%#define\t%_%I %E\n", s, value_);
	}
	g->emit("static const %F %I", s, type_);
	if (!nested) {
		g->emit(" = %E;\n", nil, value_);
	} else {
		g->emit(";\n", s);
	}
	return (false);
}

bool
Unary::generate(Generator *g)
{
	g->emit("(");
	g->emit_op(op_);
	expr_->generate(g);
	g->emit(")");
	return (true);
}

bool
Binary::generate(Generator *g)
{
	g->emit("(");
	left_->generate(g);
	g->emit_op(op_);
	right_->generate(g);
	g->emit(")");
	return (true);
}

bool
TypeName::generate(Generator *g)
{
	if (seq_) {
		return (type_->generate(g));
	}
	bool ref = g->interface_is_ref(false);
	// XXX This expects generate() to not output the ";\n".
	g->emit(g->aggr_decl(type_) ? "%E;\ntypedef %N " : "typedef %E ",
	    nil, type_);
	Symbol *s = type_->symbol();
	generate_list(declarators_, &Expr::generate, g,
	    s == SymbolTable::the()->string_type() ? ", *" : ", ", ";\n");

	put_extra_types(g);

	g->interface_is_ref(ref);
	return (false);
}

//
// Generate the extra names for a typedef where the IDL type
// maps to several C++ types.  For example, if an IDL interface
// defines A, A_ptr, A_var, A_out, then "typedef A B" should define
// B, B_ptr, B_var, and B_out.
//
void
TypeName::put_extra_types(Generator *g)
{
	const char *macro = nil;
	Symbol *s = type_->actual_type();
	if (type_->string_type()) {
		for (ListItr(ExprList) i(*declarators_); i.more(); i.next()) {
			//
			// Declarators with subscripts will have marshal
			// functions declared so only handle the no
			// subscript case here.
			//
			Declarator *d = i.cur()->declarator();
			if (d->subscripts() == nil) {
				g->emit("typedef CORBA::String%r %I""%r;\n"
				    "typedef CORBA::String_out %I_out;\n",
				    d->ident()->string());
			}
		}
		return;
	}
	switch (s->tag()) {
	case Symbol::sym_interface:
		macro = "InterfaceTypedef";
		break;
	case Symbol::sym_sequence:
	case Symbol::sym_array:
	case Symbol::sym_struct:
	case Symbol::sym_union:
		if (type_->symbol()->tag() == Symbol::sym_typedef) {
			macro = "AggrTypedef";
		}
		break;
	}
	if (macro != nil) {
		for (ListItr(ExprList) i(*declarators_); i.more(); i.next()) {
			g->emit("_Ix_");
			g->emit(macro);
			g->emit("(%F, ", nil, type_);
			g->emit("%N)\n", nil, i.cur());
		}
	}
}

bool
UnsignedType::generate(Generator *g)
{
	Symbol *s = symbol_;
	if (s->tag() == Symbol::sym_typedef) {
		TypeName *t = s->type_name();
		if (t->type() == nil) {
			// builtin
			g->emit("%I", t->mapping());
			return (true);
		}
	}
	return (false);
}

bool
CaseElement::generate(Generator *g)
{
	return (element_->generate(g));
}

bool
CaseLabel::generate(Generator *g)
{
	return (value_->generate(g));
}

bool
DefaultLabel::generate(Generator *)
{
	// do not output anything
	return (true);
}

bool
EnumDecl::generate(Generator *g)
{
	g->emit("enum %I {\n%i", ident_->string());
	generate_list(members_, &Expr::generate, g, ",%b");
	g->emit("%u\n}");
	return (true);
}

bool
Enumerator::generate(Generator *g)
{
	return (ident_->generate(g));
}

//
// Still need to handle context.
//

bool
IdentifierImpl::generate(Generator *g)
{
	Symbol *s = symbol_;
	String *str = value_;
	if (s != nil) {
		switch (s->tag()) {
		case Symbol::sym_typedef:
			if (s->is_builtin_type()) {
				str = s->type_name()->mapping();
			}
			break;
		case Symbol::sym_constant:
			g->emit("%E", nil, s->constant()->value());
			return (true);
		}
	}
	g->emit("%P", str, this);
	return (true);
}

bool
boolLiteral::generate(Generator *g)
{
	g->emit_integer(value_ ? 1 : 0);
	return (true);
}

bool
IntegerLiteral::generate(Generator *g)
{
	g->emit_integer(value_);
	return (true);
}

bool
IntegerLiteral::generate_type_name(Generator *g)
{
	g->add_operation_attribute(value_);
	return (false);
}

bool
FloatLiteral::generate(Generator *g)
{
	g->emit_float(value_);
	return (true);
}

bool
StringLiteral::generate(Generator *g)
{
	g->emit("\"%I\"", value_);
	return (true);
}

bool
CharLiteral::generate(Generator *g)
{
	g->emit("\'");
	g->emit_char(value_);
	g->emit("\'");
	return (true);
}

//
// This expression is for the preprocessor line indicating a possible
// file name change.
//
bool
SrcPos::do_source(Generator *g)
{
	//
	// This possibly changes the g->is_source() flag
	// and if so, the generate() function below might be called.
	//
	return (g->set_source(position()));
}

bool
SrcPos::generate(Generator *g)
{
	SourcePosition *p = position();
	FileName name = p->filename();
	LineNumber n = p->lineno();
	if (g->is_source()) {
		if (g->need_ifndef()) {
			g->emit_ifndef(name);
			g->emit_includes();
		} else {
			g->emit_endif(name);
		}
	} else if (n == 1 && g->was_source()) {
		g->emit_include(name);
	}
	return (false); /* no semi-colon */
}

void
HashCode::generate(Generator *g)
{
	g->emit("\"");
	char buf[20];
	for (int i = sizeof (hash_) / sizeof (hash_[0]) - 1; i >= 0; i--) {
		sprintf(buf, "%08x", hash_[i]);
		g->emit_str(buf, 8);
	}
	g->emit("\"");
}

//
// Generate the "name" for an expression, which is for type expressions.
//

bool
ExprImpl::generate_name(Generator *)
{
	return (false);
}

bool
ExprImpl::generate_type_name(Generator *g)
{
	Symbol *s = actual_type();
	if (s == nil)
		return (false);

	//
	// This is kind of ugly. We would need to store the type Expr pointer
	// in IdentifierImpl and AccesorImpl when symbols are resolved
	// for typedefs, interfaces, etc where the name is a reference
	// to the type rather than storing it in the symbol.
	//
	switch (s->tag()) {
	default:
		fprintf(stderr, "IDL: Unknown parameter type passed %d\n",
			s->tag());
		assert(0);
		break;
	case Symbol::sym_array:
		s->array()->generate_type_name(g);
		break;
	case Symbol::sym_enum:
		s->enum_tag()->generate_type_name(g);
		break;
	case Symbol::sym_enum_value:
		s->enum_value_tag()->generate_type_name(g);
		break;
	case Symbol::sym_interface:
		s->interface()->generate_type_name(g);
		break;
	case Symbol::sym_sequence:
		s->sequence_type()->generate_type_name(g);
		break;
	case Symbol::sym_string:
		g->add_operation_param("string");
		break;
	case Symbol::sym_typedef:
		s->type_name()->generate_type_name(g);
		break;
	case Symbol::sym_struct:
		s->struct_tag()->generate_type_name(g);
		break;
	case Symbol::sym_union:
		s->union_tag()->generate_type_name(g);
		break;
	case Symbol::sym_exception:
		s->except_type()->generate_type_name(g);
		break;
	}
	return (false);
}

bool
IdentifierImpl::generate_name(Generator *g)
{
	return (generate(g));
}

bool
Accessor::generate_name(Generator *g)
{
	Symbol *s = this->actual_type();
	if (s != nil && s->tag() == Symbol::sym_sequence) {
		return (s->sequence_type()->generate_name(g));
	}
	g->emit("%P", string_, this);
	return (true);
}

bool
Constant::generate_name(Generator *g)
{
	return (value_->generate(g));
}

bool
TypeName::generate_name(Generator *g)
{
	return ((type_ == nil) ? generate(g) : type_->generate_name(g));
}

bool
TypeName::generate_type_name(Generator *g)
{
	if (type_ != nil)
		(void) type_->generate_type_name(g);
	else
		g->add_operation_param(str_->string());
	return (false);
}

bool
UnsignedType::generate_name(Generator *g)
{
	return (generate(g));
}

bool
CaseLabel::generate_name(Generator *g)
{
	return (value_->generate(g));
}

bool
EnumDecl::generate_name(Generator *g)
{
	g->emit("%I", ident_->string());
	return (true);
}

bool
EnumDecl::generate_type_name(Generator *g)
{
	g->add_operation_param("enum");
	generate_list(members_, &Expr::generate_type_name, g);
	return (false);
}

bool
Enumerator::generate_name(Generator *g)
{
	return (generate(g));
}

bool
Enumerator::generate_type_name(Generator *g)
{
	g->add_operation_param("e-val");
	g->add_operation_attribute(value_);
	return (false);
}
