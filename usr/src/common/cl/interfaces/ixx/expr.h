/*
 *  Copyright 1998-2002 Sun Microsystems, Inc.  All rights reserved.
 *  Use is subject to license terms.
 *
 */

#ifndef _EXPR_H
#define	_EXPR_H

#pragma ident	"@(#)expr.h	1.31	02/09/11 SMI"

/*
 * Copyright (c) 1992-1993 Silicon Graphics, Inc.
 * Copyright (c) 1993 Fujitsu, Ltd.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Silicon Graphics and Fujitsu may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Silicon Graphics and Fujitsu.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

/*
 * Expr - node in parse graph.
 */


#include <stdio.h>
#include "list.h"
#include "types.h"
#include <orb/invo/invo_args.h>

class Declarator;
class ErrorHandler;
class Generator;
class Resolver;
class SourcePosition;
class String;
class StringList;
class Symbol;
class ListUpdater(ExprList);

//
// Information from the command line arguments.
//
struct ConfigInfo {
	const char	*filename;	// name of IDL file being compiled
	const char	*stubfile;	// client side stubs output file
	const char	*serverfile;	// server side skels output file
	const char	*schemafile;	// interface descriptor and type IDs
	const char	*tidfile;	// type ID output file
	const char	*inclpath;
	const char	*inclext;
	StringList	*includes;
	StringList	*stub_includes;
	StringList	*server_includes;
	const char	*prefix;
	bool		pass_env;	// pass environment variable for methods
	bool		copy_outs;
};

//
// expr_descriptor - describes the expression value and scope
//
class expr_descriptor {
public:
	expr_descriptor() :
		any_objs(false),
		size(0),
		objtref(0),
		unknown(0),
		numstr(0)
		{ strings[0] = strings[1] = strings[2] = ""; }

	//
	// True when the expr can contain an object reference.
	// For example, a union that has a case that is an object reference
	// would have this field set to true.
	//
	bool	any_objs;

	long	size;		// known size
	long	objtref;	// number of object references
	long	unknown;   	// indicates number of unknowns

	//
	// Scoping information
	//
	// This indicates number of scopes + 1.
	// For example, numstr for module::interface::element is 2 + 1 = 3.
	//
	int	numstr;
	char	*strings[3];
};

class Expr {
protected:
	Expr();
public:
	virtual	~Expr();

	expr_descriptor		ExprValue;

	bool	ExprValueAnyobjs()	{ return (ExprValue.any_objs); }
	long	ExprValueSize()		{ return (ExprValue.size); }
	long	ExprValueObjtref()	{ return (ExprValue.objtref); }
	long	ExprValueUnknown()	{ return (ExprValue.unknown); }
	void	ExprValueSize(long n)	{ ExprValue.size = n; }
	void	ExprValueObjtref(long n) { ExprValue.objtref = n; }
	void	ExprValueUnknown(long n) { ExprValue.unknown = n; }

	virtual long	version()	{ return (0); }
	virtual String	*string()	{ return (nil); }

	bool		addr_type();
	bool		void_type();
	bool		string_type();
	bool		interface_type();

	virtual Symbol		*actual_type();
	virtual Symbol		*symbol() = 0;
	virtual Declarator	*declarator() = 0;

	virtual bool	varying() = 0;

	// Resolve pass builds the symbol table and performs semantic checks.
	virtual void	resolve(Resolver *) = 0;

	// Rewrite pass converts expression tree into the versioned hierarchy.
	virtual void	rewrite(Generator *, ListUpdater(ExprList) *) = 0;

	//
	// Return true if code for this object should be output
	// (i.e., the IDL source line is not from an included file).
	//
	virtual bool	do_source(Generator *) = 0;

	// Output the code for the header file.
	virtual bool	generate(Generator *) = 0;

	// Output the name (identifier).
	virtual bool	generate_name(Generator *) = 0;

	//
	// Output an external declaration for marshal/unmarshal routines
	// (this is needed if the type was defined in an included IDL
	// file or is a forward reference).
	//
	virtual bool	generate_extern_stubs(Generator *) = 0;

	// Output code for the client side (stub).
	virtual bool	generate_stub(Generator *) = 0;

	//
	// The next 4 methods generate marshal time support functions for a type
	//
	virtual bool	generate_marshal(Generator *) = 0;
	virtual bool	generate_unmarshal(Generator *) = 0;
	virtual bool	generate_release(Generator *) = 0;
	virtual bool	generate_marshal_rollback(Generator *) = 0;

	// Output code for the server side (skel).
	virtual bool	generate_server(Generator *) = 0;

	// Output the receiver function name for the receiver table.
	virtual bool	generate_receive_table(Generator *) = 0;

	//
	// Output the code to define the interface descriptor and type ID
	// data structures (schema file).
	//
	virtual bool	generate_schema(Generator *) = 0;

	// Output the string for the type ID (MD5 hash).
	virtual bool	generate_type_name(Generator *) = 0;

	// Used for error handling.
	virtual SourcePosition	*position() = 0;
	virtual void	set_source_position(ErrorHandler *) = 0;
};

typedef Expr Identifier;

#endif	/* _EXPR_H */
