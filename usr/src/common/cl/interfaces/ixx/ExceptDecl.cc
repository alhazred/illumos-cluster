//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

/*
 * Copyright (c) 1992-1993 Silicon Graphics, Inc.
 * Copyright (c) 1993 Fujitsu, Ltd.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Silicon Graphics and Fujitsu may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Silicon Graphics and Fujitsu.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

#pragma ident	"@(#)ExceptDecl.cc	1.11	06/05/24 SMI"

//
// Class Module Methods
//

#include <string.h>
#include "generator.h"
#include "ExceptDecl.h"
#include "main.h"
#include "Resolver.h"
#include "Symbol.h"
#include "StructMember.h"


ExceptDecl::ExceptDecl(SourcePosition *p, Identifier *i, ExprList *members) :
	StructDecl(p, i, members),
	index_(0)
{
}

ExceptDecl::~ExceptDecl()
{
}

//
// Note: this should be similar to StructDecl::put_member_list().
//
void
ExceptDecl::put_init_list(Generator *g, bool decl)
{
	bool need_sep = false;
	for (ListItr(ExprList) i(*members_); i.more(); i.next()) {
		StructMember *s = i.cur()->symbol()->struct_member();
		if (s == nil || s->declarators() == nil)
			continue;
		for (ListItr(ExprList) d(*s->declarators()); d.more();
		    d.next()) {
			Expr *m = d.cur();
			if (need_sep) {
				g->emit(", ");
			}

			if (decl) {
				//
				// This section produces a declaration
				// for an exception member.
				//
				// If the member is of type
				// 'string', we want the
				// corresponding ctor to have arg
				// of type const char *. This is to
				// avoid memory leak in temporary
				// objects instantited when the
				// exception is created by passing
				// it a const char *.
				//
				if (s->type()->string_type()) {
					g->emit("const char *");
				} else {
					g->emit("%F ", nil, s->type());
				}
				g->emit("_%N", nil, m);
			} else {
				//
				// This produces code initializing
				// an exception member.
				//
				g->emit("%N(_%N)", nil, m);
			}
			need_sep = true;
		}
	}
}

//
// Generate the Typeid for this specific exception.
//
void
ExceptDecl::generate_typeid(Generator *g)
{
	// Build the typeid string for this exception.
	g->start_exception("++", this, ident_);

	// Add members to typeid to be able to detect changes.
	(void) generate_list(members_, &Expr::generate_type_name, g);

	// Run MD5 on the string.
	char *s = g->get_type_string();
	unsigned char digest[16];
	MDString(s, digest);

	// Output the object type id.
	g->emit_tid(digest, ident_->string());

	if (g->tidfile() != nil) {
		fprintf(g->tidfile(), "TID: %s\n", s);
		fprintf(g->tidfile(), "MD5: ");
		for (int i = 0; i < 16; i++)
			fprintf(g->tidfile(), "%02x", digest[i]);
		fprintf(g->tidfile(), "\n");
	}
	if (type_debug == true) {
		int i;

		fprintf(stderr, "TID digest input: %s\n", s);
		fprintf(stderr, "MD5: ");
		for (i = 0; i < 16; i++)
			fprintf(stderr, "%02x", digest[i]);
		fprintf(stderr, "\n");
	}
	free(s);
}

//
// Generate type name for Operations.
//
bool
ExceptDecl::generate_type_name(Generator *g)
{
	return (generate_list(members_, &Expr::generate_type_name, g));
}

//
// Generate an exception declaration in the header file.
//
bool
ExceptDecl::generate(Generator *g)
{
	String *except_namep = ident_->string();
	g->emit("class %I : public CORBA::UserException {\npublic:\n%i",
	    except_namep);

	// Generate the exception minor number.
	g->emit("enum { _index = ");
	g->emit_integer(index_);
	g->emit(" };\n\n");

	// Generate default constructor.
	g->emit("%I();\n", except_namep);

	//
	// When the size is variable, cannot rely on default methods.
	//
	if (varying()) {
		// Generate copy constructor.
		g->emit("%I(const %I &);\n", except_namep);

		// Generate destructor.
		g->emit("~%I();\n\n", except_namep);

		// Generate assignment operator.
		g->emit("%I &operator = (const %I &);\n\n", except_namep);
	}

	g->emit("_Ix_ExceptCast(%I)\n\n", except_namep);

	// Generate marshal method.
	g->emit("void _put(%< &) const;\n");

	// Generate unmarshal method.
	g->emit("static CORBA::UserException *_get(%< &);\n", except_namep);

	// Generate the members of the exception structure.
	if (members_ != nil) {
		// A separator between methods and data members.
		g->emit("\n// Data members\n");
		generate_list(members_, &Expr::generate, g, ";\n", ";\n");

		//
		// Generate constructor with arguments.
		// This has to come here rather than with the other
		// contructors since the types for the data members may get
		// defined above (struct, union, array, or sequence).
		//
		g->emit("\n%I(", except_namep);

		//
		// Call put_init_list() rather than generate_list() because
		// put_init_list() knows to use 'const char *' for members of
		// type string in constructor.
		//
		put_init_list(g, true);
		g->emit(");\n", except_namep);
	}

	g->emit("%u};\n");
	return (false);
}

//
// Generate any functions needed to support this type
// at marshal time, such as put, get, etc.
//
bool
ExceptDecl::generate_stub(Generator *g)
{
	String	*except_namep = ident_->string();
	bool	vary = varying();

	g->enter_scope(block_);
	generate_list(members_, &Expr::generate_stub, g, "\n");
	g->leave_scope();

	//
	// Output extern declarations for the elements that need them.
	// (i.e., those not output from the above tree walk)
	//
	if (generate_list(members_, &Expr::generate_extern_stubs, g)) {
		g->emit("\n");
	}

	// Generate a type id for this exception.
	generate_typeid(g);

	// Generate a name for the exception.
	g->emit("static const char *_%_%I_name = \"%Q\";\n\n", except_namep);

	// Generate the constructor with no arguments.
	g->emit("%Q::%I() :%i\n", except_namep);
	g->emit("UserException(");
	g->emit("_index, ");
	g->emit("_Ix__tid(%_%I), ", except_namep);
	g->emit("_%_%I_name)", except_namep);
	if (vary) {
		put_member_list(g, &ExceptDecl::put_ctor_member);
	}
	g->emit("%u\n{\n}\n\n");

	// Generate the constructor with arguments.
	if (members_ != nil) {
		g->emit("%Q::%I(", except_namep);
		put_init_list(g, true);
		g->emit(") :\n%i");
		g->emit("UserException(_index, _Ix__tid(%_%I), _%_%I_name),\n",
		    except_namep);
		put_init_list(g, false);
		g->emit("\n%u{\n}\n\n");
	}

	if (vary) {
		g->need_sep(true);

		// Generate copy constructor.
		g->emit("%Q::%I(const %I &__s) %i", except_namep);
		put_member_list(g, &ExceptDecl::put_copy_ctor_hdr_member);
		g->emit(",\nUserException(__s) ");
		g->emit("%u\n{\n%i");
		put_member_list(g, &ExceptDecl::put_copy_ctor_body_member);
		g->emit("%u}\n\n");

		// Generate destructor.
		g->emit("%Q::~%I()\n{", except_namep);
		g->emit("\n%i");
		put_member_list(g, &ExceptDecl::put_dtor_member);
		g->emit("%u}\n\n", except_namep);

		// Generate assignment operator.
		g->emit("%Q &\n"
		    "%Q::operator = (const %I &__s)\n"
		    "{\n%i", except_namep);
		put_member_list(g, &ExceptDecl::put_asg_member);
		g->emit("return (*this);\n");
		g->emit("%u}\n\n");
	}

	//
	// Generate the function that determines whether a given exception
	// is of this type.
	//
	g->emit("void *\n"
	    "%Q::_check_cast(CORBA::Exception *exceptp)\n"
	    "{\n%i", except_namep);
	g->emit("return (_try_cast(exceptp, _Ix__tid(%_%I)));\n"
	    "%u}\n\n", except_namep);

	g->emit("_Ix_ExceptDescriptor(%_%I,\n%i"
	    "\"%Q\",\n"
	    "%Q\n"
	    "%u);\n\n", except_namep);

	g->emit("void\n"
	    "%Q::_put(%< &_b) const\n"
	    "{\n%i", except_namep);
	g->emit("CORBA::UserException::_put(_b);\n", except_namep);
	if (members_ != nil) {
		g->emit("%F *structp = (%F *)this;\n", nil, this);
		generate_list(members_, &Expr::generate_marshal, g);
	}
	g->emit("%u}\n\n");

	//
	// The output code is a little strange because it uses
	// a reference temporary and then returns its address.
	// This usage is necessary because the member unmarshal code
	// assumes _this is an object rather than a pointer.
	//
	g->emit("CORBA::UserException *\n"
	    "%Q::_get(%< &", except_namep);
	if (members_ == nil) {
		g->emit(")\n"
		    "{\n%i"
		    "return (new %I);\n"
		    "%u}\n", except_namep);
	} else {
		g->emit("_b)\n"
		    "{\n%i"
		    "%F *structp = new %I;\n"
		    "if (structp != 0) {%i\n", except_namep, this);
		generate_list(members_, &Expr::generate_unmarshal, g);
		g->emit("%u}\n"
		    "return (structp);\n"
		    "%u}\n");
	}
	return (true);
}

void
ExceptDecl::resolve(Resolver *r)
{
	StructDecl::resolve(r);
	symbol_->except_type(this);
	Scope *s = r->scope();
	s->except_index += 1;
	index_ = s->except_index;
}
