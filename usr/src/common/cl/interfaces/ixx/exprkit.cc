//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

/*
 * Copyright (c) 1992-1993 Silicon Graphics, Inc.
 * Copyright (c) 1993 Fujitsu, Ltd.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Silicon Graphics and Fujitsu may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Silicon Graphics and Fujitsu.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

#pragma ident	"@(#)exprkit.cc	1.7	06/05/24 SMI"

#include "generator.h"
#include "err.h"
#include "tokendefs.h"		// to get tokens like LSHIFT, RSHIFT etc.
#include "exprkit.h"
#include "expr-impl.h"
#include "list.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <orb/invo/invo_args.h>
#include "Symbol.h"
#include "Operation.h"
#include "Parameter.h"
#include "InterfaceDef.h"
#include "Module.h"
#include "ExceptDecl.h"
#include "StructDecl.h"
#include "StructMember.h"
#include "UnionDecl.h"
#include "UnionMember.h"
#include "SequenceDecl.h"
#include "StringDecl.h"
#include "Declarator.h"
#include "SymbolTable.h"

extern char	*module_name;
extern char	*interface_name;

// for parser
extern ExprKit	*yyparse_exprkit;

//
// debug_size - when true output debug information about size calculation
//
bool	debug_size = false;

//
// The symbol table consists of a list of modules.
// Each module has a list of interfaces, that belong to that particular module.
// Each interface has a list of elements that belong to that
// particular interface.
//

struct element_list {
	Expr			*element;
	struct element_list	*next;
};

struct interface_list {
	char			*interface_name;

	// start_element_list  - the root pointer to the list of elements
	struct element_list	*start_element_list;

	struct interface_list	*next;
};

struct module_list {
	char			*module_name;

	// start_interface_list - root pointer to the list of interfaces
	struct interface_list	*start_interface_list;

	struct module_list	*next;
};

// root pointer to the symbol table
struct module_list		*start_module_list;

implementPtrList(ExprList, Expr)
implementPtrList(CaseList, CaseElement)

//
// Support functions that do not belong to a class
//

//
// This function adds the element "element_name" to the list of elements, for
// the specifed "module_name" and "interface_name"
//
void
add_element_name(char *mod_name, char *iface_name, Expr *element_name)
{
	struct module_list	*l_mod;
	struct interface_list	*l_int;
	struct element_list	*l_elmt;

	// Make p point to the correct module object
	for (l_mod = start_module_list; l_mod != NULL; l_mod = l_mod->next) {
		if (strcmp(l_mod->module_name, mod_name) == 0) {
			break;
		}
	}
	if (l_mod == NULL) {
		fprintf(stderr, "Module name not found!!!!!!\n");
		exit(1);
	}

	// Make l_int point to the correct interface object
	for (l_int = l_mod->start_interface_list; l_int != NULL;
		l_int = l_int->next) {
		if (strcmp(l_int->interface_name, iface_name) == 0) {
			break;
		}
	}
	if (l_int == NULL) {
		fprintf(stderr, "Module name not found!!!!!!\n");
		exit(1);
	}

	// Create a new element member, l_elmt
	l_elmt = new(element_list);
	l_elmt->element = element_name;
	l_elmt->next = NULL;

	// Add this element to the specified module
	l_elmt->next = l_int->start_element_list;
	l_int->start_element_list = l_elmt;
}

//
// This function adds the name of the interface to the list of interfaces for
// the module specified by "module_name"
//
void
add_interface_name(char *mod_name, char *iface_name)
{
	struct module_list	*l_mod;
	struct interface_list	*l_int;

	// Make l_mod point to the correct module object
	for (l_mod = start_module_list; l_mod != NULL; l_mod = l_mod->next) {
		if (strcmp(l_mod->module_name, mod_name) == 0) {
			break;
		}
	}
	if (l_mod == NULL) {
		fprintf(stderr, "Module name not found!!!!!!\n");
		exit(1);
	}

	//
	// Forward declarations of interfaces are allowed.
	// Determine whether this interface has already been declared.
	//
	for (l_int = l_mod->start_interface_list;
	    l_int != NULL;
	    l_int = l_int->next) {
		if (strcmp(l_int->interface_name, iface_name) == 0) {
			//
			// Interface name already declared.
			//
			return;
		}
	}

	// Create a new interface member, l_int
	l_int = new(interface_list);
	l_int->interface_name = strdup(iface_name);
	l_int->start_element_list = NULL;
	l_int->next = NULL;


	// Add this interface to the specified module
	l_int->next = l_mod->start_interface_list;
	l_mod->start_interface_list = l_int;

	//
	// An interface name can be used as an object reference.
	// So enter a "type" object with the name of the interface
	// in the current module scope (outside all interfaces).
	//
	// An interface name of "" acts a place holder for elements
	// defined within the module and outside of an interface.
	//
	if (strcmp(iface_name, "") != 0) {

		Expr	*ptype = yyparse_exprkit->declarator_decl(
		    yyparse_exprkit->ident(new String(strdup(iface_name))),
		    (ExprList *) nil);

		ptype->ExprValue.any_objs = true;

		// A declarator has a size of 1 for an interface name
		ptype->ExprValueSize(0);

		ptype->ExprValueObjtref(1);

		add_element_name(mod_name, "", ptype);
	}
}

//
// this function adds the name of the module to the symbol table
//
void
add_module_name(char *mod_name)
{
	struct module_list *l_mod;

	// First, create a new member, l_mod
	l_mod = new(module_list);
	l_mod->module_name = strdup(mod_name);
	l_mod->start_interface_list = NULL;
	l_mod->next = NULL;

	// Add this member to the beginning of the list
	l_mod->next = start_module_list;
	start_module_list = l_mod;

	//
	// An empty interface is used to store types which do not
	// belong to any particular interface
	//
	add_interface_name(mod_name, "");

	//
	// A module name can be used as an object reference. So
	// enter a "type" object with the name of the module in
	// the global scope (outside all modules).
	//
	if (strcmp(mod_name, "") != 0) {
		Expr	*ptype = yyparse_exprkit->declarator_decl(
		    yyparse_exprkit->ident(new String(strdup(mod_name))),
		    (ExprList *)nil);

		ptype->ExprValue.any_objs = true;

		// A declarator has a size of 1 for a module name
		ptype->ExprValueSize(0);

		ptype->ExprValueObjtref(1);

		add_element_name("", "", ptype);
	}
}

//
// print_module_list - prints the contents of the symbol table.
//
void
print_module_list()
{
	struct module_list	*l_mod;
	struct interface_list	*l_int;
	struct element_list	*l_elmt;
	Declarator		*declaratorp;

	for (l_mod = start_module_list; l_mod != NULL; l_mod = l_mod->next) {
		if (strcmp(l_mod->module_name, "") == 0) {
			fprintf(stderr, "Module Name List\n");
		} else {
			fprintf(stderr, "Module Name = %s\n",
			    l_mod->module_name);
		}

		for (l_int = l_mod->start_interface_list;
		    l_int != NULL;
		    l_int = l_int->next) {
			if (strcmp(l_int->interface_name, "") == 0) {
				fprintf(stderr, "  Element List\n");
			} else {
				fprintf(stderr,
				    "  Interface Name = %s\n",
				    l_int->interface_name);
			}
			for (l_elmt = l_int->start_element_list;
			    l_elmt != NULL;
			    l_elmt = l_elmt->next) {
				declaratorp = (Declarator *)l_elmt->element;

				fprintf(stderr,
				    "    %s\t%d\t%d\t%d\t%s\n",
				    (l_elmt->element->ExprValue.any_objs ?
				    "t" : "f"),
				    l_elmt->element->ExprValueSize(),
				    l_elmt->element->ExprValueObjtref(),
				    l_elmt->element->ExprValueUnknown(),
				    declaratorp->ident()->string()->string());
			}
		}
	}
}

//
// return_info - finds an Expr object already defined for the specified:
//	module "module_name",
//	interface "interface_name", and
//	symbol name.
// NULL is returned when no match is found.
//
Expr *
return_info(char *mod_name, char *iface_name, char *token)
{
	struct module_list	*l_mod;
	struct interface_list	*l_int;
	struct element_list	*l_elmt;
	Declarator		*declaratorp;

	// Make l_mod point to the correct module object
	for (l_mod = start_module_list; l_mod != NULL; l_mod = l_mod->next) {
		if (strcmp(l_mod->module_name, mod_name) == 0) {
			break;
		}
	}
	if (l_mod == NULL) {
		return (NULL);
	}

	// Make l_int point to the correct interface object
	for (l_int = l_mod->start_interface_list;
	    l_int != NULL;
	    l_int = l_int->next) {
		if (strcmp(l_int->interface_name, iface_name) == 0) {
			break;
		}
	}
	if (l_int == NULL) {
		return (NULL);
	}

	for (l_elmt = l_int->start_element_list;
	    l_elmt != NULL;
	    l_elmt = l_elmt->next) {
		declaratorp = (Declarator *)l_elmt->element;
		if (strcmp(token,
		    (char *)declaratorp->ident()->string()->string()) == 0) {
			//
			// Found a matching definition.
			// Return this Expr object
			//
			return ((Expr *)declaratorp);
		}
	}
	return (NULL);
}

Expr *
name_decl(Expr *name)
{
	Identifier	*identifierp = (Identifier *)name;

	if (strcmp((char *)identifierp->string()->string(), "void") == 0) {
		// This has no value and hence no size

	} else if (strcmp((char *)identifierp->string()->string(),
	    "boolean") == 0) {
		name->ExprValueSize(1);

	} else if (strcmp((char *)identifierp->string()->string(),
	    "char") == 0) {
		name->ExprValueSize(1);

	} else if (strcmp((char *)identifierp->string()->string(),
	    "octet") == 0) {
		name->ExprValueSize(1);

	} else if (strcmp((char *)identifierp->string()->string(),
	    "short") == 0) {
		name->ExprValueSize(2);

	} else if (strcmp((char *)identifierp->string()->string(),
	    "unsigned_short") == 0) {
		name->ExprValueSize(2);

	} else if (strcmp((char *)identifierp->string()->string(),
	    "long") == 0) {
		name->ExprValueSize(4);

	} else if (strcmp((char *)identifierp->string()->string(),
	    "unsigned_long") == 0) {
		name->ExprValueSize(4);

	} else if (strcmp((char *)identifierp->string()->string(),
	    "longlong") == 0) {
		name->ExprValueSize(8);

	} else if (strcmp((char *)identifierp->string()->string(),
	    "unsigned_longlong") == 0) {
		name->ExprValueSize(8);

	} else if (strcmp((char *)identifierp->string()->string(),
	    "float") == 0) {
		name->ExprValueSize(4);

	} else if (strcmp((char *)identifierp->string()->string(),
	    "double") == 0) {
		name->ExprValueSize(8);

	} else if (strcmp((char *)identifierp->string()->string(),
	    "string") == 0) {
		//
		// Just set it to unknown. Should not reach here.
		// Strings reduce to a string_type
		//
		name->ExprValueUnknown(1);

	} else if (strcmp((char *)identifierp->string()->string(),
	    "true") == 0) {
		//
		// Set it to unknown. The compiler will catch it
		// at a later stage. This is a syntax error.
		//
		name->ExprValueUnknown(1);

	} else if (strcmp((char *)identifierp->string()->string(),
	    "false") == 0) {
		//
		// Set it unknown. The compiler will catch it
		// later. This is a syntax error.
		//
		name->ExprValueUnknown(1);

	} else if (strcmp((char *)identifierp->string()->string(),
	    "oneway") == 0) {
		//
		// Set it unknown. The compiler will catch it at
		// later. This is a syntax error.
		//
		name->ExprValueUnknown(1);

	} else {
		Expr	*typevalue;
		char	**strings = (char **)name->ExprValue.strings;

		//
		// This is where the size of scoped types are determined.
		//
		// name->ExprValue.numstr contains the number of scopes - 1.
		// If number of strings is 3,
		// then the declaration is of the form
		//	"module_name::interface_name::type_name".
		// string[0] contains module name,
		// string[1] contains interface name, and
		// string[2] contains type_name.
		//
		switch (name->ExprValue.numstr) {

		case (0):
			fprintf(stderr, "Too few scopes to handle !\n");
			assert(0);
			break;

		case (1):
			//
			// Check if the type is defined in the current interface
			//
			typevalue = return_info(module_name, interface_name,
			    (char *)identifierp->string()->string());
			if (typevalue != NULL) {
				break;
			}
			//
			// We have to check if the type has been declared
			// within the module, but outside any interface
			//
			typevalue = return_info(module_name, "",
			    (char *)identifierp->string()->string());
			if (typevalue != NULL) {
				break;
			}
			//
			// We have to check if the type has been declared
			// outside all modules and interfaces
			//
			typevalue = return_info("", "",
			    (char *)identifierp->string()->string());
			break;

		case (2):
			// Check if the name is interface_name::type_name,
			// where interfaca_name is defined within the
			// current module.
			//
			typevalue = return_info(module_name,
					strings[0], strings[1]);
			if (typevalue != NULL) {
				break;
			}
			//
			// Check if the name is module_name::type_name
			//
			typevalue = return_info(strings[0], "", strings[1]);
			if (typevalue != NULL) {
				break;
			}
			//
			// Check if the name is interface_name::type_name,
			// where interface_name is defined globally.
			//
			typevalue = return_info("", strings[0], strings[1]);
			break;

		case (3):
			typevalue = return_info(strings[0], strings[1],
			    strings[2]);
			break;

		default:
			fprintf(stderr, "Too many scopes to handle !\n");
			assert(0);
		}

		if (typevalue != NULL) {
			name->ExprValue.any_objs =
			    typevalue->ExprValue.any_objs;
			name->ExprValueSize(typevalue->ExprValueSize());
			name->ExprValueObjtref(typevalue->ExprValueObjtref());
			name->ExprValueUnknown(typevalue->ExprValueUnknown());

			assert(!(name->ExprValueObjtref() != 0 &&
			    !name->ExprValue.any_objs));
		} else {
			name->ExprValueUnknown(1);
		}
	}
	return (name);
}

//
// class ExprKit
//

ExprKit::ExprKit(ErrorHandler *h)
{
	yyparse_exprkit = this;
	impl_ = new ExprKitImpl(h);
}

ExprKit::~ExprKit()
{
	yyparse_exprkit = nil;
}

//
// Create generator.
//
Generator *
ExprKit::generator(const ConfigInfo &info)
{
	return (new Generator(impl_->handler(), info));
}

Expr *
ExprKit::root(ExprList *defs)
{
	return (new RootExpr(impl_->pos(), defs));
}

//
// actual_interface - create an InterfaceDef object for the interface.
//
Expr *
ExprKit::actual_interface(Identifier *ident, ExprList *supertypes,
    ExprList *defs)
{
	InterfaceDef	*e =
	    new InterfaceDef(impl_->pos(), false, false, ident, supertypes,
		defs);
	e->ExprValue.any_objs = true;
	e->ExprValueObjtref(1);
	return (e);
}

//
// forward_interface - create a dummy InterfaceDef object for the forward
// declaration of an interface.
//
Expr *
ExprKit::forward_interface(Identifier *ident)
{
	InterfaceDef	*e =
	    new InterfaceDef(impl_->pos(), true, false, ident, nil, nil);
	e->ExprValue.any_objs = true;
	e->ExprValueObjtref(1);
	return (e);
}

//
// server_only - create a dummy InterfaceDef object similar to a forward
// declaration of an interface but marked as a "server only" interface.
//
Expr *
ExprKit::server_only(Identifier *ident)
{
	InterfaceDef	*e =
	    new InterfaceDef(impl_->pos(), true, true, ident, nil, nil);
	e->ExprValue.any_objs = true;
	e->ExprValueObjtref(1);
	return (e);
}

//
// parent_interface - create an ParentImpl object for the
// definition of an inherited interface.
//
Expr *
ExprKit::parent_interface(Expr *name, long version, long parent_version)
{
	return (new ParentImpl(impl_->pos(), name, version, parent_version));
}

Expr *
ExprKit::module(Identifier *ident, ExprList *defs)
{
	return (new Module(impl_->pos(), ident, defs));
}

Expr *
ExprKit::scoped(Expr *scope, String *s)
{
	return (new Accessor(impl_->pos(), scope, s));
}

Expr *
ExprKit::constant(Identifier *ident, Expr *type, Expr *value)
{
	return (new Constant(impl_->pos(), ident, type, value));
}

Expr *
ExprKit::unary(Opcode op, Expr *expr)
{
	long value;

	switch (op) {
	case '+':
		value = 0 + expr->ExprValueSize();
		break;

	case '-':
		value = 0 - expr->ExprValueSize();
		break;

	case '~':
		value = expr->ExprValueSize();
		value = ~value;
		break;

	default:
		// Should never reach here
		assert(0);
		break;
	}

	Unary *e = new Unary(impl_->pos(), op, expr);
	e->ExprValueSize(value);
	return (e);
}

Expr *
ExprKit::binary(Opcode op, Expr *left, Expr *right)
{
	long value;

	switch (op) {
	case '+':
		value = left->ExprValueSize() + right->ExprValueSize();
		break;

	case '-':
		value = left->ExprValueSize() - right->ExprValueSize();
		break;

	case '*':
		value = left->ExprValueSize() * right->ExprValueSize();
		break;

	case '/':
		value = left->ExprValueSize() / right->ExprValueSize();
		break;

	case '%':
		value = left->ExprValueSize() % right->ExprValueSize();
		break;

	case LSHIFT:
		value = left->ExprValueSize() << right->ExprValueSize();
		break;

	case RSHIFT:
		value = left->ExprValueSize() >> right->ExprValueSize();
		break;

	case '&':
		value = left->ExprValueSize() & right->ExprValueSize();
		break;

	case '|':
		value = left->ExprValueSize() | right->ExprValueSize();
		break;

	case '^':
		value = left->ExprValueSize() ^ right->ExprValueSize();
		break;

	default:
		// Cannot determine value
		assert(0);
		break;
	}

	Binary	*e = new Binary(impl_->pos(), op, left, right);
	e->ExprValueSize(value);
	return (e);
}

Expr *
ExprKit::type_name(Expr *type, ExprList *declarator_list)
{
	TypeName	*e = new TypeName(impl_->pos(), type, declarator_list);

	e->ExprValueSize(type->ExprValueSize());

	for (ListItr(ExprList) i(*declarator_list); i.more(); i.next()) {
		i.cur()->ExprValue.any_objs = type->ExprValue.any_objs;
		i.cur()->ExprValueSize(type->ExprValueSize());
		i.cur()->ExprValueObjtref(type->ExprValueObjtref());
		i.cur()->ExprValueUnknown(type->ExprValueUnknown());
		add_element_name(module_name, interface_name, i.cur());
	}
	return (e);
}

Expr *
ExprKit::unsigned_type(Expr *type)
{
	UnsignedType	*e = new UnsignedType(impl_->pos(), type);
	e->ExprValueSize(type->ExprValueSize());

	//
	// An unsigned type should only be one of the
	// various sized integer types.
	//
	assert(type->ExprValueSize() != 0);
	assert(type->ExprValueObjtref() == 0);
	assert(type->ExprValueUnknown() == 0);

	return (e);
}

//
// declarator - process the declaration of an item,
// and calculate the number of elements,
// which will be one unless this item is an array.
//
Expr *
ExprKit::declarator_decl(Identifier *ident, ExprList *subscripts)
{
	Declarator	*e = new Declarator(impl_->pos(), ident, subscripts);

	//
	// The ExprValue of a declarator contains the size required for
	// the subscripts. The size of a declaration of type
	//	"long x[10][20]"	is 10 * 20 = 200.
	// The size of a declaration which has no subscripts is 1.
	// In effect this calculates the number of elements and not the
	// size in bytes.
	//
	long	value = 1;
	if (subscripts != nil) {
		for (ListItr(ExprList) i(*subscripts); i.more(); i.next()) {
			value *= i.cur()->ExprValueSize();
		}
	}
	e->ExprValueSize(value);	// Actually the number of elements.

	return (e);
}

Expr *
ExprKit::sequence_decl(Expr *typep, Expr *length)
{
	SequenceDecl	*e = new SequenceDecl(impl_->pos(), typep, length);

	//
	// Sequences are to be treated as unknowns.
	// But, should add the calculation here, it may be used later.
	//
	e->ExprValueUnknown(1);

	//
	// Even if we do not know how many elements exist,
	// we need to know whether the sequence can possibly contain
	// any object references.
	//
	e->ExprValue.any_objs = typep->ExprValue.any_objs;

	if (debug_size) {
		fprintf(stderr, "sequence_decl\n"
		    "    %s\t%d\t%d\t%d\t%s::%s::%s\n",
		    (typep->ExprValue.any_objs ? "t" : "f"),
		    typep->ExprValueSize(),
		    typep->ExprValueObjtref(),
		    typep->ExprValueUnknown(),
		    typep->ExprValue.strings[0],
		    typep->ExprValue.strings[1],
		    typep->ExprValue.strings[2]);
	}
	return (e);
}

Expr *
ExprKit::string_decl(Expr *length)
{
	StringDecl	*e = new StringDecl(impl_->pos(), length);

	if (length != nil) {
		e->ExprValueSize(length->ExprValueSize());
	} else {
		// We dont know the length of the string, hence it is an UNKNOWN
		e->ExprValueUnknown(1);
	}

	return (e);
}

//
// struct_decl - process one struct declaration.
//
Expr *
ExprKit::struct_decl(Identifier *ident, ExprList *members)
{
	StructDecl	*e = new StructDecl(impl_->pos(), ident, members);
	long		size = 0;
	long		objtref = 0;
	long		unknown = 0;
	bool		anyobjs = false;

	if (debug_size) {
		fprintf(stderr, "struct_decl\t\t%s\n",
		    ident->string()->string());
	}
	//
	// This loop process each type declaration within the struct.
	// Each type declaration may declare multiple struct members.
	//
	if (members != nil) {
		for (ListItr(ExprList) i(*members); i.more(); i.next()) {
			anyobjs |= i.cur()->ExprValue.any_objs;
			size += i.cur()->ExprValueSize();
			objtref += i.cur()->ExprValueObjtref();
			unknown += i.cur()->ExprValueUnknown();

			if (debug_size) {
				fprintf(stderr,
				    "    %s\t%d\t%d\t%d\t%s::%s::%s\n",
				    (i.cur()->ExprValue.any_objs ? "t" : "f"),
				    i.cur()->ExprValueSize(),
				    i.cur()->ExprValueObjtref(),
				    i.cur()->ExprValueUnknown(),
				    i.cur()->ExprValue.strings[0],
				    i.cur()->ExprValue.strings[1],
				    i.cur()->ExprValue.strings[2]);
			}
		}
	}

	e->ExprValue.any_objs = anyobjs;
	e->ExprValueSize(size);
	e->ExprValueObjtref(objtref);
	e->ExprValueUnknown(unknown);
	add_element_name(module_name, interface_name, e);

	return (e);
}

//
// struct_member - the name of this method can easily mislead !
//
// The syntax allows the programmer to specify multiple struct members
// per "type" declaration, such as:
//	long	first_long, second_long, third_long;
//
// This method processes all of the struct members for a particular
// type declaration.
//
Expr *
ExprKit::struct_member(Expr *type, ExprList *declarator_list)
{
	StructMember *e = new StructMember(impl_->pos(), type, declarator_list);
	long		size = 0;
	long		objtref = 0;
	long		unknown = 0;

	if (debug_size) {
		fprintf(stderr, "struct_member\t\t%s::%s::%s\n",
		    type->ExprValue.strings[0],
		    type->ExprValue.strings[1],
		    type->ExprValue.strings[2]);
	}
	//
	// The ExprValue of this declaration contains the size required by
	// all the struct members for one specific type declaration.
	//
	for (ListItr(ExprList) i(*declarator_list); i.more(); i.next()) {
		size += type->ExprValueSize() * i.cur()->ExprValueSize();
		objtref += type->ExprValueObjtref() * i.cur()->ExprValueSize();
		unknown += type->ExprValueUnknown() * i.cur()->ExprValueSize();

		if (debug_size) {
			fprintf(stderr,
			    "    %s\t%d\t%d\t%d\t%s::%s::%s\n",
			    (type->ExprValue.any_objs ? "t" : "f"),
			    i.cur()->ExprValueSize(),
			    i.cur()->ExprValueObjtref(),
			    i.cur()->ExprValueUnknown(),
			    i.cur()->ExprValue.strings[0],
			    i.cur()->ExprValue.strings[1],
			    i.cur()->ExprValue.strings[2]);
		}
	}
	//
	// The type for all of these 1 or more struct members
	// identifies whether any object references are present.
	//
	e->ExprValue.any_objs = type->ExprValue.any_objs;

	e->ExprValueSize(size);
	e->ExprValueObjtref(objtref);
	e->ExprValueUnknown(unknown);

	return (e);
}

Expr *
ExprKit::union_decl(Identifier *ident, Expr *type, CaseList *case_list)
{
	UnionDecl *e = new UnionDecl(impl_->pos(), ident, type, case_list);
	long		size = 0;
	long		objtref = 0;
	long		unknown = 0;
	bool		anyobjs = false;

	if (debug_size) {
		fprintf(stderr, "union_decl\t\t%s\n",
		    ident->string()->string());
	}
	//
	// The union size information is the worst case of
	// union members, which are found indirectly through
	// the case elements.
	//
	for (ListItr(CaseList) i(*case_list); i.more(); i.next()) {
		//
		// If any case can have an object reference this is true.
		//
		anyobjs |= i.cur()->element()->ExprValue.any_objs;

		if (size < i.cur()->element()->ExprValueSize()) {
			size = i.cur()->element()->ExprValueSize();
		}

		if (objtref < i.cur()->element()->ExprValueObjtref()) {
			objtref = i.cur()->element()->ExprValueObjtref();
		}

		if (unknown < i.cur()->element()->ExprValueUnknown()) {
			unknown = i.cur()->element()->ExprValueUnknown();
		}

		if (debug_size) {
			fprintf(stderr,
			    "    %s\t%d\t%d\t%d\t%s::%s::%s\n",
			    (i.cur()->element()->ExprValue.any_objs ?
			    "t" : "f"),
			    i.cur()->element()->ExprValueSize(),
			    i.cur()->element()->ExprValueObjtref(),
			    i.cur()->element()->ExprValueUnknown(),
			    i.cur()->element()->ExprValue.strings[0],
			    i.cur()->element()->ExprValue.strings[1],
			    i.cur()->element()->ExprValue.strings[2]);
		}
	}
	e->ExprValue.any_objs = anyobjs;

	//
	// The IDL compiler automatically generates a discriminator for a union.
	// The discriminator is marshalled with the active union member.
	// So increase the size to account for the discriminator.
	//
	e->ExprValueSize(size + 4);

	e->ExprValueObjtref(objtref);

	e->ExprValueUnknown(unknown);

	add_element_name(module_name, interface_name, e);

	return (e);
}

//
// union_member - process one member of a union.
// This member can only be a single entity,
// which could be either a simple or complex type.
//
UnionMember *
ExprKit::union_member(Expr *typep, Expr *declaratorp)
{

	ExprList *members = new ExprList;
	members->append(declaratorp);
	UnionMember *e = new UnionMember(impl_->pos(), typep, members);

	//
	// Unlike the struct_member, the union_member can only have one
	// declaration.
	//
	e->ExprValue.any_objs = typep->ExprValue.any_objs;

	e->ExprValueSize(typep->ExprValueSize() * declaratorp->ExprValueSize());

	e->ExprValueObjtref(typep->ExprValueObjtref() *
	    declaratorp->ExprValueSize());

	e->ExprValueUnknown(typep->ExprValueUnknown() *
	    declaratorp->ExprValueSize());

	return (e);
}

CaseElement *
ExprKit::case_element(ExprList *case_label_list, UnionMember *elementp)
{
	return (new CaseElement(impl_->pos(), case_label_list, elementp));
}

Expr *
ExprKit::case_label(Expr *value)
{
	return (new CaseLabel(impl_->pos(), value));
}

Expr *
ExprKit::default_label()
{
	return (new DefaultLabel(impl_->pos()));
}

Expr *
ExprKit::enum_decl(Identifier *ident, ExprList *members)
{
	EnumDecl *e = new EnumDecl(impl_->pos(), ident, members);

	e->ExprValueSize(4);		// Enums take 4 bytes

	add_element_name(module_name, interface_name, e);
	return (e);
}

Expr *
ExprKit::enumerator(Identifier *ident)
{
	return (new Enumerator(impl_->pos(), ident));
}

Expr *
ExprKit::except_decl(Identifier *i, ExprList *members)
{
	return (new ExceptDecl(impl_->pos(), i, members));
}

//
// Processes the definition of a method (or operation)
// on an interface (or object).
//
Expr *
ExprKit::operation(Identifier *ident, long version, Expr *type,
    ExprList *params, ExprList *exceptions, long attributes,
    ExprList *context, long pool)
{
	return (new Operation(impl_->pos(), ident, version, type,
	    params, exceptions, attributes, context, pool));
}

//
// parameter - this creates a Parameter object.
//
Expr *
ExprKit::parameter(invo_args::arg_size_info size_type, invo_args::arg_mode attr,
    Expr *type, Identifier *ident, ExprList *subscripts)
{
	return (new Parameter(impl_->pos(), size_type, attr, type, ident,
	    subscripts));
}

Identifier *
ExprKit::ident(String *s)
{
	return (new IdentifierImpl(s, impl_->pos()));
}

Expr *
ExprKit::boolean_literal(bool b)
{
	return (new boolLiteral(impl_->pos(), b));
}

Expr *
ExprKit::integer_literal(long n)
{
	return (new IntegerLiteral(impl_->pos(), n));
}

Expr *
ExprKit::float_literal(double d)
{
	return (new FloatLiteral(impl_->pos(), d));
}

Expr *
ExprKit::string_literal(String *s)
{
	return (new StringLiteral(impl_->pos(), s));
}

Expr *
ExprKit::char_literal(long n)
{
	return (new CharLiteral(impl_->pos(), n));
}

ExprList *
ExprKit::exprlist()
{
	return (new ExprList);
}

CaseList *
ExprKit::caselist()
{
	return (new CaseList);
}

Expr *
ExprKit::position(SourcePosition *p)
{
	return (new SrcPos(p));
}
