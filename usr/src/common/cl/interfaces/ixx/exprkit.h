/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _EXPRKIT_H
#define	_EXPRKIT_H

#pragma ident	"@(#)exprkit.h	1.6	06/05/24 SMI"

/*
 * Copyright (c) 1992-1993 Silicon Graphics, Inc.
 * Copyright (c) 1993 Fujitsu, Ltd.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Silicon Graphics and Fujitsu may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Silicon Graphics and Fujitsu.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */


#include <stdio.h>
#include "list.h"
#include "types.h"
#include <orb/invo/invo_args.h>
#include "expr.h"

// These constants support options for methods in the IDL specification
#define	DEFAULT_OPTION 0
#define	RECONFIG_OPTION 1

#define	TWO_WAY 0
#define	ONE_WAY 1
#define	ONE_WAY_NB_UR 2	// non_blocking unreliable
#define	ONE_WAY_UR 3	// unreliable
// Uncomment if using checkpoint
// #define CHECK_POINT 5	// checkpoint messages

class CaseElement;
class ExprKitImpl;
class Generator;
class Resolver;
class UnionMember;

typedef unsigned long Opcode;

declarePtrList(ExprList, Expr)
declarePtrList(CaseList, CaseElement)

//
// ExprKit - supports the actions of the parser.
// The ExprKit provides methods which create individual objects
// to represent constructs found by the parser.
//
class ExprKit {
public:
	ExprKit(ErrorHandler *);
	virtual		~ExprKit();

	virtual Expr	*root(ExprList *defs);

	virtual Expr	*actual_interface(Identifier *ident,
			    ExprList *supertypes,
			    ExprList *defs);

	virtual Expr	*forward_interface(Identifier *ident);
	virtual Expr	*server_only(Identifier *ident);

	virtual Expr	*parent_interface(Expr *, long, long);

	virtual Expr	*module(Identifier *ident, ExprList *defs);
	virtual Expr	*scoped(Expr *scope, String *ident);
	virtual Expr	*constant(Identifier *ident, Expr *type, Expr *value);
	virtual Expr	*unary(Opcode op, Expr *expr);
	virtual Expr	*binary(Opcode op, Expr *left, Expr *right);
	virtual Expr	*type_name(Expr *type, ExprList *declarator_list);
	virtual Expr	*unsigned_type(Expr *type);

	virtual Expr	*declarator_decl(Identifier *ident,
			    ExprList *subscripts);

	virtual Expr	*struct_decl(Identifier *ident, ExprList *members);
	virtual Expr	*struct_member(Expr *type, ExprList *declarator_list);

	virtual Expr	*union_decl(Identifier *ident, Expr *type,
			    CaseList *case_list);

	virtual CaseElement	*case_element(ExprList *case_label_list,
				    UnionMember *element);

	virtual Expr	*case_label(Expr *);
	virtual Expr	*default_label();

	virtual UnionMember	*union_member(Expr *type, Expr *declarator);

	virtual Expr	*enum_decl(Identifier *ident, ExprList *members);
	virtual Expr	*enumerator(Identifier *ident);
	virtual Expr	*sequence_decl(Expr *type, Expr *length);
	virtual Expr	*string_decl(Expr *length);
	virtual Expr	*except_decl(Identifier *ident, ExprList *members);

	virtual Expr	*operation(Identifier *ident, long version, Expr *type,
			    ExprList *params, ExprList *exceptions,
			    long attributes, ExprList *context, long pool);

	virtual Expr	*parameter(invo_args::arg_size_info size_type,
				invo_args::arg_mode attr,
				Expr *type,
				Identifier *ident,
				ExprList *subscripts);

	virtual Identifier	*ident(String *);

	virtual Expr		*boolean_literal(bool);
	virtual Expr		*integer_literal(long);
	virtual Expr		*float_literal(double);
	virtual Expr		*string_literal(String *);
	virtual Expr		*char_literal(long);
	virtual ExprList	*exprlist();
	virtual CaseList	*caselist();
	virtual Expr		*position(SourcePosition *);

	virtual Resolver	*resolver(const ConfigInfo &);
	virtual Generator	*generator(const ConfigInfo &);

private:
	ExprKitImpl		*impl_;
};

Expr	*name_decl(Expr *name);
void	add_module_name(char *module_name);
void	add_interface_name(char *module_name, char *interface_name);
void	print_module_list();

#endif	/* _EXPRKIT_H */
