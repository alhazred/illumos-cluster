//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

/*
 * Copyright (c) 1992-1993 Silicon Graphics, Inc.
 * Copyright (c) 1993 Fujitsu, Ltd.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Silicon Graphics and Fujitsu may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Silicon Graphics and Fujitsu.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

#pragma ident	"@(#)StringDecl.cc	1.6	06/05/24 SMI"

//
// Class StringDecl Methods
//

#include "generator.h"
#include "StringDecl.h"
#include "Symbol.h"
#include "Resolver.h"
#include "SymbolTable.h"

//
// The following methods came from expr.cc
//

StringDecl::StringDecl(SourcePosition *p, Expr *length) :
	ExprImpl(p),
	length_(length)
{
}

StringDecl::~StringDecl()
{
}

bool
StringDecl::compute_varying()
{
	return (true);
}

//
// The following methods came from Resolver.cc
//

void
StringDecl::resolve(Resolver *r)
{
	Symbol *s;
	if (length_ == nil) {
		s = SymbolTable::the()->string_type();
	} else {
		// Should check that the length expr evaluates to a constant.
		length_->resolve(r);
		s = new Symbol(r->scope());
		s->string_type(this);
	}
	r->symbol(s);
	symbol_ = s;
}

bool
StringDecl::generate(Generator *g)
{
	g->emit("char *");
	return (true);
}

bool
StringDecl::generate_name(Generator *g)
{
	g->emit(g->concat() ? "String" : "char *");
	return (true);
}

bool
StringDecl::generate_type_name(Generator *g)
{
	g->add_operation_param("string");
	return (false);
}
