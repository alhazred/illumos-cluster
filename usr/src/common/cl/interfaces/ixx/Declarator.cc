//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

/*
 * Copyright (c) 1992-1993 Silicon Graphics, Inc.
 * Copyright (c) 1993 Fujitsu, Ltd.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Silicon Graphics and Fujitsu may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Silicon Graphics and Fujitsu.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

#pragma ident	"@(#)Declarator.cc	1.9	06/05/24 SMI"

//
// Class Declarator Methods
//

#include "generator.h"
#include "Declarator.h"
#include "Resolver.h"
#include "Symbol.h"
#include <assert.h>

Declarator::Declarator(SourcePosition *pos, Identifier *ident,
    ExprList *subscripts) :
	ExprImpl(pos),
	ident_(ident),
	subscripts_(subscripts)
{
}

Declarator::~Declarator()
{
}

Declarator *
Declarator::declarator()
{
	return (this);
}

void
Declarator::resolve(Resolver *resolverp)
{
	Context *contextp = resolverp->context();
	if (contextp == nil) {
		resolverp->handler()->internal("no context for declarator");
	}
	Symbol *symbolp = resolverp->new_symbol(ident_);
	symbol_ = symbolp;
	element_type_ = contextp->type_expr;
	ExprValue.any_objs = element_type_->ExprValue.any_objs;
	symbolp->hash()->add_type(element_type_);
	if (subscripts_ == nil) {
		symbolp->copy_value(contextp->in_symbol);
	} else {
		symbolp->array(this);
		resolverp->symbol(symbolp);
	}
}

//
// Output code for the header file declaration.
// This is also called when generating stubs.
//
bool
Declarator::generate(Generator *g)
{
	g->emit_declarator_ident(ident_);
	if (subscripts_ != nil && g->is_array_decl()) {
		// We are defining the declarator rather than referencing it.
		g->emit("[");
		generate_list(subscripts_, &Expr::generate, g, "][");
		g->emit("]");
	}
	return (true);
}

bool
Declarator::generate_name(Generator *g)
{
	g->emit("%I", ident_->string());
	return (true);
}

//
// Construct the type ID name.
//
bool
Declarator::generate_type_name(Generator *g)
{
	if (subscripts_ == nil) {
		(void) element_type_->generate_type_name(g);
	} else {
		g->add_operation_param("array");
		(void) element_type_->generate_type_name(g);
		(void) generate_list(subscripts_, &Expr::generate_type_name, g);
	}
	return (false);
}

//
// Output code to define an external declaration for the marshal routines.
// This is used when the stub code is defined in a different IDL file
// but is included in the the one being compiled.
//
bool
Declarator::generate_extern_stubs(Generator *g)
{
	long filemask = g->file_mask();
	if (subscripts_ != nil && !symbol_->declared_stub(filemask)) {
		g->emit("extern void _%Y_put(%< &, void *);\n"
		    "extern void _%Y_get(%< &, void *);\n", nil, this);
		if (varying()) {
			g->emit("extern void _%Y_release(void *);\n",
			    nil, this);
		}

		//
		// Only an array of object references needs an unput function.
		//
		if (ExprValueAnyobjs()) {
			g->emit("extern void _%Y_unput(void *);\n", nil, this);
		}
		g->emit("extern MarshalInfo_complex _%Y_ArgMarshalFuncs;\n",
		    nil, this);

		symbol_->declare_stub(filemask);
		return (true);
	}
	return (false);
}

//
// Generate any functions needed to support this type
// at marshal time, such as put, get, etc.
//
bool
Declarator::generate_stub(Generator *g)
{
	symbol_->declare_stub(g->file_mask());
	if (subscripts_ == nil) {
		return (false);
	}

	//
	// A declaration with subscripts represents an array.
	//
	g->emit("//\n"
	    "// Marshal time support functions for %F\n"
	    "//\n\n", nil, this);

	//
	// Generate marshal function.
	//
	g->emit("void\n_%Y_put(%< &_b, void *voidp)\n"
	    "{%i\n", nil, this);
	g->emit("%F (*_array)", nil, element_type_);
	emit_array_dimen(g, &Expr::generate, true);
	g->emit(" = (%F (*)", nil, element_type_);
	emit_array_dimen(g, &Expr::generate, true);
	g->emit(")voidp;\n");
	emit_array_setup(g);
	g->emit_put(element_type_, "_tmp", element_type_);
	g->emit_array_loop_finish(subscripts_->count());
	g->emit("%u}\n\n");

	//
	// Generate unmarshal function.
	//
	g->emit("void\n_%Y_get(%< &_b, void *voidp)\n"
	    "{%i\n", nil, this);
	g->emit("%F (*_array)", nil, element_type_);
	emit_array_dimen(g, &Expr::generate, true);
	g->emit(" = (%F (*)", nil, element_type_);
	emit_array_dimen(g, &Expr::generate, true);
	g->emit(")voidp;\n");
	emit_array_setup(g);
	g->emit_get(element_type_, "_tmp", element_type_);
	g->emit_array_loop_finish(subscripts_->count());
	g->emit("%u}\n\n");

	if (element_type_->varying()) {
		//
		// Generate make function.
		//
		g->emit("void *\n_%Y_makef()\n"
		    "{%i\n", nil, this);
		g->emit("return (new %F ", nil, element_type_);
		emit_array_dimen(g, &Expr::generate, false);
		g->emit(");\n"
		    "%u}\n\n");

		//
		// Generate free function.
		//
		g->emit("void\n_%Y_freef(void *voidp)\n"
		    "{%i\n", nil, this);
		g->emit("%F (*_array)", nil, element_type_);
		emit_array_dimen(g, &Expr::generate, true);
		g->emit(" = (%F (*)", nil, element_type_);
		emit_array_dimen(g, &Expr::generate, true);
		g->emit(")voidp;\n"
		    "delete [] _array;\n"
		    "%u}\n\n");

		//
		// Generate release function.
		//
		g->emit("void\n_%Y_release(%< &_b, void *voidp)\n"
		    "{%i\n", nil, this);
		g->emit("%F (*_array)", nil, element_type_);
		emit_array_dimen(g, &Expr::generate, true);
		g->emit(" = (%F (*)", nil, element_type_);
		emit_array_dimen(g, &Expr::generate, true);
		g->emit(")voidp;\n");
		emit_array_setup(g);
		g->emit_release(element_type_, "_tmp", element_type_);
		g->emit_array_loop_finish(subscripts_->count());
		g->emit("%u}\n\n");
	}

	//
	// Generate marshal roll back function
	// if the array can contain object references.
	//
	if (ExprValueAnyobjs()) {
		g->emit("void\n_%Y_unput(void *voidp)\n"
		    "{\n%i", nil, this);
		g->emit("%F (*_array)", nil, element_type_);
		emit_array_dimen(g, &Expr::generate, true);
		g->emit(" = (%F (*)", nil, element_type_);
		emit_array_dimen(g, &Expr::generate, true);
		g->emit(")voidp;\n");
		g->emit_array_loop_start(subscripts_);
		g->emit("%F _tmp = _array", nil, element_type_);
		g->emit_array_loop_indices(subscripts_->count());
		g->emit(";\n");
		g->emit_unput(element_type_, "_tmp", element_type_);
		g->emit_array_loop_finish(subscripts_->count());
		g->emit("%u}\n\n");
	}

	//
	// Create a structure containing references to all
	// of the marshal time support functions.
	//
	g->emit("MarshalInfo_complex _%Y_ArgMarshalFuncs = {\n%i",
	    nil, this);
	g->emit("&_%Y_put,\n", nil, this);
	g->emit("&_%Y_get,\n", nil, this);
	if (element_type_->varying()) {
		g->emit("&_%Y_makef,\n"
		    "&_%Y_freef,\n"
		    "&_%Y_release,\n", nil, this);
	} else {
		g->emit("NULL,\n"
		    "NULL,\n"
		    "NULL,\n");
	}
	if (ExprValueAnyobjs()) {
		g->emit("&_%Y_unput,\n", nil, this);
	} else {
		g->emit("NULL,\n");		// no unput method
	}
	g->emit("NULL\n");		// no marshal size method
	g->emit("%u};\n");

	return (true);
}

void
Declarator::emit_array_setup(Generator *g)
{
	ExprList *subs = subscripts();
	g->emit_array_loop_start(subs);

	g->emit("%F &_tmp = _array", nil, element_type_);

	g->emit_array_loop_indices(subs->count());
	g->emit(";\n");
}

//
// ----------
//

//
// emit_array_upper_dimen - generate the upper dimensional array
// subscripts for a multi-dimensional array.
//
void
Declarator::emit_array_dimen(Generator *g,
	// CSTYLED
	bool (Expr::*func)(Generator *), bool skip_first)
{
	// Arrays must have subscripts.
	assert(subscripts_ != nil);

	int count = 0;
	Expr *exprp;
	for (ListItr(ExprList) i(*subscripts_); i.more(); i.next(), count++) {
		if (skip_first && count == 0) {
			// Do not output first dimension.
			continue;
		}
		// Output the dimension.
		g->emit("[");
		exprp = i.cur();
		(void) (exprp->*func)(g);
		g->emit("]");
	}
}
