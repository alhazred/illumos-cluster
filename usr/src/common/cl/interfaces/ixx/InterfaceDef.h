/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _INTERFACEDEF_H
#define	_INTERFACEDEF_H

#pragma ident	"@(#)InterfaceDef.h	1.11	06/05/24 SMI"

/*
 * Copyright (c) 1992-1993 Silicon Graphics, Inc.
 * Copyright (c) 1993 Fujitsu, Ltd.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Silicon Graphics and Fujitsu may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Silicon Graphics and Fujitsu.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */


#include "expr-impl.h"
#include "list.h"

struct OpInfo {
	Operation	*op;
	char		*type_string;
};

class VersionInfo {
public:
	VersionInfo();

	long		nops;		// Number of methods (size of ops).
	OpInfo		*ops;		// Type ID info for each method.
	bool		declared_rfunc;	// True if extern emitted in schema.
	bool		emitted_tid;	// True if tid or extern tid emitted.
};

struct InterfaceInfo {
	Scope		*block;			// Scope where defined.
	bool		server_only;
	bool		multiple_inheritance;	// True if multiple inheritance.
	bool		has_methods;		// True if has methods.
	bool		declared_stub;
	bool		declared_corba_tid;
	bool		generated_decl;		// True if forward decl output.
	bool		generated_body;		// True if class decl output.
	long		nvinfo;			// Size of vinfo array.
	VersionInfo	*vinfo;
	long		max_params;
	long		max_methods;		// number of methods + parents'
	HashCode	hash;
};

//
// AncestorImpl - defines an inherited interface.
// This is kind of ugly but trying to inherit from IdentifierImpl and
// AccessorImpl is worse.
//
class ParentImpl : public ExprImpl {
public:
	ParentImpl(SourcePosition *pos, Expr *, long, long,
	    InterfaceDef * = nil);
	~ParentImpl();

	InterfaceDef	*interface()	{ return (interface_); }
	void	interface(InterfaceDef *inf) { interface_ = inf; }

	long	parent_version()	{ return (parent_version_); }

	String	*string()		{ return (name_->string()); }

	Symbol	*symbol()		{ return (name_->symbol()); }
	bool	generate(Generator *g);
	bool	generate_name(Generator *g) {
		return (name_->generate_name(g));
	}
	bool	generate_stub(Generator *g) {
		return (name_->generate_stub(g));
	}
	bool	generate_extern_stubs(Generator *g) {
		return (name_->generate_extern_stubs(g));
	}
	bool	generate_type_name(Generator *g) {
		return (name_->generate_type_name(g));
	}

	void	resolve(Resolver *);

private:
	Expr	*name_;
	long	parent_version_;	// version of interface being inherited
	InterfaceDef *interface_;
};

//
// There is one InterfaceDef object created when parsing the IDL file.
// During the rewrite() phase, additional InterfaceDef objects are
// created for each version of the interface to match the generated C++
// code class structure.
//
class InterfaceDef : public ExprImpl {
public:
	InterfaceDef(SourcePosition *pos, bool forward, bool server_only,
	    Identifier *ident, ExprList *supertypes, ExprList *defs);

	~InterfaceDef();

	void		resolve(Resolver *);
	void		rewrite(Generator *, ListUpdater(ExprList) *);
	bool		generate(Generator *);

	Identifier	*ident()	{ return (ident_); }
	String		*string()	{ return (ident_->string()); }
	ExprList	*parents()	{ return (supertypes_); }
	InterfaceInfo	*info()		{ return (info_); }
	Scope		*block()	{ return (info_->block); }

	void		params(long n)	{
						if (n > info_->max_params) {
						    info_->max_params = n;
						}
					}

	//
	// InterfaceDef::kind should be next after Environment's tag
	//
	invo_args::arg_type	kind()	{ return (invo_args::t_object); }

	bool	generate_schema(Generator *);
	bool	generate_name(Generator *);
	bool	generate_type_name(Generator *);
	bool	generate_stub(Generator *);
	bool	generate_extern_stubs(Generator *);
	bool	generate_server(Generator *);
	bool	put_cast_up(Generator *);

protected:
	void	put_hdr(Generator *);
	void	emit_versionex_hdr(Generator *, char **, long &, long v,
		    bool incl);
	void	emit_methods_hdr(Generator *, char **, long &, long v);
	void	emit_versionex_stubs(Generator *, char **, long &, long v,
		    bool incl);
	void	emit_methods_stubs(Generator *, char **, long &, long v,
		    bool incl);
	void	emit_schema(Generator *, long v);
	void	generate_typeid(Generator *, long v);
	bool	has_any_methods(Generator *, long v);
	void	emit_tid_extern(Generator *, long v);
	long	emit_class_tid(Generator *, long v, long index);
	long	emit_class_no_methods_tid(Generator *, long v, long index);
	void	emit_class_func(Generator *, long v);
	void	emit_offset_info(Generator *, String *, long v, bool methods);

	// Search for "p" in "parents_" and return it if found.
	ParentImpl *inherits(ParentImpl *p);

	// Search for "inf" in "parents_" and its index (off_index).
	long off_index(InterfaceDef *inf);

	//
	// Return true if this interface or one of its ancestors has multiple
	// inheritance.
	//
	bool multiple_inherits();

	// Count the total number of methods an interface can call.
	void cnt_methods(long &cnt);

	// Return true/false if we have seen this method signature.
	bool seen_method(Operation *op, char **op_table, long &cnt);

	// Sort OpInfo structures by type ID string.
	static int opinfo_sort(const void *a, const void *b);

	bool		forward_;		// true if forward decl.
	bool		server_only_;		// true if server only decl.
	Identifier	*ident_;
	ExprList	*supertypes_;		// list of ancestors from IDL
	ExprList	*parents_;		// unique list of ancestors
	ExprList	*defs_;
	InterfaceInfo	*info_;			// shared info for forward & def
};

#endif	/* _INTERFACEDEF_H */
