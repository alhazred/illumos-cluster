//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

/*
 * Copyright (c) 1992-1993 Silicon Graphics, Inc.
 * Copyright (c) 1993 Fujitsu, Ltd.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Silicon Graphics and Fujitsu may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Silicon Graphics and Fujitsu.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

#pragma ident	"@(#)Module.cc	1.7	06/05/24 SMI"

//
// Class Module Methods
//

#include "generator.h"
#include "Module.h"
#include "main.h"
#include "Symbol.h"
#include "Resolver.h"

//
// The following modules come from expr.cc
//

Module::Module(SourcePosition *pos, Identifier *ident, ExprList *defs) :
	ExprImpl(pos),
	ident_(ident),
	defs_(defs)
{
}

Module::~Module()
{
}

//
// The following modules come from gen-hdr.cc
//

//
// generate_schema - generates the schema information for a module.
//
bool
Module::generate_schema(Generator *g)
{
	g->start_module(ident_->string()->string());

	g->enter_scope(block_);
	generate_list(defs_, &Expr::generate_schema, g);
	g->leave_scope();

	//
	// Generate a type id for this module.
	//
	generate_typeid(g);

	// Generate the InterfaceDescriptor definition.
	g->emit("InterfaceDescriptor _Ix__type(%_%I)(\n%i"
	    "\"%Q\",\n"
	    "_Ix__tid(%Y),\n"
	    "_Ix__tid(%Y),\n"
	    "0,\n"
	    "NULL,\n"
	    "NULL,\n"
	    "NULL,\n"
	    "NULL);\n\n%u", ident_->string(), this);
	return (false);
}

//
// Generate header declaration for a module.  Currently, we use
// a class for the scoping.  This should be changed to generate
// C++ namespaces if/when supported by the compiler.
//
bool
Module::generate(Generator *g)
{
	String *s = ident_->string();
	g->emit("class %I {\npublic:\n%i", s);

	//
	// Generate virtual destructor - so that lint does not complain about
	// destructor not being virtual.
	//
	g->emit("virtual ~%I() {}\n\n", s);

	g->enter_scope(block_);
	generate_list(defs_, &Expr::generate, g, ";\n", ";\n");
	g->leave_scope();

	g->emit("%u};\n");

	return (false);
}

//
// Generate the Typeid for this module (used with exceptions).
//
void
Module::generate_typeid(Generator *g)
{
	// Run MD5 on the string
	char *s = g->get_module_string();
	unsigned char digest[16];
	MDString(s, digest);

	// This type id must be recorded for schema initialization.
	g->add_id_to_table(this);

	// Output the object type id.
	g->emit_tid(digest, ident_->string());

	if (g->tidfile() != nil) {
		fprintf(g->tidfile(), "TID: %s\n", s);
		fprintf(g->tidfile(), "MD5: ");
		for (int i = 0; i < 16; i++)
			fprintf(g->tidfile(), "%02x", digest[i]);
		fprintf(g->tidfile(), "\n");
	}
	if (type_debug == true) {
		fprintf(stderr, "TID digest input: %s\n", s);
		fprintf(stderr, "MD5: ");
		for (int i = 0; i < 16; i++)
			fprintf(stderr, "%02x", digest[i]);
		fprintf(stderr, "\n");
	}
}

bool
Module::generate_name(Generator* g)
{
	g->emit("%I", ident_->string());
	return (true);
}

//
// Generate code for client side stub.
//
bool
Module::generate_stub(Generator *g)
{
	bool b = false;
	if (defs_ != nil) {
		g->enter_scope(block_);
		b = generate_list(defs_, &Expr::generate_stub, g, "\n");

		// Output initialization code for exception TID schema table.
		if (block_->except_index > 0) {
			put_except_list(defs_, g);
		}
		g->leave_scope();
	}
	return (b);
}

bool
Module::generate_server(Generator *g)
{
	g->enter_scope(block_);
	bool b = generate_list(defs_, &Expr::generate_server, g, "\n");
	g->leave_scope();
	return (b);
}

//
// The following modules come from Resolver.cc
//

void
Module::resolve(Resolver *r)
{
	Symbol* s = r->new_symbol(ident_);
	s->module(this);
	symbol_ = s;
	block_ = r->enter_scope(ident_);
	resolve_list(defs_, r);
	r->leave_scope();
	s->hash()->add_list(defs_);
}

void
Module::rewrite(Generator *g, ListUpdater(ExprList) *)
{
	rewrite_list(g, defs_);
}
