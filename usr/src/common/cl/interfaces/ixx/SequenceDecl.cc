//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

/*
 * Copyright (c) 1992-1993 Silicon Graphics, Inc.
 * Copyright (c) 1993 Fujitsu, Ltd.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Silicon Graphics and Fujitsu may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Silicon Graphics and Fujitsu.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

#pragma ident	"@(#)SequenceDecl.cc	1.12	06/05/24 SMI"

//
// Class SequenceDecl Methods
//

#include <assert.h>
#include "generator.h"
#include "SequenceDecl.h"
#include "Symbol.h"
#include "Resolver.h"
#include "InterfaceDef.h"

const int	marshal_string_size_estimate = 16;
const int	marshal_sequence_size_estimate = 32;
const int	marshal_unknown_size_estimate = 20;

SequenceDecl::SequenceDecl(SourcePosition *p, Expr *type, Expr *length) :
	ExprImpl(p),
	name_(nil),
	type_(type),
	length_(length)
{
}

SequenceDecl::~SequenceDecl()
{
}

bool
SequenceDecl::compute_varying()
{
	return (true);
}

void
SequenceDecl::resolve(Resolver *r)
{
	type_->resolve(r);
	if (length_ != nil) {
		// XXX Should check that the length expr evaluates to a constant
		length_->resolve(r);
	}
	Scope *block = r->scope();
	Symbol *s = new Symbol(block);
	s->sequence_type(this);
	s->hash()->add_type(type_);
	r->symbol(s);
	symbol_ = s;
	id_ = block->id + 1;
	block->id = id_;
}

bool
SequenceDecl::generate(Generator *g)
{
	if (type_->string_type()) {
		g->emit("typedef CORBA::StringSeq %N;\n"
		    "typedef CORBA::StringSeq_var %N_var;\n"
		    "typedef CORBA::StringSeq_out %N_out",
		    nil, this);
	} else if (type_->interface_type()) {
		bool ref = g->interface_is_ref(false);
		g->emit("typedef _Ix_InterfaceSeq_<%E, %E_field, %E_ptr> ",
		    nil, type_);
		g->emit("%N;\n", nil, this);
		g->emit("typedef _T_out_<%N, %N *> %N_out;\n", nil, this);
		g->emit("typedef _ManagedSeq_<%N, ", nil, this);
		g->emit("%E_field> ", nil, type_);
		g->emit("%N_var", nil, this);
		g->interface_is_ref(ref);
	} else {
		if (!type_->varying()) {
			g->emit("typedef _FixedSeq_<%E> ", nil, type_);
		} else {
			g->emit("typedef _NormalSeq_<%E> ", nil, type_);
		}
		g->emit("%N;\n", nil, this);
		g->emit("typedef _T_out_<%N, %N *> %N_out;\n", nil, this);
		g->emit("typedef _ManagedSeq_<%N, ", nil, this);
		g->emit("%E> ", nil, type_);
		g->emit("%N_var", nil, this);
	}
	return (true);
}

bool
SequenceDecl::generate_name(Generator *g)
{
	if (name_ != nil) {
		name_->generate_name(g);
	} else {
		if (type_->string_type())
			g->emit("_StringSeq", nil, type_);
		else
			g->emit("_%NSeq", nil, type_);
		if (id_ > 1) {
			g->emit_integer(id_);
		}
	}
	return (true);
}

bool
SequenceDecl::generate_type_name(Generator *g)
{
	g->add_operation_param("seq");
	if (type_->string_type()) {
		g->add_operation_param("string");
	} else {
		type_->generate_type_name(g);
	}
	if (length_ != nil) {
		length_->generate_type_name(g);
	}
	return (false);
}

//
// Generate forward declarations for marshal time functions.
//
bool
SequenceDecl::generate_extern_stubs(Generator *g)
{
	if (type_->string_type())
		return (false);
	bool b = type_->generate_extern_stubs(g);
	if (!symbol_->declared_stub(g->file_mask())) {
		symbol_->declare_stub(g->file_mask());
		g->emit("extern void _%Y_put(%< &, void *);\n"
		    "extern void _%Y_get(%< &, void *);\n"
		    "extern void _%Y_release(void *);\n", nil, this);
		if (ExprValueAnyobjs()) {
			g->emit("extern void _%Y_unput(void *);\n", nil, this);
		}
		g->emit("extern MarshalInfo_complex _%Y_ArgMarshalFuncs;\n",
		    nil, this);
		b = true;
	}
	return (b);
}

//
// Generate any functions needed to support this type
// at marshal time, such as put, get, etc.
//
bool
SequenceDecl::generate_stub(Generator *g)
{
	//
	// The IDL compiler does not generate marshal time support
	// functions for sequences of strings since the string sequence
	// marshal functions are defined in the ORB.
	//
	if (!type_->string_type()) {
		if (type_->generate_extern_stubs(g))
			g->emit("\n");

		g->emit("//\n"
		    "// Marshal time support functions for %F\n"
		    "//\n\n", nil, this);

		//
		// Generate marshal function.
		//
		g->emit("void\n"
		    "_%Y_put(%< &_b, void *voidp)\n"
		    "{%i\n"
		    "%F *sequencep = (%F *)voidp;\n",
		    nil, this);
		generate_marshal(g);
		g->emit("%u}\n\n");

		//
		// Generate unmarshal function.
		//
		g->emit("void\n"
		    "_%Y_get(%< &_b, void *voidp)\n"
		    "{%i\n"
		    "%F *sequencep = (%F *)voidp;\n",
		    nil, this);
		generate_unmarshal(g);
		g->emit("%u}\n\n");

		symbol_->declare_stub(g->file_mask());

		// Sequence memory allocation functions.

		//
		// Generate make function.
		//
		g->emit("void *\n"
		    "_%Y_makef()\n"
		    "{%i\n"
		    "return (new %F);%u\n"
		    "}\n\n",
		    nil, this);

		//
		// Generate free function.
		//
		g->emit("void\n"
		    "_%Y_freef(void *voidp)\n"
		    "{%i\n"
		    "%F *sequencep = (%F *)voidp;\n"
		    "delete sequencep;%u\n"
		    "}\n\n", nil, this);

		//
		// Generate release function.
		//
		g->emit("void\n"
		    "_%Y_release(void *voidp)\n"
		    "{%i\n"
		    "%F *sequencep = (%F *)voidp;\n", nil, this);
		generate_release(g);
		g->emit("%u}\n\n");

		//
		// Generate marshal roll back function,
		// which is only required if the sequence can possibly
		// contain object references.
		//
		if (ExprValueAnyobjs()) {
			g->emit("void\n"
			    "_%Y_unput(void *voidp)\n"
			    "{\n%i"
			    "%F *sequencep = (%F *)voidp;\n",
			    nil, this);
			generate_marshal_rollback(g);
			g->emit("%u}\n\n");
		}

		//
		// Generate the marshal time size determination function.
		//
		g->emit("uint_t\n"
		    "_%Y_marshal_size(void *voidp)\n"
		    "{%i\n"
		    "%F *sequencep = (%F *)voidp;\n", nil, this);
		(void) generate_marshal_size(g);
		g->emit("%u}\n\n");

		//
		// Create a structure containing references to all
		// of the marshal time support functions.
		//
		g->emit("MarshalInfo_complex _%Y_ArgMarshalFuncs = {\n%i",
		    nil, this);
		g->emit("&_%Y_put,\n", nil, this);
		g->emit("&_%Y_get,\n", nil, this);
		g->emit("&_%Y_makef,\n", nil, this);
		g->emit("&_%Y_freef,\n", nil, this);
		g->emit("&_%Y_release,\n", nil, this);
		if (ExprValueAnyobjs()) {
			g->emit("&_%Y_unput,\n", nil, this);
		} else {
			g->emit("NULL,\n");	// no unput method
		}
		g->emit("&_%Y_marshal_size\n", nil, this);
		g->emit("%u};\n");

		return (true);
	}
	return (false);
}

//
// generate_marshal - creates the command to marshal the element of a sequence
//
bool
SequenceDecl::generate_marshal(Generator *g)
{
	Symbol *symbolp = type_->actual_type();
	bool ref;
	switch (symbolp->tag()) {
	case Symbol::sym_typedef:
		// builtin type
		g->emit("_b.put_seq(sequencep, (unsigned)sizeof(%F));\n",
		    nil, type_);
		break;
	case Symbol::sym_interface:
		ref = g->interface_is_ref(false);
		g->emit("sequencep->_put(_b);\n");
		g->interface_is_ref(ref);
		break;
	default:
		g->emit("_b.put_seq_hdr(sequencep);\n"
		    "for (uint_t i = 0; i < sequencep->length(); i++) {%i\n");
		g->emit_put(type_, "sequencep->operator[](i)", type_);
		g->emit("%u}\n");
		break;
	}
	return (true);
}

bool
SequenceDecl::generate_unmarshal(Generator *g)
{
	Symbol *symbolp = type_->actual_type();
	bool ref;
	switch (symbolp->tag()) {
	case Symbol::sym_typedef:
		// builtin type
		g->emit("_b.get_seq(sequencep, (unsigned)sizeof(%F));\n",
		    nil, type_);
		break;
	case Symbol::sym_interface:
		ref = g->interface_is_ref(false);
		g->emit("sequencep->_get(_b, &_%Y_ArgMarshalFuncs);\n",
		    nil, symbolp->interface());
		g->interface_is_ref(ref);
		break;
	default:
		g->emit("_b.get_seq_hdr(sequencep);\n"
		    "uint_t _max = sequencep->maximum();\n"
		    "if (_max == 0) {%i\n"
		    "return;\n"
		    "%u}\n"
		    "sequencep->_init(_max, sequencep->length(),\n%i"
		    "(void *)sequencep->allocbuf(_max), true);\n%u"
		    "for (uint_t i = 0; i < sequencep->length(); i++) {%i\n");
		g->emit_get(type_, "sequencep->operator[](i)", type_);
		g->emit("%u}\n");
		break;
	}
	return (true);
}

bool
SequenceDecl::generate_release(Generator *g)
{
	Symbol	*symbolp = type_->actual_type();
	switch (symbolp->tag()) {
	case Symbol::sym_typedef: // builtin type
	case Symbol::sym_interface:
		// Loading NULL value will free any previous allocations
		g->emit("sequencep->load(0, 0, NULL, false);\n");
		break;

	default:
		g->emit("for (uint_t i = 0; i < sequencep->length();"
		    " i++) {%i\n");
		g->emit_release(type_, "sequencep->operator[](i)", type_);
		g->emit("%u}\n"
		    "sequencep->_load(0, 0, (void *)NULL, false);\n");
		break;
	}
	return (true);
}

bool
SequenceDecl::generate_marshal_rollback(Generator *g)
{
	if (!ExprValueAnyobjs()) {
		// This item can not possibly contain any object references.
		return (false);
	}
	// This item can contain object references

	Symbol	*symbolp = type_->actual_type();
	switch (symbolp->tag()) {

	case Symbol::sym_interface:
		g->emit("sequencep->_unput();\n");
		break;

	case Symbol::sym_array:
	case Symbol::sym_sequence:
	case Symbol::sym_union:
	case Symbol::sym_struct:
		g->emit("for (uint_t i = 0; i < sequencep->length(); i++)"
		    " {%i\n");
		g->emit_unput(type_, "sequencep->operator[](i)", type_);
		g->emit("%u}\n");
		break;

	default:
		// Nothing else can contain object references
		assert(0);
		return (false);
	}
	return (true);
}

//
// generate_marshal_size - generate the function that determines the
// size of the sequence in bytes at marshal time.
//
// Since the marshal size function is used to estimate the
// space required at marshal time for this entity, we do not want to expend
// vast amounts of resources to determine this quantity.
// For example, we do not want to count each character in
// each string of a sequence of strings.
// Thus the system uses an estimate for those entities,
// whose size is unknown. Otherwise, the system uses
// the size calculated by the IDL compiler.
//
bool
SequenceDecl::generate_marshal_size(Generator *g)
{
	// Output the code that is common to all sequences
	// This part calculates the space required for the sequence overhead
	//
	g->emit("return (sequencep->_marshal_size() +\n%i");

	//
	// Output the code that computes the size, which is
	// the number of elements times the size of the type
	// (based on the IDL calculated size or an estimate).
	//
	Symbol	*symbolp = type_->actual_type();
	switch (symbolp->tag()) {
	case Symbol::sym_typedef:
		// builtin type
		g->emit("(sequencep->length() * (unsigned)sizeof(%F))",
		    nil, type_);
		break;

	case Symbol::sym_enum:
		// builtin type
		g->emit("(sequencep->length() * (unsigned)sizeof(long))");
		break;

	case Symbol::sym_interface:
		//
		// Object references are loaded into a special
		// buffer for object references and thus
		// do not take up data space. Any decent optimizing compiler
		// will eliminate a zero addition.
		//
		g->emit("0");
		break;

	case Symbol::sym_string:
		// Use an estimate for a string length
		g->emit("(sequencep->length() * ");
		g->emit_integer(marshal_string_size_estimate);
		g->emit(")");
		break;

	case Symbol::sym_sequence:
		// Use an estimate for a string length
		g->emit("(sequencep->length() * ");
		g->emit_integer(marshal_sequence_size_estimate);
		g->emit(")");
		break;

	case Symbol::sym_array:
	case Symbol::sym_struct:
	case Symbol::sym_union:
		// Use the IDL calculated size.
		// This value does not include the size of embedded objects
		// or sequences. We just believe that this will provide
		// a reasonable estimate.
		//
		{
			long	size = type_->ExprValueSize();

			// Now add a reasonable (we hope) size estimate
			// for the unknowns.
			size += type_->ExprValueUnknown() *
			    marshal_unknown_size_estimate;

			assert(size > 0);

			g->emit("(sequencep->length() * ");
			g->emit_integer(size);
			g->emit(")");
		}
		break;

	case Symbol::sym_unknown:
	case Symbol::sym_module:
	case Symbol::sym_constant:
	case Symbol::sym_operation:
	case Symbol::sym_parameter:
	case Symbol::sym_exception:
	case Symbol::sym_member:
	case Symbol::sym_union_member:
	case Symbol::sym_enum_value:
		// Cannot have a sequence of the above types
		assert(0);
		break;
	}

	// Output the code that terminates the body of the function
	g->emit(");%u\n");

	return (true);
}
