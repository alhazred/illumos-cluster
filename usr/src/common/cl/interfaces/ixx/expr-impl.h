/*
 *  Copyright 1998-2002 Sun Microsystems, Inc.  All rights reserved.
 *  Use is subject to license terms.
 *
 */

#ifndef _EXPR_IMPL_H
#define	_EXPR_IMPL_H

#pragma ident	"@(#)expr-impl.h	1.40	02/09/11 SMI"

/*
 * Copyright (c) 1992-1993 Silicon Graphics, Inc.
 * Copyright (c) 1993 Fujitsu, Ltd.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Silicon Graphics and Fujitsu may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Silicon Graphics and Fujitsu.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

//
// Expr implementations
//


#include <orb/invo/invo_args.h>
#include "expr.h"
#include "exprkit.h"
#include "list.h"
#include "err.h"

class HashCode {
public:
	void	init();
	void	copy(const HashCode &);
	void	copy_string(String *);
	void	copy_ident(Identifier *);
	void	add(const HashCode &);
	void	add_string(String *);
	void	add_type(Expr *);
	void	add_list(ExprList *);

	bool	match(String *);
	void	generate(Generator *);

private:
	unsigned long hash_[2];
};

class ExprImpl : public Expr {
public:
	ExprImpl(SourcePosition *);
	~ExprImpl();

	long		version() { return (version_); }
	Symbol		*symbol();
	Declarator	*declarator();

	void	resolve(Resolver *);
	void	rewrite(Generator *, ListUpdater(ExprList) *);
	bool	do_source(Generator *);
	bool	generate(Generator *);
	bool	generate_name(Generator *);
	bool	generate_extern_stubs(Generator *);
	bool	generate_stub(Generator *);
	bool	generate_marshal(Generator *);
	bool	generate_unmarshal(Generator *);
	bool	generate_release(Generator *);
	bool	generate_marshal_rollback(Generator *);
	bool	generate_server(Generator *);
	bool	generate_receive_table(Generator *);
	bool	generate_type_name(Generator *);
	bool	generate_schema(Generator *);
	bool	varying();

	SourcePosition	*position();

	void	set_source_position(ErrorHandler *);

	static void	resolve_list(ExprList *, Resolver *);

	static void	rewrite_list(Generator *, ExprList *);

	static bool	generate_list(ExprList *,
			    // CSTYLED
			    bool (Expr::*func)(Generator *),
			    Generator *,
			    const char *sep = nil,
			    const char *trail = nil);

	static bool	put_except_list(ExprList *, Generator *);

protected:
	virtual bool	compute_varying();

	enum { unknown, fixed, varies } varying_;

	long		version_;
	Symbol		*symbol_;

private:
	SourcePosition	*position_;
};

class Module;
class InterfaceDef;
class Declarator;
class Operation;
class Parameter;
class StructDecl;
class StructMember;
class UnionDecl;
class UnionMember;
class EnumDecl;
class Enumerator;
class SequenceDecl;
class StringDecl;

struct Scope {
	String	*name;
	Scope	*outer;
	long	id;
	long	except_index;
};

class IdentifierImpl : public ExprImpl {
public:
	IdentifierImpl(String *, SourcePosition *);
	~IdentifierImpl();

	String	*string();
	bool	varying();

	void	resolve(Resolver *);
	bool	generate(Generator *);
	bool	generate_name(Generator *);
	bool	generate_extern_stubs(Generator *);
	bool	generate_stub(Generator *);
	bool	generate_marshal(Generator *);
	bool	generate_unmarshal(Generator *);
	bool	generate_release(Generator *);
	bool	generate_marshal_rollback(Generator *);

protected:
	void		check(Resolver *, UnionDecl *, Enumerator *);

	String		*value_;	// Name from IDL file
};

struct GenSepState;

class ExprKitImpl {
public:
	ExprKitImpl(ErrorHandler *h) : handler_(h) {}

	ErrorHandler	*handler() { return (handler_); }
	SourcePosition	*pos() { return handler_->position(); }

private:
	ErrorHandler	*handler_;
};

class RootExpr : public ExprImpl {
public:
	RootExpr(SourcePosition *, ExprList *);
	~RootExpr();

	void	resolve(Resolver *);
	void	rewrite(Generator *, ListUpdater(ExprList) *);
	bool	generate(Generator *);

protected:
	// CSTYLED
	void		put_file(bool (Expr::*func)(Generator *), Generator *);

	ExprList	*defs_;
};

class Accessor : public ExprImpl {
public:
	Accessor(SourcePosition *, Expr *, String *);
	~Accessor();

	void	resolve(Resolver *);
	bool	generate(Generator *);

	Expr	*qualifier()	{ return qualifier_; }

	String	*string()	{ return string_; }

	bool	generate_name(Generator *);
	bool	generate_extern_stubs(Generator *);

protected:
	bool	compute_varying();

	Expr	*qualifier_;
	String	*string_;
};

class Constant : public ExprImpl {
public:
	Constant(SourcePosition *, Identifier *, Expr *, Expr *);
	~Constant();

	void	resolve(Resolver *);
	bool	generate(Generator *);

	Expr	*type()		{ return type_; }
	Expr	*value()	{ return value_; }

	bool	generate_stub(Generator *);
	bool	generate_name(Generator *);

protected:
	Identifier	*ident_;
	Expr		*type_;
	Expr		*value_;
};

class Unary : public ExprImpl {
public:
	Unary(SourcePosition *, Opcode, Expr *);
	~Unary();

	void	resolve(Resolver *);
	bool	generate(Generator *);

private:
	Opcode		op_;
	Expr		*expr_;
};

class Binary : public ExprImpl {
public:
	Binary(SourcePosition *, Opcode, Expr *, Expr *);
	~Binary();

	void	resolve(Resolver *);
	bool	generate(Generator *);

private:
	Opcode		op_;
	Expr		*left_;
	Expr		*right_;
};

class TypeName : public ExprImpl {
public:
	TypeName(SourcePosition *, Expr *, ExprList *);

	// Used to define builtin types.
	TypeName(SourcePosition *p, String *s, String *m,
	    invo_args::arg_type argtype) : ExprImpl(p)
	{
		str_ = s; mapping_ = m; kind_ = argtype; type_ = nil;
		declarators_ = nil;
	}

	~TypeName();

	void	resolve(Resolver *);
	bool	generate(Generator *);

	String		*str()		{ return str_; }
	String		*mapping()	{ return mapping_; }

	invo_args::arg_type	kind()		{ return kind_; }
	Expr			*type()		{ return type_; }
	ExprList		*declarators()	{ return declarators_; }
	bool			seq()		{ return seq_; }

	bool		generate_name(Generator *);
	bool		generate_type_name(Generator *);
	bool		generate_extern_stubs(Generator *);
	bool		generate_stub(Generator *);

protected:
	bool		compute_varying();
	void		put_extra_types(Generator *);

	//
	// This are used for builtin types.
	// type_ and declarators_ will be nil.
	//
	String			*str_;
	String			*mapping_;
	invo_args::arg_type	kind_;

	Expr			*type_;		// type for declarators_
	ExprList		*declarators_;	// list of names typedef'ed
	bool			seq_;		// true if type_ is a sequence
};

class UnsignedType : public ExprImpl {
public:
	UnsignedType(SourcePosition *, Expr *);
	~UnsignedType();

	void	resolve(Resolver *);
	bool	generate(Generator *);
	bool	generate_name(Generator *);

protected:
	Expr	*type_;
};

class CaseElement : public ExprImpl {
public:
	CaseElement(SourcePosition *, ExprList *, UnionMember *);
	~CaseElement();

	void		resolve(Resolver *);
	bool		generate(Generator *);

	bool		generate_copy(bool(Expr::*func)(Generator *),
				Generator *);
	bool		generate_extern_stubs(Generator *);

	ExprList	*labels()	{ return labels_; }
	UnionMember	*element()	{ return element_; }

protected:
	bool		compute_varying();

	ExprList	*labels_;
	UnionMember	*element_;
};

class DefaultLabel : public ExprImpl {
public:
	DefaultLabel(SourcePosition *);
	~DefaultLabel();

	void	resolve(Resolver *);
	bool	generate(Generator *);
	bool	generate_marshal(Generator *);
	bool	generate_unmarshal(Generator *);
	bool	generate_release(Generator *);
	bool	generate_marshal_rollback(Generator *);

protected:
	virtual bool	label(Generator *);
};

class CaseLabel : public DefaultLabel {
public:
	CaseLabel(SourcePosition *, Expr *);
	~CaseLabel();

	void	resolve(Resolver *);
	bool	generate(Generator *);
	bool	generate_name(Generator *);

protected:
	bool	label(Generator *);

	Expr	*value_;
};

class EnumDecl : public ExprImpl {
public:
	EnumDecl(SourcePosition *, Identifier *, ExprList *);
	~EnumDecl();

	void	resolve(Resolver *);
	bool	generate(Generator *);
	bool	generate_name(Generator *);
	bool	generate_type_name(Generator *);

	ExprList	*members()	{ return members_; }

	long		assign_value()	{ return enums_++; }
	long		enums()		{ return enums_; }

protected:
	Identifier	*ident_;
	ExprList	*members_;
	long		enums_;
};

class Enumerator : public ExprImpl {
public:
	Enumerator(SourcePosition *, Identifier *);
	~Enumerator();

	void	resolve(Resolver *);
	bool	generate(Generator *);

	long		value() { return value_; }
	EnumDecl	*decl() { return decl_; }

	bool	generate_name(Generator *);
	bool	generate_type_name(Generator *);

protected:
	Identifier	*ident_;
	EnumDecl	*decl_;
	long		value_;
};

class boolLiteral : public ExprImpl {
public:
	boolLiteral(SourcePosition *, bool);
	~boolLiteral();

	void	resolve(Resolver *);
	bool	generate(Generator *);

private:
	bool	value_;
};

class IntegerLiteral : public ExprImpl {
public:
	IntegerLiteral(SourcePosition *, long);
	~IntegerLiteral();

	void	resolve(Resolver *);
	bool	generate(Generator *);
	bool	generate_type_name(Generator *);

private:
	void	check(Resolver *, Symbol *switch_type);

	long	value_;
};

class FloatLiteral : public ExprImpl {
public:
	FloatLiteral(SourcePosition *, double);
	~FloatLiteral();

	void	resolve(Resolver *);
	bool	generate(Generator *);

private:
	double	value_;
};

class StringLiteral : public ExprImpl {
public:
	StringLiteral(SourcePosition *, String *);
	~StringLiteral();

	void	resolve(Resolver *);
	bool	generate(Generator *);

private:
	String	*value_;
};

class CharLiteral : public ExprImpl {
public:
	CharLiteral(SourcePosition *, long);
	~CharLiteral();

	void	resolve(Resolver *);
	bool	generate(Generator *);

private:
	long	value_;
};

class SrcPos : public ExprImpl {
public:
	SrcPos(SourcePosition *);
	~SrcPos();

	bool	do_source(Generator *);
	bool	generate(Generator *);
};

#endif	/* _EXPR_IMPL_H */
