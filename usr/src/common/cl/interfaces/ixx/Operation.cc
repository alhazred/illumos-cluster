//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

/*
 * Copyright (c) 1992-1993 Silicon Graphics, Inc.
 * Copyright (c) 1993 Fujitsu, Ltd.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Silicon Graphics and Fujitsu may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Silicon Graphics and Fujitsu.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

#pragma ident	"@(#)Operation.cc	1.31	06/05/24 SMI"

//
// Class Operation Methods
//

#include "generator.h"
#include "expr.h"
#include <orb/invo/invo_args.h>
#include <assert.h>
#include "Symbol.h"
#include "Resolver.h"
#include "Operation.h"
#include "Parameter.h"
#include <stdlib.h>
#include "InterfaceDef.h"
#include "SequenceDecl.h"
#include "Declarator.h"
#include "SymbolTable.h"
#include "ExceptDecl.h"
#include <strings.h>

Operation::Operation(SourcePosition *p, Identifier *ident, long version,
    Expr *type, ExprList *params, ExprList *exceptions, long attributes,
    ExprList *context, long pool) :
	ExprImpl(p),
	ident_(ident),
	type_(type),
	params_(params),
	exceptions_(exceptions),
	attributes_(attributes),
	pool_(pool),
	context_(context),
	block_(nil),
	interface_(nil)
{
	version_ = version;
}

Operation::~Operation()
{
}

void
Operation::resolve(Resolver *resolverp)
{
	Symbol *symbolp = resolverp->new_symbol(ident_);
	symbolp->operation(this);
	HashCode *hashp = symbolp->hash();
	hashp->copy_ident(ident_);

	resolverp->push_context();
	type_->resolve(resolverp);
	resolverp->pop_context();

	hashp->add_type(type_);
	interface_ = resolverp->context()->in_symbol->interface();

	symbol_ = symbolp;
	if (params_ != nil) {
		block_ = resolverp->enter_scope(ident_);
		resolve_list(params_, resolverp);
		resolverp->leave_scope();
		interface_->params(params_->count());
		hashp->add_list(params_);

		// Ensure that the marshal size attribute is correctly used.
		check_marshal_size_attribute(resolverp);
	}
	if (exceptions_ != nil) {
		resolverp->push_context(symbolp);
		resolve_list(exceptions_, resolverp);
		resolverp->pop_context();
	}
	switch (attributes_) {
	case TWO_WAY:		/* twoway */
		oneway_ = false;
		nonblocking_ = false;
		unreliable_ = false;
		// Uncomment if using checkpoint
		// checkpoint_ = false;
		break;

	case ONE_WAY:		/* oneway */
		oneway_ = true;
		nonblocking_ = false;
		unreliable_ = false;
		// Uncomment if using checkpoint
		// checkpoint_ = false;
		check_oneway(resolverp);
		break;

	case ONE_WAY_NB_UR:	/* nonblocking unreliable oneway */
		oneway_ = true;
		nonblocking_ = true;
		unreliable_ = true;
		// Uncomment if using checkpoint
		// checkpoint_ = false;
		check_oneway(resolverp);
		break;

	case ONE_WAY_UR:	/* unreliable oneway */
		oneway_ = true;
		nonblocking_ = false;
		unreliable_ = true;
		// Uncomment if using checkpoint
		// checkpoint_ = false;
		check_oneway(resolverp);
		break;

	// Uncomment if using checkpoint
#if 0
	case CHECK_POINT:
		oneway_ = true;
		nonblocking_ = false;
		unreliable_ = false;
		checkpoint_ = true;
		check_oneway(resolverp);
		break;
#endif

	default:    /* twoway */
		oneway_ = false;
		nonblocking_ = false;
		unreliable_ = false;
		// Uncomment if using checkpoint
		// checkpoint_ = false;
		break;
	}
}

//
// check_marshal_size_attribute - verify that a user has only specified the
// "marshal_size" attribute for a type which can support marshal time
// size determination.
//
void
Operation::check_marshal_size_attribute(Resolver *resolverp)
{
	// This method is only called if parameters exist.
	for (ListItr(ExprList) e(*params_); e.more(); e.next()) {
		Symbol *symbolp = e.cur()->symbol();
		Parameter *paramp = symbolp->parameter();
		// valid params_ => paramp != nil

		if (paramp->size_type() == invo_args::size_marshal_time) {

			// Determine argument type.
			Symbol *param_symbolp = paramp->actual_type();
			assert(param_symbolp != nil);
			switch (param_symbolp->type_desc()) {
			case invo_args::t_void:
				// A method argument cannot be void.
				assert(0);
				break;
			case invo_args::t_boolean:
			case invo_args::t_char:
			case invo_args::t_octet:
			case invo_args::t_short:
			case invo_args::t_unsigned_short:
			case invo_args::t_long:
			case invo_args::t_unsigned_long:
			case invo_args::t_longlong:
			case invo_args::t_unsigned_longlong:
#ifdef ORBFLOAT
			case invo_args::t_float:
			case invo_args::t_double:
#endif
				//
				// The above types do not allow marshal time
				// size determination because the compiler
				// can and does determine size.
				//
				{
					ErrorHandler *err =
					    resolverp->handler();
					set_source_position(err);
					err->begin_error();
					err->put_chars("Parameter \"");
					err->put_string(*paramp->declarator()->
					    ident()->string());
					err->put_chars("\" size determined by "
					    "compiler");
					err->end();
				}
				break;

			case invo_args::t_object:
			case invo_args::t_string:
				//
				// These types do not yet support
				// marshal time size determination.
				//
				{
					ErrorHandler *err =
					    resolverp->handler();
					set_source_position(err);
					err->begin_error();
					err->put_chars("Parameter \"");
					err->put_string(*paramp->declarator()->
					    ident()->string());
					err->put_chars("\" marshal_size not yet"
					    " supported for this type");
					err->end();
				}
				break;

			case invo_args::t_addr:
				//
				// These types can allow marshal time
				// size determination.
				//
				break;
			}
		}
	}
}

//
// Check to make sure that an operation specified as oneway
// doesn't try to return anything.
//
void
Operation::check_oneway(Resolver *resolverp)
{
	if (type_->symbol() != SymbolTable::the()->void_type()) {
		resolverp->error(this,
		    "Return value must be \"void\" for oneway operation");
	} else if (params_ != nil) {
		for (ListItr(ExprList) e(*params_); e.more(); e.next()) {
			Symbol *symbolp = e.cur()->symbol();
			Parameter *paramp = symbolp->parameter();
			// valid params_ => paramp != nil
			if (paramp->attr() != invo_args::in_param) {
				ErrorHandler *err = resolverp->handler();
				set_source_position(err);
				err->begin_error();
				err->put_chars("Parameter \"");
				err->put_string(
				    *paramp->declarator()->ident()->string());
				err->put_chars("\" must be passed \"in\" to"
				    " oneway operation");
				err->end();
			}
		}
	}
}

//
// set_index - set the method number for an operation on an interface object.
//
void
Operation::set_index(long n)
{
	index_ = n;
}

//
// Output the type information for the TID string.
//
bool
Operation::generate_type_name(Generator *g)
{
	// Operation name.
	g->start_operation(ident_->string()->string());

	// Operation return value.
	(void) type_->generate_type_name(g);

	// Operation parameters.
	(void) generate_list(params_, &Expr::generate_type_name, g);

	if (exceptions_ != NULL) {
		//
		// Walk through the list of user level exceptions declared
		// for this operation and add them to the type ID string.
		//
		// The entries on the exception list are not ExceptDecl items.
		// The entries are either IdentifierImpl or Accessor items.
		// In either case the scoped name is available from
		// ExprValue in the form of character strings.
		//
		for (ListItr(ExprList) e(*exceptions_); e.more(); e.next()) {
			int i, n = e.cur()->ExprValue.numstr;
			assert(n > 0);
			assert(n <= 3);

			// Compute the string size.
			size_t len = 1;
			for (i = 0; i < n; i++) {
				len += strlen(e.cur()->ExprValue.strings[i]);
				// Account for "::".
				if (i != 0)
					len += 2;
			}
			// Construct the scoped exception name.
			char *str = new char [len];
			(void) strcpy(str, e.cur()->ExprValue.strings[0]);
			for (i = 1; i < n; i++) {
				(void) strcat(str, "::");
				(void) strcat(str,
				    e.cur()->ExprValue.strings[i]);
			}
			// Output the scoped exception name.
			g->add_operation_param(str);
			delete [] str;

			//
			// Output the exception type so its not just the
			// exception name that we are relying on not changing.
			//
			(void) e.cur()->generate_type_name(g);
		}
	}
	return (false);
}

//
// Generate one method (or operation) on the class (or interface).
//
bool
Operation::generate(Generator *g)
{
	if (interface_ == nil)
		return (false);
	String *stringp = ident_->string();
	g->emit("virtual %F %*%I(", stringp, type_);
	g->emit_param_decls(params_, Generator::emit_env_formals);
	g->emit(")%=;\n");
	return (false);
}

//
// Generate one method (or operation) stub header definition.
//
void
Operation::emit_stub_hdr(Generator *g)
{
	if (interface_ == nil)
		return;
	String *stringp = ident_->string();
	g->emit("%E %*%I_stub", stringp, type_);
	g->emit_param_list(params_, Generator::emit_env_formals);
	g->emit(";\n");
}

bool
Operation::generate_name(Generator *g)
{
	g->emit("%I", ident_->string());
	return (true);
}

//
// determine_argument_marshal_order - this method determines the order
// parameters are marshalled and unmarshalled. This ordering does not
// affect the externally visible parameter order used by stubs and
// skels.
//
// At this time parameters are marshalled in the same order externally visible
// to stubs and skels.
//
// It is safe to call this method in any order for the stub and skel.
//
void
Operation::determine_argument_marshal_order(Generator *)
{
	if (params_ == nil) {
		// There are no parameters
		return;
	}

	// The zero position is reserved for the return argument
	int	param_num = 1;

#ifdef MARSHAL_TIME_PARAMS_FIRST
//
// Currently, this performance optimization is turned off.
//
// Under this optimization the system does not marshal parameters
// (method arguments) in the same order that the parameters are specified
// in the IDL file. The system marshals first those parameters requiring
// marshal time size determination. This method specifies the order in
// which parameters are marshalled. The parameters remain in the list according
// to the order specified in the IDL file.
//
	//
	// Those parameters requiring marshal time size determination
	// are marshalled first.
	//
	for (ListItr(ExprList) e(*params_); e.more(); e.next()) {
		// valid params_ => paramp != nil
		Parameter	   *paramp = e.cur()->symbol()->parameter();

		if (paramp->size_type() == invo_args::size_marshal_time) {

			// Determine argument type.
			Symbol	*param_symbolp = paramp->actual_type();
			assert(param_symbolp != nil);
			switch (param_symbolp->type_desc()) {
			case invo_args::t_void:
				// A method argument cannot be void.
			case invo_args::t_boolean:
			case invo_args::t_char:
			case invo_args::t_octet:
			case invo_args::t_short:
			case invo_args::t_unsigned_short:
			case invo_args::t_long:
			case invo_args::t_unsigned_long:
			case invo_args::t_longlong:
			case invo_args::t_unsigned_longlong:
#ifdef ORBFLOAT
			case invo_args::t_float:
			case invo_args::t_double:
#endif	// ORBFLOAT
				//
				// The above types do not allow marshal time
				// size determination because the compiler
				// can and does determine size.
				//
			case invo_args::t_object:
			case invo_args::t_string:
				//
				// These types do not yet support
				// marshal time size determination.
				//
				assert(0);
				break;

			case invo_args::t_addr:
				//
				// These types can allow marshal time
				// size determination.
				//
				break;
			}
			//
			// This method argument does require marshal time
			// size determination.
			//
			assert(paramp->get_marshal_position() == 0 ||
			    paramp->get_marshal_position() == param_num);
			paramp->set_marshal_position(param_num);
			param_num++;
		}
	}
	//
	// Those parameters not requiring marshal time size determination
	// are marshalled next.
	//
	for (ListItr(ExprList) e2(*params_); e2.more(); e2.next()) {
		// valid params_ => paramp != nil
		Parameter	*paramp = e2.cur()->symbol()->parameter();

		if (paramp->size_type() != invo_args::size_marshal_time) {
			//
			// This method argument does not require marshal time
			// size determination.
			//
			assert(paramp->get_marshal_position() == 0 ||
			    paramp->get_marshal_position() == param_num);
			paramp->set_marshal_position(param_num);
			param_num++;
		}
	}
#else	// MARSHAL_TIME_PARAMS_FIRST not enabled

	//
	// Marshal parameters in the standard order.
	//
	for (ListItr(ExprList) e2(*params_); e2.more(); e2.next()) {
		// valid params_ => paramp != nil
		Parameter *paramp = e2.cur()->symbol()->parameter();

		//
		// This method argument does not require marshal time
		// size determination.
		//
		assert(paramp->get_marshal_position() == 0 ||
		    paramp->get_marshal_position() == param_num);
		paramp->set_marshal_position(param_num);
		param_num++;
	}
#endif	// MARSHAL_TIME_PARAMS_FIRST

	// This checks that all parameters received a marshal order number.
	assert(param_num == params_->count() + 1);
}

//
// generate_arg_desc_arg_sizes - creates the data structure
// arg_desc and arg_sizes for this operation (method).
//
void
Operation::generate_arg_desc_arg_sizes(Generator *g, long nparams,
    invo_args::arg_type	return_argtype)
{
	String *s = ident_->string();

	// Specify size information about the arguments for this method.
	arg_sizes argsizes = { nparams, 0, 0, 0, 0, 0, 0 };

	// Record size of the return value.
	argsizes.out_size = type_->ExprValueSize();
	argsizes.out_objtref = type_->ExprValueObjtref();
	argsizes.out_unknown = type_->ExprValueUnknown();

	// Emit an array describing the method arguments.
	g->emit("static arg_desc _%_%I_pdesc[", s);
	// Array size equals the number of parameters.
	g->emit_integer(nparams);
	g->emit("] = {\n%i");
	//
	// First field contains return argument descriptor.
	// Create descriptor containing type and mode.
	//
	arg_desc descriptor(return_argtype, invo_args::out_param,
		    invo_args::size_unknown);
	g->emit_hexinteger((uint_t)descriptor);
	//
	// The type of each parameter is emitted.
	// Size information is accumulated while processing each arg.
	//
	generate_param_desc(g, nparams, argsizes);
	g->emit("%u\n};\n");

	// Emit an array describing the total size of in and out parameters.
	g->emit("static arg_sizes _%_%I_argsizes = {\n%i", s);
	g->emit_integer(argsizes.num_params);
	g->emit(", ");
	g->emit_integer(argsizes.in_size);
	g->emit(", ");
	g->emit_integer(argsizes.in_unknown);
	g->emit(", ");
	g->emit_integer(argsizes.in_objtref);
	g->emit(", ");
	g->emit_integer(argsizes.out_size);
	g->emit(", ");
	g->emit_integer(argsizes.out_unknown);
	g->emit(", ");
	g->emit_integer(argsizes.out_objtref);
	g->emit(", ");
	g->emit_integer(argsizes.num_marshal_size);
	g->emit("%u\n};\n");
}

//
// generate_ArgMarshal - creates the data structure
// ArgMarshal for this operation (method).
//
void
Operation::generate_ArgMarshal(Generator *g, long nparams)
{
	if (has_marshal_funcs(g)) {
		String *s = ident_->string();
		g->emit("ArgMarshalFuncs _%_%I_pfunc[", s);
		g->emit_integer(nparams);
		g->emit("] = {\n%i");
		g->need_sep(false);

		generate_param_marshal(g);

		if (g->need_sep(false)) {
			g->emit(",\n");
		}
		// This is for the return value...
		generate_marshal_func(g, type_, invo_args::out_param);
		g->emit("\n%u};\n");
	}
}

//
// generate_arg_info - creates the data structure
// arg_info for this operation (method).
//
void
Operation::generate_arg_info(Generator *g)
{
	String *s = ident_->string();
	g->emit("arg_info _%_%I_pinfo(\n%i", s);
	g->emit("_Ix__tid(%S_");		// corba type ID
	g->emit_integer(version());
	g->emit("),\n");
	g->emit_integer(index_);		// method
	g->emit(",\n_%_%I_pdesc,\n", s);	// param descriptions
	if (has_marshal_funcs(g)) {		// function descriptors
		g->emit("_%_%I_pfunc", s);
	} else {
		g->emit("NULL");
	}
						// size descriptor structure
	g->emit(",\n&_%_%I_argsizes,\n", s);
						// emit invocation mode
	if (pool_ == RECONFIG_OPTION) {
		g->emit("invocation_mode::RECONFIG | ");
	}
	if (oneway_) {
		if (nonblocking_ && unreliable_) {
			g->emit("invocation_mode::ONEWAY |"
			    " invocation_mode::NONBLOCKING |"
			    " invocation_mode::UNRELIABLE");
		} else if (unreliable_) {
			g->emit("invocation_mode::ONEWAY |"
			    " invocation_mode::UNRELIABLE");
		} else {
			g->emit("invocation_mode::ONEWAY");
		}
#if 0	// define if checkpoint is needed
	} else if (checkpoint_) {
		g->emit("invocation_mode::ONEWAY |"
		    "invocation_mode::SYNCHRONOUS");
#endif
	} else {
		g->emit("invocation_mode::NONE");
	}
	g->emit(");%u\n\n");
}

//
// generate_stub_code - creates the client code or stub
// for this operation (method).
//
void
Operation::generate_stub_code(Generator *g, long nparams,
    invo_args::arg_type	return_argtype)
{
	String *stringp = ident_->string();
	bool has_return_value = !type_->void_type();

	//
	// The Intel compiler has a problem with certain cases of
	// struct or union return (see 4087749).  Until the compiler
	// rejects these with a nice error message pointing to the
	// offending line of the input file, we insert an error
	// directive into the stubs file so compilation will fail.
	// Note that variable-length structs and unions are returned
	// as a pointer so those aren't a problem.
	//
	if (!oneway_ && !type_->varying() &&
	    ((type_->actual_type()->struct_tag() != nil) ||
	    (type_->actual_type()->union_tag() != nil))) {
		g->emit("#error %I returns struct or union\n", stringp);
	}

	// Generate the method declaration.
	if (has_return_value) {
		g->emit("%F %*\n%~_stub::%I", stringp, type_);
	} else {
		g->emit("%F\n%~_stub::%I", stringp, type_);
	}
	g->emit_param_list(params_, Generator::emit_env_formals_body);
	g->emit("\n{\n%i");

	// A TNF probe records when the stub begins execution.
	g->emit("MC_PROBE_0(%_%I_start,\n%i"
	    "\"%m %n clustering\", \"\");\n%u", stringp);

	// Initialize a local handler variable.
	g->emit("handler *_handlerp = _handler();\n");

	g->emit("ExceptionStatus _status;\n");
	g->emit("%~%p this_obj;\n", nil, this);

	// Declare the result
	if (has_return_value) {
		g->emit("%F %*_result;\n", nil, type_);
	}

	g->emit("\n"
	    "do {\n%i");

	//
	// Body of "do while" loop.
	// First, get the local implementation pointer if available.
	//
	g->emit("this_obj = (%~%p)\n"
	    "    _handlerp->local_invoke(_Ix__tid(%S_", nil, this);
	g->emit_integer(version());
	g->emit("),\n"
	    "\t_deep_type(), _environment);\n");

	// Check for exceptions from local_invoke().
	g->emit("if (_environment.exception() != NULL) {\n%i"
	    "_status = CORBA::LOCAL_EXCEPTION;\n"
	    "break;\n%u"
	    "} else if (CORBA::is_nil(this_obj)) {\n%i");

	//
	// Not a local object or exception, prepare for remote invocation.
	// First line contains declaration for the array holding the arguments.
	//
	g->emit("ArgValue ");
	generate_arg(g, nparams, ";\n");

	//
	// Second line is optional.
	// When present it identifies the return value.
	//
	if (return_argtype == invo_args::t_addr) {
		// Pass address of return value.
		generate_arg(g, 0, ".t_addr = &_result;\n");
	}

	// Subsequent lines identify the method parameters.
	generate_param_value(g);

	// Generate the remote invocation.
	g->emit("_status = _handlerp->invoke(_%_%I_pinfo,\n%i"
	    "_arg, _deep_type()%,%a);\n%u", stringp, this);

	if (has_return_value) {
		generate_result_value(g, type_);
	}

	g->emit("%u} else {\n%i");

	// Perform local invocation using the local object pointer.
	if (has_return_value) {
		g->emit("_result = ");
	}
	g->emit("this_obj->%I", stringp);
	g->emit_param_list(params_, Generator::emit_env_actuals);
	g->emit(";\n");
	g->emit("_status = _handlerp->local_invoke_done(_environment);\n");

	g->emit("%u}\n");

	// End of "do while".
	g->emit("%u} while (_status == CORBA::RETRY_EXCEPTION);\n");

	// Generate debug code to check for illegal exceptions.
	operation_exception_validation(g);

	// A TNF probe records the time of stub completion.
	g->emit("MC_PROBE_0(%_%I_end,\n%i", stringp);
	g->emit("\"%m %n clustering\", \"\");\n%u", nil, this);

	if (has_return_value) {
		g->emit("return (_result);\n");
	}
	g->emit("%u}\n");		// End of stub function
}

//
// operation_exception_validation -
// Generate debug code to check for illegal exceptions in stub
// just prior to returning results to client.
//
// The system can declare any SystemException for any operation.
//
// The IDL specification file must declare all of the permissible
// user level exceptions.
//
void
Operation::operation_exception_validation(Generator *g)
{
	g->emit("\n%#ifdef DEBUG\n"
	    "// Validate that only allowed exceptions occurred\n"
	    "CORBA::Exception *exceptp = _environment.exception();\n"
	    "if (exceptp != NULL) {\n%i"
	    "if (!((CORBA::SystemException::_exnarrow(exceptp) != NULL)");

	if (exceptions_ != NULL) {
		//
		// Walk through the list of user level exceptions declared
		// for this operation.
		//
		// The entries on the exception list are not ExceptDecl items.
		// The entries are either IdentifierImpl or Accessor items.
		// In either case the scoped name is available from
		// ExprValue in the form of character strings.
		//
		for (ListItr(ExprList) e(*exceptions_); e.more(); e.next()) {
			int	num_strings = e.cur()->ExprValue.numstr;
			assert(num_strings > 0);
			assert(num_strings <= 3);

			g->emit(" ||\n"
			    "(");

			// Output the scoped exception name
			for (int i = 0; i < num_strings; i++) {
				g->emit_str(e.cur()->ExprValue.strings[i],
				    strlen(e.cur()->ExprValue.strings[i]));
				g->emit("::");
			}

			g->emit("_exnarrow(exceptp) != NULL)");
		}
	}

	g->emit(")) {\n%i"
	    "exceptp->print_exception(\"Illegal exception on \"\n%i"
	    "\"operation %:%I\");\n%u%u"
	    "}\n"
	    "_environment.mark_used();\n%u"
	    "}\n"
	    "%#endif // DEBUG\n\n",
	    ident_->string());
}

//
// generate_stub - generates the stub for one method
//
bool
Operation::generate_stub(Generator *g)
{
	long	nparams = (params_ == nil) ? 1 : params_->count() + 1;
	invo_args::arg_type	return_argtype;

	//
	// Generate extern definitions for any marshal routines we need.
	// Most of the code needed will have already been output since the
	// IDL definition has to preceed the use in a parameter or return
	// value. The exceptions are if the IDL definition is in an included
	// file or the parameter has a subscript (array).
	//
	if (type_->generate_extern_stubs(g)) {
		g->emit("\n");
	}
	if (generate_list(params_, &Expr::generate_extern_stubs, g)) {
		g->emit("\n");
	}

	//
	// Determine return argument type
	// OR oneway_ with checkpoint_ if checkpoint is needed
	//
	if (oneway_) {
		return_argtype =
		    SymbolTable::the()->oneway_type()->type_name()->kind();
	} else {
		Symbol	*symbolp = type_->actual_type();
		assert(symbolp != nil);
		return_argtype = symbolp->type_desc();
	}

	determine_argument_marshal_order(g);

	generate_arg_desc_arg_sizes(g, nparams, return_argtype);

	generate_ArgMarshal(g, nparams);

	generate_arg_info(g);

	generate_stub_code(g, nparams, return_argtype);

	return (true);
}

//
// Output a stub wrapper routine.
//
void
Operation::emit_wrapper_stub(Generator *g)
{
	long	nparams = (params_ == nil) ? 1 : params_->count() + 1;
	invo_args::arg_type	return_argtype;

	//
	// Determine return argument type
	// OR oneway_ with checkpoint_ if checkpoint is needed
	//
	if (oneway_) {
		return_argtype =
		    SymbolTable::the()->oneway_type()->type_name()->kind();
	} else {
		Symbol	*symbolp = type_->actual_type();
		assert(symbolp != nil);
		return_argtype = symbolp->type_desc();
	}

	String *stringp = ident_->string();
	bool has_return_value = !type_->void_type();

	//
	// The Intel compiler has a problem with certain cases of
	// struct or union return (see 4087749).  Until the compiler
	// rejects these with a nice error message pointing to the
	// offending line of the input file, we insert an error
	// directive into the stubs file so compilation will fail.
	// Note that variable-length structs and unions are returned
	// as a pointer so those aren't a problem.
	//
	if (!oneway_ && !type_->varying() &&
	    ((type_->actual_type()->struct_tag() != nil) ||
	    (type_->actual_type()->union_tag() != nil))) {
		g->emit("#error %I returns struct or union\n", stringp);
	}

	// Generate the method declaration.
	if (has_return_value) {
		g->emit("%F %*\n%~_stub::%I", stringp, type_);
	} else {
		g->emit("%F\n%~_stub::%I", stringp, type_);
	}
	g->emit_param_list(params_, Generator::emit_env_formals_body);
	g->emit("\n{\n%i");

	if (has_return_value) {
		g->emit("return (");
		g->emit_scope(interface_->info()->block);
		g->emit("_stub::%I", stringp);
		g->emit_param_list(params_, Generator::emit_env_actuals);
		g->emit(");\n");
	} else {
		g->emit_scope(interface_->info()->block);
		g->emit("_stub::%I", stringp);
		g->emit_param_list(params_, Generator::emit_env_actuals);
		g->emit(";\n");
	}
	g->emit("%u}\n");
}

//
// Output a version exception routine.
//
void
Operation::emit_versionex_stub(Generator *g)
{
	long	nparams = (params_ == nil) ? 1 : params_->count() + 1;
	invo_args::arg_type	return_argtype;

	//
	// Determine return argument type
	// OR oneway_ with checkpoint_ if checkpoint is needed
	//
	if (oneway_) {
		return_argtype =
		    SymbolTable::the()->oneway_type()->type_name()->kind();
	} else {
		Symbol	*symbolp = type_->actual_type();
		assert(symbolp != nil);
		return_argtype = symbolp->type_desc();
	}

	String *stringp = ident_->string();
	bool has_return_value = !type_->void_type();

	//
	// The Intel compiler has a problem with certain cases of
	// struct or union return (see 4087749).  Until the compiler
	// rejects these with a nice error message pointing to the
	// offending line of the input file, we insert an error
	// directive into the stubs file so compilation will fail.
	// Note that variable-length structs and unions are returned
	// as a pointer so those aren't a problem.
	//
	if (!oneway_ && !type_->varying() &&
	    ((type_->actual_type()->struct_tag() != nil) ||
	    (type_->actual_type()->union_tag() != nil))) {
		g->emit("#error %I returns struct or union\n", stringp);
	}

	// Generate the method declaration.
	if (has_return_value) {
		g->emit("%F %*\n%:%I(", stringp, type_);
	} else {
		g->emit("%F\n%:%I(", stringp, type_);
	}
	if (params_ != nil) {
		bool save = g->op_body(false);
		g->emit_param_decls(params_, Generator::emit_formals);
		(void) g->op_body(save);
		g->emit(", ");
	}
	g->emit("Environment &_environment)\n"
	    "{\n%i"
	    "_environment.system_exception("
	    "CORBA::VERSION(0, CORBA::COMPLETED_NO));\n");

	if (has_return_value) {
		g->emit("return (nil);\n");
	}
	g->emit("%u}\n");
}

//
// generate_marshal_func - outputs the pointer to a struct containing
// information needed to support one method parameter at marshal time.
//
// static
void
Operation::generate_marshal_func(Generator *g, Expr* exprp,
    invo_args::arg_mode)
{
	Symbol	*symbolp = exprp->actual_type();
	switch (symbolp->tag()) {

	case Symbol::sym_unknown:
	case Symbol::sym_module:
	case Symbol::sym_constant:
	case Symbol::sym_operation:
	case Symbol::sym_parameter:
	case Symbol::sym_exception:
	case Symbol::sym_enum_value:
	case Symbol::sym_member:
	case Symbol::sym_union_member:
		// The above types should not occur as a parameter actual type
		assert(0);
		break;

	case Symbol::sym_string:
	case Symbol::sym_typedef:
	case Symbol::sym_enum:
		// Don't need marshal functions
		g->emit("ArgMarshalFuncs()");
		break;

	case Symbol::sym_interface:
		g->emit("ArgMarshalFuncs(&_%Y_ArgMarshalFuncs)",
		    nil, symbolp->interface());
		break;

	case Symbol::sym_sequence:
		if (symbolp->sequence_type()->type()->string_type()) {
			// This is a sequence of strings
			g->emit("ArgMarshalFuncs(&CORBA::StringSeq::"
			    "ArgMarshalFuncs)");
			break;
		}
		// Normal sequence. Fall through

	case Symbol::sym_array:
	case Symbol::sym_struct:
	case Symbol::sym_union:
		g->emit("ArgMarshalFuncs(&_%Y_ArgMarshalFuncs)", nil, exprp);
		break;
	}
}

bool
Operation::has_marshal(Generator *, Expr *e)
{
	switch (e->actual_type()->tag()) {
	case Symbol::sym_string:
	case Symbol::sym_typedef:
	case Symbol::sym_enum:
		return (false);
	}
	return (true);
}

bool
Operation::has_marshal_funcs(Generator *g)
{
	if (has_marshal(g, type_)) {
		return (true);
	}
	if (params_ != nil) {
		for (ListItr(ExprList) e(*params_); e.more(); e.next()) {
			Parameter *p = e.cur()->symbol()->parameter();
			// valid params_ => p != nil
			Declarator *d = p->declarator();
			Expr *t = (d->subscripts() != nil) ? d : p->type();
			if (has_marshal(g, t)) {
				return (true);
			}
		}
	}
	return (false);
}

//
// generate_param_desc - determine type of each argument and output that value.
// Accumulate the size information for each argument. The method makes two
// passes through the parameter list. The first pass outputs those parameters
// requiring marshal time size determination. The second pass outputs the
// remaining parameters.
//
void
Operation::generate_param_desc(Generator *g, long nparams,
    arg_sizes &argsizes)
{
	assert(argsizes.num_marshal_size == 0);

	if (params_ == nil) {
		assert(nparams == 1);
		// No parameters
		return;
	}

	// The zero position is reserved for the return argument
	int	param_num = 1;

#ifdef MARSHAL_TIME_PARAMS_FIRST
	//
	// Those parameters requiring marshal time size determination
	// are marshalled first.
	//
	for (ListItr(ExprList) e(*params_); e.more(); e.next()) {
		// valid params_ => paramp != nil
		Parameter	*paramp = e.cur()->symbol()->parameter();

		if (paramp->size_type() == invo_args::size_marshal_time) {
			//
			// This method argument does require marshal time
			// size determination
			//
			assert(paramp->get_marshal_position() == param_num);

			g->emit(", ");
			paramp->process_arg_desc(g, argsizes);
			param_num++;
			argsizes.num_marshal_size++;
		}
	}
	//
	// Those parameters not requiring marshal time size determination
	// are marshalled next.
	//
	for (ListItr(ExprList) e2(*params_); e2.more(); e2.next()) {
		// valid params_ => paramp != nil
		Parameter	*paramp = e2.cur()->symbol()->parameter();

		if (paramp->size_type() != invo_args::size_marshal_time) {
			//
			// This method argument does not require marshal time
			// size determination
			//
			assert(paramp->get_marshal_position() == param_num);

			g->emit(", ");
			paramp->process_arg_desc(g, argsizes);
			param_num++;
		}
	}
#else	// MARSHAL_TIME_PARAMS_FIRST not enabled

	//
	// Marshal parameters in normal order
	//
	for (ListItr(ExprList) e2(*params_); e2.more(); e2.next()) {
		// valid params_ => paramp != nil
		Parameter	*paramp = e2.cur()->symbol()->parameter();

		//
		// This method argument does not require marshal time
		// size determination
		//
		assert(paramp->get_marshal_position() == param_num);

		g->emit(", ");
		paramp->process_arg_desc(g, argsizes);
		param_num++;
	}
#endif	// MARSHAL_TIME_PARAMS_FIRST

	// This checks that all parameters received a marshal order number
	assert(param_num == params_->count() + 1);
}

//
// generate_param_marshal - outputs the pointer to a struct containing
// information supporting marshal time activity.
//
void
Operation::generate_param_marshal(Generator *g)
{
	if (params_ == nil) {
		// No parameters
		return;
	}

	// The zero position is reserved for the return argument
	int	param_num = 1;

#ifdef MARSHAL_TIME_PARAMS_FIRST
	//
	// Those parameters requiring marshal time size determination
	// are marshalled first.
	//
	for (ListItr(ExprList) e(*params_); e.more(); e.next()) {
		// valid params_ => paramp != nil
		Parameter	*paramp = e.cur()->symbol()->parameter();

		if (paramp->size_type() == invo_args::size_marshal_time) {
			//
			// This method argument does require marshal time
			// size determination
			//
			assert(paramp->get_marshal_position() == param_num);
			param_num++;

			Declarator *declarp = paramp->declarator();
			Expr *exprp = (declarp->subscripts() != nil) ?
			    declarp : paramp->type();
			generate_marshal_func(g, exprp, paramp->attr());
			g->emit(",\n");
		}
	}
	//
	// Those parameters not requiring marshal time size determination
	// are marshalled next.
	//
	for (ListItr(ExprList) e2(*params_); e2.more(); e2.next()) {
		// valid params_ => paramp != nil
		Parameter	*paramp = e2.cur()->symbol()->parameter();

		if (paramp->size_type() != invo_args::size_marshal_time) {
			//
			// This method argument does not require marshal time
			// size determination
			//
			assert(paramp->get_marshal_position() == param_num);
			param_num++;

			Declarator *declarp = paramp->declarator();
			Expr *exprp = (declarp->subscripts() != nil) ?
			    declarp : paramp->type();
			generate_marshal_func(g, exprp, paramp->attr());
			g->emit(",\n");
		}
	}
#else	// MARSHAL_TIME_PARAMS_FIRST not enabled
	//
	// Marshal parameters in normal order
	//
	for (ListItr(ExprList) e2(*params_); e2.more(); e2.next()) {
		// valid params_ => paramp != nil
		Parameter	*paramp = e2.cur()->symbol()->parameter();

		assert(paramp->get_marshal_position() == param_num);
		param_num++;

		Declarator *declarp = paramp->declarator();
		Expr *exprp = (declarp->subscripts() != nil) ?
		    declarp : paramp->type();
		generate_marshal_func(g, exprp, paramp->attr());
		g->emit(",\n");
	}
#endif	// MARSHAL_TIME_PARAMS_FIRST
}

//
// generate_param_value - loads appropriate information for each method
// argument (not including the return value) into the ArgValue array
// for the stub method. The arguments are processed according to the IDL
// order, while the ArgValue array uses the marshal order.
//
void
Operation::generate_param_value(Generator *g)
{
	if (params_ == nil) {
		// No parameters
		return;
	}

	for (ListItr(ExprList) e(*params_); e.more(); e.next()) {
		// valid params_ => paramp != nil
		Parameter	*paramp = e.cur()->symbol()->parameter();
		bool		in_param =
				    (paramp->attr() == invo_args::in_param);
		Declarator	*declarp = paramp->declarator();

		//
		// This contains the name of the operation (method) argument
		//
		Identifier	*namep = declarp->ident();

		Symbol		*symbolp = declarp->actual_type();
		TypeName	*type_namep;

		switch (symbolp->tag()) {
		case Symbol::sym_string:
			if (in_param) {
				generate_arg(g, paramp->get_marshal_position(),
				    ".t_const_string = ");
				g->emit("%E;\n", nil, namep);
			}
			break;
		case Symbol::sym_typedef:
			type_namep = symbolp->type_name();
			if (in_param) {
				generate_arg(g, paramp->get_marshal_position(),
				    ".t_");
				//
				// Need to compare type_name's because
				// declarator have their own symbols.
				//
				if (type_namep == SymbolTable::the()->
				    string_type()->type_name()) {
					// The param is a string
					g->emit("const_");
				}
				g->emit("%I = %E;\n", type_namep->str(), namep);
			}
			break;
		case Symbol::sym_enum:
			if (in_param) {
				generate_arg(g, paramp->get_marshal_position(),
				    ".t_long = ");
				g->emit("%E;\n", nil, namep);
			}
			break;
		case Symbol::sym_interface:
			if (in_param) {
				generate_arg(g, paramp->get_marshal_position(),
				    ".t_objref = ");
				symbolp->interface()->put_cast_up(g);
				g->emit("%E;\n", nil, namep);
			    }
			break;
		default:
			in_param = false;
			break;
		}

		if (!in_param) {
			generate_arg(g, paramp->get_marshal_position(),
			    ".t_addr = ");
			const char *fmt;
			if (symbolp->tag() == Symbol::sym_array) {
				fmt = "%E;\n";
			} else {
				fmt = "&%E;\n";
			}
			g->emit(fmt, nil, namep);
		}
	}
}

void
Operation::generate_result_value(Generator *g, Expr *typep)
{
	Symbol	*symbolp = typep->actual_type();
	switch (symbolp->tag()) {
	case Symbol::sym_enum:
		g->emit("_result = (%F)_arg[0].t_long;\n", nil, typep);
		break;
	case Symbol::sym_interface:
		g->emit("_result = (%F)_arg[0].t_vaddr;\n", nil, typep);
		break;
	case Symbol::sym_string:
		g->emit("_result = _arg[0].t_string;\n");
		break;
	case Symbol::sym_typedef:
		g->emit("_result = _arg[0].t_%I;\n",
		    symbolp->type_name()->str());
		break;
	default:
		// _result filled in by dereferencing pointer passed in with
		// args.
		break;
	}
}

//
// generate_arg
//
// static
void
Operation::generate_arg(Generator *g, long n, const char *str)
{
	g->emit("_arg[");
	g->emit_integer(n);
	g->emit("]");
	g->emit(str);
}

bool
Operation::generate_receive_table(Generator *g)
{
	g->emit("_%_%I_receive", ident_->string(), this);
	return (true);
}

//
// generate_server - produces the server side function (skeleton)
// that processes requests from the client.
//
bool
Operation::generate_server(Generator *g)
{
	String *s = ident_->string();
	bool has_return = !type_->void_type();

	// Generate method declaration.
	g->emit("static void\n");
	g->emit("_%_%I_receive(void *objectp, service &_stream)\n", s, this);
	g->emit("{\n%i");

	// A TNF probe records the skel start time.
	g->emit("MC_PROBE_0(%_%I_skel_start,\n%i"
	    "\"%m %n clustering\", \"\");\n%u", s);

	//
	// Declare local variables.
	//
	g->emit("%~%p _this = (%~%p)objectp;\n");
	if (g->has_env()) {
		g->emit_env_param(Generator::emit_formals);
		g->emit(" = *(_stream.get_env());\n");
	}
	g->emit("extern arg_info _%_%I_pinfo;\n", s);
	g->emit("//lint -e530 -e603\n");
	g->emit("ArgValue ");
	long nparams = params_ == nil ? 1 : params_->count() + 1;
	generate_arg(g, nparams, ";\n");
	Symbol *symbolp = type_->actual_type();
	assert(symbolp != nil);
	invo_args::arg_type	argtype = symbolp->type_desc();
	if (has_return && argtype == invo_args::t_addr) {
		g->emit("%F %*_result;\n", nil, type_);
	}

	if (params_ != nil) {
		determine_argument_marshal_order(g);

		// Identify addreses for arguments.
		generate_receive_addresses(g);

		g->emit("_stream.unmarshal_receive(_%_%I_pinfo, _arg);\n", s);
	}

	//
	// Generate method invocation from stub.
	//
	if (has_return) {
		if (argtype == invo_args::t_addr) {
			g->emit("_result = ");
		} else {
			generate_receive_asg(g, type_);
		}
	}
	g->emit("_this->%I", s);
	bool b = g->array_decl(false);
	g->emit_param_list(params_, Generator::emit_env_actuals);
	g->array_decl(b);
	g->emit(";\n");
	if (has_return && argtype == invo_args::t_addr) {
		generate_receive_asg(g, type_);
		g->emit("_result;\n");
	}

	g->emit("//lint +e530 +e603\n");

	//
	// Generate function that completes invocation.
	//
	if (oneway_) {
		//
		// This substitutes compile time information about
		// the one/two-way status of an invocation for a run time
		// check of what is an invariant property.
		//
		// Oneway invocation.
		g->emit("_stream.request_complete(_%_%I_pinfo, _arg);\n", s);
	} else {
		// Twoway invocation.
		g->emit("_stream.send_reply(_this, _%_%I_pinfo, _arg);\n", s);
	}
	// A TNF probe records the skel completion time.
	g->emit("MC_PROBE_0(%_%I_skel_end,\n%i"
	    "\"%m %n clustering\", \"\");\n%u", s);

	g->emit("%u}\n\n");
	return (false);
}

//
// generate_receive_addresses - generates the code in the skeleton that
// declares a local variable for each method argument and provides location
// information to the ArgValue array for each method argument.
//
void
Operation::generate_receive_addresses(Generator *g)
{
	for (ListItr(ExprList) e(*params_); e.more(); e.next()) {
		// valid params_ => parameter() != nil
		Parameter	*paramp = e.cur()->symbol()->parameter();

		//
		// The system should have already assigned
		// a marshal position for each argument.
		//
		assert(0 != paramp->get_marshal_position());

		Expr		*typep = paramp->type();
		Expr		*valuep = paramp->declarator();

		//
		// Declare a local variable of the correct type with the
		// IDL specified variable name.
		//
		g->emit("%F ", nil, typep);
		if (paramp->attr() == invo_args::out_param &&
		    !g->has_copy_outs() &&
		    typep->varying() &&
		    !typep->string_type() &&
		    !typep->interface_type()) {
			g->emit("*");
		}
		g->emit("%E", nil, valuep);

		// Initialize local variables that are pointers to 0;
		if (paramp->attr() == invo_args::out_param &&
		    !g->has_copy_outs() &&
		    typep->varying() &&
		    !typep->string_type() &&
		    !typep->interface_type()) {
			g->emit(" = NULL");
		}
		g->emit(";\n");
		//
		// Load ArgValue array entry with location of the local variable
		//
		generate_arg(g, paramp->get_marshal_position(), ".t_addr = ");
		if (valuep->actual_type()->tag() != Symbol::sym_array) {
			g->emit("&");
		}
		bool b = g->array_decl(false);
		g->emit("%E;\n", nil, valuep);
		g->array_decl(b);
	}
}

void
Operation::generate_receive_asg(Generator *g, Expr *type)
{
	Symbol *symbolp = type->actual_type();
	generate_arg(g, 0, ".t_");
	switch (symbolp->tag()) {
	case Symbol::sym_string:
		g->emit("string = ");
		break;
	case Symbol::sym_typedef:
		g->emit("%I = ", symbolp->type_name()->str());
		break;
	case Symbol::sym_enum:
		g->emit("long = ");
		break;
	case Symbol::sym_interface:
		g->emit("objref = ");
		symbolp->interface()->put_cast_up(g);
		break;
	default:
		g->emit("addr = &");
		break;
	}
}
