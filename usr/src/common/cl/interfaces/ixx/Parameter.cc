//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

/*
 * Copyright (c) 1992-1993 Silicon Graphics, Inc.
 * Copyright (c) 1993 Fujitsu, Ltd.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Silicon Graphics and Fujitsu may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Silicon Graphics and Fujitsu.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

#pragma ident	"@(#)Parameter.cc	1.12	06/05/24 SMI"

//
// Class Parameter Methods
//

#include "generator.h"
#include "expr.h"
#include <orb/invo/invo_args.h>
#include <assert.h>
#include "Symbol.h"
#include "Resolver.h"
#include "Parameter.h"
#include "InterfaceDef.h"
#include "Declarator.h"
#include "SequenceDecl.h"
#include "SymbolTable.h"

Parameter::Parameter(SourcePosition *pos, invo_args::arg_size_info size_type,
    invo_args::arg_mode attr, Expr *type, Identifier *ident,
    ExprList *subscripts) :
	ExprImpl(pos),
	marshal_position_(0),
	size_type_(size_type),
	attr_(attr),
	type_(type)
{
	if (ident == nil) {
		declarator_ = nil;
	} else {
		declarator_ = new Declarator(pos, ident, subscripts);
	}
}

Parameter::~Parameter()
{
}

void
Parameter::resolve(Resolver *resolverp)
{
	if (attr_ == invo_args::err_param) {
		resolverp->error(this, "Missing attribute for parameter");
	}
	resolverp->push_context();
	type_->resolve(resolverp);
	Symbol *symbolp = resolverp->context()->out_symbol;
	resolverp->pop_context();
	if (declarator_ == nil) {
		resolverp->error(this, "Missing parameter name");
	} else {
		Symbol *type_symbolp = type_->symbol();
		if (type_symbolp != nil) {
			if (type_symbolp->tag() == Symbol::sym_sequence) {
				resolverp->error(this, "Anonymous types not "
				    "supported for parameters");
			}
			InterfaceDef *inf = type_symbolp->interface();
			if (inf != nil && inf->info()->server_only) {
				resolverp->symerr(this, inf->string(),
				    "Server only interfaces "
				    "cannot be used as a parameter");
			}
		}
		symbol_ = new Symbol(resolverp->scope());
		symbol_->parameter(this);
		resolverp->push_context(symbolp, type_);
		declarator_->resolve(resolverp);
		resolverp->pop_context();
		symbol_->hash()->add_type(type_);
	}
}

//
// Generate a parameter declaration.  The basic idea for "in" parameters
// is that primitive types (char, short, long) and object pointers
// are passed by value, while structured types (string, sequence, struct)
// are passed by const reference.  For "inout" or "out" parameters,
// everything is passed non-const reference.  Arrays are the strange case,
// as they are passed by "value pointer" meaning the mode looks like
// by-value but is really by-reference.
//
bool
Parameter::generate(Generator *g)
{
	bool formal = g->formals();
	bool array_dec = g->array_decl(formal);
	if (formal) {
		bool ref = g->interface_is_ref(false);
		Symbol *symbolp = type_->actual_type();
		if (symbolp->tag() == Symbol::sym_array) {
			put_array_param(g);
		} else if (symbolp == SymbolTable::the()->string_type()) {
			put_string_param(g);
		} else if (symbolp->tag() == Symbol::sym_interface) {
			put_objref_param(g);
		} else {
			put_param(g);
		}
		g->interface_is_ref(ref);
	}
	declarator_->generate(g);
	g->array_decl(array_dec);
	return (true);
}

void
Parameter::put_array_param(Generator *g)
{
	if (attr_ == invo_args::in_param) {
		g->emit("const ");
	}
	if (attr_ == invo_args::out_param && type_->varying()) {
		g->emit("%E_slice *", nil, type_);
	} else {
		g->emit("%E ", nil, type_);
	}
}

//
// Generate the parameter type for a string.
// This is complicated by C++ outs, which need to be a class
// so that String_var can know to release the old value
// (making their use "deprecated" according to the CORBA spec).
//
void
Parameter::put_string_param(Generator *g)
{
	if (attr_ == invo_args::out_param) {
		g->emit("CORBA::String_out ");
	} else {
		if (attr_ == invo_args::in_param) {
			g->emit("const ");
		}
		g->emit("%E ", nil, type_);
		if (attr_ == invo_args::inout_param) {
			g->emit("&");
		}
	}
}

static const char *objref_param_suffix[] = { "??", "_ptr ", "_out ", "_ptr &" };

void
Parameter::put_objref_param(Generator *g)
{
	Symbol		*tsym = type_->symbol();
	InterfaceDef	*interfacep = tsym->interface();
	Scope		*scopep = symbol_->scope()->outer;
	if (interfacep != nil && tsym->is_top_level()) {
		g->emit("%^");
	}
	bool in_scope = (scopep == tsym->scope() ||
	    (interfacep != nil && interfacep->block() == scopep));
	g->emit(in_scope ? "%E" : "%F", nil, type_);
	g->emit(objref_param_suffix[attr_]);
}

void
Parameter::put_param(Generator *g)
{
	bool addr = type_->addr_type();
	if (addr && attr_ == invo_args::in_param) {
		g->emit("const ");
	}
	bool t_out = (attr_ == invo_args::out_param &&
	    !g->has_copy_outs() && type_->varying());

	Scope *tscope = type_->symbol()->scope();
	Scope *opscope = symbol_->scope()->outer;

	bool in_scope = (tscope == opscope || tscope == opscope->outer);
	g->emit("%F", nil, type_);
	if (t_out) {
		g->emit("_out ");
	} else {
		//
		// as an alternative to the t_out approach, we could
		// generate T*& here
		// if (attr_ == invo_args::out_param && type_->varying()) {
		//	 g->emit("*");
		// }
		if (addr || attr_ != invo_args::in_param) {
			g->emit(" &");
		} else {
			g->emit(" ");
		}
	}
}

bool
Parameter::generate_stub(Generator *g)
{
	if (attr_ != invo_args::out_param) {
		g->emit_put(type_, "%E", declarator_);
		return (true);
	}
	return (false);
}

bool
Parameter::generate_extern_stubs(Generator *g)
{
	//
	// Parameters can have a subscript declaration.
	// In this case, we need to generate the stub code now.
	//
	return (type_->generate_extern_stubs(g) |
	    declarator_->generate_stub(g));
}

bool
Parameter::generate_type_name(Generator *g)
{
	switch (attr_) {
	case invo_args::in_param:
		g->add_operation_param("in");
		break;
	case invo_args::inout_param:
		g->add_operation_param("inout");
		break;
	case invo_args::out_param:
		g->add_operation_param("out");
		break;
	default:
		g->add_operation_param("unknown");
		break;
	}
	return (declarator_->generate_type_name(g));
}

//
// process_arg_desc - generate an arg_desc value for this parameter
// and accumulate the size information.
//
void
Parameter::process_arg_desc(Generator *g, arg_sizes &argsizes)
{
	// Create descriptor containing type, mode, and kind of size.
	Symbol *symbolp = this->actual_type();
	assert(symbolp != nil);

	if (type_->ExprValueObjtref() + type_->ExprValueUnknown() == 0) {
		// Then the size is fixed.
		size_type_ = invo_args::size_compile_time;
	}

	//
	// At this point we know that the actual parameter type has
	// been resolved. Certain types of parameters automatically
	// require marshal time size determination. Now we
	// coerce these types to require marshal time size determination.
	//
	if (symbolp->sequence_type() &&
	    !(symbolp->sequence_type()->type()->string_type())) {
		assert(size_type_ != invo_args::size_compile_time);
		assert(type_->ExprValueUnknown() > 0);
		size_type_ = invo_args::size_marshal_time;
	}

	arg_desc descriptor(symbolp->type_desc(), attr_, size_type_);
	g->emit_hexinteger((uint_t)descriptor);

	// Accumulate size information for this parameter.

	if (attr_ == invo_args::in_param ||
	    attr_ == invo_args::inout_param) {
		argsizes.in_size += type_->ExprValueSize();
		argsizes.in_objtref += type_->ExprValueObjtref();
		argsizes.in_unknown += type_->ExprValueUnknown();

		if (size_type_ == invo_args::size_marshal_time) {
			//
			// Currently only support marshal time
			// size determination for sequences, which
			// are always 1 unknown.
			//
			argsizes.in_unknown--;
		}
	}

	if (attr_ == invo_args::out_param ||
	    attr_ == invo_args::inout_param) {
		argsizes.out_size += type_->ExprValueSize();
		argsizes.out_objtref += type_->ExprValueObjtref();
		argsizes.out_unknown += type_->ExprValueUnknown();

		//
		// The sizes for those parameters requiring marshal time
		// size determination is unknown when the request msg
		// is processed by flow control. Thus we do not adjust
		// the unknown count for reply parameters using marshal time
		// size determination.
		//
	}
	//
	// Accumulate the count of the number of arguments needing
	// marshal time size determination.
	//
	if (size_type_ == invo_args::size_marshal_time) {
		argsizes.num_marshal_size++;
	}
}
