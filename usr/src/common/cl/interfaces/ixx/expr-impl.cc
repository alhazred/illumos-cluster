//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

/*
 * Copyright (c) 1992-1993 Silicon Graphics, Inc.
 * Copyright (c) 1993 Fujitsu, Ltd.
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Silicon Graphics and Fujitsu may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Silicon Graphics and Fujitsu.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

#pragma ident	"@(#)expr-impl.cc	1.3	06/05/24 SMI"

//
// Expression implementation classes
//

#include "generator.h"
#include "Symbol.h"
#include "UnionMember.h"

//
// class ExprImpl
//

ExprImpl::ExprImpl(SourcePosition *p)
{
	version_ = 0;
	symbol_ = nil;
	varying_ = ExprImpl::unknown;
	position_ = p->clone();
}

ExprImpl::~ExprImpl()
{
	if (position_ != nil)
		position_->unref();
}

Symbol *
ExprImpl::symbol()
{
	return (symbol_);
}

bool
ExprImpl::varying()
{
	if (varying_ == ExprImpl::unknown) {
		varying_ = compute_varying() ?
		    ExprImpl::varies : ExprImpl::fixed;
	}
	return (varying_ == ExprImpl::varies);
}

bool
ExprImpl::compute_varying()
{
	return (false);
}

SourcePosition *
ExprImpl::position()
{
	return (position_);
}

//
// Set the error handler's source position to the location this
// expression was defined in the IDL input file.
//
void
ExprImpl::set_source_position(ErrorHandler *h)
{
	if (position_ != nil) {
		h->position(position_);
	}
}

//
// Return true if we should output code for this expression.
// In other words, return false if this expression is defined in some
// included file.
bool
ExprImpl::do_source(Generator *g)
{
	return (g->is_source());
}

Declarator *
ExprImpl::declarator()
{
	return (nil);
}

//
// Rewrite the expression list after the resolve phase to handle versioned
// interfaces.
//
void
ExprImpl::rewrite_list(Generator *g, ExprList *list)
{
	if (list != nil) {
		for (ListUpdater(ExprList) i(*list); i.more(); i.next()) {
			i.cur()->rewrite(g, &i);
		}
	}
}

//
// Default routine for rewriting the parse tree from a single Expr for
// all versions of an interface to separate Exprs for each version.
//
void
ExprImpl::rewrite(Generator *, ListUpdater(ExprList) *)
{
}

//
// Generate a list of expressions using the given function
// on each expression.
// Return true if any of the functions return true.
//
bool
ExprImpl::generate_list(ExprList *list,
	// CSTYLED
	bool (Expr::*func)(Generator *),
	Generator *g, const char *sep, const char *trail)
{
	bool b = false;
	if (list != nil) {
		long n = g->counter();
		g->counter(0);
		bool need_sep = false;
		Expr *e;
		for (ListItr(ExprList) i(*list); i.more(); i.next()) {
			if (need_sep && sep != nil) {
				g->emit(sep);
			}
			e = i.cur();
			if (e->do_source(g)) {
				need_sep = (e->*func)(g);
				b |= need_sep;
				g->count(+1);
			}
		}
		if (need_sep && trail != nil) {
			g->emit(trail);
		}
	}
	return (b);
}

bool
ExprImpl::generate(Generator *)
{
	return (false);
}

//
// class IdentifierImpl
//

IdentifierImpl::IdentifierImpl(String *s, SourcePosition *p) :
	ExprImpl(p),
	value_(s)
{
}

IdentifierImpl::~IdentifierImpl()
{
}

String *
IdentifierImpl::string()
{
	return (value_);
}

bool
IdentifierImpl::varying()
{
	return (symbol_ != nil && symbol_->varying());
}

//
// constructors and destructors for different kinds of expressions
//

RootExpr::RootExpr(SourcePosition *srcpos, ExprList *defs) :
	ExprImpl(srcpos), defs_(defs)
{
}

RootExpr::~RootExpr()
{
}

Accessor::Accessor(SourcePosition *srcpos, Expr *qualifier, String *string) :
	ExprImpl(srcpos),
	qualifier_(qualifier),
	string_(string)
{
}

Accessor::~Accessor()
{
}

Constant::Constant(SourcePosition *srcpos, Identifier *ident, Expr *type,
    Expr *value) :
	ExprImpl(srcpos),
	ident_(ident),
	type_(type),
	value_(value)
{
	ExprValueSize(value->ExprValueSize());
}

Constant::~Constant()
{
}

Unary::Unary(SourcePosition *srcpos, Opcode op, Expr *expr) :
	ExprImpl(srcpos),
	op_(op),
	expr_(expr)
{
}

Unary::~Unary()
{
}

Binary::Binary(SourcePosition *srcpos, Opcode op, Expr *left, Expr *right) :
	ExprImpl(srcpos),
	op_(op),
	left_(left),
	right_(right)
{
}

Binary::~Binary()
{
}

TypeName::TypeName(SourcePosition *srcpos, Expr *type, ExprList *decls) :
	ExprImpl(srcpos),
	type_(type),
	declarators_(decls)
{
}

TypeName::~TypeName()
{
}

UnsignedType::UnsignedType(SourcePosition *srcpos, Expr *type) :
	ExprImpl(srcpos),
	type_(type)
{
}

UnsignedType::~UnsignedType()
{
}

CaseElement::CaseElement(SourcePosition *srcpos, ExprList *labels,
    UnionMember *element) :
	ExprImpl(srcpos),
	labels_(labels),
	element_(element)
{
}

CaseElement::~CaseElement()
{
}

CaseLabel::CaseLabel(SourcePosition *srcpos, Expr *value) :
	DefaultLabel(srcpos),
	value_(value)
{
}

CaseLabel::~CaseLabel()
{
}

DefaultLabel::DefaultLabel(SourcePosition *srcpos) :
	ExprImpl(srcpos)
{
}

DefaultLabel::~DefaultLabel()
{
}

EnumDecl::EnumDecl(SourcePosition *srcpos, Identifier *ident,
    ExprList *members) :
	ExprImpl(srcpos),
	ident_(ident),
	members_(members),
	enums_(0)
{
}

EnumDecl::~EnumDecl()
{
}

Enumerator::Enumerator(SourcePosition *srcpos, Identifier *ident) :
	ExprImpl(srcpos),
	ident_(ident)
{
}

Enumerator::~Enumerator()
{
}

boolLiteral::boolLiteral(SourcePosition *srcpos, bool b) :
	ExprImpl(srcpos),
	value_(b)
{
	ExprValueSize(0);
}

boolLiteral::~boolLiteral()
{
}

IntegerLiteral::IntegerLiteral(SourcePosition *srcpos, long n) :
	ExprImpl(srcpos),
	value_(n)
{
	ExprValueSize(n);
}

IntegerLiteral::~IntegerLiteral()
{
}

FloatLiteral::FloatLiteral(SourcePosition *srcpos, double d) :
	ExprImpl(srcpos),
	value_(d)
{
}

FloatLiteral::~FloatLiteral()
{
}

StringLiteral::StringLiteral(SourcePosition *srcpos, String *s) :
	ExprImpl(srcpos),
	value_(s)
{
}

StringLiteral::~StringLiteral()
{
}

CharLiteral::CharLiteral(SourcePosition *srcpos, long n) :
	ExprImpl(srcpos),
	value_(n)
{
	ExprValueSize(n);
}

CharLiteral::~CharLiteral()
{
}

SrcPos::SrcPos(SourcePosition *srcpos) :
	ExprImpl(srcpos)
{
}

SrcPos::~SrcPos()
{
}

bool
TypeName::compute_varying()
{
	if (type_ != nil) {
		return (type_->varying());
	} else {
		//
		// This check depends on the types defined in
		// Resolver::init_types().
		//
		return (kind_ == invo_args::t_string);
	}
}

bool
Accessor::compute_varying()
{
	// XXX What about versioned interfaces not in the symtable?
	return (symbol_ != nil && symbol_->varying());
}

bool
CaseElement::compute_varying()
{
	return (element_->varying());
}
