/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

//
// This code provides the C++ interface for the RSMAPI Kernel Agent to
// obtain callbacks from the Path Manager.  Calls to add_adapter build
// a list of adapters supported by RSMPI.  Calls to add_path build a list
// of paths pertaining to the RSMAPI.  No path state is maintained here;
// path state transitions and processing is done in C code.
//

#pragma ident	"@(#)clif_rsm.cc	1.21	08/05/20 SMI"

int clifrsm_modunload_ok = 0;
int clifrsm_shutdown_done = 0;

#include <sys/modctl.h>
#include <orb/invo/common.h>
#include <rsm/clif_rsm.h>
#include <orb/transport/path_manager.h>




clifrsm_path_monitor *clifrsm_path_monitor::the_clifrsm_path_monitor = NULL;


/* inter-module dependencies */
char    _depends_on[] =  "misc/cl_runtime misc/cl_load misc/cl_orb "
	"misc/cl_haci misc/cl_quorum misc/cl_comm /drv/rsm";

static struct modlmisc modlmisc = {
	&mod_miscops, "CLUSTER-RSMKA Interface module v1.0",
};

static struct modlinkage modlinkage = {
#ifdef _LP64
	MODREV_1, { (void *) &modlmisc, NULL, NULL, NULL, NULL, NULL, NULL }
#else
	MODREV_1, { (void *) &modlmisc, NULL, NULL, NULL }
#endif
};



int
_init(void)
{
	int error;
	nodeid_t local_node_id;

	if ((cluster_bootflags & CLUSTER_BOOTED) == 0)
		return (EINVAL);

	if ((error = mod_install(&modlinkage)) != 0) {
		return (error);
	}

	/* Set the local node id */
	local_node_id = clconf_get_nodeid();
	rsmka_set_my_nodeid(local_node_id);

	_cplpl_init();		/* C++ initialization */
	clifrsm_path_monitor::initialize();
	return (0);
}

int
_fini(void)
{
	if (clifrsm_modunload_ok == 0)
		return (EBUSY);

	if (clifrsm_shutdown_done == 0)
		clifrsm_path_monitor::terminate();

	_cplpl_fini();
	return (mod_remove(&modlinkage));
}

int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}

//
// CONSTRUCTOR
//
clifrsm_path_monitor::clifrsm_path_monitor() :
	_SList::ListElem(this)
{
#ifdef NOTNOW
	os::printf("clifrsm_path_monitor constructor: this = %x\n", this);
#endif
}



//
// DESTRUCTOR
//
clifrsm_path_monitor::~clifrsm_path_monitor()
{
#ifdef NOTNOW
	os::printf("clifrsm_path_monitor destructor: \n");
#endif
}



//
// Register with the cluster Path Manager
//
void
clifrsm_path_monitor::initialize()
{
	ASSERT(the_clifrsm_path_monitor == NULL);
	the_clifrsm_path_monitor = new clifrsm_path_monitor();
	ASSERT(the_clifrsm_path_monitor != NULL);

	path_manager::the().add_client(path_manager::PM_OTHER,
	    the_clifrsm_path_monitor);
}




//
// Un-register with the Path Manager callback list.  Then purge the
// adapter and path lists.  Then destroy the interface object
//
void
clifrsm_path_monitor::terminate()
{

	path_manager::the().remove_client(path_manager::PM_OTHER,
		the_clifrsm_path_monitor);

	clifrsm_path_monitor::the_clifrsm_path_monitor->purge_pathlist();
	clifrsm_path_monitor::the_clifrsm_path_monitor->purge_adapterlist();

	delete the_clifrsm_path_monitor;
	the_clifrsm_path_monitor = NULL;
}



void
clifrsm_path_monitor::purge_adapterlist()
{
	clifrsm_adapter_elem *current;
	int flags = RSMKA_USE_COOKIE;

	lock_adapterlist();
	while ((current = adapterlist.reapfirst()) != NULL) {
		//
		// Notify the RSMAPI Kernel Agent path manager
		//
		(void) rsmka_remove_adapter(NULL, 0,
						current->adapter_cookie,
						flags);

		delete current;
	}
	unlock_adapterlist();

}

void
clifrsm_path_monitor::purge_pathlist()
{
	clifrsm_path_elem *current;
	int flags = RSMKA_USE_COOKIE;

	lock_pathlist();
	while ((current = pathlist.reapfirst()) != NULL) {
		//
		// Notify the RSMAPI Kernel Agent path manager
		//
		rsmka_remove_path(0, 0, 0, (uint64_t)0, current->path_cookie,
					flags);

		delete current;
	}
	unlock_pathlist();

}


//
// Adapter list element constructor
//
clifrsm_adapter_elem::clifrsm_adapter_elem(const char *devname,
					int instance, void *cookie) :
	adapter_instance(instance),
	adapter_cookie(cookie),
	_SList::ListElem(this)
{
	(void) os::strcpy(adapter_devname, devname);

}


//
// Search the list for the specified adapter. If not found return NULL.
//
clifrsm_adapter_elem *
clifrsm_path_monitor::lookup_adapter(const char *devname, int instance)
{
	ASSERT(adapterlist_lck.lock_held());
	clifrsm_adapterlist_t::ListIterator iter(adapterlist);
	clifrsm_adapter_elem *current;
	while ((current = iter.get_current()) != NULL) {
		if (current->adapter_instance == instance &&
			(strcmp(devname, current->adapter_devname) == 0))
			break;
		iter.advance();
	}
	return (current);
}

//
// Path list element constructor
//
clifrsm_path_elem::clifrsm_path_elem(clconf_path_t *pth, const char *devname,
				int instance, nodeid_t rem_node,
				uint64_t rem_adapter_hwaddr,
				int rem_adapter_instance,
				void *ad_cookie, void *cookie) :
	path(pth),
	adapter_instance(instance),
	remote_node(rem_node),
	remote_adapter_hwaddr(rem_adapter_hwaddr),
	remote_adapter_instance(rem_adapter_instance),
	adapter_cookie(ad_cookie),
	path_cookie(cookie),
	_SList::ListElem(this)
{
	(void) os::strcpy(adapter_devname, devname);
}

//
// Search the list for the specified path. If not found return NULL.
//
clifrsm_path_elem *
clifrsm_path_monitor::lookup_path(clconf_path_t *pth)
{
	ASSERT(pathlist_lck.lock_held());
	clifrsm_pathlist_t::ListIterator iter(pathlist);
	clifrsm_path_elem *current;
	while ((current = iter.get_current()) != NULL &&
		current->path != pth) {
		iter.advance();
	}
	return (current);
}


//
// The rsmka_add_adapter routine returns TRUE if RSMPI supports the
// specified adapter.  A list of supported adapters is maintained for the
// support of other callbacks.
//
bool
clifrsm_path_monitor::add_adapter(clconf_adapter_t *adapter_p)
{
	const char *name;
	int instance;
	uint64_t adapter_hwaddr;
	void	*cookie;
	clifrsm_adapter_elem *new_entry_p;
	const char *id = NULL;

	//
	// Extract adapter parameters from the database
	//

#ifdef NOTNOW
	os::printf("ADD ADAPT adp = %x\n", adapter_p);
#endif

	name = clconf_adapter_get_device_name(adapter_p);
	instance = clconf_adapter_get_device_instance(adapter_p);
	id = clconf_obj_get_property((clconf_obj_t *)adapter_p, "adapter_id");
	if (id == NULL)
		return (false);
	else
		adapter_hwaddr = (uint64_t)(int64_t)os::atoi(id);

	// An RSMPI supported adapter will return true
	cookie = rsmka_add_adapter(name, instance, adapter_hwaddr);

	if (cookie != NULL) {
		new_entry_p = new clifrsm_adapter_elem(name, instance,
							cookie);
		lock_adapterlist();
		adapterlist.append(new_entry_p);
		unlock_adapterlist();
		return (true);
	} else
		return (false);

}


//
// If the specified adapter was added to the list of supported adapters
// by add_adapter, then remove it from the list and call
// rsmka_remove_adapter for additional processing.
//
bool
clifrsm_path_monitor::remove_adapter(clconf_adapter_t *adapter_p)
{
	clifrsm_adapter_elem *elemp;
	int flags = RSMKA_USE_COOKIE;
	const char *name;
	int instance;

	ASSERT(adapter_p != NULL);

	name = clconf_adapter_get_device_name(adapter_p);
	instance = clconf_adapter_get_device_instance(adapter_p);

	//
	// Must be an adapter for RSMAPI
	//
	lock_adapterlist();
	elemp = lookup_adapter(name, instance);
	if (elemp != NULL) {
		(void) adapterlist.erase(elemp);
		unlock_adapterlist();
	} else {
		unlock_adapterlist();
		return (false);
	}


	(void) rsmka_remove_adapter(0, 0, elemp->adapter_cookie, flags);

	delete elemp;

	return (true);

}




bool
clifrsm_path_monitor::add_path(clconf_path_t *path_p)
{
	nodeid_t		rem_node;
	clifrsm_adapter_elem	*adapter_elem;
	clifrsm_path_elem	*new_entry_p;
	uint64_t		rem_adapter_hwaddr;
	int			rem_adapter_instance;
	clconf_cluster_t	*cl_cluster_p;
	clconf_adapter_t	*cl_adapter_p;
	void			*cookie;
	const char 		*name;
	int 			instance;
	int flags = RSMKA_USE_COOKIE;

#ifdef NOTNOW
	os::printf("ADD PATH pathp = %x\n", path_p);
#endif

	// Get adapter, starting at the root of the cluster database
	cl_cluster_p = clconf_cluster_get_current();
	cl_adapter_p = clconf_path_get_adapter(path_p, CL_LOCAL, cl_cluster_p);
	name = clconf_adapter_get_device_name(cl_adapter_p);
	instance = clconf_adapter_get_device_instance(cl_adapter_p);

	// If the local adapter for this path is being used by RSMAPI, then
	// the add_adapter method would have linked a corresponding
	// adapter_elem on the adapterlist.
	lock_adapterlist();
	adapter_elem = lookup_adapter(name, instance);
	unlock_adapterlist();
	if (adapter_elem == NULL) {
		clconf_obj_release((clconf_obj_t *)cl_cluster_p);
		return (false);
	}

	//
	// Retrieve the remote node and remote adapter hardware address
	// from the cluster database
	rem_node = clconf_path_get_remote_nodeid(path_p);
	cl_adapter_p = clconf_path_get_adapter(path_p, CL_REMOTE, cl_cluster_p);
	ASSERT(cl_adapter_p != NULL);
	rem_adapter_hwaddr =
		(uint64_t)(int64_t)os::atoi(clconf_obj_get_property(
		(clconf_obj_t *)cl_adapter_p, "adapter_id"));
	rem_adapter_instance = clconf_adapter_get_device_instance(cl_adapter_p);


	clconf_obj_release((clconf_obj_t *)cl_cluster_p);

	//
	// Kernel Agent path processing
	//
	cookie = rsmka_add_path(adapter_elem->adapter_devname,
				adapter_elem->adapter_instance, rem_node,
				rem_adapter_hwaddr,
				rem_adapter_instance,
				adapter_elem->adapter_cookie, flags);
#ifdef NOTNOW
	os::printf("ADD PATH pathp = %x cookie = %x\n", path_p, cookie);
#endif

	//
	// Link a path_elem on the pathlist.  The object data is used
	// in subsequent path_up/path_down calls.
	//
	if (cookie != NULL) {
#ifdef NOTNOW
		os::printf("ADD PATH pathp = %x name = %s\n",
				path_p, adapter_elem->adapter_devname);
#endif
		new_entry_p = new clifrsm_path_elem(path_p,
						adapter_elem->adapter_devname,
						adapter_elem->adapter_instance,
						rem_node, rem_adapter_hwaddr,
						rem_adapter_instance,
						adapter_elem->adapter_cookie,
						cookie);
		lock_pathlist();
		pathlist.append(new_entry_p);
		unlock_pathlist();
		return (true);
	}
	else
		return (false);
}



//
//
bool
clifrsm_path_monitor::remove_path(clconf_path_t *pth)
{
	clifrsm_path_elem *element;
	int flags = RSMKA_USE_COOKIE;

	ASSERT(pth != NULL);

	//
	// Must be a path for RSMAPI
	//
	lock_pathlist();
	element = lookup_path(pth);
	if (element != NULL) {
		(void) pathlist.erase(element);
		unlock_pathlist();
	} else {
		unlock_pathlist();
		return (false);
	}

	//
	// Call the RSMAPI Kernel Agent path manager
	//
	rsmka_remove_path(NULL, 0, 0, (uint64_t)0, element->path_cookie,
				flags);


	delete element;

	return (true);
}

//
// Search the pathlist for pth; then call the Kernel Agent pathup routine
// with the adapter and path descriptor pointers (cookies).
// This routine is called at clock ipl, so blocking is not allowed
//
bool
clifrsm_path_monitor::path_up(clconf_path_t *pth)
{
	clifrsm_path_elem *element;
	int flags = RSMKA_USE_COOKIE | RSMKA_NO_SLEEP;


	ASSERT(pth != NULL);

#ifdef NOTNOW
	os::printf("PATH UP pathp = %x\n", pth);
#endif

	//
	// Must be a path for RSMAPI
	//
	lock_pathlist();
	element = lookup_path(pth);
	unlock_pathlist();
	if (element == NULL)
		return (false);

#ifdef NOTNOW
	os::printf("PATH UP pathp = %x calling rsm\n", pth);
#endif
	(void) rsmka_path_up(NULL, 0, 0, (uint64_t)0,
				element->path_cookie, flags);

	return (true);
}


//
// Search the pathlist for pth; then call the Kernel Agent pathdown routine
// with the adapter and path descriptor pointers (cookies).
// This routine is called at clock ipl, so blocking is not allowed
//
bool
clifrsm_path_monitor::path_down(clconf_path_t *pth)
{
	clifrsm_path_elem *element;
	int flags = RSMKA_USE_COOKIE | RSMKA_NO_SLEEP;


	ASSERT(pth != NULL);

	//
	// Must be a path for RSMAPI
	//
	lock_pathlist();
	element = lookup_path(pth);
	if (element == NULL) {
		unlock_pathlist();
		return (false);
	}

	(void) rsmka_path_down(0, 0, 0, (uint64_t)0,
				element->path_cookie, flags);
	unlock_pathlist();


	return (true);
}

//
// disconnect_node is treated like a path_down for all paths pertaining
// to the node.  All elements of the pathlist are checked; when there is
// a node_id match, the rsmka_path_down routine is called in the Kernel Agent
// path manager. disconnect_node is called at clock interrupt level so
// rsmka_path_down is called with the sleep flag set to NO_SLEEP.
bool
clifrsm_path_monitor::disconnect_node(nodeid_t ndid, incarnation_num)
{
	int flags = RSMKA_USE_COOKIE | RSMKA_NO_SLEEP;
	clifrsm_path_elem *element;

	lock_pathlist();
	clifrsm_pathlist_t::ListIterator iter(pathlist);
	while ((element = iter.get_current()) != NULL) {
		iter.advance();
		if (element->remote_node == ndid) {
			(void) rsmka_path_down(0, 0, 0, (uint64_t)0,
						element->path_cookie,
						flags);
		}
	}

	unlock_pathlist();
	return (true);
}

//
//  call the Kernel Agent  rsmka_node_alive
//
bool
clifrsm_path_monitor::node_alive(nodeid_t node, incarnation_num)
{
	int ret = 0;

	ret = rsmka_node_alive(node);
	if (ret == 1)
		return (true);
	else
		return (false);
}


//
// All processing is done by disconnect_node
//
bool
clifrsm_path_monitor::node_died(nodeid_t node, incarnation_num)
{
	(void) rsmka_node_died(node);
	return (true);
}
