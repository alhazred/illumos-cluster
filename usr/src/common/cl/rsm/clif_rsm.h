/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _CLIF_RSM_H
#define	_CLIF_RSM_H

#pragma ident	"@(#)clif_rsm.h	1.14	08/05/20 SMI"

#include <sys/list_def.h>
#include <orb/transport/pm_client.h>


//
// For clifrsm_add_adapter, an clifrsm_adapter_elem is allocated/constructed and
// added to the list of adapters.  The private data is used for an efficient
// initialization of clifrsm_path_elem data for add_path.
//
class clifrsm_adapter_elem : public _SList::ListElem {
	friend class clifrsm_path_monitor;
public:
	clifrsm_adapter_elem(const char *, int, void *);
private:
	char			adapter_devname[MAXNAMELEN];
	int			adapter_instance;
	void			*adapter_cookie;

};


//
// For clifrsm_add_path, an clifrsm_path_elem is allocated/constructed and
// added to the list of paths (if the path local adapter is on the adapter
// list.
// The private data is subsequently used by friend clifrsm_path_monitor in
// methods clifrsm_path_up, clifrsm_path_down, etc.
//
class clifrsm_path_elem : public _SList::ListElem {
	friend class clifrsm_path_monitor;
public:
	clifrsm_path_elem(clconf_path_t *, const char *, int, nodeid_t,
			uint64_t, int, void *, void *);
private:
	clconf_path_t	*path;
	char		adapter_devname[MAXNAMELEN];
	int		adapter_instance;
	nodeid_t	remote_node;
	uint64_t	remote_adapter_hwaddr;
	int		remote_adapter_instance;
	void		*adapter_cookie;
	void		*path_cookie;

};



typedef IntrList < clifrsm_adapter_elem, _SList > clifrsm_adapterlist_t;
typedef IntrList < clifrsm_path_elem, _SList > clifrsm_pathlist_t;


// This is the class for the interface between the Path Manager (C++) and
// the RSMAPI Kernel Agent (C)
//
// It will register with the Path Manager for monitoring the node and path
// events and forward the events to C code in the Kernel Agent.
//

class clifrsm_path_monitor : public pm_client, public _SList::ListElem {
public:
	bool add_adapter(clconf_adapter_t *);
	bool remove_adapter(clconf_adapter_t *);
	bool change_adapter(clconf_adapter_t *);
	bool add_path(clconf_path_t *);
	bool remove_path(clconf_path_t *);
	bool node_alive(nodeid_t, incarnation_num);
	bool node_died(nodeid_t, incarnation_num);
	bool path_up(clconf_path_t *);
	bool path_down(clconf_path_t *);
	bool disconnect_node(nodeid_t, incarnation_num);

	clifrsm_adapter_elem *lookup_adapter(const char *, int);
	clifrsm_path_elem *lookup_path(clconf_path_t *);
	static void initialize();
	static void terminate();
	void shutdown();
	void purge_adapterlist();
	void purge_pathlist();

	_SList::ListElem *get_pmlist_elem();

	static clifrsm_path_monitor &the();


	clifrsm_path_monitor();
	~clifrsm_path_monitor();


private:

	static clifrsm_path_monitor *the_clifrsm_path_monitor;

	// Adapterlist search for the specified adapter.
	// ipconf_path_elem *locate_path(clconf_adapter_t *);

	// Pathlist search for the specified path.
	// ipconf_path_elem *locate_path(clconf_path_t *);

	clifrsm_pathlist_t pathlist;
	clifrsm_adapterlist_t adapterlist;

	void lock_pathlist();
	void unlock_pathlist();
	void lock_adapterlist();
	void unlock_adapterlist();

	os::mutex_t pathlist_lck;
	os::mutex_t adapterlist_lck;


	//
	// Disallow assignments and pass by value
	//
	clifrsm_path_monitor(const clifrsm_path_monitor &);
	clifrsm_path_monitor &operator = (clifrsm_path_monitor &);

};


#include <rsm/clif_rsm_in.h>

#endif	/* _CLIF_RSM_H */
