/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident   "@(#).future-atomic_copy.s 1.3     08/05/20 SMI"

	.section ".text"			! [internal]
	.proc	18
	.global	atomic_copy64
	.align	4

	/* atomic_copy64(char *src, char *dest) */
atomic_copy64:
!#PROLOGUE# 0
!#PROLOGUE# 1
	save	%sp,-128,%sp
	rd	%fprs, %l3
	wr	%g0, 0x4, %fprs
	add	%sp, 64, %l7
	andn	%l7, 0x3f, %l7
	stda	%d0, [%l7] 0xF0
	ldda	[%i0] 0xF0, %d0
	membar  #LoadStore
	stda	%d0, [%i1] 0xF0
	ldda	[%l7] 0xF0, %d0
        and     %l3, 0x4, %l3           ! fprs.du = fprs.dl = 0
	wr      %l3, %g0, %fprs         ! fprs = l3 - restore fprs.fef
	membar  #StoreLoad|#StoreStore
	ret
	restore
	.optim	"-O~Q~R~S"
       .LF12 = -64
	.LP12 = 64
	.LST12 = 64
	.LT12 = 64
	.type	atomic_copy64,#function
	.size	atomic_copy64,.-atomic_copy64
	.global	atomic_copy64
	.align	4

	/* atomic_copy(char *src, char *dest, int len) */
atomic_copy:
!#PROLOGUE# 0
!#PROLOGUE# 1
	save	%sp,-128,%sp
	brz,pn	%i2, 2f
	rd	%fprs, %l3
	wr	%g0, 0x4, %fprs
	add	%sp, 64, %l7
	mov	%g0, %i3
	andn	%l7, 0x3f, %l7
	srl	%i2, 6, %i2
	stda	%d0, [%l7] 0xF0
	membar	#StoreLoad
1:
	ldda	[%i0+%i3] 0xF0, %d0
	dec	%i2		
	membar  #LoadStore
	stda	%d0, [%i1+%i3] 0xF0
	brnz,pt	%i2, 1b
	add	%i3, 64, %i3
	
	ldda	[%l7] 0xF0, %d0
        and     %l3, 0x4, %l3           ! fprs.du = fprs.dl = 0
	wr      %l3, %g0, %fprs         ! fprs = l3 - restore fprs.fef
	membar  #StoreLoad|#StoreStore
2:		
	ret
	restore
	.type	atomic_copy,#function
	.size	atomic_copy,.-atomic_copy
	.global	atomic_copy
	.align	4

