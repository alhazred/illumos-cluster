/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident   "@(#).future-membar.s 1.3     08/05/20 SMI"

	.section	".text"
	.proc	022
	.global	membar_sync

	.align	4
membar_sync:
	!#PROLOGUE# 0
	retl
	membar	#Sync
	.optim	"-O~Q~R~S"
       .LF12 = -64
	.LP12 = 64
	.LST12 = 64
	.LT12 = 64
	.type	membar_sync,#function
	.size	membar_sync,.-membar_sync
	
	.global	membar_ldst
	.align	4
membar_ldst:
	retl
	membar	#LoadStore
	.type	membar_ldst,#function
	.size	membar_ldst,.-membar_ldst

	.global	membar_stst
	.align	4
membar_stst:
	retl
	membar	#StoreStore
	.type	membar_stst,#function
	.size	membar_stst,.-membar_stst

	.global	cas32
	.align	4

cas32:
	cas	[%o0], %o1, %o2
	retl
	mov	%o2, %o0
	.type	cas32,#function
	.size	cas32,.-cas32


