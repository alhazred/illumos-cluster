/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  clif_rsm_in.h
 *
 */

#ifndef _CLIF_RSM_IN_H
#define	_CLIF_RSM_IN_H

#pragma ident	"@(#)clif_rsm_in.h	1.15	08/05/20 SMI"

#include <sys/types.h>
#include <cplplrt/cplplrt.h>


extern "C" void *rsmka_add_adapter(const char *, int, uint64_t);
extern "C" boolean_t rsmka_remove_adapter(const char *, int, void *, int);
extern "C" void *rsmka_add_path(const char *, int, nodeid_t, uint64_t, int,
				void *, int);
extern "C" void rsmka_remove_path(const char *, int, nodeid_t, uint64_t,
					void *, int);
extern "C" boolean_t rsmka_path_up(const char *, int, nodeid_t, uint64_t,
					void *, int);
extern "C" boolean_t rsmka_path_down(const char *, int, nodeid_t, uint64_t,
					void *,	int);
extern "C" boolean_t rsmka_node_alive(nodeid_t);
extern "C" boolean_t rsmka_node_died(nodeid_t);
extern "C" void rsmka_set_my_nodeid(nodeid_t);


#include <rsm/clif_rsm.h>

#define	RSMKA_NO_SLEEP		1
#define	RSMKA_USE_COOKIE	2

inline _SList::ListElem *
clifrsm_path_monitor::get_pmlist_elem()
{
	return (this);
}

inline void
clifrsm_path_monitor::shutdown()
{
	clifrsm_shutdown_done++;
	clifrsm_modunload_ok++;
	clifrsm_path_monitor::terminate();
	// ASSERT(paths.empty());
}

inline void
clifrsm_path_monitor::lock_adapterlist()
{
	adapterlist_lck.lock();
}

inline void
clifrsm_path_monitor::unlock_adapterlist()
{
	adapterlist_lck.unlock();
}

inline void
clifrsm_path_monitor::lock_pathlist()
{
	pathlist_lck.lock();
}

inline void
clifrsm_path_monitor::unlock_pathlist()
{
	pathlist_lck.unlock();
}

// Static
inline clifrsm_path_monitor &
clifrsm_path_monitor::the(void)
{
	ASSERT(the_clifrsm_path_monitor != NULL);
	return (*the_clifrsm_path_monitor);

}

inline bool
clifrsm_path_monitor::change_adapter(clconf_adapter_t *)
{
	return (false);
}

#endif	/* _CLIF_RSM_IN_H */
