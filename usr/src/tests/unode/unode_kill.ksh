#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)unode_kill.ksh	1.14	08/05/20 SMI"
#

PROGDIR=$(dirname $0)		# directory where this program resides
if [ "$PROGDIR" = "." ]; then
	PROGDIR=$PWD
fi
readonly PROGDIR

USAGE="Usage: $0 <cluster_name> all
       $0 <cluster_name> <node_id> ..."

# Make sure we have path to 'unode_load' which should be located in the
# same directory as this script
export PATH=$(dirname $0):/usr/proc/bin:$PATH

# Check number of arguments.
if (( $# < 2 ))
then
	print -u2 "$USAGE"
	exit 1
fi

CLUSTER_NAME=$1
shift

# Make sure $CLUSTER_NAME is not blank
if ! test $CLUSTER_NAME
then
	print -u2 "$USAGE"
	exit 1
fi

# Need to source this file in to obtain unode options from "rc" file.
. $PROGDIR/unode_config

# Cluster info file
CINFOFILE=$UNODE_DIR/$CLUSTER_NAME/clusterinfo

# Construct PID file
function get_pidfile # <node_id>
{
	print $UNODE_DIR/$CLUSTER_NAME/node-$1/pid
}
	


typeset -l LOWCASE

# Verify we can read the cluster info file.
if [[ ! -r $CINFOFILE ]]
then
	print -u2 "ERROR: Couldn't read cluster info file $CINFOFILE"
	exit 1
fi

# Get number of nodes from the cluster info file.
NUMNODES=$(< $CINFOFILE)

# Gather node id's to kill.
NODEIDS=
while (( $# > 0 ))
do
	LOWCASE=$1				# case insensitive

	# If one of the arguments is "all" then kill all nodes.
	if [[ "$LOWCASE" == all ]]
	then
		# Construct node from # of nodes to 1 (in reverse because
		# we don't want to kill the root node first).
		NODEIDS=			# override previous node ids
		NID=$NUMNODES
		while (( NID > 0 ))
		do
			NODEIDS="$NODEIDS $NID"
			(( NID -= 1 ))
		done

		shift $#
		break				# we're done
	else
		NODEIDS="$NODEIDS $LOWCASE"
	fi
	shift
done

# Kill each specified node.
for NID in $NODEIDS
do
	# File storing the PID of the unode process for node id $NID.
	PIDFILE=$(get_pidfile $NID)

	if [[ ! -f $PIDFILE ]]
	then
		continue
	fi
	if [[ ! -r $PIDFILE ]]
	then
		print -u2 "WARNING: Couldn't read $PIDFILE to get" \
			"PID of node $NID"
		continue
	fi

	PID=$(< $PIDFILE)
	print "Killing node $NID (pid = $PID)"
	unode_load $CLUSTER_NAME $NID orbtest quit
	pwait $PID
done

# If all nodes have been killed, then remove the cluster info file.
# (Note, this is to handle the case where previous calls to this script were
# only to kill a subset of the nodes.)
NID=1
while (( NID <= NUMNODES ))
do
	PIDFILE=$(get_pidfile $NID)

	# If there is one pid file still exists, then exit without removing
	# the cluster info file.
	if [[ -f $PIDFILE ]]
	then
		exit 0
	fi
	(( NID += 1 ))
done
rm -f $CINFOFILE
exit 0
