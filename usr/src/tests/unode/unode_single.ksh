#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)unode_single.ksh	1.8	08/05/20 SMI"
#

PROGDIR=$(dirname $0)		# directory where this program resides
if [ "$PROGDIR" = "." ]; then
	PROGDIR=$PWD
fi
readonly PROGDIR

USAGE="Usage: $0 [-l] <node number> <number_of_nodes> <transport_string> [dlpi|rsm]"

# Make sure we have path to xterm and 'unode' which should be located in the
# same directory as this script
export PATH=$(dirname $0):/usr/openwin/bin:$PATH

# -l : log unode output to file

while getopts l OPT
do
    case $OPT in
    \?) print -u2 "$USAGE"
	exit 1
    esac
done
shift `expr $OPTIND - 1`
    
# Check number of arguments.
if (( $# < 3 ||  $# > 4))
then
	print -u2 "$USAGE"
	exit 1
fi

CLUSTER_NAME="ucluster"
NODE=$1
NUMNODES=$2
TRANSPORT=$3

if (($# == 4 ))
then
	TRANSPORT_TYPE=$4
	if [[ $TRANSPORT_TYPE != "dlpi" && $TRANSPORT_TYPE != "rsm" ]]
	then
		print -u2 "$USAGE"
		exit 1
	fi
else
	TRANSPORT_TYPE="dlpi"
fi

# Make sure $CLUSTER_NAME and $TRANSPORT_TYPE are not blank and $NUMNODES is
# a positive number
if ! test $CLUSTER_NAME || ! test $NUMNODES || ! test $TRANSPORT_TYPE
then
	print -u2 "$USAGE"
	exit 1
fi
if [[ $(expr "$NUMNODES" : '[0-9]*') -ne ${#NUMNODES} ||
	$NUMNODES -le 0 ]]
then
	print -u2 "ERROR: Number of nodes must a positive number"
	exit 1
fi

# Need to source this file in to obtain unode options from "rc" file.
. $PROGDIR/unode_config

/bin/rm -rf $UNODE_DIR/$CLUSTER_NAME

pkill -x unode

# Create cluster directory, if it doesn't exist yet.
mkdir -p $UNODE_DIR/$CLUSTER_NAME || exit 1

# Cluster info file
CINFOFILE=$UNODE_DIR/$CLUSTER_NAME/clusterinfo


# Make sure we do not clobber another active unode cluster
if [[ -a $CINFOFILE ]]
then
	print -u2 "ERROR: file $CINFOFILE already exists."
	print -u2 "  Check if unode cluster with same name already active."
	exit 1
fi

# Store the number of nodes in the new cluster info file.
echo $NUMNODES > $CINFOFILE
if [[ ! -a $CINFOFILE ]]
then
	print -u2 "ERROR: Couldn't create $CINFOFILE"
	exit 1
fi

print "Starting ${NUMNODES}-node cluster called $CLUSTER_NAME"

UNODE_TRANSPORT=${TRANSPORT_TYPE} UNODE_ADAPTERS=${TRANSPORT} unode $CLUSTER_NAME $NODE 1-$NUMNODES &
