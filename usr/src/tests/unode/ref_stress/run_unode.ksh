#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)run_unode.ksh	1.20	08/05/20 SMI"
#

#
# A simple script to automatically start UNODE nodes (displayed in xterm
# windows), start the UNODE ref_stress components (driver, clients and
# servers), run the tests once, and unload the components.
# At the end, the user is prompted whether to kill the nodes or not.
#
# NOTE: Do not use variables named UNODE_* in this script.  These variable
#	names are reserved and used by unode.
#

#
# Set a few ksh parameters, in case user has them set/unset in the $ENV file
#
ulimit -c unlimited		# size of core dumps
ulimit -f unlimited		# max size of files written
ulimit -v unlimited		# size of virtual memory

set +o allexport		# don't automatically export vars
set +o errexit			# don't exit if commands return nonzero
set +o keyword			# only use variable assignments before commands
set +o markdirs			# don't put trailing / on directory names
set -o monitor			# so bg jobs run with own PGID
set +o noclobber		# allow clobber
set +o noglob			# enable file name globbing
set -o nolog			# don't save functions in history file
set +o notify			# don't notify bg job completion
set +o nounset			# allow use of unset variables


readonly PROGNAME=${0##*/}

PROGDIR=$(dirname $0)		# directory where this program resides
if [ "$PROGDIR" = "." ]; then
	PROGDIR=$PWD
fi
readonly PROGDIR

# Template infrastructure file to use.
TEMPLATE=
use_template=no

# Name of unode shared library containing the test.
readonly TEST_LIB=ref_stress

# Figure out where the unode utilities are
if [ $ROOT ]
then
	readonly BINDIR_UNODE=$ROOT/usr/unode/bin
	readonly LIBDIR_UNODE=$ROOT/usr/unode/lib
else
	# Relative to the $PROGDIR directory.
	readonly BINDIR_UNODE=$PROGDIR/../bin
	readonly LIBDIR_UNODE=$PROGDIR/../lib
fi

# Need to source this file in to obtain unode options from "rc" file.
. $BINDIR_UNODE/unode_config

export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$LIBDIR_UNODE"

# Special utility which simply lists available tests.
CMD_LIST_TESTS=$PROGDIR/UN/list-tests

# unode utilities we're going to use.
readonly CMD_UNODE=$BINDIR_UNODE/unode
readonly CMD_UNODE_LOAD=$BINDIR_UNODE/unode_load
readonly CMD_UNODE_SETUP=$BINDIR_UNODE/unode_setup

readonly CMD_XTERM='/usr/openwin/bin/xterm -name UNode'

# What's my login name?
: ${LOGNAME:=$(logname)}

# Make up cluster name (make it somewhat unique by using login name and this
# script's PID to avoid two different users running the same test and
# clobbering each other's unode files in $UNODE_DIR).
readonly CLUSTER_NAME=${LOGNAME}_$$_ref_stress

# Output directory.
readonly OUTPUTDIR=$UNODE_DIR/$CLUSTER_NAME

# Some sleep amounts.
readonly DELAY_CLUSTER_INIT=3		# for cluster to init (plus # of nodes)
readonly DELAY_MODULE_START=3		# for a unode module to start
readonly DELAY_UNREF_CHECK=2		# sleep between checks for unreferenced

# Prefix string used by unode to print function result codes.
readonly RESULT_PREFIX='REF_STRESS'

# Node to run the driver.
readonly DRIVER_NODE=1

# Options and configuration information to be passed to driver.
export REFSTRESS_FD_SCRIPT2DRIVER
export REFSTRESS_FD_DRIVER2SCRIPT
export REFSTRESS_FD_SCRIPT_STDOUT
export REFSTRESS_FD_SCRIPT_STDERR
export REFSTRESS_OPT_NODES=0
export REFSTRESS_OPT_CMM_RECONFIG=0		# no CMM reconfiguration
export REFSTRESS_OPT_VERBOSITY=0		# don't be verbose

NUM_LIVE_NODES=3			# default number of nodes
USE_XTERM=0				# whether user wants xterms
KEEP_RESULTS=0				# remove $OUTPUTDIR if all tests pass
TEST_FAILURE=0


#
# Print usage
#
function usage #
{
	print -u2 "\
Usage: $PROGNAME [options...]
Options:
   -n <num_nodes>
	Number of nodes to simulate (default: $NUM_LIVE_NODES).
   -r
	Force CMM reconfiguration at random points in time.
   -t <test_number>[:<param>[,<param> ...]]
	Run test number <test_number> with the specified test parameter(s).
	Multiple -t options are allowed; these tests will be run in the
	given order.  If multiple -t options specify the same test (possibly
	(with different parameters), then the test will be run multiple times.
	See below for available tests and the expected parameter(s).
	E.g.:
		-t 3		(run test 3 with default parameters)
		-t 3:100,40	(run test 3 with parameters 100 and 40)
   -i <infrastructure_file>
	Use the template file as the infrastructure file instead of
	generating it.
   -x
	Use an xterm for each node.
   -K
	Keep the output directory even if all tests pass.
   -v
	Verbose.
   -vv
	Doubly verbose.
   -?
	prints this message."

	# List available tests.
	$CMD_LIST_TESTS
}


#
# Parse program arguments.
#
function parse_args # [program_args...]
{
	while (( $# > 0 ))
	do
		case $1 in
		-n)
			# Number of nodes to simulate.
			NUM_LIVE_NODES=$2
			if [[ -z "$NUM_LIVE_NODES" ]]
			then
				print -u2 "Option $1 requires an argument"
				exit 1
			fi
			shift
			;;
		-n?*)
			# Allow -n<arg> construct.
			NUM_LIVE_NODES=${1#-n}
			;;
		-t)
			# Test specification.
			if [[ -z "$2" ]]
			then
				print -u2 "Option $1 requires an argument"
				exit 1
			fi
			parse_test_spec "$2" || exit 1
			shift
			;;
		-t?*)
			# Allow -t<arg> construct.
			parse_test_spec "${1#-t}" || exit 1
			;;
		-i)
			# Use specified template as infrastrucutre file
			use_template=yes
			TEMPLATE=$2
			[[ ! -f $TEMPLATE ]] && { print "Cannot find $TEMPLATE." ; exit 1 ; }
			shift
			;;
		-r)
			# Do CMM reconfiguration.
			REFSTRESS_OPT_CMM_RECONFIG=1
			;;
		-x)
			# Use an xterm for each node.
			USE_XTERM=1
			;;
		-K)
			# Keep results directory.
			KEEP_RESULTS=1
			;;
		-v)
			# Be verbose.
			REFSTRESS_OPT_VERBOSITY=1
			;;
		-vv)
			# Be doubly verbose.
			REFSTRESS_OPT_VERBOSITY=2
			;;
		-\?)
			usage
			exit 0
			;;
		*)
			print -u2 "Illegal option: $1"
			usage
			exit 1
			;;
		esac
		shift
	done

	# Check specified number of nodes.
	if ! let "$NUM_LIVE_NODES >= 2" 2>/dev/null
	then
		print -u2 "Number of nodes ($NUM_LIVE_NODES) must be >= 2"
		exit 1
	fi
}


#
# Parse a test specification (-t option argument), which is in the form of
#
#	<test_num>:[arg1[,arg2 ...]]
#
# that is, a test number, followed by ':', followed by one or more
# comma-separated arguments to be passed to the specified test.
# These arguments are appended to the array TEST_ARGS[].
#
function parse_test_spec # <test_spec>
{
	typeset testnum
	typeset testargs
	typeset index

	testnum=${1%%:*}
	if [[ $testnum != +([0-9]) ]]
	then
		print -u2 "ERROR: Illegal test number: $testnum"
		return 1
	fi

	testargs=${1##${testnum}*(:)}

	# Append to TEST_ARGS[] array.
	index=${#TEST_ARGS[*]}
	TEST_ARGS[index]="$testnum $(echo "$testargs" | sed 's/,/ /g')"

	return 0
}


#
# Script setup.
#
function setup #
{
	typeset prog
	typeset nid
	typeset live_nids

	# Verify utilities exist and are executable.
	for prog in $CMD_UNODE $CMD_UNODE_LOAD $CMD_UNODE_SETUP
	do
		if [ ! -f $prog ]
		then
			print -u2 "ERROR: can't find $prog"
			exit 1
		elif [ ! -x $prog ]
		then
			print -u2 "ERROR: $prog is not executable"
			exit 1
		fi
	done

	# Verify the unode shared library exists.
	if [ ! -f $LIBDIR_UNODE/$TEST_LIB ]
	then
		print -u2 "ERROR: can't find test library" \
			"$LIBDIR_UNODE/$TEST_LIB"
		exit 1
	fi

	if (( ! USE_XTERM ))
	then
		mkdir -p $OUTPUTDIR || exit 1
		print "Output saved in ${OUTPUTDIR}/"
	fi

	# Construct node ID's.
	nid=1
	while (( nid <= NUM_LIVE_NODES ))
	do
		live_nids="$live_nids $nid"
		(( nid += 1 ))
	done

	# Construct cluster information to be passed to driver.
	REFSTRESS_OPT_NODES="$NUM_LIVE_NODES $live_nids"

	# Use template file as infrastructure, if necessary
	[[ $use_template = yes ]] && $CMD_UNODE_SETUP -n $NUM_LIVE_NODES -c $CLUSTER_NAME -i $TEMPLATE

	# Start the cluster.
	start_cluster
}


#
# Clean up routine
#
function cleanup #
{
	typeset nid
	typeset core

	teardown_cluster

	# Check core dumps.
	nid=1
	while (( nid <= NUM_LIVE_NODES ))
	do
		core=$OUTPUTDIR/node-$nid/core
		if [[ -s $core ]]
		then
			print -u2 "ERROR: Core found for node $nid: $core"
			TEST_FAILURE=1
		fi
		(( nid += 1 ))
	done

	if (( TEST_FAILURE )); then
		print "Test failures detected, examine $OUTPUTDIR/"
		print "(Please remove it when you're done.)"
	elif (( KEEP_RESULTS ))
	then
		print "Keeping output directory $OUTPUTDIR"
		print "(Please remove it when you're done.)"
	else
		print "No test failures detected, removing $OUTPUTDIR/"
		rm -rf $OUTPUTDIR
	fi

	exit $TEST_FAILURE
}


#
# Start the cluster.  The node where the driver is to run ($DRIVER_NODE) will
# be run as a co-process with the following file descriptor (re)assignments:
#
#	6		-- this script's stdout (as opposed to the unode
#				process' stdout, which could be different)
#	7		-- this script's stderr (as opposed to the unode
#				process' stderr, which could be different)
#	8		-- the pipe for script->driver communication.
#	9		-- the pipe for driver->script communication.
#
function start_cluster #
{
	typeset nid
	typeset xterm_geoms
	typeset	xterm_titles
	typeset -r xterm_size=80x24
	typeset ypos=0
	typeset -r yincr=200
	typeset nodedir

	if (( USE_XTERM ))
	then
		# Set xterm geometry and title.
		nid=1
		while (( nid <= NUM_LIVE_NODES ))
		do
			xterm_geoms[nid]="${xterm_size}+0+${ypos}"
			xterm_geoms[nid + 1]="${xterm_size}-0+${ypos}"
			(( nid += 2 ))
			(( ypos += yincr ))
		done

		nid=1
		while (( nid <= NUM_LIVE_NODES ))
		do
			xterm_titles[nid]="unode: $nid"
			(( nid += 1 ))
		done
	else
		# Set up paths to node log files.
		nid=1
		while (( nid <= NUM_LIVE_NODES ))
		do
			nodedir=$OUTPUTDIR/node-$nid
			mkdir -p $nodedir || exit 1
			NODE_LOGS[nid]=$nodedir/log
			(( nid += 1 ))
		done
	fi

	# Start all nodes.
	nid=1
	while (( nid <= NUM_LIVE_NODES ))
	do
		if (( nid == DRIVER_NODE ))
		then
			REFSTRESS_FD_SCRIPT_STDOUT=6
			REFSTRESS_FD_SCRIPT_STDERR=7
			REFSTRESS_FD_SCRIPT2DRIVER=8
			REFSTRESS_FD_DRIVER2SCRIPT=9

			# Run the node where the driver is to run as
			# a co-process.
			exec 8>&1 9>&2		# save script's stdout/stderr.
			if (( USE_XTERM ))
			then
				# Note: xterm will redirect the unode's
				# stdin/stdout/stderr to its window.
				$CMD_XTERM -geom "${xterm_geoms[nid]}" \
					-T "${xterm_titles[nid]}" \
					-e $CMD_UNODE $CLUSTER_NAME $nid \
					1-$NUM_LIVE_NODES \
					6>&8 7>&9 8<&0 9>&1 |&
			else
				$CMD_UNODE $CLUSTER_NAME $nid 1-$NUM_LIVE_NODES \
					6>&8 7>&9 8<&0 9>&1 \
					0</dev/null >${NODE_LOGS[nid]} 2>&1 |&
			fi
			exec 8>&- 9>&-		# close the saved fd's
		else
			if (( USE_XTERM ))
			then
				# Note: xterm will redirect the unode's
				# stdin/stdout/stderr to its window.
				$CMD_XTERM -geom "${xterm_geoms[nid]}" \
					-T "${xterm_titles[nid]}" \
					-e $CMD_UNODE $CLUSTER_NAME $nid \
					1-$NUM_LIVE_NODES &
			else
				$CMD_UNODE $CLUSTER_NAME $nid 1-$NUM_LIVE_NODES \
					0</dev/null >${NODE_LOGS[nid]} 2>&1 &
			fi
		fi

		NODE_PIDS[nid]=$!
		(( nid += 1 ))
	done

	# Give cluster time to initialize (amount depends on number of nodes)
	sleep $(( DELAY_CLUSTER_INIT + NUM_LIVE_NODES ))
}


#
# Kill the cluster
#
function teardown_cluster #
{
	typeset pid

	# Kill all nodes.
	for pid in ${NODE_PIDS[*]}
	do
		kill -KILL $pid 2>/dev/null
		wait $pid 2>/dev/null
	done

	unset NODE_PIDS
}


#
# Load a function on a node.
# Returns the value returned by the loaded function.
#
function unode_load   # <nodeid> <unode_lib> <function> [function_arg...]
{
	typeset nid=$1
	typeset lib=$2
	typeset func=$3
	shift 3

	typeset ret
	typeset exitval_prefix="EXITVAL_${RESULT_PREFIX}"

	# Load the function.  Capture the output of $CMD_UNODE_LOAD to
	# determine the returned value of the loaded function.
	{
		$CMD_UNODE_LOAD -x $RESULT_PREFIX $CLUSTER_NAME $nid $lib \
			$func "$@" 2>&1
		ret=$?
		(( ret != 0 )) && print "$exitval_prefix $ret"
	} | nawk -v respfx=$RESULT_PREFIX -v exitpfx=$exitval_prefix '
		$1 == respfx { retval = $2; next }
		$1 == exitpfx { retval = $2; next }
		{ print }
		END { exit retval }'

	# Note, nawk's exit value is the loaded function's returned value
	# or $CMD_UNODE_LOAD's exit value if it encounters an error.
	return $?
}


#
# If this is the first time a unode module is loaded (the first time
# $CMD_UNODE_LOAD is called) on a node, then sleep for $DELAY_MODULE_START
# seconds to give time for the module to get loaded.
#
function loading_delay # <nodeid>
{
	typeset nid=$1

	if [[ -z ${MODULE_LOADED[nid]} ]]		# if 1st time
	then
		sleep $DELAY_MODULE_START
		MODULE_LOADED[nid]=yes
	fi
}


#
# Run the driver
#
function start_driver # [driver_args ...]
{
	unode_load $DRIVER_NODE $TEST_LIB driver "$@" &
	DRIVER_PID=$!

	loading_delay $DRIVER_NODE	# give unode module time to start
	return 0
}


#
# Run a client
#
function start_client # <nodeid> <client_id>
{
	typeset nid=$1
	typeset client_id=$2

	unode_load $nid $TEST_LIB client $client_id &
	CLIENT_PIDS[client_id]=$!
	CLIENT_NODEIDS[client_id]=$nid

	loading_delay $nid		# give unode module time to start
	return 0
}


#
# End a client
#
function end_client # <client_id> [terminate]
{
	typeset client_id=$1
	typeset terminate=$2

	typeset pid=${CLIENT_PIDS[client_id]}
	typeset retval

	# Client will stay alive until it's unreferenced.
	if [ $terminate ]
	then
		kill $pid 2>/dev/null		# terminate
		retval=$?
	else
		kill -0 $pid 2>/dev/null	# check if it's still alive
		(( $? != 0 ))
		retval=$?			# nonzero if still alive
	fi

	return $retval
}


#
# Run a server
#
function start_server # <nodeid> <server_id>
{
	typeset nid=$1
	typeset server_id=$2

	unode_load $nid $TEST_LIB server $server_id &
	SERVER_PIDS[server_id]=$!
	SERVER_NODEIDS[server_id]=$nid

	loading_delay $nid		# give unode module time to start
	return 0
}


#
# End a server
#
function end_server # <server_id> [terminate]
{
	typeset server_id=$1
	typeset terminate=$2

	typeset pid=${SERVER_PIDS[server_id]}
	typeset retval

	# Server will stay alive until it's unreferenced.
	if [ $terminate ]
	then
		kill $pid 2>/dev/null		# terminate
		retval=$?
	else
		kill -0 $pid 2>/dev/null	# check if it's still alive
		(( $? != 0 ))
		retval=$?			# nonzero if still alive
	fi

	return $retval
}


#
# Run a resource balancer interface
#
function start_rsrc_balancer # <nodeid> <server_id>
{
	typeset nid=$1
	typeset server_id=$2

	unode_load $nid $TEST_LIB unode_rsrc_balancer &
	RSRC_BAL_PIDS[server_id]=$!
	RSRC_BAL_NODEIDS[server_id]=$nid

	loading_delay $nid		# give unode module time to start
	return 0
}


#
# End a resource balancer interface
#
function end_rsrc_balancer # <server_id> [terminate]
{
	typeset server_id=$1
	typeset terminate=$2

	typeset pid=${RSRC_BAL_PIDS[server_id]}
	typeset retval

	# The balancer interface will stay alive until it's unreferenced.
	if [ $terminate ]
	then
		kill $pid 2>/dev/null		# terminate
		retval=$?
	else
		kill -0 $pid 2>/dev/null	# check if it's still alive
		(( $? != 0 ))
		retval=$?			# nonzero if still alive
	fi

	return $retval
}


#
# Start an entity.
# Returns 0 if successful, nonzero otherwise.
#
function start # <nodeid> <entity> <id>
{
	typeset nid=$1
	typeset entity=$2
	typeset id=$3
	typeset retval

	case $entity in
	client)
		start_client $nid $id
		retval=$?
		;;
	server)
		start_server $nid $id
		retval=$?
		;;
	rsrc_balancer)
		start_rsrc_balancer $nid $id
		retval=$?
		;;
	*)
		print -u2 "ERROR: Unknown entity: $entity"
		retval=1
		;;
	esac

	return $retval
}


#
# End an entity.
# If the last argument (terminate) is not empty the entity will be terminated.
# Otherwise, this function simply verifies whether it's still alive or not --
# the entity will stay alive until it's unreferenced; 0 is returned if the
# process already exited, nonzero otherwise.
# Returns 0 if successful, nonzero otherwise.
#
function end # <nodeid> <entity> <id> [terminate]
{
	typeset nid=$1					# ignored
	typeset entity=$2
	typeset id=$3
	typeset terminate=$4
	typeset retval

	case $entity in
	client)
		end_client $id $terminate
		retval=$?
		;;
	server)
		end_server $id $terminate
		retval=$?
		;;
	rsrc_balancer)
		end_rsrc_balancer $id $terminate
		retval=$?
		;;
	*)
		print -u2 "ERROR: Unknown entity: $entity"
		retval=1
		;;
	esac

	return $retval
}


#
# Run test
#
function run_test # [driver_arg ...]
{
	typeset command args
	typeset pid

	start_driver "$@"

	# Read and execute commands from the driver.
	while read -p command args
	do
		case $command in
		start)	# cmd: start <nodeid> <entity> <id>
			start $args
			print -p $?			# print status as reply
			;;

		end)	# cmd: end <nodeid> <entity> <id>
			end $args
			print -p $?			# print status as reply
			;;

		terminate) # cmd: terminate <nodeid> <entity> <id>
			end $args terminate
			print -p $?			# print status as reply
			;;

		done)	# driver says it's time to quit
			break
			;;

		*)	print -u2 "ERROR: unknown command \"$command\" from" \
				"driver"
			exit 1
			;;
		esac
	done

	# Wait until all components are finished and examine their results.
	for pid in $DRIVER_PID ${SERVER_PIDS[*]} ${CLIENT_PIDS[*]} \
		   ${RSRC_BAL_PIDS[*]}
	do
		wait $pid 2>/dev/null
		if (( $? != 0 ))
		then
			TEST_FAILURE=1
		fi
	done

	unset DRIVER_PID
	unset CLIENT_PIDS CLIENT_NODEIDS
	unset SERVER_PIDS SERVER_NODEIDS
	unset RSRC_BAL_PIDS RSRC_BAL_NODEIDS
}


#
# Run each specified test
#
function do_tests #
{
	typeset testargs

	TEST_FAILURE=0

	if (( ${#TEST_ARGS[*]} == 0 ))
	then
		run_test				# run default tests
	else
		for testargs in "${TEST_ARGS[@]}"
		do
			run_test $testargs
		done
	fi
	print

	if (( TEST_FAILURE ))
	then
		print "RESULT: FAIL"
	else
		print "RESULT: PASS"
	fi

	# Prompt user if xterms were run.
	if (( USE_XTERM ))
	then
		print "\nPress [RETURN] when done. This will dismantle all" \
			"nodes." >/dev/tty
		read </dev/tty
	fi
}


#
# MAIN
#
parse_args "$@"
trap 'cleanup' EXIT
setup
do_tests
