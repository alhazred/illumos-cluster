#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)unode_config.ksh	1.7	08/05/20 SMI"
#

#
# This file is to be sourced in by ksh scripts that run unode.  It allows
# those scripts to obtain unode options from a "unode rc" file.
#
# A "unode rc" file contains definitions of unode options in the form of
#
#	<variable> = <value>
#
# (empty lines and lines starting with '#' are ignored).  Each variable
# must start with "UNODE_".  Other variables (including "UNODE_RC", which can
# can be specified only in the environment variable) are ignored.
#
# This script transforms each valid variable read from the file into an
# exported environment variable with the same name, unless that environment
# variable has been set prior to executing this script.  In other words,
# the environment variable has higher precedence than the variable in
# the "unode rc" file.
#
# There are several places where a "unode rc" can exist.  Only one of them,
# if any, will be used.  This allows the user to easily override one file with
# another.  The algorithm to find the file is as follows:
#
#	1. If a file called "unoderc" (note, it's NOT a dot-file) exists in
# 	   the current directory, then it will be read in.  Else, go to
#	   step 2.
#
#	2. If the environment variable UNODE_RC is set, it's assumed to
#	   contain a path to a file.  And if that file exists, then it will be
#	   read in; else, no file will be read in.
#
#	   Go to step 3 if environment variable UNODE_RC is not set.
#
#	3. If the file ".unoderc" (note, it IS a dot-file) exists in the
#	   user's home directory, then it will be read in.  Else, no file
#	   will be read in.
#
# This file also sets default values for some unode options, in particular
# UNODE_DIR.
#

function unode_config_read_rcfile
{
	#
	# Note, we use a function here to avoid polluting variable namespace
	# since this file is to be sourced in by scripts.
	#
	typeset file
	typeset variable value

	# Find an rc file to read.
	if [[ -f unoderc ]]
	then
		file=unoderc
	elif [[ -n "$UNODE_RC" ]]
	then
		if [[ -f "$UNODE_RC" ]]
		then
			file=$UNODE_RC
		else
			return
		fi
	elif [[ -f ~/.unoderc ]]
	then
		file=~/.unoderc
	else
		return
	fi

	/bin/nawk '
		/^[ \t]*UNODE_[^ \t=]+[ \t]*=[ \t]*.*[^ \t]/ {
			match($0, "UNODE_[^ \t=]+")
			variable = substr($0, RSTART, RLENGTH)

			# Ignore UNODE_RC since it can be specified only as
			# environment variable.
			if (variable == "UNODE_RC") {
				next
			}

			match($0, "=[ \t]*")
			value = substr($0, RSTART + RLENGTH)
			sub("[ \t]+$", "", value)

			# Store in array to allow multiple definitions of
			# the same variable to override each other.
			vararray[variable] = value
		}
		END {
			# Print variable and value pairs
			for (variable in vararray) {
				print variable " " vararray[variable]
			}
		}' $file \
	| while read variable value
	do
		# Set the variable as exported environment variable, if
		# the latter is not already set.
		eval : \${$variable:=\$value}
		eval export $variable
	done
}

unode_config_read_rcfile
unset -f unode_config_read_rcfile

#
# Set default values for some unode options.
#
: ${UNODE_DIR:=/tmp}
export UNODE_DIR
