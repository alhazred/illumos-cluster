/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)whatdir.cc	1.9	08/05/20 SMI"

/*
 * This finds the run directory of the executable, with argv[0] passed
 * in.  This code was modified from cmd/sgs/whatdir/common in the Solaris 8
 * gate.
 */

#include <sys/param.h>
#include <sys/os.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>
#include <unode/unode_config.h>

static int resolve(char *, char *, char *);
static int check_if_exec(char *);

/*
 * resolve - check for specified file in specified directory
 *	sets up dir, following symlinks.
 *	returns zero for success, or
 *	-1 for error (with errno set properly)
 *   *indir;	search directory
 *   *cmd;	search for name
 *   *dir;	directory buffer
 *  **run;	resultion name ptr ptr
 *
 */

static int
resolve(char *indir, char *cmd, char *dir)
{
	char	*p;
	int	sll;
	char	symlink[MAXPATHLEN + 1];

	errno = ENAMETOOLONG;
	if (os::strlen(indir) + os::strlen(cmd) + 2 > MAXPATHLEN) {
		return (-1);
	}

	(void) os::sprintf(dir, "%s/%s", indir, cmd);

	if (check_if_exec(dir) != 0) {
		return (-1);
	}

	while ((sll = readlink(dir, symlink, MAXPATHLEN)) >= 0) {
		symlink[sll] = 0;
		if (*symlink == '/')
			os::strcpy(dir, symlink);
		else
			(void) os::sprintf(strrchr(dir, '/'), "/%s", symlink);
	}

	if (errno != EINVAL) {
		return (-1);
	}

	// Trim off executable name
	p = strrchr(dir, '/');
	*p = 0;

	return (0);
}

/*
 * This routine checks to see if a given filename is an executable or not.
 * Logically similar to the csh statement : if  ( -x $i && ! -d $i )
 */
static int
check_if_exec(char *file)
{
	struct stat stb;
	if (stat(file, &stb) < 0) {
		return (-1);
	}
	if (S_ISDIR(stb.st_mode)) {
		return (-1);
	}
#ifdef linux
	if (!(stb.st_mode & S_IXUSR)) {
#else
	if (!(stb.st_mode & S_IEXEC)) {
#endif
		return (-1);
	}
	return (0);
}

/*
 * find_run_directory - find executable file in PATH
 * PARAMETERS:
 *	cmd	filename as typed by user
 *	dir	where to return program's directory
 *	path	user's path from environment
 * RETURNS:
 *	returns zero for success,
 *	-1 for error (with errno set properly).
 */

int
find_run_directory(char *cmd, char *dir, char *path)
{
	int	rv = 0;
	char	*f, *s;
	char	*tmp_path;
	char	cwd[MAXPATHLEN];

	if (!cmd || !*cmd || !dir) {
		errno = EINVAL;		/* stupid arguments! */
		return (-1);
	}

	if (!path || !*path)	/* missing or null path */
		path = ".";	/* assume sanity */

	tmp_path = new char[os::strlen(path)+1];

	if (tmp_path == NULL) {
		errno = EINVAL;
		return (-1);
	}

	if (!(getcwd(cwd, MAXPATHLEN)))
		return (-1); /* cant get working directory */

	f = strrchr(cmd, '/');

	if (dir) { /* user wants program directory */
		rv = -1;
		if (*cmd == '/')	/* absname given */
			rv = resolve("", cmd + 1, dir);
		else if (f)		/* relname given */
			rv = resolve(cwd, cmd, dir);
		else {	/* from searchpath */
			os::strcpy(tmp_path, path);
			f = tmp_path;
			rv = -1;
			errno = ENOENT;	/* errno gets this if path empty */
			while (*f && (rv < 0)) {
				s = f;
				while (*f && (*f != ':'))
					++f;
				if (*f)
					*f++ = 0;
				if (*s == '/')
					rv = resolve(s, cmd, dir);
				else {
					char abuf[MAXPATHLEN];
					(void) os::sprintf(abuf, "%s/%s",
					    cwd, s);
					rv = resolve(abuf, cmd, dir);
				}
			}
		}
	}
	delete [] tmp_path;
	return (rv);
}
