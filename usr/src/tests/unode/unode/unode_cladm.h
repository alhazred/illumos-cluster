/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _UNODE_CLADM_H
#define	_UNODE_CLADM_H

#pragma ident	"@(#)unode_cladm.h	1.9	08/05/20 SMI"

//
// These defines reside in sys/cladm.h, but are exported only in
// the kernel.  We've duplicated them here.
//
#define	CLUSTER_INSTALLING	0x0004	/* cluster is being installed */
#define	CLUSTER_DCS_ENABLED	0x0008	/* cluster device framework enabled */

#include <unode/systm.h>
#include <sys/cladm_int.h>
#include <sys/cl_assert.h>
#include <orb/handler/gateway.h>
#include <unode/unode_config.h>

extern "C" int cladmin(int, int, void *);

extern int cladm_start(void);
extern void cladm_end();

#endif /* _UNODE_CLADM_H */
