/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)qd.cc	1.24	08/05/20 SMI"

#include <unode/qd.h>
#include <strings.h>
#include <signal.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <quorum/common/quorum_util.h>

char			**qd_names;
int			*qd_ddesc;
reservation_keys 	*kr;
scsi2_pgre_area		*s2pa;
bool			got_signal;
os::mutex_t		signal_lock;
os::condvar_t		signal_cv;

reservation_keys::reservation_keys(uint_t num_nodes)
{
	uint_t	i;

	nodes = num_nodes;

	bzero(&reservation, sizeof (reservation));

	keylist = new mhioc_resv_key_t[nodes];

	for (i = 0; i < MHIOC_RESV_KEY_SIZE; i++) {
		null_key.key[i] = 0;
	}

	for (i = 0; i < nodes; i++) {
		scsi_pgr_util::copy(keylist[i], null_key);
	}
}

reservation_keys::~reservation_keys(void)
{
	delete [] keylist;
}

void
reservation_keys::lock()
{
	rks_lock.lock();
}

void
reservation_keys::unlock()
{
	rks_lock.unlock();
}

bool
reservation_keys::lock_held()
{
	return (rks_lock.lock_held() != 0);
}

bool
reservation_keys::key_in_list(mhioc_resv_key_t &the_key)
{
	ASSERT(lock_held());

	if (scsi_pgr_util::eq(the_key, null_key)) {
		return (false);
	}

	for (uint_t i = 0; i < nodes; i++) {
		if (scsi_pgr_util::eq(the_key, keylist[i])) {
			return (true);
		}
	}

	return (false);
}

int
reservation_keys::add_key(mhioc_resv_key_t new_key, uint64_t node_id)
{
	ASSERT(lock_held());
	ASSERT(node_id > 0 && node_id <= nodes);

	if (key_in_list(new_key)) {
		return (QD_KEY_ALREADY_IN_LIST);
	}

	scsi_pgr_util::copy(keylist[node_id-1], new_key);
	os::printf("Adding key 0x%llx\n", mhioc_key_to_int64_key(new_key));
	return (QD_OK);
}

int
reservation_keys::add_reservation(mhioc_resv_key_t key, uint64_t node_id)
{
	ASSERT(lock_held());
	ASSERT(node_id > 0 && node_id <= nodes);

	//
	// The reserving node must have its key registered.
	//
	if (!key_in_list(keylist[node_id-1])) {
		return (QD_KEY_NOT_REGISTERED);
	}

	//
	// The key passed in must match the existing key.
	//
	if (! scsi_pgr_util::eq(keylist[node_id-1], key)) {
		return (QD_KEY_NOT_REGISTERED);
	}

	bcopy(&key, reservation.key.key, sizeof (key));
	reservation.type = SCSI3_RESV_WRITEEXCLUSIVEREGISTRANTSONLY;
	reservation.scope = SCSI3_SCOPE_LOGICALUNIT;
	reservation.scope_specific_addr = 0;

	os::printf("Adding reservation: "
		"key=0x%llx,type=%d,scope=%d,scope_addr=0x%lx\n",
		mhioc_key_to_int64_key(key), reservation.type,
		reservation.scope, reservation.scope_specific_addr);
	return (QD_OK);
}

uint64_t
reservation_keys::list_keys(uint64_t ol_size, mhioc_resv_key_t *out_list)
{
	uint64_t length = 0;

	ASSERT(lock_held());

	for (uint_t i = 0; i < nodes; i++) {
		if (! scsi_pgr_util::eq(keylist[i], null_key)) {
			if (length < ol_size) {
				scsi_pgr_util::copy(out_list[length],
				    keylist[i]);
				os::printf("Reading key 0x%llx\n",
				    mhioc_key_to_int64_key(out_list[length]));
				length++;
			}
		}
	}

	return (length);
}

uint64_t
reservation_keys::list_resvs(uint64_t ol_size, mhioc_resv_desc_t *out_list)
{
	mhioc_resv_key_t	key;
	uint64_t		length = 0;

	ASSERT(lock_held());

	if (ol_size == 0) {
		return (0);
	}

	bcopy(reservation.key.key, &key, sizeof (key));
	if (! scsi_pgr_util::eq(key, null_key)) {
		length = 1;
		bcopy(&reservation, out_list, sizeof (reservation));
	}

	return (length);
}

int
reservation_keys::del_key(mhioc_resv_key_t the_key)
{
	ASSERT(lock_held());

	for (uint_t i = 0; i < nodes; i++) {
		if (scsi_pgr_util::eq(keylist[i], the_key)) {
			scsi_pgr_util::copy(keylist[i], null_key);
			return (QD_OK);
		}
	}

	return (QD_DEL_KEY_NOT_THERE);
}


scsi2_pgre_area::scsi2_pgre_area(void)
{
	uint_t	i;

	for (i = 0; i < MHIOC_RESV_KEY_SIZE; i++) {
		null_key.key[i] = 0;
	}

	//
	// The following is eqvt to the disk-scrubbing command.
	// Zero everything out when the disk is first configured
	// into the cluster.
	//
	for (i = 0; i <= NODEID_MAX; i++) {
		scsi_pgr_util::copy(node_info[i].key, null_key);
		//
		// The following is also done by the respective nodes
		// when they do a qd_open.
		//
		node_info[i].number = 0;
		node_info[i].choosing = 0;

		//
		// Normally, this is done during the scrub, but in unode,
		// QDs that are configured at the start of the test aren't
		// scrubbed, so set the er pair here.
		//
		node_info[i].epoch = (int64_t)-1;
		node_info[i].seqnum = 0;
	}
	tkown_nodeid = NODEID_UNKNOWN;
}

scsi2_pgre_area::~scsi2_pgre_area(void)
{
}


void
scsi2_pgre_area::lock(void)
{
	pgre_lock.lock();
}

void
scsi2_pgre_area::unlock(void)
{
	pgre_lock.unlock();
}


bool
scsi2_pgre_area::lock_held(void)
{
	return (pgre_lock.lock_held() != 0);
}


int
scsi2_pgre_area::get_sblkno(uint64_t& sblkno, uint64_t myid)
{
	ASSERT(lock_held());

	if (SCSI2_ACCESS_DENIED(tkown_nodeid, myid))
	    return (QD_ACCESS_DENIED);

	sblkno = 0;
	return (QD_OK);
}


int
scsi2_pgre_area::write_key(uint64_t nindex, mhioc_resv_key_t& new_key,
    uint64_t myid)
{
	ASSERT(lock_held());
	ASSERT(nindex <= NODEID_MAX);

	if (SCSI2_ACCESS_DENIED(tkown_nodeid, myid))
		return (QD_ACCESS_DENIED);

	scsi_pgr_util::copy(node_info[nindex].key, new_key);
	os::printf("Setting key for PGRE area # %d to 0x%llx\n",
	    nindex, mhioc_key_to_int64_key(new_key));
	return (QD_OK);
}

int
scsi2_pgre_area::read_key(uint64_t nindex, mhioc_resv_key_t *new_key,
    uint64_t myid)
{
	ASSERT(lock_held());
	ASSERT(nindex <= NODEID_MAX);

	if (SCSI2_ACCESS_DENIED(tkown_nodeid, myid))
	    return (QD_ACCESS_DENIED);

	scsi_pgr_util::copy(*new_key, node_info[nindex].key);
	return (QD_OK);
}

int
scsi2_pgre_area::find_key(
    uint64_t nindex,
    mhioc_resv_key_t& key,
    uint64_t &bn,
    uint64_t myid)
{
	uint_t i;
	uint_t nindex2 = (uint_t)nindex;

	ASSERT(lock_held());
	ASSERT(nindex == 0);

	if (SCSI2_ACCESS_DENIED(tkown_nodeid, myid))
	    return (QD_ACCESS_DENIED);

	// Look only in the node area, and not in the owner area.
	for (i = (nindex2 + 1); i <= NODEID_MAX; i++) {
		if (scsi_pgr_util::eq(node_info[i].key, key)) {
			bn = i;
			return (QD_OK);
		}
	}
	return (QD_KEY_NOT_REGISTERED);
}

int
scsi2_pgre_area::delete_key(uint64_t nindex, uint64_t myid)
{
	ASSERT(lock_held());
	ASSERT(nindex <= NODEID_MAX);

	if (SCSI2_ACCESS_DENIED(tkown_nodeid, myid))
	    return (QD_ACCESS_DENIED);

	scsi_pgr_util::copy(node_info[nindex].key, null_key);
	return (QD_OK);
}

int
scsi2_pgre_area::write_number(uint64_t nindex, uint64_t new_number,
    uint64_t myid)
{
	ASSERT(lock_held());
	ASSERT((nindex >= 1) && (nindex <= NODEID_MAX));

	if (SCSI2_ACCESS_DENIED(tkown_nodeid, myid))
	    return (QD_ACCESS_DENIED);

	node_info[nindex].number = new_number;
	os::printf("Setting number for PGRE area # %d to 0x%llx\n",
	    nindex, new_number);
	return (QD_OK);
}


int
scsi2_pgre_area::read_number(uint64_t nindex, uint64_t &new_number,
    uint64_t myid)
{
	ASSERT(lock_held());
	ASSERT((nindex >= 1) && (nindex <= NODEID_MAX));

	if (SCSI2_ACCESS_DENIED(tkown_nodeid, myid))
	    return (QD_ACCESS_DENIED);

	new_number = node_info[nindex].number;
	return (QD_OK);
}

int
scsi2_pgre_area::write_er_pair(uint64_t nindex, int64_t new_epoch,
    uint64_t new_seqnum, uint64_t myid)
{
	ASSERT(lock_held());
	ASSERT((nindex >= 1) && (nindex <= NODEID_MAX));

	if (SCSI2_ACCESS_DENIED(tkown_nodeid, myid))
	    return (QD_ACCESS_DENIED);

	node_info[nindex].epoch = new_epoch;
	node_info[nindex].seqnum = new_seqnum;
	os::printf("Setting epoch and seqnum for PGRE area # %d to %lld and"
	    "%llu\n", nindex, new_epoch, new_seqnum);
	return (QD_OK);
}


int
scsi2_pgre_area::read_er_pair(uint64_t nindex, int64_t &new_epoch,
    uint64_t &new_seqnum, uint64_t myid)
{
	ASSERT(lock_held());
	ASSERT((nindex >= 1) && (nindex <= NODEID_MAX));

	if (SCSI2_ACCESS_DENIED(tkown_nodeid, myid))
	    return (QD_ACCESS_DENIED);

	new_epoch = node_info[nindex].epoch;
	new_seqnum = node_info[nindex].seqnum;
	return (QD_OK);
}


int
scsi2_pgre_area::write_choosing(uint64_t nindex, uint64_t new_choosing,
    uint64_t myid)
{
	ASSERT(lock_held());
	ASSERT((nindex >= 1) && (nindex <= NODEID_MAX));

	if (SCSI2_ACCESS_DENIED(tkown_nodeid, myid))
	    return (QD_ACCESS_DENIED);

	node_info[nindex].choosing = new_choosing;
	os::printf("Setting choosing for PGRE area # %d to 0x%llx\n",
	    nindex, new_choosing);
	return (QD_OK);
}


int
scsi2_pgre_area::read_choosing(uint64_t nindex, uint64_t &new_choosing,
    uint64_t myid)
{
	ASSERT(lock_held());
	ASSERT((nindex >= 1) && (nindex <= NODEID_MAX));

	if (SCSI2_ACCESS_DENIED(tkown_nodeid, myid))
	    return (QD_ACCESS_DENIED);

	new_choosing = node_info[nindex].choosing;
	return (QD_OK);
}


int
scsi2_pgre_area::tkown(uint64_t nodeid)
{
	ASSERT(lock_held());
	tkown_nodeid = nodeid;
	return (QD_OK);
}


int
scsi2_pgre_area::release(void)
{
	ASSERT(lock_held());
	tkown_nodeid = NODEID_UNKNOWN;
	return (QD_OK);
}


int
main(int argc, char **argv)
{
	char *skip_string = "NONE";

	uint_t nodes = (uint_t)(argc - 1);
	qd_ddesc = new int[nodes];
	qd_names = new char*[nodes];

	//
	// Initialize the quorum disk - both as a SCSI2 and as a
	// SCSI3 disk, since it is not known which it will be
	// used as.
	//
	// NOTE: These initializations need to be done at this pt
	// rather than when a QD_SCSI_OPEN cmd comes in because that
	// cmd will be invoked by each node.
	//
	kr = new reservation_keys(nodes);
	s2pa = new scsi2_pgre_area();

	for (uint_t i = 1; i <= nodes; i++) {
		qd_ddesc[i] = -1;
		if (strcmp(argv[i], skip_string) == 0) {
			qd_names[i] = NULL;
		} else {
			qd_names[i] = argv[i];
		}
	}

	got_signal = false;

	if (!start_qd_doors((int)nodes)) {
		stop_qd_doors((int)nodes);
		return (1);
	}

	(void) signal(SIGTERM, wait_for_signal);
	(void) signal(SIGINT, wait_for_signal);

	signal_lock.lock();

	while (! got_signal) {
		signal_cv.wait(&signal_lock);
	}

	signal_lock.unlock();

	stop_qd_doors((int)nodes);

	return (0);
}

void
wait_for_signal(int err_sig)
{
	signal_lock.lock();

	if ((err_sig == SIGTERM) || (err_sig == SIGINT)) {
		got_signal = true;
		signal_cv.signal();
	}
	signal_lock.unlock();
}

bool
start_qd_doors(int nodes)
{
	int tfd;
	int i, j;

	for (i = 1; i <= nodes; i++) {
		int initialized = 0;

		if (qd_names[i] == NULL) {
			continue;
		}

		for (j = 1; j < i; j++) {
			if (qd_names[j] &&
			    strcmp(qd_names[i], qd_names[j]) == 0) {
				initialized = 1;
				break;
			}
		}

		if (initialized) {
			qd_ddesc[i] = qd_ddesc[j];
			os::printf("door %s already created\n", qd_names[i]);
		} else {
			qd_ddesc[i] = door_create(qd_door, qd_names[i], 0);
			(void) unlink(qd_names[i]);
			tfd = open(qd_names[i], O_CREAT|O_EXCL, 0666);
			if (tfd < 0) {
				if (errno != EEXIST) {
					os::printf("could not create %s (%s)\n",
					    qd_names[i], strerror(errno));
					return (false);
				}
				os::printf("Warning: %s already exists\n",
				    qd_names[i]);
			}
			(void) close(tfd);

			if (fattach(qd_ddesc[i], qd_names[i]) < 0) {
				os::printf("Could not attach door...");
				return (false);
			}
		}
	}
	return (true);
}

void
stop_qd_doors(int nodes)
{
	for (int i = 1; i <= nodes; i++) {
		int deleted = 0;

		for (int j = 1; j < 1; j++) {
			if (strcmp(qd_names[i], qd_names[j]) == 0) {
				deleted = 1;
				break;
			}
		}

		if (!deleted) {
			(void) door_revoke(qd_ddesc[i]);
			(void) unlink(qd_names[i]);
		}
	}
}

//
// This function receives a buffer (argp) and its size (arg_size). This
// buffer is used for both input and output.
//
// The first four values are uint64_t's meaning:
//    - the nodeid (in) / the return value (out)
//    - the fi returned value (out)
//    - the command requested (in)
//    - the maximum size to return (in) [only used when needed]
//
// From this point, the structure depends on the command requested.
// For instance, QD_SCSI_OPEN has no extra argument, and QD_SCSI3_REGISTER
// receives a key in uint64_t format. Some commands like QD_SCSI3_READKEYS
// receives the maximum number of keys to retrieve, and store starting at this
// possition the keys found.
//

void
qd_door(void *cookie, char *argp, size_t arg_size, door_desc_t *, uint_t)
{
	uint64_t		node_id, command_code, *args;
	uint64_t		max_keys;
	mhioc_resv_key_t	in_key, victim_key;
	uint64_t		pgre_index;

	ASSERT(arg_size >= 4 * sizeof (uint64_t));
	args = (uint64_t *)argp;

	node_id = args[0];
	ASSERT(node_id != 0);
	command_code = args[2];

	uint64_t &return_val = args[0];
	uint64_t &fi_return = args[1];

	return_val = QD_OK;
	fi_return = 0;

	// Checks needed:
	// Verify that we're coming from the right node
	if ((qd_names[node_id] == NULL) ||
	    (strcmp((char *)cookie, qd_names[node_id]) != 0)) {
		os::printf("Wrong door for %lld\n", node_id);
		return_val = QD_WRONG_DOOR;
		(void) door_return(argp, arg_size, NULL, 0);
	}

	switch (command_code) {
	case QD_SCSI_OPEN:
		os::printf("Got QD_SCSI_OPEN, node %lld\n", node_id);
		break;
	case QD_SCSI3_REGISTER:
		if (arg_size < 5 * sizeof (uint64_t)) {
			return_val = QD_NOT_ENOUGH_ARGS;
			break;
		}

		scsi_pgr_util::copy(in_key, *((mhioc_resv_key_t *)&(args[4])));
		os::printf("Got QD_SCSI3_REGISTER, node %lld, key 0x%llx\n",
		    node_id, mhioc_key_to_int64_key(in_key));

		kr->lock();
		(void) kr->add_key(in_key, node_id);
		kr->unlock();

		break;
	case QD_SCSI3_REGISTERANDIGNOREKEY:
		if (arg_size < 5 * sizeof (uint64_t)) {
			return_val = QD_NOT_ENOUGH_ARGS;
			break;
		}

		scsi_pgr_util::copy(in_key, *((mhioc_resv_key_t *)&(args[4])));
		os::printf("Got QD_SCSI3_REGISTERANDIGNOREKEY, node %lld,"
		    " key 0x%llx\n", node_id, mhioc_key_to_int64_key(in_key));

		kr->lock();
		(void) kr->add_key(in_key, node_id);
		kr->unlock();

		break;
	case QD_SCSI3_PREEMPT:
		if (arg_size < 6 * sizeof (uint64_t)) {
			return_val = QD_NOT_ENOUGH_ARGS;
			break;
		}

		scsi_pgr_util::copy(in_key, *((mhioc_resv_key_t *)&(args[4])));
		scsi_pgr_util::copy(victim_key,
			*((mhioc_resv_key_t *)&(args[5])));

		os::printf("Got QD_SCSI3_PREEMPT, node %lld, key 0x%llx, "
		    "victim key 0x%llx\n", node_id,
		    mhioc_key_to_int64_key(in_key),
		    mhioc_key_to_int64_key(victim_key));

		kr->lock();
		if (! kr->key_in_list(in_key)) {
			return_val = QD_KEY_NOT_REGISTERED;
		} else {
			(void) kr->del_key(victim_key);
			//
			// Pre-empting also adds a reservation for the
			// pre-emptor.
			//
			return_val = (uint_t)kr->add_reservation(in_key,
								node_id);
		}
		kr->unlock();

		break;
	case QD_SCSI3_RESERVE:
		if (arg_size < 5 * sizeof (uint64_t)) {
			return_val = QD_NOT_ENOUGH_ARGS;
			break;
		}

		scsi_pgr_util::copy(in_key, *((mhioc_resv_key_t *)&(args[4])));

		os::printf("Got QD_SCSI3_RESERVE, node %lld, key=0x%llx\n",
		    node_id, mhioc_key_to_int64_key(in_key));
		kr->lock();
		return_val = (uint_t)kr->add_reservation(in_key, node_id);
		kr->unlock();

		break;
	case QD_SCSI3_READKEYS:
		if (arg_size < 5 * sizeof (uint64_t)) {
			return_val = QD_NOT_ENOUGH_ARGS;
			break;
		}

		max_keys = args[4];
		os::printf("Got QD_SCSI3_READKEYS, node %lld, max_keys %lld\n",
		    node_id, max_keys);

		kr->lock();
		args[2] = kr->list_keys(max_keys, (mhioc_resv_key_t *)
		    &(args[3]));
		if (args[2] * sizeof (uint64_t) > arg_size + 24) {
			return_val = QD_NOT_ENOUGH_MEM;
		}
		kr->unlock();
		break;
	case QD_SCSI3_READRESV:
		if (arg_size < 5 * sizeof (uint64_t)) {
			return_val = QD_NOT_ENOUGH_ARGS;
			break;
		}

		uint64_t	max_resvs;

		max_resvs = args[4];
		os::printf("Got QD_SCSI3_READRESV, node %lld, max_resvs %lld\n",
		    node_id, max_resvs);

		kr->lock();
		args[2] = kr->list_resvs(max_resvs, (mhioc_resv_desc_t *)
		    &(args[3]));
		if (args[2] * sizeof (mhioc_resv_desc_t) > arg_size + 24) {
			return_val = QD_NOT_ENOUGH_MEM;
		}
		kr->unlock();
		break;

	case QD_SCSI2_GET_SBLKNO:
		s2pa->lock();
		return_val = (uint_t)s2pa->get_sblkno(args[2], node_id);
		s2pa->unlock();
		break;

	case QD_SCSI2_KEY_WRITE:
		if (arg_size < 6 * sizeof (uint64_t)) {
			return_val = QD_NOT_ENOUGH_ARGS;
			break;
		}

		pgre_index = args[4];
		scsi_pgr_util::copy(in_key, *((mhioc_resv_key_t *)&(args[5])));

		os::printf("Got QD_SCSI2_KEY_WRITE, node %lld, blkno %lld, "
		    "key 0x%llx\n", node_id, pgre_index,
		    mhioc_key_to_int64_key(in_key));

		s2pa->lock();
		return_val = (uint_t)s2pa->write_key(pgre_index, in_key,
		    node_id);
		s2pa->unlock();
		break;
	case QD_SCSI2_KEY_READ:
		if (arg_size < 5 * sizeof (uint64_t)) {
			return_val = QD_NOT_ENOUGH_ARGS;
			break;
		}

		pgre_index = args[4];

		s2pa->lock();
		return_val = (uint_t)s2pa->read_key(pgre_index,
		    (mhioc_resv_key_t *)&args[2], node_id);
		s2pa->unlock();
		break;
	case QD_SCSI2_KEY_FIND:
		if (arg_size < 6 * sizeof (uint64_t)) {
			return_val = QD_NOT_ENOUGH_ARGS;
			break;
		}

		pgre_index = args[4];
		scsi_pgr_util::copy(in_key, *((mhioc_resv_key_t *)&(args[5])));

		s2pa->lock();
		return_val = (uint_t)s2pa->find_key(pgre_index, in_key,
		    args[2], node_id);
		s2pa->unlock();

		break;
	case QD_SCSI2_KEY_DELETE:
		if (arg_size < 5 * sizeof (uint64_t)) {
			return_val = QD_NOT_ENOUGH_ARGS;
			break;
		}

		pgre_index = args[4];

		s2pa->lock();
		return_val = (uint_t)s2pa->delete_key(pgre_index, node_id);
		s2pa->unlock();

		break;
	case QD_SCSI2_CHOOSING_WRITE:
		if (arg_size < 6 * sizeof (uint64_t)) {
			return_val = QD_NOT_ENOUGH_ARGS;
			break;
		}

		pgre_index = args[4];

		s2pa->lock();
		return_val = (uint_t)s2pa->write_choosing(pgre_index,
		    args[5], node_id);
		s2pa->unlock();

		break;
	case QD_SCSI2_CHOOSING_READ:
		if (arg_size < 5 * sizeof (uint64_t)) {
			return_val = QD_NOT_ENOUGH_ARGS;
			break;
		}

		pgre_index = args[4];

		s2pa->lock();
		return_val = (uint_t)s2pa->read_choosing(pgre_index,
		    args[2], node_id);
		s2pa->unlock();

		break;
	case QD_SCSI2_NUMBER_WRITE:
		if (arg_size < 6 * sizeof (uint64_t)) {
			return_val = QD_NOT_ENOUGH_ARGS;
			break;
		}

		pgre_index = args[4];

		s2pa->lock();
		return_val = (uint_t)s2pa->write_number(pgre_index,
		    args[5], node_id);
		s2pa->unlock();

		break;
	case QD_SCSI2_NUMBER_READ:

		if (arg_size < 5 * sizeof (uint64_t)) {
			return_val = QD_NOT_ENOUGH_ARGS;
			break;
		}

		pgre_index = args[4];

		s2pa->lock();
		return_val = (uint_t)s2pa->read_number(pgre_index,
		    args[2], node_id);
		s2pa->unlock();

		break;
	case QD_SCSI_ER_PAIR_WRITE:
		if (arg_size < 7 * sizeof (uint64_t)) {
			return_val = QD_NOT_ENOUGH_ARGS;
			break;
		}

		pgre_index = args[4];

		s2pa->lock();
		return_val = (uint_t)s2pa->write_er_pair(pgre_index,
		    args[5], args[6], node_id);
		s2pa->unlock();

		break;
	case QD_SCSI_ER_PAIR_READ:

		if (arg_size < 6 * sizeof (uint64_t)) {
			return_val = QD_NOT_ENOUGH_ARGS;
			break;
		}

		pgre_index = args[4];

		// Use a separate arg to read into to avoid casting issues.
		int64_t new_epoch;

		s2pa->lock();
		return_val = (uint_t)s2pa->read_er_pair(pgre_index,
		    new_epoch, args[3], node_id);
		s2pa->unlock();
		args[2] = new_epoch;

		break;
	case QD_SCSI2_TKOWN:
		s2pa->lock();
		return_val = (uint_t)s2pa->tkown(node_id);
		s2pa->unlock();

		break;

	case QD_SCSI2_RELEASE:
		s2pa->lock();
		return_val = (uint_t)s2pa->release();
		s2pa->unlock();

		break;
	default:
		return_val = QD_INVALID_COMMAND;
		break;
	}
	(void) door_return(argp, arg_size, NULL, 0);
}
