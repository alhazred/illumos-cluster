/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)unode_cladm.cc	1.10	08/05/20 SMI"

//
// tests/unode/unode/unode_cladm.cc
//
// This module provides emulation for the cladm system call in unode.
// It is provided by a door interface called cladm, exported
// in the configuration directory.
//
// The majority of this code was originally in the fakegate.cc
// file, as it was originally intended to leverage that door interface,
// however, the purpose of the two doors are disjoint enough to
// justify a seperate implementation (for now).
//

#include <sys/types.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <sys/os.h>
#include <stdio.h>
#include <unistd.h>
#include <limits.h>
#include <dlfcn.h>
#include <stdlib.h>
#include <door.h>
#include "unode_cladm.h"

//
// for unode version of ORB, emulate the kernel variable cluster_bootflags
//
int cluster_bootflags = CLUSTER_CONFIGURED | CLUSTER_BOOTED;

static char cladm_file[PATH_MAX + 1] = {0};	// cladm door file
static int cladm_ddesc = -1;			// cladm door descriptor

#ifdef linux
static int
cladm_internal(int fac, int cmd, void *arg)
{
	errno = ENOTSUP;
	return (-1);
}
static void
cladm_door(void *, char *, size_t, door_desc_t *, uint_t)
{
}
#else

//
// Following code was duplicated from usr/src/uts/common/syscall/cladm.c
// for emulation of the cladm system call in unode.
//

//
// cladm(2) cluster administation system call.
//
static int
cladm_internal(int fac, int cmd, void *arg)
{
	int error = 0;
	int copyout_bootflags;
	extern nodeid_t clconf_get_nodeid();

	switch (fac) {
	case CL_INITIALIZE:
		if (cmd != CL_GET_BOOTFLAG) {
			error = EINVAL;
			printf("cmd != CL_GET_BOOTFLAG\n");
			break;
		}

		/*
		 * The CLUSTER_INSTALLING and CLUSTER_DCS_ENABLED bootflags are
		 * internal flags. We do not want to expose these to the user
		 * level.
		 */
		copyout_bootflags = (cluster_bootflags &
		    ~(CLUSTER_INSTALLING | CLUSTER_DCS_ENABLED));
		if (copyout(&copyout_bootflags, arg, sizeof (int))) {
			error = EFAULT;
		}
		break;

	case CL_CONFIG:
		/*
		 * We handle CL_NODEID here so that the node number
		 * can be returned if the system is configured as part
		 * of a cluster but not booted as part of the cluster.
		 */
		if (cmd == CL_NODEID) {
			nodeid_t nid;

			/* return error if not configured as a cluster */
			if (!(cluster_bootflags & CLUSTER_CONFIGURED)) {
				error = ENOSYS;
				break;
			}

			nid = clconf_get_nodeid();
			error = copyout(&nid, arg, sizeof (nid));
			break;
		}
		/* FALLTHROUGH */

	default:
		if ((cluster_bootflags & (CLUSTER_CONFIGURED|CLUSTER_BOOTED)) !=
		    (CLUSTER_CONFIGURED|CLUSTER_BOOTED)) {
			error = EINVAL;
			break;
		}
		error = cladmin(fac, cmd, arg);
		/*
		 * error will be -1 if the cladm module cannot be loaded;
		 * otherwise, it is the errno value returned
		 * (see {i86,sparc}/ml/modstubs.s).
		 */
		if (error < 0)
			error = errno;
		break;
	}

	if (error) {
		errno = error;
		return (-1);
	} else {
		return (0);
	}
}

//
// This is the server side door handler for cladm.  Most of the work is
// performed in cladmin, however, door handles for the name server
// need to be bundled up here to be passed back to the client properly.
//
static void
cladm_door(void *handle, char *argp, size_t arg_size, door_desc_t *dp,
    uint_t n_desc)
{
	door_info_t dinfo;
	door_desc_t res;
	cllocal_ns_data_t ns_data;

	int *fac = &((int *)argp)[0];
	int *cmd = &((int *)argp)[1];
	void *arg = (void *)&((int *)argp)[2];

	//
	// We currently pass back our errno/exit code via our fac/cmd arguments
	//
	if (*fac == CL_CONFIG) {
		//
		// CL_GET_LOCAL_NS and CL_GET_NS_DATA are exceptional
		// in that they pass back door handles to the caller.
		//
		if (*cmd == CL_GET_LOCAL_NS) {
			*cmd = cladm_internal(*fac, *cmd, arg);
			*fac = errno;
			//
			// Check to make sure we have a valid door descriptor
			//
			if (door_info(*(int *)arg, &dinfo) < 0) {
				ASSERT(("cladm door_info", 0));
				perror("cladm door_info");
				exit(1);
			}
			//
			// Send the file descriptor back
			//
			res.d_attributes = DOOR_DESCRIPTOR;
			res.d_data.d_desc.d_descriptor = *(int *)arg;

			//
			// Our arguments only pass back the cladm errno/edit
			// information.
			//
			arg_size = sizeof (int);
			arg_size += sizeof (int);

			//
			// Send it all back
			//
			door_return((char *)argp, arg_size, &res, 1);
			ASSERT(("CL_GET_LOCAL_NS door_return", 0));
			perror("CL_GET_LOCAL_NS door_return");
			exit(1);
		}
		if (*cmd == CL_GET_NS_DATA) {
			//
			// This is the main ns object interface.
			// The above command is included for completeness
			// but is not (currently) called.
			//
			*cmd = cladm_internal(*fac, *cmd, &ns_data);
			*fac = errno;

			//
			// Check to make sure we have a valid door descriptor
			//
			if (door_info(ns_data.filedesc, &dinfo) < 0) {
				ASSERT(("cladm door_info", 0));
				perror("cladm door_info");
				exit(1);
			}

			//
			// Send the file descriptor back
			//
			res.d_attributes = DOOR_DESCRIPTOR;
			res.d_data.d_desc.d_descriptor = ns_data.filedesc;

			//
			// Send the typeid back in the arguments beyond
			// the errno/exit code.
			//
			bcopy(ns_data.tid, arg, sizeof (ns_data.tid));

			//
			// Accommodate the size of both the typeid and
			// errno/exit arguments.
			//
			arg_size = sizeof (int);
			arg_size += sizeof (int);
			arg_size += sizeof (ns_data.tid);

			//
			// Send it all back
			//
			door_return((char *)argp, arg_size, &res, 1);
			ASSERT(("CL_GET_NS_DATA door_return", 0));
			perror("CL_GET_NS_DATA door_return");
			exit(1);
		}
	}

	*cmd = cladm_internal(*fac, *cmd, arg);
	*fac = errno;
	door_return((char *)argp, arg_size, NULL, 0);
	ASSERT(("cladm door_return", 0));
	perror("cladm door_return");
	exit(1);
}
#endif

#ifdef	linux
int
cladm_start(void)
{
	errno = ENOTSUP;
	return (-1);
}
#else
int
cladm_start(void)
{
	int	tfd;
	static char cladm_file[PATH_MAX + 1];

	cladm_ddesc = door_create(cladm_door, NULL, 0);
	if (cladm_ddesc < 0) {
		os::printf("cladm door server failed to initialize (%s)\n",
			strerror(errno));
		goto error;
	}

	// Construct file name to attach door to.
	(void) sprintf(cladm_file, "%s/cladm",
	    unode_config::the().node_dir());

	// Create the file to attach door to.
	(void) unlink(cladm_file);
	tfd = open(cladm_file, O_CREAT|O_EXCL, 0666);
	if (tfd < 0) {
		if (errno != EEXIST) {
			os::printf("cladm could not create %s (%s)\n",
				cladm_file, strerror(errno));
			goto error;
		}
		os::printf("Warning: %s already exists\n", cladm_file);
	}
	(void) close(tfd);

	// Attach door to file.
	if (fattach(cladm_ddesc, cladm_file) < 0) {
		os::printf("cladm could not attach door to %s (%s)\n",
			cladm_file, strerror(errno));
		goto error;
	}

	return (0);					// success

error:
	// Clean up on error
	(void) close(cladm_ddesc);
	cladm_ddesc = -1;
	if (cladm_file[0] != '\0') {
		(void) unlink(cladm_file);
		cladm_file[0] = '\0';
	}
	return (-1);
}
#endif

#ifdef	linux
void
cladm_end()
{
}
#else
void
cladm_end()
{
	if (cladm_file[0] != '\0') {
		(void) close(cladm_ddesc);
		cladm_ddesc = -1;

		(void) fdetach(cladm_file);

		(void) unlink(cladm_file);
		cladm_file[0] = '\0';
	}
}
#endif
