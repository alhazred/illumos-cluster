/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)sim_clconf.cc	1.67	08/05/20 SMI"

#include <limits.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <ccr/persistent_table.h>
#include <arpa/inet.h>
#include <sys/heartbeat.h>
#include "unode_config.h"
#include <sys/nodeset.h>

int
create_infrastructure(nodeset &node_list)
{
	char		infrastructure[PATH_MAX + 1];
	char		path[PATH_MAX + 1];
	char		*devname;
	FILE		*fp;		// pointer to infrastructure file
	FILE		*qp;		// pointer to quorum file
	nodeid_t	lowest_nodeid, nodeid;
	uint_t		num_adapters;
	uint_t		num_transports;
	uint_t		num_nodes;
	uint_t		trnum, adnum;
	uint_t		adapter;
	uint_t		qvote;
	in_addr_t	in_netno, in_ip;
	struct in_addr	in;
	unode_config	&config = unode_config::the();

	// Check if infrastructure file exists (under this node's directory).
	(void) sprintf(infrastructure, "%s/infrastructure", config.ccr_dir());
	if (access(infrastructure, R_OK) == 0) {
		os::printf("Using existing infrastructure file.\n");
		return (0);
	}

	// Create infrastructure file.
	if ((fp = fopen(infrastructure, "w")) == NULL) {
		perror(infrastructure);
		return (1);
	}

	// Calculate lowest nodeid from node_list
	lowest_nodeid = NODEID_UNKNOWN;
	for (nodeid = 1; nodeid <= NODEID_MAX; ++nodeid) {
		if (node_list.contains(nodeid)) {
			lowest_nodeid = nodeid;
			break;
		}
	}
	ASSERT(lowest_nodeid != NODEID_UNKNOWN);

	// Calculate number of nodes in the cluster from node_list
	num_nodes = 0;
	for (nodeid = 1; nodeid <= NODEID_MAX; ++nodeid) {
		if (node_list.contains(nodeid)) {
			num_nodes++;
		}
	}

	// Open the quorum file (under the cluster directory)
	(void) sprintf(path, "%s/quoruminfo", config.cluster_dir());
	if ((qp = fopen(path, "r")) == NULL) {
		os::printf("Quoruminfo file not found. Ignored\n");
	}

	num_adapters = (uint_t)config.num_adapters();
	if (config.direct_connect()) {
		// When doing direct_connect,
		// config.num_adapters() is the number of
		// adapters per node-pair
		num_adapters *= (num_nodes - 1);
	}
	num_transports = (uint_t)config.num_transports();

	os::printf("Creating infrastructure file ...\n");

	(void) fprintf(fp,
		"cluster.name\t%s\n",
		config.cluster_name());
	(void) fprintf(fp,
		"cluster.state\tenabled\n");

	for (nodeid = 1; nodeid <= NODEID_MAX; ++nodeid) {
		if (!node_list.contains(nodeid)) {
			continue;
		}

		// If quorum devices are specified all nodes gets 1 vote
		// If not, the lowest node gets 1 vote and rest get 0
		// XX This needs to be fixed once all the tests are fixed
		qvote = (qp != NULL ? 1 : ((nodeid == lowest_nodeid) ? 1 : 0));

		(void) fprintf(fp,
			"cluster.nodes.%i.state\tenabled\n",
			nodeid);
		(void) fprintf(fp,
			"cluster.nodes.%i.name\tnode-%i\n",
			nodeid, nodeid);
		(void) fprintf(fp,
			"cluster.nodes.%i.properties.quorum_vote\t%i\n",
			nodeid, qvote);
		(void) fprintf(fp,
			"cluster.nodes.%i.properties.quorum_resv_key\t%016x\n",
			nodeid, nodeid);

		for (trnum = 0; trnum < num_transports; trnum++) {
			const char	*trtype = config.transports()[trnum];
			const char	**stransports =
				unode_config::supported_transports;

			// Set adapter name based on transport type.
			if (strcmp(trtype, "rsm") == 0) {
				devname = "rsmunode";
			} else if (strcmp(trtype, "doors") == 0) {
				devname = "door";
			} else {
				devname = "adp";
			}

			for (adnum = 1; adnum <= num_adapters; adnum++) {
				adapter = trnum * num_adapters + adnum;

				int instance = adapter;
				// See if adapter was specified
				const char *adp =
				    config.adapter(nodeid, adnum - 1);
				if (adp != NULL) {
					devname = (char *)adp;
					instance = config.adapter_instance(
					    nodeid, adnum - 1);
				}

				(void) fprintf(fp,
					"cluster.nodes.%i.adapters.%i.name\t"
					"%s%i\n",
					nodeid, adapter, devname, instance);
				(void) fprintf(fp,
					"cluster.nodes.%i.adapters.%i.state\t"
					"enabled\n",
					nodeid, adapter);
				(void) fprintf(fp,
					"cluster.nodes.%i.adapters.%i."
					"properties.transport_type\t%s\n",
					nodeid, adapter, trtype);
				for (int j = 0; stransports[j] != NULL; j++) {
				    (void) fprintf(fp,
					"cluster.nodes.%i.adapters.%i."
					"properties.%s_heartbeat_quantum\t%d\n",
					nodeid, adapter, stransports[j],
					HB_EMERGENCY_DEFAULT_QUANTUM);
				    (void) fprintf(fp,
					"cluster.nodes.%i.adapters.%i."
					"properties.%s_heartbeat_timeout\t%d\n",
					nodeid, adapter, stransports[j],
					HB_EMERGENCY_DEFAULT_TIMEOUT);
				}
				(void) fprintf(fp,
					"cluster.nodes.%i.adapters.%i."
					"properties.device_name\t%s\n",
					nodeid, adapter, devname);
				(void) fprintf(fp,
					"cluster.nodes.%i.adapters.%i."
					"properties.device_instance\t%i\n",
					nodeid, adapter, instance);

				// Calculate IP address
				in_netno = (in_addr_t)adapter << 7;
				in_netno |= inet_network("172.16.0.0");
				in_ip = (uint_t)nodeid;	// same as nodeid
				// Arguments in host byte order.
				// Result in network byte order.
				in = inet_makeaddr(in_netno, in_ip);
				(void) fprintf(fp,
					"cluster.nodes.%i.adapters.%i."
					"properties.ip_address\t%s\n",
					nodeid, adapter, inet_ntoa(in));
				(void) fprintf(fp,
					"cluster.nodes.%i.adapters.%i."
					"properties.netmask\t255.255.255.128\n",
					nodeid, adapter);

				if (strcmp(trtype, "doors") == 0) {
					// Store the name of the door in
					// CCR for each adapter
					config.node_path(path, nodeid,
						"/transport.%i", adapter);
					(void) fprintf(fp,
						"cluster.nodes.%i.adapters.%i."
						"properties.door_name\t%s\n",
						nodeid, adapter, path);
				} else if (strcmp(trtype, "rsm") == 0) {
					// Emulation for rsm_transport in
					// unode mode.  The node number
					// is embedded in adapter_id.
					(void) fprintf(fp,
						"cluster.nodes.%i.adapters."
						"%i.properties.adapter_id\t"
						"%d\n",
						nodeid, adapter,
						(nodeid << 8) +
							(uint_t)adapter);
				}

				(void) fprintf(fp,
					"cluster.nodes.%i.adapters.%i."
					"ports.1.name\t1\n",
					nodeid, adapter);
				(void) fprintf(fp,
					"cluster.nodes.%i.adapters.%i."
					"ports.1.state\tenabled\n",
					nodeid, adapter);
			}
		}
	}

	if (!config.direct_connect()) {
	    for (adapter = 1; adapter <= num_adapters * num_transports;
		++adapter) {
		(void) fprintf(fp,
			"cluster.blackboxes.%i.name\t%s-%i\n",
			adapter,
			config.transports()[(adapter - 1) / num_adapters],
			adapter);	//lint !e414 complains of division by 0
		(void) fprintf(fp,
			"cluster.blackboxes.%i.state\tenabled\n",
			adapter);
		(void) fprintf(fp,
			"cluster.blackboxes.%i.properties.type\tswitch\n",
			adapter);
		for (nodeid = 1; nodeid <= NODEID_MAX; ++nodeid) {
			if (!node_list.contains(nodeid)) {
				continue;
			}

			// cable numbers are being derived from adapter/nodeid
			// XX This could done by just using a counter too
			uint_t cable = ((adapter - 1) * NODEID_MAX) + nodeid;

			(void) fprintf(fp,
				"cluster.blackboxes.%i.ports.%i.name\t%i\n",
				adapter, nodeid, nodeid);
			(void) fprintf(fp,
				"cluster.blackboxes.%i.ports.%i.state\t"
				"enabled\n",
				adapter, nodeid);
			(void) fprintf(fp,
				"cluster.cables.%i.properties.end1\t"
				"cluster.nodes.%i.adapters.%i.ports.1\n",
				cable, nodeid, adapter);
			(void) fprintf(fp,
				"cluster.cables.%i.properties.end2\t"
				"cluster.blackboxes.%i.ports.%i\n",
				cable, adapter, nodeid);
		}
	    }
	} else {
	    //
	    // For direct_connect clusters, we have n_adapters * n_transports
	    // cables between every pair of nodes
	    // We assign cable numbers using a cluster wide cable number counter
	    // so that we get unique cable numbers
	    // We use per-node adapter number counters to get adapter numbers
	    // for the adapters

	    // The first two for loops, cover every node-pair
	    // the while loop sets up the cables between the nodes

	    // XXX This algorithm depends on the adapter initialization above
	    // to have assigned transport names to adapters in the same order
	    // on all nodes, we are blindly connecting up the adapters here
	    // without checking that the adapter types match.

	    // cluster wide cable number (counter)
	    uint_t cable = 0;
	    // per-node adapter number counter
	    uint_t adapter_no[NODEID_MAX+1] = { 0 };
	    nodeid_t nodeid1, nodeid2;

	    for (nodeid1 = 1; nodeid1 <= NODEID_MAX; ++nodeid1) {
		if (!node_list.contains(nodeid1)) {
			continue;
		}
		// Do for all higher numbered nodes
		for (nodeid2 = nodeid1+1; nodeid2 <= NODEID_MAX; ++nodeid2) {
			if (!node_list.contains(nodeid2)) {
				continue;
			}

			adnum = (uint_t)config.num_adapters() * num_transports;
			while (adnum-- != 0) {
				cable++;	// increment cable number
				(void) fprintf(fp,
				    "cluster.cables.%d.properties.end1\t"
				    "cluster.nodes.%d.adapters.%d.ports.1\n",
				    cable, nodeid1, ++adapter_no[nodeid1]);
				(void) fprintf(fp,
				    "cluster.cables.%d.properties.end2\t"
				    "cluster.nodes.%d.adapters.%d.ports.1\n",
				    cable, nodeid2, ++adapter_no[nodeid2]);
			}
		}
	    }
	}

	if (qp != NULL) {
		int	ch;

		// Append the quoruminfo file to the infrastructure file.
		while ((ch = fgetc(qp)) != EOF) {
			(void) fputc(ch, fp);
		}
		(void) fclose(qp);
	}

	(void) fclose(fp);

	if (ccrlib::initialize_ccr_table(infrastructure, "0") != 0) {
		os::panic("initialize_ccr_table");
	}

	return (0);
}

// Wrapper functions to make unode code look similar to kernel code
nodeid_t
clconf_get_nodeid()
{
	return (unode_config::the().node_number());
}

void
clconf_init()
{
	// XX Eventually this should read the /etc/cluster/nodeid file instead
	// of generating it as in above.
}

int
create_adapter_clplfile(const char *dname)
{
	char		fname[PATH_MAX + 1];
	unode_config	&config = unode_config::the();
	const char	**supported_transports =
				unode_config::supported_transports;
	FILE		*fp;
	int		i;

	sprintf(fname, "%s/SUNW.adapter.%s.clpl", config.clpl_dir(), dname);
	if ((fp = fopen(fname, "w")) == NULL) {
		perror(fname);
		return (1);
	}
	(void) fprintf(fp, "adapters.%s.transport_type.type\tenum\n", dname);
	(void) fprintf(fp, "adapters.%s.transport_type.required\t1\n", dname);
	(void) fprintf(fp,
	    "adapters.%s.transport_type.default\t%s\n", dname,
	    unode_config::default_transports);

	(void) fprintf(fp,
	    "adapters.%s.transport_type.values\tdoors", dname);
	for (i = 1; supported_transports[i] != NULL; i++)
		(void) fprintf(fp, ":%s", supported_transports[i]);
	fprintf(fp, "\n");
	(void) fprintf(fp, "adapters.%s.bandwidth.type\tint\n", dname);
	(void) fprintf(fp, "adapters.%s.bandwidth.required\t0\n", dname);
	(void) fprintf(fp, "adapters.%s.bandwidth.min\t0\n", dname);
	(void) fprintf(fp, "adapters.%s.bandwidth.max\t100\n", dname);
	(void) fprintf(fp, "adapters.%s.bandwidth.default\t100\n", dname);
	(void) fprintf(fp, "adapters.%s.nw_bandwidth.type\tint\n", dname);
	(void) fprintf(fp, "adapters.%s.nw_bandwidth.required\t0\n", dname);
	(void) fprintf(fp, "adapters.%s.nw_bandwidth.min\t0\n", dname);
	(void) fprintf(fp, "adapters.%s.nw_bandwidth.max\t100\n", dname);
	(void) fprintf(fp, "adapters.%s.nw_bandwidth.default\t100\n", dname);

	for (i = 0; supported_transports[i] != NULL; i++) {
	    (void) fprintf(fp, "adapters.%s.%s_heartbeat_quantum.type\tint\n",
		dname, supported_transports[i]);
	    (void) fprintf(fp, "adapters.%s.%s_heartbeat_quantum.required\t0\n",
		dname, supported_transports[i]);
	    (void) fprintf(fp, "adapters.%s.%s_heartbeat_quantum.min\t500\n",
		dname, supported_transports[i]);
	    (void) fprintf(fp, "adapters.%s.%s_heartbeat_quantum.max\t10000\n",
		dname, supported_transports[i]);
	    (void) fprintf(fp,
		"adapters.%s.%s_heartbeat_quantum.default\t1000\n",
		dname, supported_transports[i]);
	    (void) fprintf(fp, "adapters.%s.%s_heartbeat_timeout.type\tint\n",
		dname, supported_transports[i]);
	    (void) fprintf(fp, "adapters.%s.%s_heartbeat_timeout.required\t0\n",
		dname, supported_transports[i]);
	    (void) fprintf(fp, "adapters.%s.%s_heartbeat_timeout.min\t1500\n",
		dname, supported_transports[i]);
	    (void) fprintf(fp,
		"adapters.%s.%s_heartbeat_timeout.max\t3600000\n",
		dname, supported_transports[i]);
	    (void) fprintf(fp,
		"adapters.%s.%s_heartbeat_timeout.default\t10000\n",
		dname, supported_transports[i]);
	}
	fclose(fp);
	return (0);
}


int
create_clplfiles()
{
	FILE	*fp;
	char		fname[PATH_MAX + 1];
	unode_config	&config = unode_config::the();

	sprintf(fname, "%s/SUNW.generic.clpl", config.clpl_dir());
	if ((fp = fopen(fname, "w")) == NULL) {
		perror(fname);
		return (1);
	}
	(void) fprintf(fp, "adapters.generic.device_name.type\tenum\n");
	(void) fprintf(fp, "adapters.generic.device_name.required\t1\n");
	(void) fprintf(fp, "adapters.generic.device_name.default\tdoor\n");
	(void) fprintf(fp,
		"adapters.generic.device_name.values\tdoor\n");
	(void) fprintf(fp, "blackboxes.generic.type.type\tenum\n");
	(void) fprintf(fp, "blackboxes.generic.type.required\t1\n");
	(void) fprintf(fp, "blackboxes.generic.type.default\tswitch\n");
	(void) fprintf(fp, "blackboxes.generic.type.values\tswitch\n");
	(void) fclose(fp);
	if (create_adapter_clplfile("door") != 0) {
		return (1);
	}

	return (0);
}
