/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_UNODE_OUTPUT_H
#define	_UNODE_OUTPUT_H

#pragma ident	"@(#)output.h	1.6	08/05/20 SMI"

#include <stdio.h>

// For unode entry points that want to accept "-o filename" as
// an argument for specifying an output file, the output class
// makes it easier.  Entry points use it like:
//
//	int
//	foo(int argc, char **argv)
//	{
//		output out(&argc, argv);
//
//		fprintf(out, "...");
//		...
//	}
//
// The constructor scans the argument list looking for "-o filename".
// If it isn't found, or no filename follows -o, the arguments are
// unchanged and stdout is used.  If "-o filename" is found it is
// removed from the arguments and argc is updated.  If the file can
// be opened for writing, its pointer is stored.  No indication of
// failure to open the file is given.  "-o filename" can be anywhere
// in the arguments.
//
// When stdout is used, output will go the window where the unode
// process is running, not where unode_load is run.  Giving the
// argument "-o `tty`" to the unode_load command wil cause the output
// to go to unode_load's window.
//
// Note that we don't use freopen because that would affect unode
// or other loaded modules use of stdout.
//
// The destructor closes the file if it's not stdout and is
// convenient because an entry point can call return anytime and
// not worry about explicitly closing the file.

class output {
public:
	output(int *argcp, char **argv);

	~output()
	{
		if (fp != stdout)
			(void) fclose(fp);
	}

	// conversion function from class output to FILE pointer
	// so the output object can be used as the first argument
	// to fprintf

	operator FILE *()
	{
		return (fp);
	}

private:
	FILE *fp;
};

#endif // _UNODE_OUTPUT_H
