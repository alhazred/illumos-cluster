/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)unode_config.cc	1.41	08/05/20 SMI"

#include <limits.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/resource.h>
#include <errno.h>
#include <stdio.h>
#include <regex.h>
#include <unistd.h>
#include <signal.h>
#include <pwd.h>
#include <libgen.h>
#include <stdarg.h>
#include <sys/os.h>
#include <sys/dbg_printf.h>
#include <ccr/persistent_table.h>

#include "unode.h"
#include "unode_util.h"
#include "fakegate.h"

//
// maxusers - defines the maximum numbers of users for a unode system.
// This value is used to configure the size of various data structures.
// "maxusers" corresponds directly to same named variable for real systems.
// Recommend using a small value for unode systems.
//
volatile int	maxusers = 10;

// Global variable to store the args of the binary currently in execution
char	**unode_args;


//
// List of supported transports.
//
const char * const	unode_config::supported_transports[] = {
	"dlpi",
	"rsm",
	"doors",
	NULL
};


//
// Default values.
//
const int	unode_config::default_num_adapters = 4;
const char	unode_config::default_transports[] = "doors";
const char	unode_config::default_unode_dir[] = "/tmp";


unode_config	unode_config::_the_unode_config;

unode_config::unode_config()
{
	_dbg_bufname_hash_initialized = false;
	_node_number = NODEID_UNKNOWN;
	_verbose = false;
	_boot_time_stamp = false;
	_hang_on_signal = false;
	_direct_connect = false;
	_ccr_dir = NULL;
	_clpl_dir = NULL;
	_cluster_dir = NULL;
	_cluster_init = NULL;
	_cluster_name = NULL;
	_node_dir = NULL;
	_node_init = NULL;
	_orig_cwd = NULL;
	_pid_file = NULL;
	_save_dbg_buf = NULL;
	_num_adapters = default_num_adapters;
	_num_transports = 0;
	_transports = NULL;
	_adapters = NULL;
	_adapter_instances = NULL;

	// Set default transports.
	_parse_transport_list(default_transports);

	// Set default unode directory.
	_unode_dir = os::strdup(default_unode_dir);
	ASSERT(_unode_dir != NULL);
}

unode_config::~unode_config()
{
	int			i;

	_dbg_bufname_hash.dispose();

	delete [] _ccr_dir;
	delete [] _cluster_dir;
	delete [] _cluster_init;
	delete [] _cluster_name;
	delete [] _node_dir;
	delete [] _node_init;
	delete [] _orig_cwd;
	delete [] _save_dbg_buf;
	delete [] _unode_dir;

	for (i = 0; i < _num_transports; ++i) {
		delete [] _transports[i];
	}
	delete [] _transports;
	_num_transports = 0;

	if (_pid_file != NULL) {
		(void) unlink(_pid_file);
		delete [] _pid_file;
	}
}


//
// Initialize unode configuration.
// Read unode configuration options from rc file and environment.
// Returns 0 if successful.
//
int
unode_config::initialize(const char *clust_name, nodeid_t nodeid,
		bool verbose /* = false */)
{
	_node_number = nodeid;
	_verbose = verbose;

	// Copy cluster name given in the argument.
	_cluster_name = os::strdup(clust_name);

	// Get options from "rc" file and environment variables.
	// (Options specified in environment variables override those
	// specified in the "rc" file.)
	if (_get_rc_opts() || _get_env_opts()) {
		return (-1);
	}

	// Since we might change working directory later, save current one.
	_orig_cwd = new char[PATH_MAX + 1];
	ASSERT(_orig_cwd != NULL);
	if (getcwd(_orig_cwd, (size_t)(PATH_MAX + 1)) == NULL) {
		perror("unode_config::initialize(): getcwd");
		return (-1);
	}

	// Determine path of cluster directory.
	_cluster_dir = new char[PATH_MAX + 1];
	ASSERT(_cluster_dir != NULL);
	if (_unode_dir[0] == '/') {
		(void) sprintf(_cluster_dir, "%s/%s", _unode_dir, clust_name);
	} else {
		char	remainder[PATH_MAX + 1];

		// Temporarily create the unode dir so we can get full path.
		if (_mkdirp(_unode_dir)) {
			return (-1);
		}

		// Convert to full pathname since we might change cwd later on.
		if (realpath(_unode_dir, _cluster_dir) == NULL) {
			perror("unode_config::initialize(): realpath");
#ifdef linux
			(void) rmdir(_unode_dir);
#else
			(void) rmdirp(_unode_dir, remainder);
#endif
			return (-1);
		}

		(void) sprintf(_cluster_dir + strlen(_cluster_dir), "/%s",
			clust_name);
#ifndef linux
		(void) rmdirp(_unode_dir, remainder);
#endif
	}

	// Determine path of node directory.
	_node_dir = new char[PATH_MAX + 1];
	ASSERT(_node_dir != NULL);
	node_path(_node_dir, _node_number, "");

	// Determine path of ccr directory (under node directory).
	_ccr_dir = new char[PATH_MAX + 1];
	ASSERT(_ccr_dir != NULL);
	(void) sprintf(_ccr_dir, "%s/ccr", _node_dir);

	// Determine path of clpl directory (under node directory).
	_clpl_dir = new char[PATH_MAX + 1];
	ASSERT(_clpl_dir != NULL);
	(void) sprintf(_clpl_dir, "%s/clpl", _node_dir);

	// Determine path of clpl directory (under node directory).
	_vp_dir = new char[PATH_MAX + 1];
	ASSERT(_vp_dir != NULL);
	(void) sprintf(_vp_dir, "%s/vp", _node_dir);

	return (0);
}

//
// configures current node's directories and files.
// Returns 0 if successful.
//
int
unode_config::configure()
{
	char	path[PATH_MAX + 1];
	FILE	*fp;
	int	i;

	//
	// Verify list of transports against list of supported transports.
	//
	if (num_transports() == 0) {
		os::printf("ERROR: No transports specified\n");
		return (-1);
	}
	for (i = 0; i < num_transports(); ++i) {
		const char	*trans = transports()[i];
		int		j;

		for (j = 0; supported_transports[j] != NULL; ++j) {
			if (strcmp(supported_transports[j], trans) == 0)
				break;
		}
		if (supported_transports[j] == NULL) {
			os::printf("ERROR: Unsupported transport: %s\n", trans);
			return (-1);
		}
	}

	//
	// Cause receipt of SIGABRT and SIGSEGV to pause process.
	//
	if (hang_on_signal()) {
		if (is_verbose()) {
			os::printf("Catching SIGABRT, SIGSEGV to pause ...\n");
		}

		if (signal(SIGABRT, unode_spin_loop) == SIG_ERR ||
		    signal(SIGSEGV, unode_spin_loop) == SIG_ERR) {
			perror("Can't set signal handler");
			return (-1);
		}
	}

	//
	// Set process limits acceptable for debugging.
	//
	_set_process_limits();

	//
	// Create cluster, node, ccr and clpl directories.
	//
	if (_mkdirp(cluster_dir()) || _mkdirp(node_dir()) ||
	    _mkdirp(ccr_dir()) || _mkdirp(clpl_dir())) {
		return (-1);
	}

	//
	// Create file and record PID of current node.
	//
	_pid_file = new char[PATH_MAX + 1];
	ASSERT(_pid_file != NULL);
	(void) sprintf(_pid_file, "%s/pid", node_dir());
	if ((fp = fopen(_pid_file, "w")) == NULL) {
		perror(_pid_file);
		return (-1);
	}
	(void) fprintf(fp, "%d\n", getpid());
	(void) fclose(fp);

	//
	// Record boot time timestamp file, if user wants it.
	//
	if (boot_time_stamp()) {
		(void) sprintf(path, "%s/timestamp", node_dir());
		if ((fp = fopen(path, "a")) == NULL) {
			perror(path);
			return (-1);
		}
		(void) fprintf(fp, "%lld\n", os::gethrtime());
		(void) fclose(fp);
	}

	//
	// Save names of debug buffers user wants to save to files in a hash
	// table so each debug buffer can determine whether to save its
	// contents to file or not (by calling lookup_dbg_buf()).
	//
	if (save_dbg_buf() != NULL) {
		const char	separators[] = " \t";
		char		*buflist, *bufname, *last;

		// Copy list since we're going to muck around with it.
		buflist = os::strdup(save_dbg_buf());
		ASSERT(buflist != NULL);

		for (bufname = strtok_r(buflist, separators, &last);
		    bufname != NULL;
		    bufname = strtok_r(NULL, separators, &last)) {
			// In case user specifies duplicate names.
			if (_dbg_bufname_hash.lookup(bufname)) {
				continue;
			}
			_dbg_bufname_hash.add(true, bufname);
		}

		delete [] buflist;
	}
	_dbg_bufname_hash_initialized = true;			// set flag

	//
	// Create "directory" ccr file.
	//
	(void) sprintf(path, "%s/directory", ccr_dir());
	if (access(path, F_OK) < 0) {
		// Create the file
		if ((fp = fopen(path, "w")) == NULL) {
			perror(path);
			return (-1);
		}

		//
		// XXX add table names needed for initialization
		// eg. (void) fprintf(fp, "infrastructure\n");
		// run initialize_ccr_table for each of these
		//
		(void) fprintf(fp, "infrastructure\n");
		(void) fclose(fp);

		if (ccrlib::initialize_ccr_table(path, "0") != 0) {
			perror("initialize_ccr_table");
			return (-1);
		}
	}

	if (access(_vp_dir, F_OK) < 0) {
		// There is no customized versioned protocol directory, so
		// switch to the one in the distribution

		if (find_run_directory(unode_args[0], _vp_dir,
		    getenv("PATH")) == -1) {
			perror("find_run_directory");
			return (-1);
		}
		strcat(_vp_dir, "/../lib/vp");
		if (access(_vp_dir, F_OK) < 0) {
			perror("No default vp database");
			return (-1);
		}
	}

	//
	// Create "epoch" ccr file.
	//
	(void) sprintf(path, "%s/epoch", ccr_dir());
	if (access(path, F_OK) < 0) {
		// Create the file
		if ((fp = fopen(path, "w")) == NULL) {
			perror(path);
			return (-1);
		}
		(void) fprintf(fp, "ccr_epoch\t0\n");
		(void) fclose(fp);
	}

	return (0);
}

//
// Builds a path to a node's directory, and appends whatever string you want
// to that path.  (This way we can build paths into the node's
// directory structure).  The arguments are like printf variable
// length arguments.  E.g.:
//
//	node_path(pathbuf, 1, "/root/usr/cluster/bin/%s", "foo");
//
// would place something like in 'pathbuf':
//
//	"<cluster_dir>/root/usr/cluster/bin/foo"
//
// The resulting path is returned in the supplied buffer.
//
void
unode_config::node_path(char *pathbuf, nodeid_t nid, const char *format, ...)
{
	va_list	ap;

	(void) sprintf(pathbuf, "%s/%s%d",
		cluster_dir(), UNODE_NODE_DIR_PREFIX, nid);

	va_start(ap, format);	//lint !e40 see comment in src/libkos/os_misc.cc
	(void) vsprintf(pathbuf + strlen(pathbuf), format, ap);
	va_end(ap);
}


//
// Look up a debug buffer name in the hash table _dbg_bufname_hash.
// Each debug buffer can use this to determine whether its contents
// should be saved to a file.
// Returns:
//	UNINITIALIZED	-- hash table is not initialized yet.  Caller
//			   should check back later.
//	FOUND		-- the name is found in the hash table.
//	NOT_FOUND	-- the name is not found in the hash table.
//
unode_config::lookup_t
unode_config::lookup_dbg_buf(const char *name, bool print_by_default)
{
	if (! _dbg_bufname_hash_initialized) {
		return (UNINITIALIZED);
	}

	// If the hash table is empty (default) or the name "all" exists,
	// then all buffers should be saved.
	if (_dbg_bufname_hash.empty() || _dbg_bufname_hash.lookup("all")) {
		if (print_by_default)
			return (FOUND);
	}

	// If the name "none" exists, then no buffer should be saved.
	if (_dbg_bufname_hash.lookup("none")) {
		return (NOT_FOUND);
	}

	// Otherwise, check the given name.
	if (_dbg_bufname_hash.lookup(name)) {
		return (FOUND);
	}

	return (NOT_FOUND);
}


//
// Load initial libraries and/or functions, if any.
// Returns 0 if successful.
//
int
unode_config::load_init()
{
	// Load libraries/functions for every node in the cluster first, then
	// those for this specific node.
	if (_do_load_init(cluster_init()) || _do_load_init(node_init())) {
		return (-1);
	}
	return (0);
}


//
// Does the actual loading of initial libraries/functions.
// Returns 0 if successful.
//
int
unode_config::_do_load_init(const char *str)
{
	const char	loadline_sep[] = ";";
	const char	part_sep[] = " \t";
	char		*initstr, *loadline, *libname;
	char		*loadline_last;

	if (str == NULL || str[0] == '\0') {
		return (0);			// nothing to load
	}

	// Make a copy since we're going to mangle it with strtok_r().
	initstr = os::strdup(str);
	ASSERT(initstr != NULL);

	// For each library and/or function to load (separated by ';').
	for (loadline = strtok_r(initstr, loadline_sep, &loadline_last);
	    loadline != NULL;
	    loadline = strtok_r(NULL, loadline_sep, &loadline_last)) {
		char		*fargv[FG_NUMARGS_MAX];
		int		fargc;
		char		*part_last;
		int		func_res = 0;
		fakegate_result	load_res;
		int		i;

		// Get the library name part.
		libname = strtok_r(loadline, part_sep, &part_last);
		if (libname == NULL) {
			continue;
		}

		// Get the function to call and its arguments.
		for (fargc = 0; fargc < FG_NUMARGS_MAX; ++fargc) {
			fargv[fargc] = strtok_r(NULL, part_sep, &part_last);
			if (fargv[fargc] == NULL) {
				break;
			}
		}
		if (fargc >= FG_NUMARGS_MAX) {
			os::printf("ERROR: Can't load %s %s: %s\n",
				libname, fargv[0], FG_ERRSTRING(FG_ARGMAX));
			delete [] initstr;
			return (-1);
		}

		if (is_verbose()) {
			os::printf("Loading %s", initstr);
			for (i = 0; i < fargc; ++i) {
				os::printf(" %s", fargv[i]);
			}
			os::printf("\n");
		}

		load_res = fakegate_load(libname, fargc, fargv, func_res);
		if (load_res) {
			os::printf("ERROR: Can't load %s %s: %s\n",
				libname, fargv[0] != NULL ? fargv[0] : "",
				FG_ERRSTRING(load_res));
			delete [] initstr;
			return (-1);
		}

		if (is_verbose() && fargv[0] != NULL) {
			os::printf("The test %s function %s returned %d\n",
				libname, fargv[0], func_res);
		}
	}

	delete [] initstr;
	return (0);
}


//
// Get options from an "rc" file.
// Returns 0 if successful.
//
// Note: if you add/rename/delete an option, also do it in _get_env_opts().
//
int
unode_config::_get_rc_opts()
{
	const char	pattern[] = "^[ \t]*(UNODE_[^ \t=]+)[ \t]*=[ \t]*"
				    "(.*[^ \t\n])";
	const size_t	nmatch = 3;	// num of parenthesized expressions + 1
	regmatch_t	regmatch[nmatch];
	regex_t		regexpr;
	FILE		*filep = NULL;
	char		line[256];
	char		node_init_varname[64];
	int		ret;
	char		*endptr;

	// Find and open rc file.
	if (_get_rc_file(filep)) {
		return (-1);				// error occured
	}
	if (filep == NULL) {
		if (is_verbose()) {
			os::printf("No rc file found. Ignored\n");
		}
		return (0);				// no rc file
	}

	// Compile regular expression pattern.
	ret = regcomp(&regexpr, pattern, REG_EXTENDED);
	if (ret) {
		char	errbuf[128];

		(void) regerror(ret, &regexpr, errbuf, sizeof (errbuf));
		os::printf("ERROR: Can't compile regular expression: %s\n",
			errbuf);

		(void) fclose(filep);
		return (-1);
	}
	ASSERT(regexpr.re_nsub == (nmatch - 1));

	(void) sprintf(node_init_varname, "UNODE_INIT_%d", _node_number);

	while (fgets(line, (int)sizeof (line), filep) != NULL) {
		if (regexec(&regexpr, line, nmatch, regmatch, 0)) {
			continue;		// line didn't match pattern
		}

		char	*variable = line + regmatch[1].rm_so;
		char	*value = line + regmatch[2].rm_so;
		size_t	variable_len = (size_t)
				    (regmatch[1].rm_eo - regmatch[1].rm_so);
		size_t	value_len = (size_t)
				    (regmatch[2].rm_eo - regmatch[2].rm_so);

		// We need to do this so we can use strcmp() instead of
		// strncmp() below to prevent partial matches.  And it's safe
		// to do this since there is at least an intervening '='
		// between variables and values (see pattern above).
		variable[variable_len] = '\0';

		value[value_len] = '\0';

		//
		// UNODE_HANG_ONSIG: hang on SIGSEGV/SIGABRT to attach debugger.
		//
		if (strcmp(variable, "UNODE_HANG_ONSIG") == 0) {
			_hang_on_signal = (strcasecmp(value, "true") == 0 ||
						strcasecmp(value, "yes") == 0);
		}

		//
		// UNODE_DIRECT_CONNECT_CABLES: do not use transport
		// hubs/switches and direct connect the nodes
		//
		else if (strcmp(variable, "UNODE_DIRECT_CONNECT_CABLES") == 0) {
			_direct_connect = (strcasecmp(value, "true") == 0 ||
						strcasecmp(value, "yes") == 0);
		}

		//
		// UNODE_BOOT_TIME_STAMP: if set, a timestamp will be added
		//	at boot time to the boot timestap file.
		//
		else if (strcmp(variable, "UNODE_BOOT_TIME_STAMP") == 0) {
			_boot_time_stamp = (strcasecmp(value, "true") == 0 ||
						strcasecmp(value, "yes") == 0);
		}

		//
		// UNODE_DIR: base directory where to save files.
		//
		else if (strcmp(variable, "UNODE_DIR") == 0) {
			delete [] _unode_dir;
			_unode_dir = os::strdup(value);
			ASSERT(_unode_dir != NULL);
		}

		//
		// UNODE_SAVE_DBG_BUF: space-separated list of debug buffers
		//	to save to files.
		//
		else if (strcmp(variable, "UNODE_SAVE_DBG_BUF") == 0) {
			delete [] _save_dbg_buf;
			_save_dbg_buf = os::strdup(value);
			ASSERT(_save_dbg_buf != NULL);
		}

		//
		// UNODE_TRANSPORT: comma-/space-separated transports to use.
		//
		else if (strcmp(variable, "UNODE_TRANSPORT") == 0) {
			_parse_transport_list(value);
		}

		//
		// UNODE_ADAPTERS: specifies list of adapters to use
		//
		else if (strcmp(variable, "UNODE_ADAPTERS") == 0) {
			_parse_adapter_list(value);
		}

		//
		// UNODE_NUM_ADAPTERS: specifies number of adapter per node.
		//
		else if (strcmp(variable, "UNODE_NUM_ADAPTERS") == 0) {
			errno = 0;
			_num_adapters = (int)strtol(value, &endptr, 0);
			if (errno || *endptr != '\0' || (_num_adapters <= 0)) {
				os::printf("Invalid number of adapters : %s \n",
					value);
				_num_adapters = default_num_adapters;
			}
		}

		//
		// UNODE_INIT: semicolon-separated list of libraries and/or
		//	functions to be loaded at start of EVERY node in
		//	the cluster.
		//
		else if (strcmp(variable, "UNODE_INIT") == 0) {
			delete [] _cluster_init;
			_cluster_init = os::strdup(value);
			ASSERT(_cluster_init != NULL);
		}

		//
		// UNODE_INIT_<nodeid>: semicolon-separated list of libraries
		//	and/or functions to be loaded at start of THIS node.
		//
		else if (strcmp(variable, node_init_varname) == 0) {
			delete [] _node_init;
			_node_init = os::strdup(value);
			ASSERT(_node_init != NULL);
		}
	}

	regfree(&regexpr);
	(void) fclose(filep);
	return (0);
}


//
// Get options from environment variables.
// Returns 0 if successful.
//
// Note: if you add/rename/delete an option, also do it in _get_rc_opts().
//
int
unode_config::_get_env_opts()
{
	char	*envp;
	char	*endptr;

	//
	// $UNODE_HANG_ONSIG: hang on SIGSEGV/SIGABRT to attach debugger.
	//
	if ((envp = getenv("UNODE_HANG_ONSIG")) != NULL) {
		_hang_on_signal = (strcasecmp(envp, "true") == 0 ||
					strcasecmp(envp, "yes") == 0);
	}

	//
	// $UNODE_DIRECT_CONNECT_CABLES: do not use transport
	// hubs/switches and direct connect the nodes
	//
	if ((envp = getenv("UNODE_DIRECT_CONNECT_CABLES")) != NULL) {
		_direct_connect = (strcasecmp(envp, "true") == 0 ||
					strcasecmp(envp, "yes") == 0);
	}

	//
	// $UNODE_BOOT_TIME_STAMP: if set, a timestamp will be added at boot
	//	time to the boot timestap file.
	//
	if ((envp = getenv("UNODE_BOOT_TIME_STAMP")) != NULL) {
		_boot_time_stamp = (strcasecmp(envp, "true") == 0 ||
					strcasecmp(envp, "yes") == 0);
	}

	//
	// $UNODE_DIR: base directory where to save files.
	//
	if ((envp = getenv("UNODE_DIR")) != NULL) {
		delete [] _unode_dir;
		_unode_dir = os::strdup(envp);
		ASSERT(_unode_dir != NULL);
	}

	//
	// $UNODE_SAVE_DBG_BUF: space-separated list of debug buffers to
	//	save to files.
	//
	if ((envp = getenv("UNODE_SAVE_DBG_BUF")) != NULL) {
		delete [] _save_dbg_buf;
		_save_dbg_buf = os::strdup(envp);
		ASSERT(_save_dbg_buf != NULL);
	}

	//
	// $UNODE_TRANSPORT: comma-/space-separated transports to use.
	//
	if ((envp = getenv("UNODE_TRANSPORT")) != NULL) {
		_parse_transport_list(envp);
	}

	//
	// $UNODE_ADAPTERS: comma-/space-separated adapters to use.
	//
	if ((envp = getenv("UNODE_ADAPTERS")) != NULL) {
		_parse_adapter_list(envp);
	}

	//
	// $UNODE_NUM_ADAPTERS: specifies number of adapter per node.
	//
	if ((envp = getenv("UNODE_NUM_ADAPTERS")) != NULL) {
		errno = 0;
		_num_adapters = (int)strtol(envp, &endptr, 0);
		if (errno || *endptr != '\0' || (_num_adapters <= 0)) {
			os::printf("Invalid number of adapters : %s \n", envp);
			_num_adapters = default_num_adapters;
		}
	}

	//
	// $UNODE_INIT: semicolon-separated list of libraries and/or functions
	//	to be loaded at the start of EVERY node in the cluster.
	//
	if ((envp = getenv("UNODE_INIT")) != NULL) {
		delete [] _cluster_init;
		_cluster_init = os::strdup(envp);
		ASSERT(_cluster_init != NULL);
	}

	//
	// $UNODE_INIT_<nodeid>: semicolon-separated list of libraries and/or
	//	functions to be loaded at the start of THIS node.
	//
	char	node_init_env[64];
	(void) sprintf(node_init_env, "UNODE_INIT_%d", _node_number);
	if ((envp = getenv(node_init_env)) != NULL) {
		delete [] _node_init;
		_node_init = os::strdup(envp);
		ASSERT(_node_init != NULL);
	}

	return (0);
}


//
// Find and open an rc file to use.  The resulting file pointer is
// returned in the parameter (set to NULL if no file is found).
// Returns 0 if successful, non-zero if an error occurs.
//
// Algorithm for determining which rc file to use:
//
//	1. If a file named "unoderc" exists in the current directory,
//	   then use it.  Else, go to step 2.
//	2. If environment variable $UNODE_RC exists, then go to step 3.
//	   Else go to step 4.
//	3. If the file specified by $UNODE_RC exists, then use it.
//	   Else return.
//	4. If a file named ".unoderc" (notice the dot) exists in the
//	   user's home directory exists, then use it.  Else return.
//
int
unode_config::_get_rc_file(FILE *&filep)
{
	char	*envp;

	filep = NULL;

	// Try ./unoderc.
	filep = fopen("unoderc", "r");
	if (filep != NULL) {
		if (is_verbose()) {
			os::printf("Using rc file: unoderc\n");
		}
		return (0);
	} else if (errno != ENOENT) {
		perror("./unoderc");
		return (-1);
	}

	// Try $UNODE_RC.
	if ((envp = getenv("UNODE_RC")) != NULL) {
		filep = fopen(envp, "r");
		if (filep != NULL) {
			if (is_verbose()) {
				os::printf("Using rc file: %s\n", envp);
			}
			return (0);
		} else if (errno != ENOENT) {
			perror(envp);
			return (-1);
		}

		if (is_verbose()) {
			os::printf("Rc file %s not found. Ignored\n", envp);
		}
		return (0);
	}

	char	*homedir;
	char	pwbuf[PATH_MAX + 1];
	char	dotrc[PATH_MAX + 1];

	// Try ~/.unoderc.
	if ((envp = getenv("HOME")) != NULL) {
		homedir = envp;
	} else {
		struct passwd	pw, *ret;

		// Figure out user's home directory from passwd info.
		if ((envp = getenv("USER")) != NULL) {
			// Get passwd info based on the user name.
			errno = 0;
			ret = os::getpwnam(envp, &pw, pwbuf,
			    (int)sizeof (pwbuf));
			if (ret == NULL) {
				if (errno) {
					perror("getpwnam_r");
					return (-1);
				}
				os::warning("No passwd info for '%s'", envp);
				return (0);	// assume there is no rc file
			}
		} else {
			// Get passwd info based on caller's UID.
			errno = 0;
			ret = os::getpwuid(getuid(), &pw, pwbuf,
			    (int)sizeof (pwbuf));
			if (ret == NULL) {
				if (errno) {
					perror("getpwuid_r");
					return (-1);
				}
				os::warning("No passwd info for uid %d",
					getuid());
				return (0);	// assume there is no rc file
			}
		}

		homedir = ret->pw_dir;
	}

	(void) sprintf(dotrc, "%s/.unoderc", homedir);
	filep = fopen(dotrc, "r");
	if (filep != NULL) {
		if (is_verbose()) {
			os::printf("Using rc file: %s\n", dotrc);
		}
		return (0);
	} else if (errno != ENOENT) {
		perror(dotrc);
		return (-1);
	}

	return (0);
}


//
// Parse comma-/space-separated list of transports and copy them
// into _transports[].
//
void
unode_config::_parse_transport_list(const char *list)
{
	const char	separators[] = ", \t";
	char		*tr, *tmplist, *last;
	int		i;

	// Make a copy since we're going to mangle it with strtok_r() twice.
	tmplist = os::strdup(list);
	ASSERT(tmplist != NULL);

	// Delete the old list of transports first.
	for (i = 0; i < _num_transports; ++i) {
		delete [] _transports[i];
	}
	delete [] _transports;
	_num_transports = 0;

	// Count how many transports are specified.
	for (tr = strtok_r(tmplist, separators, &last); tr != NULL;
	    tr = strtok_r(NULL, separators, &last)) {
		_num_transports++;
	}

	// Reallocate _transports[].
	_transports = new char *[(uint_t)(_num_transports + 1)];
	ASSERT(_transports != NULL);

	// Go through the list again and copy them into _transports[].
	(void) strcpy(tmplist, list);
	tr = strtok_r(tmplist, separators, &last);
	for (i = 0; i < _num_transports; ++i) {
		_transports[i] = os::strdup(tr);
		ASSERT(_transports[i] != NULL);
		tr = strtok_r(NULL, separators, &last);
	}
	_transports[i] = NULL;			// NULL terminate

	delete [] tmplist;
}

// Return specified adapter or NULL
const char *
unode_config::adapter(int nodeid, int adp)
{
	if (_adapters != NULL && _adapters[nodeid] != NULL &&
	    adp < _num_adapters) {
		return (_adapters[nodeid][adp]);
	} else {
		return (NULL);
	}
}

// Return specified adapter or NULL
int
unode_config::adapter_instance(int nodeid, int adp)
{
	if (_adapters != NULL && _adapter_instances[nodeid] != NULL &&
	    adp < _num_adapters) {
		return (_adapter_instances[nodeid][adp]);
	} else {
		return (NULL);
	}
}

//
// Parse comma-/space-separated list of adapters and copy them
// into _adapters[].
// Node information must be separated by semicolons.
// e.g. hme0,hme1;hme0,hme1;qfe0,qfe1
//
void
unode_config::_parse_adapter_list(const char *list_in)
{
	const char	separators[] = ", \t";
	const char	node_separators[] = ";";
	char		*tr, *last;
	int		i;
	char		*list = (char *)list_in;


	_adapters = new char **[NODEID_MAX+1];
	_adapter_instances = new int *[NODEID_MAX+1];
	ASSERT(_adapters != NULL);
	ASSERT(_adapter_instances != NULL);

	for (i = 0; i <= NODEID_MAX; i++) {
	    _adapters[i] = NULL;
	    _adapter_instances[i] = NULL;
	}

	int nodecnt = 1;

	// Step through each node's info
	for (tr = strtok_r(list, node_separators, &last); tr != NULL;
	    tr = strtok_r(NULL, node_separators, &last)) {

		char 	*tmplist, *tok, *adplist;
		// Make a copy since we're going to mangle it with strtok_r()
		tmplist = os::strdup(tr);
		ASSERT(tmplist != NULL);

		int adpcnt = 0;

		// Count number of adapters
		for (tok = strtok_r(tmplist, separators, &adplist); tok != NULL;
		    tok = strtok_r(NULL, separators, &adplist)) {
			adpcnt++;
		}

		if (nodecnt == 1) {
		    _num_adapters = adpcnt;
		} else {
		    if (_num_adapters != adpcnt) {
			fprintf(stderr, "Number of adapters mismatch\n");
			ASSERT(0);
		    }
		}

		delete [] tmplist;

		_adapters[nodecnt] = new char *[_num_adapters];
		_adapter_instances[nodecnt] = new int [_num_adapters];
		for (i = 0; i < _num_adapters; i++) {
		    _adapters[nodecnt][i] = NULL;
		    _adapter_instances[nodecnt][i] = NULL;
		}

		tmplist = os::strdup(tr);
		ASSERT(tmplist != NULL);

		adpcnt = 0;

		// Record adapters
		for (tok = strtok_r(tmplist, separators, &adplist); tok != NULL;
		    tok = strtok_r(NULL, separators, &adplist)) {
			int len = strlen(tok);
			ASSERT(len > 1);
			ASSERT(tok[len-1] >= '0' && tok[len-1] <= '9');
			// instance number is last digit of adapter
			_adapter_instances[nodecnt][adpcnt] = atoi(tok+len-1);
			_adapters[nodecnt][adpcnt] = os::strdup(tok);
			// Remove instance number
			_adapters[nodecnt][adpcnt][len-1] = '\0';

			adpcnt++;
		}

		delete [] tmplist;
		nodecnt++;
	}
}

//
// Set process limits to acceptable levels for debugging.
//
void
unode_config::_set_process_limits()
{
	struct rlimit	rlp;
	int		err = 0;

	// Set core file size to as high as possible.
	if (geteuid() == 0) {
		// This process is run as root.  Max it all the way.
		rlp.rlim_cur = (rlim_t)RLIM_INFINITY;
		rlp.rlim_max = (rlim_t)RLIM_INFINITY;
	} else {
		// Get the current limits.
		if (getrlimit(RLIMIT_CORE, &rlp) < 0) {
			err = errno;
			os::warning("getrlimit(RLIMIT_CORE): %s",
				strerror(errno));
		}

		// Set soft limit to the hard limit.
		rlp.rlim_cur = rlp.rlim_max;
	}

	if (err == 0 && setrlimit(RLIMIT_CORE, &rlp) < 0) {
		os::warning("setrlimit(RLIMIT_CORE): %s", strerror(errno));
	}
}


//
// Given a directory path, create it and its parent directories.
// Returns 0 if successful.
//
int
unode_config::_mkdirp(const char *path)
{
#ifdef linux
	if (mkdir(path, S_IRWXU) < 0 && errno != EEXIST) {
#else
	if (mkdirp(path, S_IRWXU) < 0 && errno != EEXIST) {
#endif
		os::printf("ERROR: mkdirp: %s: %s\n", path, strerror(errno));
		return (-1);
	}
	return (0);
}

nodeid_t
unode_node_number()
{
	return (unode_config::the().node_number());
}

const char *
unode_get_clpldir()
{
	return (unode_config::the().clpl_dir());
}

const char *
unode_get_vpdir()
{
	return (unode_config::the().vp_dir());
}

const char *
unode_get_clustername()
{
	return (unode_config::the().cluster_name());
}
