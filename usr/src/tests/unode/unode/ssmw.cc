/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)ssmw.cc	1.8	08/05/20 SMI"

// ssmw is a unode-only module that is a wrapper for the SSM
// calls defined in ssm.h.  All the non-static functions in
// this file are unode entry points and correspond to a SSM
// routine.
//
// The form of the entry points is similar.  Each checks the
// number of arguments and prints a usage statement if an
// incorrect number is given.  Then the arguments are converted
// to the form needed by the SSM function, the function is called,
// and a message is printed if the SSM function fails.  -1 is
// returned on error and 0 on success.  1 can also be a successful
// return from an entry point that returns a boolean.

// Only meant to be built for unode

#if defined(_KERNEL) || !defined(_KERNEL_ORB)
#error unsupported build variant
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>

#include <sys/ssm.h>
#include <unode/output.h>

// unode initialization function, called only once when the module
// is first loaded.  The module stays resident and subsequent unode_load
// runs don't load the module or call unode_init.

int
unode_init()
{
	(void) printf("ssmw loaded\n");

	return (0);
}

// start of utility functions

// convert a string to the appropriate scha_ssm_policy_t enum

static int
pol_str_to_policy(const char *pol_str, scha_ssm_policy_t *polp)
{
	unsigned int i;
	struct str_pol {
		const char *str;
		scha_ssm_policy_t pol;
	} str_pols[] = {
		{ "LB_WEIGHTED",	scha_ssm_lb_weighted },
		{ "LB_STICKY",		scha_ssm_lb_st },
		{ "LB_STICKY_WILD",	scha_ssm_lb_sw },
		{ "LB_PERF",		scha_ssm_lb_perf },
		{ "LB_USER",		scha_ssm_lb_user },
	};

	for (i = 0; i < (sizeof (str_pols) / sizeof (str_pols[0])); i++)
		if (strcasecmp(str_pols[i].str, pol_str) == 0) {
			*polp = str_pols[i].pol;
			return (1);
		}

	return (0);
}

// Convert a string to the appropriate protocol.  Only TCP and UDP
// are recognized by scalable services, but we include two more
// for testing purposes (and don't bother including all).

static int
prot_str_to_prot(const char *prot_str, uint8_t *protp)
{
	unsigned int i;
	struct str_prot {
		const char *str;
		uint8_t prot;
	} str_prots[] = {
		{ "IP",		IPPROTO_IP },
		{ "TCP",	IPPROTO_TCP },
		{ "UDP",	IPPROTO_UDP },
		{ "ICMP",	IPPROTO_ICMP },
		{ "HELLO",	IPPROTO_HELLO },
	};

	for (i = 0; i < (sizeof (str_prots) / sizeof (str_prots[0])); i++)
		if (strcasecmp(str_prots[i].str, prot_str) == 0) {
			*protp = str_prots[i].prot;
			return (1);
		}

	return (0);
}

// From here to the end of the file are functions that can be
// given on the command line to unode_load, for example:
//
//	unode_load foo 1 ssmw create_scal_srv_grp dss-res lb_weighted
//
// In general when the functions encounter an error they print
// a message and return -1.  The printf will go the window where
// the unode process is running, unless -o is used to send the
// output to a file.

int
create_scal_srv_grp(int argc, char **argv)
{
	output out(&argc, argv);
	const char *res, *pol_str;
	ssm_group_t g_name;
	scha_ssm_policy_t policy;
	scha_ssm_exceptions ex;

	argc--; argv++;

	if (argc != 2) {
		(void) fprintf(out, "usage: create_scal_srv_grp "
		    "service-name lb-policy\n");
		return (-1);
	}

	res = argv[0];
	pol_str = argv[1];

	(void) snprintf(g_name.resource, sizeof (g_name.resource), "%s", res);

	if (!pol_str_to_policy(pol_str, &policy)) {
		(void) fprintf(out, "%s: bad policy\n", pol_str);
		return (-1);
	}

	ex = ssm_create_scal_srv_grp(&g_name, policy);

	if (ex != SCHA_SSM_SUCCESS) {
		(void) fprintf(out, "ssm_create_scal_srv_grp(\"%s\", \"%s\") "
		    "failed: %s\n", res, pol_str, ssm_err_code_to_mesg(ex));
		return (-1);
	}

	return (0);
}

int
add_scal_service(int argc, char **argv)
{
	output out(&argc, argv);
	const char *res, *ips, *ports, *prots;
	ssm_group_t g_name;
	ssm_service_t ssm_svc;
	scha_ssm_exceptions ex;

	argc--; argv++;

	if (argc != 4) {
		(void) fprintf(out, "usage: add_scal_service "
		    "service-name ip-addr port protocol\n");
		return (-1);
	}

	res = argv[0];
	ips = argv[1];
	ports = argv[2];
	prots = argv[3];

	(void) snprintf(g_name.resource, sizeof (g_name.resource), "%s", res);

	IN6_IPADDR_TO_V4MAPPED(inet_addr(ips), &ssm_svc.ipaddr);
	ssm_svc.port = (in_port_t)atoi(ports);
	if (!prot_str_to_prot(prots, &ssm_svc.protocol)) {
		(void) fprintf(out, "%s: bad protocol\n", prots);
		return (-1);
	}

	ex = ssm_add_scal_service(&g_name, &ssm_svc);

	if (ex != SCHA_SSM_SUCCESS) {
		(void) fprintf(out, "ssm_add_scal_service(\"%s\", %s, %s, %s) "
		    "failed: %s\n", res, ips, ports, prots,
		    ssm_err_code_to_mesg(ex));
		return (-1);
	}

	return (0);
}

int
del_scal_srv_grp(int argc, char **argv)
{
	output out(&argc, argv);
	const char *res;
	ssm_group_t g_name;
	scha_ssm_exceptions ex;

	argc--; argv++;

	if (argc != 1) {
		(void) fprintf(out, "usage: del_scal_service service-name\n");
		return (-1);
	}

	res = argv[0];

	(void) snprintf(g_name.resource, sizeof (g_name.resource), "%s", res);

	ex = ssm_del_scal_srv_grp(&g_name);

	if (ex != SCHA_SSM_SUCCESS) {
		(void) fprintf(out, "ssm_del_scal_srv_grp(\"%s\") "
		    "failed: %s\n", res, ssm_err_code_to_mesg(ex));
		return (-1);
	}

	return (0);
}

int
rem_scal_service(int argc, char **argv)
{
	output out(&argc, argv);
	const char *res, *ips, *ports, *prots;
	ssm_group_t g_name;
	ssm_service_t ssm_svc;
	scha_ssm_exceptions ex;

	argc--; argv++;

	if (argc != 4) {
		(void) fprintf(out, "usage: rem_scal_service "
		    "service-name ip-addr port protocol\n");
		return (-1);
	}

	res = argv[0];
	ips = argv[1];
	ports = argv[2];
	prots = argv[3];

	(void) snprintf(g_name.resource, sizeof (g_name.resource), "%s", res);

	IN6_IPADDR_TO_V4MAPPED(inet_addr(ips), &ssm_svc.ipaddr);
	ssm_svc.port = (in_port_t)atoi(ports);
	if (!prot_str_to_prot(prots, &ssm_svc.protocol)) {
		(void) fprintf(out, "%s: bad protocol\n", prots);
		return (-1);
	}

	ex = ssm_rem_scal_service(&g_name, &ssm_svc);

	if (ex != SCHA_SSM_SUCCESS) {
		(void) fprintf(out, "ssm_rem_scal_service(\"%s\", %s, %s, %s) "
		    "failed: %s\n", res, ips, ports, prots,
		    ssm_err_code_to_mesg(ex));
		return (-1);
	}

	return (0);
}

int
add_nodeid(int argc, char **argv)
{
	output out(&argc, argv);
	const char *res, *nodestr;
	ssm_group_t g_name;
	nodeid_t node;
	scha_ssm_exceptions ex;

	argc--; argv++;

	if (argc != 2) {
		(void) fprintf(out, "usage: add_nodeid "
		    "service-name nodeid\n");
		return (-1);
	}

	res = argv[0];
	nodestr = argv[1];

	(void) snprintf(g_name.resource, sizeof (g_name.resource), "%s", res);
	node = (nodeid_t)atoi(nodestr);

	ex = ssm_add_nodeid(&g_name, node);

	if (ex != SCHA_SSM_SUCCESS) {
		(void) fprintf(out, "ssm_add_nodeid(\"%s\", %s) failed: %s\n",
		    res, nodestr, ssm_err_code_to_mesg(ex));
		return (-1);
	}

	return (0);
}

int
remove_nodeid(int argc, char **argv)
{
	output out(&argc, argv);
	const char *res, *nodestr;
	ssm_group_t g_name;
	nodeid_t node;
	scha_ssm_exceptions ex;

	argc--; argv++;

	if (argc != 2) {
		(void) fprintf(out, "usage: remove_nodeid "
		    "service-name nodeid\n");
		return (-1);
	}

	res = argv[0];
	nodestr = argv[1];

	(void) snprintf(g_name.resource, sizeof (g_name.resource), "%s", res);
	node = (nodeid_t)atoi(nodestr);

	ex = ssm_remove_nodeid(&g_name, node);

	if (ex != SCHA_SSM_SUCCESS) {
		(void) fprintf(out, "ssm_remove_nodeid(\"%s\", %s) "
		    "failed: %s\n", res, nodestr, ssm_err_code_to_mesg(ex));
		return (-1);
	}

	return (0);
}

int
is_scalable_service_group(int argc, char **argv)
{
	output out(&argc, argv);
	ssm_group_t g_name;
	int result;
	scha_ssm_exceptions ex;

	argc--; argv++;

	if (argc != 1) {
		(void) fprintf(out, "usage: is_scalable_service_group "
		    "service-name\n");
		return (-1);
	}

	(void) snprintf(g_name.resource, sizeof (g_name.resource), "%s",
	    argv[0]);

	ex = ssm_is_scalable_service_group(&g_name, &result);

	if (ex != SCHA_SSM_SUCCESS) {
		(void) fprintf(out, "ssm_is_scalable_service_group(\"%s\") "
		    "failed: %s\n", argv[0], ssm_err_code_to_mesg(ex));
		return (-1);
	}

	return (result);
}

int
is_scalable_service(int argc, char **argv)
{
	output out(&argc, argv);
	const char *res, *ips, *ports, *prots;
	ssm_service_t ssm_svc;
	scha_ssm_exceptions ex;
	int result;

	argc--; argv++;

	if (argc != 4) {
		(void) fprintf(out, "usage: is_scalable_service "
		    "service-name ip-addr port protocol\n");
		return (-1);
	}

	res = argv[0];
	ips = argv[1];
	ports = argv[2];
	prots = argv[3];

	IN6_IPADDR_TO_V4MAPPED(inet_addr(ips), &ssm_svc.ipaddr);
	ssm_svc.port = (in_port_t)atoi(ports);
	if (!prot_str_to_prot(prots, &ssm_svc.protocol)) {
		(void) fprintf(out, "%s: bad protocol\n", prots);
		return (-1);
	}

	ex = ssm_is_scalable_service(&ssm_svc, &result);

	if (ex != SCHA_SSM_SUCCESS) {
		(void) fprintf(out, "ssm_is_scalable_service(\"%s\", %s, %s, "
		    "%s) failed: %s\n", res, ips, ports, prots,
		    ssm_err_code_to_mesg(ex));
		return (-1);
	}

	return (result);
}

int
set_primary_gifnode(int argc, char **argv)
{
	output out(&argc, argv);
	const char *ips, *ports, *prots, *nodestr;
	ssm_service_t ssm_svc;
	scha_ssm_exceptions ex;
	nodeid_t node;

	argc--; argv++;

	if (argc != 4) {
		(void) fprintf(out, "usage: set_primary_gifnode ip-addr port "
		    "protocol nodeid\n");
		return (-1);
	}

	ips = argv[0];
	ports = argv[1];
	prots = argv[2];
	nodestr = argv[3];

	IN6_IPADDR_TO_V4MAPPED(inet_addr(ips), &ssm_svc.ipaddr);
	ssm_svc.port = (in_port_t)atoi(ports);
	if (!prot_str_to_prot(prots, &ssm_svc.protocol)) {
		(void) fprintf(out, "%s: bad protocol\n", prots);
		return (-1);
	}
	node = (nodeid_t)atoi(nodestr);

	ex = ssm_set_primary_gifnode(&ssm_svc, node);

	if (ex != SCHA_SSM_SUCCESS) {
		(void) fprintf(out, "ssm_set_primary_gifnode(%s, %s, %s, %s) "
		    "failed: %s\n", ips, ports, prots, nodestr,
		    ssm_err_code_to_mesg(ex));
		return (-1);
	}

	return (0);
}

int
attach_gifnode(int argc, char **argv)
{
	output out(&argc, argv);
	const char *res, *ips, *ports, *prots, *nodestr;
	ssm_group_t g_name;
	ssm_service_t ssm_svc;
	scha_ssm_exceptions ex;
	nodeid_t node;

	argc--; argv++;

	if (argc != 5) {
		(void) fprintf(out, "usage: attach_gifnode service-name "
		    "ip-addr port protocol nodeid\n");
		return (-1);
	}

	res = argv[0];
	ips = argv[1];
	ports = argv[2];
	prots = argv[3];
	nodestr = argv[4];

	(void) snprintf(g_name.resource, sizeof (g_name.resource), "%s", res);

	IN6_IPADDR_TO_V4MAPPED(inet_addr(ips), &ssm_svc.ipaddr);
	ssm_svc.port = (in_port_t)atoi(ports);
	if (!prot_str_to_prot(prots, &ssm_svc.protocol)) {
		(void) fprintf(out, "%s: bad protocol\n", prots);
		return (-1);
	}
	node = (nodeid_t)atoi(nodestr);

	ex = ssm_attach_gifnode(&g_name, &ssm_svc, node);

	if (ex != SCHA_SSM_SUCCESS) {
		(void) fprintf(out, "attach_gifnode(\"%s\", %s, %s, %s, %s) "
		    "failed: %s\n", res, ips, ports, prots, nodestr,
		    ssm_err_code_to_mesg(ex));
		return (-1);
	}

	return (0);
}

int
detach_gifnode(int argc, char **argv)
{
	output out(&argc, argv);
	const char *res, *ips, *ports, *prots, *nodestr;
	ssm_group_t g_name;
	ssm_service_t ssm_svc;
	scha_ssm_exceptions ex;
	nodeid_t node;

	argc--; argv++;

	if (argc != 5) {
		(void) fprintf(out, "usage: detach_gifnode service-name "
		    "ip-addr port protocol nodeid\n");
		return (-1);
	}

	res = argv[0];
	ips = argv[1];
	ports = argv[2];
	prots = argv[3];
	nodestr = argv[4];

	(void) snprintf(g_name.resource, sizeof (g_name.resource), "%s", res);

	IN6_IPADDR_TO_V4MAPPED(inet_addr(ips), &ssm_svc.ipaddr);
	ssm_svc.port = (in_port_t)atoi(ports);
	if (!prot_str_to_prot(prots, &ssm_svc.protocol)) {
		(void) fprintf(out, "%s: bad protocol\n", prots);
		return (-1);
	}
	node = (nodeid_t)atoi(nodestr);

	ex = ssm_detach_gifnode(&g_name, &ssm_svc, node);

	if (ex != SCHA_SSM_SUCCESS) {
		(void) fprintf(out, "detach_gifnode(\"%s\", %s, %s, %s, %s) "
		    "failed: %s\n", res, ips, ports, prots, nodestr,
		    ssm_err_code_to_mesg(ex));
		return (-1);
	}

	return (0);
}
