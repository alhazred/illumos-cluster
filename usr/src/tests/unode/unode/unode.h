/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_UNODE_H
#define	_UNODE_H

#pragma ident	"@(#)unode.h	1.22	08/05/20 SMI"

#include <sys/types.h>
#include <sys/os.h>
#include <sys/clconf.h>

/* Variable to emulate a variable with the same name in the base kernel */
extern int	cluster_bootflags;

extern os::notify_t shutdown_notify;


/* Functions to emulate the kernel functions defined in <sys/clconf.h> */
void		clconf_init(void);
nodeid_t	clconf_get_nodeid(void);

int	fakeclock_start(void);
void	fakeclock_end(void);

/* Path manager cyclic emulation */
int	fakecyclic_start(void);
void	fakecyclic_end(void);

void	unode_shutdown(void);
void	unode_quit(void);

void	reboot_unode_clean(void);
void	reboot_unode(void);

int	unode_transport_modload(const char *trtype);
void	unode_spin_loop(int);

#endif /* _UNODE_H */
