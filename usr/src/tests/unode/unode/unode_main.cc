/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)unode_main.cc	1.63	08/05/20 SMI"

#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/os.h>
#include <new.h>

#include <unode/unode.h>
#include <unode/fakegate.h>
#include <unode/unode_cladm.h>
#include <sys/nodeset.h>
#include <orb/infrastructure/orb.h>

extern "C" char *strtok_r(char *, const char *, char **);

extern int create_infrastructure(nodeset &);
extern int create_clplfiles();

//
// Print usage line.
//
static void
print_usage(char *prog)
{
	os::printf(
	    "Usage: %s <cluster_name> <nid> <cluster_nids> [sleep_time]\n",
	    prog);

	os::printf("Where:\n");
	os::printf("  cluster_name is the name of the cluster\n");
	os::printf("  nid is nodeid for this node \n");
	os::printf("  cluster_nids is comma separated list of nodeid ranges\n");
	os::printf("  e.g. 1-5 or 3-6,9,32-64\n");
	os::printf("  sleep_time (optional) is # of secs to sleep on reboot\n");
}

//
// Convert a string to a nodeid
//
static nodeid_t
convert_str_to_nodeid(const char *arg)
{
	char	*endptr;
	nodeid_t x;

	errno = 0;
	x = (nodeid_t)strtoul(arg, &endptr, 0);
	if (errno || *endptr != '\0') {
		os::printf("Invalid node ID: %s \n", arg);
		return (NODEID_UNKNOWN);
	}
	if ((x < 1) || (x > NODEID_MAX)) {
		os::printf("Node ID (%u) must be >= 1 and <= %u\n",
			x, NODEID_MAX);
		return (NODEID_UNKNOWN);
	}
	return (x);
}

//
// We get a comma separated list of nodeid ranges that need to be converted to
// a nodeset. Returns an empty nodeset on error
//
static void
convert_nodelist_into_nodeset(const char *arg, nodeset &nodes)
{
	char *range_str, *lastr = NULL;
	char *node_str, *lastn;
	nodeid_t x, y;

	char *str = os::strdup(arg);	// take a copy as we write into this

	// Make input empty
	nodes.empty();

	// iterate through comma separated list
	for (range_str = strtok_r(str, ",", &lastr); range_str != NULL;
		range_str = strtok_r(NULL, ",", &lastr)) {

		lastn = NULL;
		// Convert "x", or "x-y" argument to nodeset
		node_str = strtok_r(range_str, "-", &lastn);
		x = convert_str_to_nodeid(node_str);
		if (lastn != NULL) {
			y = convert_str_to_nodeid(lastn);
		} else {
			// treat "x" same as "x-x"
			y = x;
		}
		if ((x == NODEID_UNKNOWN) || (y == NODEID_UNKNOWN)) {
			nodes.empty();
			break;
		}
		if (x > y) {
			os::printf("please specify node range in increasing "
			    "order, %u > %u\n", x, y);
			nodes.empty();
			break;
		}
		// Add the range x-y to the nodeset
		for (; x <= y; x++) {
			nodes.add_node(x);
		}
	}
	delete [] str;
}


int
main(int argc, char **argv)
{
	char		*cluster_name;
	nodeid_t	my_nodeid;
	int		i;

	// Close file descriptors other than stdin/stdout/stderr.
	// This is nescessary as unode simulates reboot by re-exec
	// of itself (see reboot_node()) and file descriptors are
	// not closed on exec.
	long max_file_descritors = sysconf(_SC_OPEN_MAX);
	if (max_file_descritors == -1) {
		return (1);
	}
	for (int file_descriptor = 3; file_descriptor <= max_file_descritors;
		file_descriptor++) {
		(void) close(file_descriptor);
	}

	// Save arguments.
	unode_args = argv;

	if (argc < 4) {
		print_usage(argv[0]);
		return (1);
	}

	if (*argv[1] == '\0') {
		os::printf("No cluster name specified\n");
		print_usage(argv[0]);
		return (1);
	}
	cluster_name = argv[1];

	// Convert nid argument to number.
	my_nodeid = convert_str_to_nodeid(argv[2]);
	if (my_nodeid == NODEID_UNKNOWN) {
		return (1);
	}

	nodeset	node_list;

	convert_nodelist_into_nodeset(argv[3], node_list);
	if (node_list.is_empty()) {
		os::printf("Invalid set of nodes \"%s\"\n", argv[3]);
		return (1);
	}

	// Check validity of node id.
	if (!node_list.contains(my_nodeid)) {
		os::printf("Node id (%u) is not in nodeset \"%s\" 0x%llx\n",
			my_nodeid, argv[3], node_list.bitmask());
		return (1);
	}

	//
	// Force the program to abort instead of generating an xalloc exception
	// in "new" when the program cannot allocate storage.
	// There is no handler for this exception.
	//
	(void) set_new_handler(&abort);

	unode_config	&config = unode_config::the();

	os::printf("Cluster name: %s, node ID: %u, nodeset (0x%llx) %s\n",
		cluster_name, my_nodeid, node_list.bitmask(), argv[3]);

	// Initialize unode configurations.
	if (config.initialize(cluster_name, my_nodeid, true)) {
		return (1);
	}

	// Print some information about the cluster.
	os::printf("Adapters per node: %u\n", config.num_adapters());
	os::printf("Transports:");
	for (i = 0; i < config.num_transports(); ++i) {
		os::printf("%s%s", i > 0 ? ", " : " ", config.transports()[i]);
	}
	os::printf("\n");

	// Configure unode info for this node.
	if (config.configure()) {
		return (1);
	}

	// Create CCR infrastructure file.
	if (create_infrastructure(node_list)) {
		return (1);
	}

	// Create clpl files.
	if (create_clplfiles()) {
		return (1);
	}

	// Change cwd to the node directory (such that if this node generates
	// core it won't clobber cores from other nodes).
	if (chdir(config.node_dir()) < 0) {
		os::printf("ERROR: Can't chdir to %s: %s\n",
			config.node_dir(), strerror(errno));
		return (1);
	}

	if (fakegate_start()) {
		return (1);
	}

	if (cladm_start()) {
		return (1);
	}

	// Sleep after booting if neccesary
	if (argc > 4) {
		size_t sleep_time;
		char *endptr;

		errno = 0;
		sleep_time = strtoul(argv[4], &endptr, 0);
		if (errno || *endptr != '\0') {
			os::printf("Invalid sleep time: %s \n", argv[4]);
			sleep_time = 0;
		}
		if (sleep_time > 0) {
			os::printf("*** Sleeping after reboot for %u secs\n",
			    sleep_time);
			os::usecsleep((os::usec_t)sleep_time * 1000000);
		}
	}

	if (fakeclock_start()) {
		fakegate_end();
		cladm_end();
		return (1);
	}

	// Path manager cyclic emulation
	if (fakecyclic_start()) {
		fakeclock_end();
		fakegate_end();
		cladm_end();
		return (1);
	}


	if (ORB::initialize()) {
		os::printf("ERROR: Can't initialize ORB\n");
		fakeclock_end();
		fakecyclic_end();
		fakegate_end();
		cladm_end();
		return (1);
	}

	// Load initial libraries and/or functions, if any.
	if (config.load_init()) {
		unode_shutdown();
		return (1);
	}

	// Signal fakegate that it can start accepting requests.
	startup_notify.signal();

	//
	// The ORB shutdown and exit from the process
	// happens by executing the following :
	// unode_load <clname> <ndid> orbtest quit
	//
	shutdown_notify.wait();

	unode_shutdown();

	return (0);
}
