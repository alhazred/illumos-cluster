/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _UNODE_CONFIG_IN_H
#define	_UNODE_CONFIG_IN_H

#pragma ident	"@(#)unode_config_in.h	1.17	08/05/20 SMI"

inline
unode_config &
unode_config::the()
{
	return (_the_unode_config);
}

inline nodeid_t
unode_config::node_number()
{
	ASSERT(_node_number != 0);
	return (_node_number);
}

inline const char *
unode_config::orig_cwd()
{
	return (_orig_cwd);
}

inline const char *
unode_config::cluster_name()
{
	return (_cluster_name);
}

inline const char *
unode_config::cluster_dir()
{
	return (_cluster_dir);
}

inline const char *
unode_config::node_dir()
{
	return (_node_dir);
}

inline const char *
unode_config::ccr_dir()
{
	return (_ccr_dir);
}

inline const char *
unode_config::clpl_dir()
{
	return (_clpl_dir);
}

inline const char *
unode_config::vp_dir()
{
	return (_vp_dir);
}

inline const char *
unode_config::pid_file()
{
	return (_pid_file);
}

inline const char **
unode_config::transports()
{
	return ((const char **)_transports);
}

inline int
unode_config::num_transports() const
{
	return (_num_transports);
}

inline int
unode_config::num_adapters() const
{
	return (_num_adapters);
}

inline const char *
unode_config::save_dbg_buf()
{
	return (_save_dbg_buf);
}

inline bool
unode_config::hang_on_signal()
{
	return (_hang_on_signal);
}

inline bool
unode_config::direct_connect()
{
	return (_direct_connect);
}

inline const char *
unode_config::node_init()
{
	return (_node_init);
}

inline const char *
unode_config::cluster_init()
{
	return (_cluster_init);
}

inline bool
unode_config::boot_time_stamp()
{
	return (_boot_time_stamp);
}

inline bool
unode_config::is_verbose()
{
	return (_verbose);
}

#endif	/* _UNODE_CONFIG_IN_H */
