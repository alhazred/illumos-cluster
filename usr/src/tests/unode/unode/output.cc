/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)output.cc	1.6	08/05/20 SMI"

#include <unode/output.h>
#include <string.h>

// Remove "-o filename" from args and set private file pointer.

output::output(int *argcp, char **argv)
{
	int argc;
	const char *filename;

	fp = stdout;

	argc = *argcp;
	argc--;		// first arg is always function called by unode
	argv++;

	while (argc > 0) {
		if (strcmp(*argv, "-o") == 0 && argc > 1) {

			// we found "-o filename", so update the arg
			// count and shift the upper arguments down
			// to remove "-o filename"

			filename = argv[1];
			argc -= 2;
			*argcp -= 2;
			while (argc > 0) {
				argv[0] = argv[2];
				argc--;
				argv++;
			}

			// if we can open the file, use it, otherwise
			// use stdout

			fp = fopen(filename, "w");
			if (fp == NULL)
				fp = stdout;
			return;
		}

		argc--;
		argv++;
	}
}
