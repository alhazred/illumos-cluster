/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_UNODE_UTIL_H
#define	_UNODE_UTIL_H

#pragma ident	"@(#)unode_util.h	1.11	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

unsigned int    unode_node_number();
const char	*unode_get_clpldir();
const char	*unode_get_vpdir();
const char	*unode_get_clustername();

#ifdef __cplusplus
}
#endif

#endif	/* _UNODE_UTIL_H */
