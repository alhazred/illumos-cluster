/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_UNODE_CONFIG_H
#define	_UNODE_CONFIG_H

#pragma ident	"@(#)unode_config.h	1.27	08/05/20 SMI"

#include <sys/os.h>
#include <sys/clconf_int.h>
#include <sys/hashtable.h>

#define	UNODE_NODE_DIR_PREFIX	"node-"

int find_run_directory(char *, char *, char *);

// Global variables to store the name of the executable for this process.
extern char	**unode_args;

//
// unode_config
//
// The unode config object is a repository for configuration options
// that can be set during the execution of a unode process.
//
class unode_config
{
public:
	//
	// List of supported transports.
	//
	static const char * const	supported_transports[];

	//
	// Default values.
	//
	static const int	default_num_adapters;
	static const char	default_transports[];
	static const char	default_unode_dir[];

	static unode_config	&the();

	// Initialize unode configurations.
	// Read unode configuration options from rc file and environment.
	// nodeid is the simulated node in unode_main
	//	and is the node we want to talk to in unode_load
	// Returns 0 if successful.
	int	initialize(const char *cluster_name, nodeid_t nodeid,
			    bool verbose = false);

	// Configure current node's directories and files.
	// Returns 0 if successful.
	// Note: initialize is called from both unode_main and unode_load
	// configure is only called from unode_main for the program that
	// is simulating a node
	int	configure();

	// Builds a path to a node's directory, and appends whatever string
	// you want to that path.  (This way we can build paths into the node's
	// directory structure).  The arguments are like printf variable
	// length arguments.  E.g.:
	//
	//	node_path(pathbuf, 1, "/root/usr/cluster/bin/%s", "foo");
	//
	// would place something like in 'pathbuf':
	//
	//	"<cluster_dir>/root/usr/cluster/bin/foo"
	//
	// The resulting path is returned in the supplied buffer.
	//
	void	node_path(char *buf, nodeid_t, const char *format, ...);

	// Look up a debug buffer name in the hash table _dbg_bufname_hash.
	// Each debug buffer can use this to determine whether its contents
	// should be saved to a file.
	// Returns:
	//	UNINITIALIZED	-- hash table is not initialized yet.  Caller
	//			   should check back later.
	//	FOUND		-- the name is found in the hash table.
	//	NOT_FOUND	-- the name is not found in the hash table.
	//
	enum lookup_t { UNINITIALIZED, FOUND, NOT_FOUND };
	lookup_t	lookup_dbg_buf(const char *name, bool print_by_default);

	// Load initial libraries and/or functions, if any.
	// Returns 0 if successful.
	int	load_init();

	// Returns the current node ID (mimics orb_conf::node_number()).
	nodeid_t	node_number();

	// Returns the original working directory (before unode chdir
	// to the node directory).
	const char	*orig_cwd();

	// Returns the cluster name.
	const char	*cluster_name();

	// Returns the cluster directory.
	const char	*cluster_dir();

	// Returns current node's directory.
	const char	*node_dir();

	// Returns current node's ccr directory.
	const char	*ccr_dir();

	// Returns current node's clpl directory.
	const char	*clpl_dir();

	// Returns current node's versioned protocol spec file directory.
	const char	*vp_dir();

	// Returns file containing PID of current node.
	const char	*pid_file();

	// Returns NULL-terminated array of transports used.
	const char	**transports();

	// Returns number of transports used.
	int		num_transports() const;

	// Returns number of adapters per node.
	int		num_adapters() const;

	// Returns specified adapter (e.g. hme), or NULL if unspecified
	const char	*adapter(int nodeid, int adp);

	// Returns specified adapter instance, or NULL if unspecified
	int		adapter_instance(int nodeid, int adp);

	// Returns (space-separated) list of debug buffers to save to files.
	const char	*save_dbg_buf();

	// Returns true if to pause when a signal, e.g. SIGSEGV, is received.
	bool	hang_on_signal();

	// Returns true if should use direct connect transport cables
	// instead of swtiches/hubs/blackboxes
	bool	direct_connect();

	// Returns semicolon-separated list of libraries and/or functions to
	// be loaded at start of THIS node.
	const char	*node_init();

	// Returns semicolon-separated list of libraries and/or functions to
	// be loaded at start of EVERY node.
	const char	*cluster_init();

	// Returns true if boot time is to be stamped on the timestamp file.
	bool	boot_time_stamp();

	// Returns true if verbosity is wanted.
	bool	is_verbose();

private:
	// Only allow one instance of this class: _the_unode_config.
	unode_config();
	~unode_config();

	// Get options from "rc" file.
	// Return 0 if successful.
	int	_get_rc_opts();

	// Get options from environment variables.
	// Return 0 if successful.
	int	_get_env_opts();

	// Find and open an rc file to use.  The resulting file pointer is
	// returned in the parameter (set to NULL if no file is found).
	// Returns 0 if successful, non-zero if an error occurs.
	//
	// Algorithm for determining which rc file to use:
	//
	//	1. If a file named "unoderc" exists in the current directory,
	//	   then use it.  Else, go to step 2.
	//	2. If environment variable $UNODE_RC exists, then go to step 3.
	//	   Else go to step 4.
	//	3. If the file specified by $UNODE_RC exists, then use it.
	//	   Else return.
	//	4. If a file named ".unoderc" (notice the dot) exists in the
	//	   user's home directory exists, then use it.  Else return.
	//
	int	_get_rc_file(FILE *&);

	// Parse comma-/space-separated list of transports and copy them into
	// _transports[].
	void	_parse_transport_list(const char *list);

	// Parse comma-/space-seperated list of transports and copy them into
	// _adapters[].
	void	_parse_adapter_list(const char *list);

	// Set process limits acceptable for debugging.
	void	_set_process_limits();

	// Do the actual loading of initial libraries/functions.
	// Returns 0 if successful.
	int	_do_load_init(const char *);

	// Given a directory path, create it and its parent directories.
	// Returns 0 if successful.
	int	_mkdirp(const char *);

	typedef string_hashtable_t<bool>	_string_hash_t;

	// Hash table to store names of debug buffers which user wants to
	// save to files.
	_string_hash_t	_dbg_bufname_hash;
	bool		_dbg_bufname_hash_initialized;

	nodeid_t	_node_number;

	bool	_verbose;
	bool	_boot_time_stamp;
	bool	_hang_on_signal;
	bool	_direct_connect;

	char	*_ccr_dir;
	char	*_clpl_dir;
	char	*_cluster_dir;
	char	*_cluster_init;
	char	*_cluster_name;
	char	*_node_dir;
	char	*_node_init;
	char	*_orig_cwd;
	char	*_pid_file;
	char	*_save_dbg_buf;
	char	*_unode_dir;
	char	*_vp_dir;
	int	_num_adapters;
	int	_num_transports;
	char	**_transports;
	char	***_adapters;
	int	**_adapter_instances;

	static unode_config	_the_unode_config;

	// Disallow assignments or pass by value
	unode_config(const unode_config &);
	unode_config &operator = (const unode_config &);
};

#include <unode/unode_config_in.h>

#endif	/* _UNODE_CONFIG_H */
