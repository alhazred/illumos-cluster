/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)tmpl_insts.cc	1.11	08/05/20 SMI"

//
// The GNU C++ compiler (g++) handles template instance generation
// differently, and less automatically, than the Forte compiler.
//
// This file contains template instance declarations that allow
// us to compile unode.  This needs to be cleaned up at some point.
//
// Some of the wierd formatting is due to cstyle complaints.
//

#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/os.h>
#include <new.h>

#include <unode/unode.h>
#include <unode/fakegate.h>
#include <sys/nodeset.h>
#include <orb/infrastructure/orb.h>
#include <orb/idl_datatypes/sequence.cc>

#include <h/replica.h>
template class _NormalSeq_<replica::priority_info>;
template class _NormalSeq_<replica::prov_dependency>;
template class _NormalSeq_<replica::service_state_info>;
template class _NormalSeq_<replica::repl_prov_info>;
template class _NormalSeq_<replica::service_info>;
#include <h/version_manager.h>
template class _NormalSeq_<version_manager::ucc>;
template class _FixedSeq_<version_manager::vp_version_t>;
template class _NormalSeq_<version_manager::vp_info_t>;

#include <h/quorum.h>
template class _NormalSeq_<quorum::quorum_device_config_t>;
template class _NormalSeq_<quorum::quorum_device_status_t>;
template class _FixedSeq_<quorum::node_status_t>;
template class _FixedSeq_<quorum::node_config_t>;
template class _FixedSeq_<quorum::quorum_result_t>;

template class _FixedSeq_<int>;
template class _FixedSeq_<unsigned int>;
template class _FixedSeq_<unsigned char>;
template class _FixedSeq_<unsigned short>;

#include <h/dc.h>
template class _NormalSeq_<mdc::service_property>;
template class _NormalSeq_<mdc::device_service_info>;
template class _FixedSeq_<mdc::dev_range>;
template class _FixedSeq_<mdc::node_preference>;

template class _NormalSeq_<naming::binding>;

#include <h/network.h>
template class _NormalSeq_<network::grpinfo_t>;
template class _NormalSeq_<network::macinfo_t>;
template class _NormalSeq_<network::srvinfo_t>;
template class _NormalSeq_<network::fwd_t>;
template class _FixedSeq_<network::cid_t>;
template class _FixedSeq_<network::node_status>;
template class _FixedSeq_<network::inaddr_t>;
template class _FixedSeq_<network::print_state_cmd_t>;
template class _FixedSeq_<network::gns_bind_t>;
template class _FixedSeq_<network::hashinfo_t>;

#include <h/bulkio.h>
template class _FixedSeq_<bulkio::file_hole>;

#include <h/sol.h>
template class _FixedSeq_<sol::aclent_t>;

#include <h/rgm.h>
template class _NormalSeq_<rgm::r_state>;
template class _NormalSeq_<rgm::rg_state>;
template class _NormalSeq_<rgm::idl_nameval>;
template class _NormalSeq_<rgm::idl_nameval_node>;

#include <h/replica_int.h>
template class _NormalSeq_<replica_int::init_service_info>;
template class _FixedSeq_<replica_int::rma_service_state>;

#include <h/ref_stress.h>
template class _NormalSeq_<ref_stress::test_param>;
#include <h/component_state.h>
template class _NormalSeq_<component_state::component_state_t>;

#include <h/ccr.h>
template class _NormalSeq_<ccr::table_element>;

#include <h/ccr_data.h>
template class _FixedSeq_<ccr_data::consis>;

#include <h/repl_pxfs.h>
template class _FixedSeq_<repl_pxfs::lock_info_t>;

template class _NormalSeq_<fs::unixdir::direntry_t>;

template class _T_var_<_NormalSeq_<component_state::component_state_t>,
    _NormalSeq_<component_state::component_state_t> *>;
template class _T_var_ < component_state::component_state_t,
    component_state::component_state_t *>
;

template class _ManagedSeq_<_NormalSeq_<component_state::component_state_t>,
    component_state::component_state_t >
;

#include <orb/idl_datatypes/interfaceseq.cc>
template class _Ix_InterfaceSeq_ < ccr_data::updatable_table_copy,
    _A_field_ < ccr_data::updatable_table_copy,
    ccr_data::updatable_table_copy * >, ccr_data::updatable_table_copy * >
;
template class _Ix_InterfaceSeq_ < replica::checkpoint,
    _A_field_<replica::checkpoint, replica::checkpoint *>,
    replica::checkpoint * >
;
template class _Ix_InterfaceSeq_ < network::mcobj, _A_field_ < network::mcobj,
    network::mcobj * >, network::mcobj * >
;
template class _Ix_InterfaceSeq_ < ccr::callback, _A_field_ < ccr::callback,
    ccr::callback * >, ccr::callback * >
;
template class _Ix_InterfaceSeq_ < CORBA::Object, _A_field_ < CORBA::Object,
    CORBA::Object * >, CORBA::Object * >
;

#include <h/streams.h>
template class _Ix_InterfaceSeq_ < streams::queue, _A_field_ < streams::queue,
    streams::queue * >, streams::queue * >
;

template class _Ix_InterfaceSeq_ < version_manager::vm_lca,
    _A_field_ < version_manager::vm_lca, version_manager::vm_lca * >,
    version_manager::vm_lca * >
;

template class _Ix_InterfaceSeq_ < replica_int::cb_coord_control,
    _A_field_ < replica_int::cb_coord_control, replica_int::cb_coord_control *>,
    replica_int::cb_coord_control * >
;

#include <orb/object/proxy.h>
#include <orb/object/proxy.cc>
#include <h/orbtest.h>
#include <h/orbmsg.h>
#include <h/repl_sample.h>
#include <h/ccr_trans.h>
#include <h/mc_sema.h>
#include <h/orphan_fi.h>
#include <h/repl_dc.h>
#include <h/data_container.h>
#include <h/ha_stress.h>
#include <h/unref_fi.h>
#include <h/flowcontrol_fi.h>
#include <h/repl_ns.h>
#include <h/repl_rm.h>
#include <h/test_kproxy.h>
#include <h/ff.h>
#include <h/clevent.h>
#include <h/addrspc.h>
template class proxy<CORBA::Object_stub>;
template class proxy<bulkio::aio_write_obj_stub>;
template class proxy<bulkio::in_aio_stub>;
template class proxy<bulkio::in_pages_stub>;
template class proxy<bulkio::in_uio_stub>;
template class proxy<bulkio::inout_pages_stub>;
template class proxy<bulkio::inout_uio_stub>;
template class proxy<bulkio::pages_write_obj_stub>;
template class proxy<bulkio::uio_write_obj_stub>;
template class proxy<bulkio_holder::holder_stub>;
template class proxy<ccr::callback_stub>;
template class proxy<ccr::directory_stub>;
template class proxy<ccr::readonly_table_stub>;
template class proxy<ccr::updatable_table_stub>;
template class proxy<ccr_data::data_server_stub>;
template class proxy<ccr_data::updatable_table_copy_stub>;
template class proxy<ccr_trans::ckpt_stub>;
template class proxy<ccr_trans::handle_stub>;
template class proxy<ccr_trans::manager_stub>;
template class proxy<clevent::clevent_comm_stub>;
template class proxy<cmm::automaton_stub>;
template class proxy<cmm::callback_registry_stub>;
template class proxy<cmm::callback_stub>;
template class proxy<cmm::comm_stub>;
template class proxy<cmm::control_stub>;
template class proxy<cmm::kernel_ucmm_agent_stub>;
template class proxy<cmm::remote_ucmm_proxy_stub>;
template class proxy<cmm::userland_cmm_stub>;
template class proxy<component_state::registry_stub>;
template class proxy<data_container::data_stub>;
template class proxy<ff::failfast_admin_stub>;
template class proxy<ff::failfast_stub>;
template class proxy<flowcontrol_fi::node_stub>;
template class proxy<flowcontrol_fi::server_stub>;
template class proxy<fs::dc_callback_stub>;
template class proxy<fs::device_stub>;
template class proxy<fs::dircache_stub>;
template class proxy<fs::dirprov_stub>;
template class proxy<fs::file_stub>;
template class proxy<fs::filesystem_stub>;
template class proxy<fs::fobj_stub>;
template class proxy<fs::fobjcache_stub>;
template class proxy<fs::fobjprov_stub>;
template class proxy<fs::fs_collection_stub>;
template class proxy<fs::fsmgr_client_stub>;
template class proxy<fs::fsmgr_server_stub>;
template class proxy<fs::io_stub>;
template class proxy<fs::memcache_stub>;
template class proxy<fs::mempager_stub>;
template class proxy<fs::mount_client_died_stub>;
template class proxy<fs::mount_client_stub>;
template class proxy<fs::mount_server_stub>;
template class proxy<fs::pxfs_llm_callback_stub>;
template class proxy<fs::special_stub>;
template class proxy<fs::symbolic_link_stub>;
template class proxy<fs::unixdir_stub>;
template class proxy<ha_stress::client_stub>;
template class proxy<mc_sema::sema_stub>;
template class proxy<mdc::dc_config_stub>;
template class proxy<mdc::dc_mapper_stub>;
template class proxy<mdc::device_server_stub>;
template class proxy<mdc::device_service_manager_stub>;
template class proxy<mdc::reservation_client_stub>;
template class proxy<naming::binding_iterator_stub>;
template class proxy<naming::naming_context_stub>;
template class proxy<network::PDTServer_stub>;
template class proxy<network::ckpt_stub>;
template class proxy<network::cl_net_ns_if_stub>;
template class proxy<network::lbobj_i_stub>;
template class proxy<network::mcnet_node_stub>;
template class proxy<network::mcobj_stub>;
template class proxy<network::perf_mon_policy_stub>;
template class proxy<network::user_policy_stub>;
template class proxy<network::weighted_lbobj_stub>;
template class proxy<orbmsg::message_mgr_stub>;
template class proxy<orbtest::test_ckpt_stub>;
template class proxy<orbtest::test_mgr_stub>;
template class proxy<orbtest::test_obj_stub>;
template class proxy<orbtest::test_params_stub>;
template class proxy<orphan_fi::common_stub>;
template class proxy<orphan_fi::server_stub>;
template class proxy<orphan_fi::testobj_stub>;
template class proxy<perf::cbk_stub>;
template class proxy<perf::group_stub>;
template class proxy<perf::kstats_stub>;
template class proxy<perf::stats_stub>;
template class proxy<pxfs_aio::aio_callback_stub>;
template class proxy<quorum::quorum_algorithm_stub>;
template class proxy<ref_stress::client_stub>;
template class proxy<ref_stress::rsrc_balancer_stub>;
template class proxy<ref_stress::server_stub>;
template class proxy<ref_stress::testobj_stub>;
template class proxy<repl_dc::repl_dc_ckpt_stub>;
template class proxy<repl_dc::repl_dev_server_ckpt_stub>;
template class proxy<repl_ns::ns_replica_stub>;
template class proxy<repl_pxfs::fs_replica_stub>;
template class proxy<repl_pxfs::ha_mounter_stub>;
template class proxy<repl_pxfs::mount_replica_stub>;
template class proxy<repl_rm::rm_ckpt_stub>;
template class proxy<repl_sample::ckpt_stub>;
template class proxy<repl_sample::factory_stub>;
template class proxy<repl_sample::persist_obj_stub>;
template class proxy<repl_sample::persistent_store_stub>;
template class proxy<repl_sample::value_obj_stub>;
template class proxy<replica::checkpoint_stub>;
template class proxy<replica::repl_prov_reg_stub>;
template class proxy<replica::repl_prov_stub>;
template class proxy<replica::rm_admin_stub>;
template class proxy<replica::rma_public_stub>;
template class proxy<replica::service_admin_stub>;
template class proxy<replica::service_state_callback_stub>;
template class proxy<replica::trans_state_stub>;
template class proxy<replica::transaction_id_stub>;
template class proxy<replica_int::handler_repl_prov_stub>;
template class proxy<replica_int::reconnect_object_stub>;
template class proxy<replica_int::rm_service_admin_stub>;
template class proxy<replica_int::rm_stub>;
template class proxy<replica_int::rma_admin_stub>;
template class proxy<replica_int::rma_reconf_stub>;
template class proxy<replica_int::rma_repl_prov_stub>;
template class proxy<replica_int::rmm_stub>;
template class proxy<replica_int::tid_factory_stub>;
template class proxy<rgm::rgm_comm_stub>;
template class proxy<solobj::cred_stub>;
template class proxy<streams::msg_stub>;
template class proxy<streams::queue_stub>;
template class proxy<test_kproxy::server_stub>;
template class proxy<unref_fi::server_stub>;
template class proxy<unref_fi::testobj_stub>;
template class proxy<mc_sema::server_stub>;
template class proxy<addrspc::addrspace_stub>;
template class proxy<bulkio::in_aio_pages_stub>;
template class proxy<version_manager::vm_admin_stub>;
template class proxy<version_manager::inter_vm_stub>;
template class proxy<version_manager::vm_lca_stub>;
template class proxy<replica_int::cb_coord_control_stub>;
template class proxy<version_manager::custom_vp_query_stub>;

template class Anchor<orbmsg::message_mgr_stub>;
template class Anchor<replica_int::rmm_stub>;

#include <orb/object/adapter.cc>
template class McServerof<replica_int::rma_reconf>;
template class McServerof<ccr::callback>;
template class McServerof<ccr::directory>;
template class McServerof<ccr::readonly_table>;
template class McServerof<ccr_data::data_server>;
template class McServerof<ccr_data::updatable_table_copy>;
template class McServerof<cmm::automaton>;
template class McServerof<cmm::callback>;
template class McServerof<cmm::callback_registry>;
template class McServerof<cmm::comm>;
template class McServerof<cmm::control>;
template class McServerof<cmm::kernel_ucmm_agent>;
template class McServerof<component_state::registry>;
template class McServerof<ff::failfast>;
template class McServerof<ff::failfast_admin>;
template class McServerof<naming::naming_context>;
template class McServerof<quorum::quorum_algorithm>;
template class McServerof<replica::repl_prov>;
template class McServerof<replica::trans_state>;
template class McServerof<replica::transaction_id>;
template class McServerof<replica_int::handler_repl_prov>;
template class McServerof<replica_int::reconnect_object>;
template class McServerof<replica_int::rma_admin>;
template class McServerof<replica_int::rma_repl_prov>;
template class McServerof<replica_int::tid_factory>;
template class McServerof<version_manager::vm_lca>;
template class McServerof<version_manager::vm_admin>;
template class McServerof<version_manager::custom_vp_query>;
template class McServerof<mc_sema::sema>;
template class McServerof<mc_sema::server>;

template class McNoref<replica_int::rmm>;
template class McNoref<naming::naming_context>;
template class McNoref<orbmsg::message_mgr>;
template class McNoref<version_manager::inter_vm>;

#include <repl/service/replica_tmpl.h>
#include <repl/service/replica_tmpl.cc>
template class mc_checkpoint<ccr_trans::ckpt>;
template class mc_checkpoint<repl_ns::ns_replica>;
template class mc_checkpoint<repl_rm::rm_ckpt>;

template class mc_replica_of<ccr::updatable_table>;
template class mc_replica_of<ccr_trans::handle>;
template class mc_replica_of<ccr_trans::manager>;
template class mc_replica_of<naming::naming_context>;
template class mc_replica_of<replica::repl_prov_reg>;
template class mc_replica_of<replica_int::rm>;
template class mc_replica_of<replica_int::rm_service_admin>;
template class mc_replica_of<replica_int::cb_coord_control>;

template class repl_server<ccr_trans::ckpt>;
template class repl_server<repl_ns::ns_replica>;
template class repl_server<repl_rm::rm_ckpt>;

#include <h/data_container.h>
#include <nslib/data_container_handler.h>
template class impl_spec<data_container::data, data_container_handler>;
#include <nslib/binding_iter_handler.h>
template class impl_spec<naming::binding_iterator, binding_iter_handler>;

#include <sys/list_def.h>
#include <orb/refs/refcount.h>
#include <orb/refs/refcount.cc>
#include <orb/xdoor/rxdoor_defs.h>
template class ref_jobs<rxdoor_ref>;
template class ref_jobs<translate_ack>;

#include <orb/object/schema.h>
#include <orb/object/schema.cc>
template class schema_list<ExceptDescriptor>;
template class schema_list<InterfaceDescriptor>;

#include <h/version.h>
template class McNoref<idlversion>;
template class proxy<idlversion_stub>;
template class _FixedSeq_<inf_version_tbl_entry>;
