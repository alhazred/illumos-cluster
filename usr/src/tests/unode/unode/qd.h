/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_QD_H
#define	_QD_H

#pragma ident	"@(#)qd.h	1.20	08/05/20 SMI"

#include <door.h>
#include <sys/types.h>
#include <orb/invo/common.h>
#include <sys/quorum_int.h>
#include <sys/os.h>

#include <quorum/common/shared_disk_util.h>

extern "C" {

#define	PATHNAME_SIZE		64

#define	SCSI2_ACCESS_DENIED(t, m)	((t != NODEID_UNKNOWN) && (t != m))

typedef struct quorum_pathname {
	uchar_t		name[PATHNAME_SIZE];
} quorum_pathname_t;

}

enum qd_command_vals {
	QD_SCSI_OPEN = 1,
	QD_SCSI3_REGISTER,
	QD_SCSI3_REGISTERANDIGNOREKEY,
	QD_SCSI3_PREEMPT,
	QD_SCSI3_RESERVE,
	QD_SCSI3_READKEYS,
	QD_SCSI3_READRESV,
	QD_SCSI2_GET_SBLKNO,
	QD_SCSI2_KEY_WRITE,
	QD_SCSI2_KEY_READ,
	QD_SCSI2_KEY_FIND,
	QD_SCSI2_KEY_DELETE,
	QD_SCSI2_CHOOSING_WRITE,
	QD_SCSI2_CHOOSING_READ,
	QD_SCSI2_NUMBER_WRITE,
	QD_SCSI2_NUMBER_READ,
	QD_SCSI_ER_PAIR_WRITE,
	QD_SCSI_ER_PAIR_READ,
	QD_SCSI2_TKOWN,
	QD_SCSI2_RELEASE
};

enum qd_return_vals {
	QD_OK = 0,
	QD_DOOR_CALL_FAILED,
	QD_NOT_ENOUGH_MEM,
	QD_NOT_ENOUGH_ARGS,
	QD_KEY_NOT_REGISTERED,
	QD_WRONG_DOOR,
	QD_DEL_KEY_NOT_THERE,
	QD_KEY_ALREADY_IN_LIST,
	QD_ACCESS_DENIED,
	QD_INVALID_COMMAND
};

class scsi_pgr_util
{
public:
	static bool	eq(mhioc_resv_key_t &, mhioc_resv_key_t &);
	static void	copy(mhioc_resv_key_t &, mhioc_resv_key_t &);
};

class reservation_keys
{
public:
	reservation_keys(uint_t);
	~reservation_keys(void);

	void		lock(void);
	void		unlock(void);
	int		add_key(mhioc_resv_key_t, uint64_t);
	int		add_reservation(mhioc_resv_key_t, uint64_t);
	int		del_key(mhioc_resv_key_t);
	uint64_t	list_keys(uint64_t, mhioc_resv_key_t*);
	uint64_t	list_resvs(uint64_t, mhioc_resv_desc_t*);
	bool		key_in_list(mhioc_resv_key_t &);
private:
	bool			lock_held(void);
	uint_t			nodes;
	mhioc_resv_key_t	*keylist;
	mhioc_resv_desc_t	reservation;
	os::mutex_t		rks_lock;
	mhioc_resv_key_t	null_key;

	// Disallow assignments and pass by value.
	reservation_keys(const reservation_keys &);
	reservation_keys &operator = (reservation_keys &);
};

/*
 * All fields are 64 bit numbers, to fit in the existing unode-qd
 * infrastructure.
 *
 * It is necessary for all fields to be 64 bits because of the way the qd_door
 * function processes arguments. In that function, the argument bitstring is
 * cast as an array of uint64_t.
 */
typedef struct unode_pgre_sector {
	mhioc_resv_key_t	key;
	uint64_t		number;
	uint64_t		choosing;
	int64_t			epoch;
	uint64_t		seqnum;
} unode_pgre_sector_t;

class scsi2_pgre_area
{
public:
	scsi2_pgre_area(void);
	~scsi2_pgre_area(void);

	void		lock(void);
	void		unlock(void);

	int 		write_key(uint64_t, mhioc_resv_key_t &, uint64_t);
	int		read_key(uint64_t, mhioc_resv_key_t *, uint64_t);
	int		find_key(uint64_t, mhioc_resv_key_t &, uint64_t &,
			    uint64_t);
	int		delete_key(uint64_t, uint64_t);

	int		write_choosing(uint64_t, uint64_t, uint64_t);
	int		read_choosing(uint64_t, uint64_t &, uint64_t);
	int		write_number(uint64_t, uint64_t, uint64_t);
	int		read_number(uint64_t, uint64_t &, uint64_t);
	int		write_er_pair(uint64_t, int64_t, uint64_t, uint64_t);
	int		read_er_pair(uint64_t, int64_t&, uint64_t&, uint64_t);
	int		get_sblkno(uint64_t&, uint64_t);

	int		release(void);
	int		tkown(uint64_t);

private:
	bool			lock_held(void);
	unode_pgre_sector_t		node_info[NODEID_MAX + 1];
	os::mutex_t		pgre_lock;
	mhioc_resv_key_t	null_key;
	uint64_t		tkown_nodeid;

	// Disallow assignments and pass by value.
	scsi2_pgre_area(const scsi2_pgre_area &);
	scsi2_pgre_area &operator = (scsi2_pgre_area &);
};

void wait_for_signal(int);
bool start_qd_doors(int);
void stop_qd_doors(int);
void qd_door(void *, char *, size_t, door_desc_t *, uint_t);

#include <unode/qd_in.h>

#endif	/* _QD_H */
