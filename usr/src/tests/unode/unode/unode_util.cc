/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)unode_util.cc	1.38	08/05/20 SMI"

#include <orb/infrastructure/orb_conf.h>
#include <orb/infrastructure/orb.h>
#include <orb/transport/path_manager.h>
#include <sys/heartbeat.h>
#include <sys/lwp.h>
#include <sys/ddi.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <dlfcn.h>
#include <time.h>
#include <orb/infrastructure/clusterproc.h>
#include <cmm/cmm_impl.h>
#include <sys/priocntl.h>
#include <sys/rtpriocntl.h>
#include <sys/tspriocntl.h>

#include "unode.h"
#include "fakegate.h"

extern void (*path_manager_cyclic_interface_funcp)(
    path_manager::cyclic_caller_t);

os::notify_t shutdown_notify;

static uint_t	fakeclock_up, shutdown_clock, fakecyclic_up, shutdown_cyclic;
static clock_t	fake_lbolt;
void (*cmm_clock_callout)(void);

//
// Clock tick emulation
// Runs with a frequency of 10 rather than 100.  This is done through a
// time dilation in drv_hztousec().
//
void
fakeclock(void)
{
	void (*tmpcall)(void);

	while (!shutdown_clock) {
		fake_lbolt++;
		os::envchk::set_nonblocking(os::envchk::NB_INTR);
		tmpcall = cmm_clock_callout; // avoid race condition
		if (tmpcall) {
			tmpcall();
		}
		os::envchk::clear_nonblocking(os::envchk::NB_INTR);
		os::usecsleep((os::usec_t)drv_hztousec((clock_t)1));
	}
	fakeclock_up = 0;
}

//
// path manager cyclic emulation
// fires with frequency PM_CYCLIC_HZ (currently 250 msec).
//
void
fake_cyclic(void)
{

	while (!shutdown_cyclic) {
		os::envchk::set_nonblocking(os::envchk::NB_INTR);

		if (path_manager_cyclic_interface_funcp != NULL)
			cluster_heartbeat();

		os::envchk::clear_nonblocking(os::envchk::NB_INTR);
		os::usecsleep((os::usec_t)((1000/PM_CYCLIC_HZ) * 1000));
	}
	fakecyclic_up = 0;
}

int
fakeclock_start(void)
{
	int ret;
	pcinfo_t pc_info;
	pri_t	pri = 0;
#ifdef linux
	const char *cname = "TS";
#else
	const char *cname = NULL;

	// Calculate Maximum RT priority. We call this routine before
	// ORB::initialize where this is calculated. Hence the code is
	// duplicated. If RT class is not configured, we pick the maximum
	// TS class priority.

	(void) strcpy(pc_info.pc_clname, "RT");
	if ((geteuid() == 0) &&
	    priocntl((idtype_t)0, (id_t)0, PC_GETCID, (char *)&pc_info) != -1) {
		rtinfo_t *rt_info = (rtinfo_t *)pc_info.pc_clinfo;
		pri = rt_info->rt_maxpri;
		cname = "RT";
	} else {
		(void) strcpy(pc_info.pc_clname, "TS");
		if (priocntl((idtype_t)0, (id_t)0, PC_GETCID, (char *)&pc_info)
		    != -1) {
			tsinfo_t *ts_info = (tsinfo_t *)pc_info.pc_clinfo;
			pri = ts_info->ts_maxupri;
			cname = "TS";
		} else {
			os::panic("Neither RT or TS classes configured");
		}
	}
#endif
	ASSERT(cname != NULL);

	shutdown_clock = 0;
	fake_lbolt = 0;
	fakeclock_up = 1;
	ret = clnewlwp((void (*)(void *))fakeclock, NULL, pri, cname, NULL);
	if (ret != 0) {
		os::printf("thread_create failed for RT clock simulator (%s)\n",
			strerror(ret));
		fakeclock_up = 0;
		return (-1);
	}
	return (0);
}

int
fakecyclic_start(void)
{
	int ret;
	pcinfo_t pc_info;
	pri_t	pri = 0;
#ifdef linux
	const char *cname = "TS";
#else
	const char *cname = NULL;

	// Calculate Maximum RT priority. We call this routine before
	// ORB::initialize where this is calculated. Hence the code is
	// duplicated. If RT class is not configured, we pick the maximum
	// TS class priority.

	(void) strcpy(pc_info.pc_clname, "RT");
	if ((geteuid() == 0) &&
	    priocntl((idtype_t)0, (id_t)0, PC_GETCID, (char *)&pc_info) != -1) {
		rtinfo_t *rt_info = (rtinfo_t *)pc_info.pc_clinfo;
		pri = rt_info->rt_maxpri;
		cname = "RT";
	} else {
		(void) strcpy(pc_info.pc_clname, "TS");
		if (priocntl((idtype_t)0, (id_t)0, PC_GETCID, (char *)&pc_info)
		    != -1) {
			tsinfo_t *ts_info = (tsinfo_t *)pc_info.pc_clinfo;
			pri = ts_info->ts_maxupri;
			cname = "TS";
		} else {
			os::panic("Neither RT or TS classes configured");
		}
	}
#endif
	ASSERT(cname != NULL);

	shutdown_cyclic = 0;
	fakecyclic_up = 1;
	ret = clnewlwp((void (*)(void *))fake_cyclic, NULL, pri, cname, NULL);
	if (ret != 0) {
		os::printf("thread_create failed for cyclic simulator (%s)\n",
			strerror(ret));
		fakecyclic_up = 0;
		return (-1);
	}
	return (0);
}

void
fakeclock_end()
{
	while (fakeclock_up) {
		shutdown_clock = 1;
		// XX Could use a cv - but too lazy!
		os::usecsleep((os::usec_t)1000000);
	}
}

void
fakecyclic_end()
{
	while (fakecyclic_up) {
		shutdown_cyclic = 1;
		// XX Could use a cv - but too lazy!
		os::usecsleep((os::usec_t)1000000);
	}
}

#undef	drv_usectohz
#undef	drv_hztosec

extern "C" {
clock_t
drv_usectohz(clock_t theclock)
{
	return (theclock/100000);
}

//
// For UNODE, time is dilated in the conversion (using the factor 100000
// instead of 10000).  This will make the fake clock run at frequency 10
// rather than 100.
//
clock_t
drv_hztousec(clock_t thehertz)
{
	return (100000*thehertz);
}

int
drv_getparm(unsigned int parm, void *valuep)
{
	switch (parm) {
		case LBOLT:
			*(clock_t *)valuep = fake_lbolt;
			break;

		default:
			return (-1);
	}
	return (0);
}
} // extern "C"

//
// unode_shutdown()
//
// Will shutdown all clustering and UNODE modules
void
unode_shutdown()
{
	ORB::shutdown();
	fakeclock_end();
	fakecyclic_end();
	fakegate_end();
}


void
unode_quit()
{
	os::printf("Shutting down unode %d on quit\n", orb_conf::node_number());
	shutdown_notify.signal();
}


//
// reboot_unode_clear()
//
// Will re-exec the unode process (This same binary) with a flag to make it
// sleep for about 25 seconds before it begins execution.
// Prior to reexec does an ORB::shutodnw() so we get a clean shutdown
//
void
reboot_unode_clean()
{
	ORB::shutdown();
	reboot_unode();
}


//
// reboot_unode()
//
// Will re-exec the unode process (This same binary) with a flag to make it
// sleep for about 25 seconds before it begins execution.
void
reboot_unode()
{
	ASSERT(unode_args != NULL);

	int	argc;
	char	**argv;

	// Count the unode_args
	for (argc = 0; unode_args[argc] != 0; argc++) {
		// EMPTY
	}

	if (argc > 4) {
		// Time argument is already written in
		argv = unode_args;
	} else {
		// Allocate that many args + 1.
		// Lint Info override for promotion int to size_t
		argv = new char *[argc + 1];	//lint !e737

		// Copy the arguments
		for (argc = 0; unode_args[argc] != 0; argc++) {
			size_t	arglen;
			arglen = strlen(unode_args[argc]);
			argv[argc] = new char[arglen + 1];
			os::sprintf(argv[argc], "%s", unode_args[argc]);
		}

		// Write in the extra argument (sleep value);
		// XXX - This sleep value should be large enough
		// to allow the node to be found unknown and then
		// down!
		//
		// The reason for this is because of the quorum
		// tests, some of which need the node to stay away
		// from the cluster.
		//
		argv[argc] = new char[3];
		os::sprintf(argv[argc], "30");

		// Write in a NULL ending
		argv[argc + 1] = 0;
	}

	// Move back to the original directory so the new process will start
	// with the same condition as the current one.
	(void) chdir(unode_config::the().orig_cwd());

	(void) execv(argv[0], argv);
	os::panic("reboot_unode: execv: %s", strerror(errno));
}

#ifdef linux
extern "C" int unode_init();
#endif

//
// This should return -1 on error and zero for no error to mimic the kernel's
// modload() routine which returns -1 or the module ID.
//
int
unode_transport_modload(const char *trtype)
{
#ifdef linux
	return (unode_init());
#else
	void	*lhandle;
	unode_initp	inip;

	// see if the library is already loaded
	lhandle = dlopen(trtype, RTLD_NOLOAD);
	if (lhandle != NULL) {
		// Library already loaded
		return (0);
	}

	lhandle = dlopen(trtype, RTLD_NOW|RTLD_LOCAL);
	if (lhandle == NULL) {
		os::warning("%s\n", dlerror());
		return (-1);
	}

	inip = (unode_initp)dlsym(lhandle, "unode_init");	//lint !e611
	if (inip == NULL) {
		os::warning("Could not find unode_init in %s\n", trtype);
		return (-1);
	}
	return ((inip() == 0) ? 0 : -1);
#endif
}


//
// Signal handler for pausing when a signal is received.
//
void
unode_spin_loop(int sig)
{
	os::printf("***Waiting in %s handler after panic/abort***\n",
		strsignal(sig));
	::symtracedump();
	(void) pause();
}
