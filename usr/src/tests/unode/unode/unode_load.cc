/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)unode_load.cc	1.19	08/05/20 SMI"

#include <door.h>
#include <string.h>
#include <fcntl.h>
#include <sys/os.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#include "fakegate.h"

#define	RETRY_MAX	3
#define	RETRY_SLEEP	5

void
print_usage(char *prog)
{
	os::printf("Usage: %s cluster_name nid shared_obj "
		"[func [ arg ... ] ]\n", prog);
	os::printf("Where:\n");
	os::printf("\tcluster_name is the name of the cluster\n");
	os::printf("\tnid is the ID of the node\n");
	os::printf("\tshared_obj is the name of the shared object to load\n");
	os::printf("\tfunc is the name of a function in shared_object "
		"to execute\n");
	os::printf("\targ ... are arguments to be passed func\n");
}

int
main(int argc, char **argv)
{
	char		*fakegate_file;
	int		nodeid;
	int		dp;
	int		result;
	size_t		argsize;
	int		i;
	fgret		retval;
	char		*sendbuf;
	char		*endptr;
	door_arg_t	darg;
	int		arg;
	char		*output_keyword = NULL;
	int		retry_counter = 0;

	// Examine command line options
	while ((arg = getopt(argc, argv, "x:")) != EOF) {
		switch (arg) {
		case 'x' :
			// KEYWORD to print prior to printing the output
			// of the function that was loaded
			//
			// This option is here so that the fault injection
			// test driver can have some dependable string to
			// search for, when looking for the result of the
			// entry point loaded.  DO not remove this functionality
			// unless you know that you wont break the fault
			// injection test driver.
			output_keyword = optarg;
			break;
		default :
			print_usage(argv[0]);
			return (1);
		}
	}

	if ((argc - optind) < 2) {
		print_usage(argv[0]);
		return (1);
	}

	// Convert nid argument to number.
	errno = 0;
	nodeid = (int)strtol(argv[optind + 1], &endptr, 0);
	if (errno || *endptr != '\0') {
		os::printf("Invalid node ID: %s\n", argv[optind + 1]);
		return (1);
	}
	if (nodeid < 1 || nodeid > NODEID_MAX) {
		os::printf("Node ID (%d) must be >= 1 and <= %d\n",
			nodeid, NODEID_MAX);
		return (1);
	}

	// Initialize the unode_config object
	if (unode_config::the().initialize(argv[optind], (nodeid_t)nodeid)) {
		return (1);
	}

	// Open the control door gateway
	fakegate_file = fakegate_fname();
	for (;;) {
		dp = open(fakegate_file, O_RDONLY);
		if (dp < 0) {
			if (errno == ENOENT) {
				// Retry
				if (retry_counter < RETRY_MAX) {
					retry_counter++;
					os::printf("Open (%s) failed due to: "
					    "%s\n", fakegate_file,
					    strerror(errno));
					os::printf("Will retry in %d secs.\n",
					    RETRY_SLEEP);
					os::usecsleep(RETRY_SLEEP * 1000000L);
					continue;
				}
			}

			// Other errors or too many retries
			os::printf("Could not open %s (%s)\n",
				fakegate_file, strerror(errno));
			return (1);
		}

		break;
	}

	darg.data_ptr = "getack";
	darg.data_size = 7;
	darg.desc_num = 0;
	darg.rsize = sizeof (fgret);
	darg.desc_ptr = NULL;
	darg.rbuf = (char *)&retval;

	result = door_call(dp, &darg);
	if (result < 0) {
		os::printf("Could not make door call to %s to get ACK (%s)\n",
			fakegate_file, strerror(errno));
		return (1);
	}

	// We do the separate ack to confirm that the server is running
	// (more useful debugging info)
	if (retval.fg_ret_num != FG_TESTACK) {
		os::printf("Unknown ACK %d from node %d\n",
			retval.fg_ret_num, nodeid);
		return (1);
	}

	//
	// Now we compose the message and send the sucker
	//

	// If there is no function name specified, we're done.
	if ((argc - optind) < 3) {
		return (0);
	}

	argsize = 0;
	for (i = optind + 2; i < argc; i++) {
		argsize += strlen(argv[i]);
	}

	// lint complains of suspicious cast
	sendbuf = new char[argsize +
	    (size_t)(argc - (optind + 1))]; //lint !e571
	if (sendbuf == NULL) {
		os::printf("Can't allocate buffer\n");
		return (1);
	}

	sendbuf[0] = 0;

	for (i = optind + 2; i < argc; i++) {
		(void) strcat(sendbuf, argv[i]);
		(void) strcat(sendbuf, "\001");
	}

	darg.data_size = strlen(sendbuf);
	sendbuf[darg.data_size - 1] = (char)0;
	darg.data_ptr = sendbuf;
	darg.desc_num = 0;
	darg.rsize = sizeof (fgret);
	darg.desc_ptr = NULL;
	darg.rbuf = (char *)&retval;

	result = door_call(dp, &darg);
	if (result < 0) {
		os::printf("door call failed with errno %d\n", errno);
		os::printf("Could not make door call to %s to send message "
			"(%s)\n", fakegate_file, strerror(errno));
		return (1);
	}

	if (retval.fg_ret_num) {
		os::printf("An error occured in the door call: %s\n",
			FG_ERRSTRING(retval.fg_ret_num));
		return (1);
	}

	if (output_keyword != NULL) {
		// This option is here so that the fault injection
		// test driver can have some dependable string to
		// search for, when looking for the result of the
		// entry point loaded.  DO not remove this functionality
		// unless you know that you wont break the fault
		// injection test driver.
		os::printf("%s %d\n", output_keyword, retval.test_ret_num);
	} else {
		os::printf("The test %s function %s returned %d\n",
		    argv[optind + 2], argv[optind + 3], retval.test_ret_num);

	}

	return (0);
}
