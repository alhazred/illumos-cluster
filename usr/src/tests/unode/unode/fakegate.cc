/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fakegate.cc	1.36	08/05/20 SMI"

#include <sys/types.h>
#include <door.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <sys/os.h>
#include <stdio.h>
#include <unistd.h>
#include <limits.h>
#include <dlfcn.h>
#include <unode/fakegate.h>
#include <stdlib.h>

//
// fakegate.cc
//
// This module will provide a way of dynamically loading new test packages
// into a unode, and calling a main-like (argv+argc) function inside it.
//
// If the module is not part of the system, it is dlopened, and the function
// unode_init is called if it exists.  Then the given function (if any) is
// called with the supplied arguments.  If the module is already in the
// system, the function is just called immediately.
// Communication is through specially allocated doors, one per process,
// which are in addition to the existing doors used in the unode_transport.
//

// Error messages corresponding to each fakegate_result member.
char *fg_errarray[] = {
	"",
	"The data sent to the door was not a string.",
	"There is no dynamically loadable object by that name.",
	"There is no unode_init function in the loaded object.",
	"The execution of unode_init failed.",
	"There was no function of the specified name in the object.",
	"The execution of the specified function failed.",
	"There are too many function arguments."
};

os::notify_t	startup_notify;

static char	*fakegate_file = NULL;			// door file
static int	fakegate_ddesc = -1;			// door descriptor


int
fakegate_start(void)
{
#ifdef linux
	return (0);
#else
	int	tfd;

	fakegate_ddesc = door_create(fakegate_door, NULL, 0);
	if (fakegate_ddesc < 0) {
		os::printf("fakegate door server failed to initialize (%s)\n",
			strerror(errno));
		goto error;
	}

	// Construct file name to attach door to.
	fakegate_file = fakegate_fname();

	// Create the file to attach door to.
	(void) unlink(fakegate_file);
	tfd = open(fakegate_file, O_CREAT|O_EXCL, 0666);
	if (tfd < 0) {
		if (errno != EEXIST) {
			os::printf("fakegate could not create %s (%s)\n",
				fakegate_file, strerror(errno));
			goto error;
		}
		os::printf("Warning: %s already exists\n", fakegate_file);
	}
	(void) close(tfd);

	// Attach door to file.
	if (fattach(fakegate_ddesc, fakegate_file) < 0) {
		os::printf("fakegate could not attach door to %s (%s)\n",
			fakegate_file, strerror(errno));
		goto error;
	}

	return (0);					// success

error:
	// Clean up on error
	(void) close(fakegate_ddesc);
	fakegate_ddesc = -1;
	if (fakegate_file != NULL) {
		(void) unlink(fakegate_file);
		fakegate_file = NULL;
	}
	return (-1);
#endif
}


void
fakegate_end()
{
#ifdef linux
#else
	if (fakegate_file != NULL) {
		(void) close(fakegate_ddesc);
		fakegate_ddesc = -1;

		(void) fdetach(fakegate_file);

		(void) unlink(fakegate_file);
		fakegate_file = NULL;
	}
#endif
}


void
dreturn(fakegate_result fgerr, int testerr)
{
#ifdef linux
#else
	fgret retval;

	retval.fg_ret_num = fgerr;
	retval.test_ret_num = testerr;
	(void) door_return((char *)&retval, sizeof (retval), NULL, 0);
#endif
}


//
// Find a specified function [0]) in the specified shared library 'libname'
// and execute that function, passing the fargc and fargv.
// Note, fargc and fargv are similar to main()'s argc and argv, except that
// fargv[0] specifies the name of the function to execute.
// The returned value of the called function will be returned in the
// argument 'func_result'.
//
fakegate_result
fakegate_load(const char *libname, int fargc, char *fargv[], int &func_result)
{
#ifdef linux
	return (FG_NOFUNC);
#else
	void	*lhandle;
	char	*funcname;
	char	mangledfunc[70];  // pulled 70 out of thin air
	unode_initp	inip;
	unode_funcp	funcp;

	// see if the library is already loaded
	lhandle = dlopen(libname, RTLD_NOLOAD);
	if (lhandle == NULL) {
		// open the library for real
		lhandle = dlopen(libname, RTLD_NOW|RTLD_LOCAL);
		if (lhandle == NULL) {
			os::printf("%s\n", dlerror());
			return (FG_NOLIB);
		}

		inip = (unode_initp)dlsym(lhandle, "unode_init"); //lint !e611
		if (inip == NULL) {
			// Try mangled symbol
#define	CCSYM_UNODE_INIT "__1cKunode_init6F_i_"

			inip = (unode_initp)dlsym(lhandle,
			    CCSYM_UNODE_INIT);	//lint !e611
			if (inip == NULL) {
				return (FG_NOINIT);
			}
		}

		if (inip()) {
			return (FG_INITFAILED);
		}

	}

	// If no function name, then caller just wants to load the library.
	if (fargc <= 0 || fargv == NULL) {
		func_result = 0;
		return (FG_SUCCESS);
	}
	funcname = fargv[0];

	funcp = (unode_funcp)dlsym(lhandle, funcname);		//lint !e611
	if (funcp == NULL) {
		size_t funclen = strlen(funcname);

		//
		// Try mangled symbol
		//
		//
		// SC5.0+ mangling
		// If funclen > 25
		//	__1cXXfuncname6Fippc_i_ == int name(int,char**)
		//
		// If funclen <= 25
		// 	__1cXfuncname6Fippc_i_ == int name(int,char**)
		//
		//
		if (funclen > 25) {
			(void) strcpy(mangledfunc, "__1c00");
		} else {
			(void) strcpy(mangledfunc, "__1c0");
		}
		(void) strcat(mangledfunc, funcname);
		(void) strcat(mangledfunc, "6Fippc_i_");

		if (funclen > 25) {
			mangledfunc[4] = 'b';
			mangledfunc[5] = (char)('A' - 26 + funclen);
		} else {
			mangledfunc[4] = (char)('A' + funclen);
		}

		funcp = (unode_funcp)dlsym(lhandle, mangledfunc); //lint !e611
		if (funcp == NULL) {
			return (FG_NOFUNC);
		}
	}

	// Call the function.
	optind = 1;
	func_result = funcp(fargc, fargv);

	return (FG_SUCCESS);
#endif
}

#ifdef __i386

// dbx on Intel has a problem with threads that originate
// from door calls that often makes debugging impossible
// (see 4377697).  To work around that we have fakegate_door
// spawn a thread to run fakegate_load.

// Mutex and condition variable.  Lint doesn't like how
// DEFAULTMUTEX initializes a union, but there isn't anything
// we can do about it.

#ifndef linux
static mutex_t fgl_mtx = DEFAULTMUTEX;	//lint !e708
static cond_t fgl_cv = DEFAULTCV;
#endif

// Struct for bundling fakegate_load args, plus synchronization
// info.

struct fgl_args {
	const char	*libname;
	int		fargc;
	char		**fargv;
	int		func_result;
	fakegate_result result;
	int		done;
};

// Thread to run fakegate_load.  Calls fakegate_load and wakes up
// run_fakegate_load.

extern "C" void *
fakegate_load_thread(void *arg)
{
#ifdef linux
	return (NULL);
#else
	fgl_args *fap;

	fap = (fgl_args *)arg;
	fap->result = fakegate_load(fap->libname, fap->fargc, fap->fargv,
	    fap->func_result);

	(void) mutex_lock(&fgl_mtx);
	fap->done = 1;
	(void) cond_broadcast(&fgl_cv);
	(void) mutex_unlock(&fgl_mtx);

	return (NULL);
#endif
}

// Batch up the arguments and create a thread starting at
// fakegate_load_thread.  If the thr_create fails for some
// reason, call fakegate_load directly.  This would let us
// continue when a thread couldn't be created and the
// workaround isn't needed, although that is an unlikely
// case.

static fakegate_result
run_fakegate_load(const char *libname, int fargc, char *fargv[],
    int &func_result)
{
#ifdef linux
	return (FG_FUNCFAILED);
#else
	fgl_args fa;

	fa.libname = libname;
	fa.fargc = fargc;
	fa.fargv = fargv;
	fa.func_result = func_result;

	fa.done = 0;

	// Try to create a thread.  The thread is created as a detached
	// thread since we don't care about doing a thr_join on it.

	if (thr_create(NULL, (size_t)0, fakegate_load_thread, &fa,
	    (long)THR_DETACHED, NULL) != 0) {

		// Couldn't create thread so call fakegate_load
		// directly.

		return (fakegate_load(libname, fargc, fargv, func_result));
	}

	// Wait for fakegate_load_thread to tell
	// us it is done.

	(void) mutex_lock(&fgl_mtx);
	while (!fa.done)
		(void) cond_wait(&fgl_cv, &fgl_mtx);
	(void) mutex_unlock(&fgl_mtx);

	func_result = fa.func_result;
	return (fa.result);
#endif
}

#endif

void
fakegate_door(void *, char *argp, size_t arg_size, door_desc_t *, uint_t)
{
#ifdef linux
#else
	char		breakon[] = "\001";
	char		*stsave;
	int		fargc;
	char		*fargv[FG_NUMARGS_MAX];
	char		*libname;
	int		func_result = 0;
	fakegate_result	result;

	// Wait for join cluster to complete before we start using cluster code
	startup_notify.wait();

	if (argp[arg_size - 1] != 0) {
		dreturn(FG_NOTSTRING, 0);
	}

	libname = strtok_r(argp, breakon, &stsave);

	// If it's a test call, return an ACK right away
	if (strcmp(libname, "getack") == 0) {
		dreturn(FG_TESTACK, 0);
	}

	for (fargc = 0; fargc < FG_NUMARGS_MAX; ++fargc) {
		fargv[fargc] = strtok_r(NULL, breakon, &stsave);
		if (fargv[fargc] == NULL) {
			break;
		}
	}
	if (fargc >= FG_NUMARGS_MAX) {
		dreturn(FG_ARGMAX, 0);
	}

#ifdef __i386
	result = run_fakegate_load(libname, fargc, fargv, func_result);
#else
	result = fakegate_load(libname, fargc, fargv, func_result);
#endif

	dreturn(result, func_result);
#endif
}

char *
fakegate_fname()
{
#ifdef linux
	return (NULL);
#else
	static short	firsttime = 1;
	static char	file[PATH_MAX + 1];

	if (firsttime) {
		// Construct the control door file pathname
		(void) sprintf(file, "%s/control",
					unode_config::the().node_dir());
		firsttime = 0;
	}
	return (file);
#endif
}
