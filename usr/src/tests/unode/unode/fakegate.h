/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FAKEGATE_H
#define	_FAKEGATE_H

#pragma ident	"@(#)fakegate.h	1.19	08/05/20 SMI"

#include <unode/unode_config.h>
#include <stdio.h>
#include <sys/os.h>
#include <sys/door.h>
#include <orb/object/adapter.h>

extern os::notify_t startup_notify;

#define	FG_NUMARGS_MAX	512

enum fakegate_result {
	FG_SUCCESS,			// this must be the first element
	FG_NOTSTRING,
	FG_NOLIB,
	FG_NOINIT,
	FG_INITFAILED,
	FG_NOFUNC,
	FG_FUNCFAILED,
	FG_ARGMAX,
	FG_TESTACK			// this must be the last element
};

// Error messages corresponding to each fakegate_result member above.
extern char *fg_errarray[];

#define	FG_ERRSTRING(i) (fg_errarray[i])

typedef int (*unode_initp)(void);
typedef int (*unode_funcp)(int, char **);
typedef struct fgret_struct {
	fakegate_result	fg_ret_num;
	int		test_ret_num;
} fgret;

int		fakegate_start(void);
void		fakegate_end(void);
fakegate_result	fakegate_load(const char *libname, int fargc, char *fargv[],
			int &func_result);
void		fakegate_door(void *, char *, size_t, door_desc_t *, uint_t);
char		*fakegate_fname(void);
void		dreturn(fakegate_result, int);

#endif	/* _FAKEGATE_H */
