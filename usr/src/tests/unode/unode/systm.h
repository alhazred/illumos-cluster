/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _UNODE_SYSTM_H
#define	_UNODE_SYSTM_H

#pragma ident	"@(#)systm.h	1.10	08/05/20 SMI"

//
// The purpose of this header file is to provide replacements
// for selected functions defined on a real cluster in systm.h
//

#include <strings.h>

//
// copyin - replace with a bcopy function call and
// a sequence operator (the comma) followed by the return value of success.
// The unode copyin has no problems crossing from user to kernel space.
//
#define	copyin(src, dest, size) (bcopy(src, dest, size), 0)
#define	xcopyin(src, dest, size) (bcopy(src, dest, size), 0)

//
// copyout - replace with a bcopy function call and
// a sequence operator (the comma) followed by the return value of success.
// The unode copyout has no problems crossing from kernel to user space.
//
#define	copyout(src, dest, size) (bcopy(src, dest, size), 0)
#define	xcopyout(src, dest, size) (bcopy(src, dest, size), 0)
#define	copyoutstr(src, dest, size, lencopied) \
	(bcopy(src, dest, *(lencopied) = strlen(src)), 0)

//
// maxusers - unode version defines the maximum number of users
// a unode system supports. This really is used for system configuration.
//
extern volatile int maxusers;

#endif	/* _UNODE_SYSTM_H */
