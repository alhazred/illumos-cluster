#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)unode_setup.ksh	1.19	08/05/20 SMI"
#

#
#
# Creates the infrastructure file.
PROGDIR=$(dirname $0)		# directory where this program resides
# make PROGDIR an absolute path
case $PROGDIR in 
	"/"*)
	;;

	*)
	PROGDIR=`pwd`/$PROGDIR
	;;
esac
readonly PROGDIR

# Need to source this file in to obtain unode options from "rc" file.
. $PROGDIR/unode_config

# Print message and then exit with a status of 1.
function exit_msg
{
  print $1
  exit 1
}

# Initialize the globals 
function initialize
{
  APPENDFILE=none
  CLUSTER=none
  FILE=none
  MODE=silent
  NUMNODES=none
  TEMPLATE=none
  CUR_DIR=`pwd`
  # Figure out where the proto area is
  readonly BUILD_DIR=$PROGDIR/../../../
  INSTALL_CMD=/usr/sbin/install
  LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$BUILD_DIR/usr/cluster/lib
  export LD_LIBRARY_PATH
}

# Parse the command line options
function parse
{
  while getopts a:c:n:i:v OPT
  do
    case $OPT in
      a ) APPENDFILE=$OPTARG
	  ;;
      c ) CLUSTER=$OPTARG
	  ;;
      n ) NUMNODES=$OPTARG
	  ;;
      i ) TEMPLATE=$OPTARG
	  ;;
      v ) MODE=verbose
	  ;;
      : ) exit 1
	  ;;
      \?) print "usage: unode_setup -c [cluster] -n num_of_nodes -i [template] [-v]"
	  exit 1
	  ;;
    esac
  done
}

# Validate all the globals.
function validate_globals
{
  [[ $NUMNODES = none ]] && exit_msg "Number of nodes must be given with -n option"
  [[ $CLUSTER = none ]] && exit_msg "Cluster name must be given with -c option"
  [[ $NUMNODES -le 0 ]] && exit_msg "Number of nodes must be at least 1."
  [[ $FILE != none && ! -f $FILE ]] && exit_msg "File $FILE not found"
  [[ $TEMPLATE != none && ! -f $TEMPLATE ]] && exit_msg "Template $TEMPLATE not found"
  [[ $TEMPLATE = none && $FILE = none ]] && exit_msg "No template or file given"

  # Change all relative paths to absolute ones so 'install' will work correctly
  [[ "$UNODE_DIR" != /* ]] && UNODE_DIR=$CUR_DIR/$UNODE_DIR
 
  # Change all relative paths to TEMPLATE so 'install' will correctly work.
  [[ "$TEMPLATE" != /* ]] && TEMPLATE=$CUR_DIR/$TEMPLATE

  # Change all relative file name paths to absolute ones.
  if [[ $APPENDFILE != none ]]
  then
      [[ "$APPENDFILE" != /* ]] && APPENDFILE=$CUR_DIR/$APPENDFILE
      [[ -f $APPENDFILE ]] || exit_msg "The file $APPENDFILE does not exist."
  fi

}

  
# Create the directories for each node.
function create_directories
{
  # Remove any old directories first.
  # rm -fr $UNODE_DIR/$CLUSTER
  [[ ! -d $UNODE_DIR/$CLUSTER ]] && mkdir -p $UNODE_DIR/$CLUSTER
  cd $UNODE_DIR/$CLUSTER
  typeset -i num=1
  while [[ $num -le $NUMNODES ]]
  do
    mkdir -p node-$num/ccr
    num=$num+1
  done
  return 0
}

# Setup the precedence and if no errors, then call for creation of
# infrastructure.
function setup
{
  create_directories
}


# Create the infrastructure file
# Takes the template file and runs under sed to instaniate key words.
function create_infrastructure
{
  typeset -i num=1

  if grep % $TEMPLATE > /dev/null 2>&1
  then
      $BUILD_DIR/usr/unode/bin/unode_instantiate -f $TEMPLATE -n $NUMNODES -C $CLUSTER > infrastructure.$$
  else
	cp $TEMPLATE infrastructure.$$
  fi

  # append a file if necessary to infrastructure.$$
  [[ ! $APPENDFILE = none ]] && cat $APPENDFILE >> infrastructure.$$

  if ! grep '^ccr_checksum' infrastructure.$$ 
  then
    $BUILD_DIR/usr/cluster/lib/sc/ccradm -F -i infrastructure.$$ -o
  fi

  while [[ $num -le $NUMNODES ]]
  do
    $INSTALL_CMD -f $UNODE_DIR/$CLUSTER/node-$num/ccr -m 644 infrastructure.$$
    mv $UNODE_DIR/$CLUSTER/node-$num/ccr/infrastructure.$$ $UNODE_DIR/$CLUSTER/node-$num/ccr/infrastructure
    num=$num+1
  done

  # remove temp infrastructure
  # rm -fr infrastructure.$$
}

# Append lines from appendfile if it exists
function append_file
{
  [[ ! $APPENDFILE = none ]] && cat $APPENDFILE >> $UNODE_DIR/$CLUSTER/node-$num/ccr/infrastructure
}

# Main
initialize
parse $@
validate_globals
setup
[[ $MODE = silent ]] && exec 1>$UNODE_DIR/$CLUSTER/unode_setup.log 2>$UNODE_DIR/$CLUSTER/unode_setup.stderr
create_infrastructure
#append_file
exit 0
