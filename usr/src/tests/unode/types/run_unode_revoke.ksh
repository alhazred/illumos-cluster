#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)run_unode_revoke.ksh	1.22	08/05/20 SMI"
#

#
# A simple script to automatically start UNODE nodes (displayed in xterm
# windows), start the UNODE IDL types test client and server,
# run the revoke test, and unload the client and server.
# At the end, the user is prompted whether to kill the nodes or not.
# This is useful for a quick kenrnel ORB putback testing in user land.
#
# NOTE: Do not use variables named UNODE_* in this script.  These variable
#	names are reserved and used by unode.
#

PROGDIR=$(dirname $0)			# directory where this program resides

ITERS=10				# how many times to run the IDL tests.


# Figure out where the unode utilities are
if [ $ROOT ]
then
	readonly BINDIR_UNODE=$ROOT/usr/unode/bin
	readonly LIBDIR_UNODE=$ROOT/usr/unode/lib
else
	# Relative to the $PROGDIR directory.
	readonly BINDIR_UNODE=$PROGDIR/../bin
	readonly LIBDIR_UNODE=$PROGDIR/../lib
fi

# Need to source this file in to obtain unode options from "rc" file.
. $BINDIR_UNODE/unode_config

# unode utilities we're going to use.
CMD_UNODE_CLUSTER=$BINDIR_UNODE/unode_cluster
CMD_UNODE_LOAD=$BINDIR_UNODE/unode_load
CMD_UNODE_KILL=$BINDIR_UNODE/unode_kill

export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$LIBDIR_UNODE"

# What's my login name?
: ${LOGNAME:=$(logname)}

# Make up cluster name (make it somewhat unique by using login name and this
# script's PID to avoid two different users running the types tests and
# clobbering each other's unode files in $UNODE_DIR).
CLUSTER_NAME=${LOGNAME}_$$_revoketest


# Try to create the $UNODE_DIR directory first.  So in case of error,
# we can catch it first before unode does (otherwise the xterms we're
# going to create will disappear so fast, user won't know what
# happens).
[[ -d "$UNODE_DIR" ]] || mkdir -p $UNODE_DIR || exit 1

# Make sure we can find all the unode utilities.
for PROG in $CMD_UNODE_CLUSTER $CMD_UNODE_LOAD $CMD_UNODE_KILL
do
	if [[ ! -x $PROG ]]
	then
		print -u2 "Can't find $PROG"
		exit 1
	fi
done


# Functions to start cluster, setup/run/teardown tests, and kill cluster
function start_cluster #
{
	# 3 nodes in the cluster: one each for server, client and driver.
	$CMD_UNODE_CLUSTER $CLUSTER_NAME 3
}

function run_server #
{
	# Run in node 1.
	$CMD_UNODE_LOAD $CLUSTER_NAME 1 types_v0 server
}

function run_client #
{
	# Run in node 2.
	$CMD_UNODE_LOAD $CLUSTER_NAME 2 types_v0 client
}

function run_test # <program_args>
{
	# Run driver in node .
	$CMD_UNODE_LOAD $CLUSTER_NAME 3 types_v0 driver -revoke "$@"
}

function end_test #
{
	# Use driver to tell server and client that testing is done.
	# This can be run from any node
	$CMD_UNODE_LOAD $CLUSTER_NAME 2 types_v0 driver -done
}

function teardown_cluster #
{
	$CMD_UNODE_KILL $CLUSTER_NAME all
	if [ -f $UNODE_DIR/$CLUSTER_NAME/*/core ]
	then
		print "FAIL : Core files found. Not removing directory $UNODE_DIR/$CLUSTER_NAME"
	else
		rm -rf $UNODE_DIR/$CLUSTER_NAME
	fi
}


#
# Do it.
#
trap 'teardown_cluster' EXIT

start_cluster
sleep 5		# give cluster time to initialize

run_server &
run_client &
sleep 5		# give client and server time to register with name server

run_test "$@"

end_test
wait

# Prompt user.
# XX Removing this as a workaround so we can make this run in a script
print "\nExpect revoke succeeded message on node 2"
print "Expect revoke operation timings dump on node 3"
# print "\nPress [RETURN] when done. This will dismantle all nodes." > /dev/tty
# read < /dev/tty

exit 0
