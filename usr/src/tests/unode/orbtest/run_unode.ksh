#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)run_unode.ksh	1.21	08/05/20 SMI"
#

#
# A simple script to automatically start UNODE nodes (displayed in xterm
# windows), start the test clients and servers, run the tests once, and
# unload the clients and servers.  At the end, the user is prompted whether
# to kill the nodes or not.  This is useful for a quick kernel ORB putback
# testing in user land.
#
# NOTE: Do not use variables named UNODE_* in this script.  These variable
#	names are reserved and used by unode.
#

readonly PROGNAME=$0
readonly PROGDIR=$(dirname $0)		# directory where this program resides

# Default parameter values.
integer -r DEFAULT_NUM_NODES=4
integer -r DEFAULT_NUM_ITERS=5
integer -r DEFAULT_NUM_THREADS=30
integer -r DEFAULT_NUM_INVOS=1
integer -r DEFAULT_NUM_GETS=25
integer -r DEFAULT_NEW_OBJECTS=0	# false -- reuse objects
integer -r DEFAULT_VERBOSE=0		# false

# Parameters.
integer NUM_NODES=$DEFAULT_NUM_NODES
integer NUM_ITERS=$DEFAULT_NUM_ITERS
integer NUM_THREADS=$DEFAULT_NUM_THREADS
integer NUM_INVOS=$DEFAULT_NUM_INVOS
integer NUM_GETS=$DEFAULT_NUM_GETS
integer NEW_OBJECTS=$DEFAULT_NEW_OBJECTS
integer VERBOSE=$DEFAULT_VERBOSE
integer SERVER_NODEID			# node to run the test server

# Template used as infrastructure file instead of generating it.
TEMPLATE=
use_template=no

# Figure out where the unode utilities are
if [ $ROOT ]
then
	readonly BINDIR_UNODE=$ROOT/usr/unode/bin
	readonly LIBDIR_UNODE=$ROOT/usr/unode/lib
else
	# Relative to the $PROGDIR directory.
	readonly BINDIR_UNODE=$PROGDIR/../../../proto_unode/bin
	readonly LIBDIR_UNODE=$PROGDIR/../../../proto_unode/lib
fi

# Need to source this file in to obtain unode options from "rc" file.
. $BINDIR_UNODE/unode_config

# unode utilities we're going to use.
readonly CMD_UNODE_CLUSTER=$BINDIR_UNODE/unode_cluster
readonly CMD_UNODE_LOAD=$BINDIR_UNODE/unode_load
readonly CMD_UNODE_KILL=$BINDIR_UNODE/unode_kill
readonly CMD_UNODE_SETUP=$BINDIR_UNODE/unode_setup

export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$LIBDIR_UNODE"

# What's my login name?
: ${LOGNAME:=$(logname)}

# Make up cluster name (make it somewhat unique by using login name and this
# script's PID to avoid two different users running the same test and
# clobbering each other's unode files in $UNODE_DIR).
readonly CLUSTER_NAME=${LOGNAME}_$$_orbtest

# Usage instruction.
readonly USAGE="\
Usage: $PROGNAME [options]

Options:
   -help|-?
	Prints this message.
   -node <num_nodes>
	Number of nodes in the cluster.
	Default: $DEFAULT_NUM_NODES
   -server <node_id>
	The node to run the test server (node 1 is the root node).
	Default: random
   -iter <num_iters>
	Number of times each client test should run the test.
	Default: $DEFAULT_NUM_ITERS
   - i <infrastructure_file>
	Tells test server to use this infrastructure file instead of
	creating it by default.
   -thread <num_threads>
	Number of threads each client creates (one client per node).
	Default: $DEFAULT_NUM_THREADS
   -get <num_gets>
	Number of times each client thread should get test objects.
	Default: $DEFAULT_NUM_GETS
   -invo <num_invos>
	Number of put() and get() invocations each client thread makes
	on the test objects.
	Default: $DEFAULT_NUM_INVOS
   -new
	Tells test server to create a new test object implementation for
	each client request for a test object.  Otherwise, only one
	implementation will be created and the server will pass references
	to it for each each client request.
   -v
	Verbose output.
"


#
# Obtains a positive option argument and prints to stdout.
# Returns 0 if successful, 1 otherwise.
#
# $1 must contain the option string
#
function pos_optarg # <args>
{
	typeset opt=$1
	typeset optarg=$2

	if [[ -z "$optarg" ]]
	then
		print -u2 "ERROR: option $opt requires argument"
		return 1
	fi

	if ! ( integer numarg=$optarg ) || (( optarg <= 0 ))
	then
		print -u2 "ERROR: $opt option argument must be > 0"
		return 1
	fi

	print $optarg
	return 0
}


#
# Setup
#
function setup # <program_args>
{
	integer server_nid=0
	typeset mode_str verbose_str

	# Make sure we can find all the unode utilities.
	for PROG in $CMD_UNODE_CLUSTER $CMD_UNODE_LOAD $CMD_UNODE_KILL
	do
		if [[ ! -x $PROG ]]
		then
			print -u2 "Can't find $PROG"
			exit 1
		fi
	done

	# Parse options.
	while (( $# ))
	do
		case "$1" in
		-h*|-\?*)
			print -u2 "$USAGE"
			exit 0
			;;
		-nod*)
			NUM_NODES=$(pos_optarg "$@") || exit 1
			shift
			;;
		-ser*)
			server_nid=$(pos_optarg "$@") || exit 1
			shift
			;;
		-ite*)
			NUM_ITERS=$(pos_optarg "$@") || exit 1
			shift
			;;
		-thr*)
			NUM_THREADS=$(pos_optarg "$@") || exit 1
			shift
			;;
		-get*)
			NUM_GETS=$(pos_optarg "$@") || exit 1
			shift
			;;
		-inv*)
			NUM_INVOS=$(pos_optarg "$@") || exit 1
			shift
			;;
		-new*)
			NEW_OBJECTS=1
			;;
		-v*)
			VERBOSE=1
			;;
		-i) 
			use_template=yes
			TEMPLATE=$2
			[[ ! -f $TEMPLATE ]] && { print "Cannot find $TEMPLATE"; exit 1; }
			shift
			;;
		*)
			print -u2 "ERROR: option $1 unknown"
			exit 1
			;;
		esac
		shift
	done

	# Try to create the $UNODE_DIR directory first.  So in case of error,
	# we can catch it first before unode does (otherwise the xterms we're
	# going to create will disappear so fast, user won't know what
	# happens).
	[[ -d "$UNODE_DIR" ]] || mkdir -p $UNODE_DIR || exit 1

	# If user specified node to run test server, then use it, otherwise
	# pick one at random.
	if (( server_nid != 0 ))
	then
		# Make sure specified node is valid (while loop above already
		# verified that the node id is > 0).
		if (( server_nid > NUM_NODES ))
		then
			print -u2 "ERROR: server node id ($server_nid)" \
				"> number of nodes ($NUM_NODES)"
			exit 1
		fi
		SERVER_NODEID=$server_nid
	else
		SERVER_NODEID=$(( (RANDOM % NUM_NODES) + 1 ))
	fi

	# Print options.
	if (( NEW_OBJECTS ))
	then
		mode_str="new objects"
	else
		mode_str="reuse object"
	fi

	if (( VERBOSE ))
	then
		verbose_str="true"
	else
		verbose_str="false"
	fi

	echo "----------------------------------------------------------------"
	echo "Server node: $SERVER_NODEID"
	echo "Test mode  : $mode_str"
	echo "Num clients: $NUM_NODES"
	echo "Num iters  : $NUM_ITERS"
	echo "Num threads: $NUM_THREADS"
	echo "Num gets   : $NUM_GETS"
	echo "Num invos  : $NUM_INVOS"
	echo "Verbose    : $verbose_str"
	echo "----------------------------------------------------------------"
}


#
# Functions to start cluster, start clients and servers, and kill cluster.
#
function start_cluster #
{
	[[ $use_template = yes ]] && \
	$CMD_UNODE_SETUP -n $NUM_NODES -c $CLUSTER_NAME -i $TEMPLATE
	$CMD_UNODE_CLUSTER $CLUSTER_NAME $NUM_NODES
}

function run_params_server #
{
	# Always run in node 1 (root node).
	print "Starting parameters server on node 1 (root node)"

	$CMD_UNODE_LOAD $CLUSTER_NAME 1 orbtest params_server \
		-iter $NUM_ITERS		\
		-thread $NUM_THREADS		\
		-get $NUM_GETS			\
		-invo $NUM_INVOS		\
		$(let NEW_OBJECTS && echo -new)	\
		$(let VERBOSE && echo -v)	\
		$NUM_NODES &

	sleep 4			# give time to register with name server
}

function run_server #
{
	print "Starting test server on node $SERVER_NODEID"

	$CMD_UNODE_LOAD $CLUSTER_NAME $SERVER_NODEID orbtest mgr &

	sleep 4			# give time to register with name server
}


function run_clients #
{
	integer nid=1

	print "Starting test clients on all nodes"

	while (( nid <= NUM_NODES ))
	do
		$CMD_UNODE_LOAD $CLUSTER_NAME $nid orbtest mgr_clnt &
		(( nid += 1 ))
	done
}


function teardown_cluster #
{
	$CMD_UNODE_KILL $CLUSTER_NAME all
	rm -rf $UNODE_DIR/$CLUSTER_NAME
}


#
# Do it.
#
setup "$@"

trap 'teardown_cluster' EXIT
start_cluster

run_params_server				# parameters server
run_server					# test server
run_clients					# test clients (one per node)

wait						# wait until all are done

# Prompt user.
print "\nPress [RETURN] when done. This will dismantle all nodes." > /dev/tty
read < /dev/tty

exit 0
