#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)unode_cluster.ksh	1.18	08/05/20 SMI"
#

PROGDIR=$(dirname $0)		# directory where this program resides
if [ "$PROGDIR" = "." ]; then
	PROGDIR=$PWD
fi
readonly PROGDIR

USAGE="Usage: $0 [-l ] <cluster_name> <number_of_nodes>"

# Make sure we have path to xterm and 'unode' which should be located in the
# same directory as this script
export PATH=$(dirname $0):/usr/openwin/bin:$PATH

# -l : log unode output to file

NO_XTERM=0
while getopts l OPT
do
    case $OPT in
    l) 	NO_XTERM=1
	;;
    \?) print -u2 "$USAGE"
	exit 1
    esac
done
shift `expr $OPTIND - 1`
    
if [ "$DISPLAY" = "" ]
then
	NO_XTERM=1
fi

# Check number of arguments.
if (( $# != 2 ))
then
	print -u2 "$USAGE"
	exit 1
fi

CLUSTER_NAME=$1
NUMNODES=$2

# Make sure $CLUSTER_NAME is not blank and $NUMNODES is a positive number
if ! test $CLUSTER_NAME || ! test $NUMNODES
then
	print -u2 "$USAGE"
	exit 1
fi
if [[ $(expr "$NUMNODES" : '[0-9]*') -ne ${#NUMNODES} ||
	$NUMNODES -le 0 ]]
then
	print -u2 "ERROR: Number of nodes must a positive number"
	exit 1
fi

# Need to source this file in to obtain unode options from "rc" file.
. $PROGDIR/unode_config

# Create cluster directory, if it doesn't exist yet.
mkdir -p $UNODE_DIR/$CLUSTER_NAME || exit 1

# Cluster info file
CINFOFILE=$UNODE_DIR/$CLUSTER_NAME/clusterinfo

if [ $NO_XTERM -eq 1 ]
then
    print "Logging individual unode output to files $UNODE_DIR/$CLUSTER_NAME/node-<nodeid>.log"
fi


# Make sure we do not clobber another active unode cluster
if [[ -a $CINFOFILE ]]
then
	print -u2 "ERROR: file $CINFOFILE already exists."
	print -u2 "  Check if unode cluster with same name already active."
	exit 1
fi

# Store the number of nodes in the new cluster info file.
echo $NUMNODES > $CINFOFILE
if [[ ! -a $CINFOFILE ]]
then
	print -u2 "ERROR: Couldn't create $CINFOFILE"
	exit 1
fi

print "Starting ${NUMNODES}-node cluster called $CLUSTER_NAME"

# xterm window geometries (to make them similar to those started by mcconsole)
XTERM_SIZE=80x24
set -A XTERM_POS --	\
	dummy		\
	+600+50		\
	+100+200	\
	+100+545	\
	+600+200	\
	+600+545

# Start an xterm window for each node in the cluster
NID=1
XTERM_POS_IDX=1				# index into $XTERM_POS array
while (( NID <= $NUMNODES ))
do
	if [ $NO_XTERM = 1 ]
	then
		unode $CLUSTER_NAME $NID 1-$NUMNODES > \
		    $UNODE_DIR/$CLUSTER_NAME/node-$NID.log 2>&1 &
	else
		XTERM_TITLE="$CLUSTER_NAME $NID"

		xterm -name UNode \
		    -geom "${XTERM_SIZE}${XTERM_POS[$XTERM_POS_IDX]}" \
		    -n "$XTERM_TITLE" \
		    -title "unode: $XTERM_TITLE" \
		    -e unode $CLUSTER_NAME $NID 1-$NUMNODES &
        fi
	sleep 1
	(( NID += 1 ))
	
	(( XTERM_POS_IDX += 1 ))
	if (( XTERM_POS_IDX >= ${#XTERM_POS[*]} ))
	then
		XTERM_POS_IDX=2
	fi
done
