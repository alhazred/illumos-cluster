#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)run_unode_fi.ksh	1.96	08/05/20 SMI"
#

# This script will drive the fault injection testing on UNODE.
# The responsibilities of the script are:
#
#	1 - Setup the cluster
#	2 - Load test modules
#	3 - After test module completes bring cluster back to a known state
#	4 - Back to 2
#
# Depending on the test, different setup may be needed.  (ie: Primary/Client
# locations differ between tests)
#
# NOTE: Do not use variables named UNODE_* in this script.  These variable
#	names are reserved and used by unode.
#
# The script must be run from the unode proto area.
#
# Set a few ksh parameters, in case user has them set/unset in the $ENV file
#
ulimit -c unlimited		# size of core dumps
ulimit -f unlimited		# max size of files written
ulimit -v unlimited		# size of virtual memory

set +o allexport		# don't automatically export vars
set +o errexit			# don't exit if commands return nonzero
set +o keyword			# only use variable assignments before commands
set +o markdirs			# don't put trailing / on directory names
set -o monitor			# so bg jobs run with own PGID, and
				# we can trap SIGCHLD and clean them up
set +o noclobber		# allow clobber
set +o noglob			# enable file name globbing
set -o nolog			# don't save functions in history file
set +o notify			# don't notify bg job completion
set +o nounset			# allow use of unset variables


readonly PROGNAME=$0
readonly LOGINNAME=$(logname)

# Template used as infrastructure file instead of generating it.
TEMPLATE=
NEW_USE_TEMPLATE=
use_template=no
overwrite_use_template=no

PROGDIR=$(dirname $0)		# directory where this program resides
if [ "$PROGDIR" = "." ]; then
	PROGDIR=$PWD
fi
readonly PROGDIR

# Figure out where the unode utilities are
if [ -z "$ROOT" ]; then
	# Not defined.  Must be relative to the current dir
	#
	readonly BINDIR_UNODE=$PROGDIR/../bin
	readonly LIBDIR_UNODE=$PROGDIR/../lib
else
	# defined
	readonly BINDIR_UNODE=$ROOT/usr/unode/bin
	readonly LIBDIR_UNODE=$ROOT/usr/unode/lib
fi

#
# Need to source this file in to obtain unode options from "rc" file.
#
. $BINDIR_UNODE/unode_config

#
# Default values
#
readonly DEFAULT_FAULT_DATAFILE=$PROGDIR/fault.data
readonly DEFAULT_OUTPUT_BASEDIR=${UNODE_DIR:-/tmp}

#
# Unode Utilites we use
#
readonly CMD_UNODE=$BINDIR_UNODE/unode
readonly CMD_UNODE_LOAD=$BINDIR_UNODE/unode_load
readonly CMD_QD=$BINDIR_UNODE/qd
readonly CMD_UNODE_SETUP=$BINDIR_UNODE/unode_setup

export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$LIBDIR_UNODE"

#
# Set UNODE_BOOT_TIME_STAMP env var to force unode to generate boot time stamps
# (used to determine whether a unode has rebooted or not)
#
export UNODE_BOOT_TIME_STAMP=true

#
# Prefix string used by unode to print function result codes
#
readonly RESULT_PREFIX='UNODE_FI_TEST'

#
# Timeout for loading a routine on a unode
#
LOAD_DELAY_TIME=10			# default 10 seconds 
LOAD_TIMEOUT=180			# default 3 minutes
readonly LOAD_TIMEOUT_RETVAL=242	# value returned by unode_load() if
					# loading timeout occurred

#
# Set number of cluster nodes from command line
#
total_nodes_defined=0
TOTAL_NODES=0

###############################################################################
# print the Usage of this tool
#
function usage   #
{
	cat >&2 << EOF
Usage:
   $PROGNAME [-f file] [-x] [test options]
   $PROGNAME [-G] [-N] [-T] [-f file]
   $PROGNAME -?

Main Options:
   -G
	List all test groups defined in the fault data file.
   -N
	Display the maximum number of unodes that will be created when running
	all tests defined in the fault data file.
   -T
	List all tests defined in fault the data file.
   -f <file>
	Fault data file.
	Default: $DEFAULT_FAULT_DATAFILE
   -x
	Use an xterm window to display the output of nodeid 1.
   -?
	Displays this message.
   -i <infrastructure_file>
	Uses the infrastructure file that is specified.

Test Options:
   -d <load_delay>
	Specifies the load delay time value (in seconds) after starting all 
	unodes.  Default is 10 seconds.
   -g <group[,group,...]>
	Run tests which belong to the specified test group(s).
	Multiple groups can be specified as a comma-separated list
	or using multiple -g options.
   -I <infrastructure_file_in_USE_TEMPLATE>
	Uses the infrastructure file that is specified to override the one
	specified by USE_TEMPLATE in fault.data file.
   -K
	Keep results directories even if tests pass.
   -L <load_timeout>
	Specifies load timeout value in seconds.  Default is 180 seconds.	
   -n <number_of_unodes>
	Specifies the number of unodes that will be required for the test.  
	This will override the REQ_NODES in fault.data file.
   -o <dir>
	Output directory.
	Default: The unode option UNODE_DIR, if set.
	         Otherwise $DEFAULT_OUTPUT_BASEDIR.
   -s <test_number>
	Run tests starting at specified test number.
   -t <arg[,arg,...]>
	Run test with the specified test number.
	Multiple tests can be specified as a comma-separated list of test
	numbers and/or ranges, e.g.:

		3,4	-- run tests 3 and 4.
		4-7,9	-- run tests 4 through 7 and 9.
		-8	-- run tests 1 through 8.
		1,10-	-- run tests 1 and 10 through last.

	Multiple -t options can also be specified.
EOF
}


###############################################################################
# Examine the comand line
#
function examine_command_line   # [program_arg...]
{
	typeset opt
	typeset display_tests=0
	typeset display_groups=0
	typeset display_numnodes=0
	typeset dont_run=0
	typeset groups
	typeset testnums

	while getopts ':d:f:g:i:I:L:n:GKNo:s:t:Tx?' opt; do
		case $opt in
		d)
			LOAD_DELAY_TIME=$OPTARG
			;;
		f)
			FAULT_DATAFILE=$OPTARG
			;;
		g)
			# Allow comma-separated groups
			groups=$(echo $OPTARG | sed 's/,/ /g')
			SELECTED_GROUPS="$SELECTED_GROUPS $groups"
			;;
		i)  	
			# Use infrastructure file specified.
			TEMPLATE=$OPTARG
			use_template=yes
			if [[ ! -f $TEMPLATE ]]; then
				print "Cannot find $TEMPLATE"
				exit 1
			fi
			;;
		I)  	
			# Use infrastructure file specified
			# to override the infrastructure file
			# specified by USE_TEMPLATE.
			NEW_USE_TEMPLATE=$OPTARG
			overwrite_use_template=yes
			if [[ ! -f $NEW_USE_TEMPLATE ]]; then
				print "Cannot find $NEW_USE_TEMPLATE"
				exit 1
			fi
			;;
		L)
			LOAD_TIMEOUT=$OPTARG
			;;
		n)
			TOTAL_NODES=$OPTARG
			total_nodes_defined=1
			;;
		G)
			display_groups=1
			dont_run=1
			;;
		K)
			KEEP_RESULTS=1
			;;
		N)
			display_numnodes=1
			dont_run=1
			;;
		o)
			# Override unode's $UNODE_DIR
			UNODE_DIR=$OPTARG
			;;
		s)
			# This is the same as specifying "-t <num>-"
			testnums="$testnums ${OPTARG}-"
			;;
		t)
			# Collect the specified test number(s) first
			testnums="$testnums $OPTARG"
			;;
		T)
			display_tests=1
			dont_run=1
			;;
		x)
			XTERM_DISPLAY=1
			;;
		\?)
			if [[ "$OPTARG" = '?' ]]; then
				usage
				exit 0
			else
				print -u2 "ERROR: Option $OPTARG unknown"
				exit 1
			fi
			;;
		:)
			print -u2 "ERROR: Option $OPTARG needs argument"
			exit 1
			;;
		*)
			print -u2 "ERROR: Option $opt unrecognized"
			exit 1
			;;
		esac
	done
	shift 'OPTIND - 1'

	# Check fault data file.
	if [ ! -f "$FAULT_DATAFILE" ]; then
		print -u2 "Fault data file \"$FAULT_DATAFILE\" doesn't exist"
		exit 1
	fi

	if [ ! -r "$FAULT_DATAFILE" ]; then
		print -u2 "Fault data file \"$FAULT_DATAFILE\" is unreadable"
		exit 1
	fi

	# If user wants to display some stuff...
	if [ "$display_tests" -ne 0 ]; then
		display_test_cases
	fi
	if [ "$display_groups" -ne 0 ]; then
		 display_test_groups
	fi
	if [ "$display_numnodes" -ne 0 ]; then
		display_req_nodes
	fi

	# If user only wanted to display stuff...
	if [ "$dont_run" -ne 0 ]; then
		exit 0
	fi

	# Parse the specified test number(s)
	parse_test_numbers $testnums
	if [ $? -ne 0 ]; then
		exit 1
	fi

	# Output directory
	OUTPUTDIR=$UNODE_DIR/$CLUSTERNAME
	mkdir -p $OUTPUTDIR || exit 1

	# Quorum directory
	QD_DIR=$OUTPUTDIR/qd
	mkdir $QD_DIR || exit 1

	# Set the path to temporary quorum info file
	QD_FILE=$OUTPUTDIR/quoruminfo
}


###############################################################################
# Parse user-specified test numbers, which can be numbers and/or ranges
# Returns: 0 if successful, non-zero otherwise.
#
function parse_test_numbers   # [test_number...]
{
	typeset total
	typeset index
	typeset arg
	typeset begin end
	typeset temp

	# First figure out how many tests are defined in fault data file
	total=$(grep -c '^[ 	]*TEST[ 	]' $FAULT_DATAFILE)

	# Reset the RUN_LIST array
	unset RUN_LIST
	index=0
	while (( index <= total )); do
		RUN_LIST[index]=0
		(( index += 1 ))
	done

	# Process the test number arguments, each may be a comma separated list
	# of numbers and/or ranges.  If there were no test numbers specified,
	# run all tests (same as specifying '1-').
	for arg in $(echo ${*:-'1-'} | sed 's/,/ /g'); do
		case $arg in
		+([0-9]))		# a number, e.g. 3
			begin=$arg
			end=$arg
			;;
		+([0-9])-+([0-9]))	# range, e.g. 3-7
			begin=${arg%-*}
			end=${arg#*-}

			# If end number < beginning number, swap them.
			if (( end < begin )); then
				temp=$begin
				begin=$end
				end=$temp
			fi
			;;
		+([0-9])-)		# range, e.g. 3- (3 through last)
			begin=${arg%-}
			end=$total
			;;
		-+([0-9]))		# range, e.g. -3 (1 through 3)
			begin=1
			end=${arg#-}
			;;
		*)
			print -u2 "ERROR: Illegal test number/range: $arg"
			return 1
			;;
		esac

		# Verify 1 <= test numbers <= $total
		if (( begin < 1 )); then
			print -u2 "ERROR: Test number $begin is less than 1"
			return 1
		fi

		if (( end > total )); then
			print -u2 "ERROR: Test number $end is greater than" \
				"the total number of tests ($total)"
			return 1
		fi

		# Mark specified test numbers in RUN_LIST array
		while (( begin <= end )); do
			RUN_LIST[begin]=1
			(( begin += 1 ))
		done
	done

	return 0
}


###############################################################################
# Set the default values of all the global variables
#
function set_default_values   #
{
	# Some array indices
	TEST_INDEX=0
	SETUP_INDEX=0
	CLIENT_INDEX=0
	SERVER_INDEX=0
	SVC_SHUTDOWN_INDEX=0
	QUORUM_INDEX=0
	NUM_SECS_INDEX=0
	PROV_PRI_INDEX=0

	# Set the default values for the global variables
	DEF_UPATH=		# UPATH is empty by default
	DEF_KPATH=		# KPATH is empty by default
	DEF_UNPATH=		# UNPATH is empty by default
	DEF_USER=root		# Scripts run as ROOT by default
	DEF_UFILE=fi_driver	# Binary containing user-level tests
	DEF_KFILE=fi_module	# Module containing user-level tests
	DEF_UNFILE=		# UNODE library is empty

	FAULT_DATAFILE=$DEFAULT_FAULT_DATAFILE

	KEEP_RESULTS=0

	# Don't ignore reboots
	IGNORE_REBOOTS=0

	# Clear the quorum pids list
	QUORUM_PIDS=

	# Don't use xterms by default
	XTERM_DISPLAY=0

	# If an infrastructure file is specified, just generate it once.
	# This value acts like a sentinel allowing UNODE_SETUP to be called
	# exactly once.
	setup_flag=0
}


###############################################################################
# Set argument variables.
#
# CLIENT and SERVER keywords may specify certain variables as arguments to
# be passed to the client or server.  These variables are called
# "argument variables" and are specified in the same manner as shell
# environment variables.  Prior to executing the client/server this script
# substitutes these variables with their corresponding values.
#
# The following argument variables are available:
#
#	NUM_NODES	-- number of nodes in the cluster.
#	NODE_<num>	-- name of node number <num>
#	NODE_<num>_ID	-- node ID of node number <num>.
#	ALL_NODES	-- names of all nodes in the cluster.
#	ALL_NODE_IDS	-- node IDs of all nodes in the cluster.
#	HA_RM_ID	-- node ID of the RM primary node.
#
# Example:
#
#	SERVER	-n NODE_1 -f myserver myserver $NODE_2 $NODE_2_ID
#	CLIENT	-n NODE_3 myclient $ALL_NODES
#
# $NODE_2 and $NODE_2_ID above will be replaced with the name and node ID,
# respectively, of node number 2.  These values are then passed as arguments
# to the server myserver.  The variable $ALL_NODES will be replaced with the
# names of all nodes in the cluster.
#
function set_arg_vars   #
{
	typeset i

	# Since the size of the cluster can change from one test to another,
	# we need to unset some variable arguments from previous cluster first
	for i in $PREV_NODES; do
		eval unset NODE_${i} NODE_${i}_ID
	done

	# Note, $NUM_NODES is set in get_node_names()

	for i in ${NODENAME_LIST[*]}; do
		eval NODE_${i}=${NODENAME_LIST[i]}
		eval NODE_${i}_ID=${NODEID_LIST[i]}
	done

	ALL_NODES=${NODENAME_LIST[*]}
	ALL_NODE_IDS=${NODEID_LIST[*]}

	# Save current nodes
	PREV_NODES=${NODENAME_LIST[*]}

	# Set the node id of RM primary node. RM primary is always on
	# node with highest node id. If this changes, move 2 lines
	# below to end of start_cluster function.
	# unode_load 1 fi_support rm_primary_node > /dev/null

	HA_RM_ID=$NUM_NODES
}


###############################################################################
# Verifies that we can access the utlities for unode
#
function verify_utils   #
{
	typeset prog

	for prog in $CMD_UNODE $CMD_UNODE_LOAD $CMD_QD $CMD_UNODE_SETUP; do
		if [[ ! -x $prog ]]; then
			print -u2 "ERROR: Can't find $prog"
			exit 1
		fi
	done
}


###############################################################################
# Set the names of the nodes for this cluster.
# Note this function simply sets some cluster-related variables whose names
# match those in the "run_test_fi" script.  It is mainly to keep the two
# FI test drivers look similar to ease debugging/enhancements.
#
function get_node_names   #
{
	typeset nodeidx

	# NODENAME_LIST[] contains the names of the nodes in the cluster
	# indexed by node numbers (not node IDs)
	# NODEID_LIST[] contains the node IDs of the nodes in the cluster.
	unset NODENAME_LIST
	unset NODEID_LIST

	# Number of nodes in the cluster
	NUM_NODES=$REQ_NODES		# this is also an argument variable
	: ${NUM_NODES:=0}		# in case $REQ_NODES is blank

	nodeidx=1
	while (( nodeidx <= NUM_NODES )); do
		NODENAME_LIST[nodeidx]=$nodeidx
		NODEID_LIST[nodeidx]=$nodeidx
		(( nodeidx += 1 ))
	done
}


###############################################################################
# Figure out the name of the cluster we'll be creating
#
function set_cluster_name   #
{
	CLUSTERNAME=${LOGINNAME}_$$_unode_fi_tests
}


###############################################################################
# Parse node specifications.  Nodes can be given in the arguments as a
# comma-separated and/or space-separated list.  Nodes can specified as:
#
#	NODE_<num>	-- name of node number <num> (not the node ID).
#	ALL_NODES	-- names of all nodes in the cluster.
#	HA_RM		-- the name of the node that is hosting the HA RM
#
# A node specification can also be prefixed with '!' which means to exclude
# that node:
#
#	!NODE_<num>	-- names of all nodes except node number <num>.
#
# Example:
#
#	!NODE_1,!NODE_3		means all nodes except NODE_1 and NODE_3
#
# The resulting node names will be printed to stdout as a space-separated list.
#
# Returns: 0 if successful, non-zero otherwise.
#
function parse_node_specs   # [node_spec...]
{
	typeset spec nodeidx
	typeset include exclude

	for spec in $(echo "$@" | sed 's/,/ /g'); do
		case $spec in
		NODE_[1-9]*([0-9]))
			nodeidx=${spec#NODE_}
			if (( nodeidx > NUM_NODES )); then
				print -u2 "ERROR: Node $spec is non-existent"
				return 1
			fi

			include[nodeidx]=${NODENAME_LIST[nodeidx]}
			unset exclude[nodeidx]
			;;
		HA_RM)
			# Figure out the nodeid of the HA RM host
			unode_load 1 fi_support rm_primary_node > /dev/null
			nodeidx=$?
			if [[ $nodeidx -le 0 ]]; then
				print -u2 "ERROR: Error getting HA RM primary"
				return 1
			fi
			if (( nodeidx > NUM_NODES )); then
				print -u2 "ERROR: Node $spec is non-existent"
				print -u2 "ERROR: (HA RM Primary Host)"
				return 1
			fi

			include[nodeidx]=${NODENAME_LIST[nodeidx]}
			unset exclude[nodeidx]
			;;

		!NODE_[1-9]*([0-9]))
			nodeidx=${spec#!NODE_}
			if (( nodeidx > NUM_NODES )); then
				print -u2 "ERROR: Node ${spec#!} is" \
					"non-existent"
				return 1
			fi

			unset include[nodeidx]
			exclude[nodeidx]=${NODENAME_LIST[nodeidx]}
			;;

		!HA_RM)
			# Figure out the nodeid of the HA RM host
			unode_load 1 fi_support rm_primary_node > /dev/null
			nodeidx=$?
			if [[ $nodeidx -le 0 ]]; then
				print -u2 "ERROR: Error getting HA RM primary"
				return 1
			fi
			if (( nodeidx > NUM_NODES )); then
				print -u2 "ERROR: Node $spec is non-existent"
				print -u2 "ERROR: (HA RM Primary Host)"
				return 1
			fi
			
			unset include[nodeidx]
			exclude[nodeidx]=${NODENAME_LIST[nodeidx]}
			;;

		ALL_NODES)
			set -A include BOGUS ${NODENAME_LIST[*]}
			unset include[0]		# no node 0
			unset exclude
			;;

		*)
			print -u2 "ERROR: Illegal node specification: $spec"
			return 1
			;;
		esac
	done

	# If there is at least one node to exclude...
	if (( ${#exclude[*]} > 0 )); then
		# Include all nodes first
		set -A include BOGUS ${NODENAME_LIST[*]}	
		unset include[0]			# no node 0
	fi

	# Exclude all nodes in the exclude list from the include list
	nodeidx=1
	while (( nodeidx <= NUM_NODES )); do
		if [ -n "${exclude[nodeidx]}" ]; then
			unset include[nodeidx]
		fi
		(( nodeidx += 1 ))
	done
	echo ${include[*]}			# print resulting nodes

	return 0
}


###############################################################################
# Display the test cases specified in the fault data file
#
function display_test_cases   #
{
	integer testnum
	typeset key val
	typeset title

	print "The following tests are defined:\n"
	testnum=1
	while read key val; do
		[[ "$key" != "TEST" ]] && continue

		# Format output (fold long test descriptions)
		title="   TEST [$testnum] -"
		typeset -R${#title} indent=" "
		{
			echo "$title"
			echo "$indent" $val
		} | fmt -c -w 80
		(( testnum += 1 ))
	done < $FAULT_DATAFILE
	print
}


###############################################################################
# Display test groups specified in the fault data file
#
function display_test_groups   #
{
	print "The following test groups are defined:\n"

	nawk '$1 == "GROUP" {
		for (i = 2; i <= NF; ++i) {
			print "   " $i
		}
	}' $FAULT_DATAFILE | sort -u

	print
}


###############################################################################
# Display required number of tests to run tests in the fault data file.
#
function display_req_nodes   #
{
	nawk '$1 == "REQ_NODES" {
		if ($2 > largest)
			largest = $2
	}
	END {
		print "Required number of nodes to run tests: " largest
	}' $FAULT_DATAFILE
}


###############################################################################
# start_node
# Will start a node of a cluster, background it, and redirect its output
# to a file called output.
# We start each unode node in a separate directory.
# Returns: 0 if successful, non-zero otherwise.
#
function start_node   # <node_number>
{
	typeset nodenum=$1
	typeset pid
	typeset unode_setup_options=""

	# Test log file
	TEST_LOG=$TEST_DIR/node-${nodenum}.log

	pid=${NODEPID[nodenum]}
	if [ -z "$pid" ]; then
		pid=0
	fi

	if [ $pid -ne 0 ]; then
		# Check if this node is already running
		kill -0 ${NODEPID[nodenum]} 2>/dev/null
		if [ $? -eq 0 ]; then
			# This is already running
			print -u2 "ERROR: Node $nodenum is already running."
			return 1
		else
			NODEPID[nodenum]=0
		fi
	fi

	# Create TEST_LOG if neccesary.
	[[ ! -d $TEST_DIR ]] && mkdir -p $TEST_DIR

	$CMD_UNODE $CLUSTERNAME $nodenum 1-$NUM_NODES >$TEST_LOG 2>&1 &
	NODEPID[nodenum]=$!

	if [ "$XTERM_DISPLAY" -ne 0 ] && [ $nodenum -eq 1 ]; then
		/usr/openwin/bin/xterm -title NODE_$nodenum \
			-e tail -f $TEST_LOG &
		XTERM_PID=$!
		sleep 5				# give xterm time to start
	fi

	return 0
}


###############################################################################
# start_cluster
# Will start a complete cluster
# Returns: 0 if successful, -1 otherwise.
#
function start_cluster   #
{
	typeset i

	print " ** Starting all nodes"

	# If an infrastructure file is specified, then run UNODE_SETUP
	# to create the directories and infrastructure file BEFORE
	# executing $CMD_UNODE.  This way the user's infrastructure file
	# won't be clobbered by $CMD_UNODE.
	if [[ $use_template = "yes" ]]; then
		if [[ -s "$QD_FILE" ]]; then
			unode_setup_options="-a $QD_FILE"
		fi

		$CMD_UNODE_SETUP -n $NUM_NODES -c $CLUSTERNAME \
				-i $TEMPLATE $unode_setup_options
		if [ $? -ne 0 ]; then
			return 1
		fi
	fi

	unset LAST_BOOT_COUNT		# for saving each node's boot count
	for i in ${NODENAME_LIST[*]}; do
		start_node $i
		if [ $? -ne 0 ]; then
			return 1
		fi
		LAST_BOOT_COUNT[i]=1	# nodes always start w/ 1 when booting
	done

	# Sleep long enough for the nodes to reach a stable state
	sleep $LOAD_DELAY_TIME

	return 0
}


###############################################################################
# get_boot_count
# The unode process will write out a timestamp onto its timestamp file
# every time it boots/reboots (thus there must be at least one timestamp
# in the file).  This function prints to stdout the number of times the
# specified node has booted/rebooted.  Also returns nonzero if there are
# errors in obtaining the boot count.
# Returns: 0 if successful, non-zero otherwise.
#
function get_boot_count   # <node>
{
	typeset node=$1
	typeset timestamp=$OUTPUTDIR/node-$node/timestamp
	typeset bootcount

	if [ ! -f "$timestamp" ]; then
		print -u2 "ERROR: No reboot time stamp file found for node:" \
			"$node"
		return 1
	fi

	bootcount=$(wc -w $timestamp | awk '{print $1}')

	if (( bootcount == 0 )); then
		print -u2 "ERROR: Node $node is missing its time stamps"
		return 1
	fi

	print $bootcount
	return 0
}


###############################################################################
# kill_nodes
# Stop all nodes in cluster
#
function kill_nodes 
{
	typeset nid
	typeset pid
	typeset dbgdir
	typeset killcmd="kill -KILL"
	typeset waitcmd="wait"

	# construct the kill and wait commands fill killing all nodes in the
	# unode cluster
	nid=$NUM_NODES
	while (( nid > 0 )); do
		pid=${NODEPID[nid]}
		if [ -z "$pid" ]; then
			(( nid -= 1 ))
			continue
		fi
		if [ "$pid" -eq 0 ]; then
			# This node has not been started
			(( nid -= 1 ))
			continue
		else
			# append this to the kill command 
			killcmd="$killcmd $pid"
			waitcmd="$waitcmd $pid"
			(( nid -= 1 ))
		fi
	done

	# execute both commands
	$killcmd 2>/dev/null
	$waitcmd 2>/dev/null

	# wrap up
	nid=$NUM_NODES
	while (( nid > 0 )); do
		pid=${NODEPID[nid]}
		if [ -z "$pid" ]; then
			(( nid -= 1 ))
			continue
		fi 	
		if [ "$pid" -eq 0 ]; then
			# This node has not been started
			(( nid -= 1 ))
			continue
		fi
		NODEPID[nid]=0
		
		# Save the debug buffers if any
		dbgdir=$TEST_DIR/node-$nid.dbg
		mkdir $dbgdir 2>/dev/null
		mv -f $OUTPUTDIR/node-$nid/*.dbg $dbgdir 2>/dev/null
		rmdir $dbgdir 2>/dev/null # in case there were no debug buffers

		# Remove the unode pid/control/timestamp/door files.
		# Don't delete the triggerinfo (Boot triggers) file, however.
		rm -f $OUTPUTDIR/node-$nid/?(pid|control|timestamp|transport)
		(( nid -= 1 ))
	done
}


###############################################################################
# teardown_cluster
# Stop all nodes
#
function teardown_cluster   #
{
	typeset pid
	typeset i
	typeset node

	if [ -z "$REQ_NODES" ]; then
		return
	fi

	print " ** Stopping all nodes"
	kill_nodes

	rm -f $QD_FILE 2>/dev/null
	rm -f $OUTPUTDIR/* 2>/dev/null

	for node in ${NODENAME_LIST[*]}; do
		rm -f $OUTPUTDIR/node-$node/ccr/infrastructure > /dev/null
	done

	# Kill all the qd's
	if [ -n "$QUORUM_PIDS" ]; then
		kill -INT $QUORUM_PIDS 2>/dev/null
		for pid in $QUORUM_PIDS; do
			print "    Waiting for QD to exit (pid: $pid)"
			wait $pid 2>/dev/null
		done
		QUORUM_PIDS=
	fi

	print " ** Stopped all nodes"

	if [ "$XTERM_DISPLAY" -ne 0 ] && [ -n "$XTERM_PID" ]; then
		kill -KILL $XTERM_PID 2>/dev/null
		wait $XTERM_PID 2>/dev/null
	fi
}


###############################################################################
# unode_load
# Loads libs and entry point onto a unode node.
# Returns: the value returned by the entry point, or $LOAD_TIMEOUT_RETVAL
# if the loading hangs and the timer expired.
#
function unode_load   # <node_number> <unode_lib> <function> [function_arg...]
{
	typeset node=$1
	typeset lib=$2
	typeset func=$3
	shift 3

	typeset ret
	typeset timer
	typeset coproc_pid
	typeset nodepid
	typeset pidlist

	print "    Loading: $func with args \"$@\" on node $node"

	# Load the entry point on the node.  We'll run $CMD_UNODE_LOAD as a
	# co-process so we can set a timer in case $CMD_UNODE_LOAD hangs and
	# get its output.
	$CMD_UNODE_LOAD -x $RESULT_PREFIX $CLUSTERNAME $node $lib $func "$@" \
		2>&1 |&
	coproc_pid=$!

	# Set a timer with 1-second sleeps in between
	timer=${TIMEOUT:-$LOAD_TIMEOUT}
	while (( timer > 0 )); do
		kill -0 $coproc_pid 2>/dev/null
		if [ $? -ne 0 ]; then
			break			# $CMD_UNODE_LOAD has returned
		fi
		sleep 1
		(( timer -= 1 ))
	done

	# If timer expired...
	if (( timer == 0 )); then
		print "+++ FAIL: No response from node $node" \
			"after ${TIMEOUT:-$LOAD_TIMEOUT} seconds"
		kill -KILL $coproc_pid 2>/dev/null
		wait $coproc_pid 2>/dev/null

		# Kill all the nodes (CORE THEM)
		pidlist=""
		for nodepid in ${NODEPID[*]}; do
			if [[ $nodepid -ne 0 ]]; then
				pidlist="$pidlist $nodepid"
			fi
		done
		if [[ -n "$pidlist" ]]; then
			print "  * Time-out, forcing core of pids: $pidlist"
			kill -ABRT $pidlist 2>/dev/null
			sleep 3
		fi

		return $LOAD_TIMEOUT_RETVAL
	fi

	# Get exit value of the $CMD_UNODE_LOAD above
	wait $coproc_pid 2>/dev/null
	ret=$?

	if [ $ret -ne 0 ]; then
		cat <&p			# print output of $CMD_UNODE_LOAD
	else
		# Get the returned value of the entry point, which is embedded
		# in the output string of $CMD_UNODE_LOAD
		ret=$(awk '/'$RESULT_PREFIX'/ { print $2 }' <&p)
	fi

	return $ret
}


###############################################################################
# wait_for_node
# Wait for a node to reboot and join the cluster
# Returns: 0 if successful, non-zero otherwise.
#
function wait_for_node # <node>
{
	typeset nid=$1
	typeset rslt=1
	typeset counter=0

	# Test if this node has rejoined the cluster
	print "  * Will wait for node $nid to re-join cluster"

	while [ $rslt -ne 0 ]; do
		(( counter += 1 ))
		if (( counter > 15 )); then
			print "    Still waiting for node $nid"
			counter=0
		fi

		# Check if node is dead
		kill -0 ${NODEPID[$nid]} 2>/dev/null
		rslt=$?
		if [ $IGNORE_REBOOTS -eq 0 ]; then
			if [ $rslt -ne 0 ]; then
				print -u1 "    WARNING: Node $nid was found dead"
				return 1
			fi
		fi

		unode_load $nid orbtest noop >/dev/null
		rslt=$?
		if [ $rslt -eq $LOAD_TIMEOUT_RETVAL ]; then
			print "+++ FAIL: No response from node $nid" \
				"after $LOAD_TIMEOUT seconds"
			return 1		# the node didn't respond
		fi
		if [ $rslt -ne 0 -a $counter -eq 5 ]; then
		    print "+++ FAIL: Unode_load of no-op failed on node $nid"
		    return 1
		fi
	done

	return 0
}


###############################################################################
# reboot_all_nodes
# Reboot all the nodes in the cluster
# Returns: 0 if successful, non-zero otherwise.
#
function reboot_all_nodes
{
	typeset node
	typeset rslt

	for node in ${NODENAME_LIST[*]} END; do
		if [ $node = END ]; then
			print "*** No more nodes to attempt reboot on"
			return 1
		fi

		unode_load $node orbtest reboot_cluster
		if [ $? -eq 0 ]; then
			break
		fi

		print "  * Rebooting from node $nid failed," \
			"will retry from another node"
	done

	# Now wait for all the nodes to return
	for node in ${NODENAME_LIST[*]}; do
		wait_for_node $node
		if [ $? -ne 0 ]; then
			return 1
		fi
	done

	return 0
}

###############################################################################
# set_num_secs
#
#
function set_num_secs   # [service_name] [desired_num_secs]
{
	typeset svc_name=$1
	typeset num_secs=$2
	typeset rslt=0

	print "    Set ${svc_name}'s desired number of secondaries to " \
		"${num_secs}"

	unode_load 1 replctl replctl -n ${svc_name} ${num_secs}
	case $? in
	0)      # changing desired number of secondaries succeeded
		;;
	*)      # changing desired number of secondaries failed
		print " ** Changing ${svc_name}'s desired number of " \
			"secondaries failed"
		rslt=1
		;;
	esac

	return $rslt
}

###############################################################################
# set_prov_pri
#
#
function set_prov_pri   # [service_name] [num_prov] [priority [priority...]]
{
	typeset svc_name
	typeset num_prov
	typeset rslt=0

	str=$1
	num_args=`echo $str | nawk 'BEGIN{FS=":"}{print NF}'`

	# Get the service name from the pass-in string.
	str=$(echo $str | sed 's/:/#/1')
	svc_name=`echo $str | nawk 'BEGIN{FS="#"}{print $1}'`
	str=$(echo $str | sed 's/^.*#//1')

	# Get the number of providers from the pass-in string.
	str=$(echo $str | sed 's/:/#/1')
	num_prov=`echo $str | nawk 'BEGIN{FS="#"}{print $1}'`
	str=$(echo $str | sed 's/^.*#//1')

	# Assign the pass-in priority to the appropriate provider
	# ONLY for provider 1 to provider (num_prov-1).
	#
	# String format: e.g. 1:2:3:4:4:4:4:4
	#
	typeset i=1
	while [ $i -le $num_prov-1 ]
	do
		str=$(echo $str | sed 's/:/#/1')
		priority[${i}]=`echo $str | nawk 'BEGIN{FS="#"}{print $1}'`
		str=$(echo $str | sed 's/^.*#//1')
		((i += 1))
	done

	# Assign the priority to the provider (num_prov)

	priority[${i}]=`echo $str`

	i=1
	while [ $i -le $num_prov ]
	do
		print "    Set the priority of provider $i in $svc_name to "\
			 "${priority[${i}]}"
		unode_load 1 replctl replctl -c $svc_name $i ${priority[${i}]}

		case $? in
		0)      # changing priority of provider succeeded
			;;
		*)      # changing priority of provider failed
			print "Changing the priority of provider $i in " \
				"$svc_name failed"
                        rslt=1
                        ;;
                esac
                ((i += 1))
        done

        return $rslt
}

###############################################################################
# load_server
# Load a server entry point into one or more unodes
# Returns: 0 if successful, non-zero otherwise.
#
function load_server   # [load_option...] [entry [arg...]]
{
	typeset server_nodes
	typeset file=$DEF_UNFILE
	typeset entry
	typeset rslt tmprslt
	typeset node
	typeset opt

	while getopts ':n:m:f:u:' opt; do
		case $opt in
		n)
			# Collect all -n option arguments, each of which
			# could be a comma-separated list of nodes.
			server_nodes="$server_nodes $OPTARG"
			;;
		f)
			file=$OPTARG
			;;
		m|u)
			# Ignore in unode mode
			;;
		\?)
			print -u2 "ERROR: SERVER: Option $OPTARG unknown"
			return 1
			;;
		:)
			print -u2 "ERROR: SERVER: Option $OPTARG" \
				"needs argument"
			return 1
			;;
		*)
			print -u2 "ERROR: SERVER: Option $opt unrecognized"
			return 1
			;;
		esac
	done
	shift 'OPTIND - 1'

	# Resolve all node specifications
	server_nodes=$(parse_node_specs $server_nodes)
	if [ $? -ne 0 ]; then
		return 1
	fi

	# Make sure a node was specified
	if [ -z "$server_nodes" ]; then
		print -u2 "ERROR: SERVER: No node is specified"
		return 1
	fi

	# Check if we need to make the file name a full path
	if [[ "$file" != /* && -n "$DEF_UNPATH" ]]; then
		# Not a full path and $DEF_UNPATH is defined
		file=${DEF_UNPATH}/${file}
	fi

	# If there are arguments left, then the first is assumed to be the
	# name of the entry, and the rest will be passed to the server.
	# Otherwise, the file name will be used as the entry name.
	if (( $# > 0 )); then
		entry=$1
		shift 1
	else
		entry=${file##*/}
	fi

	# Load the server on one or more unodes
	rslt=0
	for node in $server_nodes; do
		unode_load $node $file $entry "$@"
		tmprslt=$?
		if [ $tmprslt -eq $LOAD_TIMEOUT_RETVAL ]; then
			return 1		# the node didn't respond
		fi
		(( rslt |= $tmprslt ))
	done

	if [ $rslt -ne 0 ]; then
		print "+++ FAIL: \"$entry\" failed." \
			"Check log files for messages"
	fi

	return $rslt
}


###############################################################################
# load_client
# Load a client entry point into one or more unodes
# Returns: 0 if successful, non-zero otherwise.
#
function load_client   # [load_option...] [entry [arg...]]
{
	typeset client_nodes
	typeset file=$DEF_UNFILE
	typeset entry
	typeset load_error=0
	typeset down_nodes
	typeset num_clients
	typeset down_clients
	typeset client_pid_list
	typeset pid
	typeset rslt=0
	typeset tmprslt
	typeset opt
	typeset boot_count
	typeset node
	typeset reboot_after_client_load=0

	while getopts ':n:m:f:u:xd:r' opt; do
		case $opt in
		n)
			# Collect all -n option arguments, each of which
			# could be a comma-separated list of nodes.
			client_nodes="$client_nodes $OPTARG"
			;;
		f)
			file=$OPTARG
			;;
		x)
			# This means that we expect a load_error
			load_error=1
			;;
		d)
			# Collect all -d option arguments, each of which
			# could be a comma-separated list of nodes expected
			# to go down
			down_nodes="$down_nodes $OPTARG"
			;;
		r)
			reboot_after_client_load=1
			;;
		m|u)
			# Ignore in unode mode
			;;
		\?)
			print -u2 "ERROR: CLIENT: Option $OPTARG unknown"
			return 1
			;;
		:)
			print -u2 "ERROR: CLIENT: Option $OPTARG" \
				"needs argument"
			return 1
			;;
		*)
			print -u2 "ERROR: CLIENT: Option $opt unrecognized"
			return 1
			;;
		esac
	done
	shift 'OPTIND - 1'

	# Resolve node specifications
	client_nodes=$(parse_node_specs $client_nodes)
	if [ $? -ne 0 ]; then
		return 1
	fi

	down_nodes=$(parse_node_specs $down_nodes)
	if [ $? -ne 0 ]; then
		return 1
	fi

	# Make sure a node was specified
	if [ -z "$client_nodes" ]; then
		print -u2 "ERROR: CLIENT: No node is specified"
		return 1
	fi

	# Check if we need to make the file name a full path
	if [[ "$file" != /* && -n "$DEF_UNPATH" ]]; then
		# Not a full path and $DEF_UNPATH is defined
		file=${DEF_UNPATH}/${file}
	fi

	# If there are arguments left, then the first is assumed to be the
	# name of the entry, and the rest will be passed to the client.
	# Otherwise, the file name will be used as the entry name.
	if (( $# > 0 )); then
		entry=$1
		shift 1
	else
		entry=${file##*/}
	fi

	# For saving list of nodes that go down during this client's run
	unset CURRENT_DOWN

	# Load the client on one or more unodes
	num_clients=0
	for node in $client_nodes; do
		unode_load $node $file $entry "$@" &
		client_pid_list[num_clients]=$!
		(( num_clients += 1 ))
	done

	# Wait for all clients to return and collect their result codes
	rslt=0
	for pid in ${client_pid_list[*]}; do
		wait $pid 2>/dev/null
		tmprslt=$?
		if [ $tmprslt -eq $LOAD_TIMEOUT_RETVAL ]; then
			# The node didn't respond
			kill -KILL ${client_pid_list[*]} 2>/dev/null
			wait ${client_pid_list[*]} 2>/dev/null
			return 1
		fi
		(( rslt |= $tmprslt ))
	done

	# Verify condition of each node
	for node in ${NODENAME_LIST[*]}; do
		# Verify if the unode process is still alive
		kill -0 ${NODEPID[node]} 2>/dev/null
		if [ $? -ne 0 ]; then
			print "  * Found node $node dead"
			CURRENT_DOWN[node]=1
			continue
		fi

		# Try loading the noop routine on the node.  It should fail
		# if the node is in the process of rebooting
		unode_load $node orbtest noop >/dev/null
		case $? in
		0)	;;
		$LOAD_TIMEOUT_RETVAL)	# The node didn't respond
			print "+++ FAIL: No response from node $node" \
				"after $LOAD_TIMEOUT seconds"
			return 1
			;;
		*)	# The node is in the process of rebooting
                        wait_for_node $node
                        if [ $? -ne 0 ]; then
                                return 1
                        fi
			;;
		esac

		# Get the current boot count
		boot_count=$(get_boot_count $node)
		if [ $? -ne 0 ]; then
			return 1
		fi

		# If the node has rebooted
		if (( boot_count > LAST_BOOT_COUNT[node] )); then
			CURRENT_DOWN[node]=1
		fi

		# Remember the current boot count
		LAST_BOOT_COUNT[node]=$boot_count
	done

	# Determine which client nodes had gone down
	for node in $client_nodes; do
		if [ "${CURRENT_DOWN[node]}" -ne 0 ]; then
			down_clients="$down_clients $node"
		fi
	done

	# If the client(s) were expected to fail...
	if [ $load_error -ne 0 ]; then
		if [ $rslt -eq 0 -a -z "$down_clients" ]; then
			print "+++ FAIL: Expected error during load" \
				"did not occur"
			rslt=1
		else
			print " ** Load failure was expected"
			rslt=0
		fi
	else
		# If any client node died...
		if [ $IGNORE_REBOOTS -eq 0 ]; then
			if [ -n "$down_clients" ]; then
				print "+++ FAIL: client node(s)" $down_clients \
					"unexpectedly died/rebooted"
				rslt=1
			fi
		fi
		if [ $rslt -ne 0 ]; then
			print "+++ FAIL: \"$entry\" failed." \
				"Check log files for messages"
		fi
	fi

	# Verify nodes expected to go down were really down
	check_down_nodes $down_nodes
	if [ $? -ne 0 ]; then
		rslt=1
	fi

	# Now see if we have to reboot all the nodes
	if [ "$reboot_after_client_load" -ne 0 ]; then
		reboot_all_nodes
		if [ $? -ne 0 ]; then
			# XXX - Should we abort the testing
			rslt=1
		fi
	fi

	return $rslt
}


###############################################################################
# setup_cluster
# Load neccesary libs when cluster is first brought up
# Returns: 0 if successful, non-zero otherwise.
#
function setup_cluster   #
{
	typeset index=1

	while (( index <= SETUP_INDEX )); do
		eval load_server "${SETUP_LIST[index]}"
		if [ $? -ne 0 ]; then
			return 1
		fi

		(( index += 1 ))
	done

	return 0
}


###############################################################################
# svc_shutdown
# Will shutdown ha services
# Returns: 0 if successful, non-zero otherwise.
#
function svc_shutdown   #
{
	typeset index=1

	while (( index <= SVC_SHUTDOWN_INDEX )); do
		eval service_shutdown "${SVC_SHUTDOWN_LIST[index]}"
		if [ $? -ne 0 ]; then
			return 1
		fi
		(( index += 1 ))
	done

	return 0
}


###############################################################################
# setup_quorum
# Setup the files and processes to do quorum testing (SCSI-3 Simulation)
# Returns: 0 if successful, non-zero otherwise.
#
function setup_quorum_device   # <qd_num> <qd_votes> <qd_def...>
{
	typeset qd_number=$1
	typeset qd_votes=$2
	typeset qd_name=$3
	shift 3
	typeset qd_defs=$@

	typeset qd_path
	typeset qd_arg
	typeset qd_properties
	typeset qd_line
	typeset qd_pid
	integer node=0

	qd_properties="cluster.quorum_devices.$qd_number.properties"

	# Write the vote count out to the config file
	qd_line="$qd_properties.votecount\t$qd_votes"
	echo $qd_line >> $QD_FILE

	# Write the global device name out to the config file
	qd_line="$qd_properties.gdevname\t$QD_DIR/$qd_name"
	echo $qd_line >> $QD_FILE

	# Setup the quorum line for the qd program argument
	for qd_path in $qd_defs; do
		node=node+1
		if [ "$qd_path" != 0 ]; then
			qd_arg="$qd_arg $QD_DIR/$qd_name"
			qd_line="$qd_properties.path_$node\tenabled"
			echo $qd_line >> $QD_FILE
		else
			qd_arg="$qd_arg NONE"
		fi
	done

	# Start up the qd process
	$CMD_QD $qd_arg > $QD_DIR/test-${TEST_INDEX}.${qd_number} 2>&1 &
	qd_pid=$!
	QUORUM_PIDS="$QUORUM_PIDS $qd_pid"

	print " ** Configured QD with $qd_votes vote(s)"

	# Copy the quoruminfo file for debugging purposes
	cp $QD_FILE $QD_DIR/test-${TEST_INDEX}.quoruminfo

	# Make sure that all the QUORUM PIDS are running
	if [ -z "$QUORUM_PIDS" ]; then
		print "+++ FAIL: No quorum processes were started!?"
		return 1
	fi

	# Wait for QD process to be started
	until kill -0 $qd_pid 2>/dev/null; do
		print "    Waiting for QD with pid $qd_pid to start"
		sleep 1
	done

	return 0
}


###############################################################################
# setup_quorum
# Calls setup_quorum_device for each QD line
# Returns: 0 if successful, non-zero otherwise.
#
function setup_quorum   #
{
	typeset quorum_index=1

	while (( quorum_index <= QUORUM_INDEX )); do
		eval setup_quorum_device $quorum_index \
			"${QUORUM_LIST[quorum_index]}"
		if [ $? -ne 0 ]; then
			return 1
		fi
		(( quorum_index += 1 ))
	done

	sync
	return 0
}


###############################################################################
# service_shutdown
# Shuts down each service (using the new and improved replctl)
# Returns: 0 if successful, non-zero otherwise.
#
function service_shutdown   # [service...]
{
	typeset svc_list=$@
	typeset svc_name

	print " ** Shutting down HA services"

	for svc_name in $svc_list; do
		unode_load 1 replctl replctl -x $svc_name
		if [ $? -ne 0 ]; then
			print "+++ FAIL: Shutdown of service $svc_name failed"
			return 1
		fi
	done

	return 0
}


###############################################################################
# check_for_core
# Verify that none of the node dumped a core file (sign of trouble)
# Returns: 0 if NO core files were found, otherwise nonzero.
#
function check_for_core   #
{
	typeset node
	typeset old new
	typeset retval=0

	print " ** Checking for core dumps"

	for node in ${NODENAME_LIST[*]}; do
		old=$OUTPUTDIR/node-$node/core
		new=$TEST_DIR/node-${node}.core

		if [ -s $old ]; then
			mv $old $new		# rename the core
			print "+++ FAIL: Core found for node $node"
			print "    Core renamed to $new"
			retval=1
		fi
		rmdir ${old%/*} 2>/dev/null
	done

	return $retval
}


###############################################################################
# check_down_nodes
# Verifies that only the nodes that we specifiy DOWN are down
# Returns: 0 if successful, non-zero otherwise.
#
function check_down_nodes   # [node ...]
{
	typeset nodepid
	typeset expected_list
	typeset node
	typeset rslt=0

	if (( $# > 0 )); then
		print " ** Verifying only node(s) $@ went down"

		for node in "$@"; do
			expected_list[node]=1
		done
	fi

	# Verify that only nodes user expects to be down did go down
	for node in ${NODENAME_LIST[*]}; do
		if [ "${expected_list[node]}" -ne 0 ]; then
			if [ "${CURRENT_DOWN[node]}" -eq 0 ]; then
				print "+++ FAIL: Node $node stayed up!" \
					"Expected to go down"
				rslt=1
			fi
		else
			if [ $IGNORE_REBOOTS -eq 0 ]; then
				if [ "${CURRENT_DOWN[node]}" -ne 0 ]; then
					print "+++ FAIL: Node $node went down!" \
						"Expected to stay up"
					rslt=1
				fi
			fi
		fi
	done

	return $rslt
}


###############################################################################
# set_test_sequence
#
# Processes fault data file.
# Returns: 0 if successful, non-zero otherwise.
#
function set_test_sequence   #
{
	typeset parsing_context=GLOBAL
	typeset keyword value
	typeset line_count=0

	while read keyword value; do
		(( line_count += 1 ))

		# Ignore blank and comment lines
		[[ -z "$keyword" || "$keyword" = \#* ]] && continue

		# Delete all newlines (for lines that end with '\')
		value=$(echo "$value" | tr -d "\n")

		case $keyword in
		#
		# Global Variable
		#
		DEF_UPATH)
			DEF_UPATH=$value
			;;
		DEF_KPATH)
			DEF_KPATH=$value
			;;
		DEF_UNPATH)
			DEF_UNPATH=$value
			;;
		DEF_USER)
			DEF_USER=$value
			;;
		DEF_UFILE)
			DEF_UFILE=$value
			;;
		DEF_KFILE)
			DEF_KFILE=$value
			;;
		DEF_UNFILE)
			DEF_UNFILE=$value
			;;

		#
		# Cluster SETUP
		#
		SETUP)
			parsing_context=SETUP
			;;
		COMMAND)
			;;

		#
		# Test case def
		#
		TEST)
			(( TEST_INDEX += 1 ))
			TEST[TEST_INDEX]=$value
			parsing_context=TEST
			;;
		CLIENT)
			case $parsing_context in
			GLOBAL|SETUP)
				print -u2 "ERROR: line $line_count:" \
					"\"$keyword\" must be used in TEST" \
					"context ($parsing_context)"
				return 1
				;;
			*)
				(( CLIENT_INDEX += 1 ))
				CLIENT_LIST[CLIENT_INDEX]=$value
				;;
			esac
			;;
		SERVER)
			case $parsing_context in
			GLOBAL)
				print -u2 "ERROR: line $line_count:" \
					"\"$keyword\" must be used in SETUP" \
					"or TEST context ($parsing_context)"
				return 1
				;;
			SETUP)
				(( SETUP_INDEX += 1 ))
				SETUP_LIST[SETUP_INDEX]=$value
				;;
			*)
				(( SERVER_INDEX += 1 ))
				SERVER_LIST[SERVER_INDEX]=$value
				;;
			esac
			;;
		SKIP)
			SKIP=$value
			;;
		GROUP)
			GROUP=$value
			;;
		SVC_SHUTDOWN)
			(( SVC_SHUTDOWN_INDEX += 1 ))
			SVC_SHUTDOWN_LIST[SVC_SHUTDOWN_INDEX]=$value
			;;
		QUORUM_DEVICE)
			(( QUORUM_INDEX += 1 ))
			QUORUM_LIST[QUORUM_INDEX]=$value
			;;
		REQ_NODES)
			if [[ total_nodes_defined -eq 0 ]]; then
				# Strips the -x option for unode
				REQ_NODES=`echo $value | nawk \
					'{sub(/-x[ \t]*/, ""); print}'`
			else
				# Overwrite the number of cluster nodes
				# by input from command line
				REQ_NODES=$TOTAL_NODES
			fi
			;;
		IGNORE_REBOOTS)
			IGNORE_REBOOTS=$value
			;;
		USE_TEMPLATE)
			TEMPLATE=$value
			if [[ $overwrite_use_template = "yes" ]]; then
				TEMPLATE=$NEW_USE_TEMPLATE
			fi
			use_template=yes
			if [[ ! -f $TEMPLATE ]]; then
				print "Cannot find $TEMPLATE"
				exit 1
			fi
			;;
		REBOOT_AFTER)
			REBOOT_AFTER=REBOOT_AFTER
			;;
		TIMEOUT)
			TIMEOUT=$value
			;;
		NUM_SECS)
			(( NUM_SECS_INDEX += 1))
			NUM_SECS_LIST[NUM_SECS_INDEX]=$value
			;;
		PROV_PRI)
			(( PROV_PRI_INDEX += 1))
			PROV_PRI_LIST[PROV_PRI_INDEX]=$value
			;;
		#
		# Marker for END of test case definition
		END)
			# Where test log files are kept
			TEST_DIR=$OUTPUTDIR/test-${TEST_INDEX}

			# This simply sets some cluster-related variables whose
			# names match those in the "run_test_fi" script.  It is
			# mainly to keep the two FI test drivers look similar
			# to ease debugging/enhancements.
			get_node_names

			# Set argument variables
			set_arg_vars

			#
			# Run the test
			#
			execute_test

			case ${TEST_RESULTS[TEST_INDEX]} in
			''|SKIPPED)
				;;
			*)
				teardown_cluster
				check_for_core
				if [ $? -ne 0 ]; then
					# Found core.  Mark test as failed.
					TEST_RESULTS[TEST_INDEX]=FAIL
					TEST_CORES[TEST_INDEX]=1
				fi
				;;
			esac

			#
			# Remove test directory if the test passed and
			# user doesn't want to keep it.
			#
			if [[ "${TEST_RESULTS[TEST_INDEX]}" = PASS &&
			   $KEEP_RESULTS -eq 0 ]]; then
				print " ** Removing test directory $TEST_DIR"
				rm -rf $TEST_DIR 2>/dev/null
			fi

			#
			# Reset values
			#
			parsing_context=GLOBAL
			SKIP=
			GROUP=
			REQ_NODES=
			REBOOT_AFTER=
			IGNORE_REBOOTS=0
			TIMEOUT=
			TEMPLATE=
			use_template=no

			unset CLIENT_LIST
			CLIENT_INDEX=0

			unset SERVER_LIST
			SERVER_INDEX=0

			unset SVC_SHUTDOWN_LIST
			SVC_SHUTDOWN_INDEX=0

			unset QUORUM_LIST
			QUORUM_INDEX=0

			unset NUM_SECS_LIST
			NUM_SECS_INDEX=0

			unset PROV_PRI_LIST
			PROV_PRI_INDEX=0
			;;
		esac
	done < $FAULT_DATAFILE

	return 0
}


###############################################################################
# Checks that the group for this test matches the groups specified
# on the command line (i.e. execute this test).
# Returns: 0 if the group matches (i.e. execute the test), non-zero otherwise.
#
function check_group   # [group...]
{
	typeset group_list=$@
	typeset a_group b_group

	# It's considered a match if user didn't specify any group
	[ -z "$SELECTED_GROUPS" ] && return 0

	for a_group in $SELECTED_GROUPS; do
		# Recurse through the groups specified by the user
		for b_group in $group_list; do
			# Recurse through the groups in the TEST def
			if [ "$a_group" = "$b_group" ]; then
				return 0	# found a match (run test)
			fi
		done
	done

	return 1				# no match (don't run test)
}


###############################################################################
# Check if we need to skip this test
# Returns: 0 - test is to continue.
#          1 - test is skipped.
#          2 - an error occured.
#
function check_skip   # [-r <remark>] [mode...]
{
	typeset remark="Not supported in UNODE mode"
	typeset remark16="Not supported in 16 UNODE mode"
	typeset skip_modes
	typeset a_mode
	typeset opt

	while getopts ':r:' opt; do
		case $opt in
		r)
			remark=$OPTARG
			;;
		\?)
			print -u2 "ERROR: SKIP: Option $OPTARG unknown"
			return 2
			;;
		:)
			print -u2 "ERROR: SKIP: Option $OPTARG needs argument"
			return 2
			;;
		*)
			print -u2 "ERROR: SKIP: Option $opt unrecognized"
			return 2
			;;
		esac
	done
	shift 'OPTIND - 1'

	# The rest of the arguments are skip modes.
	for a_mode in "$@"; do
		if [ "$a_mode" = UN ]; then
			SKIP_REMARK[TEST_INDEX]=$remark
			print " ** Skipping test due to: $remark"
			return 1			# skip test
		fi

		if [ "$a_mode" = UN16 ] && [ $total_nodes_defined -eq 1 ] && \
			[ $TOTAL_NODES -eq 16 ]; then
			SKIP_REMARK[TEST_INDEX]=$remark16
			print " ** Skipping test due to: $remark16"
			return 1			# skip test
		fi
	done

	return 0					# continue test
}


###############################################################################
# load_all_servers
# Run all test servers
# Returns: 0 if successful, non-zero otherwise.
#
function load_all_servers   #
{
	typeset index=1

	while (( index <= SERVER_INDEX )); do
		eval load_server "${SERVER_LIST[index]}"
		if [ $? -ne 0 ]; then
			return 1
		fi
		(( index += 1 ))
	done
	return 0
}


###############################################################################
# load_all_clients
# Run all test clients one by one.  They perform the actual testing.
# Returns: 0 if successful, non-zero otherwise.
#
function load_all_clients   #
{
	typeset rslt=0
	typeset index=1

	while (( index <= CLIENT_INDEX )); do
		eval load_client "${CLIENT_LIST[index]}"
		if [ $? -ne 0 ]; then
			# Note, even if the client returns a failure, we'll
			# continue with the next one, because other clients
			# might do other tests or some clean up procedures
			rslt=1
		fi
		(( index += 1 ))
	done
	return $rslt
}

###############################################################################
# set_all_num_secs
# Set desired number of secondaries for each service one by one.
# Returns: 0 if successful, non-zero otherwise.
#
function set_all_num_secs   #
{
	typeset index=1

	while (( index <= NUM_SECS_INDEX )); do
		eval set_num_secs "${NUM_SECS_LIST[index]}"
		if [ $? -ne 0 ]; then
			return 1
		fi
		(( index += 1 ))
	done
	return 0
}

###############################################################################
# set_all_prov_pri
# Set the priority of all the providers within each service one by one.
# Returns: 0 if successful, non-zero otherwise.
#
function set_all_prov_pri   #
{
	typeset index=1
	while (( index <= PROV_PRI_INDEX )); do
		# Concatenate the list of providers into a string.
		str=$(echo ${PROV_PRI_LIST[index]} | sed 's/ /:/g')

		# Pass in the provider string to the function for
		# processing.
		eval set_prov_pri $str
		if [ $? -ne 0 ]; then
			return 1
		fi
		(( index += 1 ))
	done
	return 0
}

###############################################################################
# execute_test
#
# Executes a test and sets TEST_RESULTS[TEST_INDEX].
# Returns: non-zero if a test failure/error occured.
#
function execute_test   #
{
	typeset rslt=0

	#
	# If this test is not in the $RUN_LIST, don't run it
	#
	if [ ${RUN_LIST[TEST_INDEX]} -eq 0 ]; then
		TEST_RESULTS[TEST_INDEX]=
		return 0
	fi

	#
	# Check the GROUP for this test, versus the GROUP selected
	# If there's no match, ignore this test
	#
	eval check_group "$GROUP"
	if [ $? -ne 0 ]; then
		TEST_RESULTS[TEST_INDEX]=
		return 0
	fi

	print

	# Format test description (fold long ones)
	typeset title="*** TEST [$TEST_INDEX] -"
	typeset -R${#title} indent=" "
	{
		echo "$title"
		echo "$indent" ${TEST[TEST_INDEX]}
	} | fmt -c -w 80

	print " ** TEST [$TEST_INDEX] - NODES=$REQ_NODES GROUP=$GROUP" \
		"$REBOOT_AFTER"

	#
	# Check if we should skip this test
	#
	eval check_skip "$SKIP"
	case $? in
	0)
		# Test continues.
		;;
	1)
		# Test is skipped.
		TEST_RESULTS[TEST_INDEX]=SKIPPED
		return 0
		;;
	*)
		# An error occured.
		TEST_RESULTS[TEST_INDEX]=FAIL
		return 1
		;;
	esac

	#
	# Set the global REQ_NODES to the number of required nodes
	#
	if [ -z "$REQ_NODES" ]; then
		print -u2 "ERROR: REQ_NODES missing from test entry:" \
			"${TEST[TEST_INDEX]}"
		TEST_RESULTS[TEST_INDEX]=FAIL
		return 1
	fi

	#
	# Create test directory where test log files will be kept
	#
	mkdir -p $TEST_DIR
	if [ $? -ne 0 ]; then
		TEST_RESULTS[TEST_INDEX]=FAIL
		return 1
	fi

	#
	# Set up quorum
	#
	setup_quorum
	if [ $? -ne 0 ]; then
		TEST_RESULTS[TEST_INDEX]=FAIL
		return 1
	fi

	#
	# Bring the cluster up
	#
	start_cluster
	if [ $? -ne 0 ]; then
		TEST_RESULTS[TEST_INDEX]=FAIL
		return 1
	fi

	#
	# Do setup as defined in the fault data file
	#
	setup_cluster
	if [ $? -ne 0 ]; then
		TEST_RESULTS[TEST_INDEX]=FAIL
		return 1
	fi

	#
	# Load servers
	#
	load_all_servers
	if [ $? -ne 0 ]; then
		TEST_RESULTS[TEST_INDEX]=FAIL
		return 1
	fi

	set_all_prov_pri
	if [ $? -ne 0 ]; then
		TEST_RESULTS[TEST_INDEX]=FAIL
		return 1
	fi

	#
	# Set desired number of secondaries
	#
	set_all_num_secs
	if [ $? -ne 0 ]; then
		TEST_RESULTS[TEST_INDEX]=FAIL
		return 1
	fi

	#
	# Load clients (they perform the actual testing), one by one
	# After each client is loaded, and completes we will wait for
	# the nodes to reboot.  (and rejoin the cluster)
	#
	load_all_clients
	if [ $? -ne 0 ]; then
		rslt=1
	fi

	#
	# Shutdown services
	#
	svc_shutdown
	if [ $? -ne 0 ]; then
		rslt=1
	fi

	if (( rslt == 0 )); then
		TEST_RESULTS[TEST_INDEX]=PASS
	else
		TEST_RESULTS[TEST_INDEX]=FAIL
	fi
	return $rslt
}


###############################################################################
# print_info
# Prints info about what we're about to do
#
function print_info   #
{
	print "Fault Injection Tests."
	print -n "======================================="
	print    "======================================="
	print "Output will be saved in directory: $OUTPUTDIR"
}


###############################################################################
# print_results
# Prints info about what transpired
#
function print_results   #
{
	typeset -R4 tnum
	typeset -L10 tresult
	typeset tdesc
	typeset total_fail=0
	typeset total_pass=0
	typeset total_skipped=0
	typeset index

	print
	print -n "======================================="
	print    "======================================="
	print " ** Results for Fault Injection Tests"
	print "NOTE: In case of failure the test description will be printed"
	print "NOTE: Output has been saved at $OUTPUTDIR"
	print

	tnum=TEST
	tresult=RESULT
	tdesc=DESCRIPTION
	print "$tnum   $tresult   $tdesc"

	index=1
	while (( index <= TEST_INDEX )); do
		case ${TEST_RESULTS[index]} in
		PASS)
			tnum=$index
			tresult=PASS
			tdesc=
			(( total_pass += 1 ))
			;;
		FAIL)
			tnum=$index
			if [[ -n "${TEST_CORES[index]}" ]]; then
				tresult="FAIL/CORE"
			else
				tresult=FAIL
			fi
			tdesc=${TEST[index]}
			(( total_fail += 1 ))
			;;
		SKIPPED)
			tnum=$index
			tresult=SKIPPED
			tdesc=${SKIP_REMARK[index]}
			(( total_skipped += 1 ))
			;;
		*)
			(( index += 1 ))
			continue
			;;
		esac

		print "$tnum   $tresult   $tdesc"
		(( index += 1 ))
	done

	if (( ${#TEST_CORES[*]} > 0 )); then
		print
		print -n "TESTS WITH CORE FILES:"
		index=1
		while (( index <= TEST_INDEX )); do
			if [[ -n "${TEST_CORES[index]}" ]]; then
				print -n " $index"
			fi
			(( index += 1 ))
		done
		print
	fi

	print
	print -n "TOTALS:   FAIL $total_fail   PASS $total_pass   "
	print    "SKIPPED $total_skipped"

	print -n "======================================="
	print    "======================================="

	# If there was no failure, delete the output directory
	if (( total_fail == 0 )); then
		if [ $KEEP_RESULTS -eq 0 ]; then
			print "*** No test failures detected," \
				"removing $OUTPUTDIR/"
			rm -rf $OUTPUTDIR 2>/dev/null
		else
			print "*** Keeping results directory $OUTPUTDIR"
			print "    (Please remove it when you're done.)"
		fi
	else
		print "*** Test failures detected, examine $OUTPUTDIR/"
		print "    (Please remove it when you're done.)"
	fi
}


###############################################################################
# kill_jobs
# Terminate all background jobs
# 
function kill_jobs   #
{
	typeset pid

	for pid in $(jobs -p); do
		kill -KILL -$pid 2>/dev/null	# terminate entire process group
	done
	wait					# leave no zombies
}


###############################################################################
# handle_broken_pipe
# Magic trick to handle the case where this script is in a pipeline, e.g.
#
#	run_unode_fi | tee mylogfile
#
# and the user hits ^C.  In this case, "tee" would be terminated before this
# script finishes its EXIT trap.  If any function called by the trap prints
# to stdout (or stderr if it's also redirected to the pipe), this script would
# be terminated with SIGPIPE since there is no longer a reader on the pipe.
#
# Note, we can't use a global PIPE trap since ksh resets all traps prior
# to executing a function.  We could use a PIPE trap for every function
# called in the EXIT trap, but that would be too much bookkeeping.  Using
# a single function like this is much easier.
# 
function handle_broken_pipe   #
{
	GOT_SIGPIPE=0
	trap 'GOT_SIGPIPE=1' PIPE

	# Test whether we really need to redirect stdout/stderr due to SIGPIPE
	print "\nCleaning up. Please wait..."
	sleep 1				# give time for SIGPIPE to be delivered

	if (( ! GOT_SIGPIPE )); then
		return			# no need to do redirections
	fi

	# Redirect stdout if it's a pipe (we must use /dev/fd/n since
	# 'test -p' takes a pathname)
	if [[ -p /dev/fd/1 ]]; then
		# Try print to /dev/tty.  If succeeds, redirect stdout to
		# /dev/tty, else to /dev/null (e.g. script's run under cron).
		# Note, below 2>/dev/null must be placed before >/dev/tty,
		# otherwise ksh might not filter out error message from print.
		print "\nCleaning up. Please wait..." 2>/dev/null >/dev/tty
		if [ $? -eq 0 ]; then
			exec >/dev/tty
		else
			exec >/dev/null
		fi
	fi

	# Redirect stderr to stdout if it's a pipe
	if [[ -p /dev/fd/2 ]]; then
		exec 2>&1
	fi
}


###############################################################################
# Main Execution
#

# Setup Routines
verify_utils
set_cluster_name
set_default_values
examine_command_line "$@"

ON_EXIT=handle_broken_pipe
trap 'eval "$ON_EXIT"' EXIT

# Print info about the tests we're going to run
ON_EXIT="$ON_EXIT; print_results"	# print results of tests run so far
print_info

# Set up the test_sequence
ON_EXIT="$ON_EXIT; teardown_cluster; kill_jobs"
set_test_sequence

exit $?
