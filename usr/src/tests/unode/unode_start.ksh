#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)unode_start.ksh	1.13	08/05/20 SMI"
#


PROGDIR=$(dirname $0)		# directory where this program resides
if [ "$PROGDIR" = "." ]; then
	PROGDIR=$PWD
fi
readonly PROGDIR

USAGE="Usage: $0 <cluster_name> <node_id> ..."

# Make sure we have path to xterm and 'unode' which should be located in the
# same directory as this script
export PATH=$(dirname $0):/usr/openwin/bin:$PATH

# Check number of arguments.
if (( $# < 2 ))
then
	print -u2 "$USAGE"
	exit 1
fi
CLUSTER_NAME=$1
shift

# Make sure $CLUSTER_NAME is not blank
if ! test $CLUSTER_NAME
then
	print -u2 "$USAGE"
	exit 1
fi

# Need to source this file in to obtain unode options from "rc" file.
. $PROGDIR/unode_config

# Cluster info file
CINFOFILE=$UNODE_DIR/$CLUSTER_NAME/clusterinfo

# Make sure we can read the cluster info file $CINFOFILE
if [[ ! -r $CINFOFILE ]]
then
	print -u2 "ERROR: Couldn't read $CINFOFILE"
	exit 1
fi

# Get the number of nodes from the cluster info file.
NUMNODES=$(< $CINFOFILE)

# Check the validity of each specified node id
for NID in $*
do
	if (( $(expr "$NID" : '[0-9]*') != ${#NID} ))
	then
		print -u2 "ERROR: Invalid node id $NID"
		exit 1
	fi
	if (( NID < 1 ))
	then
		print -u2 "ERROR: Node id $NID must be > 1"
		exit 1
	fi
	if (( NID > NUMNODES ))
	then
		print -u2 "ERROR: Node id $NID > number of nodes in cluster" \
			"($NUMNODES)"
		exit 1
	fi
done


# xterm window geometries (to make them similar to those started by mcconsole)
XTERM_SIZE=80x24
set -A XTERM_POS --	\
	dummy		\
	+600+50		\
	+100+200	\
	+100+545	\
	+600+200	\
	+600+545
XTERM_POS_LEN=${#XTERM_POS[*]}

# Start an xterm window for each specified node
for NID in $*
do
	XTERM_TITLE="$CLUSTER_NAME $NID"

	if (( NID < XTERM_POS_LEN ))
	then
		XTERM_POS_IDX=$NID
	else
		XTERM_POS_IDX=$(( (NID - 2) % (XTERM_POS_LEN - 2) + 2 ))
	fi

	print "Starting node $NID of $NUMNODES in cluster $CLUSTER_NAME"

	xterm -name UNode \
		-geom "${XTERM_SIZE}${XTERM_POS[$XTERM_POS_IDX]}" \
		-n "$XTERM_TITLE" \
		-title "unode: $XTERM_TITLE" \
		-e unode $CLUSTER_NAME $NID 1-$NUMNODES &
	sleep 1
done
