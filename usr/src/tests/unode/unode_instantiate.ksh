#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)unode_instantiate.ksh	1.11	08/05/20 SMI"
#


# Takes a template of an infrastructure file and instantiates lines that 
# contain a key word.  A key word starts with a '%' and must be replaced
# with an integer that represents the instance.  For example, if there are
# 3 nodes, then each line that contains the key word '%nodeid' will be
# instantiated 3 times, once each for 1,2,3.  
#
# Usage:
# 	unode_instantiate <template_file> <num_nodes> <cluster_name>
#
# By default, only three arguments are mandatory: clustername, number of nodes,
# and the template file.  Other parameters are optional.  If optional 
# parameters are omitted, the default is given as follows: 
# 	The number of blackboxes and of ports are fixed to 2. 
#	The number of adapters is twice the number of nodes. 
#	The number of cables equals number of nodes times number of adapters. 

PROGDIR=$(dirname $0)		# directory where this program resides
if [ "$PROGDIR" = "." ]; then
	PROGDIR=$PWD
fi
readonly PROGDIR

# Need to source this file in to obtain unode options from "rc" file.
. $PROGDIR/unode_config


function usage
{
	print -u2 "Usage: $0 -C clustername -n num_nodes -f template_file"
	print -u2 "          [-a num_adapters ] [ -b num_black_boxes ]"
	print -u2 "          [ -c num_cables ] [ -p num_ports ]"
}
  
while getopts :a:b:c:C:f:n:p: OPT
do
    case $OPT in
    a )
	NUMADAPTERS=$OPTARG
	;;
    b ) 
	NUMBBOXES=$OPTARG
	;;
    c ) 
	NUMCABLES=$OPTARG
	;;
    C ) 
	CLUSTERNAME=$OPTARG
	;;
    f )
	TEMPLATE=$OPTARG
	;;
    n ) 
	NUMNODES=$OPTARG 
	;;
    p )
	NUMPORTS=$OPTARG
	;;
    * ) 
	print -u2 "Bad option $OPT."
	usage
	;;
    esac
done

# Validate certain variables.
: ${CLUSTERNAME:?"Cluster requires a name. Use -C option"}
: ${TEMPLATE:?"Requires a file. Use -f option"}
: ${NUMNODES:?"Specify number of nodes in cluster. Use -n option"}

# Set default values to certain variables.
: ${NUMADAPTERS:=$(( 2 * NUMNODES ))}
: ${NUMBBOXES:=2}
: ${NUMPORTS:=2}
: ${NUMCABLES:=$(( NUMNODES * NUMADAPTERS ))}

# Process the template.
/bin/nawk \
	-v CLUSTERNAME=$CLUSTERNAME \
	-v UNODE_DIR=$UNODE_DIR \
	-v TEMPLATE=$TEMPLATE \
	-v NUMNODES=$NUMNODES \
	-v NUMADAPTERS=$NUMADAPTERS \
	-v NUMBBOXES=$NUMBBOXES \
	-v NUMPORTS=$NUMPORTS \
	-v NUMCABLES=$NUMCABLES \
'
BEGIN {
	adapter_instance = 0
	node_instance = 0
}

# Only output the line if it has all its key words replaced.
function output(line)
{
	if (line !~ /%/) {
		gsub(" ", "\t", line)
		print line
		return 0
	}
	return 1
}

# Returns the correct adapter instance.  This format is dictated by the 
# format of the infrastructure file.  By design there are 4 adapters for 
# every cluster.  So for a 5 node cluster, there are 20 cables   
# (cables=2*nodes*adapters).  Each cable is associated with an adapter -
# back to our example, cables 1-5 correspond to adapter 1, cables 6-10 to
# adapter 2, cables 11-15 to adapter 3, cables 16-20 to adapter 4.
function get_adapter_instant(cablenum)
{
	return int(1 + (cablenum - 1) / NUMNODES)
}

# Returns the correct node instance.  This format is dictated by the format
# of the infrastructure file.  A cable connects a nodes adapter
# to a blackbox.  
function get_node_instant(node_instance)
{
	if (++node_instance > NUMNODES)  {
		node_instance = 1
	}
	return node_instance
}

function get_bbox_instant(bbox_instance)
{
	--bbox_instance
	return get_adapter_instant(bbox_instance)
}

# Alternately returns 1 or 2.
function get_port_instant(port_instance)
{
	return get_node_instant(port_instance)
}

function instantiate_cables()
{
	cable_instant = 1
	node_instant = 0
	adapter_instant = 1
	port_instant = 0
	bbox_instant = 1
	numcables = 1
	numnodes = 1
	numports = 1
	numadapters = 1
	numbboxes = 1

	while (cable_instant <= NUMCABLES) {
		node_instant = get_node_instant(node_instant)
		adapter_instant = get_adapter_instant(cable_instant)

		newline1 = $0
		gsub("%cableid", cable_instant, newline1)
		gsub("%nodeid", node_instant, newline1)
		gsub("%adapterid", adapter_instant, newline1)

		++cable_instant
		if (output(newline1) == 0) {
			continue
		}

		bbox_instant = get_bbox_instant(cable_instant)
		port_instant = get_port_instant(port_instant)

		newline2 = newline1
		gsub("%bboxid", bbox_instant, newline2)
		gsub("%portid", port_instant, newline2)

		output(newline2)
	}
}

function instantiate_bboxes()
{
	bbox_instant = 1
	port_instant = 1

	while (bbox_instant <= NUMBBOXES) {
		newline = $0
		gsub("%bboxid", bbox_instant, newline)
		++bbox_instant
		if (output(newline) == 0) {
			continue
		}
		while (port_instant <= NUMNODES) {
			newline2 = newline
			gsub("%portid", port_instant, newline2)
			++port_instant
			output(newline2)
		}
		port_instant = 1
	}
	bbox_instant = 1
}

function instantiate_nodes()
{
	node_instant = 1
	adapter_instant = 1

	while (node_instant <= NUMNODES) {
		newline = $0
		gsub("%nodeid", node_instant, newline)
		++node_instant
		if (output(newline) == 0) {
			continue
		}
		while (adapter_instant <= NUMADAPTERS) {
			newline2 = newline
			gsub("%adapterid", adapter_instant, newline2)
			++adapter_instant
			if (output(newline2) == 0) {
				continue
			}

			newline3 = newline2
			gsub("%unode_dir", UNODE_DIR, newline3)
			if (output(newline3) != 0) {
				print "NOT PRINTED:" newline3
			}
		}
		adapter_instant = 1
	}
	node_instant = 1
}

# XXX - REMOVE THIS FUNCTION AFTER ROOT DEPENDENCY GOES AWAY. - XXX
function instantiate_quorum_vote()
{
	node_instant = 2

	while (node_instant <= NUMNODES) {
		newline1 = $0
		gsub("%nodeid", node_instant, newline1)
		++node_instant
		output(newline1)
	}
}

# Create the quorum_resv_key to have a length of 16 characters 
function instantiate_quorum_resv_key()
{
	node_instant = 1
	while (node_instant <= NUMNODES) {
		key = sprintf("%016x", node_instant)
		newline1 = $0
		sub("%nodeid", node_instant, newline1)
		sub("%nodeid", key, newline1)
		++node_instant
		output(newline1)
	}
} 
     
function instantiate_line()
{
	# XXX - REMOVE AFTER ROOT NODE GOES AWAY!!! - XXX
	# Case 0: For root node dependency, we need to make the root node 
	# have quorum_vote of 1, and all other nodes have a vote of 0.
	if ($0 ~ /^cluster.nodes.%nodeid.properties.quorum_vote/) {
		instantiate_quorum_vote()
		return
	}

	# Special case to generate quorum_resv_key number.
	# This has to be a number that is 16 spaces wide, prepended with
	# zeroes at the beginning.
	if ($0 ~ /^cluster.nodes.%nodeid.properties.quorum_resv_key/) {
		instantiate_quorum_resv_key()
		return
	}
 
	# Case 1: Node properties.
	if ($0 ~ /^cluster.nodes/) {
		instantiate_nodes()
		return
	} 

	# Case 2: Blackbox properties.
	if ($0 ~ /^cluster.blackboxes/) {
		instantiate_bboxes()
		return
	}

	# Case 3: Cable properties.
	if ($0 ~ /^cluster.cables/) {
		instantiate_cables()
		return
	}

	# Case 4: Cluster name.
}

{
	gsub("%clustername", CLUSTERNAME)
	if (output($0) != 0) {
		instantiate_line()
	}
}
' $TEMPLATE
