#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)run_unode.ksh	1.33	08/05/20 SMI"
#

#
# A simple script to automatically start UNODE nodes (displayed in xterm
# windows), start the repl tests.
# At the end, the user is prompted whether to kill the nodes or not.
# This is useful for a quick kernel ORB putback testing in user land.
#
# This crude test will make use of a test-matrix which is a crude method
# of specifying what nodes we want to kill and re-start.
# The format of the test matrix is an array with members like:
#	K# or S# or KI# or KV# SI# or T#
# K# means Kill node number and sleep for a few seconds to reconfig
# S# means Start node number #
# KI# means Kill and dont wait for reconfiguration (This is useful
#	when you want to kill several nodes before reconfig occurs)
# KV# means kill and verify that a switchover occurred.
# S# means Start node and dont wait for reconfiguration
# T# means a switchover to node #.. where the node 0 is a random node
# TI# means a switchover to node #.. where the node 0 is a random node, with no
#	verification
#
# The test completes each instruction and sleeps for a enough time for
# the unode cluster to reconfig, then checks for any cores, (A sign
# that a node has failed).
#
# NOTE: Do not use variables named UNODE_* in this script.  These variable
#	names are reserved and used by unode.
#

PROGDIR=$(dirname $0)			# directory where this program resides
if [ "$PROGDIR" = "." ]
then
	PROGDIR=`pwd`
fi
readonly PROGDIR

# Template used as infrastructure file instead of generating it.
TEMPLATE=
use_template=no

# Default test sequences.
readonly ALL_TESTS='1 2 3 4 5 6 7 8'	# all tests
readonly PUTBACK_TESTS='1 2 3 5 6'	# putback tests only


CLIENT_NODE=1				# Node to run the client on
					# Usually the root node
TEST_THREADS=2				# number of threads in test


readonly SERVER_SO="repl_test"		# Unode shared object for server
readonly CLIENT_SO="repl_test"		# Unode shared object for client
readonly RMPROBE_SO="rm_probe"		# Unode shared object for rm probe
readonly SERVER_ENTRY="server"		# Server's entry point on the so
readonly CLIENT_ENTRY="start_test_thread"	# Clients entry point on the so
readonly RMPROBE_ENTRY="rm_probe"	# rm probe's entry on its so

RMPROBE_PID=0				# This is the pid of the rm_probe

# Figure out where the unode utilities are
if [ $ROOT ]
then
	# defined
	readonly BINDIR_UNODE=$ROOT/usr/unode/bin
	readonly LIBDIR_UNODE=$ROOT/usr/unode/lib
else
	# Relative to the $PROGDIR directory.
	readonly BINDIR_UNODE=$PROGDIR/../bin
	readonly LIBDIR_UNODE=$PROGDIR/../lib
fi

# Need to source this file in to obtain unode options from "rc" file.
. $BINDIR_UNODE/unode_config

# unode utilities we're going to use.
readonly CMD_UNODE_CLUSTER=$BINDIR_UNODE/unode_cluster
readonly CMD_UNODE_LOAD=$BINDIR_UNODE/unode_load
readonly CMD_UNODE_KILL=$BINDIR_UNODE/unode_kill
readonly CMD_UNODE=$BINDIR_UNODE/unode
readonly CMD_UNODE_SETUP=$BINDIR_UNODE/unode_setup

export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$LIBDIR_UNODE"

# Other binaries we need
readonly XTERM=/usr/openwin/bin/xterm

# Make sure we can find all the unode utilities.
for PROG in $CMD_UNODE_CLUSTER $CMD_UNODE_LOAD $CMD_UNODE_KILL $XTERM
do
	if [[ ! -x $PROG ]]
	then
		print -u2 "Can't find $PROG"
		exit 1
	fi
done

# What's my login name?
: ${LOGNAME:=$(logname)}

# Prefix string used by unode to print function result codes
readonly RESULT_PREFIX='UNODE_REPL_TESTS'

# Timeout for loading a routine on a unode
readonly LOAD_TIMEOUT=240		# 4 minutes
readonly LOAD_TIMEOUT_RETVAL=242	# value returned by unode_load() if
					# loading timeout occurred

set -o monitor


#
# Usage
#
function usage
{
	cat << EOF
usage:	run_unode [-p]
	run_unode [-t <test-seq>] [-c client_node] [-o dir] [-x] [-i infrastructure_file]

	-p : runs tests $PUTBACK_TESTS for putback criteria

Sequence 1 kills all the servers, starting at node 2 to 4.
Sequence 2 Kills all the servers, starting at node 2 to 4 then
	brings them all back and makes them primaries, by
	forcing a switchover
Sequence 3 Kills all the non-primary nodes and brings them back
	while keeping the client and server on the same node(root)
Sequence 4 Kills nodes 2, and them node 3 without waiting for
	reconfig (bringing node 2 back inmediately)
Sequence 5 Kills the primary and brings it back during reconfig
Sequence 6 Runs the switchover scenario - 15 consecutive with
	verification.
Sequence 7 Runs the switchover scenario - 15 consecutive no
	verification

EOF
}

#
# Examine the command line
#
function examine_command_line
{
	typeset opt
	typeset putback_test=0

	# Initialize some global variables
	XTERM_DISPLAY=""

	while getopts ':c:o:i:t:px?' opt
	do
		case $opt in
		o)
			# Override $UNODE_DIR.
			UNODE_DIR=$OPTARG
			;;
		c)
			CLIENT_NODE=$OPTARG
			;;
		t)
			# Allow multiple tests.
			TEST_SEQUENCE="$TEST_SEQUENCE $OPTARG"
			;;
		i)	
			# Specify a infrastructure file to use.
			TEMPLATE=$OPTARG
			use_template=yes
			[[ ! -f $TEMPLATE ]] && { echo "Cannot find $TEMPLATE "; exit 1 ; }
			;;
		p)
			putback_test=1
			;;
		x)
			XTERM_DISPLAY=1
			;;
		\?)
			if [[ "$OPTARG" = '?' ]]; then
				usage
				exit 0
			else
				print -u2 "ERROR: Option $OPTARG unknown"
				exit 1
			fi
			;;
		:)
			print -u2 "ERROR: Option $OPTARG needs argument"
			exit 1
			;;
		*)
			print -u2 "ERROR: Option $opt unrecognized"
			exit 1
			;;
		esac
	done

	# If user wants to run putback tests only...
	if (( putback_test ))
	then
		TEST_SEQUENCE=$PUTBACK_TESTS
	elif [ -z "$TEST_SEQUENCE" ]
	then
		# By default, run all tests
		TEST_SEQUENCE=$ALL_TESTS
	fi
}

#
# unode_load
#
# Searches for the return value from the unode_load and returns that.
#
function unode_load
{
	typeset node=$1
	typeset lib=$2
	typeset func=$3
	shift 3

	typeset ret
	typeset timer
	typeset coproc_pid
	typeset nodepid
	typeset pidlist

	# Load the entry point on the node.  We'll run $CMD_UNODE_LOAD as a
	# co-process so we can set a timer in case $CMD_UNODE_LOAD hangs and
	# get its output.
	$CMD_UNODE_LOAD -x $RESULT_PREFIX $CLUSTERNAME $node $lib $func "$@" \
		2>&1 |&
	coproc_pid=$!

	# Set a timer with 1-second sleeps in between
	timer=$LOAD_TIMEOUT
	while (( timer > 0 ))
	do
		kill -0 $coproc_pid 2>/dev/null
		if [ $? -ne 0 ]
		then
			break			# $CMD_UNODE_LOAD has returned
		fi
		sleep 1
		(( timer -= 1 ))
	done

	# If timer expired...
	if (( timer == 0 ))
	then
		print "+++ FAIL: No response from node $node" \
			"after $LOAD_TIMEOUT seconds"
		kill -KILL $coproc_pid 2>/dev/null
		wait $coproc_pid 2>/dev/null

		# Kill all the nodes (CORE THEM)
		pidlist=
		for nodepid in ${NODEPID[*]}; do
			if [[ $nodepid -ne 0 ]]; then
				pidlist="$pidlist $nodepid"
			fi
		done
		if [[ -n "$pidlist" ]]; then
			print "  * Time-out, forcing core of pids: $pidlist"
			kill -ABRT $pidlist 2>/dev/null
			sleep 3
		fi

		return $LOAD_TIMEOUT_RETVAL
	fi

	# Get exit value of the $CMD_UNODE_LOAD above
	wait $coproc_pid 2>/dev/null
	ret=$?

	if [ $ret -ne 0 ]; then
		cat <&p 		# print output of $CMD_UNODE_LOAD
	else
		# Get the returned value of the entry point, which is embedded
		# in the output string of $CMD_UNODE_LOAD
		ret=$(awk '/'$RESULT_PREFIX'/ { print $2 }' <&p)
	fi

	return $ret
}



# start_node will start a unode in the background and redirect its output
# to a file caled output
function start_node
{
	typeset NODENUM=$1
	typeset RET
	typeset PID
	typeset OUTPUT=$OUTPUTDIR/node-$NODENUM.output

	PID=${NODEPID[$NODENUM]}
	if [ -z "$PID" ]; then
		PID=0
	fi

	# Only check if the PID is not already 0
	if [ $PID -ne 0 ]; then
		# Check if this node is already running
		kill -0 $PID 2>/dev/null
		if [ $? -eq 0 ]; then
			# Node is already running
			print -u2 "+++ ERROR: Node $NODENUM is already running"
			return 1
		fi
	fi

	# Sleep for 5 minutes on boot to settle things
	$CMD_UNODE $CLUSTERNAME $NODENUM 1-$NUM_NODES 5 >> $OUTPUT 2>&1 &
	PID=$!
	RET=$?
	NODEPID[$NODENUM]=$PID

	return $RET
}

#
# Functions to start cluster
# This will start cluster of NUMNODES
function start_cluster
{
	typeset I

	print " ** Starting cluster $CLUSTERNAME"

	I=1

	[[ $use_template = yes ]] && \
	$CMD_UNODE_SETUP -n $NUM_NODES -c $CLUSTERNAME -i $TEMPLATE

	while [ $I -le $NUM_NODES ]
	do
		start_node $I
		(( I += 1 ))
	done

	if [ -n "$XTERM_DISPLAY" ]; then
		typeset OUTPUT=$OUTPUTDIR/node-1.output

		$XTERM -title NODE_1 -e tail -f $OUTPUT &
		XTERM_PIDS=$!
	fi

	# Sleep to let the cluster reach a stable state
	sleep 3
}

#
# function to start the test_thread (this is the client)
# Will begin the test thread of the $CLIENT_NODE
function start_test_thread
{
	print " ** Starting client on node $CLIENT_NODE"
	unode_load $CLIENT_NODE $CLIENT_SO $CLIENT_ENTRY
	return $?
}


#
# start server on a node
# Will start a server on the node specified
function start_server
{
	typeset NODEID=$1

	print "    Starting replica server on node $NODEID"
	unode_load $NODEID $CLIENT_SO $SERVER_ENTRY
	return $?
}


#
# star the server on a number of nodes
#
function run_server
{
	typeset I
	typeset ret

	I=2
	while [ $I -le $NUM_NODES ]
	do
		start_server $I
		ret=$?
		if [ $ret -ne 0 ]
		then
			return $ret
		fi
		(( I += 1 ))
	done

	# Start the server on the root node last
	start_server 1
}

#
# kill a node
#
function kill_node
{
	typeset NODENUM=$1
	typeset PID

	PID=${NODEPID[$NODENUM]}
	if [ -z "$PID" ]
	then
		return 0
	fi

	if [ $PID -eq 0 ]
	then
		# This node is dead
		return 1
	fi

	kill -KILL $PID 2>/dev/null
	wait $PID 2>/dev/null

	# Set PID to 0
	NODEPID[$NODENUM]=0
	return 0
}


#
# bring the whole cluster down
# Kill all the nodes
#
function teardown_cluster
{
	typeset I=$NUM_NODES

	if [ -z $I ]; then
		return 0
	fi

	print " ** Tearing down clusters..."

	while [ $I -gt 0 ]
	do
		kill_node $I
		(( I -= 1 ))
	done

	NUM_NODES=

	# Stop the XTERMS
	if [ -n "$XTERM_PIDS" ]; then
		kill -KILL $XTERM_PIDS 2>/dev/null
		wait $XTERM_PIDS 2>/dev/null
		XTERM_PIDS=
	fi
}


#
# Start the probe that will verify the switchovers
# The probe takes the following parameters
# -t 60 : 60 second timeout
# -p 2  : poll every 2 seconds
# -s "ha test server" : service name is "ha test server"
# verify-switchover : verify that the primary changes
#
function start_verify_switch_probe
{
	$CMD_UNODE_LOAD $CLUSTERNAME $1 $RMPROBE_SO $RMPROBE_ENTRY -t 60 -p 2 \
		-s "ha test server" verify-switchover >/dev/null 2>&1 &
	RMPROBE_PID=$!

	# Race condition with the probe starting
	sleep 5
}

# Trigger switchovers
function switchover
{
	set -A DESC_A 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16

	typeset NODE=$1
	typeset INDEX
	typeset DESC=""

	if [ $NODE -ne 0 ]
	then
		DESC="-p"
		(( INDEX = $NODE - 1 ))
		DESC="$DESC ${DESC_A[$INDEX]}"
	fi

	unode_load 1 repl_test switchover $DESC
	return $?
}

# Run replctl
function run_replctl
{
	unode_load 1 replctl replctl
	return $?
}


# look for core files from all nodes
function check_for_core
{
	typeset I=1
	typeset COREDIR
	typeset RESULT=0

	while [ $I -le $NUM_NODES ]
	do
		COREDIR=$OUTPUTDIR/node-$I

		if [ -s "$COREDIR/core" ]
		then
			print "ERROR: core found in $COREDIR"
			RESULT=1
		fi

		(( I += 1 ))
	done

	return $RESULT
}

# Set the test matrix
function set_test_matrix
{
	# S	: Start node
	# SI	: Start, dont wait for reconfig
	# K	: Kill node
	# KI	: Kill dont wait for reconfig
	# KV	: Kill and verify switchover
	# T	: Switchover

	#
	# $TEST signals which test sequence we want to run
	#
	# Sequence 1 kills all the servers, starting at node 2 to
	#	NUM_NODES-1
	# Sequence 2 Kills all the servers, starting at node 2 to
	#	NUM_NODES-1 then brings them all back, and Makes them primary
	#	as it brings them back
	# Sequence 3 Kills all the non-primary nodes and brings them back,
	#	Witht the client and PRIMARY on the same node (1)
	# Sequence 4 Kills primary nodes, and the next primary without
	#	waiting for reconfig (bringing node 2 back inmediately).
	# Sequence 5 Kills the primary and brings it back during reconfig
	# Sequence 6 Is a sequence of switchovers with verification
	# Sequence 7 is a sequence of switchovers without verification

	# EXPECTED_SWITCHOVER
	# This is the # of times that we expect the rm
	# to switch over
	# This number should include the switchover when the
	# Server first goes into execution (So it looks like its 1 too many

	case $TEST in
	1)
		set -A TEST_MATRIX KV2 KV3 KV4
		EXPECTED_SWITCHOVERS=3
		NUM_NODES=4
		;;
	2)
		set -A TEST_MATRIX KV2 KV3 KV4 S4 T4 S3 T3 S2 T2
		EXPECTED_SWITCHOVERS=6
		NUM_NODES=4
		;;
	3)
		set -A TEST_MATRIX T1 K2 S2 K3 S3 K4 S4
		EXPECTED_SWITCHOVERS=1
		NUM_NODES=4
		;;
	4)
		set -A TEST_MATRIX KI2 K3 S2
		EXPECTED_SWITCHOVERS=1
		# NOTE: This test confuses the rm_probes, therefore when i
		# examine the results, i modify the count of the rm_probed
		# switchovers by 1
		print "NOTE: This sequence is failing intermittently due to "\
			"bugid 4183005"
		NUM_NODES=4
		;;
	5)
		set -A TEST_MATRIX KI2 S2
		EXPECTED_SWITCHOVERS=1
		NUM_NODES=4
		;;
	6)
		set -A TEST_MATRIX T1 T2 T3 T4 T0 T0 T0 T0 T0 T0 T0 T0 T0 T0 T0
		EXPECTED_SWITCHOVERS=15
		NUM_NODES=4
		;;
	7)
		set -A TEST_MATRIX TI1 TI2 TI3 TI4 TI0 TI0 TI0 TI0 TI0 TI0 \
			TI0 TI0 TI0 TI0 TI0
		EXPECTED_SWITCHOVERS=15
		print "NOTE: This sequence yields correct results 99% of" \
			"the time, so if you fail, make sure that you check" \
			"the output!"
		NUM_NODES=4
		;;
	8)	set -A TEST_MATRIX  KV2 KV3 KV4 KV5 KV6 KV7 KV8 KV9 KV10 KV11 \
			KV12 KV13 KV14 KV15 KV16
		NUM_NODES=16
		EXPECTED_SWITCHOVERS=15
		XTERM_DISPLAY=1
		;;
	*)
		print "ERROR: You've specified an unsupported test sequence"
		usage
		exit 3
		;;
	esac
}


function execute_test_matrix
{
	typeset ACTION
	typeset NODE
	typeset VERIFY=1
	typeset SLEEP

	for INSTR in "${TEST_MATRIX[@]}"
	do
		#
		# Run replctl to show what is currently running
		#
		run_replctl || return 1

		ACTION=""

		case $INSTR in
		K*)
			case ${INSTR#K} in
			I*)
				# Kill and dont verify
				VERIFY=0
				SLEEP=0
				NODE=${INSTR#KI}
			;;
			V*)
				# Kill with verify
				VERIFY=1
				NODE=${INSTR#KV}
			;;
			*)
				# Kill with sleep
				VERIFY=0
				NODE=${INSTR#K}
				SLEEP=5
			;;
			esac

			if [ $NODE -eq $CLIENT_NODE ]; then
				print "WARNING: About to kill client node"
			fi

			start_verify_switch_probe $CLIENT_NODE

			print " ** Killing node $NODE"
			kill_node $NODE
			if [ $? -ne 0 ]; then
				print "ERROR: Node $NODE was already dead"
				return 1
			fi

			if [ $VERIFY -eq 1 ]
			then
				print "  * Waiting for switchover"
				wait $RMPROBE_PID
			else
				print "    Will not wait for switchover"
				sleep $SLEEP
			fi
		;;

		S*)
			case ${INSTR#S} in
			I*)
				NODE=${INSTR#SI}
				VERIFY=0
				SLEEP=5
			;;
			*)
				NODE=${INSTR#S}
				VERIFY=1
				SLEEP=15
			;;
			esac

			print " ** Starting node $NODE"
			start_node $NODE
			if [ $? -ne 0 ]
			then
				print "ERROR: Node $NODE failed to come up"
				return 1
			fi

			# Let node come up
			sleep 5

			if [ $SLEEP -gt 5 ]
			then
				sleep $SLEEP
				print "  * Waiting for cluster to reconfig"
			else
				sleep $SLEEP
				print "    Will not wait for reconfig"
			fi

			start_server $NODE || return 1
			sleep 3

			if [ $NODE -eq $CLIENT_NODE ]
			then
				print "  * Re-starting client on node" \
					"$CLIENT_NODE"
				start_test_thread || return 1
			fi
		;;

		T*)
			case ${INSTR#T} in
			I*)
				NODE=${INSTR#TI}
				VERIFY=0
			;;
			*)
				NODE=${INSTR#T}
				VERIFY=1
			;;
			esac

			start_verify_switch_probe $CLIENT_NODE

			if [ $NODE -eq 0 ]
			then
				print " ** Triggering a random switchover"
			else
				print " ** Triggering a switchover to node" \
					"$NODE"
			fi

			switchover $NODE
			if [ $? -ne 0 ]
			then
				print "ERROR: Switchover failed"
				return 1
			fi

			sleep 3

			if [ $VERIFY -ne 0 ]; then
				print "  * Verifying switchover"
				wait $RMPROBE_PID
			else
				print "  * Will not verify switchover"
			fi
		;;

		*)
			print "ERROR: Bad entry in test_matrix (Test Bug)"
			ACTION="NOOP"
		;;
		esac

		if [ "$ACTION" != "NOOP" ]
		then
			check_for_core
			if [ $? -ne 0 ]
			then
				return 1
			fi
		fi
	done

	return 0
}

#
# Compare the results to theoretical results
#
function examine_results
{
	typeset OUTPUT
	typeset ACT_EXP_SW
	typeset PASS

	print " ** Examining Results"

	# Give time for everything to finish
	sleep 5

	PASS=0
	ACT_EXP_SW=`expr $EXPECTED_SWITCHOVERS \* $TEST_THREADS`

	OUTPUT=$OUTPUTDIR/node-$CLIENT_NODE.output

	SWITCH_COUNT=`grep -c "Changed server replicas" $OUTPUT`
	RM_SWITCH_COUNT=`grep -c "rm_probe: SUCCESS" $OUTPUT`

	# if This is sequence 4, adjust the RM_SWITCH_COUNT
	if [ $TEST -eq 4 ]
	then
		(( RM_SWITCH_COUNT -= 1 ))
	fi

	print
	print "*** Results for Test: $TEST"
	print " ** Output can be found at: $OUTPUTDIR"
	print " ** Number of expected switchovers = $EXPECTED_SWITCHOVERS" \
			 "* $TEST_THREADS (Test threads) = $ACT_EXP_SW"
	print " ** Switch count from client threads = $SWITCH_COUNT"

	if [ $SWITCH_COUNT -ne $ACT_EXP_SW ]
	then
		print "+++ FAIL: Expected switchovers do not match!"
		PASS=1
	else
		print "    PASS: Expected switchovers match"
	fi

	print " ** Switch count from RM PROBE = $RM_SWITCH_COUNT"
	if [ $RM_SWITCH_COUNT -ne $EXPECTED_SWITCHOVERS ]
	then
		print "+++ FAIL: Count from RM_PROBE does not match" \
			"expected count!"
		PASS=1
	else
		print "    PASS: Count from RM_PROBE matches expected count"
	fi

	print
	return $PASS
}


###############################################################################
# handle_broken_pipe
# Magic trick to handle the case where this script is in a pipeline, e.g.
#
#	run_unode | tee mylogfile
#
# and the user hits ^C.  In this case, "tee" would be terminated before this
# script finishes its EXIT trap.  If any function called by the trap prints
# to stdout (or stderr if it's also redirected to the pipe), this script would
# be terminated with SIGPIPE since there is no longer a reader on the pipe.
#
# Note, we can't use a global PIPE trap since ksh resets all traps prior
# to executing a function.  We could use a PIPE trap for every function
# called in the EXIT trap, but that would be too much bookkeeping.  Using
# a single function like this is much easier.
#
function handle_broken_pipe   #
{
	GOT_SIGPIPE=0
	trap 'GOT_SIGPIPE=1' PIPE

	# Test whether we really need to redirect stdout/stderr due to SIGPIPE
	print "\nCleaning up. Please wait..."
	sleep 1 			# give time for SIGPIPE to be delivered

	if (( ! GOT_SIGPIPE )); then
		return			# no need to do redirections
	fi

	# Redirect stdout if it's a pipe (we must use /dev/fd/n since
	# 'test -p' takes a pathname)
	if [[ -p /dev/fd/1 ]]; then
		# Try print to /dev/tty.  If succeeds, redirect stdout to
		# /dev/tty, else to /dev/null (e.g. script's run under cron).
		# Note, below 2>/dev/null must be placed before >/dev/tty,
		# otherwise ksh might not filter out error message from print.
		print "\nCleaning up. Please wait..." 2>/dev/null >/dev/tty
		if [ $? -eq 0 ]; then
			exec >/dev/tty
		else
			exec >/dev/null
		fi
	fi

	# Redirect stderr to stdout if it's a pipe
	if [[ -p /dev/fd/2 ]]; then
		exec 2>&1
	fi
}


#
# Do it.
#

examine_command_line "$@"

trap 'handle_broken_pipe; teardown_cluster' EXIT

TOTAL_PASS=0
TOTAL_FAIL=0

for TEST in $TEST_SEQUENCE
do
	# Make up cluster name.
	CLUSTERNAME=${LOGNAME}_$$_repl_test_$TEST

	# Output directory
	OUTPUTDIR=$UNODE_DIR/$CLUSTERNAME
	mkdir -p $OUTPUTDIR || exit 1

	# Print some information
	print
	print "=========================================================="
	print " ** Test: $TEST"
	print " ** Results Dir: $OUTPUTDIR"
	print " ** NOTE: If tests fail, this directory will NOT be removed."
	print

	# Set up the test matrix.
	set_test_matrix

	# Start all the nodes in the cluster
	start_cluster
	sleep 5				# give cluster time to initialize

	run_server
	if [ $? -ne 0 ]
	then
		(( TOTAL_FAIL += 1 ))
		print "*** Examine files under $OUTPUTDIR for failure"
		teardown_cluster
		continue
	fi

	start_test_thread
	if [ $? -ne 0 ]
	then
		(( TOTAL_FAIL += 1 ))
		print "*** Examine files under $OUTPUTDIR for failure"
		teardown_cluster
		continue
	fi

	sleep 5		# give client/server time to register with name server

	execute_test_matrix
	if [ $? -ne 0 ]
	then
		(( TOTAL_FAIL += 1 ))
		print "*** Examine files under $OUTPUTDIR for failure"
		teardown_cluster
		continue
	fi

	sleep 1		# give the client time to detect the latest switchover

	examine_results
	if [ $? -ne 0 ]
	then
		(( TOTAL_FAIL += 1 ))
		print "*** Examine files under $OUTPUTDIR for failure"
		teardown_cluster
		continue
	fi

	(( TOTAL_PASS += 1 ))
	teardown_cluster
	rm -rf $OUTPUTDIR
done

print
print "=========================================================="
print "TOTALS:    FAIL $TOTAL_FAIL   PASS $TOTAL_PASS"
print "=========================================================="

exit 0
