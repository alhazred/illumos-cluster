#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)kmod_starter.ksh	1.12	08/05/20 SMI"
#

#
# This script starts a reference stress entity kernel module.
# The first argument specifies the entity.
# The second argument is the id number of the entity.
# To tell the entity what its id number is a simple trick is used:
# a symbolic link to the module code is placed in /tmp.  The name of this link
# (which will become the module's module name) ends with the id number.  The
# module parses this name to obtain the id.
# This script is to be executed remotely by the "run_test" script.
#
# Usage: $0 <entity> <id>
#
# Items with "@@" markers will be substituted by make.
#

readonly PROGNAME=${0##*/}

# The base install test directory.
readonly DIR="@@PROGS_DIR@@"

readonly ENTITY=$1
readonly ID=$2

# Determine if we're running 64-bit kernel or not.
if [[ $(isainfo -vk) = 64* ]]
then
	# 64-bit: use kernel arch for pathname component to kernel modules.
	readonly KERNEL_ARCH=$(isainfo -k)
else
	# 32-bit: no kernel arch component.
	readonly KERNEL_ARCH=
fi

# The modules.
readonly KCLIENT=$DIR/kernel/$KERNEL_ARCH/ref_stress_client
readonly KSERVER=$DIR/kernel/$KERNEL_ARCH/ref_stress_server
readonly KRSRC_BAL=$DIR/kernel/$KERNEL_ARCH/ref_stress_rsrc_balancer

# The base directory where we're going to create symlink to the kernel module.
readonly LINK_BASEDIR=/tmp/refstress.$$
trap 'rm -rf $LINK_BASEDIR' EXIT		# remove it upon exit

# The directory where we're going to create the symlink.
#
# NOTE: On 64-bit systems, we must have a directory component with the
#	kernel arch name since modload will automatically prepend it to the
#	filename component of its supplied path.
#
readonly LINK_DIR=$LINK_BASEDIR/$KERNEL_ARCH
mkdir -p $LINK_DIR 2>/dev/null

# Create symbolic link to the kernel module.
#
# NOTE: This is a hackish and cheapie way to pass information to the kernel
#	module without having to use ioctl() -- once loaded the registered
#	module name will be be the symlink's, and that name contains the
#	passed information.
#
case $ENTITY in
client)
	readonly LINK_FILE=refstress.$$.c.$ID
	ln -sf $KCLIENT $LINK_DIR/$LINK_FILE
	;;
server)
	readonly LINK_FILE=refstress.$$.s.$ID
	ln -sf $KSERVER $LINK_DIR/$LINK_FILE
	;;
rsrc_balancer)
	readonly LINK_FILE=refstress.$$.r.$ID
	ln -sf $KRSRC_BAL $LINK_DIR/$LINK_FILE
	;;
esac

# Now load the module using the symbolic link.  Have modload execute
# /bin/echo to get the module id; this is more precise than searching
# the output of modinfo for the module id.
#
# NOTE: On 64-bit systems, modload always prepends the kernel arch to the
#	filename component of its supplied path.  Thus if our symlink is
#	/tmp/foo/sparcv9/bar, we must call 'modload /tmp/foo/bar'
#
RETSTR=$(modload -e /bin/echo $LINK_BASEDIR/$LINK_FILE 2>&1)
STATUS=$?
if (( STATUS == 0 ))
then
	# Get the module id from /bin/echo's output.
	RETSTR=$(echo $RETSTR | nawk '{print $1}')
fi

# Pass modload's status and the module's id (or modload's error message if
# it failed) back to "run_test" script.
echo $STATUS $RETSTR

exit $STATUS
