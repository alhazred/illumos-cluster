#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)run_test.ksh	1.25	08/05/20 SMI"
#

#
# This script starts and stops a process/module on a remote node.
# The driver program is started as a co-process by and communicates with
# this script.
# The driver tells this script which process/module to start/stop and on
# which node.
#
# Items with "@@" markers will be substituted by make.
#

#
# Set a few ksh parameters, in case user has them set/unset in the $ENV file
#
ulimit -c unlimited		# size of core dumps
ulimit -f unlimited		# max size of files written
ulimit -v unlimited		# size of virtual memory

set +o allexport		# don't automatically export vars
set +o errexit			# don't exit if commands return nonzero
set +o keyword			# only use variable assignments before commands
set +o markdirs			# don't put trailing / on directory names
set -o monitor			# so bg jobs run with own PGID
set +o noclobber		# allow clobber
set +o noglob			# enable file name globbing
set -o nolog			# don't save functions in history file
set +o notify			# don't notify bg job completion
set +o nounset			# allow use of unset variables


readonly PROGNAME=${0##*/}

# Where test programs/scripts are.
readonly DIR="@@PROGS_DIR@@"

# Some utilities we need.
readonly MC_INFO=/usr/cluster/orbtest/fi_support/cluster_info
readonly HATIMERUN=/usr/cluster/bin/hatimerun
readonly HATIMERUN_TIMEOUT=100
readonly CLINFO=/usr/sbin/clinfo
readonly RSH='rsh -n -l root'
readonly RSH_USED="$HATIMERUN -t $HATIMERUN_TIMEOUT $RSH"

# Script to start kernel module
readonly KMOD_STARTER=$DIR/kmod_starter

readonly DRIVER=$DIR/driver			# the driver program

readonly UCLIENT=$DIR/ref_stress_client		# test client program
readonly USERVER=$DIR/ref_stress_server		# test server program

# Client/server combination flags.
let 'COMBO_U2U = 1 << 0'
let 'COMBO_U2K = 1 << 1'
let 'COMBO_K2U = 1 << 2'
let 'COMBO_K2K = 1 << 3'
readonly COMBO_U2U COMBO_U2K COMBO_K2U COMBO_K2K

# Default client/server combinations to run.
let 'DEFAULT_TEST_COMBOS = (COMBO_U2U | COMBO_U2K | COMBO_K2U | COMBO_K2K)'
readonly DEFAULT_TEST_COMBOS

# Options and configuration information to be passed to driver.
export REFSTRESS_FD_SCRIPT2DRIVER
export REFSTRESS_FD_DRIVER2SCRIPT
export REFSTRESS_OPT_NODES=0
export REFSTRESS_OPT_CMM_RECONFIG=0		# no CMM reconfiguration
export REFSTRESS_OPT_VERBOSITY=0		# don't be verbose

TEST_FAILURE=0


#
# Print usage
#
function usage #
{
	typeset combos

	# Construct description of default client/server combos
	(( DEFAULT_TEST_COMBOS & COMBO_U2U )) && combos="$combos u2u"
	(( DEFAULT_TEST_COMBOS & COMBO_U2K )) && combos="$combos u2k"
	(( DEFAULT_TEST_COMBOS & COMBO_K2U )) && combos="$combos k2u"
	(( DEFAULT_TEST_COMBOS & COMBO_K2K )) && combos="$combos k2k"
	combos=$(echo $combos | sed 's; ;, ;g')

	print -u2 "\
Usage: $PROGNAME [options...]
Options:
   -u2u  -- USER clients -> USER servers
   -u2k  -- USER clients -> KERNEL servers
   -k2u  -- KERNEL clients ->  USER servers
   -k2k  -- KERNEL clients ->  KERNEL servers

         If none of the above options are specified, then the following
         client/server combinations will be run: $combos.

   -t <test_number>[:<param>[,<param> ...]]
	Run test number <test_number> with the specified test parameter(s).
	Multiple -t options are allowed; these tests will be run in the
	given order.  If multiple -t options specify the same test (possibly
	(with different parameters), then the test will be run multiple times.
	See below for available tests and the expected parameter(s).
	E.g.:
		-t 3		(run test 3 with default parameters)
		-t 3:100,40	(run test 3 with parameters 100 and 40)
   -r
	Force CMM reconfiguration at random points in time.
   -v
	Verbose.
   -vv
	Doubly verbose.
   -?
	prints this message."

	# Have driver list available tests.
	$DRIVER '-?'
}


#
# Parse program arguments.
#
function parse_args # [program_args...]
{
	TEST_COMBOS=0
	while (( $# > 0 ))
	do
		case $1 in
		-u2u)
			# User clients -> user servers.
			(( TEST_COMBOS |= COMBO_U2U ))
			;;
		-u2k)
			# User clients -> kernel servers.
			(( TEST_COMBOS |= COMBO_U2K ))
			;;
		-k2u)
			# Kernel clients -> user servers.
			(( TEST_COMBOS |= COMBO_K2U ))
			;;
		-k2k)
			# Kernel clients -> kernel servers.
			(( TEST_COMBOS |= COMBO_K2K ))
			;;
		-t)
			# Test specification.
			if [[ -z "$2" ]]
			then
				print -u2 "Option $1 requires an argument"
				exit 1
			fi
			parse_test_spec "$2" || exit 1
			shift
			;;
		-t?*)
			# Allow -t<arg> construct
			parse_test_spec "${1#-t}" || exit 1
			;;
		-r)
			# Do CMM reconfiguration.
			REFSTRESS_OPT_CMM_RECONFIG=1
			;;
		-v)
			# Be verbose.
			REFSTRESS_OPT_VERBOSITY=1
			;;
		-vv)
			# Be doubly verbose.
			REFSTRESS_OPT_VERBOSITY=2
			;;
		-\?)
			usage
			exit 0
			;;
		*)
			print -u2 "Illegal option: $1"
			usage
			exit 1
			;;
		esac
		shift
	done

	# If user didn't specify any client/server combinations,
	# use the default ones.
	if (( TEST_COMBOS == 0 ))
	then
		TEST_COMBOS=$DEFAULT_TEST_COMBOS
	fi
}


#
# Parse a test specification (-t option argument), which is in the form of
#
#	<test_num>:[arg1[,arg2 ...]]
#
# that is, a test number, followed by ':', followed by one or more
# comma-separated arguments to be passed to the specified test.
# These arguments are appended to the array TEST_ARGS[].
#
function parse_test_spec # <test_spec>
{
	typeset testnum
	typeset testargs
	typeset index

	testnum=${1%%:*}
	if [[ $testnum != +([0-9]) ]]
	then
		print -u2 "ERROR: Illegal test number: $testnum"
		return 1
	fi

	testargs=${1##${testnum}*(:)}

	# Append to TEST_ARGS[] array.
	index=${#TEST_ARGS[*]}
	TEST_ARGS[index]="$testnum $(echo "$testargs" | sed 's/,/ /g')"

	return 0
}


#
# Script setup.
#
# Get hostname of each node in the cluster.
# Hostnames are stored in the global array HOSTNAMES with indices
# matching node ids.
#
# XXX This only works with the machines in the Galileo lab at present,
# because of the need to access a local data file containing nodes
# belonging to each cluster.
#
function setup #
{
	typeset prog
	integer nid
	typeset allhosts
	typeset host
	integer num_configured
	integer num_live_nodes=0
	typeset live_nids

	# Verify all user-land programs/tools exist and are executable.
	for prog in $MC_INFO $CLINFO $KMOD_STARTER $DRIVER $UCLIENT $USERVER
	do
		if [ ! -f $prog ]
		then
			print -u2 "ERROR: can't find $prog"
			exit 1
		elif [ ! -x $prog ]
		then
			print -u2 "ERROR: $prog not executable"
			exit 1
		fi
	done

	# Get the names of all nodes in the cluster.
	if ! allhosts=$($MC_INFO -n)
	then
		print -u2 "ERROR: $MC_INFO failed to get node names"
		exit 1
	fi

	# Get the number of configured (not necessarily alive) nodes.
	if ! num_configured=$($MC_INFO -s)
	then
		print -u2 "ERROR: $MC_INFO failed to get cluster size"
		exit 1
	fi

	# For each of the found hosts above, find its node id and map
	# its hostname to the node id in the HOSTNAMES array.
	unset HOSTNAMES
	for host in $allhosts
	do
		# Get the host's node id (use rsh -l root so we can also
		# verify that we can indeed do that).
		# Use ping to make sure the host is alive, so that way
		# we won't be stuck waiting for rsh to complete.
		if ! ping $host 3 >/dev/null 2>&1
		then
			# Skip dead host; assume it's not part of cluster
			continue
		fi

		if ! nid=$(rsh -n -l root $host $CLINFO -n)
		then
			print -u2 "ERROR: can't rsh $host as root"
			exit 1
		fi

		(( num_live_nodes += 1 ))
		live_nids="$live_nids $nid"
		HOSTNAMES[nid]=$host
	done

	if (( num_live_nodes != num_configured ))
	then
		print -u2 "WARNING: detected $num_live_nodes out of" \
			"$num_configured nodes alive"
	fi

	# Construct cluster information to be passed to driver.
	REFSTRESS_OPT_NODES="$num_live_nodes $live_nids"
}


#
# Clean up routine
#
function cleanup #
{
	typeset pid

	# Kill all (non-kernel) components, if they're still running.
	for pid in $DRIVER_PID ${SERVER_PIDS[*]} ${CLIENT_PIDS[*]}
	do
		kill $pid 2>/dev/null
		wait $pid 2>/dev/null
	done

	exit $TEST_FAILURE
}


#
# Starts the driver program as a co-process with the following
# file descriptor (re)assignments:
#
#	8		-- the pipe for script->driver communication.
#	9		-- the pipe for driver->script communication.
#
function start_driver # [driver_args ...]
{
	REFSTRESS_FD_SCRIPT2DRIVER=8
	REFSTRESS_FD_DRIVER2SCRIPT=9

	exec 7>&1			# save stdout of this script
	$DRIVER "$@" 8<&0 9>&1 0</dev/null 1>&7 |&
	DRIVER_PID=$!
	exec 7>&-			# close the saved fd's
}


#
# Run a client
#
function start_client # <nodeid> <client_id>
{
	typeset nid=$1
	typeset client_id=$2
	typeset host
	typeset rest
	typeset retval=0

	host=${HOSTNAMES[nid]}
	CLIENT_NODEIDS[client_id]=$nid

	case $CLIENT_SPACE in
	USER)
		$RSH_USED $host sh -c "'$UCLIENT $client_id >/dev/console 2>&1'" &
		CLIENT_PIDS[client_id]=$!
		sleep 3				# give client time to start
		;;

	KERNEL)
		$RSH_USED $host $KMOD_STARTER client $client_id | read retval rest
		if (( retval != 0 ))
		then
			print -u2 "ERROR: can't load kernel client: $rest"
		else
			CLIENT_MODIDS[client_id]=$rest
		fi
		;;
	esac

	return $retval
}


#
# End a client
#
function end_client # <client_id> [terminate]
{
	typeset client_id=$1
	typeset terminate=$2
	typeset nid
	typeset host
	typeset pid modid
	typeset retval=0

	nid=${CLIENT_NODEIDS[client_id]}
	host=${HOSTNAMES[nid]}

	case $CLIENT_SPACE in
	USER)
		pid=${CLIENT_PIDS[client_id]}
		if [ $terminate ]
		then
			kill $pid 2>/dev/null		# terminate
			retval=$?
		else
			# Check if still alive.
			if kill -0 $pid 2>/dev/null
			then
				retval=1		# client still alive
			else
				retval=0		# client already gone
			fi
		fi
		;;

	KERNEL)
		# Note, rsh returns its own status and not the exit value of
		# the command executed.
		modid=${CLIENT_MODIDS[client_id]}
		if [ $modid ]
		then
			retval=$($RSH_USED $host "modunload -i $modid; echo \$?" \
				2>/dev/null)
		fi
		;;
	esac

	return $retval
}


#
# Run a server
#
function start_server # <nodeid> <server_id>
{
	typeset nid=$1
	typeset server_id=$2
	typeset host
	typeset rest
	typeset retval=0

	host=${HOSTNAMES[nid]}
	SERVER_NODEIDS[server_id]=$nid

	case $SERVER_SPACE in
	USER)
		$RSH_USED $host sh -c "'$USERVER $server_id >/dev/console 2>&1'" &
		SERVER_PIDS[server_id]=$!
		sleep 3			# give server time to start
		;;

	KERNEL)
		$RSH_USED $host $KMOD_STARTER server $server_id | read retval rest
		if (( retval != 0 ))
		then
			print -u2 "ERROR: can't load kernel server: $rest"
		else
			SERVER_MODIDS[server_id]=$rest
		fi
		;;
	esac

	return $retval
}


#
# End a server
#
function end_server # <server_id> [terminate]
{
	typeset server_id=$1
	typeset terminate=$2
	typeset nid
	typeset host
	typeset pid modid
	typeset retval=0

	nid=${SERVER_NODEIDS[server_id]}
	host=${HOSTNAMES[nid]}

	case $SERVER_SPACE in
	USER)
		pid=${SERVER_PIDS[server_id]}
		if [ $terminate ]
		then
			kill $pid 2>/dev/null		# terminate
			retval=$?
		else
			# Check if still alive.
			if kill -0 $pid 2>/dev/null
			then
				retval=1		# server still alive
			else
				retval=0		# server already gone
			fi
		fi
		;;

	KERNEL)
		# Note, rsh returns its own status and not the exit value of
		# the command executed.
		modid=${SERVER_MODIDS[server_id]}
		if [ $modid ]
		then
			retval=$($RSH_USED $host "modunload -i $modid; echo \$?" \
					2>/dev/null)
		fi
		;;
	esac

	return $retval
}


#
# Run a resource balancer interface (can only be in kernel)
#
function start_rsrc_balancer # <nodeid> <server_id>
{
	typeset nid=$1
	typeset server_id=$2
	typeset host
	typeset rest
	typeset retval=0

	host=${HOSTNAMES[nid]}
	RSRC_BAL_NODEIDS[server_id]=$nid

	# Resource balancer interface can only be in the kernel.
	$RSH_USED $host $KMOD_STARTER rsrc_balancer $server_id | read retval rest
	if (( retval != 0 ))
	then
		print -u2 "ERROR: can't load kernel rsrc_balancer: $rest"
	else
		RSRC_BAL_MODIDS[server_id]=$rest
	fi

	return $retval
}


#
# End a resource balancer interface (can only be in kernel)
#
function end_rsrc_balancer # <server_id> [terminate]
{
	typeset server_id=$1
	typeset terminate=$2
	typeset nid
	typeset host
	typeset pid modid
	typeset retval=0

	nid=${RSRC_BAL_NODEIDS[server_id]}
	host=${HOSTNAMES[nid]}

	# Resource balancer interface can only be in the kernel.
	# Note, rsh returns its own status and not the exit value of
	# the command executed.
	modid=${RSRC_BAL_MODIDS[server_id]}
	if [ $modid ]
	then
		retval=$($RSH_USED $host "modunload -i $modid; echo \$?" 2>/dev/null)
	fi

	return $retval
}


#
# Start an entity.
# Returns 0 if successful, nonzero otherwise.
#
function start # <nodeid> <entity> <id>
{
	typeset nid=$1
	typeset entity=$2
	typeset id=$3
	typeset retval

	case $entity in
	client)
		start_client $nid $id
		retval=$?
		;;
	server)
		start_server $nid $id
		retval=$?
		;;
	rsrc_balancer)
		start_rsrc_balancer $nid $id
		retval=$?
		;;
	*)
		print -u2 "ERROR: Unknown entity: $entity"
		return 1
		;;
	esac

	return $retval
}


#
# End an entity.
# User-land entity will be terminated if the last argument (terminate)
# is not empty.  Otherwise, this function simply verifies whether it's still
# alive or not -- user-land entity will stay alive until it's unreferenced;
# 0 is returned if the process already exited, nonzero otherwise.
# There is no way to terminate a kernel-land entity.  The last argument
# (terminate) is ignored; modunload is always attempted and its exit status
# is returned.
# Returns 0 if successful, nonzero otherwise.
#
function end # <nodeid> <entity> <id> [terminate]
{
	typeset nid=$1					# ignored
	typeset entity=$2
	typeset id=$3
	typeset terminate=$4				# ignored if kernel
	typeset retval

	case $entity in
	client)
		end_client $id $terminate
		retval=$?
		;;
	server)
		end_server $id $terminate
		retval=$?
		;;
	rsrc_balancer)
		end_rsrc_balancer $id $terminate
		retval=$?
		;;
	*)
		print -u2 "ERROR: Unknown entity: $entity"
		return 1
		;;
	esac

	return $retval
}


#
# Run test
#
function run_test # [driver_arg ...]
{
	typeset command args
	typeset pid

	start_driver "$@"

	# Read and execute commands from the driver.
	while read -p command args
	do
		case $command in
		start)	# cmd: start <nodeid> <entity> <id>
			start $args
			print -p $?			# print status as reply
			;;

		end)	# cmd: end <nodeid> <entity> <id>
			end $args
			print -p $?			# print status as reply
			;;

		terminate) # cmd: terminate <nodeid> <entity> <id>
			end $args terminate
			print -p $?			# print status as reply
			;;

		done)	# driver says it's time to quit
			break
			;;

		*)	print -u2 "ERROR: unknown command \"$command\" from" \
				"driver"
			exit 1
			;;
		esac
	done

	# Wait until all (non-kernel) components are finished and
	# examine their results.
	for pid in $DRIVER_PID ${SERVER_PIDS[*]} ${CLIENT_PIDS[*]}
	do
		wait $pid 2>/dev/null
		if (( $? != 0 ))
		then
			TEST_FAILURE=1
		fi
	done

	unset DRIVER_PID
	unset CLIENT_PIDS CLIENT_MODIDS CLIENT_NODEIDS
	unset SERVER_PIDS SERVER_MODIDS SERVER_NODEIDS
	unset RSRC_BAL_MODIDS RSRC_BAL_NODEIDS
}


#
# Run each specified test
#
function do_tests #
{
	typeset combo
	typeset testargs

	TEST_FAILURE=0

	# Run each specified client/server combination.
	for combo in $COMBO_U2U $COMBO_U2K $COMBO_K2U $COMBO_K2K
	do
		case $(( TEST_COMBOS & combo )) in
		$COMBO_U2U)
			CLIENT_SPACE=USER
			SERVER_SPACE=USER
			;;

		$COMBO_U2K)
			CLIENT_SPACE=USER
			SERVER_SPACE=KERNEL
			;;

		$COMBO_K2U)
			CLIENT_SPACE=KERNEL
			SERVER_SPACE=USER
			;;

		$COMBO_K2K)
			CLIENT_SPACE=KERNEL
			SERVER_SPACE=KERNEL
			;;

		*)
			continue			# combo not specified
			;;
		esac

		print ">>> $CLIENT_SPACE clients -> $SERVER_SPACE servers <<<"

		# Run each specified test.
		if (( ${#TEST_ARGS[*]} == 0 ))
		then
			run_test			# run default tests
		else
			for testargs in "${TEST_ARGS[@]}"
			do
				run_test $testargs
			done
		fi

		print
	done

	if (( TEST_FAILURE ))
	then
		print "RESULT: FAIL"
	else
		print "RESULT: PASS"
	fi
}


#
# MAIN
#
parse_args "$@"
trap 'cleanup' EXIT
setup
do_tests
