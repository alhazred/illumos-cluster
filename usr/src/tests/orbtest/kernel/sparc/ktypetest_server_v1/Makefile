#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)Makefile	1.7	08/05/20 SMI"
#

#
# tests/kernel/sparc/ktypetest_server_v0/Makefile
#
#	This makefile drives the production of the ktypetest_server driver
#	kernel module.
#
#	sparc architecture dependent
#

#
#	Path to the base of the uts directory tree (usually /usr/src/uts).
#
UTSBASE = $(SRC)/uts

#
#	Include common rules.
#
include $(UTSBASE)/sparc/Makefile.sparc
include $(SRC)/common/cl/orbtest/types/Makefile.files
include ../Makefile.com

#
#	Define the module and object file sets.
#
MODULE		= ktypetest_server_v1
TESTMOD_OBJECTS	+= $(TYPES_SERVER_KOBJS:%=$(OBJS_DIR)/%)
LINTS		= $(TYPES_SERVER_KOBJS:%.o=$(LINTS_DIR)/%.ln)
ROOTMODULE	= $(ROOT_MISC_DIR)/$(MODULE)

.PARALLEL: $(TESTMOD_OBJECTS)

CLOBBERFILES += $(TESTMOD_OBJECTS)

#
#	Define targets
#
ALL_TARGET	= $(BINARY)
LINT_TARGET	= $(LINTS)
INSTALL_TARGET	= $(BINARY) $(ROOTMODULE)

#
#	Overrides.
#

#
# BUG: 4086497
# Synopsis: Array definitions do not generate _slice defintion.
#
# BUG: 4089645
# Synopsis: Object references within IDL exceptions are not supported.
#
CPPFLAGS += -DBUG_4086497 -DBUG_4089645

#
# Toggle version 0 code in ktypetest_server.cc
#
CPPFLAGS += -DORBTEST_V1

#
#	Default build targets.
#
.KEEP_STATE:

def:		$(DEF_DEPS)

all:		$(ALL_DEPS)

clean:		$(CLEAN_DEPS)

clobber:	$(CLOBBER_DEPS)

lint:		$(LINT_DEPS)

clean.lint:	$(CLEAN_LINT_DEPS)

install:	$(INSTALL_DEPS)

#
#	Include common targets.
#
include $(UTSBASE)/sparc/Makefile.targ
include $(UTSBASE)/sparc/Makefile.cl
include $(SRC)/common/cl/orbtest/Makefile.targ
include $(SRC)/common/cl/orbtest/types/Makefile.rules
