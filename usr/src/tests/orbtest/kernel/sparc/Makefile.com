#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)Makefile.com	1.17	08/05/20 SMI"
#

#
# tests/kernel/sparc/Makefile.com
#

include $(SRC)/tests/orbtest/kernel/Makefile.modules

LD=@LD_NOEXEC64=1 LD_NOEXEC_64=1  /usr/ccs/bin/ld

#
# We need at least one remaining object to populate the template
# archive so that ld doesn't complain about empty symbol tables.
#
NILOBJ		= $(OBJS_DIR:%=%/nil.o)

OBJECTS         = $(MODULE:%=$(OBJS_DIR)/%.tmod)

PRE_OBJECTS	= $(OBJS_DIR)/crti.o
POST_OBJECTS	= $(OBJS_DIR)/crtn.o

CLOBBERFILES	+= $(ALL_BUILDS:%=%/$(MODULE)) $(MODULE:%=$(OBJS_DIR)/%.tmod)
CLOBBERFILES	+= $(NILOBJ)
