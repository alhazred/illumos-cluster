#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)Makefile	1.12	08/05/20 SMI"
#

#
# tests/kernel/i386/orbtest_server/Makefile
#
#	This makefile drives the production of the orbtest_server driver
#	kernel module.
#
#	i386 architecture dependent
#

#
#	Path to the base of the uts directory tree (usually /usr/src/uts).
#
UTSBASE = $(SRC)/uts

#
#	Include common rules.
#
include $(UTSBASE)/intel/Makefile.intel
include $(SRC)/common/cl/orbtest/dev_tests/Makefile.files
include ../Makefile.com

#
#	Define the module and object file sets.
#
MODULE		= orbtest_server
TESTMOD_OBJECTS	+= $(DEV_TESTS_SERVER_KOBJS:%=$(OBJS_DIR)/%)
LINTS		= $(DEV_TESTS_SERVER_KOBJS:%.o=$(LINTS_DIR)/%.ln)
ROOTMODULE	= $(ROOT_MISC_DIR)/$(MODULE)

.PARALLEL: $(TESTMOD_OBJECTS)

CLOBBERFILES += $(FAULT_CC_OBJS_GLOB) $(TESTMOD_OBJECTS)

#
#	Define targets
#
ALL_TARGET	= $(BINARY)
LINT_TARGET	= $(LINTS)
INSTALL_TARGET	= $(BINARY) $(ROOTMODULE)

#
#	Overrides.
#

#
#	Default build targets.
#
.KEEP_STATE:

def:		$(DEF_DEPS)

all:		$(ALL_DEPS)

clean:		$(CLEAN_DEPS)

clobber:	$(CLOBBER_DEPS)

lint:		$(LINT_DEPS)

clean.lint:	$(CLEAN_LINT_DEPS)

install:	$(INSTALL_DEPS)

#
#	Include common targets.
#
include $(UTSBASE)/intel/Makefile.targ
include $(UTSBASE)/intel/Makefile.cl
include $(SRC)/common/cl/orbtest/Makefile.targ
include $(SRC)/common/cl/orbtest/dev_tests/Makefile.rules
