#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)run.ksh	1.8	08/05/20 SMI"
#

#
# A simple convenience script to execute the driver program for the
# IDL types test.
#
# Items with "@@" markers will be substituted by make.
#

# Where test programs/scripts are.
readonly DIR="@@PROGS_DIR@@"

readonly DRIVER_V0=$DIR/driver0		# version 0 driver program
readonly DRIVER_V1=$DIR/driver1		# version 1 driver program

# Make sure the driver programs are there
if [[ ! -f $DRIVER_V0 ]]
then
	print -u2 "ERROR: Can't find version 0 driver: $DRIVER"
	exit 1
elif [[ ! -x $DRIVER_V0 ]]
then
	print -u2 "ERROR: Can't execute version 0 driver: $DRIVER"
	exit 1
fi

if [[ ! -f $DRIVER_V1 ]]
then
	print -u2 "ERROR: Can't find version 1 driver: $DRIVER"
	exit 1
elif [[ ! -x $DRIVER_V1 ]]
then
	print -u2 "ERROR: Can't execute version 1 driver: $DRIVER"
	exit 1
fi

# Now execute the driver itself
exec $DRIVER_V0 "$@"
sleep 5
exec $DRIVER_V1 "$@"
