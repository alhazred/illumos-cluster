#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)run_test.ksh	1.23	08/05/20 SMI"
#

#
# A simple script to automatically start the user-land and kernel-land
# clients and servers on various nodes of the cluster, run the tests once,
# and unload the clients and servers.  This is useful for quick putback
# testing.
#
# Items with "@@" markers will be substituted by make.
#

# Where test programs/scripts are.
readonly DIR="@@PROGS_DIR@@"

readonly MC_INFO=/usr/cluster/orbtest/fi_support/cluster_info
readonly HATIMERUN=/usr/cluster/bin/hatimerun
readonly HATIMERUN_TIMEOUT=100
readonly MODLOAD='/usr/sbin/modload -p'
readonly MODUNLOAD=/usr/sbin/modunload
readonly MODINFO=/usr/sbin/modinfo

readonly ITERS=10			# how many times to run the IDL tests.

readonly SLEEP_BEFORE_UNLOADS=3		# before unloading kernel client/server
readonly UNLOAD_RETRIES=20		# how many times to try modunloading
readonly SLEEP_BETWEEN_UNLOADS=2	# sleep between the modunload retries

readonly UCLIENT=$DIR/uclient0			# test client program
readonly KCLIENT=misc/ktypetest_client_v1	# test client kernel module

readonly USERVER=$DIR/userver0			# test server program
readonly KSERVER=misc/ktypetest_server_v1	# test server kernel module
readonly TEST_FORK=$DIR/test_fork       # the actual test_fork program


# Get node names and number of nodes.
if ! set -A NODE_NAMES $($MC_INFO -n)
then
	print -u2 "ERROR: $MC_INFO failed to get node names"
	exit 1
fi
readonly NUM_NODES=${#NODE_NAMES[*]}
readonly RSH_USED="$HATIMERUN -t $HATIMERUN_TIMEOUT rsh"

# Verify that we can rsh as root to each node.
for host in ${NODE_NAMES[*]}
do
	# First make sure the host is alive, so that way we won't
	# be stuck waiting for rsh to complete.
	if ping $host 3 >/dev/null 2>&1
	then
		if ! $RSH_USED -n -l root $host /bin/true
		then
			print -u2 "ERROR: can't rsh $host as root"
			exit 1
		fi
	else
		print -u2 "ERROR: node $host is apparently dead"
		exit 1
	fi
done

# Assign hosts to run the clients and servers.
# Note: assignment of hosts is done in a round-robin manner; thus it is
#       independent of the number of nodes in the cluster.
integer IDX=0
for ITEM in UCLIENT USERVER KCLIENT KSERVER
do
	eval ${ITEM}_HOST=${NODE_NAMES[IDX]}
	(( IDX = (IDX + 1) % NUM_NODES ))
done

#
# Determine which clients and servers are needed
#
typeset U2U
typeset U2K
typeset K2U
typeset K2K

if echo "$@" | grep -- '-\<u2u\>' > /dev/null 2>&1
then
	U2U="true"
else
	U2U=""
fi

if echo "$@" | grep -- '-\<u2k\>' > /dev/null 2>&1
then
	U2K="true"
else
	U2K=""
fi

if echo "$@" | grep -- '-\<k2u\>' > /dev/null 2>&1
then
	K2U="true"
else
	K2U=""
fi

if echo "$@" | grep -- '-\<k2k\>' > /dev/null 2>&1
then
	K2K="true"
else
	K2K=""
fi

if [[ -z $U2U ]] && [[ -z $U2K ]] && [[ -z $K2U ]] && [[ -z $K2K ]]
then
	U2U="true"
	U2K="true"
	K2U="true"
	K2K="true"
fi

#
# Start clients and servers
#
if [[ -n  $U2U ]] || [[ -n $U2K ]]
then
	echo "Starting user client on $UCLIENT_HOST"
	$RSH_USED -n -l root $UCLIENT_HOST sh -c "'$UCLIENT >/dev/console 2>&1'" &
	UCLIENT_PID=$!
fi

if [[ -n $U2U ]] || [[ -n $K2U ]]
then
	echo "Starting user server on $USERVER_HOST"
	$RSH_USED -n -l root $USERVER_HOST sh -c "'$USERVER >/dev/console 2>&1'" &
	USERVER_PID=$!
fi

if [[ -n $K2U ]] || [[ -n $K2K ]]
then
	echo "Starting kernel client on $KCLIENT_HOST"
	$RSH_USED -n -l root $KCLIENT_HOST $MODLOAD $KCLIENT
	KCLIENT_MODID=$($RSH_USED -n -l root $KCLIENT_HOST $MODINFO \| \
	    nawk -v modfile=${KCLIENT##*/} "'\$6 == modfile {print \$1}'")
fi

if [[ -n $U2K ]] || [[ -n $K2K ]]
then
	echo "Starting kernel server on $KSERVER_HOST"
	$RSH_USED -n -l root $KSERVER_HOST $MODLOAD $KSERVER
	KSERVER_MODID=$($RSH_USED -n -l root $KSERVER_HOST $MODINFO \| \
	    nawk -v modfile=${KSERVER##*/} "'\$6 == modfile {print \$1}'")
fi

# Now run the tests ($ITERS times if user doesn't specify -iter option).
sleep 3				# give user-land client/server time to start
if echo "$@" | grep -- '-\<iter\>' > /dev/null 2>&1
then
	$DIR/run "$@"
else
	$DIR/run "$@" -iter $ITERS
fi

# Tell all clients and kernels that tests are done.
if [[ -n $U2U ]] && [[ -n $U2K ]] && [[ -n $K2U ]] && [[ -n $K2K ]]
then
	$DIR/finish
elif [[ -n $U2U ]]
then
	$DIR/finish -u2u
elif [[ -n $U2K ]]
then
	$DIR/finish -u2k
elif [[ -n $K2U ]]
then
	$DIR/finish -k2u
else
	$DIR/finish -k2k
fi

# Wait until the user-land client/server exit
if [[ -n $U2U ]] || [[ -n $U2K ]]
then
	wait $UCLIENT_PID
fi

if [[ -n $U2U ]] || [[ -n $K2U ]]
then
	wait $USERVER_PID
fi

# Following code is to execute test_fork executable for testing
#fork support.
# Make sure the test_fork program is there
if [[ ! -f $TEST_FORK ]]
then
        print -u2 "ERROR: Can't find test_fork program: $TEST_FORK"
        exit 1
elif [[ ! -x $TEST_FORK ]]
then
        print -u2 "ERROR: Can't execute test_fork program: $TEST_FORK"
        exit 1
fi

# Now execute the test_fork itself
# execute test_fork to test fork() function.
echo	"\n-------------Test fork support-------	"

# execute test_fork to test fork1() function.
exec $TEST_FORK 2 &
TEST_FORK_PID=$!
wait $TEST_FORK_PID

# execute test_fork to test vfork() function.
exec $TEST_FORK 3 &
TEST_FORK_PID=$!
wait $TEST_FORK_PID
echo	"\n-------------End of Test fork support-------	"
#
# Unload the kernel client/server after a short sleep.  If the unloading fails,
# then we'll retry it for several times until it's successful.  These are
# because reference counting messages could take awhile to reach the object
# implementations.
#
function unload_kernmod # <"client"/"server"> <host> <module_id>
{
	typeset modtype=$1
	typeset host=$2
	typeset modid=$3
	typeset -i count=0
	typeset infoline
	typeset rsh_output

	while true
	do
		# Unload the kernel module.
		rsh_output=$($RSH_USED -n -l root $host $MODUNLOAD -i $modid 2>&1)

		# Check to make sure it has really been unloaded.
		infoline=$($RSH_USED -n -l root $host $MODINFO -i $modid 2>/dev/null)

		# If the module has been unloaded, break.
		[[ -z "$infoline" ]] && break

		# The module is still loaded, retry unloading.
		if (( count < UNLOAD_RETRIES ))
		then
			sleep $SLEEP_BETWEEN_UNLOADS
			echo "Retrying modunload of kernel $modtype" \
				"on $host ..."
			(( count += 1 ))
		else
			echo "$rsh_output"
			echo "ERROR: Can't unload kernel $modtype on $host" \
			     "after $count tries"
			break
		fi
	done
}

if [[ -n $K2U ]] || [[ -n $K2K ]]
then
	unload_kernmod "client" $KCLIENT_HOST $KCLIENT_MODID
fi

if [[ -n $U2K ]] || [[ -n $K2K ]]
then
	unload_kernmod "server" $KSERVER_HOST $KSERVER_MODID
fi
