#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)finish.ksh	1.8	08/05/20 SMI"
#

#
# A simple script to notify all types test servers and clients that testing
# is finished.  This allows user-space kernel and client to exit and
# the kernel-space versions to be modunload'ed.  This script simply executes
# the driver program for the IDL types test with the special option -done.
#
# Items with "@@" markers will be substituted by make.
#

# Where test programs/scripts are.
readonly DIR="@@PROGS_DIR@@"

exec $DIR/driver1 -done
