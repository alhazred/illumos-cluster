/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)daemonize.c	1.7	08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <signal.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/param.h>
#include <string.h>

#define	MAXARGS 64
#define	ARGSIZE 64

/*
 * Forward declarations
 */
static void print_usage(void);
/*
 * main()
 */
int
main(int argc, char **argv)
{
	char pathname[MAXPATHLEN], command[MAXPATHLEN], *c;
	char *argvector[MAXARGS];
	int pid1, j = 0, i;
	int err;
	struct stat statbuf;
	/*
	 * parse and validate command line args
	 */
	if (argc < 2) {
		print_usage();
		exit(1);
	}
	if (argv[1] == NULL) {
		print_usage();
		exit(1);
	}
	(void) strcpy(pathname, argv[1]);

	if ((c = strrchr(pathname, '/')) == NULL) {
		(void) strcpy(command, pathname);
	} else {
		(void) strcpy(command, c+1);
	}
	if (stat(pathname, &statbuf) != 0) {
		(void) fprintf(stderr,
		"FATAL ERROR: pathname %s is not valid\n", pathname);
		exit(1);
	}
	/*
	 * Allocate and populate argument vector
	 */
	for (i = 0; i < MAXARGS; i++) {
		argvector[i] = malloc(ARGSIZE);
	}
	(void) strcpy(argvector[j++], command);
	for (i = 2; i < argc; i++) {
		(void) strcpy(argvector[j++], argv[i]);
	}
	argvector[j] = NULL;
	/*
	 * Fork off child to exec the daemon and then exit
	 */
	if ((pid1 = fork()) == 0) {
		(void) close(0);
		(void) close(1);
		(void) close(2);
		(void) setsid();
		(void) sleep(10);
		(void) execv(pathname, &argvector[0]);
	} else {
		if (pid1 < 0) {
			err = errno; /*lint !e746 */
			(void) fprintf(stderr,
				"FATAL ERROR: "
				"Unable to fork process errno=%d (%s)\n",
				err, strerror(err));
			for (i = 0; i < MAXARGS; i++) {
				if (argvector[i] != NULL) free(argvector[i]);
			}
			exit(1);
		}
		for (i = 0; i < MAXARGS; i++) {
			if (argvector[i] != NULL) free(argvector[i]);
		}
	}
	(void) sleep(2);
	exit(0);
}

/*
 * Print the usage message
 */
static void print_usage(void)
{
	(void) fprintf(stderr,
		"\nUsage: daemonize <pathname> <args>\n\n");
}
