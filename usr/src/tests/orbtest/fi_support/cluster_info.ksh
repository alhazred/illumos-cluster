#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)cluster_info.ksh	1.9	08/05/20 SMI"
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident "@(#)cluster_info.ksh 1.9     08/05/20 SMI"
#

#
# Print cluster information
#

readonly PROG=$0
readonly NODENAME=$2
readonly NUMARGS=$#
readonly PROGDIR=$(dirname $0)
readonly LOCALHOST=`hostname`

#
# Print usage information to stderr.
#
function usage #
{
        cat >&2 << EOF
Usage: $PROG [option] action

Options:
  -h|-?
      Prints this message.

Actions:
  -e [node_name]
      Prints node ID's in the specified cluster.
      The current cluster is assumed if cluster_name is not given.
  -n [node_name]
      Prints node names in the specified cluster.
      The current cluster is assumed if cluster_name is not given.
  -r [node_name]
      Prints the root node's name of the specified cluster.
      The current cluster is assumed if cluster_name is not given.
  -s [node_name]
      Prints size of the specified cluster.
      The current cluster is assumed if cluster_name is not given.
  -v [node_name]
      Returns with exit value zero if cluster_name is valid.
      NOTE: the action argument cluster_name is required.

EOF
}


#
# Parse program arguments
#
function parse_args # <program_args...>
{
        typeset opt

        while getopts ':h?cemnrsv' opt
        do
                case $opt in
                h)      # print usage info
                        usage
                        exit 0
                        ;;

                e)      # print node ID's of specified/current cluster
                        ACTION=print_nodeids_by_cluster
                        ;;

                n)      # print node names in specified/current cluster
                        ACTION=print_nodenames_by_cluster
                        ;;

                r)      # print root node in specified/current cluster
                        ACTION=print_root_node_by_cluster
                        ;;

                s)      # print size of specified/current cluster
                        ACTION=print_cluster_size
                        ;;

                v)      # exit with 0 if specified cluster is valid
                        ACTION=verify_cluster
                        ;;

                \?)     # unknown option/action or -? for help
                        if [[ "$OPTARG" = '?' ]]
                        then
                                usage
                                exit 0
			else
                        	print -u2 "ERROR: Option $OPTARG unknown"
                        	exit 1
                        fi
                        ;;

                :)      # no option argument
                        print -u2 "ERROR: Option $OPTARG needs argument"
                        exit 1
                        ;;
                *)
                        print -u2 "ERROR: Option $opt unrecognized"
                        exit 1
                        ;;
                esac
        done
        shift 'OPTIND - 1'

        # Make sure that an action was specified.
        if [[ -z "$ACTION" ]]
        then
                print -u2 "ERROR: no action was specified"
                exit 1
        fi

	# Verify there is at most one argument after "-" option.
	# If not, print the usage
	if [[ $NUMARGS -gt 2 ]]
        then
                usage
                exit 1
        fi

	# Verify communication to destinated node
	check_communication
}

function check_communication
{
	# Verify communication to destinated node
        if [ $NUMARGS -eq 1 ]; then # request is about this cluster
                nodename=$LOCALHOST
        else
                nodename=$NODENAME

                # Verify the remote node is pingable before verifying
                # whether the rsh oeration will be successfully, since
                # rsh operation could take quite some time to return
                # when it fails
                #
                /usr/sbin/ping $nodename 2>/dev/null 1>&2
                if [ $? -ne 0 ]
                then
                        print -u2 "ERROR: Node $nodename is unreachable "
                                "or does not exist"
                        exit 1
                fi

                rsh $nodename -l root "uname -a" 2>/dev/null 1>&2
                if [ $? -ne 0 ]
                then
                        print -u2 "ERROR: Cannot rsh to $nodename as "
                                "root from `hostname` - check /.rhosts "
                                "on $nodename"
                        exit 1
                fi
        fi
}

function print_nodeids_by_cluster # [node_name]
{
        if [ $nodename == $LOCALHOST ]
	then
		/usr/cluster/bin/scconf -p | \
		    awk '/Cluster node name:/ { printf ("%s " , $4 ) }\
		    /Node ID:/ { printf ("%d \n", $3 ) }  ' |
		    sort -n +1 | awk '{printf ("%s ",$2) } END {print " "}'
	else
		rsh -l root $nodename "/usr/cluster/bin/scconf -p" | \
         	awk '/Cluster node name:/ { printf ("%s " , $4 ) }\
                    /Node ID:/ { printf ("%d \n", $3 ) }  ' |
                    sort -n +1 | awk '{printf ("%s ",$2) } END {print " "}'
	fi
}

function print_nodenames_by_cluster # [node_name]
{
        if [ $nodename == $LOCALHOST ]
        then
		/usr/cluster/bin/scconf -p | \
		    awk '/Cluster node name:/ { printf ("%s " , $4 ) }\
		    /Node ID:/ { printf ("%d \n", $3 ) }  ' |
		    sort -n +1 | awk '{printf ("%s ",$1) } END {print " "}'
        else
		rsh -l root $nodename "/usr/cluster/bin/scconf -p" | \
		    awk '/Cluster node name:/ { printf ("%s " , $4 ) }\
		    /Node ID:/ { printf ("%d \n", $3 ) }  ' |
		    sort -n +1 | awk '{printf ("%s ",$1) } END {print " "}'
        fi
}

function print_root_node_by_cluster # [node_name]
{
        if [ $nodename == $LOCALHOST ]
        then
                /usr/cluster/bin/scconf -p | grep 'Cluster nodes:'| \
                    awk '{print $3}'
        else
                rsh -l root $nodename "/usr/cluster/bin/scconf -p | " \
                    "grep 'Cluster nodes:' | " \
                    "awk '{print \$3}'"
        fi
}

function print_cluster_size # [node_name]
{
        if [ $nodename == $LOCALHOST ]
        then
                /usr/cluster/bin/scstat -n | grep -c 'Cluster node:'
        else
                rsh -l root $nodename "/usr/cluster/bin/scstat -n | " \
                    "grep -c 'Cluster node:'" 
        fi
}

#
# Note: the functionality of the verify_cluster routine in this script
# is different from original sense/functionality of the same routine
# in /ws/galileo/tools/bin/mc_info. All this routine does is to verify
# the current node or user-specified node is a valid cluster node. 
# And that is.
#
function verify_cluster # [node_name]
{
        if [ $nodename == $LOCALHOST ]
	then 
		/usr/sbin/clinfo
	else
		rsh -l root $nodename "/usr/sbin/clinfo" 2>/dev/null
	fi
}



#
# Set up
#
function setup # <program_args...>
{
        # Parse program arguments.
        parse_args "$@"
}

#
# Main Routine
#

setup "$@"

# Call action routine to do the processing
$ACTION 

