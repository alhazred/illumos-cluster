#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)user_server_unload.ksh	1.9	08/05/20 SMI"
#

readonly ENTRY=$1

###############################################################################
# wait_loop
# Print a message that we're waiting for the server every 15 seconds
#
function wait_loop #
{
	# Note, the "INFO:" message prefix below notifies the run_test_fi
	# script and it will be filtered out
	while true; do
		sleep 15
		echo "INFO:  * Waiting for server program $ENTRY to return..."
	done
}


set -o monitor				# so bg jobs have separate PGIDs

readonly SYMLINK=/tmp/$ENTRY
readonly OUTFILE=${SYMLINK}.out		# contains server output
readonly TRUSSOUT=${SYMLINK}.truss	# to capture truss output

echo "  * Unloading server program: $ENTRY"

# Get the PID of the server program
SERVER_PID=$(ps -e -o pid,comm | nawk '$2 == "'$SYMLINK'" {print $1}')
if [ $? -ne 0 ]; then
	echo "+++ ERROR: Server program $ENTRY is not running"
	rm -f $SYMLINK
	exit 1
fi

# Start wait_loop() in the background
wait_loop &
WAIT_LOOP_PID=$!

# Do some cleanup upon exit.
trap "	rm -f $SYMLINK $OUTFILE $TRUSSOUT
	kill -KILL -$WAIT_LOOP_PID
	wait $WAIT_LOOP_PID 2>/dev/null" EXIT


# Send a SIGTERM so server can do some clean up upon exit if it chooses to
kill -TERM $SERVER_PID 2>/dev/null

# Wait until server exits.
# Note, since the server was started by a different instance of the shell
# (by the script user_server_load), we won't be able to get the exit status
# of the server from this script.  So we'll do a kludge here: we'll run truss
# on the server, looking for the _exit() system call.  From the truss output
# we'll also look for if the server was killed by a signal other than SIGTERM
# (which was sent by kill above).
RETSTR=$(truss -o $TRUSSOUT -t _exit -s !TERM -p $SERVER_PID 2>&1)
if [ $? -ne 0 ]; then
	# If the server is still alive, then it's a truss problem; otherwise
	# the server may have exited before truss had a chance to control it
	kill -0 $SERVER_PID 2>/dev/null
	if [ $? -eq 0 ]; then
		if [ -n "$RETSTR" ]; then
			echo "+++ FAIL: $RETSTR"
		fi
		echo "+++ FAIL: Can't wait on server program $ENTRY"
		exit 1
	fi
fi

# Process output of truss
if [ -s "$TRUSSOUT" ]; then
	LASTLINE=$(tail -1 $TRUSSOUT)
	case "$LASTLINE" in
	_exit*)
		# Get server's exit value
		EXITVAL=${LASTLINE#_exit\(}
		EXITVAL=${EXITVAL%%\)*}

		if [ "$EXITVAL" -ne 0 ]; then
			cat $OUTFILE 2>/dev/null
			echo "+++ FAIL: server program $ENTRY exited with" \
				"nonzero value $EXITVAL"
			exit 1
		fi
		;;

	*killed*)
		# Server was killed by a signal; find that signal
		SIGNAL=$(tail -2 $TRUSSOUT | nawk '/siginfo/ {print $2}')
		if [ -n "$SIGNAL" ]; then
			cat $OUTFILE 2>/dev/null
			echo "+++ FAIL: server program $ENTRY was killed by" \
				"signal $SIGNAL"
			exit 1
		fi
		;;
	esac
fi

cat $OUTFILE 2>/dev/null
echo "--- PASS: Server program $ENTRY unloaded"
exit 0
