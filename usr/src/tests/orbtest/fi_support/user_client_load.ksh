#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)user_client_load.ksh	1.9	08/05/20 SMI"
#

readonly MODLOAD=/usr/sbin/modload
readonly MODUNLOAD=/usr/sbin/modunload
readonly MODINFO=/usr/sbin/modinfo

BIN_PATH=$1
BINNAME=$2
ENTRY=$3
shift 3

# The rest of the arguments will be passed to the client program


function find_path_entry
{
	typeset found=0
	typeset save_ifs

	if [ -f "$BINNAME" ]; then
		found=1
		PATH_ENTRY=$PWD
	elif [ -n "$BIN_PATH" ]; then
		#
		# Find the module first in the given paths
		#
		save_ifs=$IFS
		IFS=:
		set $BIN_PATH
		IFS=$save_ifs

		for PATH_ENTRY
		do
			: ${PATH_ENTRY:=$PWD}
			if [ -f "$PATH_ENTRY/$BINNAME" ]; then
				echo "  * Found client program $BINNAME in" \
					"$PATH_ENTRY"
				found=1
				break
			fi
		done
	fi

	if [ $found -eq 0 ]; then
		echo "+++ FAIL: could not find client program $BINNAME in PATH"
		exit 1
	fi
}


#
# Find path to client program, then create a link.
#
find_path_entry
readonly SOURCE=$PATH_ENTRY/$BINNAME
readonly SYMLINK=/tmp/$ENTRY

echo "  * Starting client program: $ENTRY $@"

rm -f $SYMLINK
ln -s $SOURCE $SYMLINK

trap "rm -f $SYMLINK" EXIT

# Copy all output to /dev/console, if possible
if [[ -w /dev/console ]]; then
	OUTDEV=/dev/console
fi

$SYMLINK "$@" 2>&1 | tee $OUTDEV
