#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)run_test_fi.ksh	1.103	08/10/17 SMI"
#

#
# This script will drive the fault injection testing.  It must be run from
# a host outside the cluster.
#

#
# Set a few ksh parameters, in case user has them set/unset in the $ENV file
#
ulimit -c unlimited		# size of core dumps
ulimit -f unlimited		# max size of files written
ulimit -v unlimited		# size of virtual memory

set +o allexport		# don't automatically export vars
set +o errexit			# don't exit if commands return nonzero
set +o keyword			# only use variable assignments before commands
set +o markdirs			# don't put trailing / on directory names
set -o monitor			# so bg jobs run with own PGID, and
				# we can trap SIGCHLD and clean them up
set +o noclobber		# allow clobber
set +o noglob			# enable file name globbing
set -o nolog			# don't save functions in history file
set +o notify			# don't notify bg job completion
set +o nounset			# allow use of unset variables


readonly PROGNAME=$0
readonly LOGINNAME=$(logname)

PROGDIR=$(dirname $0)		# directory where this program resides
if [ "$PROGDIR" = "." ]; then
	PROGDIR=$PWD
fi
readonly PROGDIR

#
# Default values
#
readonly DEFAULT_FAULT_DATAFILE=$PROGDIR/fault.data
readonly DEFAULT_OUTPUT_BASEDIR=/tmp
readonly DEFAULT_BOOT_ARGS='kadb'
readonly DEFAULT_MCBOOT_BOOT_ARGS='kadb -x'
readonly DEFAULT_SPARC_BOOT_ARGS='kadb -D kernel/unix'
readonly DEFAULT_SPARC_MCBOOT_BOOT_ARGS='kadb -x -D kernel/unix'
readonly DEFAULT_INTEL_BOOT_ARGS='kadb'
readonly DEFAULT_INTEL_MCBOOT_BOOT_ARGS='kadb -x'
#
# If the cluster is installed sc30u2 later or older version
# since set nodes default vote is only available for sc30u2 and later
# version
#
#
# Some boot argument shorthands
#
readonly BOOT_SPARC_32='kadb -D kernel/unix'
readonly BOOT_SPARC_32x='kadb -x -D kernel/unix'
readonly BOOT_SPARC_64='kadb -D kernel/sparcv9/unix'
readonly BOOT_SPARC_64x='kadb -x -D kernel/sparcv9/unix'
readonly BOOT_INTEL='kadb'
readonly BOOT_INTELx='kadb -x'

#
# Tools
#
readonly EEPROM=/usr/sbin/eeprom
readonly UNAME=/bin/uname
readonly BOOTADM=/usr/sbin/bootadm
readonly MCBOOT=/usr/cluster/bin/mcboot
readonly MCBOOT_RCFILE_SRC=/usr/cluster/orbtest/fi_support/S99fi_support_mcboot
readonly MCBOOT_RCFILE=/etc/rc2.d/${MCBOOT_RCFILE_SRC##*/}
MC_INFO=cluster_info
readonly MODINFO=/usr/sbin/modinfo
readonly PING=/usr/sbin/ping
readonly REBOOT=/usr/sbin/reboot
readonly RSH="/usr/bin/rsh -n -l root"
readonly WHO_CMD='who -b'
readonly SVC_SHUTDOWN=/usr/cluster/orbtest/fi_support/ha_service_shutdown
readonly SCSTAT=/usr/cluster/bin/scstat
readonly SCCONF=/usr/cluster/bin/scconf
readonly SCDIDADM=/usr/cluster/bin/scdidadm
readonly RM_PRIMARY_NODE=/usr/cluster/bin/rm_primary_node
readonly REPLCTL=/usr/cluster/lib/sc/replctl
readonly GET_CLUSTER_STATE='/usr/cluster/lib/sc/cmm_ctl -g'
readonly HATIMERUN=hatimerun
readonly HATIMERUN_TIMEOUT=150
readonly HATIMERUN_TIMEOUT_SHORT=50
readonly HATIMERUN_RETRY=5
RSH_USED=
RSH_USED_SHORT=
readonly KCLIENT_LOAD=/usr/cluster/orbtest/fi_support/kernel_client_load
readonly KSERVER_LOAD=/usr/cluster/orbtest/fi_support/kernel_server_load
readonly KSERVER_UNLOAD=/usr/cluster/orbtest/fi_support/kernel_server_unload

readonly UCLIENT_LOAD=/usr/cluster/orbtest/fi_support/user_client_load
readonly USERVER_LOAD=/usr/cluster/orbtest/fi_support/user_server_load
readonly USERVER_UNLOAD=/usr/cluster/orbtest/fi_support/user_server_unload


###############################################################################
# print the Usage of this tool
#
function usage   #
{
	cat >&2 << EOF
Usage:
   $PROGNAME -c cluster_node [-f file] [boot options] [test options]
   $PROGNAME [-G] [-N] [-T] [-f file]
   $PROGNAME -?

Main Options:
   -c <cluster_node>
	Run tests on the specified cluster, it should be a node name like phys-foo-1.
   -G
	List all test groups defined in the fault data file.
   -N
	Display minimum number of nodes required to run all tests defined
	in the fault data file.
   -T
	List all tests defined in the fault data file.
   -f <file>
	Fault data file.
	Default: $DEFAULT_FAULT_DATAFILE
   -w
	Normally if a serious error occured while running a test case, all
	nodes of the cluster will be rebooted before continuing with the next
	test case.  If this program is run from a terminal, this option causes
	this program to wait for a user input before rebooting all nodes.
	This allows the user to debug the error before continuing.

	This option is ignored if this program is not run from a terminal.
   -?
	Displays this message.

Boot Options:
   -b <boot_args>
	Use the specified boot arguments when rebooting nodes.  The EEPROM
	boot-file parameter on each node will be set to these arguments and
	restored to its original value at the end.  Possible values for
	<boot_args> are:

		32	(short for '$BOOT_32')
		32x	(short for '$BOOT_32x')
		64	(short for '$BOOT_64')
		64x	(short for '$BOOT_64x')
		Any boot arguments you wish.

	Default: '$DEFAULT_BOOT_ARGS'
   -m
	Boot cluster using mcboot.  In this case the default boot arguments
	are '$DEFAULT_MCBOOT_BOOT_ARGS' unless the -b option is also specified.
   -I
	Perform initial reboot of the cluster before running the tests.
	Useful to ensure all nodes are first booted with the desired flags
	(e.g. to run 64-bit kernel or mcboot).

Test Options:
   -g <group[,group,...]>
	Run tests which belong to the specified test group.
	Multiple groups can be specified as a comma-separated list
	or using multiple -g options.
   -k
	Use kernel clients.
   -K
	Keep results directories even if tests pass.
   -o <dir>
	Output directory.
	Default: $DEFAULT_OUTPUT_BASEDIR
   -s <test_number>
	Run tests starting at specified test number.
   -t <arg[,arg,...]>
	Run test with the specified test number.
	Multiple tests can be specified as a comma-separated list of test
	numbers and/or ranges, e.g.:

		3,4	-- run tests 3 and 4.
		4-7,9	-- run tests 4 through 7 and 9.
		-8	-- run tests 1 through 8.
		1,10-	-- run tests 1 and 10 through last.

	Multiple -t options can also be specified.
EOF
}


###############################################################################
# Examine the comand line
#
function examine_command_line   # [program_arg...]
{
	typeset opt
	typeset display_tests=0
	typeset display_groups=0
	typeset display_numnodes=0
	typeset dont_run=0
	typeset groups
	typeset boot_opt
	typeset boot_args
	typeset platform
	typeset testnums
	typeset kernel_mode=0

	while getopts ':b:c:f:g:GkKmNo:Is:t:Twr?' opt; do
		case $opt in
		b)
			# Check for shorthand boot arguments
			case "$OPTARG" in
			32)
				boot_opt=32
				;;
			32[xX])
				boot_opt=32x
				;;
			64)
				boot_opt=64
				;;
			64[xX])
				boot_opt=64x
				;;
			*)
				boot_opt=$OPTARG
				;;
			esac
			;;
		c)
			CLUSTER_NAME=$OPTARG
			;;
		f)
			FAULT_DATAFILE=$OPTARG
			;;
		g)
			# Allow comma-separated groups
			groups=$(echo $OPTARG | sed 's/,/ /g')
			SELECTED_GROUPS="$SELECTED_GROUPS $groups"
			;;
		G)
			display_groups=1
			dont_run=1
			;;
		I)
			DO_INITIAL_REBOOT=1
			;;
		k)
			kernel_mode=1
			;;
		K)
			KEEP_RESULTS=1
			;;
		m)
			DO_MCBOOT=1
			;;
		N)
			display_numnodes=1
			dont_run=1
			;;
		o)
			OUTPUT_BASEDIR=$OPTARG
			;;
		s)
			# This is the same as specifying "-t <num>-"
			testnums="$testnums ${OPTARG}-"
			;;
		t)
			# Collect the specified test number(s) first
			testnums="$testnums $OPTARG"
			;;
		T)
			display_tests=1
			dont_run=1
			;;
		w)
			# If user has a terminal, then wait on error (in
			# error_wait()), else ignore option.
			# This is normally used before rebooting all nodes
			# because a serious error/failure has been detected.
			if print -n 2>/dev/null <>/dev/tty
			then
				ERROR_WAIT=1
			fi
			;;
		r)
			#
			# Shuffle nodes during tests. But if cluster
			# installs sc30 or sc30u1, and test will reboot
			# more than one node, doesn't shuffle_nodes for
			# these testcases.
			#
			WANT_SHUFFLE=1
			;;
		\?)
			if [[ "$OPTARG" = '?' ]]; then
				usage
				exit 0
			else
				print -u2 "ERROR: Option $OPTARG unknown"
				exit 1
			fi
			;;
		:)
			print -u2 "ERROR: Option $OPTARG needs argument"
			exit 1
			;;
		*)
			print -u2 "ERROR: Option $opt unrecognized"
			exit 1
			;;
		esac
	done
	shift 'OPTIND - 1'

	# Check fault data file.
	if [ ! -f "$FAULT_DATAFILE" ]; then
		print -u2 "Fault data file \"$FAULT_DATAFILE\" doesn't exist"
		exit 1
	fi

	if [ ! -r "$FAULT_DATAFILE" ]; then
		print -u2 "Fault data file \"$FAULT_DATAFILE\" is unreadable"
		exit 1
	fi

	# If user wants to display some stuff...
	if [ "$display_tests" -ne 0 ]; then
		display_test_cases
	fi
	if [ "$display_groups" -ne 0 ]; then
		 display_test_groups
	fi
	if [ "$display_numnodes" -ne 0 ]; then
		display_req_nodes
	fi

	# If user only wanted to display stuff...
	if [ "$dont_run" -ne 0 ]; then
		exit 0
	fi

	# Parse the specified test number(s)
	parse_test_numbers $testnums
	if [ $? -ne 0 ]; then
		exit 1
	fi

	# Check specified cluster.
	if [ -z "$CLUSTER_NAME" ]; then
		print -u2 "You must define a cluster (-c)"
		exit 1
	fi

	$MC_INFO -v "$CLUSTER_NAME"
	if [ $? -ne 0 ]; then
		print -u2 "Cluster name $CLUSTER_NAME is invalid"
		exit 1
	fi

	#
	# Get the platform info
	#
	platform=
	platform=$($RSH_USED ${CLUSTER_NAME} \
		"$UNAME -p" 2>&1)
	if [ "$platform" != "i386" -a "$platform" != "sparc" ]; then 
		print -u2 "Don't support platform $platform."
		exit 1
	fi
	if [[ "$platform" = "sparc" ]]; then
		if [[ $boot_opt = "32" ]]; then
			boot_args=$BOOT_SPARC_32
		fi
		if [[ $boot_opt = "32x" ]]; then
			boot_args=$BOOT_SPARC_32x
		fi
		if [[ $boot_opt = "64" ]]; then
			boot_args=$BOOT_SPARC_64
		fi
		if [[ $boot_opt = "64x" ]]; then
			boot_args=$BOOT_SPARC_64x
		fi
	else
		if [[ $boot_opt == *x ]]; then
			boot_args=$BOOT_INTELx
		else
			boot_args=$BOOT_INTEL
		fi
	fi
	# Figure out the appropriate boot arguments to use
	if [ "$DO_MCBOOT" -ne 0 ]; then
		if [[ "$platform" = "sparc" ]]; then
			BOOT_ARGS=${boot_args:-$DEFAULT_SPARC_MCBOOT_BOOT_ARGS}
		else
			BOOT_ARGS=${boot_args:-$DEFAULT_INTEL_MCBOOT_BOOT_ARGS}
		fi
	else
		if [[ "$platform" = "sparc" ]]; then
			BOOT_ARGS=${boot_args:-$DEFAULT_SPARC_BOOT_ARGS}
		else
			BOOT_ARGS=${boot_args:-$DEFAULT_INTEL_BOOT_ARGS}
		fi
	fi

	# Figure out the apropriate run mode
	if [ $kernel_mode -ne 0 ]; then
		if [[ "$platform" = "sparc" ]]; then
			if [[ "$BOOT_ARGS" = *sparcv9* ]]; then
				RUN_MODE=K64
			else
				RUN_MODE=K32
			fi
		else
			RUN_MODE=K
		fi	
	else
		RUN_MODE=U
	fi

	# Set real output directory
	OUTPUTDIR=$OUTPUT_BASEDIR/${LOGINNAME}_$$_fi_tests
	mkdir -p $OUTPUTDIR || exit 1
}


###############################################################################
# Parse user-specified test numbers, which can be numbers and/or ranges
# Returns: 0 if successful, non-zero otherwise.
#
function parse_test_numbers   # [test_number...]
{
	typeset total
	typeset index
	typeset arg
	typeset begin end
	typeset temp

	# First figure out how many tests are defined in fault data file
	total=$(grep -c '^[ 	]*TEST[ 	]' $FAULT_DATAFILE)

	# Reset the RUN_LIST array
	unset RUN_LIST
	index=0
	while (( index <= total )); do
		RUN_LIST[index]=0
		(( index += 1 ))
	done

	# Process the test number arguments, each may be a comma separated list
	# of numbers and/or ranges.  If there were no test numbers specified,
	# run all tests (same as specifying '1-').
	for arg in $(echo ${*:-'1-'} | sed 's/,/ /g'); do
		case $arg in
		+([0-9]))		# a number, e.g. 3
			begin=$arg
			end=$arg
			;;
		+([0-9])-+([0-9]))	# range, e.g. 3-7
			begin=${arg%-*}
			end=${arg#*-}

			# If end number < beginning number, swap them.
			if (( end < begin )); then
				temp=$begin
				begin=$end
				end=$temp
			fi
			;;
		+([0-9])-)		# range, e.g. 3- (3 through last)
			begin=${arg%-}
			end=$total
			;;
		-+([0-9]))		# range, e.g. -3 (1 through 3)
			begin=1
			end=${arg#-}
			;;
		*)
			print -u2 "ERROR: Illegal test number/range: $arg"
			return 1
			;;
		esac

		# Verify 1 <= test numbers <= $total
		if (( begin < 1 )); then
			print -u2 "ERROR: Test number $begin is less than 1"
			return 1
		fi

		if (( end > total )); then
			print -u2 "ERROR: Test number $end is greater than" \
				"the total number of tests ($total)"
			return 1
		fi

		# Mark specified test numbers in RUN_LIST array
		while (( begin <= end )); do
			RUN_LIST[begin]=1
			(( begin += 1 ))
		done
	done

	return 0
}

###############################################################################
# Check if scconf can reset node vote. This is new feature for sc30u2
# or later. This function will be called if -r option is used when
# invoking run_test_fi and the current testcase  need to reboot more than
# one node. In that case, we need to reset node vote for down nodes 0
# to avoid quorum error in the tests.
# Returns: set IS_SHUFFLE_OK to 1 if sconf can reset node vote,
# otherwise set IS_SHUFFLE_OK to 0.
#
function check_scconf_on_cluster
{
	typeset rslt=0
	typeset vote=0
	typeset i

	#
	# Since it's just a test for scconf command, we don't
	# want to change node vote now, we just get the current
	# node vote and use it in scconf -c.
	#
	i=0
	while (( i < $HATIMERUN_RETRY ))
	do
		vote=
		eval vote=$($RSH_USED_SHORT ${TEST_NODENAME_LIST[1]} "$SCSTAT -q" 2>&1 | grep "Node votes:" | grep "${TEST_NODENAME_LIST[1]}" | awk '{print $4}')
		if [ -z $vote ]
		then
			i=`expr $i + 1`
			sleep 5
			continue
		else
			break
		fi
	done
	if [ -z $vote ]
	then
		IS_SHUFFLE_OK=0
		return
	fi
	rslt=`$RSH_USED ${TEST_NODENAME_LIST[1]} "$SCCONF -c -q node=${TEST_NODENAME_LIST[1]},defaultvote=$vote 1>/dev/null 2>&1;echo \\$?"`
	if [ -z $rslt ]
	then
		#
		# Can't get return code, probably rsh timeout
		#
		IS_SHUFFLE_OK=0
	else
		if [ $rslt -eq 0 ]; then
			#
			# scconf is ok to reset the node vote
			#
			IS_SHUFFLE_OK=1
		else
			#
			# scconf can't reset the node vote
			#
			IS_SHUFFLE_OK=0
		fi
	fi
}

###############################################################################
# Set the default values of all the global variables
#
function set_default_values   #
{
	# Some array indices
	TEST_INDEX=0
	SETUP_INDEX=0
	CLIENT_INDEX=0
	SERVER_INDEX=0
	SVC_SHUTDOWN_INDEX=0
	QUORUM_INDEX=0
	NUM_SECS_INDEX=0
	PROV_PRI_INDEX=0

	# Boot arguments
	BOOT_ARGS=$DEFAULT_BOOT_ARGS

	#
	# If user want to shuffle nodes
	# 0 -- user doesn't want to shuffle nodes
	# 1 -- user want to shuffle nodes 
	# This variable is changed to 1 if user specify
	# -r option in run_test_fi.
	#
	WANT_SHUFFLE=0
	#
	# If cluster is ok to shuffle_nodes.
	# 0 -- cluster not ok to shuffle nodes
	# 1 -- cluster ok to shuffle nodes
	# If -r option is used in run_test_fi, and none, 1, or all_nodes
	# will rebooted during the tests, IS_SHUFFLE_OK set to 1,
	# since we don't need to reset node vote if down nodes in the
	# test is none, 1 or all_nodes. Otherwise IS_SHUFFULE_OK will
	# depends on if scconf can reset node vote on
	# cluster.
	#
	IS_SHUFFLE_OK=0
	#
	# Max num of down nodes of all clients in one test case.
	#
	NUM_DOWN_NODES=0

	# Don't use mcboot
	DO_MCBOOT=0

	# Don't reboot cluster first before running tests.
	DO_INITIAL_REBOOT=0

	# Fault data file
	FAULT_DATAFILE=$DEFAULT_FAULT_DATAFILE

	# Clear the ABORT TEST variable
	ABORT_TEST=0

	# Output base directory
	OUTPUT_BASEDIR=$DEFAULT_OUTPUT_BASEDIR

	# Cluster to work with
	CLUSTER_NAME=

	# Don't keep results
	KEEP_RESULTS=0

	# Don't ignore reboots
	IGNORE_REBOOTS=0

	# Don't reboot at end of test case.
	REBOOT_AFTER=0

	# Don't reboot before the test case.
	REBOOT_BEFORE=0

	# On error don't wait (normally used before rebooting all nodes).
	ERROR_WAIT=0

	# By default run in user mode -- that is for clients/servers that
	# don't specify specific modes (-m option).
	# Possible values to this variable: U, K32, K64, K.
	RUN_MODE=U

	# Set the default values for the global variables
	DEF_UPATH=		# UPATH is empty by default
	DEF_KPATH=		# KPATH is empty by default
	DEF_UNPATH=		# UNPATH is empty by default
	DEF_USER=root		# Scripts run as ROOT by default
	DEF_UFILE=fi_driver	# Binary containing user-level tests
	DEF_KFILE=fi_module	# Module containing user-level tests
	DEF_UNFILE=		# UNODE library is empty
}


###############################################################################
# Set argument variables.
#
# CLIENT and SERVER keywords may specify certain variables as arguments to
# be passed to the client or server.  These variables are called
# "argument variables" and are specified in the same manner as shell
# environment variables.  Prior to executing the client/server this script
# substitutes these variables with their corresponding values.
#
# The following argument variables are available:
#
#	NUM_NODES	-- number of nodes in the cluster.
#	NODE_<num>	-- name of node number <num> (note: node number
#			   1 always refer to the root node).
#	NODE_<num>_ID	-- node ID of node number <num>.
#	ALL_NODES	-- names of all nodes in the cluster.
#	ALL_NODE_IDS	-- node IDs of all nodes in the cluster.
#
# Example:
#
#	SERVER	-n NODE_1 -f myserver myserver $NODE_2 $NODE_2_ID
#	CLIENT	-n NODE_3 myclient $ALL_NODES
#
# $NODE_2 and $NODE_2_ID above will be replaced with the name and node ID,
# respectively, of node number 2.  These values are then passed as arguments
# to the server myserver.  The variable $ALL_NODES will be replaced with the
# names of all nodes in the cluster.
#
function set_arg_vars   #
{
	typeset i=1

	# Note, $NUM_NODES is set in get_node_names()
	while (( i <= NUM_NODES )); do
		eval NODE_${i}=${TEST_NODENAME_LIST[i]}
		eval NODE_${i}_ID=${TEST_NODEID_LIST[i]}
		(( i += 1 ))
	done

	ALL_NODES=${TEST_NODENAME_LIST[*]}
	ALL_NODE_IDS=${TEST_NODEID_LIST[*]}
}


###############################################################################
# Verifies that we can access the utlities for unode
#
function verify_utils   #
{
	#
	# check if ping program can be found in $PATH
	#
	if [[ ! -x $PING ]]; then
		print -u2 "ERROR: Can't find $PING"
		exit 1
	fi

	ret=0
	MC_INFO_FULL_PATH=`type $MC_INFO 2>/dev/null | nawk ' { print $3 }'`
	if [ ! -z "$MC_INFO_FULL_PATH" ]
	then
		#
		# Find the full path of cluster_info
		#
		if [[ -x $MC_INFO_FULL_PATH ]]; then
			#
			# cluster_info is executable
			#
			ret=0
		else
			ret=1
		fi
	else
		ret=1
	fi
	#
	# cluster_info probably doesn't found in $PATH,
	# check if the one in $CODEMGR_WS is ok to use.
	# For user who invoke run_test_fi directly instead of
	# using labrun.
	#
	if [ "$ret" -ne 0 ]
	then
		if [ ! -z "$CODEMGR_WS" ]
		then
			MC_INFO="$CODEMGR_WS/usr/src/tests/orbtest/fi_support/cluster_info"
			if [[ ! -x $MC_INFO ]]
			then
				print -u2 "ERROR: Can't find $MC_INFO. Please rebuild your WS or set \$PATH to include path of cluster_info."
				exit 1
			fi
		else
			print -u2 "ERROR: Can't find cluster_info. Please set your \$PATH or \$CODEMGR_WS to include cluster_info."
			exit 1
		fi
		
	fi
	readonly MC_INFO 

	ret=0
	HATIMERUN_FULL_PATH=`type $HATIMERUN 2>/dev/null | nawk ' { print $3 }'`
	if [ ! -z "$HATIMERUN_FULL_PATH" ]
	then
		#
		# Find the full path of hatimerun 
		#
		if [[ -x $HATIMERUN_FULL_PATH ]]; then
			#
			# hatimerun is executable
			#
			RSH_USED="$HATIMERUN_FULL_PATH -t $HATIMERUN_TIMEOUT $RSH"
			RSH_USED_SHORT="$HATIMERUN_FULL_PATH -t $HATIMERUN_TIMEOUT_SHORT $RSH"
#			RSH_USED="/opt/SUNWscts/tset_bin/hatimerun -t $HATIMERUN_TIMEOUT $RSH"
#			RSH_USED_SHORT="/opt/SUNWscts/tset_bin/hatimerun -t $HATIMERUN_TIMEOUT_SHORT $RSH"
		fi
	else
		RSH_USED="$RSH"
		RSH_USED_SHORT="$RSH"
	fi
	readonly RSH_USED
	readonly RSH_USED_SHORT
}


###############################################################################
# Set the names of the nodes for this cluster
#
function get_node_names   #
{
	typeset all_nodes all_nodeids
	typeset root_node
	typeset node nodeid
	typeset list_index index

	# Get the names of all nodes
	if ! set -A all_nodes `$MC_INFO -n $CLUSTER_NAME`; then
		print -u2 "ERROR: $MC_INFO failed to get node names"
		exit 1
	fi

	# Get the node IDs of all nodes (assume that the list of node id's
	# correspond to the list of node names obtained above)
	if ! set -A all_nodeids `$MC_INFO -e $CLUSTER_NAME`; then
		print -u2 "ERROR: $MC_INFO failed to get node names"
		exit 1
	fi

	# Get the name of the root node
	if ! root_node=`$MC_INFO -r $CLUSTER_NAME`; then
		print -u2 "ERROR: $MC_INFO failed to get name of root node"
		exit 1
	fi

	# NODENAME_LIST[] contains the names of the nodes in the cluster
	# indexed by node numbers (not node IDs) with node 1 as the root node.
	# NODEID_LIST[] contains the node IDs of the nodes in the cluster.
	unset NODENAME_LIST
	unset NODEID_LIST

	# Number of nodes in the cluster
	NUM_NODES=${#all_nodes[*]}	# this is also an argument variable

	list_index=2		# index into NODENAME_LIST[]
	index=0			# index into all_nodes[] and all_nodeids[]
	for node in ${all_nodes[*]}; do
		nodeid=${all_nodeids[index]}

		if [[ $node = $root_node ]]; then
			# Always put root node as node 1
			NODENAME_LIST[1]=$node
			NODEID_LIST[1]=$nodeid
			#
			# Init TEST_NODENAME_LIST same with NODENAME_LISt
			# Init TEST_NODEID_LIST same with NODEID_LISt
			#
			TEST_NODENAME_LIST[1]=$node
			TEST_NODEID_LIST[1]=$nodeid
		else
			NODENAME_LIST[list_index]=$node
			NODEID_LIST[list_index]=$nodeid
			#
			# Init TEST_NODENAME_LIST same with NODENAME_LISt
			# Init TEST_NODEID_LIST same with NODEID_LISt
			#
			TEST_NODENAME_LIST[list_index]=$node
			TEST_NODEID_LIST[list_index]=$nodeid

			(( list_index += 1 ))
		fi

		(( index += 1 ))
	done
}

###############################################################################
# Convert node name in the argument to the corresponding node number (note,
# not the node ID, but index in the NODENAME_LIST[]).
#
function nodename_to_nodeidx   # <node_name>
{
	typeset node=$1
	typeset nodeidx=1

	while (( nodeidx <= NUM_NODES )); do
#		if [ "${NODENAME_LIST[nodeidx]}" = "$node" ]; then
		if [ "${TEST_NODENAME_LIST[nodeidx]}" = "$node" ]; then
			print $nodeidx
			return
		fi
		(( nodeidx += 1 ))
	done
}

###############################################################################
# Convert nodeid in the argument to the corresponding node number
#
function nodeid_to_nodeidx	# <nodeid>
{
	typeset nodeid=$1
	typeset nodeidx=1

	while (( nodeidx <= NUM_NODES )); do
#		if [ "${NODEID_LIST[nodeidx]}" = "$nodeid" ]; then
		if [ "${TEST_NODEID_LIST[nodeidx]}" = "$nodeid" ]; then
			print $nodeidx
			return
		fi
		(( nodeidx += 1))
	done
}

###############################################################################
# Shuffle nodes. Randomize the node in NODENAME_LIST and
# store them in new list TEST_NODENAME_LIST and TEST_NODEID_LIST.
# And testcases will use TEST_NODENAME_LIST and TEST_NODEID_LIST
# to run tests instead of NODENAME_LIST. The purpose is to remove root node
# dependency in FI tests.
#
function shuffle_nodes
{
	typeset nodeidx
	typeset num_nodes
	typeset new_id
	typeset n
	typeset idx
	typeset rest_idx
	typeset tmp_rest_idx
	typeset i
	typeset r_num
	typeset node
	typeset id

	unset TEST_NODENAME_LIST TEST_NODEID_LIST
	nodeidx=1
	num_nodes=$NUM_NODES
	new_id=0
	idx=1
	#
	# init rest_idx to all node idx.
	# Example, for 4-node cluster, reset_idx init to "1 2 3 4"
	#
	while (( idx <= num_nodes )); do
		rest_idx="$rest_idx $idx"
		(( idx += 1 ))
	done
	#
	# Loop all node in NODENAME_LIST, get new random id $new_id
	# for this node, set TEST_NODENAME_LIST[new_id] to this node.
	# and remove $new_id from $rest_idx.
	#
	while (( nodeidx <= NUM_NODES )); do
		node=${NODENAME_LIST[nodeidx]}
		id=${NODEID_LIST[nodeidx]}
		tmp_rest_idx=
		for idx in $rest_idx; do	
			if [ $idx -ne $new_id ]
			then
				tmp_rest_idx="$tmp_rest_idx $idx"
			fi
		done
		rest_idx="$tmp_rest_idx"
		(( num_nodes = $NUM_NODES - $nodeidx + 1 ))
		size=$num_nodes
			size=`expr $size`
		n=`date +'%S'`
		r_num=`expr $n % $size + 1`
		i=1
		#
		# random num $r_num will be index in $rest_idx
		#
		for n_id in $rest_idx; do
			if [ $i -eq $r_num ]
			then
				new_id=$n_id
				break
			fi
			(( i += 1 ))
		done
		TEST_NODENAME_LIST[new_id]=$node
		TEST_NODEID_LIST[new_id]=$id
		(( nodeidx += 1 ))
	done
	#
	# since shuffle nodes, we need to reset 
	# argument variables like NODE_x, !NODE_x, ALL_NODES,
	# which is internal mapping of nodename and fault.data
	#
	set_arg_vars
}
###############################################################################
# Unshuffle nodes. Make TEST_NODENAME_LIST and TEST_NODEID_LIST
# same with NODENAME_LIST and NODEID_LIST.
#
function unshuffle_nodes
{
	typeset nodeidx

	unset TEST_NODENAME_LIST TEST_NODEID_LIST
	nodeidx=1
	while (( nodeidx <= NUM_NODES )); do
		TEST_NODENAME_LIST[nodeidx]=${NODENAME_LIST[nodeidx]}
		TEST_NODEID_LIST[nodeidx]=${NODEID_LIST[nodeidx]}
		(( nodeidx += 1 ))
	done
	#
	# We need to reset 
	# argument variables like NODE_x, !NODE_x, ALL_NODES,
	# which is internal mapping of nodename and fault.data
	#
	set_arg_vars
}

###############################################################################
# Parse node specifications.  Nodes can be given in the arguments as a
# comma-separated and/or space-separated list.  Nodes can specified as:
#
#	NODE_<num>	-- name of node number <num> (not the node ID).
#	ALL_NODES	-- names of all nodes in the cluster.
#	HA_RM		-- the name of the node that is hosting the HA RM
#
# A node specification can also be prefixed with '!' which means to exclude
# that node:
#
#	!NODE_<num>	-- names of all nodes except node number <num>.
#
# Example:
#
#	!NODE_1,!NODE_3		means all nodes except NODE_1 and NODE_3
#
# The resulting node names will be printed to stdout as a space-separated list.
#
# Returns: 0 if successful, non-zero otherwise.
#
function parse_node_specs   # [node_spec...]
{
	typeset spec nodeidx nodeid
	typeset include exclude

	for spec in $(echo "$@" | sed 's/,/ /g'); do
		case $spec in
		NODE_[1-9]*([0-9]))
			nodeidx=${spec#NODE_}
			if (( nodeidx > NUM_NODES )); then
				print -u2 "ERROR: Node $spec is non-existent"
				return 1
			fi

			include[nodeidx]=${TEST_NODENAME_LIST[nodeidx]}
			unset exclude[nodeidx]
			;;

		HA_RM)
			# Figure out the nodeid of the HA RM host
			
			nodeid=$($RSH_USED ${TEST_NODENAME_LIST[1]} \
				"$RM_PRIMARY_NODE" 2>&1 | \
				awk '{print $2}')
			nodeidx=$(nodeid_to_nodeidx $nodeid)
			if [[ $nodeidx -le 0 ]]; then
				print -u2 "ERROR: Error getting HA RM primary"
				return 1
			fi
			
			if (( nodeidx > NUM_NODES )); then
				print -u2 "ERROR: Node $spec is non-existent"
				print -u2 "ERROR: (HA RM Primary Host)"
				return 1
			fi

			include[nodeidx]=${TEST_NODENAME_LIST[nodeidx]}
			unset exclude[nodeidx]
			;;

		!NODE_[1-9]*([0-9]))
			nodeidx=${spec#!NODE_}
			if (( nodeidx > NUM_NODES )); then
				print -u2 "ERROR: Node ${spec#!} is" \
					"non-existent"
				return 1
			fi

			unset include[nodeidx]
			exclude[nodeidx]=${TEST_NODENAME_LIST[nodeidx]}
			;;

		!HA_RM)
			# Figure out the nodeid of the HA RM host
			
			nodeid=`$RSH_USED ${TEST_NODENAME_LIST[1]} "$RM_PRIMARY_NODE" 2>& 1 | awk '{print $2}'`
			nodeidx=$(nodeid_to_nodeidx $nodeid)

			if [[ $nodeidx -le 0 ]]; then
				print -u2 "ERROR: Error getting HA RM primary"
				return 1
			fi
			if (( nodeidx > NUM_NODES )); then
				print -u2 "ERROR: Node $spec is non-existent"
				print -u2 "ERROR: (HA RM Primary Host)"
				return 1
			fi
			
			unset include[nodeidx]
			exclude[nodeidx]=${TEST_NODENAME_LIST[nodeidx]}
			;;

		ALL_NODES)
			set -A include BOGUS ${TEST_NODENAME_LIST[*]}
			unset include[0]		# no node 0
			unset exclude
			;;

		*)
			print -u2 "ERROR: Illegal node specification: $spec"
			return 1
			;;
		esac
	done

	# If there is at least one node to exclude...
	if (( ${#exclude[*]} > 0 )); then
		# Include all nodes first
		set -A include BOGUS ${TEST_NODENAME_LIST[*]}	
		unset include[0]			# no node 0
	fi

	# Exclude all nodes in the exclude list from the include list
	nodeidx=1
	while (( nodeidx <= NUM_NODES )); do
		if [ -n "${exclude[nodeidx]}" ]; then
			unset include[nodeidx]
		fi
		(( nodeidx += 1 ))
	done
	echo ${include[*]}			# print resulting nodes

	return 0
}


###############################################################################
# Display the test cases specified in the fault data file
#
function display_test_cases   #
{
	integer testnum
	typeset key val
	typeset title

	print "The following tests are defined:\n"
	testnum=1
	while read key val; do
		[[ "$key" != "TEST" ]] && continue

		# Format output (fold long test descriptions)
		title="   TEST [$testnum] -"
		typeset -R${#title} indent=" "
		{
			echo "$title"
			echo "$indent" $val
		} | fmt -c -w 80
		(( testnum += 1 ))
	done < $FAULT_DATAFILE
	print
}


###############################################################################
# Display test groups specified in the fault data file
#
function display_test_groups   #
{
	print "The following test groups are defined:\n"

	nawk '$1 == "GROUP" {
		for (i = 2; i <= NF; ++i) {
			print "   " $i
		}
	}' $FAULT_DATAFILE | sort -u

	print
}


###############################################################################
# Display required number of tests to run tests in the fault data file.
#
function display_req_nodes   #
{
	nawk '$1 == "REQ_NODES" {
		if ($2 > largest)
			largest = $2
	}
	END {
		print "Required number of nodes to run tests: " largest
	}' $FAULT_DATAFILE
}


###############################################################################
# Verify communications to each node.
#
function verify_communication   #
{
	typeset node
	typeset error=0

	print "*** Verifying communications to all nodes"

	for node in ${NODENAME_LIST[*]}; do
		print " ** Verifying we can talk to: $node"

		$PING $node 30 >/dev/null 2>&1
		if [ $? -ne 0 ]; then
			print -u2 "ERROR: Node $node is dead"
			error=1
		else
			$RSH_USED $node true
			if [ $? -ne 0 ]; then
				print -u2 "ERROR: Can't talk to node $node"
				error=1
			fi
		fi
	done

	if [ $error -ne 0 ]; then
		exit 1
	fi
}


###############################################################################
# Set EEPROM boot-file setting for each node.
#
function set_eeprom_boot_file   #
{
	typeset index
	typeset node
	typeset bootfile
	typeset orig

	# For saving original setting
	unset ORIG_BOOT_FILE

	index=1
	while (( index <= NUM_NODES )); do
		node=${NODENAME_LIST[index]}

		# First get the original value
		orig=$($RSH_USED $node "$EEPROM boot-file")
		if [ $? -ne 0 ]; then
			print -u2 "ERROR: Can't get boot-file parameter from" \
				"node $node"
			exit 1
		fi

		# Set the boot-file only if the original boot-file is
		# different from the one we're going to set.
		diff -w <(echo "boot-file=$BOOT_ARGS") <(echo "$orig") \
			>/dev/null 2>&1
		if [ $? -ne 0 ]; then
			# Remember it so we can restore it later
			ORIG_BOOT_FILE[index]=$orig

			print "*** Setting boot-file on $node to '$BOOT_ARGS'"

			$RSH_USED $node "$EEPROM 'boot-file=$BOOT_ARGS'"
			if [ $? -ne 0 ]; then
				print -u2 "ERROR: Can't set boot-file" \
					"parameter on node $node"
				exit 1
			fi

	                $RSH_USED $node "$BOOTADM list-archive >/dev/null 2>&1"
			ret=$?
			if [ $ret -eq 0 ]; then
				print "  * Run bootadm update after setting boot-file to $BOOT_ARGS on node: $node"
	                	$RSH_USED $node "$BOOTADM update-archive"
				if [ $? -ne 0 ]; then
					print -u2 "EROR: Can't run bootadm after set boot-file" \
						"parameter on node $node"
					exit 1
				fi
			else
				print  " * bootadm update-archive not supported " \
					" on node $node: Setting boot-file to $BOOT_ARGS"
			fi
		fi

		(( index += 1 ))
	done

	# If the original boot-file parameter on one or more nodes had been
	# different from what was set, then force an initial reboot if
	# user didn't specify it.
	if (( ${#ORIG_BOOT_FILE[*]} > 0 && ! DO_INITIAL_REBOOT )); then
		print  "WARNING: Some nodes had boot-file different from" \
			"'$BOOT_ARGS'"
		print  "WARNING: Initial reboot of all nodes will be" \
			"performed"
		DO_INITIAL_REBOOT=1
	fi
}


###############################################################################
# Restore EEPROM boot-file setting (saved in set_eeprom_boot_file()) on
# each node
# Returns: 0 if successful, non-zero otherwise.
#
function restore_eeprom_boot_file   #
{
	typeset index
	typeset node
	typeset rslt=0

	print

	index=1
	while (( index <= NUM_NODES )); do
		if [ -z "${ORIG_BOOT_FILE[index]}" ]; then
			(( index += 1 ))
			continue
		fi

		# If there was no boot-file parameter before...
		if [[ "${ORIG_BOOT_FILE[index]}" = *'data not available'* ]]
		then
			ORIG_BOOT_FILE[index]='boot-file='
		fi

		node=${NODENAME_LIST[index]}
		print "*** Restoring boot-file parameter on $node to" \
			"'${ORIG_BOOT_FILE[index]#boot-file=}'"

		$PING $node 30 >/dev/null 2>&1
		if [ $? -eq 0 ]; then
			$RSH_USED $node "$EEPROM '${ORIG_BOOT_FILE[index]}'"
			if [ $? -ne 0 ]; then
				print -u2 "WARNING: Can't restore boot-file" \
					"parameter on node $node"
				rslt=1
			fi

	                ret=`$RSH_USED $node "$BOOTADM list-archive >/dev/null 2>&1;echo \\$?"`
			if [ $ret -eq 0 ]; then
				print "  * Run bootadm update restore boot-file on node: $node"
	                	$RSH_USED $node "$BOOTADM update-archive"
				if [ $? -ne 0 ]; then
					print -u2 "ERROR: Can't run bootadm after set boot-file" \
						"parameter on node $node"
					rslt=1
				fi
			else
				print " * bootadm update-archive not supported " \
					" on node $node: restore boot-file"
			fi
		else
			print -u2 "WARNING: Can't restore boot-file" \
				"parameter on node $node"
			print -u2 "WARNING: Node is dead"
			rslt=1
		fi

		(( index += 1 ))
	done

	return $rslt
}


###############################################################################
# Set up an rc file on each node to run mcboot everytime it boots,
# if requested by user
#
function set_mcboot_rcfile   #
{
	typeset node
	typeset vote=0

	# If user doesn't want mcboot, then skip it
	if [ "$DO_MCBOOT" -eq 0 ]; then
		return
	fi

	for node in ${NODENAME_LIST[*]}; do
		print "*** Setting node $node to boot with mcboot"

		# Note, sync is done below to ensure that after the next reboot
		# $MCBOOT_RCFILE exists.
		$RSH_USED $node "cp -f $MCBOOT_RCFILE_SRC $MCBOOT_RCFILE;" \
			"sync;sync;sync"
		if [ $? -ne 0 ]; then
			print -u2 "ERROR: Can't set node to run mcboot"
			exit 1
		fi

		# Check if node is currently running mcboot, clustering or none
		$RSH_USED $node "$MODINFO" | nawk '
			$6 == "orb" {orb = 1}		# orb is running
			/mcboot/ {mcboot = 1}		# mcboot is running
			END {
				if (mcboot) {
					# mcboot is already running: ok
					exit 1
				} else if (orb) {
					# clustering is running: need reboot
					exit 2
				} else {
					# nothing is running: start mcboot
					exit 4
				}
			}'
		(( vote |= $? ))
	done

	# Figure out if we need to start mcboot manually on all nodes,
	# reboot all nodes or do nothing.
	case $vote in
	1)	# All nodes are already running mcboot, so no need to
		# reboot or run mcboot manually
		;;
	4)	# All nodes are currently running nothing; start mcboot
		# manually, if initial reboot hasn't been scheduled
		if [ "$DO_INITIAL_REBOOT" -eq 0 ]; then
			start_mcboot
		fi
		;;
	*)	# Either all nodes are running clustering or they're running
		# different things.  Schedule initial reboot if necessary
		if [ "$DO_INITIAL_REBOOT" -eq 0 ]; then
			print -u2 "WARNING: Some nodes are not running mcboot"
			print -u2 "WARNING: Initial reboot of all nodes will" \
				"be performed"
			DO_INITIAL_REBOOT=1
		fi
		;;
	esac
}


###############################################################################
# Remove the mcboot rc file (planted by set_mcboot()) on each node,
# if mcboot is requested by user
# Returns: 0 if successful, non-zero otherwise.
#
function remove_mcboot_rcfile   #
{
	typeset node
	typeset rslt=0

	# If user doesn't want mcboot, then skip it
	if [ "$DO_MCBOOT" -eq 0 ]; then
		return 0
	fi

	print

	for node in ${NODENAME_LIST[*]}; do
		print "*** Removing mcboot startup on node $node"

		$PING $node 30 >/dev/null 2>&1
		if [ $? -eq 0 ]; then
			# Note, sync is done below to ensure that after the
			# next reboot $MCBOOT_RCFILE doesn't exist anymore.
			$RSH_USED $node "rm -f $MCBOOT_RCFILE; sync;sync;sync"
			if [ $? -ne 0 ]; then
				print -u2 "WARNING: Can't remove" \
					"$MCBOOT_RCFILE on node $node"
				rslt=1
			fi
		else
			print -u2 "WARNING: Can't remove $MCBOOT_RCFILE on" \
				"node $node"
			print -u2 "WARNING: Node is dead"
			rslt=1
		fi
	done

	return $rslt
}


###############################################################################
# start_mcboot
# Start mcboot manually on all nodes
#
function start_mcboot   #
{
	typeset node

	for node in ${NODENAME_LIST[*]}; do
		print " ** Starting mcboot on node $node"
		$RSH_USED $node "$MCBOOT" 2>&1 | fgrep 'BOOT FAILED'
		if [ $? -eq 0 ]; then
			print -u2 "ERROR: mcboot failed on node $node"
			exit 1
		fi
	done
}


###############################################################################
# wait_for_node
#
function wait_for_node   # <node_name>
{
	typeset node=$1

	#
	# Wait for a node to re-join the cluster
	#
	# Poll until we can rsh
#	print " ** Waiting for node $node to re-join cluster"
#	print " ** If node hangs during reboot, test will hang"

	until $RSH_USED_SHORT $node true 1>/dev/null 2>&1; do
		sleep 2				# sleep some time
	done

	# If user wanted mcboot, then also wait until the ORB is up.
	if [ "$DO_MCBOOT" -ne 0 ]; then
		typeset ret

		while true; do
			ret=$($RSH_USED_SHORT $node "$MODINFO | fgrep mcboot")
			if [ -z "$ret" ]; then
				sleep 2
			else
				break
			fi
		done
	fi
}


###############################################################################
# check_down_nodes
# Verifies that only the nodes that user expects to be down are down
# and nodes not supposed to go down weren't rebooted during the tests.
# Returns: 0 if successful, non-zero otherwise.
#
function check_down_nodes   # [node_name ...]
{
	typeset i
	typeset node nodeidx
	typeset to_wait
	typeset rslt=0

	unset NODES_CURRENT_REBOOT
	#
	# Get the all nodes' current reboot time 
	# This will be used to compare with NODES_LAST_REBOOT before test
	# to check if the node is rebooted during the test
	#
	nodeidx=1
	while (( nodeidx <= NUM_NODES )); do
		node=${TEST_NODENAME_LIST[nodeidx]}
		NODES_CURRENT_REBOOT[nodeidx]=$($RSH_USED $node "$WHO_CMD" \
			2>&1 | awk '{print $4 " " $5 " " $6}')
		(( nodeidx += 1 ))
	done

	if (( $# > 0 )); then
		print " ** Verifying only node(s) $@ went down"

	#
	# Comment it out since we already store the info
	# in global variable EXPECTED_DOWN
	#
#		for node in "$@"; do
#			nodeidx=$(nodename_to_nodeidx $node)
#			expected_list[nodeidx]=1
#		done
	fi

	# Verify that only nodes user expects to be down did go down
	nodeidx=1
	while (( nodeidx <= NUM_NODES )); do
		node=${TEST_NODENAME_LIST[nodeidx]}

		#
		# If user expects this node to go down
		#
		if [ "${EXPECTED_DOWN[nodeidx]}" -ne 0 ]; then
			if [[ "${NODES_LAST_REBOOT[nodeidx]}" = \
				"${NODES_CURRENT_REBOOT[nodeidx]}" ]]; then
				print "+++ FAIL: Node $node stayed up!" \
					"Expected to go down"
				rslt=1
			fi
		else
			# If user doesn't expect this node to go down
			if [ $IGNORE_REBOOTS  -eq 0 ]; then
				if [[ "${NODES_LAST_REBOOT[nodeidx]}" != \
					"${NODES_CURRENT_REBOOT[nodeidx]}" ]]; then
					print "+++ FAIL: Node $node went down!" \
						"Expected to stay up"
					rslt=1
				fi
			fi
		fi

		(( nodeidx += 1 ))
	done

	return $rslt
}


###############################################################################
# test_output
# Searches the specified file(s) (if not specified or '-' then stdin is used)
# for certain keyword to indicate whether a test/entry has passed or failed.
# The file usually contains the output of a client.
#
# Returns:
#	0 -- if the test/entry passed (there is a PASS message and
#	     no FAIL/ERROR messages)
#	1 -- if the test/entry failed (there is at least one FAIL/ERROR
#	     message)
#	2 -- neither (no PASS, FAIL/ERROR messages found)
#
function test_output   # [file...]
{
	# In case $file is stdin, we can't use the grep family to search
	# for failure and then search for pass messages, since that would
	# mean reading stdin twice.  So we'll use nawk instead.
	nawk -v logfile=${TEST_LOG:-/dev/null} '
		# Failure/error messages may start with "+++ "
		/^\+\+\+ (FAIL|ERROR):/ {
			fail = 1
			print				# copy to stdout
		}
		/^(FAIL|ERROR):/ {
			fail = 1
			sub("^", "+++ ")
			print				# copy to stdout
		}

		# Pass messages may start with "--- "
		/^(--- )?PASS:/ {
			pass = 1
		}

		# Echo messages starting with "INFO:"
		/^INFO:/ {
			sub("^INFO:", "")		# filter out "INFO:"
			print				# copy to stdout
		}

		# Copy all messages to logfile
		{ print >> logfile }

		END {
			if (fail) {
				# There was at least one FAIL/ERROR message
				exit 1
			} else if (pass) {
				# No FAIL/ERROR, but there was PASS messages
				exit 0
			} else {
				# There were neither FAIL/ERROR nor PASS msgs
				exit 2
			}
		}' "$@"

	return $?
}


###############################################################################
# load_server
# Run a server on one or more nodes
# Returns: 0 if successful, non-zero otherwise.
#
function load_server   # [load_option...] [entry [entry_arg...]]
{
	typeset -r log_suffix=serverloadlog
	typeset server_nodes
	typeset load_mode
	typeset file
	typeset user=$DEF_USER
	typeset entry
	typeset node
	typeset kernel_args
	typeset rslt
	typeset arg
	typeset opt
	typeset tmp_logfile
	typeset rsh_rslt

	# Set default load mode
	case "$RUN_MODE" in
	K32|K64|K)
		load_mode=K
		;;
	*)
		load_mode=U
		;;
	esac

	while getopts ':n:m:f:u:' opt; do
		case $opt in
		n)
			# Collect all -n option arguments, each of which
			# could be a comma-separated list of nodes.
			server_nodes="$server_nodes $OPTARG"
			;;
		m)
			load_mode=$OPTARG

			# Verify load_mode
			case $load_mode in
			K|U)	# okay
				;;
			*)
				print -u2 "ERROR: SERVER: illegal -m" \
					"option argument: $load_mode"
				return 1
				;;
			esac
			;;
		f)
			file=$OPTARG
			;;
		u)
			user=$OPTARG
			;;
		\?)
			print -u2 "ERROR: SERVER: Option $OPTARG unknown"
			return 1
			;;
		:)
			print -u2 "ERROR: SERVER: Option $OPTARG" \
				"needs argument"
			return 1
			;;
		*)
			print -u2 "ERROR: SERVER: Option $opt unrecognized"
			return 1
			;;
		esac
	done
	shift 'OPTIND - 1'

	# Resolve all node specifications
	server_nodes=$(parse_node_specs $server_nodes)
	if [ $? -ne 0 ]; then
		return 1
	fi

	# Make sure at least one node was specified
	if [ -z "$server_nodes" ]; then
		print -u2 "ERROR: SERVER: No node is specified"
		return 1
	fi

	# If no file was defined.. assign one
	if [ -z "$file" ]; then
		case $load_mode in
		K)	file=$DEF_KFILE
			;;
		*)	file=$DEF_UFILE
			;;
		esac
	fi

	# If there are arguments left, then the first is assumed to be the
	# name of the entry, and the rest will be passed to the server.
	# Otherwise, the file name will be used as the entry name.
	if (( $# > 0 )); then
		entry=$1
		shift 1
	else
		entry=${file##*/}
	fi

	# For kernel module, construct arguments which will be part of the
	# module filename.
	if [ "$load_mode" = 'K' ]; then
		for arg in $*; do
			kernel_args="$kernel_args.$arg"
		done
	fi

	# Load the server on one or more nodes
	rslt=0
	for node in $server_nodes; do
		tmp_logfile=$OUTPUTDIR/server.load.$entry.$node.$log_suffix
		rsh_rslt=0
		case $load_mode in
		K)	# KERNEL MODE
			print "    Loading: $entry (kernel) with args \"$@\"" \
				"on node $node"

			$RSH_USED $node "$KSERVER_LOAD $DEF_KPATH $file" \
				"$entry $kernel_args" > $tmp_logfile 2>&1
			rsh_rslt=$?
			;;
	
		U)	# USER MODE
			print "    Loading: $entry (user) with args \"$@\"" \
				"on node $node"

			# XXX - Need to call as USER
			$RSH_USED $node "$USERVER_LOAD $DEF_UPATH $file $entry" \
				"$@" > $tmp_logfile 2>&1
			rsh_rslt=$?
			;;
		esac
	done
	
	test_output $OUTPUTDIR/server.load.*.$log_suffix
	rslt=$?

	case $rslt in
	0)	# server(s) reported pass
		;;
	1)	# there was at least one FAIL/ERROR message
		# these messages would have been printed out already
		;;
	*)	# there were neither FAIL/ERROR nor PASS messages
		# so print a canned message
		if [ $rsh_rslt -eq 99 ]
		then
			print "+++ RSH WARNING: Loading \"$entry\" got RSH timeout, you may need to rerun this test case."
		fi
		print "+++ FAIL: Loading \"$entry\" failed"
		rslt=1
		;;
	esac

	# Remove all server logs
	rm -f $OUTPUTDIR/server.load.*.$log_suffix 2>/dev/null
	return $rslt
}


###############################################################################
# unload_server
# Unload a server from one or more nodes
# Returns: 0 if successful, non-zero otherwise.
#
function unload_server   # [load_option...] [entry [entry_arg...]]
{
	typeset -r log_suffix=serverunloadlog
	typeset server_nodes
	typeset load_mode
	typeset file
	typeset entry
	typeset kernel_args
	typeset node nodeidx
	typeset arg
	typeset rslt
	typeset opt
	typeset tmp_logfile
	typeset rsh_rslt

	# Set default load mode
	case "$RUN_MODE" in
	K32|K64|K)
		load_mode=K
		;;
	*)
		load_mode=U
		;;
	esac

	while getopts ':n:m:f:u:' opt; do
		case $opt in
		n)
			# Collect all -n option arguments, each of which
			# could be a comma-separated list of nodes.
			server_nodes="$server_nodes $OPTARG"
			;;
		m)
			load_mode=$OPTARG

			# Verify load_mode
			case $load_mode in
			K|U)	# okay
				;;
			*)
				print -u2 "ERROR: SERVER: illegal -m"
					"option argument: $load_mode"
				return 1
				;;
			esac
			;;
		f)
			file=$OPTARG
			;;
		u)
			# Ignore
			;;
		\?)
			print -u2 "ERROR: SERVER: Option $OPTARG unknown"
			return 1
			;;
		:)
			print -u2 "ERROR: SERVER: Option $OPTARG" \
				"needs argument"
			return 1
			;;
		*)
			print -u2 "ERROR: SERVER: Option $opt unrecognized"
			return 1
			;;
		esac
	done
	shift 'OPTIND - 1'

	# Resolve all node specifications
	server_nodes=$(parse_node_specs $server_nodes)
	if [ $? -ne 0 ]; then
		return 1
	fi

	# Make sure a node was specified
	if [ -z "$server_nodes" ]; then
		print -u2 "ERROR: SERVER: No node is specified"
		return 1
	fi

	# If no file was defined.. assign one
	if [ -z "$file" ]; then
		case $load_mode in
		K)
			file=$DEF_KFILE
			;;
		*)
			file=$DEF_UFILE
			;;
		esac
	fi

	# If there are arguments left, then the first is assumed to be the
	# name of the entry, and the rest are arguments to the server.
	# Otherwise, the file name will be used as the entry name.
	if (( $# > 0 )); then
		entry=$1
		shift 1
	else
		entry=${file##*/}
	fi

	# For kernel module, construct arguments which is part of the
	# module filename.
	if [ "$load_mode" = 'K' ]; then
		for arg in $*; do
			kernel_args="$kernel_args.$arg"
		done
	fi

	# Unload the server from one or more nodes
	rslt=0
	for node in $server_nodes; do
		nodeidx=$(nodename_to_nodeidx $node)

		# If this node went down, no need to unload server
		if [ "${DOWN_LIST[nodeidx]}" -ne 0 ]; then
			print "  * Skipping unload of $entry on node $node" \
				"(it was down)"
			continue
		fi

		# If this node is down right now, no need to unload server
		$PING $node 30 >/dev/null 2>&1
		if [ $? -ne 0 ]; then
			print "  * Skipping unload of $entry on node $node" \
				"(it went down)"
			continue
		fi

		print "    Unloading: $entry from node $node"

		rsh_rslt=0
		tmp_logfile=$OUTPUTDIR/server.unload.$entry.$node.$log_suffix
		case $load_mode in
		K)	# KERNEL MODE
			$RSH_USED $node "$KSERVER_UNLOAD $entry $kernel_args" > $tmp_logfile 2>&1
			rsh_rslt=$?
			;;

		*)	# USER MODE
			$RSH_USED $node "$USERVER_UNLOAD $entry" > $tmp_logfile 2>&1
			rsh_rslt=$?
			;;
		esac
	done

	test_output $OUTPUTDIR/server.unload.*.$log_suffix
	rslt=$?
	case $rslt in
	0)	# module unloading succeeded
		;;
	1)	# there was at least one FAIL/ERROR message
		# these messages would have been printed out already
		;;
	*)	# there were neither FAIL/ERROR nor PASS messages
		# so print a canned message
		if [ $rsh_rslt -eq 99 ]
		then
			print "+++ RSH WARNING: Unloading \"$entry\" got RSH timeout, you may need to rerun this test case."
		fi
		print "+++ FAIL: Unload of module $entry failed"
		rslt=1
		;;
	esac

	# Remove all server logs
	rm -f $OUTPUTDIR/server.unload.*.$log_suffix 2>/dev/null

	return $rslt
}

###############################################################################
# reset_node_vote 
# Arg: node_list which vote need to set to 0
# It will first set all nodes' vote to 1,
# then if $@ not empty, set node vote in $@ to 0.
# This function is called both on setting node vote of down_nodes
# to 0 if more than 1 node expected to rebooting during on client load,
# or at the end of load_client, need to reset all nodes' vote back to 1.
# If user doesn't want to shuffle_nodes, this function will never
# got called and user is responsible to get correct node vote before
# running tests.
# Returns: 0 if successful, non-zero otherwise.
#
function reset_node_vote # down_nodes list
{
	typeset nodeidx
	typeset rslt=0
	typeset down_nodes
	typeset node
	typeset vote
	typeset i

	down_nodes=$*
	nodeidx=1
	#
	# First, set all node's vote to 1
	#
	while (( nodeidx <= NUM_NODES ))
	do
		node=${TEST_NODENAME_LIST[$nodeidx]}
		i=0
		while (( $i < $HATIMERUN_RETRY ))
		do 
			node=${TEST_NODENAME_LIST[$nodeidx]}
			rslt=`$RSH_USED_SHORT ${TEST_NODENAME_LIST[1]} "$SCCONF -c -q " \
				"node=$node,defaultvote=1 " \
				"1>/dev/null 2>&1;echo \\$?"`
			if [ -z $rslt ]; then
				vote=
				eval vote=$($RSH_USED_SHORT ${TEST_NODENAME_LIST[1]} "$SCSTAT -q" 2>&1 | grep "Node votes:" | grep "$node" | awk '{print $4}')
				if [ -z $vote ]
				then
					sleep 5
					i=`expr $i + 1`
					rslt=99
					continue
				fi
				if [ $vote -ne 1 ]; then
					i=`expr $i + 1`
					rslt=99
					sleep 5
					continue
				else
					rslt=0
					break
				fi
			else
				break
			fi
		done
		if [ $rslt -ne 0 ]; then
			if [ $rslt -eq 99 ]; then
				print "+++ RSH WARNING: Set node vote of $node to 1 get RSH timeout, you may need to rerun this test case."
			else
				print -u2 "ERROR: set node ovte of $node to 1 failed with return code $rslt."
			fi
			return 1
		fi
		(( nodeidx += 1 ))
	done
	#
	# If $@ not empty, set node vote in $@ to 0.
	# If $@ is empty, following code do nothing.
	#
	for node in $down_nodes; do
		i=0
		while (( $i < $HATIMERUN_RETRY ))
		do 
			rslt=`$RSH_USED_SHORT ${TEST_NODENAME_LIST[1]} "$SCCONF -c -q " \
				"node=$node,defaultvote=0 1>/dev/null 2>&1;echo \\$?"`
			if [ -z $rslt ]; then
				vote=
				eval vote=$($RSH_USED_SHORT ${TEST_NODENAME_LIST[1]} "$SCSTAT -q" 2>&1 | grep "Node votes:" | grep "$node" | awk '{print $4}')
				if [ -z $vote ]
				then
					sleep 5
					i=`expr $i + 1`
					rslt=99
					continue
				fi
				if [ $vote -ne 0 ]; then
					sleep 5
					i=`expr $i + 1`
					rslt=99
					continue
				else
					rslt=0
					break
				fi
			else
				break
			fi
		done
		if [ $rslt -ne 0 ]; then
			if [ $rslt -eq 99 ]; then
				print "+++ RSH WARNING: Set node vote of $node to 0 get RSH timeout, you may need to rerun this test case."
			else
				print -u2 "ERROR: set node ovte of $node to 0 failed with return code $rslt."
			fi
			return 1
		fi
	done
	return 0
} 

###############################################################################
# load_client
# Run a client on one or more nodes in the background and monitor the nodes,
# marking which nodes go down
# Returns: 0 if successful, non-zero otherwise.
#
function load_client   # [load_option...] [entry [entry_arg...]]
{
	typeset -r log_suffix=clientlog

	typeset client_nodes
	typeset load_mode
	typeset file
	typeset user=$DEF_USER
	typeset load_error=0
	typeset reboot_after_client_load=0
	typeset down_nodes
	typeset node
	typeset entry
	typeset pid
	typeset down_clients
	typeset clientidx num_clients
	typeset client_pid_list client_node_list
	typeset tmp_logfile
	typeset kernel_args
	typeset rslt=0
	typeset arg
	typeset opt
	typeset rsh_rslt

	num_down_nodes=0
	# Set default load mode
	case "$RUN_MODE" in
	K32|K64|K)
		load_mode=K
		;;
	*)
		load_mode=U
		;;
	esac

	while getopts ':n:m:f:u:xd:r' opt; do
		case $opt in
		n)
			# Collect all -n option arguments, each of which
			# could be a comma-separated list of nodes.
			client_nodes="$client_nodes $OPTARG"
			;;
		m)
			load_mode=$OPTARG

			# Verify load_mode
			case $load_mode in
			K|U)	# okay
				;;
			*)
				print -u2 "ERROR: CLIENT: illegal -m"
					"option argument: $load_mode"
				return 1
				;;
			esac
			;;
		f)
			file=$OPTARG
			;;
		u)
			user=$OPTARG
			;;
		x)
			# This means that we expect a load_error
			load_error=1
			;;
		d)
			# Collect all -d option arguments, each of which
			# could be a comma-separated list of nodes expected
			# to go down
			down_nodes="$down_nodes $OPTARG"
			;;
		r)
			reboot_after_client_load=1
			;;
		\?)
			print -u2 "ERROR: CLIENT: Option $OPTARG unknown"
			return 1
			;;
		:)
			print -u2 "ERROR: CLIENT: Option $OPTARG" \
				"needs argument"
			return 1
			;;
		*)
			print -u2 "ERROR: CLIENT: Option $opt unrecognized"
			return 1
			;;
		esac
	done
	shift 'OPTIND - 1'

	# Resolve node specifications
	client_nodes=$(parse_node_specs $client_nodes)
	if [ $? -ne 0 ]; then
		return 1
	fi

	# Make sure a node was specified
	if [ -z "$client_nodes" ]; then
		print -u2 "ERROR: CLIENT: No node is specified"
		return 1
	fi

	# If no file was defined.. assign one

	#
	# get nodenames of down_nodes and num of down nodes
	# of current client
	#
	down_nodes=$(parse_node_specs $down_nodes)
	if [ $? -ne 0 ]; then
		return 1
	fi
	num_down_nodes=`echo $down_nodes | awk '{print NF}'`

	#
	# For saving list of nodes that go down during this client's run
	# EXPECTED_DOWN will be used in check_down_nodes function
	# current_down will be used later to save actual down_nodes
	# while loading this client.
	#
	unset EXPECTED_DOWN current_down 
	for node in $down_nodes; do
		nodeidx=$(nodename_to_nodeidx $node)
		EXPECTED_DOWN[nodeidx]=1
		current_down[nodeidx]=0
	done

	# If no file was defined.. assign one
	if [ -z "$file" ]; then
		case $load_mode in
		K)	file=$DEF_KFILE
			;;
		*)	file=$DEF_UFILE
			;;
		esac
	fi

	# If there are arguments left, then the first is assumed to be the
	# name of the entry, and the rest will be passed to the client.
	# Otherwise, the file name will be used as the entry name.
	if (( $# > 0 )); then
		entry=$1
		shift 1
	else
		entry=${file##*/}
	fi

	# For kernel module, construct arguments which will be part of the
	# module filename.
	if [ "$load_mode" = 'K' ]; then
		for arg in $*; do
			kernel_args="$kernel_args.$arg"
		done
	fi

	#
	# Get the all nodes' last reboot time 
	# This will be used to compare with reboot_time after test
	# to check if the node is rebooted during the test
	#
	unset NODES_LAST_REBOOT NODES_CURRENT_REBOOT
	nodeidx=1
	while (( nodeidx <= NUM_NODES )); do
		node=${TEST_NODENAME_LIST[nodeidx]}
		NODES_LAST_REBOOT[nodeidx]=$($RSH_USED $node "$WHO_CMD" \
			2>&1 | awk '{print $4 " " $5 " " $6}')
		(( nodeidx += 1 ))
	done

	#
	# if test expecting more than one nodes to go down, it should
	# reset the node vote of down_nodes to 0.
	#
	if [ $IS_SHUFFLE_OK -eq 1 ]; then
		if [ $num_down_nodes -gt 1 -a $num_down_nodes -lt $NUM_NODES ]; then
			#
			# For quorum tests, won't need to reset nodes' vote
			#
			if [[ $QUORUM_INDEX -eq 0 ]]
			then
				#
				# Reset rebooting nodes' vote to 0 on all tests
				# except quorum tests
				#
				echo "DEBUG: reset $down_nodes vote to 0"
				eval reset_node_vote $down_nodes 
				if [ $? -ne 0 ]; then
					return 1
				fi
			fi
		fi
	fi
	# If all the nodes are rebooted then disable
	# cmm path monitoring so that nodes do not
	# panic due to loss of quorum, instead reboot cleanly.
	if [ $num_down_nodes -eq $NUM_NODES ]; then
		#
		# For quorum tests, won't do anything
		#
		if [[ $QUORUM_INDEX -eq 0 ]]
		then
			#Disable CMM monitoring
			print " ** Will disable cmm monitor"
			$RSH_USED $CLUSTER_NAME "echo '/usr/cluster/lib/sc/cmm_ctl \
			    -d' | at now" > /dev/null 2>&1
			sleep 10
			#Disable CMM failfasts
			print " ** Will disable cmm failfast"
			$RSH_USED $CLUSTER_NAME "echo '/usr/cluster/lib/sc/cmm_ctl \
			    -f' | at now" > /dev/null 2>&1
			sleep 10
		fi
	fi
	# Load the client on one or more nodes
	num_clients=0
	for node in $client_nodes; do
		client_node_list[num_clients]=$node

		# Temporary file to save this client output
		tmp_logfile=$OUTPUTDIR/client.load.$entry.$node.$log_suffix
		rsh_rslt=0
		case $load_mode in
		K)	# KERNEL MODE
			print "    Loading: $entry (kernel) with args \"$@\"" \
				"on node $node"

			$RSH_USED $node "$KCLIENT_LOAD $DEF_KPATH $file $entry" \
				"$kernel_args" > $tmp_logfile 2>&1 &
			rsh_rslt=$?
			;;

		U)	# USER MODE
			print "    Loading: $entry (user) with args \"$@\"" \
				"on node $node"

			# XXX - Need to call as USER
			$RSH_USED $node "$UCLIENT_LOAD $DEF_UPATH $file $entry" \
				"$@" > $tmp_logfile 2>&1 &
			rsh_rslt=$?
			;;
		esac

		client_pid_list[num_clients]=$!
		(( num_clients += 1 ))
	done

	# Wait for sometime to let the client operation sync in
	sleep 30

	#
	# Wait long enough time to make sure all
	# down_nodes are actually go down
	#
	max_sleep=500
	sleep_time=1
	num_current_down=0
	while (( sleep_time <= max_sleep )); do
		for node in $down_nodes; do
			nodeidx=$(nodename_to_nodeidx $node)
			if $PING $node 20 >/dev/null; then
				# Node is up
				# It means two possible cases:
				#   1) node did not go down yet
				#   2) node went down but came back up already
				NODES_CURRENT_REBOOT[nodeidx]=$($RSH_USED $node "$WHO_CMD" \
				    2>&1 | awk '{print $4 " " $5 " " $6}')
				#
				# Check if node did go DOWN.
				#
				if [[ "${NODES_LAST_REBOOT[nodeidx]}" != \
					"${NODES_CURRENT_REBOOT[nodeidx]}" ]]; then
					# So node actually did go down and came back up
					if [ ${current_down[nodeidx]} -eq 0 ]; then
						# first time check the node is down
						current_down[nodeidx]=1
						(( num_current_down += 1 ))
					fi
				else
					# So node did not go down yet. Keep checking.
					continue
				fi
			else
				# Node is down
				if [ ${current_down[nodeidx]} -eq 0 ]; then
					# first time check the node is down
					current_down[nodeidx]=1
					(( num_current_down += 1 ))
				fi
			fi
		done
		if [ $num_current_down -eq $num_down_nodes ]; then
			break
		fi
		sleep 5
		(( sleep_time += 1 ))
	done

	# Wait to make sure all cluster nodes come up
	# before resuming with next client.  This especially important if
	# mcboot is selected.
	for node in $down_nodes; do
		print " ** Waiting for node $node to re-join cluster"
		print " ** If node hangs during reboot, test will hang"
		wait_for_node $node 
	done
	nodeidx=1
	while (( nodeidx <= NUM_NODES )); do
		#
		# comment it out
		#
#		if [ "${CURRENT_DOWN[nodeidx]}" -ne 0 ]; then
#			wait_for_node ${TEST_NODENAME_LIST[nodeidx]}
#		fi
		node=${TEST_NODENAME_LIST[nodeidx]}
		print " ** Make sure node $node is up in the cluster after tests."
		print " ** If node hangs during reboot, test will hang"
		wait_for_node $node
		(( nodeidx += 1 ))
	done

	#
	# Check output of all clients
	# We need to check client output before do client cleanup
	# to make sure client actually finished.
	#
	max_sleep=50
	sleep_time=1
	rslt=0
	while (( sleep_time <= max_sleep )); do
		test_output $OUTPUTDIR/client.load.*.$log_suffix
		ret=$?

#		echo "test_output get $ret at $sleep_time check"
		if [ "$load_error" -ne 0 ]; then
			if [ $ret -eq 0 -a -z "$down_clients" ]; then
				print "+++ FAIL: Expected error during load" \
					"did not occur"
				rslt=1
			else
				#
				# It's possible the client doesn't finished yet
				#
				if [ $ret -eq 2 ]; then
					sleep 60
				fi
				print " ** Load failure was expected"
				rslt=0
			fi
			break
		else
			# If any client died... and we care
			if [ -z $IGNORE_REBOOTS ]; then
				if [ -n "$down_clients" ]; then
					print "+++ FAIL: client node(s)" $down_clients \
						"unexpectedly died/rebooted"
					rslt=1
					break
				fi
			fi
			if [ $ret -eq 0 -o $ret -eq 1 ]; then
				rslt=$ret
				break
			fi
		fi
		#
		# Probably the client didn't finished yet, sleep some time to check again
		#
		sleep 10 
		(( sleep_time += 1 ))
	done

	# Monitor ALL nodes in the cluster until all clients return
	while true; do
		nodeidx=1
		while (( nodeidx <= NUM_NODES )); do
			node=${TEST_NODENAME_LIST[nodeidx]}

			# If this node is one of the client nodes, kill the
			# corresponding rsh; otherwise rsh might not return
			# for a loooong time.
			clientidx=0
			while (( clientidx < num_clients )); do
				if [ $node = "${client_node_list[clientidx]}" ]
				then
					down_clients="$down_clients $node"

					# Make sure node is checked once only
					unset client_node_list[clientidx]

					pid=${client_pid_list[clientidx]}
					kill -KILL $pid 2>/dev/null

					break
				fi
				(( clientidx += 1 ))
			done

			(( nodeidx += 1 ))
		done

		# If no more clients, get out.  Note, this means that if
		# during the previous pass of this loop the client check loop
		# below found all clients finished, then we would've done
		# one more pass through the node check loop above.  This is
		# for handling the possibility that an rsh might return right
		# away after the node died but it was too late for the node
		# check above to notice it.
		if [ ${#client_pid_list[*]} -eq 0 ]; then
			break
		fi

		# Check health of clients
		clientidx=0
		while (( clientidx < num_clients )); do
			pid=${client_pid_list[clientidx]}
			if [ -n "$pid" ]; then
				kill -0 $pid 2>/dev/null
				if [ $? -ne 0 ]; then
					# Client is gone
					wait $pid	# leave no zombies
					unset client_pid_list[clientidx]
				fi
			fi
			(( clientidx += 1 ))
		done

		sleep 2					# rest for a bit
	done

	#
	# Check the test result
	#
	case $rslt in

		0)	# client(s) reported pass
			;;
		1)	# there was at least one FAIL/ERROR message
			# these messages would have been printed out already
			;;
		*)	# there were neither FAIL/ERROR nor PASS messages
			# so print a canned message
			if [ $rsh_rslt -eq 99 ]
			then
				print "+++ RSH WARNING: Loading \"$entry\" got RSH timeout, you may need to rerun this test case."
			fi
			print "+++ FAIL: Loading \"$entry\" failed"
			rslt=1
			;;
	esac

	# Remove all client logs
	rm -f $OUTPUTDIR/client.load.*.$log_suffix 2>/dev/null

	#
	# Verify nodes expected to go down were really down
	# and nodes who not expected to go down weren't rebooted
	# during the tests
	if ! check_down_nodes $down_nodes
	then
		print " ** Will reboot all nodes due to error in test"

		# If requested, prompt and wait for user before continuing.
		error_wait

		REBOOT_AFTER=1
		rslt=1
	elif [ "$reboot_after_client_load" -ne 0 ]
	then
		# The client spec has the reboot flag on, reboot all nodes.
		if ! reboot_all_nodes
		then
			ABORT_TEST=1
			rslt=1
		fi
	fi

	return $rslt
}

###############################################################################
# service_shutdown
# shutdown HA services
# Returns: 0 if successful, non-zero otherwise.
#
function service_shutdown   # [service...]
{
	typeset -r log_suffix=svclog
	typeset svc_name
	typeset rslt=0
	typeset tmp_logfile
	typeset rsh_rslt=0

	for svc_name in "$@"; do
		tmp_logfile=$OUTPUTDIR/service.${TEST_NODENAME_LIST[1]}.$svc_name.$log_suffix
		print "    Shutting down service: $svc_name"
		$RSH_USED ${TEST_NODENAME_LIST[1]} "$SVC_SHUTDOWN $svc_name 0" > $tmp_logfile 2>&1
		rsh_rslt=$?
		test_output $tmp_logfile 
		rslt=$?
		case $rslt in
		0)	# shutdown succeeded
			;;
		1)	# there was at least one FAIL/ERROR message
			# these message would have been printed out already
			rslt=1
			;;
		*)	# there were neither FAIL/ERROR nor PASS messages
			# so print a canned message
			if [ $rsh_rslt -eq 99 ]
			then
				print "+++ RSH WARNING: svc_shutdown \"$svc_name\" got RSH timeout, you may need to rerun this test case."
			fi
			print " ** Shutdown of service $svc_name failed"
			rslt=1
			;;
		esac
	done

	rm -f $OUTPUTDIR/service.${TEST_NODENAME_LIST[1]}.*.$log_suffix 2>/dev/null
	return $rslt
}


###############################################################################
# svc_shutdown
# Will shutdown ha services
# Returns: 0 if successful, non-zero otherwise.
#
function svc_shutdown   #
{
	typeset index=1

	while (( index <= SVC_SHUTDOWN_INDEX )); do
		eval service_shutdown "${SVC_SHUTDOWN_LIST[index]}"
		if [ $? -ne 0 ]; then
			return 1
		fi
		(( index += 1 ))
	done

	return 0
}


#############################################################################
#
# If $ERROR_WAIT is nonzero the user is prompted to press the <RETURN> key
# before continuing.
#
# Normally used in cases where all nodes must be rebooted because some serious
# error/failure has been detected.  By prompting, the user is given a chance
# to debug the error/failure before continuing with the test.
#
function error_wait   #
{
	if (( ERROR_WAIT ))
	then
		{
			print '\a'
			print -n '>>> AN ERROR HAS OCCURED. PRESS RETURN' \
				'TO CONTINUE '
			read
		} <> /dev/tty
	fi
}



###############################################################################
# reboot_all_nodes
#
# Reboot the repl nodes
# Returns: 0 if successful, non-zero otherwise.
#
function reboot_all_nodes   #
{
	typeset node 
	typeset index

	print " ** Make sure all nodes are up" 

	index=1                                 # start at node 1
	while (( index <= NUM_NODES )); do
		node=${TEST_NODENAME_LIST[index]}
		wait_for_node $node
		(( index += 1 ))
		sleep 15
	done

	# Get the all nodes' last reboot time
	# This will be used to compare with reboot_time after reboot
	# to check if the node is rebooted SUCCESSFULLY
	#
	unset NODES_LAST_REBOOT NODES_CURRENT_REBOOT
	nodeidx=1
	while (( nodeidx <= NUM_NODES )); do
		node=${TEST_NODENAME_LIST[nodeidx]}
		NODES_LAST_REBOOT[nodeidx]=$($RSH_USED $node "$WHO_CMD" \
		    2>&1 | awk '{print $4 " " $5 " " $6}')
		(( nodeidx += 1 ))
	done


	print " ** Rebooting all nodes one by one"

	#Disable CMM monitoring
	print " ** Will disable cmm monitor"
	$RSH_USED $CLUSTER_NAME "echo '/usr/cluster/lib/sc/cmm_ctl -d' | at now" > /dev/null 2>&1
	sleep 10
	#Disable CMM failfasts
	print " ** Will disable cmm failfast"
	$RSH_USED $CLUSTER_NAME "echo '/usr/cluster/lib/sc/cmm_ctl -f' | at now" > /dev/null 2>&1
	sleep 10

	index=1                                 # start at node 1
	while (( index <= NUM_NODES )); do
		node=${TEST_NODENAME_LIST[index]}
		print "  * Rebooting node: $node"

		# This at job will allow rsh to return inmediately
#		$RSH $node "echo 'sleep 5;$REBOOT' | at now" >/dev/null 2>&1
		$RSH_USED $node "echo '/usr/cluster/orbtest/fi_support/daemonize /sbin/uadmin 1 1' | at now" >/dev/null 2>&1

		# Sleep 15 seconds to let the node down
		sleep 5

#		# Wait for the rebooted node to come back up
#		wait_for_node ${TEST_NODENAME_LIST[index]}

		(( index += 1 ))
	done
	index=1                                 # start at node 1
	while (( index <= NUM_NODES )); do
		# Wait for the rebooted node to come back up
		wait_for_node ${TEST_NODENAME_LIST[index]}
		(( index += 1 ))
	done
	#
	# Get the all nodes' current reboot time
	# This will be used to compare with NODES_LAST_REBOOT
	# when nodes come up. to check if the node is rebooted during
	# the test
	#
	nodeidx=1
	while (( nodeidx <= NUM_NODES )); do
		node=${TEST_NODENAME_LIST[nodeidx]}
		NODES_CURRENT_REBOOT[nodeidx]=$($RSH_USED $node "$WHO_CMD" \
		    2>&1 | awk '{print $4 " " $5 " " $6}')
		#
		# Check if node did go DOWN.
		#
		# Verify that only nodes user expects to be down did go down
		if [[ "${NODES_LAST_REBOOT[nodeidx]}" = \
			"${NODES_CURRENT_REBOOT[nodeidx]}" ]]; then
			print "+++ FAIL: Node $node stayed up!" \
			    "Expected to go down"
			rslt=1
		fi
		(( nodeidx += 1 ))
	done
        #
	# If node reboot Fails	enable cmm and failfast. To avoid  undefined
	# behaviour of other tests.
        #
	if [[ $rslt -eq 1 ]]
	then
		#Enable CMM monitoring
		print " ** Will enable cmm monitor on all cluster nodes."
		$RSH_USED $CLUSTER_NAME "echo '/usr/cluster/lib/sc/cmm_ctl -e' | \
		    at now" > /dev/null 2>&1
		sleep 10
		#Disable CMM failfasts
		print " ** Will enable cmm failfast on all cluster nodes"
		$RSH_USED $CLUSTER_NAME "echo '/usr/cluster/lib/sc/cmm_ctl -F' | \
		    at now" > /dev/null 2>&1
		sleep 10
	fi
	return 0
}

###############################################################################
# set_num_secs
#
#
function set_num_secs   # [service_name] [desired_num_secs]
{
	typeset svc_name=$1
	typeset num_secs=$2
        typeset rslt=0
	typeset i
	typeset numSec

	print "    Set ${svc_name}'s desired number of secondaries to " \
		"${num_secs}"

	i=0
	while (( i < $HATIMERUN_RETRY ))
	do
		$RSH_USED_SHORT ${TEST_NODENAME_LIST[1]} "$REPLCTL -n ${svc_name} ${num_secs}"
		case $? in
        		0)      # changing desired number of secondaries succeeded
				rslt=0
				break
                		;;
        		99)      # RSH timeout check to see if command succeed or not,
				 # if not, retry replctl cmd
				numSec=
				eval numSec=$($RSH_USED_SHORT ${TEST_NODENAME_LIST[1]} "$REPLCTL -d ${svc_name}" 2>&1 \
					 | grep $svc_name | awk '{print $4}')
				if [ $numSec -ne $num_secs ]
				then
					i=`expr $i + 1`
					sleep 5
					continue
				else
					rslt=0
					break
				fi
                		;;
			*)	# changing desired number of secondaries failed
				print " ** Changing ${svc_name}'s desired number of " \
					"secondaries failed"
				rslt=1
				break
				;;
		esac
	done

	return $rslt
}


###############################################################################
# set_prov_pri
#
#
function set_prov_pri   # [service_name] [num_prov] [priority [priority...]]
{
	typeset svc_name
	typeset num_prov
	typeset rslt=0
	typeset k
	typeset numProv

        str=$1
        num_args=`echo $str | nawk 'BEGIN{FS=":"}{print NF}'`

        # Get the number of providers from the pass-in string.
        num_prov=$num_args-2

        # Get the service name from the pass-in string.
        str=$(echo $str | sed 's/:/#/1')
        svc_name=`echo $str | nawk 'BEGIN{FS="#"}{print $1}'`
        str=$(echo $str | sed 's/^.*#//1')

        # Get the number of providers from the pass-in string.
        str=$(echo $str | sed 's/:/#/1')
        num_prov=`echo $str | nawk 'BEGIN{FS="#"}{print $1}'`
        str=$(echo $str | sed 's/^.*#//1')

        # Assign the pass-in priority to the appropriate provider
        # ONLY for provider 1 to provider (num_prov-1).
        #
        # String format: e.g. 1:2:3:4:4:4:4:4
        #
        typeset i=1
        while [ $i -le $num_prov-1 ]
        do
                str=$(echo $str | sed 's/:/#/1')
                priority[${i}]=`echo $str | nawk 'BEGIN{FS="#"}{print $1}'`
                str=$(echo $str | sed 's/^.*#//1')
                ((i += 1))
        done

        # Assign the priority to the last provider on the list (num_prov)
        priority[${i}]=`echo $str`

	i=1
        while [ $i -le $num_prov ]
        do
		print "    Set the priority of provider ${TEST_NODEID_LIST[$i]} in $svc_name to " \
			"${priority[${i}]}"
		k=0
		while (( k < $HATIMERUN_RETRY ))
		do
			$RSH_USED_SHORT ${TEST_NODENAME_LIST[1]} "$REPLCTL -c $svc_name ${TEST_NODEID_LIST[$i]} ${priority[${i}]}" > /dev/null 2>&1
        		case $? in
			0)	# changing priority of provider succeeded
				rslt=0
				break
				;;
			99)	# there was at least one FAIL/ERROR message
				# these messages would have been printed out already
				numProv=
				numProv=$($RSH_USED_SHORT ${TEST_NODENAME_LIST[1]} "$REPLCTL -c $svc_name" 2>&1 | grep ${TEST_NODEID_LIST[$i]} | awk '{print $3}')
				if [$numProv -ne ${priority[$i]} ]
				then
					k=`expr $k + 1`
					sleep 5
					continue
				else
					rslt=0
					break
				fi
				;;
			*)	# changing priority of provider failed
				print "Changing the priority of provider ${TEST_NODEID_LIST[$i]} in " \
					"$svc_name failed"
				rslt=1
				break
				;;
			esac
			((k += 1))
		done
		((i += 1))
        done

	return $rslt
}


###############################################################################
# set_test_sequence
#
# Processes fault data file.
# Returns: 0 if successful, non-zero otherwise.
#
function set_test_sequence   #
{
	typeset parsing_context=GLOBAL
	typeset keyword value
	typeset line_count=0
	
	while read keyword value; do

		(( line_count += 1 ))

		# Ignore blank and comment lines
		[[ -z "$keyword" || "$keyword" = \#* ]] && continue

		# Delete all newlines (for lines that end with '\')
		value=$(echo "$value" | tr -d "\n")

		case $keyword in
		#
		# Global Variable
		#
		DEF_UPATH)
			DEF_UPATH=$value
			;;
		DEF_KPATH)
			DEF_KPATH=$value
			;;
		DEF_UNPATH)
			DEF_UNPATH=$value
			;;
		DEF_USER)
			DEF_USER=$value
			;;
		DEF_UFILE)
			DEF_UFILE=$value
			;;
		DEF_KFILE)
			DEF_KFILE=$value
			;;
		DEF_UNFILE)
			DEF_UNFILE=$value
			;;

		#
		# Cluster SETUP
		#
		SETUP)
			parsing_context=SETUP
			;;
		COMMAND)
			;;

		#
		# Test case def
		#
		TEST)
			(( TEST_INDEX += 1 ))
			TEST[TEST_INDEX]=$value
			parsing_context=TEST
			;;
		CLIENT)
			case $parsing_context in
			GLOBAL|SETUP)
				print -u2 "ERROR: line $line_count:" \
					"\"$keyword\" must be used in TEST" \
					"context ($parsing_context)"
				return 1
				;;
			*)
				(( CLIENT_INDEX += 1 ))
				CLIENT_LIST[CLIENT_INDEX]=$value
				;;
			esac
			;;
		SERVER)
			case $parsing_context in
			GLOBAL)
				print -u2 "ERROR: line $line_count:" \
					"\"$keyword\" must be used in SETUP" \
					"or TEST context ($parsing_context)"
				return 1
				;;
			SETUP)
				(( SETUP_INDEX += 1 ))
				SETUP_LIST[SETUP_INDEX]=$value
				;;
			*)
				(( SERVER_INDEX += 1 ))
				SERVER_LIST[SERVER_INDEX]=$value
				;;
			esac
			;;
		SKIP)
			SKIP=$value
			;;
		GROUP)
			GROUP=$value
			;;
		QUORUM_DEVICE)
			(( QUORUM_INDEX += 1 ))
			QUORUM_LIST[QUORUM_INDEX]=$value
			;;
		SVC_SHUTDOWN)
			(( SVC_SHUTDOWN_INDEX += 1 ))
			SVC_SHUTDOWN_LIST[SVC_SHUTDOWN_INDEX]=$value
			;;
		REQ_NODES)
			REQ_NODES=$value
			;;
		IGNORE_REBOOTS)
			IGNORE_REBOOTS=$value
			;;
		REBOOT_AFTER)
			REBOOT_AFTER=1
			;;
		REBOOT_BEFORE)
			REBOOT_PARAM=$value
			REBOOT_BEFORE=1
			;;
		NUM_SECS)
			(( NUM_SECS_INDEX += 1))
			NUM_SECS_LIST[NUM_SECS_INDEX]=$value
			;;
		PROV_PRI)
			(( PROV_PRI_INDEX += 1))
			PROV_PRI_LIST[PROV_PRI_INDEX]=$value
			;;
		#
		# Marker for END of test case definition
		END)
			# File to log test output
			TEST_LOG=$OUTPUTDIR/test-${TEST_INDEX}.log

			#
			# Run the test
			#
			execute_test

			if [ $ABORT_TEST -eq 1 ]; then
				print " ** ABORTING TESTS...."
				return 1
			fi

			#
			# Remove test log if the test passed and
			# user doesn't want to keep it.
			#
			if [[ "${TEST_RESULTS[TEST_INDEX]}" = PASS &&
			   $KEEP_RESULTS -eq 0 ]]; then
				print " ** Removing test log $TEST_LOG"
				rm -f $TEST_LOG 2>/dev/null
			fi

			#
			# Reset values
			#
			parsing_context=GLOBAL
			SKIP=
			GROUP=
			REQ_NODES=
			IGNORE_REBOOTS=0
			REBOOT_AFTER=0
			REBOOT_BEFORE=0

			unset CLIENT_LIST
			CLIENT_INDEX=0

			unset SERVER_LIST
			SERVER_INDEX=0

			unset SVC_SHUTDOWN_LIST
			SVC_SHUTDOWN_INDEX=0

			unset QUORUM_LIST
			QUORUM_INDEX=0

			unset NUM_SECS_LIST
			NUM_SECS_INDEX=0

			unset PROV_PRI_LIST
			PROV_PRI_INDEX=0

			NUM_DOWN_NODES=0

			#
			# need to unshuffle_nodes in case
			# next testcase can't do shuffle_nodes
			#
			if [ $WANT_SHUFFLE -eq 1 ]; then
				if [ $IS_SHUFFLE_OK -eq 1 ]; then
					unshuffle_nodes
				fi
			fi
			;;
		esac
	done < $FAULT_DATAFILE

	return 0
}
###############################################################################
# This function will get num of down nodes for each client entry.
# It will be called by get_num_down_nodes to get maxinum of down nodes in the tests.
#
function calculate_num_down_nodes # [client_entry ]
{
	typeset down_nodes
	typeset opt

	while getopts ':n:m:f:u:xd:r' opt; do
		case $opt in
		d)
			# Collect all -d option arguments, each of which
			# could be a comma-separated list of nodes expected
			# to go down
			down_nodes="$down_nodes $OPTARG"
			;;
		esac
	done
	shift 'OPTIND - 1'
	down_nodes=$(parse_node_specs $down_nodes)
	if [ $? -ne 0 ]; then
		return 1
	fi
	num=`echo $down_nodes | awk '{print NF}'`
	return 0
}

###############################################################################
# This function will get num of down nodes in one testcase.
# If current testcase has more than one clients, it will get
# max num of down nodes in all_clients. 
# It will be called when user want to shuffle nodes during the tests.
# Result: save the max num od down nodes in current testcase
# to global variable $NUM_DOWN_NODES
#
function get_num_down_nodes
{
	typeset index=1

	NUM_DOWN_NODES=0
	while (( index <= CLIENT_INDEX )); do
		eval calculate_num_down_nodes "${CLIENT_LIST[index]}"
		if [ $? -ne 0 ]; then
			break
		else
			if [ $NUM_DOWN_NODES -lt $num ]; then
				NUM_DOWN_NODES=$num
			fi
		fi
		(( index += 1 ))
	done
}
###############################################################################
# This disables the SCSI disk failfast mechanism.  Its a hack to prevent a node
# from panicing with a "Reservation Conflict".  Instead the node will lose
# quorum.  BugID: 4329810
#
function disable_sd_failfast
{
	typeset index
	typeset node
	index=1

	while (( index <= NUM_NODES ))
	do
		node=${NODENAME_LIST[index]}
		print "  * Disabling sd failfast on node: $node"

		# Twiddles the bits using adb so a reboot is not required
		$RSH_USED $node "echo 'sd_failfast_enable/W0; ssd_failfast_enable/W)'			| /usr/bin/adb -kw" >/dev/null 2>&1 

		# This makes it a bit more permanent, in case of reboots
		$RSH_USED $node "echo 'set sd:sd_failfast_enable=0' >> /etc/system"
		$RSH_USED $node "echo 'set ssd:ssd_failfast_enable=0' >> /etc/system"

                # Sync the change to each node
                $RSH_USED $node "sync;sync;sync"

	        ret=`$RSH_USED $node "$BOOTADM list-archive >/dev/null 2>&1;echo \\$?"`
		if [ $ret -eq 0 ]; then
			print "  * Run bootadm update after disabling sd failfast on node: $node"
	               	$RSH_USED $node "$BOOTADM update-archive"
			if [ $? -ne 0 ]; then
				print -u2 "WARNING: Can't run bootadm after set boot-file" \
					"parameter on node $node"
				exit 1
			fi
		else
			print " * bootadm update-archive not supported " \
				" on node $node : disabling sd failfast"
		fi
		(( index += 1 ))
	done
}

###############################################################################
# This enables the SCSI disk failfast mechanism.  This undoes the hack above.
#
function enable_sd_failfast
{
	typeset index
	typeset node
	index=1

	while (( index <= NUM_NODES ))
	do
		node=${NODENAME_LIST[index]}
		print "  * Enabling sd failfast on node: $node"

		# Untwiddle the bits so its back to normal
		$RSH_USED $node "echo 'sd_failfast_enable/W1; ssd_failfast_enable/W1'			| /usr/bin/adb -kw"\
		>/dev/null 2>&1 

		# Remove the /etc/system modification
		$RSH_USED $node "cp -f /etc/system /tmp; sed /sd_failfast_enable/d /tmp/system > /etc/system; rm -f /tmp/system"

	        ret=`$RSH_USED $node "$BOOTADM list-archive >/dev/null 2>&1;echo \\$?"`
		if [ $ret -eq 0 ]; then
			print "  * Run bootadm update after enabling sd failfast on node: $node"
	               	$RSH_USED $node "$BOOTADM update-archive"
			if [ $? -ne 0 ]; then
				print -u2 "ERROR: Can't run bootadm after set boot-file" \
					"parameter on node $node"
				exit 1
			fi
		else
			print  " * bootadm update-archive not supported " \
				" on node $node: enabling sd failfast"
		fi
		(( index += 1 ))
	done
}

###############################################################################
# Checks that the group for this test matches the groups specified
# on the command line (i.e. execute this test).
# Returns: 0 if the group matches (i.e. execute the test), non-zero otherwise.
#
function check_group   # [group...]
{
	typeset group_list=$@
	typeset a_group b_group

	# It's considered a match if user didn't specify any group
	[ -z "$SELECTED_GROUPS" ] && return 0

	for a_group in $SELECTED_GROUPS; do
		# Recurse through the groups specified by the user
		for b_group in $group_list; do
			# Recurse through the groups in the TEST def
			if [ "$a_group" = "$b_group" ]; then
				return 0	# found a match (run test)
			fi
		done
	done

	return 1				# no match (don't run test)
}

###############################################################################
#
# See if the REBOOT_BEFORE flag was specified
# Input: HARM: If the Replica Manager is on Node1, then reboot all nodes
#		Otherwise keep going
#	HARM_HIGHEST_NODEID: If the Replica Manager is not located on the
#		node that has highest nodeid, reboot the current RM node,
#		enforce the RM failover to the node that has highest nodeid
#	No Arg: Reboot all nodes
# Return: 0 - We always return 0 because can't fix anything if the node dies
#		unexpectedly

function check_reboot_before
{
	# Is the first arg HARM
	if [[ $1 == HARM ]]
	then
		# Find out where is RM
		nodeid=$($RSH_USED ${NODENAME_LIST[1]} "$RM_PRIMARY_NODE" 2>&1 | \
		awk '{print $2}')

		#
		# If we need to reboot all nodes.
		#
		if [[ nodeid -eq $NODE_1_ID ]]
		then
			reboot_all_nodes
			if [ $? -ne 0 ]; then
				ABORT_TEST=1
				return 1
			fi
		fi
	# Check if HARM has the highest nodeid. If not, make it happen
	elif [[ $1 == HARM_HIGHEST_NODEID ]]
	then
		# Find out where is RM
		nodeid=$($RSH_USED ${NODENAME_LIST[1]} "$RM_PRIMARY_NODE" 2>&1 | \
		awk '{print $2}')

		highest_nodeid=$($RSH_USED ${NODENAME_LIST[1]} "$GET_CLUSTER_STATE" \		2>&1 | grep "Highest Node Id" | awk '{print $5}')


		if [[ nodeid -ne highest_nodeid ]]
		then
			# reboot the current RM if not equal
			$RSH_USED ${NODENAME_LIST[$nodeid]} \
			"echo '/usr/cluster/orbtest/fi_support/daemonize \
			/sbin/uadmin 1 1' | at now" >/dev/null 2>&1

			# Sleep 15 seconds to let the node down
	                sleep 15

			wait_for_node ${NODENAME_LIST[$nodeid]}
		fi
		# else, nothing need to be done.

	# Otherwise blindly reboot (this only works if quorum is set to 1 0 0 0)
	else
		reboot_all_nodes
		if [ $? -ne 0 ]; then
			ABORT_TEST=1
			return 1
		fi
	fi

	have_done_initial_reboot=1
	return 0
}

###############################################################################
# Check quorum config
# Returns: 0 - test is to continue.
#          1 - test is skipped.
#          2 - an error occured.
#
function check_quorum_old
{
	typeset current_quorum
	typeset find_quorum_device
	typeset quorum_disks
	typeset counter
	typeset index_for_nodesconnected
	typeset real_nodeconnected
	typeset tempvar

	# Defaults
	current_quorum=0
	counter=0
	index_for_nodesconnected=0
	real_nodeconnected=0

	# See how many quorum disks the cluster has
	quorumindex=$QUORUM_INDEX
	current_quorum=`$RSH_USED ${NODENAME_LIST[1]} $SCSTAT -q | grep -c "Device votes"`

	# If the cluster has enough quorum disks then run the test
	if (( $current_quorum < $QUORUM_INDEX ));
	then
		remark="Need $QUORUM_INDEX quorum devices, have $current_quorum"
		SKIP_REMARK[TEST_INDEX]=$remark
		print " ** Skipping test due to: $remark"
		return 1
	fi	

	# Get a list of the quorum disks
	set -A quorum_disks `$RSH_USED ${NODENAME_LIST[1]} $SCCONF -p \
	| grep 'Quorum devices:' | nawk -F':' '{print $2}'`

	# As long as the test requires quorum disks keep checking for real
	# quorum disks on the cluster
	while [[ $quorumindex -gt 0 ]]
	do
		tempvar=0

		# Get a list of nodes connected to the quorum disk
#		set -A nodes_connected `$RSH_USED ${NODENAME_LIST[1]} \
#		$SCDIDADM -L ${quorum_disks[$quorumindex - 1]} \
#		| nawk '{sub(/^.*-/, "") sub(/:.*$/, "");print}'`

		set -A nodes_connected `$RSH_USED ${NODENAME_LIST[1]} \
		$SCDIDADM -L ${quorum_disks[$quorumindex - 1]} \
		| nawk '{print $2}' \
		| nawk '{sub(/^.*-/, "");sub(/^[A-Za-z]+/, "");sub(/:.*$/, "");print}'`
	
		# Get a list of nodes that the test needs connected
		set -A qd `echo ${QUORUM_LIST[$quorumindex]} \
		| nawk '{sub("^.*" $2 "[ \t]+", "" ); print}'`

		# If something should not be connected, unset it
		for j in ${qd[*]}
		do
			if [[ ${qd[$tempvar]} -eq 0 ]]
			then
				unset qd[$tempvar]
			fi
			(( tempvar += 1 ))
		done

		# Checking the QD config
		# Cycle through what is needed and if the cluster has the
		# proper connection unset the element so there is nothing left
		for index_for_nodesconnected in ${nodes_connected[*]}
		do
			(( real_nodeconnected = index_for_nodesconnected - 1 ))
			
			# The quorum disk is connected in reality and is needed
			if [[ ${qd[$real_nodeconnected]} -eq 1 ]]
			then
				unset qd[$real_nodeconnected]

			# Its not connected, so skipping the test
			elif [[ ${qd[$real_nodeconnected]} -eq 0 ]]
			then
				remark="Quorum device is connected to ${nodes_connected[$counter]} but device should not be"
				SKIP_REMARK[TEST_INDEX]=$remark
				print " ** Skipping test due to: $remark"
				return 1
			fi
			(( counter += 1 ))
		done

		# If there is something left, then we didn't unset everything
		# Something must be wrong
		if [[ -n ${qd[*]} ]]
		then
			# ERROR, Need better output
			remark="Nodes not properly connected"
			SKIP_REMARK[TEST_INDEX]=$remark
			print " ** Skipping test due to: $remark"
			return 1
		fi

		(( quorumindex -= 1 ))
	done

	return 0
}

###############################################################################
# Check quorum config
# This function does same thing with check_quorum() and
# fix bugs in check_quorum.
# Returns: 0 - test is to continue.
#          1 - test is skipped.
#          2 - an error occured.
#
function check_quorum
{
	typeset actual_quorum_num
	typeset quorum_checked
	typeset actual_quorum_disks
	typeset quorum_not_ok
	typeset num_ok_quorums
	typeset num_required_nodes_connected
	typeset required_nodes_connected 
	typeset real_nodes_connected 
	typeset num_real_nodes_connected
	typeset	nodeid 
	typeset connected_nodes
	typeset node
	typeset i
	typeset j
	typeset k

	if [[ $QUORUM_INDEX -eq 0 ]]
	then
		return 0
	fi
	# Defaults
	actual_quorum_num=0
	nodeid=0
	k=0
	num_ok_quorums=0

	# See how many quorum disks the cluster has
	quorumindex=$QUORUM_INDEX
	actual_quorum_num=`$RSH_USED ${TEST_NODENAME_LIST[1]} $SCSTAT -q 2>/dev/null | grep -c "Device votes"`

	# If the cluster has enough quorum disks then run the test
	if (( $actual_quorum_num < $QUORUM_INDEX ));
	then
		remark="Need $QUORUM_INDEX quorum devices, have $actual_quorum_num"
		SKIP_REMARK[TEST_INDEX]=$remark
		print " ** Skipping test due to: $remark"
		return 1
	fi	

	# Get a list of the quorum disks
	set -A actual_quorum_disks `$RSH_USED ${TEST_NODENAME_LIST[1]} $SCCONF -p 2>/dev/null \
	| grep 'Quorum devices:' | nawk -F':' '{print $2}'`
#	echo "DEBUG: check_quorum: actual_quorum_disks is ${actual_quorum_disks[*]}"
	#
	# reset the quorum_checked array to 0 in all items.
	# It will save if the corresponding quorum device
	# is selected or not by the tests. 0 means current
	# qourum is not selected by tests yet, 1 means this
	# quorum device already selected by the tests.
	# 
	i=0
	unset quorum_checked 
	while [[ $i -lt $actual_quorum_num ]]
	do
		quorum_checked[i]=0
		(( i += 1 ))
	done

	#
	# As long as the test requires quorum disks keep checking for real
	# quorum disks on the cluster
	while [[ $quorumindex -gt 0 ]]
	do
#		echo "DEBUG: check_quorum: check quorum on QUORUM_LIST ${QUORUM_LIST[$quorumindex]}"
		# Get a list of nodes that the test needs connected
		set -A required_nodes_connected `echo ${QUORUM_LIST[$quorumindex]} \
		| nawk '{sub("^.*" $2 "[ \t]+", "" ); print}'`


#		echo "DEBUG: required_nodes_connected is ${required_nodes_connected[*]}"
		# If something should not be connected, unset it
		i=0
		num_required_nodes_connected=0
		for j in ${required_nodes_connected[*]}
		do
			if [[ ${required_nodes_connected[$i]} -eq 0 ]]
			then
				unset required_nodes_connected[$i]
			else
				(( num_required_nodes_connected += 1 ))
			fi
			(( i += 1 ))
		done
#		echo "DEBUG: check_quorum: num_required_nodes_connected is $num_required_nodes_connected."

		#
		# Loop all quorum disks which current in cluster to
		# if any quorum disks is ok for testing
		#
		i=0
		while [[ $i -lt $actual_quorum_num ]]
		do
			#
			# First, check if the current quorum is selected
			# or not by previous checking.
			#
			if [[ ${quorum_checked[$i]} -eq 1 ]]
			then
#				echo "DEBUG: check_quorum: quorum ${actual_quorum_disks[$i]} is already be selected"
				(( i += 1 ))
				continue
			fi
#			echo "DEBUG: check_quorum: check quorum on cluster ${actual_quorum_disks[$i]}"
			#
			# Get a list of nodes connected to the quorum disk
			#
			set -A connected_nodes `$RSH_USED ${TEST_NODENAME_LIST[1]} \
			$SCDIDADM -L ${actual_quorum_disks[$i]} 2>/dev/null \
			| nawk '{print $2}' | nawk -F':' '{print $1}'`
#			echo "DEBUG: check_quorum: nodes is ${connected_nodes[*]}"
			#
			# Get a list of nodeidx connected to the quorum disk
			#
			num_real_nodes_connected=0
			unset real_nodes_connected
			for node in ${connected_nodes[*]}; do
				real_nodes_connected[num_real_nodes_connected]=$(nodename_to_nodeidx $node)
				(( num_real_nodes_connected += 1 ))
			done
#			echo "DEBUG: check_quorum: real_nodes_connected is ${real_nodes_connected[*]}"
#			echo "DEBUG: check_quorum: num_real_nodes_connected is $num_real_nodes_connected"
			if [[ $num_required_nodes_connected -ne $num_real_nodes_connected ]]
			then
#				echo "DEBUG: check_quorum: skip this quorum due to different nodes num connected"
				(( i += 1 ))
				continue
			fi

			# Checking the QD config
			# Cycle through what is needed and if the cluster has the
			# proper connection unset the element so there is nothing left
			quorum_not_ok=0
			for nodeid in ${real_nodes_connected[*]}
			do
				(( k = nodeid - 1 ))
			
				# The quorum disk is connected in reality and is needed
				if [[ ${required_nodes_connected[$k]} -eq 1 ]]
				then
					unset required_nodes_connected[$k]
				# Its not connected, so skipping the test
				elif [[ ${required_nodes_connected[$k]} -eq 0 ]]
				then
					quorum_not_ok=1
#					echo "DEBUG: check_quorum: Skip this quorum due to wrong connection."
					break
				fi
			done
			if [[ $quorum_not_ok -eq 1 ]]
			then
#				echo "DEBUG: check_quorum: skip this quorum due to wrong connection between nodes."
				(( i += 1 ))
				continue
			fi
			#
			# this quorum disk is ok for use in the test
			#
			quorum_checked[i]=1
			(( num_ok_quorums += 1 ))
			# If there is something left, then we didn't unset everything
			# Something must be wrong
			if [[ -n ${required_nodes_connected[*]} ]]
			then
				remark="Nodes not correctly connected"
				# ERROR, Need better output
				print " ** Skipping test due to: $remark"
				SKIP_REMARK[TEST_INDEX]=$remark
				return 1
			fi
	#		echo "DEBUG: check_quorum: this quorum is ok for use"
			break
		done

		if [[ $num_ok_quorums -eq $QUORUM_INDEX ]]
		then
	#		echo "DEBUG: check_quorum: find enough $num_ok_quorums quorums."
			break
		fi
		(( quorumindex -= 1 ))
	done
	if [[ $num_ok_quorums -lt $QUORUM_INDEX ]]
	then
#		echo "DEBUG: check_quorum: Can only find $num_ok_quorums quorum while test need $QUORUM_INDEX quorums."
		# ERROR, Can't find enough quorum devices
		remark="Need $QUORUM_INDEX quorums, Can only find $num_ok_quorums ok quorum devices."
		print " ** Skipping test due to: $remark"
		SKIP_REMARK[TEST_INDEX]=$remark
		return 1
	fi
#	echo "DEBUG: check_quorum:quorum_check ok"
	return 0
}
	
###############################################################################
# Check if we need to skip this test
# Returns: 0 - test is to continue.
#          1 - test is skipped.
#          2 - an error occured.
#
function check_skip   # [-r <remark>] [mode...]
{
	typeset remark
	typeset skip_mode
	typeset a_mode
	typeset opt
	typeset current_quorum

	# Defaults
	current_quorum=0

	# Set default remark
	case "$RUN_MODE" in
	K32)
		remark="Not supported in 32-bit KERNEL mode"
		skip_mode=K32
		;;
	K64)
		remark="Not supported in 64-bit KERNEL mode"
		skip_mode=K64
		;;
	K)
		remark="Not supported in InTel KERNEL mode"
		skip_mode=K
		;;
	*)
		remark="Not supported in USER mode"
		skip_mode=U
		;;
	esac

	while getopts ':r:' opt; do
		case $opt in
		r)
			remark=$OPTARG
			;;
		\?)
			print -u2 "ERROR: SKIP: Option $OPTARG unknown"
			return 2
			;;
		:)
			print -u2 "ERROR: SKIP: Option $OPTARG needs argument"
			return 2
			;;
		*)
			print -u2 "ERROR: SKIP: Option $opt unrecognized"
			return 2
			;;
		esac
	done
	shift 'OPTIND - 1'
	
	# The rest of the arguments are skip modes.
	for a_mode in "$@"; do
		# Note, we allow user to specify SKIP with mode 'K' which
		# applies to both 32-bit and 64-bit kernel
		if [ "$a_mode" = "$skip_mode" ] ||
		   [[ ("$a_mode" = K) && ("$skip_mode" = K*) ]]; then
			SKIP_REMARK[TEST_INDEX]=$remark
			print " ** Skipping test due to: $remark"
			return 1			# skip test
		fi
	done

	return 0					# continue test
}

###############################################################################
# Check if we need exact test config
# Returns: 0 - test is to continue.
#          1 - test is skipped.
#          2 - an error occured.
#
function check_reqnodes   # [-x] Exact node config
{
	typeset nodes
	typeset exitvalue

	# Defaults, exitvalue is 2 so that if something goes wrong it will
	# be picked up
	nodes=
	exitvalue=2

	while getopts ':x:' opt
	do
		case $opt in
		x) 
			#
			# Need an exact match for the number of nodes
			#
			nodes=$OPTARG

			if (( NUM_NODES == nodes ))
			then
				exitvalue=0
			else
				exitvalue=1
			fi
			;;
		esac
	done
	shift 'OPTIND - 1'

	if [[ -n $nodes ]]
	then
		REQ_NODES=$nodes
	fi

	for nodesreq_not_match in "$@"
	do
		if (( NUM_NODES < REQ_NODES ))
		then
			exitvalue=1
		else
			exitvalue=0
		fi
	done

	# If exitvalue is 1, then we skip the test
	if [[ $exitvalue -eq 1 ]]
	then
		print " ** Test requires $REQ_NODES nodes, skipping."
		SKIP_REMARK[TEST_INDEX]="Test requires $REQ_NODES nodes"
		TEST_RESULTS[TEST_INDEX]=SKIPPED
	fi

	return  $exitvalue
}

###############################################################################
# initial_reboot
# If user wants an initial cluster reboot and we haven't done so, reboot
# all nodes
# Returns: 0 if successful, non-zero otherwise.
#
integer have_done_initial_reboot=0
function initial_reboot   #
{
	if (( DO_INITIAL_REBOOT && !have_done_initial_reboot )); then
		print " ** Performing initial reboot on all nodes"
		reboot_all_nodes
		if [ $? -ne 0 ]; then
			return 1
		fi
		have_done_initial_reboot=1
	elif (( REBOOT_BEFORE ))
	# Check to see if cluster should be rebooted before test
	then
		print "REBOOT_BEFORE"
		#
		# Check if we should reboot the node to move the RM
		#
		eval check_reboot_before "$REBOOT_PARAM"
		if [ $? -ne 0 ]; then
			return 1
		fi
	fi
	return 0
}


###############################################################################
# setup_cluster
# Load all servers to run when cluster starts up
# Returns: 0 if successful, non-zero otherwise.
#
function setup_cluster   #
{
	typeset index=1

	while (( index <= SETUP_INDEX )); do
		eval load_server "${SETUP_LIST[index]}"
		if [ $? -ne 0 ]; then
			return 1
		fi
		(( index += 1 ))
	done
	return 0
}

###############################################################################
# load_all_servers
# Run all test servers
# Returns: 0 if successful, non-zero otherwise.
#
function load_all_servers   #
{
	typeset index=1

	while (( index <= SERVER_INDEX )); do
		eval load_server "${SERVER_LIST[index]}"
		if [ $? -ne 0 ]; then
			return 1
		fi
		(( index += 1 ))
	done
	return 0
}

###############################################################################
# unload_all_servers
# Unload all test servers
# Returns: 0 if successful, non-zero otherwise.
#
function unload_all_servers   #
{
	typeset index=$SERVER_INDEX		# unload in reverse order

	while (( index > 0 )); do
		eval unload_server "${SERVER_LIST[index]}"
		if [ $? -ne 0 ]; then
			return 1
		fi
		(( index -= 1 ))
	done
	return 0
}

###############################################################################
# load_all_clients
# Run test clients one by one.  They perform the actual testing.
# Returns: 0 if successful, non-zero otherwise.
#
function load_all_clients   #
{
	typeset rslt=0
	typeset index=1

	while (( index <= CLIENT_INDEX )); do
		eval load_client "${CLIENT_LIST[index]}"
		if [ $? -ne 0 ]; then
			# Note, even if the client returns a failure, we'll
			# continue with the next one, because other clients
			# might do other tests or some clean up procedures
			rslt=1
		fi
		#
		# reset all nodes vote to 1 if tests set down_nodes to 0 
		# in loading this client.
		#
		if [ $IS_SHUFFLE_OK -eq 1 ]; then
			if [ $num_down_nodes -gt 1 -a $num_down_nodes -lt $NUM_NODES ]; then
				#
				# For quorum tests, won't need to reset nodes' vote
				#
				if [[ $QUORUM_INDEX -eq 0 ]]
				then
					#
					# Reset all nodes' vote to 1 after tests on all tests
					# except quorum tests
					#
					echo "DEBUG:reset all node vote to 1"
					eval reset_node_vote ""
					if [ $? -ne 0 ]; then
						# Cleanup failed, bu we'll
						# continue with the next one
						rslt=1
					fi
				fi
			fi
		fi
		(( index += 1 ))
	done
	return $rslt
}

###############################################################################
# set_all_num_secs
# Set desired number of secondaries for each service one by one.
# Returns: 0 if successful, non-zero otherwise.
#
function set_all_num_secs   #
{
        typeset index=1

        while (( index <= NUM_SECS_INDEX )); do
                eval set_num_secs "${NUM_SECS_LIST[index]}"
                if [ $? -ne 0 ]; then
                        return 1
                fi
                (( index += 1 ))
        done
        return 0
}


###############################################################################
# set_all_prov_pri
# Set the priority of all the providers within each service one by one.
# Returns: 0 if successful, non-zero otherwise.
#
function set_all_prov_pri   #
{
        typeset index=1

        while (( index <= PROV_PRI_INDEX )); do
                # Concatenate service name, number of providers and the list of 
		# providers and their priorities into a string.
		# String format: e.g. svc_name:num_prov:1:2:3:4:4:4:4:4
		#
                str=$(echo ${PROV_PRI_LIST[index]} | sed 's/ /:/g')

                # Pass in the provider string to the function for
                # processing.
                eval set_prov_pri $str
                if [ $? -ne 0 ]; then
                        return 1
                fi
                (( index += 1 ))
        done
        return 0
}


###############################################################################
# execute_test
#
# Executes a test and sets TEST_RESULTS[TEST_INDEX].
# Returns: non-zero if a test failure/error occured.
#
function execute_test   #
{
	typeset rslt=0

	#
	# If this test is not in the $RUN_LIST, don't run it
	#
	if [ ${RUN_LIST[TEST_INDEX]} -eq 0 ]; then
		TEST_RESULTS[TEST_INDEX]=
		return 0
	fi

	#
	# Check the GROUP for this test, versus the GROUP selected
	# If there's no match, ignore this test
	#
	eval check_group "$GROUP"
	if [ $? -ne 0 ]; then
		TEST_RESULTS[TEST_INDEX]=
		return 0
	fi

	print

	# Format test description (fold long ones)
	typeset title="*** TEST [$TEST_INDEX] -"
	typeset -R${#title} indent=" "
	{
		echo "$title"
		echo "$indent" ${TEST[TEST_INDEX]}
	} | fmt -c -w 80

	print -n " ** TEST [$TEST_INDEX] - NODES=$REQ_NODES GROUP=$GROUP "

	if (( REBOOT_AFTER ))
	then
		print "REBOOT_AFTER"
	else
		print
	fi

	#
	# Check if we should skip this test
	#
	eval check_skip "$SKIP"
	case $? in
	0)
		# Test continues.
		;;
	1)
		# Test is skipped.
		TEST_RESULTS[TEST_INDEX]=SKIPPED
		return 0
		;;
	*)
		# An error occured.
		TEST_RESULTS[TEST_INDEX]=FAIL
		return 1
		;;
	esac

	eval check_reqnodes "$REQ_NODES"
	case $? in
	0)
		# Test continues.
		;;
	1)
		# Test is skipped.
		TEST_RESULTS[TEST_INDEX]=SKIPPED
		return 0
		;;
	*)
		# An error occured.
		TEST_RESULTS[TEST_INDEX]=FAIL
		return 1
		;;
	esac

	#
	# Check if suncluster bit is ok to reset the node votes
	#
	get_num_down_nodes
	if [ $NUM_DOWN_NODES -gt 1 -a $NUM_DOWN_NODES -lt $NUM_NODES ]; then

		check_scconf_on_cluster
	else
		IS_SHUFFLE_OK=1
	fi
#	#
#	# If user expected to shuffle nodes during tests,
#	# check how many nodes expected to go down during the test
#	# because if test will reboot more than one nodes,
#	# test should set the node vote of down nodes first, which
#	# which is only available since sc30u2
#	#
#	if [ $WANT_SHUFFLE -eq 1 ]; then
#		#
#		# User want to shuffle_nodes in the tests
#		# Need to do more check to see if cluster
#		# is ok to shuffle_nodes in this testcase.
#		#
#		get_num_down_nodes
#
#		if [ $NUM_DOWN_NODES -gt 1 -a $NUM_DOWN_NODES -lt $NUM_NODES ]; then
#			check_scconf_on_cluster
#		else
#			#
#			# current testcase will rebooting none,1
#			# or all_nodes which doesn't need to
#			# reset node vote, so it will be always
#			# ok to shuffle_nodes
#			#
#			IS_SHUFFLE_OK=1
#		fi
#	else
#		#
#		# User doesn't want to shuffle_nodes in the tests
#		#
#		IS_SHUFFLE_OK=0
##	fi
#	#
#	# Do shuffle nodes if required
#	#
#	if [ $IS_SHUFFLE_OK -eq 1 ]; then
#		if [ $QUORUM_INDEX -le 0 ]; then
#			shuffle_nodes
#		fi
#	fi
	#
	# Do shuffle nodes if required
	#
	if [ $WANT_SHUFFLE -eq 1 ]; then
		if [ $IS_SHUFFLE_OK -eq 1 ]; then
			if [ $QUORUM_INDEX -le 0 ]; then
				shuffle_nodes
			fi
		fi
	fi
	#
	# Check quorum config
	#
	eval check_quorum "$SKIP"
#	if [ $IS_SHUFFLE_OK -eq 1 ]; then
#		echo "DEBUG:execute_tests: before check quorum_shuffle"
#		eval check_quorum_shuffle "$SKIP"
#		echo "DEBUG:execute_tests: after check quorum_shuffle"
#	else
#		eval check_quorum "$SKIP"
#	fi
	case $? in
	0)
		# Test continues.
		;;
	1)
		# Test is skipped.
		TEST_RESULTS[TEST_INDEX]=SKIPPED
		return 0
		;;
	*)
		# An error occured.
		TEST_RESULTS[TEST_INDEX]=FAIL
		return 1
		;;
	esac

	# Do initial cluster reboot if user wants it
	#
	initial_reboot
	if [ $? -ne 0 ]; then
		ABORT_TEST=1
		TEST_RESULTS[TEST_INDEX]=FAIL
		return 1
	fi

	# Reset list of down nodes
	unset DOWN_LIST

	#
	# Do setup as defined in the fault data file
	#
	setup_cluster
	if [ $? -ne 0 ]; then
		ABORT_TEST=1
		TEST_RESULTS[TEST_INDEX]=FAIL
		return 1
	fi

	#
	# Load servers
	#
	load_all_servers
	if [ $? -ne 0 ]; then
		print " ** Will reboot all nodes due to error in loading" \
			"servers"

		# If requested, prompt and wait for user before continuing.
		error_wait

		reboot_all_nodes
		if [ $? -ne 0 ]; then
			ABORT_TEST=1
		fi
		TEST_RESULTS[TEST_INDEX]=FAIL
		return 1
	fi

        #
        # Set providers priority
        #
        set_all_prov_pri
        if [ $? -ne 0 ]; then
                ABORT_TEST=1
                TEST_RESULTS[TEST_INDEX]=FAIL
                return 1
        fi

	#
	# Set desired number of secondaries
	#
	set_all_num_secs
        if [ $? -ne 0 ]; then
                ABORT_TEST=1
                TEST_RESULTS[TEST_INDEX]=FAIL
                return 1
        fi

	#
	# Load clients (they perform the actual testing), one by one
	#
	load_all_clients
	if [ $? -ne 0 ]; then
		rslt=1
	fi

	#
	# Shutdown services
	#
	svc_shutdown
	if [ $? -ne 0 ]; then
		print " ** Will reboot all nodes due to error in shutdown"

		# If requested, prompt and wait for user before continuing.
		error_wait

		REBOOT_AFTER=1
		rslt=1
	fi

	#
	# Unload test servers (clients don't need to be unloaded)
	#
	unload_all_servers
	if [ $? -ne 0 ]; then
		print " ** Will reboot all nodes due to error in unloading" \
			"servers"

		# If requested, prompt and wait for user before continuing.
		error_wait

		REBOOT_AFTER=1
		rslt=1
	fi

	#
	# If we need to reboot all nodes.
	#
	if (( REBOOT_AFTER ))
	then
		reboot_all_nodes
		if [ $? -ne 0 ]; then
			ABORT_TEST=1
			rslt=1
		fi
	fi

	if (( rslt == 0 )); then
		TEST_RESULTS[TEST_INDEX]=PASS
	else
		TEST_RESULTS[TEST_INDEX]=FAIL
	fi
	return $rslt
}

###############################################################################
# print_info
# Prints info about what we're about to do
#
function print_info   #
{
	print
	print "Fault Injection Tests."
	print -n "======================================="
	print    "======================================="
	print "NOTE: Logs are at: $OUTPUTDIR"
}


###############################################################################
# print_results
# Prints info about what transpired
#
function print_results   #
{
	typeset -R4 tnum
	typeset -L10 tresult
	typeset tdesc
	typeset total_fail=0
	typeset total_pass=0
	typeset total_skipped=0
	typeset index

	print
	print -n "======================================="
	print    "======================================="
	print " ** Results for Fault Injection Tests"
	print "NOTE: In case of failure the test description will be printed"
	print "NOTE: Output has been saved at $OUTPUTDIR"
	print

	tnum=TEST
	tresult=RESULT
	tdesc=DESCRIPTION
	print "$tnum   $tresult   $tdesc"

	index=1
	while (( index <= TEST_INDEX )); do
		case ${TEST_RESULTS[index]} in
		PASS)
			tnum=$index
			tresult=PASS
			tdesc=
			(( total_pass += 1 ))
			;;
		FAIL)
			tnum=$index
			tresult=FAIL
			tdesc=${TEST[index]}
			(( total_fail += 1 ))
			;;
		SKIPPED)
			tnum=$index
			tresult=SKIPPED
			tdesc=${SKIP_REMARK[index]}
			(( total_skipped += 1 ))
			;;
		*)
			(( index += 1 ))
			continue
			;;
		esac

		print "$tnum   $tresult   $tdesc"
		(( index += 1 ))
	done

	print
	print -n "TOTALS:   FAIL $total_fail   PASS $total_pass   "
	print    "SKIPPED $total_skipped"

	print -n "======================================="
	print    "======================================="

	# If there was no failure, delete the output directory
	if (( total_fail == 0 )); then
		if [ $KEEP_RESULTS -eq 0 ]; then
			print "*** No test failures detected," \
				"removing $OUTPUTDIR/"
			rm -rf $OUTPUTDIR 2>/dev/null
		else
			print "*** Keeping results directory $OUTPUTDIR"
			print "    (Please remove it when you're done.)"
		fi
	else
		print "*** Test failures detected, examine $OUTPUTDIR/"
		print "    (Please remove it when you're done.)"
	fi
}


###############################################################################
# kill_jobs
# Terminate all background jobs
# 
function kill_jobs   #
{
	typeset pid

	for pid in $(jobs -p); do
		kill -KILL -$pid 2>/dev/null	# terminate entire process group
	done
	wait					# leave no zombies
}


###############################################################################
# handle_broken_pipe
# Magic trick to handle the case where this script is in a pipeline, e.g.
#
#	run_test_fi | tee mylogfile
#
# and the user hits ^C.  In this case, "tee" would be terminated before this
# script finishes its EXIT trap.  If any function called by the trap prints
# to stdout (or stderr if it's also redirected to the pipe), this script would
# be terminated with SIGPIPE since there is no longer a reader on the pipe.
#
# Note, we can't use a global PIPE trap since ksh resets all traps prior
# to executing a function.  We could use a PIPE trap for every function
# called in the EXIT trap, but that would be too much bookkeeping.  Using
# a single function like this is much easier.
# 
function handle_broken_pipe   #
{
	GOT_SIGPIPE=0
	trap 'GOT_SIGPIPE=1' PIPE

	# Test whether we really need to redirect stdout/stderr due to SIGPIPE
	print "\nCleaning up. Please wait..."
	sleep 1				# give time for SIGPIPE to be delivered

	if (( ! GOT_SIGPIPE )); then
		return			# no need to do redirections
	fi

	# Redirect stdout if it's a pipe (we must use /dev/fd/n since
	# 'test -p' takes a pathname)
	if [[ -p /dev/fd/1 ]]; then
		# Try print to /dev/tty.  If succeeds, redirect stdout to
		# /dev/tty, else to /dev/null (e.g. script's run under cron).
		# Note, below 2>/dev/null must be placed before >/dev/tty,
		# otherwise ksh might not filter out error message from print.
		print "\nCleaning up. Please wait..." 2>/dev/null >/dev/tty
		if [ $? -eq 0 ]; then
			exec >/dev/tty
		else
			exec >/dev/null
		fi
	fi

	# Redirect stderr to stdout if it's a pipe
	if [[ -p /dev/fd/2 ]]; then
		exec 2>&1
	fi
}



###############################################################################
# Main Execution
#
# Setup Routines
verify_utils
set_default_values
examine_command_line "$@"
get_node_names
set_arg_vars				# set argument variables
verify_communication			# verify we can talk to the cluster

ON_EXIT=handle_broken_pipe
trap 'eval "$ON_EXIT"' EXIT

# Set boot-file parameter on each node to the desired boot arguments
#ON_EXIT="$ON_EXIT; restore_eeprom_boot_file"
#set_eeprom_boot_file

# Set an rc file on each node to run mcboot, if requested
ON_EXIT="$ON_EXIT; remove_mcboot_rcfile"
set_mcboot_rcfile

# Restore the SCSI disk failfast
ON_EXIT="$ON_EXIT; enable_sd_failfast"
disable_sd_failfast

# Print info about the tests we're going to run
print_info

# Set up the test_sequence and run tests
ON_EXIT="$ON_EXIT; print_results; kill_jobs"
set_test_sequence

exit $?
