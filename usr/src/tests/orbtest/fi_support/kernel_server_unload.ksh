#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)kernel_server_unload.ksh	1.9	08/05/20 SMI"
#

readonly ENTRY=$1
readonly MOD_ARGS=$2

readonly MODUNLOAD=/usr/sbin/modunload
readonly MODINFO=/usr/sbin/modinfo

# Determine where the symlink is.
if [[ $(isainfo -kv) = 64* ]]
then
        # 64-bit: use kernel arch for pathname component to kernel modules.
	readonly KERNEL_ARCH="$(isainfo -k)/"
else
	# 32-bit: no kernel arch component.
	readonly KERNEL_ARCH=
fi

readonly SYMLINK_BASEDIR=/tmp
readonly SYMLINK_DIR=$SYMLINK_BASEDIR/$KERNEL_ARCH
readonly SYMLINK_NAME=${ENTRY}${MOD_ARGS}
readonly SYMLINK=$SYMLINK_DIR/$SYMLINK_NAME
readonly MODID_FILE=$SYMLINK_BASEDIR/${SYMLINK_NAME}.modid

trap "rm -f $SYMLINK $MODID_FILE" EXIT

echo "  * Unloading server module: ${SYMLINK##*/}"

# Find the module id from $MODID_FILE
if [ -f "$MODID_FILE" ]; then
	MODID=$(< $MODID_FILE)
fi

# Unload module
if [ -n "$MODID" ]; then
	RETSTR=$($MODUNLOAD -i $MODID 2>&1)
	if [ $? -ne 0 ]; then
		if [ -n "$RETSTR" ]; then
			echo "+++ FAIL: $RETSTR"
		fi
		echo "+++ FAIL: Could not unload server module ${SYMLINK##*/}"
		exit 1
	fi
fi

echo "--- PASS: Server module ${SYMLINK##*/} unloaded"
exit 0
