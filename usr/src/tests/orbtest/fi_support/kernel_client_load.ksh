#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)kernel_client_load.ksh	1.18	08/05/20 SMI"
#

readonly MODLOAD=/usr/sbin/modload
readonly MODUNLOAD=/usr/sbin/modunload
readonly MODINFO=/usr/sbin/modinfo

MOD_PATH=$1
MODNAME=$2
ENTRY=$3
MOD_ARGS=$4


#
# All we need to do is load the module specified in $1, and unload it
# If we loaded successfully we'll output PASS
#

# Determine which module to use: 32-bit or 64-bit
if [[ $(isainfo -kv) = 64* ]]
then
        # 64-bit: use kernel arch for pathname component to kernel modules.
	readonly KERNEL_ARCH="$(isainfo -k)/"
else
	# 32-bit: no kernel arch component.
	readonly KERNEL_ARCH=
fi
MODNAME="${KERNEL_ARCH}${MODNAME}"

FOUND=0

if [ -f "$MODNAME" ]; then
	FOUND=1
	PATH_ENTRY=$PWD
elif [ -n "$MOD_PATH" ]; then
	#
	# Find the module first in the given paths
	#
	save_ifs=$IFS
	IFS=:
	set $MOD_PATH
	IFS=$save_ifs

	for PATH_ENTRY
	do
		: ${PATH_ENTRY:=$PWD}
		if [ -f "$PATH_ENTRY/$MODNAME" ]; then
			echo "  * Found client module $MODNAME in $PATH_ENTRY"
			FOUND=1
			break
		fi
	done
fi

if [ $FOUND -eq 0 ]; then
	echo "+++ FAIL: could not find client module $MODNAME in PATH"
	exit 1
fi


#
# We found the path to this module
# Now create a link
#
readonly SOURCE=$PATH_ENTRY/$MODNAME

readonly SYMLINK_BASEDIR=/tmp
readonly SYMLINK_DIR=$SYMLINK_BASEDIR/$KERNEL_ARCH
readonly SYMLINK_NAME=${ENTRY}${MOD_ARGS}
readonly SYMLINK=$SYMLINK_DIR/$SYMLINK_NAME

mkdir -p $SYMLINK_DIR 2>/dev/null
rm -f $SYMLINK
ln -s $SOURCE $SYMLINK

trap "rm -f $SYMLINK" EXIT

#
# Note we assume client modules do their tests inside _init().
# Also, for 64-bit systems, modload always prepends file name component of the
# module with the kernel arch.  Therefore, below we can't modload $SYMLINK
# directly; instead we have to use $SYMLINK_BASEDIR/$SYMLINK_NAME, even if
# the symlink itself resides in $SYMLINK_DIR.
#
echo "  * Loading client module: ${SYMLINK##*/}"

#RETSTR=$($MODLOAD -e /bin/echo $SYMLINK_BASEDIR/$SYMLINK_NAME 2>&1)
$MODLOAD -e /bin/echo $SYMLINK_BASEDIR/$SYMLINK_NAME 2>&1
if [ $? -ne 0 ]; then
	if [ -n "$RETSTR" ]; then
		echo "+++ FAIL: $RETSTR"
	fi
	echo "+++ FAIL: Client module ${SYMLINK##*/} returned an error"
	exit 1
else
	RETSTR=$($MODINFO -c | grep ${ENTRY}${MOD_ARGS} | nawk '{print $1}')
	MODID=$(echo $RETSTR | nawk '{print $1}')
fi

# Unload module
if [ -n "$MODID" ]; then
	RETSTR=$($MODUNLOAD -i $MODID 2>&1)
	if [ $? -ne 0 ]; then
		if [ -n "$RETSTR" ]; then
			echo "+++ FAIL: $RETSTR"
		fi
		echo "+++ FAIL: Could not unload client module ${SYMLINK##*/}"
		exit 1
	fi
fi

echo "--- PASS: Client module ${SYMLINK##*/} returned 0"
exit 0
