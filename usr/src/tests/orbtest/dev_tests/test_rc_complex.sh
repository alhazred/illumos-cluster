#!/bin/sh

#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)test_rc_complex.sh	1.7	08/05/20 SMI"
#

# A more complex test for reference counting (comared to test_rc.sh).
# We have more a more complex protocol in which object references are
# being passed.  Each client program is multi threaded.

# The test waits for all clients to cleanly drop their reference. (so
# we don't need 
# it could be better, since the clients exit and cause references to go away
# anyway when the reconfig occurs.  It would be nice to add something which
# causes the client to wait until he knows that unreferenced has been
# called on the server.

# the client programs should be enhanced to make the number of
# iterations and hreads tunable from the command line.


# In both sets of tests, we use a small number of clients to aviod
# hitting the procs_to_node limit.

CLIENTS=4
./test_mgr_impl $CLIENTS &

while [ $CLIENTS != 0 ]
do
	./test_mgr_clnt &
	sleep 10
	CLIENTS=`expr $CLIENTS - 1`
done
wait



echo "XXXXXXXXXXXXXXXXXXXXXX First test completed XXXXXXXXXXXXXXXXXXXXX"

# This test uses timers to die, so that the clients die in the middle of
# performing work.

CLIENTS=4

./test_mgr_impl $CLIENTS &:

while [ $CLIENTS != 0 ]
do
	./test_mgr_clnt -t 25 &
	sleep 10
	CLIENTS=`expr $CLIENTS - 1`
done
wait

echo "XXXXXXXXXXXXXXXXXXXXXX Second test completed XXXXXXXXXXXXXXXXXXXXX"
