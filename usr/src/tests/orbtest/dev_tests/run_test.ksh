#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)run_test.ksh	1.13	08/05/20 SMI"
#

# Items with "@@" markers will be substituted by make.
#

readonly PROGNAME=$0

# Where test programs/scripts are.
readonly DIR="@@PROGS_DIR@@"

# Default parameter values.
integer -r DEFAULT_NUM_ITERS=5
integer -r DEFAULT_NUM_THREADS=30
integer -r DEFAULT_NUM_INVOS=1
integer -r DEFAULT_NUM_GETS=25
integer -r DEFAULT_NEW_OBJECTS=0	# false -- reuse objects
integer -r DEFAULT_VERBOSE=0		# false

# Parameters.
integer NUM_ITERS=$DEFAULT_NUM_ITERS
integer NUM_THREADS=$DEFAULT_NUM_THREADS
integer NUM_INVOS=$DEFAULT_NUM_INVOS
integer NUM_GETS=$DEFAULT_NUM_GETS
integer NEW_OBJECTS=$DEFAULT_NEW_OBJECTS
integer VERBOSE=$DEFAULT_VERBOSE
typeset SERVER_NODE			# name of node to run test server

readonly SLEEP_BEFORE_UNLOADS=3		# before unloading kernel client/server
readonly UNLOAD_RETRIES=20		# how many times to try modunloading
readonly SLEEP_BETWEEN_UNLOADS=2	# sleep between the modunload retries

# Client-server combination bits.
integer -r U2U=1
integer -r U2K=2
integer -r K2U=4
integer -r K2K=8

integer TEST=0

# Some utilities we need.
readonly MC_INFO=/usr/cluster/orbtest/fi_support/cluster_info
readonly MODLOAD='/usr/sbin/modload -p'
readonly MODUNLOAD=/usr/sbin/modunload
readonly MODINFO=/usr/sbin/modinfo

# Clients and servers.
readonly UCLIENT=$DIR/test_mgr_clnt	# test client program
readonly KCLIENT=misc/orbtest_client	# test client kernel module

readonly USERVER=$DIR/test_mgr_server	# test server program
readonly KSERVER=misc/orbtest_server	# test server kernel module

readonly PARAMS_SERVER=$DIR/test_params_server	# test parameters server

# Usage instruction.
readonly USAGE="\
Usage: $PROGNAME [options]

Options:
   -help|-?
	Prints this message.

   -u2u
	Runs USER clients and USER server.
   -u2k
	Runs USER clients and KERNEL server.
   -k2u
	Runs KERNEL clients and USER server.
   -k2k
	Runs KERNEL clients and KERNEL server.

   If none of the options above are specified, all combinations will be run.

   -server <node_id | host_name>
	The node (specify node id or host name) to run the test server.
	Default: random
   -iter <num_iters>
	Number of times each client test should run the test.
	Default: $DEFAULT_NUM_ITERS
   -thread <num_threads>
	Number of threads each client creates (one client per node).
	Default: $DEFAULT_NUM_THREADS
   -get <num_gets>
	Number of times each client thread should get test objects.
	Default: $DEFAULT_NUM_GETS
   -invo <num_invos>
	Number of put() and get() invocations each client thread makes
	on the test objects.
	Default: $DEFAULT_NUM_INVOS
   -new
	Tells test server to create a new test object implementation for
	each client request for a test object.  Otherwise, only one
	implementation will be created and the server will pass references
	to it for each each client request.
   -v
	Verbose output.
"


#
# Obtains a positive option argument and prints to stdout.
# Returns 0 if successful, 1 otherwise.
#
# $1 must contain the option string
#
function pos_optarg # <args>
{
	typeset opt=$1
	typeset optarg=$2

	if [[ -z "$optarg" ]]
	then
		print -u2 "ERROR: option $opt requires argument"
		return 1
	fi

	if ! ( integer numarg=$optarg ) || (( optarg <= 0 ))
	then
		print -u2 "ERROR: $opt option argument must be > 0"
		return 1
	fi

	print $optarg
	return 0
}


#
# Setup
#
function setup # <program_args>
{
	typeset server_node
	typeset host
	typeset mode_str verbose_str

	TEST=0

	# Parse options.
	while (( $# ))
	do
		case "$1" in
		-h*|-\?*)
			print -u2 "$USAGE"
			exit 0
			;;
		-u2u)
			(( TEST |= U2U ))
			;;
		-u2k)
			(( TEST |= U2K ))
			;;
		-k2u)
			(( TEST |= K2U ))
			;;
		-k2k)
			(( TEST |= K2K ))
			;;
		-ser*)
			if [[ -z "$2" ]]
			then
				print -u2 "ERROR: option $1 requires argument"
				exit 1
			fi
			server_node=$2
			shift
			;;
		-ite*)
			NUM_ITERS=$(pos_optarg "$@") || exit 1
			shift
			;;
		-thr*)
			NUM_THREADS=$(pos_optarg "$@") || exit 1
			shift
			;;
		-get*)
			NUM_GETS=$(pos_optarg "$@") || exit 1
			shift
			;;
		-inv*)
			NUM_INVOS=$(pos_optarg "$@") || exit 1
			shift
			;;
		-new*)
			NEW_OBJECTS=1
			;;
		-v*)
			VERBOSE=1
			;;
		*)
			print -u2 "ERROR: option $1 unknown"
			exit 1
			;;
		esac
		shift
	done

	# Get cluster information.
	# Note: the order of node names returned by $MC_INFO should
	# correspond to the order of node ID's.
	if ! set -A NODE_NAMES $($MC_INFO -n)
	then
		print -u2 "ERROR: $MC_INFO failed to get node names"
		exit 1
	fi
	if ! set -A NODE_IDS $($MC_INFO -e)
	then
		print -u2 "ERROR: $MC_INFO failed to get node IDs"
		exit 1
	fi
	if ! ROOT_NODE=$($MC_INFO -r)
	then
		print -u2 "ERROR: $MC_INFO failed to get name of root node"
		exit 1
	fi

	readonly ROOT_NODE
	readonly NUM_NODES=${#NODE_IDS[*]}

	# A little sanity check for what $MC_INFO returned.
	if (( ${#NODE_NAMES[*]} != ${#NODE_IDS[*]} ))
	then
		print -u2 "ERROR: number of node names returned by $MC_INFO"
		print -u2 "       is different from number of node ID's"
		exit 1
	fi
	if [[ -z "$ROOT_NODE" ]]
	then
		print -u2 "ERROR: $MC_INFO didn't find name of root node"
		exit 1
	fi

	# If no client-server combination options were specified, we'll test
	# all combinations.
	if (( TEST == 0 ))
	then
		(( TEST = -1 ))		# set all bits
	fi

	# If user specified a server node, verify it; otherwise pick one
	# at random.
	if [[ -n "$server_node" ]]
	then
		integer gotit=0

		if [[ $server_node = +([0-9]) ]]
		then
			# User specified a node id.
			integer index=0

			while (( index < NUM_NODES ))
			do
				if (( server_node == NODE_IDS[index] ))
				then
					SERVER_NODE=${NODE_NAMES[index]}
					gotit=1			# got it!
					break
				fi
				(( index += 1 ))
			done
		else
			# User specified a host name.
			for SERVER_NODE in ${NODE_NAMES[*]}
			do
				if [[ $server_node = $SERVER_NODE ]]
				then
					gotit=1			# got it!
					break
				fi
			done
		fi

		if (( ! gotit ))
		then
			print -u2 "ERROR: invalid server node ($server_node)"
			exit 1
		fi
	else
		# Pick a node at random.
		SERVER_NODE=${NODE_NAMES[RANDOM % NUM_NODES]}
	fi

	# Verify we can rsh as root to each node.
	for host in ${NODE_NAMES[*]}
	do
		# First make sure the node is alive, so we won't be stuck
		# waiting for rsh to complete.
		if ! ping $host 3 >/dev/null 2>&1
		then
			print -u2 "ERROR: node $host is apparently dead"
			exit 1
		fi

		# Now verify rsh.
		if ! rsh -n -l root $host /bin/true
		then
			print -u2 "ERROR: can't rsh to node $host as root"
			exit 1
		fi
	done

	# Print options.
	if (( NEW_OBJECTS ))
	then
		mode_str="new objects"
	else
		mode_str="reuse object"
	fi
	if (( VERBOSE ))
	then
		verbose_str="true"
	else
		verbose_str="false"
	fi

	echo "----------------------------------------------------------------"
	echo "Server node: $SERVER_NODE"
	echo "Test mode  : $mode_str"
	echo "Num clients: $NUM_NODES"
	echo "Num iters  : $NUM_ITERS"
	echo "Num threads: $NUM_THREADS"
	echo "Num gets   : $NUM_GETS"
	echo "Num invos  : $NUM_INVOS"
	echo "Verbose    : $verbose_str"
	echo "----------------------------------------------------------------"
}


#
# Run the parameters server (user-land server).
#
function run_params_server #
{
	# Always run it on the root node.
	print "Starting parameters server on node $ROOT_NODE (root node)"
	rsh -n -l root $ROOT_NODE sh -c "'" $PARAMS_SERVER \
			-iter $NUM_ITERS		\
			-thread $NUM_THREADS		\
			-get $NUM_GETS			\
			-invo $NUM_INVOS		\
			$(let NEW_OBJECTS && echo -new)	\
			$(let VERBOSE && echo -v)	\
			$NUM_NODES			\
			">/dev/console 2>&1'" &

	sleep 4					# give server time to start
}


#
# Run the USER test server.
#
function run_user_server #
{
	print "Starting USER server on node $SERVER_NODE"
	rsh -n -l root $SERVER_NODE sh -c "'$USERVER >/dev/console 2>&1'" &

	sleep 4					# give server time to start
}


#
# Run the KERNEL test server.
#
function run_kernel_server #
{
	print "Starting KERNEL server on node $SERVER_NODE"
	rsh -n -l root $SERVER_NODE $MODLOAD $KSERVER
	KSERVER_MODID=$(rsh -n -l root $SERVER_NODE $MODINFO \| \
		nawk -v modfile=${KSERVER##*/} "'\$6 == modfile {print \$1}'")
}


#
# Unload the KERNEL test server.
#
function unload_kernel_server #
{
	integer count=0
	typeset infoline

	while (( count < UNLOAD_RETRIES ))
	do
		# Unload the kernel test server.
		rsh -n -l root $SERVER_NODE $MODUNLOAD -i $KSERVER_MODID

		# Check to make sure it has really been unloaded.
		infoline=$(rsh -n -l root $SERVER_NODE $MODINFO \
						-i $KSERVER_MODID 2>/dev/null)

		# If the module has been unloaded, we're done.
		if [[ -z "$infoline" ]]
		then
			return 0
		fi

		# The module is still loaded, retry unloading.
		echo "Retrying modunload of kernel test server" \
			"on $SERVER_NODE ..."
		sleep $SLEEP_BETWEEN_UNLOADS
		(( count += 1 ))
	done

	# If we're here, that means server is still loaded after $count tries.
	echo "ERROR: Can't unload kernel test server on $SERVER_NODE" \
		"after $count tries"
	return 1
}


#
# Run the USER test clients (one per node).
#
function run_user_clients #
{
	typeset node

	print "Starting USER clients on all nodes"
	for node in ${NODE_NAMES[*]}
	do
		rsh -n -l root $node sh -c "'$UCLIENT >/dev/console 2>&1'" &
	done
}


#
# Run the KERNEL test clients (one per node).
#
function run_kernel_clients #
{
	typeset node

	print "Starting KERNEL clients on all nodes"
	for node in ${NODE_NAMES[*]}
	do
		# Note, test client modules run tests from _init().  So we must
		# run rsh in the background.
		rsh -n -l root $node $MODLOAD $KCLIENT &
	done
}


#
# Unload all KERNEL test clients (one per node).
#
function unload_kernel_clients #
{
	typeset node
	typeset failed=0

	for node in ${NODE_NAMES[*]}
	do
		integer count=0
		typeset infoline
		typeset modid

		# First get the test client's module id.
		modid=$(rsh -n -l root $node $MODINFO \| \
			nawk -v modfile=${KCLIENT##*/} \
				"'\$6 == modfile {print \$1}'")

		while (( count < UNLOAD_RETRIES ))
		do
			# Unload test client.
			rsh -n -l root $node $MODUNLOAD -i $modid

			# Check to make sure it has really been unloaded.
			infoline=$(rsh -n -l root $node $MODINFO \
							-i $modid 2>/dev/null)

			# If the module has been unloaded, we're done.
			if [[ -z "$infoline" ]]
			then
				continue 2		# do next node
			fi

			# The module is still loaded, retry unloading.
			echo "Retrying modunload of kernel test client" \
				"on $node ..."
			sleep $SLEEP_BETWEEN_UNLOADS
			(( count += 1 ))
		done

		# If we're here, it means client is still loaded after
		# $count tries.
		echo "ERROR: Can't unload kernel test client on $node" \
			"after $count tries"
		failed=1
	done

	return $failed
}


#
# Run U->U tests.
#
function test_u2u #
{
	if (( TEST & U2U ))
	then
		print "USER clients -> USER server:"

		run_params_server
		run_user_server
		run_user_clients

		wait				# wait until all are done
		print
	fi
}


#
# Run U->K tests.
#
function test_u2k #
{
	if (( TEST & U2K ))
	then
		print "USER clients -> KERNEL server:"

		run_params_server
		run_kernel_server
		run_user_clients

		wait				# wait until all are done
		unload_kernel_server || exit 1
		print
	fi
}


#
# Run K->U tests.
#
function test_k2u #
{
	if (( TEST & K2U ))
	then
		print "KERNEL clients -> USER server:"

		run_params_server
		run_user_server
		run_kernel_clients

		wait				# wait until all are done
		unload_kernel_clients || exit 1
		print
	fi
}


#
# Run K->K tests.
#
function test_k2k #
{
	if (( TEST & K2K ))
	then
		integer failed=0

		print "KERNEL clients -> KERNEL server:"

		run_params_server
		run_kernel_server
		run_kernel_clients

		wait				# wait until all are done
		unload_kernel_server || failed=1
		unload_kernel_clients || failed=1
		(( failed )) && exit 1
		print
	fi
}


#
# Do the tests.
#
setup "$@"

test_k2k
test_u2k
test_k2u
test_u2u

exit 0
