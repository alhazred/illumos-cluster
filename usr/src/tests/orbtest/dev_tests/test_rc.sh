#!/bin/sh -x
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)test_rc.sh	1.9	08/05/20 SMI"
#

# This script provides a simple test for the reference counting algorithm.
# If it hangs, then there is a problem.

set -e
./test_obj_impl &
impl_pid=$!
(
	
    for i in 1 2 3 4 5 6 7 8 #9 10 11 12
    do
	./test_obj_clnt &
    done
    wait
)

./test_obj_unbind

wait $impl_pid	# test_obj_impl will exit when it gets _unreferenced().

echo "Test passed"
