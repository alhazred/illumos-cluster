#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)wrapper.ksh	1.8	08/05/20 SMI"
#


PROGNAME=$0

##############################################################################
# Print usage of this script
#
function usage
{
	cat >&2 << EOF

Usage:
	$PROGNAME -?
	$PROGNAME [-p string] [-r path] command <arg1, arg2, ..., argn>

Options:
	-p	- prefix result from command with
	-r	- redirect output to specified path

EOF
}

##############################################################################
# Examine command line
#
function examine_command_line
{
	typeset opt
	typeset argc=$#
	typeset shift_len

	OUTPUT_PREFIX=""
	REDIRECT_PATH=""

	while getopts ':p:r:?' opt; do
		case $opt in
		p)
			OUTPUT_PREFIX=$OPTARG
			;;
		r)
			REDIRECT_PATH=$OPTARG
			;;
		\?)
			if [[ "$OPTARG" = '?' ]]; then
				usage
				exit 0
			else
				print "ERROR: Option $OPTARG unknown"
				exit 1
			fi
			;;
		:)
			print "ERROR: Option $OPTARG needs argument"
			exit 1
			;;
		*)
			print "ERROR: Option $opt unrecognized"
			exit 1
			;;
		esac
	done

	if [[ $argc -eq 0 ]]; then
		usage
		exit 0
	fi

	(( shift_len = OPTIND - 1 ))
	if [[ $shift_len -gt 0 ]]; then
		shift $shift_len
	fi

	COMMAND_TO_EXECUTE=$1
	shift 1
	ARGS_FOR_COMMAND=$@

	return 0
}


##############################################################################
# Main
#
function main
{
	typeset rslt

	# Examine command line
	eval "examine_command_line $@"
	if [[ $? -ne 0 ]]; then
		return 1
	fi

	# Execute Command
	if [[ -n "$REDIRECT_PATH" ]]; then
		eval "$COMMAND_TO_EXECUTE $ARGS_FOR_COMMAND" > $REDIRECT_PATH 2>&1
	else
		eval "$COMMAND_TO_EXECUTE $ARGS_FOR_COMMAND"
	fi

	rslt=$?

	if [[ -n "$OUTPUT_PREFIX" ]]; then
		print
		print "$OUTPUT_PREFIX $rslt"
		print
	else
		print $rslt
	fi

	return 0
}


##############################################################################
eval "main $@"
exit $?
