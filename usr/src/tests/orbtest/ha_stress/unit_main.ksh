#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)unit_main.ksh	1.9	08/05/20 SMI"
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)unit_main.ksh	1.9	08/05/20 SMI"
#
#
# Run test
#
# Will run the HA Stress test suite on the specified cluster
#

readonly PROGNAME=$0		# Program name
PROGDIR=$(dirname $PROGNAME)	# Directory of program
if [[ "$PROGDIR" = "." ]]; then
	PROGDIR=$(pwd)
fi
readonly PROGDIR

##############################################################################
# Files that need to be sourced
#
# Source the aux file
. $PROGDIR/ha_stress_aux


##############################################################################
# Global Settings

set -o monitor			# so bg jobs can run with their own PGID and
				# we can trap SIGCHLD
set +o nounset			# allow use of unset variables
set +o notify			# dont notify of bg job completion

ON_EXIT=""			# String of functions to run ON_EXIT
REMOVE_LOGS="NO"		# Should we remove logs at end of run
readonly LOGINNAME=$(logname)	# User's name

#
# Default values
#
readonly DEFAULT_OUTPUT_BASEDIR=/tmp

#
# Tool location
#
readonly RSH='rsh -n -l root'
readonly KILL=kill
readonly GETOPTS=getopts
readonly TAR=/bin/tar
readonly PING=/usr/sbin/ping
readonly REBOOT=/usr/sbin/reboot
readonly SORT=/bin/sort
readonly REPLCTL=/usr/cluster/bin/replctl

#
# UNIT Locations
#
UNIT_SWITCHOVER=$PROGDIR/unit_switchover
UNIT_START_END_SVC=$PROGDIR/unit_start_end_svc
UNIT_CMM_RECONF=$PROGDIR/unit_cmm_reconf

#
# Time constants
#
readonly START_HOURS=$($DATE +%H)
readonly START_MINUTES=$($DATE +%M)
readonly START_SECONDS=$($DATE +%S)

#
# Tool location
#
readonly WRAPPER=/usr/cluster/orbtest/ha_stress/wrapper
readonly LIST_CLIENT=/usr/cluster/orbtest/ha_stress/list_client

#
# Time checks
#
TIME_TO_RUN=0:5:0
TIME_RAN=0:0:0

##############################################################################
# Verify tools
#
function verify_tools
{
	typeset prog

	for prog in $TAR $PING $SORT; do
		if [[ ! -x $prog ]]; then
			print -u1 "ERROR: Can't find $prog"
			return 1
		fi
	done

	return 0
}


##############################################################################
# get_elapsed_time
#
# Will print the time that has elapsed since we started running
# in format (HH:MM:SS)
#
function get_elapsed_time
{
	typeset hours
	typeset minutes
	typeset seconds

	typeset diff_hours
	typeset diff_minutes
	typeset diff_seconds

	hours=$($DATE +%H)
	minutes=$($DATE +%M)
	seconds=$($DATE +%S)

	if [[ $seconds -lt $START_SECONDS ]]; then
		(( seconds += 60 ))
		(( minutes -= 1 ))
	fi

	if [[ $minutes -lt $START_MINUTES ]]; then
		(( minutes += 60 ))
		(( hours -= 1 ))
	fi

	if [[ $hours -lt $START_HOURS ]]; then
		(( hours += 24 ))
	fi

	(( diff_hours = hours - START_HOURS ))
	(( diff_minutes = minutes - START_MINUTES ))
	(( diff_seconds = seconds - START_SECONDS ))

	print "$diff_hours:$diff_minutes:$diff_seconds"
}
##############################################################################
# spent_time
#
# Will return time 
function spent_time
{
	echo $TIME_TO_RUN | sed 's/:/ /g' | read HOURRUN MINUTERUN SECONDRUN 
	echo $TIME_RAN | sed 's/:/ /g' | read HOURRAN MINUTERAN SECONDRAN 
	
	if ((HOURRUN - HOURRAN <= 0)); then
		if ((MINUTERUN - MINUTERAN <= 0)); then
			if ((SECONDRUN - SECONDRAN <= 0)); then
				return 1
			fi
		fi
	fi

	return 0

}
##############################################################################
# cleanup when exiting
#
function cleanup
{
	if [[ -n "$OUTPUTDIR" ]]; then
		print ""
		print "======================================================="
		if [[ "$REMOVE_LOGS" = "YES" ]]; then
			print "*** Removing saved logs: $OUTPUTDIR"
			rm -rf $OUTPUTDIR
		else
			print "*** Logs were not removed: $OUTPUTDIR"
		fi

		print ""
	fi

	return 0
}


##############################################################################
# Print usage of this script
#
function usage
{
	cat >&2 << EOF

Usage:
	$PROGNAME -?
	$PROGNAME -c <cluster> -f <file> -t <hours:minutes:seconds> [-o dir]

Options:
	-c	- Name of cluster
	-f	- Parameter file [stress.data]
	-t	- Time limit for tests [0:5:0]

Testing Options:


Misc. Options:
	-o	- Output directory

EOF
}


##############################################################################
# Examine command line
#
function examine_command_line
{
	typeset opt

	TMP_DIR=""
	CLUSTER_NAME=""
	OUTPUT_BASEDIR=$DEFAULT_OUTPUT_BASEDIR
	INPUT_FILE="stress.data"

	while getopts ':f:c:o:t:h?' opt; do
		case $opt in
		o)
			if [[ "$OPTARG" = /* ]]; then
				OUTPUT_BASEDIR=$OPTARG
			else
				OUTPUT_BASEDIR=$PROTDIR/$OPTARG
			fi
			;;
		c)
			CLUSTER_NAME=$OPTARG
			;;
		f)
			INPUT_FILE=$OPTARG
			;;
		t)
			TIME_TO_RUN=$OPTARG
			;;
		\?)
			if [[ "$OPTARG" = '?' ]]; then
				usage
				return 1
			else
				print -u1 "ERROR: Option $OPTARG unknown"
				return 1
			fi
			;;
		*)
			print -u1 "ERROR: Option $opt unrecognized"
			return 1
			;;
		esac
	done

	# Check non-optional variables
	if [[ -z "$CLUSTER_NAME" ]]; then
		print -u1 "ERROR: You must specify a cluster (-c)"
		return 1
	fi

	if [[ -z "$INPUT_FILE" ]]; then
		print -u1 "ERROR: You must specify a data file (-f)"
		return 1
	fi

	# Set output directory
	OUTPUTDIR=$OUTPUT_BASEDIR/${LOGINNAME}_$$_ha_stress
	OUTPUTFILE=$OUTPUTDIR/ha_stress.log

	mkdir $OUTPUTDIR || return 1
}


##############################################################################
# Verify cluster communications
#
function verify_cluster_comm
{
	typeset node

	print "*** Verifying communications to all nodes"

	for node in ${NODENAME_LIST[*]}; do
		print " ** Verifying we can talk to: $node"

		$PING $node 3 > /dev/null 2>&1
		if [ $? -ne 0 ]; then
			print -u1 "ERROR: Node $node is unreachable"
			return 1
		else
			$RSH $node true 2>&1
			if [ $? -ne 0 ]; then
				print -u1 "ERROR: Cant talk to $node"
				print -u1 "ERROR: Check rhost entries on $node"
				return 1
			fi
		fi

	done

	return 0
}



##############################################################################
# Info Banner
#
function print_banner
{
	cat >&2 << EOF
=======================================================================
=== HA Stress Test Suite
=== Questions, problems? - Azeem Jiva (azeem@eng)
===
=== Output will be saved at: $OUTPUTDIR
===
=======================================================================

EOF
}


##############################################################################
# Read input file
#
# Will read the file that was prepared for this unit_stress run, the file
# will specify the parameters to run under, such as service name, and
# dependencies, etc.
#
function read_input_file
{
	typeset keyword value
	typeset line_count=0

	QUANTUM=10			# in secs
	SERVICE_NAME_PREFIX=test

	PROVIDER_SWITCHOVER_UNITS=2
	PROVIDER_MTB_SWITCHOVER=2

	SERVICE_START_END_UNITS=2
	SERVICE_MTB_START=3
	SERVICE_MTB_END=6
	SERVICE_MAX_COUNT=10
	SERVICE_MIN_COUNT=5
	DEPENDENCY_TREE_MAX_HEIGHT=3
	DEPENDENCY_TREE_MAX_WIDTH=5
	DEPENDENCY_TREE_MTB_START=10
	DEPENDENCY_TREE_MTB_END=12
	DEPENDENCY_TREE_MAX_COUNT=5
	DEPENDENCY_TREE_MIN_COUNT=1

	PROVIDER_MTB_ADD=3
	PROVIDER_MTB_REMOVE=5
	PROVIDER_MAX_COUNT=$NUM_NODES

	CLIENT_MAX_COUNT=5
	CLIENT_MIN_COUNT=0
	CLIENT_MTB_START=5
	CLIENT_MTB_END=3
	CLIENT_MAX_THREADS=5
	CLIENT_MIN_THREADS=3

	CMM_RECONF_UNITS=3
	MTB_CMM_RECONF=6

	if [[ -a "$INPUT_FILE" ]]; then
		print -u1 "  * Reading file $INPUT_FILE"
	else
		print -u1 "WARNING: File $INPUT_FILE does not exist"
		print -u1 "WARNING: Using stored default test parameters"
		return 0
	fi

	while read keyword value; do
		(( line_count += 1 ))
	
		# Ignore blank lines and comment lines
		[[ -z "$keyword" || "$keyword" = \#* ]] && continue

		case $keyword in
			QUANTUM)
				QUANTUM=$value
				;;
			SERVICE_NAME_PREFIX)
				SERVICE_NAME_PREFIX=$value
				;;
			PROVIDER_MTB_ADD)
				PROVIDER_MTB_ADD=$value
				;;
			PROVIDER_MTB_REMOVE)
				PROVIDER_MTB_REMOVE=$value
				;;
			PROVIDER_MAX_COUNT)
				PROVIDER_MAX_COUNT=$value
				;;
			PROVIDER_MTB_SWITCHOVER)
				PROVIDER_MTB_SWITCHOVER=$value
				;;
			PROVIDER_SWITCHOVER_UNITS)
				PROVIDER_SWITCHOVER_UNITS=$value
				;;
			SERVICE_MTB_END)
				PROVIDER_MTB_END=$value
				;;
			SERVICE_MTB_START)
				SERVICE_MTB_START=$value
				;;
			SERVICE_MAX_COUNT)
				SERVICE_MAX_COUNT=$value
				;;
			SERVICE_MIN_COUNT)
				SERVICE_MIN_COUNT=$value
				;;
			SERVICE_START_END_UNITS)
				SERVICE_START_END_UNITS=$value
				;;
			DEPENDENCY_TREE_MAX_HEIGHT)
				DEPENDENCY_TREE_MAX_HEIGHT=$value
				;;
			DEPENDENCY_TREE_MAX_WIDTH)
				DEPENDENCY_TREE_MAX_WIDTH=$value
				;;
			DEPENDENCY_TREE_MTB_START)
				DEPENDENCY_TREE_MTB_START=$value
				;;
			DEPENDENCY_TREE_MTB_END)
				DEPENDENCY_TREE_MTB_END=$value
				;;
			DEPENDENCY_TREE_MAX_COUNT)
				DEPENDENCY_TREE_MAX_COUNT=$value
				;;
			DEPENDENCY_TREE_MIN_COUNT)
				DEPENDENCY_TREE_MIN_COUNT=$value
				;;
			CLIENT_MAX_COUNT)
				CLIENT_MAX_COUNT=$value
				;;
			CLIENT_MIN_COUNT)
				CLIENT_MIN_COUNT=$value
				;;
			CLIENT_MTB_START)
				CLIENT_MTB_START=$value
				;;
			CLIENT_MTB_END)
				CLIENT_MTB_END=$value
				;;
			CLIENT_MAX_THREADS)
				CLIENT_MAX_THREADS=$value
				;;
			CLIENT_MIN_THREADS)
				CLIENT_MIN_THREADS=$value
				;;
			CMM_RECONF_UNITS)
				CMM_RECONF_UNITS=$value
				;;
			MTB_CMM_RECONF)
				MTB_CMM_RECONF=$value
				;;
		esac
	done < $INPUT_FILE

	return 0
}


##############################################################################
# print values we're gonna execute with
#
function print_values
{
	# Print the values we're running with
	cat >&2 << EOF

Execution values:
	QUANTUM				= $QUANTUM (seconds)
	SERVICE_NAME_PREFIX		= $SERVICE_NAME_PREFIX

	SERVICE_START_END_UNITS		= $SERVICE_START_END_UNITS
	SERVICE_MTB_START		= $SERVICE_MTB_START
	SERVICE_MTB_END			= $SERVICE_MTB_END
	SERVICE_MAX_COUNT		= $SERVICE_MAX_COUNT
	SERVICE_MIN_COUNT		= $SERVICE_MIN_COUNT
	DEPENDENCY_TREE_MTB_START	= $DEPENDENCY_TREE_MTB_START
	DEPENDENCY_TREE_MTB_END		= $DEPENDENCY_TREE_MTB_END
	DEPENDENCY_TREE_MAX_COUNT	= $DEPENDENCY_TREE_MAX_COUNT
	DEPENDENCY_TREE_MIN_COUNT	= $DEPENDENCY_TREE_MIN_COUNT
	DEPENDENCY_TREE_MAX_HEIGHT	= $DEPENDENCY_TREE_MAX_HEIGHT
	DEPENDENCY_TREE_MAX_WIDTH	= $DEPENDENCY_TREE_MAX_WIDTH

	PROVIDER_MTB_SWITCHOVER		= $PROVIDER_MTB_SWITCHOVER
	PROVIDER_SWITCHOVER_UNITS	= $PROVIDER_SWITCHOVER_UNITS

	PROVIDER_MTB_ADD		= $PROVIDER_MTB_ADD
	PROVIDER_MTB_REMOVE		= $PROVIDER_MTB_REMOVE
	PROVIDER_MAX_COUNT		= $PROVIDER_MAX_COUNT

	CLIENT_MAX_COUNT		= $CLIENT_MAX_COUNT
	CLIENT_MIN_COUNT		= $CLIENT_MIN_COUNT
	CLIENT_MAX_THREADS		= $CLIENT_MAX_THREADS
	CLIENT_MIN_THREADS		= $CLIENT_MIN_THREADS
	CLIENT_MTB_START		= $CLIENT_MTB_START
	CLIENT_MTB_END			= $CLIENT_MTB_END

	CMM_RECONF_UNITS		= $CMM_RECONF_UNITS
	MTB_CMM_RECONF			= $MTB_CMM_RECONF

EOF
}


##############################################################################
# setup_temp_work_area
#
function setup_temp_work_area
{
	TMP_DIR=$OUTPUTDIR/temp

	mkdir $TMP_DIR > /dev/null

	print " ** Temporary work area created: $TMP_DIR"

	return 0
}


##############################################################################
# get_load_info
#
# Will print 1 line of load information
# ie:
#	services: # clients: #
#
function get_load_info
{
	typeset index
	typeset node
	typeset service_count
	typeset latency
	typeset start_time
	typeset end_time
	typeset clients_in_core

	index=$(find_working_node)
	if [[ $? -ne 0 ]]; then
		print "all are down ($CLUSTER_NAME)"
		return 1
	fi

	node=${NODENAME_LIST[$index]}

	# Get service count
	service_count=$($RSH $node $REPLCTL 2>&1 | grep $SERVICE_NAME_PREFIX | grep -v "Depend" | wc -l)
	if [[ $? -ne 0 ]]; then
		service_count="n/a"
	else
		service_count=$(echo $service_count | awk '{print $1}')
	fi


	# Get latency to a node
	start_time=$(get_time)
	$RSH $node /usr/bin/true 1>/dev/null 2>/dev/null
	if [[ $? -ne 0 ]]; then
		latency="n/a"
	else
		end_time=$(get_time)
		(( latency = end_time - start_time ))
	fi

	# Find the number of clients currently in core
	clients_in_core=$($RSH $node "$LIST_CLIENT $SERVICE_NAME_PREFIX" 2>&1)
	if [[ $? -ne 0 ]]; then
		clients_in_core="n/a"
	fi

	print "services: $service_count latency: $node $latency clients: $clients_in_core"

	return 0
}


##############################################################################
# stop_switchover_units
#
function stop_switchover_units
{
	typeset index=0
	typeset count
	typeset pid

	count=${#UNIT_SWITCHOVER_PID_LIST[*]}

	while [[ $index -lt $count ]]; do
		pid=${UNIT_SWITCHOVER_PID_LIST[$index]}
		$KILL -INT $pid > /dev/null 2>&1

		(( index += 1 ))
	done

	index=0
	while [[ $index -lt $count ]]; do
		pid=${UNIT_SWITCHOVER_PID_LIST[$index]}

		print " ** Waiting for switchover unit to end ($pid)"
		wait $pid

		(( index += 1 ))
	done

	return 0
}


##############################################################################
# stop_start_end_svc_units
#
function stop_start_end_svc_units
{
	typeset index=0
	typeset count
	typeset pid

	count=${#UNIT_START_END_SVC_PID_LIST[*]}

	while [[ $index -lt $count ]]; do
		pid=${UNIT_START_END_SVC_PID_LIST[$index]}
		$KILL -INT $pid > /dev/null 2>&1

		(( index += 1 ))
	done

	index=0
	while [[ $index -lt $count ]]; do
		pid=${UNIT_START_END_SVC_PID_LIST[$index]}

		print " ** Waiting for start_end_svc unit to end ($pid)"
		wait $pid

		(( index += 1 ))
	done

	return 0
}


##############################################################################
# stop_cmm_reconf_units
#
function stop_cmm_reconf_units
{
	typeset index=0
	typeset count
	typeset pid

	count=${#UNIT_CMM_RECONF_PID_LIST[*]}

	while [[ $index -lt $count ]]; do
		pid=${UNIT_CMM_RECONF_PID_LIST[$index]}
		$KILL -INT $pid > /dev/null 2>&1

		(( index += 1 ))
	done

	index=0
	while [[ $index -lt $count ]]; do
		pid=${UNIT_CMM_RECONF_PID_LIST[$index]}

		print " ** Waiting for cmm_reconf unit to end ($pid)"
		wait $pid

		(( index += 1 ))
	done

	return 0
}


##############################################################################
# start_switchover_units
#
function start_switchover_units
{
	typeset count=0
	typeset filename
	typeset pid
	typeset log_file

	unset UNIT_SWITCHOVER_PID_LIST

	# Add a stop to cleanup routine
	ON_EXIT="stop_switchover_units; $ON_EXIT"

	filename=$TMP_DIR/unit_switchover.in

	print "QUANTUM $QUANTUM" > $filename
	print "PROVIDER_MTB_SWITCHOVER $PROVIDER_MTB_SWITCHOVER" >> $filename
	print "SERVICE_NAME_PREFIX $SERVICE_NAME_PREFIX" >> $filename

	while [[ $count -lt $PROVIDER_SWITCHOVER_UNITS ]]; do
		log_file=$TMP_DIR/unit_switchover_$count.out

		$UNIT_SWITCHOVER -c $CLUSTER_NAME -f $filename 1>$log_file 2>&1 &
		pid=$!

		print " ** Started switchover unit ($pid)"

		UNIT_SWITCHOVER_PID_LIST[$count]=$pid

		(( count += 1 ))
	done

	return 0
}


##############################################################################
# start_end_svc_units
#
function start_end_svc_units
{
	typeset count=0
	typeset filename
	typeset pid
	typeset log_file

	unset UNIT_START_END_SVC_PID_LIST

	# Add a stop_start_end_svc_unit routine
	ON_EXIT="stop_start_end_svc_units; $ON_EXIT"

	filename=$TMP_DIR/unit_start_end_svc.in

	print "QUANTUM $QUANTUM" > $filename
	print "SERVICE_NAME_PREFIX $SERVICE_NAME_PREFIX" >> $filename
	print "SERVICE_MTB_END $SERVICE_MTB_END" >> $filename
	print "SERVICE_MTB_START $SERVICE_MTB_START" >> $filename
	print "SERVICE_MAX_COUNT $SERVICE_MAX_COUNT" >> $filename
	print "SERVICE_MIN_COUNT $SERVICE_MIN_COUNT" >> $filename
	print "PROVIDER_MTB_REMOVE $PROVIDER_MTB_REMOVE" >> $filename
	print "PROVIDER_MTB_ADD $PROVIDER_MTB_ADD" >> $filename
	print "PROVIDER_MAX_COUNT $PROVIDER_MAX_COUNT" >> $filename

	print "DEPENDENCY_TREE_MAX_HEIGHT $DEPENDENCY_TREE_MAX_HEIGHT" >> $filename
	print "DEPENDENCY_TREE_MAX_WIDTH $DEPENDENCY_TREE_MAX_WIDTH" >> $filename
	print "DEPENDENCY_TREE_MTB_START $DEPENDENCY_TREE_MTB_START" >> $filename
	print "DEPENDENCY_TREE_MTB_END $DEPENDENCY_TREE_MTB_END" >> $filename
	print "DEPENDENCY_TREE_MAX_COUNT $DEPENDENCY_TREE_MAX_COUNT" >> $filename
	print "DEPENDENCY_TREE_MIN_COUNT $DEPENDENCY_TREE_MIN_COUNT" >> $filename

	print "CLIENT_MAX_COUNT $CLIENT_MAX_COUNT" >> $filename
	print "CLIENT_MIN_COUNT $CLIENT_MIN_COUNT" >> $filename
	print "CLIENT_MTB_START $CLIENT_MTB_START" >> $filename
	print "CLIENT_MTB_END $CLIENT_MTB_END" >> $filename
	print "CLIENT_MAX_THREADS $CLIENT_MAX_THREADS" >> $filename
	print "CLIENT_MIN_THREADS $CLIENT_MIN_THREADS" >> $filename

	while [[ $count -lt $SERVICE_START_END_UNITS ]]; do

		log_file=$TMP_DIR/unit_start_end_svc_$count.out

		$UNIT_START_END_SVC -c $CLUSTER_NAME -f $filename -t $TMP_DIR 1>$log_file 2>&1 &
		pid=$!

		print " ** Started start_end_svc unit ($pid)"

		UNIT_START_END_SVC_PID_LIST[$count]=$pid
		(( count += 1 ))
	done

	return 0
}


##############################################################################
# start cmm reconf units
# 
function start_cmm_reconf_units
{
	typeset count=0
	typeset filename
	typeset pid
	typeset log_file

	unset UNIT_CMM_RECONF_PID_LIST

	# Add a stop to cleanup routine
	ON_EXIT="stop_cmm_reconf_units; $ON_EXIT"

	filename=$TMP_DIR/unit_cmm_reconf.in

	print "QUANTUM $QUANTUM" > $filename
	print "MTB_CMM_RECONF $MTB_CMM_RECONF" >> $filename

	while [[ $count -lt $CMM_RECONF_UNITS ]]; do
		log_file=$TMP_DIR/unit_cmm_reconf_$count.out

		$UNIT_CMM_RECONF -c $CLUSTER_NAME -f $filename 1>$log_file 2>&1 &
		pid=$!
		print " ** Started cmm_reconf unit ($pid)"

		UNIT_CMM_RECONF_PID_LIST[$count]=$pid
		(( count += 1 ))
	done

	return 0
}


##############################################################################
# prepare logs for output
#
function prepare_logs
{
	$SORT -k 1 $TMP_DIR/*.out > $OUTPUTFILE
	print "*** Log file: $OUTPUTFILE"

	return 0
}


##############################################################################
# wait msg
#
function wait_msg
{
	print "*** HA Stress Test suite exiting, please wait for cleanup"
	print "*** This operation can take several minutes"

	return 0
}

##############################################################################
# Main Execution
#
function main
{
	typeset elapsed_time
	typeset load_info

	# Check that tools exists
	verify_tools
	if [[ $? -ne 0 ]]; then
		return 1
	fi

	# Examine command line
	eval "examine_command_line $@"
	if [[ $? -ne 0 ]]; then
		return 1
	fi

	# Init cluster structure
	init_cluster_structure
	if [[ $? -ne 0 ]]; then
		return 1
	fi

	# Print banner
	print_banner

	# Verify communications with cluster
	verify_cluster_comm
	if [[ $? -ne 0 ]]; then
		return 1
	fi

	# Read input file
	read_input_file
	if [[ $? -ne 0 ]]; then
		return 1
	fi

	# Print values (we're running with)
	print_values
	
	# Mark logs to be removed
	REMOVE_LOGS=NO

	# create tmp work area
	setup_temp_work_area
	if [[ $? -ne 0 ]]; then
		return 1
	fi

	ON_EXIT="prepare_logs; $ON_EXIT"

	# Setup the units
	start_end_svc_units
	if [[ $? -ne 0 ]]; then
		return 1
	fi

	start_switchover_units
	if [[ $? -ne 0 ]]; then
		return 1
	fi

	start_cmm_reconf_units
	if [[ $? -ne 0 ]]; then
		return 1
	fi

	ON_EXIT="wait_msg; $ON_EXIT"

	while true; do
		elapsed_time=$(get_elapsed_time)
		TIME_RAN=$(get_elapsed_time)
		load_info=$(get_load_info)

		spent_time
		if [[ $? -ne 0 ]]; then
			return 0
		fi
		print "  * Elapsed: $elapsed_time $load_info"
		sleep 15
	done
}


##############################################################################
# Shell Starting Point
#
# Call main
ON_EXIT=cleanup
trap 'eval "$ON_EXIT"' EXIT
eval "main $@"
exit $?
