#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)unit_svc.ksh	1.11	08/05/20 SMI"
#

#
#
# Node Stress
#
# A copy of this shell script will run on ever node on a cluster that
# is running the HA stress test suite.  This script will make sure that
# the node its running on has its share of the load running.
#

readonly PROGNAME=$0		# Program name
readonly THIS_PROG=${0##*/}

PROGDIR=$(dirname $PROGNAME)	# Directory of program
if [[ "$PROGDIR" = "." ]]
then
	PROGDIR=$PWD
fi
readonly PROGDIR

##############################################################################
# Files that need to be sourced
#
# Source the aux file
. $PROGDIR/ha_stress_aux


##############################################################################
# Global Settings
#

set -o monitor			# so bg jobs can run with their own PGID and
				# we can trap SIGCHLD
set +o nounset			# allow use of unset variables
set +o notify			# dont notify of bg job completion


RANDOM=$$			# some seed
TMP_DIR="/tmp/HAstress.tmp"	# Temporary Directory
DONE=0				# Done is 1 when complete
unset PROV_MAP			# Map of where providers have been started
PROV_COUNT=0			# Number of providers
readonly LOGINNAME=$(logname)	# User's name

#
# RANDOM Globals
#
PROV_ADD_RANDOM=$RANDOM	# Will be used in mtb start
PROV_REMOVE_RANDOM=$RANDOM	# Will be used in mtb shutdown

#
# Tool location
#
readonly RSH="rsh -n -l root"
readonly PING=/usr/sbin/ping
readonly REPLCTL=/usr/cluster/bin/replctl
readonly WRAPPER=/usr/cluster/orbtest/ha_stress/wrapper

#
# Unit's locaqtion
#
readonly UNIT_CLIENT=$PROGDIR/unit_client
UNIT_CLIENT_PID=""

#
# Time constants
#
readonly START_HOURS=$($DATE +%H)
readonly START_MINUTES=$($DATE +%M)
readonly START_SECONDS=$($DATE +%S)

#
# Repl Sample
#
readonly REPL_SAMPLE_ENTRY=ha_sample_server

#
# KProxy
#
readonly KPROXY_CLIENT="/usr/cluster/orbtest/test_kproxy/kproxy_client"


##############################################################################
# Verify tools
#
function verify_tools
{
	typeset prog

	# Check to see if programs are there
	for prog in $MC_INFO_CLUSTER $DATE $PING
	do
		if [[ ! -x $prog ]]
		then
			print_info ERROR verify_tools "Can't find $prog"
			return 1
		fi
	done

	return 0
}


##############################################################################
# cleanup when exiting
#
function cleanup
{
	typeset pid

	# Kill all child processes
	for pid in $(jobs -p)
	do
		kill -INT -$pid 2>/dev/null
	done
	wait

	return 0
}


##############################################################################
# Print usage of this script
#
function usage
{
	cat >&2 << EOF

Usage:
	$PROGNAME -?
	$PROGNAME -f <file> -c <cluster> -t <path>

Options:
	-f	- file with command options
	-c	- cluster
	-t	- temporary work area

EOF
}


##############################################################################
# Examine command line
#
function examine_command_line
{
	typeset opt

	while getopts ':t:c:f:h?' opt
	do
		case $opt in
		f)
			INPUT_FILE=$OPTARG
			;;
		c)
			CLUSTER_NAME=$OPTARG
			;;
		esac
	done

	# Make sure file exists, otherwise print error
	if [[ ! -e "$INPUT_FILE" ]]
	then
		print_info ERROR examine_command_line \
			"You must specify an input file (-f)"
		return 1
	fi

	# Need a cluster name, otherwise can't test a cluster!
	if [[ -z "$CLUSTER_NAME" ]]
	then
		print_info ERROR examine_command_line \
			"You must specify a cluster (-c)"
		return 1
	fi

	return 0
}


##############################################################################
# Read input file
#
# Will read the file that was prepared for this unit_stress run, the file
# will specify the parameters to run under, such as service name, and
# dependencies, etc.
#
# Will set the following variables
#	SERVICE_NAME
#	SERVICE_DEPENDS
#	PROVIDER_MTB_REMOVE
#	PROVIDER_MTB_ADD
#	PROVIDER_MAX_COUNT
#
#	CLIENT_MAX_COUNT
#	CLIENT_MIN_COUNT
#	CLIENT_MTB_START
#	CLIENT_MTB_END
#	CLIENT_MAX_THREADS
#	CLIENT_MIN_THREADS
#
#	QUANTUM
#
function read_input_file
{
	typeset keyword value
	typeset line_count=0

	# Does file exist?
	if [[ ! -e "$INPUT_FILE" ]]
	then
		print_info ERROR read_input_file \
			"File $INPUT_FILE does not exist"
		return 1
	fi

	# Values for running service
	SERVICE_NAME=""
	SERVICE_MTB_REMOVE=""
	SERVICE_MTB_ADD=""
	SERVICE_DEPENDS=""
	PROVIDER_MAX_COUNT=$NUM_NODES

	CLIENT_MAX_COUNT=""
	CLIENT_MIN_COUNT=""
	CLIENT_MTB_START=""
	CLIENT_MTB_END=""
	CLIENT_MAX_THREADS=""
	CLIENT_MIN_THREADS=""

	QUANTUM=60			# in secs

	# Fill in above values
	while read keyword value
	do
		(( line_count += 1 ))
	
		# Ignore blank lines and comment lines
		[[ -z "$keyword" || "$keyword" = \#* ]] && continue

		case $keyword in
			SERVICE_NAME)
				SERVICE_NAME=$value
				;;
			SERVICE_DEPENDS)
				SERVICE_DEPENDS=$value
				;;
			PROVIDER_MTB_REMOVE)
				PROVIDER_MTB_REMOVE=$value
				;;
			PROVIDER_MTB_ADD)
				PROVIDER_MTB_ADD=$value
				;;
			PROVIDER_MAX_COUNT)
				PROVIDER_MAX_COUNT=$value
				;;
			CLIENT_MAX_COUNT)
				CLIENT_MAX_COUNT=$value
				;;
			CLIENT_MIN_COUNT)
				CLIENT_MIN_COUNT=$value
				;;
			CLIENT_MTB_START)
				CLIENT_MTB_START=$value
				;;
			CLIENT_MTB_END)
				CLIENT_MTB_END=$value
				;;
			CLIENT_MAX_THREADS)
				CLIENT_MAX_THREADS=$value
				;;
			CLIENT_MIN_THREADS)
				CLIENT_MIN_THREADS=$value
				;;
			QUANTUM)
				QUANTUM=$value
				;;
		esac
	done < $INPUT_FILE

	# Make sure value was set
	if [[ -z "$SERVICE_NAME" ]]
	then
		print_info ERROR read_input_file  "Missing SERVICE_NAME"
		return 1
	fi

	# Make sure value was set
	PROVIDER_MAX_COUNT=$NUM_NODES
	if [[ -z "$PROVIDER_MTB_REMOVE" ]]
	then
		print_info ERROR read_input_file "Missing PROVIDER_MTB_REMOVE"
		return 1
	fi

	# Make sure value was set
	if [[ -z "$PROVIDER_MTB_ADD" ]]
	then
		print_info ERROR read_input_file "Missing PROVIDER_MTB_ADD"
		return 1
	fi

	# Make sure value was set
	if [[ -z "$CLIENT_MAX_COUNT" ]]
	then
		print_info ERROR read_input_file "Missing CLIENT_MAX_COUNT"
		return 1
	fi

	# Make sure value was set
	if [[ -z "$CLIENT_MIN_COUNT" ]]
	then
		print_info ERROR read_input_file "Missing CLIENT_MIN_COUNT"
		return 1
	fi

	# Make sure value was set
	if [[ -z "$CLIENT_MTB_START" ]]
	then
		print_info ERROR read_input_file "Missing CLIENT_MTB_START"
		return 1
	fi

	# Make sure value was set
	if [[ -z "$CLIENT_MTB_END" ]]
	then
		print_info ERROR read_input_file "Missing CLIENT_MTB_END"
		return 1
	fi

	# Make sure value was set
	if [[ -z "$CLIENT_MAX_THREADS" ]]
	then
		print_info ERROR read_input_file "Missing CLIENT_MAX_THREADS"
		return 1
	fi

	# Make sure value was set
	if [[ -z "$CLIENT_MIN_THREADS" ]]
	then
		print_info ERROR read_input_file "Missing CLIENT_MIN_THREADS"
		return 1
	fi

	return 0
}

##############################################################################
# Start provider
#
# Arguments:
#	node to start provider on
#
function start_provider
{
	typeset index
	typeset node
	typeset nodeid
	typeset rslt

	index=$1

	node=${NODENAME_LIST[$index]}
	nodeid=${NODEID_LIST[$index]}

	# If there is a provider then don't start one
	if [[ ${PROV_MAP[$nodeid]} -eq 1 ]]
	then
		return 1
	fi

	# If too many providers, then don't start anymore
	if [[ $PROV_COUNT -ge $PROVIDER_MAX_COUNT ]]
	then
		return -1
	fi

	# Check to see if node is up
	$PING $node 3 > /dev/null
	if [ $? -ne 0 ]
	then
		print_info $PR_PROV_ADD $SERVICE_NAME "$node is not responding"
		return -1
	fi

	# Run provider on node
	rslt=$($RSH $node "$WRAPPER -r /dev/null $KPROXY_CLIENT \
			$REPL_SAMPLE_ENTRY $SERVICE_NAME $SERVICE_DEPENDS")

	# Was there a problem?
	if [[ $? -ne 0 ]]
	then
		print_info $PR_PROV_ADD $SERVICE_NAME \
			"rsh returned non-zero on $node"
		rslt=1
	fi

	# Woohoo it ran!
	if [[ $rslt -eq 0 ]]
	then
		print_info $PR_PROV_ADD $SERVICE_NAME "on $node"

		PROV_MAP[$nodeid]=1
		(( PROV_COUNT += 1 ))
	else
		print_info $PR_PROV_ADD $SERVICE_NAME "failed on $node"
	fi

	return 0
}


##############################################################################
# shutdown provider
#
function shutdown_provider
{
	typeset index
	typeset node
	typeset nodeid
	typeset rslt
	typeset rsh_rslt

	index=$1

	node=${NODENAME_LIST[$index]}
	nodeid=${NODEID_LIST[$index]}

	print_info $PR_INFO $PR_PROV_REMOVE $SERVICE_NAME "on $node"

	# If there are no providers to stop, then don't
	if [[ ${PROV_MAP[$nodeid]} -eq 0 ]]
	then
		return 1
	fi

	# If there are no providers to stop, then don't
	if [[ $PROV_COUNT -le 1 ]]
	then
		return 1
	fi

	# Check to see if node is up
	$PING $node 3 > /dev/null
	if [ $? -ne 0 ]	
	then

		print_info $PR_PROV_REMOVE $SERVICE_NAME \
			"$node is not responding"
		return -1
	fi

	# Stop the provider
	rsh_rslt=$($RSH $node "$WRAPPER -r /dev/null $REPLCTL -f -c \
			$SERVICE_NAME $nodeid remove")
	rslt=$?

	# If it worked, then decrease the total provider number
	if [[ $rslt -eq 0 ]] && [[ $rsh_rslt -eq 0 ]]
	then
		(( PROV_COUNT -= 1 ))
		PROV_MAP[$nodeid]=0
	fi

	if [[ $rslt -ne 0 ]]
	then
		print_info $PR_PROV_REMOVE $SERVICE_NAME \
			"rsh returned non-zero on $node"
		return $rslt
	else
		rslt=$rsh_rslt
	fi

	# Otherwise, print error
	if [[ $rsh_rslt -ne 0 ]]
	then
		print_info $PR_PROV_REMOVE $SERVICE_NAME "failed on $node"
	fi

	return $rslt
}


##############################################################################
# shutdown service
#
function shutdown_service
{
	typeset shutdown_pid
	typeset index
	typeset node

	# Shutdown the client in the background
	if [[ -n "$UNIT_CLIENT_PID" ]]
	then
		kill -INT -$UNIT_CLIENT_PID 1>/dev/null 2>/dev/null
	fi

	# Any nodes up an running?
	index=$(find_working_node)
	if [[ $? -ne 0 ]]
	then
		print_info $PR_CLIENT_END $SERVICE_NAME \
			"all nodes are dead ($CLUSTER_NAME)"
		return 1
	fi

	node=${NODENAME_LIST[$index]}
	print_info $PR_SVC_END $SERVICE_NAME "on node $node"

	$RSH $node "$WRAPPER -r /dev/null $REPLCTL -x $SERVICE_NAME 2>&1" \
			1>/dev/null 2>&1
	if [[ $? -ne 0 ]]
	then
		print_info $PR_CLIENT_END $SERVICE_NAME \
			"rsh returned non-zero on $node"
		print_info $PR_CLIENT_END $SERVICE_NAME \
			"could not be stopped, test may deadlock!"
	fi

	if [[ -n "$UNIT_CLIENT_PID" ]]
	then
		print_info $PR_CLIENT_END $SERVICE_NAME \
			"waiting for clients to finish"
		wait $UNIT_CLIENT_PID
		UNIT_CLIENT_PID=""
	fi

	cleanup

	return 0
}


##############################################################################
# do_work_prov_start
#
# Will figure out if a provider needs to be started (due to the mtb start)
function do_work_prov_start
{
	typeset num
	typeset index
	typeset nodeid

	# Calculation lifted from solaris (and now cmm code)
	# This algorithm was lifeted from the work by Jeff Bonwick in the
	# kmem allocated from usr/src/uts/common/os/kmem.c
	(( PROV_ADD_RANDOM *= 2416 ))
	(( PROV_ADD_RANDOM += 374441 ))
	(( PROV_ADD_RANDOM %= 1771875 ))

	(( num = 1771875 / PROVIDER_MTB_ADD ))

	# If we already reached the MAX_count, then stop
	if [[ $PROV_COUNT -ge $PROVIDER_MAX_COUNT ]]
	then
		return 0
	fi

	# If there are no nodes left to start provider on, stop
	if [[ $PROV_COUNT -ge $NUM_NODES ]]
	then
		return 0
	fi

	if [[ $PROV_ADD_RANDOM -lt $num ]]
	then
		# We hit the MTB Start
		index=$(find_working_node)
		if [[ $? -ne 0 ]]
		then
			print_info $PR_PROV_ADD $SERVICE_NAME \
				"all nodes are dead ($CLUSTER_NAME)"
			return 1
		fi

		nodeid=${NODEID_LIST[$index]}

		while [[ ${PROV_MAP[$nodeid]} -eq 1 ]]
		do
			#
			# If the random node we picked aleady had a provider
			# running in it, then we will just look at the next
			# node, to start a provider in it
			#
			(( index += 1 ))
			if [[ $index -ge $NUM_NODES ]]
			then
				index=0
			fi
			nodeid=${NODEID_LIST[$index]}
		done

		start_provider $index
	fi

	return 0
}

##############################################################################
# do_work_prov_shutdown
#
# Will figure out if a provider needs to be sutdown (due to mtb start)
function do_work_prov_shutdown
{
	typeset num

	# Calculation lifted from solaris (and now cmm code)
	# This algorithm was lifeted from the work by Jeff Bonwick in the
	# kmem allocated from usr/src/uts/common/os/kmem.c
	(( PROV_REMOVE_RANDOM *= 2416 ))
	(( PROV_REMOVE_RANDOM += 374441 ))
	(( PROV_REMOVE_RANDOM %= 1771875 ))

	(( num = 1771875 / PROVIDER_MTB_REMOVE ))

	# Any providers left?  If not then don't do anything
	if [[ $PROV_COUNT -le 1 ]]
	then
		return 0
	fi

	# If there are some then kill one
	if [[ $PROV_REMOVE_RANDOM -lt $num ]]
	then
		# We hit the MTB shutdown
		index=$(find_working_node)
		if [[ $? -ne 0 ]]
		then
			print_info $PR_PROV_REMOVE $SERVICE_NAME \
				"all nodes are dead ($CLUSTER_NAME)"
			return 1
		fi
		nodeid=${NODEID_LIST[$index]}

		while [[ ${PROV_MAP[$nodeid]} -eq 0 ]]
		do
			#
			# If the random node we picked does not have a provider
			# running on it, then we will just look at the next
			# node, to shutdown a provider in it
			#
			(( index += 1 ))
			if [[ $index -ge $NUM_NODES ]]
			then
				index=0
			fi
			nodeid=${NODEID_LIST[$index]}
		done

		shutdown_provider $index
	fi


	return 0
}


##############################################################################
# start_unit_client
#
# Sets up the input file for the client unit
#
function start_unit_client
{

	typeset tmp_file
	typeset out_file

	# Create a unique_name
	tmp_file=$TMP_DIR/unit_client.$SERVICE_NAME.in
	out_file=$TMP_DIR/unit_client.$SERVICE_NAME.out

	rm -rf $tmp_file 1>/dev/null 2>/dev/null

	# Required values for unit_client
	print "QUANTUM $QUANTUM" > $tmp_file
	print "SERVICE_NAME $SERVICE_NAME" >> $tmp_file
	print "CLIENT_MAX_COUNT $CLIENT_MAX_COUNT" >> $tmp_file
	print "CLIENT_MIN_COUNT $CLIENT_MIN_COUNT" >> $tmp_file
	print "CLIENT_MTB_START $CLIENT_MTB_START" >> $tmp_file
	print "CLIENT_MTB_END $CLIENT_MTB_END" >> $tmp_file
	print "CLIENT_MAX_THREADS $CLIENT_MAX_THREADS" >> $tmp_file
	print "CLIENT_MIN_THREADS $CLIENT_MIN_THREADS" >> $tmp_file

	# Run client with parameters
	$UNIT_CLIENT -f $tmp_file -c $CLUSTER_NAME 1>$out_file 2>&1 &
	UNIT_CLIENT_PID=$!

	return 0

}

##############################################################################
# Do stress sequence
#
# This function will run until interrupted, sleeping for the quantum time
# and at each quantum it will call functions to figure out if a provider
# needs to be started or killed, etc
# 
function do_stress_sequence
{
	typeset	shutdown_pid
	typeset sleep_time
	typeset start_time
	typeset end_time
	typeset diff_time

	# Start the client UNIT
	start_unit_client

	# Seed the start time counter
	start_time=$(get_time)

	while [[ $DONE -eq 0 ]]
	do
		# Get the end time
		end_time=$(get_time)

		# Calculate the time spent executing
		(( diff_time = end_time - start_time ))
		(( sleep_time = QUANTUM - diff_time ))
		if [[ $sleep_time -gt 0 ]]
		then
			sleep $sleep_time
		fi

		# Get the start time
		start_time=$(get_time)

		# Do Work
		do_work_prov_start
		do_work_prov_shutdown
	done

	return 0
}


##############################################################################
# Main Execution
#
function main
{
	typeset index
	typeset	node

	# Check that tools exists
	if ! verify_tools
	then
		return 1
	fi

	# Examine command line
	if ! examine_command_line "$@"
	then
		return 1
	fi

	# Init cluster structure
	if ! init_cluster_structure
	then
		return 1
	fi

	# Read the input file
	if ! read_input_file
	then
		return 1
	fi

	# Execute Stress Sequences
	if ! do_stress_sequence
	then
		return 1
	fi

	return 0
}


##############################################################################
# Call main
#
trap 'shutdown_service' EXIT
main "$@"
