#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)unit_switchover.ksh	1.10	08/05/20 SMI"
#

#
#
# Node Stress
#
# A copy of this shell script will run on ever node on a cluster that
# is running the HA stress test suite.  This script will make sure that
# the node its running on has its share of the load running.
#

readonly PROGNAME=$0		# Program name
readonly THIS_PROG=${0##*/}

PROGDIR=$(dirname $PROGNAME)	# Directory of program
if [[ "$PROGDIR" = "." ]]
then
	PROGDIR=$PWD
fi
readonly PROGDIR

##############################################################################
# Files that need to be sourced
#
# Source the aux file
. $PROGDIR/ha_stress_aux

##############################################################################
# Global Settings
#
set -o monitor                  # so bg jobs can run with their own PGID and
                                # we can trap SIGCHLD

RANDOM=$$			# some seed
DONE=0
readonly LOGINNAME=$(logname)	# User's name

#
# RANDOM Globals
#
PROV_SWITCHOVER_RANDOM=$RANDOM	# Will be used in mtb shutdown

#
# Tool location
#
readonly RSH="rsh -n -l root"
readonly PING=/usr/sbin/ping
readonly REPLCTL=/usr/cluster/bin/replctl
readonly WRAPPER=/usr/cluster/orbtest/ha_stress/wrapper

#
# Time constants
#
readonly START_HOURS=$($DATE +%H)
readonly START_MINUTES=$($DATE +%M)
readonly START_SECONDS=$($DATE +%S)

##############################################################################
# cleanup when exiting
#
function cleanup
{
	typeset pid

	# Kill all background jobs
	for pid in $(jobs -p)
	do
		kill -INT -$pid 2>/dev/null
	done

	wait
	return 0
}

##############################################################################
# Examine command line
#
function examine_command_line
{
	typeset opt

	while getopts ':c:f:' opt
	do
		case $opt in
		c)
			CLUSTER_NAME=$OPTARG
			;;
		f)
			DATA_FILE=$OPTARG
			;;
		esac
	done

	# Is there a data file somewhere?
	if [[ ! -e "$DATA_FILE" ]]
	then
		print_info ERROR examine_command_line "$DATA_FILE not found"
		return 1
	fi

	# Need a cluster name, otherwise can't test a cluster!
        if [[ -z "$CLUSTER_NAME" ]]
        then
                print_info ERROR examine_command_line \
                        "You must specify a cluster (-c)"
                return 1
        fi


	return 0
}


##############################################################################
# Read input file
#
# Will read the file that was prepared for this unit_stress run, the file
# will specify the parameters to run under, such as service name, and
# dependencies, etc.
#
# Will set the following variables
#	PROVIDER_MTB_SWITCHOVER
#	QUANTUM
#
function read_input_file
{
	typeset keyword value
	typeset line_count=0

	# Does the data file exist?
	if [[ ! -e "$DATA_FILE" ]]
	then
		print_info ERROR read_input_file "$DATA_FILE does not exist"
		return 1
	fi

	SERVICE_NAME_PREFIX=""
	PROVIDER_MTB_SWITCHOVER=""
	QUANTUM=60	#in secs

	# Read in above values
	while read keyword value
	do
		(( line_count += 1 ))
	
		# Ignore blank lines and comment lines
		[[ -z "$keyword" || "$keyword" = \#* ]] && continue

		case $keyword in
			PROVIDER_MTB_SWITCHOVER)
				PROVIDER_MTB_SWITCHOVER=$value
				;;
			SERVICE_NAME_PREFIX)
				SERVICE_NAME_PREFIX=$value
				;;
			QUANTUM)
				QUANTUM=$value
				;;
		esac
	done < $DATA_FILE

	# Is the value there?  If not then print error
	if [[ -z "$PROVIDER_MTB_SWITCHOVER" ]]
	then
		print_info ERROR read_input_file \
			"Missing PROVIDER_MTB_SWITCHOVER"
		return 1
	fi

	# Is the value there?  If not then print error
	if [[ -z "$SERVICE_NAME_PREFIX" ]]
	then
		print_info ERROR read_input_file \
			"Missing SERVICE_NAME_PREFIX"
		return 1
	fi

	return 0
}


##############################################################################
# switchover provider
#
# Arguments:
#	node to run commandon
#	service_name
#	provider_name
#	state to switch to
function switchover_provider
{
	typeset node=$1
	typeset service_name=$2
	typeset prov_name=$3
	typeset state=$4
	typeset rslt
	typeset rsh_rslt

	# Is node up?
	$PING $node 3 > /dev/null
	if [ $? -ne 0 ]
	then
		print_info $PR_WARNING switchover_provider \
			"$node is not responding"
		return -1
	fi

	# Change state of provider
	print_info $PR_INFO switchover_provider "$prov_name to $state on $node"

	rsh_rslt=$($RSH $node "$WRAPPER -r /dev/null $REPLCTL -f -c $service_name $prov_name $state")
	rslt=$?

	# Return result to calling function
	if [[ $rslt -eq 0 ]] && [[ $rsh_rslt -ne 0 ]]
	then
		rslt=$rsh_rslt
	fi

	return $rslt
}


##############################################################################
# do_work_prov_switchover
#
# Will figure out if a provider needs to be switchover (due to mtb switchover)
function do_work_prov_switchover
{
	typeset num
	typeset service_name
	typeset service_list
	typeset prov_name_list
	typeset prov_name
	typeset size
	typeset index
	typeset node
	typeset tmp

	# Calculation lifted from solaris (and now cmm code)
	# This algorithm was lifeted from the work by Jeff Bonwick in the
	# kmem allocated from usr/src/uts/common/os/kmem.c
	(( PROV_SWITCHOVER_RANDOM *= 2416 ))
	(( PROV_SWITCHOVER_RANDOM += 374441 ))
	(( PROV_SWITCHOVER_RANDOM %= 1771875 ))

	(( num = 1771875 / $PROVIDER_MTB_SWITCHOVER ))

	if [[ $PROV_SWITCHOVER_RANDOM -lt $num ]]
	then
		# Select a node to do this on
		index=$(find_working_node)
		if [[ $? -ne 0 ]]
		then
			print_info $PR_ERROR do_work_prov_switchover \
				"all nodes are down ($CLUSTER_NAME)"
			return 1
		fi

		node=${NODENAME_LIST[$index]}

		# Get the service list
		set -A service_list $($RSH $node "$REPLCTL 2>&1" \
		       | grep UP | grep $SERVICE_NAME_PREFIX | awk '{print $2}')

		if [[ $? -ne 0 ]]
		then
			print_info $PR_WARNING do_work_prov_switchover \
				"rsh failed on $node"
			return 1
		fi

		size=${#service_list[*]}
		if [[ $size -le 0 ]]
		then
			return 0
		fi

		index=$(random_number 1 $size)
		(( index -= 1 ))
		service_name=${service_list[$index]}

		# Get the prov list for that service (secondary prov list)
		set -A prov_name_list $($RSH $node "$REPLCTL -d $service_name \
			2>&1" | grep SECONDARY | awk '{print $1 }')
		if [[ $? -ne 0 ]]
		then
			print_info $PR_WARNING do_work_prov_switchover \
				"rsh failed on $node"
			return 1
		fi

		size=${#prov_name_list[*]}
		if [[ $size -le 0 ]]
		then
			return 0
		fi

		index=$(random_number 1 $size)
		(( index -= 1 ))
		prov_name=${prov_name_list[$index]}

		# Switchover the provider we found
		switchover_provider $node $service_name $prov_name primary
	fi

	return 0
}

##############################################################################
# Do stress sequence
#
# This function will run until interrupted, sleeping for the quantum time
# and at each quantum it will call functions to figure out if a provider
# needs to be started or killed, etc
# 
function do_stress_sequence
{
	typeset sleep_time
	typeset start_time
	typeset end_time
	typeset diff_time

	# Seed the start time counter
	start_time=$(get_time)

	while [[ $DONE -eq 0 ]]
	do
		# Get the end time
		end_time=$(get_time)

		# Calculate the time spent executing
		(( diff_time = end_time - start_time ))
		(( sleep_time = QUANTUM - diff_time ))
		if [[ $sleep_time -gt 0 ]]
		then
			sleep $sleep_time
		fi

		# Get the start time
		start_time=$(get_time)

		# Do Work
		do_work_prov_switchover
	done

	return 0
}

##############################################################################
# Main Execution
#
function main
{
	typeset index
	typeset	node

	# Examine command line
	if ! examine_command_line "$@"
	then
		return 1
	fi

	# Init cluster structure
	if ! init_cluster_structure
	then
		return 1
	fi

	# Read the input file
	if ! read_input_file
	then
		return 1
	fi

	# Execute Stress Sequences
	if ! do_stress_sequence
	then
		return 1
	fi

	return 0
}


##############################################################################
# Call main
#
trap 'cleanup' EXIT
main "$@"
