#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)unit_start_end_svc.ksh	1.13	08/05/20 SMI"
#

#
# Node Stress
#
# A copy of this shell script will run on ever node on a cluster that
# is running the HA stress test suite.  This script will make sure that
# the node its running on has its share of the load running.
#

readonly PROGNAME=$0		# Program name
readonly THIS_PROG=${0##*/}

PROGDIR=$(dirname $PROGNAME)	# Directory of program
if [[ "$PROGDIR" = "." ]]
then
	PROGDIR=$PWD
fi
readonly PROGDIR

##############################################################################
# Files that need to be sourced
#
# Source the aux file
. $PROGDIR/ha_stress_aux


##############################################################################
# Global Settings

set -o monitor			# so bg jobs can run with their own PGID and
				# we can trap SIGCHLD
set +o notify			# dont notify of bg job completion

RANDOM=$$			# some seed
DONE=0
SVC_COUNTER=0			# Of service that this unit has started
TMP_DIR="/tmp/HAstress.tmp"	# Temporary Dir
DEP_TREE_COUNTER=0		# Of service trees started
readonly LOGINNAME=$(logname)	# User's name

#
# RANDOM Globals
#
SERVICE_START_RANDOM=$RANDOM	# Will be used in mtb start
SERVICE_END_RANDOM=$RANDOM	# Will be used in mtb end
DEP_TREE_START_RANDOM=$RANDOM	# Will be used in mtb start
DEP_TREE_END_RANDOM=$RANDOM	# Will be used in mtb end

#
# Keeps track of the service unit's pid
#
SERVICE_LIST_INDEX=0
unset SERVICE_PID_LIST
unset SERVICE_NAME_LIST

#
# Keeps track of the dependent service unit's pid
#
DEP_TREE_LIST_INDEX=0
unset DEP_TREE_PID_LIST


#
# Tool location
#
readonly RSH="rsh -n -l root"
readonly PING=/usr/sbin/ping
readonly REPLCTL=/usr/cluster/bin/replctl
readonly SORT=/bin/sort

#
# Other HA_STRESS units
#
readonly UNIT_SVC=$PROGDIR/unit_svc
readonly UNIT_DEP_SVC=$PROGDIR/unit_dep_svc

#
# Time constants
#
readonly START_HOURS=$($DATE +%H)
readonly START_MINUTES=$($DATE +%M)
readonly START_SECONDS=$($DATE +%S)

##############################################################################
# Verify tools
#
function verify_tools
{
	typeset prog

	# Check to see if programs exist
	for prog in $MC_INFO_CLUSTER $DATE $PING $UNIT_SVC $SORT
	do
		if [[ ! -x $prog ]]
		then
			print_info ERROR verify_tools "Can't find $prog"
			return 1
		fi
	done

	return 0
}


##############################################################################
# cleanup when exiting
#
function cleanup
{
	typeset pid

	stop_all_services
	
	# Kill all child processes
	for pid in $(jobs -p)
	do
		print $pid
		kill -INT -$pid 2>/dev/null
	done
	wait

	return 0
}


##############################################################################
# Print usage of this script
#
function usage
{
	cat >&2 << EOF

Usage:
	$PROGNAME -?
	$PROGNAME -f <file> -c <cluster> 

Options:
	-f	- file with command options
	-c	- cluster

EOF
}


##############################################################################
# Examine command line
#
function examine_command_line
{
	typeset opt

	while getopts ':t:c:f:?' opt
	do
		case $opt in
		c)
			CLUSTER_NAME=$OPTARG
			;;
		f)
			INPUT_FILE=$OPTARG
			;;
		esac
	done

	# Make sure file exists
	if [[ ! -e "$INPUT_FILE" ]]
	then
		print_info ERROR examine_command_line \
			"$INPUT_FILE: not found"
		return 1
	fi

	# Check for valid cluster
	if [[ -z "$CLUSTER_NAME" ]]
	then
		print_info ERROR examine_command_line \
			"You must specify a cluster (-c)"
		return 1
	fi

	return 0
}


##############################################################################
# Read input file
#
# Will read the file that was prepared for this unit_stress run, the file
# will specify the parameters to run under, such as service name, and
# dependencies, etc.
#
# Will set the following variables
#	SERVICE_MTB_END
#	SERVICE_MTB_START
#	SERVICE_NAME_PREFIX
#	SERVICE_MAX_COUNT
#	SERVICE_MIN_COUNT
#	DEPENDENCY_TREE_MAX_HEIGHT
#	DEPENDENCY_TREE_MAX_WIDTH
#	DEPENDENCY_TREE_MTB_START
#	DEPENDENCY_TREE_MTB_END
#	DEPENDENCY_TREE_MAX_COUNT
#	DEPENDENCY_TREE_MIN_COUNT
#	PROVIDER_MTB_REMOVE
#	PROVIDER_MTB_ADD
#	PROVIDER_MAX_COUNT
#
#	CLIENT_MAX_COUNT
#	CLIENT_MIN_COUNT
#	CLIENT_MTB_START
#	CLIENT_MTB_END
#	CLIENT_MAX_THREADS
#	CLIENT_MIN_THREADS
#
#	QUANTUM
#
function read_input_file
{
	typeset keyword value
	typeset line_count=0

	# Check to see if file exists
	if [[ ! -e "$INPUT_FILE" ]]
	then
		print_info ERROR read_input_file \
			"File $INPUT_FILE does not exist"

		return 1
	fi

	SERVICE_MTB_END=""
	SERVICE_MTB_START=""
	SERVICE_NAME_PREFIX=""
	SERVICE_MAX_COUNT=""
	SERVICE_MIN_COUNT=""

	DEPENDENCY_TREE_MAX_HEIGHT=""
	DEPENDENCY_TREE_MAX_WIDTH=""
	DEPENDENCY_TREE_MTB_START=""
	DEPENDENCY_TREE_MTB_END=""
	DEPENDENCY_TREE_MAX_COUNT=""
	DEPENDENCY_TREE_MIN_COUNT=""

	PROVIDER_MTB_REMOVE=""
	PROVIDER_MTB_ADD=""
	PROVIDER_MAX_COUNT=""

	CLIENT_MAX_COUNT=""
	CLIENT_MIN_COUNT=""
	CLIENT_MTB_START=""
	CLIENT_MTB_END=""
	CLIENT_MAX_THREADS=""
	CLIENT_MIN_THREADS=""
		
	QUANTUM=60			# in secs

	# Fill in values for services
	while read keyword value
	do
		(( line_count += 1 ))
	
		# Ignore blank lines and comment lines
		[[ -z "$keyword" || "$keyword" = \#* ]] && continue

		case $keyword in
			SERVICE_MTB_END)
				SERVICE_MTB_END=$value
				;;
			SERVICE_MTB_START)
				SERVICE_MTB_START=$value
				;;
			SERVICE_NAME_PREFIX)
				SERVICE_NAME_PREFIX=$value
				;;
			SERVICE_MAX_COUNT)
				SERVICE_MAX_COUNT=$value
				;;
			SERVICE_MIN_COUNT)
				SERVICE_MIN_COUNT=$value
				;;
			DEPENDENCY_TREE_MAX_HEIGHT)
				DEPENDENCY_TREE_MAX_HEIGHT=$value
				;;
			DEPENDENCY_TREE_MAX_WIDTH)
				DEPENDENCY_TREE_MAX_WIDTH=$value
				;;
			DEPENDENCY_TREE_MTB_START)
				DEPENDENCY_TREE_MTB_START=$value
				;;
			DEPENDENCY_TREE_MTB_END)
				DEPENDENCY_TREE_MTB_END=$value
				;;
			DEPENDENCY_TREE_MAX_COUNT)
				DEPENDENCY_TREE_MAX_COUNT=$value
				;;
			DEPENDENCY_TREE_MIN_COUNT)
				DEPENDENCY_TREE_MIN_COUNT=$value
				;;
			PROVIDER_MTB_REMOVE)
				PROVIDER_MTB_REMOVE=$value
				;;
			PROVIDER_MTB_ADD)
				PROVIDER_MTB_ADD=$value
				;;
			PROVIDER_MAX_COUNT)
				PROVIDER_MAX_COUNT=$value
				;;
			CLIENT_MAX_COUNT)
				CLIENT_MAX_COUNT=$value
				;;
			CLIENT_MIN_COUNT)
				CLIENT_MIN_COUNT=$value
				;;
			CLIENT_MTB_START)
				CLIENT_MTB_START=$value
				;;
			CLIENT_MTB_END)
				CLIENT_MTB_END=$value
				;;
			CLIENT_MAX_THREADS)
				CLIENT_MAX_THREADS=$value
				;;
			CLIENT_MIN_THREADS)
				CLIENT_MIN_THREADS=$value
				;;
			QUANTUM)
				QUANTUM=$value
				;;
		esac
	done < $INPUT_FILE

	# Check to see if value was set
	if [[ -z "$SERVICE_MTB_END" ]]
	then
		print_info ERROR read_input_file "Missing SERVICE_MTB_END"
		return 1
	fi

	# Check to see if value was set
	if [[ -z "$SERVICE_NAME_PREFIX" ]]
	then
		print_info ERROR read_input_file " Missing SERVICE_NAME_PREFIX"
		return 1
	fi

	# Check to see if value was set
	if [[ -z "$SERVICE_MTB_START" ]]
	then
		print_info ERROR read_input_file "Missing SERVICE_MTB_START"
		return 1
	fi

	# Check to see if value was set
	if [[ -z "$SERVICE_MAX_COUNT" ]]
	then
		print_info ERROR read_input_file "Missing SERVICE_MAX_COUNT"
		return 1
	fi

	# Check to see if value was set
	if [[ -z "$SERVICE_MIN_COUNT" ]]
	then
		print_info ERROR read_input_file "Missing SERVICE_MIN_COUNT"
		return 1
	fi

	# Check to see if value was set
	if [[ -z "$DEPENDENCY_TREE_MAX_HEIGHT" ]]
	then
		print_info ERROR read_input_file \
			"Missing DEPENDENCY_TREE_MAX_HEIGHT"
		return 1
	fi

	# Check to see if value was set
	if [[ -z "$DEPENDENCY_TREE_MAX_WIDTH" ]]
	then
		print_info ERROR read_input_file \
			"Missing DEPENDENCY_TREE_MAX_WIDTH"
		return 1
	fi

	# Check to see if value was set
	if [[ -z "$DEPENDENCY_TREE_MTB_START" ]]
	then
		print_info ERROR read_input_file \
			"Missing DEPENDENCY_TREE_MTB_START"
		return 1
	fi

	# Check to see if value was set
	if [[ -z "$DEPENDENCY_TREE_MTB_END" ]]
	then
		print_info ERROR read_input_file \
			"Missing DEPENDENCY_TREE_MTB_END"
		return 1
	fi

	# Check to see if value was set
	if [[ -z "$DEPENDENCY_TREE_MAX_COUNT" ]]
	then
		print_info ERROR read_input_file \
			"Missing DEPENDENCY_TREE_MAX_COUNT"
		return 1
	fi

	# Check to see if value was set
	if [[ -z "$DEPENDENCY_TREE_MIN_COUNT" ]]
	then
		print_info ERROR read_input_file \
			"Missing DEPENDENCY_TREE_MIN_COUNT"
		return 1
	fi

	# Check to see if value was set
	if [[ -z "$PROVIDER_MTB_REMOVE" ]]
	then
		print_info ERROR read_input_file "Missing PROVIDER_MTB_REMOVE"
		return 1
	fi

	# Check to see if value was set
	if [[ -z "$PROVIDER_MTB_ADD" ]]
	then
		print_info ERROR read_input_file "Missing PROVIDER_MTB_ADD"
		return 1
	fi

	# Check to see if value was set
	if [[ -z "$PROVIDER_MAX_COUNT" ]]
	then
		print_info ERROR read_input_file "Missing PROVIDER_MAX_COUNT"
		return 1
	fi

	# Check to see if value was set
	if [[ -z "$CLIENT_MAX_COUNT" ]]
	then
		print_info ERROR read_input_file "Missing CLIENT_MAX_COUNT"
		return 1
	fi

	# Check to see if value was set
	if [[ -z "$CLIENT_MIN_COUNT" ]]
	then
		print_info ERROR read_input_file "Missing CLIENT_MIN_COUNT"
		return 1
	fi

	# Check to see if value was set
	if [[ -z "$CLIENT_MTB_START" ]]
	then
		print_info ERROR read_input_file "Missing CLIENT_MTB_START"
		return 1
	fi

	# Check to see if value was set
	if [[ -z "$CLIENT_MTB_END" ]]
	then
		print_info ERROR read_input_file "Missing CLIENT_MTB_END"
		return 1
	fi

	# Check to see if value was set
	if [[ -z "$CLIENT_MAX_THREADS" ]]
	then
		print_info ERROR read_input_file "Missing CLIENT_MAX_THREADS"
		return 1
	fi

	# Check to see if value was set
	if [[ -z "$CLIENT_MIN_THREADS" ]]
	then
		print_info ERROR read_input_file "Missing CLIENT_MIN_THREADS"
		return 1
	fi

	return 0
}


##############################################################################
# shutdown service
#
# Arguments:
#	service pid
#	service name
function shutdown_service
{
	typeset service_pid=$1
	typeset service_name=$2

	# Kill individual service 
	kill -INT -$service_pid > /dev/null 2>&1
	if [ $? -eq 0 ]
	then
		wait $service_pid
	fi

	return 0
}


##############################################################################
# shutdown dep_tree
#
# Arguments:
#	service pid
function shutdown_dep_tree
{
	typeset dep_tree_pid=$1

	# Kill service tree
	kill -INT -$dep_tree_pid > /dev/null 2>&1
	if [ $? -eq 0 ]
	then
		wait $dep_tree_pid
	fi

	return 0
}


##############################################################################
# start_service
#
# Will start a service unit, and update the service_pid_list
function start_service
{
	typeset service_name
	typeset pid
	typeset output_file

	service_name="$SERVICE_NAME_PREFIX.$$.$SVC_COUNTER"
	tmp_file=$TMP_DIR/$service_name.in

	# Clear it first
	rm -f $tmp_file

	# Output values into file to be used by service
	print "QUANTUM $QUANTUM" >> $tmp_file
	print "SERVICE_NAME $service_name" >> $tmp_file
	print "PROVIDER_MTB_REMOVE $PROVIDER_MTB_REMOVE" >> $tmp_file
	print "PROVIDER_MTB_ADD $PROVIDER_MTB_ADD" >> $tmp_file
	print "PROVIDER_MAX_COUNT $PROVIDER_MAX_COUNT" >> $tmp_file

	print "CLIENT_MAX_COUNT $CLIENT_MAX_COUNT" >> $tmp_file
	print "CLIENT_MIN_COUNT $CLIENT_MIN_COUNT" >> $tmp_file
	print "CLIENT_MTB_START $CLIENT_MTB_START" >> $tmp_file
	print "CLIENT_MTB_END $CLIENT_MTB_END" >> $tmp_file
	print "CLIENT_MAX_THREADS $CLIENT_MAX_THREADS" >> $tmp_file
	print "CLIENT_MIN_THREADS $CLIENT_MIN_THREADS" >> $tmp_file

	# come up with a unique name for its output file
	output_file="$TMP_DIR/$service_name.out"

	# Run non services with parameters	
	$UNIT_SVC -c $CLUSTER_NAME -f $tmp_file -t $TMP_DIR 1>$output_file 2>&1 &
	pid=$!

	print_info $PR_SVC_START $service_name "pid: $pid (started unit_svc)"

	SERVICE_PID_LIST[$SERVICE_LIST_INDEX]=$pid
	SERVICE_NAME_LIST[$SERVICE_LIST_INDEX]=$service_name
	(( SERVICE_LIST_INDEX += 1 ))

	# Total number of services
	(( SVC_COUNTER += 1 ))

	return 0
}


##############################################################################
# start_dep_tree
#
# Will start a dep_tree unit, and update the dep_tree_pid_list
function start_dep_tree
{
	typeset tmp_file
	typeset service_name
	typeset pid
	typeset output_file

	tmp_file=$TMP_DIR/unit_dep_svc.$DEP_TREE_COUNTER.in

	# Clear it first
	rm -f $tmp_file

	# Output values to be used by service
	print "QUANTUM $QUANTUM" >> $tmp_file
	print "SERVICE_NAME_PREFIX $SERVICE_NAME_PREFIX" >> $tmp_file
	print "DEPENDENCY_TREE_MAX_HEIGHT $DEPENDENCY_TREE_MAX_HEIGHT" >> $tmp_file
	print "DEPENDENCY_TREE_MAX_WIDTH $DEPENDENCY_TREE_MAX_WIDTH" >> $tmp_file
	print "PROVIDER_MTB_REMOVE $PROVIDER_MTB_REMOVE" >> $tmp_file
	print "PROVIDER_MTB_ADD $PROVIDER_MTB_ADD" >> $tmp_file
	print "PROVIDER_MAX_COUNT $PROVIDER_MAX_COUNT" >> $tmp_file

	print "CLIENT_MAX_COUNT $CLIENT_MAX_COUNT" >> $tmp_file
	print "CLIENT_MIN_COUNT $CLIENT_MIN_COUNT" >> $tmp_file
	print "CLIENT_MTB_START $CLIENT_MTB_START" >> $tmp_file
	print "CLIENT_MTB_END $CLIENT_MTB_END" >> $tmp_file
	print "CLIENT_MAX_THREADS $CLIENT_MAX_THREADS" >> $tmp_file
	print "CLIENT_MIN_THREADS $CLIENT_MIN_THREADS" >> $tmp_file

	# come up with a unique name for its output file
	output_file="$TMP_DIR/unit_dep-svc.$DEP_TREE_COUNTER.out"

	# Run depended services with parameters
	$UNIT_DEP_SVC -c $CLUSTER_NAME -f $tmp_file -t $TMP_DIR 1>$output_file 2>&1 &
	pid=$!

	print_info $PR_DEP_TREE_START none \
		"counter: $DEP_TREE_COUNTER (started unit_dep_svc)"

	DEP_TREE_PID_LIST[$DEP_TREE_LIST_INDEX]=$pid
	(( DEP_TREE_LIST_INDEX += 1 ))
	(( DEP_TREE_COUNTER += 1 ))

	return 0
}


##############################################################################
# do_work_service_start
#
# Will figure out if a service needs to be started (due to mtb start)
function do_work_service_start
{
	typeset num

	# Calculation lifted from solaris (and now cmm code)
	# This algorithm was lifeted from the work by Jeff Bonwick in the
	# kmem allocated from usr/src/uts/common/os/kmem.c
	(( SERVICE_START_RANDOM *= 2416 ))
	(( SERVICE_START_RANDOM += 374441 ))
	(( SERVICE_START_RANDOM %= 1771875 ))

	(( num = 1771875 / $SERVICE_MTB_START ))

	# If there are too many services, then don't run any more (duh!)
	if [[ $SERVICE_LIST_INDEX -ge $SERVICE_MAX_COUNT ]]
	then
		return 0
	fi

	# Otherwise Just Do it (TM)!
	if [[ $SERVICE_START_RANDOM -lt $num ]]
	then
		start_service
	fi
}


##############################################################################
# do_work_dep_tree_start
#
# Will figure out if a dependency tree needs to be started (due to mtb start)
function do_work_dep_tree_start
{
	typeset num

	# Calculation lifted from solaris (and now cmm code)
	# This algorithm was lifeted from the work by Jeff Bonwick in the
	# kmem allocated from usr/src/uts/common/os/kmem.c
	(( DEP_TREE_START_RANDOM *= 2416 ))
	(( DEP_TREE_START_RANDOM += 374441 ))
	(( DEP_TREE_START_RANDOM %= 1771875 ))

	(( num = 1771875 / $DEPENDENCY_TREE_MTB_START ))

	# If there are too many services, then don't run any more (duh!)
	if [[ $DEP_TREE_LIST_INDEX -ge $DEPENDENCY_TREE_MAX_COUNT ]]
	then
		return 0
	fi

	# Otherwise Just Do it (TM)!
	if [[ $DEP_TREE_START_RANDOM -lt $num ]]
	then
		start_dep_tree
	fi
}


##############################################################################
# do_work_service_shutdown
#
# Will figure out if a service needs to be shutdown (due to mtb shutdown)
function do_work_service_shutdown
{
	typeset num
	typeset index
	typeset pid
	typeset service_name

	# Calculation lifted from solaris (and now cmm code)
	# This algorithm was lifeted from the work by Jeff Bonwick in the
	# kmem allocated from usr/src/uts/common/os/kmem.c
	(( SERVICE_END_RANDOM *= 2416 ))
	(( SERVICE_END_RANDOM += 374441 ))
	(( SERVICE_END_RANDOM %= 1771875 ))

	(( num = 1771875 / $SERVICE_MTB_END ))

	# If no more services to kill, the don't
	if [[ $SERVICE_LIST_INDEX -eq 0 ]]
	then
		return 0
	fi

	# If less services then minimum services to run, then don't kill
	if [[ $SERVICE_LIST_INDEX -lt $SERVICE_MIN_COUNT ]]
	then
		return 0
	fi

	# Otherwise kill one
	if [[ $SERVICE_END_RANDOM -lt $num ]]
	then
		index=$(random_number 1 $SERVICE_LIST_INDEX)
		(( index -= 1 ))
		pid=${SERVICE_PID_LIST[$index]}
		service_name=${SERVICE_NAME_LIST[$index]}

		shutdown_service $pid $service_name
		if [[ $? -eq 0 ]]	
		then
			# Remove that entry from the pid list
			SERVICE_PID_LIST[$index]=
			SERVICE_NAME_LIST[$index]=
			set -A SERVICE_PID_LIST $(echo ${SERVICE_PID_LIST[*]})
			set -A SERVICE_NAME_LIST $(echo ${SERVICE_NAME_LIST[*]})
			SERVICE_LIST_INDEX=${#SERVICE_PID_LIST[*]}
		fi
	fi

	return 0
}


##############################################################################
# do_work_dep_tree_shutdown
#
# Will figure out if a dep_tree needs to be shutdown (due to mtb shutdown)
function do_work_dep_tree_shutdown
{
	typeset num
	typeset index
	typeset pid

	# Calculation lifted from solaris (and now cmm code)
	# This algorithm was lifeted from the work by Jeff Bonwick in the
	# kmem allocated from usr/src/uts/common/os/kmem.c
	(( DEP_TREE_END_RANDOM *= 2416 ))
	(( DEP_TREE_END_RANDOM += 374441 ))
	(( DEP_TREE_END_RANDOM %= 1771875 ))

	(( num = 1771875 / $DEPENDENCY_TREE_MTB_END ))

	# If no services to kill, then don't
	if [[ $DEP_TREE_LIST_INDEX -eq 0 ]]
	then
		return 0
	fi

	# If less then minimum services, then don't kill any
	if [[ $DEP_TREE_LIST_INDEX -lt $DEPENDENCY_TREE_MIN_COUNT ]]
	then
		return 0
	fi

	# Otherwise find on to kill
	if [[ $DEP_TREE_END_RANDOM -lt $num ]]
	then
		index=$(random_number 1 $DEP_TREE_LIST_INDEX)
		(( index -= 1 ))
		pid=${DEP_TREE_PID_LIST[$index]}

		shutdown_dep_tree $pid
		if [[ $? -eq 0 ]]
		then
			# Remove that entry from the pid list
			DEP_TREE_PID_LIST[$index]=
			set -A DEP_TREE_PID_LIST $(echo ${DEP_TREE_PID_LIST[*]})
			DEP_TREE_LIST_INDEX=${#DEP_TREE_PID_LIST[*]}
		fi
	fi

	return 0
}


##############################################################################
# stop_all_services
#
function stop_all_services
{
	typeset pid
	typeset service_name
	typeset index=0

	print_info $PR_INFO $PR_OTHER none "Stopping ALL services"

	# Kill all services
	while [[ $index -lt $SERVICE_LIST_INDEX ]]
	do
		pid=${SERVICE_PID_LIST[$index]}
		service_name=${SERVICE_NAME_LIST[$index]}

		shutdown_service $pid $service_name

		(( index += 1 ))
	done

	print_info INFO stop_all_services "Completed stopping dependency trees"

	return 0
}


##############################################################################
# stop_all_dep_tree
#
function stop_all_dep_tree
{
	typeset pid
	typeset index=0

	print_info $PR_INFO $PR_OTHER none "Stopping ALL dependency trees"

	# Kill all dependent services
	while [[ $index -lt $DEP_TREE_LIST_INDEX ]]
	do
		pid=${DEP_TREE_PID_LIST[$index]}

		shutdown_dep_tree $pid

		(( index += 1 ))
	done

	print_info INFO stop_all_dep_tree "Completed stopping dependency trees"

	return 0
}

##############################################################################
# Do stress sequence
#
# This function will run until interrupted, sleeping for the quantum time
# and at each quantum it will call functions to figure out if a provider
# needs to be started or killed, etc
# 
function do_stress_sequence
{
	typeset sleep_time
	typeset start_time
	typeset end_time
	typeset diff_time

	# Seed the start time counter
	start_time=$(get_time)

	# Loop till Done == 0
	while [[ $DONE -eq 0 ]]	
	do
		# Get the end time
		end_time=$(get_time)

		# Calculate the time spent executing
		(( diff_time = end_time - start_time ))
		(( sleep_time = QUANTUM - diff_time ))
		if [[ $sleep_time -gt 0 ]]
		then
			sleep $sleep_time
		fi

		# Get the start time
		start_time=$(get_time)

		# Do Work
		do_work_service_start
		do_work_service_shutdown
		do_work_dep_tree_start
		do_work_dep_tree_shutdown
	done

	return 0
}

##############################################################################
# Main Execution
#
function main
{
	typeset index
	typeset	node

	# Check that tools exists
	if ! verify_tools
	then
		return 1
	fi

	# Examine command line
	if ! examine_command_line "$@"
	then
		return 1
	fi

	# Init cluster structure
	if ! init_cluster_structure
	then
		return 1
	fi

	if ! mkdir -p $TMP_DIR
	then	
		return 1
	fi
	# Read the input file
	if ! read_input_file
	then
		return 1
	fi

	# Execute Stress Sequences
	if ! do_stress_sequence
	then
		return 1
	fi

	return 0
}


##############################################################################
# Call main
#
trap 'cleanup' EXIT
main "$@"
