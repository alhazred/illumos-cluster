#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)ha_stress_aux.ksh	1.9	08/05/20 SMI"
#

##############################################################################
# GLOBAL VALUES
#
readonly MC_INFO_MASTER=cluster_info
readonly MC_INFO_CLUSTER=/usr/cluster/orbtest/fi_support/cluster_info
readonly DATE=/bin/date

# Type of log msg
PR_INFO="INFO"
PR_WARNING="WARNING"
PR_ERROR="ERROR"

# Type of action
PR_SWITCHOVER="switchover"
PR_SVC_END="svc_shutdown"
PR_SVC_START="svc_start"
PR_DEP_TREE_START="dep_tree_start"
PR_DEP_TREE_END="dep_tree_end"
PR_DEP_SVC_START="dep_svc_start"
PR_DEP_SVC_END="dep_svc_end"
PR_PROV_REMOVE="prov_remove"
PR_PROV_ADD="prov_add"
PR_CLIENT_START="client_start"
PR_CLIENT_END="client_end"
PR_CLIENT="client_thread"
PR_CMM_RECONF="cmm_reconf"
PR_OTHER="other"


##############################################################################
# print_info
#
# Prints out messages
#
# Arguments
#	Type of msg
#	Function name
#
function print_info
{
	typeset t_msg=$1
	typeset service_name=$2
	shift 2
	typeset date_info=$(date +%Y-%m-%d@%H:%M:%S)

	if [[ $t_msg == "ERROR" ]]
	then
		print -u2 "$date_info | $$ | $t_msg | $THIS_PROG | $service_name | $@"
	else
		
		print "$date_info | $$ | $t_msg | $THIS_PROG | $service_name | $@"
	fi
	
	return 0
}

##############################################################################
# get_time
#
# Will print the number of seconds since we started executing
#
function get_time
{
	typeset hours
	typeset minutes
	typeset seconds
	typeset diff_hours
	typeset diff_minutes
	typeset diff_seconds

	#get the current hour, minute, and second
	hours=$($DATE +%H)
	minutes=$($DATE +%M)
	seconds=$($DATE +%S)

	# Convert minutes to seconds
	if [[ $seconds -lt $START_SECONDS ]]
	then
		(( seconds += 60 ))
		(( minutes -= 1 ))
	fi

	# Convert hours to minutes
	if [[ $minutes -lt $START_MINUTES ]]
	then
		(( minutes += 60 ))
		(( hours -= 1 ))
	fi

	# Convert days to hours
	if [[ $hours -lt $START_HOURS ]]
	then
		(( hours += 24 ))
	fi

	# Calculate the time difference
	(( diff_hours = hours - START_HOURS ))
	(( diff_minutes = minutes - START_MINUTES ))
	(( diff_seconds = seconds - START_SECONDS ))

	# Convert to seconds
	(( diff_hours *= 3600 ))
	(( diff_minutes *= 60 ))

	(( diff_seconds += diff_minutes ))
	(( diff_seconds += diff_hours ))

	print "$diff_seconds"
}


##############################################################################
# random_node
#
# Returns a random node of the cluster
#
function random_node
{
	typeset a_node
	typeset num

	# Random node number
	num=$RANDOM
	(( a_node = num % NUM_NODES ))

	print $a_node
}


##############################################################################
# random_number
#
# Returns a random number between the two numbers given
#
function random_number
{
	typeset low=$1
	typeset high=$2
	typeset diff
	typeset num
	typeset rslt

	(( diff = high - low ))
	if [ $diff -lt 1 ]
	then
		print $low
		return 1
	fi

	# Add 1 to diff to make it inclusive on both ends
	(( diff += 1 ))

	num=$RANDOM
	(( rslt = num % diff ))

	(( rslt += low ))
	print $rslt
}


##############################################################################
# init_cluster_structure
#
# get the name of nodes and their id's + set the value of NUM_NODES
#
function init_cluster_structure
{
	typeset nodeid

	unset NODENAME_LIST
	unset NODEID_LIST

	mc_info_path=`type $MC_INFO_MASTER 2>/dev/null | nawk ' { print $3 }'`
	if [ -z "$mc_info_path" ]
	then
		MC_INFO=$MC_INFO_CLUSTER
	else
		if [ -x "$mc_info_path" ]
		then
			MC_INFO=$MC_INFO_MASTER
		fi
	fi
	# Get node names
	if ! set -A NODENAME_LIST `$MC_INFO -n $CLUSTER_NAME`
	then
		print_info ERROR init_cluster_structure \
		  "$MC_INFO failed to get node names"
		return 1
	fi

	# get node id
	if ! set -A NODEID_LIST `$MC_INFO -e $CLUSTER_NAME`
	then
		print_info ERROR init_cluster_structure \
		  "$MC_INFO failed to get node names"
		return 1
	fi

	# Total number of nodes
	NUM_NODES=${#NODENAME_LIST[*]}

	return 0
}

##############################################################################
# find_working_node
#
# will print the index of a random that is up and running
#
function find_working_node
{
	typeset index
	typeset start_index
	typeset rslt=0

	index=$(random_node)
	start_index=$index

	# Loop until done
	while true
	do
		# Check to see if choosen node is up
		node=${NODENAME_LIST[$index]}
		$PING $node 3 1>/dev/null 2>/dev/null

		# If ping return is 0 (alive) then exit loop
		if [[ $? -eq 0 ]]
		then
			break

		# Otherwise keep looking
		else
			(( index += 1))
			if [[ $index -ge $NUM_NODES ]]
			then
				index=0
			fi

			# See if we've wrapped
			if [[ $index -eq $start_index ]]
			then
				rslt=1
				break
			fi
		fi
	done

	print $index

	return $rslt
}
