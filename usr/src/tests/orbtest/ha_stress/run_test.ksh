#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)run_test.ksh	1.19	08/05/20 SMI"
#

#
#
# Run test
#
# Will run the HA Stress test suite on the specified cluster
#

readonly PROGNAME=$0		# Program name
readonly THIS_PROG=${0##*/}

PROGDIR=$(dirname $PROGNAME)	# Directory of program
if [[ "$PROGDIR" = "." ]]
then
	PROGDIR=$PWD
fi
readonly PROGDIR

##############################################################################
# Files that need to be sourced
#
# Source the aux file
. $PROGDIR/ha_stress_aux

##############################################################################
# Global Settings
#
set -o monitor                  # so bg jobs can run with their own PGID and
                                # we can trap SIGCHLD
#
# Default Values
#
OUTPUT_BASEDIR=/tmp			# Where results will be kept
OUTPUT_DIR=$OUTPUT_BASEDIR/${LOGNAME}_$$_ha_stress
OUTPUT_FILE=$OUTPUT_DIR/hastress.log
SUMMARY_FILE=$OUTPUT_DIR/summary
INPUT_FILE="/usr/cluster/orbtest/ha_stress/stress.data" # Data file

#
# General Tools
#
readonly RSH='rsh -n -l root'

#
# Kernel Module tools
#
readonly KSERVER_LOAD=/usr/cluster/orbtest/fi_support/kernel_server_load
readonly KSERVER_UNLOAD=/usr/cluster/orbtest/fi_support/kernel_server_unload

#
# Path to search kernel modules for
#
readonly DEF_KPATH=/usr/cluster/orbtest/test_kproxy/kernel:/kernel/misc

#
# Kernel Proxy and Persist_store load and entry
#
readonly KPROXY_LOAD=kproxy
readonly KPROXY_ENTRY=kproxy
readonly PERSIST_STORE_LOAD=persist_store
readonly PERSIST_STORE_ENTRY=persist_store

#
# Parameters for "at".
#
readonly AT_QUEUE=w
readonly AT_SIGNAL=USR2
AT_JOBID=
AT_TIME="now + 5 hours"


##############################################################################
# verify_tools
#
# Makes sure the tools exist
#
function verify_tools
{
	typeset prog
	MC_INFO_FULL_PATH=`type $MC_INFO_MASTER | nawk ' { print $3 }'`
	if [ -z "$MC_INFO_FULL_PATH" ]
	then
		print_info ERROR verify_tools "Can't set full path of $MC_INFO_MASTER"
		return 1
	fi
	for prog in $MC_INFO_FULL_PATH
	do
		# If programs do not exist, exit from program
		if [[ ! -x $prog ]]
		then
			print_info ERROR verify_tools "Can't find $prog"
			return 1
		fi
	done

	return 0
}

##############################################################################
# Print Banner
#
# Prints the Info Banner
#
function print_banner
{
	cat >&2 << EOF
=======================================================================
=== HA Stress Test Suite
===
=== Output will be saved at: $OUTPUT_DIR
===
=======================================================================

EOF
}
	
##############################################################################
# cleanup 
#
# Unloads the kernel modules from root node
#
function cleanup
{

	typeset pid

	if [[ -n "$AT_JOBID" ]]
	then
		at -r $AT_JOBID 2>/dev/null
	fi

	print "Killing all background processes, this will take awhile"
	# Terminate all background processes.
	for pid in $(jobs -p)
	do
		# Interrupt the entire process group (use negative pid).
		kill -INT -$pid 2>/dev/null
	done
	wait

	# Unload kernel modules persist_store and kproxy
	$RSH $FIRST_NODE "$KSERVER_UNLOAD $PERSIST_STORE_LOAD \
		$PERSIST_STORE_ENTRY $KERNEL_ARGS" 
	$RSH $FIRST_NODE "$KSERVER_UNLOAD $KPROXY_LOAD \
		$KPROXY_ENTRY $KERNEL_ARGS"

	if ! summary
	then
		return 1
	fi

	return 0
}


##############################################################################
# at_setup
#
# Sets "AT" job to kill all tests
#
function at_setup
{

	typeset output

        # If no "at" time specified, return.
        [ -z $AT_TIME ] && return 0

	# Set an "at" job that sends an AT_SIGNAL to all bg programs
        output=$(at -k -q$AT_QUEUE $AT_TIME 2>&1 <<- EOF
                        kill -$AT_SIGNAL $$ 2>/dev/null
	EOF)
        if (( $? != 0 ))
        then
                print_info ERROR at_setup "Can't set at job: $output"
                return 1
        else
                # Save the at job's id.
                AT_JOBID=$(echo "$output" | awk '/^job/ {print $2}')
        fi

        return 0

}
##############################################################################
# usage
#
# Prints the usage of script
#
function usage
{
	cat >&2 << EOF

Usage:
	$PROGNAME -?
	$PROGNAME -c <cluster> -f <file> -t <"at" format> [-o dir]

Options:
	-c	- Name of cluster
	-f	- Parameter file [$INPUT_FILE]
	-t	- Time limit for tests [$AT_TIME]

Testing Options:

Misc. Options:
	-o	- Output directory

EOF
}


##############################################################################
# examine_command_line
#
# Checks to see if the command line arguments are correct
#
function examine_command_line
{
	typeset opt

	#check for options
	while getopts ':f:c:o:t:' opt
	do
		case $opt in
		o)
			OUTPUT_BASEDIR=$OPTARG
			OUTPUT_DIR=$OUTPUT_BASEDIR/${LOGNAME}_$$_ha_stress
			OUTPUT_FILE=$OUTPUT_DIR/hastress.log
			SUMMARY_FILE=$OUTPUT_DIR/summary
			;;
		c)
			CLUSTER_NAME=$OPTARG
			;;
		f)
			INPUT_FILE=$OPTARG
			;;
		t)
			AT_TIME=$OPTARG
			;;
		*)
			usage
			return 1
			;;
		esac
	done

	# Check non-optional variables
	if [[ -z "$CLUSTER_NAME" ]]
	then
		print_info ERROR examine_command_line \
			"You must specify a cluster (-c)"
		usage
		return 1
	fi
}

##############################################################################
# summary
#
# Summarize the outputs
#
function summary
{
	cmm_reconf='grep cmm_reconf'
	warnings='grep WARNING'
	repl_name='grep repl_name_server'
	switchover='grep switchover_provider'
	counter='grep counter'
	count='wc -l'

	print "Number of Cluster Membership Monitor (CMM) reconfigurations: `$cmm_reconf $OUTPUT_FILE | $count`" >> $SUMMARY_FILE
	print "Number of provider switchovers: `$switchover $OUTPUT_FILE | $count`" >> $SUMMARY_FILE
	print "Number of dependent services started: `$repl_name $OUTPUT_FILE | $count`" >> $SUMMARY_FILE

	$warnings $OUTPUT_FILE >> $SUMMARY_FILE

	return 0
}

##############################################################################
# run_all_tests
#
# Runs all tests
#
function run_all_tests
{
	$RSH $FIRST_NODE /usr/cluster/orbtest/ha_stress/unit_switchover -c $CLUSTER_NAME -f $INPUT_FILE >> $OUTPUT_FILE &
       	$RSH $FIRST_NODE /usr/cluster/orbtest/ha_stress/unit_cmm_reconf -c $CLUSTER_NAME -f $INPUT_FILE >> $OUTPUT_FILE &
       	$RSH $FIRST_NODE /usr/cluster/orbtest/ha_stress/unit_start_end_svc -c $CLUSTER_NAME -f $INPUT_FILE -t /tmp  >> $OUTPUT_FILE &


	return 0
}


##############################################################################
# load_kernel_modules 
#
# Loads the kernel modules persist_store and kproxy on the root node
#
function load_kernel_modules
{
	# A node of cluster
	FIRST_NODE=${NODENAME_LIST[0]}

	# Load Kernel modules
	$RSH $FIRST_NODE $KSERVER_LOAD $DEF_KPATH $PERSIST_STORE_LOAD \
		$PERSIST_STORE_ENTRY

	$RSH $FIRST_NODE $KSERVER_LOAD $DEF_KPATH $KPROXY_LOAD $KPROXY_ENTRY
}


##############################################################################
# Main
#
function main
{
	# Check that tools exists
	if ! verify_tools
	then
		return 1
	fi

	# Examine command line
	if ! examine_command_line "$@"
	then
		return 1
	fi

	# Init cluster structure
	if ! init_cluster_structure
	then
		return 1
	fi

	# Print Banner info
	print_banner

	# Make directory for output
	if ! mkdir -p $OUTPUT_DIR
	then
		return 1
	fi

	# Load kernel module
	if ! load_kernel_modules
	then
		return 1
	fi

	# set up "at" job
	if ! at_setup
	then
		return 1
	fi

	# Run the tests
	if ! run_all_tests
	then
		return 1
	fi

	# Wait till all tests have finised running
	wait

	return 0
}


##############################################################################
# Script Execution
#
trap 'cleanup' EXIT
trap 'echo TERMINATED BY AT: $(date); exit' $AT_SIGNAL
main "$@"
