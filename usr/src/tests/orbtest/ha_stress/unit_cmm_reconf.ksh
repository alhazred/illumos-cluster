#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)unit_cmm_reconf.ksh	1.10	08/05/20 SMI"
#

#
#
# Node Stress
#
# A copy of this shell script will run on ever node on a cluster that
# is running the HA stress test suite.  This script will make sure that
# the node its running on has its share of the load running.
#

readonly PROGNAME=$0		# Program name
readonly THIS_PROG=${0##*/}

PROGDIR=$(dirname $PROGNAME)	# Directory of program
if [[ "$PROGDIR" = "." ]]
then
	PROGDIR=$PWD
fi
readonly PROGDIR

##############################################################################
# Files that need to be sourced
#
# Source the aux file
. $PROGDIR/ha_stress_aux

##############################################################################
# Global Settings
#
set -o monitor                  # so bg jobs can run with their own PGID and
                                # we can trap SIGCHLD

RANDOM=$$			# some seed
DONE=0
readonly LOGINNAME=$(logname)	# User's name

#
# RANDOM Globals
#
CMM_RECONF_RANDOM=$RANDOM	# Will be used in mtb shutdown

#
# Tool location
#
readonly RSH="rsh -n -l root"
readonly PING=/usr/sbin/ping
readonly REPLCTL=/usr/cluster/bin/replctl
readonly WRAPPER=/usr/cluster/orbtest/ha_stress/wrapper

#
# Time constants
#
readonly START_HOURS=$($DATE +%H)
readonly START_MINUTES=$($DATE +%M)
readonly START_SECONDS=$($DATE +%S)

#
# CMM CTL
#
readonly CMM_CTL=/usr/cluster/bin/cmm_ctl

##############################################################################
# cleanup
#
# cleanup when exiting
#
function cleanup
{
	typeset pid

	#kill all child processes
	for pid in $(jobs -p)
	do
		kill -INT -$pid 2>/dev/null
	done
	wait

	return 0
}

##############################################################################
# examine_command_line
#
function examine_command_line
{
	typeset opt

	while getopts ':c:f:' opt
	do
		case $opt in
		c)
			CLUSTER_NAME=$OPTARG
			;;
		f)
			INPUT_FILE=$OPTARG
			;;
		esac
	done


	 # Check non-optional variables
        if [[ -z "$CLUSTER_NAME" ]]
        then
                print_info ERROR examine_command_line \
                        "You must specify a cluster (-c)"
                return 1
        fi

	# if file doesn't exist then exit
	if [[ ! -e "$INPUT_FILE" ]]
	then
		print_info ERROR examine_command_line "$INPUT_FILE not found"
		return 1
	fi

	return 0
}

##############################################################################
# read_input_file
#
# Will read the file that was prepared for this unit_stress run, the file
# will specify the parameters to run under, such as service name, and
# dependencies, etc.
#
# Will set the following variables
#	MTB_CMM_RECONF
#	QUANTUM
#
function read_input_file
{
	typeset keyword value
	typeset line_count=0

	# check to see if file exists
	if [[ ! -e "$INPUT_FILE" ]]
	then
		print_info ERROR read_input_file "$INPUT_FILE does not exist"
		return 1
	fi

	MTB_CMM_RECONF=""

	while read keyword value
	do
		(( line_count += 1 ))
	
		# Ignore blank lines and comment lines
		[[ -z "$keyword" || "$keyword" = \#* ]] && continue

		case $keyword in
			MTB_CMM_RECONF)
				MTB_CMM_RECONF=$value
				;;
			QUANTUM)
				QUANTUM=$value
				;;
		esac
	done < $INPUT_FILE

	# check to see if options were set
	if [[ -z "$MTB_CMM_RECONF" ]]
	then
		print_info ERROR read_input_file "Missing MTB_CMM_RECONF"
		return 1
	fi

	return 0
}


##############################################################################
# cmm_reconf
#
# Arguments:
#	node to run command on
#
function cmm_reconf
{
	typeset node=$1
	typeset rslt
	typeset rsh_rslt

	# check to see if node is up
	$PING $node 3 > /dev/null
	if [ $? -ne 0 ]
	then
		print_info WARNING cmm_reconf "$node is not responding"
		return -1
	fi

	print_info INFO cmm_reconf "$PR_CMM_RECONF on $node"
	
	# reconfig the cluster
	rsh_rslt=$($RSH $node "$WRAPPER -r /dev/null $CMM_CTL -r")
	rslt=$?

	if [[ $rslt -eq 0 ]] && [[ $rsh_rslt -ne 0 ]]
	then
		print_info WARNING cmm_reconf "failed on $node"
		rslt=$rsh_rslt
	fi

	return $rslt
}


##############################################################################
# do_work_cmm_reconf
#
# Will figure out if we need to do a cmm reconf
#
function do_work_cmm_reconf
{
	typeset num
	typeset node

	# Calculation lifted from solaris (and now cmm code)
	# This algorithm was lifeted from the work by Jeff Bonwick in the
	# kmem allocated from usr/src/uts/common/os/kmem.c
	(( CMM_RECONF_RANDOM *= 2416 ))
	(( CMM_RECONF_RANDOM += 374441 ))
	(( CMM_RECONF_RANDOM %= 1771875 ))

	(( num = 1771875 / $MTB_CMM_RECONF ))

	if [[ $CMM_RECONF_RANDOM -lt $num ]]
	then
		# Select a node to do this on
		index=$(find_working_node)
		if [[ $? -ne 0 ]]
		then
			print_info ERROR do_work_cmm_reconf \
				"all nodes are down ($CLUSTER_NAME)"
			return 1
		fi

		node=${NODENAME_LIST[$index]}

		# Do a cmm reconf on the node we found
		cmm_reconf $node
	fi

	return 0
}


##############################################################################
# do_stress_sequence
#
# This function will run until interrupted, sleeping for the quantum time
# and at each quantum it will call functions to figure out if a provider
# needs to be started or killed, etc
# 
function do_stress_sequence
{
	typeset sleep_time
	typeset start_time
	typeset end_time
	typeset diff_time

	# Seed the start time counter
	start_time=$(get_time)

	while [[ $DONE -eq 0 ]]
	do
		# Get the end time
		end_time=$(get_time)

		# Calculate the time spent executing
		(( diff_time = end_time - start_time ))
		(( sleep_time = QUANTUM - diff_time ))
		if [[ $sleep_time -gt 0 ]]
		then
			sleep $sleep_time
		fi

		# Get the start time
		start_time=$(get_time)

		# Do Work
		do_work_cmm_reconf
	done

	return 0
}

##############################################################################
# Main Execution
#
function main
{
	typeset index
	typeset	node

	# Examine command line
	if ! examine_command_line "$@"
	then
		return 1
	fi

	# Init cluster structure
	if ! init_cluster_structure
	then
		return 1
	fi

	# Read the input file
	if ! read_input_file
	then
		return 1
	fi

	# Execute Stress Sequences
	if ! do_stress_sequence
	then
		return 1
	fi

	return 0
}


##############################################################################
# Call main
#
trap 'cleanup' EXIT
main "$@"
