#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)unit_client.ksh	1.11	08/05/20 SMI"
#


readonly PROGNAME=$0		# Program name
readonly THIS_PROG=${0##*/}

PROGDIR=$(dirname $PROGNAME)	# Directory of program
if [[ "$PROGDIR" = "." ]]
then
	PROGDIR=$PWD
fi
readonly PROGDIR

##############################################################################
# Files that need to be sourced
#
# Source the aux file
. $PROGDIR/ha_stress_aux


##############################################################################
# Global Settings
#

set -o monitor			# so bg jobs can run with their own PGID and
				# we can trap SIGCHLD
set +o nounset			# allow use of unset variables
set +o notify			# dont notify of bg job completion


RANDOM=$$			# some seed
DONE=0				# Done is 1 when complete
readonly LOGINNAME=$(logname)	# User's name

#
# RANDOM Globals
#
CLIENT_START_RANDOM=$RANDOM		# Will be used in mtb start
CLIENT_END_RANDOM=$RANDOM		# Will be used in mtb shutdown

#
# State
#
CLIENT_COUNT=0				# actually counts the # of clients
					# currently running
CLIENT_INDEX=0				# Always increasing counter
unset CLIENT_NAME_LIST


#
# Tool location
#
readonly RSH="rsh -n -l root"
readonly PING=/usr/sbin/ping
readonly REPLCTL=/usr/cluster/bin/replctl
readonly WRAPPER=/usr/cluster/orbtest/ha_stress/wrapper

#
# Time constants
#
readonly START_HOURS=$($DATE +%H)
readonly START_MINUTES=$($DATE +%M)
readonly START_SECONDS=$($DATE +%S)

#
# Repl Sample
#
readonly REPL_SAMPLE_ENTRY=ha_sample_server

#
# KProxy
#
readonly KPROXY_CLIENT=/usr/cluster/orbtest/test_kproxy/kproxy_client
readonly CLIENT_START=/usr/cluster/orbtest/ha_stress/start_client
readonly CLIENT_STOP=/usr/cluster/orbtest/ha_stress/stop_client


##############################################################################
# verify_tools
#
# Check to see if the tools exist
#
function verify_tools
{
	typeset prog

	# Make sure the programs exist other wise exit program
	for prog in $DATE $PING $WRAPPER $REPLCTL
	do
		if [[ ! -x $prog ]]
		then
			print_info ERROR verify_tools "Can't find $prog"
			return 1
		fi
	done

	return 0
}


##############################################################################
# cleanup 
#
# kill all sub processes
#
function cleanup
{
	typeset pid
	
	end_all_clients

	# Kill all child processes
	for pid in $(jobs -p)
	do
		kill -INT -$pid 2>/dev/null
	done
	wait

	return 0
}

##############################################################################
# examine_command_line
#
# check the command line options
#
function examine_command_line
{
	typeset opt

	while getopts ':c:f:h?' opt
	do
		case $opt in
		f)
			INPUT_FILE=$OPTARG
			;;
		c)
			CLUSTER_NAME=$OPTARG
			;;
		esac
	done

	# check to see if file exists
	if [[ ! -e "$INPUT_FILE" ]]
	then
		print_info ERROR examine_command_line \
			"You must specify an input file (-f)"
		return 1
	fi

	# check to see if cluster was specified
	if [[ -z "$CLUSTER_NAME" ]]
	then
		print_info ERROR examine_command_line \
			"You must specify a cluster (-c)"
		return 1
	fi

	return 0
}


##############################################################################
# Read input file
#
# Will read the file that was prepared for this unit_stress run, the file
# will specify the parameters to run under, such as service name, and
# dependencies, etc.
#
# Will set the following variables
#	QUANTUM
#	CLIENT_MAX_COUNT
#	CLIENT_MIN_COUNT
#	CLIENT_MTB_START
#	CLIENT_MTB_END
#	CLIENT_MAX_THREADS
#	CLIENT_MIN_THREADS
#	SERVICE_NAME
#
function read_input_file
{
	typeset keyword value
	typeset line_count=0

	if [[ ! -e "$INPUT_FILE" ]]
	then
		print_info ERROR read_input_file \
			"File $INPUT_FILE does not exist"
		return 1
	fi

	QUANTUM=60			# in secs
	CLIENT_MAX_COUNT=""
	CLIENT_MIN_COUNT=""
	CLIENT_MTB_START=""
	CLIENT_MTB_END=""
	CLIENT_MAX_THREADS=""
	CLIENT_MIN_THREADS=""
	SERVICE_NAME=""

	while read keyword value
	do
		(( line_count += 1 ))
	
		# Ignore blank lines and comment lines
		[[ -z "$keyword" || "$keyword" = \#* ]] && continue

		case $keyword in
			CLIENT_MAX_COUNT)
				CLIENT_MAX_COUNT=$value
				;;
			CLIENT_MIN_COUNT)
				CLIENT_MIN_COUNT=$value
				;;
			CLIENT_MTB_START)
				CLIENT_MTB_START=$value
				;;
			CLIENT_MTB_END)
				CLIENT_MTB_END=$value
				;;
			CLIENT_MAX_THREADS)
				CLIENT_MAX_THREADS=$value
				;;
			CLIENT_MIN_THREADS)
				CLIENT_MIN_THREADS=$value
				;;
			SERVICE_NAME)
				SERVICE_NAME=$value
				;;
			QUANTUM)
				QUANTUM=$value
				;;
		esac
	done < $INPUT_FILE

	if [[ -z "$CLIENT_MAX_COUNT" ]]
	then
		print_info ERROR read_input_file "Missing CLIENT_MAX_COUNT"
		return 1
	fi

	if [[ -z "$CLIENT_MIN_COUNT" ]]
	then
		print_info ERROR read_input_file "Missing CLIENT_MIN_COUNT"
		return 1
	fi

	if [[ -z "$CLIENT_MTB_START" ]]
	then
		print_info ERROR read_input_file "Missing CLIENT_MTB_START"
		return 1
	fi

	if [[ -z "$CLIENT_MTB_END" ]]
	then
		print_info ERROR read_input_file "Missing CLIENT_MTB_END"
		return 1
	fi

	if [[ -z "$CLIENT_MAX_THREADS" ]]
	then
		print_info ERROR read_input_file "Missing CLIENT_MAX_THREADS"
		return 1
	fi

	if [[ -z "$CLIENT_MIN_THREADS" ]]
	then
		print_info ERROR read_input_file "Missing CLIENT_MIN_THREADS"
		return 1
	fi

	if [[ -z "$SERVICE_NAME" ]]
	then
		print_info ERROR read_input_file "Missing SERVICE_NAME"
		return 1
	fi

	return 0
}


##############################################################################
# Start client
#
# Arguments:
#	index of node to start client on
#
function start_client
{
	typeset index
	typeset node
	typeset nodeid
	typeset rslt
	typeset num_threads
	typeset kernel_client
	typeset client_name

	index=$1

	node=${NODENAME_LIST[$index]}
	nodeid=${NODEID_LIST[$index]}

	# Figure out the number of threads that we're going to start
	# with this client
	num_threads=$(random_number $CLIENT_MIN_THREADS $CLIENT_MAX_THREADS)

	# Figure out if this should be a kernel client
	kernel_client=$(random_number 0 1)

	# Figure out a unieq client name
	client_name="$SERVICE_NAME.client.$CLIENT_INDEX"

	if [[ $CLIENT_COUNT -ge $CLIENT_MAX_COUNT ]]
	then
		return -1
	fi

	$PING $node 3 > /dev/null
	if [ $? -ne 0 ]
	then
		print_info $PR_CLIENT_START $SERVICE_NAME \
			"$node is not responding"
		return -1
	fi

	# Place an entry in the list
	CLIENT_NAME_LIST[$CLIENT_COUNT]=$client_name
	(( CLIENT_COUNT += 1 ))
	(( CLIENT_INDEX += 1 ))

	if [[ $kernel_client -eq 0 ]]
	then
		#
		# USER LEVEL CLIENT
		#
		rslt=$($RSH $node "$WRAPPER -r /dev/null $CLIENT_START $SERVICE_NAME $client_name $num_threads")
	else
		#
		# KERNEL LEVEL CLIENT
		#
		rslt=$($RSH $node "$WRAPPER $KPROXY_CLIENT ha_stress_start_client $SERVICE_NAME $client_name $num_threads")
	fi

	if [[ $? -ne 0 ]]
	then
		print_info $PR_CLIENT_START $client_name \
			"rsh returned non-zero to node $node"
		rslt=1
	fi

	if [[ $rslt -eq 0 ]]
	then
		print_info $PR_CLIENT_START $client_name \
			"on $node (kernel:$kernel_client threads:$num_threads)"
	else
		# Take back the entry we added
		# I chose not to roll back the client_index counter
		# since its not very important if we skip a number
		(( CLIENT_COUNT -= 1 ))
		CLIENT_NAME_LIST[$CLIENT_COUNT]=""
		print_info $PR_CLIENT_START $client_name \
			"failed on $node"
	fi

	return 0
}


##############################################################################
# end client
#
# Stop a particular client
#
# Arugments:
#	name of node to stop client on
#
function end_client
{
	typeset index
	typeset node
	typeset nodeid
	typeset rslt
	typeset rsh_rslt
	typeset wrapper_rslt

	typeset client_index
	typeset client_name
	
	index=$1

	node=${NODENAME_LIST[$index]}
	nodeid=${NODEID_LIST[$index]}

	#
	# Pick a random client to stop
	#
	client_index=$(random_number 1 $CLIENT_COUNT)
	(( client_index -= 1 ))
	client_name=${CLIENT_NAME_LIST[$client_index]}

	$PING $node 3 > /dev/null
	if [ $? -ne 0 ]
	then
		print_info $PR_WARNING $PR_CLIENT_END $SERVICE_NAME \
			"$node is not responding"
		return -1
	fi

	#
	# Remove the entry for this client regarless of success or failure
	#
	CLIENT_NAME_LIST[$client_index]=""
	set -A CLIENT_NAME_LIST $(echo ${CLIENT_NAME_LIST[*]})
	CLIENT_COUNT=${#CLIENT_NAME_LIST[*]}

	rsh_rslt=$($RSH $node "$WRAPPER -p WRAPPER $CLIENT_STOP $client_name" 2>&1)
	rslt=$?
	wrapper_rslt=""

	#
	# rsh_rslt will contain the output from the client threads
	# and the output from the wrapper command
	#
	echo "$rsh_rslt" | while read msg
	do
		echo $msg | grep WRAPPER > /dev/null
		if [[ $? -eq 0 ]]
		then
			wrapper_rslt=$(echo $msg | awk '{ print $2 }')
		else
			print_info $PR_CLIENT $client_name "$msg"
		fi
	done

	if [[ -z "$wrapper_rslt" ]]
	then
		print_info ERROR $PR_CLIENT_END $client_name \
			"no result from wrapper found!"
		wrapper_rslt=0
	fi

	if [[ $rslt -eq 0 ]] && [[ $wrapper_rslt -eq 0 ]]
	then
		print_info $PR_CLIENT_END $client_name \
			"completed on node $node"
	else
		rslt=1
		print_info $PR_CLIENT_END $client_name "failed on $node"
	fi

	return $rslt
}


##############################################################################
# end all clients
#
function end_all_clients
{
	typeset node_index

	print_info $PR_CLIENT_END $SERVICE_NAME "stopping all clients"

	while [[ $CLIENT_COUNT -gt 0 ]]
	do
		node_index=$(find_working_node)
		if [[ $? -eq 0 ]]
		then
			end_client $node_index
		else
			print_info $PR_CLIENT_END $SERVICE_NAME \
				"aborted operation, all nodes are dead"
			return 1
		fi
	done

	print_info $PR_CLIENT_END $SERVICE_NAME "stopped all clients"

	return 0
}



##############################################################################
# do_work_client_start
#
# Will figure out if a client needs to be started (due to the mtb start)
function do_work_client_start
{
	typeset num
	typeset index
	typeset nodeid

	# Calculation lifted from solaris (and now cmm code)
	# This algorithm was lifeted from the work by Jeff Bonwick in the
	# kmem allocated from usr/src/uts/common/os/kmem.c
	(( CLIENT_START_RANDOM *= 2416 ))
	(( CLIENT_START_RANDOM += 374441 ))
	(( CLIENT_START_RANDOM %= 1771875 ))

	(( num = 1771875 / $CLIENT_MTB_START ))

	# If we already reached the MAX_count, then stop
	if [[ $CLIENT_COUNT -ge $CLIENT_MAX_COUNT ]]
	then
		return 0
	fi

	if [[ $CLIENT_START_RANDOM -lt $num ]]
	then
		# We hit the MTB Start
		index=$(find_working_node)
		if [[ $? -ne 0 ]]
		then
			print_info $PR_CLIENT_START $SERVICE_NAME \
				"all nodes are down ($CLUSTER_NAME)"
			return 1
		fi

		start_client $index
	fi

	return 0
}

##############################################################################
# do_work_client_end
#
# Will figure out if a client needs to end (due to mtb end)
function do_work_client_end
{
	typeset num

	# Calculation lifted from solaris (and now cmm code)
	# This algorithm was lifeted from the work by Jeff Bonwick in the
	# kmem allocated from usr/src/uts/common/os/kmem.c
	(( CLIENT_END_RANDOM *= 2416 ))
	(( CLIENT_END_RANDOM += 374441 ))
	(( CLIENT_END_RANDOM %= 1771875 ))

	(( num = 1771875 / $CLIENT_MTB_END ))

	if [[ $CLIENT_COUNT -le $CLIENT_MIN_COUNT ]]
	then
		return 0
	fi

	if [[ $CLIENT_END_RANDOM -lt $num ]]
	then
		# We hit the MTB shutdown
		index=$(find_working_node)
		if [[ $? -ne 0 ]]
		then
			print_info $PR_CLIENT_END $SERVICE_NAME \
				"all nodes are down ($CLUSTER_NAME)"
			return 1
		fi

		end_client $index
	fi

	return 0
}


##############################################################################
# Do stress sequence
#
# This function will run until interrupted, sleeping for the quantum time
# and at each quantum it will call functions to figure out if a provider
# needs to be started or killed, etc
# 
function do_stress_sequence
{
	typeset	shutdown_pid
	typeset sleep_time
	typeset start_time
	typeset end_time
	typeset diff_time

	# Seed the start time counter
	start_time=$(get_time)

	while [[ $DONE -eq 0 ]]
	do
		# Get the end time
		end_time=$(get_time)

		# Calculate the time spent executing
		(( diff_time = end_time - start_time ))
		(( sleep_time = QUANTUM - diff_time ))
		if [[ $sleep_time -gt 0 ]]
		then
			sleep $sleep_time
		fi

		# Get the start time
		start_time=$(get_time)

		# Do Work
		do_work_client_start
		do_work_client_end
	done

	return 0
}


##############################################################################
# Main Execution
#
function main
{
	typeset index
	typeset	node

	# Check that tools exists
	if ! verify_tools
	then
		return 1
	fi

	# Examine command line
	if ! examine_command_line "$@"
	then
		return 1
	fi

	# Init cluster structure
	if ! init_cluster_structure
	then
		return 1
	fi

	# Read the input file
	if ! read_input_file
	then
		return 1
	fi

	# Execute Stress Sequences
	if ! do_stress_sequence
	then
		return 1
	fi

	return 0
}


##############################################################################
# Call main
#
trap 'cleanup' EXIT
main "$@"
