#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)unit_dep_svc.ksh	1.10	08/05/20 SMI"
#

#
#
# Node Stress
#
# A copy of this shell script will run on ever node on a cluster that
# is running the HA stress test suite.  This script will make sure that
# the node its running on has its share of the load running.
#

readonly PROGNAME=$0		# Program name
readonly THIS_PROG=${0##*/}

PROGDIR=$(dirname $PROGNAME)	# Directory of program
if [[ "$PROGDIR" = "." ]]
then
	PROGDIR=$PWD
fi
readonly PROGDIR

##############################################################################
# Files that need to be sourced
#
# Source the aux file
. $PROGDIR/ha_stress_aux


##############################################################################
# Global Settings

set -o monitor			# so bg jobs can run with their own PGID and
				# we can trap SIGCHLD
set +o nounset			# allow use of unset variables
set +o notify			# dont notify of bg job completion

RANDOM=$$			# some seed
DONE=0
SVC_COUNTER=0			# Of service that this unit has started
TMP_DIR="/tmp/HAstress.tmp"	# Temporary Directory
readonly LOGINNAME=$(logname)	# User's name

#
# RANDOM Globals
#
SERVICE_START_RANDOM=$RANDOM	# Will be used in mtb shutdown
SERVICE_END_RANDOM=$RANDOM	# Will be used in mtb shutdown

#
# Keeps track of the service unit's pid
#
unset SERVICE_PID_LIST
unset SERVICE_NAME_LIST

#
# Tool location
#
readonly RSH="rsh -n -l root"
readonly PING=/usr/sbin/ping
readonly REPLCTL=/usr/cluster/bin/replctl

#
# Other HA_STRESS units
#
readonly UNIT_SVC=$PROGDIR/unit_svc

#
# Time constants
#
readonly START_HOURS=$($DATE +%H)
readonly START_MINUTES=$($DATE +%M)
readonly START_SECONDS=$($DATE +%S)

##############################################################################
# verify_tools
#
# Check to see if tools are there
#
function verify_tools
{
	typeset prog

	# Check to see if programs exist
	for prog in $DATE $PING $UNIT_SVC $REPLCTL
	do
		if [[ ! -x $prog ]]
		then
			print_info ERROR verify_tools "Can't find $prog"
			return 1
		fi
	done

	return 0
}


##############################################################################
# cleanup
#
# kill all child processes
#
function cleanup
{
	typeset pid

	stop_all_services

	# Kill all child processes
	for pid in $(jobs -p)
	do
		kill -INT -$pid 2>/dev/null
	done
	wait

	return 0
}


##############################################################################
# examine_command_line
#
# Look at options passed in
#
function examine_command_line
{
	typeset opt

	while getopts ':t:c:f:?' opt
	do
		case $opt in
		c)
			CLUSTER_NAME=$OPTARG
			;;
		f)
			INPUT_FILE=$OPTARG
			;;
		esac
	done
	
	#see if input file exists
	if [[ ! -e "$INPUT_FILE" ]]
	then
		print_info ERROR examine_command_line \
			 "You must specify an input file (-f)"
		return 1
	fi

	# check to see if cluster name was supplied
	if [[ -z "$CLUSTER_NAME" ]]
	then
		print_info ERROR examine_command_line \
			"You must specify a cluster (-c)"
		return 1
	fi

	return 0
}


##############################################################################
# Read input file
#
# Will read the file that was prepared for this unit_stress run, the file
# will specify the parameters to run under, such as service name, and
# dependencies, etc.
#
# Will set the following variables
#	SERVICE_NAME_PREFIX
#	PROVIDER_MTB_REMOVE
#	PROVIDER_MTB_ADD
#	PROVIDER_MAX_COUNT
#	DEPENDENCY_TREE_MAX_HEIGHT
#	DEPENDENCY_TREE_MAX_WIDTH
#
#	CLIENT_MAX_COUNT
#	CLIENT_MIN_COUNT
#	CLIENT_MTB_START
#	CLIENT_MTB_END
#	CLIENT_MAX_THREADS
#	CLIENT_MIN_THREADS

#	QUANTUM
#
function read_input_file
{
	typeset keyword value
	typeset line_count=0

	# Check to see if file exists
	if [[ ! -e "$INPUT_FILE" ]]
	then
		print_info ERROR read_input_file "$INPUT_FILE does not exist"
		return 1
	fi

	SERVICE_NAME_PREFIX=""
	PROVIDER_MTB_REMOVE=""
	PROVIDER_MTB_ADD=""
	PROVIDER_MAX_COUNT=""
	DEPENDENCY_TREE_MAX_HEIGHT=""
	DEPENDENCY_TREE_MAX_WIDTH=""

	CLIENT_MAX_COUNT=""
	CLIENT_MIN_COUNT=""
	CLIENT_MTB_START=""
	CLIENT_MTB_END=""
	CLIENT_MAX_THREADS=""
	CLIENT_MIN_THREADS=""
		
	QUANTUM=60			# in secs

	# Read in values for test
	while read keyword value
	do
		(( line_count += 1 ))
	
		# Ignore blank lines and comment lines
		[[ -z "$keyword" || "$keyword" = \#* ]] && continue

		case $keyword in
			SERVICE_NAME_PREFIX)
				SERVICE_NAME_PREFIX=$value
				;;
			DEPENDENCY_TREE_MAX_HEIGHT)
				DEPENDENCY_TREE_MAX_HEIGHT=$value
				;;
			DEPENDENCY_TREE_MAX_WIDTH)
				DEPENDENCY_TREE_MAX_WIDTH=$value
				;;
			PROVIDER_MTB_REMOVE)
				PROVIDER_MTB_REMOVE=$value
				;;
			PROVIDER_MTB_ADD)
				PROVIDER_MTB_ADD=$value
				;;
			PROVIDER_MAX_COUNT)
				PROVIDER_MAX_COUNT=$value
				;;
			CLIENT_MAX_COUNT)
				CLIENT_MAX_COUNT=$value
				;;
			CLIENT_MIN_COUNT)
				CLIENT_MIN_COUNT=$value
				;;
			CLIENT_MTB_START)
				CLIENT_MTB_START=$value
				;;
			CLIENT_MTB_END)
				CLIENT_MTB_END=$value
				;;
			CLIENT_MAX_THREADS)
				CLIENT_MAX_THREADS=$value
				;;
			CLIENT_MIN_THREADS)
				CLIENT_MIN_THREADS=$value
				;;
			QUANTUM)
				QUANTUM=$value
				;;
		esac
	done < $INPUT_FILE

	# If value is not set then print error
	if [[ -z "$SERVICE_NAME_PREFIX" ]]
	then
		print_info ERROR read_input_file "Missing SERVICE_NAME_PREFIX"
		return 1
	fi

	# If value is not set then print error
	if [[ -z "$DEPENDENCY_TREE_MAX_HEIGHT" ]]
	then
		print_info ERROR read_input_file \
			"Missing DEPENDENCY_TREE_MAX_HEIGHT"
		return 1
	fi

	# If value is not set then print error
	if [[ -z "$DEPENDENCY_TREE_MAX_WIDTH" ]]
	then
		print_info ERROR read_input_file \
			"Missing DEPENDENCY_TREE_MAX_WIDTH"
		return 1
	fi

	# If value is not set then print error
	if [[ -z "$PROVIDER_MTB_REMOVE" ]]
	then
		print_info ERROR read_input_file \
			"Missing PROVIDER_MTB_REMOVE"
		return 1
	fi

	# If value is not set then print error
	if [[ -z "$PROVIDER_MTB_ADD" ]]
	then
		print_info ERROR read_input_file "Missing PROVIDER_MTB_ADD"
		return 1
	fi

	# If value is not set then print error
	if [[ -z "$PROVIDER_MAX_COUNT" ]]
	then
		print_info ERROR read_input_file "Missing PROVIDER_MAX_COUNT"
		return 1
	fi

	# If value is not set then print error
	if [[ -z "$CLIENT_MAX_COUNT" ]]
	then
		print_info ERROR read_input_file "Missing CLIENT_MAX_COUNT"
		return 1
	fi

	# If value is not set then print error
	if [[ -z "$CLIENT_MIN_COUNT" ]]
	then
		print_info ERROR read_input_file "Missing CLIENT_MIN_COUNT"
		return 1
	fi

	# If value is not set then print error
	if [[ -z "$CLIENT_MTB_START" ]]
	then
		print_info ERROR read_input_file "Missing CLIENT_MTB_START"
		return 1
	fi

	# If value is not set then print error
	if [[ -z "$CLIENT_MTB_END" ]]
	then
		print_info ERROR read_input_file "Missing CLIENT_MTB_END"
		return 1
	fi

	# If value is not set then print error
	if [[ -z "$CLIENT_MAX_THREADS" ]]
	then
		print_info ERROR read_input_file "Missing CLIENT_MAX_THREADS"
		return 1
	fi

	# If value is not set then print error
	if [[ -z "$CLIENT_MIN_THREADS" ]]
	then
		print_info ERROR read_input_file "Missing CLIENT_MIN_THREADS"
		return 1
	fi

	return 0
}


##############################################################################
# shutdown service
#
# Arguments:
#	service pid
#	service name
function shutdown_service
{
	typeset service_pid=$1
	typeset service_name=$2

	# Kill services
	kill -INT -$service_pid > /dev/null 2>&1
	if [ $? -eq 0 ]
	then
		wait $service_pid
	fi

	# Show the output file
	if [[ -s "$output_file" ]]
	then
		cat $output_file
	fi

	return 0
}


##############################################################################
# start_service
#
# Will start a service unit, and update the service_pid_list
function start_service
{
	typeset tmp_file
	typeset setvice_name
	typeset pid
	typeset output_file
	typeset tree_height
	typeset max_width=$DEPENDENCY_TREE_MAX_WIDTH
	typeset width
	typeset level=0
	typeset level_width
	typeset dep_tree=""
	typeset depends_on=""
	typeset prev_lvl
	typeset index
	typeset tmp_array
	typeset svc_count

	# Calculate the tree height of this dep service tree
	tree_height=$(random_number 1 $DEPENDENCY_TREE_MAX_HEIGHT)

	#
	# Calculate the width of each level
	# This is an inverted tree were level 0 is the root on the dependencies
	#
	level=$tree_height
	while [[ $level -gt 0 ]]
	do
		level_width=$(random_number 1 $max_width)
		dep_tree[$level]=$level_width
		max_width=$level_width
		(( level -= 1 ))
	done

	#
	# The bottom of the tree (root of inverted tree)
	# needs to always be 1 wide
	#
	dep_tree[0]=1
	level=0

	#
	# Walk the dep tree backwards and create each service
	#
	# This means we're starting at the bottom of the tree from the root
	# of the dependency
	#
	while [[ $level -le $tree_height ]]
	do
		svc_count=0
		while [[ $svc_count -lt ${dep_tree[$level]} ]]
		do

			# unique service name
			service_name="$SERVICE_NAME_PREFIX.$$.$SVC_COUNTER"
			tmp_file=$TMP_DIR/$service_name.in

			# Clear it first
			rm -f $tmp_file

			if [[ $level -ne 0 ]]
			then
				# Find a dependency
				(( prev_lvl = level - 1 ))
				width=${dep_tree[$prev_lvl]}
				(( width -= 1 ))
				index=$(random_number 0 width)
				set -A tmp_array ${SERVICE_NAME_LIST[$prev_lvl]}
				if [[ ${#tmp_array[*]} -eq 0 ]]
				then
					print_info ERROR start_service \
						"No services to depend on"
					return 1
				fi

				depends_on=${tmp_array[$index]}
				unset tmp_array
			else
				depends_on=""
			fi

			# Put settings to run service into a file
			print "QUANTUM $QUANTUM" > $tmp_file
			print "SERVICE_NAME $service_name" >> $tmp_file
			print "SERVICE_DEPENDS $depends_on" >> $tmp_file
			print "PROVIDER_MTB_REMOVE $PROVIDER_MTB_REMOVE" >> $tmp_file
			print "PROVIDER_MTB_ADD $PROVIDER_MTB_ADD" >> $tmp_file
			print "PROVIDER_MAX_COUNT $PROVIDER_MAX_COUNT" >> $tmp_file

			print "CLIENT_MAX_COUNT $CLIENT_MAX_COUNT" >> $tmp_file
			print "CLIENT_MIN_COUNT $CLIENT_MIN_COUNT" >> $tmp_file
			print "CLIENT_MTB_START $CLIENT_MTB_START" >> $tmp_file
			print "CLIENT_MTB_END $CLIENT_MTB_END" >> $tmp_file
			print "CLIENT_MAX_THREADS $CLIENT_MAX_THREADS" >> $tmp_file
			print "CLIENT_MIN_THREADS $CLIENT_MIN_THREADS" >> $tmp_file

			# come up with a unique name for its output file
			output_file="$TMP_DIR/$service_name.out"

			$UNIT_SVC -c $CLUSTER_NAME -f $tmp_file -t $TMP_DIR 1>$output_file 2>&1 &
			pid=$!

			print_info $PR_DEP_SVC_START $service_name \
				"pid: $pid depends_on: $depends_on"

			SERVICE_PID_LIST[$level]="${SERVICE_PID_LIST[$level]} $pid"
			SERVICE_NAME_LIST[$level]="${SERVICE_NAME_LIST[$level]} $service_name"
			(( SVC_COUNTER += 1 ))
			(( svc_count += 1 ))
		done

	(( level += 1 ))
	done

	return 0
}


##############################################################################
# do_work_service_start
#
# Will figure out if a service needs to be started (due to mtb start)
function do_work_service_start
{
	start_service

	return $?
}


##############################################################################
# stop_all_services
#
function stop_all_services
{
	typeset pid
	typeset service_name
	typeset level=${#SERVICE_PID_LIST[*]}
	typeset index
	typeset tmp_pid_v
	typeset tmp_name_v

	print_info INFO stop_all_services "Begin stopping dependency tree"

	# Loop till all services are stopped
	while [[ $level -ge 0 ]]
	do
		set -A tmp_pid_v ${SERVICE_PID_LIST[$level]}
		set -A tmp_name_v ${SERVICE_NAME_LIST[$level]}

		index=0

		# Loop forever till there are no jobs left
		while [[ $index -lt ${#tmp_pid_v[*]} ]]
		do
			pid=${tmp_pid_v[$index]}
			service_name=${tmp_name_v[$index]}
			
			# Die service, Die!
			shutdown_service $pid $service_name
			(( index += 1 ))
		done

		(( level -= 1 ))
	done

	print_info INFO stop_all_services "completed stoppping dependency tree"

	cleanup

	return 0
}


##############################################################################
# Do stress sequence
#
# This function will run until interrupted, sleeping for the quantum time
# and at each quantum it will call functions to figure out if a provider
# needs to be started or killed, etc
# 
function do_stress_sequence
{
	do_work_service_start

	while true
	do
		sleep $QUANTUM
	done

	return $?
}

##############################################################################
# Main Execution
#
function main
{
	typeset index
	typeset	node

	# Check that tools exists
	if ! verify_tools
	then
		return 1
	fi

	# Examine command line
	if ! examine_command_line "$@"
	then
		return 1
	fi

	# Init cluster structure
	if ! init_cluster_structure
	then
		return 1
	fi

	# Read the input file
	if ! read_input_file
	then
		return 1
	fi

	# Execute Stress Sequences
	if ! do_stress_sequence
	then
		return 1
	fi

	return 0
}


##############################################################################
# Call main
#

trap 'cleanup' EXIT
main "$@"
