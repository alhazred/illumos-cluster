#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)run_test.ksh	1.8	08/05/20 SMI"
#

# run_test
#
# Is a front end to run
# This frontend invoked run in both modes, kernel client, and non-kernel client
#
# Items with "@@" markers will be substituted by make.
#
#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

# Where test programs/scripts are.
readonly DIR="@@PROGS_DIR@@"

# Run tests with kernel client.
# The last -k option forces $DIR/run to run with kernel client.
$DIR/run "$@" -k

print "\nPress [RETURN] for user-client tests." > /dev/tty
read < /dev/tty

# Run test with user client.
# The last -u option forces $DIR/run to run with user client.
$DIR/run "$@" -u
