#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)run.ksh	1.21	08/05/20 SMI"
#

#
# A simple script to automatically start the user-land and kernel-land
# clients and servers on various nodes of the cluster, run the tests once,
# and unload the clients and servers.  This is useful for a quick putback
# testing.
#
# Items with "@@" markers will be substituted by make.
#
# USER Land test programs are currently disabled

readonly PROGRAM=$0

# Where test programs/scripts are.
readonly DIR="@@PROGS_DIR@@"

readonly MC_INFO=/usr/cluster/orbtest/fi_support/cluster_info

readonly MODLOAD='/usr/sbin/modload -p'
readonly MODUNLOAD=/usr/sbin/modunload
readonly MODINFO=/usr/sbin/modinfo

readonly RMPROBE=/usr/cluster/bin/rm_probe

readonly SLEEP_BEFORE_UNLOADS=3         # before unloading kernel client/server
readonly UNLOAD_RETRIES=20              # how many times to try modunloading
readonly SLEEP_BETWEEN_UNLOADS=2        # sleep between the modunload retries

readonly UCLIENT=$DIR/repl_test_client	# test client program
readonly USWITCH=$DIR/repl_test_switch	# switchover trigger user prog

readonly KSERVER=misc/repl_test_server	# test server kernel module
readonly KCLIENT=misc/repl_test_client	# test client kernel module
readonly KSWITCH=misc/repl_test_switch	# switchover trigger kernel module

readonly POLL_TIME=1			# Seconds between each poll (switchover)
readonly VERIFY_TRIES=10		# How many times will we check?
readonly SERVICE_NAME="ha test server"	# Name of repl service
integer  NUM_FAILED_TESTS=0		# Number of failed switchovers
readonly NUM_SWITCHES=20		# Number of switchovers to cause
readonly SLEEP_BETWEEN_SWITCHES=5       # between switchovers

# Get the name of the host we're executing on
readonly HOSTNAME=$(/bin/hostname)

# Get node names and number of nodes.
if ! set -A NODE_NAMES $($MC_INFO -n)
then
	print -u2 "ERROR: $MC_INFO failed to get node names"
	exit 1
fi
readonly NUM_NODES=${#NODE_NAMES[*]}

# Get the name of the root node
if ! ROOT_NODE=$($MC_INFO -r)
then
	print -u2 "ERROR: $MC_INFO failed to get name of root node"
	exit 1
fi
readonly ROOT_NODE


#
# Print usage
#
function usage #
{
	print -u2 "usage:"
	print -u2 "\t$PROGRAM: [-c client-node] [-k|-u]"
	print -u2
	print -u2 "client-node is the node where the client should be run"
	print -u2 "-k runs the test with kernel client"
	print -u2 "-u runs the test with user client (the default)"
}


#
# Set up routine
#
function setup # [program_args ...]
{
	# Examine the command line
	while getopts hc:ku opt
	do
		case $opt in
		h)
			# Help
			usage
			exit 0
			;;
		c)
			# Get the cluster name.
			CLIENT_NODE=$OPTARG
			;;
		k)
			# Run the Kernel Client
			CLIENT=KERNEL
			;;
		u)
			# Run the User Client
			CLIENT=USER
			;;
		\?)
			usage
			exit 1
			;;
		esac
	done
	shift 'OPTIND - 1'

	if [ -z "$CLIENT_NODE" ]
	then
		# If client node is not set, set it to the root node
		CLIENT_NODE=$ROOT_NODE
	fi

	if [ -z "$CLIENT" ]
	then
		# if Client is not set, set it to user-land client
		CLIENT=USER
	fi

	if [ -z "$HOSTNAME" ]
	then
		print -u2 "Cannot determine the current host."
		exit 1
	fi
		
	# Make sure we're on the root node (run the test from root node)
	if [ "$HOSTNAME" != "$ROOT_NODE" ]
	then
		print -u2 "$PROGRAM must be run from the root node"
		exit 1
	fi

	# Verify that rm_probe is there
	if [ ! -x "$RMPROBE" ]
	then
		print -u2 "$RMPROBE not found."
		exit 1
	fi

	# Make sure we have the minimum number of nodes.
	case $NUM_NODES in
	1)
		# HA tests on 1 node clusters dont make sense
		print -u2 "ERROR: HA Tests on 1-node cluster are not supported."
		exit 1
		;;
	2)
		# HA tests on 2 nodes carry warnings
		print -u2 "WARNING: HA should also be tested on > 2 nodes."
		print -u2 "WARNING: Because of the root-node restriction."
		;;
	esac

	# Verify that we can rsh as root to each node.
	for host in ${NODE_NAMES[*]}
	do
		# First make sure the host is alive, so that way we won't
		# be stuck waiting for rsh to complete.
		if ping $host 3 >/dev/null 2>&1
		then
			if ! rsh -n -l root $host /bin/true
			then
				print -u2 "ERROR: can't rsh $host as root"
				exit 1
			fi
		else
			print -u2 "ERROR: node $host is apparently dead"
			exit 1
		fi
	done
}


#
# Unload the kernel client/server after a short sleep.  If the unloading fails,
# then we'll retry it for several times until it's successful.  These are
# because reference counting messages could take awhile to reach the object
# implementations.
#
function unload_kernmod # <"client"/"server"> <host> <module_id>
{
	typeset modtype=$1
	typeset host=$2
	typeset modid=$3
	typeset -i count=0
	typeset infoline

	while true
	do
		# Unload the kernel module.
		rsh -n -l root $host "$MODUNLOAD -i $modid"

		# Check to make sure it has really been unloaded.
		infoline=$(rsh -n -l root $host $MODINFO -i $modid 2>/dev/null)

		# If the module has been unloaded, break.
		[[ -z "$infoline" ]] && break

		# The module is still loaded, retry unloading.
		if (( count < UNLOAD_RETRIES ))
		then
			echo "Retrying modunload of kernel $modtype on $host ..."
			sleep $SLEEP_BETWEEN_UNLOADS
			(( count += 1 ))
		else
			print -u2 "ERROR: Can't unload kernel $modtype on" \
				"$host after $count tries"
			break
		fi
	done
}


#
# Start servers on all nodes
#
# Note: wel'll load the server for the root node last to cause a sleep to
#	occur during the invocations when the client/server are on the
#	root node, since the replicated test_mgr will initially be located
#	on a separate node than the client
#
function start_servers #
{
	typeset -i index
	typeset host

	echo "*** Starting KERNEL server on all nodes"

	index=0
	set -A KSERVER_HOSTS
	for host in ${NODE_NAMES[*]}
	do
		if [ "$host" != "$ROOT_NODE" ]
		then
			KSERVER_HOSTS[index]=$host
			(( index += 1 ))
		fi
	done
	KSERVER_HOSTS[index]=$ROOT_NODE		# make root node last

	index=0
	set -A KSERVER_MODIDS
	for host in ${KSERVER_HOSTS[*]}
	do
		rsh -n -l root $host $MODLOAD $KSERVER
		KSERVER_MODIDS[index]=$(rsh -n -l root $host $MODINFO \| \
			nawk -v modfile=${KSERVER##*/} \
				"'\$6 == modfile {print \$1}'")
		(( index += 1 ))
	done
}


#
# Stop servers
#
function stop_servers #
{
	typeset -i num_servers=${#KSERVER_MODIDS[*]}
	typeset -i index=0

	while (( index < num_servers ))
	do
		unload_kernmod "server" ${KSERVER_HOSTS[index]} \
					${KSERVER_MODIDS[index]}
		(( index += 1 ))
	done
}


#
# Start clients
#
function start_clients #
{
	# Assign hosts to run the clients.
	UCLIENT_HOST=$CLIENT_NODE
	KCLIENT_HOST=$CLIENT_NODE

	if [ "$CLIENT" = "USER" ]
	then
		set -o monitor				# so we can get pgid's

		echo "*** Starting USER client on $UCLIENT_HOST"
		rsh -n -l root $UCLIENT_HOST sh -c "'$UCLIENT 2>&1'"&
		UCLIENT_PID=$!
	else
		echo "*** Starting KERNEL client on $KCLIENT_HOST"
		rsh -n -l root $KCLIENT_HOST $MODLOAD $KCLIENT
		KCLIENT_MODID=$(rsh -n -l root $KCLIENT_HOST $MODINFO \| \
			nawk -v modfile=${KCLIENT##*/} \
				"'\$6 == modfile {print \$1}'")
	fi
}


#
# Stop clients
#
function stop_clients #
{
	if [ "$CLIENT" = "KERNEL" ]
	then
		# Unload the kernel client
		echo "*** Unloading KERNEL client..."
		unload_kernmod "client" $KCLIENT_HOST $KCLIENT_MODID
	else
		# Kill using the process group id
		kill -TERM -$UCLIENT_PID 2>/dev/null

		echo "*** Waiting for USER client to finish..."
		wait $UCLIENT_PID
	fi
}


#
# Function to do each switchover.. using the probes to verify that they
# Occurred
#
function switchover
{
	echo "*** Forcing a random switchover"
	typeset	CUR_PRIMARY=""
	typeset NEW_PRIMARY=""
	typeset RETRY_COUNT=0

	CUR_PRIMARY=$($RMPROBE -s "$SERVICE_NAME" find-primary 2>&1 | \
			awk '/rm_probe/ { print $2 }')

	# Do switch (do it from userland)
	$USWITCH 2>&1

	# Verify that the switch took place
	while (( RETRY_COUNT < VERIFY_TRIES ))
	do
		# Get the current primary
		NEW_PRIMARY=$($RMPROBE -s "$SERVICE_NAME" find-primary 2>&1 | \
				awk '/rm_probe/ { print $2 }')

		[[ "$CUR_PRIMARY" != "$NEW_PRIMARY" ]] && break;
		(( RETRY_COUNT += 1 ))
		sleep $POLL_TIME
	done

	if (( RETRY_COUNT == VERIFY_TRIES ))
	then
		(( NUM_FAILED_TESTS += 1 ))
	fi
}


#
# Do the tests
#
#	XXX For now all we'll test is switchovers
#
function do_tests #
{
	typeset -i count=0

	NUM_FAILED_TESTS=0
	while (( count < NUM_SWITCHES ))
	do
		# Call function
		switchover

		# allow new primary some time XXX due to bug
		sleep $SLEEP_BETWEEN_SWITCHES
		(( count += 1 ))
	done

	echo ------------------------------------------------------------------
	echo "Expect $NUM_SWITCHES replica changes from each client thread"
	echo "This verifies that client continued to do work"
	echo "RM PROBE will verify the number of switchovers at the rma level"
	echo "RM PROBE VERIFICATION: $NUM_FAILED_TESTS switchovers failed."
	if (( NUM_FAILED_TESTS == 0 ))
	then
		echo "RM PROBE VERIFICATION: PASSED"
	else
		echo "RM PROBE VERIFICATION: FAILED"
	fi
	echo ------------------------------------------------------------------
}


#
# Do it
#
setup "$@"

start_servers
start_clients

sleep 3				# give user-land clients/servers time to start
do_tests

stop_clients
# XXX Unloading of server modules has not been implemented yet
# stop_servers

exit 0
