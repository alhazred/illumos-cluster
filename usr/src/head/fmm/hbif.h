/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_HBIF_H
#define	_HBIF_H

#pragma ident	"@(#)hbif.h	1.3	08/05/20 SMI"

#include <netinet/in.h>
#include <fmm/hb.h>
#include <sys/hbxctl.h>

#ifdef	__cplusplus
extern "C" {
#endif

#define	HB_MAX_PHYS_LINKS 8
/*
 * Callback type
 */
typedef void hbif_callback(hbx_event_t event);

hb_error_t
hbif_init(hbif_callback *cb);

hb_error_t
hbif_set_params(hbx_params_t *params);

enum hbx_host_state
hbif_get_host_state(ipaddr_t host, unsigned int link);

hb_error_t
hbif_delete_host(ipaddr_t host, unsigned int link);

hb_error_t
hbif_start(enum hbx_mode_t mode);

hb_error_t
hbif_stop(void);

hb_error_t
hbif_reset(void);

hb_error_t
hbif_set_pkey(hbx_pkey_t *pkey);

hb_error_t
hbif_set_alt_pkey(hbx_pkey_t *pkey);

hb_error_t
hbif_commit_pkey(void);

#ifdef	__cplusplus
}
#endif

#endif	/* _HBIF_H */
