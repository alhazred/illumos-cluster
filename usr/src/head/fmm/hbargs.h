/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_HBARGS_H
#define	_HBARGS_H

#pragma ident	"@(#)hbargs.h	1.3	08/05/20 SMI"

#include <fmm/hb.h>
#include <sys/hbxctl.h>

#ifdef	__cplusplus
extern "C" {
#endif

#define	HB_MAX_NIC	10
#define	HB_NIC_LENGTH	10
typedef char* hb_nic_list_t[HB_MAX_NIC];

#define	HB_MAX_PKEY	2
#define	HB_PKEY_LENGTH	10
typedef hbx_pkey_t* hb_pkey_list_t[HB_MAX_PKEY];

hb_error_t
hbargs_init(void);

boolean_t
hbargs_get_debug(void);

ipaddr_t
hbargs_get_mca(void);

uint8_t
hbargs_get_nic_list(hb_nic_list_t nic_list);

uint8_t
hbargs_get_nic_count(void);

hb_error_t
hbargs_get_heartbeat_params(hbx_params_t *params);

#ifdef	__cplusplus
}
#endif

#endif	/* _HBARGS_H */
