/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _API_COMM_H
#define	_API_COMM_H

#pragma ident	"@(#)fmm_api_comm.h	1.2	08/05/20 SMI"

#include "saf_clm.h"

#ifdef __cplusplus
extern "C" {
#endif

#define	FMM_TMP_DIR "/var/run"
#define	FMM_KEY_PREFIX "FMM_"
#define	FMM_API_DOOR_NAME	"/var/run/FMM_api_v1_door"

typedef enum {
	FMM_API_REGISTER,
	FMM_API_CONNECT,
	FMM_API_UNREGISTER,
	FMM_API_NODE_GET,
	FMM_API_ALL_NODES_GET
} fmm_api_services_t;


/*
 * ***************************************************************
 * NOTICE: This structure is quite particular, because in some
 *	cases an array of element must be returned and we have
 *	no way to know its size at compile-time.
 *	Currently, the only member with this particularity is
 *	out.member_getall.member_table, so it must be placed
 *	in the last position in the structure (since out is a
 *	union, this trick can also be done with the rest of the
 *	element of the union) to be able to treat it as an
 *	array.
 * **************************************************************
 */


typedef struct {
	SaVersionT version;
	int id;
} fmm_api_register_args_t;

typedef struct {
	int id;
} fmm_api_connect_args_t;

typedef struct {
	int id;
} fmm_api_unregister_args_t;

typedef struct {
	SaClmNodeIdT nodeId;
	SaTimeT timeout;
} fmm_api_node_get_args_t;

typedef struct {
	SaUint32T numberOfItems;
} fmm_api_all_nodes_get_args_t;

typedef struct {
	SaClmClusterNodeT node;
} fmm_api_node_get_res_t;

typedef struct {
	SaUint32T numberOfItems;
	SaUint64T viewNumber;
	SaClmClusterNotificationT notificationBuffer[1];
} fmm_api_all_nodes_get_res_t;

typedef struct {
	fmm_api_services_t service;
	SaErrorT    	result;
	union {
		fmm_api_register_args_t fmm_register;
		fmm_api_connect_args_t fmm_connect;
		fmm_api_unregister_args_t fmm_unregister;
		fmm_api_node_get_args_t fmm_node_get;
		fmm_api_all_nodes_get_args_t fmm_all_nodes_get;
	} in;
	union {
		fmm_api_node_get_res_t fmm_node_get;
		fmm_api_all_nodes_get_res_t fmm_all_nodes_get;
	} out;
} fmm_api_args_t;

typedef struct {
	unsigned long viewnumber;
} fmm_api_notification_message_t;


#ifdef __cplusplus
}
#endif

#endif /* _API_COMM_H */
