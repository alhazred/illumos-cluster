/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_HBR_H
#define	_HBR_H

#pragma ident	"@(#)hbr.h	1.3	08/05/20 SMI"

#include <fmm/hb.h>

#ifdef	__cplusplus
extern "C" {
#endif

hb_error_t
hbr_init(hb_callback *cb);

hb_error_t
hbr_start(void);

hb_error_t
hbr_stop(void);

hb_error_t
hbr_reset(void);

hb_error_t
hbr_delete_node(hb_nodeid_t nodeid);

#ifdef	__cplusplus
}
#endif

#endif	/* _HBR_H */
