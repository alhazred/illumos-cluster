/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_HBRSIMU_H
#define	_HBRSIMU_H

#pragma ident	"@(#)hbrsimu.h	1.3	08/05/20 SMI"

#include <fmm/hb.h>

#define	HBSIMU_TMP_DIR "/var/run"
#define	HBSIMU_FIFO "hbrsimu"

typedef struct {
	hb_nodeid_t nodeid;
	unsigned int incarnation;
	hb_evt_type_t event;
} hbsimu_msg_t;

#endif /* _HBRSIMU_H */
