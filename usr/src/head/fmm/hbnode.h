/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_HBNODE_H
#define	_HBNODE_H

#pragma ident	"@(#)hbnode.h	1.3	08/05/20 SMI"

#include <sys/types.h>
#include <sys/int_types.h>
#include <sys/clconf.h>
#include <sys/socket.h>

#include <fmm/hb.h>
#include <fmm/hbif.h>

#ifdef	__cplusplus
extern "C" {
#endif

/* Maximum number of nodes supported */
#define	HB_MAX_NODES 512

/* This macro provides the address as an integer */
#define	INT_ADDR(node)	(((struct sockaddr_in *)&(node))->sin_addr.s_addr)

typedef struct {
	hb_state_t current_state;
	hb_state_t previous_state;
} hb_link_status_t;

typedef hb_link_status_t hb_node_status_t;

/* Database element : HB link description + interfaces status */
typedef struct {
	struct sockaddr_storage links_addr[HB_MAX_PHYS_LINKS];
	nodeid_t node_id;
	unsigned int  in;
	hb_node_status_t node_status;
	hb_link_status_t links_status[HB_MAX_PHYS_LINKS];
	pthread_mutex_t	mutex;
} hb_node_t;

hb_error_t
hbnode_add(hb_node_t *node);

hb_error_t
hbnode_del(hb_node_t *node);

hb_error_t
hbnode_reset(void);

hb_error_t
hbnode_create_list(size_t l_size, size_t e_size);

hb_error_t
hbnode_init(hb_callback *cb);

void
hbnode_destroy_list(void);

void
hbnode_set_status(void);

hb_node_t *
hbnode_get_from_link(ipaddr_t addr);

hb_error_t
hbnode_delete_node(hb_nodeid_t nodeid);

/* Conversion macros */
#define	ADDR_TO_ID(a)	((hb_nodeid_t)	\
	(((struct sockaddr_in *)&(a))->sin_addr.s_addr))
#define	NODE_TO_ID(n)	ADDR_TO_ID((n).node.addr)

#ifdef	__cplusplus
}
#endif

#endif	/* _HBNODE_H */
