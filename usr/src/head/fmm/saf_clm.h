/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SAF_CLM_H
#define	_SAF_CLM_H

#pragma ident	"@(#)saf_clm.h	1.2	08/05/20 SMI"

#include "saf_types.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef SaUint32T SaClmHandleT;
typedef SaUint32T SaClmNodeIdT;

#define	SA_CLM_MAX_ADDRESS_LENGTH 64
typedef struct {
	SaUint8T length;
	unsigned char value[SA_CLM_MAX_ADDRESS_LENGTH];
} SaClmNodeAddressT;

typedef struct {
	SaClmNodeIdT nodeId;
	SaClmNodeAddressT nodeAddress;
	SaNameT nodeName;
	SaNameT clusterName;
	SaBoolT member;
	SaTimeT bootTimestamp;
	SaUint64T initialViewNumber;
} SaClmClusterNodeT;

typedef enum {
	SA_CLM_NODE_NO_CHANGE = 1,
	SA_CLM_NODE_JOINED = 2,
	SA_CLM_NODE_LEFT = 3
} SaClmClusterChangesT;

typedef struct {
	SaClmClusterNodeT clusterNode;
	SaClmClusterChangesT clusterChanges;
} SaClmClusterNotificationT;

typedef void (*SaClmClusterNodeGetCallbackT)(
	SaInvocationT invocation,
	SaClmClusterNodeT *clusterNode,
	SaErrorT error);

typedef void (*SaClmClusterTrackCallbackT)(
	SaClmClusterNotificationT *notificationBuffer,
	SaUint32T numberOfItems,
	SaUint32T numberOfMembers,
	SaUint64T viewNumber,
	SaErrorT error);

typedef struct {
	SaClmClusterNodeGetCallbackT saClmClusterNodeGetCallback;
	SaClmClusterTrackCallbackT saClmClusterTrackCallback;
} SaClmCallbacksT;


SaErrorT saClmInitialize(
	SaClmHandleT *clmHandle,
	const SaClmCallbacksT *clmCallbacks,
	const SaVersionT *version);

SaErrorT saClmSelectionObjectGet(
	SaClmHandleT clmHandle,
	SaSelectionObjectT *selectionObject);

SaErrorT saClmDispatch(
	SaClmHandleT clmHandle,
	SaDispatchFlagsT dispatchFlags);

SaErrorT saClmFinalize(
	SaClmHandleT clmHandle);

SaErrorT saClmClusterTrackStart(
	SaClmHandleT clmHandle,
	SaUint8T trackFlags,
	SaClmClusterNotificationT *notificationBuffer,
	SaUint32T numberOfItems);

SaErrorT saClmClusterTrackStop(
	SaClmHandleT clmHandle);

SaErrorT saClmClusterNodeGet(
	SaClmHandleT clmHandle,
	SaClmNodeIdT nodeId,
	SaTimeT timeout,
	SaClmClusterNodeT *clusterNode);

SaErrorT saClmClusterNodeGetAsync(
	SaClmHandleT clmHandle,
	SaInvocationT invocation,
	SaClmNodeIdT nodeId,
	SaClmClusterNodeT *clusterNode);

#ifdef __cplusplus
}
#endif

#endif /* _SAF_CLM_H */
