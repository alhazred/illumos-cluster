/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SAF_TYPES_H
#define	_SAF_TYPES_H

#pragma ident	"@(#)saf_types.h	1.2	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <sys/types.h>

typedef enum {
	SA_FALSE = 0,
	SA_TRUE = 1
} SaBoolT;

typedef int8_t SaInt8T;
typedef int16_t SaInt16T;
typedef int32_t SaInt32T;
typedef int64_t SaInt64T;

typedef uint8_t SaUint8T;
typedef uint16_t SaUint16T;
typedef uint32_t SaUint32T;
typedef uint64_t SaUint64T;

typedef SaInt64T SaTimeT;

#define	SA_TIME_END ((SaTimeT)0x7FFFFFFFFFFFFFFF)
#define	SA_TIME_BEGIN ((SaTimeT)0x8000000000000001)
#define	SA_TIME_RELATIVE_LIMIT ((SaTimeT)0x0C00000000000000)
#define	SA_TIME_UNKNOWN ((SaTimeT)0x8000000000000000)

#define	SA_MAX_NAME_LENGTH 256
typedef struct {
	SaUint16T length;
	unsigned char value[SA_MAX_NAME_LENGTH];
} SaNameT;

typedef struct {
	char releaseCode;
	unsigned char major;
	unsigned char minor;
} SaVersionT;

#define	SA_TRACK_CURRENT 0x01
#define	SA_TRACK_CHANGES 0x02
#define	SA_TRACK_CHANGES_ONLY 0x04

typedef enum {
	SA_DISPATCH_ONE = 1,
	SA_DISPATCH_ALL = 2,
	SA_DISPATCH_BLOCKING = 3
} SaDispatchFlagsT;

typedef SaUint64T SaSelectionObjectT;
typedef SaUint64T SaInvocationT;

typedef enum {
	SA_OK = 1,
	SA_ERR_LIBRARY = 2,
	SA_ERR_VERSION = 3,
	SA_ERR_INIT = 4,
	SA_ERR_TIMEOUT = 5,
	SA_ERR_TRY_AGAIN = 6,
	SA_ERR_INVALID_PARAM = 7,
	SA_ERR_NO_MEMORY = 8,
	SA_ERR_BAD_HANDLE = 9,
	SA_ERR_BUSY = 10,
	SA_ERR_ACCESS = 11,
	SA_ERR_NOT_EXIST = 12,
	SA_ERR_NAME_TOO_LONG = 13,
	SA_ERR_EXIST = 14,
	SA_ERR_NO_SPACE = 15,
	SA_ERR_INTERRUPT = 16,
	SA_ERR_SYSTEM = 17,
	SA_ERR_NAME_NOT_FOUND = 18,
	SA_ERR_NO_RESOURCES = 19,
	SA_ERR_NOT_SUPPORTED = 20,
	SA_ERR_BAD_OPERATION = 21,
	SA_ERR_FAILED_OPERATION = 22,
	SA_ERR_MESSAGE_ERROR = 23,
	SA_ERR_NO_MESSAGE = 24,
	SA_ERR_QUEUE_FULL = 25,
	SA_ERR_QUEUE_NOT_AVAILABLE = 26,
	SA_ERR_BAD_CHECKPOINT = 27,
	SA_ERR_BAD_FLAGS = 28
} SaErrorT;

#ifdef	__cplusplus
}
#endif

#endif /* _SAF_TYPES_H */
