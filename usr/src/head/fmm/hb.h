/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _HB_H
#define	_HB_H

#pragma ident	"@(#)hb.h	1.3	08/05/20 SMI"

#ifdef Solaris
#include <sys/types.h>
#include <sys/int_types.h>
#endif

#ifdef linux
#include <stdint.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

typedef uint32_t hb_nodeid_t;

typedef uint64_t* hb_node_data_t;

typedef enum {
	HB_ST_UNKNOWN = 0,
	HB_ST_DOWN,
	HB_ST_UP
} hb_state_t;

static char *hb_states[] = {
	"HB_ST_UNKNOWN",
	"HB_ST_DOWN",
	"HB_ST_UP"
};

/*
 * Indication definitions
 */
typedef struct {
	int		type;
	unsigned int	linkid;
	unsigned int	in;
	hb_state_t	state;
	hb_nodeid_t	nodeid;
} hb_indication_t;

/*
 * Indication-type values
 */
#define	HB_LINK_FAILURE   0
#define	HB_LINK_RECOVER   1
#define	HB_NODE_FAILURE   2
#define	HB_NODE_RECOVER   3

static char *hb_indications[] = {
	"HB_LINK_FAILURE",
	"HB_LINK_RECOVER",
	"HB_NODE_FAILURE",
	"HB_NODE_RECOVER"
};

typedef int hb_error_t;

#define	HB_OK		0
#define	HB_ENOTSETUP	-1
#define	HB_EINVAL	-2
#define	HB_ENOTSUP	-3
#define	HB_ENOMEM	-4
#define	HB_EEXIST	-5

typedef enum {
	F_NODE_UP,
	F_NODE_DOWN
} hb_evt_type_t;

/*
 * Callback type
 */

typedef void hb_callback(hb_nodeid_t nodeid, hb_evt_type_t event,
    uint32_t incn);

#ifdef __cplusplus
}
#endif

#endif /* _HB_H */
