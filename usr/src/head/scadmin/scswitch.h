/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SCSWITCH_H
#define	_SCSWITCH_H

#pragma ident	"@(#)scswitch.h	1.31	08/07/24 SMI"

/*
 * This is the header file for libscswitch. It contains functions scswitch and
 * GUI needed to perform ownership change of resource groups and HA devices.
 *
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <string.h>
#include <sys/errno.h>
#include <sys/types.h>
#include <sys/clconf_int.h>
#include <rgm/rgm_scswitch.h>
#include <dc/libdcs/libdcs.h>
#include <scadmin/scconf.h>

#define	CMD_SUSPEND	1
#define	CMD_RESUME	2

/* Error codes returned by scswitch functions. */
/* This definition is covered by PSARC/1999/462.  DO NOT change it. */
typedef enum scswitch_errno {
	SCSWITCH_NOERR = 0,		/* normal return - no error */
	SCSWITCH_ERROR = 1,		/* action failed */
	SCSWITCH_EINVAL = 2,		/* scshutdown: invalid argument */
	SCSWITCH_ENOMEM = 3,		/* not enough memory */
	SCSWITCH_EPERM = 4,		/* not root, permission denied */
	SCSWITCH_ENOEXIST = 5,		/* the nodename (or, id) is not found */
	SCSWITCH_ENOCLUSTER = 6,	/* not a cluster node */
	SCSWITCH_EINVALSTATE = 7,	/* node is in a invalid state */
	SCSWITCH_EUSAGE = 8,		/* command usage error */
	SCSWITCH_ECCR = 9,		/* error accessing the CCR */
	SCSWITCH_ENAME = 10,		/* service name invalid */
	SCSWITCH_EBUSY = 11,		/* service is busy */
	SCSWITCH_EINACTIVE = 12,	/* service has not yet been started */
	SCSWITCH_ENODEID = 13,		/* node does not exist in the cluster */
	SCSWITCH_EREPLICA_FAILED = 14,	/* node failed to become the primary */
	SCSWITCH_RGSWITCH_FAILED = 15,	/* switch rg failed */
	SCSWITCH_ERECONFIG = 16,	/* cluster is reconfiguring */
	SCSWITCH_EOBSOLETE = 17,	/* Resource/RG has been updated */
	SCSWITCH_ERG = 18,		/* invalid rg */
	SCSWITCH_ERGNODE = 19,		/* no valid nodelist for rg */
	SCSWITCH_EUNEXPECTED = 20,	/* internal or unexpected error */
	SCSWITCH_ENULLRGLIST = 21,	/* null rg list */
	SCSWITCH_EDUPNODE = 22,		/* duplicate node name */
	SCSWITCH_ERGSTATE = 23,		/* unmanaged rg */
	SCSWITCH_EDUPRG = 24,		/* duplicate rg name */
	SCSWITCH_EDETACHED = 25,	/* rg contains detached resource */
	SCSWITCH_EMAXPRIMARIES = 26,	/* exceeds maximum_primaries for rg */
	SCSWITCH_EMASTER = 27,		/* node not a potential master of rg */
	SCSWITCH_ESTOPFAILED = 28,	/* rg ERROR_STOP_FAILED */
	SCSWITCH_ERGRECONF = 29,	/* one or more RGs are reconfiguring */
	SCSWITCH_ESCHA_ERR = 30,	/* scha error */
	SCSWITCH_EMASTER_SERVICE = 31,	/* node not allowed to be a primary */
	SCSWITCH_ECZONE = 32		/* command not allowed from a CZ */
} scswitch_errno_t;

typedef struct scswitch_errbuff {
	scswitch_errno_t err_code;
	char	*err_msg;
} scswitch_errbuff_t;

/* This definition is covered by PSARC/1999/462.  DO NOT change it. */
typedef struct scswitch_service_struct {
	char				*scswitch_service_name;
	struct  scswitch_service_struct *next;
} scswitch_service_t;

/*
 * Switch services to specified node.
 *
 * Possible return values:
 *	SCSWITCH_NOERR
 *	SCSWITCH_ERROR
 *	SCSWITCH_EINVAL
 *	SCSWITCH_ENOMEM
 *	SCSWITCH_ENOEXIST
 *	SCSWITCH_ENODEID
 *	SCSWITCH_EINVALSTATE
 *	SCSWITCH_EMASTER_SERVICE
 */
/* This definition is covered by PSARC/1999/462.  DO NOT change it. */
scswitch_errno_t scswitch_switch_service(
    scswitch_service_t *scswitch_service_list, char *node_name);

/*
 * scswitch_resume_service
 *
 * resumes a dcs service that is currently suspended.
 *
 * Possible return values :
 * SCSWITCH_NOERR               - Success
 * SCSWITCH_ECLUSTER            - Not a cluster member
 * SCSWITCH_EINVAL              - Service is in invalid state
 * SCSWITCH_EINTERNAL           - Bad call
 *
 */
scswitch_errno_t
scswitch_resume_service(char *service);

/*
 * Take specified HA services offline.
 *
 * Possible return values:
 *	SCSWITCH_NOERR
 *	SCSWITCH_ERROR
 *	SCSWITCH_EINVAL
 *	SCSWITCH_ENOMEM
 *
 */
/* This definition is covered by PSARC/1999/462.  DO NOT change it. */
scswitch_errno_t scswitch_take_service_offline(
    scswitch_service_t *scswitch_service_list);

/*
 * Suspend specified HA services .
 *
 * Possible return values:
 *	SCSWITCH_NOERR
 *	SCSWITCH_ERROR
 *	SCSWITCH_EINVAL
 *	SCSWITCH_ENOMEM
 *
 */
scswitch_errno_t scswitch_suspend_service(
    scswitch_service_t *scswitch_service_list);

/*
 * scswitch_strerr
 *
 * Map scswitch_errno_t to a string.
 *
 * The supplied "text" should be of at least SCSWITCH_MAX_STRING_LEN
 * in length.
 */
/* This definition is covered by PSARC/1999/462.  DO NOT change it. */
void
scswitch_strerr(char *name, char *text, scswitch_errno_t err);

/*
 * scswitch_convert_scconf_error_code
 *
 * convert a scconf error code to scstat error code
 *
 * Possible return values:
 *
 *      SCSWITCH_NOERR          - success
 *      SCSWITCH_ENOMEM         - not enough memory
 *      SCSWITCH_ENOTCLUSTER    - not a cluster node
 *      SCSWITCH_EINVAL         - scshutdown: invalid argument
 *      SCSWITCH_EPERM          - not root
 *      SCSWITCH_EUNEXPECTED    - internal or unexpected error
 *	SCSWITCH_ENOEXIST       - the nodename (or, id) is not found
 */
scswitch_errno_t
scswitch_convert_scconf_error_code(const scconf_errno_t scconf_error);


/*
 * scswitch_evacuate_devices
 *  evacuate devices from current primary to secondary or shudown all the
 *  devices in the cluster  based on the value passed to 'nodeoption' parameter.
 *
 * input: if (nodeid) => evacuate devices from current primary to secondary
 *	  if (!nodeid) => shutdown all the devices in the cluster.
 *
 * Possible return values:
 *      SCSWITCH_NOERR          - success
 *      SCSWITCH_ENOMEM         - not enough memory
 *      SCSWITCH_ENOTCLUSTER    - not a cluster node
 *      SCSWITCH_EINVAL         - scconf: invalid argument
 *      SCSWITCH_EPERM          - not root
 *      SCSWITCH_ECCR           - error accessing the CCR
 *      SCSWITCH_ENAME          - service name invalid
 *      SCSWITCH_EBUSY          - service busy
 *      SCSWITCH_EINACTIVE      - service has not yet been started
 *      SCSWITCH_ENODEID        - Node id does not exist in the cluster
 *      SCSWITCH_EINVALID_STATE - Node is in a invalid state
 *      SCSWITCH_EREPLICA_FAILED- Node failed to become the primary
 *      SCSWITCH_EUNEXPECTED    - internal or unexpected error
 *
 */
scswitch_errbuff_t scswitch_evacuate_devices(nodeid_t nodeid);

/*
 * scswitch_evacuate_rgs
 *  evacuate rgs from a given node, or bring all the
 *  rgs offline in the cluster  based on the value passed to 'nodename'
 *  parameter.
 *
 * input: if (nodename) => switch all rgs off of nodename
 *	  if (!nodename) => bring all the rgs in the cluster offline.
 *
 * 'evac_timeout' parameter gives number of seconds that RGs should continue
 * to be prevented from failover onto the node that is being evacuated,
 * after the evacuation has completed.
 * verbose_flag is set for verbose output used by the newcli.
 *
 * Return code/message is passed back from the rgmd.
 */
scha_errmsg_t scswitch_evacuate_rgs(const char *nodename,
    ushort_t evac_timeout, boolean_t verbose_flag, char *);

/*
 * scswitch_evacuate
 *
 *  Evacuate all resource and device groups from the given "nodeid"/"nodename".
 *
 * Possible return values:
 *      SCSWITCH_NOERR          - success
 *      SCSWITCH_EINVAL         - scshutdown: invalid argument
 *      SCSWITCH_ENOMEM         - not enough memory
 *      SCSWITCH_EPERM          - not root
 *      SCSWITCH_ENOEXIST       - the nodename (or, id) is not found
 *      SCSWITCH_ENOTCLUSTER    - not a cluster node
 *      SCSWITCH_ERECONFIG      - cluster is reconfiguring
 *      SCSWITCH_EOBSOLETE      - Resource/RG has been updated
 *      SCSWITCH_EUNEXPECTED    - unexpected error
 *
 *  RG evacuation is now performed by the rgmd, so we pass back
 *  a scha_errmsg_t result (thru the scha_err argument) as well as
 *  as the scswitch_errbuf_t return value.  If the scha_err code
 *  is non-zero, then the function returns early and the caller should
 *  use the scha error code/message.
 * verbose_flag is set for verbose output used by the newcli.
 */
scswitch_errbuff_t scswitch_evacuate(nodeid_t nodeid, const char *nodename,
    const char *zonename, ushort_t timeout, scha_errmsg_t *scha_err,
    boolean_t verbose_flag, char *);


/*
 * scswitch_switch_rg
 *	Switch RGs to specified nodes. If reconfig is enabled,
 *	the RGs start reconfiguration.
 * verbose_flag is set for verbose output used by the newcli.
 *
 *  Possible return values:
 *	scha_errmsg_t
 *
 */
scha_errmsg_t
scswitch_switch_rg(char *scswitch_node_list, char *scswitch_rg_list,
    boolean_t verbose_flag, char *);

/*
 * scswitch_switch_device_service
 *	Switch device services to specified node.
 *
 *  Possible return values:
 *	SCSWITCH_NOERR 			- no error was found
 *	SCSWITCH_ENOCLUSTER		- node not a cluster member
 *	SCSWITCH_ECCR			- read CCR failed
 *	SCSWITCH_EINVAL			- invalid argument
 *	SCSWITCH_EINACTIVE 		- service is inactive
 *	SCSWITCH_ENODEID 		- invalid node
 *	SCSWITCH_EINVALSTATE		- object is in wrong state
 *	SCSWITCH_EREPLICA_FAILED 	- cluster membership changed during call
 *
 */
scswitch_errno_t
scswitch_switch_device_service(char *scswitch_node_list,
	char *scswitch_service_list);

/*
 * scswitch_restart_rg
 *	Restart RGs on the specified nodes.
 * verbose_flag is set for verbose output used by the newcli.
 *
 * Possible return values:
 *	scha_errmsg_t
 *
 */
scha_errmsg_t
scswitch_restart_rg(char *scswitch_node_list, char *scswitch_rg_list,
    boolean_t verbose_flag, char *);

/*
 * scswitch_cli_take_service_offline
 *	Take specified device group offline.
 *	This function is been called from the command scswitch(1M)
 *	with the argument of type char *
 *
 *  Possible return values:
 *	SCSWITCH_NOERR 		- no error was found
 *	SCSWITCH_ENOCLUSTER	- node not a cluster member
 *	SCSWITCH_ECCR		- read CCR failed
 *	SCSWITCH_EINVAL		- invalid argument
 *	SCSWITCH_EBUSY		- service is busy
 *
 */
scswitch_errno_t
scswitch_cli_take_service_offline(char *scswitch_service_list, int offline_flg);

/*
 * scswitch_take_resourcegrp_offline
 *	Take specified resource group offline.
 *	This function is been called from the command scswitch(1M)
 *	with the argument of type char *
 *
 *  Possible return values:
 *			scha_errmsg_t
 *
 */
scha_errmsg_t
scswitch_take_resourcegrp_offline(char *scswitch_rg_list,
    char *scswitch_node_list, switch_rg_action_t, boolean_t, char *);

/*
 * scswitch_quiesce_resourcegrp
 *	Quiesce resource group. If scswitch_rg_list is null, quiesce all RGs.
 *	scswitch_r_list is always passed as NULL for now. Wth kflg set,
 *	it kills the start methods of all resources in the RGs.
 *	This function is been called from the command scswitch(1M),
 *	and is for the newer quiesce and suspend i/f.
 * verbose_flag is set for verbose output used by the newcli.
 *
 * Possible return values:
 *			scha_errmsg_t
 *
 */
scha_errmsg_t
scswitch_quiesce_resourcegrp(char *scswitch_rg_list,
    char *scswitch_r_list, uint_t kflg, boolean_t verbose_flag, char *);

/*
 * scswitch_suspend_resume_resourcegrp
 *	According to the cmd_flg setting, perform suspend or resume
 *	operation to the RGs. *scswitch_r_list is always passed as NULL
 *	for now. If kflg is set, it suspend the RGs by killing all
 *	the running start methods of their resources.
 *
 * Possible return values:
 *		scha_errmsg_t
 */
scha_errmsg_t
scswitch_suspend_resume_resourcegrp(char *scswitch_rg_list,
    char *scswitch_r_list, uint_t cmd_flg, uint_t kflg, char *);

/*
 * scswitch_set_resource_switch
 *	Enable or disable resource.
 *	This function sets the on/off_switch to DISABLED or ENABLE of the
 *	given Resource.
 *
 * FBC 1403: Adding a new parameter nodelist to the function.
 *
 * verbose_flag is set for verbose output used by the newcli.
 *
 * Possible return values:
 *	scha_errmsg_t
 *
 */
scha_errmsg_t
scswitch_set_resource_switch(char *scswitch_resource_list, scha_switch_t rs,
    boolean_t flg, char *scswitch_node_list, boolean_t verbose_flag, char *);

/*
 * scswitch_set_monitor_switch
 *
 *	Enable or disable resource fault monitoring.
 *	This function sets the value of Monitored_switch of the resource
 *	to suppress or unsupress the invocation of MONITOR_INIT and
 *	MONITOR_START method of the given resource.
 *
 * FBC 1403: Adding a new parameter nodelist to the function.
 *
 * verbose_flag is set for verbose output used by the newcli.
 *
 * Possible return values:
 *	scha_errmsg_t
 *
 */
scha_errmsg_t
scswitch_set_monitor_switch(char *scswitch_resource_list, scha_switch_t rs,
    char *scswitch_node_list, boolean_t verbose_flag, char *);

/*
 * scswitch_change_rg_state
 *	Switch specified RGs from unmanaged state to offline state or
 *	from offline state to unmanaged state.
 *
 *	The precondition for switching a managed RG to the UNMANAGED state
 *	is that all Resources contained in the RG must be disabled.
 *
 * verbose_flag is set for verbose output used by the newcli.
 *
 * Possible return values:
 *	scha_errmsg_t
 *
 */
scha_errmsg_t
scswitch_change_rg_state(char *scswitch_rg_list,
	scha_scswitch_obj_state_t state, boolean_t verbose_flag, char *);

/*
 * scswitch_clear
 *	Clear error condition.
 *	(See the comments for scswitch_error_flag_t.)
 *
 * Possible return values:
 *	scha_errmsg_t
 *
 */
scha_errmsg_t
scswitch_clear(char *scswitch_node_list, char *scswitch_resource_list,
    scswitch_error_flag_t flag, char *);

/*
 * scswitch_alloc_service_list
 *
 * This function builds the list scswitch_service_t. Caller is responsible
 * for freeing the memory.
 *
 * Possible return values:
 *      SCSWITCH_NOERR
 *      SCSWITCH_EINVAL
 *      SCSWITCH_ENOMEM
 *
 */
scswitch_errno_t scswitch_alloc_service_list(scswitch_service_t **slist,
    char *service_list);

/*
 * scswitch_free_service_list
 *
 * Possible return values:
 *      NONE
 */
void
scswitch_free_service_list(scswitch_service_t *slist);

/*
 * scswitch_rgmerrmsg
 *
 * Function to obtain the scha error message for the scha error
 *
 * Returns :
 *	char * : Error Message
 *
 */
char *
scswitch_rgmerrmsg(scha_errmsg_t error);


/*
 * scswitch_getrglist
 *
 * This function returns a single list of all known resource group names.
 * Both managed and unmanaged RGs are returned in the single list.
 *
 * The structure of the new list is a NULL terminated array of pointers
 * to strings.  Upon success, a pointer to the list is placed
 * in "all_rgnamesp".   It is the caller's responsibility to
 * free all memory associated with the list.
 * If "zc_name", zone cluster name, is specified, then all the
 * resource group names will be pre-pended with the zone cluster name
 * in the format <zone cluster name>:<rg name>
 */
scha_errmsg_t
scswitch_getrglist(char **all_rgnamesp[], boolean_t only_local_rgs,
    char *zc_name);

/*
 * is_current_cluster_member()
 *
 * Input: nodename
 *
 * Action:
 *	determine if machine named by nodename is a current cluster member
 *
 * Returns: B_TRUE/B_FALSE
 *
 * Note that if scconf returns other than SCCONF_NOERR we return B_FALSE.
 *	If we're out of memory it will show up in other calls and be reported
 *	from them. This effectively masks SCCONF_EINVAL and SCCONF_EUNEXPECTED
 *	but this shouldn't really be a problem...
 */
boolean_t
is_current_cluster_member(char *nodename);


/*
 * scswitch_dup_list
 *
 * This function allocates memory and converts a comma-separated list into
 * a NULL-terminated string array.
 *
 * Possible return values:
 *	scha_errmsg_t
 *
 */
scha_errmsg_t
scswitch_dup_list(char **list[], char *node_list);


#ifdef __cplusplus
}
#endif

#endif /* _SCSWITCH_H */
