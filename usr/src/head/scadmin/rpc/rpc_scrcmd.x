/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * #pragma ident	"@(#)rpc_scrcmd.x	1.7	08/05/27 SMI"
 */

/*
 * RPC for support of remote cluster command
 */

#ifdef RPC_HDR
%#include <netdb.h>
%
%#define	SCRCMD_RPCSVC_CLOSEDOWN (5 * 60)	/* Closedown timeout */
%
%#define	SCRCMD_NODEID_FILE  "/etc/cluster/nodeid"
%#define	SCRCMD_REMOTE_FILE  "/etc/cluster/remoteconfiguration"
%
%#define	SCRCMD_CMD_STATUS_STRING	"SCRCMD_CMD_STATUS"
%
%/*
% * scrcmd error message strings
% */
%#define	SCRCMD_MSG_NOERR	"No error"
%#define	SCRCMD_MSG_EINVAL	"Usage error"
%#define	SCRCMD_MSG_ENOMEM	"Not enough memory"
%#define	SCRCMD_MSG_EEXIST	"Node is already configured as a cluster node"
%#define	SCRCMD_MSG_EPROTONOSUPPORT "Cannot create socket"
%#define	SCRCMD_MSG_ENOTACTIVE	"Node does not allow remote configuration"
%#define	SCRCMD_MSG_ETIMEDOUT	"Connection timed out"
%#define	SCRCMD_MSG_ECONNREFUSED	"Connection refused"
%#define	SCRCMD_MSG_ENOEXEC	"Command did not execute"
%#define	SCRCMD_MSG_EIO		"IO error"
%#define	SCRCMD_MSG_ECONNABORTED	"Connection aborted"
%
%/*
% * scrcmd exit codes
% */
%#define	SCRCMD_NOERR		0
%#define	SCRCMD_EINVAL		100
%#define	SCRCMD_ENOMEM		101
%#define	SCRCMD_EEXIST		102
%#define	SCRCMD_EPROTONOSUPPORT	103
%#define	SCRCMD_ENOTACTIVE	104
%#define	SCRCMD_ETIMEDOUT	105
%#define	SCRCMD_ECONNREFUSED	106
%#define	SCRCMD_ENOEXEC		107
%#define	SCRCMD_EIO		108
%#define	SCRCMD_ECONNABORTED	109
%#define	SCRCMD_UNKNOWN		110
%
%/*
% * scrcmd_cmd_t
% */
%typedef struct scrcmd_cmd {
%	char *cmd_name;
%	char *cmd_path;
%	u_int cmd_flags;
%} scrcmd_cmd_t; 
%
%/* cmd_flags */
%#define	SCRCMD_FLG_NODEID	(1 << 0)  /* Must be configured */
%#define	SCRCMD_FLG_NONODEID	(1 << 1)  /* Must NOT be configured */
%#define	SCRCMD_FLG_REMOTECONFIG	(1 << 2)  /* configfile must exist */
%#define	SCRCMD_FLG_INTERCONNECT	(1 << 3)  /* Private inter. ONLY */
%
%/* command list */
%#define	SCRCMD_CMDS						\
%	{								\
%		"test",							\
%		"/usr/cluster/lib/scadmin/lib/cmd_test",		\
%		(SCRCMD_FLG_REMOTECONFIG)				\
%	},								\
%	{								\
%		"reboot",						\
%		"/usr/cluster/lib/scadmin/lib/cmd_reboot",		\
%		(SCRCMD_FLG_REMOTECONFIG)				\
%	},								\
%	{								\
%		"install",						\
%		"/usr/cluster/lib/scadmin/lib/cmd_install",		\
%		(SCRCMD_FLG_NONODEID | SCRCMD_FLG_REMOTECONFIG)		\
%	},								\
%	{								\
%		"chk_globaldev",					\
%		"/usr/cluster/lib/scadmin/lib/cmd_chk_globaldev",	\
%		(SCRCMD_FLG_NONODEID | SCRCMD_FLG_REMOTECONFIG)		\
%	},								\
%	{								\
%		"sccheck",						\
%		"/usr/cluster/lib/scadmin/lib/cmd_sccheck",		\
%		(SCRCMD_FLG_NONODEID | SCRCMD_FLG_REMOTECONFIG)		\
%	},								\
%	{								\
%		"autodiscovery",					\
%		"/usr/cluster/lib/scadmin/lib/cmd_autodiscovery",	\
%		(SCRCMD_FLG_NONODEID | SCRCMD_FLG_REMOTECONFIG)		\
%	},								\
%	{								\
%		"scprivipadm",					\
%		"/usr/cluster/lib/scadmin/lib/cmd_scprivipadm",	\
%		(SCRCMD_FLG_NODEID)		\
%	},								\
%	{								\
%		"modifyip",					\
%		"/usr/cluster/lib/scadmin/lib/cmd_modifyip",	\
%		(SCRCMD_FLG_NODEID)		\
%	},								\
%       {                                                               \
%               "showrev",						\
%		"/usr/cluster/lib/scadmin/lib/cmd_showrev",		\
%               (0)							\
%       },                                                              \
%	{								\
%		"snmp",							\
%		"/usr/cluster/bin/sceventmib",				\
%		(SCRCMD_FLG_NODEID | SCRCMD_FLG_INTERCONNECT)		\
%	},								\
%	{								\
%		"scdidadm",						\
%		"/usr/cluster/bin/scdidadm",				\
%		(SCRCMD_FLG_NODEID | SCRCMD_FLG_INTERCONNECT)		\
%	},								\
%	{								\
%		"administerzone",					\
%		"/usr/sbin/zoneadm",					\
%		(SCRCMD_FLG_NODEID)					\
%	},								\
%	{								\
%		"clzonehelper",						\
%		"/usr/cluster/lib/sc/zc_helper",			\
%		(SCRCMD_FLG_NODEID)					\
%	},								\
%	{ NULL, NULL, 0 }
%
%/*
% * Count of running command threads, to avoid closedown.
% */
%extern int do_command_threads;
%extern mutex_t do_command_threads_lock;
%
#endif

typedef int sc_errno_t;
typedef string sc_name_t<>;
typedef sc_name_t sc_namelist_t<>;
typedef sc_namelist_t sc_errlist_t;
typedef unsigned int sc_port_t;
typedef string sc_env_t<>;
typedef sc_env_t sc_envlist_t<>;

/*
 * Return values
 */
struct sc_result {
	sc_errno_t	sc_res_errno;
	sc_errlist_t	sc_res_errlist;
};

/*
 * The program and procedures
 */
program SCRCMDPROG {
	version SCRCMDVERS {
		sc_result SCRCMDPROC_DO_CMD(sc_port_t outport,
		    sc_port_t errport, string command_name<>,
		    string command_args<>, sc_envlist_t envlist,
		    string buffer<>) = 1;
	} = 1;
} = 100533;

#ifdef RPC_HDR
%/* Name of RPC service, as identified in rpc(4) */
%#define	SCRCMDPROGNAME	"scrcmd"
%
%#if defined(__STDC__) || defined(__cplusplus)
%extern void scrcmdprog_1(struct svc_req *, register SVCXPRT *);
%
%#else /* K&R C */
%extern void scrcmdprog_1();
%
%#endif /* K&R C */
%
#endif
