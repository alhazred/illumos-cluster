/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/* #pragma ident	"@(#)rpc_scadmd.x	1.25	08/05/20 SMI"	*/

/*
 * RPC for cluster administration.
 */

#ifdef RPC_HDR
%#include <scadmin/scconf.h>
%#include <netdb.h>
%
%#define	FILEP	FILE *
%#define      SCCONF_RPCSVC_CLOSEDOWN (5 * 60)        /* Closedown timeout */
%#define      SCCONF_RPCSVC_TOKENTO (2 * 60)          /* Boot token timeout */
%
%/*
% * Permission-to-Boot token
% *
% * The _boot_token is set to the node ID of the current owner.
% * The token is protected by the _boot_token_lock.
% *
% * After SCCONF_RPCSVC_TOKENTO seconds, the boot token is automatically
% * cleared.  Programs which use the boot token should immediately halt
% * or reboot using halt(1M), reboot(1M), or uadmin(1M);  programs should
% * NOT use shutdown(1M) or init(1M), since shutdown time could exceed
% * the SCCONF_RPCSVC_TOKENTO value.
% */
%extern scconf_nodeid_t _boot_token;	/* permission-to-boot token */
%extern mutex_t _boot_token_lock;	/* lock for token */
%
%extern void *boot_token_timeout(void *arg);
%
#endif

/*
 * The following rpcgen typedefs all have C equivalents defined in
 * scconf.h:
 *
 *	rpc_scadmd.x (rpcgen)			scconf.h (C)
 *
 *	  sc_errno_t				  scconf_errno_t
 *	  sc_authtype_t				  scconf_authtype_t
 *	  sc_epoint_type_t			  scconf_epoint_type_t
 *	  sc_portid_t				  scconf_portid_t
 *	  sc_portstate_t			  scconf_portstate_t
 *	  sc_cltr_epoint_t			  scconf_cltr_epoint_t
 *	  sc_nodeid_t				  scconf_nodeid_t
 *
 *	rpc_scadmd.x (rpcgen)			sys/type.h (C)
 *
 *        sc_major_t                              major_t
 *        sc_minor_t                              minor_t
 *
 */
typedef int sc_errno_t;
typedef int sc_authtype_t;
typedef int sc_epoint_type_t;
typedef int sc_portid_t;
typedef int sc_portstate_t;
typedef int sc_state_t;
typedef unsigned long sc_major_t;
typedef unsigned long sc_minor_t;
typedef string sc_dsnode_t<>;
typedef sc_dsnode_t sc_dsnodes_t<>;
typedef unsigned int sc_nodeid_t;
typedef unsigned int sc_uint_t;
typedef string sc_name_t<>;
typedef sc_name_t sc_namelist_t<>;

struct sc_cltr_epoint {
	sc_epoint_type_t	sc_epoint_type;
	string			sc_epoint_nodename<>;
	string			sc_epoint_devicename<>;
	string			sc_epoint_portname<>;
	sc_portid_t		sc_epoint_portid;
	sc_portstate_t		sc_epoint_portstate;
};
typedef struct sc_cltr_epoint sc_cltr_epoint_t;

struct sc_gdev_range_entry {
	sc_major_t		sc_range_major_no;
	sc_minor_t		sc_range_start_minor;
	sc_minor_t		sc_range_end_minor;
};
typedef struct sc_gdev_range_entry sc_gdev_range_entry_t;
typedef sc_gdev_range_entry_t sc_gdev_range_t<>;

struct sc_cfg_prop_entry {
	string			sc_prop_key<>;
	string			sc_prop_value<>;
};
typedef struct sc_cfg_prop_entry sc_cfg_prop_entry_t;
typedef sc_cfg_prop_entry_t sc_cfg_prop_t<>;

/*
 * Return values
 */
struct sc_result {
	sc_errno_t	sc_res_errno;
};

struct sc_result_addnode {
	sc_errno_t	sc_res_errno;
	string		sc_res_busynode<>;
};

struct sc_result_authtype {
	sc_errno_t	sc_res_errno;
	sc_authtype_t	sc_res_authtype;
};

struct sc_result_ismember {
	sc_errno_t	sc_res_errno;
	unsigned int	sc_res_ismember;
};

struct sc_result_cname {
	sc_errno_t	sc_res_errno;
	string		sc_res_cname<>;
};

struct sc_result_file {
	sc_errno_t	sc_res_errno;
	FILEP		sc_res_fp;
};

struct sc_result_adapters {
	sc_namelist_t	sc_res_adapters;
};

struct sc_result_trconfig {
	sc_errno_t	sc_res_errno;
	sc_namelist_t	sc_res_trconfig;
};

struct sc_result_snoop {
	int		sc_res_result;
	sc_namelist_t	sc_res_discovered;
	sc_namelist_t	sc_res_errlist;
};

struct sc_result_autodiscover {
	int		sc_res_result;
	sc_namelist_t	sc_res_discovered;
	sc_namelist_t	sc_res_errlist;
};

struct sc_result_removenode {
	sc_errno_t	sc_res_errno;
	sc_namelist_t	sc_res_errlist;
};

struct sc_result_major_number {
	sc_errno_t	sc_res_errno;
	sc_namelist_t	sc_res_major_numbers;
};

struct sc_result_ds {
	sc_errno_t	sc_res_errno;
	string		sc_res_errmsg<>;
};

struct sc_vm_stream {
        opaque data<>;
};

struct sc_node_streams {
        sc_vm_stream      sc_streams_by_type<>;
};

struct sc_result_cluster_vm {
	/* RPC call result */
	sc_errno_t	sc_res_errno;
	/* Info from CMM */
	unsigned hyper int    sc_current_seq;
	unsigned hyper int    sc_upgrade_seq;
	unsigned int    sc_members<>;
	/* Quorum info */
	unsigned int	sc_current_votes;
	unsigned int	sc_votes_needed_for_quorum;
	/* VM streams info */
        sc_node_streams sc_nodes_table<>;
};

struct sc_result_zonelist {
	sc_errno_t	sc_res_errno;
	sc_namelist_t	sc_res_zonelist;
};

/*
 * The program and procedures
 */
program SCADMPROG {
	version SCADMVERS {
		sc_result_addnode SCADMPROC_ADD_NODE(string nodename<>) = 1;
		sc_result SCADMPROC_ADD_CLTR_ADAPTER(string nodename<>,
		    string transport_type<>, string adaptername<>, int vlanid,
		    string properties<>) = 2;
		sc_result SCADMPROC_ADD_CLTR_CPOINT(string cpoint_type<>,
		    string cpointname<>, string properties<>) = 3;
		sc_result SCADMPROC_ADD_CLTR_CABLE(
		    sc_cltr_epoint_t *epoint1, sc_cltr_epoint_t *epoint2) = 4;
		sc_result_authtype SCADMPROC_GET_AUTHTYPE(void) = 5;
		sc_result_ismember SCADMPROC_GET_ISMEMBER(void) = 6;
		sc_result_cname	SCADMPROC_GET_CLUSTERNAME(void) = 7;
		sc_result_file SCADMPROC_COPY_FROM(string srcfile<>) = 8;
		sc_result SCADMPROC_REMOVE_FILE(string filename<>,
		    string nodename<>) = 9;
		sc_result_ds SCADMPROC_ADD_DS (string dstype<>,
		    string dsname<>, sc_dsnodes_t dsnodes,
		    sc_state_t dspreference, sc_gdev_range_t dsdevices,
		    sc_state_t dsfailback, sc_cfg_prop_t prop_list,
		    string dsoptions<>, sc_uint_t dsnumsecondaries) = 10;
		sc_result_ds SCADMPROC_CHANGE_DS (string dsname<>,
		    sc_dsnodes_t dsnodes, sc_state_t dspreference,
		    sc_gdev_range_t dsdevices, sc_state_t dsfailback,
		    sc_cfg_prop_t prop_list, string dsoptions<>,
		    sc_uint_t dsnumsecondaries) = 11;
		sc_result SCADMPROC_GET_BOOTTOKEN(sc_nodeid_t nodeid) = 12;
		sc_result_adapters SCADMPROC_GET_NETWORK_ADAPTERS(
		    sc_uint_t flag, string adaptypes<>) = 13;
		sc_result_trconfig SCADMPROC_TRANSPORT_CONFIG(void) = 14;
		void SCADMPROC_BROADCAST_PING(void) = 15;
		sc_result_snoop SCADMPROC_SNOOP(string adapters<>,
		    sc_uint_t discover_options, sc_uint_t recvto) = 16;
		sc_result_autodiscover SCADMPROC_AUTODISCOVER(
		    string adapters<>, string vlans<>, string filename<>,
		    sc_uint_t discover_options, sc_uint_t sendcount,
		    sc_uint_t recvto, sc_uint_t waitcount, string token<>) = 17;
		sc_result_removenode SCADMPROC_REMOVE_NODE(
		    string node_to_be_removed<>, sc_uint_t flag) = 18;
		sc_result_major_number SCADMPROC_GET_MAJOR_NUMBER(
		    string driver<>) = 19;
		sc_result_cluster_vm SCADMPROC_GET_CLUSTER_VM_INFO(void) = 20;
                sc_result SCADMPROC_UPDATE_NTP(void) = 21;
		sc_result SCADMPROC_UPDATE_HOSTS(void) = 22;
		sc_result_zonelist SCADMPROC_GET_ZONELIST() = 23;
	} = 1;
} = 100145;

#ifdef RPC_HDR
%/* Name of RPC service, as identified in rpc(4) */
%#define	SCADMPROGNAME	"scadmd"
%
%#if defined(__STDC__) || defined(__cplusplus)
%extern void scadmprog_1(struct svc_req *, register SVCXPRT *);
%
%extern bool_t xdr_FILEP(register XDR *xdrs, FILE **fpp);
%
%#else /* K&R C */
%extern void scadmprog_1();
%
%extern bool_t xdr_FILEP();
%
%#endif /* K&R C */
%
#endif
