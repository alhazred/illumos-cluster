/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SCSYMON_SRV_H_
#define	_SCSYMON_SRV_H_

#pragma ident	"@(#)scsymon_srv.h	1.16	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <rgm/rgm_scrgadm.h>
#include <rgm/rgm_util.h>
#include <scadmin/scconf.h>
#include <scadmin/scstat.h>

#define	SCSYMON_DOOR_FILE "/var/cluster/run/scsymon_door"
#define	SCSYMON_MAX_STRING_LEN 1024

/* define string constant */
#define	SCSYMON_LIST_DELIMITER ","
#define	SCSYMON_LIST_HEAD ""
#define	SCSYMON_NOT_APPLIED "N/A"
#define	SCSYMON_ALL "<ALL>"
#define	SCSYMON_NULL "<NULL>"
#define	SCSYMON_TRUE "true"
#define	SCSYMON_FALSE "false"
#define	SCSYMON_UNKNOWN "unknown"
#define	SCSYMON_GENERIC "generic"
#define	SCSYMON_STRING "string"
#define	SCSYMON_INT "int"
#define	SCSYMON_BOOLEAN "boolean"
#define	SCSYMON_ENUM "enum"
#define	SCSYMON_STRING_ARRAY "string array"
#define	SCSYMON_ALL_POTENTIAL_MASTERS "all potential masters"
#define	SCSYMON_LOGICAL_HOSTNAME "logical hostname"
#define	SCSYMON_SHARED_ADDRESS "shared address"
#define	SCSYMON_NOT_TUNABLE "not tunable"
#define	SCSYMON_AT_CREATION "at creation"
#define	SCSYMON_ANYTIME "anytime"
#define	SCSYMON_WHEN_DISABLED "when disabled"
#define	SCSYMON_OFFLINE "Offline"
#define	SCSYMON_LOGICAL_HOSTNAME_RT "SUNW.LogicalHostname"
#define	SCSYMON_SHARED_ADDRESS_RT "SUNW.SharedAddress"
#define	SCSYMON_SCALABLE "Scalable"
#define	SCSYMON_NETIF_LIST "NetIfList"
#define	SCSYMON_HOSTNAME_LIST "HostnameList"
#define	SCSYMON_AUX_NODE_LIST "AuxNodeList"
#define	SCSYMON_SHARED_ADDRESSES_USED "Shared_Addresses_Used"
#define	SCSYMON_LB_POLICY "Load_Balancing_Policy"
#define	SCSYMON_LB_STRING "Load_Balancing_String"
#define	SCSYMON_CONFDIR_LIST "Confdir_list"
#define	SCSYMON_PROTOCOL_FAMILY "Protocol_Family"
#define	SCSYMON_PORT_LIST "Port_List"
#define	SCSYMON_MONITOR_RETRY_COUNT "Monitor_retry_count"
#define	SCSYMON_MONITOR_RETRY_INTERVAL "Monitor_retry_interval"
#define	SCSYMON_PROBE_TIMEOUT "Probe_timeout"
#define	SCSYMON_AUTH_SYS "unix"
#define	SCSYMON_AUTH_DH "Diffie-Hellman"
#define	SCSYMON_TIMEOUT_SUFFIX "_TIMEOUT"
#define	SCSYMON_ENOERR_TEXT		"no error"
#define	SCSYMON_EUSAGE_TEXT		"usage error"
#define	SCSYMON_EPERM_TEXT		"not root"
#define	SCSYMON_ENOTCLUSTER_TEXT	"not a cluster node"
#define	SCSYMON_ENOTCONFIGURED_TEXT	"not found in CCR"
#define	SCSYMON_ENOMEM_TEXT		"not enough memory"
#define	SCSYMON_EINVAL_TEXT		"invalid argument"
#define	SCSYMON_ESERVICENAME_TEXT	"invalid device group name"
#define	SCSYMON_ECLUSTERRECONFIG_TEXT	"cluster is reconfiguring"
#define	SCSYMON_ERGRECONFIG_TEXT	"RG is reconfiguring"
#define	SCSYMON_EOBSOLETE_TEXT		"Resource/RG has been updated"
#define	SCSYMON_EDOOROPEN_TEXT		"Unable to open door"
#define	SCSYMON_EDOORCALL_TEXT		"Unable to make door call"
#define	SCSYMON_EDOORCREATE_TEXT	"Unable to create door"
#define	SCSYMON_EDOORBIND_TEXT		"Unable to bind door"
#define	SCSYMON_EORBINIT_TEXT		"Unable to initialize ORB"
#define	SCSYMON_EMKDOORDIR_TEXT		"Unable to create door directory"
#define	SCSYMON_ECREATEDOORFILE_TEXT	"Unable to create door file"
#define	SCSYMON_EFORK_TEXT		"error fork child process"
#define	SCSYMON_EOPENDEVCONSOLE_TEXT	"error open /dev/console"
#define	SCSYMON_ESETSID_TEXT		"error set session leader"
#define	SCSYMON_EUNEXPECTED_TEXT	"internal or unexpected error"
#define	SCSYMON_EUNKNOWN_TEXT		"unknown error"

/* Error codes returned by scsymon functions. */
typedef enum scsymon_errno {
	SCSYMON_ENOERR,		/* no error */
	SCSYMON_EUSAGE,		/* usage error */
	SCSYMON_EPERM,		/* not root */
	SCSYMON_ENOTCLUSTER,	/* not a cluster node */
	SCSYMON_ENOTCONFIGURED,	/* not found in CCR */
	SCSYMON_ENOMEM,		/* not enough memory */
	SCSYMON_EINVAL,		/* invalid argument */
	SCSYMON_ESERVICENAME,	/* invalid device group name */
	SCSYMON_ECLUSTERRECONFIG, /* cluster is reconfiguring */
	SCSYMON_ERGRECONFIG,	/* RG is reconfiguring */
	SCSYMON_EOBSOLETE,	/* Resource/RG has been updated */
	SCSYMON_EDOOROPEN,	/* unable to open door */
	SCSYMON_EDOORCALL,	/* unable to make door call */
	SCSYMON_EDOORCREATE,	/* unable to create door */
	SCSYMON_EDOORBIND,	/* unable to bind door */
	SCSYMON_EORBINIT,	/* error initialize ORB */
	SCSYMON_EMKDOORDIR,	/* error create door directory */
	SCSYMON_ECREATEDOORFILE, /* error create door file */
	SCSYMON_EFORK,		/* error fork child process */
	SCSYMON_EOPENDEVCONSOLE, /* error open /dev/console */
	SCSYMON_ESETSID,	/* error set session leader */
	SCSYMON_EUNEXPECTED,	/* internal or unexpected error */
	SCSYMON_EUNKNOWN	/* unknown error */
} scsymon_errno_t;

/* RG type */
typedef enum scsymon_rg_type {
	SCSYMON_FAILOVER_RG = 0x1,		/* failover RG */
	SCSYMON_SCALABLE_RG = 0x2,		/* scalable RG */
	SCSYMON_ANY_RG = 0x3			/* any RG */
} scsymon_rg_type_t;

/* Resource property type */
typedef enum scsymon_rs_prop_type {
	SCSYMON_RS_COM_PROP,		/* standard property */
	SCSYMON_RS_EXT_PROP,		/* extended property */
	SCSYMON_RS_TIMEOUT_PROP		/* timeout */
} scsymon_rs_prop_type_t;

/* cluster properties */
typedef struct cluster_config_struct {
	char *pname;		/* cluster name */
	char *pinstall_mode;	/* install mode */
	char *pprivnetaddr;	/* private net */
	char *pprivnetmask;	/* private net mask */
	char *pauthtype;	/* add node auth Type */
	char *pauthlist;	/* add node auth List */
} scsymon_cluster_config_t;

/* cluster status */
typedef struct cluster_status_struct {
	char *pname;		/* cluster name */
	unsigned int min_vote;	/* minimum votes required */
	unsigned int current_vote; /* current votes */
} scsymon_cluster_status_t;

/* node properties */
typedef struct node_config_struct {
	char *pname;		/* node name */
	unsigned int default_vote; /* votes configured */
	char *pprivnethostname;	/* private net hostname */
	char *padapterlist;	/* transport adapter list */
	struct node_config_struct *pnext;
} scsymon_node_config_t;

/* node status */
typedef struct node_status_struct {
	char *pname;		/* node name */
	char *pstate;		/* status */
	unsigned int current_vote; /* current votes */
	struct node_status_struct *pnext;
} scsymon_node_status_t;

/* properties of device associated with a node */
typedef struct node_device_config_struct {
	char *pname;		/* node name */
	char *pqdevlist;	/* quorum device list */
	char *pdevgrplist;	/* possible mastered device group list */
	char *prglist;		/* possible mastered RG list */
	struct node_device_config_struct *pnext;
} scsymon_node_device_config_t;

/* status of device associated with a node */
typedef struct node_device_status_struct {
	char *pname;		/* node name */
	char *pdevgrplist;	/* mastered device group list */
	char *prglist;		/* mastered RG list */
	struct node_device_status_struct *pnext;
} scsymon_node_device_status_t;

/* device group properties */
typedef struct devgrp_config_struct {
	char *pname;		/* device group name */
	char *pservice_type;	/* service type */
	int failback_enabled;	/* failback enabled */
	char *pnodelist;	/* node list  */
	int node_order;		/* nodes are ordered by preference */
	char *pdevicelist;	/* device list */
	struct devgrp_config_struct *pnext;
} scsymon_devgrp_config_t;

/* device group status */
typedef struct devgrp_status_struct {
	char *pname;		/* device group name */
	char *pstate;		/* status */
	char *pprimarylist;		/* primary list */
	struct devgrp_status_struct *pnext;
} scsymon_devgrp_status_t;

/* device group replica status */
typedef struct replica_status_struct {
	char *pdevgrp_name;	/* device group name */
	char *pnode_name;	/* node name */
	char *pstate;		/* replica status */
	struct replica_status_struct *pnext;
} scsymon_replica_status_t;

/* quorum device properties */
typedef struct qdev_config_struct {
	char *pname;		/* quorum device name */
	char *ppath;		/* device path */
	int enabled;		/* is enabled */
	unsigned int default_vote; /* votes configured */
	char *pportlist;	/* port list */
	struct qdev_config_struct *pnext;
} scsymon_qdev_config_t;

/* quorum device status */
typedef struct qdev_status_struct {
	char *pname;		/* quorum device name */
	char *pstate;		/* status */
	unsigned int current_vote; /* current votes */
	struct qdev_status_struct *pnext;
} scsymon_qdev_status_t;

/* quorum device port properties */
typedef struct qdevport_config_struct {
	char *pname;		/* quorum device name */
	char *pnode;		/* quorum device host */
	int enabled;		/* is enabled */
	struct qdevport_config_struct *pnext;
} scsymon_qdevport_config_t;

/* transport path status */
typedef struct path_status_struct {
	char *padapter1;	/* endpoint 1 */
	char *padapter2;	/* endpoint 2 */
	char *pstate;		/* path status */
	struct path_status_struct *pnext;
} scsymon_path_status_t;

/* transport adapter properties */
typedef struct transport_adapter_config_struct {
	char *pnode;		/* node name */
	char *padapter;		/* adapter name */
	char *ptype;		/* adapter type */
	int enabled;		/* enabled or not */
	struct transport_adapter_config_struct *pnext;
} scsymon_transport_adapter_config_t;

/* transport junction properties */
typedef struct junction_config_struct {
	char *pname;		/* junction name */
	char *ptype;		/* junction type */
	int enabled;		/* enabled or not */
	char *pportlist;	/* port list */
	struct junction_config_struct *pnext;
} scsymon_junction_config_t;

/* transport cable properties */
typedef struct cable_config_struct {
	char *pend_type1;	/* endpoint1 type */
	char *pend1;		/* endpoint1 */
	char *pend_type2;	/* endpoint2 type */
	char *pend2;		/* endpoint2 */
	int enabled;		/* enabled or not */
	struct cable_config_struct *pnext;
} scsymon_cable_config_t;

/* RT properties */
typedef struct rt_config_struct {
	char *pname;		/* RT name */
	char *pinstalled_nodes;	/* installed node list */
	char *pdesc;		/* description */
	char *pbasedir;		/* RT base directory */
	int single_inst;	/* single instance or not */
	char *pinit_nodes;	/* init node list */
	int failover;		/* faiover or not */
	int proxy;		/* proxy or not */
	char *psysdeftype;	/* logical hostname, shared address or none */
	char *pdepend;		/* RT dependencies */
	unsigned int api_version; /* api version */
	char *pversion;		/* RT version */
	char *ppkglist;		/* packages comprise the RT */
	struct rt_config_struct *pnext;
} scsymon_rt_config_t;

/* RT method pathnames */
typedef struct rt_method_config_struct {
	char *pname;		/* RT name */
	char *pstart;		/* start method pathname */
	char *pstop;		/* stop method pathname */
	char *pprim_change;	/* primary_change method pathname */
	char *pvalidate;	/* validate method pathname */
	char *pupdate;		/* update method pathname */
	char *pinit;		/* init method pathname */
	char *pfini;		/* fini method pathname */
	char *pboot;		/* boot method pathname */
	char *pmon_init;	/* monitor_init method pathname */
	char *pmon_start;	/* monitor_start method pathname */
	char *pmon_stop;	/* monitor_stop method pathname */
	char *pmon_check;	/* monitor_check method pathname */
	char *ppre_net_start;	/* pre_net_start method pathname */
	char *ppost_net_stop;	/* post_net_stop method pathname */
	struct rt_method_config_struct *pnext;
} scsymon_rt_method_config_t;

/* RT param table */
typedef struct rt_param_config_struct {
	char *prt_name;	/* RT name */
	char *pname;	/* property name */
	int ext;	/* extenstion or not */
	int per_node;	/* per-node property or not */
	char *ptunable;	/* NONE, CREATION_TIME or RUN_TIME */
	char *ptype;	/* STRING, BOOL, INT, ENUM ... */
	char *pdefault;	/* default value */
	char *pmin; 	/* min INT type, minlen for STRING and STRINGARRY */
	char *pmax; 	/* max INT type, maxlen for STRING and STRINGARRY */
	char *parraymin; /* minimum array size for STRINGARRAY type */
	char *parraymax; /* maximum array size for STRINGARRAY type */
	char *pdesc;	/* description */
	struct rt_param_config_struct *pnext;
} scsymon_rt_param_config_t;

/* RT resource table */
typedef struct rt_rs_config_struct {
	char *prt_name;	/* RT name */
	char *prg_name;	/* RG name */
	char *prslist;	/* Resource list */
	struct rt_rs_config_struct *pnext;
} scsymon_rt_rs_config_t;

/* RG properties */
typedef struct rg_config_struct {
	char *pname;		/* RG name */
	char *pprimaries;	/* primary list */
	char *pdesc;		/* description */
	int maxPrim;		/* maximum primaries */
	int desPrim;		/* desired primaries */
	int failback;		/* failback or not */
	int net_depend;		/* implicit network dependency */
	char *pRG_depend;	/* RG dependencies */
	char *pglobal_res_used;	/* global resources used */
	char *ppath_prefix;	/* path prefix */
	char *prslist;		/* resource list */
	struct rg_config_struct *pnext;
} scsymon_rg_config_t;

/* RG status */
typedef struct rg_status_struct {
	char *pname;		/* RG name */
	char *pprimary;		/* primary node name */
	char *pprimary_state;	/* primary state */
	char *prg_state;	/* RG state on the node */
	struct rg_status_struct *pnext;
} scsymon_rg_status_t;

/* Resource configuration */
typedef struct rs_config_struct {
	char *prg_name;			/* RG name */
	char *prs_name;			/* Resource name */
	char *ptype;			/* RT name */
	char *on_off_switch;		/* On/Off switch */
	char *monitored_switch;		/* Monitored switch */
	char *pstrong_depend;		/* Resource strong dependencies */
	char *pweak_depend;		/* Resource weak dependencies */
	struct rs_config_struct *pnext;
} scsymon_rs_config_t;

/* Resource status */
typedef struct rs_status_struct {
	char *prg_name;		/* RG name */
	char *prs_name;		/* Resource name */
	char *pprimary;		/* primary node name */
	char *pprimary_state;	/* primary state */
	char *pfm_state;	/* FM reported state */
	char *prgm_state;	/* RGM reported state */
	struct rs_status_struct *pnext;
} scsymon_rs_status_t;

/* Resource properties */
typedef struct rs_prop_config_struct {
	char *prg_name;		/* RG name */
	char *prs_name;		/* Resource name */
	char *pprop_name;	/* property name */
	char *pprop_value;	/* property value */
	char *pprop_desc;	/* property description */
	struct rs_prop_config_struct *pnext;
} scsymon_rs_prop_config_t;

/* RGM Resource Type properties */
typedef struct scsymon_rgm_rt_struct {
	rgm_rt_t *prgm_rt;
	struct scsymon_rgm_rt_struct *pnext;
} scsymon_rgm_rt_t;

/* RGM Resource properties */
typedef struct scsymon_rgm_rs_struct {
	rgm_resource_t *prgm_rs;
	struct scsymon_rgm_rs_struct *pnext;
} scsymon_rgm_rs_t;

/* RGM Resource Group properties including Resource properties */
typedef struct scsymon_rgm_rg_rs_struct {
	rgm_rg_t *prgm_rg;
	scsymon_rgm_rs_t *plrss;
	struct scsymon_rgm_rg_rs_struct *pnext;
} scsymon_rgm_rg_rs_t;

/*
 * scsymon_strerr
 *
 * Map scsymon_errno_t to a string.
 *
 * The supplied "text" should be of at least SCSYMON_MAX_STRING_LEN
 * in length.
 */
extern void scsymon_strerr(scsymon_errno_t err, char *text);

/*
 * scsymon_convert_scconf_error_code
 *
 * convert a scconf error code to scsymon error code
 *
 * Possible return values:
 *
 *	SCSYMON_NOERR		- success
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
extern scsymon_errno_t
scsymon_convert_scconf_error_code(const scconf_errno_t scconf_error);

/*
 * scsymon_convert_scstat_error_code
 *
 * convert a scstat error code to scsymon error code
 *
 * Possible return values:
 *
 *      SCSYMON_ENOMEM           - not enough memory
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ESERVICENAME	- invalid device group name
 *	SCSYMON_EINVAL		- scconf: invalid argument
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ECLUSTERRECONFIG	- cluster is reconfiguring
 *	SCSYMON_ERGRECONFIG	- RG is reconfiguring
 *	SCSYMON_EOBSOLETE	- Resource/RG has been updated
 *      SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
extern scsymon_errno_t
scsymon_convert_scstat_error_code(const scstat_errno_t scstat_error);

/*
 * scsymon_dbg_print
 *
 * If DEBUG is defined, print the arguments in specified format to stdout.
 * Otherwise, do nothing.
 */
extern void scsymon_dbg_print(char *format, ...);

/*
 * scsymon_delete_newline
 *
 * Delete all newline characters from the string buffer. It returns the
 * same buffer with the newline charactors removed from it
 *
 */
extern const char *scsymon_delete_newline(const char *pbuf);

/*
 * scsymon_double_buffer_size
 *
 * double the size of the buffer passed in.
 *
 * Upon success, a buf objects of scsymon_rt_method_config_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_rt_method_configs().
 *
 * INPUT:	buf_size - input buffer size
 *		pbuf - input buffer address
 * OUTPUT:	psize_new - pointer to output buffer size
 *		ppbuf_new - pointer to output buffer address
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *
 */
extern scsymon_errno_t
scsymon_double_buffer_size(const unsigned int buf_size, char *pbuf,
    unsigned int *psize_new, char **ppbuf_new);

/*
 * scsymon_free_name_array
 *
 * Free a char array.
 *
 */
extern void
scsymon_free_name_array(char **pplnames);

/*
 * scsymon_get_rg_type
 *
 * Return RG type of the specified RG object.
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
extern scsymon_errno_t scsymon_get_rg_type(rgm_rg_t *prgm_rg,
    scsymon_rg_type_t *ptype);

/*
 * scsymon_get_rgm_rts
 *
 * Call rgm_scrgadm interface to get properties for all Resource Types.
 *
 * Upon success, a list of objects of scsymon_rgm_rt_t is returned.
 * The caller is responsible for freeing the array using
 * scsymon_free_rgm_rts().
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
extern scsymon_errno_t
scsymon_get_rgm_rts(scsymon_rgm_rt_t **pplrts);

/*
 * scsymon_free_rgm_rts
 *
 * free Resource Type properties
 */
extern void
scsymon_free_rgm_rts(scsymon_rgm_rt_t *plrts);

/*
 * scsymon_get_rgm_rg_rss
 *
 * Call rgm_scrgadm interface to get properties for all Resource Groups
 * as well as the Resources for every Resource Group.
 *
 * Upon success, a list of objects of scsymon_rgm_rg_rs_t is returned.
 * The caller is responsible for freeing the array using
 * scsymon_free_rgm_rg_rss().
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
extern scsymon_errno_t
scsymon_get_rgm_rg_rss(scsymon_rgm_rg_rs_t **pplrgs);

/*
 * scsymon_free_rgm_rg_rss
 *
 * free properties for Resource Group as well the Resources in each
 * Resource Group.
 */
extern void
scsymon_free_rgm_rg_rss(scsymon_rgm_rg_rs_t *plrgs);

/*
 * scsymon_get_rgm_rss
 *
 * Call rgm_scrgadm interface to get properties for all Resources
 * for a particular Resource Group.
 *
 * Upon success, a list of objects of scsymon_rgm_rs_t is returned.
 * The caller is responsible for freeing the array using
 * scsymon_free_rgm_rss().
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
extern scsymon_errno_t
scsymon_get_rgm_rss(char *rg_name, scsymon_rgm_rs_t **pplrss);

/*
 * scsymon_free_rgm_rss
 *
 * free properties for Resources.
 */
extern void
scsymon_free_rgm_rss(scsymon_rgm_rs_t *plrss);

/*
 * scsymon_get_node_status
 *
 * Call scha interface to get node status.  The supplied "pstatus" should
 * be of at least SCSTAT_MAX_STRING_LEN.
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
extern scsymon_errno_t
scsymon_get_node_status(char *node_name, char *pstatus);

/*
 * scsymon_get_rg_type_by_name
 *
 * Call scha interface to get RG type.
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
extern scsymon_errno_t
scsymon_get_rg_type_by_name(char *rg_name, scsymon_rg_type_t *ptype);

/*
 * scsymon_get_rt_names
 *
 * Get names of all RTs registered for the cluster. The result name array
 * must be freed using scsymon_free_name_array().
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_ENOMEM		- not enough memory
 *
 */
extern scsymon_errno_t
scsymon_get_rt_names(char ***ppplnames);

/*
 * scsymon_get_rg_names
 *
 * Get names of all RGs configured for the cluster. The result name array
 * must be freed using scsymon_free_name_array().
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_ENOMEM		- not enough memory
 *
 */
extern scsymon_errno_t
scsymon_get_rg_names(char ***ppplnames);

/*
 * scsymon_get_rs_names
 *
 * Get names of all RSs configured for the RG. The result name array
 * must be freed using scsymon_free_name_array().
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_ENOMEM		- not enough memory
 *
 */
extern scsymon_errno_t
scsymon_get_rs_names(char *rg_name, char ***ppplnames);

/*
 * scsymon_get_cluster_config
 *
 * get the following properties of a cluster:
 *	cluster name
 *	install mode
 *	private net
 *	private netmask
 *	add node auth type
 *	add node auth list
 *
 * Upon success, an object of scsymon_cluster_config_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_cluster_config().
 *
 * INPUT: pcluster_config
 * OUTPUT:ppcluster
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_ECLUSTERRECONFIG	- cluster is reconfiguring
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
extern scsymon_errno_t
scsymon_get_cluster_config(scconf_cfg_cluster_t *pcluster_config,
    scsymon_cluster_config_t **ppcluster);

/*
 * scsymon_free_cluster_config
 *
 * free cluster properties
 */
extern void
scsymon_free_cluster_config(scsymon_cluster_config_t *pcluster);

/*
 * scsymon_get_cluster_status
 *
 * get the following properties of a cluster:
 *	cluster name
 *	minimum votes required
 *	current votes
 *
 * Upon success, an object of scsymon_cluster_status_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_cluster_status().
 *
 * INPUT: pcluster_status
 * OUTPUT:ppcluster
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_ECLUSTERRECONFIG	- cluster is reconfiguring
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
extern scsymon_errno_t
scsymon_get_cluster_status(scstat_cluster_t *pcluster_status,
    scsymon_cluster_status_t **ppcluster);

/*
 * scsymon_free_cluster_status
 *
 * free cluster status
 */
extern void
scsymon_free_cluster_status(scsymon_cluster_status_t *pcluster);

/*
 * scsymon_get_node_configs
 *
 * get the following properties of all nodes configured for a cluster
 *	node name
 *	votes configured
 *	private net hostname
 *	transport adapter list
 *
 * Upon success, a list of objects of scsymon_node_config_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_node_configs().
 *
 * INPUT: pcluster_config
 * OUTPUT:pplnodes
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
extern scsymon_errno_t
scsymon_get_node_configs(scconf_cfg_cluster_t *pcluster_config,
    scsymon_node_config_t **pplnodes);

/*
 * scsymon_free_node_configs
 *
 * free cluster node properties
 */
extern void
scsymon_free_node_configs(scsymon_node_config_t *plnodes);

/*
 * scsymon_get_node_device_configs
 *
 * get the following properties of devices associated with a node.
 *	node name
 *	quorum device list
 *	possible mastered device group list
 *
 * Upon success, a list of objects of scsymon_node_device_config_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_node_device_configs().
 *
 * INPUT: pcluster_config
 * OUTPUT:pplnodedevs
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
extern scsymon_errno_t
scsymon_get_node_device_configs(scconf_cfg_cluster_t *pcluster_config,
    scsymon_node_device_config_t **pplnodedevs);

/*
 * scsymon_free_node_device_configs
 *
 * free cluster node device properties
 */
extern void
scsymon_free_node_device_configs(scsymon_node_device_config_t *plnodedevs);

/*
 * scsymon_get_node_statuss
 *
 * get the status of all nodes configured for a cluster
 *	node name
 *	node state
 *	current votes
 *
 * Upon success, a list of objects of scsymon_node_status_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_node_statuss().
 *
 * INPUT: pcluster_status
 * OUTPUT:pplnodes
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
extern scsymon_errno_t
scsymon_get_node_statuss(scstat_cluster_t *pcluster_status,
    scsymon_node_status_t **pplnodes);

/*
 * scsymon_free_node_statuss
 *
 * free cluster node status
 */
extern void
scsymon_free_node_statuss(scsymon_node_status_t *plnodes);

/*
 * scsymon_get_node_device_statuss
 *
 * get the status of devices associated with a node.
 *	node name
 *	mastered device group list
 *
 * Upon success, a list of objects of scsymon_node_device_status_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_node_device_statuss().
 *
 * INPUT: pcluster_status
 * OUTPUT:pplnodedevs
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
extern scsymon_errno_t
scsymon_get_node_device_statuss(scstat_cluster_t *pcluster_status,
    scsymon_node_device_status_t **pplnodedevs);

/*
 * scsymon_free_node_device_statuss
 *
 * free cluster node device status
 */
extern void
scsymon_free_node_device_statuss(scsymon_node_device_status_t *plnodedevs);

/*
 * scsymon_get_devgrp_configs
 *
 * get the following properties of all device groups configured for a cluster
 *	device group name
 *	service type
 *	failback enabled
 *	node list
 *	node order
 *	device list
 *
 * Upon success, a list of objects of scsymon_devgrp_config_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_devgrp_configs().
 *
 * INPUT: pcluster_config, pldevgrp_configs
 * OUTPUT:ppldevgrps
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
extern scsymon_errno_t
scsymon_get_devgrp_configs(
    scconf_cfg_cluster_t *pcluster_config,
    scconf_cfg_ds_t *pldevgrp_configs,
    scsymon_devgrp_config_t **ppldevgrps);

/*
 * scsymon_free_devgrp_configs
 *
 * free device group properties
 */
extern void
scsymon_free_devgrp_configs(scsymon_devgrp_config_t *pldevgrps);

/*
 * scsymon_get_devgrp_statuss
 *
 * get the status of all device groups configured for a cluster
 *	device group name
 *	status
 *	primary list
 *
 * Upon success, a list of objects of scsymon_devgrp_status_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_devgrp_statuss().
 *
 * INPUT: pldevgrp_statuss
 * OUTPUT:ppldevgrps
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
extern scsymon_errno_t
scsymon_get_devgrp_statuss(scstat_ds_t *pldevgrp_statuss,
    scsymon_devgrp_status_t **ppldevgrps);

/*
 * scsymon_free_devgrp_statuss
 *
 * free device group status
 */
extern void
scsymon_free_devgrp_statuss(scsymon_devgrp_status_t *pldevgrps);

/*
 * scsymon_get_replica_statuss
 *
 * get the status of all replicas configured for a device group
 *	device group name
 *	node name
 *	replica state
 *
 * Upon success, a list of objects of scsymon_replica_status_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_replica_statuss().
 *
 * INPUT: pldevgrp_statuss
 * OUTPUT:pplreplicas
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
extern scsymon_errno_t
scsymon_get_replica_statuss(scstat_ds_t *pldevgrp_statuss,
    scsymon_replica_status_t **pplreplicas);

/*
 * scsymon_free_replica_statuss
 *
 * free device group replica status
 */
extern void
scsymon_free_replica_statuss(scsymon_replica_status_t *plreplicas);

/*
 * scsymon_get_qdev_configs
 *
 * get the following properties of all quorum devices configured for a cluster:
 *	quorum device name
 *	device path
 *	enabled
 *	votes configured
 *	port list
 *
 * Upon success, a list of objects of scsymon_qdev_config_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_qdev_config_s().
 *
 * INPUT: pcluster_config
 * OUTPUT:pplqdevs
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
extern scsymon_errno_t
scsymon_get_qdev_configs(scconf_cfg_cluster_t *pcluster_config,
    scsymon_qdev_config_t **pplqdevs);

/*
 * scsymon_free_qdev_configs
 *
 * free quorum device properties
 */
extern void
scsymon_free_qdev_configs(scsymon_qdev_config_t *plqdevs);

/*
 * scsymon_get_qdevport_configs
 *
 * get the following properties of hosts of all quorum devices configured
 * for a cluster:
 *	quorum device name
 *	quorum device host name
 *	enabled
 *
 * Upon success, a list of objects of scsymon_qdevport_config_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_qdevport_config_s().
 *
 * INPUT: pcluster_config
 * OUTPUT:pplqdevports
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
extern scsymon_errno_t
scsymon_get_qdevport_configs(scconf_cfg_cluster_t *pcluster_config,
    scsymon_qdevport_config_t **pplqdevports);

/*
 * scsymon_free_qdevport_configs
 *
 * free quorum device host properties
 */
extern void
scsymon_free_qdevport_configs(scsymon_qdevport_config_t *plqdevports);

/*
 * scsymon_get_qdev_statuss
 *
 * get the status of all quorum devices configured for a cluster:
 *	quorum device name
 *	status
 *	current votes
 *	owner node
 *
 * Upon success, a list of objects of scsymon_qdev_status_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_qdev_statuss().
 *
 * INPUT: pcluster_status
 * OUTPUT:pplqdevs
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
extern scsymon_errno_t
scsymon_get_qdev_statuss(scstat_cluster_t *pcluster_status,
    scsymon_qdev_status_t **pplqdevs);

/*
 * scsymon_free_qdev_statuss
 *
 * free quorum device status
 */
extern void
scsymon_free_qdev_statuss(scsymon_qdev_status_t *plqdevs);

/*
 * scsymon_get_path_statuss
 *
 * get the status of all paths configured for a cluster
 *	source adapter
 *	destination adapter
 *	status
 *
 * Upon success, a list of objects of scsymon_path_status_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_path_statuss().
 *
 * INPUT: ptransport_status
 * OUTPUT: pplpaths
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
extern scsymon_errno_t
scsymon_get_path_statuss(scconf_cfg_cluster_t *pcluster_config,
    scstat_transport_t *ptransport_status, scsymon_path_status_t **pplpaths);

/*
 * scsymon_free_path_statuss
 *
 * free cluster path status
 */
extern void
scsymon_free_path_statuss(scsymon_path_status_t *plpaths);

/*
 * scsymon_get_transport_adapter_configs
 *
 * get the following properties of all transport adapters configured for a
 * cluster
 *	node name
 *	adapter name
 *	type
 *	enabled or not
 *
 * Upon success, a list of objects of scsymon_transport_adapter_config_t is
 * returned. The caller is responsible for freeing the space using
 * scsymon_free_adapter_configs().
 *
 * INPUT: pcluster_config
 * OUTPUT:ppladapters
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
extern scsymon_errno_t
scsymon_get_transport_adapter_configs(scconf_cfg_cluster_t *pcluster_config,
    scsymon_transport_adapter_config_t **ppladapters);

/*
 * scsymon_free_transport_adapter_configs
 *
 * free transport adapter properties
 */
extern void
scsymon_free_transport_adapter_configs(
    scsymon_transport_adapter_config_t *pladapters);

/*
 * scsymon_get_junction_configs
 *
 * get the following properties of all junctions configured for a cluster
 *	junction name
 *	junction type
 *	enabled or not
 *	configured port list
 *
 * Upon success, a list of objects of scsymon_junction_config_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_junction_configs().
 *
 * INPUT: pcluster_config
 * OUTPUT:ppljunctions
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
extern scsymon_errno_t
scsymon_get_junction_configs(scconf_cfg_cluster_t *pcluster_config,
    scsymon_junction_config_t **ppljunctions);

/*
 * scsymon_free_junction_configs
 *
 * free junction properties
 */
extern void
scsymon_free_junction_configs(scsymon_junction_config_t *pljunctions);

/*
 * scsymon_get_cable_configs
 *
 * get the following properties of all cables configured for a cluster
 *	endpoint1 type
 *	endpoint1
 *	endpoint1 port
 *	endpoint2 type
 *	endpoint2
 *	endpoint2 port
 *	enabled or not
 *
 * Upon success, a list of objects of scsymon_cable_config_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_cable_configs().
 *
 * INPUT: pcluster_config
 * OUTPUT:pplcables
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
extern scsymon_errno_t
scsymon_get_cable_configs(scconf_cfg_cluster_t *pcluster_config,
    scsymon_cable_config_t **pplcables);

/*
 * scsymon_free_cable_configs
 *
 * free cable properties
 */
extern void
scsymon_free_cable_configs(scsymon_cable_config_t *plcables);

/*
 * scsymon_get_rt_configs
 *
 * get the following properties of all Resource Types
 *	RT name
 *	installed node list
 *	description
 *	RT base directory
 *	single instance or not
 *	init node list
 *	faiover or not
 *	logical hostname, shared address or none
 *	RT dependencies
 *	api version
 *	RT version
 *	packages comprise the RT
 *
 * Upon success, a list of objects of scsymon_rt_config_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_rt_configs().
 *
 * INPUT: plrgm_rts
 * OUTPUT:pplrts
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
extern scsymon_errno_t
scsymon_get_rt_configs(scsymon_rgm_rt_t *plrgm_rts,
    scsymon_rt_config_t **pplrts);

/*
 * scsymon_free_rt_configs
 *
 * free Resource Type properties
 */
extern void
scsymon_free_rt_configs(scsymon_rt_config_t *plrts);

/*
 * scsymon_get_rt_method_configs
 *
 * get the following method pathnames of all Resource Types:
 *	RT name
 *	start method pathname
 *	stop method pathname
 *	primary_change method pathname
 *	validate method pathname
 *	update method pathname
 *	init method pathname
 *	fini method pathname
 *	boot method pathname
 *	monitor_init method pathname
 *	monitor_start method pathname
 *	monitor_stop method pathname
 *	monitor_check method pathname
 *	pre_net_start method pathname
 *	post_net_stop method pathname
 *
 * Upon success, a list of objects of scsymon_rt_method_config_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_rt_method_configs().
 *
 * INPUT: plrgm_rts
 * OUTPUT:pplrts
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
extern scsymon_errno_t
scsymon_get_rt_method_configs(scsymon_rgm_rt_t *plrgm_rts,
    scsymon_rt_method_config_t **pplrts);

/*
 * scsymon_free_rt_method_configs
 *
 * free Resource Type method pathnames
 */
extern void
scsymon_free_rt_method_configs(scsymon_rt_method_config_t *plrts);

/*
 * scsymon_get_rt_param_configs
 *
 * get parameters of all Resource Types
 *	RT name
 *	property name
 *	extenstion or not
 *	tunable
 *	type
 *	default value
 *	min INT type, minlen for STRING and STRINGARRY
 *	max INT type, maxlen for STRING and STRINGARRY
 *	minimum array size for STRINGARRAY type
 *	maximum array size for STRINGARRAY type
 *	description
 *
 * Upon success, a list of objects of scsymon_rt_param_config_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_rt_param_configs().
 *
 * INPUT: plrgm_rts
 * OUTPUT:pplrts
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
extern scsymon_errno_t
scsymon_get_rt_param_configs(scsymon_rgm_rt_t *plrgm_rts,
    scsymon_rt_param_config_t **pplrts);

/*
 * scsymon_free_rt_param_configs
 *
 * free Resource Type paramtables
 */
extern void
scsymon_free_rt_param_configs(scsymon_rt_param_config_t *plrts);

/*
 * scsymon_get_rt_rs_configs
 *
 * get resource list of all Resource Types
 *	RT name
 *	RG name
 *	Resource list
 *
 * Upon success, a list of objects of scsymon_rt_rs_config_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_rt_rs_configs().
 *
 * INPUT: plrgm_rts
 * OUTPUT:pplrts
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
extern scsymon_errno_t
scsymon_get_rt_rs_configs(scsymon_rgm_rt_t *plrgm_rts,
    scsymon_rt_rs_config_t **pplrts);

/*
 * scsymon_free_rt_rs_configs
 *
 * free Resource Type Resource list
 */
extern void
scsymon_free_rt_rs_configs(scsymon_rt_rs_config_t *plrts);

/*
 * scsymon_get_rg_configs
 *
 * get the following properties of a Resource Group:
 *	RG name
 *	primary list
 *	description
 *	maximum primaries
 *	desired primaries
 *	failback or not
 *	RG dependencies
 *	global resources used
 *	path prefix
 *
 * Upon success, a list of objects of scsymon_rg_config_t of the specified type
 * is returned. The caller is responsible for freeing the space using
 * scsymon_free_rg_configs().
 *
 * Possible RG type:
 *	SCSYMON_FAILOVER_RG
 *	SCSYMON_SCALABLE_RG
 *
 * INPUT: plrgm_rgs, rg_type_filter
 * OUTPUT:pplrgs
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
extern scsymon_errno_t
scsymon_get_rg_configs(scsymon_rgm_rg_rs_t *plrgm_rgs,
    scsymon_rg_type_t rg_type_filter,
    scsymon_rg_config_t **pplrgs);

/*
 * scsymon_free_rg_configs
 *
 * free Resource Group properties
 */
extern void
scsymon_free_rg_configs(scsymon_rg_config_t *plrgs);

/*
 * scsymon_get_rg_statuss
 *
 * get Resource Group states:
 *	RG name
 *	primary node name
 *	primary state
 *	RG state on the node
 *
 * Upon success, a list of objects of scsymon_rg_status_t of the specified type
 * is returned. The caller is responsible for freeing the space using
 * scsymon_free_rg_statuss().
 *
 * Possible RG type:
 *	SCSYMON_FAILOVER_RG
 *	SCSYMON_SCALABLE_RG
 *
 * INPUT: pcluster_status, rg_type_filter
 * OUTPUT:pplrgs
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_ECLUSTERRECONFIG	- cluster is reconfiguring
 *	SCSYMON_ERGRECONFIG	- RG is reconfiguring
 *	SCSYMON_EOBSOLETE	- Resource/RG has been updated
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
extern scsymon_errno_t
scsymon_get_rg_status(scstat_cluster_t *pcluster_status,
    scsymon_rg_type_t rg_type_filter,
    scsymon_rg_status_t **pplrgs);

/*
 * scsymon_free_rg_statuss
 *
 * free Resource Group state structures
 */
extern void
scsymon_free_rg_statuss(scsymon_rg_status_t *plrgs);

/*
 * scsymon_get_rs_configs
 *
 * get Resource properties:
 *	RG name
 *	Resource name
 *	RT name
 *	On/Off switch
 *	Monitored switch
 *	logical hostnames used
 *	Resource dependencies
 *
 * Upon success, a list of objects of scsymon_rs_config_t of the specified type
 * is returned. The caller is responsible for freeing the space using
 * scsymon_free_rs_configs().
 *
 * Possible RG type:
 *	SCSYMON_FAILOVER_RG
 *	SCSYMON_SCALABLE_RG
 *
 * INPUT: plrgm_rss, rg_type_filter
 * OUTPUT: pplrss
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
extern scsymon_errno_t
scsymon_get_rs_configs(scsymon_rgm_rg_rs_t *plrgm_rgs,
    scsymon_rg_type_t rg_type_filter,
    scsymon_rs_config_t **pplrss);

/*
 * scsymon_free_rs_configs
 *
 * free Resource properties
 */
extern void
scsymon_free_rs_configs(scsymon_rs_config_t *plrss);

/*
 * scsymon_get_rs_statuss
 *
 * get Resource state information:
 *	RG name
 *	Resource name
 *	primary node name
 *	primary state
 *	FM reported state
 *	RGM reported state
 *
 * Upon success, a list of objects of scsymon_rs_status_t of the specified type
 * is returned. The caller is responsible for freeing the space using
 * scsymon_free_rs_statuss().
 *
 * Possible RG type:
 *	SCSYMON_FAILOVER_RG
 *	SCSYMON_SCALABLE_RG
 *
 * INPUT: pcluster_status, rg_type_filter
 * OUTPUT: pplrss
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_ECLUSTERRECONFIG - cluster is reconfiguring
 *	SCSYMON_ERGRECONFIG	- RG is reconfiguring
 *	SCSYMON_EOBSOLETE	- Resource/RG has been updated
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
extern scsymon_errno_t
scsymon_get_rs_statuss(scstat_cluster_t *pcluster_status,
    scsymon_rg_type_t rg_type_filter,
    scsymon_rs_status_t **pplrss);

/*
 * scsymon_free_rs_statuss
 *
 * free Resource state structures
 */
extern void
scsymon_free_rs_statuss(scsymon_rs_status_t *plrss);

/*
 * scsymon_get_rs_prop_configs
 *
 * get predefined properties of all Resources:
 *	RG name
 *	Resource name
 *	property name
 *	property value
 *	property description
 *
 * Upon success, a list of objects of scsymon_rs_prop_config_t of the
 * specified type is returned. The caller is responsible for freeing the space
 * using scsymon_free_rs_prop_configs().
 *
 * Possible RG type:
 *	SCSYMON_FAILOVER_RG
 *	SCSYMON_SCALABLE_SERVICE_RG
 *
 * Possible RS property type type:
 *	SCSYMON_RS_COM_PROP
 *	SCSYMON_RS_EXT_PROP
 *	SCSYMON_RS_TIMEOUT_PROP
 *
 * INPUT: plrgm_rgs, rg_type_filter, rs_prop_type_filter
 * OUTPUT: pplrss
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
extern scsymon_errno_t
scsymon_get_rs_prop_configs(scsymon_rgm_rg_rs_t *plrgm_rgs,
    scsymon_rg_type_t rg_type_filter,
    scsymon_rs_prop_type_t rs_prop_type_filter,
    scsymon_rs_prop_config_t **pplrss);

/*
 * scsymon_free_rs_prop_configs
 *
 * free Resource properties
 */
extern void
scsymon_free_rs_prop_configs(scsymon_rs_prop_config_t *plrss);

#ifdef __cplusplus
}
#endif

#endif	/* _SCSYMON_SRV_H_ */
