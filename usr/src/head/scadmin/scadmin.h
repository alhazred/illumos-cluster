/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SCADMIN_H
#define	_SCADMIN_H

#pragma ident	"@(#)scadmin.h	1.7	08/05/20 SMI"

/*
 * This is the header file for libscadmin.  All users of libscadmin should
 * include this file.
 */

#include <stdio.h>
#include <sys/types.h>

#ifdef __cplusplus
extern "C" {
#endif

/* A list of names, or strings */
typedef struct scadmin_namelist {
	char		*scadmin_nlist_name;			/* name */
	struct scadmin_namelist *scadmin_nlist_next;		/* next */
} scadmin_namelist_t;

/*
 * Flags for network funcitons
 */
#define	SCADMIN_ADAPTERS_ALL		1  /* All adapters in device tree */
#define	SCADMIN_ADAPTERS_AVAILABLE	2  /* All available adapters */
#define	SCADMIN_ADAPTERS_ACTIVE		3  /* All active adapters */
#define	SCADMIN_ADAPTERS_ACTIVE_LIF	4  /* All logical interfaces */

#define	SCADMIN_TRANSPORT_CONFIG	5  /* Get the transport config */
#define	SCADMIN_ADAPTERS_PING		6  /* broadcast ping */
#define	SCADMIN_ADAPTERS_DLPI_DISCOVER	7  /* dlpi discover */


#define	SCADMIN_ADAPTERS_ISVLAN		8  /* check vlan capability */

/*
 * Flags for system files update functions
 */
#define	SCADMIN_UPDATE_HOSTS		1  /* Update hosts */
#define	SCADMIN_UPDATE_NTP		2  /* Update ntp.conf.cluster */

/*
 * Flags for autodiscover
 */
#define	SCADMIN_AUTODISCOVER_SEND	(1 << 0)	/* Send */
#define	SCADMIN_AUTODISCOVER_RECV	(1 << 1)	/* Recevie */
#define	SCADMIN_AUTODISCOVER_PING	(1 << 2)	/* Finish w/ ping */
#define	SCADMIN_AUTODISCOVER_NOWAIT	(1 << 3)	/* Dont wait */
#define	SCADMIN_AUTODISCOVER_SNOOP	(1 << 4)	/* Just snoop */

/*
 * Network adapters, and related functions
 *
 *	scadmin_get_network_adapters()
 *	scadmin_get_active_adapters()
 */

/*
 * scadmin_get_network_adapters
 *
 * Return a list of network adapters in the device tree.  A list of adapter
 * types may also be given.   If "adaptypes" is not NULL, it is a list
 * of known adapter types for which to also search.
 *
 * This function recogizes the following flags:
 *
 *	SCADMIN_ADAPTERS_ALL		- All adapters in device tree
 *	SCADMIN_ADAPTERS_AVAILABLE	- All available adapters
 *	SCADMIN_ADAPTERS_ACTIVE		- All active adapters
 *	SCADMIN_ADAPTERS_LIF		- All logical interfaces
 *
 * It is the caller's responsibility to free the returned namelist
 * by calling scadmin_free_namelist().
 *
 */
extern scadmin_namelist_t *scadmin_get_network_adapters(uint_t flag,
    char *adaptypes);

/*
 * scadmin_get_active_adapters
 *
 * Return a list of all logical network interfaces which are active.
 *
 * This function recogizes the following flags:
 *
 *	SCADMIN_ADAPTERS_ACTIVE		- All active adapters
 *	SCADMIN_ADAPTERS_LIF		- All logical interfaces
 *
 * It is the caller's responsibility to free the returned namelist
 * by calling scadmin_free_namelist().
 *
 */
extern scadmin_namelist_t *scadmin_get_active_adapters(uint_t flag);

/*
 * Autodiscovery and related functions
 *
 *	scadmin_autodiscovery()
 *	scadmin_snoop()
 *	scadmin_broadcast_ping()
 */

/*
 * scadmin_autodiscover
 *
 * Send and/or recieve dlpi packets to and/or from the specified list
 * of "adapters" in order to discover connectivety.   The arguments to
 * this function control behavior in the following ways:
 *
 *	adapters	This string is a comma-separated list of adapters
 *			to use for sending and/or receiving packtets.
 *
 *	filename	If not NULL, results of the discovery attempt
 *			are printed to this file.
 *
 *	outputp		If not NULL, results are added to this list.
 *
 *	flags		The following flags may be or-ed together:
 *
 *	    SCADMIN_AUTODISCOVER_SEND		Send packets
 *	    SCADMIN_AUTODISCOVER_RECV		Receive packets
 *	    SCADMIN_AUTODISCOVER_PING		Finish up w/ broadcast ping
 *
 *	    SCADMIN_AUTODISCOVER_NOWAIT		Stop receiving before timeout
 *	    SCADMIN_AUTODISCOVER_SNOOP		Just snoop
 *
 *			If SCADMIN_AUTODISCOVER_NOWAIT is given, we
 *			quit as soon as at least one packet has been
 *			received on each adapters, without waiting
 *			for the timeout.   Or, if wait_cnt is non-zero,
 *			quit as soon as at least one packet has been
 *			received on wait_cnt adapters.
 *
 *			Setting "flags" to zero results in the same behavior
 *			as or-ing the FIRST THREE flags together.
 *
 *	send_cnt	If sending, send the given number of packets on
 *			each of the adapters at 1 second intervals.  If
 *			zero, packets are sent at 1 second intervals until
 *			the receive timeout is exhausted.
 *
 *	recv_to		If receiving, look for packets on each of the adapters
 *			until "recv_to" seconds are exhasted.   If zero,
 *			a default of 30 seconds is used.
 *
 *	wait_cnt	If SCADMIN_AUTODISCOVER_NOWAIT is given and wait_cnt
 *			is non-zero, quit as soon as at least one packet
 *			has been received on wait_cnt adapters.
 *
 *	token		If not NULL, it is added to each packet sent
 *			and is used to identify packets intended for
 *			us on the receiving side.
 *
 *	errsp		If not NULL, error messages will be added to this list.
 *
 * Possible return values:
 *
 *	0		No unexpected errors
 *	1		Unexpected errors
 */
extern uint_t scadmin_autodiscover(char *adapters, char *vlans, char *filename,
    scadmin_namelist_t **outputp, uint_t flags, uint_t send_cnt,
    uint_t recv_to, uint_t wait_cnt, char *token, scadmin_namelist_t **errsp);

/*
 * scadmin_snoop
 *
 * Snoop for all packets on the list of "adapters".
 *
 *	adapters	This string is a comma-separated list of adapters
 *			to use for snooping.
 *
 *	outputp		A line is added to the "outputp" list for each
 *			adapter in the form "<adapter>:<count>", where
 *			<count> is the number of packets seen over the
 *			"recv_to" period.
 *
 *	flags		The following flags may be given:
 *
 *	    SCADMIN_AUTODISCOVER_NOWAIT		Stop receiving before timeout
 *
 *			If SCADMIN_AUTODISCOVER_NOWAIT is given, we
 *			quit as soon as one packet has been received on
 *			each adapters, without waiting for the timeout.
 *
 *	recv_to		Look for packets on each of the adapters
 *			until "recv_to" seconds are exhasted.   If zero,
 *			a default of 30 seconds is used.
 *
 *	errsp		If not NULL, error messages will be added to this list.
 *
 * Possible return values:
 *
 *	0		No unexpected errors
 *	1		Unexpected errors
 */
extern uint_t scadmin_snoop(char *adapters, scadmin_namelist_t **outputp,
    uint_t flags, uint_t recv_to, scadmin_namelist_t **errsp);

/*
 * scadmin_broadcast_ping
 *
 * Send a ping to 255.255.255.255.
 */
extern void scadmin_broadcast_ping(void);

/*
 * Miscelaneous functions
 *
 *	scadmin_append_namelist()
 *	scadmin_namelist_search()
 *	scadmin_namelist_print()
 *	scadmin_free_namelist()
 */

/*
 * scadmin_append_namelist
 *
 * Add a new "name" to the given scadmin namelist.
 *
 * Possible return values:
 *
 *	0		Success
 *	1		Not enough memory
 */
extern int scadmin_append_namelist(scadmin_namelist_t **namelistp, char *name);

/*
 * scadmin_namelist_search
 *
 * Find the given name in the namelist.
 *
 * This function returns NULL if the name is not there.
 *
 */
extern scadmin_namelist_t *scadmin_namelist_search(
    scadmin_namelist_t *namelist, char *name);

/*
 * scadmin_namelist_print
 *
 * Print the namelist.
 *
 */
extern void scadmin_namelist_print(char *prefix, scadmin_namelist_t *namelist,
    FILE *fp);

/*
 * scadmin_free_namelist
 *
 * Free an scadmin namelist.
 *
 */
extern void scadmin_free_namelist(scadmin_namelist_t *namelist);

/*
 * scadmin_get_major_number
 *
 * Obtain the major number entry that corresponds with the selected
 * 'driver' as well as the maximum major number entry on this node.
 *
 * Returns
 * 	A pair of integers (driver_entry,, maximum major_num entry) - success
 * 	NULL							    - failure
 *
 */
extern scadmin_namelist_t *scadmin_get_major_number(char *driver);

/*
 * scadmin_get_zonelist
 *	Returns
 *		A list of excl ip zones on the node
 *
 */
scadmin_namelist_t *scadmin_get_zonelist();

#ifdef __cplusplus
}
#endif

#endif /* _SCADMIN_H */
