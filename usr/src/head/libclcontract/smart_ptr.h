//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _SMART_PTR_H
#define	_SMART_PTR_H

#pragma ident	"@(#)smart_ptr.h	1.2	08/07/23 SMI"

//
// This template class provides a reference counted smart pointer. Objects
// of the class encapsulate a single pointer (of any type). The object uses
// a static map to track a count of all references to a pointer. When the
// object is copied or assigned, the reference count is incremented.
// When the object is destroyed, the reference count is decremented. When
// the reference count reaches 0, delete is called on the pointer.
//
// A few notes:
// Never pass a smart pointer object by reference. Always pass by value.
// Never call delete on a pointer from which you've constructed a smart
// pointer.
// Don't use with pointers allocated with operator new[] or malloc.
// Use only when allocated with operator new.
//
// Sample usage:
//
// void
// do_something()
// {
// 	my_class *p = new my_class;
//	smart_ptr<my_class> sp(p);
//	do_something_else(sp);
//
//	// no need to call delete on p! Destructor of sp will clean up!
// }
//
// void
// do_something_else(smart_ptr<char> sp) // Note pass-by-value!
// {
//	sp->method_on_my_class();
//	(*sp).another_method_on_my_class();
//	// No need to free sp!
// }
//
//
// This implementation of the smart pointer adapted from Chapter 25 of
// "Professional C++" by Nicholas Solter and Scott Kleper (Wrox, 2005).
//
// The main change is the addition of locking for multi-threaded use.
//
//
#include <map>

typedef void (*dbg_print_func_t)(char *fmt, ...);
extern dbg_print_func_t dbg_print_func;

template <class T> class smart_ptr {
public:
	// The ctor is marked explicit so that pointers aren't converted
	// implicitly to smart pointers. You must explicitly construct
	// a smart ptr from a dumb ptr.
	explicit smart_ptr(T *in_ptr);
	smart_ptr();
	~smart_ptr();

	smart_ptr(const smart_ptr<T> &src);
	smart_ptr<T>& operator=(const smart_ptr<T> &rhs);

	T &operator*() const;
	T *operator->() const;

	// use with care!
	T* get_ptr() const { return ptr; }

protected:
	T* ptr;
	static std::map<T*, int> ref_count_map;
	static pthread_mutex_t map_lock;

	void finalize_pointer();
	void init_pointer(T* in_ptr);
};

#include "smart_ptr_in.h"

#endif /* _SMART_PTR_H */
