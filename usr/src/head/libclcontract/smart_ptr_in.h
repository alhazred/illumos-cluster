//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _SMART_PTR_IN_H
#define	_SMART_PTR_IN_H

#pragma ident	"@(#)smart_ptr_in.h	1.2	08/07/23 SMI"

//
// This implementation adapted from Chapter 25 of
// "Professional C++" by Nicholas Solter and Scott Kleper (Wrox, 2005).
//
// The main change is the addition of locking for multi-threaded use.
//

#include <sys/cl_assert.h>

#define	NOGET(s)	(s)

template <class T>
std::map<T*, int> smart_ptr<T>::ref_count_map;

template <class T>
pthread_mutex_t smart_ptr<T>::map_lock = PTHREAD_MUTEX_INITIALIZER;

template <class T>
smart_ptr<T>::smart_ptr(T *in_ptr)
{
	init_pointer(in_ptr);
}

template <class T>
smart_ptr<T>::smart_ptr()
{
	init_pointer(NULL);
}

template <class T>
smart_ptr<T>::smart_ptr(const smart_ptr<T> &src)
{
	init_pointer(src.ptr);
}

template <class T>
smart_ptr<T>&
// CSTYLED
smart_ptr<T>::operator=(const smart_ptr<T> &rhs)
{
	if (this == &rhs) {
		return (*this);
	}
	finalize_pointer();
	init_pointer(rhs.ptr);

	return (*this);
}

template <class T>
smart_ptr<T>::~smart_ptr()
{
	finalize_pointer();
}

template<class T>
void
smart_ptr<T>::init_pointer(T* in_ptr)
{
	ptr = in_ptr;
	if (ptr == NULL) {
		// ignore the NULL ptr
		return;
	}
	CL_PANIC(pthread_mutex_lock(&map_lock) == 0);
	if (ref_count_map.count(ptr) == 0) {
		(*dbg_print_func)(NOGET("smart_ptr: initing %lx\n"), in_ptr);
		ref_count_map[ptr] = 1;
	} else {
		ref_count_map[ptr]++;
		(*dbg_print_func)(NOGET("smart_ptr: incr %lx, count %d\n"),
		    in_ptr, ref_count_map[ptr]);
	}
	CL_PANIC(pthread_mutex_unlock(&map_lock) == 0);
}

template<class T>
void
smart_ptr<T>::finalize_pointer()
{
	if (ptr == NULL) {
		// ignore the NULL ptr
		return;
	}
	CL_PANIC(pthread_mutex_lock(&map_lock) == 0);
	if (ref_count_map.count(ptr) == 0) {
		(*dbg_print_func)(NOGET("Error, missing entry in map.\n"));
		CL_PANIC(pthread_mutex_unlock(&map_lock) == 0);
		return;
	}
	ref_count_map[ptr]--;
	(*dbg_print_func)(
	    NOGET("smart_ptr: decr %lx, count %d\n"), ptr, ref_count_map[ptr]);
	if (ref_count_map[ptr] == 0) {
		// no more references to this object --
		// delete it and remove from map
		(*dbg_print_func)(NOGET("last entry for %lx: deleting\n"), ptr);
		ref_count_map.erase(ptr);
		delete ptr;
	}
	CL_PANIC(pthread_mutex_unlock(&map_lock) == 0);
}

template <class T>
T*
smart_ptr<T>::operator->() const
{
	return (ptr);
}

template <class T>
T&
smart_ptr<T>::operator*() const
{
	return (*ptr);
}

#endif /* _SMART_PTR_IN_H */
