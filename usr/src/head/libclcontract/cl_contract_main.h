/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CL_CONTRACT_MAIN_H
#define	_CL_CONTRACT_MAIN_H

#pragma ident	"@(#)cl_contract_main.h	1.2	08/07/23 SMI"

#ifdef __cplusplus
extern "C" {
#endif

void libclcontracts_initialize(void);
void free_string_arr(char **arr);
void free_string_arr_c(char **arr);
char *strdup_nothrow(const char *src);

#ifdef __cplusplus
}
#endif

#endif /* _CL_CONTRACT_MAIN_H */
