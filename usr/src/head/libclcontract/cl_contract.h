//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _CL_CONTRACT_H
#define	_CL_CONTRACT_H

#pragma ident	"@(#)cl_contract.h	1.2	08/07/23 SMI"

#include <pthread.h>
#include <synch.h>
#include <string>
#include <map>
#include <vector>

#include <libcontract.h>

#include <libclcontract/smart_ptr.h>

using std::string;

extern char *syslog_tag;

class cl_contract {
public:
	//
	// Shallow copies of cmd_in and env_in are stored. Everything
	// else is deep copied.
	//
	cl_contract(char *identifier_in, char **cmd_in,
	    uint_t fatal, uint_t critical, uint_t informative, int &err,
	    char *syslog_tag_in, dbg_print_func_t func,
	    char **env_in = NULL, const char *pwd_in = NULL,
	    const char *path_in = NULL);

	virtual ~cl_contract();

	// ***********************
	// control methods
	// ************************

	// start() returns 0 if the fork succeeds, non-zero errno error code
	// otherwise.
	int start(pid_t &pid);

	void stop();

	// Returns an errno error if something goes wrong
	int wait(int seconds);

	// Process a contract event
	virtual void event_dispatch(ct_evthdl_t ev);

	// *******************
	// access methods
	// *******************
	const string &get_nametag() { return nametag; }
	char *get_nametag_copy() { return (strdup(nametag.c_str())); }
	ctid_t get_contract_id() { return contract_id; }
	// Returns shallow copy (ptr only)
	const char *get_cmd_string() { return cmd_string.c_str(); }

	// Returns deep copy. length is -1 and result is NULL if a
	// memory error occurs.
	char **get_cmd_copy(int &length);

	// length is -1 if memory error occurs. Result can be NULL
	// without memory error, if there is no env stored.
	char **get_env_copy(int &length);

protected:

	// ******************************************
	// Helper methods. See .cc file for details.
	// ******************************************

	int start_process(char **cmd_to_run, pid_t &pid);
	void end_monitoring();
	int set_up_process_template();
	int retrieve_process_template_info();
	virtual void setup_child();
	int send_signal(pid_t pid, int signal) const;
	virtual int incr_restart_and_generate_event(
	    boolean_t action_script_flag);

	void ack_event(ct_evthdl_t ev);

	static int contract_open(ctid_t ctid, const char *file, int oflag);
	static char **copy_char_arr(char** src, int &length);

	//
	// Parameters passed in by the process
	// that creates this cl_contract object.
	// These dont change throughout the lifetime of the object.
	//
	char **cmd;
	char **env;
	char *wdir;
	char *path;
	string cmd_string;
	string nametag;

	uint_t fatal_events;
	uint_t critical_events;
	uint_t informative_events;

	// ********************
	// Dynamic state
	// ********************

	// The contract id for the current process tree
	ctid_t contract_id;

	//
	// The file descriptor for the control file of the contract --
	// Used for acking events
	//
	int contract_fd;

	//
	// The id of the level 0 forked process -- used to
	// retrieve exit code of that process
	//
	pid_t parent_pid;

	//
	// The return code of the level 0 forked process -- used
	// to analyze result of action script
	//
	int parent_return_code;

	// The processes currently alive in the monitored process tree.
	std::map<pid_t, int> procs;

	// *************************
	// Synchronization variables
	// **************************
	pthread_mutex_t lock;
	pthread_cond_t cond;
	bool lock_inited;
	bool cond_inited;

	//
	// The number of threads still waiting on the condition
	// variable. Needed so that the broadcasting thread gives
	// all the waiting threads a chance to run before it goes ahead
	// and restarts the process.
	//
	int waiters;

	//
	// inter-process semaphore used to communicate between the process
	// that creates the cl_contract and the newly forked process
	//
	sema_t *sema_p2c;

	// *****************************
	// flags to track internal state
	// *****************************

	//
	// stop_monitoring is set when the stop() method is called.
	// This action is irrevocable -- once a cl_contract is told to
	// stop monitoring, it never starts again.
	//
	bool stop_monitoring;

	//
	// is_running is true whenever a process tree launched by
	// this cl_contract is running.
	//
	bool is_running;

	// Flag to decide whether or not to increment the restart count in
	// start_process().
	bool incr_restart_cnt_flag;

private:
	// prevent assignment and pass-by-value
	cl_contract(const cl_contract &src);
	cl_contract &operator = (const cl_contract &rhs);
};
#endif /* _CL_CONTRACT_H */
