//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _CL_CONTRACT_TABLE_H
#define	_CL_CONTRACT_TABLE_H

#pragma ident	"@(#)cl_contract_table.h	1.2	08/07/23 SMI"

#include <libclcontract/cl_contract.h>
#include <map>
#include <string>
#include <list>
#include <libclcontract/smart_ptr.h>
using std::string;

//
// cl_contract_table stores cl_contract objects, wrapped in smart_ptrs,
// and keyed on nametag.
//
// It also stores the current mappings of contract id to nametag.
//
// The class follows the singleton pattern.
//
class cl_contract_table {
public:
	// Returns ENOMEM if memory error.
	// Returns 1 if contract with that id already exists.
	// Returns 0 on success.
	int add_contract(smart_ptr<cl_contract> contract);

	// Returns ENOMEM if memory error.
	// Returns 1 if contract with that id already exists.
	// Returns 0 on success.
	int add_mapping(ctid_t cid, const string &name);

	int remove_mapping(ctid_t cid);

	// returns non-zero if the entry cannot be found
	int lookup(const string &name, smart_ptr<cl_contract>&) const;
	int lookup(ctid_t cid, smart_ptr<cl_contract>&) const;

	int remove(const string &name);

	int get_all(std::vector<smart_ptr<cl_contract> >&);

	static cl_contract_table &instance();
	static void create();

protected:
	cl_contract_table();

	// Don't provide implementations for these
	cl_contract_table(const cl_contract_table& src);
	cl_contract_table& operator=(const cl_contract_table& rhs);

	std::map<string, smart_ptr<cl_contract> > contracts;
	std::map<ctid_t, string> names;

	// The lock must be mutable so that it can be modified
	// (locked and unlocked) in const methods like lookup()
	mutable pthread_mutex_t lock;
	static cl_contract_table* the_table;
	static pthread_mutex_t create_lock;
};

#endif /* _CL_CONTRACT_TABLE_H */
