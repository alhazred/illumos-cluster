/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _PMF_H_
#define	_PMF_H_

#pragma ident	"@(#)pmf.h	1.4	08/05/20 SMI"
/*
 * pmf.h - door interface to HA Process Monitoring Facility
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <rpc/rpc.h>
#include <thread.h>
#include <string.h>
#include <syslog.h>
#include <door.h>
#include <rgm/door/pmf_door.h>
#include <rgm/security.h>

typedef struct pmf_input_args_t {
	int api_name;
	void *data;
} pmf_input_args_t;

int pmf_door_clnt(void *args, void *result,
    int pmf_api_name, client_handle *clnt);


extern int pmf_start(char *id, pmf_cmd *cmd,
    int argc, char **argv, pmf_result *res);
extern int pmf_status(char *id, pmf_cmd *cmd,
    pmf_status_result *status);
extern int pmf_status_all(pmf_cmd *cmd, pmf_status_result *status);
extern int pmf_stop(char *id, pmf_cmd *cmd, pmf_result *res);
extern int pmf_kill(char *id, pmf_cmd *cmd, pmf_result *res);
extern int pmf_modify(char *id, pmf_cmd *cmd, pmf_result *res);
extern int pmf_suspend(char *id, pmf_cmd *cmd, pmf_result *res);
extern int pmf_resume(char *id, pmf_cmd *cmd, pmf_result *res);
extern void free_rpc_args(pmf_status_result *result);

/* Name of RPC service, as identified in rpc(4) */
#define	PMF_PROGRAM_NAME   "pmfd"


#define	PMFPROC_NULL	0
extern enum clnt_stat pmfproc_null_2(void *, void *, client_handle *);
extern bool_t pmfproc_null_2_svc(void *, void *);
#define	PMFPROC_START	1
extern enum clnt_stat pmfproc_start_2(pmf_start_args *, pmf_result *,
    client_handle *);
extern bool_t pmfproc_start_2_svc(pmf_start_args *, pmf_result *);
#define	PMFPROC_STATUS	2
extern enum clnt_stat pmfproc_status_2(pmf_args *, pmf_status_result *,
    client_handle *);
extern bool_t pmfproc_status_2_svc(pmf_args *, pmf_status_result *);
#define	PMFPROC_STOP	3
extern enum clnt_stat pmfproc_stop_2(pmf_args *, pmf_result *, client_handle *);
extern bool_t pmfproc_stop_2_svc(pmf_args *, pmf_result *);
#define	PMFPROC_KILL	4
extern enum clnt_stat pmfproc_kill_2(pmf_args *, pmf_result *, client_handle *);
extern bool_t pmfproc_kill_2_svc(pmf_args *, pmf_result *);
#define	PMFPROC_MODIFY	5
extern enum clnt_stat pmfproc_modify_2(pmf_modify_args *, pmf_result *,
    client_handle *);
extern bool_t pmfproc_modify_2_svc(pmf_modify_args *, pmf_result *);
#define	PMFPROC_SUSPEND	6
extern enum clnt_stat pmfproc_suspend_2(pmf_args *, pmf_result *,
    client_handle *);
extern bool_t pmfproc_suspend_2_svc(pmf_args *, pmf_result *);
#define	PMFPROC_RESUME	7
extern enum clnt_stat pmfproc_resume_2(pmf_args *, pmf_result *,
    client_handle *);
extern bool_t pmfproc_resume_2_svc(pmf_args *, pmf_result *);

extern int pmf_program_2_freeresult(SVCXPRT *, xdrproc_t, caddr_t);


/* XDR Functions */

extern void pmf_result_args_xdr_free(int api_name, void *result);
extern void pmf_input_args_xdr_free(pmf_input_args_t *pmf_arg);
extern size_t pmf_xdr_sizeof(pmf_input_args_t *arg);
extern size_t pmf_xdr_sizeof_result(void *, int);

extern  bool_t xdr_pmf_input_args(XDR *xdrs, pmf_input_args_t *objp);
extern  bool_t xdr_pmf_result_args(XDR *, void *, int);

#ifdef __cplusplus
}
#endif

#endif /* _PMF_H_ */
