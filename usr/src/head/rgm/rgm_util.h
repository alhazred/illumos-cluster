/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RGM_UTIL_H_
#define	_RGM_UTIL_H_

#pragma ident	"@(#)rgm_util.h	1.36	08/07/28 SMI"


#ifdef __cplusplus
extern "C" {
#endif

#include <scha.h>
#include <rgm/scha_strings.h>
#include <sys/sol_version.h>
#if SOL_VERSION >= __s10
#include <zone.h>
#endif


/*
 * Zone suppport
 */
/* default value for the lni field in nodeidlist_t struct */
#define	LNI_UNDEF		0
/* LN is of the form <nodeid><LN_DELIMITER><zonename> */
#define	LN_DELIMITER		':'
#define	LN_DELIMITER_STR	":"

#if SOL_VERSION >= __s10
#define	MAX_LN_LEN	(CL_MAX_LEN + ZONENAME_MAX + 2)
#else
#define	MAX_LN_LEN	(CL_MAX_LEN + 1)
#endif


/*
 * Prefix of CCR tablename for RG and RT
 */
#define	RGM_PREFIX_RG		"rgm_rg_"
#define	RGM_PREFIX_RT		"rgm_rt_"

#define	RGM_PREFIX_RS		"RS_"
#define	RGM_PREFIX_PROP		"p."	/* prefix for predefined props */
#define	RGM_PREFIX_EXT		"x."	/* prefix for extension props */
#define	RGM_PREFIX_PN_EXT	"n."	/* prefix for pernode extension props */
#define	RGM_VALUE_SEPARATOR	','
#define	RGM_VALUE_SEPARATOR_STR	","
#define	RGM_PROP_SEPARATOR	';'
#define	RGM_PROP_SEPARATOR_STR	";"
#define	RGM_DEP_SEPARATOR_PRE	'{'
#define	RGM_DEP_SEPARATOR_POST	'}'
#define	RGM_VALUE_ASSIGN	'='
/* per node separators */
#define	RGM_PN_SEPARATOR_PRE    '{'
#define	RGM_PN_SEPARATOR_POST   '}'

/*
 *  Properties/flags in CCR which do not have a tag name defined in scha.h
 */
#define	RGM_CCRSTAMP		"ccr_gennum"
#define	RGM_UNMANAGED		"Unmanaged"
#define	RG_OK_TO_START		"Ok_To_Start"
#define	RG_FINI_NODES		"FiniNodes"
#define	RT_SYSDEF_TYPE		"SysdefinedType"
#define	R_DELETED		"DELETED_STATE"

/*
 * The following three defines are also defined (identically)
 * in the version manager, in vm_internal_custom.cc.  Any changes
 * here should be made there as well.
 */
#define	VP_MAJOR_NUM		"VP_Major_Number"
#define	VP_MINOR_NUM		"VP_Minor_Number"

/* CCR table for versioning */
#define	RGM_VP_TABLE		"rgm_vp_version"

/* Solaris default project name */
#define	RGM_DEFAULT_PROJ	"default"

/*
 * SSM wrapper pathname and timeout
 */
#define	SSM_WRAPPER_NAME	"ssm_wrapper"
#define	SSM_WRAPPER_PATH	"/usr/cluster/lib/sc/" SSM_WRAPPER_NAME
#define	SSM_WRAPPER_TIMEOUT	300

/*
 * RGM object types
 */
typedef enum rgm_obj_type {
	O_RG = 0,
	O_RT,
	O_RSRC
} rgm_obj_type_t;

/*
 * Functions to convert to and from enum values
 * and string name for the value.
 *
 * The conversion-to-string functions
 * return constants that should not be deleted.
 *
 * Callers of these routines are trusted to
 * provide valid input parameter values;
 * input parameter pointers must be non-NULL.
 *
 * NOTE "const" not used on return types since
 * const is not yet consistently used in
 * interface.
 */
char *bool2str(boolean_t);
boolean_t str2bool(const char *);

const char *initnodes2str(scha_initnodes_flag_t);
scha_initnodes_flag_t prop_initnodes(const char *);

const char *sysdeftype2str(rgm_sysdeftype_t);
rgm_sysdeftype_t prop_sysdeftype(const char *);

const char *rgmode2str(scha_rgmode_t);
scha_rgmode_t prop_rgmode(const char *);

const char *type2str(scha_prop_type_t);
scha_prop_type_t param_type(const char *);

const char *tune2str(rgm_tune_t);
rgm_tune_t param_tune(const char *);


char *rgm_itoa(int);
char *rgm_itoa_cpp(int);
char *nstr2str_dep(rdeplist_t *);

char *nstr2str(namelist_t *);
char *nstr2str_cpp(namelist_t *);
char *nstr_all2str(const namelist_all_t *);
char *nstr_all2str_cpp(const namelist_all_t *);
namelist_t *str2nstr(char *);
rdeplist_t *str2rdepstr(char *);
char **nstr2strarray(namelist_t *);
namelist_t *strarray2nstr(char **);
char *strarray2str(char **);
char **str2strarray(char *);
char *params2str(const rgm_param_t *);
char *params2str_cpp(rgm_param_t *);
char *rsrc2str(rgm_resource_t *);
char *my_strtok(char **str, char delimiter);
scha_errmsg_t get_property_value(char *, rgm_obj_type_t,
    const char *, char **, const char *);
scha_errmsg_t update_property(const char *, rgm_obj_type_t, const char *,
    const char *, const char *);
char *name_delete(const char *, char *);
char *nodeidlist2str(nodeidlist_t *l);
char *nodeidlist2str_cpp(nodeidlist_t *l);
nodeidlist_t *str2nodeidlist(char *s);
char *nodeidlist_all2str_cpp(const nodeidlist_all_t *l);

#ifdef __cplusplus
}
#endif

#endif /* _RGM_UTIL_H_ */
