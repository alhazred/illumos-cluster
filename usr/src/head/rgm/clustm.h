/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 *	Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 *	Use is subject to license terms.
 */

#ifndef	_CLUSTM_H_
#define	_CLUSTM_H_

#pragma ident	"@(#)clustm.h	1.6	08/05/20 SMI"

/*
 * clustm.h
 *
 * 	Definitions needed to use the cluster monitor library.
 */

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Cluster nodes are identified by integers starting from zero.
 */
typedef short nodeid_t;

/*
 * THIS_NODE is used as the argument to cm_stop_node/cm_abort_node
 * functions to specify the local node as the command target.
 */
#define	THIS_NODEID	((nodeid_t)-2)

/*
 * Cluster monitor handle.
 */
typedef void *clustmh_t;

/*
 * The state of the node.
 */
typedef enum {
	NS_NOTCONF,			/* the node not configured */
	NS_UNKNOWN,			/* the node state is unknown */
	NS_DOWN,			/* the node is down */
	NS_START,			/* executing the start transition */
	NS_RETURN,			/* executing the return transition */
	NS_STOP,			/* executing the stop transition */
	NS_ABORT,			/* executing the abort transition */
	NS_BEGIN,			/* in the begin state */
	NS_STEP,			/* executing a reconfiguration step */
	NS_END				/* in the end state */
} nodestate_t;

/*
 * Error codes returned from cluster monitor functions.
 */
typedef enum {
	CS_SUCCESS,			/* successful return */
	CS_BAD_CLUSTNAME,		/* invalid cluster name */
	CS_BAD_NODEID,			/* invalid nodeid */
	CS_INVAL,			/* invalid parameter */
	CS_PERM,			/* permission denied */
	CS_TIMEOUT,			/* request to clustd timed out */
	CS_NOMEM,			/* can't malloc */
	CS_VERS,			/* bad protocol version number */
	CS_COMM				/* can't communicate with clustd */
} cm_stat_t;

/*
 * Cluster membership information structure.
 */
typedef struct clust_state {
	nodeid_t    cs_nnodes;
	nodeid_t    cs_local_nodeid;
	nodestate_t cs_curr_state;
	int	    cs_min_quorum;
	int	    cs_curr_quorum;
	int	    cs_max_step;
	int	    cs_curr_step;
	unsigned long cs_rseqnum;
	void	    *cs_all_nodes;
	void	    *cs_curr_members;
} clust_state_t;

/*
 * These macros should be used to access the cluster membership sets.
 */
#define	CS_ISMEMBER(cs, nd) __clustm_ns_test((cs)->cs_curr_members, (nd))
#define	CS_ISCONFIG(cs, nd) __clustm_ns_test((cs)->cs_all_nodes, (nd))
#define	CS_HASQUORUM(cs) ((cs)->cs_curr_quorum >= (cs)->cs_min_quorum)

#ifdef _REENTRANT
extern int *__cm_errno();
#define	cm_errno (*(__cm_errno()))
#else
extern int cm_errno;
#endif  /* _REENTRANT */

/*
 * Cluster monitor API functions. A zero return value indicates a successful
 * completion, the value of -1 indicates an error. After an error,
 * the cm_errno contains the specific error code (see cm_stat_t).
 */
#ifdef	__STDC__
extern clustmh_t cm_getclustmbyname(char *clustname);
extern void cm_freeclustmh(clustmh_t cmh);

extern int cm_stop_node(clustmh_t cmh, nodeid_t nodeid);
extern int cm_abort_node(clustmh_t cmh, nodeid_t nodeid);
extern int cm_stop_all(clustmh_t cmh);
extern int cm_abort_all(clustmh_t cmh);
extern int cm_reconfigure(clustmh_t cmh);
extern int cm_getstate(clustmh_t cmh, clust_state_t *csp);
extern void cm_freestate(clust_state_t *csp);

extern int cm_control(clustmh_t cmh, int cmd, void *arg);
extern void cm_perror(char *msg);
extern char *cm_sperror();

extern int __clustm_ns_test(void *p, nodeid_t nd);
#else	/* __STDC__ */
extern clustmh_t cm_getclustmbyname();
extern void cm_freeclustmh();

extern int cm_stop_node();
extern int cm_abort_node();
extern int cm_stop_all();
extern int cm_abort_all();
extern int cm_reconfigure();
extern int cm_getstate();
extern void cm_freestate();

extern int cm_control();
extern void cm_perror();
extern char *cm_sperror();

extern int __clustm_ns_test();
#endif	/* __STDC__ */

/*
 * cm_control() is used for the following functions:
 *
 * int cm_control(clustmh_t cmh, CMSET_TIMEOUT, int timeout);
 * int cm_control(clustmh_t cmh, CMGET_TIMEOUT, int *timeout);
 * int cm_control(clustmh_t cmh, CMSET_RETRY_TIMEOUT, int timeout);
 * int cm_control(clustmh_t cmh, CMGET_RETRY_TIMEOUT, int *timeout);
 * (all the timeout values are in milliseconds).
 */
#define	CMSET_TIMEOUT		1
#define	CMGET_TIMEOUT		2
#define	CMSET_RETRY_TIMEOUT	3
#define	CMGET_RETRY_TIMEOUT	4

#ifdef __cplusplus
}
#endif

#endif	/* _CLUSTM_H_ */
