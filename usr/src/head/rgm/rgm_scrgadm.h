/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */


#ifndef _RGM_SCRGADM_H
#define	_RGM_SCRGADM_H

#pragma ident	"@(#)rgm_scrgadm.h	1.31	08/08/01 SMI"

/*
 * This file contains a list of the high level project-private API
 * functions which are called by scrgadm command, GUI or public API
 * to administer the configuration of the RGM objects stored in CCR.
 */


#include <rgm/rtreg.h>
#include <rgm/rgm_common.h>
#include <scha_err.h>
#include <scha.h>
#include <rgm/scha_priv.h>
#include <rgm/scha_strings.h>

#ifdef __cplusplus
extern "C" {
#endif

#define	FROM_SCRGADM	0
#define	FROM_SCSWITCH	1

/*
 * ************************
 *	Resource Type
 * ************************
 */

/*
 * rgm_scrgadm_getrtcnfg()
 *	This function is for retrieving the Resource Type configuration.
 *	It returns a pointer to rgm_rt_t struct (see rgm_common.h).
 *	If this function returns SCHA_ERR_NOERR, it is caller's
 *      responsibility to free all memory associated with the
 *      rgm_rt_t by calling rgm_free_rt().
 *
 * rgm_scrgadm_getrtlist()
 *	Upon success, "rts" is set to point to a NULL terminated
 *	array of pointers pointing to all of the registered Resource
 *	Types in the cluster.
 *
 *	Returns err_code of SCHA_ERR_NOERR on success;
 *	otherwise returns appropriate err_code and err_msg string.
 */
scha_errmsg_t rgm_scrgadm_getrtconf(char *rtname, rgm_rt_t **rt,
    const char *zone_cluster);

scha_errmsg_t rgm_scrgadm_getrtlist(char ***rts, const char *zonename);

/* Check some version number and unicity of the RT */
scha_errmsg_t rgm_rt_sanity_check(const rgm_rt_t *rt);


/*
 * rgm_scrgadm_parse2rt()
 *	This function basically converts the resource type infomation from
 *	the form of RtrFileResult into struct rgm_rt_t.
 *	Also, perform some sanity checkings on rt name, rt_description,
 *	property name and property default values
 *
 *	Returns err_code of SCHA_ERR_NOERR on success;
 *	otherwise returns appropriate err_code and err_msg string.
 */
scha_errmsg_t rgm_scrgadm_parse2rt(RtrFileResult *rtreg_res, rgm_rt_t **rt,
    const char *zonename);


/*
 * rgm_scrgadm_register_rt()
 *	This function registers a new Resource Type in RGM configuration.
 *	It calls calls the RGM api to create the CCR table for this RT.
 *	The flag bypass_install_mode is should be set to B_TRUE when -F
 *	option is specified in scrgadm.  The api will bypass the
 *	install_mode check for registering RT.  For other cases,
 *	bypass_install_mode should be B_FALSE.
 *
 *      It creates the RT table directly on the local-node without going
 *	through the RGM president.
 *
 *	Returns err_code of SCHA_ERR_NOERR on success;
 *	otherwise returns appropriate err_code and err_msg string.
 */
scha_errmsg_t rgm_scrgadm_register_rt(rgm_rt_t *rt,
    boolean_t bypass_install_mode, const char *zone);

/*
 * rgm_scrgadm_update_rt_instnodes()
 *	This function update the value of installed_node_list of the
 *	given Resource Type.
 *	All the properties of Resource Type is non-editable, except
 *	RT_installed_nodes.
 *
 *	Returns err_code of SCHA_ERR_NOERR on success;
 *	otherwise returns appropriate err_code and err_msg string.
 */
scha_errmsg_t rgm_scrgadm_update_rt_instnodes(char *rtname, char **inst_nodes,
    const char *zonename);


/*
 * rgm_scrgadm_unregister_rt()
 *	This function unregisters the given Resource Type for RGM configuration.
 *	All resources of the given Resource Type must be removed first by
 *	the caller.
 *
 *	Returns err_code of SCHA_ERR_NOERR on success;
 *	otherwise returns appropriate err_code and err_msg string.
 */
scha_errmsg_t rgm_scrgadm_unregister_rt(char *rtname, const char *zonename);


/*
 * *************************
 *	Resource Group
 * *************************
 */

/*
 * rgm_scrgadm_getrgconf()
 *	This function is for retrieving the configuration of RG.
 *	If this function returns SCHA_ERR_NOERR, it is caller's
 *      responsibility to free all memory associated with the
 *      rgm_rg_t by calling rgm_free_rg().
 *
 * rgm_scrgadm_getrglist()
 *	Upon success, this function sets "managed_rgs" to a NULL terminated
 *	array of pointers to all of the MANAGED RGs and sets "unmanaged_rgs"
 *	to a NULL terminated array of pointers to all of the UNMANAGED RGs.
 *
 *	Returns err_code of SCHA_ERR_NOERR on success;
 *	otherwise returns appropriate err_code and err_msg string.
 */
scha_errmsg_t rgm_scrgadm_getrgconf(char *rgname, rgm_rg_t **rg,
    const char *zonename);

scha_errmsg_t rgm_scrgadm_getrglist(char ***managed_rgs,
	char ***unmanaged_rgs, boolean_t only_local_rgs, const char *zonename);


/*
 * rgm_scrgadm_create_rg()
 *	This function creates a new Resource Group with the properties
 *	listed in "pl" which is a NULL terminated array of pointers to
 *	the structs of scha_property_t.
 *	When an RG is first created, it contains no Resources and is in
 *	the UNMANAGED state.
 *	The RG_ResourceList  can only be updated by rgm_scrgadm_add_resource()
 *	and rgm_scrgadm_delete_resource().
 *	The flag bypass_install_mode is should be set to B_TRUE when -F option
 *	is specified in scrgadm.  The api will bypass the install_mode
 *	check for creating RG.  For other cases, bypass_install_mode should
 *	be B_FALSE.
 *
 *	Returns err_code of SCHA_ERR_NOERR on success;
 *	otherwise returns appropriate err_code and err_msg string.
 */
scha_errmsg_t rgm_scrgadm_create_rg(
	char *rgname, scha_property_t **pl, boolean_t bypass_install_mode,
	const char *zonename);


/*
 * rgm_scrgadm_update_rg_property()
 *	This function updates the values of the list of specified RG properties.
 *
 *	Note that resource list rg_rsrclist is NOT updatable
 *	through this function, it is maintained by
 *	rgm_scrgadm_resource_add() and rgm_scrgadm_resource_remove().
 *
 *	The flg indicates where the function is called: scrgadm or scswitch.
 *
 *	rgnames contains the list of rgs for the update operation.
 *
 *	Returns err_code of SCHA_ERR_NOERR on success;
 *	otherwise returns appropriate err_code and err_msg string.
 */
scha_errmsg_t rgm_scrgadm_update_rg_property(char *rgname,
    scha_property_t **pl, uint_t flg, const char **rgnames,
    const char *zonename);


/*
 * rgm_scrgadm_update_rg_prop_no_validate()
 *	This function is the same as rgm_scrgadm_update_rg_property(),
 *	but suppresses the running of resource Validate methods.
 *
 *	This function is currently used for the purpose of removing
 *	RG_dependencies or RG_affinities upon an RG which is being
 *	force-deleted.  Since Validate methods are skipped,
 *	forced-deletion might leave the remaining resources
 *	or RGs in an invalid configuration.  This will be documented.
 */
scha_errmsg_t rgm_scrgadm_update_rg_prop_no_validate(char *rgname,
    scha_property_t **pl, uint_t flg, const char *zonename);


/*
 * rgm_scrgadm_remove_rg()
 *	This function removes the given RG.  It deletes the CCR table of
 *	the given RG.
 *
 *	The RG must be in UNMANAGED state and it should not contain any
 *	resource, that is, all its resources must be removed.
 *
 *	Returns err_code of SCHA_ERR_NOERR on success;
 *	otherwise returns appropriate err_code and err_msg string.
 *	v_flag is the verbose flag used by the newcli fopr the verbose
 *	print.
 */
scha_errmsg_t rgm_scrgadm_remove_rg(char *rgname, boolean_t v_flag,
    const char *zone);


/*
 * *******************
 *	Resource
 * *******************
 */

/*
 * rgm_scrgadm_getrsrcconf()
 * rgm_scrgadm_getrsrclist()
 *	These functions retrieve the configuration of resource(s).
 *	If these functions return SCHA_ERR_NOERR, it is caller's
 *      responsibility to free all memory associated with the
 *      rgm_resource_t by calling rgm_free_resource() or
 *	rgm_free_resourcelist().
 *
 *	Returns err_code of SCHA_ERR_NOERR on success;
 *	otherwise returns appropriate err_code and err_msg string.
 */
scha_errmsg_t rgm_scrgadm_getrsrcconf(
	char *resourcename, rgm_resource_t **resource,
	const char *zonename);
scha_errmsg_t rgm_scrgadm_getrsrclist(
	char *rgname, char ***resources, const char *zonename);


/*
 * rgm_scrgadm_check_resource_prop()
 *	This function is provided for GUI or commands to validate the input
 *	value of a single property.  Caller needs to call
 *	rgm_scrgamd_getrtconf() first to get the rt infomation and passs
 *	it to this function.  And this function will validate the value
 *	base on the property definition stored in "rt".
 *
 *	Returns err_code of SCHA_ERR_NOERR on success;
 *	otherwise returns appropriate err_code and err_msg string.
 */
scha_errmsg_t rgm_scrgadm_check_resource_prop(
	rgm_rt_t *rt, char *prop_name, char *value);


/*
 * rgm_scrgadm_add_resource()
 *	This function adds a new resource with the properties values listed
 *	in "rpl" (See the comments of scha_resource_properties_t).
 *	The flag bypass_install_mode is should be set to B_TRUE when -F option
 *	is specified in scrgadm.  The api will bypass the install_mode
 *	check for creating Resource.  For other cases bypass_install_mode
 *	should by B_FALSE.
 *	Note: Resource name must be unique in the cluster.
 *	The RG may be currently managed or unmanaged.
 *
 *	Returns err_code of SCHA_ERR_NOERR on success;
 *	otherwise returns appropriate err_code and err_msg string.
 */
scha_errmsg_t rgm_scrgadm_add_resource(
	char *rname, char *rtname, char *rgname,
	scha_resource_properties_t *rpl, boolean_t bypass_install_mode,
	const char *zonename);


/*
 * rgm_scrgadm_update_resource_property()
 *	This function updates the values of the list of specified resource
 *	properties.
 *
 *      Note that this function cannot be used to change the value of
 *	on/off_switch or monitored_switch for the Resource.  You have to
 *	to use rgm_scswitch_set_resource_switch() instead.
 *
 *	Returns err_code of SCHA_ERR_NOERR on success;
 *	otherwise returns appropriate err_code and err_msg string.
 */
scha_errmsg_t rgm_scrgadm_update_resource_property(char *rname,
    scha_resource_properties_t *rpl, const char *zonename);


/*
 * rgm_scrgadm_update_rs_prop_no_validate()
 *	This function updates the value of the specified Resource property,
 *	and suppresses the running of validate methods.  This function
 *	must be used with caution, since it allows invalid resource
 *	configurations to be created.  Do *not* use this function to
 *	perform arbitrary resource property updates; instead use
 *	rgm_scrgadm_update_resource_property(), which executes Validate
 *	methods.
 *
 *	Returns err_code of SCHA_ERR_NOERR on success;
 *	otherwise returns appropriate err_code and err_msg string.
 */
scha_errmsg_t rgm_scrgadm_update_rs_prop_no_validate(char *rname,
    scha_resource_properties_t *rpl, const char *zonename);


/*
 * rgm_scrgadm_delete_resource()
 *	This function removes the resouce information from RGM configuration.
 *
 *	Returns err_code of SCHA_ERR_NOERR on success;
 *	otherwise returns appropriate err_code and err_msg string.
 *	verbose_flag is the verbose flag used by the newcli fopr the verbose
 *	print.
 */
scha_errmsg_t rgm_scrgadm_delete_resource(char *resourcename,
    boolean_t verbose_flag, const char *zonename);


/*
 * rgm_scrgadm_get_default_proj()
 *	Find the RG project name stored in CCR of the given RG.
 *	If the RG project name is NULL (never been set), return the system
 *	default project "default".  Caller must free allocated memory.
 *	rgname can be NULL.  In this case, "default" project is returned.
 */
scha_errmsg_t rgm_scrgadm_get_default_proj(char *rgname, char **projectname,
    const char *zonename);


scha_errmsg_t rgm_scrgadm_convert_namelist_to_nodeidlist(
	namelist_t *namelist_in, nodeidlist_t **nodeidlist_out);
#if (SOL_VERSION >= __s10)
scha_errmsg_t rgm_scrgadm_convert_zc_namelist_to_nodeidlist(
	namelist_t *namelist_in, nodeidlist_t **nodeidlist_out,
	char *zc_name);
#endif
char *get_canonical_name(const char *);
scha_errmsg_t get_swtch(switch_t r_onoff_switch, char *name,
    scha_switch_t *swith);
#if (SOL_VERSION >= __s10)
scha_errmsg_t get_swtch_zc(switch_t r_onoff_switch, char *name,
    scha_switch_t *swith, char *zcname);
#endif
scha_errmsg_t get_value(value_t rp_value, char *name, char **value,
    boolean_t is_per_node);


/* SC SLM addon start */
/*
 * rgm_scrgadm_get_rg_slm_type()
 *	Retrieve the SLM type in CCR or retrun the default one : "manual"
 */
scha_errmsg_t rgm_scrgadm_get_rg_slm_type(char *rg_name, char **slm_type,
    const char *zonename);


/*
 * rgm_scrgadm_get_rg_slm_pset_type()
 *	Retrieve the SLM pset type in CCR or retrun the default one : "default"
 */
scha_errmsg_t rgm_scrgadm_get_rg_slm_pset_type(
    char *rg_name, char **slm_pset_type, const char *zonename);

/* SC SLM addon end */


#ifdef __cplusplus
}
#endif

#endif /* _RGM_SCRGADM_H */
