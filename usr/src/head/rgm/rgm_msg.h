/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_RGM_MSG_H
#define	_RGM_MSG_H

#pragma ident	"@(#)rgm_msg.h	1.9	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * For all strings that are not to be internationalized via gettext.
 * All strings are wrapped with gettext, NOGET, or SYSTEXT.
 * SYSTEXT is used for all syslog messages that should be internationalized
 * if these messages were logged to the syslog.
 * NOGET is used for all messages that should never be inernationalized.
 */

#define	SYSTEXT(s)	s
#define	NOGET(s)	s

#ifdef __cplusplus
}
#endif

#endif	/* !_RGM_MSG_H */
