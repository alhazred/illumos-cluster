/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RGM_RECEP_H_
#define	_RGM_RECEP_H_

#pragma ident	"@(#)rgm_recep.h	1.7	08/05/20 SMI"


/*
 * rgm_recep.h - interface to the Rgm Receptionist
 */

#if !DOOR_IMPL
/*
 * Receptionist is using RPC instead of Solaris doors.
 */
#include <rgm/rpc/rgm_recep.h>
#else
/*
 * Receptionist is using Solaris doors.
 */

#ifdef __cplusplus
extern "C" {
#endif


#include <rpc/rpc.h>
#include <rpc/types.h>
#include <thread.h>
#include <rgm/security.h>
#include <string.h>
#include <rgm/door/rgm_recep_door.h>

typedef struct scha_input_args_t {
	int api_name;
	void *data;
} scha_input_args_t;

typedef struct scha_result_args_t {
	void *data;
} scha_result_args_t;

extern int scha_door_clnt(void *args, void *result, int scha_api_name,
    client_handle *clnt);

extern void recep_door_server(void *cookie, char *dataptr, size_t data_size,
    door_desc_t *desc_ptr, uint_t ndesc);

/* Name of door service */
#define	SCHA_PROGRAM_NAME	"rgmd_receptionist"


#define	SCHA_RS_OPEN			1
extern  bool_t scha_rs_open_1_svc(rs_open_args *, rs_open_result_t *);
#define	SCHA_RS_GET			2
extern  bool_t scha_rs_get_1_svc(rs_get_args *, get_result_t *);
#define	SCHA_RS_GET_EXT			3
extern  bool_t scha_rs_get_ext_1_svc(rs_get_args *, get_ext_result_t *);
#define	SCHA_RS_GET_STATE		4
extern  bool_t scha_rs_get_state_1_svc(rs_get_state_status_args *,
    get_result_t *);
#define	SCHA_RS_GET_STATUS		5
extern  bool_t scha_rs_get_status_1_svc(rs_get_state_status_args *,
    get_status_result_t *);
#define	SCHA_RS_GET_FAILED_STATUS	6
extern  bool_t scha_rs_get_failed_status_1_svc(rs_get_fail_status_args *,
    get_result_t *);
#define	SCHA_GET_ALL_EXTPROPS		7
extern  bool_t scha_get_all_extprops_1_svc(rs_get_args *, get_result_t *);
#define	SCHA_RS_GET_SWITCH		8
extern  bool_t scha_rs_get_switch_1_svc(rs_get_switch_args *,
    get_switch_result_t *);
#define	SCHA_RS_SET_STATUS		9
extern  bool_t scha_rs_set_status_1_svc(rs_set_status_args *,
    set_status_result_t *);
#define	SCHA_RT_OPEN			10
extern  bool_t scha_rt_open_1_svc(rt_open_args *, open_result_t *);
#define	SCHA_RT_GET			11
extern  bool_t scha_rt_get_1_svc(rt_get_args *, get_result_t *);
#define	SCHA_RG_OPEN			20
extern  bool_t scha_rg_open_1_svc(rg_open_args *, open_result_t *);
#define	SCHA_RG_GET			21
extern  bool_t scha_rg_get_1_svc(rg_get_args *, get_result_t *);
#define	SCHA_RG_GET_STATE		22
extern  bool_t scha_rg_get_state_1_svc(rg_get_state_args *, get_result_t *);
#define	SCHA_CONTROL			23
extern  bool_t scha_control_1_svc(scha_control_args *, scha_result_t *);
#define	SCHA_CLUSTER_GET		30
extern  bool_t scha_cluster_get_1_svc(cluster_get_args *, get_result_t *);
#define	SCHA_LB				31
extern  bool_t scha_lb_1_svc(scha_ssm_lb_args *, scha_ssm_lb_result_t *);
#define	SCHA_SSM_IP			34
extern  bool_t scha_ssm_ip_1_svc(ssm_ip_args *, ssm_result_t *);
#define	SCHA_CLUSTER_OPEN		35
extern  bool_t scha_cluster_open_1_svc(void *, open_result_t *);
#define	SCHA_IS_PERNODE			36
extern  bool_t scha_is_pernode_1_svc(rt_get_args *, get_result_t *);


extern int scha_program_1_freeresult(SVCXPRT *, xdrproc_t, caddr_t);

/* the xdr functions */
extern void scha_result_args_xdr_free(int api_num, void *result);
extern void scha_input_args_xdr_free(scha_input_args_t *scha_args);
extern size_t scha_xdr_sizeof(scha_input_args_t *arg);
extern size_t scha_xdr_sizeof_result(void *data, int api);
extern bool_t xdr_scha_input_args(XDR*, scha_input_args_t*);
extern bool_t xdr_scha_result_args(XDR *, void *, int);

#ifdef __cplusplus
}
#endif

#endif /* linux */
#endif /* _RGM_RECEP_H_ */
