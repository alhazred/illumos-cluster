/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * This file contains communication related utilities definitions
 * and wrappers around IDL interfaces, mostly implemented in
 * rgm_comm.cc
 */

#ifndef	_RGM_CALL_PRES_H
#define	_RGM_CALL_PRES_H

#pragma ident	"@(#)rgm_call_pres.h	1.37	08/06/20 SMI"

#ifdef	__cplusplus
extern "C" {
#endif

#include <rgm/rgm_scrgadm.h>
#include <rgm/security.h>
#if DOOR_IMPL
#include <rgm/rgm_recep.h>
#else
#include <rgm/rpc/rgm_recep.h>
#endif

//
// Client side functions executed at the receptionist or in the
// admin utiltities to execute corrosponding admin IDL function
// on the RGM president.
// Return value is zero for a successful IDL call, a non-zero
// return value signifies IDL exceptions.
// Logical errors are supposedly returned by the president in the
// associated "result" argument. The IDL layer does't care about
// those. Those are passed thru from the president to the receptionist.
//

// List of functions needed for public API

scha_errmsg_t call_scha_rs_get_switch(
    const rs_get_switch_args *gets_args, get_switch_result_t *gets_res);

scha_errmsg_t call_scha_rs_get_state(
    const rs_get_state_status_args *gets_args, get_result_t *gets_res);
scha_errmsg_t call_scha_rs_get_ext(const rs_get_args *rs_args,
    get_ext_result_t *rs_res);
scha_errmsg_t call_scha_rs_get_status(
    const rs_get_state_status_args *getst_args, get_status_result_t *getst_res);

scha_errmsg_t call_scha_rs_set_status(
    const rs_set_status_args *setst_args, set_status_result_t *setst_res);

scha_errmsg_t call_scha_rs_get_fail_status(
    const rs_get_fail_status_args *getf_args, get_result_t *getf_res);

scha_errmsg_t call_scha_rg_get_state(const rg_get_state_args *rggs_args,
    get_result_t *rggs_res);

// List of functions in scrgadm

scha_errmsg_t 	call_scrgadm_rg_create(const char *rg_name,
    scha_property_t **pl, const char *cluster_name);

scha_errmsg_t	call_scrgadm_update_rt_instnodes(const char *rt_name,
    const char **inst_nodes, const char *cluster_name);

scha_errmsg_t	call_scrgadm_rt_unregister(const char *rt_name,
    const char *cluster_name);

scha_errmsg_t 	call_scrgadm_rg_update_prop(const char *rgname,
    const scha_property_t **pl, uint_t flg, boolean_t suppress_validate,
    const char **rgnames, const char *cluster_name);

scha_errmsg_t 	call_scrgadm_rg_remove(const char *rgname, boolean_t v_flag,
	const char *cluster_name);

scha_errmsg_t call_scrgadm_rs_add(const char *rname, const char *rtname,
    const char *rgname, const scha_resource_properties_t *rpl,
    const char *cluster_name);

scha_errmsg_t 	call_scrgadm_rs_update_prop(const char *rname,
    const scha_resource_properties_t *rpl, boolean_t suppress_validate,
    const char *cluster_name);

scha_errmsg_t 	call_scrgadm_rs_delete(const char *rname,
    boolean_t verbose_flag, const char *cluster_name);

scha_errmsg_t	call_event_gen_membership(const char *zone);

scha_errmsg_t	call_event_gen_rg_state(const char *rg_name, const char *zone);

scha_errmsg_t	call_event_gen_rs_state(const char *rs_name, const char *zone);

#ifdef	__cplusplus
}
#endif

#endif	/* !_RGM_CALL_PRES_H */
