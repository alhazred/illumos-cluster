/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * rgm_cmn_proto.h
 * function prototypes for routines common to librgmserver.a
 * and librgm.so.1
 */

#ifndef	_RGM_CMN_PROTO_H
#define	_RGM_CMN_PROTO_H

#pragma ident	"@(#)rgm_cmn_proto.h	1.22	08/06/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#include <h/rgm.h>
#include <rgm/rgm_cpp.h>
#include <rgm/rgm_malloc.h>
#include <rgm/scha_priv.h>

boolean_t fail_ccr_read_write(void);

const char *fetch_msg_locale(void);

char *new_str(const char *str);

scha_errmsg_t except_to_scha_err(Environment &e);

void array_to_strseq(const char **list,
    rgm::idl_string_seq_t &str_seq);

void plist_to_nv_seq(const scha_property_t **pl,
    rgm::idl_nameval_seq_t &nv_seq);
void plist_to_nv_seq_ext(const scha_property_t **pl,
    rgm::idl_nameval_node_seq_t &nv_seq);


/* realloc() does not exit on NULL return, but realloc_err() does. */
extern void *realloc_err(void *ptr, size_t size);

extern scha_errmsg_t rgm_orbinit();

extern rgm::rgm_comm_ptr rgm_comm_getpres_zc(char *zonename);

extern rgm::rgm_comm_ptr rgm_comm_getpres();
extern void rgm_format_errmsg(scha_errmsg_t *res, const char *fmt, ...);
extern void rgm_format_successmsg(scha_errmsg_t *res, const char *fmt, ...);

extern void rgm_sprintf(scha_errmsg_t *res, const char *fmt, ...);

extern char *strdup_nocheck(const char *s1);

extern boolean_t is_rgm_value_valid(const char *value, boolean_t allow_comma);

extern boolean_t illegal_rt_version_chars(const char *version,
    boolean_t sc30rt);

extern boolean_t is_an_int_value(const char *value);

extern boolean_t is_rgm_objname_valid(const char *objname,
    boolean_t is_ccr_table);

extern boolean_t is_valid_zonename(const char *ptr);

extern rgm_property_list_t *rgm_find_property_in_list(char *name,
    rgm_property_list_t *prop_list);

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* _RGM_CMN_PROTO_H */
