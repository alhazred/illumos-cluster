/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_RGM_COMM_IMPL_H
#define	_RGM_COMM_IMPL_H

#pragma ident	"@(#)rgm_comm_impl.h	1.71	08/06/27 SMI"

#include <orb/object/adapter.h>
#include <h/rgm.h>
#include <cmm/ucmm_api.h>

#ifdef	__cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

/*
 * macros for converting the evac timeout bit field:
 */
#define	SET_EVAC_STICKY_TIMEOUT(s)  ((uint_t)(s) << 16)
#define	GET_EVAC_STICKY_TIMEOUT(n)  ((uint_t)(n) >> 16)

/*CSTYLED*/
class rgm_comm_impl : public McServerof<rgm::rgm_comm> {
private:
	//
	// Public interface
	//
public:
	rgm_comm_impl();
	~rgm_comm_impl() {}
	void _unreferenced(unref_t);

	//
	// Methods that implement the server side of the IDL interface
	//
	void    rgm_xmit_state(rgm::node_state_out state, Environment &);

	void	rgm_set_pres(sol::nodeid_t node,
			sol::incarnation_num incarnation, Environment &);

	void	notify_me_state_change(
			rgm::lni_t lni,
			rgm::notify_change_tag change_tag,
			const char *R_RG_name,
			rgm::rgm_r_state rstate,
			rgm::rgm_rg_state rgstate,
			Environment &);

	void	rgm_r_restart(
			sol::nodeid_t president,
			sol::incarnation_num incarnation,
			rgm::lni_t lni,
			rgm::ln_incarnation_t ln_incarnation,
			rgm::rgm_restart_flag rrflag,
			const char *R_name, Environment &);

	void	rgm_chg_freeze(
			sol::nodeid_t president,
			sol::incarnation_num incarnation,
			const rgm::idl_string_seq_t &freezelist, Environment &);

	void	rgm_ack_start(
			sol::nodeid_t president,
			sol::incarnation_num incarnation,
			rgm::lni_t lni,
			rgm::ln_incarnation_t ln_incarnation,
			const char *R_name, Environment &);

	void	rgm_notify_dependencies_resolved(
			sol::nodeid_t president,
			sol::incarnation_num incarnation,
			rgm::lni_t lni,
			rgm::ln_incarnation_t ln_incarnation,
			const char *R_name, Environment &);

	void	rgm_fetch_restarting_flag(
			sol::nodeid_t president,
			sol::incarnation_num incarnation,
			rgm::lni_t lni,
			rgm::ln_incarnation_t ln_incarnation,
			const char *R_name,
			rgm::r_restart_t &restart_flag, Environment &);

	void	rgm_chg_mastery(
			sol::nodeid_t president,
			sol::incarnation_num incarnation,
			rgm::lni_t lni,
			rgm::ln_incarnation_t ln_incarnation,
			const rgm::idl_string_seq_t &onlist,
			const rgm::idl_string_seq_t &offlist, Environment &);

	void	rgm_am_i_joining(
			bool &is_joining,
			Environment &);

	void	rgm_slave_record_memb(
			sol::nodeid_t president,
			sol::incarnation_num incarnation,
			nodeset_t curr_ns,
			Environment &);

	void	rgm_read_ccr(
			sol::nodeid_t president,
			sol::incarnation_num incarnation,
			Environment &);

	void	rgm_read_ccr(
			sol::nodeid_t president,
			sol::incarnation_num incarnation,
			const rgm::lni_mappings_seq_t &lni_map,
			Environment &);

	void	rgm_run_boot_meths(
			sol::nodeid_t president,
			sol::incarnation_num incarnation,
			Environment &);

	void 	idl_scha_rs_get_switch(
			const rgm::idl_rs_get_switch_args &getst_args,
			rgm::idl_get_switch_result_t_out getst_res,
			Environment &e);

	void 	idl_scha_rs_get_state(
			const rgm::idl_rs_get_state_status_args &gets_args,
			rgm::idl_get_result_t_out gets_res, Environment &e);

	void 	idl_scha_rs_get_status(
			const rgm::idl_rs_get_state_status_args &getst_args,
			rgm::idl_get_status_result_t_out getst_res,
			Environment &e);

	void 	idl_scha_rs_set_status(
			const rgm::idl_rs_set_status_args &setst_args,
			rgm::idl_set_status_result_t_out setst_res,
			Environment &e);

	void 	idl_scha_rs_get_fail_status(
			const rgm::idl_rs_get_fail_status_args &getf_args,
			rgm::idl_get_result_t_out getf_res, Environment &e);

	void 	idl_scha_rg_get_state(
			const rgm::idl_rg_get_state_args &rggs_args,
			rgm::idl_get_result_t_out rggs_res, Environment &e);

	void	idl_scrgadm_rt_update_instnodes(
			const rgm::idl_rt_update_instnode_args &rtui_args,
			rgm::idl_regis_result_t_out rtui_res, Environment &e);

	void	idl_scrgadm_rt_delete(const char *rtname, const char *locale,
			rgm::idl_regis_result_t_out rtui_res, Environment &e);

	void 	idl_scrgadm_rg_create(
			const rgm::idl_rg_create_args &rgcr_args,
			rgm::idl_regis_result_t_out rtui_res, Environment &e);

	void	idl_scrgadm_rg_update_prop(
			const rgm::idl_rg_update_prop_args &rgup_args,
			rgm::idl_regis_result_t_out rgup_res, Environment &e);

	void	idl_scrgadm_rg_update_prop_v1(
			const rgm::idl_rg_update_prop_args_v1 &rgup_args,
			rgm::idl_regis_result_t_out rgup_res, Environment &e);

	void 	idl_scrgadm_rg_remove(
			const rgm::idl_rg_remove_args &rgrm_args,
			rgm::idl_regis_result_t_out rgrm_res,
			bool verbose_flag, Environment &e);

	void 	idl_scrgadm_rs_add(const rgm::idl_rs_add_args &rsad_args,
			rgm::idl_regis_result_t_out rsad_res, Environment &e);

	void 	idl_scrgadm_rs_update_prop(
			const rgm::idl_rs_update_prop_args &rsup_args,
			rgm::idl_regis_result_t_out rsad_res, Environment &e);

	void 	idl_scrgadm_rs_validate(
			sol::nodeid_t president,
			sol::incarnation_num incarnation,
			rgm::lni_t lni,
			rgm::ln_incarnation_t ln_incarnation,
			const rgm::idl_validate_args &rs_args,
			rgm::idl_regis_result_t_out rs_res,
			Environment &);

	void 	idl_scrgadm_rs_delete(
			const rgm::idl_rs_delete_args &rsdel_args,
			rgm::idl_regis_result_t_out rsdel_res,
			bool verbose_flag,
			Environment &e);

	void	idl_scswitch_primaries(
			const rgm::idl_scswitch_primaries_args &scswp_args,
			rgm::idl_regis_result_t_out scswp_res,
			bool verbose_flag, Environment &e);

	void	idl_quiesce_rgs(
			const rgm::idl_quiesce_rgs_args &scq_args,
			rgm::idl_regis_result_t_out scq_res,
			bool verbose_flag, Environment &e);

	void	idl_kill_methods(
			const char *rgname,
			const rgm::idl_string_seq_t &rsnames,
			Environment &e);

	void	idl_scswitch_onoff(
			const rgm::idl_scswitch_onoff_args &scswoo_args,
			rgm::idl_regis_result_t_out scswp_res,
			bool verbose_flag, Environment &e);

	void    idl_scswitch_clear(
			const rgm::idl_scswitch_clear_args	&scsw_clr_args,
			rgm::idl_regis_result_t_out		scsw_clr_res,
			Environment	&);

	void	idl_scswitch_restart_rg(
			const rgm::idl_scswitch_primaries_args &scswp_args,
			rgm::idl_regis_result_t_out scswp_res,
			bool verbose_flag, Environment &e);

	void    rgm_clear_flag(
			sol::nodeid_t president,
			sol::incarnation_num incarnation,
			rgm::lni_t lni,
			rgm::ln_incarnation_t ln_incarnation,
			const rgm::idl_string_seq_t &scsw_clr_rs,
			rgm::scswitch_clear_flags flag,
			rgm::idl_regis_result_t_out scsw_clr_ret,
			Environment &e);

	void	idl_latch_intention(
			sol::nodeid_t president,
			sol::incarnation_num incarnation,
			rgm::lni_t lni,
			rgm::ln_incarnation_t ln_incarnation,
			const rgm::rgm_intention &intent,
			Environment &e);

	void	idl_process_intention(
			sol::nodeid_t president,
			sol::incarnation_num incarnation,
			Environment &e);

	void	idl_unlatch_intention(
			sol::nodeid_t president,
			sol::incarnation_num incarnation,
			Environment &e);

	void	idl_unmanaged_to_offline(const rgm::idl_string_seq_t &rgnames,
			const char *locale,
			rgm::idl_regis_result_t_out result, bool verbose_flag,
			sol::nodeid_t nodeid, const char *idlstr_zonename,
			Environment &e);

	void	idl_offline_to_unmanaged(const rgm::idl_string_seq_t &rgnames,
			const char *locale,
			rgm::idl_regis_result_t_out result, bool verbose_flag,
			sol::nodeid_t nodeid, const char *idlstr_zonename,
			Environment &e);

	void	idl_scha_control_giveover(const rgm::idl_scha_control_args &arg,
			rgm::idl_scha_control_result_out result,
			Environment &e);

	void	idl_scha_control_restart(const rgm::idl_scha_control_args &arg,
			rgm::idl_regis_result_t_out result, Environment &e);

	void	idl_scha_control_checkall(
			sol::nodeid_t president,
			sol::incarnation_num incarnation,
			rgm::ln_incarnation_t ln_incarnation,
			const rgm::idl_scha_control_checkall_args &args,
			rgm::idl_scha_control_result_out result,
			Environment &e);

	void	idl_set_timestamp(const char *r_name, rgm::lni_t lni,
			rgm::idl_regis_result_t_out result, Environment &e);

	void	idl_scha_get_node_state(
			const rgm::idl_cluster_get_args &getcl_args,
			rgm::idl_get_result_t_out getcl_res, Environment &e);

	void	idl_event_gen_membership(
			rgm::idl_regis_result_t_out result, Environment &e);

	void	idl_event_gen_rg_state(const char *rg_name,
			rgm::idl_regis_result_t_out result, Environment &e);

	void	idl_event_gen_rs_state(const char *rs_name,
			rgm::idl_regis_result_t_out result, Environment &e);


	void	idl_retrieve_lni_mappings(
			rgm::lni_mappings_seq_t_out lni_map,
			Environment &e);

	void	idl_zone_state_change(
			sol::nodeid_t node,
			const char *zonename,
			rgm::rgm_ln_state state,
			rgm::ln_incarnation_t incarn,
			rgm::lni_t &assigned_lni,
			Environment &e);

	void	idl_send_lni_mappings(
			sol::nodeid_t president,
			sol::incarnation_num incarnation,
			const rgm::lni_mappings_seq_t &lni_map,
			Environment &e);

	void	idl_reclaim_lnis(sol::nodeid_t node,
			sol::incarnation_num incarnation,
			rgm::lni_seq_t &lni_seq,
			Environment &e);

	void	idl_get_logicalnodes(sol::nodeid_t nodeid, bool exclude_global,
			rgm::idl_string_seq_t_out lns, Environment &e);
	void	idl_scha_control_disable(const rgm::idl_scha_control_args &arg,
			rgm::idl_regis_result_t_out result, Environment &e);

	void 	idl_scha_rs_get_ext(
			const rgm::idl_rs_get_args &rs_args,
			rgm::idl_rs_get_ext_result_t_out rs_res,
			Environment &e);

#if SOL_VERSION >= __s10
	void	idl_get_remote_r_state(
			const rgm::idl_rs_get_state_args &gets_args,
			rgm::idl_rs_get_state_result_t_out gets_res,
			Environment &);

	void	idl_get_remote_rg_state(
			const rgm::idl_remote_rg_get_state_args &gets_args,
			rgm::idl_remote_rg_get_state_result_t_out gets_res,
			Environment &);

	void	idl_notify_remote_rg(
			const rgm::idl_notify_remote_rg_args &gets_args,
			rgm::idl_notify_remote_rg_result_t_out gets_res,
			Environment &);

	void	idl_notify_remote_r(
			const rgm::idl_rs_get_state_args &gets_args,
			rgm::idl_regis_result_t_out gets_res,
			Environment &);

	void    idl_ext_rg_validate_prop(
		    const rgm::idl_rg_validate_prop_args &gets_args,
		    rgm::idl_regis_result_t_out gets_res,
			Environment &);

	void idl_check_remote_rg_sp_aff(
			const rgm::idl_remote_rg_sp_aff_check_args &gets_args,
			rgm::idl_remote_rg_sp_aff_check_result_t_out gets_res,
			Environment &);

	void idl_is_rgm_alive(
			Environment &);

	void idl_reconfig_ready_to_do_step(
			sol::nodeid_t nid,
			uint32_t step_num,
			cmm::seqnum_t seqnum,
			Environment &);

	void idl_reconfig_do_step(
			uint32_t step_num,
			cmm::seqnum_t seqnum,
			rgm::sync_result_t sync_result,
			Environment &);

	void    idl_get_remote_rsrcs_in_dep_line_of_r(
			const rgm::idl_rs_get_icrd_rsrcs_args &rs_args,
			rgm::idl_rs_get_icrd_rsrcs_result_t_out rs_res,
			Environment &e);

	void	rgm_force_rg_offline(
			sol::nodeid_t president,
			sol::incarnation_num incarnation,
			rgm::lni_t lni,
			rgm::ln_incarnation_t ln_incarnation,
			const char *RG_name, Environment &);
#endif
};

#ifdef	__cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* _RGM_COMM_IMPL_H */
