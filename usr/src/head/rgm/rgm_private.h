/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RGM_PRIV_H_
#define	_RGM_PRIV_H_

#pragma ident	"@(#)rgm_private.h	1.25	08/06/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif


#include "rgm_util.h"

/* get/put string type of data in ccr table */
extern scha_errmsg_t ccr_get_string(const ccr::readonly_table_var &,
	const char *, char **);
extern scha_errmsg_t ccr_put_string(const ccr::updatable_table_var &,
	const char *, const char *);

/* get/put integer type of data in ccr table */
extern scha_errmsg_t ccr_get_int(const ccr::readonly_table_var &, const char *,
	int *);
extern scha_errmsg_t ccr_put_int(const ccr::updatable_table_var &,
	const char *, int);

/* get/put boolean type of data in ccr table */
extern scha_errmsg_t ccr_get_bool(const ccr::readonly_table_var &,
	const char *, boolean_t *);
extern scha_errmsg_t ccr_put_bool(const ccr::updatable_table_var &,
	const char *, boolean_t);

/* get/put list of strings type of data in ccr table */
extern scha_errmsg_t ccr_get_nstring(const ccr::readonly_table_var &,
	const char *, namelist_t **);
extern scha_errmsg_t ccr_put_nstring(const ccr::updatable_table_var &,
	const char *, namelist_t *);

/* get/put type for list that may have special ALL value in ccr table */
extern scha_errmsg_t ccr_get_nstring_all(const ccr::readonly_table_var &,
	const char *, namelist_all_t *);
extern scha_errmsg_t ccr_put_nstring_all(const ccr::updatable_table_var &,
	const char *, namelist_all_t *);

/* get/put rgm_resource_t type of data in ccr table */
extern scha_errmsg_t ccr_get_resource(const ccr::readonly_table_var &,
	const char *, rgm_resource_t **, const char *);
extern scha_errmsg_t ccr_put_resource(const ccr::updatable_table_var &,
	const char *, rgm_resource_t *);

extern scha_errmsg_t ccr_put_resource_group(const ccr::updatable_table_var &,
    const ccr::element_seq &);

extern scha_errmsg_t ccr_get_paramtable(const ccr::readonly_table_var &,
	rgm_param_t ***);
extern scha_errmsg_t ccr_put_paramtable(const ccr::updatable_table_var &,
	rgm_param_t **);

extern scha_errmsg_t ccr_get_rt_upgrade(const ccr::readonly_table_var &,
	boolean_t *, rgm_rt_upgrade_t **, rgm_rt_upgrade_t **);

extern scha_errmsg_t ccr_get_nodeidlist(const ccr::readonly_table_var &tabptr,
    const char *key, nodeidlist_t **data);
extern scha_errmsg_t ccr_get_nodeidlist_all(
	const ccr::readonly_table_var &tabptr,
	const char *key, nodeidlist_all_t *data);

extern void ccr_pack_string(ccr::element_seq &seq, const char *, const char *);

extern void ccr_pack_int(ccr::element_seq &seq, const char *key, int value);

extern void ccr_pack_bool(ccr::element_seq &seq, const char *key,
	boolean_t value);

extern void ccr_pack_nstring(ccr::element_seq &seq, const char *key,
    namelist_t *value);

extern void ccr_pack_nstring_all(ccr::element_seq &seq, const char *key,
    namelist_all_t *value);

extern void ccr_pack_paramtable(ccr::element_seq &seq,
    rgm_param_t **paramtable);


extern void ccr_pack_nodeidlist(ccr::element_seq &seq, const char *key,
    nodeidlist_t *value);

extern void ccr_pack_nodeidlist_all(ccr::element_seq &seq, const char *key,
    nodeidlist_all_t *value);

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* _RGM_PRIV_H_ */
