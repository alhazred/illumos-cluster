/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_RGM_METHODS_H
#define	_RGM_METHODS_H

#pragma ident	"@(#)rgm_methods.h	1.3	08/05/20 SMI"

/*
 * rgm_methods.h
 *
 * Definition of rgm methods types
 *
 */

#ifdef __cplusplus
extern "C" {
#endif

/*
 * The types of resource methods.
 * Stored in CCR and in struct pointed to by r_list_t's rl_ccrtype.
 * Given the method type and a pointer to the resource,
 * the is_method_reg() routine can determine whether the
 * method is registered for the resource and return its name.
 */
typedef enum method {
	METH_START,
	METH_STOP,
	METH_VALIDATE,
	METH_UPDATE,
	METH_INIT,
	METH_FINI,
	METH_BOOT,
	METH_MONITOR_START,
	METH_MONITOR_STOP,
	METH_MONITOR_CHECK,
	METH_PRENET_START,
	METH_POSTNET_STOP
} method_t;

#define	METH_FIRST_METHOD	METH_START
#define	METH_LAST_METHOD	METH_POSTNET_STOP
#define	METH_FEDSLM_BYPASS	-1

#ifdef __cplusplus
}
#endif

#endif	/* !_RGM_METHODS_H */
