/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */


/*
 *  ff.h - Failfast device driver header file.
 */

#ifndef	_SYS_FF_H
#define	_SYS_FF_H

#pragma ident	"@(#)ff.h	1.14	08/05/20 SMI"

#ifdef	__cplusplus
extern "C" {
#endif

/*
 * Supported ioctl calls.
 */
#define	FAILFAST_BASE		('f' << 8)

#define	FAILFAST_ARM		(FAILFAST_BASE|1)
#define	FAILFAST_DISARM		(FAILFAST_BASE|2)
#define	FAILFAST_DEBUG_MODE	(FAILFAST_BASE|3)
#define	FAILFAST_HALT_MODE	(FAILFAST_BASE|4)
#define	FAILFAST_PANIC_MODE	(FAILFAST_BASE|5)
#define	FAILFAST_SETNAME	(FAILFAST_BASE|6)
#define	FAILFAST_PROM_MODE	(FAILFAST_BASE|7)

/* the ff_name structure is used with the FAILFAST_SETNAME ioctl */
#define	MAXFFNAME	16

struct ff_name {
	int	length;
	char	name[MAXFFNAME];
};

typedef struct ff_name ff_name_t;

int ff_open(char *unit_name, int debug);
int ff_ioctl(int type, char *unit_name, long timeout);

#ifdef	__cplusplus
}
#endif

#endif	/* !_SYS_FF_H */
