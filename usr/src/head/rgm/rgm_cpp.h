/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RGM_CPP_H_
#define	_RGM_CPP_H_

#pragma ident	"@(#)rgm_cpp.h	1.3	08/05/20 SMI"


#include <rgm/rgm_common.h>
#include <map>

/*
 * This file contains information that is C++ specific. This file was
 * introduced as part of the RAC manageability project (1403) to store auxillary
 * C++ specific data structures of the rgm_resource_t.
 *
 * This file is a *private* file for the implementations.
 */

// Internal structures to store the r_onoff_switch and monitored switch.

typedef struct rgm_switch {
	std::map<rgm_lni_t, scha_switch_t>	r_switch;
	std::map<std::string, scha_switch_t>	r_switch_node;
} rgm_switch_t;

// This is an internal RGM structure to store the rp_value structure.
typedef struct rgm_value {
	std::map<rgm_lni_t, char *>	rp_value;
	std::map<std::string, char *>	rp_value_node;
} rgm_value_t;

void free_mdbrgxstate(mdb_rgxlogs_t& rgx_logs);
void free_mdbrxstate(mdb_rxlogs_t& rx_logs);
#endif /* _RGM_CPP_H_ */
