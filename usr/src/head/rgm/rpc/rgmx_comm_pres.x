%/*
% * CDDL HEADER START
% *
% * The contents of this file are subject to the terms of the
% * Common Development and Distribution License (the License).
% * You may not use this file except in compliance with the License.
% *
% * You can obtain a copy of the license at usr/src/CDDL.txt
% * or http://www.opensolaris.org/os/licensing.
% * See the License for the specific language governing permissions
% * and limitations under the License.
% *
% * When distributing Covered Code, include this CDDL HEADER in each
% * file and include the License file at usr/src/CDDL.txt.
% * If applicable, add the following below this CDDL HEADER, with the
% * fields enclosed by brackets [] replaced with your own identifying
% * information: Portions Copyright [yyyy] [name of copyright owner]
% *
% * CDDL HEADER END
% */

%/*
% * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
% * Use is subject to license terms.
% */

#ifdef RPC_HDR
%
%#pragma ident	"@(#)rgmx_comm_pres.x	1.3	08/05/20 SMI"
%
#endif

%/*
% * File: rgm_comm_pres.x: RPC interface to farm/server nodes communication
% *
% * The Farm RGM will call this RPC functions to communicate with the President.
% *
% * Subset of the rgm_comm IDL interface for actions executed on the president.
% * - rgmx_scha_rs_get_state
% * - rgmx_scha_rs_get_status
% * - rgmx_scha_rs_get_fail_status
% * - rgmx_scha_rs_set_status
% * - rgmx_scha_rg_get_state
% * - rgmx_scha_rg_get_switch
% * - rgmx_scha_get_node_state
% * - rgmx_scha_control_giveover
% * - rgmx_scha_control_restart
% * - rgmx_notify_me_state_change
% * 
% * Used to mimic the "scha_cluster_get" receptionist call.
% * - rgmx_scha_cluster_open
% * - rgmx_scha_cluster_get
% * 
% */

#ifdef RPC_HDR
%
%#include "rgmx_comm.h"
%
#endif

program RGMX_COMM_PRES_PROGRAM {
	version RGMX_COMM_PRES_VERSION {
		rgmx_get_result_t RGMX_SCHA_RS_GET_STATE(
		    rgmx_rs_get_state_status_args) = 1;
		rgmx_get_status_result_t RGMX_SCHA_RS_GET_STATUS(
		    rgmx_rs_get_state_status_args) = 2;
		rgmx_get_result_t RGMX_SCHA_RS_GET_FAIL_STATUS(
		    rgmx_rs_get_fail_status_args) = 3;
		rgmx_rs_get_switch_result_t RGMX_SCHA_RS_GET_SWITCH(
		    rgmx_rs_get_switch_args) = 4;
		rgmx_set_status_result_t RGMX_SCHA_RS_SET_STATUS(
		    rgmx_rs_set_status_args) = 5;
		rgmx_get_result_t RGMX_SCHA_RG_GET_STATE(
		    rgmx_rg_get_state_args) = 6;
		rgmx_get_result_t RGMX_SCHA_GET_NODE_STATE(
		    rgmx_cluster_get_args) = 7;
		rgmx_scha_control_result RGMX_SCHA_CONTROL_GIVEOVER(
		    rgmx_scha_control_args) = 8;
		rgmx_regis_result_t RGMX_SCHA_CONTROL_RESTART(
		    rgmx_scha_control_args) = 9;
		rgmx_regis_result_t RGMX_SCHA_CONTROL_DISABLE(
		    rgmx_scha_control_args) = 10;
		void RGMX_NOTIFY_ME_STATE_CHANGE(
		    rgmx_notify_me_state_change_args) = 11;
		rgmx_zone_state_change_result RGMX_ZONE_STATE_CHANGE(
		    rgmx_zone_state_change_args) = 12;
		rgmx_rs_get_ext_result_t RGMX_SCHA_RS_GET_EXT(
		    rgmx_rs_get_args) = 13;

		/*
  		 * types used below are defined in rgm_recep.h
		 */
		get_result_t RGMX_SCHA_CLUSTER_GET(cluster_get_args) = 30;
		open_result_t RGMX_SCHA_CLUSTER_OPEN() = 31;
	} = 1;
} = 100144;

#ifdef RPC_HDR
%/* Name of RPC service, as identified in rpc(4) */
%#define        RGMX_COMM_PRES_PROGRAM_NAME   "rgmx_comm_pres"
%
#endif
