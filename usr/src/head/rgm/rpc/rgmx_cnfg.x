%/*
% * CDDL HEADER START
% *
% * The contents of this file are subject to the terms of the
% * Common Development and Distribution License (the License).
% * You may not use this file except in compliance with the License.
% *
% * You can obtain a copy of the license at usr/src/CDDL.txt
% * or http://www.opensolaris.org/os/licensing.
% * See the License for the specific language governing permissions
% * and limitations under the License.
% *
% * When distributing Covered Code, include this CDDL HEADER in each
% * file and include the License file at usr/src/CDDL.txt.
% * If applicable, add the following below this CDDL HEADER, with the
% * fields enclosed by brackets [] replaced with your own identifying
% * information: Portions Copyright [yyyy] [name of copyright owner]
% *
% * CDDL HEADER END
% */

%/*
% * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
% * Use is subject to license terms.
% */

#ifdef RPC_HDR
%
%#pragma ident	"@(#)rgmx_cnfg.x	1.3	08/05/20 SMI"
%
#endif

%/*
% * File: rgm_cnfg.x: RPC interface to allow a farm node to access remote
% * CCR/Name server information (on server node).
% *
% * ATTENTION - THE DEFINITIONS IN THIS FILE MUST BE IN SYNC WITH
% * ./head/rgm/rgm_common.h
% *
% */

#ifdef RPC_HDR
%
%#include <sys/os.h>
%#include <rgm/rgm_common.h>
%#include <rgm/rgm_util.h>
%#include <rgmx_cnfg_common_xdr.h>
%
#endif

struct read_rttable_result_t {
	int		err;
	rgm_rt_t		*rgm_rt;
};

struct read_rgtable_result_t {
	int		err;
	rgm_rg_t		*rgm_rg;
};

struct get_resource_result_t {
	int		err;
	rgm_resource_t		*rgm_resource;
};

struct get_rtlist_result_t {
	int	err;
	namelist_t	*rts;
};

struct get_rglist_result_t {
	int	err;
	namelist_t	*managed_rgs;
	namelist_t	*unmanaged_rgs;
};

struct get_rsrclist_result_t {
	int	err;
	namelist_t	*rnames;
};

struct get_resource_args {
	string	res<>;
	string	rg<>;
};

struct get_property_args {
	string	objname<>;
	rgm_obj_type_t	objtype;
	string	key<>;
};

struct get_property_result_t {
	string	value<>;
	int	ret_code;
};

struct cl_resolve_result_t {
	int	ret_code;
	string	data<>;
};

program RGMX_CNFG_PROGRAM {
	version RGMX_CNFG_VERSION {
		read_rttable_result_t RGMX_CNFG_READ_RTTABLE(string) = 1;
		read_rgtable_result_t RGMX_CNFG_READ_RGTABLE(string) = 2;
		get_resource_result_t RGMX_CNFG_GET_RESOURCE
			(get_resource_args) = 3;
		get_rtlist_result_t RGMX_CNFG_GET_RTLIST(void) = 4;
		get_rglist_result_t RGMX_CNFG_GET_RGLIST(bool) = 5;
		get_rsrclist_result_t RGMX_CNFG_GET_RSRCLIST(string) = 6;
		get_property_result_t RGMX_CNFG_GET_PROPERTY_VALUE
			(get_property_args) = 7;
		cl_resolve_result_t RGMX_CNFG_CL_RESOLVE(string) = 8;
	} = 1;
} = 100143;

#ifdef RPC_HDR
%/* Name of RPC service, as identified in rpc(4) */
%#define	RGMX_CNFG_PROGRAM_NAME   "rgmx_cnfg"
%
#endif
