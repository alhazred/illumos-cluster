%/*
% * CDDL HEADER START
% *
% * The contents of this file are subject to the terms of the
% * Common Development and Distribution License (the License).
% * You may not use this file except in compliance with the License.
% *
% * You can obtain a copy of the license at usr/src/CDDL.txt
% * or http://www.opensolaris.org/os/licensing.
% * See the License for the specific language governing permissions
% * and limitations under the License.
% *
% * When distributing Covered Code, include this CDDL HEADER in each
% * file and include the License file at usr/src/CDDL.txt.
% * If applicable, add the following below this CDDL HEADER, with the
% * fields enclosed by brackets [] replaced with your own identifying
% * information: Portions Copyright [yyyy] [name of copyright owner]
% *
% * CDDL HEADER END
% */

%/*
% * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
% * Use is subject to license terms.
% */

#ifdef RPC_HDR
%
%#pragma ident	"@(#)rgmx_comm_farm.x	1.3	08/05/20 SMI"
%
#endif

%/*
% * File: rgmx_comm_farm.x  RPC interface to farm/server nodes communication
% *
% * The President will call this RPC functions to carry out actions 
% * on the farm nodes.
% *
% */

#ifdef RPC_HDR
%
%#include "rgmx_comm.h"
%
#endif

program RGMX_COMM_FARM_PROGRAM {
	version RGMX_COMM_FARM_VERSION {
		rgmx_regis_result_t RGMX_SCRGADM_RS_VALIDATE(
		    rgmx_validate_args) = 1;
		rgmx_scha_control_result RGMX_SCHA_CONTROL_CHECKALL(
		    rgmx_scha_control_checkall_args) = 2;
		rgmx_regis_result_t RGMX_SET_TIMESTAMP(
		    rgmx_set_timestamp_args) = 3;
		rgmx_latch_intention_result RGMX_LATCH_INTENTION(
		    rgmx_latch_intention_args) = 4;
		void RGMX_PROCESS_INTENTION(rgmx_process_intention_args) = 5;
		void RGMX_UNLATCH_INTENTION(rgmx_process_intention_args) = 6;
		node_state RGMX_XMIT_STATE(void) = 7;
		void RGMX_SET_PRES(rgmx_set_pres_args) = 8;
		bool RGMX_AM_I_JOINING(void) = 9;
		void RGMX_READ_CCR(rgmx_read_ccr_args) = 10;
		void RGMX_RUN_BOOT_METHS(rgmx_run_boot_meths_args) = 11;
		rgmx_chg_mastery_result RGMX_CHG_MASTERY(
		    rgmx_chg_mastery_args) = 12;
		rgmx_r_restart_result RGMX_R_RESTART(rgmx_r_restart_args) = 13;
		rgmx_regis_result_t RGMX_CLEAR_FLAG(rgmx_clear_flag_args) = 14;
		rgmx_ack_start_result RGMX_ACK_START(
		    rgmx_ack_start_args) = 15;
		rgmx_notify_dependencies_resolved_result RGMX_NOTIFY_DEPENDENCIES_RESOLVED(
		    rgmx_notify_dependencies_resolved_args) = 16;
		rgmx_fetch_restarting_flag_result RGMX_FETCH_RESTARTING_FLAG(
		    rgmx_fetch_restarting_flag_args) = 17;
		rgmx_retrieve_lni_mappings_result
		    RGMX_RETRIEVE_LNI_MAPPINGS(void) = 18;
		void RGMX_SEND_LNI_MAPPINGS(
		    rgmx_send_lni_mappings_args) = 19;
		rgmx_reclaim_lnis_result RGMX_RECLAIM_LNIS(rgmx_reclaim_lnis_args) = 20;

	} = 1;
} = 100145;

#ifdef RPC_HDR
%/* Name of RPC service, as identified in rpc(4) */
%#define        RGMX_COMM_FARM_PROGRAM_NAME   "rgmx_comm_farm"
%
#endif
