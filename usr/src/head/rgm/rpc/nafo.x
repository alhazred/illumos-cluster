%/*
% * CDDL HEADER START
% *
% * The contents of this file are subject to the terms of the
% * Common Development and Distribution License (the License).
% * You may not use this file except in compliance with the License.
% *
% * You can obtain a copy of the license at usr/src/CDDL.txt
% * or http://www.opensolaris.org/os/licensing.
% * See the License for the specific language governing permissions
% * and limitations under the License.
% *
% * When distributing Covered Code, include this CDDL HEADER in each
% * file and include the License file at usr/src/CDDL.txt.
% * If applicable, add the following below this CDDL HEADER, with the
% * fields enclosed by brackets [] replaced with your own identifying
% * information: Portions Copyright [yyyy] [name of copyright owner]
% *
% * CDDL HEADER END
% */

%
%/*
% * Copyright (c) 1997-1999 by Sun Microsystems, Inc.
% * All rights reserved.
% */
%
%/* 
% *
% *	nafo.x: RPC to get network failover status
% *
% */ 	
%

struct input {
	int		action;		/* see nafo_head.h */
	string		adp<20>;	/* adp name */
	string		option<>;	/* also used in callbacks */
};

struct output {
	int		error;
	int		status;
	int		flags;
	long		fo_time;
	string		act_adp<>;
	string		backups<>;
};

struct addr {
	short		sa_family;
	char		sa_data[14];
};

struct IPlist {
	int		operation;
	string		nafo<30>;
	addr		IPaddr<>;
};

/*
 * Info strcutre for a physical adapter, whether it's in a nafo group or
 * not. A list of this is returned by the NAFO_ADAPTERS service.
 */
struct nafo_adapter {
	string		adp_name<>;	/* name of adapter */
	int		adp_type;	/* 0 = public, 1 = private */
	short		adp_flags;	/* flags from SIOCGIFFLAGS */
	char		adp_ipaddr[4];	/* IP address configured */
	char		adp_ipmask[4];	/* netmask from SIOCGIFMASK */
	string		adp_nafo<>;	/* NULL or nafo group it belongs */
};

/*
 * List of nafo_adapter structs, returned by NAFO_ADAPTERS
 */
struct nafo_adapter_list {
	nafo_adapter	adps<>;
};


program NAFOD {
	version NAFO_VERS {
		int			NAFO_CONFIG(input) = 1;
		output			NAFO_STATUS(input) = 2;
		nafo_adapter_list	NAFO_ADAPTERS() = 3;
		int			NAFO_IFCONFIG(IPlist) = 4;
	} = 1;
} = 0x18730;

