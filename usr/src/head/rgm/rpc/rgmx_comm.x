%/*
% * CDDL HEADER START
% *
% * The contents of this file are subject to the terms of the
% * Common Development and Distribution License (the License).
% * You may not use this file except in compliance with the License.
% *
% * You can obtain a copy of the license at usr/src/CDDL.txt
% * or http://www.opensolaris.org/os/licensing.
% * See the License for the specific language governing permissions
% * and limitations under the License.
% *
% * When distributing Covered Code, include this CDDL HEADER in each
% * file and include the License file at usr/src/CDDL.txt.
% * If applicable, add the following below this CDDL HEADER, with the
% * fields enclosed by brackets [] replaced with your own identifying
% * information: Portions Copyright [yyyy] [name of copyright owner]
% *
% * CDDL HEADER END
% */

/*
% * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
% * Use is subject to license terms.
% */

#ifdef RPC_HDR
%#ifndef _RGMX_COMM_H
%#define _RGMX_COMM_H
%#pragma ident	"@(#)rgmx_comm.x	1.5	08/05/20 SMI"
%
#endif

#ifdef RPC_HDR
%
%#include <rgm/rgm_recep.h>
%
#endif

%/*
% * File: rgmx_comm.x
% *
% * This file contains a subset of definitions coming from the RGM IDL
% * module and two RPC programs (one on RGM server side, the other on RGM farm
% * side that implement RPC interface to farm/server nodes communication.
% *
% * ATTENTION - THE DEFINITIONS IN THIS FILE MUST BE IN SYNC WITH
% * ./common/cl/interfaces/idl/rgm.idl.
% *
% */

/*
 * Mapping for IDL return values
 * We cannot just include rgm_comm.h to get idlretval_t defined
 * as rgm_comm.h requires lots of other includes.
 */

enum rpcidlretvals {
	RGMRPC_OK,
	RGMRPC_NOREF,
	RGMRPC_RGMD_DEATH,
	RGMRPC_NODE_DEATH,
	RGMRPC_ZONE_DEATH,
	RGMRPC_UNKNOWN
};

/*
 * rgm_r_state
 * Resource state maintained by RGM.
 * Each node keeps track of its local state; the President keeps
 * track of all nodes' state.  This state is part of the extended
 * resource state structure, r_xstate_t.
 *
 * The states in the first set below drive the process_resource()
 * component of the state machine.
 *
 * The states in the second set below indicate work in progress
 * by methods launched by the state machine.
 */
enum rgm_r_state {
	R_MON_FAILED,
	R_OFFLINE,
	R_ONLINE,
	R_ONLINE_UNMON,
	R_STOPPED,
	R_PRENET_STARTED,
	R_START_FAILED,
	R_STOP_FAILED,
	R_PENDING_INIT,
	R_PENDING_FINI,
	R_ON_PENDING_UPDATE,
	R_ONUNMON_PENDING_UPDATE,
	R_MONFLD_PENDING_UPDATE,
	R_UNINITED,
	R_PENDING_BOOT,
	R_INITING,
	R_FINITING,
	R_BOOTING,
	R_MON_STARTING,
	R_MON_STOPPING,
	R_POSTNET_STOPPING,
	R_PRENET_STARTING,
	R_STARTING,
	R_STOPPING,
	R_UPDATING,
	R_JUST_STARTED,
	R_STOPPING_DEPEND,
	R_STARTING_DEPEND,
	R_OK_TO_START,
	R_OK_TO_STOP
};

/*
 * notify_change_tag
 *
 * Used in the call to notify_me_state_change to discriminate
 * between R state change, RG state change, or "wake president".
 * For the "wake president" case, the president uploads all R/RG
 * state from the slave.
 */
enum notify_change_tag {
	NOTIFY_WAKE,
	NOTIFY_RES,
	NOTIFY_RG
};

/*
 * rgm_rg_state
 *
 * RG state maintained by RGM.  This state drives the state machine.
 * Part of extended RG state, rg_xstate_t.
 * Each node keeps track of its local state; the President keeps
 * track of all nodes' state.
 */
enum rgm_rg_state {
	RG_ONLINE,
	RG_OFFLINE,
	RG_PENDING_ONLINE,
	RG_PENDING_OFFLINE,
	RG_ON_PENDING_METHODS,
	RG_OFF_PENDING_METHODS,
	RG_OFF_PENDING_BOOT,
	RG_OFF_BOOTED,
	RG_ERROR_STOP_FAILED,
	RG_PENDING_OFF_STOP_FAILED,
	RG_PENDING_OFF_START_FAILED,
	RG_OFFLINE_START_FAILED,
	RG_PENDING_ON_STARTED,
	RG_ON_PENDING_DISABLED,
	RG_ON_PENDING_MON_DISABLED,
	RG_ON_PENDING_R_RESTART,
	RG_PENDING_ONLINE_BLOCKED
};

/*
 * rgm_r_fm_status
 *
 * Resource status maintained by RGM and by resource monitor/methods.
 * Monitor or method calls scha_resource_setstatus when it wants to
 * notify the administrator of a change in resource status.
 * Each node keeps track of its local status; the President keeps
 * track of all nodes' status.
 */
enum rgm_r_fm_status {
	R_FM_ONLINE,
	R_FM_OFFLINE,
	R_FM_FAULTED,
	R_FM_DEGRADED,
	R_FM_UNKNOWN
};

/*
 * Types of operations on entities
 */
enum rgm_operation {
	RGM_OP_CREATE,
	RGM_OP_DELETE,
	RGM_OP_SYNCUP,
	RGM_OP_MANAGE,
	RGM_OP_UNMANAGE,
	RGM_OP_UPDATE,
	RGM_OP_ENABLE_R,
	RGM_OP_DISABLE_R,
	RGM_OP_FORCE_ENABLE_R,
	RGM_OP_FORCE_DISABLE_R,
	RGM_OP_ENABLE_M_R,
	RGM_OP_DISABLE_M_R
};

/*
 * Type of flags that can be cleared using scswitch -c -f
 */
enum scswitch_clear_flags {
	UPDATE_FAILED,
	INIT_FAILED,
	FINI_FAILED,
	STOP_FAILED,
	BOOT_FAILED
};

/*
 * status msg.
 */
struct r_state {
	string		idlstr_rname<>;
	rgm_r_state	rstate;
	rgm_r_fm_status	fmstatus;
	string		idlstr_status_msg<>;
};

/*
 * Sequence of elements containing resource name, state,
 * fault monitor status, and status message.
 */
typedef r_state	r_state_seq_t<>;

/*
 * Logical Node identifier and incarnation number
 */
typedef unsigned long lni_t;
typedef unsigned long ln_incarnation_t;

/*
 * Element of a sequence: resource group name and state,
 * and sequence of resource names and states.
 */
struct rg_state {
	string		idlstr_rgname<>;
	rgm_rg_state	rgstate;
	r_state_seq_t	rlist;
};

/*
 * Sequence of elements containing resource group name and state,
 * and sequence of resource names and states.
 */
typedef rg_state	rg_state_seq_t<>;

/*
 * Element of a sequence: lni,
 * and sequence of resource names and states.
 */
struct zone_state {
	lni_t	lni;
	rg_state_seq_t	zseq;
};

/*
 * Sequence of elements containing zones states
 */
typedef zone_state	zone_state_seq_t<>;

/*
 * used by rgm_xmit_state for transmitting the node's entire state
 * back to the new President.
 */
struct node_state {
	zone_state_seq_t	seq;
};

/*
 * Common result argument for all scrgadm & scswitch operations
 */
struct rgmx_regis_result_t {
	int32_t		ret_code;
	string		idlstr_err_msg<>;
	rpcidlretvals	retval;
};

/*
 * Stuff with having to do with synchronization of CCR updates
 * from the President node with slave nodes.
 */

/*
 * Type of entities handled by the RGM
 */
enum rgm_entity {
	RGM_ENT_RG,
	RGM_ENT_RS,
	RGM_ENT_RT
};

/*
 * The "intention" structure
 */

typedef unsigned long   intention_flag_t;

struct rgm_intention {
	rgm_entity	 entity_type;
	rgm_operation	 operation_type;
	string		 idlstr_entity_name<>;
	intention_flag_t flags;
};

/*
 * Flag argument for rgm_r_restart (slave side)
 */
enum rgm_restart_flag {
	RGM_RRF_R,
	RGM_RRF_RG,
	RGM_RRF_RIR,
	RGM_RRF_TR
};

/*
 * Following enum represents values for the r_pending_restart flag that
 * is kept in rlist_t struct locally on each slave.  This flag
 * modifies the actions of the state machine for a resource that is
 * being restarted.
 */
enum r_restart_t {
	RR_NONE,
	RR_STOPPING,
	RR_STARTING
};

/*
 * Zone support
 */
enum rgm_ln_state {
	LN_UNKNOWN,
	LN_UNCONFIGURED,
	LN_DOWN,
	LN_STUCK,
	LN_SHUTTING_DOWN,
	LN_UP
};

struct lni_mappings {
	string		idlstr_zonename<>;
	lni_t		lni;
	rgm_ln_state	state;
	ln_incarnation_t incarn;
};

typedef lni_mappings lni_mappings_seq_t<>;
typedef lni_t lni_seq_t<>;

/* ---------------------------------------------------------------------------
 * Interface IDL rgm_comm mapped to RPC ... 
 * Only needed IDL methods on both farm/server sides have been considered.
 * ---------------------------------------------------------------------------
 */

/*
 * rgmx_scha_rs_get_state(rgmx_rs_get_state_status_args *, rgmx_get_result_t *)
 *
 * Get resource state
 */
struct rgmx_rs_get_state_status_args {
	string	idlstr_rs_name<>;
	string	idlstr_rg_name<>;
	string	idlstr_node_name<>;
};

struct rgmx_get_result_t {
	int32_t		ret_code;
	strarr_list	*value_list;
	string		idlstr_seq_id<>;
	string		idlstr_err_msg<>;
};

/*
 * rgmx_scha_rs_get_status(rgmx_rs_get_state_status_args *, rgmx_get_status_result_t *)
 *
 * Get Dynamic Fault mon status
 */
struct rgmx_get_status_result_t {
	int32_t	ret_code;
	string	idlstr_status<>;
	string	idlstr_status_msg<>;
	string	idlstr_seq_id<>;
	string	idlstr_err_msg<>;
};

/*
 * rgmx_scha_rs_get_fail_status(rgmx_rs_get_fail_status_args *, rgmx_get_result_t *)
 *
 * Get Dynamic Fault mon status
 */
struct rgmx_rs_get_fail_status_args {
	string	idlstr_rs_name<>;
	string	idlstr_rg_name<>;
	string	idlstr_node_name<>;
	string	idlstr_method_name<>;
};

/*
 * rgmx_scha_rs_get_switch(rgmx_rs_get_switch_args *, rgmx_rs_get_switch_result_t *)
 *
 * Get On_off/Monitored switch (on the president)
 */
struct rgmx_rs_get_switch_args {
	string	idlstr_rs_name<>;
	string	idlstr_rg_name<>;
	string	idlstr_node_name<>;
	bool on_off;
};

struct rgmx_rs_get_switch_result_t {
	int32_t	ret_code;
	string	idlstr_switch<>;
	string	idlstr_seq_id<>;
	string	idlstr_err_msg<>;
};
/*
 * rgmx_scha_rs_get_ext(rgmx_rs_get_args *, rgmx_rs_get_ext_result_t *)
 *
 * Get extension property (on the president)
 */
struct rgmx_rs_get_args {
	string	idlstr_rs_name<>;
	string	idlstr_rg_name<>;
	string	idlstr_rs_node_name<>;
	string	idlstr_rs_prop_name<>;
};

struct rgmx_rs_get_ext_result_t {
	int32_t	ret_code;
	int32_t	prop_type;
	string	prop_val<>;
	string	idlstr_seq_id<>;
};

/*
 * rgmx_scha_rs_set_status(rgmx_rs_set_status_args *, rgmx_set_status_result_t *)
 *
 * Set dynamic Fault monitor status (on the president)
 */
struct rgmx_rs_set_status_args {
	string	idlstr_rs_name<>;
	string	idlstr_rg_name<>;
	string	idlstr_node_name<>;
	string	idlstr_status<>;
	string	idlstr_status_msg<>;
};

struct rgmx_set_status_result_t {
	int32_t	ret_code;
	string	idlstr_err_msg<>;
};

/*
 * rgmx_scha_rg_get_state(rgmx_rg_get_state_args *, rgmx_get_result_t *)
 *
 * Get resource group state
 */
struct rgmx_rg_get_state_args {
	string	idlstr_rg_name<>;
	string	idlstr_node_name<>;
};

/*
 * rgmx_scha_get_node_state(rgmx_cluster_get_args *, rgmx_get_result_t *)
 *
 * Get cluster node state
 */
struct rgmx_cluster_get_args {
	string	idlstr_node_name<>;
};

/*
 * rgmx_scrgadm_rs_validate(rgmx_validate_args *, rgmx_regis_result_t *)
 *
 * Methods to support the admin commands. Mostly scrgadm
 */
struct rgmx_validate_args {
	unsigned long		president;
	unsigned long		incarnation;
	lni_t			lni;
	ln_incarnation_t	ln_incarnation;
	string			idlstr_rs_name<>;
	string			idlstr_rt_name<>;
	string			idlstr_rg_name<>;
	string			locale<>;
	rgm_operation		opcode;
	bool			is_scalable;
	strarr_list		*rs_props;
	strarr_list		*ext_props;
	strarr_list		*rg_props;
};

/*
 * 32-bit word of option flags for scha_control
 */
typedef unsigned long	schactl_flag_t;

/*
 * rgmx_scha_control_giveover(rgmx_scha_control_args *,
 *                            rgmx_scha_control_result *)
 * scha_control -O GIVEOVER
 */
struct rgmx_scha_control_args {
	lni_t		lni;
	string		idlstr_R_name<>;
	string		idlstr_rg_name<>;
	uint32_t	flags;
};

struct rgmx_scha_control_result {
	int32_t		ret_code;
	string		idlstr_err_msg<>;
	string		idlstr_syslog_msg<>;
	rpcidlretvals	retval;
};

/*
 * rgmx_scha_control_restart(rgmx_scha_control_args *,
 *                           rgmx_regis_result_t *)
 * scha_control -O RESTART
 */

/*
 * rgmx_scha_control_checkall(rgmx_scha_control_checkall_args *, 
 *                            rgmx_scha_control_result *)
 * scha_control monitor/sanity check
 */
struct rgmx_scha_control_checkall_args {
	unsigned long		president;
	unsigned long		incarnation;
	ln_incarnation_t	ln_incarnation;
	lni_t			lni;
	string			idlstr_R_name<>;
	uint32_t		flags;
};

/*
 * rgmx_set_timestamp(rgmx_set_timestamp_args *,
 *		      rgmx_regis_result *)
 *
 * scha_control get timestamp
 */
struct rgmx_set_timestamp_args {
	string	R_name<>;
	lni_t	lni;
};

/*
 * rgmx_latch_intention(rgmx_latch_intention_args *,
 *			rgmx_latch_intention_result *)
 * rgmx_process_intention(rgmx_process_intention_args *, void *)
 * rgmx_unlatch_intention(rgmx_process_intention_args *, void *)
 *
 * Latch/Process/Unlatch an intention
 */
struct rgmx_latch_intention_args {
	unsigned long		president;
	unsigned long		incarnation;
	lni_t			lni;
	ln_incarnation_t	ln_incarnation;
	rgm_intention		intent;
};

struct rgmx_latch_intention_result {
	rpcidlretvals	retval;
};

struct rgmx_process_intention_args {
	unsigned long	president;
	unsigned long	incarnation;
};

/*
 * rgmx_xmit_state(void *, node_state *)
 *
 * Slave transmits its full state to President
 * upon request from President.
 */

/*
 * rgmx_set_pres(rgmx_set_pres_args *, void *)
 *
 * Slave sets its idea of President
 * upon request from President.
 */
struct rgmx_set_pres_args {
	unsigned long	node;
	unsigned long	incarnation;
	string		ipaddr<>;
};

/*
 * rgmx_am_i_joining(void *, bool_t *)
 *
 * Slave side function that returns
 * true if the slave is joining the cluster
 * This IDL call can be made among all nodes;
 * not just between President and slave.
 */

/*
 * rgmx_read_ccr(rgmx_read_ccr_args *, void *)
 *
 * Slave side function that reads the CCR
 * when node is joining cluster
 */
struct rgmx_read_ccr_args {
	unsigned long		president;
	unsigned long		incarnation;
	lni_mappings_seq_t	lni_map;
};

/*
 * rgmx_run_boot_meths(rgmx_run_boot_meths_args *, void *)
 *
 * Slave side function that kicks off boot methods
 * when node is joining cluster
 */
struct rgmx_run_boot_meths_args {
	unsigned long	president;
	unsigned long	incarnation;
};

/*
 * rgmx_chg_mastery(rgmx_chg_mastery_args *,
 *     rgmx_chg_mastery_result *)
 *
 * Slave takes RGs online and offline
 * upon request from President.
 */
struct rgmx_chg_mastery_args {
	unsigned long		president;
	unsigned long		incarnation;
	lni_t 			lni;
	ln_incarnation_t	ln_incarnation;
	strarr_list		*onlist;
	strarr_list		*offlist;
};

struct rgmx_chg_mastery_result {
	rpcidlretvals		retval;
};

/*
 * rgmx_r_restart(rgmx_r_restart_args *, rgmx_r_restart_result *)
 *
 * Slave-side of rgm_resource_restart()
 */
struct rgmx_r_restart_args {
	unsigned long           president;
        unsigned long           incarnation;
	lni_t			lni;
	ln_incarnation_t	ln_incarnation;
        rgm_restart_flag        rrflag;
        string                  R_name<>;
};

struct rgmx_r_restart_result {
	rpcidlretvals		retval;
};

/*
 * rgmx_clear_flag(rgmx_clear_flag_args *, rgmx_regis_result_t *)
 *
 * Slave side function to clear the error flags on the
 * resource. Called from idl_scswitch_clear_flags function
 * on the President.
 */
struct rgmx_clear_flag_args {
	unsigned long		president;
	unsigned long		incarnation;
	lni_t			lni;
	ln_incarnation_t	ln_incarnation;
	strarr_list		*r_list;
	scswitch_clear_flags	flag;
};

/*
 * rgmx_notify_me_state_change(rgmx_notify_me_state_change_args *)
 * 
 * Slave side function to notify president of an "interesting"
 * R or RG state change.  Also subsumes the former 'rgm_wake_me'
 * function (called by wake_president), causing the president
 * to upload all R/RG state from the given slave if change_tag
 * is NOTIFY_WAKE.
 */
struct rgmx_notify_me_state_change_args {
	lni_t			lni;
	notify_change_tag	change_tag;
	string			R_RG_name<>;
	rgm_r_state		rstate;
	rgm_rg_state		rgstate;
};

/*
 * rgmx_ack_start(rgmx_ack_start_args *, rgmx_ack_start_result *)
 *
 * president tells slave to move the resource from
 * R_JUST_STARTED to R_ONLINE_UNMON
 */
struct rgmx_ack_start_args {
	unsigned long		president;
	unsigned long		incarnation;
	lni_t			lni;
	ln_incarnation_t	ln_incarnation;
	string			R_name<>;
};

struct rgmx_ack_start_result {
	rpcidlretvals	retval;
};

/*
 * rgmx_notify_dependencies_resolved(rgmx_notify_dependencies_resolved_args *,
 *                                   rgmx_notify_dependencies_resolved_result *)
 *
 * president tells slave that the resource can be moved
 * from R_STARTING_DEPEND to R_OK_TO_START or from
 * R_STOPPING_DEPEND to R_OK_TO_STOP (depending on
 * the state of the resource on the slave).
 * Note that it is never ambiguous as to whether we are
 * resolving start or stop dependencies, because the
 * resource will be in R_STARTING_DEPEND or R_STOPPING_DEPEND,
 * but not both.
 */
struct rgmx_notify_dependencies_resolved_args {
	unsigned long		president;
	unsigned long		incarnation;
	lni_t			lni;
	ln_incarnation_t	ln_incarnation;
	string			R_name<>;
};

struct rgmx_notify_dependencies_resolved_result {
	rpcidlretvals		retval;
};

/*
 * rgmx_fetch_restarting_flag(rgmx_fetch_restarting_flag_args *,
 *			      rgmx_fetch_restarting_flag_result *)
 *
 * president fetches from slave the value of
 * the restart flag for the specified resource.
 */
struct rgmx_fetch_restarting_flag_args {
	unsigned long		president;
	unsigned long		incarnation;
	lni_t			lni;
	ln_incarnation_t	ln_incarnation;
	string			R_name<>;
};

struct rgmx_fetch_restarting_flag_result {
	r_restart_t	restart_flag;
	rpcidlretvals	retval;
};

/*
 * For zone support
 *
 * rgmx_retrieve_lni_mappings(void *,
 *			      rgmx_retrieve_lni_mappings_result *)
 *
 * Used by president when reconfiguration
 * step occurs, to recreate all the
 * LNI mappings.
 */
struct rgmx_retrieve_lni_mappings_result {
	lni_mappings_seq_t lni_map;
};

/*
 * rgmx_zone_state_change(rgmx_zone_state_change_args *,
 *			  rgmx_zone_state_change_result *)
 *
 * Used by the slave to notify the president
 * about zone state changes
 */
struct rgmx_zone_state_change_args {
	unsigned long		node;
	string			zonename<>;
	rgm_ln_state		state;
	ln_incarnation_t	incarn;
};

struct rgmx_zone_state_change_result {
	lni_t assigned_lni;
};

/*
 * rgmx_send_lni_mappings(rgmx_send_lni_mappings_args *, void *)
 *
 * Used by president to send LNI mappings
 * to the slave when a new LN is discovered
 * in the nodelist
 */
struct rgmx_send_lni_mappings_args {
	unsigned long		president;
	unsigned long		incarnation;
	lni_mappings_seq_t	lni_map;
};

/*
 * rgmx_reclaim_lnis(rgmx_reclaim_lnis_args *, void *)
 *
 * Used by president to send list of LNIs on slave to be
 * deleted.
 */
struct rgmx_reclaim_lnis_args {
	unsigned long	node;
	unsigned long	incarnation;
	lni_seq_t lni_seq;
};

struct rgmx_reclaim_lnis_result {
	lni_seq_t lni_seq;
};

#ifdef RPC_HDR
%#endif  /* !_RGMX_COMM_H */
#endif
