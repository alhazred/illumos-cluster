%/*
% * CDDL HEADER START
% *
% * The contents of this file are subject to the terms of the
% * Common Development and Distribution License (the License).
% * You may not use this file except in compliance with the License.
% *
% * You can obtain a copy of the license at usr/src/CDDL.txt
% * or http://www.opensolaris.org/os/licensing.
% * See the License for the specific language governing permissions
% * and limitations under the License.
% *
% * When distributing Covered Code, include this CDDL HEADER in each
% * file and include the License file at usr/src/CDDL.txt.
% * If applicable, add the following below this CDDL HEADER, with the
% * fields enclosed by brackets [] replaced with your own identifying
% * information: Portions Copyright [yyyy] [name of copyright owner]
% *
% * CDDL HEADER END
% */

%/*
% * Copyright 2004 Sun Microsystems, Inc.  All rights reserved.
% * Use is subject to license terms.
% */

#ifdef RPC_HDR
%
%#pragma ident  "@(#)pmf.x	1.32	08/05/20 SMI"
%
#endif

%/*
% * File: pmf.x  RPC interface to HA Process Monitor Facility
% */

%#include <rgm/security.h>

#ifdef linux
%#include <rgm/rpc_services.h>
#endif

#ifdef linux
#ifdef RPC_XDR
%
%/*
% * xdr_uid_t not defined on Linux and uid_t is an u_int
% */

%static bool_t
%xdr_uid_t (XDR *xdrs, u_int *objp) {
%	return xdr_u_int (xdrs, objp);
%}
#endif
#endif

typedef string strings<>;

enum PMF_ERRCODE {
	PMF_OKAY,	/* everything worked OK */
	PMF_DUP,	/* a process with this tag is running already */
	PMF_SYSERRNO,	/* there was a system error;
			 look at errno for more details */
	PMF_ACCESS,	/* security error - security check failed */
	PMF_CONNECT	/* error in establishing connection with server */
};

enum PMF_ACTIONS {
	PMFACTION_NULL = 0,
	PMFACTION_EXEC
};

#ifdef RPC_HDR
%
%/*
% * Flag to tell rpc.pmfd to not complain about
% * process exiting (ie: it's expected to).
% */
%#define PMF_EXIT_QUIETLY	0x1
%#define PMF_EXIT_QUIETLY_ISSET(a) ((a) & PMF_EXIT_QUIETLY)
%#define PMF_EXIT_QUIETLY_ISCLR(a) (!PMF_EXIT_QUIETLY_ISSET(a))
%#define PMF_SET_EXIT_QUIETLY(a) ((a) |= PMF_EXIT_QUIETLY)
%#define PMF_CLR_EXIT_QUIETLY(a) ((a) &= ~PMF_EXIT_QUIETLY)
%
#endif

struct pmf_error {
	enum PMF_ERRCODE	type;
	/*
	 * sys_errno Meaningful only for type PMF_SYSERRNO
	 */
	int			sys_errno;
	/*
	 * sec_errno Meaningful only for type PMF_ACCESS
	 */
	SEC_ERRCODE		sec_errno;
};

enum pmf_cmd_type {
	PMF_NULL = 0,
	PMF_START,
	PMF_STOP,
	PMF_MODIFY,
	PMF_KILL,
	PMF_STATUS,
	PMF_STATUS_ALL,
	PMF_STAT,
	PMF_SUSPEND,
	PMF_RESUME
};

/*
 * The following entry is for tracking what levels of children to monitor:
 *    - default: all levels
 *    - option -C: parent and first level of children
 *    - option -O: only parent
 */
enum PMF_MONITOR_CHILDREN {
        PMF_ALL,
        PMF_LEVEL
};

#ifdef RPC_HDR
%struct pmf_cmd {
%	char *host;
%	int period_modified;
%	int period;
%	int retries_modified;
%	int retries;
%	char *action;
%	int num_env;
%	char **env;
%	int timewait;
%	int signal;
%	SEC_TYPE	security;
%	int flags;
%	PMF_MONITOR_CHILDREN monitor_children;
%	int	monitor_level;
%	bool_t	env_passed;
%	char *project_name;
%};
%typedef struct pmf_cmd pmf_cmd;
#endif

struct pmf_result {
	struct pmf_error code;
};

struct pmf_status_result {
	struct pmf_error code;
	strings		cmd<>;
	strings		env<>;
	int		retries;
	int		period;
	string		identifier<>;
	PMF_ACTIONS	action_type;
	string		action<>;
	int		nretries;
	uid_t		owner;
	string		pids<>;
	string		upids<>;
	PMF_MONITOR_CHILDREN monitor_children;
	int		monitor_level;
	string		project_name<>;
};

struct pmf_start_args {
	strings		cmd<>;
	strings		env<>;
	string		pwd<>;
	string		path<>;
	int		retries;
	int		period;
	string		identifier<>;
	PMF_ACTIONS	action_type;
	string		action<>;
	pmf_result	result;
	int		flags;
	PMF_MONITOR_CHILDREN monitor_children;
	int		monitor_level;
	bool		env_passed;
	string		project_name<>;
};

struct pmf_modify_args {
	string		identifier<>;
	int		period_modified;
	int		period;
	int		retries_modified;
	int		retries;
};

struct pmf_args {
	string		identifier<>;
	int		signal;
	int		timewait;
	bool		query_all;
};

#ifdef RPC_HDR
%
%extern int pmf_start(char *id, pmf_cmd *cmd, int argc, char **argv, 
%pmf_result *res);
%extern int pmf_status(char *id, pmf_cmd *cmd, pmf_status_result *status);
%extern int pmf_status_all(pmf_cmd *cmd, pmf_status_result *status);
%extern int pmf_stop(char *id, pmf_cmd *cmd, pmf_result *res);
%extern int pmf_kill(char *id, pmf_cmd *cmd, pmf_result *res);
%extern int pmf_modify(char *id, pmf_cmd *cmd, pmf_result *res);
%extern int pmf_suspend(char *id, pmf_cmd *cmd, pmf_result *res);
%extern int pmf_resume(char *id, pmf_cmd *cmd, pmf_result *res);
%extern void free_rpc_args(pmf_status_result *result);
%
#endif

program PMF_PROGRAM {
	version PMF_VERSION {

		/*
		 * Can return NULL if server is out of memory.
		 */
		void
		PMFPROC_NULL(void) = 0;

		pmf_result
		PMFPROC_START(pmf_start_args) = 1;

		pmf_status_result
		PMFPROC_STATUS(pmf_args) = 2;

		pmf_result
		PMFPROC_STOP(pmf_args) = 3;

		pmf_result
		PMFPROC_KILL(pmf_args) = 4;

		pmf_result
		PMFPROC_MODIFY(pmf_modify_args) = 5;

		pmf_result
		PMFPROC_SUSPEND(pmf_args) = 6;

		pmf_result
		PMFPROC_RESUME(pmf_args) = 7;

	} = 2;
} = 100248;

#ifdef RPC_HDR
%/* Name of RPC service, as identified in rpc(4) */
%#define        PMF_PROGRAM_NAME   "pmfd"
%
#endif
