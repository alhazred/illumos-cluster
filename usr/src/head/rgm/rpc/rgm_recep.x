%/*
% * CDDL HEADER START
% *
% * The contents of this file are subject to the terms of the
% * Common Development and Distribution License (the License).
% * You may not use this file except in compliance with the License.
% *
% * You can obtain a copy of the license at usr/src/CDDL.txt
% * or http://www.opensolaris.org/os/licensing.
% * See the License for the specific language governing permissions
% * and limitations under the License.
% *
% * When distributing Covered Code, include this CDDL HEADER in each
% * file and include the License file at usr/src/CDDL.txt.
% * If applicable, add the following below this CDDL HEADER, with the
% * fields enclosed by brackets [] replaced with your own identifying
% * information: Portions Copyright [yyyy] [name of copyright owner]
% *
% * CDDL HEADER END
% */

%/*
% * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
% * Use is subject to license terms.
% */


#ifdef RPC_HDR
%
%#pragma ident	"@(#)rgm_recep.x	1.33	08/05/20 SMI"
%
#endif

%/*
% * File: rgm_recep.x  RPC interface to SC 3.0 receptionist
% */

#ifdef linux
%#include <rgm/rpc_services.h>
#endif

/*
 * typedef for sequence number
 */
typedef string seqid<>;

/*
 * typedef for a list of node names or property values for R, RG or RT
 */
typedef string strarr<>;
typedef strarr strarr_list<>;

/*
 * ********************************************************************
 * API operation code for load balancer op
 * ********************************************************************
 */
enum scha_ssm_lb_op_t {
	GET_POLICY_TYPE,
	GET_DISTRIBUTION,
	SET_DISTRIBUTION,
	SET_ONE_DISTRIBUTION,
	SET_PERF_MON_STATUS,
	SET_PERF_MON_PARAMETERS,
	GET_PERF_MON_PARAMETERS
};

/*
 * *******************************************************************
 * scha_control action code
 * *******************************************************************
 */
enum scha_control_action_t {
	GIVEOVER,
	RESTART,
	RESOURCE_RESTART,
	RESOURCE_IS_RESTARTED,
	GETOFF,
	CHECK_GIVEOVER,
	CHECK_RESTART,
	IGNORE_FAILED_START,
	RESOURCE_DISABLE,
	CHANGE_STATE_ONLINE,
	CHANGE_STATE_OFFLINE
};
  
/*
 * *******************************************************
 *  Request argument for scha_resource_open operation
 * *******************************************************
 */
struct rs_open_args {
	string		rs_name<>;	/* Resource name */
	string		rg_name<>;	/* Resource group name */
};

/*
 * *******************************************************
 *  Request argument for scha_resourcegroup_open operation
 * *******************************************************
 */
struct rg_open_args {
	string		rg_name<>;	/* Resource group name */
};

/*
 * *******************************************************
 *  Request argument for scha_resourcetype_open operation
 * *******************************************************
 */
struct rt_open_args {
	string		rt_name<>;	/* Resource type name */
};

/*
 * *******************************************************
 *  Request argument for load balancer operation
 * *******************************************************
 */
struct scha_ssm_lb_args {
	string		rs_name<>;	/* Resource name */
	scha_ssm_lb_op_t		op_code;	/* operation to perform */
	int		op_data<>;	/* operation-specific arguments */
};

/*
 * *******************************************************
 *  Request argument for scalable services operation
 * *******************************************************
 */
struct ssm_ip_args {
	string		rs_name<>;
	string		rg_name<>;
	int		protocol;
	int		nodeid;
};

/*
 * *******************************************************
 *  Request argument for error message string
 * *******************************************************
 */
struct get_err_string_args {
	int 		errcode;
};

/*
 * *********************************************************************
 *  result argument for error message string
 * *********************************************************************
 */
struct get_err_result_t {
	int		ret_code;	/* return code */
	string		err_string<>;	/* error message */
};

/*
 * *********************************************************************
 *  Common result argument for scha_*_open operation
 * *********************************************************************
 */
struct open_result_t {
	int		ret_code;	/* return code */
	seqid		seq_id;		/* CCR handle sequence number */
};

/*
 * *********************************************************************
 *  result argument for RS open operation
 * *********************************************************************
 */
struct rs_open_result_t {
	int		ret_code;	/* return code */
	string		rt_name<>;	/* RT name */
	seqid		rs_seq_id;	/* CCR handle RS sequence number */
	seqid		rt_seq_id;	/* CCR handle RT sequence number */
};

/*
 * *********************************************************************
 *  result argument for load balancer operation
 * *********************************************************************
 */
struct scha_ssm_lb_result_t {
	int		ret_code;	/* return code */
	int		ret_data<>;	/* operation-specific results */
};

/*
 * *********************************************************************
 *  result argument for ssm operation
 * *********************************************************************
 */
struct ssm_result_t {
	int		ret_code;	/* return code */
};

/*
 * ***************************************************************************
 *  Request argument for scha_resource_get operation which does not
 *	have an extra argument
 * ***************************************************************************
 */
struct rs_get_args {
	string		rs_name<>;	/* Resource name */
	string		rg_name<>;	/* Resource group name */
	string		rs_prop_name<>;	/* property name */
	string		zonename<>;	/* zonename the query is for */
	string		rs_node_name<>; /* Node name */
	int		qarg;           /* qarg for dependencies */
};

/*
 * ********************************************************************
 *  Request argument for SCHA_ON_OFF_SWITCH, SCHA_ON_OFF_SWITCH_NODE,
 *      SCHA_MONITORED_SWITCH and SCHA_MONITORED_SWITCH_NODE operations
 *      Put the current node name or the node name
 *      in node_name field.
 * ********************************************************************
 */
struct rs_get_switch_args {
	string		rs_name<>;	/* Resource name */
	string		rg_name<>;	/* Resource group name */
	string		node_name<>;	/* node name */
	string		zonename<>;
	int		on_off;		/* On_off (or) Monitored switch */
};

/*
 * ********************************************************************
 *  Request argument for SCHA_STATUS, SCHA_STATUS_NODE, SCHA_RESOURCE_STATE 
 *	and SCHA_RESOURCE_STATE_NODE operations
 *	Put the current node name or the node name
 *	in node_name field.
 * ********************************************************************
 */
struct rs_get_state_status_args {
	string		rs_name<>;	/* Resource name */
	string		rg_name<>;	/* Resource group name */
	string		node_name<>;	/* node name */
	string		zonename<>;	/* zonename */
};

/*
 * **********************************************************************
 *  Request argument for SCHA_BOOT_FAILED, SCHA_UPDATE_FAILED,
 *	SCHA_INIT_FAILED, and SCHA_FINI_FAILED operations
 *	put method name in the method_name field to identify which failed
 *	method 	
 * **********************************************************************
 */
struct rs_get_fail_status_args {
	string		rs_name<>;	/* Resource name */
	string		rg_name<>;	/* Resource group name */
	string		node_name<>;	/* node name */
	string		method_name<>;	/* method name */
	string		zonename<>;	/* zone name */
};

/*
 * ****************************************************************
 *  Request argument for scha_resourcetype_get operation which
 *	does not have an extra argument
 * ****************************************************************
 */
struct rt_get_args {
	string		rt_name<>;	/* Resource type name */
	string		rt_prop_name<>;	/* property name */
};

/*
 * *************************************************************
 *  Request argument for scha_resourcegroup_get operation
 *	which does not have an extra argument
 * *************************************************************
 */
struct rg_get_args {
	string		rg_name<>;	/* Resource group name */
	string		rg_prop_name<>;	/* property name */
};

/*
 * **************************************************************
 *  Request argument for SCHA_RG_STATE and SCHA_RG_STATE_NODE operations
 *	put the current node name or the specified node name
 *	in node_name field
 * **************************************************************
 */
struct rg_get_state_args {
	string		rg_name<>;	/* Resource group name */
	string		node_name<>;	/* node name */
	string		zonename<>;	/* zone name */
};

/*
 * *****************************************************************
 *  Common result argument for scha_*_get operation
 *	The type of return values is a string or an array of string.	
 * *****************************************************************
 */
struct get_result_t {
	int		ret_code;	/* return code */
	strarr_list	*value_list;	/* property value(s) */
	seqid		seq_id;		/* CCR handle sequence number */
};

/*
 * *****************************************************************
 *  Common result argument for get_rt_name operation
 *	The type of return values is a string or an array of string.	
 * *****************************************************************
 */
struct get_rt_name_result_t {
	int		ret_code;	/* return code */
	strarr_list	*value_list;	/* property value(s) */
	seqid		rt_seq_id;	/* CCR handle RT sequence number */
};

/*
 * ******************************************************************
 *  Result argument for SCHA_EXTENSION operation
 *	The type of return values is a string or an array of string.	
 * ******************************************************************
 */
struct get_ext_result_t {
	int		ret_code;	/* return code */
	int		prop_type;	/* property type */
	strarr_list	*value_list;	/* property value(s) */
	seqid		seq_id;		/* CCR handle sequence number */
};

/*
 * *****************************************************************
 *  Common result argument for SCHA_ON_OFF_SWITCH, SCHA_ON_OFF_SWITCH_NODE,
 *  SCHA_MONITORED_SWITCH and SCHA_MONITORED_SWITCH_NODE.
 *      The type of return value is a string and an int.
 * *****************************************************************
 */
struct get_switch_result_t {
	int		ret_code;	/* return code */
	int		rs_switch;	/* rs switch */
	seqid		seq_id;		/* CCR handle sequence number */
};

/*
 * *****************************************************************
 *  Common result argument for SCHA_STATUS and SCHA_STATUS_NODE 
 *	The type of return values is a string or an array of string.	
 * *****************************************************************
 */
struct get_status_result_t {
	int		ret_code;	/* return code */
	int		rs_status;	/* rs status */
	string		status_msg<>;	/* status message */
	seqid		seq_id;		/* CCR handle sequence number */
};

/*
 * ********************************************************************
 *  Request argument for scha_resource_setstatus operation
 * ********************************************************************
 */
struct rs_set_status_args {
	string		rs_name<>;	/* Resource name */
	string		rg_name<>;	/* Resource group name */
	int		status;		/* RS status */
	string		status_msg<>;	/* status message */
	string		zonename<>;	/* zone name */
};

/*
 *  Result argument for scha_resource_setstatus operation
 */
struct set_status_result_t {
	int		ret_code;	/* return code */
};

/*
 * *****************************************************************
 *  Common result argument for scha_control(); 
 *	The type of return values is a status
 * *****************************************************************
 */
struct scha_control_args {
	scha_control_action_t	action;	/* scha_control action */
	string		rg_name<>;	/* resource group name */
	string		r_name<>;	/* resource name */
	string		local_node<>;	/* local node name */
	string		gen_num<>;	/* generation number */
};

struct scha_result_t {
	int	ret_code;
};
			
/*
 * ********************************************************************
 *  Request argument for scha_cluster_get operation 
 * ********************************************************************
 */
struct cluster_get_args {
	string		cluster_tag<>;
	string		node_name<>;
	unsigned int	node_id;
	string		zonename<>;
};

program SCHA_PROGRAM {
	version SCHA_VERSION {

		/*
		 * Used in scha_resource_open
		 */
		rs_open_result_t SCHA_RS_OPEN(rs_open_args) = 1;

		/*
		 * Used in scha_resource_get
		 */
		get_result_t SCHA_RS_GET(rs_get_args) = 2;
		get_ext_result_t SCHA_RS_GET_EXT(rs_get_args) = 3;
		get_result_t SCHA_RS_GET_STATE
					(rs_get_state_status_args) = 4;
		get_status_result_t SCHA_RS_GET_STATUS
					(rs_get_state_status_args) = 5;
		get_result_t SCHA_RS_GET_FAILED_STATUS
					(rs_get_fail_status_args) = 6;
		get_result_t SCHA_GET_ALL_EXTPROPS(rs_get_args) = 7;
		get_switch_result_t SCHA_RS_GET_SWITCH
					(rs_get_switch_args) = 8;
		/*
		 * Used in scha_resource_setstatus
		 */
		set_status_result_t SCHA_RS_SET_STATUS(rs_set_status_args) = 9;

		/*
		 * Used in scha_resourcetype_open
		 */
		open_result_t SCHA_RT_OPEN(rt_open_args) = 10;

		/*
		 * Used in scha_resourcetype_get
		 */
		get_result_t SCHA_RT_GET(rt_get_args) = 11;

		/*
		 * Used in scha_resourcegroup_open
		 */
		open_result_t SCHA_RG_OPEN(rg_open_args) = 20;

		/*
		 * Used in scha_resourcegroup_get
		 */
		get_result_t SCHA_RG_GET(rg_get_args) = 21;
		get_result_t SCHA_RG_GET_STATE(rg_get_state_args) = 22;
		scha_result_t SCHA_CONTROL(scha_control_args) = 23;

		/*
		 * Used in scha_cluster_get
		 * the following calls will be used to handle
		 * cluster get requests
		 */
		get_result_t SCHA_CLUSTER_GET(cluster_get_args) = 30;

		/*
		 * Load balancer.
		 */
		scha_ssm_lb_result_t SCHA_LB(scha_ssm_lb_args) = 31;

		/*
		 * Scalable services.
		 */
		ssm_result_t SCHA_SSM_IP(ssm_ip_args) = 34;

		/*
		 * Used in scha_cluster_open
		 */
		open_result_t SCHA_CLUSTER_OPEN() = 35;

		/*
		 * Used in scha_rt_api.c
		 */
		get_result_t SCHA_IS_PERNODE(rt_get_args) = 36;

	} = 1;
} = 100141;

#ifdef RPC_HDR
%/* Name of RPC service, as identified in rpc(4) */
%#define        SCHA_PROGRAM_NAME   "rgmd_receptionist"
%
#endif
