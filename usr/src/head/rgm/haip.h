/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_HAIP_H_
#define	_HAIP_H_

#pragma ident	"@(#)haip.h	1.34	09/04/23 SMI"

/*
 * haip.h - Header file for the common utilities for SharedAddress and
 *	LogicalHostname RT
 */

#include <stdio.h>
#include <scha.h>
#include <rgm/pnm.h>
#include <limits.h>
#include <libintl.h>
#include <unistd.h>
#include <errno.h>

#ifdef linux
#include <linux/sockios.h>
#else
#include <sys/sockio.h>
#endif

#include <netdb.h>
#include <net/if.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <rgm/libdsdev.h>


#ifdef __cplusplus
extern "C" {
#endif

#define	MAXIPMPMSGLEN		32

#define	HAIP_IFTABLE		"NetIfList"
#define	HAIP_IPALIST		"HostnameList"
#define	HAIP_AUXLIST		"AuxNodeList"
#define	HAIP_CHECKNAMESERVICE	"CheckNameService"

/* Some strings related to IPMP callback registration modes */
#define	IPMP_REG_INIT		"init"
#define	IPMP_REG_MON		"mon"

/* The IPMP specification is of the form "ipmpgroup@nodename" */
#define	NODE_SEPARATOR    	'@'

#define	CALLBACK_BASEDIR	"/usr/cluster/lib/rgm/rt"
#define	LOCK_DIR		"/var/opt/cluster/rt/haip"
#define	LOCK_FILE		"haip.lock"

#ifdef linux
#define	HOSTSFILE		"/etc/hosts"
#define	IPNODESFILE		"/etc/hosts"
#else
#define	HOSTSFILE		"/etc/inet/hosts"
#define	IPNODESFILE		"/etc/inet/ipnodes"
#endif

#define	NSSWITCHFILE		"/etc/nsswitch.conf"

/* Return value from some of the HAIP library functions */
#define	HAIP_OK			0
#define	HAIP_ERROR		-1
#define	HAIP_RETRY		-2

/*
 * Timeout value for checking name service. This value is
 * arbitrary; Ideally we should use monitor_check timeout
 * instead of this.
 */
#define	HAIP_NS_TIMEOUT		120

/* Debug levels for error messages */
#define	DBG_LEVEL_HIGH	9
#define	DBG_LEVEL_MED	5
#define	DBG_LEVEL_LOW	1

#define	SCDS_ARRAY_SIZE	1024

/* list of ip addresses, each one is ipv6 sized and in network byte order */
typedef struct in6list {
	struct in6_addr *addr;
	int count;
} in6addr_list_t;

/*
 * This flag is used by the VALIDATE callback to indicate
 * that all functions should print any error messages to
 * stderr too.
 *
 * Only functions that are actually called (directly or indirectly)
 * from the VALIDATE method should pay attention to this flag.
 */
extern boolean_t print_errors;

/* Get the IPMP group to be used on this node */
extern char *haip_get_ipmp_group(scds_handle_t handle);

/* A wrapper around pnm_group_status */
extern int haip_ipmpstatus(char *ipmpgroup, char *status_string);

/* Performs an ifconfig style operation on a resource */
extern int haip_do_ipmp_op(char *ipmpgroup, const char *zone, int op,
    in6addr_list_t al);

/* Converts a hostname list to a list of in6addr style addresses */
extern int haip_hostlist_to_addrlist(scds_handle_t handle, in6addr_list_t *al,
    int flags);

/* Looks thru all hostnames managed by the resource to make sure they look OK */
extern boolean_t haip_validate_hostnamelist(scds_handle_t handle, char *group,
    int instances);

/* Verify settings in nsswitch.conf file */
extern boolean_t haip_check_nsswitch_hosts(void);

/* Check if the hosts (or IPs) have a mapping in the hosts/ipnodes files */
extern boolean_t haip_check_hosts_in_files(scha_str_array_t *hlist);

/* Check if the IP address has a mapping in the supplied file */
extern boolean_t haip_check_local_addr_mapping(FILE *f, void *addr,
    sa_family_t family);

/* Check if the hostname has a mapping in the supplied file */
extern boolean_t haip_check_local_name_mapping(FILE *f, char *hostname);

/* Tokenize a line of text from a file */
extern int haip_fgetline(FILE *fp, char *buffer, int bufsize, char *items[],
    int nitems, int newlines_can_be_escaped, int keep_leading_whitespace);

/* Wrapper around scha_resource_get() */
extern int haip_rs_get(const char *zone, scha_resource_t rs_handle,
    char *propname, scha_extprop_value_t **extprop);

/* Check if the ipmp group name is valid */
extern boolean_t haip_is_group_valid(char *ipmpgroup);

/* Registers callback with IPMP with the RG and RS names */
extern int haip_register_callback(char *rsname, char *rgname,
    char *zone, char *ipmpgroup, char *mode, char *callback_prog);
extern int haip_unregister_callback(char *rsname, char *ipmpgroup, char *mode);

/*
 * Utilities to serialize running of same method of the RT on the same
 * adapter for multiple resources
 */
extern int haip_serialize_lock(char *ipmpgroup);
extern int haip_serialize_unlock(int lockfd);

extern void haip_ipmp_delay(void);
extern int haip_safe_wait(struct in6_addr theip, int timeout);

/* Checks whether the name service is available and responsive */
extern int haip_check_nameservice(scds_handle_t);

/* Check if the IPMP group name is being changed */
extern int haip_check_group_changed(const char *zone,
    scds_handle_t handle, char *new_group);

/* Check to see if RG is on-line on local node */
extern boolean_t haip_rg_local(const char *zone, char *rgname);

/* Start failover script under PMF */
extern int haip_start_retry(char *cmd, char *rg, char *r, char *ipmp,
    const char *zone);

/* Stop failover script under PMF */
extern int haip_stop_retry(char *rg, char *r, char *ipmp);

/* Check if ip is already plumbed on the system */
extern int haip_check_dup(scds_handle_t handle);

/* Checks if the local nodename is in the supplied list */
extern boolean_t haip_is_node_in_rgnodelist(const char* cluster,
    scha_str_array_t *rgnodelist);

/* Retreives the RT name corresponding to the resource and group name */
extern char *haip_get_rtname(const char *cluster, char *rname, char *rgname);

/* Utility to convert the string array to string */
extern char *haip_strarray_to_str(scha_str_array_t *sa);

/* Validates the zone cluster adapter list */
extern int haip_validate_zone_adp(scds_handle_t handle, char *ipmp);

/* Checks if the resource is of type SUNW.LogicalHostname */
extern int haip_is_logical_hostname(scha_resource_t rs_handle, char *rsname,
	boolean_t *bool_val);

/* Checks if the resource is of type SUNW.SharedAddress */
extern int haip_is_shared_address(scha_resource_t rs_handle, char *rsname,
	boolean_t *bool_val);

/* Checks the uniqueness of the hostname in the haip resource */
extern boolean_t haip_validate_uniqueness(scds_handle_t scds_handle);

#ifdef __cplusplus
}
#endif

#endif	/* !_HAIP_H_ */
