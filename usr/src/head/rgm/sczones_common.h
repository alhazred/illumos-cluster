/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SCZONES_COMMON_H_
#define	_SCZONES_COMMON_H_

#pragma ident	"@(#)sczones_common.h	1.5	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <sys/priocntl.h>
#include<rpc/xdr.h>
#include <door.h>

/*
 * This file contains only the information needed by both the
 * libsczones library and the sc_zonesd server.
 *
 * This info includes common agreement on numerical values for
 * the states and the path of the server door rendezvous.
 *
 * This file is the *private* file for the implementations. The
 * public file for clients of libsczones to include is
 * usr/src/sys/rgm/sczones.h
 */
typedef enum {ZONE_STARTING, ZONE_UP, ZONE_DOWN, ZONE_SHUTTING_DOWN,
    ZONE_STUCK} sc_zone_state_t;

#define	SCZONES_LIB_SERVER_PATH "/var/cluster/run/sczones_lib_server_door"

#define	ZONEUP_SERVICE	"zoneup"

/*
 * zoneup_args are :
 *   - the pid of the process whose priority is to be modified.
 *   - the scheduling parameters for modifying the priority.
 *   - the zone name, if any, whose UP state needs to be notified.
 */
typedef struct zoneup_args {
	pid_t pid;
	pcparms_t parms;
	char *zonename;
} zoneup_args_t;

/*
 * XDR encoding/decoding function
 * used on both client and server side
 * to encode/decode zoneup_args
 */
boolean_t xdr_zoneup_args(register XDR *xdrs, zoneup_args_t *objp);
extern int do_door_call(int serverfd, door_arg_t *arg);

#define	zoneup_xdr_sizeof(args) \
	(sizeof (pid_t) + \
	    sizeof (pcparms_t) + \
	    sizeof (char *) + \
	    sizeof (int) + \
	    safe_strlen(args.zonename))

#ifdef __cplusplus
}
#endif

#endif /* _SCZONES_COMMON_H_ */
