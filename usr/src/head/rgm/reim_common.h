/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _REIM_COMMON_H_
#define	_REIM_COMMON_H_

#pragma ident	"@(#)reim_common.h	1.6	08/09/18 SMI"

// Include headers

#include <libscf.h>
#include <unistd.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#include <stdio.h>
#include <signal.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <strings.h>
#include <limits.h>
#include <rpc/rpc.h>
#include <rpc/xdr.h>
#include <rpc/types.h>
#include <rpc/svc.h>
#include <netconfig.h>
#include <syslog.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <errno.h>
#include <locale.h>
#include <netdb.h>
#include <sys/utsname.h>
#include <pthread.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#include <assert.h>
#include <door.h>
#include <zone.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/syslog.h>
#include <sys/sc_syslog_msg.h>
#include <sys/dbg_printf.h>
#include "reim_xdr.h"



// Global #defines used in SMF proxy services.

#define	NOGET(s) (s)

#define	SC_DG_DBG_BUFSIZE (64 * 1024) // 64KB ring buffer for tracing

#define	SC_DG_BUFSIZ	256

// Temporary directory created for copying of manifest files.
#define	PROXY_SVC_MANIFEST_DIR  "/var/cluster/lib/svc/method/sc_delegated_tmp"

#define	REIM_DOOR_PATH "/var/run/Reim_Door"
// Error codes used in ReIM server and SMF proxy resource methods.
#define	SC_RESTARTER_ENABLE_FAILED 1
#define	SC_RESTARTER_NOT_ONLINE 2
#define	UNABLE_TO_MAKE_DOOR_CALL 3
#define	XDR_ERROR_AT_SERVER 4
#define	XDR_ERROR_AT_CLIENT 5
#define	REIM_DOOR_CONNECT_FAILED 6
#define	RESOURCE_NOT_OF_SMF_PROXY_RT 7
#define	PROXY_SVC_ALREADY_MANAGED 8
#define	OPEN_MANIFEST_FILE_FAILED 9
#define	OPEN_FMRI_FILE_FAILED 10
#define	SMF_DISABLE_SERVICE_FAILED 11
#define	SMF_REENABLE_SERVICE_FAILED 12
#define	FAILURE_IN_PROCESSING_FMRI 13
#define	PROXY_SVC_NOT_OFFLINE 14
#define	IMPORT_MANIFEST_FILE_FAILED 15
#define	DELETION_SVC_INSTANCE_FAILED 16
#define	RESTARTER_DISABLE_FAILED 17
#define	SERVICE_TIMEOUT_RETRIVAL_FAILED 18
#define	RESOURCE_TIMEOUT_LESS 19
#define	MEMORY_ALLOCATION_FAILED 20
#define	CREATION_OF_MANIFEST_TEMP_DIR_FAILED 21
#define	ERROR_GETTING_SCHA_RS_HANDLE 22
#define	ERROR_GETTING_SCHA_RS_STATE 23
#define	ERROR_GETTING_PROXY_SVC_INSTANCES_PROP 24
#define	RESOURCE_TYPE_NOT_PROXY_SMF_TYPE 25
#define	SC_DELEGATED_RESTARTER_NOT_CONFIGURED 26
#define	ERROR_GETTING_SCDS_HANDLE 27
#define	UPDATE_OF_PROXIED_SVC_INSTANCES_NOT_ALLOWED 28
#define	INVALID_MANIFEST_FILE_NAME_SPECIFIED 29
#define	INVALID_FMRI_SVC_SPECIFIED 30
#define	REIM_DOOR_CREATE_FAILED 31
#define	REIM_DOOR_SVC_REG_FAILED 32
#define	REIM_CREATE_ATTACH_DOOR_FAILED 33
#define	PARSE_FILE_SCRIPT_NOT_FOUND 34
#define	ERROR_GETTING_SCHA_RS_TIMEOUTS 35
#define	ERROR_GETTING_METH_TIMEOUT 36
#define	DOOR_CALL_RETURN_ERROR 37

// Method types.
typedef enum method_type {
	METHOD_START,	// Method start
	METHOD_STOP,	// Method stop
	METHOD_BOOT,	// Method boot
	METHOD_INIT,	// Method init
	METHOD_RESTART,	// Method Restart Not used as of now.
	METHOD_FINI,	// Method Fini
	METHOD_VALIDATE, // Method Validate
	METHOD_VALIDATE_U, // Method Validate Update
	METHOD_MONITOR_CHECK, // Monitor check.
	METHOD_START_IS_RES_REG, // To check if service is already registered.
	METHOD_START_IS_SVC_OFFLINE,	 // To check if service is offline.
	METHOD_VALIDATE_IS_SVC_MANAGED,	// To check if all service in a res
					// is managed.
	METHOD_IS_THIS_SVC_MANAGED	// To check if this service is managed.
} method_type_t;

typedef enum {
	CHECK_ENABLE_SC_RESTARTER,
	CHECK_SC_RESTARTER
} check_restarter_flag_t;

typedef enum {
	CHECK_PROXY_BOOT_METHOD_SUCCESSFUL,
	CHECK_PROXIED_SVC_OFFLINE,
	CHECK_PROXIED_SVC_MANAGED, // For all svcs under a res
	CHECK_IS_THIS_SVC_MANAGED // For a particular svc
} check_proxy_svc_info_t;
//
// This structure is used in the SMF proxy methods
// to send the input args to the door server
//
typedef struct smf_proxy_rs_input_args {
	method_type_t method; // Method type
	void *data; // Encapsulated Data
}smf_proxy_rs_input_args_t;

// Typedef for door server function prototype.
typedef void door_server(void *cookie,
    char *dataptr, size_t data_size,
    door_desc_t *desc_ptr, size_t ndesc);

// Globale functions.
int reim_svc_reg(door_server *proc);
#endif /* _REIM_COMMON_H_ */
