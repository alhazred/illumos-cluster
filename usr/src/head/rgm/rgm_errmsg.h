/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_RGM_ERRMSG_H_
#define	_RGM_ERRMSG_H_

#pragma ident	"@(#)rgm_errmsg.h	1.90	09/01/06 SMI"

#ifdef	__cplusplus
extern "C" {
#endif

/*
 * Error Messages
 */
extern const char *REM_INVALID_NODE;
extern const char *REM_INVALID_RG;
extern const char *REM_DUP_RG;
extern const char *REM_INVALID_RS;
extern const char *REM_INVALID_RT;
extern const char *REM_RG_EXISTS;
extern const char *REM_RS_EXISTS;
extern const char *REM_RT_EXISTS;
extern const char *REM_EMPTY_NODELIST;
extern const char *REM_RG_ON_NODE;
extern const char *REM_RG_MP_GT_0;
extern const char *REM_RG_MP_LT_CURRMASTER;
extern const char *REM_RG_DP_GT_MP;
extern const char *REM_RG_DP_GT_NODES;
extern const char *REM_RG_FO_MP_GT_1;
extern const char *REM_RG_FO_DP_GT_1;
extern const char *REM_RG_DEP_NON_RG;
extern const char *REM_RG_AFF_NON_RG;
extern const char *REM_RG_AFF_SELF;
extern const char *REM_RG_AFF_MANAGED;
extern const char *REM_RG_AFF_STATE;
extern const char *REM_RG_AFF_AUTO_START;
extern const char *REM_RG_AFF_DELEGATE_MANY;
extern const char *REM_NODELIST_NO_COMMON;
extern const char *REM_RG_DEL_AFF;
extern const char *REM_RG_DEP_UNMAN_RG;
extern const char *REM_PP_IV_LT_0;
extern const char *REM_RG_DEP_CYCLIC;
extern const char *REM_RG_CYCLIC_COMBINED;
extern const char *REM_RS_CYCLIC_COMBINED;
extern const char *REM_INTRARG_ANYNODE;
extern const char *REM_PROP_TUNE_AT_CREATION;
extern const char *REM_PROP_TUNE_NONE;
extern const char *REM_PROP_TUNE_WHEN_DISABLED;
extern const char *REM_PROP_TUNE_WHEN_DISABLED_NODE;
extern const char *REM_PROP_NOT_FOUND;
extern const char *REM_PROP_NO_DEFAULT;
extern const char *REM_PROP_EMPTYLIST;
extern const char *REM_PROP_SCALABLE;
extern const char *REM_RG_R_MISMATCH;
extern const char *REM_RG_UNMANAGED;
extern const char *REM_INTERNAL;
extern const char *REM_INTERNAL_MEM;
extern const char *REM_INTERNAL_RG_CR;
extern const char *REM_INTERNAL_RG_UP;
extern const char *REM_INTERNAL_RG_DL;
extern const char *REM_RG_BUSY;
extern const char *REM_RS_IN_BUSY_RG;
extern const char *REM_RG_AFF_BUSY;
extern const char *REM_RG_AFF_SAME;
extern const char *REM_RG_AFF_STOPFAILED;
extern const char *REM_RG_STRONG_POS;
extern const char *REM_RG_STRONG_NEG;
extern const char *REM_RG_STOPFAILED;
extern const char *REM_UPDATE_RS_LIST;
extern const char *REM_RG_DEL_RS;
extern const char *REM_RG_DEL_DEP;
extern const char *REM_PROP_CANT_UPDATE;
extern const char *REM_DUPNODE;
extern const char *REM_VALIDATE_NO_NODES;
extern const char *REM_VALIDATE_FAILED;
extern const char *REM_RS_DEL_ENABLED;
extern const char *REM_RS_DEL_DEP;
extern const char *REM_RS_RDEP_METHOD;
extern const char *REM_RS_DEP_DISABLED;
extern const char *REM_RS_DEP_INVALID_STATE;
extern const char *REM_RS_DUP_DEP;
extern const char *REM_RT_NODELIST;
extern const char *REM_RT_0_NODELIST;
extern const char *REM_RT_RG_BUSY;
extern const char *REM_RT_DEL_RG_BUSY;
extern const char *REM_RT_DEL_R_EXISTS;
extern const char *REM_NODE_NOT_MEMBER;
extern const char *REM_ALLNODE_INVAL;
extern const char *REM_RG_MAXPRIMARIES;
extern const char *REM_RG_MASTERS;
extern const char *REM_RG_TO_STOPFAILED;
extern const char *REM_RS_IN_RG_TO_STOPFAILED;
extern const char *REM_RG_UNMAN_DEPEND;
extern const char *REM_RG_UNMAN_NOT_OFF;
extern const char *REM_RG_UNMAN_RS_NOT_DIS;
extern const char *REM_R_DEP_DISABLED;
extern const char *REM_R_DEP_ENABLED;
extern const char *REM_CLRECONF;
extern const char *REM_UPDATE_RGMODE;
extern const char *REM_RS_DEP_CYCLIC;
extern const char *REM_RS_DEP_NET_CYCLIC;
extern const char *REM_RS_NOT_NR;
extern const char *REM_RG_NAME_INVALID;
extern const char *REM_RS_NAME_INVALID;
extern const char *REM_INVALID_PROP_NAME;
extern const char *REM_INVALID_ENUM_LIST;
extern const char *REM_INVALID_PROP_VALUES;
extern const char *REM_INVALID_FLAG;
extern const char *REM_RT_NAME_INVALID;
extern const char *REM_INVALID_DESC_VALUES;
extern const char *REM_UNMANAGED_BUSTED;
extern const char *REM_OFFLINE_BUSTED;
extern const char *REM_SINGLE_INST;
extern const char *REM_RG_DEP_ONLINE;
extern const char *REM_R_DEP_ONLINE;
extern const char *REM_RG_DEP_OFFLINE;
extern const char *REM_RG_DEP_DESIRED_PRIMARIES;
extern const char *REM_RG_DEP_AUTO_START;
extern const char *REM_RS_NR_USED_DISABLED;
extern const char *REM_NR_USED_DISABLE_INVALID;
extern const char *REM_NR_USED_DELETE_INVALID;
extern const char *REM_PROP_PSTR_MINLEN;
extern const char *REM_PROP_PSTR_MAXLEN;
extern const char *REM_PROP_PSARRAY_MINSIZE;
extern const char *REM_PROP_PSARRAY_MAXSIZE;
extern const char *REM_PROP_PINT_MIN;
extern const char *REM_PROP_PINT_MAX;
extern const char *REM_RGM_NOT_STARTED;
extern const char *REM_PROP_NOT_EXIST;
extern const char *REM_PROP_IS_EXT;
extern const char *REM_PROP_IS_SYS;
extern const char *REM_START_FAILED;
extern const char *REM_RG_FO_STARTFAIL;
extern const char *REM_RG_NOMASTER;
extern const char *REM_RG_SW_BUSTED;
extern const char *REM_DUP_RG_DEP;
extern const char *REM_DUP_RS_DEP;
extern const char *REM_DUP_RS_NETRESUSED;
extern const char *REM_FED_RPC;
extern const char *REM_FED_SIGNAL;
extern const char *REM_FED_ABNORM;
extern const char *REM_FED_VALIDATE;
extern const char *REM_FED_FAILED_EXEC;
extern const char *REM_FED_DUP;
extern const char *REM_FED_EXEC;
extern const char *REM_FED_STAT;
extern const char *REM_FED_UNKNC;
extern const char *REM_FED_AUTH;
extern const char *REM_FED_CONNECT;
extern const char *REM_FED_TIMEDOUT;
extern const char *REM_FED_QUIESCED;
extern const char *REM_FED_NOTAG;
/*
 * extern const char *REM_ZONESCHECK;
 * extern const char *REM_RG_NODELIST_ZONESCHECK;
 * extern const char *REM_GLOBALZONE_ZONESCHECK;
 */
extern const char *REM_RG_NGZONE;
extern const char *REM_RG_NODELIST_NGZONE;
extern const char *REM_RT_NGZONE;
extern const char *REM_R_NODELIST_NGZONE;
extern const char *REM_R_GLOBALFLAG_NGZONE;
extern const char *REM_EVAC_NGZONE;
extern const char *REM_PROP_NGZONE;
extern const char *REM_RG_START_FAILED;

/* SC SLM add start */
extern const char *REM_FED_SLM_ERROR;
/* SC SLM add end */
extern const char *REM_WRONG_OS;
extern const char *REM_MULT_RT_MATCHES;
extern const char *REM_VENDOR_ID_INVALID;
extern const char *REM_RT_NO_VERSION;
extern const char *REM_ILLEGAL_RT_VERS_CHARS;
extern const char *REM_UPGRADE_AT_CREATION;
extern const char *REM_UPGRADE_WHEN_UNMONITORED;
extern const char *REM_UPGRADE_WHEN_OFFLINE;
extern const char *REM_UPGRADE_WHEN_DISABLED;
extern const char *REM_UPGRADE_WHEN_UNMANAGED;
extern const char *REM_UPDATE_TV_AT_CREATTION;
extern const char *REM_SYSTEM_RT;
extern const char *REM_SYSTEM_RG;
extern const char *REM_SYSTEM_RG_SWPRIM;
extern const char *REM_SYSTEM_RES;
extern const char *REM_API_VERSION_INVALID;
extern const char *REM_API_VERSION_LOW;
extern const char *REM_EVAC_FAILED;
extern const char *REM_RG_NAMESERVER;
extern const char *REM_ICRD_NAMESERVER;
/* SC SLM add start */
extern const char *REM_RG_SLM_TYPE_WR;
extern const char *REM_RG_SLM_PSET_TYPE_WR;
extern const char *REM_RG_SLM_CPU_GARBAGE;
extern const char *REM_RG_SLM_CPU_TOO_BIG;
extern const char *REM_RG_SLM_NAME_LENGTH;
extern const char *REM_RG_SLM_NAME_CLASH;
extern const char *REM_SLM_PSET_AND_ZONE;
extern const char *REM_SLM_PSET_TYPE_IDENTICAL_IN_ZONE;
extern const char *REM_SLM_NOT_AUTOMATED;
extern const char *REM_SLM_NO_DEDICATED;
extern const char *REM_SLM_NO_PSET_MIN;
extern const char *REM_SLM_TYPE_DIFF_IN_ZONE;
extern const char *REM_RG_SLM_CPU_ZERO;
extern const char *REM_RG_SLM_PSET_MIN_TOO_BIG;
/* SC SLM add end */

extern const char *REM_RG_ONLINING_FAILED;
extern const char *REM_RG_OFFLINING_FAILED;
extern const char *REM_RG_RST_FAILED;
extern const char *REM_RG_RST_FAILED_ON_NODE;
extern const char *REM_NODES_NOT_IN_NODELIST;
extern const char *REM_RS_IN_BUSY_RG;
extern const char *REM_RS_IN_STOPFAILED_RG;
extern const char *REM_RS_IN_SYSTEM_RG;
extern const char *REM_DUPNODE_IN_NODELIST;
extern const char *REM_RG_CYCLIC_IMPL_DEP;
extern const char *REM_INVALID_RTR_PROP;
extern const char *REM_RGM_GET_REMOTE_R_STATE_FAILED;
extern const char *REM_RGM_NOTIFY_REMOTE_R_STATE_FAILED;
extern const char *REM_RGM_CREATE_DEP_INFO_FAILED;
extern const char *REM_RGM_UPDATE_DEP_INFO_FAILED;
extern const char *REM_CREATE_ICDEP_LOCAL_CLUSTER;
extern const char *REM_ICRD_SET_FAIL;
extern const char *REM_INVALID_REMOTE_R;
extern const char *REM_UPDATE_ICDEP_LOCAL_CLUSTER;
extern const char *REM_CREATE_IC_AFF_LOCAL_CLUSTER;
extern const char *REM_RGM_CREATE_AFF_INFO_FAILED;
extern const char *REM_RGM_UPDATE_AFF_INFO_FAILED;
extern const char *REM_UPDATE_ICAFF_LOCAL_CLUSTER;
extern const char *REM_IC_RG_DEP_NOT_SUPPORTED;
extern const char *REM_ONLY_GZ_ZC_ALLOWED;
extern const char *REM_RGM_IC_UNREACH;
extern const char *REM_RGM_IC_NOT_ALLOWED;
extern const char *REM_RGM_IC_CYCLIC_DEP;
extern const char *REM_VALUE_OUT_OF_RANGE;
/*
 * Warning Messages
 */
extern const char *RWM_PROP_AFFINITY;
extern const char *RWM_PROP_RR;
extern const char *RWM_RG_NGZONE;
extern const char *RWM_VALIDATE_NODES;
extern const char *RWM_MON_START_FAILED;
extern const char *RWM_PENDING_ONLINE_BLOCKED;
extern const char *RWM_STOP_FAILED;
extern const char *RWM_SOME_RS_ONLINE;
extern const char *RWM_MON_NOT_DEF;
extern const char *RWM_PROJECT_CHANGED;
extern const char *RWM_FINIMETHOD;
extern const char *RWM_RG_DEP_ONLINE;
extern const char *RWM_RG_DEP_OFFLINE;
extern const char *RWM_AFF_DESIRED_PRIM;
extern const char *RWM_NODELIST_NO_COMMON;
extern const char *RWM_AFF_LESS_DESIRED_P;
extern const char *RWM_RG_AFF_MANAGED;
extern const char *RWM_AFF_STRONG_MULTI_RG;
extern const char *RWM_RG_AFF_STATE;
extern const char *RWM_AFFS_OFFLINE;
extern const char *RWM_RG_NOREBALANCE;
extern const char *RWM_RG_NOREBAL_UNMAN;
extern const char *RWM_RG_NOREBAL_SUSP;
extern const char *RWM_AFF_SWITCH_DELEGATE_USED;
extern const char *RWM_INVALID_RS;
extern const char *RWM_MON_RESTART_DEF;
extern const char *RWM_SYSTEM_RG_SUSPEND;
extern const char *RWM_SYSTEM_RG_QUIESCE;
extern const char *RWM_QUIESCE_STOP_FAILED;
extern const char *RWM_EVAC_SUSPENDED_RG;
extern const char *RWM_NRU_ADDED_TO_RDEP;
extern const char *RWM_NRU_OVERRIDE_DEP;
extern const char *RWM_UNCONFIGURED_ZONES;
extern const char *RWM_RG_UNMANAGED_TO_OFFLINE;
extern const char *RWM_RG_OFFLINE_TO_UNMANAGED;
extern const char *RWM_SKIP_RG_EVAC_THREAD;
extern const char *RWM_SKIP_RG;
extern const char *RWM_RG_STOPPED;
extern const char *RWM_RG_REBALANCED;
extern const char *RWM_R_ENABLED;
extern const char *RWM_R_DISABLED;
extern const char *RWM_R_MON_ENABLED;
extern const char *RWM_R_MON_DISABLED;
extern const char *RWM_RG_RESTARTED;
extern const char *RWM_R_DELETED;
extern const char *RWM_RG_DELETED;
extern const char *RWM_QUIESCE_RG;
extern const char *RWM_RG_OFFLINED;
extern const char *RWM_RG_ONLINED;
extern const char *RWM_RG_REMASTERED;
extern const char *RWM_R_NOT_IN_STOP_FAIL;
extern const char *RWM_RS_DEP_ONLINE;
extern const char *RWM_DISABLE_DEP_ONLINE;
extern const char *RWM_DUPLICATE_R_OPERAND;
extern const char *RWM_DUPLICATE_RG_OPERAND;
#ifdef	__cplusplus
}
#endif

#endif	/* !_RGM_ERRMSG_H_ */
