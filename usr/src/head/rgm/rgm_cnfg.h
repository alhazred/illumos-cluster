/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RGM_CNFG_H_
#define	_RGM_CNFG_H_

#pragma ident	"@(#)rgm_cnfg.h	1.44	08/06/23 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <scha.h>
#include <rgm/rgm_common.h>
#include <scha_err.h>
#include <rgm/scha_priv.h>
/*
 * Low level RGM API for creating/removing/updating ccr tables for RGM objects.
 * The functions listed in this file are called by rgmd directly or by
 * the administration API (rgm_scrgadm.h).  They won't do any semantic checking
 * of the value passed by the caller or the info stored in ccr tables.
 * It is the caller's responsibility to provide valid input parameter values;
 * input parameter pointers must be non-NULL.  The caller also must provide
 * valid pointers for output parameters.
 */


/*
 *    RESOURCE GROUP
 */


/* SC SLM add-on start */
#define	SCSLM_RG_NAME_MAX_LENGTH	58
/* SC SLM add-on end */


/*
 * rgmcnfg_create_rgtable()
 *	This function creates a new resource group in the RGM configuration.
 *	The new-created RG will stay in unmanaged state until it is switched
 *	to offline.
 *	Note: resource_list should be null at the time RG is created.  It
 *	can only be updated by rgmcnfg_add_resource(), rgmcnfg_update_resource()
 *	and rgmcnfg_remove_resource().
 */
scha_errmsg_t rgmcnfg_create_rgtable(rgm_rg_t *rg, const char *zc_name);


/*
 * rgmcnfg_update_rgtable()
 *	This function updates the configuration of the RG specified
 *	by rg_name;
 */
scha_errmsg_t rgmcnfg_update_rgtable(rgm_rg_t *rg, const char *zc_name,
    boolean_t rg_unset_aff);


/*
 * rgmcnfg_remove_rgtable()
 *	This function removes the CCR table of given RG.
 */
scha_errmsg_t rgmcnfg_remove_rgtable(name_t rgname, const char *zc_name);


/*
 * rgmcnfg_rg_exist()
 * ------------------
 * This function checks if the given RG name is registered in the cluster.
 * It returns B_FALSE and an error code in the err out param if there is a
 * problem reading the CCR or the current RGM version.
 */
boolean_t rgmcnfg_rg_exist(name_t rgname, scha_errmsg_t *err,
    const char *zc_name);


/*
 * rgmcnfg_read_rgtable()
 *	This function is for retrieving the configuration of RG.
 *	If this function returns SCHA_ERR_NOERR, it is caller's
 *	responsibility to free all memory associated with the
 *	rgmcnfg_rg_t by calling rgmcnfg_free_rg().
 * rgmcnfg_get_rglist()
 *	This function finds all existing RG names by looking up the
 *	CCR table names which have the prefix RGM_PREFIX_RG.
 */
scha_errmsg_t rgmcnfg_read_rgtable(name_t rgname, rgm_rg_t **rg,
    const char *zone);
scha_errmsg_t rgmcnfg_get_rglist(namelist_t **managed_rgs,
	namelist_t **unmanaged_rgs, boolean_t only_local_rgs, const char *zone);


/*
 * rgmcnfg_set_rgunmanaged()
 *	This function is used to change the RG state from UNMANAGED
 *	to managed or from managed to UNMANAGED in ccr table.
 *	If unmanaged is B_TRUE, the RG will be set to Unmanaged state.
 */
scha_errmsg_t rgmcnfg_set_rgunmanaged(char *rgname,
    boolean_t unmanaged, const char*zc_name);


/*
 *	RESOURCE TYPE
 */

/*
 * rgmcnfg_create_rttable()
 *	This function creates a ccr table for the new-registered Resource Type.
 */
scha_errmsg_t rgmcnfg_create_rttable(rgm_rt_t *rt, const char *zc_name);


/*
 * rgmcnfg_update_rttable()
 *
 * This function can only be used to update the values of inst_nodes and
 * the RT_SYSTEM property.  Other values will be ignored.
 *
 * Note that the new values come from instnodes and rt_system, not from the
 * rt structure itself (which is assumed not to be updated yet).
 */
scha_errmsg_t rgmcnfg_update_rttable(rgm_rt_t *rt,
    const char *instnodes, const boolean_t rt_system, const char *zc_name);


/*
 * rgmcnfg_remove_rttable()
 *	This function removes the ccr table which contains the info of the
 *	unregistering Resource Type.
 */
scha_errmsg_t rgmcnfg_remove_rttable(name_t rtname, const char *zc_name);


/*
 * rgmcnfg_read_rttable()
 *	This function is for retrieving the Resource Type configuration.
 *	If this function returns SCHA_ERR_NOERR, it is caller's
 *	responsibility to free all memory associated with the
 *	rgmcnfg_rt_t by calling rgmcnfg_free_rt().
 * rgmcnfg_get_rtlist()
 *	This function is for retrieving the list of registered Resource Types.
 *	If this function returns SCHA_ERR_NOERR, it is caller's
 *	responsibility to free all memory associated with the
 *	namelist_t by calling rgmcnfg_free_nlist().
 */
scha_errmsg_t rgmcnfg_read_rttable(name_t rtname,
    rgm_rt_t **rt, const char *zc_name);
scha_errmsg_t rgmcnfg_get_rtlist(namelist_t **rts, const char *zc_name);


/*
 * rgmcfng_get_invalid_tables
 * --------------------------
 * Fills in table_names with the names of all the invalid RT and RG tables
 * in the ccr directory.  The caller is responsible for freeing the linked
 * list of which *table_names is the head.
 */
scha_errmsg_t rgmcnfg_get_invalid_tables(namelist_t **table_names,
    const char *zc_name);


/*
 * rgmcnfg_rt_exist()
 * ------------------
 * This function checks if the given RT name is registered in the cluster.
 * It returns B_FALSE and an error code in the err out param if there is a
 * problem reading the CCR.
 */
boolean_t rgmcnfg_rt_exist(name_t rtname,
    scha_errmsg_t *err, const char *zc_name);

/*
 * rgmcnfg_get_rtrealname()
 *	This function returns in 'rt_realname' the CCR-stored name of
 *	the resource type whose name was specified by the caller as
 *      'rtname'.  The fully qualified name is of the form
 *	<vendor id>.<rtname>:<version>.
 */
scha_errmsg_t rgmcnfg_get_rtrealname(name_t rtname,
    char **rt_realname, const char *zc_name);


/*
 *    RESOURCE
 */


/*
 * rgmcnfg_add_resource()
 *	This function adds a new resource in the RGM configuration.
 *	Note: Resource name must be unique in the cluster.
 */
scha_errmsg_t rgmcnfg_add_resource(rgm_resource_t *resource);


/*
 * rgmcnfg_resource_tmpl()
 *
 * This function creates an rgm_resource_t struct and creates system
 * properties and extension properties from the paramtable for the Resource
 * Type specified in rtname.
 *
 * For every property in the paramtable, a structure for that property
 * is added to the r_properties or r_ext_properties list.
 *
 * If a property has a default in the paramtable, that default value is
 * set in the structure.  If there is no default value specified, but
 * the property is declared in the paramtable, then a struct for the property
 * is created and added to the appropriate properties list, but
 * no default value is specified.
 *
 * If the returned scha_errmsg_t's err_code is SCHA_ERR_NOERR, then *r will
 * contain a pointer to the newly allocated rgm_resource_t structure. If the
 * rtname is invalid, or if another error is encountered, the appropriate
 * error code is returned, and *r is set to NULL.
 *
 * Note that the caller needs to free all memory associated with the
 * rgm_resource_t.
 */
scha_errmsg_t rgmcnfg_resource_tmpl(const char *rname,
    const char *rgname, name_t rtname, rgm_rt_t *rt, rgm_resource_t **r);

/*
 * rgmcnfg_resource_tmpl_empty()
 *
 * Like rgmcnfg_resource_tmpl, except that no property or extension property
 * structures are created.
 */
scha_errmsg_t rgmcnfg_resource_tmpl_empty(const char *, const char *,
    name_t rt_name, rgm_rt_t *rt, rgm_resource_t **r);

/*
 * rgmcnfg_update_resource()
 *	This function updates the properties of the resource.
 *	The user may call rgmcnfg_get_resource() first to query the
 *	current settings of resource properties for updating the resource.
 *	It is caller's responsibility to free all memory associated with
 *	rgm_resource_t by calling rgmcnfg_free_resource().
 *	Do not use this function to change the value of on/off_switch
 *	or monitored_switch for the Resource.  Instead, use the
 *	rgmcnfg_set_onoffswitch() and rgmcnfg_set_monitoredswitch()
 *	functions.
 */
scha_errmsg_t rgmcnfg_update_resource(rgm_resource_t *resource,
    const char *zc_name);


/*
 * rgmcnfg_remove_resource()
 *	This function removes the resource information from RGM configuration.
 */
scha_errmsg_t rgmcnfg_remove_resource(name_t resourcename,
    name_t rgname, const char *zc_name);

/*
 * rgmcnfg_apply_rt_defaults
 *
 * Looks up the paramtable for the Resource Type specified in resource->r_type.
 * For each property specified in the paramtable, checks if the property
 * already exists in resource->r_properties or resource->r_ext_properties.
 * If it exists, skip this property.  If it does not exist, and if we have
 * a default specified in the paramtable for the property, creates a new
 * property structure, adds it to the list, and sets the value to the default
 * value from the paramtable.
 */
scha_errmsg_t rgmcnfg_apply_rt_defaults(rgm_resource_t *resource,
    const char *zc_name);


/*
 * rgmcnfg_get_resource()
 * rgmcnfg_get_rsrclist()
 *	These functions retrieve the configuration of resource(s).
 *	If these functions return SCHA_ERR_NOERR, it is caller's
 *	responsibility to free all memory associated with the
 *	rgmcnfg_resource_t by calling rgmcnfg_free_resource() or
 *	rgm_free_rsrclist().
 *
 * Note that rgmcnfg_get_resource reads the resource values from the CCR,
 * and also applies the defaults specified in the RTR file.
 *
 * See the note in the comments for rgmcnfg_get_resource_no_defaults (below)
 * for important information regarding which version of the
 * rgmcnfg_get_resource function should be used.
 */
scha_errmsg_t rgmcnfg_get_resource(name_t resourcename, name_t rgname,
    rgm_resource_t **resource, const char *zc_name);

scha_errmsg_t rgmcnfg_get_rsrclist(name_t rgname,
    namelist_t **resources, const char *zc_name);

/*
 * rgmcnfg_get_resource_no_defaults
 *
 * Like rgmcnfg_get_resource, except that it returns exactly what is read
 * from the CCR, without applying the defaults found in the paramtable for
 * this resource type.
 *
 * Note: in order to support cascading defaults of resource properties,
 * any code that saves a resource configuration to the CCR should
 * have retrieved the original information via
 * rgmcnfg_get_resource_no_defaults, not via rgmcnfg_get_resource.
 * This requirement is needed so that we don't save the RT defaults in the
 * CCR entry for the resource.
 *
 * Return codes are the same as those in rgmcnfg_get_resource.
 */
scha_errmsg_t rgmcnfg_get_resource_no_defaults(name_t resourcename,
    name_t rgname, rgm_resource_t **resource, const char *zc_name);

/*
 * rgmcnfg_copy_resource
 *
 * Copies an rgm_resource_t structure from src into *dest_p.
 * Does a deep copy of every field, allocating new memory where appropriate.
 * If an err_code of SCHA_ERR_NOMEM is returned, no memory will be allocated
 * in *dest_p.  If no error is returned, then *dest_p will contain an
 * exact copy of src.
 */
scha_errmsg_t rgmcnfg_copy_resource(const rgm_resource_t *src,
    rgm_resource_t **dest_p);


/*
 * rgmcnfg_rs_exist()
 *	This function checks if the given Resource name exists in the cluster.
 */
scha_errmsg_t rgmcnfg_rs_exist(name_t rname,
    name_t rgname, const char *zc_name);

/*
 * rgmconfg_set_ok_start_rg(), rgmcnfg_is_ok_start_rg() -
 *	These functions are called by the president.
 *	Resource group can not be started if Ok_To_Start is not set.
 *	This flag is written to the CCR in the following cases:
 *	- At cluster reboot if rg property auto_start_on_new_cluster is TRUE
 *	- A scswitch command for bringing the rg online is issued.
 *	- RG property auto_start_on_new_cluster is updated to TRUE.
 *	The flag will be removed from CCR at cluster reboot when
 *	auto_start_on_new_cluster is FALSE at cluster reboot.
 *
 *	If the property auto_start_on_new_cluster does not exist, it
 *	defaults to TRUE.
 *
 *	When the whole cluster is rebooted, the president sets the
 *	rgl_ok_to_start element of the internal rg state struct to TRUE and
 *	adds the flag Ok_To_Start in the rg ccr table if the property
 *	auto_start_on_new_cluster=TRUE; otherwise it sets rgl_ok_to_start to
 *	FALSE and removes Ok_To_Start from the ccr.  After a cluster
 *	reconfiguration (node join/die or new cluster), the rg will stay
 *	offline if "Ok_To_Start" flag is not in ccr.
 *
 *	The normal failover/failback of the rg will be restored when an
 *	scswitch is issued to bring it online or the property
 *	auto_start_on_new_cluster is changed to TRUE by the scrgadm command.
 *
 */
scha_errmsg_t rgmcnfg_set_ok_start_rg(char *rg_name,
    boolean_t ok_flag, const char *zc_name);
boolean_t rgmcnfg_is_ok_start_rg(char *rg_name, const char *zc_name);

scha_errmsg_t convert_namelist_to_nodeidlist(namelist_t *namelist_in,
    nodeidlist_t **nodeidlist_out);

/*
 * rgmcnfg_cl_resolve - (EUROPA sepcific).
 *      Lookup the object associated with "name" and return a pointer
 *      to information contained in "data".
 */
int rgmcnfg_cl_resolve(char *name, char **data);

/*
 * rgm_check_vp_version
 *
 *	First validate if the version numbers stored in CCR table are
 *	lower than or equal to the current version of rgm.  If the version
 *	in CCR is higher, this shouldn't happen, abort the rgmd.
 *	If the CCR table doesn't exist or the version in CCR is lower than
 *	the current version, return B_TRUE to indicate that rgm needs to
 *	create the table or update the CCR with the current version.
 *	B_FALSE will be returned if the CCR table has the current version
 *	and thus should not be updated.
 */
extern boolean_t rgm_check_vp_version(
    const uint16_t major_number, const uint16_t minor_number);

/*
 * rgm_set_vp_version()
 *
 *	Update the CCR table of rgm version with the input version numbers.
 *	If the CCR table doesn't exist, create the table.
 */
extern void rgm_set_vp_version(
    const uint16_t major_number, const uint16_t minor_number);

/*
 * The following strings are used to pass the value of RT_System
 * between "scrgadm" and idl_scrgadm_rt_update_instnodes in the
 *  instnodes namelist
 */
#define	SCHA_RTSYS_TRUE			"==SET_SYSTEM_TRUE=="
#define	SCHA_RTSYS_FALSE		"==SET_SYSTEM_FALSE=="

#ifdef __cplusplus
}
#endif
#endif /* _RGM_CNFG_H_ */
