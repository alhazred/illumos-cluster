/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_RGM_SCSWITCH_H
#define	_RGM_SCSWITCH_H

#pragma ident	"@(#)rgm_scswitch.h	1.31	08/06/20 SMI"

/*
 * This file contains a list of the high level project-private API
 * functions which are called by scswitch command, GUI or public API
 * to perform ownership change of RGs and state change of RGM objects.
 */



#ifdef	__cplusplus
extern "C" {
#endif


#include <sys/types.h>
#include <scha.h>
#include <rgm/scha_priv.h>

typedef enum scswitch_obj_state {
	SCSWITCH_UNMANAGED = 0,
	SCSWITCH_OFFLINE
} scha_scswitch_obj_state_t;


/*
 * scswitch_error_flag_t -
 * 	The system administrator may use scswitch(1m) with -c option to
 *	clear the per-node per-Resource error flags listed below.
 *	The first three of these will clear the given error flag on the
 *	given Resource and node(s).  The clearing of STOP_FAILED
 *	places the Resource into the OFFLINE state on the given node(s).
 *	The system administrator should assure that the error condition
 *	has been remedied before clearing these flags.
 */
typedef enum scswitch_error_flag {
	UPDATE_FAILED = 0,
	INIT_FAILED,
	FINI_FAILED,
	STOP_FAILED
} scswitch_error_flag_t;


/*
 * switch_rg_action_t flag -
 *
 * Tells rgm_scswitch_switch_rg which action to perform.
 *
 * RGACTION_NONE specifies that no action is set, which implies
 * that we are switching primaries.
 */

typedef enum switch_rg_action {
	RGACTION_NONE,
	RGACTION_EVACUATE,
	RGACTION_REBALANCE,
	RGACTION_CLUSTER_SHUTDOWN,
	RGACTION_ONLINE,
	RGACTION_OFFLINE,
	RGACTION_SWITCH_BACK
} switch_rg_action_t;


/*
 * rgm_scswitch_switch_rg() -
 * 	Switch RGs to specified nodes.
 *
 *	Parameters rg_list and node_list, are pointers to a NULL
 *	terminated array of pointers to NULL terminated strings.
 *
 *	The rg_action parameter indicates what action we are going to perform.
 *	Options are - node is being evacuated, or rgs are being rebalanced.
 *	evac_timeout provides the timeout interval in seconds for evacuation
 *	persistence after the evacuation has completed.
 *
 *	v_flag is the verbose flag used by the newcli fopr the verbose
 *	print.
 *
 *	Returns err_code of SCHA_ERR_NOERR on success;
 *	otherwise returns appropriate err_code and err_msg string.
 */
extern scha_errmsg_t rgm_scswitch_switch_rg(const char **node_list,
    const char **rg_list,
    switch_rg_action_t rg_action, ushort_t evac_timeout, boolean_t v_flag,
    const char *zonename);

/*
 * rgm_scswitch_quiesce_rgs() -
 * Quiesce all the RGs
 * Parameters rg_list is pointer to a NULL terminated array of pointers
 * to NULL terminated strings. The resource_list is currently empty.
 * If flag's FAST_QUIESCE bit is ON, then the running methods of resources
 * in the resourcegroup are killed to perform the fast quiesce.
 * Returns err_code  of SCHA_ERR_NOERR on success;
 * otherwise returns appropriate err_code and err_msg string.
 * verbose_flag is the verbose flag used by the newcli fopr the verbose
 * print.
 */
extern scha_errmsg_t rgm_scswitch_quiesce_rgs(const char **rg_list,
    const char **resource_list, uint_t flags, boolean_t verbose_flag,
    const char *zonename);

/*
 * rgm_scswitch_restart_rg() -
 * 	Restart RGs on the specified nodes.
 *
 *	Both parameters, rg_list and node_list, are pointers to a NULL
 *	terminated array of pointers to NULL terminated strings.
 *
 *	verbose_flag is the verbose flag used by the newcli fopr the verbose
 *	print.
 *
 *	Returns err_code of SCHA_ERR_NOERR on success;
 *	otherwise returns appropriate err_code and err_msg string.
 */
scha_errmsg_t rgm_scswitch_restart_rg(
	const char **node_list, const char **rg_list, boolean_t verbose_flag,
	const char *zonename);


/*
 * rgm_scswitch_change_rg_state() -
 *	Switch specified RGs from unmanaged state to offline state or
 *	from offline state to unmanaged state.
 *	The parameter, rg_list is a pointer to a NULL terminated array
 *	of pointers to NULL terminated strings.
 *
 *	The precondition for switching a managed RG to the UNMANAGED state
 *	is that all Resources contained in the RG must be disabled.
 *
 *	verbose_flag is the verbose flag used by the newcli fopr the verbose
 *	print.
 *
 *	Returns err_code of SCHA_ERR_NOERR on success;
 *	otherwise returns appropriate err_code and err_msg string.
 */
extern scha_errmsg_t rgm_scswitch_change_rg_state(
    const char **rg_list, const scha_scswitch_obj_state_t state,
    boolean_t verbose_flag, const char *zonename);

/*
 * rgm_scswitch_set_resource_switch() -
 * 	Enable or disable resource.
 *	This function sets the onoff_switch or monitored_switch to ENABLED
 *	or DISABLED of the given Resource on a specified set of nodes.
 *	If the nodenames are not mentioned, then the operation is peformed
 *	on all nodes.
 *
 *	In onoff_switch case, it will issue an intention to the RGM to turn
 *	the Resource off by invoking its stopping methods (STOP, STOP_NET)
 *	on appropriate nodes (disable case); or by invoking its starting
 *	methods on appropriate nodes (enable case).
 *
 *	In monitor_switch case, it will issue an intention to the RGM to
 *	turn the flag off by invoking monitor_stop method or turn it on
 * 	by invoking monitor_start method.
 *
 *	The parameter, r_list is a pointer to a NULL terminated array
 *	of pointers to NULL terminated strings; flag is the value would
 *	like to be set; monitor tells difference whether the flag is
 *	to be set to onoff_switch or monitored_switch. The parameter, n_list
 *	is similar to r_list except that it contains a list of nodenames
 *	on which the resource is to be enabled/disabled.
 *
 *	rflg indicates if the recursive option is set
 *
 *	verbose_flag is the verbose flag used by the newcli fopr the verbose
 *	print.
 *
 *	Returns err_code of SCHA_ERR_NOERR on success;
 *	otherwise returns appropriate err_code and err_msg string.
 */
extern scha_errmsg_t rgm_scswitch_set_resource_switch(const char **r_list,
	const scha_switch_t flag, const boolean_t monitor,
	const boolean_t rflg, const char **n_list,
	boolean_t verbose_flag, const char *zonename);

/*
 * rgm_scswitch_clear() -
 * 	Clear error condition.
 *	(See the comments for scswitch_error_flag_t.)
 *
 *	Both parameters, node_list and resource_list, are pointers to a
 *	NULL terminated array of pointers to NULL terminated strings.
 *
 *	Returns err_code of SCHA_ERR_NOERR on success;
 *	otherwise returns appropriate err_code and err_msg string.
 */
extern scha_errmsg_t rgm_scswitch_clear(
	const char **node_list,
	const char **resource_list,
	scswitch_error_flag_t f, const char *zonename);

/*
 * rgm_scswitch_restart_rg() -
 * 	Restart online RG on its current master.
 * 	(See the comments for idl_scswitch_restart_rg()).
 *
 *	Both parameters, node_list and rg_list, are pointers to a
 * 	NULL terminated array of pointers to NULL terminated strings.
 *
 *	verbose_flag is the verbose flag used by the newcli fopr the verbose
 *	print.
 *
 *	Returns err_code of SCHA_ERR_NOERR on success;
 *	otherwise returns appropriate err_code and err_msg string.
 */
extern scha_errmsg_t rgm_scswitch_restart_rg(
    const char **node_list, const char **rg_list, boolean_t verbose_flag,
    const char *zonename);

#ifdef	__cplusplus
}
#endif

#endif	/* _RGM_SCSWITCH_H */
