/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FE_H_
#define	_FE_H_

#pragma ident	"@(#)fe.h	1.5	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <rpc/rpc.h>
#include <rgm/door/fe_door.h>
#include <rgm/security.h>

typedef struct fed_input_args_t {
	int api_name;
	void *data;
} fed_input_args_t;

int fed_door_clnt(void *args, void *result, int api_name,
    client_handle *clnt);

/* Name of door service */
#define	FE_PROGRAM_NAME   "fed"

#define	FEPROC_NULL	0
int feproc_null_2(void *, void *, client_handle *);
extern  bool_t feproc_null_2_svc(void *, void *);
#define	FEPROC_RUN	1
int feproc_run_2(fe_run_args *, fe_run_result *, client_handle *);
extern  bool_t feproc_run_2_svc(fe_run_args *, fe_run_result *);
#define	FEPROC_SUSPEND	2
int feproc_suspend_2(fe_args *, fe_result *, client_handle *);
extern  bool_t feproc_suspend_2_svc(fe_args *, fe_result *);
#define	FEPROC_RESUME	3
int feproc_resume_2(fe_args *, fe_result *, client_handle *);
extern  bool_t feproc_resume_2_svc(fe_args *, fe_result *);
#define	FEPROC_KILL	4
int feproc_kill_2(fe_args *, fe_result *, client_handle *);
extern  bool_t feproc_kill_2_svc(fe_args *, fe_result *);
/* SC SLM addon start */
#define	FEPROC_SLM_UPDATE	5
int feproc_slm_update_2(fe_slm_update_args *, int *, client_handle *);
extern  bool_t feproc_slm_update_2_svc(fe_slm_update_args *, int *);
#define	FEPROC_SLM_CONF		6
int feproc_slm_conf_2(fe_slm_conf_args *, int *, client_handle *);
extern  bool_t feproc_slm_conf_2_svc(fe_slm_conf_args *, int *);
#define	FEPROC_SLM_DEBUG	7
int feproc_slm_debug_2(void *, void *, client_handle *);
extern  bool_t feproc_slm_debug_2_svc(void *, void *);
#define	FEPROC_SLM_CHECK	8
int feproc_slm_check_2(void *, int *, client_handle *);
extern  bool_t feproc_slm_check_2_svc(void *, int *);
/* SC SLM addon end */
extern int fe_program_2_freeresult(SVCXPRT *, xdrproc_t);

/* XDR Functions */

extern void fed_result_args_xdr_free(int api_name, void *result);
extern void fed_input_args_xdr_free(fed_input_args_t *fed_arg);
extern size_t fed_xdr_sizeof(fed_input_args_t *arg);
extern size_t fed_xdr_sizeof_result(void *data, int api);
extern bool_t xdr_fed_input_args(register XDR *xdrs, fed_input_args_t *objp);
extern bool_t xdr_fed_result_args(XDR *, void *, int);

#ifdef __cplusplus
}
#endif

#endif /* _FE_H_ */
