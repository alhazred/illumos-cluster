/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _PNM_LD_H
#define	_PNM_LD_H

#pragma ident	"@(#)pnm_ld.h	1.5	08/05/20 SMI"

/*
 *	Following code is the header file for the libpnm library
 *	that libpnm provides. It defines the data structures used for
 *	communication between libpnm and the PNM daemon.
 */

#include <sys/os.h>

#include <rgm/pnm.h>
#include <pnm/pnm_head.h>
#include <sys/sol_version.h>

#ifdef __cplusplus
extern "C" {
#endif


/* The various commands sent by libpnm to the PNM daemon */
typedef enum command_type {
	GROUP_LIST = 1,
	GROUP_STATUS,
	CALLBACK_REG,
	CALLBACK_UNREG,
	CALLBACK_LIST,
	PNM_IFCONFIG,
	ADAPTERINFO_LIST,
	MAP_ADAPTER,
	MAC_ADDRESS,
	GRP_ADP_STATUS,
	IP_CHECK,
	GROUP_CREATE,
	GET_INSTANCES,
#if SOL_VERSION >= __s10
	ADDRINFO_LIST,
	GET_ZONE_IPTYPE
#else
	ADDRINFO_LIST
#endif /* SOL_VERSION >= __s10 */
} command_type_t;

#define	NUM_GRPS		256
#define	NUM_ADPS		256
#define	NUM_CBS			100

/*
 * These are the data structures for the responses which will be sent by
 * the pnm daemon in response to the libpnm commands. No pointers
 * will be used for this since actual data structures will have to be passed
 * to libpnm. So these data structures will be created by the daemon and
 * communicated to libpnm. From these data structures libpnm can form the
 * data structures to be used by the library clients (users). It should be
 * mentioned here that only the necessary no. of bytes are sent.
 */
typedef struct rsp_group_list {
	int	error;
	char	gl[LINELEN]; /* char array for the list of groups */
} rsp_group_list;

typedef struct rsp_group_status {
	int	error;
	int	status;	/* group status */
	char	adplist[LINELEN];	/* char array for the list of adps */
} rsp_group_status;

typedef struct rsp_callback_reg {
	int	error;
} rsp_callback_reg;

typedef struct rsp_callback_unreg {
	int	error;
} rsp_callback_unreg;

typedef struct rsp_callback_list {
	int	error;
	/* the format is: id1 cmd1:id2 cmd2:....... */
	char	cb_buf[NUM_CBS * CB_LINELEN]; /* char array for callbacks */
} rsp_callback_list;

typedef struct rsp_adapterinfo_list {
	int		error;
	uint_t		num_adps;
	nw_adapter_t	*adpr;	/* dynamic array of network adapter */
} rsp_adapterinfo_list;

typedef struct rsp_map_adapter {
	int	error;
	char	gname[NAMELEN];	/* char array for the group name */
} rsp_map_adapter;

typedef struct rsp_mac_address {
	int	error;
	uint_t	mac_addrs_uniq;	/* 0 = not unique; 1 = unique */
} rsp_mac_address;

typedef struct rsp_get_instances {
	int	error;
	int	result;
} rsp_get_instances;

typedef struct rsp_grp_adp_status {
	int		error;
	uint_t		num_adps;	/* number of adapters */
	adp_status	adp_stat[NUM_ADPS]; /* char array list of adps */
} rsp_grp_adp_status;

typedef struct rsp_ip_check {
	int		error;
	uint_t		num_grps;	/* number of groups */
	grp_name	grp[NUM_GRPS]; /* char array list of grps */
} rsp_ip_check;

typedef struct rsp_group_create {
	int	error;
} rsp_group_create;

#if SOL_VERSION >= __s10

typedef struct rsp_zone_iptype {
	int error;
	pnm_zone_iptype_t type;
} rsp_zone_iptype;

#endif /* SOL_VERSION >= __s10 */

/*
 * Union of all responses. Can be used to estimate the maximum buffer size
 * requirement for receiving any response at libpnm.
 */
typedef struct responses {
	size_t rsp_size;
	union {
		struct rsp_group_list rsp_glrsp;
		struct rsp_group_status rsp_gsrsp;
		struct rsp_callback_reg rsp_crrsp;
		struct rsp_callback_unreg rsp_cursp;
		struct rsp_callback_list rsp_clrsp;
		struct rsp_adapterinfo_list rsp_alrsp;
		struct rsp_map_adapter rsp_marsp;
		struct rsp_mac_address rsp_mdrsp;
		struct rsp_grp_adp_status rsp_gdrsp;
		struct rsp_ip_check rsp_icrsp;
		struct rsp_group_create rsp_gcrsp;
		struct rsp_get_instances rsp_girsp;
#if SOL_VERSION >= __s10
		struct rsp_zone_iptype rsp_ziptype;
#endif /* SOL_VERSION >= __s10 */
	} u;
} responses;

#define	MAX_RSP_SIZE	sizeof (responses)


/*
 * These are the structures for the commands which will be sent by
 * libpnm to the pnm daemon. These data structures will be formed by
 * libpnm and sent to the daemon. From these data structures the daemon will
 * figure out which command has been requested by libpnm and execute the same,
 * after which it will create a response data structure and send it to libpnm.
 */
typedef struct cmd_group_status {
	char	gname[NAMELEN];
} cmd_group_status;

typedef struct cmd_callback_reg {
	char	gname[NAMELEN];
	char	buf[CB_LINELEN];	/* callback id and callback cmd */
} cmd_callback_reg;

typedef struct cmd_callback_unreg {
	char	gname[NAMELEN];
	char	id[LINELEN];	/* callback id */
} cmd_callback_unreg;

typedef struct cmd_callback_list {
	char	gname[NAMELEN];
} cmd_callback_list;

typedef struct cmd_map_adapter {
	char	adp[NAMELEN];
} cmd_map_adapter;

typedef struct cmd_grp_adp_status {
	char	gname[NAMELEN];
} cmd_grp_adp_status;

typedef struct cmd_get_instances {
	char	gname[NAMELEN];
} cmd_get_instances;

typedef struct cmd_group_create {
	char	adp[NAMELEN]; /* adapter name, eg: hme0 */
} cmd_group_create;

#if SOL_VERSION >= __s10

typedef struct cmd_zone_iptype {
	char zonename[ZONENAME_MAX]; /* zone name */
} cmd_zone_iptype;

#endif /* SOL_VERSION >= __s10 */

/*
 * Structure for all commands. Can be used to estimate the maximum buffer size
 * requirement for receiving any command.
 */
typedef struct commands {
	size_t cmd_len;
	command_type_t command;	/* one of the commands */
	union {
		struct cmd_group_status cmd_gscmd;
		struct cmd_callback_reg cmd_crcmd;
		struct cmd_callback_unreg cmd_cucmd;
		struct cmd_callback_list cmd_clcmd;
		struct cmd_map_adapter cmd_macmd;
		struct cmd_grp_adp_status cmd_gdcmd;
		struct cmd_get_instances cmd_gicmd;
		struct cmd_group_create cmd_gccmd;
#if SOL_VERSION >= __s10
		struct cmd_zone_iptype cmd_ztcmd;
#endif /* SOL_VERSION >= __s10 */
	} u;
} commands;

#define	MAX_CMD_SIZE	sizeof (commands)

#ifdef __cplusplus
}
#endif

#endif /* _PNM_LD_H */
