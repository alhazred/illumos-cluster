/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _PNM_HEAD_H
#define	_PNM_HEAD_H

#pragma ident	"@(#)pnm_head.h	1.7	08/09/01 SMI"

/*
 * This file is the general header file for the PNM daemon.
 */

#ifdef linux
#include <sys/os_compat.h>
#endif

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <syslog.h>
#include <stdarg.h>
#include <poll.h>
#include <netdb.h>
#include <signal.h>
#include <fcntl.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>

#include <arpa/inet.h>

#include <sys/types.h>
#include <sys/file.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

#ifdef linux
#include <linux/sockios.h>
#else
#include <sys/sockio.h>
#endif

#include <sys/stat.h>
#include <sys/sysmacros.h>

#ifndef linux
#include <sys/cladm.h>
#include <rgm/sczones.h>
#endif

#include <wait.h>
#include <assert.h>

#include <net/if.h>
#include <net/if_arp.h>
#include <net/route.h>

#ifndef linux
#include <netinet/in.h>
#include <netinet/if_ether.h>
#include <netinet/ip_var.h>
#include <netinet/udp.h>
#include <netinet/udp_var.h>
#include <netinet/tcp.h>
#include <netinet/ip6.h>
#endif

#include <scha.h>
#include <sys/sc_syslog_msg.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef strtok_r
extern char *strtok_r(char *, const char *, char **);
#endif

#define	__BUILTIN_VA_ARG_INCR 1
#define	 DEBUGP(x) if (debug) \
	{ \
		(void) fprintf(stderr, "%u ", (uint_t)pthread_self()); \
		(void) fprintf x; \
		(void) fflush(stdout); (void) fflush(stderr); \
	}

#define	LOCKFILE	"/var/cluster/run/pnm_lock"
#define	CALLFILE	"/var/cluster/run/pnm_callbacks"
#define	DAEMONLOG	"/var/cluster/run/cl_pnmd.log"

#define	ADDLEN		INET_ADDRSTRLEN	/* dotted IP address length + '\0' */
#define	LINELEN		512L

/* This is the max. adp name length. (same as LIFNAMSIZ in <net/if.h>) */
#ifdef linux
#define	NAMELEN		(long)32
#else
#define	NAMELEN		((long)LIFNAMSIZ)
#endif

#define	FILENAMELEN	128	/* length of filename */
#define	MAX_ERRMSG	128
#define	TL_LOG		20
#define	ADPLIST_BUF	65536	/* buf siz for adpinfolist */

/*
 * This means that the Logical IP is said to be "up" if the following flags:
 * UP is on.
 */
#define	IPADDR_UP	IFF_UP

/*
 * Actually the max. # of IPs on an adapter is controlled by the ndd variable
 * ip_addrs_per_if, which can has a default value of 256. It can have a max of
 * 8192. Solaris allows a max value of 8192 since this was used for web
 * hosting, load-balancing etc. But now the load-balancing is done at the dns
 * layer and hence the default of 256 is enough. Also performance degrades if
 * more IPs are hosted on a single interface.
 */
#define	MAX_NUMIFS	256	/* max. # of logical IPs on an adapter */
#define	MAX_NUMADPS	256	/* max. # of adps on a node */


/*
 * The number by which we increase the callback array every time we allocate
 * memory for it.
 */
#define	CALLB_CHUNK		20
#define	CODES			2	/* num of states in the Status array */
#define	PNMD_VERSION		"2.0"
#define	PNMD_PORT		6499
#define	CL_ETHERADDRL		14	/* max # of bytes in any MAC address */
#define	CL_IPV6_ADDR_LEN	16	/* addr len of ipv6 address */

extern int 	errno;

/* PNMD interfaces */

/* Exported by pnmd_main.c */
extern int debug;
extern int pnmd_exiting;
extern int poll_add(int);
extern void poll_remove(int);

/* Exported by pnmd_util.c */
extern int charcmp(uchar_t *, uchar_t *, size_t);
extern int get_line(FILE *, char *);
extern uint_t get_logical_inst(char *);
extern uint_t get_physical_inst(char *);
extern int execute(char *);

/* Exported by pnmd_log.c */
extern sc_syslog_msg_handle_t  msg_pnm;
extern void	log_conferr(char *fmt, ...);
extern void	log_debug(char *fmt, ...);
extern void	log_syserr(char *fmt, ...);

/* Exported by pnmd_server.c */
extern int sv_listener(void);
extern int sv_accept(int);
extern int sv_process(int);
extern void sv_close(void);

/* Exported by pnmd_probe.c */
extern int probing_timeout;
extern int probe_open(void);
extern void probe_close(void);
extern int probe_process(int);
extern int probe_snapshot(void);
extern void probe_check(void);

/* LIBPNM interfaces */
/* Exported by pnm_log.c */
extern sc_syslog_msg_handle_t  msg_libpnm;
extern void	log_err_lib(char *fmt, ...);
extern void	log_syserr_lib(char *fmt, ...);

/* Exported by pnm_util.c */
extern void show_ipmp_state();

/* Lint complains if this prototype is not included directly here. */
extern pthread_t pthread_self(void);

/* Structure for communication between the library and the daemon */
typedef struct nw_adapter {
	uint64_t num_addrs;
	uint64_t adp_flags;
	char adp_group[NAMELEN];
	char adp_name[NAMELEN];
	char *adp_net;	/* dynamic array of net and netmask */
}nw_adapter_t;

/* Structure for communication between the library and the daemon */
typedef struct adp_status {
	char adp_name[NAMELEN];
	int status;
}adp_status;

/* Structure for communication between the library and the daemon */
typedef struct grp_name {
	char grp_name[NAMELEN];
}grp_name;

/*
 * endian conversion helpers (for 64-bits)
 * This method does not really belong here, but where else to put
 * it...
 */
#if defined(_BIG_ENDIAN)
#define	ntohll(x) (x)
#else
#define	ntohll(x) \
	((((unsigned long long)ntohl(x)) << 32) + \
	(((unsigned long long)ntohl(x >> 32))))
#endif /* _BIG_ENDIAN */

#if defined(_BIG_ENDIAN)
#define	htonll(x) (x)
#else
#define	htonll(x) \
	((((unsigned long long)htonl(x)) << 32) + \
	(((unsigned long long)htonl(x >> 32))))
#endif

#ifdef __cplusplus
}
#endif

#endif	/* _PNM_HEAD_H */
