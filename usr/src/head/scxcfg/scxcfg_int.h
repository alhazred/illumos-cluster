/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_SCXCFG_INT_H
#define	_SCXCFG_INT_H

#pragma ident	"@(#)scxcfg_int.h	1.3	08/05/20 SMI"

#include <syslog.h>

#ifdef __cplusplus
extern "C" {
#endif

extern void scxcfg_log(int const pri, char const * const fmt, ...);
extern void scxcfg_log_set_level(int level);
extern void scxcfg_log_get_level(int *level);
extern void scxcfg_log_set_tag(char *tag);


typedef enum scxcfg_errno {
	SCXCFG_OK = 0,			/* normal return - no error */
	SCXCFG_EPERM = 1,		/* permission denied */
	SCXCFG_EEXIST = 2,		/* object already exists */
	SCXCFG_ENOEXIST = 3,		/* object does not exist */
	SCXCFG_ESTALE = 4,		/* object or handle is stale */
	SCXCFG_EUNKNOWN = 5,		/* unkown type */
	SCXCFG_ENOCLUSTER = 6,		/* cluster does not exist */
	SCXCFG_ENODEID = 7,
	SCXCFG_EINVAL = 8,		/* invalid argument */
	SCXCFG_EUSAGE = 9,		/* command usage error */
	SCXCFG_ETIMEDOUT = 10,		/* call timed out */
	SCXCFG_EINUSE = 11,		/* already in use */
	SCXCFG_EBUSY = 12,		/* busy, try again later */
	SCXCFG_EINSTALLMODE = 13,	/* install mode */
	SCXCFG_ENOMEM = 14,		/* not enough memory */
	SCXCFG_ESETUP = 15,		/* setup attempt failed */
	SCXCFG_EUNEXPECTED = 16,	/* internal or unexpected error */
	SCXCFG_EBADVALUE = 17,		/* bad ccr table value */
	SCXCFG_EOVERFLOW = 18,		/* message buffer overflow */
	SCXCFG_ESYSERR = 19,
	SCXCFG_ENOENT = 20,
	SCXCFG_EMODIFIED = 21,
	SCXCFG_ECORRUPT = 22,
	SCXCFG_ENOTSERVER = 23,
	SCXCFG_ENOTIMPLEMENTED = 24,
	SCXCFG_ENOINIT = 25,
	SCXCFG_EIO = 26,		/* IO error */
	SCXCFG_ERANGE = 27,		/* Input out of range */
	SCXCFG_ERECONFIG = 28,		/* cluster is reconfiguring */
	SCXCFG_ENOTFARM = 29,
	SCXCFG_ENOCONN = 30,
	SCXCFG_EUABORT = 31
} scxcfg_errno_t;

extern int scxcfg_farm;

#define	FARM_CONFIG_DIR		"/etc/cluster"
#define	SERVER_NODEID_FILE	"/etc/cluster/nodeid"
#define	FARM_NODEID_FILE	"/etc/cluster/farm_nodeid"
#define	FARM_CONFIG_FILE	"/etc/cluster/cfg"
#define	FARM_AUTH_KEY_FILE	"/etc/cluster/auth_key"
#define	FARM_CLUSTERID_FILE	"/etc/cluster/cluster_id"


/*
 * Default nodeid for the first farm node
 *
 * LN can be Server node (Pre-allocated range of LNIs : 1-64)
 * LN can be a Farm node (Pre-allocated range of LNIs : 65-576)
 *
 */

#define	FIRST_FARM_NODEID	65

#define	FARM_TABLE		"farm"
#define	INFR_TABLE		"infrastructure"

#define	SCXCFG_PORT	6551

#ifdef __cplusplus
}
#endif
#endif /* _SCXCFG_INT_H */
