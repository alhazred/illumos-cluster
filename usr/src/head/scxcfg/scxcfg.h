/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_SCXCFG_H
#define	_SCXCFG_H

#pragma ident	"@(#)scxcfg.h	1.4	08/05/20 SMI"

#include <inttypes.h>

#ifdef __cplusplus
extern "C" {
#endif

#define	FCFG_MAX_LEN	256
#define	SCXCFG_DEFAULT_CTX NULL

typedef struct scxcfg {
	void *handle;
} scxcfg;

typedef struct scxcfg  *scxcfg_t;

typedef struct f_property {
	struct f_property *next;
	const char *value;
	int index;
} f_property_t;

typedef struct scxcfg_error {
	int scxcfg_errno;
	int suberrno;
	union _u {
		int scxcfg_lineno;
		int scxcfg_syserrno;
	} _u;
} scxcfg_error;

typedef struct scxcfg_error *scxcfg_error_t;

int scxcfg_open(scxcfg_t cfg, scxcfg_error_t error);

int scxcfg_close(scxcfg_t cfg);

int scxcfg_getproperty_value(scxcfg_t scxcfg_ptr, const char *context,
	const char *property, char *value, scxcfg_error_t error);

int scxcfg_getlistproperty_value(scxcfg_t scxcfg_ptr, const char *context,
	const char *property, f_property_t **listvalue, int *num,
	scxcfg_error_t error);

int scxcfg_setproperty_value(scxcfg_t scxcfg_ptr, const char *context,
	const char *property, const char *value, scxcfg_error_t error);

int scxcfg_delproperty(scxcfg_t scxcfg_ptr, const char *context,
	const char *property, scxcfg_error_t error);

int scxcfg_geterrno(scxcfg_error_t error);

char *scxcfg_error2string(scxcfg_error_t error);

typedef uint32_t scxcfg_nodeid_t;

int scxcfg_get_local_nodeid(scxcfg_nodeid_t *nodid, scxcfg_error_t error);
int scxcfg_get_local_nodename(scxcfg_t scxcfg_ptr, char *nodename,
    scxcfg_error_t error);
int scxcfg_get_ipaddress(scxcfg_t scxcfg_ptr, scxcfg_nodeid_t nodeid, char *ip,
    scxcfg_error_t error);
int scxcfg_get_nodeid(scxcfg_t scxcfg_ptr, const char *nodename,
    scxcfg_nodeid_t *nodeid, scxcfg_error_t error);
int scxcfg_get_nodename(scxcfg_t scxcfg_ptr, const scxcfg_nodeid_t nodeid,
    char *nodename, scxcfg_error_t error);
int scxcfg_del_node(scxcfg_t scxcfg_ptr, const char *nodename,
    scxcfg_error_t error);
int scxcfg_del_subtree(scxcfg_t scxcfg_ptr, const char *root,
    scxcfg_error_t error);
int scxcfg_isfarm(void);
int scxcfg_isfarm_nodeid(scxcfg_nodeid_t n);
int scxcfg_add_node(scxcfg_t cfg, const char *nodename,
    f_property_t *adapterlist, scxcfg_nodeid_t *nodeid, scxcfg_error_t error);
int scxcfg_lookup_node_property_value(scxcfg_t cfg, const char *context,
    const char *propName, const char *searchValue, scxcfg_nodeid_t *nodeid,
    scxcfg_error_t error);
void scxcfg_freelist(f_property_t *list);

#define	FCFG_OK		0
#define	FCFG_ESYSERR	1
#define	FCFG_ESYNTAX	2
#define	FCFG_EINVAL	3
#define	FCFG_ENOTFOUND	4
#define	FCFG_EBADTYPE	5
#define	FCFG_EAGAIN	6
#define	FCFG_EEXIST	7

#ifdef __cplusplus
}
#endif

#endif /* _SCXCFG_H */
