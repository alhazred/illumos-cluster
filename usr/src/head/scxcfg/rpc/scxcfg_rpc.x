%/*
% * CDDL HEADER START
% *
% * The contents of this file are subject to the terms of the
% * Common Development and Distribution License (the License).
% * You may not use this file except in compliance with the License.
% *
% * You can obtain a copy of the license at usr/src/CDDL.txt
% * or http://www.opensolaris.org/os/licensing.
% * See the License for the specific language governing permissions
% * and limitations under the License.
% *
% * When distributing Covered Code, include this CDDL HEADER in each
% * file and include the License file at usr/src/CDDL.txt.
% * If applicable, add the following below this CDDL HEADER, with the
% * fields enclosed by brackets [] replaced with your own identifying
% * information: Portions Copyright [yyyy] [name of copyright owner]
% *
% * CDDL HEADER END
% */

%/*
% * Copyright 2005 Sun Microsystems, Inc.  All rights reserved.
% * Use is subject to license terms.
% */

typedef string strarr<>;
typedef strarr strarr_list<>;

#ifdef RPC_HDR
%
%#pragma ident	"@(#)scxcfg_rpc.x	1.2	08/05/20 SMI"
%
#endif

%/*
% * File: scxcfg.x  RPC interface
% */

#ifdef linux
%#include <rgm/rpc_services.h>
#endif

%/* Input arguments */

struct scxcfg_input {
	string	name<>;
	int	node_id;
};
typedef struct scxcfg_input scxcfg_input_t;


struct scxcfg_prop_input {
	string      context<>;
	string      key<>;
	string      value<>;
};
typedef struct scxcfg_prop_input scxcfg_prop_input_t;

struct  scxcfg_install_input {
	string		node_name<>;
	string		auth_key<>;
	string		cluster_name<>;
	string		adap1<>;
	string		adap2<>;
};

typedef struct scxcfg_install_input scxcfg_install_input_t;


struct  scxcfg_install_output {
	string		cluster_id<>;
	int		node_id;
	int		error;
};
typedef struct scxcfg_install_output scxcfg_install_output_t;

struct  scxcfg_auth_input {
	string		node_name<>;
	string		cluster_id<>;
	int		node_id;
	string		auth_key<>;
};
typedef struct scxcfg_auth_input scxcfg_auth_input_t;


struct scxcfg_prop_output {
	string		context<>;
	string		key<>;
	string		value<>;
	int		error;
};

typedef struct scxcfg_prop_output scxcfg_prop_output_t;


% /* Bulk transfert of properties */
struct scxcfg_bulk_output {
	int		node_id;
	string		context<>;
	string		buffer<>;
	int             error;
};
typedef scxcfg_bulk_output scxcfg_bulk_output_t;


% /* Generic result structure */
struct scxcfg_output {
	int         error;
};
typedef scxcfg_output  scxcfg_output_t;
	
%/*
% * Program definition.
% */
program SCXCFG_SERVER{
   version SCXCFG_VERS {
	   scxcfg_prop_output_t	SCXCFG_GET_PROPERTY(scxcfg_prop_input_t) = 1;
	   scxcfg_output_t	SCXCFG_SET_PROPERTY(scxcfg_prop_input_t) = 2;
	   scxcfg_output_t	SCXCFG_DEL_PROPERTY(scxcfg_prop_input_t) = 3;
	   scxcfg_bulk_output_t SCXCFG_BULK_TRANSFER(scxcfg_input_t) = 4;
	   scxcfg_install_output_t SCXCFG_INSTALL_FARM(scxcfg_install_input_t) = 5;
	   scxcfg_output_t	SCXCFG_AUTH_FARM(scxcfg_auth_input_t) = 6;
	   scxcfg_output_t	SCXCFG_ALIVE_NODEID(scxcfg_input_t) = 7;
  } = 1;
} = 0x20000900;	


