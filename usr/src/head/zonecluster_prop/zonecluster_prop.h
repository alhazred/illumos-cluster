/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ZONECLUSTERPROP_H
#define	_ZONECLUSTERPROP_H

#pragma ident	"@(#)zonecluster_prop.h	1.5	09/03/31 SMI"

/*
 * header file for sczonecfg command for properties definitions
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <libzccfg/libzccfg.h>

/* resource types: increment RT_MAX when expanding this list */
#define	RT_UNKNOWN	0
#define	RT_ZONENAME	1	/* really a property, but for info ... */
#define	RT_ZONEPATH	2	/* really a property, but for info ... */
#define	RT_AUTOBOOT	3	/* really a property, but for info ... */
#define	RT_POOL		4	/* really a property, but for info ... */
#define	RT_FS		5
#define	RT_IPD		6
#define	RT_NET		7
#define	RT_DEVICE	8
#define	RT_RCTL		9
#define	RT_ATTR		10
#define	RT_DATASET	11
#define	RT_LIMITPRIV	12	/* really a property, but for info ... */
#define	RT_BOOTARGS	13	/* really a property, but for info ... */
#define	RT_BRAND	14	/* really a property, but for info ... */
#define	RT_DCPU		15
#define	RT_MCAP		16
#define	RT_MAXLWPS	17	/* really a rctl alias property, but for info */
#define	RT_MAXSHMMEM	18	/* really a rctl alias property, but for info */
#define	RT_MAXSHMIDS	19	/* really a rctl alias property, but for info */
#define	RT_MAXMSGIDS	20	/* really a rctl alias property, but for info */
#define	RT_MAXSEMIDS	21	/* really a rctl alias property, but for info */
#define	RT_SHARES	22	/* really a rctl alias property, but for info */
#define	RT_SCHED	23	/* really a property, but for info ... */
#define	RT_NODE		24
#define	RT_SYSID	25
#define	RT_CLPRIVNET	26	/* really a property, but for info ... */
#define	RT_IPTYPE	27	/* really a property, but for info ... */
#define	RT_PCAP		28

#define	RT_MIN		RT_UNKNOWN
#define	RT_MAX		RT_PCAP

/* property types: increment PT_MAX when expanding this list */
#define	PT_UNKNOWN	0
#define	PT_ZONENAME	1
#define	PT_ZONEPATH	2
#define	PT_AUTOBOOT	3
#define	PT_POOL		4
#define	PT_DIR		5
#define	PT_SPECIAL	6
#define	PT_TYPE		7
#define	PT_OPTIONS	8
#define	PT_ADDRESS	9
#define	PT_PHYSICAL	10
#define	PT_NAME		11
#define	PT_VALUE	12
#define	PT_MATCH	13
#define	PT_PRIV		14
#define	PT_LIMIT	15
#define	PT_ACTION	16
#define	PT_RAW		17
#define	PT_LIMITPRIV	18
#define	PT_BOOTARGS	19
#define	PT_BRAND	20
#define	PT_NCPUS	21
#define	PT_IMPORTANCE	22
#define	PT_SWAP		23
#define	PT_LOCKED	24
#define	PT_SHARES	25
#define	PT_MAXLWPS	26
#define	PT_MAXSHMMEM	27
#define	PT_MAXSHMIDS	28
#define	PT_MAXMSGIDS	29
#define	PT_MAXSEMIDS	30
#define	PT_MAXLOCKEDMEM	31
#define	PT_MAXSWAP	32
#define	PT_SCHED	33
#define	PT_ROOTPASSWD	34
#define	PT_NAMESERVICE	35
#define	PT_NFS4DOMAIN	36
#define	PT_SECPOLICY	37
#define	PT_SYSLOCALE	38
#define	PT_TERMINAL	39
#define	PT_TIMEZONE	40
#define	PT_NETINTERFACE	41
#define	PT_HOSTNAME	42
#define	PT_PHYSICALHOST	43
#define	PT_CLPRIVNET	44
#define	PT_IPTYPE	45
#define	PT_DEFROUTER	46

#define	PT_MIN		PT_UNKNOWN
#define	PT_MAX		PT_DEFROUTER

#if !defined(BOOTARGS_MAX)
#define	BOOTARGS_MAX	256
#endif

/*
 * These *must* match the order of the RT_ defines above
 */

static char *res_types[] = {
	"unknown",
	"zonename",
	"zonepath",
	"autoboot",
	"pool",
	"fs",
	"inherit-pkg-dir",
	"net",
	"device",
	"rctl",
	"attr",
	"dataset",
	"limitpriv",
	"bootargs",
	"brand",
	"dedicated-cpu",
	"capped-memory",
	ALIAS_MAXLWPS,
	ALIAS_MAXSHMMEM,
	ALIAS_MAXSHMIDS,
	ALIAS_MAXMSGIDS,
	ALIAS_MAXSEMIDS,
	ALIAS_SHARES,
	"scheduling-class",
	"node",
	"sysid",
	"enable_priv_net",
	"ip-type",
	"capped-cpu",
	NULL
};

/*
 * These *must* match the order of the PT_ defines above
 */

static char *prop_types[] = {
	"unknown",
	"zonename",
	"zonepath",
	"autoboot",
	"pool",
	"dir",
	"special",
	"type",
	"options",
	"address",
	"physical",
	"name",
	"value",
	"match",
	"priv",
	"limit",
	"action",
	"raw",
	"limitpriv",
	"bootargs",
	"brand",
	"ncpus",
	"importance",
	"swap",
	"locked",
	ALIAS_SHARES,
	ALIAS_MAXLWPS,
	ALIAS_MAXSHMMEM,
	ALIAS_MAXSHMIDS,
	ALIAS_MAXMSGIDS,
	ALIAS_MAXSEMIDS,
	ALIAS_MAXLOCKEDMEM,
	ALIAS_MAXSWAP,
	"scheduling-class",
	"root_password",
	"name_service",
	"nfs4_domain",
	"security_policy",
	"system_locale",
	"terminal",
	"timezone",
	"netinterface",
	"hostname",
	"physical-host",
	"enable_priv_net",
	"ip-type",
	"defrouter",
	NULL
};

#ifdef __cplusplus
}
#endif

#endif	/* _ZONECLUSTERPROP_H */
