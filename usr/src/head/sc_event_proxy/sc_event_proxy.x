%/*
% * CDDL HEADER START
% *
% * The contents of this file are subject to the terms of the
% * Common Development and Distribution License (the License).
% * You may not use this file except in compliance with the License.
% *
% * You can obtain a copy of the license at usr/src/CDDL.txt
% * or http://www.opensolaris.org/os/licensing.
% * See the License for the specific language governing permissions
% * and limitations under the License.
% *
% * When distributing Covered Code, include this CDDL HEADER in each
% * file and include the License file at usr/src/CDDL.txt.
% * If applicable, add the following below this CDDL HEADER, with the
% * fields enclosed by brackets [] replaced with your own identifying
% * information: Portions Copyright [yyyy] [name of copyright owner]
% *
% * CDDL HEADER END
% */

%/*
% * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
% * Use is subject to license terms.
% */

#ifdef RPC_HDR
%
%#pragma ident	"@(#)sc_event_proxy.x	1.2	08/05/20 SMI"
%
#endif

%/*
% * File: sc_event_proxy.x 
% * This file has the xdr specification for the structure,
% * that is used by the client in the non-global zone to
% * send event related data to the event_proxy server in
% * the global zone.
% */

%/*
% * This struct is used to pack the data
% * needed for logging the sysevent.
% */
struct proxy_event_data {
	string subclass<>;
	string publisher<>;
	unsigned int buf_size;
};
