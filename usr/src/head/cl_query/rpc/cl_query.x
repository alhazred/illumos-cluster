/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 ****************************************************************************
 *
 * Component = CLQUERY library - RPC interface
 *
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *
 *****************************************************************************
 *
 * #pragma ident	"@(#)cl_query.x	1.13	08/05/20 SMI"
 *
 ****************************************************************************
 */

#ifndef _SCHA_CALLS_X
#define _SCHA_CALLS_X


typedef string strarr<>;
typedef strarr strarr_list<>;




%/* Input arguments */
struct cl_query_input {
	string      name<>;
};

typedef struct cl_query_input cl_query_input_t;

struct cl_query_cluster_prop {
  string cluster_name<>;
  string cluster_id<>;
  u_int  install_mode;
  string cluster_version<>;
  u_int  cluster_auth_type;
  strarr_list cluster_join_list;
  string cluster_netaddr<>;
  string cluster_netmask<>;
  string cluster_max_nodes<>;
  string cluster_max_priv_nets<>;
  u_int  fmm_mode;
};
typedef struct cl_query_cluster_prop  *cl_query_cluster_prop_t;

struct cl_query_cluster_output {
  int error;
  cl_query_cluster_prop_t current_cluster;
};
typedef struct cl_query_cluster_output cl_query_cluster_output_t;

%/* This structure defines the adapter port properties */
struct cl_query_port_prop_elem {
	u_int  port_id;
	u_int  port_state;
	string port_name<>;
};
typedef struct cl_query_port_prop_elem *cl_query_port_prop_elem_t;

struct cl_query_port_prop_list {
  cl_query_port_prop_elem_t prop_list<>;
};

typedef struct cl_query_port_prop_list cl_query_port_prop_list_t;


struct cl_query_didpath_list_elem {
  string node_name<>;
  u_int  node_id;
  string phys_path_name<>;
  string mount_point<>;

};

typedef struct cl_query_didpath_list_elem *cl_query_didpath_list_elem_t;

typedef cl_query_didpath_list_elem_t cl_query_didpath_list_t<>;

%/* This structure defines the adapter properties */
struct cl_query_adapter_prop {
	u_int                     adapter_id;
	string                    adapter_name<>;
	string                    device_name<>;
	u_int			  vlan_id;
	u_int                     device_instance;
	u_int                     dlpi_heartbeat_quantum;
	u_int                     dlpi_heartbeat_timeout;
	u_int                     lazy_free;
	string                    ip_addr<>;
	string                    netmask<>;
	string                    node_name<>;
	string                    type<>;
	cl_query_port_prop_list_t port_list;
	u_int                     bandwidth;
	u_int                     network_bandwidth;
	int                       state;
};
typedef cl_query_adapter_prop *cl_query_adapter_prop_t;

%/* Adapter query - result structure */
struct cl_query_adapter_output {
	int                     error;
	cl_query_adapter_prop_t  adapter_list<>;
};
typedef cl_query_adapter_output cl_query_adapter_output_t;


%/* Node properties structure */
struct cl_query_node_prop {
	u_int              node_id;
	string             node_name<>;
	string             private_host_name<>;
        string             reservation_key<>;
	strarr_list        private_adapters;
	strarr_list        public_adapters;
	u_int              cur_quorum_votes;
	u_int              possible_quorum_votes;
	int                state;
        int                status;
        int                node_reboot_flag_status; 
	string		   node_category<>;
};
typedef cl_query_node_prop *cl_query_node_prop_t;

/* Node query result structure */
struct cl_query_node_output {
	int                     error;
	cl_query_node_prop_t    node_list<>;
};
typedef cl_query_node_output cl_query_node_output_t;


%/* Quorum properties structure */
struct cl_query_quorum_prop {
	u_int              current_votes;
	u_int              possible_votes;
	u_int              votes_needed;
	u_int              votes_configured;
	u_int              votes_present;
	string             device_name<>;
	string             device_type<>;
	string             key_name<>;
	strarr_list        disabled_nodes;
	strarr_list        enabled_nodes;
	string             access_mode<>;
	u_int              status;
	u_int              state;
	u_int              owner;
/* NAS specific properties */
	string		   filer_name<>;
	string		   lunid<>;
	string		   lun_name<>;
/* Quorum Server properties */
	string		   qs_host<>;
	string		   qs_port<>;
	string		   qs_name<>;
};
typedef cl_query_quorum_prop *cl_query_quorum_prop_t;

%/* Quorum query result structure */
struct cl_query_quorum_output {
	int                      error;
        cl_query_quorum_prop_t   quorum_list<>;
};
typedef cl_query_quorum_output cl_query_quorum_output_t;

% /* Cable properties structure */
struct cl_query_cable_prop {
        u_int                     cable_id;
	string                    cable_name<>;
	string                    cable_epoint1_name<>;
	string                    cable_epoint2_name<>;
	u_int                     cable_epoint1_type;
	u_int                     cable_epoint2_type;
	u_int                     state;
};
typedef cl_query_cable_prop   *cl_query_cable_prop_t;

% /* Cable properties query result */
struct cl_query_cable_output {
  	int                    error;
	cl_query_cable_prop_t  cable_list<>;
};
typedef cl_query_cable_output cl_query_cable_output_t;

% /* Junction properties structure */
struct cl_query_junction_prop {
        u_int                      junction_id;
        string                     junction_name<>;
        string                     junction_type<>;
        string                     key_name<>;
        cl_query_port_prop_list_t  port_list;
	u_int                      status;
};
typedef cl_query_junction_prop  *cl_query_junction_prop_t;

/* Junction properties query result */
struct cl_query_junction_output {
	int                         error;
	cl_query_junction_prop_t    junction_list<>;
};
typedef cl_query_junction_output cl_query_junction_output_t;

%/* Device properties structure */
struct cl_query_device_prop {
	string                  logical_disk_path<>;
	cl_query_didpath_list_t file_system_path;
	string                  device_name<>;
	string                  primary_node_name<>;
	u_int                   desired_num_secondaries;
	strarr_list             alternate_nodes;
	strarr_list             spare_nodes;
	string                  device_type<>;
	u_int                   failback;
	u_int                   OrderedNodesList;
	u_int                   status;
	string			replication_type<>;
};
typedef cl_query_device_prop *cl_query_device_prop_t;

%/* Device properties query results */
struct cl_query_device_output {
	int                     error;
	cl_query_device_prop_t  device_list<>;
};
typedef cl_query_device_output cl_query_device_output_t;


%/* IPMP group info */
struct cl_query_ipmp_prop {
	int              status;
	string           group_name<>;
	string           node_name<>;
        strarr_list      adapter_list;
};
typedef cl_query_ipmp_prop *cl_query_ipmp_prop_t;

%/* IPMP group query results */
struct cl_query_ipmp_output {
	int                     error;
	cl_query_ipmp_prop_t    ipmp_list<>;
};
typedef cl_query_ipmp_output cl_query_ipmp_output_t;


% /* DISKPATH info */
struct cl_query_diskpath_prop {
	int    status;
	string diskpath_name<>;
	string node_name<>;
};
typedef cl_query_diskpath_prop *cl_query_diskpath_prop_t;

% /* DISKPATH query results */
struct cl_query_diskpath_output {
	int                     error;
	cl_query_diskpath_prop_t  diskpath_list<>;
};
typedef cl_query_diskpath_output cl_query_diskpath_output_t;

% /* TRANSPORT PATH info */
struct cl_query_transportpath_prop {
	u_int    status;
	string transportpath_name<>;
	string transportpath_endpoint_1_name<>;
	string transportpath_endpoint_1_strname<>;
	string transportpath_endpoint_2_name<>;
	string transportpath_endpoint_2_strname<>;
};
typedef cl_query_transportpath_prop *cl_query_transportpath_prop_t;

% /* TRANSPORT PATH query results */
struct cl_query_transportpath_output {
	int                     error;
	cl_query_transportpath_prop_t  transportpath_list<>;
};
typedef cl_query_transportpath_output cl_query_transportpath_output_t;

% /* Query CCR Table Names */
struct table_list {
	int           error;
	strarr_list   table_list;
};
typedef table_list  ccr_table_list_t;

% /* Query CCR Table length */
struct table_size {
	int           error;
	u_int         table_len;
};
typedef table_size ccr_table_size_t;


% /* CCR table elements */
struct ccr_table_elem {
	string        key<>;
	string        value<>;
};
typedef ccr_table_elem *ccr_table_elem_t;

% /* Read CCR Table contents */
struct cl_query_ccr_param {
	string                  table_name<>;
	ccr_table_elem_t        table_rows<>;
	int                     error;
};
typedef cl_query_ccr_param cl_query_ccr_param_t;

% /* Generic result structure */
struct cl_query_output {
	int         error;
};
typedef cl_query_output  cl_query_output_t;

%/*
% * Program definition.
% */
program CLQUERY_SERVER{
   version CLQUERY_VERS {
   cl_query_adapter_output_t  ADAPTER_INFO_GET(cl_query_input_t)      = 1;
   cl_query_node_output_t     NODE_INFO_GET(cl_query_input_t)         = 2;
   cl_query_quorum_output_t   QUORUM_INFO_GET(cl_query_input_t)       = 3;
   cl_query_cable_output_t    CABLE_INFO_GET(cl_query_input_t)        = 4;
   cl_query_junction_output_t JUNCTION_INFO_GET(cl_query_input_t )    = 5;
   cl_query_device_output_t   DEVICE_INFO_GET(cl_query_input_t)       = 6;
   cl_query_ipmp_output_t     IPMP_INFO_GET(cl_query_input_t)         = 7;
   cl_query_diskpath_output_t DISK_PATH_INFO_GET(cl_query_input_t)    = 8;
   cl_query_cluster_output_t  CLUSTER_INFO_GET(cl_query_input_t)      = 9;
   cl_query_transportpath_output_t TRANSPORT_PATH_INFO_GET(cl_query_input_t) = 10;
   ccr_table_list_t           TABLE_NAMES_GET(cl_query_input_t)       = 11;
   ccr_table_size_t           TABLE_LEN_GET(cl_query_input_t)         = 12;
   cl_query_ccr_param_t       READ_TABLE(cl_query_input_t)            = 13;
   cl_query_output_t          CREATE_TABLE(cl_query_ccr_param_t)      = 14;
   cl_query_output_t          WRITE_TABLE_ALL(cl_query_ccr_param_t)   = 15;
   cl_query_output_t          DELETE_TABLE(cl_query_input_t)          = 16;
   cl_query_output_t          DELETE_TABLE_ALL(cl_query_input_t)      = 17;

  } = 1;
} = 0x20000899;

#endif /* _SCHA_CALLS_X */
