/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */


#ifndef _CL_QUERY_TYPES_H
#define	_CL_QUERY_TYPES_H

#pragma ident	"@(#)cl_query_types.h	1.14	08/05/21 SMI"

/*
 * This is the header file for libcl_query.  All users of libcl_query should
 * include this file. It contains functions to retrieve current state
 * of the cluster components incluing:
 * 	Cluster
 *	Node
 *	Transport
 *	Path
 *	HA Device
 *	Quorum Device
 *	RG & Resource
 */

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <sys/errno.h>
#include <sys/types.h>
#include <sys/sc_syslog_msg.h>
#include <scadmin/scconf.h>
#include <scadmin/scstat.h>

#define	CL_QUERY_MAX_STRING_LEN	1024

/* Cluster disk path information */

#define	CLQUERY_DISKPATH_OK	0x1
#define	CLQUERY_DISKPATH_FAIL	0x2
#define	CLQUERY_DISKPATH_INVALID	0x4
#define	CLQUERY_DISKPATH_UNKNOWN	0x8
#define	CLQUERY_DISKPATH_REGISTERED	0x10
#define	CLQUERY_DISKPATH_MONITORED	0x20
#define	CLQUERY_DISKPATH_UNMONITORED	0x40
#define	CLQUERY_DISKPATH_DELETE	0x80

#define	CLQUERY_DISKPATH_IS_OK(x)	   (x & CLQUERY_DISKPATH_OK)
#define	CLQUERY_DISKPATH_IS_FAIL(x)	   (x & CLQUERY_DISKPATH_FAIL)
#define	CLQUERY_DISKPATH_IS_INVALID(x)	   (x & CLQUERY_DISKPATH_INVALID)
#define	CLQUERY_DISKPATH_IS_UNKNOWN(x)	   (x & CLQUERY_DISKPATH_UNKNOWN)
#define	CLQUERY_DISKPATH_IS_REGISTERED(x)  (x & CLQUERY_DISKPATH_REGISTERED)
#define	CLQUERY_DISKPATH_IS_MONITORED(x)   (x & CLQUERY_DISKPATH_MONITORED)
#define	CLQUERY_DISKPATH_IS_UNMONITORED(x) (x & CLQUERY_DISKPATH_UNMONITORED)
#define	CLQUERY_DISKPATH_IS_DELETED(x)	   (x & CLQUERY_DISKPATH_DELETE)

/* Cluster default netaddr, netmask, maxnodes and maxprivnets */
#define	CLQUERY_DEFAULT_NETADDR		SCCONF_DEFAULT_NETNUMBER_STRING
#define	CLQUERY_DEFAULT_NETMASK		SCCONF_DEFAULT_NETMASK_STRING
#define	CLQUERY_DEFAULT_MAXNODES	SCCONF_DEFAULT_MAXNODES_STRING
#define	CLQUERY_DEFAULT_MAXPRIVNETS	SCCONF_DEFAULT_MAXPRIVNETS_STRING

/* definitions for event publishing */
#define	CL_CCRAD "cl_ccrad"
#define	CL_CCR_TABLE "CCR Table Name"

/* Quorum Device Type Strings */
#define	SCSI_TYPE "scsi"
#define	SD_TYPE "shared_disk"
#define	NETAPP_NAS "netapp_nas"
#define	QUORUM_SERVER "quorum_server"

/* Error codes returned by cl_query functions. */
	typedef enum cl_query_errno {
		CL_QUERY_OK = 0,	/* Success */
		CL_QUERY_ENOMEM = 100,	/* not enough memory */
		CL_QUERY_ENOTCLUSTER = 101,	/* not a cluster node */
		CL_QUERY_ENOTCONFIGURED = 102,	/* not found in CCR */
		CL_QUERY_EINVAL = 103,	/* invalid argument */
		CL_QUERY_EPERM = 104,	/* not root */
		CL_QUERY_ECLUSTERRECONFIG = 105, /* cluster is reconfiguring */
		CL_QUERY_ERGRECONFIG = 106,	/* RG is reconfiguring */
		CL_QUERY_EOBSOLETE = 107,   /* Resource/RG has been updated */
		CL_QUERY_EUNEXPECTED = 108, /* internal or unexpected error */
		CL_QUERY_ETIMEDOUT = 109, /* Connection to server failed */
		CL_QUERY_EAUTH = 110,	/* Authentication error */
		CL_QUERY_EIO = 111,	/* IO error */
		CL_QUERY_ECOMM = 112,	/* communication error with server */
		CL_QUERY_ENOENT = 113,	/* Table/Key not found in repository */
		CL_QUERY_EXISTS = 114,	/* Table/Key exist in the repository */
		CL_QUERY_EINVALID = 115, /* Table or repository is invalid */
		CL_QUERY_EMODIFIED = 116, /* CCR Table has been modified */
		CL_QUERY_ECORRUPT = 117, /* CCR repository is corrupted */
		CL_QUERY_ERROR_MAX = 0x7fffffff
	} cl_query_error_t;


	typedef enum cl_query_auth_type {
		CLQUERY_AUTH_UNIX = 0,
		CLQUERY_AUTH_DES = 1,
		CLQUERY_AUTH_UNKNOWN = 2
	} cl_query_auth_type_t;

/* States a resource can be in */
	typedef enum cl_query_state {
		CL_QUERY_ONLINE = 0,	/* resource is running */
		CL_QUERY_OFFLINE = 1,	/* stopped due to user action */
		CL_QUERY_FAULTED = 2,	/* resource stopped due to a failure */
		CL_QUERY_DEGRADED = 3,	/* resource running with a minor */
					/* problem */
		CL_QUERY_WAIT = 4,	/* resource is in transition */
		CL_QUERY_ENABLED = 5,	/* resource is enabled */
		CL_QUERY_DISABLED = 6,	/* resource is disabled */
		CL_QUERY_UNKNOWN = 7,	/* resource state is unknown */
		CL_QUERY_NOTMONITORED = 8,	/* resource is not monitored */
		CL_QUERY_UNCHANGED = 9,	/* FIX_ME: */
		CL_QUERY_PRIMARY = 10,	/* resource is a primary */
		CL_QUERY_SECONDARY = 11,	/* resource is a secondary */
		CL_QUERY_SPARE = 12,	/* resource is a spare */
		CL_QUERY_INACTIVE = 13,	/* resource is inactive */
		CL_QUERY_TRANSITION = 14,	/* resource is changing state */
		CL_QUERY_INVALID = 15,	/* resource is in an invalid state */
		/* Reboot once on failure of all local disks Enable */
		CL_QUERY_REBOOT_FLAG_ENABLE = 16,
		/* Reboot once on failure of all local disks Disable */
		CL_QUERY_REBOOT_FLAG_DISABLE = 17,
		CL_QUERY_STATE_MAX = 0x7fffffff
	} cl_query_state_t;


/* Controller endpoint types */
	typedef enum cl_query_epoint {
		CL_QUERY_EPOINT_TYPE_ADAPTER = 1, /* on node adapter type */
		CL_QUERY_EPOINT_TYPE_CPOINT = 2,  /* off-node connection */
		CL_QUERY_EPOINT_TYPE_INVALID = 3,
		CL_QUERY_EPOINT_TYPE_MAX = 0x7fffffff
	} cl_query_epoint_type_t;

/* Operation/type of query codes */
	typedef enum cl_query_type {
		CL_QUERY_CLUSTER_INFO = 1,   /* Get the cluster information */
		CL_QUERY_NODE_INFO = 2,	/* Get the node properties */
		CL_QUERY_ADAPTER_INFO = 3,	/* Get adapter properties */
		CL_QUERY_QUORUM_INFO = 4,	/* Get quorum properties */
		CL_QUERY_IPMP_INFO = 5,	/* Get the IPMP Group properties */
		CL_QUERY_DEVICE_INFO = 6,	/* Get the device information */
		CL_QUERY_JUNCTION_INFO = 7, /* Get the junction information */
		CL_QUERY_CABLE_INFO = 8,	/* Get the cable info */
		CL_QUERY_DISKPATH_INFO = 9,	/* Get disk path information */
		CL_QUERY_TRANSPORTPATH_INFO = 10, /* Transport path */
		CL_QUERY_INFO_MAX = 0x7fffffff
	} cl_query_type_t;


	typedef char *cl_query_name_t;

/* Node Path Name */
	typedef struct cl_query_path {
		char *node_name;
		char *phys_path_name;
		char *mount_point;
	} cl_query_path_t;


/* Path list structure */
	typedef struct cl_query_path_list {
		uint_t list_len;
		cl_query_path_t *list_val;
	} cl_query_path_list_t;


/* Port Properties  */
	typedef struct cl_port_prop {
		uint_t port_id;
		cl_query_state_t port_state;
		char *port_name;
	} cl_port_prop_t;


/* Port list */
	typedef struct cl_query_port_list {
		uint_t list_len;
		cl_port_prop_t *list_val;
	} cl_query_port_list_t;

/* Structure to return a list of values */
	typedef struct cl_query_list {
		uint_t list_len;	/* Length of the list */
/* List of node/quorum/adapter names */
		cl_query_name_t *list_val;
	} cl_query_list_t;

	/* Result structure returned by the API. */
	/* value_list must be casted to appropriate type */
	/* before accesing it. */

	typedef struct cl_query_result {
		uint_t n_elements;	/* No. of entities returned */
		void *value_list;	/* List of vales */
	} cl_query_result_t;


/* Cluster information */
	typedef struct cl_query_cluster_info {
		cl_query_name_t cluster_id;	/* Cluster id */
		cl_query_name_t cluster_name;	/* Cluster Name */
		cl_query_state_t install_mode;	/* Cluster in install mode ? */
		cl_query_name_t cluster_version;	/* Cluster version */
		cl_query_auth_type_t cluster_auth_type;
		cl_query_list_t cluster_join_list;
		cl_query_name_t cluster_netaddr;
		cl_query_name_t cluster_netmask;
		cl_query_name_t cluster_max_nodes;
		cl_query_name_t cluster_max_priv_nets;
		cl_query_state_t fmm_mode; /* FMM enabled ? */
	} cl_query_cluster_info_t;


/* Cluster Nodes information */
	typedef struct cl_query_node_info {
		uint_t node_id;	/* Node id */
		cl_query_name_t node_name;	/* Node Name */
		cl_query_name_t private_host_name;	/* Private host name */
		cl_query_name_t resv_key;	/* scsi reservation key */
		cl_query_list_t private_adapters; /* List of private adapters */
		cl_query_list_t public_adapters;  /* List of public adapters */
		uint_t cur_quorum_votes;	/* current no. of votes */
		uint_t possible_quorum_votes;	/* Possible votes */
		cl_query_state_t state;	/* Node status */
		cl_query_state_t status;	/* Node status */
		/* "Reboot once on failure of all local disks" flag status */
		cl_query_state_t node_reboot_flag_status;
		cl_query_name_t node_category; /* Node Category */
		struct cl_query_node_info *next;	/* next */
	} cl_query_node_info_t;


/* Adapters information */
	typedef struct cl_query_adapter_info {
		uint_t adapter_id;	/* Adapter Id */
		cl_query_name_t adapter_name;	/* Adapter name */
		cl_query_name_t device_name;	/* device name */
		uint_t vlan_id;		/* VLAN Id */
		uint_t device_instance;	/* Device instance */
		uint_t dlpi_heartbeat_quantum;	/* Heartbeat quantum */
		uint_t dlpi_heartbeat_timeout;	/* Heartbeat timeout */
		uint_t lazy_free;	/* lazy free property */
		cl_query_name_t ip_addr;	/* IP Address */
		cl_query_name_t netmask;	/* Adapter netmask */
		cl_query_name_t node_name;	/* Node name */
		cl_query_name_t type;	/* Transport type */
		cl_query_port_list_t port_list;	/* List of ports */
		cl_query_state_t status;	/* Adapter status */
		uint_t bandwidth;	/* Bandwidth */
		uint_t network_bandwidth;	/* Network bandwidth */
		struct cl_query_adapter_info *next;	/* next */
	} cl_query_adapter_info_t;


/* Cluster Quorum (Node and device quorum) information */

	typedef struct cl_query_quorum_info {
		/*
		 * Current no. of votes for this quorum device
		 */
		uint_t current_votes;
		/*
		 * Possible no. of votes for this quorum device
		 */
		uint_t possible_votes;
		/*
		 * number of vote needed for quorum algorythm
		 */
		uint_t votes_needed;
		/*
		 * number of vote configured for quorum algorythm
		 */
		uint_t votes_configured;
		/*
		 * number of vote configured quorum algorythm
		 */
		uint_t votes_present;
		cl_query_name_t device_name;	/* Device name */
		cl_query_name_t device_type;	/* Device Type */
		cl_query_name_t key_name;	/* Device Key Name */
		cl_query_list_t disabled_nodes;	/* Disabled no. of nodes */
		cl_query_list_t enabled_nodes;	/* Enabled no. of nodes */
		cl_query_name_t access_mode;	/* Quorum access mode */
		cl_query_state_t status;	/* Status of the node */
		cl_query_state_t state;
		uint_t owner;	/* node id of owner */
/* NAS specific properties */
		cl_query_name_t filer_name;	/* Name of the NAS filer */
		cl_query_name_t lunid;		/* Lun ID in the NAS device */
		cl_query_name_t lun_name;	/* Lun Name in the NAS device */
/* Quorum Server properties */
		cl_query_name_t qs_host;		/* IP of the QS */
		cl_query_name_t qs_port;	/* Port number of the QS */
		cl_query_name_t qs_name;	/* Host name of the QS */
		struct cl_query_quorum_info *next;	/* next */
	} cl_query_quorum_info_t;


/* Cluster Devices information */
	typedef struct cl_query_device_info {
		cl_query_name_t logical_disk_path;	/* Logical disk path */
		cl_query_path_list_t file_systems_path;	/* File system path */
		cl_query_name_t device_name;	/* Device name */
		cl_query_name_t primary_node_name;	/* Primary node name */
		uint_t avail_alternate_nodes;	/* Available number of */
						/* alternate nodes */
		cl_query_list_t alternate_nodes; /* List of alternate nodes */
		cl_query_list_t spare_nodes;	/* List of spare nodes */
		cl_query_name_t device_type;	/* Device type */
		cl_query_name_t replication_type;	/* Replication type */
		uint_t failback;
		cl_query_state_t status;	/* Device status */
		cl_query_state_t OrderedNodesList;
		struct cl_query_device_info *next;	/* next */
	} cl_query_device_info_t;


/* Cluster IPMP group information */
	typedef struct cl_query_ipmp_info {
		cl_query_list_t adapter_list;	/* List of adapters in the */
						/* group */
		cl_query_name_t group_name;	/* IPMP group name */
		cl_query_name_t node_name;	/* Node name */
		cl_query_state_t status;	/* IPMP group status */
		struct cl_query_ipmp_info *next;
	} cl_query_ipmp_info_t;


	typedef struct cl_query_diskpath_info {
		cl_query_name_t node_name;	/* node name */
/* disk path name i.e /dev/did/rdsk/d4 */
		cl_query_name_t dpath_name;
		int status;	/* status of diskpath */
		struct cl_query_diskpath_info *next;
	} cl_query_diskpath_info_t;

/* Cable information */
	typedef struct cl_query_cable_info {
		uint_t cable_id;
		cl_query_name_t cable_name;
		cl_query_name_t cable_epoint1_name;
		cl_query_name_t cable_epoint2_name;
		cl_query_epoint_type_t cable_epoint1_type;
		cl_query_epoint_type_t cable_epoint2_type;
		cl_query_state_t state;
		struct cl_query_cable_info *next;
	} cl_query_cable_info_t;


/* Junction information */
	typedef struct cl_query_junction_info {
		uint_t junction_id;
		cl_query_name_t junction_name;
		cl_query_name_t junction_type;
		cl_query_name_t key_name;
		cl_query_port_list_t port_list;
		cl_query_state_t status;
		struct cl_query_junction_info *next;
	} cl_query_junction_info_t;

/* Transport information */
	typedef struct cl_query_transportpath_info {
		cl_query_state_t status;
		cl_query_name_t  transportpath_name;
		cl_query_name_t  transportpath_endpoint_1_name;
		cl_query_name_t  transportpath_endpoint_1_strname;
		cl_query_name_t  transportpath_endpoint_2_name;
		cl_query_name_t  transportpath_endpoint_2_strname;
		struct cl_query_transportpath_info *next;
	} cl_query_transportpath_info_t;

/* List of CCR Tables in the Repository */
	typedef struct cl_query_table_list {
		uint_t table_count;
		char **table_names;
	} cl_query_table_list_t;


/* CCR Table Elements */
	typedef struct cl_query_table_elem {
		char *key;
		char *value;
	} cl_query_table_elem_t;

/* CCR Table contents */
	typedef struct cl_query_table {
		uint_t n_rows;
		cl_query_table_elem_t **table_rows;
	} cl_query_table_t;




/* API to query the info of NODES/QUORUM/DEVICES/ADAPTERS */

	extern cl_query_error_t
	    cl_query_get_info(cl_query_type_t type, cl_query_name_t name,
	    cl_query_result_t *result);

	extern cl_query_error_t
	    cl_query_free_result(cl_query_type_t query_type,
	    cl_query_result_t *result);

	extern cl_query_error_t
	    cl_query_table_names(cl_query_name_t pattern,
	    cl_query_table_list_t *result);

	extern cl_query_error_t
	    cl_query_table_size(cl_query_name_t table_name, uint_t *n_rows);

	extern cl_query_error_t
	    cl_query_create_table(cl_query_name_t table_name,
	    cl_query_table_t *table);

	extern cl_query_error_t
	    cl_query_read_table(cl_query_name_t table_name,
	    cl_query_table_t *table);

	extern cl_query_error_t
	    cl_query_write_table_all(cl_query_name_t table_name,
	    cl_query_table_t *table);

	extern cl_query_error_t
	    cl_query_delete_table_all(cl_query_name_t table_name);

	extern cl_query_error_t
	    cl_query_delete_table(cl_query_name_t table_name);

	extern cl_query_error_t cl_query_free_table(cl_query_table_t *table);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _CL_QUERY_TYPES_H */
