/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _LIBZCCFG_H
#define	_LIBZCCFG_H

#pragma ident	"@(#)libzccfg.h	1.25	09/03/31 SMI"

/*
 * Zone Cluster configuration header file.
 */

/* sys/socket.h is required by net/if.h, which has a constant needed here */
#include <sys/param.h>
#include <sys/fstyp.h>
#include <sys/mount.h>
#include <priv.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/utsname.h>	/* for SYS_NMLN constant */
#include <net/if.h>
#include <stdio.h>
#include <rctl.h>
#include <zone.h>
#include <sys/uuid.h>
#include <sys/clconf.h>

#ifdef __cplusplus
extern "C" {
#endif

/*
 * The error codes below are similar, but not identical to those in the Solaris
 * libzonecfg.h. We don't just use the libzonecfg.h error names because we
 * have to add new error numbers to the list, and the numbers might conflict
 * if Solaris adds new errors to their list.
 */
#define	ZC_OK			0
#define	ZC_EMPTY_DOCUMENT	1	/* XML doc root element is null */
#define	ZC_WRONG_DOC_TYPE	2	/* top-level XML doc element != zone */
#define	ZC_BAD_PROPERTY		3	/* libxml-level property problem */
#define	ZC_TEMP_FILE		4	/* problem creating temporary file */
#define	ZC_SAVING_FILE		5	/* libxml error saving or validating */
#define	ZC_NO_ENTRY		6	/* no such entry */
#define	ZC_BOGUS_ZONE_NAME	7	/* illegal zone name */
#define	ZC_REQD_RESOURCE_MISSING	8	/* required resource missing */
#define	ZC_REQD_PROPERTY_MISSING	9	/* required property missing */
#define	ZC_BAD_HANDLE		10	/* bad document handle */
#define	ZC_NOMEM		11	/* out of memory (like ENOMEM) */
#define	ZC_INVAL		12	/* invalid argument (like EINVAL) */
#define	ZC_ACCES		13	/* permission denied (like EACCES) */
#define	ZC_TOO_BIG		14	/* string won't fit in char array */
#define	ZC_MISC_FS		15	/* miscellaneous file-system error */
#define	ZC_NO_ZONE		16	/* no such zone */
#define	ZC_NO_RESOURCE_TYPE	17	/* no/wrong resource type */
#define	ZC_NO_RESOURCE_ID	18	/* no/wrong resource id */
#define	ZC_NO_PROPERTY_TYPE	19	/* no/wrong property type */
#define	ZC_NO_PROPERTY_ID	20	/* no/wrong property id */
#define	ZC_BAD_ZONE_STATE	21	/* zone state invalid for given task */
#define	ZC_INVALID_DOCUMENT	22	/* libxml can't validate against DTD */
#define	ZC_NAME_IN_USE		23	/* zone name already in use (rename) */
#define	ZC_NO_SUCH_ID		24	/* delete_index: no old ID */
#define	ZC_UPDATING_INDEX	25	/* add/modify/delete_index problem */
#define	ZC_LOCKING_FILE		26	/* problem locking index file */
#define	ZC_UNLOCKING_FILE	27	/* problem unlocking index file */
#define	ZC_SYSTEM		28	/* consult errno instead */
#define	ZC_INSUFFICIENT_SPEC	29	/* resource insufficiently specified */
#define	ZC_RESOLVED_PATH	34	/* resolved path mismatch */
#define	ZC_IPV6_ADDR_PREFIX_LEN	35	/* IPv6 address prefix length needed */
#define	ZC_BOGUS_ADDRESS	36	/* not IPv[4|6] address or host name */
#define	ZC_PRIV_PROHIBITED	37	/* specified privilege is prohibited */
#define	ZC_PRIV_REQUIRED	38	/* required privilege is missing */
#define	ZC_PRIV_UNKNOWN		39	/* specified privilege is unknown */
#define	ZC_BRAND_ERROR		40	/* brand-specific error */
#define	ZC_INCOMPATIBLE		41	/* incompatible settings */
#define	ZC_ALIAS_DISALLOW	42	/* rctl alias disallowed */
#define	ZC_CLEAR_DISALLOW	43	/* clear property disallowed */
#define	ZC_POOL			44	/* generic libpool error */
#define	ZC_POOLS_NOT_ACTIVE	45	/* pool service not enabled */
#define	ZC_POOL_ENABLE		46	/* pools enable failed */
#define	ZC_NO_POOL		47	/* no such pool configured */
#define	ZC_POOL_CREATE		48	/* pool create failed */
#define	ZC_POOL_BIND		49	/* pool bind failed */
#define	ZC_NODE_DOWN		50	/* cluster node is currently down */
#define	ZC_REMOTE_EXEC_ERR	51	/* remote execution infra is down */
#define	ZC_NO_CLUSTER		52	/* node not in cluster */
#define	ZC_CCR_ERROR		53	/* Error in call to the CCR */
#define	ZC_NAMESERVER_ERR	54	/* Error in call to the nameserver */
#define	ZC_BAD_CLUSTER_ID	55	/* clconf returned a bad cluster ID */
#define	ZC_MEMBERSHIP_ERROR	56	/* Error in call to the membership */
#define	ZC_VERSION_ERR		57	/* Wrong Solaris Cluster Version */
#define	ZC_NO_SUBNET		58	/* No free subnet is available */
#define	ZC_MISSING_CONFIG	59	/* Missing clzone_config table */


#define	CZNET_CCR_TABLE		"cz_network_ccr"

#define	LIBZCCFG_PATH		"libzccfg.so.1"

/* Path where zone cluster template files are located */
#define	ZC_CONFIG_ROOT		"/etc/cluster/zone_cluster"

#define	ZC_HELPER		"/usr/cluster/lib/sc/zc_helper"

/*
 * Shortened alias names for the zones rctls.
 */
#define	ALIAS_MAXLWPS		"max-lwps"
#define	ALIAS_MAXSHMMEM		"max-shm-memory"
#define	ALIAS_MAXSHMIDS		"max-shm-ids"
#define	ALIAS_MAXMSGIDS		"max-msg-ids"
#define	ALIAS_MAXSEMIDS		"max-sem-ids"
#define	ALIAS_MAXLOCKEDMEM	"locked"
#define	ALIAS_MAXSWAP		"swap"
#define	ALIAS_SHARES		"cpu-shares"
#define	ALIAS_CPUCAP		"cpu-cap"

/*
 * The state of a zone cluster is defined as highest state of any of its
 * constituent zones. For example, if the zone state on node 1 is
 * CONFIGURED and that on node 2 is INSTALLED, then the state of the zone
 * cluster itself is INSTALLED. The states here mirror those in libzonecfg.h of
 * Solaris, but note that the ordering here is different. The RUNNING state
 * must always be the highest state. The UNKNOWN state is new here. It is used
 * when we can't determine the state of a zone cluster member on any physical
 * node for some reason (e.g., COMM_FAILURE, the node is down, etc.).
 */
typedef enum {
	ZC_STATE_UNKNOWN = 0,
	ZC_STATE_CONFIGURED,
	ZC_STATE_INCOMPLETE,
	ZC_STATE_INSTALLED,
	ZC_STATE_DOWN,
	ZC_STATE_MOUNTED,
	ZC_STATE_READY,
	ZC_STATE_SHUTTING_DOWN,
	ZC_STATE_RUNNING
} zone_cluster_state_t;

typedef struct zc_dochandle *zc_dochandle_t;	/* opaque handle */

typedef struct zc_fsopt {
	struct zc_fsopt	*zc_fsopt_next;
	char	zc_fsopt_opt[MAX_MNTOPT_STR];
} zc_fsopt_t;

struct zc_fstab {
	char		zc_fs_special[MAXPATHLEN]; 	/* special file */
	char		zc_fs_dir[MAXPATHLEN];		/* mount point */
	char		zc_fs_type[FSTYPSZ];		/* e.g. ufs */
	zc_fsopt_t	*zc_fs_options;			/* mount options */
	char		zc_fs_raw[MAXPATHLEN];		/* device to fsck */
};

/* Data structure for the network resource. */
struct zc_nwiftab {
	char		zc_nwif_address[INET6_ADDRSTRLEN];
	char		zc_nwif_physical[LIFNAMSIZ];
	char		zc_nwif_defrouter[INET6_ADDRSTRLEN];
};

struct zc_devtab {
	char	zc_dev_match[MAXPATHLEN];
};

struct zc_rctlvaltab {
	char	zc_rctlval_priv[MAXNAMELEN];
	char	zc_rctlval_limit[MAXNAMELEN];
	char	zc_rctlval_action[MAXNAMELEN];
	struct zc_rctlvaltab *zc_rctlval_next;
};

struct zc_rctltab {
	char	zc_rctl_name[MAXNAMELEN];
	struct zc_rctlvaltab *zc_rctl_valp;
};

struct zc_attrtab {
	char	zc_attr_name[MAXNAMELEN];
	char	zc_attr_type[MAXNAMELEN];
	char	zc_attr_value[2 * BUFSIZ];
};

struct zc_dstab {
	char	zc_dataset_name[MAXNAMELEN];
};

struct zc_psettab {
	char	zc_ncpu_min[MAXNAMELEN];
	char	zc_ncpu_max[MAXNAMELEN];
	char	zc_importance[MAXNAMELEN];
};

struct zc_mcaptab {
	char	zc_physmem_cap[MAXNAMELEN];
};

typedef enum zc_iptype {
	CZS_SHARED,
	CZS_EXCLUSIVE
} zc_iptype_t;

typedef struct zc_fselem {
	struct zc_fstab elem;
	struct zc_fselem *next;
} zc_fselem_t;

typedef struct zc_nwifelem {
	struct zc_nwiftab elem;
	struct zc_nwifelem *next;
} zc_nwifelem_t;

typedef struct zc_develem {
	struct zc_devtab elem;
	struct zc_develem *next;
} zc_develem_t;

typedef struct zc_dselem {
	struct zc_dstab elem;
	struct zc_dselem *next;
} zc_dselem_t;

typedef struct zc_rctlelem {
	struct zc_rctltab elem;
	struct zc_rctlelem *next;
} zc_rctlelem_t;

/*
 * A "node" resource consists of the following:
 *
 * 1. The physical cluster node that will host the zone cluster.
 * 2. The zone hostname on that physical node.
 * 3. Zero or more file system resources.
 * 4. Zero or more network resources.
 * 5. Zero or more device resources.
 * 6. Zero or more ZFS dataset resources.
 * 7. Zero or more resource control resources.
 */
struct zc_nodetab {
	char		zc_nodename[SYS_NMLN];	/* physical node name */
	char		zc_hostname[SYS_NMLN];	/* zone hostname */
	zc_fselem_t	*zc_fs_listp;
	zc_nwifelem_t	*zc_nwif_listp;
	zc_develem_t	*zc_dev_listp;
	zc_dselem_t	*zc_ds_listp;
	zc_rctlelem_t	*zc_rctl_listp;
};

/*
 * Structure to hold the sysidcfg(4) parameters.
 *
 * Note that zone_root_password, unlike the other structure members, is a
 * variable-length string. This is because the encrypted password, as can be
 * seen in /etc/shadow for example, can be any length. That is, we can't make
 * assumptions about the "maximum length" of the encrypted root password.
 *
 * TODO: Re-examine the array sizes of the other members too. For example,
 * security_policy entry in sysidcfg(4) "can" be "very" long.
 */
struct zc_sysidtab {
	char	*zc_root_password;
	char	zc_name_service[MAXNAMELEN];
	char	zc_nfs4_domain[MAXNAMELEN];
	char	zc_sec_policy[MAXNAMELEN];
	char	zc_sys_locale[MAXNAMELEN];
	char	zc_terminal[MAXNAMELEN];
	char	zc_timezone[MAXNAMELEN];
};

/*
 * Interface for remote command execution.
 */
extern	int	zccfg_execute_cmd(nodeid_t, const char *);

/*
 * Interface to obtain the state of zone member on a given node.
 */
extern	int	get_zone_state_on_node(const char *, nodeid_t,
    zone_cluster_state_t *);

/*
 * Interface to get the state of the entire zone cluster.
 */
extern int	get_zone_cluster_state(const char *, zone_cluster_state_t *);

/*
 * Given a zone cluster state number, return a string description of the state.
 */
extern	const	char *zone_cluster_state_str(zone_cluster_state_t);

/*
 * Interface to get the membership state of a zone in a zone cluster.
 */
extern int	get_zone_cluster_state_on_node(const char *, nodeid_t,
	char **status_str);

/*
 * Interfaces for command logging.
 */
extern int	zccfg_add_cmd_log_entry(const char *, const char *,
    const char *);
extern int	zccfg_delete_cmd_log_entry(const char *, const char *);
extern char	*zccfg_get_cmd_log_entry(const char *, const char *);

/*
 * Interface to validate our brand definition file against the brand DTD
 * by Solaris.
 */
extern	int	zccfg_validate_brand_definition(void);

/*
 * Basic configuration management routines.
 */
extern	zc_dochandle_t	zccfg_init_handle(void);
extern	int	zccfg_get_handle(const char *, zc_dochandle_t);
extern	int	zccfg_get_template_handle(const char *, const char *,
    zc_dochandle_t);
extern	int	zccfg_check_handle(const zc_dochandle_t);
extern	void	zccfg_fini_handle(zc_dochandle_t);
extern	int	zccfg_destroy(const char *, bool);
extern	int	zccfg_save(zc_dochandle_t);
extern	char	*zccfg_strerror(int);
extern	int	zccfg_del_all_resources(zc_dochandle_t, const char *);
extern	int	zccfg_num_resources(zc_dochandle_t, const char *);
extern	bool	zccfg_valid_ncpus(char *, char *);
extern	bool	zccfg_valid_importance(char *);
extern	bool	zccfg_valid_memlimit(char *, uint64_t *);
extern	bool	zccfg_valid_alias_limit(char *, char *, uint64_t *);

/*
 * Zone cluster name, path to zone directory, autoboot setting, pool, boot
 * arguments, private net, scheduling class, and IP type.
 */
extern	int	zccfg_validate_zonename(const char *);
extern	int	zccfg_get_name(zc_dochandle_t, char *, size_t);
extern	int	zccfg_set_name(zc_dochandle_t, const char *);
extern	int	zccfg_get_zonepath(zc_dochandle_t, char *, size_t);
extern	int	zccfg_set_zonepath(zc_dochandle_t, const char *);
extern	int	zccfg_get_autoboot(zc_dochandle_t, bool *);
extern	int	zccfg_set_autoboot(zc_dochandle_t, bool);
extern	int	zccfg_get_pool(zc_dochandle_t, char *, size_t);
extern	int	zccfg_set_pool(zc_dochandle_t, const char *);
extern	int	zccfg_get_bootargs(zc_dochandle_t, char *, size_t);
extern	int	zccfg_set_bootargs(zc_dochandle_t, const char *);
extern	int	zccfg_get_clprivnet(zc_dochandle_t, bool *);
extern	int	zccfg_set_clprivnet(zc_dochandle_t, bool);
extern	int	zccfg_get_sched_class(zc_dochandle_t, char *, size_t);
extern	int	zccfg_set_sched(zc_dochandle_t, const char *);
extern	int	zccfg_get_iptype(zc_dochandle_t, zc_iptype_t *);
extern	int	zccfg_set_iptype(zc_dochandle_t, zc_iptype_t);

/*
 * Set/retrieve the brand for the zone
 */
extern	int	zccfg_get_brand(zc_dochandle_t, char *, size_t);
extern	int	zccfg_set_brand(zc_dochandle_t, const char *);

/*
 * Filesystem configuration.
 */
extern	int	zccfg_add_filesystem(zc_dochandle_t, struct zc_fstab *);
extern	int	zccfg_delete_filesystem(zc_dochandle_t, struct zc_fstab *);
extern	int	zccfg_modify_filesystem(zc_dochandle_t, struct zc_fstab *,
    struct zc_fstab *);
extern	int	zccfg_lookup_filesystem(zc_dochandle_t, struct zc_fstab *);

/* "ipd" is short for "inherit-pkg-dir" */
extern	int	zccfg_add_ipd(zc_dochandle_t, struct zc_fstab *);
extern	int	zccfg_delete_ipd(zc_dochandle_t, struct zc_fstab *);
extern	int	zccfg_modify_ipd(zc_dochandle_t, struct zc_fstab *,
    struct zc_fstab *);
extern	int	zccfg_lookup_ipd(zc_dochandle_t, struct zc_fstab *);
extern	int	zccfg_add_fs_option(struct zc_fstab *, const char *);
extern	int	zccfg_remove_fs_option(struct zc_fstab *, const char *);
extern	void	zccfg_free_fs_option_list(zc_fsopt_t *);

/*
 * Network interface configuration.
 */
extern	int	zccfg_add_nwif(zc_dochandle_t, struct zc_nwiftab *);
extern	int	zccfg_delete_nwif(zc_dochandle_t, struct zc_nwiftab *);
extern	int	zccfg_modify_nwif(zc_dochandle_t, struct zc_nwiftab *,
    struct zc_nwiftab *);
extern	int	zccfg_lookup_nwif(zc_dochandle_t, struct zc_nwiftab *);

/*
 * Device configuration and rule matching.
 */
extern	int	zccfg_add_dev(zc_dochandle_t, struct zc_devtab *);
extern	int	zccfg_delete_dev(zc_dochandle_t, struct zc_devtab *);
extern	int	zccfg_modify_dev(zc_dochandle_t, struct zc_devtab *,
    struct zc_devtab *);
extern	int	zccfg_lookup_dev(zc_dochandle_t, struct zc_devtab *);

/*
 * Resource control configuration.
 */
extern	int	zccfg_add_rctl(zc_dochandle_t, struct zc_rctltab *);
extern	int	zccfg_delete_rctl(zc_dochandle_t, struct zc_rctltab *);
extern	int	zccfg_modify_rctl(zc_dochandle_t, struct zc_rctltab *,
    struct zc_rctltab *);
extern	int	zccfg_lookup_rctl(zc_dochandle_t, struct zc_rctltab *);
extern	int	zccfg_add_rctl_value(struct zc_rctltab *,
    struct zc_rctlvaltab *);
extern	int	zccfg_remove_rctl_value(struct zc_rctltab *,
    struct zc_rctlvaltab *);
extern	void	zccfg_free_rctl_value_list(struct zc_rctlvaltab *);
extern	bool	zccfg_aliased_rctl_ok(zc_dochandle_t, char *);
extern	int	zccfg_set_aliased_rctl(zc_dochandle_t, char *,
    uint64_t);
extern	int	zccfg_get_aliased_rctl(zc_dochandle_t, char *, uint64_t *);
extern	int	zccfg_rm_aliased_rctl(zc_dochandle_t, char *);

/*
 * Generic attribute configuration and type/value extraction.
 */
extern	int	zccfg_add_attr(zc_dochandle_t, struct zc_attrtab *);
extern	int	zccfg_delete_attr(zc_dochandle_t, struct zc_attrtab *);
extern	int	zccfg_modify_attr(zc_dochandle_t, struct zc_attrtab *,
    struct zc_attrtab *);
extern	int	zccfg_lookup_attr(zc_dochandle_t, struct zc_attrtab *);
extern	int	zccfg_get_attr_boolean(const struct zc_attrtab *, bool *);
extern	int	zccfg_get_attr_int(const struct zc_attrtab *, int64_t *);
extern	int	zccfg_get_attr_string(const struct zc_attrtab *, char *,
    size_t);
extern	int	zccfg_get_attr_uint(const struct zc_attrtab *, uint64_t *);

/*
 * ZFS configuration.
 */
extern	int	zccfg_add_ds(zc_dochandle_t, struct zc_dstab *);
extern	int	zccfg_delete_ds(zc_dochandle_t, struct zc_dstab *);
extern	int	zccfg_modify_ds(zc_dochandle_t, struct zc_dstab *,
    struct zc_dstab *);
extern	int	zccfg_lookup_ds(zc_dochandle_t, struct zc_dstab *);

/*
 * '*ent' iterator routines.
 *
 * The *set*ent functions ensure that we're at the root of the configuration
 * tree (the XML tree's 'cur' pointer points to the 'children' node).
 *
 * The *get*ent functions "get" the resource/property specified by the second
 * argument.
 *
 * The *end*ent functions "rewind" the XML tree's 'cur' pointer to the root
 * node.
 */
extern	int	zccfg_setfsent(zc_dochandle_t);
extern	int	zccfg_getfsent(zc_dochandle_t, struct zc_fstab *);
extern	int	zccfg_endfsent(zc_dochandle_t);
extern	int	zccfg_setipdent(zc_dochandle_t);
extern	int	zccfg_getipdent(zc_dochandle_t, struct zc_fstab *);
extern	int	zccfg_endipdent(zc_dochandle_t);
extern	int	zccfg_setnwifent(zc_dochandle_t);
extern	int	zccfg_getnwifent(zc_dochandle_t, struct zc_nwiftab *);
extern	int	zccfg_endnwifent(zc_dochandle_t);
extern	int	zccfg_setdevent(zc_dochandle_t);
extern	int	zccfg_getdevent(zc_dochandle_t, struct zc_devtab *);
extern	int	zccfg_enddevent(zc_dochandle_t);
extern	int	zccfg_setattrent(zc_dochandle_t);
extern	int	zccfg_getattrent(zc_dochandle_t, struct zc_attrtab *);
extern	int	zccfg_endattrent(zc_dochandle_t);
extern	int	zccfg_setrctlent(zc_dochandle_t);
extern	int	zccfg_getrctlent(zc_dochandle_t, struct zc_rctltab *);
extern	int	zccfg_endrctlent(zc_dochandle_t);
extern	int	zccfg_setdsent(zc_dochandle_t);
extern	int	zccfg_getdsent(zc_dochandle_t, struct zc_dstab *);
extern	int	zccfg_enddsent(zc_dochandle_t);
extern	int	zccfg_setnodeent(zc_dochandle_t);
extern	int	zccfg_getnodeent(zc_dochandle_t, struct zc_nodetab *);
extern	int	zccfg_endnodeent(zc_dochandle_t);
extern	int	zccfg_setsysident(zc_dochandle_t);
extern	int	zccfg_getsysident(zc_dochandle_t, struct zc_sysidtab *);
extern	int	zccfg_endsysident(zc_dochandle_t);
extern	int	zccfg_getpsetent(zc_dochandle_t, struct zc_psettab *);
extern	int	zccfg_getmcapent(zc_dochandle_t, struct zc_mcaptab *);

/*
 * Privilege-related functions.
 */
extern	int	zccfg_get_limitpriv(zc_dochandle_t, char **);
extern	int	zccfg_set_limitpriv(zc_dochandle_t, const char *);

/*
 * Node-related functions.
 */
extern	int	zccfg_add_node(zc_dochandle_t, struct zc_nodetab *);
extern	int	zccfg_delete_node(zc_dochandle_t, struct zc_nodetab *);
extern	int	zccfg_modify_node(zc_dochandle_t, struct zc_nodetab *,
    struct zc_nodetab *);
extern	int	zccfg_lookup_node(zc_dochandle_t, struct zc_nodetab *);
extern	void	zccfg_free_nwif_list(zc_nwifelem_t *);

/*
 * Sysid related functions.
 */
extern	int	zccfg_add_sysid(zc_dochandle_t, struct zc_sysidtab *);
extern	int	zccfg_delete_sysid(zc_dochandle_t, struct zc_sysidtab *);
extern	int	zccfg_modify_sysid(zc_dochandle_t, struct zc_sysidtab *,
    struct zc_sysidtab *);
extern	int	zccfg_lookup_sysid(zc_dochandle_t, struct zc_sysidtab *);

/*
 * cpu-set configuration.
 */
extern	int	zccfg_add_pset(zc_dochandle_t, struct zc_psettab *);
extern	int	zccfg_delete_pset(zc_dochandle_t);
extern	int	zccfg_modify_pset(zc_dochandle_t, struct zc_psettab *);
extern	int	zccfg_lookup_pset(zc_dochandle_t, struct zc_psettab *);

/*
 * mem-cap configuration.
 */
extern	int	zccfg_delete_mcap(zc_dochandle_t);
extern	int	zccfg_modify_mcap(zc_dochandle_t, struct zc_mcaptab *);
extern	int	zccfg_lookup_mcap(zc_dochandle_t, struct zc_mcaptab *);

/*
 * Network-related convenience functions.
 */
extern bool zccfg_same_net_address(char *, char *);
extern int zccfg_process_clprivnet(char *, char **);
extern int zccfg_write_subnet_to_netccr(char *, char *);
extern int zccfg_remove_netccr_entry(char *);
extern int zccfg_modify_subnet(void);
extern int zccfg_set_clusternetmask(char *);

/*
 * Rctl-related convenience functions.
 */
extern bool zccfg_is_rctl(const char *);
extern bool zccfg_valid_rctlname(const char *);
extern bool zccfg_valid_rctlblk(const rctlblk_t *);
extern bool zccfg_valid_rctl(const char *, const rctlblk_t *);
extern int  zccfg_construct_rctlblk(const struct zc_rctlvaltab *, rctlblk_t *);

/*
 * Function to query if Solaris version supports a given zone attribute
 */
extern bool zccfg_attr_exists_in_zones_dtd(const char *);

#ifdef __cplusplus
}
#endif

#endif	/* _LIBZCCFG_H */
