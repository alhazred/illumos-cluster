#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.6	08/08/07 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# lib/libpnmproxy/Makefile.com
#

LIBRARYCCC= libclpnmproxy.a
VERS= .1

OBJECTS = libpnmproxy.o

include $(SRC)/common/cl/Makefile.files

# include library definitions, do not change order of include and DYNLIB
include ../../Makefile.lib

SRCS =		$(OBJECTS:%.o=../common/%.cc)
LINTFILES =	$(OBJECTS:%.o=%.ln)
LINTS_DIR = .

MTFLAG		= -mt

# 4703310: Should use /usr/lib/lwp/libthread for all binaries with Sol_8
$(PRE_S9_BUILD)LDLIBS += -R/usr/lib/lwp

CPPFLAGS	+= $(CL_CPPFLAGS) $(XREGSFLAG)
CPPFLAGS	+= -I$(SRC)/common/cl -I$(SRC)/common/cl/sys
CPPFLAGS	+= -I$(SRC)/common/cl/interfaces/$(CLASS)
CPPFLAGS	+= -I$(SRC)/head
CPPFLAGS	+= -I$(SRC)

LDLIBS += -lc -lclcomm -lclos -lclconf

lint:= XREGSFLAG=

LIBS = $(DYNLIBCCC) $(LIBRARYCCC)

PMAP =

OBJS_DIR = objs

.PARALLEL: $(OBJECTS) $(LINTFILES)
.KEEP_STATE:

all: $(LIBS)

# include library targets
include ../../Makefile.targ
include $(SRC)/common/cl/Makefile.rules

objs/%.o pics/%.o: ../common/%.cc
	$(COMPILE.cc) -o $@ $<
