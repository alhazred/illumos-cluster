/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)libpnmproxy.cc	1.8	08/10/01 SMI"

#include <sys/sol_version.h>
#include <pnm/pnm_ld.h>

#include <zone.h>
#include <sys/varargs.h>
#include <h/pnm_mod.h>
#include <sys/vc_int.h>
#include <sys/sc_syslog_msg.h>
#include <orb/infrastructure/orb.h>
#include <nslib/ns.h>

#define	PNM_SERVER "pnm_server"

// Message handles for the sc_syslog facility
sc_syslog_msg_handle_t  msg_libpnmproxy;

extern "C" {

//
// This is used for all the system errors. The error will be written to the
// syslog as an error message. errno will be intrepreted in the error message.
// This will generate a single message id and so we have to write only one
// message explanation. However, this is not such a big problem because a
// system error will occur for the following types of errors:
// out of memory, can't open a file descriptor, gethostbyname failed etc.
// These types of errors can be classified into one type and have the same
// type of user action. The user action can be specified to be different also
// in the explanation.
//
void
log_syserr_lib(char *fmt, ...)
{
	static char	bufp[LINELEN];
	va_list		ap; // CSTYLED
	int		err = errno;	//lint !e746

	// CSTYLED
	va_start(ap, fmt); //lint !e40 !e26 !e50 !e10
	(void) vsnprintf(bufp, LINELEN, fmt, ap);
	va_end(ap);

	if (err != 0) {
		(void) strcat(bufp, ": ");
		(void) strcat(bufp, (char *)strerror(err));
	}
	//
	// Initialize sc_syslog handle. This calls will invoke
	// openlog(), and we shouldn't be doing openlog() ourselves.
	//
	(void) sc_syslog_msg_initialize(&msg_libpnmproxy, SC_SYSLOG_PNM_TAG,
	    "");
	//
	// SCMSGS
	// @explanation
	// A system error has occured in libpnmproxy. This could be because
	// of the resources on the system being very low, such as low memory.
	// @user_action
	// The user of libpnmproxy should handle these errors. However, if the
	// message is out of memory, increase the swap space, install more
	// memory, or reduce peak memory consumption. Otherwise the error is
	// unrecoverable, and the node needs to be rebooted.
	//
	(void) sc_syslog_msg_log(msg_libpnmproxy, SC_SYSLOG_ERROR, MESSAGE,
	    "libpnm  proxy system error: %s", bufp);
	sc_syslog_msg_done(&msg_libpnmproxy);
} //lint !e818


//
// Frees the pnm_adp_addrs_t structure.
// INPUT: pointer to pnm_adp_addrs_t struct.
// RETURN: void.
//
void
free_pnm_adp_addrs(const pnm_adp_addrs_t *pat)
{
	if (pat->adp_ipmask) {
		free(pat->adp_ipmask);
	}
	if (pat->adp_ipnet) {
		free(pat->adp_ipnet);
	}
	if (pat->adp_ipaddr) {
		free(pat->adp_ipaddr);
	}
}

//
// Free a pnm_adapterinfo_t structure and all its next pointers.
// The argument pointer adp is also freed by this function.
// INPUT: pointer to a pnm_adapterinfo_t struct
// RETURN: void.
//
void
pnm_adapterinfo_free(pnm_adapterinfo_t *adp)
{
	pnm_adapterinfo_t	*ip;
	pnm_adapterinfo_t	*ip_next;

	for (ip = adp; ip; ip = ip_next) {
		ip_next = ip->next;
		free(ip->adp_name);
		free(ip->adp_group);
		free_pnm_adp_addrs(&(ip->adp_addrs));
		free(ip);
	}
}

//
// Generate the string to identify the PNM proxy given the hostname
// and the zonename.
// Return value:
// Success:
// Return the PNM proxy server name. It also returns a boolean value
// of true if we need to communicate with a PNM proxy for a zone cluster
// and false otherwise.
// Failure:
// A NULL is returned for the proxy server name.
//
char *
get_proxy_name(char *hostnamep, char *zonenamep, bool &is_zc_from_global)
{
	uint_t			nid;
	uint_t			node = NODEID_UNKNOWN;
	uint32_t		clid;
	int			ret;
	zoneid_t		zid = getzoneid();

	char			*nm_pnm_proxy = new char[ZONENAME_MAX + 16];
	if (nm_pnm_proxy == NULL) {
		log_syserr_lib("PNM proxy: Out of memory, size = %d",
		    ZONENAME_MAX + 16);
		return (NULL);
	}
	if ((zonenamep != NULL) && (strcmp(zonenamep, "global") != 0) &&
	    (strcmp(hostnamep, "localhost") != 0)) {
		ret = clconf_get_cluster_id(zonenamep, &clid);
		if ((ret != 0) && (ret != ENOENT)) {
			// clconf error
			return (NULL);
		}
		if ((ret == 0) && (clid >= MIN_CLUSTER_ID)) {
			//
			// The hostname passed is the hostname of a
			// zone cluster zone. Since we need to
			// get the nodeid, we need to lookup in the clconf
			// context of the zone cluster.
			//
			node = clconf_zc_get_nodeid_by_nodename(zonenamep,
			    hostnamep);
			os::sprintf(nm_pnm_proxy, "%s.%d.%s", PNM_SERVER,
			    node, "global");
			is_zc_from_global = true;
			return (nm_pnm_proxy);
		}
	}
	is_zc_from_global = false;
	if ((hostnamep == NULL) || os::strcmp(hostnamep, "localhost") == 0) {
		node = clconf_get_local_nodeid();
	} else {
		node = clconf_cluster_get_nodeid_by_nodename(hostnamep);
	}
	os::sprintf(nm_pnm_proxy, "%s.%d.%s", PNM_SERVER, node, zonenamep);
	return (nm_pnm_proxy);
}

//
// PNM proxy Interfaces.
//

//
// Check if we are being invoked within a zone cluster.
// Return Value:
//	0 If invoked from global zone or native zone.
//	1 If invoked from a zone cluster.
//	-1 on failure.
//
int
is_zone_cluster()
{
	char		zone_name[ZONENAME_MAX];
	zoneid_t	zid = getzoneid();
	int		retval;
	uint_t		clid;

	// Global zone.
	if (zid == 0) {
		return (0);
	}

	if (getzonenamebyid(zid, zone_name, sizeof (zone_name)) < 0) {
		log_syserr_lib("Failed to get zone name for zone with"
		    " zone ID %d.", zid);
		return (-1);
	}
	if ((retval = clconf_get_cluster_id(zone_name, &clid)) != 0) {
		//
		// We can only get a ENOENT here if the zone does not
		// belong to the zone cluster.
		//
		return (0);
	}
	if (clid >= MIN_CLUSTER_ID) {
		// zone cluster
		return (1);
	} else {
		return (0);
	}
}

//
// Proxy interface for the pnm_group_list interface.
// Return value:
// If the hostname is null or does not correspond to a cluster node,
// return PNM_EPROG. If the PNM proxy service daemon is down,
// return PNM_ESDOWN. Else, the return value is what the pnm_group_list
// interface will return.
//
int
proxy_pnm_group_list(char *hostnamep, char *zonenamep, pnm_group_list_t *listp)
{
	CORBA::Object_var		obj_v;
	pnm_mod::pnm_server_var		pnm_server_v;
	char				*grp_listp;
	naming::naming_context_var	ctx_v = ns::root_nameserver();
	char 				*nm_pnm_proxy;
	bool				is_zc_from_global;
	Environment	env;
	int		result;

	nm_pnm_proxy = get_proxy_name(hostnamep, zonenamep, is_zc_from_global);
	if (nm_pnm_proxy == NULL) {
		log_syserr_lib("PNM proxy:Invalid hostname %s", hostnamep);
		return (PNM_EPROG);
	}
	obj_v = ctx_v->resolve(nm_pnm_proxy, env);
	if (env.exception()) {
		log_syserr_lib("Failed to resolve pnm proxy %s",
		    nm_pnm_proxy);
		env.clear();
		delete [] nm_pnm_proxy;
		return (PNM_EPROG);
	}
	pnm_server_v = pnm_mod::pnm_server::_narrow(obj_v);
	ASSERT(!CORBA::is_nil(pnm_server_v));
	//
	// If we are dealing with zone cluster, the proxy runs in the
	// global zone. The proxy needs the zonename when the client
	// is not executing in the zone cluster context.
	//
	if (is_zc_from_global) {
		result = pnm_server_v->pnm_group_list_zc(zonenamep,
		    grp_listp, env);
	} else {
		result = pnm_server_v->pnm_group_list(grp_listp, env);
	}
	if (env.exception() != NULL) {
		if ((CORBA::COMM_FAILURE::_exnarrow(env.exception()) != NULL) ||
		    (CORBA::INV_OBJREF::_exnarrow(env.exception()) != NULL)) {
			//
			// Invocation to the server returned an exception. The
			// mostly likely reason is that the server is down.
			// We will return the appropriate error code back to
			// the client.
			//
			env.clear();
			delete [] nm_pnm_proxy;
			return (PNM_ESDOWN);
		} else {
			// Error, unknown exception.
			result = PNM_EPROG;
			ASSERT(0);
		}
	}
	delete [] nm_pnm_proxy;
	if (result != 0) {
		return (result);
	}
	*listp = grp_listp;
	return (result);
}

//
// Proxy interface for the pnm_group_status interface.
// Return value:
// If the hostname is null or does not correspond to a cluster node,
// return PNM_EPROG. If the PNM proxy service daemon is down,
// return PNM_ESDOWN. Else, the return value is what the pnm_group_status
// interface will return.
//
int
proxy_pnm_group_status(char *hostnamep, char *zonenamep,
    pnm_group_t *group, pnm_status_t *statusp)
{
	CORBA::Object_var		obj_v;
	pnm_mod::pnm_server_var		pnm_server_v;
	naming::naming_context_var ctx_v = ns::root_nameserver();
	nodeid_t	node;
	char		*nm_pnm_proxy;
	bool		is_zc_from_global;
	Environment	env;
	int		result;

	if (hostnamep == NULL) {
		return (PNM_EPROG);
	}
	nm_pnm_proxy = get_proxy_name(hostnamep, zonenamep, is_zc_from_global);
	if (nm_pnm_proxy == NULL) {
		log_syserr_lib("Invalid hostname %s\n", hostnamep);
		return (PNM_EPROG);
	}
	obj_v = ctx_v->resolve(nm_pnm_proxy, env);
	if (env.exception()) {
		log_syserr_lib("Failed to resolve pnm proxy %s\n",
		    nm_pnm_proxy);
		env.clear();
		delete [] nm_pnm_proxy;
		return (PNM_EPROG);
	}
	pnm_server_v = pnm_mod::pnm_server::_narrow(obj_v);
	ASSERT(!CORBA::is_nil(pnm_server_v));
	pnm_mod::pnm_status *status;
	//
	// If we are dealing with zone cluster, the proxy runs in the
	// global zone. The proxy needs the zonename when the client
	// is not executing in the zone cluster context.
	//
	if (is_zc_from_global) {
		result = pnm_server_v->pnm_group_status_zc(zonenamep,
		    (const char *)group, status, env);
	} else {
		result = pnm_server_v->pnm_group_status((const char *)group,
		    status, env);
	}
	if (env.exception() != NULL) {
		if ((CORBA::COMM_FAILURE::_exnarrow(env.exception()) != NULL) ||
		    (CORBA::INV_OBJREF::_exnarrow(env.exception()) != NULL)) {
			//
			// Invocation to the server returned an exception. The
			// mostly likely reason is that the server is down.
			// We will return the appropriate error code back to
			// the client.
			//
			env.clear();
			delete [] nm_pnm_proxy;
			return (PNM_ESDOWN);
		} else {
			// Error, unknown exception.
			result = PNM_EPROG;
			ASSERT(0);
		}
	}
	delete [] nm_pnm_proxy;
	statusp->status = status->status;
	statusp->backups = NULL;

	if (result != 0) {
		return (result);
	}
	statusp->backups = status->backups;
	return (0);
} //lint !e818

//
// Proxy interface for the pnm_adapterinfo_list interface.
// Return value:
// If the hostname is null or does not correspond to a cluster node,
// return PNM_EPROG. If the PNM proxy service daemon is down,
// return PNM_ESDOWN. Else, the return value is what the pnm_adapterinfo_list
// interface will return.
//
int
proxy_pnm_adapterinfo_list(const char *hostnamep, char *zonenamep,
	pnm_adapterinfo_t **adpp, bool alladdrs)
{
	CORBA::Object_var		obj_v;
	pnm_mod::pnm_server_var		pnm_server_v;
	pnm_adapterinfo_t		*adp = NULL;
	pnm_adapterinfo_t		*head_adp = NULL;
	pnm_adapterinfo_t		*last_adp = NULL;
	naming::naming_context_var	ctx_v = ns::root_nameserver();
	pnm_mod::AdpinfoSeq		*adp_listp;
	bool				is_zc_from_global;
	char				*nm_pnm_proxy;
	Environment	env;
	int		result;
	uint_t		indx;
	uint_t		addr_indx;
	unsigned long long	num_addrs;

	if (hostnamep == NULL) {
		return (PNM_EPROG);
	}
	nm_pnm_proxy = get_proxy_name((char *)hostnamep, zonenamep,
	    is_zc_from_global);
	if (nm_pnm_proxy == NULL) {
		log_syserr_lib("Invalid hostname %s\n", hostnamep);
		return (PNM_EPROG);
	}
	obj_v = ctx_v->resolve(nm_pnm_proxy, env);
	if (env.exception() != NULL) {
		log_syserr_lib("Failed to resolve pnm proxy %s\n",
		    nm_pnm_proxy);
		env.clear();
		delete [] nm_pnm_proxy;
		return (PNM_EPROG);
	}
	pnm_server_v = pnm_mod::pnm_server::_narrow(obj_v);
	ASSERT(!CORBA::is_nil(pnm_server_v));
	//
	// If we are dealing with zone cluster, the proxy runs in the
	// global zone. The proxy needs the zonename when the client
	// is not executing in the zone cluster context.
	//
	if (is_zc_from_global) {
		result = pnm_server_v->pnm_adp_info_list_zc(zonenamep,
		    adp_listp, alladdrs, env);
	} else {
		result = pnm_server_v->pnm_adp_info_list(adp_listp,
		    alladdrs, env);
	}
	if (env.exception() != NULL) {
		if ((CORBA::COMM_FAILURE::_exnarrow(env.exception()) != NULL) ||
		    (CORBA::INV_OBJREF::_exnarrow(env.exception()) != NULL)) {
			//
			// Invocation to the server returned an exception. The
			// mostly likely reason is that the server is down.
			// We will return the appropriate error code back to
			// the client.
			//
			env.clear();
			delete [] nm_pnm_proxy;
			return (PNM_ESDOWN);
		} else {
			// Error, unknown exception.
			result = PNM_EPROG;
			ASSERT(0);
		}
	}
	delete [] nm_pnm_proxy;
	if (result != 0) {
		return (result);
	}
	pnm_mod::AdpinfoSeq &adp_listref = *adp_listp;
	uint_t num_adps = adp_listref.length();

	//
	// Walk through the adapter list and build a linked list of
	// adapterinfo structure that is expected by the PNM library.
	//
	for (indx = 0; indx < num_adps; ++indx) {

		adp = new pnm_adapterinfo_t;
		if (adp == NULL) {
			log_syserr_lib("PNM proxy: Out of memory, size = %d",
			    sizeof (pnm_adapterinfo_t));
			pnm_adapterinfo_free(head_adp);
			delete adp_listp;
			return (PNM_EPROG);
		}
		adp->next = NULL;

		// Construct linked list.
		if (head_adp == NULL) {
			head_adp = adp;
		}
		if (last_adp != NULL) {
			last_adp->next = adp;
			adp->prev = last_adp;
		} else {
			adp->prev = NULL;
		}
		last_adp = adp;
		adp->adp_name = os::strdup(adp_listref[indx].adp_name);

		if (adp->adp_name == NULL) {
			log_syserr_lib("Pnm proxy:Out of memory, size = %d",
			    os::strlen(adp_listref[indx].adp_name));
			pnm_adapterinfo_free(head_adp);
			delete adp_listp;
			return (PNM_EPROG);
		}
		//
		// If groupname is non-NULL then assign a pointer to it.
		//
		if (adp_listref[indx].adp_group != NULL) {
			adp->adp_group = os::strdup(
			    adp_listref[indx].adp_group);
			if (adp->adp_group == NULL) {
				log_syserr_lib("Pnm proxy:Out of memory,size "
				    "= %d.", os::strlen(
				    adp_listref[indx].adp_group));
				pnm_adapterinfo_free(head_adp);
				delete adp_listp;
				return (PNM_EPROG);
			}
		} else {
			adp->adp_group = NULL;
		}
		adp->adp_flags = adp_listref[indx].adp_flags;
		adp->adp_addrs.num_addrs =
		    adp_listref[indx].adp_ipmask.length();

		num_addrs = (uint_t)adp->adp_addrs.num_addrs;

		adp->adp_addrs.adp_ipnet = (struct in6_addr *)
		    new in6_addr[(uint_t)num_addrs];

		if (adp->adp_addrs.adp_ipnet == NULL) {
			log_syserr_lib("PNM proxy:Out of memory,size = %d.",
			    num_addrs * sizeof (struct in6_addr));
			pnm_adapterinfo_free(head_adp);
			delete adp_listp;
			return (PNM_EPROG);
		}
		adp->adp_addrs.adp_ipmask = (struct in6_addr *)
		    new in6_addr[(uint_t)num_addrs];

		if (adp->adp_addrs.adp_ipmask == NULL) {
			log_syserr_lib("PNM proxy:Out of memory, size = %d.",
			    num_addrs * sizeof (struct in6_addr));
			pnm_adapterinfo_free(head_adp);
			delete adp_listp;
			return (PNM_EPROG);
		}
		adp->adp_addrs.adp_ipaddr = (struct in6_addr *)
		    new in6_addr[(uint_t)num_addrs];

		if (adp->adp_addrs.adp_ipaddr == NULL) {
			log_syserr_lib("Pnm proxy:Out of memory, size = %d.",
			    num_addrs * sizeof (struct in6_addr));
			pnm_adapterinfo_free(head_adp);
			delete adp_listp;
			return (PNM_EPROG);
		}
		for (addr_indx = 0; addr_indx <
		    adp_listref[indx].adp_ipmask.length(); ++addr_indx) {
			bcopy(&adp_listref[indx].adp_ipmask[addr_indx],
			    &adp->adp_addrs.adp_ipmask[addr_indx],
			    sizeof (struct in6_addr));
			bcopy(&adp_listref[indx].adp_ipnet[addr_indx],
			    &adp->adp_addrs.adp_ipnet[addr_indx],
			    sizeof (struct in6_addr));
			bcopy(&adp_listref[indx].adp_ipaddr[addr_indx],
			    &adp->adp_addrs.adp_ipaddr[addr_indx],
			    sizeof (struct in6_addr));
		}
	}
	delete adp_listp;
	*adpp = head_adp;
	return (0);
}

//
// Proxy interface for the pnm_map_adapter interface.
// Return value:
// If the hostname is null or does not correspond to a cluster node,
// return PNM_EPROG. If the PNM prox service daemon is down,
// return PNM_ESDOWN. Else, the return value is what the pnm_map_adapter
// will return.
//
int
proxy_pnm_map_adapter(char *hostnamep, char *zonenamep, pnm_adapter_t adp,
    pnm_group_t *groupp)
{
	CORBA::Object_var		obj_v;
	pnm_mod::pnm_server_var		pnm_server_v;
	char				*nm_pnm_proxy;
	Environment			env;
	naming::naming_context_var	ctx_v = ns::root_nameserver();
	int				result;
	bool				is_zc_from_global;

	if (hostnamep == NULL) {
		return (PNM_EPROG);
	}
	nm_pnm_proxy = get_proxy_name(hostnamep, zonenamep,
	    is_zc_from_global);
	if (nm_pnm_proxy == NULL) {
		log_syserr_lib("Invalid hostname %s\n", hostnamep);
		return (PNM_EPROG);
	}
	obj_v = ctx_v->resolve(nm_pnm_proxy, env);
	if (env.exception()) {
		log_syserr_lib("Failed to resolve pnm proxy %s\n",
		    nm_pnm_proxy);
		env.clear();
		delete [] nm_pnm_proxy;
		return (PNM_EPROG);
	}
	pnm_server_v = pnm_mod::pnm_server::_narrow(obj_v);
	ASSERT(!CORBA::is_nil(pnm_server_v));
	//
	// If we are dealing with zone cluster, the proxy runs in the
	// global zone. The proxy needs the zonename when the client
	// is not executing in the zone cluster context.
	//
	if (is_zc_from_global) {
		result = pnm_server_v->pnm_map_adapter_zc(zonenamep,
		    (const char *)adp, (char *)*groupp, env);
	} else {
		result = pnm_server_v->pnm_map_adapter((const char *)adp,
		    (char *)*groupp, env);
	}
	if (env.exception() != NULL) {
		if ((CORBA::COMM_FAILURE::_exnarrow(env.exception()) != NULL) ||
		    (CORBA::INV_OBJREF::_exnarrow(env.exception()) != NULL)) {
			//
			// Invocation to the server returned an exception. The
			// mostly likely reason is that the server is down.
			// We will return the appropriate error code back to
			// the client.
			//
			delete [] nm_pnm_proxy;
			env.clear();
			return (PNM_ESDOWN);
		} else {
			// Error, unknown exception.
			result = PNM_EPROG;
			ASSERT(0);
		}
	}
	delete [] nm_pnm_proxy;
	if (result != 0) {
		return (result);
	}
	return (0);
} //lint !e818

//
// Proxy interface for the pnm_grp_adp_status interface.
// Return value:
// If the hostname is null or does not correspond to a cluster node,
// return PNM_EPROG. If the PNM prox service daemon is down,
// return PNM_ESDOWN. Else, the return value is what the pnm_get_instances
// will return.
//
int
proxy_pnm_grp_adp_status(char *hostnamep, char *zonenamep,
	pnm_group_t group, pnm_grp_status_t *grp_statusp)
{
	CORBA::Object_var		obj_v;
	pnm_mod::pnm_server_var		pnm_server_v;
	char				*nm_pnm_proxy;
	pnm_mod::AdpstatusSeq		*grpstatusp;
	Environment			env;
	naming::naming_context_var	ctx_v = ns::root_nameserver();
	uint_t				indx;
	int				result;
	bool				is_zc_from_global = true;

	if (hostnamep == NULL) {
		return (PNM_EPROG);
	}
	nm_pnm_proxy = get_proxy_name(hostnamep, zonenamep, is_zc_from_global);
	if (nm_pnm_proxy == NULL) {
		log_syserr_lib("Invalid hostname %s\n", hostnamep);
		return (PNM_EPROG);
	}
	obj_v = ctx_v->resolve(nm_pnm_proxy, env);
	if (env.exception()) {
		log_syserr_lib("Failed to resolve pnm proxy %s\n",
		    nm_pnm_proxy);
		env.clear();
		delete [] nm_pnm_proxy;
		return (PNM_EPROG);
	}
	pnm_server_v = pnm_mod::pnm_server::_narrow(obj_v);
	ASSERT(!CORBA::is_nil(pnm_server_v));

	if (is_zc_from_global) {
		result = pnm_server_v->pnm_grp_adp_status_zc(zonenamep,
		    (const char *) group, grpstatusp, env);
	} else {
		result = pnm_server_v->pnm_grp_adp_status(
		    (const char *)group, grpstatusp, env);
	}
	if (env.exception() != NULL) {
		if ((CORBA::COMM_FAILURE::_exnarrow(env.exception()) != NULL) ||
		    (CORBA::INV_OBJREF::_exnarrow(env.exception()) != NULL)) {
			//
			// Invocation to the server returned an exception. The
			// mostly likely reason is that the server is down.
			// We will return the appropriate error code back to
			// the client.
			//
			env.clear();
			delete [] nm_pnm_proxy;
			return (PNM_ESDOWN);
		} else {
			// Error, unknown exception.
			result = PNM_EPROG;
			ASSERT(0);
		}
	}
	delete [] nm_pnm_proxy;
	if (result != 0) {
		return (result);
	}
	pnm_mod::AdpstatusSeq &grpstatus_ref = *grpstatusp;
	grp_statusp->num_adps = grpstatus_ref.length();
	grp_statusp->pnm_adp_stat = (pnm_adp_status_t *)
	    new pnm_adp_status_t[grpstatus_ref.length()];

	if (grp_statusp->pnm_adp_stat == NULL) {
		log_syserr_lib("PNM proxy:Out of memory, size = %d",
		    sizeof (pnm_adp_status_t) * grpstatus_ref.length());
		return (PNM_EPROG);
	}

	for (indx = 0; indx < grpstatus_ref.length(); ++indx) {
		grp_statusp->pnm_adp_stat[indx].adp_name
		    = os::strdup(grpstatus_ref[indx].adp_name);

		if (grp_statusp->pnm_adp_stat[indx].adp_name == NULL) {
			log_syserr_lib("PNM proxy:Out of memory, size = %d",
			    os::strlen(grpstatus_ref[indx].adp_name));
			return (PNM_EPROG);
		}
		grp_statusp->pnm_adp_stat[indx].status =
		    grpstatus_ref[indx].status;
	}
	delete grpstatusp;
	return (0);
} //lint !e818

//
// Proxy interface for the pnm_get_instances interface.
// Return value:
// If the hostname is null or does not correspond to a cluster node,
// return PNM_EPROG. If the PNM proxy service daemon is down,
// return PNM_ESDOWN. Else, the return value is what the pnm_get_instances
// will return.
//
int
proxy_pnm_get_instances(char *hostnamep, char *zonenamep,
	pnm_group_t group, int *resultp)
{
	CORBA::Object_var		obj_v;
	pnm_mod::pnm_server_var		pnm_server_v;
	naming::naming_context_var	ctx_v = ns::root_nameserver();
	char				*nm_pnm_proxy;
	int		retval;
	Environment	env;
	bool		is_zc_from_global;

	if (!hostnamep) {
		return (PNM_EPROG);
	}
	nm_pnm_proxy = get_proxy_name(hostnamep, zonenamep,
	    is_zc_from_global);
	if (nm_pnm_proxy == NULL) {
		log_syserr_lib("Invalid hostname %s\n", hostnamep);
		return (PNM_EPROG);
	}
	obj_v = ctx_v->resolve(nm_pnm_proxy, env);
	if (env.exception()) {
		log_syserr_lib("Failed to resolve PNM proxy %s\n",
		    nm_pnm_proxy);
		env.clear();
		delete [] nm_pnm_proxy;
		return (PNM_EPROG);
	}
	pnm_server_v = pnm_mod::pnm_server::_narrow(obj_v);
	ASSERT(!CORBA::is_nil(pnm_server_v));

	if (is_zc_from_global) {
		retval = pnm_server_v->pnm_get_instances_zc(zonenamep,
		    (const char *)group, *resultp, env);
	} else {
		retval = pnm_server_v->pnm_get_instances(
		    (const char *)group, *resultp, env);
	}
	if (env.exception() != NULL) {
		if ((CORBA::COMM_FAILURE::_exnarrow(env.exception()) != NULL) ||
		    (CORBA::INV_OBJREF::_exnarrow(env.exception()) != NULL)) {
			//
			// Invocation to the server returned an exception. The
			// mostly likely reason is that the server is down.
			// We will return the appropriate error code back to
			// the client.
			//
			delete [] nm_pnm_proxy;
			env.clear();
			return (PNM_ESDOWN);
		} else {
			// Error, unknown exception.
			retval = PNM_EPROG;
			ASSERT(0);
		}
	}
	delete [] nm_pnm_proxy;
	return (retval);
} //lint !e818

} // end extern "C"
