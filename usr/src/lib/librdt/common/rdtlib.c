/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident   "@(#)rdtlib.c 1.14     08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/uio.h>
#include <sys/sysmacros.h>
#include <sys/resource.h>
#include <errno.h>
#include <dlfcn.h>
#include <fcntl.h>
#include <sched.h>
#include <stropts.h>
#include <synch.h>
#include <thread.h>

#include <rdtapi.h>
#include <rdtlib_in.h>
#include <rsmrdt_if.h>

#ifdef __STDC__

#pragma weak rdt_get_nodeid = _rdt_get_nodeid
#pragma weak rdt_hdlsize = _rdt_hdlsize
#pragma weak rdt_create = _rdt_create
#pragma weak rdt_bind = _rdt_bind
#pragma weak rdt_connect = _rdt_connect
#pragma weak rdt_send = _rdt_send
#pragma weak rdt_recvmsgs = _rdt_recvmsgs
#pragma weak rdt_getparam = _rdt_getparam
#pragma weak rdt_setparam = _rdt_setparam
#pragma weak rdt_retrieve_fd = _rdt_retrieve_fd
#pragma weak rdt_destroy = _rdt_destroy

#endif /* __STDC__ */

#ifdef DEBUG

FILE *rdtlog_fd = NULL;
static mutex_t rdtlog_lock;
int rdtlibdbg_category = RDT_LIBRARY;
int rdtlibdbg_level = RDT_ERR;
void dbg_printf(int category, int level, char *fmt, ...);

#endif /* DEBUG */

#pragma init(_rdt_librdt_init)

/*
 * The _rdt_librdt_init function is called the first time an application
 * references the RDTAPI library
 */
int
_rdt_librdt_init()
{
	char logname[MAXNAMELEN];

#ifdef DEBUG
	(void) mutex_init(&rdtlog_lock, USYNC_THREAD, NULL);
	(void) sprintf(logname, "%s.%d", TRACELOG, getpid());
	rdtlog_fd = fopen(logname, "w+");
	if (rdtlog_fd == NULL) {
		(void) fprintf(stderr, "Log file open failed\n");
		/*
		 * suppress lint complaint
		 * "call to ___errno() not made in the presence of
		 * a prototype"
		 */
		return (errno); /*lint !e746 */
	}

#endif /* DEBUG */


	DBPRINTF((RDT_LIBRARY, RDT_DEBUG_VERBOSE,
	    "_rdt_librdt_init: exit\n"));

	return (RDT_SUCCESS);
}

/*
 * Returns the local nodeid of the cluster node
 */

int
_rdt_get_nodeid()
{
	int mynodeid;
	int fd = open(DEVRSMRDT, O_RDWR);
	if (fd < 0) {
		DBPRINTF((RDT_LIBRARY, RDT_ERR,
		    "unable to open device /dev/rsmrdt\n"));
		errno = ENOENT;
		return (-1);
	}

	if (ioctl(fd, RSMRDT_IOCTL_GETNODEID, &mynodeid) < 0) {
		DBPRINTF((RDT_LIBRARY, RDT_ERR, "unable to get nodeid\n"));
		return (-1);
	}

	if (close(fd) < 0) {
		DBPRINTF((RDT_LIBRARY, RDT_ERR,
			"unable to close /dev/rsmrdt\n"));
		errno = ENOENT;
		return (-1);
	}
	return (mynodeid);
}


/*
 * Returns the size of memory a rdt handle occupies
 */

size_t
_rdt_hdlsize()
{
	return (sizeof (rdt_handle_t));
}

/*
 * The rdt_create() operation is used to create a communication endpoint.
 * On successful completion, it returns 'rdt_handle_t' which is used in
 * all the subsequent API function calls.
 */

int
_rdt_create(rdt_handle_t *hdl, uint_t flags)
{
	int fd;

	DBPRINTF((RDT_LIBRARY, RDT_DEBUG_VERBOSE, "rdt_create: enter\n"));

	if (hdl == NULL) {
		DBPRINTF((RDT_LIBRARY, RDT_ERR, "Invalid handle\n"));
		errno = EFAULT;
		return (-1);
	}

	fd = open(DEVRSMRDT, O_RDWR);

	if (fd < 0) {
		DBPRINTF((RDT_LIBRARY, RDT_ERR,
		    "unable to open device /dev/rsmrdt\n"));
		return (-1);
	}

	hdl->fd = fd;

	DBPRINTF((RDT_LIBRARY, RDT_DEBUG_VERBOSE,
	    "rdt_create: exit\n"));

	return (RDT_SUCCESS);
/*
 * Supress lint Info(715) [c:0]: flags not referenced
 */
} /*lint !e715 */

/*
 * The rdt_bind() operation returns the local endpoint port number. The
 * port numbers are re-used but are always guaranteed to be unique.
 */

int
_rdt_bind(rdt_handle_t *hdl, uint32_t *portnum, uint_t flags)
{
	DBPRINTF((RDT_LIBRARY, RDT_DEBUG_VERBOSE,
	    "rdt_bind: enter\n"));

	if ((hdl == NULL) || (hdl->fd < 0)) {
		DBPRINTF((RDT_LIBRARY, RDT_ERR, "Invalid handle\n"));
		errno = EBADF;
		return (-1);
	}

	if (ioctl(hdl->fd, RSMRDT_IOCTL_BIND, portnum) < 0) {
		DBPRINTF((RDT_LIBRARY, RDT_ERR, "unable to bind\n"));
		return (-1);
	}

	DBPRINTF((RDT_LIBRARY, RDT_DEBUG_VERBOSE,
	    "rdt_bind: exit\n"));

	return (RDT_SUCCESS);
/*
 * Supress lint Info(715) [c:0]: flags not referenced
 */
} /*lint !e715 */


/*
 * The rdt_connect() operation initiates a connection. The local
 * endpoint connects to the remote endpoint specified by remote
 * nodeid and port number. The local communication endpoint remembers
 * target address and uses this address for all the outgoing messages.
 */

int
_rdt_connect(rdt_handle_t *hdl, int nodeid, uint32_t portnum,
	uint_t flags)
{
	rsmrdt_connect_arg_t args;

	DBPRINTF((RDT_LIBRARY, RDT_DEBUG_VERBOSE,
	    "rdt_connect: enter\n"));

	if ((hdl == NULL) || (hdl->fd < 0)) {
		DBPRINTF((RDT_LIBRARY, RDT_ERR, "Invalid handle\n"));
		errno = EBADF;
		return (-1);
	}

	args.nodeid = nodeid;
	args.portnum = portnum;

	if (ioctl(hdl->fd, RSMRDT_IOCTL_CONNECT, &args) < 0) {
		DBPRINTF((RDT_LIBRARY, RDT_ERR, "unable to connect\n"));
		return (-1);
	}

	DBPRINTF((RDT_LIBRARY, RDT_DEBUG_VERBOSE,
	    "rdt_connect: exit\n"));

	return (RDT_SUCCESS);
/*
 * Supress lint Info(715) [c:0]: flags not referenced
 */
} /*lint !e715 */

/*
 * The rdt_send() operation sends a message, which is specified by a
 * pointer to uio structure (iov) and number of iovecs (iovcnt), to a
 * remote endpoint identified with rdt_connect() operation. It is a
 * non-blocking operation which sends one message at a time. It maintains
 * message boundary, send order and guarantees the delivery of the whole
 * message. In case of transient errors, the kernel agent performs all the
 * retries.
 *
 * If the resources are temporarily unavailable, it is flow controlled
 * and returns EWOULDBLOCK. The caller should use poll(2) to find out when
 * the flow controlled condition is lifted. If the message size is larger
 * than MTU size, it returns EMSGSIZE error.
 *
 * If the destination endpoint doesn't exist, the message is dropped by
 * the receiving node and an asynchronous error ESRCH is reported back to
 * the sender. Upon getting ESRCH, the sender is put in an error state
 * until the client corrects it with an rdt_connect() operation again.
 */

int
_rdt_send(rdt_handle_t *hdl, iovec_t *iov, uint_t iovcnt, uint_t flags)
{
	rsmrdt_send_arg_t args;

	DBPRINTF((RDT_LIBRARY, RDT_DEBUG_VERBOSE,
	    "rdt_send: enter\n"));

	if ((hdl == NULL) || (hdl->fd < 0)) {
		DBPRINTF((RDT_LIBRARY, RDT_ERR, "Invalid handle\n"));
		errno = EBADF;
		return (-1);
	}

	args.iov = iov;
	args.iovcnt = iovcnt;

	if (ioctl(hdl->fd, RSMRDT_IOCTL_SENDMSG, &args) < 0) {
		DBPRINTF((RDT_LIBRARY, RDT_ERR, "unable to send\n"));
		return (-1);
	}

	DBPRINTF((RDT_LIBRARY, RDT_DEBUG_VERBOSE,
	    "rdt_send: exit\n"));

	return (RDT_SUCCESS);
/*
 * Supress lint Info(715) [c:0]: flags not referenced
 */
} /*lint !e715 */

/*
 * The rdt_recvmsgs() is a non-blocking operation and is used to receive
 * multiple messages from another communication endpoint. It posts 'msgcnt'
 * number of message buffers (specified by 'rdt_recvmsg_t') to kernel
 * agent to receive multiple messages. On return, 'msgcnt' is filled with
 * the number of messages retrieved. If no messages are available at the
 * local endpoint, it returns EWOULDBLOCK. The caller should use poll(2) to
 * find out when the messages are available. If a message is too long to
 * fit in the supplied buffer, excessive bytes are discarded.
 *
 * In case of internal memory allocation errors, it does not drop packets
 * and returns ENOMEM.
 *
 */

int
_rdt_recvmsgs(rdt_handle_t *hdl, rdt_recvmsg_t *msg_iov, uint_t *msgcnt,
	int timeout, uint_t flags)
{
	rsmrdt_recvmsgs_arg_t args;

	DBPRINTF((RDT_LIBRARY, RDT_DEBUG_VERBOSE,
	    "rdt_recvmsgs: enter\n"));

	if ((hdl == NULL) || (hdl->fd < 0)) {
		DBPRINTF((RDT_LIBRARY, RDT_ERR, "Invalid handle\n"));
		errno = EBADF;
		return (-1);
	}

	args.msg_iov = msg_iov;
	args.msgcnt = *msgcnt;
	args.timeout = timeout;

	if (ioctl(hdl->fd, RSMRDT_IOCTL_RECVMSGS, &args) < 0) {
		DBPRINTF((RDT_LIBRARY, RDT_ERR, "unable to receive\n"));
		return (-1);
	}

	/* Return the number of messages read */
	*msgcnt = args.msgcnt;

	DBPRINTF((RDT_LIBRARY, RDT_DEBUG_VERBOSE,
	    "rdt_recvmsgs: exit\n"));

	return (RDT_SUCCESS);
/*
 * Supress lint Info(715) [c:0]: flags not referenced
 */
} /*lint !e715 */

/*
 * The rdt_getparam() operation is used to retrieve the value of one of
 * the following kernel agent parameters.
 *     o Maximum message size (MTU).
 *     o per fd protection key
 */

int
_rdt_getparam(rdt_handle_t *hdl, uint32_t cmd, void *value, size_t size)
{
	rsmrdt_getsetparam_arg_t args;

	DBPRINTF((RDT_LIBRARY, RDT_DEBUG_VERBOSE,
	    "rdt_getparam: enter\n"));

	if ((hdl == NULL) || (hdl->fd < 0)) {
		DBPRINTF((RDT_LIBRARY, RDT_ERR, "Invalid handle\n"));
		errno = EBADF;
		return (-1);
	}

	args.cmd = cmd;
	args.value = value;
	args.size = size;

	if (ioctl(hdl->fd, RSMRDT_IOCTL_GETPARAM, &args) < 0) {
		DBPRINTF((RDT_LIBRARY, RDT_ERR,
			"unable to getparam\n"));
		return (-1);
	}

	DBPRINTF((RDT_LIBRARY, RDT_DEBUG_VERBOSE,
	    "rdt_getparam: exit\n"));

	return (RDT_SUCCESS);
}

/*
 * The rdt_setparam() operation is used to set the value of the
 * following driver parameter.
 *     o per fd protection key
 */

int
_rdt_setparam(rdt_handle_t *hdl, uint32_t cmd, void *value, size_t size)
{
	rsmrdt_getsetparam_arg_t args;

	DBPRINTF((RDT_LIBRARY, RDT_DEBUG_VERBOSE,
	    "rdt_setparam: enter\n"));

	if ((hdl == NULL) || (hdl->fd < 0)) {
		DBPRINTF((RDT_LIBRARY, RDT_ERR, "Invalid handle\n"));
		errno = EBADF;
		return (-1);
	}

	args.cmd = cmd;
	args.value = value;
	args.size = size;

	if (ioctl(hdl->fd, RSMRDT_IOCTL_SETPARAM, &args) < 0) {
		DBPRINTF((RDT_LIBRARY, RDT_ERR,
				"unable to setparam\n"));
		return (-1);
	}

	DBPRINTF((RDT_LIBRARY, RDT_DEBUG_VERBOSE,
		"rdt_setparam: exit\n"));

	return (RDT_SUCCESS);
}


/*
 * The rdt_retrieve_fd() operation returns the file descriptor to
 * the RDT driver
 */

int
_rdt_retrieve_fd(rdt_handle_t *hdl)
{
	DBPRINTF((RDT_LIBRARY, RDT_DEBUG_VERBOSE,
	    "rdt_retrieve_fd: enter\n"));

	if ((hdl == NULL) || (hdl->fd < 0)) {
		errno = EBADF;
		DBPRINTF((RDT_LIBRARY, RDT_ERR, "not a valid handle\n"));
		return (-1);
	}

	DBPRINTF((RDT_LIBRARY, RDT_DEBUG_VERBOSE,
	    "rdt_retrieve_fd: exit\n"));

	return (hdl->fd);
}

/*
 * The rdt_destroy() operation is used to close the communication
 * endpoint. It deallocates all the resources associated with the
 * 'rdt_handle_t' handle.
 */

int
_rdt_destroy(rdt_handle_t *hdl)
{
	DBPRINTF((RDT_LIBRARY, RDT_DEBUG_VERBOSE,
	    "rdt_destroy: enter\n"));

	if ((hdl == NULL) || (hdl->fd < 0)) {
		DBPRINTF((RDT_LIBRARY, RDT_ERR, "Invalid handle\n"));
		errno = EBADF;
		return (-1);
	}

	if (close(hdl->fd) < 0) {
		DBPRINTF((RDT_LIBRARY, RDT_ERR, "unable to destroy\n"));
		return (-1);
	}

	DBPRINTF((RDT_LIBRARY, RDT_DEBUG_VERBOSE,
	    "rdt_destroy: exit\n"));

	return (RDT_SUCCESS);
}


#ifdef DEBUG
void
dbg_printf(int msg_category, int msg_level, char *fmt, ...)
{
	if ((msg_category & rdtlibdbg_category) &&
	    (msg_level <= rdtlibdbg_level)) {
		va_list arg_list;
		/*lint -e40 Undeclared identifier (__builtin_va_alist) */
		va_start(arg_list, fmt);
		/*lint +e40 */
		(void) mutex_lock(&rdtlog_lock);
		(void) fprintf(rdtlog_fd, "Thread %d ", thr_self());
		(void) vfprintf(rdtlog_fd, fmt, arg_list);
		(void) fflush(rdtlog_fd);
		(void) mutex_unlock(&rdtlog_lock);
		va_end(arg_list);
	}
}
#endif /* DEBUG */
