/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RDTAPI_H
#define	_RDTAPI_H

#pragma ident	"@(#)rdtapi.h	1.11	08/05/20 SMI"

#ifdef	__cplusplus
extern "C" {
#endif

#include <sys/param.h>
#include <sys/uio.h>

/*
 * Handle data structure - Returned by rdt_create() operation and is
 * used in all the API function calls.
 */

typedef struct rdt_handle rdt_handle_t;

/*
 * Receive message structure - contains a pointer to an array of iovecs,
 * number of iovecs and a field for size of the message (in bytes) which is
 * filled in by the kernel agent.
 */

typedef struct {
	iovec_t		*iov;		/* pointer to array of iovecs */
	uint_t		iovcnt;		/* number of iovecs */
	size_t		bytes_recvd;	/* number of bytes received */
	size_t  	msglen;		/* original length of the msg */
	int		nodeid;		/* sender's nodeid */
	uint32_t	portnum;	/* sender's portnum */
} rdt_recvmsg_t;


/*
 * Define get/setparam() commands
 */

#define	RDT_MAXMSGSIZE		0x1001  /* Max message size */
#define	RDT_PROTECTION_KEY	0x1002	/* protection key */


/*
 * function templates:
 */

int rdt_get_nodeid();
size_t rdt_hdlsize();
int rdt_create(rdt_handle_t *hdl, uint_t flags);
int rdt_bind(rdt_handle_t *hdl, uint32_t *portnum, uint_t flags);
int rdt_connect(rdt_handle_t *hdl, int nodeid,
	uint32_t portnum, uint_t flags);
int rdt_send(rdt_handle_t *hdl, iovec_t *iov, uint_t iovcnt,
	uint_t flags);
int rdt_recvmsgs(rdt_handle_t *hdl, rdt_recvmsg_t *msg_iov,
	uint_t *msgcnt, int timeout, uint_t flags);
int rdt_getparam(rdt_handle_t *hdl, uint32_t cmd, void *value, size_t size);
int rdt_setparam(rdt_handle_t *hdl, uint32_t cmd, void *value, size_t size);
int rdt_retrieve_fd(rdt_handle_t *hdl);
int rdt_destroy(rdt_handle_t *hdl);


#ifdef	__cplusplus
}
#endif

#endif	/* _RDTAPI_H */
