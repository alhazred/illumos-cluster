/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RDTLIB_IN_H
#define	_RDTLIB_IN_H

#pragma ident	"@(#)rdtlib_in.h	1.7	08/05/20 SMI"

#ifdef	__cplusplus
extern "C" {
#endif

#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>


/*
 * Handle data structure - Returned by rdt_create() operation and is
 * used in all the API function calls.
 */

struct rdt_handle {
	int fd;
};


/* Return values for RDTAPI */
#define	RDT_SUCCESS	0

#define	DEVRSMRDT	"/dev/rsmrdt"

/*
 * DEBUG message categories
 * 0xABCD:  A=module, B=functionality C=operation D=misc
 *
 */
#define	RDT_LIBRARY	0x1000  /* rdtapi library messages */

/*
 * DEBUG message levels
 */
#define	RDT_DEBUG_VERBOSE	6
#define	RDT_DEBUG_LVL2		5
#define	RDT_DEBUG_LVL1		4
#define	RDT_DEBUG		3
#define	RDT_NOTICE		2
#define	RDT_WARNING		1
#define	RDT_ERR			0

/*
 * The following macros are defined only if the DEBUG flag is enabled
 * The macro makes use of category and level values defined above
 * and the dbg_printf function defined in rdtlib.c (defined as an
 * extern below)
 */
#ifdef	DEBUG
#define	TRACELOG "/tmp/librdt.log"
#define	DBPRINTF(msg) dbg_printf msg
#else
#define	TRACELOG
#define	DBPRINTF(msg)
#endif

#ifdef	__cplusplus
}
#endif

#endif	/* _RDTLIB_IN_H */
