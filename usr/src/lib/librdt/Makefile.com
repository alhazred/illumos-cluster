#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.13	08/05/20 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# lib/librdt/Makefile.com
#

LIBRARY= librdt.a
VERS= .1

TEXT_DOMAIN=	SUNW_OST_OSLIB

OBJECTS = rdtlib.o 

# include library definitions, do not change order of include and DYNLIB
include ../../Makefile.lib

MAPFILE = 	../common/mapfile-vers
SRCS =		$(OBJECTS:%.o=../common/%.c)

LIBS = $(DYNLIB)

# The DEBUG flag is enabled for debug builds
DEBUG =
$(NOT_RELEASE_BUILD)DEBUG = -DDEBUG

MTFLAG	= -mt

CPPFLAGS += -I../inc -I../../../uts/common/io/rsmrdt
CFLAGS += $(DEBUG)
CFLAGS64 += $(DEBUG)
PMAP = -M $(MAPFILE)

LINTFLAGS +=	-I../inc -DDEBUG -I../../../uts/common/io/rsmrdt
LINTFLAGS64 +=	-I../inc -DDEBUG -D__sparcv9 -I../../../uts/common/io/rsmrdt

LINTFILES += $(OBJECTS:%.o=%.ln)

CLOBBERFILES += $(DYNLIB)

.KEEP_STATE:

all:  $(LIBS)

$(DYNLIB): 	$(MAPFILE)

# include library targets
include ../../Makefile.targ

objs/%.o pics/%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<
