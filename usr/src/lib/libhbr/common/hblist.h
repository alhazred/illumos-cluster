/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef __HBLIST_H
#define	__HBLIST_H

#pragma ident	"@(#)hblist.h	1.3	08/05/20 SMI"

#include <synch.h>
#include <sys/types.h>

/*
 * Node identifier
 */
typedef uint32_t list_key_t;

#define	LIST_INVALID_KEY    ((list_key_t) -1)
#define	HASH_TABLE_SIZE	256

/*
 * The list
 */
typedef struct list_element {
    list_key_t		key;
    void		*data;
    struct list_element	*next;
    struct list_element	*prev;
} list_element_t;

typedef list_element_t *hash_list_t;

typedef struct {
    hash_list_t		list[HASH_TABLE_SIZE];
    rwlock_t		lock;
    size_t		element_size;
    size_t		elements;
} list_list_t;

/*
 * Handler for the searches.
 */
typedef void *list_search_handler_t;

/* Node access functions */
int   list_init(list_list_t *list, const size_t initial_elements,
    const size_t element_size);
void *list_add(list_list_t *list, const list_key_t key);
void *list_get(list_list_t *list, const list_key_t key);
void *list_get_first(list_list_t *list, list_search_handler_t *handler);
void *list_get_next(list_list_t *list, list_search_handler_t *handler);
int   list_del(list_list_t *list, const list_key_t key);
int   list_count(list_list_t *list);
int   list_reset(list_list_t *list);
int   list_destroy(list_list_t *list);


#endif /* __HBLIST_H */
