/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma	ident	"@(#)hblist.c 1.3     08/05/20 SMI"

#include <libintl.h>
#include <stdlib.h>
#include <strings.h>
#include <assert.h>
#include <errno.h>

#include "hblist.h"

static unsigned int
hash_function(list_key_t key)
{
	unsigned int key_index;
	key_index = key % HASH_TABLE_SIZE;
	return (key_index);
}


/*
 * ***************************************************************
 * list:             a pointer to the list object
 * initial_elements: maximun number of elements that the list will handle
 * element_size:     size of one element
 *
 * return:
 * 0  - OK
 * -1 - error: bad arguments, no free memory, failed to lock
 * ***************************************************************
 */
int
list_init(list_list_t *list, const size_t initial_elements,
	const size_t element_size)
{
	register int i;

	/* Check the parameters */
	if (list == NULL || element_size == 0 || initial_elements == 0)
		return (-1);

	/* Create the list */
	list->element_size = element_size;
	list->elements = initial_elements;
	(void) rwlock_init(&list->lock, NULL, NULL);

	for (i = 0; i < HASH_TABLE_SIZE; i++)
		list->list[i] = NULL;

	(void) rw_unlock(&list->lock);

	return (0);
}

/*
 * ***************************************************************
 * list: a pointer to the list object
 * key:   elementid of the element to be added
 *
 * return:
 * NULL  - error: bad arguments, failed to lock, already added
 * other - pointer to the added element
 * ***************************************************************
 */
void *
list_add(list_list_t *list, const list_key_t key)
{
	list_element_t	*tmp_list;
	unsigned int hash = 0;

	if (list == NULL)
		return (NULL);

	tmp_list = malloc(sizeof (list_element_t));
	if (tmp_list == NULL)
		return (NULL);

	tmp_list->key = key;
	tmp_list->data = malloc(list->element_size);
	tmp_list->next = NULL;
	tmp_list->prev = NULL;

	hash = hash_function(key);

	/* Lock the table for writing */
	if (rw_wrlock(&list->lock) != 0)
		return (NULL);

	/* Insert tmp_list in list->list[hash] */

	tmp_list->next = list->list[hash];
	if (list->list[hash])
		list->list[hash]->prev = tmp_list;
	list->list[hash] = tmp_list;

	(void) rw_unlock(&list->lock);

	return (list->list[hash]->data);
}

/*
 * ***************************************************************
 * list: a pointer to the list object
 * key:   elementid of the searched element
 *
 * return:
 * NULL  - error: bad argument, failed to lock, not found
 * other - OK
 * ***************************************************************
 */
void *
list_get(list_list_t *list, const list_key_t key)
{
	unsigned int pos;
	list_element_t *elem;
	boolean_t found = B_FALSE;

	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (list == NULL)
		return (NULL);

	/* lock the table for reading */
	if (rw_rdlock(&list->lock) != 0)
		return (NULL);

	pos = hash_function(key);
	elem = list->list[pos];
	while ((found == B_FALSE) && elem) {
		if (elem->key == key) {
			found = B_TRUE;
			break;
		}
		elem = elem->next;
	}

	(void) rw_unlock(&list->lock);
	return ((found)?elem->data:NULL);

}


/*
 * ***************************************************************
 * list:    a pointer to the list object
 * handler: a pointer to the search handler
 *
 * return:
 * NULL  - no more elements
 * other - first element
 * ***************************************************************
 */
void *
list_get_first(list_list_t *list, list_search_handler_t *handler)
{
	unsigned int i = 0;
	list_element_t *elem;

	if (list == NULL || handler == NULL)
		return (NULL);

	/* lock the table for reading */
	if (rw_rdlock(&list->lock) != 0)
		return (NULL);

	*handler = NULL;

	for (i = 0; i < HASH_TABLE_SIZE; i++) {
		if (list->list[i] != NULL) {
			*handler = list->list[i];
			break;
		}
	}

	elem = *handler;

	(void) rw_unlock(&list->lock);

	return ((elem)?elem->data:NULL);
}

/*
 * ***************************************************************
 * list:    a pointer to the list object
 * handler: a pointer to the search handler
 *
 * return:
 * NULL  - no more elements
 * other - next element
 * ***************************************************************
 */
void *
list_get_next(list_list_t *list, list_search_handler_t *handler)
{

	unsigned int hash_index = 0;
	unsigned int i;
	list_element_t *elem;

	if (list == NULL || handler == NULL)
		return (NULL);

	elem = *handler;

	/* lock the table for reading */
	if (rw_rdlock(&list->lock) != 0)
		return (NULL);

	if (elem->next != NULL) {
		elem = elem->next;
	} else {
		/* Need to go to next list */
		hash_index = hash_function(elem->key);
		for (i = hash_index + 1; i < HASH_TABLE_SIZE; i++) {
			if (list->list[i] != NULL)
				break;
		}

		if (i < HASH_TABLE_SIZE)
			elem = list->list[hash_index];
		else
			elem = NULL;
	}

	(void) rw_unlock(&list->lock);

	*handler = elem;
	return (elem?elem->data:NULL);
}

/*
 * ***************************************************************
 * list: a pointer to the list object
 * key:   elementid of the element to be deleted
 *
 * return:
 * 0  - OK
 * -1 - error: bad arguments, failed to lock, not found
 * ***************************************************************
 */
int
list_del(list_list_t *list, const list_key_t key)
{
	unsigned int pos;
	list_element_t *elem;

	if (list == NULL)
		return (-1);

	/* Lock the table for writing */
	if (rw_wrlock(&list->lock) != 0)
		return (-1);

	pos = hash_function(key);

	for (elem = list->list[pos]; elem != NULL; elem = elem->next) {
		if (elem->key == key) {
			if (elem->prev != NULL)
				elem->prev->next = elem->next;
			else /* first node */
				list->list[pos] = elem->next;
			if (elem->next != NULL)
				elem->next->prev = elem->prev;
			free(elem->data);
			free(elem);
			elem = NULL;
			break;
		}
	}

	(void) rw_unlock(&list->lock);

	return (0);
}

/*
 * ***************************************************************
 * list: a pointer to the list object
 *
 * return:
 * 0  - OK
 * -1 - error: bad arguments, failed to lock
 * ***************************************************************
 */
int
list_count(list_list_t *list)
{
	register int count = 0;
	register int i;
	list_element_t *elem;

	if (list == NULL)
		return (-1);

	/* Lock the table for writing */
	if (rw_wrlock(&list->lock) != 0)
		return (-1);

	/* Count the number of elements */
	for (i = 0; i < HASH_TABLE_SIZE; i++) {
		for (elem = list->list[i]; elem != NULL; elem = elem->next) {
			count++;
		}
	}

	(void) rw_unlock(&list->lock);

	return (count);
}

/*
 * ***************************************************************
 * ATTENTION: the lock must be taken for writing
 *
 * list: a non-NULL pointer to the list object
 * ***************************************************************
 */
static void
list_clean(list_list_t *list)
{
	list_element_t *elem;
	unsigned int i;

	assert(rw_trywrlock(&list->lock) == EBUSY);

	for (i = 0; i < HASH_TABLE_SIZE; i++) {
		while (list->list[i] != NULL) {
			elem = list->list[i];
			list->list[i] = list->list[i]->next;
			free(elem->data);
			free(elem);
		}
	}
}

/*
 * ***************************************************************
 * list: a pointer to the list object
 *
 * return:
 * 0  - OK
 * -1 - error: bad arguments, failed to lock
 * ***************************************************************
 */
int
list_reset(list_list_t *list)
{
	if (list == NULL)
		return (-1);

	/* Lock the table for writing */
	if (rw_wrlock(&list->lock) != 0)
		return (-1);

	list_clean(list);

	(void) rw_unlock(&list->lock);

	return (0);
}

/*
 * ***************************************************************
 * list: a pointer to the list object
 *
 * return:
 * 0  - OK
 * -1 - error: bad arguments, failed to lock
 * ***************************************************************
 */
int
list_destroy(list_list_t *list)
{
	if (list == NULL)
		return (-1);

	/* Lock the table for writing */
	if (rw_wrlock(&list->lock) != 0)
		return (-1);

	list_clean(list);

	(void) rw_unlock(&list->lock);

	return (0);
}
