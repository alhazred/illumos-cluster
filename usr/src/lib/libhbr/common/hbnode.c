/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma	ident	"@(#)hbnode.c 1.4     08/05/20 SMI"

#include <pthread.h>
#include <libintl.h>
#include <assert.h>
#include <string.h>
#include <arpa/inet.h>

#include <fmm/hbnode.h>
#include <fmm/hbargs.h>
#include <fmm/fmmutils.h>
#include <sys/cl_events.h>
#include "hblist.h"

/* The list of monitored nodes */
static list_list_t G_NodesList;

static hb_callback *client_indicate = NULL;

/* Local functions */
static void
hbnode_send_link_indication(hb_node_t *H_Node, unsigned int H_Link);

static void
hbnode_send_node_indication(hb_node_t *H_Node);

static void
hbnode_event_handler(hbx_event_t event);

static void
hbnode_update_status(hb_node_t *P_node, hbx_event_t P_event);

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * These routines lock/unlock node mutexes
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Param IN:
 *   o Pointer to node related to the mutex
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static hb_error_t
hbnode_lock(hb_node_t *H_Node)
{
	int L_errno;

	if (H_Node == NULL) {
		return (HB_EINVAL);
	}

	L_errno = pthread_mutex_lock(&H_Node->mutex);
	if (L_errno != 0) {
		fmm_log(LOG_ERR, "[LIBHB] failed to lock mutex");
		assert(L_errno == 0);
		return (HB_EINVAL);
	}
	return (HB_OK);
}

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
static hb_error_t
hbnode_unlock(hb_node_t *H_Node)
{
	int L_errno;

	if (H_Node == NULL) {
		return (HB_EINVAL);
	}

	L_errno = pthread_mutex_unlock(&H_Node->mutex);
	if (L_errno != 0) {
		fmm_log(LOG_ERR, "[LIBHB] failed to unlock mutex");
		assert(L_errno == 0);
		return (HB_EINVAL);
	}
	return (HB_OK);
}


/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Get the status of a node
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static void
hbnode_get_status(hb_node_t *H_Node)
{
	unsigned int L_Link;
	int L_Count = 0;
	ipaddr_t saddr;
	uint8_t linkCount = hbargs_get_nic_count();

	if (H_Node == NULL) {
		return;
	}

	for (L_Link = 0; L_Link < linkCount; L_Link++) {
		/* poll the driver for this node's state */
		fmm_log(LOG_DEBUG, "[LIBHB] getting status for link %i",
		    L_Link);

		H_Node->links_status[L_Link].previous_state =
		    H_Node->links_status[L_Link].current_state;

		saddr = INT_ADDR(H_Node->links_addr[L_Link]);

		if (hbif_get_host_state(saddr, L_Link) == HOST_STATE_UP) {
			L_Count++;
			H_Node->links_status[L_Link].current_state = HB_ST_UP;
		} else {
			H_Node->links_status[L_Link].current_state = HB_ST_DOWN;
		}

		/* Send indications to the client */
		hbnode_send_link_indication(H_Node, L_Link);
		if (L_Count == 1 && H_Node->node_status.current_state !=
		    HB_ST_UP) {
			H_Node->node_status.previous_state =
			    H_Node->node_status.current_state;
			H_Node->node_status.current_state = HB_ST_UP;
			hbnode_send_node_indication(H_Node);
		}
	}

	if (L_Count == 0) {
		H_Node->node_status.previous_state =
		    H_Node->node_status.current_state;
		H_Node->node_status.current_state = HB_ST_DOWN;
		hbnode_send_node_indication(H_Node);
	}
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Set the status of every node
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
void
hbnode_set_status(void)
{
	list_search_handler_t	L_Handler;
	hb_node_t		*L_Node;

	L_Node = (hb_node_t *)list_get_first(&G_NodesList, &L_Handler);
	while (L_Node != NULL) {
		(void) hbnode_lock(L_Node);
		hbnode_get_status(L_Node);
		(void) hbnode_unlock(L_Node);

		L_Node = (hb_node_t *)list_get_next(&G_NodesList,
		    &L_Handler);
	}
}

hb_error_t
hbnode_delete_node(hb_nodeid_t nodeid)
{
	hb_error_t status = HB_OK;
	hb_node_t *L_Node;

	L_Node = (hb_node_t *)list_get(&G_NodesList, nodeid);
	if (L_Node != NULL) {
		uint8_t linkCount = hbargs_get_nic_count();
		ipaddr_t saddr;
		unsigned int L_Link;

		/* For each link, remove host from driver list */
		for (L_Link = 0; L_Link < linkCount; L_Link++) {
			saddr = INT_ADDR(L_Node->links_addr[L_Link]);
			if (hbif_delete_host(saddr, L_Link) != HB_OK) {
				status = HB_EINVAL;
			}
		}
	} else {
		status = HB_EINVAL;
	}

	/* Then remove the node from the list */
	(void) list_del(&G_NodesList, nodeid);
	return (status);
}

/* ********************************************************************* */
hb_error_t
hbnode_add(hb_node_t *node)
{
	hb_node_t	*L_Node;
	hb_nodeid_t	L_Id;
	int		L_Link;
	char buf[50];
	uint8_t link_count = hbargs_get_nic_count();

	if (node == NULL) {
		return (HB_EINVAL);
	}

	/* It is better to get this value here to avoid race conditions */
	(void) list_count(&G_NodesList);

	/*
	 * By convention take the node id as key
	 */
	(void) memset(buf, 0, sizeof (buf));
	L_Id = (uint32_t)node->node_id;

	(void) list_del(&G_NodesList, L_Id);
	L_Node = (hb_node_t *)list_add(&G_NodesList, L_Id);
	if (L_Node == NULL) {
		return (HB_ENOTSUP);
	}

	/* Fill the created node */
	if (pthread_mutex_init(&L_Node->mutex, NULL) != 0) {
		(void) list_del(&G_NodesList, L_Id);
		return (HB_ENOTSUP);
	}

	(void) hbnode_lock(L_Node);
	(void) memcpy(L_Node->links_addr, node->links_addr,
	    HB_MAX_PHYS_LINKS * sizeof (struct sockaddr_storage));

	L_Node->node_id = node->node_id;
	L_Node->in = node->in;
	/*
	 * If this node addition is driven by some User-Admin interaction,
	 * retrieve its status from the hb module
	 */
	if (node->node_status.current_state == HB_ST_UNKNOWN) {
		/*
		 * Initialize states values
		 */
		L_Node->node_status.current_state = HB_ST_UNKNOWN;
		L_Node->node_status.previous_state = HB_ST_UNKNOWN;
		for (L_Link = 0; L_Link < link_count; L_Link++) {
			L_Node->links_status[L_Link].current_state =
			    HB_ST_UNKNOWN;
			L_Node->links_status[L_Link].previous_state =
			    HB_ST_UNKNOWN;
		}
		/*
		 * Query actual values to the hb module
		 */
		hbnode_get_status(L_Node);
	} else {
		/*
		 * Initialize states value with one provided
		 */
		L_Node->node_status.current_state =
		    node->node_status.current_state;
		L_Node->node_status.previous_state =
		    node->node_status.previous_state;
		for (L_Link = 0; L_Link < link_count; L_Link++) {
			L_Node->links_status[L_Link].current_state =
			    node->links_status[L_Link].current_state;
			L_Node->links_status[L_Link].previous_state =
			    node->links_status[L_Link].previous_state;
		}

	}

	(void) hbnode_unlock(L_Node);

	return (HB_OK);
}


/* ********************************************************************* */
hb_error_t
hbnode_reset(void)
{
	(void) list_reset(&G_NodesList);

	return (HB_OK);
}

/* ********************************************************************* */
void
hbnode_destroy_list(void)
{
	(void) list_destroy(&G_NodesList);

}

/* ********************************************************************* */
hb_error_t
hbnode_create_list(size_t P_LSize, size_t P_ESize)
{
	hb_error_t status = HB_OK;

	/*
	 * Create an empty list
	 */
	(void) list_init(&G_NodesList, P_LSize, P_ESize);

	return (status);
}

/* ********************************************************************* */

hb_error_t
hbnode_init(hb_callback *cb)
{
	hb_error_t status = HB_OK;

	/*
	 * Initialize the client callback
	 */
	if (!cb) {
		client_indicate = NULL;
	} else {
		client_indicate = cb;
	}

	/*
	 * Initialize the hb module interface
	 */
	status = hbif_init(hbnode_event_handler);
	if (status != HB_OK) {
		fmm_log(LOG_CRIT,
		    "[LIBHB] hb module initialization failed");
		goto terminate;
	}

	/*
	 * Initialize nodes list with the already discovered nodes
	 */
	status = hbnode_create_list(HB_MAX_NODES, sizeof (hb_node_t));
	if (status != HB_OK) {
		fmm_log(LOG_CRIT,
		    "[LIBHB] Nodes list initialization failed");
	}

terminate:
	return (status);
}


static hb_state_t
get_state_from_event(enum hbx_event_type event)
{
	hb_state_t state;

	if ((event == EVT_HOST_UP) || (event == EVT_HOST_DETECTED)) {
		state = HB_ST_UP;
	} else if (event == EVT_HOST_DOWN) {
		state = HB_ST_DOWN;
	} else {
		state = HB_ST_UNKNOWN;
	}

	return (state);
}

/* ********************************************************************* */
static void
hbnode_update_status(hb_node_t *P_node, hbx_event_t P_event)
{
	int link;
	int linkCount = hbargs_get_nic_count();

	if (P_node == NULL) {
		return;
	}

	if (hbnode_lock(P_node) != HB_OK) {
		return;
	}

	/* set new link status */
	P_node->links_status[P_event.id].previous_state =
		P_node->links_status[P_event.id].current_state;
	P_node->links_status[P_event.id].current_state =
		get_state_from_event(P_event.type);

	/* Compute new node status */

	P_node->node_status.previous_state =
	    P_node->node_status.current_state;

	P_node->node_status.current_state = HB_ST_DOWN;
	link = 0;

	for (link = 0; link < linkCount; link++) {
		if (P_node->links_status[link].current_state == HB_ST_UP) {
			P_node->node_status.current_state = HB_ST_UP;
			break;
		}
	}

	/* If DOWN->UP transition, check that the incarnation has changed */
	/* Otherwise refuse the transition */
	if ((P_node->node_status.previous_state == HB_ST_DOWN) &&
	    (P_node->node_status.current_state == HB_ST_UP) &&
	    (P_node->in == P_event.in)) {
		P_node->node_status.current_state = HB_ST_DOWN;
		fmm_log(LOG_ERR,
		    "[LIBHB] Node %d with same incarnation: "
		    "DOWN->UP transition rejected",
		    P_node->node_id);
	}
	/* Update incarnation */
	P_node->in = P_event.in;

	(void) hbnode_unlock(P_node);
}

/* ********************************************************************* */

static void
hbnode_event_handler(hbx_event_t event)
{
	hb_node_t *node;
	hb_node_t new_node;
	uint8_t link_count = hbargs_get_nic_count();
	unsigned int link;

	fmm_log(LOG_DEBUG,
	    "[EVENT] Handling event %s for host \"%x\" \"%d\"",
	    hbx_events[event.type], event.src, event.node_id);

	switch (event.type) {
	case EVT_HOST_DETECTED:
	case EVT_HOST_DOWN:
	case EVT_HOST_UP:

		/*
		 * Check whether this is a known node
		 */
		node = (hb_node_t *)list_get(&G_NodesList, event.node_id);

		if (node != NULL) {
			/*
			 * Update node/link states
			 */
			hbnode_update_status(node, event);

			/*
			 * Send needed indications
			 */
			hbnode_send_link_indication(node, event.id);
			hbnode_send_node_indication(node);
		} else {
			/*
			 * Fill data of the new-node
			 */
			INT_ADDR(new_node.links_addr[event.id]) = event.src;
			new_node.node_id = event.node_id;

			new_node.node_status.current_state =
			    get_state_from_event(event.type);
			new_node.node_status.previous_state = HB_ST_UNKNOWN;
			new_node.in = event.in;

			for (link = 0; link < link_count; link++) {
				if (link != event.id) {
					new_node.links_status[link].
					    current_state = HB_ST_UNKNOWN;
				} else {
					new_node.links_status[event.id].
					    current_state =
					    get_state_from_event(event.type);
				}
				new_node.links_status[link].previous_state =
				    HB_ST_UNKNOWN;
			}

			/*
			 * Add the node to the node list
			 */
			(void) hbnode_add(&new_node);

			/*
			 * Send needed indications
			 */
			hbnode_send_link_indication(&new_node, event.id);
			hbnode_send_node_indication(&new_node);
		}


		break;
	case EVT_DEV_ADDED:
	case EVT_DEV_REMOVED:
	case EVT_HOST_DELAY:
	default:
		break;
	}
}


/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Send the link notification depending on the status.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Param IN:
 *   o P_Node:
 *   o P_Link:
 *
 * Param IN/OUT:
 *   o
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static void
hbnode_send_link_indication(hb_node_t *H_Node, unsigned int H_Link)
{
	hb_indication_t ind;
	uint32_t	reason;

	if (H_Node == NULL) {
		return;
	}

	(void) memset(&ind, 0, sizeof (ind));
	ind.nodeid = H_Node->node_id;
	ind.in = H_Node->in;

	/* Send link indication, if status changed */
	if (H_Node->links_status[H_Link].current_state !=
	    H_Node->links_status[H_Link].previous_state) {
		ind.linkid = H_Link;

		switch (H_Node->links_status[H_Link].current_state) {
		case HB_ST_UP:
			fmm_log(LOG_DEBUG,
			    "[LIBHB] link %d of node %d in %x is UP",
			    H_Link, H_Node->node_id, H_Node->in);
			ind.type =  HB_LINK_RECOVER;
			ind.state = HB_ST_UP;
			reason = CL_REASON_HBR_LINK_UP;
			break;
		case HB_ST_DOWN:
			fmm_log(LOG_DEBUG,
			    "[LIBHB] link %d of node %d in %x is DOWN",
			    H_Link, H_Node->node_id, H_Node->in);
			ind.type =  HB_LINK_FAILURE;
			ind.state = HB_ST_DOWN;
			reason = CL_REASON_HBR_LINK_DOWN;
			break;
		case HB_ST_UNKNOWN:
		default:
			fmm_log(LOG_ERR,
			    "[LIBHB] unexpected state %s in node %d",
			    hb_states[H_Node->links_status[H_Link].
			    current_state],
			    H_Node->node_id);
			return;
		}
		(void) sc_publish_event(ESC_CLUSTER_HBR_LINK_STATE_CHANGE,
		    CL_EVENT_PUB_HBR, CL_EVENT_SEV_INFO, CL_EVENT_INIT_SYSTEM,
		    0, 0, 0, "node_id", SE_DATA_TYPE_UINT32, H_Node->node_id,
		    CL_REASON_CODE, SE_DATA_TYPE_UINT32, reason,
		    "link_id", SE_DATA_TYPE_UINT32, H_Link, NULL);
	}
}


/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Send the node notification depending on the status.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Param IN:
 *   o P_Node:
 *
 * Param IN/OUT:
 *   o
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static void
hbnode_send_node_indication(hb_node_t *H_Node)
{
	hb_indication_t ind;
	hb_evt_type_t   evt_type;

	if (H_Node == NULL) {
		return;
	}

	(void) memset(&ind, 0, sizeof (ind));
	ind.nodeid = H_Node->node_id;
	ind.in = H_Node->in;
	ind.linkid = 0;

	/* send notification if status changed */
	if (H_Node->node_status.current_state !=
	    H_Node->node_status.previous_state) {

		switch (H_Node->node_status.current_state) {
		case HB_ST_UP:
			fmm_log(LOG_DEBUG, "[LIBHB] node %d in %x is UP",
			    H_Node->node_id, H_Node->in);
			ind.type = HB_NODE_RECOVER;
			ind.state = HB_ST_UP;
			evt_type = F_NODE_UP;
			break;
		case HB_ST_DOWN:
			fmm_log(LOG_DEBUG,
			    "[LIBHB] node %d in %x is DOWN",
			    H_Node->node_id, H_Node->in);
			ind.type = HB_NODE_FAILURE;
			ind.state = HB_ST_DOWN;
			evt_type = F_NODE_DOWN;
			break;
		case HB_ST_UNKNOWN:
		default:
			fmm_log(LOG_DEBUG,
			    "[LIBHB] unexpected state %s in node %d",
			    hb_states[H_Node->node_status.current_state],
			    H_Node->node_id);
			return;
		}
		client_indicate(H_Node->node_id, evt_type, H_Node->in);
	}
}
