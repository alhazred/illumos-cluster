/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma	ident	"@(#)hbr.c 1.3     08/05/20 SMI"

#include <fmm/hbr.h>
#include <fmm/hbnode.h>
#include <fmm/hbargs.h>
#include <fmm/hbif.h>
#include <fmm/fmmutils.h>

hb_error_t
hbr_init(hb_callback *cb)
{
	hb_error_t status = HB_OK;

	/*
	 * Initialize config. parameters
	 */
	status = hbargs_init();
	if (status != HB_OK) {
		fmm_log(LOG_CRIT,
		    "libhbr: HB parameters initialization failed");
		status = HB_ENOTSUP;
		goto terminate;
	}

	/*
	 * Initialize nodes database + hb module
	 */
	if (hbnode_init(cb) != 0) {
		fmm_log(LOG_CRIT,
		    "libhbr: Nodes db initialization failed");
		status = HB_ENOTSUP;
		goto terminate;
	}

terminate:
	return (status);
}


hb_error_t
hbr_start(void)
{
	hb_error_t status = HB_OK;
	hbx_params_t hb_params;

	/*
	 * Setup hbdrv parameters and activate the module
	 */
	status = hbargs_get_heartbeat_params(&hb_params);
	if (status != HB_OK) {
		fmm_log(LOG_ERR,
		    "libhbr: hbargs_get_heartbeat_params error %d",
		    status);
		goto terminate;
	}
	status = hbif_set_params(&hb_params);
	if (status != HB_OK) {
		fmm_log(LOG_ERR,
		    "libhbr: hbif_set_params error %d",
		    status);
		goto terminate;
	}

	/*
	 * Start hbdrv emission/reception loop (rcv_mode on)
	 */
	status = hbif_start(HBX_RCV_MODE_ON);
	if (status != HB_OK) {
		fmm_log(LOG_WARNING,
		    "libhbr: Cannot start hb emission/reception");
		goto terminate;
	}

	/*
	 * Get initial node states
	 */
	hbnode_set_status();

terminate:
	return (status);
}

hb_error_t
hbr_stop(void)
{
	hb_error_t status = HB_OK;

	/*
	 * Stop hbdrv emission/reception loop
	 */
	status = hbif_stop();
	if (status != HB_OK) {
		fmm_log(LOG_WARNING,
		    "libhbr: Cannot stop hb emission/reception");
		goto terminate;
	}

terminate:
	return (status);
}

hb_error_t
hbr_reset(void)
{
	hb_error_t status = HB_OK;

	/*
	 * Stop hbdrv and reset parameters
	 */
	status = hbif_reset();
	if (status != HB_OK) {
		fmm_log(LOG_WARNING,
		    "libhbr: Cannot reset hb emission/reception");
		goto terminate;
	}

terminate:
	return (status);
}

hb_error_t
hbr_delete_node(hb_nodeid_t nodeid)
{
	hb_error_t status;

	status = hbnode_delete_node(nodeid);
	if (status != HB_OK) {
		fmm_log(LOG_WARNING, "libhbr: Cannot delete node %d", nodeid);
	}

	return (status);
}
