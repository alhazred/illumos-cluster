#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#
#ident	"@(#)Makefile.com	1.3	08/05/20 SMI"
#
# lib/libhbr/Makefile.com
#

LIBRARY= libhbr.a
VERS=.1

include ../../../Makefile.master

OBJECTS += hbr.o hbnode.o hblist.o

# include library definitions
include ../../Makefile.lib

MAPFILE=	../common/mapfile-vers
SRCS=		$(OBJECTS:%.o=../common/%.c)

LIBS =		$(DYNLIB)

LINTFLAGS +=	-I..
LINTFILES +=	$(OBJECTS:%.o=%.ln)

MTFLAG = -mt
CPPFLAGS 	+=	-I$(SRC)/common/cl
CPPFLAGS	+=	-DEUROPA
CPPFLAGS +=	-v -DSolaris
PMAP =	-M $(MAPFILE)

LDLIBS += -lc -lhb -lclevent -lfmmutils

.KEEP_STATE:

$(DYNLIB):	$(MAPFILE)

CHECKHDRS=hblist.h

# include library targets
include ../../Makefile.targ

objs/%.o pics/%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<
