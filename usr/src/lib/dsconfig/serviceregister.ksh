#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

# ident	"@(#)serviceregister.ksh	1.10	08/05/20 SMI"
#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.

#
# serviceregister.ksh
#

# Program name and args list
typeset -r PROG=${0##*/}
typeset -r ARGS=$*
typeset CP=/usr/bin/cp
typeset RM=/usr/bin/rm
typeset BASENAME=/usr/bin/basename

typeset -x TEXTDOMAIN=SUNW_SC_CMD
typeset -x TEXTDOMAINDIR=/usr/cluster/lib/locale
typeset SC_BASEDIR=${SC_BASEDIR:-}
typeset SERVICEREGISTRY=${SC_BASEDIR}/opt/cluster/lib/ds/registry
typeset CACAOADM=/usr/lib/cacao/bin/cacaoadm

typeset svcdef_file=			# [-f|-r] <service_definition_file_path>[,<service_definition_file_path>,...] 

ZONECMD=/sbin/zonename
IS_NONGLOBAL=0

# detect whether we are running in a global or non-global zone
if [ "${SUNW_PKG_INSTALL_ZONENAME}" = "" ] ; then
	if [ -x ${ZONECMD} ]; then
		if [ "`${ZONECMD}`" != "global" ]; then
			IS_NONGLOBAL=1
		fi
	fi
else
	if [ "${SUNW_PKG_INSTALL_ZONENAME}" != "global" ] ; then
		IS_NONGLOBAL=1
	fi
fi

# If installing into a non-global zone, simply exit
if [ ${IS_NONGLOBAL} -eq 1 ] ; then
	exit 0
fi 

#####################################################
#
# print_usage()
#
#       Print usage message to stderr
#
#####################################################
print_usage()
{
	echo "$(gettext 'usage'):  ${PROG} [-f|-r] <service_definition_file_path>[,<service_definition_file_path>,...]" >&2

	echo >&2

	return 0
}

#####################################################
#
# reload_module()
#
#	Reloads the dataservice module
#
#####################################################
reload_module()
{
	if [ -f ${CACAOADM} ]; then
		${CACAOADM} lock com.sun.cluster.dataservices >/dev/null 2>&1
		if [ $? -ne 0 ]; then
			${CACAOADM} restart >/dev/null 2>&1
			return 0
		fi
		${CACAOADM} unlock com.sun.cluster.dataservices >/dev/null 2>&1
		if [ $? -ne 0 ]; then
			${CACAOADM} restart >/dev/null 2>&1
			return 0
		fi
	fi
}

#####################################################
#
# do_register()
#
#       Registers the service with the Data Service Configuration wizards
#
#####################################################
do_register()
{
	if [[ "${svcdef_file}" == "" ]]; then
		print_usage
		return 1
	fi

	typeset files="$(IFS=, ; echo ${svcdef_file})"
	for file in ${files}
	do
		${CP} ${file} ${SERVICEREGISTRY}
	done
	if [ "$PKG_INSTALL_ROOT" = "/" ] || [ "$PKG_INSTALL_ROOT" = "" ]
	then
		reload_module || return 1
	fi
}

#####################################################
#
# do_unregister()
#
#       Unregisters the service with the Data Service Configuration wizards
#
#####################################################
do_unregister()
{
	if [[ "${svcdef_file}" == "" ]]; then
		print_usage
		return 1
	fi

	typeset files="$(IFS=, ; echo ${svcdef_file})"
        for file in ${files}
        do
		file_name=`${BASENAME} ${file}`
		${RM} -f ${SERVICEREGISTRY}/${file_name}
	done
	if [ "$PKG_INSTALL_ROOT" = "/" ] || [ "$PKG_INSTALL_ROOT" = "" ]
	then
		reload_module || return 1
	fi
}

#####################################################
#
# Main
#
#####################################################
main() {
	while getopts f:r: c 2>/dev/null
	do
		case ${c} in
			  f)	svcdef_file=${OPTARG};
				do_register;
				break;
				;;

			  r)	svcdef_file=${OPTARG};
				do_unregister;
				break;
				;;

			  *)	# other options
				print_usage
				return 1
				 ;;
		esac
	done
	return 0
}

main $*
exit 0
