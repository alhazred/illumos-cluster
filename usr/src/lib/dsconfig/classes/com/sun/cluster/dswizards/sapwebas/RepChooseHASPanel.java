/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)RepChooseHASPanel.java 1.12     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.sapwebas;

// J2SE
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

// JMX
import javax.management.remote.JMXConnector;

// CMASS
import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.sapwebas.SapWebasMBean;
import com.sun.cluster.agent.dataservices.utils.DataServicesUtil;
import com.sun.cluster.agent.dataservices.utils.InfrastructureMBean;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.common.TrustedMBeanModel;

// CLI Wizard SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;

// Wizard Common
import com.sun.cluster.dswizards.common.CliStorageSelectionPanel;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.hastorageplus.HASPWizardConstants;

public class RepChooseHASPanel extends CliStorageSelectionPanel {

    public RepChooseHASPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(name, model, wizardFlow, wizardi18n);
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {
        desc = wizardi18n.getString("sap.chooseHASPanel.rep.query");

        boolean singleSelection = false;
        super.consoleInteraction(singleSelection);

        wizardModel.setWizardValue(SapWebasConstants.NEW_HASP_CS,
            wizardModel.getWizardValue(Util.CREATE_NEW));
        wizardModel.setWizardValue(Util.REP_SELECTED_STORAGE,
            wizardModel.getWizardValue(Util.SEL_RS));
        wizardModel.setWizardValue(Util.REP_SELECTED_FS,
            wizardModel.getWizardValue(Util.SEL_FILESYSTEMS));
        wizardModel.setWizardValue(Util.REP_SELECTED_GD,
            wizardModel.getWizardValue(Util.SEL_DEVICES));
        wizardModel.setWizardValue(Util.REP_VFSTAB_ENTRIES,
            wizardModel.getWizardValue(Util.HASP_ALL_FILESYSTEMS));

    }

    protected void refreshRGList() {
        TTYDisplay.clear(2);

        Thread t = TTYDisplay.busy(wizardi18n.getString(
                    "storageSelectionPanel.discovery.busy"));
        t.start();

        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);
        JMXConnector connector = getConnection(nodeEndPoint);
        SapWebasMBean mBean = SapWebasWizardCreator.getMBean(connector);
        HashMap values = mBean.discoverPossibilities(
                new String[] { Util.STORAGE_RGLIST }, null);
        List retList = (List) values.get(Util.STORAGE_RGLIST);

        try {
            t.interrupt();
            t.join();
        } catch (Exception e) {

        }

        wizardModel.setWizardValue(Util.STORAGE_RGLIST, retList);
    }

}
