/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)RACInvokerConstants.java	1.8	08/07/14 SMI"
 */

package com.sun.cluster.dswizards.oraclerac.invoker;

/**
 * Constants for OracleRAC Invoker Wizard
 */
public class RACInvokerConstants {

    // Flow XML
    public static final String RAC_FLOW_XML =
        "/usr/cluster/lib/ds/oraclerac/invoker/RACWizardsFlow.xml";
    public static final String RAC_PKG_NAME = "SUNWscor";

    // Constants for Wizard Flow
    public static final String CLUSTER_SEL_PANEL = "ClusterSelectionPanel";
    public static final String INVOKERPANEL = "InvokerPanel";
    public static final String FRAMEWORK_WIZ = "FrameworkWizard";
    public static final String STORAGE_WIZ = "StorageWizard";
    public static final String DATABASE_WIZ = "DatabaseWizard";
    public static final String WIZ_TYPE = "WizardType";
    public static final String FRAMEWORK = "0";
    public static final String STORAGE = "1";
    public static final String DATABASE = "2";
    public static final String RAC_STATE_FILE =
        "/opt/cluster/lib/ds/history/RacStateFile";

    /**
     * Creates a new instance of RACInvokerConstants
     */
    public RACInvokerConstants() {
    }
}
