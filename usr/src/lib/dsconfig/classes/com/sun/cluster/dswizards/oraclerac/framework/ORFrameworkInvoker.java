/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ORFrameworkInvoker.java 1.5     08/05/20 SMI"
 */

package com.sun.cluster.dswizards.oraclerac.framework;

// Java

// CMASS
import com.sun.cacao.agent.JmxClient;

import com.sun.cluster.agent.dataservices.oraclerac.OracleRACMBean;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.common.TrustedMBeanModel;
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardComposite;
import com.sun.cluster.dswizards.clisdk.core.WizardCreator;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.logicalhost.HostnameEntryPanel;
import com.sun.cluster.dswizards.logicalhost.IPMPGroupEntryPanel;
import com.sun.cluster.dswizards.logicalhost.LogicalHostWizardConstants;
import com.sun.cluster.dswizards.oraclerac.database.RacDBWizardConstants;
import com.sun.cluster.dswizards.oraclerac.framework.ORFIntroPanel;
import com.sun.cluster.dswizards.oraclerac.framework.ORFNodeSelectionPanel;
import com.sun.cluster.dswizards.oraclerac.framework.ORFResultsPanel;
import com.sun.cluster.dswizards.oraclerac.framework.ORFReviewPanel;
import com.sun.cluster.dswizards.oraclerac.framework.ORFStorageSelectionPanel;
import com.sun.cluster.dswizards.oraclerac.framework.ORFSummaryPanel;
import com.sun.cluster.dswizards.oraclerac.framework.ORFWizardConstants;
import com.sun.cluster.dswizards.oraclerac.invoker.RACInvokerConstants;

import java.io.IOException;

import java.text.MessageFormat;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.management.remote.JMXConnector;


/**
 * This class holds the Wizard Panel Tree information for all the three
 * RACWizards The Wizard lets the user invoke the three RAC Wizards
 */
public class ORFrameworkInvoker extends WizardCreator
    implements RacDBWizardConstants {

    private static final String FLOW_XML =
        ORFWizardConstants.ORF_WIZARD_FLOW_XML;
    private static final String STATE_FILE = RACInvokerConstants.RAC_STATE_FILE;

    /**
     * Default Constructor Creates ORS Wizard from the State File. The Wizard is
     * intialized to a particular state using the State file. The Wizard flow is
     * governed by the Flow XML.
     */
    public ORFrameworkInvoker() {
        super(STATE_FILE, FLOW_XML);
    }

    /**
     * Creates the actual Panel Tree Structure for the ORS Wizard
     */
    protected void createClientTree() {

        // Get the wizard root
        WizardComposite wizardRoot = getRoot();

        // RAC Framework Panels

        ORFIntroPanel orfIntroPanel = new ORFIntroPanel(
                ORFWizardConstants.PANEL_0, wizardModel, wizardFlow,
                wizardi18n);
        ORFNodeSelectionPanel orfNodeSelectionPanel = new ORFNodeSelectionPanel(
                ORFWizardConstants.PANEL_1, wizardModel, wizardFlow,
                wizardi18n);

        ORFStorageSelectionPanel orfStorageSelectionPanel =
            new ORFStorageSelectionPanel(ORFWizardConstants.PANEL_2,
                wizardModel, wizardFlow, wizardi18n);

        ORFReviewPanel orfReviewPanel = new ORFReviewPanel(
                ORFWizardConstants.PANEL_3, wizardModel, wizardFlow,
                wizardi18n);

        ORFSummaryPanel orfSummaryPanel = new ORFSummaryPanel(
                ORFWizardConstants.PANEL_4, wizardModel, wizardFlow,
                wizardi18n);

        ORFResultsPanel orfResultsPanel = new ORFResultsPanel(
                ORFWizardConstants.PANEL_5, wizardModel, wizardFlow,
                wizardi18n);

        // Each Panel is associated with a Name, the wizardModel from
        // the WizardCreator and the WizardFlow Structure.
        // Add the Panels to the Root

        // Framework Panels
        wizardRoot.addChild(orfIntroPanel);
        wizardRoot.addChild(orfNodeSelectionPanel);
        wizardRoot.addChild(orfStorageSelectionPanel);
        wizardRoot.addChild(orfReviewPanel);
        wizardRoot.addChild(orfSummaryPanel);
        wizardRoot.addChild(orfResultsPanel);

    }

    /**
     * Get the Handle to the RACBean MBean on a specified node
     *
     * @return  Handle to the RACBean MBean on a specified node
     */
    public static OracleRACMBean getOracleRACMBeanOnNode(
        JMXConnector connector) {

        // Only if there is no existing reference
        // Get Handler to OracleRACMBean
        OracleRACMBean mbeanOnNode = null;

        try {
            TTYDisplay.initialize();
            mbeanOnNode = (OracleRACMBean) TrustedMBeanModel.getMBeanProxy(
                    Util.DOMAIN, connector, OracleRACMBean.class, null, false);
        } catch (Exception e) {

            // Report Error that the user has to install
            // The Dataservice Package or upgrade it
            String errorMsg = pkgErrorString;
            MessageFormat msgFmt = new MessageFormat(errorMsg);
            errorMsg = msgFmt.format(
                    new String[] { RACInvokerConstants.RAC_PKG_NAME });
            TTYDisplay.printError(errorMsg);
            TTYDisplay.promptForEntry(wizardExitPrompt);
            System.exit(1);
        }

        return mbeanOnNode;
    }

    /**
     * Main function of the Wizards
     */
    public static void main(String args[]) {

        // Start the ORS Wizard
        ORFrameworkInvoker RACInovker = new ORFrameworkInvoker();
    }
}
