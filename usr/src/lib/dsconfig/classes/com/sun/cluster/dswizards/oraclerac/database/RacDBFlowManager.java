/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)RacDBFlowManager.java 1.16     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.oraclerac.database;

import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.dswizards.common.DSConfigWizardModel;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.DSWizardInterface;
import com.sun.cluster.dswizards.common.WizardI18N;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * The class takes decision on which panel to show "next" depending upon the
 * conditions set at a particular context.
 */
public class RacDBFlowManager implements DSWizardInterface,
    RacDBWizardConstants {

    /**
     * Creates a new instance of RacDBFlowManager
     */
    public RacDBFlowManager() {
    }

    /**
     * @see  com.sun.cluster.dswizards.common.DSWizardInterface#getNextPanel
     */
    public String getNextPanel(String curPanelName,
        Object wizCxtObj, String options[]) {

	DSConfigWizardModel guiModel = null;
	CliDSConfigWizardModel cliModel = null;

	if (wizCxtObj instanceof CliDSConfigWizardModel) {
	    cliModel = (CliDSConfigWizardModel)wizCxtObj;
	} else if (wizCxtObj instanceof DSConfigWizardModel) {
	    guiModel = (DSConfigWizardModel)wizCxtObj;
	}

        // if on version selection panel
        if (curPanelName.equals(PANEL_1)) {

            // get the user preference
            String stVersion = null;
	    if (guiModel != null) {
		stVersion = (String)guiModel.getWizardValue(
                    Util.RAC_VER);
	    } else {
		stVersion = (String)cliModel.getWizardValue(
                    Util.RAC_VER);
	    } 

            if (stVersion.equals(Util.NINE)) {
                return options[0];
            } else {
                return options[1];
            }
        } else if (curPanelName.equals(PANEL_3)) {
	    // if on Oracle Home Panel

            // get the user preference
            Integer preference = null;
	    if (guiModel != null) {
		preference = (Integer)guiModel.getWizardValue(
                    Util.DB_COMPONENTS);
	    } else {
		preference = (Integer)cliModel.getWizardValue(
                    Util.DB_COMPONENTS);
	    }

            int prefInt = preference.intValue();

            if ((prefInt == Util.SERVER) || (prefInt == Util.BOTH_S_L)) {

                // go to Oracle Sid selection panel
                return options[0];
            } else if (prefInt == Util.LISTENER) {
                return options[1];
            }
        } else if (curPanelName.equals(PANEL_5)) {
            Boolean isNodeComplete = null;
            Integer preference = null;
            Object tmpObj = null;

	    if (guiModel != null) {
		tmpObj = guiModel.getWizardValue(ORAC_PROPS_ENTRY);
            	isNodeComplete = (Boolean)guiModel.getWizardValue(
                    Util.RAC_NODELIST_COMPLETE);
		preference = (Integer)guiModel.getWizardValue(
                    Util.DB_COMPONENTS);
	    } else {
		tmpObj = cliModel.getWizardValue(ORAC_PROPS_ENTRY);
            	isNodeComplete = (Boolean)cliModel.getWizardValue(
                    Util.RAC_NODELIST_COMPLETE);
		preference = (Integer)cliModel.getWizardValue(
                    Util.DB_COMPONENTS);
	    }

            if (tmpObj != null) {
                ArrayList tmpMap = (ArrayList) tmpObj;

                if (!tmpMap.isEmpty()) {
                    return options[3];
                }
            }

            int prefInt = preference.intValue();

            if ((isNodeComplete == null) || isNodeComplete.booleanValue()) {

                if (prefInt == Util.SERVER) {

                    if (selectedHWRaid(wizCxtObj)) {

                        // the storage selection panel can be skipped
                        // move onto review panel
                        return options[5];
                    } else {

                        // go to storage selection panel
                        return options[2];
                    }
                } else if ((prefInt == Util.LISTENER) ||
                        (prefInt == Util.BOTH_S_L)) {
                    return options[1];
                }
            } else {

                if ((prefInt == Util.SERVER) || (prefInt == Util.BOTH_S_L)) {

                    // go to Sid panel to get next Nodes' value
                    return options[0];
                } else if (prefInt == Util.LISTENER) {

                    // go to same panel to get next Nodes' value
                    return options[4];
                }
            }
        } else if (curPanelName.equals(PANEL_6)) {
            String create_hasp = null;
	    if (guiModel != null) {
		create_hasp = (String)guiModel.getWizardValue(
                    CREATE_HASP_RS);
	    } else {
		create_hasp = (String)cliModel.getWizardValue(
                    CREATE_HASP_RS);
	    }

            if (create_hasp.equals(CREATE_HASP_RS)) {
                return options[0];
            } else {
                return options[1];
            }
        } else if (curPanelName.equals(PANEL_7)) {

            String wizardType = null;
	    if (guiModel != null) {
		wizardType = (String)guiModel.getWizardValue(
                    WIZARD_TYPE);
	    } else {
		wizardType = (String)cliModel.getWizardValue(
                    WIZARD_TYPE);
	    }

            if ((wizardType != null) && wizardType.equals(GUI_WIZARD)) {
                return options[4];
            }

            String create_lh = null;
	    if (guiModel != null) {
		create_lh = (String)guiModel.getWizardValue(
                    CREATE_LH_RS);
	    } else {
		create_lh = (String)cliModel.getWizardValue(
                    CREATE_LH_RS);
	    }

            if (create_lh.equals(CREATE_LH_RS)) {
                return options[0];
            } else {
                Boolean isNodeComplete = null;
		if (guiModel != null) {
		    isNodeComplete = (Boolean)guiModel.getWizardValue(
                        Util.RAC_NODELIST_COMPLETE);
		} else {
		    isNodeComplete = (Boolean)cliModel.getWizardValue(
                        Util.RAC_NODELIST_COMPLETE);
		}

                if ((isNodeComplete == null) || isNodeComplete.booleanValue()) {
                    Integer preference = null;
		    if (guiModel != null) {
			preference = (Integer)guiModel.getWizardValue(
                            Util.DB_COMPONENTS);
		    } else {
			preference = (Integer)cliModel.getWizardValue(
                            Util.DB_COMPONENTS);
		    }
                    int prefInt = preference.intValue();

                    if ((prefInt == Util.SERVER) ||
                            (prefInt == Util.BOTH_S_L)) {

                        // check if storage selection panel can be skipped
                        if (selectedHWRaid(wizCxtObj)) {

                            // skip storage selection panel
                            return options[3];
                        } else {
                            return options[2];
                        }
                    } else if (prefInt == Util.LISTENER) {
                        return options[3];
                    }

                    return options[2];
                } else {
                    return options[1];
                }
            }
        } else if (curPanelName.equals(PANEL_11)) {

            if (selectedHWRaid(wizCxtObj)) {

                // skip storage selection panel
                return options[1];
            } else {
                return options[0];
            }
        } else if (curPanelName.equals(VER10G_PANEL_0)) {
            boolean crsFrameworkPresent = false;
	    if (guiModel != null) {
		crsFrameworkPresent = 
		    ((Boolean)guiModel.getWizardValue(
		    Util.CRS_FRAMEWORK_PRESENT)).booleanValue();
	    } else {
		crsFrameworkPresent = 
		    ((Boolean)cliModel.getWizardValue(
		    Util.CRS_FRAMEWORK_PRESENT)).booleanValue();
		}

            if (selectedHWRaid(wizCxtObj) || crsFrameworkPresent) {

                // skip CRS storage choice panel
                return options[1];
            } else {
                return options[0];
            }
        } else if (curPanelName.equals(VER10G_PANEL_0_1)) {
            String storageChoice = null;
	    if (guiModel != null) {
		storageChoice = (String)guiModel.getWizardValue(
                    Util.CRS_STORAGE_CHOICE);
	    } else {
		storageChoice = (String)cliModel.getWizardValue(
                    Util.CRS_STORAGE_CHOICE);
	    }

            if (storageChoice.equals(Util.SELECT_EXISTING)) {

                // go to crs storage selection panel
                return options[0];
            } else {
                return options[1];
            }
        } else if (curPanelName.equals(VER10G_PANEL_3)) {
            String wizardType = null;
	    if (guiModel != null) {
		wizardType = (String)guiModel.getWizardValue(
                    WIZARD_TYPE);
	    } else {
		wizardType = (String)cliModel.getWizardValue(
                    WIZARD_TYPE);
	    }

            if ((wizardType != null) && wizardType.equals(GUI_WIZARD)) {

                // check if storage selection panel needs to be skipped
                if (selectedHWRaid(wizCxtObj)) {

                    // skip storage selection panel
                    return options[2];
                } else {
                    return options[1];
                }
            }

            // sid selection panel
            Boolean isNodeComplete = null;
	    if (guiModel != null) {
		isNodeComplete = (Boolean)guiModel.getWizardValue(
                    Util.RAC10G_NODELIST_COMPLETE);
	    } else {
		isNodeComplete = (Boolean)cliModel.getWizardValue(
                    Util.RAC10G_NODELIST_COMPLETE);
	    } 

            if ((isNodeComplete == null) || isNodeComplete.booleanValue()) {

                if (selectedHWRaid(wizCxtObj)) {

                    // skip storage selection panel
                    return options[2];
                } else {
                    return options[1];
                }
            } else {
                return options[0];
            }
        } else if (curPanelName.equals(PANEL_10)) {
            ArrayList propsEntryData = null;
            Integer ctr = null;

	    if (guiModel != null) {
		propsEntryData = (ArrayList)guiModel.getWizardValue(
                    ORAC_PROPS_ENTRY);
		ctr = (Integer)guiModel.getWizardValue(
                    ORAC_PROPS_ENTRY_CTR);
	    } else {
		propsEntryData = (ArrayList)cliModel.getWizardValue(
                    ORAC_PROPS_ENTRY);
		ctr = (Integer)cliModel.getWizardValue(
                    ORAC_PROPS_ENTRY_CTR);
	    }

            int ctrVal = ctr.intValue();

            if (ctrVal < propsEntryData.size()) {
                return options[2];
            }

            Integer preference = null;
	    if (guiModel != null) {
		preference = (Integer)guiModel.getWizardValue(
                    Util.DB_COMPONENTS);
	    } else {
		preference = (Integer)cliModel.getWizardValue(
                    Util.DB_COMPONENTS);
	    }
            int prefInt = preference.intValue();

            if (prefInt == Util.SERVER) {

                // check if storage selection panel can be skipped
                if (selectedHWRaid(wizCxtObj)) {

                    // skip storage selection panel and move onto review panel
                    return options[3];
                } else {
                    return options[1];
                }
            } else if ((prefInt == Util.LISTENER) ||
                    (prefInt == Util.BOTH_S_L)) {
                return options[0];
            }
        }

        return null;
    }

    private boolean selectedHWRaid(Object wizCxtObj) {
	DSConfigWizardModel guiModel = null;
	CliDSConfigWizardModel cliModel = null;
	if (wizCxtObj instanceof DSConfigWizardModel) {
	    guiModel = (DSConfigWizardModel)wizCxtObj;
	} else if (wizCxtObj instanceof CliDSConfigWizardModel) {
	    cliModel = (CliDSConfigWizardModel)wizCxtObj;
	}

        List storageScheme = null;
	if (guiModel != null) {
	    storageScheme = (ArrayList)guiModel.getWizardValue(
                Util.ORF_STORAGE_MGMT_SCHEMES);
	} else {
	    storageScheme = (ArrayList)cliModel.getWizardValue(
                Util.ORF_STORAGE_MGMT_SCHEMES);
	}

        // storageScheme should never be null
        if ((storageScheme != null) && (storageScheme.size() == 1) &&
                storageScheme.contains(Util.HWRAID)) {

            // user has selected Hardware RAID without VM option
            return true;
        }

        return false;
    }
}
