/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)SapResultsPanel.java 1.13     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.sapwebas;

// J2SE
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

// JMX
import javax.management.remote.JMXConnector;

// CMAS
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.dataservices.sapwebas.SapWebasMBean;
import com.sun.cluster.agent.dataservices.utils.DataServicesUtil;
import com.sun.cluster.agent.dataservices.utils.Util;

// CLI Wizard SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;

// Wizard Common
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;

public class SapResultsPanel extends WizardLeaf {

    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N wizardi18n; // Message Bundle
    private JMXConnector localConnection = null; // Reference to JMXConnector
    private int configStatus = Util.NOTCONFIGURED;

    /**
     * Creates a new instance of SapResultsPanel
     */
    public SapResultsPanel() {
        super();
    }

    /**
     * Creates a SapResultsPanel Panel with the given name and tree manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public SapResultsPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    public SapResultsPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;

        // Get the I18N instance
        this.wizardi18n = wizardi18n;

        if (localConnection == null) {
            localConnection = getConnection(Util.getClusterEndpoint());
        }
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {
        String title = wizardi18n.getString("sap.sapResultsPanel.title");
        String desc = wizardi18n.getString("sap.sapResultsPanel.desc");
        String help_text = wizardi18n.getString("sap.sapResultsPanel.help.cli");
        String yes = wizardi18n.getString("cliwizards.yes");
        String no = wizardi18n.getString("cliwizards.no");
        String continue_txt = wizardi18n.getString("cliwizards.continue");

        DataServicesUtil.clearScreen();

        // Title
        TTYDisplay.clear(1);
        TTYDisplay.printTitle(title);
        TTYDisplay.clear(1);
        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        HashMap confMap = (HashMap) wizardModel.getWizardValue(Util.SAPRESULT);
        ArrayList results = generateCommands(confMap);
        Boolean existStatus = (Boolean) wizardModel.getWizardValue(
                Util.EXITSTATUS);
        boolean success = existStatus.booleanValue();

        String response = null;

        if ((results != null) && (results.size() > 0)) {
            TTYDisplay.clear(1);
            TTYDisplay.printSubTitle(wizardi18n.getString(
                    "cliwizards.executionMessage"));

            Iterator i = results.iterator();

            while (i.hasNext()) {
                String command = (String) i.next();

                if (command.equals("") && (!success)) {
                    TTYDisplay.clear(1);

                    if (configStatus == Util.FAILED_ROLLBACKED) {
                        TTYDisplay.printSubTitle(wizardi18n.getString(
                                "cliwizards.rollbackExecutionMessage"));
                    } else if (configStatus == Util.ROLLBACK_FAILED) {

                        // Rollback failed due to some exception while
                        // executing rollback commands
                        TTYDisplay.printSubTitle(wizardi18n.getString(
                                "cliwizards.rollbackUnableMessage"));
                    }

                    TTYDisplay.clear(1);

                    continue;
                }

                TTYDisplay.showText(command, 4);
                TTYDisplay.clear(1);
            }

            TTYDisplay.clear(1);

            if (success) {
                TTYDisplay.printSubTitle(wizardi18n.getString(
                        "sap.resultsPanel.successGenerateCommand"));
                wizardModel.setWizardValue(Util.WIZARD_STATE, null);
                TTYDisplay.clear(1);
                TTYDisplay.promptForEntry(wizardi18n.getString(
                        "cliwizards.results.success"));
            } else {
                TTYDisplay.printSubTitle(wizardi18n.getString(
                        "sap.resultsPanel.failureGenerateCommand"));
                TTYDisplay.clear(1);

                if (configStatus == Util.FAILED_ROLLBACKED) {
                    response = TTYDisplay.promptForEntry(wizardi18n.getString(
                                "cliwizards.results.failed"));
                } else {
                    TTYDisplay.promptForEntry(wizardi18n.getString(
                            "cliwizards.results.success"));
                }
            }

        } else {
            response = TTYDisplay.promptForEntry(wizardi18n.getString(
                        "cliwizards.results.failed"));
        }

        if (!success && (response != null) &&
                response.equals(wizardi18n.getString("ttydisplay.back.char"))) {
            this.cancelDirection = true;

            return;
        }

        // Write the current context to the state file
        try {

            // Clear the command list and exit status
            wizardModel.setWizardValue(Util.EXITSTATUS, null);
            wizardModel.setWizardValue(Util.CMDLIST, null);
            wizardModel.writeToStateFile(localConnection
                .getMBeanServerConnection());
        } catch (Exception e) {
            TTYDisplay.printError(wizardi18n.getString(
                    "dswizards.stateFile.error"));
        } finally {

            // close connection
            closeConnections();
        }
    }

    private ArrayList generateCommands(Map confMap) {
        ArrayList results = new ArrayList();
        SapWebasMBean mBean = null;
        Thread t = null;

        try {
            t = TTYDisplay.busy(wizardi18n.getString(
                        "cliwizards.executingMessage"));
            t.start();

            String nodeEndPoint = (String) wizardModel.getWizardValue(
                    Util.NODE_TO_CONNECT);
            JMXConnector connector = getConnection(nodeEndPoint);
            mBean = SapWebasWizardCreator.getMBean(connector);

            List commandList = mBean.generateCommands(confMap);

            try {
                t.interrupt();
                t.join();
            } catch (Exception exe) {
            }

            if (commandList != null) {
                Iterator i = commandList.iterator();

                while (i.hasNext()) {
                    String command = (String) i.next();
                    results.add(command);
                }
            }

            wizardModel.setWizardValue(Util.EXITSTATUS, Boolean.TRUE);
            configStatus = mBean.getConfigurationStatus();
        } catch (CommandExecutionException cee) {

            try {
                t.interrupt();
                t.join();
            } catch (Exception exe) {
            }

            wizardModel.setWizardValue(Util.EXITSTATUS, Boolean.FALSE);

            List commandList = null;

            if (mBean != null) {
                commandList = mBean.getCommandList();
                configStatus = mBean.getConfigurationStatus();
            }

            if (commandList != null) {
                Iterator i = commandList.iterator();

                while (i.hasNext()) {
                    String command = (String) i.next();
                    results.add(command);
                }
            }

            String errorMessage = wizardi18n.getString(
                    "cliwizards.executionErrorMessage");
        }

        return results;
    }


    private void closeConnections() {
        String nodeList[] = (String[]) wizardModel.getWizardValue(
                Util.RG_NODELIST);
        super.closeConnections(nodeList);
    }
}
