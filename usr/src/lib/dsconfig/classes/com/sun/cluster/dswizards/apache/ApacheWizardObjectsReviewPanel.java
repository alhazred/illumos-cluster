/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ApacheWizardObjectsReviewPanel.java 1.9     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.apache;

// J2SE


// CLI SDK
import com.sun.cluster.agent.dataservices.apache.Apache;
import com.sun.cluster.agent.dataservices.apache.ApacheMBean;
import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.utils.DataServicesUtil;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.rgm.ResourceMBean;
import com.sun.cluster.common.TrustedMBeanModel;
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;

// Wizard Common
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.management.remote.JMXConnector;


/**
 * Panel to Review Sun Cluster Objects for Apache Configuration
 */
public class ApacheWizardObjectsReviewPanel extends WizardLeaf {

    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N messages; // Message Bundle
    private ApacheMBean mBean = null; // Reference to MBean
    private JMXConnector localConnection = null; // Reference to JMXConnector

    private ArrayList assignedRgNames = null;
    private ArrayList assignedResNames = null;

    /**
     * Creates a new instance of ApacheWizardObjectsReviewPanel
     */
    public ApacheWizardObjectsReviewPanel() {
        super();
    }

    /**
     * Creates an ApacheWizardObjectsReview Panel with the given name and tree
     * manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public ApacheWizardObjectsReviewPanel(String name,
        WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates a ApacheWizardObjectsReview Panel with the given name and
     * associates it with the WizardModel, WizardFlow and i18n reference
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     * @param  messages  WizardI18N
     */
    public ApacheWizardObjectsReviewPanel(String name,
        CliDSConfigWizardModel model, WizardFlow wizardFlow, WizardI18N messages) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;

        // Get the I18N instance
        this.messages = messages;

        localConnection = getConnection(Util.getClusterEndpoint());
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {

        // Prefetch Strings
        String scTableTitle = messages.getString("hasp.summarypanel.desc2");

        // String for SunCluster objects screen
        String sctitle = messages.getString("lhwizard.wizard.step5.title");
        String scinstruction = messages.getString(
                "lhwizard.wizard.step5.instruction");
        String schelp = messages.getString("apache.reviewPanel.help.2");
        String apacheResourceGroup = messages.getString(
                "apache.reviewPanel.apacheResourceGroup");
        String apacheResource = messages.getString(
                "apache.reviewPanel.apacheResource");
        String storageResource = messages.getString(
                "apache.reviewPanel.storageResource");
        String shGroup = messages.getString("apache.reviewPanel.shGroup");
        String lhResource = messages.getString(
                "apache.reviewPanel.networkResource");
        String shResource = lhResource;
        String heading1 = messages.getString(
                "hasp.create.filesystem.review.heading1");
        String heading2 = messages.getString(
                "hasp.create.filesystem.review.heading2");
        String dchar = messages.getString("ttydisplay.d.char");
        String schar = messages.getString("ttydisplay.s.char");
        String done = messages.getString("ttydisplay.menu.done");
        String portQueryText = messages.getString(
                "apache.reviewPanel.portQuery");
        String continueText = messages.getString("cliwizards.continue");
        String cliDesc = messages.getString("cliwizards.desc");

        String apacheRGDesc = messages.getString(
                "apache.reviewPanel.apacheResourceGroup.desc");
        String apacheResourceDesc = messages.getString(
                "apache.reviewPanel.apacheResource.desc");
        String apacheResourceNote = messages.getString(
                "apache.reviewPanel.apacheResource.note");
        String lhresourceDesc = messages.getString(
                "apache.reviewPanel.lhResource.desc");
        String shresourceDesc = messages.getString(
                "apache.reviewPanel.shResource.desc");
        String storageResourceDesc = messages.getString(
                "apache.reviewPanel.storageResource.desc");
        String shRGDesc = messages.getString("apache.reviewPanel.shGroup.desc");


        Vector reviewPanel = new Vector();
        HashMap userInputs = new HashMap();

        // Get the Apache Mountpoint
        String mountPoint = (String) wizardModel.getWizardValue(
                Util.APACHE_MOUNT);

        // Get the Apache Mode
        String apacheMode = (String) wizardModel.getWizardValue(
                Util.APACHE_MODE);

        String nodeList[] = (String[]) wizardModel.getWizardValue(
                Util.RG_NODELIST);

        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);
        JMXConnector connector = getConnection(nodeEndPoint);
        mBean = ApacheWizardCreator.getApacheMBeanOnNode(connector);

        /*
         * We First show the Apache Configuration information
         * and then show the Sun Cluster Objects
         */
        while (true) {
            assignedResNames = new ArrayList();
            assignedRgNames = new ArrayList();

            TTYDisplay.clearScreen();
            TTYDisplay.clear(1);
            TTYDisplay.printTitle(sctitle);
            TTYDisplay.clear(1);
            TTYDisplay.pageText(scinstruction);
            TTYDisplay.setNavEnabled(true);
            this.cancelDirection = false;
            reviewPanel.removeAllElements();

            String apacheHostName[] = (String[]) wizardModel.getWizardValue(
                    Util.APACHE_LISTEN);

            // If APACHE_LISTEN is null, then User had selected new LH or SH
            // Set APACHE_LISTEN from LH or SH wizard
            if (apacheHostName == null) {
                apacheHostName = (String[]) wizardModel.getWizardValue(
                        Util.HOSTNAMELIST);
                wizardModel.setWizardValue(Util.APACHE_LISTEN, apacheHostName);
            }

            StringBuffer apacheHostNameBuf = new StringBuffer();

            for (int i = 0; i < apacheHostName.length; i++) {
                apacheHostNameBuf = apacheHostNameBuf.append(apacheHostName[i]);

                if (i < (apacheHostName.length - 1)) {
                    apacheHostNameBuf.append(",");
                }
            }

            String apacheRes = (String) wizardModel.getWizardValue(
                    Util.APACHE_RESOURCE_NAME);

            if (apacheRes == null) {
                String disResourcePropertyName[] = {
                        Util.APACHE_RESOURCE_NAME
                    };
                HashMap discoveredMap = mBean.discoverPossibilities(
                        disResourcePropertyName, apacheHostNameBuf.toString());
                apacheRes = (String) discoveredMap.get(
                        Util.APACHE_RESOURCE_NAME);
                wizardModel.setWizardValue(Util.APACHE_RESOURCE_NAME,
                    apacheRes);
            }

            // First Row - Apache Resource Name
            reviewPanel.add(apacheResource);
            reviewPanel.add(apacheRes);

            if (!assignedResNames.contains(apacheRes)) {
                assignedResNames.add(apacheRes);
            }

            // Second Row - Apache Resource Group
            String apacheRG = (String) wizardModel.getWizardValue(
                    Util.APACHE_RESOURCE_GROUP);

            if (apacheRG == null) {
                String disResourcePropertyName[] = {
                        Util.APACHE_RESOURCE_GROUP
                    };
                HashMap discoveredMap = mBean.discoverPossibilities(
                        disResourcePropertyName, null);
                apacheRG = (String) discoveredMap.get(
                        Util.APACHE_RESOURCE_GROUP);
                wizardModel.setWizardValue(Util.APACHE_RESOURCE_GROUP,
                    apacheRG);
            }

            reviewPanel.add(apacheResourceGroup);
            reviewPanel.add(apacheRG);

            if (!assignedRgNames.contains(apacheRG)) {
                assignedRgNames.add(apacheRG);
            }

            // Third Row - Storage Resource
            String storageRes = (String) wizardModel.getWizardValue(
                ApacheWizardConstants.STORAGE_RESOURCE_NAME);
            reviewPanel.add(storageResource);
            reviewPanel.add(storageRes);

            if (!assignedResNames.contains(storageRes)) {
                assignedResNames.add(storageRes);
            }

            // Fourth Row - Hostname Resource
            String hostResource;
            String sharedAddressRG = null;

            if (apacheMode.equals(Apache.FAILOVER)) {

                // If LH_RESOURCE_NAME is null, then
                // Get the resource name from LH wizard
                if (wizardModel.getWizardValue(Util.LH_RESOURCE_NAME) == null) {
                    hostResource = (String) wizardModel.getWizardValue(
                            Util.RESOURCENAME);
                    wizardModel.setWizardValue(Util.LH_RESOURCE_NAME,
                        hostResource);
                }

                hostResource = (String) wizardModel.getWizardValue(
                        Util.LH_RESOURCE_NAME);
                reviewPanel.add(lhResource);
                reviewPanel.add(hostResource);

                if (!assignedResNames.contains(hostResource)) {
                    assignedResNames.add(hostResource);
                }
            } else {
                hostResource = (String) wizardModel.getWizardValue(
                        Util.SH_RESOURCE_NAME);

                // If SH_RESOURCE_NAME is null, then
                // Get the resource name from LH wizard
                if (wizardModel.getWizardValue(Util.SH_RESOURCE_NAME) == null) {
                    hostResource = (String) wizardModel.getWizardValue(
                            Util.RESOURCENAME);
                    sharedAddressRG = (String) wizardModel.getWizardValue(
                            Util.RESOURCEGROUPNAME);
                    wizardModel.setWizardValue(Util.SH_RESOURCE_NAME,
                        hostResource);
                    wizardModel.setWizardValue(Util.APACHE_SHAREDADDRESS_GROUP,
                        sharedAddressRG);
                }

                reviewPanel.add(shResource);
                reviewPanel.add(hostResource);

                if (!assignedResNames.contains(hostResource)) {
                    assignedResNames.add(hostResource);
                }

                sharedAddressRG = (String) wizardModel.getWizardValue(
                        Util.APACHE_SHAREDADDRESS_GROUP);
                reviewPanel.add(shGroup);
                reviewPanel.add(sharedAddressRG);

                if (!assignedRgNames.contains(sharedAddressRG)) {
                    assignedRgNames.add(sharedAddressRG);
                }

            }

            HashMap customTags = new HashMap();
            customTags.put(dchar, done);

            String subheadings[] = { heading1, heading2 };

            // Display the Table
            TTYDisplay.clear(1);

            String option = TTYDisplay.create2DTable(scTableTitle, subheadings,
                    reviewPanel, customTags, schelp);

            Object isNewHost = wizardModel.getWizardValue(
                    ApacheWizardConstants.NEWHOST);
            Object isNewMount = wizardModel.getWizardValue(
                    ApacheWizardConstants.NEWMOUNT);

            // Handle Back
            if (option.equals("<")) {
                this.cancelDirection = true;

                break;
            }

            // Done
            if (option.equals(dchar)) {
                return;
            }

            // Process the options
            if (option.equals("1")) {
                TTYDisplay.clear(2);
                TTYDisplay.pageText(cliDesc + apacheResourceDesc + ". " +
                    apacheResourceNote);
                apacheRes = getNewResourceName(apacheRes);

                if (apacheRes != null) {
                    wizardModel.setWizardValue(Util.APACHE_RESOURCE_NAME,
                        apacheRes);
                }
            }

            if (option.equals("2")) {
                TTYDisplay.clear(2);
                TTYDisplay.pageText(cliDesc + apacheRGDesc);

                if (((isNewHost != null) && (isNewMount != null)) ||
                        ((isNewMount != null) &&
                            apacheMode.equals(Apache.SCALABLE))) {
                    apacheRG = getNewResourceGroupName(apacheRG);

                    if (apacheRG != null) {
                        wizardModel.setWizardValue(Util.APACHE_RESOURCE_GROUP,
                            apacheRG);
                    }
                } else {
                    TTYDisplay.clear(1);
                    TTYDisplay.promptForEntry(continueText);
                }
            }

            if (option.equals("3")) {
                TTYDisplay.clear(2);
                TTYDisplay.pageText(cliDesc + storageResourceDesc);

                if (isNewMount != null) {
                    storageRes = getNewResourceName(storageRes);

                    if (storageRes != null) {
                        wizardModel.setWizardValue(
                            ApacheWizardConstants.STORAGE_RESOURCE_NAME,
                            storageRes);
                    }
                } else {
                    TTYDisplay.clear(1);
                    TTYDisplay.promptForEntry(continueText);
                }
            }

            if (option.equals("4")) {

                // Display description based on Apache Mode
                if (apacheMode.equals(Apache.FAILOVER)) {
                    TTYDisplay.clear(2);
                    TTYDisplay.pageText(cliDesc + lhresourceDesc);
                } else {
                    TTYDisplay.clear(2);
                    TTYDisplay.pageText(cliDesc + shresourceDesc);
                }

                if (isNewHost != null) {
                    hostResource = getNewResourceName(hostResource);

                    if (hostResource != null) {

                        if (apacheMode.equals(Apache.FAILOVER)) {
                            wizardModel.setWizardValue(Util.LH_RESOURCE_NAME,
                                hostResource);
                        } else {
                            wizardModel.setWizardValue(Util.SH_RESOURCE_NAME,
                                hostResource);
                        }
                    }
                } else {
                    TTYDisplay.clear(1);
                    TTYDisplay.promptForEntry(continueText);
                }
            }

            if (option.equals("5")) {
                TTYDisplay.clear(2);
                TTYDisplay.pageText(cliDesc + shRGDesc);

                if (isNewHost != null) {
                    sharedAddressRG = getNewResourceGroupName(sharedAddressRG);

                    if (sharedAddressRG != null) {
                        wizardModel.setWizardValue(
                            Util.APACHE_SHAREDADDRESS_GROUP, sharedAddressRG);
                    }
                } else {
                    TTYDisplay.clear(1);
                    TTYDisplay.promptForEntry(continueText);
                }
            }

        } // while
    }

    /**
     * Generic method to get the name of a new Resource It checks for
     * whitespaces and checks if the ResourceName exists
     *
     * @return  Valid Resource Name or NULL
     */
    private String getNewResourceName(String oldValue) {

        // Pattern for checking Resource and ResourceGroup Name
        Pattern p = Pattern.compile("\\s*");
        Matcher m;
        String enterNewText = messages.getString("cliwizards.enterNew");
        String enterAgainText = messages.getString("cliwizards.enterAgain");
        String invalidResourceName = messages.getString(
                "lhwizard.reviewPanel.invalidResName");
        String yes = messages.getString("cliwizards.yes");
        String no = messages.getString("cliwizards.no");
        String resourceNameUsed = messages.getString(
                "lhwizard.reviewPanel.resourceNameErr");
        String rsname_assigned = messages.getString(
                "reviewpanel.resourcename.assigned");

        while (true) {
            String rName = TTYDisplay.promptForEntry(enterNewText);
            boolean rNameOk = true;

            if (rName.equals("<")) {
                return null;
            }

            // Check whether String has white space characters
            m = p.matcher(rName);

            if (m.matches()) {
                rNameOk = false;
                TTYDisplay.printError(invalidResourceName);

                String option = TTYDisplay.getConfirmation(enterAgainText, yes,
                        no);

                if (!option.equalsIgnoreCase(yes)) {
                    break;
                }
            }

            // Validate whether the name is already used
            if (DataServicesUtil.isResourceNameUsed(rName)) {
                rNameOk = false;
                TTYDisplay.printError(resourceNameUsed);

                String option = TTYDisplay.getConfirmation(enterAgainText, yes,
                        no);

                if (!option.equalsIgnoreCase(yes)) {
                    return null;
                }
            }

            if (oldValue != null) {
                assignedResNames.remove(oldValue);
            }

            if (assignedResNames.contains(rName)) {
                rNameOk = false;
                TTYDisplay.printError(rsname_assigned);

                String option = TTYDisplay.getConfirmation(enterAgainText, yes,
                        no);

                // add the original back
                if (oldValue != null) {
                    assignedResNames.add(oldValue);
                }

                if (!option.equalsIgnoreCase(yes)) {
                    break;
                }
            }

            if (rNameOk) {
                assignedResNames.add(rName);

                return rName;
            }
        }

        return null;
    }

    /**
     * Generic method to get the name of a new Resource Group It checks for
     * whitespaces and checks if the ResourceGroup Name exists
     *
     * @return  Valid Resource Name or NULL
     */
    private String getNewResourceGroupName(String oldValue) {

        // Pattern for checking Resource and ResourceGroup Name
        Pattern p = Pattern.compile("\\s*");
        Matcher m;
        String enterNewText = messages.getString("cliwizards.enterNew");
        String enterAgainText = messages.getString("cliwizards.enterAgain");
        String invalidResourceGroupName = messages.getString(
                "lhwizard.reviewPanel.invalidRGName");
        String yes = messages.getString("cliwizards.yes");
        String no = messages.getString("cliwizards.no");
        String resourceGroupNameUsed = messages.getString(
                "lhwizard.reviewPanel.resourceGroupNameErr");
        String rgname_assigned = messages.getString(
                "reviewpanel.rgname.assigned");

        while (true) {
            String rgName = TTYDisplay.promptForEntry(enterNewText);
            boolean rgNameOk = true;

            if (rgName.equals("<")) {
                return null;
            }

            // Check whether String has white space characters
            m = p.matcher(rgName);

            if (m.matches()) {
                rgNameOk = false;
                TTYDisplay.printError(invalidResourceGroupName);

                String option = TTYDisplay.getConfirmation(enterAgainText, yes,
                        no);

                if (!option.equalsIgnoreCase(yes)) {
                    break;
                }
            }

            // Validate whether the name is already used
            if (DataServicesUtil.isRGNameUsed(rgName)) {
                rgNameOk = false;
                TTYDisplay.printError(resourceGroupNameUsed);

                String option = TTYDisplay.getConfirmation(enterAgainText, yes,
                        no);

                if (!option.equalsIgnoreCase(yes)) {
                    return null;
                }
            }

            // Remove old value
            if (oldValue != null) {
                assignedRgNames.remove(oldValue);
            }

            if (assignedRgNames.contains(rgName)) {
                rgNameOk = false;
                TTYDisplay.printError(rgname_assigned);

                String option = TTYDisplay.getConfirmation(enterAgainText, yes,
                        no);

                // add the original back
                if (oldValue != null) {
                    assignedRgNames.add(oldValue);
                }

                if (!option.equalsIgnoreCase(yes)) {
                    break;
                }
            }

            if (rgNameOk) {
                assignedRgNames.add(rgName);

                return rgName;
            }
        }

        return null;
    }
}
