/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ApacheWizardHostSelectionPanel.java 1.13     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.apache;

// J2SE
import com.sun.cluster.agent.dataservices.apache.Apache;
import com.sun.cluster.agent.dataservices.apache.ApacheMBean;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.rgm.ResourceMBean;
import com.sun.cluster.common.TrustedMBeanModel;
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliLogicalHostSelectionPanel;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;

// Wizard Common
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.hastorageplus.HASPWizardConstants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.management.remote.JMXConnector;


/**
 * Panel to get the Hostname for Apache
 */
public class ApacheWizardHostSelectionPanel
    extends CliLogicalHostSelectionPanel {

    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N messages; // Message Bundle
    private ApacheMBean mBean = null; // Reference to MBean
    private JMXConnector localConnection = null; // Reference to JMXConnector

    /**
     * Creates a new instance of ApacheWizardHostSelectionPanel
     */
    public ApacheWizardHostSelectionPanel() {
        super();
    }

    /**
     * Creates an ApacheWizardConfFile Panel with the given name and tree
     * manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public ApacheWizardHostSelectionPanel(String name,
        WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates a ApacheWizardConfFile Panel with the given name and associates
     * it with the WizardModel, WizardFlow and i18n reference
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     * @param  messages  WizardI18N
     */
    public ApacheWizardHostSelectionPanel(String name,
        CliDSConfigWizardModel model, WizardFlow wizardFlow, WizardI18N messages) {
        super(name, model, wizardFlow, messages);
        this.panelName = name;
        this.wizardModel = model;

        // Get the I18N instance
        this.messages = messages;
        localConnection = getConnection(Util.getClusterEndpoint());
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {

        // Title
        title = messages.getString("apache.hostSelectionPanel.host.title");
        desc = messages.getString("apache.hostSelectionPanel.host.query");
        help_text = messages.getString("apache.hostSelectionPanl.host.clihelp");
        continue_txt = messages.getString("cliwizards.continue");
        empty = "";
        subtitle = "";
        heading1 = messages.getString(
                "logicalhostSelectionPanel.table.colheader.0");
        heading2 = messages.getString(
                "logicalhostSelectionPanel.table.colheader.1");
        table_empty_continue = messages.getString(
                "apache.hostSelectionPanel.continue");

        String apacheMode = (String) wizardModel.getWizardValue(
                Util.APACHE_MODE);

        if (apacheMode.equals(Apache.FAILOVER)) {
            heading3 = messages.getString(
                    "logicalhostSelectionPanel.table.colheader.2");
        } else {
            heading3 = messages.getString(
                    "logicalhostSelectionPanel.table.colheader.sharedaddress");
        }

        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);
        JMXConnector connector = getConnection(nodeEndPoint);
        mBean = ApacheWizardCreator.getApacheMBeanOnNode(connector);

        String hostResource = null;
        String hostNameArr[] = null;


        displayPanel(true);

        String userSelection = (String) wizardModel.getWizardValue(
                Util.CREATE_NEW);

        if (userSelection == null) {

            // User pressed back
            // Clear any previous entry in the hasp wizards
            wizardModel.setWizardValue(
                HASPWizardConstants.HASP_FILESYSTEMS_SELECTED, null);
            wizardModel.setWizardValue(
                HASPWizardConstants.HASP_DEVICES_SELECTED, null);
            this.cancelDirection = true;

            return;
        } else if (userSelection.equals(CREATE_NEW)) {

            if (apacheMode.equals(Apache.FAILOVER)) {

                // User needs new LogicalHost
                // Setting the hostnames as null so the wizardFlow
                // would intiate LH Wizard
                wizardModel.setWizardValue(Util.LH_RESOURCE_NAME, null);
            } else {

                // User needs new SharedAddress
                // Setting the hostnames as null so the wizardFlow
                // would intiate SH Wizard
                wizardModel.setWizardValue(Util.SH_RESOURCE_NAME, null);
            }
        } else {
            Vector selectedResources = (Vector) wizardModel.getWizardValue(
                    LH_SEL_RS);
            String selectedHostnames[] = (String[]) wizardModel.getWizardValue(
                    Util.SEL_HOSTNAMES);

            if ((selectedHostnames != null) && (selectedHostnames.length > 0)) {
                hostNameArr = selectedHostnames;
            }

            if ((selectedResources != null) && (selectedResources.size() > 0))
                hostResource = (String) selectedResources.get(0);

            // User selected a hostname configured through the
            // Hostname Wizard
            if (((hostResource == null) ||
                        (hostResource.trim().length() <= 0)) &&
                    (hostNameArr != null)) {

                if (apacheMode.equals(Apache.FAILOVER)) {
                    wizardModel.setWizardValue(Util.LH_RESOURCE_NAME, " ");
                    wizardModel.setWizardValue(Util.HOSTNAMELIST, hostNameArr);
                    populateApacheInfo_ForFailover(wizardModel);

                    return;
                } else if (apacheMode.equals(Apache.SCALABLE)) {
                    wizardModel.setWizardValue(Util.SH_RESOURCE_NAME, " ");
                    wizardModel.setWizardValue(Util.HOSTNAMELIST, hostNameArr);
                    populateApacheInfo_ForScalable(wizardModel);

                    return;
                }
            }
            // User selected an existing hostname resource
            else {

                if (apacheMode.equals(Apache.FAILOVER)) {

                    // User selected a LogicalHost
                    wizardModel.setWizardValue(Util.LH_RESOURCE_NAME,
                        hostResource);
                    populateApacheInfo_ForFailover(wizardModel);

                    return;
                } else {

                    // User selected a SharedAddress
                    wizardModel.setWizardValue(Util.SH_RESOURCE_NAME,
                        hostResource);
                    populateApacheInfo_ForScalable(wizardModel);

                    return;
                }
            }
        }
    }

    /**
     * For Failover Mode Populate wizardModel with port, server root from the
     * configuration file. Also the Listen value for Apache is decided here by
     * the hostname resource selected by the user.
     */
    private void populateApacheInfo_ForFailover(CliDSConfigWizardModel wizCxt) {

        // Discover Server Root, Port from the Conf File
        String confFilePath = (String) wizCxt.getWizardValue(
                Util.APACHE_CONF_FILE);

        // The Conf File is Parsed for ServerRoot
        HashMap discoveredValue = mBean.discoverPossibilities(
                new String[] { Util.APACHE_HOME }, confFilePath);

        String serverRoot[] = (String[]) discoveredValue.get(Util.APACHE_HOME);
        wizCxt.setWizardValue(Util.APACHE_HOME, serverRoot[0]);

        // The Conf File is Parsed for Port
        discoveredValue = mBean.discoverPossibilities(
                new String[] { Util.APACHE_PORT }, confFilePath);

        String port[] = (String[]) discoveredValue.get(Util.APACHE_PORT);
        wizCxt.setWizardValue(Util.APACHE_PORT, port);

        /*
         * If Apache Resource Group is not set, get it from the
         * LH_RESOURCE_NAME selected
         */
        if (wizCxt.getWizardValue(Util.APACHE_RESOURCE_GROUP) == null) {

            // Only If the Logical Host is not new (From LH Wizard)
            // we can decide on the Apache Group
            String lhresource = (String) wizCxt.getWizardValue(
                    Util.LH_RESOURCE_NAME);

            if (!lhresource.equals(" ")) {

                try {
                    ResourceMBean hostResourceMBean = (ResourceMBean)
                        TrustedMBeanModel.getMBeanProxy(localConnection,
                            ResourceMBean.class, lhresource, true);
                    String apacheResourceGroup = hostResourceMBean
                        .getResourceGroupName();
                    wizCxt.setWizardValue(Util.APACHE_RESOURCE_GROUP,
                        apacheResourceGroup);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        /*
         * Discover the APACHE_LISTEN based on the LH Resource
         *  -> If Existing resource discover its HostNameList.
         *  -> If the selected logicalhost resource was created
         *  using the LH wizard get it from wizard model.
         */
        String lhResource = (String) wizCxt.getWizardValue(
                Util.LH_RESOURCE_NAME);

        if (!lhResource.equals(" ")) {
            discoveredValue = mBean.discoverPossibilities(
                    new String[] { Util.APACHE_LISTEN }, lhResource);

            String listen[] = (String[]) discoveredValue.get(
                    Util.APACHE_LISTEN);
            wizCxt.setWizardValue(Util.APACHE_LISTEN, listen);
        } else {
            String lhresource = (String) wizCxt.getWizardValue(
                    Util.RESOURCENAME);
            wizCxt.setWizardValue(Util.RESOURCENAME, null);
            wizCxt.setWizardValue(Util.LH_RESOURCE_NAME, lhresource);

            // Get the hostname list from the hostwizard
            // for APACHE_LISTEN
            String hostNameList[] = (String[]) wizCxt.getWizardValue(
                    Util.HOSTNAMELIST);
            wizCxt.setWizardValue(Util.APACHE_LISTEN, hostNameList);

            // Set new HOST to true, so that user can edit
            // the same in ReviewPanel
            wizCxt.setWizardValue(ApacheWizardConstants.NEWHOST, "true");
        }
    }

    /**
     * For Scalable Mode Populate wizardModel with port, server root from the
     * configuration file. Also the Listen value for Apache is decided here by
     * the hostname resource selected by the user.
     */
    private void populateApacheInfo_ForScalable(CliDSConfigWizardModel wizCxt) {

        // Discover Server Root, Port from the Conf File
        String confFilePath = (String) wizCxt.getWizardValue(
                Util.APACHE_CONF_FILE);

        // The Conf File is Parsed for ServerRoot
        HashMap discoveredValue = mBean.discoverPossibilities(
                new String[] { Util.APACHE_HOME }, confFilePath);
        String serverRoot[] = (String[]) discoveredValue.get(Util.APACHE_HOME);
        wizCxt.setWizardValue(Util.APACHE_HOME, serverRoot[0]);

        // The Conf File is Parsed for Port
        discoveredValue = mBean.discoverPossibilities(
                new String[] { Util.APACHE_PORT }, confFilePath);

        String port[] = (String[]) discoveredValue.get(Util.APACHE_PORT);

        wizCxt.setWizardValue(Util.APACHE_PORT, port);

        String shresource = (String) wizCxt.getWizardValue(
                Util.SH_RESOURCE_NAME);

        /*
         * If the Shared Address is not set by the SH Wizard
         * or if user had selected an existing SH resource, we can decide
         * on the SharedAddress RG
         */
        if (!shresource.equals(" ")) {

            try {
                ResourceMBean hostResourceMBean = (ResourceMBean)
                    TrustedMBeanModel.getMBeanProxy(localConnection,
                        ResourceMBean.class, shresource, true);
                String shResourceGroup = hostResourceMBean
                    .getResourceGroupName();
                wizCxt.setWizardValue(Util.APACHE_SHAREDADDRESS_GROUP,
                    shResourceGroup);

                /*
                 * Discover the APACHE_LISTEN based on the SH Resource
                 */
                String shResource = (String) wizCxt.getWizardValue(
                        Util.SH_RESOURCE_NAME);
                discoveredValue = mBean.discoverPossibilities(
                        new String[] { Util.APACHE_LISTEN }, shResource);

                String listen[] = (String[]) discoveredValue.get(
                        Util.APACHE_LISTEN);
                wizCxt.setWizardValue(Util.APACHE_LISTEN, listen);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        /*
         * if the selected shared address resource was done
         * using the SH wizard
         */
        else {

            // get the generated names from the SH Wizard
            shresource = (String) wizCxt.getWizardValue(Util.RESOURCENAME);
            wizCxt.setWizardValue(Util.RESOURCENAME, null);
            wizCxt.setWizardValue(Util.SH_RESOURCE_NAME, shresource);

            String shResourceGroup = (String) wizCxt.getWizardValue(
                    Util.RESOURCEGROUPNAME);
            wizCxt.setWizardValue(Util.RESOURCEGROUPNAME, null);
            wizCxt.setWizardValue(Util.APACHE_SHAREDADDRESS_GROUP,
                shResourceGroup);

            // Get the hostname list from the host wizard
            // for APACHE_LISTEN
            String hostNameList[] = (String[]) wizCxt.getWizardValue(
                    Util.HOSTNAMELIST);
            wizCxt.setWizardValue(Util.APACHE_LISTEN, hostNameList);

            // Set new HOST to true, so that user can edit
            // the same in ReviewPanel
            wizCxt.setWizardValue(ApacheWizardConstants.NEWHOST, "true");
        }
    }

    // Override the refreshRGList
    protected void refreshRGList() {

        // Discover LogicalHost or SharedAddress Resources depending on
        // the Apache Mode
        String apacheMode = (String) wizardModel.getWizardValue(
                Util.APACHE_MODE);
        String apacheNodeList[] = (String[]) wizardModel.getWizardValue(
                Util.RG_NODELIST);
        String propertyNames[] = null;
        Map discoveredMap = null;
        List rgList = null;

        if (apacheMode.equals(Apache.FAILOVER)) {
            propertyNames = new String[] { Util.LH_NAMES };

            String apacheResourceGroup = (String) wizardModel.getWizardValue(
                    Util.APACHE_RESOURCE_GROUP);
            Map helperMap = new HashMap();
            helperMap.put(Util.APACHE_RESOURCE_GROUP, apacheResourceGroup);
            helperMap.put(Util.RG_NODELIST, apacheNodeList);
            discoveredMap = mBean.discoverPossibilities(propertyNames,
                    helperMap);
            rgList = (ArrayList) discoveredMap.get(Util.LH_RGLIST);
        } else {
            propertyNames = new String[] { Util.SH_NAMES };

            Map helperMap = new HashMap();
            helperMap.put(Util.RG_NODELIST, apacheNodeList);
            discoveredMap = mBean.discoverPossibilities(propertyNames,
                    helperMap);
            rgList = (ArrayList) discoveredMap.get(Util.SH_RGLIST);
        }

        wizardModel.setWizardValue(Util.LH_RGLIST, rgList);
    }
}
