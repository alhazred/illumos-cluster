/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)HASPCreateFileSystemReviewPanel.java 1.28     08/12/10 SMI"
 */


package com.sun.cluster.dswizards.hastorageplus;

import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.hastorageplus.HAStoragePlusMBean;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.dataservices.utils.VfsStruct;
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import javax.management.remote.JMXConnector;


public class HASPCreateFileSystemReviewPanel extends WizardLeaf {

    private String panelName; // Panel Name
    private WizardI18N wizardi18n; // Message Bundle
    private CliDSConfigWizardModel wizardModel; // WizardModel

    private JMXConnector localConnection = null;
    private HAStoragePlusMBean mbean = null;
    private HAStoragePlusMBean mbeans[] = null;


    /**
     * Creates a new instance of HASPCreateFileSystemReviewPanel
     */
    public HASPCreateFileSystemReviewPanel() {
        super();
    }

    /**
     * Creates a HASPCreateFileSystemReviewPanel Panel with the 
     * given name and manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public HASPCreateFileSystemReviewPanel(String name,
        WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    public HASPCreateFileSystemReviewPanel(String name,
        CliDSConfigWizardModel model, WizardFlow wizardFlow,
        WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;
        this.wizardi18n = wizardi18n;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.
     *		WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        String title = wizardi18n.getString(
                "hasp.create.filesystem.review.title");
        String description = wizardi18n.getString(
                "hasp.create.filesystem.review.description");
        String mountpoint_txt = wizardi18n.getString(
                "hasp.create.filesystem.review.mountpoint");
        String devpath_txt = wizardi18n.getString(
                "hasp.create.filesystem.review.devicepath");
        String fstype_txt = wizardi18n.getString(
                "hasp.create.filesystem.review.fstype");
        String mountoption_txt = wizardi18n.getString(
                "hasp.create.filesystem.review.mountoption");
        String globaloption_txt = wizardi18n.getString(
                "hasp.create.filesystem.review.globaloption");
        String fsck_txt = wizardi18n.getString(
                "hasp.create.filesystem.review.fsckearly");
        String dchar = wizardi18n.getString("ttydisplay.d.char");
        String done = wizardi18n.getString("ttydisplay.menu.done");
        String table_title = wizardi18n.getString(
                "hasp.create.filesystem.review.table.title");
        String heading1 = wizardi18n.getString(
                "hasp.create.filesystem.review.heading1");
        String heading2 = wizardi18n.getString(
                "hasp.create.filesystem.review.heading2");
        String back = wizardi18n.getString("ttydisplay.back.char");
        String yes = wizardi18n.getString("cliwizards.yes");
        String no = wizardi18n.getString("cliwizards.no");
        String propNameText = (String) wizardi18n.getString(
                "hasp.create.filesystem.review.propname");
        String propTypeText = (String) wizardi18n.getString(
                "hasp.create.filesystem.review.proptype");
        String propDescText = (String) wizardi18n.getString(
                "hasp.create.filesystem.review.propdesc");
        String currValText = (String) wizardi18n.getString(
                "hasp.create.filesystem.review.currval");
        String newValText = (String) wizardi18n.getString(
                "hasp.create.filesystem.review.newval");
        String error_txt = wizardi18n.getString(
                "hasp.create.filesystem.path.error");
        String help_text = wizardi18n.getString(
                "hasp.create.filesystem.review.cli.help");
        String continue_txt = wizardi18n.getString("cliwizards.continue");

        // Review panel table
        Vector reviewPanel = new Vector();

        String discoverProps[] = { Util.HASP_FSTYPE };
        localConnection = getConnection(Util.getClusterEndpoint());
        mbean = HASPWizardCreator.getHAStoragePlusMBean(localConnection);

        String nodeList[] = (String[]) wizardModel.getWizardValue(
                Util.RG_NODELIST);
        mbeans = new HAStoragePlusMBean[nodeList.length];

        for (int i = 0; i < nodeList.length; i++) {
            String split_str[] = nodeList[i].split(Util.COLON);
            String nodeEndPoint = Util.getNodeEndpoint(localConnection,
                    split_str[0]);
            JMXConnector connector = getConnection(nodeEndPoint);
            mbeans[i] = HASPWizardCreator.getHAStoragePlusMBeanOnNode(
                    connector);
        }

        String option = "";

        while (!option.equals(dchar)) {
            TTYDisplay.setNavEnabled(true);
            this.cancelDirection = false;

            reviewPanel.removeAllElements();
            wizardModel.selectCurrWizardContext();

            // First Row - Mount Point
            String mountpoint = (String) wizardModel.getWizardValue(
                    HASPWizardConstants.HASP_CREATE_FS_MOUNTPOINT);
            reviewPanel.add(mountpoint_txt);
            reviewPanel.add(mountpoint);

            // Second Row - Device Path
            String devicepath = (String) wizardModel.getWizardValue(
                    HASPWizardConstants.HASP_CREATE_FS_DEVPATH);
            reviewPanel.add(devpath_txt);
            reviewPanel.add(devicepath);

            // Third Row - FileSystemType
            String fstype = (String) wizardModel.getWizardValue(
                    HASPWizardConstants.HASP_CREATE_FS_TYPE);
            reviewPanel.add(fstype_txt);
            reviewPanel.add(fstype);

            // Fourth row - Mount Option. default is "logging"
            String mountoption = (String) wizardModel.getWizardValue(
                    HASPWizardConstants.HASP_CREATE_FS_MOUNTOPTION);
            reviewPanel.add(mountoption_txt);
            reviewPanel.add(mountoption);

            // Fifth row - Global Mount option
            String globaloption = (String) wizardModel.getWizardValue(
                    HASPWizardConstants.HASP_CREATE_FS_GLOBALOPTION);
            reviewPanel.add(globaloption_txt);
            reviewPanel.add(globaloption);

            // Sixth row - Fsck Entry
            String fsckearly = yes;

            // Title
            TTYDisplay.printTitle(title);

            TTYDisplay.pageText(description);
            TTYDisplay.clear(1);

            HashMap customTags = new HashMap();
            customTags.put(dchar, done);

            String subheadings[] = { heading1, heading2 };

            option = TTYDisplay.create2DTable(table_title, subheadings,
                    reviewPanel, customTags, help_text);

            String desc;

            if (option.equals(back)) {
                this.cancelDirection = true;

                return;
            }

            TTYDisplay.clear(3);
            TTYDisplay.setNavEnabled(false);

            if (!option.equals(dchar)) {
                String prop = null;
                String currVal = null;

                if (option.equals("1")) {

                    // Change Mount point
                    prop = "mountpoint";
                    currVal = mountpoint;
                } else if (option.equals("2")) {

                    // device path
                    prop = "devicepath";
                    currVal = devicepath;
                } else if (option.equals("3")) {

                    // FSType
                    prop = "fstype";
                    currVal = fstype;
                } else if (option.equals("4")) {

                    // mount option
                    prop = "mountoption";
                    currVal = mountoption;
                } else if (option.equals("5")) {

                    // global mount option
                    prop = "globaloption";
                    currVal = globaloption;
                }

                if (!option.equals("4")) {
                    TTYDisplay.pageText(propNameText +
                        wizardi18n.getString(
                            "hasp.create.filesystem.review." + prop));

                    TTYDisplay.pageText(propDescText +
                        wizardi18n.getString(
                            "hasp.create.filesystem.review." + prop + ".desc"));

                    TTYDisplay.pageText(propTypeText +
                        wizardi18n.getString(
                            "hasp.create.filesystem.review." + prop + ".type"));

                    TTYDisplay.pageText(currValText + currVal);
                }

                // Prompt for new values for each of the options
                if (option.equals("1")) {

                    // Should not be editable in this screen
                    TTYDisplay.clear(1);
                    TTYDisplay.promptForEntry(continue_txt);
                } else if (option.equals("2")) {

                    // Should not be editable in this screen
                    TTYDisplay.clear(1);
                    TTYDisplay.promptForEntry(continue_txt);
                } else if (option.equals("3")) {

                    // cannot edit filesystem type
                    TTYDisplay.clear(1);
                    TTYDisplay.promptForEntry(continue_txt);
                    /* do nothing */;
                } else if (option.equals("4")) {

                    // Mount Options
                    if (fstype.equals(Util.FSTYPE_VXFS)) {

                        // cannot edit option since log is the only allowed
                        // option
                        TTYDisplay.pageText(wizardi18n.getString(
                                "hasp.create.filesystem.review.mountoption.vxfs.msg",
                                new String[] { fstype }));
                        TTYDisplay.clear(1);
                        TTYDisplay.promptForEntry(continue_txt);
                    } else {
                        List optionSelected = null;
                        String mountoptionTitle = wizardi18n.getString(
                                "hasp.create.filesystem.review.mountoption.selection.title");
                        List mountoptionList = new ArrayList();

                        String option1 = Util.UFS_LOGGING;
                        StringBuffer option2 = new StringBuffer(option1);
                        option2.append(Util.COMMA);
                        option2.append(Util.FORCEIO);
                        mountoptionList.add(option1);
                        mountoptionList.add(option2.toString());

                        String mountoptions[] = (String[]) mountoptionList
                            .toArray(new String[mountoptionList.size()]);
                        optionSelected = TTYDisplay.getScrollingOption(
                                mountoptionTitle, mountoptions, null, "", null,
                                true);

                        // Handle back
                        if (optionSelected == null) {
                            this.cancelDirection = true;

                            return;
                        }

                        wizardModel.setWizardValue(
                            HASPWizardConstants.HASP_CREATE_FS_MOUNTOPTION,
                            (String) optionSelected.get(0));
                    }
                } else if (option.equals("5")) {

                    // Global mount option
                    String tmp_globaloption = "";
                    String rgMode = (String) wizardModel.getWizardValue(
                            Util.RG_MODE);

                    if ((rgMode != null) &&
                            rgMode.equals(Util.SCALABLE_RG_MODE)) {
                        tmp_globaloption = yes;
                        TTYDisplay.clear(1);
                        TTYDisplay.promptForEntry(continue_txt);
                    } else {
                        tmp_globaloption = TTYDisplay.getConfirmation(
                                newValText, yes, no);
                    }

                    wizardModel.setWizardValue(
                        HASPWizardConstants.HASP_CREATE_FS_GLOBALOPTION,
                        tmp_globaloption);
                }
            } else {

                // validate the user input and return
                // TO DO
                // if validate succeeds, add the new filesystem mount point to
                // the list
                // construct the vfstab entry to add to all nodes
                devicepath = (String) wizardModel.getWizardValue(
                        HASPWizardConstants.HASP_CREATE_FS_DEVPATH);
                mountpoint = (String) wizardModel.getWizardValue(
                        HASPWizardConstants.HASP_CREATE_FS_MOUNTPOINT);
                mountoption = (String) wizardModel.getWizardValue(
                        HASPWizardConstants.HASP_CREATE_FS_MOUNTOPTION);
                globaloption = (String) wizardModel.getWizardValue(
                        HASPWizardConstants.HASP_CREATE_FS_GLOBALOPTION);
                fstype = (String) wizardModel.getWizardValue(
                        HASPWizardConstants.HASP_CREATE_FS_TYPE);
                fsckearly = yes;

                if (globaloption.equalsIgnoreCase(yes)) {

                    // if user says yes to global
                    if ((mountoption != null) &&
                            (mountoption.trim().length() > 0)) {

                        if (!mountoption.endsWith(Util.COMMA)) {
                            mountoption += Util.COMMA + Util.GLOBAL_OPTION;
                        } else {
                            mountoption += Util.GLOBAL_OPTION;
                        }
                    } else {
                        mountoption = Util.GLOBAL_OPTION;
                    }
                }

                devicepath = devicepath.replaceFirst(Util.RDSK, Util.DSK);

                String devicefsck = devicepath.replaceFirst(Util.DSK,
                        Util.RDSK);

                if (fsckearly.equalsIgnoreCase(yes)) {
                    fsckearly = "1";
                } else {
                    fsckearly = "2";
                }

                // Set to no by default
                String mountatboot = "no";

                // construct a vfstab struct

                VfsStruct vfsEntry = new VfsStruct();
                vfsEntry.setVfs_special(devicepath);
                vfsEntry.setVfs_fsckdev(devicefsck);
                vfsEntry.setVfs_mountp(mountpoint);
                vfsEntry.setVfs_fstype(fstype);
                vfsEntry.setVfs_fsckpass(fsckearly);
                vfsEntry.setVfs_automnt(mountatboot);
                vfsEntry.setVfs_mntopts(mountoption);

                HashMap fsHashMap = (HashMap) wizardModel.getWizardValue(
                        Util.HASP_ALL_FILESYSTEMS);
                HashMap map_to_add = (HashMap) fsHashMap.get(
                        Util.VFSTAB_MAP_TO_ADD);

                int i = 0;
                for (i = 0; i < nodeList.length; i++) {
                    String split_str[] = nodeList[i].split(Util.COLON);
                    ArrayList nodeEntry = (ArrayList) map_to_add.get(
                            split_str[0]);

                    // each element of a vector is a vfstab entry
                    if (nodeEntry == null) {
                        nodeEntry = new ArrayList();
                    } 

                    int size = nodeEntry.size();
                    boolean newEntry = true;
                    for (int n = 0; n < size; n++) {
                        VfsStruct entry = (VfsStruct)nodeEntry.get(n);
                        if (entry.getVfs_mountp().equals(mountpoint)) {
                            // no need to update MAP_TO_ADD
                            newEntry = false;
                            break; 
                        }
                    }
                    
                    if (newEntry) {
                        nodeEntry.add(vfsEntry);
                        map_to_add.put(split_str[0], nodeEntry);
                    }
                }

                fsHashMap.put(Util.VFSTAB_MAP_TO_ADD, map_to_add);

                VfsStruct vfstabEntries[] = (VfsStruct[]) fsHashMap.get(
                        Util.VFSTAB_ENTRIES);

                // add the new filesystem mount point to the list
                VfsStruct new_vfstabEntries[] =
                    new VfsStruct[vfstabEntries.length + 1];
   
                boolean newEntry = true;
                i = 0;
                for (i = 0; i < vfstabEntries.length; i++) {
                    if (vfstabEntries[i].getVfs_mountp().equals(mountpoint)) { 
                         vfstabEntries[i] = vfsEntry;
                         newEntry = false;
                         break;
                    } 
                    new_vfstabEntries[i] = vfstabEntries[i];
                }

                if (newEntry) {
                    new_vfstabEntries[i] = vfsEntry;
                    fsHashMap.put(Util.VFSTAB_ENTRIES, new_vfstabEntries);
                } 
                wizardModel.setWizardValue(Util.HASP_ALL_FILESYSTEMS,
                    fsHashMap);

                String selectedfilesystems[] = (String[]) wizardModel
                    .getWizardValue(
                        HASPWizardConstants.HASP_FILESYSTEMS_SELECTED);

                // add the new filesystemmount point created to the selection
                String new_selection[] =
                    new String[selectedfilesystems.length + 1];
                i = 0;

                for (i = 0; i < selectedfilesystems.length; i++) {
                    new_selection[i] = selectedfilesystems[i];
                }

                new_selection[i] = mountpoint;
                wizardModel.setWizardValue(HASPWizardConstants.NEWLY_ADDED_MTPT,
                    mountpoint);
                wizardModel.setWizardValue(
                    HASPWizardConstants.HASP_FILESYSTEMS_SELECTED,
                    new_selection);
            }

            TTYDisplay.clear(3);
        }
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#isDisplayComplete
     */
    public boolean isDisplayComplete() {
        return true;
    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }

}
