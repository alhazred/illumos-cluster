/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)Rac10GCrsStorageChoicePanel.java 1.6     08/12/10 SMI"
 */


package com.sun.cluster.dswizards.oraclerac.database.ver10g;

import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.oraclerac.database.*;

import java.lang.Integer;

import java.util.HashMap;


/**
 * The Panel Class that gets use preference for select existing or Hardware RAID
 * without Volume Manager.
 */
public class Rac10GCrsStorageChoicePanel extends RacDBBasePanel {


    /**
     * Creates a CrsStorageChoice Panel with the given name and Wizard state
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     */
    public Rac10GCrsStorageChoicePanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(name, model, wizardFlow, wizardi18n);
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.
     *		WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        title = wizardi18n.getString(
                "oraclerac10g.crsStorageChoicePanel.title");

        String preference = wizardi18n.getString(
                "oraclerac10g.crsStorageChoicePanel.preference");
        String option1 = wizardi18n.getString(
                "oraclerac10g.crsStorageChoicePanel.option1");
        String option2 = wizardi18n.getString(
                "oraclerac10g.crsStorageChoicePanel.option2");
        int defSelection = 0;

        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        // Title
        TTYDisplay.printTitle(title);

        String options[] = { option1, option2 };

        // Get the data preference
        String pref = (String) wizardModel.getWizardValue(
                Util.CRS_STORAGE_CHOICE);

        if ((pref == null) || pref.equals(Util.SELECT_EXISTING)) {
            defSelection = 0;
        } else {
            defSelection = 1;
        }

        int index = TTYDisplay.getMenuOption(preference, options, defSelection);

        if (index == TTYDisplay.BACK_PRESSED) {
            this.cancelDirection = true;

            return;
        }

        wizardModel.selectCurrWizardContext();

        String selection = options[index];

        if (selection.equals(option1)) {
            pref = Util.SELECT_EXISTING;
        } else if (selection.equals(option2)) {
            pref = Util.SELECT_NONE;
            wizardModel.setWizardValue(Util.SEL_SCAL_RS, null);
            wizardModel.setWizardValue(Util.ALL_SEL_CRS_FILES, null);
        }

        wizardModel.setWizardValue(Util.CRS_STORAGE_CHOICE, pref);
    }
}
