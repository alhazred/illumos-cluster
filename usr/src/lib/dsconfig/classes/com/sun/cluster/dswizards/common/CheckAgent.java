//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ident	"@(#)CheckAgent.java 1.8     08/05/20 SMI"
//

package com.sun.cluster.dswizards.common;

// JDK
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.IOException;

// JMX
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import javax.management.MBeanServerConnection;
import javax.management.MBeanServerInvocationHandler;

// Cacao
import com.sun.cacao.ModuleMBean;
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.agent.JmxClient;
import com.sun.cacao.container.HealthCheckFailureException;
import com.sun.cacao.element.OperationalStateEnum;

// CMAS
import com.sun.cluster.common.TrustedMBeanModel;
import com.sun.cluster.agent.cluster.ClusterMBean;

public class CheckAgent {

    static private Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.cluster");

    static private MBeanServerConnection mbsc = null;

    public static void main(String args[]) {

        int retval = 0; // SUCCESS
        String agentLoc = null;
        ClusterMBean mbean = null;
        JMXConnector jmxConn = null;

        try {

            if (args.length != 1)
                usage();

            agentLoc = args[0];
            jmxConn = TrustedMBeanModel.getWellKnownConnector(agentLoc);
            mbsc = jmxConn.getMBeanServerConnection();
            mbean = (ClusterMBean) JmxClient.getMBeanProxy(mbsc,
                    new ObjectNameFactory(
                        ClusterMBean.class.getPackage().getName()),
                    ClusterMBean.class, null, false);

        } catch (Exception e) {
            logger.warning(
                "Unable to connect to the CACAO agent. The agent may be down or restarting");
            retval = -1;

            try {

                if (jmxConn != null)
                    jmxConn.close();
            } catch (IOException ioe) {
                logger.warning("Unable to close the JMX Connection cleanly");
            }

            System.exit(retval);
        }

        try {

            if (jmxConn != null)
                jmxConn.close();
        } catch (IOException ioe) {
            logger.warning("Unable to close the JMX Connection cleanly");
        }

        System.exit(retval);
    }

    private static void usage() {
        System.out.println("Usage: checkagent <endpoint>");
        System.exit(1);
    }

}
