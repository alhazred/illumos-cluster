/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ComponentSelectionPanel.java 1.12     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.sapwebas;

// J2SE
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

// CMAS
import com.sun.cluster.agent.dataservices.utils.DataServicesUtil;
import com.sun.cluster.agent.dataservices.utils.Util;

// CLI Wizard SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;

// Wizard Common
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;

public class ComponentSelectionPanel extends WizardLeaf {

    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N wizardi18n; // Message Bundle


    /**
     * Creates a new instance of ComponentSelectionPanel
     */
    public ComponentSelectionPanel() {
        super();
    }

    /**
     * Creates a ComponentSelectionPanel Panel with the 
     * given name and tree manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public ComponentSelectionPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    public ComponentSelectionPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;

        // Get the I18N instance
        this.wizardi18n = wizardi18n;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {
        String title = wizardi18n.getString("sap.componentSelectPanel.title");
        String desc = wizardi18n.getString("sap.componentSelectPanel.desc");
        String query_txt = wizardi18n.getString(
                "sap.componentSelectPanel.query");
        String help_text = wizardi18n.getString(
                "sap.componentSelectPanel.help.cli");
        String yes = wizardi18n.getString("cliwizards.yes");
        String no = wizardi18n.getString("cliwizards.no");
        String sidconfirm_txt = wizardi18n.getString(
                "sap.componentSelectPanel.selection.confirm");
        String confirm_txt = wizardi18n.getString("cliwizards.confirmMessage");
        String continue_txt = wizardi18n.getString("cliwizards.continue");
        Boolean enqSelected = Boolean.FALSE;
        Boolean repSelected = Boolean.FALSE;
        Boolean scsSelected = Boolean.FALSE;

        DataServicesUtil.clearScreen();

        // Title
        TTYDisplay.clear(1);
        TTYDisplay.printTitle(title);
        TTYDisplay.clear(1);
        TTYDisplay.pageText(desc);
        TTYDisplay.clear(2);


        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        String option;

        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;
        option = TTYDisplay.getConfirmation(query_txt, yes, no);

        if (option.equalsIgnoreCase(
                    wizardi18n.getString("ttydisplay.back.char"))) {
            this.cancelDirection = true;

            return;
        }

        if (option.equalsIgnoreCase(yes)) {
            repSelected = Boolean.TRUE;

            // Set the property names to be serialized
            ArrayList toBeSerialized = (ArrayList) wizardModel.getWizardValue(
                    Util.WIZARD_STATE);
            toBeSerialized.add(Util.REP_SERVER_LOC);
            toBeSerialized.add(Util.REP_SERVER_PROFILE_LOC);
            wizardModel.setWizardValue(Util.WIZARD_STATE, toBeSerialized);
        } else {
            repSelected = Boolean.FALSE;
        }

        wizardModel.setWizardValue(Util.REP_COMPONENT, repSelected);
    }
}
