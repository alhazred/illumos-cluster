/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)CliResultsPanel.java 1.10     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.common;

// J2SE
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

//JMX
import javax.management.remote.JMXConnector;

//CMAS
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.dataservices.utils.Util;

// CLI Wizard SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;

// Wizard Common
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;

/**
 * The Panel Class is Base Result panel class for dsconfig cli wizard.
 */
public class CliResultsPanel extends WizardLeaf {

    private String panelName; // Panel Name
    private JMXConnector localConnection; // localconnection

    protected WizardI18N wizardi18n; // Message Bundle
    protected CliDSConfigWizardModel wizardModel; // WizardModel
    protected String title_msg_key;
    protected String success_msg_key;
    protected String failure_msg_key;


    /**
     * Creates a new instance of CliResultsPanel
     */
    public CliResultsPanel() {
        super();
    }

    /**
     * Creates a CliResultsPanel with the given name and manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public CliResultsPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    public CliResultsPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;
        this.wizardi18n = wizardi18n;

        if (localConnection == null) {
            localConnection = getConnection(Util.getClusterEndpoint());
        }
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    public void consoleInteraction() {
        boolean success = true;
        ExitStatus status[] = null;
        Object tmpObj = null;

        // Enable Navigation
        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        TTYDisplay.clearScreen();
        TTYDisplay.clear(1);

        // Title
        TTYDisplay.printTitle(wizardi18n.getString(title_msg_key));

        List commandList = (ArrayList) wizardModel.getWizardValue(Util.CMDLIST);

        tmpObj = wizardModel.getWizardValue(Util.EXITSTATUS);

        if (tmpObj != null) {
            CommandExecutionException cee = (CommandExecutionException) tmpObj;
            status = cee.getExitStatusArray();

            StringBuffer errStrings = new StringBuffer();

            if ((status != null) && (status.length > 0)) {
                List errList = status[0].getErrStrings();

                if (errList != null) {
                    Iterator iter = errList.iterator();

                    while (iter.hasNext()) {
                        errStrings.append(iter.next());
                        errStrings.append("\n");
                    }
                }
            }

            // Print Error Strings
            if (!errStrings.toString().equals("")) {
                TTYDisplay.printError(wizardi18n.getString(
                        "cliwizards.executionErrorMessage") +
                    errStrings.toString());
            }

            // Print Error Messages
            String errMsg = cee.getMessage();

            if (errMsg != null) {

                if (errMsg.equals("nfs.resultspanel.dumpAdmin.error") ||
                        errMsg.equals(
                            "nfs.resultspanel.createShareDir.error")) {
                    TTYDisplay.printError(wizardi18n.getString(errMsg));
                } else if (errMsg.equals(Util.IO_ERROR)) {
                    TTYDisplay.printError(wizardi18n.getString(
                            "cliwizards.resultspanel.errComm.summary"));
                } else {
                    TTYDisplay.printError(errMsg);
                }
            }

            success = false;
        }

        if ((commandList != null) && (commandList.size() > 0)) {
            TTYDisplay.printSubTitle(wizardi18n.getString(
                    "cliwizards.executionMessage"));
            TTYDisplay.clear(1);

            Iterator i = commandList.iterator();

            while (i.hasNext()) {
                String command = (String) i.next();

                if (!success && command.equals(Util.ROLLBACK_SUCCESS)) {

                    // Check to ensure the heading "Rollback commands" is not
                    // printed if there are no rollback commands.
                    if (i.hasNext()) {
                        TTYDisplay.clear(1);
                        TTYDisplay.printSubTitle(wizardi18n.getString(
                                "cliwizards.rollbackExecutionMessage"));

                        TTYDisplay.clear(1);
                    }

                    continue;
                } else if (!success && command.equals(Util.ROLLBACK_FAIL)) {

                    // Rollback failed due to some exception while
                    // executing rollback commands
                    TTYDisplay.printSubTitle(wizardi18n.getString(
                            "cliwizards.rollbackUnableMessage"));

                    TTYDisplay.clear(1);

                    continue;
                }

                TTYDisplay.showText(command, 4);
                TTYDisplay.clear(1);
            }
        } else {

            // for cases where cacao connection is lost somehow and there is
            // no command in the command list and no exception is thrown.
            if (success) {
                success = false;
                TTYDisplay.printError(wizardi18n.getString(
                        "cliwizards.resultspanel.errComm.summary"));
            }
        }

        TTYDisplay.clear(1);

        if (success) {
            TTYDisplay.printSubTitle(wizardi18n.getString(success_msg_key));
        } else {
            TTYDisplay.printSubTitle(wizardi18n.getString(failure_msg_key));
        }

        TTYDisplay.clear(1);

        // Clear the command list and exit status
        wizardModel.setWizardValue(Util.EXITSTATUS, null);
        wizardModel.setWizardValue(Util.CMDLIST, null);

        String response = "";

        if (success) {
            response = TTYDisplay.promptForEntry(wizardi18n.getString(
                        "cliwizards.results.success"));
        } else {
            response = TTYDisplay.promptForEntry(wizardi18n.getString(
                        "cliwizards.results.failed"));
        }

        if (!success &&
                response.equals(wizardi18n.getString("ttydisplay.back.char"))) {
            this.cancelDirection = true;

            return;
        }

        // Write the current context to the state file
        try {
            wizardModel.writeToStateFile(localConnection
                .getMBeanServerConnection());
        } catch (Exception e) {
            TTYDisplay.printError(wizardi18n.getString(
                    "dswizards.stateFile.error"));
        } finally {

            // Always close connection
            closeConnections();
        }

        System.exit(0);
    }


    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#isDisplayComplete
     */
    public boolean isDisplayComplete() {
        return true;
    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }

    private void closeConnections() {
        String nodeList[] = (String[]) wizardModel.getWizardValue(
                Util.RG_NODELIST);
        super.closeConnections(nodeList);
    }

}
