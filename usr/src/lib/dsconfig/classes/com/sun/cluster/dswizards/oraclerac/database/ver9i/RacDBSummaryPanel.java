/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)RacDBSummaryPanel.java	1.17	09/03/11 SMI"
 */


package com.sun.cluster.dswizards.oraclerac.database.ver9i;

// CMASS
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.rgm.property.ResourceProperty;

// Wizard CLI
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliUtil;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.ResourcePropertyUtil;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.hastorageplus.HASPWizardConstants;
import com.sun.cluster.dswizards.logicalhost.LogicalHostWizardConstants;
import com.sun.cluster.dswizards.oraclerac.database.*;
import com.sun.cluster.dswizards.oraclerac.invoker.RACInvokerCreator;

// Model
import com.sun.cluster.model.RACModel;

// Java
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

/**
 * Display a Summary Panel based on all the selections made in previous panels.
 */
public class RacDBSummaryPanel extends RacDBBasePanel {

    private static String NAME = "NAME";
    private static String VALUE = "VALUE";
    private static String DESCRIPTION = "DESCRIPTION";
    private static String COLON_DESC = "COLON_DESC";

    boolean bConfigServer = false;
    boolean bConfigListener = false;

    /**
     * Creates a Summary Panel with the given name and Wizard state
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     */
    public RacDBSummaryPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(name, model, wizardFlow, wizardi18n);
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.
     *		WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        title = wizardi18n.getString("haoracle.summarypanel.title");
        desc = wizardi18n.getString("haoracle.summarypanel.desc1");

        String server_rs_name = wizardi18n.getString(
                "haoracle.summarypanel.server.rsname.name");
        String server_rs_desc = wizardi18n.getString(
                "haoracle.summarypanel.server.rsname.desc");
        String server_rs_colon = wizardi18n.getString(
                "haoracle.summarypanel.server.rsname.colon");

        String listener_rs_name = wizardi18n.getString(
                "haoracle.summarypanel.listener.rsname.name");
        String listener_rs_desc = wizardi18n.getString(
                "haoracle.summarypanel.listener.rsname.desc");
        String listener_rs_colon = wizardi18n.getString(
                "haoracle.summarypanel.listener.rsname.colon");

        String rg_name = wizardi18n.getString(
                "haoracle.summarypanel.rgname.name");
        String rg_desc = wizardi18n.getString(
                "haoracle.summarypanel.rgname.desc");
        String rg_colon = wizardi18n.getString(
                "haoracle.summarypanel.rgname.colon");

        String nodelist_name = wizardi18n.getString(
                "haoracle.summarypanel.nodelist.name");
        String nodelist_desc = wizardi18n.getString(
                "haoracle.summarypanel.nodelist.desc");
        String nodelist_colon = wizardi18n.getString(
                "haoracle.summarypanel.nodelist.colon");

        String hasprs_name = wizardi18n.getString(
                "haoracle.summaryPanel.hasp.rsname.name");
        String hasprs_desc = wizardi18n.getString(
                "haoracle.summaryPanel.hasp.rsname.desc");
        String hasprs_colon = wizardi18n.getString(
                "haoracle.summaryPanel.hasp.rsname.colon");

        String fs_name = wizardi18n.getString(
                "haoracle.summarypanel.filesytem.name");
        String fs_desc = wizardi18n.getString(
                "haoracle.summarypanel.filesystem.desc");
        String fs_colon = wizardi18n.getString(
                "haoracle.summarypanel.filesytem.colon");

        String globaldev_name = wizardi18n.getString(
                "haoracle.summarypanel.devicegroup.name");
        String globaldev_desc = wizardi18n.getString(
                "haoracle.summarypanel.devicegroup.desc");
        String globaldev_colon = wizardi18n.getString(
                "haoracle.summarypanel.devicegroup.colon");

        String storage_rs_name = wizardi18n.getString(
                "oraclerac.summarypanel.storage.rsname.name");
        String storage_rs_desc = wizardi18n.getString(
                "oraclerac.summarypanel.storage.rsname.desc");
        String storage_rs_colon = wizardi18n.getString(
                "oraclerac.summarypanel.storage.rsname.colon");

        String fwd_arrow = wizardi18n.getString("ttydisplay.fwd.arrow");
        String table_title = wizardi18n.getString(
                "haoracle.summarypanel.desc2");
        String heading1 = wizardi18n.getString(
                "haoracle.summarypanel.heading1");
        String heading2 = wizardi18n.getString(
                "haoracle.summarypanel.heading2");
        String c_char = wizardi18n.getString("ttydisplay.c.char");
        String create_txt = wizardi18n.getString(
                "haoracle.summarypanel.menu.create");
        String q_char = wizardi18n.getString("ttydisplay.q.char");
        String quit_txt = wizardi18n.getString("ttydisplay.menu.quit");
        String panel_desc = wizardi18n.getString(
                "haoracle.summarypanel.description");
        String gencmd_txt = wizardi18n.getString(
                "haoracle.summarypanel.generateCommand");
        String exit_txt = wizardi18n.getString("cliwizards.confirmExit");
        String help_text = wizardi18n.getString("haoracle.summarypanel.help");
        HashMap confMap = new HashMap();

        Integer preference = (Integer) wizardModel.getWizardValue(
                Util.DB_COMPONENTS);
        confMap.put(Util.DB_COMPONENTS, preference);

        int prefInt = preference.intValue();

        if ((prefInt == Util.SERVER) || (prefInt == Util.BOTH_S_L)) {
            bConfigServer = true;
        }

        if ((prefInt == Util.LISTENER) || (prefInt == Util.BOTH_S_L)) {
            bConfigListener = true;
        }

        // Title
        TTYDisplay.printTitle(title);

        // Summary panel table
        Vector summaryPanel = new Vector();

        wizardModel.selectCurrWizardContext();


        String serverRsName = null;
        String listenerRsName = null;

        HashMap tableMap = new HashMap();
        HashMap selMap = null;
        int row_count = 1;

        String rgName = (String) wizardModel.getWizardValue(Util.RAC_RG_NAME);
        summaryPanel.add(rg_name);
        summaryPanel.add(rgName);
        selMap = new HashMap();
        selMap.put(NAME, rg_name);
        selMap.put(VALUE, rgName);
        selMap.put(DESCRIPTION, rg_desc);
        selMap.put(COLON_DESC, rg_colon);
        tableMap.put(Integer.toString(row_count++), selMap);
        confMap.put(Util.RAC_RG_NAME, rgName);

        String nodeList[] = (String[]) wizardModel.getWizardValue(
                Util.RG_NODELIST);
        StringBuffer nodeNames = Util.convertArraytoStringBuffer(nodeList);
        summaryPanel.add(nodelist_name);
        summaryPanel.add(nodeNames.toString());
        selMap = new HashMap();
        selMap.put(NAME, nodelist_name);
        selMap.put(VALUE, nodeNames.toString());
        selMap.put(DESCRIPTION, nodelist_desc);
        selMap.put(COLON_DESC, nodelist_colon);
        tableMap.put(Integer.toString(row_count++), selMap);
        confMap.put(Util.RG_NODELIST, nodeList);

        confMap.put(Util.RAC_FRAMEWORK_RSNAME,
            (String) wizardModel.getWizardValue(Util.RAC_FRAMEWORK_RSNAME));
        confMap.put(Util.RAC_FRAMEWORK_RGNAME,
            (String) wizardModel.getWizardValue(Util.RAC_FRAMEWORK_RGNAME));

        // Oracle Home
        String selected_orahome = (String) wizardModel.getWizardValue(
                Util.SEL_ORACLE_HOME);
        confMap.put(Util.SEL_ORACLE_HOME, selected_orahome);

        if (bConfigServer) {

            // Server resource name
            serverRsName = (String) wizardModel.getWizardValue(
                    Util.RAC_SERVER_RS_NAME);
            summaryPanel.add(server_rs_name);
            summaryPanel.add(serverRsName);
            selMap = new HashMap();
            selMap.put(NAME, server_rs_name);
            selMap.put(VALUE, serverRsName);
            selMap.put(DESCRIPTION, server_rs_desc);
            selMap.put(COLON_DESC, server_rs_colon);
            tableMap.put(Integer.toString(row_count++), selMap);
            confMap.put(Util.RAC_SERVER_RS_NAME, serverRsName);

            // Per Node Properties
            String propertyNames[] = (String[]) wizardModel.getWizardValue(
		Util.SCALABLE_SERVER_PROPS);
            confMap.put(Util.SCALABLE_SERVER_PROPS, propertyNames);

            for (int j = 0; j < nodeList.length; j++) {
                String curNode = nodeList[j];
                String tmpArry[] = curNode.split(Util.COLON);
                curNode = tmpArry[0];

                // Oracle SID
                String selected_orasid = (String) wizardModel.getWizardValue(
                        Util.SEL_ORACLE_SID + curNode);
                confMap.put(Util.SEL_ORACLE_SID + curNode, selected_orasid);

                // Other extension properties
                for (int i = 0; i < propertyNames.length; i++) {

                    // Filter already set properties
                    if (!propertyNames[i].equals(Util.RG_NAME) &&
			!propertyNames[i].equals(Util.RS_NAME) &&
			!propertyNames[i].equals(Util.NODELIST) &&
			!propertyNames[i].equals(Util.SERVER_ORACLE_HOME) &&
			!propertyNames[i].equals(Util.SERVER_PREFIX + Util.ORACLE_SID) &&
			propertyNames[i].startsWith(Util.SERVER_PREFIX)) {
                        ResourceProperty prop = (ResourceProperty) wizardModel
                            .getWizardValue(propertyNames[i] + curNode);

                        if (prop == null) {
                            prop = (ResourceProperty) wizardModel
                                .getWizardValue(propertyNames[i]);
                        }

                        String val = ResourcePropertyUtil.getAbsValue(prop);

                        if ((val != null) && (val.trim().length() > 0)) {
                            confMap.put(propertyNames[i] + curNode, prop);
                        }
                    }
                }
            }

            String storageRes[] = (String[]) wizardModel.getWizardValue(
                    Util.SEL_STORAGE);

            if ((storageRes != null) && (storageRes.length > 0)) {
                StringBuffer storageResVal = Util.convertArraytoStringBuffer(
                        storageRes);
                summaryPanel.add(storage_rs_name);
                summaryPanel.add(storageResVal.toString());
                selMap = new HashMap();
                selMap.put(NAME, storage_rs_name);
                selMap.put(VALUE, storageResVal.toString());
                selMap.put(DESCRIPTION, storage_rs_desc);
                selMap.put(COLON_DESC, storage_rs_colon);
                tableMap.put(Integer.toString(row_count++), selMap);
                confMap.put(Util.SEL_STORAGE, storageRes);
            }

            confMap.put(Util.SCAL_DEVGROUP_RSLIST,
                (List) wizardModel.getWizardValue(Util.SCAL_DEVGROUP_RSLIST));
            confMap.put(Util.SCAL_MOUNTPOINT_RSLIST,
                (List) wizardModel.getWizardValue(Util.SCAL_MOUNTPOINT_RSLIST));
        }

        if (bConfigListener) {
            listenerRsName = (String) wizardModel.getWizardValue(
                    Util.RAC_LISTENER_RS_NAME);
            summaryPanel.add(listener_rs_name);
            summaryPanel.add(listenerRsName);
            selMap = new HashMap();
            selMap.put(NAME, listener_rs_name);
            selMap.put(VALUE, listenerRsName);
            selMap.put(DESCRIPTION, listener_rs_desc);
            selMap.put(COLON_DESC, listener_rs_colon);
            tableMap.put(Integer.toString(row_count++), selMap);
            confMap.put(Util.RAC_LISTENER_RS_NAME, listenerRsName);

            String propertyNames[] = (String[]) wizardModel.getWizardValue(
                    Util.SCALABLE_LISTENER_PROPS);
            confMap.put(Util.SCALABLE_LISTENER_PROPS, propertyNames);

            // Logical host is required only for listener resource
            for (int i = 0; i < nodeList.length; i++) {
                String curNode = nodeList[i];
                String tmpArry[] = curNode.split(Util.COLON);
                curNode = tmpArry[0];

                // All extension properties
                for (int k = 0; k < propertyNames.length; k++) {

                    // Filter already set properties
                    if (!propertyNames[k].equals(Util.RG_NAME) &&
                            !propertyNames[k].equals(Util.RS_NAME) &&
                            !propertyNames[k].equals(Util.NODELIST) &&
                            !propertyNames[k].equals(
                                Util.LISTENER_ORACLE_HOME) &&
                            propertyNames[k].startsWith(Util.LISTENER_PREFIX)) {
                        ResourceProperty prop = (ResourceProperty) wizardModel
                            .getWizardValue(propertyNames[k] + curNode);

                        if (prop == null) {
                            prop = (ResourceProperty) wizardModel
                                .getWizardValue(propertyNames[k]);
                        }

                        String val = ResourcePropertyUtil.getAbsValue(prop);

                        if ((val != null) && (val.trim().length() > 0)) {
                            confMap.put(propertyNames[k] + curNode, prop);
                        }
                    }
                }

                String lhrs_name = wizardi18n.getString(
                        "oraclerac.summaryPanel.lh.rsname.name",
                        new String[] { curNode });
                String lhrs_desc = wizardi18n.getString(
                        "oraclerac.summaryPanel.lh.rsname.desc",
                        new String[] { curNode });
                String lhrs_colon = wizardi18n.getString(
                        "oraclerac.summaryPanel.lh.rsname.colon",
                        new String[] { curNode });

                String netif_name = wizardi18n.getString(
                        "oraclerac.summaryPanel.netiflist.name",
                        new String[] { curNode });
                String netif_desc = wizardi18n.getString(
                        "oraclerac.summaryPanel.netiflist.desc",
                        new String[] { curNode });
                String netif_colon = wizardi18n.getString(
                        "oraclerac.summaryPanel.netiflist.colon",
                        new String[] { curNode });

                String hostname_name = wizardi18n.getString(
                        "oraclerac.summaryPanel.hostnamelist.name",
                        new String[] { curNode });
                String hostname_desc = wizardi18n.getString(
                        "oraclerac.summaryPanel.hostnamelist.desc",
                        new String[] { curNode });
                String hostname_colon = wizardi18n.getString(
                        "oraclerac.summaryPanel.hostnamelist.colon",
                        new String[] { curNode });

                Vector existingLHRS = (Vector) wizardModel.getWizardValue(
                        Util.SEL_LH_RS + curNode);
                String lhRsName = (String) wizardModel.getWizardValue(
                        Util.SEL_LH_RSNAMES + curNode);

                Boolean BNewLH = (Boolean) wizardModel.getWizardValue(
                        Util.NEW_LH_RS + curNode);

                String allLhRsName = lhRsName;

                if (BNewLH.booleanValue() && (existingLHRS != null)) {

                    // if new LH resource
                    StringBuffer lhStringBuffer = Util.convertArraytoStringBuffer(
                            (String[]) existingLHRS.toArray(new String[0]));

                    if (lhStringBuffer.length() > 0) {
                        lhStringBuffer.append(Util.COMMA);
                    }

                    lhStringBuffer.append(allLhRsName);
                    allLhRsName = lhStringBuffer.toString();
                }

                summaryPanel.add(lhrs_name);
                summaryPanel.add(allLhRsName);
                selMap = new HashMap();
                selMap.put(NAME, lhrs_name);
                selMap.put(VALUE, allLhRsName);
                selMap.put(DESCRIPTION, lhrs_desc);
                selMap.put(COLON_DESC, lhrs_colon);
                tableMap.put(Integer.toString(row_count++), selMap);
                confMap.put(Util.SEL_LH_RS + curNode, existingLHRS);
                confMap.put(Util.SEL_LH_RSNAMES + curNode, lhRsName);
                confMap.put(Util.SEL_LH_RG + curNode,
                    (String) wizardModel.getWizardValue(
                        Util.SEL_LH_RG + curNode));

                String hostNameList[] = (String[]) wizardModel.getWizardValue(
                        Util.HOSTNAMELIST + curNode);
                StringBuffer hostNames = Util.convertArraytoStringBuffer(
                        hostNameList);

                if ((hostNameList != null) && (hostNameList.length > 0)) {
                    summaryPanel.add(hostname_name);
                    summaryPanel.add(hostNames.toString());
                    selMap = new HashMap();
                    selMap.put(NAME, hostname_name);
                    selMap.put(VALUE, hostNames.toString());
                    selMap.put(DESCRIPTION, hostname_desc);
                    selMap.put(COLON_DESC, hostname_colon);
                    tableMap.put(Integer.toString(row_count++), selMap);
                    confMap.put(Util.HOSTNAMELIST + curNode, hostNameList);
                }

                String netifList[] = (String[]) wizardModel.getWizardValue(
                        LogicalHostWizardConstants.SEL_NETIFLIST + curNode);

                if ((netifList != null) && (netifList.length > 0)) {
                    StringBuffer netifs = Util.convertArraytoStringBuffer(netifList);
                    summaryPanel.add(netif_name);
                    summaryPanel.add(netifs.toString());
                    selMap = new HashMap();
                    selMap.put(NAME, netif_name);
                    selMap.put(VALUE, netifs.toString());
                    selMap.put(DESCRIPTION, netif_desc);
                    selMap.put(COLON_DESC, netif_colon);
                    tableMap.put(Integer.toString(row_count++), selMap);
                    confMap.put(Util.NETIFLIST + curNode, netifList);
                }
            }
        }

	// pass the zone cluster name or null for base cluster
        String selZoneCluster = CliUtil.getZoneClusterName(wizardModel);
	confMap.put(Util.SEL_ZONE_CLUSTER, selZoneCluster);

        TTYDisplay.pageText(desc);
        TTYDisplay.clear(1);

        String subheadings[] = { heading1, heading2 };

        HashMap customTags = new HashMap();
        customTags.put(c_char, create_txt);
        customTags.put(q_char, quit_txt);

        boolean createconfig = false;

        while (!createconfig) {
            TTYDisplay.setNavEnabled(true);
            this.cancelDirection = false;

            String option = TTYDisplay.create2DTable(table_title, subheadings,
                    summaryPanel, customTags, help_text);

            if (option.equals(back)) {
                this.cancelDirection = true;

                return;
            }

            TTYDisplay.clear(3);
            TTYDisplay.setNavEnabled(false);

            if (!option.equals(c_char) && !option.equals(q_char)) {
                selMap = (HashMap) tableMap.get(option);

                String name = (String) selMap.get(NAME);
                String value = (String) selMap.get(VALUE);
                String desc = (String) selMap.get(DESCRIPTION);
                String colon_desc = (String) selMap.get(COLON_DESC);
                displayProp(panel_desc, desc, colon_desc, value);
            } else if (option.equals(c_char)) {
                String confirm = yes;

                if (confirm.equalsIgnoreCase(yes)) {
                    createconfig = true;
                    confMap.put(Util.CONFIGURATION_TYPE, Util.RAC_9I);
                    generateCommands(confMap);
                }
            } else if (option.equals(q_char)) {
                String exitOpt = TTYDisplay.getConfirmation(exit_txt, yes, no);

                if (exitOpt.equalsIgnoreCase(yes)) {
                    System.exit(0);
                }
            }
        }
    }

    /**
     * Generates the commands by invoking the command generator
     */
    private void generateCommands(Map confMap) {
        wizardModel.setWizardValue(Util.CONFMAP, confMap);

        List commandList = null;
        Thread t = null;

	RACModel racModel = RACInvokerCreator.getRACModel();
	String nodeEndPoint = (String) wizardModel.getWizardValue(
	    Util.NODE_TO_CONNECT);

        try {
            t = TTYDisplay.busy(wizardi18n.getString(
                        "cliwizards.executingMessage"));
            t.start();
            commandList = racModel.generateCommands(null, nodeEndPoint, confMap);
        } catch (CommandExecutionException cee) {
            commandList = racModel.getCommandList(null, nodeEndPoint);
            wizardModel.setWizardValue(Util.EXITSTATUS, cee);
        } finally {
            wizardModel.setWizardValue(Util.CMDLIST, commandList);

            try {
                t.interrupt();
                t.join();
            } catch (Exception e) {
            }
        }
    }

    private void displayProp(String panel_desc, String desc, String colon,
        String value) {

        // Display nodelist
        TTYDisplay.pageText(panel_desc + desc);
        TTYDisplay.clear(1);
        TTYDisplay.pageText(colon + value);
    }
}
