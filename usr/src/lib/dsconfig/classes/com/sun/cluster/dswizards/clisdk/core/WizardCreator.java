/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)WizardCreator.java 1.18     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.clisdk.core;

// JAVA

// CMASS
import com.sun.cluster.agent.dataservices.utils.Util;

// CLI Wizards
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.common.WizardStateManager;

import java.util.Iterator;
import java.util.Set;

import javax.management.remote.JMXConnector;


/**
 * WizardCreator is responsible for creating a wizard tree
 */
public class WizardCreator {

    /**
     * String that holds the localized error message for Package installation
     * errors. As the getMBeanonNode methods in the individual Wizards are
     * static themselves We need a localized static reference to the
     * errorMessages
     */
    protected static String pkgErrorString = null;

    /** String that holds the localized message for "Press any key to exit" */
    protected static String wizardExitPrompt = null;

    /** This is the root of the client tree. */
    protected WizardComposite root = new WizardComposite();

    /** The wizardModel reference for the CLI wizard */
    protected CliDSConfigWizardModel wizardModel;

    /** The wizardFlow reference for the CLI wizard */
    protected WizardFlow wizardFlow;

    /** Wizard Tree manager object. */
    protected WizardTreeManager wizardManager = null;

    /** The state file to use for populating the wizard state. */
    protected String stateFileName = null;

    /** The resource bundle to use. */
    protected WizardI18N wizardi18n = null;

    /** The resource bundle name */
    private String resourceBundleName =
        "com.sun.cluster.dswizards.common.WizardResource";

    /** Refernce to this Wizard's state manager */
    private WizardStateManager stateManager = null;

    /**
     * Create a WizardCreator.
     */
    public WizardCreator(String stateFileName, String flowXML) {
        this.stateFileName = stateFileName;
        this.stateManager = new WizardStateManager(stateFileName);
        wizardFlow = new WizardFlow(flowXML);
        wizardi18n = getWizardI18N();
        this.wizardModel = new CliDSConfigWizardModel(stateManager);
        pkgErrorString = wizardi18n.getString("dswizards.error.pkgNotFound");
        wizardExitPrompt = wizardi18n.getString("cliwizards.continueExit");
        TTYDisplay.initialize();
        createClientTree();
        startCommandLine();
    }

    /**
     * Returns the WizardI18N object
     *
     * @return  the resourcebundle object associated with this wizard
     */
    public WizardI18N getWizardI18N() {

        if (this.wizardi18n == null) {
            wizardi18n = new WizardI18N(resourceBundleName);
        }

        return this.wizardi18n;
    }

    /**
     * Returns the CliDSConfigWizardModel object
     *
     * @return  the wizardModel object associated with this wizard
     */
    public CliDSConfigWizardModel getWizardModel() {
        return (wizardModel);
    }

    /**
     * Sets the wizardModel object.
     *
     * @param  state  The wizard model
     */
    public void setWizardModel(CliDSConfigWizardModel model) {
        this.wizardModel = model;
    }

    public void setWizardTreeManager(WizardTreeManager wizardManager) {
        this.wizardManager = wizardManager;
    }

    public WizardTreeManager getWizardTreeManager() {
        return this.wizardManager;
    }

    /**
     * Returns the statefile name.
     */
    public String getStateFileName() {
        return stateFileName;
    }


    /**
     * Returns the root WizardComposite object
     */
    public WizardComposite getRoot() {
        return root;
    }

    /**
     * Sets the root WizardCommposite object
     *
     * @param  newRoot  The WizardComposite
     */
    public void setRoot(WizardComposite newRoot) {
        this.root = newRoot;
    }

    /**
     * Create the wizard client tree.  The default behavior only provides the
     * root of the tree.
     */
    protected void createClientTree() {
    }

    /**
     * Begin a command line wizard session.
     */
    protected void startCommandLine() {
        CommandLineConsole wizardConsole = new CommandLineConsole(this);
        wizardConsole.start();
    }
}
