/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ReplicaPanel.java 1.13     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.sapwebas;

// J2SE
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

// CMAS
import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.sapwebas.SapWebasMBean;
import com.sun.cluster.agent.dataservices.utils.DataServicesUtil;
import com.sun.cluster.agent.dataservices.utils.Util;

// CLI Wizard SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;

// Wizard Common
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;


public class ReplicaPanel extends WizardLeaf {

    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N wizardi18n; // Message Bundle


    /**
     * Creates a new instance of ReplicaPanel
     */
    public ReplicaPanel() {
        super();
    }

    /**
     * Creates a ReplicaPanel Panel with the given name and tree manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public ReplicaPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    public ReplicaPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;

        // Get the I18N instance
        this.wizardi18n = wizardi18n;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {
        String title = wizardi18n.getString("sap.replicaPanel.title");
        String table_title = wizardi18n.getString(
                "sap.replicaPanel.table.title");
        String desc = wizardi18n.getString("sap.replicaPanel.desc");
        String help_text = wizardi18n.getString("sap.replicaPanel.help.cli");
        String yes = wizardi18n.getString("cliwizards.yes");
        String no = wizardi18n.getString("cliwizards.no");
        String continue_txt = wizardi18n.getString("cliwizards.continue");
        String back = wizardi18n.getString("ttydisplay.back.char");

        String repServerLoc = null;
        String repServerProfileLoc = null;
        Boolean repSelected;
        String sArr[] = new String[1];
        String option;
        String query_txt;
        String user_query_txt;
        String rep_query_txt;
        ErrorValue retValue;

        DataServicesUtil.clearScreen();

        // Title
        TTYDisplay.clear(1);
        TTYDisplay.printTitle(title);

        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        HashMap customTag = new HashMap();
        String menuKey = wizardi18n.getString("sap.sidPanel.custom.key");
        String menuText = wizardi18n.getString("sap.sidPanel.custom.keyText");
        customTag.put(menuKey, menuText);

        wizardModel.setWizardValue(SapWebasConstants.INSIDE_REP_COMPONENT,
            Boolean.FALSE);

        String sapSid = (String) wizardModel.getWizardValue(Util.SAP_SID);
        TTYDisplay.clear(1);
        TTYDisplay.setNavEnabled(true);

        do { // Ask for Replica Server Location
            user_query_txt = wizardi18n.getString(
                    "sap.replicaPanel.repServer.cli.query");
            TTYDisplay.clear(1);
            TTYDisplay.setNavEnabled(true);

            String defaultName = SapWebasWizardCreator.discoverOption(
                    wizardModel, Util.REP_LOC, Util.REP_SERVER_LOC, sapSid);
            String enteredValue = null;

            if (defaultName != null) {
                enteredValue = TTYDisplay.promptForEntry(user_query_txt,
                        defaultName, help_text);
            } else {
                enteredValue = TTYDisplay.promptForEntry(user_query_txt,
                        help_text);
            }

            if (enteredValue.equalsIgnoreCase(
                        wizardi18n.getString("ttydisplay.back.char"))) {
                this.cancelDirection = true;

                return;
            }

            while (enteredValue.length() <= 0) {
                TTYDisplay.printError(wizardi18n.getString(
                        "sap.replicaPanel.repServer.invalid"));
                enteredValue = TTYDisplay.promptForEntry(user_query_txt);
            }

            sArr[0] = enteredValue;
            TTYDisplay.clear(1);
            TTYDisplay.setNavEnabled(false);
            retValue = new ErrorValue();
            retValue = SapWebasWizardCreator.validateData(Util.REP_LOC,
                    enteredValue, wizardModel);

            if (retValue.getReturnValue().booleanValue()) {
                wizardModel.setWizardValue(Util.REP_SERVER_LOC, enteredValue);
            } else {
                TTYDisplay.printWarning(wizardi18n.getString(
                        "sap.replicaPanel.repServer.invalid"));
                TTYDisplay.printMoreText(retValue.getErrorString());
                TTYDisplay.clear(1);

                String confirm = TTYDisplay.getConfirmation(wizardi18n
                        .getString("cliwizards.validation.override"), no, yes);

                if (confirm.equalsIgnoreCase(yes)) {
                    wizardModel.selectCurrWizardContext();
                    wizardModel.setWizardValue(Util.REP_SERVER_LOC,
                        enteredValue);

                    break;
                }
            }
        } while (!retValue.getReturnValue().booleanValue());

        String optionsArr[] = SapWebasWizardCreator.discoverOptions(wizardModel,
                Util.REP_PROF, SapWebasConstants.DISCOVERD_REP_PROFILES,
                sapSid);

        do { // Ask for Rep Profile Location

            String defRepNo = (String) wizardModel.getWizardValue(
                    Util.REP_SERVER_PROFILE_LOC);
            TTYDisplay.setNavEnabled(true);
            query_txt = wizardi18n.getString("sap.rep_prof.select");
            rep_query_txt = wizardi18n.getString("sap.rep_prof.query");

            String rep_prof;

            if (optionsArr != null) {
                List selList = TTYDisplay.getScrollingOption(query_txt,
                        optionsArr, customTag, defRepNo, help_text, true);

                if (selList == null) {
                    this.cancelDirection = true;

                    return;
                }

                TTYDisplay.clear(1);

                String selectList[] = (String[]) selList.toArray(new String[0]);
                rep_prof = selectList[0];
            } else {
                rep_prof = menuText;
            }

            if (rep_prof.equals(menuText)) {
                TTYDisplay.setNavEnabled(false);

                if (defRepNo != null) {
                    rep_prof = TTYDisplay.promptForEntry(rep_query_txt,
                            defRepNo, help_text);
                } else {
                    rep_prof = TTYDisplay.promptForEntry(rep_query_txt,
                            help_text);
                }
            }

            while (rep_prof.length() <= 0) {
                TTYDisplay.printError(wizardi18n.getString(
                        "sap.rep_prof.invalid"));
                rep_prof = TTYDisplay.promptForEntry(rep_query_txt);
            }

            sArr[0] = rep_prof;
            TTYDisplay.clear(1);
            TTYDisplay.setNavEnabled(false);
            retValue = new ErrorValue();
            retValue = SapWebasWizardCreator.validateData(Util.REP_PROF,
                    rep_prof, wizardModel);

            if (retValue.getReturnValue().booleanValue()) {
                wizardModel.setWizardValue(Util.REP_SERVER_PROFILE_LOC,
                    rep_prof);
            } else {
                TTYDisplay.printWarning(wizardi18n.getString(
                        "sap.rep_prof.invalid"));
                TTYDisplay.printMoreText(retValue.getErrorString());
                TTYDisplay.clear(1);

                String confirm = TTYDisplay.getConfirmation(wizardi18n
                        .getString("cliwizards.validation.override"), no, yes);

                if (confirm.equalsIgnoreCase(yes)) {
                    wizardModel.selectCurrWizardContext();
                    wizardModel.setWizardValue(Util.REP_SERVER_PROFILE_LOC,
                        rep_prof);

                    break;
                }
            }
        } while (!retValue.getReturnValue().booleanValue());

        wizardModel.setWizardValue(SapWebasConstants.INSIDE_REP_COMPONENT,
            Boolean.TRUE);
    }
}
