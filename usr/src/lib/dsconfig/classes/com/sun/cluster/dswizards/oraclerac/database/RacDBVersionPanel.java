/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)RacDBVersionPanel.java 1.9     08/12/10 SMI"
 */


package com.sun.cluster.dswizards.oraclerac.database;

import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;

import java.lang.Integer;

import java.util.HashMap;


/**
 * The Panel Class that gets a Hostname or list of Hostnames as input from the
 * User.
 */
public class RacDBVersionPanel extends RacDBBasePanel {


    /**
     * Creates a HostnameEntry Panel with the given name and Wizard state
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     */
    public RacDBVersionPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(name, model, wizardFlow, wizardi18n);
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.
     *		WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        title = wizardi18n.getString("oraclerac.versionPanel.title");

        String preference = wizardi18n.getString(
                "oraclerac.versionPanel.preference");
        String option1 = wizardi18n.getString("oraclerac.versionPanel.option1");
        String option2 = wizardi18n.getString("oraclerac.versionPanel.option2");
        int defSelection = 0;

        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        // Title
        TTYDisplay.printTitle(title);

        String options[] = { option1, option2 };

        // Get the data preference
        String pref = (String) wizardModel.getWizardValue(Util.RAC_VER);

        if ((pref == null) || pref.equals(Util.NINE)) {
            defSelection = 0;
        } else {
            defSelection = 1;
        }

        int index = TTYDisplay.getMenuOption(preference, options, defSelection);

        if (index == TTYDisplay.BACK_PRESSED) {
            this.cancelDirection = true;

            return;
        }

        String selection = options[index];

        if (selection.equals(option1)) {
            pref = new String(Util.NINE);
        } else if (selection.equals(option2)) {
            pref = new String(Util.TEN);
        }

        wizardModel.selectCurrWizardContext();
        wizardModel.setWizardValue(Util.RAC_VER, pref);
    }
}
