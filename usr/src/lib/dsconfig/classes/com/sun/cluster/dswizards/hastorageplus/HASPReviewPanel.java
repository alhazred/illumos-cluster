/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)HASPReviewPanel.java 1.24     09/01/16 SMI"
 */


package com.sun.cluster.dswizards.hastorageplus;

import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.hastorageplus.HAStoragePlusMBean;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.dataservices.utils.VfsStruct;
import com.sun.cluster.agent.rgm.property.ResourceProperty;
import com.sun.cluster.agent.rgm.property.ResourcePropertyBoolean;
import com.sun.cluster.agent.rgm.property.ResourcePropertyEnum;
import com.sun.cluster.agent.rgm.property.ResourcePropertyInteger;
import com.sun.cluster.agent.rgm.property.ResourcePropertyString;
import com.sun.cluster.agent.rgm.property.ResourcePropertyStringArray;
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.ResourcePropertyUtil;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;

import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.management.remote.JMXConnector;

public class HASPReviewPanel extends WizardLeaf {

    private static String TRUE = "true";
    private static String FALSE = "false";
    private String panelName; // Panel Name
    private WizardI18N wizardi18n; // Message Bundle
    private CliDSConfigWizardModel wizardModel; // WizardModel

    private HAStoragePlusMBean mbean = null;
    private JMXConnector localConnection = null;

    /**
     * Creates a new instance of HASPReviewPanel
     */
    public HASPReviewPanel() {
        super();
    }

    /**
     * Creates a HASPReviewPanel Panel with the given name and manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public HASPReviewPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    public HASPReviewPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;
        this.wizardi18n = wizardi18n;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.
     *		WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        String back = wizardi18n.getString("ttydisplay.back.char");
        String yes = wizardi18n.getString("cliwizards.yes");
        String no = wizardi18n.getString("cliwizards.no");
        String title = wizardi18n.getString("hasp.reviewpanel.title");
        String description = wizardi18n.getString("hasp.reviewpanel.desc1");
        String rsname_txt = wizardi18n.getString(
                "hasp.reviewpanel.resource.name");
        String rgname_txt = wizardi18n.getString("hasp.reviewpanel.rg.name");
        String table_title = wizardi18n.getString("hasp.reviewpanel.desc2");
        String heading1 = wizardi18n.getString("hasp.reviewpanel.heading1");
        String heading2 = wizardi18n.getString("hasp.reviewpanel.heading2");
        String dchar = (String) wizardi18n.getString("ttydisplay.d.char");
        String done = wizardi18n.getString("ttydisplay.menu.done");
        String propNameText = wizardi18n.getString("hasp.reviewpanel.propname");
        String propTypeText = wizardi18n.getString("hasp.reviewpanel.proptype");
        String propDescText = wizardi18n.getString("hasp.reviewpanel.propdesc");
        String currValText = wizardi18n.getString("hasp.reviewpanel.currval");
        String newValText = wizardi18n.getString("hasp.reviewpanel.newval");
        String rsname_error = wizardi18n.getString(
                "hasp.reviewpanel.resourcename.error");
        String confirm_txt = wizardi18n.getString("cliwizards.enterAgain");
        String rgname_error = wizardi18n.getString(
                "hasp.reviewpanel.rgname.error");
        String help_text = wizardi18n.getString("hasp.reviewpanel.cli.help");
        String continue_txt = wizardi18n.getString("cliwizards.continue");

        // Title
        TTYDisplay.printTitle(title);

        // Review panel table
        Vector reviewPanel = new Vector();

        // Generate the Resource and the ResourceGroup names
        // get the user selections
        Boolean sqfsSelected = (Boolean) wizardModel.getWizardValue(
                HASPWizardConstants.SQFS_SELECTED);
        String fsSelections[] = (String[]) wizardModel.getWizardValue(
                HASPWizardConstants.HASP_FILESYSTEMS_SELECTED);
        String devSelections[] = (String[]) wizardModel.getWizardValue(
                HASPWizardConstants.HASP_DEVICES_SELECTED);

        // Discover the Names using the above pattern
        // Resource Name
        localConnection = getConnection(Util.getClusterEndpoint());
        mbean = HASPWizardCreator.getHAStoragePlusMBean(localConnection);

        String properties[] = { Util.RS_NAME };
        HashMap helperData = new HashMap();
        helperData.put(Util.HASP_ALL_FILESYSTEMS, fsSelections);
        if (sqfsSelected == null) {
            // it's not shared QFS, DG selection is possible.
            helperData.put(Util.HASP_ALL_DEVICES, devSelections);
        }
        HashMap discoveredMap = mbean.discoverPossibilities(properties,
                helperData);

        ResourceProperty prop = null;

        String rsName = (String) discoveredMap.get(Util.RS_NAME);

        // Resource Group Name
        properties = new String[] { Util.RG_NAME };
        discoveredMap = mbean.discoverPossibilities(properties, helperData);

        String rgName = (String) discoveredMap.get(Util.RG_NAME);

        // Set the discovered values as default
        wizardModel.setWizardValue(Util.RG_NAME, rgName);

        wizardModel.setWizardValue(Util.RS_NAME, rsName);

        String option = "";
        while (!option.equals(dchar)) {
            TTYDisplay.setNavEnabled(true);
            this.cancelDirection = false;

            reviewPanel.removeAllElements();
            wizardModel.selectCurrWizardContext();

            // First Row - Resource Name
            rsName = (String) wizardModel.getWizardValue(Util.RS_NAME);
            reviewPanel.add(rsname_txt);
            reviewPanel.add(rsName);

            // Second Row - RG Name
            rgName = (String) wizardModel.getWizardValue(Util.RG_NAME);
            reviewPanel.add(rgname_txt);
            reviewPanel.add(rgName);

            TTYDisplay.pageText(description);
            TTYDisplay.clear(1);

            String subheadings[] = { heading1, heading2 };

            HashMap customTags = new HashMap();
            customTags.put(dchar, done);
            option = TTYDisplay.create2DTable(table_title, subheadings,
                    reviewPanel, customTags, help_text);

            String desc;

            if (option.equals(back)) {
                this.cancelDirection = true;

                return;
            }

            TTYDisplay.clear(3);
            TTYDisplay.setNavEnabled(false);

            String sprop = null;
            String currVal = null;

            if (option.equals("1")) {

                // Change resource name
                sprop = "resource";
                currVal = rsName;
            } else if (option.equals("2")) {

                // Change rg name
                sprop = "rg";
                currVal = rgName;
            }

            if (option.equals("1") || (option.equals("2"))) {
                TTYDisplay.pageText(propNameText +
                    wizardi18n.getString(
                        "hasp.reviewpanel." + sprop + ".name"));

                TTYDisplay.pageText(propDescText +
                    wizardi18n.getString(
                        "hasp.reviewpanel." + sprop + ".desc"));

                TTYDisplay.pageText(propTypeText +
                    wizardi18n.getString(
                        "hasp.reviewpanel." + sprop + ".type"));

                TTYDisplay.pageText(currValText + currVal);
            }

            // Prompt for new values for each of the options
            if (option.equals("1")) {
                String tmp_rsName = null;
                boolean error = true;
                String enteragain = yes;

                while (error && enteragain.equals(yes)) {
                    tmp_rsName = TTYDisplay.promptForEntry(newValText);

                    if ((tmp_rsName != null) &&
                            (tmp_rsName.trim().length() != 0)) {

                        // call validate input
                        properties = new String[] { Util.RS_NAME };

                        HashMap userInputs = new HashMap();
                        userInputs.put(Util.RS_NAME, tmp_rsName);

                        ErrorValue retVal = validateInput(properties,
                                userInputs, null);

                        if (!retVal.getReturnValue().booleanValue()) {
                            TTYDisplay.printError(rsname_error);

                            enteragain = TTYDisplay.getConfirmation(confirm_txt,
                                    yes, no);
                        } else {
                            error = false;
                            wizardModel.setWizardValue(Util.RS_NAME,
                                tmp_rsName);
                        }
                    }
                } // while
            } else if (option.equals("2")) {
                String tmp_rgName = null;
                boolean error = true;
                String enteragain = yes;

                while (error && enteragain.equals(yes)) {
                    tmp_rgName = TTYDisplay.promptForEntry(newValText);

                    if ((tmp_rgName != null) &&
                            (tmp_rgName.trim().length() != 0)) {

                        // call validate input
                        properties = new String[] { Util.RG_NAME };

                        HashMap userInputs = new HashMap();
                        userInputs.put(Util.RG_NAME, tmp_rgName);

                        ErrorValue retVal = validateInput(properties,
                                userInputs, null);

                        if (!retVal.getReturnValue().booleanValue()) {
                            TTYDisplay.printError(rgname_error);
                            enteragain = TTYDisplay.getConfirmation(confirm_txt,
                                    yes, no);
                        } else {
                            error = false;
                            wizardModel.setWizardValue(Util.RG_NAME,
                                tmp_rgName);
                        }
                    }
                } // while
            } else if (!option.equals(dchar)) {

                // extension property update
                // get the extension property name based on the selection
                int optionNum = -1;

                try {
                    optionNum = Integer.valueOf(option).intValue() - 1;
                } catch (NumberFormatException nfe) {
                }

                optionNum *= 2;

                String propVal = (String) reviewPanel.elementAt(optionNum);

                // get resource property name
                prop = (ResourceProperty) wizardModel.getWizardValue(propVal);

                ErrorValue retVal = displayProperty(prop);

                if (!retVal.getReturnValue().booleanValue()) {
                    TTYDisplay.printError(retVal.getErrorString());
                }
            }

            TTYDisplay.clear(3);
        }

    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#isDisplayComplete
     */
    public boolean isDisplayComplete() {
        return true;
    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }

    private ErrorValue validateInput(String propNames[], HashMap userInput,
        Object helperData) {
        ErrorValue retValue = null;
        mbean = HASPWizardCreator.getHAStoragePlusMBean(localConnection);
        retValue = mbean.validateInput(propNames, userInput, helperData);

        return retValue;
    }

    private ErrorValue displayProperty(ResourceProperty prop) {
        String propNameText = (String) wizardi18n.getString(
                "hasp.reviewpanel.propname");
        String propTypeText = (String) wizardi18n.getString(
                "hasp.reviewpanel.proptype");
        String propDescText = (String) wizardi18n.getString(
                "hasp.reviewpanel.propdesc");
        String currValText = (String) wizardi18n.getString(
                "hasp.reviewpanel.currval");
        String newValText = (String) wizardi18n.getString(
                "hasp.reviewpanel.newval");

        String propName = prop.getName();
        TTYDisplay.pageText(propNameText + propName);
        TTYDisplay.pageText(propDescText + prop.getDescription());
        TTYDisplay.pageText(propTypeText + ResourcePropertyUtil.getType(prop));
        TTYDisplay.pageText(currValText + ResourcePropertyUtil.getValue(prop));

        String value = TTYDisplay.promptForEntry(newValText);

        return validate(prop, value);
    }

    public ErrorValue validate(ResourceProperty prop, String propVal) {

        // Check if a value has been set
        ErrorValue result = null;
        boolean error = false;
        String detail = wizardi18n.getString(
                "hasp.reviewpanel.error.input.summary");

        String propName = prop.getName();

        if (prop.isRequired() &&
                ((propVal == null) || (propVal.length() == 0))) {
            error = true;
            detail = wizardi18n.getString("hasp.reviewpanel.error.input.detail",
                    new String[] { propName });
        }

        if (error) {
            result = new ErrorValue(new Boolean(false), detail);

            return result;
        }

        try {

            if (prop instanceof ResourcePropertyString) {
                ResourcePropertyString rps = (ResourcePropertyString) prop;

                if ((propVal != null) && (propVal.length() != 0)) {
                    rps.setValue(propVal);
                } else {

                    // Will come here only if the property is not a required
                    // field.
                    String defValue = rps.getDefaultValue();

                    if ((defValue != null) && (defValue.length() != 0)) {
                        rps.setValue(rps.getDefaultValue());
                    } else {
                        rps.setValue("");
                        rps.setDefaultValue("");
                    }
                }
            } else if (prop instanceof ResourcePropertyInteger) {
                ResourcePropertyInteger rpi = (ResourcePropertyInteger) prop;

                if ((propVal != null) && (propVal.length() != 0)) {
                    rpi.setValue(new Integer(propVal));
                } else {
                    rpi.setValue(rpi.getDefaultValue());
                }
            } else if (prop instanceof ResourcePropertyBoolean) {
                ResourcePropertyBoolean rpb = (ResourcePropertyBoolean) prop;

                if ((propVal != null) && (propVal.length() != 0)) {

                    // make sure the user enters true or false
                    if (propVal.equalsIgnoreCase(TRUE) ||
                            propVal.equalsIgnoreCase(FALSE)) {

                        // Check for Affinity on property for local mount
                        if (prop.getName().equals(Util.AFFINITY_ON) &&
                                !validateAffinityOn(propVal)) {
                            throw new IllegalArgumentException(wizardi18n
                                .getString(
                                    "hasp.reviewpanel.affinityon.error"));
                        }

                        rpb.setValue(Boolean.valueOf(propVal));
                    } else {
                        throw new IllegalArgumentException("");
                    }
                } else {
                    rpb.setValue(rpb.getDefaultValue());
                }
            } else if (prop instanceof ResourcePropertyEnum) {
                ResourcePropertyEnum rpe = (ResourcePropertyEnum) prop;
                rpe.setValue(propVal);
            } else if (prop instanceof ResourcePropertyStringArray) {
                ResourcePropertyStringArray rpsa = (ResourcePropertyStringArray)
                    prop;

                if ((propVal != null) && (propVal.length() != 0)) {
                    String newValue[] = null;
                    StringTokenizer st = new StringTokenizer(propVal, ",");

                    if (st.hasMoreTokens()) {
                        newValue = new String[st.countTokens()];

                        for (int j = 0; st.hasMoreTokens(); j++) {
                            newValue[j] = st.nextToken();
                        }
                    }

                    rpsa.setValue(newValue);
                } else {
                    rpsa.setValue(rpsa.getDefaultValue());
                }
            }
        } catch (IllegalArgumentException iae) {

            if (iae.getMessage().equalsIgnoreCase("")) {
                detail += ResourcePropertyUtil.getPropertyErrorMessage(prop);
            } else {
                detail = iae.getMessage();
            }

            result = new ErrorValue(new Boolean(false), detail);

            return result;
        }

        wizardModel.setWizardValue(propName, prop);

        return new ErrorValue(new Boolean(true), "");
    }

    private boolean validateAffinityOn(String propVal) {

        boolean retVal = true;

        HashMap fsHashMap = (HashMap) wizardModel.getWizardValue(
                Util.HASP_ALL_FILESYSTEMS);
        VfsStruct vfstabEntries[] = (VfsStruct[]) fsHashMap.get(
                Util.VFSTAB_ENTRIES);

        for (int i = 0; i < vfstabEntries.length; i++) {

            if (!(vfstabEntries[i].getVfs_mntopts().indexOf(
                            Util.GLOBAL_OPTION) > 0) &&
                    (propVal.equalsIgnoreCase(FALSE))) {
                retVal = false;

                break;
            }

        }

        return retVal;
    }
}
