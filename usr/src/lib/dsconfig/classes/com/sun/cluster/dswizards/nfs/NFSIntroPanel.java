/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)NFSIntroPanel.java 1.8     08/12/10 SMI"
 */


package com.sun.cluster.dswizards.nfs;


import com.sun.cluster.agent.dataservices.utils.DataServicesUtil;

// CLI Wizard SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;


/**
 * Introduction page to the Hostname Wizards
 */
public class NFSIntroPanel extends WizardLeaf {

    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N wizardi18n; // Message Bundle


    /**
     * Creates a new instance of NFSIntroPanel
     */
    public NFSIntroPanel() {
        super();
    }

    /**
     * Creates a NFSIntro Panel with the given name and tree manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public NFSIntroPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates a NFSIntro Panel with the given name and associates it with the
     * WizardModel and WizardFlow
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     * @param  wizardi18n  Resource Bundle
     */
    public NFSIntroPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;

        // Get the I18N instance
        this.wizardi18n = wizardi18n;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {

        String title = wizardi18n.getString("nfs.introPanel.title");
        String desc = wizardi18n.getString("nfs.introPanel.desc");
        String back = wizardi18n.getString("ttydisplay.back.char");

        String continue_txt = wizardi18n.getString("cliwizards.continue");

        DataServicesUtil.clearScreen();

        // Title
        TTYDisplay.printTitle(title);
        TTYDisplay.clear(1);

        TTYDisplay.printSubTitle(desc);
        TTYDisplay.clear(1);
        TTYDisplay.printSubTitle(wizardi18n.getString("nfs.introPanel.req1"));
        TTYDisplay.printSubTitle(wizardi18n.getString("nfs.introPanel.req2"));
        TTYDisplay.printSubTitle(wizardi18n.getString("nfs.introPanel.req3"));
        TTYDisplay.clear(2);

        String response = TTYDisplay.promptForEntry(continue_txt);

        if (response.equals(back)) {
            System.exit(0);
        }
    }
}
