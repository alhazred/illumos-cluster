/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)RacDBPreferencePanel.java 1.9     08/12/10 SMI"
 */


package com.sun.cluster.dswizards.oraclerac.database.ver9i;

import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.oraclerac.database.*;

import java.util.HashMap;


/**
 * The Panel Class that gets the preference of server, listener or
 * server and listener from User.
 */
public class RacDBPreferencePanel extends RacDBBasePanel {


    /**
     * Creates a HostnameEntry Panel with the given name and Wizard state
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     */
    public RacDBPreferencePanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(name, model, wizardFlow, wizardi18n);
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.
     *		WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        title = wizardi18n.getString("oraclerac.prefPanel.title");

        String preference = wizardi18n.getString(
                "oraclerac.prefPanel.preference");
        String option1 = wizardi18n.getString("oraclerac.prefPanel.option1");
        String option2 = wizardi18n.getString("oraclerac.prefPanel.option2");
        String option3 = wizardi18n.getString("oraclerac.prefPanel.option3");

        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        // Title
        TTYDisplay.printTitle(title);

        String options[] = { option1, option2, option3 };

        // Get the data preference
        int index = TTYDisplay.getMenuOption(preference, options, 2);

        if (index == TTYDisplay.BACK_PRESSED) {
            this.cancelDirection = true;

            return;
        }

        Integer pref = null;
        String selection = options[index];

        if (selection.equals(option1)) {
            pref = new Integer(Util.SERVER);
        } else if (selection.equals(option2)) {
            pref = new Integer(Util.LISTENER);
        } else {
            pref = new Integer(Util.BOTH_S_L);
        }

        wizardModel.selectCurrWizardContext();
        wizardModel.setWizardValue(Util.DB_COMPONENTS, pref);
    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }
}
