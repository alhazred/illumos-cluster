/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)ORSReviewPanel.java	1.19	09/03/11 SMI"
 */

package com.sun.cluster.dswizards.oraclerac.storage;

// CMASS
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.dataservices.common.ErrorValue;

// CLI SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliUtil;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.oraclerac.invoker.RACInvokerCreator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.sun.cluster.model.RACModel;

/**
 * Review Panel for Oracle RAC Storage Wizard
 */

/*
 * In this panel we display the Resources that would be
 * created for RAC Storage. For each resource we display whether it is
 * "ScalMountPoint" or a "ScalDeviceGroup" resource.
 * This information is presented in a n*2 Table.
 *
 * On Selecting a particular resource, its information is displayed to the user
 * and the user can edit the editable information for that resource.
 *
 * The SELECTED_DG_RESOURCES and SELECTED_FS_RESOURCES in the wizardModel
 * give us the names of the seleted storage resources. For every Storage
 * Resourcewe have an INFO_MAP stored in the wizardModel which would give us the
 * required Information to be displayed
 */

public class ORSReviewPanel extends WizardLeaf {

    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N messages; // Message Bundle

    private String tableTitle = null;

    /**
     * Creates a new instance of ORSReviewPanel
     */
    public ORSReviewPanel() {
        super();
    }

    /**
     * Creates an ORSReviewPanel with the given name and tree manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public ORSReviewPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates an ORSReviewPanel for racstorage Wizard with the given name and
     * associates it with the WizardModel and WizardFlow
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     */
    public ORSReviewPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N messages) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;

        // Get the I18N instance
        this.messages = messages;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {

        String title = messages.getString("lhwizard.wizard.step5.title");
        String desc = messages.getString("lhwizard.wizard.step5.instruction");
	String waitZCBootTitle = messages.getString(
		"racstorage.reviewPanel.waitZCBootResource");
        String scalDevicesTitle = messages.getString(
                "racstorage.reviewPanel.scalDevices");
        String scalMountTitle = messages.getString(
                "racstorage.reviewPanel.scalMountPoint");
        String qfsMetaServer = messages.getString(
                "racstorage.reviewPanel.qfsMetaServer");
        String resourceNameTitle = messages.getString(
                "racstorage.reviewPanel.resourceName");
        String resourceTypeTitle = messages.getString(
                "racstorage.reviewPanel.resourceType");
	String locationTitle = messages.getString(
		"racstorage.reviewPanel.location");
        String help = messages.getString("hasp.reviewpanel.cli.help");
        String empty = messages.getString("racstorage.reviewPanel.empty");
        String subtitle = messages.getString("racstorage.reviewPanel.subTitle");
        String yes = messages.getString("cliwizards.yes");
        String no = messages.getString("cliwizards.no");
        String dchar = messages.getString("ttydisplay.d.char");
        String rchar = messages.getString("ttydisplay.r.char");
        String done = messages.getString("ttydisplay.menu.done");
        String refresh = messages.getString("ttydisplay.menu.refresh");
        String continueExit = messages.getString(
                "racstorage.reviewPanel.exitquery");
        String emptySelection = messages.getString(
                "racstorage.reviewPanel.emptySelection");
        tableTitle = messages.getString("racstorage.reviewPanel.table.title");
	String baseClusterName = messages.getString(
		"racwizard.invoker.location.preference.option1");

        do {

            // Get the selected resources
            Map selectedDGResources = (HashMap) wizardModel.getWizardValue(
		Util.SELECTED_DG_RESOURCES);
            Map selectedFSResources = (HashMap) wizardModel.getWizardValue(
		Util.SELECTED_FS_RESOURCES);

            Set diskResources = null;
            Set fsResources = null;

            if (selectedDGResources != null) {
                diskResources = selectedDGResources.keySet();
            }

            if (selectedFSResources != null) {
                fsResources = selectedFSResources.keySet();
            }

            // Construct the n*3 Table
            Vector storageData = new Vector();

	    String waitZCBootRSName = (String)wizardModel.getWizardValue(Util.WAIT_ZC_BOOT_RESOURCE);

	    if (waitZCBootRSName != null) {
		storageData.add(waitZCBootRSName);
		storageData.add(waitZCBootTitle);
		storageData.add(baseClusterName);
	    }

            // Add DiskResources
            if (diskResources != null) {

                for (Iterator i = diskResources.iterator(); i.hasNext();) {
                    String diskResource = (String) i.next();
		    String clusterName = baseClusterName;
		    String[] splitStr = diskResource.split(Util.COLON);
		    if (splitStr.length == 2) {
			clusterName = splitStr[Util.ZC_INDEX];
			diskResource = splitStr[Util.R_INDEX];
		    }
		    storageData.add(diskResource);
                    storageData.add(scalDevicesTitle);
		    storageData.add(clusterName);
                }
            }

            // Add FileSystem Resources
            if (fsResources != null) {

		String clusterName = baseClusterName;

		String selectedZoneCluster = CliUtil.getZoneClusterName(wizardModel);

		if (selectedZoneCluster != null &&
		    selectedZoneCluster.trim().length() > 0) {
		    clusterName = selectedZoneCluster.trim();
		}
                for (Iterator i = fsResources.iterator(); i.hasNext();) {
                    String fsResource = (String) i.next();
                    storageData.add(fsResource);
                    storageData.add(scalMountTitle);
                    storageData.add(clusterName);
                }
            }

            HashMap customTags = new HashMap();
            customTags.put(dchar, done);

            String subheadings[] = {
		resourceNameTitle, resourceTypeTitle, locationTitle };

            TTYDisplay.clearScreen();
            TTYDisplay.clear(1);

            // Enable Back and Help
            TTYDisplay.setNavEnabled(true);
            this.cancelDirection = false;


            // Title and Description
            TTYDisplay.printTitle(title);
            TTYDisplay.clear(1);
            TTYDisplay.pageText(desc);
            TTYDisplay.clear(1);

            // Handle no selections
            if (storageData.size() == 0) {
                TTYDisplay.printInfo(emptySelection);

                String exitOption = TTYDisplay.getConfirmation(continueExit,
                        yes, no);

                if (exitOption.equalsIgnoreCase(yes)) {
                    System.exit(0);
                } else {
                    this.cancelDirection = true;

                    return;
                }
            }

            // Display the (n*3) Table
            String option[] = TTYDisplay.create2DTable(tableTitle, subheadings,
                    storageData, storageData.size() / 3, 3, customTags, help,
                    empty, true);

            // Handle Back
            if (option == null) {
                this.cancelDirection = true;

                return;
            }


            // Process the options, display the DiskResource or the Filesystem
            // resource information
            try {
                int value = Integer.parseInt(option[0]);

                // Get the coressponding resource name from the vector
                String resourceName = (String) storageData.get((value * 3) - 3);

		String resourceType = (String) storageData.get((value * 3) - 2);

		if (resourceType.equals(waitZCBootTitle)) {
		    editWaitZCResourceInformation();
			
		} else if (resourceType.equals(scalDevicesTitle)) {
		    if (diskResources != null) {
			String clusterName =
				(String) storageData.get((value * 3) - 1);
			String dgRSName = null;
			if (!clusterName.equals(baseClusterName)) {
			    dgRSName = clusterName + Util.COLON + resourceName;
			} else {
			    dgRSName = resourceName;
			}

			if (diskResources.contains(dgRSName)) {
			    editDiskResourceInformation(
				    resourceName, clusterName);
			}
		    }
		} else if (resourceType.equals(scalMountTitle)) {
		    if ((fsResources != null) &&
			fsResources.contains(resourceName)) {

			Map fsInfoMap = (HashMap) wizardModel.getWizardValue(
				resourceName);

			String fsType = (String) fsInfoMap.get(
				Util.FILE_SYSTEM_TYPE);

			if (fsType.equals(Util.SQFS)) {
			    editQFSResourceInformation(resourceName);
			} else {
			    editNASResourceInformation(resourceName);
			}
		    }
                }
            } catch (NumberFormatException nfe) {

                // Custom Tags
                if (option[0].equals(dchar)) {

                    // Goto Summary Panel
                    return;
                }
            }
        } while (true);
    }

    /**
     * Displays and lets the user edit the wait ZC Boot Resource Information
     *
     * @param waitZCBootRSName
     */
    private void editWaitZCResourceInformation() {
        String title = messages.getString("lhwizard.wizard.step5.title");
        String subtitle = messages.getString(
                "racstorage.reviewPanel.waitZCBootSubTitle");
        String resourceGroupTitle = messages.getString(
                "racstorage.reviewPanel.resourceGroupName");
        String property = messages.getString("racstorage.reviewPanel.property");
        String value = messages.getString("racstorage.reviewPanel.value");
        String done = messages.getString("ttydisplay.menu.done");
        String dchar = messages.getString("ttydisplay.d.char");
        String resourceNameTitle = messages.getString(
                "racstorage.reviewPanel.resourceName");
        String help = messages.getString("racstorage.reviewPanel.subMenuHelp");
	String waitZCBootResource = messages.getString(
		"racstorage.reviewPanel.waitZCBootResource");
	String waitZCBootResourceGroup = messages.getString(
		"racstorage.reviewPanel.waitZCBootResourceGroup");

        String subheadings[] = { property, value };
        HashMap customTags = new HashMap();
        customTags.put(dchar, done);

	String waitZCBootRSName = (String)wizardModel.getWizardValue(Util.WAIT_ZC_BOOT_RESOURCE);
	String waitZCBootRGName = (String)wizardModel.getWizardValue(Util.WAIT_ZC_BOOT_GROUP);

	Vector waitZCData = new Vector();

	boolean doneFlag = false;
	do {
	    TTYDisplay.clearScreen();
	    TTYDisplay.clear(1);

	    TTYDisplay.printTitle(title);
	    TTYDisplay.clear(1);
	    TTYDisplay.printSubTitle(subtitle);

	    // Disable Back Navigation
	    // As this is just a sub-screen
	    TTYDisplay.setNavEnabled(false);

	    // display the waitZCBoot Resource and Group only when configuring
	    // on a ZC
	    String selectedZoneCluster = CliUtil.getZoneClusterName(wizardModel);

	    waitZCData.removeAllElements();

	    if (selectedZoneCluster != null &&
		selectedZoneCluster.trim().length() > 0) {
		waitZCData.add(waitZCBootResource);
                waitZCData.add(waitZCBootRSName);
                waitZCData.add(waitZCBootResourceGroup);
                waitZCData.add(waitZCBootRGName);
            }

	    String option[] = TTYDisplay.create2DTable(tableTitle, subheadings,
		waitZCData, waitZCData.size() / 2, 2, customTags, help, "", true);

	    if (option == null) {
		return;
	    } else if (option[0].equals("1")) {
		TTYDisplay.clear(1);

		String newResourceName = getNewResourceName(null);

		if (newResourceName != null) {
		    wizardModel.setWizardValue(Util.WAIT_ZC_BOOT_RESOURCE, newResourceName);
		    waitZCBootRSName = newResourceName;
                }
	    } else if (option[0].equals("2")) {
		TTYDisplay.clear(1);

		String newResourceGroupName = getNewResourceGroupName(null);

		if (newResourceGroupName != null) {
		    wizardModel.setWizardValue(Util.WAIT_ZC_BOOT_GROUP, newResourceGroupName);
		    waitZCBootRGName = newResourceGroupName;
		}
	    } else {
		doneFlag = true;
	    }
	} while (!doneFlag);
    }

    /**
     * Displays and Lets the user to edit the DiskResource's information
     *
     * @param  DiskResource  Name
     */
    private void editDiskResourceInformation(
	    String diskResource, String clusterName) {

        String title = messages.getString("lhwizard.wizard.step5.title");
        String resourceGroupTitle = messages.getString(
                "racstorage.reviewPanel.resourceGroupName");
        String diskGroupTypeTitle = messages.getString(
                "racstorage.reviewPanel.dgType");
        String diskGroupNameTitle = messages.getString(
                "racstorage.reviewPanel.dgName");
	String logicalDevListTitle = messages.getString(
		"racstorage.reviewPanel.logicaldevice");
	String locationTitle = messages.getString(
		"racstorage.reviewPanel.location");
        String property = messages.getString("racstorage.reviewPanel.property");
        String value = messages.getString("racstorage.reviewPanel.value");
        String subtitle = messages.getString(
                "racstorage.reviewPanel.diskInfoSubTitle");
        String done = messages.getString("ttydisplay.menu.done");
        String dchar = messages.getString("ttydisplay.d.char");
        String resourceNameTitle = messages.getString(
                "racstorage.reviewPanel.resourceName");
        String help = messages.getString("racstorage.reviewPanel.subMenuHelp");
	String baseClusterName = messages.getString(
		"racwizard.invoker.location.preference.option1");
	String allDisks = messages.getString(
		"racstorage.racDisksPanel.allDisks");


        Map diskInfoMap = null;
	String dgRSName = null;
	if (clusterName.equals(baseClusterName)) {
	    dgRSName = diskResource;
	} else {
	    dgRSName = clusterName + Util.COLON + diskResource;
	}

	diskInfoMap = (HashMap) wizardModel.getWizardValue(dgRSName);
        String diskGroupName = (String) diskInfoMap.get(Util.DISKGROUP_NAME);
        String diskGroupType = (String) diskInfoMap.get(Util.DISKGROUP_TYPE);
	String logicalDevList = (String) diskInfoMap.get(Util.LOGICAL_DEVLIST);
        String resourceGroupName = (String) diskInfoMap.get(Util.STORAGEGROUP);

        HashMap customTags = new HashMap();
        customTags.put(dchar, done);

        boolean doneFlag = false;

        Vector diskData = new Vector();

        do {
            TTYDisplay.clearScreen();
            TTYDisplay.clear(1);

            TTYDisplay.printTitle(title);
            TTYDisplay.clear(1);
            TTYDisplay.printSubTitle(subtitle);

            // Disable Back Navigation
            // As this is just a sub-screen
            TTYDisplay.setNavEnabled(false);

            String subheadings[] = { property, value };

            diskData.removeAllElements();

            diskData.add(resourceNameTitle);
            diskData.add(diskResource);
            diskData.add(resourceGroupTitle);
            diskData.add(resourceGroupName);
            diskData.add(diskGroupNameTitle);
            diskData.add(diskGroupName);
	    if (logicalDevList != null && logicalDevList.trim().length() > 0) {
		diskData.add(logicalDevListTitle);
		if (logicalDevList.equals(Util.ALL)) {
		    diskData.add(allDisks);
		} else {
		    diskData.add(logicalDevList);
		}
	    }
            diskData.add(diskGroupTypeTitle);
            diskData.add(diskGroupType);
	    diskData.add(locationTitle);
	    diskData.add(clusterName);

            String option[] = TTYDisplay.create2DTable(tableTitle, subheadings,
                    diskData, diskData.size() / 2, 2, customTags, help, "",
                    true);

            // Process the options

            if (option == null) {
                return;
            }

            if (option[0].equals("1")) {
                TTYDisplay.clear(1);

                String newResourceName = getNewResourceName(clusterName);
		String rsKey = null;

                if (newResourceName != null) {
		    if (clusterName.equals(baseClusterName)) {
			rsKey = newResourceName;
		    } else {
			rsKey = clusterName + Util.COLON + newResourceName;
		    }

                    wizardModel.setWizardValue(rsKey, diskInfoMap);

                    Map selectedDGResources = (HashMap) wizardModel
                        .getWizardValue(Util.SELECTED_DG_RESOURCES);
                    HashMap resourceInfo = (HashMap) selectedDGResources.get(
			dgRSName);
                    selectedDGResources.put(rsKey, resourceInfo);
                    selectedDGResources.remove(dgRSName);
                    diskResource = newResourceName;
                }
            } else if (option[0].equals("2")) {
                TTYDisplay.clear(1);
                String newResourceGroupName =
			getNewResourceGroupName(clusterName);

                if (newResourceGroupName != null) {
                    diskInfoMap.put(Util.STORAGEGROUP, newResourceGroupName);
                    resourceGroupName = newResourceGroupName;
                }
            } else {
                doneFlag = true;
            }
        } while (!doneFlag);

        return;
    }

    /**
     * Displays and Lets the user to edit the QFS Filesystem Resource's
     * information
     *
     * @param  FileSystem  Resource Name
     */
    private void editNASResourceInformation(String fsResource) {

        String title = messages.getString("lhwizard.wizard.step5.title");
        String resourceGroupTitle = messages.getString(
                "racstorage.reviewPanel.resourceGroupName");
        String property = messages.getString("racstorage.reviewPanel.property");
        String value = messages.getString("racstorage.reviewPanel.value");
        String subtitle = messages.getString(
                "racstorage.reviewPanel.fsInfoSubTitle");
        String done = messages.getString("ttydisplay.menu.done");
        String dchar = messages.getString("ttydisplay.d.char");
        String resourceNameTitle = messages.getString(
                "racstorage.reviewPanel.resourceName");
        String help = messages.getString("racstorage.reviewPanel.subMenuHelp");
        String nasMountPoint = messages.getString(
                "racstorage.reviewPanel.mountPoint");
        String exportedFSTitle = messages.getString(
                "racstorage.reviewPanel.exportedFSTitle");
        String subheadings[] = { property, value };
        HashMap customTags = new HashMap();
        customTags.put(dchar, done);

        Map fsInfoMap = (HashMap) wizardModel.getWizardValue(fsResource);
        Vector fsData = new Vector();

        String resourceGroup = (String) fsInfoMap.get(Util.STORAGEGROUP);
        String mountPoint = (String) fsInfoMap.get(Util.MOUNT_POINT);
        String exportedFS = (String) fsInfoMap.get(Util.FILE_SYSTEM_NAME);

        boolean doneFlag = false;

        do {

            TTYDisplay.clearScreen();
            TTYDisplay.clear(1);

            TTYDisplay.printTitle(title);
            TTYDisplay.clear(1);
            TTYDisplay.printSubTitle(subtitle);

            // Disable Back Navigation
            // As this is just a sub-screen
            TTYDisplay.setNavEnabled(false);

            fsData.removeAllElements();
            fsData.add(resourceNameTitle);
            fsData.add(fsResource);
            fsData.add(resourceGroupTitle);
            fsData.add(resourceGroup);
            fsData.add(nasMountPoint);
            fsData.add(mountPoint);
            fsData.add(exportedFSTitle);
            fsData.add(exportedFS);

            String option[] = TTYDisplay.create2DTable(tableTitle, subheadings,
                    fsData, fsData.size() / 2, 2, customTags, help, "", true);

            if (option == null) {
                return;
            } else if (option[0].equals("1")) {
                TTYDisplay.clear(1);

                String newResourceName = getNewResourceName(null);

                if (newResourceName != null) {
                    wizardModel.setWizardValue(newResourceName, fsInfoMap);

                    Map selectedFSResources = (HashMap) wizardModel
                        .getWizardValue(Util.SELECTED_FS_RESOURCES);
                    String resourceInfo = (String) selectedFSResources.get(
                            fsResource);
                    selectedFSResources.put(newResourceName, resourceInfo);
                    selectedFSResources.remove(fsResource);
                    fsResource = newResourceName;

                }
            } else if (option[0].equals("2")) {
                TTYDisplay.clear(1);

                String newResourceGroupName = getNewResourceGroupName(null);

                if (newResourceGroupName != null) {
                    fsInfoMap.put(Util.STORAGEGROUP, newResourceGroupName);
                    resourceGroup = newResourceGroupName;
                }
            } else {
                doneFlag = true;
            }
        } while (!doneFlag);
    }

    /**
     * Displays and Lets the user to edit the QFS Filesystem Resource's
     * information
     *
     * @param  FileSystem  Resource Name
     */
    private void editQFSResourceInformation(String fsResource) {

        String title = messages.getString("lhwizard.wizard.step5.title");
        String resourceGroupTitle = messages.getString(
                "racstorage.reviewPanel.resourceGroupName");
        String property = messages.getString("racstorage.reviewPanel.property");
        String value = messages.getString("racstorage.reviewPanel.value");
        String subtitle = messages.getString(
                "racstorage.reviewPanel.fsInfoSubTitle");
        String done = messages.getString("ttydisplay.menu.done");
        String dchar = messages.getString("ttydisplay.d.char");
        String resourceNameTitle = messages.getString(
                "racstorage.reviewPanel.resourceName");
        String help = messages.getString("racstorage.reviewPanel.subMenuHelp");
        String qfsMetaServer = messages.getString(
                "racstorage.reviewPanel.qfsMetaServer");
        String qfsMetaServerNodes = messages.getString(
                "racstorage.reviewPanel.qfsMetaServerNodes");
        String qfsMetaServerResourceGroup = messages.getString(
                "racstorage.reviewPanel.qfsMetaServerResourceGroup");
        String qfsMountPoint = messages.getString(
                "racstorage.reviewPanel.mountPoint");
        String obanUnderQFS = messages.getString(
                "racstorage.reviewPanel.qfsUnderOban");

        String subheadings[] = { property, value };
        HashMap customTags = new HashMap();
        customTags.put(dchar, done);

        Map fsInfoMap = (HashMap) wizardModel.getWizardValue(fsResource);

        Vector fsData = new Vector();

        String resourceGroup = (String) fsInfoMap.get(Util.STORAGEGROUP);
        String mountPoint = (String) fsInfoMap.get(Util.MOUNT_POINT);

        // Information specific to QFS
        String metaserverRes = (String) fsInfoMap.get(Util.METASERVER_RESOURCE);
        List metaserverNodes = (ArrayList) fsInfoMap.get(Util.METASERVER_NODES);
        String metaserverGroup = (String) fsInfoMap.get(Util.METASERVER_GROUP);
        String obanDisk = (String) fsInfoMap.get(Util.OBAN_UNDER_QFS);

        boolean doneFlag = false;

        do {

            TTYDisplay.clearScreen();
            TTYDisplay.clear(1);

            TTYDisplay.printTitle(title);
            TTYDisplay.clear(1);
            TTYDisplay.printSubTitle(subtitle);

            // Disable Back Navigation
            // As this is just a sub-screen
            TTYDisplay.setNavEnabled(false);

            fsData.removeAllElements();
            fsData.add(resourceNameTitle);
            fsData.add(fsResource);
            fsData.add(resourceGroupTitle);
            fsData.add(resourceGroup);
            fsData.add(qfsMountPoint);
            fsData.add(mountPoint);
            fsData.add(qfsMetaServer);
            fsData.add(metaserverRes);
            fsData.add(qfsMetaServerNodes);
            fsData.add(metaserverNodes.toString());
            fsData.add(qfsMetaServerResourceGroup);
            fsData.add(metaserverGroup);

            if (obanDisk != null) {
                fsData.add(obanUnderQFS);
                fsData.add(obanDisk);
            }

            String option[] = TTYDisplay.create2DTable(tableTitle, subheadings,
                    fsData, fsData.size() / 2, 2, customTags, help, "", true);

            if (option == null) {
                return;
            } else if (option[0].equals("1")) {
                TTYDisplay.clear(1);

                String newResourceName = getNewResourceName(null);

                if (newResourceName != null) {
                    wizardModel.setWizardValue(newResourceName, fsInfoMap);

                    Map selectedFSResources = (HashMap) wizardModel
                        .getWizardValue(Util.SELECTED_FS_RESOURCES);
                    String resourceInfo = (String) selectedFSResources.get(
                            fsResource);
                    selectedFSResources.put(newResourceName, resourceInfo);
                    selectedFSResources.remove(fsResource);
                    fsResource = newResourceName;

                }
            } else if (option[0].equals("2")) {
                TTYDisplay.clear(1);

                String newResourceGroupName = getNewResourceGroupName(null);

                if (newResourceGroupName != null) {
                    fsInfoMap.put(Util.STORAGEGROUP, newResourceGroupName);
                    resourceGroup = newResourceGroupName;
                }
            } else if (option[0].equals("4")) {
                TTYDisplay.clear(1);

                String newResourceName = getNewResourceName(null);

                if (newResourceName != null) {
                    fsInfoMap.put(Util.METASERVER_RESOURCE, newResourceName);
                    metaserverRes = newResourceName;
                }
            } else if (option[0].equals("6")) {
                TTYDisplay.clear(1);

                String newResourceGroupName = getNewResourceGroupName(null);

                if (newResourceGroupName != null) {
                    fsInfoMap.put(Util.METASERVER_GROUP, newResourceGroupName);
                    metaserverGroup = newResourceGroupName;
                }
            } else {
                doneFlag = true;
            }

        } while (!doneFlag);

        return;
    }

    /**
     * Generic method to get the name of a new Resource It checks for
     * whitespaces and checks if the ResourceName exists
     *
     * @return  Valid Resource Name or NULL
     */
    private String getNewResourceName(String clusterName) {

        // Pattern for checking Resource and ResourceGroup Name
        Pattern p = Pattern.compile("\\s*");
        Matcher m;
        String enterNewText = messages.getString("cliwizards.enterNew");
        String enterAgainText = messages.getString("cliwizards.enterAgain");
        String invalidResourceName = messages.getString(
                "lhwizard.reviewPanel.invalidResName");
        String yes = messages.getString("cliwizards.yes");
        String no = messages.getString("cliwizards.no");
        String resourceNameUsed = messages.getString(
                "lhwizard.reviewPanel.resourceNameErr");
	String baseClusterName = messages.getString(
		"racwizard.invoker.location.preference.option1");

	RACModel racModel = RACInvokerCreator.getRACModel();
        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);
	String zoneClusterName = null;
	if (clusterName == null ||
	    !clusterName.equals(baseClusterName)) {
	    zoneClusterName = (String) wizardModel.getWizardValue(
		Util.SEL_ZONE_CLUSTER);
	}
	HashMap helperData = new HashMap();
	helperData.put(Util.SEL_ZONE_CLUSTER, zoneClusterName);

	HashMap userInput = new HashMap();

        while (true) {
            String rName = TTYDisplay.promptForEntry(enterNewText);
            boolean rNameOk = true;

            if (rName.equals("<")) {
                return null;
            }

            // Check whether String has white space characters
            m = p.matcher(rName);

            if (m.matches()) {
                rNameOk = false;
                TTYDisplay.printError(invalidResourceName);

                String option = TTYDisplay.getConfirmation(enterAgainText, yes,
                        no);

                if (!option.equalsIgnoreCase(yes)) {
                    break;
                }
            }

	    userInput.put(Util.RESOURCENAME, rName);
            // Validate whether the name is already used
	    ErrorValue errVal = racModel.validateInput(
		null, nodeEndPoint,
		new String[] { Util.RESOURCENAME },
		userInput, helperData);

            if (!(errVal.getReturnValue()).booleanValue()) {
		// Resource Name is in use
                rNameOk = false;
                TTYDisplay.printError(resourceNameUsed);

                String option = TTYDisplay.getConfirmation(enterAgainText, yes,
                        no);

                if (!option.equalsIgnoreCase(yes)) {
                    return null;
                }
            }

            if (rNameOk) {
                return rName;
            }
        }

        return null;
    }

    /**
     * Generic method to get the name of a new Resource Group It checks for
     * whitespaces and checks if the ResourceGroup Name exists
     *
     * @return  Valid Resource Name or NULL
     */
    private String getNewResourceGroupName(String clusterName) {

        // Pattern for checking Resource and ResourceGroup Name
        Pattern p = Pattern.compile("\\s*");
        Matcher m;
        String enterNewText = messages.getString("cliwizards.enterNew");
        String enterAgainText = messages.getString("cliwizards.enterAgain");
        String invalidResourceGroupName = messages.getString(
                "lhwizard.reviewPanel.invalidRGName");
        String yes = messages.getString("cliwizards.yes");
        String no = messages.getString("cliwizards.no");
        String resourceGroupNameUsed = messages.getString(
                "lhwizard.reviewPanel.resourceGroupNameErr");
	String baseClusterName = messages.getString(
		"racwizard.invoker.location.preference.option1");

	RACModel racModel = RACInvokerCreator.getRACModel();
        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);
	String zoneClusterName = null;
	if (clusterName == null ||
	    !clusterName.equals(baseClusterName)) {
	    zoneClusterName = (String) wizardModel.getWizardValue(
		Util.SEL_ZONE_CLUSTER);
	}
	HashMap helperData = new HashMap();
	helperData.put(Util.SEL_ZONE_CLUSTER, zoneClusterName);

	HashMap userInput = new HashMap();
        while (true) {
            String rgName = TTYDisplay.promptForEntry(enterNewText);
            boolean rgNameOk = true;

            if (rgName.equals("<")) {
                return null;
            }

            // Check whether String has white space characters
            m = p.matcher(rgName);

            if (m.matches()) {
                rgNameOk = false;
                TTYDisplay.printError(invalidResourceGroupName);

                String option = TTYDisplay.getConfirmation(enterAgainText, yes,
                        no);

                if (!option.equalsIgnoreCase(yes)) {
                    break;
                }
            }

            // Validate whether the name is already used
	    userInput.put(Util.RESOURCEGROUPNAME, rgName);
	    ErrorValue errorValue = racModel.validateInput(
		null, nodeEndPoint,
		new String[] { Util.RESOURCEGROUPNAME },
		userInput, helperData);

            if (!(errorValue.getReturnValue()).booleanValue()) {
		// RG name in use
                rgNameOk = false;
                TTYDisplay.printError(resourceGroupNameUsed);

                String option = TTYDisplay.getConfirmation(enterAgainText, yes,
                        no);

                if (!option.equalsIgnoreCase(yes)) {
                    return null;
                }
            }

            if (rgNameOk) {
                return rgName;
            }
        }

        return null;
    }
}
