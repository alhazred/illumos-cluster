/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)ORSWizardIntroPanel.java	1.14	08/12/10 SMI"
 */

package com.sun.cluster.dswizards.oraclerac.storage;

// CLI SDK

// CMASS
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;

// Wizard Common
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;

// DSCONFIG
import com.sun.cluster.dswizards.oraclerac.invoker.RACInvokerConstants;

import java.util.ArrayList;
import java.util.List;


/**
 * Introduction page to the OracleRAC Storage Wizard
 */
public class ORSWizardIntroPanel extends WizardLeaf {

    private String panelName;                   // Panel Name
    private CliDSConfigWizardModel wizardModel;    // WizardModel
    private WizardI18N messages;

    /**
     * Creates a new instance of ORSWizardIntroPanel
     */
    public ORSWizardIntroPanel() {
        super();
    }

    /**
     * Creates an Intro Panel for racstorage Wizard with the given name and
     * associates it with the WizardModel and WizardFlow
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     */
    public ORSWizardIntroPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N messages) {
	super(model,name,wizardFlow);

	this.panelName = name;
	this.wizardModel = model;

	// Get the I18N instance
	this.messages = messages;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {

        TTYDisplay.clearScreen();
        TTYDisplay.clear(1);

        this.cancelDirection = false;

        // Title
        TTYDisplay.printTitle(messages.getString(
                "racstorage.introPanel.title"));
        TTYDisplay.clear(2);
        TTYDisplay.pageText(messages.getString("racstorage.introPanel.desc"),
            4);

        TTYDisplay.clear(2);
        TTYDisplay.pageText(messages.getString(
                "racstorage.introPanel.preqTitle"), 4);

        TTYDisplay.clear(1);
        TTYDisplay.pageText(messages.getString("racstorage.introPanel.preq1"),
            4);

        TTYDisplay.clear(1);
        TTYDisplay.pageText(messages.getString("racstorage.introPanel.preq2"),
            4);

        TTYDisplay.clear(1);
        TTYDisplay.pageText(messages.getString("racstorage.introPanel.preq3"),
            4);

	String selectedCluster = (String) wizardModel.getWizardValue(
	    Util.CLUSTER_SEL);
	if (selectedCluster.equals(Util.ZONE_CLUSTER)) {
	    TTYDisplay.clear(1);
	    TTYDisplay.pageText(messages.getString("racstorage.introPanel.preq4"),
		    4);
	}

        TTYDisplay.clear(1);

        String entry = TTYDisplay.promptForEntry(messages.getString(
                    "cliwizards.continue"));

        if (entry.equals(messages.getString("ttydisplay.back.char"))) {
            this.cancelDirection = true;

            return;
        }

        // Set the wizard state key for storage management schemes.
        ArrayList propNames = new ArrayList();
        Object tmpObj = (ArrayList) wizardModel.getWizardValue(
                Util.WIZARD_STATE);

        if (tmpObj != null) {
            propNames = (ArrayList) tmpObj;
        }

	String keyPrefix = (String) wizardModel.getWizardValue(Util.KEY_PREFIX);
        propNames.add(keyPrefix + Util.ORF_STORAGE_MGMT_SCHEMES);
	propNames.add(Util.SEL_ZONE_CLUSTER);
        wizardModel.setWizardValue(Util.WIZARD_STATE, propNames);
    }
}
