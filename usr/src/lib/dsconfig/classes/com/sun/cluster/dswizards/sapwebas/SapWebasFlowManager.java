/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)SapWebasFlowManager.java 1.13     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.sapwebas;

import java.util.ArrayList;
import java.util.List;

import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.dswizards.common.DSConfigWizardModel;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.DSWizardInterface;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.hastorageplus.HASPWizardConstants;

/**
 * The class takes decision on which panel to show "next" depending upon the
 * conditions set at a particular context.
 */
public class SapWebasFlowManager implements DSWizardInterface {

    public SapWebasFlowManager() {
    }

    /**
     * @see  com.sun.cluster.dswizards.common.DSWizardInterface#getNextPanel
     */
    public String getNextPanel(String curPanelName,
        Object wizCxtObj, String options[]) {

	DSConfigWizardModel guiModel = null;
	CliDSConfigWizardModel cliModel = null;

	if (wizCxtObj instanceof CliDSConfigWizardModel) {
	    cliModel = (CliDSConfigWizardModel)wizCxtObj;
	} else if (wizCxtObj instanceof DSConfigWizardModel) {
	    guiModel = (DSConfigWizardModel)wizCxtObj;
	}

        if (curPanelName.equals(SapWebasConstants.STORAGE_SELECT)) {

            String isNewHasp = null;
            String zonesPresent = null;

            // get the user preference
	    if (guiModel != null) {
		isNewHasp = (String)guiModel.getWizardValue(
		    SapWebasConstants.NEW_HASP_CS);
		zonesPresent = (String)guiModel.getWizardValue(
		    SapWebasConstants.CS_ZONES_PRESENT);
	    } else {
		isNewHasp = (String)cliModel.getWizardValue(
		    SapWebasConstants.NEW_HASP_CS);
		zonesPresent = (String)cliModel.getWizardValue(
		    SapWebasConstants.CS_ZONES_PRESENT);
	    }

            if (isNewHasp.equals(SapWebasConstants.CREATE_NEW)) {

                if (zonesPresent.equals(HASPWizardConstants.NO)) {

                    // go to HASPLocationPreference Panel
                    return options[0];
                } else {
                    return options[1];
                }
            } else {
                return options[2];
            }
        } else if (curPanelName.equals(SapWebasConstants.HOST_SELECT)) {

            // get the user preference
            String isNewLH = null;
	    if (guiModel != null) {
		isNewLH = (String)guiModel.getWizardValue(
		    SapWebasConstants.NEW_LH_CS);
	    } else {
		isNewLH = (String)cliModel.getWizardValue(
		    SapWebasConstants.NEW_LH_CS);
	    }

            if (isNewLH.equals(SapWebasConstants.CREATE_NEW)) {
                return options[0];
            } else {
                Boolean isRepSelected = null;
		Boolean inReplica = null;
		if (guiModel != null) {
		    isRepSelected = (Boolean)guiModel.getWizardValue(
			Util.REP_COMPONENT);
		    inReplica = (Boolean)guiModel.getWizardValue(
			SapWebasConstants.INSIDE_REP_COMPONENT);
		} else {
		    isRepSelected = (Boolean)cliModel.getWizardValue(
			Util.REP_COMPONENT);
		    inReplica = (Boolean)cliModel.getWizardValue(
			SapWebasConstants.INSIDE_REP_COMPONENT);
		}

                if (isRepSelected.booleanValue()) {
                    return options[1];
                } else {
                    return options[2];
                }
            }
        } else if (curPanelName.equals(SapWebasConstants.REP_STORAGE_SELECT)) {

            // get the user preference
            String isNewHasp = null;
	    String zonesPresent = null;

	    if (guiModel != null) {
		isNewHasp = (String)guiModel.getWizardValue(
		    SapWebasConstants.NEW_HASP_CS);

		zonesPresent = (String)guiModel.getWizardValue(
		    SapWebasConstants.CS_ZONES_PRESENT);
	    } else {
		isNewHasp = (String)cliModel.getWizardValue(
		    SapWebasConstants.NEW_HASP_CS);

		zonesPresent = (String)cliModel.getWizardValue(
		    SapWebasConstants.CS_ZONES_PRESENT);
	    } 

            if (isNewHasp.equals(SapWebasConstants.CREATE_NEW)) {

                if (zonesPresent.equals(HASPWizardConstants.NO)) {

                    // go to HASPLocationPreference Panel
                    return options[0];
                } else {
                    return options[1];
                }
            } else {
                return options[2];
            }
        } else if (curPanelName.equals(SapWebasConstants.REP_HOST_SELECT)) {

            // get the user preference
            String isNewLH = null;
	    if (guiModel != null) {
		isNewLH = (String)guiModel.getWizardValue(
		    SapWebasConstants.NEW_LH_CS);
	    } else {
		isNewLH = (String)cliModel.getWizardValue(
		    SapWebasConstants.NEW_LH_CS);
	    }

            if (isNewLH.equals(SapWebasConstants.CREATE_NEW)) {
                return options[0];
            } else {
                return options[1];
            }
        }

        return null;
    }
}
