/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)ORFReviewPanel.java	1.11	09/03/11 SMI"
 */

package com.sun.cluster.dswizards.oraclerac.framework;

// CMASS
import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.utils.Util;

// CLI Wizard SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliUtil;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.oraclerac.invoker.RACInvokerCreator;

// J2SE
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import com.sun.cluster.model.RACModel;

/**
 * Review page to the OracleRAC Framework Wizard
 */
public class ORFReviewPanel extends WizardLeaf {

    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N wizardi18n; // Message Bundle
    private ArrayList assignedResNames = null;
    private ArrayList assignedRgNames = null;

    /**
     * Creates a new instance of ORFReviewPanel
     */
    public ORFReviewPanel() {
        super();
    }

    /**
     * Creates a ORFReview Panel with the given name and tree manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public ORFReviewPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates a ORFReview Panel with the given name and associates it with the
     * WizardModel and WizardFlow
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     * @param  wizardi18n  Resource Bundle
     */
    public ORFReviewPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;

        // Get the I18N instance
        this.wizardi18n = wizardi18n;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {

        String title = wizardi18n.getString("orf.reviewpanel.title");
        String desc1 = wizardi18n.getString("orf.reviewpanel.desc1");
        String contwiz = wizardi18n.getString("cliwizards.continue");
        String table_title = wizardi18n.getString("orf.reviewpanel.desc2");
        String heading1 = wizardi18n.getString("orf.reviewpanel.heading1");
        String heading2 = wizardi18n.getString("orf.reviewpanel.heading2");
        String orfrgname_txt = wizardi18n.getString(
                "orf.reviewpanel.orfrg.name");
        String orfresname_txt = wizardi18n.getString(
                "orf.reviewpanel.orfresource.name");
        String orfudlmresname_txt = wizardi18n.getString(
                "orf.reviewpanel.orfudlmresource.name");
        String orfsvmresname_txt = wizardi18n.getString(
                "orf.reviewpanel.orfsvmresource.name");
        String orfvxvmresname_txt = wizardi18n.getString(
                "orf.reviewpanel.orfvxvmresource.name");
        String done_txt = wizardi18n.getString("ttydisplay.menu.done");
        String help_text = wizardi18n.getString("orf.reviewpanel.help");
        String dchar = (String) wizardi18n.getString("ttydisplay.d.char");
        String back = wizardi18n.getString("ttydisplay.back.char");
        String propNameText = wizardi18n.getString("orf.reviewpanel.propname");
        String propTypeText = wizardi18n.getString("orf.reviewpanel.proptype");
        String propDescText = wizardi18n.getString("orf.reviewpanel.propdesc");
        String currValText = wizardi18n.getString("orf.reviewpanel.currval");
        String newValText = wizardi18n.getString("orf.reviewpanel.newval");
        String rgname_error = wizardi18n.getString(
                "orf.reviewpanel.rgname.error");
        String rsname_error = wizardi18n.getString(
                "orf.reviewpanel.resourcename.error");
        String rsname_assigned = wizardi18n.getString(
                "reviewpanel.resourcename.assigned");
        String rgname_assigned = wizardi18n.getString(
                "reviewpanel.rgname.assigned");
        String confirm_txt = wizardi18n.getString("cliwizards.enterAgain");
        String yes = wizardi18n.getString("cliwizards.yes");
        String no = wizardi18n.getString("cliwizards.no");

        String subheadings[] = { heading1, heading2 };

        boolean svm_selected = false;
        boolean vxvm_selected = false;
        boolean udlm_selected = false;
        String orfsvmResName = null;
        String orfvxvmResName = null;
        String orfudlmResName = null;

	HashMap helperData = new HashMap();
	String zoneClusterName = CliUtil.getZoneClusterName(wizardModel);
	helperData.put(Util.SEL_ZONE_CLUSTER, zoneClusterName);

	String keyPrefix = (String) wizardModel.getWizardValue(
		Util.KEY_PREFIX);

        List selStorageMgmtSchemes = (List) wizardModel.getWizardValue(
                keyPrefix + Util.ORF_STORAGE_MGMT_SCHEMES);

        ArrayList frmwrkRT = (ArrayList) wizardModel.getWizardValue(
                Util.FRMWRK_RT);

        if (frmwrkRT.contains(Util.RAC_UDLM_RTNAME)) {
            udlm_selected = true;
        }

	if (zoneClusterName == null ||
	    zoneClusterName.trim().length() == 0) {
	    if (selStorageMgmtSchemes.contains(Util.SVM) ||
		selStorageMgmtSchemes.contains(Util.QFS_SVM)) {
		svm_selected = true;
	    }
	}

        if (selStorageMgmtSchemes.contains(Util.VXVM)) {
            vxvm_selected = true;
        }

        String properties[] = null;


        // Enable Navigation
        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        boolean flag = true;

        // Review Panel Table
        Vector reviewPanelVect = new Vector();

        // Resource Group Name
        String orfRgName = getORFRGName(helperData);

        // ORF Resource name
        String orfResName = getORFResName(helperData);

        // UDLM Resource name
        if (udlm_selected) {
            orfudlmResName = getORFUDLMResName(helperData);
        }

        if (svm_selected) {
            orfsvmResName = getORFSVMResName(helperData);
        }

        if (vxvm_selected) {
            orfvxvmResName = getORFVXVMResName(helperData);
        }


        while (flag) {
            assignedResNames = new ArrayList();
            assignedRgNames = new ArrayList();
            TTYDisplay.clearScreen();
            TTYDisplay.clear(1);

            // Title
            TTYDisplay.printTitle(title);
            TTYDisplay.clear(1);
            TTYDisplay.pageText(desc1, 4);
            TTYDisplay.clear(1);

            // initialize
            reviewPanelVect.removeAllElements();

            // ORF RG Name
            reviewPanelVect.add(orfrgname_txt);
            reviewPanelVect.add(orfRgName);

            if (!assignedRgNames.contains(orfRgName)) {
                assignedRgNames.add(orfRgName);
            }

            // ORF Resource name
            reviewPanelVect.add(orfresname_txt);
            reviewPanelVect.add(orfResName);

            if (!assignedResNames.contains(orfResName)) {
                assignedResNames.add(orfResName);
            }

            // UDLM Resource name
            if (udlm_selected) {
                reviewPanelVect.add(orfudlmresname_txt);
                reviewPanelVect.add(orfudlmResName);

                if (!assignedResNames.contains(orfudlmResName)) {
                    assignedResNames.add(orfudlmResName);
                }
            }

            if (svm_selected) {
                reviewPanelVect.add(orfsvmresname_txt);
                reviewPanelVect.add(orfsvmResName);

                if (!assignedResNames.contains(orfsvmResName)) {
                    assignedResNames.add(orfsvmResName);
                }
            }

            if (vxvm_selected) {
                reviewPanelVect.add(orfvxvmresname_txt);
                reviewPanelVect.add(orfvxvmResName);

                if (!assignedResNames.contains(orfvxvmResName)) {
                    assignedResNames.add(orfvxvmResName);
                }
            }

            HashMap customTags = new HashMap();
            customTags.put(dchar, done_txt);

            String option = TTYDisplay.create2DTable(table_title, subheadings,
                    reviewPanelVect, customTags, help_text);

            if (option.equals(back)) {
                this.cancelDirection = true;

                return;
            }

            TTYDisplay.clear(3);
            TTYDisplay.setNavEnabled(false);

            if (!option.equals(dchar)) {
                String prop = null;
                String currVal = null;

                if (option.equals("1")) {

                    // Change ORF RG name
                    prop = "orfrg";
                    currVal = orfRgName;

                }

                if (option.equals("2")) {

                    // Change ORF res name
                    prop = "orfresource";
                    currVal = orfResName;

                }

                if (udlm_selected) {

                    if (option.equals("3")) {

                        // Change ORF UDLM res name
                        prop = "orfudlmresource";
                        currVal = orfudlmResName;

                    }

                    if (option.equals("4")) {

                        if (svm_selected) {
                            prop = "orfsvmresource";
                            currVal = orfsvmResName;
                        } else if (vxvm_selected) {
                            prop = "orfvxvmresource";
                            currVal = orfvxvmResName;
                        }
                    }

                    if (option.equals("5")) {

                        // Change ORF VXLM res name
                        prop = "orfvxvmresource";
                        currVal = orfvxvmResName;

                    }
                } else {

                    if (option.equals("3")) {

                        if (svm_selected) {
                            prop = "orfsvmresource";
                            currVal = orfsvmResName;
                        } else if (vxvm_selected) {
                            prop = "orfvxvmresource";
                            currVal = orfvxvmResName;
                        }
                    }

                    if (option.equals("4")) {

                        // Change ORF VXLM res name
                        prop = "orfvxvmresource";
                        currVal = orfvxvmResName;

                    }
                }

                TTYDisplay.pageText(propNameText +
                    wizardi18n.getString("orf.reviewpanel." + prop + ".name"));

                TTYDisplay.pageText(propDescText +
                    wizardi18n.getString("orf.reviewpanel." + prop + ".desc"));

                TTYDisplay.pageText(propTypeText +
                    wizardi18n.getString("orf.reviewpanel." + prop + ".type"));

                TTYDisplay.pageText(currValText + currVal);


                // Prompt for new values for each of the options
                if (option.equals("1")) {
                    String tmp_orfRgName = null;

                    while (true) {
                        tmp_orfRgName = TTYDisplay.promptForEntry(newValText);

                        if ((tmp_orfRgName != null) &&
                                (tmp_orfRgName.trim().length() != 0)) {

                            // call validate input
                            properties = new String[] {
                                    Util.RESOURCEGROUPNAME
                                };

                            HashMap userInputs = new HashMap();
                            userInputs.put(Util.RESOURCEGROUPNAME,
                                tmp_orfRgName);

                            ErrorValue retVal = validateInput(properties,
                                    userInputs, helperData);

                            if (!retVal.getReturnValue().booleanValue()) {
                                TTYDisplay.printError(rgname_error);

                                String confirm = TTYDisplay.getConfirmation(
                                        confirm_txt, yes, no);

                                if (!confirm.equalsIgnoreCase(yes)) {
                                    break;
                                }
                            } else {
                                assignedRgNames.remove(currVal);

                                if (assignedRgNames.contains(tmp_orfRgName)) {
                                    assignedRgNames.add(currVal);
                                    TTYDisplay.printError(rgname_assigned);

                                    String confirm = TTYDisplay.getConfirmation(
                                            confirm_txt, yes, no);

                                    if (!confirm.equalsIgnoreCase(yes)) {
                                        break;
                                    }
                                } else {
                                    orfRgName = tmp_orfRgName;
                                    assignedRgNames.add(orfRgName);

                                    break;
                                }
                            }
                        }
                    } // while
                } else {
                    String tmp_orfResName = null;

                    while (true) {
                        tmp_orfResName = TTYDisplay.promptForEntry(newValText);

                        if ((tmp_orfResName != null) &&
                                (tmp_orfResName.trim().length() != 0)) {

                            // call validate input
                            properties = new String[] { Util.RESOURCENAME };

                            HashMap userInputs = new HashMap();
                            userInputs.put(Util.RESOURCENAME, tmp_orfResName);

                            ErrorValue retVal = validateInput(properties,
                                    userInputs, helperData);

                            if (!retVal.getReturnValue().booleanValue()) {
                                TTYDisplay.printError(rsname_error);

                                String confirm = TTYDisplay.getConfirmation(
                                        confirm_txt, yes, no);

                                if (!confirm.equalsIgnoreCase(yes)) {
                                    break;
                                }
                            } else {
                                assignedResNames.remove(currVal);

                                if (assignedResNames.contains(tmp_orfResName)) {
                                    assignedResNames.add(currVal);
                                    TTYDisplay.printError(rsname_assigned);

                                    String confirm = TTYDisplay.getConfirmation(
                                            confirm_txt, yes, no);

                                    if (!confirm.equalsIgnoreCase(yes)) {
                                        break;
                                    }
                                } else {
                                    assignedResNames.add(tmp_orfResName);

                                    if (option.equals("2")) {
                                        orfResName = tmp_orfResName;
                                    }

                                    if (udlm_selected) {

                                        if (option.equals("3")) {
                                            orfudlmResName = tmp_orfResName;
                                        }

                                        if (option.equals("4")) {

                                            if (svm_selected) {
                                                orfsvmResName = tmp_orfResName;
                                            } else if (vxvm_selected) {
                                                orfvxvmResName = tmp_orfResName;
                                            }
                                        }

                                        if (option.equals("5")) {
                                            orfvxvmResName = tmp_orfResName;
                                        }
                                    } else {

                                        if (option.equals("3")) {

                                            if (svm_selected) {
                                                orfsvmResName = tmp_orfResName;
                                            } else if (vxvm_selected) {
                                                orfvxvmResName = tmp_orfResName;
                                            }
                                        }

                                        if (option.equals("4")) {
                                            orfvxvmResName = tmp_orfResName;
                                        }
                                    }

                                    break;
                                }
                            }
                        }
                    } // while
                }
            } else {

                // set final values in wizardModel
                // orf RG Name
                wizardModel.setWizardValue(Util.ORF_RESOURCE_GROUP +
                    Util.NAME_TAG, orfRgName);

                // orf res name
                wizardModel.setWizardValue(Util.ORF_RID + Util.NAME_TAG,
                    orfResName);

                // orf udlm res name
                wizardModel.setWizardValue(Util.ORF_UDLM_RID + Util.NAME_TAG,
                    orfudlmResName);

                if (svm_selected) {

                    // orf svm res name
                    wizardModel.setWizardValue(Util.ORF_SVM_RID + Util.NAME_TAG,
                        orfsvmResName);
                }

                if (vxvm_selected) {

                    // orf vxvm res name
                    wizardModel.setWizardValue(Util.ORF_VXVM_RID +
                        Util.NAME_TAG, orfvxvmResName);
                }

                break;
            }

            TTYDisplay.clear(3);
        }
    }


    private ErrorValue validateInput(String propNames[], HashMap userInput,
        HashMap helperData) {
        ErrorValue errValue = null;

        // get oracle rac model
        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);
        RACModel racModel = RACInvokerCreator.getRACModel();
        errValue = racModel.validateInput(null, nodeEndPoint, propNames, userInput, helperData);

        return errValue;
    }


    private String getORFRGName(HashMap helperData) {
        String orfRgName = (String) wizardModel.getWizardValue(
                Util.ORF_RESOURCE_GROUP + Util.NAME_TAG);

	String nodeEndPoint = (String) wizardModel.getWizardValue(
		Util.NODE_TO_CONNECT);

	RACModel racModel = RACInvokerCreator.getRACModel();

        if (orfRgName == null) {

            // Create a new orf rg name
            orfRgName = ORFWizardConstants.ORF_RG_NAME;

            String properties[] = { Util.RG_NAME };
	    helperData.put(Util.RG_PREFIX_KEY, orfRgName);

            HashMap discoveredMap = racModel.discoverPossibilities(
		null, nodeEndPoint, properties, helperData);

            orfRgName = (String) discoveredMap.get(Util.RG_NAME);
        }

        return orfRgName;
    }

    private String getORFResName(HashMap helperData) {

        String orfResName = (String) wizardModel.getWizardValue(Util.ORF_RID +
                Util.NAME_TAG);

	String nodeEndPoint = (String) wizardModel.getWizardValue(
		Util.NODE_TO_CONNECT);

	RACModel racModel = RACInvokerCreator.getRACModel();

        if (orfResName == null) {

            // Create a new orf rg name
            orfResName = ORFWizardConstants.ORF_RS_NAME;

            String properties[] = { Util.RS_NAME };

	    helperData.put(Util.RS_PREFIX_KEY, orfResName);

            HashMap discoveredMap = racModel.discoverPossibilities(
		null, nodeEndPoint, properties, helperData);
            orfResName = (String) discoveredMap.get(Util.RS_NAME);
        }

        return orfResName;
    }

    private String getORFUDLMResName(HashMap helperData) {

        String orfudlmResName = (String) wizardModel.getWizardValue(
                Util.ORF_UDLM_RID + Util.NAME_TAG);

	String nodeEndPoint = (String) wizardModel.getWizardValue(
		Util.NODE_TO_CONNECT);

	RACModel racModel = RACInvokerCreator.getRACModel();

        if (orfudlmResName == null) {

            // Create a new orf rg name
            orfudlmResName = ORFWizardConstants.ORF_UDLM_RS_NAME;

            String properties[] = { Util.RS_NAME };

	    helperData.put(Util.RS_PREFIX_KEY, orfudlmResName);

            HashMap discoveredMap = racModel.discoverPossibilities(
		null, nodeEndPoint, properties, helperData);
            orfudlmResName = (String) discoveredMap.get(Util.RS_NAME);
        }

        return orfudlmResName;
    }

    private String getORFSVMResName(HashMap helperData) {

        String orfsvmResName = (String) wizardModel.getWizardValue(
                Util.ORF_SVM_RID + Util.NAME_TAG);

	String nodeEndPoint = (String) wizardModel.getWizardValue(
		Util.NODE_TO_CONNECT);

	RACModel racModel = RACInvokerCreator.getRACModel();

        if (orfsvmResName == null) {

            // Create a new orf rg name
            orfsvmResName = ORFWizardConstants.ORF_SVM_RS_NAME;

            String properties[] = { Util.RS_NAME };

	    helperData.put(Util.RS_PREFIX_KEY, orfsvmResName);

            HashMap discoveredMap = racModel.discoverPossibilities(
		null, nodeEndPoint, properties, helperData);
            orfsvmResName = (String) discoveredMap.get(Util.RS_NAME);
        }

        return orfsvmResName;
    }

    private String getORFVXVMResName(HashMap helperData) {

        String orfvxvmResName = (String) wizardModel.getWizardValue(
                Util.ORF_VXVM_RID + Util.NAME_TAG);

	String nodeEndPoint = (String) wizardModel.getWizardValue(
		Util.NODE_TO_CONNECT);

	RACModel racModel = RACInvokerCreator.getRACModel();

        if (orfvxvmResName == null) {

            // Create a new orf rg name
            orfvxvmResName = ORFWizardConstants.ORF_VXVM_RS_NAME;

            String properties[] = { Util.RS_NAME };

	    helperData.put(Util.RS_PREFIX_KEY, orfvxvmResName);

            HashMap discoveredMap = racModel.discoverPossibilities(
		null, nodeEndPoint, properties, helperData);
            orfvxvmResName = (String) discoveredMap.get(Util.RS_NAME);
        }

        return orfvxvmResName;
    }
}
