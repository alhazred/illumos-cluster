/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)HostnameEntryPanel.java	1.30	08/12/10 SMI"
 */

package com.sun.cluster.dswizards.logicalhost;


// CMASS
import com.sun.cluster.agent.dataservices.logicalhost.LogicalHostMBean;
import com.sun.cluster.agent.dataservices.sharedaddress.SharedAddressMBean;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.dataservices.common.ErrorValue;

// CLI Wizard SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;

// Wizard Common
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.sharedaddress.SharedAddressWizardCreator;

// J2SE
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

// JMX
import javax.management.remote.JMXConnector;


/**
 * The Panel Class that gets a Hostname or list of Hostnames as input from the
 * User.
 */
public class HostnameEntryPanel extends WizardLeaf {


    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N messages; // Message Bundle
    private String wizardType; // Wizard Type
    private JMXConnector localConnection; // Connector


    /**
     * Creates a new instance of HostnameEntryPanel
     */
    public HostnameEntryPanel() {
        super();
    }

    /**
     * Creates a HostnameEntry Panel with the given name and tree manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public HostnameEntryPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates a HostnameEntry Panel for either the SharedAddress or
     * LogicalHostname Wizard with the given name and associates it with the
     * WizardModel and WizardFlow
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     * @param  wizardType  shwizard or lhwizard
     */
    public HostnameEntryPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N messages) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;
        this.wizardType = LogicalHostWizardCreator.WIZARDTYPE;

        // Get the I18N instance
        this.messages = messages;
        localConnection = getConnection(Util.getClusterEndpoint());
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {

        String hostNamesList[];
        String hostNames;
        boolean hostNameOk = true;
        Thread t;

        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        String back = messages.getString("ttydisplay.back.char");
        String yes = messages.getString("cliwizards.yes");
        String no = messages.getString("cliwizards.no");
        String help = messages.getString(wizardType +
                ".hostnamePanel.cli.help");

        // If this panel is called from another wizard get the wizard type
        // from the model
        Object type = wizardModel.getWizardValue(
                LogicalHostWizardConstants.WIZARDTYPE);

        if (type != null) {
            wizardType = (String) type;
        }

        // Get the previous selection
        String previousSelection = (String) wizardModel.getWizardValue(
                Util.HOST_NAME);

        do {
            TTYDisplay.clearScreen();
            TTYDisplay.clear(2);

            // Title
            TTYDisplay.printTitle(messages.getString(
                    wizardType + ".wizard.step2.title"));

            // Get the Hostname List
            TTYDisplay.clear(1);

            if (previousSelection == null) {
                hostNames = TTYDisplay.promptForEntry(messages.getString(
                            wizardType + ".wizard.step2.cli.prompt"), help);
            } else {
                hostNames = TTYDisplay.promptForEntry(messages.getString(
                            wizardType + ".wizard.step2.cli.prompt"),
                        previousSelection, help);
            }

            TTYDisplay.clear(1);

            if (hostNames.equals(back)) {
                this.cancelDirection = true;

                return;
            }

            TTYDisplay.setNavEnabled(true);
            this.cancelDirection = false;

            // Validate the Hostnames

            // The Hostnames are validated for the following
            // -> Whether the hostnames themselves are legal names
            // -> Whether any other cluster resource already
            // uses the hostname
            // -> Whether the Hostnames can be plumbed on any IPMP Groups

            hostNamesList = hostNames.split(",");

            StringBuffer hostNamesBuf = new StringBuffer();

            for (int i = 0; i < hostNamesList.length; i++) {
                hostNamesList[i] = hostNamesList[i].trim();
                hostNamesBuf.append(hostNamesList[i]);

                if (i < (hostNamesList.length - 1)) {
                    hostNamesBuf.append(",");
                }
            }

            hostNames = hostNamesBuf.toString();

            HashMap userMap = new HashMap();
            userMap.put(Util.HOSTNAMELIST, hostNamesList);

            // Validation for valid hostnames
            String propertyName[] = { Util.HOSTNAMELIST };

            TTYDisplay.clear(2);
            t = TTYDisplay.busy(messages.getString(
                        "lhwizard.hostnamePanel.validate.busy",
                        new Object[] { hostNames }));
            t.start();

            ErrorValue returnVal = validate(propertyName, userMap, null,
                    wizardType);

            try {
                t.interrupt();
                t.join();
            } catch (Exception e) {

            }

            if (!returnVal.getReturnValue().booleanValue()) {
                TTYDisplay.clear(2);
                TTYDisplay.printError(returnVal.getErrorString());
                TTYDisplay.promptForEntry(messages.getString(
                        "cliwizards.continue"));
                hostNameOk = false;
            } else {

                // Get the Discovered Nodelist From the Context
		String clusterSel = (String)wizardModel.getWizardValue(
			Util.CLUSTER_SEL);

		String optionsArr[] = null;

		// if configuring on a zone cluster use the base cluster node list
		if (clusterSel != null && clusterSel.equals(Util.ZONE_CLUSTER)) {
		    optionsArr = (String[])wizardModel.getWizardValue(
			Util.BASE_CLUSTER_NODELIST);
		} else {
		    optionsArr = (String[]) wizardModel.getWizardValue(
                        Util.RG_NODELIST);
		}
                wizardModel.setWizardValue(Util.HOSTNAMELIST, hostNamesList);
                TTYDisplay.clear(2);
                t = TTYDisplay.busy(messages.getString(
                            "lhwizard.hostnamePanel.ipmpdiscovery.busy",
                            new Object[] { hostNames }));
                t.start();
		boolean validGroups = discoverIPMPGroups(optionsArr,
		    wizardType);
		try {
		    t.interrupt();
		    t.join();
		} catch (Exception e) {
		    // A exception here is not a severe problem,
		    // therefore ignore it.
		}

                if (!validGroups) {

                    // Report error to user and goto HostnameEntry Panel
                    StringBuffer optionsArrStr = new StringBuffer();
                    optionsArrStr.append("[");

                    for (int i = 0; i < optionsArr.length; i++) {
                        optionsArrStr.append(optionsArr[i]);

                        if (i < (optionsArr.length - 1)) {
                            optionsArrStr.append(",");
                        }
                    }

                    optionsArrStr.append("] ");
                    TTYDisplay.clear(2);
                    TTYDisplay.printError(messages.getString(
                            wizardType + ".hostnamePanel.ipmpError",
                            new Object[] { optionsArrStr.toString() }) +
                        "\n \n" +
                        messages.getString(
                            wizardType + ".hostnamePanel.errorReason") +
                        "\n \n" +
                        messages.getString(
                            wizardType + ".hostnamePanel.subNetErrorDesc") +
                        "\n" +
                        messages.getString(
                            wizardType + ".hostnamePanel.ipMisMatchError") +
                        "\n" +
                        messages.getString(
                            wizardType + ".hostnamePanel.noIpmpGroup"));

                    String bContinue = TTYDisplay.getConfirmation(messages
                            .getString("hostwizard.hostnamePanel.ipmpContinue"),
                            yes, no);

                    if (bContinue.equals(yes)) {
                        hostNameOk = true;
                    } else {
                        hostNameOk = false;
                    }
                } else {
		    hostNameOk = true;
		}

                if (hostNameOk) {

                    wizardModel.setWizardValue(Util.HOST_NAME, hostNames);

                    // Generate the Resource and the ResourceGroup names
                    // Based on the Resource
                    String rName;
                    String rgName;

                    // Discover the Names using the above pattern

                    if (wizardType.equals(
                                LogicalHostWizardCreator.WIZARDTYPE)) {

                        // Resource Name
                        LogicalHostMBean mBean = LogicalHostWizardCreator
                            .getLogicalHostMBean(localConnection);
                        String disResourcePropertyName[] = {
                                Util.RESOURCENAME
                            };
                        HashMap discoveredMap = mBean.discoverPossibilities(
                                disResourcePropertyName, hostNames);
                        rName = (String) discoveredMap.get(Util.RESOURCENAME);

                        // Resource Group Name
                        String disRGPropertyName[] = { Util.RESOURCEGROUPNAME };
                        discoveredMap = mBean.discoverPossibilities(
                                disRGPropertyName, hostNames);
                        rgName = (String) discoveredMap.get(
                                Util.RESOURCEGROUPNAME);
                    } else {

                        // Resource Name
                        SharedAddressMBean mBean = SharedAddressWizardCreator
                            .getSharedAddressMBean(localConnection);
                        String disResourcePropertyName[] = {
                                Util.RESOURCENAME
                            };
                        HashMap discoveredMap = mBean.discoverPossibilities(
                                disResourcePropertyName, hostNames);
                        rName = (String) discoveredMap.get(Util.RESOURCENAME);

                        // Resource Group Name
                        String disRGPropertyName[] = { Util.RESOURCEGROUPNAME };
                        discoveredMap = mBean.discoverPossibilities(
                                disRGPropertyName, hostNames);
                        rgName = (String) discoveredMap.get(
                                Util.RESOURCEGROUPNAME);
                    }

                    // Set the discovered values
                    wizardModel.setWizardValue(Util.RESOURCENAME, rName);
                    wizardModel.setWizardValue(Util.RESOURCEGROUPNAME, rgName);
                }
            }
        } while (!hostNameOk);
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#isDisplayComplete
     */
    public boolean isDisplayComplete() {
        return true;
    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }

    /**
     * All Validations Done in this Panel
     *
     * @return  ErrorValue
     */
    private ErrorValue validate(String property[], HashMap data, Object helper,
        String wizardType) {

        ErrorValue errValue = null;

        if (wizardType.equals(LogicalHostWizardCreator.WIZARDTYPE)) {

            // Contact the LogicalHostMBean and Get All Properties
            LogicalHostMBean mBean = LogicalHostWizardCreator
                .getLogicalHostMBean(localConnection);
            errValue = mBean.validateInput(property, data, helper);
        } else {
            SharedAddressMBean mBean = SharedAddressWizardCreator
                .getSharedAddressMBean(localConnection);
            errValue = mBean.validateInput(property, data, helper);
        }

        return errValue;
    }

    /**
     * Discover the IPMPGroups for the Hostnames and Nodelist and set the
     * wizardModel accordingly
     *
     * @return  "True" If IPMPGroups could be discovered "False" If IPMPGroups
     * could not be discovered
     */
    private boolean discoverIPMPGroups(String nodeListArr[],
        String wizardType) {

        // Subnet and IPVersion Validation

        // Discover IPMPGroups for the Hostnames
        // The IPMPGroups are discovered based on the Nodelist
        // There should be a mechanism by which the Nodelist is
        // Specified to the Wizard if invoked by another wizard


        // LogicalHost Wizard
        if (wizardType.equals(LogicalHostWizardCreator.WIZARDTYPE)) {
            LogicalHostMBean mBean = LogicalHostWizardCreator
                .getLogicalHostMBean(localConnection);
            String disPropertyName[] = { Util.NETIFLIST };
            Map helperMap = new HashMap();
            helperMap.put(Util.HOSTNAMELIST,
                wizardModel.getWizardValue(Util.HOSTNAMELIST));
            helperMap.put(Util.NODELIST, nodeListArr);
	    Boolean b = (Boolean)wizardModel.getWizardValue(Util.EXCLUSIVEIP);
	    helperMap.put(Util.EXCLUSIVEIP, b);

            HashMap discoveredMap = mBean.discoverPossibilities(disPropertyName,
                    helperMap);

            // Check whether there are any IPMPGroups discovered
            if (discoveredMap.get(Util.NETIFLIST) != null) {
                List ipmpList = (List) discoveredMap.get(Util.NETIFLIST);

                // Store the discovered list in the WizardModel
                String ipmpNames[] = new String[ipmpList.size()];
                int i = 0;
                Iterator iter = ipmpList.iterator();

                while (iter.hasNext()) {
                    ipmpNames[i++] = (String) iter.next();
                }

                wizardModel.setWizardValue(Util.NETIFLIST, ipmpNames);
                wizardModel.setWizardValue(
                    LogicalHostWizardConstants.SEL_NETIFLIST, ipmpNames);

                return true;
            } else {

                // No IPMP Groups were discovered
                return false;
            }
        }
        // Shared Address
        else {
            SharedAddressMBean mBean = SharedAddressWizardCreator
                .getSharedAddressMBean(localConnection);
            String disPropertyName[] = { Util.NETIFLIST };
            Map helperMap = new HashMap();
            helperMap.put(Util.HOSTNAMELIST,
                wizardModel.getWizardValue(Util.HOSTNAMELIST));
            helperMap.put(Util.NODELIST, nodeListArr);

            HashMap discoveredMap = mBean.discoverPossibilities(disPropertyName,
                    helperMap);

            // Check whether there are any IPMPGroups discovered
            if (discoveredMap.get(Util.NETIFLIST) != null) {
                List ipmpList = (List) discoveredMap.get(Util.NETIFLIST);

                // Store the discovered list in the WizardModel
                String ipmpNames[] = new String[ipmpList.size()];
                int i = 0;
                Iterator iter = ipmpList.iterator();

                while (iter.hasNext()) {
                    ipmpNames[i++] = (String) iter.next();
                }

                wizardModel.setWizardValue(Util.NETIFLIST, ipmpNames);
                wizardModel.setWizardValue(
                    LogicalHostWizardConstants.SEL_NETIFLIST, ipmpNames);

                return true;
            } else {

                // No IPMP Groups were discovered
                return false;
            }
        }
    }
}
