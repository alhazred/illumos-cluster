/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)IteratorLayout.java	1.15	08/07/14 SMI"
 */

package com.sun.cluster.dswizards.clisdk.core;

import com.sun.cluster.dswizards.common.WizardI18N;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Insets;
import java.awt.LayoutManager2;

import java.lang.reflect.*;

import java.util.*;

import javax.swing.*;


/**
 * The IteratorLayout is a layout manager for a container. It iterates through a
 * tree of WizardComponents using the tree traversal methods <code>next</code>
 * and <code>previous</code>.
 */
public class IteratorLayout implements LayoutManager2, java.io.Serializable,
    Runnable {

    // Change this value only when making a serializationally
    // incompatible change such as deleting a field. See the
    // Java Object Serialization Specification, Chapter 5:
    // Versioning of Serializable Objects, for more details.
    private static final long serialVersionUID = 3523468479477070372L;

    /**
     * A flag that indicates whether debug messages should be printed or not.
     */
    private static boolean debugPanels = false;

    /**
     * This block of code sets the debug flag.
     */
    static {

        try {
            String debugPanelsString = System.getProperty("wizard.debugPanels");

            if (debugPanelsString != null) {
                debugPanels = true;
            }
        } catch (SecurityException e) {
        }
    }


    /**
     * This list contains all of the components that have been added to the
     * parent.
     */
    protected Vector componentList = new Vector();

    /** This is the root of the wizard tree. */
    protected WizardComposite root = null;

    /** This is the current leaf being visited by this iterator. */
    protected WizardComponent currentLeaf = null;

    /**
     * This is the parent container into which the components are being added.
     */
    protected Container parent = null;

    /** Wizard tree manager object. */
    private WizardTreeManager manager = null;

    /** First panel */
    private String firstPanel = null;

    /** Last panel */
    private String lastPanel = null;

    /** Indicates whether the panel is visible. */
    private boolean visible = true;

    /** Locking object for visible variable. */
    private Object visibleLock = new Object();

    /** Locking object for wizard tree root. */
    private Object treeRootLock = new Object();

    /**
     * Interative navigation thread for CLI/TTY mode to navigate all panels
     * after user input.
     */
    private Thread navThread = null;

    /** A flag that indicates whether or not the user is traversing forward. */
    private boolean traversingForward = true;

    /** Stores the previous panels visited */
    private List prevPanelList = null;

    /**
     * Some temporary member variables for inner class usage during
     * SwingUtilities.invokeLater()
     */
    Component tempC = null;
    Component tempChildren[] = null;
    int tempI = 0;

    /**
     * Creates a new iterator layout with the specified wizard tree manager.
     *
     * @param  manager  The WizardTreeManager.
     */
    public IteratorLayout(WizardTreeManager manager) {
        this.manager = manager;
    }

    /**
     * Adds the specified component to this iterator layout.  An application
     * should use the <code>setRoot</code> method to set the root of the tree.
     *
     * @param  comp  The component to be added.
     * @param  constraints  This should be null.
     */
    public void addLayoutComponent(Component comp, Object constraints) {
        addLayoutComponent("", comp);
    }

    /**
     * @deprecated  replaced by <code>addLayoutComponent(Component,
     * Object)</code>
     */
    public void addLayoutComponent(String name, Component comp) {

        if (!componentList.contains(comp)) {
            componentList.addElement(comp);
        }
    }

    /**
     * Removes the specified component from the layout.
     *
     * @param  comp  The component to be removed.
     */
    public void removeLayoutComponent(Component comp) {

        if (componentList.contains(comp)) {
            componentList.removeElement(comp);
        }
    }

    /**
     * Determines the preferred size of the container argument using this
     * iterator layout.
     *
     * @param  parent  The parent container.
     *
     * @return  The preferred dimensions to lay out the subcomponents.
     */
    public Dimension preferredLayoutSize(Container parent) {
        this.parent = parent;

        return (parent.getSize());
    }

    /**
     * Determines the minimum size for the specified panel.
     *
     * @param  parent  The parent container.
     *
     * @return  The minimum dimensions to lay out the subcomponents.
     */
    public Dimension minimumLayoutSize(Container parent) {
        this.parent = parent;

        return (parent.getSize());
    }

    /**
     * Returns the maximum dimensions for this layout
     *
     * @param  parent  The parent container.
     *
     * @return  The maximum dimensions to lay out the subcomponents.
     */
    public Dimension maximumLayoutSize(Container parent) {
        return (new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
    }

    /**
     * Returns the alignment along the x axis.  This specifies how the component
     * would like to be aligned relative to other components.  The value should
     * be a number between 0 and 1 where 0 represents alignment along the
     * origin, 1 is aligned the furthest away from the origin, 0.5 is centered,
     * etc.
     *
     * @param  parent  The parent container.
     *
     * @return  The horizontal alignment.
     */
    public float getLayoutAlignmentX(Container parent) {
        this.parent = parent;

        return ((float) 0.5);
    }

    /**
     * Returns the alignment along the y axis.  This specifies how the component
     * would like to be aligned relative to other components.  The value should
     * be a number between 0 and 1 where 0 represents alignment along the
     * origin, 1 is aligned the furthest away from the origin, 0.5 is centered,
     * etc.
     *
     * @param  parent  The parent container.
     *
     * @return  The vertical alignment.
     */
    public float getLayoutAlignmentY(Container parent) {
        this.parent = parent;

        return ((float) 0.5);
    }

    /**
     * Invalidates the layout, indicating that if the layout manager has cached
     * information it should be discarded.
     *
     * @param  parent  The parent container.
     */
    public void invalidateLayout(Container parent) {
        this.parent = parent;
    }

    /**
     * Lays out the specified container using this iterator layout.
     *
     * @param  parent  The parent container.
     */
    public void layoutContainer(Container parent) {
        this.parent = parent;
    }

    /**
     * Returns the root of the panel tree currently being manipulated by this
     * iterator.
     */
    public WizardComposite getRoot() {
        return (this.root);
    }

    /**
     * Sets the root of the wizard client tree.  This should be called only
     * after the tree has been completely created.  If the tree must grow at
     * runtime, use <code>addSubtree</code> to ensure that the panels are added
     * to the container correctly.
     *
     * @param  parent  The parent container.
     * @param  root  The root of the wizard client tree.
     */
    public synchronized void setRoot(Container parent, WizardComposite root) {
        setRoot(parent, root, null, null);
    }

    /**
     * Sets the root of the wizard client tree.  This should be called only
     * after the tree has been completely created.  If the tree must grow at
     * runtime, use <code>addSubtree</code> to ensure that the panels are added
     * to the container correctly.
     *
     * @param  parent  The parent container.
     * @param  root  The root of the wizard client tree.
     * @param  firstPanel  The first panel of the wizard that should be
     * displayed.
     * @param  lastPanel  The last panel of the wizard that should be displayed.
     */
    public void setRoot(Container parent, WizardComposite root,
        String firstPanel, String lastPanel) {
        setVisible(firstPanel == null);

        this.firstPanel = firstPanel;
        this.lastPanel = lastPanel;

        setVisible(true);

        if (currentLeaf != null) {
            currentLeaf.abortDisplay();
            currentLeaf.setVisible(true);
        }

        if (root != this.root) {

            /*
             * Remove the current tree from the container.
             */
            removeSubtree(parent, this.root);

            /*
             * Be certain that the panels have been added to the container.
             */
            addSubtree(parent, root);
        }

        this.parent = parent;
        this.root = root;
        this.traversingForward = true;
        setCurrentLeaf(root.next());
    }

    /**
     * This method retrun a lock on the wizard tree root.
     *
     * @return  locking object on the wizard tree root
     */
    public synchronized Object getTreeRootLock() {
        return (this.treeRootLock);
    }

    /**
     * This method puts a thread into wait state with the milliseconds specified
     *
     * @param  millisecond  time to wait
     */
    private void wait(int milliseconds) {

        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException ie) {
        }
    }

    /**
     * This method sets the panel visibility mode.  The visibility  is used to
     * hide panels being visited under the -warp command.
     *
     * @param  visible  If true, the panel will be visible.
     */
    private void setVisible(boolean visible) {

        synchronized (this.visibleLock) {
            this.visible = visible;
        }
    }

    /**
     * Returns the visibility flag.
     */
    protected boolean getVisible() {

        synchronized (this.visibleLock) {
            return (this.visible);
        }
    }

    /**
     * Adds the leaves of the specified subtree to the container so they can be
     * visited correctly.
     *
     * @param  parent  The parent container.
     * @param  subtreeRoot  The root of the subtree.
     */
    public void addSubtree(Container parent, WizardComponent subtreeRoot) {

        /**
         * Be sure to add every WizardLeaf object to the
         * container.  Do not use the next() method on the
         * subtreeRoot, as some of the panels will be skipped.
         */
        this.parent = parent;

        if (subtreeRoot instanceof WizardLeaf) {
            subtreeRoot.setVisible(false);
            subtreeRoot.setWizardTreeManager(this.manager);

            if (parent != null) {

                if (!parent.isAncestorOf(subtreeRoot)) {
                    parent.add(subtreeRoot);
                }
            }

            return;
        }

        if (subtreeRoot instanceof WizardComposite) {

            /*
             * Get the children, and add each child to the
             * container.
             */
            Enumeration children = ((WizardComposite) subtreeRoot)
                .getChildren();

            if (children != null) {

                while (children.hasMoreElements()) {
                    WizardComponent child = (WizardComponent) children
                        .nextElement();

                    addSubtree(parent, child);
                }
            }

            return;
        }
    }

    /**
     * Removes the leaves of the specified subtree to the container so they can
     * be visited correctly.
     *
     * @param  parent  The parent container.
     * @param  subtreeRoot  The root of the subtree.
     */
    public void removeSubtree(Container parent, WizardComponent subtreeRoot) {

        /**
         * Be sure to remove every WizardLeaf object to the
         * container.  Do not use the next() method on the
         * subtreeRoot, as some of the panels will be skipped.
         */
        this.parent = parent;

        if (subtreeRoot instanceof WizardLeaf) {
            subtreeRoot.setVisible(false);

            if (parent != null) {

                if (parent.isAncestorOf(subtreeRoot)) {
                    parent.remove(subtreeRoot);
                }
            }

            return;
        }

        if (subtreeRoot instanceof WizardComposite) {

            /*
             * Get the children, and remove each child from the
             * container.
             */
            Enumeration children = ((WizardComposite) subtreeRoot)
                .getChildren();

            if (children != null) {

                while (children.hasMoreElements()) {
                    WizardComponent child = (WizardComponent) children
                        .nextElement();

                    removeSubtree(parent, child);
                }
            }

            return;
        }
    }

    /**
     * Visits the specified leaf. This should not be called from an application.
     *
     * @param  newLeaf  The leaf to be visited.
     */
    public void setCurrentLeaf(WizardComponent newLeaf) {

        if ((currentLeaf != null) && (newLeaf != null)) {
            currentLeaf.setVisible(false);

            if (getVisible() && (currentLeaf.getName() != null)) {
                setPanelVisibility();
            }
        }

        /*
         * See if this is the last visible panel (from the -warp command
         * line).
         */
        if (getVisible() && (lastPanel != null)) {
            String name = currentLeaf.getName();

            if ((name != null) && name.equals(lastPanel)) {

                /*
                 * Turn off visibility.
                 */
                setVisible(false);
            }
        }

        if (System.getProperty("wizard.debugAutonext") != null) {
            // Do Nothing
        }

        if (newLeaf != null) {
            this.currentLeaf = newLeaf;
            setPanelVisibility();

        }
    }

    /**
     * This method sets the visibility of the panel.
     */
    private void setPanelVisibility() {
        setVisible(true);
    }

    /**
     * This method displays the currentPanel to user.
     *
     * @param  currentLeaf  The current panel to be displayed
     */
    public void displayCurrentPanel(WizardComponent currentLeaf) {

        currentLeaf.beginDisplay();

        setPanelVisibility();
    }

    /**
     * Thid method displays a CLI panel for user interaction.
     *
     * @param  currentLeaf  The current panel
     */
    public void cliConsoleInteraction(WizardComponent currentLeaf) {

        /**
         * Invoke the non-GUI method if we are in non-display mode
         */
        if (debugPanels == true) {
            // Do Nothing
        }

        try {
            currentLeaf.consoleInteraction();
        } catch (Exception ex) {
	    ex.printStackTrace();
            WizardI18N wizardi18n = new WizardI18N(
                    "com.sun.cluster.dswizards.common.WizardResource");
            TTYDisplay.printError(wizardi18n.getString(
                    "cliwizards.general.error.summary") + "\n\n" +
                wizardi18n.getString("cliwizards.general.error.detail"));
            TTYDisplay.promptForEntry(wizardi18n.getString(
                    "cliwizards.general.error.exit"));
            System.exit(0);
        }

        if (debugPanels == true) {
            // Do Nothing
        }
    }

    /**
     * Interactively navigate to the next panel.
     */
    public void toNextPanel() {

        if (this.navThread == null) {

            if (debugAutonext()) {
                // Do Nothing
            }

            this.navThread = new Thread(this);
            this.navThread.start();
        }
    }

    /**
     * This method interatively navigates a panel in CLI/TTY mode.
     */
    public void run() {
        boolean done = false;

        while (!done) {
            WizardComponent origPanel = this.getCurrentPanel();
            done = origPanel.isLast();

            if (displayPanel(origPanel)) {

                // panel was changed. Check origPanel again
                done = this.getCurrentPanel().isLast();
            }
        }

        manager.exitButtonPressed();
        this.navThread = null;
    }

    /**
     * Displays and navigates a panel interactively.
     */
    public boolean displayPanel(WizardComponent currentPanel) {
        WizardComponent panel = currentPanel;
        cliConsoleInteraction(currentPanel);

        if (panel != this.currentLeaf) {

            // the panel was changed in the cliConsoleInteraction
            return true;
        }

        if (panel.cancelDirection) {
            this.manager.backButtonPressed();

            return true;
        }

        // console interaction finished without panel changes. Navigate to the
        // next panel
        this.manager.nextButtonPressed();

        return false;
    }

    /**
     * Exit the applet/application with the specified exit status.  By
     * convention, a non-zero status indicates abnormal termination.  This
     * method never returns.
     *
     * @param  status  The exit status.
     */
    public void exit() {
        this.manager.exitButtonPressed();
    }

    /**
     * Visits the next leaf in the tree. The next leaf will be displayed in the
     * parent container.
     */
    public void next() {

        // Get the next Panel Name from the WizardFlow.
        String nextPanelName = currentLeaf.getNextPanelName();

        // Store the PanelNames in a List.
        if (prevPanelList == null) {
            prevPanelList = new ArrayList();
        }

        prevPanelList.add(currentLeaf.getName());

        // Get the Root of the Wizard
        WizardComposite wizardRoot = manager.getWizardCreator().getRoot();

        // Get the child corresponding to the name
        WizardComponent nextLeaf = wizardRoot.getChild(nextPanelName);

        // Set the current leaf to the desired leaf
        setCurrentLeaf(nextLeaf);
    }

    /**
     * Visits the previous leaf in the tree. The previous leaf will be displayed
     * in the parent container.
     */
    public void previous() {

        currentLeaf.abortDisplay();
        traversingForward = false;

        // Get the Last Panel's name and remove it from the index.
        int lastPanelIndex = prevPanelList.size() - 1;
        String lastPanelName = (String) prevPanelList.remove(lastPanelIndex);

        // Get the Root of the Wizard
        WizardComposite wizardRoot = manager.getWizardCreator().getRoot();

        // Get the child corresponding to the name
        WizardComponent prevLeaf = wizardRoot.getChild(lastPanelName);

        // Set the current leaf to the desired leaf
        setCurrentLeaf(prevLeaf);
    }

    /**
     * Visits the leaf in the tree that is referenced by the current leaf for a
     * cancel operation.  The current leaf has the option of whether the cancel
     * operation should move forward in the tree, or backward.
     */
    public void cancel() {

        if (currentLeaf != null) {

            /*
             * Indicate that the current panel is being
             * aborted (by traversing to the previous panel.
             *
             * This allows the panel to do cleanup (for example
             * restore navigation button labels) before exiting.
             */
            if (debugPanels == true) {
                // Do Nothing
            }

            WizardComponent leaf = currentLeaf;

            currentLeaf.abortDisplay();

            if (debugPanels == true) {
                // Do Nothing
            }

            if (leaf != currentLeaf) {

                /*
                 * The panel was changed in the abortDisplay method call.
                 * Return here to avoid duplicating calls that have already
                 * been done through recursion.
                 */
                return;
            }

            this.traversingForward = true;
            leaf = currentLeaf.cancel();

            setCurrentLeaf(leaf);

            if (leaf != currentLeaf) {

                /*
                 * The panel was changed in the setCurrentLeaf method.  This
                 * occurred because of code in the beginDisplay method or the
                 * consoleInteraction method.  Either way, we want to exit
                 * here to avoid duplicating calls that have already been
                 * done through recursion.
                 */
                return;
            }
        }
    }

    /**
     * Returns the currently displayed panel.
     *
     * @return  The panel currently being displayed.
     */
    public WizardComponent getCurrentPanel() {
        return (currentLeaf);
    }

    /**
     * Returns true if the user pressed "next" to get to the current panel,
     * false if the user pressed "back".
     *
     * @return  boolean indicates the panel traversing direction
     */
    public boolean isTraversingForward() {
        return (traversingForward);
    }

    /*
     * This method checks whether the wizard should print debug messages.
     *
     * @return  boolean indicates whether that the wizard should be launched
     *          in debug mode
     */
    private boolean isDebug() {
        return (System.getProperty("wizard.debugIteratorLayout") != null);
    }

    /*
     * This method checks whether the autonext debug message should be
     * printed.
     *
     * @return boolean value indicates whether that autonext debug message
     *         should be printed
     */
    private boolean debugAutonext() {
        return (System.getProperty("wizard.debugAutonext") != null);
    }

    class BeginDisplayThread implements Runnable {
        private WizardComponent wizardComponent = null;

        public BeginDisplayThread(WizardComponent currentLeaf) {
            this.wizardComponent = currentLeaf;
        }

        public void run() {
            this.wizardComponent.beginDisplay();
        }
    }


    class ValidateThread implements Runnable {
        private Container parent = null;

        public ValidateThread(Container parent) {
            this.parent = parent;
        }

        public void run() {
            this.parent.validate();
        }
    }

    class InvalidateThread implements Runnable {
        private Component component = null;

        public InvalidateThread(Component component) {
            this.component = component;
        }

        public void run() {
            this.component.invalidate();
        }
    }
}
