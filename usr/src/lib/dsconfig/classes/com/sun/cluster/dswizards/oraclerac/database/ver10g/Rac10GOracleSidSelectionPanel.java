/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)Rac10GOracleSidSelectionPanel.java	1.23	09/04/22 SMI"
 */

package com.sun.cluster.dswizards.oraclerac.database.ver10g;

// CMASS
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.utils.Util;

// Wizard Common
import com.sun.cluster.dswizards.common.CliUtil;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.oraclerac.database.*;
import com.sun.cluster.dswizards.oraclerac.invoker.RACInvokerCreator;

// Model
import com.sun.cluster.model.RACModel;

// J2SE
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

/**
 * This panel gets the Oracle SID from the User
 */
public class Rac10GOracleSidSelectionPanel extends RacDBBasePanel {

    /**
     * Creates an Rac10GOracleSidSelectionPanel for the wizard with the given
     * name and associates it with the WizardModel and WizardFlow.
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     * @param  wizardType  WizardType
     */
    public Rac10GOracleSidSelectionPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(name, model, wizardFlow, wizardi18n);
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        title = wizardi18n.getString("oraclerac.oracleSidSelectionPanel.title");

        String help_text = wizardi18n.getString(
                "oraclerac.oracleSidSelectionPanel.cli.help");
        String invalidSelection = wizardi18n.getString(
                "oraclerac10g.oracleSidSelectionPanel.invalidselection");

        String selected_orasid = null;
        String defSelection = null;
        Vector vOraSid = new Vector();

        String curNode = (String) wizardModel.getWizardValue(
                Util.RAC10G_CURNODENAME);
        String nodeList[] = (String[]) wizardModel.getWizardValue(
                Util.RG_NODELIST);

        if (curNode == null) {
            String split_str[] = nodeList[0].split(Util.COLON);
            curNode = split_str[0];
            wizardModel.setWizardValue(Util.RAC10G_NODEIDX, new Integer(0));
            wizardModel.setWizardValue(Util.RAC10G_CURNODENAME, curNode);
        }

        String desc = wizardi18n.getString(
                "oraclerac.oracleSidSelectionPanel.desc",
                new String[] { curNode });
        String orasid_prompt = wizardi18n.getString(
                "oraclerac10g.oracleSidSelectionPanel.prompt",
                new String[] { curNode });

	String baseClusterNodelist[] = (String[])wizardModel.getWizardValue(
	    Util.BASE_CLUSTER_NODELIST);
        // Discover the sid on the current node. Pass the node name,
        // selected CRS Home and selected DB_NAME as helper data
        String props[] = new String[] { Util.ORACLE_SID };
        String selectedDbName = (String) wizardModel.getWizardValue(
                Util.SEL_DB_NAME);
        String selectedCrsHome = (String) wizardModel.getWizardValue(
                Util.SEL_CRS_HOME);

        String selZoneCluster = CliUtil.getZoneClusterName(wizardModel);

        HashMap helperData = new HashMap();
        helperData.put(Util.NODENAME, curNode);
        helperData.put(Util.DB_NAME, selectedDbName);
        helperData.put(Util.CRS_HOME, selectedCrsHome);
	helperData.put(Util.SEL_ZONE_CLUSTER, selZoneCluster);

	RACModel racModel = RACInvokerCreator.getRACModel();

        // discover Oracle Sid on all nodes
        for (int i = 0; i < baseClusterNodelist.length; i++) {
            String split_str[] = baseClusterNodelist[i].split(Util.COLON);

            // oracle sid on baseClusterNodelist[i]

            HashMap map = racModel.discoverPossibilities(
		null, split_str[0], props, helperData);

            Vector tmpVec = (Vector) map.get(Util.ORACLE_SID);

            if (tmpVec == null) {
                tmpVec = new Vector();
            }

            int size = tmpVec.size();

            for (int j = 0; j < size; j++) {
                String orasid = (String) tmpVec.elementAt(j);

		if (orasid != null &&
		    orasid.trim().length() > 0) {
		    if (!vOraSid.contains(orasid)) {
			vOraSid.add(orasid);
		    }
		}
            }
        }

        String optionsArr[] = (String[]) vOraSid.toArray(new String[0]);

        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        // Add SID to the property names to be serialized
        List toBeSerialized = new ArrayList();
        Object tmpObj = (ArrayList) wizardModel.getWizardValue(
                Util.WIZARD_STATE);

        if (tmpObj != null) {
            toBeSerialized = (ArrayList) tmpObj;
        }

        if (!toBeSerialized.contains(Util.SEL_ORACLE_SID + curNode)) {
            toBeSerialized.add(Util.SEL_ORACLE_SID + curNode);
        }

        wizardModel.setWizardValue(Util.WIZARD_STATE, toBeSerialized);

        // get the default selection
        selected_orasid = (String) wizardModel.getWizardValue(
                Util.SEL_ORACLE_SID + curNode);

        if (selected_orasid != null) {
            defSelection = selected_orasid;
        }

        String choice = no;
        HashMap custom = new HashMap();
        custom.put(enterKey, enterText);

        while (choice.equalsIgnoreCase(no)) {
            TTYDisplay.printTitle(title);

            // Get the oracle sid preference
            if ((optionsArr != null) && (optionsArr.length > 0)) {
                List selList = TTYDisplay.getScrollingOption(desc, optionsArr,
                        custom, defSelection, help_text, true);

                if (selList == null) {
		    // Back key pressed
		    processBackKey(nodeList, curNode);
                    return;
                }

                TTYDisplay.setNavEnabled(true);
                this.cancelDirection = false;

                String selArr[] = (String[]) selList.toArray(new String[0]);
                selected_orasid = selArr[0];
            } else {
                selected_orasid = enterText;
            }

            TTYDisplay.clear(1);

            if (selected_orasid.equals(enterText)) {
                selected_orasid = "";

                while (selected_orasid.trim().length() == 0) {
                    selected_orasid = TTYDisplay.promptForEntry(orasid_prompt);

                    if (selected_orasid.equals(back)) {
			// Back key pressed
			processBackKey(nodeList, curNode);
                        return;
                    }
                }
            }

            String nodeName = getNodeName(selected_orasid);

            if (nodeName != null) {
                String sidiinuse_error = wizardi18n.getString(
                        "oraclerac10g.oracleSidSelectionPanel.sidinuse",
                        new String[] { nodeName });

                selected_orasid = "";
                TTYDisplay.printError(sidiinuse_error);
                TTYDisplay.promptForEntry(continue_txt);

                choice = no;

                continue;
            }

            // validate the Oracle Sid
            ErrorValue retVal = validateInput(selected_orasid, baseClusterNodelist,
		selZoneCluster);

            if (!retVal.getReturnValue().booleanValue()) {
                TTYDisplay.printInfo(retVal.getErrorString());

                String enterAgain = TTYDisplay.getConfirmation(enteragain_txt,
                        yes, no);

                if (enterAgain.equalsIgnoreCase(yes)) {
                    choice = no;

                    continue;
                }
            }

            choice = yes;

            // Store the Values in the WizardModel
            wizardModel.selectCurrWizardContext();
            wizardModel.setWizardValue(Util.SEL_ORACLE_SID + curNode,
                selected_orasid);
        }

        Integer nodeIndex = (Integer) wizardModel.getWizardValue(
                Util.RAC10G_NODEIDX);
        int nodeIdx = nodeIndex.intValue();

        if (++nodeIdx < nodeList.length) {
            String nodeName = nodeList[nodeIdx];
            String split_str[] = nodeName.split(Util.COLON);
            curNode = split_str[0];
            wizardModel.setWizardValue(Util.RAC10G_CURNODENAME, curNode);
            wizardModel.setWizardValue(Util.RAC10G_NODEIDX,
                new Integer(nodeIdx));
            wizardModel.setWizardValue(Util.RAC10G_NODELIST_COMPLETE,
                Boolean.FALSE);
        } else {
            wizardModel.setWizardValue(Util.RAC10G_NODELIST_COMPLETE,
                Boolean.TRUE);
        }

    }

    private void processBackKey(String[] nodeList, String curNode) {
	this.cancelDirection = true;

	Integer nodeIndex = (Integer) wizardModel.getWizardValue(
	    Util.RAC10G_NODEIDX);
	int nodeIdx = nodeIndex.intValue();

	if (--nodeIdx >= 0) {
	    String nodeName = nodeList[nodeIdx];
	    String split_str[] = nodeName.split(Util.COLON);
	    curNode = split_str[0];
	    wizardModel.setWizardValue(Util.RAC10G_CURNODENAME, curNode);
	    wizardModel.setWizardValue(Util.RAC10G_NODEIDX,
		new Integer(nodeIdx));
	    wizardModel.setWizardValue(Util.RAC10G_NODELIST_COMPLETE,
		Boolean.FALSE);
	}
	return;
    }

    private String getNodeName(String selectedOraSid) {
        String nodeList[] = (String[])wizardModel.getWizardValue(
                Util.RG_NODELIST);
	Integer nodeIndex = (Integer)wizardModel.getWizardValue(
		Util.RAC10G_NODEIDX);
	int nodeIdx = nodeIndex.intValue();

        boolean found = false;
        int i = 0;
        String orasid = null;
        String curnode = (String) wizardModel.getWizardValue(
                Util.RAC10G_CURNODENAME);
        String nodename = null;

	// Only check for the sid value on nodes that have been displayed before
	// this current panel
        while ((i < nodeIdx) && !found) {
            String split_str[] = nodeList[i].split(Util.COLON);
            nodename = split_str[0];

            if (!nodename.equals(curnode)) {
                orasid = (String) wizardModel.getWizardValue(
                        Util.SEL_ORACLE_SID + nodename);

                if ((orasid != null) && orasid.equals(selectedOraSid)) {
                    found = true;
                }
            }

            i++;
        }

        if (!found) {
            return null;
        }

        return nodename;
    }

    private ErrorValue validateInput(String oraSid, String baseClusterNodelist[],
	String selZoneCluster) {
        String propName[] = { Util.ORACLE_SID };
        HashMap userInput = new HashMap();
        userInput.put(Util.ORACLE_SID, oraSid);

        ErrorValue err = new ErrorValue();
        err.setReturnVal(Boolean.TRUE);

        StringBuffer errStr = new StringBuffer();
        int err_count = 0;
	HashMap helperData = new HashMap();
	helperData.put(Util.SEL_ZONE_CLUSTER, selZoneCluster);
	RACModel racModel = RACInvokerCreator.getRACModel();

        for (int i = 0; i < baseClusterNodelist.length; i++) {
	    String[] splitStr = baseClusterNodelist[i].split(Util.COLON);
            ErrorValue errVal = racModel.validateInput(null, splitStr[0], propName, userInput,
		helperData);

            if (!errVal.getReturnValue().booleanValue()) {

                // error received.
                err_count++;
                errStr = new StringBuffer(errVal.getErrorString());
            }
        }

        if (err_count == baseClusterNodelist.length) {

            // failed on all nodes.
            err.setReturnVal(Boolean.FALSE);
            err.setErrorString(errStr.toString());
        }

        return err;
    }
}
