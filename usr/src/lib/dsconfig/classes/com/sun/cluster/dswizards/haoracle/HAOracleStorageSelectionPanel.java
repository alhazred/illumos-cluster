/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)HAOracleStorageSelectionPanel.java 1.13     08/12/10 SMI"
 */


package com.sun.cluster.dswizards.haoracle;

import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliStorageSelectionPanel;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.hastorageplus.HASPWizardConstants;

import java.util.Vector;


/**
 * The Panel Class that displays existing HAStoragePlus resources
 */
public class HAOracleStorageSelectionPanel extends CliStorageSelectionPanel {

    /**
     * Creates a new instance of HAOracleStorageSelectionPanel
     */
    public HAOracleStorageSelectionPanel() {
        super();
    }

    /**
     * Creates a HostnameEntry Panel with the given name and manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     * @param  flow  Handler to the WizardFlow
     */
    public HAOracleStorageSelectionPanel(String name,
        WizardTreeManager manager) {
        super(name, manager);
    }

    /**
     * Creates a HostnameEntry Panel with the given name and Wizard state
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     */
    public HAOracleStorageSelectionPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(name, model, wizardFlow, wizardi18n);
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.
     *		WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        desc = wizardi18n.getString(
                "haoracle.oracleStorageSelectionPanel.desc");
        wizardModel.setWizardValue(HAOracleWizardConstants.CREATE_HASP_RS, "");

        boolean singleSelection = false;
        super.consoleInteraction(singleSelection);
        wizardModel.setWizardValue(HAOracleWizardConstants.CREATE_HASP_RS,
            wizardModel.getWizardValue(Util.CREATE_NEW));
        wizardModel.setWizardValue(HAOracleWizardConstants.SEL_HASP_RS,
            wizardModel.getWizardValue(Util.SEL_RS));
        wizardModel.setWizardValue(HAOracleWizardConstants.SEL_HASP_RG,
            wizardModel.getWizardValue(Util.SEL_RG));
        wizardModel.setWizardValue(HAOracleWizardConstants.NEW_FS_DEFINED,
            wizardModel.getWizardValue(Util.SEL_FILESYSTEMS));
        wizardModel.setWizardValue(HAOracleWizardConstants.NEW_DEVICES_DEFINED,
            wizardModel.getWizardValue(Util.SEL_DEVICES));
    }
}
