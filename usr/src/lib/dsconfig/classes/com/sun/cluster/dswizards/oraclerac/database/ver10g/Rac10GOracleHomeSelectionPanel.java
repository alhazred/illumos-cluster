/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)Rac10GOracleHomeSelectionPanel.java	1.19	09/04/22 SMI"
 */

package com.sun.cluster.dswizards.oraclerac.database.ver10g;

// CMASS
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.dataservices.common.ErrorValue;

// Wizard Common
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.common.CliUtil;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.oraclerac.database.*;
import com.sun.cluster.dswizards.oraclerac.invoker.RACInvokerCreator;

// Model
import com.sun.cluster.model.RACModel;

// J2SE
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

/**
 * This panel gets the Oracle Home Directory from the User
 */
public class Rac10GOracleHomeSelectionPanel extends RacDBBasePanel {

    /**
     * Creates an Rac10GOracleHomeSelectionPanel for the wizard with the given
     * name and associates it with the WizardModel and WizardFlow.
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     * @param  wizardType  WizardType
     */
    public Rac10GOracleHomeSelectionPanel(String name,
        CliDSConfigWizardModel model, WizardFlow wizardFlow,
        WizardI18N wizardi18n) {
        super(name, model, wizardFlow, wizardi18n);
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        title = wizardi18n.getString("haoracle.oracleHomeSelectionPanel.title");
        desc = wizardi18n.getString("haoracle.oracleHomeSelectionPanel.desc");

        String oraclehome_prompt = wizardi18n.getString(
                "haoracle.oracleHomeSelectionPanel.prompt");
        String help_text = wizardi18n.getString(
                "haoracle.oracleHomeSelectionPanel.cli.help");
        String invalidSelection = wizardi18n.getString(
                "oraclerac10g.oracleHomeSelectionPanel.invalidselection");

        String selectedOracleHome = null;
        String defaultOracleHome;

        // Get the discovered ORACLE_HOME from the dataservice
	String baseClusterNodelist[] = (String[])wizardModel.getWizardValue(
	    Util.BASE_CLUSTER_NODELIST);
        String propNames[] = new String[] { Util.ORACLE_HOME };
        Vector vOracleHome = new Vector();

        HashMap helperData = new HashMap();
        String selectedCrsHome = (String)wizardModel.getWizardValue(
	    Util.SEL_CRS_HOME);
        helperData.put(Util.CRS_HOME, selectedCrsHome);

        String selectedDbName = (String)wizardModel.getWizardValue(
	    Util.SEL_DB_NAME);
	helperData.put(Util.DB_NAME, selectedDbName);

        String selZoneCluster = CliUtil.getZoneClusterName(wizardModel);
	helperData.put(Util.SEL_ZONE_CLUSTER, selZoneCluster);

	RACModel racModel = RACInvokerCreator.getRACModel();

        for (int i = 0; i < baseClusterNodelist.length; i++) {
            String split_str[] = baseClusterNodelist[i].split(Util.COLON);

            HashMap map = racModel.discoverPossibilities(null, split_str[0],
		propNames, helperData);

            Vector tmpVec = (Vector) map.get(Util.ORACLE_HOME);

            if (tmpVec == null) {
                tmpVec = new Vector();
            }

            int size = tmpVec.size();

            for (int j = 0; j < size; j++) {
                String oraclehome = (String)tmpVec.elementAt(j);
		if (oraclehome != null && oraclehome.trim().length() > 0) {
		    if (!vOracleHome.contains(oraclehome)) {
			vOracleHome.add(oraclehome);
		    }
		}
            }
        } // finish discovery on all nodes

        HashMap custom = new HashMap();
        custom.put(enterKey, enterText);

        String optionsArr[] = (String[]) vOracleHome.toArray(new String[0]);

        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        String choice = no;

        while (choice.equalsIgnoreCase(no)) {
            TTYDisplay.printTitle(title);

            // get the default selection
            defaultOracleHome = (String)wizardModel.getWizardValue(
		Util.SEL_ORACLE_HOME);

            // Get the oracle home preference
            // see if an oracle home is already set
            if ((optionsArr != null) && (optionsArr.length > 0)) {
                List oracleHomeList = TTYDisplay.getScrollingOption(desc,
                        optionsArr, custom, defaultOracleHome, help_text, true);

                if (oracleHomeList == null) {
                    this.cancelDirection = true;

                    return;
                }

                TTYDisplay.setNavEnabled(true);
                this.cancelDirection = false;

                TTYDisplay.clear(1);

                if (oracleHomeList.size() != 1) {
                    TTYDisplay.printError(invalidSelection);

                    continue;
                } else {
                    String oracleArr[] = (String[]) oracleHomeList.toArray(
                            new String[0]);
                    selectedOracleHome = oracleArr[0];
                }
            } else {
                selectedOracleHome = enterText;
            }

            if (selectedOracleHome.equals(enterText)) {
                selectedOracleHome = "";

                while (selectedOracleHome.trim().length() == 0) {
                    selectedOracleHome = TTYDisplay.promptForEntry(
                            oraclehome_prompt);

                    if (selectedOracleHome.equals(back)) {
                        this.cancelDirection = true;

                        return;
                    }
                }
            }

            // validate against the oracle version
            String oracle_version = getDBVersion(selectedOracleHome);

            String stVersion = "";

            if (oracle_version.equals(Util.NINE)) {
                stVersion = wizardi18n.getString(
                        "oraclerac.versionPanel.option1");
            } else if (oracle_version.equals(Util.TEN)) {
                stVersion = wizardi18n.getString(
                        "oraclerac.versionPanel.option2");
            }

            String error_txt = wizardi18n.getString(
                    "oraclerac.oracleHomeSelectionPanel.error",
                    new String[] { stVersion });

            String selectedOraVersion = (String)wizardModel.getWizardValue(
                    Util.RAC_VER);

            if ((oracle_version != null) &&
                    (oracle_version.trim().length() != 0) &&
                    !oracle_version.equals(selectedOraVersion)) {
                TTYDisplay.printError(error_txt);
                TTYDisplay.clear(2);
                TTYDisplay.promptForEntry(continue_txt);
                TTYDisplay.clear(2);
                choice = no;

                continue;
            }

            // validate the ORACLE HOME
            ErrorValue retVal = validateInput(selectedOracleHome, baseClusterNodelist,
		selZoneCluster);

            if (!retVal.getReturnValue().booleanValue()) {
                TTYDisplay.printInfo(retVal.getErrorString());

                String enterAgain = TTYDisplay.getConfirmation(enteragain_txt,
                        yes, no);

                if (enterAgain.equalsIgnoreCase(yes)) {
                    choice = no;

                    continue;
                }
            }

            choice = yes;

            // Store the Values in the WizardModel
            wizardModel.selectCurrWizardContext();
            wizardModel.setWizardValue(Util.SEL_ORACLE_HOME,
                selectedOracleHome);
	    wizardModel.setWizardValue(Util.RAC10G_CURNODENAME, null);
        }
    }

    private ErrorValue validateInput(String oraHome, String baseClusterNodelist[],
	String selZoneCluster) {

	RACModel racModel = RACInvokerCreator.getRACModel();

        // validate oracle home on all nodes of a cluster
        String propName[] = { Util.ORACLE_HOME };
        HashMap userInput = new HashMap();
        userInput.put(Util.ORACLE_HOME, oraHome);

        ErrorValue err = new ErrorValue();
        err.setReturnVal(Boolean.TRUE);

        StringBuffer errStr = new StringBuffer();
        int err_count = 0;
	HashMap helperData = new HashMap();
	helperData.put(Util.SEL_ZONE_CLUSTER, selZoneCluster);

        for (int i = 0; i < baseClusterNodelist.length; i++) {
	    String[] splitStr = baseClusterNodelist[i].split(Util.COLON);
            ErrorValue errVal = racModel.validateInput(null, splitStr[0], propName, userInput,
		helperData);

            if (!errVal.getReturnValue().booleanValue()) {
                // error
                err_count++;
                errStr = new StringBuffer(errVal.getErrorString());
            }
        }

        if (err_count == baseClusterNodelist.length) {

            // failed on all nodes
            err.setReturnVal(Boolean.FALSE);
            err.setErrorString(errStr.toString());
        }

        return err;
    }
}
