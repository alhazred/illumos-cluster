/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)SharedAddressReviewPanel.java 1.13     08/12/10 SMI"
 */


package com.sun.cluster.dswizards.sharedaddress;


// CMASS
import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.sharedaddress.SharedAddressMBean;
import com.sun.cluster.agent.dataservices.utils.Util;

// CLI Wizard SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;

// Wizard Common
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;

// J2SE
import java.util.HashMap;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// JMX
import javax.management.remote.JMXConnector;


/**
 * This is the Review Panel Class for the SharedAddress Wizard
 */
public class SharedAddressReviewPanel extends WizardLeaf {

    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N messages; // Message Bundle
    private JMXConnector localConnection; // Connector

    /**
     * Creates a new instance of SharedAddressReviewPanel
     */
    public SharedAddressReviewPanel() {
    }

    /**
     * Creates a SharedAddressReviewPanel with the given name and manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public SharedAddressReviewPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates a SharedAddressReviewPanel with the given name and Wizard state
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     * @param  messages  WizardI18N
     */
    public SharedAddressReviewPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N messages) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;

        // Get the I18N instance
        this.messages = messages;
        localConnection = getConnection(Util.getClusterEndpoint());
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }


    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.
     *		WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {

        // Description Para
        String para = messages.getString("lhwizard.wizard.step5.instruction");
        String heading1 = messages.getString(
                "hasp.create.filesystem.review.heading1");
        String heading2 = messages.getString(
                "hasp.create.filesystem.review.heading2");
        String resourceTitle = messages.getString(
                "lhwizard.reviewPanel.resourceTitle");
        String resourceGroupTitle = messages.getString(
                "lhwizard.reviewPanel.resourceGroupTitle");
        String dchar = messages.getString("ttydisplay.d.char");
        String backChar = messages.getString("ttydisplay.back.char");
        String done = messages.getString("ttydisplay.menu.done");
        String title = messages.getString("lhwizard.wizard.step5.title");
        String continueText = messages.getString("cliwizards.continue");
        String help = messages.getString("shwizard.wizard.step5.help.1") +
            "\n \n" + messages.getString("shwizard.wizard.step5.help.2");

        // Review Panel Table
        Vector reviewPanel = new Vector();
        boolean doneFlag = false;

        do {

            TTYDisplay.clearScreen();
            TTYDisplay.clear(2);
            TTYDisplay.printTitle(title);
            TTYDisplay.clear(1);

            TTYDisplay.setNavEnabled(true);
            this.cancelDirection = false;

            // Pattern for checking Resource and ResourceGroup Name
            Pattern p = Pattern.compile("\\s*");
            Matcher m;

            reviewPanel.removeAllElements();
            wizardModel.selectCurrWizardContext();

            // First Row - Resource Name
            String resourceName = (String) wizardModel.getWizardValue(
                    Util.RESOURCENAME);
            reviewPanel.add(resourceTitle);
            reviewPanel.add(resourceName);

            // Second Row - Resource Group Name
            String resourceGroupName = (String) wizardModel.getWizardValue(
                    Util.RESOURCEGROUPNAME);
            reviewPanel.add(resourceGroupTitle);
            reviewPanel.add(resourceGroupName);

            TTYDisplay.pageText(para, 4);
            TTYDisplay.clear(1);

            HashMap customTags = new HashMap();
            customTags.put(dchar, done);

            String subheadings[] = { heading1, heading2 };

            TTYDisplay.clear(1);

            // Display the Table
            String option = TTYDisplay.create2DTable("", subheadings,
                    reviewPanel, customTags, help);

            if (option.equals(backChar)) {
                this.cancelDirection = true;

                return;
            }

            TTYDisplay.clear(3);
            TTYDisplay.setNavEnabled(true);


            if (option.equals("1")) { // Resource Information

                String desc = messages.getString(
                        "shwizard.reviewPanel.resourceInfo");
                TTYDisplay.pageText(messages.getString("cliwizards.desc") +
                    desc, 6);
                TTYDisplay.clear(1);
                TTYDisplay.pageText(messages.getString(
                        "lhwizard.reviewPanel.resourceTitle") + resourceName,
                    6);
                TTYDisplay.clear(1);

                // Get the new Resource Name
                TTYDisplay.clear(1);

                boolean rNameOk = true;
                String rName = TTYDisplay.promptForEntry(messages.getString(
                            "lhwizard.reviewPanel.newResourceMsg"));

                if (rName.equals("<")) {
                    continue;
                }

                // Check whether String has white space characters
                m = p.matcher(rName);

                if (m.matches()) {
                    rNameOk = false;
                    TTYDisplay.printError(messages.getString(
                            "lhwizard.reviewPanel.invalidResName"));
                    TTYDisplay.promptForEntry(continueText);

                    continue;
                }

                // Validate if the name is already in use
                String propertyName[] = { Util.RESOURCENAME };
                HashMap userMap = new HashMap();
                userMap.put(Util.RESOURCENAME, rName);

                ErrorValue returnVal = validate(propertyName, userMap, null);

                if (!returnVal.getReturnValue().booleanValue()) {
                    rNameOk = false;
                    TTYDisplay.printError(messages.getString(
                            "lhwizard.reviewPanel.resourceNameErr"));
                    TTYDisplay.promptForEntry(continueText);

                    continue;
                }

                // Store the correct name
                if (rNameOk) {
                    wizardModel.setWizardValue(Util.RESOURCENAME, rName);
                }

                continue;
            } else if (option.equals("2")) { // Resource Group Information

                TTYDisplay.clear(1);

                String desc = messages.getString(
                        "shwizard.reviewPanel.resourceGroupInfo");
                TTYDisplay.pageText(messages.getString("cliwizards.desc") +
                    desc, 6);
                TTYDisplay.clear(1);
                TTYDisplay.pageText(messages.getString(
                        "lhwizard.reviewPanel.resourceGroupTitle") +
                    resourceGroupName, 6);
                TTYDisplay.clear(2);

                // Get the new ResourceGroup Name
                String rgName = TTYDisplay.promptForEntry(messages.getString(
                            "lhwizard.reviewPanel.resourceGroupTitle"));
                boolean rgNameOk = true;

                if (rgName.equals("<")) {
                    break;
                }

                // Check whether String has white space characters
                m = p.matcher(rgName);

                if (m.matches()) {
                    rgNameOk = false;
                    TTYDisplay.printError(messages.getString(
                            "lhwizard.reviewPanel.invalidRGName"));
                    TTYDisplay.promptForEntry(continueText);

                    continue;
                }

                // Validate whether the name is already used
                String rgpropertyName[] = { Util.RESOURCEGROUPNAME };
                HashMap userMap = new HashMap();
                userMap.put(Util.RESOURCEGROUPNAME, rgName);

                ErrorValue returnVal = validate(rgpropertyName, userMap, null);

                if (!returnVal.getReturnValue().booleanValue()) {
                    rgNameOk = false;
                    TTYDisplay.printError(messages.getString(
                            "lhwizard.reviewPanel.resourceGroupNameErr"));
                    TTYDisplay.promptForEntry(continueText);

                    continue;
                }

                // Store the valid rgName
                if (rgNameOk) {
                    wizardModel.setWizardValue(Util.RESOURCEGROUPNAME, rgName);
                }

                continue;
            } else if (option.equals(dchar)) {

                // Quit the panel
                doneFlag = true;
            }
        } while (!doneFlag);
    }

    /**
     * All Validations Done in this Panel
     *
     * @return  ErrorValue
     */
    private ErrorValue validate(String property[], HashMap data,
        Object helper) {
        ErrorValue errValue = null;
        SharedAddressMBean mBean = SharedAddressWizardCreator
            .getSharedAddressMBean(localConnection);
        errValue = mBean.validateInput(property, data, helper);

        return errValue;
    }
}
