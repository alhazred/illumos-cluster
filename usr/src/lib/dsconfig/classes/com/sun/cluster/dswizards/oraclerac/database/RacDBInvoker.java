/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)RacDBInvoker.java 1.7     08/05/20 SMI"
 */

package com.sun.cluster.dswizards.oraclerac.database;

// Java

// CMASS
import com.sun.cacao.agent.JmxClient;

import com.sun.cluster.agent.dataservices.oraclerac.OracleRACMBean;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.common.TrustedMBeanModel;
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardComposite;
import com.sun.cluster.dswizards.clisdk.core.WizardCreator;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.logicalhost.HostnameEntryPanel;
import com.sun.cluster.dswizards.logicalhost.IPMPGroupEntryPanel;
import com.sun.cluster.dswizards.logicalhost.LogicalHostWizardConstants;
import com.sun.cluster.dswizards.oraclerac.database.RacDBIntroPanel;
import com.sun.cluster.dswizards.oraclerac.database.RacDBStorageSelectionPanel;
import com.sun.cluster.dswizards.oraclerac.database.RacDBVersionPanel;
import com.sun.cluster.dswizards.oraclerac.database.RacDBWizardConstants;
import com.sun.cluster.dswizards.oraclerac.database.ver10g.
    Rac10GCrsHomeSelectionPanel;
import com.sun.cluster.dswizards.oraclerac.database.ver10g.
    Rac10GCrsStorageChoicePanel;
import com.sun.cluster.dswizards.oraclerac.database.ver10g.
    Rac10GCrsStorageSelectionPanel;
import com.sun.cluster.dswizards.oraclerac.database.ver10g.
    Rac10GDbNameSelectionPanel;
import com.sun.cluster.dswizards.oraclerac.database.ver10g.
    Rac10GOracleHomeSelectionPanel;
import com.sun.cluster.dswizards.oraclerac.database.ver10g.
    Rac10GOracleSidSelectionPanel;
import com.sun.cluster.dswizards.oraclerac.database.ver10g.Rac10GResultsPanel;
import com.sun.cluster.dswizards.oraclerac.database.ver10g.Rac10GReviewPanel;
import com.sun.cluster.dswizards.oraclerac.database.ver10g.Rac10GSummaryPanel;
import com.sun.cluster.dswizards.oraclerac.database.ver9i.
    RacDBHomeSelectionPanel;
import com.sun.cluster.dswizards.oraclerac.database.ver9i.
    RacDBLogicalHostSelectionPanel;
import com.sun.cluster.dswizards.oraclerac.database.ver9i.RacDBPreferencePanel;
import com.sun.cluster.dswizards.oraclerac.database.ver9i.RacDBPropsReviewPanel;
import com.sun.cluster.dswizards.oraclerac.database.ver9i.RacDBResultPanel;
import com.sun.cluster.dswizards.oraclerac.database.ver9i.RacDBReviewPanel;
import com.sun.cluster.dswizards.oraclerac.database.ver9i.
    RacDBSidSelectionPanel;
import com.sun.cluster.dswizards.oraclerac.database.ver9i.RacDBSummaryPanel;
import com.sun.cluster.dswizards.oraclerac.framework.ORFWizardConstants;
import com.sun.cluster.dswizards.oraclerac.invoker.RACInvokerConstants;
import com.sun.cluster.dswizards.oraclerac.storage.ORSWizardConstants;

import java.io.IOException;

import java.text.MessageFormat;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.management.remote.JMXConnector;


/**
 * This class holds the Wizard Panel Tree information for all the three
 * RACWizards The Wizard lets the user invoke the three RAC Wizards
 */
public class RacDBInvoker extends WizardCreator
    implements RacDBWizardConstants {

    private static final String FLOW_XML = RAC_DB_FLOW_XML;
    private static final String STATE_FILE = RACInvokerConstants.RAC_STATE_FILE;

    /**
     * Default Constructor Creates ORS Wizard from the State File. The Wizard is
     * intialized to a particular state using the State file. The Wizard flow is
     * governed by the Flow XML.
     */
    public RacDBInvoker() {
        super(STATE_FILE, FLOW_XML);
    }

    /**
     * Creates the actual Panel Tree Structure for the ORS Wizard
     */
    protected void createClientTree() {

        // Get the wizard root
        WizardComposite wizardRoot = getRoot();

        // RAC Database Panels
        RacDBIntroPanel racDBIntroPanel = new RacDBIntroPanel(PANEL_0,
                wizardModel, wizardFlow, wizardi18n);

        RacDBVersionPanel racDBVersionPanel = new RacDBVersionPanel(PANEL_1,
                wizardModel, wizardFlow, wizardi18n);

        RacDBPreferencePanel racDBPreferencePanel = new RacDBPreferencePanel(
                PANEL_2, wizardModel, wizardFlow, wizardi18n);

        RacDBHomeSelectionPanel racDBHomeSelectionPanel =
            new RacDBHomeSelectionPanel(PANEL_3, wizardModel, wizardFlow,
                wizardi18n);

        RacDBSidSelectionPanel racDBSidSelectionPanel =
            new RacDBSidSelectionPanel(PANEL_4, wizardModel, wizardFlow,
                wizardi18n);

        RacDBPropsReviewPanel racDBPropsReviewPanel = new RacDBPropsReviewPanel(
                PANEL_5, wizardModel, wizardFlow, wizardi18n);

        RacDBStorageSelectionPanel racDBStorageSelectionPanel =
            new RacDBStorageSelectionPanel(STORAGE_PANEL, wizardModel,
                wizardFlow, wizardi18n);

        RacDBLogicalHostSelectionPanel racDBLogicalHostSelectionPanel =
            new RacDBLogicalHostSelectionPanel(PANEL_7, wizardModel, wizardFlow,
                wizardi18n);

        HostnameEntryPanel hostnameEntryPanel = new HostnameEntryPanel(PANEL_7 +
                WizardFlow.HYPHEN + LogicalHostWizardConstants.HOSTNAMEENTRY,
                wizardModel, wizardFlow, wizardi18n);

        IPMPGroupEntryPanel ipmpGroupEntryPanel = new IPMPGroupEntryPanel(
                PANEL_7 + WizardFlow.HYPHEN +
                LogicalHostWizardConstants.IPMPGROUPENTRY, wizardModel,
                wizardFlow, wizardi18n);

        RacDBReviewPanel racDBReviewPanel = new RacDBReviewPanel(PANEL_8,
                wizardModel, wizardFlow, wizardi18n);

        RacDBSummaryPanel racDBSummaryPanel = new RacDBSummaryPanel(PANEL_9,
                wizardModel, wizardFlow, wizardi18n);

        RacDBResultPanel racDBResultPanel = new RacDBResultPanel(RESULTS_PANEL,
                wizardModel, wizardFlow, wizardi18n);

        Rac10GCrsHomeSelectionPanel rac10GCrsHomeSelectionPanel =
            new Rac10GCrsHomeSelectionPanel(VER10G_PANEL_0, wizardModel,
                wizardFlow, wizardi18n);

        Rac10GCrsStorageChoicePanel rac10GCrsStorageChoicePanel =
            new Rac10GCrsStorageChoicePanel(VER10G_PANEL_0_1, wizardModel,
                wizardFlow, wizardi18n);

        Rac10GCrsStorageSelectionPanel rac10GCrsStorageSelectionPanel =
            new Rac10GCrsStorageSelectionPanel(VER10G_PANEL_0_2, wizardModel,
                wizardFlow, wizardi18n);

        Rac10GDbNameSelectionPanel rac10GDbNameSelectionPanel =
            new Rac10GDbNameSelectionPanel(VER10G_PANEL_1, wizardModel,
                wizardFlow, wizardi18n);

        Rac10GOracleHomeSelectionPanel rac10GOracleHomeSelectionPanel =
            new Rac10GOracleHomeSelectionPanel(VER10G_PANEL_2, wizardModel,
                wizardFlow, wizardi18n);

        Rac10GOracleSidSelectionPanel rac10GOracleSidSelectionPanel =
            new Rac10GOracleSidSelectionPanel(VER10G_PANEL_3, wizardModel,
                wizardFlow, wizardi18n);

        RacDBStorageSelectionPanel rac10GStorageSelectionPanel =
            new RacDBStorageSelectionPanel(VER10G_PANEL_4, wizardModel,
                wizardFlow, wizardi18n);

        Rac10GReviewPanel rac10GReviewPanel = new Rac10GReviewPanel(
                VER10G_PANEL_5, wizardModel, wizardFlow, wizardi18n);

        Rac10GSummaryPanel rac10GSummaryPanel = new Rac10GSummaryPanel(
                VER10G_PANEL_6, wizardModel, wizardFlow, wizardi18n);

        Rac10GResultsPanel rac10GResultsPanel = new Rac10GResultsPanel(
                VER10G_PANEL_7, wizardModel, wizardFlow, wizardi18n);


        // Each Panel is associated with a Name, the wizardModel from
        // the WizardCreator and the WizardFlow Structure.
        // Add the Panels to the Root
        // Database Panels
        wizardRoot.addChild(racDBIntroPanel);
        wizardRoot.addChild(racDBVersionPanel);
        wizardRoot.addChild(racDBPreferencePanel);
        wizardRoot.addChild(racDBHomeSelectionPanel);
        wizardRoot.addChild(racDBSidSelectionPanel);
        wizardRoot.addChild(racDBPropsReviewPanel);
        wizardRoot.addChild(racDBStorageSelectionPanel);
        wizardRoot.addChild(racDBLogicalHostSelectionPanel);
        wizardRoot.addChild(hostnameEntryPanel);
        wizardRoot.addChild(ipmpGroupEntryPanel);
        wizardRoot.addChild(racDBReviewPanel);
        wizardRoot.addChild(racDBSummaryPanel);
        wizardRoot.addChild(racDBResultPanel);

        wizardRoot.addChild(rac10GCrsHomeSelectionPanel);
        wizardRoot.addChild(rac10GCrsStorageChoicePanel);
        wizardRoot.addChild(rac10GCrsStorageSelectionPanel);
        wizardRoot.addChild(rac10GDbNameSelectionPanel);
        wizardRoot.addChild(rac10GOracleHomeSelectionPanel);
        wizardRoot.addChild(rac10GOracleSidSelectionPanel);
        wizardRoot.addChild(rac10GStorageSelectionPanel);
        wizardRoot.addChild(rac10GReviewPanel);
        wizardRoot.addChild(rac10GSummaryPanel);
        wizardRoot.addChild(rac10GResultsPanel);
    }

    /**
     * Get the Handle to the RACBean MBean on a specified node
     *
     * @return  Handle to the RACBean MBean on a specified node
     */
    public static OracleRACMBean getOracleRACMBeanOnNode(
        JMXConnector connector) {

        // Only if there is no existing reference
        // Get Handler to OracleRACMBean
        OracleRACMBean mbeanOnNode = null;

        try {
            TTYDisplay.initialize();
            mbeanOnNode = (OracleRACMBean) TrustedMBeanModel.getMBeanProxy(
                    Util.DOMAIN, connector, OracleRACMBean.class, null, false);
        } catch (Exception e) {

            // Report Error that the user has to install
            // The Dataservice Package or upgrade it
            String errorMsg = pkgErrorString;
            MessageFormat msgFmt = new MessageFormat(errorMsg);
            errorMsg = msgFmt.format(
                    new String[] { RACInvokerConstants.RAC_PKG_NAME });
            TTYDisplay.printError(errorMsg);
            TTYDisplay.promptForEntry(wizardExitPrompt);
            System.exit(1);
        }

        return mbeanOnNode;
    }

    /**
     * Main function of the Wizards
     */
    public static void main(String args[]) {

        // Start the ORS Wizard
        RacDBInvoker RACInovker = new RacDBInvoker();
    }
}
