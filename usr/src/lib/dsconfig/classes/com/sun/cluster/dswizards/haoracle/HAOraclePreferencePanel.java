/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)HAOraclePreferencePanel.java 1.8     08/12/10 SMI"
 */


package com.sun.cluster.dswizards.haoracle;


import com.sun.cluster.agent.dataservices.haoracle.HAOracleMBean;
import com.sun.cluster.agent.dataservices.utils.*;
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.management.remote.JMXConnector;


/**
 * The Panel Class that gets a Hostname or list of Hostnames as input from the
 * User.
 */
public class HAOraclePreferencePanel extends WizardLeaf {


    private String panelName; // Panel Name
    private WizardI18N wizardi18n; // Message Bundle
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private HAOracleMBean mbean = null;


    /**
     * Creates a new instance of HAOraclePreferencePanel
     */
    public HAOraclePreferencePanel() {
        super();
    }

    /**
     * Creates a HostnameEntry Panel with the given name and manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     * @param  flow  Handler to the WizardFlow
     */
    public HAOraclePreferencePanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates a HostnameEntry Panel with the given name and Wizard state
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     */
    public HAOraclePreferencePanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.wizardModel = model;
        this.panelName = name;
        this.wizardi18n = wizardi18n;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.
     *		WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        String title = wizardi18n.getString("haoracle.preferencePanel.title");
        String preference = wizardi18n.getString(
                "haoracle.preferencePanel.preference");
        String option1 = wizardi18n.getString(
                "haoracle.preferencePanel.option1");
        String option2 = wizardi18n.getString(
                "haoracle.preferencePanel.option2");
        String option3 = wizardi18n.getString(
                "haoracle.preferencePanel.option3");

        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        // Title
        TTYDisplay.printTitle(title);

        String options[] = { option1, option2, option3 };

        // Get the data preference
        int index = TTYDisplay.getMenuOption(preference, options, 2);

        if (index == TTYDisplay.BACK_PRESSED) {
            this.cancelDirection = true;

            return;
        }

        String pref = null;
        String selection = options[index];
        Thread t = null;

        JMXConnector localConnection = getConnection(Util.getClusterEndpoint());
        String nodeList[] = (String[]) wizardModel.getWizardValue(
                Util.RG_NODELIST);
        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);
        JMXConnector connector = getConnection(nodeEndPoint);
        mbean = HAOracleWizardCreator.getHAOracleMBeanOnNode(connector);

        HashMap discoveredMap = null;
        String propertyNames[] = null;

        if (selection.equals(option1)) {
            pref = new String(HAOracleWizardConstants.SERVER_ONLY);

            // Discover Server Properties
            // Get the Discoverable Properties For HAOracle Server RT
            propertyNames = new String[] { Util.ORA_SERVER_RTNAME };
            TTYDisplay.clear(1);
            t = TTYDisplay.busy(wizardi18n.getString(
                        "haoracle.preferencePanel.option1.busy"));
        } else if (selection.equals(option2)) {
            pref = new String(HAOracleWizardConstants.LISTENER_ONLY);

            // Discover Listener Properties
            // Get the Discoverable Properties For HAOracle Listener RT
            propertyNames = new String[] { Util.ORA_LISTENER_RTNAME };
            TTYDisplay.clear(1);
            t = TTYDisplay.busy(wizardi18n.getString(
                        "haoracle.preferencePanel.option2.busy"));
        } else if (selection.equals(option3)) {
            pref = new String(HAOracleWizardConstants.SERVER_AND_LISTENER);

            // Get the Discoverable Properties For HAOracle Server and Listener
            propertyNames = new String[] { Util.PROPERTY_NAMES };
            TTYDisplay.clear(1);
            t = TTYDisplay.busy(wizardi18n.getString(
                        "haoracle.preferencePanel.option3.busy"));
        }

        // Start the busy thread to show the busy symbol
        t.start();

        if (mbean != null) {
            discoveredMap = mbean.discoverPossibilities(propertyNames, null);
        }

        // discovery done, interrupt the thread
        try {
            t.interrupt();
            t.join();
        } catch (Exception e) {
        }

        // Copy the Data to the WizardModel
        propertyNames = new String[discoveredMap.size()];

        Set entrySet = discoveredMap.entrySet();
        Iterator setIterator = entrySet.iterator();
        int i = 0;

        while (setIterator.hasNext()) {
            Map.Entry entry = (Map.Entry) setIterator.next();
            String key = (String) entry.getKey();
            wizardModel.setWizardValue(key, entry.getValue());
            propertyNames[i++] = key;
        }

        wizardModel.setWizardValue(Util.PROPERTY_NAMES, propertyNames);

        wizardModel.selectCurrWizardContext();
        wizardModel.setWizardValue(HAOracleWizardConstants.HAORACLE_PREF, pref);
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#isDisplayComplete
     */
    public boolean isDisplayComplete() {
        return true;
    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }
}
