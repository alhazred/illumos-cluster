/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)PreferredNodePanel.java	1.22	08/12/10 SMI"
 */

package com.sun.cluster.dswizards.logicalhost;

// J2SE
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.management.remote.JMXConnector;

import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.dataservices.logicalhost.LogicalHostMBean;
import com.sun.cluster.agent.dataservices.sharedaddress.SharedAddressMBean;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.NodeSelectionPanel;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.sharedaddress.SharedAddressWizardCreator;


/**
 * This panel gets the Preferred Node from the User
 */
public class PreferredNodePanel extends NodeSelectionPanel {

    private String wizardType; // The Type of the Wizard
    private String logicalhost;
    private String sharedaddress;

    /**
     * Creates a new instance of PreferredNodePanel
     */
    public PreferredNodePanel() {
    }

    /**
     * Creates an PreferredNode Panel with the given name and manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public PreferredNodePanel(String name, WizardTreeManager manager) {
        super(name, manager);
    }

    /**
     * Creates an PreferredNode Panel for the wizard with the given name and
     * associates it with the WizardModel and WizardFlow.
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     * @param  wizardi18n  WizardI18n
     */
    public PreferredNodePanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(name, model, wizardFlow, wizardi18n);
        this.wizardType = LogicalHostWizardCreator.WIZARDTYPE;
        initStrings();
    }

    private void initStrings() {
        desc = wizardi18n.getString(wizardType + ".wizard.step3.help.1");
        logicalhost = wizardi18n.getString("dswizards.logicalhost.wizard");
        sharedaddress = wizardi18n.getString("dswizards.sharedaddress.wizard");
    }


    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {

        // If this panel is called from another wizard get the wizard type
        // from the model
        Object type = wizardModel.getWizardValue(
                LogicalHostWizardConstants.WIZARDTYPE);

        if (type != null) {
            wizardType = (String) type;
        }

        initStrings();

	do {

	    if (wizardType.equals(LogicalHostWizardCreator.WIZARDTYPE)) {
		String nodeList[] = displayPanel(LogicalHostMBean.class,
			logicalhost);

		if (nodeList == null) {
		    this.cancelDirection = true;
		    return;
		}

		wizardModel.setWizardValue(Util.RG_NODELIST, nodeList);
		initWizardModelForLH();
	    } else {
		String nodeList[] = displayPanel(SharedAddressMBean.class,
			sharedaddress);

		if (nodeList == null) {
		    this.cancelDirection = true;

		    return;
		}

		wizardModel.setWizardValue(Util.RG_NODELIST, nodeList);
		initWizardModelForSA();
	    }

	    return;
	} while (true);
    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }

    /**
     * Initializes the Wizard Model by filling with the PropertyNames and
     * Discovered values. The Previous Values entered by the user are retrieved
     * and stored in the previous context.
     */
    public void initWizardModelForLH() {

        // Contact the LogicalHostMBean and Get All Properties
        JMXConnector localConnection = getConnection(Util.getClusterEndpoint());

        // Set the value for static variable lhMBean
        LogicalHostMBean lhMBean = LogicalHostWizardCreator.getLogicalHostMBean(
                localConnection);

        if (lhMBean != null) {

            // Get the Discoverable Properties For LogicalHost Wizard
            String propertyNames[] = { Util.PROPERTY_NAMES };
	    Map helperMap = new HashMap();
	    Boolean b = (Boolean)wizardModel.getWizardValue(Util.EXCLUSIVEIP);
	    helperMap.put(Util.EXCLUSIVEIP, b);
            HashMap discoveredMap = lhMBean.discoverPossibilities(propertyNames,
                    helperMap);

            // Copy the Data to the WizardModel
            propertyNames = new String[discoveredMap.size()];

            Set entrySet = discoveredMap.entrySet();
            Iterator setIterator = entrySet.iterator();
            int i = 0;

            while (setIterator.hasNext()) {
                Map.Entry entry = (Map.Entry) setIterator.next();
                wizardModel.setWizardValue((String) entry.getKey(),
                    entry.getValue());
                propertyNames[i++] = (String) entry.getKey();
            }

            wizardModel.setWizardValue(Util.PROPERTY_NAMES, propertyNames);
        }
    }

    /**
     * Initializes the Wizard Model by filling with the PropertyNames and
     * Discovered values. The Previous Values entered by the user are retrieved
     * and stored in the previous context.
     */
    public void initWizardModelForSA() {

        // Set wizardType for the common panels
        wizardModel.setWizardValue(LogicalHostWizardConstants.WIZARDTYPE,
            SharedAddressWizardCreator.WIZARDTYPE);

        JMXConnector localConnection = getConnection(Util.getClusterEndpoint());

        // Set the value for static variable lhMBean
        SharedAddressMBean shMBean = SharedAddressWizardCreator
            .getSharedAddressMBean(localConnection);

        if (shMBean != null) {

            // Get the Discoverable Properties For SharedAddress Wizard
            String propertyNames[] = { Util.PROPERTY_NAMES };
            HashMap discoveredMap = shMBean.discoverPossibilities(propertyNames,
                    null);

            // Copy the Data to the WizardModel
            propertyNames = new String[discoveredMap.size()];

            Set entrySet = discoveredMap.entrySet();
            Iterator setIterator = entrySet.iterator();
            int i = 0;

            while (setIterator.hasNext()) {
                Map.Entry entry = (Map.Entry) setIterator.next();
                wizardModel.setWizardDefaultValue((String) entry.getKey(),
                    entry.getValue());
                propertyNames[i++] = (String) entry.getKey();
            }

            wizardModel.setWizardValue(Util.PROPERTY_NAMES, propertyNames);
        }
    }

}
