/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)HASPFlowManager.java 1.12     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.hastorageplus;

import com.sun.cluster.dswizards.common.DSConfigWizardModel;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.DSWizardInterface;
import com.sun.cluster.dswizards.common.WizardI18N;

import java.util.ArrayList;
import java.util.List;


/**
 * The class takes decision on which panel to show "next" depending upon the
 * conditions set at a particular context.
 */
public class HASPFlowManager implements DSWizardInterface {

    /**
     * Creates a new instance of HASPFlowManager
     */
    public HASPFlowManager() {
    }

    /**
     * @see  com.sun.cluster.dswizards.common.DSWizardInterface#getNextPanel
     */
    public String getNextPanel(String curPanelName,
        Object wizCxtObj, String options[]) {
	CliDSConfigWizardModel cliModel = null;
	DSConfigWizardModel guiModel = null;

	if (wizCxtObj instanceof CliDSConfigWizardModel) {
	    cliModel = (CliDSConfigWizardModel)wizCxtObj;
	} else if (wizCxtObj instanceof DSConfigWizardModel) {
	    guiModel = (DSConfigWizardModel)wizCxtObj;
	}

        if (curPanelName.equals(HASPWizardConstants.PANEL_1)) {

            // we are in the nodeselection panel. Need to check if location
            // panel should be skipped.
            String zonesPresent = null;
	    if (guiModel != null) {
		zonesPresent = (String)guiModel.getWizardValue(
		    HASPWizardConstants.ZONES_PRESENT);
	    } else {
		zonesPresent = (String)cliModel.getWizardValue(
		    HASPWizardConstants.ZONES_PRESENT);
	    }

            if (zonesPresent.equals(HASPWizardConstants.NO)) {

                // zones are not present. Display location panel
                return options[0];
            } else {

                // skip the location panel;move to filesystem selection panel
                return options[1];
            }
        } else if (curPanelName.equals(HASPWizardConstants.PANEL_2)) {

            // Check if filesystem selection panel needs to be displayed
            String haspLocationPref = null;
	    if (guiModel != null) {
		haspLocationPref = (String)guiModel.getWizardValue(
		    HASPWizardConstants.HASP_LOCATION_PREF);
	    } else {
		haspLocationPref = (String)cliModel.getWizardValue(
		    HASPWizardConstants.HASP_LOCATION_PREF);
	    }

            if (haspLocationPref.equals(HASPWizardConstants.FS_ONLY) ||
		haspLocationPref.equals(HASPWizardConstants.FS_AND_DG)) {

                // display the filesystem selection panel
                return options[0];
            } else {
                return options[1];
            }
        } else if (curPanelName.equals(HASPWizardConstants.PANEL_3)) {
            String create_new = null;
	    if (guiModel != null) {
		create_new = (String)guiModel.getWizardValue(
		    HASPWizardConstants.HASP_CREATE_FILESYSTEM);
	    } else {
		create_new = (String)cliModel.getWizardValue(
		    HASPWizardConstants.HASP_CREATE_FILESYSTEM);
	    }

            if (create_new.equalsIgnoreCase(
                        HASPWizardConstants.SELECT_EXISTING)) {
                String haspLocationPref = null;
                Object sqfsSelected = null;
		if (guiModel != null) {
		    haspLocationPref = (String)guiModel.getWizardValue(
			HASPWizardConstants.HASP_LOCATION_PREF);

		    sqfsSelected = guiModel.getWizardValue(
			HASPWizardConstants.SQFS_SELECTED);
		} else {
		    haspLocationPref = (String)cliModel.getWizardValue(
			HASPWizardConstants.HASP_LOCATION_PREF);

		    sqfsSelected = cliModel.getWizardValue(
			HASPWizardConstants.SQFS_SELECTED);
		}
                if ((haspLocationPref.equals(HASPWizardConstants.DG_ONLY) ||
                        haspLocationPref.equals(HASPWizardConstants.FS_AND_DG)) 
                        && sqfsSelected == null) {
                    return options[1];
                } else {
                    return options[2];
                }
            } else {
                return options[0];
            }
        }

        return null;
    }
}
