/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ApacheWizardSummaryPanel.java 1.25     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.apache;

// CMASS
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.dataservices.apache.Apache;
import com.sun.cluster.agent.dataservices.apache.ApacheMBean;
import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.utils.FileAccessorWrapper;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.rgm.property.ResourceProperty;
import com.sun.cluster.agent.rgm.property.ResourcePropertyBoolean;
import com.sun.cluster.agent.rgm.property.ResourcePropertyString;
import com.sun.cluster.agent.rgm.property.ResourcePropertyStringArray;

// CLI SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;

// Wizard Common
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.hastorageplus.HASPWizardConstants;
import com.sun.cluster.dswizards.logicalhost.LogicalHostWizardConstants;

// J2SE
import java.io.IOException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.management.MBeanServerConnection;
import javax.management.remote.JMXConnector;


/**
 * Apache Configuration Summary
 */
public class ApacheWizardSummaryPanel extends WizardLeaf {

    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N messages; // Message Bundle
    private ApacheMBean mBean = null; // Reference to MBean
    private JMXConnector localConnection = null; // Reference to JMXConnector

    /**
     * Creates a new instance of ApacheWizardSummaryPanel
     */
    public ApacheWizardSummaryPanel() {
        super();
    }

    /**
     * Creates an ApacheWizardSummary Panel with the given name and tree manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public ApacheWizardSummaryPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates a ApacheWizardSummary Panel with the given name and associates it
     * with the WizardModel, WizardFlow and i18n reference
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     * @param  messages  WizardI18N
     */
    public ApacheWizardSummaryPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N messages) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;

        // Get the I18N instance
        this.messages = messages;

        localConnection = getConnection(Util.getClusterEndpoint());
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {

        String cchar = messages.getString("ttydisplay.c.char");
        String qchar = messages.getString("ttydisplay.q.char");
        String done = messages.getString("ttydisplay.menu.done");
        String quit = messages.getString("ttydisplay.menu.quit");
        String createConfigurationText = messages.getString(
                "ttydisplay.menu.createConfiguration");
        String apacheResourceGroup = messages.getString(
                "apache.reviewPanel.apacheResourceGroup");
        String apacheResource = messages.getString(
                "apache.reviewPanel.apacheResource");
        String lhResource = messages.getString(
                "apache.reviewPanel.networkResource");
        String shResource = lhResource;
        String storageResource = messages.getString(
                "apache.reviewPanel.storageResource");
        String shGroup = messages.getString("apache.reviewPanel.shGroup");
        String heading1 = messages.getString(
                "hasp.create.filesystem.review.heading1");
        String heading2 = messages.getString(
                "hasp.create.filesystem.review.heading2");
        String apacheRGDesc = messages.getString(
                "apache.reviewPanel.apacheResourceGroup.desc");
        String apacheResourceDesc = messages.getString(
                "apache.reviewPanel.apacheResource.desc");
        String lhresourceDesc = messages.getString(
                "apache.reviewPanel.lhResource.desc");
        String shresourceDesc = messages.getString(
                "apache.reviewPanel.shResource.desc");
        String storageResourceDesc = messages.getString(
                "apache.reviewPanel.storageResource.desc");
        String shRGDesc = messages.getString("apache.reviewPanel.shGroup.desc");
        String title = messages.getString("apache.summary.title");
        String instruction = messages.getString("lhwizard.wizard.step6.info");
        String help = messages.getString("apache.summary.clihelp");
        String cliDesc = messages.getString("cliwizards.desc");
        String continueText = messages.getString("cliwizards.continue");
        String scTableTitle = messages.getString("hasp.summarypanel.desc2");

        String nodeList = messages.getString("apache.summary.nodeList");
        String nodeListDesc = messages.getString("apache.summary.nodeListDesc");
        String moreSymbol = messages.getString("ttydisplay.fwd.arrow");
        String mode = messages.getString("apache.summary.mode");

        String binDir = messages.getString("apache.summary.binDir");
        String binDirDesc = messages.getString("apache.summary.binDirDesc");
        String controlFileError = messages.getString(
                "apache.summary.controlFileError");

        String nodeListArr[] = (String[]) wizardModel.getWizardValue(
                Util.RG_NODELIST);

        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);
        JMXConnector connector = getConnection(nodeEndPoint);
        mBean = ApacheWizardCreator.getApacheMBeanOnNode(connector);

        Vector reviewPanel = new Vector();

        Object isNewHost = wizardModel.getWizardValue(
                ApacheWizardConstants.NEWHOST);
        Object isNewMount = wizardModel.getWizardValue(
                ApacheWizardConstants.NEWMOUNT);
        String apacheConfFilePath = (String) wizardModel.getWizardValue(
                Util.APACHE_CONF_FILE);
        String mountPoint = (String) wizardModel.getWizardValue(
                Util.APACHE_MOUNT);
        String apacheDoc = (String) wizardModel.getWizardValue(Util.APACHE_DOC);
        String apachePort[] = (String[]) wizardModel.getWizardValue(
                Util.APACHE_PORT);
        StringBuffer apachePortsBuf = new StringBuffer();

        for (int i = 0; i < apachePort.length; i++) {
            apachePortsBuf = apachePortsBuf.append(apachePort[i]);

            if (i < (apachePort.length - 1)) {
                apachePortsBuf.append(",");
            }
        }


        // APACHE_MODE
        String apacheMode = (String) wizardModel.getWizardValue(
                Util.APACHE_MODE);
        String modeDesc = messages.getString("apache.summary.modeDesc",
                new String[] { apacheMode });
        reviewPanel.add(mode);
        reviewPanel.add(apacheMode);

        // APACHE_LISTEN
        String apacheListen[] = (String[]) wizardModel.getWizardValue(
                Util.APACHE_LISTEN);
        StringBuffer apacheHostNameBuf = new StringBuffer();

        for (int i = 0; i < apacheListen.length; i++) {
            apacheHostNameBuf = apacheHostNameBuf.append(apacheListen[i]);

            if (i < (apacheListen.length - 1)) {
                apacheHostNameBuf.append(",");
            }
        }

        // Nodelist
        StringBuffer nodeBuf = new StringBuffer();

        for (int i = 0; i < nodeListArr.length; i++) {
            nodeBuf = nodeBuf.append(nodeListArr[i]);

            if (i < (nodeListArr.length - 1)) {
                nodeBuf.append(" ");
            }
        }

        reviewPanel.add(nodeList);
        reviewPanel.add(nodeBuf.toString());

        String apacheRes = (String) wizardModel.getWizardValue(
                Util.APACHE_RESOURCE_NAME);

        // Bin_dir
        // Add "/bin" to the server root
        reviewPanel.add(binDir);
        reviewPanel.add(mountPoint + Util.SLASH + apacheRes + "/bin");

        // Second Row - Apache Resource Name
        reviewPanel.add(apacheResource);
        reviewPanel.add(apacheRes);

        // Third Row - Apache Resource Group
        String apacheRG = (String) wizardModel.getWizardValue(
                Util.APACHE_RESOURCE_GROUP);
        reviewPanel.add(apacheResourceGroup);
        reviewPanel.add(apacheRG);


        // Third Row - Storage Resource
        String storageRes = (String) wizardModel.getWizardValue(
                ApacheWizardConstants.STORAGE_RESOURCE_NAME);
        reviewPanel.add(storageResource);
        reviewPanel.add(storageRes);

        // Fourth Row - Hostname Resource
        String hostResource;
        String sharedAddressRG = null;

        if (apacheMode.equals(Apache.FAILOVER)) {
            hostResource = (String) wizardModel.getWizardValue(
                    Util.LH_RESOURCE_NAME);
            reviewPanel.add(lhResource);
            reviewPanel.add(hostResource);
        } else {
            hostResource = (String) wizardModel.getWizardValue(
                    Util.SH_RESOURCE_NAME);
            reviewPanel.add(shResource);
            reviewPanel.add(hostResource);
            sharedAddressRG = (String) wizardModel.getWizardValue(
                    Util.APACHE_SHAREDADDRESS_GROUP);
            reviewPanel.add(shGroup);
            reviewPanel.add(sharedAddressRG);
        }

        // Print the Resource and ResourceGroup names
        while (true) {
            TTYDisplay.clearScreen();
            TTYDisplay.clear(1);
            TTYDisplay.printTitle(title);
            TTYDisplay.clear(1);
            TTYDisplay.pageText(instruction);
            TTYDisplay.setNavEnabled(true);
            this.cancelDirection = false;

            HashMap customTags = new HashMap();
            customTags.put(cchar, createConfigurationText);

            String subheadings[] = { heading1, heading2 };

            // Display the Table
            TTYDisplay.clear(1);

            String option = TTYDisplay.create2DTable(scTableTitle, subheadings,
                    reviewPanel, customTags, help);

            if (option.equals("<")) {
                this.cancelDirection = true;

                return;
            }

            // Process the options
            if (option.equals("1")) {
                TTYDisplay.clear(2);
                TTYDisplay.pageText(cliDesc + modeDesc);
            } else if (option.equals("2")) {
                TTYDisplay.clear(3);
                TTYDisplay.pageText(cliDesc + nodeListDesc);
                TTYDisplay.clear(1);
                TTYDisplay.pageText(nodeList + ": " + nodeBuf.toString());
            } else if (option.equals("3")) {
                TTYDisplay.clear(2);
                TTYDisplay.pageText(cliDesc + binDirDesc);
            } else if (option.equals("4")) {
                TTYDisplay.clear(2);
                TTYDisplay.pageText(cliDesc + apacheResourceDesc);
            } else if (option.equals("5")) {
                TTYDisplay.clear(2);
                TTYDisplay.pageText(cliDesc + apacheRGDesc);
            } else if (option.equals("6")) {
                TTYDisplay.clear(2);
                TTYDisplay.pageText(cliDesc + storageResourceDesc);
            } else if (option.equals("7")) {
                TTYDisplay.clear(2);

                if (apacheMode.equals(Apache.FAILOVER)) {
                    TTYDisplay.pageText(cliDesc + lhresourceDesc);
                } else {
                    TTYDisplay.pageText(cliDesc + shresourceDesc);
                }
            } else if (option.equals("8")) {
                TTYDisplay.clear(2);
                TTYDisplay.pageText(cliDesc + shRGDesc);
            }
            // Generate Commands
            else if (option.equals("c")) {

                /*
                 * The applicationConfiguration method is called on the
                 * mBean of the current node, this edits the user given
                 * apache configuration file and the control and copies it to
                 * the ds-config temp location. Upon command generation these
                 * temp files are copied under the HA MountPoint location.
                 */

                Map appMap = new HashMap();

                String apacheZone = (String) wizardModel.getWizardValue(
                        Util.APACHE_ZONE);
                String zoneExportPath = null;
                String wizardNode = nodeEndPoint;

                String apacheServerRoot = (String) wizardModel.getWizardValue(
                        Util.APACHE_HOME);
                apacheServerRoot = apacheServerRoot.replace('\"', ' ');
                apacheServerRoot = apacheServerRoot.trim();

                if (apacheZone != null) {
                    HashMap discoveredMap = mBean.discoverPossibilities(
                            new String[] { Util.APACHE_ZONE }, nodeEndPoint);
                    zoneExportPath = (String) discoveredMap.get(
                            Util.APACHE_ZONE);
                }

                String pidFile = (String) wizardModel.getWizardValue(
                        Util.PIDFILE);
                appMap.put(Util.APACHE_ZONE, zoneExportPath);
                appMap.put(Util.APACHE_HOME, apacheServerRoot);
                appMap.put(Util.APACHE_MOUNT, mountPoint);
                appMap.put(Util.APACHE_CONF_FILE, apacheConfFilePath);
                appMap.put(Util.APACHE_PORT, apachePortsBuf.toString());
                appMap.put(Util.APACHE_LISTEN, apacheHostNameBuf.toString());
                appMap.put(Util.APACHE_DOC, apacheDoc);
                appMap.put(Util.APACHE_RESOURCE_NAME, apacheRes);
                appMap.put(Util.PIDFILE, pidFile);

                try {

                    try {
                        ErrorValue result = mBean.applicationConfiguration(
                                appMap);

                        if (!result.getReturnValue().booleanValue()) {
                            TTYDisplay.printError(result.getErrorString());
                            TTYDisplay.clear(1);
                            TTYDisplay.promptForEntry(continueText);
                        }
                    } catch (Exception e) {
                        throw new Exception(e.getMessage() + "\n" + "@" +
                            nodeEndPoint);
                    }

                    // Create the Command Generation Structure
                    Map confMap = new HashMap();

                    // Entries to aid configuration
                    confMap.put(Util.APACHE_DOC, apacheDoc);

                    if (nodeEndPoint.split(Util.COLON).length == 2) {
                        wizardNode = (nodeEndPoint.split(Util.COLON))[0];
                    }

                    confMap.put(Util.NODE_TO_CONNECT, wizardNode);
                    confMap.put(Util.APACHE_MODE, apacheMode);
                    confMap.put(Util.APACHE_MOUNT, mountPoint);
                    confMap.put(Util.APACHE_CONF_FILE, apacheConfFilePath);
                    confMap.put(Util.APACHE_HOME, apacheServerRoot);
                    confMap.put(Util.APACHE_RESOURCE_NAME, apacheRes);

                    /*
                     * Apache ResourceGroup Structure
                     */
                    // ResourceGroup Name
                    confMap.put(Util.SERVER_GROUP + Util.NAME_TAG, apacheRG);
                    Boolean qfsRsSelected = (Boolean)wizardModel.getWizardValue(
                         ApacheWizardConstants.APACHE_QFS_RS_SELECTED);

                    Map rgPropMap = new HashMap();

                    // ResourceGroup Properties
                    // Set the properties only if the Apache Resource
                    // Group is new
                    if (((isNewHost != null) && (isNewMount != null)) ||
                            (((isNewMount != null) || (qfsRsSelected != null)) &&
                                apacheMode.equals(Apache.SCALABLE))) {
                        rgPropMap.put(Util.NODELIST,
                            nodeBuf.toString().replace(' ', ','));

                        // If its a Scalable Apache,set the
                        // MaximumPrimaries and DesiredPrimaries
                        if (apacheMode.equals(Apache.SCALABLE)) {
                            rgPropMap.put(Util.MAX_PRIMARIES,
                                String.valueOf(nodeListArr.length));
                            rgPropMap.put(Util.DESIRED_PRIMARIES,
                                String.valueOf(nodeListArr.length));
                        }
                    }

                    confMap.put(Util.SERVER_GROUP + Util.RGPROP_TAG, rgPropMap);

                    /*
                     * Apache Resource Structure :
                     *
                     * We add the Resource Name and Bin_dir
                     * and Ports_Used property
                     */
                    confMap.put(Util.APACHERESOURCE + Util.NAME_TAG, apacheRes);

                    // Resource Properties
                    List apacheExtProp = new ArrayList();
                    List apacheSysProp = new ArrayList();

                    // Get ApachePort and add it to the structure
                    apachePortsBuf = new StringBuffer();

                    // If apacheListen has IPV6 Hostnames, set the protocol
                    // as tcp/6
                    HashMap resultMap = mBean.discoverPossibilities(
                            new String[] { Util.PORT_LIST }, apacheListen);
                    String protocol = (String) resultMap.get(Util.PORT_LIST);

                    for (int i = 0; i < apachePort.length; i++) {
                        apachePortsBuf = apachePortsBuf.append(apachePort[i] +
                                "/" + protocol);

                        if (i < (apachePort.length - 1)) {
                            apachePortsBuf.append(",");
                        }
                    }

                    ResourceProperty rp = new ResourcePropertyString(
                            Util.PORT_PROP, apachePortsBuf.toString());
                    apacheSysProp.add(rp);

                    // Add Scalable=TRUE is Scalable
                    if (apacheMode.equals(Apache.SCALABLE)) {
                        rp = new ResourcePropertyBoolean(Apache.SCALABLE,
                                Boolean.TRUE);
                        apacheSysProp.add(rp);
                    }

                    // Get ServerRoot and add it to the structure
                    rp = new ResourcePropertyString(Util.BIN_DIR,
                            mountPoint + Util.SLASH + apacheRes + "/bin");
                    apacheExtProp.add(rp);
                    confMap.put(Util.APACHERESOURCE + Util.SYSPROP_TAG,
                        apacheSysProp);
                    confMap.put(Util.APACHERESOURCE + Util.EXTPROP_TAG,
                        apacheExtProp);

                    /*
                     * Hostname Resource Structure :
                     *
                     * We add the Resource Name and if new we need to
                     * set the system and extension properties also
                     */
                    if (apacheMode.equals(Apache.FAILOVER)) {
                        confMap.put(Util.LOGICALHOST + Util.NAME_TAG,
                            hostResource);
                    } else {
                        confMap.put(Util.SHAREDADDRESS + Util.NAME_TAG,
                            hostResource);
                        confMap.put(Util.SHAREDADDRESSGROUP + Util.NAME_TAG,
                            sharedAddressRG);
                    }

                    if (isNewHost != null) {
                        List logicalHostExtProp = new ArrayList();
                        List logicalHostSysProp = new ArrayList();
                        String hostnameList[] = (String[]) wizardModel
                            .getWizardValue(Util.HOSTNAMELIST);
                        String netifList[] = (String[]) wizardModel
                            .getWizardValue(
                                LogicalHostWizardConstants.SEL_NETIFLIST);
                        rp = new ResourcePropertyStringArray(Util.HOSTNAMELIST,
                                hostnameList);
                        logicalHostExtProp.add(rp);

                        if (netifList != null) {
                            rp = new ResourcePropertyStringArray(Util.NETIFLIST,
                                    netifList);
                            logicalHostExtProp.add(rp);
                        }

                        if (apacheMode.equals(Apache.FAILOVER)) {
                            confMap.put(Util.LOGICALHOST + Util.SYSPROP_TAG,
                                logicalHostSysProp);
                            confMap.put(Util.LOGICALHOST + Util.EXTPROP_TAG,
                                logicalHostExtProp);
                        } else {
                            confMap.put(Util.SHAREDADDRESS + Util.SYSPROP_TAG,
                                logicalHostSysProp);
                            confMap.put(Util.SHAREDADDRESS + Util.EXTPROP_TAG,
                                logicalHostExtProp);

                            // Create the Structure for SharedAddress RG
                            // Set the nodelist as the same as the Scalable RG
                            rgPropMap = new HashMap();

                            // ResourceGroup Properties
                            rgPropMap.put(Util.NODELIST,
                                nodeBuf.toString().replace(' ', ','));
                            confMap.put(Util.SHAREDADDRESSGROUP +
                                Util.RGPROP_TAG, rgPropMap);
                        }
                    }

                    /*
                     * Storage Resource Structure :
                     *
                     * We add the Resource Name and if new we need to
                     * set the system and extension properties also
                     */

                    Boolean sqfsSelected = (Boolean) wizardModel.getWizardValue(
                        HASPWizardConstants.SQFS_SELECTED);

                    String RES_ID = Util.STORAGERESOURCE;
                    String FILESYSTEM = Util.HASP_ALL_FILESYSTEMS;

                    if (sqfsSelected != null) {
                        // shared QFS filesystem is selected
                        RES_ID = Util.QFS_RES_ID;
                        FILESYSTEM = Util.QFS_FILESYSTEM;
                    }
                    confMap.put(RES_ID + Util.NAME_TAG, storageRes);

                    if (isNewMount != null) {
                        List rsExtProp = new ArrayList();
                        List rsSysProp = new ArrayList();

                        String fsMountPoints[] = (String[]) wizardModel
                            .getWizardValue(
                                ApacheWizardConstants
                                .APACHE_FILESYSTEMS_SELECTED);

                        if ((fsMountPoints != null) &&
                                (fsMountPoints.length > 0)) {
                            rp = new ResourcePropertyStringArray(
                                    FILESYSTEM, fsMountPoints);
                            rsExtProp.add(rp);

                            // Get the map to add for /etc/vfstab
                            // and update all nodes
                            HashMap fsHashMap = (HashMap) wizardModel
                                .getWizardValue(Util.HASP_ALL_FILESYSTEMS);
                            HashMap map_to_add = (HashMap) fsHashMap.get(
                                    Util.VFSTAB_MAP_TO_ADD);

                            JMXConnector connectors[] =
                                new JMXConnector[nodeListArr.length];
                            MBeanServerConnection mbsConnections[] =
                                new MBeanServerConnection[nodeListArr.length];

                            for (int i = 0; i < nodeListArr.length; i++) {
                                String split_str[] = nodeListArr[i].split(
                                        Util.COLON);
                                nodeEndPoint = Util.getNodeEndpoint(
                                        localConnection, split_str[0]);
                                connectors[i] = getConnection(nodeEndPoint);

                                try {
                                    mbsConnections[i] = connectors[i]
                                        .getMBeanServerConnection();
                                } catch (IOException ioe) {
                                }
                            }

                            FileAccessorWrapper fileAccessor =
                                new FileAccessorWrapper(Util.VFSTAB_FILE,
                                    mbsConnections);

                            for (int i = 0; i < nodeListArr.length; i++) {
                                String split_str[] = nodeListArr[i].split(
                                        Util.COLON);
                                ArrayList nodeEntry = (ArrayList) map_to_add
                                    .get(split_str[0]);

                                if (nodeEntry != null) {

                                    // each element of the list is a vfstab
                                    // entry
                                    if (mbsConnections[i] != null) {
                                        fileAccessor.appendLine(nodeEntry,
                                            mbsConnections[i]);
                                    }
                                }
                            }
                        }

                        confMap.put(RES_ID + Util.SYSPROP_TAG, rsSysProp);
                        confMap.put(RES_ID + Util.EXTPROP_TAG, rsExtProp);
                    }

                    // Generating Commands
                    generateCommands(confMap);

                    return;
                } catch (Exception e) {
                    TTYDisplay.print(e.getLocalizedMessage());
                    TTYDisplay.promptForEntry(continueText);

                    break;
                }
            }

            TTYDisplay.clear(1);
            TTYDisplay.promptForEntry(continueText);
        }
    }

    /**
     * Generates the commands by invoking the command generator
     */
    private boolean generateCommands(Map confMap) {
        List commandList = null;
        Thread t = null;
        boolean retVal = true;

        try {
            TTYDisplay.clear(1);
            t = TTYDisplay.busy(messages.getString(
                        "cliwizards.executingMessage"));
            t.start();
            commandList = mBean.generateCommands(confMap);
        } catch (CommandExecutionException cee) {
            wizardModel.setWizardValue(Util.EXITSTATUS, cee);
            commandList = mBean.getCommandList();
            retVal = false;
        } finally {
            wizardModel.setWizardValue(Util.CMDLIST, commandList);

            try {
                t.interrupt();
                t.join();
            } catch (Exception exe) {
            }
        }

        return retVal;
    }
}
