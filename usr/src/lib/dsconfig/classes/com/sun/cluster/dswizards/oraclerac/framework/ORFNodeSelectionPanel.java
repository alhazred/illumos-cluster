/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)ORFNodeSelectionPanel.java	1.14	09/03/11 SMI"
 */

package com.sun.cluster.dswizards.oraclerac.framework;

import com.sun.cluster.agent.dataservices.oraclerac.OracleRACMBean;

// CMASS
import com.sun.cluster.agent.dataservices.utils.Util;

// DSWizards
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliUtil;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.NodeSelectionPanel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.oraclerac.invoker.RACInvokerConstants;
import com.sun.cluster.dswizards.oraclerac.invoker.RACInvokerCreator;

// J2SE
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.sun.cluster.model.RACModel;

/**
 * This panel gets the NodeList from the User
 */
public class ORFNodeSelectionPanel extends NodeSelectionPanel {

    private String oraclerac;

    private RACModel racModel = null;

    /**
     * Creates a new instance of ORFNodeSelectionPanel
     */
    public ORFNodeSelectionPanel() {
    }

    /**
     * Creates an ORFNodeSelectionPanel Panel with the given name and manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public ORFNodeSelectionPanel(String name, WizardTreeManager manager) {
        super(name, manager);
    }

    /**
     * Creates an ORFNodeSelectionPanelPanel for the wizard with the given name
     * and associates it with the WizardModel and WizardFlow.
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     */
    public ORFNodeSelectionPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(name, model, wizardFlow, wizardi18n);
        initStrings();
    }

    private void initStrings() {
        desc = wizardi18n.getString("orf.nodeSelectionPanel.desc");
        oraclerac = wizardi18n.getString("dswizards.oraclerac.wizard");
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        wizardModel.selectCurrWizardContext();

        String propertyNames[] = null;
        HashMap discoveredMap = null;
        String nodeList[] = null;

	HashMap helperData = null;
	String selectedZoneCluster = CliUtil.getZoneClusterName(wizardModel);
	if (selectedZoneCluster != null &&
	    selectedZoneCluster.trim().length() > 0) {
	    helperData = new HashMap();
	    helperData.put(Util.SEL_ZONE_CLUSTER, selectedZoneCluster);
	}

	String nodeEndPoint = (String) wizardModel.getWizardValue(
	    Util.NODE_TO_CONNECT);
        while (true) {
            nodeList = displayPanel(OracleRACMBean.class, oraclerac);

            if (nodeList == null) {
                this.cancelDirection = true;

                return;
            }

            wizardModel.setWizardValue(Util.RG_NODELIST, nodeList);

	    String[] baseClusterNodeList = (String[]) wizardModel.getWizardValue(
		Util.BASE_CLUSTER_NODELIST);

            // Contact the RACModel and Get All Properties required
            // for RACFramework Wizard

	    racModel = RACInvokerCreator.getRACModel();
	    if (racModel != null) {
                // Check for proper installation of RAC packages
                propertyNames = new String[] { Util.FRMWRK_RT };
                discoveredMap = racModel.discoverPossibilities(
			null, nodeEndPoint, baseClusterNodeList,
                        propertyNames, helperData);

                ArrayList frmwrkRT = (ArrayList) discoveredMap.get(
                        Util.FRMWRK_RT);

                if (frmwrkRT == null || frmwrkRT.isEmpty()) {

                    // Packages not installed properly
                    TTYDisplay.printError(wizardi18n.getString(
                            "orf.nodeSelectionPanel.package.error"));
                    TTYDisplay.clear(1);
                    TTYDisplay.promptForEntry(wizardi18n.getString(
                            "cliwizards.continue"));
                } else {
                    wizardModel.setWizardValue(Util.FRMWRK_RT, frmwrkRT);

                    break;
                }
            }
        }

        if (racModel != null) {

            // Get the Discoverable Properties For Oracle RAC Framework Wizard
            propertyNames = new String[] { Util.PROPERTY_NAMES };
            discoveredMap = racModel.discoverPossibilities(
		null, nodeEndPoint, propertyNames, null);

            // Copy the Data to the WizardModel
            propertyNames = new String[discoveredMap.size()];

            Set entrySet = discoveredMap.entrySet();
            Iterator setIterator = entrySet.iterator();
            int i = 0;

            while (setIterator.hasNext()) {
                Map.Entry entry = (Map.Entry) setIterator.next();
                wizardModel.setWizardDefaultValue((String) entry.getKey(),
                    entry.getValue());
                propertyNames[i++] = (String) entry.getKey();
            }

            wizardModel.setWizardValue(Util.PROPERTY_NAMES, propertyNames);
        }

        // Set the wizard state key for storage management schemes.
        ArrayList propNames = new ArrayList();
        Object tmpObj = (ArrayList) wizardModel.getWizardValue(
                Util.WIZARD_STATE);

        if (tmpObj != null) {
            propNames = (ArrayList) tmpObj;
	}

	String keyPrefix = (String) wizardModel.
	    getWizardValue(Util.KEY_PREFIX);
	propNames.add(keyPrefix + Util.ORF_STORAGE_MGMT_SCHEMES);
	propNames.add(Util.CLUSTER_SEL);
	propNames.add(Util.SEL_ZONE_CLUSTER);
	wizardModel.setWizardValue(Util.WIZARD_STATE, propNames);
    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }
}
