/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)HASPFileSystemSelectionPanel.java 1.22     08/12/10 SMI"
 */


package com.sun.cluster.dswizards.hastorageplus;

import com.sun.cluster.agent.dataservices.hastorageplus.HAStoragePlusMBean;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.dataservices.utils.VfsStruct;
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import java.util.Iterator;

import javax.management.remote.JMXConnector;

public class HASPFileSystemSelectionPanel extends WizardLeaf {

    private String panelName; // Panel Name
    private WizardI18N wizardi18n; // Message Bundle
    private CliDSConfigWizardModel wizardModel; // WizardModel
    
    private JMXConnector localConnection = null;
    /**
     * Creates a new instance of HASPFileSystemSelectionPanel
     */
    public HASPFileSystemSelectionPanel() {
        super();
    }

    /**
     * Creates a HASPFileSystemSelectionPanel Panel with the 
     * given name and manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public HASPFileSystemSelectionPanel(String name,
        WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    public HASPFileSystemSelectionPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;
        this.wizardi18n = wizardi18n;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.
     *		WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        String back = wizardi18n.getString("ttydisplay.back.char");
        String yes = wizardi18n.getString("cliwizards.yes");
        String no = wizardi18n.getString("cliwizards.no");
        String title = wizardi18n.getString("hasp.filesystem.title");
        String desc = wizardi18n.getString("hasp.filesystem.desc");
        String emptytable_desc = wizardi18n.getString(
                "cliwizards.hasp.filesystem.empty.desc");
        String cchar = wizardi18n.getString("ttydisplay.c.char");
        String createnew_txt = wizardi18n.getString(
                "ttydisplay.menu.create.new");
        String achar = wizardi18n.getString("ttydisplay.a.char");
        String all = wizardi18n.getString("ttydisplay.menu.all");
        String rchar = wizardi18n.getString("ttydisplay.r.char");
        String refresh = wizardi18n.getString("ttydisplay.menu.refresh");
        String done = wizardi18n.getString("ttydisplay.menu.done");
        String help_text = wizardi18n.getString("hasp.filesystem.cli.help");
        String continue_txt = wizardi18n.getString(
                "hasp.filesystem.defineMount.message");
        String heading1 = wizardi18n.getString(
                "hasp.filesystem.fstable.colheader.0");
        String heading2 = wizardi18n.getString(
                "hasp.filesystem.fstable.colheader.1");

        HashMap hashMap = null;
        Object obj = wizardModel.getWizardValue(Util.HASP_ALL_FILESYSTEMS);
        boolean refreshMountpoints = false;

        if ((obj == null) || !(obj instanceof HashMap)) {
            refreshMountpoints = true;
        } else {
            hashMap = (HashMap) obj;
        }

        // If nodelist was changed, refresh mountpoints
        String prevNodeList[] = (String[]) wizardModel.getWizardCurrValue(
                HASPWizardConstants.PREV_RG_NODELIST);

        if (prevNodeList != null) {
            String curNodeList[] = (String[]) wizardModel.getWizardValue(
                    Util.RG_NODELIST);
            List curList = Arrays.asList(curNodeList);
            List prevList = Arrays.asList(prevNodeList);

            if (!((curList.size() == prevList.size()) &&
                        curList.containsAll(prevList))) {
                refreshMountpoints = true;
            }
        }

        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        String subheadings[] = new String[] { heading1, heading2 };
        Vector data = new Vector();

        boolean createnew_choice = false;

        do {

            // Title
            TTYDisplay.printTitle(title);

            if (refreshMountpoints) {
                resetFilesystemWizardValue();
                hashMap = (HashMap) wizardModel.getWizardValue(
                        Util.HASP_ALL_FILESYSTEMS);
                refreshMountpoints = false;
                TTYDisplay.clear(2);
            }

            VfsStruct options[] = null;

            if (hashMap != null) {
                options = (VfsStruct[]) hashMap.get(Util.VFSTAB_ENTRIES);
            }

            if ((options == null) || (options.length == 0)) {

                // Display empty table description para
                TTYDisplay.displaySubheadings(subheadings);
                TTYDisplay.printSubTitle(emptytable_desc);

                String choice = TTYDisplay.promptForEntry(continue_txt);

                if (choice.equals(back)) {
                    this.cancelDirection = true;

                    return;
                }

                // handle create new
                wizardModel.setWizardValue(
                    HASPWizardConstants.HASP_CREATE_FILESYSTEM,
                    HASPWizardConstants.CREATE_NEW);
                wizardModel.setWizardValue(
                    HASPWizardConstants.HASP_FILESYSTEMS_SELECTED,
                    (String[]) (new ArrayList()).toArray(new String[0]));

                createnew_choice = true;
            } else {
                String addedMtpt = (String) wizardModel.getWizardValue(
                        HASPWizardConstants.NEWLY_ADDED_MTPT);

                if (addedMtpt != null) {
                    TTYDisplay.printSubTitle(wizardi18n.getString(
                            "haspfspanel.alert.summary",
                            new String[] { addedMtpt }));
                    wizardModel.setWizardValue(
                        HASPWizardConstants.NEWLY_ADDED_MTPT, null);
                }

                TTYDisplay.printSubTitle(desc);
                TTYDisplay.printSubTitle(wizardi18n.getString(
                        "hasp.filesystem.extraDesc"));

                data = getTableEntries(options);
                HashMap mntTypeMap = getMntFSTypeHash(options);

                String filesystems[] = null;

                HashMap customTags = new HashMap();
                customTags.put(cchar, createnew_txt);

                if (options.length > 0 && !bothHASPandQFS(options)) {
                    customTags.put(achar, all);
                }

                customTags.put(rchar, refresh);

                // default selections are obtained from
                // HASP_FILESYSTEMS_SELECTED
                wizardModel.selectCurrWizardContext();

                String defSelections[] = (String[]) wizardModel.getWizardValue(
                        HASPWizardConstants.HASP_FILESYSTEMS_SELECTED);

                String commaSeparatedSelections = "";

                if (defSelections != null) {

                    for (int i = 0; i < defSelections.length; i++) {

                        // When the filesystem mountpoints are refreshed, some
                        // of
                        // the default selections might be invalid.
                        // So, add only valid default selections
                        if (data.contains(defSelections[i])) {
                            commaSeparatedSelections += defSelections[i];

                            if ((commaSeparatedSelections.length() > 0) &&
                                    (i != defSelections.length)) {
                                commaSeparatedSelections += ",";
                            }
                        }
                    }
                }

                List selections = TTYDisplay.getScrollingOptions("",
                        subheadings, data, customTags, commaSeparatedSelections,
                        help_text, true);

                if (selections == null) {
                    this.cancelDirection = true;

                    return;
                }

                if (selections.contains(refresh)) {

                    // refresh the filesystem list
                    refreshMountpoints = true;

                    continue;
                }

                // Get the elements in the list
                if (selections.contains(all)) {
                    filesystems = getFileSystems(options);
                } else {
                    boolean createnew = false;

                    if (selections.contains(createnew_txt)) {
                        createnew = true;
                        selections.remove(createnew_txt);
                    }

                    filesystems = (String[]) selections.toArray(new String[0]);

                    if (createnew) {

                        // add it back to selections
                        selections.add(createnew_txt);
                    }
                }

                // validate if both QFS and HASP are selected
                int selectedFSType = getSelectedFSType(mntTypeMap, filesystems);
                if (selectedFSType == HASPWizardConstants.BOTH_FS_SELECTED) {
                    TTYDisplay.printError(wizardi18n.getString(
                        "qfs.filesystem.selection.error"));
                    String continueTxt = wizardi18n.getString(
                        "ttydisplay.msg.cli.enter");
                    TTYDisplay.promptForEntry(continueTxt);

                    continue;
                }
 
                if (selections.contains(createnew_txt)) {

                    // handle create new
                    wizardModel.setWizardValue(
                        HASPWizardConstants.HASP_CREATE_FILESYSTEM,
                        HASPWizardConstants.CREATE_NEW);
                    wizardModel.setWizardValue(
                        HASPWizardConstants.HASP_FILESYSTEMS_SELECTED,
                        filesystems);
                    createnew_choice = true;
                } else {

                    wizardModel.setWizardValue(
                        HASPWizardConstants.HASP_CREATE_FILESYSTEM,
                        HASPWizardConstants.SELECT_EXISTING);

                    // Store the Values in the WizardModel
                    wizardModel.setWizardValue(
                        HASPWizardConstants.HASP_FILESYSTEMS_SELECTED,
                        filesystems);
                    
                    if (selectedFSType == HASPWizardConstants.QFS_FS_SELECTED) {
                        wizardModel.setWizardValue(
                            HASPWizardConstants.SQFS_SELECTED, Boolean.TRUE);
                    }
                    break;
                }
            }
        } while (!createnew_choice);
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#isDisplayComplete
     */
    public boolean isDisplayComplete() {
        return true;
    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }

    private void resetFilesystemWizardValue() {
        Thread t = null;
        TTYDisplay.clear(2);
        t = TTYDisplay.busy(wizardi18n.getString(
                    "hasp.filesystem.discover.busy"));
        localConnection = getConnection(Util.getClusterEndpoint());

        HAStoragePlusMBean mbean = HASPWizardCreator.getHAStoragePlusMBean(
                localConnection);
        String rgNodeList[] = (String[]) wizardModel.getWizardValue(
                Util.RG_NODELIST);
        wizardModel.setWizardValue(HASPWizardConstants.PREV_RG_NODELIST,
            rgNodeList);

        String rgMode = (String) wizardModel.getWizardValue(Util.RG_MODE);

        HashMap helperData = new HashMap();
        helperData.put(Util.RG_NODELIST, rgNodeList);
        helperData.put(Util.RG_MODE, rgMode);
        t.start();

        HashMap hashMap = (HashMap) mbean.discoverPossibilities(
                new String[] { Util.HASP_ALL_FILESYSTEMS }, helperData);

        // discovery done, interrupt the thread
        try {
            t.interrupt();
            t.join();
        } catch (Exception e) {
        }

        if (hashMap != null) {
            wizardModel.setWizardValue(Util.HASP_ALL_FILESYSTEMS,
                hashMap.get(Util.HASP_ALL_FILESYSTEMS));
        }
    }

    private Vector getTableEntries(VfsStruct vfstabEntries[]) {
        Vector data = new Vector();

        if (vfstabEntries != null) {

            for (int i = 0; i < vfstabEntries.length; i++) {
                VfsStruct vfstabEntry = (VfsStruct) vfstabEntries[i];
                data.add(vfstabEntry.getVfs_mountp());
                data.add(vfstabEntry.getVfs_fstype());
            }
        }

        return data;
    }

    private HashMap getMntFSTypeHash(VfsStruct vfstabEntries[]) {
        HashMap mntTypeMap = new HashMap(); // return HashMap

        if (vfstabEntries != null) {
            for (int i = 0; i < vfstabEntries.length; i++) {
                mntTypeMap.put(vfstabEntries[i].getVfs_mountp(),
                    vfstabEntries[i].getVfs_fstype());
            }
        }
        return mntTypeMap;
    }

    private boolean bothHASPandQFS(VfsStruct vfstabEntries[]) {
       
        boolean foundQFS = false;
        boolean foundHASP = false;
 
        if (vfstabEntries != null) {
            for (int i = 0; i < vfstabEntries.length; i++) {

                VfsStruct vfstabEntry = (VfsStruct) vfstabEntries[i];
                String fstype = vfstabEntry.getVfs_fstype();

                if (fstype != null && fstype.equals(Util.FSTYPE_SAMFS)) {
                    if (!foundQFS) foundQFS = true;
                } else { // all others are considered as HASP
                    if (!foundHASP) foundHASP = true;
                }

                if (foundQFS && foundHASP) {
                    // don't have to go through all entries
                    return true;
                }
            }
        }
        // no both HASP and QFS mountpoint entries
        return false;
    }

    private int getSelectedFSType(HashMap mntTypeMap, String filesystems[]) {
        
        boolean foundQFS = false;
        boolean foundHASP = false;

        for (int i = 0; i < filesystems.length; i++) {

            String fstype = (String)mntTypeMap.get(filesystems[i]);
            if (fstype != null && fstype.equals(Util.FSTYPE_SAMFS)) {
                if (!foundQFS) foundQFS = true;
            } else { // all others are considered as HASP
                if (!foundHASP) foundHASP = true;
            }

            if (foundQFS && foundHASP) {
                // don't have to go through all entries
                return HASPWizardConstants.BOTH_FS_SELECTED;
            }
        }
 
        if (foundQFS) {
            return HASPWizardConstants.QFS_FS_SELECTED;
        }
        return HASPWizardConstants.OTHER_FS_SELECTED;
    }

    private String[] getFileSystems(VfsStruct vfstabEntries[]) {
        String filesystems[] = null;

        if (vfstabEntries != null) {
            filesystems = new String[vfstabEntries.length];

            for (int i = 0; i < vfstabEntries.length; i++) {
                VfsStruct vfstabEntry = (VfsStruct) vfstabEntries[i];
                filesystems[i] = vfstabEntry.getVfs_mountp();
            }
        }

        return filesystems;
    }
}
