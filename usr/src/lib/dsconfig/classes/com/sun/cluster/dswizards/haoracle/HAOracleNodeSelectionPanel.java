/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)HAOracleNodeSelectionPanel.java 1.11     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.haoracle;

import com.sun.cluster.agent.dataservices.haoracle.HAOracleMBean;

// CMASS
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.NodeSelectionPanel;

// Wizard Common
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;

// J2SE
import java.util.HashMap;


/**
 * This panel gets the Preferred Node from the User
 */
public class HAOracleNodeSelectionPanel extends NodeSelectionPanel {

    private String oracle = null;

    /**
     * Creates a new instance of HAOracleNodeSelectionPanel
     */
    public HAOracleNodeSelectionPanel() {
    }

    /**
     * Creates an PreferredNode Panel with the given name and manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public HAOracleNodeSelectionPanel(String name, WizardTreeManager manager) {
        super(name, manager);
    }

    /**
     * Creates an PreferredNode Panel for the wizard with the given name and
     * associates it with the WizardModel and WizardFlow.
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     * @param  wizardType  WizardType
     */
    public HAOracleNodeSelectionPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(name, model, wizardFlow, wizardi18n);
        initStrings();
    }


    private void initStrings() {
        desc = wizardi18n.getString("haoracle.nodeSelectionPanel.desc");
        oracle = wizardi18n.getString("dswizards.oracle.wizard");
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        String nodeList[] = displayPanel(HAOracleMBean.class, oracle);

        if (nodeList == null) {
            this.cancelDirection = true;

            return;
        }

        wizardModel.setWizardValue(Util.RG_NODELIST, nodeList);
    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }
}
