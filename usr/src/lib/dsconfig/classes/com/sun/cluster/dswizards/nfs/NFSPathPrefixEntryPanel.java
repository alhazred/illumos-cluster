/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)NFSPathPrefixEntryPanel.java 1.13     08/12/10 SMI"
 */


package com.sun.cluster.dswizards.nfs;


// J2SE
import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.logicalhost.LogicalHostMBean;

// CMASS
import com.sun.cluster.agent.dataservices.nfs.NFSMBean;
import com.sun.cluster.agent.dataservices.utils.DataServicesUtil;
import com.sun.cluster.agent.dataservices.utils.Util;

// CLI Wizard SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;

// CLI wizard
import com.sun.cluster.dswizards.logicalhost.LogicalHostWizardCreator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;


/**
 * The Panel Class that gets a PathPrefix as input from the User.
 */
public class NFSPathPrefixEntryPanel extends WizardLeaf {


    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N wizardi18n; // Message Bundle

    /**
     * Creates a new instance of NFSPathPrefixEntryPanel.
     */
    public NFSPathPrefixEntryPanel() {
        super();
    }

    /**
     * Creates a NFSPathPrefixEntryPanel Panel with the given name and tree
     * manager.
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public NFSPathPrefixEntryPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates a NFSPathPrefixEntryPanel Panel for either the NFS Wizard with
     * the given name and associates it with the WizardModel and WizardFlow.
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     * @param  wizardi18n  Resource Bundle
     */
    public NFSPathPrefixEntryPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;
        this.wizardi18n = wizardi18n;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {

        String title_txt = wizardi18n.getString("nfs.pathPrefixEntry.name");
        String pathPrefixEntry_title = wizardi18n.getString(
                "nfs.pathPrefixEntry.listtitle");
        String pathPrefixEntry_desc = wizardi18n.getString(
                "nfs.pathPrefixEntry.desc");
        String pathPrefixEntry_help = wizardi18n.getHelpString(
                "nfs.pathPrefixEntry.help");
        String yes = wizardi18n.getString("cliwizards.yes");
        String no = wizardi18n.getString("cliwizards.no");
        String back = wizardi18n.getString("ttydisplay.back.char");
        String dchar = wizardi18n.getString("ttydisplay.d.char");
        String done = wizardi18n.getString("ttydisplay.menu.done");

        List fileSysMtptsBuff = new ArrayList();
        String fileSysMtpts[] = new String[] {};
        String optionSelected[] = null;
        String defSelection = "";
        String defAdminDir = Util.NFS_DIR_ADMIN;
        HashMap rgPropMap = new HashMap();
        Object tmpObj = null;
        HashMap selPathPrefixMap = new HashMap();
        int index = 0;
        HashMap customTags = new HashMap();
        customTags.put(dchar, done);

        // clear wizardmodel variable for displaying this panel
        wizardModel.setWizardValue(NFSWizardConstants.SHOWPATHPREFIX, "false");

        tmpObj = wizardModel.getWizardValue(Util.NFS_HASP_MTPTS);

        if (tmpObj != null)
            fileSysMtptsBuff = (ArrayList) tmpObj;

        // Get an array from the list to display it using TTYDisplay classes
        fileSysMtpts = (String[]) fileSysMtptsBuff.toArray(
                new String[fileSysMtptsBuff.size()]);

        // Get already selected path-prefix data
        tmpObj = wizardModel.getWizardValue(Util.NFS_PATHPREFIX_DATA);

        if (tmpObj != null)
            selPathPrefixMap = (HashMap) tmpObj;

        // Get already selected values from wizard model
        tmpObj = wizardModel.getWizardValue(Util.NFS_RESOURCE_GROUP +
                Util.RGPROP_TAG);

        if (tmpObj != null) {
            rgPropMap = (HashMap) tmpObj;

            tmpObj = rgPropMap.get(Util.PATHPREFIX);

            if (tmpObj != null)
                defSelection = (String) tmpObj;

            tmpObj = rgPropMap.get(Util.NFS_DIR_ADMIN);

            if (tmpObj != null)
                defAdminDir = (String) tmpObj;
        }

        // Set the default selection to first mountpoint
        if (!fileSysMtptsBuff.contains(defSelection)) {
            defSelection = (String) fileSysMtptsBuff.get(0);
        }

        // generate the full path-prefix - add admin dir to mountpoint
        defSelection = NFSWizardCreator.appendDir(defSelection, defAdminDir);


        if (selPathPrefixMap.isEmpty()) {

            for (int i = 0; i < fileSysMtpts.length; i++) {
                fileSysMtpts[i] = NFSWizardCreator.appendDir(fileSysMtpts[i],
                        Util.NFS_DIR_ADMIN);
                selPathPrefixMap.put(fileSysMtpts[i], Util.NFS_DIR_ADMIN);
            }
        } else {
            Set keys = selPathPrefixMap.keySet();
            Iterator iter = keys.iterator();
            int i = 0;

            while (iter.hasNext()) {
                fileSysMtpts[i++] = (String) iter.next();
            }
        }

        boolean flag;

        do {
            flag = true;

            // Enable Navigation
            TTYDisplay.setNavEnabled(true);
            this.cancelDirection = false;
            TTYDisplay.clearScreen();
            TTYDisplay.clear(1);

            // Title
            TTYDisplay.printTitle(title_txt);
            TTYDisplay.clear(1);
            TTYDisplay.pageText(pathPrefixEntry_desc, 4);
            TTYDisplay.clear(1);

            Vector vectData = new Vector();

            for (int ctr = 0; ctr < fileSysMtpts.length; ctr++) {
                vectData.add(fileSysMtpts[ctr]);
            }

            List defSelectionList = new ArrayList();
            defSelectionList.add(
                (new Integer(vectData.indexOf(defSelection) + 1)).toString());

            optionSelected = TTYDisplay.create2DTable(pathPrefixEntry_title,
                    new String[] {}, vectData, vectData.size(), 1, customTags,
                    pathPrefixEntry_help, null, true, defSelectionList);

            if (optionSelected == null) {
                this.cancelDirection = true;

                return;
            }

            boolean doneSelected = false;

            if ((optionSelected.length != 1) &&
                    optionSelected[1].equals(dchar)) {
                doneSelected = true;
            }

            if (doneSelected) {

                try {
                    index = Integer.valueOf(optionSelected[0]).intValue() - 1;
                } catch (NumberFormatException nfe) {
                }
            } else {

                try {
                    index = Integer.valueOf(
                            optionSelected[optionSelected.length - 1])
                        .intValue() - 1;
                } catch (NumberFormatException nfe) {
                }
            }

            if (!doneSelected) {

                // Modify PathPrefix as per user input
                defAdminDir = (String) selPathPrefixMap.get(
                        fileSysMtpts[index]);

                String data[] = new String[] {
                        (String) fileSysMtptsBuff.get(index), defAdminDir
                    };
                flag = getSelPathPrefixMap(data);

                if (!flag)
                    continue;

                defAdminDir = data[1];

                selPathPrefixMap.remove(fileSysMtpts[index]);
                fileSysMtpts[index] = NFSWizardCreator.appendDir((String)
                        fileSysMtptsBuff.get(index), defAdminDir);
                selPathPrefixMap.put(fileSysMtpts[index], defAdminDir);
                defSelection = fileSysMtpts[index];
            }

            // Set the selected mountpoint as pathprefix in wizardmodel
            rgPropMap.put(Util.PATHPREFIX,
                (String) fileSysMtptsBuff.get(index));
            rgPropMap.put(Util.NFS_DIR_ADMIN, defAdminDir);
            wizardModel.setWizardValue(Util.NFS_RESOURCE_GROUP +
                Util.RGPROP_TAG, rgPropMap);
        } while (!flag);

        // Set wizard values
        wizardModel.setWizardValue(Util.NFS_PATHPREFIX_DATA, selPathPrefixMap);

        // Set the wizard state key
        ArrayList propNames = new ArrayList();
        tmpObj = (ArrayList) wizardModel.getWizardValue(Util.WIZARD_STATE);

        if (tmpObj != null)
            propNames = (ArrayList) tmpObj;

        propNames.add(Util.PATHPREFIX);
        propNames.add(Util.NFS_DIR_ADMIN);
        wizardModel.setWizardValue(Util.WIZARD_STATE, propNames);
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#isDisplayComplete
     */
    public boolean isDisplayComplete() {
        return true;
    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }

    private boolean getSelPathPrefixMap(String data[]) {

        String mtpt = data[0];
        String path = data[1];
        boolean retVal = true;

        String continue_txt = wizardi18n.getString("cliwizards.continue");
        String mtpt_txt = wizardi18n.getString(
                "nfs.pathPrefixEntry.pathPrefixTable.colheader.0");
        String path_txt = wizardi18n.getString(
                "nfs.pathPrefixEntry.pathPrefixTable.colheader.1");
        String heading1 = wizardi18n.getString("nfs.shareOptEntry.heading1");
        String heading2 = wizardi18n.getString("nfs.shareOptEntry.heading2");
        String help_txt = wizardi18n.getString("nfs.pathPrefixEntry.help.3");
        String propNameText = wizardi18n.getString(
                "nfs.shareOptEntry.propname");
        String propDescText = wizardi18n.getString(
                "nfs.shareOptEntry.propdesc");
        String currValText = wizardi18n.getString(
                "nfs.shareOptEntry.currvalue");
        String newValText = wizardi18n.getString("nfs.shareOptEntry.newvalue");
        String title_txt = wizardi18n.getString(
                "nfs.pathPrefixEntry.cliPathPrefixTable.name");
        String done_txt = wizardi18n.getString("ttydisplay.menu.done");
        String dchar = (String) wizardi18n.getString("ttydisplay.d.char");
        String back = wizardi18n.getString("ttydisplay.back.char");
        String path_error_txt = wizardi18n.getString("nfs.pathEntry.error");
        String path_error_txt2 = wizardi18n.getString("pathEntry.error");
        String confirm_txt = wizardi18n.getString("cliwizards.enterAgain");
        String yes = wizardi18n.getString("cliwizards.yes");
        String no = wizardi18n.getString("cliwizards.no");

        Vector pathPrefixVect = new Vector();
        String subheadings[] = { heading1, heading2 };
        HashMap customTags = new HashMap();
        customTags.put(dchar, done_txt);

        while (true) {

            // Enable Navigation
            TTYDisplay.setNavEnabled(true);
            this.cancelDirection = false;
            TTYDisplay.clear(3);

            // Title
            TTYDisplay.printTitle(title_txt);
            TTYDisplay.clear(1);

            // initialize
            pathPrefixVect.removeAllElements();

            // Fill Vector
            pathPrefixVect.add(mtpt_txt);
            pathPrefixVect.add(mtpt);

            pathPrefixVect.add(path_txt);
            pathPrefixVect.add(path);

            String table_title = wizardi18n.getString(
                    "nfs.pathPrefixEntry.cliPathPrefixTable.title",
                    new String[] { NFSWizardCreator.appendDir(mtpt, path) });
            String option = TTYDisplay.create2DTable(table_title, subheadings,
                    pathPrefixVect, customTags, help_txt);

            if (option.equals(back)) {
                retVal = false;
            }

            if (option.equals(back) || option.equals(dchar)) {
                break;
            }

            TTYDisplay.clear(3);
            TTYDisplay.setNavEnabled(false);

            if (!option.equals(dchar)) {
                String prop = null;
                String currVal = null;

                if (option.equals("1")) {
                    prop = "0";
                    currVal = mtpt;
                }

                if (option.equals("2")) {
                    prop = "1";
                    currVal = path;
                }

                TTYDisplay.pageText(propNameText +
                    wizardi18n.getString(
                        "nfs.pathPrefixEntry.pathPrefixTable.colheader." + prop));

                TTYDisplay.pageText(propDescText +
                    wizardi18n.getString(
                        "nfs.pathPrefixEntry.pathPrefixTable.colheader." +
                        prop + ".desc"));
                TTYDisplay.pageText(currValText + currVal);

                if (option.equals("1")) {
                    TTYDisplay.clear(1);
                    TTYDisplay.promptForEntry(continue_txt);

                    continue;
                } else if (option.equals("2")) {

                    while (true) {
                        path = TTYDisplay.promptForEntry(newValText);
                        path = NFSWizardCreator.getCanonicalPath(path);

                        if ((path == null) || (path.trim().length() == 0)) {
                            TTYDisplay.printError(path_error_txt);

                            String confirm = TTYDisplay.getConfirmation(
                                    confirm_txt, yes, no);

                            if (!confirm.equalsIgnoreCase(yes)) {
                                break;
                            }

                            continue;
                        } else if (!(DataServicesUtil.isValidMountPoint(
                                        path))) {
                            TTYDisplay.printError(path_error_txt2);

                            continue;
                        } else {
                            break;
                        }
                    } // inner while
                } // else if
            }
        } // while

        data[1] = path;

        return retVal;
    }

}
