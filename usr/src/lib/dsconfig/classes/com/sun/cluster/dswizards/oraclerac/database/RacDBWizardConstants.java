/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)RacDBWizardConstants.java 1.16     08/05/20 SMI"
 */

package com.sun.cluster.dswizards.oraclerac.database;

/**
 * This class holds all the constants for the HAOracleWizard
 */
public interface RacDBWizardConstants {

    /*
     * Constants to be used in the wizard flow
     */
    public static final String RAC_DB_FLOW_XML =
        "/usr/cluster/lib/ds/oraclerac/database/RacDBWizardFlow.xml";


    public static final String SEL_HASP_RS = "Selected_HASP_Resources";
    public static final String SEL_HASP_RG = "Selected_HASP_ResourceGroup";
    public static final String CREATE_HASP_RS = "Create_HASP_RS";
    public static final String SEL_LH_RS = "Selected_LH_Resource";
    public static final String SEL_LH_RG = "Selected_LH_ResourceGroup";
    public static final String CREATE_LH_RS = "Create_LH_RS";
    public static final String SHOW_ALL = "ShowAll";
    public static final String SERVER_ONLY = "1";
    public static final String LISTENER_ONLY = "2";
    public static final String SERVER_AND_LISTENER = "3";
    public static final String SERVER_RS_NAME = "server_resource_name";
    public static final String LISTENER_RS_NAME = "listener_resource_name";
    public static final String HASP_RS_NAME = "hastorageplus_resource_name";
    public static final String LH_RS_NAME = "logicalhost_resource_name";
    public static final String NEW_LH_RS = "New_LogicalHost_Resource";
    public static final String NEW_HASP_RS = "New_HAStoragePlus_Resource";
    public static final String NEW_RG = "New_ResourceGroup";
    public static final String NEW_LH_RES_FLAG = "New_LH_Res_Flag";
    public static final String NEW_LH_RES_FLAG_STATE = "New_LH_Res_Flag_State";
    public static final String ORAC_PROPS_ENTRY = "RacPropsEntry";
    public static final String ORAC_PROPS_ENTRY_CTR = "RacPropsEntryCtr";

    public static final String PANEL_0 = "racDBIntroPanel";
    public static final String PANEL_1 = "racDBVersionPanel";
    public static final String PANEL_2 = "racDBPreferencePanel";
    public static final String PANEL_3 = "racDBHomeSelectionPanel";
    public static final String PANEL_4 = "racDBSidSelectionPanel";
    public static final String PANEL_5 = "racDBPropsReviewPanel";

    public static final String PANEL_6 = "racDBStorageSelectionPanel";
    public static final String PANEL_7 = "racDBLogicalHostSelectionPanel";
    public static final String PANEL_8 = "racDBReviewPanel";
    public static final String PANEL_9 = "racDBSummaryPanel";
    public static final String RESULTS_PANEL = "racDBResultPanel";
    public static final String STORAGE_PANEL = "racDBStorageSelectionPanel";
    public static final String PANEL_10 = "racDBPropsEntryPanel";
    public static final String PANEL_11 = "racDBLogicalHostEntryPanel";

    public static final String ORACLE_PKG_NAME = "SUNWscor";

    public static final String VER10G_PANEL_0 = "rac10GCrsHomeSelectionPanel";
    public static final String VER10G_PANEL_1 = "rac10GDbNameSelectionPanel";
    public static final String VER10G_PANEL_2 =
        "rac10GOracleHomeSelectionPanel";
    public static final String VER10G_PANEL_3 = "rac10GOracleSidSelectionPanel";
    public static final String VER10G_PANEL_4 = "rac10GStorageSelectionPanel";
    public static final String VER10G_PANEL_5 = "rac10GReviewPanel";
    public static final String VER10G_PANEL_6 = "rac10GSummaryPanel";
    public static final String VER10G_PANEL_7 = "rac10GResultsPanel";
    public static final String VER10G_PANEL_0_1 = "rac10GCrsStorageChoicePanel";
    public static final String VER10G_PANEL_0_2 =
        "rac10GCrsStorageSelectionPanel";

    public static final String SYSTEM_DISCOVERED_CRSHOME = "1";
    public static final String OVERRIDE_CRSHOME = "2";
    public static final String SYSTEM_DISCOVERED_DBNAME = "1";
    public static final String OVERRIDE_DBNAME = "2";
    public static final String SYSTEM_DISCOVERED_ORAHOME = "1";
    public static final String OVERRIDE_ORAHOME = "2";
    public static final String SYSTEM_DISCOVERED_ORASID = "1";
    public static final String OVERRIDE_ORASID = "2";

    public static final String WIZARD_TYPE = "Wizard_Type";
    public static final String GUI_WIZARD = "1";

    public static final int LOCALPROP = 0;
    public static final int PROPNAME = 1;
    public static final int PROPVALUE = 2;
}
