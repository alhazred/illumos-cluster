/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)NFSReviewPanel.java 1.16     08/12/10 SMI"
 */


package com.sun.cluster.dswizards.nfs;


// J2SE
import com.sun.cluster.agent.dataservices.common.ErrorValue;

// CMASS
import com.sun.cluster.agent.dataservices.nfs.NFSMBean;
import com.sun.cluster.agent.dataservices.utils.DataServicesUtil;
import com.sun.cluster.agent.dataservices.utils.Util;

// CLI Wizard SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import javax.management.remote.JMXConnector;


/**
 * The Panel Class that displays RGs, Resources and other information that would
 * be created by this wizard.
 */
public class NFSReviewPanel extends WizardLeaf {


    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N wizardi18n; // Message Bundle

    private String newNFSRG = "false";
    private String newLhRes = "false";

    private ArrayList assignedResNames = null;
    private ArrayList assignedRgNames = null;


    /**
     * Creates a new instance of NFSReviewPanel
     */
    public NFSReviewPanel() {
        super();
    }

    /**
     * Creates a NFSReviewPanel Panel with the given name and tree manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public NFSReviewPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates a NFSReviewPanel Panel for either the NFS Wizard with the given
     * name and associates it with the WizardModel and WizardFlow.
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     * @param  wizardi18n  Resource Bundle
     */
    public NFSReviewPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;
        this.wizardi18n = wizardi18n;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {

        String title = wizardi18n.getString("nfs.reviewpanel.title");
        String desc1 = wizardi18n.getString("nfs.reviewpanel.desc");
        String contwiz = wizardi18n.getString("cliwizards.continue");
        String table_title = wizardi18n.getString("nfs.reviewpanel.desc.cli");
        String heading1 = wizardi18n.getString("nfs.reviewpanel.heading1");
        String heading2 = wizardi18n.getString("nfs.reviewpanel.heading2");
        String nfsrgname_txt = wizardi18n.getString(
                "nfs.reviewpanel.nfsrg.name");
        String nfsresname_txt = wizardi18n.getString(
                "nfs.reviewpanel.nfsresource.name");
        String lhresname_txt = wizardi18n.getString(
                "nfs.reviewpanel.lhresource.name");
        String haspresname_txt = wizardi18n.getString(
                "nfs.reviewpanel.haspresource.name");
        String haspMtpt_txt = wizardi18n.getString(
                "nfs.reviewpanel.haspresource.mtpt");
        String done_txt = wizardi18n.getString("ttydisplay.menu.done");
        String help_text = wizardi18n.getHelpString("nfs.reviewpanel.help");
        String dchar = (String) wizardi18n.getString("ttydisplay.d.char");
        String back = wizardi18n.getString("ttydisplay.back.char");
        String propNameText = wizardi18n.getString("nfs.reviewpanel.propname");
        String propTypeText = wizardi18n.getString("nfs.reviewpanel.proptype");
        String propDescText = wizardi18n.getString("nfs.reviewpanel.propdesc");
        String currValText = wizardi18n.getString("nfs.reviewpanel.currval");
        String newValText = wizardi18n.getString("nfs.reviewpanel.newval");
        String nfsrgunedit = wizardi18n.getString(
                "nfs.reviewpanel.nfsrgunedit");
        String lhresunedit = wizardi18n.getString(
                "nfs.reviewpanel.lhresunedit");
        String haspresunedit = wizardi18n.getString(
                "nfs.reviewpanel.haspresunedit");
        String rgname_error = wizardi18n.getString(
                "nfs.reviewpanel.rgname.error");
        String rsname_error = wizardi18n.getString(
                "nfs.reviewpanel.resourcename.error");
        String rsname_assigned = wizardi18n.getString(
                "reviewpanel.resourcename.assigned");
        String rgname_assigned = wizardi18n.getString(
                "reviewpanel.rgname.assigned");

        String confirm_txt = wizardi18n.getString("cliwizards.enterAgain");
        String yes = wizardi18n.getString("cliwizards.yes");
        String no = wizardi18n.getString("cliwizards.no");


        String subheadings[] = { heading1, heading2 };
        HashMap rgProps = (HashMap) wizardModel.getWizardValue(
                Util.NFS_RESOURCE_GROUP + Util.RGPROP_TAG);

        NFSMBean mBean = null;
        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);
        JMXConnector connector = getConnection(nodeEndPoint);
        mBean = NFSWizardCreator.getNFSMBeanOnNode(connector);

        String properties[] = null;
        Object tmpObj = null;


        // Enable Navigation
        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        boolean flag = true;

        // Review Panel Table
        Vector reviewPanelVect = new Vector();

        // PathPrefix
        String pathPrefix = (String) rgProps.get(Util.PATHPREFIX);
        tmpObj = rgProps.get(Util.NFS_DIR_ADMIN);

        if (tmpObj != null) {
            pathPrefix = NFSWizardCreator.appendDir(pathPrefix,
                    (String) tmpObj);
        }

        // Resource Group Name
        String nfsRgName = getNFSRGName(pathPrefix, mBean);

        // HA-NFS Resource Name
        String nfsResName = getNFSResName(pathPrefix, mBean);

        // LogicalHostName Resource Name
        String lhResName = getLHResName(mBean);

        int optSel = 0;

        while (flag) {
            assignedResNames = new ArrayList();
            assignedRgNames = new ArrayList();
            TTYDisplay.clearScreen();
            TTYDisplay.clear(1);

            // Title
            TTYDisplay.printTitle(title);
            TTYDisplay.clear(1);
            TTYDisplay.pageText(desc1, 4);
            TTYDisplay.clear(1);

            // initialize
            reviewPanelVect.removeAllElements();
            wizardModel.setWizardValue(NFSWizardConstants.SHOWLHREVPANEL,
                "false");

            // NFS RG Name
            reviewPanelVect.add(nfsrgname_txt);
            reviewPanelVect.add(nfsRgName);

            if (!assignedRgNames.contains(nfsRgName)) {
                assignedRgNames.add(nfsRgName);
            }

            // HA-NFS Resource
            reviewPanelVect.add(nfsresname_txt);
            reviewPanelVect.add(nfsResName);

            if (!assignedResNames.contains(nfsResName)) {
                assignedResNames.add(nfsResName);
            }

            // LogicalHost Name Resource
            reviewPanelVect.add(lhresname_txt);
            reviewPanelVect.add(lhResName);

            if (!assignedResNames.contains(lhResName)) {
                assignedResNames.add(lhResName);
            }

            // Hasp Resources
            HashMap newHaspRsrcs1 = new HashMap();
            HashMap newHaspRsrcs2 = new HashMap();
            Iterator iterkeys = null;
            Set haspWizSelKeys = null;
            List haspResNameList = new ArrayList();
            List mtptsList = new ArrayList();
            int num = 0;

            // new hasp resources
            tmpObj = wizardModel.getWizardValue(Util.NFS_HASPWIZSEL1);

            if (tmpObj != null) {
                newHaspRsrcs1 = (HashMap) tmpObj;
                newHaspRsrcs2 = (HashMap) wizardModel.getWizardValue(
                        Util.NFS_HASPWIZSEL2);
            }

            haspWizSelKeys = newHaspRsrcs1.keySet();
            iterkeys = haspWizSelKeys.iterator();

            while (iterkeys.hasNext()) {
                String haspRsName = (String) iterkeys.next();
                reviewPanelVect.add(haspresname_txt);
                reviewPanelVect.add(haspRsName);

                if (!assignedResNames.contains(haspRsName)) {
                    assignedResNames.add(haspRsName);
                }

                List tmpList = new ArrayList();
                String tmpAr[] = (String[]) newHaspRsrcs1.get(haspRsName);

                for (int ctr = 0; ctr < tmpAr.length; ctr++) {
                    tmpList.add(tmpAr[ctr]);
                }

                mtptsList.add(tmpList);
                haspResNameList.add(haspRsName);
                num++;
            }

            // hasp resources already present
            tmpObj = wizardModel.getWizardValue(Util.NFS_HASP_RID +
                    Util.NAME_TAG);

            List rsrcList = new ArrayList();

            if (tmpObj != null) {
                rsrcList = (ArrayList) tmpObj;
            }

            iterkeys = rsrcList.iterator();

            while (iterkeys.hasNext()) {
                String haspRsName = (String) iterkeys.next();
                reviewPanelVect.add(haspresname_txt);
                reviewPanelVect.add(haspRsName);

                if (!assignedResNames.contains(haspRsName)) {
                    assignedResNames.add(haspRsName);
                }

                haspResNameList.add(haspRsName);
            }

            // Display table
            HashMap customTags = new HashMap();
            customTags.put(dchar, done_txt);

            String option = TTYDisplay.create2DTable(table_title, subheadings,
                    reviewPanelVect, customTags, help_text);

            if (option.equals(back)) {
                this.cancelDirection = true;

                return;
            }

            TTYDisplay.clear(3);
            TTYDisplay.setNavEnabled(false);

            if (!option.equals(dchar)) {
                String prop = null;
                String currVal = null;

                if (option.equals("1")) {

                    // Change NFS RG name
                    prop = "nfsrg";
                    currVal = nfsRgName;

                } else if (option.equals("2")) {

                    // Change NFS RS name
                    prop = "nfsresource";
                    currVal = nfsResName;

                } else if (option.equals("3")) {

                    // Change LH Res name
                    prop = "lhresource";
                    currVal = lhResName;

                } else {

                    // Change Hasp Res Name
                    prop = "haspresource";
                    optSel = Integer.parseInt(option);
                    currVal = (String) haspResNameList.get(optSel - 4);
                }

                TTYDisplay.pageText(propNameText +
                    wizardi18n.getString("nfs.reviewpanel." + prop + ".name"));

                TTYDisplay.pageText(propDescText +
                    wizardi18n.getString("nfs.reviewpanel." + prop + ".desc"));

                TTYDisplay.pageText(propTypeText +
                    wizardi18n.getString("nfs.reviewpanel." + prop + ".type"));

                TTYDisplay.pageText(currValText + currVal);


                // Prompt for new values for each of the options
                if (option.equals("1")) {

                    if (newNFSRG.equals("false")) {

                        // User has selected an existing RG for creation of
                        // nfs resource. Here the rgname would not be
                        // editable.
                        TTYDisplay.pageText(nfsrgunedit);
                        TTYDisplay.clear(1);
                        TTYDisplay.promptForEntry(contwiz);
                    } else {
                        String tmp_nfsRgName = null;

                        while (true) {
                            tmp_nfsRgName = TTYDisplay.promptForEntry(
                                    newValText);

                            if ((tmp_nfsRgName != null) &&
                                    (tmp_nfsRgName.trim().length() != 0)) {

                                // call validate input
                                properties = new String[] {
                                        Util.RESOURCEGROUPNAME
                                    };

                                HashMap userInputs = new HashMap();
                                userInputs.put(Util.RESOURCEGROUPNAME,
                                    tmp_nfsRgName);

                                ErrorValue retVal = validateInput(properties,
                                        userInputs, null);

                                if (!retVal.getReturnValue().booleanValue()) {
                                    TTYDisplay.printError(rgname_error);

                                    String confirm = TTYDisplay.getConfirmation(
                                            confirm_txt, yes, no);

                                    if (!confirm.equalsIgnoreCase(yes)) {
                                        break;
                                    }
                                } else {
                                    // make sure that the names are unique
                                    // within the same wizard

                                    // remove the old value first
                                    assignedRgNames.remove(nfsRgName);

                                    if (assignedRgNames.contains(
                                                tmp_nfsRgName)) {
                                        TTYDisplay.printError(rgname_assigned);

                                        String confirm = TTYDisplay
                                            .getConfirmation(confirm_txt, yes,
                                                no);

                                        // add the original value back
                                        assignedRgNames.add(nfsRgName);

                                        if (!confirm.equalsIgnoreCase(yes)) {
                                            break;
                                        }
                                    } else {
                                        nfsRgName = tmp_nfsRgName;
                                        assignedRgNames.add(nfsRgName);

                                        break;
                                    }
                                }
                            }
                        } // while
                    }
                } else if (option.equals("2")) {
                    String tmp_nfsResName = null;

                    while (true) {
                        tmp_nfsResName = TTYDisplay.promptForEntry(newValText);

                        if ((tmp_nfsResName != null) &&
                                (tmp_nfsResName.trim().length() != 0)) {

                            // call validate input
                            properties = new String[] { Util.RESOURCENAME };

                            HashMap userInputs = new HashMap();
                            userInputs.put(Util.RESOURCENAME, tmp_nfsResName);
                            userInputs.put(Util.PATHPREFIX, pathPrefix);

                            String nodeList[] = (String[]) wizardModel
                                .getWizardValue(Util.RG_NODELIST);
                            userInputs.put(Util.RG_NODELIST, nodeList);

                            ErrorValue retVal = validateInput(properties,
                                    userInputs, null);
                            boolean dfstabErr = false;

                            if (!retVal.getReturnValue().booleanValue()) {

                                if (retVal.getErrorString() == null) {
                                    TTYDisplay.printError(rsname_error);
                                } else {
                                    String errArgs[] = (retVal.getErrorString())
                                        .split(Util.SEPARATOR);
                                    String dfstab_file_error = wizardi18n
                                        .getString(
                                            "nfs.reviewpanel.dfstabfile.warning.cli",
                                            errArgs);
                                    TTYDisplay.printWarning(dfstab_file_error);
                                    dfstabErr = true;
                                }

                                String confirm = TTYDisplay.getConfirmation(
                                        confirm_txt, yes, no);

                                if (!confirm.equalsIgnoreCase(yes)) {

                                    if (dfstabErr) {
                                        nfsResName = tmp_nfsResName;
                                    }

                                    break;
                                }
                            } else {
                                assignedResNames.remove(nfsResName);

                                if (assignedResNames.contains(tmp_nfsResName)) {
                                    TTYDisplay.printError(rsname_assigned);

                                    String confirm = TTYDisplay.getConfirmation(
                                            confirm_txt, yes, no);
                                    assignedResNames.add(nfsResName);

                                    if (!confirm.equalsIgnoreCase(yes)) {
                                        break;
                                    }
                                } else {
                                    nfsResName = tmp_nfsResName;
                                    assignedResNames.add(nfsResName);

                                    break;
                                }
                            }
                        }
                    } // while
                } else if (option.equals("3")) {

                    if (newLhRes.equals("false")) {

                        // User has selected an existing LH Resource. Here the
                        // lh resource name  would not be editable.
                        TTYDisplay.pageText(lhresunedit);
                        TTYDisplay.clear(1);
                        TTYDisplay.promptForEntry(contwiz);
                    } else {
                        String tmp_lhResName = null;

                        while (true) {
                            tmp_lhResName = TTYDisplay.promptForEntry(
                                    newValText);

                            if ((tmp_lhResName != null) &&
                                    (tmp_lhResName.trim().length() != 0)) {

                                // call validate input
                                properties = new String[] { Util.RESOURCENAME };

                                HashMap userInputs = new HashMap();
                                userInputs.put(Util.RESOURCENAME,
                                    tmp_lhResName);

                                ErrorValue retVal = validateInput(properties,
                                        userInputs, null);

                                if (!retVal.getReturnValue().booleanValue()) {
                                    TTYDisplay.printError(rsname_error);

                                    String confirm = TTYDisplay.getConfirmation(
                                            confirm_txt, yes, no);

                                    if (!confirm.equalsIgnoreCase(yes)) {
                                        break;
                                    }
                                } else {
                                    assignedResNames.remove(lhResName);

                                    if (assignedResNames.contains(
                                                tmp_lhResName)) {
                                        TTYDisplay.printError(rsname_assigned);

                                        String confirm = TTYDisplay
                                            .getConfirmation(confirm_txt, yes,
                                                no);
                                        assignedResNames.add(lhResName);

                                        if (!confirm.equalsIgnoreCase(yes)) {
                                            break;
                                        }
                                    } else {
                                        lhResName = tmp_lhResName;
                                        assignedResNames.add(lhResName);

                                        break;
                                    }
                                }
                            }
                        } // while
                    }
                } else {

                    // Hasp Resources
                    if ((optSel - 3) > num) {

                        // User has selected an existing Hasp Resource. Here the
                        // hasp resource name  would not be editable.
                        TTYDisplay.pageText(haspresunedit);
                        TTYDisplay.clear(1);
                        TTYDisplay.promptForEntry(contwiz);
                    } else {
                        TTYDisplay.pageText(haspMtpt_txt +
                            mtptsList.get(optSel - 4));

                        String tmp_haspResName = null;

                        while (true) {
                            tmp_haspResName = TTYDisplay.promptForEntry(
                                    newValText);

                            if ((tmp_haspResName != null) &&
                                    (tmp_haspResName.trim().length() != 0)) {

                                // call validate input
                                properties = new String[] { Util.RESOURCENAME };

                                HashMap userInputs = new HashMap();
                                userInputs.put(Util.RESOURCENAME,
                                    tmp_haspResName);

                                ErrorValue retVal = validateInput(properties,
                                        userInputs, null);

                                if (!retVal.getReturnValue().booleanValue()) {
                                    TTYDisplay.printError(rsname_error);

                                    String confirm = TTYDisplay.getConfirmation(
                                            confirm_txt, yes, no);

                                    if (!confirm.equalsIgnoreCase(yes)) {
                                        break;
                                    }
                                } else {
                                    assignedResNames.remove(currVal);

                                    if (assignedResNames.contains(
                                                tmp_haspResName)) {
                                        TTYDisplay.printError(rsname_assigned);

                                        String confirm = TTYDisplay
                                            .getConfirmation(confirm_txt, yes,
                                                no);
                                        assignedResNames.add(currVal);

                                        if (!confirm.equalsIgnoreCase(yes)) {
                                            break;
                                        }
                                    } else {
                                        newHaspRsrcs1.put(tmp_haspResName,
                                            newHaspRsrcs1.remove(currVal));
                                        newHaspRsrcs2.put(tmp_haspResName,
                                            newHaspRsrcs2.remove(currVal));
                                        assignedResNames.add(tmp_haspResName);

                                        break;
                                    }
                                }
                            }
                        } // while
                    }
                }
            } else {

                // set final values in wizardModel
                // nfs RG Name
                wizardModel.setWizardValue(Util.NFS_RESOURCE_GROUP +
                    Util.NAME_TAG, nfsRgName);

                // nfs res name
                wizardModel.setWizardValue(Util.NFS_RID + Util.NAME_TAG,
                    nfsResName);

                // logical host resname
                wizardModel.setWizardValue(Util.NFS_LH_RID + Util.NAME_TAG,
                    lhResName);

                // hasp resources
                wizardModel.setWizardValue(Util.NFS_HASPWIZSEL1, newHaspRsrcs1);
                wizardModel.setWizardValue(Util.NFS_HASPWIZSEL2, newHaspRsrcs2);

                break;
            }

            TTYDisplay.clear(3);
        }
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#isDisplayComplete
     */
    public boolean isDisplayComplete() {
        return true;
    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }

    private String getNFSRGName(String strToUse, NFSMBean mBean) {
        String nfsRgName = (String) wizardModel.getWizardValue(
                Util.NFS_RESOURCE_GROUP + Util.NAME_TAG);
        newNFSRG = (String) wizardModel.getWizardValue(
                NFSWizardConstants.NEWRGFLAG);
        strToUse = strToUse.replace('/', '-');
        strToUse = DataServicesUtil.removeSpecialChars(strToUse);

        if (nfsRgName == null) {

            // Create a new nfs rg name
            nfsRgName = NFSWizardConstants.NFS_PREFIX + strToUse;

            String properties[] = { Util.RG_NAME };
            HashMap discoveredMap = mBean.discoverPossibilities(properties,
                    nfsRgName);
            nfsRgName = (String) discoveredMap.get(Util.RG_NAME);
        }

        return nfsRgName;
    }

    private String getNFSResName(String strToUse, NFSMBean mBean) {

        HashMap userInputs = new HashMap();
        String nodeList[] = (String[]) wizardModel.getWizardValue(
                Util.RG_NODELIST);
        userInputs.put(Util.RG_NODELIST, nodeList);
        userInputs.put(Util.PATHPREFIX, strToUse);

        String nfsResName = (String) wizardModel.getWizardValue(Util.NFS_RID +
                Util.NAME_TAG);
        strToUse = strToUse.replace('/', '-');
        strToUse = DataServicesUtil.removeSpecialChars(strToUse);

        if (nfsResName == null) {

            // Create a new nfs rg name
            nfsResName = NFSWizardConstants.NFS_PREFIX + strToUse;
            userInputs.put(Util.RS_NAME, nfsResName);

            String properties[] = { Util.NFS_RID };
            HashMap discoveredMap = mBean.discoverPossibilities(properties,
                    userInputs);
            nfsResName = (String) discoveredMap.get(Util.RS_NAME);
        }

        return nfsResName;
    }

    private String getLHResName(NFSMBean mBean) {
        String lhResName = (String) wizardModel.getWizardValue(Util.NFS_LH_RID +
                Util.NAME_TAG);
        newLhRes = (String) wizardModel.getWizardValue(
                NFSWizardConstants.NEWRGFLAG);

        if (lhResName == null) {

            // Create a new LH res name.
            lhResName = (String) wizardModel.getWizardValue(Util.RESOURCENAME);

            String properties[] = { Util.RS_NAME };
            HashMap discoveredMap = mBean.discoverPossibilities(properties,
                    lhResName);
            lhResName = (String) discoveredMap.get(Util.RS_NAME);
        }

        return lhResName;
    }

    private ErrorValue validateInput(String propNames[], HashMap userInput,
        Object helperData) {
        ErrorValue errValue = null;
        NFSMBean mBean = null;
        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);
        JMXConnector connector = getConnection(nodeEndPoint);
        mBean = NFSWizardCreator.getNFSMBeanOnNode(connector);
        errValue = mBean.validateInput(propNames, userInput, helperData);

        return errValue;
    }
}
