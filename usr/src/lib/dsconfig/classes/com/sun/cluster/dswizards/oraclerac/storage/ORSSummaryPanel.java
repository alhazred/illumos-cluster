/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)ORSSummaryPanel.java	1.21	09/03/11 SMI"
 */

package com.sun.cluster.dswizards.oraclerac.storage;

// CMASS
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.zonecluster.ZoneClusterData;

// CLISDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliUtil;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.oraclerac.invoker.RACInvokerCreator;

import com.sun.cluster.model.RACModel;
import com.sun.cluster.model.ZoneClusterModel;
import com.sun.cluster.model.DataModel;

import java.util.HashMap;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

/**
 * Summary Panel for Oracle RAC Storage Wizard
 */

/*
 * In this panel we display the configuration information for RAC Storage
 *
 * We display the
 *       Framework ResourceGroup
 *       Framework Nodelist
 *       and the Storage Resources that would be created for RAC
 */
public class ORSSummaryPanel extends WizardLeaf {

    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N messages; // Message Bundle

    private RACModel racModel = null;

    /**
     * Creates a new instance of ORSSummaryPanel
     */
    public ORSSummaryPanel() {
        super();
    }

    /**
     * Creates an ORSSummary Panel with the given name and tree manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public ORSSummaryPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates an ORSSummary Panel for racstorage Wizard with the given name and
     * associates it with the WizardModel and WizardFlow
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     */
    public ORSSummaryPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N messages) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;

        // Get the I18N instance
        this.messages = messages;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {

        String title = messages.getString("orf.summarypanel.title");
        String racFrameworkRG = messages.getString(
                "racstorage.summaryPanel.racFrameworkRG");
        String racFrameworkNodes = messages.getString(
                "racstorage.summaryPanel.racFrameworkNodes");
        String storageResources = messages.getString(
                "racstorage.summaryPanel.storageResources");
        String succesMessage = messages.getString(
                "racstorage.summaryPanel.successGenerateCommand");
        String cchar = messages.getString("ttydisplay.c.char");
        String qchar = messages.getString("ttydisplay.q.char");
        String quit = messages.getString("ttydisplay.menu.quit");
        String createConfigurationText = messages.getString(
                "ttydisplay.menu.createConfiguration");
        String property = messages.getString("racstorage.reviewPanel.property");
        String value = messages.getString("racstorage.reviewPanel.value");
        String help = messages.getString("hasp.summarypanel.cli.help");
        String desc = messages.getString("racstorage.summaryPanel.desc");
        String racRGDesc = messages.getString("racstorage.summaryPanel.racRG");
        String racNodeListDesc = messages.getString(
                "racstorage.summaryPanel.racNodeList");

        String nodeList[] = (String[]) wizardModel.getWizardValue(
                Util.RG_NODELIST);

	String baseClusterNodeList[] = (String[])wizardModel.getWizardValue(
		Util.BASE_CLUSTER_NODELIST);

        Vector summaryData = new Vector();

        // TODO: Remove hardcoding of the RACFramework RG and integrate with
        // the RACFramework wizard

        summaryData.add(racFrameworkRG);
        summaryData.add((String) wizardModel.getWizardValue(
                Util.ORF_RESOURCE_GROUP + Util.NAME_TAG));
        summaryData.add(racFrameworkNodes);

        StringBuffer buffer = new StringBuffer();

        for (int j = 0; j < nodeList.length; j++) {
            buffer.append((String) nodeList[j]);

            if (j < (nodeList.length - 1)) {
                buffer.append(",");
            }
        }
        summaryData.add(buffer.toString());

        Map selectedDGRes = (HashMap) wizardModel.getWizardValue(
                Util.SELECTED_DG_RESOURCES);
        Map selectedFSRes = (HashMap) wizardModel.getWizardValue(
                Util.SELECTED_FS_RESOURCES);
        StringBuffer zAllResources = null;

        if (selectedDGRes != null) {
            Set diskResources = selectedDGRes.keySet();
            String selDGResourceArr[] = (String[]) diskResources.toArray(
                    new String[0]);

            if (zAllResources == null) {
                zAllResources = new StringBuffer();
            }

            zAllResources.append(Util.convertArraytoString(selDGResourceArr));
        }

        if (selectedFSRes != null) {
            Set fsResources = selectedFSRes.keySet();
            String selFSResourceArr[] = (String[]) fsResources.toArray(
                    new String[0]);

            if (zAllResources == null) {
                zAllResources = new StringBuffer();
            } else {
                zAllResources.append(Util.COMMA);
            }

            zAllResources.append(Util.convertArraytoString(selFSResourceArr));
        }

	String waitZCBootRSName = (String)wizardModel.getWizardValue(
		Util.WAIT_ZC_BOOT_RESOURCE);
	if (waitZCBootRSName != null) {
	    if (zAllResources == null) {
		zAllResources = new StringBuffer();
	    } else {
		zAllResources.append(Util.COMMA);
	    }
	    zAllResources.append(waitZCBootRSName);
	}	

        summaryData.add(storageResources);

        if (zAllResources != null) {
            summaryData.add(zAllResources.toString());
        } else {
            summaryData.add("");
        }

	String selZoneCluster = CliUtil.getZoneClusterName(wizardModel);

	String keyPrefix = (String) wizardModel.getWizardValue(Util.KEY_PREFIX);
	// Depending on the Storage Schemes get the respective
	// Resources and the DeviceGroups monitored by them.
	List storageScheme = (ArrayList) wizardModel.getWizardValue(
	    keyPrefix + Util.ORF_STORAGE_MGMT_SCHEMES);


        HashMap customTags = new HashMap();
        customTags.put(cchar, createConfigurationText);

        String subheadings[] = { property, value };

        do {
            TTYDisplay.clearScreen();
            TTYDisplay.clear(1);

            // Enable Back and Help
            TTYDisplay.setNavEnabled(true);
            this.cancelDirection = false;

            // Title and Description
            TTYDisplay.printTitle(title);
            TTYDisplay.clear(1);
            TTYDisplay.pageText(desc);
            TTYDisplay.clear(1);

            String option[] = TTYDisplay.create2DTable("", subheadings,
                    summaryData, summaryData.size() / 2, 2, customTags, help,
                    "", true);

            // Handle Back
            if (option == null) {
                this.cancelDirection = true;

                return;
            }

            try {
                int optionValue = Integer.parseInt(option[0]);

                switch (optionValue) {

                case 1:

                    // Display Framework RG Description
                    TTYDisplay.clear(1);
                    TTYDisplay.pageText(racRGDesc);
                    TTYDisplay.clear(1);
                    TTYDisplay.promptForEntry(messages.getString(
                            "cliwizards.continue"));

                    break;

                case 2:

                    // Display Framework Nodelist Description
                    TTYDisplay.clear(1);
                    TTYDisplay.pageText(racNodeListDesc);
                    TTYDisplay.clear(1);
                    TTYDisplay.promptForEntry(messages.getString(
                            "cliwizards.continue"));

                    break;

                case 3:

                    // Take back to the "Sun Cluster Objects Panel
                    this.cancelDirection = true;

                    return;
                }
            } catch (NumberFormatException nfe) {

                // Generate Commands
                if (option[0].equals(cchar)) {

                    Map confMap = new HashMap();

                    // Fill the structure for RAC Framework RG  that is already
                    // created so that commands are not generated

		    // if configuring on a zone cluster and if storage scheme is
		    // shared QFS with SVM or SVM or VxVM
		    // also need to pass the framework RG in the base cluster

		    if (selZoneCluster != null &&
			selZoneCluster.trim().length() > 0) {
			if (storageScheme != null &&
			    storageScheme.contains(Util.SVM) ||
			    storageScheme.contains(Util.QFS_SVM) ||
			    storageScheme.contains(Util.VXVM)) {

			    racModel = RACInvokerCreator.getRACModel();
			    Map bcRACInfoMap = null;
			    if (racModel != null) {
				String nodeEndPoint = (String) wizardModel.
				    getWizardValue(Util.NODE_TO_CONNECT);

				bcRACInfoMap = racModel.discoverPossibilities(
				    null, nodeEndPoint,
				    new String[] { Util.RAC_RG_NODES }, null);
			    }

			    confMap.put(Util.ORF_RESOURCE_GROUP + Util.NAME_TAG +
				Util.BC_TAG, bcRACInfoMap.get(Util.RAC_FRAMEWORK_RGNAME));

			    if (storageScheme.contains(Util.SVM)) {
				confMap.put(Util.ORF_SVM_RID + Util.NAME_TAG,
				    Util.GLOBAL_PREFIX + bcRACInfoMap.get(Util.ORF_SVM_RID));
			    } else if (storageScheme.contains(Util.QFS_SVM)) {
				confMap.put(Util.ORF_SVM_RID + Util.NAME_TAG,
				    bcRACInfoMap.get(Util.ORF_SVM_RID));
			    }

			    if (storageScheme.contains(Util.VXVM)) {
				confMap.put(Util.ORF_VXVM_RID + Util.NAME_TAG,
				    Util.GLOBAL_PREFIX + bcRACInfoMap.get(
				    Util.ORF_VXVM_RID));
			    }

			}

			// pass the zone cluster path
			DataModel dm = DataModel.getDataModel(null);
			ZoneClusterModel zm = dm.getZoneClusterModel();
			ZoneClusterData zcData = null;
			try {
			    zcData = zm.getZoneClusterData(
				null, selZoneCluster);

			    String zonePath = zcData.getZonePath();
			    confMap.put(Util.ZONE_PATH, zonePath);
			} catch(Exception e) {
			}
		    }

                    confMap.put(Util.ORF_RESOURCE_GROUP + Util.NAME_TAG,
                        wizardModel.getWizardValue(
                            Util.ORF_RESOURCE_GROUP + Util.NAME_TAG));
                    confMap.put(Util.ORF_RID + Util.NAME_TAG,
                        wizardModel.getWizardValue(
                            Util.ORF_RID + Util.NAME_TAG));

                    String racUDLMResource = (String) wizardModel
                        .getWizardValue(Util.ORF_UDLM_RID + Util.NAME_TAG);

                    if (racUDLMResource != null) {
                        confMap.put(Util.ORF_UDLM_RID + Util.NAME_TAG,
                            racUDLMResource);
                    }

                    String racSVMResource = (String) wizardModel.getWizardValue(
                            Util.ORF_SVM_RID + Util.NAME_TAG);
                    String racCVMResource = (String) wizardModel.getWizardValue(
                            Util.ORF_VXVM_RID + Util.NAME_TAG);

                    if (racSVMResource != null) {
                        confMap.put(Util.ORF_SVM_RID + Util.NAME_TAG,
                            racSVMResource);
                    }

                    if (racCVMResource != null) {
                        confMap.put(Util.ORF_VXVM_RID + Util.NAME_TAG,
                            racCVMResource);
                    }

                    // Fill the structure for configuring the Storage Resources

                    // Fill the structure with the selected resource names
                    // and their information
                    Map storageResourcesMap = new HashMap();
                    Map selectedDGResources = (HashMap) wizardModel
                        .getWizardValue(Util.SELECTED_DG_RESOURCES);
                    Map selectedFSResources = (HashMap) wizardModel
                        .getWizardValue(Util.SELECTED_FS_RESOURCES);

                    // Add the RG Nodelist (Same as framework)
                    storageResourcesMap.put(Util.NODELIST, nodeList);

		    // Add the BASE_CLUSTER NodeList
		    storageResourcesMap.put(Util.BASE_CLUSTER_NODELIST, baseClusterNodeList);

                    // Add DiskResources and their information
                    if (selectedDGResources != null) {
			// set of resources for device groups. For ZC the name is
			// of the form zc:Rname
                        Set diskResources = selectedDGResources.keySet();
                        storageResourcesMap.put(Util.SELECTED_DG_RESOURCES,
                            diskResources.toArray(new String[0]));

                        for (Iterator i = diskResources.iterator();
                                i.hasNext(); /* */) {
                            String diskResource = (String) i.next();
                            storageResourcesMap.put(diskResource,
                                wizardModel.getWizardValue(diskResource));
                        }
                    }

                    // Add FileSystem Resources and their information
                    if (selectedFSResources != null) {
                        Set fsResources = selectedFSResources.keySet();
                        storageResourcesMap.put(Util.SELECTED_FS_RESOURCES,
                            fsResources.toArray(new String[0]));

                        for (Iterator i = fsResources.iterator();
                                i.hasNext(); /* */) {
                            String fsResource = (String) i.next();
                            storageResourcesMap.put(fsResource,
                                wizardModel.getWizardValue(fsResource));
                        }
                    }

                    confMap.put(Util.STORAGERESOURCE, storageResourcesMap);
		    confMap.put(Util.WAIT_ZC_BOOT_RESOURCE, (String)
			wizardModel.getWizardValue(Util.WAIT_ZC_BOOT_RESOURCE));
		    confMap.put(Util.WAIT_ZC_BOOT_GROUP, (String)
			wizardModel.getWizardValue(Util.WAIT_ZC_BOOT_GROUP)); 
		    confMap.put(Util.SEL_ZONE_CLUSTER, selZoneCluster);
                    generateCommands(confMap);

                    return;
                }
            }
        } while (true);
    }

    /**
     * Generates the commands by invoking the command generator
     */
    private boolean generateCommands(Map confMap) {
        List commandList = null;
        Thread t = null;
        boolean retVal = true;

        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);

	racModel = RACInvokerCreator.getRACModel();
        try {
            TTYDisplay.clear(1);
            t = TTYDisplay.busy(messages.getString(
                        "cliwizards.executingMessage"));
            t.start();
            commandList = racModel.generateCommands(null, nodeEndPoint, confMap);
        } catch (CommandExecutionException cee) {
            wizardModel.setWizardValue(Util.EXITSTATUS, cee);
            commandList = racModel.getCommandList(null, nodeEndPoint);
            retVal = false;
        } finally {
            wizardModel.setWizardValue(Util.CMDLIST, commandList);

            try {
                t.interrupt();
                t.join();
            } catch (Exception exe) {
            }
        }

        return retVal;
    }
}
