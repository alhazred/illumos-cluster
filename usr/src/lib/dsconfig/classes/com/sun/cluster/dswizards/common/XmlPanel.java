/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)XmlPanel.java	1.8	08/07/14 SMI"
 */

package com.sun.cluster.dswizards.common;

import java.util.LinkedHashMap;


public class XmlPanel {
    String name;
    String nextNames[];
    int isSubPanel;
    DSWizardInterface flowClass = null;

    LinkedHashMap nextPanels = null;

    XmlPanel() {
    }

    public String toString() {
	StringBuffer buffer = new StringBuffer();
	buffer.append("--------------XML Panel --------------");
	buffer.append("\n");
	buffer.append("Name : "+name);
	buffer.append("\n");
	buffer.append("Next Names : ");
	buffer.append("\n");
	if (nextNames != null) {
	    for (int i = 0; i < nextNames.length; i++) {
		buffer.append(nextNames[i]);
		if (i != nextNames.length -1) {
		    buffer.append(",");
		}
	    }
	}
	buffer.append("\n");
	buffer.append("flowClass : "+flowClass);
	buffer.append("\n");
	buffer.append("Next panels : "+ nextPanels);
	buffer.append("\n");
	return buffer.toString();
    }
}
