/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)HASPWizardConstants.java 1.18     08/07/23 SMI"
 */

package com.sun.cluster.dswizards.hastorageplus;

/**
 * This class holds all the constants for the HASPWizard
 */
public class HASPWizardConstants {

    /*
     * Constants to be used in the wizard flow
     */
    public static final String HASP_LOCATION_PREF = "HASP_Location_Preference";
    public static final String WIZARDI18N = "WIZARDI18N";
    public static final String HASP_FILESYSTEMS_SELECTED = "Filesystems";
    public static final String SQFS_SELECTED = "SQFSFilesystem";
    public static final String HASP_DEVICES_SELECTED = "Devices";
    public static final String HASP_CREATE_FILESYSTEM = "CreateNewFS";
    public static final String HASP_CREATE_FS_MOUNTPOINT = "CreateFSMountPoint";
    public static final String HASP_CREATE_FS_DEVPATH = "CreateFSDevicePath";
    public static final String HASP_CREATE_FS_GLOBALOPTION =
        "CreateFSGlobalOption";
    public static final String HASP_CREATE_FS_MOUNTOPTION =
        "CreateFSMountOption";
    public static final String HASP_CREATE_FS_FSCKEARLY = "CreateFSCKEarly";
    public static final String HASP_CREATE_FS_TYPE = "CreateFSType";
    public static final String AVAIL_NODELIST = "available_nodes";
    public static final String SELECT_OR_CREATE_FS = "select_or_create";
    public static final String NEWLY_ADDED_MTPT = "mtpt_newly_added";
    public static final String SELECT_EXISTING = "1";
    public static final String CREATE_NEW = "2";
    public static final String FS_ONLY = "1";
    public static final String DG_ONLY = "2";
    public static final String FS_AND_DG = "3";
    public static final String ZONES_PRESENT = "zones_present";
    public static final String YES = "1";
    public static final String NO = "0";
    public static final String PKG_NAME = "SUNWscmasa";
    public static final String PREV_RG_NODELIST = "PREV_RG_NODELIST";

    public static final int OTHER_FS_SELECTED = 1;
    public static final int QFS_FS_SELECTED = 2;
    public static final int BOTH_FS_SELECTED = 3;


// Panel names
    public static final String PANEL_0 = "haspIntroPanel";
    public static final String PANEL_1 = "haspNodeSelectionPanel";
    public static final String PANEL_2 = "haspLocationPanel";
    public static final String PANEL_3 = "fileSystemSelectionPanel";
    public static final String PANEL_4 = "createFileSystemPanel";
    public static final String PANEL_5 = "createFileSystemReviewPanel";
    public static final String PANEL_6 = "deviceGroupSelectionPanel";
    public static final String PANEL_7 = "haspReviewPanel";
    public static final String PANEL_8 = "haspSummaryPanel";
    public static final String PANEL_9 = "haspAdvPropPanel";
    public static final String PANEL_10 = "haspResultsPanel";

}
