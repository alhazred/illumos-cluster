/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)HASPWizardCreator.java 1.19     08/05/20 SMI"
 */

package com.sun.cluster.dswizards.hastorageplus;

import com.sun.cacao.agent.JmxClient;

import com.sun.cluster.agent.dataservices.hastorageplus.HAStoragePlusMBean;
import com.sun.cluster.agent.dataservices.utils.*;
import com.sun.cluster.common.TrustedMBeanModel;
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardComposite;
import com.sun.cluster.dswizards.clisdk.core.WizardCreator;
import com.sun.cluster.dswizards.common.WizardFlow;

import java.io.IOException;

import java.text.MessageFormat;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.management.remote.JMXConnector;


/**
 * This class holds the Wizard Panel Tree information for HAStoragePlus Wizard.
 * This class creates the HAStoragePlus wizard from the State file and
 * WizardFlow File.
 */
public class HASPWizardCreator extends WizardCreator {


    private static final String FLOW_XML =
        "/usr/cluster/lib/ds/hastorageplus/HASPWizardFlow.xml";
    private static final String STATE_FILE =
        "/opt/cluster/lib/ds/history/HASPWizardState";
    private static HAStoragePlusMBean mBean = null;
    private static JMXConnector localConnection = null;

    /**
     * Default Constructor Creates  HASP Wizard from the State File. The Wizard
     * is intialized to a particular state using the State file. The Wizard flow
     * is governed by the Flow XML.
     */
    public HASPWizardCreator() {
        super(STATE_FILE, FLOW_XML);
    }

    /**
     * Creates the actual Panel Tree Structure for the HASP Wizard
     */
    protected void createClientTree() {

        // Get the wizard root
        WizardComposite haspWizardRoot = getRoot();

        // Create the Individual Panels of the Wizard

        // Each Panel is associated with a Name, the wizardModel from
        // the WizardCreator and the WizardFlow Structure.
        HASPIntroPanel haspIntroPanel = new HASPIntroPanel(
                HASPWizardConstants.PANEL_0, wizardModel, wizardFlow,
                wizardi18n);

        HASPNodeSelectionPanel haspNodeSelectionPanel =
            new HASPNodeSelectionPanel(HASPWizardConstants.PANEL_1, wizardModel,
                wizardFlow, wizardi18n);

        HASPLocationPanel locationPanel = new HASPLocationPanel(
                HASPWizardConstants.PANEL_2, wizardModel, wizardFlow,
                wizardi18n);

        HASPFileSystemSelectionPanel fileSystemSelectionPanel =
            new HASPFileSystemSelectionPanel(HASPWizardConstants.PANEL_3,
                wizardModel, wizardFlow, wizardi18n);

        HASPCreateFileSystemPanel createFileSystemPanel =
            new HASPCreateFileSystemPanel(HASPWizardConstants.PANEL_3 +
                WizardFlow.HYPHEN + HASPWizardConstants.PANEL_4, wizardModel,
                wizardFlow, wizardi18n);

        HASPCreateFileSystemReviewPanel createFileSystemReviewPanel =
            new HASPCreateFileSystemReviewPanel(HASPWizardConstants.PANEL_3 +
                WizardFlow.HYPHEN + HASPWizardConstants.PANEL_5, wizardModel,
                wizardFlow, wizardi18n);

        HASPDeviceGroupSelectionPanel deviceGroupSelectionPanel =
            new HASPDeviceGroupSelectionPanel(HASPWizardConstants.PANEL_6,
                wizardModel, wizardFlow, wizardi18n);

        HASPReviewPanel reviewPanel = new HASPReviewPanel(
                HASPWizardConstants.PANEL_7, wizardModel, wizardFlow,
                wizardi18n);

        HASPSummaryPanel summaryPanel = new HASPSummaryPanel(
                HASPWizardConstants.PANEL_8, wizardModel, wizardFlow,
                wizardi18n);

        HASPResultsPanel resultsPanel = new HASPResultsPanel(
                HASPWizardConstants.PANEL_10, wizardModel, wizardFlow,
                wizardi18n);

        // Add the Panels to the Root
        haspWizardRoot.addChild(haspIntroPanel);
        haspWizardRoot.addChild(haspNodeSelectionPanel);
        haspWizardRoot.addChild(locationPanel);
        haspWizardRoot.addChild(fileSystemSelectionPanel);
        haspWizardRoot.addChild(createFileSystemPanel);
        haspWizardRoot.addChild(createFileSystemReviewPanel);
        haspWizardRoot.addChild(deviceGroupSelectionPanel);
        haspWizardRoot.addChild(reviewPanel);
        haspWizardRoot.addChild(summaryPanel);
        haspWizardRoot.addChild(resultsPanel);
    }

    /**
     * Get the Handle to the HAStoragePlus MBean
     *
     * @return  Handle to the HAStoragePlus MBean
     */
    public static HAStoragePlusMBean getHAStoragePlusMBean(
        JMXConnector connector) {

        if (mBean == null) {
            mBean = getHAStoragePlusMBeanOnNode(connector);
        }

        return mBean;
    }

    /**
     * Get the Handle to the HAStoragePlus MBean on a node
     *
     * @return  Handle to the HAStoragePlus MBean on a node
     */
    public static HAStoragePlusMBean getHAStoragePlusMBeanOnNode(
        JMXConnector connector) {
        HAStoragePlusMBean mBeanOnNode = null;

        // Get Handler to HAStoragePlusMBean MBean.
        try {
            TTYDisplay.initialize();
            mBeanOnNode = (HAStoragePlusMBean) TrustedMBeanModel.getMBeanProxy(
                    Util.DOMAIN, connector, HAStoragePlusMBean.class, null,
                    false);
        } catch (Exception e) {

            // Report Error that the user has to install
            // The Dataservice Package or upgrade it
            String errorMsg = pkgErrorString;
            MessageFormat msgFmt = new MessageFormat(errorMsg);
            errorMsg = msgFmt.format(
                    new String[] { HASPWizardConstants.PKG_NAME });
            TTYDisplay.printError(errorMsg);
            TTYDisplay.promptForEntry(wizardExitPrompt);
            System.exit(1);
        }

        return mBeanOnNode;
    }

    /**
     * Get the Handle to the HAStoragePlus MBean
     *
     * @deprecated
     *
     * @return  Handle to the HAStoragePlus MBean
     */
    public static HAStoragePlusMBean getHAStoragePlusMBean() {

        // Only if there is no existing reference
        if (mBean == null) {

            // Get Handler to HAStoragePlusMBean MBean.
            try {
                TTYDisplay.initialize();
                mBean = (HAStoragePlusMBean) TrustedMBeanModel.getMBeanProxy(
                        Util.DOMAIN, Util.getClusterEndpoint(),
                        HAStoragePlusMBean.class, null, false);
            } catch (Exception e) {

                // Report Error that the user has to install
                // The Dataservice Package or upgrade it
                String errorMsg = pkgErrorString;
                MessageFormat msgFmt = new MessageFormat(errorMsg);
                errorMsg = msgFmt.format(
                        new String[] { HASPWizardConstants.PKG_NAME });
                TTYDisplay.printError(errorMsg);
                TTYDisplay.promptForEntry(wizardExitPrompt);
                System.exit(1);
            }
        }

        return mBean;
    }

    /**
     * Main function of the Wizards
     */
    public static void main(String args[]) {

        // Start the HAStoragePlus Wizard
        HASPWizardCreator lh = new HASPWizardCreator();
    }
}
