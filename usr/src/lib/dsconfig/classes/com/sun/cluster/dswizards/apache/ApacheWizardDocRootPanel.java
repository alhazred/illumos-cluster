/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ApacheWizardDocRootPanel.java 1.16     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.apache;

// J2SE
import com.sun.cluster.agent.dataservices.apache.Apache;

// CMASS
import com.sun.cluster.agent.dataservices.apache.ApacheMBean;
import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.hastorageplus.HAStoragePlusMBean;
import com.sun.cluster.agent.dataservices.logicalhost.LogicalHost;
import com.sun.cluster.agent.dataservices.sharedaddress.SharedAddress;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.rgm.ResourceMBean;
import com.sun.cluster.common.TrustedMBeanModel;

// CLI SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliStorageSelectionPanel;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;

// Wizard Common
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.hastorageplus.HASPWizardConstants;
import com.sun.cluster.dswizards.hastorageplus.HASPWizardCreator;
import com.sun.cluster.dswizards.logicalhost.LogicalHostWizardConstants;
import com.sun.cluster.dswizards.logicalhost.LogicalHostWizardCreator;
import com.sun.cluster.dswizards.sharedaddress.SharedAddressWizardCreator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.management.AttributeValueExp;
import javax.management.Query;
import javax.management.QueryExp;
import javax.management.remote.JMXConnector;


/**
 * Panel to get the Apache Doc Root
 */
public class ApacheWizardDocRootPanel extends WizardLeaf {

    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N messages; // Message Bundle

    private JMXConnector connection = null;
    private ApacheMBean mBean = null; // Reference to MBean

    /**
     * Creates a new instance of ApacheWizardDocRootPanel
     */
    public ApacheWizardDocRootPanel() {
        super();
    }

    /**
     * Creates an ApacheWizardDocRoot Panel with the given name and tree manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public ApacheWizardDocRootPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates a ApacheWizardDocRoot Panel with the given name and associates it
     * with the WizardModel, WizardFlow and i18n reference
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     * @param  messages  WizardI18N
     */
    public ApacheWizardDocRootPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N messages) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;

        // Get the I18N instance
        this.messages = messages;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {

        // Title
        String title = messages.getString("apache.docRootPanel.title");

        // Query Strings
        String queryText = messages.getString("apache.docRootPanel.query");
        String enterOwn = messages.getString("apache.docRootPanel.enterOwn");
        String docRootLocationQuery = messages.getString(
                "apache.docRootPanel.enterDocRoot");

        // Help messages
        String helpText = messages.getString("apache.docRootPanel.clihelp");
        String backOption = messages.getString("ttydisplay.back.char");
        String cchar = messages.getString("ttydisplay.c.char");
        String confirmMessage = messages.getString("cliwizards.confirmMessage");
        String yes = messages.getString("cliwizards.yes");
        String no = messages.getString("cliwizards.no");
        String confirmDocRoot = messages.getString(
                "apache.docRootPanel.confirm");
        String confirmSelection = messages.getString(
                "cliwizards.selectionMessage");
        String enterNewDocRoot = messages.getString(
                "apache.docRootPanel.newDocRoot");
        String selectAChoice = messages.getString(
                "apache.docRootPanel.selectChoice");

        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);

        // Get the ApacheMbean reference
        if (mBean == null) {
            connection = getConnection(nodeEndPoint);
            mBean = ApacheWizardCreator.getApacheMBeanOnNode(connection);
        }

        String propertyNames[] = null;
        boolean defaultDocRootPresent = true;
        String defaultDocRoot = (String) wizardModel.getWizardValue(
                Util.APACHE_DOC);

        String docRoot = "";
        String docRootError = null;
        String storageResource = null;

        String choices[];
        String haspMountPoint = null;
        boolean fromHAStorageWizard = false;
        boolean docRootOk = false;
        boolean displayFSTable = false;

        if (defaultDocRoot == null) {

            // Discover APACHE_DOC from the httpd.conf file
            // Get user entered httpd.conf file path
            String confFilePath = (String) wizardModel.getWizardValue(
                    Util.APACHE_CONF_FILE);
            propertyNames = new String[] { Util.APACHE_DOC };

            HashMap discoveredValue = mBean.discoverPossibilities(propertyNames,
                    confFilePath);
            String defaultApacheDocRoot[] = (String[]) discoveredValue.get(
                    Util.APACHE_DOC);

            if ((defaultApacheDocRoot == null) ||
                    (defaultApacheDocRoot.length == 0)) {
                defaultDocRootPresent = false;
            } else {
                defaultApacheDocRoot[0] = defaultApacheDocRoot[0].replace('\"',
                        ' ');
                defaultApacheDocRoot[0] = defaultApacheDocRoot[0].trim();
                defaultDocRoot = defaultApacheDocRoot[0];
            }
        }

        // Get the storage resources/mountpoints from the apache mbean
        // depending on the apache mode and the nodelist
        propertyNames = new String[] { Util.STORAGE_RESOURCES };

        String apacheMode = (String) wizardModel.getWizardValue(
                Util.APACHE_MODE);
        HashMap helperMap = new HashMap();
        helperMap.put(Util.APACHE_MODE, apacheMode);
        helperMap.put(Util.RG_NODELIST,
            wizardModel.getWizardValue(Util.RG_NODELIST));

        HashMap storageResources = mBean.discoverPossibilities(propertyNames,
                helperMap);
        wizardModel.setWizardValue(ApacheWizardConstants.APACHE_STORAGE_RESOURCES,
            storageResources);

        while (!docRootOk && !displayFSTable && (haspMountPoint == null)) {

            // Enable Back and Help
            TTYDisplay.setNavEnabled(true);
            this.cancelDirection = false;

            // Title
            TTYDisplay.printTitle(title);
            TTYDisplay.clear(1);

            if (!defaultDocRootPresent) {
                docRoot = TTYDisplay.promptForEntry(docRootLocationQuery,
                        helpText);

                if (docRoot.equals(backOption)) {
                    this.cancelDirection = true;

                    return;
                }
            } else {
                String existError = isDocRootPresent(defaultDocRoot);
                storageResource = getStorageResourceForMount(storageResources,
                        defaultDocRoot);

                if ((existError != null) || (storageResource == null)) {

                    if (existError != null) {
                        String defaultDocRootNotExist = messages.getString(
                                "apache.docRootPanel.defaultDocRoot.notExist",
                                new Object[] { defaultDocRoot, nodeEndPoint });
                        TTYDisplay.printError(defaultDocRootNotExist);
                        docRoot = TTYDisplay.promptForEntry(
                                docRootLocationQuery, helpText);

                        if (docRoot.equals(backOption)) {
                            this.cancelDirection = true;

                            return;
                        }
                    } else if (storageResource == null) {
                        String defaultDocRootNotHA = messages.getString(
                                "apache.docRootPanel.defaultDocRoot.notHA",
                                new Object[] {
                                    defaultDocRoot, apacheMode.toLowerCase()
                                });
                        TTYDisplay.printInfo(defaultDocRootNotHA);

                        String copyDocRootFilesToHA = messages.getString(
                                "apache.docRootPanel.copyDocRoot",
                                new Object[] { defaultDocRoot });
                        String choiceTableOptions[] = {
                                copyDocRootFilesToHA, enterNewDocRoot
                            };
                        int choice = TTYDisplay.getMenuOption(selectAChoice,
                                choiceTableOptions, 0, helpText);

                        if (choice == 0) {
                            wizardModel.setWizardValue(
                                ApacheWizardConstants.DISPLAY_FS_TABLE,
                                new Boolean(true));
                            wizardModel.setWizardValue(Util.APACHE_DOC,
                                defaultDocRoot);

                            return;
                        } else if (choice == -1) {
                            this.cancelDirection = true;

                            return;
                        } else {
                            defaultDocRootPresent = false;
                            docRootOk = false;

                            continue;
                        }
                    }
                } else {
                    docRoot = TTYDisplay.promptForEntry(docRootLocationQuery,
                            defaultDocRoot, helpText);

                    if (docRoot.equals(backOption)) {
                        this.cancelDirection = true;

                        return;
                    }
                }
            }

            String existError = isDocRootPresent(docRoot);
            storageResource = getStorageResourceForMount(storageResources, docRoot);

            if (existError != null) {
                TTYDisplay.printError(existError);
                TTYDisplay.clear(1);
                TTYDisplay.promptForEntry(messages.getString(
                        "cliwizards.continue"));
                docRootOk = false;

                continue;
            } else if (storageResource == null) {
                String defaultDocRootNotHA = messages.getString(
                        "apache.docRootPanel.docRoot.notHA",
                        new Object[] { docRoot, apacheMode.toLowerCase() });
                TTYDisplay.printInfo(defaultDocRootNotHA);

                String copyDocRootFilesToHA = messages.getString(
                        "apache.docRootPanel.copyDocRoot",
                        new Object[] { docRoot });
                String choiceTableOptions[] = {
                        copyDocRootFilesToHA, enterNewDocRoot
                    };
                int choice = TTYDisplay.getMenuOption(selectAChoice,
                        choiceTableOptions, 0, helpText);

                if (choice == 0) {
                    wizardModel.setWizardValue(
                        ApacheWizardConstants.DISPLAY_FS_TABLE,
                        new Boolean(true));
                    wizardModel.setWizardValue(Util.APACHE_DOC, docRoot);

                    return;
                } else if (choice == -1) {
                    this.cancelDirection = true;

                    return;
                } else {
                    defaultDocRootPresent = false;
                    docRootOk = false;

                    continue;
                }
            } else {
                wizardModel.setWizardValue(Util.APACHE_DOC, docRoot);
                wizardModel.setWizardValue(
                    ApacheWizardConstants.STORAGE_RESOURCE_NAME,
                    storageResource);

                // Get the mountPoint under which the docRoot
                // resides
                String mountPoints[] = (String[]) storageResources.get(
                        storageResource);
                String mount = null;

                for (int i = 0; i < mountPoints.length; i++) {
                    docRoot += "/";

                    if (docRoot.indexOf(mountPoints[i] + "/", 0) == 0) {
                        mount = mountPoints[i];
                    }
                }

                wizardModel.setWizardValue(
                    ApacheWizardConstants.DISPLAY_FS_TABLE, new Boolean(false));
                wizardModel.setWizardValue(Util.APACHE_MOUNT, mount);
                fillWizardModel_forExistingMountPoint(storageResource,
                    wizardModel);

                return;
            }
        }
    }

    /**
     * This method takes a docRoot and validates it for existence
     *
     * @param  docRoot
     *
     * @return  error String if docRoot does not exists
     */
    private String isDocRootPresent(String docRoot) {

        String zoneExportPath = null;
        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);

        // If user had selected a zone if the conf file panel, we need to
        // append the zone export path to the apache doc, so that the
        // MBean in the global zone can validate it
        String apacheZone = (String) wizardModel.getWizardValue(
                Util.APACHE_ZONE);

        if (apacheZone != null) {
            HashMap discoveredMap = mBean.discoverPossibilities(
                    new String[] { Util.APACHE_ZONE }, nodeEndPoint);
            zoneExportPath = (String) discoveredMap.get(Util.APACHE_ZONE);
            docRoot = zoneExportPath + docRoot;
        }

        String propertyNames[] = new String[] { Util.APACHE_DOC };
        HashMap helperMap = new HashMap();
        helperMap.put(Util.APACHE_DOC, docRoot);

        ErrorValue errVal = null;
        errVal = mBean.validateInput(propertyNames, helperMap, null);

        if (!errVal.getReturnValue().booleanValue()) {
            String error = messages.getString(errVal.getErrorString(),
                    new Object[] { docRoot, nodeEndPoint });

            return error;
        }

        return null;
    }


    /**
     * This method takes a HashMap of Storage Resources/Mountpoints and returns the
     * storage(HASP or QFS) Resource name for the apacheDoc mountpoint specified else it returns
     * null.
     *
     * @param  storageResources
     * @param  apacheDoc
     *
     * @returns  String storageResource or null
     */
    private static String getStorageResourceForMount(HashMap storageResources,
        String apacheDoc) {

        if (storageResources != null) {
            Set storageSet = storageResources.entrySet();
            Iterator iter = storageSet.iterator();

            // Iterate through the moun points and return if found
            while (iter.hasNext()) {
                Map.Entry entry = (Map.Entry) iter.next();
                String storageResName = (String) entry.getKey();
                String mountPoints[] = (String[]) entry.getValue();

                for (int i = 0; i < mountPoints.length; i++) {

                    // Check if docRoot is under any mountpoint.
                    // eg: If docRoot is /global/fs1/var/htdocs, we should
                    // pick a storageResource monitoring /global/fs1
                    apacheDoc += "/";

                    if (apacheDoc.indexOf(mountPoints[i] + "/", 0) == 0) {
                        return storageResName;
                    }
                }
            }
        }

        return null;
    }

    /**
     * This method takes a HashMap of storage Resources/Mountpoints and returns all
     * the MountPoints
     *
     * @param  storageResources
     *
     * @returns  String[] of mountPoints
     */
    private static List getStorageMountPoints(HashMap storageResources) {

        List mountPointSet = null;

        if (storageResources != null) {
            mountPointSet = new ArrayList();

            Set storageSet = storageResources.entrySet();
            Iterator iter = storageSet.iterator();

            // Iterate through the mount points and return if found
            while (iter.hasNext()) {
                Map.Entry entry = (Map.Entry) iter.next();
                String mountPoints[] = (String[]) entry.getValue();

                for (int i = 0; i < mountPoints.length; i++) {

                    if (!mountPointSet.contains(mountPoints[i])) {
                        mountPointSet.add(mountPoints[i]);
                    }
                }
            }
        }

        return mountPointSet;
    }

    /**
     * This method would fill the wizardModel with the required values if the
     * user had selected a mount point already under storage Resource
     */
    private void fillWizardModel_forExistingMountPoint(String storageResource,
        CliDSConfigWizardModel wizCxt) {

        QueryExp query = null;

        // Get the APACHE_MODE from wizardModel
        String apacheMode = (String) wizCxt.getWizardValue(Util.APACHE_MODE);

        /*
         * Since DocRoot is governed by storage resource, we take that resource
         * as reference resource and discover the Apache Resource Group and
         * LogicalHost (or) SharedAddress resources
         */

        // Get the Apache's ResourceGroup from the storage resource
        String apacheResourceGroup = null;

        try {
            ResourceMBean storageMBean = (ResourceMBean) TrustedMBeanModel
                .getMBeanProxy(connection, ResourceMBean.class, storageResource,
                    true);
            if (storageMBean.getType().indexOf(Util.HASP_RTNAME) != -1 ||
                    apacheMode.equals(Apache.FAILOVER)) {
                apacheResourceGroup = storageMBean.getResourceGroupName();
                wizCxt.setWizardValue(Util.APACHE_RESOURCE_GROUP,
                    apacheResourceGroup);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        /*
         * Based on the Apache Mode, discover the LogicalHostnames in the
         * ApacheResource Group or all the SharedAddress resources
         * in the cluster.
         */
        // ApacheMode is Failover
        if (apacheMode.equals(Apache.FAILOVER)) {

            // Set wizardType for the common panels
            wizCxt.setWizardValue(LogicalHostWizardConstants.WIZARDTYPE,
                LogicalHostWizardCreator.WIZARDTYPE);

            // Get a list of LogicalHost Resources in this ResourceGroup
            query = Query.and(Query.eq(
                        new AttributeValueExp("ResourceGroupName"),
                        Query.value(apacheResourceGroup)),
                    Query.match(new AttributeValueExp("Type"),
                        Query.value(LogicalHost.RTNAME + "*")));

            try {
                Set logicalHostResources = TrustedMBeanModel.getInstanceNames(
                        connection, ResourceMBean.class, query);
                wizCxt.setWizardValue(Util.LH_RESOURCE_NAME,
                    logicalHostResources);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        // Apache Mode is Scalable
        else {

            // Set wizardType for the common panels
            wizCxt.setWizardValue(LogicalHostWizardConstants.WIZARDTYPE,
                SharedAddressWizardCreator.WIZARDTYPE);

            // Get the SharedAddress resources configured in the cluster
            query = Query.match(new AttributeValueExp("Type"),
                    Query.value(SharedAddress.RTNAME + "*"));

            try {
                Set sharedAddressResources = TrustedMBeanModel.getInstanceNames(
                        connection, ResourceMBean.class, query);
                wizCxt.setWizardValue(Util.SH_RESOURCE_NAME,
                    sharedAddressResources);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
