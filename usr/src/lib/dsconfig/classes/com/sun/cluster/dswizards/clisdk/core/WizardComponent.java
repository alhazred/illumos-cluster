/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)WizardComponent.java	1.21	08/12/10 SMI"
 */

package com.sun.cluster.dswizards.clisdk.core;

import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.common.TrustedMBeanModel;

import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.CliUtil;

import java.io.IOException;

import javax.management.remote.JMXConnector;

import javax.swing.JPanel;


/**
 * WizardComponent defines the interface and implements default behavior common
 * to all classes in the client Wizard tree.  The Wizard tree consists of panels
 * that define the user experience of the wizard.
 *
 * <p>The wizard client panel tree consists of an arrangement of panels
 * (subclassed from WizardLeaf) and nodes (subclassed from WizardComposite). The
 * arrangement of the tree determines the order in which the panels will be
 * visited by the wizard.  Nodes can affect the traversal of the panels, which
 * allows advanced panels to be skipped for users that do not wish to use them.
 *
 * <p>"Component" was chosen as a name for this class from the book "Design
 * Patterns".
 */
public abstract class WizardComponent extends JPanel {

    // Change this value only when making a serializationally
    // incompatible change such as deleting a field. See the
    // Java Object Serialization Specification, Chapter 5:
    // Versioning of Serializable Objects, for more details.
    private static final long serialVersionUID = 8916189176033471046L;


    /**
     * This is the parent of this WizardComponent.  This helps provide a tree in
     * which a child in the tree knows who its parent is.
     */
    protected WizardComponent parent = null;

    /** The locking object for the parent. */
    private transient Object parentLock = new Object();

    /**
     * The wizard manager is responsible for creation, display, and navigation
     * of the wizard tree.
     */
    protected WizardTreeManager wizardManager = null;


    protected CliDSConfigWizardModel wizardModel = null;
    private WizardFlow wizardFlow = null;

    /** The locking object for the wizardManager. */
    private transient Object treeManagerLock = new Object();

    /**
     * The name of this wizard component.  For siblings, it is important that
     * each sibling has a unique name.
     */
    protected String name = null;

    /** The locking object for the name. */
    private transient Object nameLock = new Object();

    /**
     * This flag indicates whether or not the user interface has been created.
     */
    private boolean userInterfaceCreated = false;

    /** The locking object for the userInterfaceCreated flag. */
    private transient Object UICreatedLock = new Object();

    /**
     * The cancel direction is a flag that indicates whether a cancel operation
     * from this panel should traverse backward (false) or forward (true).
     */
    protected boolean cancelDirection = false;

    /** The locking object for the cancel direction. */
    private transient Object cancelDirectionLock = new Object();

    /**
     * Creates a WizardComponent object tree.
     */
    public WizardComponent() {
        this("noname", null);
    }

    /**
     * Creates a WizardComponent with the specified name.
     *
     * @param  wizardModel  The CliDSConfigWizardModel object.
     * @param  name  The name for this WizardComponent.
     */
    public WizardComponent(CliDSConfigWizardModel wizardModel, String name,
        WizardFlow wizardFlow) {
        this(name, null);
        this.wizardModel = wizardModel;
        this.wizardFlow = wizardFlow;
    }

    /**
     * Creates a WizardComponent with the specified name and the specified
     * wizard manager.
     *
     * @param  name  The name for this WizardComponent.
     * @param  wizardManager  The wizardManager responsible for this component.
     */
    public WizardComponent(String name, WizardTreeManager wizardManager) {
        setName(name);

        setWizardTreeManager(wizardManager);
    }

    /**
     * Sets the name of this WizardComponent to the specified name.
     *
     * @param  name  The new name for this WizardComponent.
     */
    public void setName(String name) {

        synchronized (nameLock) {
            this.name = name;
        }
    }

    /**
     * Gets the current name of this WizardComponent.
     *
     * @return  The current name.
     */
    public String getName() {

        synchronized (nameLock) {
            return (name);
        }
    }

    /**
     * This is the default implementation of getChild. WizardComposite overrides
     * this method to return a child.  This default implementation can be used
     * directly for the WizardLeaf object.
     *
     * @param  childName  The name of the child to retrieve.
     *
     * @return  The requested child, or null if the child does not exist.
     */
    public WizardComponent getChild(String childName) {
        return (null);
    }

    /**
     * Gets the parent of this WizardComponent.
     *
     * @return  The parent.
     */
    public WizardComponent getParentComponent() {

        synchronized (parentLock) {
            return (parent);
        }
    }

    /**
     * Sets the parent of this WizardComponent.
     *
     * @param  parent  The new parent of this WizardComponent.
     */
    public void setParentComponent(WizardComponent parent) {

        synchronized (parentLock) {
            this.parent = parent;
        }
    }

    /**
     * Sets the WizardTreeManager for this object.
     *
     * @param  treeManager  The new WizardTreeManager controlling the client
     * panel tree.
     */
    public void setWizardTreeManager(WizardTreeManager treeManager) {

        synchronized (treeManagerLock) {
            this.wizardManager = treeManager;
        }
    }

    /**
     * Returns the WizardTreeManager controlling the client panel tree.
     */
    public WizardTreeManager getWizardTreeManager() {

        synchronized (treeManagerLock) {
            return (this.wizardManager);
        }
    }

    /**
     * This method returns the wizardModel object of this wizard component.
     *
     * @return  a CliDSConfigWizardModel object
     */
    public CliDSConfigWizardModel getWizardModel() {
        WizardTreeManager wtm = getWizardTreeManager();

        return wtm.getWizardModel();
    }

    /**
     * Get the root of the WizardComponent tree.
     *
     * @return  The root of the WizardComponent tree.
     */
    public WizardComponent getRootComponent() {
        WizardComponent parent = getParentComponent();

        if (parent == null) {
            return (this);
        }

        return (parent.getRootComponent());
    }

    /**
     * This method returns true if this WizardComponent wants to be skipped
     * during traversal of this tree.
     *
     * @return  true if this object wants to be skipped in the traversal of the
     * tree.  false otherwise.
     */
    public boolean skip() {
        return (false);
    }

    /**
     * Says whether this component's has created its user interface or not. Once
     * the component's createUI() method has successfully completed, this method
     * returns true.
     *
     * @return  true, Once the component's createUI() method has successfully
     * completed.
     */
    public boolean getUserInterfaceCreated() {

        synchronized (UICreatedLock) {
            return userInterfaceCreated;
        }
    }

    /**
     * This method is called when the WizardComponent is displayed.
     */
    public void beginDisplay() {

        if (!getUserInterfaceCreated()) {
            createUI();

            synchronized (UICreatedLock) {
                userInterfaceCreated = true;
            }
        }
    }

    /**
     * Convenience method to get data from the CliDSConfigWizardModel that this
     * component belongs to.
     *
     * @param  key  They key to use when retrieving the data
     */
    public Object getWizardValue(String key) {
        Object rtn = getWizardModel().getWizardValue(key);

        return rtn;
    }

    /**
     * Convenience method to set data into the wizardModel that this component
     * belongs to.
     *
     * @param  key  They key to use when setting the data
     * @param  val  The object to store under the specified key
     */
    public void setWizardValue(String key, Object val) {
        getWizardModel().setWizardValue(key, val);
    }

    /**
     * This method is called when this WizardComponent is being displayed, and
     * the user presses the "back" button.  This method provides an opportunity
     * for this WizardComponent to do cleanup before exiting. This method is
     * used when the user presses the "back" button, and the <code>
     * isDisplayComplete</code> method is used when the user presses the "next"
     * button.
     */
    public void abortDisplay() {
    }

    /**
     * This method creates the user interface.
     */
    public void createUI() {
    }

    /**
     * This method is called during non-graphical execution.  In
     * this method, subclasses should interact with the console
     * (<Code>System.in</code> and <code>System.out</code>) to collect the same
     * information that the component needs.  Note that the information
     * collected should be saved in the same way as the <code>
     * isDisplayComplete()</code> and <code>abortDisplay()</code> methods expect
     * it.
     *
     * <p>For example: suppose the graphical version of this component presents
     * the user with a <code>CheckBoxGroup<code>, the user makes a selection,
     * and the result is examined in the <code>isDisplayComplete()</code> method
     * by using the <code>getSelectedCheckBox()</code> API from <code>
     * java.awt.CheckBoxGroup</code>.  To do this same thing using this method,
     * it should prompt the user with the same strings as used in the graphical
     * version, and set the selections into the <code>checkBoxGroup</code> when
     * the user supplies the information (by using the <code>
     * setSelectedCheckBox()<code>based on user input).  In this way, the code
     * already written for <code>isDisplayComplete()</code> does not have to be
     * modified.
     */
    public void consoleInteraction() {
    }

    /**
     * Used in CLI Mode  for imediate validations of data
     *
     * @param  result  The result of the call that caused the callback.
     * @param  id  An identifier, for use by the callee during the callback.
     *
     * @return  Whether the result is valid.  Subclasses should use this method
     * to validate data that is entered by the user for a particular query,
     * rather than validating ALL data on the panel at once.  This method
     * returns <code>false</code> by default.
     */
    public boolean callback(String result, String id) {
        return false;
    }

    /**
     * Display a query to the user.  When a selection is made, the corresponding
     * method will be called on the target.
     *
     * @param  target  The object to call when a selection from the dialog is
     * made.
     * @param  title  A small title for this query.
     * @param  message  The message to be displayed to the user.
     * @param  buttons  The buttons (button labels) offered to the user to
     * select from.
     * @param  methods  A list of methods (in the same order as the list of
     * buttons) that will be called when a dialog selection is made.
     */
    public void displayQuery(Object target, String title, String message,
        String buttons[], String methods[]) {
        getWizardTreeManager().displayQuery(target, title, message, buttons,
            methods);
    }

    /**
     * Returns true if this WizardComponent is finished displaying. This method
     * can be used to verify user input.
     *
     * @return  true if the display of this WizardComponent is complete; false
     * otherwise.
     */
    public boolean isDisplayComplete() {
        boolean result = true;

        return (result);
    }

    /**
     * Get the child that follows the specified child in the WizardComponent
     * tree.  Applications should use <code>next</code>.
     *
     * @param  currentChild  The child in the tree that comes before the desired
     * sibling.
     *
     * @return  The next child in the tree.
     */
    protected abstract WizardComponent getNextChild(
        WizardComponent currentChild);

    /**
     * Get the next WizardComponent in the tree.
     *
     * @return  The next child in the tree.
     */
    public abstract WizardComponent next();

    /**
     * Get the child that precedes the specified child in the WizardComponent
     * tree.  Applications should use <code>previous</code>.
     *
     * @param  currentChild  The child in the tree that comes after the desired
     * sibling.
     *
     * @return  The previous child in the tree.
     */
    protected abstract WizardComponent getPreviousChild(
        WizardComponent currentChild);

    /**
     * Get the previous WizardComponent in the tree.
     *
     * @return  The previous child in the tree.
     */
    public abstract WizardComponent previous();

    /**
     * Returns true if this is the first leaf in the tree.
     *
     * @return  true if this is the first leaf in the tree.  false otherwise.
     */
    public boolean isFirst() {
        WizardComponent root = getRootComponent();

        if (this == root.next()) {
            return (true);
        }

        return (false);
    }

    /**
     * Returns true if this is the last leaf in the tree.
     *
     * @return  true if this is the last leaf in the tree.  false otherwise.
     */
    public boolean isLast() {
        int curPageID = wizardFlow.getPanelId(name);

        return (wizardFlow.isFinishPageId(curPageID, wizardModel));
    }

    /**
     * Get the WizardComponent
     *
     * @return  The desired WizardComponent, or null if the WizardComponent does
     * not exist.
     */
    public WizardComponent getWizardComponent() {
        return this;
    }

    /**
     * Reset this component, given the specified information. This method may be
     * called when a user backtracks in the client wizard tree and changes a
     * fundamental setting. The panel that enabled the modification might
     * traverse subsequent panels and call this method to resynchronize all
     * panels with the user's choice.
     *
     * @param  info  The information required to perform a reset.
     */
    public void reset(Object info) {
    }

    /**
     * Returns the cancel direction.  The cancel direction determines whether
     * cancel would visit the previous panel or the following panel. This method
     * returns false if cancel should visit the previous panel, or true if
     * cancel should visit the next panel.
     *
     * @return  The direction within the client wizard tree that a cancel
     * operation should traverse.
     */
    public boolean getCancelDirection() {

        /*
         * Usually, a canceled panel will need to traverse to the previous
         * panel.
         */
        synchronized (cancelDirectionLock) {
            return (cancelDirection);
        }
    }

    /**
     * Sets the cancel direction.  The cancel direction determines whether
     * cancel would visit the previous panel or the following panel.
     *
     * @param  cancelDirection  If true, cancel will cause the next panel to be
     * visited.  False causes the previous panel to be visited.
     */
    public void setCancelDirection(boolean cancelDirection) {

        synchronized (cancelDirectionLock) {
            this.cancelDirection = cancelDirection;
        }
    }

    /**
     * Returns the cancel message.
     *
     * @return  A String representing the message to be displayed when a user
     * clicks "cancel".
     */
    public String getCancelMessage() {
        return ("Cancel - Are you sure?");
    }

    /**
     * Cancel this panel.  This method is only called after the cancel operation
     * is confirmed by the user.
     *
     * @return  The client panel that should be displayed after the cancel
     * operation.
     */
    public WizardComponent cancel() {

        if ((parent != null) && (parent instanceof WizardComposite)) {
            return (((WizardComposite) parent).cancel(this,
                        getCancelDirection()));
        }

        return (null);
    }


    /**
     * This method returns a boolean value indicates whether the current panel
     * can be auto-navigated. A panel can be auto-navigated when it has a next
     * button and it does not automatically navigate itself. Most of the panels
     * not at the end of tree can be auto-navigated(netButtonPressed() method
     * call can be conducted on this leaf), but there are exceptions. This
     * method should be overriden when the panel should not be nagivagated or
     * additional check need to be made to decide whether to autonavigate the
     * panel.
     *
     * @return  boolean value indicates whether the current panel can be
     * auto-navigated.
     */
    public boolean canAutonext() {
        return (true);
    }

    /**
     * This method returns the next panel's name. This method is used by the
     * Iterator Class's next method.
     *
     * @return  Name of the Next Panel
     */
    public String getNextPanelName() {

        // Get Current Panel's ID
        int curPageID = wizardFlow.getPanelId(name);

        // Using the WizardFlow get the Next Panels ID
        int nextPageID = wizardFlow.getNextPageId(curPageID, wizardModel);

        // If ID is -1, END the current Wizard
        if (nextPageID == -1) {
            return "END";
        }

        // Get the Name of the Next Panel
        String nextPanelName = wizardFlow.getPanelName(nextPageID);

        return nextPanelName;
    }

    public JMXConnector getConnection(String agentLocation) {

        String split_str[] = agentLocation.split(Util.COLON);
        agentLocation = split_str[0];

        JMXConnector connector = (JMXConnector) wizardModel.getWizardValue(
                CliUtil.JMX_CONNECTION_PREFIX + agentLocation);

        if (connector == null) {

            try {
                connector = TrustedMBeanModel.getWellKnownConnector(
                        agentLocation);
            } catch (Exception ex) {
                TTYDisplay.printError("Failed to get connection to node " +
                    agentLocation);
                System.exit(1);
            }

            setConnection(agentLocation, connector);
        }

        return connector;
    }

    public void setConnection(String agentLocation, JMXConnector connector) {
        wizardModel.setWizardValue(CliUtil.JMX_CONNECTION_PREFIX + agentLocation,
            connector);
    }

    public void closeConnections(String nodeList[]) {
        JMXConnector localConnector = (JMXConnector) wizardModel.getWizardValue(
                CliUtil.JMX_CONNECTION_PREFIX + Util.getClusterEndpoint());

        if (localConnector != null) {

            // If nodelist is null, close the local connection alone
            if (nodeList != null) {

                for (int i = 0; i < nodeList.length; i++) {
                    String nodeName = nodeList[i];

                    // extract the nodename incase this is a logical nodename
                    String split_str[] = nodeName.split(Util.COLON);
                    String nodeEndPoint = Util.getNodeEndpoint(localConnector,
                            split_str[0]);
                    JMXConnector connector = (JMXConnector) wizardModel
                        .getWizardValue(CliUtil.JMX_CONNECTION_PREFIX +
                            nodeEndPoint);

                    if (connector != null) {

                        try {
                            connector.close();
                            wizardModel.setWizardValue(
                                CliUtil.JMX_CONNECTION_PREFIX + nodeEndPoint,
                                null);
                        } catch (IOException ioe) {
                        }
                    }
                }
            }

            try {
                localConnector.close();
                wizardModel.setWizardValue(CliUtil.JMX_CONNECTION_PREFIX +
                    Util.getClusterEndpoint(), null);
            } catch (IOException ioe) {
            }
        }
    }
}
