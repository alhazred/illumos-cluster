/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)RepChooseLHPanel.java 1.12     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.sapwebas;

// J2SE
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

// CMAS
import com.sun.cluster.agent.dataservices.utils.InfrastructureMBean;
import com.sun.cluster.agent.dataservices.utils.Util;

// CLI Wizard SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;

// Wizard Common
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.common.CliLogicalHostSelectionPanel;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.logicalhost.LogicalHostWizardConstants;

public class RepChooseLHPanel extends CliLogicalHostSelectionPanel {

    public RepChooseLHPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(name, model, wizardFlow, wizardi18n);
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {
        boolean bValidate = false;
        boolean singleSelection = false;
        desc = wizardi18n.getString("sap.chooseLHPanel.rep.query");

        String csSelHosts[] = (String[]) wizardModel.getWizardValue(
                Util.CS_NEW_HOSTS);
        String csSelLhRgName = (String) wizardModel.getWizardValue(
                Util.CS_RG_NAME);
        String selHosts[] = null;
        Vector selLhRes = null;
        String selLhRgName = null;
        String errorMsg;
        String continue_txt = wizardi18n.getString("cliwizards.continue");

        while (!bValidate) {
            super.consoleInteraction(singleSelection);
            selHosts = (String[]) wizardModel.getWizardValue(
                    Util.SEL_HOSTNAMES);
            selLhRes = (Vector) wizardModel.getWizardValue(LH_SEL_RS);
            selLhRgName = (String) wizardModel.getWizardValue(LH_SEL_RG);

            if ((selLhRgName != null) && (csSelLhRgName != null) &&
                    selLhRgName.equals(csSelLhRgName)) {
                errorMsg = wizardi18n.getString("sap.repChooseLHPanel.rgerror");
                TTYDisplay.printError(errorMsg);
                TTYDisplay.promptForEntry(continue_txt);
                bValidate = false;

                continue;
            }

            if ((csSelHosts != null) && (selHosts != null)) {
                List csSelHostList = Arrays.asList(csSelHosts);
                boolean bDup = false;

                for (int i = 0; i < selHosts.length; i++) {

                    if (csSelHostList.contains(selHosts[i])) {
                        errorMsg = wizardi18n.getString(
                                "sap.repChooseLHPanel.hostname.error");
                        TTYDisplay.printError(errorMsg);
                        TTYDisplay.promptForEntry(continue_txt);
                        bDup = true;

                        break;
                    }
                }

                if (bDup) {
                    bValidate = false;

                    continue;
                }
            }

            bValidate = true;
        }

        if (selLhRgName == null) {
            wizardModel.setWizardValue(SapWebasConstants.IS_NEW_RG_REP,
                Boolean.TRUE);
        } else {
            wizardModel.setWizardValue(SapWebasConstants.IS_NEW_RG_REP,
                Boolean.FALSE);
        }

        wizardModel.setWizardValue(SapWebasConstants.NEW_LH_CS,
            wizardModel.getWizardValue(Util.CREATE_NEW));
        wizardModel.setWizardValue(Util.REP_NEW_HOSTS, selHosts);
        wizardModel.setWizardValue(Util.REP_SELECTED_LH, selLhRes);
        wizardModel.setWizardValue(Util.REP_RG_NAME, selLhRgName);
        wizardModel.setWizardValue(Util.REP_NETIFLIST,
            wizardModel.getWizardValue(
                LogicalHostWizardConstants.SEL_NETIFLIST));
    }

    protected void refreshRGList() {
        TTYDisplay.clear(2);

        Thread t = TTYDisplay.busy(wizardi18n.getString(
                    "logicalhostSelectionPanel.discovery.busy"));
        t.start();

        if (mbean == null) {
            mbean = (InfrastructureMBean) getInfrastructureMBean();
        }

        String rgName = (String) wizardModel.getWizardValue(Util.REP_RG_NAME);
        HashMap helperData = new HashMap();

        if (rgName == null) {
            String nodeList[] = (String[]) wizardModel.getWizardValue(
                    Util.RG_NODELIST);

            // construct a query and fetch resources
            helperData.put(Util.RG_NODELIST, nodeList);
        } else {

            // get all logical host resources in resource group rgName
            helperData.put(Util.RG_NAME, rgName);
        }

        HashMap map = mbean.discoverPossibilities(
                new String[] { Util.LH_RGLIST }, helperData);

        try {
            t.interrupt();
            t.join();
        } catch (Exception e) {

        }

        List rgs = (List) map.get(Util.LH_RGLIST);
        wizardModel.setWizardValue(Util.LH_RGLIST, rgs);
    }

}
