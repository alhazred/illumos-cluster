/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)RACDisksPanel.java	1.19	09/04/22 SMI"
 */

package com.sun.cluster.dswizards.oraclerac.storage;

// CMASS
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.dataservices.utils.ClusterDiskGroupInfo;
import com.sun.cluster.agent.dataservices.utils.DiskGroupInfo;
import com.sun.cluster.agent.zonecluster.ZoneClusterData;

// CLI SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;

// Wizard Common
import com.sun.cluster.dswizards.common.CliUtil;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.oraclerac.invoker.RACInvokerCreator;

import com.sun.cluster.model.DataModel;
import com.sun.cluster.model.ZoneClusterModel;
import com.sun.cluster.model.RACModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Vector;

/**
 * This panel lists to the user all the OBAN,Shared VxVM diskgroups that are
 * monitored  by ScalDeviceGroup Resources under RGM.
 */
public class RACDisksPanel extends WizardLeaf {


    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N messages; // Message Bundle

    private RACModel racModel = null;

    private static int COLS = 4;

    /**
     * Creates a new instance of RACDisksPanel
     */
    public RACDisksPanel() {
        super();
    }

    /**
     * Creates an RACDisks Panel with the given name and tree manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public RACDisksPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates an RACDisks Panel for racstorage Wizard with the given name and
     * associates it with the WizardModel and WizardFlow
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     */
    public RACDisksPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N messages) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;

        // Get the I18N instance
        this.messages = messages;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {

        String title = messages.getString("racstorage.racDisksPanel.title");
        String subtitle = messages.getString(
                "racstorage.racDisksPanel.subTitle");
        String desc = messages.getString("racstorage.racDisksPanel.desc");
        String dsNameTitle = messages.getString(
                "racstorage.racDisksPanel.diskSetName");
	String logicalDeviceTitle = messages.getString(
		"racstorage.racDisksPanel.logicalDevice");
        String dsResourceTitle = messages.getString(
                "racstorage.racDisksPanel.diskSetResource");
        String dsResourceGroupTitle = messages.getString(
                "racstorage.racDisksPanel.diskSetResourceGroup");
        String dsTypeTitle = messages.getString(
                "racstorage.racDisksPanel.diskSetType");
        String help = messages.getString("racstorage.racDisksPanel.cli.help");
        String specify = messages.getString("racstorage.racDisksPanel.other");
        String yes = messages.getString("cliwizards.yes");
        String no = messages.getString("cliwizards.no");
        String dchar = messages.getString("ttydisplay.d.char");
        String rchar = messages.getString("ttydisplay.r.char");
        String done = messages.getString("ttydisplay.menu.done");
        String refresh = messages.getString("ttydisplay.menu.refresh");
        String cchar = messages.getString("ttydisplay.c.char");
        String createNewDisksets = cchar + ") " +
            messages.getString("racstorage.racDisksPanel.createNewDiskSet");
        String confirmDisks = messages.getString(
                "racstorage.racDisksPanel.confirmDisks");
        String confirmMessage = messages.getString("cliwizards.confirmMessage");
        String exitOption = no;
        String empty = messages.getString("racstorage.racDisksPanel.empty");
        String emptySelection = messages.getString(
                "racstorage.racDisksPanel.emptySelection");
        String extraDesc = messages.getString(
                "storageSelectionPanel.extraDesc");
        String continueConfirm = messages.getString(
                "cliwizards.continueConfirm");
        String continue_txt = messages.getString("ttydisplay.msg.cli.enter");
        String back = messages.getString("ttydisplay.back.char");
        String table_empty_continue = messages.getString(
                "cliwizards.table.empty.continue");
	String allDisks = messages.getString("racstorage.racDisksPanel.allDisks");
        String help_text = messages.getString("storageSelectionPanel.cli.help");

        String baseClusterNodeList[] = (String[]) wizardModel.getWizardValue(
                Util.BASE_CLUSTER_NODELIST);
        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);

	HashMap helperData = new HashMap();
	HashMap devTabMap = null;
	ArrayList devTabList = null;
	HashMap fsTabMap = null;
	ArrayList fsTabList = null;

	String selectedZoneCluster = CliUtil.getZoneClusterName(wizardModel);
	helperData.put(Util.SEL_ZONE_CLUSTER, selectedZoneCluster);

	DataModel dm = DataModel.getDataModel(null);
	ZoneClusterModel zm = dm.getZoneClusterModel();
	racModel = RACInvokerCreator.getRACModel();

	String[] zoneClusterList = null;

	try {
	    zoneClusterList = zm.getZoneClusterList(null);

	    if (zoneClusterList != null) {
		for (int i = 0; i < zoneClusterList.length; i++) {

		    ZoneClusterData zcData = zm.getZoneClusterData(
			null, zoneClusterList[i]);

		    fsTabList = zcData.getZoneClusterFSTabList();

		    if (fsTabMap == null) {
                        fsTabMap = new HashMap();
                    }
                    fsTabMap.put(zoneClusterList[i], fsTabList);
 
		    devTabList = zcData.getZoneClusterDevTabList();

		    if (devTabMap == null) {
			devTabMap = new HashMap();
		    }
		    devTabMap.put(zoneClusterList[i], devTabList);
		}
	    }
	} catch (Exception e) {
	    System.out.println(e.getMessage());
	}

	// Pass the devtab list of all the zone clusters
	helperData.put(Util.ZONE_CLUSTER_DEVTAB_LIST, devTabMap);
	// Pass the fstab list to all the zone clusters
	helperData.put(Util.ZONE_CLUSTER_FSTAB_LIST, fsTabMap);
	helperData.put(Util.ZONE_CLUSTER_LIST, zoneClusterList);

        do {
            TTYDisplay.clearScreen();
            TTYDisplay.clear(1);

            // Enable Back and Help
            TTYDisplay.setNavEnabled(true);
            this.cancelDirection = false;

            // Title
            TTYDisplay.printTitle(title);

            // Populate the Table to be displayed
            Vector disksData = new Vector();

            ClusterDiskGroupInfo clusterDiskGroupInfo = null;
            Map vxvmMap = null;
            List fstypeList = new ArrayList();

            clusterDiskGroupInfo = (ClusterDiskGroupInfo) wizardModel.getWizardValue(
		Util.SVMDEVICEGROUP);
            vxvmMap = (Map) wizardModel.getWizardValue(
                    Util.RGM_VxVMDEVICEGROUP);

	    String keyPrefix = (String) wizardModel.getWizardValue(Util.KEY_PREFIX);
            // Depending on the Storage Schemes get the respective
            // Resources and the DeviceGroups monitored by them.
            List storageScheme = (ArrayList) wizardModel.getWizardValue(
                    keyPrefix + Util.ORF_STORAGE_MGMT_SCHEMES);

            Thread t = null;

            try {
                t = TTYDisplay.busy(messages.getString(
                            "racstorage.racDisksPanel.discovery.busy"));

                t.start();

                if (storageScheme.contains(Util.SVM) ||
		    storageScheme.contains(Util.QFS_SVM)) {

                    if (clusterDiskGroupInfo == null) {
                        clusterDiskGroupInfo = getDeviceGroupResources_forOban(
			    baseClusterNodeList, helperData);
                    }
                }

                if (storageScheme.contains(Util.VXVM)) {

                    if (vxvmMap == null) {
                        vxvmMap = getDeviceGroupResources_forVxVM(baseClusterNodeList);
                        wizardModel.setWizardValue(Util.RGM_VxVMDEVICEGROUP,
                            vxvmMap);
                    }
                }

                // The Maps pertaining to the storage schemes are added to
                // the final vector that is displayed. The device group TYPE
                // is added
                // to the final vector along with the respective Maps.
                if (clusterDiskGroupInfo != null) {

		    List diskGroupsList = clusterDiskGroupInfo.getDiskGroupInfoList();
		    if (diskGroupsList != null) {
			int size = diskGroupsList.size();
			for (int j = 0; j < size; j++) {
			    DiskGroupInfo diskGroupInfo = (DiskGroupInfo) diskGroupsList.get(j);
			    String dgName = diskGroupInfo.getDiskGroupName();

			    // get resource map for SVM resources in the ZC
			    HashMap rsSVMMap = diskGroupInfo.getSVMResourceMap();
			    Vector dgData = getDiskGroupData(
				dgName, selectedZoneCluster,
				rsSVMMap, fstypeList, Util.SVM);

			    disksData.addAll(dgData);

			    // get resource map for QFS_SVM resources in base cluster
			    HashMap rsQfsSVMMap = diskGroupInfo.getQfsSVMResourceMap();
			    dgData = getDiskGroupData(
				dgName, selectedZoneCluster,
				rsQfsSVMMap, fstypeList, Util.QFS_SVM);
			    disksData.addAll(dgData);
			} // for each disk group
		    } // if diskGroupList != null
		}

		if (vxvmMap != null) {
		    Set entrySet = vxvmMap.entrySet();

		    for (Iterator i = entrySet.iterator(); i.hasNext();) {
			Map.Entry entry = (Map.Entry) i.next();
			String dgResourceName = (String) entry.getKey();
			String dgName = (String) entry.getValue();

			// Get the resource group for the Disk Resource
			String propertyName[] = new String[] {
				Util.STORAGEGROUP
			    };

			HashMap userInput = new HashMap();
			userInput.put(Util.SEL_ZONE_CLUSTER, selectedZoneCluster);
			userInput.put(Util.RS_NAME, dgResourceName);
			Map resourceGroupMap = (HashMap)racModel.discoverPossibilities(
			    null, nodeEndPoint, propertyName, userInput);

			disksData.add(dgResourceName);
			disksData.add((String) resourceGroupMap.get(
				Util.STORAGEGROUP));
			disksData.add(dgName);
			disksData.add("");
			fstypeList.add(Util.VxVMDEVICEGROUP);
                    }
                }

		HashMap newAdditionsMap = (HashMap)
		    wizardModel.getWizardValue(Util.NEW_DG_RESOURCES);

                if (newAdditionsMap != null) {
                    Set entrySet = newAdditionsMap.entrySet();

                    List newAdditions = new ArrayList();

                    // The newAdditions Map would contain the dgResourceName
                    // that is autogenrated as the key and the dgName,volName and type
                    for (Iterator i = entrySet.iterator(); i.hasNext();) {
                        Map.Entry entry = (Map.Entry) i.next();

                        String dgResourceName = (String) entry.getKey();
			HashMap entryMap = (HashMap) entry.getValue();

			String dgName = (String) entryMap.get(Util.DISKGROUP_NAME);
			String dgType = (String) entryMap.get(Util.DISKGROUP_TYPE);
			String volumes = (String) entryMap.get(Util.LOGICAL_DEVLIST);
                        // Get dgName, volName and dgType from the New Additions Map

                        disksData.add("");
                        disksData.add("");
                        disksData.add(dgName);
			if (volumes.equals(Util.ALL)) {
			    disksData.add(allDisks);
			} else {
			    disksData.add(volumes);
			}
                        fstypeList.add(dgType);
                        newAdditions.add(dgName);
                    }

                    String flag = (String) wizardModel.getWizardValue(
                            ORSWizardConstants.SHOW_DISKS_SUMMARY);

                    if (flag.equals("true")) {
                        TTYDisplay.clear(1);
                        TTYDisplay.printSubTitle(messages.getString(
			    "racstorage.racDisksPanel.summary",
			    new String[] { Util.convertArraytoString((String[])
				newAdditions.toArray(new String[0])) } ));
                    }

                    wizardModel.setWizardValue(
                        ORSWizardConstants.SHOW_DISKS_SUMMARY, "false");
                }
            } finally {

                try {
                    t.interrupt();
                    t.join();
                } catch (Exception exe) {
                }
            }

	    // Get the previous selections from SELECTED_DG_RESOURCES
	    // and NEW_DG_RESOURCES
            HashMap prevSelections = (HashMap) wizardModel.getWizardValue(
		Util.SELECTED_DG_RESOURCES);

	    List defSelection = getDefaultSelections(disksData, prevSelections);

	    HashMap newSelections = (HashMap) wizardModel.getWizardValue(
		Util.NEW_DG_RESOURCES);

	    defSelection.addAll(getDefaultSelections(disksData, newSelections));
	    
            TTYDisplay.clear(1);

            List optionsList = null;
            String enteredKey = "";
            String option[] = null;
            HashMap customTags = new HashMap();

            TTYDisplay.pageText(desc);
            TTYDisplay.clear(1);
            TTYDisplay.printSubTitle(messages.getString(
                    "racstorage.racDisksPanel.extraDesc"));

            String subheadings[] = {
                    dsResourceTitle, dsResourceGroupTitle, dsNameTitle, logicalDeviceTitle
                };

            customTags.put(dchar, done);
            customTags.put(cchar, createNewDisksets);
	    customTags.put(rchar, refresh);

            if ((disksData != null) && (disksData.size() > 0)) {

                if ((defSelection == null) || (defSelection.size() == 0)) {

                    // Display the (n*COLS) Table
                    option = TTYDisplay.create2DTable(subtitle, subheadings,
                            disksData, disksData.size() / COLS, COLS, customTags,
                            help, empty, false);
                } else {

                    // Display the (n*COLS) Table
                    option = TTYDisplay.create2DTable(subtitle, subheadings,
                            disksData, disksData.size() / COLS, COLS, customTags,
                            help, empty, false, defSelection);
                }

                if (option != null) {
                    optionsList = Arrays.asList(option);
                }
            } else {

                // Print empty text and automatically select 'Create New'
                TTYDisplay.displaySubheadings(subheadings);
                TTYDisplay.printSubTitle(empty);
                enteredKey = TTYDisplay.promptForEntry(table_empty_continue,
                        help_text);
                optionsList = new ArrayList();
                optionsList.add(cchar);

                // Set SHOW_CLUSTER_DEVICES so that wizardFlow
                // would take the flow to ClusterDevicesPanel
                wizardModel.setWizardValue(
                    ORSWizardConstants.SHOW_CLUSTER_DEVICES, "true");
                exitOption = yes;
            }

            // Handle Back
            if ((optionsList == null) || enteredKey.equals(back)) {
                this.cancelDirection = true;

                return;
            } else {

                if (optionsList.contains(rchar)) {

                    // refresh the data
                    wizardModel.setWizardValue(Util.SVMDEVICEGROUP, null);
                    wizardModel.setWizardValue(Util.RGM_VxVMDEVICEGROUP, null);
                    wizardModel.setWizardValue(
                        Util.SELECTED_DG_RESOURCES, null);
		    wizardModel.setWizardValue(
			Util.NEW_DG_RESOURCES, null);

                    continue;
                } else {

                    // Get the dgResourceNames selected
                    Map selectedDGResources = new HashMap();
		    Map dgMap = null;

                    for (Iterator i = optionsList.iterator(); i.hasNext();) {
                        String optionValue = (String) i.next();

                        try {
                            int value = Integer.parseInt(optionValue);
                            String dgName = (String) disksData.get((value * COLS) -
				2);

			    String volumes = (String) disksData.get((value * COLS) -
				1);

                            // Get the coressponding resource name from the
                            // vector
                            // Also get the dgType
                            String dgResourceName = (String) disksData.get(
				(value * COLS) - COLS);

			    String dgZCResourceName = null;
			    String dgBCResourceName = null;
			    String dgZCRGName = null;
			    String dgBCRGName = null;

                            if (dgResourceName.length() == 0) {
				// fetch the resource info from NEW_DG_RESOURCES
				// find in the map newSelections

				Map.Entry matchingEntry = null;
				if (volumes.equals(allDisks)) {
				    matchingEntry = getMatchingEntry(dgName,
					Util.ALL);
				} else {
				    matchingEntry = getMatchingEntry(dgName,
					volumes);
				}

				if (matchingEntry != null) {
				    selectedDGResources.put(matchingEntry.getKey(),
					matchingEntry.getValue());
				} else {
				    String dgType = (String) fstypeList.get(value - 1);
				    if (selectedZoneCluster != null &&
					selectedZoneCluster.trim().length() > 0) {

					Map storageMap = getStorageMap(clusterDiskGroupInfo,
					    dgName, volumes);
					ArrayList qfsVolumes = null;
					ArrayList svmVolumes = null;

					if (storageMap != null) {
					    qfsVolumes = (ArrayList)storageMap.get(Util.SQFS); 
					    svmVolumes = (ArrayList)storageMap.get(Util.QFS_SVM);
					}

					if (qfsVolumes != null &&
					    qfsVolumes.size() > 0) {

					    // create resources in the base cluster
					    dgBCResourceName =
						getResourceName_forScalDeviceGroup(
						    dgName, null);

					    dgBCRGName = getRGName_forScalDeviceGroup(
						dgBCResourceName, null);

					    dgMap = new HashMap();
					    dgMap.put(Util.DISKGROUP_NAME, dgName);
					    dgMap.put(Util.DISKGROUP_TYPE, dgType);
					    dgMap.put(Util.STORAGEGROUP, dgBCRGName);

					    String volList = Util.convertArraytoString(
						(String []) qfsVolumes.toArray(new String[0]));
					    dgMap.put(Util.LOGICAL_DEVLIST, volList);
					    selectedDGResources.put(dgBCResourceName, dgMap);
					}

					if (svmVolumes != null &&
					    svmVolumes.size() > 0) {

					    // create resources in the zone cluster
					    dgZCResourceName =
						getResourceName_forScalDeviceGroup(
						    dgName, selectedZoneCluster);

					    dgZCRGName = getRGName_forScalDeviceGroup(
						dgZCResourceName, selectedZoneCluster);

					    dgMap = new HashMap();
					    dgMap.put(Util.DISKGROUP_NAME, dgName);
					    dgMap.put(Util.DISKGROUP_TYPE, dgType);
					    dgMap.put(Util.STORAGEGROUP, dgZCRGName);

					    String volList = Util.convertArraytoString(
						(String []) svmVolumes.toArray(new String[0]));
					    if (volList.equals(allDisks)) {
						dgMap.put(Util.LOGICAL_DEVLIST, Util.ALL);
					    } else {
						dgMap.put(Util.LOGICAL_DEVLIST, volList);
					    }
					    selectedDGResources.put(selectedZoneCluster +
						Util.COLON + dgZCResourceName, dgMap);
					}
				    } else {
					// configuring for a base cluster

					dgBCResourceName =
					    getResourceName_forScalDeviceGroup(
						dgName, null);

					dgBCRGName = getRGName_forScalDeviceGroup(
					    dgBCResourceName, null);

					dgMap = new HashMap();
					dgMap.put(Util.DISKGROUP_NAME, dgName);
					dgMap.put(Util.DISKGROUP_TYPE, dgType);
					if (volumes != null) {
					    if (volumes.equals(allDisks)) {
						dgMap.put(Util.LOGICAL_DEVLIST, Util.ALL);
					    } else {
						dgMap.put(Util.LOGICAL_DEVLIST, volumes);
					    }
					}
					dgMap.put(Util.STORAGEGROUP, dgBCRGName);
					selectedDGResources.put(dgBCResourceName, dgMap);
				    }
				}
			    } else {
				// existing resource
				String dgType = (String) fstypeList.get(value - 1);

				dgMap = new HashMap();
				dgMap.put(Util.DISKGROUP_NAME, dgName);
				dgMap.put(Util.DISKGROUP_TYPE, dgType);
				if (volumes != null) {
				    if (volumes.equals(allDisks)) {
					dgMap.put(Util.LOGICAL_DEVLIST, Util.ALL);
				    } else {
					dgMap.put(Util.LOGICAL_DEVLIST, volumes);
				    }
				}

				if (selectedZoneCluster != null &&
				    selectedZoneCluster.trim().length() > 0) {

				    // check if configuring sQFS/SVM or sQFS with hardware RAID

				    Map storageMap = getStorageMap(clusterDiskGroupInfo,
					dgName, volumes);

				    ArrayList qfsVolumes = null;
				    ArrayList svmVolumes = null;

				    if (storageMap != null) {
					qfsVolumes = (ArrayList)storageMap.get(Util.SQFS); 
					svmVolumes = (ArrayList)storageMap.get(Util.QFS_SVM);
				    }

				    if (qfsVolumes != null &&
					qfsVolumes.size() > 0) {

					// create resources in the base cluster
					selectedDGResources.put(dgResourceName, dgMap);
				    }

				    if (svmVolumes != null &&
					svmVolumes.size() > 0) {

					// create resources in the zone cluster
					selectedDGResources.put(selectedZoneCluster +
					    Util.COLON + dgResourceName, dgMap);
				    }
				} else {
				    // configuring for a base cluster
				    selectedDGResources.put( dgResourceName, dgMap);
				}
			    }
                        } catch (NumberFormatException nfe) {

                            // Custom Tags
                            if (optionValue.equals(cchar)) {

                                // Set SHOW_CLUSTER_DEVICES so that wizardFlow
                                // would take the flow to ClusterDevicesPanel
                                wizardModel.setWizardValue(
                                    ORSWizardConstants.SHOW_CLUSTER_DEVICES,
                                    "true");
                                wizardModel.setWizardValue(
                                    Util.SELECTED_DG_RESOURCES,
                                    selectedDGResources);
                                exitOption = yes;
                            }
                        }
                    }

                    // Store selection
                    if (optionsList.contains(dchar)) {
                        exitOption = yes;

                        if (selectedDGResources.size() == 0) {
                            wizardModel.setWizardValue(
                                Util.SELECTED_DG_RESOURCES, null);
                        } else {

                            // Store the user selected ScalDeviceGroupResources
                            // in the wizardModel
                            wizardModel.setWizardValue(
                                Util.SELECTED_DG_RESOURCES,
                                selectedDGResources);
                            // For every diskgroup fill the DiskGroup
                            // Information
                            fillDiskGroupInfo(selectedDGResources);
                        }
                    }
                }
            }
        } while (!exitOption.equalsIgnoreCase(yes));

        return;
    }

    HashMap getStorageMap(ClusterDiskGroupInfo clusterDiskGroupInfo, String dgName,
	String volumes) {

	// Given a dgName find out if it is SVM or
	// QFS_SVM
	// this is needed only for a Zone Cluster case
	List diskGroupInfoList = clusterDiskGroupInfo.getDiskGroupInfoList();

	DiskGroupInfo diskGroupInfo = null;

	if (diskGroupInfoList != null) {
	    int size = diskGroupInfoList.size();
	    boolean found = false;
	    int j = 0;

	    while (j < size && !found) {
		diskGroupInfo = (DiskGroupInfo)diskGroupInfoList.get(j);

		String diskGroupName = diskGroupInfo.getDiskGroupName();

		if (diskGroupName.equals(dgName)) {
		    found = true;
		} else {
		    j++;
		}
	    }
	}

	ArrayList qfsVolumes = null;
	ArrayList svmVolumes = null;

	if (diskGroupInfo != null) {
	    // find selected volume in the availSVMMap
	    // and availQfsSVMVolMap

	    HashMap availSVMVolMap = diskGroupInfo.getAvailSVMVolMap();

	    HashMap availQfsSVMVolMap = diskGroupInfo.getAvailQfsSVMVolMap(); 

	    String[] volumeArray = volumes.split(Util.COMMA);

	    if (volumeArray != null) {
		for (int j = 0; j < volumeArray.length; j++) {
		    if (availSVMVolMap != null &&
			availSVMVolMap.containsKey(volumeArray[j])) {
			if (svmVolumes == null) {
			    svmVolumes = new ArrayList();
			}

			if (!svmVolumes.contains(volumeArray[j])) {
			    svmVolumes.add(volumeArray[j]);
			}
		    } else if (availQfsSVMVolMap != null &&
			availQfsSVMVolMap.containsKey(volumeArray[j])) {
			if (qfsVolumes == null) {
			    qfsVolumes = new ArrayList();
			}

			if (!qfsVolumes.contains(volumeArray[j])) {
			    qfsVolumes.add(volumeArray[j]);
			}

		    }
		}
	    }
	}
	HashMap storageMap = new HashMap();
	storageMap.put(Util.SQFS, qfsVolumes);
	storageMap.put(Util.QFS_SVM, svmVolumes);

	return storageMap;
    }



    private Vector getDiskGroupData(String dgName, String selectedZoneCluster,
	HashMap rsMap, List fstypeList, String storageScheme) {
	Vector disksData = new Vector();
	String nodeEndPoint = (String) wizardModel.getWizardValue(
	    Util.NODE_TO_CONNECT);
	if (rsMap != null) {
	    Set entrySet = rsMap.entrySet();
	    for (Iterator i = entrySet.iterator(); i.hasNext();) {
		Map.Entry entry = (Map.Entry) i.next();
		String dgResourceName = (String) entry.getKey();
		List volumeList = (List) entry.getValue();

		String rsGenKey = null;
		if (dgResourceName == null) {
		    rsGenKey = dgName;
		} else {
		    rsGenKey = dgResourceName;
		}

		// Get the resource group for the Disk Resource
		String propertyName[] = new String[] {
		    Util.STORAGEGROUP
		};

		HashMap userInput = new HashMap();

		if (storageScheme.equals(Util.SVM)) {
		    userInput.put(Util.SEL_ZONE_CLUSTER, selectedZoneCluster);
		}

		userInput.put(Util.RS_NAME, rsGenKey);

		Map resourceGroupMap = (HashMap)racModel.
		    discoverPossibilities(null, nodeEndPoint,
			propertyName, userInput);

		disksData.add(dgResourceName);
		disksData.add((String) resourceGroupMap.get(
		    Util.STORAGEGROUP));
		disksData.add(dgName);

		String[] volumeArr = (String[]) volumeList.toArray(new String[0]);
		String volumes = Util.convertArraytoString(volumeArr); 
		disksData.add(volumes);
		fstypeList.add(Util.SVMDEVICEGROUP);
	    }
	}
	return disksData;
    }

    private List getDefaultSelections(Vector disksData, HashMap prevSelections) {
	String allDisks = messages.getString("racstorage.racDisksPanel.allDisks");

	List defSelection = new ArrayList();
	// populate the default selection list with the
	// previous selections
	if (prevSelections != null) {
	    Set keySet = prevSelections.keySet();
	    Iterator iterator = keySet.iterator();
	    while (iterator.hasNext()) {
		String key = (String) iterator.next();
		int tableSize = disksData.size()/COLS + 1;
		boolean found = false;
		int j = 1;
		while (j < tableSize && !found) {
		    HashMap prevDGMap = (HashMap) prevSelections.get(key);
		    String prevDGName = (String) prevDGMap.get(
			Util.DISKGROUP_NAME); 
		    String prevVolumes = (String) prevDGMap.get(
			Util.LOGICAL_DEVLIST);

		    String[] splitStr = prevVolumes.split(Util.COMMA);
		    List arrayList = Arrays.asList(splitStr);

		    String dgName = (String) disksData.get((j * COLS) - 2);

		    String volumeList = (String) disksData.get((j * COLS) - 1);
		    if (volumeList.equals(allDisks)) {
			volumeList = Util.ALL;
		    }

		    splitStr = volumeList.split(Util.COMMA);
		    List volArray = Arrays.asList(splitStr);

		    if (prevDGName.equals(dgName) &&
			volArray.containsAll(arrayList)) {

			found = true;
			defSelection.add(Integer.toString(j));
		    }
		    j++;
		}
	    }
	}
	return defSelection;
    }

    // Gets a Map of "ScalDeviceGroup" resources monitoring OBAN disksets
    // for a given nodelist
    private ClusterDiskGroupInfo getDeviceGroupResources_forOban(
	String nodelist[], HashMap helperData) {

	racModel = RACInvokerCreator.getRACModel();
	String nodeEndPoint = (String) wizardModel.getWizardValue(
	    Util.NODE_TO_CONNECT);

        // Get the RGM Managed OBAN DiskGroups
        String propertyNames[] = { Util.SVMDEVICEGROUP };

        Map discoveredMap = racModel.discoverPossibilities(
	    null, nodeEndPoint, nodelist, propertyNames, helperData);

        ClusterDiskGroupInfo clusterDiskGroupInfo = (ClusterDiskGroupInfo)
	    discoveredMap.get(Util.SVMDEVICEGROUP);

        wizardModel.setWizardValue(Util.SVMDEVICEGROUP, clusterDiskGroupInfo);

        return clusterDiskGroupInfo;
    }

    // Gets a Map of "ScalDeviceGroup" resources monitoring VxVM disksets
    // for a given nodelist
    private Map getDeviceGroupResources_forVxVM(String nodelist[]) {

	racModel = RACInvokerCreator.getRACModel();
	String nodeEndPoint = (String) wizardModel.getWizardValue(
	    Util.NODE_TO_CONNECT);

        // Get the RGM Managed OBAN DiskGroups
        String propertyNames[] = { Util.VxVMDEVICEGROUP };
        Map discoveredMap = racModel.discoverPossibilities(
		null, nodeEndPoint, propertyNames, nodelist);

        Map rgmVxVmDiskGroupMap = (HashMap) discoveredMap.get(
                Util.RGM_VxVMDEVICEGROUP);

	Map nonRgmVxVmDiskGroupMap = (HashMap) discoveredMap.get(
		Util.NONRGM_VxVMDEVICEGROUP);

	wizardModel.setWizardValue(Util.NONRGM_VxVMDEVICEGROUP, nonRgmVxVmDiskGroupMap);
        
	return rgmVxVmDiskGroupMap;
    }

    /**
     * For every GDD Disk Resource selected we fill the Information Map
     * DiskGroup_Name - Name of the DiskGroup DiskGroup_TYPE - Oban or CVM
     * StorageGroup - The storage group of the resource
     *
     * @param  Map  of selectedFSResources
     */
    private void fillDiskGroupInfo(Map selectedDGResources) {
        Map infoMap = null;
        Iterator i = selectedDGResources.keySet().iterator();
        String rgName = null;

	racModel = RACInvokerCreator.getRACModel();
	String nodeEndPoint = (String) wizardModel.getWizardValue(
	    Util.NODE_TO_CONNECT);

	HashMap helperData = new HashMap();

        while (i.hasNext()) {
            String key = (String) i.next();
	    String diskGroupResourceName = null;
	    infoMap = (HashMap) selectedDGResources.get(key);

	    // the key for a ZC would be a ZC:resourceName
	    String[] splitStr = key.split(Util.COLON);
	    String selectedZoneCluster = null;
	    if (splitStr.length == 2) {
		diskGroupResourceName = splitStr[Util.R_INDEX];
		selectedZoneCluster = splitStr[Util.ZC_INDEX];
	    } else {
		diskGroupResourceName = key;
	    }

	    rgName = (String) infoMap.get(Util.STORAGEGROUP);

            // Get the resource group for the Disk Resource
            if (rgName == null) {
                String propertyName[] = new String[] { Util.STORAGEGROUP };
		helperData.put(Util.SEL_ZONE_CLUSTER, selectedZoneCluster);
		helperData.put(Util.RS_NAME, diskGroupResourceName);
                HashMap resourceGroupMap = (HashMap) racModel.discoverPossibilities(
		    null, nodeEndPoint, propertyName, helperData);

		if (resourceGroupMap != null) {
		    rgName = (String) resourceGroupMap.get(Util.STORAGEGROUP);
		    resourceGroupMap.put(Util.STORAGEGROUP, rgName);
		}

		infoMap.putAll(resourceGroupMap);
	    }

            wizardModel.setWizardValue(key, infoMap);
        }
    }

    // Get a unique name for Scalable Device Group
    private String getResourceName_forScalDeviceGroup(
	String dgName, String selectedZoneCluster) {
        String propertyName[] = new String[] { Util.STORAGERESOURCE };

	racModel = RACInvokerCreator.getRACModel();
	String nodeEndPoint = (String) wizardModel.getWizardValue(
	    Util.NODE_TO_CONNECT);

	HashMap helperData = new HashMap();
	helperData.put(Util.SEL_ZONE_CLUSTER, selectedZoneCluster);
	helperData.put(Util.PREFIX, dgName);

        Map discoveredMap = racModel.discoverPossibilities(
	    null, nodeEndPoint, propertyName, helperData);

        return (String) discoveredMap.get(Util.STORAGERESOURCE);
    }

  // Get a unique RGName for Scalable Device Group
    private String getRGName_forScalDeviceGroup(
	String resourceName, String selectedZoneCluster) {

	RACModel racModel = RACInvokerCreator.getRACModel();
	String nodeEndPoint = (String) wizardModel.getWizardValue(
	    Util.NODE_TO_CONNECT);

	HashMap helperData = new HashMap();
	helperData.put(Util.SEL_ZONE_CLUSTER, selectedZoneCluster);
	helperData.put(Util.RS_NAME, resourceName);

	String propertyName[] = new String[] { Util.STORAGEGROUP };
	HashMap resourceGroupMap = (HashMap) racModel.discoverPossibilities(
	    null, nodeEndPoint, propertyName, helperData);

	return (String) resourceGroupMap.get(Util.STORAGEGROUP);
    }

    private Map.Entry getMatchingEntry(String dgName, String volumes) {
	HashMap newSelectionsMap = (HashMap) wizardModel.getWizardValue(
	    Util.NEW_DG_RESOURCES);

	HashMap matchingEntry = null;
	Map.Entry entry = null;
	String key = null;
        boolean found = false;

	String[] splitStr = volumes.split(Util.COMMA);
	List volArray = Arrays.asList(splitStr);

	if (newSelectionsMap != null) {
	    Set entrySet = newSelectionsMap.entrySet();
	    Iterator iterator = entrySet.iterator();
	    found = false;

	    while (iterator.hasNext() && !found) {
		entry = (Map.Entry) iterator.next();
		matchingEntry = (HashMap) entry.getValue();

		String mapDGName = (String) matchingEntry.get(Util.DISKGROUP_NAME);
		String mapVolumes = (String) matchingEntry.get(Util.LOGICAL_DEVLIST);

		splitStr = mapVolumes.split(Util.COMMA);
		List mapVolArray = Arrays.asList(splitStr);

		if (dgName.equals(mapDGName) &&
		    volArray.containsAll(mapVolArray)) {
		    found = true;
		}
	    }
	}

	if (found) {
	    return entry;
	}
	return null;
    }
}
