/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)NFSNodeSelectionPanel.java 1.12     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.nfs;

// J2SE
import com.sun.cluster.agent.dataservices.nfs.NFSMBean;

// CMASS
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.NodeSelectionPanel;

// Wizard Common
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.management.remote.JMXConnector;


/**
 * This panel gets the NodeList from the User
 */
public class NFSNodeSelectionPanel extends NodeSelectionPanel {

    private String nfs;

    /**
     * Creates a new instance of NFSNodeSelectionPanel
     */
    public NFSNodeSelectionPanel() {
    }

    /**
     * Creates an NFSNodeSelectionPanel Panel with the given name and manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public NFSNodeSelectionPanel(String name, WizardTreeManager manager) {
        super(name, manager);
    }

    /**
     * Creates an NFSNodeSelectionPanelPanel for the wizard with the given name
     * and associates it with the WizardModel and WizardFlow.
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     */
    public NFSNodeSelectionPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(name, model, wizardFlow, wizardi18n);
        initStrings();
    }

    private void initStrings() {
        desc = wizardi18n.getString("nfs.nodeSelectionPanel.desc");
        nfs = wizardi18n.getString("dswizards.nfs.wizard");
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        wizardModel.selectCurrWizardContext();

        String nodeList[] = displayPanel(NFSMBean.class, nfs);

        if (nodeList == null) {
            this.cancelDirection = true;

            return;
        }

        wizardModel.setWizardValue(Util.RG_NODELIST, nodeList);

        initWizardModel();

        // Set the wizard state key
        ArrayList propNames = new ArrayList();
        Object tmpObj = (ArrayList) wizardModel.getWizardValue(
                Util.WIZARD_STATE);

        if (tmpObj != null)
            propNames = (ArrayList) tmpObj;

        propNames.add(Util.RG_NODELIST);
        wizardModel.setWizardValue(Util.WIZARD_STATE, propNames);

    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }

    /**
     * Initializes the Wizard Model by filling with the PropertyNames and
     * Discovered values. The Previous Values entered by the user are retrieved
     * and stored in the previous context.
     */
    public void initWizardModel() {

        // Contact the NFSMBean and Get All Properties
        NFSMBean nfsMBean = null;
        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);
        JMXConnector connector = getConnection(nodeEndPoint);
        nfsMBean = NFSWizardCreator.getNFSMBeanOnNode(connector);

        if (nfsMBean != null) {

            // Get the Discoverable Properties For NFS Wizard
            String propertyNames[] = { Util.PROPERTY_NAMES };
            HashMap discoveredMap = nfsMBean.discoverPossibilities(
                    propertyNames, null);

            // Copy the Data to the WizardModel
            if (discoveredMap != null) {
                propertyNames = new String[discoveredMap.size()];

                Set entrySet = discoveredMap.entrySet();
                Iterator setIterator = entrySet.iterator();
                int i = 0;

                while (setIterator.hasNext()) {
                    Map.Entry entry = (Map.Entry) setIterator.next();
                    wizardModel.setWizardDefaultValue((String) entry.getKey(),
                        entry.getValue());
                    propertyNames[i++] = (String) entry.getKey();
                }
            }

            wizardModel.setWizardValue(Util.PROPERTY_NAMES, propertyNames);
        }
    }
}
