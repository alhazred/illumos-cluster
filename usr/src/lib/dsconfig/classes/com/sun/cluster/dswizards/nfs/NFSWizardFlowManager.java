/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)NFSWizardFlowManager.java 1.8     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.nfs;

// Java
import com.sun.cluster.agent.dataservices.hastorageplus.HAStoragePlusMBean;

// CMASS
import com.sun.cluster.agent.dataservices.logicalhost.LogicalHostMBean;
import com.sun.cluster.agent.dataservices.utils.Util;

// CLI SDK
import com.sun.cluster.dswizards.common.DSConfigWizardModel;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.DSWizardInterface;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.hastorageplus.HASPWizardConstants;
import com.sun.cluster.dswizards.hastorageplus.HASPWizardCreator;

// CLI Wizards
import com.sun.cluster.dswizards.logicalhost.LogicalHostWizardCreator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The class takes decision on which panel to show "next" depending upon the
 * conditions set at a particular context.
 */
public class NFSWizardFlowManager implements DSWizardInterface {

    /**
     * Creates a new instance of NFSWizardFlowManager
     */
    public NFSWizardFlowManager() {
    }

    /**
     * @see  com.sun.cluster.dswizards.common.DSWizardInterface#getNextPanel
     */
    public String getNextPanel(String curPanelName,
        Object wizCxtObj, String options[]) {

	DSConfigWizardModel guiModel = null;
	CliDSConfigWizardModel cliModel = null;

	if (wizCxtObj instanceof CliDSConfigWizardModel) {
	    cliModel = (CliDSConfigWizardModel)wizCxtObj;
	} else if (wizCxtObj instanceof DSConfigWizardModel) {
	    guiModel = (DSConfigWizardModel)wizCxtObj;
	}

        // Get the wizardi18n object
        WizardI18N wizardi18n = null;
	if (guiModel != null) {
	    wizardi18n = (WizardI18N)guiModel.getWizardValue(
		NFSWizardConstants.WIZARDI18N);
	} else {
	    wizardi18n = (WizardI18N)cliModel.getWizardValue(
		NFSWizardConstants.WIZARDI18N);
	}

        // Check if panels for creating a new logical host name resource are to
        // be displayed.
        if (curPanelName.equals(NFSWizardConstants.PANEL_2)) {
            String showlhwiz = null;
	    if (guiModel != null) {
		showlhwiz = (String)guiModel.getWizardValue(
		    NFSWizardConstants.SHOWLHWIZ);
	    } else {
		showlhwiz = (String)cliModel.getWizardValue(
		    NFSWizardConstants.SHOWLHWIZ);
	    }

            if (showlhwiz.equals("true")) {
                return options[1];
            }

            return options[0];

            // Check if panels for creating a new HASP resource are to be
            // displayed.
        } else if (curPanelName.equals(NFSWizardConstants.PANEL_3)) {
            String showhaspwiz = null;
            String showPathPrefix = null;
            String newRGFlag = null;
	    if (guiModel != null) {
		showhaspwiz = (String)guiModel.getWizardValue(
		    NFSWizardConstants.SHOWHASPWIZ);
		showPathPrefix = (String)guiModel.getWizardValue(
		    NFSWizardConstants.SHOWPATHPREFIX);
		newRGFlag = (String)guiModel.getWizardValue(
		    NFSWizardConstants.NEWRGFLAG);
		if (showhaspwiz.equals("true")) {
		    guiModel.setWizardValue(
			HASPWizardConstants.HASP_LOCATION_PREF,
			HASPWizardConstants.FS_ONLY);
		}
	    } else {
		showhaspwiz = (String)cliModel.getWizardValue(
		    NFSWizardConstants.SHOWHASPWIZ);
		showPathPrefix = (String)cliModel.getWizardValue(
		    NFSWizardConstants.SHOWPATHPREFIX);
		newRGFlag = (String)cliModel.getWizardValue(
		    NFSWizardConstants.NEWRGFLAG);
		if (showhaspwiz.equals("true")) {
		    cliModel.setWizardValue(
			HASPWizardConstants.HASP_LOCATION_PREF,
			HASPWizardConstants.FS_ONLY);
		}
	    }

            if (showhaspwiz.equals("true")) {

                return options[2];
            }

            if (showPathPrefix.equals("true") || newRGFlag.equals("true")) {
                return options[1];
            }

            return options[0];
        }

        return null;
    }
}
