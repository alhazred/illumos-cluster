/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ORFClusterWareSelectionPanel.java	1.5	08/12/10 SMI"
 */

package com.sun.cluster.dswizards.oraclerac.framework;

// CLI wizard
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.DSWizardUtil;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * This panel gets the cluster ware support intended. 
 */
public class ORFClusterWareSelectionPanel extends WizardLeaf {

    private String panelName; // Panel Name
    protected CliDSConfigWizardModel wizardModel; // WizardModel
    protected WizardI18N wizardi18n; // The resource bundle

    /**
     * Creates a new instance of ORFClusterWareSelectionPanel
     */
    public ORFClusterWareSelectionPanel() {
    }

    /**
     * Creates an ORFClusterWareSelectionPanel Panel with the given name and manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public ORFClusterWareSelectionPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates an ORFClusterWareSelectionPanelPanel for the wizard with the given
     * name and associates it with the WizardModel and WizardFlow.
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     */
    public ORFClusterWareSelectionPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;
        this.wizardi18n = wizardi18n;
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        String title = wizardi18n.getString("orf.clusterWareSelectionPanel.title");
        String pref = wizardi18n.getString("orf.clusterWareSelectionPanel.preference");
        String help_text = wizardi18n.getString(
                "orf.clusterWareSelectionPanel.cli.help");
	String option1 = wizardi18n.getString(
		"orf.clusterWareSelectionPanel.option1");
	String option2 = wizardi18n.getString(
		"orf.clusterWareSelectionPanel.option2");
	String error_txt = wizardi18n.getString(
		"orf.clusterWareSelectionPanel.error");
	String continue_txt = wizardi18n.getString(
		"cliwizards.continue");

        // Get previously selected option
        String prevSelection = (String) wizardModel.getWizardValue(
		DSWizardUtil.CLUSTERWARE_SUPPORT);

	int defSelection = 0;
	int index = -1;
	boolean bContinue = true;
	String selection = null;

	if (prevSelection == null) {
	    // default selection is native vendor clusterware support
	    prevSelection = new String(DSWizardUtil.NATIVE_SUPPORT);
	}
	try {
	    defSelection = Integer.parseInt(prevSelection);
	} catch (NumberFormatException nfe) {
	    defSelection = 0;
	}

	String[] options = new String[] { option1, option2 };

	// Enable navigation
	TTYDisplay.setNavEnabled(true);
	this.cancelDirection = false;
	TTYDisplay.clearScreen();
	TTYDisplay.clear(1);

	while (bContinue) {
	    // Title
	    TTYDisplay.printTitle(title);
	    TTYDisplay.clear(1);

	    index = TTYDisplay.getMenuOption(pref, options, defSelection);

	    if (index == TTYDisplay.BACK_PRESSED) {
		this.cancelDirection = true;
		return;
	    }

	    selection = Integer.toString(index);
	    if (selection.equals(DSWizardUtil.UDLM_SUPPORT)) {
		// This panel is displayed only when UDLM packages are not present.
		// Should be an error condition
		TTYDisplay.printError(error_txt);
		TTYDisplay.clear(1);
		TTYDisplay.promptForEntry(continue_txt);
	    } else {
		 bContinue = false;
	    }
	}

	wizardModel.setWizardValue(DSWizardUtil.CLUSTERWARE_SUPPORT, selection);
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#isDisplayComplete
     */
    public boolean isDisplayComplete() {
        return true;
    }
}
