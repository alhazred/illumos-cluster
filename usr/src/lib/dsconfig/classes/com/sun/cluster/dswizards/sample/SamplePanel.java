/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)SamplePanel.java 1.12     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.sample;

import com.sun.cluster.dswizards.clisdk.core.*;
import com.sun.cluster.dswizards.common.*;


public class SamplePanel extends WizardLeaf {
    public SamplePanel() {
        super();
    }

    public SamplePanel(String name, WizardTreeManager manager) {
        super(name, manager);
    }

    public SamplePanel(CliDSConfigWizardModel wizardModel, String name) {
        super(wizardModel, name, null);
    }


    public boolean skip() {
        return false;
    }

    public void consoleInteraction() {
        TTYDisplay.showNewline();
        TTYDisplay.showText("console interaction " + name);
        TTYDisplay.showNewline();

        TTYDisplay.setNavEnabled(true);

        String optionArr[] = new String[2];
        optionArr[0] = "yes";
        optionArr[1] = "no";

        String option =
            (TTYDisplay.queryValue("Install core", optionArr[1], optionArr,
                    null, 0)).trim();

        if ((option != null) &&
                (option.equalsIgnoreCase(TTYDisplay.backOption))) {
            System.out.println("Back button pressed");
            wizardManager.backButtonPressed();

            return;
        }

        option =
            (TTYDisplay.queryValue("Install connectors", optionArr[1],
                    optionArr, null, 0)).trim();

        if ((option != null) &&
                (option.equalsIgnoreCase(TTYDisplay.backOption))) {
            System.out.println("Back button pressed");
            wizardManager.backButtonPressed();

            return;
        }
    }

    public boolean isDisplayComplete() {
        return true;
    }
}
