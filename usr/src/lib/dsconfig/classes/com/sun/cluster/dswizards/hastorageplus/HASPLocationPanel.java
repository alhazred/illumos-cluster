/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)HASPLocationPanel.java 1.16     08/12/10 SMI"
 */


package com.sun.cluster.dswizards.hastorageplus;

import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.hastorageplus.HAStoragePlusMBean;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;

public class HASPLocationPanel extends WizardLeaf {


    private String panelName; // Panel Name
    private WizardI18N wizardi18n; // Message Bundle
    private CliDSConfigWizardModel wizardModel; // WizardModel


    /**
     * Creates a new instance of HASPLocationPanel
     */
    public HASPLocationPanel() {
        super();
    }

    /**
     * Creates a HASPLocationPanel Panel with the given name and manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public HASPLocationPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    public HASPLocationPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.wizardModel = model;
        this.panelName = name;
        this.wizardi18n = wizardi18n;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.
     *		WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        String title = wizardi18n.getString("hasp.locationPanel.title");
        String desc = wizardi18n.getString("hasp.locationPanel.desc");
        String option1 = wizardi18n.getString("hasp.locationPanel.option1");
        String option2 = wizardi18n.getString("hasp.locationPanel.option2");
        String option3 = wizardi18n.getString("hasp.locationPanel.option3");
        String locpref_txt = wizardi18n.getString(
                "hasp.locationPanel.preference");
        String help_text = wizardi18n.getString("hasp.locationPanel.cli.help");

        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        // Title
        TTYDisplay.printTitle(title);

        // Description Para
        TTYDisplay.printSubTitle(desc);

        String options[] = { option1, option2, option3 };

        // Get the data preference
        int index = TTYDisplay.getMenuOption(locpref_txt, options, 2,
                help_text);

        if (index == TTYDisplay.BACK_PRESSED) {
            this.cancelDirection = true;

            return;
        }

        wizardModel.selectCurrWizardContext();

        String selection = "";

        if (options[index].equals(option1)) {

            // shared file system
            selection = HASPWizardConstants.FS_ONLY;
        } else if (options[index].equals(option2)) {

            // shared device
            selection = HASPWizardConstants.DG_ONLY;
        } else if (options[index].equals(option3)) {

            // both
            selection = HASPWizardConstants.FS_AND_DG;
        }

        wizardModel.setWizardValue(HASPWizardConstants.HASP_LOCATION_PREF,
            selection);
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#isDisplayComplete
     */
    public boolean isDisplayComplete() {
        return true;
    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }
}
