/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)ClusterDisksPanel.java	1.15	09/04/22 SMI"
 */

package com.sun.cluster.dswizards.oraclerac.storage;

// CMASS
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.dataservices.utils.DiskGroupInfo;
import com.sun.cluster.agent.dataservices.utils.ClusterDiskGroupInfo;
import com.sun.cluster.agent.zonecluster.ZoneClusterData;

// CLI SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;

// Wizard Common
import com.sun.cluster.dswizards.common.CliUtil;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.oraclerac.invoker.RACInvokerCreator;

import com.sun.cluster.model.ZoneClusterModel;
import com.sun.cluster.model.DataModel;
import com.sun.cluster.model.RACModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;


/**
 * This panel lists to the user all the OBAN,Shared VxVM diskgroups in the
 * cluster that are NOT monitored by ScalDeviceGroup resources
 */
public class ClusterDisksPanel extends WizardLeaf {


    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N messages; // Message Bundle

    private RACModel racModel = null;

    /**
     * Creates a new instance of ClusterDisksPanel
     */
    public ClusterDisksPanel() {
        super();
    }

    /**
     * Creates an ClusterDisks Panel with the given name and tree manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public ClusterDisksPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates an ClusterDisks Panel for racstorage Wizard with the given name
     * and associates it with the WizardModel and WizardFlow
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     */
    public ClusterDisksPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N messages) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;

        // Get the I18N instance
        this.messages = messages;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {

        String title = messages.getString("racstorage.clusterDisksPanel.title");
        String desc = messages.getString("racstorage.clusterDisksPanel.desc");
        String subTitle = messages.getString(
                "racstorage.clusterDisksPanel.subTitle");
        String disksetTitle = messages.getString(
                "racstorage.clusterDisksPanel.diskSet");
        String accessibleFromTitle = messages.getString(
                "racstorage.clusterDisksPanel.accessibleFrom");
        String help = messages.getString(
                "racstorage.clusterDisksPanel.cli.help");
        String dchar = messages.getString("ttydisplay.d.char");
        String done = messages.getString("ttydisplay.menu.done");
        String rchar = messages.getString("ttydisplay.r.char");
        String yes = messages.getString("cliwizards.yes");
        String no = messages.getString("cliwizards.no");
        String confirmMessage = messages.getString("cliwizards.confirmMessage");
        String refresh = messages.getString("ttydisplay.menu.refresh");
        String confirmDisks = messages.getString(
                "racstorage.clusterDisksPanel.confirmDisks");
        String dsTypeTitle = messages.getString(
                "racstorage.racDisksPanel.diskSetType");
        String empty = messages.getString("racstorage.clusterDisksPanel.empty");
        String emptyDesc = messages.getString(
                "racstorage.clusterDisksPanel.empty.desc");
        String emptySelection = messages.getString(
                "racstorage.clusterDisksPanel.emptySelection");
        String continueConfirm = messages.getString(
                "cliwizards.continueConfirm");
        String continue_txt = messages.getString("ttydisplay.msg.cli.enter");
        String back = messages.getString("ttydisplay.back.char");
	String preference = messages.getString("racstorage.clusterDisksPanel.preference");

        String baseClusterNodeList[] = (String[]) wizardModel.getWizardValue(
                Util.BASE_CLUSTER_NODELIST);
        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);

	RACModel racModel = RACInvokerCreator.getRACModel();

	HashMap helperData = new HashMap();
	HashMap fsTabMap = null;
	ArrayList fsTabList = null;
	HashMap devTabMap = null;
	ArrayList devTabList = null;
	ZoneClusterData zcData = null;

	String selectedZoneCluster = CliUtil.getZoneClusterName(wizardModel);
	helperData.put(Util.SEL_ZONE_CLUSTER, selectedZoneCluster);

	DataModel dm = DataModel.getDataModel(null);
	ZoneClusterModel zm = dm.getZoneClusterModel();

	String[] zoneClusterList = null;
	try {
	    zoneClusterList = zm.getZoneClusterList(null);

	    if (zoneClusterList != null) {
		for (int i = 0; i < zoneClusterList.length; i++) {
		    zcData = zm.getZoneClusterData(null, zoneClusterList[i]);
		    devTabList = zcData.getZoneClusterDevTabList();

		    fsTabList = zcData.getZoneClusterFSTabList();

		    if (fsTabMap == null) {
			fsTabMap = new HashMap();
		    }
		    fsTabMap.put(zoneClusterList[i], fsTabList);

		    if (devTabMap == null) {
			devTabMap = new HashMap();
		    }

		    devTabMap.put(zoneClusterList[i], devTabList);
		}
	    }

	} catch (Exception e) {
	}
	// Pass the devtab list of all the zone clusters
	helperData.put(Util.ZONE_CLUSTER_DEVTAB_LIST, devTabMap);
	// Pass the fstab list of all the zone clusters
	helperData.put(Util.ZONE_CLUSTER_FSTAB_LIST, fsTabMap);
	helperData.put(Util.ZONE_CLUSTER_LIST, zoneClusterList);

	TTYDisplay.clearScreen();
	TTYDisplay.clear(1);

	// Enable Back and Help
	TTYDisplay.setNavEnabled(true);
	this.cancelDirection = false;

	// Depending on the Storage Schemes get the respective
	// DeviceGroups monitored by them.
	ClusterDiskGroupInfo clusterDiskGroupInfo = null;
	Map vxvmMap = null;

	wizardModel.setWizardValue(ORSWizardConstants.SHOW_DISKS_SUMMARY,
	    "false");

	// Depending on the Storage Schemes get the respective
	// Resources and the DeviceGroups monitored by them.
	String keyPrefix = (String) wizardModel.getWizardValue(
	    Util.KEY_PREFIX);

	List storageScheme = (ArrayList) wizardModel.getWizardValue(
	    keyPrefix + Util.ORF_STORAGE_MGMT_SCHEMES);

	Thread t = null;

	HashMap customTags = new HashMap();
	customTags.put(dchar, done);
	customTags.put(rchar, refresh);

	String subheadings[] = {
	    disksetTitle, accessibleFromTitle, dsTypeTitle
	};


	while (true) {
	    // Populate the table
	    Vector disksData = new Vector();

	    clusterDiskGroupInfo = (ClusterDiskGroupInfo) wizardModel.getWizardValue(
		    Util.SVMDEVICEGROUP);
	    vxvmMap = (Map) wizardModel.getWizardValue(
		    Util.NONRGM_VxVMDEVICEGROUP);

	    try {
		t = TTYDisplay.busy(messages.getString(
			    "racstorage.clusterDisksPanel.discovery.busy"));
		t.start();

		if (storageScheme.contains(Util.SVM) ||
		    storageScheme.contains(Util.QFS_SVM)) {
		    if (clusterDiskGroupInfo == null) {
			clusterDiskGroupInfo = getDeviceGroups_forOban(
			    baseClusterNodeList, helperData);
		    }
		}

		if (storageScheme.contains(Util.VXVM)) {

		    if (vxvmMap == null) {
			vxvmMap = getDeviceGroups_forVxVM(baseClusterNodeList);
			wizardModel.setWizardValue(Util.NONRGM_VxVMDEVICEGROUP,
			    vxvmMap);
		    }
		}

		// The Maps pertaining to the storage schemes are added to
		// the final vector that is displayed.
		if (clusterDiskGroupInfo != null) {

		    List diskGroups = clusterDiskGroupInfo.getDiskGroupInfoList();
		    if (diskGroups != null) {
			int size = diskGroups.size();
			for (int i = 0; i < size; i++) {
			    DiskGroupInfo diskGroup = (DiskGroupInfo) diskGroups.get(i);

			    String dgName = diskGroup.getDiskGroupName();
			    String primaryNode = diskGroup.getPrimaryNodeName();

			    HashMap availSVMVolMap = diskGroup.getAvailSVMVolMap();
			    HashMap availQfsSVMVolMap = diskGroup.getAvailQfsSVMVolMap();
			    if (availSVMVolMap != null && !availSVMVolMap.isEmpty() ||
				availQfsSVMVolMap != null && !availQfsSVMVolMap.isEmpty()) {
				// there are volumes available to be used
				disksData.add(dgName);
				disksData.add(primaryNode);
				disksData.add(Util.SVMDEVICEGROUP);	
			    }
			}

		    }
		}

		if (vxvmMap != null) {
		    Set entrySet = vxvmMap.entrySet();

		    for (Iterator i = entrySet.iterator(); i.hasNext();) {
			Map.Entry entry = (Map.Entry) i.next();
			String dgName = (String) entry.getKey();
			String primaryNode = (String) entry.getValue();
			disksData.add(dgName);
			disksData.add(primaryNode);
			disksData.add(Util.VxVMDEVICEGROUP);
		    }
		}
	    } finally {

		try {
		    t.interrupt();
		    t.join();
		} catch (Exception exe) {
		}
	    }

	    HashMap newDGResourceMap = (HashMap) wizardModel.getWizardValue(
		Util.NEW_DG_RESOURCES);

	    List selectedOptions = new ArrayList(); 

	    if (newDGResourceMap != null) {
		Set keySet = newDGResourceMap.keySet();
		Iterator iterator = keySet.iterator();
		while (iterator.hasNext()) {
		    String key = (String) iterator.next();
		    HashMap dgMap = (HashMap) newDGResourceMap.get(key);
		    String dgName = null;
		    if (dgMap != null) {
			dgName = (String) dgMap.get(Util.DISKGROUP_NAME);
		    }
		    if (selectedOptions == null) {
			selectedOptions = new ArrayList();
		    }
		    if (dgName != null && !selectedOptions.contains(dgName)) {
			selectedOptions.add(dgName);
		    }
		}
	    }

	    String option[] = null;
	    // Title and Description
	    TTYDisplay.printTitle(title);
	    TTYDisplay.pageText(desc);
	    TTYDisplay.clear(2);

	    if (disksData.size() > 0) {
		// Display the (n*3) Table
		option = TTYDisplay.create2DTable(subTitle, subheadings,
			disksData, disksData.size() / 3, 3, customTags, help,
			empty, true, selectedOptions);
	    } else {
		TTYDisplay.pageText(subTitle);
		TTYDisplay.clear(1);
		TTYDisplay.displaySubheadings(subheadings);
		TTYDisplay.clear(1);
		TTYDisplay.pageText(empty);
		TTYDisplay.clear(1);
		TTYDisplay.pageText(emptyDesc);
		TTYDisplay.clear(2);

		String choice = TTYDisplay.promptForEntry(continue_txt);

		if (choice.equals(back)) {
		    wizardModel.setWizardValue(
			ORSWizardConstants.SHOW_CLUSTER_DEVICES, null);
		    this.cancelDirection = true;

		    return;
		}
	    }

	    // Handle Back
	    if (option == null) {
		wizardModel.setWizardValue(
		    ORSWizardConstants.SHOW_CLUSTER_DEVICES, null);
		this.cancelDirection = true;

		return;
	    }

	    List optionsList = Arrays.asList(option);
	    if (optionsList.contains(rchar)) {

		// refresh the data
		wizardModel.setWizardValue(Util.SVMDEVICEGROUP, null);
		wizardModel.setWizardValue(Util.NEW_DG_RESOURCES, null);
		continue;
	    } else if (optionsList.contains(dchar)) {
		wizardModel.setWizardValue(
		    ORSWizardConstants.SHOW_CLUSTER_DEVICES, null);

		wizardModel.setWizardValue(
		    ORSWizardConstants.SHOW_DISKS_SUMMARY, "true");
		wizardModel.setWizardValue(Util.NEW_DG_RESOURCES, newDGResourceMap);

		return;

	    } else {

		// Get the dgResourceNames selected
		Iterator i = optionsList.iterator();
		String selDGName = null;
		String dgType = null;
		HashMap dgMap = null;

		while (i.hasNext()) {
		    String optionValue = (String) i.next();

		    try {
			int value = Integer.parseInt(optionValue);

			// Get the coressponding dgName from the vector
			// Also get the dgType
			selDGName = (String) disksData.get((value * 3) - 3);

			if (!selectedOptions.contains(selDGName)) {
			    selectedOptions.add(selDGName);
			}
			wizardModel.setWizardValue(Util.DISKGROUP_NAME, selDGName);
			dgType = (String) disksData.get((value * 3) - 1);
			wizardModel.setWizardValue(Util.DISKGROUP_TYPE, dgType);

		    } catch (NumberFormatException nfe) {
			// Custom Tags
		    }
		}
		// ask the user if he wishes to select logical devices
		TTYDisplay.clear(2);
		String choice = TTYDisplay.getConfirmation(preference, no, yes);
		if (choice.equals(yes)) {
		    // user wants to select logical devices. Move onto the next panel
		    return;
		} else {
		    // stay here

		    if (newDGResourceMap == null) {
			newDGResourceMap = new HashMap();
		    }

		    String dgRGName = null;
		    String dgRSName = null;
		    if (selectedZoneCluster != null &&
			selectedZoneCluster.trim().length() > 0) {
			// configuring on a zone cluster
			if (storageScheme.contains(Util.SVM)) {
			    // Scalable Device group RS and RG exists in the ZC

			    dgRSName = getResourceName_forScalDeviceGroup(
				selDGName, selectedZoneCluster);

			    dgRGName = getRGName_forScalDeviceGroup(
				dgRSName, selectedZoneCluster);

			    dgMap = new HashMap();
			    dgMap.put(Util.DISKGROUP_NAME, selDGName);
			    dgMap.put(Util.DISKGROUP_TYPE, dgType);
			    dgMap.put(Util.STORAGEGROUP, dgRGName);
			    dgMap.put(Util.LOGICAL_DEVLIST, Util.ALL);

			    newDGResourceMap.put(selectedZoneCluster +
				Util.COLON + dgRSName, dgMap);
			}
			if (storageScheme.contains(Util.QFS_SVM)) {
			    // Scalable Device group RS and RG exists in the BC
			    dgRSName = getResourceName_forScalDeviceGroup(
				selDGName, null);

			    dgRGName = getRGName_forScalDeviceGroup(
				dgRSName, null);

			    dgMap = new HashMap();
			    dgMap.put(Util.DISKGROUP_NAME, selDGName);
			    dgMap.put(Util.DISKGROUP_TYPE, dgType);
			    dgMap.put(Util.STORAGEGROUP, dgRGName);
			    dgMap.put(Util.LOGICAL_DEVLIST, Util.ALL);

			    newDGResourceMap.put(dgRSName, dgMap);
			}
		    } else {
			// configuring in a base cluster
			dgRSName = getResourceName_forScalDeviceGroup(
			    selDGName, null);

			dgRGName = getRGName_forScalDeviceGroup(
			    dgRSName, null);

			dgMap = new HashMap();
			dgMap.put(Util.DISKGROUP_NAME, selDGName);
			dgMap.put(Util.DISKGROUP_TYPE, dgType);
			dgMap.put(Util.STORAGEGROUP, dgRGName);
			dgMap.put(Util.LOGICAL_DEVLIST, Util.ALL);

			newDGResourceMap.put(dgRSName, dgMap);
		    }
		    wizardModel.setWizardValue(Util.NEW_DG_RESOURCES, newDGResourceMap);
		    continue;
		}
	    }
	} // while (true)
    }

    // Gets a Map of OBAN disksets not under ScalDeviceGroups
    // for a given nodelist
    private ClusterDiskGroupInfo getDeviceGroups_forOban(
	String nodelist[], HashMap helperData) {

        // Cluster OBAN DiskGroups
        String propertyNames[] = { Util.SVMDEVICEGROUP };

	racModel = RACInvokerCreator.getRACModel();
        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);

        Map discoveredMap = racModel.discoverPossibilities(
	    null, nodeEndPoint, nodelist, propertyNames, helperData);

	ClusterDiskGroupInfo clusterDiskGroupInfo = (ClusterDiskGroupInfo)
	    discoveredMap.get(propertyNames[0]);

	wizardModel.setWizardValue(Util.SVMDEVICEGROUP, clusterDiskGroupInfo);

	return clusterDiskGroupInfo;
    }

    // Gets a Map of VXVM disksets not under ScalDeviceGroups
    // for a given nodelist
    private Map getDeviceGroups_forVxVM(String nodelist[]) {

        // Cluster VxVM DiskGroups
        String propertyNames[] = { Util.VxVMDEVICEGROUP };

	racModel = RACInvokerCreator.getRACModel();
        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);

        Map discoveredMap = racModel.discoverPossibilities(
	    null, nodeEndPoint, propertyNames, nodelist);
        Map vxvmDiskGroupMap = (HashMap) discoveredMap.get(
                Util.NONRGM_VxVMDEVICEGROUP);

        return vxvmDiskGroupMap;
    }

    // Get a unique name for Scalable Device Group
    private String getResourceName_forScalDeviceGroup(
	String dgName, String selectedZoneCluster) {
	String propertyName[] = new String[] { Util.STORAGERESOURCE };

	RACModel racModel = RACInvokerCreator.getRACModel();
	String nodeEndPoint = (String) wizardModel.getWizardValue(
	    Util.NODE_TO_CONNECT);

	HashMap helperData = new HashMap();
	helperData.put(Util.SEL_ZONE_CLUSTER, selectedZoneCluster);
	helperData.put(Util.PREFIX, dgName);

	Map discoveredMap = racModel.discoverPossibilities(
	    null, nodeEndPoint, propertyName, helperData);

	return (String) discoveredMap.get(Util.STORAGERESOURCE);
    }

    // Get a unique RGName for Scalable Device Group
    private String getRGName_forScalDeviceGroup(
	String resourceName, String selectedZoneCluster) {

	RACModel racModel = RACInvokerCreator.getRACModel();
	String nodeEndPoint = (String) wizardModel.getWizardValue(
	    Util.NODE_TO_CONNECT);

	HashMap helperData = new HashMap();
	helperData.put(Util.SEL_ZONE_CLUSTER, selectedZoneCluster);
	helperData.put(Util.RS_NAME, resourceName);

	String propertyName[] = new String[] { Util.STORAGEGROUP };
	HashMap resourceGroupMap = (HashMap) racModel.discoverPossibilities(
	    null, nodeEndPoint, propertyName, helperData);

	return (String) resourceGroupMap.get(Util.STORAGEGROUP);
    }
}
