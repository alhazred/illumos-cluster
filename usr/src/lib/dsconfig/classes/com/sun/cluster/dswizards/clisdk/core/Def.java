/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)Def.java 1.10     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.clisdk.core;

/**
 * Class to turn debugging on/off
 */
public class Def {

    /** A flag that indicates whether or not debugging is enabled. */
    public static boolean DEBUG = (System.getProperty("wizard.debug") != null);

    /**
     * A mask value indicating that the CliDSConfigWizardModel name is initialized.
     */
    public static final int INIT_NAME = 1;

    /**
     * A mask value indicating that the CliDSConfigWizardModel data is initialized.
     */
    public static final int INIT_DATA = 4;

    /**
     * A mask value indicating that the CliDSConfigWizardModel Sequences
     * initialized.
     */
    public static final int INIT_SEQUENCES = 16;

    /**
     * A mask value indicating that the CliDSConfigWizardModel initialization is
     * complete.
     */
    public static final int INIT_COMPLETE = 0xFFFFFFFF;

    /** Exit Code: Indicates successful execution. Integer Value 0. */
    public static final int SUCCESS = 0;

    /** Exit Code: Indicates a fatal error occured. Integer Value 50. */
    public static final int ERROR_FATAL = 50;

    /**
     * Exit Code: Indicates a system resource could not be accessed. Integer
     * Value 51.
     */
    public static final int ERROR_ACCESS = 51;

    /**
     * Exit Code: Indicates user premature exit of the running wizard. Integer
     * Value 56.
     */
    public static final int ERROR_USER_EXIT = 56;

    /**
     * Exit Code: Indicates that the system is exiting because there was nothing
     * to do. Integer Value 58.
     */
    public static final int ERROR_NOTHING_TO_DO = 58;

    /** Exit Code: Indicates abnormal exit of the wizard. Integer Value 60. */
    public static final int ERROR_ABNORMAL_EXIT = 60;

}
