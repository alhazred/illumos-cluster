/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)ORFStorageSelectionPanel.java	1.17	09/03/11 SMI"
 */

package com.sun.cluster.dswizards.oraclerac.framework;

// CMASS
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.zonecluster.ZoneClusterData;

// DSWizards
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliUtil;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.oraclerac.invoker.RACInvokerCreator;

// J2SE
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.io.IOException;

// JMX
import javax.management.remote.JMXConnector;

import com.sun.cluster.model.DataModel;
import com.sun.cluster.model.ZoneClusterModel;
import com.sun.cluster.model.RACModel;

/**
 * This panel gets the Storage Selection schemes from the User
 */
public class ORFStorageSelectionPanel extends WizardLeaf {


    private String panelName; // Panel Name
    protected CliDSConfigWizardModel wizardModel; // WizardModel
    protected WizardI18N wizardi18n; // The resource bundle
    private boolean cvm = false;
    private boolean scmd = false;

    /**
     * Creates a new instance of ORFStorageSelectionPanel
     */
    public ORFStorageSelectionPanel() {
    }

    /**
     * Creates an ORFStorageSelectionPanel Panel with the given name and manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public ORFStorageSelectionPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates an ORFStorageSelectionPanelPanel for the wizard with the given
     * name and associates it with the WizardModel and WizardFlow.
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     */
    public ORFStorageSelectionPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;
        this.wizardi18n = wizardi18n;
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        String title = wizardi18n.getString("orf.storageSelectionPanel.title");
        String desc = wizardi18n.getString("orf.storageSelectionPanel.desc");
        String yes = wizardi18n.getString("cliwizards.yes");
        String no = wizardi18n.getString("cliwizards.no");
        String back = wizardi18n.getString("ttydisplay.back.char");
        String storagequery_txt = wizardi18n.getString(
                "orf.storageSelectionPanel.query");
        String storagelisttitle_txt = wizardi18n.getString(
                "orf.storageSelectionPanel.listtitle");
        String help_text = wizardi18n.getString(
                "orf.storageSelectionPanel.cli.help");
        String svm_str = wizardi18n.getString("orf.storageSelectionPanel.svm");
        String vxvm_str = wizardi18n.getString(
                "orf.storageSelectionPanel.vxvm");
        String qfssvm_str = wizardi18n.getString(
                "orf.storageSelectionPanel.qfssvm");
        String rchar = wizardi18n.getString("ttydisplay.r.char");
        String refresh = wizardi18n.getString("ttydisplay.menu.refresh");

        String keyPrefix = (String) wizardModel.getWizardValue(
                Util.KEY_PREFIX);

	String storageManagementKey = keyPrefix + Util.ORF_STORAGE_MGMT_SCHEMES;

	String zoneClusterName = CliUtil.getZoneClusterName(wizardModel);

        HashMap customTags = new HashMap();
        customTags.put(rchar, refresh);

        // Get already selected values
        List selStorageMgmtSchemes = new ArrayList();
        Object tmpObj = wizardModel.getWizardValue(storageManagementKey);

        if (tmpObj != null) {
            selStorageMgmtSchemes = (List) tmpObj;
        }

        String commaSeparatedSelections = "";

        if (!selStorageMgmtSchemes.isEmpty()) {
            selStorageMgmtSchemes = convertList(selStorageMgmtSchemes, false);

            Iterator iter = selStorageMgmtSchemes.iterator();

            while (iter.hasNext()) {
                String item = (String) iter.next();
                commaSeparatedSelections = commaSeparatedSelections + item;

                if (iter.hasNext()) {
                    commaSeparatedSelections = commaSeparatedSelections + ",";
                }
            }
        }

        String arStorageMgmtSchemes[] = getStorageManagementSchemes ();
        List optionSelected = null;
        boolean flag = true;

        while (flag) {

            // Enable Navigation
            TTYDisplay.setNavEnabled(true);
            this.cancelDirection = false;
            TTYDisplay.clearScreen();
            TTYDisplay.clear(1);

            // Title
            TTYDisplay.printTitle(title);
            TTYDisplay.clear(1);
            TTYDisplay.pageText(desc, 4);
            TTYDisplay.clear(1);

            optionSelected = TTYDisplay.getScrollingOptions(
                    storagelisttitle_txt, arStorageMgmtSchemes, customTags,
                    commaSeparatedSelections, help_text, true);

            if (optionSelected == null) {
                this.cancelDirection = true;

                return;
            }

            if (optionSelected.contains(refresh)) {
                TTYDisplay.clear(1);
                arStorageMgmtSchemes = getStorageManagementSchemes();
                commaSeparatedSelections = "";

                continue;
            }

            String err_str = "";

	    if (zoneClusterName == null ||
		zoneClusterName.trim().length() == 0) {
		if (optionSelected.contains(svm_str) && !scmd) {
		    err_str = wizardi18n.getString(
			    "orf.storageSelectionPanel.svmPackage.error") + "\n";
		}
	    }

	    if (zoneClusterName == null ||
		zoneClusterName.trim().length() == 0) {
		if ((optionSelected.contains(qfssvm_str)) && !scmd) {
		    err_str = wizardi18n.getString(
			"orf.storageSelectionPanel.svmPackage.error") + "\n";
		}
	    }

            if (optionSelected.contains(vxvm_str) && !cvm) {
                err_str = err_str +
                    wizardi18n.getString(
                        "orf.storageSelectionPanel.vxvmPackage.error");
            }

            if (!err_str.equals("")) {

                // Packages not installed properly
                TTYDisplay.printError(err_str);
                TTYDisplay.clear(1);
                TTYDisplay.promptForEntry(wizardi18n.getString(
                        "cliwizards.continue"));
            } else {
                flag = false;
            }
        }

        optionSelected = convertList(optionSelected, true);
        wizardModel.setWizardValue(storageManagementKey,
            optionSelected);
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#isDisplayComplete
     */
    public boolean isDisplayComplete() {
        return true;
    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }

    public void setPanelName(String val) {
        this.panelName = val;
    }

    public CliDSConfigWizardModel getWizardModel() {
        return wizardModel;
    }

    public void setWizardModel(CliDSConfigWizardModel val) {
        this.wizardModel = val;
    }

    public WizardI18N getWizardi18n() {
        return wizardi18n;
    }

    public void setWizardi18n(WizardI18N val) {
        this.wizardi18n = val;
    }

    private List convertList(List storageMgmtSchemes, boolean reverse) {

        // Storage schemes
        String svm_str = wizardi18n.getString("orf.storageSelectionPanel.svm");
        String vxvm_str = wizardi18n.getString(
                "orf.storageSelectionPanel.vxvm");
        String qfssvm_str = wizardi18n.getString(
                "orf.storageSelectionPanel.qfssvm");
        String qfshwraid_str = wizardi18n.getString(
                "orf.storageSelectionPanel.qfshwraid");
        String nas_str = wizardi18n.getString("orf.storageSelectionPanel.nas");
	String sunnas_str = wizardi18n.getString("orf.storageSelectionPanel.sunnas");
        String hwraid_str = wizardi18n.getString(
                "orf.storageSelectionPanel.hwraid");

        if (reverse) {

            if (storageMgmtSchemes.remove(svm_str)) {
                storageMgmtSchemes.add(Util.SVM);
            }

            if (storageMgmtSchemes.remove(vxvm_str)) {
                storageMgmtSchemes.add(Util.VXVM);
            }

            if (storageMgmtSchemes.remove(qfssvm_str)) {
                storageMgmtSchemes.add(Util.QFS_SVM);
            }

            if (storageMgmtSchemes.remove(qfshwraid_str)) {
                storageMgmtSchemes.add(Util.QFS_HWRAID);
            }

            if (storageMgmtSchemes.remove(nas_str)) {
                storageMgmtSchemes.add(Util.NAS);
            }

            if (storageMgmtSchemes.remove(sunnas_str)) {
                storageMgmtSchemes.add(Util.SUN_NAS);
            }

            if (storageMgmtSchemes.remove(hwraid_str)) {
                storageMgmtSchemes.add(Util.HWRAID);
            }
        } else {

            // Reset
            cvm = false;
            scmd = false;

            if (storageMgmtSchemes.remove(Util.SVM)) {
                storageMgmtSchemes.add(svm_str);
            }

            if (storageMgmtSchemes.remove(Util.VXVM)) {
                storageMgmtSchemes.add(vxvm_str);
            }

            if (storageMgmtSchemes.remove(Util.QFS_SVM)) {
                storageMgmtSchemes.add(qfssvm_str);
            }

            if (storageMgmtSchemes.remove(Util.QFS_HWRAID)) {
                storageMgmtSchemes.add(qfshwraid_str);
            }

            if (storageMgmtSchemes.remove(Util.NAS)) {
                storageMgmtSchemes.add(nas_str);
            }

            if (storageMgmtSchemes.remove(Util.SUN_NAS)) {
                storageMgmtSchemes.add(sunnas_str);
            }

            if (storageMgmtSchemes.remove(Util.HWRAID)) {
                storageMgmtSchemes.add(hwraid_str);
            }

            if (storageMgmtSchemes.remove(Util.SCMD)) {
                scmd = true;
            }

            if (storageMgmtSchemes.remove(Util.CVM)) {
                cvm = true;
            }
        }

        return storageMgmtSchemes;
    }

    private String[] getStorageManagementSchemes() {
        Thread t = null;
	String arStorageMgmtSchemes[] = null;

        // Get the discovered options for storage selection schemes from all
        // nodes
        String baseClusterNodeList[] = (String[]) wizardModel.getWizardValue(
                Util.BASE_CLUSTER_NODELIST);

	HashMap helperData = null;
	String selectedZoneCluster = CliUtil.getZoneClusterName(wizardModel);
	if (selectedZoneCluster != null &&
	    selectedZoneCluster.trim().length() > 0) {

	    helperData = new HashMap();
	    helperData.put(Util.SEL_ZONE_CLUSTER, selectedZoneCluster);
	    helperData.put(Util.BASE_CLUSTER_NODELIST, baseClusterNodeList);

	    DataModel dm = DataModel.getDataModel(null);
	    ZoneClusterModel zm = dm.getZoneClusterModel();
	    try {
		ZoneClusterData zcData = zm.getZoneClusterData(
		    null, selectedZoneCluster);

		helperData.put(Util.ZONE_CLUSTER_FSTAB_LIST,
		    zcData.getZoneClusterFSTabList());

		helperData.put(Util.ZONE_CLUSTER_DEVTAB_LIST,
		    zcData.getZoneClusterDevTabList());

	    } catch (IOException ioe) {
		ioe.printStackTrace();
	    }
	}

        t = TTYDisplay.busy(wizardi18n.getString(
                    "orf.storageSelectionPanel.discovery.busy"));
        t.start();

        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);


	RACModel racModel = RACInvokerCreator.getRACModel();
	if (racModel != null) {
	    HashMap resultMap = (HashMap) racModel.discoverPossibilities(
		null, nodeEndPoint, baseClusterNodeList,
                new String[] { Util.ORF_STORAGE_MGMT_SCHEMES }, helperData);

	    List storageMgmtSchemes = (List) resultMap.get(
                Util.ORF_STORAGE_MGMT_SCHEMES);

	    // Convert the data as per requirements for display
	    storageMgmtSchemes = convertList(storageMgmtSchemes, false);

	    String strArType[] = new String[] {};
	    arStorageMgmtSchemes = (String[]) storageMgmtSchemes.toArray(
		    strArType);
	}

        try {
            t.interrupt();
            t.join();
        } catch (Exception exe) {
        }

        return arStorageMgmtSchemes;
    }

}
