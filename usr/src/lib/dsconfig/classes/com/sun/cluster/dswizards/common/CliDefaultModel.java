/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident "@(#)CliDefaultModel.java 1.2     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.common; 

import java.io.Serializable;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;

public class CliDefaultModel implements java.io.Serializable
{

    public static final int INVALID_INDEX = -1;
    public static final String DEFAULT_CONTEXT_NAME="DEFAULT";

    private String name = null;
    private Map context=new HashMap();

    private Context currContext;
    private String currContextName;
    private boolean bUseDefaultValues=true;

    /**
     * Construct an empty model
     *
     */
    public CliDefaultModel() {
	this(null);
    }

    /**
     * Construct an empty model with the given name
     *
     */
    public CliDefaultModel(String name) {
	super();
	setName(name);

	// Initialize and select the default context
	initializeDefaultContext();
	try {
	    selectContext(DEFAULT_CONTEXT_NAME);
	} catch (Exception e) {
	}
    }

    /**
     * Returns the name of this model.
     *
     * @return	The name of this model
     */
    public String getName() {
	return name;
    }

    /**
     * Sets the name of this model
     *
     * @param	name the name of this model
     */
    public void setName(String value) {
	name=value;
    }

    /**
     * Indicates whether this model will use default values.
     * @return	True if a default value will be used for a field when 
     *			its value is null
     */
    public boolean useDefaultValues() {
	return bUseDefaultValues;
    }

    /**
     * Sets whether this model will use default values.
     *
     * @param	value Set to true if a default value should be used for a field when
     *			its value is null
     */
    public void setUseDefaultValues(boolean value) {
	bUseDefaultValues=value;
    }

    /**
     * Clears all values
     *
     */
    public void clear() {
	getCurrentContext().clear();
    }

    /**
     * Returns the current row index
     *
     */
    public int getRowIndex() {
	return getCurrentContext().getCurrentRow();
    }

    /**
     * Sets the current row index
     *
     */
    public void setRowIndex(int value) {
	getCurrentContext().setCurrentRow(value);
    }

    /**
     * Ensures that the specified row exists in the model
     * 
     * @param	row
     */
    protected void addRow(int row) {
	List valueList=getRowList();

	if (row >= valueList.size()) {
	    while (valueList.size() <= row) {
		valueList.add(createValueMap());
	    }
	}
    }

    /**
     * Returns the internal list used to store rows of data.
     *
     */
    protected List getRowList() {
	return getCurrentContext().getValueList();
    }

    /**
     * Allocates a new map object to store row data.
     */
    protected Map createValueMap() {
	return new HashMap();
    }

    /**
     * Returns the value map for the current row.
     *
     */
    protected Map getValueMap() {
	return getValueMap(getRowIndex());
    }


    /**
     * Returns the value map for the specified row.
     */
    protected Map getValueMap(int row) {
	if (row == INVALID_INDEX) {
	    row = 0;
	    addRow(0);
	    setRowIndex(0);
	}

	addRow(row);
	return (Map)getRowList().get(row);
    }


    /**
     * Returns the default value map
     *
     */
    protected Map getDefaultValueMap()
    {
	return getCurrentContext().getDefaultValueMap();
    }


    /**
     * Returns a named value from this model
     * 
     * @param	name    The name of the value to return
     * @return	value   If the model has multiple values for the
     *			specified name, the first value is returned.
     *			If there is no value for the specified name,
     * 			a null is returned.
     */
    public Object getValue(String name) {
	Object[] values=getValues(name);
	if (values == null || values.length==0) {
	    return null;
	} else {
	    return values[0];
	}
    }

    /**
     * Sets a named value in this model. This method overwrites any current
     * value or values. 
     * 
     * @param	name  The name of the value to set
     * @param	value The value to set in this model 
     */
    public void setValue(String name, Object value)
    {
	setValues(name, new Object[] {value});
    }

    /**
     * Returns a named set of values from this model
     * 
     * @param	name The name of the value set to return
     * @return	set of values.  If there is no value for the 
     *		specified name, this method returns an array of zero length.
     */
    public Object[] getValues(String name) {
	Object[] result=(Object[])getValueMap().get(name);

	if (result == null && useDefaultValues()) {
	    result = getDefaultValues(name);
	}

	if (result == null) {
	    result = new Object[0];
	}

	return result;
    }


    /**
     * Sets a named set of values in this model
     * 
     * @param	name
     *			The name of the value set to set in the model
     * @param	values
     *			The set of values to set in this model
     */
    public void setValues(String name, Object[] value) {
	if (value==null) {
	    value=new Object[0];
	}
	getValueMap().put(name,value);

	if (useDefaultValues()) {
	    Map defaultValueMap = getDefaultValueMap();
	    if (defaultValueMap.get(name) == null) {
		defaultValueMap.put(name,value);
	    }
	}
    }

    /**
     * Returns a named value from this model
     * 
     * @param	name The name of the value to return
     * @return	The specified value. 
     */
    public Object getDefaultValue(String name) {
	Object[] values = getDefaultValues(name);
	if (values == null || values.length == 0) {
	    return null;
	} else {
	    return values[0];
	}
    }


    /**
     * Sets a named value in this model.
     * 
     * @param	name  The name of the value to set
     * @param	value The value to set in this model 
     */
    public void setDefaultValue(String name, Object value) {
	setDefaultValues(name,new Object[] {value});
    }

	
    /**
     * Returns a named set of values from this model
     * @param	name   The name of the value set to return
     * @returnThe specified set of values.
     */
    public Object[] getDefaultValues(String name) {
	Object[] result = (Object[])getDefaultValueMap().get(name);
	if (result == null) {
	    result = new Object[0];
	}
	return result;
    }


    /**
     * Sets a named set of values in this model
     * @param	name The name of the value set to set in the model
     * @param	values  The set of values to set in this model
     */
    public void setDefaultValues(String name, Object[] value) {
	if (value == null) {
	    value = new Object[0];
	}
	getDefaultValueMap().put(name,value);
    }

    /**
     * Returns the currently active context name
     *
     * @return	The current context name
     */
    public String getCurrentContextName() {
	return currContextName;
    }


    /**
     * Returns the full set of context names available in this model
     *
     */
    public String[] getContextNames() {
	return (String[])context.keySet().toArray(new String[0]);
    }

    /**
     * Change the current context to the specified context
     *
     */
    public void selectContext(String name) throws Exception {
	currContextName = name;
	currContext = getContext(name);

	if (currContext == null) {
	    throw new Exception("Context " + name + "does not exist");
	}
    }


    /**
     * Change the current context to the default context
     *
     */
    public void selectDefaultContext() throws Exception {
	selectContext(DEFAULT_CONTEXT_NAME);
    }


    /**
     * Checks if the current context is the default context
     */
    public boolean isDefaultContext() {
	return getCurrentContextName().equals(DEFAULT_CONTEXT_NAME);
    }

    /**
     * Initializes the default context.
     */
    protected void initializeDefaultContext() {
	addContext(DEFAULT_CONTEXT_NAME);
    }

    /**
     * Returns the currently selected context object
     *
     */
    protected Context getCurrentContext() {
	if (currContext == null) {
	    currContext = getContext(currContextName);
	}
	return currContext;
    }

    /**
     * Adds an additional named context to this model
     */
    protected Context addContext(String name) {
	Context result = (Context)context.get(name);
	if (result == null) {
	    result = new Context(name);
	    context.put(name,result);
	}

	return result;
    }

    /**
     * Returns the named context object
     */
    protected Context getContext(String name) {
	return (Context)context.get(name);
    }

    /**
     * Removes the named context from this model
     */
    protected void removeContext(String name)
    {
	context.remove(name);
    }

    /**
     * Context class
     */
    protected static class Context implements Serializable {

	private String name;
	private List valueList = new ArrayList();
	private int currentRow = INVALID_INDEX;
	private Map defaultValueMap;

	public Context(String name) { super();
	    this.name = name;
	}

	public List getValueList() {
	    return valueList;
	}

	public void setValueList(List value) {
	    valueList = value;
	}

	public Map getDefaultValueMap() {
	    if (defaultValueMap == null) {
		defaultValueMap = new HashMap();
	    }

	    return defaultValueMap;
	}

	public void setDefaultValueMap(Map value) {
	    defaultValueMap = value;
	}

	public int getCurrentRow() {
	    return currentRow;
	}

	public void setCurrentRow(int value) {
	    currentRow = value;
	}
	    
	public void clear() {
	    valueList = new ArrayList();
	    currentRow = INVALID_INDEX;
	}
    }

}
