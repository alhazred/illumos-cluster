/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)HASPNodeSelectionPanel.java 1.22     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.hastorageplus;

import com.sun.cluster.agent.dataservices.hastorageplus.HAStoragePlusMBean;

// CMASS
import com.sun.cluster.agent.dataservices.utils.Util;

import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.NodeSelectionPanel;

// Wizard Common
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;

// J2SE
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

// JMX
import javax.management.remote.JMXConnector;


/**
 * This panel gets the Preferred Node from the User
 */
public class HASPNodeSelectionPanel extends NodeSelectionPanel {

    private String hasp;

    /**
     * Creates a new instance of HASPNodeSelectionPanel
     */
    public HASPNodeSelectionPanel() {
    }

    /**
     * Creates an PreferredNode Panel with the given name and manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public HASPNodeSelectionPanel(String name, WizardTreeManager manager) {
        super(name, manager);
    }

    public HASPNodeSelectionPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(name, model, wizardFlow, wizardi18n);
        initStrings();
    }


    private void initStrings() {
        desc = wizardi18n.getString("hasp.nodeSelectionPanel.desc");
        hasp = wizardi18n.getString("dswizards.hasp.wizard");
    }
    
    private void initPropList() {
        JMXConnector localConnection = getConnection(Util.getClusterEndpoint());
        String propertyNames[] = { Util.PROPERTY_NAMES };
        HAStoragePlusMBean mbean = (HAStoragePlusMBean) HASPWizardCreator
            .getHAStoragePlusMBean(localConnection);
        HashMap discoveredMap = mbean.discoverPossibilities(propertyNames,
                null);

        // Copy the Data to the WizardModel
        propertyNames = new String[discoveredMap.size()];

        Set entrySet = discoveredMap.entrySet();
        Iterator setIterator = entrySet.iterator();
        int i = 0;

        while (setIterator.hasNext()) {
            Map.Entry entry = (Map.Entry) setIterator.next();
            String key = (String) entry.getKey();
            wizardModel.setWizardValue(key, entry.getValue());
            propertyNames[i++] = key;
        }

        wizardModel.setWizardValue(Util.PROPERTY_NAMES, propertyNames);
    }
    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        String nodeList[] = displayPanel(HAStoragePlusMBean.class, hasp);

        if (nodeList == null) {
            this.cancelDirection = true;

            return;
        }

        wizardModel.setWizardValue(Util.RG_NODELIST, nodeList);
        initPropList();
    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }
}
