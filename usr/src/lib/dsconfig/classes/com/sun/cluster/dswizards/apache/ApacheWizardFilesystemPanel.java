/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ApacheWizardFilesystemPanel.java 1.9     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.apache;

// Java

// CMASS
import com.sun.cluster.agent.dataservices.apache.Apache;
import com.sun.cluster.agent.dataservices.apache.ApacheMBean;
import com.sun.cluster.agent.dataservices.hastorageplus.HAStoragePlusMBean;
import com.sun.cluster.agent.dataservices.logicalhost.LogicalHost;
import com.sun.cluster.agent.dataservices.sharedaddress.SharedAddress;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.rgm.ResourceMBean;
import com.sun.cluster.common.TrustedMBeanModel;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliStorageSelectionPanel;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.hastorageplus.HASPWizardConstants;
import com.sun.cluster.dswizards.hastorageplus.HASPWizardCreator;
import com.sun.cluster.dswizards.logicalhost.LogicalHostWizardConstants;
import com.sun.cluster.dswizards.logicalhost.LogicalHostWizardCreator;
import com.sun.cluster.dswizards.sharedaddress.SharedAddressWizardCreator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.management.AttributeValueExp;
import javax.management.Query;
import javax.management.QueryExp;
import javax.management.remote.JMXConnector;


/**
 * Panel to get the Apache Doc Root
 */
public class ApacheWizardFilesystemPanel extends CliStorageSelectionPanel {


    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N messages; // Message Bundle

    private JMXConnector connection = null;
    private ApacheMBean apacheMBean = null; // Reference to MBean

    /**
     * Creates a new instance of ApacheWizardFilesystemPanel
     */
    public ApacheWizardFilesystemPanel() {
        super();
    }

    /**
     * Creates an ApacheWizardFilesystem Panel with the given name and tree
     * manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public ApacheWizardFilesystemPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates a ApacheWizardFilesystem Panel with the given name and associates
     * it with the WizardModel, WizardFlow and i18n reference
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     * @param  messages  WizardI18N
     */
    public ApacheWizardFilesystemPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N messages) {
        super(name, model, wizardFlow, messages);
        this.panelName = name;
        this.wizardModel = model;

        // Get the I18N instance
        this.messages = messages;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {

        String docRoot = (String) wizardModel.getWizardValue(Util.APACHE_DOC);
        title = messages.getString("apache.fsPanel.title");
        desc = messages.getString("apache.fsPanel.desc");
        help_text = messages.getString("apache.fsPanel.help",
                new Object[] { docRoot });
        help_text += "\n \n";
        help_text += messages.getString("storageSelectionPanel.cli.help");
        continue_txt = messages.getString("cliwizards.continue");
        empty = "";
        subtitle = "";
        table_empty_continue = messages.getString("apache.fsPanel.continue");

        String storageMountPoint = null;
        String storageResource = null;
        boolean showMountPointPanel = false;
        String mountPoints[] = null;

        // Initialize the flag which decides whether we go to Mount Point
        // panel or not
        wizardModel.setWizardValue(ApacheWizardConstants.APACHE_MULTIPLE_MOUNTS,
            new Boolean(showMountPointPanel));

        // Get the ApacheMbean reference
        if (apacheMBean == null) {
            String nodeEndPoint = (String) wizardModel.getWizardValue(
                    Util.NODE_TO_CONNECT);
            connection = getConnection(nodeEndPoint);
            apacheMBean = ApacheWizardCreator.getApacheMBeanOnNode(connection);
        }

        displayPanel(true);

        // Handle Create New
        String userSelection = (String) wizardModel.getWizardValue(
                Util.CREATE_NEW);

        if (userSelection == null) {
            this.cancelDirection = true;

            return;
        } else if (userSelection.equals(CREATE_NEW)) {
            wizardModel.setWizardValue(
                ApacheWizardConstants.STORAGE_RESOURCE_NAME, null);

            return;
        } else if (userSelection.equals(SELECT_EXISTING)) {
            Vector selectedResources = (Vector) wizardModel.getWizardValue(
                    Util.SEL_RS);
            String selectedFS[] = (String[]) wizardModel.getWizardValue(
                    Util.SEL_FILESYSTEMS);

            wizardModel.setWizardValue(Util.SEL_RS, selectedResources);
            wizardModel.setWizardValue(ApacheWizardConstants.APACHE_FS_MTPTS,
                selectedFS);

            if ((selectedFS != null) && (selectedFS.length > 0)) {

                if (selectedFS.length == 1) {
                    storageMountPoint = selectedFS[0];
                } else if (selectedFS.length > 1) {
                    showMountPointPanel = true;
                }
            } else if ((selectedResources != null) &&
                    (selectedResources.size() > 0)) {
                storageResource = (String) selectedResources.get(0);

                // Get the Mountpoint for the user selected resource
                Map storageResources = (HashMap) wizardModel.getWizardValue(
                        ApacheWizardConstants.APACHE_STORAGE_RESOURCES);
                mountPoints = (String[]) storageResources.get(storageResource);

                if (mountPoints.length > 1) {
                    showMountPointPanel = true;
                }
            }

            if (!showMountPointPanel) {

                // If user selected a mountPoint that was created
                if (((storageResource == null) ||
                            (storageResource.trim().length() <= 0)) &&
                        (storageMountPoint != null)) {
                    wizardModel.setWizardValue(
                        ApacheWizardConstants.STORAGE_RESOURCE_NAME, " ");
                    wizardModel.setWizardValue(Util.APACHE_MOUNT,
                        storageMountPoint);
                    wizardModel.setWizardValue(
                        ApacheWizardConstants.APACHE_FILESYSTEMS_SELECTED,
                        new String[] { storageMountPoint });
                    ApacheWizardCreator.fillWizardModel_forNewMountPoint(
                        wizardModel, connection);

                    return;

                } else if (storageResource != null) {
                    storageMountPoint = mountPoints[0];
                    wizardModel.setWizardValue(
                        ApacheWizardConstants.STORAGE_RESOURCE_NAME,
                        storageResource);
                    wizardModel.setWizardValue(Util.APACHE_MOUNT,
                        storageMountPoint);
                    ApacheWizardCreator.fillWizardModel_forExistingMountPoint(
                        storageResource, wizardModel, connection);

                    return;
                }

                //
                // wizardModel.setWizardValue(
                //	ApacheWizardConstants.APACHE_MULTIPLE_MOUNTS,
                // new Boolean(showMountPointPanel));
            } else {

                // Set the value for the flag which decides whether we go to
                // Mount Point panel or not
                wizardModel.setWizardValue(
                    ApacheWizardConstants.APACHE_MULTIPLE_MOUNTS,
                    new Boolean(showMountPointPanel));

                return;
            }
        }
    }

    // Over-ride refresh storageResources in CliStoragePanel
    protected void refreshRGList() {
        Map storageResources = (HashMap) wizardModel.getWizardValue(
                ApacheWizardConstants.APACHE_STORAGE_RESOURCES);

        // Convert into a format for the common storage panel
        List storageRGList = new ArrayList();

        if (storageResources != null) {

            for (Iterator i = storageResources.entrySet().iterator();
                    i.hasNext(); /* */) {
                Map rgMap = new HashMap();
                Map.Entry entry = (Map.Entry) i.next();
                String storageResName = (String) entry.getKey();
                String mountPoints[] = (String[]) entry.getValue();
                String resourceGroup = null;
                rgMap.put(Util.RS_NAME, storageResName);
                rgMap.put(Util.HASP_ALL_FILESYSTEMS, mountPoints);

                try {
                    ResourceMBean storageMBean = (ResourceMBean)
                        TrustedMBeanModel.getMBeanProxy(connection,
                            ResourceMBean.class, storageResName, true);
                    resourceGroup = storageMBean.getResourceGroupName();
                } catch (Exception e) {
                    //
                }

                rgMap.put(Util.RG_NAME, resourceGroup);
                storageRGList.add(rgMap);
            }
        }

        wizardModel.setWizardValue(Util.STORAGE_RGLIST, storageRGList);
    }
}
