/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)RacDBPropsReviewPanel.java	1.23	09/03/11 SMI"
 */


package com.sun.cluster.dswizards.oraclerac.database.ver9i;

// CMASS
import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.rgm.property.ResourceProperty;
import com.sun.cluster.agent.rgm.property.ResourcePropertyBoolean;
import com.sun.cluster.agent.rgm.property.ResourcePropertyEnum;
import com.sun.cluster.agent.rgm.property.ResourcePropertyInteger;
import com.sun.cluster.agent.rgm.property.ResourcePropertyString;
import com.sun.cluster.agent.rgm.property.ResourcePropertyStringArray;

// CLI Wizard SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.common.CliUtil;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.ResourcePropertyUtil;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.oraclerac.database.*;
import com.sun.cluster.dswizards.oraclerac.invoker.RACInvokerCreator;

// Model
import com.sun.cluster.model.RACModel;

// Java
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Vector;


/**
 * The Panel Class that displays a review panel and lets the user edit
 * properties.
 */
public class RacDBPropsReviewPanel extends RacDBBasePanel {

    private static String RTRPROPNAME = "rtrPropName";
    private static String PROPERTY = "property";
    private static String DISPLAYPROPNAME = "displayPropName";

    private boolean server = false;
    private boolean listener = false;

    private RACModel racModel = null;

    private String curNode = null;

    /**
     * Creates a PropsReview Panel with the given name and Wizard state
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     */
    public RacDBPropsReviewPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(name, model, wizardFlow, wizardi18n);
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.
     *		WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        title = wizardi18n.getString("haoracle.oraclePropsReviewPanel.title");

        String dchar = wizardi18n.getString("ttydisplay.d.char");
        String done = wizardi18n.getString("ttydisplay.menu.done");
        String table_title = wizardi18n.getString(
                "haoracle.oraclePropsReviewPanel.table.title");
        String heading1 = wizardi18n.getString(
                "haoracle.oraclePropsReviewPanel.heading1");
        String heading2 = wizardi18n.getString(
                "haoracle.oraclePropsReviewPanel.heading2");
        String help_text = wizardi18n.getString(
                "oraclerac.oraclePropsReviewPanel.cli.help");
        Thread t;


        // Review panel table
        Vector reviewPanel = new Vector();

        curNode = (String) wizardModel.getWizardValue(Util.RAC_CURNODENAME);

        String nodeList[] = (String[]) wizardModel.getValue(Util.RG_NODELIST);

        if (curNode == null) {
            curNode = nodeList[0];
            wizardModel.setWizardValue(Util.RAC_NODEIDX, new Integer(0));
            wizardModel.setWizardValue(Util.RAC_CURNODENAME, curNode);
            wizardModel.setWizardValue(Util.RAC_NODELIST_COMPLETE,
                Boolean.FALSE);
        }

        String tmpArry[] = curNode.split(Util.COLON);
        curNode = tmpArry[0];

        desc = wizardi18n.getString("oraclerac.oraclePropsReviewPanel.cli.desc",
                new String[] { curNode });

        Integer preference = (Integer) wizardModel.getWizardValue(
                Util.DB_COMPONENTS);
        int prefInt = preference.intValue();

        ArrayList rtrProps = new ArrayList();
        ArrayList discoverableProps = new ArrayList();

        if ((prefInt == Util.SERVER) || (prefInt == Util.BOTH_S_L)) {
            rtrProps.add(Util.SERVER_ALERT_LOG_FILE);
            rtrProps.add(Util.SERVER_CONNECT_STRING);
            rtrProps.add(Util.SERVER_PARAMETER_FILE);
            rtrProps.add(Util.SERVER_CUSTOM_ACTION_FILE);
            rtrProps.add(Util.SERVER_USER_ENV);
            rtrProps.add(Util.SERVER_DEBUG_LEVEL);
            discoverableProps.add(Util.ALERT_LOG_FILE);
            discoverableProps.add(Util.PARAMETER_FILE);
            discoverableProps.add(Util.USER_ENV);
            server = true;

            if (wizardModel.getWizardValue(Util.SCALABLE_SERVER_PROPS) ==
                    null) {
                TTYDisplay.clear(1);
                t = TTYDisplay.busy(wizardi18n.getString(
                            "oraclerac.discovery.server.progess"));
                t.start();
                populateRTRProperties(Util.SCALABLE_SERVER_PROPS);

                try {
                    t.interrupt();
                    t.join();
                } catch (Exception e) {
                }

                if (wizardModel.getWizardValue(Util.SCALABLE_SERVER_PROPS) ==
                        null) {
                    String errorString = wizardi18n.getString(
                            "oraclerac.discovery.cli.error",
                            new String[] { Util.RAC_SERVER_RTNAME });
                    TTYDisplay.printInfo(errorString);
                    TTYDisplay.clear(2);
                    TTYDisplay.promptForEntry(continue_txt);
                    System.exit(0);
                }
            }
        }

        if ((prefInt == Util.LISTENER) || (prefInt == Util.BOTH_S_L)) {
            rtrProps.add(Util.LISTENER_PREFIX + Util.LISTENER_NAME);
            rtrProps.add(Util.LISTENER_USER_ENV);

            if (!discoverableProps.contains(Util.USER_ENV)) {
                discoverableProps.add(Util.USER_ENV);
            }

            if (wizardModel.getWizardValue(Util.SCALABLE_LISTENER_PROPS) ==
                    null) {
                TTYDisplay.clear(1);
                t = TTYDisplay.busy(wizardi18n.getString(
                            "oraclerac.discovery.listener.progess"));
                t.start();
                populateRTRProperties(Util.SCALABLE_LISTENER_PROPS);

                try {
                    t.interrupt();
                    t.join();
                } catch (Exception e) {
                }

                if (wizardModel.getWizardValue(Util.SCALABLE_LISTENER_PROPS) ==
                        null) {
                    String errorString = wizardi18n.getString(
                            "oraclerac.discovery.cli.error",
                            new String[] { Util.RAC_LISTENER_RTNAME });
                    TTYDisplay.printInfo(errorString);
                    TTYDisplay.clear(2);
                    TTYDisplay.promptForEntry(continue_txt);
                    System.exit(0);
                }
            }

            listener = true;
        }

        // Title
        String instructions = wizardi18n.getString(
                "oraclerac.oraclePropsReviewPanel.cli.instructions");
        TTYDisplay.printTitle(title);
        TTYDisplay.printSubTitle(instructions);

        String value;
        String rtrPropsArr[] = (String[]) rtrProps.toArray(new String[0]);
        String discoverablePropArr[] = (String[]) discoverableProps.toArray(
                new String[0]);
        HashMap map = getDiscoveredValues(discoverablePropArr);

        for (int i = 0; i < discoverablePropArr.length; i++) {
            value = "";

            String propName = discoverablePropArr[i];
            Vector vec = (Vector) map.get(propName);

            if ((vec != null) && (vec.size() > 0)) {
                value = (String) vec.elementAt(0);
            }

            ResourceProperty propFromDisc = null;

            if (server) {
                propFromDisc = (ResourceProperty) wizardModel.getWizardValue(
                        Util.SERVER_PREFIX + propName);
                validate(propFromDisc, Util.SERVER_PREFIX + propName + curNode,
                    value);
            }

            if (listener && propName.equals(Util.USER_ENV)) {
                propFromDisc = (ResourceProperty) wizardModel.getWizardValue(
                        Util.LISTENER_USER_ENV);
                validate(propFromDisc, Util.LISTENER_USER_ENV + curNode, value);
            }
        }

        HashMap tableMap = null;
        HashMap selectionMap = null;
        String option = "";

        while (!option.equals(dchar)) {
            TTYDisplay.setNavEnabled(true);
            this.cancelDirection = false;
            tableMap = new HashMap();
            reviewPanel.removeAllElements();

            for (int i = 0; i < rtrPropsArr.length; i++) {
                HashMap selMap = new HashMap();

                // properties as set in the wizard model
                ResourceProperty localProp = (ResourceProperty) wizardModel
                    .getWizardValue(rtrPropsArr[i] + curNode);

                if (localProp == null) {
                    localProp = (ResourceProperty) wizardModel.getWizardValue(
                            rtrPropsArr[i]);
                }

                selMap.put(RTRPROPNAME, rtrPropsArr[i]);
                selMap.put(PROPERTY, localProp);

                if (!server || !listener) {

                    // just display the actual property name
                    reviewPanel.add(localProp.getName());
                    selMap.put(DISPLAYPROPNAME, localProp.getName());
                } else {

                    // user selected server and listener. debug level and the
                    // user env are the only
                    // common props so prepend them with Server: and Listener:
                    if (isCommonProperty(rtrPropsArr[i])) {
                        reviewPanel.add(rtrPropsArr[i]);
                        selMap.put(DISPLAYPROPNAME, rtrPropsArr[i]);
                    } else {
                        reviewPanel.add(localProp.getName());
                        selMap.put(DISPLAYPROPNAME, localProp.getName());
                    }
                }

                tableMap.put(Integer.toString(i + 1), selMap);
                reviewPanel.add(ResourcePropertyUtil.getValue(localProp));
            }

            TTYDisplay.printSubTitle(desc);

            HashMap customTags = new HashMap();
            customTags.put(dchar, done);

            String subheadings[] = { heading1, heading2 };

            option = TTYDisplay.create2DTable(table_title, subheadings,
                    reviewPanel, customTags, help_text);

            String desc;

            if (option.equals(back)) {
                this.cancelDirection = true;

                return;
            }

            TTYDisplay.clear(3);
            TTYDisplay.setNavEnabled(false);

            if (!option.equals(dchar)) {
                ResourceProperty zLocal = null;
                ErrorValue retVal = null;
                selectionMap = (HashMap) tableMap.get(option);

                String rtrPropName = (String) selectionMap.get(RTRPROPNAME);
                String displayPropName = (String) selectionMap.get(
                        DISPLAYPROPNAME);
                zLocal = (ResourceProperty) selectionMap.get(PROPERTY);
                retVal = displayProperty(rtrPropName, displayPropName, zLocal);

                if (!retVal.getReturnValue().booleanValue()) {
                    TTYDisplay.printError(retVal.getErrorString());
                }
            } else {

                // validate the user input using the data services
                ErrorValue retVal = validateInput(rtrPropsArr);

                if (!retVal.getReturnValue().booleanValue()) {
                    TTYDisplay.printInfo(retVal.getErrorString());
                    TTYDisplay.clear(2);
                    TTYDisplay.promptForEntry(continue_txt);
                    option = "";
                }

                tableMap = null;
            }

            TTYDisplay.clear(3);
        }

        Integer nodeIndex = (Integer) wizardModel.getWizardValue(
                Util.RAC_NODEIDX);
        int nodeIdx = nodeIndex.intValue();

        if (++nodeIdx < nodeList.length) {
            String cNode = nodeList[nodeIdx];
            wizardModel.setWizardValue(Util.RAC_CURNODENAME, cNode);
            wizardModel.setWizardValue(Util.RAC_NODEIDX, new Integer(nodeIdx));
            wizardModel.setWizardValue(Util.RAC_NODELIST_COMPLETE,
                Boolean.FALSE);
        } else {
            wizardModel.setWizardValue(Util.RAC_NODELIST_COMPLETE,
                Boolean.TRUE);
            wizardModel.setWizardValue(Util.RAC_CURNODENAME, null);
            wizardModel.setWizardValue(Util.RAC_NODEIDX, null);
        }
    }

    /**
     * Returns if its a common property being shared by Server and Listener
     *
     * @return  true or false
     */
    private boolean isCommonProperty(String propName) {

        if (!(propName.startsWith(Util.LISTENER_USER_ENV) ||
                    propName.startsWith(Util.SERVER_USER_ENV) ||
                    propName.startsWith(Util.SERVER_DEBUG_LEVEL))) {
            return false;
        }

        return true;
    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }

    private HashMap getDiscoveredValues(String propNames[]) {

        // Get the discovered props from the dataservice
        HashMap map = null;
        HashMap retMap = new HashMap();
        Vector retVec[] = new Vector[propNames.length];

        for (int i = 0; i < retVec.length; i++) {
            retVec[i] = new Vector();
        }

        HashMap helperData = new HashMap();
        String orahome = (String) wizardModel.getWizardValue(
                Util.SEL_ORACLE_HOME);
        String orasid = (String) wizardModel.getWizardValue(
                Util.SEL_ORACLE_SID + curNode);
        helperData.put(Util.ORACLE_HOME, orahome);
        helperData.put(Util.ORACLE_SID, orasid);

        // get RACModel
	RACModel racModel = RACInvokerCreator.getRACModel();

        String selZoneCluster = CliUtil.getZoneClusterName(wizardModel);
	helperData.put(Util.SEL_ZONE_CLUSTER, selZoneCluster);

        map = racModel.discoverPossibilities(null, curNode, propNames, helperData);
	if (map != null) {

            for (int j = 0; j < propNames.length; j++) {
                Vector vec = (Vector) map.get(propNames[j]);

                if (vec == null) {
                    vec = new Vector();
                }

                int size = vec.size();

                for (int k = 0; k < size; k++) {
                    String propStr = (String) vec.elementAt(k);

                    if (!retVec[j].contains(propStr)) {
                        retVec[j].add(propStr);
                    }
                }
            }
        }

        for (int j = 0; j < propNames.length; j++) {
            retMap.put(propNames[j], retVec[j]);
        }

        return retMap;
    }

    private ErrorValue displayProperty(String rtrPropName,
        String displayPropName, ResourceProperty xProperty) {
        String propNameText = (String) wizardi18n.getString(
                "oraclerac.oraclePropsReviewPanel.propname",
                new String[] { displayPropName });
        String propTypeText = (String) wizardi18n.getString(
                "oraclerac.oraclePropsReviewPanel.proptype",
                new String[] { ResourcePropertyUtil.getType(xProperty) });
        String propDescText = (String) wizardi18n.getString(
                "oraclerac.oraclePropsReviewPanel.propdesc",
                new String[] { xProperty.getDescription() });
        String currValText = (String) wizardi18n.getString(
                "oraclerac.oraclePropsReviewPanel.currval",
                new String[] { ResourcePropertyUtil.getValue(xProperty) });
        String newValText = (String) wizardi18n.getString(
                "oraclerac.oraclePropsReviewPanel.newval");

        TTYDisplay.pageText(propNameText);
        TTYDisplay.pageText(propDescText);
        TTYDisplay.pageText(propTypeText);
        TTYDisplay.pageText(currValText);

        String value = TTYDisplay.promptForEntry(newValText);

        return validate(xProperty, rtrPropName + curNode, value);
    }

    public ErrorValue validate(ResourceProperty xPropArg, String rtrPropName,
        String propVal) {

        // Check if a value has been set
        ErrorValue result = null;
        boolean error = false;
        String detail = wizardi18n.getString(
                "haoracle.oraclePropsReviewPanel.error.input.summary");
        ResourceProperty zResultProp = null;

        String propName = xPropArg.getName();

        if (xPropArg.isRequired() &&
                ((propVal == null) || (propVal.length() == 0))) {
            error = true;
            detail = wizardi18n.getString(
                    "haoracle.oraclePropsReviewPanel.error.input.detail",
                    new String[] { propName });
        }

        if (error) {
            result = new ErrorValue(new Boolean(false), detail);

            return result;
        }

        try {

            if (xPropArg instanceof ResourcePropertyString) {
                zResultProp = new ResourcePropertyString();

                ResourcePropertyString rps = (ResourcePropertyString)
                    zResultProp;
                rps.setName(propName);

                if ((propVal != null) && (propVal.length() != 0)) {
                    rps.setValue(propVal);
                } else {

                    // Will come here only if the property is not a required
                    // field.
                    String defValue = ((ResourcePropertyString) xPropArg)
                        .getDefaultValue();

                    if ((defValue != null) && (defValue.length() != 0)) {
                        rps.setValue(defValue);
                    } else {
                        rps.setValue("");
                        rps.setDefaultValue("");
                    }
                }
            } else if (xPropArg instanceof ResourcePropertyInteger) {
                zResultProp = new ResourcePropertyInteger();

                ResourcePropertyInteger rpi = (ResourcePropertyInteger)
                    zResultProp;
                rpi.setName(propName);

                if ((propVal != null) && (propVal.length() != 0)) {
                    rpi.setValue(new Integer(propVal));
                } else {
                    rpi.setValue(((ResourcePropertyInteger) xPropArg)
                        .getDefaultValue());
                }
            } else if (xPropArg instanceof ResourcePropertyBoolean) {
                zResultProp = new ResourcePropertyBoolean();

                ResourcePropertyBoolean rpb = (ResourcePropertyBoolean)
                    zResultProp;
                rpb.setName(propName);

                if ((propVal != null) && (propVal.length() != 0)) {
                    rpb.setValue(Boolean.valueOf(propVal));
                } else {
                    rpb.setValue(((ResourcePropertyBoolean) xPropArg)
                        .getDefaultValue());
                }
            } else if (xPropArg instanceof ResourcePropertyEnum) {
                zResultProp = new ResourcePropertyEnum();

                ResourcePropertyEnum rpe = (ResourcePropertyEnum) zResultProp;
                rpe.setName(propName);
                rpe.setValue(propVal);
            } else if (xPropArg instanceof ResourcePropertyStringArray) {
                zResultProp = new ResourcePropertyStringArray();

                ResourcePropertyStringArray rpsa = (ResourcePropertyStringArray)
                    zResultProp;
                rpsa.setName(propName);

                if ((propVal != null) && (propVal.length() != 0)) {
                    String newValue[] = null;
                    StringTokenizer st = new StringTokenizer(propVal,
                            Util.COMMA);

                    if (st.hasMoreTokens()) {
                        newValue = new String[st.countTokens()];

                        for (int j = 0; st.hasMoreTokens(); j++) {
                            newValue[j] = st.nextToken();
                        }
                    }

                    rpsa.setValue(newValue);
                } else {
                    rpsa.setValue(((ResourcePropertyStringArray) xPropArg)
                        .getDefaultValue());
                }
            }
        } catch (IllegalArgumentException iae) {
            detail += ResourcePropertyUtil.getPropertyErrorMessage(zResultProp);
            result = new ErrorValue(new Boolean(false), detail);

            return result;
        }

        wizardModel.setWizardValue(rtrPropName, zResultProp);
        xPropArg = null;

        return new ErrorValue(new Boolean(true), "");
    }

    private ErrorValue validateInput(String rtrProps[]) {

        ErrorValue err = new ErrorValue();
        err.setReturnVal(Boolean.TRUE);

        StringBuffer errStr = new StringBuffer();

        ResourceProperty zLocProp = null;
        HashMap userInput = new HashMap();
        Vector vProps = new Vector();

        for (int j = 0; j < rtrProps.length; j++) {
            zLocProp = (ResourceProperty) wizardModel.getWizardValue(
                    rtrProps[j] + curNode);

            if (zLocProp == null) {
                zLocProp = (ResourceProperty) wizardModel.getWizardValue(
                        rtrProps[j]);
            }

            String name = zLocProp.getName();
            userInput.put(name, ResourcePropertyUtil.getValue(zLocProp));

            if (!vProps.contains(name)) {
                vProps.add(name);
            }
        }

        String props[] = (String[]) vProps.toArray(new String[0]);

        // get RACModel
	RACModel racModel = RACInvokerCreator.getRACModel();

        String selZoneCluster = CliUtil.getZoneClusterName(wizardModel);

	HashMap helperData = new HashMap();
	helperData.put(Util.SEL_ZONE_CLUSTER, selZoneCluster);

        ErrorValue errVal = racModel.validateInput(null, curNode, props, userInput, helperData);

        if (!errVal.getReturnValue().booleanValue()) {
            errStr = new StringBuffer(errVal.getErrorString());
            err.setReturnVal(Boolean.FALSE);
            err.setErrorString(errStr.toString());
        }

        return err;
    }

    private void populateRTRProperties(String resourceKey) {
        if (wizardModel.getWizardValue(resourceKey) == null) {

            String nodeList[] = (String[]) wizardModel.getWizardValue(
                    Util.RG_NODELIST);
            String nodeEndPoint = (String) wizardModel.getWizardValue(
                    Util.NODE_TO_CONNECT);

	    RACModel racModel = RACInvokerCreator.getRACModel();

            if (racModel != null) {
                String propertyNames[] = { resourceKey };
                HashMap discoveredMap = racModel.discoverPossibilities(
		    null, nodeEndPoint, propertyNames, null);

                // Copy the Data to the WizardModel
                if (discoveredMap == null) {
                    return;
                }

                discoveredMap = (HashMap) discoveredMap.get(resourceKey);

                if (discoveredMap == null) {
                    return;
                }

                propertyNames = new String[discoveredMap.size()];

                Set entrySet = discoveredMap.entrySet();
                Iterator setIterator = entrySet.iterator();
                int i = 0;

                while (setIterator.hasNext()) {
                    Map.Entry entry = (Map.Entry) setIterator.next();
                    String key = (String) entry.getKey();
                    wizardModel.setWizardValue(key, entry.getValue());
                    propertyNames[i++] = key;
                }

                wizardModel.setWizardValue(resourceKey, propertyNames);
            }
        }
    }
}
