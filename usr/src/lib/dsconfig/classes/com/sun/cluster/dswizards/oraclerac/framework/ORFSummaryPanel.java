/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)ORFSummaryPanel.java	1.13	09/03/11 SMI"
 */

package com.sun.cluster.dswizards.oraclerac.framework;


import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.dataservices.common.ErrorValue;


// CMASS
import com.sun.cluster.agent.dataservices.utils.Util;

// CLI Wizard SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliUtil;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;

// CLI wizards
import com.sun.cluster.dswizards.hastorageplus.HASPWizardConstants;
import com.sun.cluster.dswizards.oraclerac.invoker.RACInvokerCreator;

// J2SE
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.Vector;

import com.sun.cluster.model.RACModel;

/**
 * Summary page to the OracleRAC Framework Wizard
 */
public class ORFSummaryPanel extends WizardLeaf {

    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N wizardi18n; // Message Bundle


    /**
     * Creates a new instance of ORFSummaryPanel
     */
    public ORFSummaryPanel() {
        super();
    }

    /**
     * Creates a ORFSummary Panel with the given name and tree manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public ORFSummaryPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates a ORFSummary Panel with the given name and associates it with the
     * WizardModel and WizardFlow
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     * @param  wizardi18n  Resource Bundle
     */
    public ORFSummaryPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;

        // Get the I18N instance
        this.wizardi18n = wizardi18n;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {

        String title = wizardi18n.getString("orf.summarypanel.title");
        String desc1 = wizardi18n.getString("orf.summarypanel.desc1");
        String contwiz = wizardi18n.getString("cliwizards.continue");
        String table_title = wizardi18n.getString("orf.summarypanel.desc2");
        String heading1 = wizardi18n.getString("orf.summarypanel.heading1");
        String heading2 = wizardi18n.getString("orf.summarypanel.heading2");
        String orfrgname_txt = wizardi18n.getString(
                "orf.summarypanel.orfrg.name");
        String orfresname_txt = wizardi18n.getString(
                "orf.summarypanel.orfresource.name");
        String orfudlmresname_txt = wizardi18n.getString(
                "orf.summarypanel.orfudlmresource.name");
        String orfsvmresname_txt = wizardi18n.getString(
                "orf.summarypanel.orfsvmresource.name");
        String orfvxvmresname_txt = wizardi18n.getString(
                "orf.summarypanel.orfvxvmresource.name");
        String create_txt = wizardi18n.getString(
                "ttydisplay.menu.createConfiguration");
        String help_text = wizardi18n.getString("hasp.summarypanel.cli.help");
        String cchar = (String) wizardi18n.getString("ttydisplay.c.char");
        String back = wizardi18n.getString("ttydisplay.back.char");
        String propNameText = wizardi18n.getString("orf.summarypanel.propname");
        String propTypeText = wizardi18n.getString("orf.summarypanel.proptype");
        String propDescText = wizardi18n.getString("orf.summarypanel.propdesc");
        String yes = wizardi18n.getString("cliwizards.yes");
        String no = wizardi18n.getString("cliwizards.no");
        String currValText = wizardi18n.getString("orf.summarypanel.currval");
        String continue_txt = wizardi18n.getString("cliwizards.continue");

        String subheadings[] = { heading1, heading2 };

        boolean svm_selected = false;
        boolean vxvm_selected = false;
        boolean udlm_selected = false;
        String orfsvmResName = null;
        String orfvxvmResName = null;
        String orfudlmResName = null;

	String keyPrefix = (String) wizardModel.getWizardValue(
		Util.KEY_PREFIX);
	
        List selStorageMgmtSchemes = (List) wizardModel.getWizardValue(
		keyPrefix + Util.ORF_STORAGE_MGMT_SCHEMES);
 
        ArrayList frmwrkRT = (ArrayList) wizardModel.getWizardValue(
                Util.FRMWRK_RT);

        if (frmwrkRT.contains(Util.RAC_UDLM_RTNAME)) {
            udlm_selected = true;
        }

	String zoneClusterName = CliUtil.getZoneClusterName(wizardModel);

	if (zoneClusterName == null ||
	    zoneClusterName.trim().length() == 0) {
	    if (selStorageMgmtSchemes.contains(Util.SVM) ||
		selStorageMgmtSchemes.contains(Util.QFS_SVM)) {
		svm_selected = true;
	    }
	}

        if (selStorageMgmtSchemes.contains(Util.VXVM)) {
            vxvm_selected = true;
        }


        // Enable Navigation
        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        boolean flag = true;

        // Review Panel Table
        Vector reviewPanelVect = new Vector();

        // Resource Group Name
        String orfRgName = (String) wizardModel.getWizardValue(
                Util.ORF_RESOURCE_GROUP + Util.NAME_TAG);

        // ORF Resource name
        String orfResName = (String) wizardModel.getWizardValue(Util.ORF_RID +
                Util.NAME_TAG);

        // UDLM Resource name
        if (udlm_selected) {
            orfudlmResName = (String) wizardModel.getWizardValue(
                    Util.ORF_UDLM_RID + Util.NAME_TAG);
        }

        if (svm_selected) {
            orfsvmResName = (String) wizardModel.getWizardValue(
                    Util.ORF_SVM_RID + Util.NAME_TAG);
        }

        if (vxvm_selected) {
            orfvxvmResName = (String) wizardModel.getWizardValue(
                    Util.ORF_VXVM_RID + Util.NAME_TAG);
        }


        while (flag) {
            TTYDisplay.clearScreen();
            TTYDisplay.clear(1);

            // Title
            TTYDisplay.printTitle(title);
            TTYDisplay.clear(1);
            TTYDisplay.pageText(desc1, 4);
            TTYDisplay.clear(1);

            // initialize
            reviewPanelVect.removeAllElements();

            // ORF RG Name
            reviewPanelVect.add(orfrgname_txt);
            reviewPanelVect.add(orfRgName);

            // ORF Resource name
            reviewPanelVect.add(orfresname_txt);
            reviewPanelVect.add(orfResName);

            // UDLM Resource name
            if (udlm_selected) {
                reviewPanelVect.add(orfudlmresname_txt);
                reviewPanelVect.add(orfudlmResName);
            }

            if (svm_selected) {
                reviewPanelVect.add(orfsvmresname_txt);
                reviewPanelVect.add(orfsvmResName);
            }

            if (vxvm_selected) {
                reviewPanelVect.add(orfvxvmresname_txt);
                reviewPanelVect.add(orfvxvmResName);
            }


            HashMap customTags = new HashMap();
            customTags.put(cchar, create_txt);

            String option = TTYDisplay.create2DTable(table_title, subheadings,
                    reviewPanelVect, customTags, help_text);

            if (option.equals(back)) {
                this.cancelDirection = true;

                return;
            }

            TTYDisplay.clear(3);
            TTYDisplay.setNavEnabled(false);

            if (!option.equals(cchar)) {
                String prop = null;
                String currVal = null;

                if (option.equals("1")) {

                    // ORF RG name
                    prop = "orfrg";
                    currVal = orfRgName;

                }

                if (option.equals("2")) {

                    // ORF res name
                    prop = "orfresource";
                    currVal = orfResName;

                }

                if (udlm_selected) {

                    if (option.equals("3")) {

                        // ORF UDLM res name
                        prop = "orfudlmresource";
                        currVal = orfudlmResName;

                    }

                    if (option.equals("4")) {

                        if (svm_selected) {
                            prop = "orfsvmresource";
                            currVal = orfsvmResName;
                        } else if (vxvm_selected) {
                            prop = "orfvxvmresource";
                            currVal = orfvxvmResName;
                        }
                    }

                    if (option.equals("5")) {

                        // Change ORF VXLM res name
                        prop = "orfvxvmresource";
                        currVal = orfvxvmResName;

                    }
                } else {

                    if (option.equals("3")) {

                        if (svm_selected) {
                            prop = "orfsvmresource";
                            currVal = orfsvmResName;
                        } else if (vxvm_selected) {
                            prop = "orfvxvmresource";
                            currVal = orfvxvmResName;
                        }
                    }

                    if (option.equals("4")) {

                        // Change ORF VXLM res name
                        prop = "orfvxvmresource";
                        currVal = orfvxvmResName;

                    }
                }

                TTYDisplay.pageText(propNameText +
                    wizardi18n.getString("orf.summarypanel." + prop + ".name"));

                TTYDisplay.pageText(propDescText +
                    wizardi18n.getString("orf.summarypanel." + prop + ".desc"));

                TTYDisplay.pageText(propTypeText +
                    wizardi18n.getString("orf.summarypanel." + prop + ".type"));

                TTYDisplay.pageText(currValText + currVal);

                TTYDisplay.clear(1);
                TTYDisplay.promptForEntry(continue_txt);

            } else {
                break;
            }

            TTYDisplay.clear(3);
        }

        createCommands();
    }

    private void createCommands() {

        try {

            String nodeList[] = (String[]) wizardModel.getWizardValue(
                    Util.RG_NODELIST);

            boolean svm_selected = false;
            boolean vxvm_selected = false;
            boolean udlm_selected = false;
	    String zoneClusterName = CliUtil.getZoneClusterName(wizardModel);

	    String keyPrefix = (String) wizardModel.getWizardValue(
		Util.KEY_PREFIX);

	    List selStorageMgmtSchemes = (List) wizardModel.getWizardValue(
		keyPrefix + Util.ORF_STORAGE_MGMT_SCHEMES);

            ArrayList frmwrkRT = (ArrayList) wizardModel.getWizardValue(
                    Util.FRMWRK_RT);

            if (frmwrkRT.contains(Util.RAC_UDLM_RTNAME)) {
                udlm_selected = true;
            }

	    if (zoneClusterName == null || zoneClusterName.trim().length() == 0) {
		if (selStorageMgmtSchemes.contains(Util.SVM) ||
		    selStorageMgmtSchemes.contains(Util.QFS_SVM)) {
		    svm_selected = true;
		}
	    }

            if (selStorageMgmtSchemes.contains(Util.VXVM)) {
                vxvm_selected = true;
            }

            // Create ORF-RG, ORF-res, UDLM-res, and optionally svm, vxvm res
            // Create the Command Generation Structure
            Map confMap = new HashMap();
            HashMap rgProps = new HashMap();


            /** ResourceGroup Structure */

            // ResourceGroup Name
            confMap.put(Util.ORF_RESOURCE_GROUP + Util.NAME_TAG,
                (String) wizardModel.getWizardValue(
                    Util.ORF_RESOURCE_GROUP + Util.NAME_TAG));
            // ResourceGroup Properties

            // Get comma separated nodelist to pass to command generator
	    String commaSeparatedNodeList = nodeList[0];

            for (int ctr = 1; ctr < nodeList.length; ctr++) {
		commaSeparatedNodeList = commaSeparatedNodeList + Util.COMMA +
		    nodeList[ctr];
            }

            rgProps.put(Util.NODELIST, commaSeparatedNodeList);
            rgProps.put(Util.MAX_PRIMARIES, String.valueOf(nodeList.length));
            rgProps.put(Util.DESIRED_PRIMARIES,
                String.valueOf(nodeList.length));
            rgProps.put(Util.RG_MODE, Util.SCALABLE_RG_MODE);
            confMap.put(Util.ORF_RESOURCE_GROUP + Util.RGPROP_TAG, rgProps);
	    confMap.put(Util.ORF_RESOURCE_GROUP + Util.ZC_TAG, zoneClusterName);

            /** Resource Structure */

            /*  ORF Resource */

            // Resource Name
            confMap.put(Util.ORF_RID + Util.NAME_TAG,
                wizardModel.getWizardValue(Util.ORF_RID + Util.NAME_TAG));

            // Resource Properties - must copy empty list as per
            // command generator requirements.
            confMap.put(Util.ORF_RID + Util.SYSPROP_TAG, new ArrayList());
            confMap.put(Util.ORF_RID + Util.EXTPROP_TAG, new ArrayList());
            confMap.put(Util.ORF_RID + Util.ZC_TAG, zoneClusterName);

            /** Resource Structure */

            /*  ORF UDLM Resource */
            if (udlm_selected) {

                // Resource Name
                confMap.put(Util.ORF_UDLM_RID + Util.NAME_TAG,
                    wizardModel.getWizardValue(
                        Util.ORF_UDLM_RID + Util.NAME_TAG));

                // Resource Properties - must copy empty list as per
                // command generator requirements.
                confMap.put(Util.ORF_UDLM_RID + Util.SYSPROP_TAG,
                    new ArrayList());
                confMap.put(Util.ORF_UDLM_RID + Util.EXTPROP_TAG,
                    new ArrayList());
		confMap.put(Util.ORF_UDLM_RID + Util.ZC_TAG,
		    zoneClusterName);
            }


            if (svm_selected) {

                /** Resource Structure */

                /*  ORF SVM Resource */

                // Resource Name
                confMap.put(Util.ORF_SVM_RID + Util.NAME_TAG,
                    wizardModel.getWizardValue(
                        Util.ORF_SVM_RID + Util.NAME_TAG));

                // Resource Properties - must copy empty list as per
                // command generator requirements.
                confMap.put(Util.ORF_SVM_RID + Util.SYSPROP_TAG,
                    new ArrayList());
                confMap.put(Util.ORF_SVM_RID + Util.EXTPROP_TAG,
                    new ArrayList());
                confMap.put(Util.ORF_SVM_RID + Util.ZC_TAG,
		    zoneClusterName);
            }

            if (vxvm_selected) {

                /** Resource Structure */

                /*  ORF VXVM Resource */

                // Resource Name
                confMap.put(Util.ORF_VXVM_RID + Util.NAME_TAG,
                    wizardModel.getWizardValue(
                        Util.ORF_VXVM_RID + Util.NAME_TAG));

                // Resource Properties - must copy empty list as per
                // command generator requirements.
                confMap.put(Util.ORF_VXVM_RID + Util.SYSPROP_TAG,
                    new ArrayList());
                confMap.put(Util.ORF_VXVM_RID + Util.EXTPROP_TAG,
                    new ArrayList());
                confMap.put(Util.ORF_VXVM_RID + Util.ZC_TAG,
		    zoneClusterName);
            }

            generateCommands(confMap);

        } catch (Exception ex) {
	    ex.printStackTrace();
        }
    }

    /**
     * Generates the commands by invoking the command generator
     */
    private boolean generateCommands(Map confMap) {

        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);
        RACModel racModel = RACInvokerCreator.getRACModel();

        List commandList = null;
        Thread t = null;
        boolean retVal = true;

        try {
            TTYDisplay.clear(1);
            t = TTYDisplay.busy(wizardi18n.getString(
                        "cliwizards.executingMessage"));
            t.start();
            commandList = racModel.generateCommands(null, nodeEndPoint, confMap);
        } catch (CommandExecutionException cee) {
            wizardModel.setWizardValue(Util.EXITSTATUS, cee);
            commandList = racModel.getCommandList(null, nodeEndPoint);
            retVal = false;
        } finally {
            wizardModel.setWizardValue(Util.CMDLIST, commandList);

            try {
                t.interrupt();
                t.join();
            } catch (Exception exe) {
            }
        }

        return retVal;
    }
}
