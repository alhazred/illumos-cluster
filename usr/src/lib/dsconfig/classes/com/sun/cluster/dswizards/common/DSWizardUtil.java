/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident "@(#)DSWizardUtil.java 1.1     08/06/25 SMI"
 */

package com.sun.cluster.dswizards.common;

public class DSWizardUtil {

    // As and when new wizards are written, this class should be used for 
    // defining strings and functions common to both text-based and GUI wizards.
    // Would be ideal if we could have the existing wizard code use this class
    // but that might be a huge effort. For now, the plan is to have any new
    // wizards utilize this class.

    public static final String NATIVE_SUPPORT = "0";
    public static final String UDLM_SUPPORT = "1";
    public static final String CLUSTERWARE_SUPPORT = "ClusterWareSupport";
}
