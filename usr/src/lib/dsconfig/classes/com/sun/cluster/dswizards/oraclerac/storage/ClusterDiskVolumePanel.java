/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */


/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ClusterDiskVolumePanel.java	1.6	09/03/11 SMI"
 */

package com.sun.cluster.dswizards.oraclerac.storage;

// CMASS
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.dataservices.utils.DiskGroupInfo;
import com.sun.cluster.agent.dataservices.utils.ClusterDiskGroupInfo;

// CLI SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;

// Wizard Common
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.CliUtil;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.oraclerac.invoker.RACInvokerCreator;

import com.sun.cluster.model.RACModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

/**
 * This panel lists to the user all the OBAN,Shared VxVM diskgroups in the
 * cluster that are NOT monitored by ScalDeviceGroup resources
 */
public class ClusterDiskVolumePanel extends WizardLeaf {


    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N messages; // Message Bundle

    /**
     * Creates a new instance of ClusterDiskVolumePanel
     */
    public ClusterDiskVolumePanel() {
        super();
    }

    /**
     * Creates an ClusterDisks Panel with the given name and tree manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public ClusterDiskVolumePanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates an ClusterDisks Panel for racstorage Wizard with the given name
     * and associates it with the WizardModel and WizardFlow
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     */
    public ClusterDiskVolumePanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N messages) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;

        // Get the I18N instance
        this.messages = messages;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {
	String selectedDGName = (String) wizardModel.getWizardValue(
	    Util.DISKGROUP_NAME);

	String dgType = (String) wizardModel.getWizardValue(
	    Util.DISKGROUP_TYPE);

        String title = messages.getString("racstorage.clusterDiskVolumePanel.title");
        String desc = messages.getString("racstorage.clusterDiskVolumePanel.desc",
			new String[] { selectedDGName } );
        String subTitle = messages.getString(
                "racstorage.clusterDiskVolumePanel.subTitle");
        String help = messages.getString(
                "racstorage.clusterDiskVolumePanel.cli.help");
        String dchar = messages.getString("ttydisplay.d.char");
        String done = messages.getString("ttydisplay.menu.done");
        String yes = messages.getString("cliwizards.yes");
        String no = messages.getString("cliwizards.no");
	String all = messages.getString("ttydisplay.menu.all");
	String achar = messages.getString("ttydisplay.a.char");
	String refresh = messages.getString("ttydisplay.menu.refresh");
	String rchar = messages.getString("ttydisplay.r.char");
        String exitOption = no;
	String empty = messages.getString("racstorage.clusterDiskVolumePanel.empty");
	String emptyDesc = messages.getString("racstorage.clusterDiskVolumePanel.empty.desc");
        String emptySelection = messages.getString(
                "racstorage.clusterDiskVolumePanel.emptySelection");
        String continueConfirm = messages.getString(
                "cliwizards.continueConfirm");
        String continue_txt = messages.getString("ttydisplay.msg.cli.enter");
        String back = messages.getString("ttydisplay.back.char");

        String baseClusterNodeList[] = (String[]) wizardModel.getWizardValue(
	    Util.BASE_CLUSTER_NODELIST);


	do {
	    TTYDisplay.clearScreen();
	    TTYDisplay.clear(1);

	    // Enable Back and Help
	    TTYDisplay.setNavEnabled(true);
	    this.cancelDirection = false;

	    // Title and Description
	    TTYDisplay.printTitle(title);
	    TTYDisplay.pageText(desc);

	    // Populate the table
	    String[] volumeData = null;

	    ClusterDiskGroupInfo clusterDiskGroupInfo = null;

	    clusterDiskGroupInfo = (ClusterDiskGroupInfo) wizardModel.getWizardValue(
		    Util.SVMDEVICEGROUP);

	    if (clusterDiskGroupInfo != null) {

		List diskGroups = clusterDiskGroupInfo.getDiskGroupInfoList();
		if (diskGroups != null) {
		    int size = diskGroups.size();
		    int i = 0;
		    boolean found = false;
		    while ( i < size && !found) {
			DiskGroupInfo diskGroup = (DiskGroupInfo) diskGroups.get(i);
			
			String dgName = diskGroup.getDiskGroupName();
			if (selectedDGName.equals(dgName)) {
			    found = true;

			    HashMap availSVMVolMap = diskGroup.getAvailSVMVolMap();
			    Set keySet1 = null;
			    Set keySet2 = null;

			    if (availSVMVolMap != null &&
				!availSVMVolMap.isEmpty()) {
				// there are volumes available to be used
				keySet1 = availSVMVolMap.keySet();
				volumeData = (String []) keySet1.toArray(
				    new String[0]);
			    }

			    HashMap availQfsSVMVolMap = diskGroup.
				getAvailQfsSVMVolMap();

			    if (availQfsSVMVolMap != null &&
				!availQfsSVMVolMap.isEmpty()) {
				// there are volumes available to be used
				keySet2 = availQfsSVMVolMap.keySet();
			    }

			    if (keySet1 != null) {
				if (keySet2 != null) {
				    keySet1.addAll(keySet2);
				}
			    } else {
				if (keySet2 != null) {
				    keySet1 = keySet2;
				}
			    }

			    if (keySet1 != null) {
				volumeData = (String []) keySet1.toArray(
				    new String[0]);
			    }
			}
			i++;
		    }
		}
	    }

	    HashMap customTags = new HashMap();
	    customTags.put(achar, all);
	    customTags.put(rchar, refresh);

	    HashMap newDGResources = (HashMap) wizardModel.getWizardValue(
		Util.NEW_DG_RESOURCES); 

	    List selectedOptions = new ArrayList();

	    if (newDGResources != null) {
		Set keySet = newDGResources.keySet();
		Iterator iterator = keySet.iterator();
		boolean found = false;

		while (iterator.hasNext() && !found) {
		    String key = (String) iterator.next();
		    HashMap dgMap = (HashMap) newDGResources.get(key);
		   
		    String dgName = null;
		    String volumes = null;
		    if (dgMap != null) {
			dgName = (String) dgMap.get(Util.DISKGROUP_NAME);
			volumes = (String) dgMap.get(Util.LOGICAL_DEVLIST);
		    }

		    if (dgName.equals(selectedDGName)) {
			if (volumes != null) {
			    String[] volumeArr = volumes.split(Util.COMMA);
			    if (volumeArr != null) {
				for (int i = 0; i < volumeArr.length; i++) {
				    if (!selectedOptions.contains(volumeArr[i])) {
					selectedOptions.add(volumeArr[i]);
				    }
				}
			    }
			}
			found = true;
		    }
		}
	    }

	    List options = null;

	    if (volumeData != null && volumeData.length > 0) {

		// Display the (n*3) Table

		options = TTYDisplay.getScrollingOptions(
		    subTitle, volumeData,
		    customTags, selectedOptions,
		    help, true);
	    } else {
		TTYDisplay.pageText(subTitle);
		TTYDisplay.clear(1);
		TTYDisplay.pageText(emptyDesc);
		TTYDisplay.clear(2);

		String choice = TTYDisplay.promptForEntry(continue_txt);

		if (choice.equals(back)) {
		    wizardModel.setWizardValue(
			ORSWizardConstants.SHOW_CLUSTER_DEVICES, null);

		    // remove selected dgname from selection
		    removeSelectedDG(selectedDGName);

		    this.cancelDirection = true;

		    return;
		}

		continue;
	    }

	    // Handle Back
	    if (options == null) {
		wizardModel.setWizardValue(
		    ORSWizardConstants.SHOW_CLUSTER_DEVICES, null);

		// remove selected dgname from selection
		removeSelectedDG(selectedDGName);
		this.cancelDirection = true;

		return;
	    }


	    if (options.contains(refresh)) {
		// refresh the data
		removeSelectedDG(selectedDGName);

		continue;
	    } else if (options.contains(all)) {
		// Select all
		exitOption = yes;
		
		selectedOptions = (List) Arrays.asList(volumeData);
		addToSelectedDG(selectedDGName, dgType, selectedOptions);
		wizardModel.setWizardValue(
		    ORSWizardConstants.SHOW_CLUSTER_DEVICES, null);
		wizardModel.setWizardValue(
		    ORSWizardConstants.SHOW_DISKS_SUMMARY, "true");
	    } else {
		exitOption = yes;

		for (Iterator i = options.iterator(); i.hasNext();) {
		    String optionValue = (String) i.next();

		    if (!selectedOptions.contains(optionValue)) {
			selectedOptions.add(optionValue);
		    }
		}

		if (selectedOptions.size() == 0) {
			
		    TTYDisplay.printWarning(emptySelection);
		    String choice = TTYDisplay.promptForEntry(continue_txt);
		    if (choice.equals(back)) {
			removeSelectedDG(selectedDGName); 
			wizardModel.setWizardValue(
			    ORSWizardConstants.SHOW_CLUSTER_DEVICES, null);
		    }
		} else {
		    addToSelectedDG(selectedDGName, dgType, selectedOptions);		
		    wizardModel.setWizardValue(
			ORSWizardConstants.SHOW_CLUSTER_DEVICES, null);
		    wizardModel.setWizardValue(
			ORSWizardConstants.SHOW_DISKS_SUMMARY, "true");
		}
	    }
	} while (!exitOption.equalsIgnoreCase(yes));

	return;
    }

    private void addToSelectedDG(String selectedDGName, String dgType, List volumesList) {
	HashMap newDGMap = (HashMap) wizardModel.getWizardValue(
	    Util.NEW_DG_RESOURCES);

	String[] volumeArray = (String [])volumesList.toArray(new String [0]);
	String volumes = Util.convertArraytoString(volumeArray);

	boolean found = false;

	String dgBCResourceName = null;
	String dgZCResourceName = null;
	String dgBCRGName = null;
	String dgZCRGName = null;

	HashMap dgMap = null;
	if (newDGMap == null) {
	    newDGMap = new HashMap();
	}

	String selectedZoneCluster = CliUtil.getZoneClusterName(wizardModel);

	if (selectedZoneCluster != null &&
	    selectedZoneCluster.trim().length() > 0) {

	    // Given a dgName find out if it is SVM or
	    // QFS_SVM
	    // this is needed only for a Zone Cluster case

	    ClusterDiskGroupInfo clusterDiskGroupInfo =
		(ClusterDiskGroupInfo) wizardModel.getWizardValue(Util.SVMDEVICEGROUP);	
	    if (clusterDiskGroupInfo != null) {
		List diskGroupInfoList = clusterDiskGroupInfo.getDiskGroupInfoList();

		DiskGroupInfo diskGroupInfo = null;
		if (diskGroupInfoList != null) {
		    int size = diskGroupInfoList.size();
		    found = false;
		    int j = 0;

		    while (j < size && !found) {
			diskGroupInfo = (DiskGroupInfo) diskGroupInfoList.get(j);
			String diskGroupName = diskGroupInfo.getDiskGroupName();
			if (diskGroupName.equals(selectedDGName)) {
			    found = true;
			} else {
			    j++;
			}
		    }
		}

		ArrayList qfsVolumes = null;
		ArrayList svmVolumes = null;

		if (diskGroupInfo != null) {
		    // find selected volume in the availSVMMap
		    // and availQfsSVMVolMap

		    HashMap availSVMVolMap = diskGroupInfo.getAvailSVMVolMap();

		    HashMap availQfsSVMVolMap = diskGroupInfo.getAvailQfsSVMVolMap(); 

		    if (volumeArray != null) {
			for (int j = 0; j < volumeArray.length; j++) {
			    if (availSVMVolMap != null &&
				availSVMVolMap.containsKey(volumeArray[j])) {
				if (svmVolumes == null) {
				    svmVolumes = new ArrayList();
				}

				if (!svmVolumes.contains(volumeArray[j])) {
				    svmVolumes.add(volumeArray[j]);
				}
			    } else if (availQfsSVMVolMap != null &&
				availQfsSVMVolMap.containsKey(volumeArray[j])) {
				if (qfsVolumes == null) {
				    qfsVolumes = new ArrayList();
				}

				if (!qfsVolumes.contains(volumeArray[j])) {
				    qfsVolumes.add(volumeArray[j]);
				}
			    }
			}
		    }
		}

		if (qfsVolumes != null &&
		    qfsVolumes.size() > 0) {

		    // create resources in the base cluster
		    dgBCResourceName = getResourceName_forScalDeviceGroup(
			selectedDGName, null);

		    dgBCRGName = getRGName_forScalDeviceGroup(
			dgBCResourceName, null);

		    dgMap = new HashMap();
		    dgMap.put(Util.DISKGROUP_NAME, selectedDGName);
		    dgMap.put(Util.DISKGROUP_TYPE, dgType);
		    dgMap.put(Util.STORAGEGROUP, dgBCRGName);

		    String volList = Util.convertArraytoString(
			(String []) qfsVolumes.toArray(new String[0]));
		    dgMap.put(Util.LOGICAL_DEVLIST, volList);

		    newDGMap.put(dgBCResourceName, dgMap);
		}

		if (svmVolumes != null &&
		    svmVolumes.size() > 0) {

		    // create resources in the zone cluster
		    dgZCResourceName = getResourceName_forScalDeviceGroup(
			selectedDGName, selectedZoneCluster);

		    dgZCRGName = getRGName_forScalDeviceGroup(
			dgZCResourceName, selectedZoneCluster);

		    dgMap = new HashMap();
		    dgMap.put(Util.DISKGROUP_NAME, selectedDGName);
		    dgMap.put(Util.DISKGROUP_TYPE, dgType);
		    dgMap.put(Util.STORAGEGROUP, dgZCRGName);

		    String volList = Util.convertArraytoString(
			(String []) svmVolumes.toArray(new String[0]));
		    dgMap.put(Util.LOGICAL_DEVLIST, volList);
		    newDGMap.put(selectedZoneCluster +
			Util.COLON + dgZCResourceName, dgMap);
		}
	    }
	} else {
	    // configuring for a base cluster
	    dgBCResourceName = getResourceName_forScalDeviceGroup(
		selectedDGName, null);

	    dgBCRGName = getRGName_forScalDeviceGroup(
		dgBCResourceName, null);

	    dgMap = new HashMap();
	    dgMap.put(Util.DISKGROUP_NAME, selectedDGName);
	    dgMap.put(Util.DISKGROUP_TYPE, dgType);
	    dgMap.put(Util.LOGICAL_DEVLIST, volumes);
	    dgMap.put(Util.STORAGEGROUP, dgBCRGName);
	    newDGMap.put(dgBCResourceName, dgMap);
	}

	wizardModel.setWizardValue(Util.NEW_DG_RESOURCES, newDGMap);
    }

    private void removeSelectedDG(String selectedDGName) {
	HashMap newDGMap = (HashMap) wizardModel.getWizardValue(
	    Util.NEW_DG_RESOURCES);
	boolean found = false;
	if (newDGMap != null) {
	    Set keySet = newDGMap.keySet();
	    Iterator iterator = keySet.iterator();
	    while (iterator.hasNext() && !found) {
		String key = (String) iterator.next();
		HashMap dgMap = (HashMap) newDGMap.get(key);
		String dgName = (String) dgMap.get(Util.DISKGROUP_NAME);
		if (dgName.equals(selectedDGName)) {
		    dgMap.put(Util.LOGICAL_DEVLIST, null);
		    found = true;
		}
	    }
	}

	if (found) {
	    wizardModel.setWizardValue(Util.NEW_DG_RESOURCES, newDGMap);
	}
    }

    // Get a unique name for Scalable Device Group
    private String getResourceName_forScalDeviceGroup(
	String dgName, String selectedZoneCluster) {
	String propertyName[] = new String[] { Util.STORAGERESOURCE };

	RACModel racModel = RACInvokerCreator.getRACModel();
	String nodeEndPoint = (String) wizardModel.getWizardValue(
	    Util.NODE_TO_CONNECT);

	HashMap helperData = new HashMap();
	helperData.put(Util.SEL_ZONE_CLUSTER, selectedZoneCluster);
	helperData.put(Util.PREFIX, dgName);

	Map discoveredMap = racModel.discoverPossibilities(
	    null, nodeEndPoint, propertyName, helperData);

	return (String) discoveredMap.get(Util.STORAGERESOURCE);
    }

    // Get a unique RGName for Scalable Device Group
    private String getRGName_forScalDeviceGroup(
	String resourceName, String selectedZoneCluster) {

	RACModel racModel = RACInvokerCreator.getRACModel();
	String nodeEndPoint = (String) wizardModel.getWizardValue(
	    Util.NODE_TO_CONNECT);

	HashMap helperData = new HashMap();
	helperData.put(Util.SEL_ZONE_CLUSTER, selectedZoneCluster);
	helperData.put(Util.RS_NAME, resourceName);

	String propertyName[] = new String[] { Util.STORAGEGROUP };
	HashMap resourceGroupMap = (HashMap) racModel.discoverPossibilities(
	    null, nodeEndPoint, propertyName, helperData);

	return (String) resourceGroupMap.get(Util.STORAGEGROUP);
    }



}
