/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ApacheWizardConfFilePanel.java 1.16     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.apache;

// J2SE

// CMASS
import com.sun.cluster.agent.dataservices.apache.ApacheMBean;
import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.utils.Util;

// CLI Wizard SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;

// Wizard Common
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;

import java.io.IOException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.management.MBeanServerConnection;
import javax.management.remote.JMXConnector;


/**
 * Panel to get the Apache Configuration File
 */
public class ApacheWizardConfFilePanel extends WizardLeaf {


    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N messages; // Message Bundle

    private ApacheMBean mBean = null; // Reference to MBean
    private JMXConnector localConnection = null; // Reference to JMXConnector


    /**
     * Creates a new instance of ApacheWizardConfFilePanel
     */
    public ApacheWizardConfFilePanel() {
        super();
    }

    /**
     * Creates an ApacheWizardConfFile Panel with the given name and tree
     * manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public ApacheWizardConfFilePanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates a ApacheWizardConfFile Panel with the given name and associates
     * it with the WizardModel, WizardFlow and i18n reference
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     * @param  messages  WizardI18N
     */
    public ApacheWizardConfFilePanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N messages) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;

        // Get the I18N instance
        this.messages = messages;

        localConnection = getConnection(Util.getClusterEndpoint());
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {

        // Title
        String title = messages.getString("apache.confFilePanel.title");
        String desc = messages.getString("apache.confFilePanel.description");

        // Get the Help Text
        String helpText = messages.getString("apache.confFilePanel.help.1") +
            "\n \n" + messages.getString("apache.confFilePanel.help.2");

        String backOption = messages.getString("ttydisplay.back.char");

        // Get query text
        String queryText = messages.getString("apache.confFilePanel.query");
        String confFileQuery = messages.getString(
                "apache.confFilePanel.queryFileLocation");
        String echar = messages.getString("ttydisplay.e.char");
        String on = messages.getString("cliwizards.on");
        String enterCustom = messages.getString(
                "apache.confFilePanel.enterLocation");
        String nodeQueryText = messages.getString(
                "apache.confFilePanel.nodeQuery");

        String confFile = null;
        String confFileNode = null;

        // Discover Apache Configuration file on each node or zone
        String nodeListArr[] = (String[]) wizardModel.getWizardValue(
                Util.RG_NODELIST);
        List choiceList = new ArrayList();
        ApacheMBean apacheMbeans[] = new ApacheMBean[nodeListArr.length];
        JMXConnector localConnection = getConnection(Util.getClusterEndpoint());
        JMXConnector connectors[] = new JMXConnector[nodeListArr.length];
        Map discoveredMap = null;

        for (int i = 0; i < nodeListArr.length; i++) {
            String split_str[] = nodeListArr[i].split(Util.COLON);
            HashMap helperMap = null;
            String nodeEndPoint = Util.getNodeEndpoint(localConnection,
                    split_str[0]);
            connectors[i] = getConnection(nodeEndPoint);

            if (split_str.length == 2) {
                helperMap = new HashMap();
                helperMap.put(Util.APACHE_ZONE, nodeListArr[i]);
            }

            apacheMbeans[i] = ApacheWizardCreator.getApacheMBeanOnNode(
                    connectors[i]);
            discoveredMap = apacheMbeans[i].discoverPossibilities(
                    new String[] { Util.APACHE_CONF_FILE }, helperMap);

            String choiceArr[] = (String[]) discoveredMap.get(
                    Util.APACHE_CONF_FILE);

            if (choiceArr != null) {

                for (int x = 0; x < choiceArr.length; x++) {
                    StringBuffer choiceBuf = new StringBuffer(choiceArr[x]);
                    choiceBuf.append(" on ");
                    choiceBuf.append(nodeListArr[i]);
                    choiceList.add(choiceBuf.toString());
                }
            }
        }

        String choices[] = (String[]) choiceList.toArray(
                new String[choiceList.size()]);

        HashMap userInputs = new HashMap();
        HashMap customTags = new HashMap();
        customTags.put(echar, enterCustom);

        while (true) {
            TTYDisplay.clearScreen();
            TTYDisplay.clear(1);

            // Enable Back and Help
            TTYDisplay.setNavEnabled(true);
            this.cancelDirection = false;

            TTYDisplay.printTitle(title);
            TTYDisplay.clear(1);
            TTYDisplay.printSubTitle(desc);

            List entry = null;

            if (choices.length > 0) {
                entry = TTYDisplay.getScrollingOption(queryText, choices,
                        customTags, choices[0], helpText, true);

                // Handle back
                if (entry == null) {
                    this.cancelDirection = true;

                    return;
                }
            } else {
                entry = new ArrayList();
                entry.add(enterCustom);
            }

            // Process the user option
            if (entry.contains(enterCustom)) {
                TTYDisplay.clear(1);

                List nodeEntry = TTYDisplay.getScrollingOption(nodeQueryText,
                        nodeListArr, null, nodeListArr[0], helpText, false);
                confFileNode = (String) nodeEntry.get(0);
                TTYDisplay.clear(1);
                confFile = TTYDisplay.promptForEntry(confFileQuery);

                if (confFile.equals(backOption)) {
                    continue;
                }
            } else {
                String confFileInfo = (String) entry.get(0);
                confFile = (confFileInfo.split(" "))[0];
                confFileNode = (confFileInfo.split(" "))[2];
            }

            // Validate the confFile, if not acceptable show error
            String split_str[] = confFileNode.split(Util.COLON);
            JMXConnector nodeConnection = getConnection(split_str[0]);
            ApacheMBean mBeanOnNode = ApacheWizardCreator.getApacheMBeanOnNode(
                    nodeConnection);

            // Add ZonePath to the conf file if user had selected a zone
            String zoneExportPath = null;

            if (split_str.length > 1) {
                discoveredMap = mBeanOnNode.discoverPossibilities(
                        new String[] { Util.APACHE_ZONE }, confFileNode);
                zoneExportPath = (String) discoveredMap.get(Util.APACHE_ZONE);
            }

            userInputs.clear();
            userInputs.put(Util.CONF_FILE, confFile);
            userInputs.put(Util.APACHE_ZONE, zoneExportPath);

            ErrorValue errorVal = mBeanOnNode.validateInput(
                    new String[] { Util.APACHE_CONF_FILE }, userInputs, null);

            if (!errorVal.getReturnValue().booleanValue()) {
                StringBuffer errorBuf = new StringBuffer();
                errorBuf.append(messages.getString("apache.confFilePanel.error",
                        new String[] { confFile, confFileNode }));
                errorBuf.append("\n");
                errorBuf.append(messages.getString(errorVal.getErrorString()));
                TTYDisplay.printError(errorBuf.toString());
                TTYDisplay.clear(1);
                TTYDisplay.promptForEntry(messages.getString(
                        "cliwizards.continue"));
            } else {

                if (split_str.length > 1) {
                    wizardModel.setWizardValue(Util.APACHE_ZONE, split_str[1]);
                }

                wizardModel.setWizardValue(Util.APACHE_CONF_FILE, confFile);
                wizardModel.setWizardValue(Util.NODE_TO_CONNECT, confFileNode);

                // Reset the Doc Root so that the next panel will
                // rediscover the doc root, when the CONF file changes
                wizardModel.setWizardValue(Util.APACHE_DOC, null);
                discoveredMap = mBeanOnNode.discoverPossibilities(
                        new String[] { Util.APACHE_VERSION }, userInputs);

                Object verObj = discoveredMap.get(Util.APACHE_VERSION);

                if (verObj != null) {
                    wizardModel.setWizardValue(Util.APACHE_VERSION,
                        (String) verObj);
                }

                break;
            }
        }
    }
}
