/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)SampleWizardCreator.java 1.10     08/05/20 SMI"
 */

package com.sun.cluster.dswizards.sample;

import com.sun.cluster.dswizards.clisdk.core.*;
import com.sun.cluster.dswizards.common.*;


public class SampleWizardCreator extends WizardCreator {
    public SampleWizardCreator(String statefile) {
        super(statefile, null);
    }

    protected void createClientTree() {
        WizardTreeManager manager = getWizardTreeManager();
        WizardComposite root = getRoot();

        // Create the client panels
        SamplePanel firstPanel = new SamplePanel(wizardModel, "first panel");
        SamplePanel secondPanel = new SamplePanel(wizardModel, "second panel");
        root.addChild(firstPanel);
        root.addChild(secondPanel);
    }

    public static void main(String argv[]) {
        SampleWizardCreator creator = new SampleWizardCreator(argv[0]);
    }
}
