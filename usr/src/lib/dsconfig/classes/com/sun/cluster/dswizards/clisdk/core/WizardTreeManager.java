/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)WizardTreeManager.java 1.16     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.clisdk.core;

// J2SE

// CMASS
import com.sun.cluster.agent.dataservices.utils.Util;

// CLI Wizard Common
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardI18N;

import javax.management.remote.JMXConnector;

import javax.swing.JPanel;


/**
 * The WizardTreeManager class manages the creation of a wizard tree.
 *
 * <p>The WizardTreeManager is the core of the wizard client classes.  Each
 * WizardComponent wizard maintains a reference to the WizardTreeManager.
 *
 * <p>The WizardTreeManager communicates with the root CliDSConfigWizardModel
 * object directly
 */
public class WizardTreeManager {

    /** A value identifying the back button. */
    public static int BACK_BUTTON = 1;

    /** A value identifying the next button. */
    public static int NEXT_BUTTON = 2;

    /** A value identifying the cancel button. */
    public static int CANCEL_BUTTON = 4;

    /** A value identifying the exit button. */
    public static int EXIT_BUTTON = 8;

    /** A value identifying the help button. */
    public static int HELP_BUTTON = 16;

    /** The wizard creator object. */
    private WizardCreator creator = null;

    /*
     * The WizardI18N object.
     */
    private WizardI18N wizardi18n = null;

    /**
     * The application console through which queries and exit functionality can
     * be performed.
     */
    private AppConsole appConsole = null;

    /** The CliDSConfigWizardModel object. */
    private CliDSConfigWizardModel wizardModel = null;

    /** The locking object for the wizardModel. */
    private transient Object wizardModelLock = new Object();

    /** The root of the wizard client tree. */
    private WizardComposite root = null;

    /** The locking object for the client tree root. */
    private transient Object rootLock = new Object();

    /** The panel in which the client tree will be navigated. */
    private JPanel wizardPanel = null;

    /** The iterator responsible for navigating the wizard client tree. */
    private IteratorLayout iterator = null;

    /** cache the button(s) status info */
    private boolean isBackEnabled = false;
    private boolean isCancelEnabled = false;
    private boolean isExitEnabled = false;
    private boolean isHelpEnabled = false;
    private boolean isNextEnabled = false;

    /**
     * Used to flag if the TreeManager should set button focus. If false, it is
     * assumed that some panel will set the button focus...
     */
    private boolean canSetButtonFocus = true;

    /** Locking Object for access to the buttons */
    private transient Object buttonLock = new Object();

    /**
     * A temporary client tree root that is used to display the initialize panel
     * during initialization.
     */
    private transient WizardComposite tmpRoot = null;

    /** The thread used to initialize the wizard client tree. */
    private boolean initStatus = false;

    /** The locking object for the initialization thread. */
    private transient Object initStatusLock = new Object();

    /**
     * The locking object used to synchronize access to the executingAction
     * flag.
     */
    private transient Object actionLock = new Object();

    /**
     * Dispatch flag that indicates nothing is in the process of being
     * dispatched.
     */
    private final int NONE = 0;

    /** Dispatch flag that indicates the wizard is being initialized. */
    private final int INITIALIZE = 1;

    /**
     * Locking object used to synchronize access to the dispatchMessage and
     * dispatchArgument fields.
     */
    private Object dispatchMessageLock = new Object();

    /**
     * Indicates what action is being dispatched.  This field is locked with
     * dispatchMessageLock.
     */
    private int dispatchMessage = NONE;

    /**
     * Used to store an argument that may be required by the routine called by
     * the operateInSeparateThread method. This argument is made available to
     * the resulting thread.  This field is locked with dispatchMessageLock.
     */
    private transient Object dispatchArgument = null;


    /** String resource for the exit button in the exit confirmation dialog */
    private String exit_dialog_exit = null;


    /**
     * String resource for the continue button in the exit confirmation dialog
     */
    private String exit_dialog_continue = null;

    /**
     * Creates a WizardTreeManager object.
     *
     * @param  appConsole  The application console that this wizard manager
     * resides within.
     * @param  creator  The WizardCreator object which holds the client tree.
     */
    public WizardTreeManager(AppConsole appConsole, WizardCreator creator) {
        this.appConsole = appConsole;

        this.creator = creator;

        this.wizardi18n = creator.getWizardI18N();

        setWizardModel(creator.getWizardModel());

        createWizardPanel();
    }

    /**
     * Returns the root of the client panel tree.
     */
    private WizardComposite getTreeRoot() {

        synchronized (rootLock) {
            return (this.root);
        }
    }

    /**
     * Sets the root of the client panel tree.
     *
     * @param  root  The new client panel tree root.
     */
    private void setTreeRoot(WizardComposite root) {

        synchronized (rootLock) {
            this.root = root;
        }
    }

    public WizardCreator getWizardCreator() {
        return this.creator;
    }

    /**
     * Get the first component in this wizard
     */
    public WizardComponent getFirstComponent() {
        return getTreeRoot().next();
    }


    /**
     * Says whether we are currently traversing forward or not.
     *
     * @return  true if we arrived at the current panel by pressing a "next"
     * method.
     */
    public boolean isTraversingForward() {
        return (iterator.isTraversingForward());
    }

    /**
     * Sets the Wizard model that controls this wizard. Use with caution!
     *
     * @param  model  The new model to use for this tree.
     */
    public void setWizardModel(CliDSConfigWizardModel model) {

        synchronized (wizardModelLock) {
            this.wizardModel = model;
        }
    }

    /**
     * Gets the CliDSConfigWizardModel object for this wizard.
     *
     * @return  The current CliDSConfigWizardModel controlling this wizard.
     */
    public CliDSConfigWizardModel getWizardModel() {

        synchronized (wizardModelLock) {
            return (appConsole.getWizardModel());
        }
    }

    /**
     * Creates the wizard panel.  The wizard panel is the container in which the
     * wizard client tree will be displayed.  This method also creates the
     * client tree.
     */
    protected void createWizardPanel() {
        iterator = new IteratorLayout(this);

        wizardPanel = new JPanel(iterator);

        WizardComposite newTree = createSubTree(null);

        setTreeRoot(newTree);

        iterator.toNextPanel();
    }

    /**
     * Create a subtree with the specified name and the specified route to a
     * server side object.
     *
     * @param  route  The route to the correct server side object.
     * @param  subtreeName  The name of the desired subtree.  If this is set to
     * null, the entire wizard tree is created.
     *
     * @return  The desired tree, or null if the requested tree has not been
     * written into the wizard archive.
     */
    public WizardComposite createSubTree(String subtreeName) {

        /*
         * Use the creator to get the root.
         */
        this.root = creator.getRoot();

        if (root instanceof WizardComposite) {

            /**
             * Add the subtree to the iterator.
             */
            iterator.setRoot(wizardPanel, (WizardComposite) root);

            return ((WizardComposite) root);
        }

        return (null);
    }

    /**
     * Returns the current wizard panel.  The current panel is the wizard panel
     * currently being displayed by the IteratorLayout.
     *
     * @return  The wizard panel currently being displayed.
     */
    public WizardComponent getCurrentPanel() {
        return (iterator.getCurrentPanel());
    }

    /**
     * Invoked when the back button has been pressed.
     */
    public void backButtonPressed() {

        /*
         * Check if its text mode or if it is 1.1, if it is then sychronize
         * this method
         */
        synchronized (this.buttonLock) {
            actualBackButtonPressed();
        }
    }

    private void actualBackButtonPressed() {
        iterator.previous();
    }

    private void actualNextButtonPressed() {
        iterator.next();
    }

    /**
     * Invoked when the next button has been pressed.
     */
    public void nextButtonPressed() {

        /*
         * Check if its text mode or if it is 1.1, if it is then sychronize
         * this method
         */
        synchronized (this.buttonLock) {
            actualNextButtonPressed();
        }
    }

    /**
     * Invoked when the cancel button has been pressed.
     */
    public void cancelButtonPressed() {

        /*
         * Check if its text mode or if it is 1.1, if it is then sychronize
         * this method
         */
        synchronized (this.buttonLock) {
            actualCancelButtonPressed();
        }
    }

    private void actualCancelButtonPressed() {

        /*
         * Be sure that the cancel button is still valid on
         * the current panel.
         */
        WizardComponent currentPanel = iterator.getCurrentPanel();

        if (currentPanel instanceof WizardLeaf) {
            int buttonMask = ((WizardLeaf) currentPanel).getButtonMask();

            if ((buttonMask & CANCEL_BUTTON) != 0) {
                cancelConfirmed();

                return;
            }
        }

        /*
         * Resume the sequence if this is an invalid cancellation
         */
        cancelConfirmed();
    }

    /**
     * Invoked when the exit button has been pressed.
     */
    public void exitButtonPressed() {

        synchronized (this.buttonLock) {
            actualExitButtonPressed();
        }
    }

    private void actualExitButtonPressed() {

        WizardComponent currentPanel = iterator.getCurrentPanel();
        CliDSConfigWizardModel wizardModel = currentPanel.getWizardModel();
        String nodeList[] = (String[]) wizardModel.getWizardValue(
                Util.RG_NODELIST);
        JMXConnector localConnection = currentPanel.getConnection(Util
                .getClusterEndpoint());
        boolean islastPanel = currentPanel.isLast();

        if ((currentPanel instanceof WizardLeaf) && !islastPanel) {
            TTYDisplay.clear(3);

            String exitStr = wizardi18n.getString("cliwizards.confirmExit");
            String confirmExitText = wizardi18n.getString(
                    "ttydisplay.quit.warning");
            String yes = wizardi18n.getString("cliwizards.yes");
            String no = wizardi18n.getString("cliwizards.no");
            String option = TTYDisplay.getConfirmation(exitStr, yes, no);

            if (option.equalsIgnoreCase(yes)) {
                TTYDisplay.clear(1);

                // Ask user confirmation for saving the state file
                String confirm = TTYDisplay.getConfirmation(confirmExitText,
                        yes, no);

                if (confirm.equalsIgnoreCase(yes)) {

                    // Write to the state file
                    try {
                        wizardModel.writeToStateFile(localConnection
                            .getMBeanServerConnection());
                    } catch (Exception e) {
                        TTYDisplay.printError(wizardi18n.getString(
                                "dswizards.stateFile.error"));
                    } finally {

                        // Always close connection
                        currentPanel.closeConnections(nodeList);
                    }
                } else {
                    currentPanel.closeConnections(nodeList);
                }

                exitConfirmed();
            }
        } else if (islastPanel) {
            currentPanel.closeConnections(nodeList);
            exitConfirmed();
        }
    }

    /**
     * Invoked when the exit operation is confirmed by the user.
     */
    public void exitConfirmed() {
        System.exit(0);
    }

    /**
     * Invoked when the cancel operation is confirmed by the user.
     */
    public void cancelConfirmed() {

        /*
         * Be sure that the cancel button is still valid on
         * the current panel.
         */
        WizardComponent currentPanel = iterator.getCurrentPanel();

        if (currentPanel instanceof WizardLeaf) {
            int buttonMask = ((WizardLeaf) currentPanel).getButtonMask();

            if ((buttonMask & CANCEL_BUTTON) != 0) {
                iterator.cancel();
            }
        }

    }

    /**
     * Display a query to the user.  When a selection is made, the corresponding
     * method will be called on the target.
     *
     * @param  target  The object to call when a selection from the dialog is
     * made.
     * @param  message  The message to be displayed to the user.
     * @param  title  The title of the query window, or the title shown to the
     * user in CLI mode.
     * @param  buttons  The buttons (button labels) offered to the user to
     * select from.
     * @param  methods  A list of methods (in the same order as the list of
     * buttons) that will be called when a dialog selection is made.
     */
    public boolean displayQuery(Object target, String title, String message,
        String buttons[], String methods[]) {
        return appConsole.displayQuery(target, title, message, buttons,
                methods);
    }

    /**
     * Display a query to the user.  When a selection is made, the corresponding
     * method will be called on the target. This method is called by the
     * AutonextController when a panel is auto-navigated. This method will
     * display a QueryDialog to the user with the time specified, or closes the
     * dialog when a action is taken.
     *
     * @param  target  The object to call when a selection from the dialog is
     * made.
     * @param  showTime  The maximum time the dialog will be displayed before
     * the default action is taken
     * @param  message  The message to be displayed to the user.
     * @param  title  The title of the query window, or the title shown to the
     * user in CLI mode.
     * @param  buttons  The buttons (button labels) offered to the user to
     * select from.
     * @param  methods  A list of methods (in the same order as the list of
     * buttons) that will be called when a dialog selection is made.
     */
    public void displayQuery(Object target, int showTime, String title,
        String message, String buttons[], String methods[]) {
        appConsole.displayQuery(target, showTime, title, message, buttons,
            methods);
    }

    /**
     * Display a query to the user.  When a selection is made, the corresponding
     * method will be called on the target.
     *
     * @param  target  The object to call when a selection from the dialog is
     * made.
     * @param  message  The message to be displayed to the user.
     * @param  buttons  The buttons (button labels) offered to the user to
     * select from.
     * @param  methods  A list of methods (in the same order as the list of
     * buttons) that will be called when a dialog selection is made.
     */
    public boolean displayQuery(Object target, String message, String buttons[],
        String methods[]) {
        return appConsole.displayQuery(target, message, buttons, methods);
    }

    /**
     * Returns a AppConsole object represents the current wizard console.
     *
     * @return  a AppConsole object
     *
     * @see  com.sun.dswizards.clisdk.core.AppConsole
     */
    public AppConsole getAppConsole() {
        return appConsole;
    }

    /**
     * Sets the boolean indicates whether the button focus can be set by tree
     * manager.
     *
     * @param  can  a boolean indicates whether the button focus can set by the
     * tree manager.
     */
    public void TMCanSetButtonFocus(boolean can) {
        canSetButtonFocus = can;
    }

    /**
     * Checks whether the tree manager allow to reset button focus.
     *
     * @return  a boolean indicates whether the button focus can be set by the
     * tree manager
     */
    public boolean canTMSetButtonFocus() {
        return canSetButtonFocus;
    }

    /**
     * Determine if the Current Panel's Back Button is enabled
     *
     * @return  true/false; whether Back Button is enabled or not
     */
    public boolean isBackButtonEnabled() {
        return isBackEnabled;
    }

    /**
     * Determine if the Current Panel's Exit Button is enabled
     *
     * @return  true/false; whether Exit Button is enabled or not
     */
    public boolean isExitButtonEnabled() {
        return isExitEnabled;
    }

    /**
     * Determine if the Current Panel's Next Button is enabled
     *
     * @return  true/false; whether Next Button is enabled or not
     */
    public boolean isNextButtonEnabled() {
        return isNextEnabled;
    }

    /**
     * Determine if the Current Panel's Cancel Button is enabled
     *
     * @return  true/false; whether Cancel Button is enabled or not
     */
    public boolean isCancelButtonEnabled() {
        return isCancelEnabled;
    }

    /**
     * Determine if the Current Panel's Help Button is enabled
     *
     * @return  true/false; whether Help Button is enabled or not
     */
    public boolean isHelpButtonEnabled() {
        return isHelpEnabled;
    }
}
