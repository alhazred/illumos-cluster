/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)HostnameWizardIntroPanel.java 1.14     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.logicalhost;

// CLI Wizard SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;

// Wizard Common
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;


/**
 * Introduction page to the Hostname Wizards
 */
public class HostnameWizardIntroPanel extends WizardLeaf {


    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N messages; // Message Bundle
    private String wizardType; // Wizard Type


    /**
     * Creates a new instance of HostnameWizardIntroPanel
     */
    public HostnameWizardIntroPanel() {
        super();
    }

    /**
     * Creates a HostnameWizardIntroPanel Panel with the
     * given name and tree manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public HostnameWizardIntroPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    public HostnameWizardIntroPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N messages, String wizardType) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;
        this.wizardType = wizardType;

        // Get the I18N instance
        this.messages = messages;
        wizardModel.setWizardValue(LogicalHostWizardConstants.WIZARDTYPE,
            wizardType);
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {

        String back = messages.getString("ttydisplay.back.char");

        TTYDisplay.clearScreen();
        TTYDisplay.clear(2);

        // Title
        TTYDisplay.printTitle(messages.getString(
                wizardType + ".introPanel.title"));
        TTYDisplay.clear(2);
        TTYDisplay.pageText(messages.getString(
                wizardType + ".wizard.step1.instruction"), 4);
        TTYDisplay.clear(1);
        TTYDisplay.pageText(messages.getString(
                wizardType + ".wizard.step1.preq1"), 4);
        TTYDisplay.clear(1);
        TTYDisplay.pageText(messages.getString(
                wizardType + ".wizard.step1.preq2"), 4);
        TTYDisplay.clear(1);
        TTYDisplay.pageText(messages.getString("lhwizard.wizard.step1.preq3"),
            4);
        TTYDisplay.clear(2);

        String entry = TTYDisplay.promptForEntry(messages.getString(
                    "cliwizards.continue"));

        if (entry.equals(back)) {
            System.exit(0);
        }

        TTYDisplay.clear(2);
    }
}
