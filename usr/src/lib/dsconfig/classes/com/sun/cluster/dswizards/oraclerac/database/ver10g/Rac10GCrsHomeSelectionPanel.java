/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)Rac10GCrsHomeSelectionPanel.java	1.17	09/03/11 SMI"
 */
 
package com.sun.cluster.dswizards.oraclerac.database.ver10g;

// CMASS
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.dataservices.common.ErrorValue;

// Wizard Common
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.common.CliUtil;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.oraclerac.database.*;
import com.sun.cluster.dswizards.oraclerac.invoker.RACInvokerCreator;

// Model classes
import com.sun.cluster.model.RACModel;

// J2SE
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

/**
 * This panel gets the CRS Home Directory from the User
 */
public class Rac10GCrsHomeSelectionPanel extends RacDBBasePanel {
    /**
     * Creates an Rac10GCrsHomeSelectionPanel for the wizard with the given name
     * and associates it with the WizardModel and WizardFlow.
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     * @param  wizardType  WizardType
     */
    public Rac10GCrsHomeSelectionPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(name, model, wizardFlow, wizardi18n);
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        title = wizardi18n.getString(
                "oraclerac10g.crsHomeSelectionPanel.title");
        desc = wizardi18n.getString("oraclerac10g.crsHomeSelectionPanel.desc");

        String crshome_prompt = wizardi18n.getString(
                "oraclerac10g.crsHomeSelectionPanel.prompt");
        String help_text = wizardi18n.getString(
                "oraclerac10g.crsHomeSelectionPanel.cli.help");
        String invalidSelection = wizardi18n.getString(
                "oraclerac10g.crsHomeSelectionPanel.invalidselection");

        String selectedCrsHome = null;
        String defaultCrsHome;

        // Get the discovered CRS_HOME from the dataservice
        String nodeList[] = (String[]) wizardModel.getWizardValue(
	    Util.RG_NODELIST);
        String propNames[] = new String[] { Util.CRS_HOME };
        Vector vCrsHome = new Vector();

	HashMap helperData = new HashMap();

        String selZoneCluster = CliUtil.getZoneClusterName(wizardModel);
	helperData.put(Util.SEL_ZONE_CLUSTER, selZoneCluster);

	String[] baseClusterNodeList = (String[]) wizardModel.getWizardValue(
	    Util.BASE_CLUSTER_NODELIST);

        initWizardModel();
	RACModel racModel = RACInvokerCreator.getRACModel();

        for (int i = 0; i < baseClusterNodeList.length; i++) {
            String split_str[] = baseClusterNodeList[i].split(Util.COLON);
            String nodeEndPoint = split_str[0];

            HashMap map = racModel.discoverPossibilities(
		null, nodeEndPoint, propNames, helperData);

            // oracle home on nodeList[i]
            Vector tmpVec = (Vector) map.get(Util.CRS_HOME);

            if (tmpVec == null) {
                tmpVec = new Vector();
            }

            int size = tmpVec.size();

            for (int j = 0; j < size; j++) {
                String crshome = (String) tmpVec.elementAt(j);

                if (!vCrsHome.contains(crshome)) {
                    vCrsHome.add(crshome);
                }
            }
        } // finish discovery on all nodes

        HashMap custom = new HashMap();
        custom.put(enterKey, enterText);

        String optionsArr[] = (String[]) vCrsHome.toArray(new String[0]);

        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        String choice = no;

        while (choice.equals(no)) {
            TTYDisplay.printTitle(title);

            // get the default selection
            defaultCrsHome = (String) wizardModel.getWizardValue(
                    Util.SEL_CRS_HOME);

            // Get the crs home preference
            // see if an crs home is already set
            if ((optionsArr != null) && (optionsArr.length > 0)) {
                List crsHomeList = TTYDisplay.getScrollingOption(desc,
                        optionsArr, custom, defaultCrsHome, help_text, true);

                if (crsHomeList == null) {
                    this.cancelDirection = true;

                    return;
                }

                TTYDisplay.setNavEnabled(true);
                this.cancelDirection = false;

                String crsArr[] = (String[]) crsHomeList.toArray(new String[0]);
                selectedCrsHome = crsArr[0];
            } else {
                selectedCrsHome = enterText;
            }

            TTYDisplay.clear(1);

            if (selectedCrsHome.equals(enterText)) {
                selectedCrsHome = "";

                while (selectedCrsHome.trim().length() == 0) {
                    selectedCrsHome = TTYDisplay.promptForEntry(crshome_prompt);

                    if (selectedCrsHome.equals(back)) {
                        this.cancelDirection = true;

                        return;
                    }
                }
            }

            // validate the CRS HOME
            ErrorValue retVal = validateInput(selectedCrsHome, baseClusterNodeList,
		selZoneCluster);

            if (!retVal.getReturnValue().booleanValue()) {
                TTYDisplay.printInfo(retVal.getErrorString());

                String enterAgain = TTYDisplay.getConfirmation(enteragain_txt,
                        yes, no);

                if (enterAgain.equalsIgnoreCase(yes)) {
                    choice = no;

                    continue;
                }
            }

            choice = yes;

            // Store the Values in the WizardModel
            wizardModel.selectCurrWizardContext();
            wizardModel.setWizardValue(Util.SEL_CRS_HOME, selectedCrsHome);
        }
    }

    private ErrorValue validateInput(String oraHome, String baseClusterNodeList[],
	String selZoneCluster) {

        // validate oracle home on all nodes of a cluster
        String propName[] = { Util.CRS_HOME };
        HashMap userInput = new HashMap();
        userInput.put(Util.CRS_HOME, oraHome);

        ErrorValue err = new ErrorValue();
        err.setReturnVal(Boolean.TRUE);

        StringBuffer errStr = new StringBuffer();
        int err_count = 0;

	RACModel racModel = RACInvokerCreator.getRACModel();
	HashMap helperData = new HashMap();
	helperData.put(Util.SEL_ZONE_CLUSTER, selZoneCluster);
        for (int i = 0; i < baseClusterNodeList.length; i++) {
	    String[] splitStr = baseClusterNodeList[i].split(Util.COLON);
            ErrorValue errVal = racModel.validateInput(
		null, splitStr[0], propName, userInput, helperData);

            if (!errVal.getReturnValue().booleanValue()) {

                // error
                err_count++;
                errStr = new StringBuffer(errVal.getErrorString());
            }
        }

        if (err_count == baseClusterNodeList.length) {

            // failed on all nodes
            err.setReturnVal(Boolean.FALSE);
            err.setErrorString(errStr.toString());
        }

        return err;
    }

    /**
     * Initializes the Wizard Model by filling with the PropertyNames and
     * Discovered values
     */
    public void initWizardModel() {

        // Contact the RACModel and Get All Properties
        String propNames[] = (String[]) wizardModel.getWizardValue(
                Util.PROPERTY_NAMES);
        String propertyNames[] = null;
        HashMap discoveredMap = null;
	RACModel racModel = RACInvokerCreator.getRACModel();

	HashMap helperData = new HashMap();

        String selZoneCluster = CliUtil.getZoneClusterName(wizardModel);
	helperData.put(Util.SEL_ZONE_CLUSTER, selZoneCluster);

        String nodeEndPoint = (String) wizardModel.getWizardValue(
	    Util.NODE_TO_CONNECT);

        if (propNames == null) {
            if (racModel != null) {

                // Get the Discoverable Properties For Rac10G Wizard
                propertyNames = new String[] { Util.RAC_SERVER_PROXY_RTNAME };
                discoveredMap = racModel.discoverPossibilities(
		    null, nodeEndPoint, propertyNames, helperData);

                // Copy the Data to the WizardModel
                propertyNames = new String[discoveredMap.size()];

                Set entrySet = discoveredMap.entrySet();
                Iterator setIterator = entrySet.iterator();
                int i = 0;

                while (setIterator.hasNext()) {
                    Map.Entry entry = (Map.Entry) setIterator.next();
                    String key = (String) entry.getKey();
                    wizardModel.setWizardValue(key, entry.getValue());
                    propertyNames[i++] = key;
                }

                wizardModel.setWizardValue(Util.PROPERTY_NAMES, propertyNames);
            }
        }

        boolean crsFrameworkPresent = false;

        if (racModel != null) {
	    // check if there is a CRS framework resource configured in the given cluster
            propertyNames = new String[] { Util.CRS_FRAMEWORK_PRESENT };
            discoveredMap = racModel.discoverPossibilities(
		null, nodeEndPoint, propertyNames, helperData);
            crsFrameworkPresent = ((Boolean) discoveredMap.get(propertyNames[0])).
		booleanValue();

            wizardModel.setWizardValue(Util.CRS_FRAMEWORK_PRESENT,
                new Boolean(crsFrameworkPresent));

            if (crsFrameworkPresent) {
                wizardModel.setWizardValue(Util.CRS_FRAMEWORK_RSNAME,
                    (String) discoveredMap.get(Util.CRS_FRAMEWORK_RSNAME));
            }
        }
    }
}
