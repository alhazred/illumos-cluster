/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)SapWebasWizardCreator.java 1.17     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.sapwebas;

// J2SE
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Vector;

// JMX
import javax.management.remote.JMXConnector;

// CMAS
import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.sapwebas.SapWebasMBean;
import com.sun.cluster.agent.dataservices.utils.Util;

// Wizards Common
import com.sun.cluster.common.TrustedMBeanModel;
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardComposite;
import com.sun.cluster.dswizards.clisdk.core.WizardCreator;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.CliUtil;
import com.sun.cluster.dswizards.hastorageplus.HASPCreateFileSystemPanel;
import com.sun.cluster.dswizards.hastorageplus.HASPCreateFileSystemReviewPanel;
import com.sun.cluster.dswizards.hastorageplus.HASPDeviceGroupSelectionPanel;
import com.sun.cluster.dswizards.hastorageplus.HASPFileSystemSelectionPanel;
import com.sun.cluster.dswizards.hastorageplus.HASPLocationPanel;
import com.sun.cluster.dswizards.hastorageplus.HASPWizardConstants;
import com.sun.cluster.dswizards.logicalhost.HostnameEntryPanel;
import com.sun.cluster.dswizards.logicalhost.IPMPGroupEntryPanel;
import com.sun.cluster.dswizards.logicalhost.LogicalHostWizardConstants;


/**
 * This class holds the Wizard Panel Tree information for SapWebas Wizard. This
 * class creates the SapWebas wizard from the State file and WizardFlow File.
 */
public class SapWebasWizardCreator extends WizardCreator {


    private static final String FLOW_XML =
        "/usr/cluster/lib/ds/sapwebas/SapWebasWizardFlow.xml";
    private static final String STATE_FILE =
        "/opt/cluster/lib/ds/history/SapWebasState";


    /**
     * Default Constructor Creates Wizard from the State File. The Wizard
     * is intialized to a particular state using the State file. The Wizard flow
     * is governed by the Flow XML.
     */
    public SapWebasWizardCreator() {
        super(STATE_FILE, FLOW_XML);
    }

    /**
     * Creates the actual Panel Tree Structure for the Wizard
     */
    protected void createClientTree() {

        // Get the wizard root
        WizardComposite wizardRoot = getRoot();

        // Create the Individual Panels of the Wizard

        // Each Panel is associated with a Name, the wizardModel from
        // the WizardCreator and the WizardFlow Structure.
        IntroductionPanel introPanel = new IntroductionPanel(
                SapWebasConstants.PANEL_0, wizardModel, wizardFlow, wizardi18n);

        SidPanel sidPanel = new SidPanel(SapWebasConstants.PANEL_1, wizardModel,
                wizardFlow, wizardi18n);

        ComponentSelectionPanel compPanel = new ComponentSelectionPanel(
                SapWebasConstants.PANEL_2, wizardModel, wizardFlow, wizardi18n);

        CsNodeSelectionPanel nodePanel = new CsNodeSelectionPanel(
                SapWebasConstants.NODE_SELECT, wizardModel, wizardFlow,
                wizardi18n);

        CentralServicesInputPanel centralServicePanel =
            new CentralServicesInputPanel(SapWebasConstants.PANEL_3,
                wizardModel, wizardFlow, wizardi18n);

        CentralServicesDetailsPanel centralServiceDetailsPanel =
            new CentralServicesDetailsPanel(SapWebasConstants.PANEL_4,
                wizardModel, wizardFlow, wizardi18n);

        ChooseHASPanel storageSelect = new ChooseHASPanel(
                SapWebasConstants.STORAGE_SELECT, wizardModel, wizardFlow,
                wizardi18n);

        HASPLocationPanel haspLocationPanel = new HASPLocationPanel(
                SapWebasConstants.STORAGE_SELECT + WizardFlow.HYPHEN +
                HASPWizardConstants.PANEL_2, wizardModel, wizardFlow,
                wizardi18n);

        HASPFileSystemSelectionPanel haspFileSystemSelectionPanel =
            new HASPFileSystemSelectionPanel(SapWebasConstants.STORAGE_SELECT +
                WizardFlow.HYPHEN + HASPWizardConstants.PANEL_3, wizardModel,
                wizardFlow, wizardi18n);

        HASPCreateFileSystemPanel haspCreateFileSystemPanel =
            new HASPCreateFileSystemPanel(SapWebasConstants.STORAGE_SELECT +
                WizardFlow.HYPHEN + HASPWizardConstants.PANEL_4, wizardModel,
                wizardFlow, wizardi18n);

        HASPCreateFileSystemReviewPanel haspCreateFileSystemReviewPanel =
            new HASPCreateFileSystemReviewPanel(
                SapWebasConstants.STORAGE_SELECT + WizardFlow.HYPHEN +
                HASPWizardConstants.PANEL_5, wizardModel, wizardFlow,
                wizardi18n);

        HASPDeviceGroupSelectionPanel haspDeviceGroupSelectionPanel =
            new HASPDeviceGroupSelectionPanel(SapWebasConstants.STORAGE_SELECT +
                WizardFlow.HYPHEN + HASPWizardConstants.PANEL_6, wizardModel,
                wizardFlow, wizardi18n);

        ChooseLHPanel hostSelect = new ChooseLHPanel(
                SapWebasConstants.HOST_SELECT, wizardModel, wizardFlow,
                wizardi18n);

        HostnameEntryPanel hostnameEntryPanel = new HostnameEntryPanel(
                SapWebasConstants.HOST_SELECT + WizardFlow.HYPHEN +
                LogicalHostWizardConstants.HOSTNAMEENTRY, wizardModel,
                wizardFlow, wizardi18n);

        IPMPGroupEntryPanel ipmpGroupEntryPanel = new IPMPGroupEntryPanel(
                SapWebasConstants.HOST_SELECT + WizardFlow.HYPHEN +
                LogicalHostWizardConstants.IPMPGROUPENTRY, wizardModel,
                wizardFlow, wizardi18n);

        SelectDBPanel selectDBPanel = new SelectDBPanel(
                SapWebasConstants.DB_SELECT, wizardModel, wizardFlow,
                wizardi18n);

        ReplicaPanel replicaPanel = new ReplicaPanel(SapWebasConstants.PANEL_5,
                wizardModel, wizardFlow, wizardi18n);

        RepChooseHASPanel repStorageSelect = new RepChooseHASPanel(
                SapWebasConstants.REP_STORAGE_SELECT, wizardModel, wizardFlow,
                wizardi18n);

        HASPLocationPanel rephaspLocationPanel = new HASPLocationPanel(
                SapWebasConstants.REP_STORAGE_SELECT + WizardFlow.HYPHEN +
                HASPWizardConstants.PANEL_2, wizardModel, wizardFlow,
                wizardi18n);

        HASPFileSystemSelectionPanel rephaspFileSystemSelectionPanel =
            new HASPFileSystemSelectionPanel(
                SapWebasConstants.REP_STORAGE_SELECT + WizardFlow.HYPHEN +
                HASPWizardConstants.PANEL_3, wizardModel, wizardFlow,
                wizardi18n);

        HASPCreateFileSystemPanel rephaspCreateFileSystemPanel =
            new HASPCreateFileSystemPanel(SapWebasConstants.REP_STORAGE_SELECT +
                WizardFlow.HYPHEN + HASPWizardConstants.PANEL_4, wizardModel,
                wizardFlow, wizardi18n);

        HASPCreateFileSystemReviewPanel rephaspCreateFileSystemReviewPanel =
            new HASPCreateFileSystemReviewPanel(
                SapWebasConstants.REP_STORAGE_SELECT + WizardFlow.HYPHEN +
                HASPWizardConstants.PANEL_5, wizardModel, wizardFlow,
                wizardi18n);

        HASPDeviceGroupSelectionPanel rephaspDeviceGroupSelectionPanel =
            new HASPDeviceGroupSelectionPanel(
                SapWebasConstants.REP_STORAGE_SELECT + WizardFlow.HYPHEN +
                HASPWizardConstants.PANEL_6, wizardModel, wizardFlow,
                wizardi18n);

        RepChooseLHPanel rephostSelect = new RepChooseLHPanel(
                SapWebasConstants.REP_HOST_SELECT, wizardModel, wizardFlow,
                wizardi18n);

        HostnameEntryPanel rephostnameEntryPanel = new HostnameEntryPanel(
                SapWebasConstants.REP_HOST_SELECT + WizardFlow.HYPHEN +
                LogicalHostWizardConstants.HOSTNAMEENTRY, wizardModel,
                wizardFlow, wizardi18n);

        IPMPGroupEntryPanel repipmpGroupEntryPanel = new IPMPGroupEntryPanel(
                SapWebasConstants.REP_HOST_SELECT + WizardFlow.HYPHEN +
                LogicalHostWizardConstants.IPMPGROUPENTRY, wizardModel,
                wizardFlow, wizardi18n);

        SapReviewPanel sapReviewPanel = new SapReviewPanel(
                SapWebasConstants.PANEL_6, wizardModel, wizardFlow, wizardi18n);

        SapSummaryPanel sapSummaryPanel = new SapSummaryPanel(
                SapWebasConstants.SUMMARY_PANEL, wizardModel, wizardFlow,
                wizardi18n);

        SapResultsPanel sapResultsPanel = new SapResultsPanel(
                SapWebasConstants.RESULTS_PANEL, wizardModel, wizardFlow,
                wizardi18n);

        // Add the Panels to the Root
        wizardRoot.addChild(introPanel);
        wizardRoot.addChild(nodePanel);
        wizardRoot.addChild(sidPanel);
        wizardRoot.addChild(compPanel);
        wizardRoot.addChild(centralServicePanel);
        wizardRoot.addChild(centralServiceDetailsPanel);
        wizardRoot.addChild(storageSelect);
        wizardRoot.addChild(haspLocationPanel);
        wizardRoot.addChild(haspFileSystemSelectionPanel);
        wizardRoot.addChild(haspCreateFileSystemPanel);
        wizardRoot.addChild(haspCreateFileSystemReviewPanel);
        wizardRoot.addChild(haspDeviceGroupSelectionPanel);
        wizardRoot.addChild(hostSelect);
        wizardRoot.addChild(hostnameEntryPanel);
        wizardRoot.addChild(ipmpGroupEntryPanel);
        wizardRoot.addChild(replicaPanel);
        wizardRoot.addChild(repStorageSelect);
        wizardRoot.addChild(rephaspLocationPanel);
        wizardRoot.addChild(rephaspFileSystemSelectionPanel);
        wizardRoot.addChild(rephaspCreateFileSystemPanel);
        wizardRoot.addChild(rephaspCreateFileSystemReviewPanel);
        wizardRoot.addChild(rephaspDeviceGroupSelectionPanel);
        wizardRoot.addChild(rephostSelect);
        wizardRoot.addChild(rephostnameEntryPanel);
        wizardRoot.addChild(repipmpGroupEntryPanel);
        wizardRoot.addChild(selectDBPanel);
        wizardRoot.addChild(sapReviewPanel);
        wizardRoot.addChild(sapSummaryPanel);
        wizardRoot.addChild(sapResultsPanel);
    }

    public static SapWebasMBean getMBean(JMXConnector connector) {

        SapWebasMBean mBean = null;

        try {
            TTYDisplay.initialize();
            mBean = (SapWebasMBean) TrustedMBeanModel.getMBeanProxy(Util.DOMAIN,
                    Util.getClusterEndpoint(), SapWebasMBean.class, null,
                    false);
        } catch (Exception e) {

            // Report Error that the user has to install
            // The Dataservice Package or upgrade it
            String errorMsg = pkgErrorString;
            MessageFormat msgFmt = new MessageFormat(errorMsg);
            errorMsg = msgFmt.format(
                    new String[] { SapWebasConstants.SAP_PKG_NAME });
            TTYDisplay.printError(errorMsg);
            TTYDisplay.promptForEntry(wizardExitPrompt);
            System.exit(1);
        }

        return mBean;
    }

    public static String convertArraytoString(String xStrArray[]) {
        StringBuffer strBuf = new StringBuffer();

        if ((xStrArray != null) && (xStrArray.length > 0)) {

            for (int i = 0; i < xStrArray.length; i++) {
                strBuf.append(xStrArray[i]);

                if (i != (xStrArray.length - 1)) {
                    strBuf.append(Util.COMMA);
                }
            }
        }

        return strBuf.toString();
    }

    public static Vector splitArraytoVector(String xStrArray[]) {
        Vector table = new Vector();

        if ((xStrArray != null) && (xStrArray.length > 0)) {

            for (int i = 0; i < xStrArray.length; i++) {
                String rs = xStrArray[i];

                if (rs != null) {
                    String splitStr[] = rs.split(Util.COLON);

                    if (splitStr.length == 2) {
                        table.add(splitStr[0]);
                        table.add(splitStr[1]);
                    } else if (splitStr.length == 1) {
                        table.add(splitStr[0]);
                        table.add("");
                    }
                }
            }
        }

        return table;
    }

    public static String join(String stringArray[], String delimiter) {
        String joinedString = "";

        if (stringArray == null)
            return joinedString;

        for (int i = 0; i < stringArray.length; i++) {

            if (i == 0) {
                joinedString = stringArray[i];
            } else {
                joinedString = joinedString + delimiter + stringArray[i];
            }
        }

        return joinedString;
    }


    public static String[] discoverOptions(CliDSConfigWizardModel xWizModel,
        String discKey, String modelKey, Object helperData) {
        SapWebasMBean mBean;
        String optionsArr[] = (String[]) xWizModel.getWizardValue(modelKey);

        if (optionsArr == null) {
            String nodeEndPoint = (String) xWizModel.getWizardValue(
                    Util.NODE_TO_CONNECT);
            JMXConnector connector = getConnection(nodeEndPoint, xWizModel);
            mBean = SapWebasWizardCreator.getMBean(connector);

            HashMap values = mBean.discoverPossibilities(
                    new String[] { discKey }, helperData);
            optionsArr = (String[]) values.get(discKey);
            xWizModel.setWizardValue(modelKey, optionsArr);
        }

        return optionsArr;
    }

    public static String discoverOption(CliDSConfigWizardModel xWizModel,
        String discKey, String modelKey, Object helperData) {
        SapWebasMBean mBean;
        String option = (String) xWizModel.getWizardValue(modelKey);

        if (option == null) {
            String nodeEndPoint = (String) xWizModel.getWizardValue(
                    Util.NODE_TO_CONNECT);
            JMXConnector connector = getConnection(nodeEndPoint, xWizModel);
            mBean = SapWebasWizardCreator.getMBean(connector);

            HashMap values = mBean.discoverPossibilities(
                    new String[] { discKey }, helperData);
            option = (String) values.get(discKey);
            xWizModel.setWizardValue(modelKey, option);
        }

        return option;
    }


    public static ErrorValue validateData(String discKey, String xData,
        CliDSConfigWizardModel wizardModel) {
        SapWebasMBean mBean;
        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);
        JMXConnector connector = getConnection(nodeEndPoint, wizardModel);
        mBean = SapWebasWizardCreator.getMBean(connector);

        HashMap data = new HashMap();
        data.put(discKey, xData);

        return (mBean.validateInput(new String[] { discKey }, data, null));
    }


    public static JMXConnector getConnection(String agentLocation,
        CliDSConfigWizardModel wizardModel) {

        String split_str[] = agentLocation.split(Util.COLON);
        agentLocation = split_str[0];

        JMXConnector connector = (JMXConnector) wizardModel.getWizardValue(
                CliUtil.JMX_CONNECTION_PREFIX + agentLocation);

        if (connector == null) {

            try {
                connector = TrustedMBeanModel.getWellKnownConnector(
                        agentLocation);
            } catch (Exception ex) {
                TTYDisplay.printError("Failed to get connection to node " +
                    agentLocation);
                System.exit(1);
            }

            setConnection(agentLocation, connector, wizardModel);
        }

        return connector;
    }

    public static void setConnection(String agentLocation,
        JMXConnector connector, CliDSConfigWizardModel wizardModel) {
        wizardModel.setWizardValue(CliUtil.JMX_CONNECTION_PREFIX + agentLocation,
            connector);
    }


    /**
     * Main function of the Wizards
     */
    public static void main(String args[]) {

        SapWebasWizardCreator lh = new SapWebasWizardCreator();
    }
}
