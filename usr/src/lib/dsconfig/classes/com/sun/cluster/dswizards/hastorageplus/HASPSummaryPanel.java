/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)HASPSummaryPanel.java 1.27     09/01/16 SMI"
 */


package com.sun.cluster.dswizards.hastorageplus;

import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.hastorageplus.HAStoragePlusMBean;
import com.sun.cluster.agent.dataservices.utils.FileAccessorWrapper;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.dataservices.utils.VfsStruct;
import com.sun.cluster.agent.rgm.property.ResourceProperty;
import com.sun.cluster.agent.rgm.property.ResourcePropertyStringArray;
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.ResourcePropertyUtil;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;

import java.io.IOException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.management.MBeanServerConnection;
import javax.management.remote.JMXConnector;

public class HASPSummaryPanel extends WizardLeaf {

    private static String NAME = "NAME";
    private static String VALUE = "VALUE";
    private static String DESCRIPTION = "DESCRIPTION";
    private static String COLON_DESC = "COLON_DESC";


    private String panelName; // Panel Name
    private WizardI18N wizardi18n; // Message Bundle
    private CliDSConfigWizardModel wizardModel; // WizardModel

    private JMXConnector localConnection = null;

    /**
     * Creates a new instance of HASPSummaryPanel
     */
    public HASPSummaryPanel() {
        super();
    }

    /**
     * Creates a HASPSummaryPanel Panel with the given name and manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public HASPSummaryPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    public HASPSummaryPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;
        this.wizardi18n = wizardi18n;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.
     *		WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        String title = wizardi18n.getString("hasp.summarypanel.title");
        String description = wizardi18n.getString("hasp.summarypanel.desc1");
        String back = wizardi18n.getString("ttydisplay.back.char");
        String yes = wizardi18n.getString("cliwizards.yes");
        String no = wizardi18n.getString("cliwizards.no");
        String rsname_txt = wizardi18n.getString(
                "hasp.summarypanel.resource.name");
        String rgname_txt = wizardi18n.getString("hasp.summarypanel.rg.name");
        String nodelist_txt = wizardi18n.getString(
                "hasp.summarypanel.nodelist.name");
        String fs_txt = wizardi18n.getString(
                "hasp.summarypanel.filesytem.name");
        String globaldev_txt = wizardi18n.getString(
                "hasp.summarypanel.devicegroup.name");
        String table_title = wizardi18n.getString("hasp.summarypanel.desc2");
        String heading1 = wizardi18n.getString("hasp.summarypanel.heading1");
        String heading2 = wizardi18n.getString("hasp.summarypanel.heading2");
        String c_char = wizardi18n.getString("ttydisplay.c.char");
        String create_txt = wizardi18n.getString(
                "hasp.summarypanel.menu.create");
        String q_char = wizardi18n.getString("ttydisplay.q.char");
        String quit_txt = wizardi18n.getString("ttydisplay.menu.quit");
        String panel_desc = wizardi18n.getString(
                "hasp.summarypanel.description");
        String rs_desc = wizardi18n.getString(
                "hasp.summarypanel.resource.desc");
        String rs_colon = wizardi18n.getString(
                "hasp.summarypanel.resource.name.colon");
        String rg_desc = wizardi18n.getString("hasp.summarypanel.rg.desc");
        String rg_colon = wizardi18n.getString(
                "hasp.summarypanel.rg.name.colon");
        String nodelist_desc = wizardi18n.getString(
                "hasp.summarypanel.nodelist.desc");
        String nodelist_colon = wizardi18n.getString(
                "hasp.summarypanel.nodelist.name.colon");
        String fs_desc = wizardi18n.getString(
                "hasp.summarypanel.filesystem.desc");
        String fs_colon = wizardi18n.getString(
                "hasp.summarypanel.filesytem.name.colon");
        String globaldev_desc = wizardi18n.getString(
                "hasp.summarypanel.devicegroup.desc");
        String globaldev_colon = wizardi18n.getString(
                "hasp.summarypanel.devicegroup.name.colon");
        String exit_txt = wizardi18n.getString("cliwizards.confirmExit");
        String help_text = wizardi18n.getString("hasp.summarypanel.cli.help");
        String continue_txt = wizardi18n.getString("cliwizards.continue");

        // Title
        TTYDisplay.printTitle(title);

        // Review panel table
        Vector summaryPanel = new Vector();

        wizardModel.selectCurrWizardContext();


        HashMap tableMap = new HashMap();
        HashMap selMap = null;
        int row_count = 1;

        // First Row - Resource Name
        String rsName = (String) wizardModel.getWizardValue(Util.RS_NAME);
        summaryPanel.add(rsname_txt);
        summaryPanel.add(rsName);
        selMap = new HashMap();
        selMap.put(NAME, rsname_txt);
        selMap.put(VALUE, rsName);
        selMap.put(DESCRIPTION, rs_desc);
        selMap.put(COLON_DESC, rs_colon);
        tableMap.put(Integer.toString(row_count++), selMap);

        // Second Row - RG Name
        String rgName = (String) wizardModel.getWizardValue(Util.RG_NAME);
        summaryPanel.add(rgname_txt);
        summaryPanel.add(rgName);
        selMap = new HashMap();
        selMap.put(NAME, rgname_txt);
        selMap.put(VALUE, rgName);
        selMap.put(DESCRIPTION, rg_desc);
        selMap.put(COLON_DESC, rg_colon);
        tableMap.put(Integer.toString(row_count++), selMap);

        // Third row - Node list
        String nodeList[] = (String[]) wizardModel.getWizardValue(
                Util.RG_NODELIST);
        StringBuffer nodeNames = new StringBuffer();

        for (int i = 0; i < nodeList.length; i++) {
            nodeNames.append(nodeList[i]);

            if (i < (nodeList.length - 1))
                nodeNames.append(" ");
        }

        summaryPanel.add(nodelist_txt);
        summaryPanel.add(nodeNames.toString());
        selMap = new HashMap();
        selMap.put(NAME, nodelist_txt);
        selMap.put(VALUE, nodeNames.toString());
        selMap.put(DESCRIPTION, nodelist_desc);
        selMap.put(COLON_DESC, nodelist_colon);
        tableMap.put(Integer.toString(row_count++), selMap);


        // Fourth row - Filesystem mount points
        Boolean sqfsSelected = (Boolean) wizardModel.getWizardValue(
            HASPWizardConstants.SQFS_SELECTED);

        String fsMountPoints[] = (String[]) wizardModel.getWizardValue(
                HASPWizardConstants.HASP_FILESYSTEMS_SELECTED);
        StringBuffer mountpoints = new StringBuffer();

        if ((fsMountPoints != null) && (fsMountPoints.length > 0)) {

            for (int i = 0; i < fsMountPoints.length; i++) {
                mountpoints.append(fsMountPoints[i]);

                if (i < (fsMountPoints.length - 1))
                    mountpoints.append(" ");
            }

            selMap = new HashMap();

            summaryPanel.add(fs_txt);
            selMap.put(NAME, fs_txt);
            selMap.put(DESCRIPTION, fs_desc);
            selMap.put(COLON_DESC, fs_colon);

            summaryPanel.add(mountpoints.toString());
            selMap.put(VALUE, mountpoints.toString());
            tableMap.put(Integer.toString(row_count++), selMap);
        }

        // Fifth row - Global Device paths
        String globalDevicePaths[] = (String[]) wizardModel.getWizardValue(
                HASPWizardConstants.HASP_DEVICES_SELECTED);
        StringBuffer devpaths = new StringBuffer();

        if ((globalDevicePaths != null) && (globalDevicePaths.length > 0)) {

            for (int i = 0; i < globalDevicePaths.length; i++) {
                devpaths.append(globalDevicePaths[i]);

                if (i < (globalDevicePaths.length - 1))
                    devpaths.append(" ");
            }

            summaryPanel.add(globaldev_txt);
            summaryPanel.add(devpaths.toString());

            selMap = new HashMap();
            selMap.put(NAME, globaldev_txt);
            selMap.put(VALUE, devpaths.toString());
            selMap.put(DESCRIPTION, globaldev_desc);
            selMap.put(COLON_DESC, globaldev_colon);
            tableMap.put(Integer.toString(row_count++), selMap);
        }

        TTYDisplay.pageText(description);
        TTYDisplay.clear(1);

        String subheadings[] = { heading1, heading2 };

        HashMap customTags = new HashMap();
        customTags.put(c_char, create_txt);
        customTags.put(q_char, quit_txt);

        boolean createconfig = false;

        while (!createconfig) {
            TTYDisplay.setNavEnabled(true);
            this.cancelDirection = false;

            String option = TTYDisplay.create2DTable(table_title, subheadings,
                    summaryPanel, customTags, help_text);

            if (option.equals(back)) {
                this.cancelDirection = true;

                return;
            }

            TTYDisplay.clear(3);
            TTYDisplay.setNavEnabled(false);

            if (!option.equals(c_char) && !option.equals(q_char)) {
                selMap = (HashMap) tableMap.get(option);

                String name = (String) selMap.get(NAME);
                String value = (String) selMap.get(VALUE);
                String desc = (String) selMap.get(DESCRIPTION);
                String colon_desc = (String) selMap.get(COLON_DESC);

                TTYDisplay.pageText(panel_desc + desc);
                TTYDisplay.clear(1);
                TTYDisplay.pageText(colon_desc + value);
                TTYDisplay.clear(2);
                TTYDisplay.promptForEntry(continue_txt);
            } else if (option.equals(c_char)) {

                // Create the Command Generation Structure
                Map confMap = new HashMap();

                // ResourceGroup Structure

                // ResourceGroup Name
                confMap.put(Util.SERVER_GROUP + Util.NAME_TAG, rgName);

                // ResourceGroup Properties
                Map rgPropMap = new HashMap();
                rgPropMap.put(Util.NODELIST,
                    nodeNames.toString().replace(' ', ','));
                confMap.put(Util.SERVER_GROUP + Util.RGPROP_TAG, rgPropMap);

                // Resource Structure

                // Resource Name
                String RES_ID = Util.HASP_RES_ID;
                String FILESYSTEM = Util.HASP_ALL_FILESYSTEMS;
                if (sqfsSelected != null) {
                    // shared QFS filesystem is selected
                    RES_ID = Util.QFS_RES_ID;
                    FILESYSTEM = Util.QFS_FILESYSTEM;
                }

                // Resource Properties
                List rsExtProp = new ArrayList();
                List rsSysProp = new ArrayList();

                if ((fsMountPoints != null) && (fsMountPoints.length > 0)) {
                    ResourceProperty rp = new ResourcePropertyStringArray(
                            FILESYSTEM, fsMountPoints);
                    rsExtProp.add(rp);

                    // Get the map to add for /etc/vfstab and update all nodes
                    HashMap fsHashMap = (HashMap) wizardModel.getWizardValue(
                            Util.HASP_ALL_FILESYSTEMS);
                    HashMap map_to_add = (HashMap) fsHashMap.get(
                            Util.VFSTAB_MAP_TO_ADD);

                    // Get MBean Server connections on all nodes
                    localConnection = getConnection(Util.getClusterEndpoint());

                    JMXConnector connectors[] =
                        new JMXConnector[nodeList.length];
                    MBeanServerConnection mbsConnections[] =
                        new MBeanServerConnection[nodeList.length];

                    for (int i = 0; i < nodeList.length; i++) {
                        String split_str[] = nodeList[i].split(Util.COLON);
                        String nodeEndPoint = Util.getNodeEndpoint(
                                localConnection, split_str[0]);
                        connectors[i] = getConnection(nodeEndPoint);

                        try {
                            mbsConnections[i] = connectors[i]
                                .getMBeanServerConnection();
                        } catch (IOException ioe) {
                        }
                    }

                    FileAccessorWrapper fileAccessor = new FileAccessorWrapper(
                            Util.VFSTAB_FILE, mbsConnections);

                    for (int i = 0; i < nodeList.length; i++) {
                        String split_str[] = nodeList[i].split(Util.COLON);
                        ArrayList nodeEntry = (ArrayList) map_to_add.get(
                                split_str[0]);

                        if (nodeEntry != null) {
                            fileAccessor.appendLine(nodeEntry,
                                mbsConnections[i]);
                        }
                    }
                }

                if (sqfsSelected == null && (globalDevicePaths != null) &&
                        (globalDevicePaths.length > 0)) {
                    ResourceProperty rp = new ResourcePropertyStringArray(
                            Util.HASP_ALL_DEVICES, globalDevicePaths);
                    rsExtProp.add(rp);
                }

                confMap.put(RES_ID + Util.NAME_TAG, rsName);
                confMap.put(RES_ID + Util.SYSPROP_TAG, rsSysProp);
                confMap.put(RES_ID + Util.EXTPROP_TAG, rsExtProp);

                generateCommands(confMap);
                createconfig = true;
            } else if (option.equals(q_char)) {
                String exitOpt = TTYDisplay.getConfirmation(exit_txt, yes, no);

                if (exitOpt.equalsIgnoreCase(yes)) {
                    System.exit(0);
                }
            }

            TTYDisplay.clear(2);
        }
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#isDisplayComplete
     */
    public boolean isDisplayComplete() {
        return true;
    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }

    /**
     * Generates the commands by invoking the command generator
     */
    private void generateCommands(Map confMap) {
        List commandList = null;
        Thread t = null;
        HAStoragePlusMBean mBean = null;

        try {
            TTYDisplay.clear(1);
            t = TTYDisplay.busy(wizardi18n.getString(
                        "cliwizards.executingMessage"));
            t.start();
            localConnection = getConnection(Util.getClusterEndpoint());
            mBean = HASPWizardCreator.getHAStoragePlusMBean(localConnection);
            commandList = mBean.generateCommands(confMap);
        } catch (CommandExecutionException cee) {
            wizardModel.setWizardValue(Util.EXITSTATUS, cee);
            commandList = mBean.getCommandList();
        } finally {
            wizardModel.setWizardValue(Util.CMDLIST, commandList);

            try {
                t.interrupt();
                t.join();
            } catch (Exception exe) {
            }
        }
    }
}
