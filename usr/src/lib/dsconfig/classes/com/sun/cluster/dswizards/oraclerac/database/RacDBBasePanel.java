/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)RacDBBasePanel.java	1.15	09/03/11 SMI"
 */

package com.sun.cluster.dswizards.oraclerac.database;

// CMASS
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.dataservices.common.ErrorValue;

// CLI Wizard SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliUtil;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.oraclerac.invoker.RACInvokerCreator;

// Model
import com.sun.cluster.model.RACModel;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

/**
 * Introduction page to the RAC Wizards
 */
public class RacDBBasePanel extends WizardLeaf implements RacDBWizardConstants {

    protected String panelName; // Panel Name
    protected CliDSConfigWizardModel wizardModel; // WizardModel
    protected WizardI18N wizardi18n; // Message Bundle
    protected String continue_txt;
    protected String confirm_txt;
    protected String enteragain_txt;
    protected String exit_txt;
    protected String title;
    protected String desc;
    protected String yes;
    protected String no;
    protected String back;
    protected String fwd_arrow;
    protected String enterKey;
    protected String enterText;
    protected String achar;
    protected String all;
    protected String rchar;
    protected String refresh;
    protected String dchar;
    protected String done;
    protected String c_char;
    protected String create_config;
    protected String qchar;
    protected String quit;
    protected String schar;
    protected String execution_msg;
    protected String execution_error_msg;
    protected String statefile_error;

    /**
     * Creates a new instance of RacDBBasePanel
     */
    public RacDBBasePanel() {
        super();
    }

    /**
     * Creates a RacDBBasePanel with the given name and tree manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public RacDBBasePanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates a RacDBBasePanel with the given name and associates it with
     * the WizardModel and WizardFlow
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     */
    public RacDBBasePanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;

        // Get the I18N instance
        this.wizardi18n = wizardi18n;
        initStrings();
    }

    private void initStrings() {
        continue_txt = wizardi18n.getString("cliwizards.continue");
        yes = wizardi18n.getString("cliwizards.yes");
        no = wizardi18n.getString("cliwizards.no");
        back = wizardi18n.getString("ttydisplay.back.char");
        confirm_txt = wizardi18n.getString("cliwizards.confirmMessage");
        enteragain_txt = wizardi18n.getString("cliwizards.enterAgain");
        enterKey = wizardi18n.getString("custom.menu.enteryourown.key");
        enterText = wizardi18n.getString("custom.menu.enteryourown.keyText");
        achar = wizardi18n.getString("ttydisplay.a.char");
        all = wizardi18n.getString("ttydisplay.menu.all");
        rchar = wizardi18n.getString("ttydisplay.r.char");
        refresh = wizardi18n.getString("ttydisplay.menu.refresh");
        dchar = (String) wizardi18n.getString("ttydisplay.d.char");
        done = wizardi18n.getString("ttydisplay.menu.done");
        c_char = wizardi18n.getString("ttydisplay.c.char");
        create_config = wizardi18n.getString("ttydisplay.menu.create.config");
        qchar = wizardi18n.getString("ttydisplay.q.char");
        quit = wizardi18n.getString("ttydisplay.menu.quit");
        schar = wizardi18n.getString("ttydisplay.s.char");
        exit_txt = wizardi18n.getString("cliwizards.confirmExit");
        fwd_arrow = wizardi18n.getString("ttydisplay.fwd.arrow");
        execution_msg = wizardi18n.getString("cliwizards.executionMessage");
        execution_error_msg = wizardi18n.getString(
                "cliwizards.executionErrorMessage");
        statefile_error = wizardi18n.getString("dswizards.stateFile.error");
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {
    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#isDisplayComplete
     */
    public boolean isDisplayComplete() {
        return true;
    }

    protected String getDBVersion(String selectedOraHome) {
        String ora_ver = "";

        // get racModel 
        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);
        RACModel racModel = RACInvokerCreator.getRACModel();
        String propertyNames[] = new String[] { Util.ORA_VER };

        // Pass ORACLE_HOME as helper data
        HashMap helperData = new HashMap();
        helperData.put(Util.ORACLE_HOME, selectedOraHome);

        String selZoneCluster = CliUtil.getZoneClusterName(wizardModel);
	helperData.put(Util.SEL_ZONE_CLUSTER, selZoneCluster);

        HashMap discoveredMap = racModel.discoverPossibilities(
	    null, nodeEndPoint, propertyNames, helperData);
        Object tmpObj = discoveredMap.get(Util.ORA_VER);

        if (tmpObj != null) {
            ErrorValue retVal = (ErrorValue) tmpObj;

            if (retVal.getReturnValue().booleanValue()) {
                String retString = retVal.getErrorString();

                if (retString.startsWith(Util.PRE_102)) {
                    ora_ver = new String(Util.NINE);
                } else if (retString.startsWith(Util.POST_102)) {
                    ora_ver = new String(Util.TEN);
                }
            }
        }

        return ora_ver;
    }
}
