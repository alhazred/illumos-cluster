/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)NFSWizardCreator.java 1.15     08/05/20 SMI"
 */

package com.sun.cluster.dswizards.nfs;

// Cacao

import com.sun.cacao.agent.JmxClient;

// CMASS
import com.sun.cluster.agent.dataservices.nfs.NFSMBean;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.common.TrustedMBeanModel;


// CLI SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardComposite;
import com.sun.cluster.dswizards.clisdk.core.WizardCreator;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.hastorageplus.HASPCreateFileSystemPanel;
import com.sun.cluster.dswizards.hastorageplus.HASPCreateFileSystemReviewPanel;
import com.sun.cluster.dswizards.hastorageplus.HASPFileSystemSelectionPanel;
import com.sun.cluster.dswizards.hastorageplus.HASPWizardConstants;

// CLI Wizards
import com.sun.cluster.dswizards.logicalhost.HostnameEntryPanel;
import com.sun.cluster.dswizards.logicalhost.IPMPGroupEntryPanel;
import com.sun.cluster.dswizards.logicalhost.LogicalHostWizardConstants;

import java.io.File;

import java.text.MessageFormat;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

// JMX
import javax.management.remote.JMXConnector;


/**
 * This class holds the Wizard Panel Tree information for NFS Wizard. This class
 * creates the NFS wizard from the State file and WizardFlow File.
 */
public class NFSWizardCreator extends WizardCreator {


    /**
     * Default Constructor Creates  NFS Wizard from the State File. The Wizard
     * is intialized to a particular state using the State file. The Wizard flow
     * is governed by the Flow XML.
     */
    public NFSWizardCreator() {
        super(NFSWizardConstants.NFS_WIZARD_STATE_FILE,
            NFSWizardConstants.NFS_WIZARD_FLOW_XML);
    }

    /**
     * Creates the actual Panel Tree Structure for the NFS Wizard
     */
    protected void createClientTree() {


        // Get the wizard root
        WizardComposite nfsWizardRoot = getRoot();

        // Create the Individual Panels of the Wizard

        // Each Panel is associated with a Name, the wizardModel from
        // the WizardCreator and the WizardFlow Structure.

        NFSIntroPanel nfsIntroPanel = new NFSIntroPanel(
                NFSWizardConstants.PANEL_0, wizardModel, wizardFlow,
                wizardi18n);
        NFSNodeSelectionPanel nfsNodeSelectionPanel = new NFSNodeSelectionPanel(
                NFSWizardConstants.PANEL_1, wizardModel, wizardFlow,
                wizardi18n);
        NFSLhResNameEntryPanel nfsLhResNameEntryPanel =
            new NFSLhResNameEntryPanel(NFSWizardConstants.PANEL_2, wizardModel,
                wizardFlow, wizardi18n);
        NFSHaspResNameEntryPanel nfsHaspResNameEntryPanel =
            new NFSHaspResNameEntryPanel(NFSWizardConstants.PANEL_3,
                wizardModel, wizardFlow, wizardi18n);

        NFSPathPrefixEntryPanel nfsPathPrefixEntryPanel =
            new NFSPathPrefixEntryPanel(NFSWizardConstants.PANEL_4, wizardModel,
                wizardFlow, wizardi18n);

        NFSShareOptionsEntryPanel nfsShareOptionsEntryPanel =
            new NFSShareOptionsEntryPanel(NFSWizardConstants.PANEL_5,
                wizardModel, wizardFlow, wizardi18n);

        NFSReviewPanel nfsReviewPanel = new NFSReviewPanel(
                NFSWizardConstants.PANEL_6, wizardModel, wizardFlow,
                wizardi18n);

        NFSSummaryPanel nfsSummaryPanel = new NFSSummaryPanel(
                NFSWizardConstants.PANEL_7, wizardModel, wizardFlow,
                wizardi18n);

        NFSResultsPanel nfsResultsPanel = new NFSResultsPanel(
                NFSWizardConstants.PANEL_8, wizardModel, wizardFlow,
                wizardi18n);


        // LogicalHost Wizards Panels

        HostnameEntryPanel lhhostnameEntryPanel = new HostnameEntryPanel(
                NFSWizardConstants.PANEL_2 + WizardFlow.HYPHEN +
                LogicalHostWizardConstants.HOSTNAMEENTRY, wizardModel,
                wizardFlow, wizardi18n);
        IPMPGroupEntryPanel lhipmpGroupEntryPanel = new IPMPGroupEntryPanel(
                NFSWizardConstants.PANEL_2 + WizardFlow.HYPHEN +
                LogicalHostWizardConstants.IPMPGROUPENTRY, wizardModel,
                wizardFlow, wizardi18n);

        // HASP Wizards Panels

        HASPFileSystemSelectionPanel fileSystemSelectionPanel =
            new HASPFileSystemSelectionPanel(NFSWizardConstants.PANEL_3 +
                WizardFlow.HYPHEN + HASPWizardConstants.PANEL_3, wizardModel,
                wizardFlow, wizardi18n);

        HASPCreateFileSystemPanel createFileSystemPanel =
            new HASPCreateFileSystemPanel(NFSWizardConstants.PANEL_3 +
                WizardFlow.HYPHEN + HASPWizardConstants.PANEL_4, wizardModel,
                wizardFlow, wizardi18n);

        HASPCreateFileSystemReviewPanel createFileSystemReviewPanel =
            new HASPCreateFileSystemReviewPanel(NFSWizardConstants.PANEL_3 +
                WizardFlow.HYPHEN + HASPWizardConstants.PANEL_5, wizardModel,
                wizardFlow, wizardi18n);


        // Add the Panels to the Root
        nfsWizardRoot.addChild(nfsIntroPanel);
        nfsWizardRoot.addChild(nfsNodeSelectionPanel);
        nfsWizardRoot.addChild(nfsLhResNameEntryPanel);

        // Add LogicalHost Wizard Panels
        nfsWizardRoot.addChild(lhhostnameEntryPanel);
        nfsWizardRoot.addChild(lhipmpGroupEntryPanel);

        nfsWizardRoot.addChild(nfsHaspResNameEntryPanel);

        // Add HASP Wizard Panels
        nfsWizardRoot.addChild(fileSystemSelectionPanel);
        nfsWizardRoot.addChild(createFileSystemPanel);
        nfsWizardRoot.addChild(createFileSystemReviewPanel);

        nfsWizardRoot.addChild(nfsPathPrefixEntryPanel);
        nfsWizardRoot.addChild(nfsShareOptionsEntryPanel);
        nfsWizardRoot.addChild(nfsReviewPanel);
        nfsWizardRoot.addChild(nfsSummaryPanel);
        nfsWizardRoot.addChild(nfsResultsPanel);

    }

    /**
     * Get the Handle to the NFS MBean on a specified node
     *
     * @return  Handle to the NFS MBean on a specified node
     */
    public static NFSMBean getNFSMBeanOnNode(JMXConnector connector) {

        // Only if there is no existing reference
        // Get Handler to NFSMBean
        NFSMBean mbeanOnNode = null;

        try {
            TTYDisplay.initialize();
            mbeanOnNode = (NFSMBean) TrustedMBeanModel.getMBeanProxy(
                    Util.DOMAIN, connector, NFSMBean.class, null, false);
        } catch (Exception e) {

            // Report Error that the user has to install
            // The Dataservice Package or upgrade it
            String errorMsg = pkgErrorString;
            MessageFormat msgFmt = new MessageFormat(errorMsg);
            errorMsg = msgFmt.format(
                    new String[] { NFSWizardConstants.NFS_PKG_NAME });
            TTYDisplay.printError(errorMsg);
            TTYDisplay.promptForEntry(wizardExitPrompt);
            System.exit(1);
        }

        return mbeanOnNode;
    }

    public static String appendDir(String mtpt, String dir) {
        mtpt = mtpt.trim();
        dir = dir.trim();

        if (!dir.equals("") &&
                (mtpt.charAt(mtpt.length() - 1) != Util.SLASH.charAt(0)) &&
                (dir.charAt(0) != Util.SLASH.charAt(0))) {
            mtpt = mtpt + Util.SLASH;
        }

        mtpt = mtpt + dir;

        return mtpt;
    }

    public static String trimSlash(String path) {
        path = path.trim();

        if (path.equals(Util.SLASH)) {
            return path;
        }

        if (path.charAt(0) == Util.SLASH.charAt(0)) {
            path = path.substring(1);
        }

        if (path.charAt(path.length() - 1) == Util.SLASH.charAt(0)) {
            path = path.substring(0, path.length() - 2);
        }

        return path;
    }

    /**
     * This function would return canonical path corresponding to the string
     * input. The function is written assuming the return path would get
     * appended to some already existing mountpoint. A null value is returned
     * for any invalid input
     */
    public static String getCanonicalPath(String path) {
        String cPath = null;

        path = path.trim();
        path = Util.SLASH + path;

        try {
            File tmpFile = new File(path);

            // check to prevent entries like /../xyz/
            String aPath = tmpFile.getAbsolutePath();
            String tmpAr[] = aPath.split(Util.SLASH);

            if ((tmpAr.length != 0) && tmpAr[1].equals("..")) {
                return null;
            }

            // get canonical path which would convert entries like /xyz/abc/..
            // to /xyz
            cPath = tmpFile.getCanonicalPath();

        } catch (Exception ex) {
        }

        // check to prevent entries like /xyz/../../
        String testStr = Util.SLASH + "..";

        if (cPath.equals(testStr)) {
            cPath = null;
        }

        return cPath;
    }

    /**
     * Main function of the Wizards
     */
    public static void main(String args[]) {

        // Starts the NFS Wizard
        NFSWizardCreator nfs = new NFSWizardCreator();
    }
}
