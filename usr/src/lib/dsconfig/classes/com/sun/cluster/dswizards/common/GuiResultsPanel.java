/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)GuiResultsPanel.java 1.10     08/05/20 SMI"
 */

package com.sun.cluster.dswizards.common;

// J2SE
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

//JMX
import javax.management.MBeanServerConnection;

// JATO
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.DisplayField;
import com.iplanet.jato.view.RequestHandlingViewBase;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.DisplayEvent;

// cacao
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.dataservices.utils.Util;

// SPM Common Files
import com.sun.cluster.dswizards.common.DSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.spm.common.SpmUtil;
import com.sun.cluster.spm.task.wizard.ConfigureDataServiceWizard;

import com.sun.cluster.common.JmxServerConnection;

// Lockharts
import com.sun.web.ui.model.CCPropertySheetModel;
import com.sun.web.ui.model.wizard.WizardEvent;
import com.sun.web.ui.view.alert.CCAlertInline;
import com.sun.web.ui.view.html.CCButton;
import com.sun.web.ui.view.html.CCHiddenField;
import com.sun.web.ui.view.propertysheet.CCPropertySheet;
import com.sun.web.ui.view.wizard.CCWizard;
import com.sun.web.ui.view.wizard.CCWizardPage;

/**
 * The Panel Class is Base Result panel class for dsconfig gui wizard.
 */
public class GuiResultsPanel extends RequestHandlingViewBase
    implements CCWizardPage {

    /** Wizard step's title dispayed first on the right */
    public static final String STEP_TITLE = "guiwizards.resultspanel.title";

    /** Wizard step's name displayed on the left */
    public static final String STEP_TEXT = "guiwizards.resultspanel.name";

    /** Wizard step's instructions dispayed just below the title */
    public static final String STEP_INSTRUCTION =
        "guiwizards.resultspanel.desc";

    /** Prefix to retrieve the wizard's step help */
    public static final String STEP_HELP_PREFIX =
        "guiwizards.resultspanel.help";

    /** JSP file used for the wizard's step */
    public static final String PAGELET_URL =
        "/jsp/task/wizard/TaskWizardStep.jsp";

    /** Child element for the hidden field */
    public static final String CHILD_CLOSE = "CloseWizardHiddenField";

    /** Child element to display the Alert Inline */
    public static final String CHILD_ALERT = "Alert";

    /** Name of the property sheet child */
    public static final String CHILD_PROPERTYSHEET = "PropertySheet";

    /** XML file containing the property sheet's model */
    public static final String PROPERTYSHEET_XML_FILE =
        GenericDSWizard.COMMON_XML_PATH + "resultsPanel.xml";
    private static final String CHILD_CMDLIST_VALUE = "CmdListValue";

    protected String success_msg_key;
    protected String failure_msg_key;

    // Property sheet model
    private CCPropertySheetModel propertySheetModel = null;
    protected DSConfigWizardModel wizardModel = null;


    /**
     * Construct an instance with the specified model and logical page name.
     *
     * @param  parent  The parent view of this object.
     * @param  model  This view's model.
     * @param  name  Logical page name.
     */
    public GuiResultsPanel(View parent, Model model, String name) {
        super(parent, name);
        setDefaultModel(model);
        createPropertySheetModel();
        registerChildren();
        pageInit();
    }

    private void pageInit() {
        wizardModel = (DSConfigWizardModel) getDefaultModel();
    }


    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Child manipulation methods
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    protected void registerChildren() {
        registerChild(CHILD_CLOSE, CCHiddenField.class);
        registerChild(CHILD_ALERT, CCAlertInline.class);
        registerChild(CHILD_PROPERTYSHEET, CCPropertySheet.class);
        propertySheetModel.registerChildren(this);
    }

    protected View createChild(String name) {

        if (name.equals(CHILD_PROPERTYSHEET)) {
            CCPropertySheet child = new CCPropertySheet(this,
                    propertySheetModel, name);

            return child;

        } else if (name.equals(CHILD_CLOSE)) {
            CCHiddenField child = new CCHiddenField(this, name, null);

            return child;

        } else if (name.equals(CHILD_ALERT)) {
            CCAlertInline child = new CCAlertInline(this, name, null);

            return child;

        } else if ((propertySheetModel != null) &&
                propertySheetModel.isChildSupported(name)) {

            // Create child from property sheet model.
            View child = propertySheetModel.createChild(this, name);

            return child;

        } else {
            throw new IllegalArgumentException(
                "GUI ResultsPanel Invalid child name [" + name + "]");
        }
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Display methods
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


    private void createPropertySheetModel() {
        DSConfigWizardModel wizardModel = (DSConfigWizardModel)
            getDefaultModel();

        try {
            FileInputStream fileInputStream = new FileInputStream(
                    PROPERTYSHEET_XML_FILE);
            String xmlFileString = ConfigureDataServiceWizard.getXMLString(
                    fileInputStream);
            fileInputStream.close();

            propertySheetModel = new CCPropertySheetModel();
            propertySheetModel.setDocument(xmlFileString);

        } catch (Exception e) {
            e.printStackTrace();

            return;
        }
    }

    private void populatePropertySheetModel() {
        wizardModel = (DSConfigWizardModel) getDefaultModel();

        CCWizard wizard = (CCWizard) getParentViewBean().getChild("Wizard");
        CCButton prevButton = (CCButton) wizard.getChild(
                CCWizard.CHILD_PREVIOUS_BUTTON);

        try {

            // Populate the alert with the results
            Object tmpObj = null;
            List commandList = new ArrayList();
            ExitStatus status[] = null;
            StringBuffer cmdListTextVal = new StringBuffer();
            CommandExecutionException cee = null;

            CCAlertInline alert = (CCAlertInline) getChild(CHILD_ALERT);
            WizardI18N wizardi18n = GenericDSWizard.getWizardI18N();
            String msg_success = wizardi18n.getString(success_msg_key);
            String msg_error = wizardi18n.getString(failure_msg_key);

            // Get list of executed commands
            tmpObj = wizardModel.getWizardValue(Util.CMDLIST);

            if (tmpObj != null)
                commandList = (List) tmpObj;

            if (commandList.isEmpty()) {
                alert.setType(CCAlertInline.TYPE_ERROR);
                alert.setSummary(msg_error);
                alert.setDetail("guiwizards.resultspanel.errComm.summary");

                return;
            }

            // Get exit status - if there is an exception while running the
            // commands
            tmpObj = wizardModel.getWizardValue(Util.EXITSTATUS);

            if (tmpObj != null) {
                cee = (CommandExecutionException) tmpObj;
                status = cee.getExitStatusArray();
            }


            // Set the list of commands in displayed text box
            Iterator i = commandList.iterator();
            cmdListTextVal.append(wizardi18n.getString(
                    "guiwizards.executionMessage"));

            while (i.hasNext()) {
                String tmpCmd = (String) i.next();

                if (tmpCmd.equals(Util.ROLLBACK_SUCCESS)) {

                    // Check to ensure the heading "Rollback commands" is not
                    // printed if there are no rollback commands.
                    if (i.hasNext()) {
                        tmpCmd = wizardi18n.getString(
                                "guiwizards.rollbackExecutionMessage");
                    } else {
                        tmpCmd = "";
                    }
                } else if (tmpCmd.equals(Util.ROLLBACK_FAIL)) {

                    // Rollback failed due to some exception while executing
                    // rollback commands
                    tmpCmd = wizardi18n.getString(
                            "guiwizards.rollbackUnableMessage");

                    // Disable previous button
                    prevButton.setDisabled(true);
                }

                cmdListTextVal.append(tmpCmd);
                cmdListTextVal.append("\n\n");
            }

            ((DisplayField) getChild(CHILD_CMDLIST_VALUE)).setValue(
                cmdListTextVal.toString());

            // Set the error message if any, in the alert box
            if (cee != null) {
                alert.setType(CCAlertInline.TYPE_ERROR);
                alert.setSummary(msg_error);

                StringBuffer errStrings = new StringBuffer();

                if ((status != null) && (status.length > 0)) {
                    List errList = status[0].getErrStrings();

                    if (errList != null) {
                        Iterator iter = errList.iterator();

                        while (iter.hasNext()) {
                            errStrings.append(iter.next());
                            errStrings.append("\n\n");
                        }
                    }
                }

                String msgStr = cee.getMessage();

                if (msgStr != null) {

                    if (msgStr.equals("nfs.resultspanel.dumpAdmin.error") ||
                            msgStr.equals(
                                "nfs.resultspanel.createShareDir.error")) {
                        errStrings.append("\n\n");
                        errStrings.append(wizardi18n.getString(msgStr));
                    } else if (msgStr.equals(Util.IO_ERROR)) {
                        errStrings.append("\n\n");
                        errStrings.append(wizardi18n.getString(
                                "guiwizards.resultspanel.errComm.summary"));
                    } else {
                        errStrings.append("\n\n");
                        errStrings.append(msgStr);
                    }
                }

                alert.setDetail(errStrings.toString());
            } else {
                alert.setType(CCAlertInline.TYPE_INFO);
                alert.setSummary(msg_success);

                // Disable previous button
                prevButton.setDisabled(true);
            }

            wizardModel.setWizardValue(Util.EXITSTATUS, null);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Get the pagelet to use for the rendering of this instance.
     *
     * @return  The pagelet to use for the rendering of this instance.
     */
    public String getPageletUrl() {
        return PAGELET_URL;
    }

    /**
     * This method is called when the page is beginning its display.
     *
     * @param  event  DisplayEvent is passed in by the framework.
     */
    public void beginComponentDisplay(DisplayEvent event)
        throws ModelControlException {
        populatePropertySheetModel();

        CCWizard wizard = (CCWizard) getParentViewBean().getChild("Wizard");

        // Disable finsish button and rename it to next
        CCButton finishButton = (CCButton) wizard.getChild(
                CCWizard.CHILD_FINISH_BUTTON);
        finishButton.setDisplayLabel("wizard.button.next");
        finishButton.setDisabled(true);

        // Rename cancel button to close
        CCButton cancelButton = (CCButton) wizard.getChild(
                CCWizard.CHILD_CANCEL_BUTTON);
        cancelButton.setDisplayLabel("wizard.button.close");
    }

    /**
     * Step data validation is done here. This method is called from the
     * nextStep() of the wizard.
     *
     * @param  event  WizardEvent
     *
     * @return  true if page data is valid; false otherwise. The event error
     * message and severity are set in case of errors.
     */
    public boolean validate(WizardEvent event) {

        try {
            RequestContext rq = RequestManager.getRequestContext();
            MBeanServerConnection conn = JmxServerConnection.getInstance()
                .getConnection(rq, SpmUtil.getClusterEndpoint());

            // Clear the command list and exit status
            wizardModel.setWizardValue(Util.EXITSTATUS, null);
            wizardModel.setWizardValue(Util.CMDLIST, null);
            wizardModel.writeToStateFile(conn);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }
}
