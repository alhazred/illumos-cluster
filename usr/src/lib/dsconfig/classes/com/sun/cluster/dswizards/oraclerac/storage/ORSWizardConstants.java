/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)ORSWizardConstants.java	1.10	08/07/14 SMI"
 */

package com.sun.cluster.dswizards.oraclerac.storage;

/**
 * Constants for OracleRAC Storage Wizard
 */
public class ORSWizardConstants {

    // Flow XML
    public static final String ORS_FLOW_XML =
        "/usr/cluster/lib/ds/oraclerac/storage/ORSWizardFlow.xml";

    // Constants for Wizard Flow
    public static final String INTROPANEL = "orsIntroPanel";
    public static final String LOCATIONPANEL = "racLocationPanel";
    public static final String DISKPANEL = "orsDiskSetPanel";
    public static final String CLUSTERDISKPANEL = "clusterDisksPanel";
    public static final String CLUSTERDISKSELVOLPANEL = "clusterDiskSelVolPanel";
    public static final String CLUSTERDISKVOLUMEPANEL = "clusterDiskVolumePanel";
    public static final String FSPANEL = "orsFileSystemPanel";
    public static final String CLUSTERFSPANEL = "clusterFileSystemPanel";
    public static final String REVIEWPANEL = "orsReviewPanel";
    public static final String SUMMARYPANEL = "orsSummaryPanel";
    public static final String RESULTSPANEL = "orsResultsPanel";
    public static final String RAC_LOCATION_PREF = "RAC_Location_Preference";

    public static final String SHOW_RAC_DEVICES = "SHOW_RAC_DEVICES";
    public static final String SHOW_RAC_FS = "SHOW_RAC_FS";
    public static final String SHOW_RAC_DEVICES_ONLY = "SHOW_RAC_DEVICES_ONLY";
    public static final String SHOW_RAC_FS_ONLY = "SHOW_RAC_FS_ONLY";
    public static final String SHOW_RAC_DEVICES_AND_FS =
        "SHOW_RAC_DEVICES_AND_FS";

    public static final String SHOW_CLUSTER_DEVICES = "SHOW_CLUSTER_DEVICES";
    public static final String SHOW_CLUSTER_FS = "SHOW_CLUSTER_FS";
    public static final String SHOW_DISKS_SUMMARY = "SHOW_DISKS_SUMMARY";
    public static final String SHOW_FS_SUMMARY = "SHOW_FS_SUMMARY";
    public static final String ORS_DEVICES_SELECTED = "ORS_DEVICES_SELECTED";
    public static final String ORS_FS_SELECTED = "ORS_FS_SELECTED";

    public static final String WIZARD_TYPE = "Wizard_Type";
    public static final String GUI_WIZARD = "1";
    public static final String SELECT_VOLUMES = "Select_Volumes";
    public static final String TRUE = "true";
    public static final String FALSE = "false";

    /**
     * Creates a new instance of ORSWizardConstants
     */
    public ORSWizardConstants() {
    }
}
