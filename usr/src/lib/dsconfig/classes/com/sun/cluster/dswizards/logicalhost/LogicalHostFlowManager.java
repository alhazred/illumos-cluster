/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)LogicalHostFlowManager.java 1.13     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.logicalhost;

// CMASS
import com.sun.cluster.agent.dataservices.utils.Util;

// Wizard Common
import com.sun.cluster.dswizards.common.DSConfigWizardModel;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.DSWizardInterface;

// J2SE
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


/**
 * The class takes decision on which panel to show "next" depending upon the
 * conditions set at a particular context.
 */
public class LogicalHostFlowManager implements DSWizardInterface {

    /**
     * Creates a new instance of LogicalHostFlowManager
     */
    public LogicalHostFlowManager() {
    }

    /**
     * @see  com.sun.cluster.dswizards.common.DSWizardInterface#getNextPanel
     */
    public String getNextPanel(String curPanelName, Object wizCxtObj,
        String options[]) {
	DSConfigWizardModel guiModel = null;
	CliDSConfigWizardModel cliModel = null;

	if (wizCxtObj instanceof CliDSConfigWizardModel) {
	    cliModel = (CliDSConfigWizardModel)wizCxtObj;
	} else if (wizCxtObj instanceof DSConfigWizardModel) {
	    guiModel = (DSConfigWizardModel)wizCxtObj;
	}

        // Check if IPMPGroupPanel needs to be displayed
        if (curPanelName.equals(LogicalHostWizardConstants.HOSTNAMEENTRY)) {

            // Get the IPMP Groups Discovered from the Model
            String propertyNames[] = { Util.NETIFLIST };

            // Get the Discovered IPMPGroups From the wizardModel
            String ipmpGroupsNames[] = null;
	    if (guiModel != null) {
		ipmpGroupsNames = (String[])guiModel.getWizardValue(
		    Util.NETIFLIST);
	    } else {
		ipmpGroupsNames = (String[])cliModel.getWizardValue(
		    Util.NETIFLIST);
	    }

            if (ipmpGroupsNames == null) {
                return options[1];
	    }

            // Check whether there are any nodes with multiple IPMPGroups.
            // If there are, then go to the panel which lets the user
	    // select one among them.

	    // Build a list of groups on each node
	    HashMap map = new HashMap();
	    boolean hasMultipleGroups = false;
	    for (int i = 0; i < ipmpGroupsNames.length; i++) {
		String s[] = ipmpGroupsNames[i].split("@");
		String groupName = s[0];
		String nodeKey = s[1];

		ArrayList groupList = (ArrayList)map.get(nodeKey);
		if (groupList == null) {
		    groupList = new ArrayList();
		    groupList.add(groupName);
		    map.put(nodeKey, groupList);
		} else {
		    hasMultipleGroups = true;
		    groupList.add(groupName);
		    map.put(nodeKey, groupList);
		}
	    }

	    // If there are more than one group per node store the GroupLists
	    // in the WizCtx and goto IPMPGroups Panel
	    if (hasMultipleGroups) {
		// Remove entries in the map that have only one group
		String keys[] = (String [])map.keySet().toArray(new String[0]);
		for (int i = 0; i < keys.length; i++) {
		    String key = keys[i];
		    ArrayList list = (ArrayList)map.get(key);
		    if (list.size() <= 1) {
			map.remove(key);
		    }
		}
		if (guiModel != null) {
		    guiModel.setWizardValue(Util.IPMPGROUPMAP, map);
		} else {
		    cliModel.setWizardValue(Util.IPMPGROUPMAP, map);
		}
		return options[0];
	    } else {
		return options[1];
	    }
        }

        return null;
    }
}
