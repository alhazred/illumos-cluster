/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ApacheWizardConstants.java 1.12     08/07/23 SMI"
 */

package com.sun.cluster.dswizards.apache;

/**
 * This class holds all the constants for the ApacheWizard
 */
public class ApacheWizardConstants {

    // Package Name
    public static final String APACHE_PKG_NAME = "SUNWscapc";

    // State File
    public static final String APACHE_STATE_FILE =
        "/opt/cluster/lib/ds/history/ApacheWizardStateFile";

    // Flow XML
    public static final String APACHE_FLOW_XML =
        "/usr/cluster/lib/ds/apache/ApacheWizardFlow.xml";

    // Constants for Wizard Flow
    public static final String NEWHOST = "NEWHOST";
    public static final String NEWMOUNT = "NEWMOUNT";
    public static final String DISPLAY_FS_TABLE = "displayFSTable";
    public static final String APACHE_STORAGE_RESOURCES =
        "ApacheStorageResources"; // both hasp and qfs
    public static final String STORAGE_RESOURCE_NAME =
        "StorageResourceName"; // both hasp and qfs
    public static final String APACHE_MULTIPLE_MOUNTS = "ApacheMultipleMounts";
    public static final String APACHE_FILESYSTEMS_SELECTED =
        "ApacheFilesystemsSelected";
    public static final String APACHE_QFS_RS_SELECTED = "ApacheSharedQfsRSSelected";
    public static final String APACHE_FS_MTPTS = "ApacheFilesystemMountpoints";

    // Panel Names
    public static final String INTROPANEL = "apacheIntroPanel";
    public static final String NODEPANEL = "apacheNodePanel";
    public static final String MODEPANEL = "apacheModeEntryPanel";
    public static final String CONFPANEL = "apacheConfigFilePanel";
    public static final String DOCROOTPANEL = "apacheDocRootPanel";
    public static final String FSPANEL = "apacheFileSystemPanel";
    public static final String MPPANEL = "apacheMountPointPanel";
    public static final String HOSTPANEL = "apacheHostPanel";
    public static final String REVIEWPANEL = "apacheReviewPanel";
    public static final String OBJECTSREVIEWPANEL = "apacheObjectsReviewPanel";
    public static final String SUMMARYPANEL = "apacheSummaryPanel";
    public static final String RESULTSPANEL = "apacheResultsPanel";

    /**
     * Creates a new instance of ApacheWizardConstants
     */
    public ApacheWizardConstants() {
    }
}
