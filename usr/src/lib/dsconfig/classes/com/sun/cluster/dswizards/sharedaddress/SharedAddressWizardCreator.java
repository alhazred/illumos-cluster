/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)SharedAddressWizardCreator.java 1.14     08/05/20 SMI"
 */

package com.sun.cluster.dswizards.sharedaddress;

// Cacao
import com.sun.cacao.agent.JmxClient;

// CMASS
import com.sun.cluster.agent.dataservices.sharedaddress.SharedAddressMBean;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.common.TrustedMBeanModel;

// Wizard Common
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardComposite;
import com.sun.cluster.dswizards.clisdk.core.WizardCreator;
import com.sun.cluster.dswizards.logicalhost.HostnameEntryPanel;
import com.sun.cluster.dswizards.logicalhost.HostnameResultPanel;
import com.sun.cluster.dswizards.logicalhost.HostnameWizardIntroPanel;
import com.sun.cluster.dswizards.logicalhost.IPMPGroupEntryPanel;
import com.sun.cluster.dswizards.logicalhost.LogicalHostWizardConstants;
import com.sun.cluster.dswizards.logicalhost.PreferredNodePanel;

// J2SE
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

// JMX
import javax.management.remote.JMXConnector;


/**
 * This class holds the Wizard Panel Tree information for SharedAddress Wizard.
 * This class creates the SharedAddress wizard from the State file and
 * WizardFlow File.
 */
public class SharedAddressWizardCreator extends WizardCreator {


    public static String WIZARDTYPE = "shwizard";

    private static final String FLOW_XML =
        LogicalHostWizardConstants.SA_FLOW_XML;
    private static final String STATE_FILE =
        LogicalHostWizardConstants.SA_STATE_FILE;

    /**
     * Default Constructor Creates  SharedAddress Wizard from the State File.
     * The Wizard is intialized to a particular state using the State file. The
     * Wizard flow is governed by the Flow XML.
     */
    public SharedAddressWizardCreator() {
        super(STATE_FILE, FLOW_XML);
    }

    /**
     * Creates the actual Panel Tree Structure for the SharedAddress Wizard
     */
    protected void createClientTree() {


        // Get the wizard root
        WizardComposite sharedAddressWizardRoot = getRoot();

        // Create the Individual Panels of the Wizard

        // Each Panel is associated with a Name, the wizardModel from
        // the WizardCreator and the WizardFlow Structure.

        HostnameWizardIntroPanel introPanel = new HostnameWizardIntroPanel(
                LogicalHostWizardConstants.INTROPANEL, wizardModel, wizardFlow,
                wizardi18n, WIZARDTYPE);

        HostnameEntryPanel hostnameEntryPanel = new HostnameEntryPanel(
                LogicalHostWizardConstants.HOSTNAMEENTRY, wizardModel,
                wizardFlow, wizardi18n);

        IPMPGroupEntryPanel ipmpGroupEntryPanel = new IPMPGroupEntryPanel(
                LogicalHostWizardConstants.IPMPGROUPENTRY, wizardModel,
                wizardFlow, wizardi18n);
        PreferredNodePanel preferredNodePanel = new PreferredNodePanel(
                LogicalHostWizardConstants.PREFERREDNODEPANEL, wizardModel,
                wizardFlow, wizardi18n);

        SharedAddressReviewPanel reviewPanel = new SharedAddressReviewPanel(
                LogicalHostWizardConstants.REVIEWPANEL, wizardModel, wizardFlow,
                wizardi18n);

        SharedAddressSummaryPanel summaryPanel = new SharedAddressSummaryPanel(
                LogicalHostWizardConstants.SUMMARYPANEL, wizardModel,
                wizardFlow, wizardi18n);

        HostnameResultPanel resultPanel = new HostnameResultPanel(
                LogicalHostWizardConstants.RESULTSPANEL, wizardModel,
                wizardFlow, wizardi18n);

        // Add the Panels to the Root
        sharedAddressWizardRoot.addChild(introPanel);
        sharedAddressWizardRoot.addChild(hostnameEntryPanel);
        sharedAddressWizardRoot.addChild(ipmpGroupEntryPanel);
        sharedAddressWizardRoot.addChild(preferredNodePanel);
        sharedAddressWizardRoot.addChild(reviewPanel);
        sharedAddressWizardRoot.addChild(summaryPanel);
        sharedAddressWizardRoot.addChild(resultPanel);
    }

    /**
     * Get the Handle to the SharedAddress MBean on a specified node
     *
     * @return  Handle to the SharedAddress MBean on a specified node
     */
    public static SharedAddressMBean getSharedAddressMBeanOnNode(
        JMXConnector connector) {

        // Only if there is no existing reference
        // Get Handler to SharedAddressMBean
        SharedAddressMBean mbeanOnNode = null;

        try {
            TTYDisplay.initialize();
            mbeanOnNode = (SharedAddressMBean) TrustedMBeanModel.getMBeanProxy(
                    Util.DOMAIN, connector, SharedAddressMBean.class, null,
                    false);
        } catch (Exception e) {

            // Report Error that the user has to install
            // The Dataservice Package or upgrade it
            String errorMsg = pkgErrorString;
            MessageFormat msgFmt = new MessageFormat(errorMsg);
            errorMsg = msgFmt.format(
                    new String[] { LogicalHostWizardConstants.PKG_NAME });
            TTYDisplay.printError(errorMsg);
            TTYDisplay.promptForEntry(wizardExitPrompt);
            System.exit(1);
        }

        return mbeanOnNode;
    }

    /**
     * Get the Handle to the SharedAddress MBean
     *
     * @return  Handle to the SharedAddress MBean
     */
    public static SharedAddressMBean getSharedAddressMBean(
        JMXConnector connector) {
        return getSharedAddressMBeanOnNode(connector);
    }

    /**
     * Main function of the Wizards
     */
    public static void main(String args[]) {

        // Start the SharedAddress Wizard
        SharedAddressWizardCreator sh = new SharedAddressWizardCreator();
    }
}
