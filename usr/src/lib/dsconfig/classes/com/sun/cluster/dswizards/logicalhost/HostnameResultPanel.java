/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)HostnameResultPanel.java 1.10     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.logicalhost;

import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliResultsPanel;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;


/**
 * Results panel for Hostname wizard
 */
public class HostnameResultPanel extends CliResultsPanel {

    private String wizardType; // wizardType

    /**
     * Creates a new instance of HostnameResultPanel
     */
    public HostnameResultPanel() {
        super();
    }

    /**
     * Creates a HostnameResultPanel Panel with the given name and tree manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public HostnameResultPanel(String name, WizardTreeManager manager) {
        super(name, manager);
    }

    /**
     * Creates a HostnameResultPanel with the given name and associates it with
     * the WizardModel and WizardFlow.
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     * @param  wizardi18n  Resource Bundle
     */
    public HostnameResultPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(name, model, wizardFlow, wizardi18n);

        Object type = wizardModel.getWizardValue(
                LogicalHostWizardConstants.WIZARDTYPE);

        if (type != null) {
            wizardType = (String) type;
        } else {
            wizardType = LogicalHostWizardCreator.WIZARDTYPE;
        }
    }


    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {
        title_msg_key = "nfs.resultspanel.title";
        success_msg_key = wizardType + ".summaryPanel.successGenerateCommand";
        failure_msg_key = wizardType + ".summaryPanel.failureGenerateCommand";
        super.consoleInteraction();
    }

}
