/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)RACFilesystemsPanel.java	1.19	09/03/11 SMI"
 */

package com.sun.cluster.dswizards.oraclerac.storage;

// CMASS
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.zonecluster.ZoneClusterData; 

// CLI SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;

// Wizard Common
import com.sun.cluster.dswizards.common.CliUtil;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.oraclerac.invoker.RACInvokerCreator;

import com.sun.cluster.model.DataModel;
import com.sun.cluster.model.ZoneClusterModel;
import com.sun.cluster.model.RACModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;


/**
 * This panel lists to the user all the s-QFS,NAS that are monitored by
 * ScalMountPoints Resources under RGM.
 */
public class RACFilesystemsPanel extends WizardLeaf {


    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N messages; // Message Bundle

    private RACModel racModel = null;

    /**
     * Creates a new instance of RACFilesystemsPanel
     */
    public RACFilesystemsPanel() {
        super();
    }

    /**
     * Creates an RACFilesystems Panel with the given name and tree manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public RACFilesystemsPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates an RACFilesystems Panel for racstorage Wizard with the given name
     * and associates it with the WizardModel and WizardFlow
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     */
    public RACFilesystemsPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N messages) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;

        // Get the I18N instance
        this.messages = messages;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {

        String title = messages.getString("racstorage.racFSPanel.title");
        String subtitle = messages.getString("racstorage.racFSPanel.subTitle");
        String desc = messages.getString("racstorage.racFSPanel.desc");
        String mountPointDirTitle = messages.getString(
                "racstorage.racFSPanel.mountPointDirectory");
        String mountPointResTitle = messages.getString(
                "racstorage.racFSPanel.mountPointResource");
        String mountPointResGroupTitle = messages.getString(
                "racstorage.racFSPanel.mountPointResourceGroup");
        String fileSystemTypeTitle = messages.getString(
                "racstorage.racFSPanel.filesystem");
        String help = messages.getString("racstorage.racFSPanel.cli.help");
        String specify = messages.getString("racstorage.racFSPanel.other");
        String yes = messages.getString("cliwizards.yes");
        String no = messages.getString("cliwizards.no");
        String dchar = messages.getString("ttydisplay.d.char");
        String rchar = messages.getString("ttydisplay.r.char");
        String done = messages.getString("ttydisplay.menu.done");
        String refresh = messages.getString("ttydisplay.menu.refresh");
        String cchar = messages.getString("ttydisplay.c.char");
        String createNewFS = cchar + ") " +
            messages.getString("racstorage.racFSPanel.createNewFS");
        String confirmDisks = messages.getString(
                "racstorage.racFSPanel.confirmFS");
        String confirmMessage = messages.getString("cliwizards.confirmMessage");
        String exitOption = no;
        String empty = messages.getString("racstorage.racFSPanel.empty");
        String emptySelection = messages.getString(
                "racstorage.racFSPanel.emptySelection");
        String continueConfirm = messages.getString(
                "cliwizards.continueConfirm");
        String continue_txt = messages.getString("ttydisplay.msg.cli.enter");
        String back = messages.getString("ttydisplay.back.char");
        String table_empty_continue = messages.getString(
                "cliwizards.table.empty.continue");
        String help_text = messages.getString("storageSelectionPanel.cli.help");

        String nodeList[] = (String[]) wizardModel.getWizardValue(
                Util.BASE_CLUSTER_NODELIST);
        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);

	racModel = RACInvokerCreator.getRACModel();

	HashMap helperData = new HashMap();

        ZoneClusterData zcData = null;

	DataModel dm = DataModel.getDataModel(null);
	ZoneClusterModel zm = dm.getZoneClusterModel();

	ArrayList zoneClusterPaths = null;
	ArrayList fsTabList = null;
	HashMap fsTabMap = null;

	String selectedZoneCluster = CliUtil.getZoneClusterName(wizardModel);
	if (selectedZoneCluster != null &&
	    selectedZoneCluster.trim().length() > 0) {

	    try {
		zcData = zm.getZoneClusterData(null, selectedZoneCluster);
		String zonePath = zcData.getZonePath();
	    
		if (zonePath != null && zonePath.trim().length() > 0) {
		    if (zoneClusterPaths == null) {
			zoneClusterPaths = new ArrayList();
		    }
		    zoneClusterPaths.add(zonePath);
		}

		fsTabList = zcData.getZoneClusterFSTabList();

		if (fsTabMap == null) {
		    fsTabMap = new HashMap();
		}
		fsTabMap.put(selectedZoneCluster, fsTabList);

	    } catch (Exception e) {
		e.printStackTrace();
	    }
	} else {
	    try {
		// Pass the zone cluster path of all the zone clusters on the system.
		String[] zoneClusterList = zm.getZoneClusterList(null);

		if (zoneClusterList != null) {
		    for (int i = 0 ; i < zoneClusterList.length; i++) {

			zcData = zm.getZoneClusterData(null, zoneClusterList[i]);

			String zonePath = zcData.getZonePath();

			if (zonePath != null && zonePath.trim().length() > 0) { 
			    if (zoneClusterPaths == null) {
				zoneClusterPaths = new ArrayList();
			    }
			    zoneClusterPaths.add(zonePath);
			}

			fsTabList = zcData.getZoneClusterFSTabList();

			if (fsTabMap == null) {
			    fsTabMap = new HashMap();
			}

			fsTabMap.put(zoneClusterList[i], fsTabList);

		    }
		}
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}

        // Pass the fstab list
        helperData.put(Util.ZONE_CLUSTER_FSTAB_LIST, fsTabMap);
	helperData.put(Util.SEL_ZONE_CLUSTER, selectedZoneCluster);
	helperData.put(Util.ZONE_PATH, zoneClusterPaths);

        do {
            TTYDisplay.clearScreen();
            TTYDisplay.clear(1);

            // Enable Back and Help
            TTYDisplay.setNavEnabled(true);
            this.cancelDirection = false;

            // Title and Description
            TTYDisplay.printTitle(title);

            // Populate the Table to be displayed
            Vector fsData = new Vector();

            Map qfsMap = null;
            Map nasMap = null;
            Map newAdditionsMap = null;
            List fsNames = new ArrayList();
            List fstypeList = new ArrayList();

            // Get the previous selections
            List prevSelected = (ArrayList) wizardModel.getWizardValue(
                    ORSWizardConstants.ORS_FS_SELECTED);
            List defSelection = new ArrayList();

            qfsMap = (Map) wizardModel.getWizardValue(Util.RGM_SQFS);
            nasMap = (Map) wizardModel.getWizardValue(Util.RGM_NAS);

            // Depending on the Storage Schemes get the respective
            // Resources and the Filesystems monitored by them.
	    String keyPrefix = (String) wizardModel.getWizardValue(
		Util.KEY_PREFIX);

            List storageScheme = (ArrayList) wizardModel.getWizardValue(
		keyPrefix + Util.ORF_STORAGE_MGMT_SCHEMES);

            Thread t = null;

            try {
                t = TTYDisplay.busy(messages.getString(
                            "racstorage.racFSPanel.discovery.busy"));
                t.start();

                if (storageScheme.contains(Util.QFS_SVM) ||
		    storageScheme.contains(Util.QFS_HWRAID)) {

                    if (qfsMap == null) {

                        // Populate the qfsMap according to the qfs scheme
                        boolean qfsOnRaw = storageScheme.contains(
                                Util.QFS_HWRAID);
                        boolean qfsOnOban = storageScheme.contains(
                                Util.QFS_SVM);
                        qfsMap = getFilesystemResources_forQFS(nodeList,
                                qfsOnRaw, qfsOnOban, helperData);
                        wizardModel.setWizardValue(Util.RGM_SQFS, qfsMap);
                    }
                }

                if (storageScheme.contains(Util.NAS) ||
		    storageScheme.contains(Util.SUN_NAS)) {

                    if (nasMap == null) {
                        nasMap = getFilesystemResources_forNAS(nodeList, helperData);
                        wizardModel.setWizardValue(Util.RGM_NAS, nasMap);
                    }
                }

                // Get the newly added entries from the wizardModel
                newAdditionsMap = (Map) wizardModel.getWizardValue(
                        Util.NEW_FS_RESOURCES);

                // The Maps pertaining to the storage schemes ared added to
                // the final vector that is displayed. The FS TYPE is added
                // to the final vector along with the respective Maps.
                if (qfsMap != null) {
                    Set entrySet = qfsMap.entrySet();

                    for (Iterator i = entrySet.iterator(); i.hasNext();) {
                        Map.Entry entry = (Map.Entry) i.next();
                        fsNames.add(entry.getKey());

                        String qfsInfo[] = ((String) entry.getValue()).split(
                                ",");

                        // Get the resource group for the qfs Resource
                        String propertyName[] = new String[] {
                                Util.STORAGEGROUP
                            };

			helperData.put(Util.RS_NAME, qfsInfo[0]);
                        Map resourceGroupMap = (HashMap) racModel.discoverPossibilities(
			    null, nodeEndPoint, propertyName, helperData);

                        fsData.add(qfsInfo[0]); // Resource Name
                        fsData.add((String) resourceGroupMap.get(
                                Util.STORAGEGROUP)); // Resource Group Name
                        fsData.add(qfsInfo[1]); // MountPoint
                        fstypeList.add(Util.SQFS);

                        // populate the default selection list with the
                        // previous selections
                        if ((prevSelected != null) &&
                                prevSelected.contains(qfsInfo[0])) {
                            defSelection.add(Integer.toString(
                                    fsData.size() / 3));
                        }
                    }
                }

                if (nasMap != null) {
                    Set entrySet = nasMap.entrySet();

                    for (Iterator i = entrySet.iterator(); i.hasNext();) {
                        Map.Entry entry = (Map.Entry) i.next();
                        fsNames.add(entry.getKey());

                        String nasInfo[] = ((String) entry.getValue()).split(
                                ",");

                        // Get the resource group for the NAS Resource
                        String propertyName[] = new String[] {
                                Util.STORAGEGROUP
                            };

			helperData.put(Util.RS_NAME, nasInfo[0]);
                        Map resourceGroupMap = (HashMap) racModel.discoverPossibilities(
			    null, nodeEndPoint, propertyName, helperData);

                        fsData.add(nasInfo[0]); // Resource Name
                        fsData.add((String) resourceGroupMap.get(
                                Util.STORAGEGROUP)); // Resource Group Name
                        fsData.add(nasInfo[1]); // MountPoint
                        fstypeList.add(Util.NAS);

                        // populate the default selection list with the
                        // previous selections
                        if ((prevSelected != null) &&
                                prevSelected.contains(nasInfo[1])) {
                            defSelection.add(Integer.toString(
                                    fsData.size() / 3));
                        }
                    }
                }

                if (newAdditionsMap != null) {
                    Set entrySet = newAdditionsMap.entrySet();
                    List newAdditions = new ArrayList();

                    // The newAdditions Map would contain the fsName
                    // as the key and the autogenerated resourceName,mountpoint
                    // and fsType as comma-separated values
                    for (Iterator i = entrySet.iterator(); i.hasNext();) {
                        Map.Entry entry = (Map.Entry) i.next();
                        fsNames.add(entry.getKey());

                        String entryString = (String) entry.getValue();

                        // Get mountPoint,resourceName
                        String entryStringArr[] = entryString.split(",");

                        fsData.add(""); // ResourceName
                        fsData.add(""); // Resource Group Name
                        fsData.add(entryStringArr[1]); // MountPoint
                        fstypeList.add(entryStringArr[2]); // fsType
                        newAdditions.add(entryStringArr[1]);

                        // populate the default selection list with the
                        // previous selections
                        if ((prevSelected != null) &&
                                prevSelected.contains(entryStringArr[1])) {
                            defSelection.add(Integer.toString(
                                    fsData.size() / 3));
                        }
                    }

                    String flag = (String) wizardModel.getWizardValue(
                            ORSWizardConstants.SHOW_FS_SUMMARY);

                    if (flag.equals("true")) {
                        TTYDisplay.clear(1);
                        TTYDisplay.printSubTitle(messages.getString(
			    "racstorage.racFSPanel.summary",
			    new String[] { 
				Util.convertArraytoString((String[])
				    newAdditions.toArray(new String[0])) } ));
                    }

                    wizardModel.setWizardValue(
                        ORSWizardConstants.SHOW_FS_SUMMARY, "false");
                }
            } finally {

                try {
                    t.interrupt();
                    t.join();
                } catch (Exception exe) {
                }
            }

            TTYDisplay.clear(1);

            List optionsList = null;
            String enteredKey = "";
            String option[] = null;
            HashMap customTags = new HashMap();

            TTYDisplay.pageText(desc);
            TTYDisplay.clear(1);
            TTYDisplay.printSubTitle(messages.getString(
                    "racstorage.racFSPanel.extraDesc"));

            customTags.put(dchar, done);
            customTags.put(cchar, createNewFS);
            customTags.put(rchar, refresh);

            String subheadings[] = {
                    mountPointResTitle, mountPointResGroupTitle,
                    mountPointDirTitle
                };

            if ((fsData != null) && (fsData.size() > 0)) {

                if ((defSelection == null) || (defSelection.size() == 0)) {

                    // Display the (n*3) Table
                    option = TTYDisplay.create2DTable(subtitle, subheadings,
                            fsData, fsData.size() / 3, 3, customTags, help,
                            empty, false);
                } else {

                    // Display the (n*3) Table
                    option = TTYDisplay.create2DTable(subtitle, subheadings,
                            fsData, fsData.size() / 3, 3, customTags, help,
                            empty, false, defSelection);
                }

                if (option != null) {
                    optionsList = Arrays.asList(option);
                }
            } else {

                // Print empty text and automatically select 'Create New'
                TTYDisplay.displaySubheadings(subheadings);
                TTYDisplay.printSubTitle(empty);
                enteredKey = TTYDisplay.promptForEntry(table_empty_continue,
                        help_text);
                optionsList = new ArrayList();
                optionsList.add(cchar);

                // Set SHOW_CLUSTER_FS so that wizardFlow
                // would take the flow to ClusterFSPanel
                wizardModel.setWizardValue(ORSWizardConstants.SHOW_CLUSTER_FS,
                    "true");
                exitOption = yes;
            }

            // Handle Back
            if ((optionsList == null) || enteredKey.equals(back)) {
                this.cancelDirection = true;

                return;
            } else {

                // optionsList = Arrays.asList (option);
                List tmpSelected = (ArrayList) wizardModel.getWizardValue(
                        ORSWizardConstants.ORS_FS_SELECTED);

                if (tmpSelected == null) {
                    tmpSelected = new ArrayList();
                }

                if (optionsList.contains(rchar)) {

                    // refresh the data
                    wizardModel.setWizardValue(Util.RGM_SQFS, null);
                    wizardModel.setWizardValue(Util.RGM_NAS, null);
                    wizardModel.setWizardValue(
                        ORSWizardConstants.ORS_FS_SELECTED, null);

                    continue;
                } else {

                    // Get the fsResourceNames selected
                    Map selectedFSResources = new HashMap();

                    for (Iterator i = optionsList.iterator(); i.hasNext();) {
                        String optionValue = (String) i.next();

                        try {
                            int value = Integer.parseInt(optionValue);

                            String fsMountPoint = (String) fsData.get((value *
                                        3) - 1);

                            // Get the coressponding resource name from
                            // the vector
                            String fsResourceName = (String) fsData.get((value *
                                        3) - 3);

                            if (fsResourceName.length() == 0) {
                                fsResourceName =
                                    getResourceName_forScalMountPoint(
                                        fsMountPoint, helperData);
                            }

                            // Get the fileSystem Name from the fsNames list
                            String fsName = (String) fsNames.get(value - 1);
                            String fsType = (String) fstypeList.get(value - 1);
                            selectedFSResources.put(fsResourceName,
                                fsName + "," + fsMountPoint + "," + fsType);
                            tmpSelected.add(fsMountPoint);
                        } catch (NumberFormatException nfe) {

                            // Custom Tags
                            if (optionValue.equals(cchar)) {

                                // Set SHOW_CLUSTER_FS so that wizardFlow
                                // would take the flow to ClusterFSPanel
                                wizardModel.setWizardValue(
                                    ORSWizardConstants.SHOW_CLUSTER_FS, "true");
                                wizardModel.setWizardValue(
                                    ORSWizardConstants.ORS_FS_SELECTED,
                                    tmpSelected);
                                exitOption = yes;
                            }
                        }
                    }

                    // Store Selection
                    if (optionsList.contains(dchar)) {
                        exitOption = yes;

                        Set selectedResources = selectedFSResources.keySet();

                        if (selectedResources.size() == 0) {
                            wizardModel.setWizardValue(
                                Util.SELECTED_FS_RESOURCES, null);
                            wizardModel.setWizardValue(
                                ORSWizardConstants.ORS_FS_SELECTED, null);
                        } else {

                            // Store the user selected ScalFSResources
                            // in the wizardModel
                            wizardModel.setWizardValue(
                                Util.SELECTED_FS_RESOURCES,
                                selectedFSResources);
                            wizardModel.setWizardValue(
                                ORSWizardConstants.ORS_FS_SELECTED,
                                tmpSelected);

                            // For each individual resource selected
                            // store the QFS or NAS Information
                            fillQFSInfoMap(selectedFSResources, helperData);
                            fillNASInfoMap(selectedFSResources, helperData);
                        }
                    }
                }
            }
        } while (!exitOption.equalsIgnoreCase(yes));
    }


    // Gets a Map of "ScalMountPoint" resources monitoring QFS filesystems
    // for a given nodelist
    private Map getFilesystemResources_forQFS(String nodelist[],
        boolean qfsOnRaw, boolean qfsOnOban, HashMap helperData) {

	racModel = RACInvokerCreator.getRACModel();
        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);

        // Get the RGM Managed S-QFS Map
        String propertyNames[] = { Util.SQFS };
        Map discoveredMap = racModel.discoverPossibilities(
	    null, nodeEndPoint, nodelist, propertyNames, helperData);
        Map rgmQFSMap = (HashMap) discoveredMap.get(Util.RGM_SQFS);
	Map nonRgmQFSMap = (HashMap) discoveredMap.get(Util.NONRGM_SQFS);
	wizardModel.setWizardValue(Util.NONRGM_SQFS, nonRgmQFSMap);

        return rgmQFSMap;
    }

    // Gets a Map of "ScalMountPoint" resources monitoring NAS filesystems
    // for a given nodelist
    private Map getFilesystemResources_forNAS(String nodelist[], HashMap helperData) {

	racModel = RACInvokerCreator.getRACModel();
        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);

        // Get the RGM Managed NAS Map
        String propertyNames[] = { Util.NAS };
        Map discoveredMap = racModel.discoverPossibilities(
	    null, nodeEndPoint, nodelist, propertyNames, null);
        Map rgmNASMap = (HashMap) discoveredMap.get(Util.RGM_NAS);
	Map nonRgmNASMap = (HashMap) discoveredMap.get(Util.NONRGM_NAS);
	wizardModel.setWizardValue(Util.NONRGM_NAS, nonRgmNASMap);

        return rgmNASMap;
    }

    // For each qfs Filesystem and Mountpoint,
    // --> gets its metaserver resource
    // --> gets the possible nodelist
    // for the metaserver resource
    // --> gets the oban diskgroup on which it is created
    // @param QFS Filesystem Information as String[]
    // where, String Array is
    // String[0] - qfsName
    // String[1] - qfsMountpoint
    // @return HashMap of structure
    // qfsMountPoint(key) infoMap(HashMap)
    // where, infoMap is of structure
    // MetaserverResource(key) - Name of Metaserver Resource
    // MetaserverNodes(key) - String[] of Metaserver Nodes
    // ObanDiskGroup(key) - Name of obanDiskGroup
    private Map getQFSInformation(String fsInfoStrArr[], HashMap helperData) {

	racModel = RACInvokerCreator.getRACModel();
        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);

	Map discoveredMap = null;
	if (helperData != null && !helperData.isEmpty()) {
	    helperData.put(Util.QFS_INFO, fsInfoStrArr);
	    // Get the QFS Information for a qfsMountPoint
	    String propertyNames[] = { Util.QFS_INFO };
	    discoveredMap = racModel.discoverPossibilities(
		null, nodeEndPoint, propertyNames, helperData);
	}

        return discoveredMap;
    }

    /**
     * Get the waitZCBoot resource and group information
     */
    private Map getWaitZCInformation(HashMap helperData) {

	racModel = RACInvokerCreator.getRACModel();
	String nodeEndPoint = (String)wizardModel.getWizardValue(
		Util.NODE_TO_CONNECT);

	Map discoveredMap = null;
	if (helperData != null && !helperData.isEmpty()) {
	    //Get the waitZCBoot
	    String propertyNames[] = { Util.WAIT_ZC_BOOT_INFO };
	    discoveredMap = racModel.discoverPossibilities(
		null, nodeEndPoint, propertyNames, helperData);
	}
	return discoveredMap;
    }
	    

    /**
     * For every NAS Resource selected we fill the NAS Information Map
     *
     * @param  Map  of selectedFSResources
     */
    private void fillNASInfoMap(Map selectedFSResources, HashMap helperData) {
        Map nasMap = null;
        Map resourceGroupMap = null;
        Set selectedResources = selectedFSResources.keySet();
        String selectedResourcesArr[] = (String[]) selectedResources.toArray(
                new String[0]);

	racModel = RACInvokerCreator.getRACModel();
        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);

        for (int i = 0; i < selectedResourcesArr.length; i++) {
            String fsInfoStr = (String) selectedFSResources.get(
                    selectedResourcesArr[i]);
            String fsInfoStrArr[] = fsInfoStr.split(",");
            String fsType = fsInfoStrArr[2];

            // Filesystem Type should be of type NAS
            if (fsType.equals(Util.NAS)) {
                nasMap = new HashMap();

                // Mountpoint
                nasMap.put(Util.MOUNT_POINT, fsInfoStrArr[1]);

                // Exported Filesystem
                nasMap.put(Util.FILE_SYSTEM_NAME, fsInfoStrArr[0]);
                nasMap.put(Util.FILE_SYSTEM_TYPE, Util.NAS);

                // Get the resource group for the NAS Resource
                if (resourceGroupMap == null) {
                    String propertyName[] = new String[] { Util.STORAGEGROUP };
		    helperData.put(Util.RS_NAME, selectedResourcesArr[i]);
                    resourceGroupMap = (HashMap) racModel.discoverPossibilities(
			null, nodeEndPoint, propertyName, helperData);
                }

                nasMap.putAll(resourceGroupMap);
                wizardModel.setWizardValue(selectedResourcesArr[i], nasMap);
            }
        }
    }

    /**
     * For every QFS Resource selected we fill the QFS Information Map
     *
     * @param  Map  of selectedFSResources
     */
    private void fillQFSInfoMap(Map selectedFSResources, HashMap helperData) {

        Set selectedResources = selectedFSResources.keySet();

        Map resourceGroupMap = null;
        Map metaserverGroup = null;
        String selectedResourcesArr[] = (String[]) selectedResources.toArray(
                new String[0]);

	racModel = RACInvokerCreator.getRACModel();
        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);
	boolean bWaitZCRead = false;

        for (int i = 0; i < selectedResourcesArr.length; i++) {
            String fsInfoStr = (String) selectedFSResources.get(
                    selectedResourcesArr[i]);

            String fsInfoStrArr[] = fsInfoStr.split(",");
            String fsType = fsInfoStrArr[2];

            // Filesystem Type should be of type qfs
            if (fsType.equals(Util.SQFS)) {

		if (!bWaitZCRead) {
		    // this needs to be done only once since there is only one
		    // wait zc boot resource per ZC.
		    Map waitZCBootInfo = getWaitZCInformation(helperData);
		    if (waitZCBootInfo != null && !waitZCBootInfo.isEmpty()) {
			wizardModel.setWizardValue(Util.WAIT_ZC_BOOT_RESOURCE,
			    (String)waitZCBootInfo.get(Util.WAIT_ZC_BOOT_RESOURCE));
			wizardModel.setWizardValue(Util.WAIT_ZC_BOOT_GROUP,
			    (String)waitZCBootInfo.get(Util.WAIT_ZC_BOOT_GROUP));
		    }
		    bWaitZCRead = true;
		}

                // For every qfs resource(new/old) get the information
                Map qfsInfo = getQFSInformation(fsInfoStrArr, helperData);

                // Get the resource group for the QFS Resource
                String propertyName[] = new String[] { Util.STORAGEGROUP };

                if (resourceGroupMap == null) {
		    helperData.put(Util.RS_NAME, selectedResourcesArr[i]);
                    resourceGroupMap = (HashMap) racModel.discoverPossibilities(
			null, nodeEndPoint, propertyName, helperData);
                }

                qfsInfo.putAll(resourceGroupMap);

                // Get the resource group for the QFS Metaserver Resource
                if (metaserverGroup == null) {
		    helperData.put(Util.RS_NAME, qfsInfo.get(Util.METASERVER_RESOURCE));
                    metaserverGroup = (HashMap) racModel.discoverPossibilities(
			null, nodeEndPoint, propertyName, helperData);
                }

                qfsInfo.put(Util.METASERVER_GROUP,
                    metaserverGroup.get(Util.STORAGEGROUP));
                qfsInfo.put(Util.MOUNT_POINT, fsInfoStrArr[1]);
                qfsInfo.put(Util.FILE_SYSTEM_TYPE, Util.SQFS);
                qfsInfo.put(Util.FILE_SYSTEM_NAME, fsInfoStrArr[0]);

                wizardModel.setWizardValue(selectedResourcesArr[i], qfsInfo);
            }
        }
    }

    // Get a unique name for a resource for Scalable Mount Point
    private String getResourceName_forScalMountPoint(String mountPoint, HashMap helperData) {

        String propertyName[] = new String[] { Util.STORAGERESOURCE };
	racModel = RACInvokerCreator.getRACModel();
        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);

	helperData.put(Util.PREFIX, mountPoint);
        Map discoveredMap = racModel.discoverPossibilities(
	    null, nodeEndPoint, propertyName, helperData);

        return (String) discoveredMap.get(Util.STORAGERESOURCE);
    }
}
