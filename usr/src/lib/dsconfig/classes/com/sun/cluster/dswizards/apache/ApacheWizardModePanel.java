/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ApacheWizardModePanel.java 1.10     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.apache;

// CLI Wizard SDK
import com.sun.cluster.agent.dataservices.apache.Apache;

// CMASS
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;

// Wizard Common
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;


/**
 * Panel to retrieve Apache Mode
 */
public class ApacheWizardModePanel extends WizardLeaf {


    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N messages; // Message Bundle

    /**
     * Creates a new instance of ApacheWizardModePanel
     */
    public ApacheWizardModePanel() {
        super();
    }

    /**
     * Creates an ApacheWizardMode Panel with the given name and tree manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public ApacheWizardModePanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates an ApacheWizardMode Panel with the given name and associates it
     * with the WizardModel, WizardFlow and i18n reference
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     * @param  messages  WizardI18N
     */
    public ApacheWizardModePanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N messages) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;

        // Get the I18N instance
        this.messages = messages;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {

        TTYDisplay.clearScreen();
        TTYDisplay.clear(1);

        // Enable Back and Help
        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        // Title
        TTYDisplay.printTitle(messages.getString("apache.modePanel.title"));
        TTYDisplay.clear(1);

        // Get modes
        String failoverMode = messages.getString("apache.modePanel.failover");
        String scalableMode = messages.getString("apache.modePanel.scalable");
        String apacheModes[] = new String[] { failoverMode, scalableMode };
        int defaultIndex = 0;

        // Process history
        String prevSelectedMode = (String) wizardModel.getWizardValue(
                Util.APACHE_MODE);

        if ((prevSelectedMode != null) &&
                prevSelectedMode.equals(Apache.SCALABLE)) {
            defaultIndex = 1;
        }

        // Get the Help Text
        String helpText = messages.getString("apache.modePanel.desc") +
            "\n \n" + messages.getString("apache.modePanel.help.1") + "\n \n" +
            messages.getString("apache.modePanel.help.2");

        int index = TTYDisplay.getMenuOption(messages.getString(
                    "apache.modePanel.query"), apacheModes, defaultIndex,
                helpText);

        if (index == -1) {
            this.cancelDirection = true;

            return;
        }

        // Set the mode in the wizard model
        if (apacheModes[index].equals(scalableMode)) {
            wizardModel.setWizardValue(Util.APACHE_MODE, Apache.SCALABLE);
            wizardModel.setWizardValue(Util.RG_MODE, Util.SCALABLE_RG_MODE);
        } else {
            wizardModel.setWizardValue(Util.APACHE_MODE, Apache.FAILOVER);
            wizardModel.setWizardValue(Util.RG_MODE, null);
        }
    }
}
