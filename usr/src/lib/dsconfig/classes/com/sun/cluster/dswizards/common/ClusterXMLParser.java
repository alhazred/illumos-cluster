/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ClusterXMLParser.java 1.13     08/05/20 SMI"
 */

package com.sun.cluster.dswizards.common;

import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;

import java.io.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.ListIterator;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;


public class ClusterXMLParser extends DefaultHandler {
    public static String END_KEYWORD = "END";
    public static String INCLUDE_CHAR = "#";
    public static String START_SUBPANEL = ":";
    public static String END_SUBPANEL = "@";
    LinkedList xmlPanels;
    XmlPanel zPanel = null;
    HashMap nameAndPanels = new HashMap();
    DSWizardInterface flowLogicClass = null;

    String flowClassName = null;
    String jarFilePath = null;

    StringBuffer textBuffer;
    String startPanel = null;
    String endPanel = null;
    boolean add2List;
    int subpanel = 0;


    // ===========================================================
    // SAX DocumentHandler methods
    // ===========================================================

    public ClusterXMLParser(LinkedList xPanels) {
        xmlPanels = xPanels;
        add2List = true;
        subpanel = 0;
    }

    public ClusterXMLParser(LinkedList xPanels, String start, String end,
        int subLevel) {
        xmlPanels = xPanels;
        subpanel = subLevel;
        add2List = true;

        if (start != null) {
            start = start.trim();

            if (start.length() > 0)
                startPanel = start;
        }

        if (end != null) {
            end = end.trim();

            if (end.length() > 0)
                endPanel = end;
        }

        if ((startPanel != null) || (endPanel != null))
            add2List = false;
    }

    public void startDocument() throws SAXException {
    }

    public void endDocument() throws SAXException {
    }

    public void startElement(String namespaceURI, String sName, // simple name
        String qName, // qualified name
        Attributes attrs) throws SAXException {
        String eName = sName; // element name

        if ("".equals(eName))
            eName = qName; // not namespaceAware

        if (eName.equals("wizardflow")) {

            if (attrs != null) {

                for (int i = 0; i < attrs.getLength(); i++) {
                    String aName = attrs.getLocalName(i); // Attr name

                    if ("".equals(aName))
                        aName = attrs.getQName(i);

                    String val = attrs.getValue(i);

                    if (aName.equals("class")) {
                        flowClassName = val;
                    }

                    if (aName.equals("jarFile")) {
                        jarFilePath = val;
                    }
                }
            }
        }

        if (eName.equals("panel")) {

            if (attrs != null) {
                zPanel = new XmlPanel();
                zPanel.isSubPanel = subpanel;

                for (int i = 0; i < attrs.getLength(); i++) {
                    String aName = attrs.getLocalName(i); // Attr name

                    if ("".equals(aName))
                        aName = attrs.getQName(i);

                    String val = attrs.getValue(i);

                    if (aName.equals("name")) {
                        val = val.trim();

                        if (startPanel != null) {

                            if (startPanel.equals(val.trim())) {
                                add2List = true;
                            } else {

                                if (!add2List) {
                                    break;
                                }
                            }
                        }

                        zPanel.name = val;
                    }

                    if (aName.equals("next")) {
                        String nextStr[] = val.split(",");
                        zPanel.nextNames = new String[nextStr.length];
                        zPanel.nextPanels = new LinkedHashMap(nextStr.length);

                        for (int j = 0; j < nextStr.length; j++) {
                            zPanel.nextNames[j] = nextStr[j].trim();

                            if (zPanel.nextNames[j].startsWith(INCLUDE_CHAR)) {
                                zPanel.nextPanels.put((Object)
                                    zPanel.nextNames[j],
                                    (Object) handleInclude(
                                        zPanel.nextNames[j]));
                            }
                        }
                    }

                    if (aName.equals("subpanel")) {
                        val = val.trim();

                        if (val.compareToIgnoreCase("true") == 0) {
                            zPanel.isSubPanel++;
                        }
                    }
                }
            }
        }
    }

    LinkedList handleInclude(String incStr) {
        String fname[] = incStr.split(START_SUBPANEL);
        String fileN = fname[0].substring(1).trim();
        String constraints[] = fname[1].split(END_SUBPANEL);
        LinkedList xmlPanels = new LinkedList();

        ClusterXMLParser handler;
        handler = new ClusterXMLParser(xmlPanels, constraints[0],
                constraints[1], subpanel + 1);

        SAXParserFactory factory = SAXParserFactory.newInstance();

        try {
            SAXParser saxParser = factory.newSAXParser();
            saxParser.parse(new File(fileN), (DefaultHandler) handler);

        } catch (Throwable t) {
            t.printStackTrace();
        }

        return xmlPanels;
    }


    public void endElement(String namespaceURI, String sName, // simple name
        String qName /* qualified name */)
        throws SAXException {
        String eName = sName; // element name

        if ("".equals(eName))
            eName = qName; // not namespaceAware

        if (eName.equals("panel")) {

            if (zPanel != null) {

                if (flowLogicClass == null) {
                    Class curClass;
                    URL urlArr[] = new URL[1];

                    try {

                        if (jarFilePath != null) {
                            urlArr[0] = new URL("jar:file:" + jarFilePath +
                                    "!/");

                            URLClassLoader myClassLoader = URLClassLoader
                                .newInstance(urlArr,
                                    this.getClass().getClassLoader());
                            curClass = myClassLoader.loadClass(flowClassName);
                        } else {
                            curClass = this.getClass().getClassLoader()
                                .loadClass(flowClassName);
                        }

                        flowLogicClass = (DSWizardInterface) curClass
                            .newInstance();
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    } catch (InstantiationException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                }

                zPanel.flowClass = flowLogicClass;

                if (add2List)
                    xmlPanels.add(zPanel);

                if ((endPanel != null) && endPanel.equals(zPanel.name)) {
                    add2List = false;
                }
            }
        }
    }

}
