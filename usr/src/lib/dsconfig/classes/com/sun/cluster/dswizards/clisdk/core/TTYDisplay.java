/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)TTYDisplay.java	1.57	08/07/14 SMI"
 */

package com.sun.cluster.dswizards.clisdk.core;

import com.sun.cluster.agent.dataservices.utils.DataServicesUtil;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.dswizards.common.WizardI18N;

import java.io.*;

import java.text.BreakIterator;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * The TTYDisplay class provides an abstract interface to write text data to the
 * user console, and receive responses to user queries.
 *
 * <p>Each display should provide methods for getting the necessary information
 * via the TTYDisplay in environments when a GUI is not available
 */
public class TTYDisplay {

    /** Constant used with <code>setNavOption()</code> */
    public static final int BACK_OPTION = 0;

    /** Constant used with <code>setNavOption()</code> */
    public static final int EXIT_OPTION = 1;

    /** The tree manager for exit, back, etc. */
    private static transient WizardTreeManager treeManager = null;

    /** What user should type in to go back */
    public static String backOption = "<";

    /** What user should type in to exit - Control-D */
    private static String exitOption = null;

    /**
     * If the back button was pressed, this int will be returned from
     * queryValue. This can be queried for in the panel. Navigating back in TTY
     * mode via TTY is very convoluted and doesnt work This way the panel that
     * calls queryValue for something should check for the return value and call
     * tree manager back button pressed
     */
    public static int BACK_PRESSED = -1;

    /** Whether we are providing navigation or not */
    private static boolean navEnabled = false;

    /** Whether the input is echoed or not */
    private static boolean echo = true;

    /** where to write output */
    private static PrintWriter outWriter;

    /** Where to read input */
    private static BufferedReader inReader;

    /** Resource bundle object. */
    private static WizardI18N wizardi18n = null;

    /** Resource bundle name */
    private static String resourceBundleName =
        "com.sun.cluster.dswizards.common.WizardResource";

    /** CMASS Library Name */
    private static String LIBRARY =
        "/usr/cluster/lib/cmass/libcmas_agent_dataservices.so";

    /** the width of the terminal, in characters */
    private static int termWidth = 90;
    private static final int LINES = 20;
    private static final int INDENT = 4;
    private static final int INDENT_TAGS = 8;

    private static final String DOTS = "...";
    private static int COLWIDTH = 20;
    private static int MAXWIDTH = 38;

    /** The amount of apdding on a table entry */
    public static final int TABLE_PADDING = 2;
    public static final String NO_DEFAULT = "There is no default value";
    private static Vector displayedHelp;
    private static boolean blockingRead = true;
    private static InputStream defaultInputStream = System.in;
    private static OutputStream defaultOutputStream = System.out;
    private static int defaultTimeOut = 15;

    /** The attributes that can be added to the text. */
    private static String addAttr[] = new String[] {
            "<bold>", "<b>", "<italics>", "<i>", "<tt>", "<br>", "<p>"
        };

    /** The delimiters according to which the entered options would get split */
    private static String delimiters[] = new String[] { " ", "," };

    // These are bit representations of the index offset of each of
    // the attributes.
    // "Flow Control Only" attributes are marked in "attrFlowControlOnly" -
    // they affect text flow and are removed immediately after first use
    private static int attrBold = (0x01 | 0x02);
    private static int attrItalics = (0x04 | 0x08);
    private static int attrTt = (0x10);
    private static int attrBreak = (0x20);
    private static int attrParagraph = (0x40);

    private static String startTitleDecor = null;
    private static String endTitleDecor = null;

    /** The attributes that can be removed from the text. */
    private static String removeAttr[] = new String[] {
            "</bold>", "</b>", "</italics>", "</i>", "</tt>", "</br>", "</p>"
        };

    /*
     * Object variables:
     */
    private boolean performBlockingReads = false;
    private InputStream inStream = null;
    private OutputStream outStream = null;

    // private BufferedReader bReader = null;
    private String defval = null;
    private int timeOut = 0;

    /**
     * Constructor to create a TTYDisplay object.
     */
    public TTYDisplay(InputStream in, OutputStream out, boolean blocking) {
        this.inStream = in;
        this.outStream = out;
        this.performBlockingReads = blocking;
        // this.bReader = createBufferedReader(this.inStream);
    }

    public void setTimeOut(int time) {
        this.timeOut = time;
    }

    public int getTimeOut() {
        return (this.timeOut);
    }

    public static void setDefaultTimeOut(int time) {
        TTYDisplay.defaultTimeOut = time;
    }

    public static int getDefaultTimeOut() {
        return (TTYDisplay.defaultTimeOut);
    }

    public void setDefaultValue(String value) {
        this.defval = value;
    }

    public String getDefaultValue() {
        return (this.defval);
    }

    public static void setDefaultBlocking(boolean blocking) {
        TTYDisplay.blockingRead = blocking;
    }

    public static boolean getDefaultBlocking() {
        return (TTYDisplay.blockingRead);
    }

    private LineReader getLineReader() {

        if (this.performBlockingReads) {
            return (new BlockingLineReader(this.inReader, this.defval));
        } else {
            return (new NonblockingLineReader(this.inStream, this.inReader,
                        this.defval, this.timeOut));
        }
    }

    /**
     * This method return a copy of the TTYDisplay object for non-static method
     * reference from static method.
     */
    private static TTYDisplay getTTYDisplay() {
        TTYDisplay tty = new TTYDisplay(TTYDisplay.defaultInputStream,
                TTYDisplay.defaultOutputStream, TTYDisplay.blockingRead);

        tty.setTimeOut(TTYDisplay.getDefaultTimeOut());

        return (tty);
    }

    /**
     * Constructs a TTYDisplay using System.in and System.out and a width of 80
     * characters
     */
    public static void initialize() {
        initialize(System.in, System.out, termWidth);
    }

    /**
     * Constructs a TTYDisplay given a width using System.in and System.out
     *
     * @param  width  The width of the conosle, in characters
     */
    public static void initialize(int width) {
        initialize(System.in, System.out, width);
    }

    /**
     * Constructs a TTYDisplay given input and output and width
     *
     * @param  inStream  Where to read data from
     * @param  outStream  Where to write data to
     * @param  width  The width of the conosle, in characters
     */
    public static void initialize(InputStream inStream, OutputStream outStream,
        int width) {
        outWriter = new PrintWriter(outStream, true);
        inReader = new BufferedReader(new InputStreamReader(inStream));
        termWidth = width;
        displayedHelp = new Vector();

        wizardi18n = new WizardI18N(resourceBundleName);

        startTitleDecor = " " +
            wizardi18n.getString("cliwizards.startTitleDecor") + " ";
        endTitleDecor = " " + wizardi18n.getString("cliwizards.endTitleDecor") +
            " ";

        // Load library for Clear
        Runtime.getRuntime().load(LIBRARY);
    }

    /**
     * Allows this TTYDisplay class to manage wizard state in addition to
     * providing helper methods. You must call this with a valid
     * WizardTreeManager if you want to provide back/exit functionality.
     *
     * @param  mgr  The WizardTreeManager for this session
     */
    public static void setWizardTreeManager(WizardTreeManager mgr) {
        TTYDisplay.treeManager = mgr;
    }

    /**
     * Gets the current tree manager that TTY back/exit functionality will use.
     *
     * @return  the current tree manager that TTY back/exit functionality will
     * use.
     */
    public static WizardTreeManager getWizardTreeManager() {
        return TTYDisplay.treeManager;
    }

    /**
     * Allows this class to provide primitive navigation functions whenever a
     * <code>queryValue()</code> is called.
     *
     * @param  enabled  If true, each time a prompt is issued, the buttonMask of
     * the current panel is queried and an optional back and/or exit option is
     * appended to the possible query options.  The string used for each option
     * can be set with the <code>setNavOption()</code> method.
     */
    public static void setNavEnabled(boolean enabled) {
        TTYDisplay.navEnabled = enabled;
    }

    /**
     * Gets the current navigation setting for this class.
     *
     * @return  true if this class is providing navigation capabilites.
     */
    public static boolean getNavEnabled() {
        return TTYDisplay.navEnabled;
    }

    /**
     * Allow to change the echo status.
     *
     * @param  echo  If true, the input will be echoed on the console. If false,
     * the input charactor will be masked with a blank space.
     */
    public static void setEcho(boolean echo) {
        TTYDisplay.echo = echo;
    }

    /**
     * Gets the current echo setting for this class.
     *
     * @return  true if this class is in the echo mode.
     */
    public boolean getEcho() {
        return TTYDisplay.echo;
    }

    /**
     * Allows you to change the string used to prompt the user for navigation
     * (back/exit, etc).
     *
     * @param  which  Which type of prompt string to change.  BACK_OPTION or
     * EXIT_OPTION.
     * @param  option  The new string to use when prompting. Normally this is a
     * single character, but could be longer.
     */
    public static void setNavOption(int which, String option) {

        switch (which) {

        case BACK_OPTION:
            TTYDisplay.backOption = option;

            break;

        case EXIT_OPTION:
            TTYDisplay.exitOption = option;

            break;

        default:
            throw new IllegalArgumentException("TTYDIsplay: invalid type for " +
                "setNavOption");
        }
    }

    /**
     * Gets the current string used to prompt for navigation options.
     *
     * @param  Which  type of prompt string to query for.  BACK_OPTION or
     * EXIT_OPTION.
     *
     * @return  The string to use when prompting
     */
    public String getNavOption(int which) {
        String ret = null;

        switch (which) {

        case BACK_OPTION:
            ret = TTYDisplay.backOption;

            break;

        case EXIT_OPTION:
            ret = TTYDisplay.exitOption;

            break;

        default:
            throw new IllegalArgumentException("TTYDIsplay: invalid type " +
                "for setNavOption");
        }

        return ret;
    }

    /**
     * Clears the screen by scrolling N lines, where N is the height of the
     * terminal
     *
     * @param  lines  the number of lines to clear
     */
    public static void clear(int lines) {

        for (int i = 0; i < lines; i++) {
            System.out.println("");
        }
    }

    /**
     * Clears the screen using the 'clear' command
     */
    public static void clearScreen() {
	DataServicesUtil.clearScreen();
    }

    /**
     * Prints a Confirmation Message on the screen with two options and
     * retrieves the option entered by the user. The function would keep asking
     * the user to enter any one of the options until he enters one.
     *
     * @param  message  The Confirmation Message to be displayed
     * @param  option1  Option 1, This is the Default Option
     * @param  option2  Option 2, This is the Second Option
     */
    public static String getConfirmation(String message, String option1,
        String option2) {

        String entry = null;

        while (true) {
            entry = TTYDisplay.queryValue(message + " (" + option1 + "/" +
                    option2 + ")?   ", option1, null, INDENT);

            if (entry.equals("<")) {
                return entry;
            }

            if (entry.equalsIgnoreCase(option1) ||
                    entry.equalsIgnoreCase(option2)) {
                return entry;
            } else {
                clear(1);
                message = "Please Enter";
            }
        }
    }

    /**
     * Clears the screen by scrolling N lines, where N is the height of the
     * terminal
     */
    public static void clear() {
        clear(LINES);
    }

    /**
     * Calls pageText(String, int) with default space of 4.
     */
    public static void pageText(String text) {
        pageText(text, INDENT);
    }

    /**
     * Shows lots of text, a page at a time (lines should be '\n'-separated
     *
     * @param  text  The text to show, '\n'-separated.
     */
    public static void pageText(String text, int ident) {
        boolean done = false;
        int currentLine = 0;
        StringTokenizer st = new StringTokenizer(text, "\n");
        int total = st.countTokens();

        while (!done) {

            if (!st.hasMoreTokens()) {
                done = true;
            } else {
                String nextLine = st.nextToken();
                showText(nextLine, ident);
                currentLine++;

                if ((currentLine % LINES) == 0) {
                    int percent = (int) (((double) currentLine /
                                (double) total) * 100.0);

                    if (promptForNextLine(percent, ident).equalsIgnoreCase(
                                wizardi18n.getString("ttydisplay.n.char"))) {
                        done = true;
                    }
                }
            }
        }
    }

    /**
     * Method declaration
     *
     * @param  percent
     *
     * @return
     *
     * @see
     */
    private static String promptForNextLine(int percent, int ident) {
        showNewline();

        String nval = wizardi18n.getString("ttydisplay.n.char");
        Object arg[] = { "" + percent, nval };
        String result = getTTYDisplay().promptAndQuery(" <" +
                wizardi18n.getString("ttydisplay.page.continue", arg) + ">", "",
                ident);
        showNewline();

        return result;
    }

    /**
     * A method to display text to the screen.  It will format it to nicely fit
     * the available space
     *
     * @param  text  the text to show
     */
    public static void showText(String text) {
        showText(text, 0);
    }

    /**
     * Method declaration
     *
     * @param  text
     * @param  indent
     *
     * @see
     */
    public static void showText(String text, int indent) {
        Vector textVector = createTextVector(text);
        Enumeration textObjects = textVector.elements();

        while (textObjects.hasMoreElements()) {
            String theText = ((String) textObjects.nextElement());

            if (theText == "") {
                showNewline();
            } else {
                prettyPrintText(theText, true, indent);
            }
        }
    }

    /**
     * Prints the Error Text
     *
     * @param  text  Error Text
     */
    public static void printError(String text) {
        clear(1);
        showText(wizardi18n.getString("ttydisplay.error") + text, 13);
        clear(1);
    }

    /**
     * Prints the Info Text
     *
     * @param  text  Info Text
     */
    public static void printInfo(String text) {
        clear(1);
        showText(wizardi18n.getString("ttydisplay.info") + text, 13);
        clear(1);
    }

    /**
     * Prints the Warning Text
     *
     * @param  text  Warning Text
     */
    public static void printWarning(String text) {
        clear(1);
        showText(wizardi18n.getString("ttydisplay.warn") + text, 13);
        clear(1);
    }

    /**
     * Prints the Text
     *
     * @param  text  Text
     */
    public static void printMoreText(String text) {
        showText(text, 13);
    }

    /**
     * Prints the Text with two indent spaces
     *
     * @param  text  Title Text
     */
    public static void printTitle(String text) {
        clearScreen();
        clear(1);

        String title = startTitleDecor + text + endTitleDecor;
        showText(title, 2);
        clear(1);
    }

    /**
     * Prints the Text with four indent spaces
     *
     * @param  text  Subtitle Text
     */
    public static void printSubTitle(String text) {
        showText(text, INDENT);
        clear(1);
    }

    /**
     * Prints a list of items with a title
     *
     * @param  title  Optional Title For the List.
     * @param  items  String Array of Items of the List.
     */
    public static void printList(String title, String items[]) {

        if (title != null)
            showText(title, INDENT);

        clear(1);

        for (int i = 0; i < items.length; i++) {
            showText(items[i], 8);
        }
    }

    /**
     * Prints a text and spawns a thread with the busy symbol
     */
    public static Thread busy(String text) {
        prettyPrintText(text, false, INDENT);

        BusyThread t = new BusyThread();

        return t;
    }

    /**
     * Prints a prompts and retrives user entered information
     *
     * @param  message  Prompt message
     *
     * @return  String User entered value
     */
    public static String promptForEntry(String message) {
        return TTYDisplay.queryValue(message, "", INDENT);
    }

    /**
     * Prints a prompts and retrives user entered information
     *
     * @param  message  Prompt message
     *
     * @return  String User entered value
     */
    public static String promptForEntry(String message, String help) {
        return TTYDisplay.queryValue(message, help, INDENT);
    }

    /**
     * Prints a prompts and retrives user entered information
     *
     * @param  message  Prompt message
     *
     * @return  String User entered value
     */
    public static String promptForEntry(String message, String defSelection,
        String help) {
        return getTTYDisplay().promptAndQuery(message, defSelection, help,
                INDENT);
    }

    public static int getMenuOption(String title, String optionArr[],
        int defIndex, String help) {
        return queryValue(wizardi18n.getString("ttydisplay.msg.option"), title,
                optionArr, defIndex, help, INDENT);
    }

    /**
     * Prints a menu from the given array and gets user entered option. The
     * default option is mentioned by the defaultIndex.
     *
     * @param  title  Title of the option menu.
     * @param  optionArray  User Entered Array
     * @param  defIndex  Default option's index in the array.
     *
     * @return  user selected option
     */
    public static int getMenuOption(String title, String optionArr[],
        int defIndex) {
        return queryValue(wizardi18n.getString("ttydisplay.msg.option"), title,
                optionArr, defIndex, "", INDENT);
    }

    public static List getScrollingOptions(String title, String optionArr[],
        HashMap customTags, String defSelections, boolean inputRequired) {
        return getScrollingOptions(title, optionArr, customTags, defSelections,
                null, inputRequired);
    }

    

    public static void displaySubheadings(String subheadings[]) {
        String text = "";
        int colsize = 19;

	if (subheadings != null) {
	    for (int i = 0; i < subheadings.length; i++) {
		int j = 0;

		if (i != 0) {
		    colsize = getColWidth(subheadings);
		}

		int spaces_to_print = colsize - subheadings[i].length();

		while (j < spaces_to_print) {
		    text += " ";
		    j++;
		}

		text += subheadings[i];
	    }

	    text += "\n";

	    colsize = 19;

	    for (int i = 0; i < subheadings.length; i++) {
		int len = subheadings[i].length();

		if (i != 0) {
		    colsize = getColWidth(subheadings);
		}

		int spaces_to_print = colsize - len;

		for (int j = 0; j < spaces_to_print; j++) {
		    text += " ";
		}

		for (int j = 0; j < len; j++) {
		    text += "=";
		}
	    }

	    text += "\n\n";

	    pageText(text);
	}
    }

    private static int getOptionIndentation(int i) {

        if (i < 10) {
            return INDENT + 4;
        }

        return INDENT + 3;
    }

    /**
     * Prints a menu from the given array and gets use entered option The user
     * can select more than one option. A "next >" would be displayed if there
     * are more than ten options.
     *
     * @param  title  Title of the option menu
     * @param  subHeadings  subheadings used for the menu
     * @param  vector  containing the date
     * @param  HashMap  of custom tags
     * @param  defSelections  comma separated default Selections
     * @param  inputRequired  boolean indicating if the selectionList can be
     * empty
     *
     * @return  Array of user selected options
     */

    public static List getScrollingOptions(String title, String subheadings[],
        Vector data, HashMap customTags, String defSelections, String help,
        boolean inputRequired) {

        boolean errFlag = true;
        boolean nextFlag = false;
        boolean prevFlag = false;

        boolean prevClicked = false;
        boolean nextClicked = false;

        List selectedOptions = new ArrayList();
        String enteredOptions[] = null;
        int option;
        Set keySet = null;
        Iterator iterator = null;

        TTYDisplay.pageText(title);
        TTYDisplay.showNewline();
        displaySubheadings(subheadings);

        int lineNum = 0;
        int size = data.size();
        boolean displayLineNum = true;
        int colsize = getColWidth(subheadings);
        int i = 0;
        String optionName = null;

        // add the default selections passed by the user to selectedOptions
        if ((defSelections != null) && (defSelections.length() > 0)) {
            String selections[] = defSelections.split(",");

            for (i = 0; i < selections.length; i++) {

                if (!selectedOptions.contains(selections[i])) {
                    selectedOptions.add(selections[i]);
                }
            }
        }

        i = 0;

        while (true) {

            if (i < 0) { // i cannot be negative
                i = 0;
            }

            if (lineNum < 0) { // LineNumber cannot be negative
                lineNum = 0;
            }

            lineNum++;

            prevClicked = false;
            nextClicked = false;

            String dataStr = null;
            String out = null;

            if ((size > 0) && (i < size)) {

                for (int j = 0; j < subheadings.length; j++) {

                    if (j == 0) {
                        dataStr = (String) data.elementAt(i);
                        out = lineNum + ") " + dataStr;
                    } else {
                        int spaces_to_print = colsize - dataStr.length();

                        for (int k = 0; k < spaces_to_print; k++) {
                            out += " ";
                        }

                        dataStr = (String) data.elementAt(i);
                        out += dataStr;

                    }

                    if (j == (subheadings.length - 1)) {
                        showText(out, getOptionIndentation(lineNum));
                    }

                    i++;
                }
            }

            // Whether to display "next >", default percentage is 10
            nextFlag = ((lineNum % 10) == 0) && (i < size);
            prevFlag = (lineNum > 10);

            if ((nextFlag) || (i == size) || (size == 0)) {
                showNewline();

                if (customTags != null) {
                    keySet = customTags.keySet();
                    iterator = keySet.iterator();

                    while (iterator.hasNext()) {
                        String key = (String) iterator.next();
                        showText((String) customTags.get(key), INDENT_TAGS);
                    }
                }

                // Display the Next button
                if (nextFlag) {
                    showText(wizardi18n.getString("ttydisplay.menu.next"),
                        INDENT_TAGS);
                }

                // Display the prev button
                if (prevFlag) {
                    showText(wizardi18n.getString("ttydisplay.menu.prev"),
                        INDENT_TAGS);
                }

                showText(wizardi18n.getString("ttydisplay.menu.help"),
                    INDENT_TAGS);

                showText(wizardi18n.getString("ttydisplay.menu.done"),
                    INDENT_TAGS);

                // Get the Options from the user and process them
                while (errFlag && !(prevClicked || nextClicked)) {
                    showNewline();

                    if (selectedOptions.size() > 0) {
                        showText(wizardi18n.getString("ttydisplay.msg.selected",
                                new String[] { selectedOptions.toString() }),
                            INDENT);
                    }

                    while (true) {
                        String result = queryValue(wizardi18n.getString(
                                    "ttydisplay.msg.options"), help, INDENT);

                        // Process Options and Return
                        enteredOptions = getSelectedOptionsAr(result);

                        if (inputRequired && (enteredOptions.length == 0)) {
                            printError(wizardi18n.getString(
                                    "ttydisplay.menu.invalidOption",
                                    new Object[] { result }));
                        } else {
                            break;
                        }
                    }

                    showNewline();

                    if (
                        enteredOptions[0].equalsIgnoreCase(
                                wizardi18n.getString("ttydisplay.d.char")) ||
                            (enteredOptions[0].length() == 0)) {

                        if (inputRequired && (selectedOptions.size() == 0)) {

                            // stay here
                            if (nextFlag || ((lineNum % 10) == 0)) {
                                i -= (subheadings.length * 10);
                                lineNum -= 10;
                            } else {
                                int rem = lineNum % 10;
                                i -= (subheadings.length * rem);
                                lineNum -= rem;
                            }
                        } else {
                            return selectedOptions;
                        }
                    }

                    if (enteredOptions[0].equalsIgnoreCase(
                                wizardi18n.getString("ttydisplay.back.char"))) {
                        return null;
                    }

                    for (int j = 0; j < enteredOptions.length; j++) {

                        if (enteredOptions[j].equalsIgnoreCase(
                                    wizardi18n.getString(
                                        "ttydisplay.n.char"))) {

                            if (!nextFlag) {
                                printError("Invalid Option : " +
                                    enteredOptions[j]);
                                errFlag = true;

                                break;
                            }

                            // Handle a next request
                            nextClicked = true;
                            displaySubheadings(subheadings);

                            break;
                        } else if (enteredOptions[j].equalsIgnoreCase(
                                    wizardi18n.getString(
                                        "ttydisplay.d.char"))) {

                            if (inputRequired &&
                                    (selectedOptions.size() == 0)) {

                                // stay here
                                printError(wizardi18n.getString(
                                        "ttydisplay.menu.error.noneSelected"));

                                if (nextFlag || ((lineNum % 10) == 0)) {
                                    i -= (subheadings.length * 10);
                                    lineNum -= 10;
                                } else {
                                    int rem = lineNum % 10;
                                    i -= (subheadings.length * rem);
                                    lineNum -= rem;
                                }
                            } else {
                                return selectedOptions;
                            }
                        } else if (enteredOptions[j].equalsIgnoreCase(
                                    wizardi18n.getString(
                                        "ttydisplay.p.char"))) {

                            if (!prevFlag) {
                                printError("Invalid Option : " +
                                    enteredOptions[j]);
                                errFlag = true;

                                break;
                            }

                            prevClicked = true;

                            if (nextFlag || ((lineNum % 10) == 0)) {
                                i -= (subheadings.length * 20);
                                lineNum -= 20;
                            } else {
                                int rem = lineNum % 10;
                                i -= (subheadings.length * 10);
                                i -= (subheadings.length * rem);
                                lineNum -= 10;
                                lineNum -= rem;
                            }

                            displaySubheadings(subheadings);

                            break;
                        } else {

                            // Each option should only be in numbers or
                            // keys in the customTags
                            try {
                                option = Integer.valueOf(enteredOptions[j])
                                    .intValue();
                            } catch (NumberFormatException nfe) {

                                // user entered a string or char
                                if (customTags != null) {

                                    if (
                                        !customTags.containsKey(
                                                enteredOptions[j])) {
                                        printError("Invalid Option : " +
                                            enteredOptions[j]);
                                        errFlag = true;

                                        break;
                                    } else {
                                        String str = (String) customTags.get(
                                                enteredOptions[j]);
                                        selectedOptions.clear();
                                        selectedOptions.add(str);

                                        return selectedOptions;
                                    }
                                } else {
                                    printError(wizardi18n.getString(
                                            "ttydisplay.menu.invalidOption",
                                            new Object[] {
                                                enteredOptions[j]
                                            }));
                                    errFlag = true;

                                    break;
                                }
                            }

                            int max = size / subheadings.length;

                            if ((option <= 0) || (option > max)) {
                                printError(wizardi18n.getString(
                                        "ttydisplay.menu.outOfRange",
                                        new Object[] {
                                            String.valueOf(option)
                                        }));
                                errFlag = true;

                                break;
                            } else {
                                optionName = (String) data.elementAt(
                                        subheadings.length * (option - 1));

                                if (!(selectedOptions.contains(optionName))) {
                                    selectedOptions.add(optionName);
                                } else {
                                    selectedOptions.remove(selectedOptions
                                        .indexOf(optionName));
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static List getScrollingOptions(String title, String optionArr[],
        HashMap customTags, List selectedOptions, String help,
        boolean inputRequired) {
	String[] selectionsArr =  null;
	String defSelections = null;
	if (selectedOptions != null) {
	    selectionsArr = (String []) selectedOptions.toArray(new String[0]);
	    defSelections = Util.convertArraytoString(selectionsArr);
	}
	return getScrollingOptions(
	    title, optionArr, customTags, defSelections, help, inputRequired);
    }


    /**
     * Prints a menu from the given array and gets use entered option The user
     * can select more than one option. A "next >" would be displayed if there
     * are more than ten options.
     *
     * @param  title  Title of the option menu
     * @param  optionArr  User Entered Array
     * @param  HashMap  of custom tags
     * @param  defSelections  comma separated default Selections
     * @param  inputRequired  boolean indicating if the selectionList can be
     * empty
     *
     * @return  Array of user selected options
     */
    public static List getScrollingOptions(String title, String optionArr[],
        HashMap customTags, String defSelections, String help,
        boolean inputRequired) {

        boolean errFlag = true;
        boolean nextFlag = false;
        boolean prevFlag = false;

        boolean prevClicked = false;
        boolean nextClicked = false;

        int count = 0; // count of elements on each page

        List selectedOptions = new ArrayList();
        String enteredOptions[] = null;
        int i;
        int option;
        Set keySet = null;
        Iterator iterator = null;
        showNewline();

        // add the default selections passed by the user to selectedOptions
        boolean isDefSelectionsCleared = false;

        if ((defSelections != null) && (defSelections.length() > 0)) {
            String selections[] = defSelections.split(",");

            for (i = 0; i < selections.length; i++) {

                if (!selectedOptions.contains(selections[i])) {
                    selectedOptions.add(selections[i]);
                }
            }
        }

        if ((title != null) && !title.equals("")) {
            showText(title, INDENT);
            showNewline();
        }

        i = 1;

        while (true) {
            count = 0;

            prevClicked = false;
            nextClicked = false;

            if ((optionArr.length != 0) && (i < (optionArr.length + 1))) {
                showText(i + ") " + optionArr[i - 1], getOptionIndentation(i));
            }

            // Whether to display "next >", default percentage is 10
            nextFlag = ((i % 10) == 0);
            prevFlag = (i > 10);

            if ((nextFlag) || (i == optionArr.length) ||
                    (optionArr.length == 0)) {
                showNewline();

                if (customTags != null) {
                    keySet = customTags.keySet();
                    iterator = keySet.iterator();

                    while (iterator.hasNext()) {
                        String key = (String) iterator.next();
                        showText((String) customTags.get(key), INDENT_TAGS);
                    }
                }

                // Display the Next button
                if (nextFlag) {
                    showText(wizardi18n.getString("ttydisplay.menu.next"),
                        INDENT_TAGS);
                }

                // Display the prev button
                if (prevFlag) {
                    showText(wizardi18n.getString("ttydisplay.menu.prev"),
                        INDENT_TAGS);
                }

                showText(wizardi18n.getString("ttydisplay.menu.help"),
                    INDENT_TAGS);

                showText(wizardi18n.getString("ttydisplay.menu.done"),
                    INDENT_TAGS);

                // Get the Options from the user and process them
                while (errFlag && !(prevClicked || nextClicked)) {
                    showNewline();

                    if (selectedOptions.size() > 0) {
                        showText(wizardi18n.getString("ttydisplay.msg.selected",
                                new String[] { selectedOptions.toString() }),
                            INDENT);
                    }

                    while (true) {
                        String result = queryValue(wizardi18n.getString(
                                    "ttydisplay.msg.options"), help, INDENT);

                        enteredOptions = getSelectedOptionsAr(result);

                        if (inputRequired && (enteredOptions.length == 0)) {
                            printError(wizardi18n.getString(
                                    "ttydisplay.menu.invalidOption",
                                    new Object[] { result }));
                        } else {
                            break;
                        }
                    }

                    // If user presses 'return' and if there are default
                    // selections we return the default selections
                    if (
                        enteredOptions[0].equalsIgnoreCase(
                                wizardi18n.getString("ttydisplay.d.char")) ||
                            (enteredOptions[0].length() == 0)) {

                        if (inputRequired && (selectedOptions.size() == 0)) {

                            // stay here
                            if (nextFlag) {
                                i -= 10;
                            } else {
                                int rem = i % 10;
                                i -= rem;
                            }
                        } else {
                            return selectedOptions;
                        }
                    }

                    if (enteredOptions[0].equalsIgnoreCase(
                                wizardi18n.getString("ttydisplay.back.char"))) {
                        return null;
                    }

                    for (int j = 0; j < enteredOptions.length; j++) {

                        if (enteredOptions[j].equalsIgnoreCase(
                                    wizardi18n.getString(
                                        "ttydisplay.n.char"))) {

                            if (!nextFlag) {
                                printError("Invalid Option : " +
                                    enteredOptions[j]);
                                errFlag = true;

                                break;
                            }

                            // Handle a next request
                            nextClicked = true;

                            break;
                        } else if (enteredOptions[j].equalsIgnoreCase(
                                    wizardi18n.getString(
                                        "ttydisplay.d.char"))) {

                            if (inputRequired &&
                                    (selectedOptions.size() == 0)) {

                                // stay here
                                if (nextFlag) {
                                    i -= 10;
                                } else {
                                    int rem = i % 10;
                                    i -= rem;
                                }
                            } else {
                                return selectedOptions;
                            }
                        } else if (enteredOptions[j].equalsIgnoreCase(
                                    wizardi18n.getString(
                                        "ttydisplay.p.char"))) {

                            if (!prevFlag) {
                                printError("Invalid Option : " +
                                    enteredOptions[j]);
                                errFlag = true;

                                break;
                            }

                            prevClicked = true;

                            if (nextFlag) {
                                i -= 20;
                            } else {
                                int rem = i % 10;
                                i -= 10;
                                i -= rem;
                            }

                            break;
                        } else {

                            // Each option should only be in numbers
                            // or keys in the customTags
                            try {
                                option = Integer.valueOf(enteredOptions[j])
                                    .intValue();
                            } catch (NumberFormatException nfe) {

                                // user entered a string or char
                                if (customTags != null) {

                                    if (
                                        !customTags.containsKey(
                                                enteredOptions[j])) {
                                        printError("Invalid Option : " +
                                            enteredOptions[j]);
                                        errFlag = true;

                                        break;
                                    } else {
                                        String str = (String) customTags.get(
                                                enteredOptions[j]);

                                        if (!(selectedOptions.contains(str))) {
                                            selectedOptions.add(str);
                                        }

                                        return selectedOptions;
                                    }
                                } else {
                                    printError(wizardi18n.getString(
                                            "ttydisplay.menu.invalidOption",
                                            new Object[] {
                                                enteredOptions[j]
                                            }));
                                    errFlag = true;

                                    break;
                                }
                            }

                            if ((option <= 0) || (option > optionArr.length)) {
                                printError(wizardi18n.getString(
                                        "ttydisplay.menu.outOfRange",
                                        new Object[] {
                                            String.valueOf(option)
                                        }));
                                errFlag = true;

                                break;
                            } else {

                                // Clear up the default selections
                                if (!isDefSelectionsCleared) {
                                    selectedOptions.removeAll(selectedOptions);
                                    isDefSelectionsCleared = true;
                                }

                                if (!(selectedOptions.contains(
                                                optionArr[option - 1]))) {
                                    selectedOptions.add(optionArr[option - 1]);
                                } else {
                                    selectedOptions.remove(selectedOptions
                                        .indexOf(optionArr[option - 1]));
                                }
                            }
                        }
                    }
                }
            }

            i++;
        }
    }

    /**
     * Prints a menu from the given array and gets use entered option The user
     * can select only one option. A "next >" would be displayed if there are
     * more than ten options.
     *
     * @param  title  Title of the option menu
     * @param  optionArray  User Entered Array
     * @param  HashMap  of custom tags
     * @param  defSelections  comma separated default Selections
     * @param  inputRequired  boolean indicating if the selectionList can be
     * empty
     *
     * @return  Array of user selected options
     */

    public static List getScrollingOption(String title, String optionArr[],
        HashMap customTags, String defSelections, String help,
        boolean inputRequired) {

        boolean errFlag = true;
        boolean nextFlag = false;
        boolean prevFlag = false;

        boolean prevClicked = false;
        boolean nextClicked = false;

        int count = 0; // count of elements on each page

        List selectedOptions = new ArrayList();
        String enteredOptions[] = null;
        int i;
        int option = 0;
        Set keySet = null;
        Iterator iterator = null;

        String selections[] = null;

        // add the default selections passed by the user to selectedOptions
        if ((defSelections != null) && (defSelections.length() > 0)) {
            selections = defSelections.split(",");

            for (i = 0; i < selections.length; i++) {

                if (!selectedOptions.contains(selections[i])) {
                    selectedOptions.add(selections[i]);
                }
            }
        }

        if ((title != null) && !title.equals("")) {
            showText(title, INDENT);
            showNewline();
        }

        i = 1;

        while (true) {
            count = 0;

            prevClicked = false;
            nextClicked = false;

            if ((optionArr.length != 0) && (i < (optionArr.length + 1))) {
                showText(i + ") " + optionArr[i - 1], getOptionIndentation(i));
            }

            // Whether to display "next >", default percentage is 10
            nextFlag = ((i % 10) == 0);
            prevFlag = (i > 10);

            if ((nextFlag) || (i == optionArr.length) ||
                    (optionArr.length == 0)) {
                showNewline();

                if (customTags != null) {
                    keySet = customTags.keySet();
                    iterator = keySet.iterator();

                    while (iterator.hasNext()) {
                        String key = (String) iterator.next();
                        showText((String) customTags.get(key), INDENT_TAGS);
                    }
                }

                // Display the Next button
                if (nextFlag) {
                    showText(wizardi18n.getString("ttydisplay.menu.next"),
                        INDENT_TAGS);
                }

                // Display the prev button
                if (prevFlag) {
                    showText(wizardi18n.getString("ttydisplay.menu.prev"),
                        INDENT_TAGS);
                }

                showText(wizardi18n.getString("ttydisplay.menu.help"),
                    INDENT_TAGS);

                showText(wizardi18n.getString("ttydisplay.menu.done"),
                    INDENT_TAGS);

                // Get the Options from the user and process them
                while (errFlag && !(prevClicked || nextClicked)) {
                    showNewline();

                    if (selectedOptions.size() > 0) {
                        showText(wizardi18n.getString("ttydisplay.msg.selected",
                                new String[] { selectedOptions.toString() }),
                            INDENT);
                    }

                    while (true) {
                        String result = queryValue(wizardi18n.getString(
                                    "ttydisplay.msg.option"), help, INDENT);

                        // Process Options and Return
                        enteredOptions = getSelectedOptionsAr(result);

                        if (inputRequired && (enteredOptions.length == 0)) {
                            printError(wizardi18n.getString(
                                    "ttydisplay.menu.invalidOption",
                                    new Object[] { result }));
                        } else {
                            break;
                        }
                    }

                    if (enteredOptions.length > 1) {
                        printError(wizardi18n.getString(
                                "ttydisplay.menu.error.moreSelected"));
                        errFlag = true;

                        continue;
                    }

                    if (
                        enteredOptions[0].equalsIgnoreCase(
                                wizardi18n.getString("ttydisplay.d.char")) ||
                            (enteredOptions[0].length() == 0)) {

                        if (inputRequired && (selectedOptions.size() == 0)) {

                            // stay here
                            if (nextFlag) {
                                i -= 10;
                            } else {
                                int rem = i % 10;
                                i -= rem;
                            }

                            printError(wizardi18n.getString(
                                    "ttydisplay.menu.error.noneSelected"));
                            errFlag = true;

                            continue;
                        } else {
                            return selectedOptions;
                        }

                    }

                    if (enteredOptions[0].equalsIgnoreCase(
                                wizardi18n.getString("ttydisplay.back.char"))) {
                        return null;
                    }

                    int j = 0;

                    if (enteredOptions[j].equalsIgnoreCase(
                                wizardi18n.getString("ttydisplay.n.char"))) {

                        if (!nextFlag) {
                            printError("Invalid Option : " + enteredOptions[j]);
                            errFlag = true;
                        }

                        // Handle a next request
                        nextClicked = true;
                    } else if (enteredOptions[j].equalsIgnoreCase(
                                wizardi18n.getString("ttydisplay.p.char"))) {

                        if (!prevFlag) {
                            printError("Invalid Option : " + enteredOptions[j]);
                            errFlag = true;
                        }

                        prevClicked = true;

                        if (nextFlag) {
                            i -= 20;
                        } else {
                            int rem = i % 10;
                            i -= 10;
                            i -= rem;
                        }
                    } else {

                        // Each option should only be in numbers or
                        // keys in the customTags
                        try {
                            option = Integer.valueOf(enteredOptions[j])
                                .intValue();
                        } catch (NumberFormatException nfe) {

                            // user entered a string or char
                            if (customTags != null) {

                                if (!customTags.containsKey(
                                            enteredOptions[j])) {
                                    printError("Invalid Option : " +
                                        enteredOptions[j]);
                                    errFlag = true;

                                    continue;
                                } else {
                                    String str = (String) customTags.get(
                                            enteredOptions[j]);

                                    if (!(selectedOptions.contains(str))) {
                                        selectedOptions.clear();
                                        selectedOptions.add(str);

                                        return selectedOptions;
                                    }

                                    continue;
                                }
                            } else {
                                printError(wizardi18n.getString(
                                        "ttydisplay.menu.invalidOption",
                                        new Object[] { enteredOptions[j] }));
                                errFlag = true;

                                continue;
                            }
                        }

                        if ((option <= 0) || (option > optionArr.length)) {
                            printError(wizardi18n.getString(
                                    "ttydisplay.menu.outOfRange",
                                    new Object[] { String.valueOf(option) }));
                            errFlag = true;
                        } else {

                            if (!(selectedOptions.contains(
                                            optionArr[option - 1]))) {
                                selectedOptions.clear();
                                selectedOptions.add(optionArr[option - 1]);

                                return selectedOptions;
                            }
                        }
                    }
                }
            }

            i++;
        }
    }

    /**
     * Creates a 2*2 table from a Vector with a title and the specified
     * subheadings
     */
    public static String create2DTable(String title, String subheadings[],
        Vector data, HashMap customTags, String help) {
        TTYDisplay.pageText(title);
        TTYDisplay.showNewline();
        displaySubheadings(subheadings);

        String out = new String();

        int size = data.size();
        boolean displayLineNum = true;
        int lineNum = 1;
        int colsize = getColWidth(subheadings);

        for (int i = 0; i < size; i++) {
            String dataStr = (String) data.elementAt(i);

            if (displayLineNum) {
                int indent = getOptionIndentation(lineNum) - INDENT;
                String spaces = "";

                for (int s = 0; s < indent; s++) {
                    spaces += " ";
                }

                dataStr = spaces + lineNum + ") " + dataStr;
                displayLineNum = false;
                lineNum++;

                int spaces_to_print = (colsize + indent) - dataStr.length();

                if (spaces_to_print <= 0) {

                    // truncate characters from dataStr and add ...
                    dataStr = dataStr.substring(0, (colsize + indent) - 3);
                    dataStr += DOTS;
                }

                for (int j = 0; j < spaces_to_print; j++) {
                    dataStr += " ";
                }
            } else {

                if (dataStr.length() > MAXWIDTH) {
                    dataStr = dataStr.substring(0, (MAXWIDTH - 3));
                    dataStr += DOTS;
                }

                displayLineNum = true;
            }

            out += dataStr;

            if (displayLineNum) {
                out += "\n";
            }
        }

        String spaces = "";

        for (int s = 0; s < INDENT; s++) {
            spaces += " ";
        }

        // Display custom tags if any
        if (customTags != null) {
            out += "\n \n";

            Set keySet = customTags.keySet();
            Iterator iterator = keySet.iterator();

            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                out += spaces + (String) customTags.get(key) + "\n";
            }
        }

        // display help tag
        out += spaces + wizardi18n.getString("ttydisplay.menu.help") + "\n";

        out += "\n \n";

        pageText(out);

        while (true) {

            // Get the option from the user
            String option = TTYDisplay.queryValue(wizardi18n.getString(
                        "ttydisplay.msg.option"), help, INDENT);

            if (option.equals(wizardi18n.getString("ttydisplay.back.char"))) {
                return option;
            }

            if (option.equals("")) {
                String dchar = wizardi18n.getString("ttydisplay.d.char");

                if (customTags.containsKey(dchar))
                    option = dchar;
            }

            // Validate the option
            try {
                int num = Integer.valueOf(option).intValue();

                if ((num <= 0) || (num > (lineNum - 1))) {
                    printError("Invalid Option : " + option);
                } else {
                    return option;
                }
            } catch (NumberFormatException nfe) {

                // user entered a string or char
                if (customTags != null) {

                    if (!customTags.containsKey(option)) {
                        printError("Invalid Option : " + option);
                    } else {
                        return option;
                    }
                } else {
                    printError("Invalid Option : " + option);
                }
            }
        }
    }

    /**
     * Creates a n*n table from a Vector with a title and the specified
     * subheadings. User can enter multiple options, the validated options are
     * returned in a String Array. If a single option is required set multiple
     * to false. The customTags are treated as action buttons and the UI returns
     * the custom tag option along with the selectedValues This function adds
     * one more parameter defSelections to the existing function.
     */
    public static String[] create2DTable(String title, String subheadings[],
        Vector data, int numRows, int numCols, HashMap customTags, String help,
        String emptyText, boolean single, List defSelections) {

        TTYDisplay.pageText(title);
        TTYDisplay.showNewline();
        displaySubheadings(subheadings);
        TTYDisplay.showNewline();

        String out = new String();
        int size = data.size();
        boolean displayLineNum = true;
        int rowCount = 1;
        int colsize = getColWidth(subheadings);
        int colCount = 1;
        String option;

        for (int i = 0; i < size; i++) {
            String dataStr = (String) data.elementAt(i);

            if (colCount > numCols) {
                colCount = 1;
                displayLineNum = true;
                out += "\n";
            }

            if (displayLineNum) {
                int indent = getOptionIndentation(rowCount) - INDENT;
                String spaces = "";

                for (int s = 0; s < indent; s++) {
                    spaces += " ";
                }

                // Five Spaces for proper indent with the headings
                dataStr = spaces + rowCount + ") " + dataStr;
                displayLineNum = false;
                rowCount++;

                int spaces_to_print = (colsize + indent) - dataStr.length();

                if (spaces_to_print <= 0) {

                    // truncate characters from dataStr and add ...
                    dataStr = dataStr.substring(0, (colsize + indent) - 3);
                    dataStr += DOTS;
                }

                for (int j = 0; j < spaces_to_print; j++) {
                    dataStr += " ";
                }
            } else {

                if (colCount < numCols) {
                    int spaces_to_print = (colsize) - dataStr.length();

                    if (spaces_to_print <= 0) {

                        // truncate characters from dataStr and add ...
                        dataStr = dataStr.substring(0, (colsize) - 3);
                        dataStr += DOTS;
                    }

                    for (int j = 0; j < spaces_to_print; j++) {
                        dataStr += " ";
                    }
                } else {

                    if (dataStr.length() > MAXWIDTH) {
                        dataStr = dataStr.substring(0, (MAXWIDTH - 3));
                        dataStr += DOTS;
                    }
                }
            }

            out += dataStr;
            colCount++;
        }

        // If the vector had no elements print the empty message
        if (size == 0) {
            out += "\n \n";
            out += emptyText;
        }

        String spaces = "";

        for (int s = 0; s < INDENT; s++) {
            spaces += " ";
        }

        boolean helpTagPresent = false;

        // Display custom tags if any
        if (customTags != null) {
            out += "\n \n";

            Set keySet = customTags.keySet();
            Iterator iterator = keySet.iterator();

            while (iterator.hasNext()) {
                String key = (String) iterator.next();

                if (key.equals("?") || key.equals("?)")) {
                    helpTagPresent = true;
                }

                out += spaces + (String) customTags.get(key) + "\n";
            }
        }

        if (!helpTagPresent) {
            out += spaces + wizardi18n.getString("ttydisplay.menu.help") + "\n";
        }

        out += "\n \n";

        // If we use pageText, its trunctaing the columns > 80. As a workaround
        // we increase termWidth to 160 adnd revert back
        int oldtermWidth = termWidth;
        termWidth = 160;
        pageText(out);
        termWidth = oldtermWidth;

        List selectedOptions = new ArrayList();

        // add the default selections passed by the user to selectedOptions
        if (defSelections != null) {
            int sel_size = defSelections.size();

            for (int i = 0; i < sel_size; i++) {
                String elem = (String) defSelections.get(i);

                if (!selectedOptions.contains(elem)) {
                    selectedOptions.add(elem);
                }
            }
        }

        boolean exit = false;

        do {

            // Show already selected options
            if (selectedOptions.size() > 0) {
                showText("Selected : " + selectedOptions, INDENT);
            }

            // Get the option from the user
            if (!single) {
                option = TTYDisplay.queryValue("Option(s) : ", help, INDENT);
            } else {
                option = TTYDisplay.queryValue("Option : ", help, INDENT);
            }

            if (option.equals(wizardi18n.getString("ttydisplay.back.char"))) {
                return null;
            }


            if (option.equals("") && !selectedOptions.isEmpty()) {
                String dchar = wizardi18n.getString("ttydisplay.d.char");

                if (customTags.containsKey(dchar))
                    option = dchar;
                else
                    exit = true;
            }

            String options[] = getSelectedOptionsAr(option);

            if (options.length == 0) {
                printError(wizardi18n.getString("ttydisplay.menu.invalidOption",
                        new Object[] { option }));

                continue;
            }

            // Take only the first option if not multiple
            if (single && (options.length > 1)) {
                printError(wizardi18n.getString(
                        "ttydisplay.menu.error.moreSelected"));

                continue;
            }

            // Validate the option
            for (int i = 0; i < options.length; i++) {

                try {
                    int num = Integer.valueOf(options[i]).intValue();

                    if ((num <= 0) || (num > numRows)) {
                        printError("Invalid Option : " + options[i]);

                        break;
                    }
                } catch (NumberFormatException nfe) {

                    // user entered a string or char
                    if (customTags != null) {

                        if (!customTags.containsKey(options[i])) {

                            if (!options[i].equals("")) {
                                printError("Invalid Option : " + options[i]);
                            }

                            break;
                        }
                    } else {
                        printError("Invalid Option : " + options[i]);

                        break;
                    }
                }

                if (!selectedOptions.contains(options[i])) {
                    selectedOptions.add(options[i]);
                } else if (!single) {
                    selectedOptions.remove(selectedOptions.indexOf(options[i]));
                }

                // If selectedOptions has any of the custom tags
                // (or) only single option is allowed quit
                if (single || customTags.keySet().contains(options[i])) {
                    exit = true;
                }
            }
        } while (!exit);

        return (String[]) selectedOptions.toArray(new String[0]);
    }

    /**
     * Creates a n*n table from a Vector with a title and the specified
     * subheadings. User can enter multiple options, the validated options are
     * returned in a String Array. If a single option is required set multiple
     * to false. The customTags are treated as action buttons and the UI returns
     * the custom tag option along with the selectedValues
     */
    public static String[] create2DTable(String title, String subheadings[],
        Vector data, int numRows, int numCols, HashMap customTags, String help,
        String emptyText, boolean single) {

        TTYDisplay.pageText(title);
        TTYDisplay.showNewline();
        displaySubheadings(subheadings);
        TTYDisplay.showNewline();

        String out = new String();
        int size = data.size();
        boolean displayLineNum = true;
        int rowCount = 1;
        int colsize = getColWidth(subheadings);
        int colCount = 1;
        String option;

        for (int i = 0; i < size; i++) {
            String dataStr = (String) data.elementAt(i);

            if (colCount > numCols) {
                colCount = 1;
                displayLineNum = true;
                out += "\n";
            }

            if (displayLineNum) {
                int indent = getOptionIndentation(rowCount) - INDENT;
                String spaces = "";

                for (int s = 0; s < indent; s++) {
                    spaces += " ";
                }

                // Five Spaces for proper indent with the headings
                dataStr = spaces + rowCount + ") " + dataStr;
                displayLineNum = false;
                rowCount++;

                int spaces_to_print = (colsize + indent) - dataStr.length();

                if (spaces_to_print <= 0) {

                    // truncate characters from dataStr and add ...
                    dataStr = dataStr.substring(0, (colsize + indent) - 3);
                    dataStr += DOTS;
                }

                for (int j = 0; j < spaces_to_print; j++) {
                    dataStr += " ";
                }
            } else {

                if (colCount < numCols) {
                    int spaces_to_print = (colsize) - dataStr.length();

                    if (spaces_to_print <= 0) {

                        // truncate characters from dataStr and add ...
                        dataStr = dataStr.substring(0, (colsize) - 3);
                        dataStr += DOTS;
                    }

                    for (int j = 0; j < spaces_to_print; j++) {
                        dataStr += " ";
                    }
                } else {

                    if (dataStr.length() > MAXWIDTH) {
                        dataStr = dataStr.substring(0, (MAXWIDTH - 3));
                        dataStr += DOTS;
                    }
                }
            }

            out += dataStr;
            colCount++;
        }

        // If the vector had no elements print the empty message
        if (size == 0) {
            out += "\n \n";
            out += emptyText;
        }

        String spaces = "";

        for (int s = 0; s < INDENT; s++) {
            spaces += " ";
        }

        boolean helpTagPresent = false;

        // Display custom tags if any
        if ((customTags != null) && (size > 0)) {
            out += "\n \n";

            Set keySet = customTags.keySet();
            Iterator iterator = keySet.iterator();

            while (iterator.hasNext()) {
                String key = (String) iterator.next();

                if (key.equals("?") || key.equals("?)")) {
                    helpTagPresent = true;
                }

                out += spaces + (String) customTags.get(key) + "\n";
            }
        }

        if (!helpTagPresent) {
            out += spaces + wizardi18n.getString("ttydisplay.menu.help") + "\n";
        }

        out += "\n \n";

        // If we use pageText, its trunctaing the columns > 80. As a workaround
        // we increase termWidth to 160 adnd revert back
        int oldtermWidth = termWidth;
        termWidth = 160;
        pageText(out);
        termWidth = oldtermWidth;

        List selectedOptions = new ArrayList();
        boolean exit = false;

        do {

            // Show already selected options
            if (selectedOptions.size() > 0) {
                showText("Selected : " + selectedOptions, INDENT);
            }

            // Get the option from the user
            if (!single) {
                option = TTYDisplay.queryValue("Option(s) : ", help, INDENT);
            } else {
                option = TTYDisplay.queryValue("Option : ", help, INDENT);
            }

            if (option.equals(wizardi18n.getString("ttydisplay.back.char"))) {
                return null;
            }

            String options[] = getSelectedOptionsAr(option);

            if (options.length == 0) {
                printError(wizardi18n.getString("ttydisplay.menu.invalidOption",
                        new Object[] { option }));

                continue;
            }

            // Take only the first option if not multiple
            if (single) {
                String value = options[0];
                options = new String[] { value };
            }

            // Validate the option
            for (int i = 0; i < options.length; i++) {

                try {
                    int num = Integer.valueOf(options[i]).intValue();

                    if ((num <= 0) || (num > numRows)) {
                        printError("Invalid Option : " + options[i]);

                        break;
                    }
                } catch (NumberFormatException nfe) {

                    // user entered a string or char
                    if (customTags != null) {

                        if (!customTags.containsKey(options[i])) {
                            printError("Invalid Option : " + options[i]);

                            break;
                        }
                    } else {
                        printError("Invalid Option : " + options[i]);

                        break;
                    }
                }

                if (!selectedOptions.contains(options[i])) {
                    selectedOptions.add(options[i]);
                } else if (!single) {
                    selectedOptions.remove(selectedOptions.indexOf(options[i]));
                }

                // If selectedOptions has any of the custom tags
                // (or) only single option is allowed quit
                if (single || customTags.keySet().contains(options[i])) {
                    exit = true;
                }
            }
        } while (!exit);

        return (String[]) selectedOptions.toArray(new String[0]);
    }

    /**
     * Creates and Displays a 2x2 table from a Map. Numbers each row and asks
     * user for a selection
     *
     * @param  data  The Map that contains the data
     *
     * @return  option Selected by user
     */
    public static int create2DTable(Vector data) {
        String out = new String();
        String actualData[][];
        int numRows = data.size() / 2;
        int numCols = 2;
        int lineNo = 1;
        boolean flag = true;

        actualData = new String[numRows][numCols];

        int colLens[] = new int[numCols];

        for (int i = 0; i < colLens.length; i++) {
            colLens[i] = 0;
        }

        int rowCount = 0;
        int colCount = 0;

        for (Iterator e = data.iterator(); e.hasNext();) {

            String str = (String) e.next();
            colCount = 0;
            actualData[rowCount][colCount] = str;

            if (str.length() > colLens[colCount]) {
                colLens[colCount] = str.length();
            }

            colCount = 1;
            str = (String) e.next();
            actualData[rowCount][colCount] = str;

            if (str.length() > colLens[colCount]) {
                colLens[colCount] = str.length();
            }

            rowCount++;
        }

        int totalLen = 0;

        for (int i = 0; i < colLens.length; i++) {
            totalLen += colLens[i] + TABLE_PADDING;
        }

        while (totalLen > termWidth) {
            colLens[arrayLargest(colLens)]--;
            totalLen--;
        }

        for (int i = 0; i < actualData.length; i++) {

            for (int k = 0; k < actualData[0].length; k++) {
                int maxLen = colLens[k];
                String dataStr = actualData[i][k];
                int strLen = dataStr.length();

                if (strLen > maxLen) {

                    /*
                     * The line of code truncates the string to term
                     * width which is wrong. It should let long strings be
                     * wrapped. This creates serious issues if what is
                     * displayed is component or product names and they
                     * get truncated...
                     */
                }

                if (strLen < maxLen) {

                    for (int l = strLen; l < maxLen; l++) {
                        dataStr += " ";
                    }
                }

                if (flag) {
                    dataStr = "     " + lineNo + ") " + dataStr + " ";
                    lineNo++;
                    flag = false;
                } else {
                    dataStr = " " + dataStr + " ";
                    flag = true;
                }

                out += dataStr;
            }

            out += "\n \n";
        }

        pageText(out);

        while (true) {

            // Get the option from the user
            String option = TTYDisplay.promptForEntry(wizardi18n.getString(
                        "ttydisplay.msg.option"));

            if (option.equals(wizardi18n.getString("ttydisplay.back.char"))) {
                return -1;
            }

            // Validate the option
            try {
                int num = Integer.valueOf(option).intValue();

                if ((num <= 0) || (num > (lineNo - 1))) {
                    printError("Invalid Option : " + option);
                } else {
                    return num;
                }
            } catch (NumberFormatException nfe) {
                printError("Invalid Option : " + option);
            }
        }
    }

    /**
     * A method to dump directly to the screen
     */
    public static void print(String text) {
        outWriter.print(text);
        outWriter.flush();
    }

    /**
     * A method to display new line to the screen.
     */
    public static void showNewline() {
        prettyPrintText("", true, 0);
    }

    /**
     * A method to display a query to the screen and store the result with a
     * given name
     *
     * @param  query  the text to show
     * @param  name  the name of the value to store it in
     *
     * @return  the value read from the user
     */
    public static String queryValue(String prompt, String help, int ident) {
        return (getTTYDisplay().promptAndQuery(prompt, help, ident));
    }

    /**
     * A method to display a query to the screen and store the result with a
     * given name
     *
     * @param  query  the text to show
     * @param  name  the name of the value to store it in
     *
     * @return  the value read from the user
     */
    public String promptAndQuery(String prompt, String help, int ident) {
        return promptAndQuery(prompt, NO_DEFAULT, help, ident);
    }

    /**
     * A method to display a query to the screen that must return integers
     * within a certain range
     *
     * @param  text  the text to show
     * @param  defval  the default value (or Integer.MAX for none)
     * @param  min  the minimum value
     * @param  max  the maximum value
     * @param  name  the name of the value to store it in
     *
     * @return  the value read from the user
     */
    public static int queryValue(String prompt, int defval, int min, int max,
        String help, int ident) {
        return (getTTYDisplay().promptAndQuery(prompt, defval, min, max, help,
                    ident));
    }

    /**
     * A method to display a query to the screen that must return integers
     * within a certain range
     *
     * @param  text  the text to show
     * @param  defval  the default value (or Integer.MAX for none)
     * @param  min  the minimum value
     * @param  max  the maximum value
     * @param  name  the name of the value to store it in
     *
     * @return  the value read from the user
     */
    public int promptAndQuery(String prompt, int defval, int min, int max,
        String help, int ident) {

        while (true) {
            String value = "";

            value = promptAndQuery(prompt, "" + defval, help, ident);

            if ((value != null) && value.equalsIgnoreCase(backOption)) {
                return BACK_PRESSED;
            }

            try {
                int intval = Integer.parseInt(value);

                if (intval == BACK_PRESSED) {
                    return BACK_PRESSED;
                }

                if ((intval >= min) && (intval <= max)) {
                    return intval;
                }
            } catch (NumberFormatException ne) {
            }

            Object arg[] = new Object[] {
                    String.valueOf(min), String.valueOf(max)
                };

            showNewline();

            // Bug#4758632
            if ((max - min) > 1) {
                showText(wizardi18n.getString("ttydisplay.cli.error1", arg));
            } else {
                showText(wizardi18n.getString("ttydisplay.cli.error2", arg));
            }

            showNewline();
        }
    }

    /**
     * Method declaration
     *
     * @param  prompt
     * @param  defval
     * @param  vals
     * @param  help
     *
     * @return
     *
     * @see
     */
    public static String queryValue(String prompt, String defval, String vals[],
        String help, int ident) {
        return (getTTYDisplay().promptAndQuery(prompt, defval, vals, help,
                    ident));
    }

    /**
     * Method declaration
     *
     * @param  prompt
     * @param  defval
     * @param  vals
     * @param  help
     *
     * @return
     *
     * @see
     */
    public String promptAndQuery(String prompt, String defval, String vals[],
        String help, int ident) {
        return (promptAndQuery(prompt, defval, vals, true, help, ident));
    }

    /**
     * Method declaration
     *
     * @param  prompt
     * @param  defval
     * @param  vals
     * @param  ignoreCase
     * @param  help
     *
     * @return
     *
     * @see
     */
    public static String queryValue(String prompt, String defval, String vals[],
        boolean ignoreCase, String help, int ident) {
        return (getTTYDisplay().promptAndQuery(prompt, defval, vals, ignoreCase,
                    help, ident));
    }

    /**
     * Method declaration
     *
     * @param  prompt
     * @param  defval
     * @param  vals
     * @param  ignoreCase
     * @param  help
     *
     * @return
     *
     * @see
     */
    public String promptAndQuery(String prompt, String defval, String vals[],
        boolean ignoreCase, String help, int ident) {

        while (true) {
            String result = "";

            result = promptAndQuery(prompt, defval, help, ident);

            // Handle BACK button pressed
            if ((result != null) && result.equalsIgnoreCase(backOption)) {
                return backOption;
            }

            if (vals.length == 1) {
                return vals[0];
            }

            for (int i = 0; i < vals.length; i++) {

                if (ignoreCase) {

                    if (result.equalsIgnoreCase(vals[i])) {
                        return vals[i];
                    }
                } else {

                    if (result.equals(vals[i])) {
                        return vals[i];
                    }
                }
            }

            String arg1 = "";

            for (int k = 0; k < (vals.length - 2); k++) {
                arg1 += "'" + vals[k] + "' ";
            }

            arg1 += "'" + vals[vals.length - 2] + "'";

            String arg2 = "'" + vals[vals.length - 1] + "'";
            Object arg[] = new Object[] { arg1, arg2 };
            String out = wizardi18n.getString("ttydisplay.cli.error1", arg);

            showNewline();
            showText(out, ident);
            showNewline();
        }
    }

    /**
     * Method declaration
     *
     * @param  prompt
     * @param  desc
     * @param  options
     * @param  defval
     * @param  help
     *
     * @return
     *
     * @see
     */
    public static int queryValue(String prompt, String desc, String options[],
        int defval, String help, int ident) {
        return (getTTYDisplay().promptAndQuery(prompt, desc, options, defval,
                    help, ident));
    }

    /**
     * Method declaration
     *
     * @param  prompt
     * @param  desc
     * @param  options
     * @param  defval
     * @param  help
     *
     * @return
     *
     * @see
     */
    public int promptAndQuery(String prompt, String desc, String options[],
        int defval, String help, int ident) {

        if ((desc != null) && !desc.equals("")) {
            showText(desc, ident);
            showNewline();
        }

        for (int i = 0; i < options.length; i++) {
            showText((i + 1) + ") " + options[i], getOptionIndentation(i));
        }

        showNewline();

        int resnum = promptAndQuery(prompt, (defval + 1), 1, options.length,
                help, ident);

        if (resnum == BACK_PRESSED) {
            return BACK_PRESSED;
        }

        return (resnum - 1);
    }

    /**
     * Method declaration
     *
     * @param  prompt
     * @param  defval
     * @param  help
     * @param  callback
     * @param  id
     *
     * @return
     *
     * @see
     */
    public static String queryValue(String prompt, String defval, String help,
        WizardComponent callback, String id, int ident) {
        return (getTTYDisplay().promptAndQuery(prompt, defval, help, callback,
                    id, ident));
    }

    public String promptAndQuery(String prompt, String defval, String help,
        WizardComponent callback, String id, int ident) {

        while (true) {
            String result = promptAndQuery(prompt, defval, help, ident);

            if (callback.callback(result, id)) {
                return result;
            }
        }
    }

    /**
     * A method to display a query to the screen and store the result with a
     * given name, and defaulting to a certain value
     *
     * @param  query  the text to show
     * @param  default  the default value of name
     * @param  name  the name of the value to store it in
     *
     * @return  the value read from the user
     */
    public static String queryValue(String text, String defval, String help,
        int ident) {
        return (getTTYDisplay().promptAndQuery(text, defval, help, ident));
    }

    /**
     * A method to display a query to the screen and store the result with a
     * given name, and defaulting to a certain value
     *
     * @param  query  the text to show
     * @param  default  the default value of name
     * @param  name  the name of the value to store it in
     *
     * @return  the value read from the user
     */
    public String promptAndQuery(String text, String defval, String help,
        int ident) {
        String new_text = null;
        boolean displayHelp = true;

        if ((help == null) || (help.trim().length() == 0)) {
            displayHelp = false;
            help = wizardi18n.getString("ttydisplay.cli.error3");
        }

        String qmark = wizardi18n.getString("ttydisplay.cli.q");

        // if (defval == null) {
        // defval = "";
        // }

        while (true) {
            String defstr = "";

            // According to UIRB feedback, if there is no default value
            // we should not show empty brackets - [] - it should be omitted.
            // Empty brackets - [] - is reserved for the case that the default
            // value is an empty string...
            if (defval == null) {
                defstr = "";
            } else if (defval.equals("")) {
                defstr = " []";
            } else if (!defval.equals(NO_DEFAULT)) {
                defstr = " [" + defval + "]";
            }

            if (text.trim().endsWith("?")) {
                new_text = text.trim().substring(0, text.trim().length() - 1);
                defstr += "?  ";
            } else if (text.trim().endsWith(":")) {
                new_text = text.trim().substring(0, text.trim().length() - 1);
                defstr += ":  ";
            } else {
                new_text = text;

                if (!defstr.equals("")) {
                    defstr += "  ";
                }
            }

            prettyPrintText(new_text + defstr, false, ident);

            try {
                setDefaultValue(defval);

                String outval;

                if (getEcho()) {
                    outval = getLineReader().readLine();
                } else {
                    outval = getLineReader().readMaskedLine();
                }

                if (outval != null) {

                    // trim the leading and trailing spaces
                    outval = outval.trim();
                }

                /**
                 * On NT(or Windows), when user does Ctrl-C to exit the
                 * wizard in TTY mode, before the wizard is terminated,
                 * a "null" input will be caught and it will be returned
                 * as the result of queryValue. This will caused the
                 * wizard to throw a NullPointerException if a non-null
                 * value is expected.
                 *
                 * This situation is described in bug 4466023.
                 * On Solaris, when Ctrl-C is entered, the wizard
                 * execution will be suspended immediately. But on NT
                 * (most probably also the case on windows), wizard will
                 * continue the execution with "null" as the result of
                 * query. This will cause the NullPointerException to be
                 * thrown. The wizard will continue its execution and then
                 * will be suspended. The timing sounds to be random(which
                 * probably depending how busy the OS is then). The
                 * interesting behavior is that the JIT's on/off may effect how
                 * far the Java application will execute forward.
                 *
                 * This is an OS related problem specifically to NT/windows. The
                 * fix(workaround) is return an empty string when null is caught
                 * by queryValue. This will allow the wizard to still go without
                 * throwing an exception. If the wizard has a default value, the
                 * default value will be returned as the query result; if
                 * there is no default value for this query, the query message
                 * will be displayed again and then the wizard will be
                 * suspended.
                 */
                if (outval == null) {

                    // Control-D pressed
                    if (navEnabled) {
                        setNavEnabled(false);

                        if (getEcho()) {
                            treeManager.exitButtonPressed();
                        } else {
                            setEcho(true);
                            treeManager.exitButtonPressed();
                            setEcho(false);
                        }

                        setNavEnabled(true);
                    }
                } else if (outval.equals("?")) {
                    showNewline();
                    prettyPrintText(help, true, ident);
                    showNewline();
                    prettyPrintText(wizardi18n.getString(
                            "ttydisplay.help.standard"), true, ident);
                    showNewline();
                } else if (navEnabled && outval.equals(backOption)) {
                    setNavEnabled(false);

                    return outval;
                } else if (outval.equals("") && !defval.equals(NO_DEFAULT)) {
                    return defval;
                } else {
                    return outval;
                }
            } catch (IOException ioe) {
                System.err.println("Read exception: " + ioe);

                return null;
            }
        }
    }

    /**
     * Format text to length and width of screen, and display
     *
     * @param  text  the text to print
     * @param  lf  If true, add a trailing linefeed
     */
    private static void prettyPrintText(String text, boolean lf, int indent) {

        String spaces = "";

        for (int s = 0; s < indent; s++) {
            spaces += " ";
        }

        int useWidth = termWidth - indent;
        BreakIterator bi = BreakIterator.getLineInstance();

        bi.setText(text);

        int leftBoundary = 0;
        int previousRightBoundary = bi.first();
        int rightBoundary = previousRightBoundary;

        /*
         * Look for line boundaries in the string that
         * represent one-too-many words and the one before
         * that.
         */
        while (rightBoundary != BreakIterator.DONE) {
            boolean printThisLine = false;
            boolean containsNewline = false;
            int width = rightBoundary - leftBoundary;
            int newlineIndex = text.indexOf("\n", leftBoundary);

            if (newlineIndex > -1) {

                if (newlineIndex >= previousRightBoundary) {

                    /*
                     * Reset the new line index because the new line is
                     * not in the boundaries of our string.
                     */
                    newlineIndex = -1;
                }
            }

            if ((width > useWidth) || (newlineIndex > -1)) {

                if (newlineIndex > -1) {
                    previousRightBoundary = newlineIndex + 1;
                }

                /*
                 * "rightBoundary" represents one too many words.
                 * We can print the line between left boundary
                 * and previousRightBoundary.
                 */
                int tempRight = previousRightBoundary;

                /*
                 * skip trailing white space.
                 */
                while ((leftBoundary < tempRight) &&
                        (text.charAt(tempRight - 1) != '\n') &&
                        Character.isWhitespace(text.charAt(tempRight - 1))) {
                    tempRight--;
                }

                outWriter.print(spaces +
                    text.substring(leftBoundary, tempRight));

                /*
                 * Only provide a new line if one is not in the
                 * string.
                 */
                if (newlineIndex == -1) {
                    outWriter.print("\n");
                }

                /*
                 * Reset the boundaries.
                 */
                leftBoundary = previousRightBoundary;

                /*
                 * Skip white space on the next line by advancing
                 * the left boundary as needed.
                 */
                while ((leftBoundary < rightBoundary) &&
                        Character.isWhitespace(text.charAt(leftBoundary)) &&
                        (text.charAt(leftBoundary) != '\n')) {
                    leftBoundary++;
                    previousRightBoundary++;
                }
            }

            /*
             * Advance to the next line break.
             */
            previousRightBoundary = rightBoundary;

            if (newlineIndex == -1) {
                rightBoundary = bi.next();
            }
        }

        rightBoundary = previousRightBoundary;

        /*
         * If adding a line feed, skip trailing white space.
         */
        while ((lf & (leftBoundary < rightBoundary)) &&
                (text.charAt(rightBoundary - 1) != '\n') &&
                Character.isWhitespace(text.charAt(rightBoundary - 1))) {
            rightBoundary--;
        }

        /*
         * If there is text left over, print it here.
         */
        if (leftBoundary < rightBoundary) {
            outWriter.print(spaces +
                text.substring(leftBoundary, rightBoundary));
        }

        if (lf) {
            outWriter.print("\n");
        }

        outWriter.flush();
    }

    /**
     * Method declaration
     *
     * @param  data
     *
     * @see
     */
    public static void displayTable(Vector data, int ident) {
        pageText(createTable(data), ident);
        outWriter.flush();
    }

    /**
     * Method declaration
     *
     * @param  data
     *
     * @return
     *
     * @see
     */
    public static String createTable(Vector data) {
        String out = new String();
        String actualData[][];
        int numRows = data.size();
        int numCols = 0;
        int lineNo = 1;
        boolean flag = true;

        for (Enumeration e = data.elements(); e.hasMoreElements();) {
            Vector rowData = (Vector) e.nextElement();

            if (rowData.size() > numCols) {
                numCols = rowData.size();
            }
        }

        actualData = new String[numRows][numCols];

        int colLens[] = new int[numCols];

        for (int i = 0; i < colLens.length; i++) {
            colLens[i] = 0;
        }

        int count = 0;

        for (Enumeration e = data.elements(); e.hasMoreElements();) {
            Vector rowData = (Vector) e.nextElement();
            Enumeration f = rowData.elements();

            for (int i = 0; i < numCols; i++) {
                String str = "";

                if (f.hasMoreElements()) {
                    str = (String) f.nextElement();
                }

                if ((str == null) || str.equals("")) {
                    str = " ";
                }

                actualData[count][i] = str;

                if (str.length() > colLens[i]) {
                    colLens[i] = str.length();
                }
            }

            count++;
        }

        int totalLen = 0;

        for (int i = 0; i < colLens.length; i++) {
            totalLen += colLens[i] + TABLE_PADDING;
        }

        while (totalLen > termWidth) {
            colLens[arrayLargest(colLens)]--;
            totalLen--;
        }

        for (int i = 0; i < actualData.length; i++) {

            for (int k = 0; k < actualData[0].length; k++) {
                int maxLen = colLens[k];
                String dataStr = actualData[i][k];
                int strLen = dataStr.length();

                if (strLen > maxLen) {

                    /*
                     * The line of code truncates the string to term
                     * width which is wrong. It should let long strings be
                     * wrapped. This creates serious issues if what is
                     * displayed is component or product names and they
                     * get truncated...
                     */
                }

                if (strLen < maxLen) {

                    for (int l = strLen; l < maxLen; l++) {
                        dataStr += " ";
                    }
                }

                if (flag) {
                    dataStr = "     " + lineNo + ") " + dataStr + " ";
                    lineNo++;
                    flag = false;
                } else {
                    dataStr = " " + dataStr + " ";
                    flag = true;
                }

                out += dataStr;
            }

            out += "\n \n";
        }

        return out;
    }

    /**
     * Method declaration
     *
     * @param  array
     *
     * @return
     *
     * @see
     */
    private static int arrayLargest(int array[]) {
        int largest = 0;

        for (int i = 0; i < array.length; i++) {
            largest = (array[i] > array[largest]) ? i : largest;
        }

        return largest;
    }

    private static Vector createTextVector(String inputText) {
        Vector textVector = new Vector();
        int currentPosition = 0;
        int currentAttribute = 0;
        StringBuffer text = new StringBuffer();
        boolean attributeRemoved = false;

        while (currentPosition < inputText.length()) {
            attributeRemoved = false;

            /*
             * Check the character to see if it is an attribute.
             */
            while ((currentPosition < inputText.length()) &&
                    (inputText.charAt(currentPosition) == '<')) {
                attributeRemoved = false;

                /*
                 * This could be an attribute change.
                 */
                for (int index = 0; index < addAttr.length; index++) {

                    // Check for turn-on attribute
                    if ((inputText.length() >=
                                (currentPosition + addAttr[index].length())) &&
                            inputText.substring(currentPosition,
                                currentPosition + addAttr[index].length())
                            .equalsIgnoreCase(addAttr[index])) {

                        // This is a turn-on attribute
                        // First: zip past the attribute just found
                        currentPosition += addAttr[index].length();
                        attributeRemoved = true;

                        // Next: determine which attribute it is
                        int theAttribute = (1 << (index));

                        // Next: if this is a break/paragraph attributes,
                        // handle that
                        if ((theAttribute & (attrBreak | attrParagraph)) != 0) {

                            if (text.length() > 0) {
                                textVector.addElement(text.toString());

                                text = new StringBuffer();
                            }

                            if ((theAttribute & attrParagraph) != 0) {
                                textVector.addElement("");
                            }
                        }
                    }

                    // Check for turn-off attribute
                    else if ((inputText.length() >=
                                (currentPosition +
                                    removeAttr[index].length())) &&
                            inputText.substring(currentPosition,
                                currentPosition + removeAttr[index].length())
                            .equalsIgnoreCase(removeAttr[index])) {

                        // This is a turn-off (e.g. </...>) attribute.
                        // Throw them all away.
                        currentPosition += removeAttr[index].length();
                        attributeRemoved = true;
                    }

                    // If an attribute was removed, the input position has
                    // been advanced past the attribute. Break out of the
                    // attribute decoding "for" loop.
                    if (attributeRemoved == true) {
                        break;
                    }
                } // for(...)

                // If an attribute was not removed, it means we
                // encountered a "<" that did not correspond to an
                // attribute we understand.  Break out of the attribute
                // detection "while" loop so it is just included in the
                // final output text. Otherwise, let the attribute
                // detection "while" loop look to see if another attribute
                // immediately follows the one we just removed.
                if (attributeRemoved == false) {
                    break;
                }
            } // while(...)

            // If an attribute was not removed, it means we either
            // encountered a "<" that did not preceed a recognized
            // attribute or we encountered any other character - include it
            // in the final output text.
            if (attributeRemoved == false) {

                // Continue reading characters from the inputText.
                text.append(inputText.charAt(currentPosition));

                currentPosition++;
            }
        } // while(...)

        /*
         * Write the final character string into the vector.
         */
        if (text.length() > 0) {
            textVector.addElement(text.toString());
        }

        return (textVector);
    }

    public static void writeToLog(String output) {
        String logfile = null;

        if (getWizardTreeManager() != null) {
            logfile = (String) getWizardTreeManager().getWizardModel()
                .getWizardValue("install.logfile");
        }

        writeToLog(logfile, output);
    }

    /**
     * Utility function to write to a log.
     *
     * @param  logFile  Path to the desired log file.
     * @param  output  The string to write to the log.
     */
    public static void writeToLog(String logFile, String output) {

        if (logFile != null) {

            try {
                FileWriter log = new FileWriter(logFile, true);
                PrintWriter out = new PrintWriter(log);

                out.println(output);
                out.close();
            } catch (IOException ex) {
                System.out.println("writeToLog IOException: " + ex);

                if (Def.DEBUG) {
                    System.out.println("writeToLog logFile=" + logFile);
                }
            }
        } else {

            if (Def.DEBUG) {
                System.out.println(output);
            }
        }
    }


    private static String[] getSelectedOptionsAr(String input) {

        // Remove duplicate delimiters
        String regex = "";
        Pattern pattern = null;
        Matcher matcher = null;

        for (int ctr = 0; ctr < delimiters.length; ctr++) {
            pattern = Pattern.compile(delimiters[ctr] + "+");
            matcher = pattern.matcher(input);
            input = matcher.replaceAll(delimiters[ctr]);

            // Prepare a regular expression which would help while
            // splitting the options
            if (ctr > 0)
                regex = regex + "|";

            regex = regex + delimiters[ctr];
        }

        // Split options based on delimiters
        pattern = Pattern.compile(regex);

        return pattern.split(input);
    }

    private static int getColWidth(String[] subheadings) {
	if (subheadings != null && subheadings.length > 3) {
	    return 20;
	}
	return 30;
    }

}


// Thread Class Spawned by Busy Thread
class BusyThread extends Thread {

    public void run() {

        try {
            print(" "); // Add a space

            while (true) {
                printBack("\\", 1);
                sleep(40);
                printBack("|", 1);
                sleep(40);
                printBack("/", 1);
                sleep(40);
                printBack("-", 1);
                sleep(40);
            }
        } catch (InterruptedException ie) {
            printBack(" done", 1);

            return;
        }
    }

    private void printBack(String s, int i) {

        // clear previous i values and print the String
        for (int k = 0; k < i; k++) {
            print("\b");
        }

        print(s);
    }

    private void print(String s) {
        System.out.print(s);
    }
}
