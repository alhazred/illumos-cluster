/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)RacDBReviewPanel.java	1.18	09/03/11 SMI"
 */
package com.sun.cluster.dswizards.oraclerac.database.ver9i;

// CMASS
import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.utils.Util;

// CLI wizard
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.common.CliUtil;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.logicalhost.LogicalHostWizardCreator;
import com.sun.cluster.dswizards.oraclerac.database.*;
import com.sun.cluster.dswizards.oraclerac.invoker.RACInvokerCreator;

// Model
import com.sun.cluster.model.RACModel;

// Java
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

/**
 * The Panel Class that creates a review panel and lets the user edit
 * properties.
 */
public class RacDBReviewPanel extends RacDBBasePanel {

    private static String NAME = "NAME";
    private static String VALUE = "VALUE";
    private static String TAG = "TAG";
    private static String UTILNAME = "UTILNAME";
    private static String HAORACONST_NAME = "HAORACONST_NAME";
    private static String ERRORSTR = "ERRORSTR";
    private static String CANEDIT = "CANEDIT";
    private static String CANTEDIT_ERROR = "CANTEDIT_ERROR";

    private boolean bConfigListener = false;
    private boolean bConfigServer = false;

    /**
     * Creates a Review Panel with the given name and Wizard state
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     */
    public RacDBReviewPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(name, model, wizardFlow, wizardi18n);
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.
     *		WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        title = wizardi18n.getString("haoracle.reviewpanel.title");
        desc = wizardi18n.getString("haoracle.reviewpanel.desc1");
        confirm_txt = wizardi18n.getString("cliwizards.enterAgain");

        String server_rsname_txt = wizardi18n.getString(
                "haoracle.reviewpanel.server.name");
        String listener_rsname_txt = wizardi18n.getString(
                "haoracle.reviewpanel.listener.name");
        String table_title = wizardi18n.getString("haoracle.reviewpanel.desc2");
        String heading1 = wizardi18n.getString("haoracle.reviewpanel.heading1");
        String heading2 = wizardi18n.getString("haoracle.reviewpanel.heading2");
        String propNameText = wizardi18n.getString(
                "haoracle.reviewpanel.propname");
        String propTypeText = wizardi18n.getString(
                "haoracle.reviewpanel.proptype");
        String propDescText = wizardi18n.getString(
                "haoracle.reviewpanel.propdesc");
        String currValText = wizardi18n.getString(
                "haoracle.reviewpanel.currval");
        String newValText = wizardi18n.getString("haoracle.reviewpanel.newval");
        String rsname_error = wizardi18n.getString(
                "haoracle.reviewpanel.resourcename.error");
        String rsname_assigned = wizardi18n.getString(
                "reviewpanel.resourcename.assigned");
        String rgname_error = wizardi18n.getString(
                "haoracle.reviewpanel.rgname.error");
        String rgname_assigned = wizardi18n.getString(
                "reviewpanel.rgname.assigned");
        String help_text = wizardi18n.getString("haoracle.reviewpanel.help");
        String rgcantedit = wizardi18n.getString(
                "haoracle.reviewpanel.rgname.cantedit");
        String hasprscantedit = wizardi18n.getString(
                "haoracle.reviewpanel.hasp.resource.cantedit");
        String lhrscantedit = wizardi18n.getString(
                "haoracle.reviewpanel.lh.resource.cantedit");
        String db_rgname = wizardi18n.getString("haoracle.reviewpanel.rg.name");
        String db_rgname_desc = wizardi18n.getString(
                "haoracle.reviewpanel.rg.desc");

        boolean bNewRG = false;
        boolean bNewHaspRS = false;
        boolean bNewLhRS = false;

        // Title
        TTYDisplay.printTitle(title);

        // Review panel table
        Vector reviewPanel = new Vector();

        Integer preference = (Integer) wizardModel.getWizardValue(
                Util.DB_COMPONENTS);
        int prefInt = preference.intValue();

        if ((prefInt == Util.SERVER) || (prefInt == Util.BOTH_S_L)) {
            bConfigServer = true;
        }

        if ((prefInt == Util.LISTENER) || (prefInt == Util.BOTH_S_L)) {
            bConfigListener = true;
        }

        // Generate the Resource and the ResourceGroup names
        // get the user selections
        String nodeList[] = (String[]) wizardModel.getWizardValue(
	    Util.RG_NODELIST);

	String nodeEndpoint = (String) wizardModel.getWizardValue(
	    Util.NODE_TO_CONNECT);

        String sel_orasid = (String) wizardModel.getWizardValue(
	    Util.SEL_ORACLE_SID + nodeList[0]);
        String listener_name = Util.RAC_LISTERNER_PREFIX;

        String serverRsName = null;
        String listenerRsName = null;
        String haspRsName = null;
        String lhRsName = null;

	RACModel racModel = RACInvokerCreator.getRACModel();

        String properties[] = { Util.RS_NAME };
        HashMap discoveredMap = null;
	HashMap helperData = new HashMap();
	String prefixKey = null;

        if (sel_orasid != null) {
	    prefixKey = sel_orasid.replaceAll("\\d", "");
	    helperData.put(Util.RS_PREFIX_KEY, prefixKey);
        } else {
	    prefixKey = listener_name.replaceAll("\\d", "");
            helperData.put(Util.RS_PREFIX_KEY, prefixKey);
        }

        String selZoneCluster = CliUtil.getZoneClusterName(wizardModel);
	helperData.put(Util.SEL_ZONE_CLUSTER, selZoneCluster);

        if (bConfigServer) {
            discoveredMap = racModel.discoverPossibilities(
		null, nodeEndpoint, properties, helperData);
            serverRsName = (String) discoveredMap.get(properties[0]);
            wizardModel.setWizardValue(Util.RAC_SERVER_RS_NAME, serverRsName);
        }

        if (bConfigListener) {
	    helperData.put(Util.RS_PREFIX_KEY, listener_name.replaceAll("\\d", ""));
            discoveredMap = racModel.discoverPossibilities(
		null, nodeEndpoint, properties, helperData);
            listenerRsName = (String) discoveredMap.get(properties[0]);
            wizardModel.setWizardValue(Util.RAC_LISTENER_RS_NAME,
                listenerRsName);
        }

        properties = new String[] { Util.RG_NAME };
	helperData.remove(Util.RS_PREFIX_KEY);
	helperData.put(Util.RG_PREFIX_KEY, prefixKey);
        discoveredMap = racModel.discoverPossibilities(
	    null, nodeEndpoint, properties, helperData);
        String orac_rg_name = (String) discoveredMap.get(properties[0]);
        wizardModel.setWizardValue(Util.RAC_RG_NAME, orac_rg_name);

        HashMap tableMap = new HashMap();
        HashMap selMap = null;
        String option = "";
        int row_count = 1;
        ArrayList assignedRgNames = null;
        ArrayList assignedResNames = null;

        while (!option.equals(dchar)) {
            assignedResNames = new ArrayList();
            assignedRgNames = new ArrayList();
            row_count = 1;
            TTYDisplay.setNavEnabled(true);
            this.cancelDirection = false;

            reviewPanel.removeAllElements();
            wizardModel.selectCurrWizardContext();

            tableMap = new HashMap();

            // First Row - Resource Group Name
            String serverRgName = (String) wizardModel.getWizardValue(
		Util.RAC_RG_NAME);
            reviewPanel.add(db_rgname);
            reviewPanel.add(serverRgName);
            selMap = new HashMap();
            selMap.put(NAME, db_rgname);
            selMap.put(VALUE, serverRgName);
            selMap.put(TAG, "rg");
            selMap.put(UTILNAME, Util.RESOURCEGROUPNAME);
            selMap.put(HAORACONST_NAME, Util.RAC_RG_NAME);
            selMap.put(ERRORSTR, rgname_error);
            selMap.put(CANEDIT, new Boolean(true));
            selMap.put(CANTEDIT_ERROR, "");
            tableMap.put(Integer.toString(row_count++), selMap);

            if (!assignedRgNames.contains(serverRgName)) {
                assignedRgNames.add(serverRgName);
            }

            // Second Row - Server Resource Name
            if (bConfigServer) {
                serverRsName = (String) wizardModel.getWizardValue(
                        Util.RAC_SERVER_RS_NAME);
                reviewPanel.add(server_rsname_txt);
                reviewPanel.add(serverRsName);
                selMap = new HashMap();
                selMap.put(NAME, server_rsname_txt);
                selMap.put(VALUE, serverRsName);
                selMap.put(TAG, "server");
                selMap.put(UTILNAME, Util.RESOURCENAME);
                selMap.put(HAORACONST_NAME, Util.RAC_SERVER_RS_NAME);
                selMap.put(ERRORSTR, rsname_error);
                selMap.put(CANEDIT, new Boolean(true));
                selMap.put(CANTEDIT_ERROR, "");
                tableMap.put(Integer.toString(row_count++), selMap);

                if (!assignedResNames.contains(serverRsName)) {
                    assignedResNames.add(serverRsName);
                }
            }

            // Third Row - Listener Resource Name
            if (bConfigListener) {
                listenerRsName = (String) wizardModel.getWizardValue(
                        Util.RAC_LISTENER_RS_NAME);
                reviewPanel.add(listener_rsname_txt);
                reviewPanel.add(listenerRsName);
                selMap = new HashMap();
                selMap.put(NAME, listener_rsname_txt);
                selMap.put(VALUE, listenerRsName);
                selMap.put(TAG, "listener");
                selMap.put(UTILNAME, Util.RESOURCENAME);
                selMap.put(HAORACONST_NAME, Util.RAC_LISTENER_RS_NAME);
                selMap.put(ERRORSTR, rsname_error);
                selMap.put(CANEDIT, new Boolean(true));
                selMap.put(CANTEDIT_ERROR, "");
                tableMap.put(Integer.toString(row_count++), selMap);

                if (!assignedResNames.contains(listenerRsName)) {
                    assignedResNames.add(listenerRsName);
                }

                for (int i = 0; i < nodeList.length; i++) {


                    String curNode = nodeList[i];
                    String tmpArry[] = curNode.split(Util.COLON);
                    curNode = tmpArry[0];

                    String hostNameList[] = (String[]) wizardModel
                        .getWizardValue(Util.HOSTNAMELIST + curNode);
                    Vector existingLHRS = (Vector) wizardModel.getWizardValue(Util.SEL_LH_RS + curNode);
                    String lh_rsname_txt = wizardi18n.getString("oraclerac.reviewpanel.lh.name",
			new String[] { curNode });
                    String rgname_txt = wizardi18n.getString("oraclerac.reviewpanel.rg.name",
			new String[] { curNode });

                    if (hostNameList == null) {

                        // user is not creating new Logical Host resource
                        bNewLhRS = false;
                        wizardModel.setWizardValue(Util.SEL_LH_RSNAMES + curNode,
                            Util.convertArraytoString((String[]) existingLHRS.toArray(new String[0])));
                    } else {

                        // Resource Name
                        bNewLhRS = true;
			properties = new String[] { Util.RS_NAME };
                        lhRsName = (String) wizardModel.getWizardValue(
                                Util.SEL_LH_RSNAMES + curNode);

                        if ((lhRsName == null) || lhRsName.equals(Util.NEW)) {
			    String hostNames = Util.convertArraytoString(hostNameList);
			    String rName = hostNames.replace(',', '-');
			    rName = hostNames.replace('.', '-');

			    helperData.remove(Util.RG_PREFIX_KEY);
			    helperData.put(Util.RS_PREFIX_KEY, rName);

                            discoveredMap = racModel.discoverPossibilities(
				null, nodeEndpoint, properties, helperData);
                            lhRsName = (String) discoveredMap.get(properties[0]);
                        }

                        if (!assignedResNames.contains(lhRsName)) {
                            assignedResNames.add(lhRsName);
                        }

                        wizardModel.setWizardValue(Util.SEL_LH_RSNAMES +
                            curNode, lhRsName);
                    }

                    String rgName = (String) wizardModel.getWizardValue(
			Util.SEL_LH_RG + curNode);
                    bNewRG = false;

                    Boolean zRGBol = (Boolean) wizardModel.getWizardValue(
			Util.NEW_LH_RG + curNode);

                    if (zRGBol != null) {
                        bNewRG = zRGBol.booleanValue();
                    }

                    if ((rgName == null) || rgName.equals(Util.NEW)) {

                        // a new RG is being created
                        bNewRG = true;

                        // Resource Group Name
                        String data = Util.RAC_LH_RG_PREFIX;
                        helperData.put(Util.PREFIX, data);
                        helperData.put(Util.EXEMPT, assignedRgNames);
                        properties = new String[] { Util.RG_NAME_W_EXCEPTIONS };
                        discoveredMap = racModel.discoverPossibilities(
			    null, nodeEndpoint, properties, helperData);
                        rgName = (String) discoveredMap.get(properties[0]);
                    }

                    if (!assignedRgNames.contains(rgName)) {
                        assignedRgNames.add(rgName);
                    }

                    wizardModel.setWizardValue(Util.SEL_LH_RG + curNode,
                        rgName);
                    wizardModel.setWizardValue(Util.NEW_LH_RG + curNode,
                        new Boolean(bNewRG));
                    wizardModel.setWizardValue(Util.NEW_LH_RS + curNode,
                        new Boolean(bNewLhRS));

                    // add logicalhost resource
                    reviewPanel.add(lh_rsname_txt);
                    lhRsName = (String) wizardModel.getWizardValue(
                            Util.SEL_LH_RSNAMES + curNode);
                    reviewPanel.add(lhRsName);
                    selMap = new HashMap();
                    selMap.put(NAME, lh_rsname_txt);
                    selMap.put(VALUE, lhRsName);
                    selMap.put(TAG, "lh");
                    selMap.put(UTILNAME, Util.RESOURCENAME);
                    selMap.put(HAORACONST_NAME, Util.SEL_LH_RSNAMES + curNode);
                    selMap.put(ERRORSTR, rsname_error);

                    if (bNewLhRS) {
                        selMap.put(CANEDIT, new Boolean(true));
                    } else {
                        selMap.put(CANEDIT, new Boolean(false));
                    }

                    selMap.put(CANTEDIT_ERROR, lhrscantedit);
                    tableMap.put(Integer.toString(row_count++), selMap);

                    // Next Row - RG Name
                    rgName = (String) wizardModel.getWizardValue(
                            Util.SEL_LH_RG + curNode);
                    reviewPanel.add(rgname_txt);
                    reviewPanel.add(rgName);
                    selMap = new HashMap();
                    selMap.put(NAME, rgname_txt);
                    selMap.put(VALUE, rgName);
                    selMap.put(TAG, "lhrg");
                    selMap.put(UTILNAME, Util.RESOURCEGROUPNAME);
                    selMap.put(HAORACONST_NAME, Util.SEL_LH_RG + curNode);
                    selMap.put(ERRORSTR, rgname_error);

                    if (bNewRG) {
                        selMap.put(CANEDIT, new Boolean(true));
                    } else {
                        selMap.put(CANEDIT, new Boolean(false));
                    }

                    selMap.put(CANTEDIT_ERROR, rgcantedit);
                    tableMap.put(Integer.toString(row_count++), selMap);
                }
            }

            TTYDisplay.pageText(desc);
            TTYDisplay.clear(1);

            String subheadings[] = { heading1, heading2 };

            HashMap customTags = new HashMap();
            customTags.put(dchar, done);
            option = TTYDisplay.create2DTable(table_title, subheadings,
                    reviewPanel, customTags, help_text);

            String desc;

            if (option.equals(back)) {
                this.cancelDirection = true;

                return;
            }

            TTYDisplay.clear(3);
            TTYDisplay.setNavEnabled(false);

            if (!option.equals(dchar)) {
                selMap = (HashMap) tableMap.get(option);

                String name = (String) selMap.get(NAME);
                String value = (String) selMap.get(VALUE);
                String tag = (String) selMap.get(TAG);
                String utilName = (String) selMap.get(UTILNAME);
                String error_str = (String) selMap.get(ERRORSTR);
                boolean canEdit = ((Boolean) selMap.get(CANEDIT))
                    .booleanValue();
                String cantEditErrorMsg = (String) selMap.get(CANTEDIT_ERROR);
                String haoraConstName = (String) selMap.get(HAORACONST_NAME);
                TTYDisplay.pageText(propNameText +
                    wizardi18n.getString(
                        "haoracle.reviewpanel." + tag + ".name"));

                TTYDisplay.pageText(propDescText +
                    wizardi18n.getString(
                        "haoracle.reviewpanel." + tag + ".desc"));

                TTYDisplay.pageText(propTypeText +
                    wizardi18n.getString(
                        "haoracle.reviewpanel." + tag + ".type"));

                TTYDisplay.pageText(currValText + value);

                if (!canEdit) {
                    TTYDisplay.pageText(cantEditErrorMsg);
                    TTYDisplay.clear(1);
                    TTYDisplay.promptForEntry(continue_txt);

                    continue;
                }

                String tmp_name = null;
                String enteragain = yes;
                boolean error = true;

                while (error && enteragain.equals(yes)) {
                    tmp_name = TTYDisplay.promptForEntry(newValText);

                    if ((tmp_name != null) && (tmp_name.trim().length() != 0)) {
                        properties = new String[] { utilName };

                        HashMap userInputs = new HashMap();
                        userInputs.put(utilName, tmp_name);

                        ErrorValue retVal = validateInput(properties,
                                userInputs, helperData);

                        if (!retVal.getReturnValue().booleanValue()) {
                            TTYDisplay.printError(error_str);
                            enteragain = TTYDisplay.getConfirmation(confirm_txt,
                                    yes, no);
                        } else {

                            // For resource and resourcegroup names
                            // make sure that the names within the same
                            // wizard configuration are unique
                            if (utilName.equals(Util.RESOURCEGROUPNAME)) {

                                // Remove the original name else
                                // the condition will always fail
                                assignedRgNames.remove(value);

                                if (assignedRgNames.contains(tmp_name)) {
                                    TTYDisplay.printError(rgname_assigned);
                                    enteragain = TTYDisplay.getConfirmation(
                                            confirm_txt, yes, no);

                                    // Add the original name back
                                    assignedRgNames.add(value);
                                } else {
                                    error = false;
                                    assignedRgNames.add(tmp_name);
                                    wizardModel.setWizardValue(haoraConstName,
                                        tmp_name);
                                }
                            } else if (utilName.equals(Util.RESOURCENAME)) {
                                assignedResNames.remove(value);

                                if (assignedResNames.contains(tmp_name)) {
                                    TTYDisplay.printError(rsname_assigned);
                                    enteragain = TTYDisplay.getConfirmation(
                                            confirm_txt, yes, no);
                                    assignedResNames.add(value);
                                } else {
                                    error = false;
                                    assignedResNames.add(tmp_name);
                                    wizardModel.setWizardValue(haoraConstName,
                                        tmp_name);
                                }
                            } else {
                                error = false;
                                wizardModel.setWizardValue(haoraConstName,
                                    tmp_name);
                            }
                        }
                    }
                } // while
            }

            TTYDisplay.clear(3);
        }
    }

    private ErrorValue validateInput(String propNames[], HashMap userInput,
        HashMap helperData) {
        ErrorValue retValue = null;
	RACModel racModel = RACInvokerCreator.getRACModel();
	String nodeEndpoint = (String) wizardModel.getWizardValue(
	    Util.NODE_TO_CONNECT);
        retValue = racModel.validateInput(null, nodeEndpoint, propNames, userInput, helperData);

        return retValue;
    }
}
