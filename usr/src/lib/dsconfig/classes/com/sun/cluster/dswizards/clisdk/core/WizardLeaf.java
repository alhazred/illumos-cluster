/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)WizardLeaf.java 1.13     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.clisdk.core;

import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;


/**
 * WizardLeaf implements the default behavior common to all WizardComponents
 * that have no children.
 *
 * <p>The wizard client panel tree consists of an arrangement of panels
 * (subclassed from WizardLeaf) and nodes (subclassed from WizardComposite). The
 * arrangement of the tree determines the order in which the panels will be
 * visited by the wizard.  Nodes can affect the traversal of the panels, which
 * allows advanced panels to be skipped for users that do not wish to use them.
 *
 * <p>"Leaf" was chosen as a name for this class from the book "Design
 * Patterns".
 */
public class WizardLeaf extends WizardComponent {

    // Change this value only when making a serializationally
    // incompatible change such as deleting a field. See the
    // Java Object Serialization Specification, Chapter 5:
    // Versioning of Serializable Objects, for more details.
    private static final long serialVersionUID = 4526157351968451205L;


    /** A value identifying the back button. */
    public static final int BACK_BUTTON = WizardTreeManager.BACK_BUTTON;

    /** A value identifying the next button. */
    public static final int NEXT_BUTTON = WizardTreeManager.NEXT_BUTTON;

    /** A value identifying the cancel button. */
    public static final int CANCEL_BUTTON = WizardTreeManager.CANCEL_BUTTON;

    /** A value identifying the exit button. */
    public static final int EXIT_BUTTON = WizardTreeManager.EXIT_BUTTON;

    /** A value identifying the help button. */
    public static final int HELP_BUTTON = WizardTreeManager.HELP_BUTTON;

    /**
     * Creates a WizardLeaf with a route to the root of the server object tree.
     */
    public WizardLeaf() {
    }

    /**
     * Creates a WizardLeaf with the specified name.
     *
     * @param  wizardModel  The CliDSConfigWizardModel object.
     * @param  name  The name for this WizardLeaf.
     */
    public WizardLeaf(CliDSConfigWizardModel wizardModel, String name,
        WizardFlow wizardFlow) {
        super(wizardModel, name, wizardFlow);
    }

    /**
     * Creates a WizardComponent with the specified name and the specified
     * wizard manager.
     *
     * @param  name  The name for this WizardComponent.
     * @param  wizardManager  The wizardManager responsible for this component.
     */
    public WizardLeaf(String name, WizardTreeManager wizardManager) {
        super(name, wizardManager);
    }

    /**
     * This method creates the user interface.
     */
    public void createUI() {
        super.createUI();
    }

    /**
     * Get the child that follows the specified child in the WizardComponent
     * tree.  Applications should use <code>next</code>.
     *
     * @param  currentChild  The child in the tree that comes before the desired
     * sibling.
     *
     * @return  The next child in the tree.
     */
    protected WizardComponent getNextChild(WizardComponent currentChild) {
        throw new IllegalArgumentException("WizardLeaf has no children.");
    }

    /**
     * Get the next WizardComponent in the tree.
     *
     * @return  The next child in the tree.
     */
    public WizardComponent next() {
        WizardComponent parent = getParentComponent();

        if (parent != null) {

            if (parent instanceof WizardComposite) {
                return (((WizardComposite) parent).getNextChild(this));
            }
        }

        return (null);
    }

    /**
     * Get the child that precedes the specified child in the WizardComponent
     * tree.  Applications should use <code>previous</code>.
     *
     * @param  currentChild  The child in the tree that comes after the desired
     * sibling.
     *
     * @return  The previous child in the tree.
     */
    public WizardComponent getPreviousChild(WizardComponent currentChild) {
        throw new IllegalArgumentException("WizardLeaf has no children.");
    }

    /**
     * Get the previous WizardComponent in the tree.
     *
     * @return  The previous child in the tree.
     */
    public WizardComponent previous() {
        WizardComponent parent = getParentComponent();

        if (parent != null) {

            if (parent instanceof WizardComposite) {
                return (((WizardComposite) parent).getPreviousChild(this));
            }
        }

        return (null);
    }

    /**
     * Returns information suggesting which buttons should be displayed for this
     * panel.
     *
     * @return  A button mask identifying valid navigation buttons.
     */
    public int getButtonMask() {
        int buttonMask = EXIT_BUTTON;
        buttonMask |= BACK_BUTTON;

        if (!isLast()) {
            buttonMask |= NEXT_BUTTON;
        }

        buttonMask |= HELP_BUTTON;


        return (buttonMask);
    }

    /**
     * Returns the default help message string. The real panel wants to display
     * the help button should override this method to provide the real help
     * message for that panel.
     *
     * @return  the default help string to display when help button pressed
     */
    public String getHelp() {
        return null;
    }
}
