/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)Rac10GCrsStorageSelectionPanel.java	1.9	09/04/22 SMI"
 */


package com.sun.cluster.dswizards.oraclerac.database.ver10g;

// CMASS
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.rgm.ResourceData;
import com.sun.cluster.agent.rgm.property.ResourceProperty;

// Wizard Common
import com.sun.cluster.dswizards.common.CliUtil;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.oraclerac.database.*;
import com.sun.cluster.dswizards.oraclerac.invoker.RACInvokerCreator;

// Model classes
import com.sun.cluster.model.DataModel;
import com.sun.cluster.model.RACModel;
import com.sun.cluster.model.ResourceModel;

// J2SE
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * The Panel Class that gets CRS OCR and voting disks as input from the User.
 */
public class Rac10GCrsStorageSelectionPanel extends RacDBBasePanel {
    private List discoveredMountPointDirs = null;
    private List discoveredDiskGroupNames = null;
    private List scMountPointDirs = null;
    private List scDiskGroupNames = null;

    private List scalableMountPoints = null;
    private List scalableDiskGroups = null;

    /**
     * Creates an Rac10GCrsStorageSelectionPanel for the wizard with the given
     * name and associates it with the WizardModel and WizardFlow.
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     * @param  wizardType  WizardType
     */
    public Rac10GCrsStorageSelectionPanel(String name,
        CliDSConfigWizardModel model, WizardFlow wizardFlow,
        WizardI18N wizardi18n) {
        super(name, model, wizardFlow, wizardi18n);
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.
     *		WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        title = wizardi18n.getString(
                "oraclerac10g.crsStorageSelectionPanel.title");
        desc = wizardi18n.getString(
                "oraclerac10g.crsStorageSelectionPanel.desc");

        String emptytable_desc = wizardi18n.getString(
                "oraclerac10g.crsStorageSelectionPanel.empty.desc");
        String showAll = wizardi18n.getString(
                "oraclerac10g.crsStorageSelectionPanel.menu.showall");
        String showDiscovered = wizardi18n.getString(
                "oraclerac10g.crsStorageSelectionPanel.menu.showdiscovered");
        String help_text = wizardi18n.getString(
                "oraclerac10g.crsStorageSelectionPanel.cli.help");
        String error_txt = wizardi18n.getString(
                "oraclerac10g.crsStorageSelectionPanel.none.error");

        boolean bRefresh = true;
        boolean bshowAll = false;

        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        boolean bcontinue = true;
        ArrayList data = null;

        while (bcontinue) {

            // Title
            TTYDisplay.printTitle(title);

            HashMap customTags = new HashMap();

            if (bRefresh) {
                data = new ArrayList();
                fetchCrsStorageLocation();

                if (discoveryEmpty()) {

                    // display empty table information
                    TTYDisplay.printSubTitle(emptytable_desc);
                    bshowAll = true;
                } else {

                    // put the discovered list in vector
                    bshowAll = false;
                }

                bRefresh = false;
            }

            if (bshowAll) {

                // get all mount points and disk groups
                customTags.put(schar, showDiscovered);
                data = new ArrayList();

                if ((scMountPointDirs != null) &&
		    (scMountPointDirs.size() > 0)) {
                    data.addAll(scMountPointDirs);
                }

                if ((scDiskGroupNames != null) &&
		    (scDiskGroupNames.size() > 0)) {
                    data.addAll(scDiskGroupNames);
                }

                bshowAll = false;
            } else {
                customTags.put(schar, showAll);

                // get only discovered mount points and disk groups
                data = new ArrayList();

                if ((discoveredMountPointDirs != null) &&
		    (discoveredMountPointDirs.size() > 0)) {
                    data.addAll(discoveredMountPointDirs);
                }

                if ((discoveredDiskGroupNames != null) &&
		    (discoveredDiskGroupNames.size() > 0)) {
                    data.addAll(discoveredDiskGroupNames);
                }

                bshowAll = true;
            }

            customTags.put(rchar, refresh);

            // Description Para
            TTYDisplay.printSubTitle(desc);

            StringBuffer commaSeparatedSelections = new StringBuffer();
            ArrayList allSelectedCrsStorageLoc = (ArrayList) wizardModel
                .getWizardValue(Util.ALL_SEL_CRS_FILES);

            ArrayList defSelections = null;

            if (allSelectedCrsStorageLoc != null) {
                defSelections = new ArrayList();
                defSelections.addAll(allSelectedCrsStorageLoc);
            }

            if (defSelections != null) {
                int size = defSelections.size();

                for (int i = 0; i < size; i++) {
                    String defSelection = (String) defSelections.get(i);

                    if ((data != null) && data.contains(defSelection)) {
                        commaSeparatedSelections.append(defSelections);

                        if (i != size) {
                            commaSeparatedSelections.append(Util.COMMA);
                        }
                    }
                }
            }

            String optionsArray[] = new String[0];

            if (data != null) {
                optionsArray = (String[]) data.toArray(new String[0]);
            }

            List selections = TTYDisplay.getScrollingOptions("", optionsArray,
                    customTags, commaSeparatedSelections.toString(), help_text,
                    true);

            if (selections == null) {
                this.cancelDirection = true;

                return;
            }

            if (selections.contains(refresh)) {

                // refresh the list
                bRefresh = true;

                continue;
            }

            // show all
            if (selections.contains(showAll)) {
                bshowAll = true;

                continue;
            } else if (selections.contains(showDiscovered)) {
                bshowAll = false;

                continue;
            }

            // Store the selections in the WizardModel
            int size = selections.size();
            allSelectedCrsStorageLoc = new ArrayList();

            ArrayList rsList = new ArrayList();

            for (int i = 0; i < size; i++) {

                // map the selection to filesystems and disk groups
                String selection = (String) selections.get(i);
                String rsName = getRSName(selection);

                if (!allSelectedCrsStorageLoc.contains(selection)) {
                    allSelectedCrsStorageLoc.add(selection);
                }

                if (!rsList.contains(rsName)) {
                    rsList.add(rsName);
                }
            }

	    String selectedCrsHome = (String)wizardModel.getWizardValue(Util.SEL_CRS_HOME);

	    boolean found = false;
	    int count = 0;
	    int sizeMountPointDirs = -1;
	    if (scMountPointDirs != null) {
		sizeMountPointDirs = scMountPointDirs.size();
		while (count < sizeMountPointDirs && !found) {
		   // check what mount point the selected CRS home belongs to
		   String scMountPointDir = (String)scMountPointDirs.get(count);
		   if (selectedCrsHome.startsWith(scMountPointDir)) {
			found = true;
			if (!allSelectedCrsStorageLoc.contains(scMountPointDir)) {
			     allSelectedCrsStorageLoc.add(scMountPointDir);
			}
			String rsName = getRSName(scMountPointDir);
			if (!rsList.contains(rsName)) {
			     rsList.add(rsName);
			}
		    } else {
			count++;
		    }
		}
	    }

            wizardModel.setWizardValue(Util.SEL_SCAL_RS, rsList);
            wizardModel.setWizardValue(Util.ALL_SEL_CRS_FILES,
                allSelectedCrsStorageLoc);
            bcontinue = false;
        }
    }

    private boolean isMountPoint(String selection) {

        if ((scMountPointDirs != null) &&
	    scMountPointDirs.contains(selection)) {
            return true;
        }

        return false;
    }

    private boolean isDiskGroup(String selection) {

        if ((scDiskGroupNames != null) &&
	    scDiskGroupNames.contains(selection)) {
            return true;
        }

        return false;
    }

    private void fetchCrsStorageLocation() {
        String nodeEndPoint = (String) wizardModel.getWizardValue(
	    Util.NODE_TO_CONNECT);

        String rgNodeList[] = (String[]) wizardModel.getWizardValue(
	    Util.RG_NODELIST);

        HashMap helperData = new HashMap();
        helperData.put(Util.RG_NODELIST, rgNodeList);

        String selZoneCluster = CliUtil.getZoneClusterName(wizardModel);
	helperData.put(Util.SEL_ZONE_CLUSTER, selZoneCluster);

	RACModel racModel = RACInvokerCreator.getRACModel();
	DataModel dm = DataModel.getDataModel(null);
	ResourceModel rm = dm.getResourceModel();

        String props[] = { Util.SCAL_DEVGROUP_RSLIST };
        HashMap scalDGResourceMap = racModel.discoverPossibilities(
	    null, nodeEndPoint, props, helperData);

	props = new String[] { Util.SCAL_MOUNTPOINT_RSLIST };
	HashMap scalMountPointResourceMap = racModel.discoverPossibilities(
	    null, nodeEndPoint, props, helperData);

	if (scalDGResourceMap != null && !scalDGResourceMap.isEmpty()) {
            scalableDiskGroups = (List) scalDGResourceMap.get(Util.SCAL_DEVGROUP_RSLIST);
	}

	if (scalMountPointResourceMap != null && !scalMountPointResourceMap.isEmpty()) {
	    scalableMountPoints = (List) scalMountPointResourceMap.get(
		Util.SCAL_MOUNTPOINT_RSLIST);
	}

	if (scalableMountPoints != null) {
	    int size = scalableMountPoints.size();

	    for (int i = 0; i < size; i++) {
		ResourceData rData = (ResourceData) scalableMountPoints.get(i);
		List extProps = null;
		String mountPointDir = null;
		ResourceProperty rp = null;

		if (rData != null) {
		    extProps = rData.getExtProperties();
		    Iterator extPropsIter = extProps.iterator();
		    boolean found = false;
		    while (extPropsIter.hasNext() && !found) {
			rp = (ResourceProperty) extPropsIter.next();
			String propName = rp.getName();
			if (propName.equals(Util.MOUNTPOINT_DIR)) {
			    mountPointDir = (String) rm.getResourcePropertyValue(rp, false);
			    found = true;
			}
		    }

		    if (found) {
			if ((mountPointDir != null) &&
			    (mountPointDir.trim().length() > 0)) {
			    if (scMountPointDirs == null) {
				scMountPointDirs = new ArrayList();
			    }

			    if (!scMountPointDirs.contains(mountPointDir)) {
				scMountPointDirs.add(mountPointDir);
			    }
			}
		    }
		}
	    }
	    wizardModel.setWizardValue(Util.SC_SCALMOUNTPOINTS, scMountPointDirs);
	}

	if (scalableDiskGroups != null) {
	    int size = scalableDiskGroups.size();
	    for (int i = 0; i < size; i++) {
		ResourceData rData = (ResourceData) scalableDiskGroups.get(i);
		List extProps = null;
		String diskGroupName = null;
		ResourceProperty rp = null;

		if (rData != null) {
		    extProps = rData.getExtProperties();
		    Iterator extPropsIter = extProps.iterator();
		    boolean found = false;
		    while (extPropsIter.hasNext() && !found) {
			rp = (ResourceProperty) extPropsIter.next();
			String propName = rp.getName();
			if (propName.equals(Util.DISKGROUP_NAME)) {
			    diskGroupName = (String) rm.getResourcePropertyValue(rp, false);
			    found = true;
			}
		    }

		    if (found) {
			if ((diskGroupName != null) &&
			    (diskGroupName.trim().length() > 0)) {

			    if (scDiskGroupNames == null) {
				scDiskGroupNames = new ArrayList();
			    }

			    if (!scDiskGroupNames.contains(diskGroupName)) {
				scDiskGroupNames.add(diskGroupName);
			    }
			}
		    }
		}
	    }
	    wizardModel.setWizardValue(Util.SC_SCALDEVGROUPS, scDiskGroupNames);
	}

	String selectedCrsHome = (String) wizardModel.getWizardValue(Util.SEL_CRS_HOME);

	helperData = new HashMap();
        helperData.put(Util.CRS_HOME, selectedCrsHome);
	helperData.put(Util.SEL_ZONE_CLUSTER, selZoneCluster);
        helperData.put(Util.SC_SCALMOUNTPOINTS, scMountPointDirs);
        helperData.put(Util.SC_SCALDEVGROUPS, scDiskGroupNames);

        HashMap hashMap = (HashMap) racModel.discoverPossibilities(
	    null, nodeEndPoint, new String[] { Util.CRS_OCR_VOTING_FILES }, helperData);

        if (hashMap != null) {
	    wizardModel.setWizardValue(Util.DISCOVERED_MOUNTPOINTS,
		hashMap.get(Util.DISCOVERED_MOUNTPOINTS));
	    wizardModel.setWizardValue(Util.DISCOVERED_DEVGROUPS,
		hashMap.get(Util.DISCOVERED_DEVGROUPS));
        }
    }

    private boolean discoveryEmpty() {
        discoveredMountPointDirs = (List) wizardModel.getWizardValue(
                Util.DISCOVERED_MOUNTPOINTS);
        discoveredDiskGroupNames = (List) wizardModel.getWizardValue(
                Util.DISCOVERED_DEVGROUPS);

        if ((discoveredMountPointDirs == null) ||
                (discoveredMountPointDirs.size() == 0)) {

            if ((discoveredDiskGroupNames == null) ||
                    (discoveredDiskGroupNames.size() == 0)) {
                return true;
            }
        }

        return false;
    }

    private String getRSName(String selection) {
        String rsName = null;
        List list = null;
        String propName = null;

        if (isMountPoint(selection)) {
            list = scalableMountPoints;
            propName = Util.MOUNTPOINT_DIR;
        } else if (isDiskGroup(selection)) {
            list = scalableDiskGroups;
            propName = Util.DISKGROUP_NAME;
        }

        if (list != null) {
            int i = 0;
            int size = list.size();
            boolean found = false;

	    DataModel dm = DataModel.getDataModel(null);
	    ResourceModel rm = dm.getResourceModel();

            while ((i < size) && !found) {
                ResourceData rData = (ResourceData)list.get(i);
                String propVal = null;

                if (rData != null) {
		    List extProps = rData.getExtProperties();
		    if (extProps != null) {
			int extPropSize = extProps.size();
			int j = 0;
			while ( j < extPropSize && !found) {
			    ResourceProperty rProp = (ResourceProperty)extProps.get(j);
			    String name = rProp.getName();

			    if (name.equals(propName)) {
				propVal = (String)rm.getResourcePropertyValue(rProp, false);
				if (propVal.equals(selection)) {
				    rsName = rData.getName();
				    found = true;
				}
			    }
			    j++;
			}
                    }
                }

                i++;
            }
        }

        return rsName;
    }
}
