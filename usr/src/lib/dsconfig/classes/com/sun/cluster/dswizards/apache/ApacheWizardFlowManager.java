/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ApacheWizardFlowManager.java 1.11     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.apache;

import com.sun.cluster.agent.dataservices.apache.Apache;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.common.DSConfigWizardModel;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.DSWizardInterface;
import com.sun.cluster.dswizards.hastorageplus.HASPWizardConstants;

// Wizard Common


/**
 * The class takes decision on which panel to show "next" depending upon the
 * conditions set at a particular context.
 */
public class ApacheWizardFlowManager implements DSWizardInterface {

    /**
     * Creates a new instance of ApacheFlowManager
     */
    public ApacheWizardFlowManager() {
    }

    /**
     * @see  com.sun.cluster.dswizards.common.DSWizardInterface#getNextPanel
     */
    public String getNextPanel(String curPanelName, Object wizCxtObj,
        String options[]) {
	DSConfigWizardModel guiModel = null;
	CliDSConfigWizardModel cliModel = null;

	if (wizCxtObj instanceof CliDSConfigWizardModel) {
	    cliModel = (CliDSConfigWizardModel)wizCxtObj;
	} else if (wizCxtObj instanceof DSConfigWizardModel) {
	    guiModel = (DSConfigWizardModel)wizCxtObj;
	}

        // if current panel is docRootPanel
        if (curPanelName.equals(ApacheWizardConstants.DOCROOTPANEL)) {

            Boolean displayFSTable = null;
	    if (guiModel != null) {
		displayFSTable = (Boolean)guiModel.getWizardValue(
		    ApacheWizardConstants.DISPLAY_FS_TABLE);
	    } else {
		displayFSTable = (Boolean)cliModel.getWizardValue(
		    ApacheWizardConstants.DISPLAY_FS_TABLE);
	    }

            if (displayFSTable.booleanValue()) {
                return options[0];
            } else {
                return options[1];
            }
        } else if (curPanelName.equals(ApacheWizardConstants.FSPANEL)) {

	    // if current panel is FSPanel
            Object haspObj = null;
            Boolean showMountPointPanel = null;
	    if (guiModel != null) {
		haspObj = guiModel.getWizardValue(
		    ApacheWizardConstants.STORAGE_RESOURCE_NAME);
		showMountPointPanel = (Boolean)guiModel.getWizardValue(
		    ApacheWizardConstants.APACHE_MULTIPLE_MOUNTS);
	    } else {
		haspObj = cliModel.getWizardValue(
		    ApacheWizardConstants.STORAGE_RESOURCE_NAME);
		showMountPointPanel = (Boolean)cliModel.getWizardValue(
		    ApacheWizardConstants.APACHE_MULTIPLE_MOUNTS);
	    }

            if ((showMountPointPanel != null) &&
		showMountPointPanel.booleanValue()) {

                /*
                 * If there are multiple mountpoints under the selected (or) new
                 * storage resource
                 * take the user to the ApacheMountPointEntry Panel
                 */
                return options[1];
            } else if (haspObj == null) {

                /*
                 * If no storage resource governs this location, we invoke the
                 * HA storage wizard, user gives "Create New" in FS panel.
                 * After HA storage wizard completes the next panel
                 * would be Hostname selection panel
                 */
                // Set HA storage wizard for FileSystem only
		if (guiModel != null) {
		    guiModel.setWizardValue(
			HASPWizardConstants.HASP_LOCATION_PREF,
			HASPWizardConstants.FS_ONLY);
		} else {
		    cliModel.setWizardValue(
			HASPWizardConstants.HASP_LOCATION_PREF,
			HASPWizardConstants.FS_ONLY);
		}

                // Goto HA storage Wiz
                return options[0];
            } else {

                /*
                 * A new mount was created by the HA storage wizard
                 * In this case go to Hostname Selection Panel
                 * with all the LH or SH resources in the cluster (this would
                 * be filled by the FSPanel)
                 *
                 *                         (OR)
                 *
                 * DocRoot is governed by HA storage resource, we take that resource
                 * as reference resource and discover LogicalHost or
                 * SharedAddress resources according to the apache Mode (this
                 * would
                 * be filled by the FSPanel)
                 */
                return options[2];
            }
        } else if (curPanelName.equals(ApacheWizardConstants.HOSTPANEL)) {
	    // If current panel is HostSelection Panel

            String apacheMode = null;
	    if (guiModel != null) {
		apacheMode = (String)guiModel.getWizardValue(
		    Util.APACHE_MODE);
	    } else {
		apacheMode = (String)cliModel.getWizardValue(
		    Util.APACHE_MODE);
	    }

            /*
             * Apache Mode is Failover
             */
            if (apacheMode.equals(Apache.FAILOVER)) {

                /*
                 * LH_RESOURCE_NAME should be filled with a LH resource's name
                 * from the Hostname Panel, if not it would be mean user had
                 * selected create new.
                 */
		String lhRsName = null;
		if (guiModel != null) {
		    lhRsName = (String)guiModel.getWizardValue(
			Util.LH_RESOURCE_NAME);
		} else {
		    lhRsName = (String)cliModel.getWizardValue(
			Util.LH_RESOURCE_NAME);
		}
		if (lhRsName == null) {
                    // Goto LH Wizard
                    return options[0];
                }
                /*
                 * LH selected by the user so goto apache review panel
                 */
                else {

                    // Goto Review Panel
                    return options[1];
                }
            }
            /*
             * Apache Mode is Scalable
             */
            else {

                /*
                 * SH_RESOURCE_NAME should be filled with a SH resource's name
                 * from the Hostname Panel, if not it would be mean user had
                 * selected create new.
                 */
		String saRsName = null;
		if (guiModel != null) {
		    saRsName = (String)guiModel.getWizardValue(
			Util.SH_RESOURCE_NAME);
		} else {
		    saRsName = (String)cliModel.getWizardValue(
			Util.SH_RESOURCE_NAME);
		}
		if (saRsName == null) {
                    // Goto sharedaddress wizard
                    return options[0];
                }
                /*
                 * SH selected by the user so goto apache review panel
                 */
                else {
                    return options[1];
                }
            }
        }

        return null;
    }
}
