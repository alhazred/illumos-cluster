/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)RacDBStorageSelectionPanel.java	1.15	09/03/11 SMI"
 */


package com.sun.cluster.dswizards.oraclerac.database;

import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.rgm.ResourceData;

import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliUtil;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.ResourcePropertyUtil;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.oraclerac.database.*;
import com.sun.cluster.dswizards.oraclerac.invoker.RACInvokerCreator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import com.sun.cluster.model.RACModel;


/**
 * The Panel Class that gets a Hostname or list of Hostnames as input from the
 * User.
 */
public class RacDBStorageSelectionPanel extends RacDBBasePanel {


    private List resources_only = null;

    /**
     * Creates RacDBStorageSelectionPanel with the given name and Wizard state
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     */
    public RacDBStorageSelectionPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(name, model, wizardFlow, wizardi18n);
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.
     *		WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        title = wizardi18n.getString(
                "oraclerac10g.storageSelectionPanel.title");
        desc = wizardi18n.getString("oraclerac10g.storageSelectionPanel.desc");

        String help_text = wizardi18n.getString(
                "oraclerac10g.storageSelectionPanel.cli.help");
        String storageconfirm_text = wizardi18n.getString(
                "oraclerac10g.storageSelectionPanel.confirm");
        String heading1 = wizardi18n.getString(
                "oraclerac10g.storageSelectionPanel.heading1");
        String heading2 = wizardi18n.getString(
                "oraclerac10g.storageSelectionPanel.heading2");
        String storageError2 = wizardi18n.getString(
                "oraclerac.framework.storage.error2");
        String bchar = wizardi18n.getString("ttydisplay.back.char");
	String empty_text = wizardi18n.getString(
		"oraclerac10g.storageSelectionPanel.table.empty");
	String errorText = wizardi18n.getString(
		"oraclerac10g.storageSelectionPanel.error");

	String keyPrefix = (String)wizardModel.getWizardValue(
		Util.KEY_PREFIX);
        List storageScheme = (ArrayList) wizardModel.getWizardValue(
                keyPrefix + Util.ORF_STORAGE_MGMT_SCHEMES);

        boolean inputRequired = true;

        if ((storageScheme == null) || storageScheme.contains(Util.HWRAID)) {

            // its possible the user is trying to configure HWRAID
            // allow storage to be optional
            inputRequired = false;
        }

        Vector table = new Vector();
        fetchStorageResources();
        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        String choice = "";
        String storage_resources[] = null;
        String storage_rgs[] = null;

        while (!choice.equals(yes)) {

            // Title
            TTYDisplay.printTitle(title);

            List resources = (List) wizardModel.getWizardValue(Util.GDD_RSLIST);

            HashMap customTags = new HashMap();
            table = populateTable(resources);

            if ((table == null) || (table.size() == 0)) {

                if (!inputRequired) {
                    TTYDisplay.printInfo(storageError2);
                    TTYDisplay.clear(1);

                    String entry = TTYDisplay.promptForEntry(continue_txt);

                    if (entry.equals(bchar)) {
                        this.cancelDirection = true;

                        return;
                    }

                    choice = yes;

                    continue;
                }
            }

            // Description Para
            TTYDisplay.printSubTitle(desc);

	    customTags.put(dchar, done);
            customTags.put(rchar, refresh);

            String defSelections[] = (String[]) wizardModel.getWizardValue(
                    Util.SEL_STORAGE);

	    List defList = new ArrayList();

            if (defSelections != null) {

                for (int i = 0; i < defSelections.length; i++) {

                    // discovered list contains the selection
                    if (resources_only.contains(defSelections[i])) {
			defList.add(defSelections[i]);
                    }
                }
            }

            String subheadings[] = new String[] { heading1, heading2 };

	    String[] selections = TTYDisplay.create2DTable("", subheadings,
		table, table.size() / 2, 2, customTags, help_text,
		empty_text, false, defList);

            if (selections == null) {
                this.cancelDirection = true;
                return;
            }
	    List storageRSList = new ArrayList();
	    List storageRGList = new ArrayList();

	    List optionsList = Arrays.asList(selections);	
            if (optionsList.contains(rchar)) {
                fetchStorageResources();
                continue;
            } else {

		for (Iterator i = optionsList.iterator(); i.hasNext();) {
			String optionValue = (String) i.next();

                    try {
                        int value = Integer.parseInt(optionValue);

			// get the RS name and RG name
			String rsName = (String)table.get((value * 2) - 2);
			String rgName = (String)table.get((value * 2) - 1);
			if (!storageRSList.contains(rsName)) {
			    storageRSList.add(rsName);
			}

			if (!storageRGList.contains(rgName)) {
			    storageRGList.add(rgName);
			}
                    } catch (NumberFormatException nfe) {
                        // Custom Tags
                    }
                }

                // Store selection
                if (optionsList.contains(dchar)) {
		    if (inputRequired) {
			if (storageRSList == null || storageRSList.size() == 0) {
			    // print Error
			    TTYDisplay.printInfo(errorText);
			    TTYDisplay.clear(1);
			    String entry = TTYDisplay.promptForEntry(continue_txt);

			    if (entry.equals(bchar)) {
				this.cancelDirection = true;
				return;
			    }

			    continue;
			}
		    }

		    if (storageRSList != null) {
			storage_resources = (String[])storageRSList.toArray(new String[0]);
		    }

		    if (storageRGList != null) {
			storage_rgs = (String[])storageRGList.toArray(new String[0]);
		    }	

		    choice = yes;

		    // Store the Values in the WizardModel
		    wizardModel.setWizardValue(Util.SEL_STORAGE, storage_resources);
		    wizardModel.setWizardValue(Util.SEL_STORAGE_RG, storage_rgs);
		}
	    }
        }
}

    private void fetchStorageResources() {

        String nodeList[] = (String[]) wizardModel.getWizardValue(
	    Util.RG_NODELIST);

        // get racModel
        String nodeEndPoint = (String) wizardModel.getWizardValue(
	    Util.NODE_TO_CONNECT);

	RACModel racModel = RACInvokerCreator.getRACModel();

        String props[] = { Util.SCAL_DEVGROUP_RSLIST };
        HashMap helperData = new HashMap();
        helperData.put(Util.RG_NODELIST, nodeList);

        String selZoneCluster = CliUtil.getZoneClusterName(wizardModel);
	helperData.put(Util.SEL_ZONE_CLUSTER, selZoneCluster);

        HashMap hashMap = (HashMap) racModel.discoverPossibilities(
	    null, nodeEndPoint, props, helperData);

        List resources = null;
        List scalDevGroupList = (List) hashMap.get(Util.SCAL_DEVGROUP_RSLIST);

	props = new String[] { Util.SCAL_MOUNTPOINT_RSLIST };
        hashMap = (HashMap) racModel.discoverPossibilities(
	    null, nodeEndPoint, props, helperData);
        List scalMountPtList = (List) hashMap.get(Util.SCAL_MOUNTPOINT_RSLIST);

        if (scalDevGroupList != null) {

            if (scalMountPtList != null) {
                resources = scalDevGroupList;
                resources.addAll(scalMountPtList);
            } else {
                resources = scalDevGroupList;
            }
        } else {

            if (scalMountPtList != null) {
                resources = scalMountPtList;
            }
        }

        wizardModel.setWizardValue(Util.SCAL_DEVGROUP_RSLIST, scalDevGroupList);
        wizardModel.setWizardValue(Util.SCAL_MOUNTPOINT_RSLIST,
            scalMountPtList);

        wizardModel.setWizardValue(Util.GDD_RSLIST, resources);
    }

    private Vector populateTable(List list) {
        Vector table = new Vector();
        resources_only = new ArrayList();

        if (list != null) {
            int size = list.size();

            for (int i = 0; i < size; i++) {
		ResourceData rsData = (ResourceData)list.get(i);

                if (rsData != null) {
		    String rsName = rsData.getName();
		    String rgName = rsData.getResourceGroupName();
                    table.add(rsName);
                    table.add(rgName);

                    if (!resources_only.contains(rsName)) {
                        resources_only.add(rsName);
                    }
                }
            }
        }

        return table;
    }
}
