/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)LogicalHostWizardConstants.java 1.12     08/05/20 SMI"
 */

package com.sun.cluster.dswizards.logicalhost;

/**
 * This class holds all the constants for the LogicalHostWizard
 */
public class LogicalHostWizardConstants {

    // Package Name
    public static final String PKG_NAME = "SUNWscamasa";

    // State File
    public static final String LH_STATE_FILE =
        "/opt/cluster/lib/ds/history/LHStateFile";

    public static final String SA_STATE_FILE =
        "/opt/cluster/lib/ds/history/SHStateFile";

    // Flow XML
    public static final String LH_FLOW_XML =
        "/usr/cluster/lib/ds/logicalhost/LogicalHostWizardFlow.xml";
    public static final String SA_FLOW_XML =
        "/usr/cluster/lib/ds/sharedaddress/SharedAddressWizardFlow.xml";

    // Constants to be used to aid Wizard Flow
    public static final String IPMPGROUPNAMES = "IPMPGroupNames";
    public static final String WIZARDTYPE = "WIZARDTYPE";

    // Constants to be used for the step names
    public static final String HOSTNAMEENTRY = "hostNameEntryPanel";
    public static final String IPMPGROUPENTRY = "ipmpGroupEntryPanel";
    public static final String INTROPANEL = "introPanel";
    public static final String REVIEWPANEL = "reviewPanel";
    public static final String SUMMARYPANEL = "summaryPanel";
    public static final String PREFERREDNODEPANEL = "preferredNodePanel";
    public static final String RESULTSPANEL = "resultsPanel";

    public static final String SEL_NETIFLIST = "Selected_NetifList";

    /**
     * Creates a new instance of LogicalHostWizardConstants
     */
    public LogicalHostWizardConstants() {
    }
}
