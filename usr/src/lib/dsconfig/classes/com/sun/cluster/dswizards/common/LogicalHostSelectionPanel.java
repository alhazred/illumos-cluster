/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)LogicalHostSelectionPanel.java	1.17	08/05/20 SMI"
 */

package com.sun.cluster.dswizards.common;

//J2SE
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

// JATO
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.ContainerView;
import com.iplanet.jato.view.DisplayField;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.RequestHandlingViewBase;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.DisplayEvent;

// CMASS Classes
import com.sun.cluster.agent.dataservices.utils.InfrastructureMBean;
import com.sun.cluster.agent.dataservices.utils.Util;

// SPM
import com.sun.cluster.spm.common.GenericWizard;
import com.sun.cluster.spm.common.SpmUtil;
import com.sun.cluster.spm.rgm.ResourceGroupUtil;
import com.sun.cluster.spm.task.wizard.ConfigureDataServiceWizard;

//Lockhart
import com.sun.web.ui.model.CCActionTableModel;
import com.sun.web.ui.model.CCActionTableModelInterface;
import com.sun.web.ui.model.CCPropertySheetModel;
import com.sun.web.ui.view.alert.CCAlertInline;
import com.sun.web.ui.view.html.CCHiddenField;
import com.sun.web.ui.view.html.CCRadioButton;
import com.sun.web.ui.view.html.CCStaticTextField;
import com.sun.web.ui.view.propertysheet.CCPropertySheet;
import com.sun.web.ui.view.table.CCActionTable;
import com.sun.web.ui.view.wizard.CCWizardPage;
import com.sun.web.ui.model.wizard.WizardEvent;

/**
 * This panel handles selection of LH resources and creation of new LH resources
 */
public class LogicalHostSelectionPanel extends RequestHandlingViewBase
    implements CCWizardPage {

    /** Wizard step's title dispayed first on the right */
    public static final String STEP_TITLE = "logicalhostSelectionPanel.title";

    /** Wizard step's name displayed on the left */
    public static final String STEP_TEXT = "logicalhostSelectionPanel.title";

    /** Prefix to retrieve the wizard's step contextal help */
    public static final String STEP_HELP_PREFIX =
        "logicalhostSelectionPanel.help";

    /** JSP file used for the wizard's step */
    public static final String PAGELET_URL =
        "/jsp/task/wizard/TaskWizardStep.jsp";

    /** Child element for the hidden field */
    public static final String CHILD_CLOSE = "CloseWizardHiddenField";

    /** Child element to display the Alert Inline */
    public static final String CHILD_ALERT = "Alert";

    /** Name of the property sheet child */
    public static final String CHILD_PROPERTYSHEET = "PropertySheet";

    /** XML file containing the property sheet's model */
    public static final String PROPERTYSHEET_XML_FILE =
        GenericDSWizard.COMMON_XML_PATH + "logicalHostSelectionPanel.xml";

    /** XML file containing the property sheet table model */
    public static final String LH_RS_TABLE_XML =
        GenericDSWizard.COMMON_XML_PATH + "lhrsNameTable.xml";

    public static final String CHILD_LH_RS_TABLE = "LHRSNameTable";
    public static final String CHILD_LH_RS_TABLE_VIEW =
        "LHRSNameTableTiledView";
    public static final String CHILD_NEWEXISTS_VALUE = "Choice";
    protected static final String SELECT_EXISTING = "1";
    protected static final String CREATE_NEW = "2";

    // Private Members
    private static final int TABLE_NO_OF_COLUMNS = 3;
    private static final String TABLE_I18N_HEADER_PREFIX =
        "logicalhostSelectionPanel.table.colheader";
    private static final String TABLE_HEADER_PREFIX = "LhCol";
    private static final String TABLE_COLUMN_LH_HOSTNAME = "hostnameList";
    private static final String TABLE_COLUMN_RS_NAME = "rsname";
    private static final String TABLE_COLUMN_RG_NAME = "rgname";
    private static final String LHRS_SELECTED_INT = "LH_RS_SELECTED_INT";
    private static final String CHOICE_LABEL = "LHRSNameChoice";
    private static final String CHOICE_HELP = "ChoiceHelp";

    // Property Sheet Model
    private CCPropertySheetModel propertySheetModel = null;

    // Protected Members
    protected CCActionTableModel lhRSTableModel = null;
    protected DSConfigWizardModel wizardModel = null;
    protected InfrastructureMBean mbean = null;
    protected boolean enableMulipleSelection = true;

    /**
     * Construct an instance with the specified model and logical page name.
     *
     * @param  parent  The parent view of this object.
     * @param  model  This view's model.
     * @param  name  Logical page name.
     */
    public LogicalHostSelectionPanel(View parent, Model model, String name) {
        super(parent, name);
        setDefaultModel(model);
        wizardModel = (DSConfigWizardModel) getDefaultModel();
        createPropertySheetModel();
        registerChildren();
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Child manipulation methods
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    protected void registerChildren() {
        registerChild(CHILD_CLOSE, CCHiddenField.class);
        registerChild(CHILD_ALERT, CCAlertInline.class);
        registerChild(CHILD_PROPERTYSHEET, CCPropertySheet.class);
        registerChild(CHILD_LH_RS_TABLE_VIEW, LHRSTableTiledView.class);
        propertySheetModel.registerChildren(this);
        lhRSTableModel.registerChildren(this);
    }

    protected View createChild(String name) {

        if (name.equals(CHILD_PROPERTYSHEET)) {
            CCPropertySheet child = new CCPropertySheet(this,
                    propertySheetModel, name);

            return child;

        } else if ((lhRSTableModel != null) &&
                name.equals(CHILD_LH_RS_TABLE_VIEW)) {
            LHRSTableTiledView child = new LHRSTableTiledView(this,
                    lhRSTableModel, name);

            return child;

        } else if (name.equals(CHILD_CLOSE)) {
            CCHiddenField child = new CCHiddenField(this, name, null);

            return child;

        } else if (name.equals(CHILD_ALERT)) {
            CCAlertInline child = new CCAlertInline(this, name, null);

            return child;

        } else if ((propertySheetModel != null) &&
                propertySheetModel.isChildSupported(name)) {

            // Create child from property sheet model.
            View child = propertySheetModel.createChild(this, name);

            if (name.equals(CHILD_LH_RS_TABLE)) {
                ((CCActionTable) child).setTiledView((ContainerView) getChild(
                        CHILD_LH_RS_TABLE_VIEW));
            }

            return child;

        } else {
            throw new IllegalArgumentException(
                " LogicalHostSelectionPanel Invalid child name [" + name + "]");
        }
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Display methods
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    private void createPropertySheetModel() {

        try {
            FileInputStream fileInputStream = new FileInputStream(
                    PROPERTYSHEET_XML_FILE);
            String xmlFileString = ConfigureDataServiceWizard.getXMLString(
                    fileInputStream);
            fileInputStream.close();

            propertySheetModel = new CCPropertySheetModel();
            propertySheetModel.setDocument(xmlFileString);
            lhRSTableModel = (CCActionTableModel) wizardModel.getWizardValue(
                    CHILD_LH_RS_TABLE);

            CCActionTableModel model = (CCActionTableModel) wizardModel
                .getWizardValue(CHILD_LH_RS_TABLE);
            String hostNameList[] = (String[]) wizardModel.getWizardValue(
                    Util.HOSTNAMELIST);

            if ((model == null) || (hostNameList != null)) {
                FileInputStream tableModelStream = new FileInputStream(
                        LH_RS_TABLE_XML);
                String tableModelString = ConfigureDataServiceWizard
                    .getXMLString(tableModelStream);
                tableModelStream.close();
                lhRSTableModel = new CCActionTableModel();
                lhRSTableModel.setDocument(tableModelString);

                for (int i = 0; i < TABLE_NO_OF_COLUMNS; i++) {
                    lhRSTableModel.setActionValue(TABLE_HEADER_PREFIX + i,
                        TABLE_I18N_HEADER_PREFIX + "." + i);
                }
            } else {
                lhRSTableModel = model;
            }

            // Change Texts to Shared Address if RG_MODE is set to SCALABLE
            String mode = (String) wizardModel.getWizardValue(Util.RG_MODE);

            if ((mode != null) && mode.equals(Util.SCALABLE_RG_MODE)) {
                lhRSTableModel.setTitle(
                    "sharedAddressSelectionPanel.table.title");
                lhRSTableModel.setEmpty(
                    "sharedAddressSelectionPanel.table.empty");
                lhRSTableModel.setActionValue(TABLE_HEADER_PREFIX + 2,
                    "logicalhostSelectionPanel.table.colheader.sharedaddress");
            }

            propertySheetModel.setModel(CHILD_LH_RS_TABLE, lhRSTableModel);

        } catch (Exception e) {
            e.printStackTrace();

            return;
        }
    }

    protected void populatePropertySheetModel() {
        Object tmpObj = null;
        List allLHRG = null;
        int i;

        try {

            CCActionTable lhRSTableChild = (CCActionTable) getChild(
                    CHILD_LH_RS_TABLE);
            CCActionTableModel model = (CCActionTableModel) wizardModel
                .getWizardValue(CHILD_LH_RS_TABLE);

            lhRSTableChild.resetStateData();

            if ((model != null) && (model != lhRSTableModel)) {
                lhRSTableModel.clear();

                for (i = 0; i < model.getNumRows(); i++) {
                    model.setRowIndex(i);
                    lhRSTableModel.appendRow();
                    lhRSTableModel.setValue(TABLE_COLUMN_LH_HOSTNAME,
                        model.getValue(TABLE_COLUMN_LH_HOSTNAME));
                    lhRSTableModel.setValue(TABLE_COLUMN_RS_NAME,
                        model.getValue(TABLE_COLUMN_RS_NAME));
                    lhRSTableModel.setValue(TABLE_COLUMN_RG_NAME,
                        model.getValue(TABLE_COLUMN_RG_NAME));
                }
            }

            if (model == null) {
                refreshRGList();
                allLHRG = (List) wizardModel.getWizardValue(Util.LH_RGLIST);

                // Fill table model from discoverd LH resource names
                if (allLHRG != null) {
                    int size = allLHRG.size();

                    for (i = 0; i < size; i++) {
                        lhRSTableModel.appendRow();

                        HashMap map = (HashMap) allLHRG.get(i);
                        String rsName = (String) map.get(Util.RS_NAME);
                        String rgName = (String) map.get(Util.RG_NAME);
                        String hostnames[] = (String[]) map.get(
                                Util.LH_HOSTLIST_EXT_PROP);

                        StringBuffer hostNameBuffer = null;

                        if (hostnames != null) {

                            for (int j = 0; j < hostnames.length; j++) {

                                if (hostNameBuffer == null) {
                                    hostNameBuffer = new StringBuffer();
                                    hostNameBuffer.append(hostnames[j]);
                                } else {
                                    hostNameBuffer.append(Util.COMMA);
                                    hostNameBuffer.append(hostnames[j]);
                                }
                            }

                            lhRSTableModel.setValue(TABLE_COLUMN_LH_HOSTNAME,
                                hostNameBuffer.toString());
                        }

                        lhRSTableModel.setValue(TABLE_COLUMN_RS_NAME, rsName);
                        lhRSTableModel.setValue(TABLE_COLUMN_RG_NAME, rgName);
                    }
                }
            }

            String hostNameList[] = (String[]) wizardModel.getWizardValue(
                    Util.HOSTNAMELIST);

            WizardI18N wizardi18n = GenericDSWizard.getWizardI18N();
            StringBuffer alertString = null;

            if (hostNameList != null) {

                // Handle duplicates
                String addedHostNames[] = (String[]) wizardModel.getWizardValue(
                        Util.SEL_HOSTNAMES);

                if (addedHostNames != null) {
                    List addedHostList = Arrays.asList(addedHostNames);
                    ArrayList newNodes = null;

                    for (int x = 0; x < hostNameList.length; x++) {

                        if (!addedHostList.contains(hostNameList[x])) {

                            if (newNodes == null) {
                                newNodes = new ArrayList();
                            }

                            newNodes.add(hostNameList[x]);
                        }
                    }

                    if (newNodes != null) {
                        hostNameList = (String[]) newNodes.toArray(
                                new String[0]);
                    } else {
                        hostNameList = null;
                    }
                }
            }

            if (hostNameList != null) {

                for (i = 0; i < hostNameList.length; i++) {

                    if (alertString == null) {
                        alertString = new StringBuffer();
                        alertString.append(hostNameList[i]);
                    } else {
                        alertString.append(Util.COMMA);
                        alertString.append(hostNameList[i]);
                    }
                }

                lhRSTableModel.appendRow();
                lhRSTableModel.setValue(TABLE_COLUMN_LH_HOSTNAME,
                    alertString.toString());
                lhRSTableModel.setValue(TABLE_COLUMN_RS_NAME, "");
                lhRSTableModel.setValue(TABLE_COLUMN_RG_NAME, "");
                lhRSTableModel.setRowSelected(true);
                wizardModel.setWizardValue(Util.HOSTNAMELIST, null);
            }

            // Change Texts to Shared Address if RG_MODE is set to SCALABLE
            String mode = (String) wizardModel.getWizardValue(Util.RG_MODE);

            if (alertString != null) {
                CCAlertInline alert = (CCAlertInline) getChild(CHILD_ALERT);
                alert.setValue(CCAlertInline.TYPE_INFO);

                if ((mode != null) && mode.equals(Util.SCALABLE_RG_MODE)) {
                    String summary_text = wizardi18n.getString(
                            "shlistpanel.alert.summary",
                            new String[] { alertString.toString() });
                    alert.setSummary(summary_text);
                } else {
                    String summary_text = wizardi18n.getString(
                            "lhlistpanel.alert.summary",
                            new String[] { alertString.toString() });
                    alert.setSummary(summary_text);
                }
            }

            if (!enableMulipleSelection) {
                lhRSTableModel.setSelectionType(
                    CCActionTableModelInterface.SINGLE);
            }

            if ((mode != null) && mode.equals(Util.SCALABLE_RG_MODE)) {
                CCStaticTextField hostLabel = (CCStaticTextField) getChild(
                        CHOICE_LABEL);
                hostLabel.setValue("sharedAddressSelectionPanel.choice");

                CCStaticTextField choiceHelp = (CCStaticTextField) getChild(
                        CHOICE_HELP);
                choiceHelp.setValue(
                    "logicalhostSelectionPanel.choice.new.help.sharedaddress");
            }

            lhRSTableChild.restoreStateData();
            wizardModel.setWizardValue(CHILD_LH_RS_TABLE, lhRSTableModel);

            Integer userSelections[] = (Integer[]) wizardModel.getWizardValue(
                    LHRS_SELECTED_INT);

            if (userSelections != null) {

                for (i = 0; i < userSelections.length; i++) {
                    lhRSTableModel.setRowSelected(userSelections[i].intValue(),
                        true);
                }
            }

            // Set Option Button Value
            DisplayField childOptBtn = (DisplayField) getChild(
                    CHILD_NEWEXISTS_VALUE);

            if (lhRSTableModel.getNumRows() > 0) {
                childOptBtn.setValue(SELECT_EXISTING);
            } else {
                childOptBtn.setValue(CREATE_NEW);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Get the pagelet to use for the rendering of this instance.
     *
     * @return  The pagelet to use for the rendering of this instance.
     */
    public String getPageletUrl() {
        return PAGELET_URL;
    }

    /**
     * This method is called when the page is beginning its display.
     *
     * @param  event  DisplayEvent is passed in by the framework.
     */
    public void beginComponentDisplay(DisplayEvent event)
        throws ModelControlException {
        populatePropertySheetModel();

        CCHiddenField closeWizard = (CCHiddenField) getChild(CHILD_CLOSE);
        closeWizard.setValue("false");
    }


    public boolean previous(WizardEvent event) {
	wizardModel.setWizardValue(LHRS_SELECTED_INT, null);
        wizardModel.setWizardValue(Util.SEL_HOSTNAMES, null);
	wizardModel.setWizardValue(CHILD_LH_RS_TABLE, null);
	refreshRGList();
	return true;
    }

    /**
     * Step data validation is done here. This method is called from the
     * nextStep() of the wizard.
     *
     * The event error message and severity are set in case of errors.
     */
    protected HashMap getSelections() {
        ArrayList selectedResources = new ArrayList();
        ArrayList selectedRGs = new ArrayList();
        ArrayList selectedEntries = new ArrayList();
        HashMap returnValues = new HashMap();
        Integer selectedRows[] = null;

        // If define new is selected, the table is disabled so
        // do not read the table selections.
        String qtype = (String)
            ((CCRadioButton) getChild(CHILD_NEWEXISTS_VALUE)).getValue();

        if (qtype.equals(CREATE_NEW)) {
            returnValues.put(Util.CREATE_NEW, qtype);
            returnValues.put(Util.SEL_HOSTNAMES, null);
            returnValues.put(Util.SEL_RS, null);
            returnValues.put(Util.SEL_RG, null);

            return returnValues;
        }

        try {
            CCActionTable child = (CCActionTable) getChild(CHILD_LH_RS_TABLE);
            child.restoreStateData();
            lhRSTableModel = (CCActionTableModel) wizardModel.getWizardValue(
                    CHILD_LH_RS_TABLE);
            selectedRows = lhRSTableModel.getSelectedRows();

            for (int i = 0; i < selectedRows.length; i++) {
                lhRSTableModel.setLocation(selectedRows[i].intValue());

                String rsName = (String) lhRSTableModel.getValue(
                        TABLE_COLUMN_RS_NAME);

                if ((rsName != null) && (rsName.trim().length() > 0)) {

                    if (!selectedResources.contains(rsName)) {
                        selectedResources.add(rsName);
                    }
                }

                String rgName = (String) lhRSTableModel.getValue(
                        TABLE_COLUMN_RG_NAME);

                if ((rgName != null) && (rgName.trim().length() > 0)) {

                    if (!selectedRGs.contains(rgName)) {
                        selectedRGs.add(rgName);
                    }
                }

                String hostName = (String) lhRSTableModel.getValue(
                        TABLE_COLUMN_LH_HOSTNAME);

                if (((rsName == null) || (rsName.trim().length() == 0)) &&
                        ((rgName == null) || (rgName.trim().length() == 0))) {

                    // implies this is a new LH resource being created.
                    if ((hostName != null) && (hostName.trim().length() > 0)) {

                        if (!selectedEntries.contains(hostName)) {
                            selectedEntries.add(hostName);
                        }
                    }
                }
            }
        } catch (Exception mce) {
            mce.printStackTrace();
        }

        int i1 = 0;
        int size = selectedRGs.size();
        boolean error = false;
        String rgName1 = null;
        String rgName2 = null;
        String rgName = null;
        int rs_index1 = -1;
        int rs_index2 = -1;

        if (size == 1) {
            rgName = (String) selectedRGs.get(0);
        } else {

            while ((i1 < (size - 1)) && !error) {
                rgName1 = (String) selectedRGs.get(i1);
                rs_index1 = i1;

                for (int i2 = (i1 + 1); i2 < size; i2++) {
                    rgName2 = (String) selectedRGs.get(i2);
                    rs_index2 = i2;

                    if ((rgName1 != null) && (rgName2 != null)) {

                        if (!rgName1.equals(rgName2)) {
                            error = true;
                        }
                    }
                }

                if (!error) {
                    i1++;
                }
            }

            rgName = (rgName1 != null) ? rgName1 : rgName2;
        }

        if (error) {
            WizardI18N wizardi18n = GenericDSWizard.getWizardI18N();
            String errMsg = wizardi18n.getString(
                    "logicalhostSelectionPanel.rgmismatch.error",
                    new String[] {
                        (String) selectedResources.get(rs_index1),
                        (String) selectedResources.get(rs_index2)
                    });

            CCAlertInline alert = (CCAlertInline) getChild(CHILD_ALERT);
            alert.setType(CCAlertInline.TYPE_ERROR);
            alert.setSummary(wizardi18n.getString(
                    "logicalhostSelectionPanel.rgmismatch.summary"));
            alert.setDetail(errMsg);

            // Clear the selection
            return null;
        }

        if (qtype.equals(SELECT_EXISTING) && (selectedRows.length == 0)) {

            // error condition. User chose select existing but did not select
            // a row
            WizardI18N wizardi18n = GenericDSWizard.getWizardI18N();
            CCAlertInline alert = (CCAlertInline) getChild(CHILD_ALERT);
            alert.setType(CCAlertInline.TYPE_ERROR);

            String mode = (String) wizardModel.getWizardValue(Util.RG_MODE);

            if ((mode != null) && mode.equals(Util.SCALABLE_RG_MODE)) {
                alert.setSummary(wizardi18n.getString(
                "logicalhostSelectionPanel.noselection.summary.sharedaddress"));
                alert.setDetail(wizardi18n.getString(
                "logicalhostSelectionPanel.noselection.error.sharedaddress"));
            } else {
                alert.setSummary(wizardi18n.getString(
                        "logicalhostSelectionPanel.noselection.summary"));
                alert.setDetail(wizardi18n.getString(
                        "logicalhostSelectionPanel.noselection.error"));
            }

            // Clear the selection
            return null;

        }

        String selectedHostnames[] = null;

        if (selectedEntries.size() > 0) {
            selectedHostnames = (String[]) selectedEntries.toArray(
                    new String[0]);
        }

        Vector vselectedResources = new Vector(selectedResources);

        // Need to set this for LH wizard
        wizardModel.setWizardValue(LHRS_SELECTED_INT, selectedRows);
        wizardModel.setWizardValue(Util.SEL_HOSTNAMES, selectedHostnames);

        returnValues.put(Util.CREATE_NEW, qtype);
        returnValues.put(Util.SEL_HOSTNAMES, selectedHostnames);
        returnValues.put(Util.SEL_RS, vselectedResources);
        returnValues.put(Util.SEL_RG, rgName);

        return returnValues;
    }

    protected void refreshRGList() {

        if (mbean == null) {
            mbean = GenericDSWizard.getInfrastructureMBean();
        }

        String rgName = (String) wizardModel.getWizardValue(Util.SEL_RG);
        HashMap helperData = new HashMap();

        if (rgName == null) {
            String nodeList[] = (String[]) wizardModel.getWizardValue(
                    Util.NODELIST);

            // construct a query and fetch resources
            helperData.put(Util.RG_NODELIST, nodeList);
        } else {

            // get all logical host resources in resource group rgName
            helperData.put(Util.RG_NAME, rgName);
        }

        HashMap map = mbean.discoverPossibilities(
                new String[] { Util.LH_RGLIST }, helperData);

        List rgs = (List) map.get(Util.LH_RGLIST);
        wizardModel.setWizardValue(Util.LH_RGLIST, rgs);
    }

    public class LHRSTableTiledView extends RequestHandlingTiledViewBase {

        private CCActionTableModel model = null;

        public LHRSTableTiledView(View parent, CCActionTableModel model,
            String name) {
            super(parent, name);
            this.model = model;
            registerChildren();
            setPrimaryModel(model);
        }

        protected void registerChildren() {
            model.registerChildren(this);
        }

        protected View createChild(String name) {

            if (model.isChildSupported(name)) {
                View child = model.createChild(this, name);

                return child;

            } else {
                throw new IllegalArgumentException("Invalid child name [" +
                    name + "]");
            }
        }
    }

}
