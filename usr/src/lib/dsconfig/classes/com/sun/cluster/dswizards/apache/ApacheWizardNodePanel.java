/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ApacheWizardNodePanel.java 1.13     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.apache;

import com.sun.cluster.agent.dataservices.apache.ApacheMBean;

// CMASS
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.NodeSelectionPanel;

// Wizard Common
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.management.remote.JMXConnector;


/**
 * This panel gets the Preferred Node from the User
 */
public class ApacheWizardNodePanel extends NodeSelectionPanel {

    private String apache;

    /**
     * Creates a new instance of ApacheWizardNodePanel
     */
    public ApacheWizardNodePanel() {
    }

    /**
     * Creates an PreferredNode Panel with the given name and manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public ApacheWizardNodePanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates an PreferredNode Panel for the wizard with the given name and
     * associates it with the WizardModel and WizardFlow.
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     */
    public ApacheWizardNodePanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(name, model, wizardFlow, wizardi18n);
        initStrings();
    }


    private void initStrings() {
        desc = wizardi18n.getString("apache.nodePanel.instruction");
        apache = wizardi18n.getString("dswizards.apache.wizard");
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        String nodeList[] = displayPanel(ApacheMBean.class, apache);

        if (nodeList == null) {
            this.cancelDirection = true;

            return;
        }

        wizardModel.setWizardValue(Util.RG_NODELIST, nodeList);
        initWizardModel();
    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }

    /**
     * Initializes the Wizard Model by filling with the PropertyNames and
     * Discovered values. The Previous Values entered by the user are retrieved
     * and stored in the previous context.
     */
    public void initWizardModel() {

        // Populate the Wizard's default context
        // Contact the ApacheMBean and Get All Properties
        JMXConnector localConnection = getConnection(Util.getClusterEndpoint());
        String nodeList[] = (String[]) wizardModel.getWizardValue(
                Util.RG_NODELIST);
        ApacheMBean mBean = null;

        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);
        JMXConnector connector = getConnection(nodeEndPoint);
        mBean = ApacheWizardCreator.getApacheMBeanOnNode(connector);

        if (mBean != null) {

            // Get the Discoverable Properties For Apache Wizard
            String propertyNames[] = { Util.PROPERTY_NAMES };
            HashMap discoveredMap = mBean.discoverPossibilities(propertyNames,
                    null);

            // Copy the Data to the WizardModel
            propertyNames = new String[discoveredMap.size()];

            Set entrySet = discoveredMap.entrySet();
            Iterator setIterator = entrySet.iterator();
            int i = 0;

            while (setIterator.hasNext()) {
                Map.Entry entry = (Map.Entry) setIterator.next();
                wizardModel.setWizardDefaultValue((String) entry.getKey(),
                    entry.getValue());
                propertyNames[i++] = (String) entry.getKey();
            }

            wizardModel.setWizardValue(Util.PROPERTY_NAMES, propertyNames);

            // Set the property names to be serialized
            List toBeSerialized = new ArrayList();
            toBeSerialized.add(Util.APACHE_MODE);
            toBeSerialized.add(Util.RG_NODELIST);
            toBeSerialized.add(Util.APACHE_CONF_FILE);
            wizardModel.setWizardValue(Util.WIZARD_STATE, toBeSerialized);
        }
    }
}
