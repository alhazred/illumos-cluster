/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)SapWebasConstants.java 1.12     08/05/20 SMI"
 */

package com.sun.cluster.dswizards.sapwebas;

/**
 * This class holds all the constants for the SAP Wizard
 */
public class SapWebasConstants {

    /*
     * Constants to be used in the wizard flow
     */

    public static final String SAP_PKG_NAME = "SUNWsapwebas";
    public static final String DISCOVERD_SIDS = "discovered_sid";
    public static final String DISCOVERD_ENQ_I_NO = "discovered_enq_i_no";
    public static final String DISCOVERD_SCS_I_NO = "discovered_scs_i_no";
    public static final String DISCOVERD_ENQ_PROFILES =
        "discovered_enq_profiles";
    public static final String DISCOVERD_REP_PROFILES =
        "discovered_rep_profiles";
    public static final String DISCOVERD_SCS_NAMES = "discovered_scs_names";
    public static final String NEW_HASP_CS = "create_new_hasp_for_cs";
    public static final String REP_DISC_LH_RESOURCES =
        "discovered_lh_res_for replica";
    public static final String REP_DISC_HASP_RESOURCES =
        "discovered_hasp_res_for_replica";
    public static final String INSIDE_REP_COMPONENT = "inside_replica_wizard";
    public static final String NEW_LH_CS = "create_new_lh_for_cs";
    public static final String IS_NEW_RG_CS = "is_newrg_for_lh_for_cs";
    public static final String IS_NEW_RG_REP = "is_newrg_for_lh_for_rep";
    public static final String DISC_CLUSTER_OBJ =
        "discovered_cluster_objects_for_sap";
    public static final String DISCOVERD_DB_RESOURCES =
        "discovered_database_res";
    public static final String SELECT_EXISTING = "1";
    public static final String CREATE_NEW = "2";


    // Constants used in GUI only
    public static final String DISC_OR_OVERRIDE = "discovered_or_override";
    public static final String DISC_OPTION = "disc";
    public static final String OVERRIDE_OPTION = "new";
    public static final String CS_AVAIL_NODELIST = "available_nodelist_for_cs";
    public static final String REP_AVAIL_NODELIST =
        "available_nodelist_for_replica";
    public static final String ENQINO_DISCORNEW =
        "Enq_ino_discovered_or_override";
    public static final String ENQPROF_DISCORNEW =
        "Enq_prof_discovered_or_override";
    public static final String SCSINO_DISCORNEW =
        "Scs_ino_discovered_or_override";
    public static final String SCSNAME_DISCORNEW =
        "Scs_name_discovered_or_override";
    public static final String REPPROF_DISCORNEW =
        "replica_profile_discovered_or_override";
    public static final String DB_DISCORNEW = "dbname_discovered_or_override";
    public static final String CS_HASP_CREATE =
        "create_or_existing_choice_for_cs";
    public static final String REP_HASP_CREATE =
        "create_or_existing_choice_for_rep";
    public static final String CS_LH_CREATE =
        "create_or_existing_choice_forlh_for_cs";
    public static final String REP_LH_CREATE =
        "create_or_existing_choice_forlh_for_rep";

    // Panel names
    public static final String PANEL_0 = "IntroductionPanel";
    public static final String PANEL_1 = "SidPanel";
    public static final String PANEL_2 = "ComponentSelectionPanel";
    public static final String PANEL_3 = "CentralServicesInputPanel";
    public static final String PANEL_4 = "CentralServicesDetailsPanel";
    public static final String PANEL_5 = "ReplicaPanel";
    public static final String PANEL_6 = "SapReviewPanel";
    public static final String STORAGE_SELECT = "ChooseHASPanel";
    public static final String REP_STORAGE_SELECT = "RepChooseHASPanel";
    public static final String NODE_SELECT = "NodeSelectionPanel";
    public static final String HOST_SELECT = "ChooseLHPanel";
    public static final String REP_HOST_SELECT = "RepChooseLHPanel";
    public static final String REP_NODE_SELECT = "ReplicaNodeSelectionPanel";
    public static final String DB_SELECT = "SelectDBPanel";
    public static final String SUMMARY_PANEL = "SapSummaryPanel";
    public static final String RESULTS_PANEL = "SapResultsPanel";
    public static final String CS_ZONES_PRESENT = "CsZonesPresent";
    public static final String REP_ZONES_PRESENT = "RepZonesPresent";
}
