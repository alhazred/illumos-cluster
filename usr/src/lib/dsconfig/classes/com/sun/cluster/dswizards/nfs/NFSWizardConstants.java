/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)NFSWizardConstants.java 1.11     08/05/20 SMI"
 */

package com.sun.cluster.dswizards.nfs;

/**
 * This class holds all the constants for the NFSWizard
 */
public class NFSWizardConstants {

    /*
     * Constants to be used in the wizard flow
     */

    public static final String WIZARDI18N = "WIZARDI18N";
    public static final String RG_POSTFIX = "-rg";
    public static final String RS_POSTFIX = "-rs";
    public static final String NFS_PREFIX = "nfs";
    public static final String NFS_RES_ID = "NFS";
    public static final String NFS_SHOW_ADVANCED = "ShowAdvanced";

    public static final String SHARE = "share";
    public static final String SHARE_D_OPT = "-d";
    public static final String SHARE_F_OPT = "-F nfs";
    public static final String SHARE_O_OPT = "-o";
    public static final String SHARE_SEC_OPT = "sec=";
    public static final String NONE_OPT = "none";
    public static final String SHARE_NOSUID_OPT = "nosuid";
    public static final String SHOWLHWIZ = "showlhwiz";
    public static final String NEWRGFLAG = "newrgflag";
    public static final String SHOWHASPWIZ = "showhaspwiz";
    public static final String SHOWPATHPREFIX = "showpathprefix";
    public static final String SHOWLHREVPANEL = "showlhrevpanel";
    public static final String NEW_HOSTNAMES_DEFINED =
        "nfs_new_hostnames_defined";

    // Flow xml
    public static final String NFS_WIZARD_FLOW_XML =
        "/usr/cluster/lib/ds/nfs/NFSWizardFlow.xml";

    // State File
    public static final String NFS_WIZARD_STATE_FILE =
        "/opt/cluster/lib/ds/history/NFSWizardState";

    // Package Name
    public static final String NFS_PKG_NAME = "SUNWscnfs";

    // Panel names
    public static final String PANEL_0 = "nfsIntroPanel";
    public static final String PANEL_1 = "nfsNodeSelectionPanel";
    public static final String PANEL_2 = "nfsLhResNameEntryPanel";
    public static final String PANEL_3 = "nfsHaspResNameEntryPanel";
    public static final String PANEL_4 = "nfsPathPrefixEntryPanel";
    public static final String PANEL_5 = "nfsShareOptionsEntryPanel";
    public static final String PANEL_6 = "nfsReviewPanel";
    public static final String PANEL_7 = "nfsSummaryPanel";
    public static final String PANEL_8 = "nfsResultsPanel";

}
