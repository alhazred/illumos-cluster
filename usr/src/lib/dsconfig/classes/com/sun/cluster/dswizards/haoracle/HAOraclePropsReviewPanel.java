/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)HAOraclePropsReviewPanel.java 1.13     08/12/10 SMI"
 */


package com.sun.cluster.dswizards.haoracle;

import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.haoracle.HAOracleMBean;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.rgm.property.ResourceProperty;
import com.sun.cluster.agent.rgm.property.ResourcePropertyBoolean;
import com.sun.cluster.agent.rgm.property.ResourcePropertyEnum;
import com.sun.cluster.agent.rgm.property.ResourcePropertyInteger;
import com.sun.cluster.agent.rgm.property.ResourcePropertyString;
import com.sun.cluster.agent.rgm.property.ResourcePropertyStringArray;
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.ResourcePropertyUtil;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.management.remote.JMXConnector;


/**
 * The Panel Class that gets a Hostname or list of Hostnames as input from the
 * User.
 */
public class HAOraclePropsReviewPanel extends WizardLeaf {

    private static String RTRPROPNAME = "rtrPropName";
    private static String PROPERTY = "property";
    private static String DISPLAYPROPNAME = "displayPropName";


    private String panelName; // Panel Name
    private WizardI18N wizardi18n; // Message Bundle
    private CliDSConfigWizardModel wizardModel; // WizardModel

    private boolean server = false;
    private boolean listener = false;

    private HAOracleMBean mbeans[] = null;

    private JMXConnector localConnection = null;

    /**
     * Creates a new instance of HAOraclePropsReviewPanel
     */
    public HAOraclePropsReviewPanel() {
        super();
    }

    /**
     * Creates a HostnameEntry Panel with the given name and manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     * @param  flow  Handler to the WizardFlow
     */
    public HAOraclePropsReviewPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates a HostnameEntry Panel with the given name and Wizard state
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     */
    public HAOraclePropsReviewPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;
        this.wizardi18n = wizardi18n;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.
     *		WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        String title = wizardi18n.getString(
                "haoracle.oraclePropsReviewPanel.title");
        String description = wizardi18n.getString(
                "haoracle.oraclePropsReviewPanel.desc");
        String dchar = wizardi18n.getString("ttydisplay.d.char");
        String done = wizardi18n.getString("ttydisplay.menu.done");
        String table_title = wizardi18n.getString(
                "haoracle.oraclePropsReviewPanel.table.title");
        String heading1 = wizardi18n.getString(
                "haoracle.oraclePropsReviewPanel.heading1");
        String heading2 = wizardi18n.getString(
                "haoracle.oraclePropsReviewPanel.heading2");
        String back = wizardi18n.getString("ttydisplay.back.char");
        String yes = wizardi18n.getString("cliwizards.yes");
        String no = wizardi18n.getString("cliwizards.no");
        String help_text = wizardi18n.getString(
                "haoracle.oraclePropsReviewPanel.cli.help");
        String continue_txt = wizardi18n.getString("cliwizards.continue");
        String confirm_txt = wizardi18n.getString("cliwizards.enterAgain");

        // Title
        TTYDisplay.printTitle(title);

        // Review panel table
        Vector reviewPanel = new Vector();

        String preference = (String) wizardModel.getWizardValue(
                HAOracleWizardConstants.HAORACLE_PREF);

        String rtrProps[] = null;
        String discoverableProps[] = null;

        if (preference.equals(HAOracleWizardConstants.SERVER_ONLY)) {
            rtrProps = new String[] {
                    Util.SERVER_ALERT_LOG_FILE, Util.SERVER_CONNECT_STRING,
                    Util.SERVER_PARAMETER_FILE, Util.SERVER_CUSTOM_ACTION_FILE,
                    Util.SERVER_USER_ENV, Util.SERVER_DEBUG_LEVEL
                };

            discoverableProps = new String[] {
                    Util.ALERT_LOG_FILE, Util.PARAMETER_FILE, Util.USER_ENV
                };

            server = true;
        } else if (preference.equals(HAOracleWizardConstants.LISTENER_ONLY)) {
            rtrProps = new String[] {
                    Util.LISTENER_PREFIX + Util.LISTENER_NAME,
                    Util.LISTENER_USER_ENV
                };
            discoverableProps = new String[] { Util.USER_ENV };

            listener = true;
        } else if (preference.equals(
                    HAOracleWizardConstants.SERVER_AND_LISTENER)) {
            rtrProps = new String[] {
                    Util.SERVER_ALERT_LOG_FILE, Util.SERVER_CONNECT_STRING,
                    Util.SERVER_PARAMETER_FILE, Util.SERVER_CUSTOM_ACTION_FILE,
                    Util.SERVER_USER_ENV, Util.SERVER_DEBUG_LEVEL,
                    Util.LISTENER_PREFIX + Util.LISTENER_NAME,
                    Util.LISTENER_USER_ENV
                };

            discoverableProps = new String[] {
                    Util.ALERT_LOG_FILE, Util.PARAMETER_FILE, Util.USER_ENV
                };

            server = true;
            listener = true;
        }

        String value;
        HashMap map = getDiscoveredValues(discoverableProps);

        for (int i = 0; i < discoverableProps.length; i++) {
            value = "";

            String propName = discoverableProps[i];
            Vector vec = (Vector) map.get(propName);

            if ((vec != null) && (vec.size() > 0)) {
                value = (String) vec.elementAt(0);
            }

            ResourceProperty prop = null;

            if (server) {
                prop = (ResourceProperty) wizardModel.getWizardValue(
                        Util.SERVER_PREFIX + propName);

                String prevValue = (String) ResourcePropertyUtil.getValue(prop);

                // set value in the wizardModel
                if ((prevValue == null) || (prevValue.trim().length() == 0)) {
                    validate(prop, Util.SERVER_PREFIX + propName, value);
                }
            }

            if (listener) {

                if (propName.equals(Util.USER_ENV)) {
                    prop = (ResourceProperty) wizardModel.getWizardValue(
                            Util.LISTENER_USER_ENV);

                    String prevValue = (String) ResourcePropertyUtil.getValue(
                            prop);

                    if ((prevValue == null) ||
                            (prevValue.trim().length() == 0)) {
                        validate(prop, Util.LISTENER_USER_ENV, value);
                    }
                }
            }
        }

        HashMap tableMap = new HashMap();
        HashMap selectionMap = null;
        String option = "";

        do {

            // Title
            TTYDisplay.printTitle(title);

            TTYDisplay.setNavEnabled(true);
            this.cancelDirection = false;
            tableMap = new HashMap();
            reviewPanel.removeAllElements();

            for (int i = 0; i < rtrProps.length; i++) {
                HashMap selMap = new HashMap();

                // properties as set in the wizard model
                ResourceProperty prop = (ResourceProperty) wizardModel
                    .getWizardValue(rtrProps[i]);
                selMap.put(RTRPROPNAME, rtrProps[i]);
                selMap.put(PROPERTY, prop);

                if (!server || !listener) {

                    // just display the actual property name
                    reviewPanel.add(prop.getName());
                    selMap.put(DISPLAYPROPNAME, prop.getName());
                } else {

                    // user selected server and listener. debug level and the
                    // user env are the only
                    // common props so prepend them with Server: and Listener:
                    if (isCommonProperty(rtrProps[i])) {
                        reviewPanel.add(rtrProps[i]);
                        selMap.put(DISPLAYPROPNAME, rtrProps[i]);
                    } else {
                        reviewPanel.add(prop.getName());
                        selMap.put(DISPLAYPROPNAME, prop.getName());
                    }
                }

                tableMap.put(Integer.toString(i + 1), selMap);
                reviewPanel.add(ResourcePropertyUtil.getValue(prop));
            }

            TTYDisplay.printSubTitle(description);

            HashMap customTags = new HashMap();
            customTags.put(dchar, done);

            String subheadings[] = { heading1, heading2 };

            option = TTYDisplay.create2DTable(table_title, subheadings,
                    reviewPanel, customTags, help_text);

            String desc;

            if (option.equals(back)) {
                this.cancelDirection = true;

                return;
            }

            TTYDisplay.clear(3);
            TTYDisplay.setNavEnabled(false);

            if (!option.equals(dchar)) {
                ResourceProperty prop = null;
                ErrorValue retVal = null;
                selectionMap = (HashMap) tableMap.get(option);

                String rtrPropName = (String) selectionMap.get(RTRPROPNAME);
                String displayPropName = (String) selectionMap.get(
                        DISPLAYPROPNAME);
                prop = (ResourceProperty) selectionMap.get(PROPERTY);

                boolean inputInvalid = false;

                do {
                    retVal = displayProperty(rtrPropName, displayPropName,
                            prop);

                    if (!retVal.getReturnValue().booleanValue()) {
                        TTYDisplay.printError(retVal.getErrorString());
                        inputInvalid = true;
                    } else {
                        inputInvalid = false;
                    }
                } while (inputInvalid);
            } else {

                // validate the user input using the data services
                Thread t = TTYDisplay.busy(wizardi18n.getString(
                            "haoracle.oraclePropsReviewPanel.validate.busy"));
                t.start();

                ResourceProperty prop = null;
                boolean error = false;
                List propToValidate = null;
                String detail = "";

                for (int j = 0; j < rtrProps.length; j++) {
                    prop = (ResourceProperty) wizardModel.getWizardValue(
                            rtrProps[j]);

                    String name = prop.getName();
                    String propVal = ResourcePropertyUtil.getValue(prop);

                    if (prop.isRequired()) {

                        if ((propVal == null) || (propVal.length() == 0)) {
                            error = true;
                            detail = wizardi18n.getString(
                                    "haoracle.oraclePropsReviewPanel.error.input.detail",
                                    new String[] { name });

                            break;
                        } else if (!isFilePathProperty(name)) {
                            String mandatoryPropToValidate[] = new String[1];

                            if (server) {
                                mandatoryPropToValidate[0] =
                                    Util.SERVER_PREFIX + name;
                            } else if (listener) {

                                if (name.equals(Util.USER_ENV)) {
                                    mandatoryPropToValidate[0] =
                                        Util.LISTENER_USER_ENV;
                                }
                            } else {
                                mandatoryPropToValidate[0] = name;
                            }

                            ErrorValue ret = validateInput(
                                    mandatoryPropToValidate);

                            if (!ret.getReturnValue().booleanValue()) {
                                error = true;
                                detail = ret.getErrorString();
                            }
                        } else {

                            if (propToValidate == null) {
                                propToValidate = new ArrayList();
                            }

                            if (server) {
                                propToValidate.add(Util.SERVER_PREFIX + name);
                            } else if (listener) {

                                if (name.equals(Util.USER_ENV)) {
                                    propToValidate.add(Util.LISTENER_USER_ENV);
                                }
                            } else {
                                propToValidate.add(name);
                            }
                        }
                    } else {

                        if (isFilePathProperty(name)) {

                            if (propToValidate == null) {
                                propToValidate = new ArrayList();
                            }

                            if (server) {
                                propToValidate.add(Util.SERVER_PREFIX + name);
                            } else if (listener) {

                                if (name.equals(Util.USER_ENV)) {
                                    propToValidate.add(Util.LISTENER_USER_ENV);
                                }
                            } else {
                                propToValidate.add(name);
                            }
                        }
                    }
                }

                ErrorValue retVal = null;

                if (!error) {

                    if (propToValidate != null) {
                        String arrToValidate[] = (String[]) propToValidate
                            .toArray(new String[0]);
                        retVal = validateInput(arrToValidate);
                    }
                }

                // Validation done, interrupt the busy thread
                try {
                    t.interrupt();
                } catch (Exception e) {
                }

                if (error) {

                    // Wait for the prev busy thread to be interrupted or killed
                    try {
                        t.join();
                    } catch (Exception e) {
                    }

                    // Resetting the value of option to make the screen loop
                    // again
                    TTYDisplay.clear(2);
                    TTYDisplay.printError(detail);
                    TTYDisplay.promptForEntry(continue_txt);
                    TTYDisplay.clearScreen();
                    option = "";
                } else {

                    // Wait for the prev busy thread to be interrupted or killed
                    try {
                        t.join();
                    } catch (Exception e) {

                    }

                    if (!retVal.getReturnValue().booleanValue()) {
                        TTYDisplay.clear(2);
                        TTYDisplay.printInfo(retVal.getErrorString());
                        TTYDisplay.clear(2);

                        String confirm = TTYDisplay.getConfirmation(confirm_txt,
                                yes, no);

                        if (confirm.equalsIgnoreCase(yes)) {
                            option = "";
                        }
                    }
                }
            }
        } while (!option.equals(dchar));
    }

    /**
     * Returns if its a common property being shared by Server and Listener
     *
     * @return  true or false
     */
    private boolean isCommonProperty(String propName) {

        if (!(propName.equals(Util.LISTENER_USER_ENV) ||
                    propName.equals(Util.SERVER_USER_ENV) ||
                    propName.equals(Util.SERVER_DEBUG_LEVEL))) {
            return false;
        }

        return true;
    }

    /**
     * Returns if its value is related to a file path or not
     *
     * @return  true or false
     */
    private boolean isFilePathProperty(String propName) {

        if (propName.equals(Util.ALERT_LOG_FILE) ||
                propName.equals(Util.PARAMETER_FILE) ||
                propName.equals(Util.CUSTOM_ACTION_FILE) ||
                propName.equals(Util.USER_ENV)) {
            return true;
        }

        return false;
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#isDisplayComplete
     */
    public boolean isDisplayComplete() {
        return true;
    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }

    private HashMap getDiscoveredValues(String propNames[]) {

        // Get the discovered props from the dataservice
        String nodeList[] = (String[]) wizardModel.getWizardValue(
                Util.RG_NODELIST);
        HashMap map = null;
        HashMap retMap = new HashMap();
        Vector retVec[] = new Vector[propNames.length];

        for (int i = 0; i < retVec.length; i++) {
            retVec[i] = new Vector();
        }

        HashMap helperData = new HashMap();
        String orahome = (String) wizardModel.getWizardValue(
                HAOracleWizardConstants.SEL_ORACLE_HOME);
        String orasid = (String) wizardModel.getWizardValue(
                HAOracleWizardConstants.SEL_ORACLE_SID);
        helperData.put(Util.ORACLE_HOME, orahome);
        helperData.put(Util.ORACLE_SID, orasid);

        mbeans = new HAOracleMBean[nodeList.length];

        localConnection = getConnection(Util.getClusterEndpoint());

        for (int i = 0; i < nodeList.length; i++) {

            // get mbean on the node
            String split_str[] = nodeList[i].split(Util.COLON);
            String nodeEndPoint = Util.getNodeEndpoint(localConnection,
                    split_str[0]);
            JMXConnector connector = getConnection(nodeEndPoint);
            mbeans[i] = HAOracleWizardCreator.getHAOracleMBeanOnNode(connector);

            // property discovered on nodeList[i]
            map = mbeans[i].discoverPossibilities(propNames, helperData);

            if (map != null) {

                for (int j = 0; j < propNames.length; j++) {
                    Vector vec = (Vector) map.get(propNames[j]);

                    if (vec == null) {
                        vec = new Vector();
                    }

                    int size = vec.size();

                    for (int k = 0; k < size; k++) {
                        String prop = (String) vec.elementAt(k);

                        if (!retVec[j].contains(prop)) {
                            retVec[j].add(prop);
                        }
                    }
                }
            }
        }

        for (int j = 0; j < propNames.length; j++) {
            retMap.put(propNames[j], retVec[j]);
        }

        return retMap;
    }

    private ErrorValue displayProperty(String rtrPropName,
        String displayPropName, ResourceProperty prop) {
        String propNameText = (String) wizardi18n.getString(
                "haoracle.oraclePropsReviewPanel.propname");
        String propTypeText = (String) wizardi18n.getString(
                "haoracle.oraclePropsReviewPanel.proptype");
        String propDescText = (String) wizardi18n.getString(
                "haoracle.oraclePropsReviewPanel.propdesc");
        String currValText = (String) wizardi18n.getString(
                "haoracle.oraclePropsReviewPanel.currval");
        String newValText = (String) wizardi18n.getString(
                "haoracle.oraclePropsReviewPanel.newval");

        TTYDisplay.pageText(propNameText + displayPropName);
        TTYDisplay.pageText(propDescText + prop.getDescription());
        TTYDisplay.pageText(propTypeText + ResourcePropertyUtil.getType(prop));
        TTYDisplay.pageText(currValText + ResourcePropertyUtil.getValue(prop));

        String value = TTYDisplay.promptForEntry(newValText);

        return validate(prop, rtrPropName, value);
    }

    public ErrorValue validate(ResourceProperty prop, String rtrPropName,
        String propVal) {

        // Check if a value has been set
        ErrorValue result = null;
        boolean error = false;
        String detail = wizardi18n.getString(
                "haoracle.oraclePropsReviewPanel.error.input.summary");

        String propName = prop.getName();

        if (prop.isRequired() &&
                ((propVal == null) || (propVal.length() == 0))) {
            error = true;
            detail = wizardi18n.getString(
                    "haoracle.oraclePropsReviewPanel.error.input.detail",
                    new String[] { propName });
        }

        if (error) {
            result = new ErrorValue(new Boolean(false), detail);

            return result;
        }

        try {

            if (prop instanceof ResourcePropertyString) {
                ResourcePropertyString rps = (ResourcePropertyString) prop;

                if ((propVal != null) && (propVal.length() != 0)) {
                    rps.setValue(propVal);
                } else {

                    // Will come here only if the property is not a required
                    // field.
                    String defValue = rps.getDefaultValue();

                    if ((defValue != null) && (defValue.length() != 0)) {
                        rps.setValue(rps.getDefaultValue());
                    } else {
                        rps.setValue("");
                        rps.setDefaultValue("");
                    }
                }
            } else if (prop instanceof ResourcePropertyInteger) {
                ResourcePropertyInteger rpi = (ResourcePropertyInteger) prop;

                if ((propVal != null) && (propVal.length() != 0)) {
                    rpi.setValue(new Integer(propVal));
                } else {
                    rpi.setValue(rpi.getDefaultValue());
                }
            } else if (prop instanceof ResourcePropertyBoolean) {
                ResourcePropertyBoolean rpb = (ResourcePropertyBoolean) prop;

                if ((propVal != null) && (propVal.length() != 0)) {
                    rpb.setValue(Boolean.valueOf(propVal));
                } else {
                    rpb.setValue(rpb.getDefaultValue());
                }
            } else if (prop instanceof ResourcePropertyEnum) {
                ResourcePropertyEnum rpe = (ResourcePropertyEnum) prop;
                rpe.setValue(propVal);
            } else if (prop instanceof ResourcePropertyStringArray) {
                ResourcePropertyStringArray rpsa = (ResourcePropertyStringArray)
                    prop;

                if ((propVal != null) && (propVal.length() != 0)) {
                    String newValue[] = null;
                    StringTokenizer st = new StringTokenizer(propVal,
                            Util.COMMA);

                    if (st.hasMoreTokens()) {
                        newValue = new String[st.countTokens()];

                        for (int j = 0; st.hasMoreTokens(); j++) {
                            newValue[j] = st.nextToken();
                        }
                    }

                    rpsa.setValue(newValue);
                } else {
                    rpsa.setValue(rpsa.getDefaultValue());
                }
            }
        } catch (IllegalArgumentException iae) {
            detail += ResourcePropertyUtil.getPropertyErrorMessage(prop);
            result = new ErrorValue(new Boolean(false), detail);

            return result;
        }

        wizardModel.setWizardValue(rtrPropName, prop);

        return new ErrorValue(new Boolean(true), "");
    }

    private ErrorValue validateInput(String rtrProps[]) {
        String nodeList[] = (String[]) wizardModel.getWizardValue(
                Util.RG_NODELIST);

        ErrorValue err = new ErrorValue();
        err.setReturnVal(Boolean.TRUE);

        StringBuffer errStr = new StringBuffer();
        int err_count = 0;

        for (int i = 0; i < nodeList.length; i++) {
            ResourceProperty prop = null;
            HashMap userInput = new HashMap();
            Vector vProps = new Vector();

            for (int j = 0; j < rtrProps.length; j++) {
                prop = (ResourceProperty) wizardModel.getWizardValue(
                        rtrProps[j]);

                String name = prop.getName();
                userInput.put(name, ResourcePropertyUtil.getValue(prop));

                if (!vProps.contains(name)) {
                    vProps.add(name);
                }
            }

            String props[] = (String[]) vProps.toArray(new String[0]);

            ErrorValue errVal = mbeans[i].validateInput(props, userInput, null);

            if (!errVal.getReturnValue().booleanValue()) {
                err_count++;
                errStr = new StringBuffer(errVal.getErrorString());
            }
        }

        if (err_count == nodeList.length) {

            // failed on all nodes
            err.setReturnVal(Boolean.FALSE);
            err.setErrorString(errStr.toString());
        }

        return err;
    }
}
