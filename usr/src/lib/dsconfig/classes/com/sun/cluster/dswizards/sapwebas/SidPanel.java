/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)SidPanel.java 1.13     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.sapwebas;

// J2SE
import java.util.HashMap;
import java.util.List;

// JMX
import javax.management.remote.JMXConnector;

// CMAS
import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.sapwebas.SapWebasMBean;
import com.sun.cluster.agent.dataservices.utils.DataServicesUtil;
import com.sun.cluster.agent.dataservices.utils.Util;

// CLI Wizard SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;

// Wizard Common
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;


public class SidPanel extends WizardLeaf {

    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N wizardi18n; // Message Bundle


    /**
     * Creates a new instance of SidPanel
     */
    public SidPanel() {
        super();
    }

    /**
     * Creates a SidPanel Panel with the given name and tree manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public SidPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    public SidPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;

        // Get the I18N instance
        this.wizardi18n = wizardi18n;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {
        String title = wizardi18n.getString("sap.sidPanel.title");
        String desc = wizardi18n.getString("sap.sidPanel.desc");
        String query_txt = wizardi18n.getString("sap.sidPanel.query");
        String help_text = wizardi18n.getString("sap.sidPanel.help.cli");
        String yes = wizardi18n.getString("cliwizards.yes");
        String no = wizardi18n.getString("cliwizards.no");
        String enter_sid = wizardi18n.getString("sap.sidPanel.enter_new");
        SapWebasMBean mBean;


        String continue_txt = wizardi18n.getString("cliwizards.continue");

        DataServicesUtil.clearScreen();

        // Title
        TTYDisplay.clear(1);
        TTYDisplay.printTitle(title);
        TTYDisplay.printSubTitle(desc);

        // Get the Discovered nodelist From the DefaultContext
        String optionsArr[] = (String[]) wizardModel.getWizardValue(
                SapWebasConstants.DISCOVERD_SIDS);

        if (optionsArr == null) {
            String nodeEndPoint = (String) wizardModel.getWizardValue(
                    Util.NODE_TO_CONNECT);
            JMXConnector connector = getConnection(nodeEndPoint);
            mBean = SapWebasWizardCreator.getMBean(connector);

            HashMap values = mBean.discoverPossibilities(
                    new String[] { Util.SID }, null);
            optionsArr = (String[]) values.get(Util.SID);
            wizardModel.setWizardValue(SapWebasConstants.DISCOVERD_SIDS,
                optionsArr);
        }

        String chosenSid;
        String defaultSid;

        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        String menuKey = wizardi18n.getString("sap.sidPanel.custom.key");
        String menuText = wizardi18n.getString("sap.sidPanel.custom.keyText");
        HashMap custom = new HashMap();
        custom.put(menuKey, menuText);

        boolean repeatLoop = true;

        while (repeatLoop) {
            String option;
            TTYDisplay.clear(1);
            defaultSid = (String) wizardModel.getWizardValue(Util.SAP_SID);

            if (optionsArr != null) {
                List sidList = TTYDisplay.getScrollingOption(query_txt,
                        optionsArr, custom, defaultSid, help_text, false);

                if (sidList == null) {
                    this.cancelDirection = true;

                    return;
                }

                TTYDisplay.setNavEnabled(true);
                this.cancelDirection = false;

                // Set the Hostname List
                TTYDisplay.clear(1);

                if (sidList.size() != 1) {
                    TTYDisplay.printError(wizardi18n.getString(
                            "sap.sidPanel.invalidselection"));

                    continue;
                } else {
                    String sidArr[] = (String[]) sidList.toArray(new String[0]);
                    chosenSid = sidArr[0];
                }
            } else {
                chosenSid = menuText;
            }

            if (chosenSid.equals(menuText)) {
                TTYDisplay.setNavEnabled(false);
                chosenSid = TTYDisplay.promptForEntry(enter_sid, help_text);
            }

            if ((chosenSid == null) || (chosenSid.length() <= 0)) {
                continue;
            }

            TTYDisplay.clear(1);
            TTYDisplay.setNavEnabled(false);

            // Store the Values in the WizardModel
            String nodeEndPoint = (String) wizardModel.getWizardValue(
                    Util.NODE_TO_CONNECT);
            JMXConnector connector = getConnection(nodeEndPoint);
            mBean = SapWebasWizardCreator.getMBean(connector);

            HashMap data = new HashMap();
            data.put(Util.SID, chosenSid);

            ErrorValue err = mBean.validateInput(new String[] { Util.SID },
                    data, null);

            if (err.getReturnValue().booleanValue()) {
                wizardModel.selectCurrWizardContext();
                wizardModel.setWizardValue(Util.SAP_SID, chosenSid);
                repeatLoop = false;
            } else {
                TTYDisplay.printWarning(wizardi18n.getString(
                        "sap.sidPanel.invalid"));
                TTYDisplay.printMoreText(err.getErrorString());
                TTYDisplay.clear(1);

                String confirm = TTYDisplay.getConfirmation(wizardi18n
                        .getString("cliwizards.validation.override"), no, yes);

                if (confirm.equalsIgnoreCase(yes)) {
                    wizardModel.selectCurrWizardContext();
                    wizardModel.setWizardValue(Util.SAP_SID, chosenSid);
                    repeatLoop = false;
                }
            }
        }
    }
}
