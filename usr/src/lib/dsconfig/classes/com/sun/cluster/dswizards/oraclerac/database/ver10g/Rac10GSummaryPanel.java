/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)Rac10GSummaryPanel.java	1.19	09/03/11 SMI"
 */

package com.sun.cluster.dswizards.oraclerac.database.ver10g;

// CMASS
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.dataservices.utils.Util;

// Wizard Common
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.common.CliUtil;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.oraclerac.database.*;
import com.sun.cluster.dswizards.oraclerac.invoker.RACInvokerCreator;

// Model
import com.sun.cluster.model.RACModel;

// Java
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.management.remote.JMXConnector;


/**
 * RAC 10G SummaryPanel
 */
public class Rac10GSummaryPanel extends RacDBBasePanel {
    private String NAME = "name";
    private String VALUE = "value";
    private String KEY = "key";
    private String DESC = "desc";
    private String TYPE = "type";
    private int rowId = 0;
    private Map confMap = new HashMap();

    /**
     * Creates a Rac10GSummaryPanel with the given name and associates it with
     * the WizardModel and WizardFlow
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     */
    public Rac10GSummaryPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(name, model, wizardFlow, wizardi18n);
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {
        title = wizardi18n.getString("oraclerac10g.summaryPanel.title");
        desc = wizardi18n.getString("oraclerac10g.summaryPanel.desc1");

        String table_title = wizardi18n.getString(
                "oraclerac10g.summaryPanel.desc2");
        String heading1 = wizardi18n.getString(
                "oraclerac10g.summaryPanel.heading1");
        String heading2 = wizardi18n.getString(
                "oraclerac10g.summaryPanel.heading2");
        String panel_desc = wizardi18n.getString(
                "oraclerac10g.summaryPanel.description");
        String gencmd_txt = wizardi18n.getString(
                "oraclerac10g.summaryPanel.generateCommand");
        String help_text = wizardi18n.getString(
                "oraclerac10g.summaryPanel.cli.help");
        String crsframework_rsname_txt = wizardi18n.getString(
                "oraclerac10g.summaryPanel.crsframeworkresource.name");
        String crsframework_rsname_desc = wizardi18n.getString(
                "oraclerac10g.reviewPanel.crsframeworkresource.desc");
        String racframework_rgname_txt = wizardi18n.getString(
                "oraclerac10g.summaryPanel.racframeworkrg.name");
        String racframework_rgname_desc = wizardi18n.getString(
                "oraclerac10g.reviewPanel.racframeworkrg.desc");
        String racframework_rsname_txt = wizardi18n.getString(
                "oraclerac10g.summaryPanel.racframeworkrs.name");
        String proxy_rsname_txt = wizardi18n.getString(
                "oraclerac10g.summaryPanel.proxyresource.name");
        String proxy_rsname_desc = wizardi18n.getString(
                "oraclerac10g.reviewPanel.proxyresource.desc");
        String proxy_rgname_txt = wizardi18n.getString(
                "oraclerac10g.summaryPanel.proxyrg.name");
        String proxy_rgname_desc = wizardi18n.getString(
                "oraclerac10g.reviewPanel.proxyrg.desc");
        String nodelist_name = wizardi18n.getString(
                "oraclerac10g.summaryPanel.nodelist.name");
        String nodelist_desc = wizardi18n.getString(
                "oraclerac10g.summaryPanel.nodelist.desc");
        String gddrs_name = wizardi18n.getString(
                "oraclerac10g.summaryPanel.gddrs.name");
        String gddrs_desc = wizardi18n.getString(
                "oraclerac10g.summaryPanel.gddrs.desc");
        String crshome_name = wizardi18n.getString(
                "oraclerac10g.summaryPanel.crshome.name");
        String crshome_desc = wizardi18n.getString(
                "oraclerac10g.summaryPanel.crshome.desc");
        String crsfile_name = wizardi18n.getString(
                "oraclerac10g.summaryPanel.crsfile.name");
        String crsfile_desc = wizardi18n.getString(
                "oraclerac10g.summaryPanel.crsfile.desc");
        String db_name = wizardi18n.getString(
                "oraclerac10g.summaryPanel.dbname.name");
        String db_desc = wizardi18n.getString(
                "oraclerac10g.summaryPanel.dbname.desc");
        String orahome_name = wizardi18n.getString(
                "oraclerac10g.summaryPanel.oraclehome.name");
        String orahome_desc = wizardi18n.getString(
                "oraclerac10g.summaryPanel.oraclehome.desc");
        String orasid_name = wizardi18n.getString(
                "oraclerac10g.summaryPanel.oraclesid.name");
        String orasid_desc = wizardi18n.getString(
                "oraclerac10g.summaryPanel.oraclesid.desc");

        String option;

        TTYDisplay.printTitle(title);
        TTYDisplay.pageText(desc);
        TTYDisplay.clear(1);

        String subheadings[] = { heading1, heading2 };
        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        HashMap customTags = new HashMap();
        customTags.put(c_char, create_config);
        customTags.put(qchar, quit);

        HashMap tableEntries = new HashMap();
        int i = 0;
        boolean crsFrameworkPresent = ((Boolean) wizardModel.
	    getWizardValue(Util.CRS_FRAMEWORK_PRESENT)).booleanValue();

        while (true) {
            TTYDisplay.clear(1);
            TTYDisplay.setNavEnabled(true);

            Vector data = new Vector();

            if (!crsFrameworkPresent) {
                addtoTable(tableEntries, data, crsframework_rsname_txt,
                    crsframework_rsname_desc, Util.CRS_FRAMEWORK_RSNAME);
            }

            addtoTable(tableEntries, data, racframework_rgname_txt,
                racframework_rgname_desc, Util.RAC_FRAMEWORK_RGNAME);

            confMap.put(Util.RAC_FRAMEWORK_RSNAME,
                (String) wizardModel.getWizardValue(Util.RAC_FRAMEWORK_RSNAME));

            addtoTable(tableEntries, data, proxy_rsname_txt, proxy_rsname_desc,
                Util.RAC_PROXY_RSNAME);

            addtoTable(tableEntries, data, proxy_rgname_txt, proxy_rgname_desc,
                Util.RAC_PROXY_RGNAME);

            addArraytoTable(tableEntries, data, nodelist_name, nodelist_desc,
                Util.RG_NODELIST);

            addArraytoTable(tableEntries, data, gddrs_name, gddrs_desc,
                Util.SEL_STORAGE);

            addtoTable(tableEntries, data, crshome_name, crshome_desc,
                Util.SEL_CRS_HOME);

            addArrayListtoTable(tableEntries, data, crsfile_name, crsfile_desc,
                Util.ALL_SEL_CRS_FILES);

            addtoTable(tableEntries, data, db_name, db_desc, Util.SEL_DB_NAME);

            addtoTable(tableEntries, data, orahome_name, orahome_desc,
                Util.SEL_ORACLE_HOME);

            addHashMaptoTable(tableEntries, data, orasid_name, orasid_desc,
                Util.SEL_ORACLE_SID);

            if (crsFrameworkPresent) {

                // add existing crs resource name to confMap
                confMap.put(Util.CRS_FRAMEWORK_RSNAME,
                    (String) wizardModel.getWizardValue(
                        Util.CRS_FRAMEWORK_RSNAME));
            }

            // set value in confmap to indicate if this is an existing crs
            // resource
            confMap.put(Util.CRS_FRAMEWORK_PRESENT,
                new Boolean(crsFrameworkPresent));
            confMap.put(Util.SCAL_DEVGROUP_RSLIST,
                (List) wizardModel.getWizardValue(Util.SCAL_DEVGROUP_RSLIST));

            confMap.put(Util.SCAL_MOUNTPOINT_RSLIST,
                (List) wizardModel.getWizardValue(Util.SCAL_MOUNTPOINT_RSLIST));

            confMap.put(Util.SEL_STORAGE_RG,
                (String[]) wizardModel.getWizardValue(Util.SEL_STORAGE_RG));

            confMap.put(Util.SEL_SCAL_RS,
                (ArrayList) wizardModel.getWizardValue(Util.SEL_SCAL_RS));

            confMap.put(Util.CONFIGURATION_TYPE, Util.RAC_10G);

	    // pass the zone cluster name if being configured in a zone cluster
	    String selZoneCluster = CliUtil.getZoneClusterName(wizardModel);
	    confMap.put(Util.SEL_ZONE_CLUSTER, selZoneCluster);

            option = TTYDisplay.create2DTable(table_title, subheadings, data,
                    customTags, help_text);

            if (option.equals(back)) {
                this.cancelDirection = true;

                return;
            }

            if (option.equals(qchar)) {
                String exitOpt = TTYDisplay.getConfirmation(wizardi18n
                        .getString("cliwizards.confirmExit"), yes, no);

                if (exitOpt.equalsIgnoreCase(yes)) {
                    System.exit(0);
                } else {
                    continue;
                }
            }

            if (option.equals(c_char)) {
                generateCommands(confMap);

                return;
            }

            this.cancelDirection = false;

            String sprop;
            String currVal;
            String newVal;
            HashMap selectedRow = (HashMap) tableEntries.get(option);

            if (selectedRow == null) {
                TTYDisplay.promptForEntry(continue_txt);

                break;
            }

            String propNameText = wizardi18n.getString(
                    "oraclerac10g.summaryPanel.propname",
                    new String[] { (String) selectedRow.get(NAME) });
            String propDescText = wizardi18n.getString(
                    "oraclerac10g.summaryPanel.propdesc",
                    new String[] { (String) selectedRow.get(DESC) });
            String currValText = wizardi18n.getString(
                    "oraclerac10g.summaryPanel.currval",
                    new String[] { (String) selectedRow.get(VALUE) });

            TTYDisplay.setNavEnabled(false);
            TTYDisplay.clear(1);
            TTYDisplay.pageText(propNameText);
            TTYDisplay.pageText(propDescText);
            TTYDisplay.pageText(currValText);
            TTYDisplay.clear(1);
            TTYDisplay.promptForEntry(continue_txt);
        }
    }

    private void addtoTable(HashMap xTableModel, Vector xData, String xName,
        String xDesc, String xModelKey) {
        HashMap tableRow = new HashMap();
        tableRow.put(NAME, xName);
        tableRow.put(DESC, xDesc);
        tableRow.put(KEY, xModelKey);

        String value = (String) wizardModel.getWizardValue(xModelKey);

        if (value != null) {
            tableRow.put(VALUE, value);
            xData.add(xName);
            xData.add(value);
            xTableModel.put(String.valueOf(++rowId), tableRow);
            confMap.put(xModelKey, value);
        }
    }

    private void addHashMaptoTable(HashMap xTableModel, Vector xData,
        String xName, String xDesc, String xModelKey) {
        HashMap tableRow = new HashMap();
        tableRow.put(NAME, xName);
        tableRow.put(DESC, xDesc);
        tableRow.put(KEY, xModelKey);

        ArrayList oraSids = new ArrayList();
        String nodeList[] = (String[]) wizardModel.getWizardValue(
                Util.RG_NODELIST);
        HashMap map = new HashMap();

        for (int i = 0; i < nodeList.length; i++) {
            String split_str[] = nodeList[i].split(Util.COLON);
            String oraSid = (String) wizardModel.getWizardValue(xModelKey +
                    split_str[0]);
            String displayOraSid = wizardi18n.getString(
                    "oraclerac10g.summaryPanel.oraclesid.display",
                    new String[] { split_str[0], oraSid });

            if (!oraSids.contains(displayOraSid)) {
                oraSids.add(displayOraSid);
            }

            map.put(split_str[0], oraSid);
        }

        String arrayOraSIDs[] = (String[]) oraSids.toArray(new String[0]);
        String value = Util.convertArraytoStringBuffer(arrayOraSIDs).toString();

        if (value != null) {
            tableRow.put(VALUE, value);
            xData.add(xName);
            xData.add(value);
            xTableModel.put(String.valueOf(++rowId), tableRow);
            confMap.put(xModelKey, map);
        }
    }

    private void addArraytoTable(HashMap xTableModel, Vector xData,
        String xName, String xDesc, String xModelKey) {
        HashMap tableRow = new HashMap();
        tableRow.put(NAME, xName);
        tableRow.put(DESC, xDesc);
        tableRow.put(KEY, xModelKey);

        String value[] = (String[]) wizardModel.getWizardValue(xModelKey);

        if ((value != null) && (value.length > 0)) {
            String val = Util.convertArraytoStringBuffer(value).toString();
            tableRow.put(VALUE, val);
            xData.add(wizardi18n.getString(xName));
            xData.add(val);
            xTableModel.put(String.valueOf(++rowId), tableRow);
            confMap.put(xModelKey, value);
        }
    }

    private void addArrayListtoTable(HashMap xTableModel, Vector xData,
        String xName, String xDesc, String xModelKey) {
        HashMap tableRow = new HashMap();
        tableRow.put(NAME, xName);
        tableRow.put(DESC, xDesc);
        tableRow.put(KEY, xModelKey);

        ArrayList value = (ArrayList) wizardModel.getWizardValue(xModelKey);

        if ((value != null) && (value.size() > 0)) {
            String strArray[] = (String[]) value.toArray(new String[0]);
            String val = Util.convertArraytoStringBuffer(strArray).toString();
            tableRow.put(VALUE, val);
            xData.add(wizardi18n.getString(xName));
            xData.add(val);
            xTableModel.put(String.valueOf(++rowId), tableRow);
            confMap.put(xModelKey, value);
        }
    }

    /**
     * Generates the commands by invoking the command generator
     */
    private void generateCommands(Map confMap) {
        Thread t = null;
        List commandList = null;
	RACModel racModel = RACInvokerCreator.getRACModel();

	String nodeEndPoint = (String) wizardModel.getWizardValue(
	    Util.NODE_TO_CONNECT);

        try {

            TTYDisplay.clear(1);
            t = TTYDisplay.busy(wizardi18n.getString(
                        "cliwizards.executingMessage"));
            t.start();
            commandList = racModel.generateCommands(null, nodeEndPoint, confMap);
        } catch (CommandExecutionException cee) {
            wizardModel.setWizardValue(Util.EXITSTATUS, cee);
            commandList = racModel.getCommandList(null, nodeEndPoint);
        } finally {
            wizardModel.setWizardValue(Util.CMDLIST, commandList);

            try {
                t.interrupt();
                t.join();
            } catch (Exception exe) {
            }
        }
    }
}
