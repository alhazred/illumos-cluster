/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)NonblockingLineReader.java 1.9     08/05/20 SMI"
 */

package com.sun.cluster.dswizards.clisdk.core;

import java.io.*;


/*
 * This class facilitates a line read from an input stream.
 * The read will break and take default value when specified
 * time out is reached. This is a private interface used by
 * TTYDisplay.java in the SDK.
 *
 */
class NonblockingLineReader implements LineReader {

    private boolean responseTimeOut = false;
    private Object responseLock = new Object();
    private String defval = null;
    private InputStream inStream = null;
    private BufferedReader bReader = null;
    private long timeOut = 0;

    /*
     * Constructor: creates a NonblockingLineReader object.
     *
     * @param in an input stream object
     * @param defval the default string value
     * @param timeout a timeout in miliseconds
     */
    public NonblockingLineReader(InputStream in, BufferedReader bReader,
        String defval, int timeOut) {
        this.inStream = in;
        this.bReader = bReader;
        this.defval = defval;
        this.timeOut = (long) timeOut;
    }

    /*
     * This method reaturns the default string.
     *
     * @return a String object represents the default value
     */
    public String getDefault() {
        return (this.defval);
    }

    /*
     * This method reads a line from the underlying input stream. It will break
     * and returns the default value if time out specified is reached.
     *
     * @return  String object reprsents the line read from the underlying stream
     */
    public String readLine() {
        long startTime = System.currentTimeMillis();
        long currentTime = 0;
        long timeElapsed = 0;
        String result = getDefault();

        try {

            while (!getTimeOut(timeElapsed) &&
                    (this.inStream.available() <= 0)) {
                Thread.sleep(500);
                currentTime = System.currentTimeMillis();
                timeElapsed = currentTime - startTime;
            }

            if (this.inStream.available() > 0) {
                result = this.bReader.readLine();
            }
        } catch (IOException ie) {
            TTYDisplay.writeToLog("IOException in read query value.\n" +
                ie.toString());
        } catch (InterruptedException ie) {
            TTYDisplay.writeToLog(
                "InterruptedException in read query value.\n" + ie.toString());
        }

        return (result);
    }

    /*
     * This method reads a masked line from the underlying input stream.
     * It will break and returns the default value if time out specified
     * is reached.
     *
     * @return  String object reprsents the masked line read from the stream.
     */
    public String readMaskedLine() {
        long startTime = System.currentTimeMillis();
        long currentTime = 0;
        long timeElapsed = 0;
        String result = getDefault();

        // create the masking thread and start it.
        MaskingThread mt = new MaskingThread(' ', 1);
        Thread mask = new Thread(mt);
        mask.start();

        try {

            while (!getTimeOut(timeElapsed) &&
                    (this.inStream.available() <= 0)) {
                Thread.sleep(500);
                currentTime = System.currentTimeMillis();
                timeElapsed = currentTime - startTime;
            }

            if (this.inStream.available() > 0) {
                result = this.bReader.readLine();
            }
        } catch (IOException ie) {
            TTYDisplay.writeToLog("IOException in read query value.\n" +
                ie.toString());
        } catch (InterruptedException ie) {
            TTYDisplay.writeToLog(
                "InterruptedException in read query value.\n" + ie.toString());
        } finally {

            // stop masking
            mt.stopMasking();
        }

        // return the masked line, for example, a password.
        return (result);
    }

    /*
     * This method checks whether the time out specified has been reached.
     *
     * @return boolean object to indicates whether the time out is reached
     */
    public boolean getTimeOut(long timeElapsed) {

        if (timeElapsed >= this.timeOut) {
            this.responseTimeOut = true;
        }

        return (this.responseTimeOut);
    }
}
