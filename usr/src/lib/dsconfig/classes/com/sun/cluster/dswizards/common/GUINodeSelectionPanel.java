/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)GUINodeSelectionPanel.java	1.15	08/08/22 SMI"
 */

package com.sun.cluster.dswizards.common;

// J2SE
import java.io.FileInputStream;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

// JATO
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.RequestHandlingViewBase;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.DisplayEvent;
import com.iplanet.jato.view.html.OptionList;

// CMASS
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.dataservices.utils.InfrastructureMBean;
import com.sun.cluster.dswizards.hastorageplus.HASPWizardConstants;
import com.sun.cluster.agent.dataservices.common.ServiceConfig;
import com.sun.cluster.agent.zonecluster.ZoneClusterData;

// Lockhart
import com.sun.cluster.agent.node.ZoneMBean;
import com.sun.web.ui.model.CCAddRemoveModel;
import com.sun.web.ui.model.CCPropertySheetModel;
import com.sun.web.ui.view.addremove.CCAddRemove;
import com.sun.web.ui.view.alert.CCAlertInline;
import com.sun.web.ui.view.html.CCHiddenField;
import com.sun.web.ui.view.propertysheet.CCPropertySheet;
import com.sun.web.ui.view.wizard.CCWizardPage;

// SPM
import com.sun.cluster.spm.common.SpmUtil;
import com.sun.cluster.spm.task.wizard.ConfigureDataServiceWizard;

// Common files
import com.sun.cluster.dswizards.hastorageplus.HASPWizardConstants;
import com.sun.cluster.spm.common.SpmUtil;
import com.sun.cluster.spm.task.wizard.ConfigureDataServiceWizard;

import com.sun.cluster.common.MBeanModel;
import com.sun.cluster.spm.zone.ZoneUtil;
import com.sun.cluster.model.ZoneClusterModel;
import com.sun.cluster.model.DataModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.Iterator;
/**
 * This {@link View} handles the initial step of the resource group wizard used
 * for resource groups' creation and edition.
 */
public class GUINodeSelectionPanel extends RequestHandlingViewBase
    implements CCWizardPage {

    /** Wizard step's title dispayed first on the right */
    public static final String STEP_TITLE = "nodeSelectionPanel.title";

    /** Wizard step's name displayed on the left */
    public static final String STEP_TEXT = "nodeSelectionPanel.name";

    /** Wizard step's instructions dispayed just below the title */
    /** Prefix to retrieve the wizard's step contextal help */
    public static final String STEP_HELP_PREFIX = "lhwizard.wizard.step3.help";

    /** JSP file used for the wizard's step */
    public static final String PAGELET_URL =
        "/jsp/task/wizard/TaskWizardStep.jsp";

    /** Child element for the hidden field */
    public static final String CHILD_CLOSE = "CloseWizardHiddenField";

    /** Child element to display the Alert Inline */
    public static final String CHILD_ALERT = "Alert";

    /** Name of the property sheet child */
    public static final String CHILD_PROPERTYSHEET = "PropertySheet";

    /** XML file containing the property sheet's model */
    public static final String PROPERTYSHEET_XML_FILE =
        GenericDSWizard.COMMON_XML_PATH + "guiNodeSelectionPanel.xml";

    /** Child's name to enter the value of nodelist attribute */
    public static final String CHILD_NODELIST = "NodeList";

    // Property sheet model
    private CCPropertySheetModel propertySheetModel = null;
    private CCAddRemoveModel addRemoveModel = null;

    protected DSConfigWizardModel wizardModel = null;

    /**
     * Construct an instance with the specified model and logical page name.
     *
     * @param  parent  The parent view of this object.
     * @param  model  This view's model.
     * @param  name  Logical page name.
     */
    public GUINodeSelectionPanel(View parent, Model model, String name) {
        super(parent, name);
        setDefaultModel(model);
        wizardModel = (DSConfigWizardModel) getDefaultModel();
        createPropertySheetModel();
        registerChildren();
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Child manipulation methods
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    protected void registerChildren() {
        registerChild(CHILD_CLOSE, CCHiddenField.class);
        registerChild(CHILD_ALERT, CCAlertInline.class);
        registerChild(CHILD_PROPERTYSHEET, CCPropertySheet.class);
        propertySheetModel.registerChildren(this);
    }

    protected View createChild(String name) {

        if (name.equals(CHILD_PROPERTYSHEET)) {
            CCPropertySheet child = new CCPropertySheet(this,
                    propertySheetModel, name);

            return child;

        } else if (name.equals(CHILD_CLOSE)) {
            CCHiddenField child = new CCHiddenField(this, name, null);

            return child;

        } else if (name.equals(CHILD_ALERT)) {
            CCAlertInline child = new CCAlertInline(this, name, null);

            return child;

        } else if ((propertySheetModel != null) &&
                propertySheetModel.isChildSupported(name)) {

            // Create child from property sheet model.
            View child = propertySheetModel.createChild(this, name);

            return child;

        } else {
            throw new IllegalArgumentException(
                "GUINodeSelectionPanel Invalid child name [" + name + "]");
        }
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Display methods
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    private void createPropertySheetModel() {

        try {
            FileInputStream fileInputStream = new FileInputStream(
                    PROPERTYSHEET_XML_FILE);
            propertySheetModel = new CCPropertySheetModel();

            String xmlFileString = ConfigureDataServiceWizard.getXMLString(
                    fileInputStream);
            propertySheetModel.setDocument(xmlFileString);
            fileInputStream.close();

        } catch (Exception e) {
            System.out.println(
                "Exception when loading GUINodeSelectionPanel XML Schema");

            return;
        }

        addRemoveModel = new CCAddRemoveModel();
        addRemoveModel.setListboxWidth(15);
        propertySheetModel.setModel(CHILD_NODELIST, addRemoveModel);
    }

    private void populatePropertySheetModel() {
	String propName = null;
	String[] propertyNames = null;
	String selectedCluster = null;
	String selectedZoneCluster = null;

        // Discover Nodelist
        String optionsArr[] = (String[]) wizardModel.getWizardValue(
                Util.NODELIST);
        String availableNodes[] = (String[]) wizardModel.getWizardValue(
                Util.AVAIL_NODELIST);

        if (optionsArr != null) {
            addRemoveModel.setSelectedOptionList(new OptionList(optionsArr,
                    optionsArr));
        }

        if (availableNodes != null) {
            addRemoveModel.setAvailableOptionList(
		new OptionList(availableNodes,
                    availableNodes));
        }

	selectedCluster = (String) wizardModel.getWizardValue(
		Util.CLUSTER_SEL);

	if (selectedCluster == null ||
	    selectedCluster.equals(Util.BASE_CLUSTER)) {
	    propName = Util.NODELIST;
	} else {
	    selectedZoneCluster = (String) wizardModel.getWizardValue(
		Util.SEL_ZONE_CLUSTER);
	    propName = Util.ZONE_CLUSTER_HOSTLIST;
	}

        if (optionsArr == null) {
	    try {
		RequestContext rq = RequestManager.getRequestContext();
		DataModel dm = DataModel.getDataModel(rq);

		if (selectedCluster == null ||
		    selectedCluster.equals(Util.BASE_CLUSTER)) {

		    InfrastructureMBean mbean = GenericDSWizard.
			getInfrastructureMBean(); 
		    HashMap discoveredMap = mbean.discoverPossibilities(
			new String[] { propName }, null);
		    optionsArr = (String[]) discoveredMap.get(propName);
		} else {
		    ZoneClusterData zcData = null;
		    ArrayList zcNames = null;
		    ZoneClusterModel zm = dm.getZoneClusterModel();

		    zcData = zm.getZoneClusterData(rq, selectedZoneCluster);

		    zcNames = zcData.getHostNameList();

		    if (zcNames != null) {
			// Parse the list to get only the ZCs that are running
			int size = zcNames.size();
			ArrayList runningZCs = null;
			for (int i = 0; i < size; i++) {
			    String hostName = (String) zcNames.get(i);
			    if (zm.isZoneRunning(rq, selectedZoneCluster, hostName)) {
				if (runningZCs == null) {
				    runningZCs = new ArrayList();
				}
				runningZCs.add(hostName);
			    }
			}

			if (runningZCs != null) {
			    optionsArr = (String [])runningZCs.toArray(new String[0]);
			}
		    }
		}

	    } catch(Exception e) {
		System.out.println(e.getMessage());
	    }
	}

	if (optionsArr != null) {
	    wizardModel.setWizardValue(propName, optionsArr);
	    // set default values
	    // available node list = only zone names i.e. excluding node names
	    // selected node list = only nodes ie. excluding zones
	    OptionList selectedOptList = new OptionList();
	    OptionList availableOptList = new OptionList();

	    for (int j = 0; j < optionsArr.length; j++) {
		String split_str[] = optionsArr[j].split(Util.COLON);

		if (split_str.length == 1) {
		    selectedOptList.add(split_str[0], split_str[0]);
		} else {
		    availableOptList.add(optionsArr[j], optionsArr[j]);
		}
	    }

	    addRemoveModel.setSelectedOptionList(selectedOptList);
	    addRemoveModel.setAvailableOptionList(availableOptList);
	}
    }

    /**
     * Get the pagelet to use for the rendering of this instance.
     *
     * @return  The pagelet to use for the rendering of this instance.
     */
    public String getPageletUrl() {
        return PAGELET_URL;
    }

    /**
     * This method is called when the page is beginning its display.
     *
     * @param  event  DisplayEvent is passed in by the framework.
     */
    public void beginComponentDisplay(DisplayEvent event)
        throws ModelControlException {
        populatePropertySheetModel();

        CCHiddenField closeWizard = (CCHiddenField) getChild(CHILD_CLOSE);
        closeWizard.setValue("false");
    }


    /**
     * Step data validation is done here. This method is called from the
     * nextStep() of the wizard.
     *
     * The error message and severity are set in case of errors.
     */
    protected HashMap getSelections(Class mBeanToCheck, String packageName) {

        CCAddRemove nodesList = (CCAddRemove) getChild(CHILD_NODELIST);
        OptionList selectedOptions = addRemoveModel.getSelectedOptionList(
                nodesList);
        OptionList availOptions = addRemoveModel.getAvailableOptionList(
                nodesList);
        CCAlertInline alert = (CCAlertInline) getChild(CHILD_ALERT);
	wizardModel.setWizardValue(Util.EXCLUSIVEIP, Boolean.FALSE);

        if (selectedOptions.size() > 0) {
            String selectedArr[] = new String[selectedOptions.size()];

            for (int i = 0; i < selectedArr.length; i++) {
                selectedArr[i] = selectedOptions.getValue(i);
            }

            String availArr[] = new String[availOptions.size()];

            for (int i = 0; i < availArr.length; i++) {
                availArr[i] = availOptions.getValue(i);
            }


            // If multiple zones are selected from the same node - error out
            List tmpList = new ArrayList();

            for (int j = 0; j < selectedArr.length; j++) {
                String split_str[] = selectedArr[j].split(Util.COLON);

                if (tmpList.contains(split_str[0])) {
                    alert.setValue(CCAlertInline.TYPE_ERROR);
                    alert.setSummary("dswizards.error.multipleZonesSelection");

                    return null;
                } else {
                    tmpList.add(split_str[0]);
                }
            }

	    String[] baseClusterNodeList = null;
	    ArrayList baseClusterNodeArray = null;

	    String selectedCluster = (String) wizardModel.getWizardValue(
		Util.CLUSTER_SEL);
	    if (selectedCluster != null &&
		selectedCluster.equals(Util.ZONE_CLUSTER)) {
		// for a zone cluster the node list corresponds to
		// the public host names of the zones.
		// get the baseCluster node names given the public
		// host names.

		String selectedZoneCluster = (String) wizardModel.getWizardValue(
		    Util.SEL_ZONE_CLUSTER);

		RequestContext rq = RequestManager.getRequestContext();
		DataModel dm = DataModel.getDataModel(rq);

		ZoneClusterData zcData = null;

		try {
		    zcData = dm.getZoneClusterModel().
			getZoneClusterData(rq, selectedZoneCluster); 
		} catch (Exception e) {
		}

		HashMap hostNodeMap = null;

		if (zcData != null) {
		    hostNodeMap = zcData.getHostNodeMap();
		}

		if (hostNodeMap != null) {
		    for (int i = 0; i < selectedArr.length; i++) {
			String racNode = selectedArr[i];
			boolean found = false;
			Set keySet = hostNodeMap.keySet();
			Iterator iterator = keySet.iterator();
			while (iterator.hasNext() && !found) {
			    String hostName = (String)iterator.next();
			    if (racNode.equals(hostName)) {
				found = true;
				if (baseClusterNodeArray == null) {
				    baseClusterNodeArray = new ArrayList();
				}
				baseClusterNodeArray.add((String)
				    hostNodeMap.get(hostName));
			    }
			}
		    }
		}

		if (baseClusterNodeArray != null) {
		    baseClusterNodeList = (String [])baseClusterNodeArray.toArray(
			new String[0]);
		}
	    } else {
		baseClusterNodeList = selectedArr;
	    }

	    wizardModel.setWizardValue(Util.BASE_CLUSTER_NODELIST,
			baseClusterNodeList);

            String error = checkMBeanOnNodes(baseClusterNodeList, mBeanToCheck,
                    packageName);

            if (error != null) {
                alert.setValue(CCAlertInline.TYPE_ERROR);
                alert.setSummary("nodeSelectionPanel.summary");
                alert.setDetail(error);

                return null;
            } else {
                wizardModel.setWizardValue(Util.NODELIST, selectedArr);
                wizardModel.setWizardValue(Util.AVAIL_NODELIST, availArr);

                // check if the nodelist includes the local nodes
                // if yes set NODE_TO_CONNECT as the localnode
                boolean localNodeInNodeList = false;

                try {
                    InetAddress currNodeAddress = Inet4Address.getLocalHost();
                    String localNode = currNodeAddress.getHostName();

                    for (int i = 0; i < baseClusterNodeList.length; i++) {
                        String node_i = baseClusterNodeList[i];

                        if (node_i.equalsIgnoreCase(localNode)) {
                            wizardModel.setWizardValue(Util.NODE_TO_CONNECT,
                                localNode);
                            localNodeInNodeList = true;

                            break;
                        }
                    }
                } catch (Exception e) {

                }

                if (!localNodeInNodeList) {
                    wizardModel.setWizardValue(Util.NODE_TO_CONNECT,
                        baseClusterNodeList[0]);
                }

                // check if the nodelist includes any zones
                boolean zonesPresent = false;
                int i = 0;

                while ((i < selectedArr.length) && !zonesPresent) {

                    if (selectedArr[i].indexOf(Util.COLON) != -1) {
                        zonesPresent = true;
                    } else {
                        i++;
                    }
                }

                if (zonesPresent && !zonesEnable(mBeanToCheck)) {
                    alert.setValue(CCAlertInline.TYPE_ERROR);
                    alert.setSummary("dswizards.error.zonesSelection");

                    return null;
                }

		// Perform homgeneity check for exclusive IP zones
		if (zonesPresent && !allSameIPType(selectedArr)) {
		    alert.setType(CCAlertInline.TYPE_ERROR);
		    alert.setValue(CCAlertInline.TYPE_ERROR);
		    alert.setSummary("dswizards.error.mixedZonesSelection");
		    return null;
		}

                if (zonesPresent) {
                    wizardModel.setWizardValue(
                        HASPWizardConstants.ZONES_PRESENT,
                        HASPWizardConstants.YES);
                    wizardModel.setWizardValue(
                        HASPWizardConstants.HASP_LOCATION_PREF,
                        HASPWizardConstants.FS_ONLY);
                } else {
                    wizardModel.setWizardValue(
                        HASPWizardConstants.ZONES_PRESENT,
                        HASPWizardConstants.NO);
                }

                HashMap returnValues = new HashMap();
                returnValues.put(Util.NODELIST, selectedArr);

                return returnValues;
            }
        } else {
            alert.setValue(CCAlertInline.TYPE_ERROR);
            alert.setSummary("nodeSelectionPanel.summary");
            alert.setDetail("nodeSelectionPanel.invalid");

            return null;
        }
    }

    /**
     * Checks whether the required MBean is accessible on the nodes returns Null
     * if MBeans are accessible, if not returns a proper error String mentioning
     * the user to check if the correct package is installed and whether the
     * required CACAO module is up
     */
    private String checkMBeanOnNodes(String nodeList[], Class mBeanToCheck,
        String packageName) {
        WizardI18N wizardi18n = GenericDSWizard.getWizardI18N();

        try {
            InfrastructureMBean mBean = GenericDSWizard
                .getInfrastructureMBean();
            String errorNodes[] = mBean.checkCacaoagent(nodeList);

            if ((errorNodes != null) && (errorNodes.length > 0)) {
                StringBuffer errorNodesString = new StringBuffer();

                for (int i = 0; i < errorNodes.length; i++) {
                    errorNodesString.append(errorNodes[i]);

                    if (i < (errorNodes.length - 1)) {
                        errorNodesString.append(",");
                    }
                }

                String error = wizardi18n.getString(
                        "dswizards.cacao.communicationFailure",
                        new String[] { errorNodesString.toString() });

                return error;
            }

            errorNodes = mBean.checkMBeans(nodeList, mBeanToCheck);

            if ((errorNodes != null) && (errorNodes.length > 0)) {
                StringBuffer errorNodesString = new StringBuffer();

                for (int i = 0; i < errorNodes.length; i++) {
                    errorNodesString.append(errorNodes[i]);

                    if (i < (errorNodes.length - 1)) {
                        errorNodesString.append(",");
                    }
                }

                String error = wizardi18n.getString("dswizards.error.pkg",
                        new Object[] {
                            packageName, errorNodesString.toString()
                        });

                return error;
            }
        } catch (Exception e) {
            String errorMsg = wizardi18n.getString("dswizards.cacao.down",
                    new Object[] { Util.getClusterEndpoint() });

            return errorMsg;
        }

        return null;
    }

    /**
     * Checks whether zones are supported for the given dataservice returns a
     * boolean
     */
    private boolean zonesEnable(Class mBeanToCheck) {

        ServiceConfig mBean = null;
        String nodeName = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);
        RequestContext rq = RequestManager.getRequestContext();
        String split_str[] = nodeName.split(Util.COLON);
        boolean retVal = false;

        try {
            mBean = (ServiceConfig) MBeanModel.getMBeanProxy(RequestManager
                    .getRequestContext(), Util.DOMAIN,
                    SpmUtil.getNodeEndpoint(rq, split_str[0]), mBeanToCheck,
                    null, false);

            HashMap retMap = mBean.discoverPossibilities(
                    new String[] { Util.ZONE_ENABLE }, null);
            Boolean value = (Boolean) retMap.get(Util.ZONE_ENABLE);

            if (value.booleanValue()) {
                retVal = true;
            }
        } catch (Exception e) {
        }

        return retVal;
    }


    /**
     * Homogeneity check for exclusive IP zones.  If one entry in
     * nodeList is an exclusive IP type zone, then all must be.
     *
     * @param  nodeList[]  RG nodelist array; each element is "node" or
     *			    "node:zone"
     * @return true if the homogeneity check passes; false if there are mixed IP
     *		types present
     */
    private boolean allSameIPType(String nodeList[]) {

	// If multiple zones are selected of different IP-type - error out
	// (i.e., if one zone is IP-type=exclusive, then all must be)
	boolean exclusiveZones = false;
	boolean nonExclusiveZones = false;
	for (int i = 0; i < nodeList.length; i++) {
	    String nodeKey = nodeList[i];

	    // is it a zone? is it exclusive IP zone?
	    if (ZoneUtil.isZone(nodeKey) &&
		ZoneUtil.isExclusiveIPZone(getZoneMBean(nodeKey))) {

		wizardModel.setWizardValue(Util.EXCLUSIVEIP, Boolean.TRUE);
		exclusiveZones = true;
	    } else {
		// regular node, or non-exclusive IP zone
		nonExclusiveZones = true;
	    }
	}
	if (exclusiveZones && nonExclusiveZones) {
	    return false;
	}

	return true;
    }

    /**
     * Returns zonembean for the given nodekey
     *
     */
    private ZoneMBean getZoneMBean(String nodeKey) {
        CCAlertInline alert = (CCAlertInline)getChild(CHILD_ALERT);
	String split_str[] = nodeKey.split(Util.COLON);
        RequestContext rq = RequestManager.getRequestContext();
	try {
	    ZoneMBean mBean = ZoneUtil.getZone(rq,
		SpmUtil.getNodeEndpoint(rq, split_str[0]),
		false, nodeKey);
	    return mBean;
	} catch (Exception e) {
	    alert.setType(CCAlertInline.TYPE_ERROR);
	    alert.setValue(CCAlertInline.TYPE_ERROR);
	    alert.setSummary("dswizards.cacao.communicationFailure");
	}
	return null;
    }
}
