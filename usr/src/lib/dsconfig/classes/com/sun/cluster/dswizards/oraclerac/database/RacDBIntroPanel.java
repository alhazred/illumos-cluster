/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)RacDBIntroPanel.java	1.17	09/03/11 SMI"
 */

package com.sun.cluster.dswizards.oraclerac.database;

// CLI Wizard SDK
import com.sun.cluster.agent.dataservices.utils.*;
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;

// Wizard Common
import com.sun.cluster.dswizards.common.CliUtil;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.oraclerac.invoker.RACInvokerConstants;
import com.sun.cluster.dswizards.oraclerac.invoker.RACInvokerCreator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

// Model classes
import com.sun.cluster.model.RACModel;


/**
 * Introduction page to the Hostname Wizards
 */
public class RacDBIntroPanel extends RacDBBasePanel {

    /**
     * Creates a RacDBIntroPanel Panel with the given name and associates it
     * with the WizardModel and WizardFlow
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     */
    public RacDBIntroPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(name, model, wizardFlow, wizardi18n);
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {

        try {
            String title = wizardi18n.getString("oraclerac.introPanel.title");
            String desc = wizardi18n.getString("oraclerac.introPanel.desc");

            String frError = wizardi18n.getString("oraclerac.framework.error");
            String storageError = wizardi18n.getString(
                    "oraclerac.framework.storage.error");
            String storageError2 = wizardi18n.getString(
                    "oraclerac.framework.storage.error2");
            String bchar = wizardi18n.getString("ttydisplay.back.char");
            String continue_confirm = wizardi18n.getString(
                    "cliwizards.continueConfirm");

            this.cancelDirection = false;

            // Title
            TTYDisplay.printTitle(title);

            TTYDisplay.pageText(desc);
            TTYDisplay.clear(2);

            String entry = TTYDisplay.promptForEntry(continue_txt);

            if (entry.equals(bchar)) {
                this.cancelDirection = true;

                return;
            }

	    String keyPrefix = (String) wizardModel.getWizardValue(Util.KEY_PREFIX);

            List storageScheme = (ArrayList) wizardModel.getWizardValue(
		keyPrefix + Util.ORF_STORAGE_MGMT_SCHEMES);

            // Set the property names to be serialized
            List toBeSerialized = new ArrayList();
            Object tmpObj = (ArrayList) wizardModel.getWizardValue(
                    Util.WIZARD_STATE);

            if (tmpObj != null) {
                toBeSerialized = (ArrayList) tmpObj;
            }

            toBeSerialized.add(Util.RAC_VER);
            toBeSerialized.add(Util.SEL_CRS_HOME);
            toBeSerialized.add(Util.SEL_DB_NAME);
            toBeSerialized.add(Util.SEL_ORACLE_HOME);
	    toBeSerialized.add(Util.CLUSTER_SEL);
	    toBeSerialized.add(Util.SEL_ZONE_CLUSTER);

            wizardModel.setWizardValue(Util.WIZARD_STATE, toBeSerialized);

            // storageScheme should never be null
            if ((storageScheme != null) && (storageScheme.size() == 1) &&
		storageScheme.contains(Util.HWRAID)) {

                // user has selected Hardware RAID without VM option
                return;
            } else {
                TTYDisplay.clear(2);

                if (!isStorageResourcePresent()) {

                    // selection contains HWRAID
                    if ((storageScheme == null) ||
			storageScheme.contains(Util.HWRAID)) {

                        // allow the user to proceed since its possible
                        // he just wants a setup for HWRAID
                        TTYDisplay.printError(storageError2);
                        TTYDisplay.clear(1);

                        String choice = TTYDisplay.getConfirmation(
                                continue_confirm, yes, no);

                        if (choice.equals(no)) {
                            System.exit(1);
                        }
                    } else {
                        TTYDisplay.printError(storageError);
                        TTYDisplay.clear(1);

                        String wizardExitPrompt = wizardi18n.getString(
                                "cliwizards.continueExit");
                        TTYDisplay.promptForEntry(wizardExitPrompt);
                        System.exit(1);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isStorageResourcePresent() {
	// Based on the storage scheme check for the resources

        String nodeList[] = (String[]) wizardModel.getWizardValue(
	    Util.BASE_CLUSTER_NODELIST);

	String nodeEndpoint = (String) wizardModel.getWizardValue(
	    Util.NODE_TO_CONNECT);

        // get oracle rac model
	RACModel racModel = RACInvokerCreator.getRACModel(); 

        String selZoneCluster = CliUtil.getZoneClusterName(wizardModel);

        HashMap helperData = new HashMap();
        helperData.put(Util.RG_NODELIST, nodeList);
	helperData.put(Util.SEL_ZONE_CLUSTER, selZoneCluster);

        String props[] = { Util.SCAL_DEVGROUP_RSLIST };
        List resources = null;
	// Check for scalable device group resources in the zone cluster
	HashMap hashMap = (HashMap) racModel.discoverPossibilities(
	    null, nodeEndpoint, props, helperData);
        List scalDevGroupList = (List) hashMap.get(Util.SCAL_DEVGROUP_RSLIST);
		
        props = new String[] { Util.SCAL_MOUNTPOINT_RSLIST };
	// Check for scalable mount point resources in the zone cluster
	hashMap = (HashMap) racModel.discoverPossibilities(
	    null, nodeEndpoint, props, helperData);

        List scalMountPtList = (List) hashMap.get(Util.SCAL_MOUNTPOINT_RSLIST);

        if (scalDevGroupList != null) {
            if (scalMountPtList != null) {
                resources = scalDevGroupList;
                resources.addAll(scalMountPtList);
            } else {
                resources = scalDevGroupList;
            }
        } else {

            if (scalMountPtList != null) {
                resources = scalMountPtList;
            }
        }

        if ((resources != null) && (resources.size() > 0)) {
            return true;
        } else {
            return false;
        }
    }
}
