/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */


/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)RACClusterSelectionPanel.java	1.4	08/12/10 SMI"
 */

package com.sun.cluster.dswizards.oraclerac.invoker;

import com.sun.cluster.model.DataModel;
import com.sun.cluster.model.ZoneClusterModel;

import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.common.WizardStateManager;
import com.sun.cluster.dswizards.oraclerac.database.RacDBBasePanel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.sun.cluster.agent.dataservices.utils.Util;

public class RACClusterSelectionPanel extends RacDBBasePanel {

    protected WizardI18N wizardi18n;

    /**
     * Creates a RACClusterSelectionPanel Panel with the given name
     * and associates it with the WizardModel, WizardFlow
     * and i18n reference
     * @param name Name of the Panel
     * @param model CliDSConfigWizardModel
     * @param wizardFlow WizardFlow
     * @param wizardi18n WizardI18N
     */
    public RACClusterSelectionPanel(String name,
            CliDSConfigWizardModel model,
            WizardFlow wizardFlow,
            WizardI18N wizardi18n) {
        super(name, model, wizardFlow, wizardi18n);
        // Get the I18N instance
        this.wizardi18n = wizardi18n;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     *                                             #consoleInteraction
     */
    public void consoleInteraction() {
        title = wizardi18n.getString("racwizard.invoker.location.title");
	desc = wizardi18n.getString("racwizard.invoker.location.desc");
        String preference = wizardi18n.getString("racwizard.invoker.location.preference.label");
        String option1 = wizardi18n.getString("racwizard.invoker.location.preference.option1");
        String option2 = wizardi18n.getString("racwizard.invoker.location.preference.option2");
	String descZoneCluster = wizardi18n.getString(
		"racwizard.invoker.zonecluster.desc");
	String help_text = wizardi18n.getString(
		"racwizard.invoker.location.cli.help");
	String zoneClusterTitle = wizardi18n.getString(
		"racwizard.invoker.zonecluster.title");

        int defSelection = 0;
	String selectedZoneCluster = null;
	String defZoneClusterSelection = null;

	boolean bRepeat = false;
	String pref = null;
	String[] zoneClusterList = null;

	do { 
	    bRepeat = false;

	    TTYDisplay.setNavEnabled(true);
	    this.cancelDirection = false;

	    // Title
	    TTYDisplay.printTitle(title);

	    String[] options = {option1,
				option2};

	    wizardModel.clearWizardData();
	    wizardModel.setWizardStateManager(new WizardStateManager(
		    RACInvokerConstants.RAC_STATE_FILE));

	    // Get the data preference
	    pref = (String) wizardModel.getWizardValue(Util.CLUSTER_SEL);
	    if (pref == null || pref.equals(Util.BASE_CLUSTER)) {
		defSelection = 0;
	    } else {
		defSelection = 1;
	    }

	    int index = TTYDisplay.getMenuOption(
				    preference,
				    options,
				    defSelection);

	    if (index == TTYDisplay.BACK_PRESSED) {
	       System.exit(0);
	    }

	    String selection = options[index];
	    if (selection.equals(option1)) {
		pref = new String(Util.BASE_CLUSTER);
	    } else if (selection.equals(option2)) {
		pref = new String(Util.ZONE_CLUSTER);
	    }

	    // Fetch the zone cluster list from the zone cluster model

	    try {
		DataModel dm = DataModel.getDataModel(null);
		zoneClusterList = dm.getZoneClusterModel().
			getZoneClusterList(null);
	    } catch (Exception e) {
		// do nothing; if user selected zone cluster the wizard
		// will display an appropriate error message
	    }

	    if (pref.equals(Util.ZONE_CLUSTER)) {
		if (zoneClusterList == null || zoneClusterList.length == 0) {
		    TTYDisplay.printError(wizardi18n.
			getString("racwizard.invoker.location.error"));
		    TTYDisplay.promptForEntry(
			wizardi18n.getString("cliwizards.continueExit"));
		    System.exit(1);
		}

		defZoneClusterSelection = (String) wizardModel.
			getWizardValue(Util.SEL_ZONE_CLUSTER);

		TTYDisplay.clearScreen();
		TTYDisplay.printTitle(zoneClusterTitle);
		List selList = TTYDisplay.getScrollingOption(
			descZoneCluster, zoneClusterList,
			null, defZoneClusterSelection,
			help_text, true);

		if (selList == null) {
		    // Back key pressed
		    this.cancelDirection = true;
		    bRepeat = true;
		    continue;
		}

		String[] sel = (String [])selList.toArray(new String[0]);
		selectedZoneCluster = sel[0];
	    }

	} while (bRepeat);

        StringBuffer newKey = new StringBuffer();
        if (selectedZoneCluster != null &&
	    selectedZoneCluster.trim().length() > 0 ) {
            newKey.append(selectedZoneCluster);
            newKey.append(Util.COLON);
        }

	wizardModel.selectCurrWizardContext();
        wizardModel.setWizardValue(Util.KEY_PREFIX, newKey.toString());
        wizardModel.setWizardValue(Util.CLUSTER_SEL, pref);
	wizardModel.setWizardValue(Util.SEL_ZONE_CLUSTER, selectedZoneCluster);
    }
}
