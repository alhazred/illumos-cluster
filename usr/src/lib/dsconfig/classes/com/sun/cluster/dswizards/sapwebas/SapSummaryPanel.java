/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)SapSummaryPanel.java 1.17     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.sapwebas;

// J2SE
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

// JMX
import javax.management.remote.JMXConnector;

// CMAS
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.dataservices.sapwebas.SapWebasMBean;
import com.sun.cluster.agent.dataservices.utils.DataServicesUtil;
import com.sun.cluster.agent.dataservices.utils.Util;

// CLI Wizard SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;

// Wizard Common
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;

public class SapSummaryPanel extends WizardLeaf {

    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N wizardi18n; // Message Bundle
    private String NAME = "name";
    private String VALUE = "value";
    private String KEY = "key";
    private String DESC = "desc";
    private String TYPE = "type";
    private String DISCKEY = "key_for_discovery";
    private String PRIMARY = ".primary";
    private String SECONDARY = ".secondary";
    private int rowId = 0;
    private Map confMap = new HashMap();


    /**
     * Creates a new instance of SapSummaryPanel
     */
    public SapSummaryPanel() {
        super();
    }

    /**
     * Creates a SapSummaryPanel Panel with the given name and tree manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public SapSummaryPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    public SapSummaryPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;

        // Get the I18N instance
        this.wizardi18n = wizardi18n;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {
        String title = wizardi18n.getString("sap.sapSummaryPanel.title");
        String table_title = wizardi18n.getString(
                "sap.sapSummaryPanel.table.title");
        String desc = wizardi18n.getString("sap.sapSummaryPanel.desc");
        String help_text = wizardi18n.getString("sap.sapSummaryPanel.help.cli");
        String yes = wizardi18n.getString("cliwizards.yes");
        String no = wizardi18n.getString("cliwizards.no");
        String confirm_txt = wizardi18n.getString("cliwizards.confirmMessage");
        String continue_txt = wizardi18n.getString("cliwizards.continue");
        String back = wizardi18n.getString("ttydisplay.back.char");

        String propNameText = wizardi18n.getString("hasp.reviewpanel.propname");
        String propTypeText = wizardi18n.getString("hasp.reviewpanel.proptype");
        String propDescText = wizardi18n.getString("hasp.reviewpanel.propdesc");
        String currValText = wizardi18n.getString("hasp.reviewpanel.currval");
        String newValText = wizardi18n.getString("hasp.reviewpanel.newval");
        String subheading[] = new String[2];
        subheading[0] = wizardi18n.getString(
                "sap.centralServicesDetailsPanel.table.col0");
        subheading[1] = wizardi18n.getString(
                "sap.centralServicesDetailsPanel.table.col1");

        Boolean enqSelected;
        Boolean scsSelected;
        String option;

        DataServicesUtil.clearScreen();

        // Title
        TTYDisplay.clear(1);
        TTYDisplay.printTitle(title);
        TTYDisplay.pageText(desc);
        TTYDisplay.clear(1);

        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        HashMap customTag = new HashMap();
        String continueKey = wizardi18n.getString("ttydisplay.c.char");
        String continueMenuText = wizardi18n.getString(
                "ttydisplay.menu.create.config");
        customTag.put(continueKey, continueMenuText);

        HashMap tableEntries = new HashMap();
        int i = 0;

        while (true) {
            TTYDisplay.clear(1);
            TTYDisplay.setNavEnabled(true);

            Vector data = new Vector();

            addtoTable(tableEntries, data, "sap.sapSummaryPanel.sid.name",
                "sap.sapSummaryPanel.sid.desc", Util.SAP_SID);

            addNodelist(tableEntries, data,
                "sap.sapSummaryPanel.csNodeList.name",
                "sap.sapSummaryPanel.csNodeList.desc", Util.RG_NODELIST);

            addtoTable(tableEntries, data, "sap.sapSummaryPanel.user.name",
                "sap.sapSummaryPanel.user.desc", Util.SAP_USER);

            addtoTable(tableEntries, data,
                "sap.sapSummaryPanel.enqservloc.name",
                "sap.sapSummaryPanel.enqservloc.desc", Util.ENQ_SERVER_LOC);

            addtoTable(tableEntries, data, "sap.sapSummaryPanel.enqino.name",
                "sap.sapSummaryPanel.enqino.desc", Util.ENQ_INSTANCE_NUM);

            addtoTable(tableEntries, data, "sap.sapSummaryPanel.enqprof.name",
                "sap.sapSummaryPanel.enqprof.desc",
                Util.ENQ_SERVER_PROFILE_LOC);

            addtoTable(tableEntries, data, "sap.sapSummaryPanel.db.name",
                "sap.sapSummaryPanel.db.desc", Util.SELECTED_DB);

            addtoTable(tableEntries, data, "sap.sapSummaryPanel.scsino.name",
                "sap.sapSummaryPanel.scsino.desc", Util.SCS_INSTANCE_NUM);

            addtoTable(tableEntries, data, "sap.sapSummaryPanel.scsname.name",
                "sap.sapSummaryPanel.scsname.desc", Util.SCS_INSTANCE_NAME);

            addVectortoTable(tableEntries, data,
                "sap.sapSummaryPanel.cshasp.name",
                "sap.sapSummaryPanel.cshasp.desc", Util.CS_SELECTED_STORAGE);

            addArraytoTable(tableEntries, data, "sap.sapSummaryPanel.csfs.name",
                "sap.sapSummaryPanel.csfs.desc", Util.CS_SELECTED_FS);

            addArraytoTable(tableEntries, data, "sap.sapSummaryPanel.csgd.name",
                "sap.sapSummaryPanel.csgd.desc", Util.CS_SELECTED_GD);

            addVectortoTable(tableEntries, data,
                "sap.sapSummaryPanel.cslh.name",
                "sap.sapSummaryPanel.cslh.desc", Util.SELECTED_LH);

            addArraytoTable(tableEntries, data,
                "sap.sapSummaryPanel.csnewlh.name",
                "sap.sapSummaryPanel.csnewlh.desc", Util.CS_NEW_HOSTS);

            addtoTable(tableEntries, data, "sap.sapReviewPanel.enqrg.name",
                "sap.sapReviewPanel.enqrg.desc", Util.CS_RG_NAME);

            addtoTable(tableEntries, data, "sap.sapReviewPanel.enqres.name",
                "sap.sapReviewPanel.enqres.desc", Util.ENQ_RES_NAME);

            addtoTable(tableEntries, data, "sap.sapReviewPanel.scsres.name",
                "sap.sapReviewPanel.scsres.desc", Util.SCS_RES_NAME);

            addtoTable(tableEntries, data, "sap.sapReviewPanel.cshasp.name",
                "sap.sapReviewPanel.cshasp.desc", Util.CS_STORAGE_NAME);

            addtoTable(tableEntries, data, "sap.sapReviewPanel.cslh.name",
                "sap.sapReviewPanel.cslh.desc", Util.CS_LH_NAME);

            HashMap vfstabEntries = (HashMap) wizardModel.getWizardValue(
                    Util.CS_VFSTAB_ENTRIES);
            confMap.put(Util.CS_VFSTAB_ENTRIES, vfstabEntries);

            Boolean isRepSelected = (Boolean) wizardModel.getWizardValue(
                    Util.REP_COMPONENT);

            if (isRepSelected.booleanValue()) {

                addtoTable(tableEntries, data,
                    "sap.sapSummaryPanel.reploc.name",
                    "sap.sapSummaryPanel.reploc.desc", Util.REP_SERVER_LOC);

                addtoTable(tableEntries, data,
                    "sap.sapSummaryPanel.repprof.name",
                    "sap.sapSummaryPanel.repprof.desc",
                    Util.REP_SERVER_PROFILE_LOC);

                addVectortoTable(tableEntries, data,
                    "sap.sapSummaryPanel.rephasp.name",
                    "sap.sapSummaryPanel.rephasp.desc", Util.REP_SELECTED_STORAGE);

                addArraytoTable(tableEntries, data,
                    "sap.sapSummaryPanel.repfs.name",
                    "sap.sapSummaryPanel.repfs.desc", Util.REP_SELECTED_FS);

                addArraytoTable(tableEntries, data,
                    "sap.sapSummaryPanel.repgd.name",
                    "sap.sapSummaryPanel.repgd.desc", Util.REP_SELECTED_GD);

                addVectortoTable(tableEntries, data,
                    "sap.sapSummaryPanel.replh.name",
                    "sap.sapSummaryPanel.replh.desc", Util.REP_SELECTED_LH);

                addArraytoTable(tableEntries, data,
                    "sap.sapSummaryPanel.repnewlh.name",
                    "sap.sapSummaryPanel.repnewlh.desc", Util.REP_NEW_HOSTS);

                addtoTable(tableEntries, data, "sap.sapReviewPanel.reprg.name",
                    "sap.sapReviewPanel.reprg.desc", Util.REP_RG_NAME);

                addtoTable(tableEntries, data, "sap.sapReviewPanel.repres.name",
                    "sap.sapReviewPanel.repres.desc", Util.REP_RES_NAME);

                addtoTable(tableEntries, data,
                    "sap.sapReviewPanel.rephasp.name",
                    "sap.sapReviewPanel.rephasp.desc", Util.REP_STORAGE_NAME);

                addtoTable(tableEntries, data, "sap.sapReviewPanel.replh.name",
                    "sap.sapReviewPanel.replh.desc", Util.REP_LH_NAME);

                vfstabEntries = (HashMap) wizardModel.getWizardValue(
                        Util.REP_VFSTAB_ENTRIES);
                confMap.put(Util.REP_VFSTAB_ENTRIES, vfstabEntries);

            }

            option = TTYDisplay.create2DTable(table_title, subheading, data,
                    customTag, help_text);

            if (option.equals(back)) {
                this.cancelDirection = true;

                return;
            }

            if (option.equals(continueKey)) {
                wizardModel.setWizardValue(Util.SAPRESULT, confMap);

                return;
            }

            this.cancelDirection = false;

            String sprop;
            String currVal;
            String newVal;
            HashMap selectedRow = (HashMap) tableEntries.get(option);

            if (selectedRow == null) {
                TTYDisplay.promptForEntry(continue_txt);

                break;
            }

            TTYDisplay.setNavEnabled(false);
            TTYDisplay.pageText(propNameText +
                wizardi18n.getString((String) selectedRow.get(NAME)));

            TTYDisplay.pageText(propDescText +
                wizardi18n.getString((String) selectedRow.get(DESC)));

            TTYDisplay.pageText(currValText + (String) selectedRow.get(VALUE));
            TTYDisplay.promptForEntry(continue_txt);
        }
    }

    private void addtoTable(HashMap xTableModel, Vector xData, String xName,
        String xDesc, String xModelKey) {
        HashMap tableRow = new HashMap();
        tableRow.put(NAME, xName);
        tableRow.put(DESC, xDesc);
        tableRow.put(KEY, xModelKey);

        String value = (String) wizardModel.getWizardValue(xModelKey);

        if (value != null) {
            tableRow.put(VALUE, value);
            xData.add(wizardi18n.getString(xName));
            xData.add(value);
            xTableModel.put(String.valueOf(++rowId), tableRow);
            confMap.put(xModelKey, value);
        }
    }

    private void addVectortoTable(HashMap xTableModel, Vector xData,
        String xName, String xDesc, String xModelKey) {
        HashMap tableRow = new HashMap();
        tableRow.put(NAME, xName);
        tableRow.put(DESC, xDesc);
        tableRow.put(KEY, xModelKey);

        Vector value = (Vector) wizardModel.getWizardValue(xModelKey);

        if (value != null) {
            String valArray[] = (String[]) value.toArray(new String[0]);
            String val = SapWebasWizardCreator.convertArraytoString(valArray);

            if ((val != null) && (val.trim().length() > 0)) {
                tableRow.put(VALUE, val);
                xData.add(wizardi18n.getString(xName));
                xData.add(val);
                xTableModel.put(String.valueOf(++rowId), tableRow);
            }

            confMap.put(xModelKey, val);
        }
    }


    private void addArraytoTable(HashMap xTableModel, Vector xData,
        String xName, String xDesc, String xModelKey) {
        HashMap tableRow = new HashMap();
        tableRow.put(NAME, xName);
        tableRow.put(DESC, xDesc);
        tableRow.put(KEY, xModelKey);

        String value[] = (String[]) wizardModel.getWizardValue(xModelKey);

        if (value != null) {
            String val = SapWebasWizardCreator.convertArraytoString(value);
            tableRow.put(VALUE, val);
            xData.add(wizardi18n.getString(xName));
            xData.add(val);
            xTableModel.put(String.valueOf(++rowId), tableRow);
            confMap.put(xModelKey, value);
        }
    }


    private void addNodelist(HashMap xTableModel, Vector xData, String xName,
        String xDesc, String xModelKey) {
        HashMap tableRow = new HashMap();
        String primaryNode;
        String secNodes;

        String name = xName + PRIMARY;
        String desc = xDesc + PRIMARY;

        String value[] = (String[]) wizardModel.getWizardValue(xModelKey);

        if (value == null) {
            primaryNode = "";
            secNodes = "";
        } else {
            primaryNode = value[0];

            String potentialNodes[] = new String[value.length - 1];

            for (int i = 0; i < potentialNodes.length; i++) {
                potentialNodes[i] = value[i + 1];
            }

            secNodes = SapWebasWizardCreator.join(potentialNodes, Util.COMMA);
            confMap.put(xModelKey, value);
        }

        tableRow.put(NAME, name);
        tableRow.put(DESC, desc);
        tableRow.put(VALUE, primaryNode);
        xData.add(wizardi18n.getString(name));
        xData.add(primaryNode);
        xTableModel.put(String.valueOf(++rowId), tableRow);

        name = xName + SECONDARY;
        desc = xDesc + SECONDARY;
        tableRow = null;
        tableRow = new HashMap();
        tableRow.put(NAME, name);
        tableRow.put(DESC, desc);
        tableRow.put(VALUE, secNodes);
        xData.add(wizardi18n.getString(name));
        xData.add(secNodes);
        xTableModel.put(String.valueOf(++rowId), tableRow);
    }

}
