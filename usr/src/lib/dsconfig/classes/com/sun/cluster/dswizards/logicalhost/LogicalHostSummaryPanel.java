/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)LogicalHostSummaryPanel.java 1.23     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.logicalhost;

// CMASS
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.dataservices.logicalhost.LogicalHostMBean;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.rgm.property.ResourceProperty;
import com.sun.cluster.agent.rgm.property.ResourcePropertyStringArray;

// CLI Wizard SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;

// Wizard
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;

// J2SE
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

// JMX
import javax.management.remote.JMXConnector;


/**
 * This is the Summary Panel Class for the LogicalHost Wizard.
 */
public class LogicalHostSummaryPanel extends WizardLeaf {

    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N messages; // Message Bundle
    private JMXConnector localConnection; // Connector

    /**
     * Creates a new instance of LogicalHostSummaryPanel
     */
    public LogicalHostSummaryPanel() {
        super();
    }

    /**
     * Creates a LogicalHostSummaryPanel with the given name and manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public LogicalHostSummaryPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates a LogicalHostSummaryPanel with the given name and WizardModel and
     * WizardFlow
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     * @param  messages  WizardI18N
     */
    public LogicalHostSummaryPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N messages) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;

        // Get the I18N instance
        this.messages = messages;
        localConnection = getConnection(Util.getClusterEndpoint());

    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.
     *		WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {

        String heading1 = messages.getString(
                "hasp.create.filesystem.review.heading1");
        String heading2 = messages.getString(
                "hasp.create.filesystem.review.heading2");
        String scTableTitle = messages.getString("hasp.summarypanel.desc2");
        String para = messages.getString("lhwizard.wizard.step6.info");
        String cchar = messages.getString("ttydisplay.c.char");
        String createConfigurationText = messages.getString(
                "ttydisplay.menu.createConfiguration");
        String moreSymbol = messages.getString("ttydisplay.fwd.arrow");
        String desc = null;
        String title = messages.getString("lhwizard.wizard.step6.title");

        // Summary Panel Table
        Vector summaryPanel = new Vector();

        wizardModel.selectCurrWizardContext();

        // First Row - Resource Name
        String resourceName = (String) wizardModel.getWizardValue(
                Util.RESOURCENAME);

        summaryPanel.add(messages.getString(
                "lhwizard.reviewPanel.resourceTitle"));
        summaryPanel.add(resourceName);

        // Second Row - Resource Group Name
        String resourceGroupName = (String) wizardModel.getWizardValue(
                Util.RESOURCEGROUPNAME);

        summaryPanel.add(messages.getString(
                "lhwizard.reviewPanel.resourceGroupTitle"));
        summaryPanel.add(resourceGroupName);

        // Third Row - Node List
        String nodeList[] = (String[]) wizardModel.getWizardValue(
                Util.RG_NODELIST);
        StringBuffer nodeNames = new StringBuffer();

        for (int i = 0; i < nodeList.length; i++) {
            nodeNames.append(nodeList[i]);

            if (i < (nodeList.length - 1))
                nodeNames.append(" ");
        }

        summaryPanel.add(messages.getString(
                "lhwizard.summaryPanel.nodeListTitle"));
        summaryPanel.add(moreSymbol);

        // Fourth Row - Hostname List
        String hostnameList[] = (String[]) wizardModel.getWizardValue(
                Util.HOSTNAMELIST);
        StringBuffer hostnames = new StringBuffer();

        for (int i = 0; i < hostnameList.length; i++) {
            hostnames.append(hostnameList[i] + " ");
        }

        summaryPanel.add(messages.getString(
                "lhwizard.summaryPanel.hostnameListTitle"));
        summaryPanel.add(hostnames.toString());

        // Fifth Row - Interfaces List
        String netifList[] = null;
        netifList = (String[]) wizardModel.getWizardValue(
                LogicalHostWizardConstants.SEL_NETIFLIST);

        StringBuffer ipmpnames = new StringBuffer();

        if (netifList != null) {

            for (int i = 0; i < netifList.length; i++) {
                ipmpnames.append(netifList[i] + " ");
            }
        }

        summaryPanel.add(messages.getString(
                "lhwizard.summaryPanel.interfaceListTitle"));
        summaryPanel.add(moreSymbol);

        boolean createConfig;

        do {
            TTYDisplay.clearScreen();
            TTYDisplay.clear(1);
            TTYDisplay.printTitle(title);
            TTYDisplay.clear(1);
            TTYDisplay.pageText(para, 4);

            TTYDisplay.setNavEnabled(true);
            this.cancelDirection = false;

            HashMap customTags = new HashMap();
            customTags.put(cchar, createConfigurationText);

            String subheadings[] = { heading1, heading2 };

            // Display the Table
            TTYDisplay.clear(1);

            String option = TTYDisplay.create2DTable(scTableTitle, subheadings,
                    summaryPanel, customTags, "");

            createConfig = false;

            if (option.equals("<")) {
                this.cancelDirection = true;

                return;
            }

            if (option.equals("1")) { // Resource Information
                TTYDisplay.clear(2);
                desc = messages.getString("lhwizard.reviewPanel.resourceInfo");
                TTYDisplay.pageText(messages.getString("cliwizards.desc") +
                    desc, 6);
                TTYDisplay.clear(1);
                TTYDisplay.pageText(messages.getString(
                        "lhwizard.reviewPanel.resourceTitle") + resourceName,
                    6);
            } else if (option.equals("2")) { // Resource Group Information
                TTYDisplay.clear(2);
                desc = messages.getString(
                        "lhwizard.reviewPanel.resourceGroupInfo");
                TTYDisplay.pageText(messages.getString("cliwizards.desc") +
                    desc, 6);
                TTYDisplay.clear(1);
                TTYDisplay.pageText(messages.getString(
                        "lhwizard.reviewPanel.resourceGroupTitle") +
                    resourceGroupName, 6);
            } else if (option.equals("3")) { // NodeList
                TTYDisplay.clear(2);
                desc = messages.getString("lhwizard.summaryPanel.nodeListDesc");
                TTYDisplay.pageText(messages.getString("cliwizards.desc") +
                    desc, 6);
                TTYDisplay.clear(1);

                TTYDisplay.pageText(messages.getString(
                        "lhwizard.summaryPanel.nodeListTitle") +
                    nodeNames.toString(), 6);
            } else if (option.equals("5")) { // NetIfList
                TTYDisplay.clear(2);
                desc = messages.getString(
                        "lhwizard.summaryPanel.netifListDesc");
                TTYDisplay.pageText(messages.getString("cliwizards.desc") +
                    desc, 6);
                TTYDisplay.clear(1);

                TTYDisplay.pageText(messages.getString(
                        "lhwizard.summaryPanel.interfaceListTitle") +
                    ipmpnames.toString(), 6);
            } else if (option.equals("4")) { // Hostnames
                TTYDisplay.clear(2);
                desc = messages.getString(
                        "lhwizard.summaryPanel.hostnameListDesc");
                TTYDisplay.pageText(messages.getString("cliwizards.desc") +
                    desc, 6);
                TTYDisplay.clear(1);

                TTYDisplay.pageText(messages.getString(
                        "lhwizard.summaryPanel.hostnameListTitle") +
                    hostnames.toString(), 6);
            } else if (option.equals(cchar)) { // Generate Commands

                createConfig = true;

                // Create the Command Generation Structure
                Map confMap = new HashMap();

                // ResourceGroup Structure

                // ResourceGroup Name
                confMap.put(Util.SERVER_GROUP + Util.NAME_TAG,
                    resourceGroupName);

                // ResourceGroup Properties
                Map rgPropMap = new HashMap();
                rgPropMap.put(Util.NODELIST,
                    nodeNames.toString().replace(' ', ','));
                confMap.put(Util.SERVER_GROUP + Util.RGPROP_TAG, rgPropMap);

                // Resource Structure
                // Resource Name
                confMap.put(Util.LOGICALHOST + Util.NAME_TAG, resourceName);

                // Resource Properties
                List logicalHostExtProp = new ArrayList();
                List logicalHostSysProp = new ArrayList();

                ResourceProperty rp = new ResourcePropertyStringArray(
                        Util.HOSTNAMELIST, hostnameList);
                logicalHostExtProp.add(rp);

                if (netifList != null) {
                    rp = new ResourcePropertyStringArray(Util.NETIFLIST,
                            netifList);
                    logicalHostExtProp.add(rp);
                }

                confMap.put(Util.LOGICALHOST + Util.SYSPROP_TAG,
                    logicalHostSysProp);
                confMap.put(Util.LOGICALHOST + Util.EXTPROP_TAG,
                    logicalHostExtProp);

                // Generating Commands
                generateCommands(confMap);

                return;
            }

            TTYDisplay.clear(2);
            TTYDisplay.promptForEntry(messages.getString(
                    "cliwizards.continue"));
            TTYDisplay.clear(2);
        } while (!createConfig);
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#isDisplayComplete
     */
    public boolean isDisplayComplete() {
        return true;
    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }

    /**
     * Generates the commands by invoking the command generator
     */
    private boolean generateCommands(Map confMap) {
        List commandList = null;
        Thread t = null;
        LogicalHostMBean mBean = null;
        boolean retVal = true;

        try {
            mBean = LogicalHostWizardCreator.getLogicalHostMBean(
                    localConnection);
            TTYDisplay.clear(1);
            t = TTYDisplay.busy(messages.getString(
                        "cliwizards.executingMessage"));
            t.start();
            commandList = mBean.generateCommands(confMap);
        } catch (CommandExecutionException cee) {
            wizardModel.setWizardValue(Util.EXITSTATUS, cee);
            commandList = mBean.getCommandList();
            retVal = false;
        } finally {
            wizardModel.setWizardValue(Util.CMDLIST, commandList);

            try {
                t.interrupt();
                t.join();
            } catch (Exception exe) {
            }
        }

        return retVal;
    }
}
