/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)HAOracleWizardConstants.java 1.9     08/05/20 SMI"
 */

package com.sun.cluster.dswizards.haoracle;

/**
 * This class holds all the constants for the HAOracleWizard
 */
public class HAOracleWizardConstants {

    /*
     * Constants to be used in the wizard flow
     */

    public static final String ORACLE_PKG_NAME = "SUNWscor";
    public static final String HAORACLE_PREF = "HAOracle_Preference";
    public static final String SEL_ORACLE_HOME = "Selected_Oracle_Home";
    public static final String SEL_ORACLE_SID = "Selected_Oracle_Sid";
    public static final String SEL_HASP_RS = "Selected_HASP_Resources";
    public static final String SEL_HASP_RG = "Selected_HASP_ResourceGroup";
    public static final String CREATE_HASP_RS = "Create_HASP_RS";
    public static final String SELECT_EXISTING = "1";
    public static final String CREATE_NEW = "2";
    public static final String SEL_LH_RS = "Selected_LH_Resource";
    public static final String SEL_LH_RG = "Selected_LH_ResourceGroup";
    public static final String CREATE_LH_RS = "Create_LH_RS";
    public static final String SHOW_ALL = "ShowAll";
    public static final String SERVER_ONLY = "1";
    public static final String LISTENER_ONLY = "2";
    public static final String SERVER_AND_LISTENER = "3";
    public static final String SERVER_RS_NAME = "server_resource_name";
    public static final String LISTENER_RS_NAME = "listener_resource_name";
    public static final String HASP_RS_NAME = "hastorageplus_resource_name";
    public static final String LH_RS_NAME = "logicalhost_resource_name";
    public static final String NEW_LH_RS = "New_LogicalHost_Resource";
    public static final String NEW_HASP_RS = "New_HAStoragePlus_Resource";
    public static final String NEW_RG = "New_ResourceGroup";
    public static final String AVAIL_NODELIST = "available_nodes";
    public static final String SYSTEM_DISCOVERED_ORAHOME = "1";
    public static final String OVERRIDE_ORAHOME = "2";
    public static final String SYSTEM_DISCOVERED_ORASID = "1";
    public static final String OVERRIDE_ORASID = "2";
    public static final String PANEL_0 = "haoracleIntroPanel";
    public static final String PANEL_1 = "haoracleNodeSelectionPanel";
    public static final String PANEL_2 = "haoraclePreferencePanel";
    public static final String PANEL_3 = "haoracleHomeSelectionPanel";
    public static final String PANEL_4 = "haoracleSidSelectionPanel";
    public static final String PANEL_5 = "haoraclePropsReviewPanel";
    public static final String PANEL_6 = "haoracleStorageSelectionPanel";
    public static final String PANEL_7 = "haoracleLogicalHostSelectionPanel";
    public static final String PANEL_8 = "haoracleReviewPanel";
    public static final String PANEL_9 = "haoracleSummaryPanel";
    public static final String PANEL_10 = "haoracleResultPanel";

    // Wizard Model keys
    public static final String NEW_HOSTNAMES_DEFINED = "hostnames_defined";
    public static final String NEW_FS_DEFINED = "filessystems_defined";
    public static final String NEW_DEVICES_DEFINED = "devices_defined";

}
