/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ApacheWizardMountPointPanel.java 1.7     08/12/10 SMI"
 */


package com.sun.cluster.dswizards.apache;


// Java

// CMASS
import com.sun.cluster.agent.dataservices.apache.Apache;
import com.sun.cluster.agent.dataservices.apache.ApacheMBean;
import com.sun.cluster.agent.dataservices.hastorageplus.HAStoragePlusMBean;
import com.sun.cluster.agent.dataservices.logicalhost.LogicalHost;
import com.sun.cluster.agent.dataservices.sharedaddress.SharedAddress;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.rgm.ResourceMBean;
import com.sun.cluster.common.TrustedMBeanModel;

// CLI Wizard SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliStorageSelectionPanel;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.hastorageplus.HASPWizardConstants;
import com.sun.cluster.dswizards.hastorageplus.HASPWizardCreator;
import com.sun.cluster.dswizards.logicalhost.LogicalHostWizardConstants;
import com.sun.cluster.dswizards.logicalhost.LogicalHostWizardCreator;
import com.sun.cluster.dswizards.sharedaddress.SharedAddressWizardCreator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.management.AttributeValueExp;
import javax.management.Query;
import javax.management.QueryExp;
import javax.management.remote.JMXConnector;


// CLI wizard
// import com.sun.cluster.dswizards.logicalhost.LogicalHostWizardCreator;


/**
 * The Panel Class that gets a MountPoint as input from the User.
 */
public class ApacheWizardMountPointPanel extends WizardLeaf {


    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N wizardi18n; // Message Bundle

    private JMXConnector connection = null;
    private ApacheMBean apacheMBean = null; // Reference to MBean

    /**
     * Creates a new instance of ApacheWizardMountPointPanel.
     */
    public ApacheWizardMountPointPanel() {
        super();
    }

    /**
     * Creates a ApacheWizardMountPointPanel Panel with the given name and tree
     * manager.
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public ApacheWizardMountPointPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates a ApacheWizardMountPointPanel Panel for either the Apache Wizard
     * with the given name and associates it with the WizardModel and
     * WizardFlow.
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     * @param  wizardi18n  Resource Bundle
     */
    public ApacheWizardMountPointPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;
        this.wizardi18n = wizardi18n;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {

        String title_txt = wizardi18n.getString("apache.mountPointEntry.name");
        String mountPointEntry_title = wizardi18n.getString(
                "apache.mountPointEntry.listtitle");
        String mountPointEntry_desc = wizardi18n.getString(
                "apache.mountPointEntry.desc");
        String mountPointEntry_help = wizardi18n.getString(
                "apache.mountPointEntry.clihelp");
        String yes = wizardi18n.getString("cliwizards.yes");
        String no = wizardi18n.getString("cliwizards.no");
        String back = wizardi18n.getString("ttydisplay.back.char");
        String dchar = wizardi18n.getString("ttydisplay.d.char");
        String done = wizardi18n.getString("ttydisplay.menu.done");

        String fileSysMtpts[] = new String[] {};
        String optionSelected[] = null;
        String defSelection = "";

        int index = 0;
        boolean newlyCreated = false;
        String storageResource = "";
        HashMap customTags = new HashMap();
        customTags.put(dchar, done);

        // clear wizardmodel variable for displaying this panel
        wizardModel.setWizardValue(ApacheWizardConstants.APACHE_MULTIPLE_MOUNTS,
            null);

        // Get the ApacheMbean reference
        if (apacheMBean == null) {
            String nodeEndPoint = (String) wizardModel.getWizardValue(
                    Util.NODE_TO_CONNECT);
            connection = getConnection(nodeEndPoint);
            apacheMBean = ApacheWizardCreator.getApacheMBeanOnNode(connection);
        }

        Vector selectedResources = (Vector) wizardModel.getWizardValue(
                Util.SEL_RS);
        String selectedFS[] = (String[]) wizardModel.getWizardValue(
                ApacheWizardConstants.APACHE_FS_MTPTS);

        if ((selectedFS != null) && (selectedFS.length > 0)) {
            fileSysMtpts = selectedFS;
            defSelection = selectedFS[0];
            newlyCreated = true;
        } else if ((selectedResources != null) &&
                (selectedResources.size() > 0)) {
            storageResource = (String) selectedResources.get(0);

            // Get the Mountpoint for the user selected resource
            Map storageResources = (HashMap) wizardModel.getWizardValue(
                    ApacheWizardConstants.APACHE_STORAGE_RESOURCES);
            String mountPoints[] =
                (String[]) storageResources.get(storageResource);
            fileSysMtpts = mountPoints;
            defSelection = mountPoints[0];
        }

        boolean flag;

        do {
            flag = true;

            // Enable Navigation
            TTYDisplay.setNavEnabled(true);
            this.cancelDirection = false;
            TTYDisplay.clearScreen();
            TTYDisplay.clear(1);

            // Title
            TTYDisplay.printTitle(title_txt);
            TTYDisplay.clear(1);
            TTYDisplay.pageText(mountPointEntry_desc, 4);
            TTYDisplay.clear(1);

            Vector vectData = new Vector();

            for (int ctr = 0; ctr < fileSysMtpts.length; ctr++) {
                vectData.add(fileSysMtpts[ctr]);
            }

            List defSelectionList = new ArrayList();
            defSelectionList.add(
                (new Integer(vectData.indexOf(defSelection) + 1)).toString());

            optionSelected = TTYDisplay.create2DTable(mountPointEntry_title,
                    new String[] {}, vectData, vectData.size(), 1, customTags,
                    mountPointEntry_help, null, true, defSelectionList);

            if (optionSelected == null) {
                this.cancelDirection = true;

                return;
            }

            boolean doneSelected = false;

            if ((optionSelected.length != 1) &&
                    optionSelected[1].equals(dchar)) {
                doneSelected = true;
            }

            if (doneSelected) {

                try {
                    index = Integer.valueOf(optionSelected[0]).intValue() - 1;
                } catch (NumberFormatException nfe) {
                }
            } else {

                try {
                    index = Integer.valueOf(
                            optionSelected[optionSelected.length - 1])
                        .intValue() - 1;
                } catch (NumberFormatException nfe) {
                }
            }

            // Set the selected mountpoint as Apache Mount Point in wizardmodel
            // If user selected a mountPoint that was created
            if (newlyCreated) {
                wizardModel.setWizardValue(
                    ApacheWizardConstants.STORAGE_RESOURCE_NAME, " ");
                wizardModel.setWizardValue(Util.APACHE_MOUNT,
                    fileSysMtpts[index]);
                wizardModel.setWizardValue(
                    ApacheWizardConstants.APACHE_FILESYSTEMS_SELECTED,
                    new String[] { fileSysMtpts[index] });
                ApacheWizardCreator.fillWizardModel_forNewMountPoint(
                    wizardModel, connection);

                return;
            } else {
                wizardModel.setWizardValue(
                    ApacheWizardConstants.STORAGE_RESOURCE_NAME,
                    storageResource);
                wizardModel.setWizardValue(Util.APACHE_MOUNT,
                    fileSysMtpts[index]);
                ApacheWizardCreator.fillWizardModel_forExistingMountPoint(
                    storageResource, wizardModel, connection);

                return;
            }
        } while (!flag);
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#isDisplayComplete
     */
    public boolean isDisplayComplete() {
        return true;
    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }
}
