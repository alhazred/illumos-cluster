/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)CentralServicesDetailsPanel.java 1.13     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.sapwebas;

// J2SE
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

// CMAS
import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.sapwebas.SapWebasMBean;
import com.sun.cluster.agent.dataservices.utils.DataServicesUtil;
import com.sun.cluster.agent.dataservices.utils.Util;

// CLI Wizard SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;

// Wizard Common
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;


public class CentralServicesDetailsPanel extends WizardLeaf {

    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N wizardi18n; // Message Bundle
    private String NAME = "name";
    private String VALUE = "value";
    private String KEY = "key";
    private String DESC = "desc";
    private String TYPE = "type";


    /**
     * Creates a new instance of CentralServicesDetailsPanel
     */
    public CentralServicesDetailsPanel() {
        super();
    }

    /**
     * Creates a CentralServicesDetailsPanel Panel with the 
     * given name and tree manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public CentralServicesDetailsPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    public CentralServicesDetailsPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;

        // Get the I18N instance
        this.wizardi18n = wizardi18n;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {
        String title = wizardi18n.getString(
                "sap.centralServicesDetailsPanel.title");
        String table_title = wizardi18n.getString(
                "sap.centralServicesDetailsPanel.table.title");
        String desc = wizardi18n.getString(
                "sap.centralServicesDetailsPanel.desc");
        String help_text = wizardi18n.getString(
                "sap.centralServicesDetailsPanel.help.cli");
        String yes = wizardi18n.getString("cliwizards.yes");
        String no = wizardi18n.getString("cliwizards.no");
        String continue_txt = wizardi18n.getString("cliwizards.continue");
        String back = wizardi18n.getString("ttydisplay.back.char");

        String propNameText = wizardi18n.getString("hasp.reviewpanel.propname");
        String propTypeText = wizardi18n.getString("hasp.reviewpanel.proptype");
        String propDescText = wizardi18n.getString("hasp.reviewpanel.propdesc");
        String currValText = wizardi18n.getString("hasp.reviewpanel.currval");
        String newValText = wizardi18n.getString("hasp.reviewpanel.newval");
        String option;
        String scs_i_no;
        String defScsNo;
        String query_txt;
        String scs_query_txt;
        String sArr[] = new String[1];
        HashMap custom = new HashMap();
        String menuKey = wizardi18n.getString("sap.sidPanel.custom.key");
        String menuText = wizardi18n.getString("sap.sidPanel.custom.keyText");
        custom.put(menuKey, menuText);

        ErrorValue retValue;


        DataServicesUtil.clearScreen();

        // Title
        TTYDisplay.clear(1);
        TTYDisplay.printTitle(title);
        TTYDisplay.pageText(desc);
        TTYDisplay.clear(2);

        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        HashMap customTag = new HashMap();
        customTag.put(wizardi18n.getString(
                "sap.centralServicesDetailsPanel.custom.key"),
            wizardi18n.getString(
                "sap.centralServicesDetailsPanel.custom.keyText"));

        HashMap tableEntries = new HashMap();
        int i = 0;
        String sapSid = (String) wizardModel.getWizardValue(Util.SAP_SID);
        TTYDisplay.setNavEnabled(true);

        Vector data = new Vector();

        String optionsArr[] = SapWebasWizardCreator.discoverOptions(wizardModel,
                Util.SCS_I_NO, SapWebasConstants.DISCOVERD_SCS_I_NO, sapSid);

        do { // Ask for SCS Instance number
            TTYDisplay.clear(1);
            TTYDisplay.setNavEnabled(true);
            defScsNo = (String) wizardModel.getWizardValue(
                    Util.SCS_INSTANCE_NUM);
            query_txt = wizardi18n.getString("sap.scs_i_no.select");
            scs_query_txt = wizardi18n.getString("sap.scs_i_no.query");

            if (optionsArr != null) {
                List selList = TTYDisplay.getScrollingOption(query_txt,
                        optionsArr, custom, defScsNo, help_text, true);

                if (selList == null) {
                    this.cancelDirection = true;

                    return;
                }

                TTYDisplay.clear(1);

                String selectList[] = (String[]) selList.toArray(new String[0]);
                scs_i_no = selectList[0];
            } else {
                scs_i_no = menuText;
            }

            if (scs_i_no.equals(menuText)) {
                TTYDisplay.setNavEnabled(false);

                if (defScsNo != null) {
                    scs_i_no = TTYDisplay.promptForEntry(scs_query_txt,
                            defScsNo, help_text);
                } else {
                    scs_i_no = TTYDisplay.promptForEntry(scs_query_txt,
                            help_text);
                }
            }

            while (scs_i_no.length() <= 0) {
                TTYDisplay.printError(wizardi18n.getString(
                        "sap.centralServicesInputPanel.scs.invalid"));
                scs_i_no = TTYDisplay.promptForEntry(scs_query_txt);
            }

            sArr[0] = scs_i_no;
            TTYDisplay.clear(1);
            TTYDisplay.setNavEnabled(false);
            retValue = new ErrorValue();
            retValue = SapWebasWizardCreator.validateData(Util.SCS_I_NO,
                    scs_i_no, wizardModel);

            if (retValue.getReturnValue().booleanValue()) {
                wizardModel.setWizardValue(Util.SCS_INSTANCE_NUM, scs_i_no);
            } else {
                TTYDisplay.printWarning(wizardi18n.getString(
                        "sap.scs_i_no.invalid"));
                TTYDisplay.printMoreText(retValue.getErrorString());
                TTYDisplay.clear(1);

                String confirm = TTYDisplay.getConfirmation(wizardi18n
                        .getString("cliwizards.validation.override"), no, yes);

                if (confirm.equalsIgnoreCase(yes)) {
                    wizardModel.selectCurrWizardContext();
                    wizardModel.setWizardValue(Util.SCS_INSTANCE_NUM, scs_i_no);

                    break;
                }

            }
        } while (!retValue.getReturnValue().booleanValue());

        optionsArr = SapWebasWizardCreator.discoverOptions(wizardModel,
                Util.SCS_NAME, SapWebasConstants.DISCOVERD_SCS_NAMES, sapSid);

        do { // Ask for SCS Instance number
            TTYDisplay.setNavEnabled(true);

            String defScsName = (String) wizardModel.getWizardValue(
                    Util.SCS_INSTANCE_NAME);
            query_txt = wizardi18n.getString("sap.scs_name.select");

            String selectedName;
            scs_query_txt = wizardi18n.getString("sap.scs_name.query");

            if (optionsArr != null) {
                List selList = TTYDisplay.getScrollingOption(query_txt,
                        optionsArr, custom, defScsName, help_text, true);

                if (selList == null) {
                    this.cancelDirection = true;

                    return;
                }

                TTYDisplay.clear(1);

                String selectList[] = (String[]) selList.toArray(new String[0]);
                selectedName = selectList[0];
            } else {
                selectedName = menuText;
            }

            if (selectedName.equals(menuText)) {
                TTYDisplay.setNavEnabled(false);

                if (defScsName != null) {
                    selectedName = TTYDisplay.promptForEntry(scs_query_txt,
                            defScsName, help_text);
                } else {
                    selectedName = TTYDisplay.promptForEntry(scs_query_txt,
                            help_text);
                }
            }

            while (selectedName.length() <= 0) {
                TTYDisplay.printError(wizardi18n.getString(
                        "sap.scs_name.invalid"));
                selectedName = TTYDisplay.promptForEntry(scs_query_txt);
            }

            sArr[0] = selectedName;
            TTYDisplay.clear(1);
            TTYDisplay.setNavEnabled(false);
            retValue = new ErrorValue();
            retValue = SapWebasWizardCreator.validateData(Util.SCS_NAME,
                    selectedName, wizardModel);

            if (retValue.getReturnValue().booleanValue()) {
                wizardModel.setWizardValue(Util.SCS_INSTANCE_NAME,
                    selectedName);
            } else {
                TTYDisplay.printWarning(wizardi18n.getString(
                        "sap.scs_name.invalid"));
                TTYDisplay.printMoreText(retValue.getErrorString());
                TTYDisplay.clear(1);

                String confirm = TTYDisplay.getConfirmation(wizardi18n
                        .getString("cliwizards.validation.override"), no, yes);

                if (confirm.equalsIgnoreCase(yes)) {
                    wizardModel.selectCurrWizardContext();
                    wizardModel.setWizardValue(Util.SCS_INSTANCE_NAME,
                        selectedName);

                    break;
                }
            }
        } while (!retValue.getReturnValue().booleanValue());
    }

}
