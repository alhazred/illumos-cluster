/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)CommandLineConsole.java 1.13     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.clisdk.core;

import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardI18N;

import java.lang.reflect.*;


/**
 * CommandLineConsole is the object that facilitates command line wizard
 * operation by simulating a user pressing the "next" button until the wizard
 * operation is complete.
 *
 * <p>The command line mode currently only works if the wizard requires no user
 * input other than to traverse through the wizard.
 */
public class CommandLineConsole implements AppConsole {

    /** boolean value indicating the status of the current thread. */
    protected static boolean exitTimeThread = false;

    /** The wizardModel object. */
    private CliDSConfigWizardModel wizardModel = null;

    /**
     * The manager responsible for creating and navigating the wizard client
     * tree.
     */
    private WizardTreeManager wizardManager = null;

    /** The wizard creator object. */
    private WizardCreator creator = null;

    /** The wizard I18n object. */
    private WizardI18N wizardi18n = null;

    /**
     * Creates a CommandLineConsole with the specified WizardCreator and
     * resource bundle object.
     */
    public CommandLineConsole(WizardCreator creator) {
        this.creator = creator;
        this.wizardModel = creator.getWizardModel();
        this.wizardi18n = creator.getWizardI18N();
        TTYDisplay.initialize();
    }

    /**
     * After initialization is complete, the panels in the client panel tree are
     * traversed by this method automatically.
     */
    public void start() {
        wizardManager = new WizardTreeManager(this, creator);
        setWizardTreeManager(wizardManager);

        Boolean ttyNavEnabled = (Boolean) wizardModel.getWizardValue(
                "wizard.ttyNavEnabled");

        if (ttyNavEnabled == null) {
            ttyNavEnabled = Boolean.FALSE;
        }

        TTYDisplay.setNavEnabled(ttyNavEnabled.booleanValue());

        String backOption = (String) wizardModel.getWizardValue(
                "wizard.ttyNavBackOption");
        String exitOption = (String) wizardModel.getWizardValue(
                "wizard.ttyNavExitOption");

        if (backOption != null) {
            TTYDisplay.setNavOption(TTYDisplay.BACK_OPTION, backOption);
        }

        if (exitOption != null) {
            TTYDisplay.setNavOption(TTYDisplay.EXIT_OPTION, exitOption);
        }
    }

    /**
     * Sets the wizardModel object into this console.
     *
     * @param  state  The new wizard model.
     */
    public void setWizardModel(CliDSConfigWizardModel model) {
        this.wizardModel = model;
    }

    /**
     * Returns the wizardModel object for this console.
     *
     * @return  The wizard model for this console.
     */
    public CliDSConfigWizardModel getWizardModel() {
        return this.wizardModel;
    }

    /**
     * Returns the wizardi18n object.
     *
     * @return  wizardi18n WizardI18N object.
     */
    public WizardI18N getWizardI18N() {
        return this.wizardi18n;
    }

    /**
     * Sets the wizard manager object.
     */
    public void setWizardTreeManager(WizardTreeManager wizardManager) {
        this.wizardManager = wizardManager;
        creator.setWizardTreeManager(wizardManager);
        TTYDisplay.setWizardTreeManager(wizardManager);
    }

    /**
     * Gets the wizard manager object.
     */
    public WizardTreeManager getWizardTreeManager() {
        return this.wizardManager;
    }

    /**
     * Display a query to the user.  This method is provided to fulfill the
     * AppConsole interface.  This method currently fails silently.
     *
     * @param  target  The object to call when a selection from the dialog is
     * made.
     * @param  title  The title of the query
     * @param  message  The message to be displayed to the user.
     * @param  buttons  The buttons (button labels) offered to the user to
     * select from.
     * @param  methods  A list of methods (in the same order as the list of
     * buttons) that will be called when a dialog selection is made.
     *
     * @return  boolean true if the dialog was closed via a button and false if
     * it was forcibly closed
     */
    public boolean displayQuery(Object target, String title, String message,
        String buttons[], String methods[]) {

        TTYDisplay.showNewline();

        if (buttons.length < 2) {

            TTYDisplay.showText(message);
            TTYDisplay.showNewline();
            TTYDisplay.queryValue("<" +
                wizardi18n.getString("ttydisplay.msg.cli.enter") + ">", "", 0);
            TTYDisplay.showNewline();

            if (methods != null) {
                String methodName = methods[0];

                if (methodName != null) {
                    callObjectMethod(target, methodName, null, null);
                }
            }

            return true;
        }

        String promptString = wizardi18n.getString("ttydisplay.cli.clc.prompt");
        String helpString = "";
        int val = TTYDisplay.queryValue(promptString, message, buttons, 0,
                helpString, 0);

        if (val == TTYDisplay.BACK_PRESSED)
            return false;

        String methodName = methods[val];

        if (methodName != null) {
            callObjectMethod(target, methodName, null, null);
        }

        return true;
    }

    /**
     * Display a query to the user.  This method is provided to fulfill the
     * AppConsole interface.  This method currently fails silently.
     *
     * @param  target  The object to call when a selection from the dialog is
     * made.
     * @param  title  The title of the query
     * @param  message  The message to be displayed to the user.
     * @param  buttons  The buttons (button labels) offered to the user to
     * select from.
     * @param  methods  A list of methods (in the same order as the list of
     * buttons) that will be called when a dialog selection is made.
     */
    public void displayQuery(Object target, int showTime, String title,
        String message, String buttons[], String methods[]) {
        String cval = wizardi18n.getString("ttydisplay.c.char").toLowerCase();
        String pval = wizardi18n.getString("ttydisplay.p.char").toLowerCase();

        TTYDisplay.showNewline();

        TTYDisplay ttyDisplay = new TTYDisplay(System.in, System.out, false);
        ttyDisplay.setTimeOut(showTime);

        String result = ttyDisplay.promptAndQuery(message, cval,
                new String[] { cval, pval }, "", 0);
        TTYDisplay.showNewline();

        if (result.equalsIgnoreCase(cval)) {
            callObjectMethod(target, methods[0], null, null);
        } else {
            callObjectMethod(target, methods[1], null, null);
        }
    }

    /**
     * Display a query to the user.  This method is provided to fulfill the
     * AppConsole interface.  This method currently fails silently.
     *
     * @param  target  The object to call when a selection from the dialog is
     * made.
     * @param  message  The message to be displayed to the user.
     * @param  buttons  The buttons (button labels) offered to the user to
     * select from.
     * @param  methods  A list of methods (in the same order as the list of
     * buttons) that will be called when a dialog selection is made.
     *
     * @return  boolean true if the dialog was closed via a button and false if
     * it was forcibly closed. Not applicable in tty mode
     */
    public boolean displayQuery(Object target, String message, String buttons[],
        String methods[]) {
        return displayQuery(target, " ", message, buttons, methods);
    }

    /**
     * Call an object method through reflection.
     *
     * @param  target  The object to call the method on.
     * @param  methodName  The name of the method to call.
     * @param  argTypes  The parameter list identifying the correct method.
     * @param  args  The parameters to pass to the method.
     *
     * @return  The method return value.
     */
    public Object callObjectMethod(Object target, String methodName,
        Class argTypes[], Object args[]) {

        try {
            Class targetClass = target.getClass();
            Method method = targetClass.getMethod(methodName, argTypes);

            return (method.invoke(target, args));
        } catch (NoSuchMethodException e) {
            System.out.println("Method " + methodName +
                " is not declared in class " + target.getClass().getName());
            e.printStackTrace();
        } catch (SecurityException e) {
            System.out.println("No access for method " + methodName +
                " in class " + target.getClass().getName());
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            System.out.println("No access for method " + methodName +
                " in class " + target.getClass().getName());
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            System.out.println("Bad argument passed to method " + methodName +
                " in class " + target.getClass().getName());
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            System.out.println("InvocationTargetException thrown in method " +
                methodName + " in class " + target.getClass().getName());
            e.getTargetException().printStackTrace();
        } catch (NullPointerException e) {
            System.out.println("Null object passed to callObjectMethod.");
            e.printStackTrace();
        }

        return (null);
    }
}
