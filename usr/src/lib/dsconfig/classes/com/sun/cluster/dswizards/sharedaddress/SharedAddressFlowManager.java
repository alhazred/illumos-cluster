/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)SharedAddressFlowManager.java 1.12     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.sharedaddress;

// J2SE
import java.util.ArrayList;
import java.util.List;

// Wizards Common
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.DSConfigWizardModel;
import com.sun.cluster.dswizards.common.DSWizardInterface;
import com.sun.cluster.dswizards.logicalhost.LogicalHostWizardConstants;
import com.sun.cluster.agent.dataservices.utils.Util;


/**
 * The class takes decision on which panel to show "next" depending upon the
 * conditions set at a particular context.
 */
public class SharedAddressFlowManager implements DSWizardInterface {

    /**
     * Creates a new instance of SharedAddressFlowManager
     */
    public SharedAddressFlowManager() {
    }

    /**
     * @see  com.sun.cluster.dswizards.common.DSWizardInterface#getNextPanel
     */
    public String getNextPanel(String curPanelName, Object wizCxtObj,
        String options[]) {
	DSConfigWizardModel guiModel = null;
	CliDSConfigWizardModel cliModel = null;

	if (wizCxtObj instanceof CliDSConfigWizardModel) {
	    cliModel = (CliDSConfigWizardModel)wizCxtObj;
	} else if (wizCxtObj instanceof DSConfigWizardModel) {
	    guiModel = (DSConfigWizardModel)wizCxtObj;
	}

        // Check if IPMPGroupPanel needs to be displayed
        if (curPanelName.equals(LogicalHostWizardConstants.HOSTNAMEENTRY)) {

            // Get the IPMP Groups Discovered from the Model
            String propertyNames[] = { Util.NETIFLIST };

            // Get the Discovered IPMPGroups From the wizardModel
            String ipmpGroupsNames[] = null;
	    if (guiModel != null) {
		ipmpGroupsNames= (String[])guiModel.getWizardValue(
		    Util.NETIFLIST);
	    } else {
		ipmpGroupsNames= (String[])cliModel.getWizardValue(
		    Util.NETIFLIST);
	    }

            // Parse the List and get the names of the IPMP Groups
            List groupList = new ArrayList();

            // Check whether there are one or more IPMPGroups
            if (ipmpGroupsNames != null) {

                for (int i = 0; i < ipmpGroupsNames.length; i++) {
                    String groupName = ipmpGroupsNames[i].substring(0,
                            ipmpGroupsNames[i].indexOf('@'));

                    if (!groupList.contains(groupName)) {
                        groupList.add(groupName);
                    }
                }

                // If there are more than one Groups store the GroupList in the
                // WizCtx and goto IPMPGroups Panel
                if (groupList.size() > 1) {
		    if (guiModel != null) {
			guiModel.setWizardValue(
			    LogicalHostWizardConstants.IPMPGROUPNAMES,
			    groupList);
		    } else {
			cliModel.setWizardValue(
			    LogicalHostWizardConstants.IPMPGROUPNAMES,
			    groupList);
		    }

                    return options[0];
                } else {
                    return options[1];
                }
            } else {
                return options[1];
            }
        }

        return null;
    }
}
