/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)MaskingThread.java 1.8     08/05/20 SMI"
 */

package com.sun.cluster.dswizards.clisdk.core;

import java.io.*;


/**
 * This class attempts to erase characters echoed to the console.
 */

public class MaskingThread extends Thread {

    private volatile boolean stop;
    private char echochar = ' ';
    private int interval = 1;


    /**
     * Print a blank space in the constructor.
     */
    public MaskingThread(char echochar, int interval) {
        this.echochar = echochar;
        this.interval = interval;
        System.out.print(" ");
    }


    /**
     * Begin masking until asked to stop.
     */
    public void run() {

        stop = true;

        while (stop) {
            System.out.print("\010" + echochar);

            try {

                // attempt masking at this rate
                Thread.currentThread().sleep(interval);
            } catch (InterruptedException iex) {
                Thread.currentThread().interrupt();

                return;
            }
        }
    }


    /**
     * Instruct the thread to stop masking.
     */
    public void stopMasking() {
        this.stop = false;
    }
}
