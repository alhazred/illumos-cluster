/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)HASPDeviceGroupSelectionPanel.java 1.19     08/12/10 SMI"
 */


package com.sun.cluster.dswizards.hastorageplus;

import com.sun.cluster.agent.dataservices.hastorageplus.HAStoragePlusMBean;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.ResourcePropertyUtil;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import javax.management.remote.JMXConnector;

public class HASPDeviceGroupSelectionPanel extends WizardLeaf {


    private String panelName; // Panel Name
    private WizardI18N wizardi18n; // Message Bundle
    private CliDSConfigWizardModel wizardModel; // WizardModel

    private JMXConnector localConnection = null;

    /**
     * Creates a new instance of HASPDeviceGroupSelectionPanel
     */
    public HASPDeviceGroupSelectionPanel() {
        super();
    }

    /**
     * Creates a HASPDeviceGroupSelectionPanel Panel with the 
     * given name and manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public HASPDeviceGroupSelectionPanel(String name,
        WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    public HASPDeviceGroupSelectionPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;
        this.wizardi18n = wizardi18n;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.
     *		WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        String back = wizardi18n.getString("ttydisplay.back.char");
        String yes = wizardi18n.getString("cliwizards.yes");
        String no = wizardi18n.getString("cliwizards.no");
        String title = wizardi18n.getString("hasp.rawdevice.title");
        String desc = wizardi18n.getString("hasp.rawdevice.desc");
        String emptytable_desc = wizardi18n.getString(
                "cliwizards.hasp.rawdevice.empty.desc");
        String schar = wizardi18n.getString("hasp.rawdevice.s.char");
        String showraw_txt = wizardi18n.getString(
                "hasp.rawdevice.menu.showraw");
        String hchar = wizardi18n.getString("hasp.rawdevice.h.char");
        String hideraw_txt = wizardi18n.getString(
                "hasp.rawdevice.menu.hideraw");
        String achar = wizardi18n.getString("ttydisplay.a.char");
        String all = wizardi18n.getString("ttydisplay.menu.all");
        String rchar = wizardi18n.getString("ttydisplay.r.char");
        String refresh = wizardi18n.getString("ttydisplay.menu.refresh");
        String help_text = wizardi18n.getString("hasp.rawdevice.cli.help");
        String heading1 = wizardi18n.getString(
                "hasp.rawdevice.devtable.colheader.0");
        String heading2 = wizardi18n.getString(
                "hasp.rawdevice.devtable.colheader.type");

        Object obj = wizardModel.getWizardValue(Util.HASP_ALL_DEVICES);
        HashMap hashMap = null;
        boolean refreshDeviceGroups = false;

        if ((obj == null) || !(obj instanceof HashMap)) {
            refreshDeviceGroups = true;
        } else {
            hashMap = (HashMap) obj;
        }

        // If nodelist was changed, refresh mountpoints
        String prevNodeList[] = (String[]) wizardModel.getWizardValue(
                HASPWizardConstants.PREV_RG_NODELIST);

        if (prevNodeList != null) {
            String curNodeList[] = (String[]) wizardModel.getWizardValue(
                    Util.RG_NODELIST);
            List curList = Arrays.asList(curNodeList);
            List prevList = Arrays.asList(prevNodeList);

            if (!((curList.size() == prevList.size()) &&
                        curList.containsAll(prevList))) {
                refreshDeviceGroups = true;
            }
        }

        boolean showrawdevices = false;
        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        boolean bcontinue = true;
        String subheadings[] = new String[] { heading1, heading2 };
        Vector data = new Vector();

        while (bcontinue) {

            // Title
            TTYDisplay.printTitle(title);

            if (refreshDeviceGroups) {
                resetRawDeviceWizardValue();
                hashMap = (HashMap) wizardModel.getWizardValue(
                        Util.HASP_ALL_DEVICES);
                refreshDeviceGroups = false;
                TTYDisplay.clear(2);
            }

            String options[] = null;

            // fetch the device groups
            String dgs[] = null;
            HashMap customTags = new HashMap();

            if (hashMap != null) {
                dgs = (String[]) hashMap.get(Util.DEVICE_GROUPS);
                options = dgs;

                if ((options != null) && (options.length > 0)) {

                    // There are existing device groups
                    if (!showrawdevices) {
                        customTags.put(schar, showraw_txt);
                    } else {
                        customTags.put(hchar, hideraw_txt);
                    }
                } else {

                    // if no device groups discovered, query for raw devices
                    // also
                    showrawdevices = true;
                }

                if (showrawdevices) {

                    // show device groups and raw devices
                    String rawdevices[] = (String[]) hashMap.get(
                            Util.RAW_DEVICES);
                    options = ResourcePropertyUtil.concatenateArrays(dgs,
                            rawdevices);
                }
            }

            if ((options == null) || (options.length == 0)) {

                // Display empty table description
                TTYDisplay.printSubTitle(emptytable_desc);
            } else {

                // Description Para
                TTYDisplay.printSubTitle(desc);
            }

            String devices[] = null;

            if (options.length > 0) {
                customTags.put(achar, all);
            }

            customTags.put(rchar, refresh);

            data = populateTable(options);

            String commaSeparatedSelections = "";
            String defSelections[] = (String[]) wizardModel.getWizardValue(
                    HASPWizardConstants.HASP_DEVICES_SELECTED);

            if (defSelections != null) {

                for (int i = 0; i < defSelections.length; i++) {

                    // When the deviceGroups are refreshed, some of
                    // the default selections might be invalid.
                    // So, add only valid default selections
                    if (data.contains(defSelections[i])) {
                        commaSeparatedSelections += defSelections[i];

                        if ((commaSeparatedSelections.length() > 0) &&
                                (i != defSelections.length)) {
                            commaSeparatedSelections += ",";
                        }
                    }
                }
            }

            List selections = TTYDisplay.getScrollingOptions("", subheadings,
                    data, customTags, commaSeparatedSelections, help_text,
                    true);

            if (selections == null) {
                this.cancelDirection = true;

                return;
            }

            if (selections.contains(showraw_txt)) {

                // user clicked on show raw. refresh the list
                showrawdevices = true;

                continue;
            } else if (selections.contains(hideraw_txt)) {

                // user clicked on hide raw
                showrawdevices = false;

                continue;
            }

            if (selections.contains(refresh)) {

                // refresh the devices list
                refreshDeviceGroups = true;

                continue;
            }

            // Get the elements in the list
            if (selections.contains(all)) {
                devices = options;
            } else {
                devices = (String[]) selections.toArray(new String[0]);
            }

            // Store the Values in the WizardModel
            wizardModel.setWizardValue(
                HASPWizardConstants.HASP_DEVICES_SELECTED, devices);
            bcontinue = false;
        }
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#isDisplayComplete
     */
    public boolean isDisplayComplete() {
        return true;
    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }

    private void resetRawDeviceWizardValue() {
        Thread t = null;
        TTYDisplay.clear(2);
        localConnection = getConnection(Util.getClusterEndpoint());

        HAStoragePlusMBean mbean = HASPWizardCreator.getHAStoragePlusMBean(
                localConnection);
        String rgNodeList[] = (String[]) wizardModel.getWizardValue(
                Util.RG_NODELIST);
        wizardModel.setWizardValue(HASPWizardConstants.PREV_RG_NODELIST,
            rgNodeList);

        String rgMode = (String) wizardModel.getWizardValue(Util.RG_MODE);

        HashMap helperData = new HashMap();
        helperData.put(Util.RG_NODELIST, rgNodeList);
        helperData.put(Util.RG_MODE, rgMode);
        t = TTYDisplay.busy(wizardi18n.getString(
                    "hasp.rawdevice.discover.busy"));
        t.start();

        HashMap hashMap = (HashMap) mbean.discoverPossibilities(
                new String[] { Util.HASP_ALL_DEVICES }, helperData);

        // discovery done, interrupt the thread
        try {
            t.interrupt();
            t.join();
        } catch (Exception e) {
        }

        if (hashMap != null) {
            wizardModel.setWizardValue(Util.HASP_ALL_DEVICES,
                hashMap.get(Util.HASP_ALL_DEVICES));
        }
    }

    private Vector populateTable(String options[]) {
        Vector table = new Vector();

        if (options != null) {

            for (int i = 0; i < options.length; i++) {
                String rs = (String) options[i];

                if (rs != null) {
                    String splitStr[] = rs.split(Util.COLON);
                    table.add(splitStr[0]);
                    table.add(splitStr[1]);
                }
            }
        }

        return table;
    }
}
