/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ORStorageInvoker.java 1.7     08/05/20 SMI"
 */

package com.sun.cluster.dswizards.oraclerac.storage;

// Java

// CMASS
import com.sun.cacao.agent.JmxClient;

import com.sun.cluster.agent.dataservices.oraclerac.OracleRACMBean;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.common.TrustedMBeanModel;
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardComposite;
import com.sun.cluster.dswizards.clisdk.core.WizardCreator;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.logicalhost.HostnameEntryPanel;
import com.sun.cluster.dswizards.logicalhost.IPMPGroupEntryPanel;
import com.sun.cluster.dswizards.logicalhost.LogicalHostWizardConstants;
import com.sun.cluster.dswizards.oraclerac.database.RacDBWizardConstants;
import com.sun.cluster.dswizards.oraclerac.framework.ORFWizardConstants;
import com.sun.cluster.dswizards.oraclerac.invoker.RACInvokerConstants;
import com.sun.cluster.dswizards.oraclerac.storage.ClusterDisksPanel;
import com.sun.cluster.dswizards.oraclerac.storage.ClusterFilesystemsPanel;
import com.sun.cluster.dswizards.oraclerac.storage.ORSResultsPanel;
import com.sun.cluster.dswizards.oraclerac.storage.ORSReviewPanel;
import com.sun.cluster.dswizards.oraclerac.storage.ORSSummaryPanel;
import com.sun.cluster.dswizards.oraclerac.storage.ORSWizardConstants;
import com.sun.cluster.dswizards.oraclerac.storage.ORSWizardIntroPanel;
import com.sun.cluster.dswizards.oraclerac.storage.RACDisksPanel;
import com.sun.cluster.dswizards.oraclerac.storage.RACFilesystemsPanel;
import com.sun.cluster.dswizards.oraclerac.storage.RACLocationPanel;

import java.io.IOException;

import java.text.MessageFormat;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.management.remote.JMXConnector;


/**
 * This class holds the Wizard Panel Tree information for all the three
 * RACWizards The Wizard lets the user invoke the three RAC Wizards
 */
public class ORStorageInvoker extends WizardCreator
    implements RacDBWizardConstants {

    private static final String FLOW_XML = ORSWizardConstants.ORS_FLOW_XML;
    private static final String STATE_FILE = RACInvokerConstants.RAC_STATE_FILE;

    /**
     * Default Constructor Creates ORS Wizard from the State File. The Wizard is
     * intialized to a particular state using the State file. The Wizard flow is
     * governed by the Flow XML.
     */
    public ORStorageInvoker() {
        super(STATE_FILE, FLOW_XML);
    }

    /**
     * Creates the actual Panel Tree Structure for the ORS Wizard
     */
    protected void createClientTree() {

        // Get the wizard root
        WizardComposite wizardRoot = getRoot();

        // RAC Storage Panels

        ORSWizardIntroPanel introPanel = new ORSWizardIntroPanel(
                ORSWizardConstants.INTROPANEL, wizardModel, wizardFlow,
                wizardi18n);
        RACLocationPanel locationPanel = new RACLocationPanel(
                ORSWizardConstants.LOCATIONPANEL, wizardModel, wizardFlow,
                wizardi18n);
        RACDisksPanel racDisksPanel = new RACDisksPanel(
                ORSWizardConstants.DISKPANEL, wizardModel, wizardFlow,
                wizardi18n);
        RACFilesystemsPanel racFSPanel = new RACFilesystemsPanel(
                ORSWizardConstants.FSPANEL, wizardModel, wizardFlow,
                wizardi18n);
        ClusterDisksPanel clusterDisksPanel = new ClusterDisksPanel(
                ORSWizardConstants.DISKPANEL + WizardFlow.HYPHEN +
                ORSWizardConstants.CLUSTERDISKPANEL, wizardModel, wizardFlow,
                wizardi18n);
        ClusterFilesystemsPanel clusterFSPanel = new ClusterFilesystemsPanel(
                ORSWizardConstants.FSPANEL + WizardFlow.HYPHEN +
                ORSWizardConstants.CLUSTERFSPANEL, wizardModel, wizardFlow,
                wizardi18n);

        ORSReviewPanel reviewPanel = new ORSReviewPanel(
                ORSWizardConstants.REVIEWPANEL, wizardModel, wizardFlow,
                wizardi18n);

        ORSSummaryPanel summaryPanel = new ORSSummaryPanel(
                ORSWizardConstants.SUMMARYPANEL, wizardModel, wizardFlow,
                wizardi18n);

        ORSResultsPanel resultsPanel = new ORSResultsPanel(
                ORSWizardConstants.RESULTSPANEL, wizardModel, wizardFlow,
                wizardi18n);

        // Each Panel is associated with a Name, the wizardModel from
        // the WizardCreator and the WizardFlow Structure.
        // Add the Panels to the Root

        // Storage Panels
        wizardRoot.addChild(introPanel);
        wizardRoot.addChild(locationPanel);
        wizardRoot.addChild(racDisksPanel);
        wizardRoot.addChild(clusterDisksPanel);
        wizardRoot.addChild(racFSPanel);
        wizardRoot.addChild(clusterFSPanel);
        wizardRoot.addChild(reviewPanel);
        wizardRoot.addChild(summaryPanel);
        wizardRoot.addChild(resultsPanel);
    }

    /**
     * Get the Handle to the RACBean MBean on a specified node
     *
     * @return  Handle to the RACBean MBean on a specified node
     */
    public static OracleRACMBean getOracleRACMBeanOnNode(
        JMXConnector connector) {

        // Only if there is no existing reference
        // Get Handler to OracleRACMBean
        OracleRACMBean mbeanOnNode = null;

        try {
            TTYDisplay.initialize();
            mbeanOnNode = (OracleRACMBean) TrustedMBeanModel.getMBeanProxy(
                    Util.DOMAIN, connector, OracleRACMBean.class, null, false);
        } catch (Exception e) {

            // Report Error that the user has to install
            // The Dataservice Package or upgrade it
            String errorMsg = pkgErrorString;
            MessageFormat msgFmt = new MessageFormat(errorMsg);
            errorMsg = msgFmt.format(
                    new String[] { RACInvokerConstants.RAC_PKG_NAME });
            TTYDisplay.printError(errorMsg);
            TTYDisplay.promptForEntry(wizardExitPrompt);
            System.exit(1);
        }

        return mbeanOnNode;
    }

    /**
     * Main function of the Wizards
     */
    public static void main(String args[]) {

        // Start the ORS Wizard
        ORStorageInvoker RACInovker = new ORStorageInvoker();
    }
}
