/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)NFSHaspResNameEntryPanel.java 1.19     08/12/10 SMI"
 */


package com.sun.cluster.dswizards.nfs;


// J2SE
import com.sun.cluster.agent.dataservices.hastorageplus.HAStoragePlusMBean;

// CMASS
import com.sun.cluster.common.TrustedMBeanModel;
import com.sun.cluster.agent.dataservices.nfs.NFSMBean;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;

// CLI Wizard SDK
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliStorageSelectionPanel;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.hastorageplus.HASPWizardCreator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.management.remote.JMXConnector;


/**
 * The Panel Class that gets a the mountpoints as input from the User.
 */
public class NFSHaspResNameEntryPanel extends CliStorageSelectionPanel {


    /**
     * Creates a new instance of HASPResNameEntryPanel
     */
    public NFSHaspResNameEntryPanel() {
        super();
    }

    /**
     * Creates a NFSHaspResNameEntryPanel Panel with the given name and tree
     * manager.
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public NFSHaspResNameEntryPanel(String name, WizardTreeManager manager) {
        super(name, manager);
    }

    /**
     * Creates a NFSHaspResNameEntryPanel with the given name and Wizard state
     */
    public NFSHaspResNameEntryPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(name, model, wizardFlow, wizardi18n);
    }


    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {

        desc = wizardi18n.getString("nfs.haspResNameEntry.desc");

        NFSMBean nfsMBean = null;
        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);
        JMXConnector connector = getConnection(nodeEndPoint);
        nfsMBean = NFSWizardCreator.getNFSMBeanOnNode(connector);

        // Initialize flags for displaying hasp wizard.
        wizardModel.setWizardValue(NFSWizardConstants.SHOWHASPWIZ, "false");
        wizardModel.setWizardValue(NFSWizardConstants.SHOWPATHPREFIX, "false");

        // set pathprefix from user selected RG
        wizardModel.setWizardValue(Util.RG_NAME,
            wizardModel.getWizardValue(
                Util.NFS_RESOURCE_GROUP + Util.NAME_TAG));

        HashMap resultMap = nfsMBean.discoverPossibilities(
                new String[] { Util.PATHPREFIX },
                wizardModel.getWizardValue(
                    Util.NFS_RESOURCE_GROUP + Util.NAME_TAG));

        if ((resultMap.get(Util.PATHPREFIX) == null) ||
                ((String) resultMap.get(Util.PATHPREFIX)).equals("")) {
            wizardModel.setWizardValue(NFSWizardConstants.SHOWPATHPREFIX,
                "true");
        } else {
            wizardModel.setWizardValue(Util.NFS_RESOURCE_GROUP +
                Util.RGPROP_TAG, resultMap);
        }

        super.consoleInteraction(false);

        HashMap haspWizSelection1 = new HashMap();
        HashMap haspWizSelection2 = new HashMap();
        List fileSysMtptsBuff = new ArrayList();
        List tmpFileSysMtptsBuff = new ArrayList();
        Object tmpObj = null;
        String create_new = null;
        Iterator iter = null;

        tmpObj = wizardModel.getWizardValue(Util.CREATE_NEW);

        if (tmpObj == null) {

            // clear all selected values
            wizardModel.setWizardValue(Util.NFS_HASP_RID + Util.NAME_TAG, null);
            wizardModel.setWizardValue(Util.NFS_HASP_MTPTS, null);
            wizardModel.setWizardValue(Util.NFS_HASPWIZSEL1, null);
            wizardModel.setWizardValue(Util.NFS_HASPWIZSEL2, null);
            wizardModel.setWizardValue(NFSWizardConstants.SHOWHASPWIZ, "false");

            return;
        } else {
            create_new = (String) tmpObj;
        }

        // Get mountpoints selected in previous invocation of this panel
        tmpObj = wizardModel.getWizardValue(Util.NFS_HASP_MTPTS);

        if (tmpObj != null) {
            fileSysMtptsBuff = (ArrayList) tmpObj;
        }

        // Retrieve wizardmodel values for previous invocation of this panel
        tmpObj = wizardModel.getWizardValue(Util.NFS_HASPWIZSEL1);

        if (tmpObj != null) {
            haspWizSelection1 = (HashMap) tmpObj;
        }

        tmpObj = wizardModel.getWizardValue(Util.NFS_HASPWIZSEL2);

        if (tmpObj != null) {
            haspWizSelection2 = (HashMap) tmpObj;
        }

        // Store filesystem mountpoints for to be created hasp resources
        tmpObj = wizardModel.getWizardValue(Util.SEL_FILESYSTEMS);

        if (tmpObj != null) {
            String HaspMtptsAr[] = (String[]) tmpObj;

            if (HaspMtptsAr.length != 0) {

                for (int ctr = 0; ctr < HaspMtptsAr.length; ctr++) {

                    if (!fileSysMtptsBuff.contains(HaspMtptsAr[ctr])) {
                        tmpFileSysMtptsBuff.add(HaspMtptsAr[ctr]);
                        fileSysMtptsBuff.add(HaspMtptsAr[ctr]);
                    }
                }

                if (!tmpFileSysMtptsBuff.isEmpty()) {
                    /*
                     * iterate through all mount points to see if 
                     * QFS RS is required 
                     */
                    HashMap fsMntMap = getFSMntMap();

                    ArrayList qfsFS = new ArrayList();
                    ArrayList haspFS = new ArrayList();
                    iter =  tmpFileSysMtptsBuff.iterator();
                    while (iter.hasNext()) {
                        String mtpt = (String) iter.next();
                        String fsType = (String)fsMntMap.get(mtpt);
                        if (fsType != null && fsType.indexOf(Util.FSTYPE_SAMFS)
                                != -1) {
                            qfsFS.add(mtpt);
                        } else {
                            haspFS.add(mtpt);
                        }
                    }
                    if (qfsFS.size() > 0) {
                        String qfsMtpts[] = 
                            (String[]) qfsFS.toArray( new String[] {});
                        String rsName = getStorageResName(qfsMtpts);
                        haspWizSelection1.put(rsName, qfsMtpts);
                    }
                    if (haspFS.size() > 0) {
                        String haspMtpts[] = 
                            (String[]) haspFS.toArray( new String[] {});
                        String rsName = getStorageResName(haspMtpts);
                        haspWizSelection1.put(rsName, haspMtpts);
                        haspWizSelection2.put(rsName,
                            (HashMap) wizardModel.getWizardValue(
                                Util.HASP_ALL_FILESYSTEMS));
                    }

                    wizardModel.setWizardValue(Util.NFS_HASPWIZSEL1,
                        haspWizSelection1);
                    wizardModel.setWizardValue(Util.NFS_HASPWIZSEL2,
                        haspWizSelection2);
                }
            }
        }

        // Store selected hasp resources
        Vector selRsrcs = (Vector) wizardModel.getWizardValue(Util.SEL_RS);
        ArrayList haspRsrcBuff = new ArrayList();
        iter = selRsrcs.iterator();

        while (iter.hasNext()) {
            String tmp = (String) iter.next();

            if (!haspRsrcBuff.contains(tmp)) {
                haspRsrcBuff.add(tmp);
            }
        }

        wizardModel.setWizardValue(Util.NFS_HASP_RID + Util.NAME_TAG,
            haspRsrcBuff);

        // Store filesystem mountpoints for already present hasp resources
        tmpFileSysMtptsBuff = (ArrayList) wizardModel.getWizardValue(FS_MTPTS);
        iter = tmpFileSysMtptsBuff.iterator();

        while (iter.hasNext()) {
            String tmp = (String) iter.next();

            if (!fileSysMtptsBuff.contains(tmp)) {
                fileSysMtptsBuff.add(tmp);
            }
        }

        wizardModel.setWizardValue(Util.NFS_HASP_MTPTS, fileSysMtptsBuff);

        if (create_new.equals(CREATE_NEW)) {

            // Jump to Hasp wizard
            wizardModel.setWizardValue(NFSWizardConstants.SHOWHASPWIZ, "true");
        }
    }

    // override refreshRGList for NFS specific case
    protected void refreshRGList() {
        String rgName = (String) wizardModel.getWizardValue(Util.RG_NAME);

        if (rgName != null) {
            super.refreshRGList();
        } else {
            wizardModel.setWizardValue(Util.STORAGE_RGLIST, null);
        }
    }

    private String getStorageResName(String mountpoints[]) {

        String name = null;

        try {
            String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);
            JMXConnector connector = getConnection(nodeEndPoint);
            HAStoragePlusMBean mBean =
                (HAStoragePlusMBean) TrustedMBeanModel.getMBeanProxy(
                    Util.DOMAIN, connector, HAStoragePlusMBean.class,
                    null, false);

            String properties[] = { Util.RS_NAME };
            HashMap helperData = new HashMap();
            helperData.put(Util.HASP_ALL_FILESYSTEMS, mountpoints);

            HashMap discoveredMap = mBean.discoverPossibilities(properties,
                helperData);
            name = (String) discoveredMap.get(Util.RS_NAME);

        } catch (Exception e) {
            e.printStackTrace();
        }

        if ((name == null || name.equals("")) && 
                mountpoints != null) {
            name = mountpoints[0];
        }

        return name;
    }

    /* 
     * Get HaspMap which contains the available entries(FS and type 
     * in vfstab. 
     */
    public HashMap getFSMntMap() {

        HashMap fsMntMap = new HashMap(); // null is never returned

        try {
            String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);
            JMXConnector connector = getConnection(nodeEndPoint);
            HAStoragePlusMBean mBean =
                (HAStoragePlusMBean) TrustedMBeanModel.getMBeanProxy(
                    Util.DOMAIN, connector, HAStoragePlusMBean.class,
                    null, false);

            // retrieve mountpoint fstype and overwrite fsMntMap 
            fsMntMap = mBean.obtainMountpointFSTypes(
                (String[])wizardModel.getWizardValue(Util.RG_NODELIST));
                        
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fsMntMap;
    }
}

