/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)HAOracleWizardCreator.java 1.14     08/05/20 SMI"
 */

package com.sun.cluster.dswizards.haoracle;

import com.sun.cacao.agent.JmxClient;

import com.sun.cluster.agent.dataservices.haoracle.HAOracleMBean;
import com.sun.cluster.agent.dataservices.utils.*;
import com.sun.cluster.common.TrustedMBeanModel;
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardComposite;
import com.sun.cluster.dswizards.clisdk.core.WizardCreator;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.hastorageplus.HASPCreateFileSystemPanel;
import com.sun.cluster.dswizards.hastorageplus.HASPCreateFileSystemReviewPanel;
import com.sun.cluster.dswizards.hastorageplus.HASPDeviceGroupSelectionPanel;
import com.sun.cluster.dswizards.hastorageplus.HASPFileSystemSelectionPanel;
import com.sun.cluster.dswizards.hastorageplus.HASPLocationPanel;
import com.sun.cluster.dswizards.hastorageplus.HASPWizardConstants;
import com.sun.cluster.dswizards.logicalhost.HostnameEntryPanel;
import com.sun.cluster.dswizards.logicalhost.IPMPGroupEntryPanel;
import com.sun.cluster.dswizards.logicalhost.LogicalHostWizardConstants;

import java.text.MessageFormat;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.management.remote.JMXConnector;


/**
 * This class holds the Wizard Panel Tree information for HAOracle Wizard. This
 * class creates the HAOracle wizard from the State file and WizardFlow File.
 */
public class HAOracleWizardCreator extends WizardCreator {


    private static final String FLOW_XML =
        "/usr/cluster/lib/ds/haoracle/HAOracleWizardFlow.xml";
    private static final String STATE_FILE =
        "/opt/cluster/lib/ds/history/HAOracleStateFile";

    /**
     * Default Constructor Creates  HAOracle Wizard from the State File. The
     * Wizard is intialized to a particular state using the State file. The
     * Wizard flow is governed by the Flow XML.
     */
    public HAOracleWizardCreator() {
        super(STATE_FILE, FLOW_XML);
    }

    /**
     * Creates the actual Panel Tree Structure for the HAOracle Wizard
     */
    protected void createClientTree() {

        // Get the wizard root
        WizardComposite haoracleWizardRoot = getRoot();

        // Create the Individual Panels of the Wizard

        // Each Panel is associated with a Name, the wizardModel from
        // the WizardCreator and the WizardFlow Structure.
        HAOracleIntroPanel haoracleIntroPanel = new HAOracleIntroPanel(
                HAOracleWizardConstants.PANEL_0, wizardModel, wizardFlow,
                wizardi18n);

        HAOracleNodeSelectionPanel haoracleNodeSelectionPanel =
            new HAOracleNodeSelectionPanel(HAOracleWizardConstants.PANEL_1,
                wizardModel, wizardFlow, wizardi18n);

        HAOraclePreferencePanel haoraclePreferencePanel =
            new HAOraclePreferencePanel(HAOracleWizardConstants.PANEL_2,
                wizardModel, wizardFlow, wizardi18n);

        HAOracleHomeSelectionPanel haoracleHomeSelectionPanel =
            new HAOracleHomeSelectionPanel(HAOracleWizardConstants.PANEL_3,
                wizardModel, wizardFlow, wizardi18n);

        HAOracleSidSelectionPanel haoracleSidSelectionPanel =
            new HAOracleSidSelectionPanel(HAOracleWizardConstants.PANEL_4,
                wizardModel, wizardFlow, wizardi18n);

        HAOraclePropsReviewPanel haoraclePropsReviewPanel =
            new HAOraclePropsReviewPanel(HAOracleWizardConstants.PANEL_5,
                wizardModel, wizardFlow, wizardi18n);

        HAOracleStorageSelectionPanel haoracleStorageSelectionPanel =
            new HAOracleStorageSelectionPanel(HAOracleWizardConstants.PANEL_6,
                wizardModel, wizardFlow, wizardi18n);

        HASPLocationPanel haspLocationPanel = new HASPLocationPanel(
                HAOracleWizardConstants.PANEL_6 + WizardFlow.HYPHEN +
                HASPWizardConstants.PANEL_2, wizardModel, wizardFlow,
                wizardi18n);

        HASPFileSystemSelectionPanel haspFileSystemSelectionPanel =
            new HASPFileSystemSelectionPanel(HAOracleWizardConstants.PANEL_6 +
                WizardFlow.HYPHEN + HASPWizardConstants.PANEL_3, wizardModel,
                wizardFlow, wizardi18n);

        HASPCreateFileSystemPanel haspCreateFileSystemPanel =
            new HASPCreateFileSystemPanel(HAOracleWizardConstants.PANEL_6 +
                WizardFlow.HYPHEN + HASPWizardConstants.PANEL_4, wizardModel,
                wizardFlow, wizardi18n);

        HASPCreateFileSystemReviewPanel haspCreateFileSystemReviewPanel =
            new HASPCreateFileSystemReviewPanel(
                HAOracleWizardConstants.PANEL_6 + WizardFlow.HYPHEN +
                HASPWizardConstants.PANEL_5, wizardModel, wizardFlow,
                wizardi18n);

        HASPDeviceGroupSelectionPanel haspDeviceGroupSelectionPanel =
            new HASPDeviceGroupSelectionPanel(HAOracleWizardConstants.PANEL_6 +
                WizardFlow.HYPHEN + HASPWizardConstants.PANEL_6, wizardModel,
                wizardFlow, wizardi18n);

        HAOracleLogicalHostSelectionPanel haoracleLogicalHostSelectionPanel =
            new HAOracleLogicalHostSelectionPanel(
                HAOracleWizardConstants.PANEL_7, wizardModel, wizardFlow,
                wizardi18n);

        HostnameEntryPanel hostnameEntryPanel = new HostnameEntryPanel(
                HAOracleWizardConstants.PANEL_7 + WizardFlow.HYPHEN +
                LogicalHostWizardConstants.HOSTNAMEENTRY, wizardModel,
                wizardFlow, wizardi18n);

        IPMPGroupEntryPanel ipmpGroupEntryPanel = new IPMPGroupEntryPanel(
                HAOracleWizardConstants.PANEL_7 + WizardFlow.HYPHEN +
                LogicalHostWizardConstants.IPMPGROUPENTRY, wizardModel,
                wizardFlow, wizardi18n);

        HAOracleReviewPanel haoracleReviewPanel = new HAOracleReviewPanel(
                HAOracleWizardConstants.PANEL_8, wizardModel, wizardFlow,
                wizardi18n);

        HAOracleSummaryPanel haoracleSummaryPanel = new HAOracleSummaryPanel(
                HAOracleWizardConstants.PANEL_9, wizardModel, wizardFlow,
                wizardi18n);

        HAOracleResultPanel haoracleResultPanel = new HAOracleResultPanel(
                HAOracleWizardConstants.PANEL_10, wizardModel, wizardFlow,
                wizardi18n);

        // Add the Panels to the Root
        haoracleWizardRoot.addChild(haoracleIntroPanel);
        haoracleWizardRoot.addChild(haoracleNodeSelectionPanel);
        haoracleWizardRoot.addChild(haoraclePreferencePanel);
        haoracleWizardRoot.addChild(haoracleHomeSelectionPanel);
        haoracleWizardRoot.addChild(haoracleSidSelectionPanel);
        haoracleWizardRoot.addChild(haoraclePropsReviewPanel);
        haoracleWizardRoot.addChild(haoracleStorageSelectionPanel);
        haoracleWizardRoot.addChild(haspLocationPanel);
        haoracleWizardRoot.addChild(haspFileSystemSelectionPanel);
        haoracleWizardRoot.addChild(haspCreateFileSystemPanel);
        haoracleWizardRoot.addChild(haspCreateFileSystemReviewPanel);
        haoracleWizardRoot.addChild(haspDeviceGroupSelectionPanel);
        haoracleWizardRoot.addChild(haoracleLogicalHostSelectionPanel);
        haoracleWizardRoot.addChild(hostnameEntryPanel);
        haoracleWizardRoot.addChild(ipmpGroupEntryPanel);
        haoracleWizardRoot.addChild(haoracleReviewPanel);
        haoracleWizardRoot.addChild(haoracleSummaryPanel);
        haoracleWizardRoot.addChild(haoracleResultPanel);
    }

    /**
     * Get the Handle to the HAOracle MBean on a specified node
     *
     * @return  Handle to the HAOracle MBean on a specified node
     */
    public static HAOracleMBean getHAOracleMBeanOnNode(JMXConnector connector) {

        // Only if there is no existing reference
        // Get Handler to HAOracleMBean
        HAOracleMBean mbeanOnNode = null;

        try {
            TTYDisplay.initialize();
            mbeanOnNode = (HAOracleMBean) TrustedMBeanModel.getMBeanProxy(
                    Util.DOMAIN, connector, HAOracleMBean.class, null, false);
        } catch (Exception e) {

            // Report Error that the user has to install
            // The Dataservice Package or upgrade it
            String errorMsg = pkgErrorString;
            MessageFormat msgFmt = new MessageFormat(errorMsg);
            errorMsg = msgFmt.format(
                    new String[] { HAOracleWizardConstants.ORACLE_PKG_NAME });
            TTYDisplay.printError(errorMsg);
            TTYDisplay.promptForEntry(wizardExitPrompt);
            System.exit(1);
        }

        return mbeanOnNode;
    }

    /**
     * Main function of the Wizards
     */
    public static void main(String args[]) {

        // Start the HAOracle Wizard
        HAOracleWizardCreator haoracle = new HAOracleWizardCreator();
    }
}
