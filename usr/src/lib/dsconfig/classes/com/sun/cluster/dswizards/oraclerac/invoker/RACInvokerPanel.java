/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)RACInvokerPanel.java	1.21	09/03/11 SMI"
 */

package com.sun.cluster.dswizards.oraclerac.invoker;

import com.sun.cluster.agent.dataservices.oraclerac.OracleRACMBean;
import com.sun.cluster.agent.dataservices.utils.InfrastructureMBean;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.zonecluster.ZoneClusterData;
import com.sun.cluster.common.TrustedMBeanModel;

import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliUtil;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.common.WizardStateManager;
import com.sun.cluster.dswizards.oraclerac.database.RacDBBasePanel;
import com.sun.cluster.dswizards.oraclerac.storage.ORSWizardConstants;

import com.sun.cluster.model.DataModel;
import com.sun.cluster.model.ZoneClusterModel;
import com.sun.cluster.model.RACModel;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.Iterator;

import javax.management.remote.JMXConnector;

public class RACInvokerPanel extends RacDBBasePanel {

    protected WizardI18N messages; // Message Bundle

    protected JMXConnector localConnection = null; // Reference to JMXConnector

    /**
     * Creates a new instance of RACInvokerPanel
     */
    public RACInvokerPanel() {
        super();
    }

    /**
     * Creates a RACInvokerPanel Panel with the given name and associates it
     * with the WizardModel, WizardFlow and i18n reference
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     * @param  messages  WizardI18N
     */
    public RACInvokerPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N messages) {
        super(name, model, wizardFlow, messages);

        // Get the I18N instance
        this.messages = messages;
        localConnection = getConnection(Util.getClusterEndpoint());
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {

	boolean showFrameworkWiz = false;
	boolean showStorageWiz = false;
	boolean showDatabaseWiz = false;
	 
	//Prompt the user for the 3 options
	//Framework, Storage or Database
	title = wizardi18n.getString("racwizard.invoker.title");
	desc = wizardi18n.getString("racwizard.invoker.desc.gui");
	String preference = wizardi18n.getString("racwizard.invoker.query");
	String option1 = wizardi18n.getString("racwizard.invoker.frameworkWizard");
	String option2 = wizardi18n.getString("racwizard.invoker.storageWizard");
	String option3 = wizardi18n.getString("racwizard.invoker.databaseWizard");

	int defSelection;
	 
	TTYDisplay.setNavEnabled(true);
	this.cancelDirection = false;
	 
	String[] options = { option1, option2, option3 };

	String pref = (String) wizardModel.getWizardValue(RACInvokerConstants.WIZ_TYPE);
	try {
	    defSelection = Integer.parseInt(pref);
	} catch (Exception e) {
	    defSelection = Integer.parseInt(RACInvokerConstants.FRAMEWORK);
	}

	HashMap helperData = null;
	String selectedZoneCluster = CliUtil.getZoneClusterName(wizardModel);

	if (selectedZoneCluster != null &&
	    selectedZoneCluster.trim().length() > 0) {
	    helperData = new HashMap();
	    helperData.put(Util.SEL_ZONE_CLUSTER, selectedZoneCluster);
	}

	RACModel racModel = RACInvokerCreator.getRACModel();
        // discover default properties
        Map racInfoMap = null;
	if (racModel != null) {
	    racInfoMap = racModel.discoverPossibilities(
		null, Util.getClusterEndpoint(),
                new String[] { Util.RAC_RG_NODES }, helperData);
	}

        // Discover the Existing RACFramework Structure
        String racNodeList[] = null;

        if (racInfoMap != null) {
            racNodeList = (String[]) racInfoMap.get(Util.RAC_RG_NODES);
        }

	if (racNodeList != null && racNodeList.length > 0 ) {
	    //RAC framework is already configured. Set default option to STORAGE
	    defSelection = Integer.parseInt(RACInvokerConstants.STORAGE);
	} 

	boolean bCorrectOption = false;
	while (!bCorrectOption) {
	    //Title 
	    TTYDisplay.printTitle(title);

	    int index = TTYDisplay.getMenuOption(
		preference,
		options,
		defSelection);

	    if (index == TTYDisplay.BACK_PRESSED) {
		this.cancelDirection = true;
		return;
	    }

	    String selection = Integer.toString(index);
	    if (selection.equals(RACInvokerConstants.FRAMEWORK)) {
		if (racNodeList == null || racNodeList.length <= 0) {
		    bCorrectOption = true;
		} else {
		    //User selected framework and its already created.
		    //Display error
		    TTYDisplay.printError(
			messages.getString("racwizard.invoker.desc"));
		    TTYDisplay.promptForEntry(
			messages.getString("cliwizards.continueExit"));
		    bCorrectOption = false;
		}
	    } else { //user selected storage or database
		if (racNodeList == null || racNodeList.length <= 0) {
		    //User selected storage or database when framework
		    //does not exist
		    TTYDisplay.printError(messages.getString(
			"racwizard.invoker.framework.unavailable"));
		    TTYDisplay.promptForEntry(messages.getString(
			"cliwizards.continueExit"));
		   
		    bCorrectOption = false;
		} else {
		    bCorrectOption = true;
		}
	    }
	    if (bCorrectOption) {
		wizardModel.setWizardValue(RACInvokerConstants.WIZ_TYPE,
		    selection);
	    }
	}

	pref =  (String) wizardModel.getWizardValue(RACInvokerConstants.WIZ_TYPE);
	if (pref.equals(RACInvokerConstants.FRAMEWORK)) {
	    showFrameworkWiz = true;
	} else if (pref.equals(RACInvokerConstants.STORAGE) ||
	    pref.equals(RACInvokerConstants.DATABASE)) {
	    String[] baseClusterNodeList = null; 
	    ArrayList baseClusterNodeArray = null;

	    ZoneClusterData zcData = null;
	    HashMap hostNodeMap = null;

	    if (selectedZoneCluster != null &&
		selectedZoneCluster.trim().length() > 0) {
		//for a zone cluster, the RAC nodelist corresponds to the
		//public hostnames of the zones.
		DataModel dm = DataModel.getDataModel(null);
		try {
		    zcData = dm.getZoneClusterModel().getZoneClusterData(null,
			selectedZoneCluster);
		} catch (Exception e) {
		}

		if (zcData != null) {
		    hostNodeMap = zcData.getHostNodeMap();
		}

		if (hostNodeMap != null) {
		    for (int i = 0; i < racNodeList.length; i++) {
			String racNode = racNodeList[i];
			boolean found = false;
			Set keySet = hostNodeMap.keySet();
			Iterator iterator = keySet.iterator();

			while (iterator.hasNext() && !found) {
			    String hostName = (String)iterator.next();
			    if (racNode.equals(hostName)) {
				found = true;
				if (baseClusterNodeArray == null) {
				    baseClusterNodeArray = new ArrayList();
				}
				baseClusterNodeArray.add((String) hostNodeMap.get(hostName));
			    }
			}
		    }
		}

		if (baseClusterNodeArray != null) {
		    baseClusterNodeList = (String [])
			baseClusterNodeArray.toArray(new String[0]);
		}
	    } else {
		baseClusterNodeList = racNodeList;
	    }
	    wizardModel.setWizardValue(Util.BASE_CLUSTER_NODELIST, baseClusterNodeList);

	    // Check if the required MBean is available on the nodes
	    // selected. If not, error out with a proper error message
	    String error = checkMBeanOnNodes(baseClusterNodeList, OracleRACMBean.class,
                messages.getString("dswizards.oraclerac.wizard"));

	    if (error != null) {
		TTYDisplay.printError(error);
		TTYDisplay.promptForEntry(messages.getString(
			"cliwizards.continueExit"));
		System.exit(1);
	    }

	    // check if the rac nodelist includes the local nodes
	    // if yes set NODE_TO_CONNECT as the localnode
	    boolean localNodeInNodeList = false;

	    try {
		InetAddress currNodeAddress = Inet4Address.getLocalHost();
		String localNode = currNodeAddress.getHostName();

		for (int i = 0; i < baseClusterNodeList.length; i++) {
		    String node_i = baseClusterNodeList[i];

		    if (node_i.equalsIgnoreCase(localNode)) {
			wizardModel.setWizardValue(Util.NODE_TO_CONNECT, localNode);
			localNodeInNodeList = true;

			break;
		    }
		}
	    } catch (Exception e) {

	    }

	    if (!localNodeInNodeList) {
		wizardModel.setWizardValue(Util.NODE_TO_CONNECT, baseClusterNodeList[0]);
	    }

	    // Retrieve the Storage Schemes selected by the user
	    // from the Wizard StateFile, and init the
	    // wizardModel

	    String keyPrefix = (String) wizardModel.getWizardValue(
		Util.KEY_PREFIX);

	    List storageScheme = (ArrayList) wizardModel.getWizardValue(
		keyPrefix + Util.ORF_STORAGE_MGMT_SCHEMES);

	    // If the state file is not present
	    if (storageScheme == null) {
		wizardModel.setWizardValue(Util.RAC_STATE_FILE_ABSENT,
		    Boolean.TRUE);
		// The possible storage schemes are asked again
		// in the location panel of the storage wizard
	    } else {
		wizardModel.setWizardValue(Util.RAC_STATE_FILE_ABSENT,
		    Boolean.FALSE);
	    }

	    // Process the user option
	    if (pref.equals(RACInvokerConstants.STORAGE)) {

		// initialise values as required by rac storage wizard
		wizardModel.setWizardValue(Util.RG_NODELIST, racNodeList);
		wizardModel.setWizardValue(Util.ORF_RESOURCE_GROUP + Util.NAME_TAG,
		    racInfoMap.get(Util.RAC_FRAMEWORK_RGNAME));
		wizardModel.setWizardValue(Util.ORF_RID + Util.NAME_TAG,
		    racInfoMap.get(Util.RAC_FRAMEWORK_RSNAME));
		wizardModel.setWizardValue(Util.ORF_SVM_RID + Util.NAME_TAG,
		    racInfoMap.get(Util.ORF_SVM_RID));
		wizardModel.setWizardValue(Util.ORF_VXVM_RID + Util.NAME_TAG,
		    racInfoMap.get(Util.ORF_VXVM_RID));
		wizardModel.setWizardValue(Util.ORF_UDLM_RID + Util.NAME_TAG,
		    racInfoMap.get(Util.ORF_UDLM_RID));

    // START --
    // The code in this block is repeated in the RACLocationPanel.java
    // Find a way to making code a function call, that can be called
    // from both the place. The code is repeated here to avoid
    // changes to many file and thus increasing the risk in the show-stopper
    // builds of SC3.2
		// Show the Devices configured for RAC if user had selected
		// SVM or VXVM
		if (storageScheme != null) {

		    if (storageScheme.contains(Util.SVM) ||
			storageScheme.contains(Util.VXVM)) {
			wizardModel.setWizardValue(
			    ORSWizardConstants.SHOW_RAC_DEVICES, "true");
		    }

		    // Show Filesystems configured for RAC if user had selected
		    // NAS or QFS
		    if (storageScheme.contains(Util.QFS_HWRAID) ||
			storageScheme.contains(Util.NAS) ||
			storageScheme.contains(Util.SUN_NAS)) {
			wizardModel.setWizardValue(ORSWizardConstants.SHOW_RAC_FS,
			    "true");
		    }

		    if (storageScheme.contains(Util.QFS_SVM)) {
			wizardModel.setWizardValue(ORSWizardConstants.SHOW_RAC_FS,
			    "true");
			wizardModel.setWizardValue(
			    ORSWizardConstants.SHOW_RAC_DEVICES, "true");
		    }

		    if ((storageScheme.size() == 1) &&
			    storageScheme.contains(Util.HWRAID)) {

			// user has selected Hardware RAID without VM option
			String hwraid_desc = messages.getString(
				"racstorage.racLocationPanel.hwraid.desc");
			TTYDisplay.clear(2);
			TTYDisplay.showText(hwraid_desc, 4);
			TTYDisplay.clear(2);
			TTYDisplay.promptForEntry(messages.getString(
				"cliwizards.continueExit"));
			System.exit(0);
		    }
		}
    // END --
		showStorageWiz = true;
	    } else if (pref.equals(RACInvokerConstants.DATABASE)) {
		wizardModel.setWizardValue(Util.RG_NODELIST, racNodeList);
		wizardModel.setWizardValue(Util.RAC_FRAMEWORK_RSNAME,
		    racInfoMap.get(Util.RAC_FRAMEWORK_RSNAME));

		wizardModel.setWizardValue(Util.RAC_FRAMEWORK_RGNAME,
		    racInfoMap.get(Util.RAC_FRAMEWORK_RGNAME));

		String version = (String) wizardModel.getWizardValue(Util.RAC_VER);
		if (version == null) { 
		    wizardModel.setWizardValue(Util.RAC_VER, new String(Util.NINE));
		}

		// DB Wizards Initializes the wizardModel in its
		// IntroPanel
		showDatabaseWiz = true;
	    }
	}

	wizardModel.setWizardValue(RACInvokerConstants.FRAMEWORK_WIZ,
	    new Boolean(showFrameworkWiz));

	wizardModel.setWizardValue(RACInvokerConstants.STORAGE_WIZ,
	    new Boolean(showStorageWiz));

	wizardModel.setWizardValue(RACInvokerConstants.DATABASE_WIZ,
	    new Boolean(showDatabaseWiz));
    }

    protected InfrastructureMBean getInfrastructureMBean() {
        JMXConnector localConnection = getConnection(Util.getClusterEndpoint());
        InfrastructureMBean mbean = null;

        try {
            mbean = (InfrastructureMBean) TrustedMBeanModel.getMBeanProxy(
                    Util.DOMAIN, localConnection, InfrastructureMBean.class,
                    null, false);
        } catch (Exception e) {
            String wizardExitPrompt = messages.getString(
                    "cliwizards.continueExit");
            String errorMsg = messages.getString("dswizards.cacao.down",
                    new Object[] { Util.getClusterEndpoint() });
            TTYDisplay.printError(errorMsg);
            TTYDisplay.promptForEntry(wizardExitPrompt);
            System.exit(1);
        }

        return mbean;
    }

    private String checkMBeanOnNodes(String nodeList[], Class mBeanToCheck,
        String packageName) {
        JMXConnector localConnection = getConnection(Util.getClusterEndpoint());

        try {
            InfrastructureMBean mBean = getInfrastructureMBean();
            String errorNodes[] = mBean.checkCacaoagent(nodeList);

            if ((errorNodes != null) && (errorNodes.length > 0)) {
                StringBuffer errorNodesString = new StringBuffer();

                for (int i = 0; i < errorNodes.length; i++) {
                    errorNodesString.append(errorNodes[i]);

                    if (i < (errorNodes.length - 1)) {
                        errorNodesString.append(",");
                    }
                }

                String error = messages.getString(
                        "dswizards.cacao.communicationFailure",
                        new String[] { errorNodesString.toString() });

                return error;
            }

            errorNodes = mBean.checkMBeans(nodeList, mBeanToCheck);

            if ((errorNodes != null) && (errorNodes.length > 0)) {
                StringBuffer errorNodesString = new StringBuffer();

                for (int i = 0; i < errorNodes.length; i++) {
                    errorNodesString.append(errorNodes[i]);

                    if (i < (errorNodes.length - 1)) {
                        errorNodesString.append(",");
                    }
                }

                String error = messages.getString("dswizards.error.pkg",
                        new Object[] {
                            packageName, errorNodesString.toString()
                        });

                return error;
            }
        } catch (Exception e) {
            String wizardExitPrompt = messages.getString(
                    "cliwizards.continueExit");
            String errorMsg = messages.getString("dswizards.cacao.down",
                    new Object[] { Util.getClusterEndpoint() });
            TTYDisplay.printError(errorMsg);
            TTYDisplay.promptForEntry(wizardExitPrompt);
            System.exit(1);
        }

        return null;
    }

}
