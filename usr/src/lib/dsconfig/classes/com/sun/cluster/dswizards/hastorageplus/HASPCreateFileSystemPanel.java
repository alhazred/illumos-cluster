/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)HASPCreateFileSystemPanel.java 1.21     08/12/10 SMI"
 */


package com.sun.cluster.dswizards.hastorageplus;

import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.hastorageplus.HAStoragePlusMBean;
import com.sun.cluster.agent.dataservices.utils.DataServicesUtil;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;

import java.util.HashMap;

import javax.management.remote.JMXConnector;


public class HASPCreateFileSystemPanel extends WizardLeaf {


    private String panelName; // Panel Name
    private WizardI18N wizardi18n; // Message Bundle
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private JMXConnector localConnection = null;
    private HAStoragePlusMBean mbeans[] = null;

    /**
     * Creates a new instance of HASPCreateFileSystemPanel
     */
    public HASPCreateFileSystemPanel() {
        super();
    }

    /**
     * Creates a HASPCreateFileSystemPanel Panel with the given name and manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public HASPCreateFileSystemPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    public HASPCreateFileSystemPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;
        this.wizardi18n = wizardi18n;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.
     *		WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        String title = wizardi18n.getString("hasp.create.filesystem.title");
        String back = wizardi18n.getString("ttydisplay.back.char");
        String yes = wizardi18n.getString("cliwizards.yes");
        String no = wizardi18n.getString("cliwizards.no");
        String mountpoint_str = wizardi18n.getString(
                "hasp.create.filesystem.mountpoint.cli");
        String devicepath_str = wizardi18n.getString(
                "hasp.create.filesystem.path.cli");
        String error1 = wizardi18n.getString(
                "hasp.create.filesystem.path.error");
        String error2 = wizardi18n.getString(
                "hasp.create.filesystem.path.invalid");
        String global_confirm = wizardi18n.getString(
                "hasp.create.filesystem.globaloption");
        String fsck_confirm = wizardi18n.getString(
                "hasp.create.filesystem.fsckearlyoption");
        String dev_path_err = wizardi18n.getString(
                "hasp.create.filesystem.path.error1");

        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        // Title
        TTYDisplay.printTitle(title);
        localConnection = getConnection(Util.getClusterEndpoint());

        String nodeList[] = (String[]) wizardModel.getWizardValue(
                Util.RG_NODELIST);
        mbeans = new HAStoragePlusMBean[nodeList.length];

        for (int i = 0; i < nodeList.length; i++) {
            String split_str[] = nodeList[i].split(Util.COLON);
            String nodeEndPoint = Util.getNodeEndpoint(localConnection,
                    split_str[0]);
            JMXConnector connector = getConnection(nodeEndPoint);
            mbeans[i] = HASPWizardCreator.getHAStoragePlusMBeanOnNode(
                    connector);
        }

        String mountpoint = "";

        while (mountpoint.trim().length() == 0) {
            mountpoint = TTYDisplay.promptForEntry(mountpoint_str);
            mountpoint = mountpoint.trim();

            if (mountpoint.equals(back)) {
                this.cancelDirection = true;

                return;
            }

            if (mountpoint.length() > 0) {

                // Trim the last '/' as it would not allow the directory to
                // mount
                if (mountpoint.endsWith(Util.SLASH)) {
                    mountpoint = mountpoint.substring(0,
                            mountpoint.length() - 1);
                }

                if (DataServicesUtil.isValidMountPoint(mountpoint) != true) {
                    TTYDisplay.printError(wizardi18n.getString(
                            "pathEntry.error"));
                    mountpoint = "";
                } else {

                    // make sure that the mount point does not already
                    // exist as a file on all nodes the user has selected
                    ErrorValue errVal = validateInput(mountpoint, nodeList);

                    if (!errVal.getReturnValue().booleanValue()) {
                        TTYDisplay.printError(errVal.getErrorString());
                        mountpoint = "";
                    }
                }
            }
        }

        TTYDisplay.showNewline();

        String devicepath = "";
        boolean error = true;
        String propName[] = { Util.HASP_FSTYPE };
        HAStoragePlusMBean mbean = HASPWizardCreator.getHAStoragePlusMBean(
                localConnection);
        String fstype = null;
        String rgMode = (String) wizardModel.getWizardValue(Util.RG_MODE);

        while (error) {
            devicepath = TTYDisplay.promptForEntry(devicepath_str);
            devicepath = devicepath.trim();

            if (devicepath.equals(back)) {
                this.cancelDirection = true;

                return;
            }

            if ((devicepath.trim().length() != 0) &&
                    (devicepath.startsWith("/dev"))) {

                if (DataServicesUtil.isValidMountPoint(devicepath) != true) {
                    TTYDisplay.printError(wizardi18n.getString(
                            "pathEntry.error"));
                } else {
                    HashMap fstypeMap = mbean.discoverPossibilities(propName,
                            devicepath);

                    if ((fstype = (String) fstypeMap.get(propName[0])) ==
                            null) {
                        TTYDisplay.printError(error1);
                    } else {
                        error = false;
                    }
                }
            } else {
                TTYDisplay.printError(error2);
            }

            // Check if the devicepath entry is unique on all
            // the nodes
            if (!error) {
                String devPathName[] = { Util.DEVICE_PATH };
                HashMap userInput = new HashMap();
                String devpath = devicepath.replaceFirst(Util.RDSK, Util.DSK);
                userInput.put(Util.DEVICE_PATH, devpath);
                userInput.put(Util.RG_NODELIST, nodeList);

                ErrorValue errVal = null;
                errVal = mbean.validateInput(devPathName, userInput, null);

                if (!errVal.getReturnValue().booleanValue()) {
                    error = true;

                    String devpath_error = wizardi18n.getString(
                            "hasp.create.filesystem.path.error2",
                            new String[] { errVal.getErrorString() });

                    TTYDisplay.printError(devpath_error);
                }
            }

            // Check if the device can be primaried on all the node
            // of this resource group
            if (!error) {
                String propertyName[] = { Util.HASP_ALL_FILESYSTEMS };
                HashMap userMap = new HashMap();
                userMap.put(Util.RG_NODELIST, nodeList);
                userMap.put(Util.DEVPATH, devicepath);
                userMap.put(Util.RG_MODE, rgMode);

                ErrorValue returnVal = mbean.validateInput(propertyName,
                        userMap, null);

                if (!returnVal.getReturnValue().booleanValue()) {
                    error = true;
                    TTYDisplay.printError(dev_path_err);
                }
            }
        }

        String global_option = "";

        if ((rgMode != null) && rgMode.equals(Util.SCALABLE_RG_MODE)) {
            global_option = yes;
        } else {
            TTYDisplay.showNewline();
            global_option = TTYDisplay.getConfirmation(global_confirm, yes, no);
        }

        if (global_option.equals(back)) {
            this.cancelDirection = true;

            return;
        }

        TTYDisplay.showNewline();

        // fsck option is not used by Solaris anymore
        String checkfilesystem_option = yes;

        wizardModel.selectCurrWizardContext();
        wizardModel.setWizardValue(
            HASPWizardConstants.HASP_CREATE_FS_MOUNTPOINT, mountpoint);
        wizardModel.setWizardValue(HASPWizardConstants.HASP_CREATE_FS_DEVPATH,
            devicepath);
        wizardModel.setWizardValue(
            HASPWizardConstants.HASP_CREATE_FS_GLOBALOPTION, global_option);
        wizardModel.setWizardValue(HASPWizardConstants.HASP_CREATE_FS_FSCKEARLY,
            checkfilesystem_option);
        wizardModel.setWizardValue(HASPWizardConstants.HASP_CREATE_FS_TYPE,
            fstype);

        if (fstype.equals(Util.FSTYPE_VXFS)) {
            wizardModel.setWizardValue(
                HASPWizardConstants.HASP_CREATE_FS_MOUNTOPTION,
                Util.VXFS_LOGGING);
        } else {
            wizardModel.setWizardValue(
                HASPWizardConstants.HASP_CREATE_FS_MOUNTOPTION,
                Util.UFS_LOGGING);
        }
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#isDisplayComplete
     */
    public boolean isDisplayComplete() {
        return true;
    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }

    /**
     * All Validations Done in this Panel
     *
     * @return  ErrorValue
     */
    private ErrorValue validateInput(String mountpoint, String nodeList[]) {

        // validate mountpoint on all nodes of a cluster
        String propName[] = { Util.MOUNT_POINT };
        HashMap userInput = new HashMap();
        userInput.put(Util.MOUNT_POINT, mountpoint);
        userInput.put(Util.RG_NODELIST, nodeList);

        boolean error = false;
        int i = 0;
        ErrorValue errVal = null;
        StringBuffer errStr = new StringBuffer();

        while (!error && (i < nodeList.length)) {
            String split_str[] = nodeList[i].split(Util.COLON);
            errVal = mbeans[i].validateInput(propName, userInput, null);

            if (!errVal.getReturnValue().booleanValue()) {
                error = true;

                String mountpoint_error = wizardi18n.getString(
                        "hasp.create.filesystem.mountpoint.error4",
                        new String[] { split_str[0] });
                errStr.append(mountpoint_error);
            } else {
                i++;
            }
        }

        errVal.setErrorString(errStr.toString());

        return errVal;
    }
}
