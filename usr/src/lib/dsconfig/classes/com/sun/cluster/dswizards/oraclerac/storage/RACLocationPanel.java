/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)RACLocationPanel.java	1.13	08/12/10 SMI"
 */

package com.sun.cluster.dswizards.oraclerac.storage;

// CMASS
import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.utils.Util;

import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;

import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.oraclerac.framework.ORFStorageSelectionPanel;
import com.sun.cluster.dswizards.oraclerac.invoker.RACInvokerCreator;

import java.util.ArrayList;
import java.util.List;

import com.sun.cluster.model.RACModel;

import javax.management.remote.JMXConnector;

/**
 * The Panel Class that gets a Hostname or list of Hostnames as input from the
 * User.
 */
public class RACLocationPanel extends ORFStorageSelectionPanel {

    /**
     * Creates a new instance of RACLocationPanel
     */
    public RACLocationPanel() {
        super();
    }

    /**
     * Creates a RACLocationPanel with the given name and manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     * @param  flow  Handler to the WizardFlow
     */
    public RACLocationPanel(String name, WizardTreeManager manager) {
        super(name, manager);
    }

    /**
     * Creates a HostnameEntry Panel with the given name and Wizard state
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     */
    public RACLocationPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(name, model, wizardFlow, wizardi18n);
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.
     *		WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        String error_txt = wizardi18n.getString(
                "racstorage.racLocationPanel.error.desc");
        String error_txt1 = wizardi18n.getString(
                "racstorage.racLocationPanel.error.desc1");
        String error_txt2 = wizardi18n.getString(
                "racstorage.racLocationPanel.error.desc2");
        String error_txt3 = wizardi18n.getString(
                "racstorage.racLocationPanel.error.desc3");
        String error_txt4 = wizardi18n.getString(
                "racstorage.racLocationPanel.error.desc4");
        String hwraid_desc = wizardi18n.getString(
                "racstorage.racLocationPanel.hwraid.desc");
        String continue_exit = wizardi18n.getString("cliwizards.continueExit");

	String keyPrefix = (String) wizardModel.getWizardValue(Util.KEY_PREFIX);
	String storageManagementKey = keyPrefix + Util.ORF_STORAGE_MGMT_SCHEMES;
        wizardModel.setWizardValue(storageManagementKey, null);
        super.consoleInteraction();

        ArrayList storageScheme = (ArrayList) wizardModel.getWizardValue(
                storageManagementKey);

        if (storageScheme == null) {
            TTYDisplay.printSubTitle(error_txt);
            TTYDisplay.clear(1);
            TTYDisplay.showText(error_txt1, 4);
            TTYDisplay.clear(1);
            TTYDisplay.showText(error_txt2, 4);
            TTYDisplay.clear(2);
            TTYDisplay.showText(error_txt3, 4);
            TTYDisplay.clear(2);
            TTYDisplay.showText(error_txt4, 4);
            TTYDisplay.clear(2);
            TTYDisplay.promptForEntry(continue_exit);

            // Always close connection
            closeConnections();
            System.exit(0);
        }

// START --
// The code in this block is repeated in the RACInvokerPanel.java
// Find a way to making code a function call, that can be called
// from both the places. The code is repeated here to avoid
// changes to many file and thus increasing the risk in the show-stopper
// builds of SC3.2
        // Show the Devices configured for RAC if user had selected
        // SVM or VXVM
        wizardModel.setWizardValue(Util.SELECTED_DG_RESOURCES, null);
        wizardModel.setWizardValue(Util.SELECTED_FS_RESOURCES, null);

        if (storageScheme.contains(Util.SVM) ||
                storageScheme.contains(Util.VXVM)) {
            wizardModel.setWizardValue(ORSWizardConstants.SHOW_RAC_DEVICES,
                "true");
        }

        // Show Filesystems configured for RAC if user had selected
        // NAS or QFS
        if (storageScheme.contains(Util.QFS_HWRAID) ||
	    storageScheme.contains(Util.NAS) ||
	    storageScheme.contains(Util.SUN_NAS)) {
            wizardModel.setWizardValue(ORSWizardConstants.SHOW_RAC_FS, "true");
        }

        if (storageScheme.contains(Util.QFS_SVM)) {
            wizardModel.setWizardValue(ORSWizardConstants.SHOW_RAC_FS, "true");
            wizardModel.setWizardValue(ORSWizardConstants.SHOW_RAC_DEVICES,
                "true");
        }

        if ((storageScheme.size() == 1) &&
                storageScheme.contains(Util.HWRAID)) {

            // user has selected Hardware RAID without VM option
            TTYDisplay.clear(2);
            TTYDisplay.showText(hwraid_desc, 4);
            TTYDisplay.clear(2);
            TTYDisplay.promptForEntry(continue_exit);

            // Always close connection
            closeConnections();
            System.exit(0);
        }
// END --
    }

    private void closeConnections() {

        String nodeList[] = (String[]) wizardModel.getWizardValue(
                Util.BASE_CLUSTER_NODELIST);

        try {
	    JMXConnector localConnection = getConnection(Util.getClusterEndpoint());
            wizardModel.writeToStateFile(localConnection
                .getMBeanServerConnection());
	    RACModel racModel = RACInvokerCreator.getRACModel();
	    racModel.closeConnections(nodeList);
        } catch (Exception e) {
            TTYDisplay.printError(wizardi18n.getString(
                    "dswizards.stateFile.error"));
        }
    }

}
