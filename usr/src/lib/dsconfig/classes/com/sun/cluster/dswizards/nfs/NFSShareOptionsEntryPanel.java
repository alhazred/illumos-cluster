/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)NFSShareOptionsEntryPanel.java 1.19     08/12/10 SMI"
 */


package com.sun.cluster.dswizards.nfs;


// J2SE
import com.sun.cluster.agent.dataservices.common.ErrorValue;

// CMASS
import com.sun.cluster.agent.dataservices.nfs.NFSMBean;
import com.sun.cluster.agent.dataservices.utils.DataServicesUtil;
import com.sun.cluster.agent.dataservices.utils.Util;

// CLI SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.management.remote.JMXConnector;


/**
 * The Panel Class that gets a ShareOptions for corresponding mountpoints as
 * input from the User.
 */
public class NFSShareOptionsEntryPanel extends WizardLeaf {


    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N wizardi18n; // Message Bundle
    private HashMap defShareOptMap = null;


    /**
     * Creates a new instance of NFSShareOptionsEntryPanel
     */
    public NFSShareOptionsEntryPanel() {
        super();
    }

    /**
     * Creates a NFSShareOptionsEntryPanel Panel with the given name and tree
     * manager.
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public NFSShareOptionsEntryPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates a NFSShareOptionsEntryPanel Panel for either the NFS Wizard with
     * the given name and associates it with the WizardModel and WizardFlow.
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     * @param  wizardi18n  Resource Bundle
     */
    public NFSShareOptionsEntryPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;
        this.wizardi18n = wizardi18n;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {

        String shareCmdsList[] = null;
        String fileSysMtpts[] = null;
        List fileSysMtptsBuff = null;
        HashMap selShareOptMap = new HashMap();
        Object tmpObj = null;
        int index = 0;
        List optionSelected = null;

        String shareOptEntry_listtitle = wizardi18n.getString(
                "nfs.shareOptEntry.list.title");
        String shareOptEntry_help = wizardi18n.getString(
                "nfs.shareOptEntry.help.cli");
        String desc_cli_txt = wizardi18n.getString(
                "nfs.shareOptEntry.desc.cli");
        String title_txt = wizardi18n.getString("nfs.shareOptEntry.title");
        String desc_txt = wizardi18n.getString("nfs.shareOptEntry.desc");
        String done_txt = wizardi18n.getString("ttydisplay.menu.done");
        String dchar = (String) wizardi18n.getString("ttydisplay.d.char");
        String back = wizardi18n.getString("ttydisplay.back.char");

        // Get already selected share option data
        tmpObj = wizardModel.getWizardValue(Util.NFS_SHARE_OPT_DATA);

        if (tmpObj != null)
            selShareOptMap = (HashMap) tmpObj;

        // Get share commands list
        tmpObj = wizardModel.getWizardValue(Util.NFS_SHARE_CMDS);

        if (tmpObj != null)
            shareCmdsList = (String[]) tmpObj;

        // Get selected file system mountpoints
        tmpObj = wizardModel.getWizardValue(Util.NFS_HASP_MTPTS);

        if (tmpObj != null)
            fileSysMtptsBuff = (ArrayList) tmpObj;

        fileSysMtpts = (String[]) fileSysMtptsBuff.toArray(
                new String[fileSysMtptsBuff.size()]);

        if (selShareOptMap.isEmpty()) {

            HashMap shareOptsMap = getDefShareOptMap();

            // Get String arrays for possible options
            String secOptions[] = (String[]) shareOptsMap.get(
                    Util.SHARE_OPT_SECURITY);
            String accOptions[] = (String[]) shareOptsMap.get(
                    Util.SHARE_OPT_ACCESS);
            String nosuidOptions[] = (String[]) shareOptsMap.get(
                    Util.SHARE_OPT_NOSUID);

            HashMap defShareOptsMap = new HashMap();
            defShareOptsMap.put(Util.SHARE_OPT_SECURITY, secOptions[0]);
            defShareOptsMap.put(Util.SHARE_OPT_ACCESS, accOptions[0]);
            defShareOptsMap.put(Util.SHARE_OPT_NOSUID, nosuidOptions[0]);
            defShareOptsMap.put(Util.SHARE_OPT_PATH, Util.NFS_DIR_DATA);

            shareCmdsList = new String[fileSysMtpts.length];

            for (int i = 0; i < fileSysMtpts.length; i++) {
                shareCmdsList[i] = getShareCmd(fileSysMtpts[i],
                        defShareOptsMap);
                selShareOptMap.put(fileSysMtpts[i], defShareOptsMap);
            }
        }

        while (true) {

            // Enable Navigation
            TTYDisplay.setNavEnabled(true);
            this.cancelDirection = false;
            TTYDisplay.clearScreen();
            TTYDisplay.clear(1);

            // Title
            TTYDisplay.printTitle(title_txt);
            TTYDisplay.clear(1);
            TTYDisplay.pageText(desc_txt, 4);
            TTYDisplay.clear(1);
            TTYDisplay.pageText(desc_cli_txt, 4);
            TTYDisplay.clear(1);

            optionSelected = TTYDisplay.getScrollingOption(
                    shareOptEntry_listtitle, shareCmdsList, null, "",
                    shareOptEntry_help, false);

            if (optionSelected == null) {
                this.cancelDirection = true;

                return;
            }

            if (optionSelected.isEmpty()) {

                // Store selected share commands and share opt data in wizard
                // model.
                wizardModel.setWizardValue(Util.NFS_SHARE_CMDS, shareCmdsList);
                wizardModel.setWizardValue(Util.NFS_SHARE_OPT_DATA,
                    selShareOptMap);

                break;
            } else {
                // Modify Share command as per user input

                // get the index number of selected value
                for (index = 0; index < shareCmdsList.length; index++) {

                    if (shareCmdsList[index].equals(optionSelected.get(0))) {
                        break;
                    }
                }

                HashMap selOptMap = (HashMap) selShareOptMap.get(
                        fileSysMtpts[index]);
                selOptMap = getSelShareOptMap(fileSysMtpts[index], selOptMap);
                shareCmdsList[index] = getShareCmd(fileSysMtpts[index],
                        selOptMap);
            }
        }
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#isDisplayComplete
     */
    public boolean isDisplayComplete() {
        return true;
    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }

    private String getShareCmd(String mtpt, HashMap shareOptions) {

        String desc = "";
        String access = "";
        String nosuid = "";
        String security = "";
        String path = "";
        String shareCmd = "";

        desc = ((shareOptions.get(Util.SHARE_OPT_DESC)) == null)
            ? "" : (String) shareOptions.get(Util.SHARE_OPT_DESC);

        path = ((shareOptions.get(Util.SHARE_OPT_PATH)) == null)
            ? "" : (String) shareOptions.get(Util.SHARE_OPT_PATH);

        access = (String) shareOptions.get(Util.SHARE_OPT_ACCESS);

        if (((String) shareOptions.get(Util.SHARE_OPT_NOSUID)).equals(
                    Util.SHARE_OPT_NOSUID_YVAL)) {
            nosuid = NFSWizardConstants.SHARE_NOSUID_OPT;
        } else
            nosuid = "";

        security = (String) shareOptions.get(Util.SHARE_OPT_SECURITY);

        // Construct share command and store it in array
        shareCmd = NFSWizardConstants.SHARE + Util.SPACE;

        if (!desc.equals("")) {
            shareCmd = shareCmd + NFSWizardConstants.SHARE_D_OPT + Util.SPACE +
                desc + Util.SPACE;
        }

        shareCmd = shareCmd + NFSWizardConstants.SHARE_F_OPT + Util.SPACE +
            NFSWizardConstants.SHARE_O_OPT + Util.SPACE +
            NFSWizardConstants.SHARE_SEC_OPT + security + Util.COMMA + access;

        if (!nosuid.equals("")) {
            shareCmd = shareCmd + Util.COMMA + nosuid;
        }

        mtpt = NFSWizardCreator.appendDir(mtpt, path);

        shareCmd = shareCmd + Util.SPACE + mtpt;

        return shareCmd;

    }


    private HashMap getSelShareOptMap(String mtpt, HashMap selOptMap) {


        String shareOpt_error_txt = wizardi18n.getString(
                "nfs.shareOptEntry.shareopt.error");
        String secOpt_error_txt = wizardi18n.getString(
                "nfs.shareOptEntry.secopt.error");
        String dataDir_error_txt = wizardi18n.getString(
                "nfs.shareOptEntry.datadir.error");
        String continue_txt = wizardi18n.getString("cliwizards.continue");
        String table_title = wizardi18n.getString(
                "nfs.shareOptEntry.shareOptTable.title");
        String mtpt_txt = wizardi18n.getString(
                "nfs.shareOptEntry.shareOptTable.colheader.0");
        String desc_txt = wizardi18n.getString(
                "nfs.shareOptEntry.shareOptTable.colheader.2");
        String acc_txt = wizardi18n.getString(
                "nfs.shareOptEntry.shareOptTable.colheader.3");
        String nosuid_txt = wizardi18n.getString(
                "nfs.shareOptEntry.shareOptTable.colheader.4");
        String sec_txt = wizardi18n.getString(
                "nfs.shareOptEntry.shareOptTable.colheader.5");
        String path_txt = wizardi18n.getString(
                "nfs.shareOptEntry.shareOptTable.colheader.1");
        String heading1 = wizardi18n.getString("nfs.shareOptEntry.heading1");
        String heading2 = wizardi18n.getString("nfs.shareOptEntry.heading2");
        String help_txt = wizardi18n.getString("nfs.shareOptEntry.help.cli.1");
        String propNameText = wizardi18n.getString(
                "nfs.shareOptEntry.propname");
        String propDescText = wizardi18n.getString(
                "nfs.shareOptEntry.propdesc");
        String currValText = wizardi18n.getString(
                "nfs.shareOptEntry.currvalue");
        String newValText = wizardi18n.getString("nfs.shareOptEntry.newvalue");
        String confirm_txt = wizardi18n.getString("cliwizards.enterAgain");
        String yes = wizardi18n.getString("cliwizards.yes");
        String no = wizardi18n.getString("cliwizards.no");
        String title_txt = wizardi18n.getString("nfs.shareOptEntry.title");
        String done_txt = wizardi18n.getString("ttydisplay.menu.done");
        String dchar = (String) wizardi18n.getString("ttydisplay.d.char");
        String back = wizardi18n.getString("ttydisplay.back.char");
        String path_error_txt = wizardi18n.getString("nfs.pathEntry.error");
        String path_error_txt2 = wizardi18n.getString("pathEntry.error");


        Vector shareCmdVect = new Vector();
        String subheadings[] = { heading1, heading2 };
        String desc = "";
        String access = "";
        String nosuid = "";
        String security = "";
        String path = "";

        HashMap defOptMap = getDefShareOptMap();

        HashMap rgProps = (HashMap) wizardModel.getWizardValue(
                Util.NFS_RESOURCE_GROUP + Util.RGPROP_TAG);

        // PathPrefix
        String pathPrefix = (String) rgProps.get(Util.PATHPREFIX);
        Object tmpObj = rgProps.get(Util.NFS_DIR_ADMIN);
        String dirAdmin = "";

        if (tmpObj != null) {
            dirAdmin = (String) tmpObj;
        }

        boolean kerberosPresent = false;
        String securityUtilName = Util.SHARE_OPT_SECURITY;
        String securityOpts[] = (String[]) defOptMap.get(securityUtilName);

        for (int i = 0; i < securityOpts.length; i++) {

            if (securityOpts[i].indexOf("krb") != -1) {
                kerberosPresent = true;

                break;
            }
        }

        while (true) {

            // Enable Navigation
            TTYDisplay.setNavEnabled(true);
            this.cancelDirection = false;
            TTYDisplay.clearScreen();
            TTYDisplay.clear(1);

            // Title
            TTYDisplay.printTitle(title_txt);
            TTYDisplay.clear(1);

            // initialize
            shareCmdVect.removeAllElements();

            mtpt = ((selOptMap.get(Util.SHARE_OPT_MTPT)) == null)
                ? mtpt : (String) selOptMap.get(Util.SHARE_OPT_MTPT);

            desc = ((selOptMap.get(Util.SHARE_OPT_DESC)) == null)
                ? "" : (String) selOptMap.get(Util.SHARE_OPT_DESC);

            path = ((selOptMap.get(Util.SHARE_OPT_PATH)) == null)
                ? "" : (String) selOptMap.get(Util.SHARE_OPT_PATH);

            // Remove leading '/' to avoid confusion to the user
            if (path.charAt(0) == Util.SLASH.charAt(0)) {
                path = path.substring(1);
            }

            access = (String) selOptMap.get(Util.SHARE_OPT_ACCESS);

            nosuid = (String) selOptMap.get(Util.SHARE_OPT_NOSUID);

            security = (String) selOptMap.get(Util.SHARE_OPT_SECURITY);

            // Fill Vector
            shareCmdVect.add(mtpt_txt);
            shareCmdVect.add(mtpt);

            shareCmdVect.add(path_txt);
            shareCmdVect.add(path);

            shareCmdVect.add(desc_txt);
            shareCmdVect.add(desc);

            shareCmdVect.add(sec_txt);
            shareCmdVect.add(security);

            shareCmdVect.add(acc_txt);
            shareCmdVect.add(access);

            shareCmdVect.add(nosuid_txt);
            shareCmdVect.add(nosuid);


            HashMap customTags = new HashMap();
            customTags.put(dchar, done_txt);

            String option = TTYDisplay.create2DTable(table_title, subheadings,
                    shareCmdVect, customTags, help_txt);

            if (option.equals(back) || option.equals(dchar)) {
                return selOptMap;
            }

            TTYDisplay.clear(3);
            TTYDisplay.setNavEnabled(false);

            if (!option.equals(dchar)) {
                String prop = null;
                String currVal = null;
                String utilName = null;

                if (option.equals("1")) {
                    prop = "0";
                    currVal = mtpt;
                    utilName = Util.SHARE_OPT_MTPT;
                }

                if (option.equals("2")) {
                    prop = "1";
                    currVal = path;
                    utilName = Util.SHARE_OPT_PATH;
                }

                if (option.equals("3")) {
                    prop = "2";
                    currVal = desc;
                    utilName = Util.SHARE_OPT_DESC;
                }

                if (option.equals("4")) {
                    prop = "5";
                    currVal = security;
                    utilName = Util.SHARE_OPT_SECURITY;
                }

                if (option.equals("5")) {
                    prop = "3";
                    currVal = access;
                    utilName = Util.SHARE_OPT_ACCESS;
                }

                if (option.equals("6")) {
                    prop = "4";
                    currVal = nosuid;
                    utilName = Util.SHARE_OPT_NOSUID;
                }

                TTYDisplay.pageText(propNameText +
                    wizardi18n.getString(
                        "nfs.shareOptEntry.shareOptTable.colheader." + prop));

                TTYDisplay.pageText(propDescText +
                    wizardi18n.getString(
                        "nfs.shareOptEntry.shareOptTable.colheader." + prop +
                        ".desc"));
                TTYDisplay.pageText(currValText + currVal);

                if (option.equals("1")) {
                    TTYDisplay.clear(1);
                    TTYDisplay.promptForEntry(continue_txt);

                    continue;
                }

                if (!option.equals("1")) {

                    while (true) {
                        String tmpVal = "";
                        String tmpArr[] = null;

                        if (option.equals("2")) {
                            tmpVal = TTYDisplay.promptForEntry(newValText);
                            tmpVal = NFSWizardCreator.getCanonicalPath(tmpVal);

                            if ((tmpVal == null) ||
                                    (tmpVal.trim().length() == 0)) {
                                TTYDisplay.printError(path_error_txt);
                                selOptMap.put(utilName, currVal);

                                String confirm = TTYDisplay.getConfirmation(
                                        confirm_txt, yes, no);

                                if (!confirm.equalsIgnoreCase(yes)) {
                                    break;
                                }

                                continue;
                            } else if (!(DataServicesUtil.isValidMountPoint(
                                            tmpVal))) {
                                TTYDisplay.printError(path_error_txt2);
                                selOptMap.put(utilName, currVal);

                                continue;
                            }

                            if (mtpt.equals(pathPrefix) &&
                                    validateSharePath(tmpVal, dirAdmin)) {
                                TTYDisplay.printError(dataDir_error_txt);
                                selOptMap.put(utilName, currVal);

                                String confirm = TTYDisplay.getConfirmation(
                                        confirm_txt, yes, no);

                                if (!confirm.equalsIgnoreCase(yes)) {
                                    break;
                                }

                                continue;
                            }
                        }

                        if (option.equals("3")) {
                            tmpVal = TTYDisplay.promptForEntry(newValText);
                            tmpVal = tmpVal.trim();

                            if (tmpVal.indexOf(Util.SPACE) != -1) {

                                if ((tmpVal.startsWith(Util.DOUBLEQUOTE) &&
                                            tmpVal.endsWith(
                                                Util.DOUBLEQUOTE)) ||
                                        (tmpVal.startsWith(Util.SINGLEQUOTE) &&
                                            tmpVal.endsWith(
                                                Util.SINGLEQUOTE))) {
                                    tmpVal = tmpVal;
                                } else {
                                    tmpVal = Util.DOUBLEQUOTE + tmpVal +
                                        Util.DOUBLEQUOTE;
                                }
                            }
                        }

                        int kerberosSelected = 0;
                        int nonKerberosSelected = 0;

                        if (option.equals("4")) {
                            List optionSelected = TTYDisplay
                                .getScrollingOptions(newValText,
                                    (String[]) defOptMap.get(utilName), null,
                                    currVal, null, true);

			    if (optionSelected == null) {
				// do nothing, move on
                            } else if (optionSelected.size() > 1) {
                                int i = 0;
                                String tmp = null;

                                for (i = 0; i < optionSelected.size(); i++) {
                                    tmp = (String) optionSelected.get(i);

                                    if (tmp.indexOf("krb") != -1) {
                                        kerberosSelected++;
                                    }

                                    if ((tmp.indexOf("sys") != -1) ||
                                            (tmp.indexOf("none") != -1) ||
                                            (tmp.indexOf("default") != -1)) {
                                        nonKerberosSelected++;
                                    }
                                }

                                if (((kerberosSelected >= 1) &&
                                            (nonKerberosSelected >= 1)) ||
                                        ((kerberosSelected == 0) &&
                                            (nonKerberosSelected > 1))) {

                                    // validation failed
                                    TTYDisplay.printError(secOpt_error_txt);

                                    continue;
                                }

                                String sec = "";

                                for (int j = 0; j < optionSelected.size();
                                        j++) {
                                    sec = sec + (String) optionSelected.get(j) +
                                        ":";
                                }

                                int len = sec.length();
                                sec = sec.substring(0, len - 1);
                                tmpVal = sec;
                            } else {
                                tmpVal = (String) optionSelected.get(0);
                            }
                        }

                        if (option.equals("5") || option.equals("6")) {

                            List optionSelected = TTYDisplay.getScrollingOption(
                                    newValText,
                                    (String[]) defOptMap.get(utilName), null,
                                    currVal, null, true);
                            tmpVal = (String) optionSelected.get(0);
                        }

                        if (tmpVal != null) {

                            // validate user input
                            String properties[] = new String[] {
                                    Util.NFS_SHARE_OPT
                                };
                            selOptMap.put(utilName, tmpVal);

                            ErrorValue retVal = validateInput(properties,
                                    selOptMap, null);

                            if (!retVal.getReturnValue().booleanValue()) {
                                TTYDisplay.printError(shareOpt_error_txt);
                                selOptMap.put(utilName, currVal);

                                String confirm = TTYDisplay.getConfirmation(
                                        confirm_txt, yes, no);

                                if (!confirm.equalsIgnoreCase(yes)) {
                                    break;
                                }
                            } else {
                                break;
                            } // if else for validate
                        }
                    } // while
                }
            }
        } // while
    }


    private ErrorValue validateInput(String property[], HashMap data,
        Object helper) {
        ErrorValue errValue = null;
        NFSMBean nfsMBean = null;
        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);
        JMXConnector connector = getConnection(nodeEndPoint);
        nfsMBean = NFSWizardCreator.getNFSMBeanOnNode(connector);
        errValue = nfsMBean.validateInput(property, data, helper);

        return errValue;
    }

    private HashMap getDefShareOptMap() {

        if (defShareOptMap == null) {
            // Discover default values for nfs share options

            Thread t = null;
            String nodeList[] = (String[]) wizardModel.getWizardValue(
                    Util.RG_NODELIST);
            TTYDisplay.clear(1);
            t = TTYDisplay.busy(wizardi18n.getString(
                        "nfs.shareOptEntry.discovery.busy"));
            t.start();

            defShareOptMap = new HashMap();

            // get nfs mbean
            NFSMBean nfsMBean = null;
            String nodeEndPoint = (String) wizardModel.getWizardValue(
                    Util.NODE_TO_CONNECT);
            JMXConnector connector = getConnection(nodeEndPoint);
            nfsMBean = NFSWizardCreator.getNFSMBeanOnNode(connector);

            defShareOptMap = nfsMBean.discoverPossibilities(nodeList,
                    new String[] { Util.NFS_SHARE_OPT }, null);
            defShareOptMap = (HashMap) defShareOptMap.get(Util.NFS_SHARE_OPT);

            try {
                t.interrupt();
                t.join();
            } catch (Exception exe) {
            }
        }

        return defShareOptMap;
    }

    private boolean validateSharePath(String dataDir, String adminDir) {
        dataDir = NFSWizardCreator.trimSlash(dataDir);
        adminDir = NFSWizardCreator.trimSlash(adminDir);

        String dataDirAr[] = dataDir.split(Util.SLASH);
        String adminDirAr[] = adminDir.split(Util.SLASH);

        boolean error = true;

        if (dataDirAr.length <= adminDirAr.length) {

            for (int i = 0; i < dataDirAr.length; i++) {

                if (!dataDirAr[i].equals(adminDirAr[i])) {
                    error = false;

                    break;
                }
            }
        } else {
            error = false;
        }

        return error;

    }
}
