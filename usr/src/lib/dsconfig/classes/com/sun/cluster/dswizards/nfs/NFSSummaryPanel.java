/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)NFSSummaryPanel.java 1.14     08/12/10 SMI"
 */


package com.sun.cluster.dswizards.nfs;


// J2SE
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.dataservices.common.ErrorValue;

// CMASS
import com.sun.cluster.agent.dataservices.nfs.NFSMBean;
import com.sun.cluster.agent.dataservices.utils.DataServiceInfo;
import com.sun.cluster.agent.dataservices.utils.FileAccessorWrapper;
import com.sun.cluster.agent.dataservices.utils.ResourceInfo;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.rgm.property.ResourceProperty;
import com.sun.cluster.agent.rgm.property.ResourcePropertyStringArray;

// CLI Wizard SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;

// CLI wizards
import com.sun.cluster.dswizards.hastorageplus.HASPWizardConstants;
import com.sun.cluster.dswizards.logicalhost.LogicalHostWizardConstants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.Vector;

import javax.management.remote.JMXConnector;


/**
 * Summary Panel for NFS wizard
 */
public class NFSSummaryPanel extends WizardLeaf {


    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N wizardi18n; // Message Bundle


    /**
     * Creates a new instance of NFSSummaryPanel
     */
    public NFSSummaryPanel() {
        super();
    }

    /**
     * Creates a NFSSummaryPanel Panel with the given name and tree manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public NFSSummaryPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates a NFSSummaryPanel Panel for either the NFS Wizard with the given
     * name and associates it with the WizardModel and WizardFlow.
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     * @param  wizardi18n  Resource Bundle
     */
    public NFSSummaryPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;
        this.wizardi18n = wizardi18n;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {

        String title = wizardi18n.getString("nfs.summarypanel.title");
        String desc1 = wizardi18n.getString("nfs.summarypanel.desc1");
        String contwiz = wizardi18n.getString("cliwizards.continue");
        String table_title = wizardi18n.getString("nfs.summarypanel.desc2");
        String heading1 = wizardi18n.getString("nfs.summarypanel.heading1");
        String heading2 = wizardi18n.getString("nfs.summarypanel.heading2");
        String nfsrgname_txt = wizardi18n.getString(
                "nfs.summarypanel.nfsrg.name");
        String nfsresname_txt = wizardi18n.getString(
                "nfs.summarypanel.nfsresource.name");
        String lhresname_txt = wizardi18n.getString(
                "nfs.summarypanel.lhresource.name");
        String haspresname_txt = wizardi18n.getString(
                "nfs.reviewpanel.haspresource.name");
        String shareOptions_txt = wizardi18n.getString(
                "nfs.summarypanel.shareoptions.name");
        String pathPrefix_txt = wizardi18n.getString(
                "nfs.summarypanel.pathprefix.name");
        String create_txt = wizardi18n.getString(
                "ttydisplay.menu.createConfiguration");
        String help_text = wizardi18n.getHelpString("nfs.summarypanel.help");
        String cchar = (String) wizardi18n.getString("ttydisplay.c.char");
        String back = wizardi18n.getString("ttydisplay.back.char");
        String propNameText = wizardi18n.getString("nfs.summarypanel.propname");
        String propTypeText = wizardi18n.getString("nfs.summarypanel.proptype");
        String propDescText = wizardi18n.getString("nfs.summarypanel.propdesc");
        String currValText = wizardi18n.getString("nfs.summarypanel.currval");
        String confirm_txt = wizardi18n.getString("cliwizards.enterAgain");
        String yes = wizardi18n.getString("cliwizards.yes");
        String no = wizardi18n.getString("cliwizards.no");
        String fwd_arrow = wizardi18n.getString("ttydisplay.fwd.arrow");
        String continue_txt = wizardi18n.getString("cliwizards.continue");
        String shareopt_name = wizardi18n.getString(
                "nfs.summarypanel.shareoptions.name");
        String shareopt_desc = wizardi18n.getString(
                "nfs.summarypanel.shareoptions.desc");
        String shareopt_type = wizardi18n.getString(
                "nfs.summarypanel.shareoptions.type");

        String subheadings[] = { heading1, heading2 };
        HashMap rgProps = (HashMap) wizardModel.getWizardValue(
                Util.NFS_RESOURCE_GROUP + Util.RGPROP_TAG);
        String properties[] = null;
        Object tmpObj = null;

        // Enable Navigation
        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        boolean flag = true;

        // Summary Panel Table
        Vector summaryPanelVect = new Vector();

        // PathPrefix
        String pathPrefix = (String) rgProps.get(Util.PATHPREFIX);
        tmpObj = rgProps.get(Util.NFS_DIR_ADMIN);

        if (tmpObj != null) {
            pathPrefix = NFSWizardCreator.appendDir(pathPrefix,
                    (String) tmpObj);
        }

        // Resource Group Name
        String nfsRgName = (String) wizardModel.getWizardValue(
                Util.NFS_RESOURCE_GROUP + Util.NAME_TAG);

        // HA-NFS Resource Name
        String nfsResName = (String) wizardModel.getWizardValue(Util.NFS_RID +
                Util.NAME_TAG);

        // LogicalHostName Resource Name
        String lhResName = (String) wizardModel.getWizardValue(Util.NFS_LH_RID +
                Util.NAME_TAG);

        // Share Commands List
        String shareCmdsList[] = (String[]) wizardModel.getWizardValue(
                Util.NFS_SHARE_CMDS);

        int optSel = 0;

        while (flag) {
            TTYDisplay.clearScreen();
            TTYDisplay.clear(1);

            // Title
            TTYDisplay.printTitle(title);
            TTYDisplay.clear(1);
            TTYDisplay.pageText(desc1, 4);
            TTYDisplay.clear(1);

            // initialize
            summaryPanelVect.removeAllElements();

            // NFS RG Name
            summaryPanelVect.add(nfsrgname_txt);
            summaryPanelVect.add(nfsRgName);

            // PathPrefix
            summaryPanelVect.add(pathPrefix_txt);
            summaryPanelVect.add(pathPrefix);

            // HA-NFS Resource
            summaryPanelVect.add(nfsresname_txt);
            summaryPanelVect.add(nfsResName);

            // LogicalHost Name Resource
            summaryPanelVect.add(lhresname_txt);
            summaryPanelVect.add(lhResName);

            // Share Options
            summaryPanelVect.add(shareOptions_txt);
            summaryPanelVect.add(fwd_arrow);

            // Hasp Resources
            HashMap newHaspRsrcs1 = new HashMap();
            Iterator iterkeys = null;
            Set haspWizSelKeys = null;
            List haspResNameList = new ArrayList();

            tmpObj = wizardModel.getWizardValue(Util.NFS_HASPWIZSEL1);

            if (tmpObj != null) {
                newHaspRsrcs1 = (HashMap) tmpObj;
            }

            haspWizSelKeys = newHaspRsrcs1.keySet();
            iterkeys = haspWizSelKeys.iterator();

            while (iterkeys.hasNext()) {
                String haspRsName = (String) iterkeys.next();
                summaryPanelVect.add(haspresname_txt);
                summaryPanelVect.add(haspRsName);
                haspResNameList.add(haspRsName);
            }

            // hasp resources already present
            tmpObj = wizardModel.getWizardValue(Util.NFS_HASP_RID +
                    Util.NAME_TAG);

            List rsrcList = new ArrayList();

            if (tmpObj != null) {
                rsrcList = (ArrayList) tmpObj;
            }

            iterkeys = rsrcList.iterator();

            while (iterkeys.hasNext()) {
                String haspRsName = (String) iterkeys.next();
                summaryPanelVect.add(haspresname_txt);
                summaryPanelVect.add(haspRsName);
                haspResNameList.add(haspRsName);
            }

            HashMap customTags = new HashMap();
            customTags.put(cchar, create_txt);

            String option = TTYDisplay.create2DTable(table_title, subheadings,
                    summaryPanelVect, customTags, help_text);

            if (option.equals(back)) {
                this.cancelDirection = true;

                return;
            }

            TTYDisplay.clear(3);
            TTYDisplay.setNavEnabled(false);

            if (!option.equals(cchar)) {
                String prop = null;
                String currVal = null;

                if (option.equals("1")) {

                    // Change NFS RG name
                    prop = "nfsrg";
                    currVal = nfsRgName;

                } else if (option.equals("2")) {

                    // Change PathPrefix
                    prop = "pathprefix";
                    currVal = pathPrefix;

                } else if (option.equals("3")) {

                    // Change PathPrefix
                    prop = "nfsresource";
                    currVal = nfsResName;

                } else if (option.equals("4")) {

                    // Change PathPrefix
                    prop = "lhresource";
                    currVal = lhResName;

                } else if (option.equals("5")) {
                    TTYDisplay.pageText(propNameText + shareopt_name);

                    TTYDisplay.pageText(propDescText + shareopt_desc);

                    TTYDisplay.pageText(propTypeText + shareopt_type);

                    TTYDisplay.printList(currValText, shareCmdsList);
                } else {
                    prop = "haspresource";
                    optSel = Integer.parseInt(option);
                    currVal = (String) haspResNameList.get(optSel - 6);
                }

                if (!option.equals("5")) {
                    TTYDisplay.pageText(propNameText +
                        wizardi18n.getString(
                            "nfs.summarypanel." + prop + ".name"));

                    TTYDisplay.pageText(propDescText +
                        wizardi18n.getString(
                            "nfs.summarypanel." + prop + ".desc"));

                    TTYDisplay.pageText(currValText + currVal);
                }

                TTYDisplay.clear(1);
                TTYDisplay.promptForEntry(continue_txt);
            } else {
                break;
            }

            TTYDisplay.clear(3);
        }

        createCommands();
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#isDisplayComplete
     */
    public boolean isDisplayComplete() {
        return true;
    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }

    private void createCommands() {

        HashMap rgProps = (HashMap) wizardModel.getWizardValue(
                Util.NFS_RESOURCE_GROUP + Util.RGPROP_TAG);
        String newRG = (String) wizardModel.getWizardValue(
                NFSWizardConstants.NEWRGFLAG);

        // PathPrefix
        String pathPrefix = (String) rgProps.get(Util.PATHPREFIX);
        String dirAdmin = "";
        Object tmpObj = rgProps.get(Util.NFS_DIR_ADMIN);

        if (tmpObj != null) {
            dirAdmin = (String) tmpObj;
        }

        /** Application Configuration helper data */
        Map appConfMap = new HashMap();
        appConfMap.put(Util.PATHPREFIX,
            NFSWizardCreator.appendDir(pathPrefix, dirAdmin));
        appConfMap.put(Util.NFS_SHARE_CMDS,
            wizardModel.getWizardValue(Util.NFS_SHARE_CMDS));
        appConfMap.put(Util.NFS_RID + Util.NAME_TAG,
            wizardModel.getWizardValue(Util.NFS_RID + Util.NAME_TAG));


        // Create the Command Generation Structure
        Map confMap = new HashMap();

        /** NFS Application Configuration Data */
        confMap.put(Util.NFS_APPCONFDATA, appConfMap);


        /** ResourceGroup Structure */

        // ResourceGroup Name
        confMap.put(Util.NFS_RESOURCE_GROUP + Util.NAME_TAG,
            (String) wizardModel.getWizardValue(
                Util.NFS_RESOURCE_GROUP + Util.NAME_TAG));
        // ResourceGroup Properties

        // Get comma separated nodelist to pass to command generator
        String nodeList[] = (String[]) wizardModel.getWizardValue(
                Util.RG_NODELIST);
        String commaSeparatedNodeList = nodeList[0];

        for (int ctr = 1; ctr < nodeList.length; ctr++) {
            commaSeparatedNodeList = commaSeparatedNodeList + Util.COMMA +
                nodeList[ctr];
        }

        rgProps.put(Util.NODELIST, commaSeparatedNodeList);

        // If user has chosen an existing RG
        // - remove nodelist from RG properties
        // - if PathPrefix is already set remove PathPrefix from RG Properties
        if (newRG.equals("false")) {
            rgProps.remove(Util.NODELIST);

            NFSMBean nfsMBean = getNFSMBean();

            HashMap resultMap = nfsMBean.discoverPossibilities(
                    new String[] { Util.PATHPREFIX },
                    wizardModel.getWizardValue(
                        Util.NFS_RESOURCE_GROUP + Util.NAME_TAG));

            if (!((String) resultMap.get(Util.PATHPREFIX)).equals("")) {
                rgProps.remove(Util.PATHPREFIX);
                rgProps.remove(Util.NFS_DIR_ADMIN);
            }
        }

        confMap.put(Util.NFS_RESOURCE_GROUP + Util.RGPROP_TAG, rgProps);


        /** Resource Structure */

        /*  NFS Resource */

        // Resource Name
        confMap.put(Util.NFS_RID + Util.NAME_TAG,
            wizardModel.getWizardValue(Util.NFS_RID + Util.NAME_TAG));

        // Resource Properties - must copy empty list as per
        // command generator requirements.
        List nfsResExtProp = new ArrayList();
        List nfsResSysProp = new ArrayList();

        confMap.put(Util.NFS_RID + Util.SYSPROP_TAG, nfsResSysProp);
        confMap.put(Util.NFS_RID + Util.EXTPROP_TAG, nfsResExtProp);


        /* Logical Host Resource  */

        // Resource Name
        confMap.put(Util.NFS_LH_RID + Util.NAME_TAG,
            (String) wizardModel.getWizardValue(
                Util.NFS_LH_RID + Util.NAME_TAG));

        // Resource Properties
        List logicalHostExtProp = new ArrayList();
        List logicalHostSysProp = new ArrayList();

        if (newRG.equals("true")) {
            String hostnameList[] = (String[]) wizardModel.getWizardValue(
                    Util.SEL_HOSTNAMES);
            String netifList[] = (String[]) wizardModel.getWizardValue(
                    LogicalHostWizardConstants.SEL_NETIFLIST);
            ResourceProperty rp = new ResourcePropertyStringArray(
                    Util.HOSTNAMELIST, hostnameList);
            logicalHostExtProp.add(rp);

            if (netifList != null) {
                rp = new ResourcePropertyStringArray(Util.NETIFLIST, netifList);
                logicalHostExtProp.add(rp);
            }
        }

        confMap.put(Util.NFS_LH_RID + Util.SYSPROP_TAG, logicalHostSysProp);
        confMap.put(Util.NFS_LH_RID + Util.EXTPROP_TAG, logicalHostExtProp);


        /* HASP Resources */

        // Hasp Resources already present on the cluster to be used by NFS res
        confMap.put(Util.NFS_HASP_RID + Util.NAME_TAG,
            (ArrayList) wizardModel.getWizardValue(
                Util.NFS_HASP_RID + Util.NAME_TAG));

        // Hasp Resources created through NFS wizard
        confMap.put(Util.NFS_HASPWIZSEL1,
            (HashMap) wizardModel.getWizardValue(Util.NFS_HASPWIZSEL1));
        confMap.put(Util.NFS_HASPWIZSEL2,
            (HashMap) wizardModel.getWizardValue(Util.NFS_HASPWIZSEL2));
        confMap.put(Util.RG_NODELIST, nodeList);

        // Operation Message for share commands dump operation
        confMap.put(Util.NFS_OP_MSG,
            wizardi18n.getString("nfs.resultspanel.dumpAdmin"));

        generateCommands(confMap);
    }

    /**
     * Generates the commands by invoking the command generator
     */
    private boolean generateCommands(Map confMap) {

        NFSMBean nfsMBean = getNFSMBean();
        List commandList = null;
        Thread t = null;
        boolean retVal = true;

        try {
            TTYDisplay.clear(1);
            t = TTYDisplay.busy(wizardi18n.getString(
                        "cliwizards.executingMessage"));
            t.start();
            commandList = nfsMBean.generateCommands(confMap);
        } catch (CommandExecutionException cee) {
            wizardModel.setWizardValue(Util.EXITSTATUS, cee);
            commandList = nfsMBean.getCommandList();
            retVal = false;
        } finally {
            wizardModel.setWizardValue(Util.CMDLIST, commandList);

            try {
                t.interrupt();
                t.join();
            } catch (Exception exe) {
            }
        }

        return retVal;
    }

    private NFSMBean getNFSMBean() {
        NFSMBean nfsMBean = null;
        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);
        JMXConnector connector = getConnection(nodeEndPoint);
        nfsMBean = NFSWizardCreator.getNFSMBeanOnNode(connector);

        return nfsMBean;
    }
}
