/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)WizardStateManager.java	1.15	08/12/10 SMI"
 */

package com.sun.cluster.dswizards.common;

// J2SE
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

// JMX
import javax.management.MBeanServerConnection;
import javax.management.MBeanServerInvocationHandler;
import javax.management.ObjectName;

// Cacao
import com.sun.cacao.ObjectNameFactory;

// Dataservices
import com.sun.cluster.agent.dataservices.utils.InfrastructureMBean;
import com.sun.cluster.agent.dataservices.utils.Util;

/**
 * WizardStateManager is an utility Class that can be used to read/write the
 * wizardModel's currentContext from/into the wizard state file The wizard state
 * file is used to represent the current state of the wizard.
 */
public class WizardStateManager {

    /**
     * The stateFilename is the absolute path and filename of the state this
     * WizardStateManager object will read.
     */
    private String stateFilename;

    /*
     * Object stream references for reading and writing the state file
     */
    private ObjectOutputStream objectOutput;
    private ObjectInputStream objectInput;

    /**
     * Creates a WizardStateManager using the specified state file name
     *
     * @param  stateFilename  The absolute path and filename for the state this
     * reader will read/write.
     */
    public WizardStateManager(String stateFilename) {
        this.stateFilename = stateFilename;
    }

    /**
     * Serialize the current state of the wizard into the statefile. If the
     * statefile is not found a new statefile is created. This method uses
     * MBeanServer directly for getting access to the Infrastructure MBean
     *
     * @throws  IOException
     */
    public synchronized void writeCurrentContext(
        Object obj, MBeanServerConnection mbsc)
        throws IOException {

        // Get the current context Map
        Map curContextMap = getCurrentContext(obj);
        String statefile = this.stateFilename;

        if (statefile == null) {
            throw new FileNotFoundException("State File is not specified");
        }

        // Get handle to the InfrastructureMBean to write to
        // the WizardState File
        try {
            ObjectNameFactory onf = new ObjectNameFactory(Util.DOMAIN);
            ObjectName infraMBeanObjName = onf.getObjectName(
                    InfrastructureMBean.class, null);
            InfrastructureMBean infrastructureMBean = (InfrastructureMBean)
                MBeanServerInvocationHandler.newProxyInstance(mbsc,
                    infraMBeanObjName, InfrastructureMBean.class, false);
            infrastructureMBean.writeObject(statefile, curContextMap);
        } catch (IOException ioe) {
            throw ioe;
        }
    }

    /**
     * De-serialize the state file into the wizardModel
     *
     * @param boolean guiMode true if called from the gui wizard false otherwise
     * @return  wizardModel read from the statefile (or) null if no StateFile is
     * found.
     *
     * @throws  IOException
     */
    public synchronized Object readPreviousContext(boolean guiMode)
        throws IOException, ClassNotFoundException {

        String statefile = this.stateFilename;

        if (statefile == null) {
            throw new FileNotFoundException("State File is not specified");
        }

        // Return "null" if no state file is found
        File statefileDes = new File(statefile);

        if (!statefileDes.exists()) {
            return null;
        }

        // De-serialize the context and create a new wizardModel
        DSConfigWizardModel wizardModel = null;
	CliDSConfigWizardModel cliWizardModel = null;

	if (guiMode) {
	    wizardModel = new DSConfigWizardModel();
	} else {
	    cliWizardModel = new CliDSConfigWizardModel();
	}

        try {
            objectInput = new ObjectInputStream(new FileInputStream(statefile));

            Map prevContextMap = (HashMap) objectInput.readObject();

            // Create wizardModel
            if (prevContextMap != null) {
                Set propertyNames = prevContextMap.keySet();

                for (Iterator i = propertyNames.iterator(); i.hasNext();) {
                    String propertyName = (String) i.next();
		    if (guiMode) {
			wizardModel.setWizardValue(propertyName,
			    prevContextMap.get(propertyName));
		    } else {
			cliWizardModel.setWizardValue(propertyName,
			    prevContextMap.get(propertyName));
		    }
                }

                if (propertyNames != null) {
                    ArrayList wizardStateArray = new ArrayList(propertyNames);
		    if (guiMode) {
			wizardModel.setWizardValue(Util.WIZARD_STATE,
			    wizardStateArray);
		    } else {
			cliWizardModel.setWizardValue(Util.WIZARD_STATE,
			    wizardStateArray);
		    }
                }
            }

	    if (guiMode) {
		return wizardModel;
	    }
	    return cliWizardModel;
        } catch (IOException e) {

            if (e instanceof EOFException) {
                // do nothing, a blank state file
            }

            throw new IOException("Cannot read statefile \"" + statefile +
                "\" :" + e.toString());
        } finally {

            try {

                if (objectInput != null) {
                    objectInput.close();
                }
            } catch (Exception e) {

                if (e instanceof EOFException) {
                    // do nothiing, a blank state file
                }

                throw new IOException("Error while closing statefile \"" +
                    e.toString());
            }
        }
    }

    /**
     * Get the state path.
     *
     * @return  The path to this state.
     */
    public String getWizardStatePath() {
        return (stateFilename.substring(0,
                    stateFilename.lastIndexOf(File.separatorChar)));
    }

    /**
     * Get the state filename.
     *
     * @return  The filename of this state, including the path.
     */
    public String getWizardStateFilename() {
        return (stateFilename);
    }

    /**
     * Get the current context Map from the wizardModel
     *
     * @param  wizardModel
     *
     * @return  HashMap with the current context
     */
    public Map getCurrentContext(Object obj) {

        // Get the propertyNames from the valueMap
        Map curContextMap = new HashMap();
	DSConfigWizardModel guiModel = null;
	CliDSConfigWizardModel cliModel = null;
	ArrayList propertyNames = null;

	if (obj instanceof CliDSConfigWizardModel) {
	    cliModel = (CliDSConfigWizardModel)obj;
	    propertyNames = (ArrayList)cliModel.getWizardValue(
                Util.WIZARD_STATE);
	} else if (obj instanceof DSConfigWizardModel) {
	    guiModel = (DSConfigWizardModel)obj;
	    propertyNames = (ArrayList)guiModel.getWizardValue(
                Util.WIZARD_STATE);
	}

        if (propertyNames != null) {

            for (Iterator i = propertyNames.iterator(); i.hasNext();) {
                String propertyName = (String) i.next();

                // Check whether the value is serializable
		Object value = null;
		if (obj instanceof CliDSConfigWizardModel) {
		    value = cliModel.getWizardValue(propertyName);
		} else if (obj instanceof DSConfigWizardModel) {
		    value = guiModel.getWizardValue(propertyName);
		}

                if ((value != null) && isSerializable(value)) {
                    curContextMap.put(propertyName, value);
                }
            }
        }

        return curContextMap;
    }

    /**
     * checks if the object is serializable
     *
     * @return  True if object is serializable
     */
    private boolean isSerializable(Object o) {
        Class implInterfaces[] = o.getClass().getInterfaces();
        List interfaceList = Arrays.asList(implInterfaces);

        if (interfaceList.contains(java.io.Serializable.class)) {
            return true;
        }

        return false;
    }
}
