/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)DSConfigWizardModel.java	1.13	08/12/10 SMI"
 */

package com.sun.cluster.dswizards.common;

// J2SE
import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

// JATO
import com.iplanet.jato.model.DefaultModel;
import com.iplanet.jato.model.InvalidContextException;

//JMX
import javax.management.MBeanServerConnection;
import javax.management.remote.JMXConnector;


/**
 * A customized wizard Model for the DSConfig Wizards
 */
public class DSConfigWizardModel extends DefaultModel implements Serializable {

    // Constants representing various WizardContexts
    private final String WIZARD_CURR_CONTEXT = "WIZARD_CURR_CONTEXT";
    private final String WIZARD_DEFAULT_CONTEXT = "WIZARD_DEFAULT_CONTEXT";
    private final String WIZARD_PREV_CONTEXT = "WIZARD_PREV_CONTEXT";

    // StateManager used for representing the WizardStateFile
    // When the WizardModel serializes, we dont want the state manager to
    // serialize
    private transient WizardStateManager wizardStateManager;

    // Reference to the wizardModel read from the statefile
    private DSConfigWizardModel prevWizardModel;


    /**
     * Default Constructor
     */
    public DSConfigWizardModel() {
        super();
        setName(null);
        addContext(WIZARD_CURR_CONTEXT);
        addContext(WIZARD_DEFAULT_CONTEXT);
        addContext(WIZARD_PREV_CONTEXT);
        setUseDefaultValues(false);
    }

    /**
     * Creates a new DSConfigWizardModel for a given stateManager
     */
    public DSConfigWizardModel(WizardStateManager stateManager) {
        this(null, stateManager);
    }

    /**
     * Creates a new DSConfigWizardModel for a given stateManager and for a
     * given name
     */
    public DSConfigWizardModel(String name, WizardStateManager stateManager) {
        super();
        setName(name);
        addContext(WIZARD_CURR_CONTEXT);
        addContext(WIZARD_DEFAULT_CONTEXT);
        addContext(WIZARD_PREV_CONTEXT);
        setUseDefaultValues(false);
        this.wizardStateManager = stateManager;

        // Get the Previous WizardModel from the StateFile
        try {
            prevWizardModel = (DSConfigWizardModel)
		stateManager.readPreviousContext(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void selectCurrWizardContext() {

        try {
            selectContext(WIZARD_CURR_CONTEXT);
        } catch (InvalidContextException e) {
            // This means that the wizard has not set any
            // values in the model
            //
            // Should never fail.
        }
    }

    public void selectDefaultContext() {

        try {
            selectContext(DEFAULT_CONTEXT_NAME);
        } catch (InvalidContextException e) {
            // This means that the wizard has not set any
            // values in the model
            //
            // Should never fail.
        }
    }

    public void selectPrevContext() {

        try {
            selectContext(WIZARD_PREV_CONTEXT);
        } catch (InvalidContextException e) {
            // This should never fail
        }
    }

    /**
     * Clears all the WizardModel data
     */
    public void clearWizardData() {

        try {
            selectContext(WIZARD_CURR_CONTEXT);
            clear();
            selectContext(WIZARD_DEFAULT_CONTEXT);
            clear();
            selectContext(WIZARD_PREV_CONTEXT);
            clear();
        } catch (InvalidContextException e) {
            // This should never fail
        }

        // Restore the default context.
        try {
            selectContext(DEFAULT_CONTEXT_NAME);
        } catch (InvalidContextException e) {
            // never fails.
        }
    }

    /**
     * Get the WizardValue stored for a particular fieldName or Key The Key is
     * searched in the following order : 1. CurrentContext 2. Check for the key
     * in the WizardModel derived from the State file 3. Previous Context 4.
     * Default Context
     *
     * @param  fieldName  or Key to search
     *
     * @return  Value for the Key, if key not found returns null
     */
    public Object getWizardValue(String fieldName) {

        // Remember the current context
        String currentContext = getCurrentContextName();

        // Set the model to retrieve data from the WIZARD_CURR_CONTEXT
        // in the model
        //
        try {
            selectContext(WIZARD_CURR_CONTEXT);
        } catch (InvalidContextException e) {
            // this means that the wizard has not set any
            // This should never fail
        }

        Object obj = getValue(fieldName);

        // Try to get the value from the wizardModel read from the
        // StateFile.
        if ((obj == null) && (prevWizardModel != null)) {

            try {
                obj = prevWizardModel.getWizardValue(fieldName);
            } catch (Exception e) {
                // Some problem in retriving the value from the
                // state file
            }
        }

        // Restore the previous context.
        try {
            selectContext(WIZARD_PREV_CONTEXT);
        } catch (InvalidContextException e) {
            // Never happens
        }

        if (obj == null) {
            obj = getValue(fieldName);
        }

        // Restore the default context.
        try {
            selectContext(WIZARD_DEFAULT_CONTEXT);
        } catch (InvalidContextException e) {
            // never fails.
        }

        // If there is no wizard value return the default
        // context value
        //
        if (obj == null) {
            obj = getValue(fieldName);
        }

        // Restore the original context
        //
        try {
            selectContext(currentContext);
        } catch (InvalidContextException e) {
            // this means that the wizard has not set any
            // values in the model
            //
            // Should never fail.
        }

        return obj;
    }

    /**
     * Set the WizardValue for a particular fieldName or Key in the current
     * context
     *
     * @param  fieldName  or Key to search
     * @param  value  Value for the Key
     */
    public void setWizardValue(String fieldName, Object value) {

        // Remember the current context
        String currentContext = getCurrentContextName();

        // Set the model to store data to the WIZARD_CURR_CONTEXT
        // in the model
        //
        try {
            selectContext(WIZARD_CURR_CONTEXT);
        } catch (InvalidContextException e) {
            // this means that the wizard has not set any
            // This should never fail
        }

        setValue(fieldName, value);

        // Restore the original context
        //
        try {
            selectContext(currentContext);
        } catch (InvalidContextException e) {
            // this means that the wizard has not set any
            // values in the model
            //
            // Should never fail.
        }
    }

    /**
     * Get the wizardValue stored for a particular key in the prev context
     *
     * @param  fieldName  FieldName or Key to search
     *
     * @return  Object value for the key, null if key is not found in the prev
     * context
     */
    public Object getWizardPrevValue(String fieldName) {

        // Remember the current context
        String currentContext = getCurrentContextName();

        // Set the model to retrieve data from the WIZARD_CURR_CONTEXT
        // in the model
        try {
            selectContext(WIZARD_PREV_CONTEXT);
        } catch (InvalidContextException e) {
            // this means that the wizard has not set any
            // This should never fail
        }

        Object obj = getValue(fieldName);

        // Restore the default context.
        try {
            selectContext(WIZARD_DEFAULT_CONTEXT);
        } catch (InvalidContextException e) {
            // never fails.
        }

        // If there is no wizard value return the default
        // context value
        //
        if (obj == null) {
            obj = getValue(fieldName);
        }

        // Restore the original context
        //
        try {
            selectContext(currentContext);
        } catch (InvalidContextException e) {
            // this means that the wizard has not set any
            // values in the model
            //
            // Should never fail.
        }

        return obj;
    }

    /**
     * Get the wizardValue stored for a particular key in the current context
     *
     * @param  fieldName  FieldName or Key to search
     *
     * @return  Object value for the key, null if key is not found in the
     * default context
     */
    public Object getWizardCurrValue(String fieldName) {

        // Remember the current context
        String currentContext = getCurrentContextName();

        // Set the model to retrieve data from the WIZARD_CURR_CONTEXT
        // in the model
        //
        try {
            selectContext(WIZARD_CURR_CONTEXT);
        } catch (InvalidContextException e) {
            // This should never fail
        }

        Object obj = getValue(fieldName);

        // Restore the original context
        //
        try {
            selectContext(currentContext);
        } catch (InvalidContextException e) {
            // Should never fail.
        }

        return obj;
    }

    /**
     * Get the wizardValue stored for a particular key in the default context
     *
     * @param  fieldName  FieldName or Key to search
     *
     * @return  Object value for the key, null if key is not found in the
     * default context
     */
    public Object getWizardDefaultValue(String fieldName) {

        // Remember the current context
        String currentContext = getCurrentContextName();

        // Set the model to retrieve data from the WIZARD_CURR_CONTEXT
        // in the model
        //
        try {
            selectContext(WIZARD_DEFAULT_CONTEXT);
        } catch (InvalidContextException e) {
            // this means that the wizard has not set any
            // This should never fail
        }

        Object obj = getValue(fieldName);

        // Restore the default context.
        try {
            selectContext(DEFAULT_CONTEXT_NAME);
        } catch (InvalidContextException e) {
            // never fails.
        }

        // If there is no wizard value return the default
        // context value
        //
        if (obj == null) {
            obj = getValue(fieldName);
        }

        // Restore the original context
        //
        try {
            selectContext(currentContext);
        } catch (InvalidContextException e) {
            // this means that the wizard has not set any
            // values in the model
            //
            // Should never fail.
        }

        return obj;
    }

    /**
     * Set the wizardValue for a particular key in the prev context
     *
     * @param  fieldName  FieldName or Key to search
     * @param  Object  value for the key
     */
    public void setWizardPrevValue(String fieldName, Object value) {

        // Remember the current context
        String currentContext = getCurrentContextName();

        // Set the model to store data to the WIZARD_CURR_CONTEXT
        // in the model
        //
        try {
            selectContext(WIZARD_PREV_CONTEXT);
        } catch (InvalidContextException e) {
            // this means that the wizard has not set any
            // This should never fail
        }

        setValue(fieldName, value);

        // Restore the original context
        //
        try {
            selectContext(currentContext);
        } catch (InvalidContextException e) {
            // this means that the wizard has not set any
            // values in the model
            //
            // Should never fail.
        }
    }

    /**
     * Set the wizardValue for a particular key in the default context
     *
     * @param  fieldName  FieldName or Key to search
     * @param  Object  value for the key
     */
    public void setWizardDefaultValue(String fieldName, Object value) {

        // Remember the current context
        String currentContext = getCurrentContextName();

        // Set the model to store data to the WIZARD_CURR_CONTEXT
        // in the model
        //
        try {
            selectContext(WIZARD_DEFAULT_CONTEXT);
        } catch (InvalidContextException e) {
            // this means that the wizard has not set any
            // This should never fail
        }

        setValue(fieldName, value);

        // Restore the original context
        //
        try {
            selectContext(currentContext);
        } catch (InvalidContextException e) {
            // this means that the wizard has not set any
            // values in the model
            //
            // Should never fail.
        }
    }

    /**
     * Write the current wizardModel contents into the stateFile with
     * MBeanServer
     */
    public void writeToStateFile(MBeanServerConnection mbsc) {

        try {
            wizardStateManager.writeCurrentContext(this, mbsc);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    /**
     * Set the StateManager for the WizardModel NOTE : This would overwrite the
     * PrevWizardValues according to the new StateManager.
     */
    public void setWizardStateManager(WizardStateManager stateManager) {
        wizardStateManager = stateManager;

        // Get the Previous WizardModel from the StateFile
        try {
            prevWizardModel = (DSConfigWizardModel)
		wizardStateManager.readPreviousContext(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * get Map of all the values in the model
     *
     * @return  Map
     */
    public Map getValueMap() {
        return super.getValueMap();
    }

    public String toString() {
	return ((super.getValueMap()).toString());
    }
	
}
