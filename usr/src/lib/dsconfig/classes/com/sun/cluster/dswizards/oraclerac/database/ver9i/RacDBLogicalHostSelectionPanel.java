/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)RacDBLogicalHostSelectionPanel.java	1.19	09/03/11 SMI"
 */


package com.sun.cluster.dswizards.oraclerac.database.ver9i;

// CMASS
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.rgm.ResourceData;

// Wizard CLI
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.common.CliUtil;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.ResourcePropertyUtil;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.logicalhost.LogicalHostWizardConstants;
import com.sun.cluster.dswizards.oraclerac.database.*;
import com.sun.cluster.dswizards.oraclerac.invoker.RACInvokerCreator;

// Model classes
import com.sun.cluster.model.RACModel;

// Java
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

/**
 * The Panel Class that gets a Hostname or list of Hostnames as input from the
 * User.
 */
public class RacDBLogicalHostSelectionPanel extends RacDBBasePanel {

    /**
     * Creates a HostnameEntry Panel with the given name and Wizard state
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     */
    public RacDBLogicalHostSelectionPanel(String name,
        CliDSConfigWizardModel model, WizardFlow wizardFlow,
        WizardI18N wizardi18n) {
        super(name, model, wizardFlow, wizardi18n);
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.
     *		WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        String cchar = wizardi18n.getString("ttydisplay.c.char");
        String createnew_txt = wizardi18n.getString(
                "ttydisplay.menu.create.new");
        String rchar = wizardi18n.getString("ttydisplay.r.char");
        String refresh = wizardi18n.getString("ttydisplay.menu.refresh");
        String confirm_txt = wizardi18n.getString("cliwizards.confirmMessage");
        String heading1 = wizardi18n.getString(
                "logicalhostSelectionPanel.table.colheader.0");
        String heading2 = wizardi18n.getString(
                "logicalhostSelectionPanel.table.colheader.1");
        String help_text = wizardi18n.getString(
                "oraclerac.logicalhostSelectionPanel.cli.help");

        String curNode = (String) wizardModel.getWizardValue(
                Util.RAC_CURNODENAME);
        String nodeList[] = (String[]) wizardModel.getValue(Util.RG_NODELIST);

        if (curNode == null) {
            curNode = nodeList[0];
            wizardModel.setWizardValue(Util.RAC_NODEIDX, new Integer(0));
            wizardModel.setWizardValue(Util.RAC_CURNODENAME, curNode);
            wizardModel.setWizardValue(Util.RAC_NODELIST_COMPLETE,
                Boolean.FALSE);
        }

        String tmpArry[] = curNode.split(Util.COLON);
        curNode = tmpArry[0];

        title = wizardi18n.getString(
                "oraclerac.logicalhostSelectionPanel.title",
                new String[] { curNode });
        desc = wizardi18n.getString("oraclerac.logicalhostSelectionPanel.desc",
                new String[] { curNode });

        String emptyDesc = wizardi18n.getString(
                "oraclerac.logicalhostSelectionPanel.emptytable.desc",
                new String[] { curNode });
        String lh_confirm_txt = wizardi18n.getString(
                "oraclerac.logicalhostSelectionPanel.confirm",
                new String[] { curNode });
        Vector table = new Vector();
        List resources = null;
        refreshResourceList();
        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        String rgName = null;

        wizardModel.setWizardValue(CREATE_LH_RS, "");

        boolean createnew_choice = false;
        String option = "";

        while (!createnew_choice && !option.equals(yes)) {

            // Title
            TTYDisplay.printTitle(title);


            table.removeAllElements();

            resources = (List) wizardModel.getWizardValue(Util.LH_RSLIST);

            HashMap customTags = new HashMap();
            table = populateTable(resources);

            // Remove previous selections from the list of options.
            Integer iter = (Integer) wizardModel.getWizardValue(
                    Util.RAC_NODEIDX);

            for (int i = 0; i < iter.intValue(); i++) {
                int idx;
                String nodeName = nodeList[i];
                Vector lh_forNode = (Vector) wizardModel.getWizardValue(
                        SEL_LH_RS + nodeName);

                if (lh_forNode != null) {

                    for (int j = 0; j < lh_forNode.size(); j++) {
                        String lhname = (String) lh_forNode.get(j);

                        if ((lhname != null) && table.contains(lhname)) {
                            idx = table.indexOf(lhname);

                            // Remove the entry and its RG name
                            // The first remove automatically adjusts the
                            // indexes
                            // so the second time the index need not be
                            // incremented.
                            table.remove(idx);
                            table.remove(idx);
                        }
                    }
                }

                String lhRg_forNode = (String) wizardModel.getWizardValue(
                        SEL_LH_RG + nodeName);

                if (lhRg_forNode != null) {

                    while (table.contains(lhRg_forNode)) {
                        idx = table.indexOf(lhRg_forNode);

                        // Remove the entry and its RG name
                        // The first remove automatically adjusts the indexes
                        // so the second time the index need not be incremented.
                        table.remove(idx - 1);
                        table.remove(idx - 1);
                    }
                }
            }

            customTags.put(cchar, createnew_txt);
            customTags.put(rchar, refresh);


            Vector lhRsSelections = (Vector) wizardModel.getWizardValue(
                    Util.SEL_LH_RS + curNode);
            String lhSelections[] = null;

            if (lhRsSelections != null) {
                lhSelections = (String[]) lhRsSelections.toArray(new String[0]);
            }

            String hostNameList[] = (String[]) wizardModel.getWizardValue(
                    Util.HOSTNAMELIST);

            String allSelections[] = null;

            if (lhSelections != null) {

                if (hostNameList != null) {
                    allSelections = ResourcePropertyUtil.concatenateArrays(
                            lhSelections, hostNameList);
                } else {
                    allSelections = lhSelections;
                }
            } else {

                if (hostNameList != null) {
                    allSelections = hostNameList;
                }
            }

            StringBuffer commaSeparatedSelections = new StringBuffer();

            if (allSelections != null) {

                for (int i = 0; i < allSelections.length; i++) {
                    commaSeparatedSelections.append(allSelections[i]);
                    table.add(allSelections[i]);
                    table.add(Util.NEW);

                    if ((commaSeparatedSelections.length() > 0) &&
                            (i != allSelections.length)) {
                        commaSeparatedSelections.append(Util.COMMA);
                    }
                }
            }

            List selections = new ArrayList();
            Vector selectedResources = new Vector();
            String selectedList[] = null;

            if (table.size() > 0) {

                // Description Para
                TTYDisplay.printSubTitle(desc);

                String commaSeparatedSelections_str = commaSeparatedSelections
                    .toString();

                String subheadings[] = new String[] { heading1, heading2 };
                String userInput = TTYDisplay.create2DTable("", subheadings,
                        table, customTags, help_text);

                if (userInput.equals(
                            wizardi18n.getString("ttydisplay.back.char"))) {
                    this.cancelDirection = true;

                    return;
                }

                if (userInput.equals(rchar)) {

                    // refresh the resource list
                    refreshResourceList();

                    continue;
                }

                if (userInput.equals(cchar)) {

                    // user chose to create logical host resource
                    // handle create new
                    wizardModel.setWizardValue(CREATE_LH_RS, CREATE_LH_RS);

                    break;
                }

                int num = Integer.valueOf(userInput).intValue();

                // In the table vector, the data is organised as resource name
                // followed by the RG names. Given a user selection from the
                // display table, the formula to get the resource name is
                // i = (num - 1) * 2
                String userSel = (String) table.elementAt((num - 1) * 2);
                selections.add(userSel);

                TTYDisplay.setNavEnabled(true);
                this.cancelDirection = false;

                //
                // remove all customTags from tmp_selections
                List tmp_selections = new ArrayList(selections);

                // Remove newly create hostnames. They are handled seperatly
                if (hostNameList != null) {

                    for (int y = 0; y < hostNameList.length; y++) {
                        tmp_selections.remove(hostNameList[y]);
                    }
                }

                selectedList = (String[]) tmp_selections.toArray(new String[0]);

                TTYDisplay.setNavEnabled(true);
                this.cancelDirection = false;

                // validateInput. Make sure the RGs are the same for the
                // selected resources
                // get index of RSName
                int rs_index1 = -1;
                int rs_index2 = -1;
                String rgName1 = null;
                String rgName2 = null;

                boolean error = false;
                int i1 = 0;

                while ((i1 < (selectedList.length - 1)) && !error) {

                    if (table.contains(selectedList[i1])) {

                        if (!selectedResources.contains(selectedList[i1])) {
                            selectedResources.add(selectedList[i1]);
                        }

                        rs_index1 = table.indexOf(selectedList[i1]);
                        rgName1 = (String) table.elementAt(rs_index1 + 1);
                    }

                    for (int i2 = (i1 + 1); i2 < selectedList.length; i2++) {

                        if (table.contains(selectedList[i2])) {

                            if (!selectedResources.contains(selectedList[i2])) {
                                selectedResources.add(selectedList[i2]);
                            }

                            rs_index2 = table.indexOf(selectedList[i2]);
                            rgName2 = (String) table.elementAt(rs_index2 + 1);
                        }

                        if ((rgName1 != null) && (rgName2 != null)) {

                            if (!rgName1.equals(rgName2)) {
                                error = true;
                            }
                        }
                    }

                    if (!error) {
                        i1++;
                    }
                }

                if (selectedList.length == 1) {

                    if (table.contains(selectedList[0])) {

                        if (!selectedResources.contains(selectedList[0])) {
                            selectedResources.add(selectedList[0]);
                        }

                        rgName1 = (String) table.elementAt(table.indexOf(
                                    selectedList[0]) + 1);
                    }
                }

                rgName = (rgName1 != null) ? rgName1 : rgName2;

                if (error) {
                    String errMsg = wizardi18n.getString(
                            "haoracle.logicalhostSelectionPanel.rgmismatch.error",
                            new String[] {
                                (String) table.elementAt(rs_index1),
                                (String) table.elementAt(rs_index2)
                            });
                    TTYDisplay.printError(errMsg);
                    TTYDisplay.promptForEntry(continue_txt);

                    // Clear the selection
                    wizardModel.setWizardValue(Util.SEL_LH_RS + curNode, null);
                    wizardModel.setWizardValue(Util.SEL_LH_RG + curNode, null);

                    continue;
                }
            } else {
                TTYDisplay.printSubTitle(emptyDesc);
                TTYDisplay.promptForEntry(continue_txt);
                selections.add(createnew_txt);
            }

            if (selections.contains(createnew_txt)) {

                // user chose to create logical host resource
                // handle create new
                wizardModel.setWizardValue(CREATE_LH_RS, CREATE_LH_RS);
                createnew_choice = true;
            } else {
                wizardModel.setWizardValue(CREATE_LH_RS, "");
                option = yes;

                if (option.equalsIgnoreCase(yes)) {

                    // store selection
                    wizardModel.setWizardValue(Util.SEL_LH_RS + curNode,
                        selectedResources);
                    wizardModel.setWizardValue(Util.SEL_LH_RG + curNode,
                        rgName);

                    String newHostNames[] = (String[]) wizardModel
                        .getWizardValue(Util.HOSTNAMELIST);
                    wizardModel.setWizardValue(Util.HOSTNAMELIST + curNode,
                        newHostNames);
                    wizardModel.setWizardValue(Util.HOSTNAMELIST, null);

                    String netifList[] = (String[]) wizardModel.getWizardValue(
                            LogicalHostWizardConstants.SEL_NETIFLIST);
                    wizardModel.setWizardValue(
                        LogicalHostWizardConstants.SEL_NETIFLIST + curNode,
                        netifList);
                    wizardModel.setWizardValue(
                        LogicalHostWizardConstants.SEL_NETIFLIST, null);

                    Integer nodeIndex = (Integer) wizardModel.getWizardValue(
                            Util.RAC_NODEIDX);
                    int nodeIdx = nodeIndex.intValue();

                    if (++nodeIdx < nodeList.length) {
                        curNode = nodeList[nodeIdx];
                        wizardModel.setWizardValue(Util.RAC_CURNODENAME,
                            curNode);
                        wizardModel.setWizardValue(Util.RAC_NODEIDX,
                            new Integer(nodeIdx));
                        wizardModel.setWizardValue(Util.RAC_NODELIST_COMPLETE,
                            Boolean.FALSE);
                    } else {
                        wizardModel.setWizardValue(Util.RAC_NODELIST_COMPLETE,
                            Boolean.TRUE);
                        wizardModel.setWizardValue(Util.RAC_CURNODENAME, null);
                        wizardModel.setWizardValue(Util.RAC_NODEIDX, null);
                    }
                } else {

                    // Clear the selection
                    wizardModel.setWizardValue(Util.SEL_LH_RS + curNode, null);
                    wizardModel.setWizardValue(Util.SEL_LH_RG + curNode, null);
                }

            }
        }
    }

    private void refreshResourceList() {
        String nodeList[] = (String[]) wizardModel.getWizardValue(
                Util.RG_NODELIST);
        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);

	RACModel racModel = RACInvokerCreator.getRACModel();

        HashMap helperData = new HashMap();

        // construct a query and fetch resources
        helperData.put(Util.RG_NODELIST, nodeList);

        String selZoneCluster = CliUtil.getZoneClusterName(wizardModel);
	helperData.put(Util.SEL_ZONE_CLUSTER, selZoneCluster);

        HashMap map = racModel.discoverPossibilities(
	    null, nodeEndPoint, new String[] { Util.LH_RSLIST }, helperData);
        List resources = (List) map.get(Util.LH_RSLIST);
        wizardModel.setWizardValue(Util.LH_RSLIST, resources);
    }

    private Vector populateTable(List list) {
        Vector table = new Vector();
        if (list != null) {
            int size = list.size();

            for (int i = 0; i < size; i++) {
		ResourceData rsData = (ResourceData)list.get(i);

                if (rsData != null) {
                    table.add(rsData.getName());
                    table.add(rsData.getResourceGroupName());
                }
            }
        }

        return table;
    }

}
