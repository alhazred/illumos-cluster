/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)StorageSelectionPanel.java 1.16     08/07/23 SMI"
 */

package com.sun.cluster.dswizards.common;

// J2SE
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import java.util.Iterator;

// JATO
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.model.ModelControlException;
import com.iplanet.jato.view.ContainerView;
import com.iplanet.jato.view.DisplayField;
import com.iplanet.jato.view.RequestHandlingTiledViewBase;
import com.iplanet.jato.view.RequestHandlingViewBase;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.event.DisplayEvent;

// CACAO Agent
import com.sun.cluster.agent.dataservices.utils.InfrastructureMBean;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.dataservices.hastorageplus.HAStoragePlusMBean;
import com.sun.cluster.common.MBeanModel;

// CLI wizard sdk
import com.sun.cluster.dswizards.common.DSConfigWizardModel;
import com.sun.cluster.dswizards.common.GenericDSWizard;
import com.sun.cluster.dswizards.common.ResourcePropertyUtil;
import com.sun.cluster.dswizards.common.WizardI18N;

// CLI Wizard
import com.sun.cluster.dswizards.hastorageplus.HASPWizardConstants;

// SPM
import com.sun.cluster.spm.common.GenericWizard;
import com.sun.cluster.spm.common.SpmUtil;
import com.sun.cluster.spm.rgm.ResourceGroupUtil;
import com.sun.cluster.spm.task.wizard.ConfigureDataServiceWizard;

// Lockharts
import com.sun.web.ui.common.CCI18N;
import com.sun.web.ui.model.CCActionTableModel;
import com.sun.web.ui.model.CCActionTableModelInterface;
import com.sun.web.ui.model.CCAddRemoveModel;
import com.sun.web.ui.model.CCPropertySheetModel;
import com.sun.web.ui.model.wizard.WizardEvent;
import com.sun.web.ui.view.alert.CCAlertInline;
import com.sun.web.ui.view.html.CCHiddenField;
import com.sun.web.ui.view.html.CCRadioButton;
import com.sun.web.ui.view.html.CCTextField;
import com.sun.web.ui.view.propertysheet.CCPropertySheet;
import com.sun.web.ui.view.table.CCActionTable;
import com.sun.web.ui.view.wizard.CCWizardPage;

/**
 * This panel handles selection of storage resources and creation of a new
 * storage resource(either HASP or QFS resource)
 */
public class StorageSelectionPanel extends RequestHandlingViewBase
    implements CCWizardPage {

    /** Wizard step's title dispayed first on the right */
    public static final String STEP_TITLE = "storageSelectionPanel.title";

    /** Wizard step's name displayed on the left */
    public static final String STEP_TEXT = "storageSelectionPanel.title";

    /** Prefix to retrieve the wizard's step contextal help */
    public static final String STEP_HELP_PREFIX = "storageSelectionPanel.help";

    /** JSP file used for the wizard's step */
    public static final String PAGELET_URL =
        "/jsp/task/wizard/TaskWizardStep.jsp";

    /** Child element for the hidden field */
    public static final String CHILD_CLOSE = "CloseWizardHiddenField";

    /** Child element to display the Alert Inline */
    public static final String CHILD_ALERT = "Alert";

    /** Name of the property sheet child */
    public static final String CHILD_PROPERTYSHEET = "PropertySheet";

    /** XML file containing the property sheet's model */
    public static final String PROPERTYSHEET_XML_FILE =
        GenericDSWizard.COMMON_XML_PATH + "storageSelectionPanel.xml";

    /** XML file containing the property sheet table model */
    public static final String HASP_RS_TABLE_XML =
        GenericDSWizard.COMMON_XML_PATH + "hasprsNameTable.xml";
    protected static final String SELECT_EXISTING = "1";
    protected static final String CREATE_NEW = "2";
    protected static final String FS_MTPTS = "fs_mtpts";

    public static final String CHILD_HASP_RS_TABLE = "HASPRSNameTable";
    public static final String CHILD_HASP_RS_TABLE_VIEW =
        "HASPRSNameTableTiledView";
    public static final String CHILD_NEWEXISTS_VALUE = "Choice";
    public static final String HASP_FS_TABLE_DATA = "HaspFSTableData";
    public static final String HASP_GD_TABLE_DATA = "HaspGDTableData";
    private static final int TABLE_NO_OF_COLUMNS = 4;
    private static final String TABLE_I18N_HEADER_PREFIX =
        "storageSelectionPanel.table.colheader";
    private static final String TABLE_HEADER_PREFIX = "HaspCol";
    private static final String TABLE_COLUMN_HASP_FS = "haspFileSystem";
    private static final String TABLE_COLUMN_HASP_GLOBALDEV =
        "haspGlobalDevicePaths";
    private static final String TABLE_COLUMN_RS_NAME = "rsname";
    private static final String TABLE_COLUMN_RG_NAME = "rgname";

    private static final String HASPRS_SELECTED_INT = "HASPRS_SELECTED_INT";


    // Property Sheet Model
    private CCPropertySheetModel propertySheetModel = null;

    protected DSConfigWizardModel wizardModel = null;
    protected InfrastructureMBean mbean = null;
    protected boolean enableMulipleSelection = true;

    // Table model
    private CCActionTableModel haspRSTableModel = null;

    /**
     * Construct an instance with the specified model and logical page name.
     *
     * @param  parent  The parent view of this object.
     * @param  model  This view's model.
     * @param  name  Logical page name.
     */
    public StorageSelectionPanel(View parent, Model model, String name) {
        super(parent, name);
        setDefaultModel(model);
        wizardModel = (DSConfigWizardModel) getDefaultModel();
        createPropertySheetModel();
        registerChildren();
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Child manipulation methods
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    protected void registerChildren() {
        registerChild(CHILD_CLOSE, CCHiddenField.class);
        registerChild(CHILD_ALERT, CCAlertInline.class);
        registerChild(CHILD_PROPERTYSHEET, CCPropertySheet.class);
        registerChild(CHILD_HASP_RS_TABLE_VIEW, HASPRSTableTiledView.class);
        propertySheetModel.registerChildren(this);
        haspRSTableModel.registerChildren(this);
    }

    protected View createChild(String name) {

        if (name.equals(CHILD_PROPERTYSHEET)) {
            CCPropertySheet child = new CCPropertySheet(this,
                    propertySheetModel, name);

            return child;

        } else if ((haspRSTableModel != null) &&
                name.equals(CHILD_HASP_RS_TABLE_VIEW)) {
            HASPRSTableTiledView child = new HASPRSTableTiledView(this,
                    haspRSTableModel, name);

            return child;

        } else if (name.equals(CHILD_CLOSE)) {
            CCHiddenField child = new CCHiddenField(this, name, null);

            return child;

        } else if (name.equals(CHILD_ALERT)) {
            CCAlertInline child = new CCAlertInline(this, name, null);

            return child;

        } else if ((propertySheetModel != null) &&
                propertySheetModel.isChildSupported(name)) {

            // Create child from property sheet model.
            View child = propertySheetModel.createChild(this, name);

            if (name.equals(CHILD_HASP_RS_TABLE)) {
                ((CCActionTable) child).setTiledView((ContainerView) getChild(
                        CHILD_HASP_RS_TABLE_VIEW));
            }

            return child;

        } else {
            throw new IllegalArgumentException(
                " StorageSelectionPanel Invalid child name [" + name + "]");
        }
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Display methods
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    private void createPropertySheetModel() {

        try {
            FileInputStream fileInputStream = new FileInputStream(
                    PROPERTYSHEET_XML_FILE);
            String xmlFileString = ConfigureDataServiceWizard.getXMLString(
                    fileInputStream);
            fileInputStream.close();

            propertySheetModel = new CCPropertySheetModel();
            propertySheetModel.setDocument(xmlFileString);

            CCActionTableModel model = (CCActionTableModel) wizardModel
                .getWizardValue(CHILD_HASP_RS_TABLE);

            // Whenever a new row is added to the table that already contains
            // existing rows, the action table model is recreated. Without
            // recreation the new rows are not added to the table.

            String globalDeviceSelections[] = (String[]) wizardModel
                .getWizardValue(HASPWizardConstants.HASP_DEVICES_SELECTED);
            String filesystemSelections[] = (String[]) wizardModel
                .getWizardValue(HASPWizardConstants.HASP_FILESYSTEMS_SELECTED);

            if ((model == null) || (globalDeviceSelections != null) ||
                    (filesystemSelections != null)) {
                FileInputStream tableModelStream = new FileInputStream(
                        HASP_RS_TABLE_XML);
                String tableModelString = ConfigureDataServiceWizard
                    .getXMLString(tableModelStream);
                tableModelStream.close();
                haspRSTableModel = new CCActionTableModel();
                haspRSTableModel.setDocument(tableModelString);

                for (int i = 0; i < TABLE_NO_OF_COLUMNS; i++) {
                    haspRSTableModel.setActionValue(TABLE_HEADER_PREFIX + i,
                        TABLE_I18N_HEADER_PREFIX + "." + i);
                }
            } else {
                haspRSTableModel = model;
            }

            propertySheetModel.setModel(CHILD_HASP_RS_TABLE, haspRSTableModel);

        } catch (Exception e) {
            e.printStackTrace();

            return;
        }
    }

    private void populatePropertySheetModel() {
        Object tmpObj = null;
        int i;

        try {
            CCActionTable haspRSTableChild = (CCActionTable) getChild(
                    CHILD_HASP_RS_TABLE);
            CCActionTableModel model = (CCActionTableModel) wizardModel
                .getWizardValue(CHILD_HASP_RS_TABLE);

            haspRSTableChild.resetStateData();

            if ((model != null) && (model != haspRSTableModel)) {
                haspRSTableModel.clear();

                for (i = 0; i < model.getNumRows(); i++) {
                    model.setRowIndex(i);
                    haspRSTableModel.appendRow();
                    haspRSTableModel.setValue(TABLE_COLUMN_HASP_FS,
                        model.getValue(TABLE_COLUMN_HASP_FS));
                    haspRSTableModel.setValue(TABLE_COLUMN_HASP_GLOBALDEV,
                        model.getValue(TABLE_COLUMN_HASP_GLOBALDEV));
                    haspRSTableModel.setValue(TABLE_COLUMN_RS_NAME,
                        model.getValue(TABLE_COLUMN_RS_NAME));
                    haspRSTableModel.setValue(TABLE_COLUMN_RG_NAME,
                        model.getValue(TABLE_COLUMN_RG_NAME));
                }
            }

            if (model == null) {
                refreshRGList();

                List allStorageRG =
                    (List) wizardModel.getWizardValue(Util.STORAGE_RGLIST);

                // Fill table model from discoverd storage resource names
                if (allStorageRG != null) {
                    int size = allStorageRG.size();

                    for (i = 0; i < size; i++) {
                        haspRSTableModel.appendRow();
                        haspRSTableModel.setValue(TABLE_COLUMN_HASP_GLOBALDEV,
                            "");

                        HashMap map = (HashMap) allStorageRG.get(i);
                        String rgName = (String) map.get(Util.RG_NAME);
                        String rsName = (String) map.get(Util.RS_NAME);
                        haspRSTableModel.setValue(TABLE_COLUMN_RS_NAME, rsName);
                        haspRSTableModel.setValue(TABLE_COLUMN_RG_NAME, rgName);

                        String filesystemProp[] = (String[]) map.get(
                                Util.HASP_ALL_FILESYSTEMS);
                        String devPathProp[] = (String[]) map.get(
                                Util.HASP_ALL_DEVICES);

                        if (filesystemProp == null && devPathProp == null) {
                            // it must be QFS resource
                            filesystemProp = (String[]) map.get(
                                Util.QFS_FILESYSTEM);
                        }

                        if (filesystemProp != null) {
                            StringBuffer definedFilesystems = null;

                            for (int j = 0; j < filesystemProp.length; j++) {

                                if (definedFilesystems == null) {
                                    definedFilesystems = new StringBuffer();
                                    definedFilesystems.append(
                                        filesystemProp[j]);
                                } else {
                                    definedFilesystems.append(Util.COMMA);
                                    definedFilesystems.append(
                                        filesystemProp[j]);
                                }
                            }

                            if (definedFilesystems != null) {
                                String definedFSStr = definedFilesystems
                                    .toString();
                                haspRSTableModel.setValue(TABLE_COLUMN_HASP_FS,
                                    definedFSStr);
                            }
                        }

                        if (devPathProp != null) {
                            StringBuffer definedDevices = null;

                            for (int j = 0; j < devPathProp.length; j++) {

                                if (definedDevices == null) {
                                    definedDevices = new StringBuffer();
                                    definedDevices.append(devPathProp[j]);
                                } else {
                                    definedDevices.append(Util.COMMA);
                                    definedDevices.append(devPathProp[j]);
                                }
                            }

                            if (definedDevices != null) {
                                String definedDevStr = definedDevices
                                    .toString();
                                haspRSTableModel.setValue(
                                    TABLE_COLUMN_HASP_GLOBALDEV, definedDevStr);
                            }
                        }
                    }
                }
            }

            // default selections can also be obtained from
            // HASP_FILESYSTEMS_SELECTED
            String filesystemSelections[] = (String[]) wizardModel
                .getWizardValue(HASPWizardConstants.HASP_FILESYSTEMS_SELECTED);

            String globalDeviceSelections[] = (String[]) wizardModel
                .getWizardValue(HASPWizardConstants.HASP_DEVICES_SELECTED);
            WizardI18N wizardi18n = GenericDSWizard.getWizardI18N();
            StringBuffer alertString = null;

            // Handle Duplicate values
            Set oldTableData = new HashSet();
            Set newTableData = new HashSet();

            if (((filesystemSelections != null) &&
                        (filesystemSelections.length != 0))) {
                oldTableData = (HashSet) wizardModel.getWizardValue(
                        HASP_FS_TABLE_DATA);
                newTableData.addAll(Arrays.asList(filesystemSelections));

                if (oldTableData != null) {

                    newTableData.removeAll(oldTableData);
                    oldTableData.addAll(newTableData);
                    filesystemSelections = (String[]) newTableData.toArray(
                            new String[0]);
                    wizardModel.setWizardValue(HASP_FS_TABLE_DATA,
                        oldTableData);
                } else {
                    wizardModel.setWizardValue(HASP_FS_TABLE_DATA,
                        newTableData);
                }
            }

            newTableData = new HashSet();

            if (((globalDeviceSelections != null) &&
                        (globalDeviceSelections.length != 0))) {
                oldTableData = (HashSet) wizardModel.getWizardValue(
                        HASP_GD_TABLE_DATA);
                newTableData.addAll(Arrays.asList(globalDeviceSelections));

                if (oldTableData != null) {

                    newTableData.removeAll(oldTableData);
                    oldTableData.addAll(newTableData);
                    globalDeviceSelections = (String[]) newTableData.toArray(
                            new String[0]);
                    wizardModel.setWizardValue(HASP_GD_TABLE_DATA,
                        oldTableData);
                } else {
                    wizardModel.setWizardValue(HASP_GD_TABLE_DATA,
                        newTableData);
                }
            }

            // Populate the table with HASP_FILESYSTEMS_SELECTED
            if (((filesystemSelections != null) &&
                        (filesystemSelections.length != 0)) ||
                    ((globalDeviceSelections != null) &&
                        (globalDeviceSelections.length != 0))) {
                haspRSTableModel.appendRow();
                haspRSTableModel.setValue(TABLE_COLUMN_RS_NAME, "");
                haspRSTableModel.setValue(TABLE_COLUMN_RG_NAME, "");
                haspRSTableModel.setValue(TABLE_COLUMN_HASP_GLOBALDEV, "");
                haspRSTableModel.setValue(TABLE_COLUMN_HASP_FS, "");

                if (filesystemSelections != null) {
                    StringBuffer definedFilesystems = null;

                    for (i = 0; i < filesystemSelections.length; i++) {

                        if (definedFilesystems == null) {
                            definedFilesystems = new StringBuffer();
                            definedFilesystems.append(filesystemSelections[i]);
                        } else {
                            definedFilesystems.append(Util.COMMA);
                            definedFilesystems.append(filesystemSelections[i]);
                        }
                    }

                    if (definedFilesystems != null) {
                        String definedFSStr = definedFilesystems.toString();
                        haspRSTableModel.setValue(TABLE_COLUMN_HASP_FS,
                            definedFSStr);

                        if (alertString == null) {
                            alertString = new StringBuffer();
                            alertString.append(definedFSStr);
                        } else {
                            alertString.append(Util.COMMA);
                            alertString.append(definedFSStr);
                        }
                    }

                    wizardModel.setWizardValue(
                        HASPWizardConstants.HASP_FILESYSTEMS_SELECTED, null);
                }

                if (globalDeviceSelections != null) {
                    StringBuffer definedDevices = null;

                    for (i = 0; i < globalDeviceSelections.length; i++) {

                        if (definedDevices == null) {
                            definedDevices = new StringBuffer();
                            definedDevices.append(globalDeviceSelections[i]);
                        } else {
                            definedDevices.append(Util.COMMA);
                            definedDevices.append(globalDeviceSelections[i]);
                        }
                    }

                    if (definedDevices != null) {
                        String definedDevStr = definedDevices.toString();
                        haspRSTableModel.setValue(TABLE_COLUMN_HASP_GLOBALDEV,
                            definedDevStr);

                        if (alertString == null) {
                            alertString = new StringBuffer();
                            alertString.append(definedDevStr);
                        } else {
                            alertString.append(Util.COMMA);
                            alertString.append(definedDevStr);
                        }
                    }

                    wizardModel.setWizardValue(
                        HASPWizardConstants.HASP_DEVICES_SELECTED, null);
                }

                haspRSTableModel.setRowSelected(true);
            }


            if (alertString != null) {
                CCAlertInline alert = (CCAlertInline) getChild(CHILD_ALERT);
                alert.setValue(CCAlertInline.TYPE_INFO);

                String summary_text = wizardi18n.getString(
                        "storagepanel.alert.summary",
                        new String[] { alertString.toString() });
                alert.setSummary(summary_text);
            }

            if (!enableMulipleSelection) {
                haspRSTableModel.setSelectionType(
                    CCActionTableModelInterface.SINGLE);
            }

            haspRSTableChild.restoreStateData();
            wizardModel.setWizardValue(CHILD_HASP_RS_TABLE, haspRSTableModel);

            Integer userSelections[] = (Integer[]) wizardModel.getWizardValue(
                    HASPRS_SELECTED_INT);

            if (userSelections != null) {

                for (i = 0; i < userSelections.length; i++) {
                    haspRSTableModel.setRowSelected(userSelections[i]
                        .intValue(), true);
                }
            }

            // Set Option Button Value
            DisplayField childOptBtn = (DisplayField) getChild(
                    CHILD_NEWEXISTS_VALUE);

            if (haspRSTableModel.getNumRows() > 0) {
                childOptBtn.setValue(SELECT_EXISTING);
            } else {
                childOptBtn.setValue(CREATE_NEW);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Get the pagelet to use for the rendering of this instance.
     *
     * @return  The pagelet to use for the rendering of this instance.
     */
    public String getPageletUrl() {
        return PAGELET_URL;
    }

    /**
     * This method is called when the page is beginning its display.
     *
     * @param  event  DisplayEvent is passed in by the framework.
     */
    public void beginComponentDisplay(DisplayEvent event)
        throws ModelControlException {
        populatePropertySheetModel();

        CCHiddenField closeWizard = (CCHiddenField) getChild(CHILD_CLOSE);
        closeWizard.setValue("false");
    }

    /**
     * Step data validation is done here. This method is called from the
     * nextStep() of the wizard.
     *
     * The event error message and severity are set in case of errors.
     */
    protected HashMap getSelections() {
        ArrayList selectedResources = new ArrayList();
        ArrayList selectedRGs = new ArrayList();
        ArrayList selectedFilesystems = new ArrayList();
        ArrayList selectedGlobalDevicePaths = new ArrayList();
        ArrayList fileSysMtpts = new ArrayList();
        Integer selectedRows[] = null;
        HashMap returnValues = new HashMap();

        // If define new is selected, the table is disabled so
        // do not read the table selections
        String qtype = (String)
            ((CCRadioButton) getChild(CHILD_NEWEXISTS_VALUE)).getValue();

        if (qtype.equals(CREATE_NEW)) {
            returnValues.put(Util.CREATE_NEW, qtype);
            returnValues.put(Util.SEL_RS, null);
            returnValues.put(Util.SEL_RG, null);
            returnValues.put(Util.SEL_FILESYSTEMS, null);
            returnValues.put(Util.SEL_DEVICES, null);
            returnValues.put(FS_MTPTS, null);

            return returnValues;
        }

        try {
            CCActionTable child = (CCActionTable) getChild(CHILD_HASP_RS_TABLE);
            child.restoreStateData();
            haspRSTableModel = (CCActionTableModel) wizardModel.getWizardValue(
                    CHILD_HASP_RS_TABLE);
            selectedRows = haspRSTableModel.getSelectedRows();

            for (int i = 0; i < selectedRows.length; i++) {
                haspRSTableModel.setLocation(selectedRows[i].intValue());

                String rsName = (String) haspRSTableModel.getValue(
                        TABLE_COLUMN_RS_NAME);

                if ((rsName != null) && (rsName.trim().length() > 0)) {

                    if (!selectedResources.contains(rsName)) {
                        selectedResources.add(rsName);
                    }
                }

                String rgName = (String) haspRSTableModel.getValue(
                        TABLE_COLUMN_RG_NAME);

                if ((rgName != null) && (rgName.trim().length() > 0)) {

                    if (!selectedRGs.contains(rgName)) {
                        selectedRGs.add(rgName);
                    }
                }

                String fsMountPoint = (String) haspRSTableModel.getValue(
                        TABLE_COLUMN_HASP_FS);

                if (((rsName == null) || (rsName.trim().length() == 0)) &&
                        ((rgName == null) || (rgName.trim().length() == 0))) {
                    // implies this is a new HASP resource being created.

                    if ((fsMountPoint != null) &&
                            (fsMountPoint.trim().length() > 0)) {

                        if (!selectedFilesystems.contains(fsMountPoint)) {
                            selectedFilesystems.add(fsMountPoint);
                        }
                    }
                }

                String globalDev = (String) haspRSTableModel.getValue(
                        TABLE_COLUMN_HASP_GLOBALDEV);

                if (((rsName == null) || (rsName.trim().length() == 0)) &&
                        ((rgName == null) || (rgName.trim().length() == 0))) {

                    // implies this is a new HASP resource being created.
                    if ((globalDev != null) &&
                            (globalDev.trim().length() > 0)) {

                        if (!selectedGlobalDevicePaths.contains(globalDev)) {
                            selectedGlobalDevicePaths.add(globalDev);
                        }
                    }
                }

                // Store selected filesystem mountpoint
                if ((fsMountPoint != null) &&
                        (fsMountPoint.trim().length() > 0)) {

                    if (!fileSysMtpts.contains(fsMountPoint)) {
                        fileSysMtpts.add(fsMountPoint);
                    }
                }

            }
        } catch (Exception mce) {
            mce.printStackTrace();
        }

        int i1 = 0;
        int size = selectedRGs.size();
        boolean error = false;
        String rgName1 = null;
        String rgName2 = null;
        String rgName = null;
        int rs_index1 = -1;
        int rs_index2 = -1;

        if (size == 1) {
            rgName = (String) selectedRGs.get(0);
        } else {

            while ((i1 < (size - 1)) && !error) {
                rgName1 = (String) selectedRGs.get(i1);
                rs_index1 = i1;

                for (int i2 = (i1 + 1); i2 < size; i2++) {
                    rgName2 = (String) selectedRGs.get(i2);
                    rs_index2 = i2;

                    if ((rgName1 != null) && (rgName2 != null)) {

                        if (!rgName1.equals(rgName2)) {
                            error = true;
                        }
                    }
                }

                if (!error) {
                    i1++;
                }
            }

            rgName = (rgName1 != null) ? rgName1 : rgName2;
        }

        if (error) {
            WizardI18N wizardi18n = GenericDSWizard.getWizardI18N();
            String errMsg = wizardi18n.getString(
                    "storageSelectionPanel.rgmismatch.error",
                    new String[] {
                        (String) selectedResources.get(rs_index1),
                        (String) selectedResources.get(rs_index2)
                    });

            CCAlertInline alert = (CCAlertInline) getChild(CHILD_ALERT);
            alert.setType(CCAlertInline.TYPE_ERROR);
            alert.setSummary(wizardi18n.getString(
                    "storageSelectionPanel.rgmismatch.summary"));
            alert.setDetail(errMsg);
            wizardModel.setWizardValue(FS_MTPTS, null);

            // Clear the selection
            return null;
        }

        if (qtype.equals(SELECT_EXISTING) && (selectedRows.length == 0)) {

            // error condition. User chose select existing but did not select
            // a row
            WizardI18N wizardi18n = GenericDSWizard.getWizardI18N();
            CCAlertInline alert = (CCAlertInline) getChild(CHILD_ALERT);
            alert.setType(CCAlertInline.TYPE_ERROR);
            alert.setSummary(wizardi18n.getString(
                    "storageSelectionPanel.noselection.summary"));
            alert.setDetail(wizardi18n.getString(
                    "storageSelectionPanel.noselection.error"));

            // Clear the selection
            return null;

        }

        Vector vselectedResources = new Vector(selectedResources);
        String fs[] = null;

        if (selectedFilesystems.size() > 0) {
            fs = (String[]) selectedFilesystems.toArray(new String[0]);
        }

        String globaldev[] = null;

        if (selectedGlobalDevicePaths.size() > 0) {
            globaldev = (String[]) selectedGlobalDevicePaths.toArray(
                    new String[0]);
        }

        wizardModel.setWizardValue(HASPRS_SELECTED_INT, selectedRows);

        returnValues.put(Util.CREATE_NEW, qtype);
        returnValues.put(Util.SEL_RS, vselectedResources);
        returnValues.put(Util.SEL_RG, rgName);
        returnValues.put(Util.SEL_FILESYSTEMS, fs);
        returnValues.put(Util.SEL_DEVICES, globaldev);
        returnValues.put(FS_MTPTS, fileSysMtpts);

        return returnValues;
    }

    protected void refreshRGList() {

        if (mbean == null) {
            mbean = GenericDSWizard.getInfrastructureMBean();
        }

        HashMap helperData = new HashMap();
        String rgName = (String) wizardModel.getWizardValue(Util.RG_NAME);

        if (rgName == null) {
            String nodeList[] = (String[]) wizardModel.getWizardValue(
                    Util.NODELIST);
            helperData.put(Util.RG_NODELIST, nodeList);
        } else {
            helperData.put(Util.RG_NAME, rgName);
        }

        // returns a map that has list of resource names : rgnames
        HashMap map = mbean.discoverPossibilities(
                new String[] { Util.STORAGE_RGLIST }, helperData);
        List rgs = (List) map.get(Util.STORAGE_RGLIST);
        wizardModel.setWizardValue(Util.STORAGE_RGLIST, rgs);
    }


    public class HASPRSTableTiledView extends RequestHandlingTiledViewBase {

        private CCActionTableModel model = null;

        public HASPRSTableTiledView(View parent, CCActionTableModel model,
            String name) {
            super(parent, name);
            this.model = model;
            registerChildren();
            setPrimaryModel(model);
        }

        protected void registerChildren() {
            model.registerChildren(this);
        }

        protected View createChild(String name) {

            if (model.isChildSupported(name)) {
                View child = model.createChild(this, name);

                return child;

            } else {
                throw new IllegalArgumentException("Invalid child name [" +
                    name + "]");
            }
        }
    }

    protected String getStorageResName(String mountpoints[]) {

        String name = null;

        try {
            HAStoragePlusMBean mBean =
                (HAStoragePlusMBean) MBeanModel.getMBeanProxy(
                    getRequestContext(), 
                    Util.DOMAIN,
                    SpmUtil.getClusterEndpoint(), 
                    HAStoragePlusMBean.class,
                    null, 
                    false);
            String properties[] = { Util.RS_NAME };
            HashMap helperData = new HashMap();
            helperData.put(Util.HASP_ALL_FILESYSTEMS, mountpoints);

            HashMap discoveredMap = mBean.discoverPossibilities(properties,
                helperData);
            name = (String) discoveredMap.get(Util.RS_NAME);

        } catch (Exception e) {
            e.printStackTrace();
        }

        if ((name == null || name.equals("")) && 
                mountpoints != null) {
            name = mountpoints[0];
        }

        return name;
    }

    protected Object convertFSMtpts(Object obj) {
        ArrayList retList = new ArrayList();

        if (obj instanceof String[]) {
            String fsMtptsAr[] = (String[]) obj;

            for (int ctr1 = 0; ctr1 < fsMtptsAr.length; ctr1++) {
                String tmpStr[] = fsMtptsAr[ctr1].split(Util.COMMA);

                for (int ctr2 = 0; ctr2 < tmpStr.length; ctr2++) {
                    retList.add(tmpStr[ctr2]);
                }
            }

            return retList.toArray(new String[] {});
        } else if (obj instanceof ArrayList) {
            ArrayList fsMtptsList = (ArrayList) obj;
            Iterator iter = fsMtptsList.iterator();

            while (iter.hasNext()) {
                String tmpStr[] = ((String) iter.next()).split(Util.COMMA);

                for (int ctr2 = 0; ctr2 < tmpStr.length; ctr2++) {
                    retList.add(tmpStr[ctr2]);
                }
            }
        }

        return retList;
    }
}
