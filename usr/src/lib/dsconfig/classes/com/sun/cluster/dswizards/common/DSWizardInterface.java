/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)DSWizardInterface.java 1.7     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.common;

/**
 * Interface that every Panel or Page in the Wizard Should implement.
 */
public interface DSWizardInterface {

    /**
     * Specifies the Next Panel to be displayed from a Current Panel from a set
     * of possible Panels, pertaining to the current Wizard Context.
     *
     * @param  curPanelName  Name of the Current Panel
     * @param  wizCxt  WizardContext that helps in taking the decision
     * @param  options  Set of Panels to opt from.
     *
     * @return  Name of the Next Panel to be displayed
     */
    public String getNextPanel(String curPanelName, Object wizCxt,
        String options[]);
}
