/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)IPMPGroupEntryPanel.java	1.21	08/12/10 SMI"
 */

package com.sun.cluster.dswizards.logicalhost;


// CMASS
import com.sun.cluster.agent.dataservices.utils.Util;

// CLI Wizard SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;

// Wizard Common
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;

// J2SE
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * The Panel Class That Gets the IPMP Groups from the User
 */
public class IPMPGroupEntryPanel extends WizardLeaf {

    private String panelName; // Panel Name
    private WizardI18N messages; // Message Bundle
    private CliDSConfigWizardModel wizardModel; // WizardModel

    /**
     * Creates a new instance of IPMPGroupEntryPanel
     */
    public IPMPGroupEntryPanel() {
    }

    /**
     * Creates an IPMPGroupEntry Panel with the given name and manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public IPMPGroupEntryPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates an IPMPGroupEntry Panel with the given name and associates it
     * with the WizardModel and I18N Strings
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  messages  WizardI18n
     */
    public IPMPGroupEntryPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N messages) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;

        // Get the I18N instance
        this.messages = messages;
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {


        String propertyNames[] = { Util.NETIFLIST };
        String finalGroupNames[] = null;

        // Get the Discovered Nodelist From the Context
        String optionsArr[] = (String[]) wizardModel.getWizardValue(
                Util.RG_NODELIST);

        // Set Navigation Enabled to true and set cancel direction
        // to False, to indicate right direction.
        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        // Get the Discovered IPMPGroups From the wizardModel
        String ipmpGroupsNames[] = (String[]) wizardModel.getWizardValue(
                Util.NETIFLIST);

        // Set Navigation Enabled to true and set cancel direction
        // to False, to indicate right direction.
        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        boolean groupListOk;

        while (true) {

            TTYDisplay.clearScreen();
            TTYDisplay.clear(2);

            TTYDisplay.printTitle(messages.getString(
                    "lhwizard.wizard.step4.title"));
            TTYDisplay.clear(1);

            // Get the IPMP Groups From the User
            List option = TTYDisplay.getScrollingOptions(messages.getString(
                        "lhwizard.wizard.step4.instruction"), ipmpGroupsNames,
                    null, null, false);

            groupListOk = true;

            if (option == null) {
                this.cancelDirection = true;

                return;
            }

	    // Check if configuring on a zone cluster
	    String clusterSel = (String)wizardModel.getWizardValue(
		    Util.CLUSTER_SEL);
	    String nodeList[] = null;
	    if (clusterSel != null && clusterSel.equals(Util.ZONE_CLUSTER)) {
		nodeList = (String[])wizardModel.getWizardValue(
		    Util.BASE_CLUSTER_NODELIST);
	    } else {
		nodeList = (String[]) wizardModel.getWizardValue(
		    Util.RG_NODELIST);
	    }

            if (option.size() == 0) {

                // Store the default values
                List ipmpList = (List) wizardModel.getWizardValue(
                        LogicalHostWizardConstants.IPMPGROUPNAMES);


                List nodesToDiscover = new ArrayList();

                for (int i = 0; i < nodeList.length; i++) {
                    String split_str[] = nodeList[i].split(Util.COLON);
                    String node = split_str[0];

                    if (!nodesToDiscover.contains(node)) {
                        nodesToDiscover.add(node);
                    }
                }

                nodeList = (String[]) nodesToDiscover.toArray(
                        new String[nodesToDiscover.size()]);

                // The default IPMP Group is the first group
                String ipmpGroup = ipmpList.get(0).toString();

                finalGroupNames = new String[nodeList.length];

                for (int i = 0; i < nodeList.length; i++) {

                    if (nodeList[i].indexOf(Util.COLON) == -1) {
                        finalGroupNames[i] = ipmpGroup + "@" + nodeList[i];
                    }
                }
            } else {

                // If user had selected some IPMP Groups
                // Finalize the IPMPGroups
                List nodesToDiscover = new ArrayList();

                for (int i = 0; i < nodeList.length; i++) {
                    String split_str[] = nodeList[i].split(Util.COLON);
                    String node = split_str[0];

                    if (!nodesToDiscover.contains(node)) {
                        nodesToDiscover.add(node);
                    }
                }

                nodeList = (String[]) nodesToDiscover.toArray(
                        new String[nodesToDiscover.size()]);

                finalGroupNames = new String[option.size()];

                Iterator iter = option.iterator();
                int k = 0;

                while (iter.hasNext()) {
                    finalGroupNames[k++] = iter.next().toString();
                }

                // Check whether there is only one Group specified for a node
                if (option.size() > nodeList.length) {
                    TTYDisplay.printError(messages.getString(
                            "lhwizard.ipmpgroupPanel.tooMany"));
                    groupListOk = false;
                    TTYDisplay.promptForEntry(messages.getString(
                            "cliwizards.continue"));
                }

                // Check whether a Group has been selected for each node
                boolean found;

                for (int i = 0; i < nodeList.length; i++) {

                    if (nodeList[i].indexOf(Util.COLON) == -1) {
                        String node = nodeList[i];

                        // Check if IPMP Group is selected for this node
                        found = false;

                        for (int j = 0; j < finalGroupNames.length; j++) {
                            String ipmpGroup = finalGroupNames[j];

                            if (node.equals(
                                        ipmpGroup.substring(
                                            ipmpGroup.indexOf('@') + 1,
                                            ipmpGroup.length()))) {
                                found = true;

                                break;
                            }
                        }

                        if (!found) {
                            TTYDisplay.printError(messages.getString(
                                    "lhwizard.ipmpgroupPanel.noIpmpForNode",
                                    new Object[] { node }));
                            groupListOk = false;
                            TTYDisplay.promptForEntry(messages.getString(
                                    "cliwizards.continue"));

                            break;
                        }
                    }
                }
            }

            if (groupListOk) {

                // Confirm
                // Print the IPMP List
                TTYDisplay.clear(1);
                TTYDisplay.printList(messages.getString(
                        "lhwizard.ipmpgroupPanel.confirm"), finalGroupNames);

                TTYDisplay.setNavEnabled(false);
                TTYDisplay.clear(1);

                // Store the Values in the WizardModel Current Context
                wizardModel.selectCurrWizardContext();
                wizardModel.setWizardValue(
                    LogicalHostWizardConstants.SEL_NETIFLIST, finalGroupNames);

                break;
            }
        }
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#isDisplayComplete
     */
    public boolean isDisplayComplete() {
        return true;
    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }
}
