/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)RacDBResultPanel.java	1.16	08/12/10 SMI"
 */

package com.sun.cluster.dswizards.oraclerac.database.ver9i;

// J2SE
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.dataservices.utils.Util;

// CLI Wizard SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.oraclerac.database.*;
import com.sun.cluster.dswizards.oraclerac.invoker.RACInvokerCreator;

// Model classes
import com.sun.cluster.model.RACModel;

// Java
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.management.remote.JMXConnector; 

public class RacDBResultPanel extends RacDBBasePanel {

    JMXConnector localConnection = null;

    /**
     * Creates a RacDBResultPanel Panel for either the Wizard with the given
     * name and associates it with the WizardModel and WizardFlow.
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     * @param  wizardi18n  Resource Bundle
     */
    public RacDBResultPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(name, model, wizardFlow, wizardi18n);
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {

        // Enable Navigation
        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        String errorMessage = null;
        CommandExecutionException cee = null;

        // Title
        TTYDisplay.printTitle(wizardi18n.getString(
	    "haoracle.resultpanel.title"));

	String[] baseClusterNodelist = (String []) wizardModel.getWizardValue(
	    Util.BASE_CLUSTER_NODELIST);

        List commandList = (ArrayList) wizardModel.getWizardValue(Util.CMDLIST);

        Object tmpObj = wizardModel.getWizardValue(Util.EXITSTATUS);
        String tmpErrMsg = null;

        if (tmpObj != null) {
            cee = (CommandExecutionException) tmpObj;

            ExitStatus status[] = cee.getExitStatusArray();
	    if (status != null && status.length > 0) {
	       errorMessage = status[0].getErrStrings().toString();
	    }
            tmpErrMsg = cee.getMessage();
        }

        TTYDisplay.clear(2);
        printCommandList(commandList);

        if ((errorMessage == null) &&
	    ((tmpErrMsg == null) || tmpErrMsg.equals(""))) {
            TTYDisplay.printSubTitle(wizardi18n.getString(
                    "oraclerac.summarypanel.successGenerateCommand"));
            TTYDisplay.clear(1);

            // Write the current context to the state file
            try {

                // Clear the command list and exit status
                wizardModel.setWizardValue(Util.EXITSTATUS, null);
                wizardModel.setWizardValue(Util.CMDLIST, null);
                TTYDisplay.clear(1);
            } catch (Exception e) {
                TTYDisplay.printError(wizardi18n.getString(
                        "dswizards.stateFile.error"));
            }

            String query = wizardi18n.getString(
                    "oraclerac.resultspanel.online.query");
            String choice = TTYDisplay.getConfirmation(query, yes, no);

            if (choice.equalsIgnoreCase(yes)) {
                commandList = bringOnline();
                printCommandList(commandList);
            }

            TTYDisplay.setNavEnabled(false);
            TTYDisplay.promptForEntry(wizardi18n.getString(
                    "cliwizards.continue"));
	    closeConnections();
            System.exit(0);
        } else {

            if ((errorMessage != null) && !errorMessage.equals("")) {
                TTYDisplay.printError(wizardi18n.getString(
		    "cliwizards.executionErrorMessage") + "\n \n" + errorMessage);
            } else if ((tmpErrMsg != null) && tmpErrMsg.equals(Util.IO_ERROR)) {
                TTYDisplay.printError(wizardi18n.getString(
		    "cliwizards.resultspanel.errComm.summary"));
            }

            String query = wizardi18n.getString(
		"oraclerac.resultspanel.rollback.query");
            String choice = TTYDisplay.getConfirmation(query, yes, no);

            if (choice.equalsIgnoreCase(yes)) {
                commandList = rollBack();
                printCommandList(commandList);
            }

            String key = TTYDisplay.promptForEntry(wizardi18n.getString(
		"cliwizards.continue"));

            if (key.equals("<")) {
                this.cancelDirection = true;

                return;
            }

            // Write the current context to the state file
            try {

                // Clear the command list and exit status
                wizardModel.setWizardValue(Util.EXITSTATUS, null);
                wizardModel.setWizardValue(Util.CMDLIST, null);
            } catch (Exception e) {
                TTYDisplay.printError(wizardi18n.getString(
                        "dswizards.stateFile.error"));
            } finally {

                // Always close connection
		closeConnections();
            }

            System.exit(1);
        }
    }

    private List bringOnline() {
        HashMap zMap = (HashMap) wizardModel.getWizardValue(Util.CONFMAP);
        String zType = (String) zMap.get(Util.CONFIGURATION_TYPE);
        List commandList = null;
        Thread t = null;

        if (zType.equals(Util.RAC_9I)) {

            try {
		RACModel racModel = RACInvokerCreator.getRACModel();
		String nodeEndPoint = (String) wizardModel.getWizardValue(
		    Util.NODE_TO_CONNECT);

                TTYDisplay.clear(1);
                t = TTYDisplay.busy(wizardi18n.getString(
                            "cliwizards.executingMessage"));
                t.start();
                commandList = racModel.bringOnline9IDatabase(null, nodeEndPoint, zMap);
            } catch (CommandExecutionException cee) {
                ExitStatus status[] = cee.getExitStatusArray();
		String tmpErrMsg = null;
		if (status != null && status.length > 0) {
		    tmpErrMsg = status[0].getErrStrings().toString();
		}

                if ((tmpErrMsg != null) && !tmpErrMsg.equals("")) {
                    TTYDisplay.printError(wizardi18n.getString(
                            "cliwizards.onlineErrorMessage") + tmpErrMsg);
                } else {
                    String tmpErr = cee.getMessage();

                    if ((tmpErr != null) && tmpErr.equals(Util.IO_ERROR)) {
                        TTYDisplay.printError(wizardi18n.getString(
                                "cliwizards.resultspanel.errComm.summary"));
                    }
                }

                TTYDisplay.clear(1);
            } finally {

                try {
                    t.interrupt();
                    t.join();
                } catch (Exception exe) {
                }
            }
        }

        return commandList;
    }

    private List rollBack() {
        HashMap zMap = (HashMap) wizardModel.getWizardValue(Util.CONFMAP);
        String zType = (String) zMap.get(Util.CONFIGURATION_TYPE);
        List commandList = null;
        Thread t = null;

        if (zType.equals(Util.RAC_9I)) {

            try {

		RACModel racModel = RACInvokerCreator.getRACModel();
		String nodeEndPoint = (String) wizardModel.getWizardValue(
		    Util.NODE_TO_CONNECT);

                TTYDisplay.clear(1);
                t = TTYDisplay.busy(wizardi18n.getString(
                            "cliwizards.executingMessage"));
                t.start();
                commandList = racModel.rollBack9ICreation(null, nodeEndPoint, zMap);
            } catch (CommandExecutionException cee) {
                ExitStatus status[] = cee.getExitStatusArray();
                String tmpErrMsg = null;
		if (status != null && status.length > 0) {
		    tmpErrMsg = status[0].getErrStrings().toString();
		}

                if ((tmpErrMsg != null) && !tmpErrMsg.equals("")) {
                    TTYDisplay.printError(wizardi18n.getString(
                            "cliwizards.rollBackErrorMessage") + tmpErrMsg);
                } else {
                    String tmpErr = cee.getMessage();

                    if ((tmpErr != null) && tmpErr.equals(Util.IO_ERROR)) {
                        TTYDisplay.printError(wizardi18n.getString(
                                "cliwizards.resultspanel.errComm.summary"));
                    }
                }

                TTYDisplay.clear(1);
            } finally {

                try {
                    t.interrupt();
                    t.join();
                } catch (Exception exe) {
                }
            }

        }

        return commandList;
    }


    void printCommandList(List commandList) {

        if (commandList != null) {
            TTYDisplay.printSubTitle(wizardi18n.getString(
                    "cliwizards.executionMessage"));
            TTYDisplay.clear(1);

            Iterator i = commandList.iterator();

            while (i.hasNext()) {
                String command = (String) i.next();
                TTYDisplay.showText(command, 4);
                TTYDisplay.clear(1);
            }
        } else {
            TTYDisplay.printError(wizardi18n.getString(
                    "cliwizards.resultspanel.errComm.summary"));
        }
    }

    private void closeConnections() {
	String nodeList[] = (String[]) wizardModel.getWizardValue(
	    Util.BASE_CLUSTER_NODELIST);

	try {
	    if (localConnection == null) {
		localConnection = getConnection(Util.getClusterEndpoint());
	    }
	    wizardModel.writeToStateFile(localConnection.getMBeanServerConnection());
	    RACModel racModel = RACInvokerCreator.getRACModel();
	    racModel.closeConnections(nodeList);
	} catch (Exception e) {
	    TTYDisplay.printError(wizardi18n.getString(
		"dswizards.stateFile.error"));
	}
    }
}
