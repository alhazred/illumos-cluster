/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)CliStorageSelectionPanel.java 1.15     08/12/10 SMI"
 */


package com.sun.cluster.dswizards.common;

import com.sun.cluster.agent.dataservices.utils.InfrastructureMBean;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.rgm.property.ResourceProperty;
import com.sun.cluster.common.TrustedMBeanModel;
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.hastorageplus.HASPWizardConstants;

import java.lang.ArrayIndexOutOfBoundsException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.management.remote.JMXConnector;


public class CliStorageSelectionPanel extends WizardLeaf {
    protected static final String SELECT_EXISTING = "1";
    protected static final String CREATE_NEW = "2";
    protected static final String FS_MTPTS = "fs_mtpts";

    private static final String HASP_TABLE = "HASP_Table";
    private static final String SEL_INDICES = "Selected_Indices";

    private String panelName; // Panel Name
    protected WizardI18N wizardi18n; // Message Bundle
    protected CliDSConfigWizardModel wizardModel; // WizardModel

    protected InfrastructureMBean mbean = null;

    protected String back;
    protected String yes;
    protected String no;
    protected String title;
    protected String desc;
    protected String subtitle;
    protected String cchar;
    protected String createnew_txt;
    protected String rchar;
    protected String refresh;
    protected String dchar;
    protected String done;
    protected String heading1;
    protected String heading2;
    protected String heading3;
    protected String help_text;
    protected String continue_txt;
    protected String table_empty_continue;
    protected String empty;

    private int COL_COUNT = 3;
    private Vector table = null;

    /**
     * Creates a new instance of CliStorageSelectionPanel
     */
    public CliStorageSelectionPanel() {
        super();
    }

    /**
     * Creates a CliStorageSelectionPanel Panel with the given name and manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public CliStorageSelectionPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    public CliStorageSelectionPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;
        this.wizardi18n = wizardi18n;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    public void consoleInteraction(boolean singleSelection) {
        title = wizardi18n.getString("storageSelectionPanel.title");
        continue_txt = wizardi18n.getString("cliwizards.continue");
        table_empty_continue = wizardi18n.getString(
                "cliwizards.table.empty.continue");
        empty = wizardi18n.getString("storageSelectionPanel.table.empty");
        subtitle = "";
        help_text = wizardi18n.getString("storageSelectionPanel.cli.help");

        displayPanel(singleSelection);
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.
     *		WizardComponent#consoleInteraction
     */
    public void displayPanel(boolean singleSelection) {

        back = wizardi18n.getString("ttydisplay.back.char");
        yes = wizardi18n.getString("cliwizards.yes");
        no = wizardi18n.getString("cliwizards.no");
        cchar = wizardi18n.getString("ttydisplay.c.char");
        createnew_txt = wizardi18n.getString("ttydisplay.menu.create.new");
        rchar = wizardi18n.getString("ttydisplay.r.char");
        refresh = wizardi18n.getString("ttydisplay.menu.refresh");
        dchar = wizardi18n.getString("ttydisplay.d.char");
        done = wizardi18n.getString("ttydisplay.menu.done");

        heading1 = wizardi18n.getString(
                "storageSelectionPanel.table.colheader.0");
        heading2 = wizardi18n.getString(
                "storageSelectionPanel.table.colheader.1");
        heading3 = wizardi18n.getString(
                "storageSelectionPanel.table.colheader.2");

        refreshRGList();
        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        List rgList = null;

        boolean createnew_choice = false;

        boolean storageOk = false;

        while (!storageOk) {
            rgList = (List) wizardModel.getWizardValue(Util.STORAGE_RGLIST);

            // Title
            TTYDisplay.printTitle(title);

            String zFilesystemSelections[] = (String[]) wizardModel
                .getWizardValue(HASPWizardConstants.HASP_FILESYSTEMS_SELECTED);

            String zGlobalDeviceSelections[] = (String[]) wizardModel
                .getWizardValue(HASPWizardConstants.HASP_DEVICES_SELECTED);

            String newlyAdded = null;

            if ((zFilesystemSelections != null) &&
                    (zFilesystemSelections.length > 0)) {
                newlyAdded = ResourcePropertyUtil.join(zFilesystemSelections,
                        Util.COMMA);
            }

            if ((zGlobalDeviceSelections != null) &&
                    (zGlobalDeviceSelections.length > 0)) {

                if (newlyAdded == null) {
                    newlyAdded = ResourcePropertyUtil.join(
                            zGlobalDeviceSelections, Util.COMMA);
                } else {
                    StringBuffer zStr = new StringBuffer();
                    zStr.append(newlyAdded);
                    zStr.append(Util.COMMA);
                    zStr.append(ResourcePropertyUtil.join(
                            zGlobalDeviceSelections, Util.COMMA));
                    newlyAdded = zStr.toString();
                }
            }

            if (newlyAdded != null) {
                TTYDisplay.printSubTitle(wizardi18n.getString(
                        "storagepanel.alert.summary",
                        new String[] { newlyAdded }));
            }

            // Description Para
            TTYDisplay.printSubTitle(desc);

            if (!singleSelection) {
                TTYDisplay.printSubTitle(wizardi18n.getString(
                        "storageSelectionPanel.extraDesc"));
            }

            HashMap customTags = new HashMap();
            populateTable(rgList);

            customTags.put(cchar, createnew_txt);
            customTags.put(rchar, refresh);
            customTags.put(dchar, done);

            List optionsList = null;
            String enteredKey = "";
            String option[] = null;

            // get older selections
            List oldSelections = (List) wizardModel.getWizardValue(SEL_INDICES);
            String subheadings[] = new String[] {
                    heading1, heading2, heading3
                };

            if ((table != null) && (table.size() > 0)) {

                // Display the (n*3) Table
                option = TTYDisplay.create2DTable(subtitle, subheadings, table,
                        table.size() / COL_COUNT, COL_COUNT, customTags,
                        help_text, empty, singleSelection, oldSelections);

                if (option != null) {
                    optionsList = Arrays.asList(option);
                }
            } else {

                // Print empty text and automatically select 'Create New'
                TTYDisplay.displaySubheadings(subheadings);
                TTYDisplay.printSubTitle(empty);
                enteredKey = TTYDisplay.promptForEntry(table_empty_continue,
                        help_text);
                optionsList = new ArrayList();
                optionsList.add(cchar);
            }

            // Handle Back
            if ((optionsList == null) || enteredKey.equals(back)) {
                wizardModel.setWizardValue(Util.CREATE_NEW, null);
                this.cancelDirection = true;

                return;
            }

            if (optionsList.contains(rchar)) {

                // refresh the resource list
                wizardModel.setWizardValue(HASP_TABLE, null);
                wizardModel.setWizardValue(SEL_INDICES, null);
                wizardModel.setWizardValue(Util.SEL_FILESYSTEMS, null);
                wizardModel.setWizardValue(Util.SEL_DEVICES, null);
                refreshRGList();

                continue;
            }

            if ((optionsList.size() == 1) && optionsList.contains(dchar)) {
                continue;
            }

            storageOk = true;

            TTYDisplay.setNavEnabled(true);
            this.cancelDirection = false;

            // remove all customTags from tmp_selections
            List tmp_selections = new ArrayList(optionsList);

            Iterator keyIterator = (customTags.keySet()).iterator();

            while (keyIterator.hasNext()) {
                String key = (String) keyIterator.next();
                tmp_selections.remove(key);
            }

            String selectedList[] = (String[]) tmp_selections.toArray(
                    new String[0]);

            // validateInput. Make sure the RGs are the same for the selected
            // resources
            String rsName1 = null;
            String rsName2 = null;
            String rgName1 = null;
            String rgName2 = null;

            String rgName;
            boolean error = false;
            int i1 = 0;
            Vector selectedResources = new Vector();

            while ((i1 < (selectedList.length - 1)) && !error) {
                String optionValue1 = (String) selectedList[i1];

                try {
                    int value = Integer.parseInt(optionValue1);
                    rsName1 = getRSName(value);

                    if ((rsName1 != null) && (rsName1.trim().length() > 0)) {

                        // existing resource
                        if (!selectedResources.contains(rsName1)) {
                            selectedResources.add(rsName1);
                        }

                        rgName1 = getRGName(value);
                    }
                } catch (NumberFormatException nfe) {
                    // Custom Tags
                }

                for (int i2 = (i1 + 1); i2 < selectedList.length; i2++) {
                    String optionValue2 = (String) selectedList[i2];

                    try {
                        int value = Integer.parseInt(optionValue2);
                        rsName2 = getRSName(value);

                        if ((rsName2 != null) &&
                                (rsName2.trim().length() > 0)) {

                            if (!selectedResources.contains(rsName2)) {
                                selectedResources.add(rsName2);
                            }

                            rgName2 = getRGName(value);
                        }
                    } catch (NumberFormatException nfe) {
                    }

                    if (((rgName1 != null) && (rgName1.trim().length() > 0)) &&
                            ((rgName2 != null) &&
                                (rgName2.trim().length() > 0))) {

                        if (!rgName1.equals(rgName2)) {
                            error = true;
                        }
                    }
                }

                if (!error) {
                    i1++;
                }
            }

            if (selectedList.length == 1) {
                String optionValue = (String) selectedList[0];

                try {
                    int value = Integer.parseInt(optionValue);
                    String rsName = getRSName(value);

                    if ((rsName != null) && (rsName.trim().length() > 0)) {

                        if (!selectedResources.contains(rsName)) {
                            selectedResources.add(rsName);
                        }

                        rgName1 = getRGName(value);
                    }
                } catch (NumberFormatException nfe) {
                }
            }

            rgName = (rgName1 != null) ? rgName1 : rgName2;

            if (error) {
                String errMsg = wizardi18n.getString(
                        "storageSelectionPanel.rgmismatch.error",
                        new String[] { rsName1, rsName2 });
                TTYDisplay.printError(errMsg);
                TTYDisplay.promptForEntry(continue_txt);

                // Clear the selection
                wizardModel.setWizardValue(Util.SEL_RS, null);
                wizardModel.setWizardValue(Util.SEL_RG, null);
                wizardModel.setWizardValue(SEL_INDICES, null);
                wizardModel.setWizardValue(FS_MTPTS, null);
                storageOk = false;

                continue;
            }

            if (optionsList.contains(cchar)) {

                // handle create new
                wizardModel.setWizardValue(Util.CREATE_NEW, CREATE_NEW);
            } else {
                wizardModel.setWizardValue(Util.CREATE_NEW, SELECT_EXISTING);
                TTYDisplay.clear(1);
            }

            // store selection
            wizardModel.setWizardValue(Util.SEL_RS, selectedResources);
            wizardModel.setWizardValue(Util.SEL_RG, rgName);
            wizardModel.setWizardValue(SEL_INDICES, tmp_selections);

            // Store selected file system mountpoints
            Iterator iter = tmp_selections.iterator();
            List fileMtpts = new ArrayList();

            while (iter.hasNext()) {
                fileMtpts.add(getFSMountPoint(
                        Integer.parseInt((String) iter.next())));
            }

            wizardModel.setWizardValue(FS_MTPTS, fileMtpts);

        }
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#isDisplayComplete
     */
    public boolean isDisplayComplete() {
        return true;
    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }

    protected void refreshRGList() {
        TTYDisplay.clear(2);

        Thread t = TTYDisplay.busy(wizardi18n.getString(
                    "storageSelectionPanel.discovery.busy"));
        t.start();

        if (mbean == null) {
            mbean = (InfrastructureMBean) getInfrastructureMBean();
        }

        HashMap helperData = new HashMap();

        String rgName = (String) wizardModel.getWizardValue(Util.RG_NAME);

        if (rgName == null) {
            String nodeList[] = (String[]) wizardModel.getWizardValue(
                    Util.RG_NODELIST);

            helperData.put(Util.RG_NODELIST, nodeList);
        } else {
            helperData.put(Util.RG_NAME, rgName);
        }

        // returns a map that has list of resource names : rgnames
        HashMap map = mbean.discoverPossibilities(
                new String[] { Util.STORAGE_RGLIST }, helperData);
        List rgs = (List) map.get(Util.STORAGE_RGLIST);

        try {
            t.interrupt();
            t.join();
        } catch (Exception e) {

        }

        wizardModel.setWizardValue(Util.STORAGE_RGLIST, rgs);
    }

    private void populateTable(List list) {
        table = (Vector) wizardModel.getWizardValue(HASP_TABLE);

        if (table == null) {
            table = new Vector();

            if (list != null) {
                int size = list.size();

                for (int i = 0; i < size; i++) {
                    HashMap map = (HashMap) list.get(i);
                    String rgName = (String) map.get(Util.RG_NAME);
                    String rsName = (String) map.get(Util.RS_NAME);
                    String filesystemProp[] = (String[]) map.get(
                            Util.HASP_ALL_FILESYSTEMS);
                    String devPathProp[] = (String[]) map.get(
                            Util.HASP_ALL_DEVICES);
                    if (filesystemProp == null && devPathProp == null) {
                        // it must be QFS resource
                        filesystemProp = (String[]) map.get(
                            Util.QFS_FILESYSTEM);
                    }

                    // add RS Name
                    table.add(rsName);

                    // add RG name
                    table.add(rgName);

                    // add filesystem mount point/global dev path
                    if ((filesystemProp != null) &&
                            (filesystemProp.length > 0)) {
                        table.add(filesystemProp[0]);
                    } else if ((devPathProp != null) &&
                            (devPathProp.length > 0)) {
                        table.add(devPathProp[0]);
                    } else {
                        table.add("");
                    }

                }
            }
        }

        // default selections can be obtained from HASP_FILESYSTEMS_SELECTED
        String filesystemSelections[] = (String[]) wizardModel.getWizardValue(
                HASPWizardConstants.HASP_FILESYSTEMS_SELECTED);

        String globalDeviceSelections[] = (String[]) wizardModel.getWizardValue(
                HASPWizardConstants.HASP_DEVICES_SELECTED);
        String newFS[] = (String[]) wizardModel.getWizardValue(
                Util.SEL_FILESYSTEMS);
        String newDev[] = (String[]) wizardModel.getWizardValue(
                Util.SEL_DEVICES);

        if ((newFS != null) && (newFS.length > 0)) {

            // Handle duplicates
            if (filesystemSelections != null) {
                List addedList = Arrays.asList(newFS);
                ArrayList newList = null;

                for (int x = 0; x < filesystemSelections.length; x++) {

                    if (!addedList.contains(filesystemSelections[x])) {

                        if (newList == null) {
                            newList = new ArrayList();
                        }

                        newList.add(filesystemSelections[x]);
                    }
                }

                if (newList != null) {
                    filesystemSelections = (String[]) newList.toArray(
                            new String[0]);
                } else {
                    filesystemSelections = null;
                }
            }
        }

        if ((newDev != null) && (newDev.length > 0)) {

            // Handle duplicates
            if (globalDeviceSelections != null) {
                List addedList = Arrays.asList(newDev);
                ArrayList newList = null;

                for (int x = 0; x < globalDeviceSelections.length; x++) {

                    if (!addedList.contains(globalDeviceSelections[x])) {

                        if (newList == null) {
                            newList = new ArrayList();
                        }

                        newList.add(globalDeviceSelections[x]);
                    }
                }

                if (newList != null) {
                    globalDeviceSelections = (String[]) newList.toArray(
                            new String[0]);
                } else {
                    globalDeviceSelections = null;
                }
            }
        }

        // display all the new selections in the table
        boolean addRow = false;

        if ((filesystemSelections != null) &&
                (filesystemSelections.length > 0)) {

            // blanks for RS Name and RG Name
            table.add("");
            table.add("");
            table.add(filesystemSelections[0]);
            addRow = true;
        } else if ((globalDeviceSelections != null) &&
                (globalDeviceSelections.length > 0)) {

            // blanks for RS Name and RG Name
            table.add("");
            table.add("");
            table.add(globalDeviceSelections[0]);
            addRow = true;
        }

        // Mark the new selections as automatically selected
        if (addRow) {
            List selectedIndices = (List) wizardModel.getWizardValue(
                    SEL_INDICES);

            if (selectedIndices == null) {
                selectedIndices = new ArrayList();
            }

            int rowIndex = table.size() / COL_COUNT;
            String stIndex = Integer.toString(rowIndex);

            if (!selectedIndices.contains(stIndex)) {
                selectedIndices.add(stIndex);
            }

            wizardModel.setWizardValue(SEL_INDICES, selectedIndices);
        }

        // set the selections from HASP_FILESYSTEMS_SELECTED and
        // HASP_DEVICES_SELECTED to null
        // save selected filesystems
        String existingFS[] = (String[]) wizardModel.getWizardValue(
                HASPWizardConstants.HASP_FILESYSTEMS_SELECTED);
        String selectedFS[] = null;

        if (existingFS != null) {

            if (newFS != null) {
                selectedFS = ResourcePropertyUtil.concatenateArrays(existingFS,
                        newFS);
            } else {
                selectedFS = existingFS;
            }
        } else {

            if (newFS != null) {
                selectedFS = newFS;
            }
        }

        String existingDev[] = (String[]) wizardModel.getWizardValue(
                HASPWizardConstants.HASP_DEVICES_SELECTED);
        String selectedDev[] = null;

        if (existingDev != null) {

            if (newDev != null) {
                selectedDev = ResourcePropertyUtil.concatenateArrays(
                        existingDev, newDev);
            } else {
                selectedDev = existingDev;
            }
        } else {

            if (newDev != null) {
                selectedDev = newDev;
            }
        }

        wizardModel.setWizardValue(HASP_TABLE, table);
        wizardModel.setWizardValue(Util.SEL_FILESYSTEMS, selectedFS);
        wizardModel.setWizardValue(Util.SEL_DEVICES, selectedDev);
        wizardModel.setWizardValue(
            HASPWizardConstants.HASP_FILESYSTEMS_SELECTED, null);
        wizardModel.setWizardValue(HASPWizardConstants.HASP_DEVICES_SELECTED,
            null);
    }


    private String getRSName(int index) {
        String rsName = null;

        try {
            rsName = (String) table.get((index * COL_COUNT) - 3);
        } catch (ArrayIndexOutOfBoundsException arrayEx) {
        }

        return rsName;
    }

    private String getRGName(int index) {
        String rgName = null;

        try {
            rgName = (String) table.get((index * COL_COUNT) - 2);
        } catch (ArrayIndexOutOfBoundsException arrayEx) {
        }

        return rgName;
    }

    private String getFSMountPoint(int index) {
        String fs = null;

        try {
            fs = (String) table.get((index * COL_COUNT) - 1);
        } catch (ArrayIndexOutOfBoundsException arrayEx) {
        }

        return fs;
    }

    protected InfrastructureMBean getInfrastructureMBean() {
        JMXConnector localConnection = getConnection(Util.getClusterEndpoint());

        if (mbean == null) {

            try {
                mbean = (InfrastructureMBean) TrustedMBeanModel.getMBeanProxy(
                        Util.DOMAIN, localConnection, InfrastructureMBean.class,
                        null, false);
            } catch (Exception e) {
            }
        }

        return mbean;
    }
}
