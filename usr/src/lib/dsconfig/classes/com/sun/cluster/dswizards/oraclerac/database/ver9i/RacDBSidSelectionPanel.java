/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)RacDBSidSelectionPanel.java	1.17	09/03/11 SMI"
 */

package com.sun.cluster.dswizards.oraclerac.database.ver9i;

// CMASS
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.dataservices.common.ErrorValue;

// CLI Wizard Core
import com.sun.cluster.dswizards.common.CliUtil;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.oraclerac.database.*;
import com.sun.cluster.dswizards.oraclerac.invoker.RACInvokerCreator;

// Model
import com.sun.cluster.model.RACModel;

// J2SE
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

/**
 * This panel gets the Oracle SID from the User
 */
public class RacDBSidSelectionPanel extends RacDBBasePanel {

    /**
     * Creates an Oracle SID selection panel for the wizard with the given
     * name and associates it with the WizardModel and WizardFlow.
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     * @param  wizardType  WizardType
     */
    public RacDBSidSelectionPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(name, model, wizardFlow, wizardi18n);
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        title = wizardi18n.getString("haoracle.oracleSidSelectionPanel.title");

        String enteryourown = wizardi18n.getString(
                "haoracle.oracleSidSelectionPanel.enteryourown");
        String help_text = wizardi18n.getString(
                "oraclerac.oracleSidSelectionPanel.cli.help");
        String selected_orasid = null;
        String defSelection = null;

        String instructions = wizardi18n.getString(
                "oraclerac.oracleSidSelectionPanel.instructions");
        TTYDisplay.printTitle(title);
        TTYDisplay.printSubTitle(instructions);

        String curNode = (String) wizardModel.getWizardValue(
	    Util.RAC_CURNODENAME);
        String nodeList[] = (String[]) wizardModel.getValue(Util.RG_NODELIST);

        if (curNode == null) {
            curNode = nodeList[0];
            wizardModel.setWizardValue(Util.RAC_NODEIDX, new Integer(0));
            wizardModel.setWizardValue(Util.RAC_CURNODENAME, curNode);
        }

        String tmpArry[] = curNode.split(Util.COLON);
        curNode = tmpArry[0];
        desc = wizardi18n.getString("oraclerac.oracleSidSelectionPanel.desc",
                new String[] { curNode });

        String orasid_prompt = wizardi18n.getString(
                "oraclerac.oracleSidSelectionPanel.prompt",
                new String[] { curNode });
        String orasidconfirm_txt = wizardi18n.getString(
                "oraclerac.oracleSidSelectionPanel.confirm",
                new String[] { curNode });

        // Get the discovered ORACLE_HOME and ORACLE_SID from the dataservice
        Vector vOraSid = (Vector) wizardModel.getWizardValue(Util.ORACLE_SID);

        String optionsArr[] = (String[]) vOraSid.toArray(new String[0]);

        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        // Add SID to the property names to be serialized
        List toBeSerialized = new ArrayList();
        Object tmpObj = (ArrayList) wizardModel.getWizardValue(
                Util.WIZARD_STATE);

        if (tmpObj != null) {
            toBeSerialized = (ArrayList) tmpObj;
        }

        if (!toBeSerialized.contains(Util.SEL_ORACLE_SID + curNode)) {
            toBeSerialized.add(Util.SEL_ORACLE_SID + curNode);
        }

        wizardModel.setWizardValue(Util.WIZARD_STATE, toBeSerialized);

        // get the default selection
        selected_orasid = (String) wizardModel.getWizardValue(
                Util.SEL_ORACLE_SID + curNode);

        if (selected_orasid != null) {
            defSelection = selected_orasid;
        }

        String choice = no;
        HashMap custom = new HashMap();
        custom.put(enterKey, enterText);

        while (choice.equals(no)) {

            TTYDisplay.clear(1);

            // Get the oracle home preference
            // see if an oracle home is already set
            if ((optionsArr != null) && (optionsArr.length > 0)) {
                List selList = TTYDisplay.getScrollingOption(desc, optionsArr,
                        custom, defSelection, help_text, true);

                if (selList == null) {
                    this.cancelDirection = true;

                    return;
                }

                TTYDisplay.setNavEnabled(true);
                this.cancelDirection = false;

                String selArr[] = (String[]) selList.toArray(new String[0]);
                selected_orasid = selArr[0];
            } else {
                selected_orasid = enterText;
            }

            if (selected_orasid.equals(enterText)) {
                TTYDisplay.clear(1);
                selected_orasid = "";

                while (selected_orasid.trim().length() == 0) {
                    selected_orasid = TTYDisplay.promptForEntry(orasid_prompt);

                    if (selected_orasid.equals(back)) {
                        this.cancelDirection = true;

                        return;
                    }
                }
            }

            String nodeName = getNodeName(selected_orasid);

            if (nodeName != null) {
                String sidinuse_error = wizardi18n.getString(
                        "oraclerac10g.oracleSidSelectionPanel.sidinuse",
                        new String[] { nodeName });
                selected_orasid = "";
                TTYDisplay.printError(sidinuse_error);
                TTYDisplay.promptForEntry(continue_txt);

                choice = no;

                continue;
            }

            // validate the Oracle Sid
            ErrorValue retVal = validateInput(selected_orasid);

            if (!retVal.getReturnValue().booleanValue()) {
                TTYDisplay.printInfo(retVal.getErrorString());

                String enterAgain = TTYDisplay.getConfirmation(enteragain_txt,
                        yes, no);

                if (enterAgain.equalsIgnoreCase(yes)) {
                    choice = no;

                    continue;
                }
            }

            choice = yes;

            // Store the Values in the WizardModel
            wizardModel.selectCurrWizardContext();
            wizardModel.setWizardValue(Util.SEL_ORACLE_SID + curNode,
                selected_orasid);
        }
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#isDisplayComplete
     */
    public boolean isDisplayComplete() {
        return true;
    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }

    private String getNodeName(String selectedOraSid) {
        String nodeList[] = (String[]) wizardModel.getWizardValue(
                Util.RG_NODELIST);
        boolean found = false;
        int i = 0;
        String orasid = null;
        String curnode = (String) wizardModel.getWizardValue(
                Util.RAC_CURNODENAME);
        String nodename = null;

        while ((i < nodeList.length) && !found) {
            String split_str[] = nodeList[i].split(Util.COLON);
            nodename = split_str[0];

            if (!nodename.equals(curnode)) {
                orasid = (String) wizardModel.getWizardValue(
                        Util.SEL_ORACLE_SID + nodename);

                if ((orasid != null) && orasid.equals(selectedOraSid)) {
                    found = true;
                }
            }

            i++;
        }

        if (!found) {
            return null;
        }

        return nodename;
    }

    private ErrorValue validateInput(String oraSid) {
        String propName[] = { Util.ORACLE_SID };
        HashMap userInput = new HashMap();
        userInput.put(Util.ORACLE_SID, oraSid);

	String baseClusterNodelist[] = (String[]) wizardModel.getWizardValue(
	    Util.BASE_CLUSTER_NODELIST); 
        ErrorValue err = new ErrorValue();
        err.setReturnVal(Boolean.TRUE);

        StringBuffer errStr = new StringBuffer();
        int err_count = 0;
	RACModel racModel = RACInvokerCreator.getRACModel();

        String selZoneCluster = CliUtil.getZoneClusterName(wizardModel);

	HashMap helperData = new HashMap();
	helperData.put(Util.SEL_ZONE_CLUSTER, selZoneCluster);

        for (int i = 0; i < baseClusterNodelist.length; i++) {
            String tmpArry[] = baseClusterNodelist[i].split(Util.COLON);
            String cNode = tmpArry[0];

            ErrorValue errVal = racModel.validateInput(
		null, cNode, propName, userInput, helperData);

            if (!errVal.getReturnValue().booleanValue()) {

                // error received.
                err_count++;
                errStr = new StringBuffer(errVal.getErrorString());
            }
        }

        if (err_count == baseClusterNodelist.length) {

            // failed on all nodes.
            err.setReturnVal(Boolean.FALSE);
            err.setErrorString(errStr.toString());
        }

        return err;
    }
}
