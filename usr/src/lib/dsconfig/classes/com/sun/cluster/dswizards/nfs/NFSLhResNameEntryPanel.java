/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)NFSLhResNameEntryPanel.java 1.13     08/12/10 SMI"
 */


package com.sun.cluster.dswizards.nfs;

// J2SE
import com.sun.cluster.agent.dataservices.logicalhost.LogicalHostMBean;

// CMASS
import com.sun.cluster.agent.dataservices.nfs.NFSMBean;
import com.sun.cluster.agent.dataservices.utils.Util;

// CLI Wizard SDK
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliLogicalHostSelectionPanel;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;

// CLI wizard
import com.sun.cluster.dswizards.logicalhost.LogicalHostWizardCreator;

import java.util.Vector;

import javax.management.remote.JMXConnector;


/**
 * The Panel Class that gets a LogicalHostname as input from the User.
 */
public class NFSLhResNameEntryPanel extends CliLogicalHostSelectionPanel {

    /**
     * Creates a new instance of NFSLhResNameEntryPanel.
     */
    public NFSLhResNameEntryPanel() {
        super();
    }

    /**
     * Creates a NFSLhResNameEntryPanel Panel with the given name and tree
     * manager.
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public NFSLhResNameEntryPanel(String name, WizardTreeManager manager) {
        super(name, manager);
    }

    /**
     * Creates a NFSLhResNameEntryPanel Panel for either the NFS Wizard with the
     * given name and associates it with the WizardModel and WizardFlow.
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     * @param  wizardi18n  Resource Bundle
     */
    public NFSLhResNameEntryPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(name, model, wizardFlow, wizardi18n);
    }


    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {

        desc = wizardi18n.getString("nfs.lhResNameEntry.desc");

        // initialize values
        wizardModel.setWizardValue(NFSWizardConstants.NEWRGFLAG, "false");
        wizardModel.setWizardValue(NFSWizardConstants.SHOWLHWIZ, "false");

        super.consoleInteraction(true);

        Object tmpObj = wizardModel.getWizardValue(Util.CREATE_NEW);

        if (tmpObj == null) {
            return;
        } else if (((String) tmpObj).equals(CREATE_NEW)) {

            // Jump to Logical Host Name wizard
            wizardModel.setWizardValue(NFSWizardConstants.SHOWLHWIZ, "true");

            return;
        }

        String selectedHost[] = (String[]) wizardModel.getWizardValue(
                Util.SEL_HOSTNAMES);

        if (selectedHost == null) {

            // user is not creating a new logical host
            wizardModel.setWizardValue(Util.NFS_RESOURCE_GROUP + Util.NAME_TAG,
                wizardModel.getWizardValue(LH_SEL_RG));

            Vector rsname = (Vector) wizardModel.getWizardValue(LH_SEL_RS);
            wizardModel.setWizardValue(Util.NFS_LH_RID + Util.NAME_TAG,
                (String) rsname.get(0));
        } else {

            // user has selected new lh res
            wizardModel.setWizardValue(NFSWizardConstants.NEWRGFLAG, "true");
        }
    }
}
