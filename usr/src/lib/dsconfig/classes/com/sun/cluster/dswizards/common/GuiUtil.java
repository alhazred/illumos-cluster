/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007-2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident "@(#)GuiUtil.java 1.4     09/03/12 SMI"
 */

package com.sun.cluster.dswizards.common;

import com.sun.cluster.agent.dataservices.utils.Util;

public class GuiUtil {

    // This is a Util class provided to define constants and functions that
    // are common to all web-based wizards.

    // As and when new wizards are written, this class should be used.
    // Would be ideal if we could have the existing wizard code use this class
    // but that might be a huge effort. For now, the plan is to have any new
    // wizards utilize this class.

    public static String getZoneClusterName(Object wizCxtObj) {
        CliDSConfigWizardModel cliModel = null;
        DSConfigWizardModel guiModel = null;

        if (wizCxtObj instanceof CliDSConfigWizardModel) {
            cliModel = (CliDSConfigWizardModel)wizCxtObj;
        } else if (wizCxtObj instanceof DSConfigWizardModel) {
            guiModel = (DSConfigWizardModel)wizCxtObj;
        }

        String clusterSelected = null;

	if (guiModel != null) {
	    clusterSelected = (String)guiModel.getWizardValue(
		Util.CLUSTER_SEL);
	} else {
	    clusterSelected = (String)cliModel.getWizardValue(
		Util.CLUSTER_SEL);
	}
        String selZoneCluster = null;
        if (clusterSelected != null &&
	    clusterSelected.equals(Util.ZONE_CLUSTER)) {
	    if (guiModel != null) {
		selZoneCluster = (String)guiModel.getWizardValue(
		    Util.SEL_ZONE_CLUSTER);
	    } else {
		selZoneCluster = (String)cliModel.getWizardValue(
		    Util.SEL_ZONE_CLUSTER);
	    }
        }
        return selZoneCluster;
    }

}

