/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)NodeSelectionPanel.java	1.22	08/12/10 SMI"
 */

package com.sun.cluster.dswizards.common;

// J2SE
import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.Iterator;

// JMX
import javax.management.remote.JMXConnector;

// CMASS
import com.sun.cluster.agent.dataservices.common.ServiceConfig;
import com.sun.cluster.agent.dataservices.utils.InfrastructureMBean;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.common.TrustedMBeanModel;
import com.sun.cluster.agent.zonecluster.ZoneClusterData;

// Wizards CLI SDK
import com.sun.cluster.agent.node.ZoneMBean;
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.hastorageplus.HASPWizardConstants;

import com.sun.cluster.model.ZoneClusterModel;
import com.sun.cluster.model.DataModel;

/**
 * This panel gets the Preferred Node from the User
 */
public class NodeSelectionPanel extends WizardLeaf {


    protected String panelName; // Panel Name
    protected CliDSConfigWizardModel wizardModel; // WizardModel
    protected WizardI18N wizardi18n; // The resource bundle

    protected String title;
    protected String desc;
    protected String yes;
    protected String no;
    protected String back;
    protected String nodeselect_txt;
    protected String achar;
    protected String all;
    protected String nodeconfirm_txt;
    protected String confirm_txt;
    protected String help_text;
    protected String rchar;
    protected String refresh;
    protected String continue_txt;
    protected String empty_txt;
    protected String optionsArr[] = null;

    private InfrastructureMBean mbean = null;

    private ZoneClusterData zcData = null;

    /**
     * Creates a new instance of NodeSelectionPanel
     */
    public NodeSelectionPanel() {
    }

    /**
     * Creates an PreferredNode Panel with the given name and manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public NodeSelectionPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates an PreferredNode Panel for the wizard with the given name and
     * associates it with the WizardModel and WizardFlow.
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     * @param  wizardi18n  WizardI18N
     */
    public NodeSelectionPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;
        this.wizardi18n = wizardi18n;
        init();
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    private void init() {
        yes = wizardi18n.getString("cliwizards.yes");
        no = wizardi18n.getString("cliwizards.no");
        back = wizardi18n.getString("ttydisplay.back.char");
        achar = wizardi18n.getString("ttydisplay.a.char");
        all = wizardi18n.getString("ttydisplay.menu.all");
        nodeselect_txt = wizardi18n.getString("nodeSelectionPanel.select");
        nodeconfirm_txt = wizardi18n.getString("nodeSelectionPanel.confirm");
        confirm_txt = wizardi18n.getString("cliwizards.confirmMessage");
        help_text = wizardi18n.getString("nodeSelectionPanel.cli.help");
        title = wizardi18n.getString("nodeSelectionPanel.title");
        rchar = wizardi18n.getString("ttydisplay.r.char");
        refresh = wizardi18n.getString("ttydisplay.menu.refresh");
        continue_txt = wizardi18n.getString("cliwizards.continue");
	empty_txt = wizardi18n.getString("nodeSelectionPanel.empty");
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {

    }

    /**
     * Displays the available nodes and zones in the cluster. The method checks
     * whether the provided mBean is accessible on the selected nodes
     */
    public String[] displayPanel(Class mBeanToCheck, String packageName) {

        // Discover Nodelist

	String selectedCluster = (String) wizardModel.getWizardValue(Util.CLUSTER_SEL);
	String selectedZoneCluster = null;
	String[] optionsArr = null;
	String propName = null;
	String[] propertyNames = null; 
	HashMap helperData = null;

	if (selectedCluster == null ||
	    selectedCluster.equals(Util.BASE_CLUSTER)) {
	    propName = Util.NODELIST;
	} else {
	    propName = Util.ZONE_CLUSTER_HOSTLIST;
	    selectedZoneCluster = (String) wizardModel.getWizardValue(Util.SEL_ZONE_CLUSTER);

	    Object[] arg = new Object[] { selectedZoneCluster }; 
	    nodeselect_txt = wizardi18n.getString("nodeSelectionPanel.zonecluster.select", arg);
	    help_text = wizardi18n.getString("nodeSelectionPanel.zonecluster.cli.help");
	    title = wizardi18n.getString("nodeSelectionPanel.zonecluster.title");
	    desc = wizardi18n.getString("orf.nodeSelectionPanel.zonecluster.desc");
	}

	optionsArr = (String[]) wizardModel.getWizardValue(propName);

	boolean nodesOk;
        String nodeListArr[] = null;
	wizardModel.setWizardValue(Util.EXCLUSIVEIP, Boolean.FALSE);
	HashMap discoveredMap = null;

        do {
            nodesOk = true;

            if (optionsArr == null) {
		try {
		    // If base cluster, discover all the online nodes and
		    // the zones configured in them.
		    // If zone cluster discover all zones forming it.
		    DataModel dm = DataModel.getDataModel(null);

		    if (selectedCluster == null ||
			selectedCluster.equals(Util.BASE_CLUSTER)) {
			// Discover all the online nodes and native zones
			// configured on the base cluster
			mbean = getInfrastructureMBean();

			discoveredMap = mbean.discoverPossibilities(
			    new String[] { propName }, null);
			
			optionsArr = (String [])discoveredMap.get(propName);
		    } else {
			ZoneClusterModel zm = dm.getZoneClusterModel();
			zcData = zm.getZoneClusterData(null, selectedZoneCluster);
			ArrayList zcNames = zcData.getHostNameList();
			if (zcNames != null) {
			    // Parse the list to get only the ZCs that are running
			    int size = zcNames.size();
			    ArrayList runningZCs = null;
			    for (int i = 0; i < size; i++) {
				String hostName = (String) zcNames.get(i);
				if (zm.isZoneRunning(null, selectedZoneCluster,
				    hostName)) {
				    if (runningZCs == null) {
					runningZCs = new ArrayList();
				    }
				    runningZCs.add(hostName);
				}
			    }

			    if (runningZCs != null) {
				optionsArr = (String [])runningZCs.toArray(new String[0]);
			    }
			}
		    }
	
		} catch(Exception e) {
		    System.out.println(e.getMessage());
		}
	    }

            // Get the previous value entered by the user
            String selectedArr[] = new String[] {};
            Object tmpObj = wizardModel.getWizardValue(Util.RG_NODELIST);

            if (tmpObj != null)
                selectedArr = (String[]) tmpObj;

            String selectedList = "";

            // Selected list would contain only those nodenames which are
            // present
            // in discovered nodelist.
            if (optionsArr != null) {
                boolean flag = false;

                for (int i = 0; i < selectedArr.length; i++) {

                    for (int j = 0; j < optionsArr.length; j++) {

                        if (selectedArr[i].equals(optionsArr[j])) {

                            if (flag)
                                selectedList = selectedList + ",";

                            selectedList = selectedList + selectedArr[i];
                            flag = true;
                        }
                    }
                }
            }


            // Make all node names as the default choice, i.e.excluding local
            // zones.
            if (selectedList.equals("")) {

                if (optionsArr != null) {
                    boolean flag = false;

                    for (int j = 0; j < optionsArr.length; j++) {
                        String split_str[] = optionsArr[j].split(Util.COLON);

                        if (split_str.length == 1) {

                            if (flag)
                                selectedList = selectedList + ",";

                            selectedList = selectedList + split_str[0];
                            flag = true;
                        }
                    }
                }
            }

            HashMap customTags = new HashMap();
            customTags.put(achar, all);
            customTags.put(rchar, refresh);

            TTYDisplay.clearScreen();
            TTYDisplay.clear(1);
            TTYDisplay.printTitle(title);
            TTYDisplay.printSubTitle(desc);

            TTYDisplay.setNavEnabled(true);
            this.cancelDirection = false;

	    if (optionsArr == null ||
		optionsArr.length == 0) {
		TTYDisplay.printError(empty_txt);
		TTYDisplay.promptForEntry(continue_txt);
                this.cancelDirection = true;
		return null;
	    }


            List nodeList = TTYDisplay.getScrollingOptions(nodeselect_txt,
                    optionsArr, customTags, selectedList, help_text, true);

            if (nodeList == null) {
                this.cancelDirection = true;

                return null;
            }

            // Set the Hostname List
            TTYDisplay.clear(1);

            if (nodeList.contains(all)) {
                nodeListArr = optionsArr;
            } else if (nodeList.contains(refresh)) {
                optionsArr = null;
                nodesOk = false;

                continue;
            } else {

                // use the list entered by the user
                nodeListArr = (String[]) nodeList.toArray(new String[0]);
            }

            // If multiple zones are selected from the same node - error out
            List tmpList = new ArrayList();

            for (int j = 0; j < nodeListArr.length; j++) {
                String split_str[] = nodeListArr[j].split(Util.COLON);

                if (tmpList.contains(split_str[0])) {
                    nodesOk = false;

                    break;
                } else {
                    tmpList.add(split_str[0]);
                }
            }

            if (!nodesOk) {
                TTYDisplay.printError(wizardi18n.getString(
                        "dswizards.error.multipleZonesSelection"));
                TTYDisplay.promptForEntry(continue_txt);

                continue;
            }

            String[] baseClusterNodeList = null;
	    ArrayList baseClusterNodeArray = null;
	    // Check if the required MBean is available on the nodes
	    // selected. If not, error out with a proper error message
	    if (selectedCluster != null &&
	        selectedCluster.equals(Util.ZONE_CLUSTER)) {
		//for a zone cluster, the nodelist corresponds to the
		//public hostnames of the zones.
		//get the base cluster nodenames given the public hostnames

		HashMap hostNodeMap = null;

		if (zcData != null) {
		    hostNodeMap = zcData.getHostNodeMap();
		}

		if (hostNodeMap != null) {
		    for (int i = 0; i < nodeListArr.length; i++) {
			String racNode = nodeListArr[i];
			boolean found = false;
			Set keySet = hostNodeMap.keySet();
			Iterator iterator = keySet.iterator();
			while (iterator.hasNext() && !found) {
			    String hostName = (String)iterator.next();
			    if (racNode.equals(hostName)) {
				found = true;
				if (baseClusterNodeArray == null) {
				    baseClusterNodeArray = new ArrayList();
				}
				baseClusterNodeArray.add((String) hostNodeMap.get(hostName));
			    }
			}
		    }
		}

		if (baseClusterNodeArray != null) {
		    baseClusterNodeList = (String [])baseClusterNodeArray.toArray(new String[0]);
		}
	    } else {
		baseClusterNodeList = nodeListArr;
	    }
	    
	    wizardModel.setWizardValue( Util.BASE_CLUSTER_NODELIST, baseClusterNodeList);

            // Check if the required MBean is available on the nodes
            // selected. If not, error out with a proper error message
            String error = checkMBeanOnNodes(baseClusterNodeList, mBeanToCheck,
                    packageName);

            if (error != null) {
                nodesOk = false;
                TTYDisplay.printError(error);
                TTYDisplay.promptForEntry(continue_txt);

                continue;
            } else {

                // check if the nodelist includes the local nodes
                // if yes set NODE_TO_CONNECT as the localnode
                boolean localNodeInNodeList = false;

                try {
                    InetAddress currNodeAddress = Inet4Address.getLocalHost();
                    String localNode = currNodeAddress.getHostName();

                    for (int i = 0; i < baseClusterNodeList.length; i++) {
                        String node_i = baseClusterNodeList[i];

                        if (node_i.equalsIgnoreCase(localNode)) {
                            wizardModel.setWizardValue(Util.NODE_TO_CONNECT,
                                localNode);
                            localNodeInNodeList = true;

                            break;
                        }
                    }
                } catch (Exception e) {

                }

                if (!localNodeInNodeList) {
                    wizardModel.setWizardValue(Util.NODE_TO_CONNECT,
                        baseClusterNodeList[0]);
                }

                // check if the nodelist includes any zones
                boolean zonesPresent = false;
                int i = 0;

                while ((i < nodeListArr.length) && !zonesPresent) {

                    if (nodeListArr[i].indexOf(Util.COLON) != -1) {
                        zonesPresent = true;
                    } else {
                        i++;
                    }
                }

                if (zonesPresent && !zonesEnable(mBeanToCheck)) {
                    nodesOk = false;
                    TTYDisplay.printError(wizardi18n.getString(
                            "dswizards.error.zonesSelection"));
                    TTYDisplay.promptForEntry(continue_txt);
                    continue;
                }

		if (zonesPresent && !allSameIPType(nodeListArr)) {
		    TTYDisplay.printError(wizardi18n.getString(
			"dswizards.error.mixedZonesSelection"));
		    TTYDisplay.promptForEntry(continue_txt);
		    continue;
		}

                if (zonesPresent) {
                    wizardModel.setWizardValue(
                        HASPWizardConstants.ZONES_PRESENT,
                        HASPWizardConstants.YES);
                    wizardModel.setWizardValue(
                        HASPWizardConstants.HASP_LOCATION_PREF,
                        HASPWizardConstants.FS_ONLY);
                } else {
                    wizardModel.setWizardValue(
                        HASPWizardConstants.ZONES_PRESENT,
                        HASPWizardConstants.NO);
                }
            }
        } while (!nodesOk);

        return nodeListArr;
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#isDisplayComplete
     */
    public boolean isDisplayComplete() {
        return true;
    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }

    private InfrastructureMBean getInfrastructureMBean() {
        JMXConnector localConnection = getConnection(Util.getClusterEndpoint());

        if (mbean == null) {

            try {
                mbean = (InfrastructureMBean) TrustedMBeanModel.getMBeanProxy(
                        Util.DOMAIN, localConnection, InfrastructureMBean.class,
                        null, false);
            } catch (Exception e) {
                String wizardExitPrompt = wizardi18n.getString(
                        "cliwizards.continueExit");
                String errorMsg = wizardi18n.getString("dswizards.cacao.down",
                        new Object[] { Util.getClusterEndpoint() });
                TTYDisplay.printError(errorMsg);
                TTYDisplay.promptForEntry(wizardExitPrompt);
                System.exit(1);
            }
        }

        return mbean;
    }

    /**
     * Checks whether the required MBean is accessible on the nodes returns Null
     * if MBeans are accessible, if not returns a proper error String mentioning
     * the user to check if the correct package is installed and whether the
     * required CACAO module is up
     */
    private String checkMBeanOnNodes(String nodeList[], Class mBeanToCheck,
        String packageName) {

        JMXConnector localConnection = getConnection(Util.getClusterEndpoint());

        try {
            InfrastructureMBean mBean = getInfrastructureMBean();
            String errorNodes[] = mBean.checkCacaoagent(nodeList);

            if ((errorNodes != null) && (errorNodes.length > 0)) {
                StringBuffer errorNodesString = new StringBuffer();

                for (int i = 0; i < errorNodes.length; i++) {
                    errorNodesString.append(errorNodes[i]);

                    if (i < (errorNodes.length - 1)) {
                        errorNodesString.append(",");
                    }
                }

                String error = wizardi18n.getString(
                        "dswizards.cacao.communicationFailure",
                        new String[] { errorNodesString.toString() });

                return error;
            }

            errorNodes = mBean.checkMBeans(nodeList, mBeanToCheck);

            if ((errorNodes != null) && (errorNodes.length > 0)) {
                StringBuffer errorNodesString = new StringBuffer();

                for (int i = 0; i < errorNodes.length; i++) {
                    errorNodesString.append(errorNodes[i]);

                    if (i < (errorNodes.length - 1)) {
                        errorNodesString.append(",");
                    }
                }

                String error = wizardi18n.getString("dswizards.error.pkg",
                        new Object[] {
                            packageName, errorNodesString.toString()
                        });

                return error;
            }
        } catch (Exception e) {
            String wizardExitPrompt = wizardi18n.getString(
                    "cliwizards.continueExit");
            String errorMsg = wizardi18n.getString("dswizards.cacao.down",
                    new Object[] { Util.getClusterEndpoint() });
            TTYDisplay.printError(errorMsg);
            TTYDisplay.promptForEntry(wizardExitPrompt);
            System.exit(1);
        }

        return null;
    }

    /**
     * Checks whether zones are supported for the given dataservice returns a
     * boolean
     */
    private boolean zonesEnable(Class mBeanToCheck) {

        ServiceConfig mBean = null;
        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);
        JMXConnector connector = getConnection(nodeEndPoint);
        boolean retVal = false;

        try {
            mBean = (ServiceConfig) TrustedMBeanModel.getMBeanProxy(Util.DOMAIN,
                    connector, mBeanToCheck, null, false);

            HashMap retMap = mBean.discoverPossibilities(
                    new String[] { Util.ZONE_ENABLE }, null);
            Boolean value = (Boolean) retMap.get(Util.ZONE_ENABLE);

            if (value.booleanValue()) {
                retVal = true;
            }
        } catch (Exception e) {
        }

        return retVal;
    }


    /**
     * Homogeneity check for exclusive IP zones.  If one entry in
     * nodeList is an exclusive IP type zone, then all must be.
     *
     * @param  nodeList[]  RG nodelist array; each element is "node" or
     *			    "node:zone"
     * @return true if the homogeneity check passes; false if there are mixed IP
     *		types present
     */
    private boolean allSameIPType(String nodeList[]) {

	// If multiple zones are selected of different IP-type - error out
	// (i.e., if one zone is IP-type=exclusive, then all must be)
	boolean exclusiveZones = false;
	boolean nonExclusiveZones = false;
	for (int i = 0; i < nodeList.length; i++) {
	    String nodeKey = nodeList[i];

	    // is it a zone? is it exclusive IP zone?
	    if (isZone(nodeKey) &&
		isExclusiveIPZone(getZoneMBean(nodeKey))) {

		wizardModel.setWizardValue(Util.EXCLUSIVEIP, Boolean.TRUE);
		exclusiveZones = true;
	    } else {
		// regular node, or non-exclusive IP zone
		nonExclusiveZones = true;
	    }
	}
	if (exclusiveZones && nonExclusiveZones) {
	    return false;
	}

	return true;
    }


    /**
     * @return  true if the given nodeKey is a zone
     */
    public static boolean isZone(String nodeKey) {
        // if nodeKey contains a colon this is a zonename
        int index = nodeKey.indexOf(Util.COLON);
        return (index != -1);
    }


    /**
     * @return  true if the zone is IP type = exclusive
     */
    public static boolean isExclusiveIPZone(ZoneMBean mBean) {

        try {
            String type = mBean.getIpType();
            if (type.equals("excl"))
                return true;
        } catch (Exception e) {
        }

        return false;
    }


    /**
     * @return  Handle to the Zone MBean on a specified node
     */
    public ZoneMBean getZoneMBean(String nodeKey) {

        // Only if there is no existing reference
        // Get Handler to LogicalHostMBean
        ZoneMBean mbean = null;
        JMXConnector connector = getConnection(Util.getClusterEndpoint());

        try {
            String domain = ZoneMBean.class.getPackage().getName();
	    mbean = (ZoneMBean)TrustedMBeanModel.getMBeanProxy(
		domain, connector, ZoneMBean.class, nodeKey, false);
	    return mbean;
        } catch (Exception e) {
            TTYDisplay.initialize();
            // Report Error
	    TTYDisplay.printError(wizardi18n.getString(
		"dswizards.cacao.communicationFailure",
		new String[] { nodeKey }));
	    TTYDisplay.promptForEntry(continue_txt);
        }

	return null;
    }
}
