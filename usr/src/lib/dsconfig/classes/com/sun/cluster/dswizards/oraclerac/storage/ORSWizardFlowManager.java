/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ORSWizardFlowManager.java 1.10     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.oraclerac.storage;

// Wizard Common
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.dswizards.common.DSConfigWizardModel;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.DSWizardInterface;


/**
 * The class takes decision on which panel to show "next" depending upon the
 * conditions set at a particular context.
 */
public class ORSWizardFlowManager implements DSWizardInterface {

    /**
     * Creates a new instance of ORSFlowManager
     */
    public ORSWizardFlowManager() {
    }

    /**
     * @see  com.sun.cluster.dswizards.common.DSWizardInterface#getNextPanel
     */
    public String getNextPanel(String curPanelName, Object wizCxtObj,
        String options[]) {

	DSConfigWizardModel guiModel = null;
	CliDSConfigWizardModel cliModel = null;

	if (wizCxtObj instanceof CliDSConfigWizardModel) {
	    cliModel = (CliDSConfigWizardModel)wizCxtObj;
	} else if (wizCxtObj instanceof DSConfigWizardModel) {
	    guiModel = (DSConfigWizardModel)wizCxtObj;
	}

        // If Current Panel is Intro Panel
        if (curPanelName.equals(ORSWizardConstants.INTROPANEL)) {
            Object showRACDevices = null;
            Object showRACFS = null;
            Boolean isStateFileAbsent = null;

	    if (guiModel != null) {
		showRACDevices = guiModel.getWizardValue(
                    ORSWizardConstants.SHOW_RAC_DEVICES);

		showRACFS = guiModel.getWizardValue(
                    ORSWizardConstants.SHOW_RAC_FS);

		isStateFileAbsent = (Boolean)guiModel.getWizardValue(
                    Util.RAC_STATE_FILE_ABSENT);
	    } else {
		showRACDevices = cliModel.getWizardValue(
                    ORSWizardConstants.SHOW_RAC_DEVICES);

		showRACFS = cliModel.getWizardValue(
                    ORSWizardConstants.SHOW_RAC_FS);

		isStateFileAbsent = (Boolean)cliModel.getWizardValue(
                    Util.RAC_STATE_FILE_ABSENT);
	    }

            if ((isStateFileAbsent != null) &&
                    isStateFileAbsent.booleanValue()) {
                return options[0];
            } else if (showRACDevices != null) {
                return options[1];
            } else if (showRACFS != null) {
                return options[2];
            }
        } else if (curPanelName.equals(ORSWizardConstants.LOCATIONPANEL)) {
            String showRACDevices = null;
            String showRACFS = null;

	    if (guiModel != null) {
		showRACDevices = (String)guiModel.getWizardValue(
                    ORSWizardConstants.SHOW_RAC_DEVICES);

		showRACFS = (String)guiModel.getWizardValue(
                    ORSWizardConstants.SHOW_RAC_FS);
	    } else {
		showRACDevices = (String)cliModel.getWizardValue(
                    ORSWizardConstants.SHOW_RAC_DEVICES);

		showRACFS = (String)cliModel.getWizardValue(
                    ORSWizardConstants.SHOW_RAC_FS);
	    }

            if ((showRACDevices != null) && showRACDevices.equals("true")) {

                // display the device selection panel
                return options[0];
            } else if ((showRACFS != null) && showRACFS.equals("true")) {
                return options[1];
            }
        } else if (curPanelName.equals(ORSWizardConstants.DISKPANEL)) {
	    // If Current Panel is RAC Disks Panel
            Object showClusterDisksPanel = null;
            String showRACFS = null;

	    if (guiModel != null) {
		showClusterDisksPanel = guiModel.getWizardValue(
                    ORSWizardConstants.SHOW_CLUSTER_DEVICES);

		showRACFS = (String)guiModel.getWizardValue(
                    ORSWizardConstants.SHOW_RAC_FS);
	    } else {
		showClusterDisksPanel = cliModel.getWizardValue(
                    ORSWizardConstants.SHOW_CLUSTER_DEVICES);

		showRACFS = (String)cliModel.getWizardValue(
                    ORSWizardConstants.SHOW_RAC_FS);
	    }

            // If the user had selected create new in the menu, go to the
            // ClusterDisksPanel
            if (showClusterDisksPanel != null) {
                return options[0];
            }
            // If user is done with configuring Disks, go to the RACFilesystems
            // Panel if he had selected QFS or NAS in the Framework
            // wizard/Location Panel
            else if ((showRACFS != null) && showRACFS.equals("true")) {
                return options[1];
            }
            // If the user is done with configuring Disks, go to the Review
            // Panel if filesystems need not be configured
            else {
                return options[2];
            }
        } else if (curPanelName.equals(ORSWizardConstants.FSPANEL)) {
	    // If user is in Filesystems Panel
            Object showClusterFSPanel = null;

	    if (guiModel != null) {
		showClusterFSPanel = guiModel.getWizardValue(
                    ORSWizardConstants.SHOW_CLUSTER_FS);
	    } else {
		showClusterFSPanel = cliModel.getWizardValue(
                    ORSWizardConstants.SHOW_CLUSTER_FS);
	    }

            // If the user had selected create new in the menu, go to the
            // ClusterDisksPanel
            if (showClusterFSPanel != null) {
                return options[0];
            }
            // Goto review panel is user is done with the configuring FS
            else {
                return options[1];
            }
        }

        return null;
    }
}
