/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)HAOracleReviewPanel.java 1.14     08/12/10 SMI"
 */


package com.sun.cluster.dswizards.haoracle;

import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.haoracle.HAOracleMBean;
import com.sun.cluster.agent.dataservices.hastorageplus.HAStoragePlusMBean;
import com.sun.cluster.agent.dataservices.logicalhost.LogicalHostMBean;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.rgm.property.ResourceProperty;
import com.sun.cluster.agent.rgm.property.ResourcePropertyBoolean;
import com.sun.cluster.agent.rgm.property.ResourcePropertyEnum;
import com.sun.cluster.agent.rgm.property.ResourcePropertyInteger;
import com.sun.cluster.agent.rgm.property.ResourcePropertyString;
import com.sun.cluster.agent.rgm.property.ResourcePropertyStringArray;
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.ResourcePropertyUtil;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.hastorageplus.HASPWizardConstants;
import com.sun.cluster.dswizards.hastorageplus.HASPWizardCreator;
import com.sun.cluster.dswizards.logicalhost.LogicalHostWizardConstants;
import com.sun.cluster.dswizards.logicalhost.LogicalHostWizardCreator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.management.remote.JMXConnector;


/**
 * The Panel Class that gets a Hostname or list of Hostnames as input from the
 * User.
 */
public class HAOracleReviewPanel extends WizardLeaf {

    private static String NAME = "NAME";
    private static String VALUE = "VALUE";
    private static String TAG = "TAG";
    private static String UTILNAME = "UTILNAME";
    private static String HAORACONST_NAME = "HAORACONST_NAME";
    private static String ERRORSTR = "ERRORSTR";
    private static String CANEDIT = "CANEDIT";
    private static String CANTEDIT_ERROR = "CANTEDIT_ERROR";
    private String panelName; // Panel Name
    private WizardI18N wizardi18n; // Message Bundle
    private CliDSConfigWizardModel wizardModel; // WizardModel

    private boolean bConfigListener = false;
    private boolean bConfigServer = false;

    private HAOracleMBean mbean = null;
    private JMXConnector localConnection = null;

    private ArrayList assignedResNames = null;
    private ArrayList assignedRgNames = null;

    /**
     * Creates a new instance of HAOracleReviewPanel
     */
    public HAOracleReviewPanel() {
        super();
    }

    /**
     * Creates a HostnameEntry Panel with the given name and manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     * @param  flow  Handler to the WizardFlow
     */
    public HAOracleReviewPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates a HostnameEntry Panel with the given name and Wizard state
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     */
    public HAOracleReviewPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;
        this.wizardi18n = wizardi18n;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.
     *		WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        String back = wizardi18n.getString("ttydisplay.back.char");
        String yes = wizardi18n.getString("cliwizards.yes");
        String no = wizardi18n.getString("cliwizards.no");
        String title = wizardi18n.getString("haoracle.reviewpanel.title");
        String description = wizardi18n.getString("haoracle.reviewpanel.desc1");
        String server_rsname_txt = wizardi18n.getString(
                "haoracle.reviewpanel.server.name");
        String listener_rsname_txt = wizardi18n.getString(
                "haoracle.reviewpanel.listener.name");
        String lh_rsname_txt = wizardi18n.getString(
                "haoracle.reviewpanel.lh.name");
        String hasp_rsname_txt = wizardi18n.getString(
                "haoracle.reviewpanel.hasp.name");
        String rgname_txt = wizardi18n.getString(
                "haoracle.reviewpanel.rg.name");
        String table_title = wizardi18n.getString("haoracle.reviewpanel.desc2");
        String heading1 = wizardi18n.getString("haoracle.reviewpanel.heading1");
        String heading2 = wizardi18n.getString("haoracle.reviewpanel.heading2");
        String dchar = (String) wizardi18n.getString("ttydisplay.d.char");
        String done = wizardi18n.getString("ttydisplay.menu.done");
        String propNameText = wizardi18n.getString(
                "haoracle.reviewpanel.propname");
        String propTypeText = wizardi18n.getString(
                "haoracle.reviewpanel.proptype");
        String propDescText = wizardi18n.getString(
                "haoracle.reviewpanel.propdesc");
        String currValText = wizardi18n.getString(
                "haoracle.reviewpanel.currval");
        String newValText = wizardi18n.getString("haoracle.reviewpanel.newval");
        String rsname_error = wizardi18n.getString(
                "haoracle.reviewpanel.resourcename.error");
        String confirm_txt = wizardi18n.getString("cliwizards.enterAgain");
        String rgname_error = wizardi18n.getString(
                "haoracle.reviewpanel.rgname.error");
        String help_text = wizardi18n.getString("hasp.reviewpanel.cli.help");
        String continue_txt = wizardi18n.getString("cliwizards.continue");
        String rgcantedit = wizardi18n.getString(
                "haoracle.reviewpanel.rgname.cantedit");
        String hasprscantedit = wizardi18n.getString(
                "haoracle.reviewpanel.hasp.resource.cantedit");
        String lhrscantedit = wizardi18n.getString(
                "haoracle.reviewpanel.lh.resource.cantedit");
        String rsname_assigned = wizardi18n.getString(
                "reviewpanel.resourcename.assigned");
        String rgname_assigned = wizardi18n.getString(
                "reviewpanel.rgname.assigned");
        String name_invalid = wizardi18n.getString("reviewpanel.name.invalid");

        boolean bNewRG = false;
        boolean bNewHaspRS = false;
        boolean bNewLhRS = false;

        // Title
        TTYDisplay.printTitle(title);

        // Review panel table
        Vector reviewPanel = new Vector();

        String preference = (String) wizardModel.getWizardValue(
                HAOracleWizardConstants.HAORACLE_PREF);

        if (preference.equals(HAOracleWizardConstants.SERVER_ONLY)) {
            bConfigServer = true;
        } else if (preference.equals(
                    HAOracleWizardConstants.SERVER_AND_LISTENER)) {
            bConfigServer = true;
            bConfigListener = true;
        } else if (preference.equals(HAOracleWizardConstants.LISTENER_ONLY)) {
            bConfigListener = true;
        }

        // Generate the Resource and the ResourceGroup names
        // get the user selections
        String sel_orasid = (String) wizardModel.getWizardValue(
                HAOracleWizardConstants.SEL_ORACLE_SID);

        String serverRsName = null;
        String listenerRsName = null;
        String haspRsName = null;
        String lhRsName = null;

        if (mbean == null) {
            String nodeEndPoint = (String) wizardModel.getWizardValue(
                    Util.NODE_TO_CONNECT);
            JMXConnector connector = getConnection(nodeEndPoint);
            mbean = HAOracleWizardCreator.getHAOracleMBeanOnNode(connector);
        }

        String properties[] = { Util.RS_NAME };
        HashMap discoveredMap = null;

        if (bConfigServer) {
            HashMap helperData = new HashMap();
            helperData.put(Util.RTNAME, Util.ORA_SERVER_RTNAME);
            helperData.put(Util.ORACLE_SID, sel_orasid);
            discoveredMap = mbean.discoverPossibilities(properties, helperData);
            serverRsName = (String) discoveredMap.get(Util.RS_NAME);
            wizardModel.setWizardValue(HAOracleWizardConstants.SERVER_RS_NAME,
                serverRsName);
        }

        if (bConfigListener) {
            HashMap helperData = new HashMap();
            helperData.put(Util.RTNAME, Util.ORA_LISTENER_RTNAME);
            discoveredMap = mbean.discoverPossibilities(properties, helperData);
            listenerRsName = (String) discoveredMap.get(Util.RS_NAME);
            wizardModel.setWizardValue(HAOracleWizardConstants.LISTENER_RS_NAME,
                listenerRsName);
        }

        String filesystemMountPoints[] = (String[]) wizardModel.getWizardValue(
                HAOracleWizardConstants.NEW_FS_DEFINED);

        String globalDevicePaths[] = (String[]) wizardModel.getWizardValue(
                HAOracleWizardConstants.NEW_DEVICES_DEFINED);

        String hostNameList[] = (String[]) wizardModel.getWizardValue(
                HAOracleWizardConstants.NEW_HOSTNAMES_DEFINED);

        Vector existingHASPRS = (Vector) wizardModel.getWizardValue(
                HAOracleWizardConstants.SEL_HASP_RS);

        Vector existingLHRS = (Vector) wizardModel.getWizardValue(
                HAOracleWizardConstants.SEL_LH_RS);


        if ((filesystemMountPoints == null) && (globalDevicePaths == null)) {

            // user is not creating a new HAStoragePlus resource
            // get list of hastorageplus resources selected
            bNewHaspRS = false;
            wizardModel.setWizardValue(HAOracleWizardConstants.HASP_RS_NAME,
                convertArrayToString(
                    (String[]) existingHASPRS.toArray(new String[0])));
        } else {
            bNewHaspRS = true;

            HashMap helperData = new HashMap();
            helperData.put(Util.HASP_ALL_FILESYSTEMS, filesystemMountPoints);
            helperData.put(Util.HASP_ALL_DEVICES, globalDevicePaths);
            localConnection = getConnection(Util.getClusterEndpoint());

            HAStoragePlusMBean haspMBean = HASPWizardCreator
                .getHAStoragePlusMBean(localConnection);
            discoveredMap = haspMBean.discoverPossibilities(properties,
                    helperData);
            haspRsName = (String) discoveredMap.get(Util.RS_NAME);
            wizardModel.setWizardValue(HAOracleWizardConstants.HASP_RS_NAME,
                haspRsName);
        }

        if (hostNameList == null) {

            // user is not creating new Logical Host resource
            bNewLhRS = false;
            wizardModel.setWizardValue(HAOracleWizardConstants.LH_RS_NAME,
                convertArrayToString(
                    (String[]) existingLHRS.toArray(new String[0])));
        } else {

            // Resource Name
            bNewLhRS = true;
            localConnection = getConnection(Util.getClusterEndpoint());

            LogicalHostMBean lhMBean = LogicalHostWizardCreator
                .getLogicalHostMBean(localConnection);
            String disResourcePropertyName[] = { Util.RESOURCENAME };
            discoveredMap = lhMBean.discoverPossibilities(
                    disResourcePropertyName,
                    convertArrayToString(hostNameList));
            lhRsName = (String) discoveredMap.get(Util.RESOURCENAME);
            wizardModel.setWizardValue(HAOracleWizardConstants.LH_RS_NAME,
                lhRsName);
        }


        String rgName = (String) wizardModel.getWizardValue(
                HAOracleWizardConstants.SEL_HASP_RG);

        if (rgName == null) {
            rgName = (String) wizardModel.getWizardValue(
                    HAOracleWizardConstants.SEL_LH_RG);
        }

        if (rgName == null) {

            // a new RG is being created
            bNewRG = true;

            // Resource Group Name
            properties = new String[] { Util.RG_NAME };
            discoveredMap = mbean.discoverPossibilities(properties, null);
            rgName = (String) discoveredMap.get(Util.RG_NAME);
        }

        wizardModel.setWizardValue(Util.RG_NAME, rgName);

        // Get the names of all the properties
        String propertyNames[] = (String[]) wizardModel.getWizardValue(
                Util.PROPERTY_NAMES);

        HashMap tableMap = new HashMap();
        HashMap selMap = null;
        String option = "";
        int row_count = 1;

        while (!option.equals(dchar)) {
            assignedResNames = new ArrayList();
            assignedRgNames = new ArrayList();

            row_count = 1;
            TTYDisplay.setNavEnabled(true);
            this.cancelDirection = false;

            reviewPanel.removeAllElements();
            wizardModel.selectCurrWizardContext();

            tableMap = new HashMap();

            // First Row - Server Resource Name
            if (bConfigServer) {
                serverRsName = (String) wizardModel.getWizardValue(
                        HAOracleWizardConstants.SERVER_RS_NAME);
                reviewPanel.add(server_rsname_txt);
                reviewPanel.add(serverRsName);
                selMap = new HashMap();
                selMap.put(NAME, server_rsname_txt);
                selMap.put(VALUE, serverRsName);
                selMap.put(TAG, "server");
                selMap.put(UTILNAME, Util.RS_NAME);
                selMap.put(HAORACONST_NAME,
                    HAOracleWizardConstants.SERVER_RS_NAME);
                selMap.put(ERRORSTR, rsname_error);
                selMap.put(CANEDIT, new Boolean(true));
                selMap.put(CANTEDIT_ERROR, "");
                tableMap.put(Integer.toString(row_count++), selMap);

                if (!assignedResNames.contains(serverRsName)) {
                    assignedResNames.add(serverRsName);
                }
            }

            // Second Row - Listener Resource Name
            if (bConfigListener) {
                listenerRsName = (String) wizardModel.getWizardValue(
                        HAOracleWizardConstants.LISTENER_RS_NAME);
                reviewPanel.add(listener_rsname_txt);
                reviewPanel.add(listenerRsName);
                selMap = new HashMap();
                selMap.put(NAME, listener_rsname_txt);
                selMap.put(VALUE, listenerRsName);
                selMap.put(TAG, "listener");
                selMap.put(UTILNAME, Util.RS_NAME);
                selMap.put(HAORACONST_NAME,
                    HAOracleWizardConstants.LISTENER_RS_NAME);
                selMap.put(ERRORSTR, rsname_error);
                selMap.put(CANEDIT, new Boolean(true));
                selMap.put(CANTEDIT_ERROR, "");
                tableMap.put(Integer.toString(row_count++), selMap);

                if (!assignedResNames.contains(listenerRsName)) {
                    assignedResNames.add(listenerRsName);
                }
            }

            // add hastorageplus resources
            reviewPanel.add(hasp_rsname_txt);
            haspRsName = (String) wizardModel.getWizardValue(
                    HAOracleWizardConstants.HASP_RS_NAME);
            reviewPanel.add(haspRsName);
            selMap = new HashMap();
            selMap.put(NAME, hasp_rsname_txt);
            selMap.put(VALUE, haspRsName);
            selMap.put(TAG, "hasp");
            selMap.put(UTILNAME, Util.RS_NAME);
            selMap.put(HAORACONST_NAME, HAOracleWizardConstants.HASP_RS_NAME);
            selMap.put(ERRORSTR, rsname_error);

            if (bNewHaspRS) {
                selMap.put(CANEDIT, new Boolean(true));
            } else {
                selMap.put(CANEDIT, new Boolean(false));
            }

            selMap.put(CANTEDIT_ERROR, hasprscantedit);
            tableMap.put(Integer.toString(row_count++), selMap);

            if (!assignedResNames.contains(haspRsName)) {
                assignedResNames.add(haspRsName);
            }

            // add logicalhost resource
            reviewPanel.add(lh_rsname_txt);
            lhRsName = (String) wizardModel.getWizardValue(
                    HAOracleWizardConstants.LH_RS_NAME);
            reviewPanel.add(lhRsName);
            selMap = new HashMap();
            selMap.put(NAME, lh_rsname_txt);
            selMap.put(VALUE, lhRsName);
            selMap.put(TAG, "lh");
            selMap.put(UTILNAME, Util.RS_NAME);
            selMap.put(HAORACONST_NAME, HAOracleWizardConstants.LH_RS_NAME);
            selMap.put(ERRORSTR, rsname_error);

            if (bNewLhRS) {
                selMap.put(CANEDIT, new Boolean(true));
            } else {
                selMap.put(CANEDIT, new Boolean(false));
            }

            selMap.put(CANTEDIT_ERROR, lhrscantedit);
            tableMap.put(Integer.toString(row_count++), selMap);

            if (!assignedResNames.contains(lhRsName)) {
                assignedResNames.add(lhRsName);
            }

            // Next Row - RG Name
            rgName = (String) wizardModel.getWizardValue(Util.RG_NAME);
            reviewPanel.add(rgname_txt);
            reviewPanel.add(rgName);
            selMap = new HashMap();
            selMap.put(NAME, rgname_txt);
            selMap.put(VALUE, rgName);
            selMap.put(TAG, "rg");
            selMap.put(UTILNAME, Util.RG_NAME);
            selMap.put(HAORACONST_NAME, Util.RG_NAME);
            selMap.put(ERRORSTR, rgname_error);

            if (bNewRG) {
                selMap.put(CANEDIT, new Boolean(true));
            } else {
                selMap.put(CANEDIT, new Boolean(false));
            }

            selMap.put(CANTEDIT_ERROR, rgcantedit);
            tableMap.put(Integer.toString(row_count++), selMap);

            if (!assignedRgNames.contains(rgName)) {
                assignedRgNames.add(rgName);
            }

            TTYDisplay.pageText(description);
            TTYDisplay.clear(1);

            String subheadings[] = { heading1, heading2 };

            HashMap customTags = new HashMap();
            customTags.put(dchar, done);
            option = TTYDisplay.create2DTable(table_title, subheadings,
                    reviewPanel, customTags, help_text);

            String desc;

            if (option.equals(back)) {
                this.cancelDirection = true;

                return;
            }

            TTYDisplay.clear(3);
            TTYDisplay.setNavEnabled(false);

            if (!option.equals(dchar)) {
                selMap = (HashMap) tableMap.get(option);

                String name = (String) selMap.get(NAME);
                String value = (String) selMap.get(VALUE);
                String tag = (String) selMap.get(TAG);
                String utilName = (String) selMap.get(UTILNAME);
                String error_str = (String) selMap.get(ERRORSTR);
                boolean canEdit = ((Boolean) selMap.get(CANEDIT))
                    .booleanValue();
                String cantEditErrorMsg = (String) selMap.get(CANTEDIT_ERROR);
                String haoraConstName = (String) selMap.get(HAORACONST_NAME);
                TTYDisplay.pageText(propNameText +
                    wizardi18n.getString(
                        "haoracle.reviewpanel." + tag + ".name"));

                TTYDisplay.pageText(propDescText +
                    wizardi18n.getString(
                        "haoracle.reviewpanel." + tag + ".desc"));

                TTYDisplay.pageText(propTypeText +
                    wizardi18n.getString(
                        "haoracle.reviewpanel." + tag + ".type"));

                TTYDisplay.pageText(currValText + value);

                if (!canEdit) {
                    TTYDisplay.pageText(cantEditErrorMsg);
                    TTYDisplay.clear(1);
                    TTYDisplay.promptForEntry(continue_txt);

                    continue;
                }

                String tmp_name = null;
                String enteragain = yes;
                boolean error = true;

                while (error && enteragain.equals(yes)) {
                    tmp_name = TTYDisplay.promptForEntry(newValText);

                    if ((tmp_name != null) && (tmp_name.trim().length() != 0)) {
                        properties = new String[] { utilName };

                        HashMap userInputs = new HashMap();
                        userInputs.put(utilName, tmp_name);

                        if (utilName.equals(Util.RS_NAME) ||
                                utilName.equals(Util.RG_NAME)) {

                            if (
                                !Pattern.compile("[a-zA-Z0-9[-_]]*").matcher(
                                        tmp_name).matches()) {
                                TTYDisplay.printError(name_invalid);
                                enteragain = TTYDisplay.getConfirmation(
                                        confirm_txt, yes, no);

                                continue;
                            }
                        }

                        ErrorValue retVal = validateInput(properties,
                                userInputs, null);

                        if (!retVal.getReturnValue().booleanValue()) {
                            TTYDisplay.printError(error_str);
                            enteragain = TTYDisplay.getConfirmation(confirm_txt,
                                    yes, no);
                        } else {

                            if (utilName.equals(Util.RS_NAME)) {
                                assignedResNames.remove(value);

                                if (assignedResNames.contains(tmp_name)) {
                                    TTYDisplay.printError(rsname_assigned);
                                    enteragain = TTYDisplay.getConfirmation(
                                            confirm_txt, yes, no);
                                    assignedResNames.add(value);
                                } else {
                                    error = false;
                                    assignedResNames.add(tmp_name);
                                    wizardModel.setWizardValue(haoraConstName,
                                        tmp_name);
                                }
                            } else if (utilName.equals(Util.RG_NAME)) {
                                assignedRgNames.remove(value);

                                if (assignedRgNames.contains(tmp_name)) {
                                    TTYDisplay.printError(rgname_assigned);
                                    enteragain = TTYDisplay.getConfirmation(
                                            confirm_txt, yes, no);
                                    assignedRgNames.add(value);
                                } else {
                                    error = false;
                                    assignedRgNames.add(tmp_name);
                                    wizardModel.setWizardValue(haoraConstName,
                                        tmp_name);
                                }
                            } else {
                                error = false;
                                wizardModel.setWizardValue(haoraConstName,
                                    tmp_name);
                            }
                        }
                    }
                } // while
            }

            TTYDisplay.clear(3);
        }

        wizardModel.setWizardValue(HAOracleWizardConstants.NEW_RG,
            new Boolean(bNewRG));
        wizardModel.setWizardValue(HAOracleWizardConstants.NEW_HASP_RS,
            new Boolean(bNewHaspRS));
        wizardModel.setWizardValue(HAOracleWizardConstants.NEW_LH_RS,
            new Boolean(bNewLhRS));
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#isDisplayComplete
     */
    public boolean isDisplayComplete() {
        return true;
    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }

    private ErrorValue validateInput(String propNames[], HashMap userInput,
        Object helperData) {
        ErrorValue retValue = null;
        retValue = mbean.validateInput(propNames, userInput, helperData);

        return retValue;
    }

    /**
     * Converts a given String Array into a String
     */
    private String convertArrayToString(String array[]) {
        StringBuffer buffer = new StringBuffer();

        if ((array != null) && (array.length > 0)) {

            for (int i = 0; i < array.length; i++) {
                buffer.append(array[i]);

                if (i < (array.length - 1))
                    buffer.append(Util.COMMA);
            }
        }

        return buffer.toString();
    }

}
