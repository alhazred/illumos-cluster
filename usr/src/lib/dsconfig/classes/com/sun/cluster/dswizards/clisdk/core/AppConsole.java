/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)AppConsole.java 1.10     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.clisdk.core;

import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;


/**
 * AppConsole is the interface between wizard panels and the client-side running
 * environment.  This interface facilitates exiting the applet or application,
 * as well as displaying user queries.
 */
public interface AppConsole {

    /**
     * Display a query to the user.  When a selection is made, the corresponding
     * method will be called on the target.
     *
     * @param  target  The object to call when a selection from the dialog is
     * made.
     * @param  message  The message to be displayed to the user.
     * @param  buttons  The buttons (button labels) offered to the user to
     * select from.
     * @param  methods  A list of methods (in the same order as the list of
     * buttons) that will be called when a dialog selection is made.
     *
     * @return  boolean true if the dialog was closed via a button and false if
     * it was forcibly closed
     */
    public boolean displayQuery(Object target, String message, String buttons[],
        String methods[]);

    /**
     * Display a query to the user.  When a selection is made, the corresponding
     * method will be called on the target.
     *
     * @param  target  The object to call when a selection from the dialog is
     * made.
     * @param  title  The title of the dialog that appears
     * @param  message  The message to be displayed to the user.
     * @param  buttons  The buttons (button labels) offered to the user to
     * select from.
     * @param  methods  A list of methods (in the same order as the list of
     * buttons) that will be called when a dialog selection is made.
     *
     * @return  boolean true if the dialog was closed via a button and false if
     * it was forcibly closed
     */
    public boolean displayQuery(Object target, String title, String message,
        String buttons[], String methods[]);

    /**
     * Display a query to the user.  When a selection is made, the corresponding
     * method will be called on the target.
     *
     * @param  target  The object to call when a selection from the dialog is
     * made.
     * @param  showTime  The maximum time this QqueryDiadlog to be displayed
     * @param  title  The title of the dialog that appears
     * @param  message  The message to be displayed to the user.
     * @param  buttons  The buttons (button labels) offered to the user to
     * select from.
     * @param  methods  A list of methods (in the same order as the list of
     * buttons) that will be called when a dialog selection is made.
     */
    public void displayQuery(Object target, int showTime, String title,
        String message, String buttons[], String methods[]);

    /**
     * Sets the model of the wizard frame.
     *
     * @param  state  The new model for the wizard console.
     */
    public void setWizardModel(CliDSConfigWizardModel wizardModel);

    /**
     * Returns the model of the wizard frame.
     *
     * @return  The current wizardModel set in this console
     */
    public CliDSConfigWizardModel getWizardModel();
}
