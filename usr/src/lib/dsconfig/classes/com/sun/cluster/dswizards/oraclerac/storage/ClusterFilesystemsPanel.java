/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)ClusterFilesystemsPanel.java	1.15	09/03/11 SMI"
 */

package com.sun.cluster.dswizards.oraclerac.storage;

// CMASS
import com.sun.cluster.agent.dataservices.utils.Util;

// CLI SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;

// Wizard Common
import com.sun.cluster.dswizards.common.CliUtil;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.oraclerac.invoker.RACInvokerCreator;

// J2SE
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import com.sun.cluster.model.RACModel;

/**
 * This panel lists to the user all the S-QFS,NAS mountpoints in the cluster
 * that are NOT monitored by ScalMountPoint resources
 */
public class ClusterFilesystemsPanel extends WizardLeaf {


    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N messages; // Message Bundle

    private RACModel racModel = null;

    /**
     * Creates a new instance of ClusterFilesystemsPanel
     */
    public ClusterFilesystemsPanel() {
        super();
    }

    /**
     * Creates an ClusterFilesystems Panel with the given name and tree manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public ClusterFilesystemsPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates an ClusterFilesystems Panel for racstorage Wizard with the given
     * name and associates it with the WizardModel and WizardFlow
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     */
    public ClusterFilesystemsPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N messages) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;

        // Get the I18N instance
        this.messages = messages;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {

        String title = messages.getString("racstorage.clusterFSPanel.title");
        String desc = messages.getString("racstorage.clusterFSPanel.desc");
        String subTitle = messages.getString(
                "racstorage.clusterFSPanel.subTitle");
        String mountPointTitle = messages.getString(
                "racstorage.clusterFSPanel.mountPoint");
        String fsNameTitle = messages.getString(
                "racstorage.clusterFSPanel.fsname");
        String help = messages.getString("racstorage.clusterFSPanel.cli.help");
        String dchar = messages.getString("ttydisplay.d.char");
        String done = messages.getString("ttydisplay.menu.done");
        String rchar = messages.getString("ttydisplay.r.char");
        String yes = messages.getString("cliwizards.yes");
        String no = messages.getString("cliwizards.no");
        String confirmMessage = messages.getString("cliwizards.confirmMessage");
        String refresh = messages.getString("ttydisplay.menu.refresh");
        String confirmFS = messages.getString(
                "racstorage.clusterFSPanel.confirmFS");
        String fsTypeTitle = messages.getString(
                "racstorage.clusterFSPanel.fstype");
        String exitOption = no;
        String empty = messages.getString("racstorage.clusterFSPanel.empty");
        String emptyDesc = messages.getString(
                "racstorage.clusterFSPanel.empty.desc");
        String emptySelection = messages.getString(
                "racstorage.clusterFSPanel.emptySelection");
        String continueConfirm = messages.getString(
                "cliwizards.continueConfirm");
        String continue_txt = messages.getString("ttydisplay.msg.cli.enter");
        String back = messages.getString("ttydisplay.back.char");

        String nodeList[] = (String[]) wizardModel.getWizardValue(
                Util.BASE_CLUSTER_NODELIST);
        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);

	racModel = RACInvokerCreator.getRACModel();

	HashMap helperData = new HashMap();
	String selectedZoneCluster = CliUtil.getZoneClusterName(wizardModel);

	if (selectedZoneCluster != null &&
	    selectedZoneCluster.trim().length() > 0) {
	    helperData.put(Util.SEL_ZONE_CLUSTER, selectedZoneCluster);
	    helperData.put(Util.ZONE_PATH,
		(String) wizardModel.getWizardValue(Util.ZONE_PATH));
	}

        do {

            TTYDisplay.clearScreen();
            TTYDisplay.clear(1);

            // Enable Back and Help
            TTYDisplay.setNavEnabled(true);
            this.cancelDirection = false;

            // Title and Description
            TTYDisplay.printTitle(title);
            TTYDisplay.pageText(desc);
            TTYDisplay.clear(1);

            // Populate the table
            Vector fsData = new Vector();

            // Depending on the Storage Schemes get the respective
            // DeviceGroups monitored by them.
            Map qfsMap = null;
            Map nasMap = null;

            qfsMap = (Map) wizardModel.getWizardValue(Util.NONRGM_SQFS);
            nasMap = (Map) wizardModel.getWizardValue(Util.NONRGM_NAS);

            wizardModel.setWizardValue(ORSWizardConstants.SHOW_FS_SUMMARY,
                "false");

            // Depending on the Storage Schemes get the respective
            // filesystems and mountpoints
	    String keyPrefix = (String) wizardModel.getWizardValue(
		Util.KEY_PREFIX);

            List storageScheme = (ArrayList) wizardModel.getWizardValue(
		keyPrefix + Util.ORF_STORAGE_MGMT_SCHEMES);

            Thread t = null;

            try {
                t = TTYDisplay.busy(messages.getString(
                            "racstorage.clusterFSPanel.discovery.busy"));
                t.start();

                if (storageScheme.contains(Util.QFS_SVM) ||
		    storageScheme.contains(Util.QFS_HWRAID)) {

                    if (qfsMap == null) {

                        // Populate the qfsMap according to the qfs scheme
                        boolean qfsOnRaw = storageScheme.contains(
                                Util.QFS_HWRAID);
                        boolean qfsOnOban = storageScheme.contains(
                                Util.QFS_SVM);
                        qfsMap = getMountPoints_forQFS(nodeList, qfsOnRaw,
                                qfsOnOban, helperData);
                        wizardModel.setWizardValue(Util.NONRGM_SQFS, qfsMap);
                    }
                }

                if (storageScheme.contains(Util.NAS) ||
		    storageScheme.contains(Util.SUN_NAS)) {

                    if (nasMap == null) {
                        nasMap = getMountPoints_forNAS(nodeList, helperData);
                        wizardModel.setWizardValue(Util.NONRGM_NAS, nasMap);
                    }
                }

                // The Maps pertaining to the storage schemes ared added to
                // the final vector that is displayed.
                if (qfsMap != null) {
                    Set entrySet = qfsMap.entrySet();

                    for (Iterator i = entrySet.iterator(); i.hasNext();) {
                        Map.Entry entry = (Map.Entry) i.next();
                        String qfsName = (String) entry.getKey();
                        String mountPoint = (String) entry.getValue();
                        fsData.add(qfsName);
                        fsData.add(mountPoint);
                        fsData.add(Util.SQFS);
                    }
                }

                if (nasMap != null) {
                    Set entrySet = nasMap.entrySet();

                    for (Iterator i = entrySet.iterator(); i.hasNext();) {
                        Map.Entry entry = (Map.Entry) i.next();
                        String exportedFS = (String) entry.getKey();
                        String mountPoint = (String) entry.getValue();
                        fsData.add(exportedFS);
                        fsData.add(mountPoint);
                        fsData.add(Util.NAS);
                    }
                }
            } finally {

                try {
                    t.interrupt();
                    t.join();
                } catch (Exception exe) {
                }
            }

            TTYDisplay.clear(1);

            HashMap customTags = new HashMap();
            customTags.put(dchar, done);
            customTags.put(rchar, refresh);

            String subheadings[] = {
                    fsNameTitle, mountPointTitle, fsTypeTitle
                };
            String option[] = null;

            if (fsData.size() > 0) {

                // Display the (n*3) Table
                option = TTYDisplay.create2DTable(subTitle, subheadings, fsData,
                        fsData.size() / 3, 3, customTags, help, empty, false);
            } else {
                TTYDisplay.pageText(subTitle);
                TTYDisplay.clear(1);
                TTYDisplay.displaySubheadings(subheadings);
                TTYDisplay.clear(1);
                TTYDisplay.pageText(empty);
                TTYDisplay.clear(1);
                TTYDisplay.pageText(emptyDesc);
                TTYDisplay.clear(2);

                String choice = TTYDisplay.promptForEntry(continue_txt);

                if (choice.equals(back)) {
                    wizardModel.setWizardValue(
                        ORSWizardConstants.SHOW_CLUSTER_FS, null);
                    this.cancelDirection = true;

                    return;
                }

                continue;
            }

            // Handle Back
            if (option == null) {
                wizardModel.setWizardValue(ORSWizardConstants.SHOW_CLUSTER_FS,
                    null);
                this.cancelDirection = true;

                return;
            }

            List optionsList = Arrays.asList(option);
            List tmpSelected = (ArrayList) wizardModel.getWizardValue(
                    ORSWizardConstants.ORS_FS_SELECTED);

            if (tmpSelected == null) {
                tmpSelected = new ArrayList();
            }

            if (optionsList.contains(rchar)) {

                // refresh the data
                wizardModel.setWizardValue(Util.NONRGM_SQFS, null);
                wizardModel.setWizardValue(Util.NONRGM_NAS, null);

                continue;
            } else {

                // Get the dgResourceNames selected
                Map selectedFSs = new HashMap();

                for (Iterator i = optionsList.iterator(); i.hasNext();) {
                    String optionValue = (String) i.next();

                    try {
                        int value = Integer.parseInt(optionValue);

                        // Get the coressponding fsName from the vector
                        // Also get the mountpoints,fsType
                        String fsName = (String) fsData.get((value * 3) - 3);
                        String mountPoint = (String) fsData.get((value * 3) -
                                2);
                        String fsResourceName =
                            getResourceName_forScalMountPoint(mountPoint, helperData);
                        String fsType = (String) fsData.get((value * 3) - 1);
                        selectedFSs.put(fsName,
                            fsResourceName + "," + mountPoint + "," + fsType);

                        tmpSelected.add(mountPoint);
                    } catch (NumberFormatException nfe) {
                        // Custom Tags
                    }
                }

                // Store selection
                if (optionsList.contains(dchar)) {
                    exitOption = yes;

                    if (selectedFSs.size() == 0) {
                        wizardModel.setWizardValue(Util.NEW_FS_RESOURCES, null);
                        wizardModel.setWizardValue(
                            ORSWizardConstants.SHOW_CLUSTER_FS, null);
                        wizardModel.setWizardValue(
                            ORSWizardConstants.ORS_FS_SELECTED, null);
                    } else {

                        // Store the user selected ScalDeviceGroupResources
                        // in the wizardModel
                        Map prevValue = (HashMap) wizardModel.getWizardValue(
                                Util.NEW_FS_RESOURCES);

                        if (prevValue != null) {

                            // Add previously selected Disksets
                            selectedFSs.putAll(prevValue);
                        }

                        wizardModel.setWizardValue(Util.NEW_FS_RESOURCES,
                            selectedFSs);
                        wizardModel.setWizardValue(
                            ORSWizardConstants.SHOW_CLUSTER_FS, null);
                        wizardModel.setWizardValue(
                            ORSWizardConstants.SHOW_FS_SUMMARY, "true");
                        wizardModel.setWizardValue(
                            ORSWizardConstants.ORS_FS_SELECTED, tmpSelected);
                    }
                }
            }
        } while (!exitOption.equalsIgnoreCase(yes));

        return;
    }

    // Gets a Map of qfs filesystem mountpoints not under any ScalMountPoint
    // Resource for a given nodelist
    private Map getMountPoints_forQFS(String nodelist[], boolean qfsOnRaw,
        boolean qfsOnOban, HashMap helperData) {
        String propertyNames[] = { Util.SQFS };

        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);
	racModel = RACInvokerCreator.getRACModel();

        Map discoveredMap = racModel.discoverPossibilities(
	    null, nodeEndPoint, nodelist, propertyNames, helperData);
        Map nonRgmQFSMap = (HashMap) discoveredMap.get(Util.NONRGM_SQFS);

        return nonRgmQFSMap;
    }

    // Gets a Map of NAS filesystem mountpoints not under any ScalMountPoint
    // Resource for a given nodelist
    private Map getMountPoints_forNAS(String nodelist[], HashMap helperData) {
        String propertyNames[] = { Util.NAS };

        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);
	racModel = RACInvokerCreator.getRACModel();

        Map discoveredMap = racModel.discoverPossibilities(
	    null, nodeEndPoint, nodelist, propertyNames, helperData);
        Map nonRgmQFSMap = (HashMap) discoveredMap.get(Util.NONRGM_NAS);

        return nonRgmQFSMap;
    }

    // Get a unique name for a resource for Scalable Mount Point
    private String getResourceName_forScalMountPoint(String mountPoint, HashMap helperData) {
        String propertyName[] = new String[] { Util.STORAGERESOURCE };

        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);
	racModel = RACInvokerCreator.getRACModel();

	helperData.put(Util.PREFIX, mountPoint);

        Map discoveredMap = racModel.discoverPossibilities(
	    null, nodeEndPoint, propertyName, helperData);

        return (String) discoveredMap.get(Util.STORAGERESOURCE);
    }
}
