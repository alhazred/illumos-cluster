/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)SelectDBPanel.java 1.13     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.sapwebas;

// J2SE
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

// JMX
import javax.management.remote.JMXConnector;

// CMAS
import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.sapwebas.SapWebasMBean;
import com.sun.cluster.agent.dataservices.utils.DataServicesUtil;
import com.sun.cluster.agent.dataservices.utils.Util;

// CLI Wizard SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;

// Wizard Common
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;

/**
 * Introduction page to the Hostname Wizards
 */
public class SelectDBPanel extends WizardLeaf {

    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N wizardi18n; // Message Bundle


    /**
     * Creates a new instance of SelectDBPanel
     */
    public SelectDBPanel() {
        super();
    }

    /**
     * Creates a SelectDBPanel Panel with the given name and tree manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public SelectDBPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    public SelectDBPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;

        // Get the I18N instance
        this.wizardi18n = wizardi18n;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {
        String title = wizardi18n.getString("sap.selectDBPanel.title");
        String query_txt = wizardi18n.getString("sap.selectDBPanel.query");
        String help_text = wizardi18n.getString("sap.selectDBPanel.help.cli");
        String yes = wizardi18n.getString("cliwizards.yes");
        String no = wizardi18n.getString("cliwizards.no");
        String continue_txt = wizardi18n.getString("cliwizards.continue");
        String refreshKey = wizardi18n.getString("ttydisplay.r.char");
        String refreshText = wizardi18n.getString("ttydisplay.menu.refresh");
        String back = wizardi18n.getString("ttydisplay.back.char");
        String heading1 = wizardi18n.getString("sap.dbres.cli.table.col0");
        String heading2 = wizardi18n.getString("sap.dbres.cli.table.col1");
        String selDBKey;
        String discDBKey;
        String enterOwnKey = wizardi18n.getString(
                "custom.menu.enteryourown.key");
        String enterOwnText = wizardi18n.getString(
                "custom.menu.enteryourown.keyText");
        String enterOwnQuery = wizardi18n.getString("sap.dbres.cli.query");

        Vector table = new Vector();
        selDBKey = Util.SELECTED_DB;
        discDBKey = SapWebasConstants.DISCOVERD_DB_RESOURCES;

        HashMap customTag = new HashMap();
        customTag.put(refreshKey, refreshText);
        customTag.put(enterOwnKey, enterOwnText);

        int bnextStep = 0;
        String option;
        boolean repeatLoop = false;
        DataServicesUtil.clearScreen();
        TTYDisplay.clear(1);
        TTYDisplay.printTitle(title);
        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;
        TTYDisplay.clear(1);

        do {
            StringBuffer defaultSelections = new StringBuffer();
            String def = (String) wizardModel.getWizardValue(selDBKey);

            if (def != null) {
                defaultSelections.append(def);
            }

            String optionsArr[] = getConfiguredDBRes(discDBKey);
            String selDBName = null;

            if ((optionsArr != null) && (optionsArr.length > 0)) {
                table = SapWebasWizardCreator.splitArraytoVector(optionsArr);

                Vector currentResources = new Vector(table);

                TTYDisplay.setNavEnabled(true);
                this.cancelDirection = false;

                String subheadings[] = new String[] { heading1, heading2 };

                option = TTYDisplay.create2DTable(query_txt, subheadings, table,
                        customTag, help_text);

                if (option.equals(back)) {
                    this.cancelDirection = true;

                    return;
                }

                TTYDisplay.clear(1);

                int index = 0;

                try {
                    index = Integer.parseInt(option);

                    String selected = optionsArr[index - 1];
                    String splitArr[] = selected.split(Util.COLON);
                    selDBName = splitArr[0];
                } catch (Exception e) {
                    repeatLoop = true;

                    if (option.equals(refreshKey)) {
                        wizardModel.setWizardValue(discDBKey, null);

                        continue;
                    }
                }
            } else {
                option = enterOwnKey;
            }

            if (option.equals(enterOwnKey)) {
                TTYDisplay.setNavEnabled(false);
                selDBName = TTYDisplay.promptForEntry(enterOwnQuery, help_text);
            }

            if ((selDBName == null) || (selDBName.length() <= 0)) {
                repeatLoop = true;
                TTYDisplay.printError(wizardi18n.getString(
                        "sap.dbres.error.nullValue"));

                continue;
            }

            String selectArr[] = { selDBName };

            String nodeEndPoint = (String) wizardModel.getWizardValue(
                    Util.NODE_TO_CONNECT);
            JMXConnector connector = getConnection(nodeEndPoint);
            SapWebasMBean mBean = SapWebasWizardCreator.getMBean(connector);
            HashMap data = new HashMap();
            data.put(Util.SCS_R_NAME, selDBName);

            ErrorValue err = mBean.validateInput(
                    new String[] { Util.SCS_R_NAME }, data, null);

            if (!err.getReturnValue().booleanValue()) {
                repeatLoop = false;
                wizardModel.setWizardValue(selDBKey,
                    SapWebasWizardCreator.convertArraytoString(selectArr));
            } else {
                repeatLoop = true;
                TTYDisplay.printError(wizardi18n.getString(
                        "sap.dbres.error.exists"));
            }

        } while (repeatLoop);
    }

    String[] getConfiguredDBRes(String discKey) {
        SapWebasMBean mBean;
        String optionsArr[] = (String[]) wizardModel.getWizardValue(discKey);

        if (optionsArr == null) {
            String nodeEndPoint = (String) wizardModel.getWizardValue(
                    Util.NODE_TO_CONNECT);
            JMXConnector connector = getConnection(nodeEndPoint);
            mBean = SapWebasWizardCreator.getMBean(connector);

            HashMap values = mBean.discoverPossibilities(
                    new String[] { Util.DB_RSLIST }, null);
            List retList = (List) values.get(Util.DB_RSLIST);
            optionsArr = (String[]) retList.toArray(new String[0]);
            wizardModel.setWizardValue(discKey, optionsArr);
        }

        return optionsArr;
    }

}
