/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)Rac10GReviewPanel.java	1.16	09/03/11 SMI"
 */


package com.sun.cluster.dswizards.oraclerac.database.ver10g;

// CMASS
import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.utils.Util;

// CLI wizard
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.common.CliUtil;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.oraclerac.database.*;
import com.sun.cluster.dswizards.oraclerac.invoker.RACInvokerCreator;

// Model
import com.sun.cluster.model.RACModel;

// Java
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

/**
 * The Panel Class that displays the review panel.
 */
public class Rac10GReviewPanel extends RacDBBasePanel {

    private static String NAME = "NAME";
    private static String VALUE = "VALUE";
    private static String DESC = "DESC";
    private static String TYPE = "TYPE";
    private static String UTILNAME = "UTILNAME";
    private static String VALIDATENAME = "VALIDATENAME";
    private static String ERRORSTR = "ERRORSTR";
    private static String CANEDIT = "CANEDIT";
    private static String CANTEDIT_ERROR = "CANTEDIT_ERROR";

    private ArrayList assignedResNames = null;
    private ArrayList assignedRgNames = null;

    /**
     * Creates a Rac10GReviewPanel with the given name and Wizard state
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     */
    public Rac10GReviewPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(name, model, wizardFlow, wizardi18n);
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.
     *		WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        title = wizardi18n.getString("oraclerac10g.reviewPanel.title");
        desc = wizardi18n.getString("oraclerac10g.reviewPanel.desc1");

        String rgname_txt = wizardi18n.getString(
                "oraclerac10g.reviewPanel.rg.name");
        String crsframework_rsname_txt = wizardi18n.getString(
                "oraclerac10g.reviewPanel.crsframeworkresource.name");
        String crsframework_rsname_desc = wizardi18n.getString(
                "oraclerac10g.reviewPanel.crsframeworkresource.desc");
        String crsframework_rsname_type = wizardi18n.getString(
                "oraclerac10g.reviewPanel.crsframeworkresource.type");
        String racframework_rgname_txt = wizardi18n.getString(
                "oraclerac10g.reviewPanel.racframeworkrg.name");
        String racframework_rgname_desc = wizardi18n.getString(
                "oraclerac10g.reviewPanel.racframeworkrg.desc");
        String racframework_rgname_type = wizardi18n.getString(
                "oraclerac10g.reviewPanel.racframeworkrg.type");
        String proxy_rsname_txt = wizardi18n.getString(
                "oraclerac10g.reviewPanel.proxyresource.name");
        String proxy_rsname_desc = wizardi18n.getString(
                "oraclerac10g.reviewPanel.proxyresource.desc");
        String proxy_rsname_type = wizardi18n.getString(
                "oraclerac10g.reviewPanel.proxyresource.type");
        String proxy_rgname_txt = wizardi18n.getString(
                "oraclerac10g.reviewPanel.proxyrg.name");
        String proxy_rgname_desc = wizardi18n.getString(
                "oraclerac10g.reviewPanel.proxyrg.desc");
        String proxy_rgname_type = wizardi18n.getString(
                "oraclerac10g.reviewPanel.proxyrg.type");
        String table_title = wizardi18n.getString(
                "oraclerac10g.reviewPanel.desc2");
        String heading1 = wizardi18n.getString(
                "oraclerac10g.reviewPanel.heading1");
        String heading2 = wizardi18n.getString(
                "oraclerac10g.reviewPanel.heading2");
        String newValText = wizardi18n.getString(
                "oraclerac10g.reviewPanel.newval");
        String rsname_error = wizardi18n.getString(
                "oraclerac10g.reviewPanel.rsname.error");
        String rgname_error = wizardi18n.getString(
                "oraclerac10g.reviewPanel.rgname.error");
        String rsname_assigned = wizardi18n.getString(
                "reviewpanel.resourcename.assigned");
        String rgname_assigned = wizardi18n.getString(
                "reviewpanel.rgname.assigned");
        String cantedit_error = wizardi18n.getString(
                "oraclerac10g.reviewPanel.rgname.cantedit");

        String help_text = wizardi18n.getString(
                "oraclerac10g.reviewPanel.cli.help");

        // Title
        TTYDisplay.printTitle(title);

        // Review panel table
        Vector reviewPanel = new Vector();

        // Generate the Resource and the ResourceGroup names
        String crsFrameworkRSName = null;
        String racFrameworkRGName = null;
        String racProxyRSName = null;
        String racProxyRGName = null;

        String properties[] = null;
        HashMap discoveredMap = null;

        // check if there is any CRS framework resource configured
        boolean crsFrameworkPresent =
            ((Boolean)wizardModel.getWizardValue(Util.CRS_FRAMEWORK_PRESENT))
            .booleanValue();

	RACModel racModel = RACInvokerCreator.getRACModel();
	String nodeEndpoint = (String)wizardModel.getWizardValue(Util.NODE_TO_CONNECT);
	HashMap helperData = new HashMap();

        String selZoneCluster = CliUtil.getZoneClusterName(wizardModel);
	helperData.put(Util.SEL_ZONE_CLUSTER, selZoneCluster);

        if (!crsFrameworkPresent) {
            properties = new String[] { Util.CRS_FRAMEWORK_RSNAME };
            discoveredMap = racModel.discoverPossibilities(null, nodeEndpoint, properties, helperData);
            crsFrameworkRSName = (String) discoveredMap.get(properties[0]);
            wizardModel.setWizardValue(Util.CRS_FRAMEWORK_RSNAME,
                crsFrameworkRSName);
        }

        properties = new String[] { Util.RAC_PROXY_RSNAME };
        discoveredMap = racModel.discoverPossibilities(null, nodeEndpoint, properties, helperData);
        racProxyRSName = (String) discoveredMap.get(Util.RAC_PROXY_RSNAME);
        wizardModel.setWizardValue(Util.RAC_PROXY_RSNAME, racProxyRSName);

        // Resource Group Name
        properties = new String[] { Util.RAC_PROXY_RGNAME };
        discoveredMap = racModel.discoverPossibilities(null, nodeEndpoint, properties, helperData);
        racProxyRGName = (String) discoveredMap.get(Util.RAC_PROXY_RGNAME);
        wizardModel.setWizardValue(Util.RAC_PROXY_RGNAME, racProxyRGName);

        HashMap tableMap = new HashMap();
        HashMap selMap = null;
        String option = "";
        int row_count = 1;

        while (!option.equals(dchar)) {
            assignedResNames = new ArrayList();
            assignedRgNames = new ArrayList();
            row_count = 1;
            TTYDisplay.setNavEnabled(true);
            this.cancelDirection = false;

            reviewPanel.removeAllElements();
            wizardModel.selectCurrWizardContext();

            tableMap = new HashMap();

            // First Row - CRS Framework resource name
            if (!crsFrameworkPresent) {
                crsFrameworkRSName = (String)wizardModel.getWizardValue(
                        Util.CRS_FRAMEWORK_RSNAME);
                reviewPanel.add(crsframework_rsname_txt);
                reviewPanel.add(crsFrameworkRSName);
                selMap = new HashMap();
                selMap.put(NAME, crsframework_rsname_txt);
                selMap.put(VALUE, crsFrameworkRSName);
                selMap.put(DESC, crsframework_rsname_desc);
                selMap.put(TYPE, crsframework_rsname_type);
                selMap.put(UTILNAME, Util.CRS_FRAMEWORK_RSNAME);
                selMap.put(VALIDATENAME, Util.RESOURCENAME);
                selMap.put(ERRORSTR, rsname_error);
                selMap.put(CANEDIT, new Boolean(true));
                selMap.put(CANTEDIT_ERROR, "");
                tableMap.put(Integer.toString(row_count++), selMap);

                if (!assignedResNames.contains(crsFrameworkRSName)) {
                    assignedResNames.add(crsFrameworkRSName);
                }

                // Second row - RAC Framework RG Name
                racFrameworkRGName = (String)wizardModel.getWizardValue(
                        Util.RAC_FRAMEWORK_RGNAME);
                reviewPanel.add(racframework_rgname_txt);
                reviewPanel.add(racFrameworkRGName);
                selMap = new HashMap();
                selMap.put(NAME, racframework_rgname_txt);
                selMap.put(VALUE, racFrameworkRGName);
                selMap.put(DESC, racframework_rgname_desc);
                selMap.put(TYPE, racframework_rgname_type);
                selMap.put(UTILNAME, Util.RAC_FRAMEWORK_RGNAME);
                selMap.put(VALIDATENAME, Util.RESOURCEGROUPNAME);
                selMap.put(ERRORSTR, rgname_error);
                selMap.put(CANEDIT, new Boolean(false));
                selMap.put(CANTEDIT_ERROR, cantedit_error);
                tableMap.put(Integer.toString(row_count++), selMap);

                if (!assignedRgNames.contains(racFrameworkRGName)) {
                    assignedRgNames.add(racFrameworkRGName);
                }
            }

            // Third row - Proxy resource name
            racProxyRSName = (String)wizardModel.getWizardValue(
                    Util.RAC_PROXY_RSNAME);
            reviewPanel.add(proxy_rsname_txt);
            reviewPanel.add(racProxyRSName);
            selMap = new HashMap();
            selMap.put(NAME, proxy_rsname_txt);
            selMap.put(VALUE, racProxyRSName);
            selMap.put(DESC, proxy_rsname_desc);
            selMap.put(TYPE, proxy_rsname_type);
            selMap.put(UTILNAME, Util.RAC_PROXY_RSNAME);
            selMap.put(VALIDATENAME, Util.RESOURCENAME);
            selMap.put(ERRORSTR, rsname_error);
            selMap.put(CANEDIT, new Boolean(true));
            selMap.put(CANTEDIT_ERROR, "");
            tableMap.put(Integer.toString(row_count++), selMap);

            if (!assignedResNames.contains(racProxyRSName)) {
                assignedResNames.add(racProxyRSName);
            }

            // Fourth row - Proxy RG Name
            racProxyRGName = (String)wizardModel.getWizardValue(
                    Util.RAC_PROXY_RGNAME);
            reviewPanel.add(proxy_rgname_txt);
            reviewPanel.add(racProxyRGName);
            selMap = new HashMap();
            selMap.put(NAME, proxy_rgname_txt);
            selMap.put(VALUE, racProxyRGName);
            selMap.put(DESC, proxy_rgname_desc);
            selMap.put(TYPE, proxy_rgname_type);
            selMap.put(UTILNAME, Util.RAC_PROXY_RGNAME);
            selMap.put(VALIDATENAME, Util.RESOURCEGROUPNAME);
            selMap.put(ERRORSTR, rgname_error);
            selMap.put(CANEDIT, new Boolean(true));
            selMap.put(CANTEDIT_ERROR, "");
            tableMap.put(Integer.toString(row_count++), selMap);

            if (!assignedRgNames.contains(racProxyRGName)) {
                assignedRgNames.add(racProxyRGName);
            }

            TTYDisplay.pageText(desc);
            TTYDisplay.clear(1);

            String subheadings[] = { heading1, heading2 };

            HashMap customTags = new HashMap();
            customTags.put(dchar, done);
            option = TTYDisplay.create2DTable(table_title, subheadings,
                    reviewPanel, customTags, help_text);

            if (option.equals(back)) {
                this.cancelDirection = true;

                return;
            }

            TTYDisplay.clear(3);
            TTYDisplay.setNavEnabled(false);

            if (!option.equals(dchar)) {
                selMap = (HashMap) tableMap.get(option);

                String name = (String) selMap.get(NAME);
                String value = (String) selMap.get(VALUE);
                String description = (String) selMap.get(DESC);
                String type = (String) selMap.get(TYPE);
                String utilName = (String) selMap.get(UTILNAME);
                String validateName = (String) selMap.get(VALIDATENAME);
                String error_str = (String) selMap.get(ERRORSTR);
                boolean canEdit = ((Boolean) selMap.get(CANEDIT))
                    .booleanValue();
                String cantEditErrorMsg = (String) selMap.get(CANTEDIT_ERROR);

                String propNameText = wizardi18n.getString(
                        "oraclerac10g.reviewPanel.propname",
                        new String[] { name });
                String propTypeText = wizardi18n.getString(
                        "oraclerac10g.reviewPanel.proptype",
                        new String[] { type });
                String propDescText = wizardi18n.getString(
                        "oraclerac10g.reviewPanel.propdesc",
                        new String[] { description });
                String currValText = wizardi18n.getString(
                        "oraclerac10g.reviewPanel.currval",
                        new String[] { value });

                TTYDisplay.pageText(propNameText);
                TTYDisplay.pageText(propDescText);
                TTYDisplay.pageText(propTypeText);
                TTYDisplay.pageText(currValText);

                if (!canEdit) {
                    TTYDisplay.pageText(cantEditErrorMsg);
                    TTYDisplay.clear(1);
                    TTYDisplay.promptForEntry(continue_txt);

                    continue;
                }

                String tmp_name = null;
                String enteragain = yes;
                boolean error = true;

                while (error && enteragain.equals(yes)) {
                    tmp_name = TTYDisplay.promptForEntry(newValText);

                    if ((tmp_name != null) && (tmp_name.trim().length() != 0)) {
                        properties = new String[] { validateName };

                        HashMap userInputs = new HashMap();
                        userInputs.put(validateName, tmp_name);

                        ErrorValue retVal = validateInput(properties,
			    userInputs, helperData);

                        if (!retVal.getReturnValue().booleanValue()) {
                            TTYDisplay.printError(error_str);
                            enteragain = TTYDisplay.getConfirmation(wizardi18n
                                    .getString("cliwizards.enterAgain"), yes,
                                    no);
                        } else {

                            if (validateName.equals(Util.RESOURCENAME)) {
                                assignedResNames.remove(value);

                                if (assignedResNames.contains(tmp_name)) {
                                    assignedResNames.add(value);
                                    TTYDisplay.printError(rsname_assigned);
                                    enteragain = TTYDisplay.getConfirmation(
                                            wizardi18n.getString(
                                                "cliwizards.enterAgain"), yes,
                                            no);
                                } else {
                                    error = false;
                                    assignedResNames.add(tmp_name);
                                    wizardModel.setWizardValue(utilName,
                                        tmp_name);
                                }
                            } else if (validateName.equals(
                                        Util.RESOURCEGROUPNAME)) {
                                assignedRgNames.remove(value);

                                if (assignedRgNames.contains(tmp_name)) {
                                    assignedRgNames.add(value);
                                    TTYDisplay.printError(rgname_assigned);
                                    enteragain = TTYDisplay.getConfirmation(
                                            wizardi18n.getString(
                                                "cliwizards.enterAgain"), yes,
                                            no);
                                } else {
                                    error = false;
                                    assignedRgNames.add(tmp_name);
                                    wizardModel.setWizardValue(utilName,
                                        tmp_name);
                                }
                            } else {
                                error = false;
                                wizardModel.setWizardValue(utilName, tmp_name);
                            }
                        }
                    }
                } // while
            }

            TTYDisplay.clear(3);
        }
    }

    private ErrorValue validateInput(String propNames[], HashMap userInput,
        HashMap helperData) {
        ErrorValue retValue = null;
	RACModel racModel = RACInvokerCreator.getRACModel();
	String nodeEndpoint = (String) wizardModel.getWizardValue(Util.NODE_TO_CONNECT);
        retValue = racModel.validateInput(null, nodeEndpoint, propNames, userInput, helperData);

        return retValue;
    }
}
