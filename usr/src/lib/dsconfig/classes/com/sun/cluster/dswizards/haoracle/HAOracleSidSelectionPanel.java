/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)HAOracleSidSelectionPanel.java 1.12     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.haoracle;

// CLI Wizard Core
import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.haoracle.HAOracleMBean;

// CMASS
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;

// Wizard Common
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;

import java.util.HashMap;

// J2SE
import java.util.List;
import java.util.Vector;

import javax.management.remote.JMXConnector;


/**
 * This panel gets the Preferred Node from the User
 */
public class HAOracleSidSelectionPanel extends WizardLeaf {


    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N wizardi18n; // The resource bundle

    private HAOracleMBean mbeans[] = null;


    /**
     * Creates a new instance of HAOracleSidSelectionPanel
     */
    public HAOracleSidSelectionPanel() {
    }

    /**
     * Creates an PreferredNode Panel with the given name and manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public HAOracleSidSelectionPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates an PreferredNode Panel for the wizard with the given name and
     * associates it with the WizardModel and WizardFlow.
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     * @param  wizardType  WizardType
     */
    public HAOracleSidSelectionPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;
        this.wizardi18n = wizardi18n;
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        String title = wizardi18n.getString(
                "haoracle.oracleSidSelectionPanel.title");
        String desc = wizardi18n.getString(
                "haoracle.oracleSidSelectionPanel.desc");
        String yes = wizardi18n.getString("cliwizards.yes");
        String no = wizardi18n.getString("cliwizards.no");
        String back = wizardi18n.getString("ttydisplay.back.char");
        String orasid_prompt = wizardi18n.getString(
                "haoracle.oracleSidSelectionPanel.prompt");
        String help_text = wizardi18n.getString(
                "haoracle.oracleSidSelectionPanel.cli.help");
        String continue_txt = wizardi18n.getString("cliwizards.continue");
        String enterKey = wizardi18n.getString("custom.menu.enteryourown.key");
        String enterText = wizardi18n.getString(
                "custom.menu.enteryourown.keyText");
        String confirm_txt = wizardi18n.getString("cliwizards.enterAgain");
        String oraSidErrInfoMsg = wizardi18n.getString(
                "haoracle.oracleSidSelectionPanel.cli.infomsg");

        boolean isOracleSidValid = false;

        while (!isOracleSidValid) {
            String selected_orasid = null;
            String defaultOracleSid = null;

            TTYDisplay.printTitle(title);

            // Get the discovered ORACLE_HOME and ORACLE_SID from the
            // dataservice
            Vector vOraSid = (Vector) wizardModel.getWizardValue(
                    Util.ORACLE_SID);

            HashMap custom = new HashMap();
            custom.put(enterKey, enterText);

            String optionsArr[] = (String[]) vOraSid.toArray(new String[0]);

            TTYDisplay.setNavEnabled(true);
            this.cancelDirection = false;

            // get the default selection
            defaultOracleSid = (String) wizardModel.getWizardValue(
                    HAOracleWizardConstants.SEL_ORACLE_SID);

            TTYDisplay.printTitle(title);

            if ((optionsArr != null) && (optionsArr.length > 0)) {
                List oracleSidList = TTYDisplay.getScrollingOption(desc,
                        optionsArr, custom, defaultOracleSid, help_text, true);

                // back key pressed
                if (oracleSidList == null) {
                    this.cancelDirection = true;

                    return;
                }

                TTYDisplay.setNavEnabled(true);
                this.cancelDirection = false;

                TTYDisplay.clear(1);

                String oracleArr[] = (String[]) oracleSidList.toArray(
                        new String[0]);
                selected_orasid = oracleArr[0];
            } else {
                selected_orasid = enterText;
            }

            if (selected_orasid.equals(enterText)) {
                selected_orasid = "";

                while (selected_orasid.trim().length() == 0) {
                    selected_orasid = TTYDisplay.promptForEntry(orasid_prompt);

                    if (selected_orasid.equals(back)) {
                        this.cancelDirection = true;

                        return;
                    }
                }
            }

            // validate the Oracle Sid
            ErrorValue retVal = validateInput(selected_orasid);

            if (!retVal.getReturnValue().booleanValue()) {
                TTYDisplay.printError(oraSidErrInfoMsg);
                TTYDisplay.clear(2);

                String confirm = TTYDisplay.getConfirmation(confirm_txt, yes,
                        no);

                if (confirm.equalsIgnoreCase(yes)) {
                    isOracleSidValid = false;
                } else {
                    isOracleSidValid = true;
                }
            } else {
                isOracleSidValid = true;
            }

            // Store the Values in the WizardModel
            wizardModel.selectCurrWizardContext();
            wizardModel.setWizardValue(HAOracleWizardConstants.SEL_ORACLE_SID,
                selected_orasid);
        }
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#isDisplayComplete
     */
    public boolean isDisplayComplete() {
        return true;
    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }

    private ErrorValue validateInput(String oraSid) {
        String propName[] = { Util.ORACLE_SID };
        HashMap userInput = new HashMap();
        userInput.put(Util.ORACLE_SID, oraSid);

        String nodeList[] = (String[]) wizardModel.getValue(Util.RG_NODELIST);
        ErrorValue err = new ErrorValue();
        err.setReturnVal(Boolean.TRUE);

        StringBuffer errStr = new StringBuffer();
        int err_count = 0;
        mbeans = new HAOracleMBean[nodeList.length];

        JMXConnector localConnection = getConnection(Util.getClusterEndpoint());

        for (int i = 0; i < nodeList.length; i++) {
            String split_str[] = nodeList[i].split(Util.COLON);
            String nodeEndPoint = Util.getNodeEndpoint(localConnection,
                    split_str[0]);
            JMXConnector connector = getConnection(nodeEndPoint);
            mbeans[i] = HAOracleWizardCreator.getHAOracleMBeanOnNode(connector);

            ErrorValue errVal = mbeans[i].validateInput(propName, userInput,
                    null);

            if (!errVal.getReturnValue().booleanValue()) {

                // error received.
                err_count++;
                errStr = new StringBuffer(errVal.getErrorString());
            }
        }

        if (err_count == nodeList.length) {

            // failed on all nodes.
            err.setReturnVal(Boolean.FALSE);
            err.setErrorString(errStr.toString());
        }

        return err;
    }
}
