/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)HAOracleFlowManager.java 1.8     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.haoracle;

import com.sun.cluster.dswizards.common.DSConfigWizardModel;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.DSWizardInterface;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.hastorageplus.HASPWizardConstants;

import java.util.ArrayList;
import java.util.List;


/**
 * The class takes decision on which panel to show "next" depending upon the
 * conditions set at a particular context.
 */
public class HAOracleFlowManager implements DSWizardInterface {

    /**
     * Creates a new instance of HAOracleFlowManager
     */
    public HAOracleFlowManager() {
    }

    /**
     * @see  com.sun.cluster.dswizards.common.DSWizardInterface#getNextPanel
     */
    public String getNextPanel(String curPanelName,
        Object wizCxtObj, String options[]) {

	DSConfigWizardModel guiModel = null;
	CliDSConfigWizardModel cliModel = null;

	if (wizCxtObj instanceof CliDSConfigWizardModel) {
	    cliModel = (CliDSConfigWizardModel)wizCxtObj;
	} else if (wizCxtObj instanceof DSConfigWizardModel) {
	    guiModel = (DSConfigWizardModel)wizCxtObj;
	}

        // if on Oracle Home Panel
        if (curPanelName.equals(HAOracleWizardConstants.PANEL_3)) {

            // get the user preference
            String preference = null;
	    if (guiModel != null) {
		preference = (String)guiModel.getWizardValue(
		    HAOracleWizardConstants.HAORACLE_PREF);
	    } else {
		preference = (String)cliModel.getWizardValue(
		    HAOracleWizardConstants.HAORACLE_PREF);
	    }

            if (preference.equals(HAOracleWizardConstants.SERVER_ONLY) ||
                    preference.equals(
                        HAOracleWizardConstants.SERVER_AND_LISTENER)) {

                // go to Oracle Sid selection panel
                return options[0];
            } else if (preference.equals(
                        HAOracleWizardConstants.LISTENER_ONLY)) {
                return options[1];
            }
        } else if (curPanelName.equals(HAOracleWizardConstants.PANEL_6)) {
            String create_hasp = null;
	    String zonesPresent = null;
	    if (guiModel != null) {
		create_hasp = (String)guiModel.getWizardValue(
		    HAOracleWizardConstants.CREATE_HASP_RS);
            	zonesPresent = (String)guiModel.getWizardValue(
		    HASPWizardConstants.ZONES_PRESENT);
	    } else {
		create_hasp = (String)cliModel.getWizardValue(
		    HAOracleWizardConstants.CREATE_HASP_RS);
            	zonesPresent = (String)cliModel.getWizardValue(
		    HASPWizardConstants.ZONES_PRESENT);
	    }

            if (create_hasp.equals(HAOracleWizardConstants.CREATE_NEW)) {

                if (zonesPresent.equals(HASPWizardConstants.NO)) {
                    return options[0];
                } else {

                    // skip hasp location panel
                    return options[1];
                }
            } else {
                return options[2];
            }
        } else if (curPanelName.equals(HAOracleWizardConstants.PANEL_7)) {
            String create_lh = null;
	    if (guiModel != null) {
		create_lh = (String)guiModel.getWizardValue(
		    HAOracleWizardConstants.CREATE_LH_RS);
	    } else {
		create_lh = (String)cliModel.getWizardValue(
		    HAOracleWizardConstants.CREATE_LH_RS);
	    }

            if (create_lh.equals(HAOracleWizardConstants.CREATE_NEW)) {
                return options[0];
            } else {
                return options[1];
            }
        }

        return null;
    }
}
