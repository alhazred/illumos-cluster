/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)RacDBHomeSelectionPanel.java	1.17	09/03/11 SMI"
 */

package com.sun.cluster.dswizards.oraclerac.database.ver9i;

// CMASS
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.dataservices.common.ErrorValue;

// CLI Wizard Core
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.common.CliUtil;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.oraclerac.database.*;
import com.sun.cluster.dswizards.oraclerac.invoker.RACInvokerCreator;

// Model
import com.sun.cluster.model.RACModel;

// J2SE
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

/**
 * This panel gets the Oracle Home from the User
 */
public class RacDBHomeSelectionPanel extends RacDBBasePanel {
    /**
     * Creates an OracleHome Selection Panel for the wizard with the given
     * name and associates it with the WizardModel and WizardFlow.
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     * @param  wizardType  WizardType
     */
    public RacDBHomeSelectionPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(name, model, wizardFlow, wizardi18n);
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        title = wizardi18n.getString("haoracle.oracleHomeSelectionPanel.title");
        desc = wizardi18n.getString("haoracle.oracleHomeSelectionPanel.desc");

        String orahome_prompt = wizardi18n.getString(
                "haoracle.oracleHomeSelectionPanel.prompt");
        String help_text = wizardi18n.getString(
                "haoracle.oracleHomeSelectionPanel.help");

        String selected_orahome = null;
        String defSelection = null;

        TTYDisplay.printTitle(title);

        // Get the discovered ORACLE_HOME and ORACLE_SID from the dataservice
        String nodeList[] = (String[]) wizardModel.getWizardValue(
	    Util.RG_NODELIST);

        String selZoneCluster = CliUtil.getZoneClusterName(wizardModel);

	String baseClusterNodelist[] = (String[]) wizardModel.getWizardValue(
	    Util.BASE_CLUSTER_NODELIST);

        String propNames[] = new String[] { Util.ORACLE_HOME, Util.ORACLE_SID };
        Vector vOraHome = new Vector();
        Vector vOraSid = new Vector();

	HashMap helperData = new HashMap();
	helperData.put(Util.SEL_ZONE_CLUSTER, selZoneCluster);

	RACModel racModel = RACInvokerCreator.getRACModel();
        for (int i = 0; i < baseClusterNodelist.length; i++) {
            String tmpArry[] = baseClusterNodelist[i].split(Util.COLON);
	    // Current node
            String cNode = tmpArry[0];

	    HashMap map = racModel.discoverPossibilities(null, cNode, propNames, helperData);

            // oracle home on nodeList[i]
            Vector tmpVec = (Vector) map.get(Util.ORACLE_HOME);

            if (tmpVec == null) {
                tmpVec = new Vector();
            }

            int size = tmpVec.size();

            for (int j = 0; j < size; j++) {
                String orahome = (String) tmpVec.elementAt(j);

                if (!vOraHome.contains(orahome)) {
                    vOraHome.add(orahome);
                }
            }

            // oracle sid on nodeList[i]
            tmpVec = (Vector) map.get(Util.ORACLE_SID);

            if (tmpVec == null) {
                tmpVec = new Vector();
            }

            size = tmpVec.size();

            for (int j = 0; j < size; j++) {
                String orasid = (String) tmpVec.elementAt(j);

                if (!vOraSid.contains(orasid)) {
                    vOraSid.add(orasid);
                }
            }
        } // finish discovery on all nodes

        // save the value of Oracle SID to use in the future panels
        wizardModel.setWizardValue(Util.ORACLE_SID, vOraSid);

        String optionsArr[] = (String[]) vOraHome.toArray(new String[0]);

        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        // get the default selection
        selected_orahome = (String) wizardModel.getWizardValue(
                Util.SEL_ORACLE_HOME);

        if (selected_orahome != null) {
            defSelection = selected_orahome;
        }

        String choice = no;
        HashMap custom = new HashMap();
        custom.put(enterKey, enterText);

        while (choice.equals(no)) {
            TTYDisplay.clear(1);

            // Get the oracle home preference
            // see if an oracle home is already set
            if ((optionsArr != null) && (optionsArr.length > 0)) {
                List selList = TTYDisplay.getScrollingOption(desc, optionsArr,
                        custom, defSelection, help_text, true);

                if (selList == null) {
                    this.cancelDirection = true;

                    return;
                }

                TTYDisplay.setNavEnabled(true);
                this.cancelDirection = false;

                String selArr[] = (String[]) selList.toArray(new String[0]);
                selected_orahome = selArr[0];
            } else {
                selected_orahome = enterText;
            }

            if (selected_orahome.equals(enterText)) {
                TTYDisplay.clear(1);
                selected_orahome = "";

                while (selected_orahome.trim().length() == 0) {
                    selected_orahome = TTYDisplay.promptForEntry(
                            orahome_prompt);

                    if (selected_orahome.equals(back)) {
                        this.cancelDirection = true;

                        return;
                    }
                }
            }

            // validate against the oracle version
            String oracle_version = getDBVersion(selected_orahome);
            String stVersion = "";

            if (oracle_version.equals(Util.NINE)) {
                stVersion = wizardi18n.getString(
                        "oraclerac.versionPanel.option1");
            } else if (oracle_version.equals(Util.TEN)) {
                stVersion = wizardi18n.getString(
                        "oraclerac.versionPanel.option2");
            }

            String error_txt = wizardi18n.getString(
                    "oraclerac.oracleHomeSelectionPanel.error",
                    new String[] { stVersion });
            String selectedOraVersion = (String) wizardModel.getWizardValue(
                    Util.RAC_VER);

            if ((oracle_version != null) &&
                    (oracle_version.trim().length() != 0) &&
                    !oracle_version.equals(selectedOraVersion)) {
                TTYDisplay.printError(error_txt);
                TTYDisplay.clear(2);
                TTYDisplay.promptForEntry(continue_txt);
                TTYDisplay.clear(2);
                choice = no;

                continue;
            }

            // validate the Oracle home
            ErrorValue retVal = validateInput(
		selected_orahome, selZoneCluster, baseClusterNodelist);

            if (!retVal.getReturnValue().booleanValue()) {
                TTYDisplay.printInfo(retVal.getErrorString());

                String enterAgain = TTYDisplay.getConfirmation(enteragain_txt,
                        yes, no);

                if (enterAgain.equalsIgnoreCase(yes)) {
                    choice = no;

                    continue;
                }
            }

            choice = yes;

            // Store the Values in the WizardModel
            wizardModel.selectCurrWizardContext();
            wizardModel.setWizardValue(Util.SEL_ORACLE_HOME, selected_orahome);
        }
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#isDisplayComplete
     */
    public boolean isDisplayComplete() {
        return true;
    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }

    private ErrorValue validateInput(String oraHome, String selZoneCluster,
	String baseClusterNodelist[]) {

        // validate oracle home on all nodes of a cluster
        String propName[] = { Util.ORACLE_HOME };
        HashMap userInput = new HashMap();
        userInput.put(Util.ORACLE_HOME, oraHome);

        ErrorValue err = new ErrorValue();
        err.setReturnVal(Boolean.TRUE);

        StringBuffer errStr = new StringBuffer();
        int err_count = 0;
	HashMap helperData = new HashMap();
	helperData.put(Util.SEL_ZONE_CLUSTER, selZoneCluster);

	RACModel racModel = RACInvokerCreator.getRACModel();
        for (int i = 0; i < baseClusterNodelist.length; i++) {
	    String tmpArry[] = baseClusterNodelist[i].split(Util.COLON);
	    // Current node
            String cNode = tmpArry[0];

            ErrorValue errVal = racModel.validateInput(null, cNode, propName, userInput,
		helperData);

            if (!errVal.getReturnValue().booleanValue()) {
                // error
                err_count++;
                errStr = new StringBuffer(errVal.getErrorString());
            }
        }

        if (err_count == baseClusterNodelist.length) {

            // failed on all nodes
            err.setReturnVal(Boolean.FALSE);
            err.setErrorString(errStr.toString());
        }

        return err;
    }
}
