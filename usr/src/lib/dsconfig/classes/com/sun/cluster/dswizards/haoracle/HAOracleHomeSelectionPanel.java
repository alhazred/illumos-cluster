/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)HAOracleHomeSelectionPanel.java 1.12     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.haoracle;

// CLI Wizard Core
import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.haoracle.HAOracleMBean;

// CMASS
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;

// Wizard Common
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;

import java.util.HashMap;

// J2SE
import java.util.List;
import java.util.Vector;

import javax.management.remote.JMXConnector;


/**
 * This panel gets the Preferred Node from the User
 */
public class HAOracleHomeSelectionPanel extends WizardLeaf {
    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N wizardi18n; // The resource bundle

    private HAOracleMBean mbeans[] = null;

    private JMXConnector localConnection = null;

    /**
     * Creates a new instance of HAOracleHomeSelectionPanel
     */
    public HAOracleHomeSelectionPanel() {
    }

    /**
     * Creates an PreferredNode Panel with the given name and manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public HAOracleHomeSelectionPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates an PreferredNode Panel for the wizard with the given name and
     * associates it with the WizardModel and WizardFlow.
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     * @param  wizardType  WizardType
     */
    public HAOracleHomeSelectionPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;
        this.wizardi18n = wizardi18n;
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        String title = wizardi18n.getString(
                "haoracle.oracleHomeSelectionPanel.title");
        String desc = wizardi18n.getString(
                "haoracle.oracleHomeSelectionPanel.desc");
        String yes = wizardi18n.getString("cliwizards.yes");
        String no = wizardi18n.getString("cliwizards.no");
        String back = wizardi18n.getString("ttydisplay.back.char");
        String orahome_prompt = wizardi18n.getString(
                "haoracle.oracleHomeSelectionPanel.prompt");
        String help_text = wizardi18n.getString(
                "haoracle.oracleHomeSelectionPanel.cli.help");
        String continue_txt = wizardi18n.getString("cliwizards.continue");
        String enterKey = wizardi18n.getString("custom.menu.enteryourown.key");
        String enterText = wizardi18n.getString(
                "custom.menu.enteryourown.keyText");
        String confirm_txt = wizardi18n.getString("cliwizards.enterAgain");
        String oraHomeErrInfoMsg = wizardi18n.getString(
                "haoracle.oracleHomeSelectionPanel.cli.infomsg");

        boolean isOracleHomeValid = false;

        while (!isOracleHomeValid) {
            String selected_orahome = null;
            String defaultOracleHome = null;

            TTYDisplay.printTitle(title);

            // Get the discovered ORACLE_HOME and ORACLE_SID from the
            // dataservice
            String nodeList[] = (String[]) wizardModel.getWizardValue(
                    Util.RG_NODELIST);
            String propNames[] = new String[] {
                    Util.ORACLE_HOME, Util.ORACLE_SID
                };
            Vector vOraHome = new Vector();
            Vector vOraSid = new Vector();
            mbeans = new HAOracleMBean[nodeList.length];

            localConnection = getConnection(Util.getClusterEndpoint());

            for (int i = 0; i < nodeList.length; i++) {
                String split_str[] = nodeList[i].split(Util.COLON);
                String nodeEndPoint = Util.getNodeEndpoint(localConnection,
                        split_str[0]);
                JMXConnector connector = getConnection(nodeEndPoint);
                mbeans[i] = HAOracleWizardCreator.getHAOracleMBeanOnNode(
                        connector);

                HashMap map = mbeans[i].discoverPossibilities(propNames, null);

                // oracle home on nodeList[i]
                Vector tmpVec = (Vector) map.get(Util.ORACLE_HOME);

                if (tmpVec == null) {
                    tmpVec = new Vector();
                }

                int size = tmpVec.size();

                for (int j = 0; j < size; j++) {
                    String orahome = (String) tmpVec.elementAt(j);

                    if (!vOraHome.contains(orahome)) {
                        vOraHome.add(orahome);
                    }
                }

                // oracle sid on nodeList[i]
                tmpVec = (Vector) map.get(Util.ORACLE_SID);

                if (tmpVec == null) {
                    tmpVec = new Vector();
                }

                size = tmpVec.size();

                for (int j = 0; j < size; j++) {
                    String orasid = (String) tmpVec.elementAt(j);

                    if (!vOraSid.contains(orasid)) {
                        vOraSid.add(orasid);
                    }
                }
            } // finish discovery on all nodes

            // save the value of Oracle SID to use in the future panels
            wizardModel.setWizardValue(Util.ORACLE_SID, vOraSid);

            HashMap custom = new HashMap();
            custom.put(enterKey, enterText);

            String optionsArr[] = (String[]) vOraHome.toArray(new String[0]);

            TTYDisplay.setNavEnabled(true);
            this.cancelDirection = false;

            TTYDisplay.printTitle(title);

            defaultOracleHome = (String) wizardModel.getWizardValue(
                    HAOracleWizardConstants.SEL_ORACLE_HOME);

            // Get the oracle home preference
            // see if an oracle home is already set

            if ((optionsArr != null) && (optionsArr.length > 0)) {
                List oracleHomeList = TTYDisplay.getScrollingOption(desc,
                        optionsArr, custom, defaultOracleHome, help_text, true);

                // back key pressed
                if (oracleHomeList == null) {
                    this.cancelDirection = true;

                    return;
                }

                TTYDisplay.setNavEnabled(true);
                this.cancelDirection = false;

                TTYDisplay.clear(1);

                String oracleArr[] = (String[]) oracleHomeList.toArray(
                        new String[0]);
                selected_orahome = oracleArr[0];
            } else {
                selected_orahome = enterText;
            }

            if (selected_orahome.equals(enterText)) {
                selected_orahome = "";

                while (selected_orahome.trim().length() == 0) {
                    selected_orahome = TTYDisplay.promptForEntry(
                            orahome_prompt);

                    if (selected_orahome.equals(back)) {
                        this.cancelDirection = true;

                        return;
                    }
                }
            }

            // validate the Oracle home
            ErrorValue retVal = validateInput(selected_orahome, nodeList);

            if (!retVal.getReturnValue().booleanValue()) {
                TTYDisplay.printError(oraHomeErrInfoMsg);
                TTYDisplay.clear(2);

                String confirm = TTYDisplay.getConfirmation(confirm_txt, yes,
                        no);

                if (confirm.equalsIgnoreCase(yes)) {
                    isOracleHomeValid = false;
                } else {
                    isOracleHomeValid = true;
                }
            } else {
                isOracleHomeValid = true;
            }

            // Store the Values in the WizardModel
            wizardModel.selectCurrWizardContext();
            wizardModel.setWizardValue(HAOracleWizardConstants.SEL_ORACLE_HOME,
                selected_orahome);
        }
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#isDisplayComplete
     */
    public boolean isDisplayComplete() {
        return true;
    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }

    private ErrorValue validateInput(String oraHome, String nodeList[]) {

        // validate oracle home on all nodes of a cluster
        String propName[] = { Util.ORACLE_HOME };
        ErrorValue err = new ErrorValue();
        err.setReturnVal(Boolean.TRUE);

        StringBuffer errStr = new StringBuffer();
        int err_count = 0;

        for (int i = 0; i < nodeList.length; i++) {
            StringBuffer newOraHome = new StringBuffer(oraHome);

            // see if the name is a zone
            String split_str[] = nodeList[i].split(Util.COLON);

            if (split_str.length == 2) {

                // get the zone root path
                HashMap discoveredMap = mbeans[i].discoverPossibilities(
                        new String[] { Util.ZONE_ROOTPATH }, nodeList[i]);

                String zoneRootPath = (String) discoveredMap.get(
                        Util.ZONE_ROOTPATH);

                if (zoneRootPath != null) {
                    newOraHome = new StringBuffer(zoneRootPath);
                    newOraHome.append(oraHome);
                }
            }

            HashMap userInput = new HashMap();
            userInput.put(Util.ORACLE_HOME, newOraHome.toString());

            ErrorValue errVal = mbeans[i].validateInput(propName, userInput,
                    null);

            if (!errVal.getReturnValue().booleanValue()) {

                // error
                err_count++;
                errStr = new StringBuffer(errVal.getErrorString());
            }
        }

        if (err_count == nodeList.length) {

            // failed on all nodes
            err.setReturnVal(Boolean.FALSE);
            err.setErrorString(errStr.toString());
        }

        return err;
    }
}
