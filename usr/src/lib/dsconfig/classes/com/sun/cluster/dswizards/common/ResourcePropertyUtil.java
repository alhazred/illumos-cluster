/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ResourcePropertyUtil.java 1.10     08/05/20 SMI"
 */

package com.sun.cluster.dswizards.common;

import com.sun.cluster.agent.rgm.property.ResourceProperty;
import com.sun.cluster.agent.rgm.property.ResourcePropertyBoolean;
import com.sun.cluster.agent.rgm.property.ResourcePropertyEnum;
import com.sun.cluster.agent.rgm.property.ResourcePropertyInteger;
import com.sun.cluster.agent.rgm.property.ResourcePropertyString;
import com.sun.cluster.agent.rgm.property.ResourcePropertyStringArray;

import java.util.Iterator;


public class ResourcePropertyUtil {

    public static String getType(ResourceProperty property) {

        if (property instanceof ResourcePropertyString) {
            return "String";
        } else if (property instanceof ResourcePropertyInteger) {
            return "Integer";
        } else if (property instanceof ResourcePropertyBoolean) {
            return "Boolean";
        } else if (property instanceof ResourcePropertyEnum) {
            return "Enum";
        } else if (property instanceof ResourcePropertyStringArray) {
            return "String Array";
        }

        return null;
    }

    //This function returns english strings. A i18n bug must be filed 
    //on this function.
    public static String getPropertyErrorMessage(ResourceProperty property) {
        String msg = "";

        if (property instanceof ResourcePropertyString) {
            ResourcePropertyString rps = (ResourcePropertyString) property;
            msg = " type = String\n";

            if (rps.getMinLength() != null) {
                msg += "min length = " + rps.getMinLength() + "\n";
            }

            if (rps.getMaxLength() != null) {
                msg += "max length = " + rps.getMaxLength();
            }
        } else if (property instanceof ResourcePropertyInteger) {
            ResourcePropertyInteger rpi = (ResourcePropertyInteger) property;
            msg = " type = Integer\n";

            if (rpi.getMinValue() != null) {
                msg += "min value = " + rpi.getMinValue() + "\n";
            }

            if (rpi.getMaxValue() != null) {
                msg += "max value = " + rpi.getMaxValue();
            }
        } else if (property instanceof ResourcePropertyBoolean) {
            msg = " type = Boolean";

        } else if (property instanceof ResourcePropertyEnum) {
            ResourcePropertyEnum rpe = (ResourcePropertyEnum) property;
            msg = " type   = Enum\n";
            msg += "values =";

            Iterator i = rpe.getEnumList().iterator();

            while (i.hasNext()) {
                msg += " " + (String) i.next();
            }
        } else if (property instanceof ResourcePropertyStringArray) {
            ResourcePropertyStringArray rpsa = (ResourcePropertyStringArray)
                property;
            msg = " type = String Array\n";

            if (rpsa.getMinLength() != null) {
                msg += "element min length = " + rpsa.getMinLength() + "\n";
            }

            if (rpsa.getMaxLength() != null) {
                msg += "element max length = " + rpsa.getMaxLength() + "\n";
            }

            if (rpsa.getArrayMinSize() != null) {
                msg += "array min size      = " + rpsa.getArrayMinSize() + "\n";
            }

            if (rpsa.getArrayMaxSize() != null) {
                msg += "array max size      = " + rpsa.getArrayMaxSize();
            }
        }

        return msg;

    }

    /*
     * Construct a string containing all the elements in
     * <code>stringArray</code> seperated by </code>delimiter<code>.
     *
     * @param stringArray the array of string to join
     * @param delimiter the string to use as a separator in the constructed
     *                   string
     *
     * @return the formatted string
     */
    public static String join(String stringArray[], String delimiter) {
        String joinedString = "";

        if (stringArray == null)
            return joinedString;

        for (int i = 0; i < stringArray.length; i++) {

            if (i == 0) {
                joinedString = stringArray[i];
            } else {
                joinedString = joinedString + delimiter + stringArray[i];
            }
        }

        return joinedString;
    }

    public static String[] concatenateArrays(String array1[], String array2[]) {
        String concat[] = new String[array1.length + array2.length];
        int i = 0;
        int j = 0;

        for (i = 0; i < array1.length; i++) {
            concat[i] = array1[i];
        }

        j = i;

        for (i = 0; i < array2.length; i++) {
            concat[j++] = array2[i];
        }

        return concat;
    }

    public static String getValue(ResourceProperty prop) {
        String value = null;

        if (prop instanceof ResourcePropertyString) {
            ResourcePropertyString rps = (ResourcePropertyString) prop;
            value = rps.getValue();

            if ((value == null) || (value.length() == 0)) {
                value = rps.getDefaultValue();
            }
        } else if (prop instanceof ResourcePropertyInteger) {
            ResourcePropertyInteger rpi = (ResourcePropertyInteger) prop;
            Integer ivalue = rpi.getValue();

            if (ivalue == null) {
                ivalue = rpi.getDefaultValue();
            }

            if (ivalue != null) {
                value = ivalue.toString();
            }
        } else if (prop instanceof ResourcePropertyBoolean) {
            ResourcePropertyBoolean rpb = (ResourcePropertyBoolean) prop;
            Boolean bvalue = rpb.getValue();

            if (bvalue == null) {
                bvalue = rpb.getDefaultValue();
            }

            if (bvalue != null) {
                value = bvalue.toString();
            }
        } else if (prop instanceof ResourcePropertyEnum) {
            ResourcePropertyEnum rpe = (ResourcePropertyEnum) prop;
            value = rpe.getValue();

            if ((value == null) || (value.length() == 0)) {
                value = rpe.getDefaultValue();
            }
        } else if (prop instanceof ResourcePropertyStringArray) {
            ResourcePropertyStringArray rpsa = (ResourcePropertyStringArray)
                prop;
            String arvalue[] = rpsa.getValue();

            if (arvalue == null) {
                arvalue = rpsa.getDefaultValue();
            }

            if (arvalue != null) {
                value = ResourcePropertyUtil.join(arvalue, ",");
            }
        }

        if ((value == null) || (value.length() == 0)) {
            value = "";
        }

        return value;
    }

    public static String getAbsValue(ResourceProperty prop) {
        String value = null;

        if (prop instanceof ResourcePropertyString) {
            ResourcePropertyString rps = (ResourcePropertyString) prop;
            value = rps.getValue();
        } else if (prop instanceof ResourcePropertyInteger) {
            ResourcePropertyInteger rpi = (ResourcePropertyInteger) prop;
            Integer ivalue = rpi.getValue();

            if (ivalue != null) {
                value = ivalue.toString();
            }
        } else if (prop instanceof ResourcePropertyBoolean) {
            ResourcePropertyBoolean rpb = (ResourcePropertyBoolean) prop;
            Boolean bvalue = rpb.getValue();

            if (bvalue != null) {
                value = bvalue.toString();
            }
        } else if (prop instanceof ResourcePropertyEnum) {
            ResourcePropertyEnum rpe = (ResourcePropertyEnum) prop;
            value = rpe.getValue();
        } else if (prop instanceof ResourcePropertyStringArray) {
            ResourcePropertyStringArray rpsa = (ResourcePropertyStringArray)
                prop;
            String arvalue[] = rpsa.getValue();

            if (arvalue != null) {
                value = ResourcePropertyUtil.join(arvalue, ",");
            }
        }

        return value;
    }
}
