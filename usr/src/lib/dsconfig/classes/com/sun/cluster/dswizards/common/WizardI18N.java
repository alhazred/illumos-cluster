/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)WizardI18N.java 1.13     08/05/20 SMI"
 */

package com.sun.cluster.dswizards.common;

import java.text.*;

import java.util.*;


/**
 * WizardI18N is a message catalog that facilitates localization of string
 * resources.  It can look up locale-specific resource bundles and extract
 * messages from them.
 */
public class WizardI18N {

    public String resourceBundleName;

    private ResourceBundle bundle = null; // Message Bundle

    public WizardI18N(String resourceBundleName) {
        this.resourceBundleName = resourceBundleName;

        // Initialize the messageBundle for the Current Locale
        Locale currentLocale = Locale.getDefault();

        try {
            this.bundle = ResourceBundle.getBundle(resourceBundleName,
                    currentLocale);
        } catch (MissingResourceException e) {
            System.out.println("WizardI18N, ResourceBundle [" +
                resourceBundleName + "]: not found");
        }

    }

    /**
     * Return a formated localized string based on a specified message pattern
     * key.
     *
     * @param  msgKey  An identifier that specifies which string within the
     * resource is being requested.
     * @param  msgArgs  Arguments that are to be inserted into the localized
     * string.
     *
     * @return  The localized string.
     */
    public String getString(String msgKey, Object msgArgs[]) {
        String rStr = null;
        String pattern = getString(msgKey);
        MessageFormat msgFmt = new MessageFormat(pattern);

        if (msgFmt == null) {
            System.out.println("WizardI18N: getString (" + msgKey +
                ") is null");
        } else {

            try {
                rStr = msgFmt.format(msgArgs);
            } catch (NullPointerException e) {
                System.out.println("WizardI18N: getString (" + msgKey +
                    ") unmatched message Args");
                System.out.println("pattern = " + pattern);

                rStr = pattern;
            }
        }

        if (rStr == null) {
            return (msgKey);
        } else {
            return (rStr);
        }
    }

    /**
     * Return a localized string for the supplied message key.
     *
     * @param  msgKey  An identifier that specifies which string within the
     * resource is being requested.
     *
     * @return  The localized string.
     */
    public String getString(String msgKey) {
        String msg = null;

        if (this.bundle != null) {

            try {
                msg = bundle.getString(msgKey);
            } catch (MissingResourceException e) {

                // If no Key is found, print the key as it is
                msg = msgKey;
            } catch (NullPointerException e) {
                msg = msgKey;
            }
        }

        if (msg == null) {
            msg = msgKey;
        }

        return msg;
    }


    /**
     * Return a localized string for the supplied help message key.
     *
     * @param  helpPrefix  An identifier that specifies the help prefix key.
     *
     * @return  The localized string.
     */
    public String getHelpString(String helpPrefix) {
        String helpStr = "";
        int count = 0;

        if (this.bundle != null) {

            try {
                count = Integer.parseInt(this.bundle.getString(helpPrefix));

                for (int i = 1; i < count; i++) {
                    String key = helpPrefix + "." + i;
                    helpStr = helpStr + this.bundle.getString(key);

                    if (i < (count - 1))
                        helpStr = helpStr + "\n\n";
                }
            } catch (Exception e) {

                // If no Key is found, print the key as it is
                helpStr = helpPrefix;
            }
        }

        if (helpStr.equals("")) {
            helpStr = helpPrefix;
        }

        return helpStr;
    }
}
