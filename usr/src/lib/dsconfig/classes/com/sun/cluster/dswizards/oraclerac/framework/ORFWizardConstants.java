/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)ORFWizardConstants.java	1.10	08/07/14 SMI"
 */

package com.sun.cluster.dswizards.oraclerac.framework;

/**
 * This class holds all the constants for the ORFWizard
 */
public class ORFWizardConstants {

    public static final String WIZARDI18N = "WIZARDI18N";

    // Flow xml
    public static final String ORF_WIZARD_FLOW_XML =
        "/usr/cluster/lib/ds/oraclerac/framework/ORFWizardFlow.xml";

    // State File
    public static final String ORF_WIZARD_STATE_FILE =
        "/opt/cluster/lib/ds/history/ORFWizardState";
    public static final String ORF_RG_NAME = "rac-framework";
    public static final String ORF_RS_NAME = "rac-framework";
    public static final String ORF_UDLM_RS_NAME = "rac-udlm";
    public static final String ORF_SVM_RS_NAME = "rac-svm";
    public static final String ORF_VXVM_RS_NAME = "rac-cvm";

    // Panel names
    public static final String PANEL_0 = "orfIntroPanel";
    public static final String PANEL_1 = "orfNodeSelectionPanel";
    public static final String PANEL_2 = "orfStorageSelectionPanel";
    public static final String PANEL_2_0 = "orfClusterWareSelectionPanel";
    public static final String PANEL_3 = "orfReviewPanel";
    public static final String PANEL_4 = "orfSummaryPanel";
    public static final String PANEL_5 = "orfResultsPanel";

}
