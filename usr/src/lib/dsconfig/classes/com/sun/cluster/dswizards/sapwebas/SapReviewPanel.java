/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)SapReviewPanel.java 1.18     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.sapwebas;

// J2SE
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

// JMX
import javax.management.remote.JMXConnector;

// CMAS
import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.hastorageplus.HAStoragePlusMBean;
import com.sun.cluster.agent.dataservices.logicalhost.LogicalHostMBean;
import com.sun.cluster.agent.dataservices.sapwebas.SapWebasMBean;
import com.sun.cluster.agent.dataservices.utils.DataServicesUtil;
import com.sun.cluster.agent.dataservices.utils.Util;

// CLI Wizard SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;

// Wizard Common
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.hastorageplus.HASPWizardCreator;
import com.sun.cluster.dswizards.logicalhost.LogicalHostWizardCreator;

public class SapReviewPanel extends WizardLeaf {

    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N wizardi18n; // Message Bundle
    private String NAME = "name";
    private String VALUE = "value";
    private String KEY = "key";
    private String DESC = "desc";
    private String TYPE = "type";
    private String UTILNAME = "utilname";
    private String DISCKEY = "key_for_discovery";
    private String CANEDIT = "is_value_editable";

    private ArrayList assignedResNames = null;
    private ArrayList assignedRgNames = null;


    /**
     * Creates a new instance of SapReviewPanel
     */
    public SapReviewPanel() {
        super();
    }

    /**
     * Creates a SapReviewPanel Panel with the given name and tree manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public SapReviewPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    public SapReviewPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;

        // Get the I18N instance
        this.wizardi18n = wizardi18n;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {
        String title = wizardi18n.getString("sap.sapReviewPanel.title");
        String table_title = wizardi18n.getString(
                "sap.sapReviewPanel.table.title");
        String desc = wizardi18n.getString("sap.sapReviewPanel.desc");
        String help_text = wizardi18n.getString("hasp.reviewpanel.cli.help");
        String yes = wizardi18n.getString("cliwizards.yes");
        String no = wizardi18n.getString("cliwizards.no");
        String confirm_txt = wizardi18n.getString("cliwizards.confirmMessage");
        String continue_txt = wizardi18n.getString("cliwizards.continue");
        String back = wizardi18n.getString("ttydisplay.back.char");

        String propNameText = wizardi18n.getString("hasp.reviewpanel.propname");
        String propTypeText = wizardi18n.getString("hasp.reviewpanel.proptype");
        String propDescText = wizardi18n.getString("hasp.reviewpanel.propdesc");
        String currValText = wizardi18n.getString("hasp.reviewpanel.currval");
        String newValText = wizardi18n.getString("hasp.reviewpanel.newval");
        String subheading[] = new String[2];
        subheading[0] = wizardi18n.getString(
                "sap.centralServicesDetailsPanel.table.col0");
        subheading[1] = wizardi18n.getString(
                "sap.centralServicesDetailsPanel.table.col1");

        String rsname_assigned = wizardi18n.getString(
                "reviewpanel.resourcename.assigned");
        String rgname_assigned = wizardi18n.getString(
                "reviewpanel.rgname.assigned");

        Boolean enqSelected;
        Boolean scsSelected;
        String option;

        DataServicesUtil.clearScreen();

        // Title
        TTYDisplay.clear(1);
        TTYDisplay.printTitle(title);
        TTYDisplay.pageText(desc);
        TTYDisplay.clear(2);

        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        HashMap customTag = new HashMap();
        customTag.put(wizardi18n.getString(
                "sap.centralServicesDetailsPanel.custom.key"),
            wizardi18n.getString(
                "sap.centralServicesDetailsPanel.custom.keyText"));

        HashMap tableEntries = new HashMap();
        int i = 0;

        while (true) {
            assignedResNames = new ArrayList();
            assignedRgNames = new ArrayList();

            TTYDisplay.clear(1);
            TTYDisplay.setNavEnabled(true);

            Vector data = new Vector();

            HashMap enqRow = new HashMap();
            enqRow.put(NAME, "sap.sapReviewPanel.enqrg.name");
            enqRow.put(DESC, "sap.sapReviewPanel.enqrg.desc");
            enqRow.put(TYPE, "sap.sapReviewPanel.enqrg.type");
            enqRow.put(KEY, Util.CS_RG_NAME);
            enqRow.put(DISCKEY, Util.SAP_CS_RG_NAME);
            enqRow.put(UTILNAME, Util.RG_NAME);

            Boolean isNewRG = (Boolean) wizardModel.getWizardValue(
                    SapWebasConstants.IS_NEW_RG_CS);

            if (isNewRG.booleanValue()) {
                enqRow.put(CANEDIT, new Boolean(true));
            } else {
                enqRow.put(CANEDIT, new Boolean(false));
            }

            String retVal = addtoVector(data, enqRow);
            tableEntries.put(String.valueOf(++i), enqRow);

            if (retVal != null) {

                if (!assignedRgNames.contains(retVal)) {
                    assignedRgNames.add(retVal);
                }
            }

            HashMap enqResRow = new HashMap();
            enqResRow.put(NAME, "sap.sapReviewPanel.enqres.name");
            enqResRow.put(DESC, "sap.sapReviewPanel.enqres.desc");
            enqResRow.put(TYPE, "sap.sapReviewPanel.enqres.type");
            enqResRow.put(KEY, Util.ENQ_RES_NAME);
            enqResRow.put(DISCKEY, Util.ENQ_R_NAME);
            enqResRow.put(CANEDIT, new Boolean(true));
            enqResRow.put(UTILNAME, Util.RS_NAME);
            retVal = addtoVector(data, enqResRow);
            tableEntries.put(String.valueOf(++i), enqResRow);

            if (retVal != null) {

                if (!assignedResNames.contains(retVal)) {
                    assignedResNames.add(retVal);
                }
            }

            HashMap scsResRow = new HashMap();
            scsResRow.put(NAME, "sap.sapReviewPanel.scsres.name");
            scsResRow.put(DESC, "sap.sapReviewPanel.scsres.desc");
            scsResRow.put(TYPE, "sap.sapReviewPanel.scsres.type");
            scsResRow.put(KEY, Util.SCS_RES_NAME);
            scsResRow.put(DISCKEY, Util.SCS_R_NAME);
            scsResRow.put(CANEDIT, new Boolean(true));
            scsResRow.put(UTILNAME, Util.RS_NAME);
            retVal = addtoVector(data, scsResRow);
            tableEntries.put(String.valueOf(++i), scsResRow);

            if (retVal != null) {

                if (!assignedResNames.contains(retVal)) {
                    assignedResNames.add(retVal);
                }
            }

            HashMap csNewHaspRow = new HashMap();
            csNewHaspRow.put(NAME, "sap.sapReviewPanel.cslh.name");
            csNewHaspRow.put(DESC, "sap.sapReviewPanel.cslh.desc");
            csNewHaspRow.put(TYPE, "sap.sapReviewPanel.cslh.type");
            csNewHaspRow.put(KEY, Util.CS_LH_NAME);
            csNewHaspRow.put(DISCKEY, Util.CS_LH_NAME);
            csNewHaspRow.put(UTILNAME, Util.RS_NAME);

            if (wizardModel.getWizardValue(Util.CS_NEW_HOSTS) != null) {
                csNewHaspRow.put(CANEDIT, new Boolean(true));
            } else {
                csNewHaspRow.put(CANEDIT, new Boolean(false));
            }

            retVal = addtoVector(data, csNewHaspRow);

            if (retVal != null) {
                tableEntries.put(String.valueOf(++i), csNewHaspRow);

                if (!assignedResNames.contains(retVal)) {
                    assignedResNames.add(retVal);
                }
            }

            csNewHaspRow = new HashMap();
            csNewHaspRow.put(NAME, "sap.sapReviewPanel.cshasp.name");
            csNewHaspRow.put(DESC, "sap.sapReviewPanel.cshasp.desc");
            csNewHaspRow.put(TYPE, "sap.sapReviewPanel.cshasp.type");
            csNewHaspRow.put(KEY, Util.CS_STORAGE_NAME);
            csNewHaspRow.put(DISCKEY, Util.CS_STORAGE_NAME);
            csNewHaspRow.put(UTILNAME, Util.RS_NAME);

            if ((wizardModel.getWizardValue(Util.CS_SELECTED_FS) != null) ||
                    (wizardModel.getWizardValue(Util.CS_SELECTED_GD) != null)) {
                csNewHaspRow.put(CANEDIT, new Boolean(true));
            } else {
                csNewHaspRow.put(CANEDIT, new Boolean(false));
            }

            retVal = addtoVector(data, csNewHaspRow);

            if (retVal != null) {
                tableEntries.put(String.valueOf(++i), csNewHaspRow);

                if (!assignedResNames.contains(retVal)) {
                    assignedResNames.add(retVal);
                }
            }

            Boolean isRepSelected = (Boolean) wizardModel.getWizardValue(
                    Util.REP_COMPONENT);

            if (isRepSelected.booleanValue()) {
                HashMap repRow = new HashMap();
                repRow.put(NAME, "sap.sapReviewPanel.reprg.name");
                repRow.put(DESC, "sap.sapReviewPanel.reprg.desc");
                repRow.put(TYPE, "sap.sapReviewPanel.reprg.type");
                repRow.put(KEY, Util.REP_RG_NAME);
                repRow.put(DISCKEY, Util.SAP_REP_RG_NAME);
                repRow.put(UTILNAME, Util.RG_NAME);

                Boolean zIsNewRG = (Boolean) wizardModel.getWizardValue(
                        SapWebasConstants.IS_NEW_RG_REP);

                if (zIsNewRG.booleanValue()) {
                    repRow.put(CANEDIT, new Boolean(true));
                } else {
                    repRow.put(CANEDIT, new Boolean(false));
                }

                retVal = addtoVector(data, repRow);
                tableEntries.put(String.valueOf(++i), repRow);

                if (retVal != null) {

                    if (!assignedRgNames.contains(retVal)) {
                        assignedRgNames.add(retVal);
                    }
                }

                HashMap repResRow = new HashMap();
                repResRow.put(NAME, "sap.sapReviewPanel.repres.name");
                repResRow.put(DESC, "sap.sapReviewPanel.repres.desc");
                repResRow.put(TYPE, "sap.sapReviewPanel.repres.type");
                repResRow.put(KEY, Util.REP_RES_NAME);
                repResRow.put(DISCKEY, Util.REP_R_NAME);
                repResRow.put(CANEDIT, new Boolean(true));
                repResRow.put(UTILNAME, Util.RS_NAME);
                retVal = addtoVector(data, repResRow);
                tableEntries.put(String.valueOf(++i), repResRow);

                if (retVal != null) {

                    if (!assignedResNames.contains(retVal)) {
                        assignedResNames.add(retVal);
                    }
                }

                csNewHaspRow = new HashMap();
                csNewHaspRow.put(NAME, "sap.sapReviewPanel.replh.name");
                csNewHaspRow.put(DESC, "sap.sapReviewPanel.replh.desc");
                csNewHaspRow.put(TYPE, "sap.sapReviewPanel.replh.type");
                csNewHaspRow.put(KEY, Util.REP_LH_NAME);
                csNewHaspRow.put(DISCKEY, Util.REP_LH_NAME);
                csNewHaspRow.put(UTILNAME, Util.RS_NAME);

                if (wizardModel.getWizardValue(Util.REP_NEW_HOSTS) != null) {
                    csNewHaspRow.put(CANEDIT, new Boolean(true));
                } else {
                    csNewHaspRow.put(CANEDIT, new Boolean(false));
                }

                retVal = addtoVector(data, csNewHaspRow);

                if (retVal != null) {
                    tableEntries.put(String.valueOf(++i), csNewHaspRow);

                    if (!assignedResNames.contains(retVal)) {
                        assignedResNames.add(retVal);
                    }
                }

                csNewHaspRow = new HashMap();
                csNewHaspRow.put(NAME, "sap.sapReviewPanel.rephasp.name");
                csNewHaspRow.put(DESC, "sap.sapReviewPanel.rephasp.desc");
                csNewHaspRow.put(TYPE, "sap.sapReviewPanel.rephasp.type");
                csNewHaspRow.put(KEY, Util.REP_STORAGE_NAME);
                csNewHaspRow.put(DISCKEY, Util.REP_STORAGE_NAME);
                csNewHaspRow.put(UTILNAME, Util.RS_NAME);

                if ((wizardModel.getWizardValue(Util.REP_SELECTED_FS) !=
                            null) ||
                        (wizardModel.getWizardValue(Util.REP_SELECTED_GD) !=
                            null)) {
                    csNewHaspRow.put(CANEDIT, new Boolean(true));
                } else {
                    csNewHaspRow.put(CANEDIT, new Boolean(false));
                }

                retVal = addtoVector(data, csNewHaspRow);

                if (retVal != null) {
                    tableEntries.put(String.valueOf(++i), csNewHaspRow);

                    if (!assignedResNames.contains(retVal)) {
                        assignedResNames.add(retVal);
                    }
                }
            }

            option = TTYDisplay.create2DTable(table_title, subheading, data,
                    customTag, help_text);

            if (option.equals(back)) {
                this.cancelDirection = true;

                return;
            }

            this.cancelDirection = false;

            String sprop;
            String currVal;
            String newVal;
            HashMap selectedRow = (HashMap) tableEntries.get(option);

            if (selectedRow == null) {
                break;
            }

            TTYDisplay.setNavEnabled(false);
            TTYDisplay.pageText(propNameText +
                wizardi18n.getString((String) selectedRow.get(NAME)));

            TTYDisplay.pageText(propDescText +
                wizardi18n.getString((String) selectedRow.get(DESC)));

            TTYDisplay.pageText(propTypeText +
                wizardi18n.getString((String) selectedRow.get(TYPE)));

            String value = (String) selectedRow.get(VALUE);
            TTYDisplay.pageText(currValText + value);

            Boolean canEdit = (Boolean) selectedRow.get(CANEDIT);
            String utilName = (String) selectedRow.get(UTILNAME);

            if (canEdit.booleanValue()) {
                ErrorValue retValue = new ErrorValue();
                retValue.setReturnVal(Boolean.TRUE);

                do {
                    newVal = TTYDisplay.promptForEntry(newValText);

                    if ((newVal != null) && (newVal.trim().length() > 0)) {
                        retValue = SapWebasWizardCreator.validateData((String)
                                selectedRow.get(DISCKEY), newVal, wizardModel);

                        if (retValue.getReturnValue().booleanValue()) {

                            // check for duplicates in the same wizard
                            // configuration
                            if (utilName.equals(Util.RS_NAME)) {
                                assignedResNames.remove(value);

                                if (assignedResNames.contains(newVal)) {
                                    TTYDisplay.printError(rsname_assigned);
                                    assignedResNames.add(value);
                                    newVal = null;
                                } else {
                                    assignedResNames.add(newVal);
                                    wizardModel.setWizardValue((String)
                                        selectedRow.get(KEY), newVal);
                                }
                            } else if (utilName.equals(Util.RG_NAME)) {
                                assignedRgNames.remove(value);

                                if (assignedRgNames.contains(newVal)) {
                                    TTYDisplay.printError(rgname_assigned);
                                    assignedRgNames.add(value);
                                    newVal = null;
                                } else {
                                    assignedRgNames.add(newVal);
                                    wizardModel.setWizardValue((String)
                                        selectedRow.get(KEY), newVal);
                                }
                            } else {
                                wizardModel.setWizardValue((String) selectedRow
                                    .get(KEY), newVal);
                            }
                        } else {
                            TTYDisplay.printError(retValue.getErrorString());
                        }
                    }
                } while ((newVal == null) || (newVal.trim().length() == 0) ||
                    !retValue.getReturnValue().booleanValue());
            } else {
                TTYDisplay.clear(1);
                TTYDisplay.promptForEntry(continue_txt);
            }
        }
    }

    private String addtoVector(Vector xData, HashMap xElement) {
        String modelKey = (String) xElement.get(KEY);
        String discKey = (String) xElement.get(DISCKEY);
        String value = discoverOption(discKey, modelKey);

        if (value != null) {
            xElement.put(VALUE, value);
            xData.add(wizardi18n.getString((String) xElement.get(NAME)));
            xData.add(value);
        }

        return value;
    }

    private String discoverOption(String discKey, String modelKey) {
        String option = (String) wizardModel.getWizardValue(modelKey);

        if (option == null) {
            HashMap discoverdValues = (HashMap) wizardModel.getWizardValue(
                    SapWebasConstants.DISC_CLUSTER_OBJ);

            if (discoverdValues == null) {
                discoverdValues = doDiscovery();
            }

            option = (String) discoverdValues.get(discKey);
            wizardModel.setWizardValue(modelKey, option);
        }

        return option;
    }

    private HashMap doDiscovery() {
        SapWebasMBean mBean;
        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);
        JMXConnector connector = getConnection(nodeEndPoint);
        mBean = SapWebasWizardCreator.getMBean(connector);

        HashMap helper = new HashMap();
        String enq_i_no = (String) wizardModel.getWizardValue(
                Util.ENQ_INSTANCE_NUM);
        String scs_i_no = (String) wizardModel.getWizardValue(
                Util.SCS_INSTANCE_NUM);
        helper.put(Util.ENQ_I_NO, enq_i_no);
        helper.put(Util.SCS_I_NO, scs_i_no);

        HashMap values = mBean.discoverPossibilities(
                new String[] {
                    Util.SAP_CS_RG_NAME, Util.SAP_REP_RG_NAME, Util.ENQ_R_NAME,
                    Util.SCS_R_NAME, Util.REP_R_NAME
                }, helper);

        String newFS[] = (String[]) wizardModel.getWizardValue(
                Util.CS_SELECTED_FS);
        String newGD[] = (String[]) wizardModel.getWizardValue(
                Util.CS_SELECTED_GD);
        String properties[];
        String storageRsName;
        String lhRsName;
        HashMap helperData;
        HAStoragePlusMBean haspMBean = null;
        LogicalHostMBean lhMBean = null;
        String disResourcePropertyName[];
        HashMap discoveredMap;

        if ((newFS != null) || (newGD != null)) {
            properties = new String[] { Util.RS_NAME };
            helperData = new HashMap();
            helperData.put(Util.HASP_ALL_FILESYSTEMS, newFS);
            helperData.put(Util.HASP_ALL_DEVICES, newGD);
            haspMBean = HASPWizardCreator.getHAStoragePlusMBean();
            discoveredMap = haspMBean.discoverPossibilities(properties,
                    helperData);
            storageRsName = (String) discoveredMap.get(Util.RS_NAME);
            wizardModel.setWizardValue(Util.CS_STORAGE_NAME, storageRsName);
            values.put(Util.CS_STORAGE_NAME, storageRsName);
        }

        String hostNameList[] = (String[]) wizardModel.getWizardValue(
                Util.CS_NEW_HOSTS);

        if (hostNameList != null) {
            lhMBean = LogicalHostWizardCreator.getLogicalHostMBean(
                    getConnection(Util.getClusterEndpoint()));
            disResourcePropertyName = new String[] { Util.RESOURCENAME };
            discoveredMap = lhMBean.discoverPossibilities(
                    disResourcePropertyName,
                    SapWebasWizardCreator.convertArraytoString(hostNameList));
            lhRsName = (String) discoveredMap.get(Util.RESOURCENAME);
            wizardModel.setWizardValue(Util.CS_LH_NAME, lhRsName);
            values.put(Util.CS_LH_NAME, lhRsName);
        }

        newFS = (String[]) wizardModel.getWizardValue(Util.REP_SELECTED_FS);
        newGD = (String[]) wizardModel.getWizardValue(Util.REP_SELECTED_GD);

        if ((newFS != null) || (newGD != null)) {
            properties = new String[] { Util.RS_NAME };
            helperData = new HashMap();
            helperData.put(Util.HASP_ALL_FILESYSTEMS, newFS);
            helperData.put(Util.HASP_ALL_DEVICES, newGD);

            String csHaspResName = (String) wizardModel.getWizardValue(
                    Util.CS_STORAGE_NAME);

            if (csHaspResName != null) {
                ArrayList exemptList = new ArrayList();
                exemptList.add(csHaspResName);
                helperData.put(Util.EXEMPT, exemptList);
            }

            if (haspMBean == null) {
                haspMBean = HASPWizardCreator.getHAStoragePlusMBean();
            }

            discoveredMap = haspMBean.discoverPossibilities(properties,
                    helperData);
            storageRsName = (String) discoveredMap.get(Util.RS_NAME);
            wizardModel.setWizardValue(Util.REP_STORAGE_NAME, storageRsName);
            values.put(Util.REP_STORAGE_NAME, storageRsName);
        }

        hostNameList = (String[]) wizardModel.getWizardValue(
                Util.REP_NEW_HOSTS);

        if (hostNameList != null) {

            if (lhMBean == null) {
                lhMBean = LogicalHostWizardCreator.getLogicalHostMBean(
                        getConnection(Util.getClusterEndpoint()));
            }

            disResourcePropertyName = new String[] { Util.RESOURCENAME };
            discoveredMap = lhMBean.discoverPossibilities(
                    disResourcePropertyName,
                    SapWebasWizardCreator.convertArraytoString(hostNameList));
            lhRsName = (String) discoveredMap.get(Util.RESOURCENAME);
            wizardModel.setWizardValue(Util.REP_LH_NAME, lhRsName);
            values.put(Util.REP_LH_NAME, lhRsName);
        }

        wizardModel.setWizardValue(SapWebasConstants.DISC_CLUSTER_OBJ, values);

        return values;
    }
}
