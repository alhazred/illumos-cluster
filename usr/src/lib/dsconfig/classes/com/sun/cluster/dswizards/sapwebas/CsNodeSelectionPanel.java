/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)CsNodeSelectionPanel.java 1.13     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.sapwebas;

// J2SE
import java.util.ArrayList;
import java.util.List;

// JMX
import javax.management.remote.JMXConnector;

// CMASS
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.dataservices.sapwebas.SapWebasMBean;

// Wizard Common
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.NodeSelectionPanel;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.hastorageplus.HASPWizardConstants;


public class CsNodeSelectionPanel extends NodeSelectionPanel {

    private String sap;


    /**
     * Creates a new instance of CsNodeSelectionPanel
     */
    public CsNodeSelectionPanel() {
    }

    /**
     * Creates an CsNodeSelectionPanel Panel with the given name and manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public CsNodeSelectionPanel(String name, WizardTreeManager manager) {
        super(name, manager);
    }

    public CsNodeSelectionPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(name, model, wizardFlow, wizardi18n);
        initStrings();
    }


    private void initStrings() {
        title = wizardi18n.getString("sap.nodeSelectionPanel.title");
        desc = wizardi18n.getString("sap.nodeSelectionPanel.desc");
        sap = wizardi18n.getString("dswizards.sap.wizard");
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        wizardModel.selectCurrWizardContext();

        String nodeList[] = displayPanel(SapWebasMBean.class, sap);

        if (nodeList == null) {
            this.cancelDirection = true;

            return;
        }

        wizardModel.setWizardValue(Util.RG_NODELIST, nodeList);
        initWizardModel();
        wizardModel.setWizardValue(SapWebasConstants.CS_ZONES_PRESENT,
            (String) wizardModel.getWizardValue(
                HASPWizardConstants.ZONES_PRESENT));
    }

    /**
     * Initializes the Wizard Model by filling with the PropertyNames and
     * Discovered values values
     */
    public void initWizardModel() {
        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);
        JMXConnector connector = getConnection(nodeEndPoint);
        SapWebasMBean mBean = SapWebasWizardCreator.getMBean(connector);
        String propertyNames[];

        if (mBean != null) {
            propertyNames = mBean.getAllProperties();
            wizardModel.setWizardValue(Util.PROPERTY_NAMES, propertyNames);
        }

        // Set the property names to be serialized
        List toBeSerialized = new ArrayList();
        toBeSerialized.add(Util.RG_NODELIST);
        toBeSerialized.add(Util.SAP_SID);
        toBeSerialized.add(Util.ENQ_SERVER_LOC);
        toBeSerialized.add(Util.ENQ_INSTANCE_NUM);
        toBeSerialized.add(Util.ENQ_SERVER_PROFILE_LOC);
        toBeSerialized.add(Util.SCS_INSTANCE_NUM);
        toBeSerialized.add(Util.SCS_INSTANCE_NAME);
        wizardModel.setWizardValue(Util.WIZARD_STATE, toBeSerialized);
    }


    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }
}
