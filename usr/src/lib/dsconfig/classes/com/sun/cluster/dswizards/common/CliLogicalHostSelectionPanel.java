/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)CliLogicalHostSelectionPanel.java	1.15	08/12/10 SMI"
 */


package com.sun.cluster.dswizards.common;

import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.utils.DataServicesUtil;
import com.sun.cluster.agent.dataservices.utils.InfrastructureMBean;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.rgm.property.ResourceProperty;
import com.sun.cluster.common.TrustedMBeanModel;
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.ResourcePropertyUtil;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.management.remote.JMXConnector;


/**
 * The Panel Class that gets a Hostname or list of Hostnames as input from the
 * User.
 */
public class CliLogicalHostSelectionPanel extends WizardLeaf {
    protected static final String SELECT_EXISTING = "1";
    protected static final String CREATE_NEW = "2";

    private static final String LH_TABLE = "LH_Table";
    private static final String LH_SEL_INDICES = "LH_Selected_Indices";
    protected static final String LH_SEL_RS = "LH_Selected_Resources";
    protected static final String LH_SEL_RG = "LH_Selected_RGs";

    private String panelName; // Panel Name
    protected WizardI18N wizardi18n; // Message Bundle
    protected CliDSConfigWizardModel wizardModel; // WizardModel

    protected InfrastructureMBean mbean = null;

    protected String back;
    protected String yes;
    protected String no;
    protected String title;
    protected String desc;
    protected String subtitle;
    protected String cchar;
    protected String createnew_txt;
    protected String rchar;
    protected String refresh;
    protected String dchar;
    protected String done;
    protected String heading1;
    protected String heading2;
    protected String heading3;
    protected String help_text;
    protected String continue_txt;
    protected String table_empty_continue;
    protected String empty;

    private int COL_COUNT = 3;
    private Vector table = null;

    /**
     * Creates a new instance of CliLogicalHostSelectionPanel
     */
    public CliLogicalHostSelectionPanel() {
        super();
    }

    /**
     * Creates a CliLogicalHostSelectionPanel Panel with the given name and manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public CliLogicalHostSelectionPanel(String name,
        WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    public CliLogicalHostSelectionPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;
        this.wizardi18n = wizardi18n;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    public void consoleInteraction(boolean singleSelection) {

        subtitle = "";
        title = wizardi18n.getString("logicalhostSelectionPanel.title");
        help_text = wizardi18n.getString("logicalhostSelectionPanel.cli.help");
        continue_txt = wizardi18n.getString("cliwizards.continue");
        table_empty_continue = wizardi18n.getString(
                "cliwizards.table.empty.continue");
        empty = wizardi18n.getString("logicalhostSelectionPanel.table.empty");
        heading1 = wizardi18n.getString(
                "logicalhostSelectionPanel.table.colheader.0");
        heading2 = wizardi18n.getString(
                "logicalhostSelectionPanel.table.colheader.1");
        heading3 = wizardi18n.getString(
                "logicalhostSelectionPanel.table.colheader.2");
        displayPanel(singleSelection);
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.
     *		WizardComponent#consoleInteraction
     */
    protected void displayPanel(boolean singleSelection) {

        back = wizardi18n.getString("ttydisplay.back.char");
        yes = wizardi18n.getString("cliwizards.yes");
        no = wizardi18n.getString("cliwizards.no");

        cchar = wizardi18n.getString("ttydisplay.c.char");
        createnew_txt = wizardi18n.getString("ttydisplay.menu.create.new");
        rchar = wizardi18n.getString("ttydisplay.r.char");
        refresh = wizardi18n.getString("ttydisplay.menu.refresh");
        dchar = wizardi18n.getString("ttydisplay.d.char");
        done = wizardi18n.getString("ttydisplay.menu.done");

        // reset wizardmodel keys
        wizardModel.setWizardValue(Util.CREATE_NEW, null);

        refreshRGList();
        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        List rgList = null;
        boolean createnew_choice = false;
        boolean hostOk = false;

        while (!hostOk) {
            rgList = (List) wizardModel.getWizardValue(Util.LH_RGLIST);

            HashMap customTags = new HashMap();

            // Title
            TTYDisplay.printTitle(title);

            String zHostNameList[] = (String[]) wizardModel.getWizardValue(
                    Util.HOSTNAMELIST);

            if ((zHostNameList != null) && (zHostNameList.length > 0)) {
                String newlyAdded = ResourcePropertyUtil.join(zHostNameList,
                        Util.COMMA);
                TTYDisplay.printSubTitle(wizardi18n.getString(
                        "lhlistpanel.alert.summary",
                        new String[] { newlyAdded }));
            }

            // Description Para
            TTYDisplay.printSubTitle(desc);

            if (!singleSelection) {
                TTYDisplay.printSubTitle(wizardi18n.getString(
                        "logicalhostSelectionPanel.extraDesc"));
            }

            populateTable(rgList);

            customTags.put(cchar, createnew_txt);
            customTags.put(rchar, refresh);
            customTags.put(dchar, done);

            List optionsList = null;
            String enteredKey = "";
            String option[] = null;

            // get older selections
            List oldSelections = (List) wizardModel.getWizardValue(
                    LH_SEL_INDICES);

            String subheadings[] = new String[] {
                    heading1, heading2, heading3
                };

            if ((table != null) && (table.size() > 0)) {

                // Display the (n*3) Table
                option = TTYDisplay.create2DTable(subtitle, subheadings, table,
                        table.size() / COL_COUNT, COL_COUNT, customTags,
                        help_text, empty, singleSelection, oldSelections);

                if (option == null) {
                    optionsList = null;
                } else {
                    optionsList = Arrays.asList(option);
                }
            } else {

                // Print empty text and automatically select 'Create New'
                TTYDisplay.displaySubheadings(subheadings);
                TTYDisplay.printSubTitle(empty);
                enteredKey = TTYDisplay.promptForEntry(table_empty_continue,
                        help_text);
                optionsList = new ArrayList();
                optionsList.add(cchar);
            }

            if ((optionsList == null) || enteredKey.equals(back)) {
                this.cancelDirection = true;

		// refresh the resource list
                wizardModel.setWizardValue(LH_TABLE, null);
                wizardModel.setWizardValue(LH_SEL_INDICES, null);
                wizardModel.setWizardValue(Util.SEL_HOSTNAMES, null);
                refreshRGList();

                return;
            }

            if (optionsList.contains(rchar)) {

                // refresh the resource list
                wizardModel.setWizardValue(LH_TABLE, null);
                wizardModel.setWizardValue(LH_SEL_INDICES, null);
                wizardModel.setWizardValue(Util.SEL_HOSTNAMES, null);
                refreshRGList();

                continue;
            }

            if ((optionsList.size() == 1) && optionsList.contains(dchar)) {
                continue;
            }

            hostOk = true;

            TTYDisplay.setNavEnabled(true);
            this.cancelDirection = false;

            // remove all customTags from tmp_selections
            List tmp_selections = new ArrayList(optionsList);

            Iterator keyIterator = (customTags.keySet()).iterator();

            while (keyIterator.hasNext()) {
                String key = (String) keyIterator.next();
                tmp_selections.remove(key);
            }

            String selectedList[] = (String[]) tmp_selections.toArray(
                    new String[0]);

            // validateInput. Make sure the RGs are the same for the selected
            // resources
            // get index of RSName
            String rsName1 = null;
            String rsName2 = null;
            String rgName1 = null;
            String rgName2 = null;

            String rgName = null;
            boolean error = false;
            int i1 = 0;
            Vector selectedResources = new Vector();

            while ((i1 < (selectedList.length - 1)) && !error) {
                String optionValue1 = (String) selectedList[i1];

                try {
                    int value = Integer.parseInt(optionValue1);
                    rsName1 = getRSName(value);

                    if ((rsName1 != null) && (rsName1.trim().length() > 0)) {

                        // existing resource
                        if (!selectedResources.contains(rsName1)) {
                            selectedResources.add(rsName1);
                        }

                        rgName1 = getRGName(value);
                    }
                } catch (NumberFormatException nfe) {
                    // Custom Tags
                }

                for (int i2 = (i1 + 1); i2 < selectedList.length; i2++) {
                    String optionValue2 = (String) selectedList[i2];

                    try {
                        int value = Integer.parseInt(optionValue2);
                        rsName2 = getRSName(value);

                        if ((rsName2 != null) &&
                                (rsName2.trim().length() > 0)) {

                            if (!selectedResources.contains(rsName2)) {
                                selectedResources.add(rsName2);
                            }

                            rgName2 = getRGName(value);
                        }
                    } catch (NumberFormatException nfe) {
                    }

                    if (((rgName1 != null) && (rgName1.trim().length() > 0)) &&
                            ((rgName2 != null) &&
                                (rgName2.trim().length() > 0))) {

                        if (!rgName1.equals(rgName2)) {
                            error = true;
                        }
                    }
                }

                if (!error) {
                    i1++;
                }
            }

            if (selectedList.length == 1) {
                String optionValue = (String) selectedList[0];

                try {
                    int value = Integer.parseInt(optionValue);
                    String rsName = getRSName(value);

                    if ((rsName != null) && (rsName.trim().length() > 0)) {

                        if (!selectedResources.contains(rsName)) {
                            selectedResources.add(rsName);
                        }

                        rgName1 = getRGName(value);
                    }
                } catch (NumberFormatException nfe) {
                }
            }

            rgName = (rgName1 != null) ? rgName1 : rgName2;

            if (error) {
                String errMsg = wizardi18n.getString(
                        "logicalhostSelectionPanel.rgmismatch.error",
                        new String[] { rsName1, rsName2 });
                TTYDisplay.printError(errMsg);
                TTYDisplay.promptForEntry(continue_txt);

                // Clear the selection
                wizardModel.setWizardValue(LH_SEL_RS, null);
                wizardModel.setWizardValue(LH_SEL_RG, null);
                wizardModel.setWizardValue(LH_SEL_INDICES, null);
                hostOk = false;

                continue;
            }

            if (optionsList.contains(cchar)) {

                // user chose to create logical host resource
                // handle create new
                wizardModel.setWizardValue(Util.CREATE_NEW, CREATE_NEW);
            } else {
                wizardModel.setWizardValue(Util.CREATE_NEW, SELECT_EXISTING);
                TTYDisplay.clear(1);
            }

            // store selection
            wizardModel.setWizardValue(LH_SEL_RS, selectedResources);
            wizardModel.setWizardValue(LH_SEL_RG, rgName);
            wizardModel.setWizardValue(LH_SEL_INDICES, tmp_selections);
        }
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#isDisplayComplete
     */
    public boolean isDisplayComplete() {
        return true;
    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }

    protected void refreshRGList() {
        TTYDisplay.clear(2);

        Thread t = TTYDisplay.busy(wizardi18n.getString(
                    "logicalhostSelectionPanel.discovery.busy"));
        t.start();

        if (mbean == null) {
            mbean = (InfrastructureMBean) getInfrastructureMBean();
        }

        String rgName = (String) wizardModel.getWizardValue(Util.SEL_RG);
        HashMap helperData = new HashMap();

        if (rgName == null) {
            String nodeList[] = (String[]) wizardModel.getWizardValue(
                    Util.RG_NODELIST);

            // construct a query and fetch resources
            helperData.put(Util.RG_NODELIST, nodeList);
        } else {

            // get all logical host resources in resource group rgName
            helperData.put(Util.RG_NAME, rgName);
        }

        HashMap map = mbean.discoverPossibilities(
                new String[] { Util.LH_RGLIST }, helperData);

        try {
            t.interrupt();
            t.join();
        } catch (Exception e) {

        }

        List rgs = (List) map.get(Util.LH_RGLIST);
        wizardModel.setWizardValue(Util.LH_RGLIST, rgs);
    }

    private void populateTable(List list) {
        table = (Vector) wizardModel.getWizardValue(LH_TABLE);

        if (table == null) {
            table = new Vector();

            if (list != null) {
                int size = list.size();

                for (int i = 0; i < size; i++) {
                    HashMap map = (HashMap) list.get(i);
                    String rgName = (String) map.get(Util.RG_NAME);
                    String rsName = (String) map.get(Util.RS_NAME);
                    String hostnames[] = (String[]) map.get(
                            Util.LH_HOSTLIST_EXT_PROP);

                    // add RS Name
                    table.add(rsName);

                    // add RG name
                    table.add(rgName);

                    // add first hostname
                    if ((hostnames != null) && (hostnames.length > 0)) {
                        table.add(hostnames[0]);
                    } else {
                        table.add("");
                    }

                }
            }
        }

        // default selections can be obtained from HOSTNAMELIST
        String existingHost[] = (String[]) wizardModel.getWizardValue(
                Util.HOSTNAMELIST);

        // display all the new selections in the table
        boolean addRow = false;

        if ((existingHost != null) && (existingHost.length > 0)) {

            // Handle duplicates
            String addedHostNames[] = (String[]) wizardModel.getWizardValue(
                    Util.SEL_HOSTNAMES);

            if (addedHostNames != null) {
                List addedHostList = Arrays.asList(addedHostNames);
                ArrayList newNodes = null;

                for (int x = 0; x < existingHost.length; x++) {

                    if (!addedHostList.contains(existingHost[x])) {

                        if (newNodes == null) {
                            newNodes = new ArrayList();
                        }

                        newNodes.add(existingHost[x]);
                    }
                }

                if (newNodes != null) {
                    existingHost = (String[]) newNodes.toArray(new String[0]);
                } else {
                    existingHost = null;
                }
            }
        }

        if ((existingHost != null) && (existingHost.length > 0)) {
            table.add("");
            table.add("");
            table.add(existingHost[0]);
            addRow = true;
        }

        if (addRow) {
            List selectedIndices = (List) wizardModel.getWizardValue(
                    LH_SEL_INDICES);

            if (selectedIndices == null) {
                selectedIndices = new ArrayList();
            }

            int rowIndex = table.size() / COL_COUNT;
            String stIndex = Integer.toString(rowIndex);

            if (!selectedIndices.contains(stIndex)) {
                selectedIndices.add(stIndex);
            }

            wizardModel.setWizardValue(LH_SEL_INDICES, selectedIndices);
        }


        // set the selections from HOSTNAMELIST to null
        // save selected hostnames
        String newHost[] = (String[]) wizardModel.getWizardValue(
                Util.SEL_HOSTNAMES);
        String selectedHost[] = null;

        if (existingHost != null) {

            if (newHost != null) {
                selectedHost = ResourcePropertyUtil.concatenateArrays(
                        existingHost, newHost);
            } else {
                selectedHost = existingHost;
            }
        } else {

            if (newHost != null) {
                selectedHost = newHost;
            }
        }

        wizardModel.setWizardValue(LH_TABLE, table);
        wizardModel.setWizardValue(Util.SEL_HOSTNAMES, selectedHost);
        wizardModel.setWizardValue(Util.HOSTNAMELIST, null);
    }

    private String getRSName(int index) {
        String rsName = null;

        try {
            rsName = (String) table.get((index * COL_COUNT) - 3);
        } catch (ArrayIndexOutOfBoundsException arrayEx) {
        }

        return rsName;
    }

    private String getRGName(int index) {
        String rgName = null;

        try {
            rgName = (String) table.get((index * COL_COUNT) - 2);
        } catch (ArrayIndexOutOfBoundsException arrayEx) {
        }

        return rgName;
    }

    private String getLogicalHostName(int index) {
        String hostname = null;

        try {
            hostname = (String) table.get((index * COL_COUNT) - 1);
        } catch (ArrayIndexOutOfBoundsException arrayEx) {
        }

        return hostname;
    }

    protected InfrastructureMBean getInfrastructureMBean() {
        JMXConnector localConnection = getConnection(Util.getClusterEndpoint());

        if (mbean == null) {

            try {
                mbean = (InfrastructureMBean) TrustedMBeanModel.getMBeanProxy(
                        Util.DOMAIN, localConnection, InfrastructureMBean.class,
                        null, false);
            } catch (Exception e) {
            }
        }

        return mbean;
    }
}
