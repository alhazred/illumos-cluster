/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)RACWizardsFlowManager.java	1.16	08/12/10 SMI"
 */

package com.sun.cluster.dswizards.oraclerac.invoker;

// CMASS
import com.sun.cluster.agent.dataservices.utils.Util;

// Wizard Common
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.common.DSConfigWizardModel;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.DSWizardInterface;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.oraclerac.framework.ORFWizardConstants;
import com.sun.cluster.dswizards.oraclerac.database.RacDBWizardConstants;
import com.sun.cluster.dswizards.oraclerac.storage.ORSWizardConstants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The class takes decision on which panel to show "next" depending upon the
 * conditions set at a particular context. This file is used only by the GUI
 * version of the RAC wizard. The CLI version uses the individual flow XML
 * files. See CR 6411478 and 6438080 The downside is that this flow XML file
 * must be manually synced whenever the flow or the number of panels changes in
 * the CLI and the GUI. The up side is that both GUI and CLI wizard can now have
 * independent flow and panel counts.
 */
public class RACWizardsFlowManager implements DSWizardInterface,
    RacDBWizardConstants {

    /**
     * Creates a new instance of RACWizardsFlowManager
     */
    public RACWizardsFlowManager() {
    }

    /**
     * @see  com.sun.cluster.dswizards.common.DSWizardInterface#getNextPanel
     */
    public String getNextPanel(String curPanelName, Object wizCxtObj,
        String options[]) {
	DSConfigWizardModel guiModel = null;
	CliDSConfigWizardModel cliModel = null;

	if (wizCxtObj instanceof CliDSConfigWizardModel) {
	    cliModel = (CliDSConfigWizardModel)wizCxtObj;
	} else if (wizCxtObj instanceof DSConfigWizardModel) {
	    guiModel = (DSConfigWizardModel)wizCxtObj;
	}

	if (curPanelName.equals(ORFWizardConstants.PANEL_2)) {
	    ArrayList frmwrkRT = null;
	    if (guiModel != null) {
		frmwrkRT = (ArrayList)guiModel.getWizardValue(
		    Util.FRMWRK_RT);
	    } else {
		frmwrkRT = (ArrayList)cliModel.getWizardValue(
		    Util.FRMWRK_RT);
	    }

	    String osArch = System.getProperty("os.arch");

	    if (frmwrkRT.contains(Util.RAC_UDLM_RTNAME) ||
		osArch.equals(Util.X86)) {
		// UDLM packages are present, so create the UDLM resources
		// Skip the clusterware selection panel.
		return options[1];
	    } else {
		// go to the clusterware selection panel
		return options[0];
	    }
	}

        if (curPanelName.equals(RACInvokerConstants.INVOKERPANEL)) {
            Boolean showFrameWork = null;
            Boolean showStorage = null;
            Boolean showDatabase = null;

	    if (guiModel != null) {
		showFrameWork = (Boolean)guiModel.getWizardValue(
                    RACInvokerConstants.FRAMEWORK_WIZ);

		showStorage = (Boolean)guiModel.getWizardValue(
                    RACInvokerConstants.STORAGE_WIZ);

		showDatabase = (Boolean)guiModel.getWizardValue(
                    RACInvokerConstants.DATABASE_WIZ);
	    } else {
		showFrameWork = (Boolean)cliModel.getWizardValue(
                    RACInvokerConstants.FRAMEWORK_WIZ);

		showStorage = (Boolean)cliModel.getWizardValue(
                    RACInvokerConstants.STORAGE_WIZ);

		showDatabase = (Boolean)cliModel.getWizardValue(
                    RACInvokerConstants.DATABASE_WIZ);
	    }

            if ((showFrameWork != null) && showFrameWork.booleanValue()) {
                return options[0];
            } else if ((showStorage != null) && showStorage.booleanValue()) {
                return options[1];
            } else if ((showDatabase != null) && showDatabase.booleanValue()) {
                return options[2];
            }
        }

        // No branching in the framework wizard panels
        // Storage Wizard Starts

        // If Current Panel is Intro Panel
        if (curPanelName.equals(ORSWizardConstants.INTROPANEL)) {
            Object showRACDevices = null;
            Object showRACFS = null;
            Boolean isStateFileAbsent = null;

	    if (guiModel != null) {
		showRACDevices = (Object)guiModel.getWizardValue(
                    ORSWizardConstants.SHOW_RAC_DEVICES);

		showRACFS = (Object)guiModel.getWizardValue(
                    ORSWizardConstants.SHOW_RAC_FS);

		isStateFileAbsent = (Boolean)guiModel.getWizardValue(
                    Util.RAC_STATE_FILE_ABSENT);
	    } else {
		showRACDevices = (Object)cliModel.getWizardValue(
                    ORSWizardConstants.SHOW_RAC_DEVICES);

		showRACFS = (Object)cliModel.getWizardValue(
                    ORSWizardConstants.SHOW_RAC_FS);

		isStateFileAbsent = (Boolean)cliModel.getWizardValue(
                    Util.RAC_STATE_FILE_ABSENT);
	    }

            if ((isStateFileAbsent != null) &&
		isStateFileAbsent.booleanValue()) {
                return options[0];
            } else if (showRACDevices != null) {
                return options[1];
            } else if (showRACFS != null) {
                return options[2];
            }
        }

        if (curPanelName.equals(ORSWizardConstants.LOCATIONPANEL)) {
            String showRACDevices = null;
            String showRACFS = null;

	    if (guiModel != null) {
		showRACDevices = (String)guiModel.getWizardValue(
                    ORSWizardConstants.SHOW_RAC_DEVICES);

		showRACFS = (String)guiModel.getWizardValue(
                    ORSWizardConstants.SHOW_RAC_FS);
	    } else {
		showRACDevices = (String)cliModel.getWizardValue(
                    ORSWizardConstants.SHOW_RAC_DEVICES);

		showRACFS = (String)cliModel.getWizardValue(
                    ORSWizardConstants.SHOW_RAC_FS);
	    }

            if ((showRACDevices != null) && showRACDevices.equals("true")) {

                // display the device selection panel
                return options[0];
            } else if ((showRACFS != null) && showRACFS.equals("true")) {
                return options[1];
            }
        }

        // If Current Panel is RAC Disks Panel
        if (curPanelName.equals(ORSWizardConstants.DISKPANEL)) {
            Object showClusterDisksPanel = null;
            Object showRACFS = null;

	    if (guiModel != null) {
		showClusterDisksPanel = guiModel.getWizardValue(
                    ORSWizardConstants.SHOW_CLUSTER_DEVICES);

		showRACFS = guiModel.getWizardValue(
                    ORSWizardConstants.SHOW_RAC_FS);
	    } else {
		showClusterDisksPanel = cliModel.getWizardValue(
                    ORSWizardConstants.SHOW_CLUSTER_DEVICES);

		showRACFS = cliModel.getWizardValue(
                    ORSWizardConstants.SHOW_RAC_FS);
	    }

            // If the user had selected create new in the menu, go to the
            // ClusterDisksPanel
            if (showClusterDisksPanel != null) {
                return options[0];
            }
            // If user is done with configuring Disks, go to the RACFilesystems
            // Panel if he had selected QFS or NAS in the Framework
            // wizard/Location Panel
            else if ((showRACFS != null) && showRACFS.equals("true")) {
                return options[1];
            }
            // If the user is done with configuring Disks, go to the Review
            // Panel if filesystems need not be configured
            else {
                return options[2];
            }
        }

	if (curPanelName.equals(ORSWizardConstants.CLUSTERDISKPANEL)) {
	    // For CLI wizards can either go to the disk set panel or
	    // or the logical disks panel
	    String wizType = null;
	    if (guiModel != null) {
		wizType = (String)guiModel.getWizardValue(
		    ORSWizardConstants.WIZARD_TYPE);
	    } else {
		wizType = (String)cliModel.getWizardValue(
		    ORSWizardConstants.WIZARD_TYPE);
	    }

	    if (wizType == null) {
		// CLI wizard
		String showDisksSummary = null;
		if (guiModel != null) {
		    showDisksSummary = (String)guiModel.getWizardValue(
			ORSWizardConstants.SHOW_DISKS_SUMMARY);
		} else {
		    showDisksSummary = (String)cliModel.getWizardValue(
			ORSWizardConstants.SHOW_DISKS_SUMMARY);
		}
		if (showDisksSummary.equals("true")) {
		    // go back to disk set panel
		    return options[0];
		} else {
		    // go to logical device selection panel
		    return options[1];
		}
	    } else if (wizType.equals(ORSWizardConstants.GUI_WIZARD)) {
		//  go to the volume selection panel for all selected DGs
		return options[2];
	    }
	}

	if (curPanelName.equals(ORSWizardConstants.CLUSTERDISKSELVOLPANEL)) {
	    String selectVolumes = null;
	    if (guiModel != null) {
		selectVolumes = (String)guiModel.getWizardValue(
		    ORSWizardConstants.SELECT_VOLUMES);
	    } else {
		selectVolumes = (String)cliModel.getWizardValue(
		    ORSWizardConstants.SELECT_VOLUMES);
	    } 
	    if (selectVolumes.equals(ORSWizardConstants.FALSE)) {
		// go to disk set selection panel
		return options[0];
	    } else {
		// go to volume selection panel
		return options[1];
	    }
	} 

	if (curPanelName.equals(ORSWizardConstants.CLUSTERDISKVOLUMEPANEL)) {
	    // For CLI wizards go to the ClusterDisksPanel
	    String wizType = null;
	    if (guiModel != null) {
		wizType = (String)guiModel.getWizardValue(
		    ORSWizardConstants.WIZARD_TYPE);
	    } else {
		wizType = (String)cliModel.getWizardValue(
		    ORSWizardConstants.WIZARD_TYPE);
	    }
	    if (wizType == null) {
		// CLI wizard
		return options[0];
	    } else if (wizType.equals(ORSWizardConstants.GUI_WIZARD)) {
		// Move to volume selection panel for all wizards
		return options[1];
	    }
	}

        // If user is in Filesystems Panel
        if (curPanelName.equals(ORSWizardConstants.FSPANEL)) {
            Object showClusterFSPanel = null;
	    if (guiModel != null) {
		showClusterFSPanel = guiModel.getWizardValue(
                    ORSWizardConstants.SHOW_CLUSTER_FS);
	    } else {
		showClusterFSPanel = cliModel.getWizardValue(
                    ORSWizardConstants.SHOW_CLUSTER_FS);
	    }

            // If the user had selected create new in the menu, go to the
            // ClusterDisksPanel
            if (showClusterFSPanel != null) {
                return options[0];
            }
            // Goto review panel is user is done with the configuring FS
            else {
                return options[1];
            }
        }

        // Storage Wizard Ends
        // Database Panels Starts

        // if on version selection panel
        if (curPanelName.equals(PANEL_1)) {

            // get the user preference
            String version = null;
	    if (guiModel != null) {
		version = (String)guiModel.getWizardValue(Util.RAC_VER);
	    } else {
		version = (String)cliModel.getWizardValue(Util.RAC_VER);
	    }

            if (version.equals(Util.NINE)) {
                return options[0];
            } else {
                return options[1];
            }
        }

        // if on Oracle Home Panel
        if (curPanelName.equals(PANEL_3)) {

            // get the user preference
            Integer preference = null;
	    if (guiModel != null) {
		preference = (Integer)guiModel.getWizardValue(
                    Util.DB_COMPONENTS);
	    } else {
		preference = (Integer)cliModel.getWizardValue(
                    Util.DB_COMPONENTS);
	    }
            int prefInt = preference.intValue();

            if ((prefInt == Util.SERVER) || (prefInt == Util.BOTH_S_L)) {

                // go to Oracle Sid selection panel
                return options[0];
            } else if (prefInt == Util.LISTENER) {
                return options[1];
            }
        } else if (curPanelName.equals(PANEL_5)) {

            Object tmpObj = null;
	    if (guiModel != null) {
		tmpObj = guiModel.getWizardValue(ORAC_PROPS_ENTRY);
	    } else {
		tmpObj = cliModel.getWizardValue(ORAC_PROPS_ENTRY);
	    }

            if (tmpObj != null) {
                HashMap tmpMap = (HashMap) tmpObj;

                if (!tmpMap.isEmpty()) {
                    return options[3];
                }
            }

            Boolean isNodeComplete = null;
            Integer preference = null;

	    if (guiModel != null) {
		isNodeComplete = (Boolean)guiModel.getWizardValue(
                    Util.RAC_NODELIST_COMPLETE);

		preference = (Integer)guiModel.getWizardValue(
                    Util.DB_COMPONENTS);
	    } else {
		isNodeComplete = (Boolean)cliModel.getWizardValue(
                    Util.RAC_NODELIST_COMPLETE);

		preference = (Integer)cliModel.getWizardValue(
                    Util.DB_COMPONENTS);
	    }
            int prefInt = preference.intValue();

            if ((isNodeComplete == null) || isNodeComplete.booleanValue()) {

                if (prefInt == Util.SERVER) {

                    if (selectedHWRaid(wizCxtObj)) {

                        // the storage selection panel can be skipped
                        // move onto review panel
                        return options[5];
                    } else {

                        // go to storage selection panel
                        return options[2];
                    }
                } else if ((prefInt == Util.LISTENER) ||
                        (prefInt == Util.BOTH_S_L)) {
                    return options[1];
                }
            } else {

                if ((prefInt == Util.SERVER) || (prefInt == Util.BOTH_S_L)) {

                    // go to Sid panel to get next Nodes' value
                    return options[0];
                } else if (prefInt == Util.LISTENER) {

                    // go to same panel to get next Nodes' value
                    return options[4];
                }
            }
        } else if (curPanelName.equals(PANEL_6)) {
            String create_hasp = null;
	    if (guiModel != null) {
		create_hasp = (String)guiModel.getWizardValue(CREATE_HASP_RS);
	    } else {
		create_hasp = (String)cliModel.getWizardValue(CREATE_HASP_RS);
	    }

            if (create_hasp.equals(CREATE_HASP_RS)) {
                return options[0];
            } else {
                return options[1];
            }
        } else if (curPanelName.equals(PANEL_7)) {

            String wizardType = null;
            String create_lh = null;
	    Boolean isNodeComplete = null;
	    Integer preference = null;

	    if (guiModel != null) {
		wizardType = (String)guiModel.getWizardValue(WIZARD_TYPE);
		create_lh = (String)guiModel.getWizardValue(CREATE_LH_RS);
                isNodeComplete = (Boolean)guiModel.getWizardValue(
		    Util.RAC_NODELIST_COMPLETE);
		preference = (Integer)guiModel.getWizardValue(
		    Util.DB_COMPONENTS);
	    } else {
		wizardType = (String)cliModel.getWizardValue(WIZARD_TYPE);
		create_lh = (String)cliModel.getWizardValue(CREATE_LH_RS);
                isNodeComplete = (Boolean)cliModel.getWizardValue(
		    Util.RAC_NODELIST_COMPLETE);
		preference = (Integer)cliModel.getWizardValue(
		    Util.DB_COMPONENTS);
	    }

            if ((wizardType != null) && wizardType.equals(GUI_WIZARD)) {
                return options[4];
            }

            if (create_lh.equals(CREATE_LH_RS)) {
                return options[0];
            } else {

                if ((isNodeComplete == null) || isNodeComplete.booleanValue()) {
                    int prefInt = preference.intValue();

                    if ((prefInt == Util.SERVER) ||
                            (prefInt == Util.BOTH_S_L)) {

                        // check if storage selection panel can be skipped
                        if (selectedHWRaid(wizCxtObj)) {

                            // skip storage selection panel
                            return options[3];
                        } else {
                            return options[2];
                        }
                    } else if (prefInt == Util.LISTENER) {
                        return options[3];
                    }

                    return options[2];
                } else {
                    return options[1];
                }
            }
        } else if (curPanelName.equals(PANEL_11)) {

            if (selectedHWRaid(wizCxtObj)) {

                // skip storage selection panel
                return options[1];
            } else {
                return options[0];
            }
        }

        if (curPanelName.equals(VER10G_PANEL_0)) {
            boolean crsFrameworkPresent = false;
	    if (guiModel != null) {
		crsFrameworkPresent = ((Boolean)guiModel.getWizardValue(
		    Util.CRS_FRAMEWORK_PRESENT)).booleanValue();
	    } else {
		crsFrameworkPresent = ((Boolean)cliModel.getWizardValue(
		    Util.CRS_FRAMEWORK_PRESENT)).booleanValue();
	    }

            if (selectedHWRaid(wizCxtObj) || crsFrameworkPresent) {

                // skip CRS storage choice panel
                return options[1];
            } else {
                return options[0];
            }
        }

        if (curPanelName.equals(VER10G_PANEL_0_1)) {
            String storageChoice = null;
	    if (guiModel != null) {
		storageChoice = (String)guiModel.getWizardValue(
                    Util.CRS_STORAGE_CHOICE);
	    } else {
		storageChoice = (String)cliModel.getWizardValue(
                    Util.CRS_STORAGE_CHOICE);
	    }

            if (storageChoice.equals(Util.SELECT_EXISTING)) {

                // go to crs storage selection panel
                return options[0];
            } else {
                return options[1];
            }
        }

        if (curPanelName.equals(VER10G_PANEL_3)) {
            String wizardType = null;
            Boolean isNodeComplete = null;

            if ((wizardType != null) && wizardType.equals(GUI_WIZARD)) {
		wizardType = (String)guiModel.getWizardValue(WIZARD_TYPE);
		isNodeComplete = (Boolean)guiModel.getWizardValue(
                    Util.RAC10G_NODELIST_COMPLETE);
	    } else {
		wizardType = (String)cliModel.getWizardValue(WIZARD_TYPE);
		isNodeComplete = (Boolean)cliModel.getWizardValue(
                    Util.RAC10G_NODELIST_COMPLETE);
	    }

            if ((wizardType != null) && wizardType.equals(GUI_WIZARD)) {

                // check if storage selection panel needs to be skipped
                if (selectedHWRaid(wizCxtObj)) {

                    // skip storage selection panel
                    return options[2];
                } else {
                    return options[1];
                }
            }

            // sid selection panel

            if ((isNodeComplete == null) || isNodeComplete.booleanValue()) {

                if (selectedHWRaid(wizCxtObj)) {

                    // skip storage selection panel
                    return options[2];
                } else {
                    return options[1];
                }
            } else {
                return options[0];
            }
        }

        if (curPanelName.equals(PANEL_10)) {
            HashMap nodePropsMap = null;
            Integer ctr = null;

	    if (guiModel != null) {
		nodePropsMap = (HashMap)guiModel.getWizardValue(
                    ORAC_PROPS_ENTRY);
		ctr = (Integer)guiModel.getWizardValue(ORAC_PROPS_ENTRY_CTR);
	    } else {
		nodePropsMap = (HashMap)cliModel.getWizardValue(
                    ORAC_PROPS_ENTRY);
		ctr = (Integer)cliModel.getWizardValue(ORAC_PROPS_ENTRY_CTR);
	    } 
            int ctrVal = ctr.intValue();

            String nodeList[] = null;
	    if (guiModel != null) {
		nodeList = (String[])guiModel.getWizardValue(
                    Util.RG_NODELIST);
	    } else {
		nodeList = (String[])cliModel.getWizardValue(
                    Util.RG_NODELIST);
	    }
            String curNode = nodeList[0];
            String tmpArry[] = curNode.split(Util.COLON);
            curNode = tmpArry[0];

            ArrayList propsEntryData = (ArrayList) nodePropsMap.get(curNode);

            if (ctrVal < propsEntryData.size()) {
                return options[2];
            }

            Integer preference = null;
	    if (guiModel != null) {
		preference = (Integer)guiModel.getWizardValue(
                    Util.DB_COMPONENTS);
	    } else {
		preference = (Integer)cliModel.getWizardValue(
                    Util.DB_COMPONENTS);
	    } 
            int prefInt = preference.intValue();

            if (prefInt == Util.SERVER) {

                if (selectedHWRaid(wizCxtObj)) {

                    // skip storage selection panel and move onto review panel
                    return options[3];
                } else {
                    return options[1];
                }
            } else if ((prefInt == Util.LISTENER) ||
                    (prefInt == Util.BOTH_S_L)) {
                return options[0];
            }
        }

        // Database Panels Ends

        return null;
    }

    private boolean selectedHWRaid(Object wizCxtObj) {
	DSConfigWizardModel guiModel = null;
	CliDSConfigWizardModel cliModel = null;
	if (wizCxtObj instanceof DSConfigWizardModel) {
	    guiModel = (DSConfigWizardModel)wizCxtObj;
	} else if (wizCxtObj instanceof CliDSConfigWizardModel) {
	    cliModel = (CliDSConfigWizardModel)wizCxtObj;
	}

	StringBuffer newKey = new StringBuffer(Util.ORF_STORAGE_MGMT_SCHEMES);
	String selectedCluster = null;
	if (guiModel != null) {
	    selectedCluster = (String)guiModel.getWizardValue(
		Util.CLUSTER_SEL);
	} else {
	    selectedCluster = (String)cliModel.getWizardValue(
		Util.CLUSTER_SEL);
	}
	if (selectedCluster != null &&
	    selectedCluster.equals(Util.ZONE_CLUSTER)) {
	    String zoneClusterName = null;
	    if (guiModel != null) {
		zoneClusterName = (String)guiModel.getWizardValue(
		    Util.SEL_ZONE_CLUSTER);
	    } else {
		zoneClusterName = (String)cliModel.getWizardValue(
		    Util.SEL_ZONE_CLUSTER);
	    }
	    newKey.append(Util.COLON);
	    newKey.append(zoneClusterName);
	}
        List storageScheme = null;
	if (guiModel != null) {
	    storageScheme = (ArrayList)guiModel.getWizardValue(
                newKey.toString());
	} else {
	    storageScheme = (ArrayList)cliModel.getWizardValue(
                newKey.toString());
	}

        // storageScheme should never be null
        if ((storageScheme != null) && (storageScheme.size() == 1) &&
	    storageScheme.contains(Util.HWRAID)) {

            // user has selected Hardware RAID without VM option
            return true;
        }

        return false;
    }
}
