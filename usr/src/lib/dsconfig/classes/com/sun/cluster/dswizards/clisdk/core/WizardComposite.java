/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)WizardComposite.java 1.13     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.clisdk.core;

import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;

import java.util.*;


/**
 * WizardComposite implements the default behavior common to all
 * WizardComponents that have children.  This is the superclass of nodes (panel
 * objects that have child panels).
 *
 * <p>The wizard client panel tree consists of an arrangement of panels
 * (subclassed from WizardLeaf) and nodes (subclassed from WizardComposite). The
 * arrangement of the tree determines the order in which the panels will be
 * visited by the wizard.  Nodes can affect the traversal of the panels, which
 * allows advanced panels to be skipped for users that do not wish to use them.
 *
 * <p>"Composite" was chosen as a name for this class from the book "Design
 * Patterns".
 */
public class WizardComposite extends WizardComponent {

    // Change this value only when making a serializationally
    // incompatible change such as deleting a field. See the
    // Java Object Serialization Specification, Chapter 5:
    // Versioning of Serializable Objects, for more details.
    private static final long serialVersionUID = -4235618498158735851L;


    /**
     * A list of this objects children.
     *
     */
    protected Vector children = new Vector();

    /**
     * The cancel attribute indicates whether or not this composite will
     * determine the child to visit after a cancel has been performed.
     *
     */
    protected boolean cancelAttribute = false;

    /** The locking object for the cancel attribute. */
    private transient Object cancelAttributeLock = new Object();

    /**
     * Creates a WizardComposite with a route to the root of the server object
     * tree.
     */
    public WizardComposite() {
    }

    /**
     * Creates a WizardComposite with the specified name.
     *
     * @param  wizardModel  The CliDSConfigWizardModel object.
     * @param  name  The name for this WizardComposite.
     */
    public WizardComposite(CliDSConfigWizardModel wizardModel, String name,
        WizardFlow wizardFlow) {
        super(wizardModel, name, wizardFlow);
    }

    /**
     * Returns the first direct child of this WizardComposite having the
     * specified name.
     *
     * @param  childName  The name of the desired child.
     *
     * @return  The desired child or null if the named child is not a child of
     * this object.
     */
    public WizardComponent getChild(String childName) {
        Vector children = getChildVector();

        if (children != null) {
            Enumeration childList = null;

            synchronized (children) {
                childList = children.elements();
            }

            while (childList.hasMoreElements()) {
                WizardComponent child = (WizardComponent) childList
                    .nextElement();

                if (child.getName().equals(childName)) {

                    /*
                     * Found the child.
                     */
                    return (child);
                }
            }
        }

        return (null);
    }

    /**
     * Get a list of children of this WizardComposite object.
     *
     * @return  The children.
     */
    public Enumeration getChildren() {
        return (children.elements());
    }

    /**
     * Gets a vector containing the children of this WizardComposite.
     */
    protected Vector getChildVector() {
        return ((Vector) children.clone());
    }

    /**
     * Get the child that follows the specified child in the WizardComponent
     * tree.
     *
     * @param  currentChild  The child that comes before the desired child in
     * the wizard tree.
     *
     * @return  The next child in the wizard tree or null if no children follow
     * the specified child in the tree.
     */
    protected WizardComponent getNextChild(WizardComponent currentChild) {
        Vector children = getChildVector();

        if (children != null) {
            int currentChildIndex = children.indexOf(currentChild);

            if (currentChildIndex != -1) {

                /*
                 * The currentChild IS a child of this WizardComposite.
                 */
                if (children.size() > (currentChildIndex + 1)) {

                    /*
                     * The next child is also a child of this WizardComposite.
                     */
                    WizardComponent nextChild = (WizardComponent) children
                        .elementAt(currentChildIndex + 1);

                    if (nextChild.skip()) {

                        /*
                         * This child wants to be skipped.  Move to the
                         * next child.
                         */
                        return (getNextChild(nextChild));
                    } else {

                        if (nextChild instanceof WizardComposite) {
                            return (((WizardComposite) nextChild).next());
                        }

                        return (nextChild);
                    }
                } else {

                    /*
                     * The next child is not a child of this WizardComposite.
                     * Defer the next action to the parent.
                     */
                    WizardComponent parent = getParentComponent();

                    if (parent != null) {
                        return (parent.getNextChild(this));
                    }
                }
            }
        }

        return (null);
    }

    /**
     * Get the next WizardComponent in the tree.  For a composite, next will
     * return the first child.next() or null.
     *
     * @return  The next WizardComponent in the tree.
     */
    public WizardComponent next() {
        Vector children = getChildVector();

        if (children != null) {

            if (children.size() > 0) {
                WizardComponent nextChild = (WizardComponent) children
                    .firstElement();

                if (nextChild.skip()) {

                    /*
                     * This child wants to be skipped.  Move to the next child.
                     */
                    nextChild = getNextChild(nextChild);
                }

                if (!(nextChild instanceof WizardLeaf)) {

                    /*
                     * This is not a leaf of the tree.  Keep searching.
                     */
                    return (nextChild.next());
                }

                /*
                 * Found the next leaf of the tree.
                 */
                return (nextChild);
            }
        }

        /*
         * If this WizardComposite has no children, return this object.
         * That behavior gives the WizardComposite an opportunity to
         * add subtrees during the traversal and display of the tree.
         */
        return (this);
    }

    /**
     * Get the child that precedes the specified child in the WizardComponent
     * tree.
     *
     * @param  currentChild  The child that comes after the desired child in the
     * wizard tree.
     *
     * @return  The previous child in the wizard tree or null if no children
     * precede the specified child in the tree.
     */
    protected WizardComponent getPreviousChild(WizardComponent currentChild) {
        Vector children = getChildVector();

        if (children != null) {
            int currentChildIndex = children.indexOf(currentChild);

            if (currentChildIndex != -1) {

                /*
                 * The currentChild IS a child of this WizardComposite.
                 */
                if ((currentChildIndex - 1) >= 0) {

                    /*
                     * The previous child is also a child of this
                     * WizardComposite.
                     */
                    WizardComponent previousChild = (WizardComponent) children
                        .elementAt(currentChildIndex - 1);

                    if (previousChild.skip()) {

                        /*
                         * This child wants to be skipped.  Move to the
                         * previous child.
                         */
                        return (getPreviousChild(previousChild));
                    } else {

                        if (previousChild instanceof WizardComposite) {
                            return (previousChild.previous());
                        }

                        return (previousChild);
                    }
                } else {

                    /*
                     * The previous child is not a child of this
                     * WizardComposite.  Defer the previous action to the
                     * parent.
                     */
                    WizardComponent parent = getParentComponent();

                    if (parent != null) {
                        return (parent.getPreviousChild(this));
                    }
                }
            }
        }

        return (null);
    }

    /**
     * Get the previous WizardComponent in the tree.  For a composite, previous
     * will return the last child.previous() or null.
     *
     * @return  The previous WizardComponent in the tree.
     */
    public WizardComponent previous() {
        Vector children = getChildVector();

        if (children != null) {

            if (children.size() > 0) {
                WizardComponent previousChild = (WizardComponent) children
                    .lastElement();

                if (previousChild.skip()) {

                    /*
                     * This child wants to be skipped.  Move to the previous
                     * child.
                     */
                    previousChild = getPreviousChild(previousChild);
                }

                if (!(previousChild instanceof WizardLeaf)) {

                    /*
                     * This is not a leaf of the tree.  Keep searching.
                     */
                    return (previousChild.previous());
                }

                /*
                 * Found the previous leaf of the tree.
                 */
                return (previousChild);
            }
        }

        WizardComponent parent = getParentComponent();

        if (parent != null) {
            return (parent.getPreviousChild(this));
        }

        return (null);
    }

    /**
     * Add the specified child to this WizardComposite.  The order that the
     * children were added will be maintained.
     *
     * @param  child  The child to add to this object.
     */
    public final void addChild(WizardComponent child) {

        if (children == null) {
            children = new Vector();
        }

        child.setParentComponent(this);
        children.addElement(child);
    }

    /**
     * Inserts a child into this WizardComposite before the specified <code>
     * nextChild</code>.  This new child will be visited before <code>
     * nextChild</code>.
     *
     * @param  child  The child to add to this object.
     * @param  nextChild  The already-existing child to insert in front of.
     */
    public final void insertChild(WizardComponent child,
        WizardComponent nextChild) {

        if (children == null) {
            children = new Vector();
        }

        int index = -1;

        if (nextChild != null) {
            index = children.indexOf(nextChild);

            if (index != -1) {
                children.insertElementAt(child, index);
                child.setParentComponent(this);
            }
        } else {
        }
    }

    /**
     * Removes the specified child from this object.
     *
     * @param  child  The child to remove.
     */
    public final void removeChild(WizardComponent child) {

        if (children == null) {
            children = new Vector();
        }

        children.removeElement(child);
    }

    /**
     * Add the subtree of the specified name.
     *
     * @param  subtreeName  The name of the desired subtree.
     */
    public void addSubTree(String subtreeName) {

        /*
         * Get the subtree.
         */
        WizardComposite subtree = getWizardTreeManager().createSubTree(
                subtreeName);

        addChild(subtree);
    }

    /**
     * Reset this component, given the specified information. This method may be
     * called when a user backtracks in the client wizard tree and changes a
     * fundamental setting. The panel that enabled the modification might
     * traverse subsequent panels and call this method to resynchronize all
     * panels with the user's choice.
     *
     * @param  info  The information required to perform a reset.
     */
    public void reset(Object info) {
        Vector children = getChildVector();

        if (children != null) {

            for (int index = 0; index < children.size(); index++) {
                WizardComponent child = (WizardComponent) children.elementAt(
                        index);

                child.reset(info);
            }
        }
    }

    /**
     * Sets the cancel attribute of this node.
     *
     * <p>The default behavior of the cancel button is to identify the nearest
     * ancestor node with the cancel attribute set, and then traverse to the
     * previous child or the next child of that node.
     *
     * <p>The previous or next behavior of cancel is set by the current panel's
     * cancelDirection flag.
     *
     * @see  com.sun.dswizards.clisdk.core.WizardComponent#setCancelDirection(
     *		boolean)
     *
     * @param  cancelAttribute  If true, the cancel attribute of this node will
     * be set.
     */
    public void setCancel(boolean cancelAttribute) {

        synchronized (cancelAttributeLock) {
            this.cancelAttribute = cancelAttribute;
        }
    }

    /**
     * Returns true if the cancel attribute of this node is set.
     */
    public boolean isCancelSet() {

        synchronized (cancelAttributeLock) {
            return (this.cancelAttribute);
        }
    }

    /**
     * Cancel this panel.  This method is only called after the cancel operation
     * is confirmed by the user.
     *
     * @param  child  The child currently being visited.
     * @param  direction  The cancel direction.
     *
     * @return  The client panel that should be displayed after the cancel
     * operation.
     */
    public WizardComponent cancel(WizardComponent child, boolean direction) {

        if (isCancelSet()) {

            if (direction == false) {
                return (getPreviousChild(child));
            } else {
                return (getNextChild(child));
            }
        } else {
            WizardComponent parent = getParentComponent();

            if ((parent != null) && (parent instanceof WizardComposite)) {
                return (((WizardComposite) parent).cancel(this, direction));
            }

            return (null);
        }
    }
}
