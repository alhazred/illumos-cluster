/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)Rac10GDbNameSelectionPanel.java	1.17	09/04/22 SMI"
 */

package com.sun.cluster.dswizards.oraclerac.database.ver10g;

// CMASS
import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.utils.Util;

// Wizard Common
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliUtil;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.oraclerac.database.*;
import com.sun.cluster.dswizards.oraclerac.invoker.RACInvokerCreator;

// Model classes
import com.sun.cluster.model.RACModel;

// J2SE
import java.util.List;
import java.util.Vector;
import java.util.HashMap;

/**
 * This panel gets the DB Name from the User
 */
public class Rac10GDbNameSelectionPanel extends RacDBBasePanel {
    /**
     * Creates an Rac10GDbNameSelectionPanel for the wizard with the given name
     * and associates it with the WizardModel and WizardFlow.
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     * @param  wizardType  WizardType
     */
    public Rac10GDbNameSelectionPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(name, model, wizardFlow, wizardi18n);
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        title = wizardi18n.getString("oraclerac10g.dbNameSelectionPanel.title");
        desc = wizardi18n.getString("oraclerac10g.dbNameSelectionPanel.desc");

        String dbname_prompt = wizardi18n.getString(
                "oraclerac10g.dbNameSelectionPanel.prompt");
        String help_text = wizardi18n.getString(
                "oraclerac10g.dbNameSelectionPanel.cli.help");
        String invalidSelection = wizardi18n.getString(
                "oraclerac10g.dbNameSelectionPanel.invalidselection");

        String selectedDbName = null;
        String defaultDbName;

        // Get the discovered DB_NAME from the dataservice
	String[] baseClusterNodelist = (String[])wizardModel.getWizardValue(
	    Util.BASE_CLUSTER_NODELIST);
        String propNames[] = new String[] { Util.DB_NAME };
        Vector vDbName = new Vector();

        HashMap helperData = new HashMap();
        String selectedCrsHome = (String) wizardModel.getWizardValue(
	    Util.SEL_CRS_HOME);

        String selZoneCluster = CliUtil.getZoneClusterName(wizardModel);

	helperData.put(Util.SEL_ZONE_CLUSTER, selZoneCluster);
        helperData.put(Util.CRS_HOME, selectedCrsHome);
	RACModel racModel = RACInvokerCreator.getRACModel();

        for (int i = 0; i < baseClusterNodelist.length; i++) {
            String split_str[] = baseClusterNodelist[i].split(Util.COLON);

            HashMap map = racModel.discoverPossibilities(null, split_str[0], propNames,
		helperData);

            Vector tmpVec = (Vector) map.get(Util.DB_NAME);

            if (tmpVec == null) {
                tmpVec = new Vector();
            }

            int size = tmpVec.size();

            for (int j = 0; j < size; j++) {
                String dbname = (String)tmpVec.elementAt(j);
		if (dbname != null && dbname.trim().length() > 0) {
		    if (!vDbName.contains(dbname)) {
			vDbName.add(dbname);
		    }
		}
            }
        } // finish discovery on all nodes

        HashMap custom = new HashMap();
        custom.put(enterKey, enterText);

        String optionsArr[] = (String[]) vDbName.toArray(new String[0]);

        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        String choice = no;

        while (choice.equalsIgnoreCase(no)) {
            TTYDisplay.printTitle(title);

            // get the default selection
            defaultDbName = (String) wizardModel.getWizardValue(
                    Util.SEL_DB_NAME);

            if ((optionsArr != null) && (optionsArr.length > 0)) {
                List dbNameList = TTYDisplay.getScrollingOption(desc,
                        optionsArr, custom, defaultDbName, help_text, true);

                if (dbNameList == null) {
                    this.cancelDirection = true;

                    return;
                }

                TTYDisplay.setNavEnabled(true);
                this.cancelDirection = false;

                String dbNameArr[] = (String[]) dbNameList.toArray(
                        new String[0]);
                selectedDbName = dbNameArr[0];
            } else {
                selectedDbName = enterText;
            }

            TTYDisplay.clear(1);

            if (selectedDbName.equals(enterText)) {
                selectedDbName = "";

                while (selectedDbName.trim().length() == 0) {
                    selectedDbName = TTYDisplay.promptForEntry(dbname_prompt);

                    if (selectedDbName.equals(back)) {
                        this.cancelDirection = true;

                        return;
                    }
                }
            }

            // validate the DB NAME
            ErrorValue retVal = validateInput(selectedDbName, baseClusterNodelist,
		selZoneCluster);

            if (!retVal.getReturnValue().booleanValue()) {
                TTYDisplay.printError(retVal.getErrorString());
                TTYDisplay.clear(2);
                TTYDisplay.promptForEntry(continue_txt);
                TTYDisplay.clear(2);
                choice = no;

                continue;
            }

            choice = yes;

            // Store the Values in the WizardModel
            wizardModel.selectCurrWizardContext();
            wizardModel.setWizardValue(Util.SEL_DB_NAME, selectedDbName);
        }
    }

    private ErrorValue validateInput(String dbName, String baseClusterNodelist[],
	String selZoneCluster) {
        String propName[] = { Util.DB_NAME };
        HashMap userInput = new HashMap();
        userInput.put(Util.DB_NAME, dbName);

        ErrorValue err = new ErrorValue();
        err.setReturnVal(Boolean.TRUE);

        StringBuffer errStr = new StringBuffer();
        int err_count = 0;
	RACModel racModel = RACInvokerCreator.getRACModel();
	HashMap helperData = new HashMap();
	helperData.put(Util.SEL_ZONE_CLUSTER, selZoneCluster);

        for (int i = 0; i < baseClusterNodelist.length; i++) {
	    String[] splitStr = baseClusterNodelist[i].split(Util.COLON);
	    ErrorValue errVal = racModel.validateInput(
		null, splitStr[0], propName, userInput, helperData);

            if (!errVal.getReturnValue().booleanValue()) {

                // error
                err_count++;
                errStr = new StringBuffer(errVal.getErrorString());
            }
        }

        if (err_count == baseClusterNodelist.length) {

            // failed on all nodes
            err.setReturnVal(Boolean.FALSE);
            err.setErrorString(errStr.toString());
        }

        return err;
    }
}
