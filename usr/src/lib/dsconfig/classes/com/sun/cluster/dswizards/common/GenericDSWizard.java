/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)GenericDSWizard.java 1.20     08/05/20 SMI"
 */

package com.sun.cluster.dswizards.common;

// J2SE
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

//J2EE
import javax.servlet.http.HttpSession;

//JMX
import javax.management.MBeanServerConnection;

// JATO
import com.iplanet.jato.ModelManager;
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.RequestManager;
import com.iplanet.jato.model.Model;
import com.iplanet.jato.util.HtmlUtil;
import com.iplanet.jato.view.View;
import com.iplanet.jato.view.ViewBean;

// CMASS Classes
import com.sun.cluster.agent.node.NodeMBean;
import com.sun.cluster.agent.dataservices.utils.InfrastructureMBean;
import com.sun.cluster.agent.dataservices.utils.Util;

// DS Wizard Common Classes
import com.sun.cluster.dswizards.common.WizardI18N;

// SPM Classes
import com.sun.cluster.spm.common.SpmUtil;

import com.sun.cluster.common.MBeanModel;
import com.sun.cluster.common.JmxServerConnection;

// Lockhart
import com.sun.web.ui.common.CCI18N;
import com.sun.web.ui.model.CCWizardModel;
import com.sun.web.ui.model.CCWizardModelInterface;
import com.sun.web.ui.model.wizard.WizardEvent;
import com.sun.web.ui.model.wizard.WizardInterface;
import com.sun.web.ui.model.wizard.WizardInterfaceExt;

public abstract class GenericDSWizard implements WizardInterface,
    WizardInterfaceExt {


    private static final String STEP_NAME = "STEP_NAME";
    private static final String STEP_TITLE = "STEP_TITLE";
    private static final String STEP_TEXT = "STEP_TEXT";
    private static final String STEP_INSTRUCTION = "STEP_INSTRUCTION";
    private static final String STEP_HELP_PREFIX = "STEP_HELP_PREFIX";
    private static final String STEP_CANCEL_MSG = "STEP_CANCEL_MSG";
    private static final String PLACEHOLDER_TEXT = "PLACEHOLDER_TEXT";
    private static WizardI18N wizardi18n = null;

    private static final String COLON = ":";

    public static final String WIZARD_MODE = "WizardMode";
    public static final String WIZARD_TITLE = "WizardTitle";
    public static final String WIZARD_PARAM = "WizardName";
    public static final String WIZARD_MODEL_PARAM = "WizardModelName";
    public static final String WIZARD_LAUNCHED = "WizardLaunched";

    public static final String COMMAND_EXECUTED = "CommandExecuted";
    public static final String COMMAND_LIST = "CommandList";
    public static final String DS_WIZARD_TYPE = "DSWizardType";
    public static final String DEFAULT_PLACEHOLDER =
        "dswizards.branching.placeholder";

    public static final String CRITICAL_VALUE_CHANGED = "CriticalValueChanged";
    public static final String WIZARD_MODEL = "WIZ_MODEL";
    public static final String COMMON_XML_PATH =
        "/usr/cluster/lib/ds/common/gui/";

    protected DSConfigWizardModel wizardModel = null;
    protected HashMap wizardPageClasses = null;
    protected WizardFlow wizFlow = null;

    private String wizardTitle = null;
    private CCI18N cci18n = null;

    public GenericDSWizard(RequestContext requestContext) {
        wizardPageClasses = new HashMap();
        getWizardModel(requestContext);

        wizardTitle = getParameter(WIZARD_TITLE, requestContext);

        String wizardMode = getParameter(WIZARD_MODE, requestContext);
        wizardModel.setWizardValue(WIZARD_MODE, wizardMode);

        cci18n = new CCI18N(requestContext.getRequest(),
                requestContext.getResponse(), getResourceBundle(), null, null);
    }

    /*
     * This must be called immediatly after the call to constructor to set the
     * wizard flow
     */

    public void setFlow(String xmlFile) {
        this.wizFlow = new WizardFlow(xmlFile);
    }

    /**
     * Returns the name of this WizardInterface implementation.
     */
    public abstract String getName();

    /**
     * This method register the class name of a page view used in this wizard.
     * It is important to register each pages in the order they will be
     * displayed by the wizard.
     *
     * @param  pageClass  the class implementing the page view.
     */
    public void registerPage(Class pageClass) {

        try {
            wizardPageClasses.put(Integer.toString(wizardPageClasses.size()),
                pageClass);
        } catch (Exception e) { /* Should never happen */
        }
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Implementation of WizardInterface
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    public String getTitle() {
        return wizardTitle;
    }

    public Model getPageModel(String pageId) {
        return wizardModel;
    }

    public Class getPageClass(String pageId) {
        return (Class) wizardPageClasses.get(pageId);
    }

    public String getPageName(String pageId) {
        return (String) getField(pageId, STEP_NAME);
    }

    public String getFirstPageId() {
        return "0";
    }

    public String getNextPageId(String pageId) {

        try {
            int current = Integer.parseInt(pageId);
            Integer nextPageId = new Integer(wizFlow.getNextPageId(current,
                        wizardModel));

            return nextPageId.toString();
        } catch (Exception e) {
            return null;
        }
    }

    public String getResourceBundle() {
        return "com.sun.cluster.dswizards.common.WizardResource";
    }

    public String getStepTitle(String pageId) {
        int current = 0;

        try {
            current = Integer.parseInt(pageId);
        } catch (Exception e) {
            return null;
        }

        String text = (String) getField(pageId, STEP_TITLE);
        int subLevel = wizFlow.isSubstep(current, wizardModel);

        if (subLevel > 0) {
            text = Integer.toString(subLevel) + COLON + text;
        }

        return text;
    }

    public String[] getFuturePages(String currentPageId) {
        int pageId = 0;

        try {
            pageId = Integer.parseInt(currentPageId);
        } catch (Exception e) { /* Should never happen */
        }

        // The following is not a public interface. CCWizardModel is not
        // exposed.
        ModelManager mm = RequestManager.getRequestContext().getModelManager();
        CCWizardModel internalModel = (CCWizardModel) mm.getModel(
                com.sun.web.ui.model.CCWizardModel.class);

        return wizFlow.getFuturePageIDs(pageId, wizardModel, internalModel);
    }

    public String[] getFutureSteps(String currentPageId) {
        String futurePages[] = getFuturePages(currentPageId);
        String futureSteps[] = new String[futurePages.length];

        for (int i = 0; i < futurePages.length; i++) {
            futureSteps[i] = getStepText(futurePages[i]);
        }

        return futureSteps;
    }

    public String getStepInstruction(String pageId) {
        return (String) getField(pageId, STEP_INSTRUCTION);
    }

    public String getStepText(String pageId) {
        return (String) getField(pageId, STEP_TEXT);
    }

    public String[] getStepHelp(String pageId) {
        String prefix = (String) getField(pageId, STEP_HELP_PREFIX);
        int nbParagraphs = 0;

        try {
            nbParagraphs = Integer.parseInt(cci18n.getMessage(prefix));
        } catch (Exception e) {
            return null;
        }

        String help[] = new String[nbParagraphs];

        for (int i = 1; i <= nbParagraphs; i++) {
            help[i - 1] = prefix + "." + i;
        }

        return help;
    }

    public boolean isFinishPageId(String pageId) {
        int Id = 0;

        try {
            Id = Integer.parseInt(pageId);
        } catch (Exception e) {
            return false;
        }

        return wizFlow.isFinishPageId(Id, wizardModel);
    }

    public boolean isSubstep(String pageId) {
        int current = 0;

        try {
            current = Integer.parseInt(pageId);
        } catch (Exception e) {
            return false;
        }

        return (wizFlow.isSubstep(current, wizardModel) != 0);
    }

    public boolean hasPreviousPageId(String pageId) {
        return !pageId.equals(getFirstPageId());
    }

    public String getCancelPrompt(String pageId) {

        if (isFinishPageId(pageId)) {
            return null;
        } else {
            return "dswizards.cancel.prompt";
        }
    }

    public boolean warnOnRevisitStep() {
        return false;
    }

    public String toString() {
        return getName();
    }

    public boolean done(String wizardName) {
        wizardModel.selectDefaultContext();

        return true;
    }

    public boolean nextStep(WizardEvent wizardEvent) {
        String curPageId = wizardEvent.getPageId();
        boolean ret = validate(wizardEvent);
        ModelManager mm = wizardEvent.getRequestContext().getModelManager();
        CCWizardModel internalModel = (CCWizardModel) mm.getModel(
                com.sun.web.ui.model.CCWizardModel.class);

        if (ret) {
            String next = getNextPageId(curPageId);
            String pagesTraversed[] = internalModel.getVisitedPages();
            boolean dup = false;
            int len = (pagesTraversed == null) ? 0 : pagesTraversed.length;

            for (int i = 0; i < len; i++) {

                if (next.equals(pagesTraversed[i])) {
                    dup = true;

                    break;
                }
            }

            if (dup) {
                internalModel.setValue(
                    CCWizardModelInterface.WIZARD_PAGING_HREF, next);
                ret = internalModel.gotoRequest(wizardEvent.getView());
                ret = internalModel.previousRequest(wizardEvent.getView());

                return ret;
            }

            return ret;
        } else {
            return ret;
        }
    }

    public boolean previousStep(WizardEvent wizardEvent) {
        boolean ret = previous(wizardEvent);

        return (!wizardEvent.getPageId().equals(getFirstPageId()) && ret);
    }

    public boolean gotoStep(WizardEvent wizardEvent) {
        return true;
    }

    public boolean finishStep(WizardEvent wizardEvent) {
        return validate(wizardEvent);
    }

    public boolean cancelStep(WizardEvent wizardEvent) {

        // Need to store the values in the state file
        // Get the WizardState Reader from the session
        // Get the current wizardModel from the session
        try {
            HttpSession session = RequestManager.getRequestContext()
                .getRequest().getSession();
            DSConfigWizardModel wizardModel = (DSConfigWizardModel) session
                .getAttribute(WIZARD_MODEL);
            RequestContext rq = RequestManager.getRequestContext();
            MBeanServerConnection conn = JmxServerConnection.getInstance()
                .getConnection(rq, SpmUtil.getClusterEndpoint());

            if (wizardModel != null) {
                wizardModel.writeToStateFile(conn);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    public void closeStep(WizardEvent wizardEvent) {
        return;
    }

    public boolean helpTab(WizardEvent wizardEvent) {
        return true;
    }

    public String getPlaceholderText(String pageId) {
        int current = 0;

        try {
            current = Integer.parseInt(pageId);
        } catch (Exception e) {
            return null;
        }

        if (wizFlow.isBranchingStep(current, wizardModel)) {
            String placeHolder = (String) getField(pageId, PLACEHOLDER_TEXT);

            if ((placeHolder == null) || (placeHolder.length() == 0)) {
                placeHolder = DEFAULT_PLACEHOLDER;
            }

            return placeHolder;
        } else {
            return null;
        }
    }

    public boolean canBeStepLink(String pageId) {
        int Id = 0;

        try {
            Id = Integer.parseInt(pageId);
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    public String getResultsPageId(String pageId) {
        return null;
    }


    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Utility methods
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    /**
     * Retrieve the wizard model from framework's {@link ModelManager} based on
     * model's instance name.
     *
     * @param  modelInstanceName  the unique name of the instance name used to
     * identify the model
     * @param  requestContext  the current context
     *
     * @return  the {@link DSConfigWizardModel} used for the wizard or <code>
     * null</code> if the model was not found in the model manager
     */
    public static DSConfigWizardModel getGenericWizardModel(
        String modelInstanceName, RequestContext requestContext) {

        ModelManager modelManager = requestContext.getModelManager();

        try {
            Model model = modelManager.getModel(DSConfigWizardModel.class,
                    modelInstanceName, true, true);

            return (DSConfigWizardModel) model;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Get or generates a unique name for the wizard instance. The name is
     * stored in the page session for future references.
     *
     * @param  view  the {@link ViewBean} calling the wizard; this method use
     * the page session of the provided view
     * @param  wizardClassName  the fully qualified class name of the used
     * wizard
     *
     * @return  a session unique name for the wizard instance
     */
    public static String getWizardName(ViewBean view, String wizardClassName) {
        String wizardName = (String) view.getPageSessionAttribute(WIZARD_PARAM);

        if (wizardName == null) {
            wizardName = wizardClassName + "_" + HtmlUtil.getUniqueValue();
            view.setPageSessionAttribute(WIZARD_PARAM, wizardName);
        }

        return wizardName;
    }

    /**
     * Get or generates a unique name for the wizard model instance. The name is
     * stored in the page session for future references.
     *
     * @param  view  the {@link ViewBean} calling the wizard; this method use
     * the page session of the provided view
     * @param  modelClassName  the fully qualified class name of the used wizard
     * model
     *
     * @return  a session unique name for the wizard model instance
     */
    public static String getWizardModelName(ViewBean view,
        String modelClassName) {
        String wizardModelName = (String) view.getPageSessionAttribute(
                WIZARD_MODEL_PARAM);

        if (wizardModelName == null) {
            wizardModelName = modelClassName + "_" + HtmlUtil.getUniqueValue();
            view.setPageSessionAttribute(WIZARD_MODEL_PARAM, wizardModelName);
        }

        return wizardModelName;
    }

    public static WizardI18N getWizardI18N() {

        if (wizardi18n == null) {
            wizardi18n = new WizardI18N(
                    "com.sun.cluster.dswizards.common.WizardResource");
        }

        return wizardi18n;
    }

    public static String getNodeEndpoint(String nodename) {

        try {
            NodeMBean mbean = (NodeMBean) MBeanModel.getMBeanProxy(
                    RequestManager.getRequestContext(), Util.DOMAIN_NODE,
                    SpmUtil.getClusterEndpoint(), NodeMBean.class, nodename,
                    false);

            return mbean.getPublicInetAddress();
        } catch (Exception e) {
            return nodename;
        }
    }


    public static InfrastructureMBean getInfrastructureMBean() {
        InfrastructureMBean mbean = null;

        try {
            mbean = (InfrastructureMBean) MBeanModel.getMBeanProxy(
                    RequestManager.getRequestContext(), Util.DOMAIN,
                    SpmUtil.getClusterEndpoint(), InfrastructureMBean.class,
                    null, false);
        } catch (Exception e) {
        }

        return mbean;

    }

    public static InfrastructureMBean getInfrastructureMBean(String nodeName) {
        InfrastructureMBean mbean = null;

        try {
            String split_str[] = nodeName.split(Util.COLON);
            mbean = (InfrastructureMBean) MBeanModel.getMBeanProxy(
                    RequestManager.getRequestContext(), Util.DOMAIN,
                    getNodeEndpoint(split_str[0]), InfrastructureMBean.class,
                    null, false);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mbean;
    }


    private Object getField(String pageId, String fieldName) {

        try {
            Class pageClass = (Class) wizardPageClasses.get(pageId);
            Field field = pageClass.getField(fieldName);

            return field.get(null);
        } catch (Exception e) {
            return null;
        }
    }

    private boolean validate(WizardEvent wizardEvent) {

        try {
            Class pageClass = (Class) wizardPageClasses.get(wizardEvent
                    .getPageId());
            Method validate = pageClass.getMethod("validate",
                    new Class[] { WizardEvent.class });

            View pageView = wizardEvent.getView();
            Boolean result = (Boolean) validate.invoke(pageView,
                    new Object[] { wizardEvent });

            return result.booleanValue();
        } catch (Exception e) {
            return true;
        }
    }

    private boolean previous(WizardEvent wizardEvent) {

        try {
            Class pageClass = (Class) wizardPageClasses.get(wizardEvent
                    .getPageId());
            Method previous = pageClass.getMethod("previous",
                    new Class[] { WizardEvent.class });

            View pageView = wizardEvent.getView();
            Boolean result = (Boolean) previous.invoke(pageView,
                    new Object[] { wizardEvent });

            return result.booleanValue();
        } catch (Exception e) {
            return true;
        }
    }

    private String getParameter(String name, RequestContext requestContext) {
        Map parameterMap = requestContext.getRequest().getParameterMap();
        String values[] = (String[]) parameterMap.get(name);

        return (values == null) ? null : values[0];
    }

    private void getWizardModel(RequestContext requestContext) {
        String modelInstanceName = getParameter(WIZARD_MODEL_PARAM,
                requestContext);

        ModelManager mm = requestContext.getModelManager();
        wizardModel = (DSConfigWizardModel) mm.getModel(
                DSConfigWizardModel.class, modelInstanceName, true, true);
        wizardModel.selectCurrWizardContext();
    }

    public boolean stepTab(WizardEvent wizardEvent) {
        return true;
    }
}
