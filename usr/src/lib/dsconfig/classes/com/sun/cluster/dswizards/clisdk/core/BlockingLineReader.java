/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)BlockingLineReader.java 1.8     08/05/20 SMI"
 */

package com.sun.cluster.dswizards.clisdk.core;

import java.io.*;


/**
 * This class facilitates a line read from an input stream. It is a private
 * interface used by TTYDisplay.java in the SDK.
 */
class BlockingLineReader implements LineReader {

    private BufferedReader bReader = null;
    private String defval = null;

    /*
     * Constructor, create a line reader object.
     *
     * @param in an InputStream object
     * @param defval the default input string
     */
    public BlockingLineReader(BufferedReader bReader, String defval) {
        this.bReader = bReader;
        this.defval = defval;
    }

    /*
     * This method reads a line from the underlying input stream.
     * The read will wait till the Enter key stroke happens.
     *
     * @return  String object represents the line read from the stream
     */
    public String readLine() {

        String result = this.defval;

        try {
            result = this.bReader.readLine();
        } catch (IOException ie) {
            TTYDisplay.writeToLog("IOException in read query value.\n" +
                ie.toString());
        }

        return (result);
    }

    /*
     * This method reads a masked line from the underlying input stream.
     * The read will wait till the Enter key stroke happens.
     *
     * @return  String object represents the masked line read from the stream
     */
    public String readMaskedLine() {

        String result = this.defval;

        // create the masking thread and start it.
        MaskingThread mt = new MaskingThread(' ', 1);
        Thread mask = new Thread(mt);
        mask.start();

        try {
            result = this.bReader.readLine();
        } catch (IOException ie) {
            TTYDisplay.writeToLog("IOException in read query value.\n" +
                ie.toString());
        } finally {

            // stop masking
            mt.stopMasking();
        }

        // return the masked line, for example, a password.
        return (result);
    }
}
