/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ApacheWizardReviewPanel.java 1.15     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.apache;

// J2SE


// CLI SDK
import com.sun.cluster.agent.dataservices.apache.Apache;
import com.sun.cluster.agent.dataservices.apache.ApacheMBean;
import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.utils.DataServicesUtil;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.rgm.ResourceMBean;
import com.sun.cluster.common.TrustedMBeanModel;
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;

// Wizard Common
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;

import java.io.File;

import java.util.HashMap;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.management.remote.JMXConnector;


/**
 * Panel to Review Apache Configuration
 */
public class ApacheWizardReviewPanel extends WizardLeaf {
    private static String KEY = "KEY";
    private static String NAME = "NAME";
    private static String DESC = "DESC";
    private static String VALUE = "VALUE";

    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N messages; // Message Bundle
    private ApacheMBean mBean = null; // Reference to MBean
    private JMXConnector localConnection = null; // Reference to JMXConnector

    /**
     * Creates a new instance of ApacheWizardReviewPanel
     */
    public ApacheWizardReviewPanel() {
        super();
    }

    /**
     * Creates an ApacheWizardReview Panel with the given name and tree manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public ApacheWizardReviewPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates a ApacheWizardReview Panel with the given name and associates it
     * with the WizardModel, WizardFlow and i18n reference
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     * @param  messages  WizardI18N
     */
    public ApacheWizardReviewPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N messages) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;

        // Get the I18N instance
        this.messages = messages;

        localConnection = getConnection(Util.getClusterEndpoint());
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {

        // Prefetch Strings
        String title = messages.getString("apache.reviewPanel.title");
        String desc = messages.getString("apache.reviewPanel.desc");
        String help = messages.getString("apache.reviewPanel.help.1");
        String subTitle = messages.getString("apache.reviewPanel.subTitle");
        String confFile = messages.getString("apache.reviewPanel.confFile");
        String confFileDesc = messages.getString(
                "apache.reviewPanel.confFile.desc");
        String docRoot = messages.getString("apache.reviewPanel.docRoot");
        String docRootDesc = messages.getString(
                "apache.reviewPanel.docRoot.desc");
        String serverRoot = messages.getString("apache.reviewPanel.serverRoot");
        String serverRootDesc = messages.getString(
                "apache.reviewPanel.serverRoot.desc");
        String hostName = messages.getString("apache.reviewPanel.hostname");
        String hostNameDesc = messages.getString(
                "apache.reviewPanel.hostname.desc");
        String apachePort = messages.getString("apache.reviewPanel.port");
        String apachePortDesc = messages.getString(
                "apache.reviewPanel.port.desc");
        String scTableTitle = messages.getString("hasp.summarypanel.desc2");
        String sctext = messages.getString(
                "ttydisplay.menu.suncClusterObjects");
        String pidFile = messages.getString("apache.reviewPanel.pidfile");
        String pidFileDesc = messages.getString(
                "apache.reviewPanel.pidfile.desc");

        String propNameText = messages.getString("hasp.reviewpanel.propname");
        String propDescText = messages.getString("hasp.reviewpanel.propdesc");
        String currValText = messages.getString("hasp.reviewpanel.currval");


        // String for SunCluster objects screen
        String heading1 = messages.getString(
                "hasp.create.filesystem.review.heading1");
        String heading2 = messages.getString(
                "hasp.create.filesystem.review.heading2");
        String dchar = messages.getString("ttydisplay.d.char");
        String done = messages.getString("ttydisplay.menu.done");
        String portQueryText = messages.getString(
                "apache.reviewPanel.portQuery");
        String continueText = messages.getString("cliwizards.continue");

        Vector reviewPanel = new Vector();
        HashMap userInputs = new HashMap();
        HashMap screenData = new HashMap();
        HashMap tableRow = new HashMap();

        // Get the Apache Mountpoint
        String mountPoint = (String) wizardModel.getWizardValue(
                Util.APACHE_MOUNT);

        // Get the Apache Mode
        String apacheMode = (String) wizardModel.getWizardValue(
                Util.APACHE_MODE);

        String nodeList[] = (String[]) wizardModel.getWizardValue(
                Util.RG_NODELIST);

        String nodeEndPoint = (String) wizardModel.getWizardValue(
                Util.NODE_TO_CONNECT);
        JMXConnector connector = getConnection(nodeEndPoint);
        mBean = ApacheWizardCreator.getApacheMBeanOnNode(connector);

        /*
         * We First show the Apache Configuration information
         * and then show the Sun Cluster Objects
         */
        int rowNo;

        while (true) {
            TTYDisplay.clearScreen();
            TTYDisplay.clear(1);
            TTYDisplay.printTitle(title);
            TTYDisplay.clear(1);
            TTYDisplay.pageText(desc);
            TTYDisplay.setNavEnabled(true);
            this.cancelDirection = false;
            reviewPanel.removeAllElements();
            screenData.clear();
            rowNo = 0;

            String apacheHostName[] = (String[]) wizardModel.getWizardValue(
                    Util.APACHE_LISTEN);

            // If APACHE_LISTEN is null, then User had selected new LH or SH
            // Set APACHE_LISTEN from LH or SH wizard
            if (apacheHostName == null) {
                apacheHostName = (String[]) wizardModel.getWizardValue(
                        Util.HOSTNAMELIST);
                wizardModel.setWizardValue(Util.APACHE_LISTEN, apacheHostName);
            }

            StringBuffer apacheHostNameBuf = new StringBuffer();

            for (int i = 0; i < apacheHostName.length; i++) {
                apacheHostNameBuf = apacheHostNameBuf.append(apacheHostName[i]);

                if (i < (apacheHostName.length - 1)) {
                    apacheHostNameBuf.append(",");
                }
            }

            // First Row - Apache Conf file
            String apacheConfFile = (String) wizardModel.getWizardValue(
                    Util.APACHE_CONF_FILE);
            reviewPanel.add(confFile);

            String confFileName = apacheConfFile.substring(apacheConfFile
                    .lastIndexOf('/') + 1, apacheConfFile.length());
            String apacheRes = (String) wizardModel.getWizardValue(
                    Util.APACHE_RESOURCE_NAME);

            if (apacheRes == null) {
                String disResourcePropertyName[] = {
                        Util.APACHE_RESOURCE_NAME
                    };
                HashMap discoveredMap = mBean.discoverPossibilities(
                        disResourcePropertyName, apacheHostNameBuf.toString());
                apacheRes = (String) discoveredMap.get(
                        Util.APACHE_RESOURCE_NAME);
                wizardModel.setWizardValue(Util.APACHE_RESOURCE_NAME,
                    apacheRes);
            }

            StringBuffer newConfFilePath = new StringBuffer();
            newConfFilePath.append(mountPoint);
            newConfFilePath.append(Util.SLASH);
            newConfFilePath.append(apacheRes);
            newConfFilePath.append("/etc/apache/");
            newConfFilePath.append(confFileName);
            reviewPanel.add(newConfFilePath.toString());
            tableRow = new HashMap();
            tableRow.put(KEY, Util.APACHE_CONF_FILE);
            tableRow.put(NAME, confFile);
            tableRow.put(DESC, confFileDesc);
            tableRow.put(VALUE, newConfFilePath.toString());
            screenData.put(String.valueOf(++rowNo), tableRow);

            // Second Row - Document Root
            // Remove " and spaces from doc root if any
            String apacheDocRoot = (String) wizardModel.getWizardValue(
                    Util.APACHE_DOC);
            reviewPanel.add(docRoot);
            wizardModel.setWizardValue(Util.APACHE_DOC, apacheDocRoot);
            apacheDocRoot = apacheDocRoot.replace('\"', ' ');
            apacheDocRoot = apacheDocRoot.trim();

            String docRootStr;

            if (apacheDocRoot.indexOf(mountPoint, 0) == 0) {
                docRootStr = apacheDocRoot;
            } else {
                docRootStr = mountPoint + Util.SLASH + apacheRes +
                    apacheDocRoot;
            }

            reviewPanel.add(docRootStr);
            tableRow = new HashMap();
            tableRow.put(KEY, Util.APACHE_DOC);
            tableRow.put(NAME, docRoot);
            tableRow.put(DESC, docRootDesc);
            tableRow.put(VALUE, docRootStr);
            screenData.put(String.valueOf(++rowNo), tableRow);

            // Third Row - Server Root
            String apacheServerRoot = (String) wizardModel.getWizardValue(
                    Util.APACHE_HOME);
            apacheServerRoot = apacheServerRoot.replace('\"', ' ');
            apacheServerRoot = apacheServerRoot.trim();

            // If Server Root is null, discover it from the
            // conf file
            String confFilePath = (String) wizardModel.getWizardValue(
                    Util.APACHE_CONF_FILE);

            if (apacheServerRoot == null) {

                // The Conf File is Parsed for ServerRoot
                HashMap discoveredValue = mBean.discoverPossibilities(
                        new String[] { Util.APACHE_HOME }, confFilePath);
                String serverRootArr[] = (String[]) discoveredValue.get(
                        Util.APACHE_HOME);
                wizardModel.setWizardValue(Util.APACHE_HOME, serverRootArr[0]);
                apacheServerRoot = serverRootArr[0];
            }

            reviewPanel.add(serverRoot);
            reviewPanel.add(apacheServerRoot);
            tableRow = new HashMap();
            tableRow.put(KEY, Util.APACHE_HOME);
            tableRow.put(NAME, serverRoot);
            tableRow.put(DESC, serverRootDesc);
            tableRow.put(VALUE, apacheServerRoot);
            screenData.put(String.valueOf(++rowNo), tableRow);

            // Fourth Row - when Apache version is 2
            String apacheVer = (String) wizardModel.getWizardValue(
                    Util.APACHE_VERSION);

            if ((apacheVer != null) && apacheVer.startsWith(Util.TWO)) {
                HashMap discoveredValue = mBean.discoverPossibilities(
                        new String[] { Util.PIDFILE_DIR }, confFilePath);
                String disDir[] = (String[]) discoveredValue.get(
                        Util.PIDFILE_DIR);
                File fp = new File(disDir[0]);
                String pidFileDir = fp.getParent();

                if (pidFileDir != null) {
                    StringBuffer pidFileBuf = new StringBuffer(pidFileDir);
                    pidFileBuf.append(Util.SLASH);
                    pidFileBuf.append(apacheRes);
                    pidFileBuf.append(".pid");
                    reviewPanel.add(pidFile);

                    String pidFileStr = pidFileBuf.toString();
                    reviewPanel.add(pidFileStr);
                    wizardModel.setWizardValue(Util.PIDFILE, pidFileStr);
                    tableRow = new HashMap();
                    tableRow.put(KEY, Util.PIDFILE_DIR);
                    tableRow.put(NAME, pidFile);
                    tableRow.put(DESC, pidFileDesc);
                    tableRow.put(VALUE, pidFileStr);
                    screenData.put(String.valueOf(++rowNo), tableRow);
                }
            }

            // Fourth row/Fifth Row - Hostname
            String hostNameBufStr = apacheHostNameBuf.toString();
            reviewPanel.add(hostName);
            reviewPanel.add(hostNameBufStr);
            tableRow = new HashMap();
            tableRow.put(KEY, Util.APACHE_LISTEN);
            tableRow.put(NAME, hostName);
            tableRow.put(DESC, hostNameDesc);
            tableRow.put(VALUE, hostNameBufStr);
            screenData.put(String.valueOf(++rowNo), tableRow);

            // Fifth row/Sixth Row - Port
            String port[] = (String[]) wizardModel.getWizardValue(
                    Util.APACHE_PORT);

            if (port == null) {

                // The Conf File is Parsed for ServerRoot
                HashMap discoveredValue = mBean.discoverPossibilities(
                        new String[] { Util.APACHE_PORT }, confFilePath);
                port = (String[]) discoveredValue.get(Util.APACHE_PORT);
                wizardModel.setWizardValue(Util.APACHE_PORT, port);
            }

            reviewPanel.add(apachePort);

            StringBuffer apachePortsBuf = new StringBuffer();

            for (int i = 0; i < port.length; i++) {
                apachePortsBuf = apachePortsBuf.append(port[i]);

                if (i < (port.length - 1)) {
                    apachePortsBuf.append(",");
                }
            }

            String portsStr = apachePortsBuf.toString();
            reviewPanel.add(portsStr);
            tableRow = new HashMap();
            tableRow.put(KEY, Util.APACHE_PORT);
            tableRow.put(NAME, apachePort);
            tableRow.put(DESC, apachePortDesc);
            tableRow.put(VALUE, portsStr);
            screenData.put(String.valueOf(++rowNo), tableRow);

            HashMap customTags = new HashMap();
            customTags.put(dchar, done);

            String subheadings[] = { heading1, heading2 };

            TTYDisplay.clear(1);

            // Display the Table
            String option = TTYDisplay.create2DTable(subTitle, subheadings,
                    reviewPanel, customTags, help);

            // Process Options
            if (option.equals("<")) {
                this.cancelDirection = true;

                return;
            }

            HashMap selectedRow = (HashMap) screenData.get(option);
            String itemName = null;

            if (selectedRow != null) {
                itemName = (String) selectedRow.get(KEY);
            }

            if (itemName != null) {

                if (itemName.equals(Util.APACHE_PORT)) {
                    TTYDisplay.clear(2);

                    String newPort = TTYDisplay.promptForEntry(portQueryText);

                    if ((newPort != null) && (newPort.length() > 0)) {

                        // Validate newPort
                        boolean portsOk = true;

                        for (int i = 0; i < apacheHostName.length; i++) {
                            userInputs.put(Util.PORT_LIST, newPort);
                            userInputs.put(Util.HOST_NAME, apacheHostName[i]);

                            ErrorValue errorVal = mBean.validateInput(
                                    new String[] { Util.APACHE_PORT },
                                    userInputs, null);

                            if (!errorVal.getReturnValue().booleanValue()) {
                                portsOk = false;
                                TTYDisplay.printError(messages.getString(
                                        errorVal.getErrorString()));
                                TTYDisplay.clear(1);
                                TTYDisplay.promptForEntry(continueText);

                                break;
                            }
                        }

                        if (portsOk) {
                            wizardModel.setWizardValue(Util.APACHE_PORT,
                                newPort.split(","));
                        }
                    }
                } else {
                    TTYDisplay.pageText(propNameText +
                        (String) selectedRow.get(NAME));
                    TTYDisplay.pageText(propDescText +
                        (String) selectedRow.get(DESC));
                    TTYDisplay.pageText(currValText +
                        (String) selectedRow.get(VALUE));
                    TTYDisplay.clear(1);
                    TTYDisplay.promptForEntry(continueText);

                }
            }

            // Sun Cluster Objects
            if (option.equals(dchar)) {
                return;
            }
        }
    }
}
