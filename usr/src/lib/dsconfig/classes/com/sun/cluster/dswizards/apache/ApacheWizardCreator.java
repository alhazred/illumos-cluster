/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ApacheWizardCreator.java 1.17     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.apache;

// JAVA

// Cacao
import com.sun.cacao.agent.JmxClient;

// CMASS
import com.sun.cluster.agent.dataservices.apache.Apache;
import com.sun.cluster.agent.dataservices.apache.ApacheMBean;
import com.sun.cluster.agent.dataservices.hastorageplus.HAStoragePlusMBean;
import com.sun.cluster.agent.dataservices.logicalhost.LogicalHost;
import com.sun.cluster.agent.dataservices.sharedaddress.SharedAddress;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.rgm.ResourceMBean;
import com.sun.cluster.common.TrustedMBeanModel;

// CLI SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardComposite;
import com.sun.cluster.dswizards.clisdk.core.WizardCreator;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.WizardFlow;

// DS Wizards
import com.sun.cluster.dswizards.hastorageplus.HASPCreateFileSystemPanel;
import com.sun.cluster.dswizards.hastorageplus.HASPCreateFileSystemReviewPanel;
import com.sun.cluster.dswizards.hastorageplus.HASPFileSystemSelectionPanel;
import com.sun.cluster.dswizards.hastorageplus.HASPWizardConstants;
import com.sun.cluster.dswizards.hastorageplus.HASPWizardCreator;
import com.sun.cluster.dswizards.logicalhost.HostnameEntryPanel;
import com.sun.cluster.dswizards.logicalhost.IPMPGroupEntryPanel;
import com.sun.cluster.dswizards.logicalhost.LogicalHostWizardConstants;
import com.sun.cluster.dswizards.logicalhost.LogicalHostWizardCreator;
import com.sun.cluster.dswizards.sharedaddress.SharedAddressWizardCreator;

import java.text.MessageFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.management.AttributeValueExp;
import javax.management.Query;
import javax.management.QueryExp;
import javax.management.remote.JMXConnector;


/**
 * This class holds the Wizard Panel Tree information for ApacheWizard. This
 * class creates the ApacheWizard from the State file and WizardFlow File.
 */
public class ApacheWizardCreator extends WizardCreator {

    private static final String FLOW_XML =
        ApacheWizardConstants.APACHE_FLOW_XML;
    private static final String STATE_FILE =
        ApacheWizardConstants.APACHE_STATE_FILE;

    private JMXConnector localConnection = null;

    /**
     * Default Constructor Creates Apache Wizard from the State File. The Wizard
     * is intialized to a particular state using the State file. The Wizard flow
     * is governed by the Flow XML.
     */
    public ApacheWizardCreator() {
        super(STATE_FILE, FLOW_XML);
    }

    /**
     * Creates the actual Panel Tree Structure for the Apache Wizard
     */
    protected void createClientTree() {

        // Get the wizard root
        WizardComposite apacheWizardRoot = getRoot();

        // Create the Individual Panels of the Wizard
        ApacheWizardIntroPanel introPanel = new ApacheWizardIntroPanel(
                ApacheWizardConstants.INTROPANEL, wizardModel, wizardFlow,
                wizardi18n);

        ApacheWizardModePanel modePanel = new ApacheWizardModePanel(
                ApacheWizardConstants.MODEPANEL, wizardModel, wizardFlow,
                wizardi18n);

        ApacheWizardNodePanel nodePanel = new ApacheWizardNodePanel(
                ApacheWizardConstants.NODEPANEL, wizardModel, wizardFlow,
                wizardi18n);

        ApacheWizardConfFilePanel confFilePanel = new ApacheWizardConfFilePanel(
                ApacheWizardConstants.CONFPANEL, wizardModel, wizardFlow,
                wizardi18n);

        ApacheWizardDocRootPanel docRootPanel = new ApacheWizardDocRootPanel(
                ApacheWizardConstants.DOCROOTPANEL, wizardModel, wizardFlow,
                wizardi18n);

        ApacheWizardFilesystemPanel fsPanel = new ApacheWizardFilesystemPanel(
                ApacheWizardConstants.FSPANEL, wizardModel, wizardFlow,
                wizardi18n);

        // LogicalHostWizardPanels and SharedAddress Panels
        HostnameEntryPanel hostnameEntryPanel = new HostnameEntryPanel(
                ApacheWizardConstants.HOSTPANEL + WizardFlow.HYPHEN +
                LogicalHostWizardConstants.HOSTNAMEENTRY, wizardModel,
                wizardFlow, wizardi18n);
        IPMPGroupEntryPanel ipmpGroupEntryPanel = new IPMPGroupEntryPanel(
                ApacheWizardConstants.HOSTPANEL + WizardFlow.HYPHEN +
                LogicalHostWizardConstants.IPMPGROUPENTRY, wizardModel,
                wizardFlow, wizardi18n);

        // HA Storage Wizard Panels
        HASPFileSystemSelectionPanel fileSystemSelectionPanel =
            new HASPFileSystemSelectionPanel(ApacheWizardConstants.FSPANEL +
                WizardFlow.HYPHEN + HASPWizardConstants.PANEL_3, wizardModel,
                wizardFlow, wizardi18n);

        HASPCreateFileSystemPanel createFileSystemPanel =
            new HASPCreateFileSystemPanel(ApacheWizardConstants.FSPANEL +
                WizardFlow.HYPHEN + HASPWizardConstants.PANEL_4, wizardModel,
                wizardFlow, wizardi18n);

        HASPCreateFileSystemReviewPanel createFileSystemReviewPanel =
            new HASPCreateFileSystemReviewPanel(ApacheWizardConstants.FSPANEL +
                WizardFlow.HYPHEN + HASPWizardConstants.PANEL_5, wizardModel,
                wizardFlow, wizardi18n);

        ApacheWizardMountPointPanel mpPanel = new ApacheWizardMountPointPanel(
                ApacheWizardConstants.MPPANEL, wizardModel, wizardFlow,
                wizardi18n);

        ApacheWizardHostSelectionPanel hostPanel =
            new ApacheWizardHostSelectionPanel(ApacheWizardConstants.HOSTPANEL,
                wizardModel, wizardFlow, wizardi18n);

        ApacheWizardReviewPanel reviewPanel = new ApacheWizardReviewPanel(
                ApacheWizardConstants.REVIEWPANEL, wizardModel, wizardFlow,
                wizardi18n);

        ApacheWizardObjectsReviewPanel ObjectsReviewPanel =
            new ApacheWizardObjectsReviewPanel(
                ApacheWizardConstants.OBJECTSREVIEWPANEL, wizardModel,
                wizardFlow, wizardi18n);

        ApacheWizardSummaryPanel summaryPanel = new ApacheWizardSummaryPanel(
                ApacheWizardConstants.SUMMARYPANEL, wizardModel, wizardFlow,
                wizardi18n);

        ApacheWizardResultPanel resultPanel = new ApacheWizardResultPanel(
                ApacheWizardConstants.RESULTSPANEL, wizardModel, wizardFlow,
                wizardi18n);

        // Each Panel is associated with a Name, the wizardModel from
        // the WizardCreator and the WizardFlow Structure.
        apacheWizardRoot.addChild(introPanel);
        apacheWizardRoot.addChild(modePanel);
        apacheWizardRoot.addChild(nodePanel);
        apacheWizardRoot.addChild(confFilePanel);
        apacheWizardRoot.addChild(docRootPanel);
        apacheWizardRoot.addChild(fsPanel);
        apacheWizardRoot.addChild(fileSystemSelectionPanel);
        apacheWizardRoot.addChild(createFileSystemPanel);
        apacheWizardRoot.addChild(createFileSystemReviewPanel);
        apacheWizardRoot.addChild(mpPanel);
        apacheWizardRoot.addChild(hostPanel);
        apacheWizardRoot.addChild(hostnameEntryPanel);
        apacheWizardRoot.addChild(ipmpGroupEntryPanel);
        apacheWizardRoot.addChild(reviewPanel);
        apacheWizardRoot.addChild(ObjectsReviewPanel);
        apacheWizardRoot.addChild(summaryPanel);
        apacheWizardRoot.addChild(resultPanel);
    }

    /**
     * Get the Handle to the Apache MBean on a specified node
     *
     * @return  Handle to the Apache MBean on a specified node
     */
    public static ApacheMBean getApacheMBeanOnNode(JMXConnector connector) {

        // Only if there is no existing reference
        // Get Handler to ApacheMBean
        ApacheMBean mbeanOnNode = null;

        try {
            mbeanOnNode = (ApacheMBean) TrustedMBeanModel.getMBeanProxy(
                    Util.DOMAIN, connector, ApacheMBean.class, null, false);
        } catch (Exception e) {

            // Report Error that the user has to install
            // The Dataservice Package or upgrade it
            String errorMsg = pkgErrorString;
            MessageFormat msgFmt = new MessageFormat(errorMsg);
            errorMsg = msgFmt.format(
                    new String[] { ApacheWizardConstants.APACHE_PKG_NAME });
            TTYDisplay.printError(errorMsg);
            TTYDisplay.promptForEntry(wizardExitPrompt);
            System.exit(1);
        }

        return mbeanOnNode;
    }

    /**
     * This method would fill the wizardModel with the required values if the
     * user had selected a mount point created by the HA storage wizard
     */
    public static void fillWizardModel_forNewMountPoint(
        CliDSConfigWizardModel wizCxt, JMXConnector connection) {

        QueryExp query = null;

        // Get the APACHE_MODE from wizardModel
        String apacheMode = (String) wizCxt.getWizardValue(Util.APACHE_MODE);

        // Set Apache RG to null as HA storage Resource is new
        wizCxt.setWizardValue(Util.APACHE_RESOURCE_GROUP, null);

        // Set auto-generated name for HA storage Resource
        String strToUse = (String) wizCxt.getWizardValue(Util.APACHE_MOUNT);

        // Get the Resource Name from the HASPMBean
        HashMap helper = new HashMap();
        helper.put(Util.HASP_ALL_FILESYSTEMS, new String[] { strToUse });

        HAStoragePlusMBean mBean = HASPWizardCreator.getHAStoragePlusMBean(
                connection);
        String properties[] = { Util.RS_NAME };
        HashMap discoveredMap = mBean.discoverPossibilities(properties, helper);
        String rsName = (String) discoveredMap.get(Util.RS_NAME);
        wizCxt.setWizardValue(
            ApacheWizardConstants.STORAGE_RESOURCE_NAME, rsName);

        // Set new MOUNT to true, so that user can edit
        // the same in ReviewPanel
        wizCxt.setWizardValue(ApacheWizardConstants.NEWMOUNT, "true");

        // Discover hostnames based on apache mode
        if (apacheMode.equals(Apache.FAILOVER)) {

            // Set wizardType for the common panels
            wizCxt.setWizardValue(LogicalHostWizardConstants.WIZARDTYPE,
                LogicalHostWizardCreator.WIZARDTYPE);

            // Discover all the LogicalHost resources in the cluster
            query = Query.match(new AttributeValueExp("Type"),
                    Query.value(LogicalHost.RTNAME + "*"));

            try {
                Set logicalHostResources = TrustedMBeanModel.getInstanceNames(
                        connection, ResourceMBean.class, query);
                wizCxt.setWizardValue(Util.LH_RESOURCE_NAME,
                    logicalHostResources);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        // Apache mode is Scalable
        else {

            // Set wizardType for the common panels
            wizCxt.setWizardValue(LogicalHostWizardConstants.WIZARDTYPE,
                SharedAddressWizardCreator.WIZARDTYPE);

            // Discover all the SharedAddress resources in the cluster
            query = Query.match(new AttributeValueExp("Type"),
                    Query.value(SharedAddress.RTNAME + "*"));

            try {
                Set sharedAddressResources = TrustedMBeanModel.getInstanceNames(
                        connection, ResourceMBean.class, query);
                wizCxt.setWizardValue(Util.SH_RESOURCE_NAME,
                    sharedAddressResources);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * This method would fill the wizardModel with the required values if the
     * user had selected a HA mount point already under HA storage Resource
     */
    public static void fillWizardModel_forExistingMountPoint(
        String storageResource, CliDSConfigWizardModel wizCxt,
        JMXConnector connection) {

        QueryExp query = null;

        // Get the APACHE_MODE from wizardModel
        String apacheMode = (String) wizCxt.getWizardValue(Util.APACHE_MODE);

        /*
         * Since DocRoot is governed by storage resource, we take that resource
         * as reference resource and discover the Apache Resource Group and
         * LogicalHost (or) SharedAddress resources
         */

        // Get the Apache's ResourceGroup from the storage resource
        String apacheResourceGroup = null;

        try {
            ResourceMBean storageMBean = (ResourceMBean) TrustedMBeanModel
                .getMBeanProxy(connection, ResourceMBean.class, storageResource,
                    true);

            String storageRsType = storageMBean.getType(); 
            if (storageRsType.indexOf(Util.HASP_RTNAME) != -1 ||
                    apacheMode.equals(Apache.FAILOVER)) {
                apacheResourceGroup = storageMBean.getResourceGroupName();
                wizCxt.setWizardValue(Util.APACHE_RESOURCE_GROUP,
                    apacheResourceGroup);
            } else if (storageRsType.indexOf(Util.QFS_RTNAME) != -1) {
                wizCxt.setWizardValue(
                    ApacheWizardConstants.APACHE_QFS_RS_SELECTED, 
                    new Boolean(true));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        /*
         * Based on the Apache Mode, discover the LogicalHostnames in the
         * ApacheResource Group or all the SharedAddress resources
         * in the cluster.
         */
        // ApacheMode is Failover
        if (apacheMode.equals(Apache.FAILOVER)) {

            // Set wizardType for the common panels
            wizCxt.setWizardValue(LogicalHostWizardConstants.WIZARDTYPE,
                LogicalHostWizardCreator.WIZARDTYPE);

            // Get a list of LogicalHost Resources in this ResourceGroup
            query = Query.and(Query.eq(
                        new AttributeValueExp("ResourceGroupName"),
                        Query.value(apacheResourceGroup)),
                    Query.match(new AttributeValueExp("Type"),
                        Query.value(LogicalHost.RTNAME + "*")));

            try {
                Set logicalHostResources = TrustedMBeanModel.getInstanceNames(
                        connection, ResourceMBean.class, query);
                wizCxt.setWizardValue(Util.LH_RESOURCE_NAME,
                    logicalHostResources);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        // Apache Mode is Scalable
        else {

            // Set wizardType for the common panels
            wizCxt.setWizardValue(LogicalHostWizardConstants.WIZARDTYPE,
                SharedAddressWizardCreator.WIZARDTYPE);

            // Get the SharedAddress resources configured in the cluster
            query = Query.match(new AttributeValueExp("Type"),
                    Query.value(SharedAddress.RTNAME + "*"));

            try {
                Set sharedAddressResources = TrustedMBeanModel.getInstanceNames(
                        connection, ResourceMBean.class, query);
                wizCxt.setWizardValue(Util.SH_RESOURCE_NAME,
                    sharedAddressResources);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Main function of the Wizards
     */
    public static void main(String args[]) {

        // Start the Apache Wizard
        ApacheWizardCreator apacheWiz = new ApacheWizardCreator();
    }
}
