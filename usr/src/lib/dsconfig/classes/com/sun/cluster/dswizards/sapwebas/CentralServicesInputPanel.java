/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)CentralServicesInputPanel.java 1.13     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.sapwebas;

// J2SE
import java.util.HashMap;
import java.util.List;

// CMAS
import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.sapwebas.SapWebasMBean;
import com.sun.cluster.agent.dataservices.utils.DataServicesUtil;
import com.sun.cluster.agent.dataservices.utils.Util;

// CLI Wizard SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;

// Wizard Common
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;


public class CentralServicesInputPanel extends WizardLeaf {

    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N wizardi18n; // Message Bundle


    /**
     * Creates a new instance of CentralServicesInputPanel
     */
    public CentralServicesInputPanel() {
        super();
    }

    /**
     * Creates a CentralServicesInputPanel Panel with the 
     * given name and tree manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public CentralServicesInputPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    public CentralServicesInputPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;

        // Get the I18N instance
        this.wizardi18n = wizardi18n;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {
        String title = wizardi18n.getString(
                "sap.centralServicesInputPanel.title");
        String desc = wizardi18n.getString(
                "sap.centralServicesInputPanel.desc");
        String help_text = wizardi18n.getString(
                "sap.centralServicesInputPanel.help.cli");
        String yes = wizardi18n.getString("cliwizards.yes");
        String no = wizardi18n.getString("cliwizards.no");
        String continue_txt = wizardi18n.getString("cliwizards.continue");


        String enq_query_txt = wizardi18n.getString(
                "sap.centralServicesInputPanel.query.enq");
        String user_query_txt = wizardi18n.getString(
                "sap.centralServicesInputPanel.query.user");
        String defUserName;
        String sapUsername;
        String enq_i_no;
        String scs_i_no;
        String defEnqNo;
        String defScsNo;
        String sArr[] = new String[1];
        String query_txt;

        DataServicesUtil.clearScreen();

        // Title
        TTYDisplay.clear(1);
        TTYDisplay.printTitle(title);
        TTYDisplay.clear(1);
        TTYDisplay.pageText(desc);
        TTYDisplay.clear(2);

        TTYDisplay.setNavEnabled(true);
        this.cancelDirection = false;

        HashMap custom = new HashMap();
        String menuKey = wizardi18n.getString("sap.sidPanel.custom.key");
        String menuText = wizardi18n.getString("sap.sidPanel.custom.keyText");
        custom.put(menuKey, menuText);

        ErrorValue retValue;

        String option;
        TTYDisplay.setNavEnabled(true);

        String sapSid = (String) wizardModel.getWizardValue(Util.SAP_SID);

        do { // Ask for SAP User Name
            TTYDisplay.setNavEnabled(true);
            defUserName = SapWebasWizardCreator.discoverOption(wizardModel,
                    Util.SAP_USER, Util.SAP_USER, sapSid);

            if (defUserName != null) {
                sapUsername = TTYDisplay.promptForEntry(user_query_txt,
                        defUserName, help_text);
            } else {
                sapUsername = TTYDisplay.promptForEntry(user_query_txt,
                        help_text);
            }

            if (sapUsername.equalsIgnoreCase(
                        wizardi18n.getString("ttydisplay.back.char"))) {
                this.cancelDirection = true;

                return;
            }

            while (sapUsername.length() <= 0) {
                TTYDisplay.printError(wizardi18n.getString(
                        "sap.centralServicesInputPanel.user.invalid"));
                sapUsername = TTYDisplay.promptForEntry(user_query_txt);
            }

            TTYDisplay.clear(1);
            TTYDisplay.setNavEnabled(false);
            retValue = new ErrorValue();
            retValue = SapWebasWizardCreator.validateData(Util.SAP_USER,
                    sapUsername, wizardModel);

            if (retValue.getReturnValue().booleanValue()) {
                wizardModel.setWizardValue(Util.SAP_USER, sapUsername);
            } else {
                TTYDisplay.printWarning(wizardi18n.getString(
                        "sap.user.invalid"));
                TTYDisplay.printMoreText(retValue.getErrorString());
                TTYDisplay.clear(1);

                String confirm = TTYDisplay.getConfirmation(wizardi18n
                        .getString("cliwizards.validation.override"), no, yes);

                if (confirm.equalsIgnoreCase(yes)) {
                    wizardModel.selectCurrWizardContext();
                    wizardModel.setWizardValue(Util.SAP_USER, sapUsername);

                    break;
                }
            }
        } while (!retValue.getReturnValue().booleanValue());

        do { // Ask for Enqueue Server Location
            user_query_txt = wizardi18n.getString(
                    "sap.enqServer.location.query");
            TTYDisplay.setNavEnabled(true);

            String defaultName = SapWebasWizardCreator.discoverOption(
                    wizardModel, Util.ENQ_LOC, Util.ENQ_SERVER_LOC, sapSid);
            String enteredValue = null;

            if (defaultName != null) {
                enteredValue = TTYDisplay.promptForEntry(user_query_txt,
                        defaultName, help_text);
            } else {
                enteredValue = TTYDisplay.promptForEntry(user_query_txt,
                        help_text);
            }

            if (enteredValue.equalsIgnoreCase(
                        wizardi18n.getString("ttydisplay.back.char"))) {
                this.cancelDirection = true;

                return;
            }

            while (enteredValue.length() <= 0) {
                TTYDisplay.printError(wizardi18n.getString(
                        "sap.centralServicesInputPanel.user.invalid"));
                enteredValue = TTYDisplay.promptForEntry(user_query_txt);
            }

            sArr[0] = enteredValue;
            TTYDisplay.clear(1);
            TTYDisplay.setNavEnabled(false);
            retValue = new ErrorValue();
            retValue = SapWebasWizardCreator.validateData(Util.ENQ_LOC,
                    enteredValue, wizardModel);

            if (retValue.getReturnValue().booleanValue()) {
                wizardModel.setWizardValue(Util.ENQ_SERVER_LOC, enteredValue);
            } else {
                TTYDisplay.printWarning(wizardi18n.getString(
                        "sap.enqServer.invalid"));
                TTYDisplay.printMoreText(retValue.getErrorString());
                TTYDisplay.clear(1);

                String confirm = TTYDisplay.getConfirmation(wizardi18n
                        .getString("cliwizards.validation.override"), no, yes);

                if (confirm.equalsIgnoreCase(yes)) {
                    wizardModel.selectCurrWizardContext();
                    wizardModel.setWizardValue(Util.ENQ_SERVER_LOC,
                        enteredValue);

                    break;
                }
            }
        } while (!retValue.getReturnValue().booleanValue());


        String optionsArr[] = SapWebasWizardCreator.discoverOptions(wizardModel,
                Util.ENQ_I_NO, SapWebasConstants.DISCOVERD_ENQ_I_NO, sapSid);

        do { // Ask for Enq Instance Number
            defEnqNo = (String) wizardModel.getWizardValue(
                    Util.ENQ_INSTANCE_NUM);
            TTYDisplay.setNavEnabled(true);
            query_txt = wizardi18n.getString("sap.enq_i_no.query");

            if (optionsArr != null) {
                List selList = TTYDisplay.getScrollingOption(query_txt,
                        optionsArr, custom, defEnqNo, help_text, true);

                if (selList == null) {
                    this.cancelDirection = true;

                    return;
                }

                TTYDisplay.clear(1);

                String selectList[] = (String[]) selList.toArray(new String[0]);
                enq_i_no = selectList[0];
            } else {
                enq_i_no = menuText;
            }

            if (enq_i_no.equals(menuText)) {
                TTYDisplay.setNavEnabled(false);

                if (defEnqNo != null) {
                    enq_i_no = TTYDisplay.promptForEntry(enq_query_txt,
                            defEnqNo, help_text);
                } else {
                    enq_i_no = TTYDisplay.promptForEntry(enq_query_txt,
                            help_text);
                }
            }

            while (enq_i_no.length() <= 0) {
                TTYDisplay.printError(wizardi18n.getString(
                        "sap.centralServicesInputPanel.enq.invalid"));
                enq_i_no = TTYDisplay.promptForEntry(enq_query_txt);
            }

            sArr[0] = enq_i_no;
            TTYDisplay.clear(1);
            TTYDisplay.setNavEnabled(false);
            retValue = new ErrorValue();
            retValue = SapWebasWizardCreator.validateData(Util.ENQ_I_NO,
                    enq_i_no, wizardModel);

            if (retValue.getReturnValue().booleanValue()) {
                wizardModel.setWizardValue(Util.ENQ_INSTANCE_NUM, enq_i_no);
            } else {
                TTYDisplay.printWarning(wizardi18n.getString(
                        "sap.enq_i_no.invalid"));
                TTYDisplay.printMoreText(retValue.getErrorString());
                TTYDisplay.clear(1);

                String confirm = TTYDisplay.getConfirmation(wizardi18n
                        .getString("cliwizards.validation.override"), no, yes);

                if (confirm.equalsIgnoreCase(yes)) {
                    wizardModel.selectCurrWizardContext();
                    wizardModel.setWizardValue(Util.ENQ_INSTANCE_NUM, enq_i_no);

                    break;
                }

            }
        } while (!retValue.getReturnValue().booleanValue());

        optionsArr = SapWebasWizardCreator.discoverOptions(wizardModel,
                Util.ENQ_PROF, SapWebasConstants.DISCOVERD_ENQ_PROFILES,
                sapSid);

        do { // Ask for Enq Profile Location
            defEnqNo = (String) wizardModel.getWizardValue(
                    Util.ENQ_SERVER_PROFILE_LOC);
            TTYDisplay.setNavEnabled(true);
            query_txt = wizardi18n.getString("sap.enq_prof.select");
            enq_query_txt = wizardi18n.getString("sap.enq_prof.query");

            String enq_prof;

            if (optionsArr != null) {
                List selList = TTYDisplay.getScrollingOption(query_txt,
                        optionsArr, custom, defEnqNo, help_text, true);

                if (selList == null) {
                    this.cancelDirection = true;

                    return;
                }

                TTYDisplay.clear(1);

                String selectList[] = (String[]) selList.toArray(new String[0]);
                enq_prof = selectList[0];
            } else {
                enq_prof = menuText;
            }

            if (enq_prof.equals(menuText)) {
                TTYDisplay.setNavEnabled(false);

                if (defEnqNo != null) {
                    enq_prof = TTYDisplay.promptForEntry(enq_query_txt,
                            defEnqNo, help_text);
                } else {
                    enq_prof = TTYDisplay.promptForEntry(enq_query_txt,
                            help_text);
                }
            }

            while (enq_prof.length() <= 0) {
                TTYDisplay.printError(wizardi18n.getString(
                        "sap.enq_prof.invalid"));
                enq_prof = TTYDisplay.promptForEntry(enq_query_txt);
            }

            sArr[0] = enq_prof;
            TTYDisplay.clear(1);
            TTYDisplay.setNavEnabled(false);
            retValue = new ErrorValue();
            retValue = SapWebasWizardCreator.validateData(Util.ENQ_PROF,
                    enq_prof, wizardModel);

            if (retValue.getReturnValue().booleanValue()) {
                wizardModel.setWizardValue(Util.ENQ_SERVER_PROFILE_LOC,
                    enq_prof);
            } else {
                TTYDisplay.printWarning(wizardi18n.getString(
                        "sap.enq_prof.invalid"));
                TTYDisplay.printMoreText(retValue.getErrorString());
                TTYDisplay.clear(1);

                String confirm = TTYDisplay.getConfirmation(wizardi18n
                        .getString("cliwizards.validation.override"), no, yes);

                if (confirm.equalsIgnoreCase(yes)) {
                    wizardModel.selectCurrWizardContext();
                    wizardModel.setWizardValue(Util.ENQ_SERVER_PROFILE_LOC,
                        enq_prof);

                    break;
                }

            }
        } while (!retValue.getReturnValue().booleanValue());

        this.cancelDirection = false;

    }

}
