/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)HASPIntroPanel.java 1.10     08/12/10 SMI"
 */

package com.sun.cluster.dswizards.hastorageplus;

// CLI Wizard SDK
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;

// Wizard Common
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;


/**
 * Introduction page to the Wizard
 */
public class HASPIntroPanel extends WizardLeaf {

    private String panelName; // Panel Name
    private CliDSConfigWizardModel wizardModel; // WizardModel
    private WizardI18N wizardi18n; // Message Bundle


    /**
     * Creates a new instance of HASPIntroPanel
     */
    public HASPIntroPanel() {
        super();
    }

    /**
     * Creates a HASPInto Panel with the given name and tree manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     */
    public HASPIntroPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates a HASPInto Panel with the given name and associates it with the
     * WizardModel and WizardFlow
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     * @param  wizardFlow  WizardFlow
     */
    public HASPIntroPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;

        // Get the I18N instance
        this.wizardi18n = wizardi18n;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent
     * #consoleInteraction
     */
    public void consoleInteraction() {
        String title = wizardi18n.getString("hasp.introPanel.title");
        String desc = wizardi18n.getString("hasp.introPanel.desc");

        String continue_txt = wizardi18n.getString("cliwizards.continue");

        // Title
        TTYDisplay.printTitle(title);

        TTYDisplay.pageText(desc);
        TTYDisplay.clear(2);
        TTYDisplay.promptForEntry(continue_txt);
        TTYDisplay.clear(2);
    }
}
