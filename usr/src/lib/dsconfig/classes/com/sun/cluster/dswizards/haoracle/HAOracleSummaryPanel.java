/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)HAOracleSummaryPanel.java 1.20     08/12/10 SMI"
 */


package com.sun.cluster.dswizards.haoracle;

import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.dataservices.haoracle.HAOracleMBean;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.rgm.property.ResourceProperty;
import com.sun.cluster.agent.rgm.property.ResourcePropertyString;
import com.sun.cluster.dswizards.clisdk.core.TTYDisplay;
import com.sun.cluster.dswizards.clisdk.core.WizardLeaf;
import com.sun.cluster.dswizards.clisdk.core.WizardTreeManager;
import com.sun.cluster.dswizards.common.CliDSConfigWizardModel;
import com.sun.cluster.dswizards.common.ResourcePropertyUtil;
import com.sun.cluster.dswizards.common.WizardFlow;
import com.sun.cluster.dswizards.common.WizardI18N;
import com.sun.cluster.dswizards.hastorageplus.HASPWizardConstants;
import com.sun.cluster.dswizards.logicalhost.LogicalHostWizardConstants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.management.remote.JMXConnector;


/**
 * The Panel Class that gets a Hostname or list of Hostnames as input from the
 * User.
 */
public class HAOracleSummaryPanel extends WizardLeaf {

    private static String NAME = "NAME";
    private static String VALUE = "VALUE";
    private static String DESCRIPTION = "DESCRIPTION";
    private static String COLON_DESC = "COLON_DESC";


    private String panelName; // Panel Name
    private WizardI18N wizardi18n; // Message Bundle
    private CliDSConfigWizardModel wizardModel; // WizardModel

    boolean bConfigServer = false;
    boolean bConfigListener = false;

    private HAOracleMBean mbean = null;

    /**
     * Creates a new instance of HAOracleSummaryPanel
     */
    public HAOracleSummaryPanel() {
        super();
    }

    /**
     * Creates a HostnameEntry Panel with the given name and manager
     *
     * @param  name  Panel Name
     * @param  manager  Manager for the Panel
     * @param  flow  Handler to the WizardFlow
     */
    public HAOracleSummaryPanel(String name, WizardTreeManager manager) {
        super(name, manager);
        this.panelName = name;
    }

    /**
     * Creates a HostnameEntry Panel with the given name and Wizard state
     *
     * @param  name  Name of the Panel
     * @param  model  CliDSConfigWizardModel
     */
    public HAOracleSummaryPanel(String name, CliDSConfigWizardModel model,
        WizardFlow wizardFlow, WizardI18N wizardi18n) {
        super(model, name, wizardFlow);
        this.panelName = name;
        this.wizardModel = model;
        this.wizardi18n = wizardi18n;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.WizardComponent#skip
     */
    public boolean skip() {
        return false;
    }

    /**
     * @see  com.sun.cluster.dswizards.clisdk.core.
     *		WizardComponent#consoleInteraction
     */
    public void consoleInteraction() {
        String title = wizardi18n.getString("haoracle.summarypanel.title");
        String description = wizardi18n.getString(
                "haoracle.summarypanel.desc1");
        String back = wizardi18n.getString("ttydisplay.back.char");
        String yes = wizardi18n.getString("cliwizards.yes");
        String no = wizardi18n.getString("cliwizards.no");

        String server_rs_name = wizardi18n.getString(
                "haoracle.summarypanel.server.rsname.name");
        String server_rs_desc = wizardi18n.getString(
                "haoracle.summarypanel.server.rsname.desc");
        String server_rs_colon = wizardi18n.getString(
                "haoracle.summarypanel.server.rsname.colon");

        String listener_rs_name = wizardi18n.getString(
                "haoracle.summarypanel.listener.rsname.name");
        String listener_rs_desc = wizardi18n.getString(
                "haoracle.summarypanel.listener.rsname.desc");
        String listener_rs_colon = wizardi18n.getString(
                "haoracle.summarypanel.listener.rsname.colon");

        String rg_name = wizardi18n.getString(
                "haoracle.summarypanel.rgname.name");
        String rg_desc = wizardi18n.getString(
                "haoracle.summarypanel.rgname.desc");
        String rg_colon = wizardi18n.getString(
                "haoracle.summarypanel.rgname.colon");

        String nodelist_name = wizardi18n.getString(
                "haoracle.summarypanel.nodelist.name");
        String nodelist_desc = wizardi18n.getString(
                "haoracle.summarypanel.nodelist.desc");
        String nodelist_colon = wizardi18n.getString(
                "haoracle.summarypanel.nodelist.colon");

        String hasprs_name = wizardi18n.getString(
                "haoracle.summaryPanel.hasp.rsname.name");
        String hasprs_desc = wizardi18n.getString(
                "haoracle.summaryPanel.hasp.rsname.desc");
        String hasprs_colon = wizardi18n.getString(
                "haoracle.summaryPanel.hasp.rsname.colon");

        String fs_name = wizardi18n.getString(
                "haoracle.summarypanel.filesytem.name");
        String fs_desc = wizardi18n.getString(
                "haoracle.summarypanel.filesystem.desc");
        String fs_colon = wizardi18n.getString(
                "haoracle.summarypanel.filesytem.colon");

        String globaldev_name = wizardi18n.getString(
                "haoracle.summarypanel.devicegroup.name");
        String globaldev_desc = wizardi18n.getString(
                "haoracle.summarypanel.devicegroup.desc");
        String globaldev_colon = wizardi18n.getString(
                "haoracle.summarypanel.devicegroup.colon");

        String lhrs_name = wizardi18n.getString(
                "haoracle.summaryPanel.lh.rsname.name");
        String lhrs_desc = wizardi18n.getString(
                "haoracle.summaryPanel.lh.rsname.desc");
        String lhrs_colon = wizardi18n.getString(
                "haoracle.summaryPanel.lh.rsname.colon");

        String netif_name = wizardi18n.getString(
                "haoracle.summaryPanel.netiflist.name");
        String netif_desc = wizardi18n.getString(
                "haoracle.summaryPanel.netiflist.desc");
        String netif_colon = wizardi18n.getString(
                "haoracle.summaryPanel.netiflist.colon");

        String hostname_name = wizardi18n.getString(
                "haoracle.summaryPanel.hostnamelist.name");
        String hostname_desc = wizardi18n.getString(
                "haoracle.summaryPanel.hostnamelist.desc");
        String hostname_colon = wizardi18n.getString(
                "haoracle.summaryPanel.hostnamelist.colon");

        String table_title = wizardi18n.getString(
                "haoracle.summarypanel.desc2");
        String heading1 = wizardi18n.getString(
                "haoracle.summarypanel.heading1");
        String heading2 = wizardi18n.getString(
                "haoracle.summarypanel.heading2");
        String c_char = wizardi18n.getString("ttydisplay.c.char");
        String create_txt = wizardi18n.getString(
                "haoracle.summarypanel.menu.create");
        String q_char = wizardi18n.getString("ttydisplay.q.char");
        String quit_txt = wizardi18n.getString("ttydisplay.menu.quit");
        String panel_desc = wizardi18n.getString(
                "haoracle.summarypanel.description");
        String gencmd_txt = wizardi18n.getString(
                "haoracle.summarypanel.generateCommand");
        String exit_txt = wizardi18n.getString("cliwizards.confirmExit");
        String help_text = wizardi18n.getString("hasp.summarypanel.cli.help");
        String continue_txt = wizardi18n.getString("cliwizards.continue");

        String preference = (String) wizardModel.getWizardValue(
                HAOracleWizardConstants.HAORACLE_PREF);

        if (preference.equals(HAOracleWizardConstants.SERVER_ONLY)) {
            bConfigServer = true;
        } else if (preference.equals(
                    HAOracleWizardConstants.SERVER_AND_LISTENER)) {
            bConfigServer = true;
            bConfigListener = true;
        } else if (preference.equals(HAOracleWizardConstants.LISTENER_ONLY)) {
            bConfigListener = true;
        }

        // Title
        TTYDisplay.printTitle(title);

        // Review panel table
        Vector summaryPanel = new Vector();

        wizardModel.selectCurrWizardContext();


        String serverRsName = null;
        String listenerRsName = null;

        HashMap tableMap = new HashMap();
        HashMap selMap = null;
        int row_count = 1;

        if (bConfigServer) {
            serverRsName = (String) wizardModel.getWizardValue(
                    HAOracleWizardConstants.SERVER_RS_NAME);
            summaryPanel.add(server_rs_name);
            summaryPanel.add(serverRsName);
            selMap = new HashMap();
            selMap.put(NAME, server_rs_name);
            selMap.put(VALUE, serverRsName);
            selMap.put(DESCRIPTION, server_rs_desc);
            selMap.put(COLON_DESC, server_rs_colon);
            tableMap.put(Integer.toString(row_count++), selMap);
        }

        if (bConfigListener) {
            listenerRsName = (String) wizardModel.getWizardValue(
                    HAOracleWizardConstants.LISTENER_RS_NAME);
            summaryPanel.add(listener_rs_name);
            summaryPanel.add(listenerRsName);
            selMap = new HashMap();
            selMap.put(NAME, listener_rs_name);
            selMap.put(VALUE, listenerRsName);
            selMap.put(DESCRIPTION, listener_rs_desc);
            selMap.put(COLON_DESC, listener_rs_colon);
            tableMap.put(Integer.toString(row_count++), selMap);
        }

        String rgName = (String) wizardModel.getWizardValue(Util.RG_NAME);
        summaryPanel.add(rg_name);
        summaryPanel.add(rgName);
        selMap = new HashMap();
        selMap.put(NAME, rg_name);
        selMap.put(VALUE, rgName);
        selMap.put(DESCRIPTION, rg_desc);
        selMap.put(COLON_DESC, rg_colon);
        tableMap.put(Integer.toString(row_count++), selMap);

        String nodeList[] = (String[]) wizardModel.getWizardValue(
                Util.RG_NODELIST);
        StringBuffer nodeNames = convertArrayToStringBuffer(nodeList);
        summaryPanel.add(nodelist_name);
        summaryPanel.add(nodeNames.toString());
        selMap = new HashMap();
        selMap.put(NAME, nodelist_name);
        selMap.put(VALUE, nodeNames.toString());
        selMap.put(DESCRIPTION, nodelist_desc);
        selMap.put(COLON_DESC, nodelist_colon);
        tableMap.put(Integer.toString(row_count++), selMap);

        String haspRsName = (String) wizardModel.getWizardValue(
                HAOracleWizardConstants.HASP_RS_NAME);

        Vector existingHASPRS = (Vector) wizardModel.getWizardValue(
                HAOracleWizardConstants.SEL_HASP_RS);

        Boolean BNewHASP = (Boolean) wizardModel.getWizardValue(
                HAOracleWizardConstants.NEW_HASP_RS);

        String allHaspRsName = haspRsName;

        if (BNewHASP.booleanValue() && (existingHASPRS != null)) {

            // if new HASP resource
            StringBuffer haspStringBuffer = convertArrayToStringBuffer(
                    (String[]) existingHASPRS.toArray(new String[0]));

            if (haspStringBuffer.length() > 0) {
                haspStringBuffer.append(Util.COMMA);
            }

            haspStringBuffer.append(allHaspRsName);
            allHaspRsName = haspStringBuffer.toString();
        }

        summaryPanel.add(hasprs_name);
        summaryPanel.add(allHaspRsName);
        selMap = new HashMap();
        selMap.put(NAME, hasprs_name);
        selMap.put(VALUE, allHaspRsName);
        selMap.put(DESCRIPTION, hasprs_desc);
        selMap.put(COLON_DESC, hasprs_colon);
        tableMap.put(Integer.toString(row_count++), selMap);

        String fsMountPoints[] = (String[]) wizardModel.getWizardValue(
                HAOracleWizardConstants.NEW_FS_DEFINED);
        StringBuffer mountpoints = convertArrayToStringBuffer(fsMountPoints);

        if ((fsMountPoints != null) && (fsMountPoints.length > 0)) {
            summaryPanel.add(fs_name);
            summaryPanel.add(mountpoints.toString());
            selMap = new HashMap();
            selMap.put(NAME, fs_name);
            selMap.put(VALUE, mountpoints.toString());
            selMap.put(DESCRIPTION, fs_desc);
            selMap.put(COLON_DESC, fs_colon);
            tableMap.put(Integer.toString(row_count++), selMap);
        }

        String globalDevicePaths[] = (String[]) wizardModel.getWizardValue(
                HAOracleWizardConstants.NEW_DEVICES_DEFINED);
        StringBuffer devpaths = convertArrayToStringBuffer(globalDevicePaths);

        if ((globalDevicePaths != null) && (globalDevicePaths.length > 0)) {
            summaryPanel.add(globaldev_name);
            summaryPanel.add(devpaths.toString());
            selMap = new HashMap();
            selMap.put(NAME, globaldev_name);
            selMap.put(VALUE, devpaths.toString());
            selMap.put(DESCRIPTION, globaldev_desc);
            selMap.put(COLON_DESC, globaldev_colon);
            tableMap.put(Integer.toString(row_count++), selMap);
        }

        String lhRsName = (String) wizardModel.getWizardValue(
                HAOracleWizardConstants.LH_RS_NAME);
        Vector existingLHRS = (Vector) wizardModel.getWizardValue(
                HAOracleWizardConstants.SEL_LH_RS);

        Boolean BNewLH = (Boolean) wizardModel.getWizardValue(
                HAOracleWizardConstants.NEW_LH_RS);

        String allLhRsName = lhRsName;
        summaryPanel.add(lhrs_name);

        if (BNewLH.booleanValue() && (existingLHRS != null)) {

            // if new LH resource
            StringBuffer lhStringBuffer = convertArrayToStringBuffer((String[])
                    existingLHRS.toArray(new String[0]));

            if (lhStringBuffer.length() > 0) {
                lhStringBuffer.append(Util.COMMA);
            }

            lhStringBuffer.append(allLhRsName);
            allLhRsName = lhStringBuffer.toString();
        }

        summaryPanel.add(allLhRsName);
        selMap = new HashMap();
        selMap.put(NAME, lhrs_name);
        selMap.put(VALUE, allLhRsName);
        selMap.put(DESCRIPTION, lhrs_desc);
        selMap.put(COLON_DESC, lhrs_colon);
        tableMap.put(Integer.toString(row_count++), selMap);

        String hostNameList[] = (String[]) wizardModel.getWizardValue(
                HAOracleWizardConstants.NEW_HOSTNAMES_DEFINED);
        StringBuffer hostNames = convertArrayToStringBuffer(hostNameList);

        if ((hostNameList != null) && (hostNameList.length > 0)) {
            summaryPanel.add(hostname_name);
            summaryPanel.add(hostNames.toString());
            selMap = new HashMap();
            selMap.put(NAME, hostname_name);
            selMap.put(VALUE, hostNames.toString());
            selMap.put(DESCRIPTION, hostname_desc);
            selMap.put(COLON_DESC, hostname_colon);
            tableMap.put(Integer.toString(row_count++), selMap);
        }

        String netifList[] = (String[]) wizardModel.getWizardValue(
                LogicalHostWizardConstants.SEL_NETIFLIST);

        if ((netifList != null) && (netifList.length > 0)) {
            StringBuffer netifs = convertArrayToStringBuffer(netifList);
            summaryPanel.add(netif_name);
            summaryPanel.add(netifs.toString());
            selMap = new HashMap();
            selMap.put(NAME, netif_name);
            selMap.put(VALUE, netifs.toString());
            selMap.put(DESCRIPTION, netif_desc);
            selMap.put(COLON_DESC, netif_colon);
            tableMap.put(Integer.toString(row_count++), selMap);
        }

        TTYDisplay.pageText(description);
        TTYDisplay.clear(1);

        String subheadings[] = { heading1, heading2 };

        HashMap customTags = new HashMap();
        customTags.put(c_char, create_txt);
        customTags.put(q_char, quit_txt);

        boolean createconfig = false;

        while (!createconfig) {
            TTYDisplay.setNavEnabled(true);
            this.cancelDirection = false;

            String option = TTYDisplay.create2DTable(table_title, subheadings,
                    summaryPanel, customTags, help_text);

            if (option.equals(back)) {
                this.cancelDirection = true;

                return;
            }

            TTYDisplay.clear(3);
            TTYDisplay.setNavEnabled(false);

            if (!option.equals(c_char) && !option.equals(q_char)) {
                selMap = (HashMap) tableMap.get(option);

                String name = (String) selMap.get(NAME);
                String value = (String) selMap.get(VALUE);
                String desc = (String) selMap.get(DESCRIPTION);
                String colon_desc = (String) selMap.get(COLON_DESC);
                displayProp(panel_desc, desc, colon_desc, value);
                TTYDisplay.clear(2);
                TTYDisplay.promptForEntry(continue_txt);
            } else if (option.equals(c_char)) {

                // Create the Command Generation Structure
                Map confMap = new HashMap();

                // ResourceGroup Structure

                Boolean BNewRG = (Boolean) wizardModel.getWizardValue(
                        HAOracleWizardConstants.NEW_RG);

                // ResourceGroup Name
                confMap.put(Util.SERVER_GROUP + Util.NAME_TAG, rgName);

                if (BNewRG.booleanValue()) {

                    // ResourceGroup Properties
                    Map rgPropMap = new HashMap();
                    rgPropMap.put(Util.NODELIST,
                        nodeNames.toString().replace(' ', ','));
                    confMap.put(Util.SERVER_GROUP + Util.RGPROP_TAG, rgPropMap);
                }

                // Resource Structure
                if (bConfigServer) {

                    // Oracle Server Resource
                    confMap.put(Util.ORA_SERVER_RES_ID + Util.NAME_TAG,
                        serverRsName);

                    // Resource Properties
                    List HAOracleExtProp = new ArrayList();
                    List HAOracleSysProp = new ArrayList();

                    String oraHome = (String) wizardModel.getWizardValue(
                            HAOracleWizardConstants.SEL_ORACLE_HOME);
                    String oraSid = (String) wizardModel.getWizardValue(
                            HAOracleWizardConstants.SEL_ORACLE_SID);
                    ResourceProperty rp = new ResourcePropertyString(
                            Util.ORACLE_HOME, oraHome);
                    HAOracleExtProp.add(rp);
                    rp = new ResourcePropertyString(Util.ORACLE_SID, oraSid);
                    HAOracleExtProp.add(rp);

                    // get all other extension properties

                    String propertyNames[] = (String[]) wizardModel
                        .getWizardValue(Util.PROPERTY_NAMES);

                    // All extension properties
                    for (int i = 0; i < propertyNames.length; i++) {

                        // Filter already set properties
                        if (!propertyNames[i].equals(Util.RG_NAME) &&
                                !propertyNames[i].equals(Util.RS_NAME) &&
                                !propertyNames[i].equals(Util.NODELIST) &&
                                !propertyNames[i].equals(
                                    Util.SERVER_ORACLE_HOME) &&
                                !propertyNames[i].equals(
                                    Util.SERVER_PREFIX + Util.ORACLE_SID) &&
                                propertyNames[i].startsWith(
                                    Util.SERVER_PREFIX)) {
                            ResourceProperty prop = (ResourceProperty)
                                wizardModel.getWizardValue(propertyNames[i]);
                            String val = ResourcePropertyUtil.getAbsValue(prop);

                            if ((val != null) && (val.trim().length() > 0)) {
                                HAOracleExtProp.add(prop);
                            }
                        }
                    }

                    confMap.put(Util.ORA_SERVER_RES_ID + Util.SYSPROP_TAG,
                        HAOracleSysProp);

                    confMap.put(Util.ORA_SERVER_RES_ID + Util.EXTPROP_TAG,
                        HAOracleExtProp);
                }

                if (bConfigListener) {
                    confMap.put(Util.ORA_LISTENER_RES_ID + Util.NAME_TAG,
                        listenerRsName);

                    // Resource properties
                    List HAOracleExtProp = new ArrayList();
                    List HAOracleSysProp = new ArrayList();

                    String oraHome = (String) wizardModel.getWizardValue(
                            HAOracleWizardConstants.SEL_ORACLE_HOME);
                    ResourceProperty rp = new ResourcePropertyString(
                            Util.ORACLE_HOME, oraHome);
                    HAOracleExtProp.add(rp);

                    // get all other extension properties

                    String propertyNames[] = (String[]) wizardModel
                        .getWizardValue(Util.PROPERTY_NAMES);

                    // All extension properties
                    for (int i = 0; i < propertyNames.length; i++) {

                        // Filter already set properties
                        if (!propertyNames[i].equals(Util.RG_NAME) &&
                                !propertyNames[i].equals(Util.RS_NAME) &&
                                !propertyNames[i].equals(Util.NODELIST) &&
                                !propertyNames[i].equals(
                                    Util.LISTENER_ORACLE_HOME) &&
                                propertyNames[i].startsWith(
                                    Util.LISTENER_PREFIX)) {
                            ResourceProperty prop = (ResourceProperty)
                                wizardModel.getWizardValue(propertyNames[i]);
                            String val = ResourcePropertyUtil.getAbsValue(prop);

                            if ((val != null) && (val.trim().length() > 0)) {
                                HAOracleExtProp.add(prop);
                            }
                        }

                    }

                    confMap.put(Util.ORA_LISTENER_RES_ID + Util.SYSPROP_TAG,
                        HAOracleSysProp);

                    confMap.put(Util.ORA_LISTENER_RES_ID + Util.EXTPROP_TAG,
                        HAOracleExtProp);
                }

                if (BNewHASP.booleanValue()) {
                    HashMap newHashMap = new HashMap();
                    newHashMap.put(Util.NEW_HASP_RSNAME, haspRsName);
                    newHashMap.put(Util.NEW_FSMOUNTPOINTS, fsMountPoints);

                    // Get the map to add for /etc/vfstab and update all nodes
                    HashMap fsHashMap = (HashMap) wizardModel.getWizardValue(
                            Util.HASP_ALL_FILESYSTEMS);
                    newHashMap.put(Util.HASP_ALL_FILESYSTEMS, fsHashMap);
                    newHashMap.put(Util.HASP_ALL_DEVICES, globalDevicePaths);
                    confMap.put(Util.NEW_HASP_RS, newHashMap);
                }

                // add all existing HASPResources to the configuration Map
                if (existingHASPRS != null) {
                    ArrayList arrayList = new ArrayList(existingHASPRS);
                    confMap.put(Util.HASP_RES_ID + Util.NAME_TAG, arrayList);
                }

                if (BNewLH.booleanValue()) {

                    // new logical host resource
                    HashMap newHashMap = new HashMap();
                    newHashMap.put(Util.NEW_LH_RSNAME, lhRsName);
                    newHashMap.put(Util.HOSTNAMELIST, hostNameList);
                    newHashMap.put(Util.NETIFLIST, netifList);
                    confMap.put(Util.NEW_LH_RS, newHashMap);
                }

                // add all existing LH Resources to the configuration Map
                if (existingLHRS != null) {
                    ArrayList arrayList = new ArrayList(existingLHRS);
                    confMap.put(Util.LH_RES_ID + Util.NAME_TAG, arrayList);
                }

                confMap.put(Util.RG_NODELIST, nodeList);

                // Generating Commands For Dummy
                generateCommands(confMap);
                createconfig = true;
            } else if (option.equals(q_char)) {
                String exitOpt = TTYDisplay.getConfirmation(exit_txt, yes, no);

                if (exitOpt.equalsIgnoreCase(yes)) {
                    System.exit(0);
                }
            }

            TTYDisplay.clear(2);
        }
    }

    /**
     * @see  com.sun.cluster.wizards.core.WizardComponent#isDisplayComplete
     */
    public boolean isDisplayComplete() {
        return true;
    }

    /**
     * Returns the Name of the panel
     *
     * @return  Name of the Panel
     */
    public String getPanelName() {
        return this.panelName;
    }

    /**
     * Generates the commands by invoking the command generator
     */
    private void generateCommands(Map confMap) {
        List commandList = null;
        Thread t = null;

        try {
            TTYDisplay.clear(2);
            t = TTYDisplay.busy(wizardi18n.getString(
                        "haoracle.summarypanel.commandgeneration.busy"));
            t.start();

            if (mbean == null) {
                String nodeEndPoint = (String) wizardModel.getWizardValue(
                        Util.NODE_TO_CONNECT);
                JMXConnector connector = getConnection(nodeEndPoint);
                mbean = HAOracleWizardCreator.getHAOracleMBeanOnNode(connector);
            }

            commandList = mbean.generateCommands(confMap);
        } catch (CommandExecutionException cee) {
            wizardModel.setWizardValue(Util.EXITSTATUS, cee);
            commandList = mbean.getCommandList();
        } finally {
            wizardModel.setWizardValue(Util.CMDLIST, commandList);

            try {
                t.interrupt();
                t.join();
            } catch (Exception exe) {
            }
        }
    }

    /**
     * Converts a given String Array into a StringBuffer
     */
    private StringBuffer convertArrayToStringBuffer(String array[]) {
        StringBuffer buffer = new StringBuffer();

        if ((array != null) && (array.length > 0)) {

            for (int i = 0; i < array.length; i++) {
                buffer.append(array[i]);

                if (i < (array.length - 1))
                    buffer.append(Util.COMMA);
            }
        }

        return buffer;
    }

    private void displayProp(String panel_desc, String desc, String colon,
        String value) {

        // Display nodelist
        TTYDisplay.pageText(panel_desc + desc);
        TTYDisplay.clear(1);
        TTYDisplay.pageText(colon + value);
    }
}
