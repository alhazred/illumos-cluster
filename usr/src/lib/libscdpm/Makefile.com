#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)Makefile.com	1.6	08/05/20 SMI"
#
# lib/libscdpm/Makefile.com
#

LIBRARYCCC = libscdpm.a
VERS = .1

C_OBJECTS=  scdpm.o

CC_OBJECTS = \
	libdpm.o \
	dpm_impl.o

OBJECTS = $(C_OBJECTS) $(CC_OBJECTS)

# include library definitions
include ../../Makefile.lib

SRCS =		$(C_OBJECTS:%.o=../common/%.c)
SRCS +=		$(CC_OBJECTS:%.o=../common/%.cc)

LIBS =		$(DYNLIB)

CLEANFILES =

LINTFLAGS +=	-I..
LINTFILES +=	$(OBJECTS:%.o=%.ln)

MTFLAG	=	-mt

MAPFILE =	../common/mapfile-vers
PMAP =

CPPFLAGS += $(CL_CPPFLAGS)
CPPFLAGS += -I$(SRC)/lib/libdid/include -I$(SRC)/common/cl \
	-I$(SRC)/common/cl/interfaces/$(CLASS) \
	-I../include \
	-I../common

CHECKHDRS = dpm_impl.h ../include/libscdpm.h

LDLIBS += -lclcomm -lfe -lclos -lthread -ldid -lsocket -lxnet -lnsl -lc -lclconf

.KEEP_STATE:

# include library targets
include ../../Makefile.targ

objs/%.o pics/%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<

objs/%.o pics/%.o: ../common/%.cc
	$(COMPILE.cc) -o $@ $<
