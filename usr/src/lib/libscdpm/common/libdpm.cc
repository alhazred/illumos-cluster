/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)libdpm.cc	1.18	08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <strings.h>
#include <unistd.h>
#include <limits.h>
#include <netdb.h>
#include <sys/os.h>
#include <h/ccr.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/infrastructure/orb.h>
#include <orb/object/adapter.h>
#include <orb/fault/fault_injection.h>
#include <nslib/ns.h>
#include <sys/clconf_int.h>
#include <sys/clconf.h>
#include <orb/invo/common.h>
#include <orb/invo/corba.h>
#include <nslib/ns.h>
#include <pthread.h>
#include <sys/sc_syslog_msg.h>
#include <h/dpm.h>

#include <dpm_impl.h>
#include <libscdpm.h>

static	naming::naming_context_ptr ctxp = naming::naming_context::_nil();
static  ccr::directory_ptr ccr_dir = ccr::directory::_nil();
static  ccr::updatable_table_ptr ccr_writetable
		= ccr::updatable_table::_nil();

os::sc_syslog_msg msg_handle("", "", "");

//
// DPM daemon calls this function when it starts. It registers
// an object of type dpm_impl (defined by the interface in
// dpm.idl) with the name server. Naming convention for this
// object is "DPM.<nodeid>".
// Input parameter dpmcom contains pointers to the device list
// and the device list lock used by the daemon.
//
int
register_dpm_daemon(
    dpm_comm_t *dpmcom)
{
	char dpmd_name[MAXNAMELEN];
	if (dpm_impl::initialize(dpmcom) != 0) {
		//
		// SCMSGS
		// @explanation
		// Disk Path Monitoring daemon could not register itself with
		// the Name Server.
		// @user_action
		// This is a fatal error for the Disk Path Monitoring daemon
		// and will mean that the daemon cannot start on this node.
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void)  msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		    "%s: Could not register DPM daemon. Daemon will not"
		    " start on this node", LIBSCDPM);
		return (-1);
	}

	disk_path_monitor::dpm_var dpmv = dpm_impl::the()->get_objref();
	naming::naming_context_var ctxv = ns::root_nameserver();
	ASSERT(! CORBA::is_nil(ctxv));

	os::sprintf(dpmd_name, "%s%d", DPMD_NAME_PREFIX,
			orb_conf::local_nodeid());
	Environment e;
	ctxv->rebind(dpmd_name, dpmv, e);
	if (e.exception()) {
			(void)  msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
			"%s: Could not register DPM daemon. Daemon will not"
			" start on this node", LIBSCDPM);
		e.clear();
		return (-1);
	}

	return (DPM_SUCCESS);
}

//
// Remove the binding for the DPM daemon on this node from
// the Name Server.
//
void
unregister_dpm_daemon()
{
	char dpmd_name[10];
	os::sprintf(dpmd_name, "DPM.%d", orb_conf::local_nodeid());
	Environment e;
	naming::naming_context_var ctxv = ns::root_nameserver();
	ASSERT(!CORBA::is_nil(ctxv));
	ctxv->unbind(dpmd_name, e);
	if (e.exception()) {
	}
	e.clear();
}

//
// Helper function to initialize the ORB.
//
int
dpm_initialize()
{
	return (ORB::initialize());
}

//
// Extracts the error code from the environment passed in and
// clears the environment. The input argument should not be
// used subsequently by the caller to check for exceptions.
//
static int
get_err_from_env(
    Environment &e)
{
	CORBA::Exception *ex;
	disk_path_monitor::dpm_error *ex_dpm;
	int error = DPM_SUCCESS;

	if ((ex = e.exception()) != NULL) {
	    if ((ex_dpm =
		disk_path_monitor::dpm_error::_exnarrow(ex)) != NULL) {
			error = (int)ex_dpm->error;
	    } else {
			error = DPM_ERR_COMM;
	    }
	}
	e.clear();
	return (error);
}

//
// Helper function that returns a reference to the DPM object
// for a given node. Reference is passed back in the second
// argument. The caller determines success of the call by
// checking the return value.
//
static int
get_dpmd_reference(
    disk_path_monitor::dpm_var *ret_dpm_var,
    nodeid_t nodeid)
{
	CORBA::Object_var dpm_obj;
	CORBA::Exception *ex;
	Environment e, env;
	char dpmd_name[MAXNAMELEN];
	int error;

	//
	// Construct the object name for the node whose
	// nodeid is passed in.
	//
	os::sprintf(dpmd_name, "%s%d", DPMD_NAME_PREFIX, (int)nodeid);

	naming::naming_context_var dpmd_ctxp = ns::root_nameserver();
	dpm_obj = dpmd_ctxp->resolve(dpmd_name, e);

	if ((ex = e.exception()) != NULL) {
		if (naming::not_found::_exnarrow(ex) != NULL) {
			error = DPM_NO_DAEMON;
		} else {
			error = DPM_UNKNOWN_ERROR;
		}
		*ret_dpm_var = disk_path_monitor::dpm::_nil();
		e.clear();
		return (error);
	}

	ASSERT(! CORBA::is_nil(dpm_obj));

	disk_path_monitor::dpm_var tmp_dpm_var =
		disk_path_monitor::dpm::_narrow(dpm_obj);

	ASSERT(! CORBA::is_nil(tmp_dpm_var));

	bool is_daemon_alive = tmp_dpm_var->is_alive(env);
	if (env.exception() == NULL && is_daemon_alive) {
		*ret_dpm_var =
		    disk_path_monitor::dpm::_duplicate(tmp_dpm_var);
		return (DPM_SUCCESS);
	} else {
		*ret_dpm_var = disk_path_monitor::dpm::_nil();
		return (DPM_NO_DAEMON);
	}
}

void
dpm_free_status_list(
	disk_status_node_t *disk_list)
{
	disk_status_node_t *ptr, *tmp_ptr;

	if (disk_list == NULL) {
		return;
	}

	ptr = tmp_ptr = disk_list;
	while (ptr != NULL) {
		tmp_ptr = ptr;
		ptr = ptr->next;
		if (tmp_ptr->name != NULL)
			delete tmp_ptr->name;
		delete tmp_ptr;
	}
}

void
dpm_free_status_seq(
	disk_path_monitor::dpm_disk_status_seq *disk_seq)
{
	if (disk_seq != NULL)
		delete disk_seq;
}

//
// Helper function for data type conversion of
// disk_status_node_t to dpm_disk_status_seq defined in
// the dpm interface. Memory for the sequence returned
// should be freed by the caller.
//
static disk_path_monitor::dpm_disk_status_seq *
conv_to_status_seq(disk_status_node_t *disk_list, unsigned int count)
{
	disk_path_monitor::dpm_disk_status_seq *_dpm_disk_list =
		new disk_path_monitor::dpm_disk_status_seq;

	if (count == 0) {
		_dpm_disk_list->length(0);
		return (_dpm_disk_list);
	}

	ASSERT(disk_list != NULL);

	_dpm_disk_list->length(count);
	disk_status_node_t *tmp_node = disk_list;
	for (uint_t i = 0; i < count; i++) {
		if (((*_dpm_disk_list)[i].disk_path =
			(const char *)os::strdup(tmp_node->name)) ==
			(const char *)NULL) {
			perror("libscdpm: ");
			dpm_free_status_seq(_dpm_disk_list);
			return (NULL);
		}
		(*_dpm_disk_list)[i].status = DPM_PATH_UNKNOWN;
		tmp_node = tmp_node->next;
	}

	return (_dpm_disk_list);
}

//
// Helper function for data type conversion of
// dpm_disk_status_seq to disk_status_node_t.
//
static disk_status_node_t *
conv_to_status_list(disk_path_monitor::dpm_disk_status_seq *dpm_disk_list)
{

	disk_status_node_t *disk_list = NULL;
	disk_status_node_t *tmp_disk_list = NULL;
	disk_status_node_t *disk_list_head = NULL;

	if ((dpm_disk_list == NULL) || (dpm_disk_list->length() == 0)) {
		return (NULL);
	}

	for (uint_t i = 0; i < dpm_disk_list->length(); i++) {
		disk_list = new disk_status_node_t;
		if (tmp_disk_list == NULL) {
			disk_list_head = disk_list;
		} else {
			tmp_disk_list->next = disk_list;
		}
		tmp_disk_list = disk_list;
		if ((tmp_disk_list->name =
		    os::strdup((*dpm_disk_list)[i].disk_path)) == NULL) {
			perror("libscdpm: ");
			dpm_free_status_list(disk_list_head);
			return (NULL);
		}
		tmp_disk_list->status = (*dpm_disk_list)[i].status;
	}

	tmp_disk_list->next = NULL;

	return (disk_list_head);
}

static void
fill_in_disk_status(disk_status_node_t *disk_list,
	disk_path_monitor::dpm_disk_status_seq
	*dpm_disk_list)
{
	ASSERT(disk_list != NULL);
	for (uint_t i = 0; i < dpm_disk_list->length(); i++) {
		disk_list->status = (*dpm_disk_list)[i].status;
		disk_list = disk_list->next;
	}
}

//
// This function calls the daemon on the appropriate node to
// change the status of a list of disks. It is called by the
// command line interface. It returns the status of the each
// disk in the list to the CLI.
//
int
libdpm_change_monitoring_status(
	nodeid_t nodeid, disk_status_node_t *monitor_list,
	uint_t mon_list_count, disk_status_node_t
	*unmonitor_list, uint_t unmon_list_count)
{
	disk_path_monitor::dpm_disk_status_seq *dpm_monitor_list;
	disk_path_monitor::dpm_disk_status_seq *dpm_unmonitor_list;
	disk_path_monitor::dpm_var dpmd_ref;
	int error, err;

	error = get_dpmd_reference(&dpmd_ref, nodeid);
	if (error != DPM_SUCCESS) {
		return (error);
	}

	dpm_monitor_list = conv_to_status_seq(monitor_list,
	    mon_list_count);
	if (dpm_monitor_list == NULL) {
		return (DPM_OUT_OF_MEMORY);
	}

	dpm_unmonitor_list = conv_to_status_seq(unmonitor_list,
	    unmon_list_count);
	if (dpm_unmonitor_list == NULL) {
		return (DPM_OUT_OF_MEMORY);
	}

	Environment e;
	dpmd_ref->dpm_change_monitoring_status(
	    *dpm_monitor_list, *dpm_unmonitor_list, e);
	if ((err = get_err_from_env(e)) != DPM_SUCCESS) {
		goto cleanup;
	}

	if (monitor_list != NULL) {
	    fill_in_disk_status(monitor_list, dpm_monitor_list);
	}

	if (unmonitor_list != NULL) {
	    fill_in_disk_status(unmonitor_list, dpm_unmonitor_list);
	}

cleanup:
	dpm_free_status_seq(dpm_monitor_list);
	dpm_free_status_seq(dpm_unmonitor_list);
	return (err);
}

//
// This function calls the daemon on the appropriate node to
// change the status of all disks. The status could be changed
// from "monitored" to "unmonitored" and vice versa.
//
int
libdpm_change_monitoring_status_all(
    nodeid_t nodeid, int command)
{
	disk_path_monitor::dpm_var dpmd_ref;
	int error;

	error = get_dpmd_reference(&dpmd_ref, nodeid);
	if (error != DPM_SUCCESS) {
		return (error);
	}

	Environment e;
	dpmd_ref->dpm_change_monitoring_status_all(
		command, e);

	return (get_err_from_env(e));
}

//
// This functions calls the daemon on the appropriate node
// to get the monitoring status of a list of disks supplied
// by the command line interface.
//
int
libdpm_get_monitoring_status(
    nodeid_t nodeid, disk_status_node_t *disk_list,
    uint_t disk_list_count,
    char do_probe)
{
	disk_path_monitor::dpm_disk_status_seq *dpm_status_list;
	disk_path_monitor::dpm_var dpmd_ref;
	int error;

	error = get_dpmd_reference(&dpmd_ref, nodeid);
	if (error != DPM_SUCCESS) {
		return (error);
	}

	dpm_status_list = conv_to_status_seq(disk_list, disk_list_count);
	if (dpm_status_list == NULL) {
		return (DPM_OUT_OF_MEMORY);
	}

	Environment e;

	dpmd_ref->dpm_get_monitoring_status(*dpm_status_list, do_probe, e);
	if ((error = get_err_from_env(e)) == DPM_SUCCESS) {
		fill_in_disk_status(disk_list, dpm_status_list);
	}

	dpm_free_status_seq(dpm_status_list);
	return (error);
}

//
// This function calls the daemon on the appropriate node to get the
// monitoring status of all disks being monitored from that node.
//
int
libdpm_get_monitoring_status_all(
    nodeid_t nodeid, disk_status_node_t **disk_list)
{
	disk_path_monitor::dpm_disk_status_seq_var disk_status_list;
	disk_path_monitor::dpm_var dpmd_ref;
	int error = DPM_SUCCESS;

	error = get_dpmd_reference(&dpmd_ref, nodeid);
	if (error != DPM_SUCCESS) {
		return (error);
	}

	Environment e;
	dpmd_ref->dpm_get_monitoring_status_all(disk_status_list, e);
	if ((error = get_err_from_env(e)) == DPM_SUCCESS) {
		*disk_list = conv_to_status_list(disk_status_list);
	}

	return (error);
}

//
// This function calls the daemon on the appropriate node to get
// the list of all the disks with failed paths on that node.
//
int
libdpm_get_failed_disk_status(
    nodeid_t nodeid, disk_status_node_t **disk_list)
{
	disk_path_monitor::dpm_disk_status_seq_var disk_status_list;
	disk_path_monitor::dpm_var dpmd_ref;
	int error = DPM_SUCCESS;

	error = get_dpmd_reference(&dpmd_ref, nodeid);
	if (error != DPM_SUCCESS) {
		return (error);
	}

	Environment e;
	dpmd_ref->dpm_get_failed_disk_status(disk_status_list, e);
	if ((error = get_err_from_env(e)) == DPM_SUCCESS) {
		*disk_list = conv_to_status_list(disk_status_list);
	}

	dpm_free_status_seq(disk_status_list);
	return (error);
}

int
libdpm_remove_disks(
	nodeid_t nodeid, disk_status_node_t *disk_list,
	uint_t count)
{
	disk_path_monitor::dpm_disk_status_seq *dpm_remove_list;
	disk_path_monitor::dpm_var dpmd_ref;
	int error, err;

	error = get_dpmd_reference(&dpmd_ref, nodeid);
	if (error != DPM_SUCCESS) {
		return (error);
	}

	dpm_remove_list = conv_to_status_seq(disk_list, count);
	if (dpm_remove_list == NULL) {
		return (DPM_OUT_OF_MEMORY);
	}

	Environment e;
	dpmd_ref->dpm_remove_disks(
	    *dpm_remove_list, e);
	if ((err = get_err_from_env(e)) != DPM_SUCCESS) {
		goto cleanup;
	}

	if (disk_list != NULL) {
	    fill_in_disk_status(disk_list, dpm_remove_list);
	}

cleanup:
	dpm_free_status_seq(dpm_remove_list);
	return (err);
}

//
// Read the auto reboot setting from the CCR. If an error is encountered,
// the arrary will be set to -1 and an error code will be returned.
//
static int
read_auto_reboot_feature_state_from_ccr(boolean_t *autorebootlist)
{
	Environment		e;
	CORBA::Exception 	*ex;
	char			*rebootstatestr;
	ccr::table_element 	*outelem = NULL;
	int			ret = DPM_SUCCESS;
	int			i = 0;

	// Initialize to B_FALSE
	(void) memset(autorebootlist, B_FALSE, sizeof (boolean_t) * NODEID_MAX);

retry:
	ccr::readonly_table_var	read_v = ccr_dir->lookup(DPM_AUTOREBOOT_TABLE,
	    e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::no_such_table::_exnarrow(ex)) {
			e.clear();
			//
			// Table does not exist, treat it as B_FALSE for all
			// nodes.
			//
			return (DPM_SUCCESS);
		} else {
			ret = DPM_CCR_ACCESS_ERROR;
			goto error_return;
		}
	}

	read_v->atfirst(e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::table_modified::_exnarrow(ex)) {
			// Table has been modified. get a new handle
			e.clear();
			goto retry;
		}
		ret = DPM_CCR_ACCESS_ERROR;
		goto error_return;
	}

	read_v->next_element(outelem, e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::NoMoreElements::_exnarrow(ex)) {
			//
			// Empty table. The table must have just been created.
			// Treat it as all nodes B_FALSE.
			//
			e.clear();
			delete outelem;
			return (DPM_SUCCESS);
		} else if (ccr::table_modified::_exnarrow(ex)) {
			e.clear();
			goto retry;
		} else {
			ret = DPM_CCR_ACCESS_ERROR;
			goto error_return;
		}
	}

	if (os::strcmp(outelem->key, DPM_AUTOREBOOT_KEY) != 0) {
		ret = DPM_CCR_FORMAT_ERROR;
		goto error_return;
	}

	rebootstatestr = strtok(outelem->data, "|");
	while (rebootstatestr != NULL && i < NODEID_MAX) {
		autorebootlist[i] = (boolean_t)atoi(rebootstatestr);
		if (autorebootlist[i] != B_FALSE &&
		    autorebootlist[i] != B_TRUE) {
			autorebootlist[i] = (boolean_t)-1;
			ret = DPM_CCR_FORMAT_ERROR;
			goto error_return;
		}
		i++;
		rebootstatestr = strtok(NULL, "|");
	}

	delete outelem;
	return (DPM_SUCCESS);

error_return:
	// Error encountered, set everything to -1.
	(void) memset(autorebootlist, -1, sizeof (boolean_t) * NODEID_MAX);
	delete outelem;
	e.clear();
	return (ret);
}

//
// This function reads the auto reboot state for all nodes from the CCR.
// This is called by the command line interface.
//
int
libdpm_get_all_auto_reboot_state(boolean_t *autorebootlist)
{
	if (dpmccr_ready() != 0)
		return (DPM_CCR_ACCESS_ERROR);

	return (read_auto_reboot_feature_state_from_ccr(autorebootlist));
}

//
// Get the local node's auto reboot configuration. If it's called by
// the daemon, we will look it up from dpm_impl class. Otherwise,
// we look it up from the CCR.
//
int
libdpm_get_local_node_auto_reboot_state(boolean_t *rebootstate)
{
	disk_path_monitor::dpm_var dpmd_v;
	nodeid_t this_nodeid = orb_conf::local_nodeid();
	int	error;
	Environment e;
	boolean_t	autorebootlist[NODEID_MAX];

	if (dpm_impl::the() != NULL) {
		// We are running on the daemon side.
		*rebootstate = dpm_impl::the()->get_auto_reboot_state();
		if (*rebootstate != -1) {
			return (DPM_SUCCESS);
		} else {
			return (-1);
		}
	}

	// We are running on the client side, read the states from the CCR.
	if ((error = libdpm_get_all_auto_reboot_state(autorebootlist)) !=
	    DPM_SUCCESS) {
		return (error);
	}

	*rebootstate = autorebootlist[this_nodeid - 1];
	return (DPM_SUCCESS);
}

//
// This function writes the new autoreboot state for the specified node
// or all nodes to the CCR. This is called by the command line interface.
//
int
libdpm_set_auto_reboot_feature_state(
	nodeid_t nodeid,
	boolean_t reboot_state)
{
	Environment	e;
	boolean_t	autorebootlist[NODEID_MAX];
	char		data_buf[NODEID_MAX * 2] = "";
	ccr::element_seq *seq = new ccr::element_seq(1, 1);
	int		dirty = 0, ret = DPM_SUCCESS;
	CORBA::Exception *ex;

	if (dpmccr_ready() != 0)
	    return (DPM_CCR_ACCESS_ERROR);

	ccr::updatable_table_var ut_v = ccr_dir->begin_transaction(
	    DPM_AUTOREBOOT_TABLE, e);
	while ((ex = e.exception()) != NULL) {
		if (ccr::no_such_table::_exnarrow(ex)) {
			e.clear();
			ccr_dir->create_table(DPM_AUTOREBOOT_TABLE, e);
			if (((ex = e.exception()) == NULL) ||
			    ccr::table_exists::_exnarrow(ex)) {
				//
				// If create table succeeds or someone else
				// has already created the table, just
				// fall through.
				//
				e.clear();
				dirty++;
				ut_v = ccr_dir->begin_transaction(
				    DPM_AUTOREBOOT_TABLE, e);
				continue;
			} else {
				ret = DPM_CCR_ACCESS_ERROR;
				goto error_return;
			}
		} else {
			ret = DPM_CCR_ACCESS_ERROR;
			goto error_return;
		}
	}

	if ((ret = read_auto_reboot_feature_state_from_ccr(autorebootlist))
	    != 0) {
		goto error_return;
	}

	if (nodeid == ALL_NODEIDS) {
		for (int i = 0; i < NODEID_MAX; i++) {
			if (autorebootlist[i] != reboot_state) {
				autorebootlist[i] = reboot_state;
				dirty++;
			}
		}
	} else {
		if (autorebootlist[nodeid - 1] != reboot_state) {
			autorebootlist[nodeid - 1] = reboot_state;
				dirty++;
		}
	}

	if (! dirty) {
		// No need to change
		delete seq;
		// Abort the transaction;
		ut_v->abort_transaction(e);
		e.clear();
		return (DPM_SUCCESS);
	}

	// Prepare the data_buf for the CCR
	(void) sprintf(data_buf, "%d", autorebootlist[0]);
	for (int i = 1; i < NODEID_MAX; i++) {
		(void) sprintf(data_buf, "%s|%d", data_buf, autorebootlist[i]);
	}

	if (((*seq)[0].key = os::strdup(DPM_AUTOREBOOT_KEY)) ==
	    (const char *)NULL) {
		ret = DPM_OUT_OF_MEMORY;
		goto error_return;
	}
	if (((*seq)[0].data = os::strdup(data_buf)) == (const char *)NULL) {
		ret = DPM_OUT_OF_MEMORY;
		goto error_return;
	}

	ut_v->remove_all_elements(e);
	if (e.exception()) {
		ret = DPM_CCR_ACCESS_ERROR;
		goto error_return;
	}

	ut_v->add_elements(*seq, e);
	if (e.exception()) {
		ret = DPM_CCR_ACCESS_ERROR;
		goto error_return;
	}

	ut_v->commit_transaction(e);
	if (e.exception()) {
		ret = DPM_CCR_ACCESS_ERROR;
		goto error_return;
	}

	delete seq;

	return (DPM_SUCCESS);

error_return:
	delete seq;
	e.clear();
	// Abort the transaction;
	if (! CORBA::is_nil(ut_v)) {
		ut_v->abort_transaction(e);
		e.clear();
	}

	return (ret);
}

boolean_t
libdpm_policy_engine_registered()
{
	return (dpm_policy_impl::policy_engine_registered());
}

//
// This function is called by the DPM daemon to trigger a fault
// point. The input parameter specifies which fault point to
// trigger.
//
int
dpm_inject_fault(
	int which_fault_pt)
{
	switch (which_fault_pt) {

	case FAULTPT_PATH_BAD:
#ifdef _FAULT_INJECTION
		if (fault_triggered(FAULTNUM_DPM_PATH_BAD,
			NULL, NULL)) {
			return (1);
		}
#endif
		break;

	default:
		break;
	}
	return (-1);
}

//
// Given a nodename, this function returns its nodeid.
//
int
get_nodeid_by_nodename(char *nodename)
{
	ASSERT(nodename != NULL);
	int i, error;
	const char *tempname;
	clconf_cluster_t *cl;

	error = clconf_lib_init();
	if (error) {
	    (void) fprintf(stderr,
		"libscdpm: clconf library init failed.\n");
	    return (-1);
	}

	cl = clconf_cluster_get_current();

	for (i = 0; i < NODEID_MAX + 1; i++) {
		tempname = clconf_cluster_get_nodename_by_nodeid(cl,
			(nodeid_t)i);

		if (tempname == NULL) {
		    continue;
		}

		if (strcmp(tempname, nodename) == 0) {
		    return (i);
		}
	}

	// Didn't find a match.
	return (-1);
}

//
// Helper function to resolve the CCR directory table from
// the name server.
//
// The initial call to this function should only be done in a non
// multi threaded environment as the test for ccr_dir being NULL
// is non atomic with respect to the creation of the references
// stored in ctxp and ccrdir
//
int
dpmccr_ready()
{
	Environment e;
	CORBA::Object_var obj;

	if (! CORBA::is_nil(ccr_dir)) {
		return (0);
	}

	ctxp = ns::local_nameserver();
	if (CORBA::is_nil(ctxp)) {
		//
		// SCMSGS
		// @explanation
		// libscdpm could not perform an operation on the CCR table
		// specified in the message.
		// @user_action
		// Check if the table specified in the message is present in
		// the Cluster Configuration Repository (CCR). The CCR is
		// located in /etc/cluster/ccr. This error could happen
		// because the table is corrupted, or because there is no
		// space left on the root file system to make any updates to
		// the table. This message will mean that the Disk Path
		// Monitoring daemon cannot access the persistent information
		// it maintains about disks and their monitoring status.
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void)  msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		"%s: CCR exception for %s table", LIBSCDPM,
			DPM_STATUS_TABLE);
		return (-1);
	}

	obj = ctxp->resolve("ccr_directory", e);
	if (e.exception()) {
	    (void)  msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
	    "%s: CCR exception for %s table", LIBSCDPM, "directory");
	    return (-1);
	}
	ccr_dir = ccr::directory::_narrow(obj);
	ASSERT(ccr_dir);
	return (0);
}


//
// This function is called by the daemon and the CLI to create the
// dpm_status_table and dpm_autoreboot_table if it does not exist. It populate
// the dpm_status_table table with entries for all disks in the cluster.
// This is the format of each entry in dpm_status_table:
// <nodeid>:<device_path>	<status>
// For example:
//
// 1:/dev/did/rdsk/d6s0	65535
// 2:/dev/rdsk/c0t0d0s0 65535
//
// Memory for ret_ccr_list must be freed by the caller.
// When it is called from the CLI, the argument is NULL. In that
// case we do not return the list read from the CCR. DPM daemon
// calls this method when it comes up and it expects the list
// read from the CCR to be returned pointed to by the argument.
int
dpmccr_initialize(disk_status_node_t **ret_ccr_list)
{
	int error = 0;
	uint_t count = 0;
	disk_status_node_t *ccr_disk_list = NULL, *tmp_disk_list;

	if (dpmccr_ready() != 0) {
	    return (-1);
	}

	if (dpmccr_create_table(DPM_AUTOREBOOT_TABLE)) {
	    return (-1);
	}

	if (dpmccr_create_table(DPM_STATUS_TABLE)) {
	    return (-1);
	}

	if (dpmccr_begin_write_operation()) {
	    return (-1);
	}

	if (dpmccr_read_disks_info(&ccr_disk_list, &count)) {
	    (void) dpmccr_abort_write_operation();
	    return (-1);
	}

	if (ccr_disk_list == NULL) {
	    //
	    // Read the did_instances table.
	    //
	    tmp_disk_list = dpmccr_read_did_inst_table();
	    if (tmp_disk_list == NULL) {
		(void) dpmccr_abort_write_operation();
		return (-1);
	    }
	    //
	    // Write all disks' info to the status table.
	    //
	    error = dpmccr_write_disks_info(tmp_disk_list, SCDPM_UPDATE_FORCE);
	    if (error) {
		if (ret_ccr_list != NULL) {
		    *ret_ccr_list = NULL;
		}
		(void) dpmccr_abort_write_operation();
		dpm_free_status_list(tmp_disk_list);
		return (-1);
	    }
	    if (ret_ccr_list != NULL) {
		*ret_ccr_list = tmp_disk_list;
	    }
	} else {
	    if (ret_ccr_list != NULL) {
		*ret_ccr_list = ccr_disk_list;
	    }
	}

	error = dpmccr_end_write_operation();
	return (error);
}

//
// Helper function called by dpmccr_initialize() to read the
// did_instances table and construct a list to be written to
// the dpm_status_table.
// list_all_devices() returns a pointer to a did_device_list_t
// type. For each DID instance, the subpath_listp field in this
// structure points to the list of paths on different nodes that
// map to this instance. Each element in this list is of the form
// <nodename:CTD-name>.
// This is the algorithm:
// 1.	list_all_devices();
// 2.	    for (each element in the list) {
// 3.		extract the DID name into DID_NAME;
// 4.		for (each subpath) {
// 5.		    extract nodename;
// 6.		    convert nodename to nodeid;
// 7.		    construct a string: <nodeid>:/dev/did/<DID_NAME>s0;
// 8.		    append it the list to be returned;
// 9.		}
// 10.	    }
//
// An improvement to this method will be to use the Name Server
// optimization that is used by DID.
//
disk_status_node_t *
dpmccr_read_did_inst_table()
{
	did_device_list_t *devp, *tmp_devp;
	did_subpath_t *subpath_ptr;
	disk_status_node_t *ret_list = NULL;
	char *curr, *did_path = NULL;
	char full_path[_POSIX_PATH_MAX];
	char nodeidstr[MAXNAMELEN];
	int error = 0;

	devp = list_all_devices("disk");

	tmp_devp = devp;
	while (tmp_devp != NULL) {
		subpath_ptr = tmp_devp->subpath_listp;
		//
		// did_path is of the form: rdsk/dN
		//
		did_path = did_get_did_path(tmp_devp);
		if (did_path == NULL) {
			if (did_geterrorstr() != NULL) {
				(void) fprintf(stderr, "%s", did_geterrorstr());
			}
			tmp_devp = tmp_devp->next;
			continue;
		}
		while (subpath_ptr != NULL) {
		    //
		    // A NULL diskid means it's a tape or a CDROM.
		    //
		    if (tmp_devp->diskid != NULL) {
			//
			// Extract nodename.
			//
			curr = strtok(subpath_ptr->device_path, ":");
			if (curr == NULL) {
				dpm_free_status_list(ret_list);
				free(did_path);
				return (NULL);
			}
			os::sprintf(nodeidstr, "%d",
				(int)get_nodeid_by_nodename(curr));
			if (snprintf(full_path, _POSIX_PATH_MAX,
				":/dev/did/%ss0", did_path) < 0) {
				dpm_free_status_list(ret_list);
				free(did_path);
				return (NULL);
			}
			error = dpm_append_status_list(&ret_list,
					strcat(nodeidstr, full_path),
					DPM_PATH_MONITORED|DPM_PATH_UNKNOWN);
			if (error) {
				dpm_free_status_list(ret_list);
				free(did_path);
				return (NULL);
			}
		    }
		    subpath_ptr = subpath_ptr->next;
		}
		tmp_devp = tmp_devp->next;
		if (did_path != NULL) {
			free(did_path);
		}
	}

	return (ret_list);
}

//
// Helper function to create the dpm tables (status and autoreboot) if it does
// not already exist.
//
int
dpmccr_create_table(const char *tname)
{
	Environment e;
	CORBA::Exception *ex;
	ccr::readonly_table_var table;

	if (dpmccr_ready() != 0)
		return (-1);

	table = ccr_dir->lookup(tname, e);
	if ((ex = e.exception()) != NULL) {
	    if (ccr::no_such_table::_exnarrow(ex)) {
		e.clear();
		ccr_dir->create_table(tname, e);
		if ((ex = e.exception()) != NULL) {
		    if (ccr::table_exists::_exnarrow(ex)) {
			//
			// If someone else created the table already
			// it is not an error, so just fall through.
			//
			e.clear();
		    } else {
			e.clear();
			//
			// SCMSGS
			// @explanation
			// libscdpm could not create either
			// 1. cluster-wide consistent table that the Disk Path
			// Monitoring Daemon uses to maintain persistent
			// information about disks and their monitoring status.
			// OR
			// 2. cluster-wide consistent table that the Disk Path
			// Monitoring Daemon uses to maintain persistent
			// information about the autoreboot feature status of
			// all nodes.
			// @user_action
			// This is a fatal error for the Disk Path Monitoring
			// daemon and will mean that the daemon cannot run on
			// this node. Check to see if there is enough space
			// available on the root file system. Contact your
			// authorized Sun service provider to determine
			// whether a workaround or patch is available.
			//
			(void)  msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
			"%s: Could not create %s table", LIBSCDPM, tname);
			return (-1);
		    }
		}
	    } else {
		e.clear();
		(void)  msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		"%s: Could not create %s table", LIBSCDPM, tname);
		return (-1);
	    }
	}

	return (0);
}

//
// This function reads the dpm_status_table, and constructs a list,
// which is returned in the first argument.
//
int
dpmccr_read_disks_info(
	disk_status_node_t **ret_disk_list,
	uint_t *disk_count)
{
	Environment e;
	ccr::readonly_table_var table;
	ccr::table_element_var  elem;
	disk_status_node_t *disk_list = NULL;

	table = ccr_dir->lookup(DPM_STATUS_TABLE, e);
	if (e.exception()) {
	    (void)  msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
	    "%s: CCR exception for %s table", LIBSCDPM, DPM_STATUS_TABLE);
	    e.clear();
	    return (-1);
	}

	table->atfirst(e);
	if (e.exception()) {
	    (void)  msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
	    "%s: CCR exception for %s table", LIBSCDPM, DPM_STATUS_TABLE);
	    e.clear();
	    return (-1);
	}

	for (;;) {
		table->next_element(elem, e);
		if (e.exception()) {
		    if (ccr::NoMoreElements::_exnarrow(e.exception())) {
			e.clear();
			break;
		    } else {
			(void)  msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
			"%s: CCR exception for %s table", LIBSCDPM,
			DPM_STATUS_TABLE);
			e.clear();
			return (-1);
		    }
		}

		if (dpm_append_status_list(&disk_list, (char *)elem->key,
			atoi(elem->data))) {
			dpm_free_status_list(disk_list);
			return (-1);
		}
		(*disk_count)++;
	}

	*ret_disk_list = disk_list;
	return (0);
}

//
// Helper function to start a transaction on the dpm_status_table.
//
// This function/method is not multi threaded safe.
//
int
dpmccr_begin_write_operation(void)
{
	Environment e;

	if (CORBA::is_nil(ccr_writetable)) {
	    ccr_writetable = ccr_dir->begin_transaction(DPM_STATUS_TABLE, e);
	    if (e.exception()) {
		(void)  msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		"%s: CCR exception for %s table", LIBSCDPM, DPM_STATUS_TABLE);
		e.clear();
		return (-1);
	    }
	}
	return (0);
}

//
// Helper function to end a transaction on the dpm_status_table.
//
// This function/method is not multi threaded safe.
//
int
dpmccr_end_write_operation(void)
{
	Environment e;

	if (!CORBA::is_nil(ccr_writetable)) {
		ccr_writetable->commit_transaction(e);
		if (e.exception()) {
			(void)  msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
			    "%s: CCR exception for %s table", LIBSCDPM,
			    DPM_STATUS_TABLE);
			e.clear();
			return (-1);
		}
		CORBA::release(ccr_writetable);
		ccr_writetable = ccr::updatable_table::_nil();
	}

	return (0);
}

//
// Helper function to remove the dpm_status_table.
//
int
dpmccr_remove_status_table(void)
{
	Environment e;

	ASSERT(ccr_dir != NULL);
	ccr_dir->remove_table(DPM_STATUS_TABLE, e);
	if (e.exception() != NULL) {
		os::printf("REmove table failed !!\n");
		return (-1);
	}

	return (0);
}

//
// Helper function to abort the transaction on dpm_status_table.
int
dpmccr_abort_write_operation(void)
{
	Environment e;

	if (!CORBA::is_nil(ccr_writetable)) {
		ccr_writetable->abort_transaction(e);
		if (e.exception()) {
			e.clear();
			return (-1);
		}
		CORBA::release(ccr_writetable);
		ccr_writetable = ccr::updatable_table::_nil();
	}
	return (0);
}

//
// Helper function to copy the pathname and status information
// to the element sequence constructed by construct_list_for_ccr().
//
static int
dpmccr_add_disk_to_seq(
	ccr::element_seq *seq,
	uint_t j,
	disk_status_node_t *disk_node)
{
	char data_buf[MAXNAMELEN];

	os::sprintf(data_buf, "%d", disk_node->status);

	if (((*seq)[j].key =
		os::strdup(disk_node->name)) == (const char *)NULL) {
		    return (-1);
	}
	if (((*seq)[j].data =
		os::strdup(data_buf)) == (const char *)NULL) {
		    return (-1);
	}
	return (0);
}

//
// Helper function called by dpmccr_write_disks_info() to construct
// a list of elements (<devicepath, status> pairs) to be written
// to dpm_status_table.
// If SCDPM_UPDATE_FORCE is set, it constructs the list just from the input list
// without comparing it with the list in dpm_status_table. Force
// option is used by dpmccr_initialize() when the table is created
// and populated for the first time.
//
// If SCDPM_UPDATE_FORCE is not set, it reads the dpm_status_table and compares
// the input list to the list just read. If there is even a single
// change (status change, new disk etc.), it increments dirty. When
// the whole input list has been compared with the list currently in
// the CCR, it constructs a list of elements and returns it to the
// caller. This list contains ALL the entries currently in the CCR
// with any requested changes, AND the new entries, if any.
// dirty is used by the caller to determine if it needs to write the
// list to the table or not.
//
// When SCDPM_UPDATE_INCORE is set, new local disks are added to the local dpm
// daemon incore list (SCDPM_UPDATE_FORCE is not set in this case).
//
ccr::element_seq *
construct_list_for_ccr(
	disk_status_node_t *disk_list,
	int todo, int *dirty)
{
	uint_t count = 0;
	ccr::element_seq *ret_seq = new ccr::element_seq();
	disk_status_node_t *ccr_disk_list = NULL;
	disk_status_node_t *tmp_disk_list, *tmp_ccr_list;
	int error = 0;
	uint_t i = 0;
	uint_t disk_count = 0;
	bool foundit;
	uint_t dpm_disk_count = 0;
	nodeid_t this_nodeid = orb_conf::local_nodeid();
	disk_status_node_t *new_local_disks = NULL;
	bool	local_daemon_running = true;

	//
	// Do not compare the input list with the list currently in the
	// CCR.
	//
	if (todo & SCDPM_UPDATE_FORCE) {
	    tmp_disk_list = disk_list;
	    while (tmp_disk_list != NULL) {
		count++;
		tmp_disk_list = tmp_disk_list->next;
	    }
	    ret_seq->length(count);

	    tmp_disk_list = disk_list;
	    while (tmp_disk_list != NULL) {
		if ((error = dpmccr_add_disk_to_seq(ret_seq,
			i, tmp_disk_list)) != 0) {
			    delete ret_seq;
			    return (NULL);
		}
		i++;
		tmp_disk_list = tmp_disk_list->next;
	    }

	    return (ret_seq);
	}

	//
	// Is local scdpmd deamon already running ?
	//
	if (todo & SCDPM_UPDATE_INCORE) {
		disk_path_monitor::dpm_var dpmd_ref;
		if (get_dpmd_reference(&dpmd_ref, this_nodeid) != DPM_SUCCESS) {
			local_daemon_running = false;
		}
	}

	//
	// Read the list of disks from the CCR. We also get the the
	// count of disks in disk_count.
	//
	if ((error = dpmccr_read_disks_info(
			&ccr_disk_list, &disk_count)) != 0) {
	    delete ret_seq;
	    return (NULL);
	}

	tmp_disk_list = disk_list;
	while (tmp_disk_list != NULL) {
	    tmp_ccr_list = ccr_disk_list;
	    foundit = false;
	    while (tmp_ccr_list != NULL) {
		if (strcmp(tmp_disk_list->name, tmp_ccr_list->name) == 0) {
			//
			// Check if there is a status change. Only Check this
			// when SCDPM_UPDATE_INCORE is not set. See bug 6354440.
			//
			if (!(todo & SCDPM_UPDATE_INCORE) &&
			    tmp_disk_list->status != tmp_ccr_list->status) {
				tmp_ccr_list->status = tmp_disk_list->status;
				(*dirty)++;
			}
		    foundit = true;
		    break;
		}
		tmp_ccr_list = tmp_ccr_list->next;
	    }
	    if (!foundit) {
		error =
		    dpm_append_status_list(&ccr_disk_list, tmp_disk_list->name,
			tmp_disk_list->status);
		if (error != 0) {
			dpm_free_status_list(ccr_disk_list);
			dpm_free_status_list(new_local_disks);
			delete ret_seq;
			return (NULL);
		}

		(*dirty)++;
		//
		// A new disk is being added, increment disk_count.
		//
		disk_count++;
		//
		// Add new local disks to dpm daemon incore list
		//
		if ((todo & SCDPM_UPDATE_INCORE) && local_daemon_running) {
			char *tmp_str = os::strdup(tmp_disk_list->name);
			if (tmp_str == NULL) {
				perror("libscdpm: ");
				dpm_free_status_list(ccr_disk_list);
				dpm_free_status_list(new_local_disks);
				delete ret_seq;
				return (NULL);
			}
			//
			// Extract nodeid from the path.
			// The path is of the form: <nodeid>:/dev/did/..
			// or <nodeid>:/dev/rdsk/...
			//
			char *curr = strtok(tmp_str, ":");
			if (curr == NULL) {
				perror("libscdpm: ");
				delete tmp_str;
				dpm_free_status_list(ccr_disk_list);
				dpm_free_status_list(new_local_disks);
				delete ret_seq;
				return (NULL);
			}
			// Add only new local disks
			if (((uint_t)atoi(curr)) == this_nodeid) {
				error = dpm_append_status_list(&new_local_disks,
					strchr(tmp_disk_list->name, '/'),
					DPM_PATH_MONITORED | DPM_PATH_UNKNOWN);
				if (error != 0) {
					delete tmp_str;
					dpm_free_status_list(ccr_disk_list);
					dpm_free_status_list(new_local_disks);
					delete ret_seq;
					return (NULL);
				}
				dpm_disk_count++;
			}
			delete tmp_str;
		}
	    }
	    tmp_disk_list = tmp_disk_list->next;
	}

	//
	// We want to allocate an element sequence only of something has
	// changed in the list we read from the CCR.
	//
	if (dirty) {
		ret_seq->length(disk_count);
		tmp_ccr_list = ccr_disk_list;
		uint_t j = 0;
		while (tmp_ccr_list != NULL) {
		    if ((error = dpmccr_add_disk_to_seq(ret_seq,
			j, tmp_ccr_list)) != 0) {
			    dpm_free_status_list(ccr_disk_list);
			    delete ret_seq;
			    return (NULL);
		    }
		    j++;
		    tmp_ccr_list = tmp_ccr_list->next;
		}
	}

	//
	// Add new local disks to local dpm daemon incore list
	//
	if (dpm_disk_count > 0) {
		error = libdpm_change_monitoring_status(this_nodeid,
			new_local_disks, dpm_disk_count, NULL, 0);
		if (error != DPM_SUCCESS) {
			// This means that to local scdpmd deamon
			// has a problem. Simply log an error in
			// this case.

			//
			// SCMSGS
			// @explanation
			// scdidadm -r was run and some disk paths may have
			// been added, but DPM daemon on the local node may
			// not have them in its list of paths to be monitored.
			// @user_action
			// Kill and restart the daemon on the local node. If
			// the status of new local disks are not shown by
			// local DPM daemon check if paths are present in the
			// persistent state maintained by the daemon in the
			// CCR. Contact your authorized Sun service provider
			// to determine whether a workaround or patch is
			// available.
			//
			(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
				"%s: Could not call Disk Path Monitoring"
				" daemon to add path(s)",
				LIBSCDPM);
		}
		dpm_free_status_list(new_local_disks);
	}

	dpm_free_status_list(ccr_disk_list);

	return (ret_seq);
}

//
// This function takes an input list and writes it out to the
// dpm_status_table.
// If SCDPM_UPDATE_FORCE is set, it writes out exactly the list given to it.
// If SCDPM_UPDATE_FORCE is not set, it writes out the list only if something
// has changed in the list read from the table. It calls
// construct_list_for_ccr(), to construct the list and to determine
// if the list needs to be written.
// Writes to the dpm_status_table are done batched. If the list
// needs to be written, all elements are first removed and then
// added again.
// Memory for disk_list must be freed by the caller.
// SCDPM_UPDATE_INCORE is set when called from scdidadm -r (SCDPM_UPDATE_FORCE
// is zero in this case). When SCDPM_UPDATE_INCORE is set, new local disks are
// added to the local dpm daemon incore list.
//
int
dpmccr_write_disks_info(
	disk_status_node_t *disk_list,
	int todo)
{
	Environment e;
	ccr::element_seq *seq_to_write;
	int dirty = 0;

	seq_to_write = construct_list_for_ccr(
			disk_list, todo, &dirty);

	if (seq_to_write == NULL) {
	    return (-1);
	}

	//
	// todo & SCDPM_UPDATE_FORCE means we _have_ to write the list out
	// to the CCR.
	// dirty means the list we got back from construct_list_for_ccr()
	// needs to be written back.
	//
	if ((todo & SCDPM_UPDATE_FORCE) || dirty) {
	    ccr_writetable->remove_all_elements(e);
	    if (e.exception()) {
		e.clear();
		delete seq_to_write;
		return (-1);
	    }

	    ccr_writetable->add_elements(*seq_to_write, e);
	    if (e.exception()) {
		e.clear();
		delete seq_to_write;
		return (-1);
	    }
	}

	delete seq_to_write;
	return (0);
}

//
// This function is called from scdidadm -C to write out the
// new list of disks to the CCR. It also updates the in-core
// list of disks maintained by the daemon on the local node.
// Input is the new list of disks. Each entry in the list is
// of the form: <nodeid>:/dev/did/rdsk/dNs0.
// Memory for the input list is freed by the caller.
//
int
dpmccr_cleanup_path(
	disk_status_node_t *disk_list)
{
	disk_status_node_t *ccr_disk_list = NULL;
	disk_status_node_t *tmp_disk_list, *tmp_ccr_list, *prev = NULL;
	disk_status_node_t *cleanup_list = NULL, *delete_ptr;
	int error = 0;
	uint_t disk_count = 0, cleanup_disk_count = 0;
	bool foundit;
	Environment e;
	char *curr;
	nodeid_t this_nodeid = orb_conf::local_nodeid();

	//
	// Read the list of disks from dpm_status_table.
	//
	if ((error = dpmccr_read_disks_info(
		    &ccr_disk_list, &disk_count)) != 0) {
		//
		// ccr_disk_list is freed by
		// dpmccr_read_disks_info in case of an error.
		//
		return (-1);
	}

	//
	// Algorithm:
	// 1. for (each entry in dpm_status_table) {
	// 2.	if (entry is for local node AND
	// 3.	    entry is not in input list) {
	// 4.		append entry to cleanup list;
	// 5.		remove entry from CCR list;
	// 6.	}
	// 7. }
	//
	// Each node cleans up only its own entries.
	// Cleanup list is the list given to the daemon on
	// the local node for it to change the status of
	// paths in the list to "deleted".
	//
	tmp_ccr_list = ccr_disk_list;
	while (tmp_ccr_list != NULL) {
	    char *tmp_str = os::strdup(tmp_ccr_list->name);
	    if (tmp_str == NULL) {
		perror("libscdpm: ");
		dpm_free_status_list(ccr_disk_list);
		return (-1);
	    }
	    //
	    // Extract nodeid from the path read from the CCR.
	    // The path is of the form: <nodeid>:/dev/did/..
	    // or <nodeid>:/dev/rdsk/...
	    //
	    curr = strtok(tmp_str, ":");
	    if (curr == NULL) {
		dpm_free_status_list(ccr_disk_list);
		delete tmp_str;
		return (-1);
	    }
	    //
	    // Make changes to dpm_status_table and the daemon's
	    // in-core state only for paths pertaining to local
	    // node.
	    //
	    if ((uint_t)atoi(curr) == this_nodeid) {
		delete tmp_str;
		tmp_disk_list = disk_list;
		foundit = false;
		while (tmp_disk_list != NULL) {
		    if (strcmp(tmp_disk_list->name, tmp_ccr_list->name)
			    == 0) {
			foundit = true;
			break;
		    }
		    tmp_disk_list = tmp_disk_list->next;
		}
		//
		// Path is not found in the input list. Add it to
		// cleanup list and remove it from the list read
		// from the CCR.
		//
		if (!foundit) {
		    //
		    // Need to remove the nodeid from the pathname
		    // because the daemon maintains its in-core list
		    // without the nodeid prepended.
		    //
		    error = dpm_append_status_list(&cleanup_list,
				strchr(tmp_ccr_list->name, '/'),
				DPM_PATH_DELETE);
		    if (error) {
			dpm_free_status_list(ccr_disk_list);
			return (-1);
		    }
		    if (prev == NULL) {
			ccr_disk_list = tmp_ccr_list->next;
			delete_ptr = tmp_ccr_list;
			tmp_ccr_list = tmp_ccr_list->next;
		    } else {
			prev->next = tmp_ccr_list->next;
			delete_ptr = tmp_ccr_list;
			tmp_ccr_list = tmp_ccr_list->next;
		    }
		    delete (delete_ptr->name);
		    delete (delete_ptr);
		    cleanup_disk_count++;
		    continue;
		}
	    } else {
		delete tmp_str;
	    }
	    prev = tmp_ccr_list;
	    tmp_ccr_list = tmp_ccr_list->next;
	}

	//
	// A non-zero cleanup_disk_count means dpm_status_table
	// and the local daemon's in-core state need to be
	// updated. If cleanup_disk_count is zero, free memory and
	// return.
	//
	if (cleanup_disk_count == 0) {
		dpm_free_status_list(ccr_disk_list);
		return (0);
	}

	//
	// Count the number of disks in the list to be written
	// out to the dpm_status_table.
	//
	uint_t tmp_count = 0;
	tmp_disk_list = ccr_disk_list;
	while (tmp_disk_list != NULL) {
		tmp_count++;
		tmp_disk_list = tmp_disk_list->next;
	}

	//
	// Create an element sequence to be written out to CCR.
	//
	ccr::element_seq *seq_to_write = new ccr::element_seq();
	seq_to_write->length(tmp_count);
	tmp_disk_list = ccr_disk_list;
	uint_t j = 0;
	//
	// Add paths to element sequence.
	//
	while (tmp_disk_list != NULL) {
	    if ((error = dpmccr_add_disk_to_seq(seq_to_write,
			j, tmp_disk_list)) != 0) {
		dpm_free_status_list(ccr_disk_list);
		delete seq_to_write;
		return (-1);
	    }
	    j++;
	    tmp_disk_list = tmp_disk_list->next;
	}

	ccr_writetable->remove_all_elements(e);
	if (e.exception()) {
		(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		"%s: CCR exception for %s table", LIBSCDPM,
			DPM_STATUS_TABLE);
		e.clear();
		dpm_free_status_list(ccr_disk_list);
		delete seq_to_write;
		return (-1);
	}

	ccr_writetable->add_elements(*seq_to_write, e);
	if (e.exception()) {
		(void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
		"%s: CCR exception for %s table", LIBSCDPM,
			DPM_STATUS_TABLE);
		e.clear();
		dpm_free_status_list(ccr_disk_list);
		delete seq_to_write;
		return (-1);
	}

	dpm_free_status_list(ccr_disk_list);
	delete seq_to_write;

	//
	// Update the in-core state of the daemon on the local
	// node.
	//
	disk_path_monitor::dpm_var dpmd_ref;
	error = get_dpmd_reference(&dpmd_ref, this_nodeid);
	if (error != DPM_SUCCESS) {
	    //
	    // SCMSGS
	    // @explanation
	    // scdidadm -C was run and some disk paths may have been cleaned
	    // up, but DPM daemon on the local node may still have them in its
	    // list of paths to be monitored.
	    // @user_action
	    // This message means that the daemon may declare one or more
	    // paths to have failed even though these paths have been removed.
	    // Kill and restart the daemon on the local node. If the status of
	    // one or more paths is shown to be "Failed" although those paths
	    // have been removed, it means that those paths are still present
	    // in the persistent state maintained by the daemon in the CCR.
	    // Contact your authorized Sun service provider to determine
	    // whether a workaround or patch is available.
	    //
	    (void) msg_handle.log(SC_SYSLOG_WARNING, MESSAGE,
	    "%s: Could not call Disk Path Monitoring daemon to"
	    " cleanup path(s)", LIBSCDPM);
	    return (-1);
	}
	ASSERT(dpmd_ref);

	//
	// Create a CORBA sequence of paths whose status needs to be
	// changed by the daemon.
	//
	disk_path_monitor::dpm_disk_status_seq *dpm_cleanup_seq;
	dpm_cleanup_seq = conv_to_status_seq(cleanup_list, cleanup_disk_count);
	if (dpm_cleanup_seq == NULL) {
		return (-1);
	}
	Environment env;
	dpmd_ref->dpm_remove_disks(*dpm_cleanup_seq, env);
	if ((error = get_err_from_env(e)) != DPM_SUCCESS) {
		dpm_free_status_seq(dpm_cleanup_seq);
		env.clear();
		return (-1);
	}

	return (0);
}
