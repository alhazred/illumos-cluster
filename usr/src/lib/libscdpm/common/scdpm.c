/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scdpm.c	1.9	08/05/20 SMI"

/*
 * scdpm.c
 *
 */

#include <stdio.h>
#include <unistd.h>
#include <strings.h>
#include <limits.h>
#include <time.h>
#include <malloc.h>
#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include <devid.h>
#include <netdb.h>
#include <errno.h>
#include <sys/didio.h>
#include <libdid.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/dkio.h>
#include <sys/scsi/scsi.h>
#include <sys/cladm_int.h>
#include <sys/sc_syslog_msg.h>
#include <sys/resource.h>

#include <libscdpm.h>

#define	SEPARATOR	":"
#define	DEFAULT_DIDPATH	"/dev/did/"

static void get_status(dpm_comm_t *, const char *, int *, char);
static disk_status_node_t *get_status_all(dpm_comm_t *, int);
static dpm_core_lst_t *device_set_monitor_core(dpm_core_lst_t *, int);
static void device_free_core(dpm_core_lst_t *);
static char *format_path(did_device_list_t *, char *);
static char *did_fmt_fullname(char *, int, did_device_list_t *);

/*
 * get_status()
 *
 * Get the status of a specific device path.
 *
 * Args:
 *	dpm_comm_t *: pointer to the communication parameters.
 *	const char *:path string pointer to the path.
 *	int *: status
 *		The possible status are DPM_PATH_OK or
 *		DPM_PATH_FAIL
 *	char: do_probe
 *		boolean indicating whether the status should
 *		come from the in-core cache or whether a probe
 *		of the device should be done now.
 *
 */
static void
get_status(
	dpm_comm_t *dpmcom,
	const char *path,
	int *status,
	char do_probe)
{
	dpm_core_lst_t *lstptr;

	(void) pthread_rwlock_rdlock(dpmcom->_lock);

	lstptr = dpmcom->devlst;
	while (lstptr != NULL) {
		(void) pthread_mutex_lock(&(lstptr->_lock));

		if (!DPM_PATH_IS_DELETED(lstptr->mon_status) &&
		    strncmp(path, lstptr->path, _POSIX_PATH_MAX) == 0) {
			if (do_probe) {
				*status = query_device(path, DPM_DEFAULT_RETRY,
				    DPM_DEFAULT_TIMEOUT);
			} else
				*status = lstptr->mon_status;
			(void) pthread_mutex_unlock(&(lstptr->_lock));
			break;
		}

		(void) pthread_mutex_unlock(&(lstptr->_lock));
		lstptr = lstptr->next;
	}

	(void) pthread_rwlock_unlock(dpmcom->_lock);
}

/*
 * get_status_all(dpm_comm_t *, int status)
 *
 * Args:
 *	dpm_comm_t: pointer to the communication parameters.
 *	int: status
 *		The status can be DPM_PATH_OK to only get the
 *		disks where the path is OK. DPM_PATH_FAIL to
 *		get the disks where the path is faild.
 *		DPM_PATH_OK|DPM_PATH_FAIL will get the status
 *		of the good and failed paths. The caller is
 *		responsible for freeing this memory allocated
 *		by this function, by calling
 *		dpm_disk_status_free()
 *
 */
static disk_status_node_t *
get_status_all(dpm_comm_t *dpmcom, int status)
{
	dpm_core_lst_t *lstptr;
	disk_status_node_t *dskptr = NULL;
	int err;

	(void) pthread_rwlock_rdlock(dpmcom->_lock);

	lstptr = dpmcom->devlst;
	while (lstptr != NULL) {
		(void) pthread_mutex_lock(&(lstptr->_lock));

		if (!DPM_PATH_IS_DELETED(lstptr->mon_status) &&
		    status & lstptr->mon_status) {
			err = dpm_append_status_list(&dskptr, lstptr->path,
			    lstptr->mon_status);
			if (err)
				goto error;
		}

		(void) pthread_mutex_unlock(&(lstptr->_lock));
		lstptr = lstptr->next;
	}

	(void) pthread_rwlock_unlock(dpmcom->_lock);
	return (dskptr);
error:
	dpm_disk_status_free(dskptr);
	(void) pthread_rwlock_unlock(dpmcom->_lock);
	return (NULL);
}

/*
 * dpm_disk_status_free(disk_status_node_t *)
 *
 * Arg:
 *	disk_status_node_t *: pointer to the structure containing
 *	the list of the disks we want the status.
 *
 *		Free all the memoy
 */
void
dpm_disk_status_free(disk_status_node_t *dskptr)
{
	disk_status_node_t *ptr, *pptr;

	if (dskptr == NULL)
		return;

	ptr = pptr = dskptr;
	while (ptr != NULL) {
		pptr = ptr;
		ptr = ptr->next;

		if (pptr->name != NULL)
			free(pptr->name);
		free(pptr);
	}
}

/*
 * device_set_monitor_core(dpm_core_lst_t *)
 *
 * Arg:
 *	dpm_core_lst_t *
 *	int status
 *
 *	Set a given status to all the path in registered
 *	in the core list.
 */
static dpm_core_lst_t *
device_set_monitor_core(dpm_core_lst_t *devlst, int status)
{
	dpm_core_lst_t *ptr;

	ptr = devlst;
	while (ptr != NULL) {
		if (DPM_PATH_IS_DELETED(ptr->mon_status)) {
			ptr = ptr->next;
			continue;
		}
		if (DPM_GET_MONITOR(status)
		    != DPM_GET_MONITOR(ptr->mon_status)) {
			ptr->mon_status = status;
			ptr->timestamp = 0; /* Will force a check */
		}
		ptr = ptr->next;
	}

	return (devlst);
}

/*
 * device_free_core(dpm_core_lst_t *)
 *
 * Arg:
 *	dpm_core_lst_t *
 *
 * Return:
 *	void
 *
 *	Free the incore list of devices to monitor.
 */
static void
device_free_core(dpm_core_lst_t *devlst)
{
	dpm_core_lst_t *ptr, *pptr;

	if (devlst == NULL)
		return;

	ptr = pptr = devlst;
	while (ptr != NULL) {
		pptr = ptr;
		ptr = ptr->next;
		free(pptr);
	}
}

int
dpm_append_status_list(disk_status_node_t **dskptr, const char *path,
    int status)
{
	disk_status_node_t *ptr, *new;

	new = (disk_status_node_t *)malloc(sizeof (disk_status_node_t));

	if (new == NULL)
		return (-1);

	new->name = strdup(path);
	new->status = status;
	new->next = NULL;

	/* this is the first element */
	if (*dskptr == NULL) {
		*dskptr = new;
		return (0);
	}

	/*
	 * Append the new element at the end of the list.
	 */
	ptr = *dskptr;
	while (ptr->next != NULL)
		ptr = ptr->next;
	ptr->next = new;

	return (0);
}

/*
 * dpmcom_init()
 *
 *	Create and allocate the memory of the communication
 *	interface data structure.
 *
 *	The caller is responsable for freeing the allocated
 *	memory by calling dpmcom_free()
 */
dpm_comm_t *
dpmcom_init()
{
	pthread_rwlock_t *lock = NULL;
	dpm_comm_t *dpmcom = NULL;

	lock = (pthread_rwlock_t *)malloc(sizeof (pthread_rwlock_t));
	if (lock == NULL)
		goto error;

	dpmcom = (dpm_comm_t *)calloc(1, sizeof (dpm_comm_t));
	if (dpmcom == NULL)
		goto error;

	dpmcom->devlst = NULL;
	dpmcom->_lock = lock;

	if (pthread_rwlock_init(dpmcom->_lock, NULL) != 0)
		goto error;

	return (dpmcom);

error:
	if (lock != NULL)
		free(lock);
	if (dpmcom != NULL)
		free(dpmcom);
	return (NULL);
}

void
dpmcom_free(dpm_comm_t *dpmcom)
{
	if (dpmcom == NULL)
		return;

	if (dpmcom->_lock != NULL)
		free(dpmcom->_lock);

	free(dpmcom);
}


dpm_core_lst_t *
dpm_append(dpm_comm_t *dpmcom, const char *path, int status)
{
	dpm_core_lst_t *devlst;

	if (dpmcom == NULL)
		return (NULL);

	if (path == NULL || *path == NULL)
		return (NULL);

	(void) pthread_rwlock_wrlock(dpmcom->_lock);
	devlst = dpm_device_append_core(dpmcom->devlst,
	    path, status);
	(void) pthread_rwlock_unlock(dpmcom->_lock);

	return (devlst);
}

void
dpm_set_monitor_all(dpm_comm_t *dpmcom, int status)
{
	if (dpmcom == NULL)
		return;

	(void) pthread_rwlock_wrlock(dpmcom->_lock);
	(void) device_set_monitor_core(dpmcom->devlst, status);
	(void) pthread_rwlock_unlock(dpmcom->_lock);
}

/*
 * dpm_free(dpm_core_lst_t)
 *
 * Arg:
 *	dpm_core_lst_t * to the list to free.
 * Retrurn:
 *	void
 *
 * Wrapper to device_free_core. Free the incore list of
 * monitored disks.
 *
 */
void
dpm_free(dpm_comm_t *dpmcom, dpm_core_lst_t *devlst)
{
	if (dpmcom == NULL)
		return;

	(void) pthread_rwlock_wrlock(dpmcom->_lock);
	device_free_core(devlst);
	(void) pthread_rwlock_unlock(dpmcom->_lock);
}


/*
 * dpm_device_append_core(dpm_core_lst_t *, const char *,
 *	int, int)
 *
 * Args:
 *	dpm_core_lst_t * pointer to the begining of the incore
 *	list of managed disks. If this pointer is NULL,
 *	initialise this list.
 *	const char * disk path to add in the monitor list.
 *	int monitored / unmonitored flag.
 *	int status initial status.
 * Retrurn:
 *	dpm_core_lst_t * to the begining of the incore list.
 *
 * Add the disk to the incore list if it is not already
 * regisred, otherwhise update the monitor/unmonitor flag.
 *
 */
dpm_core_lst_t *
dpm_device_append_core(dpm_core_lst_t *devlst, const char *path, int status)
{
	dpm_core_lst_t *ptr, *new;

	/*
	 * If the path is already registered just return.
	 */

	ptr = devlst;
	while (ptr != NULL) {
		/*
		 * This is an empty slot we can re-use it
		 */
		if (DPM_PATH_IS_DELETED(ptr->mon_status) &&
		    !DPM_PATH_IS_DELETED(status)) {
			ptr->timestamp = 0;
			(void) strncpy(ptr->path, path, _POSIX_PATH_MAX);
			ptr->mon_status = status;
			return (devlst);
		}
		/*
		 * Look it this is the path we are looking for
		 */
		if (strcmp(ptr->path, path) == 0) {
			/*
			 * Set timestamp to 0 will force a
			 * check
			 */
			ptr->timestamp = 0;
			if (DPM_GET_MONITOR(status)
			    != DPM_GET_MONITOR(ptr->mon_status))
				ptr->mon_status = status;
			return (devlst);
		}

		ptr = ptr->next;
	}

	/*
	 * The path is not registered or there is no empty slot
	 * allocate and append
	 */
	new = (dpm_core_lst_t *)calloc(1, sizeof (dpm_core_lst_t));
	if (new == NULL)
		return (NULL);

	(void) pthread_mutex_init(&(new->_lock), NULL);
	(void) strncpy(new->path, path, _POSIX_PATH_MAX);
	new->mon_status = status;
	if (devlst == NULL)
		return (new);

	/*
	 * Append at the end of the list.
	 */
	ptr = devlst;
	while (ptr->next != NULL)
		ptr = ptr->next;
	ptr->next = new;

	return (devlst);
}


/*
 * dpm_set_devices(dpm_comm_t *, disk_status_node_t *, int)
 *
 * Args:
 *	dpm_comm_t *: pointer to the communication paramaters.
 *	disk_node_t *: list list of the disks to monitor.
 *     add the disk into the incore list if the disk is not
 *	already registered. Otherwhise set the monitoring status
 *	to monitored or unmonitored
 *
 *	if disk_status_node_t * is null get the list of disks from the
 *	CCR.
 *
 * Return:
 *	0	Operation complete.
 *	-1	on error.
 */
int
dpm_set_devices(dpm_comm_t *dpmcom, disk_status_node_t *dsk, int monitored)
{

	disk_status_node_t *dskptr;

	/*
	 * we know the path just register it and exit.
	 */
	if (dsk != NULL) {
		dskptr = dsk;
		while (dskptr != NULL) {
			dpmcom->devlst = dpm_append(dpmcom, dskptr->name,
			    monitored | DPM_PATH_UNKNOWN);
			dskptr->status = DPM_PATH_REGISTERED;

			dskptr = dskptr->next;
		}
		return (0);
	}

	/*
	 * for all the path already registered.
	 */
	dpm_set_monitor_all(dpmcom, monitored | DPM_PATH_UNKNOWN);

	return (0);
}

/*
 * dpm_get_status(dpm_comm_t *, disk_status_node_t *, char);
 *
 * Args:
 *	dpm_comm_t *: pointer to the communication paramaters.
 *	disk_status_node_t *: pointer to the structure containing
 *	the list of the disks we want the status.
 *	do_probe: boolean of whether a probe should be done else use cache
 *
 * Return:
 *	void
 *
 *	Set the status field of the list pointed by
 *	disk_status_node_t * with the actual status of the
 *	specified path.
 */
void
dpm_get_status(
	dpm_comm_t *dpmcom,
	disk_status_node_t *dsk,
	char do_probe)
{

	disk_status_node_t *dskptr;
	int status;

	dskptr = dsk;
	while (dskptr != NULL) {
		get_status(dpmcom, dskptr->name, &status, do_probe);
		dskptr->status = status;
		dskptr = dskptr->next;
	}
}

/*
 * dpm_get_status_all(dpm_comm_t *, int status)
 *
 * Args:
 *	dpm_comm_t: pointer to the communication parameters.
 *	int: status
 *		The status can be DPM_PATH_OK to only get the
 *		disks where the path is OK. DPM_PATH_FAIL to
 *		get the disks where the path is faild.
 *		DPM_PATH_OK|DPM_PATH_FAIL will get the status
 *		of the good and failed paths.
 * Return:
 *	disk_status_node_t * to a link list.
 *
 * The caller of that function is responsible for freeing
 * the memory by calling dpm_disk_status_free()
 *
 */
disk_status_node_t *
dpm_get_status_all(dpm_comm_t *dpmcom, int status)
{
	return (get_status_all(dpmcom, status));
}


static char *
did_fmt_fullname(char *path, int len, did_device_list_t *dlistp)
{
	if (strcmp(dlistp->device_type->type_name, "disk") == 0) {
		(void) snprintf(path, (size_t)len, "%srdsk/d%d",
		    DEFAULT_DIDPATH, dlistp->instance);
		return (path);
	}
	return (NULL);
}

/*
 * format_path()
 * Returns a formated path for scdpm in the form
 * nodeid:/dev/did/rdsk/d??s0
 * Returns NULL if the path is not a disk
 */
static char *
format_path(did_device_list_t *devptr, char *path)
{
	static char fullpath[MAXNAMELEN+4];
	char tmp[MAXNAMELEN];
	char fullname[MAXNAMELEN];
	char *nodename;
	nodeid_t nodeid;

	if (devptr->target_device_id == NULL)
		return (NULL);	/* This is a cdrom */

	if (did_fmt_fullname(fullname, sizeof (fullname), devptr) == NULL)
		return (NULL);

	(void) strncpy(tmp, path, sizeof (tmp));

	nodename = strtok(tmp, SEPARATOR);
	if (nodename == NULL)
		return (NULL);

	nodeid = (nodeid_t)get_nodeid_by_nodename(nodename);
	if (nodeid == (nodeid_t)-1)
		return (NULL);

	(void) snprintf(fullpath, sizeof (fullpath), "%d%s%ss0", nodeid,
	    SEPARATOR, fullname);


	return (fullpath);
}

/*
 * This function is NOT multi threaded safe due to its calls to
 * dpmccr_begin_write_operation() and dpmccr_end_write_operttion()
 * both of which are not muti threaded safe.
 */

int
scdpm_update_db(int cmd, did_device_list_t *ilist)
{
	did_device_list_t *devptr;
	did_subpath_t *subptr;
	disk_status_node_t *hstatus = NULL;
	int err;
	int pathstatus;
	char *path;

	switch (cmd) {
	case SCDPM_UPDATE_REGISTER:
		pathstatus = DPM_PATH_MONITORED | DPM_PATH_UNKNOWN;
		break;
	case SCDPM_UPDATE_CLEANUP:
		pathstatus = DPM_PATH_DELETE;
		break;
	default:
		return (-1);
	}

	for (devptr = ilist; devptr; devptr = devptr->next) {

		for (subptr = devptr->subpath_listp; subptr;
			subptr = subptr->next) {

			path = format_path(devptr, subptr->device_path);
			if (path == NULL)
				continue;

			err = dpm_append_status_list(&hstatus, path,
				pathstatus);
			if (err == -1) {
				dpm_disk_status_free(hstatus);
				return (err);
			}
		}
	}

	if (hstatus == NULL)	/* nothing to register */
		return (0);

	if ((err = dpmccr_ready()) == -1) {
		dpm_disk_status_free(hstatus);
		return (err);
	}

	if ((err = dpmccr_create_table(DPM_STATUS_TABLE)) == -1) {
		dpm_disk_status_free(hstatus);
		return (err);
	}

	if ((err = dpmccr_begin_write_operation()) == -1) {
		dpm_disk_status_free(hstatus);
		return (err);
	}

	switch (cmd) {
	case SCDPM_UPDATE_REGISTER:
		err = dpmccr_write_disks_info(hstatus, SCDPM_UPDATE_INCORE);
		if (err == -1) {
			(void) dpmccr_abort_write_operation();
			dpm_disk_status_free(hstatus);
			return (err);
		}
		break;
	case SCDPM_UPDATE_CLEANUP:
		err = dpmccr_cleanup_path(hstatus);
		if (err == -1) {
			(void) dpmccr_abort_write_operation();
			dpm_disk_status_free(hstatus);
			return (err);
		}
		break;
	default:
		/* Just to make lint happy ! */
		break;
	}

	err = dpmccr_end_write_operation();
	if (err == -1) {
		(void) dpmccr_abort_write_operation();
		dpm_disk_status_free(hstatus);
		return (err);
	}

	dpm_disk_status_free(hstatus);

	return (0);
}

static int
read_inquiry(int fd, int retry)
{
	int			status;
	int			rc;
	char			read_buf[512];

	do {
		status = read(fd, read_buf, sizeof (read_buf));
		rc = errno;				/*lint !e746 */
		retry--;
	} while ((rc == EAGAIN || rc == EINTR) && status == -1 && retry > 0);

	if (status == -1) {
		return (DPM_PATH_FAIL);
	}

	return (DPM_PATH_OK);
}

static int
scsi_inquiry(int fd, int retry, int timeout)
{
	struct scsi_inquiry	inq;
	struct uscsi_cmd	ucmd;
	union scsi_cdb		cdb;
	int			status;
	int			rc;

#ifdef _FAULT_INJECTION
	if (dpm_inject_fault(FAULTPT_PATH_BAD) != -1)
	    return (DPM_PATH_FAIL);
#endif

	(void) memset((char *)&inq, 0, sizeof (inq));
	(void) memset((char *)&ucmd, 0, sizeof (ucmd));
	(void) memset((char *)&cdb, 0, sizeof (union scsi_cdb));
	cdb.scc_cmd = SCMD_INQUIRY;
	FORMG0COUNT(&cdb, sizeof (inq));
	ucmd.uscsi_cdb = (caddr_t)&cdb;
	ucmd.uscsi_cdblen = CDB_GROUP0;
	ucmd.uscsi_bufaddr = (caddr_t)&inq;
	ucmd.uscsi_buflen = sizeof (inq);
	ucmd.uscsi_flags = USCSI_READ;
	ucmd.uscsi_timeout = (short int) timeout;

	do {
		status = ioctl(fd, USCSICMD, &ucmd);
		rc = errno;
		/*
		 * USCSICMD ioctl is not supported.
		 * Use simple read call to determine the disk status.
		 */
		if (rc == ENOTTY || rc == ENOTSUP) {
			return (read_inquiry(fd, retry));
		}
		retry--;
	} while (status == -1 && retry > 0);

	if (status == -1) {
		return (DPM_PATH_FAIL);
	}

	return (DPM_PATH_OK);
}

int
query_device(const char *path, const int retry, const int timeout)
{
	int fd;
	int status;

	status = DPM_PATH_FAIL;

	fd = open(path, O_RDWR|O_NDELAY);
	if (fd >= 0) {
		status = scsi_inquiry(fd, retry, timeout);
		(void) close(fd);
	}

	return (status);
}
