/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)dpm_impl.cc	1.11	08/06/04 SMI"

#include <sys/os.h>
#include <cmm/cmm_ns.h>
#include <dpm_impl.h>
#include <libscdpm.h>
#include <pthread.h>
#include <sys/stat.h>
#include <fcntl.h>

os::mutex_t	dpm_policy_impl::lck;
os::mutex_t	dpm_impl::_lck;
dpm_impl	*dpm_impl::the_dpm_implp = NULL;
boolean_t	dpm_policy_impl::policy_engine_exists = B_FALSE;

//
// Helper function to convert a CORBA sequence of type
// dpm_disk_status_seq to disk_status_node_t.
//
static disk_status_node_t *
conv_to_status_list(disk_path_monitor::dpm_disk_status_seq dpm_disk_list)
{

	disk_status_node_t *disk_list = NULL;
	disk_status_node_t *tmp_disk_list = NULL;
	disk_status_node_t *disk_list_head = NULL;

	if ((dpm_disk_list->length() == 0)) {
		return (NULL);
	}

	for (uint_t i = 0; i < dpm_disk_list.length(); i++) {
		if ((disk_list =
		    new disk_status_node_t) == NULL) {
			return (NULL);
		}
		if (tmp_disk_list == NULL) {
			disk_list_head = disk_list;
		} else {
			tmp_disk_list->next = disk_list;
		}
		tmp_disk_list = disk_list;
		tmp_disk_list->name = new char[_POSIX_PATH_MAX];
		if ((tmp_disk_list->name =
			os::strdup(dpm_disk_list[i].disk_path)) == NULL) {
			dpm_free_status_list(disk_list_head);
			return (NULL);
		}
		tmp_disk_list->status = dpm_disk_list[i].status;
	}

	if (tmp_disk_list != NULL) {
	    tmp_disk_list->next = NULL;
	}

	return (disk_list_head);
}

//
// Helper function to fill in the status for a list of disks.
//
static void
fill_in_disk_status(disk_status_node_t *disk_list,
	disk_path_monitor::dpm_disk_status_seq
	*dpm_disk_list)
{
	for (uint_t i = 0; i < dpm_disk_list->length(); i++) {
		ASSERT(disk_list);
		(*dpm_disk_list)[i].status = disk_list->status;
		disk_list = disk_list->next;
	}
}

//
// When the DPM daemon comes up on a node, it creates an object
// of type dpm_impl and registers it with the name server as
// DPM.<local-nodeid>.
// The constructor initializes the list and lock pointers for
// the list on the node where this DPM object is being created.
// The list pointer gives access to the local list, which needs to
// be read/manipulated when CLI calls are made. The lock pointer
// provides mutual exclusion.
//
dpm_impl::dpm_impl(dpm_comm_t *dpmcom) :
    auto_reboot_feature((boolean_t)-1),
    ccr_cb_p(nil),
    sysmsg(DPM_SC_SYSLOG_TAG, "", NULL)
{
	ASSERT(dpmcom->devlst != NULL);
	ASSERT(dpmcom->_lock != NULL);
	dpmd_device_list = dpmcom->devlst;
	dpmd_device_list_lock = dpmcom->_lock;
	mynodeid = orb_conf::local_nodeid();
}

dpm_impl::~dpm_impl()
{
	Environment	e;

	// Unregister ccr callback, ignore any errors.
	ccr::readonly_table_var	read_v = get_ccr_readonly(DPM_AUTOREBOOT_TABLE);
	if (!CORBA::is_nil(read_v)) {
		read_v->unregister_callbacks(ccr_cb_p, e);
		e.clear();
	}
	CORBA::release(ccr_cb_p);
	ccr_cb_p = nil;

	dpmd_device_list = NULL;
	dpmd_device_list_lock = NULL;
}

dpm_impl *
dpm_impl::the()
{
	return (the_dpm_implp);
}

// Initialize method called by caller to create a dpm_impl object.
int
dpm_impl::initialize(dpm_comm_t *dpmcom)
{
	_lck.lock();
	if (the_dpm_implp != NULL) {
		// Do nothing, already initialized.
		_lck.unlock();
		return (0);
	}

	the_dpm_implp = new dpm_impl(dpmcom);

	if (the_dpm_implp == NULL) {
		_lck.unlock();
		return (-1);
	}

	(void) the_dpm_implp->register_ccr_callbacks();

	_lck.unlock();

	return (0);
}

// Register ccr callbacks for updates to CCR table DPM_AUTOREBOOT_TABLE
int
dpm_impl::register_ccr_callbacks()
{
	ASSERT(_lck.lock_held());

	dpm_ccr_callback_impl	*cbp = NULL;

	if (!CORBA::is_nil(ccr_cb_p)) {
		// do nothing
		return (0);
	}

	// register CCR callback from table DPM_AUTOREBOOT_TABLE
	Environment	e;
	ccr::readonly_table_var	read_v = get_ccr_readonly(DPM_AUTOREBOOT_TABLE);
	if (CORBA::is_nil(read_v)) {
		goto error_return;
	}

	cbp = new dpm_ccr_callback_impl(this);
	if (cbp == NULL) {
		goto error_return;
	}

	ccr_cb_p = cbp->get_objref();
	read_v->register_callbacks(ccr_cb_p, e);
	if (e.exception()) {
		goto error_return;
	}

	return (0);

error_return:

	CORBA::release(ccr_cb_p);
	ccr_cb_p = nil;
	return (-1);
}

os::sc_syslog_msg &
dpm_impl::msg()
{
	return (sysmsg);
}

// Return a CCR readonly reference to the specified CCR table file.
ccr::readonly_table_ptr
dpm_impl::get_ccr_readonly(char *tablename)
{
	Environment		e;
	ccr::directory_var	dir_v = nil;
	naming::naming_context_var ctx_v = ns::local_nameserver();
	if (CORBA::is_nil(ctx_v)) {
		return (nil);
	}

	CORBA::Object_var	obj_v = ctx_v->resolve("ccr_directory", e);
	if (e.exception()) {
		return (nil);
	}

	dir_v = ccr::directory::_narrow(obj_v);

	ccr::readonly_table_ptr	readonly_p = dir_v->lookup(tablename, e);
	if (e.exception()) {
		return (nil);
	}
	return (readonly_p);
}

void
dpm_impl::_unreferenced(unref_t arg) {
	if (_last_unref(arg)) {
	    delete this;
	    return;
	}
	ASSERT(0);
}

//
// libdpm_change_monitoring_status() in libscdpm calls this
// function to change the monitoring status of a list of disks
// on a node.
//
void
dpm_impl::dpm_change_monitoring_status(
    disk_path_monitor::dpm_disk_status_seq &monitor_list,
    disk_path_monitor::dpm_disk_status_seq &unmonitor_list,
    Environment &env)
{
	disk_status_node_t *mon_list = NULL, *unmon_list = NULL;

	dpm_comm_t *dpmcom = new dpm_comm_t;
	if (dpmcom == NULL) {
	    env.exception(new disk_path_monitor::dpm_error(
		DPM_OUT_OF_MEMORY));
	    return;
	}


	dpmcom->devlst = dpmd_device_list;
	dpmcom->_lock = dpmd_device_list_lock;

	if (monitor_list.length() > 0) {
		if ((mon_list =
		    conv_to_status_list(monitor_list)) == NULL) {
			env.exception(new disk_path_monitor::dpm_error(
			    DPM_OUT_OF_MEMORY));
			return;
		}
		(void) dpm_set_devices(dpmcom, mon_list,
		    DPM_PATH_MONITORED);
		fill_in_disk_status(mon_list, &monitor_list);
	}

	if (unmonitor_list.length() > 0) {
		if ((unmon_list =
		    conv_to_status_list(unmonitor_list)) == NULL) {
			env.exception(new disk_path_monitor::dpm_error(
			    DPM_OUT_OF_MEMORY));
			return;
		}
		(void) dpm_set_devices(dpmcom, unmon_list,
		    DPM_PATH_UNMONITORED);
		fill_in_disk_status(unmon_list, &unmonitor_list);
	}

	delete dpmcom;
	dpm_free_status_list(mon_list);
	dpm_free_status_list(unmon_list);
}

//
// libdpm_change_monitoring_status_all() in libscdpm calls this
// function to change the monitoring status of all disks
// on a node.
//
void
dpm_impl::dpm_change_monitoring_status_all(
    int what,
    Environment &env)
{
	dpm_comm_t *dpmcom = new dpm_comm_t;
	if (dpmcom == NULL) {
	    env.exception(new disk_path_monitor::dpm_error(
		DPM_OUT_OF_MEMORY));
	    return;
	}

	dpmcom->devlst = dpmd_device_list;
	dpmcom->_lock = dpmd_device_list_lock;

	(void) dpm_set_devices(dpmcom, NULL, what);

	delete dpmcom;
}

//
// libdpm_get_monitoring_status() in libscdpm calls this
// function to get the monitoring status of a list of disks
// on a node.
//
void
dpm_impl::dpm_get_monitoring_status(
    disk_path_monitor::dpm_disk_status_seq &disk_list,
    char do_probe,
    Environment &env)
{
	disk_status_node_t *dpm_disk_list;
	if ((dpm_disk_list =
		conv_to_status_list(disk_list)) == NULL) {
		env.exception(new disk_path_monitor::dpm_error(
		    DPM_OUT_OF_MEMORY));
		return;
	}

	dpm_comm_t *dpmcom = new dpm_comm_t;
	if (dpmcom == NULL) {
	    env.exception(new disk_path_monitor::dpm_error(
		DPM_OUT_OF_MEMORY));
	    return;
	}
	dpmcom->devlst = dpmd_device_list;
	dpmcom->_lock = dpmd_device_list_lock;

	dpm_get_status(dpmcom, dpm_disk_list, do_probe);

	uint_t i = 0;
	disk_status_node_t *tmp_list_ptr = dpm_disk_list;
	while (tmp_list_ptr != NULL) {
	    disk_list[i].status = tmp_list_ptr->status;
	    i++;
	    tmp_list_ptr = tmp_list_ptr->next;
	}

	delete dpmcom;
	dpm_free_status_list(dpm_disk_list);
}

//
// libdpm_dpm_get_monitoring_status_all() in libscdpm calls this
// function to get the monitoring status of all the disks
// connected to a node.
//
void
dpm_impl::dpm_get_monitoring_status_all(
    disk_path_monitor::dpm_disk_status_seq_out disk_list,
    Environment &env)
{
	dpm_comm_t *dpmcom = new dpm_comm_t;
	if (dpmcom == NULL) {
	    env.exception(new disk_path_monitor::dpm_error(
		DPM_OUT_OF_MEMORY));
	    return;
	}
	dpmcom->devlst = dpmd_device_list;
	dpmcom->_lock = dpmd_device_list_lock;

	disk_status_node_t *dpm_disk_list =
		dpm_get_status_all(dpmcom,
		    DPM_PATH_MONITORED | DPM_PATH_UNMONITORED);
	if (dpm_disk_list == NULL) {
		env.exception(new disk_path_monitor::dpm_error(
		    DPM_OUT_OF_MEMORY));
		return;
	}

	disk_status_node_t *tmp_list = dpm_disk_list;
	uint_t count = 0;
	while (tmp_list != NULL) {
		tmp_list = tmp_list->next;
		count++;
	}

	tmp_list = dpm_disk_list;
	disk_path_monitor::dpm_disk_status_seq *disk_seq =
		new disk_path_monitor::dpm_disk_status_seq;
	if (disk_seq == NULL) {
	    env.exception(new disk_path_monitor::dpm_error(
		DPM_OUT_OF_MEMORY));
	    goto cleanup;
	}
	disk_seq->length(count);

	for (uint_t i = 0; i < disk_seq->length(); i++) {
		if (((*disk_seq)[i].disk_path =
		    os::strdup(tmp_list->name)) == (const char *)NULL) {
			env.exception(new disk_path_monitor::dpm_error(
			    DPM_OUT_OF_MEMORY));
			goto cleanup;
		}
		(*disk_seq)[i].status = tmp_list->status;
		tmp_list = tmp_list->next;
	}

	disk_list = disk_seq;
cleanup:
	delete dpmcom;
	dpm_free_status_list(dpm_disk_list);
}

//
// libdpm_get_failed_disk_status() in libscdpm calls this
// function to get the list of all failed disks on a node.
//
void
dpm_impl::dpm_get_failed_disk_status(
    disk_path_monitor::dpm_disk_status_seq_out disk_list,
    Environment &env)
{
	dpm_comm_t *dpmcom = new dpm_comm_t;
	if (dpmcom == NULL) {
	    env.exception(new disk_path_monitor::dpm_error(
		DPM_OUT_OF_MEMORY));
	    return;
	}
	dpmcom->devlst = dpmd_device_list;
	dpmcom->_lock = dpmd_device_list_lock;

	disk_status_node_t *dpm_disk_list =
		dpm_get_status_all(dpmcom, DPM_PATH_FAIL);
	if (dpm_disk_list == NULL) {
		env.exception(new disk_path_monitor::dpm_error(
		    DPM_OUT_OF_MEMORY));
		return;
	}

	disk_status_node_t *tmp_list = dpm_disk_list;
	uint_t count = 0;
	while (tmp_list != NULL) {
		tmp_list = tmp_list->next;
		count++;
	}

	tmp_list = dpm_disk_list;
	disk_path_monitor::dpm_disk_status_seq *disk_seq =
		new disk_path_monitor::dpm_disk_status_seq;
	if (disk_seq == NULL) {
	    env.exception(new disk_path_monitor::dpm_error(
		DPM_OUT_OF_MEMORY));
	    goto cleanup;
	}
	disk_seq->length(count);

	for (uint_t i = 0; i < disk_seq->length(); i++) {
	    if (((*disk_seq)[i].disk_path =
		os::strdup(tmp_list->name)) == (const char *)NULL) {
		    env.exception(new disk_path_monitor::dpm_error(
			DPM_OUT_OF_MEMORY));
		goto cleanup;
	    }
	    (*disk_seq)[i].status = tmp_list->status;
	    tmp_list = tmp_list->next;
	}

	disk_list = disk_seq;
cleanup:
	delete dpmcom;
	dpm_free_status_list(dpm_disk_list);
}

void
dpm_impl::dpm_remove_disks(
    disk_path_monitor::dpm_disk_status_seq &disk_list,
    Environment &env)
{
	disk_status_node_t *remove_list = NULL;

	dpm_comm_t *dpmcom = new dpm_comm_t;
	if (dpmcom == NULL) {
	    env.exception(new disk_path_monitor::dpm_error(
		DPM_OUT_OF_MEMORY));
	    return;
	}

	dpmcom->devlst = dpmd_device_list;
	dpmcom->_lock = dpmd_device_list_lock;

	if (disk_list.length() > 0) {
		if ((remove_list =
		    conv_to_status_list(disk_list)) == NULL) {
			env.exception(new disk_path_monitor::dpm_error(
			    DPM_OUT_OF_MEMORY));
			return;
		}
		(void) dpm_set_devices(dpmcom, remove_list,
		    DPM_PATH_DELETE);
		fill_in_disk_status(remove_list, &disk_list);
	}

	delete dpmcom;
	dpm_free_status_list(remove_list);
}

//
// This is called by get_dpmd_reference() in libscdpm
// to find out if the daemon is running on a particular
// node or not. Even when the daemon dies on a node,
// the mapping for its object DPM.<local.nodeid> still
// exists in the Name Server. This method verfies
// whether that mapping is valid or not.
//
bool
dpm_impl::is_alive(
	Environment &)
{
	return (true);
}

boolean_t
dpm_impl::get_auto_reboot_state()
{
	//
	// If the callback doesn't exist, try to register it again. An earlier
	// attempt to register ccr callback could fail, most likely because the
	// table hasn't been created after the installation. The table is
	// created as soon as someone attempts to read from or write to it.
	//
	_lck.lock();
	// Try to register for callback if it hasn't been registered yet.
	int ret = register_ccr_callbacks();
	_lck.unlock();

	//
	// If auto_reboot_feature is invalid or we failed to register ccr
	// callbacks which means that the auto_reboot_feature might be out
	// of date, try to read from the CCR.
	//
	if (auto_reboot_feature == -1 || ret != 0) {
		update_auto_reboot_state();
	}

	return (auto_reboot_feature);
}

// Get auto reboot state for this node from the CCR.
void
dpm_impl::update_auto_reboot_state()
{
	boolean_t autorebootlist[NODEID_MAX];
	(void) libdpm_get_all_auto_reboot_state(autorebootlist);
	//
	// auto_reboot_feature will be -1 if libdpm_get_all_auto_reboot_state()
	// returns an error.
	//
	auto_reboot_feature = autorebootlist[mynodeid - 1];
}

//
// dpm_policy is used to keep track of whether or not a policy engine
// is present and thus overrides the built in auto reboot behaviour
// of scdpmd.
//
dpm_policy_impl::dpm_policy_impl()
{
	lck.lock();
	policy_engine_exists = B_TRUE;
	lck.unlock();
}

dpm_policy_impl::~dpm_policy_impl()
{
}

boolean_t
dpm_policy_impl::policy_engine_registered()
{
	return (policy_engine_exists);
}

void
dpm_policy_impl::_unreferenced(unref_t arg)
{
	lck.lock();
	if (_last_unref(arg)) {
		policy_engine_exists = B_FALSE;
		delete this;
	}
	lck.unlock();
}

// dpm ccr callback methods

dpm_ccr_callback_impl::dpm_ccr_callback_impl(dpm_impl *dpmp) :
    _dpmp(dpmp)
{
}

dpm_ccr_callback_impl::~dpm_ccr_callback_impl()
{
	_dpmp = NULL;
}

void
dpm_ccr_callback_impl::did_update_zc(const char *, const char *,
    ccr::ccr_update_type, Environment &)
{
	ASSERT(0);
}

// did_update, called whenever the registered table is updated.
void
dpm_ccr_callback_impl::did_update(const char *table_name,
    ccr::ccr_update_type, Environment &)
{
	ASSERT(os::strcmp(table_name, DPM_AUTOREBOOT_TABLE) == 0);
	_dpmp->update_auto_reboot_state();
}

void
dpm_ccr_callback_impl::did_recovery_zc(const char *,
    const char *, ccr::ccr_update_type, Environment &)
{
	ASSERT(0);
}

// did_recovery, called after CCR recovery after a node has booted up.
void
dpm_ccr_callback_impl::did_recovery(const char *table_name,
    ccr::ccr_update_type, Environment &)
{
	ASSERT(os::strcmp(table_name, DPM_AUTOREBOOT_TABLE) == 0);
	_dpmp->update_auto_reboot_state();
}

// Unreference method
void
#ifdef DEBUG
dpm_ccr_callback_impl::_unreferenced(unref_t cookie)
#else
dpm_ccr_callback_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));

	delete this;
}
