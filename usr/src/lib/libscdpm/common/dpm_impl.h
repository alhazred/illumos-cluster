/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _DPM_IMPL_H
#define	_DPM_IMPL_H

#pragma ident	"@(#)dpm_impl.h	1.12	08/08/01 SMI"

#include <h/naming.h>
#include <h/ccr.h>
#include <nslib/ns.h>
#include <sys/types.h>
#include <h/sol.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/infrastructure/orb.h>
#include <orb/object/adapter.h>
#include <sys/list_def.h>
#include <h/dpm.h>
#include <pthread.h>
#include <libscdpm.h>

#define	DPM_SC_SYSLOG_TAG	"Cluster.scdpmd"

class dpm_impl : public McServerof<disk_path_monitor::dpm> {

public:

	void _unreferenced(unref_t);

	//
	// IDL definitions.
	//
	void dpm_change_monitoring_status(
		disk_path_monitor::dpm_disk_status_seq &monitor_list,
		disk_path_monitor::dpm_disk_status_seq &unmonitor_list,
		Environment &_environment);

	void dpm_change_monitoring_status_all(
		int what,
		Environment &_environment);

	void dpm_get_monitoring_status(
		disk_path_monitor::dpm_disk_status_seq &disk_list,
		char do_probe,
		Environment &_environment);

	void dpm_get_monitoring_status_all(
		disk_path_monitor::dpm_disk_status_seq_out disk_list,
		Environment &_environment);

	void dpm_get_failed_disk_status(
		disk_path_monitor::dpm_disk_status_seq_out disk_list,
		Environment &_environment);

	void dpm_remove_disks(
		disk_path_monitor::dpm_disk_status_seq &disk_list,
		Environment &_environment);

	bool is_alive(
		Environment &);

	// C++ methods
	static dpm_impl	*dpm_impl::the();
	static int	dpm_impl::initialize(dpm_comm_t *dpmcom);
	int		register_ccr_callbacks();
	boolean_t	get_auto_reboot_state();
	void		update_auto_reboot_state();
	os::sc_syslog_msg	&msg();
	ccr::readonly_table_ptr	dpm_impl::get_ccr_readonly(char *tablename);

private:
	//
	// Pointer to the in-core list of disks maintained by the daemon
	// on the local node.
	//
	dpm_core_lst_t *dpmd_device_list;

	//
	// Lock for the list above.
	//
	pthread_rwlock_t *dpmd_device_list_lock;

	// Boolean indicates if the auto reboot feature is enabled.
	boolean_t		auto_reboot_feature;
	// Callback object to CCR changes
	ccr::callback_ptr	ccr_cb_p;
	nodeid_t		mynodeid;
	os::sc_syslog_msg	sysmsg;

	static os::mutex_t	_lck;
	static	dpm_impl	*the_dpm_implp;

	dpm_impl(dpm_comm_t *dpmcom);
	~dpm_impl();

};

// dpm_policy_impl Engine, Not used currently.
class dpm_policy_impl : public McServerof<disk_path_monitor::dpm_policy> {

public:

	dpm_policy_impl();

	~dpm_policy_impl();

	static	boolean_t	policy_engine_registered();

	void _unreferenced(unref_t);

private:
	static	os::mutex_t	lck;
	static	boolean_t	policy_engine_exists;
};

// dpm CCR callback object
class dpm_ccr_callback_impl : public McServerof<ccr::callback> {
public:
	void	did_update_zc(const char *cluster, const char *table_name,
	    ccr::ccr_update_type, Environment &);

	void	did_update(const char *table_name, ccr::ccr_update_type,
	    Environment &);

	void	did_recovery_zc(const char *cluster, const char *table_name,
	    ccr::ccr_update_type, Environment &);

	void	did_recovery(const char *table_name, ccr::ccr_update_type,
	    Environment &);

	void	_unreferenced(unref_t);

	dpm_ccr_callback_impl(dpm_impl *);
	~dpm_ccr_callback_impl();
private:
	dpm_impl	*_dpmp;
};

#endif	/* _DPM_IMPL_H */
