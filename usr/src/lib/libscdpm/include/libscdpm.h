/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _LIBDPM_H
#define	_LIBDPM_H

#pragma ident	"@(#)libscdpm.h	1.10	08/05/20 SMI"

#include <sys/clconf.h>
#include <unistd.h>
#include <strings.h>
#include <limits.h>
#include <time.h>
#include <pthread.h>
#include <devid.h>
#include <sys/didio.h>
#include <libdid.h>

#ifdef	__cplusplus
extern "C" {
#endif

#define	DPMD_NAME_PREFIX	"DPM."
#define	DPMD_POLICY_PREFIX	"DPM-policy."

#define	DPMD_DIR			"/etc/cluster/scdpm"

#undef TRUE
#define	TRUE    1

#undef FALSE
#define	FALSE   0

#define	DPM_PATH_OK		1	/* path to the disk OK */
#define	DPM_PATH_FAIL		1<<1	/* Path failure */
#define	DPM_PATH_INVALID	1<<2	/* path has become invalid */
#define	DPM_PATH_UNKNOWN	1<<3	/* path status unknown */
#define	DPM_PATH_REGISTERED	1<<4
#define	DPM_PATH_MONITORED	1<<16	/* path monitored */
#define	DPM_PATH_UNMONITORED	1<<17	/* path not monitored */
#define	DPM_PATH_DELETE		1<<18	/* path not monitored */

#define	DPM_STATUS_MASK		0xFFFF0000
#define	DPM_MONITOR_MASK	~DPM_STATUS_MASK

#define	DPM_SET_INIT(x)		((x) = 0)

#define	DPM_SET_STATUS(x, s)	\
	((x) = (DPM_MONITOR_MASK | (x)) & (DPM_STATUS_MASK | (s)))

#define	DPM_GET_STATUS(x)	((x) & ~DPM_STATUS_MASK)

#define	DPM_SET_MONITOR(x, s)	\
	((x) = (DPM_MONITOR_MASK | (s)) & (DPM_STATUS_MASK | (x)))

#define	DPM_GET_MONITOR(x)	((x) & ~DPM_MONITOR_MASK)


#define	DPM_PATH_IS_OK(x)		(x & DPM_PATH_OK)
#define	DPM_PATH_IS_FAIL(x)		(x & DPM_PATH_FAIL)
#define	DPM_PATH_IS_INVALID(x)		(x & DPM_PATH_INVALID)
#define	DPM_PATH_IS_UNKNOWN(x)		(x & DPM_PATH_UNKNOWN)
#define	DPM_PATH_IS_REGISTERED(x)	(x & DPM_PATH_REGISTERED)
#define	DPM_PATH_IS_MONITORED(x)	(x & DPM_PATH_MONITORED)
#define	DPM_PATH_IS_UNMONITORED(x)	(x & DPM_PATH_UNMONITORED)
#define	DPM_PATH_IS_DELETED(x)		(x & DPM_PATH_DELETE)

#define	SCDPM_UPDATE_REGISTER	1
#define	SCDPM_UPDATE_CLEANUP	2

#define	SCDPM_UPDATE_FORCE	1
#define	SCDPM_UPDATE_INCORE	2

/* Errors reported by the DPM functions call */
#define	DPM_SUCCESS		(0)	/* Operation successful no error */
#define	DPM_NO_DAEMON		(1)	/* No daemon present */
#define	DPM_ENOENT		(2)	/* No such file or directory */
#define	DPM_ENOMEM		(3)	/* Cannot allocate memory */
#define	DPM_EEXIST		(4)	/* Disk_path allready registered */
#define	DPM_EACCES		(5)	/* Permission denied */
#define	DPM_DAEMON_NOT_FOUND	(7)
#define	DPM_OUT_OF_MEMORY	(8)	/* Could not allocate memory */
#define	DPM_ERR_COMM		(9)	/* Communication Error */
#define	DPM_CCR_ACCESS_ERROR	(10)
#define	DPM_CCR_FORMAT_ERROR	(11)
#define	DPM_UNKNOWN_ERROR	(12)

#define	LIBSCDPM		"libscdpm"
#define	DPM_STATUS_TABLE	"dpm_status_table"
#define	DPM_AUTOREBOOT_TABLE	"dpm_autoreboot_table"
#define	DPM_AUTOREBOOT_KEY	"autoreboot"

/* Used for specifying the actions of monitor and unmonitor */
/* Used for specifying the fault point to trigger */
#define	FAULTPT_PATH_BAD	(0)

#define	ALL_NODEIDS	0

#define	DPM_DEFAULT_TIMEOUT	5	/* ping timeout (seconds) */
#define	DPM_DEFAULT_INTERVAL	600	/* ping interval (seconds) */
#define	DPM_DEFAULT_RETRY	3	/* number of retry */

typedef struct DPM_CORE_LST {
	char path[_POSIX_PATH_MAX];
	int mon_status;
	time_t timestamp;
	pthread_mutex_t _lock;
	struct DPM_CORE_LST *next;
} dpm_core_lst_t;

typedef struct DPM_COMM {
	dpm_core_lst_t *devlst;
	pthread_rwlock_t *_lock;
} dpm_comm_t;


typedef struct DISK_NODE {
	char *name;
	struct DISK_NODE *next;
} disk_node_t;

typedef struct DISK_STATUS_NODE {
	char *name;
	int status;
	struct DISK_STATUS_NODE *next;
} disk_status_node_t;

int
register_dpm_daemon(
	dpm_comm_t *dpmcom);

void
unregister_dpm_daemon(void);

int
dpm_initialize(void);

int
libdpm_change_monitoring_status(
	nodeid_t nodeid, disk_status_node_t *monitor_list,
	uint_t mon_list_count, disk_status_node_t
	*unmonitor_list, uint_t unmon_list_count);

int
libdpm_change_monitoring_status_all(
	nodeid_t nodeid,
	int command);

int
libdpm_get_monitoring_status(
	nodeid_t nodeid,
	disk_status_node_t *disk_list,
	uint_t count,
	char do_probe);

int
libdpm_get_monitoring_status_all(
	nodeid_t nodeid,
	disk_status_node_t **disk_list);

int
libdpm_get_failed_disk_status(
	nodeid_t nodeid,
	disk_status_node_t **disk_list);

int
libdpm_remove_disks(
	nodeid_t nodeid, disk_status_node_t *disk_list,
	uint_t count);

/* Reads into "state" whether or not the auto reboot feature is enabled */
int
libdpm_get_all_auto_reboot_state(boolean_t *autorebootlist);

int
libdpm_get_local_node_auto_reboot_state(boolean_t *state);

/* Sets whether or not the auto reboot feature should be enabled */
int
libdpm_set_auto_reboot_feature_state(nodeid_t nodeid, boolean_t state);

/* Check if the policy engine exists */
boolean_t
libdpm_policy_engine_registered();

int
dpm_inject_fault(
	int which_fault_pt);

void dpm_free_status_list(disk_status_node_t *disk_list);

int get_device_type(const char *);

dpm_core_lst_t *dpm_append(dpm_comm_t *, const char *, int);
void dpm_set_monitor_all(dpm_comm_t *, int);
void dpm_free(dpm_comm_t *, dpm_core_lst_t *);

dpm_core_lst_t *dpm_device_append_core(dpm_core_lst_t *,
    const char *, int);
void dpm_disk_status_free(disk_status_node_t *);

int is_did_connected(did_device_list_t *, const char *);

dpm_comm_t *dpmcom_init(void);
void dpmcom_free(dpm_comm_t *dpmcom);

int dpm_set_devices(dpm_comm_t *, disk_status_node_t *, int);
void dpm_get_status(dpm_comm_t *, disk_status_node_t *, char);
disk_status_node_t *dpm_get_status_all(dpm_comm_t *, int);

int dpm_append_status_list(disk_status_node_t **, const char *, int);

int query_device(const char *, const int, const int);

/*
 * Interfaces for DPM-CCR interaction
 */
int dpmccr_ready(void);
int dpmccr_begin_write_operation(void);
int dpmccr_end_write_operation(void);
int dpmccr_abort_write_operation(void);

int dpmccr_initialize(disk_status_node_t **);
int dpmccr_create_table(const char *);
int dpmccr_remove_status_table(void);
int get_nodeid_by_nodename(char *);

int dpmccr_read_disks_info(disk_status_node_t **, uint_t *);
int dpmccr_write_disks_info(disk_status_node_t *, int);
disk_status_node_t *dpmccr_read_did_inst_table(void);
int dpmccr_cleanup_path(disk_status_node_t *);

int scdpm_update_db(int cmd, did_device_list_t *);

#ifdef	__cplusplus
}
#endif

#endif	/* _LIBDPM_H */
