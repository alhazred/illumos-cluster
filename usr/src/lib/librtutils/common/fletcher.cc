//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)fletcher.cc	1.1	09/01/21 SMI"

#include "librtutils.h"
#include <string.h>
#include <stdlib.h>

//
// fletcher_cksum
//
// Calculates 128 bit fletcher check sum for the data in buf whose
// size is 'length' bytes. The checksum is passed back in cksum.
//
void
fletcher_cksum(const void *buf, size_t length, fletcher_cksum_t *cksum)
{
	uint64_t a, b;
	size_t index, last_index;
	uint32_t *buffer = (uint32_t *)buf;

	//
	// We use two 64 bit accumulators to generate a 128 bit checksum
	// First calculate checksum till 4-bytes aligned buffer.
	//
	for (index = 0, a = b = 0L, last_index = length/sizeof (uint32_t);
	    index < last_index; index++) {
		a += buffer[index];
		b += a;
	}

	if (length % sizeof (uint32_t) != 0) {
		//
		// Calculate check sum for non-aligned remaining bytes after
		// padding with zero.
		//
		uint32_t trail_buf = 0;

		(void) memcpy(&trail_buf,
		    (char *)buf + index * sizeof (uint32_t),
		    length % sizeof (uint32_t));

		a += trail_buf;
		b += a;
	}

	cksum->word[0] = a;
	cksum->word[1] = b;
}
