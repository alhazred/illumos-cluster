/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _LIBRTUTILS_H
#define	_LIBRTUTILS_H

#pragma ident	"@(#)librtutils.h	1.1	09/01/21 SMI"

#include <limits.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/sysmacros.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct fletcher_cksum {
	uint64_t word[2];
} fletcher_cksum_t;

#define	CKSUM_SIZE	(sizeof (uint64_t) * 2)
#define	CKSUM_SIZE_ENCODED	(CKSUM_SIZE * 2)

#define	ZCACHE_SIZE		(sizeof (size_t))
#define	ZCACHE_SIZE_ENCODED	(ZCACHE_SIZE * 2)

/* Return 1 if both checksum's are same, '0' otherwise */
#define	CKSUM_EQUAL(a, b)	\
	(((a).word[0] == (b).word[0] &&	\
	(b).word[1] == (b).word[1]) ? 1 : 0)

#define	ERR_MSG_SIZE	1024

typedef struct err_msg {
	char message[ERR_MSG_SIZE];
} err_msg_t;

/*
 * The directory for cachefiles of the zpools that are configured in
 * HAStoragePlus resources. This will be the location where HAStoragePlus
 * callback method creates the cachefile.
 * The plugin will pickup these files and will synchronize to other cluster
 * nodes when it detect the changes in the configuration.
 * NOTE: Location of HAStoargePlus cachefile directory should be consistent with
 * this plugin and HAStoragePlus callback methods.
 */

#define	HASP_ZPOOLS_CACHEFILES_DIR	"/var/cluster/run/HAStoragePlus/zfs"

#define	CACHEFILE_TRAIL_KEYWORD		"cachefile"

#define	IMPORT_CACHEFILE_TRAIL_KEYWORD	"cachefile.import"

/*
 * CCR keys associated for zpool cachefile
 * - The fletcher checksum of the cachefile
 * - The search directory where the vdevs of the cachefile searched
 * - The size of the cachefile
 * - The (lzjb) compressed base64 enocded contents of the cachefile
 */
#define	CACHE_CKSUM_KEY	"zpool_cache_cksum"
#define	SEARCH_DIR_KEY	"zpool_search_dir"
#define	CACHE_SIZE_KEY	"zpool_cache_size"
#define	CDATA_KEY	"zpool_cache_cdata"

/*
 * General API
 */
char *get_file_contents(const char *file_name, size_t *file_size,
    err_msg_t *err);

int write_contents_to_file(const void *data, size_t data_len,
    const char *target_file, err_msg_t *err);

/*
 * HAStoragePlus API.
 */

boolean_t is_cachefile_met_compression_criteria(size_t cachefile_size);

void get_pool_cachefile_path(const char *pool_name,
    char *path, uint_t max_path_len);

void get_pool_import_cachefile_path(const char *pool_name,
    char *path, uint_t max_path_len);

void get_pool_ccr_tabname(const char *pool_name,
    char *tabname, uint_t max_tabname_len);

/*
 * Checksum (Fletcher) API
 */
void fletcher_cksum(const void *buf, size_t length,
    fletcher_cksum_t *cksum);

/*
 * zlib compression API
 */

int z_compress(void *dst, size_t *dstlen, const void *src, size_t srclen);

int z_uncompress(void *dst, size_t *dstlen, const void *src, size_t srclen);

/*
 * Base64 encoding and decoding API.
 */
uchar_t *base64_encode(const uchar_t *input_buffer, size_t input_buffer_size,
    size_t  *encoded_buffer_size, err_msg_t *err);

uchar_t *base64_decode(
    const uchar_t *encoded_buffer, size_t encoded_buffer_size,
    size_t *decoded_buffer_size, err_msg_t *err);

/*
 * CCR access API.
 */

int create_pool_table(const char *poolname, err_msg_t *err);

int check_pool_table(const char *poolname, boolean_t *table_exists,
    err_msg_t *err);

int remove_pool_table(const char *poolname, err_msg_t *err);

int cksum_pool(const char *poolname, fletcher_cksum_t *cksum, err_msg_t *err);

void *convert_compressed_encoded_data_to_data(
    const void *compressed_enoded_data, size_t compressed_enoded_data_len,
    size_t data_len, err_msg_t *err);

void *convert_data_to_compressed_encoded_data(const void *data, size_t data_len,
    size_t *compressed_encoded_data_len, err_msg_t *err);

int read_zcache_from_ccr(const char *poolname, const char *file_name,
    char **search_dir_key_value, err_msg_t *err);

int dump_zcache_to_ccr(const char *poolname, const char *file_name,
    const char *search_dir_key_value, err_msg_t *err);

#ifdef __cplusplus
}
#endif

#endif /* _LIBRTUTILS_H */
