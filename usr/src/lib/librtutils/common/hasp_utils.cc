//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)hasp_utils.cc	1.1	09/01/21 SMI"

#include "librtutils.h"
#include <stdio.h>


//
// Get the location of cachefile for a given pool name.
//
void
get_pool_cachefile_path(const char *pool_name, char *path, uint_t max_path_len)
{
	(void) snprintf(path, max_path_len, "%s/%s.%s",
	    HASP_ZPOOLS_CACHEFILES_DIR, pool_name, CACHEFILE_TRAIL_KEYWORD);
}

//
// Get the location of import cachefile for a given pool name.
//
void
get_pool_import_cachefile_path(const char *pool_name,
    char *import_path, uint_t max_path_len)
{
	(void) snprintf(import_path, max_path_len, "%s/%s.%s",
	    HASP_ZPOOLS_CACHEFILES_DIR, pool_name,
	    IMPORT_CACHEFILE_TRAIL_KEYWORD);
}

//
// Get the CCR table name where the cachefile contents are stored
// for given pool name.
//
void
get_pool_ccr_tabname(const char *pool_name, char *tabname,
    uint_t max_tabname_len)
{
	(void) snprintf(tabname, max_tabname_len, "%s.%s",
	    pool_name, CACHEFILE_TRAIL_KEYWORD);
}

#define	MIN_CACHEFILE_SIZE_TO_COMPRESS	10*1024		// 10 KB
boolean_t
is_cachefile_met_compression_criteria(size_t cachefile_size)
{
	return ((cachefile_size >
	    (MIN_CACHEFILE_SIZE_TO_COMPRESS))? B_TRUE : B_FALSE);
}
