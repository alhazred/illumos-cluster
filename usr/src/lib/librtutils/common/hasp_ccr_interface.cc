//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)hasp_ccr_interface.cc	1.1	09/01/21 SMI"

#include "librtutils.h"

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/param.h>
#include <errno.h>
#include <string.h>
#include <syslog.h>
#include <sys/time.h>
#include <stdarg.h>

#include <orb/invo/corba.h>
#include <nslib/ns.h>
#include <h/ccr.h>
#include <h/ccr_data.h>
#include <h/naming.h>

// Global CCR handle initialized once in the libraries life time
static	naming::naming_context_ptr ctxp = naming::naming_context::_nil();
static  ccr::directory_ptr ccr_dir = ccr::directory::_nil();

#define	CCR_RETRY_COUNT	15
#define	CCR_RETRY_WAIT	2

//
// is_ccr_ready
//
// Checks if the reference to CCR is already ready. Initialize the reference to
// CCR directory service if not ready.
//
// Return 'true' on success and 'false' on failure.
//
static bool
is_ccr_ready(err_msg_t *err)
{
	Environment env;

	if (!CORBA::is_nil(ccr_dir)) {
		return (true);
	}

	if (ORB::initialize() != 0) {
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "Internal error : failed to initialize ORB.");
		return (false);
	}

	ctxp = ns::local_nameserver();
	if (CORBA::is_nil(ctxp)) {
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "Internal error : failed to obtain name server reference.");
		return (false);
	}

	CORBA::Object_var obj = ctxp->resolve("ccr_directory", env);
	if (env.exception()) {
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "Internal error : Error while accessing CCR.");
		return (false);
	}

	ccr_dir = ccr::directory::_narrow(obj);
	return (true);
}

//
// create_pool_table (API)
//
// Create new CCR table for pool, and create keys with initial values.
// This returns simply if the table and keys already exists.
//
// Return '0' on success and '1' on failure.
//
int
create_pool_table(const char *poolname, err_msg_t *err)
{
	if (!is_ccr_ready(err)) {
		return (1);
	}

	Environment e;
	CORBA::Exception *ex;
	char	tbname[MAXPATHLEN];

	get_pool_ccr_tabname(poolname, tbname, MAXPATHLEN);

	// Lookup CCR table for given pool
	ccr::readonly_table_var pool_table_v = ccr_dir->lookup(tbname, e);
	if ((ex = e.exception()) != NULL) {
		// An expception other than 'no_such_table is error
		if (!ccr::no_such_table::_exnarrow(ex)) {
			(void) snprintf(err->message, ERR_MSG_SIZE,
			    "Error (%s) while lookup for CCR table for pool %s",
			    ex->_name(), poolname);
			return (1);
		}

		e.clear();
		// Table does not exists create one
		ccr_dir->create_table(tbname, e);
		if ((ex = e.exception()) != NULL) {
			(void) snprintf(err->message, ERR_MSG_SIZE,
			    "Error (%s) while creating CCR table for pool %s",
			    ex->_name(), poolname);
			return (1);
		}

		pool_table_v = ccr_dir->lookup(tbname, e);
		if ((ex = e.exception()) != NULL) {
			(void) snprintf(err->message, ERR_MSG_SIZE,
			    "Error (%s) while reading CCR table for pool %s",
			    ex->_name(), poolname);
			return (1);
		}
	}

	//
	// Check for keys existence in the table also to decide that table for
	// the pool exists. This is to cover the case when node is down
	// after table creation is successful.
	//
	int count = pool_table_v->get_num_elements(e);
	if ((ex = e.exception()) != NULL) {
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "Error (%s) while finding number of keys in CCR table "
		    "for pool %s",
		    ex->_name(), poolname);
		return (1);
	}
	if (count >= 1) {
		// The required keys in the table are created in single
		// transaction. Hence having more than on key can be
		// assumed that all keys exists.
		return (0);
	}

	//
	// Create an updatable_table object for the table to add keys to the
	// table.
	//
	ccr::updatable_table_var pool_update_table_v;
	pool_update_table_v = ccr_dir->begin_transaction(tbname, e);
	if ((ex = e.exception()) != NULL) {
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "Error (%s) opening created CCR table for pool %s",
		    ex->_name(), poolname);
		return (1);
	}

	// Add all the keys with appropriate initial values.

	char    cksum_encoded[CKSUM_SIZE_ENCODED + 1];
	(void) sprintf(cksum_encoded, "%016llx%016llx", 0ll, 0ll);
	pool_update_table_v->add_element(CACHE_CKSUM_KEY, cksum_encoded, e);
	if ((ex = e.exception()) != NULL) {
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "Error (%s) in creating key %s in CCR table for pool %s",
		    ex->_name(), CACHE_CKSUM_KEY, poolname);
		pool_update_table_v->abort_transaction(e);
		return (1);
	}

	pool_update_table_v->add_element(SEARCH_DIR_KEY, "", e);
	if ((ex = e.exception()) != NULL) {
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "Error (%s) in creating key %s in CCR table for pool %s",
		    ex->_name(), CDATA_KEY, poolname);
		pool_update_table_v->abort_transaction(e);
		return (1);
	}

	char	zcache_len_encoded[ZCACHE_SIZE_ENCODED + 1];
	(void) sprintf(zcache_len_encoded, "%08lx", 0l);
	pool_update_table_v->add_element(CACHE_SIZE_KEY, zcache_len_encoded, e);
	if ((ex = e.exception()) != NULL) {
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "Error (%s) in creating key %s in CCR table for pool %s",
		    ex->_name(), CACHE_SIZE_KEY, poolname);
		pool_update_table_v->abort_transaction(e);
		return (1);
	}

	pool_update_table_v->add_element(CDATA_KEY, "", e);
	if ((ex = e.exception()) != NULL) {
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "Error (%s) in creating key %s in CCR table for pool %s",
		    ex->_name(), CDATA_KEY, poolname);
		pool_update_table_v->abort_transaction(e);
		return (1);
	}

	pool_update_table_v->commit_transaction(e);
	if ((ex = e.exception()) != NULL) {
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "Error (%s) while commiting transaction after "
		    "adding cksum and data keys in CCR table for pool %s",
		    ex->_name(), poolname);
		pool_update_table_v->abort_transaction(e);
		return (1);
	}
	return (0);
}

//
// check_pool_table (API)
//
// Checks the existence of pool table and also keys.
// The status is retuned in 'table_exists'.
//
// Return '0' on success and '1' on failure.
//
int
check_pool_table(const char *poolname, boolean_t *table_exists, err_msg_t *err)
{
	*table_exists = B_TRUE;
	int	retry_count = CCR_RETRY_COUNT;
	int	count;

	if (!is_ccr_ready(err)) {
		return (1);
	}

	Environment e;
	CORBA::Exception *ex;
	char	tbname[MAXPATHLEN];

	get_pool_ccr_tabname(poolname, tbname, MAXPATHLEN);

	while (1) {
		// Lookup CCR table for given pool
		ccr::readonly_table_var pool_table_v =
		    ccr_dir->lookup(tbname, e);

		if ((ex = e.exception()) != NULL) {
			// Table does exists.
			if (ccr::no_such_table::_exnarrow(ex)) {
				*table_exists = B_FALSE;
				return (0);
			}
			(void) snprintf(err->message, ERR_MSG_SIZE,
			    "Error (%s) while reading CCR table for pool %s",
			    ex->_name(), poolname);
			return (1);
		}

		//
		// Check for keys existence in the table also to cover the case
		// where the can go down after table creation.
		//
		count = pool_table_v->get_num_elements(e);
		if ((ex = e.exception()) == NULL) {
			// Read the successfully. Bail out
			break;
		}
		// Table is modified by someone, re-read the table for latest
		if (ccr::table_modified::_exnarrow(ex) && retry_count--) {
			// Table is modified retry after wait
			e.clear();
			(void) sleep(CCR_RETRY_WAIT);
			continue;
		}
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "Error (%s) while finding number of keys in "
		    "CCR table for pool %s",
		    ex->_name(), poolname);
		return (1);
	}
	if (count <= 0) {
		// The required keys in the table are created in single
		// transaction which will have atleast one key.
		*table_exists = B_FALSE;
	}
	return (0);
}

//
// remove_pool_table (API)
//
// Create new CCR table for pool, and create keys with initial values.
// This returns simply if the table and keys already exists.
//
// Return '0' on success and '1' on failure.
//
int
remove_pool_table(const char *poolname, err_msg_t *err)
{
	if (!is_ccr_ready(err)) {
		return (1);
	}

	Environment e;
	CORBA::Exception *ex;
	char	tbname[MAXPATHLEN];

	get_pool_ccr_tabname(poolname, tbname, MAXPATHLEN);

	// Lookup CCR table for given pool
	ccr::readonly_table_var pool_table_v = ccr_dir->lookup(tbname, e);
	if ((ex = e.exception()) != NULL) {
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "Error (%s) while lookup for CCR table for pool %s",
		    ex->_name(), poolname);
		return (1);
	}

	// Table does not exists create one
	ccr_dir->remove_table(tbname, e);
	if ((ex = e.exception()) != NULL) {
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "Error (%s) while creating CCR table for pool %s",
		    ex->_name(), poolname);
		return (1);
	}
	return (0);
}

//
// cksum_pool (API)
//
// Read the checksum of the given pool stored in CCR.
//
// Return '0' on success and '1' on failure.
//
int
cksum_pool(const char *poolname, fletcher_cksum_t *cksum, err_msg_t *err)
{
	Environment e;
	CORBA::Exception *ex;
	char	*cksum_string;
	int	retry_count = CCR_RETRY_COUNT;
	char	tbname[MAXPATHLEN];
	ccr::readonly_table_var pool_table_v;

	if (!is_ccr_ready(err)) {
		return (1);
	}

	get_pool_ccr_tabname(poolname, tbname, MAXPATHLEN);

	while (1) {

		// Lookup CCR table for given pool
		pool_table_v = ccr_dir->lookup(tbname, e);
		if ((ex = e.exception()) != NULL) {
			(void) snprintf(err->message, ERR_MSG_SIZE,
			    "Error (%s) while reading CCR entry for pool %s",
			    ex->_name(), poolname);
			return (1);
		}

		// We got a reference to the table, now read the checksum
		cksum_string = pool_table_v->query_element(CACHE_CKSUM_KEY, e);
		if ((ex = e.exception()) == NULL) {
			// Read the checksum key successfully. Bail out
			break;
		}
		// Table is modified by someone, re-read the table for latest
		// checksum
		if (ccr::table_modified::_exnarrow(ex) && retry_count--) {
			// Table is modified retry after wait
			e.clear();
			(void) sleep(CCR_RETRY_WAIT);
			continue;
		}
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "Error (%s) while reading checksum key for pool %s",
		    ex->_name(), poolname);
		return (1);
	}

	//
	// Convert CCR cksum key to a fletcher_cksum_t type.
	//
	(void) sscanf(cksum_string,
	    "%016llx%016llx", &cksum->word[0], &cksum->word[1]);

	delete [] cksum_string;

	return (0);
}

//
// convert_compressed_encoded_data_to_data (API)
//
// Converts the data which is in compressed and base 64 encoded format to
// normal data.
// Returns a valid pointer to converted data on success and NULL on failure.
// Caller is responsible to free the returned buffer.
//
void *
convert_compressed_encoded_data_to_data(
    const void *compressed_enoded_data, size_t compressed_enoded_data_len,
    size_t data_len, err_msg_t *err)
{
	int	error;

	// Decode the contents first
	void	*compressed_data;
	size_t	compressed_data_len;
	compressed_data = base64_decode((uchar_t *)compressed_enoded_data,
	    compressed_enoded_data_len, &compressed_data_len, err);
	if (compressed_data == NULL) {
		// Decoding failed
		return (NULL);
	}

	// Uncompress the contents now
	void	*data;
	data = malloc(data_len);
	if (data == NULL) {
		error = errno;
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "%s", strerror(error));
		free(compressed_data);
		return (NULL);
	}
	size_t	uncompressed_len = data_len;
	if (z_uncompress(data, &uncompressed_len,
	    (void *)compressed_data, compressed_data_len) != 0) {
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "Uncompression failed.");
		free(compressed_data);
		free(data);
		return (NULL);
	}
	free(compressed_data);

	// This shouldn't happen but let us do sanity check.
	if (uncompressed_len != data_len) {
		// The uncompressed length didn't match with original cachefile
		// size.
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "The uncompressed contents does not match with "
		    "original data size.");
		return (NULL);
	}
	return (data);
}

//
// convert_data_to_compressed_encoded_data (API)
//
// Converts the normal data to compressed and base 64 encoded format.
//
// Returns a valid pointer to converted data on success and NULL on failure.
// Caller is responsible to free the returned buffer.
//
void *
convert_data_to_compressed_encoded_data(const void *data, size_t data_len,
    size_t *compressed_encoded_data_len, err_msg_t *err)
{
	// Compress the data first
	void *compressed_data;

	// The compression contents couldn't be more than actual data size
	compressed_data = malloc(data_len);
	if (compressed_data == NULL) {
		int error = errno;
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "%s", strerror(error));
		return (NULL);
	}

	size_t compress_data_len = data_len;
	if (z_compress((void *)compressed_data, &compress_data_len,
	    (void *)data, data_len) != 0) {
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "Compress failed.");
		free(compressed_data);
		return (NULL);
	}

	// Base 64 encode the compressed contents now

	void	*compressed_encoded_data;

	compressed_encoded_data = base64_encode((uchar_t *)compressed_data,
	    compress_data_len, compressed_encoded_data_len, err);
	free(compressed_data);
	return (compressed_encoded_data);
}

//
// dump_zcache_to_ccr (API)
//
// Updates the CCR with cachefile contents from the specified file after
// compressing(if met compression criteria) and encoding the contents.
// It also ensures that updating cachefile contents is different from contents
// in the CCR by verifying the checksums.
//
// The SEARCH_DIR_KEY (value presented in 'search_dir_value') will be optional
// as the key value is not known for zpool_cachefile_plugin. The
// zpool_cachefile_plugin need not update that key as it will not be changed
// by the plugin after HAStoragePlus imports the pool.
// This key value is only used by HAStoragePlus to validate whether the
// cachefile contents conatins the devices that are searched in that directory.
//
// NOTE: This is API which might get called by different threads
// (zpool_cachefile_plugin and HAStoragePlus) to update the CCR
// with the same cachefile. This function takes care to avoid updating twice,
// by comparing the checksum of file contents with CCR checksum after the
// update transaction starts.
//
// Return '0' on success and '1' on failure.
// The actual error message information will be in 'err'.
//
int
dump_zcache_to_ccr(const char *poolname, const char *zcachefile_name,
    const char *search_dir_value, err_msg_t *err)
{

	if (!is_ccr_ready(err)) {
		return (1);
	}

	fletcher_cksum_t zcache_cksum;
	fletcher_cksum_t ccr_cksum;
	char	*zcache_data;
	size_t	zcache_data_len;
	char	tbname[MAXPATHLEN];
	int	error;

	get_pool_ccr_tabname(poolname, tbname, MAXPATHLEN);

	// Get the cachefile contents from the file
	zcache_data = get_file_contents(zcachefile_name, &zcache_data_len, err);
	if (zcache_data == NULL) {
		return (1);
	}

	// Calculate the checksum of the cachefile contents
	fletcher_cksum(zcache_data, zcache_data_len, &zcache_cksum);

	free(zcache_data);

	// Get the checksum of cachefile contents stored in CCR for the pool.
	if (cksum_pool(poolname, &ccr_cksum, err) != 0) {
		return (1);
	}

	// Ignore updating the contents in CCR if file is not modified.
	if (CKSUM_EQUAL(zcache_cksum, ccr_cksum)) {
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "Dump ignored as CCR is up-to-date.");
		return (0);
	}

	//
	// Obatain a updatable object to write out keys into the table
	//
	ccr::updatable_table_var pool_table_v;
	Environment e;
	CORBA::Exception *ex;

	// Obtain updatable_table object to perform updates.
	pool_table_v = ccr_dir->begin_transaction(tbname, e);
	if ((ex = e.exception()) != NULL) {
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "Error(%s) while reading CCR table for pool %s",
		    ex->_name(), poolname);
		return (1);
	}

	//
	// Compare again checksum of file contents with CCR checksum incase the
	// table is changed just before starting the transcation.
	//
	zcache_data = get_file_contents(zcachefile_name, &zcache_data_len, err);
	if (zcache_data == NULL) {
		return (1);
	}

	fletcher_cksum(zcache_data, zcache_data_len, &zcache_cksum);

	if (cksum_pool(poolname, &ccr_cksum, err) != 0) {
		free(zcache_data);
		pool_table_v->abort_transaction(e);
		return (1);
	}
	// Ignore updating the contents in CCR if file is not modified.
	if (CKSUM_EQUAL(zcache_cksum, ccr_cksum)) {
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "Dump ignored as CCR is up-to-date.");
		free(zcache_data);
		pool_table_v->abort_transaction(e);
		return (0);
	}

	// The zfs cachefile contents are modified.

	char *ccr_data;

	// Check whether the cachefile information to be a compressed

	if (is_cachefile_met_compression_criteria(zcache_data_len)) {

		// The cachefile information to be in compressed and encoded
		// format. Convert data to write to CCR

		size_t	zcache_compress_encode_data_len;
		ccr_data = (char *)convert_data_to_compressed_encoded_data(
		    zcache_data, zcache_data_len,
		    &zcache_compress_encode_data_len, err);
	} else {

		// The cachefile information just need encoding to write to CCR

		size_t	encoded_data_len;
		ccr_data = (char *)base64_encode((uchar_t *)zcache_data,
		    zcache_data_len, &encoded_data_len, err);
	}
	free(zcache_data);
	if (ccr_data == NULL) {
		pool_table_v->abort_transaction(e);
		return (1);
	}

	pool_table_v->update_element(CDATA_KEY, ccr_data, e);
	if ((ex = e.exception()) != NULL) {
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "Error (%s) updating cachedata in CCR table for pool %s",
		    ex->_name(), poolname);
		free(ccr_data);
		pool_table_v->abort_transaction(e);
		return (1);
	}

	free(ccr_data);
	//
	// Update the checksum key in the table with hex encoded representation
	//
	char zcache_cksum_encoded[CKSUM_SIZE_ENCODED + 1]; // +1 for terminator
	(void) snprintf(zcache_cksum_encoded, CKSUM_SIZE_ENCODED + 1,
	    "%016llx%016llx", zcache_cksum.word[0], zcache_cksum.word[1]);

	pool_table_v->update_element(CACHE_CKSUM_KEY, zcache_cksum_encoded, e);
	if ((ex = e.exception()) != NULL) {
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "Error (%s) updating checksum in CCR table for pool %s",
		    ex->_name(), poolname);
		pool_table_v->abort_transaction(e);
		return (1);
	}

	//
	// Update the zfs cache data size which will be used during
	// decompression of the contents.
	//
	char	zcache_data_len_encoded[ZCACHE_SIZE_ENCODED + 1];
	(void) sprintf(zcache_data_len_encoded, "%08lx", zcache_data_len);
	pool_table_v->update_element(CACHE_SIZE_KEY,
	    zcache_data_len_encoded, e);
	if ((ex = e.exception()) != NULL) {
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "Error (%s) in updating file size in CCR table for pool %s",
		    ex->_name(), poolname);
		pool_table_v->abort_transaction(e);
		return (1);
	}

	// Update the seach dir key value
	if (search_dir_value != NULL) {
		pool_table_v->update_element(SEARCH_DIR_KEY,
		    search_dir_value, e);
		if ((ex = e.exception()) != NULL) {
			(void) snprintf(err->message, ERR_MSG_SIZE,
			    "Error (%s) updating serachdir in CCR table "
			    "for pool %s", ex->_name(), poolname);
			pool_table_v->abort_transaction(e);
			return (1);
		}
	}

	pool_table_v->commit_transaction(e);
	if ((ex = e.exception()) != NULL) {
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "Error (%s) while commiting transaction afer update "
		    "for pool %s",
		    ex->_name(), poolname);
		pool_table_v->abort_transaction(e);
		return (1);
	}
	(void) snprintf(err->message, ERR_MSG_SIZE,
	    "Successfully updated the CCR with file contents.");
	return (0);
}

//
// read_zcache_from_ccr (API)
//
// Read the cachefile associated with given poolname from CCR and dump
// it into zcachefile_name after decoding and decompressing (if compressed
// earlier).
//
// Returns 0 on success and 1 on failure.
// The actual error message information will be in 'err'.
//
int
read_zcache_from_ccr(const char *poolname, const char *zcachefile_name,
    char **search_dir_value, err_msg_t *err)
{
	Environment	e;
	CORBA::Exception *ex;
	char *search_dir_key_val;
	char	tbname[MAXPATHLEN];
	int	error;

	if (!is_ccr_ready(err)) {
		return (1);
	}

	get_pool_ccr_tabname(poolname, tbname, MAXPATHLEN);

	// Lookup CCR table for given pool
	ccr::readonly_table_var pool_table_v = ccr_dir->lookup(tbname, e);
	if ((ex = e.exception()) != NULL) {
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "Error (%s) while reading CCR table for pool %s",
		    ex->_name(), poolname);
		return (1);
	}

	// Read the SEARCH_DIR_KEY value.
	search_dir_key_val = pool_table_v->query_element(SEARCH_DIR_KEY, e);
	if ((ex = e.exception()) != NULL) {
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "Error (%s) while reading key %s of CCR table for pool %s",
		    ex->_name(), SEARCH_DIR_KEY, poolname);
		return (1);
	}
	if ((*search_dir_value = strdup(search_dir_key_val)) == NULL) {
		error = errno;
		delete [] search_dir_key_val;
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "%s", strerror(error));
		return (1);
	}
	delete [] search_dir_key_val;

	// Read the CACHE_SIZE_KEY value. It is required to decompress the
	// contents
	char	*zcache_data_len_encoded;
	size_t	zcache_data_len;
	zcache_data_len_encoded =
	    pool_table_v->query_element(CACHE_SIZE_KEY, e);
	if ((ex = e.exception()) != NULL) {
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "Error (%s) while reading key %s of CCR table for pool %s",
		    ex->_name(), CACHE_SIZE_KEY, poolname);
		return (1);
	}
	(void) sscanf(zcache_data_len_encoded, "%08lx", &zcache_data_len);
	delete [] zcache_data_len_encoded;

	// Read the zfs cache data from CCR
	char	*ccr_data;
	ccr_data = pool_table_v->query_element(CDATA_KEY, e);
	if ((ex = e.exception()) != NULL) {
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "Error (%s) while reading key %s of CCR table "
		    "for pool %s",
		    ex->_name(), CDATA_KEY, poolname);
		return (1);
	}

	void *file_data;
	// Check whether the cachefile information will be a compressed one
	if (is_cachefile_met_compression_criteria(zcache_data_len)) {
		// The CCR cachefile information is in compressed and encoded
		// format. Convert to normal data and dump the data to the file.
		file_data = convert_compressed_encoded_data_to_data(ccr_data,
		    strlen(ccr_data), zcache_data_len, err);
	} else {
		// The CCR information is just encoded one.
		// Decode the data and dump the data to the file.
		size_t	decoded_data_len;
		file_data = base64_decode((uchar_t *)ccr_data,
		    strlen(ccr_data), &decoded_data_len, err);
	}
	delete [] ccr_data;
	if (file_data == NULL) {
		return (1);
	}
	// Write the data to target file.
	error = write_contents_to_file(file_data, zcache_data_len,
	    zcachefile_name, err);
	free(file_data);
	return (error);
}
