//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)librtutils_misc.cc	1.1	09/01/21 SMI"

#include "librtutils.h"
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

//
// get_file_contents
//
// Reads the contents of file_name into a dynamically allocated buffer,
// and returns the buffer, the file size.
// The caller must free the buffer returned.
//
// In case of errors NULL is returned and 'err' contains the failure message.
//
char *
get_file_contents(const char *file_name, size_t *file_size, err_msg_t *err)
{
	int	fd;
	int	error;
	char	*buf = NULL;
	struct stat stat_buf;

	fd = open(file_name, O_RDONLY);
	if (fd == -1) {
		error = errno;
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "file %s open failed: %s",
		    file_name, strerror(error));
		return (NULL);
	}

	error = fstat(fd, &stat_buf);
	if (error == -1) {
		error = errno;
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "fstat failed on file %s: %s",
		    file_name, strerror(error));
		return (NULL);
	}

	*file_size = (size_t)stat_buf.st_size;
	buf = (char *)malloc(*file_size);
	if (buf == NULL) {
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "%s", strerror(error));
		return (NULL);
	}

	error = read(fd, (void *)buf, *file_size);
	if (error != stat_buf.st_size) {
		// Save errno as free() or close() may change this value
		error = errno;
		free(buf);
		(void) close(fd);

		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "Reading %d bytes from file %s failed: %s",
		    *file_size, file_name, strerror(error));
		return (NULL);
	}

	(void) close(fd);

	return (buf);
}

//
// write_contents_to_file
//
// Write the contents to target file.
//
// Returns 0 if completed successfuly.
// In case of errors it returns > 0 and 'err' conatains failure message.
//
int
write_contents_to_file(const void *data, size_t data_len,
    const char *target_file, err_msg_t *err)
{
	int	target_fd;
	int	error;
	ssize_t	nbytes;

	target_fd = open(target_file, O_CREAT | O_TRUNC | O_WRONLY, 0644);
	if (target_fd == -1) {
		error = errno;
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "file %s open failed: %s",
		    target_file, strerror(error));
		return (error);
	}

	nbytes = write(target_fd, data, data_len);
	if ((nbytes == -1) || ((size_t)nbytes != data_len)) {
		error = errno;
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "Write %d bytes to file %s failed: %s",
		    data_len, target_file, strerror(error));
		(void) close(target_fd);
		return (error);
	}
	(void) close(target_fd);
	return (0);
}
