//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)base64.cc	1.2	09/04/08 SMI"

#include "librtutils.h"
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>


// ASCII character position for decoding base64
#define	ORD_A   65
#define	ORD_a   97
#define	ORD_0   48

#define	NUM_BASE64_ENCODING_DIGITS	64
#define	NUM_LETTERS	26
#define	OFFSET_OF_PLUS	62
#define	OFFSET_OF_SLASH	63

extern uchar_t base64_digits[NUM_BASE64_ENCODING_DIGITS+1];

uchar_t base64_digits[NUM_BASE64_ENCODING_DIGITS+1] =
	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";


//
// encode_3bytes_to_base64
// Encode the 3 bytes in 'encode_input' to base64 representation
// and populate the output which is 4 bytes in 'endcode_output'.
// This function assumes user provided atleast 3 bytes in 'encode_input'
// to read and atleast 4 bytes space to write to 'endcode_output'.
//
// [a][b][c] => [first 6-bits of a][last 2-bits of a, first 4-bits of b]
//		[last 4-bits of b, first 2-bits of c][last 6-bits of c]
//
static void
encode_3bytes_to_base64(const uchar_t *encode_input, uchar_t *endcode_output)
{
	endcode_output[0] = base64_digits[encode_input[0] >> 2];
	endcode_output[1] = base64_digits[
	    ((encode_input[0] & 0x3) << 4) | (encode_input[1] >> 4)];
	endcode_output[2] = base64_digits[
	    ((encode_input[1] & 0xf) << 2) | ((encode_input[2] & 0xc0) >> 6)];
	endcode_output[3] = base64_digits[encode_input[2] & 0x3f];
}

//
// base64_encode
//
// Do base64 encoding of the contents of 'input_buffer' as per RFC3548.
// The encoded result with NULL termination is returned on success and should
// be freed by the caller.
//
// Returns a valid pointer on success and NULL on failure.
//
uchar_t *
base64_encode(const uchar_t *input_buffer, size_t input_buffer_size,
    size_t  *encoded_buffer_size, err_msg_t *err)
{
	size_t	index;
	uchar_t	*encoded_buffer;

	//
	// base64 encoding needs 4 bytes for every 3 ascii bytes and an extra
	// byte for NULL termination.
	//
	*encoded_buffer_size = ((roundup(input_buffer_size, 3)/3) * 4) + 1;
	encoded_buffer = (uchar_t *)malloc(*encoded_buffer_size);
	if (encoded_buffer == NULL) {
		int error = errno;
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "%s", strerror(error));
		return (NULL);
	}

	//
	// Encode the buffer to base64 till 3 byte alignment chunks and
	// store in the buffer which will be returned to the caller.
	//
	for (index = 0; index < input_buffer_size/3; index++) {
		encode_3bytes_to_base64(input_buffer + index * 3,
		    encoded_buffer + index * 4);
	}

	//
	// Encode the last bytes which are not 3 byte aligned after
	// adding zero to make to 3 byte alignment.
	//
	if (input_buffer_size % 3 != 0) {

		uchar_t	trailing_3bytes[3];

		uint_t pad_count = 3 - (input_buffer_size % 3);

		(void) memcpy(trailing_3bytes, input_buffer + index * 3,
		    input_buffer_size % 3);
		(void) memset(trailing_3bytes + input_buffer_size % 3,
		    0, pad_count);

		encode_3bytes_to_base64(trailing_3bytes,
		    encoded_buffer + index * 4);

		index++;

		//
		// Also add padding byte '=' at the end as this not 3 byte
		// aligned so that decoding can be done correctly from
		// encoded buffer.
		// The padding bytes depending on number of leftover bytes.
		(void) memset(encoded_buffer + index * 4 - pad_count,
		    '=', pad_count);
	}
	encoded_buffer[*encoded_buffer_size - 1] = '\0';
	return (encoded_buffer);
}

//
// decode_base64_to_3bytes
//
// Decode the first 4 bytes passed in 'decode_input' in big-endian
// fashion from base64 to 3 bytes in 'decode_output'
// This function assumes user provided atleast 4 bytes in 'decode_input'
// to read and atleast 3 bytes space to write to 'decode_output'.
//
// "[a][b][c][d]" => [last 6-bits of a, higher 2-bits from last 6-bits of b]
//			[last 4-bits of b, higher 4-bits from last 6-bits of c]
//			[last 2-bits of c, last 6 bits of d]
//
// Return '0' on success, 'ECANCELED' if a padding byte is seen and
// 'EINVAL' if an invalid base64 character is seen.
//
static int
decode_base64_to_3bytes(const uchar_t *decode_input, uchar_t *decode_output)
{

	int	index;
	uchar_t	char_map[4];
	uchar_t	byte;

	for (index = 0; index < 4; index++) {
		byte = decode_input[index];
		if (byte >= 'A' && byte <= 'Z') {
			char_map[index] = byte - ORD_A;
		} else if (byte >= 'a' && byte <= 'z') {
			char_map[index] = (byte - ORD_a) + NUM_LETTERS;
		} else if (byte >= '0' && byte <= '9') {
			char_map[index] = (byte - ORD_0) +
			    NUM_LETTERS + NUM_LETTERS;
		} else if (byte == '+') {
			char_map[index] = OFFSET_OF_PLUS;
		} else if (byte == '/') {
			char_map[index] = OFFSET_OF_SLASH;
		} else if (byte == '=') {
			// Valid padding , ignore and stop processing
			// the stream.
			char_map[index] = 0;
			break;
		} else {
			//
			// Not a valid base64 digit or padding, most
			// probably the encoding is corrupted.
			//
			return (EINVAL);
		}
	}

	//
	// Now decode the string.
	//
	if ((decode_input[0] == '=') || (decode_input[1] == '=')) {
		return (EINVAL);
	}

	decode_output[0] =
	    ((char_map[0] & 0x3f) << 2) | ((char_map[1] & 0x30) >> 4);

	if (decode_input[2] == '=') {
		return (ECANCELED);
	}
	decode_output[1] =
	    ((char_map[1] & 0xf) << 4) | ((char_map[2] & 0x3c) >> 2);

	if (decode_input[3] == '=') {
		return (ECANCELED);
	}
	decode_output[2] = ((char_map[2] & 0x3) << 6) | (char_map[3]);

	return (0);
}

//
// base64_decode
//
// Decode the contents of buffer from base64 to ascii and return the
// decoded buffer.
// Base64 encoding is expected to be as per RFC3548.
//
// Returns decoded buffer on success and NULL on failure.
//
uchar_t *
base64_decode(const uchar_t *encoded_buffer, size_t encoded_buffer_size,
    size_t *decoded_buffer_size, err_msg_t *err)
{
	size_t	index;
	int	error = 0;
	uchar_t	*decoded_buffer;
	size_t	cur_decoded_buffer_size;

	//
	// The base 64 encoded buffer should always be multiples of 4.
	//
	if (encoded_buffer_size % 4 != 0) {
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "Base64 encoded buffer size(%d) to decode "
		    "is not multiple of 4.",
		    encoded_buffer_size);
		return (NULL);
	}

	//
	// base64 decoding needs 3 bytes for every 4 decoded bytes.
	//
	decoded_buffer = (uchar_t *)malloc((encoded_buffer_size/4) * 3);
	if (decoded_buffer == NULL) {
		error = errno;
		(void) snprintf(err->message, ERR_MSG_SIZE,
		    "%s", strerror(error));
		return (NULL);
	}

	//
	// Decode the base64 encoded stream 4 characters at a time
	// which give us 3 bytes of decoded bytes. We have made sure
	// the buffer is 4 byte aligned.
	//
	for (index = 0; index * 4 < encoded_buffer_size; index++) {
		error = decode_base64_to_3bytes(
		    encoded_buffer + index * 4,
		    decoded_buffer + index * 3);
		if (error == EINVAL) {
			// error, invalid base64 string
			(void) snprintf(err->message, ERR_MSG_SIZE,
			    "Invalid base64 encoded buffer.");
			free(decoded_buffer);
			return (NULL);
		}

		if (error == ECANCELED) {
			break;
		}
	}

	cur_decoded_buffer_size = (encoded_buffer_size/4) * 3;

	if (error != 0) {
		//
		// The error should be bacause of PADDING character.
		//
		if (error != ECANCELED) {
			(void) snprintf(err->message, ERR_MSG_SIZE,
			    "Internal error while decoding.");
			free(decoded_buffer);
			return (NULL);
		}

		//
		// Conversion suspended because PADDING character was seen.
		// The conversion suspension due to PADDING character should
		// happen only during processing of last 4 bytes of
		// encoded buffer.
		//

		if (index * 4 != encoded_buffer_size - 4) {
			(void) snprintf(err->message, ERR_MSG_SIZE,
			    "Invalid base64 encoded buffer with padding "
			    "character (=) found in the middle of buffer.");
			free(decoded_buffer);
			return (NULL);
		}

		//
		// Decoding is happened until PADDING character is
		// encountered.
		// Adjust the file size according to the padding bytes, as the
		// padding bytes can be atmost 2 at the end of encoded buffer.
		//
		if (encoded_buffer[encoded_buffer_size - 1] == '=') {
			cur_decoded_buffer_size--;
		}
		if (encoded_buffer[encoded_buffer_size - 2] == '=') {
			cur_decoded_buffer_size--;
		}
	}

	*decoded_buffer_size = cur_decoded_buffer_size;

	return (decoded_buffer);
}
