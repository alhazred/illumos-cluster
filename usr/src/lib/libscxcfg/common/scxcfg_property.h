/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_SCXCFG_PROPERTY_H
#define	_SCXCFG_PROPERTY_H

#pragma ident	"@(#)scxcfg_property.h	1.2	08/05/20 SMI"

#include <string>

using std::string;

struct scxcfg_property
{
	scxcfg_property(string p, string v) {
		name = p;
		value = v;
	}
	friend int operator > (const scxcfg_property& left,
				const scxcfg_property& right)
	{
		return (left.name > right.name);
	}
	friend int operator < (const scxcfg_property& left,
				const scxcfg_property& right)
	{
		return (left.name < right.name);
	}
	string name;
	string value;
};

#endif	/* _SCXCFG_PROPERTY_H */
