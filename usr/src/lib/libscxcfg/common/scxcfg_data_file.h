/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_SCXCFG_DATA_FILE_H
#define	_SCXCFG_DATA_FILE_H

#pragma ident	"@(#)scxcfg_data_file.h	1.3	08/05/20 SMI"

#include <sstream>
#include "scxcfg_cached_data.h"


class scxcfg_data_file : public scxcfg_cached_data
{
public:
	scxcfg_data_file(const char *file);
	scxcfg_data_file(const char *file, int admin);
	~scxcfg_data_file();
	int init(const char *file);
	int close();
	int getProperty(const char *context, const char *prop,
	    char *val) { return (SCXCFG_ENOTIMPLEMENTED); };
	int setProperty(const char *, const char *, const char *);
	int setListProperty(const char *context, std::list<Prop*> *list);
	int delProperty(const char *, const char *);
	int delListProperty(const char *context, std::list<Prop*> *list);
	int getConfig(const char *, char **, const scxcfg_nodeid_t)
	    { return (SCXCFG_ENOTIMPLEMENTED); };
	int deleteConfig(const char *) { return (SCXCFG_ENOTIMPLEMENTED); };
	int writeFile(std::stringstream *);
private:
	// checkForUpdate no-op since the access is read-only.
	int checkForUpdate() { return (SCXCFG_OK); };
	int fillCache(const char *context);
	string *fileName;
	bool admin;
};

#endif /* _SCXCFG_DATA_FILE_H */
