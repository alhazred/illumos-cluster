/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scxcfg_cached_data.cc	1.4	08/05/20 SMI"

#include <fstream>
#include <iostream>
#include <iterator>
#include <set>
#include <sstream>
#include <string>
#include <cstring>

#include <scxcfg/scxcfg.h>
#include "scxcfg_cached_data.h"
#include "scxcfg_int.h"

using namespace std;

//
// scxcfg_node::traverse()
//
// Traverse the configuration tree. Starting at this node.
// Calls callback function for each node.
//
void
scxcfg_node::traverse(int (*callback)(scxcfg_node *, void *), void *arg)
{
	// current points to the node in the original tree that we
	// are copying.
	scxcfg_node *current = this;
	current->atfirst();

	// This loop traverses the tree. We access nodes (callback on them)
	// on the way up the tree (depth first traverse).
	while (true) {	//lint !e716 !e774
		scxcfg_node *nxt = current->get_current_child();
		if (nxt != NULL) {
			// Note that we only access a node once on the
			// way down, so we initialize the node here.
			nxt->atfirst();
			// Advance the current node, so that next time
			// we get to it we'll have the right child.
			current->advance();
			current = nxt;
		} else {
			// On the way up.
			scxcfg_node *prnt = current->get_parent();
			if (callback(current, arg) == 0) {
				return;
			}
			if (current == this) {
				// Back to where we started, done.
				break;
			}
			current = prnt;
		}
	}
}

//
// scxcfg_node::get_named_child()
//
// Get the child of this node whose name is n.
// Returns NULL if not found.
//
scxcfg_node *
scxcfg_node::get_named_child(const char *n)
{
	std::list<scxcfg_node*>::const_iterator it;
	it = children.begin();
	while ((it != children.end()) && strcmp(n, (*it)->name) != 0) {
		it++;
	}
	if (it == children.end())
		return (NULL);
	return (*it);
}

//
// scxcfg_node::get_named_child_create()
//
// Get the child of this node whose name is n.
// Create one if not found.
// Returns NULL if it cannot be added.
//
scxcfg_node *
scxcfg_node::get_named_child_create(const char *n)
{
	if (!scxcfg_node::name_ok(n)) {
		return (NULL);
	}

	scxcfg_node *child = get_named_child(n);
	if (child == NULL) {
		// Create the child.
		child = create_node(n);
		if (!add(child)) {
			delete child;
			return (NULL);
		}
	}
	return (child);
}

//
// scxcfg_node::get_child_by_name()
//
// Get a child by the relative name.
// The name given is one that starts with this. That is, it contains
// the node of this node.
//
scxcfg_node *
scxcfg_node::get_child_by_name(const char *n)
{
	char str[FCFG_MAX_LEN + 1];

	char *ptr = (char *)n;
	scxcfg_node *node = this;
	(void) scxcfg_node::get_next_str(ptr, str); // Get to the next element.

	while (get_next_str(ptr, str)) {
		node = node->get_named_child(str);
		if (node == NULL) {
			return (NULL);
		}
	}
	return (node);
}

//
// scxcfg_node::get_child_by_name_create()
//
// Get a child by the relative name. Create child if not found.
// The name given is one that starts from the this. That is, it contains
// the name of this node.
//
scxcfg_node *
scxcfg_node::get_child_by_name_create(const char *n)
{
	char str[FCFG_MAX_LEN + 1];
	//	cout << "get_child_by_name_create: " << n << endl;
	char *ptr = (char *)n;
	scxcfg_node *node = this;
	(void) scxcfg_node::get_next_str(ptr, str); // Get to the next element.

	while (get_next_str(ptr, str)) {
		node = node->get_named_child_create(str);
	}
	return (node);
}

//
// scxcfg_node::get_parent()
//
scxcfg_node *
scxcfg_node::get_parent()
{
	return (parent);
}

//
// scxcfg_node::get_value()
//
const char *
scxcfg_node::get_value()
{
	return (value);
}

//
// scxcfg_node::get_name()
//
const char *
scxcfg_node::get_name()
{
	return (name);
}

//
// scxcfg_node::get_full_name()
//
// Get the full name of a node.
// Note: uses static buffer.
//
char *
scxcfg_node::get_full_name()
{
	std::list<scxcfg_node*> stack;
	std::list<scxcfg_node*>::const_iterator it;
	scxcfg_node *current = this;
	static char full_name[FCFG_MAX_LEN];
	// Get the full path.
	do {
		stack.push_front(current);
		current = current->get_parent();
	} while (current != NULL);
	full_name[0] = 0;
	for (it = stack.begin(); it != stack.end(); it++) {
		strcat(full_name, (*it)->name);
		strcat(full_name, ".");
	}
	// remove last '.'
	full_name[strlen(full_name) -1] = 0;
	//	full_name[length - 1] = 0;
	return (full_name);
}

//
// scxcfg_node::set_value()
//
// Set the value of this node if it is a leaf node, and return true.
// If not a leaf node, returns false.
//
bool
scxcfg_node::set_value(const char *v)
{
	if (is_leaf()) {
		// Leaf node.
		delete[] value;
		value = strdup(v);
		return (true);
	}
	return (false);
}

//
// scxcfg_node::set_parent()
//
// Set the parent of this node.
//
void
scxcfg_node::set_parent(scxcfg_node *n)
{
	parent = n;
}

//
// scxcfg_node::add()
//
// Adds the node in the children list of this node.
//
bool
scxcfg_node::add(scxcfg_node *node)
{
	children.push_back(node);
	// cout << " ADD: this :" << this->name << ":" << this << endl;
	// cout << " child: " << node->name << ":"  << node <<  endl;
	node->set_parent(this);
	return (true);
}

//
// scxcfg_node::dremoveadd()
//
// Removes the node from the children list of this node.
//
void
scxcfg_node::remove(scxcfg_node *node)
{
	children.remove(node);
}

//
// scxcfg_node::get_next_str()
//
// Helper function. Copy one field from ptr to str. Returns false when
// ptr reaches the end.
//
bool
scxcfg_node::get_next_str(char *&ptr, char *str, char sep)
{
	if (ptr[0] == 0) {
		return (false);
	}
	while ((*str++ = *ptr++) != sep) {
		// check whether we reached the end of the NULL terminating
		// string.
		if (*(ptr-1) == 0) {
			ptr--;
			break;
		}
	}
	str--;
	*str = 0;
	return (true);
}

//
// scxcfg_node::is_leaf()
//
bool
scxcfg_node::is_leaf()
{
	return (children.empty());
}

//
// scxcfg_node::atfirst()
//
// Set the children iterator at the beginning of the list.
//
void
scxcfg_node::atfirst()
{
	child_iter = children.begin();
}

//
// scxcfg_node::advance()
//
// advance children list iterator.
//
void
scxcfg_node::advance()
{
	child_iter++;
}

//
//  scxcfg_node::get_current_child()
//
// Returns the current child pointed by list iterator, or NULL if at end
// of list.
//
scxcfg_node *
scxcfg_node::get_current_child()
{
	if (child_iter != children.end())
		return (*child_iter);
	else
		return (NULL);
}

//
// delete_tree_call_back()
//
// The callback which will delete a tree.
//
static int
delete_tree_call_back(scxcfg_node *n, void *)
{
	scxcfg_node *prt = n->get_parent();
	if (prt != NULL) {
		(void) prt->remove(n);
	}
	delete n;
	return (1);
}

//
// scxcfg_node::delete_tree()
//
// Deletes a tree, starting from this node.
//
void
scxcfg_node::delete_tree()
{
	traverse(delete_tree_call_back, NULL);
}

//
// scxcfg_node::name_ok()
//
// A helper function. It checks whether the name contains illegal
// characters. Returns true if name is ok.
//
bool
scxcfg_node::name_ok(const char *str, char ch)
{
	char c;
	while ((c = *str++) != 0) {
		if (c == ch) {
			return (false);
		}
	}
	return (true);
}

//
// scxcfg_node::create_node()
//
scxcfg_node *
scxcfg_node::create_node(const char *n)
{
	return (new scxcfg_node(n));
}

//
// scxcfg_node constructor.
//
scxcfg_node::scxcfg_node(const char *n) :
	parent(NULL),
	value(NULL)
{
	name = strdup(n);
}

//
// scxcfg_node destructor.
//
scxcfg_node::~scxcfg_node()
{
	if (name)
		free(name);
	if (value)
		free(value);
	parent = NULL;
}

//
// scxcfg_node::print()
//
// For debug, prints the full name of this node.
//
int
scxcfg_node::print(scxcfg_node *node, void *)
{
	if (node->is_leaf()) {
		char *fname = node->get_full_name();
		cout << "DBG ";
		if (node->get_value() != NULL) {
			cout <<  fname <<  " value: <<" <<
				node->get_value() << ">>" << endl;
		} else {
			cout << fname << "  NO_VALUES !!" << endl;
		}
	}
	else
		cout << "DBG: " << node->name << endl;
	return (1);
}


//
// scxcfg_cached_data methods
//

//
// scxcfg_cached_data constructor.
//
scxcfg_cached_data::scxcfg_cached_data()
{
	stringCache = NULL;
	farmRootNode = NULL;
	cachedContext = NULL;
}

//
// scxcfg_cached_data destructor.
//
// Free everything.
//
scxcfg_cached_data::~scxcfg_cached_data()
{
	lock();
	if (farmRootNode)
		farmRootNode->delete_tree();
	propertiesList.clear();
	if (stringCache)
		delete stringCache;
	unlock();
}

//
// scxcfg_cached_data::init()
//
// Calls the implementation dependent fillCache() with the given context.
// Then constructs the property list, and the configuration tree from
// this cache.
//
int
scxcfg_cached_data::init(const char *context)
{
	int rc;
	rc = fillCache(context);
	if (rc == SCXCFG_OK) {
		cachedContext = new string(context);
		//
		// fillCache has updated stringCache,
		// build now the propertiesList.
		//
		buildList(stringCache);
	}
	if (rc == SCXCFG_OK) {
		buildFarmTree();
	}
	return (rc);
}

//
// scxcfg_cached_data::close()
//
// The counterpart of init().
//
int
scxcfg_cached_data::close()
{
	if (farmRootNode)
		farmRootNode->delete_tree();
	farmRootNode = NULL;
	propertiesList.clear();
	if (stringCache)
		delete stringCache;
	stringCache = NULL;
	return (SCXCFG_OK);
}


//
// scxcfg_cached_data::getCachedProperty()
//
// Returns the value of a property, if this property is in the cached
// list of properties.
//
int
scxcfg_cached_data::getCachedProperty(const char *prop, char *val)
{
	scxcfg_log(LOG_DEBUG, "scxcfg_cached_data::getCachedProperty");
	Prop p = Prop(prop, string(""));
	std::set<Prop>::const_iterator it;
	string s;
	checkForUpdate();
	lock();
	it = propertiesList.find(p);

	if (it == propertiesList.end()) {
		unlock();
		return (SCXCFG_ENOEXIST);
	}
	strcpy(val, (*it).value.c_str());
	unlock();
	return (0);
}

//
// scxcfg_cached_data::getCachedListProperty()
//
// Returns a list of children names from a given (partial) property name.
// If the property is a leaf, a single element list is returned with
// the value of this leaf.
// If the property points a node with children, the returned list
// contains the names of these children.
//
int
scxcfg_cached_data::getCachedListProperty(const char *prop,
    f_property_t **listvalue, int*cnt)
{
	scxcfg_log(LOG_DEBUG, "scxcfg_cached_data::getCachedListProperty");
	string s;
	scxcfg_node *n, *n2;
	f_property_t *p, *p2;

	checkForUpdate();
	lock();
	*listvalue = NULL;
	*cnt = 0;

	if (!farmRootNode) {
		unlock();
		return (SCXCFG_EUNEXPECTED);
	}
	n = farmRootNode->get_child_by_name(prop);
	if (n == NULL) {
		unlock();
		return (SCXCFG_ENOEXIST);
	}

	if (n->is_leaf()) {
		*cnt = 1;
		p = (f_property_t *)malloc(sizeof (f_property_t));
		p->value = strdup(n->get_value());
		p->next = NULL;
		*listvalue = p;
	} else {
		*cnt = 0;
		n->atfirst();
		p2 = NULL;
		while ((n2 = n->get_current_child()) != NULL) {
			p = (f_property_t *)malloc(sizeof (f_property_t));
			p->value = strdup(n2->get_name());
			if (p2)  {
				p2->next = p;
			} else {
				*listvalue = p;
			}
			p2 = p;
			(*cnt)++;
			n->advance();
		}
		p->next = NULL;
	}
	unlock();
	return (SCXCFG_OK);
}


//
// scxcfg_cached_data::buildList()
//
// Builds the property list from the cached raw data passed in the
// stringstream.
//
int
scxcfg_cached_data::buildList(stringstream *ss)
{
	scxcfg_log(LOG_DEBUG, "scxcfg_cached_data::buidList");
	string line;
	int lineNum = 0;
	// DEBUG, output entire buffer
	//	cout << ss->rdbuf();
	ss->seekg(0, ios::beg);
	while (getline(*ss, line)) {
		lineNum++;
		std::size_t end1 = line.find_first_of(" \t");
		if (end1 == string::npos)
			continue;
		string remain = line.substr(end1, string::npos);
		std::size_t beg2 = remain.find_first_not_of(" \t");
		if (beg2 == string::npos) {
			scxcfg_log(LOG_ERR, "Incorrect property in config"
			    " line %d", lineNum);
			continue;
		}
		string *prop = new string(line.substr(0, end1));
		string *val = new string(remain.substr(beg2, string::npos));
		//  cout << "scxcfg_property:" << *prop << " " << *val << endl;
		Prop *p = new Prop(*prop, *val);
		propertiesList.insert(*p);
	}
	return (SCXCFG_OK);
}

//
// scxcfg_cached_data::buildFarmTree()
//
// Build the farm configuration tree, from the cached propertiesList.
//
int
scxcfg_cached_data::buildFarmTree()
{
	scxcfg_log(LOG_DEBUG, "scxcfg_cached_data::buildFarmTree");
	std::set<Prop>::const_iterator it;
	scxcfg_node *n;

	//
	// Create the root of the farm configuration.
	//
	n = new scxcfg_node("farm");
	//
	// Go through the simple list of properties and build the tree.
	//
	for (it = propertiesList.begin(); it != propertiesList.end(); it++) {
		(void) n->get_child_by_name_create((*it).name.c_str())->
			set_value((*it).value.c_str());
	}
	farmRootNode = n;
	// DEBUG
	//  n->traverse(scxcfg_node::print, NULL);
	return (SCXCFG_OK);
}


//
// addLeafsToList()
//
// A callback utility function used with traverse() to get all
// property full names from a given sub-tree. See getSubTreeList().
//
static int
addLeafsToList(scxcfg_node *n,  void *v)
{
	std::list<Prop*> *list = (std::list<Prop*> *)v;
	if (n->is_leaf()) {
		Prop *v = new Prop(n->get_full_name(), n->get_value());
		list->push_back(v);
	}
	return (1);
}

//
// scxcfg_cached_data::getSubtreeList()
//
// Returns a list of Prop objects which are all the complete
// properties with their associated values that are contained
// in a  given subtree.
// The subtree starts at node pointed by "name".
//
// Returns:
// SCXCFG_EUNEXPECTED: there is no property cached.
// SCXCFG_ENOXIST:  name is not present in the tree.
// SCXCFG_OK: name found (list may be empty).
//
int
scxcfg_cached_data::getSubtreeList(const char *name, std::list<Prop*> *listv)
{
	scxcfg_log(LOG_DEBUG, "scxcfg_cached_data::getSubtreeList"
	    " name = %s", name);
	if (!name || !listv) {
		return (SCXCFG_EINVAL);
	}
	checkForUpdate();
	lock();
	std::set<Prop>::const_iterator it;
	scxcfg_node *n;

	it = propertiesList.begin();
	if (it == propertiesList.end()) {
		unlock();
		return (SCXCFG_EUNEXPECTED);
	}
	if (!farmRootNode) {
		unlock();
		return (SCXCFG_EUNEXPECTED);
	}
	scxcfg_log(LOG_DEBUG, "getSubtreeList: get_child_by_name %s", name);
	n = farmRootNode->get_child_by_name(name);
	if (n == NULL) {
		scxcfg_log(LOG_DEBUG, "getSubtreeList: name not found");
		unlock();
		return (SCXCFG_ENOEXIST);
	}
	n->traverse(addLeafsToList, (void *)listv);
	unlock();
	return (SCXCFG_OK);
}
