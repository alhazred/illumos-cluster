/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scxcfg_data_file.cc	1.3	08/05/20 SMI"

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

#include <cerrno>

#include <scxcfg_int.h>
#include "scxcfg_data_file.h"

using namespace std;

//
// scxcfg_data_file
//
// Implementation of scxcfg_cached_data with a simple file
// as data store.
//
// This used on farm nodes where the file is created from the
// server configuration as part of starting farm services.
//
// This implementation is read-only.
//


//
// scxcfg_data_file standard constructor
//
scxcfg_data_file::scxcfg_data_file(const char *file)
{
	lock();
	admin = false;
	fileName = new string(file);
	unlock();
}

//
// scxcfg_data_file constructor for admin access
//
scxcfg_data_file::scxcfg_data_file(const char *file, int)
{
	lock();
	admin = true;
	fileName = new string(file);
	unlock();
}

//
// scxcfg_data_file destructor.
//
scxcfg_data_file::~scxcfg_data_file()
{
	lock();
	if (fileName)
		delete fileName;
	unlock();
}

//
// scxcfg_data_file::init()
//
// Access the file, and fill the cache.
//
int
scxcfg_data_file::init(const char *file)
{
	int rc  = SCXCFG_OK;
	lock();
	if (fileName == NULL) {
		unlock();
		return (SCXCFG_EUNEXPECTED);
	}
	ifstream propFile(fileName->c_str());
	if (!propFile) {
		perror(fileName->c_str());
		unlock();
		return (SCXCFG_EUNEXPECTED);
	}
	// call parent class method
	rc = scxcfg_cached_data::init(file);
	unlock();
	return (rc);
	// propfile is closed
}

//
// scxcfg_data_file::close()
//
int
scxcfg_data_file::close()
{
	int rc = SCXCFG_OK;
	lock();
	delete fileName;
	fileName = NULL;
	// call parent class method
	rc = scxcfg_cached_data::close();
	unlock();
	return (rc);
}

//
// scxcfg_data_file::fillCache()
//
int
scxcfg_data_file::fillCache(const char *)
{
	if (stringCache) {
		// already filled, must close and init
		return (SCXCFG_EEXIST);
	}
	ifstream propFile(fileName->c_str());
	if (!propFile) {
		return (SCXCFG_ENOEXIST);
	}
	stringCache = new stringstream;
	*stringCache << propFile.rdbuf();
	// DEBUG
	//	cout << "DUMP of string cache" << endl;
	//	cout << stringCache->rdbuf();
	return (SCXCFG_OK);
}

//
// scxcfg_data_file::setProperty()
//
// Read-only cannot write.
//
int
scxcfg_data_file::setProperty(const char *context, const char *key,
    const char *val)
{
	return (SCXCFG_ENOTIMPLEMENTED);
}

//
// scxcfg_data_file::setListProperty()
//
// Read-only cannot write.
//
int
scxcfg_data_file::setListProperty(const char *context, std::list<Prop*> *list)
{
	return (SCXCFG_ENOTIMPLEMENTED);
}

//
// scxcfg_data_file::delProperty()
//
// Read-only cannot write.
//
int
scxcfg_data_file::delProperty(const char *context, const char *key)
{
	return (SCXCFG_ENOTIMPLEMENTED);
}

//
// scxcfg_data_file::delListProperty()
//
// Read-only cannot write.
//
int scxcfg_data_file::delListProperty(const char *context,
    std::list<Prop*> *list)
{
	return (SCXCFG_ENOTIMPLEMENTED);
}


//
// scxcfg_data_file::writeFile()
//
// Admin access only, write the cache file.
//
int
scxcfg_data_file::writeFile(stringstream *ss)
{
	int rc = SCXCFG_OK;
	lock();
	if (admin == false) {
		unlock();
		return (SCXCFG_EINVAL);
	}
	if (fileName == NULL) {
		unlock();
		return (SCXCFG_ENOINIT);
	}
	ofstream propFile(fileName->c_str());
	if (!propFile) {
		perror("fileName");
		unlock();
		return (errno);
	}
	propFile << ss->str();
	unlock();
	return (rc);
}
