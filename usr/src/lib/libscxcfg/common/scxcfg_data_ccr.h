/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_SCXCFG_DATA_CCR_H
#define	_SCXCFG_DATA_CCR_H

#pragma ident	"@(#)scxcfg_data_ccr.h	1.3	08/05/20 SMI"

#include <string>
#include <list>
#include <h/ccr.h>
#include <h/naming.h>

#include <scxcfg/scxcfg.h>
#include "scxcfg_int.h"
#include "scxcfg_cached_data.h"


class scxcfg_data_ccr : public scxcfg_cached_data
{
public:
	scxcfg_data_ccr();
	~scxcfg_data_ccr();
	int init(const char *context);
	int close();
	int getProperty(const char *context, const char *prop,
	    char *val);
	int setProperty(const char *, const char *, const char *);
	int setListProperty(const char *context, std::list<Prop*> *list);
	int delProperty(const char *, const char *);
	int delListProperty(const char *context, std::list<Prop*> *list);
	int getConfig(const char *context, char **buf, const scxcfg_nodeid_t n);
	int deleteConfig(const char *context);
private:
	int checkForUpdate();
	int fillCache(const char *context);
	scxcfg_errno_t read_table(const char *table_name, char **buf,
	    const scxcfg_nodeid_t n);
	scxcfg_errno_t delete_table(const char *table_name);
	scxcfg_errno_t read_element(const char *table_name, const char *key,
	    char *value);
	scxcfg_errno_t write_elements(const char *table_name,
	    std::list<Prop*> *v);
	scxcfg_errno_t remove_elements(const char *table_name,
	    std::list<Prop*> *v);
	scxcfg_errno_t create_farm_table();
	static scxcfg_errno_t convert_ccr_error(CORBA::Exception *ex);
	naming::naming_context_var namingCtxp;
	int currentVersion;
	ccr::directory_ptr	ccr_dir;
	ccr::readonly_table_ptr	tablePtr;
	ccr::updatable_table_ptr updatePtr;
	Environment	e;
};

#endif /* _SCXCFG_DATA_CCR_H */
