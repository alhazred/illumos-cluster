/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)data_access.cc	1.4	08/05/20 SMI"

#include <iostream>
#include <string>
#include <cstddef>
#include <cstdlib>
#include <dlfcn.h>

#include <scxcfg/scxcfg.h>
#include "scxcfg_int.h"
#include "scxcfg_data.h"
#include "scxcfg_data_file.h"
#include "data_access.h"

//
// data_access
//
// Interface between the API and the implementation.
//
// On a server node, init_scxcfg_data creates a CCR access
// by allocating a scxcfg_data_ccr object.
// On a farm node,  init_scxcfg_data creates an access to the cached
// partial config of the farm node in a file (scxcfg_data_file) object.
//
// Since the CCR (Sun Cluster ORB) libraries are not present on a farm
// node, the  scxcfg_data_ccr objects are on a separate library, dynamically
// load only on a serverv node.
//
// Data access are always cached_data. Depending on the context parameters
// some calls (getProperty) can bypass the cache (for a different context)
// if the underlying object supports it.
//
// eg: if the cached context is the "farm" table in CCR, a
// a getProperty with context "infrastructure" can retrive a property
// from the Sun Cluster config.
//

//
// The data access object pointer.
//
static scxcfg_cached_data *dataAccess;

#define	LIBSCXCFGCCR		"libscxcfg_ccr.so.1"
#define	SCXCFG_DATA_CCR_ALLOC	"scxcfg_data_ccr_alloc"

//
// init_scxcfg_data()
//
// Creates and init the scxcfg_data object corresponding to this node.
//
int
init_scxcfg_data(void **v)
{
	char *access = NULL;

	if (dataAccess) {
		// cannot open twice
		return (SCXCFG_EEXIST);
	}

	if (scxcfg_farm) {
		//
		// Farm node case.
		//
		if (getenv("SCXCFG_DEBUG_FILE") == NULL) {
			access = FARM_CONFIG_FILE;
		} else { // DEBUG in local file
			access = "./cfg";
		}
		//
		// Default API path on farm node is to accces the
		// file cache.
		// It must have been updated previously, downloaded
		// from a server.
		//
		dataAccess = new scxcfg_data_file(access);
	} else {
		//
		// Server node case.
		// Direct access to CCR through ORB.
		// load library containing CCR access code.
		//
		scxcfg_cached_data *(*fn_dataAccessCcr)();
		void *dlhandle;
		if ((dlhandle = dlopen(LIBSCXCFGCCR, RTLD_LAZY)) == NULL) {
			fprintf(stderr, "Error in dlopen: %s\n", dlerror());
			return (SCXCFG_EUNEXPECTED);
		}
		if ((fn_dataAccessCcr = (scxcfg_cached_data *(*)())
		    dlsym(dlhandle, SCXCFG_DATA_CCR_ALLOC)) == NULL) {
			fprintf(stderr, "Error in dlsym: %s\n", dlerror());
			return (SCXCFG_EUNEXPECTED);
		}
		//
		// Allocate scxcfg_data_ccr object.
		//
		dataAccess = (*fn_dataAccessCcr)();
		access = FARM_TABLE;
	}
	if (dataAccess == NULL) {
		return (SCXCFG_ENOMEM);
	}
	//
	// Init access.
	//
	int rc = dataAccess->init(access);
	if (rc) {
		delete dataAccess;
		return (rc);
	}
	*v = dataAccess;
	return (SCXCFG_OK);
}

//
// end_scxcfg_data()
//
// Close and destroy the data access object.
//
int
end_scxcfg_data(void *v)
{
	if (dataAccess) {
		if (v != (void *)dataAccess)
			return (SCXCFG_EINVAL);
		dataAccess->close();
		delete dataAccess;
		dataAccess = NULL;
	}
	return (SCXCFG_OK);
}

//
// dataGetProperty()
//
// If the given context is the cached context, search in the cache,
// else bypass the cache.
//
int
dataGetProperty(const char *context, const char*prop, char *val)
{
	if (!context ||
	    (string(context) ==  *(dataAccess->getCachedContext())))
		return (dataAccess->getCachedProperty(prop, val));
	else
		return (dataAccess->getProperty(context, prop, val));
}

//
// dataGetListProperty()
//
// Currnently this works only for cached access, context is ignored.
//
int
dataGetListProperty(const char *context, const char *prop,
    f_property_t **listvalue, int *cnt)
{
	return (dataAccess->getCachedListProperty(prop, listvalue, cnt));
}

//
// dataSetProperty()
//
// Set a property, update or create if it does not exist.
// Always access directly the data store, no cache.
//
int
dataSetProperty(const char *context, const char *prop, const char *val)
{
	return (dataAccess->setProperty(context, prop, val));
}

//
// dataDelProperty()
//
// Delete a property.
// Always access directly the data store, no cache.
//
int
dataDelProperty(const char *context, const char *prop)
{
	return (dataAccess->delProperty(context, prop));
}

//
// utilGetNameNodeid()
//
// Get the nodeid for a given nodename.
//
int
utilGetNameNodeid(const char *nodename, scxcfg_nodeid_t *nodeid)
{
	f_property_t *list;
	f_property_t *v;
	int rc = SCXCFG_OK;
	char prop[FCFG_MAX_LEN];
	char name[FCFG_MAX_LEN];
	int num, i;

	rc = dataAccess->getCachedListProperty("farm.nodes", &list, &num);

	if ((rc != SCXCFG_OK) && (rc != SCXCFG_ENOEXIST)) {
		scxcfg_log(LOG_ERR, "utilGetNameNodeid:error "
		    "getting nodes: %d", rc);
		return (rc);
	}
	if (num == 0) {
		scxcfg_log(LOG_DEBUG, "utilGetNameNodeid:no nodes in config");
		return (rc);
	}
	*nodeid = 0;
	for (i = 0, v = list; (i < num) && v; i++, v = v->next) {
		sprintf(prop, "farm.nodes.%s.name", v->value);
		rc = dataAccess->getCachedProperty(prop, name);
		if (rc != SCXCFG_OK) {
			scxcfg_log(LOG_DEBUG, "no name for nid %s in farm cfg",
			    v->value);
			continue;
		}
		if (strcmp(name, nodename) == 0) {
			// found
			*nodeid = atoi(v->value);
			if (*nodeid < 0)
				rc = SCXCFG_EUNEXPECTED;
			else
				rc = SCXCFG_OK;
			break;
		}
	}
	if (*nodeid == 0) {
		rc = SCXCFG_ENOEXIST;
	}
	scxcfg_freelist(list);
	return (rc);
}

//
// utilGetNodeidName()
//
// Get the nodename for a given nodeid.
//
int
utilGetNodeidName(const scxcfg_nodeid_t nodeid, char *nodename)
{
	int rc = SCXCFG_OK;
	char prop[FCFG_MAX_LEN];
	char value[FCFG_MAX_LEN];
	sprintf(prop, "farm.nodes.%d.name", nodeid);
	rc = dataAccess->getCachedProperty(prop, value);
	if ((rc != SCXCFG_OK) && rc != (SCXCFG_ENOEXIST)) {
		scxcfg_log(LOG_ERR, "utilGetNodeidName:error "
		    "getting nodes: %d", rc);
		return (rc);
	}
	strcpy(nodename, value);
	return (rc);
}

//
// utilDeleteNode()
//
// Delete a node given by nodename from the farm configuration.
// Delete the whole subtree farm.nodes.<nodeid>
//
int
utilDeleteNode(const char *nodeName)
{
	std::list<Prop*> nodeProps;
	char prefix[FCFG_MAX_LEN];
	uint32_t nodeId;
	int rc;
	scxcfg_log(LOG_DEBUG, "deleteNode %s", nodeName);
	rc = utilGetNameNodeid(nodeName, &nodeId);
	if (rc != SCXCFG_OK)
		return (rc);
	sprintf(prefix, "farm.nodes.%d", nodeId);
	rc = utilDeleteSubtree(prefix);
	return (rc);
}

//
// utilDeleteSubtree()
//
// Delete the whole subtree in the config tree, starting at root.
//
int
utilDeleteSubtree(const char *root)
{
	std::list<Prop*> nodeProps;
	int rc;
	scxcfg_log(LOG_DEBUG, "deleteSubtree %s", root);
	if (strncmp(root, "farm.", 5) != 0)
		return (SCXCFG_ENOEXIST);
	rc = ((scxcfg_cached_data*)dataAccess)->getSubtreeList(root,
	    &nodeProps);
	if (rc != SCXCFG_OK)
		return (rc);
	rc = dataAccess->delListProperty(NULL, &nodeProps);
	return (rc);
}


//
// utilAllocateNodeid()
//
// Utility function to allocate a new nodeid for a nodename.
// or give the existing nodeid if it is already in config.
//
static int
utilAllocateNodeid(const char *nodeName, scxcfg_nodeid_t *nodeid)
{
	int rc = SCXCFG_OK;
	int result = SCXCFG_OK;
	int num, n, i;
	f_property_t *list;
	f_property_t *v;
	std::set<int>allocatedNodeids;
	char name[FCFG_MAX_LEN];

	if (!nodeName || !nodeid) {
		return (SCXCFG_EINVAL);
	}
	num = 0;
	rc = dataAccess->getCachedListProperty("farm.nodes", &list, &num);

	if ((rc != SCXCFG_OK) && rc != (SCXCFG_ENOEXIST)) {
		scxcfg_log(LOG_ERR, "scxcfg_allocate_nodeid:error list nodes");
		return (rc);
	}
	if (rc == SCXCFG_ENOEXIST) {
		*nodeid = FIRST_FARM_NODEID;
	} else {
		*nodeid = 0;
		v = list;
		for (i = 0; (i < num) && v; i++, v = v->next) {
			n = atoi(v->value);
			if (n <= 0) {
				scxcfg_log(LOG_NOTICE, "invalid nodeid "
				    "in config");
				continue;
			}
			string p = "farm.nodes.";
			p += v->value;
			p += ".name";
			rc = dataAccess->getCachedProperty(p.c_str(), name);
			if (rc != SCXCFG_OK) {
				// No name, this is an error in config.
				scxcfg_log(LOG_NOTICE, "cannot find name"
				    " for node %s", v->value);
				// anyway put it in the "allocated" list.
			} else {
				if (strcmp(nodeName, name) == 0) {
					// node already in config
					scxcfg_log(LOG_NOTICE,
					    "request allocate nodeid "
					    "for existing node %s", name);
					*nodeid = n;
					result = SCXCFG_EEXIST;
					break;
				}
			}
			allocatedNodeids.insert(n);
		}
		// nodeName was not in config, allocate a new nodeId.
		if (*nodeid == 0) {
			std::set<int>::reverse_iterator it;
			it = allocatedNodeids.rbegin();
			if (it != allocatedNodeids.rend()) {
				*nodeid = *it + 1;
			} else {
				*nodeid = FIRST_FARM_NODEID;
			}
		}
	}
	scxcfg_freelist(list);
	return (result);
}

//
// utilAllocateIpAddrs()
//
// Utility function. Returns the IP Addresses reserved on the
// farm interconnect for this nodeid.
//
// Note: currently it allocates addresses only for config where there
// is one farm interconnect.
//
int
utilAllocateIpAddrs(int nodeid, std::list<string*> *ipAddrs)
{
	scxcfg_log(LOG_DEBUG, "dataAllocateIpAddrs");
	int rc = SCXCFG_OK;
	char name[FCFG_MAX_LEN];
	char value[FCFG_MAX_LEN];
	int num;
	f_property_t *list;
	f_property_t *v;
	// Find how many network we have in the config
	// rc = dataAccess->getCachedListProperty("farm.network", &list, &num);
	// Actually, we support only one network ...
	// if (num != 1) {
	//	scxcfg_log(LOG_NOTICE, "more than 1 network for farm");
	//	return (SCXCFG_EUNEXPECTED);
	// }

	//
	// Find network address of first farm interconnect.
	//
	rc = dataAccess->getCachedProperty("farm.network.1.address",
		name);
	if (rc != SCXCFG_OK) {
		return (rc);
	}
	string net(name);
	size_t delim = net.find(".");
	string remain = net.substr(delim+1, string::npos);
	size_t delim2 = remain.find(".");
	string prefix = net.substr(0, delim + delim2 + 1);
	//
	// Always reserve a range of 4 addresses  (ipmp case).
	//
	int n = nodeid * 4;

	int a = n / 256;
	int b = n % 256;
	sprintf(value, "%s.%d.%d", prefix.c_str(), a, b);
	string *s = new string(value);
	if (!s)
		return (SCXCFG_ENOMEM);
	ipAddrs->push_back(s);
	sprintf(value, "%s.%d.%d", prefix.c_str(), a, b + 1);
	s = new string(value);
	if (!s)
		return (SCXCFG_ENOMEM);
	ipAddrs->push_back(s);
	sprintf(value, "%s.%d.%d", prefix.c_str(), a, b + 2);
	s = new string(value);
	if (!s)
		return (SCXCFG_ENOMEM);
	ipAddrs->push_back(s);
end:
	return (SCXCFG_OK);
}

//
// scxcfg_add_node()
//
// High Level API function. Should be used only by the
// configuration daemon scxcfgd, and scconf to add a farm node.
//
// Add a farm node to the farm config, allocates a nodeid,
// creates all the needed properties, typically network properties.
//
// Input: nodename, adapterlist
// Output: nodeid
//
int
scxcfg_add_node(scxcfg_t cfg, const char *nodename, f_property_t *adapterlist,
    scxcfg_nodeid_t *nodeid, scxcfg_error_t error)
{
	scxcfg_log(LOG_DEBUG, "scxcfg_add_node");
	int result = FCFG_OK;
	if (!error)
		return (FCFG_EINVAL);
	if (!cfg || !nodename || !adapterlist ||
	    (cfg->handle != (void *)dataAccess)) {
		error->scxcfg_errno = FCFG_EINVAL;
		error->suberrno = SCXCFG_EINVAL;
		return (FCFG_EINVAL);
	}
	//
	// There must be either one adapter or two:
	//
	if ((!adapterlist->value) ||
	    (adapterlist->next && !adapterlist->next->value) ||
	    (adapterlist->next && adapterlist->next->next)) {
		error->scxcfg_errno = FCFG_EINVAL;
		error->suberrno = SCXCFG_EBADVALUE;
		return (FCFG_EINVAL);
	}
	error->scxcfg_errno = FCFG_OK;
	error->suberrno = SCXCFG_OK;

	int rc = SCXCFG_OK;
	char prefix[FCFG_MAX_LEN];
	char prop[FCFG_MAX_LEN];
	char value[FCFG_MAX_LEN];
	std::list<Prop*> nodeProps;
	std::list<string*> ipAddrs;
	int isIpmp = 0;

	//
	// If there are two adapters, this is a redundant case.
	//
	if (adapterlist->next) {
		isIpmp = 1;
	} else {
		isIpmp = 0;
	}

	rc = utilAllocateNodeid(nodename, nodeid);
	if (rc == SCXCFG_EEXIST) {
		scxcfg_log(LOG_DEBUG, "scxcfg_add_node: existing node: %s",
		    nodename);
		result = FCFG_EEXIST;
	} else if (rc != SCXCFG_OK) {
		error->scxcfg_errno = FCFG_ESYSERR;
		error->suberrno = SCXCFG_EUNEXPECTED;
		return (FCFG_ESYSERR);
	}
	scxcfg_log(LOG_DEBUG, "scxcfg_add_node:%s,nodeid %d", nodename,
	    *nodeid);

	rc = utilAllocateIpAddrs(*nodeid, &ipAddrs);
	if (rc != SCXCFG_OK) {
		error->scxcfg_errno = FCFG_ESYSERR;
		error->suberrno = rc;
		return (FCFG_ESYSERR);
	}

	sprintf(prefix, "farm.nodes.%d.", *nodeid);
	strcpy(prop, prefix);
	strcat(prop, "name");
	Prop *v = new Prop(prop, nodename);
	nodeProps.push_back(v);

	strcpy(prop, prefix);
	strcat(prop, "state");
	v = new Prop(prop, "enabled");
	nodeProps.push_back(v);

	strcpy(prop, prefix);
	strcat(prop, "monitoring_state");
	v = new Prop(prop, "enabled");
	nodeProps.push_back(v);

	strcpy(prop, prefix);
	strcat(prop, "properties.private_hostname");
	sprintf(value, "farmnode%d-priv", *nodeid);
	v = new Prop(prop, value);
	nodeProps.push_back(v);

	if (!isIpmp) {
		strcpy(prop, prefix);
		strcat(prop, "network.interface.1.type");
		v = new Prop(prop, "simple");
		nodeProps.push_back(v);

		const char *adapName = adapterlist->value;
		strcpy(prop, prefix);
		strcat(prop, "network.interface.1.name");
		v = new Prop(prop, adapName);
		nodeProps.push_back(v);

		strcpy(prop, prefix);
		strcat(prop, "network.interface.1.address");

		std::list<string*>::const_iterator it;
		it = ipAddrs.begin();
		const char *ipAddr = (*it)->c_str();
		v = new Prop(prop, ipAddr);
		nodeProps.push_back(v);
	} else {
		//
		// farm.nodes.<i>.network.interface.<j>.adapters.<k>.name.
		//
		const char *ipAddr;
		std::list<string*>::const_iterator it;

		strcpy(prop, prefix);
		strcat(prop, "network.interface.1.type");
		v = new Prop(prop, "redundant");
		nodeProps.push_back(v);

		const char *ipmpName = "scxredund0";
		strcpy(prop, prefix);
		strcat(prop, "network.interface.1.name");
		v = new Prop(prop, ipmpName);
		nodeProps.push_back(v);

		strcpy(prop, prefix);
		strcat(prop, "network.interface.1.address");
		it = ipAddrs.begin();
		ipAddr = (*it)->c_str();
		v = new Prop(prop, ipAddr);
		nodeProps.push_back(v);

		const char *adap1 = adapterlist->value;
		strcpy(prop, prefix);
		strcat(prop, "network.interface.1.adapters.1.name");
		v = new Prop(prop, adap1);
		nodeProps.push_back(v);

		strcpy(prop, prefix);
		strcat(prop, "network.interface.1.adapters.1.address");
		it++;
		ipAddr = (*it)->c_str();
		v = new Prop(prop, ipAddr);
		nodeProps.push_back(v);

		const char *adap2 = adapterlist->next->value;
		strcpy(prop, prefix);
		strcat(prop, "network.interface.1.adapters.2.name");
		v = new Prop(prop, adap2);
		nodeProps.push_back(v);

		strcpy(prop, prefix);
		strcat(prop, "network.interface.1.adapters.2.address");
		it++;
		ipAddr = (*it)->c_str();
		v = new Prop(prop, ipAddr);
		nodeProps.push_back(v);
	}

	rc = dataAccess->setListProperty(FARM_TABLE, &nodeProps);
	if (rc != SCXCFG_OK) {
		result = FCFG_ESYSERR;
		error->scxcfg_errno = FCFG_ESYSERR;
		error->suberrno = rc;
	}
	return (result);
}

//
// utilLookupNodePropertyValue()
//
// implementation of scxcfg_lookup_node_property_value()
//
// Lookup in the farm config if one node has already a property
// named propName and whose value is searchValue.
//
// Returns SCXCFG_ENOEXIST if not found, SCXCFG_OK if found.
//
int
utilLookupNodePropertyValue(const char *context, const char *propName,
    const char *searchValue, scxcfg_nodeid_t *nodeid)
{

	f_property_t *nodeList;
	f_property_t *n;
	int rc, numNodes, found, i;
	//
	// We can work this only in the cached context.
	//
	if (context) {
		if (string(context) != *(dataAccess->getCachedContext())) {
			return (FCFG_EINVAL);
		}
	}
	//
	// Get all farm nodes.
	//
	rc = dataAccess->getCachedListProperty("farm.nodes", &nodeList,
	    &numNodes);
	if ((rc != SCXCFG_OK) && rc != (SCXCFG_ENOEXIST)) {
		scxcfg_log(LOG_ERR, "lookup value:getting nodes: %d", rc);
		return (rc);
	}
	if (rc == SCXCFG_ENOEXIST) {
		scxcfg_log(LOG_NOTICE, "lookup value:no nodes in config");
		return (rc);
	}
	*nodeid = 0;
	found = 0;
	for (i = 0, n = nodeList; (i < numNodes) && n; i++, n = n->next) {
		char prop[FCFG_MAX_LEN];
		char value[FCFG_MAX_LEN];
		sprintf(prop, "farm.nodes.%s.properties.%s", n->value,
		    propName);
		rc = dataAccess->getCachedProperty(prop, value);
		if (rc != SCXCFG_OK) {
			scxcfg_log(LOG_DEBUG, "no properties %s for nid %s",
			    propName, n->value);
			continue;
		} else if (strcmp(value,  searchValue) == 0) {
			found = 1;
			*nodeid = atoi(n->value);
			break;
		}
	}
	if (found == 0) {
		return (SCXCFG_ENOEXIST);

	} else {
		return (SCXCFG_OK);
	}
}
