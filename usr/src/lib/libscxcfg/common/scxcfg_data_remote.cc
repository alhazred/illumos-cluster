/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scxcfg_data_remote.cc	1.3	08/05/20 SMI"

#include <iostream>
#include <string>
#include <set>

#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/utsname.h>
#include <errno.h>
#include <strings.h>

#define	PORTMAP
#include <rpc/rpc.h>
#include <rpc/pmap_clnt.h>

#include "scxcfg_int.h"
#include "scxcfg_data_remote.h"

using namespace std;

//
// scxcfg_data_remote
//
// This class can be used to access the farm configuration on the
// server nodes through the network, using the RPC server scxcfgd
// as the data store.
//
// Inherits from scxcfg_cached_data but is mostly use
// for adminstrative commands, like refreshing the configuration
// cache file on farm nodes, or making install requests for new farm
// nodes.
//

extern "C" int get_local_node_id(scxcfg_nodeid_t *n);
extern "C" int get_local_cluster_id(char *);

//
// scxcfg_data_remote constructor.
//
scxcfg_data_remote::scxcfg_data_remote()
{
	scxcfg_log(LOG_DEBUG, "scxcfg_data_remote::scxcfg_data_remote()");
	currentServer = "";
	clntHandle = NULL;
	sockfd = 0;
	localName = NULL;
}

//
// scxcfg_data_remote destructor.
//
scxcfg_data_remote::~scxcfg_data_remote()
{
	scxcfg_log(LOG_DEBUG, "scxcfg_data_remote::~scxcfg_data_remote()");
	if (localName)
		delete localName;

	if (clntHandle)
		clnt_destroy(clntHandle);
	if (sockfd)
		::close(sockfd);
}

//
// getLocalName()
//
// Utility function.
//
static string *
getLocalName()
{
	struct utsname u;
	int rc;

	rc = uname(&u);
	if (rc == -1) {
		scxcfg_log(LOG_ERR, "uname failed: errno %d", errno);
		return (NULL);
	} else {
		return (new string(u.nodename));
	}
}

//
// scxcfg_data_remote::init()
//
// The main work performed is to find a RPC servers on the network
// then choose the ones that corresponds to our cluster.
//
int
scxcfg_data_remote::init(const char *context)
{
	scxcfg_log(LOG_DEBUG, "scxcfg_data_remote::init()");
	lock();
	int rc = SCXCFG_OK;

	localName = getLocalName();

	// Need nodeid to ask for filter from server.
	rc = get_local_node_id(&nodeId);
	if (rc) {
		scxcfg_log(LOG_WARNING, "scxcfg_data_remote::init:"
		    " no nodeid");
		unlock();
		return (SCXCFG_EUNEXPECTED);
	}
	rc = get_local_cluster_id(clusterId);
	if (rc) {
		scxcfg_log(LOG_WARNING, "scxcfg_data_remote::init:"
		    " no cluster ID");
		unlock();
		return (SCXCFG_EUNEXPECTED);
	}

	if (currentServer == "") {
		rc = find_server();
		if (rc != SCXCFG_OK) {
			unlock();
			return (rc);
		}
	}
	scxcfg_auth_input_t input;
	scxcfg_output_t result;
	enum clnt_stat rpc_stat;

	bzero(&input, sizeof (input));
	bzero(&result, sizeof (result));

	input.node_name = strdup(localName->c_str());
	input.cluster_id = strdup(clusterId);
	input.node_id = nodeId;
	input.auth_key = strdup("");

	//
	// Find a server matching our cluster and authentication
	// information.
	//
	while (nextServer()) {
		if (!clntHandle) {
			unlock();
			return (SCXCFG_EUNEXPECTED);
		}
		rpc_stat = scxcfg_auth_farm_1(&input, &result, clntHandle);
		if (rpc_stat != RPC_SUCCESS) {
			clnt_perror(clntHandle, "call failed");
			scxcfg_log(LOG_ERR, "Auth Farm failed for: %s",
			    " RPC system error", input.node_name);
			continue;
		}
		if (result.error != SCXCFG_OK) {
			if (result.error == SCXCFG_ENOCLUSTER) {
				scxcfg_log(LOG_NOTICE, "RPC: Auth contacted "
				    "invalid server");
				continue;
			} else {
				scxcfg_log(LOG_NOTICE, "RPC: Auth Farm "
				    "failed for: %s : rc %d",
				    input.cluster_id, result.error);
				continue;
			}
		} else {
			// Authenticate succesfull
			break;
		}
	}
	if (result.error != SCXCFG_OK) {
		scxcfg_log(LOG_ERR, "auth_farm: tried all servers");
		unlock();
		return (SCXCFG_ENOCONN);
	}
	// if rc is ok , we should have a clntHandle now
	if (!clntHandle) {
		unlock();
		return (SCXCFG_EUNEXPECTED);
	}
	//
	// call parent method to initialize cache.
	//
	rc = scxcfg_cached_data::init(context);
	unlock();
	return (rc);
}

//
// scxcfg_data_remote::close()
//
int
scxcfg_data_remote::close()
{
	scxcfg_log(LOG_DEBUG, "scxcfg_data_remote::close()");
	lock();
	int rc = SCXCFG_OK;
	if (clntHandle)
		clnt_destroy(clntHandle);
	clntHandle = NULL;
	nodeId = 0;
	// call parent method
	rc = scxcfg_cached_data::close();
	unlock();
	return (rc);
}

//
// scxcfg_data_remote::fillCache()
//
int
scxcfg_data_remote::fillCache(const char *context)
{
	scxcfg_log(LOG_DEBUG, "scxcfg_data_remote::fillCache()");
	scxcfg_input_t input;
	scxcfg_bulk_output_t result;
	enum clnt_stat rpc_stat;

	input.name = (char *)context;
	input.node_id = nodeId;

	result.context = NULL;
	result.buffer = NULL;

	if (clntHandle == NULL)
		return (SCXCFG_EINVAL);

	rpc_stat = scxcfg_bulk_transfer_1(&input, &result, clntHandle);
	if (rpc_stat != RPC_SUCCESS) {
		clnt_perror(clntHandle, "call failed");
		return (SCXCFG_ESYSERR);
	}
	if (result.error != SCXCFG_OK) {
		return (result.error);
	}
	stringCache = new stringstream;
	*stringCache << result.buffer;

	// DEBUG
	// cout << "DUMP of string cache" << endl;
	// if (stringCache->fail()){
	// cout <<"stringCache FAILED" << endl;
	// }
	//	cout << stringCache->rdbuf();
	return (SCXCFG_OK);
}

//
// scxcfg_data_remote::setProperty()
//
// Set a property on the server.
//
int
scxcfg_data_remote::setProperty(const char *context, const char *key,
    const char *val)
{
	scxcfg_log(LOG_DEBUG, "scxcfg_data_remote::setProperty()");
	scxcfg_prop_input_t input;
	scxcfg_output_t result;
	enum clnt_stat rpc_stat;

	lock();
	if (clntHandle == NULL) {
		unlock();
		return (SCXCFG_EINVAL);
	}
	if (context != NULL)
		input.context = strdup(context);
	else
		input.context = NULL;

	input.key = strdup(key);
	input.value = strdup(val);

	rpc_stat = scxcfg_set_property_1(&input, &result, clntHandle);
	if (rpc_stat != RPC_SUCCESS) {
		clnt_perror(clntHandle, "call failed");
		result.error = SCXCFG_ESYSERR; }
	unlock();
	return (result.error);
}

//
// scxcfg_data_remote::setListProperty()
//
// Not implemented.
//
int
scxcfg_data_remote::setListProperty(const char *context,
    std::list<Prop*> *list)
{
	return (SCXCFG_ENOTIMPLEMENTED);
}


//
// scxcfg_data_remote::delProperty()
//
// Delete a property on the server.
//
int
scxcfg_data_remote::delProperty(const char *context, const char *key)
{
	scxcfg_log(LOG_DEBUG, "scxcfg_data_remote::delProperty()");
	scxcfg_prop_input_t input;
	scxcfg_output_t result;
	enum clnt_stat rpc_stat;

	lock();
	if (clntHandle == NULL) {
		unlock();
		return (SCXCFG_EINVAL);
	}
	if (context != NULL)
		input.context = strdup(context);
	else
		input.context = NULL;
	input.key = strdup(key);

	rpc_stat = scxcfg_del_property_1(&input, &result, clntHandle);
	if (rpc_stat != RPC_SUCCESS) {
		clnt_perror(clntHandle, "call failed");
		result.error = SCXCFG_ESYSERR;
	}
	unlock();
	return (result.error);
}

//
// scxcfg_data_remote::setListProperty()
//
// Not implemented.
//
int scxcfg_data_remote::delListProperty(const char *context,
    std::list<Prop*> *list)
{
	return (SCXCFG_ENOTIMPLEMENTED);
}


//
// scxcfg_data_remote::addServer()
//
// Add a RPC server to the server's list.
//
bool
scxcfg_data_remote::addServer(char *addr)
{
	std::set<string>::const_iterator it;
	string *s = new string(addr);

	it = serverS.find(*s);
	if (it == serverS.end()) {
		// not found yet, insert
		serverS.insert(*s);
		return (false);
	} else {
		// already seen this one,
		delete s;
		// stop now
		return (true);
	}
}

//
// scxcfg_data_remote::getServer()
//
// Get current server.
//
const char *
scxcfg_data_remote::getServer()
{
	if (currentServer == "")
		return (NULL);
	else
		return (currentServer.c_str());
}


//
// bcast_proc()
//
// Broadcast procedure callback.
//
static bool_t
bcast_proc(char *p, struct sockaddr_in *addr)
{
	char *addrA;
	bool res;

	if (!addr) {
		return (FALSE);

	} else {
		addrA = inet_ntoa((addr->sin_addr));
		// add to list
		res = ((scxcfg_data_remote*)p)->addServer(addrA);
	}
	if (res) {
		// stop searching
		return (TRUE);
	} else {
		// continue searching
		return (FALSE);
	}
}

//
// scxcfg_data_remote::find_server()
//
// Do a UDP broadcast to find a server on the network,
// bcast_proc will register all the answers for further analysis
// in the serverS list.
//
int
scxcfg_data_remote::find_server()
{
	scxcfg_log(LOG_DEBUG, "scxcfg_data_remote::find_server()");
	int rc = SCXCFG_OK;
	enum clnt_stat rpc_stat;

	serverS.clear();
	//
	// Do a broadcast on the networks.
	//
	rpc_stat = clnt_broadcast(SCXCFG_SERVER, SCXCFG_VERS, NULLPROC,
	    (xdrproc_t)xdr_void, (char *)NULL, (xdrproc_t)xdr_void,
	    (char *)this,
#ifdef linux
#define	PATCH	(bool_t (*)(char*, sockaddr_in*))
#else
#define	PATCH 	(int(*)(char*, ...))
#endif
	    PATCH bcast_proc);
#undef PATCH

	if (rpc_stat == RPC_TIMEDOUT) {
		if (serverS.empty()) {
			scxcfg_log(LOG_WARNING, "Timeout waiting for server");
			rc = SCXCFG_ETIMEDOUT;
		}
	} else if (rpc_stat != RPC_SUCCESS) {
		scxcfg_log(LOG_ERR, "clnt_broadcast failed");
		rc = SCXCFG_ESYSERR;
	}
	//
	// Initialize for the further server checking.
	//
	curS =  nextS = serverS.begin();
	return (rc);
}


//
// scxcfg_data_remote::nextServer()
//
// Advance in the list of serverS and try to connect in TCP
// mode. Exit when one connection is established, or list
// is exhausted.
//
// Returns:
//  true if connection was established.
//  false if no connection could be established.
//
//
bool
scxcfg_data_remote::nextServer()
{
	// Try to connect to TCP on given port
	struct sockaddr_in  addr;
	int rc;

	std::set<string>::const_iterator it;
	for (it = nextS; it != serverS.end(); it++) {
		const char *ipAddr = (*it).c_str();

		scxcfg_log(LOG_DEBUG, "remote init trying %s", ipAddr);
		sockfd = socket(AF_INET, SOCK_STREAM, 0);

		addr.sin_family = AF_INET;
		addr.sin_port = htons(SCXCFG_PORT);
		addr.sin_addr.s_addr = inet_addr(ipAddr);
		rc = connect(sockfd, (struct sockaddr*)&addr, sizeof (addr));
		if (rc) {
			// The server may have answered to the broadcast
			// on a interface where it does no currently listen.
			::close(sockfd);
			continue;
		}
		clntHandle = clnttcp_create(&addr, SCXCFG_SERVER,
		    SCXCFG_VERS, &sockfd, 0, 0);

		if (clntHandle == (CLIENT *) NULL) {
			scxcfg_log(LOG_ERR, "tcp client rpc: cannot"
			    " create tcp rpc client with %s", ipAddr);
			::close(sockfd);
			continue;
		} else {
			// exit, we've one that answers.
			curS = it;
			nextS = ++it;
			return (true);
		}
	}
	if ((clntHandle == (CLIENT *)NULL) ||
	    (it) == serverS.end()) {
		scxcfg_log(LOG_NOTICE, "Could not find any server");
		return (false);
	}
	return (false);
}

//
// scxcfg_data_remote::installFarmNode()
//
// Admin command, executed on a farm node, to configure
// it into the desired cluster.
// The input parameter is the RPC input parameter of
// the scxcfg_install_farm RPC procedure.
//
int
scxcfg_data_remote::installFarmNode(scxcfg_install_input_t *input,
    scxcfg_install_output_t *result)
{
	scxcfg_log(LOG_DEBUG, "scxcfg_data_remote::installFarm()");
	enum clnt_stat rpc_stat;
	int rc = SCXCFG_ENOCONN;

	if (!input || !result)
		return (SCXCFG_EINVAL);
	if (!input->node_name || !input->auth_key || !input->cluster_name)
		return (SCXCFG_EINVAL);

	lock();
	if (currentServer == "") {
		rc = find_server();
		if (rc != SCXCFG_OK) {
			unlock();
			return (rc);
		}
	}
	while (nextServer()) {
		if (!clntHandle) {
			unlock();
			return (SCXCFG_EUNEXPECTED);
		}
		rpc_stat = scxcfg_install_farm_1(input, result, clntHandle);
		if (rpc_stat != RPC_SUCCESS) {
			clnt_perror(clntHandle, "call failed");
			scxcfg_log(LOG_ERR, "Install farm failed for: %s",
			    " RPC system error", input->node_name);
			continue;
		}
		if (result->error != SCXCFG_OK) {
			if (result->error == SCXCFG_ENOCLUSTER) {
				scxcfg_log(LOG_NOTICE, "RPC: Install,"
				    " contacted invalid server");
				continue;
			} else {
				scxcfg_log(LOG_NOTICE, "RPC: Install,"
				    " failed for: %s : rc %d",
				    input->node_name, result->error);
				continue;
			}
		} else {
			// install succesfull
			rc = SCXCFG_OK;
			break;
		}
	}

	if (result->error != SCXCFG_OK) {
		scxcfg_log(LOG_ERR, "install_farm: tried all servers");
		rc = SCXCFG_ENOCONN;
	} else {
		scxcfg_log(LOG_DEBUG, "install_farm: found nodeid %d,"
		    "cluster_id %s", result->node_id, result->cluster_id);
	}
	unlock();
	return (rc);
}
