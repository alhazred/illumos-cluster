/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scxcfg_api.c	1.4	08/05/20 SMI"

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <syslog.h>
#include <string.h>
#include <stdarg.h>

#include <scxcfg/scxcfg.h>
#include "scxcfg_int.h"
#include "data_access.h"

/*
 * scxcfg_api
 *
 * API to access the Europa farm configuration.
 *
 * This file implements the C API.
 *
 * Some simple interfaces are implemented here.
 *
 * The Access to the data store is done through routines
 * provided by data_access, which uses the C++ class implmenation.
 *
 */

/*
 * Has scxcfg_open been called?
 */
static int isOpened = 0;

/*
 * Cache for the local nodeid.
 */
static scxcfg_nodeid_t localNodeid = 0;
/*
 * Is this node a farm node?
 */
int scxcfg_farm = -1;

/*
 * Log related support.
 */
#define	DEFAULT_LOG_TAG "scxcfglib"
static int logLevel = LOG_NOTICE;
static char *logTag =  DEFAULT_LOG_TAG;


/*
 * scxcfg_init()
 *
 * SCXCFG initialization function, called only once when the library
 * is loaded.  This sets scxcfg_farm global variable to determine
 * if we are running on a farm node or a server node.
 */
#ifdef __GNUC__
void scxcfg_init(void) __attribute__((constructor));
#else
void scxcfg_init(void);
#pragma init(scxcfg_init)
#endif

void
scxcfg_init(void)
{
	struct stat filestat;
	if (getenv("SCXCFG_DEBUG"))
		scxcfg_log_set_level(LOG_DEBUG);

	if (stat(SERVER_NODEID_FILE, &filestat) < 0)
		scxcfg_farm = 1;
	else
		scxcfg_farm = 0;
	scxcfg_log(LOG_DEBUG, "scxcfg_init lib: scxcfg_farm = %d\n",
	    scxcfg_farm);
}

/*
 * get_node_id()
 *
 * Helper function.
 * Returns this node's nodeid. Can be called without a previous scxcfg_open().
 * Note this is not part of API, it is called internally.
 */
int
get_local_node_id(scxcfg_nodeid_t *n)
{
	char value[FCFG_MAX_LEN];
	FILE *nodeFile;
	char *file, *s;

	if (scxcfg_farm)
		file = FARM_NODEID_FILE;
	else
		file = SERVER_NODEID_FILE;

	nodeFile = fopen(file, "r");
	if (nodeFile == NULL) {
		scxcfg_log(LOG_ERR, "get_local_node_id: no nodeid file");
		return (FCFG_ENOTFOUND);
	}
	s = fgets(value, sizeof (value), nodeFile);
	(void) fclose(nodeFile);
	if (s != NULL) {
		*n = (uint32_t)atoi(value);
	} else {
		scxcfg_log(LOG_ERR, "get_local_node_id: invalid"
		    " nodeid in file");
		return (FCFG_ENOTFOUND);
	}
	return (FCFG_OK);
}

/*
 * get_local_cluster_id()
 *
 * Returns this node's cluster_id. Valid only on farm node.
 * Can be called without a previous scxcfg_open()
 * Note this is not part of API, it is called internally.
 */
int
get_local_cluster_id(char *clusterId)
{
	char value[FCFG_MAX_LEN];
	FILE *clusterFile;
	char *file;
	uint32_t i;

	if (scxcfg_farm)
		file = FARM_CLUSTERID_FILE;
	else
		return (FCFG_EINVAL);

	clusterFile = fopen(file, "r");
	if (clusterFile == NULL) {
		scxcfg_log(LOG_ERR, "scxcfg_get_cluster_id: no clusterid file");
		return (FCFG_ENOTFOUND);
	}
	(void) fgets(value, sizeof (value), clusterFile);
	for (i = 0; (value[i] != 0) && (i < sizeof (value)); i++) {
		if (value[i] == '\n') {
			value[i] = 0;
		}
	}
	(void) fclose(clusterFile);
	(void) strcpy(clusterId, value);
	return (FCFG_OK);
}

/*
 * scxcfg_open()
 *
 * Opens the API. Retrieves the local nodeid, then calls the data_access
 * init_scxcfg_data() to init data store access.
 *
 * Note cannot open twice, otherwise FCFG_EEXIST is returned.
 */
int
scxcfg_open(scxcfg_t cfg, scxcfg_error_t error)
{
	void *v;
	int rc;

	if (!error || !cfg)
		return (FCFG_EINVAL);
	if (isOpened) {
		return (FCFG_EEXIST);
	}
	scxcfg_log_set_tag("Cluster.scxcfg.api");

	error->scxcfg_errno = FCFG_OK;
	rc = get_local_node_id(&localNodeid);
	if (rc != FCFG_OK) {
		localNodeid = 0;
		return (rc);
	}
	rc = init_scxcfg_data(&v);
	if (rc) {
		scxcfg_log(LOG_DEBUG, "scxcfg_open: init_scxcfg_data"
		    " returns: %d", rc);
		error->scxcfg_errno = FCFG_ESYSERR;
		error->suberrno = rc;
		return (FCFG_ESYSERR);
	}
	scxcfg_log(LOG_DEBUG, "scxcfg_open: data_access = 0x%x", v);
	cfg->handle = v;
	isOpened = 1;
	return (FCFG_OK);
}

/*
 * scxcfg_close()
 *
 * Calls the data_access end_scxcfg_data() to free resources
 * used for data store access.
 */
int
scxcfg_close(scxcfg_t cfg)
{
	int rc;
	scxcfg_log(LOG_DEBUG, "scxcfg_close");
	if (!isOpened || !cfg)
		return (FCFG_EINVAL);
	rc = end_scxcfg_data(cfg->handle);
	if (rc)
		return (FCFG_EINVAL);
	isOpened = 0;
	cfg->handle = NULL;
	return (FCFG_OK);
}

/*
 * scxcfg_getproperty_value()
 *
 * Call the data store to get the value of a given property.
 * The context is analogous to a table name for a data base.
 * NULL context means default context.
 */
int
scxcfg_getproperty_value(scxcfg_t scxcfg_ptr, const char *context,
	const char *property, char *value, scxcfg_error_t error)
{
	int rc;
	if (!scxcfg_ptr || !error || !property || !value ||!property[0])
		return (FCFG_EINVAL);
	error->scxcfg_errno = FCFG_OK;
	rc = dataGetProperty(context, property, value);
	if (rc == SCXCFG_ENOEXIST) {
		error->scxcfg_errno = FCFG_ENOTFOUND;
	} else if (rc != SCXCFG_OK) {
		error->scxcfg_errno = FCFG_ESYSERR;
	}
	error->suberrno = rc;
	return (error->scxcfg_errno);
}

/*
 * scxcfg_getlistproperty_value()
 *
 * Call the data store to get the list of "children" for a node
 * in the configuration tree.
 * The context is analogous to a table name for a data base.
 * NULL context means default context.
 */
int scxcfg_getlistproperty_value(scxcfg_t scxcfg_ptr, const char *context,
	const char *property, f_property_t **listvalue, int *num,
	scxcfg_error_t error)
{
	int rc;

	if (!scxcfg_ptr || !error || !property || !property[0])
		return (FCFG_EINVAL);
	error->scxcfg_errno = FCFG_OK;
	rc  = dataGetListProperty(context, property, listvalue, num);
	if (rc == SCXCFG_ENOEXIST) {
		/*
		 * No property of this type: return OK,
		 * but num is set to 0.
		 */
		error->scxcfg_errno = FCFG_OK;
	} else if (rc != SCXCFG_OK) {
		error->scxcfg_errno = FCFG_ESYSERR;
	}
	error->suberrno = rc;
	return (error->scxcfg_errno);
}

/*
 * scxcfg_setproperty_value()
 *
 * Call the data store to set the value of a given property.
 * The context is analogous to a table name for a data base.
 * NULL context means default context.
 */
int
scxcfg_setproperty_value(scxcfg_t scxcfg_ptr, const char *context,
	const char *property, const char *value, scxcfg_error_t error)
{
	int rc;
	if (!scxcfg_ptr || !error || !property || !value ||
	    !property[0] || !value[0])
		return (FCFG_EINVAL);
	error->scxcfg_errno = FCFG_OK;
	rc  = dataSetProperty(context, property, value);
	if (rc) {
		error->scxcfg_errno = FCFG_ESYSERR;
		error->suberrno = rc;
		return (FCFG_ESYSERR);
	}
	return (error->scxcfg_errno);
}

/*
 * scxcfg_delproperty_value()
 *
 * Call the data store to delete the value of a given property.
 * The context is analogous to a table name for a data base.
 * NULL context means default context.
 */
int
scxcfg_delproperty(scxcfg_t scxcfg_ptr, const char *context,
	const char *property, scxcfg_error_t error)
{
	int rc;
	if (!scxcfg_ptr || !error || !property || !property[0])
		return (FCFG_EINVAL);
	error->scxcfg_errno = FCFG_OK;
	rc = dataDelProperty(context, property);
	if (rc == SCXCFG_ENOEXIST) {
		error->scxcfg_errno = FCFG_ENOTFOUND;
	} else if (rc != SCXCFG_OK) {
		error->scxcfg_errno = FCFG_ESYSERR;
	}
	error->suberrno = rc;
	return (error->scxcfg_errno);
}

/*
 * scxcfg_geterrno()
 */
int
scxcfg_geterrno(scxcfg_error_t error)
{
	if (!error)
		return (FCFG_EINVAL);
	return (FCFG_OK);
}

/*
 * scxcfg_error2string()
 *
 * Returns a string for a given error.
 */
char *
scxcfg_error2string(scxcfg_error_t error)
{
	static char msg[256];
	if (!error)
		return (NULL);
	(void) sprintf(msg, "scxcfg error %d:%d", error->scxcfg_errno,
	    error->suberrno);
	return (msg);
}

/*
 *  scxcfg_get_local_nodeid()
 *
 *  can be called without a previous scxcfg_open()
 */
int
scxcfg_get_local_nodeid(scxcfg_nodeid_t *nodeid, scxcfg_error_t error)
{
	scxcfg_nodeid_t n = 0;
	int rc = FCFG_OK;
	if (!error || !nodeid)
		return (FCFG_EINVAL);
	error->scxcfg_errno = FCFG_OK;
	if (!localNodeid) {
		rc = get_local_node_id(&n);
		if (rc != FCFG_OK)
			return (rc);
		else
			*nodeid = n;
	} else {
		*nodeid = localNodeid;
	}
	return (rc);
}

int
scxcfg_get_local_nodename(scxcfg_t scxcfg_ptr, char *nodename,
    scxcfg_error_t error)
{
	int rc;
	char prop[128];
	if (!scxcfg_ptr || !nodename || !error)
		return (FCFG_EINVAL);
	if (!localNodeid) {
		error->scxcfg_errno = FCFG_ESYSERR;
		return (FCFG_ESYSERR);
	}
	error->scxcfg_errno = FCFG_OK;
	(void) sprintf(prop, "farm.nodes.%d.name", localNodeid);
	rc = dataGetProperty(NULL, prop, nodename);
	if (rc == SCXCFG_ENOEXIST) {
		error->scxcfg_errno = FCFG_ENOTFOUND;
	} else if (rc != SCXCFG_OK) {
		error->scxcfg_errno = FCFG_ESYSERR;
	}
	error->suberrno = rc;
	return (error->scxcfg_errno);
}

int
scxcfg_get_ipaddress(scxcfg_t scxcfg_ptr, scxcfg_nodeid_t nodeid,
    char *ip, scxcfg_error_t error)
{
	int rc;
	char prop[128];

	if (!scxcfg_ptr || !error || !ip) {
		return (FCFG_EINVAL);
	}
	error->scxcfg_errno = FCFG_OK;
	if (nodeid >= FIRST_FARM_NODEID) {
		(void) sprintf(prop,
		    "farm.nodes.%d.network.interface.1.address", nodeid);
	} else {
		(void) sprintf(prop,
		    "farm.servers.%d.network.interface.1.address", nodeid);
	}
	rc = dataGetProperty(NULL, prop, ip);
	if (rc == SCXCFG_ENOEXIST) {
		error->scxcfg_errno = FCFG_ENOTFOUND;
	} else if (rc != SCXCFG_OK) {
		error->scxcfg_errno = FCFG_ESYSERR;
	}
	error->suberrno = rc;
	return (error->scxcfg_errno);
}

void
scxcfg_freelist(f_property_t *flist)
{
	f_property_t *p1, *p2;
	if (!flist)
		return;
	p1 = flist;
	while (p1) {
		p2 = p1->next;
		free((void *)p1->value);
		free((void *)p1);
		p1 = p2;
	}
}

int
scxcfg_del_subtree(scxcfg_t scxcfg_ptr, const char *root,
    scxcfg_error_t error)
{
	int rc;
	if (!scxcfg_ptr || !error || !root) {
		return (FCFG_EINVAL);
	}
	error->scxcfg_errno = FCFG_OK;
	rc = utilDeleteSubtree(root);
	if (rc == SCXCFG_ENOEXIST) {
		error->scxcfg_errno = FCFG_ENOTFOUND;
	} else if (rc != SCXCFG_OK) {
		error->scxcfg_errno = FCFG_ESYSERR;
	}
	error->suberrno = rc;
	return (error->scxcfg_errno);
}

int
scxcfg_del_node(scxcfg_t scxcfg_ptr, const char *nodename,
    scxcfg_error_t error)
{
	int rc;
	if (!scxcfg_ptr || !error || !nodename) {
		return (FCFG_EINVAL);
	}
	error->scxcfg_errno = FCFG_OK;
	rc = utilDeleteNode(nodename);
	if (rc == SCXCFG_ENOEXIST) {
		error->scxcfg_errno = FCFG_ENOTFOUND;
	} else if (rc != SCXCFG_OK) {
		error->scxcfg_errno = FCFG_ESYSERR;
	}
	error->suberrno = rc;
	return (error->scxcfg_errno);
}


int
scxcfg_get_nodeid(scxcfg_t scxcfg_ptr, const char *nodename,
    scxcfg_nodeid_t *nodeid, scxcfg_error_t error)
{
	int rc;
	if (!scxcfg_ptr || !error || !nodename || !nodeid) {
		return (FCFG_EINVAL);
	}
	error->scxcfg_errno = FCFG_OK;
	rc = utilGetNameNodeid(nodename, nodeid);
	if (rc == SCXCFG_ENOEXIST) {
		error->scxcfg_errno = FCFG_ENOTFOUND;
	} else if (rc != SCXCFG_OK) {
		error->scxcfg_errno = FCFG_ESYSERR;
	}
	error->suberrno = rc;
	return (error->scxcfg_errno);
}

int
scxcfg_get_nodename(scxcfg_t scxcfg_ptr, const scxcfg_nodeid_t nodeid,
    char *nodename, scxcfg_error_t error)
{
	int rc;

	if (!scxcfg_ptr || !error || !nodename || !nodeid) {
		return (FCFG_EINVAL);
	}
	error->scxcfg_errno = FCFG_OK;
	rc = utilGetNodeidName(nodeid, nodename);
	if (rc == SCXCFG_ENOEXIST) {
		error->scxcfg_errno = FCFG_ENOTFOUND;
	} else if (rc != SCXCFG_OK) {
		error->scxcfg_errno = FCFG_ESYSERR;
	}
	error->suberrno = rc;
	return (error->scxcfg_errno);
}

int
scxcfg_isfarm()
{
	return (scxcfg_farm);
}

int
scxcfg_isfarm_nodeid(scxcfg_nodeid_t n)
{
	return ((n >= FIRST_FARM_NODEID) ? 1 : 0);
}

/*
 * scxcfg_lookup_node_property_value()
 *
 * Search for all farm nodes in config if the property propName
 * has the value searchValue.
 *
 * If found, returns FCFG_OK, and nodeid is filled
 * with corresponding node id.
 * If not found returns FCFG_ENOTFOUND
 */
int
scxcfg_lookup_node_property_value(scxcfg_t cfg, const char *context,
    const char *propName, const char *searchValue,
    scxcfg_nodeid_t *nodeid, scxcfg_error_t error)
{
	int rstatus = FCFG_OK;
	int rc;
	if (!error)
		return (FCFG_EINVAL);
	if (!cfg || !propName || !searchValue || !nodeid) {
		error->scxcfg_errno = FCFG_EINVAL;
		error->suberrno = SCXCFG_EINVAL;
		return (FCFG_EINVAL);
	}
	rc = utilLookupNodePropertyValue(context, propName, searchValue,
	    nodeid);
	switch (rc) {
	case SCXCFG_ENOEXIST:
		rstatus = FCFG_ENOTFOUND;
		break;
	case SCXCFG_OK:
		rstatus = FCFG_OK;
		break;
	default:
		rstatus = FCFG_ESYSERR;
		break;
	}
	error->scxcfg_errno = rstatus;
	error->suberrno = rc;
	return (rstatus);
}

/*
 * Log support related routines.
 */
void
scxcfg_log_get_level(int *level)
{
	*level = logLevel;
}

void
scxcfg_log_set_level(int level)
{
	logLevel = level;
}

void
scxcfg_log_set_tag(char *tag)
{
	if (!tag)
		return;
	/* only first call initialize tag */
	if (strcmp(logTag, DEFAULT_LOG_TAG) == 0)
		logTag = strdup(tag);
}

void
scxcfg_log(int const pri, char const * const fmt, ...)
{
	va_list ap;
	if (pri > logLevel)
		return;
	va_start(ap, fmt);
	openlog(logTag, LOG_CONS | LOG_PID, LOG_DAEMON);
	vsyslog(pri, fmt, ap);
	closelog();
	va_end(ap);
}
