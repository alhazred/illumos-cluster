/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scxcfg_data_ccr.cc	1.3	08/05/20 SMI"

#include <fstream>
#include <iostream>
#include <string>

#include <strings.h>

#include <orb/invo/common.h>
#include <nslib/ns.h>

#include "scxcfg_data_ccr.h"
#include "scxcfg_int.h"


//
// scxcfg_data_ccr : implementation of the scxcfg_cached_data
// using the Sun Cluster CCR backstore.
//

//
// scxcfg_data_ccr_alloc()
//
// Extra "C" allocator used when dynamically loaded (dlopen).
// C code can call this entry point to create an object  of class
// "scxcfg_data_ccr".
//
extern "C" {
scxcfg_data_ccr *scxcfg_data_ccr_alloc();
}

scxcfg_data_ccr *
scxcfg_data_ccr_alloc()
{
	return (new scxcfg_data_ccr);
}

//
// filter()
//
// Utility function used to filter the properties that
// concern a particuliar nodeid.
// Keep only the global farm properties (servers, network, properties)
// and the properties farm.node.<nodeid>
//
static int
filter(scxcfg_nodeid_t n, char*key)
{

	char nodeBegin[FCFG_MAX_LEN];
#define	NETPROPS	"farm.network."
#define	SERVERPROPS	"farm.servers."
#define	FARMPROPS	"farm.properties."
#define	NODE		"farm.nodes.%d."
	sprintf(nodeBegin, NODE, n);
	if ((strncmp(key, FARMPROPS, strlen(FARMPROPS)) == 0) ||
	    (strncmp(key, NETPROPS, strlen(NETPROPS)) == 0) ||
	    (strncmp(key, SERVERPROPS, strlen(SERVERPROPS)) == 0) ||
	    (strncmp(key, nodeBegin, strlen(nodeBegin)) == 0)) {
		return (1);
	}
	return (0);
}


//
// scxcfg_data_ccr constructor.
//
scxcfg_data_ccr::scxcfg_data_ccr()
{
	scxcfg_log(LOG_DEBUG, "scxcfg_data_ccr::scxcfg_data_ccr()");
	lock();
	namingCtxp = NULL;
	ccr_dir = NULL;
	tablePtr = NULL;
	updatePtr = NULL;
	unlock();
}

//
// scxcfg_data_ccr destructor.
//
scxcfg_data_ccr::~scxcfg_data_ccr()
{
	scxcfg_log(LOG_DEBUG, "scxcfg_data_ccr::~scxcfg_data_ccr()");
}


//
// scxcfg_data_ccr::init()
//
// Initialize access to the CCR.
//
int
scxcfg_data_ccr::init(const char *context)
{
	if (!context)
		return (SCXCFG_EINVAL);
	scxcfg_log(LOG_DEBUG, "scxcfg_data_ccr::init() %s", context);
	CORBA::Object_var	obj;
	CORBA::Exception*excep = NULL;
	int rc = SCXCFG_OK;

	lock();
	if (ORB::initialize() != 0) {
		unlock();
		return (SCXCFG_ESYSERR);
	}
	//
	// look up ccr directory in name server
	//
	namingCtxp = ns::local_nameserver();
	obj = namingCtxp->resolve("ccr_directory", e);
	if ((excep = e.exception()) != NULL) {
		rc = convert_ccr_error(excep);
		e.clear();
		unlock();
		return (rc);
	}
	ccr_dir = ccr::directory::_narrow(obj);

	//
	// Lookup the table named context.
	//
	int create = 0;
	tablePtr = ccr_dir->lookup(context, e);
	if ((excep = e.exception()) != NULL) {
		//
		// The only exception supported is that
		// the farm table does not exist.
		//
		if ((ccr::no_such_table::_exnarrow(excep) != NULL) &&
		    (strcmp(context, FARM_TABLE) == 0)) {
			e.clear();
			create = 1;
		} else {
			rc = convert_ccr_error(excep);
			e.clear();
			unlock();
			return (rc);
		}
	} else {
		currentVersion = tablePtr->get_gennum(e);
		CORBA::release(tablePtr);
	}

	tablePtr = NULL;

	if (create) {
		rc = create_farm_table();
	} if (rc != SCXCFG_OK) {
		unlock();
		return (rc);
	}
	//
	// Call parent-class method.
	//
	rc = scxcfg_cached_data::init(context);
	unlock();
	return (rc);
}

//
// scxcfg_data_ccr::close()
//
int
scxcfg_data_ccr::close()
{
	scxcfg_log(LOG_DEBUG, "scxcfg_data_ccr::close()");
	lock();
	int rc = SCXCFG_OK;
	// release any pending exception
	e.exception();
	ccr_dir = NULL;
	if (tablePtr)
		CORBA::release(tablePtr);
	tablePtr = NULL;
	if (updatePtr)
		CORBA::release(updatePtr);
	updatePtr = NULL;
	namingCtxp = NULL;
	//
	// Call parent-class method.
	//
	rc = scxcfg_cached_data::close();
	unlock();
	return (rc);
}

//
//  Implementation of virtual method of scxcfg_cached_data
//

//
// scxcfg_data_ccr::fillCache()
//
int
scxcfg_data_ccr::fillCache(const char *context)
{
	scxcfg_log(LOG_DEBUG, "scxcfg_data_ccr::fillCache()");
	int rc;
	char *buf;

	if (ccr_dir == NULL)
		return (SCXCFG_ENOINIT);

	rc = read_table(context, &buf, 0);
	if (rc != SCXCFG_OK) {
		// XX
		return (SCXCFG_ESYSERR);
	}
	stringCache = new std::stringstream;
	*stringCache << buf;
	// DEBUG
	// cout << "DUMP of string cache" << endl;
	// cout << stringCache->rdbuf();
	free(buf);
	return (rc);
}

//
// scxcfg_data_ccr::verifyUpdate()
//
// Verify if the underlying table has changed. If it has changed,
// reload the cache.
//
int
scxcfg_data_ccr::checkForUpdate()
{
	CORBA::Exception*excep = NULL;
	int newVersion;
	int rc;
	lock();
	if (!cachedContext) {
		unlock();
		return (SCXCFG_ENOINIT);
	}
	tablePtr = ccr_dir->lookup(cachedContext->c_str(), e);
	if ((excep = e.exception()) != NULL) {
		rc = convert_ccr_error(excep);
		e.clear();
		tablePtr = NULL;
		unlock();
		return (rc);
	}
	newVersion = tablePtr->get_gennum(e);
	CORBA::release(tablePtr);
	tablePtr = NULL;
	if ((newVersion != currentVersion) && (newVersion >= 0)) {
		scxcfg_log(LOG_DEBUG, "newVersion %d", newVersion);
		unlock();
		// delete old information.
		close();
		// reload all, still from the same context.
		rc = init(cachedContext->c_str());
		return (rc);
	}
	unlock();
	return (SCXCFG_OK);
}

//
// Implementation for virtual method of scxcfg_cached_data
//

//
// scxcfg_data_ccr::ggetProperty()
//
int
scxcfg_data_ccr::getProperty(const char *context, const char *key,
    char *val)
{
	scxcfg_log(LOG_DEBUG, "scxcfg_data_ccr::getProperty %s %s",
	    (context == NULL) ? "nil" : context, key);
	int rc;
	lock();
	if ((context == NULL) || (context[0] == 0)) {
		context = FARM_TABLE;
	}
	rc = read_element(context, key, val);
	unlock();
	return (rc);
}


//
// scxcfg_data_ccr::setProperty()
//
int
scxcfg_data_ccr::setProperty(const char *context, const char *key,
    const char *val)
{
	scxcfg_log(LOG_DEBUG, "scxcfg_data_ccr::setProperty %s %s %s",
	    (context == NULL) ? "nil" : context, key, val);
	int rc;
	lock();
	if ((context == NULL) || (context[0] == 0)) {
		context = FARM_TABLE;
	}
	std::list<Prop*> oneProp;
	Prop *v = new Prop(key, val);
	oneProp.push_back(v);
	rc = write_elements(context, &oneProp);
	unlock();
	return (rc);
}

//
// scxcfg_data_ccr::setListProperty()
//
int
scxcfg_data_ccr::setListProperty(const char *context, std::list<Prop*> *list)
{
	scxcfg_log(LOG_DEBUG, "scxcfg_data_ccr::setListProperty");
	int rc;
	lock();
	if ((context == NULL) || (context[0] == 0)) {
		context = FARM_TABLE;
	}
	rc =  write_elements(context, list);
	unlock();
	return (rc);
}

//
// scxcfg_data_ccr::delProperty()
//
int
scxcfg_data_ccr::delProperty(const char *context, const char *key)
{
	scxcfg_log(LOG_DEBUG, "scxcfg_data_ccr::delProperty");
	int rc;
	lock();
	if ((context == NULL) || (context[0] == 0)) {
		context = FARM_TABLE;
	}
	std::list<Prop*> oneProp;
	Prop *v = new Prop(key, "");
	oneProp.push_back(v);
	rc = remove_elements(context, &oneProp);
	unlock();
	return (rc);
	// oneProp destructor deletes v
}

//
// scxcfg_data_ccr::delListProperty()
//
int scxcfg_data_ccr::delListProperty(const char *context,
    std::list<Prop*> *list)
{
	scxcfg_log(LOG_DEBUG, "scxcfg_data_ccr::delListProperty");
	int rc;
	lock();
	if ((context == NULL) || (context[0] == 0)) {
		context = FARM_TABLE;
	}
	rc = remove_elements(context, list);
	unlock();
	return (rc);
}

//
// scxcfg_data_ccr::getConfig()
//
// Get config raw data as a string returned in buf.
//
int
scxcfg_data_ccr::getConfig(const char *context, char **buf,
    const scxcfg_nodeid_t n)
{
	scxcfg_log(LOG_DEBUG, "scxcfg_data_ccr::getConfig");
	int rc;
	if ((context == NULL) || (context[0] == 0)) {
		context = FARM_TABLE;
	}
	lock();
	rc = read_table(context, buf, n);
	unlock();
	return (rc);
}

//
// scxcfg_data_ccr::deleteConfig()
//
int
scxcfg_data_ccr::deleteConfig(const char *tableName)
{
	int rc = SCXCFG_OK;
	scxcfg_log(LOG_DEBUG, "scxcfg_data_ccr::deleteConfig");
	if (!tableName || strcmp(tableName, FARM_TABLE) != 0) {
		return (SCXCFG_EINVAL);
	}
	lock();
	rc = delete_table(tableName);
	unlock();
	return (rc);
}

//
// Utility methods to access directly CCR tables.
//

//
// scxcfg_data_ccr::create_farm_table()
//
scxcfg_errno_t
scxcfg_data_ccr::create_farm_table()
{
	CORBA::Exception*excep = NULL;
	scxcfg_errno_t	res = SCXCFG_OK;

	ccr_dir->create_table(FARM_TABLE, e);
	if ((excep = e.exception()) != NULL) {
		res = convert_ccr_error(excep);
		e.clear();
		scxcfg_log(LOG_ERR, "Cannot create farm table");
		return (res);
	}
	scxcfg_log(LOG_INFO, "Created farm table");

	tablePtr = ccr_dir->lookup(FARM_TABLE, e);
	if ((excep = e.exception()) != NULL) {
		scxcfg_log(LOG_ERR, "Cannot access created farm table");
		e.clear();
	} else {
		currentVersion = tablePtr->get_gennum(e);
		CORBA::release(tablePtr);
	}
	tablePtr = NULL;
	scxcfg_log(LOG_DEBUG, "CurrentVersion %d", currentVersion);
	return (SCXCFG_OK);
}

//
// scxcfg_data_ccr::delete_table()
//
scxcfg_errno_t
scxcfg_data_ccr::delete_table(const char *table_name)
{
	scxcfg_log(LOG_DEBUG, "scxcfg_data_ccr::delete_table");
	CORBA::Exception	*excep = NULL;
	scxcfg_errno_t	res = SCXCFG_OK;
	ccr_dir->remove_table(table_name, e);
	if ((excep = e.exception()) != NULL) {
		e.clear();
		res = convert_ccr_error(excep);
	}
	return (res);
}

//
// scxcfg_data_ccr::read_table()
//
// Read a raw table.  Optionally uses filtering nodeid (this filtering
// nodeid is meaningful only is table_name is the farm table).
// The filtering should probably be done
// by the consumer of this function. (e.g config daemon).
//
// If filterNodeId is 0, there is no filtering.
//
// buf is allocated here, and must be freed by the caller.
//
scxcfg_errno_t
scxcfg_data_ccr::read_table(const char *table_name, char **buf,
    const scxcfg_nodeid_t filterNodeId)
{
	if (!table_name || !buf)
		return (SCXCFG_EINVAL);
	scxcfg_log(LOG_DEBUG, "scxcfg_data_ccr::read_table: %s", table_name);
	CORBA::Exception	*excep = NULL;
	scxcfg_errno_t	res = SCXCFG_OK;
	int i = 0, size = 0;
	int num_rows = 0;
	ccr::table_element  *outelem = NULL;

	// lookup table
	tablePtr = ccr_dir->lookup(table_name, e);
	if ((excep = e.exception()) != NULL) {
		res = convert_ccr_error(excep);
		e.clear();
		tablePtr = NULL;
		return (res);
	}
	ASSERT(tablePtr);

	num_rows = tablePtr->get_num_elements(e);
	// iterate through table
	tablePtr->atfirst(e);
	if ((excep = e.exception()) != NULL) {
		res = convert_ccr_error(excep);
		e.clear();
		CORBA::release(tablePtr);
		tablePtr = NULL;
		return (res);
	}
	// Read table one row at a time and
	// keep only those that match filter,
	// put htme in following list.
	std::list<ccr::table_element*> elems;

	for (i = 0; i < num_rows; i++) {
		/* Read the row element */
		tablePtr->next_element(outelem, e);
		if ((excep = e.exception()) != NULL) {
			res = convert_ccr_error(excep);
			e.clear();
			CORBA::release(tablePtr);
			tablePtr = NULL;
			return (res);
		}
		if ((filterNodeId == 0) ||
		    filter(filterNodeId, outelem->key))
			elems.push_back(outelem);
	}
	// calculate size needed
	std::list<ccr::table_element*>::const_iterator it;
	size = 0;
	it = elems.begin();
	while (it != elems.end()) {
		size += strlen((*it)->key);
		size += strlen((*it)->data);
		size += 2;
		it++;
	}
	// allocate
	*buf = (char*)malloc(size+1);
	if (*buf == NULL) {
		res = SCXCFG_ENOMEM;
		CORBA::release(tablePtr);
		tablePtr = NULL;
		return (res);
	}
	// copy all in one string
	char *p = *buf;
	it = elems.begin();
	while (it != elems.end()) {
		strcpy(p, (*it)->key);
		p += strlen((*it)->key);
		*p++ = ' ';
		strcpy(p, (*it)->data);
		p += strlen((*it)->data);
		*p++ = '\n';
		it++;
	}
	*p++ = 0;

	// DEBUG
	// stringstream stringBuffer(*buf);
	// cout << "read_table: DUMP of string Buffer" << endl;
	// cout << stringBuffer.rdbuf();

	CORBA::release(tablePtr);
	tablePtr = NULL;
	return (res);
	// The container list "elems" going out of scope will
	// call  destructor for each of its elements (outelem)
	// allocated  by CCR.
}

//
// scxcfg_data_ccr::read_element()
//
// Read a value for a given key in the CCR table table_name.
//
scxcfg_errno_t
scxcfg_data_ccr::read_element(const char *table_name, const char *key,
    char *value)
{
	if (!table_name || !key || !value) {
		return (SCXCFG_EINVAL);
	}
	scxcfg_log(LOG_DEBUG, "scxcfg_data_ccr::read_element %s %s",
	    table_name, key);
	CORBA::Exception *excep = NULL;
	scxcfg_errno_t res = SCXCFG_OK;
	char *data = NULL;

	tablePtr = ccr_dir->lookup(table_name, e);
	if ((excep = e.exception()) != NULL) {
		res = convert_ccr_error(excep);
		e.clear();
		tablePtr = NULL;
		return (res);
	}
	data = tablePtr->query_element(key, e);
	if ((excep = e.exception()) != NULL) {
		res = convert_ccr_error(excep);
		e.clear();
		goto cleanup;
	}
	if (data == NULL) {
		res = SCXCFG_ESYSERR;
		goto cleanup;
	}
	strcpy(value, data);
cleanup:
	if (data) delete [] data;
	CORBA::release(tablePtr);
	tablePtr = NULL;
	return (res);
}

//
// scxcfg_data_ccr::write_elements()
//
// Update or create the entries in the CCR table corresponding to the
// properties of the list listv.
//
scxcfg_errno_t
scxcfg_data_ccr::write_elements(const char *table_name,
    std::list<Prop*> *listv)
{
	if (!table_name || !listv || listv->empty()) {
		return (SCXCFG_EINVAL);
	}
	scxcfg_log(LOG_DEBUG, "scxcfg_data_ccr::write_elements: %s",
	    table_name);
	CORBA::Exception *excep = NULL;
	scxcfg_errno_t res = SCXCFG_OK;
	std::list<Prop*>::const_iterator it;
	const char *key,  *value;
	int create = 0;

	tablePtr = ccr_dir->lookup(table_name, e);
	if ((excep = e.exception()) != NULL) {
		res = convert_ccr_error(excep);
		e.clear();
		tablePtr = NULL;
		return (res);
	}

	updatePtr = ccr_dir->begin_transaction(table_name, e);
	if ((excep = e.exception()) != NULL) {
		res = convert_ccr_error(excep);
		e.clear();
		goto cleanup;
	}

	for (it = listv->begin(); it != listv->end(); it++) {
		key = (*it)->name.c_str();
		value = (*it)->value.c_str();
		create = 0;
		// cout << "key:" << key << endl;
		// cout << "value:" << value << endl;
		tablePtr->query_element(key, e);
		if ((excep = e.exception()) != NULL) {
			if (ccr::no_such_key::_exnarrow(excep) != NULL) {
				create = 1;
				e.clear();
			} else {
				res = convert_ccr_error(excep);
				e.clear();
				break;
			}
		}
		if (value == NULL) {
			value = "";
		}
		if (create) {
			updatePtr->add_element(key, value, e);
			if ((excep = e.exception()) != NULL) {
				res = convert_ccr_error(excep);
				e.clear();
				break;
			}
		} else {
			updatePtr->update_element(key, value, e);
			if ((excep = e.exception()) != NULL) {
				res = convert_ccr_error(excep);
				e.clear();
				break;
			}
		}
	}
	if (res != SCXCFG_OK) {
		updatePtr->abort_transaction(e);
	} else {
		updatePtr->commit_transaction(e);
	}
	if ((excep = e.exception()) != NULL) {
		res = convert_ccr_error(excep);
		e.clear();
	}
cleanup:
	CORBA::release(tablePtr);
	CORBA::release(updatePtr);
	tablePtr = NULL;
	updatePtr = NULL;
	return (res);
}

//
// scxcfg_data_ccr::remove_elements()
//
// Delete the entries in the CCR table corresponding to the
// properties of the list listv.
//
scxcfg_errno_t
scxcfg_data_ccr::remove_elements(const char *table_name,
    std::list<Prop*> *listv)
{
	scxcfg_log(LOG_DEBUG, "scxcfg_data_ccr::remove_elements()");
	CORBA::Exception *excep = NULL;
	scxcfg_errno_t res = SCXCFG_OK;
	std::list<Prop*>::const_iterator it;
	const char *key;

	if (!table_name || !listv || listv->empty()) {
		return (SCXCFG_EINVAL);
	}

	updatePtr = ccr_dir->begin_transaction(table_name, e);
	if ((excep = e.exception()) != NULL) {
		res = convert_ccr_error(excep);
		e.clear();
		return (res);
	}
	for (it = listv->begin(); it != listv->end(); it++) {
		key = (*it)->name.c_str();

		updatePtr->remove_element(key, e);
		if ((excep = e.exception()) != NULL) {
			res = convert_ccr_error(excep);
			e.clear();
			break;
		}
	}
	if (res != SCXCFG_OK) {
		updatePtr->abort_transaction(e);
	} else {
		updatePtr->commit_transaction(e);
	}
	if ((excep = e.exception()) != NULL) {
		res = convert_ccr_error(excep);
		e.clear();
	}
cleanup:
	CORBA::release(updatePtr);
	updatePtr = NULL;
	return (res);
}

//
// scxcfg_data_ccr::convert_ccr_error()
//
// Translate CCR exceptions to scxcfg errors.
//
static scxcfg_errno_t
scxcfg_data_ccr::convert_ccr_error(CORBA::Exception *ex)
{
	char msg[128];
	scxcfg_errno_t res;
	int level = LOG_ERR;

	if (ex == NULL) {
		sprintf(msg, "convert_ccr_error: invalid NULL except pointer");
		res = SCXCFG_EINVAL;
	} else if (ccr::no_such_table::_exnarrow(ex) != NULL) {
		sprintf(msg, "no such table");
		res = SCXCFG_ENOENT;
	} else if (ccr::table_exists::_exnarrow(ex) != NULL) {
		sprintf(msg, "table exist");
		res = SCXCFG_EEXIST;
	} else if (ccr::invalid_table_name::_exnarrow(ex) != NULL) {
		sprintf(msg, "invalid table name");
		res = SCXCFG_EINVAL;
	} else if (ccr::no_such_key::_exnarrow(ex) != NULL) {
		sprintf(msg, "no such key");
		level = LOG_DEBUG;
		res = SCXCFG_ENOEXIST;
	} else if (ccr::key_exists::_exnarrow(ex) != NULL) {
		sprintf(msg, "key exist");
		res = SCXCFG_EEXIST;
	} else if (ccr::table_invalid::_exnarrow(ex) != NULL) {
		sprintf(msg, "table invalid ");
		res = SCXCFG_EINVAL;
	} else if (ccr::table_modified::_exnarrow(ex) != NULL) {
		sprintf(msg, "table_modified:");
		level = LOG_DEBUG;
		res = SCXCFG_EMODIFIED;
	} else if (ccr::readonly_access::_exnarrow(ex) != NULL) {
		sprintf(msg, "readonly_access");
		res = SCXCFG_EPERM;
	} else if ((ccr::out_of_service::_exnarrow(ex) != NULL) ||
	    ccr::database_invalid::_exnarrow(ex) != NULL) {
		sprintf(msg, "out_of_service");
		res = SCXCFG_ECORRUPT;
	} else if (ccr::lost_quorum::_exnarrow(ex) != NULL) {
		sprintf(msg, "lost_quorum");
		res = SCXCFG_EIO;
	} else {
		sprintf(msg, "Other exception");
		res = SCXCFG_EUNEXPECTED;
	}
	scxcfg_log(level, "CORBA:exception %s", msg);
	return (res);
}
