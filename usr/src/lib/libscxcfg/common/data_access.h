/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_DATA_ACCESS_H
#define	_DATA_ACCESS_H

#pragma ident	"@(#)data_access.h	1.3	08/05/20 SMI"

#include <scxcfg/scxcfg.h>

#ifdef	__cplusplus
extern "C" {
#endif

int init_scxcfg_data(void **v);
int end_scxcfg_data(void *);
int dataGetProperty(const char *context, const char *prop, char *val);
int dataGetListProperty(const char *context, const char *prop,
    f_property_t ** listvalue, int*cnt);
int dataSetProperty(const char *context, const char *prop, const char *val);
int dataDelProperty(const char *context, const char *prop);
int utilDeleteNode(const char *nodename);
int utilDeleteSubtree(const char *root);
int utilGetNameNodeid(const char *nodename, scxcfg_nodeid_t *nodeid);
int utilGetNodeidName(const scxcfg_nodeid_t nodeid, char *nodename);
int utilLookupNodePropertyValue(const char *context, const char *propName,
    const char *searchValue, scxcfg_nodeid_t *nodeid);
int scxcfg_add_node(scxcfg_t cfg, const char *nodename,
    f_property_t *adapterlist, scxcfg_nodeid_t *nodeid,
    scxcfg_error_t error);

#ifdef	__cplusplus
}
#endif

#endif /* _DATA_ACCESS_H */
