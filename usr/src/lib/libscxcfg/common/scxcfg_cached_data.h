/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_SCXCFG_CACHED_DATA_H
#define	_SCXCFG_CACHED_DATA_H

#pragma ident	"@(#)scxcfg_cached_data.h	1.3	08/05/20 SMI"

#include <set>
#include <list>
#include <sstream>
#include <string>
#include <scxcfg/scxcfg.h>
#include "scxcfg_data.h"

//
//  scxcfg_node: helper class to build a tree of properties
//  within a cached scxcfg_data access class
//
class scxcfg_node
{
public:
	static const char separator = '.';
	// Traverse the tree starting from this. Calls callback and passes it
	// the current node pointer and the user specified arg. It stops when
	// it is done with the tree or when the callback returns zero.
	void traverse(int (*callback)(scxcfg_node *, void *arg), void *arg);
	// returns a child by relative name of this node. Returns
	// NULL if not found. The name doesn't contain this node (parent)
	// The name is just one level deep (doesn't contain seperators).
	scxcfg_node *get_named_child(const char *);
	// returns a child by relative name of this node. Create if
	// necessary. Returns NULL if the name is bad. The name doesn't
	// contain this node (parent).
	scxcfg_node *get_named_child_create(const char *);
	// Get a child by the relative name. Returns NULL if not found.
	// The name given is one that starts from this (include this).
	scxcfg_node *get_child_by_name(const char *);
	// Get a child by the relative name. Create child if not found.
	// The name given is one that starts from this (including this).
	// The name may contain seperators and as a result we may create
	// multiple children of different depth.
	scxcfg_node *get_child_by_name_create(const char *);
	// returns the parent, NULL if this is root.
	scxcfg_node *get_parent();
	// Returns the value stored in this node.
	const char *get_value();
	// returns the name of this node.
	const char *get_name();
	// returns the full name of this node. The full name starts
	// from the root node and may contain seperator. For example,
	// "farm.nodes.1" is the full name for node_1.
	char *get_full_name();
	// sets the value of the specified node. Error if node
	// isn't a leaf node. Returns true for success, false for fail.
	bool set_value(const char *);
	// Set up the parent pointer. It may be overloaded if a child class
	// want to setup some more info based on the parent.
	virtual void set_parent(scxcfg_node *);
	// Adds a tree (w/ perhaps only one node) onto existing tree...
	// will fail if name of node is duplicated. Returns true for
	// success, false for fail.
	bool add(scxcfg_node *);
	// Removes the specified node (and any children) from the tree.
	// Returns true if the node is found in the tree. Returns false
	// if it's not in the tree. It just remove the node from the
	// tree. It won't try to delete any children.
	void remove(scxcfg_node *);
	bool is_leaf();
	// Set the children iterator to the first.
	void atfirst();
	// Advance the children iterator.
	void advance();
	// returns the current child of this node.
	scxcfg_node *get_current_child();
	void delete_tree();
	// Checks whether the name contains illegal char. The first
	// parameter is the name, the second is the illegal char to
	// check for.
	static bool name_ok(const char *, char ch = scxcfg_node::separator);
	// helper function. Copy the next field (between separators) from ptr
	// to buf. Returns false when ptr reaches the end.
	static bool get_next_str(char *&ptr, char *buf,
	    char sep = scxcfg_node::separator);
	// Destructor. It will free name and value, and assert that
	// there is no children around.
	virtual scxcfg_node *create_node(const char *);
	// Create a new node the specified name.
	scxcfg_node(const char *);
	// Destructor.
	virtual ~scxcfg_node();
	// DEBUG
	static int print(scxcfg_node *, void *arg);
protected:
	std::list<scxcfg_node *> children;
	std::list<scxcfg_node *>::const_iterator child_iter;
	scxcfg_node *parent;
	// We copy the name and free it when the node is deleted.
	char *name;
	// The value of this node. It's NULL for non-leaf node.
	char *value;
	// Disable these constructors.
	scxcfg_node(const scxcfg_node &);
	scxcfg_node & operator = (const scxcfg_node &);
};


class scxcfg_cached_data : public scxcfg_data
{
public:
	scxcfg_cached_data();
	virtual ~scxcfg_cached_data();
	int init(const char *);
	int close();
	virtual int fillCache(const char *context) = 0;
	int getCachedProperty(const char *prop, char *val);
	int getCachedListProperty(const char *prop, f_property_t **listvalue,
	    int *cnt);
	int getSubtreeList(const char *prop, std::list<Prop*> *listv);
	std::stringstream *getStringCache() { return (stringCache); };
	string *getCachedContext() { return (cachedContext); };
protected:
	int buildList(std::stringstream *ss);
	int buildFarmTree();
	// checkForUpdate() is called on when getting proerties to
	// verify if the underlying data store has changed.
	// If data has changed, the cache is reloaded.
	// it must be implemented by the class managing the data.
	virtual int checkForUpdate() = 0;	// pure virtual
	// Cached raw data.
	std::stringstream *stringCache;
	// Cached list of property/value pairs.
	std::set<Prop> propertiesList;
	// Cache tree of properties.
	scxcfg_node  *farmRootNode;
	// The context used when initializing the cache.
	string *cachedContext;
};

#endif /* _SCXCFG_CACHED_DATA_H */
