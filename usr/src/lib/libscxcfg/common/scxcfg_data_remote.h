/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_SCXCFG_DATA_REMOTE_H
#define	_SCXCFG_DATA_REMOTE_H

#pragma ident	"@(#)scxcfg_data_remote.h	1.3	08/05/20 SMI"

#include <string>
#include <list>
#ifndef linux
#include <netdir.h>
#endif
#include <scxcfg/rpc/scxcfg_rpc.h>

#include "scxcfg_cached_data.h"

//
// scxcfg_data_remote
//
// This class can be used to access the farm configuration on the
// server nodes through the network, using the RPC server scxcfgd
// as the data store.
//
// It inherits the scxcfg_cached_data but little of the interface is
// actually used. Because, on a farm node, the normal access (API access)
// are mapped on a file access (scxcfg_data_file). The contents of this file
// is retrieved from the server nodes by the administrative command (scxcfg)
// using the additionnal services provided by this scxcfg_data_remote class.
//
// These services includes:
// - server discovery on the network  used
//   - for config retrieval
//   - installation of a farm node
//

class scxcfg_data_remote : public scxcfg_cached_data
{
public:
	scxcfg_data_remote();
	virtual ~scxcfg_data_remote();
	int init(const char *);
	int close();
	int getProperty(const char *context, const char *prop,
	    char *val) { return (SCXCFG_ENOTIMPLEMENTED); };
	int setProperty(const char *, const char *, const char *);
	int setListProperty(const char *context, std::list<Prop*> *list);
	int delProperty(const char *, const char *);
	int delListProperty(const char *context, std::list<Prop*> *list);
	int getConfig(const char *, char **, const scxcfg_nodeid_t)
	    { return (SCXCFG_ENOTIMPLEMENTED); };
	int deleteConfig(const char *)  { return (SCXCFG_ENOTIMPLEMENTED); };
	int installFarmNode(scxcfg_install_input_t *input,
	    scxcfg_install_output_t *output);
	bool addServer(char *);
private:
	int checkForUpdate()  { return (SCXCFG_OK); };
	int fillCache(const char *context);
	int find_server();
	const char *getServer();
	bool nextServer();
	string currentServer;
	std::set<string> serverS;
	std::set<string>::const_iterator curS;
	std::set<string>::const_iterator nextS;
	int sockfd;
	CLIENT *clntHandle;
	string *localName;
	scxcfg_nodeid_t nodeId;
	char clusterId[FCFG_MAX_LEN];
};

#endif /* _SCXCFG_DATA_REMOTE_H */
