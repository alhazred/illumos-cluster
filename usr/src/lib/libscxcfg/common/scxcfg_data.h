/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_SCXCFG_DATA_H
#define	_SCXCFG_DATA_H

#pragma ident	"@(#)scxcfg_data.h	1.3	08/05/20 SMI"

#include <list>
#include <string>
#include <pthread.h>
#include <scxcfg/scxcfg.h>

using std::string;

//
// Utility class used to store properties and values.
//
struct Prop
{
	Prop(const char *k, const char *v) :
		name(k), value(v) {};
	Prop(string p, string v) {
		name = p;
		value = v;
	};
	friend int operator > (const Prop& left, const Prop& right)
	{
		return (left.name > right.name);
	}
	friend int operator < (const Prop& left, const Prop& right)
	{
		return (left.name < right.name);
	}
	string name;
	string value;
};


//
// scxcfg_data
//
// Virtual class, almost just an interface. Any implementation
// should derive from it.
//
class scxcfg_data
{
public:
	scxcfg_data();
	virtual ~scxcfg_data();
	virtual int init(const char *) = 0;
	virtual int close() = 0;
	virtual int getProperty(const char *context, const char *prop,
	    char *val) = 0;
	virtual int setProperty(const char *context, const char *prop,
	    const char *val) = 0;
	virtual int setListProperty(const char *context,
	    std::list<Prop*> *list) = 0;
	virtual int delProperty(const char *context, const char *prop) = 0;
	virtual int delListProperty(const char *context,
	    std::list<Prop*> *list) = 0;
	virtual int getConfig(const char *context, char **buf,
	    const scxcfg_nodeid_t n) = 0;
	virtual int deleteConfig(const char *context) = 0;
protected:
	void lock();
	void unlock();
	pthread_mutex_t mutex;
};

#endif /* _SCXCFG_DATA_H */
