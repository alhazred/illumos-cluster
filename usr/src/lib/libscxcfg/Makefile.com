#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# ident	"@(#)Makefile.com	1.4	08/05/20 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# lib/libscxcfg/Makefile.com
#

LIBRARYCCC =	libscxcfg.a

VERS= .1

include		$(SRC)/Makefile.master

RPC_OBJECTS =\
	scxcfg_rpc_clnt.o \
	scxcfg_rpc_xdr.o

COM_OBJECTS =\
	scxcfg_api.o \
	data_access.o \
	scxcfg_data.o \
	scxcfg_cached_data.o \
	scxcfg_data_file.o \
	scxcfg_data_remote.o

OBJECTS = $(COM_OBJECTS) $(RPC_OBJECTS)

# include library definitions, do not change order of include and DYNLIB
include ../../Makefile.lib

LINTFILES += $(OBJECTS:%.o=%.ln)

RPCFILE =		$(ROOTCLUSTHDRDIR)/scxcfg/rpc/scxcfg_rpc.x
RPCFILE_CLNT =		../common/scxcfg_rpc_clnt.c 
RPCFILE_XDR =		../common/scxcfg_rpc_xdr.c
RPCFILES =	$(RPCFILE_CLNT) $(RPCFILE_XDR)

CLOBBERFILES +=	$(RPCFILES)
CHECKHDRS = data_access.h scxcfg_data.h scxcfg_cached_data.h scxcfg_data_file.h \
	  scxcfg_data_ccr.h scxcfg_data_remote.h

CHECK_FILES = $(COM_OBJECTS:%.o=%.c_check) $(CHECKHDRS:%.h=%.h_check)

MAPFILE=	../common/mapfile-vers
SRCS =		$(OBJECTS:%.o=../common/%.c)

LIBS = $(DYNLIBCCC)

CLEANFILES =	$(SPROLINTOUT)

CPPFLAGS += -D_REENTRANT -I../../../head/scxcfg


ADDLIBS += -L/usr/ucblib -R/usr/ucblib -lrpcsoc -lsocket

LDLIBS		+=  -lnsl -ldl -lc

PMAP =	-M $(MAPFILE)

RPCGENFLAGS	= $(RPCGENMT) -C -K -1
$(EUROPA)RPCGENFLAGS	+= -DEUROPA

.KEEP_STATE:


all: $(RPCFILES) $(LIBS)

$(DYNLIBCCC): 	$(MAPFILE)

# include library targets
include ../../Makefile.targ

objs/%.o pics/%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<

objs/%.o pics/%.o: ../common/%.cc
	$(COMPILE.cc) -o $@ $<

