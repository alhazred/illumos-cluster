#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)Makefile.com	1.3	08/05/20 SMI"
#
LIBRARYCCC =	libfence.a

VERS= .1

include		$(SRC)/Makefile.master

COM_OBJECTS =\
	fence_api.o \
	fencer.o \
	device_netapp.o \
	device_hanfs.o \
	hanfs_dfstab.o


OBJECTS = $(COM_OBJECTS)

# include library definitions, do not change order of include and DYNLIB
include ../../Makefile.lib

LINTFILES += $(OBJECTS:%.o=%.ln)

CHECKHDRS =
CHECK_FILES = $(COM_OBJECTS:%.o=%.c_check) $(CHECKHDRS:%.h=%.h_check)

MAPFILE=	../common/mapfile-vers
SRCS =		$(OBJECTS:%.o=../common/%.c)

LIBS = $(DYNLIBCCC)

CLEANFILES =	$(SPROLINTOUT)

CPPFLAGS += -D_REENTRANT

LDLIBS		+=  -lscxcfg -lscha -lc

PMAP =	-M $(MAPFILE)

RPCGENFLAGS	= $(RPCGENMT) -C -K -1

.KEEP_STATE:

all: $(RPCFILES) $(LIBS)

$(DYNLIBCCC): 	$(MAPFILE)

# include library targets
include ../../Makefile.targ

objs/%.o pics/%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<

objs/%.o pics/%.o: ../common/%.cc
	$(COMPILE.cc) -o $@ $<

