/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fencer.cc	1.3	08/05/20 SMI"

#include <fstream>
#include <iostream>
#include <string>
#include <list>

#include <syslog.h>
#include <scxcfg/scxcfg.h>

#include "fencer.h"
#include "device_netapp.h"
#include "device_hanfs.h"


//
// Fencer contructor.
//
Fencer::Fencer()
{
	if (getenv("FENCE_DEBUG")) {
		printf("setting fence debug\n");
		fence_log_set_level(LOG_DEBUG);
	}
	fence_log(LOG_DEBUG, "Fencer CTOR");
	Cfg = NULL;
}

//
// Fencer destuctor.
//
Fencer::~Fencer()
{
	Cfg = NULL;
}

//
// Fencer::init()
//
// Read the config to create the fencing devices.
//
int
Fencer::init(scxcfg_t cfg)
{
	int rc;
	void *dataAccess;

	dataAccess = cfg->handle;
	if (!dataAccess)
		return (FENCE_EINVAL);

	Cfg = cfg;
	rc = readConfig(Cfg);
	if (rc != FENCE_OK) {
		return (rc);
	}
	return (FENCE_OK);
}

//
// Fencer::close()
//
int
Fencer::close()
{
	return (FENCE_OK);
}

//
// typeStr2Int()
//
// Utility routine.
//
static devType_t
typeStr2Int(const char *type)
{
	if (strcmp(type, STR_NAS_NETAPP_TYPE) == 0)
		return (TYPE_NETAPP_NAS);
	else if (strcmp(type, STR_HANFS_TYPE) == 0)
		return (TYPE_HANFS);
	return (TYPE_INVALID);
}

//
// allocateDevice()
//
// Allocate and init a fencing device acoording to its type.
//
static FenceDevice *
allocateDevice(const char *type, const char *server, const char *path,
    const char *rg)
{
	devType_t t;
	FenceDevice *dev;
	int rc;
	const char *arg;

	t = typeStr2Int(type);
	if (t == TYPE_INVALID) {
		fence_log(LOG_ERR, "allocateDevice: unknown type: %s type");
		return (NULL);
	}

	if (t == TYPE_NETAPP_NAS) {
		arg = path;
		dev = new DeviceNetapp(server, path);
	} else if (t == TYPE_HANFS) {
		if (!rg) {
			fence_log(LOG_ERR, "Cannot init HANFS fence dev: %s:%s "
			    "missing required argument: rg", server, path);
			return (NULL);
		}
		arg = rg;
		dev = new DeviceHANFS(server, rg);
	}
	rc = dev->init(server, arg);
	if (rc != FENCE_OK) {
		fence_log(LOG_ERR, "allocateDevice: error init %s:%s:"
		    "returns: %d", server, path, rc);
		delete dev;
		return (NULL);
	}
	return (dev);
}

//
// Fencer::readConfig()
//
// Read the famr config to retrive all fencing file systems.
// If the correspoinding device is allocated and inited,
// it is added to deviceList.
//
int
Fencer::readConfig(scxcfg_t cfg)
{
	int rc;
	char prop[FCFG_MAX_LEN];
	char type[FCFG_MAX_LEN];
	char server[FCFG_MAX_LEN];
	char path[FCFG_MAX_LEN];
	char rgbuf[FCFG_MAX_LEN];
	char *rg = NULL;
	f_property_t *list = NULL, *v;
	int num = 0;
	scxcfg_error err;
	int rstatus = FENCE_OK;

	fence_log(LOG_DEBUG, "Fencer::readConfig");
	// read the fencing devices in scxcfg config
	// verify the NAS devices
	// farm.properties.fencing_fs.1.type
	// farm.properties.fencing_fs.1.server
	// farm.properties.fencing_fs.1.path

	sprintf(prop, "farm.properties.fencing_fs");
	rc = scxcfg_getlistproperty_value(cfg, NULL, prop, &list, &num, &err);

	if (rc != FCFG_OK) {
		fence_log(LOG_ERR, "Fencer::readConfig: err reading cfg: %d",
		    rc);
		return (FENCE_EINVAL);
	}
	if (num == 0) {
		//
		// No fencing FS in config.
		//
		return (FENCE_OK);
	}
	for (v = list; v != NULL; v = v->next) {
		sprintf(prop, "farm.properties.fencing_fs.%s.type", v->value);
		rc =  scxcfg_getproperty_value(cfg, NULL, prop, type, &err);
		if (rc != FCFG_OK) {
			fence_log(LOG_ERR, "Fencer::readConfig: err read: %s",
			    prop);
			rstatus  = FENCE_EINVAL;
			goto cleanup;
		}
		sprintf(prop, "farm.properties.fencing_fs.%s.server", v->value);
		rc =  scxcfg_getproperty_value(cfg, NULL, prop, server, &err);
		if (rc != FCFG_OK) {
			fence_log(LOG_ERR, "Fencer::readConfig: err read: %s",
			    prop);
			rstatus  = FENCE_EINVAL;
			goto cleanup;
		}
		sprintf(prop, "farm.properties.fencing_fs.%s.path", v->value);
		rc =  scxcfg_getproperty_value(Cfg, NULL, prop, path, &err);
		if (rc != FCFG_OK) {
			fence_log(LOG_ERR, "Fencer::readConfig: err read: %s",
			    prop);
			rstatus  = FENCE_EINVAL;
			goto cleanup;
		}
		sprintf(prop, "farm.properties.fencing_fs.%s.rg", v->value);
		rc =  scxcfg_getproperty_value(Cfg, NULL, prop, rgbuf, &err);
		if (rc == FCFG_ENOTFOUND) {
			rg = NULL;
		} else if (rc != FCFG_OK) {
			fence_log(LOG_ERR, "Fencer::readConfig: err read: %s",
			    prop);
			rstatus  = FENCE_EINVAL;
			goto cleanup;
		} else {
			rg = rgbuf;
		}
		FenceDevice *dev = allocateDevice(type, server, path, rg);
		if (dev == NULL) {
			//
			// This one failed, continue in case another
			// could be configured.
			//
			continue;
		}
		deviceList.push_back(dev);
	}
cleanup:
	if (list)
		scxcfg_freelist(list);
	return (rstatus);
}

//
// Fencer::fenceNodes()
//
// Fence the nodes for all known devices in deviceList.
//
int
Fencer::fenceNodes(nodeList_t &nodes)
{
	std::list<FenceDevice*>::const_iterator it;
	int rc;
	int rstatus = FENCE_OK;

	for (it = deviceList.begin(); it != deviceList.end(); it++) {
		rc = (*it)->doFence(nodes);
		if (rc != FENCE_OK) {
			fence_log(LOG_ERR, "doFence failed for %s",
			    (*it)->getName().c_str());
		}
	}
	return (rstatus);
}

//
// Fencer::unFenceNodes()
//
// Unfence the nodes for all known devices in deviceList.
//
int
Fencer::unFenceNodes(nodeList_t &nodes)
{
	std::list<FenceDevice*>::const_iterator it;
	int rc;
	int rstatus = FENCE_OK;

	for (it = deviceList.begin(); it != deviceList.end(); it++) {
		rc = (*it)->doUnFence(nodes);
		if (rc != FENCE_OK) {
			fence_log(LOG_ERR, "doFence failed for %s",
			    (*it)->getName().c_str());
		}
	}
	return (rstatus);
}


//
// Called when config  is changed.
// Verify if new NAS shared device are havailable
//
int
Fencer::configChangeCallBack()
{
	return (FENCE_OK);
}
