/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)device_hanfs.cc	1.3	08/05/20 SMI"

#include <fstream>
#include <iostream>
#include <string>
#include <list>
#include <cerrno>

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <locale.h>
#include <scha.h>

#include "hanfs_dfstab.h"
#include "device_hanfs.h"

static const char *HANFS_TYPE = "SUNW.nfs";
static int shareable(const char *path, const char *dfstab, struct share **sh);

//
// DeviceHANFS Constructor
//
DeviceHANFS::DeviceHANFS(const char *serv, const char *rg_info)
{
	fence_log(LOG_DEBUG, "DeviceHANFS::CTOR,  %s %s", serv, rg_info);
	RG = string("");
	Resource = string("");
	Path = string("");
	if (serv)
		logicalHostName = string(serv);
	if (rg_info) {
		string info = string(rg_info);
		std::size_t mark1;
		std::size_t mark2;
		mark1 = info.find_first_of(":");
		mark2 = info.find_last_of(":");
		if (mark1 == std::string::npos ||
		    mark2 == std::string::npos) {
			fence_log(LOG_DEBUG, "DeviceHANFS:invalid RG info %s",
			    rg_info);
			return;
		}
		RG = info.substr(0, mark1);
		Resource = info.substr(mark1 + 1, mark2 - mark1 -1);
		Path = info.substr(mark2 + 1, std::string::npos);
	}
}

//
// DeviceHANFS destructor.
//
DeviceHANFS::~DeviceHANFS()
{
}

//
// is_hanfs_resource()
//
// Utility function, given a rgm resource, returns if it is
// of type SUNW.nfs (HA-NFS) type.
//
static bool
is_hanfs_resource(const char *res, const char *rg)
{
	scha_resource_t handle;
	scha_err_t err;
	char *type;
	bool rstatus = false;

	err = scha_resource_open(res, rg, &handle);
	if (err != SCHA_ERR_NOERR) {
		fence_log(LOG_ERR, "is_hanfs_resource: err scha_resource_open"
		    " %s: %s", rg, scha_strerror(err));
		return (rstatus);
	}
	err = scha_resource_get(handle, SCHA_TYPE, &type);
	if (err != SCHA_ERR_NOERR) {
		fence_log(LOG_ERR, "is_hanfs_resource: err scha_resource_get"
		    " %s: %s", rg, scha_strerror(err));
		goto cleanup;
	}
	fence_log(LOG_DEBUG, "is_hanfs_resource: type %s", type);

	if (strncmp(type, HANFS_TYPE, strlen(HANFS_TYPE)) == 0) {
		rstatus = true;
	}
cleanup:
	scha_resource_close(handle);
	return (rstatus);
}


//
// DeviceHANFS::init()
//
// Initalize this DeviceHANFS for fencing.
//  Find information about the HA-NFS RGM resource to fill out
//  needed information.
//
int
DeviceHANFS::init(const char *serv, const char *rg_info)
{
	int rstatus = FENCE_OK;
	scha_resourcegroup_t handle;
	scha_err_t err;
	scha_str_array_t *resources = NULL;

	fence_log(LOG_DEBUG, "DeviceHANFS::init, %s:%s", serv, rg_info);

	//
	// The constructor should have initialized
	// local members.
	//
	if (RG.length() == 0 || Resource.length() == 0 || Path.length() == 0) {
		fence_log(LOG_ERR, "DeviceHANFS invalid RG info:"
		    " %s", rg_info);
		rstatus = FENCE_EINVAL;
		goto cleanup;
	}
	fence_log(LOG_DEBUG, "DeviceHANFS::init, Path  %s", Path.c_str());
	//
	// Intitialize the dfstab file name.
	//
	dfstabFileName = Path + "/";
	dfstabFileName += HANFS_TYPE;
	dfstabFileName += "/dfstab.";
	dfstabFileName += Resource;
	fence_log(LOG_DEBUG, "DeviceHANFS::init, dfstabFileName %s",
	    dfstabFileName.c_str());
cleanup:
	return (rstatus);
}

int
DeviceHANFS::fini()
{
	return (FENCE_OK);
}

//
// remNodesFromOpt()
//
// In the dfstab-like option string contained in sh->sh_opts,
// Update the sh_opts to remove the nodes contained in the node list
// for option "req_opt".
//
static void
remNodesFromOpt(std::list<Node*> nlist, struct share *sh, const char *req_opt)
{
	std::list<Node*>::const_iterator it;
	std::size_t beg;
	std::size_t end;
	std::size_t n;
	std::size_t initialLength;
	string op;
	string optlist = string(sh->sh_opts);

	op = string(req_opt) + "=";
	//
	//  Find option in option list.
	//
	beg =  optlist.find(op);
	if (beg == string::npos) {
		//
		// No option, nothing to do.
		//
		return;
	}
	//
	// The option exist
	//
	end = (optlist.substr(beg, string::npos)).find_first_of(",");

	string nodeList = optlist.substr(beg, end);
	initialLength = nodeList.length();

	bool modified = false;
	for (it = nlist.begin(); it != nlist.end(); it++) {
		//
		// Is this node in the node list ?
		//
		if ((n = nodeList.find((*it)->name)) != string::npos) {
			int len = (*it)->name.length();
			if ((n + len) != initialLength)
				len += 1;
			nodeList.erase(n, len);
			if (nodeList[nodeList.length() -1] == ':')
				nodeList.erase(nodeList.length() - 1, 1);
			modified = true;
		}
	}
	if (modified) {
		// erase previous node list.
		optlist.erase(beg, initialLength);
		// insert new node list if it is not empty
		if (nodeList != op) {
			optlist.insert(beg, nodeList);
		} else {
			// if empty, remove the option, remove the ","
			// if any.
			if (beg > 1)
				optlist.erase(beg - 1, 1);
		}
	}
	//
	// Replace the old options list with the new one.
	//
	if (sh->sh_opts)
		free((void *)sh->sh_opts);
	sh->sh_opts = strdup(optlist.c_str());
}


//
// addNodesFromOpt()
//
// In the  dfstab-like option string contained in sh->sh_opts,
// Update the sh_opts to add the nodes contained in the node list
// for the option "req_opt"
//
static void
addNodesToOpt(std::list<Node*> nlist, struct share *sh, const char *req_opt)
{
	std::list<Node*>::const_iterator it;
	std::size_t beg;
	std::size_t end;
	string op;
	string optlist = string(sh->sh_opts);

	op = string(req_opt) + "=";
	//
	// Find option in option list.
	//
	beg = optlist.find(op);
	if (beg != string::npos) {
		//
		// The option exist, nodify it,
		//
		end = (optlist.substr(beg, string::npos)).find_first_of(",");
		string nodeList = optlist.substr(beg, end);
		for (it = nlist.begin(); it != nlist.end(); it++) {
			if (nodeList.find((*it)->name) == string::npos) {
				// add a ':' only if not the only node.
				if (nodeList != op)
				    nodeList += ":";
				nodeList += (*it)->name;
			}
		}
		// erase old node list
		optlist.erase(beg, end);
		// insert new node list.
		optlist.insert(beg, nodeList);
	} else {
		//
		// Add the option.
		//
		optlist += ",";
		optlist += req_opt;
		optlist += "=";
		int i;
		for (i = 1, it = nlist.begin(); it != nlist.end(); i++, it++) {
			optlist += (*it)->name;
			// add a ":' only is not the last to add
			if (i != nlist.size())
				optlist += ":";
		}
	}
	//
	// Replace the old options list be the new one.
	//
	if (sh->sh_opts)
		free((void *)sh->sh_opts);
	sh->sh_opts = strdup(optlist.c_str());
}


//
// DeviceHANFS::doFence()
//
// Do fencing (farm nodes leaving cluster).  remove nodes from option "rw",
// add them in in option "ro", assure they are in option "root=".
//
DeviceHANFS::doFence(std::list<Node*> list)
{
	int rstatus = FENCE_OK;
	std::list<Node*>::const_iterator it;
	struct share *sh = NULL;
	int rc;

	fence_log(LOG_DEBUG, "DeviceHANFS::doFence");

	int create = 0;
	rc = shareable(Path.c_str(), dfstabFileName.c_str(), &sh);
	fence_log(LOG_DEBUG, "doFence, shareable returns %d", rc);

	switch (rc) {
	case 0:// The path is invalid.
		fence_log(LOG_ERR, "DeviceHANFS::doFence: Path invalid %s",
		    Path.c_str());
		return (FENCE_EINVAL);
		break;
	case 1:// The export entry does not exist yet.
		create = 1;
		break;
	case 2: // export entry aleady exist.
		break;
	}

	if (create) {
		sh = (struct share *)malloc(sizeof (struct share));
		sh->sh_path = strdup(Path.c_str());
		sh->sh_opts = "";
	}
	//
	// Add the nodes to the Read Only node list.
	//
	addNodesToOpt(list, sh, SHOPT_RO);
	//
	// Remove the nodes to the Read Write node list.
	//
	remNodesFromOpt(list, sh, SHOPT_RW);
	//
	// Always make sure they are in the root access list.
	//
	addNodesToOpt(list, sh, SHOPT_ROOT);

	fence_log(LOG_DEBUG, "DeviceHANFS::doFence: NEW opts = <%s>",
	    sh->sh_opts);

	rc = dfstabSet(sh, create);
	if (rc != FENCE_OK) {
		fence_log(LOG_ERR, "DeviceHANFS::doFence, ERROR dfstabSet %d",
		    rc);
	}
	//
	// Execute the share cmd.
	//
	string cmd = "sh -c 'share -F nfs -o ";
	cmd += sh->sh_opts;
	cmd += " ";
	cmd += sh->sh_path;
	cmd += "'";

	rc = system(cmd.c_str());
	if (rc != 0) {
		fence_log(LOG_ERR, "DeviceHANFS::doFence, ERROR cmd share\n%s",
		    cmd.c_str());
	}
	sharefree(sh);
	return (rstatus);
}

//
// DeviceHANFS::doUnFence()
//
// Do unfencing (joining farm nodes), remove nodes from option "ro", add
// them in in option "rw", assure they are in option "root=".
//
DeviceHANFS::doUnFence(std::list<Node*> list)
{
	int rstatus = FENCE_OK;
	std::list<Node*>::const_iterator it;
	struct share *sh = NULL;
	int rc;
	int create = 0;

	fence_log(LOG_DEBUG, "DeviceHANFS::doUnFence");

	rc = shareable(Path.c_str(), dfstabFileName.c_str(), &sh);
	fence_log(LOG_DEBUG, "doUnFence, shareable returns %d", rc);

	switch (rc) {
	case 0:// The path is invalid.
		fence_log(LOG_ERR, "DeviceHANFS::doUnFence: Path invalid %s",
		    Path.c_str());
		return (FENCE_EINVAL);
		break;
	case 1:// The export entry does not exist yet.
		create = 1;
		break;
	case 2: // export entry aleady exist.
		break;
	}

	if (create) {
		sh = (struct share *)malloc(sizeof (struct share));
		sh->sh_path = strdup(Path.c_str());
		sh->sh_opts = "";
	}
	//
	// Add the nodes to the Read Write node list.
	//
	addNodesToOpt(list, sh, SHOPT_RW);
	//
	// Remove the nodes to the Read Only node list.
	//
	remNodesFromOpt(list, sh, SHOPT_RO);
	//
	// Always make sure they are in the root access list.
	//
	addNodesToOpt(list, sh, SHOPT_ROOT);

	fence_log(LOG_DEBUG, "DeviceHANFS::doUnFence: NEW opts = <%s>",
	    sh->sh_opts);

	//
	// Update the dfstab file.
	//
	rc = dfstabSet(sh, create);
	if (rc != FENCE_OK) {
		fence_log(LOG_ERR, "DeviceHANFS::doUnFence, ERROR dfstabSet %d",
		    rc);
	}
	//
	// Execute the share cmd.
	//
	string cmd = "sh -c 'share -F nfs -o ";
	cmd += sh->sh_opts;
	cmd += " ";
	cmd += sh->sh_path;
	cmd += "'";
	rc = system(cmd.c_str());
	if (rc != 0) {
		fence_log(LOG_ERR, "DeviceHANFS::doUnFence, ERROR share\n%s",
		    cmd.c_str());
	}
	sharefree(sh);
	return (rstatus);
}

//
// direq()
//
// utility to determine if two  directories are equal or equivalent.
//
static int
direq(const char *dir1, const char *dir2)
{
	struct stat st1, st2;

	if (strcmp(dir1, dir2) == 0)
		return (1);
	if (stat(dir1, &st1) < 0 || stat(dir2, &st2) < 0)
		return (0);
	return (st1.st_ino == st2.st_ino && st1.st_dev == st2.st_dev);
}


//
// shareable()
//
// Check the nfs share cmds in dfstab file.
// Returns:
//	0  dir not shareable
//	1  dir is shareable
//	2  dir is already shared (can modify options)
//
// If the share command for the path is found, sh is allocated
// and returned. *sh must be freed by the caller.
//
static int
shareable(const char *path, const char *dfstab, struct share **sh)
{
	struct share *tmp_sh;
	FILE *f;
	struct stat st;
	int res;

	fence_log(LOG_DEBUG, "shareable: %s:  %s", path, dfstab);
	if (*path != '/')
		fence_log(LOG_ERR, gettext("%s: not a full pathname\n"), path);

	if (stat(path, &st) < 0)
		fence_log(LOG_ERR, gettext("%s: %s\n"), path, strerror(errno));

	f = fopen(dfstab, "r");
	if (f == NULL) {
		if (errno == ENOENT)
			return (1);
		fence_log(LOG_ERR, "%s: %s\n", dfstab, strerror(errno));
		return (0);
	}

	while ((res = getShareCmd(f, &tmp_sh)) > 0) {
		if (direq(path, tmp_sh->sh_path)) {
			//
			// We found the entry, copy it for
			// return.
			//
			*sh =  sharedup(tmp_sh);
			(void) fclose(f);
			return (2);
		}
	}
	if (res < 0) {
		fence_log(LOG_ERR, gettext("error reading %s\n"), dfstab);
	}
	(void) fclose(f);
	return (1);
}


//
// DeviceHANFS::dfstabSet()
//
// Append/modify an entry to the export (dfstab) file associated with
// this DeviceHANFS.
//
// Returns:
//	0  ok
//	-1  error
//
int
DeviceHANFS::dfstabSet(struct share *sh, int create)
{
	int ret;
	FILE *f;

	fence_log(LOG_DEBUG, "DeviceHANFS::dfstabSet, create = %d", create);
	//
	// Open the file for update and create it if necessary.
	// This may leave the I/O offset at the end of the file,
	// so rewind back to the beginning of the file.
	//
	f = fopen(dfstabFileName.c_str(), "a+");
	if (f == NULL) {
		fence_log(LOG_ERR, "%s: %s\n",  dfstabFileName.c_str(),
		    strerror(errno));
		return (-1);
	}
	rewind(f);

	if (lockf(fileno(f), F_LOCK, 0L) < 0) {
		fence_log(LOG_ERR, gettext("cannot lock %s: %s\n"),
		    dfstabFileName.c_str(), strerror(errno));
		(void) fclose(f);
		return (-1);
	}

	//
	// If re-sharing an old share with new options
	// then first remove the old share entry.
	//
	if (!create) {
		if ((ret = remShareCmd(f, Path.c_str())) < 0) {
			switch (ret) {
			case -1:
				fence_log(LOG_ERR, gettext(
					"failed to remove old dfstab"
					" entry, no memory\n"));
				break;
			case -2:
				fence_log(LOG_ERR, gettext(
					"dfstab may be corrupt, ftrucate call"
					" failure during dfstab update\n"));
				break;
			case -3:
				fence_log(LOG_ERR, gettext(
					"failed to remove old dfstab entry,"
					" corrupt dfstab file\n"));
				break;
			}
		}
	}
	if (putShareCmd(f, sh) < 0)
		fence_log(LOG_ERR, gettext("dfstabSet: err changing dfstab"
		    " for %s\n"),  Path.c_str());
	(void) fclose(f);
	return (0);
}
