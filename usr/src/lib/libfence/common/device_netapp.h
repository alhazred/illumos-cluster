/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _DEVICE_NETAPP_H
#define	_DEVICE_NETAPP_H

#pragma ident	"@(#)device_netapp.h	1.3	08/05/20 SMI"

#include <string>

#include "fence_device.h"

using std::string;

class DeviceNetapp : public FenceDevice
{
public:
	DeviceNetapp(const char *serv, const char *path);
	~DeviceNetapp();
	int init(const char *, const char *);
	int fini();
	int doFence(std::list<Node*> nodes);
	int doUnFence(std::list<Node*> nodes);
private:
	string hostName;
	string Path;
	int state;
};

#endif /* _DEVICE_NETAPP_H */
