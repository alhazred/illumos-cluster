/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_FENCE_H
#define	_FENCE_H

#pragma ident	"@(#)fence.h	1.3	08/05/20 SMI"

#include <list>
#include <string>
#include <scxcfg/scxcfg.h>

using std::string;

struct Node
{
	Node(const char *n) :
		name(n) {};
	Node(string n) {
		name = n;
	};
	string name;
};

typedef enum fenceError {
	FENCE_OK = 0,
	FENCE_EINVAL = 1,
	FENCE_ENOMEM = 2
} fenceError_t;

typedef std::list<Node*> nodeList_t;

extern "C" {
int fenceInitialize(scxcfg_t cfg);
int fenceNodes(nodeList_t &nl);
int unFenceNodes(nodeList_t &nl);
int fenceFinalize();
}

#endif /* _FENCE_H */
