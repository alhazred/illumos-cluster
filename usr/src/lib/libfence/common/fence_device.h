/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FENCE_DEVICE_H
#define	_FENCE_DEVICE_H

#pragma ident	"@(#)fence_device.h	1.3	08/05/20 SMI"

#include <list>

#include "fence.h"
#include "fence_int.h"

#define	STR_NAS_NETAPP_TYPE "netapp"
#define	STR_HANFS_TYPE "hanfs"

typedef enum devType {
	TYPE_INVALID = 0,
	TYPE_NETAPP_NAS = 1,
	TYPE_HANFS = 2
} devType_t;

typedef enum devState {
	STATE_UNKNOWN = 0,
	STATE_REACHABLE = 1,
	STATE_UNREACHABLE = 2,
	STATE_ERROR = 3
} devState_t;


class FenceDevice
{
public:
	FenceDevice() { pthread_mutex_init(&mutex, NULL); };
	virtual ~FenceDevice() {};
	virtual int init(const char *, const char*) = 0;
	virtual int fini() = 0;
	devType_t getType() { return (Type); };
	string &getName() { return (Name); };
	virtual int doFence(std::list<Node*> nodes) = 0;
	virtual int doUnFence(std::list<Node*> nodes) = 0;
	//	virtual int getNodeState(std::list<Node*> nodes) = 0;
protected:
	devType_t Type;
	devState_t deviceState;
	string Name;
private:
	void lock();
	void unlock();
	pthread_mutex_t mutex;
};

#endif /* _FENCE_DEVICE_H */
