/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_DEVICE_HANFS_H
#define	_DEVICE_HANFS_H

#pragma ident	"@(#)device_hanfs.h	1.3	08/05/20 SMI"

#include <string>

#include "fence_device.h"
#include "hanfs_dfstab.h"

using std::string;

class DeviceHANFS : public FenceDevice
{
public:
	DeviceHANFS(const char *serv, const char *path);
	~DeviceHANFS();
	int doFence(std::list<Node*> nodes);
	int doUnFence(std::list<Node*> nodes);
	int init(const char *serv, const char *rg);
	int fini();
private:
	int changeExportFile();
	int dfstabSet(struct share *, int replace);
	int state;
	string logicalHostName;
	string Path;
	string RG;
	string Resource;
	string dfstabFileName;
	uint32_t currPrimary;
};

#endif /* _DEVICE_HANFS_H */
