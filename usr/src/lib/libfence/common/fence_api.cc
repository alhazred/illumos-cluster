/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fence_api.cc	1.3	08/05/20 SMI"

#include <fstream>
#include <iostream>
#include <string>
#include <list>

#include <syslog.h>

#include "fence.h"
#include "fencer.h"

//
// The Fencer singleton class, implmenting fencing services.
//
static Fencer theFencer;

//
// libfence logging support.
//
#define	DEFAULT_LOG_TAG "Fencer"

static int logLevel = LOG_NOTICE;
static char *logTag =  DEFAULT_LOG_TAG;

//
// simple API to access fencing functions.
//

int
fenceInitialize(scxcfg_t cfg)
{
	return (theFencer.init(cfg));
}

int
fenceNodes(nodeList_t &nl)
{
	return (theFencer.fenceNodes(nl));
}

int
unFenceNodes(nodeList_t &nl)
{
	return (theFencer.unFenceNodes(nl));
}

int
fenceFinalize()
{
	return (theFencer.close());
}

//
// Log utility function for libfence.
//
void
fence_log_set_tag(char *tag)
{
	if (!tag)
		return;
	/* only first call initialize tag */
	if (strcmp(logTag, DEFAULT_LOG_TAG) == 0)
		logTag = strdup(tag);
}

void
fence_log_set_level(int level)
{
	logLevel = level;
}

void
fence_log(int const pri, char const * const fmt, ...)
{
	va_list ap;
	if (pri > logLevel)
		return;
	va_start(ap, fmt);
	openlog(logTag, LOG_CONS | LOG_PID, LOG_DAEMON);
	vsyslog(pri, fmt, ap);
	closelog();
	va_end(ap);
}
