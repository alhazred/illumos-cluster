/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)hanfs_dfstab.c	1.3	08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "hanfs_dfstab.h"

/*
 * hanfs_dfstab.c
 *
 * Basic routines to access a dfstab-like file.
 *
 */

/*
 * Separator used to parse the file.
 */
static const char	sepstr[] = " \t\n";

/*
 * getline()
 *
 * Get valid lines from dfstab file.
 * Bypassing comment lines and empty lines.
 */
static int
getline(char *lp, FILE *fd)
{
	char	*cp;
	char *l;

again:
	l = fgets(lp, MAXBUFSIZE, fd);
	if (l != NULL) {
		if (strlen(lp) == MAXBUFSIZE-1 && lp[MAXBUFSIZE-2] != '\n')
			return (TOOLONG);
		for (cp = lp; *cp == ' ' || *cp == '\t'; cp++)
			;
		if (*cp != '#' && *cp != '\n') {
			/* valid line */
			return (1);
		} else {
			goto again;
		}
	}
	return (0);
}

/*
 * getShareCmd()
 *
 * Get an entry from the file fd.
 * This entry is  a share command whith parameters.
 * It should follow this format.
 *
 * share -F nfs -o <options> [ -d <desciption> ] <path>
 *
 * Returns:
 *	> 1  valid entry
 *	= 0  end of file
 *	< 0  error
 */
int
getShareCmd(FILE *fd, struct share **shp)
{
	static char *line = NULL;
	static struct share *sh = NULL;
	char *p, *tmp;
	char *lasts;

	if (line == NULL) {
		line = (char *)malloc(MAXBUFSIZE + 1);
		if (line == NULL)
			return (-1);
	}
	if (sh == NULL) {
		sh = (struct share *)malloc(sizeof (*sh));
		if (sh == NULL)
			return (-1);
	}
	(void) memset((char *)sh, 0, sizeof (*sh));

	if (getline(line, fd) == 0) {
		/* end of file */
		return (0);
	}
	p = line;
	/*
	 * Parse the line. We'll keep only the "opts" from -o,
	 * the descriptionn from -d and the path.
	 * The beginning of the line MUST be "share -F nfs "
	 */
	tmp = (char *)strtok_r(p, sepstr, &lasts);
	if (!tmp || strncmp(tmp, "share", strlen("share")) != 0)
		return (-1);
	tmp = (char *)strtok_r(NULL, sepstr, &lasts);
	if (!tmp || strncmp(tmp, "-F", strlen("-F")) != 0)
		return (-1);
	tmp = (char *)strtok_r(NULL, sepstr, &lasts);
	if (!tmp || strncmp(tmp, "nfs", strlen("nfs")) != 0)
		return (-1);
	tmp = (char *)strtok_r(NULL, sepstr, &lasts);
	if (!tmp || strncmp(tmp, "-o", strlen("-o")) != 0)
		return (-1);
	sh->sh_opts = (char *)strtok_r(NULL, sepstr, &lasts);
	if (sh->sh_opts == NULL)
		return (-2);
	tmp = (char *)strtok_r(NULL, sepstr, &lasts);

	if (strncmp(tmp, "-d", strlen("-d")) == 0) {
		sh->sh_descr = (char *)strtok_r(NULL, sepstr, &lasts);
		if (sh->sh_descr == NULL)
			return (-2);
		/*
		 * Remaining must be the path
		 */
		sh->sh_path = (char *)strtok_r(NULL, "", &lasts);

	} else {
		sh->sh_path = tmp;
	}
	if (sh->sh_path == NULL)
		return (-3);
	*shp = sh;
	return (1);
}


/*
 * Append an entry to the dfstab file fd.
 */
int
putShareCmd(FILE *fd, struct share *sh)
{
	int r;
	if (fseek(fd, 0L, 2) < 0)
		return (-1);

	if (sh->sh_descr != NULL) {
		r = fprintf(fd, "share -F nfs -o %s -d %s %s\n",
		    sh->sh_opts,
		    sh->sh_descr,
		    sh->sh_path);
	} else {
		r = fprintf(fd, "share -F nfs -o %s %s\n",
		    sh->sh_opts,
		    sh->sh_path);
	}
	return (r);
}


/*
 * The entry corresponding to path is removed from the
 * dsfstab file.  The file is assumed to be locked.
 * Read the entries into a linked list of share structures
 * minus the entry to be removed.  Then truncate the sharetab
 * file and write almost all of it back to the file from the
 * linked list.
 * Note: The file is assumed to be locked.
 *
 * Returns :
 *     1 : OK
 *   < 0 : error
 *
 */
int
remShareCmd(FILE *fd, const char *path)
{
	struct share *sh_tmp;
	struct shl {			/* the linked list */
		struct shl   *shl_next;
		struct share *shl_sh;
	};
	struct shl *shl_head = NULL;
	struct shl *shl, *prev, *next;
	int res, remcnt, rc;

	rewind(fd);
	remcnt = 0;
	shl = NULL;

	/*
	 * Read the file, keeping the lines that we want to keep
	 * and skipping the entry corresponding to path.
	 */
	while ((res = getShareCmd(fd, &sh_tmp)) > 0) {
		if (strcmp(path, sh_tmp->sh_path) == 0) {
			/*
			 * Found it.
			 */
			remcnt++;
		} else {
			prev = shl;
			shl = (struct shl *)malloc(sizeof (*shl));
			if (shl == NULL) {
				res = -1;
				goto dealloc;
			}
			if (shl_head == NULL)
				shl_head = shl;
			else
				prev->shl_next = shl;
			shl->shl_next = NULL;
			shl->shl_sh = sharedup(sh_tmp);
			if (shl->shl_sh == NULL) {
				res = -3;
				goto dealloc;
			}
		}
	}
	if (res < 0)
		goto dealloc;
	if (remcnt == 0) {
		res = 1;	/* nothing removed */
		goto dealloc;
	}

	if (ftruncate(fileno(fd), 0) < 0) {
		res = -2;
		goto dealloc;
	}
	/*
	 * Rewriting back the saved entries.
	 */
	for (shl = shl_head; shl; shl = shl->shl_next) {
		rc = putShareCmd(fd, shl->shl_sh);
		if (rc < 0) {
			res = -4;
			goto dealloc;
		}
	}
	res = 1;
dealloc:
	for (shl = shl_head; shl; shl = next) {
		/*
		 * make sure we don't reference sharefree with NULL shl->shl_sh
		 */
		if (shl->shl_sh != NULL)
			sharefree(shl->shl_sh);
		next = shl->shl_next;
		free(shl);
	}
	return (res);
}


struct share *
sharedup(struct share *sh)
{
	struct share *nsh;

	nsh = (struct share *)malloc(sizeof (*nsh));
	if (nsh == NULL)
		return (NULL);

	(void) memset((char *)nsh, 0, sizeof (*nsh));
	if (sh->sh_path) {
		nsh->sh_path = strdup(sh->sh_path);
		if (nsh->sh_path == NULL)
			goto alloc_failed;
	}
	if (sh->sh_opts) {
		nsh->sh_opts = strdup(sh->sh_opts);
		if (nsh->sh_opts == NULL)
			goto alloc_failed;
	}
	if (sh->sh_descr) {
		nsh->sh_descr = strdup(sh->sh_descr);
		if (nsh->sh_descr == NULL)
			goto alloc_failed;
	}
	return (nsh);

alloc_failed:
	sharefree(nsh);
	return (NULL);
}

void
sharefree(struct share *sh)
{
	if (sh->sh_path != NULL)
		free((void *)sh->sh_path);
	if (sh->sh_opts != NULL)
		free((void *)sh->sh_opts);
	if (sh->sh_descr != NULL)
		free((void *)sh->sh_descr);
	free((void *)sh);
}
