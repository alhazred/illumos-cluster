/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)device_netapp.cc	1.3	08/05/20 SMI"

#include <fstream>
#include <iostream>
#include <string>
#include <list>

#include <sys/stat.h>
#include <locale.h>

#include "device_netapp.h"

#define	NETAPP_CMD "/usr/sbin/NTAPfence"
#define	SUNCLUSTER_CMD "/usr/cluster/lib/sc/nas/sc-netapp-ctrl"
#define	JOIN  SUNCLUSTER_CMD " -c node_join -h '"
#define	FENCE SUNCLUSTER_CMD " -c fence_node -f '"

static int
execute(string cmd)
{
	int rc;
	fence_log(LOG_DEBUG, "DeviceNetapp execute cmd %s", cmd.c_str());
	rc = system(cmd.c_str());
	return (rc);
}

//
// DeviceNetapp constructor.
//
DeviceNetapp::DeviceNetapp(const char *serv, const char *path)
{
}

//
// DeviceNetapp destructor.
//
DeviceNetapp::~DeviceNetapp()
{
}

//
// DeviceNetapp::init()
//
int
DeviceNetapp::init(const char *serv, const char *path)
{
	struct stat st;
	int rstatus = FENCE_OK;
	if (stat(NETAPP_CMD, &st) < 0) {
		fence_log(LOG_ERR, gettext("DeviceNetapp::init %s:%s\n"
		    "NTAP support file not installed: %s: %s"),
		    serv, path, NETAPP_CMD, strerror(errno));
		rstatus = FENCE_EINVAL;
	}
	if (stat(SUNCLUSTER_CMD, &st) < 0) {
		fence_log(LOG_ERR, gettext("DeviceNetapp::init %s:%s\n"
		    "Sun Cluster support file not installed: %s: %s"),
		    serv, path, SUNCLUSTER_CMD, strerror(errno));
		rstatus = FENCE_EINVAL;
	}
	return (rstatus);
}

int
DeviceNetapp::fini()
{
	return (FENCE_OK);
}



//
// DeviceNetapp::doFence()
//
int
DeviceNetapp::doFence(std::list<Node*> list)
{
	std::list<Node*>::const_iterator it;
	string nodeList;
	int rc;
	int rstatus = FENCE_OK;

	fence_log(LOG_DEBUG, "DeviceNetapp::doFence");
	for (it = list.begin(); it != list.end(); it++) {
		nodeList += (*it)->name;
		nodeList += " ";
	}
	string cmd = FENCE + nodeList + "'";

	rc = execute(cmd);
	if (rc != 0) {
		fence_log(LOG_ERR, "DeviceNetapp::doFence cmd: %s failed",
		    cmd.c_str());
	}

	return (rstatus);
}

//
// DeviceNetapp::doUnFence()
//
int
DeviceNetapp::doUnFence(std::list<Node*> list)
{
	std::list<Node*>::const_iterator it;
	string nodeList;
	int rc;
	int rstatus = FENCE_OK;
	fence_log(LOG_DEBUG, "DeviceNetapp::doUnFence");

	for (it = list.begin(); it != list.end(); it++) {
		nodeList += (*it)->name;
		nodeList += " ";
	}
	string cmd = JOIN + nodeList + "'";
	rc = execute(cmd);
	if (rc != 0) {
		fence_log(LOG_ERR, "DeviceNetapp::doUnFence cmd: %s failed",
		    cmd.c_str());
	}

	return (rstatus);
}
