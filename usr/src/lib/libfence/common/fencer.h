/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FENCER_H
#define	_FENCER_H

#pragma ident	"@(#)fencer.h	1.3	08/05/20 SMI"

#include <list>
#include <string>
#include <pthread.h>

#include <scxcfg/scxcfg.h>

#include "fence_device.h"
#include "fence_int.h"


class Fencer
{
public:
	Fencer();
	virtual ~Fencer();
	int init(scxcfg_t cfg);
	int close();
	int fenceNodes(nodeList_t &nodes);
	int unFenceNodes(nodeList_t &nodes);
	int configChangeCallBack();
private:
	void lock();
	void unlock();
	int readConfig(scxcfg_t cfg);
	int checkConfig(scxcfg_t cfg);
	scxcfg_t Cfg;
	std::list<FenceDevice*> deviceList;
	pthread_mutex_t mutex;
};

#endif /* _FENCER_H */
