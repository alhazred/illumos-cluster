/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_HANFS_DFSTAB_H
#define	_HANFS_DFSTAB_H

#pragma ident	"@(#)hanfs_dfstab.h	1.3	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

struct share {
	const char *sh_path;
	const char *sh_opts;
	const char *sh_descr;
};

struct sh_list {		/* cached share list */
	struct sh_list *shl_next;
	struct share   *shl_sh;
};


#define	TOOLONG		1
#define	MAXBUFSIZE	65536

#define	SHOPT_RO	"ro"
#define	SHOPT_RW	"rw"

#define	SHOPT_SEC	"sec"
#define	SHOPT_SECURE	"secure"
#define	SHOPT_ROOT	"root"
#define	SHOPT_ANON	"anon"
#define	SHOPT_WINDOW	"window"
#define	SHOPT_NOSUB	"nosub"
#define	SHOPT_NOSUID	"nosuid"
#define	SHOPT_ACLOK	"aclok"
#define	SHOPT_PUBLIC	"public"
#define	SHOPT_INDEX	"index"
#define	SHOPT_LOG	"log"

int		getShareCmd(FILE *, struct share **);
int		putShareCmd(FILE *, struct share *);
int		remShareCmd(FILE *, const char *);
char 		*getshareopt(char *, const char *);
struct share	*sharedup(struct share *);
void		sharefree(struct share *);

#ifdef __cplusplus
}
#endif

#endif /* _HANFS_DFSTAB_H */
