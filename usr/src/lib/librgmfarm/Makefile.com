#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.6	08/05/20 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# lib/librgmfarm/Makefile.com
#

LIBRARY = librgmfarm.a
VERS = .1

# include library definitions, do not change order of include and DYNLIB
include ../../Makefile.lib

# include the macros for doors or rpc implementation
include $(SRC)/Makefile.ipc

RGMFARM_OBJS += rgm_callerr.o rgm_error.o
RGMFARM_OBJS += rgm_main.o rgm_schedprio.o rgm_comm.o rgm_comm_impl.o
RGMFARM_OBJS += rgm_intention_impl.o
RGMFARM_OBJS += rgm_launch_method.o rgm_lock_state.o 
RGMFARM_OBJS += rgm_process_resource.o rgm_readccr.o
RGMFARM_OBJS += rgm_recep_cluster.o rgm_recep_rg.o
RGMFARM_OBJS += rgm_recep_rs.o rgm_recep_rt.o
RGMFARM_OBJS += rgm_recep_start.o rgm_recep_utils.o rgm_receptionist.o rgm_rg.o
RGMFARM_OBJS += rgm_rs.o rgm_rs_impl.o rgm_run_state.o
RGMFARM_OBJS += rgm_scha_control.o rgm_scha_control_impl.o rgm_scswitch_impl.o
RGMFARM_OBJS += rgm_threads.o rgm_util.o
RGMFARM_OBJS += rgm_logical_node.o rgm_logical_nodeset.o
RGMFARM_OBJS += rgm_logical_node_manager.o
$(POST_S9_BUILD)RGMFARM_OBJS += rgm_zone_state.o

RGMXFARM_OBJS += rgmx_hook.o
RGMXFARM_OBJS += rgmx_main.o
RGMXFARM_OBJS += rgmx_cfg.o  rgmx_rpcomm.o
RGMXFARM_OBJS += rgmx_receptionist.o
RGMXFARM_OBJS += rgmx_util.o

RGMXFARM_OBJS += rgmx_cnfg_client.o
RGMXFARM_OBJS += rgmx_comm_pres_client.o rgmx_comm_farm_server.o
RGMXFARM_OBJS += rgmx_cnfg.o
RGMXFARM_OBJS += rgmx_rgm_compat.o

LIBRGM_SRC_DIR = $(SRC)/lib/librgm/common
LIBRGMSERVER_SRC_DIR = $(SRC)/lib/librgmserver/common
LIBRGMX_SRC_DIR = $(SRC)/lib/librgmx/common
LIBRGM_OBJS = rgm_cnfg.o rgm_common.o rgm_errmsg.o rgm_free.o rgm_datatype.o

RGMFARM_OBJS += $(LIBRGM_OBJS)
RGMFARM_OBJS += $(RGMXFARM_OBJS)

RGMFARM_DOOR_OBJS = rgm_recep_door_xdr.o rgm_recep_serv_xdr.o
$(RPC_IMPL)RGMFARM_DOOR_OBJS =
$(RPC_IMPL)RGMFARM_RPC_OBJS = rgm_recep_xdr.o
$(RPC_IMPL)RGMFARM_RPC_OBJS += rgm_recep_svc.o

RGMFARM_RPC_OBJS += rgmx_cnfg_clnt.o rgmx_cnfg_xdr.o rgmx_cnfg_common_xdr.o
RGMFARM_RPC_OBJS += rgmx_comm_pres_clnt.o rgmx_comm_xdr.o
RGMFARM_RPC_OBJS += rgmx_comm_farm_svc.o

OBJECTS += $(RGMFARM_OBJS) $(RGMFARM_RPC_OBJS) $(RGMFARM_DOOR_OBJS)

TEMPLATE_SRCS = rgm_array_free_list.cc rgm_obj_manager.cc

OBJS_DIR = objs

CPPFLAGS += $(CL_CPPFLAGS)

MAPFILE=	../common/mapfile-vers
PMAP =
SRCS =		$(OBJECTS:%.o=../common/%.c)

LIBS = $(LIBRARY)

CLEANFILES =

CHECK_FILES = $(RGMFARM_OBJS:%.o=%.c_check) \
	$(CHECKHDRS:%.h=%.h_check)
CHECK_FILES += $(TEMPLATE_SRCS:%.cc=%.c_check)

# definitions for lint
LINTFILES += $(OBJECTS:%.o=%.ln) $(TEMPLATE_SRCS:%.cc=%.ln)
LINTS_DIR = .

CC_PICFLAGS = -KPIC
C_PICFLAGS = -KPIC

CPPFLAGS += $(MTFLAG)
CFLAGS += $(C_PICFLAGS)
CFLAGS64 += $(C_PICFLAGS)
CCFLAGS += $(CC_PICFLAGS)
CCFLAGS64 += $(CC_PICFLAGS)
CPPFLAGS += -DPIC -D_TS_ERRNO
CPPFLAGS += -I../common -I$(SRC)/common/cl/interfaces/$(CLASS)

CPPFLAGS += -I$(SRC)/common/cl
CPPFLAGS += -I$(SRC)/lib/librgmx/common
CPPFLAGS += -I$(SRC)/lib/librgmserver/common
CPPFLAGS += -I$(SRC)/lib/dlscxcfg/common
CPPFLAGS += -DEUROPA_FARM -D_POSIX_PTHREAD_SEMANTICS
CPPFLAGS += -DSolaris

CPPFLAGS += -DSC_SRM
$(POST_S9_BUILD)CPPFLAGS += -I$(REF_PROTO)/usr/src/head
$(POST_S9_BUILD)CPPFLAGS += -I$(SRC)/common/cl/privip

LDLIBS += -lc -ldl  -lclos -lnsl -lgen -lscutils
LDLIBS += -lclevent -lscxcfg -lclst
LDLIBS += -lproject

RPCGENFLAGS	= $(RPCGENMT) -C -K -1

RPCFILE_XDR     = $(RPCFILE:$(SRC)/head/rgm/door/%.x=../common/%_xdr.c)
RPCFILE = $(SRC)/head/rgm/door/rgm_recep_door.x
RPCFILE_SVC = 

$(RPC_IMPL)RPCFILE_XDR	= $(RPCFILE:$(SRC)/head/rgm/rpc/%.x=../common/%_xdr.c) 
$(RPC_IMPL)RPCFILE = $(SRC)/head/rgm/rpc/rgm_recep.x

$(RPC_IMPL)RPCFILE_SVC	= $(RPCFILE:$(SRC)/head/rgm/rpc/%.x=../common/%_svc.c)

RPCFILE2_XDR = $(RPCFILE2:$(SRC)/head/rgm/rpc/%.x=../common/%_xdr.c)
RPCFILE2 = $(SRC)/head/rgm/rpc/rgmx_cnfg.x
RPCFILE2_CLNT = $(RPCFILE2:$(SRC)/head/rgm/rpc/%.x=../common/%_clnt.c)

RPCFILE3_XDR = $(RPCFILE3:$(SRC)/head/rgm/rpc/%.x=../common/%_xdr.c)
RPCFILE3 = $(SRC)/head/rgm/rpc/rgmx_comm.x

RPCFILE4_SVC = $(RPCFILE4:$(SRC)/head/rgm/rpc/%.x=../common/%_svc.c)
RPCFILE4 = $(SRC)/head/rgm/rpc/rgmx_comm_farm.x

RPCFILE5_CLNT = $(RPCFILE5:$(SRC)/head/rgm/rpc/%.x=../common/%_clnt.c)
RPCFILE5 = $(SRC)/head/rgm/rpc/rgmx_comm_pres.x

CLOBBERFILES += $(RPCFILE_XDR) $(RPCFILE_SVC) $(RPCFILE_CLNT)
CLOBBERFILES += $(RPCFILE2_XDR) $(RPCFILE2_CLNT)
CLOBBERFILES += $(RPCFILE3_XDR)
CLOBBERFILES += $(RPCFILE4_SVC)
CLOBBERFILES += $(RPCFILE5_CLNT)

.KEEP_STATE:

all:  $(LIBS)

# include library targets
include ../../Makefile.targ

objs/%.o pics/%.o: ../common/%.cc
	$(COMPILE.cc) -o $@ $<

objs/%.o pics/%.o: $(LIBRGM_SRC_DIR)/%.cc
	$(COMPILE.cc) -o $@ $<

objs/%.o pics/%.o: $(LIBRGMSERVER_SRC_DIR)/%.cc
	$(COMPILE.cc) -o $@ $<

objs/%.o pics/%.o: $(LIBRGMX_SRC_DIR)/%.cc
	$(COMPILE.cc) -o $@ $<

objs/%.o pics/%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<

%.ln: $(LIBRGMSERVER_SRC_DIR)/%.cc
	$(LINT.cc) $< > $@
	$(CAT) $@

%.ln: $(LIBRGMSERVER_SRC_DIR)/%.c
	$(LINT.c) $< > $@
	$(CAT) $@

%.ln: $(LIBRGMX_SRC_DIR)/%.cc
	$(LINT.cc) $< > $@
	$(CAT) $@

%.ln: $(LIBRGM_SRC_DIR)/%.cc
	$(LINT.cc) $< > $@
	$(CAT) $@

%.c_check: $(LIBRGMSERVER_SRC_DIR)/%.cc
	$(DOT_C_CHECK)

%.c_check: $(LIBRGMX_SRC_DIR)/%.cc
	$(DOT_C_CHECK)

%.c_check: $(LIBRGM_SRC_DIR)/%.cc
	$(DOT_C_CHECK)

%.h_check: $(LIBRGMSERVER_SRC_DIR)/%.h
	$(DOT_H_CHECK)

%.h_check: $(LIBRGMX_SRC_DIR)/%.h
	$(DOT_H_CHECK)

objs/%.o pics/%.o: $(LIBRGMSERVER_SRC_DIR)/%.c
	$(COMPILE.c) -o $@ $<

objs/%.o pics/%.o: $(LIBRGMX_SRC_DIR)/%.c
	$(COMPILE.c) -o $@ $<

$(RPCFILE2_XDR): $(RPCFILE2)
	@$(RM) -f $@
	$(RPCGEN) $(RPCGENFLAGS) -c -o $@ $(RPCFILE2)

$(RPCFILE2_CLNT): $(RPCFILE2)
	@$(RM) -f $@
	$(RPCGEN) $(RPCGENFLAGS) -l -o $@ $(RPCFILE2)

$(RPCFILE3_XDR): $(RPCFILE3)
	@$(RM) -f $@
	$(RPCGEN) $(RPCGENFLAGS) -c -o $@ $(RPCFILE3)

$(RPCFILE4_SVC): $(RPCFILE4)
	@$(RM) -f $@
	$(RPCGEN) $(RPCGENFLAGS) -m -o $@ $(RPCFILE4)

$(RPCFILE5_CLNT): $(RPCFILE5)
	@$(RM) -f $@
	$(RPCGEN) $(RPCGENFLAGS) -l -o $@ $(RPCFILE5)
