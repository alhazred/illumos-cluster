//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)rgmx_rgm_compat.cc	1.3	08/05/20 SMI"

//
// This code is used to provide compatibility support for Farm RGM
// running on farms nodes without libclomm (ie: the ORB in user mode).
// The RGM code re-used  for the Farm RGM sometimes makes
// use of utility types, whose definition come
// from the rgm.idl and its genrated file  rgm.h and rgm_stubs.c.
// The implementation of these utility variables is done here.
//
// For each idl type :  the methods needed are constructor, destructor
// and operator =
// All the methods related to ORB invocations are not used,
// so they are not reproduced in here.
//

#include "rgmx_rgm_compat.h"

//
// Marshal time support functions for rgm::node_state
//

rgm::node_state::node_state()
{
}

rgm::node_state::node_state(const node_state &__s) :
	seq(__s.seq)
{
}

rgm::node_state::~node_state()
{
}

rgm::node_state &
rgm::node_state::operator = (const node_state &__s)
{
	seq = __s.seq;
	return (*this);
}

//
// Marshal time support functions for rgm::zone_state
//

rgm::zone_state::zone_state()
{
}

rgm::zone_state::zone_state(const zone_state &__s) :
	lni(__s.lni),
	zseq(__s.zseq)
{
}

rgm::zone_state::~zone_state()
{
}

rgm::zone_state &
rgm::zone_state::operator = (const zone_state &__s)
{
	lni = __s.lni;
	zseq = __s.zseq;
	return (*this);
}

//
// Marshal time support functions for rgm::rg_state
//

rgm::rg_state::rg_state() :
	idlstr_rgname((char *)NULL)
{
}

rgm::rg_state::rg_state(const rg_state &__s) :
	idlstr_rgname(__s.idlstr_rgname),
	rgstate(__s.rgstate),
	rlist(__s.rlist)
{
}

rgm::rg_state::~rg_state()
{
	delete [] (char *)idlstr_rgname;
}

rgm::rg_state &
rgm::rg_state::operator = (const rg_state &__s)
{
	idlstr_rgname = __s.idlstr_rgname;
	rgstate = __s.rgstate;
	rlist = __s.rlist;
	return (*this);
}

//
// Marshal time support functions for rgm::r_state
//

rgm::r_state::r_state() :
	idlstr_rname((char *)NULL),
	idlstr_status_msg((char *)NULL)
{
}

rgm::r_state::r_state(const r_state &__s) :
	idlstr_rname(__s.idlstr_rname),
	rstate(__s.rstate),
	fmstatus(__s.fmstatus),
	idlstr_status_msg(__s.idlstr_status_msg)
{
}

rgm::r_state::~r_state()
{
	delete [] (char *)idlstr_rname;
	delete [] (char *)idlstr_status_msg;
}

rgm::r_state &
rgm::r_state::operator = (const r_state &__s)
{
	idlstr_rname = __s.idlstr_rname;
	rstate = __s.rstate;
	fmstatus = __s.fmstatus;
	idlstr_status_msg = __s.idlstr_status_msg;
	return (*this);
}

//
// Marshal time support functions for rgm::idl_validate_args
//

rgm::idl_validate_args::idl_validate_args() :
	idlstr_rs_name((char *)NULL),
	idlstr_rt_name((char *)NULL),
	idlstr_rg_name((char *)NULL),
	locale((char *)NULL)
{
}

rgm::idl_validate_args::idl_validate_args(const idl_validate_args &__s) :
	idlstr_rs_name(__s.idlstr_rs_name),
	idlstr_rt_name(__s.idlstr_rt_name),
	idlstr_rg_name(__s.idlstr_rg_name),
	locale(__s.locale),
	opcode(__s.opcode),
	is_scalable(__s.is_scalable),
	rs_props(__s.rs_props),
	ext_props(__s.ext_props),
	all_nodes_ext_props(__s.all_nodes_ext_props),
	rg_props(__s.rg_props)
{
}

rgm::idl_validate_args::~idl_validate_args()
{
	delete [] (char *)idlstr_rs_name;
	delete [] (char *)idlstr_rt_name;
	delete [] (char *)idlstr_rg_name;
	delete [] (char *)locale;
}

const rgm::schactl_flag_t rgm::SCHACTL_CHECKONLY = (1<<0);

const rgm::schactl_flag_t rgm::SCHACTL_GETOFF = (1<<1);

const rgm::schactl_flag_t rgm::SCHACTL_R_RESTART = (1<<2);

const rgm::schactl_flag_t rgm::SCHACTL_R_IS_RESTARTED = (1<<3);

const rgm::schactl_flag_t rgm::SCHACTL_R_DISABLE = (1 << 4);

const rgm::schactl_flag_t rgm::SCHACTL_R_ONLINE = (1 << 5);

const rgm::schactl_flag_t rgm::SCHACTL_R_OFFLINE = (1 << 6);
//
// Marshal time support functions for rgm::idl_scha_control_args
//

rgm::idl_scha_control_args::idl_scha_control_args() :
	idlstr_R_name((char *)NULL),
	idlstr_rg_name((char *)NULL)
{
}

rgm::idl_scha_control_args::idl_scha_control_args(
    const idl_scha_control_args &__s) :
	lni(__s.lni),
	idlstr_R_name(__s.idlstr_R_name),
	idlstr_rg_name(__s.idlstr_rg_name),
	flags(__s.flags)
{
}

rgm::idl_scha_control_args::~idl_scha_control_args()
{
	delete [] (char *)idlstr_R_name;
	delete [] (char *)idlstr_rg_name;
}

rgm::idl_scha_control_args &
rgm::idl_scha_control_args::operator = (const idl_scha_control_args &__s)
{
	lni = __s.lni;
	idlstr_R_name = __s.idlstr_R_name;
	idlstr_rg_name = __s.idlstr_rg_name;
	flags = __s.flags;
	return (*this);
}

//
// Marshal time support functions for rgm::idl_scha_control_result
//

rgm::idl_scha_control_result::idl_scha_control_result() :
	idlstr_err_msg((char *)NULL),
	idlstr_syslog_msg((char *)NULL)
{
}

rgm::idl_scha_control_result::idl_scha_control_result(
    const idl_scha_control_result &__s) :
	ret_code(__s.ret_code),
	idlstr_err_msg(__s.idlstr_err_msg),
	idlstr_syslog_msg(__s.idlstr_syslog_msg)
{
}

rgm::idl_scha_control_result::~idl_scha_control_result()
{
	delete [] (char *)idlstr_err_msg;
	delete [] (char *)idlstr_syslog_msg;
}

rgm::idl_scha_control_result &
rgm::idl_scha_control_result::operator = (const idl_scha_control_result &__s)
{
	ret_code = __s.ret_code;
	idlstr_err_msg = __s.idlstr_err_msg;
	idlstr_syslog_msg = __s.idlstr_syslog_msg;
	return (*this);
}

//
// Marshal time support functions for rgm::idl_regis_result_t
//

rgm::idl_regis_result_t::idl_regis_result_t() :
	idlstr_err_msg((char *)NULL)
{
}

rgm::idl_regis_result_t::idl_regis_result_t(const idl_regis_result_t &__s) :
	ret_code(__s.ret_code),
	idlstr_err_msg(__s.idlstr_err_msg)
{
}

rgm::idl_regis_result_t::~idl_regis_result_t()
{
	delete [] (char *)idlstr_err_msg;
}

rgm::idl_regis_result_t &
rgm::idl_regis_result_t::operator = (const idl_regis_result_t &__s)
{
	ret_code = __s.ret_code;
	idlstr_err_msg = __s.idlstr_err_msg;
	return (*this);
}

//
// Marshal time support functions for rgm::idl_scha_control_checkall_args
//

rgm::idl_scha_control_checkall_args::idl_scha_control_checkall_args() :
	idlstr_R_name((char *)NULL)
{
}

rgm::idl_scha_control_checkall_args::idl_scha_control_checkall_args(
    const idl_scha_control_checkall_args &__s) :
	lni(__s.lni),
	idlstr_R_name(__s.idlstr_R_name),
	flags(__s.flags)
{
}

rgm::idl_scha_control_checkall_args::~idl_scha_control_checkall_args()
{
	delete [] (char *)idlstr_R_name;
}

rgm::idl_scha_control_checkall_args &
rgm::idl_scha_control_checkall_args::operator = (
    const idl_scha_control_checkall_args &__s)
{
	lni = __s.lni;
	idlstr_R_name = __s.idlstr_R_name;
	flags = __s.flags;
	return (*this);
}

const rgm::intention_flag_t rgm::INTENT_ACTION = (1<<0);

const rgm::intention_flag_t rgm::INTENT_OK_FLAG = (1<<1);

const rgm::intention_flag_t rgm::INTENT_SYSTEM_ONLY = (1<<2);

const rgm::intention_flag_t rgm::INTENT_MANAGE_RG = (1<<3);

const rgm::intention_flag_t rgm::INTENT_RUN_UPDATE = (1<<4);

const rgm::intention_flag_t rgm::INTENT_RUN_FINI = (1<<5);

const rgm::intention_flag_t rgm::INTENT_READ_RS = (1<<6);

//
// Marshal time support functions for rgm::rgm_intention
//

rgm::rgm_intention::rgm_intention() :
	idlstr_entity_name((char *)NULL)
{
}

rgm::rgm_intention::rgm_intention(const rgm_intention &__s) :
	entity_type(__s.entity_type),
	operation_type(__s.operation_type),
	idlstr_entity_name(__s.idlstr_entity_name),
	flags(__s.flags)
{
}

rgm::rgm_intention::~rgm_intention()
{
	delete [] (char *)idlstr_entity_name;
}

rgm::rgm_intention &
rgm::rgm_intention::operator = (const rgm_intention &__s)
{
	entity_type = __s.entity_type;
	operation_type = __s.operation_type;
	idlstr_entity_name = __s.idlstr_entity_name;
	flags = __s.flags;
	return (*this);
}

//
// Marshal time support functions for rgm::lni_mappings
//
//

rgm::lni_mappings::lni_mappings() :
	idlstr_zonename((char *)NULL)
{
}

rgm::lni_mappings::lni_mappings(const lni_mappings &__s) :
	idlstr_zonename(__s.idlstr_zonename),
	lni(__s.lni),
	state(__s.state),
	incarn(__s.incarn)
{
}

rgm::lni_mappings::~lni_mappings()
{
	delete [] (char *)idlstr_zonename;
}

rgm::lni_mappings &
rgm::lni_mappings::operator = (const lni_mappings &__s)
{
	idlstr_zonename = __s.idlstr_zonename;
	lni = __s.lni;
	state = __s.state;
	incarn = __s.incarn;
	return (*this);
}
