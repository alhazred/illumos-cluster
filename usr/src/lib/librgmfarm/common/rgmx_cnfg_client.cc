//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// rgmx_cnfg_client.cc
//
// The functions in this file implement the client side of the internode
// Farm to Server communication through the RPC.
//
// CCR and NameServer service requests from Farm RGM delegates.
//

#pragma ident	"@(#)rgmx_cnfg_client.cc	1.6	08/08/01 SMI"

#include <sys/sc_syslog_msg.h>
#include <rgm/rgm_msg.h>
#include <rgm_proto.h>
#include <rgmx_proto.h>
#include <rgm/rpc/rgmx_cnfg.h>

//
// Get a reference to the rgm_cnfg server on the president node.
// Add it to pending connection list.
//
CLIENT *
rgmx_cnfg_getpres(char *cluster_name)
{
	CLIENT *clnt;
	sc_syslog_msg_handle_t handle;

	clnt = rgmx_clnt_create(rgmx_getpres(cluster_name),
		RGMX_CNFG_PROGRAM_NAME,
		RGMX_CNFG_PROGRAM, RGMX_CNFG_VERSION,
		FALSE); /* No keepalive sent in the way Farm->Server */
	if (clnt == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "rgmx_cnfg_getref(pres %s) failed",
		    rgmx_getpres(cluster_name));
		sc_syslog_msg_done(&handle);
		return (NULL);
	}

	rgmx_clnt_list_add(clnt);

	return (clnt);
}

//
// Free the reference to the rgm_cnfg server.
// Remove it also from the pending connection list.
//
void
rgmx_cnfg_freecnt(CLIENT *clnt)
{
	CL_PANIC(clnt != NULL);
	if (rgmx_clnt_list_remove(clnt) == TRUE)
		rgmx_clnt_destroy(clnt);
}
