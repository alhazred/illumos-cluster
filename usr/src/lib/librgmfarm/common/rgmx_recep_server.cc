/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */


/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * Linux does not support MT RPC services. Hence, we have provided our
 * own MT RPC implementation (see libscutils).
 * There are three public interfaces that each individual rpc server needs to
 * call:
 * - Call rpc_init_rpc_mgr() at the startup to initialize the rpc manager.
 * - Call rpc_req_queue_append() when receives rpc client request.
 * - Call rpc_cleanup_rpc_mgr() to clean up the rpc manager at shut down.
 */

#pragma ident	"@(#)rgmx_recep_server.cc	1.4	08/05/20 SMI"

#include <rgm/rpc/rgm_recep.h>
#include <rgm/rpc_services.h>

extern "C" {
	void scha_program_1(struct svc_req *rqstp, register SVCXPRT *transp);
}

void
scha_program_1(struct svc_req *rqstp, register SVCXPRT *transp)
{
	union {
		rs_open_args scha_rs_open_1_arg;
		rs_get_args scha_rs_get_1_arg;
		rs_get_args scha_rs_get_ext_1_arg;
		rs_get_state_status_args scha_rs_get_state_1_arg;
		rs_get_state_status_args scha_rs_get_status_1_arg;
		rs_get_fail_status_args scha_rs_get_failed_status_1_arg;
		rs_get_args scha_get_all_extprops_1_arg;
		rs_set_status_args scha_rs_set_status_1_arg;
		rt_open_args scha_rt_open_1_arg;
		rt_get_args scha_rt_get_1_arg;
		rt_get_args scha_is_pernode_1_arg;
		rg_open_args scha_rg_open_1_arg;
		rg_get_args scha_rg_get_1_arg;
		rg_get_state_args scha_rg_get_state_1_arg;
		scha_control_args scha_control_1_arg;
		cluster_get_args scha_cluster_get_1_arg;
	} argument;
	union {
		rs_open_result_t scha_rs_open_1_res;
		get_result_t scha_rs_get_1_res;
		get_ext_result_t scha_rs_get_ext_1_res;
		get_result_t scha_rs_get_state_1_res;
		get_status_result_t scha_rs_get_status_1_res;
		get_result_t scha_rs_get_failed_status_1_res;
		get_result_t scha_get_all_extprops_1_res;
		set_status_result_t scha_rs_set_status_1_res;
		open_result_t scha_rt_open_1_res;
		get_result_t scha_rt_get_1_res;
		get_result_t scha_is_pernode_1_res;
		open_result_t scha_rg_open_1_res;
		get_result_t scha_rg_get_1_res;
		get_result_t scha_rg_get_state_1_res;
		scha_result_t scha_control_1_res;
		get_result_t scha_cluster_get_1_res;
		open_result_t scha_cluster_open_1_res;
	} result;
	bool_t retval;
	xdrproc_t _xdr_argument, _xdr_result;
	bool_t (*local)(char *, void *, struct svc_req *);

	switch (rqstp->rq_proc) {
	case NULLPROC:
		(void) svc_sendreply(transp, (xdrproc_t)xdr_void, (char *)NULL);
		return;

	case SCHA_RS_OPEN:
		_xdr_argument = (xdrproc_t)xdr_rs_open_args;
		_xdr_result = (xdrproc_t)xdr_rs_open_result_t;
		local = (bool_t (*)
		    (char *, void *, struct svc_req *))scha_rs_open_1_svc;
		break;

	case SCHA_RS_GET:
		_xdr_argument = (xdrproc_t)xdr_rs_get_args;
		_xdr_result = (xdrproc_t)xdr_get_result_t;
		local = (bool_t (*)
		    (char *, void *, struct svc_req *))scha_rs_get_1_svc;
		break;

	case SCHA_RS_GET_EXT:
		_xdr_argument = (xdrproc_t)xdr_rs_get_args;
		_xdr_result = (xdrproc_t)xdr_get_ext_result_t;
		local = (bool_t (*)
		    (char *, void *, struct svc_req *))scha_rs_get_ext_1_svc;
		break;

	case SCHA_RS_GET_STATE:
		_xdr_argument = (xdrproc_t)xdr_rs_get_state_status_args;
		_xdr_result = (xdrproc_t)xdr_get_result_t;
		local = (bool_t (*)
		    (char *, void *, struct svc_req *))scha_rs_get_state_1_svc;
		break;

	case SCHA_RS_GET_STATUS:
		_xdr_argument = (xdrproc_t)xdr_rs_get_state_status_args;
		_xdr_result = (xdrproc_t)xdr_get_status_result_t;
		local = (bool_t (*)
		    (char *, void *, struct svc_req *))scha_rs_get_status_1_svc;
		break;

	case SCHA_RS_GET_FAILED_STATUS:
		_xdr_argument = (xdrproc_t)xdr_rs_get_fail_status_args;
		_xdr_result = (xdrproc_t)xdr_get_result_t;
		local = (bool_t (*) (char *, void *, struct svc_req *))
		    scha_rs_get_failed_status_1_svc;
		break;

	case SCHA_GET_ALL_EXTPROPS:
		_xdr_argument = (xdrproc_t)xdr_rs_get_args;
		_xdr_result = (xdrproc_t)xdr_get_result_t;
		local = (bool_t (*) (char *, void *, struct svc_req *))
		    scha_get_all_extprops_1_svc;
		break;

	case SCHA_IS_PERNODE:
		_xdr_argument = (xdrproc_t)xdr_rt_get_args;
		_xdr_result = (xdrproc_t)xdr_get_result_t;
		local = (bool_t (*) (char *, void *, struct svc_req *))
		    scha_is_pernode_1_svc;
		break;

	case SCHA_RS_GET_SWITCH:
		_xdr_argument = (xdrproc_t)xdr_rs_get_switch_args;
		_xdr_result = (xdrproc_t)xdr_get_switch_result_t;
		local = (bool_t (*)
		    (char *, void *, struct svc_req *))scha_rs_get_switch_1_svc;
		break;

	case SCHA_RS_SET_STATUS:
		_xdr_argument = (xdrproc_t)xdr_rs_set_status_args;
		_xdr_result = (xdrproc_t)xdr_set_status_result_t;
		local = (bool_t (*)
		    (char *, void *, struct svc_req *))scha_rs_set_status_1_svc;
		break;

	case SCHA_RT_OPEN:
		_xdr_argument = (xdrproc_t)xdr_rt_open_args;
		_xdr_result = (xdrproc_t)xdr_open_result_t;
		local = (bool_t (*)
		    (char *, void *, struct svc_req *))scha_rt_open_1_svc;
		break;

	case SCHA_RT_GET:
		_xdr_argument = (xdrproc_t)xdr_rt_get_args;
		_xdr_result = (xdrproc_t)xdr_get_result_t;
		local = (bool_t (*)
		    (char *, void *, struct svc_req *))scha_rt_get_1_svc;
		break;

	case SCHA_RG_OPEN:
		_xdr_argument = (xdrproc_t)xdr_rg_open_args;
		_xdr_result = (xdrproc_t)xdr_open_result_t;
		local = (bool_t (*)
		    (char *, void *, struct svc_req *))scha_rg_open_1_svc;
		break;

	case SCHA_RG_GET:
		_xdr_argument = (xdrproc_t)xdr_rg_get_args;
		_xdr_result = (xdrproc_t)xdr_get_result_t;
		local = (bool_t (*)
		    (char *, void *, struct svc_req *))scha_rg_get_1_svc;
		break;

	case SCHA_RG_GET_STATE:
		_xdr_argument = (xdrproc_t)xdr_rg_get_state_args;
		_xdr_result = (xdrproc_t)xdr_get_result_t;
		local = (bool_t (*)
		    (char *, void *, struct svc_req *))scha_rg_get_state_1_svc;
		break;

	case SCHA_CONTROL:
		_xdr_argument = (xdrproc_t)xdr_scha_control_args;
		_xdr_result = (xdrproc_t)xdr_scha_result_t;
		local = (bool_t (*)
		    (char *, void *, struct svc_req *))scha_control_1_svc;
		break;

	case SCHA_CLUSTER_GET:
		_xdr_argument = (xdrproc_t)xdr_cluster_get_args;
		_xdr_result = (xdrproc_t)xdr_get_result_t;
		local = (bool_t (*)
		    (char *, void *, struct svc_req *))scha_cluster_get_1_svc;
		break;

	case SCHA_CLUSTER_OPEN:
		_xdr_argument = (xdrproc_t)xdr_void;
		_xdr_result = (xdrproc_t)xdr_open_result_t;
		local = (bool_t (*)
		    (char *, void *, struct svc_req *))scha_cluster_open_1_svc;
		break;

	default:
		svcerr_noproc(transp);
		return;
	}

	/*
	 * Append the request in the rpc request queue
	 */
	(void) rpc_req_queue_append(transp, sizeof (argument),
	    _xdr_argument, _xdr_result, sizeof (result), local, rqstp);

}
