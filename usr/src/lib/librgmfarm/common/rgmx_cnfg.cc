//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// rgmx_cnfg.cc - API for storing/retrieving RGM configuration in CCR
//
// This file is used to build the ligthweight FRGMD daemon running on
// Europa farm nodes.
//
// The API is the same as for Sun Cluster server node (RGMD), for:
//
// + rgmcnfg_get_resource
// + rgmcnfg_get_rsrclist
// + rgmcnfg_get_rtlist
// + rgmcnfg_get_rglist
// + rgmcnfg_read_rgtable
// + rgmcnfg_read_rttable
// + get_property_value
//
// only the implementation will differ:
//
// + On server nodes, the rgmd is accessing its own copy of the CCR through
//   the ORB local.
// + On farm nodes, the frgmd is accessing the CCR copy located on the
//   president node through a president RGMD RPC service.
//
// The only modifications to the server node API are the following:
//
// + rgmcnfg_cl_resolve has been added to allow a farm node to access the
//   name server through a remove server node (president).
//

#pragma ident	"@(#)rgmx_cnfg.cc	1.5	08/07/21 SMI"

#include <sys/os.h>
#include <h/ccr.h>
#include <h/version_manager.h>
#include <nslib/ns.h>
#include <cmm/ucmm_api.h>
#include <rgm/rgm_cnfg.h>
#include <rgm/rgm_cmn_proto.h>
#include <rgm/rgm_private.h>
#include <sys/sc_syslog_msg.h>
#include <rgm/rpc/rgmx_cnfg.h>
#include <rgm_proto.h>
#include <rgmx_proto.h>

//
// rgmcnfg_get_resource
//
// Get the resource data from a server node remote copy of the CCR.
//
scha_errmsg_t
rgmcnfg_get_resource(name_t resourcename, name_t rgname,
	rgm_resource_t **resource)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	CLIENT *clnt = NULL;

	get_resource_args rpc_args;
	get_resource_result_t rpc_result = {0};

	clnt = rgmx_cnfg_getpres(ZONE);
	if (clnt == NULL) {
		res.err_code = SCHA_ERR_ORB;
		*resource = NULL;
		return (res);
	}

	rpc_args.res = resourcename;
	rpc_args.rg = rgname;

	if (rgmx_cnfg_get_resource_1(&rpc_args, &rpc_result, clnt)
	    != RPC_SUCCESS) {
		res.err_code = SCHA_ERR_ORB;
		*resource = NULL;
		return (res);
	}

	*resource = NULL;
	res.err_code = rpc_result.err;
	if (res.err_code == SCHA_ERR_NOERR)
		*resource = rpc_result.rgm_resource;

	/* Do not call xdr_free to avoid duplicating the structure */
	rgmx_cnfg_freecnt(clnt);
	return (res);
}


//
// rgmcnfg_get_rsrclist -
// Get a list of all the resource names in a given RG. This function
// allocates the memory for the resource name list. The caller must
// de-allocate the memory.
//
scha_errmsg_t
rgmcnfg_get_rsrclist(name_t rgname, namelist_t **rnames)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	CLIENT *clnt = NULL;
	get_rsrclist_result_t rpc_result = {0};

	if (rgname == NULL) {
		*rnames = NULL;
		return (res);
	}

	clnt = rgmx_cnfg_getpres(ZONE);
	if (clnt == NULL) {
		res.err_code = SCHA_ERR_ORB;
		*rnames = NULL;
		return (res);
	}

	if (rgmx_cnfg_get_rsrclist_1(&rgname, &rpc_result, clnt)
	    != RPC_SUCCESS) {
		res.err_code = SCHA_ERR_ORB;
		*rnames = NULL;
		return (res);
	}

	res.err_code = rpc_result.err;
	*rnames = namelist_copy(rpc_result.rnames);
	xdr_free((xdrproc_t)xdr_get_rsrclist_result_t, (char *)&rpc_result);
	rgmx_cnfg_freecnt(clnt);
	return (res);
}

//
// rgmcnfg_read_rgtable -
//	Get RG table from Server side and query each RG property
//	Each property is stored in CCR table as follows:
//	<property name>	<property value>
//
scha_errmsg_t
rgmcnfg_read_rgtable(name_t rgname, rgm_rg_t **rg)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	CLIENT *clnt = NULL;
	read_rgtable_result_t rpc_result = {0};

	clnt = rgmx_cnfg_getpres(ZONE);
	if (clnt == NULL) {
		res.err_code = SCHA_ERR_ORB;
		*rg = NULL;
		return (res);
	}

	if (rgmx_cnfg_read_rgtable_1(&rgname, &rpc_result, clnt)
	    != RPC_SUCCESS) {
		res.err_code = SCHA_ERR_ORB;
		*rg = NULL;
		return (res);
	}

	*rg = NULL;
	res.err_code = rpc_result.err;
	if (res.err_code == SCHA_ERR_NOERR)
		*rg = rpc_result.rgm_rg;

	/* Do not call xdr_free to avoid duplicating the structure */
	rgmx_cnfg_freecnt(clnt);
	return (res);
}

//
// rgmcnfg_get_rtlist -
//	Get a list of all RTs in the cluster from Server side.
//	Return NULL if no RT in the cluster
//
scha_errmsg_t
rgmcnfg_get_rtlist(namelist_t **rts)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	CLIENT *clnt = NULL;
	get_rtlist_result_t rpc_result = {0};

	*rts = NULL;

	clnt = rgmx_cnfg_getpres(ZONE);
	if (clnt == NULL) {
		res.err_code = SCHA_ERR_ORB;
		return (res);
	}

	if (rgmx_cnfg_get_rtlist_1(NULL, &rpc_result, clnt)
	    != RPC_SUCCESS) {
		res.err_code = SCHA_ERR_ORB;
		return (res);
	}

	res.err_code = rpc_result.err;
	*rts = namelist_copy(rpc_result.rts);
	xdr_free((xdrproc_t)xdr_get_rtlist_result_t, (char *)&rpc_result);
	rgmx_cnfg_freecnt(clnt);
	return (res);
}

//
// rgmcnfg_get_rglist():
//
//	Get all managed and unmanaged RGs in the cluster from Server side.
//
scha_errmsg_t
rgmcnfg_get_rglist(namelist_t **managed_rgs, namelist_t **unmanaged_rgs,
    boolean_t only_local_rgs)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	CLIENT *clnt = NULL;
	get_rglist_result_t rpc_result = {0};
	bool_t rpc_only_local_rgs = only_local_rgs ? true : false;

	*managed_rgs = NULL;
	*unmanaged_rgs = NULL;

	clnt = rgmx_cnfg_getpres(ZONE);
	if (clnt == NULL) {
		res.err_code = SCHA_ERR_ORB;
		return (res);
	}

	if (rgmx_cnfg_get_rglist_1(&rpc_only_local_rgs,
	    &rpc_result, clnt) != RPC_SUCCESS) {
		res.err_code = SCHA_ERR_ORB;
		return (res);
	}

	res.err_code = rpc_result.err;
	*managed_rgs = namelist_copy(rpc_result.managed_rgs);
	*unmanaged_rgs = namelist_copy(rpc_result.unmanaged_rgs);
	xdr_free((xdrproc_t)xdr_get_rglist_result_t, (char *)&rpc_result);
	rgmx_cnfg_freecnt(clnt);
	return (res);
}


//
// rgmcnfg_read_rttable -
//	Get all properties of a given RT from Server side.
//	Each property is stored in CCR table as follows:
//	   For system defined property:
//		<property name> <property value>
//	   For optional system defined property:
//		p.<property name> Tunable=<value>;Type=<value>;
//			Default=<value>;Min=<value>; Max=<value>;
//	   For extension property:
//		x.<property name>  Tunable=<value>;Type=<value>;
//			Default=<value>Max=<value>;Description=<value>"
scha_errmsg_t
rgmcnfg_read_rttable(name_t rtname, rgm_rt_t **rt)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	CLIENT *clnt = NULL;
	read_rttable_result_t rpc_result = {0};

	clnt = rgmx_cnfg_getpres(ZONE);
	if (clnt == NULL) {
		res.err_code = SCHA_ERR_ORB;
		*rt = NULL;
		return (res);
	}

	if (rgmx_cnfg_read_rttable_1(&rtname, &rpc_result, clnt)
	    != RPC_SUCCESS) {
		res.err_code = SCHA_ERR_ORB;
		*rt = NULL;
		return (res);
	}

	*rt = NULL;
	res.err_code = rpc_result.err;
	if (res.err_code == SCHA_ERR_NOERR)
		*rt = rpc_result.rgm_rt;

	/* Do not call xdr_free to avoid duplicating the structure */
	rgmx_cnfg_freecnt(clnt);
	return (res);
}


//
// get_property_value -
//	Get the value of specified property from Server side.
//	Note: only for RT or RG properties.
//
// Return codes:
// SCHA_ERR_NOERR: No Error,
// SCHA_ERR_RT: The RT not found (only applicable if objtype == O_RT),
// SCHA_ERR_RG: The RG not found (only applicable if objtype == O_RG),
// SCHA_ERR_CCR: CCR Open Table or Read Table (of RG or RT) failed,
// SCHA_ERR_PROP: The given key not found in the RG/RT table.
//
scha_errmsg_t
get_property_value(name_t objname, rgm_obj_type_t objtype,
	char *key, char **value)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	CLIENT *clnt = NULL;
	get_property_args rpc_args;
	get_property_result_t rpc_res = {0};

	*value = 0;

	//
	// This function cannot be used to get a resource property.
	//
	CL_PANIC(objtype != O_RSRC);

	clnt = rgmx_cnfg_getpres(ZONE);
	if (clnt == NULL) {
		res.err_code = SCHA_ERR_ORB;
		return (res);
	}

	rpc_args.objname = objname;
	rpc_args.objtype = objtype;
	rpc_args.key = key;

	if (rgmx_cnfg_get_property_value_1(&rpc_args, &rpc_res, clnt)
	    != RPC_SUCCESS) {
		res.err_code = SCHA_ERR_ORB;
		return (res);
	}

	res.err_code = rpc_res.ret_code;
	*value = rpc_res.value == NULL ? NULL : strdup_nocheck(rpc_res.value);
	xdr_free((xdrproc_t)xdr_get_property_result_t, (char *)&rpc_res);
	rgmx_cnfg_freecnt(clnt);

	return (res);
}


//
// rgmcnfg_cl_resolve -
//	Request to the Server to lookup the object associated with "name"
//	and return a pointer to information contained in "data".
//
// Returns:
//	0 on success
//	-1 on failure
//
int
rgmcnfg_cl_resolve(char *name, char **data)
{
	int cl_err;
	CLIENT *clnt = NULL;
	cl_resolve_result_t rpc_res = {0};

	clnt = rgmx_cnfg_getpres(ZONE);
	if (clnt == NULL)
		return (B_FALSE);

	if (rgmx_cnfg_cl_resolve_1(&name, &rpc_res, clnt)
	    != RPC_SUCCESS)
		return (-1);

	cl_err = rpc_res.ret_code;
	if (cl_err == 0) {
		*data = strdup_nocheck(rpc_res.data);
	}
	xdr_free((xdrproc_t)xdr_cl_resolve_result_t, (char *)&rpc_res);
	rgmx_cnfg_freecnt(clnt);

	return (cl_err);
}
