//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rgmx_comm_pres_client.cc	1.6	08/07/21 SMI"

//
// rgmx_comm_pres_server.cc
//
// The functions in this file implement the client side of the internode
// Farm to Server communication through the RPC.
//
#include <list>
#include <sys/sc_syslog_msg.h>
#include <rgm/rgm_msg.h>
#include <rgm_proto.h>
#include <rgmx_proto.h>
#include <rgm/rpc/rgmx_comm_pres.h>

#ifdef linux
#include <rgm/rpc_services.h>
#endif

//
// President address on the Europa private network is here.
// Use to request president services on both RGMX_COMM_PRES,
// and RGMX_CNFG RPC servers.
//
static char *the_pres_addr = NULL;
static os::mutex_t the_pres_lock;

//
// Keep the list of client handles opened with the current president.
//
// Used to handle the unresponsiveness of the Server president.
// In case of President crash or partitioned from the rest of the cluster,
// the Farm RGM retransmits its request using the standard RPC/TCP mechanism.
// If the RPC exits on timeout or reset, it is assumed the Server node is dead.
// When a new president comes up, it notifies all the Farm nodes by sending
// them its private address on the europa network. When receiving "I'm the new
// President" call, the Farm RGM resets all the pending connections with the
// defunct president.
//
static std::list<CLIENT*> clnt_list;
static os::mutex_t clnt_list_lock;

// Add a just opened connection to the pending list.
void
rgmx_clnt_list_add(CLIENT *clnt)
{
	clnt_list_lock.lock();

	clnt_list.push_front(clnt);

	clnt_list_lock.unlock();
}

// Remove a just closed connection from the pending list.
bool_t
rgmx_clnt_list_remove(CLIENT *clnt)
{
	bool_t found = FALSE;

	clnt_list_lock.lock();
	found = (std::find(clnt_list.begin(), clnt_list.end(), clnt) !=
	    clnt_list.end());
	if (found)
	    clnt_list.remove(clnt);
	clnt_list_lock.unlock();
	return (found);
}

// Abort all the pending connections belonging to the pending list.
void
rgmx_clnt_list_abort()
{
	clnt_list_lock.lock();

	std::list<CLIENT*>::const_iterator it;
	for (it = clnt_list.begin(); it != clnt_list.end(); it ++) {
		rgmx_clnt_destroy((*it));
	}

	clnt_list.clear();
	clnt_list_lock.unlock();
}

//
// Get the address of the current president on the europa
// private network.
//
char *
rgmx_getpres()
{
	char *pres_addr;

	the_pres_lock.lock();

	pres_addr = the_pres_addr;

	the_pres_lock.unlock();

	return (pres_addr);
}

//
// Set the address of the new president on the europa
// private network.
//
void
rgmx_setpres(char *pres_addr)
{
	/*
	 * If we are here, it means that a new president has been elected
	 * and therefore the old one is dead.
	 * Close all pending connections to the old president, before updating
	 * the president address.
	 */
	rgmx_clnt_list_abort();

	the_pres_lock.lock();

	ucmm_print("RGM", NOGET("rgmx_setpres: Set president address %s"),
			pres_addr);

	if (the_pres_addr != NULL)
		free(the_pres_addr);

	the_pres_addr = strdup(pres_addr);

	the_pres_lock.unlock();
}

//
// Get a reference to the rgm_comm_pres server on the president node.
// Add it to pending connection list.
//
CLIENT *
rgmx_comm_getpres(char *cluster_name)
{
	CLIENT *clnt = NULL;
	sc_syslog_msg_handle_t handle;
	char *pres_addr = rgmx_getpres(cluster_name);

	if (pres_addr == NULL) {
		return (NULL);
	}
	clnt = rgmx_clnt_create(pres_addr,
		RGMX_COMM_PRES_PROGRAM_NAME,
		RGMX_COMM_PRES_PROGRAM, RGMX_COMM_PRES_VERSION,
		FALSE); /* No keepalive sent in the way Farm->Server */
	if (clnt == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "rgmx_comm_getpres(pres %s) failed", pres_addr);
		sc_syslog_msg_done(&handle);
		return (NULL);
	}

	rgmx_clnt_list_add(clnt);

	return (clnt);
}

//
// Free the reference to the rgm_comm_pres server.
// Remove it also from the pending connection list.
//
void
rgmx_comm_freecnt(CLIENT *clnt)
{
	if (rgmx_clnt_list_remove(clnt) == TRUE)
		rgmx_clnt_destroy(clnt);
}
