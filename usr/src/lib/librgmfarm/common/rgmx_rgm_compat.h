/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RGMX_RGM_COMPAT_H
#define	_RGMX_RGM_COMPAT_H

#pragma ident	"@(#)rgmx_rgm_compat.h	1.3	08/05/20 SMI"

/*
 * This code is used to provide compatibility support for Farm RGM
 * running on farms nodes without libclomm (ie: the ORB in user mode).
 * The RGM code re-used  for the Farm RGM sometimes makes
 * use of utility types, whose definition come
 * from the rgm.idl and its genrated file  rgm.h and rgm_stubs.c.
 * The implementation of these utility variables is done here.
 *
 * For each idl type :  the methods needed are constructor, destructor
 * and operator =
 * All the methods related to ORB invocations are not used,
 * so they are not reproduced in here.
 */

#include  <orb/idl_datatypes/sequence.h>
#include <orb/idl_datatypes/idl_strings.h>
#include <orb/idl_datatypes/string_seq.h>


class rgm
{

public:
	enum rgm_rg_state {
		RG_ONLINE,
		RG_OFFLINE,
		RG_PENDING_ONLINE,
		RG_PENDING_OFFLINE,
		RG_ON_PENDING_METHODS,
		RG_OFF_PENDING_METHODS,
		RG_OFF_PENDING_BOOT,
		RG_OFF_BOOTED,
		RG_ERROR_STOP_FAILED,
		RG_PENDING_OFF_STOP_FAILED,
		RG_PENDING_OFF_START_FAILED,
		RG_OFFLINE_START_FAILED,
		RG_PENDING_ON_STARTED,
		RG_ON_PENDING_DISABLED,
		RG_ON_PENDING_MON_DISABLED,
		RG_ON_PENDING_R_RESTART,
		RG_PENDING_ONLINE_BLOCKED
	};

	enum rgm_r_state {
		R_MON_FAILED, R_OFFLINE, R_ONLINE, R_ONLINE_UNMON,
		R_STOPPED, R_PRENET_STARTED, R_START_FAILED, R_STOP_FAILED,
		R_PENDING_INIT, R_PENDING_FINI, R_ON_PENDING_UPDATE,
		R_ONUNMON_PENDING_UPDATE, R_MONFLD_PENDING_UPDATE,
		R_UNINITED, R_PENDING_BOOT, R_INITING, R_FINIING, R_BOOTING,
		R_MON_STARTING, R_MON_STOPPING, R_POSTNET_STOPPING,
		R_PRENET_STARTING, R_STARTING, R_STOPPING, R_UPDATING,
		R_JUST_STARTED, R_STOPPING_DEPEND, R_STARTING_DEPEND,
		R_OK_TO_START, R_OK_TO_STOP
	};

	enum rgm_r_fm_status {
		R_FM_ONLINE, R_FM_OFFLINE, R_FM_FAULTED, R_FM_DEGRADED,
		R_FM_UNKNOWN
	};

	struct r_state {
		r_state();
		r_state(const r_state &);
		~r_state();
		r_state &operator = (const r_state &);

		_string_field idlstr_rname;
		rgm_r_state rstate;
		rgm_r_fm_status fmstatus;
		_string_field idlstr_status_msg;
	};

	typedef uint32_t lni_t;
	typedef uint32_t ln_incarnation_t;
	typedef _NormalSeq_<r_state> r_state_seq_t;

	struct rg_state {
		rg_state();
		rg_state(const rg_state &);
		~rg_state();
		rg_state &operator = (const rg_state &);

		_string_field idlstr_rgname;
		rgm_rg_state rgstate;
		r_state_seq_t rlist;
	};
	typedef _NormalSeq_<rg_state> rg_state_seq_t;

	struct zone_state {
		zone_state();
		zone_state(const zone_state &);
		~zone_state();
		zone_state &operator = (const zone_state &);

		lni_t lni;
		rg_state_seq_t zseq;
	};
	typedef _NormalSeq_<zone_state> zone_state_seq_t;

	struct node_state {
		node_state();
		node_state(const node_state &);
		~node_state();
		node_state &operator = (const node_state &);

		zone_state_seq_t seq;
	};


	enum rgm_entity {
		RGM_ENT_RG, RGM_ENT_RS, RGM_ENT_RT
	};
	enum rgm_operation {
		RGM_OP_CREATE, RGM_OP_DELETE, RGM_OP_SYNCUP, RGM_OP_MANAGE,
		RGM_OP_UNMANAGE, RGM_OP_UPDATE, RGM_OP_ENABLE_R,
		RGM_OP_DISABLE_R, RGM_OP_FORCE_ENABLE_R,
		RGM_OP_FORCE_DISABLE_R, RGM_OP_ENABLE_M_R, RGM_OP_DISABLE_M_R
	};

	struct idl_validate_args {
		idl_validate_args();
		idl_validate_args(const idl_validate_args &);
		~idl_validate_args();
		idl_validate_args &operator = (const idl_validate_args &);

		_string_field idlstr_rs_name;
		_string_field idlstr_rt_name;
		_string_field idlstr_rg_name;
		_string_field locale;
		rgm_operation opcode;
		bool is_scalable;
		_string_seq  rs_props;
		_string_seq ext_props;
		_string_seq all_nodes_ext_props;
		_string_seq rg_props;
	};
	/*
	 * scha ctl stuff
	 */
	typedef uint32_t schactl_flag_t;
#define	rgm_SCHACTL_CHECKONLY (1<<0)
	static const rgm::schactl_flag_t SCHACTL_CHECKONLY;
#define	rgm_SCHACTL_GETOFF (1<<1)
	static const rgm::schactl_flag_t SCHACTL_GETOFF;
#define	rgm_SCHACTL_R_RESTART (1<<2)
	static const rgm::schactl_flag_t SCHACTL_R_RESTART;
#define	rgm_SCHACTL_R_IS_RESTARTED (1<<3)
	static const rgm::schactl_flag_t SCHACTL_R_IS_RESTARTED;
#define	rgm_SCHACTL_R_DISABLE (1<<4)
	static const rgm::schactl_flag_t SCHACTL_R_DISABLE;
#define	rgm_SCHACTL_R_ONLINE (1<<5)
	static const rgm::schactl_flag_t SCHACTL_R_ONLINE;
#define	rgm_SCHACTL_R_OFFLINE (1<<6)
	static const rgm::schactl_flag_t SCHACTL_R_OFFLINE;

	struct idl_scha_control_args {
		idl_scha_control_args();
		idl_scha_control_args(const idl_scha_control_args &);
		~idl_scha_control_args();
		idl_scha_control_args &operator =
		    (const idl_scha_control_args &);
		lni_t lni;
		_string_field idlstr_R_name;
		_string_field idlstr_rg_name;
		schactl_flag_t flags;
	};

	struct idl_scha_control_result {
		idl_scha_control_result();
		idl_scha_control_result(const idl_scha_control_result &);
		~idl_scha_control_result();
		idl_scha_control_result &operator =
		    (const idl_scha_control_result &);
		int32_t ret_code;
		_string_field idlstr_err_msg;
		_string_field idlstr_syslog_msg;
	};

	struct idl_regis_result_t {
		idl_regis_result_t();
		idl_regis_result_t(const idl_regis_result_t &);
		~idl_regis_result_t();
		idl_regis_result_t &operator = (const idl_regis_result_t &);

		int32_t ret_code;
		_string_field idlstr_err_msg;
	};

	struct idl_scha_control_checkall_args {
		idl_scha_control_checkall_args();
		idl_scha_control_checkall_args
		    (const idl_scha_control_checkall_args &);
		~idl_scha_control_checkall_args();
		idl_scha_control_checkall_args &operator = (const
		    idl_scha_control_checkall_args &);
		lni_t lni;
		_string_field idlstr_R_name;
		schactl_flag_t flags;
	};

	typedef uint32_t intention_flag_t;
#define	rgm_INTENT_ACTION (1<<0)
	static const rgm::intention_flag_t INTENT_ACTION;
#define	rgm_INTENT_OK_FLAG (1<<1)
	static const rgm::intention_flag_t INTENT_OK_FLAG;
#define	rgm_INTENT_SYSTEM_ONLY (1<<2)
	static const rgm::intention_flag_t INTENT_SYSTEM_ONLY;
#define	rgm_INTENT_MANAGE_RG (1<<3)
	static const rgm::intention_flag_t INTENT_MANAGE_RG;
#define	rgm_INTENT_RUN_UPDATE (1<<4)
	static const rgm::intention_flag_t INTENT_RUN_UPDATE;
#define	rgm_INTENT_RUN_FINI (1<<5)
	static const rgm::intention_flag_t INTENT_RUN_FINI;
#define	rgm_INTENT_READ_RS (1<<6)
	static const rgm::intention_flag_t INTENT_READ_RS;
	struct rgm_intention {
		rgm_intention();
		rgm_intention(const rgm_intention &);
		~rgm_intention();
		rgm_intention &operator = (const rgm_intention &);

		rgm_entity entity_type;
		rgm_operation operation_type;
		_string_field idlstr_entity_name;
		intention_flag_t flags;
	};

	enum rgm_ln_state {
		LN_UNKNOWN, LN_UNCONFIGURED, LN_DOWN, LN_STUCK,
		LN_SHUTTING_DOWN, LN_UP
	};
	struct lni_mappings {
		lni_mappings();
		lni_mappings(const lni_mappings &);
		~lni_mappings();
		lni_mappings &operator = (const lni_mappings &);

		_string_field idlstr_zonename;
		lni_t lni;
		rgm_ln_state state;
		ln_incarnation_t incarn;
	};

	typedef _NormalSeq_<lni_mappings> lni_mappings_seq_t;

};

#endif /* _RGMX_RGM_COMPAT_H */
