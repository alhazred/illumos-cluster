//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rgmx_comm_farm_server.cc	1.3	08/05/20 SMI"

//
// rgmx_comm_farm_server.cc
//
// The functions in this file implement the server side of the internode
// Server to Farm communication through the RPC.
//

#include <sys/sc_syslog_msg.h>
#include <rgm/rgm_msg.h>
#include <rgm_proto.h>
#include <rgmx_proto.h>
#include <rgm/rpc/rgmx_comm_farm.h>

#ifdef linux
#include <rgm/rpc_services.h>
#endif

//
// RPC server handle on TCP network.
// Bound to local private Europa network address / port.
//
static SVCXPRT *svc_transp = NULL;

extern "C" {
void rgmx_comm_farm_program_1(struct svc_req *rqstp, register SVCXPRT *transp);
}

//
// Start the rgmx_comm_farm server and bind it to the local
// europa private address.
//
void
rgmx_comm_init(void)
{
	sol::nodeid_t nodeid;
	char *local_addr;
	sc_syslog_msg_handle_t handle;

	(void) rgm_get_nodeid(NULL, &nodeid);
	local_addr = rgmx_cfg_getaddr(nodeid);
	if (local_addr == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"rgmx_comm_init: unable to get network private "
			"address; aborting node");
		sc_syslog_msg_done(&handle);
		abort();
		/* NOTREACHED */
	}

	// Create the rcp/tcp server handle.
	if ((svc_transp = rgmx_svc_create(local_addr)) == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("rgmx_comm_init: unable to create "
		    "rpc/tcp server handle; aborting node"));
		sc_syslog_msg_done(&handle);
		abort();
		/* NOTREACHED */
	}

	if (rgmx_svc_register(svc_transp,
		rgmx_comm_farm_program_1,
		RGMX_COMM_FARM_PROGRAM_NAME,
		RGMX_COMM_FARM_PROGRAM, RGMX_COMM_FARM_VERSION) != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("rgmx_comm_init: unable to register "
		    "rgmx_comm_farm rpc service; aborting node"));
		sc_syslog_msg_done(&handle);
		abort();
		/* NOTREACHED */
	}

	ucmm_print("RGM", NOGET("rgmx_comm_init: rgmx_comm_farm service started"
			" local address is %s\n"), local_addr);
	free(local_addr);
}

//
// Free RPC results.
// Called by rgmx_comm_farm_program_1 (dispatcher) after having
// sent the reply to the client.
//
int
rgmx_comm_farm_program_1_freeresult(SVCXPRT *, xdrproc_t xdr_result,
	caddr_t res)
{
	/*
	 * Insert additional freeing code here, if needed
	 */
	xdr_free(xdr_result, res);

	return (1);
}

#ifdef linux
//
// Linux does not support MT RPC services. Hence, we have provided our
// own MT RPC implementation (see libscutils).
// There are three public interfaces that each individual rpc server needs to
// call:
// - Call rpc_init_rpc_mgr() at the startup to initialize the rpc manager.
// - Call rpc_req_queue_append() when receives rpc client request.
// - Call rpc_cleanup_rpc_mgr() to clean up the rpc manager at shut down.
//
void
rgmx_comm_farm_program_1(struct svc_req *rqstp, register SVCXPRT *transp)
{
	union {
		rgmx_validate_args rgmx_scrgadm_rs_validate_1_arg;
		rgmx_scha_control_checkall_args
		    rgmx_scha_control_checkall_1_arg;
		char *rgmx_set_timestamp_1_arg;
		rgmx_latch_intention_args rgmx_latch_intention_1_arg;
		rgmx_process_intention_args rgmx_process_intention_1_arg;
		rgmx_process_intention_args rgmx_unlatch_intention_1_arg;
		rgmx_set_pres_args rgmx_set_pres_1_arg;
		rgmx_read_ccr_args rgmx_read_ccr_1_arg;
		rgmx_run_boot_meths_args rgmx_run_boot_meths_1_arg;
		rgmx_chg_mastery_args rgmx_chg_mastery_1_arg;
		rgmx_r_restart_args rgmx_r_restart_1_arg;
		rgmx_clear_flag_args rgmx_clear_flag_1_arg;
		rgmx_ack_start_args rgmx_ack_start_1_arg;
		rgmx_notify_dependencies_resolved_args
		    rgmx_notify_dependencies_resolved_1_arg;
		rgmx_fetch_restarting_flag_args
		    rgmx_fetch_restarting_flag_1_arg;
		rgmx_send_lni_mappings_args rgmx_send_lni_mappings_1_arg;
		rgmx_reclaim_lnis_args rgmx_reclaim_lnis_1_arg;
	} argument;
	union {
		rgmx_regis_result_t rgmx_scrgadm_rs_validate_1_res;
		rgmx_scha_control_result rgmx_scha_control_checkall_1_res;
		rgmx_regis_result_t rgmx_set_timestamp_1_res;
		node_state rgmx_xmit_state_1_res;
		bool_t rgmx_am_i_joining_1_res;
		rgmx_regis_result_t rgmx_clear_flag_1_res;
		r_restart_t rgmx_fetch_restarting_flag_1_res;
		rgmx_retrieve_lni_mappings_result
		    rgmx_retrieve_lni_mappings_1_res;
	} result;
	bool_t retval;
	xdrproc_t _xdr_argument, _xdr_result;
	bool_t (*local)(char *, void *, struct svc_req *);

	switch (rqstp->rq_proc) {
	case NULLPROC:
		(void) svc_sendreply(transp,
			(xdrproc_t)xdr_void, (char *)NULL);
		return;

	case RGMX_SCRGADM_RS_VALIDATE:
		_xdr_argument = (xdrproc_t)xdr_rgmx_validate_args;
		_xdr_result = (xdrproc_t)xdr_rgmx_regis_result_t;
		local = (bool_t (*) (char *,  void *,  struct svc_req *))
		    rgmx_scrgadm_rs_validate_1_svc;
		break;

	case RGMX_SCHA_CONTROL_CHECKALL:
		_xdr_argument = (xdrproc_t)xdr_rgmx_scha_control_checkall_args;
		_xdr_result = (xdrproc_t)xdr_rgmx_scha_control_result;
		local = (bool_t (*) (char *,  void *,  struct svc_req *))
		    rgmx_scha_control_checkall_1_svc;
		break;

	case RGMX_SET_TIMESTAMP:
		_xdr_argument = (xdrproc_t)xdr_wrapstring;
		_xdr_result = (xdrproc_t)xdr_rgmx_regis_result_t;
		local = (bool_t (*) (char *,  void *,  struct svc_req *))
		    rgmx_set_timestamp_1_svc;
		break;

	case RGMX_LATCH_INTENTION:
		_xdr_argument = (xdrproc_t)xdr_rgmx_latch_intention_args;
		_xdr_result = (xdrproc_t)xdr_rgmx_latch_intention_result;
		local = (bool_t (*) (char *,  void *,  struct svc_req *))
		    rgmx_latch_intention_1_svc;
		break;

	case RGMX_PROCESS_INTENTION:
		_xdr_argument = (xdrproc_t)xdr_rgmx_process_intention_args;
		_xdr_result = (xdrproc_t)xdr_void;
		local = (bool_t (*) (char *,  void *,  struct svc_req *))
		    rgmx_process_intention_1_svc;
		break;

	case RGMX_UNLATCH_INTENTION:
		_xdr_argument = (xdrproc_t)xdr_rgmx_process_intention_args;
		_xdr_result = (xdrproc_t)xdr_void;
		local = (bool_t (*) (char *,  void *,  struct svc_req *))
		    rgmx_unlatch_intention_1_svc;
		break;

	case RGMX_XMIT_STATE:
		_xdr_argument = (xdrproc_t)xdr_void;
		_xdr_result = (xdrproc_t)xdr_node_state;
		local = (bool_t (*) (char *,  void *,  struct svc_req *))
		    rgmx_xmit_state_1_svc;
		break;

	case RGMX_SET_PRES:
		_xdr_argument = (xdrproc_t)xdr_rgmx_set_pres_args;
		_xdr_result = (xdrproc_t)xdr_void;
		local = (bool_t (*) (char *,  void *,  struct svc_req *))
		    rgmx_set_pres_1_svc;
		break;

	case RGMX_AM_I_JOINING:
		_xdr_argument = (xdrproc_t)xdr_void;
		_xdr_result = (xdrproc_t)xdr_bool;
		local = (bool_t (*) (char *,  void *,  struct svc_req *))
		    rgmx_am_i_joining_1_svc;
		break;

	case RGMX_READ_CCR:
		_xdr_argument = (xdrproc_t)xdr_rgmx_read_ccr_args;
		_xdr_result = (xdrproc_t)xdr_void;
		local = (bool_t (*) (char *,  void *,  struct svc_req *))
		    rgmx_read_ccr_1_svc;
		break;

	case RGMX_RUN_BOOT_METHS:
		_xdr_argument = (xdrproc_t)xdr_rgmx_run_boot_meths_args;
		_xdr_result = (xdrproc_t)xdr_void;
		local = (bool_t (*) (char *,  void *,  struct svc_req *))
		    rgmx_run_boot_meths_1_svc;
		break;

	case RGMX_CHG_MASTERY:
		_xdr_argument = (xdrproc_t)xdr_rgmx_chg_mastery_args;
		_xdr_result = (xdrproc_t)xdr_rgmx_chg_mastery_result;
		local = (bool_t (*) (char *,  void *,  struct svc_req *))
		    rgmx_chg_mastery_1_svc;
		break;

	case RGMX_R_RESTART:
		_xdr_argument = (xdrproc_t)xdr_rgmx_r_restart_args;
		_xdr_result = (xdrproc_t)xdr_rgmx_r_restart_result;
		local = (bool_t (*) (char *,  void *,  struct svc_req *))
		    rgmx_r_restart_1_svc;
		break;

	case RGMX_CLEAR_FLAG:
		_xdr_argument = (xdrproc_t)xdr_rgmx_clear_flag_args;
		_xdr_result = (xdrproc_t)xdr_rgmx_regis_result_t;
		local = (bool_t (*) (char *,  void *,  struct svc_req *))
		    rgmx_clear_flag_1_svc;
		break;

	case RGMX_ACK_START:
		_xdr_argument = (xdrproc_t)xdr_rgmx_ack_start_args;
		_xdr_result = (xdrproc_t)xdr_rgmx_ack_start_result;
		local = (bool_t (*) (char *,  void *,  struct svc_req *))
		    rgmx_ack_start_1_svc;
		break;

	case RGMX_NOTIFY_DEPENDENCIES_RESOLVED:
		_xdr_argument =
		    (xdrproc_t)xdr_rgmx_notify_dependencies_resolved_args;
		_xdr_result =
		    (xdrproc_t)xdr_rgmx_notify_dependencies_resolved_result;
		local = (bool_t (*) (char *,  void *,  struct svc_req *))
		    rgmx_notify_dependencies_resolved_1_svc;
		break;

	case RGMX_FETCH_RESTARTING_FLAG:
		_xdr_argument = (xdrproc_t)xdr_rgmx_fetch_restarting_flag_args;
		_xdr_result = (xdrproc_t)xdr_rgmx_fetch_restarting_flag_result;
		local = (bool_t (*) (char *,  void *,  struct svc_req *))
		    rgmx_fetch_restarting_flag_1_svc;
		break;

	case RGMX_RETRIEVE_LNI_MAPPINGS:
		_xdr_argument = (xdrproc_t)xdr_void;
		_xdr_result = (xdrproc_t)xdr_rgmx_retrieve_lni_mappings_result;
		local = (bool_t (*) (char *,  void *,  struct svc_req *))
		    rgmx_retrieve_lni_mappings_1_svc;
		break;

	case RGMX_SEND_LNI_MAPPINGS:
		_xdr_argument = (xdrproc_t)xdr_rgmx_send_lni_mappings_args;
		_xdr_result = (xdrproc_t)xdr_void;
		local = (bool_t (*) (char *,  void *,  struct svc_req *))
		    rgmx_send_lni_mappings_1_svc;
		break;

	case RGMX_RECLAIM_LNIS:
		_xdr_argument = (xdrproc_t)xdr_rgmx_reclaim_lnis_args;
		_xdr_result = (xdrproc_t)xdr_void;
		local = (bool_t (*) (char *,  void *,  struct svc_req *))
		    rgmx_reclaim_lnis_1_svc;
		break;

	default:
		svcerr_noproc(transp);
		return;
	}

	/*
	 * Append the request in the rpc request queue
	 */
	(void) rpc_req_queue_append(transp, sizeof (argument),
	    _xdr_argument, _xdr_result, sizeof (result), local, rqstp);
}
#endif
