/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_ST_FF_CLIENT_H
#define	_ST_FF_CLIENT_H

#pragma ident	"@(#)st_ff_client.h	1.6	08/05/20 SMI"

/*
 * Implementation details:
 *
 * The failfast objects are proxied by doors, which have their own
 * unreferenced protocol.  The server that hands out these doors is
 * fattach()ed at ST_FF_DOORNAME.  A door_call() to that door with
 * the string ST_FF_REQUEST in the data_ptr field will return with a
 * door that acts as the ff proxy and is disarmed.  The string should be
 * followed by a ST_FF_NAMESIZE or fewer character string that identifies the
 * process requesting the failfast object.  A call to that door
 * passing the string ST_FF_ARM will arm the door, and a call passing
 * ST_FF_DISARM will disarm the door.  The state of the failfast (armed
 * or disarmed) is maintained on the client side and checked there before
 * calls are made).
 */

#define	ST_FF_DOORNAME	"/etc/.cl_failfast_door"
#define	ST_FF_REQUEST	"CREATE_FAILFAST_PROXY"
#define	ST_FF_ARM	"ARM_FAILFAST_PROXY"
#define	ST_FF_DISARM	"DISARM_FAILFAST_PROXY"
#define	ST_FF_NAMESIZE	32

#endif	/* _ST_FF_CLIENT_H */
