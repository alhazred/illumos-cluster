/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)st_ff_client.c	1.18	08/05/20 SMI"

#include <door.h>
#include <assert.h>
#include <string.h>
#include <strings.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <thread.h>
#include <synch.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#define	ASSERT assert
#include <sys/st_failfast.h>
#include <st_ff_client.h>

static int ff_desc = -1;
static char ff_armed = 0;
static char ff_arm_str[] = ST_FF_ARM;
static char ff_disarm_str[] = ST_FF_DISARM;
static mutex_t ff_mutex = DEFAULTMUTEX; /*lint !e708 */

/*
 * These two routines, st_ff_arm() and st_ff_disarm(), provide an interface
 * to the failfast service in the kernel via a proxy door provided by the
 * st_ff_server. They assume that there is only one failfast object per
 * process and that the process is single threaded.  Multithreaded processes
 * should link against libclcomm and libclconf and use the normal failfast
 * mechanism.
 */

/*
 * Door calls are made in a loop because they can be interrupted by signals.
 * All of the calls made are idempotent.
 */

int
st_ff_arm(char *pname)
{
	door_arg_t door_args;
	int ff_factory = -1;
	int the_err = 0;
	size_t strsize = 0;
	char rbuf[100];
	char request_str[100];
	int err;

	(void) mutex_lock(&ff_mutex);

	if (ff_desc == -1) {
		if (strlen(pname) > ST_FF_NAMESIZE) {
			(void) mutex_unlock(&ff_mutex);
			return (E2BIG);
		}
		(void) strcpy(request_str, ST_FF_REQUEST);
		(void) strcpy(request_str + strlen(ST_FF_REQUEST) + 1, pname);
		strsize = strlen(ST_FF_REQUEST) + strlen(pname) + 2;
	}

	while (ff_desc == -1) {
		ASSERT(ff_armed == 0);

		/* Get failfast door descriptor */
		ff_factory = open(ST_FF_DOORNAME, O_RDONLY);
		if (ff_factory < 0) {
			(void) mutex_unlock(&ff_mutex);
			return (errno); /*lint !e746 */
		}

		/* Make request for failfast proxy door */
		door_args.data_ptr = request_str;
		door_args.data_size = strsize;
		door_args.desc_ptr = NULL;
		door_args.desc_num = 0;
		door_args.rbuf = rbuf;
		door_args.rsize = sizeof (rbuf);

		if (door_call(ff_factory,  &door_args) == -1) {
			err = errno;
			(void) close(ff_factory);
			ff_factory = -1;
			if (err == EINTR) {
				continue;
			} else {
				(void) mutex_unlock(&ff_mutex);
				return (err);
			}
		}

		/* Close the descriptor for the server */
		(void) close(ff_factory);

		/* If there are no errors, store the proxy door descriptor */
		ASSERT(door_args.data_size == sizeof (int));
		bcopy(door_args.data_ptr, &the_err, sizeof (int));
		if (the_err != 0) {
			(void) mutex_unlock(&ff_mutex);
			return (the_err);
		}
		ASSERT(door_args.rbuf == rbuf);
		ASSERT(door_args.desc_num == 1);
		ff_desc = door_args.desc_ptr[0].d_data.d_desc.d_descriptor;
		(void) fcntl(ff_desc, F_SETFD, FD_CLOEXEC);
		ASSERT(ff_desc >= 0);
	}

	/* If the failfast is already armed, don't bother making the call */
	if (ff_armed == 1) {
		(void) mutex_unlock(&ff_mutex);
		return (EINVAL);
	}
	while (ff_armed == 0) {
		/* Make the call to arm the failfast (it starts disarmed) */
		door_args.data_ptr = ff_arm_str;
		door_args.data_size = strlen(ff_arm_str) + 1;
		door_args.desc_ptr = NULL;
		door_args.desc_num = 0;
		door_args.rbuf = rbuf;
		door_args.rsize = sizeof (rbuf);

		if (door_call(ff_desc,  &door_args) == -1) {
			if (errno == EINTR)
				continue;
			else {
				(void) mutex_unlock(&ff_mutex);
				return (errno);
			}
		}

		/* Return an error, if any */
		ASSERT(door_args.data_size == sizeof (int));
		ASSERT(door_args.rbuf == rbuf);
		bcopy(door_args.data_ptr, &the_err, sizeof (int));
		if (the_err != 0) {
			(void) mutex_unlock(&ff_mutex);
			return (the_err);
		}
		ff_armed = 1;
	}

	(void) mutex_unlock(&ff_mutex);
	return (0);
}

int
st_ff_disarm()
{
	door_arg_t door_args;
	int the_err = 0;
	char rbuf[20];

	(void) mutex_lock(&ff_mutex);

	/* If the failfast is already unarmed, or there is none, don't bother */
	if ((ff_desc == -1) || (ff_armed == 0)) {
		(void) mutex_unlock(&ff_mutex);
		return (EINVAL);
	}

	while (ff_armed == 1) {
		/* Make the call to disarm the failfast */
		door_args.data_ptr = ff_disarm_str;
		door_args.data_size = strlen(ff_disarm_str) + 1;
		door_args.desc_ptr = NULL;
		door_args.desc_num = 0;
		door_args.rbuf = rbuf;
		door_args.rsize = sizeof (rbuf);

		if (door_call(ff_desc,  &door_args) == -1) {
			if (errno == EINTR)
				continue;
			else {
				(void) mutex_unlock(&ff_mutex);
				return (errno);
			}
		}

		/* Return an error, if any */
		ASSERT(door_args.data_size == sizeof (int));
		ASSERT(door_args.rbuf == rbuf);
		bcopy(door_args.data_ptr, &the_err, sizeof (int));
		if (the_err != 0) {
			(void) mutex_unlock(&ff_mutex);
			return (the_err);
		}
		ff_armed = 0;
	}

	(void) mutex_unlock(&ff_mutex);
	return (0);
}
