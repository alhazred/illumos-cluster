#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)Makefile.lib	1.35	08/05/20 SMI"
#
# lib/Makefile.lib
# Definitions common to libraries.
#

include		$(SRC)/Makefile.master

TMPLDIR=	.

#
# Default to 32bit
#
CLASS=	32

LIBS=		$(LIBRARY) $(LIBRARYCCC)
MACHLIBS=	$(LIBS:%=$(MACH)/%)
MACHLIBS64=	$(LIBS:%=$(MACH64)/%)
DYNLIB=		$(LIBRARY:.a=.so$(VERS))
DYNLIBCCC=	$(LIBRARYCCC:.a=.so$(VERS))
LIBLINKS=	$(LIBRARY:.a=.so)
LIBLINKSCCC=	$(LIBRARYCCC:.a=.so)
LIBNAME=	$(LIBRARY:lib%.a=%)
LIBLINKPATH=
ROOTLIBDIR=	$(VROOT)/usr/lib
ROOTLIBDIR64=	$(VROOT)/usr/lib/$(MACH64)
ROOTLIBS=	$(LIBS:%=$(ROOTLIBDIR)/%)
ROOTLIBS64=	$(LIBS:%=$(ROOTLIBDIR64)/%)
ROOTLINKS=	$(ROOTLIBDIR)/$(LIBLINKS)
ROOTLINKS64=	$(ROOTLIBDIR64)/$(LIBLINKS)
ROOTLINKSCCC=	$(ROOTLIBDIR)/$(LIBLINKSCCC)
ROOTLINKSCCC64=	$(ROOTLIBDIR64)/$(LIBLINKSCCC)

LIBCLUSTLINKPATH=
LIBRGMLINKPATH=
ROOTCLUSTINC=		$(VROOT)/usr/cluster/include
ROOTCLUSTHDRDIR=	$(ROOTCLUSTINC)
ROOTCLUSTLIBDIR=	$(VROOT)/usr/cluster/lib
ROOTCLUSTLIBDIR64=	$(ROOTCLUSTLIBDIR)/$(MACH64)
ROOTCLUSTLIBSCDIR=	$(ROOTCLUSTLIBDIR)/sc
ROOTCLUSTLIBSCDIR64=	$(ROOTCLUSTLIBSCDIR)/$(MACH64)
ROOTRGMLIBDIR=		$(ROOTCLUSTLIBDIR)/rgm
ROOTRGMLIBDIR64=	$(ROOTRGMLIBDIR)/$(MACH64)
ROOTCLUSTLIBS=		$(LIBS:%=$(ROOTCLUSTLIBDIR)/%)
ROOTCLUSTLIBS64=	$(LIBS:%=$(ROOTCLUSTLIBDIR64)/%)
ROOTCLUSTPRIVLIBS=	$(LIBS:%=$(ROOTCLUSTLIBSCDIR)/%)
ROOTCLUSTPRIVLIBS64=	$(LIBS:%=$(ROOTCLUSTLIBSCDIR64)/%)
ROOTRGMLIBS=		$(LIBS:%=$(ROOTRGMLIBDIR)/%)
ROOTRGMLIBS64=		$(LIBS:%=$(ROOTRGMLIBDIR64)/%)
ROOTCLUSTLINKS=		$(ROOTCLUSTLIBDIR)/$(LIBLINKS)
ROOTCLUSTLINKS64=	$(ROOTCLUSTLIBDIR64)/$(LIBLINKS)
ROOTCLUSTPRIVLINKS=	$(ROOTCLUSTLIBSCDIR)/$(LIBLINKS)
ROOTCLUSTPRIVLINKS64=	$(ROOTCLUSTLIBSCDIR64)/$(LIBLINKS)
ROOTCLUSTLINKSCCC=	$(ROOTCLUSTLIBDIR)/$(LIBLINKSCCC)
ROOTCLUSTLINKSCCC64=	$(ROOTCLUSTLIBDIR64)/$(LIBLINKSCCC)
ROOTRGMLINKS=		$(ROOTRGMLIBDIR)/$(LIBLINKS)
ROOTRGMLINKS64=		$(ROOTRGMLIBDIR64)/$(LIBLINKS)
ROOTRGMLINKSCCC=	$(ROOTRGMLIBDIR)/$(LIBLINKSCCC)
ROOTRGMLINKSCCC64=	$(ROOTRGMLIBDIR64)/$(LIBLINKSCCC)
ROOTOPTLIBDIR=		$(VROOT)/opt/$(PKGNAME)/lib
ROOTOPTLIBDIR64=	$(VROOT)/opt/$(PKGNAME)/lib/$(MACH64)
ROOTOPTLIBS=		$(LIBS:%=$(ROOTOPTLIBDIR)/%)
ROOTOPTLIBS64=		$(LIBS:%=$(ROOTOPTLIBDIR64)/%)
ROOTOPTLINKS=		$(ROOTOPTLIBDIR)/$(LIBLINKS)
ROOTOPTLINKS64=		$(ROOTOPTLIBDIR64)/$(LIBLINKS)

LINTLIB=	llib-l$(LIBNAME).ln
LINTFLAGS=	$(LINTDEFS) -I$(CCC_INCLUDE) -D_REENTRANT
LINTFLAGS64=	$(LINTDEFS64) -I$(CCC_INCLUDE) -D_REENTRANT
LINTOUT=	lint.out
SPROLINTLIB=    llib-l$(LIBNAME).ln
SPROLINTFLAGS=  -uax
SPROLINTFLAGS64=        -uax
SPROLINTOUT=    lint.out
$(SPROLINTLIB):=        LOG = -DLOGGING
ARFLAGS=	r
ARFLAGS = $(XAR)
SONAME=		$(DYNLIB)
# For most libraries, we should be able to resolve all symbols at link time,
# either within the library or as dependencies, all text should be pure, and
# combining relocations into one relocation table reduces startup costs.
# All options are tunable to allow overload/omission from lower makefiles.
ZDEFS=          -z defs
ZTEXT=          -z text
ZCOMBRELOC=	-z combreloc
HSONAME=	-h $(SONAME)
PMAP=		-M $(SRC)/lib/common/mapfile_pga_$(MACH)
DYNFLAGS=	$(HSONAME) $(ZTEXT) $(ZDEFS) $(ZCOMBRELOC) $(PMAP)

LDLIBS=		$(LDLIBS.lib)

OBJS=		$(OBJECTS:%=objs/%)
PICS=		$(OBJECTS:%=pics/%)

# Declare that all library .o's can all be made in parallel.
# The DUMMY target is for those instances where OBJS, PROFS and PICS
# are empty (to avoid an unconditional .PARALLEL declaration).
.PARALLEL:	$(OBJS) $(PICS) $(LINTFILES) DUMMY

# default value for "portable" source
SRCS=		$(OBJECTS:.o=.c)

# default build of an archive and a shared object,
# overridden locally when extra processing is needed
BUILD.AR=       $(CCC) $(ARFLAGS) -o $@ $(AROBJS)
BUILD.SO= $(CC) -o $@ -G $(STRIPFLAG) $(DYNFLAGS) $(PICS) $(LDLIBS)
BUILDCCC.SO= $(CCC) -o $@ -G $(STRIPFLAG) $(DYNFLAGS) $(PICS) $(LDLIBS) $(LIBCC)

# default dynamic library symlink
INS.liblink=	-$(RM) $@; $(SYMLINK) $(LIBLINKPATH)$(LIBLINKS)$(VERS) $@
INS.liblinkccc=	-$(RM) $@; $(SYMLINK) $(LIBLINKPATH)$(LIBLINKSCCC)$(VERS) $@
INS.libclustlink=	-$(RM) $@; \
	$(SYMLINK) $(LIBCLUSTLINKPATH)$(LIBLINKS)$(VERS) $@
INS.libclustsclink=	-$(RM) $@; \
	$(SYMLINK) $(LIBCLUSTLINKPATH)$(LIBLINKS)$(VERS) $@
INS.libclustlinkccc=	-$(RM) $@; \
	$(SYMLINK) $(LIBCLUSTLINKPATH)$(LIBLINKSCCC)$(VERS) $@
INS.librgmlink=	-$(RM) $@; \
	$(SYMLINK) $(LIBRGMLINKPATH)$(LIBLINKS)$(VERS) $@
INS.librgmlinkccc=	-$(RM) $@; \
	$(SYMLINK) $(LIBRGMLINKPATH)$(LIBLINKSCCC)$(VERS) $@

# default 64-bit dynamic library symlink
INS.liblink64=	-$(RM) $@; $(SYMLINK) $(LIBLINKPATH)$(LIBLINKS)$(VERS) $@
INS.liblinkccc64=	-$(RM) $@; $(SYMLINK) $(LIBLINKPATH)$(LIBLINKSCCC)$(VERS) $@
INS.libclustlink64=	-$(RM) $@; \
	$(SYMLINK) $(LIBCLUSTLINKPATH)$(LIBLINKS)$(VERS) $@
INS.libclustsclink64=	-$(RM) $@; \
	$(SYMLINK) $(LIBCLUSTLINKPATH)$(LIBLINKS)$(VERS) $@
INS.libclustlinkccc64=	-$(RM) $@; \
	$(SYMLINK) $(LIBCLUSTLINKPATH)$(LIBLINKSCCC)$(VERS) $@
INS.librgmlink64=	-$(RM) $@; \
	$(SYMLINK) $(LIBRGMLINKPATH)$(LIBLINKS)$(VERS) $@
INS.librgmlinkccc64=	-$(RM) $@; \
	$(SYMLINK) $(LIBRGMLINKPATH)$(LIBLINKSCCC)$(VERS) $@
INS.liboptlink=    -$(RM) $@; \
	$(SYMLINK) $(LIBOPTLINKPATH)$(LIBLINKS)$(VERS) $@
INS.liboptlink64=  -$(RM) $@; \
	$(SYMLINK) $(LIBOPTLINKPATH)$(LIBLINKS)$(VERS) $@

# Handle different PIC models on different ISAs
# (May be overridden by lower-level Makefiles)

sparc_C_PICFLAGS =		-Kpic
sparcv9_C_PICFLAGS =		-Kpic
i386_C_PICFLAGS =		-Kpic
amd64_C_PICFLAGS =		-Kpic
C_PICFLAGS = $($(MACH)_C_PICFLAGS)

# should be -K pic but the CC driver is ?currently? broken
sparc_CC_PICFLAGS =		-Kpic
sparcv9_CC_PICFLAGS =		-KPIC
i386_CC_PICFLAGS = 		-Kpic
amd64_CC_PICFLAGS = 		-KPIC
CC_PICFLAGS = $($(MACH)_CC_PICFLAGS)

# conditional assignments

$(OBJS)  :=	sparc_CFLAGS += -xregs=no%appl

$(PICS)  :=	sparc_CFLAGS += -xregs=no%appl $(sparc_C_PICFLAGS)
$(PICS)  :=	i386_CFLAGS += $(i386_C_PICFLAGS)
$(PICS)  :=	amd64_CFLAGS += $(amd64_C_PICFLAGS)
$(PICS)  :=	sparcv9_CFLAGS += -xregs=no%appl $(sparcv9_C_PICFLAGS)
# should be -K pic but the CC driver is currently broken
$(PICS)  :=	CPPFLAGS += -DPIC -D_TS_ERRNO
$(PICS)  :=	sparcv9_CCFLAGS += -xregs=no%appl $(CC_PICFLAGS)
$(PICS)  :=	sparc_CCFLAGS += $(CC_PICFLAGS)
$(PICS)  :=	i386_CCFLAGS += $(CC_PICFLAGS)
$(PICS)  :=	amd64_CCFLAGS += $(CC_PICFLAGS)

$(LINTLIB):=	LOG = -DLOGGING
$(LIBRARY):=	AROBJS = $(OBJS)
$(LIBRARYCCC):=	AROBJS = $(OBJS)
$(LIBRARY):=	DIR = objs
$(DYNLIB):=	DIR = pics
$(DYNLIBCCC):=	DIR = pics

SONAMECCC=	$(DYNLIBCCC)
HSONAMECCC=	-h $(SONAMECCC)
$(DYNLIBCCC):=	DYNFLAGS = $(HSONAMECCC) $(ZTEXT) $(ZDEFS) \
    $(ZCOMBRELOC) $(NORUNPATH) $(NOLIB)

# build rule for "portable" source
objs/%.o pics/%.o: %.c
	$(COMPILE.c) -o $@ $<
	$(POST_PROCESS_O)

objs/%.o:= TMPLDIR=objs
pics/%.o:= TMPLDIR=pics

objs/%.o pics/%.o: %.cc
	$(COMPILE.cc) -o $@ $<
	$(POST_PROCESS_O)

objs/%.o pics/%.o: %.c
	$(COMPILE.c) -o $@ $<
	$(POST_PROCESS_O)

.PRECIOUS: $(LIBS)

# Define the majority text domain in this directory.
TEXT_DOMAIN= SUNW_SC_LIB

POFILE=		$(LIBRARY:%.a=%.po)

CLOBBERFILES += $(LINTFILES) $(CHECK_FILES)
CHECK_FILES = $(OBJECTS:%.o=%.c_check) $(CHECKHDRS:%.h=%.h_check)
