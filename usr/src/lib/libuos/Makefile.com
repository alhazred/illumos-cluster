#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.21	08/05/20 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# lib/libuos/Makefile.com
#

LIBRARYCCC= libclos.a
VERS=.1

include $(SRC)/common/cl/Makefile.files

OBJECTS += $(LIBUOS_UOBJS)

# include library definitions, do not change order of include and DYNLIB
include ../../Makefile.lib

CC_PICFLAGS = -KPIC

CLOBBERFILES +=	$(RPCFILES)

MAPFILE=	../common/mapfile-vers
SRCS =		$(OBJECTS:%.o=../common/%.c)

CPPFLAGS += $(CL_CPPFLAGS)
CPPFLAGS += -D_REENTRANT -I$(SRC)/common/cl -I$(SRC)/common/cl/interfaces/$(CLASS)

#
# Overrides
#
CPPFLAGS += -DBUG_4517304

LIBS = $(DYNLIBCCC) $(LIBRARYCCC)

#
# WARNING: Do not include any libraries that cause libthread to be included
#          as one of the consumers of this library cannot be linked
#          with libthread.
#
#          New dependencies here should be marked in cmd/scrconf/Makefile 
#          also to build the static command.
LDLIBS += -lsocket -lnsl -lposix4 -lc -ldl -lsecdb -ldoor -lgen

CLEANFILES =
LINTFILES += $(OBJECTS:%.o=%.ln)

PMAP =

TMPLDIR = pics

.KEEP_STATE:

.NO_PARALLEL:

def all: $(LIBS)

$(DYNLIB): 	$(MAPFILE)

# include library targets
include ../../Makefile.targ
include $(SRC)/common/cl/Makefile.rules

#objs/%.o pics/%.o: ../common/%.c
#	$(COMPILE.c) -o $@ $<
