#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.6	08/05/20 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# lib/libvm/Makefile.com
#
include $(SRC)/Makefile.master

LIBRARYCCC= libvm.a
VERS= .1

include $(SRC)/common/cl/Makefile.files

OBJECTS = $(LIBVM_UOBJS)

include ../../Makefile.lib

CLOBBERFILES +=	$(RPCFILES)

CC_PICFLAGS = -KPIC

SRCS =		$(OBJECTS:%.o=../common/%.c)

MTFLAG	= -mt
CPPFLAGS += $(CL_CPPFLAGS) -DLIBVM
CPPFLAGS += -I$(SRC)/common/cl -I$(SRC)/common/cl/interfaces/$(CLASS)

#
# Overrides
#
LIBS = $(LIBRARYCCC)

LDLIBS += -lclos -lc -lclcomm

CLEANFILES =
LINTFILES += $(OBJECTS:%.o=%.ln)

PMAP =

OBJS_DIR = objs

.KEEP_STATE:

.NO_PARALLEL:

all: $(LIBS)

# include library targets
include ../../Makefile.targ
include $(SRC)/common/cl/Makefile.rules

objs/%.o pics/%.o: ../common/%.cc
	$(COMPILE.cc) -o $@ $<

objs/%.o pics/%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<
