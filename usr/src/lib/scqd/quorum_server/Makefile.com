#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)Makefile.com	1.7	08/05/20 SMI"
#
# lib/scqd/quorum_server/Makefile.com
#

LIBRARY= scconf_quorum_server.a
VERS=.1

OBJECTS= \
	scqd_quorum_server.o \
	scqd_util.o

# include library definitions
include ../../../Makefile.lib

# override value of ROOTCLUSTLIBDIR set in Makefile.lib
ROOTCLUSTLIBDIR=	$(VROOT)/usr/cluster/lib/qd

MAPFILE=	../common/mapfile-vers
SRCS=		../common/scqd_quorum_server.c ../common/scqd_util.cc
LIBS =		$(DYNLIB)

LINTFLAGS +=	-I..
LINTFILES +=	$(OBJECTS:%.o=%.ln) $(CCOBJS:%.o=%.ln)

MTFLAG	=	-mt
CPPFLAGS +=	-I.. -I$(SRC)/lib/libdid/include
CPPFLAGS +=	-I$(SRC)/lib/libscconf
CPPFLAGS +=	-I$(SRC)/lib/libclcomm/common
CPPFLAGS +=	-I$(SRC)/head/scadmin/rpc
CPPFLAGS +=	-I$(SRC)/common/cl
CPPFLAGS +=	-I$(SRC)/common/cl/interfaces/$(CLASS)
PMAP =	-M $(MAPFILE)
LDLIBS +=	-lclconf -lc -lscconf -lclcomm -lclos -lsocket -lnsl
LDLIBS +=	$(REF_USRLIB:%=%/libCrun.so.1)

.KEEP_STATE:

$(DYNLIB):	$(MAPFILE)

# include library targets
include ../../../Makefile.targ

objs/%.o pics/%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<

objs/%.o pics/%.o: ../common/%.cc
	$(COMPILE.cc) -o $@ $<

