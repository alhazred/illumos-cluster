//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//


//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)scqd_util.cc	1.13	08/05/20 SMI"

#include <sys/socket.h>
#include <netinet/tcp.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <libintl.h>

#include <quorum/quorum_server/qd_qs_msg.h>

#define	LIB_SCQD_STRING "lib_scqd"
#define	CONNECT_INITIAL_TIME_OUT	10 /* seconds */
#define	MSECS_PER_SEC 1000

extern "C" {

/*
 * Helper for get_scqsd_host_name.
 *
 * This is a wrap-around function for inet_ntop(). In case the af is AF_INET6
 * and the address pointed by src is a IPv4-mapped IPv6 address, it
 * returns printable IPv4 address, not IPv4-mapped IPv6 address. In other cases
 * it behaves just like inet_ntop().
 */
const char *
inet_ntop_native(int af, const void *src, char *dst, size_t size)
{
	struct in_addr src4;
	const char *result;

	if (af == AF_INET6) {
		if (IN6_IS_ADDR_V4MAPPED((struct in6_addr *)src)) {
			IN6_V4MAPPED_TO_INADDR((struct in6_addr *)src, &src4);
			result = inet_ntop(AF_INET, &src4, dst, size);
		} else {
			result = inet_ntop(AF_INET6, src, dst, size);
		}
	} else {
		result = inet_ntop(af, src, dst, size);
	}

	return (result);
}

//
// This function gets a Quorum Server address and port and
// returns the name on which of its host. We use this hostname
// to make sure that user does not attempt to configure more
// than one quorum server type of device on the same physical Quorum
// Server host.
//
int
get_scqsd_host_name(char *host, uint16_t port, char *buffer)
{
	static struct   sockaddr_in6    remove_addr;
	bzero((char *)&remove_addr, sizeof (struct sockaddr_in6));
	register struct hostent *hp = 0;
	int error_num;
	int socket_fd;
	static char host_name_buf[80];
	struct in6_addr ipv6_addr;

	bzero((char *)&remove_addr, sizeof (struct sockaddr_in6));
	hp = getipnodebyname(host, AF_INET6, AI_ALL | AI_ADDRCONFIG |
	    AI_V4MAPPED, &error_num);
	if (hp == NULL) {
		return (1);
	}

	bcopy(hp->h_addr_list[0], (caddr_t)&remove_addr.sin6_addr,
	    (unsigned int)hp->h_length);
	/*
	 * If hp->h_name is a IPv4-mapped IPv6 literal, we'll convert it to
	 * IPv4 literal address.
	 */
	if ((inet_pton(AF_INET6, hp->h_name, &ipv6_addr) > 0) &&
	    IN6_IS_ADDR_V4MAPPED(&ipv6_addr)) {
		host_name_buf[0] = '\0';
		(void) inet_ntop_native(AF_INET6, &ipv6_addr, host_name_buf,
		    sizeof (host_name_buf));
		/*
		 * It can even be the case that the "host" supplied by the user
		 * can be a IPv4-mapped IPv6 literal. So, let's fix that too.
		 */
		if ((inet_pton(AF_INET6, host, &ipv6_addr) > 0) &&
		    IN6_IS_ADDR_V4MAPPED(&ipv6_addr) &&
		    strlen(host_name_buf) <= strlen(host)) {
			(void) strlcpy(host, host_name_buf, strlen(host) + 1);
		}
	} else {
		(void) strlcpy(host_name_buf, hp->h_name,
		    sizeof (host_name_buf));
	}
	remove_addr.sin6_family = (sa_family_t)hp->h_addrtype;
	if ((socket_fd = socket(AF_INET6, SOCK_STREAM, 0)) < 0) {
		freehostent(hp);
		return (1);
	}
	remove_addr.sin6_port = htons(port);

	//
	// Set the timeout to give up connection establishment to a
	// reasonable value.
	//
	int cabort_timeout = CONNECT_INITIAL_TIME_OUT * MSECS_PER_SEC;
	if (setsockopt(socket_fd, IPPROTO_TCP, TCP_CONN_ABORT_THRESHOLD,
	    (void *)&cabort_timeout, (t_uscalar_t)sizeof (cabort_timeout))
	    != 0) {
		freehostent(hp);
		return (1);
	}

	if (connect(socket_fd, (struct sockaddr *)&remove_addr,
		sizeof (remove_addr)) < 0) {
		freehostent(hp);
		return (1);
	}

	freehostent(hp);
	uint_t recv_size;
	char *status_m = NULL;

	qd_qs_msghdr * status_msg =
		new qd_qs_msghdr(LIB_SCQD_STRING, 0, 0, QD_HOST);

	if (status_msg == NULL) {
		return (1);
	}

	if (send(socket_fd, status_msg, sizeof (qd_qs_msghdr), 0) == -1) {
		delete status_msg;
		return (1);
	}

	qd_qs_msghdr result_status;
	result_status.set_error(QD_QS_INVALID);

	if (recv(socket_fd, &result_status, sizeof (qd_qs_msghdr), 0) < 0) {
		delete status_msg;
		return (1);
	}

	//
	// Perform validity checks on received message.
	//
	if ((result_status.get_error() != QD_QS_SUCCESS) ||
	    (result_status.get_nid() != 0) ||
	    (result_status.get_cluster_id() != 0) ||
	    (strcmp(result_status.cluster_name, LIB_SCQD_STRING)) ||
	    (result_status.get_incn() != 0) ||
	    (result_status.get_msg_num() != 0) ||
	    (result_status.get_version_major() !=
		(uint16_t)SCQSD_VERSION_MAJOR) ||
	    (result_status.get_version_minor() !=
		(uint16_t)SCQSD_VERSION_MINOR) ||
	    (result_status.get_msg_type() != QD_HOST) ||
	    (!result_status.is_cookie_set())) {
		delete status_msg;
		return (1);
	}

	/* Obtain the length of data received */
	recv_size = result_status.get_length();
	if (recv_size > 0) {

		/* Allocate space */
		status_m = (char *)malloc(sizeof (char) * recv_size);
		if (status_m == NULL) {
			delete status_msg;
			return (1);
		}

		/* Read in the remaining msg here */
		if (recv(socket_fd, status_m, recv_size, 0) != (int)recv_size) {
			delete status_msg;
			free(status_m);
			return (1);
		}
		(void) strncpy(buffer, status_m, MAXHOSTNAMELEN+1);
	}

	delete status_msg;
	if (status_m != NULL) {
		free(status_m);
	}
	return (0);
}
}
