/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scqd_quorum_server.c	1.28	08/11/28 SMI"

/*
 * scqd_quorum_server.so - Quorum Server quorum config functions
 */
#include <sys/socket.h>
#include <netdb.h>
#include <assert.h>

#include "../../scqd_common.h"

#define	SCQD_SCCONF_QD_TYPE	"quorum_server"
#define	PROP_QDEV_IPADDR	"qshost"
#define	PROP_QDEV_PORT		"port"
#define	PROP_QDEV_SECKEY	"secure"
#define	PROP_QDEV_TIMEOUT	"timeout"
#define	PROP_QDEV_HOSTNAME	"hostname"

#define	DEFAULT_TIMEOUT_MIN	1
#define	DEFAULT_TIMEOUT_MAX	10000

#define	PROP_PRESENT		1
#define	PROP_NOTPRESENT		0

#define	PROP_VALID		1
#define	PROP_INVALID		0

#define	PING		"/usr/sbin/ping"
#define	PING_TIMEOUT	10
#define	GREP		"/usr/xpg4/bin/grep "
#define	AWK		"/bin/awk"

static int		is_valid_quorum_server_prop(const char *xProp);
static boolean_t	is_quorum_server_usable(void);
static boolean_t 	is_updateable_prop(const char *prop, const char *value);
static boolean_t 	is_ok_ipaddress(char *ipaddrp, char **messages);
static boolean_t	ping(char *ipaddrp);

static int		run_cmd(const char *cmd, char *output);
extern int get_scqsd_host_name(char *server, uint16_t port, char *buffer);

/*
 * scqd_get_qd_type
 *
 * This function confirms if the quorum device type is Quorum Server. It
 * is to make sure that the corresponding quorum device type .so is
 * being opened.
 *
 * Return values:
 *
 *	boolean_t
 */
boolean_t
scqd_get_qd_type(char *scconf_type, char *qd_type)
{
	/* Check to enaure the device type is Quorum Server */
	if (((qd_type) && (strcmp(qd_type, SCQD_SCCONF_QD_TYPE) == 0)) ||
	    ((scconf_type) && (strcmp(scconf_type, SCQD_SCCONF_QD_TYPE)
	    == 0))) {
		return (B_TRUE);
	} else {
		return (B_FALSE);
	}
}

/*
 * scqd_get_qd_device
 *
 * For the given global quorum device name, check if its type
 * is quorum_server, and then return the CCR name of this
 * device in "qdevname".
 *
 * Return values:
 *	boolean_t
 */
boolean_t
scqd_get_qd_device(char *globalname, char *qdevname)
{
	scconf_errno_t		rstatus = SCCONF_EUNEXPECTED;
	clconf_cluster_t	*clconf = (clconf_cluster_t *)0;
	clconf_obj_t		*qdev;
	const char		*qdev_type;
	boolean_t		found = B_FALSE;

	/* Check the arguments */
	if (globalname == NULL || *globalname == '\0' ||
	    qdevname == NULL) {
		return (B_FALSE);
	}

	/* Get the clconf */
	rstatus = scconf_cltr_openhandle(
	    (scconf_cltr_handle_t *)&clconf);
	if (rstatus != SCCONF_NOERR) {
		goto cleanup;
	}

	/* Find the quorum device */
	rstatus = conf_get_qdevobj(clconf, globalname, &qdev);
	if (rstatus != SCCONF_NOERR) {
		goto cleanup;
	}

	/* Find its type property */
	qdev_type = clconf_obj_get_property(qdev,
	    PROP_QDEV_TYPE);

	/* Find the match */
	if (!qdev_type) {
		found = B_FALSE;
		goto cleanup;
	} else if (strcmp(qdev_type, SCQD_SCCONF_QD_TYPE) == 0) {
		found = B_TRUE;
		(void) strcpy(qdevname, globalname);
		goto cleanup;
	}

cleanup:
	/* Release cluster config */
	if (clconf != (clconf_cluster_t *)0) {
		clconf_obj_release((clconf_obj_t *)clconf);
	}

	return (found);
}

/*
 * scqd_add_quorum
 *
 * All scqd_add_quorum would do is add a device with the appropriate paths and
 * configuration info, and a vote of 0. Then scconf would increase the vote
 * to the appropriate amount.
 *
 * Add the given Quorum server "qdname" as a quorum device, if it is
 * not already configured as such.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_EEXIST		- quorum device is already configured
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_EBADVALUE	- bad CCR data
 *	SCCONF_VP_MISMATCH	- unmatched running version
 *	SCCONF_NO_BINARY	- required binary does not exist
 *	SCCONF_EQD_SCRUB	- scrub failed
 */
scconf_errno_t
scqd_add_quorum(char *qdname, char *nodeslist[], char *properties,
    char **messages)
{
	clconf_cluster_t	*clconf = (clconf_cluster_t *)0;
	clconf_obj_t		*qdev;
	scconf_errno_t		rstatus;
	clconf_errnum_t		clconf_err;
	scconf_cfg_prop_t	*prop = NULL;
	scconf_cfg_prop_t	*qdproperties = (scconf_cfg_prop_t *)0;
	scconf_cfg_node_t	*clusternodes = (scconf_cfg_node_t *)0;
	scconf_cfg_node_t	*node;
	char			*pathstate;
	char			pathprop[BUFSIZ];
	scconf_cfg_qdev_t	*qdevs_p = (scconf_cfg_qdev_t *)0;
	scconf_cfg_qdev_t	**qdev_p;
	boolean_t		found;
	quorum_status_t		*quorum_status = (quorum_status_t *)0;
	uint_t			i;
	char			qshost[128];
	boolean_t		ipaddr_found = B_FALSE;
	char			port_num[10];
	uint16_t 		port_value;
	boolean_t		port_found = B_FALSE;
	char			key_file[BUFSIZ];
	char			errbuf[BUFSIZ];
	char			host_name[MAXHOSTNAMELEN+1];
	char			ccr_host_name[MAXHOSTNAMELEN+1];

	qshost[0] = key_file[0] = '\0';

	/* Must be root */
	if (getuid() != 0) {
		return (SCCONF_EPERM);
	}

	/* Check arguments */
	if (qdname == NULL || nodeslist) {
		return (SCCONF_EINVAL);
	}

	/* Check if quorum algorithm version is valid */
	if (!is_quorum_server_usable()) {
		return (SCCONF_VP_MISMATCH);
	}

	/* Get the property list from the properties string */
	rstatus = scconf_propstr_to_proplist(properties, &qdproperties);
	if (rstatus != SCCONF_NOERR) {
		goto cleanup;
	}

	assert(messages != (char **)NULL);
	/*
	 * Add quorum device with a zero vote count.
	 *
	 * Loop until the commit succeeds, or until a fatal error
	 * is encountered.
	 */
	for (;;) {
		/* Release cluster config */
		if (clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)clconf);
			clconf = (clconf_cluster_t *)0;
		}

		/* Allocate the cluster config */
		rstatus = scconf_cltr_openhandle((scconf_cltr_handle_t *)
			&clconf);
		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}

		/* Make sure that the device does not already exist */
		rstatus = conf_get_qdevobj(clconf, qdname, &qdev);
		switch (rstatus) {
		case SCCONF_ENOEXIST:
			rstatus = SCCONF_NOERR;
			break;

		case SCCONF_NOERR:
			rstatus = SCCONF_EEXIST;
			goto cleanup;

		default:
			goto cleanup;
		} /*lint !e788 */

		/* Release quorum status */
		cluster_release_quorum_status(quorum_status);

		/* Get quorum status */
		if (cluster_get_quorum_status(&quorum_status) != 0) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		/* Create the new device */
		qdev = (clconf_obj_t *)clconf_quorum_device_create();
		if (qdev == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* Add it */
		clconf_err = clconf_cluster_add_quorum_device(clconf,
		    (clconf_quorum_t *)qdev);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
				rstatus = SCCONF_EINVAL;
				goto cleanup;

			case CL_INVALID_ID:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		/* Add the name */
		clconf_obj_set_name(qdev, qdname);

		/* Set the state to enabled */
		rstatus = conf_set_state(qdev, SCCONF_STATE_ENABLED);
		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}

		/* Set the vote count to zero */
		clconf_err = clconf_obj_set_property(qdev,
		    PROP_QDEV_QUORUM_VOTE, "0");
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		/*
		 * Set the path to the global device The global device
		 * property is unused for a Quorum Sever device. It is
		 * still added to the CCR because clconf expects for
		 * each Quorum Device to have a gdevname entry, and if
		 * we remove that, we have to add CCR-reading
		 * functionality into the SCSI device modules.
		 */
		clconf_err = clconf_obj_set_property(qdev, PROP_QDEV_GDEVNAME,
		    qdname);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
				rstatus = SCCONF_EINVAL;
				goto cleanup;

			case CL_INVALID_ID:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		/* Set property "type" */
		clconf_err = clconf_obj_set_property(qdev, PROP_QDEV_TYPE,
		    SCQD_SCCONF_QD_TYPE);
		if (clconf_err != CL_NOERROR) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		/*
		 * Set property "qshost"
		 */
		ipaddr_found = PROP_NOTPRESENT;
		for (prop = qdproperties; prop; prop = prop->scconf_prop_next) {

			/* Skip other properties */
			if (!prop->scconf_prop_key) {
				continue;
			}

			if (is_valid_quorum_server_prop(prop->scconf_prop_key)
			    == PROP_INVALID) {
				rstatus = SCCONF_EBADVALUE;
				if (messages != (char **)0) {
					(void) sprintf(errbuf,
					    dgettext(TEXT_DOMAIN,
					    "Invalid property \"%s\" for "
					    "quorum \"%s\".\n"),
					    prop->scconf_prop_key, qdname);
					scconf_addmessage(errbuf, messages);
				}
				continue;
			}

			if (strcmp(prop->scconf_prop_key, PROP_QDEV_IPADDR)
			    == 0) {
				if (prop->scconf_prop_value) {
					ipaddr_found = PROP_PRESENT;
					(void) strcpy(qshost,
					    prop->scconf_prop_value);
				}
			}
			if (strcmp(prop->scconf_prop_key, PROP_QDEV_SECKEY)
			    == 0) {
				if (prop->scconf_prop_value) {
					(void) strcpy(key_file,
					    prop->scconf_prop_value);

				}
			}
			if (strcmp(prop->scconf_prop_key, PROP_QDEV_PORT)
			    == 0) {
				if (prop->scconf_prop_value) {
					port_found = PROP_PRESENT;
					(void) strcpy(port_num,
					    prop->scconf_prop_value);

				}
			}

		}
		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}

		/* Only qshost and port are necessary properties. */
		if (ipaddr_found == PROP_NOTPRESENT) {
			rstatus = SCCONF_EUSAGE;
			(void) sprintf(errbuf, dgettext(TEXT_DOMAIN,
			    "Hostname must be specified for quorum "
			    "\"%s\".\n"), qdname);
			scconf_addmessage(errbuf, messages);
		}

		if (port_found == PROP_NOTPRESENT) {
			rstatus = SCCONF_EUSAGE;
			(void) sprintf(errbuf, dgettext(TEXT_DOMAIN,
			    "Port number must be specified for quorum "
			    "\"%s\".\n"), qdname);
			scconf_addmessage(errbuf, messages);
		}
		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}

		port_value = (uint16_t)atoi(port_num); /*lint !e645 */

		/*
		 * Validate IP address
		 *
		 * 1. Check that the host is reachable.
		 *    (The check to validate qshost should
		 *    be done outside the loop, but this seems to be the style
		 *    of rest of the code.)
		 * 2. Check that this ip address is not in use by any
		 *    other QS configured in the cluster.
		 *    i.e. there cannot be multiple quorum devices
		 *    all pointing to the same host running the quorum
		 *    server.
		 */
		if (!is_ok_ipaddress(qshost, messages)) {
			rstatus = SCCONF_EBADVALUE;
			(void) sprintf(errbuf, dgettext(TEXT_DOMAIN,
			    "Invalid hostname \"%s\" for quorum \"%s\".\n"),
			    qshost, qdname);
			scconf_addmessage(errbuf, messages);
			goto cleanup;
		}

		if (!ping(qshost)) {
			rstatus = SCCONF_EQD_CONFIG;
			(void) sprintf(errbuf, dgettext(TEXT_DOMAIN,
			    "Could not ping host \"%s\" for quorum \"%s\".\n"),
			    qshost, qdname);
			scconf_addmessage(errbuf, messages);
			goto cleanup;
		}

		/* Get the hostname */
		if (get_scqsd_host_name(qshost, port_value, host_name) != 0) {
			rstatus = SCCONF_EQD_CONFIG;
			(void) sprintf(errbuf, dgettext(TEXT_DOMAIN,
				"Could not contact quorum server "
				"on hostname \"%s\".\n"), qshost);
			scconf_addmessage(errbuf, messages);
			goto cleanup;
		}

		rstatus = conf_get_qdevs(clconf, &qdevs_p);
		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}

		for (qdev_p = &qdevs_p; *qdev_p;
		    qdev_p = &(*qdev_p)->scconf_qdev_next) {

			ccr_host_name[0] = '\0';

			/* Go throught the properties */
			for (prop = (*qdev_p)->scconf_qdev_propertylist;
			    prop; prop = prop->scconf_prop_next) {
				if (prop->scconf_prop_key == NULL) {
					continue;
				}

				if (strcmp(prop->scconf_prop_key,
					PROP_QDEV_HOSTNAME) == 0) {
					(void) strcpy(ccr_host_name,
					    prop->scconf_prop_value);
					continue;
				}
			}
			if (ccr_host_name[0] != '\0') {
				if (strcmp(ccr_host_name, host_name) == 0) {
					rstatus = SCCONF_DUP_QSQD;
					break;
				}
			}
		}
		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}

		/* Set the hostname property */
		clconf_err = clconf_obj_set_property(qdev,
		    PROP_QDEV_HOSTNAME, host_name);
		if (clconf_err != CL_NOERROR) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		/* Set the property "qshost"  */
		clconf_err = clconf_obj_set_property(qdev,
		    PROP_QDEV_IPADDR, qshost);
		if (clconf_err != CL_NOERROR) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		/* Process the properties */
		for (prop = qdproperties; prop; prop = prop->scconf_prop_next) {
			if (!prop->scconf_prop_key ||
			    !prop->scconf_prop_value) {
				continue;
			}

			if (strcmp(prop->scconf_prop_key, PROP_QDEV_TIMEOUT)
			    == 0) {
				if (!is_updateable_prop(
				    prop->scconf_prop_key,
				    prop->scconf_prop_value)) {
					rstatus = SCCONF_EBADVALUE;
					if (messages != (char **)0) {
						(void) sprintf(errbuf,
						    dgettext(TEXT_DOMAIN,
						    "The \"%s\" value \"%s\" "
						    "is out of range for "
						    "quorum \"%s\".\n"),
						    prop->scconf_prop_key,
						    prop->scconf_prop_value,
						    qdname);
						scconf_addmessage(errbuf,
						    messages);
					}
					goto cleanup;
				}
			}

			clconf_err = clconf_obj_set_property(qdev,
			    prop->scconf_prop_key, prop->scconf_prop_value);
			if (clconf_err != CL_NOERROR) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}
		}

		/* Get cluster nodes. No nodeslist for QS */
		rstatus = conf_get_clusternodes(clconf,
		    (scconf_cfg_node_t **)&clusternodes);
		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}

		for (node = clusternodes; node;
			node = node->scconf_node_next) {
			(void) sprintf(pathprop, "path_%d",
				node->scconf_node_nodeid);
			if (node->scconf_node_qvotes > 0) {
				pathstate = "enabled";
			} else {
				if (node->scconf_node_qvotes == 0) {
					pathstate = "disabled";
				} else {
					rstatus = SCCONF_EUNEXPECTED;
					goto cleanup;
				}
			}

			/* Set the "path" property */
			clconf_err = clconf_obj_set_property(qdev,
			    pathprop, pathstate);
			if (clconf_err != CL_NOERROR) {
				switch (clconf_err) {
				case CL_BADNAME:
				default:
					rstatus = SCCONF_EUNEXPECTED;
					goto cleanup;
				} /*lint !e788 */
			}
		}

		/*
		 * The requirement is all cluster nodes must be
		 * up and connected to the QS host. If one node
		 * is not active, the add operation should fail.
		 *
		 * Before committing the addition of the new QD to the
		 * CCR, scrub the quorum device for all active nodes.
		 *
		 * We will do the scrub loop twice. The first time we
		 * will make sure that at least one node succeeds. The
		 * one node may be the only node which was still a
		 * cluster member before this QD was (possibly)
		 * removed previously. The second time we loop, we
		 * will require that the scrub succeed from all
		 * nodes. This will guarantee that the device is
		 * accessible from all nodes of the cluster as
		 * required by CLARC review for FBC 1225.
		 */

		/* Check if all nodes are in cluster mode */
		for (i = 0;  i < quorum_status->num_nodes;  ++i) {
			if (quorum_status->nodelist[i].state
			    != QUORUM_STATE_ONLINE) {
				/* Node is offline */
				found = B_FALSE;
				for (node = clusternodes; node;
				    node = node->scconf_node_next) {
					if (node->scconf_node_nodeid ==
					    quorum_status->nodelist[i].nid) {
						found = B_TRUE;
						break;
					}
				}

				/* Print error */
				if (found) {
					(void) sprintf(errbuf,
					    dgettext(TEXT_DOMAIN,
					    "Node \"%s\" is not "
					    "in cluster mode.\n"),
					    node->scconf_node_nodename);
					scconf_addmessage(errbuf,
					    messages);
				}
				rstatus = SCCONF_EQD_CONFIG;
				goto cleanup;
			}
		}

		/* Scrub each node, in turn, until one node success */
		for (i = 0;  i < quorum_status->num_nodes;  ++i) {
			/* Attempt to scrub the device */
			clconf_err = clconf_quorum_device_scrub(
			    qdname, quorum_status->nodelist[i].nid,
			    SCQD_SCCONF_QD_TYPE, qdev);

			/*
			 * If any node fails, it means that node has a
			 * problem connecting to the Quorum Server.
			 */
			if (clconf_err != CL_NOERROR) {
				rstatus = SCCONF_EQD_SCRUB;
				goto cleanup;
			}

		}

		/* Commit */
		rstatus = conf_cluster_commit(clconf, NULL);

		/* if commit returned okay, we are done */
		if (rstatus == SCCONF_NOERR) {
			break;

		/* else if outdated handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			goto cleanup;
		}
	}
	/* reset the device */
	rstatus = scconf_reset_quorum_device(qdname);
	if (rstatus != SCCONF_NOERR) {
		goto cleanup;
	}
cleanup:
	if (clusternodes) {
		conf_free_clusternodes(clusternodes);
	}
	/* Free the properties list */
	if (qdproperties != (scconf_cfg_prop_t *)0) {
		conf_free_proplist(qdproperties);
	}
	/* Free the quorum device list */
	if (qdevs_p != (scconf_cfg_qdev_t *)0) {
		conf_free_qdevs(qdevs_p);
	}
	/* Release the cluster config */
	if (clconf != (clconf_cluster_t *)0) {
		clconf_obj_release((clconf_obj_t *)clconf);
	}
	cluster_release_quorum_status(quorum_status);

	return (rstatus);
}

/*
 * scqd_remove_quorum
 *
 * Remove a Quorum Server typed quorum device.
 * The entry criteria for this function is that the QD already have a vote of
 * 0. This function is just responsible for the final remove step.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOEXIST		- quorumdev not found
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_ESETUP		- handle data is invalid
 *	SCCONF_EQUORUM		- would result in loss of quorum
 *	SCCONF_EOVERFLOW	- message buffer overflow
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_EBADVALUE	- bad CCR data
 *	SCCONF_EQD_REMOVE	- remove failed
 */
scconf_errno_t
scqd_remove_quorum(char *qdname, uint32_t force_flg, char **messages) {

	scconf_errno_t		rstatus;
	clconf_cluster_t	*clconf = (clconf_cluster_t *)0;
	clconf_obj_t		*qdev, *qdev_copy;
	char pathprop[BUFSIZ];
	int success;
	scconf_nodeid_t		nodeid;
	clconf_errnum_t		clconf_err;
	const  char *pathstate;
	char		buf[BUFSIZ];

	/* Must be root */
	if (getuid() != 0) {
		return (SCCONF_EPERM);

	}

	/* Check arguments */
	if (qdname == NULL) {
		return (SCCONF_EINVAL);
	}

	/* Check if quorum algorithm version is valid */
	if (!is_quorum_server_usable()) {
		return (SCCONF_VP_MISMATCH);
	}

	/* Get the clconf */
	rstatus = scconf_cltr_openhandle((scconf_cltr_handle_t *)
	    &clconf);
	if (rstatus != SCCONF_NOERR) {
		return (rstatus);
	}

	/* Find the quorum device */
	rstatus = conf_get_qdevobj(clconf, qdname, &qdev);
	if (rstatus != SCCONF_NOERR) {
		clconf_obj_release((clconf_obj_t *)clconf);
		return (rstatus);
	}

	/* make a copy of it */
	qdev_copy = clconf_obj_deep_copy(qdev);
	if (qdev_copy == NULL) {
		clconf_obj_release((clconf_obj_t *)clconf);
		return (SCCONF_ENOMEM);
	}

	/* Release the original handle */
	clconf_obj_release((clconf_obj_t *)clconf);

	rstatus = (conf_rm_quorum_device(qdname, force_flg));
	if (rstatus != SCCONF_NOERR) {
		return (rstatus);
	}

	/* Try each node, in turn, until success */
	success = 0;
	for (nodeid = 1; nodeid <= SCCONF_NODEID_MAX; ++nodeid) {

		(void) sprintf(pathprop, "path_%d", nodeid);

		/* Get the "path" property */
		pathstate = clconf_obj_get_property(qdev_copy,
		    pathprop);
		if (pathstate == NULL) {
			continue;
		}

		/*
		 * if (strcmp(pathstate, "enabled")) {
		 * continue;
		 * }
		 */
		/* Attempt to remove the device */
		clconf_err = clconf_quorum_remove(qdname, nodeid,
		    SCQD_SCCONF_QD_TYPE, qdev_copy);

		/*
		 * If any node fails, it means that node has a
		 * problem connecting to the Quorum Server.
		 */
		if (clconf_err != CL_NOERROR) {
		} else {
			success = 1;
			break;
		}
	}

	if (success == 0) {
		/*
		 * Log a message as we could not complete
		 * the remove opertaion on the server side.
		 */
		(void) sprintf(buf, dgettext(TEXT_DOMAIN,
			"Could not contact the quorum server for "
			"\"%s\".\n"), qdname);
		scconf_addmessage(buf, messages);
		scconf_addmessage(dgettext(TEXT_DOMAIN,
			"The remove operation did not "
			"complete on the quorum server side.\n"),
			messages);
		return (SCCONF_NOERR);
	}

	return (SCCONF_NOERR);
}

/*
 * scqd_set_qd_properties
 *
 * Set the property name and value from qdproperties
 * for the Quorum Server quorum device qdname. Property values
 * can only be set for a device that already exists.
 * If the property already exists, its value is overwritten
 * with the value given. If the property does not exist
 * it is created.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOEXIST		- quorumdev not found
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_ESTALE		- cluster handle is stale
 */
scconf_errno_t
scqd_set_qd_properties(char *qdname, char *properties, char **messages)
{
	clconf_cluster_t	*clconf = (clconf_cluster_t *)0;
	clconf_obj_t		*qdev;
	scconf_errno_t		rstatus = SCCONF_EUNEXPECTED;
	clconf_errnum_t		clconf_err;
	scconf_cfg_prop_t	*prop = NULL;
	scconf_cfg_prop_t	*qdproperties = (scconf_cfg_prop_t *)0;
	char errbuf[BUFSIZ];

	/* Must be root */
	if (getuid() != 0) {
		return (SCCONF_EPERM);
	}

	/* Check arguments */
	if ((qdname == NULL) || (properties == NULL)) {
		return (SCCONF_EINVAL);
	}

	/* Get the property list from the properties string */
	rstatus = scconf_propstr_to_proplist(properties, &qdproperties);
	if (rstatus != SCCONF_NOERR) {
		goto cleanup;
	}

	/*
	 * Loop until the commit succeeds, or until a fatal error
	 * is encountered.
	 */

	for (;;) {
		/* Release cluster config */
		if (clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)clconf);
			clconf = (clconf_cluster_t *)0;
		}

		/* Allocate the cluster config */
		rstatus = scconf_cltr_openhandle((scconf_cltr_handle_t *)
				&clconf);
		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}

		/* Make sure that the device does already exist */
		rstatus = conf_get_qdevobj(clconf, qdname, &qdev);
		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}

		/* Validate the property */
		for (prop = qdproperties; prop; prop = prop->scconf_prop_next) {
			/* Skip */
			if (!(prop->scconf_prop_key)) {
				continue;
			}

			/* Valid? */
			if (!is_updateable_prop(prop->scconf_prop_key,
			    prop->scconf_prop_value)) {
				if (messages != (char **)0) {
					(void) sprintf(errbuf,
					    dgettext(TEXT_DOMAIN,
					    "Invalid property \"%s\" for "
					    "quorum \"%s\".\n"),
					    prop->scconf_prop_key, qdname);
					scconf_addmessage(errbuf,
					    messages);
				}
				rstatus = SCCONF_EBADVALUE;
				continue;
			}
		}
		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}

		/* Set the property */
		for (prop = qdproperties; prop; prop = prop->scconf_prop_next) {
			/* Skip */
			if (!(prop->scconf_prop_key)) {
				continue;
			}

			/* Set the property */
			clconf_err = clconf_obj_set_property(qdev,
			    prop->scconf_prop_key, prop->scconf_prop_value);
			if (clconf_err != CL_NOERROR) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}
		}

		/* Commit */
		rstatus = conf_cluster_commit(clconf, NULL);

		/* if commit returned okay, we are done */
		if (rstatus == SCCONF_NOERR) {
			break;

		/* else if outdated handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			goto cleanup;
		}
	}

	/* reset the device */
	rstatus = scconf_reset_quorum_device(qdname);
	if (rstatus != SCCONF_NOERR) {
		goto cleanup;
	}

cleanup:
	/* Free the properties list */
	if (qdproperties != (scconf_cfg_prop_t *)0) {
		conf_free_proplist(qdproperties);
	}

	/* Release the cluster config */
	if (clconf != (clconf_cluster_t *)0) {
		clconf_obj_release((clconf_obj_t *)clconf);
	}

	return (rstatus);
} /*lint !e715 */

/*
 * is_valid_quorum_server_prop
 *
 * Check if the given argument is a valid Quorum Server
 * property.
 *
 * Return:
 *	PROP_INVALID	Unknown property
 *	PROP_VALID	Valid property
 */
int
is_valid_quorum_server_prop(const char *xProp)
{
	if (!xProp) {
		return (PROP_INVALID);
	}
	if (strcmp(xProp, PROP_QDEV_IPADDR) == 0) {
		return (PROP_VALID);
	}
	if (strcmp(xProp, PROP_QDEV_PORT) == 0) {
		return (PROP_VALID);
	}
	if (strcmp(xProp, PROP_QDEV_SECKEY) == 0) {
		return (PROP_VALID);
	}
	if (strcmp(xProp, PROP_QDEV_TIMEOUT) == 0) {
		return (PROP_VALID);
	}
	return (PROP_INVALID);
}

/*
 * is_quorum_server_usable
 *
 * Check if Quorum Server support is available.
 *
 * Return:
 *	B_TRUE		Quorum Server is supported
 *	B_FALSE		Quorum Server isn't available
 */
static boolean_t
is_quorum_server_usable(void)
{
	clcomm_vp_version_t quorum_server_version;

	if (clcomm_get_running_version("quorum",
	    &quorum_server_version) != 0) {
		return (B_FALSE);
	}

	return ((boolean_t)(quorum_server_version.major_num >= 1) &&
	    (quorum_server_version.minor_num >= 2));
}

/*
 * is_updateable_prop
 *
 *	Check if the given property is updateable or not.
 *
 * Return:
 *	B_TRUE		valid updateable property
 *	B_FALSE		property cannot be updated
 */
static boolean_t
is_updateable_prop(const char *prop, const char *value)
{
	int timeout_val;

	if (!prop) {
		return (B_FALSE);
	}

	if (strcmp(prop, PROP_QDEV_TIMEOUT) == 0) {
		/* Check the range */
		if (value) {
			if (sscanf(value, "%d", &timeout_val) != 1) {
				return (B_FALSE);
			}

			if ((timeout_val < DEFAULT_TIMEOUT_MIN) ||
			    (timeout_val > DEFAULT_TIMEOUT_MAX)) {
				return (B_FALSE);
			}
		}

		return (B_TRUE);
	}

	return (B_FALSE);
}

/*
 * is_ok_ipaddress
 *
 * Checks if the given ip address is a valid IPv4 or IPv6 format.
 *
 * Return:
 *	B_TRUE if the ip address is a valid IPv6 or IPv4 format.
 *	B_FALSE if not.
 */
static boolean_t
is_ok_ipaddress(char *ipaddrp, char **messages)
{
	struct hostent	*hostp;
	int 		error;
	boolean_t	ret = B_FALSE;
	char		buf[BUFSIZ];

	/*
	 * Check that the ipaddress is a valid '.' or ':' separated address.
	 * inet_pton() returns 1 if the conversion was successful and
	 * 0 otherwise.
	 */
	if ((inet_pton(AF_INET, ipaddrp, buf) == 0) &&
	    (inet_pton(AF_INET6, ipaddrp, buf) == 0)) {

		(void) sprintf(buf, "%s -w %s /etc/hosts | "
		    "%s -F# \'{print $1}\' | %s -w %s", GREP, ipaddrp,
		    AWK, GREP, ipaddrp);

		if (run_cmd(buf, buf) == 0) {
			ret = B_TRUE;
		}


		(void) sprintf(buf, "%s -w %s /etc/inet/ipnodes | "
		    "%s -F# \'{print $1}\' | %s -w %s", GREP, ipaddrp,
		    AWK, GREP, ipaddrp);
		if (run_cmd(buf, buf) == 0) {
			ret = B_TRUE;
		}

		if (ret == B_FALSE) {
			(void) sprintf(buf, dgettext(TEXT_DOMAIN,
				"IP address for hostname \"%s\" is "
				"not specified in the /etc/hosts file, "
				"the /etc/inet/ipnodes file, or both.\n"),
			    ipaddrp);
			scconf_addmessage(buf, messages);
			return (B_FALSE);
		}
	}
	/* get host entry for ipaddr */
	if ((hostp = getipnodebyname(ipaddrp, AF_INET6,
	    AI_DEFAULT, &error)) == NULL) {
		/* A configured address should resolve to something */
		return (B_FALSE);
	}
	freehostent(hostp);

	return (B_TRUE);
}


/*
 * run_cmd
 *
 * Uses popen() to run the given command and returns the output. The
 * output buffer must be sufficient to accommodate the output from the
 * command.
 *
 * Return:
 *   The value returned by the command.
 */

int
run_cmd(const char *cmd, char *output)
{
	int ret;
	FILE *fp;
	char *p = output;

	fp = popen(cmd, "r");
	while (fgets(p, BUFSIZ, fp) != NULL) {
		p += strlen(output) + 1;
	}

	ret = pclose(fp);
	return (ret);
}


/*
 * ping
 *
 * Checks if the host with the given ip address is on the same
 * subnet. It is if it can be reached in a single hop.
 *
 * Return:
 *	B_TRUE if host is on the same subnet.
 *	B_FALSE if not.
 */
boolean_t
ping(char *ipaddrp)
{
	char		buf[BUFSIZ];

	/* run ping specifying timeout of 10s */
	(void) sprintf(buf, "%s %s %d > /dev/null 2>&1", PING,
	    ipaddrp, PING_TIMEOUT);

	if (run_cmd(buf, buf) != 0) {
		return (B_FALSE);
	}
	return (B_TRUE);
}
