/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SCQD_COMMON_H
#define	_SCQD_COMMON_H

#pragma ident	"@(#)scqd_common.h	1.9	08/05/20 SMI"

/*
 * This is the header file for device type specific quorum
 * functions. Currently the following functions that contains
 * device type specific tasks are defined here, and implemented
 * in the device type quorum library:
 *
 *	scqd_add_quorum		Add a quorum device
 *	scqd_remove_quorum	Remove a quorum device
 *	scqd_set_qd_properties	Change quorum properties
 *
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include <sys/stat.h>
#include <sys/param.h>
#include <sys/types.h>

#include <scadmin/scconf.h>
#include <sys/dditypes.h>
#include <sys/didio.h>
#include <scconf_private.h>
#include <clcomm_cluster_vm.h>

/*
 * scqd_get_qd_type
 *
 * This function confirms if the quorum device type is "scsi". It is
 * to make sure the right quorum device type .so is being opened.
 *
 * Return values:
 *
 *	boolean_t
 */
extern boolean_t scqd_get_qd_type(char *, char *);

/*
 * scqd_get_qd_device
 *
 * For the given global quorum device name, construct the full path
 * name of the rdsk device for the corresponding DID device in didname
 * and verify this device exist in CCR. If it exists, return a copy
 * of name used in CCR table for the quorum device name.
 *
 * Return values:
 *
 *	boolean_t
 */
extern boolean_t scqd_get_qd_device(char *, char *);

/*
 * scqd_add_quorum
 *
 * Add the given "devicename" as a quorum device, if it is
 * not already configured as such.  Then, for each node in the
 * the "nodeslist", add a port from the node to the device.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_EEXIST		- quorum device is already configured
 *	SCCONF_ENOEXIST		- globaldev not found or not ported to node(s)
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_EQUORUM		- would result in loss of quorum
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_EBADVALUE	- bad CCR data
 */
extern scconf_errno_t scqd_add_quorum(char *devicename,
    char *nodeslist[], char *properties, char **messages);

/*
 * scqd_remove_quorum
 *
 * Remove a quorum device.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOEXIST		- quorumdev not found
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_EQUORUM		- would result in loss of quorum
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_EBADVALUE	- bad CCR data
 */
extern scconf_errno_t scqd_remove_quorum(char *quorumdevice,
	uint32_t force_flg, char **messages);

/*
 * scqd_set_qd_properties
 *
 * Set the specified properties for the given quorum device.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOEXIST		- quorum does not exist
 *	SCCONF_EUNKNOWN		- unkown quorum device type
 *	SCCONF_EPERM		- not root
 */
extern scconf_errno_t scqd_set_qd_properties(char *devicename,
    char *properties, char **messages);

#ifdef __cplusplus
}
#endif

#endif /* _SCQD_COMMON_H */
