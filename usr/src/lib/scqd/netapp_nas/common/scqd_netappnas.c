/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scqd_netappnas.c	1.17	08/11/28 SMI"

/*
 * scqd_netappnas.so NetApp NAS quorum config functions
 */

#include "../../scqd_common.h"
#include <sys/cladm_int.h>
#include <clcomm_cluster_vm.h>
#include <sys/socket.h>
#include <netdb.h>

#define	SCQD_SCCONF_QD_TYPE	"netapp_nas"
#define	NETAPP_BINARY		"/usr/sbin/NTAPquorum"
#define	NETAPP_INITIATOR	"iqn.1986-05.com.sun:cluster-iscsi-quorum"

#define	PROP_QDEV_FILER_NAME	"filer"
#define	PROP_QDEV_LUNID	"lun_id"
#define	PROP_QDEV_LUN_NAME	"lun"
#define	PROP_QDEV_TIMEOUT	"timeout"
#define	PROP_QDEV_HOSTNAME	"filer_hostname"

#define	DEFAULT_LUNID		"0"
#define	DEFAULT_TIMEOUT_MIN	1
#define	DEFAULT_TIMEOUT_MAX	10000

#define	PROP_PRESENT		1
#define	PROP_NOTPRESENT		0

#define	PROP_VALID		1
#define	PROP_INVALID		0


static int is_valid_nas_prop(const char *xProp);
static boolean_t is_netapp_nas_usable(void);
static boolean_t is_updateable_prop(const char *prop, const char *value);

/*
 * scqd_get_qd_type
 *
 * This function confirms if the quorum device type is NetApp
 * NAS. It is to make sure the right quorum device type .so
 * is being opened.
 *
 * Return values:
 *
 *	boolean_t
 */
boolean_t
scqd_get_qd_type(char *scconf_type, char *qd_type)
{
	/* Check to enaure the device type is NetApp NAS */
	if (((qd_type) && (strcmp(qd_type, SCQD_SCCONF_QD_TYPE) == 0)) ||
	    ((scconf_type) && (strcmp(scconf_type, SCQD_SCCONF_QD_TYPE)
	    == 0)))
		return (B_TRUE);
	else
		return (B_FALSE);
}

/*
 * scqd_get_qd_device
 *
 * For the given global quorum device name, check if its type
 * is NetApp NAS, and then return the CCR name of this
 * quorum in "qdevname". For NetApp NAS, there is only one
 * naming format for the quorum. So "globalname" and
 * "qdevname" should be same. It is to make sure the right
 * quorum device type .so is being opened.
 *
 * Return values:
 *	boolean_t
 */
boolean_t
scqd_get_qd_device(char *globalname, char *qdevname)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	clconf_obj_t *qdev;
	const char *qdev_type;
	boolean_t found = B_FALSE;

	/* Check the arguments */
	if (globalname == NULL || *globalname == '\0' ||
	    qdevname == NULL)
		return (B_FALSE);

	/* Get the clconf */
	rstatus = scconf_cltr_openhandle(
	    (scconf_cltr_handle_t *)&clconf);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/* Find the quorum device */
	rstatus = conf_get_qdevobj(clconf, globalname, &qdev);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/* Find its type property */
	qdev_type = clconf_obj_get_property(qdev,
	    PROP_QDEV_TYPE);

	/* Find the match */
	if (!qdev_type) {
		found = B_FALSE;
		goto cleanup;
	} else if (strcmp(qdev_type, SCQD_SCCONF_QD_TYPE) == 0) {
		found = B_TRUE;
		(void) strcpy(qdevname, globalname);
		goto cleanup;
	}

cleanup:
	/* Release cluster config */
	if (clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)clconf);

	return (found);
}


/*
 * scqd_add_quorum
 *
 * All scqd_add_quorum would do is add a device with the appropriate paths and
 * configuration info, and a vote of 0. Then scconf would increase the vote
 * to the appropriate amount.
 *
 * Add the given NAS device "qdname" as a quorum device, if it is
 * not already configured as such.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_EEXIST		- quorum device is already configured
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_EBADVALUE	- bad CCR data
 *	SCCONF_VP_MISMATCH	- unmatched running version
 *	SCCONF_NO_BINARY	- required binary does not exist
 */
scconf_errno_t
scqd_add_quorum(char *qdname, char *nodeslist[],
    char *properties, char **messages)
{
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	clconf_obj_t *qdev;
	scconf_errno_t rstatus;
	clconf_errnum_t clconf_err;
	scconf_cfg_prop_t *prop = NULL;
	scconf_cfg_prop_t *qdproperties = (scconf_cfg_prop_t *)0;
	int bFilerName;
	const char *clustername;
	scconf_cfg_node_t *clusternodes = (scconf_cfg_node_t *)0;
	scconf_cfg_node_t *node;
	char *pathstate;
	char pathprop[BUFSIZ];
	scconf_nodeid_t nodeid;
	char filer_name[BUFSIZ];
	scconf_cfg_qdev_t *qdevs_p = (scconf_cfg_qdev_t *)0;
	scconf_cfg_qdev_t **qdev_p;
	struct stat stat_buf;
	boolean_t found;
	quorum_status_t *quorum_status = (quorum_status_t *)0;
	uint_t i;
	FILE *file_ptr;
	char output_buf[BUFSIZ];
	char cmd_line[BUFSIZ];
	char errbuf[BUFSIZ];
	char *nodenamep;
	char *lun_id = NULL;
	struct hostent *hostentp = (struct hostent *)0;
	int error_num = 0;
	in_addr_t addr;
	in6_addr_t addr6;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (qdname == NULL || nodeslist)
		return (SCCONF_EINVAL);

	*filer_name = '\0';

	/* Check if NAS version is valid */
	if (!is_netapp_nas_usable()) {
		return (SCCONF_VP_MISMATCH);
	}

	/* Check if the Netapp binary exists */
	if ((stat(NETAPP_BINARY, &stat_buf) != 0) ||
	    (access(NETAPP_BINARY, X_OK) != 0)) {
		return (SCCONF_NO_BINARY);
	}

	/*
	 * We are root, so need to check execute permissions
	 * because access(2) will return success for X_OK even if
	 * none of the execute file permission bits are set.
	 */
	if ((stat_buf.st_mode & (S_IXUSR|S_IXGRP|S_IXOTH)) == 0) {
		return (SCCONF_NO_BINARY);
	}

	/* Get the property list from the properties string */
	rstatus = scconf_propstr_to_proplist(properties, &qdproperties);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/*
	 * Add quorum device with a zero vote count.
	 *
	 * Loop until the commit succeeds, or until a fatal error
	 * is encountered.
	 */
	for (;;) {
		/* Release cluster config */
		if (clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)clconf);
			clconf = (clconf_cluster_t *)0;
		}

		/* Allocate the cluster config */
		rstatus = scconf_cltr_openhandle((scconf_cltr_handle_t *)
			&clconf);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Make sure that the device does not already exist */
		rstatus = conf_get_qdevobj(clconf, qdname, &qdev);
		switch (rstatus) {
		case SCCONF_ENOEXIST:
			rstatus = SCCONF_NOERR;
			break;

		case SCCONF_NOERR:
			rstatus = SCCONF_EEXIST;
			goto cleanup;

		default:
			goto cleanup;
		} /*lint !e788 */

		/* Release quorum statu */
		cluster_release_quorum_status(quorum_status);

		/* Get quorum status */
		if (cluster_get_quorum_status(&quorum_status) != 0) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		/* Create the new device */
		qdev = (clconf_obj_t *)clconf_quorum_device_create();
		if (qdev == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* Add it */
		clconf_err = clconf_cluster_add_quorum_device(clconf,
		    (clconf_quorum_t *)qdev);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
				rstatus = SCCONF_EINVAL;
				goto cleanup;

			case CL_INVALID_ID:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		/* Add the name */
		clconf_obj_set_name(qdev, qdname);

		/* Set the state to enabled */
		rstatus = conf_set_state(qdev, SCCONF_STATE_ENABLED);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Set the vote count to zero */
		clconf_err = clconf_obj_set_property(qdev,
		PROP_QDEV_QUORUM_VOTE, "0");
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		/*
		 * Set the path to the global device
		 * The global device property is unused for a NAS device
		 * we still add this to the CCR because clconf expects for
		 * each Quorum Device to have a gdevname entry, and if we remove
		 * that, we have to add CCR-reading functionality into the SCSI
		 * device modules.
		 */
		clconf_err = clconf_obj_set_property(qdev, PROP_QDEV_GDEVNAME,
		    qdname);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
				rstatus = SCCONF_EINVAL;
				goto cleanup;

			case CL_INVALID_ID:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		/* Set property "type" */
		clconf_err = clconf_obj_set_property(qdev, PROP_QDEV_TYPE,
		    SCQD_SCCONF_QD_TYPE);
		if (clconf_err != CL_NOERROR) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		/*
		 * Set property "filer"
		 */

		bFilerName = PROP_NOTPRESENT;

		for (prop = qdproperties; prop; prop = prop->scconf_prop_next) {

			/* Skip other properties */
			if (!prop->scconf_prop_key)
				continue;

			/* Check if the property is valid */
			if (is_valid_nas_prop(prop->scconf_prop_key)
			    == PROP_INVALID) {
				rstatus = SCCONF_EBADVALUE;
				if (messages != (char **)0) {
					(void) sprintf(errbuf,
					    dgettext(TEXT_DOMAIN,
					    "Invalid property \"%s\" for "
					    "quorum \"%s\".\n"),
					    prop->scconf_prop_key, qdname);
					scconf_addmessage(errbuf, messages);
				}
				continue;
			}

			if (strcmp(prop->scconf_prop_key, PROP_QDEV_FILER_NAME)
			    != 0)
				continue;

			if (prop->scconf_prop_value) {
				bFilerName = PROP_PRESENT;
				(void) strcpy(filer_name,
				    prop->scconf_prop_value);
			}
		}
		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}

		/*
		 * The FBC 1225 decided that the filername will default
		 * to quorum devicename, when it is not specified
		 * during addition.
		 */
		if (bFilerName == PROP_NOTPRESENT) {
			(void) strcpy(filer_name, qdname);
		}

		/*
		 * FBC 1225 decided any particular NetApp NAS filer
		 * should only be configured as a single QD for a
		 * given cluster. The use of the 'hostname' enables
		 * the system to detect attempts to configure the
		 * same filer multiple times.
		 */

		/* Is the given filer name an IPv4? */
		addr = inet_addr(filer_name);
		if (addr != (uint32_t)-1) {
			/* Get the hostname */
			hostentp = getipnodebyaddr((char *)&addr, 4, AF_INET,
			    &error_num);
		} else {
			/* Is it an IPv6? */
			if (inet_pton(AF_INET6, filer_name, (void *) &addr6)
			    == 1) {
				hostentp = getipnodebyaddr((char *)&addr6, 16,
				    AF_INET6, &error_num);
			} else {
				/* Must be host name or alias */
				hostentp = getipnodebyname(filer_name, AF_INET6,
				    AI_ALL | AI_ADDRCONFIG | AI_V4MAPPED,
				    &error_num);
			}
		}
		if (hostentp == NULL || hostentp->h_name == NULL) {
			if (messages != (char **)0) {
				(void) sprintf(errbuf, dgettext(TEXT_DOMAIN,
				    "Cannot communicate with NAS device "
				    "\"%s\" for quorum \"%s\".\n"),
				    filer_name, qdname);
				scconf_addmessage(errbuf, messages);
			}
			rstatus = SCCONF_EQD_CONFIG;
			goto cleanup;
		}

		rstatus = conf_get_qdevs(clconf, &qdevs_p);
		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}

		/* Check whether a quorum configured for the same filer */
		for (qdev_p = &qdevs_p; *qdev_p;
		    qdev_p = &(*qdev_p)->scconf_qdev_next) {

			/* Go through the properties */
			for (prop = (*qdev_p)->scconf_qdev_propertylist;
			    prop; prop = prop->scconf_prop_next) {
				if (prop->scconf_prop_key == NULL)
					continue;

				if (strcmp(prop->scconf_prop_key,
				    PROP_QDEV_HOSTNAME) != 0)
					continue;

				if (strcmp(prop->scconf_prop_value,
				    hostentp->h_name) == 0) {
					rstatus = SCCONF_DUP_NASQD;
					break;
				}
			}
		}
		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}

		/* Set the property "filer" and "filer_hostname" */
		clconf_err = clconf_obj_set_property(qdev,
		    PROP_QDEV_FILER_NAME, filer_name);
		if (clconf_err != CL_NOERROR) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		clconf_err = clconf_obj_set_property(qdev,
		    PROP_QDEV_HOSTNAME, hostentp->h_name);
		if (clconf_err != CL_NOERROR) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		/* Process the properties */
		for (prop = qdproperties; prop; prop = prop->scconf_prop_next) {
			if (!prop->scconf_prop_key || !prop->scconf_prop_value)
				continue;

			if (strcmp(prop->scconf_prop_key, PROP_QDEV_FILER_NAME)
			    == 0)
				continue;

			if (strcmp(prop->scconf_prop_key, PROP_QDEV_LUNID)
			    == 0) {
				if (prop->scconf_prop_value) {
					lun_id = strdup(
					    prop->scconf_prop_value);
					if (lun_id == NULL) {
						rstatus = SCCONF_ENOMEM;
						goto cleanup;
					}
				}
			}

			if (strcmp(prop->scconf_prop_key, PROP_QDEV_TIMEOUT)
			    == 0) {
				if (!is_updateable_prop(
				    prop->scconf_prop_key,
				    prop->scconf_prop_value)) {
					rstatus = SCCONF_EBADVALUE;

					if (messages != (char **)0) {
						(void) sprintf(errbuf,
						    dgettext(TEXT_DOMAIN,
						    "The \"%s\" value \"%s\" "
						    "is out of range for "
						    "quorum \"%s\".\n"),
						    prop->scconf_prop_key,
						    prop->scconf_prop_value,
						    qdname);
						scconf_addmessage(errbuf,
						    messages);
					}
					goto cleanup;
				}
			}

			clconf_err = clconf_obj_set_property(qdev,
			    prop->scconf_prop_key, prop->scconf_prop_value);
			if (clconf_err != CL_NOERROR) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}
		}

		/*
		 * The FBC 1225 decided that the lun id will default to
		 * 0, when it is not specified by during addition.
		 */
		if (lun_id == NULL) {
			clconf_err = clconf_obj_set_property(qdev,
			    PROP_QDEV_LUNID, DEFAULT_LUNID);
			if (clconf_err != CL_NOERROR) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}
		}

		/*
		 * The FBC 1225 decided that the lun_name could be
		 * anything but for simplicity we will define that the
		 * LUN configured on the filer as a quorum device for
		 * < clustername > will be < clustername >
		 */
		clustername = clconf_obj_get_name((clconf_obj_t *)clconf);
		if (clustername != NULL) {
			clconf_err = clconf_obj_set_property(qdev,
			    PROP_QDEV_LUN_NAME, clustername);
			if (clconf_err != CL_NOERROR) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}
		}

		/* Get cluster nodes. No nodelist for NAS */
		rstatus = conf_get_clusternodes(clconf,
		    (scconf_cfg_node_t **)&clusternodes);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		for (node = clusternodes; node;
			node = node->scconf_node_next) {
			(void) sprintf(pathprop, "path_%d",
				node->scconf_node_nodeid);
			if (node->scconf_node_qvotes > 0) {
				pathstate = "enabled";
			} else {
				if (node->scconf_node_qvotes == 0) {
					pathstate = "disabled";
				} else {
					rstatus = SCCONF_EUNEXPECTED;
					goto cleanup;
				}
			}

			/* Set the "path" property */
			clconf_err = clconf_obj_set_property(qdev,
			    pathprop, pathstate);
			if (clconf_err != CL_NOERROR) {
				switch (clconf_err) {
				case CL_BADNAME:
				default:
					rstatus = SCCONF_EUNEXPECTED;
					goto cleanup;
				} /*lint !e788 */
			}
		}

		/*
		 * Make sure that the properties provided on the command line
		 * are correct and will allow the quorum algorithm to contact
		 * the filer. We will use the "read keys" form of the
		 * NTAPquorum command, because that should always succeed
		 * even if this node was previously fenced from the device.
		 */

		/* Get the nodeid of the current node. */
		if (cladm(CL_CONFIG, CL_NODEID, &nodeid) != 0) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		/* Get the nodename of the current node. */
		rstatus = conf_get_nodename(clconf, nodeid, &nodenamep);
		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}

		(void) sprintf(cmd_line, "%s -t %s -I %s.%s -l %s rk 2>&1",
		    NETAPP_BINARY, filer_name, NETAPP_INITIATOR, nodenamep,
		    lun_id ? lun_id : DEFAULT_LUNID);
		free(nodenamep);
		file_ptr = popen(cmd_line, "r");
		if (file_ptr != NULL) {
			*output_buf = '\0';
			(void) fgets(output_buf, BUFSIZ, file_ptr);
			(void) pclose(file_ptr);
		} else {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		/* Search the output to determine if there was an error. */
		if (strncmp(output_buf, "ERROR: LUN", 10) == 0) {
			/* Add error messages */
			if (messages != (char **)0) {
				(void) sprintf(errbuf, dgettext(TEXT_DOMAIN,
				    "Error in filer LUN or igroup "
				    "configuration.\n"));
				scconf_addmessage(errbuf, messages);
			}
			rstatus = SCCONF_EQD_CONFIG;
			goto cleanup;
		} else if (strncmp(output_buf, "ERROR: Session", 14) == 0) {
			if (messages != (char **)0) {
				(void) sprintf(errbuf, dgettext(TEXT_DOMAIN,
				    "Cannot communicate with NAS device "
				    "\"%s\".\n"),
				    filer_name);
				scconf_addmessage(errbuf, messages);
			}
			rstatus = SCCONF_EQD_CONFIG;
			goto cleanup;
		}

		/*
		 * The requirement is all cluster nodes must be
		 * up and connected to the NAS device. If one node
		 * is not active, the add operation should fail.
		 *
		 * Before committing the addition of the new QD to
		 * the CCR, scrub the quorum device for all active
		 * nodes.
		 *
		 * We will do the scrub loop twice. The first time we will
		 * make sure that at least one node succeeds. The one node
		 * may be the only node which was still a cluster member
		 * before this QD was (possibly) removed previously. The second
		 * time we loop, we will require that the scrub succeed from
		 * all nodes. This will guarantee that the device is
		 * accessible from all nodes of the cluster as required by
		 * CLARC review for FBC 1225.
		 */

		/* Check if all nodes are in cluster mode */
		for (i = 0;  i < quorum_status->num_nodes;  ++i) {
			if (quorum_status->nodelist[i].state
			    != QUORUM_STATE_ONLINE) {
				/* Node is offline */
				found = B_FALSE;
				for (node = clusternodes; node;
				    node = node->scconf_node_next) {
					if (node->scconf_node_nodeid ==
					    quorum_status->nodelist[i].nid) {
						found = B_TRUE;
						break;
					}
				}

				/* Print error */
				if (found) {
					(void) sprintf(errbuf,
					    dgettext(TEXT_DOMAIN,
					    "Node \"%s\" is not "
					    "in cluster mode.\n"),
					    node->scconf_node_nodename);
					scconf_addmessage(errbuf,
					    messages);
				}
				rstatus = SCCONF_EQD_CONFIG;
				goto cleanup;
			}
		}

		/* Scrub each node, in turn, until one node success */
		for (i = 0;  i < quorum_status->num_nodes;  ++i) {
			/* Attempt to scrub the device */
			clconf_err = clconf_quorum_device_scrub(
			    qdname, quorum_status->nodelist[i].nid,
			    SCQD_SCCONF_QD_TYPE, qdev);

			/* If any node succeeds, we should be done. */
			if (clconf_err == CL_NOERROR) {
				break;
			}

		}

		/* Try each node, in turn, until all success */
		for (i = 0;  i < quorum_status->num_nodes;  ++i) {
			/* Attempt to scrub the device */
			clconf_err = clconf_quorum_device_scrub(
			    qdname, quorum_status->nodelist[i].nid,
			    SCQD_SCCONF_QD_TYPE, qdev);

			/* if one node fail, it's not valid config */
			if (clconf_err != CL_NOERROR) {
				rstatus = SCCONF_EQD_SCRUB;
				goto cleanup;
			}
		}

		/* Commit */
		rstatus = conf_cluster_commit(clconf, NULL);

		/* if commit returned okay, we are done */
		if (rstatus == SCCONF_NOERR) {
			break;

		/* else if outdated handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			goto cleanup;
		}
	}
	/* reset the device */
	rstatus = scconf_reset_quorum_device(qdname);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

cleanup:
	if (clusternodes) {
		conf_free_clusternodes(clusternodes);
	}

	if (lun_id) {
		free(lun_id);
	}

	if (hostentp) {
		freehostent(hostentp);
	}

	/* Free the properties list */
	if (qdproperties != (scconf_cfg_prop_t *)0) {
		conf_free_proplist(qdproperties);
	}

	/* Free the quorum device list */
	if (qdevs_p != (scconf_cfg_qdev_t *)0) {
		conf_free_qdevs(qdevs_p);
	}

	/* Release the cluster config */
	if (clconf != (clconf_cluster_t *)0) {
		clconf_obj_release((clconf_obj_t *)clconf);
	}

	cluster_release_quorum_status(quorum_status);

	return (rstatus);
}

/*
 * scqd_remove_quorum
 *
 * Remove a NAS quorum device.
 * The entry criteria for this function is that the QD already have a vote of
 * 0. This function is just responsible for the final remove step.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOEXIST		- quorumdev not found
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_ESETUP		- handle data is invalid
 *	SCCONF_EQUORUM		- would result in loss of quorum
 *	SCCONF_EOVERFLOW	- message buffer overflow
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_EBADVALUE	- bad CCR data
 */
scconf_errno_t
scqd_remove_quorum(char *qdname, uint32_t force_flg, char **messages) {
	return (conf_rm_quorum_device(qdname, force_flg));
} /*lint !e715 */

/*
 * scqd_set_qd_properties
 *
 * Set the property name and value from qdproperties
 * for the NAS quorum device qdname. Property values
 * can only be set for a device that already exists.
 * If the property already exists, its value is overwritten
 * with the value given. If the property does not exist
 * it is created.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOEXIST		- quorumdev not found
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_ESTALE		- cluster handle is stale
 */
scconf_errno_t
scqd_set_qd_properties(char *qdname, char *properties, char **messages)
{
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	clconf_obj_t *qdev;
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_errnum_t clconf_err;
	scconf_cfg_prop_t *prop = NULL;
	scconf_cfg_prop_t *qdproperties = (scconf_cfg_prop_t *)0;
	char errbuf[BUFSIZ];

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if ((qdname == NULL) || (properties == NULL))
		return (SCCONF_EINVAL);

	/* Get the property list from the properties string */
	rstatus = scconf_propstr_to_proplist(properties, &qdproperties);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/*
	 * Loop until the commit succeeds, or until a fatal error
	 * is encountered.
	 */

	for (;;) {
		/* Release cluster config */
		if (clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)clconf);
			clconf = (clconf_cluster_t *)0;
		}

		/* Allocate the cluster config */
		rstatus = scconf_cltr_openhandle((scconf_cltr_handle_t *)
				&clconf);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Make sure that the device does already exist */
		rstatus = conf_get_qdevobj(clconf, qdname, &qdev);
		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}

		for (prop = qdproperties; prop; prop = prop->scconf_prop_next) {
			/* Skip */
			if (!(prop->scconf_prop_key))
				continue;

			/* Valid? */
			if (!is_updateable_prop(prop->scconf_prop_key,
			    prop->scconf_prop_value)) {
				rstatus = SCCONF_EBADVALUE;
				if (messages != (char **)0) {
					(void) sprintf(errbuf,
					    dgettext(TEXT_DOMAIN,
					    "Invalid property \"%s\" for "
					    "quorum \"%s\".\n"),
					    prop->scconf_prop_key, qdname);
					scconf_addmessage(errbuf, messages);
				}
			}
		}
		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}

		/* Set the property */
		for (prop = qdproperties; prop; prop = prop->scconf_prop_next) {
			/* Skip */
			if (!(prop->scconf_prop_key))
				continue;

			/* Set the property */
			clconf_err = clconf_obj_set_property(qdev,
			    prop->scconf_prop_key, prop->scconf_prop_value);
			if (clconf_err != CL_NOERROR) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}
		}

		/* Commit */
		rstatus = conf_cluster_commit(clconf, NULL);

		/* if commit returned okay, we are done */
		if (rstatus == SCCONF_NOERR) {
			break;

		/* else if outdated handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			goto cleanup;
		}
	}

	/* reset the device */
	rstatus = scconf_reset_quorum_device(qdname);
	if (rstatus != SCCONF_NOERR) {
		goto cleanup;
	}

cleanup:
	/* Free the properties list */
	if (qdproperties != (scconf_cfg_prop_t *)0) {
		conf_free_proplist(qdproperties);
	}

	/* Release the cluster config */
	if (clconf != (clconf_cluster_t *)0) {
		clconf_obj_release((clconf_obj_t *)clconf);
	}

	return (rstatus);

/*
 * Suppress lint Info(715) about un-referenced arg messages,
 * which can used for detailed messages to display to users.
 */
} /*lint !e715 */

/*
 * is_valid_nas_prop
 *
 * Check if the given argument is a valid netapp NAS quorum
 * property.
 *
 * Return:
 *	PROP_INVALID	Unknown property
 *	PROP_VALID	Valid property
 */
int
is_valid_nas_prop(const char *xProp)
{
	if (!xProp)
		return (PROP_INVALID);
	if (strcmp(xProp, PROP_QDEV_FILER_NAME) == 0)
		return (PROP_VALID);
	if (strcmp(xProp, PROP_QDEV_LUNID) == 0)
		return (PROP_VALID);
	if (strcmp(xProp, PROP_QDEV_TIMEOUT) == 0)
		return (PROP_VALID);

	return (PROP_INVALID);
}

/*
 * is_netapp_nas_usable
 *
 * Check if Netapp NAS support is available.
 *
 * Return:
 *	B_TRUE		Netapp NAS is supported
 *	B_FALSE		Netapp NAS isn't available
 */
static boolean_t
is_netapp_nas_usable(void)
{
	clcomm_vp_version_t netapp_nas_version;

	if (clcomm_get_running_version("quorum",
	    &netapp_nas_version) != 0) {
		return (B_FALSE);
	}

	return ((boolean_t)(netapp_nas_version.major_num >= 1));
}

/*
 * is_updateable_prop
 *
 *	Check if the given property is updateable or not.
 *
 * Return:
 *	B_TRUE		valid updateable property
 *	B_FALSE		property cannot be updated
 */
static boolean_t
is_updateable_prop(const char *prop, const char *value)
{
	int timeout_val;

	if (!prop) {
		return (B_FALSE);
	}

	if (strcmp(prop, PROP_QDEV_TIMEOUT) == 0) {
		/* Check the range */
		if (value) {
			if (sscanf(value, "%d", &timeout_val) != 1) {
				return (B_FALSE);
			}

			if ((timeout_val < DEFAULT_TIMEOUT_MIN) ||
			    (timeout_val > DEFAULT_TIMEOUT_MAX)) {
				return (B_FALSE);
			}
		}

		return (B_TRUE);
	}

	return (B_FALSE);
}
