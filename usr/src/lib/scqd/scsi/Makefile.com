#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)Makefile.com	1.8	08/05/21 SMI"
#
# lib/scqd/scsi/Makefile.com
#

LIBRARY= scconf_scsi.a
VERS=.1

OBJECTS= scqd_scsi.o

# include library definitions
include ../../../Makefile.lib

# override value of ROOTCLUSTLIBDIR set in Makefile.lib
ROOTCLUSTLIBDIR=        $(VROOT)/usr/cluster/lib/qd

MAPFILE=	../common/mapfile-vers
SRCS=		$(OBJECTS:%.o=../common/%.c)

CLOBBERFILES += $(PIFILES)

LIBS=		$(DYNLIB)

LINTFLAGS += -I.. -D_POSIX_PTHREAD_SEMANTICS
LINTFILES += $(OBJECTS:%.o=%.ln)

MTFLAG	=	-mt
CPPFLAGS +=	-I.. -I$(SRC)/lib/libdid/include -I$(SRC)/lib/libscconf
CPPFLAGS +=	-I$(SRC)/lib/libclcomm/common
CPPFLAGS +=	-I$(SRC)/head/scadmin/rpc
PMAP =	-M $(MAPFILE)
LDLIBS +=       -lscconf -ldid -lclconf -lc -lclcomm

.KEEP_STATE:

$(DYNLIB):	$(MAPFILE)

# include library targets
include ../../../Makefile.targ

objs/%.o pics/%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<
