/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scqd_scsi.c	1.18	09/02/01 SMI"

/*
 * scqd_scsi.so SCSI quorum config functions
 */

#include "../../scqd_common.h"
#include <unistd.h>
#include <libdid.h>
#include <assert.h>

#define	SCSI_SCCONF_QD_TYPE	"scsi"
#define	SHARED_DISK_SCCONF_QD_TYPE	"shared_disk"

#define	PROP_QDEV_TIMEOUT	"timeout"
#define	DEFAULT_TIMEOUT_MIN	1
#define	DEFAULT_TIMEOUT_MAX	10000

static char **conf_getdidnodes(char *dname);
static boolean_t is_updateable_prop(char *prop, char *value);
static boolean_t is_shared_disk_usable(void);

/*
 * scqd_get_qd_type
 *
 * This function confirms if the quorum device type is "scsi". It is
 * to make sure the right quorum device type .so is being opened.
 *
 * Return values:
 *
 *	boolean_t
 */
/*
 * lint throws informational message that the arguments to this function
 * can be declared const. This function is exported by this library for
 * use by clients, and this function signature is common to all different
 * scconf_* libraries for configuring various quorum types. Hence we
 * ignore this informational message by lint.
 */
/*lint -e818 */
boolean_t
scqd_get_qd_type(char *scconf_type, char *qd_type)
{
	/* Check to ensure the device type is "scsi" or "shared_disk" */
	if (((qd_type) && (strcmp(qd_type, SCSI_SCCONF_QD_TYPE) == 0)) ||
	    ((scconf_type) && (strcmp(scconf_type, SCSI_SCCONF_QD_TYPE)
	    == 0))) {
		return (B_TRUE);
	} else if (
	    ((qd_type) && (strcmp(qd_type, SHARED_DISK_SCCONF_QD_TYPE) == 0)) ||
	    ((scconf_type) && (strcmp(scconf_type, SHARED_DISK_SCCONF_QD_TYPE)
	    == 0))) {
		return (B_TRUE);
	} else {
		return (B_FALSE);
	}
}
/*lint +e818 */

/*
 * scqd_get_qd_device
 *
 * For the given global quorum device name, construct the full path
 * name of the rdsk device for the corresponding DID device in didname
 * and verify this device exist in CCR. If it exists, return a copy
 * of name used in CCR table for the quorum device name with B_TRUE.
 * For other situations, simply return B_FALSE.
 *
 * Return values:
 *	boolean_t
 */
boolean_t
scqd_get_qd_device(char *globalname, char *qdevname)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	char didname[MAXPATHLEN];
	const char *ccr_qdevname;
	clconf_obj_t *qdev;
	boolean_t found = B_FALSE;

	/* Check arguments */
	if (globalname == NULL || *globalname == '\0' ||
	    qdevname == NULL)
		return (B_FALSE);

	/* Convert from quorum device name to did device name */
	if (scconf_didname_slice(globalname, didname))
		return (B_FALSE);

	/* Generate the quorum device name from the did name */
	if (scconf_qdevname(didname, qdevname))
		return (B_FALSE);

	/* Get the clconf */
	rstatus = scconf_cltr_openhandle(
	    (scconf_cltr_handle_t *)&clconf);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/* Find the quorum device */
	rstatus = conf_get_qdevobj(clconf, qdevname, &qdev);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/* Find its gdevname property */
	ccr_qdevname = clconf_obj_get_property(qdev,
	    PROP_QDEV_GDEVNAME);

	/* Find the match */
	if (!ccr_qdevname) {
		found = B_FALSE;
		goto cleanup;
	} else if (strcmp(ccr_qdevname, didname) == 0) {
		found = B_TRUE;
		goto cleanup;
	}

cleanup:
	/* Release cluster config */
	if (clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)clconf);

	return (found);
}

/*
 * scqd_add_quorum
 *
 * Add the given "globaldevicename" as a quorum device, if it is
 * not already configured as such.  Then, for each node in the
 * the "nodeslist", add a port from the node to the device.
 *
 * If /dev/global/rdsk/<devicename> is given for the "globaldevicename",
 * it is converted to /dev/did/rdsk/<devicename>.   Only the last
 * component of the name (i.e., <devicename>) need be given.   If the
 * slice is missing, slice two is used.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_EEXIST		- quorum device is already configured
 *	SCCONF_ENOEXIST		- globaldev not found or not ported to node(s)
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_EQUORUM		- would result in loss of quorum
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_EBADVALUE	- bad CCR data
 *	SCCONF_EQD_CONFIG	- replicated device
 */
scconf_errno_t
scqd_add_quorum(char *globaldevicename, char *nodeslist[],
    char *properties, char **messages)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_errnum_t clconf_err;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	char **dev_nodeslist = (char **)0;
	char **tmp_nodeslist;
	char dev_nodeids[SCCONF_NODEID_MAX + 1];
	char port_nodeids[SCCONF_NODEID_MAX + 1];
	scconf_nodeid_t nodeid;
	clconf_obj_t *qdev;
	clconf_obj_t *node;
	char didname[MAXPATHLEN];
	char qdevname[MAXPATHLEN];
	char pathprop[BUFSIZ];
	char *pathstate;
	const char *propval;
	char **nodeslistp;
	int num_ports;
	did_device_list_t *did_dev_list;
	int num_paths;
	char *acc_modep;
	scconf_cfg_prop_t *prop = NULL;
	scconf_cfg_prop_t *prop_list = (scconf_cfg_prop_t *)0;
	unsigned short globalfencingstatus;
	int is_scsi2_disk = 0;
	char errbuf[BUFSIZ];
	did_repl_list_t repl_data;
	did_repl_t repl_status;
	int libinit;
	int is_no_fencing = 0;

	/* Check arguments */
	if (globaldevicename == NULL)
		return (SCCONF_EINVAL);

	/* Check if shared_disk version is valid */
	if (!is_shared_disk_usable()) {
		return (SCCONF_VP_MISMATCH);
	}

	/* Convert from global to did device name */
	if (scconf_didname_slice(globaldevicename, didname))
		return (SCCONF_ENOEXIST);

	/*
	 * Quorum device cannot be a replicated device
	 */

	/* libdid initialization.  Needed for check_for_repl_device */
	libinit = did_initlibrary(1, DID_NONE, NULL);
	if (libinit != DID_INIT_SUCCESS) {
		return (SCCONF_EUNEXPECTED);
	}

	repl_status = check_for_repl_device(
					didname, &repl_data);
	switch (repl_status) {
	case DID_REPL_TRUE:
		/* Replicated device */
		if (messages != (char **)0) {
			(void) sprintf(errbuf,
			    dgettext(TEXT_DOMAIN,
			    "\"%s\" is a replicated device. "
			    "Replicated devices cannot be configured "
			    "as quorum devices.\n"),
			    globaldevicename);
			scconf_addmessage(errbuf, messages);
		}
		return (SCCONF_EQD_CONFIG);

	case DID_REPL_FALSE:
		/* Non-replicated device */
		break;

	case DID_REPL_BAD_DEV:
		/* non-existent did device */
		return (SCCONF_ENOEXIST);

	case DID_REPL_ERROR:
	default:
		/* internal error */
		return (SCCONF_EUNEXPECTED);
	}

	/* Generate the quorum device name (i.e., "d<N>") from the did name */
	if (scconf_qdevname(didname, qdevname))
		return (SCCONF_ENOEXIST);

	/* Get the nodes list for the DID device */
	dev_nodeslist = conf_getdidnodes(qdevname);
	if (dev_nodeslist == NULL)
		return (SCCONF_ENOEXIST);

	/* Get the property list from the properties string */
	rstatus = scconf_propstr_to_proplist(properties, &prop_list);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/*
	 * Add quorum device with a zero vote count.
	 *
	 * Loop until the commit succeeds, or until a fatal error
	 * is encountered.
	 */
	for (;;) {

		/* Release cluster config */
		if (clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)clconf);
			clconf = (clconf_cluster_t *)0;
		}

		/* Allocate the cluster config */
		rstatus = scconf_cltr_openhandle(
		    (scconf_cltr_handle_t *)&clconf);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Initialize the map of device node IDs */
		bzero(dev_nodeids, sizeof (dev_nodeids));
		for (nodeslistp = dev_nodeslist;  *nodeslistp;  ++nodeslistp) {

			/* Get the nodeid */
			rstatus = conf_get_nodeid(clconf, *nodeslistp,
			    &nodeid);

			/*
			 * If the infrastructure and DID CCR tables are
			 * out of sync, DID could list devices which
			 * are not really in the cluster.   For our
			 * purposes here, we just toss any nodes
			 * which are not found.
			 */
			if (rstatus == SCCONF_NOERR)
				dev_nodeids[nodeid] = 1;
		}

		/* if no "nodelist" given, default to all device nodes */
		if (nodeslist == NULL || *nodeslist == NULL) {
			bcopy(dev_nodeids, port_nodeids, sizeof (dev_nodeids));

		/* otherwise, verify that each node is ported to the device */
		} else {
			bzero(port_nodeids, sizeof (port_nodeids));
			for (nodeslistp = nodeslist;  *nodeslistp;
			    ++nodeslistp) {
				rstatus = conf_get_nodeid(clconf, *nodeslistp,
				    &nodeid);
				if (rstatus != SCCONF_NOERR)
					goto cleanup;
				if (!dev_nodeids[nodeid]) {
					rstatus = SCCONF_ENOEXIST;
					goto cleanup;
				}
				port_nodeids[nodeid] = 1;
			}
		}

		/* Count the node paths */
		num_ports = 0;
		for (nodeid = 1;  nodeid <= SCCONF_NODEID_MAX;  ++nodeid)
			if (port_nodeids[nodeid])
				++num_ports;

		/* There must be at least two node paths */
		if (num_ports < 2) {
			rstatus = SCCONF_ENOEXIST;
			goto cleanup;
		}

		/* Make sure that the device does not already exist */
		rstatus = conf_get_qdevobj(clconf, qdevname, &qdev);
		switch (rstatus) {
		case SCCONF_ENOEXIST:
			rstatus = SCCONF_NOERR;
			break;

		case SCCONF_NOERR:
			rstatus = SCCONF_EEXIST;
			goto cleanup;

		default:
			goto cleanup;
		} /*lint !e788 */

		/* Create the new device */
		qdev = (clconf_obj_t *)clconf_quorum_device_create();
		if (qdev == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* Add it */
		clconf_err = clconf_cluster_add_quorum_device(clconf,
		    (clconf_quorum_t *)qdev);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
				rstatus = SCCONF_EINVAL;
				goto cleanup;

			case CL_INVALID_ID:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		/* Add the name */
		clconf_obj_set_name(qdev, qdevname);

		/* Set the state to enabled */
		rstatus = conf_set_state(qdev, SCCONF_STATE_ENABLED);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Set the vote count to zero */
		clconf_err = clconf_obj_set_property(qdev,
		    PROP_QDEV_QUORUM_VOTE, "0");
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		/* Set the path to the global device */
		clconf_err = clconf_obj_set_property(qdev,
		    PROP_QDEV_GDEVNAME, didname);

		/* add each node port */
		for (nodeid = 1;  nodeid <= SCCONF_NODEID_MAX;  ++nodeid) {

			/* Skip any nodes not ported to the device */
			if (!port_nodeids[nodeid])
				continue;

			/* Initialize the property name for the "path" */
			(void) sprintf(pathprop, "path_%d", nodeid);

			/* Skip this node, if there is already a "path" */
			propval = clconf_obj_get_property(qdev, pathprop);
			if (propval != NULL)
				continue;

			/* See if the node has a non-zero vote count */
			rstatus = conf_get_nodeobj(clconf, nodeid, &node);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
			propval = clconf_obj_get_property(node,
			    PROP_NODE_QUORUM_VOTE);
			if (propval == NULL) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}

			/* If vote count is zero, path will be disabled */
			if (strcmp(propval, "0") == 0)
				pathstate = "disabled";
			else
				pathstate = "enabled";

			/* Set the "path" property */
			clconf_err = clconf_obj_set_property(qdev,
			    pathprop, pathstate);
			if (clconf_err != CL_NOERROR) {
				switch (clconf_err) {
				case CL_BADNAME:
				default:
					rstatus = SCCONF_EUNEXPECTED;
					goto cleanup;
				} /*lint !e788 */
			}
		}

		if (did_getglobalfencingstatus(&globalfencingstatus)) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		/* Set the access mode and device type property */
		did_dev_list = map_from_did_device(qdevname);
		if (did_dev_list == NULL) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		/*
		 * If the disk's fencing flag is set to use the global
		 * fencing status, then determine the access_mode based
		 * on the global fencing status.
		 * Else use the disk's fencing flag to determine access_mode.
		 */
		is_no_fencing = 0;
		is_scsi2_disk = 0;
		if (did_dev_list->default_fencing == FENCING_USE_GLOBAL) {
			/* Determine access_mode based on global fencing flag */
			switch (globalfencingstatus) {
			case GLOBAL_NO_FENCING:
				is_no_fencing++;
				break;

			case GLOBAL_FENCING_PREFER3:
				if (did_dev_list->detected_fencing ==
				    SCSI2_FENCING) {
					is_scsi2_disk++;
				} else {
					/*
					 * XXX Currently, detected_fencing
					 * can only be set to SCSI2_FENCING
					 * or FENCING_NA because we have
					 * no good way of telling
					 * if a device supports scsi3 or not.
					 * So if the global or per disk setting
					 * is prefer3 or scsi3 and
					 * the detected_fencing is not
					 * SCSI2_FENCING, we are leaving it
					 * to users' best judgement and
					 * honor their scsi3 override request.
					 *
					 * Once we have a way of detecting scsi3
					 * device, we will differentiate
					 * SCSI3_FENCING and FENCNG_NA.
					 * Most likely use pathcount if
					 * the detected_fencing is FENCING_NA.
					 */
					assert(did_dev_list->detected_fencing ==
					    FENCING_NA);
				}
				break;

			case GLOBAL_FENCING_PATHCOUNT:
				num_paths = did_get_num_paths(did_dev_list);
				if (num_paths < 3) {
					is_scsi2_disk++;
				}
				break;

			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}

		} else {
			/* Determine access_mode based on disk's fencing flag */
			switch (did_dev_list->default_fencing) {
			case NO_FENCING:
				is_no_fencing++;
				break;

			case FENCING_USE_SCSI3:
				if (did_dev_list->detected_fencing ==
				    SCSI2_FENCING) {
					is_scsi2_disk++;
				} else {
					/*
					 * XXX Currently, detected_fencing
					 * can only be set to SCSI2_FENCING
					 * or FENCING_NA because we have
					 * no good way of telling
					 * if a device supports scsi3 or not.
					 * So if the global or per disk setting
					 * is prefer3 or scsi3 and
					 * the detected_fencing is not
					 * SCSI2_FENCING, we are leaving it
					 * to users' best judgement and
					 * honor their scsi3 override request.
					 *
					 * Once we have a way of detecting scsi3
					 * device, we will differentiate
					 * SCSI3_FENCING and FENCNG_NA.
					 * Most likely use pathcount if
					 * the detected_fencing is FENCING_NA.
					 */
					assert(did_dev_list->detected_fencing ==
					    FENCING_NA);
				}
				break;

			case FENCING_USE_PATHCOUNT:
				num_paths = did_get_num_paths(did_dev_list);
				if (num_paths < 3) {
					is_scsi2_disk++;
				}
				break;
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}
		}

		if (is_no_fencing) {
			/*
			 * The device will not use SCSI fencing.
			 * Use software quorum instead.
			 */
			acc_modep = "sq_disk";
		} else {
			/* Use SCSI fencing */
			if (is_scsi2_disk) {
				acc_modep = "scsi2";
			} else {
				acc_modep = "scsi3";
			}
		}

		clconf_err = clconf_obj_set_property(qdev, PROP_QDEV_ACCESSMODE,
		    acc_modep);
		if (clconf_err != CL_NOERROR) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		/* Save "shared_disk" as the type. */
		clconf_err = clconf_obj_set_property(qdev, PROP_QDEV_TYPE,
		    SHARED_DISK_SCCONF_QD_TYPE);
		if (clconf_err != CL_NOERROR) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		/* Set timeout. Go through the properties */
		for (prop = prop_list; prop; prop = prop->scconf_prop_next) {

			/* Skip */
			if (!(prop->scconf_prop_key) ||
			    !(prop->scconf_prop_value))
				continue;

			/* Timeout is the only valid property for now */
			if (strcmp(prop->scconf_prop_key, "timeout") != 0) {
				rstatus = SCCONF_EBADVALUE;
				if (messages != (char **)0) {
					(void) sprintf(errbuf,
					    dgettext(TEXT_DOMAIN,
					    "Invalid property \"%s\" for "
					    "quorum \"%s\".\n"),
					    prop->scconf_prop_key, qdevname);
					scconf_addmessage(errbuf, messages);
				}
				continue;
			}

			if (!is_updateable_prop(prop->scconf_prop_key,
			    prop->scconf_prop_value)) {
				rstatus = SCCONF_EBADVALUE;
				if (messages != (char **)0) {
					(void) sprintf(errbuf,
					    dgettext(TEXT_DOMAIN,
					    "The \"%s\" value \"%s\" is out "
					    "of range for quorum \"%s\".\n"),
					    prop->scconf_prop_key,
					    prop->scconf_prop_value, qdevname);
					scconf_addmessage(errbuf, messages);
				}
				continue;
			}

			clconf_err = clconf_obj_set_property(qdev,
			    PROP_QDEV_TIMEOUT, prop->scconf_prop_value);
			if (clconf_err != CL_NOERROR) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}
		}
		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}

		/*
		 * Before committing the addition of the new QD to
		 * the CCR, scrub the PGRe section of the disk for
		 * SCSI2 and software_quorum devices. For SCSI3
		 * devices, scrub all the keys.
		 */
		/* Try each ported node, in turn, until success */
		for (nodeid = 1; nodeid <= SCCONF_NODEID_MAX;
		    ++nodeid) {

			/* Skip any nodes not ported to the device */
			if (!port_nodeids[nodeid])
				continue;

			/* Attempt to scrub the device */
			clconf_err = clconf_quorum_device_scrub(
			    didname, nodeid, acc_modep, qdev);

			/* If success, we are done trying to scrub. */
			if (clconf_err == CL_NOERROR) {
				break;
			}

		}

		/* If we hit the end of the list, exit with error */
		if (nodeid > SCCONF_NODEID_MAX) {
			rstatus = SCCONF_EIO;
			goto cleanup;
		}

		/* Commit */
		rstatus = conf_cluster_commit(clconf, NULL);

		/* if commit returned okay, we are done */
		if (rstatus == SCCONF_NOERR) {
			break;

		/* else if outdated handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			goto cleanup;
		}
	}

	/* reset the device */
	rstatus = scconf_reset_quorum_device(globaldevicename);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

cleanup:
	/* Release the cluster config */
	if (clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)clconf);

	/* Rlease the property list */
	if (prop_list != (scconf_cfg_prop_t *)0)
		conf_free_proplist(prop_list);

	/* Free the device ports nodes list */
	if (dev_nodeslist != NULL) {
		for (tmp_nodeslist = dev_nodeslist;  *tmp_nodeslist;
		    ++tmp_nodeslist)
			free(*tmp_nodeslist);
		free(dev_nodeslist);
	}

	return (rstatus);

/*
 * Suppress lint Info(715) about un-referenced arg messages,
 * which can used for detailed messages to display to users.
 */
} /*lint !e715 */

/*
 * scqd_remove_quorum
 *
 * Remove a quorum device.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOEXIST		- quorumdev not found
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_EQUORUM		- would result in loss of quorum
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_EBADVALUE	- bad CCR data
 */
scconf_errno_t
scqd_remove_quorum(char *quorumdevice, uint32_t force_flg, char **messages)
{
	return (conf_rm_quorum_device(quorumdevice, force_flg));
}

/*
 * scqd_set_qd_properties
 *
 * Set the property name and value from qdproperties
 * for the quorum device qdname. Property values
 * can only be set for a device that already exists.
 * If the property already exists, its value is overwritten
 * with the value given. If the property does not exist
 * it is created.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOEXIST		- quorumdev not found
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_ESTALE		- cluster handle is stale
 */
scconf_errno_t
scqd_set_qd_properties(char *qdname, char *properties, char **messages)
{
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	clconf_obj_t *qdev;
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_errnum_t clconf_err;
	scconf_cfg_prop_t *prop = NULL;
	scconf_cfg_prop_t *qdproperties = (scconf_cfg_prop_t *)0;
	char errbuf[BUFSIZ];

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if ((qdname == NULL) || (properties == NULL))
		return (SCCONF_EINVAL);

	/* Get the property list from the properties string */
	rstatus = scconf_propstr_to_proplist(properties, &qdproperties);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/*
	 * Loop until the commit succeeds, or until a fatal error
	 * is encountered.
	 */

	for (;;) {
		/* Release cluster config */
		if (clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)clconf);
			clconf = (clconf_cluster_t *)0;
		}

		/* Allocate the cluster config */
		rstatus = scconf_cltr_openhandle((scconf_cltr_handle_t *)
				&clconf);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Make sure that the device does already exist */
		rstatus = conf_get_qdevobj(clconf, qdname, &qdev);
		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}

		/* Validate the property */
		for (prop = qdproperties; prop; prop = prop->scconf_prop_next) {
			/* Skip */
			if (!(prop->scconf_prop_key))
				continue;

			/* Timeout is the only updateable property */
			if (!is_updateable_prop(prop->scconf_prop_key,
			    prop->scconf_prop_value)) {
				rstatus = SCCONF_EBADVALUE;
				if (messages != (char **)0) {
					(void) sprintf(errbuf,
					    dgettext(TEXT_DOMAIN,
					    "Invalid property \"%s\" for "
					    "quorum \"%s\".\n"),
					    prop->scconf_prop_key, qdname);
					scconf_addmessage(errbuf, messages);
				}
				continue;
			}
		}
		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}

		/* Set the property */
		for (prop = qdproperties; prop; prop = prop->scconf_prop_next) {

			/* Skip */
			if (!(prop->scconf_prop_key))
				continue;

			/* Set the property */
			clconf_err = clconf_obj_set_property(qdev,
			    prop->scconf_prop_key, prop->scconf_prop_value);
			if (clconf_err != CL_NOERROR) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}
		}

		/* Commit */
		rstatus = conf_cluster_commit(clconf, NULL);

		/* if commit returned okay, we are done */
		if (rstatus == SCCONF_NOERR) {
			break;

		/* else if outdated handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			goto cleanup;
		}
	}

	/* reset the device */
	rstatus = scconf_reset_quorum_device(qdname);
	if (rstatus != SCCONF_NOERR) {
		goto cleanup;
	}

cleanup:
	/* Free the properties list */
	if (qdproperties != (scconf_cfg_prop_t *)0) {
		conf_free_proplist(qdproperties);
	}

	/* Release the cluster config */
	if (clconf != (clconf_cluster_t *)0) {
		clconf_obj_release((clconf_obj_t *)clconf);
	}

	return (rstatus);

/*
 * Suppress lint Info(715) about un-referenced arg messages,
 * which can used for detailed messages to display to users.
 */
} /*lint !e715 */

/*
 * conf_getdidnodes
 *
 * Return the list of node names to which the given did device (didname)
 * is attached.  This list is returned as a NULL terminated array of
 * pointers to node names.   It is the caller's responsibility to free
 * all memory associated with both the names and the array.
 *
 * If the did device is not found, or has no nodes attached to it,
 * NULL is returned.
 */
static char **
conf_getdidnodes(char *dname)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	did_device_list_t *didlist;
	char **tmplist, **nodeslist = NULL;
	char *path, *paths = NULL;
	char *ptr, *last;
	size_t count = 0;

	/* Check argument */
	if (dname == NULL)
		return ((char **)0);

	/* get the didlist */
	didlist = map_from_did_device(dname);
	if (didlist == NULL)
		return ((char **)0);

	/* get the paths */
	paths = did_get_path(didlist);
	if (paths == NULL)
		return ((char **)0);

	/* break it down to a list of nodes */
	path = strtok_r(paths, " ", &last);
	while (path != NULL) {
		if ((ptr = strchr(path, ':')) != NULL && ptr != path) {
			*ptr = '\0';
			++count;
			tmplist = (char **)realloc(nodeslist,
			    (count + 1) * sizeof (char *));
			if (tmplist == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}
			nodeslist = tmplist;
			nodeslist[count] = (char *)0;
			nodeslist[count - 1] = strdup(path);
			if (nodeslist[count - 1] == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}
		}
		path = strtok_r(NULL, " ", &last);
	}

cleanup:
	/* Free paths */
	if (paths)
		free(paths);

	/* If error, free nodelist, else return it */
	if (rstatus != SCCONF_NOERR) {
		if (nodeslist) { /*lint !e644 */
			char **tmpnl;
			for (tmpnl = nodeslist;  *tmpnl; ++tmpnl)
				free(*tmpnl);
			free(nodeslist);
		}
		return ((char **)0);
	} else {
		return (nodeslist);
	}
}

/*
 * is_updateable_prop
 *
 *	Check if the given property is updateable or not.
 *
 * Return:
 *	B_TRUE		valid updateable property
 *	B_FALSE		property cannot be updated
 */
static boolean_t
is_updateable_prop(char *prop, char *value)
{
	int timeout_val;

	if (!prop) {
		return (B_FALSE);
	}

	if (strcmp(prop, PROP_QDEV_TIMEOUT) == 0) {
		/* Check the range */
		if (value) {
			if (sscanf(value, "%d", &timeout_val) != 1) {
				return (B_FALSE);
			}

			if ((timeout_val < DEFAULT_TIMEOUT_MIN) ||
			    (timeout_val > DEFAULT_TIMEOUT_MAX)) {
				return (B_FALSE);
			}
		}

		return (B_TRUE);
	}

	return (B_FALSE);
}

/*
 * is_shared_disk_usable
 *
 * Check if shared disk support is available.
 *
 * Return:
 *      B_TRUE          shared_disk is supported
 *      B_FALSE         shared_disk isn't available
 */
static boolean_t
is_shared_disk_usable(void)
{
	clcomm_vp_version_t vp_version;

	if (clcomm_get_running_version("clquorum",
	    &vp_version) != 0) {
		return (B_FALSE);
	}

	return ((boolean_t)(vp_version.major_num >= 2));
}
