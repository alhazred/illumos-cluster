/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * dlscxcfg.h
 *
 *	scxcfg API through dynamic linking ...
 */

#ifndef	_DLSCXCFG_H
#define	_DLSCXCFG_H

#pragma ident	"@(#)dlscxcfg.h	1.2	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <scxcfg/scxcfg.h>

#define	SCXCFG_LIB_PATH	"/usr/cluster/lib/libscxcfg.so.1"

typedef int (*scxcfg_open_t)(scxcfg_t, scxcfg_error_t);
typedef int (*scxcfg_close_t)(scxcfg_t);
typedef int (*scxcfg_getproperty_value_t)(scxcfg_t, const char *,
    const char *, char *, scxcfg_error_t);
typedef int (*scxcfg_getlistproperty_value_t)(scxcfg_t, const char *,
    const char *, f_property_t **, int *, scxcfg_error_t);
typedef int (*scxcfg_setproperty_value_t)(scxcfg_t, const char *,
    const char *, const char *, scxcfg_error_t);
typedef int (*scxcfg_delproperty_t)(scxcfg_t, const char *,
    const char *, scxcfg_error_t);
typedef int (*scxcfg_get_local_nodeid_t)(scxcfg_nodeid_t *, scxcfg_error_t);
typedef int (*scxcfg_get_local_nodename_t)(scxcfg_t, char *, scxcfg_error_t);
typedef int (*scxcfg_get_ipaddress_t)(scxcfg_t, scxcfg_nodeid_t, char *ip,
    scxcfg_error_t);
typedef int (*scxcfg_get_nodeid_t)(scxcfg_t, const char *, scxcfg_nodeid_t *,
    scxcfg_error_t);
typedef int (*scxcfg_get_nodename_t)(scxcfg_t, const scxcfg_nodeid_t,
    char *, scxcfg_error_t);
typedef int (*scxcfg_del_node_t)(scxcfg_t, const char *, scxcfg_error_t);
typedef int (*scxcfg_isfarm_t)(void);
typedef int (*scxcfg_isfarm_nodeid_t)(scxcfg_nodeid_t);
typedef int (*scxcfg_add_node_t)(scxcfg_t cfg, const char *, f_property_t *,
    scxcfg_nodeid_t *, scxcfg_error_t);
typedef int (*scxcfg_lookup_node_property_value_t)(scxcfg_t, const char *,
    const char *, const char *, scxcfg_nodeid_t *,
    scxcfg_error_t);
typedef void (*scxcfg_freelist_t)(f_property_t *);


typedef struct {
	scxcfg_open_t			scxcfg_open;
	scxcfg_close_t			scxcfg_close;
	scxcfg_getproperty_value_t	scxcfg_getproperty_value;
	scxcfg_getlistproperty_value_t	scxcfg_getlistproperty_value;
	scxcfg_setproperty_value_t	scxcfg_setproperty_value;
	scxcfg_delproperty_t		scxcfg_delproperty;
	scxcfg_get_local_nodeid_t	scxcfg_get_local_nodeid;
	scxcfg_get_local_nodename_t	scxcfg_get_local_nodename;
	scxcfg_get_ipaddress_t		scxcfg_get_ipaddress;
	scxcfg_get_nodeid_t		scxcfg_get_nodeid;
	scxcfg_get_nodename_t		scxcfg_get_nodename;
	scxcfg_del_node_t		scxcfg_del_node;
	scxcfg_isfarm_t			scxcfg_isfarm;
	scxcfg_isfarm_nodeid_t		scxcfg_isfarm_nodeid;
	scxcfg_add_node_t		scxcfg_add_node;
	scxcfg_lookup_node_property_value_t scxcfg_lookup_node_property_value;
	scxcfg_freelist_t		scxcfg_freelist;
} scxcfg_api_t;

int scxcfg_load_module(void **instance, scxcfg_api_t *api);
int scxcfg_unload_module(void **instance, scxcfg_api_t *api);

#ifdef __cplusplus
}
#endif

#endif /* _DLSCXCFG_H */
