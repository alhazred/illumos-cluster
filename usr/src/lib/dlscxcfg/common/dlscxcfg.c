/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * dlscxcfg.c
 *
 *	scxcfg API through dynamic linking ...
 */

#pragma ident	"@(#)dlscxcfg.c	1.2	08/05/20 SMI"

#ifdef linux
#include <linux/stddef.h>
#endif

#include <dlfcn.h>
#include "dlscxcfg.h"

static char *funcnames[] = {
	"scxcfg_open",
	"scxcfg_close",
	"scxcfg_getproperty_value",
	"scxcfg_getlistproperty_value",
	"scxcfg_setproperty_value",
	"scxcfg_delproperty",
	"scxcfg_get_local_nodeid",
	"scxcfg_get_local_nodename",
	"scxcfg_get_ipaddress",
	"scxcfg_get_nodeid",
	"scxcfg_get_nodename",
	"scxcfg_del_node",
	"scxcfg_isfarm",
	"scxcfg_isfarm_nodeid",
	"scxcfg_add_node",
	"scxcfg_lookup_node_property_value",
	"scxcfg_freelist"
};

static char path[] = SCXCFG_LIB_PATH;

/*
 * Load scxcfg dynamic library. Returns instance of opened library,
 * and populates the api argument with the functions addresses exported
 * by the API.
 * Return 0 in case of success, -1 otherwise.
 */
int
scxcfg_load_module(void **instance, scxcfg_api_t *api)
{

	*instance = dlopen(path, RTLD_LAZY);
	if (*instance == NULL) {
		return (-1);
	}

	if ((api->scxcfg_open =
	    (scxcfg_open_t)dlsym(*instance, funcnames[0])) == NULL) {
		(void) scxcfg_unload_module(instance, api);
		return (-1);
	}
	if ((api->scxcfg_close =
	    (scxcfg_close_t)dlsym(*instance, funcnames[1])) == NULL) {
		(void) scxcfg_unload_module(instance, api);
		return (-1);
	}
	if ((api->scxcfg_getproperty_value =
	    (scxcfg_getproperty_value_t)dlsym(*instance,
	    funcnames[2])) == NULL) {
		(void) scxcfg_unload_module(instance, api);
		return (-1);
	}
	if ((api->scxcfg_getlistproperty_value =
	    (scxcfg_getlistproperty_value_t)dlsym(*instance,
	    funcnames[3])) == NULL) {
		(void) scxcfg_unload_module(instance, api);
		return (-1);
	}
	if ((api->scxcfg_setproperty_value =
	    (scxcfg_setproperty_value_t)dlsym(*instance,
	    funcnames[4])) == NULL) {
		(void) scxcfg_unload_module(instance, api);
		return (-1);
	}
	if ((api->scxcfg_delproperty =
	    (scxcfg_delproperty_t)dlsym(*instance,
	    funcnames[5])) == NULL) {
		(void) scxcfg_unload_module(instance, api);
		return (-1);
	}
	if ((api->scxcfg_get_local_nodeid =
	    (scxcfg_get_local_nodeid_t)dlsym(*instance,
	    funcnames[6])) == NULL) {
		(void) scxcfg_unload_module(instance, api);
		return (-1);
	}
	if ((api->scxcfg_get_local_nodename =
	    (scxcfg_get_local_nodename_t)dlsym(*instance,
	    funcnames[7])) == NULL) {
		(void) scxcfg_unload_module(instance, api);
		return (-1);
	}
	if ((api->scxcfg_get_ipaddress =
	    (scxcfg_get_ipaddress_t)dlsym(*instance,
	    funcnames[8])) == NULL) {
		(void) scxcfg_unload_module(instance, api);
		return (-1);
	}
	if ((api->scxcfg_get_nodeid =
	    (scxcfg_get_nodeid_t)dlsym(*instance,
	    funcnames[9])) == NULL) {
		(void) scxcfg_unload_module(instance, api);
		return (-1);
	}
	if ((api->scxcfg_get_nodename =
	    (scxcfg_get_nodename_t)dlsym(*instance,
	    funcnames[10])) == NULL) {
		(void) scxcfg_unload_module(instance, api);
		return (-1);
	}
	if ((api->scxcfg_del_node =
	    (scxcfg_del_node_t)dlsym(*instance,
	    funcnames[11])) == NULL) {
		(void) scxcfg_unload_module(instance, api);
		return (-1);
	}
	if ((api->scxcfg_isfarm =
	    (scxcfg_isfarm_t)dlsym(*instance,
	    funcnames[12])) == NULL) {
		(void) scxcfg_unload_module(instance, api);
		return (-1);
	}
	if ((api->scxcfg_isfarm_nodeid =
	    (scxcfg_isfarm_nodeid_t)dlsym(*instance,
	    funcnames[13])) == NULL) {
		(void) scxcfg_unload_module(instance, api);
		return (-1);
	}
	if ((api->scxcfg_add_node =
	    (scxcfg_add_node_t)dlsym(*instance,
	    funcnames[14])) == NULL) {
		(void) scxcfg_unload_module(instance, api);
		return (-1);
	}
	if ((api->scxcfg_lookup_node_property_value =
	    (scxcfg_lookup_node_property_value_t)dlsym(*instance,
	    funcnames[15])) == NULL) {
		(void) scxcfg_unload_module(instance, api);
		return (-1);
	}
	if ((api->scxcfg_freelist =
	    (scxcfg_freelist_t)dlsym(*instance,
	    funcnames[16])) == NULL) {
		(void) scxcfg_unload_module(instance, api);
		return (-1);
	}

	return (0);
}

/*
 * Unload a scxcfg library instance.
 */
int
scxcfg_unload_module(void **instance, scxcfg_api_t *api)
{
	static scxcfg_api_t null_api;

	if (dlclose(*instance) != 0) {
		return (-1);
	}

	*instance = NULL;
	*api = null_api;

	return (0);
}
