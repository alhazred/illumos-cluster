#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)Makefile.com	1.16	09/04/20 SMI"
#
# lib/libhaip/Makefile.com
#

LIBRARY= libhaip.a
VERS= .1

OBJECTS = haip_util.o haip_ipmp.o haip_safe_wait.o

# include library definitions
include $(SRC)/lib/Makefile.lib

SRCS =		$(OBJECTS:%.o=../common/%.c)

MTFLAG = -mt

CPPFLAGS += $(MTFLAG) -K PIC
$(EUROPA)CPPFLAGS += -DEUROPA

LDLIBS += -lscha -lc -lnsl -ldsdev
LDLIBS += -lsocket

LINTFILES += $(OBJECTS:%.o=%.ln)

.KEEP_STATE:

all: $(LIBRARY)

# include library targets
include $(SRC)/lib/Makefile.targ

objs/%.o pics/%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<
