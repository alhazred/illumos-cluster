/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * haip_util.c - Various utilities for network resource
 */

#pragma ident	"@(#)haip_util.c	1.40	09/02/02 SMI"

#include <sys/os.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <errno.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/modctl.h>
#include <sys/stream.h>
#include <sys/stropts.h>
#include <sys/fcntl.h>
#include <sys/wait.h>
#include <libintl.h>
#include <sys/stat.h>
#ifdef linux
#include <sys/param.h>
#endif
#include <libgen.h>
#include <sys/cladm_int.h>
#include <sys/clconf_int.h>

#include <rgm/scha_strings.h>

#include <rgm/haip.h>
#include <rgm/libdsdev.h>

/*
 * This flag tells the library whether to print errs
 * to stderr too or not. Only the functions called
 * (directly or indirectly) from VALIDATE callback pay
 * attention to this flag.
 */
boolean_t print_errors = B_FALSE;

/*
 * haip_rs_get()
 * Wrapper around scha_resource_get()
 *
 * The return value from the function is an INTEGER which indicates either
 * SUCCESS (HAIP_OK)  or the call needs to be retried (HAIP_RETRY)  or there
 * was some other error (HAIP_ERROR) while retreiving the required resource
 * property.
 *
 * On Success, the IN/OUT parameter will point to the structure of type
 * scha_ectprop_value_t that contains the information about the property
 * that was requested.  If there was an error, then the IN/OUT parameter
 * is pointing to NULL so that approppriate action can be takedn by the
 * called after looking at the integer value that is returned by the function.
 *
 */

int
haip_rs_get(const char *zone, scha_resource_t rs_handle, char *propname,
	scha_extprop_value_t **ip_prop)
{
	scha_err_t		rc;
	scha_extprop_value_t	*extprop = NULL;

	if (rs_handle == NULL) {
		char	internal_err_str[SCDS_ARRAY_SIZE];

		(void) snprintf(internal_err_str,
		    sizeof (internal_err_str),
		    "Can't retrieve the resource property: "
		    "Null resource handle");

		/* ignore err return code above */
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		    internal_err_str);
		ip_prop = NULL;
		return (HAIP_ERROR);
	}

	rc = scha_resource_get_zone
	    (zone, rs_handle, SCHA_EXTENSION, propname, &extprop);
	if (rc == SCHA_ERR_SEQID) {
		ip_prop = NULL;
		return (HAIP_RETRY);
	} else if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the resource property %s: %s.",
		    propname, scds_error_string(rc));
		ip_prop = NULL;
		return (HAIP_ERROR);
	}
	*ip_prop = extprop;
	return (HAIP_OK);
}

/*
 * Utilities to serialize running of same method of
 * this RT on the same ipmpgroup for multiple resources.
 * We don't want them stepping over each other.
 * The locks are achieved by obtaining file lock on
 * a lock file.  There are three types of locks:
 * - global lock for all IPMP
 * - per IPMP basis (only in INIT/BOOT/FINI)
 * - per ipmpgroup basis (we are only touching lo0)
 * Waits for a lock to be released.
 * Returns an id (basically just the descriptor),
 * to be used in the unlock calls.
 */

#define	LOCK_MSG_HELD  "Automatically created, do not touch. pid %d\n"
#define	LOCK_MSG_UNHELD  "Automatically created, do not touch. Lock Not Held\n"

int
haip_serialize_lock(char *ipmpgroup)
{
	char	lockfilename[PATH_MAX];
	char	msg[512];
	flock_t	fl;
	int	lockfd;
	char    internal_err_str[SCDS_ARRAY_SIZE];

	if (mkdirp(LOCK_DIR, 0755) != 0) { /* CSTYLED */
		if (errno != EEXIST) {	/*lint !e746 */
			/*
			 * SCMSGS
			 * @explanation
			 * This network resource failed to create a directory
			 * in which to store lock files. These lock files are
			 * needed to serialize the running of the same
			 * callback method on the same adapter for multiple
			 * resources.
			 * @user_action
			 * This might be the result of a lack of system
			 * resources. Check whether the system is low in
			 * memory and take appropriate action. For specific
			 * error information, check the syslog message.
			 */
			scds_syslog(LOG_ERR,
			    "Failed to create lock directory %s: %s.",
			    LOCK_DIR, strerror(errno));
			return (-1);
		}
	}
	(void) sprintf(lockfilename, "%s/%s.%s", LOCK_DIR, LOCK_FILE,
	    ipmpgroup);
	lockfd = open(lockfilename, O_RDWR | O_CREAT, 0755);
	if (lockfd < 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * System has failed to open the specified file.
		 * @user_action
		 * Check whether the permissions are valid. This might be the
		 * result of a lack of system resources. Check whether the
		 * system is low in memory and take appropriate action. For
		 * specific error information, check the syslog message.
		 */
		scds_syslog(LOG_ERR,
		    "Could not open file %s: %s.",
		    lockfilename, strerror(errno));
		return (-1);
	}
	scds_syslog_debug(1,
	    "About to attempt getting lock");
	(void) memset((char *)&fl, 0, sizeof (fl));
	fl.l_type = F_WRLCK;
	if (fcntl(lockfd, F_SETLKW, &fl) != 0) {	/* Wait for lock */
		(void) sprintf(internal_err_str,
		    "fcntl call has failed to lock the file %s: %s",
		    lockfilename, strerror(errno));
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		(void) close(lockfd);
		lockfd = -1;
		return (-1);
	}
	(void) ftruncate(lockfd, (off_t)0);
	(void) sprintf(msg, LOCK_MSG_HELD, (int)getpid());
	(void) write(lockfd, msg, strlen(msg));
	scds_syslog_debug(1,
	    "Returning from lock() routine");
	return (lockfd);
}

/* ARGSUSED */

/*
 * Releases the serialization lock.
 */

int
haip_serialize_unlock(int lockfd)
{
	flock_t		fl;
	char		internal_err_str[SCDS_ARRAY_SIZE];

	if (lockfd < 0) {
		(void) snprintf(internal_err_str,
		    sizeof (internal_err_str),
		    "Failed to release the serialization lock: "
		    "Invalid lockid %d",
		    lockfd);

		/* ignore err return code above */
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		    internal_err_str);
		return (-1);
	}
	(void) ftruncate(lockfd, (off_t)0);
	(void) lseek(lockfd, 0L, SEEK_SET);
	(void) write(lockfd, LOCK_MSG_UNHELD, strlen(LOCK_MSG_UNHELD));
	scds_syslog_debug(1,
	    "Releasing serialization lock");
	(void) memset((char *)&fl, 0, sizeof (fl));
	fl.l_type = F_UNLCK;
	if (fcntl(lockfd, F_SETLKW, &fl) != 0) {
		(void) sprintf(internal_err_str,
		    "fcntl call has failed to unlock the file : %s",
		    strerror(errno));
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (-1);
	}
	(void) close(lockfd);
	return (0);
}


/*
 * haip_register_callback()
 * Registers callback with IPMP with the RG and RS names.
 */

int
haip_register_callback(char *rsname, char *rgname, char *zone,
char *ipmpgroup, char *mode, char *callback_prog)
{
	char	cmd[1024];
	char	tag[1024];
	char    internal_err_str[SCDS_ARRAY_SIZE];


	if (rsname == NULL || ipmpgroup == NULL ||
	    mode == NULL || callback_prog == NULL) {

		(void) snprintf(internal_err_str,
		    sizeof (internal_err_str),
		    "NULL argument in haip_register_callback() routine");

		/* ignore err return code above */
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		    internal_err_str);
		return (-1);
	}

	(void) sprintf(tag, "%s.%s", rsname, mode);
	if (zone == NULL) {
	(void) sprintf(cmd, "%s/%s %s %s %s",
	    CALLBACK_BASEDIR, callback_prog, mode, rgname, rsname);
	} else {
	(void) sprintf(cmd, "%s/%s %s %s %s %s",
	    CALLBACK_BASEDIR, callback_prog, mode, rgname, rsname, zone);
	}
	return (pnm_callback_reg(ipmpgroup, tag, cmd));
}

/* ARGSUSED */
int
haip_unregister_callback(char *rsname, char *ipmpgroup, char *mode)
{
	char	tag[1024];
	char    internal_err_str[SCDS_ARRAY_SIZE];

	if (rsname == NULL || ipmpgroup == NULL || mode == NULL) {
		(void) snprintf(internal_err_str,
		    sizeof (internal_err_str),
		    "NULL argument in haip_unregister_callback() routine");

		/* ignore err return code above */
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		    internal_err_str);
		return (-1);
	}

	(void) sprintf(tag, "%s.%s", rsname, mode);

	return (pnm_callback_unreg(ipmpgroup, tag));
}

/*
 * haip_fgetline() - borrowed with slight modification from
 *   /ws/hadf/1.3/usr/src/cmd/cmd-admin/lib/libhacommon/ha_misc.c
 *
 * Read a line and pass back a list of pointers to null terminated
 * strings.  Each of these strings is derived by replacing all white
 * space in the original line with NULLs (with one exception below).
 *
 * Since leading whitespace invalidates entries in /etc/nsswitch.conf,
 * any leading white-space is mapped to a single leading space when
 * keep_leading_whitespace is non-zero. Leading whitespace will be a
 * separate item in the items array.
 *
 * The items list itself is also terminated with a null.  The number
 * of items placed in the list is returned by the function.
 *
 * Newlines may be escaped with '\' if newlines_can_be_escaped is non-zero.
 * If a comment symbol is found (#), the remainder of the line is ignored.
 */
int
haip_fgetline(FILE *fp, char *buffer, int bufsize, char *items[], int nitems,
    int newlines_can_be_escaped, int keep_leading_whitespace)
{
	register char *s1;
	char *s2;
	char **i1, **i2;
	int c, eol = 0;

	bzero(buffer, (uint_t)bufsize * sizeof (char));
	bzero(items, (uint_t)nitems * sizeof (char *));

	s1 = buffer;
	s2 = buffer + bufsize;
	while (!eol && s1 < s2) {
		switch (c = getc(fp)) {
		case EOF:
			if (s1 == buffer)
				return (EOF);
			++eol;
			/* FALLTHROUGH */

		case '\n':
			if (newlines_can_be_escaped) {
				if (s1 > buffer && s1[-1] == '\\')
					--s1;
				else
					++eol;
			} else {
				++eol;
			}

			*s1++ = '\0';
			break;

		case '#':
			++eol;
			*s1++ = '\0';
			while ((c = getc(fp)) != '\n' && c != EOF)
				;
			break;

		case ' ':
		case '\t':
			if (s1 > buffer && s1[-1] != '\0')
				*s1++ = '\0';
			/* Leading white space can invalidate entries */
			else if (keep_leading_whitespace != 0 && s1 == buffer) {
				*s1++ = (char)c;
				if (s1 < s2)
					*s1++ = '\0';
			}

			break;

		default:
			*s1++ = (char)c;
			break;
		}
	}
	s2 = s1;
	s1 = buffer;
	while (s1 < s2 && !*s1)
		++s1;
	i1 = items;
	i2 = items + nitems - 1;
	while (i1 < i2 && s1 < s2) {
		*i1++ = s1;
		s1 = &s1[strlen(s1)];
		while (s1 < s2 && !*s1)
			++s1;
	}
	*i1 = (char *)0;

	return ((int)(i1 - items));
}


/*
 * Do a best-effort check if the lookup order for "hosts" is "files"
 * before name server. The entry for "cluster" is the only database
 * that can come before "files".  We (naively) ignore everything
 * in between '[' and ']'. Also, it is illegal to have a leading
 * whitespace character at the beginning of a line, so those lines are
 * skipped.
 *
 * This function returns B_TRUE if a "valid" entry was found for "hosts".
 * It returns B_FALSE otherwise, and prints an appropriate warning message.
 */
#define	MAXNSSLINELENGTH	1024
#define	MAXNSSENTRIES		30
#define	STRLENHOSTS		5	/* strlen("hosts") = 5 */
#define	STRLENCLUSTER		7	/* strlen("cluster") = 7 */
boolean_t
haip_check_nsswitch_hosts(void)
{
	static char buffer[MAXNSSLINELENGTH]; /* bzero'ed in fgetline() */

	char *items[MAXNSSENTRIES + 1]; /* NULL terminated */
	int nitems = (sizeof (items) / sizeof (char *));
	int rc;
	boolean_t valid = B_FALSE;
	int in_bracket = 0;
	char *b1 = NULL;
	char *b2 = buffer + sizeof (buffer);
	FILE *fp;

	if (print_errors)
		(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if ((fp = fopen(NSSWITCHFILE, "r")) == NULL) {
		scds_syslog(LOG_ERR,
		    "Could not open file %s: %s.",
		    NSSWITCHFILE, strerror(errno));
		goto finished;
	}

	/* newlines cannot be escaped, but keep leading whitespace */
	while ((rc = haip_fgetline(fp, buffer, (int)sizeof (buffer),
		items, nitems, 0, 1)) != EOF) {

		if (rc == 0)
			continue; /* empty line */

		/*
		 * "hosts" must be the very first entry in the line.
		 * The word is case sensitive.
		 * Leading whitespace invalidates the entry.
		 * The ":" can optionally have whitespace on both sides.
		 */
		if (strncmp("hosts", buffer, (size_t)STRLENHOSTS) == 0) {
			b1 = NULL;
			/* set b1 to the character after ':' */
			if (buffer[STRLENHOSTS] == ':')
				b1 = &buffer[STRLENHOSTS + 1];
			if (buffer[STRLENHOSTS] == '\0' &&
			    buffer[STRLENHOSTS + 1] == ':')
				b1 = &buffer[STRLENHOSTS + 2];
			if (b1 == NULL)
				continue; /* Not a valid "hosts" line */

			/*
			 * We are on a valid hosts line. Skip everything
			 * between '[' and ']'.  Otherwise, verify that
			 * "files" is the first or second entry. It can
			 * only be second if "cluster is first".
			 */
			for (/* b1 init'ed above */; b1 < b2; b1++) {
				if (in_bracket == 1) {
					if (*b1 == ']') {
						in_bracket = 0;
						continue;
					}
					if (*b1 == '[') {
						/* syntax error */
						/*
						 * SCMSGS
						 * @explanation
						 * Validation callback method
						 * has failed to validate the
						 * hostname list. There may be
						 * syntax error in the
						 * nsswitch.conf file.
						 * @user_action
						 * Check for the following
						 * syntax rules in the
						 * nsswitch.conf file. 1)
						 * Check if the lookup order
						 * for "hosts" has "files". 2)
						 * "cluster" is the only entry
						 * that can come before
						 * "files". 3) Everything in
						 * between '[' and ']' is
						 * ignored. 4) It is illegal
						 * to have any leading
						 * whitespace character at the
						 * beginning of the line;
						 * these lines are skipped.
						 *
						 * Correct the syntax in the
						 * nsswitch.conf file and try
						 * again.
						 */
						scds_syslog(LOG_ERR,
						    "Possible "
						    "syntax error in hosts "
						    "entry in %s.",
						    NSSWITCHFILE);
						if (print_errors) {
							(void) fprintf(stderr,
							    dgettext(
							    TEXT_DOMAIN,
							    "Possible syntax "
							    "error in hosts "
							    "entry in %s.\n"),
							    NSSWITCHFILE);
						}
						/*
						 * Bail out. We aren't experts
						 * at parsing this file, so
						 * don't try to second guess
						 * the error recovery
						 * algorithms.
						 */
						goto finished;
					}
					continue;
				}

				if (*b1 == '[') {
					in_bracket = 1;
					continue;
				}

				if (*b1 == '\0') {
					continue;
				}

				if (strcmp("files", b1) == 0) {
					valid = B_TRUE; /* success */
					goto finished;
				}

				if (strcmp("cluster", b1) == 0) {
					b1 = &b1[STRLENCLUSTER];
					continue;
				}

				/* invalid entry */
				goto finished;
			}
		}
	}

finished:
	if (fp != NULL)
		(void) fclose(fp);

	return (valid);
}

/*
 * helper function called by haip_check_hosts_in_file
 * Looks for an ip address (not string/hostname) of a particular
 * address family (v4 or v6) in the given file.
 * Returns B_TRUE if the ip address is in the file, B_FALSE otherwise
 * syslogs on errors (and returns B_FALSE)
 */
boolean_t
haip_check_local_addr_mapping(FILE *f, void *addr, sa_family_t family)
{
	static char buffer[(MAXALIASES + 1) * MAXHOSTNAMELEN];
	char *items[(MAXALIASES + 1) + 1]; /* NULL terminated */
	int nitems = (sizeof (items) / sizeof (char *));
	struct in_addr in;
	struct in6_addr in6;

	/* Should start looking from the begining of the file */
	rewind(f);

	/* newlines can be escaped, but discard leading whitespace */
	while (haip_fgetline(f, buffer, (int)sizeof (buffer),
	    items, nitems, 1, 0) != EOF) {
		/* Ignore lines with fewer than two elements */
		if (!items[0] || !items[1])
			continue;

		/* To avoid dependency on ascii representation, use inet_pton */
		switch (family) {
		case AF_INET: /* addr is a *in_addr */
			if ((inet_pton(AF_INET, items[0], &in) == 1) &&
			    (bcmp(&in, addr, sizeof (struct in_addr)) == 0))
				    return (B_TRUE);
			break;
		case AF_INET6: /* addr is a *in6_addr (it had better be!!) */
			if ((inet_pton(AF_INET6, items[0], &in6) == 1) &&
			    IN6_ARE_ADDR_EQUAL((struct in6_addr *)addr, &in6))
				    return (B_TRUE);
			break;
		default:
			/* We know nothing about this address family */
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", "Invalid "
			    "address family");
			return (B_FALSE);
		}
	}

	return (B_FALSE);
}

/*
 * helper function called by haip_check_hosts_in_file
 * Looks for a hostname in the given file.
 * Returns B_TRUE if it is in the file, B_FALSE otherwise
 */
boolean_t
haip_check_local_name_mapping(FILE *f, char *hostname)
{
	static char buffer[(MAXALIASES + 1) * MAXHOSTNAMELEN];
	char *items[(MAXALIASES + 1) + 1]; /* NULL terminated */
	int nitems = (sizeof (items) / sizeof (char *));
	int i, rc;

	/* Should start looking from the begining of the file */
	rewind(f);

	/* newlines can be escaped, but discard leading whitespace */
	while ((rc = haip_fgetline(f, buffer, (int)sizeof (buffer),
	    items, nitems, 1, 0)) != EOF) {
		/* Ignore lines with less than two items */
		if (!items[0] || !items[1])
			continue;

		/* Skip the IP address, which is items[0] */
		for (i = 1; i < rc; i++) {
			if (!strcasecmp(items[i], hostname)) {
				scds_syslog_debug(1, "Found local mapping "
				    "for host <%s>: <%s> maps to IP <%s>",
				    hostname, items[i], items[0]);
				return (B_TRUE);
			}
		}
	}

	return (B_FALSE);
}

/* Check if there is a mapping for the hosts or ips in the local files */
boolean_t
haip_check_hosts_in_files(scha_str_array_t *hlist)
{
	boolean_t rc = B_TRUE; /* everything is assumed to be OK by default */
	uint_t i;
	struct in6_addr a6;
	struct in_addr a4;
	FILE *hosts, *ipnodes;
	int err;

	if (print_errors)
		(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/* will need hosts and ipnodes files later */
	if ((hosts = fopen(HOSTSFILE, "r")) == NULL) {
		err = errno;
		scds_syslog(LOG_ERR, "Could not open file %s: %s.", HOSTSFILE,
		    strerror(err));
		if (print_errors) {
			(void) fprintf(stderr, dgettext(TEXT_DOMAIN,
			    "Could not open file %s: %s.\n"), HOSTSFILE,
			    dgettext(TEXT_DOMAIN, strerror(err)));
		}
		return (B_FALSE);
	}
	if ((ipnodes = fopen(IPNODESFILE, "r")) == NULL) {
		err = errno;
		scds_syslog(LOG_ERR, "Could not open file %s: %s.", IPNODESFILE,
		    strerror(err));
		if (print_errors) {
			(void) fprintf(stderr, dgettext(TEXT_DOMAIN,
			    "Could not open file %s: %s.\n"), HOSTSFILE,
			    dgettext(TEXT_DOMAIN, strerror(err)));
		}
		(void) fclose(hosts);
		return (B_FALSE);
	}

	for (i = 0; i < hlist->array_cnt; i++) {
		if (inet_pton(AF_INET6, hlist->str_array[i], &a6) == 1) {
			/*
			 * We have an IPv6 address as a string, this should be
			 * in the ipnodes file
			 */
			if (haip_check_local_addr_mapping(ipnodes, &a6,
			    AF_INET6) == B_FALSE) {
				rc = B_FALSE;
				/*
				 * SCMSGS
				 * @explanation
				 * No mapping was found in the local hosts
				 * file for the specified ip address.
				 * @user_action
				 * Applications might use hostnames instead of
				 * ip addresses. It is recommended to have a
				 * mapping in the hosts file. Add an entry in
				 * the hosts file for the specified ip
				 * address.
				 */
				scds_syslog(LOG_ERR, "Could not find a "
				    "mapping for %s in %s. It is recommended "
				    "that a mapping for %s be added to %s.",
				    hlist->str_array[i], IPNODESFILE,
				    hlist->str_array[i], IPNODESFILE);
				if (print_errors) {
					(void) fprintf(stderr, dgettext(
					    TEXT_DOMAIN, "Could not find a "
					    "mapping for %s in %s. It is "
					    "recommended that a mapping for %s "
					    "be added to %s.\n"),
					    hlist->str_array[i], IPNODESFILE,
					    hlist->str_array[i], IPNODESFILE);
				}
			}
		} else if (inet_pton(AF_INET, hlist->str_array[i], &a4) == 1) {
			/*
			 * We have an IPv4 address as a string, this should be
			 * in the ipnodes file (preferably) or in the hosts file
			 */
			if ((haip_check_local_addr_mapping(ipnodes, &a4,
			    AF_INET) == B_FALSE) &&
			    (haip_check_local_addr_mapping(hosts, &a4,
			    AF_INET) == B_FALSE)) {
				rc = B_FALSE;
				/*
				 * SCMSGS
				 * @explanation
				 * No mapping was found in the local hosts or
				 * ipnodes file for the specified ip address.
				 * @user_action
				 * Applications might use hostnames instead of
				 * ip addresses. It is recommended to have a
				 * mapping in the hosts and/or ipnodes file.
				 * Add an entry in the hosts and/or ipnodes
				 * file for the specified ip address.
				 */
				scds_syslog(LOG_ERR, "Could not find a "
				    "mapping for %s in %s or %s. It is "
				    "recommended that a mapping for %s be "
				    "added to %s.",
				    hlist->str_array[i], IPNODESFILE,
				    HOSTSFILE, hlist->str_array[i],
				    IPNODESFILE);
				if (print_errors) {
					(void) fprintf(stderr, dgettext(
					    TEXT_DOMAIN, "Could not find a "
					    "mapping for %s in %s or %s. It is "
					    "recommended that a mapping for %s "
					    "be added to %s.\n"),
					    hlist->str_array[i], IPNODESFILE,
					    HOSTSFILE, hlist->str_array[i],
					    IPNODESFILE);
				}
			}
		} else {
			/* this is a hostname, locate it in either file */
			if ((haip_check_local_name_mapping(ipnodes,
			    hlist->str_array[i]) == B_FALSE) &&
			    (haip_check_local_name_mapping(hosts,
			    hlist->str_array[i]) == B_FALSE)) {
				rc = B_FALSE;
				scds_syslog(LOG_ERR, "Could not find a "
				    "mapping for %s in %s or %s. It is "
				    "recommended that a mapping for %s be "
				    "added to %s.",
				    hlist->str_array[i], IPNODESFILE,
				    HOSTSFILE, hlist->str_array[i],
				    IPNODESFILE);
				if (print_errors) {
					(void) fprintf(stderr, dgettext(
					    TEXT_DOMAIN, "Could not find a "
					    "mapping for %s in %s or %s. It is "
					    "recommended that a mapping for %s "
					    "be added to %s.\n"),
					    hlist->str_array[i], IPNODESFILE,
					    HOSTSFILE, hlist->str_array[i],
					    IPNODESFILE);
				}
			}
		}
	}

	(void) fclose(hosts);
	(void) fclose(ipnodes);

	return (rc);
}

/*
 * Is the RG local or not.
 * We get that by making sure that the
 * RG State on the local node is ONLINE
 * Returns B_FALSE for errors.
 */

boolean_t
haip_rg_local(const char *zone, char *rgname)
{
	scha_err_t	rc;
	scha_resourcegroup_t	rg_handle;
	scha_rgstate_t			rg_state;
	char	*local_node = NULL;
	scha_cluster_t	clust_handle;
	scha_str_array_t	*nodelist;
	uint_t	i = 0;
	char 	*ptr = NULL;

	/* Get the local node name */
retry_cluster:
	rc = scha_cluster_open_zone(zone, &clust_handle);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the cluster handle : %s.",
		    scds_error_string(rc));
		return (B_FALSE);
	}

	rc = scha_cluster_get_zone(zone, clust_handle, SCHA_NODENAME_LOCAL,
	    &local_node);
	if (rc == SCHA_ERR_SEQID) {
		scds_syslog(LOG_INFO,
		    "Retrying to retrieve the cluster information.");
		/* close the cluster handle */
		(void) scha_cluster_close(clust_handle);
		clust_handle = NULL;
		(void) sleep(1);
		goto retry_cluster;
	} else if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the property %s: %s.",
		    SCHA_NODENAME_LOCAL, scds_error_string(rc));
		(void) scha_cluster_close(clust_handle);
		return (B_FALSE);
	}

retry_rg:
	rc = scha_resourcegroup_open_zone(zone, rgname, &rg_handle);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the resource group handle: %s.",
		    scds_error_string(rc));
		(void) scha_cluster_close(clust_handle);
		clust_handle = NULL;
		return (B_FALSE);
	}

	/* Get the nodelist of the RG */
	rc = scha_resourcegroup_get_zone
	    (zone, rg_handle, SCHA_NODELIST, &nodelist);
	if (rc == SCHA_ERR_SEQID) {
		scds_syslog(LOG_INFO,
		    "Retrying to retrieve the resource group information.");
		/* close the resource group handle */
		(void) scha_resourcegroup_close(rg_handle);
		rg_handle = NULL;
		(void) sleep(1);
		goto retry_rg;
	} else if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the resource group property %s: %s.",
		    SCHA_NODELIST, scds_error_string(rc));
		(void) scha_resourcegroup_close(rg_handle);
		rg_handle = NULL;
		(void) scha_cluster_close(clust_handle);
		clust_handle = NULL;
		return (B_FALSE);
	}

	if (nodelist == NULL) {
		scds_syslog(LOG_ERR,
		    "Resource group nodelist is empty.");
		(void) scha_resourcegroup_close(rg_handle);
		rg_handle = NULL;
		(void) scha_cluster_close(clust_handle);
		clust_handle = NULL;
		return (B_FALSE);
	}

	for (i = 0; i < nodelist->array_cnt; i++) {
		/* Check whether this is the local node */
		ptr = strstr(nodelist->str_array[i], local_node);
		if (ptr == NULL)
			continue;

		/* Local node found, get the state of RG on this node */
		rc = scha_resourcegroup_get_zone
		    (zone, rg_handle, SCHA_RG_STATE_NODE,
		    nodelist->str_array[i], &rg_state);

		if (rc == SCHA_ERR_SEQID) {
			scds_syslog(LOG_INFO,
			    "Retrying to retrieve the resource group "
			    "information.");
			/* close the resource group handle and try again */
			(void) scha_resourcegroup_close(rg_handle);
			rg_handle = NULL;
			goto retry_rg;
		}

		if (rc != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR,
			    "Failed to retrieve the resource group property"
				" %s: %s.",
			    SCHA_RG_STATE,
				scds_error_string(rc));
			(void) scha_resourcegroup_close(rg_handle);
			rg_handle = NULL;
			(void) scha_cluster_close(clust_handle);
			clust_handle = NULL;
			return (B_FALSE);
		}
		(void) scha_resourcegroup_close(rg_handle);
		rg_handle = NULL;
		(void) scha_cluster_close(clust_handle);
		clust_handle = NULL;
		scds_syslog_debug(1, "RG state is <%d>\n", rg_state);
		if (rg_state == SCHA_RGSTATE_ONLINE) {
			return (B_TRUE);
		}
	}

	/*
	 * Consider any other state to mean that RG is not
	 * on line on this node.
	 */
	return (B_FALSE);
}

/*
 * haip_start_retry()
 * Starts the specified program under PMF tag
 * <rgname>.<rname>.<ipmpgroup>.failover
 * Command line options <rgname> <rname> <ipmpgroup>
 * are passed to it in that order.
 * Returns 0 for success, a positive number if the
 * command is already running (PMF tag busy), negative
 * for other errors.
 */

int
haip_start_retry(char *cmd, char *rg, char *r, char *ipmp, const char *zone)
{
	char	pmf_tag[MAXPATHLEN];
	char	pmf_cmd[MAXPATHLEN];
	uint_t		rc;
	char    internal_err_str[SCDS_ARRAY_SIZE];

	(void) sprintf(pmf_tag, "%s.%s.%s.failover", rg, r, ipmp);
	(void) sprintf(pmf_cmd, "/usr/cluster/bin/pmfadm -q %s", pmf_tag);
	rc = (uint_t)system(pmf_cmd);
	if (WIFEXITED(rc)) {
		if ((rc = WEXITSTATUS(rc)) == 0) {
			scds_syslog_debug(1, "PMF tag <%s> busy", pmf_tag);
			return (1);		/* Already running */
		}
	} else {
		return (-1);	/* Other problems */
	}
	/* launch the specified command under PMF */
	if (zone == NULL) {
	(void) sprintf(pmf_cmd, "/usr/cluster/bin/pmfadm -c %s %s %s %s %s",
		pmf_tag, cmd, rg, r, ipmp);
	} else {
	(void) sprintf(pmf_cmd, "/usr/cluster/bin/pmfadm -c %s %s %s %s %s %s",
		pmf_tag, cmd, rg, r, ipmp, zone);
	}

	rc = (uint_t)system(pmf_cmd);
	if (WIFEXITED(rc)) {
		if ((rc = WEXITSTATUS(rc)) == 0) {
			return (0);		/* Success */
		}

		/*
		 * haip_start_retry is called from multiple unrelated processes
		 * raising chance of race condition. In that case previous
		 * "pmfadm -q" (which checks for the existence of
		 * the pmftag) might pass for both the callers while
		 * "pmfadm -c" (which starts the process identified by the
		 * pmftag) could pass for one and fail for other.
		 *
		 * That's the reason when "pmfadm -c" returns non-zero
		 * we try "pmfadm -q" once again, rather than
		 * returning -1 immediately. If it returns zero someone
		 * might have started the process in the mean time and we
		 * return 1. If it returns non-zero then there may be some other
		 * problem and we return -1.
		 */
		(void) sprintf(pmf_cmd, "/usr/cluster/bin/pmfadm -q %s",
		    pmf_tag);
		rc = (uint_t)system(pmf_cmd);
		if (WIFEXITED(rc)) {
			if ((rc = WEXITSTATUS(rc)) == 0) {
				scds_syslog_debug(1,
				    "PMF tag <%s> busy", pmf_tag);
				return (1);		/* Already running */
			}
		}
	}
	(void) sprintf(internal_err_str,
	    "Unable to launch failover utility: %s",
	    cmd);
	scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
	    internal_err_str);
	return (-1);
}

/*
 * haip_stop_retry()
 * Stops the specified program under PMF tag
 * <rgname>.<rname>.<ipmpgroup>.failover
 * Returns 0 for success, a positive number if the
 * command is already stopped, negative
 * for other errors.
 */

int
haip_stop_retry(char *rg, char *r, char *ipmp)
{
	char	pmf_tag[MAXPATHLEN];
	char	pmf_cmd[MAXPATHLEN];
	uint_t		rc;
	char    internal_err_str[SCDS_ARRAY_SIZE];

	(void) sprintf(pmf_tag, "%s.%s.%s.failover", rg, r, ipmp);
	(void) sprintf(pmf_cmd, "/usr/cluster/bin/pmfadm -q %s", pmf_tag);
	rc = (uint_t)system(pmf_cmd);
	if (WIFEXITED(rc)) {
		if ((rc = WEXITSTATUS(rc)) != 0) {
			scds_syslog_debug(1, "PMF tag <%s> non-existant",
				pmf_tag);
			return (1);		/* Already Stopped */
		}
	} else {
		return (-1);	/* Other problems */
	}
	/*
	 * Run pmfadm -s to stop monitoring it
	 * also kill with SIGTERM
	 */
	(void) sprintf(pmf_cmd, "/usr/cluster/bin/pmfadm -s %s %d",
		pmf_tag, SIGTERM);
	rc = (uint_t)system(pmf_cmd);
	if (WIFEXITED(rc)) {
		if ((rc = WEXITSTATUS(rc)) == 0) {
			return (0);		/* Success */
		}

		/*
		 * haip_stop_retry is called from multiple unrelated processes
		 * raising chance of race condition. For instance,
		 * "hafoip_montior_stop" and "hafoip_ipmp_callback" may call
		 * this function at the same if the public adapter fails and
		 * gets repaired quickly. In that case previous
		 * "pmfadm -q" (which checks for the existence of
		 * the pmftag) might pass for both the callers while
		 * "pmfadm -s" (which signals the process identified by the
		 * pmftag) could pass for one and fail for other.
		 *
		 * That's the reason when "pmfadm -s" returns non-zero
		 * we try "pmfadm -q" once again, rather than
		 * returning -1 immediately. If it returns non-zero
		 * someone might have stopped the process in the mean time and
		 * we return 1. If it returns 0 then there may be some other
		 * problem and we return -1.
		 */
		(void) sprintf(pmf_cmd, "/usr/cluster/bin/pmfadm -q %s",
		    pmf_tag);
		rc = (uint_t)system(pmf_cmd);
		if (WIFEXITED(rc)) {
			if ((rc = WEXITSTATUS(rc)) != 0) {
				scds_syslog_debug(1,
				    "PMF tag <%s> non-existant", pmf_tag);
				return (1);		/* Already Stopped */
			}
		}
	}
	(void) sprintf(internal_err_str,
	    "Unable to stop PMF tag: %s.",
	    pmf_tag);
	scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
	    internal_err_str);
	return (-1);
}

boolean_t
haip_is_node_in_rgnodelist(const char *cluster, scha_str_array_t *rgnodelist)
{
	char *nodename = NULL;
	int rc = 0;
	uint_t i;
	char err[SCDS_ARRAY_SIZE];
	char	internal_err_str[SCDS_ARRAY_SIZE];

	if (rgnodelist == NULL) {
		(void) snprintf(internal_err_str,
		    sizeof (internal_err_str),
		    "NULL argument in haip_is_node_in_rgnodelist() routine");

		/* ignore err return code above */
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		    internal_err_str);
		return (-1);
	}

	rc = scha_cluster_getnodename_zone(cluster, &nodename);
	if (nodename == NULL) {
		(void) snprintf(err, sizeof (err), "Failed to retrieve "
		    "nodename <%s>", scds_error_string(rc));
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", err);
		return (B_FALSE);
	}

	for (i = 0; i < rgnodelist->array_cnt; i++) {
		scds_syslog_debug(1, "RG Node <%s>", rgnodelist->str_array[i]);
		if (strcmp(nodename, rgnodelist->str_array[i]) == 0) {
			free(nodename);
			return (B_TRUE);
		}
	}
	return (B_FALSE);
}

/*
 * haip_check_dup()
 * Looks thru the list of IPs plumbed on the system
 * and compares with the IPs being added to the resource.
 * returns:
 * 0  : No IPs exist on the system
 * -1 : Otherwise
 * Also complains if the resource has no ip addresses
 */
#ifdef linux
int
haip_check_dup(scds_handle_t handle)
{
	int	s;
	uint_t	j;
	int rc = 0, i;
	struct ifconf ifc;
	struct ifreq *pifr;
	struct in_addr *p4 = NULL;
	struct in6_addr *p6 = NULL;
	struct in6_addr	ina;
	in6addr_list_t al = {NULL, 0};
	int numreqs;

	if (print_errors)
		(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	s = socket(AF_INET, SOCK_STREAM, 0);
	if (s < 0) {
		rc = errno;
		scds_syslog(LOG_ERR, "Failed to create socket: %s.",
			strerror(rc));
		if (print_errors) {
			(void) fprintf(stderr, dgettext(TEXT_DOMAIN,
				"Failed to create socket: %s.\n"),
				dgettext(TEXT_DOMAIN, strerror(rc)));
		}
		return (-1);
	}

	/* Get the number of interfaces */
	ifc.ifc_len = 0;
	if (ioctl(s, SIOCGIFCONF, &ifc) < 0) {
		rc = errno;
		scds_syslog(LOG_ERR,
			"%s operation failed : %s.",
			"SIOCGIFCONF", strerror(rc));
		if (print_errors) {
			(void) fprintf(stderr, dgettext(TEXT_DOMAIN,
				"%s operation failed : %s.\n"),
				"SIOCGIFCONF",
				dgettext(TEXT_DOMAIN, strerror(rc)));
		}
		rc = -1;
		goto finished;
	}
	numreqs = ifc.ifc_len / sizeof (struct ifreq);
	numreqs++;	/* assume 1 extra interface */

	ifc.ifc_len = sizeof (struct ifreq) * numreqs;
	ifc.ifc_buf = (char *)malloc(ifc.ifc_len);
	if (!ifc.ifc_buf) {
		scds_syslog(LOG_ERR, "No memory.");
		if (print_errors) {
			(void) fprintf(stderr, dgettext(TEXT_DOMAIN,
				"No memory.\n"));
		}
		rc = -1;
		goto finished;
	}

	if (ioctl(s, SIOCGIFCONF, &ifc) < 0) {
		rc = errno;
		scds_syslog(LOG_ERR,
			"%s operation failed : %s.",
			"SIOCGIFCONF", strerror(rc));
		if (print_errors) {
			(void) fprintf(stderr, dgettext(TEXT_DOMAIN,
				"%s operation failed : %s.\n"),
				"SIOCGIFCONF",
				dgettext(TEXT_DOMAIN, strerror(rc)));
		}
		rc = -1;
		goto finished;
	}

	scds_syslog_debug(DBG_LEVEL_LOW,
		"haip_check(): numifs = %d, ifc_len = %d",
		ifc.ifc_len/sizeof (struct ifreq), ifc.ifc_len);

	/*
	 * get a list of all addresses that this resource will handle.
	 * We do _not_ take the IPMP group's instances into account here
	 */
	if (haip_hostlist_to_addrlist(handle, &al, PNMV4MASK | PNMV6MASK)
	    != 0) {
		scds_syslog(LOG_ERR, "Failed to obtain list of IP addresses "
		    "for this resource");
		if (print_errors) {
			(void) fprintf(stderr, dgettext(TEXT_DOMAIN,
			    "Failed to obtain list of IP addresses for this "
			    "resource\n"));
		}
		rc = -1;
		/* al.addr will be set to NULL now, so the free() later is ok */
		goto finished;
	}

	/* should have atleast one IP address to manage */
	if (al.count == 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * There was a failure in obtaining a list of IP addresses for
		 * the hostnames in the resource. Messages logged immediately
		 * before this message may indicate what the exact problem is.
		 * @user_action
		 * Check the settings in /etc/nsswitch.conf and verify that
		 * the resolver is able to resolve the hostnames.
		 */
		scds_syslog(LOG_ERR, "None of the hostnames/IP addresses "
		    "specified can be managed by this resource.");
		if (print_errors) {
			(void) fprintf(stderr, dgettext(TEXT_DOMAIN,
			    "None of the hostnames/IP addresses "
			    "specified can be managed by this resource.\n"));
		}
		rc = -1;
		goto finished;
	}

	/* If no dups are detected by the loop that follows, rc will be 0 */
	rc = 0;

	/*
	 * make sure that any address that this resource will handle is
	 * not already plumbed on a network interface on this node
	 */
	for (i = 0; i < al.count; i++) {
		pifr = ifc.ifc_req;
		for (j = (uint_t)ifc.ifc_len; j;
			j -= sizeof (struct ifreq), pifr++) {
			switch (pifr->ifr_addr.sa_family) {
			case AF_INET:
				/*
				 * Cant compare apples to oranges. This v4
				 * address must be converted to a v4mapped
				 * address - thats what we got from
				 * haip_hostlist_to_addrlist earlier.
				 */
				p4 = &(((struct sockaddr_in *)&(pifr->
				    ifr_addr))->sin_addr);
				IN6_INADDR_TO_V4MAPPED(p4, &ina);
				break;
			case AF_INET6:
				p6 = &(((struct sockaddr_in6 *)&(pifr->
				    ifr_addr))->sin6_addr);
				bcopy(p6, &ina, sizeof (struct in6_addr));
				break;
			default:
				/* not v4, not v6 - what could this be?? */
				scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				    "Unknown address family");
				if (print_errors) {
					(void) fprintf(stderr,
					    dgettext(TEXT_DOMAIN,
					    "INTERNAL ERROR: %s.\n"),
					    dgettext(TEXT_DOMAIN,
					    "Invalid address family"));
				}
				rc = -1;
				goto finished; /* abort immediately */
			}

			if (IN6_ARE_ADDR_EQUAL(al.addr + i, &ina)) {
				/* this buf is good enough for ipv4 addrs too */
				char ipa[INET6_ADDRSTRLEN];
				const char *ipstr;

				ipstr = pifr->ifr_addr.sa_family == AF_INET ?
				    inet_ntop(AF_INET, p4, ipa, sizeof (ipa)) :
				    inet_ntop(AF_INET6, p6, ipa, sizeof (ipa));

				/*
				 * SCMSGS
				 * @explanation
				 * An IP address corresponding to a hostname
				 * in this resource is already plumbed on the
				 * system. You can not create a
				 * LogicalHostname or a SharedAddress resource
				 * with hostnames that map to that IP address.
				 * @user_action
				 * Unplumb the IP address or remove the
				 * hostname corresponding to that IP address
				 * from the list of hostnames in the
				 * SUNW.LogicalHostname or SUNW.SharedAddress
				 * resource.
				 */
				scds_syslog(LOG_ERR, "IP Address %s is "
				    "already plumbed.", ipstr == NULL ? "NULL" :
				    ipstr);
				if (print_errors) {
					(void) fprintf(stderr,
					    dgettext(TEXT_DOMAIN,
					    "IP Address %s is already "
					    "plumbed.\n"), ipstr == NULL ?
					    "NULL" : ipstr);
				}
				rc = -1;
				break;
			}
		}	/* Loop over all addresses plumbed on the system */
	}	/* Loop over all addresses in the resource config */

	/* If any dups were detected, rc would've been set to -1 */

finished:
	(void) close(s);
	if (al.addr)
		free(al.addr);
	if (ifc.ifc_buf)
		free(ifc.ifc_buf);
	return (rc);
}
#else
int
haip_check_dup(scds_handle_t handle)
{
	int	s;
	uint_t	j;
	int rc = 0, i;
	ulong_t numifs, len;
	struct lifconf lifc;
	struct lifreq *pifr;
	struct lifnum lifn;
	struct in_addr *p4 = NULL;
	struct in6_addr *p6 = NULL;
	struct in6_addr	ina;
	char	*ifbuf = NULL;
	in6addr_list_t al = {NULL, 0};

	if (print_errors)
		(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	s = socket(AF_INET6, SOCK_STREAM, 0);
	if (s < 0) {
		rc = errno;
		scds_syslog(LOG_ERR, "Failed to create socket: %s.",
			strerror(rc));
		if (print_errors) {
			(void) fprintf(stderr, dgettext(TEXT_DOMAIN,
				"Failed to create socket: %s.\n"),
				dgettext(TEXT_DOMAIN, strerror(rc)));
		}
		return (-1);
	}

	/* count the number of network interfaces on this node */
	lifn.lifn_family = AF_UNSPEC; /* AF_UNSPEC to count both v4 and v6 */
	lifn.lifn_flags  = LIFC_NOXMIT;

	/*
	 * Use LIFC_ALLZONES flag in the ioctl to retrieve the
	 * adapter instances that are plumbed for zones. This flag is
	 * available from Solaris 10 release.
	 */

#ifdef LIFC_ALLZONES
	lifn.lifn_flags |= LIFC_ALLZONES;
#endif

	if (ioctl(s, SIOCGLIFNUM, &lifn) < 0) {
		rc = errno;
		scds_syslog(LOG_ERR,
			"%s operation failed : %s.",
			"SIOCGLIFNUM", strerror(rc));
		if (print_errors) {
			(void) fprintf(stderr, dgettext(TEXT_DOMAIN,
				"%s operation failed : %s.\n"), "SIOCGLIFNUM",
				dgettext(TEXT_DOMAIN, strerror(rc)));
		}
		rc = -1;
		goto finished;
	}
	numifs = (uint_t)lifn.lifn_count;

	/*
	 * Allocate space for the interface list. Note that we
	 * allocate space for two extra elements, just in case
	 * an interface or two is added between SIOCGLIFNUM above
	 * and SIOCGLIFCONF below.
	 */
	len = (numifs + 2) * sizeof (struct lifreq);
	ifbuf = (char *)malloc(len);
	if (ifbuf == NULL) {
		scds_syslog(LOG_ERR, "No memory.");
		if (print_errors) {
			(void) fprintf(stderr, dgettext(TEXT_DOMAIN,
				"No memory.\n"));
		}
		rc = -1;
		goto finished;
	}

	/* get the interface configuration list for this node */
	lifc.lifc_family = AF_UNSPEC; /* AF_UNSPEC to get both v4 and v6 */
	lifc.lifc_flags = LIFC_NOXMIT;
	lifc.lifc_len = (int)len;
	lifc.lifc_buf = ifbuf;

#ifdef LIFC_ALLZONES
	lifc.lifc_flags |= LIFC_ALLZONES;
#endif

	if (ioctl(s, SIOCGLIFCONF, &lifc) < 0) {
		rc = errno;
		scds_syslog(LOG_ERR,
			"%s operation failed : %s.",
			"SIOCGLIFCONF", strerror(rc));
		if (print_errors) {
			(void) fprintf(stderr, dgettext(TEXT_DOMAIN,
				"%s operation failed : %s.\n"), "SIOCGLIFCONF",
				dgettext(TEXT_DOMAIN, strerror(rc)));
		}
		rc = -1;
		goto finished;
	}
	scds_syslog_debug(DBG_LEVEL_LOW,
		"haip_check(): numifs = %d, len = %d, lifc_len = %d",
		numifs, len, lifc.lifc_len);

	/*
	 * get a list of all addresses that this resource will handle.
	 * We do _not_ take the IPMP group's instances into account here
	 */
	if (haip_hostlist_to_addrlist(handle, &al, PNMV4MASK | PNMV6MASK)
	    != 0) {
		scds_syslog(LOG_ERR, "Failed to obtain list of IP addresses "
		    "for this resource");
		if (print_errors) {
			(void) fprintf(stderr, dgettext(TEXT_DOMAIN,
			    "Failed to obtain list of IP addresses for this "
			    "resource\n"));
		}
		rc = -1;
		/* al.addr will be set to NULL now, so the free() later is ok */
		goto finished;
	}

	/* should have atleast one IP address to manage */
	if (al.count == 0) {
		scds_syslog(LOG_ERR, "None of the hostnames/IP addresses "
		    "specified can be managed by this resource.");
		if (print_errors) {
			(void) fprintf(stderr, dgettext(TEXT_DOMAIN,
			    "None of the hostnames/IP addresses "
			    "specified can be managed by this resource.\n"));
		}
		rc = -1;
		goto finished;
	}

	/* If no dups are detected by the loop that follows, rc will be 0 */
	rc = 0;

	/*
	 * make sure that any address that this resource will handle is
	 * not already plumbed on a network interface on this node
	 */
	for (i = 0; i < al.count; i++) {
		pifr = lifc.lifc_req;
		for (j = (uint_t)lifc.lifc_len; j;
			j -= sizeof (struct lifreq), pifr++) {
			switch (pifr->lifr_addr.ss_family) {
			case AF_INET:
				/*
				 * Cant compare apples to oranges. This v4
				 * address must be converted to a v4mapped
				 * address - thats what we got from
				 * haip_hostlist_to_addrlist earlier.
				 */
				p4 = &(((struct sockaddr_in *)&(pifr->
				    lifr_addr))->sin_addr);
				IN6_INADDR_TO_V4MAPPED(p4, &ina);
				break;
			case AF_INET6:
				p6 = &(((struct sockaddr_in6 *)&(pifr->
				    lifr_addr))->sin6_addr);
				bcopy(p6, &ina, sizeof (struct in6_addr));
				break;
			default:
				/* not v4, not v6 - what could this be?? */
				scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				    "Unknown address family");
				if (print_errors) {
					(void) fprintf(stderr,
					    dgettext(TEXT_DOMAIN,
					    "INTERNAL ERROR: %s.\n"),
					    dgettext(TEXT_DOMAIN,
					    "Invalid address family"));
				}
				rc = -1;
				goto finished; /* abort immediately */
			}

			if (IN6_ARE_ADDR_EQUAL(al.addr + i, &ina)) {
				/* this buf is good enough for ipv4 addrs too */
				char ipa[INET6_ADDRSTRLEN];
				const char *ipstr;

				ipstr = pifr->lifr_addr.ss_family == AF_INET ?
				    inet_ntop(AF_INET, p4, ipa, sizeof (ipa)) :
				    inet_ntop(AF_INET6, p6, ipa, sizeof (ipa));

				scds_syslog(LOG_ERR, "IP Address %s is "
				    "already plumbed.", ipstr == NULL ? "NULL" :
				    ipstr);
				if (print_errors) {
					(void) fprintf(stderr,
					    dgettext(TEXT_DOMAIN,
					    "IP Address %s is already "
					    "plumbed.\n"), ipstr == NULL ?
					    "NULL" : ipstr);
				}
				rc = -1;
				break;
			}
		}	/* Loop over all addresses plumbed on the system */
	}	/* Loop over all addresses in the resource config */

	/* If any dups were detected, rc would've been set to -1 */

finished:
	(void) close(s);
	if (al.addr)
		free(al.addr);
	if (ifbuf)
		free(ifbuf);
	return (rc);
}
#endif

/*
 * Returns the exact name of the RT that corresponds to the
 * given resource and resource group. This is the exact
 * equivalent of scha_resource_get -O TYPE -R <rname> -G <rgname>
 *
 * Memory for the string is alloted by this function. It is
 * the caller's responsibility to free it after use.
 */
char *
haip_get_rtname(const char *cluster, char *rname, char *rgname)
{
	char *rtname, *rc;
	scha_resource_t rs_handle;
	scha_err_t e;

retry:
	e = scha_resource_open_zone(cluster, rname, rgname, &rs_handle);
	if (e != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the resource "
		    "handle: %s.", scds_error_string(e));
		return (NULL);
	}

	e = scha_resource_get_zone(cluster, rs_handle, SCHA_TYPE, &rtname);
	if (e == SCHA_ERR_SEQID) {
		(void) scha_resource_close(rs_handle);
		(void) sleep(1);
		goto retry;
	} else	if (e != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the resource "
		    "property %s: %s.", SCHA_TYPE, scds_error_string(e));
		return (NULL);
	}

	rc = strdup(rtname);

	(void) scha_resource_close(rs_handle);

	if (rc == NULL)
		scds_syslog(LOG_ERR, "Out of memory.");

	return (rc);
}

/*
 * Convert an array of strings to a string and each sub-string is
 * separated by a comma character
 */
char *
haip_strarray_to_str(scha_str_array_t *sa)
{
	char s[SCDS_ARRAY_SIZE];
	char tmp[SCDS_ARRAY_SIZE];
	char *ptr;
	uint_t i;

	if (sa == NULL || sa->array_cnt == 0) {
		ptr = strdup("NULL");
		return (ptr);
	}

	s[0] = '\0';
	for (i = 0; i < sa->array_cnt - 1; i++) {
		(void) sprintf(tmp, "%s,", sa->str_array[i]);
		(void) strcat(s, tmp);
	}

	(void) strcat(s, sa->str_array[i]);

	ptr = strdup(s);
	return (ptr);
}

/*
 * The return value from the function is an INTEGER which indicates either
 * SUCCESS (HAIP_OK)  or the call needs to be retried (HAIP_RETRY)  or there
 * was some other error (HAIP_ERROR) while retreiving the required resource
 * property.
 * On SUCCESS, a boolean value will be the OUT parameter.True if the RS is a
 * shared address. False otherwise.
 * This is the exact equivalent of scha_resource_get -O IS_SHARED_ADDRESS
 * -R <rsname>
 */
int
haip_is_shared_address(scha_resource_t rs_handle, char *rsname,
			boolean_t *ret)
{

	scha_err_t		err;
	boolean_t		bool_val = B_FALSE;

	if (rs_handle == NULL) {
		char    internal_err_str[SCDS_ARRAY_SIZE];

		(void) snprintf(internal_err_str,
		sizeof (internal_err_str),
		"Can't retrieve the resource: "
		"Null resource handle");

		/* ignore err return code above */
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		internal_err_str);
		return (HAIP_ERROR);
	}
	err = scha_resource_get(rs_handle, SCHA_IS_SHARED_ADDRESS,
				&bool_val);
	if (err == SCHA_ERR_SEQID) {
		return (HAIP_RETRY);
	} else	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the resource "
		    "property %s: %s.", SCHA_IS_SHARED_ADDRESS,
		    scds_error_string(err));
		return (HAIP_ERROR);
	}

	*ret = bool_val;
	return (HAIP_OK);
}

/*
 * The return value from the function is an INTEGER which indicates either
 * SUCCESS (HAIP_OK)  or the call needs to be retried (HAIP_RETRY)  or there
 * was some other error (HAIP_ERROR) while retreiving the required resource
 * property.
 * On SUCCESS, a boolean value will be the OUT parameter.True if the RS is a
 * logical hostname. False otherwise.
 * This is the exact equivalent of scha_resource_get -O IS_LOGICAL_HOSTNAME
 * -R <rsname>
 */
int
haip_is_logical_hostname(scha_resource_t rs_handle, char *rsname,
			boolean_t *ret)
{

	scha_err_t		err;
	boolean_t		bool_val = B_FALSE;

	if (rs_handle == NULL) {
		char    internal_err_str[SCDS_ARRAY_SIZE];

		(void) snprintf(internal_err_str,
		sizeof (internal_err_str),
		"Can't retrieve the resource: "
		"Null resource handle");

		/* ignore err return code above */
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		internal_err_str);
		return (HAIP_ERROR);
	}
	err = scha_resource_get(rs_handle, SCHA_IS_LOGICAL_HOSTNAME,
				&bool_val);
	if (err == SCHA_ERR_SEQID) {
		return (HAIP_RETRY);
	} else	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the resource "
		    "property %s: %s.", SCHA_IS_LOGICAL_HOSTNAME,
		    scds_error_string(err));
		return (HAIP_ERROR);
	}
	*ret = bool_val;
	return (HAIP_OK);
}

/*
 * haip_validate_zone_adp()
 * verify the adapters in the ipmp group are authorized to
 * be used for this zone cluster. First we get the adapters
 * on this system and check whether this adapter belongs to
 * ipmp group to be used with this resource creation. If yes,
 * verify whether this adapter has been authorized to be used
 * on the zone cluster. The idea here is if atleast one of
 * the adapter from the ipmp group is authorized to be used
 * on the zone cluster, we continue creation of resource
 * and plumb the adapter on one of these adapters that's
 * visible in this zone. This is to preserver the security
 * aspect of the zones. Otherwise, we just exit here with error.
 *
 */
int
haip_validate_zone_adp(scds_handle_t handle, char *ipmp_group)
{
	int rc = 0;
	pnm_adapterinfo_t *adp_list = NULL, *p = NULL;
	int adp_auth = B_FALSE;

	/* Get public adapter list */
	if ((rc = pnm_adapterinfo_list(&adp_list, B_FALSE)) != 0) {
		scds_syslog(LOG_ERR, "Failed to validate the "
		    "accessibility of the adapters in the zone "
		    "cluster.");
		(void) fprintf(stderr, gettext("Failed to validate the "
		    "accessibility of the adapters in the zone "
		    "cluster.\n"));
		return (rc);
	}
	/* Collect the adapters belonging to this group */
	for (p = adp_list; p != NULL; p = p->next) {
		if ((p->adp_group != NULL) &&
		    (strcmp(p->adp_group, ipmp_group) == 0)) {
			/*
			 * Verify whether this is authorized to
			 * be used on this zone cluster.
			 */
			if (scds_verify_adapter
			    (handle, p->adp_name) == SCHA_ERR_NOERR) {
				adp_auth = B_TRUE;
				break;
			}
		}
	}
	if (adp_auth != B_TRUE) {
		/*
		 * SCMSGS
		 * @explanation
		 * There is no adapter found on this system that is
		 * authorized to be used for this zone cluster.
		 * @user_action
		 * Use the clzonecluster(1M) command to configure
		 * the adapters to be used for this zone cluster and
		 * run the command to create the resource.
		 */
		scds_syslog(LOG_ERR, "Failed to validate the "
		    "accessibility of the adapters in the zone "
		    "cluster.");
		(void) fprintf(stderr, gettext("Failed to validate the "
		    "accessibility of the adapters in the zone "
		    "cluster.\n"));
		return (1);
	}
	return (0);
}
