/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * haip_ipmp.c - Various ipmp related utilities for network resource
 */

#pragma ident   "@(#)haip_ipmp.c 1.40     09/01/14 SMI"

#include <sys/os.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <errno.h>
#include <netdb.h>
#include <sys/wait.h>
#include <signal.h>
#include <fcntl.h>
#include <ctype.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/modctl.h>
#include <sys/stream.h>
#include <sys/stropts.h>
#include <sys/fcntl.h>
#include <syslog.h>
#include <unistd.h>
#ifdef linux
#include <sys/param.h>
#endif

#include <libgen.h>
#include <sys/cladm_int.h>
#include <sys/clconf_int.h>

#include <rgm/haip.h>
#include <rgm/libdsdev.h>

static char *
_haip_get_ipmp_group(scha_str_array_t *iftable, const char *myzone);

static int validate_unique_hostname(char *hostname, char *resname);
/*
 * haip_get_ipmp_group()
 * Returns the IPMP group to be used on this node.
 * The returned pointer points to static storage,
 * the caller should not attempt to free() it,
 * also, subsequent calls to this function would
 * overwrite the previous value.
 */
char *
haip_get_ipmp_group(scds_handle_t handle)
{
	scha_extprop_value_t *ift;
	int rc;
	char *rv = NULL;
	const char *zone = NULL;

	if (print_errors)
		(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if ((rc = scds_get_ext_property(handle, HAIP_IFTABLE,
	    SCHA_PTYPE_STRINGARRAY, &ift)) != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the resource "
		    "property %s: %s.", HAIP_IFTABLE, scds_error_string(rc));
		if (print_errors) {
			(void) fprintf(stderr, dgettext(TEXT_DOMAIN,
			    "Failed to retrieve the resource property %s: "
			    "%s.\n"), HAIP_IFTABLE,
			    dgettext(TEXT_DOMAIN, scds_error_string(rc)));
		}
		return (rv);
	}

	zone = scds_get_zone_name(handle);

	/* error will be logged if this call here fails */
	rv = _haip_get_ipmp_group(ift->val.val_strarray, zone);

	scds_free_ext_property(ift);
	return (rv);
}

/*
 * _haip_get_ipmp_group()
 * Returns the IPMP group to be used on this node.
 * The returned pointer points to static storage,
 * the caller should not attempt to free() it,
 * also, subsequent calls to this function would
 * overwrite the previous value.
 */
char *
_haip_get_ipmp_group(scha_str_array_t *iftable, const char *myzone)
{
	uint_t		i;
	static	char	ipmp_group[SCDS_ARRAY_SIZE];
	char		*p = NULL;
	char		*nodep = NULL, *zonep = NULL;
	char		buf[MAXHOSTNAMELEN];
	nodeid_t	mynodeid, node = 0;
	char		*mynodename = NULL;
	char		*s = NULL;
	scha_err_t	rc;
	scha_cluster_t	clust_handle;

	if (print_errors)
		(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	rc = scha_cluster_open(&clust_handle);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the cluster handle : %s.",
		    scds_error_string(rc));
		if (print_errors) {
			(void) fprintf(stderr, dgettext(TEXT_DOMAIN,
				"Failed to retrieve the cluster handle : "
				"%s.\n"), dgettext(TEXT_DOMAIN,
				scds_error_string(rc)));
		}
		return (NULL);
	}

	rc = scha_cluster_get(clust_handle, SCHA_NODEID_LOCAL, &mynodeid);
	if (rc == SCHA_ERR_SEQID) {
		/*
		 * Just ignore the SCHA_ERR_SEQID error as NODEID will not
		 * change during the cluster reconfigurations.
		 */
		scds_syslog_debug(DBG_LEVEL_HIGH,
		    "Ignoring the SCHA_ERR_SEQID while retrieving %s",
		    SCHA_NODEID_LOCAL);
	} else if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the property %s: %s.",
		    SCHA_NODEID_LOCAL, scds_error_string(rc));
		if (print_errors) {
			(void) fprintf(stderr, dgettext(TEXT_DOMAIN,
				"Failed to retrieve the property %s: %s.\n"),
				SCHA_NODEID_LOCAL, dgettext(TEXT_DOMAIN,
				scds_error_string(rc)));
		}
		(void) scha_cluster_close(clust_handle);
		return (NULL);
	}

	rc = scha_cluster_get(clust_handle, SCHA_NODENAME_LOCAL, &mynodename);
	if (rc == SCHA_ERR_SEQID) {
		/*
		 * Just ignore the SCHA_ERR_SEQID error as NODENAME will not
		 * change during the cluster reconfigurations.
		 */
		scds_syslog_debug(DBG_LEVEL_HIGH,
		    "Ignoring the SCHA_ERR_SEQID while retrieving %s",
		    SCHA_NODENAME_LOCAL);
	} else if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the property %s: %s.",
		    SCHA_NODENAME_LOCAL, scds_error_string(rc));
		if (print_errors) {
			(void) fprintf(stderr, dgettext(TEXT_DOMAIN,
				"Failed to retrieve the property %s: %s.\n"),
				SCHA_NODENAME_LOCAL, dgettext(TEXT_DOMAIN,
				scds_error_string(rc)));
		}
		(void) scha_cluster_close(clust_handle);
		return (NULL);
	}

	for (i = 0; i < iftable->array_cnt; i++) {
		/* Guard against leading spaces */
		if (isspace(iftable->str_array[i][0])) {
			(void) scha_cluster_close(clust_handle);
			/*
			 * SCMSGS
			 * @explanation
			 * Failed to retrieve the ipmp information. The given
			 * IPMP group specification is invalid.
			 * @user_action
			 * Check whether the IPMP group specification is in
			 * the form of ipmpgroup&at;nodename. If not, recreate
			 * the resource with the properly formatted group
			 * information.
			 */
			scds_syslog(LOG_ERR,
			    "Malformed %s group specification %s.",
			    pnm_group_name(), iftable->str_array[i]);
			if (print_errors) {
				(void) fprintf(stderr, dgettext(TEXT_DOMAIN,
					"Malformed %s group specification "
					"%s.\n"), pnm_group_name(),
					iftable->str_array[i]);
			}
			return (NULL);
		}
		p = strchr(iftable->str_array[i], NODE_SEPARATOR);
		if (p == NULL) {
			(void) scha_cluster_close(clust_handle);
			scds_syslog(LOG_ERR,
			    "Malformed %s group specification %s.",
			    pnm_group_name(), iftable->str_array[i]);
			if (print_errors) {
				(void) fprintf(stderr, dgettext(TEXT_DOMAIN,
					"Malformed %s group specification "
					"%s.\n"), pnm_group_name(),
					iftable->str_array[i]);
			}
			return (NULL);
		}

		nodep = ++p;

		(void) strlcpy(buf, nodep, sizeof (buf));
		nodep = strtok_r(buf, ":", &zonep);

		for (s = nodep;  *s;  ++s)
			if (!isdigit(*s))
				break;
		if (*s == '\0') {
			node = (nodeid_t)atoi(nodep);
			if (node < 1 || (s && s[0])) {
				scds_syslog(LOG_ERR,
					"Malformed %s group "
					"specification %s.",
					pnm_group_name(),
					iftable->str_array[i]);
				if (print_errors) {
					(void) fprintf(stderr, dgettext
						(TEXT_DOMAIN,
						"Malformed %s group "
						"specification "
						"%s.\n"), pnm_group_name(),
						iftable->str_array[i]);
				}
				(void) scha_cluster_close(clust_handle);
				return (NULL);
			}
		} else {
			node = 0;
		}

		scds_syslog_debug(DBG_LEVEL_LOW,
			"Nodepart in the ipmp group is %s",
			nodep);

		if ((node == mynodeid || strcmp(nodep, mynodename) == 0) &&
			(myzone == NULL || zonep == NULL || zonep[0] == 0 ||
					strcmp(zonep, myzone) == 0)) {
			/*
			 * Now p is aligned just pass the @ sign, decrement it,
			 * so that "p - iftable->str_array[i]" becomes the
			 * size of the "ipmpX" string. Then we copy in that
			 * many bytes into the static var ipmpname[]
			 */
			p--;
			(void) strncpy(ipmp_group, iftable->str_array[i],
			    (ulong_t)(p - iftable->str_array[i]));
			scds_syslog_debug(DBG_LEVEL_LOW,
			    "Returning %s group %s",
			    pnm_group_name(), ipmp_group);

			(void) scha_cluster_close(clust_handle);
			return (ipmp_group);
		}
	}	/* for loop over iftable elements */

	/*
	* SCMSGS
	* @explanation
	* Netiflist property value does not have an IPMP group name
	* specified for the node.
	* @user_action
	* Update the netiflist property to include the IPMP group name
	* for the given node. Then, retry the resource group update
	* operation.
	*/
	scds_syslog(LOG_ERR, "%s group name is not found in the "
	    "netiflist property for node %s. Current netiflist value is %s.",
	    pnm_group_name(), mynodename, haip_strarray_to_str(iftable));

	if (print_errors) {
		(void) fprintf(stderr, dgettext(TEXT_DOMAIN,
		    "%s group name is not found in the netiflist "
		    "property for node %s. Current netiflist value is %s.\n"),
		    pnm_group_name(), mynodename,
		    haip_strarray_to_str(iftable));
	}

	(void) scha_cluster_close(clust_handle);
	return (NULL);
}


/*
 * Validate the group name presence in /etc/hostname.<adapter>
 */
static boolean_t
validate_ipmp_group_name(char *grp_name, pnm_adapter_list_t adp_list)
{
#ifdef linux
	return (B_TRUE);
#else
	const char *file_prefix = "/etc/hostname";
	const char *seperators = " \t\n\r";

	char *adapter_name = NULL;
	char *adps_head = strdup(adp_list);
	char *adps;
	int adapter_count = 0;

	char *key;
	char *str;
	char filename[30]; /* max filename length would not exceed 30 */
	char *filebuf = NULL;
	int filesize;
	int filed = -1;
	boolean_t search_result;
	boolean_t ret = B_TRUE;

	adps =  adps_head;
	while ((adapter_name = strtok_r(adps, ":", &adps)) != NULL) {
		adapter_count++;
		(void) sprintf(filename, "%s.%s", file_prefix, adapter_name);
		if ((filed = open(filename, O_RDONLY)) < 0) {
			ret = B_FALSE;
			break;
		}

		if ((filesize = lseek(filed, 0, SEEK_END)) < 0) {
			ret = B_FALSE;
			break;
		}
		(void) lseek(filed, 0, SEEK_SET);

		/*
		 * Since its a small file, we can afford to read it
		 * in memory and then perform the search
		 */
		filebuf = (char *)malloc((size_t)(filesize + 1) *
		    sizeof (char));
		if (filebuf == NULL) {
			/*
			 * SCMSGS
			 * @explanation
			 * The program ran out of memory.
			 * @user_action
			 * Determine why the user program ran out of memory.
			 * Contact your authorized Sun service provider for
			 * assistance in diagnosing the problem.
			 */
			scds_syslog(LOG_ERR, "Out of memory");
			ret = B_FALSE;
			break;
		}
		if (read(filed, filebuf, (size_t)filesize) < 0) {
			ret = B_FALSE;
			break;
		}
		filebuf[filesize] = '\0';

		search_result = B_FALSE;
		str = filebuf;
		while ((key = strtok_r(str, seperators, &str)) != NULL) {
			if (strcmp(key, "group") == 0) {
				key = strtok_r(str, seperators, &str);
				if (key != NULL &&
				    strcmp(key, grp_name) == 0) {
					search_result = B_TRUE;
					break;
				}
			}
		}

		free(filebuf);
		(void) close(filed);
		filebuf = NULL;
		filed = -1;

		if (search_result == B_FALSE) {
			ret = B_FALSE;
			break;
		}
	}

	if (adapter_count == 0) {
		ret = B_FALSE;
	}

	if (ret == B_FALSE) {
		if (filebuf != NULL) {
			free(filebuf);
		}
		if (filed != -1) {
			(void) close(filed);
		}

		/*
		 * SCMSGS
		 * @explanation
		 * An IP address resource (of type LogicalHostname or
		 * SharedAddress) has been mis-configured. The
		 * indicated IPMP group name is not found in the
		 * indicated file on this node.
		 * This will cause errors in any attempted operation
		 * upon the IP address resource. Additional related
		 * failure messages are likely to be found near this
		 * one in the syslog output.
		 * @user_action
		 * Check the configuration of network resources using
		 * the "clresourcegroup show -v +" command, and make
		 * any necessary corrections.
		 */
		scds_syslog(LOG_ERR, "Validation failed for IPMP "
		    "groups name <%s> : No entry found in <%s.%s>",
		    grp_name, file_prefix, adapter_name);
	}

	free(adps_head);

	return (ret);
#endif
}

/*
 * haip_is_group_valid()
 */
boolean_t
haip_is_group_valid(char *ipmpgroup)
{
	int	rc;
	pnm_status_t	st;
	boolean_t ret = B_TRUE;

	if (print_errors)
		(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	rc = pnm_group_status_direct(ipmpgroup, &st);

	if (rc != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		scds_syslog(LOG_ERR, "Failed to validate %s group name <%s> "
			"pnm errorcode <%d>.", pnm_group_name(), ipmpgroup, rc);
		if (print_errors) {
			(void) fprintf(stderr, dgettext(TEXT_DOMAIN,
				"Failed to validate %s group name <%s> "
				"pnm errorcode <%d>.\n"), pnm_group_name(),
				ipmpgroup, rc);
		}
		ret = B_FALSE;	/* No, this is not a IPMP group */
	} else {
		ret = validate_ipmp_group_name(ipmpgroup, st.backups);
	}

	/* free memory allocated by pnm_group_status_direct() */
	pnm_status_free(&st);

	return (ret);
}


/*
 * haip_ipmpstatus(): A wrapper around pnm_groupstatus
 * Returns
 * 0 for OK
 * >0 for "cant determine"
 * <0 for "Bad" i.e. DOWN. NOT MONITORING and NETWORK DOWN
 * Also returns a status string to the caller. [Caller
 * should provide storage, 32 bytes max]. This is for
 * easy debugging.
 */
int
haip_ipmpstatus(char *ipmpgroup, char *status_string)
{
	int		rc;
	pnm_status_t	st;

	rc = pnm_group_status_direct(ipmpgroup, &st);
	if (rc != 0) {
		(void) strcpy(status_string, "pnm_group_status() failure");
		return (1);
	}

	rc = st.status;
	(void) strcpy(status_string, pnm_status_str(st.status));
	pnm_status_free(&st);

	/* non-zero rc means error, we collapse all error codes to -1 */
	return (rc == 0 ? 0 : -1);
}


/*
 * haip_do_ipmp_op()
 * Performs an ifconfig style operation on a resource.
 * op is one of IF_UP, IF_DOWN, IF_PLUMB, IF_UNPLUMB
 *
 * XXX Right now the IPMP client RPC ipmp_ifconfig_1()
 * does not return the number of ipaddresses on which
 * the requested operation was successfully applied.
 * Modify the return value behaviour so that it returns
 * a more meaningful value [e.g. ALL/SOME/NONE].
 *
 * XXX Right now, the return code from the ipmp client
 * RPC is really weird, -1 if invalid ipmp group, +1 otherwise!!
 */

int
haip_do_ipmp_op(char *ipmpgroup, const char *zone, int op, in6addr_list_t al)
{
	int		lockid;
	int		rc = 0;

	lockid = haip_serialize_lock(ipmpgroup);

	if (al.count <= 0) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", "No addresses "
		    "to perform operation on");
		return (1); /* total failure */
	}

	rc = pnm_ifconfig_direct(ipmpgroup, zone, op, (uint_t)al.count,
	    al.addr);
	if (rc != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * An attempt to bring a network interface up or down has
		 * failed with the indicated error, while attempting to bring
		 * an IP address resource (of type LogicalHostname or
		 * SharedAddress) online or offline. Additional related
		 * failure messages are likely to be found following this one
		 * in the syslog output.
		 * @user_action
		 * Check the configuration of network resources using the
		 * "clresourcegroup show -v +" command, and make any necessary
		 * corrections to the network configuration.
		 */
		scds_syslog(LOG_ERR, "%s logical interface configuration "
			"operation failed with <%d>.", pnm_group_name(), rc);
	}
	if (rc < 0 && -rc == al.count) {
		/* all operations failed */
		rc = 1;
	}

	(void) haip_serialize_unlock(lockid);
	return (rc);
}

/*
 * haip_hostlist_to_addrlist()
 * Converts all scha_str_array_t list of hostnames/string-ipaddrs to a list
 * of in6_addrs. Space for the new list is dynamically allocated and the
 * caller should free it after its done with the list. The struct container
 * for the list is _not_ allocated for the caller.
 */
int
haip_hostlist_to_addrlist(scds_handle_t handle, in6addr_list_t *al,
    int flags)
{
	int f, err;
	uint_t i;
	struct hostent *hp;
	void *rp;
	char **p;
	scha_extprop_value_t *ift = NULL;
	int rc = 0, numips;

	if (print_errors)
		(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if ((rc = scds_get_ext_property(handle, HAIP_IPALIST,
	    SCHA_PTYPE_STRINGARRAY, &ift)) != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the resource "
		    "property %s: %s.", HAIP_IPALIST, scds_error_string(rc));
		if (print_errors) {
			(void) fprintf(stderr, dgettext(TEXT_DOMAIN,
			    "Failed to retrieve the resource property %s: "
			    "%s.\n"), HAIP_IPALIST,
			    dgettext(TEXT_DOMAIN, scds_error_string(rc)));
		}
		return (-1);
	}

	/* initialize the addresslist */
	al->count = 0;
	al->addr = NULL;

	/*
	 * Iterate over the whole list of hostnames. Look for *all* mappings
	 * for each hostname and then prune the list based on instances. We
	 * do the pruning after the lookup because we'd like to lookup a name
	 * in one getipnodebyname call and theres no way to lookup IPv4
	 * addresses only with address family set to AF_INET6
	 */
	for (i = 0; i < ift->val.val_strarray->array_cnt; i++) {
		/*
		 * lookup all mappings for the hostname. AI_ADDRCONFIG is a
		 * minor optimization - if no interface on this node has a
		 * particular type (v4 or v6) of address, then theres no way
		 * that pnm_get_instances_direct will ask us to deal with those
		 * types of addresses. Also helps on nodes of a scalable
		 * service that are only in the auxlist. On those nodes,
		 * all ips are plumbed on loopback. It makes sense to make
		 * that hostname->ip conversion based on what can be hosted.
		 */
		hp = getipnodebyname(ift->val.val_strarray->str_array[i],
		    AF_INET6, AI_V4MAPPED | AI_ALL | AI_ADDRCONFIG, &err);
		if (hp == NULL) {
			scds_syslog(LOG_ERR, "Hostname lookup failed for %s: "
			    "%s", ift->val.val_strarray->str_array[i],
			    hstrerror(err));
			if (print_errors) {
				(void) fprintf(stderr, dgettext(TEXT_DOMAIN,
				    "Hostname lookup failed for %s: %s\n"),
				    ift->val.val_strarray->str_array[i],
				    hstrerror(err));
			}
			rc = -1;
			goto finished;
		}

		/* we are only interested in these two bits */
		f = flags & (PNMV4MASK | PNMV6MASK);

		/*
		 * To keep track of the number of IPs for this host that
		 * are added to the main list. it should have a value of
		 * 1 or 2 after the following loop ends
		 */
		numips = 0;

		/*
		 * Iterate over all mappings of this hostname, selecting
		 * at most the first IPv4 and the first IPv6 resolution - but
		 * only if that bit is set in the instance flags.
		 */
		for (p = hp->h_addr_list; (*p != NULL) && f; p++) {
			/*
			 * As soon as a v4 or v6 address is found, we reset
			 * the bit in the flag to make sure that we do not
			 * look for any more address(es) of that type for this
			 * hostname.
			 */
			if ((f & PNMV4MASK) && IN6_IS_ADDR_V4MAPPED(
			    (struct in6_addr *)(*p))) {
				/* reset the v4 bit, we got one match */
				f &= ~PNMV4MASK;
			} else if ((f & PNMV6MASK) && !(IN6_IS_ADDR_V4MAPPED(
			    (struct in6_addr *)(*p)))) {
				/* reset the v6 bit, we got one match */
				f &= ~PNMV6MASK;
			} else
				continue; /* skip over this address */

			/* found an ip that should be added to the list */
			rp = realloc(al->addr, ((size_t)al->count + 1) *
			    sizeof (struct in6_addr));
			if (rp == NULL) {
				scds_syslog(LOG_ERR, "Out of memory.");
				if (print_errors) {
					(void) fprintf(stderr,
					    dgettext(TEXT_DOMAIN,
					    "Out of memory.\n"));
				}

				/* cleanup and return error */
				freehostent(hp);
				rc = -1;
				goto finished;
			}
			al->addr = rp;
			bcopy(*p, al->addr + al->count, sizeof (in6_addr_t));
			(al->count)++;
			numips++; /* added an IP for this host to the list */
		}

		if (numips == 0) {
			/*
			 * We traversed the whole list of addresses and not
			 * a single mapping that satisfies the group instances
			 * was found.
			 *
			 * During VALIDATE: this is OK because VALIDATE
			 * actually tries to create a list of addresses that
			 * the group *cant* host (to determine whether it
			 * should warn users that these will be ignored).
			 *
			 * For other callbacks: This is a fairly unusual
			 * situation - someone changed hosts/ipnodes? resolver
			 * problems?
			 */
			if (!print_errors) { /* log only if not in VALIDATE */
				/*
				 * SCMSGS
				 * @explanation
				 * The hostname does not have any IP addresses
				 * that can be hosted on the resource's IPMP
				 * group. This could happen if the hostname
				 * maps to an IPv6 address only and the IPMP
				 * group is capable of hosting IPv4 addresses
				 * only (or the other way round).
				 * @user_action
				 * Use ifconfig -a to determine if the network
				 * adapter in the resource's IPMP group can
				 * host IPv4, IPv6 or both kinds of addresses.
				 * Make sure that the hostname specified has
				 * atleast one IP address that can be hosted
				 * by the underlying IPMP group.
				 */
				scds_syslog(LOG_ERR, "Hostname %s has no "
				    "usable address.",
				    ift->val.val_strarray->str_array[i]);
			}

			/*
			 * Note that we continue here. haip_do_ipmp_op()
			 * _will_ fail if the whole list has 0 elements. And we
			 * dont want to fail here if only some hosts are causing
			 * problems.
			 */
		} else {
			scds_syslog_debug(1, "%d usable addresses "
			    "found for hostname %s", numips,
			    ift->val.val_strarray->str_array[i]);
		}

		freehostent(hp);
	}

finished:
	if (rc != 0) {
		free(al->addr); /* will only be NULL or a valid ptr */
		al->addr = NULL;
		al->count = 0;
	}

	scds_free_ext_property(ift);

	return (rc);
}

/*
 * haip_validate_hostnamelist()
 * Looks thru all hostnames managed by the resource
 * to make sure they look OK. hostnames specified are
 * validated using getipnodebyname().
 * Returns TRUE if ALL of them pass, FALSE otherwise.
 * XXX How to make sure they are all on same subnet?
 */
boolean_t
haip_validate_hostnamelist
(scds_handle_t handle, char *group, int instances)
{
	uint_t		i;
	int err;
	boolean_t	allok = B_TRUE;
	boolean_t	nsswitchok;
	boolean_t	allinfiles = B_TRUE;
	scha_extprop_value_t *ift;
	scha_str_array_t *ipaddrs;
	struct hostent *hp;
	int rc;
	const char *zone_name;

	if (print_errors)
		(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if ((rc = scds_get_ext_property(handle, HAIP_IPALIST,
	    SCHA_PTYPE_STRINGARRAY, &ift)) != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the resource "
		    "property %s: %s.", HAIP_IPALIST, scds_error_string(rc));
		if (print_errors) {
			(void) fprintf(stderr, dgettext(TEXT_DOMAIN,
			    "Failed to retrieve the resource property %s: "
			    "%s.\n"), HAIP_IPALIST,
			    dgettext(TEXT_DOMAIN, scds_error_string(rc)));
		}
		return (B_FALSE);
	}

	/* If the hostlist is empty invalidate the resource */
	ipaddrs = ift->val.val_strarray;
	if (ipaddrs == NULL || ipaddrs->array_cnt < 1) {
		/*
		 * SCMSGS
		 * @explanation
		 * An attempt was made to create a Network resource without
		 * specifying a hostname.
		 * @user_action
		 * At least one hostname must be specified by using the -h
		 * option to clreslogicalhostname or clressharedaddress.
		 */
		scds_syslog(LOG_ERR, "No hostnames specified.");
		if (print_errors) {
			(void) fprintf(stderr, dgettext(TEXT_DOMAIN,
				"No hostnames specified.\n"));
		}
		return (B_FALSE);
	}

	/*
	 * If the zone name is non-null, check whether the zone
	 * belongs to a zone cluster.
	 */
	if (((zone_name = scds_get_zone_name(handle)) != NULL) &&
	    (scds_is_zone_cluster(handle))) {
		/*
		 * This is a zone cluster.
		 * Verify whether the given hostnames are
		 * authorized to be used in this zone cluster
		 */
		for (i = 0; i < ipaddrs->array_cnt; i++) {
			rc = scds_verify_ip(handle, ipaddrs->str_array[i]);
			if (rc != 0) {
				/*
				 * SCMSGS
				 * @explanation
				 * Given hostname is not authorized to be used
				 * for this zone cluster.
				 * @user_action
				 * User the clzonecluster(1M) command to
				 * configure the hostnames to be used for this
				 * zone cluster and then rerun this command to
				 * create the resource.
				 */
				scds_syslog(LOG_ERR,
				    "The hostname %s is not authorized to "
				    "be used in this zone cluster %s.\n",
				    ipaddrs->str_array[i], zone_name);
				if (print_errors) {
					(void) fprintf(stderr,
					    dgettext(TEXT_DOMAIN,
					    "The hostname %s is not authorized "
					    "to be used in this zone cluster "
					    "%s.\n"),
					    ipaddrs->str_array[i],
					    zone_name);
				}
				return (B_FALSE);
			}
		}
	}

	/* Check if the settings in nsswitch.conf are as recommended */
	nsswitchok = haip_check_nsswitch_hosts();
	if (nsswitchok == B_TRUE) {
		scds_syslog_debug(DBG_LEVEL_LOW, "%s hosts line is as desired",
		    NSSWITCHFILE);
	} else {
		/*
		 * SCMSGS
		 * @explanation
		 * Validation callback method has failed to validate the
		 * hostname list. There may be syntax error in the
		 * nsswitch.conf file.
		 * @user_action
		 * Check for the following syntax rules in the nsswitch.conf
		 * file. 1) Check if the lookup order for "hosts" has "files".
		 * 2) "cluster" is the only entry that can come before
		 * "files". 3) Everything in between '[' and ']' is ignored.
		 * 4) It is illegal to have any leading whitespace character
		 * at the beginning of the line; these lines are skipped.
		 *
		 * Correct the syntax in the nsswitch.conf file and try again.
		 */
		scds_syslog(LOG_ERR,
		    "Could not validate the "
		    "settings in %s. It is recommended that the settings "
		    "for host lookup consult `files` before a name server.",
		    NSSWITCHFILE);
		if (print_errors) {
			(void) fprintf(stderr, dgettext(TEXT_DOMAIN,
				"Could not validate the settings in %s. It "
				"is recommended that the settings for host "
				"lookup consult `files` before a name "
				"server.\n"), NSSWITCHFILE);
		}
	}

	/* Check for entries in local files */
	allinfiles = haip_check_hosts_in_files(ipaddrs);

	for (i = 0; i < ipaddrs->array_cnt; i++) {
		/*
		 * Make sure the address can be resolved by our mechanism.
		 * IPMP group instances enters the picture here:
		 * If the group can host both IPv4 and IPv6 addresses,
		 * 	any kind of resolution will do.
		 * If the group can host IPv4 addresses only,
		 * 	the hostname had better have an IPv4 mapping
		 * If the group can host IPv6 addresses only,
		 * 	the hostname had better have an IPv6 mapping
		 * Ofcourse if the hostname has both types of mappings and
		 * the group can host only one type, only a warning is
		 * issued (that has already been done) and the mapping
		 * incompatible with the group instance is ignored.
		 */
		if ((instances & PNMV4MASK) && (instances & PNMV6MASK)) {
			/* group can host both types, do a catchall lookup  */
			hp = getipnodebyname(ipaddrs->str_array[i],
			    AF_INET6, AI_V4MAPPED | AI_ALL, &err);
			if (hp == NULL) {
				allok = B_FALSE;
				/*
				 * SCMSGS
				 * @explanation
				 * Validation method has failed to validate
				 * the ip addresses.
				 * @user_action
				 * Invalid hostnames/ip addresses have been
				 * specified while creating resource. Recreate
				 * the resource with valid hostnames. Check
				 * the syslog message for the specific
				 * information.
				 */
				scds_syslog(LOG_ERR, "Host %s is not valid.",
				    (ipaddrs->str_array[i] == NULL ?  "NULL" :
				    ipaddrs->str_array[i]));
				if (print_errors) {
					(void) fprintf(stderr, dgettext(
					    TEXT_DOMAIN,
					    "Host %s is not valid.\n"),
					    (ipaddrs->str_array[i] == NULL ?
					    "NULL" : ipaddrs->str_array[i]));
				}
			} else
				freehostent(hp);
		} else if (instances & PNMV4MASK) {
			/* group can host v4 addresses only */
			hp = getipnodebyname(ipaddrs->str_array[i],
			    AF_INET, 0, &err);
			if (hp == NULL) {
				allok = B_FALSE;
				/*
				 * SCMSGS
				 * @explanation
				 * The IPMP group can not host IP addresses of
				 * the type that the hostname maps to. eg if
				 * the hostname maps to IPv6 address(es) only
				 * and the IPMP group is setup to host IPv4
				 * addresses only.
				 * @user_action
				 * Use ifconfig -a to determine if the network
				 * adapter in the resource's IPMP group can
				 * host IPv4, IPv6 or both kinds of addresses.
				 * Make sure that the hostname specified has
				 * atleast one IP address that can be hosted
				 * by the underlying IPMP group.
				 */
				scds_syslog(LOG_ERR, "%s group %s can host "
				    "%s addresses only. Host %s has no mapping "
				    "of this type.", pnm_group_name(),
				    group, "IPv4",
				    (ipaddrs->str_array[i] == NULL ?
				    "NULL" : ipaddrs->str_array[i]));
				if (print_errors) {
					(void) fprintf(stderr, dgettext(
					    TEXT_DOMAIN, "%s group %s can "
					    "host %s addresses only. Host %s "
					    "has no mapping of this type.\n"),
					    pnm_group_name(),
					    group, "IPv4",
					    (ipaddrs->str_array[i] == NULL ?
					    "NULL" : ipaddrs->str_array[i]));
				}
			} else
				freehostent(hp);
		} else if (instances & PNMV6MASK) {
			/* group can host v6 addresses only */
			hp = getipnodebyname(ipaddrs->str_array[i],
			    AF_INET6, 0, &err);
			if (hp == NULL) {
				allok = B_FALSE;
				scds_syslog(LOG_ERR, "%s group %s can host "
				    "%s addresses only. Host %s has no mapping "
				    "of this type.", pnm_group_name(),
				    group, "IPv6",
				    (ipaddrs->str_array[i] == NULL ?
				    "NULL" : ipaddrs->str_array[i]));
				if (print_errors) {
					(void) fprintf(stderr, dgettext(
					    TEXT_DOMAIN, "%s group %s can "
					    "host %s addresses only. Host %s "
					    "has no mapping of this type.\n"),
					    pnm_group_name(), group, "IPv6",
					    (ipaddrs->str_array[i] == NULL ?
					    "NULL" : ipaddrs->str_array[i]));
				}
			} else
				freehostent(hp);
		} else {
			/*
			 * whaa? this shouldve been caught much earlier -
			 * in VALIDATE's main() itself. Bail out NOW!!!
			 */
			/*
			 * SCMSGS
			 * @explanation
			 * The IPMP group can not host any IP address. This is
			 * an unusual situation.
			 * @user_action
			 * Contact your authorized Sun service provider.
			 */
			scds_syslog(LOG_ERR, "%s group %s is incapable of "
			    "hosting IPv4 or IPv6 addresses", pnm_group_name(),
			    group);
			(void) fprintf(stderr, gettext("%s group %s is "
			    "incapable of hosting IPv4 or IPv6 addresses\n"),
			    pnm_group_name(), group);

			return (B_FALSE);
		}
	}

	if (allok == B_TRUE && (nsswitchok == B_FALSE || allinfiles == B_FALSE))
		/*
		 * SCMSGS
		 * @explanation
		 * While validating the hostname list, non fatal errors have
		 * been found.
		 * @user_action
		 * This is informational message. It is suggested to correct
		 * the errors if applicable. For the error information, check
		 * the syslog messages that have been encountered before this
		 * message.
		 */
		scds_syslog(LOG_INFO, "Despite the warnings, the validation "
		    "of the hostname list succeeded.");

	return (allok);
}

/*
 * haip_ipmp_delay()
 * sleep for the IPMP failure detection time. Called by
 * IPMP callback utility. Makes sure that in case of
 * subnet failures, other nodes would have detected
 * the problem as well.
 * The detection time is read from /etc/default/mpathd file.
 */

/*
 * For Linux, as we are using the bonding driver that uses MII monitoring
 * to detect NIC failures, no need here to delayed the failover operation.
 * This function is not invoked as the "nodelay" parameter will be present in
 * the callback utility.
 * If the bonding interface is configured to use an ARP monitoring, sleep 1
 * second.
 */
#ifdef linux
void
haip_ipmp_delay(void)
{
	(void) usleep(MICROSEC);
}
#else

/* Default value of the failure detection time */
#define	MPATHD_DETECTION_TIME	10000
#define	MPATHD_DEFAULT_FILE	"/etc/default/mpathd"
#define	MPATHD_DETECTION_PARAM	"FAILURE_DETECTION_TIME"
#define	MPATHD_LINELEN		1024

void
haip_ipmp_delay(void)
{
	useconds_t		sleep_time;
	FILE	*fp = NULL;
	char	line[MPATHD_LINELEN];
	char	*p = NULL, *val = NULL, *lasts = NULL;
	ulong_t		v;

	sleep_time = MPATHD_DETECTION_TIME;

	fp = fopen(MPATHD_DEFAULT_FILE, "r");
	if (fp == NULL) {
		scds_syslog_debug(1, "Unable to open <%s>: %s.", /* CSTYLED */
			MPATHD_DEFAULT_FILE, strerror(errno));	/*lint !e746 */
	} else {
		while (fgets(line, MPATHD_LINELEN, fp) != NULL) {
			/* Skip white spaces */
			for (p = line; isspace(*p); p++)
				;
			/* Skip empty lines */
			if (*p == 0)
				continue;
			/* Skip comment lines */
			if (*p == '#')
				continue;
			if ((p = (char *)strtok_r(p, "=", &lasts)) == NULL)
				continue;
			if ((val = (char *)strtok_r(NULL, "\n", &lasts))
				== NULL)
				continue;
			if (strcasecmp(p, MPATHD_DETECTION_PARAM) == 0) {
				v = (ulong_t)atol(val);
				if (v > 0) {
					sleep_time = (useconds_t)v;
				}
			}
		}
	}
	/* Convert milliseconds to microseconds */
	sleep_time *= 1000;
	scds_syslog_debug(2, "Sleep time is %ld.", sleep_time);
	(void) usleep(sleep_time);
}
#endif

static boolean_t	alarmflag;

static void
alrmhandler(int signum)
{
	scds_syslog_debug(1,
	    "signal %d is received", signum);

	alarmflag = B_TRUE;

} /* alrmhandler */


/*
 * haip_check_nameservice()
 * Use getipnodebyname() to check whether the name service is available and
 * responsive.
 * If timeout occurs, assume that name service is down,
 * else assume it is OK.
 * Return 0 for nameservice ok, 1 for errors
 */
int
haip_check_nameservice(scds_handle_t handle)
{
	char	candidate[MAXHOSTNAMELEN];
	pid_t	child_pid, pid;
	int	rc, err;
	scha_extprop_value_t *cr = NULL;
	boolean_t check_resolver = B_TRUE;

	/*
	 * Figure out if the user has enabled or disabled resolver check.
	 * If the property CheckResolver does not exist (eg new binaries
	 * with resource of older type), this code fragment does nothing
	 * and check_resolver remains TRUE by default
	 */
	rc = scds_get_ext_property(handle, HAIP_CHECKNAMESERVICE,
	    SCHA_PTYPE_BOOLEAN, &cr);
	if (rc == SCHA_ERR_NOERR) {
		check_resolver = cr->val.val_boolean;
		scds_free_ext_property(cr);
	} else if ((rc != SCHA_ERR_NOERR) && (rc != SCHA_ERR_PROP)) {
		/* failed, and not with SCHA_ERR_PROP */
		scds_syslog(LOG_ERR, "Failed to retrieve the property %s: %s.",
		    HAIP_CHECKNAMESERVICE, scds_error_string(rc));
		return (1);
	}

	/* stop right here if check_resolver is not TRUE */
	if (!check_resolver)
		return (0);

	/* We generate a unique name to query because we want to bypass */
	/* any client side caching, both positive and negative. */
	(void) sprintf(candidate, "NOSUCHHOST%lld", gethrtime());

	child_pid = fork();
	if (child_pid < 0) {
		scds_syslog(LOG_ERR,
		    "fork() failed: %m.");
		return (1);
	}

	if (child_pid == 0) {	/* Child process */
		errno = 0;

		(void) getipnodebyname(candidate, AF_INET6,
		    AI_V4MAPPED | AI_ALL, &err);

		_exit(errno);
	}

	/* Parent process */
	alarmflag = B_FALSE;
	(void) sigset(SIGALRM, alrmhandler);
	(void) alarm(HAIP_NS_TIMEOUT);

	pid = waitpid(child_pid, &rc, 0);
	/* We don't need the returned status 'rc' */

	(void) alarm(0);

	if (alarmflag) {
		/* Assume name service is not available if getipnodebyname() */
		/* timed out. */
		/*
		 * SCMSGS
		 * @explanation
		 * A call to getipnodebyname(3SOCKET) timed out.
		 * @user_action
		 * Check the settings in /etc/nsswitch.conf and make sure that
		 * the name resolver is not having problems on the system.
		 */
		scds_syslog(LOG_ERR, "getipnodebyname() timed out.");

		/* Kill the dangling process still running getipnodebyname() */
		if (child_pid > 0)
			(void) kill(child_pid, SIGKILL);

		return (1);
	}

	if (pid == -1) {
		/* unexpected failure. */
		scds_syslog(LOG_ERR, "waitpid() failed: %m.");
		return (1);
	}

	/* Assume name service is ok getipnodebyname() return */
	/* immediately, even for the case of mis-configuration */
	return (0);
}

/*
 * haip_check_group_changed()
 * Check if the ipmp group is being changed.
 * Called by the validate method of hafoip and hascip if -u flag is set.
 * Compare the value passed in the command line with the value stored in ccr.
 * Returns the following values:
 * -1 for errors (also logs and prints messages)
 * 1 when the group is being changed and the resource is online on this node
 * 0 otherwise
 */
int
haip_check_group_changed(const char *zone,
    scds_handle_t handle, char *new_group)
{
	int	rc;
	char	*adapter_in_ccr;
	scha_resource_t		rs_handle;
	scha_extprop_value_t	*extprop = NULL;
	scha_rsstate_t state;
	scha_err_t e;

	if (print_errors)
		(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

retry_rs:
	rc = scha_resource_open_zone(zone, scds_get_resource_name(handle),
	    scds_get_resource_group_name(handle), &rs_handle);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the resource handle: %s.",
		    scds_error_string(rc));
		if (print_errors) {
			(void) fprintf(stderr, dgettext(TEXT_DOMAIN,
				"Failed to retrieve the resource handle: "
				"%s.\n"), dgettext(TEXT_DOMAIN,
				scds_error_string(rc)));
		}
		return (-1);
	}

	rc = haip_rs_get(zone, rs_handle, HAIP_IFTABLE, &extprop);
	if (rc == HAIP_RETRY) {
		scds_syslog(LOG_INFO,
		    "Retrying to retrieve the resource information.");
		(void) scha_resource_close(rs_handle);
		rs_handle = NULL;
		(void) sleep(1);
		goto retry_rs;
	}
	if (extprop == NULL || extprop->val.val_strarray == NULL) {
		scds_syslog(LOG_ERR,
		    "NULL value returned for the extension property %s.",
		    HAIP_IFTABLE);
		if (print_errors) {
			(void) fprintf(stderr, dgettext(TEXT_DOMAIN,
				"NULL value returned for the extension "
				"property %s.\n"), HAIP_IFTABLE);
		}
		rc = -1;
		goto finished;
	}

	/* use the version that works with strarrays */
	adapter_in_ccr = _haip_get_ipmp_group(extprop->val.val_strarray, zone);
	if (adapter_in_ccr == NULL) {
		/* message already logged */
		rc = -1;
		goto finished;
	}

	if (strcmp(adapter_in_ccr, new_group) == 0)
		rc = 0;
	else {
		scds_syslog_debug(DBG_LEVEL_LOW,
		    "The %s group name specified in command line is %s, "
		    "the current value in ccr is %s.",
		    pnm_group_name(), new_group, adapter_in_ccr);
		e = scha_resource_get_zone(zone,
		    rs_handle, SCHA_RESOURCE_STATE, &state);
		if (e == SCHA_ERR_SEQID) {
			(void) scha_resource_close(rs_handle);
			rs_handle = NULL;
			(void) sleep(1);

			/* retry the whole operation, including IFTABLE */
			goto retry_rs;
		} else if (e != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR, "Failed to retrieve the resource "
			    "property %s: %s.", SCHA_RESOURCE_STATE,
			    scds_error_string(e));
			if (print_errors) {
				(void) fprintf(stderr, dgettext(TEXT_DOMAIN,
				    "Failed to retrieve the resource property "
				    "%s: %s."), SCHA_RESOURCE_STATE,
				    scds_error_string(e));
			}
			rc = -1;
			goto finished;
		}

		rc = (state == SCHA_RSSTATE_OFFLINE) ? 0 : 1;
	}

finished:
	(void) scha_resource_close(rs_handle);
	return (rc);
}

/*
 * haip_validate_uniqueness()
 * Check if the hostname in the hostlist already exists in any other RG.
 * Called by the validate method of hafoip and hascip.
 * Returns the following values:
 * B_FALSE for errors (also logs and prints messages)
 * B_TRUE if the hostname is unique
 */
boolean_t
haip_validate_uniqueness(scds_handle_t scds_handle)
{

	uint_t		hostindex;
	scha_extprop_value_t *ipa_list;
	scha_str_array_t *hostlist;
	char		*resname;
	int rc;

	scds_get_ext_property(scds_handle, HAIP_IPALIST,
				SCHA_PTYPE_STRINGARRAY, &ipa_list);

	/* Get the resource name */
	resname = (char *)scds_get_resource_name(scds_handle);

	hostlist = ipa_list->val.val_strarray;
	for (hostindex = 0; hostindex < hostlist->array_cnt; hostindex++) {

		rc = validate_unique_hostname
			(hostlist->str_array[hostindex], resname);
		if (rc != 0)
			return (B_FALSE);
	}
	return (B_TRUE);
}

/*
 * validate_unique_hostname()
 * This function iterates through a list of RGs configured
 * in the cluster, gets the list of resources under each RG
 * iterates through the list of resources, checks to see if
 * the rs is of type logical hostname or shared address. If
 * yes, checks the HostNameList extension property and compares
 * each hostname in the HostNameList with the hostname passed
 * as argument to this function.
 * Called by haip_validate_uniqueness() function.
 * Returns a non-zero value if the hostname is not unique or
 * for any errors(also logs and prints messages)
 * Returns zero if the hostname is unique
 */
int
validate_unique_hostname(char *hostname, char *resname)
{
	uint_t		i;
	scha_str_array_t	*all_rgs = NULL;
	scha_str_array_t	*all_res = NULL;
	scha_extprop_value_t	*extprop = NULL;
	int		rg_count;
	int		rs_count;
	char		*rgname = NULL;
	char		*rsname = NULL;
	char		*rtname = NULL;
	scha_err_t	rc = 0, rc1 = 0;
	scha_cluster_t	clust_handle;
	scha_resourcegroup_t	rg_handle = NULL;
	scha_resource_t		rs_handle = NULL;
	scha_str_array_t *hostlist;
	boolean_t	is_log = B_FALSE;
	boolean_t	is_shared = B_FALSE;

	if (print_errors)
		(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
retry_cluster:
	rc = scha_cluster_open(&clust_handle);
	if (rc != SCHA_ERR_NOERR) {
		if (print_errors) {
			(void) fprintf(stderr, dgettext(TEXT_DOMAIN,
			"Failed to retrieve the cluster handle : "
			"%s.\n"), dgettext(TEXT_DOMAIN,
			scds_error_string(rc)));
		}
		return (-1);
	}

	rc = scha_cluster_get(clust_handle, SCHA_ALL_RESOURCEGROUPS, &all_rgs);
	if (rc == SCHA_ERR_SEQID) {
		scds_syslog(LOG_INFO,
		    "Retrying to retrieve the cluster information.");
		clust_handle = NULL;
		(void) sleep(1);
		goto retry_cluster;
	} else if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the property %s: %s.",
		    SCHA_ALL_RESOURCEGROUPS, scds_error_string(rc));
		(void) scha_cluster_close(clust_handle);
		clust_handle = NULL;
		return (-1);
	}

	/*
	 * Get the list of resources under each RG and
	 * check if the resource type is SUNW.LogicalHostname
	 * or SUNW.SharedAddress.
	 */
	for (rg_count = 0; rg_count < (int)all_rgs->array_cnt;
		rg_count++) {
		rgname = all_rgs->str_array[rg_count];
retry_rg:
		rc = scha_resourcegroup_open(rgname, &rg_handle);
		if (rc != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR,
			"Failed to retrieve the resource group handle: %s.",
			scds_error_string(rc));
			(void) scha_cluster_close(clust_handle);
			clust_handle = NULL;
			return (-1);
		}

		rc = scha_resourcegroup_get(rg_handle,
				SCHA_RESOURCE_LIST,
				&all_res);
		if (rc == SCHA_ERR_SEQID) {
			scds_syslog(LOG_INFO,
				"Retrying to retrieve the "
				"resource group information.");
			(void) scha_resourcegroup_close(rg_handle);
			rg_handle = NULL;
			(void) sleep(1);
			goto retry_rg;
		} else if (rc != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR,
				"Failed to retrieve the resource group "
				"property %s: %s.",
				SCHA_RESOURCE_LIST, scds_error_string(rc));
			rc = -1;
			goto finished;
		}
		/*
		 * Iterate through the list of resources under
		 * each RG only if the resource name does not
		 * match with the current resource.
		 */
		for (rs_count = 0; rs_count <
			(int)all_res->array_cnt; rs_count++) {

			rsname = all_res->str_array[rs_count];
			if (strcmp(rsname, resname) != 0) {
				/*
				 * Get the value of HostNameList ext property
				 * if the rs is of type LogicalHostname of
				 * Shared Address.Then compare each hostname
				 * in the hostlist with the arguments of this
				 * function.
				 */
retry_rs:
				rc = scha_resource_open(rsname, rgname,
					&rs_handle);
				if (rc != SCHA_ERR_NOERR) {
					if (print_errors) {
						(void) fprintf(stderr,
						dgettext(TEXT_DOMAIN,
						"Failed to retrieve the "
						"resource handle: "
						"%s.\n"), dgettext(TEXT_DOMAIN,
						scds_error_string(rc)));
					}
					rc = -1;
					goto finished;
				}

				rc = haip_is_logical_hostname(rs_handle, rsname,
					&is_log);
				if (rc == HAIP_RETRY) {
					scds_syslog(LOG_INFO,
					"Retrying to retrieve the resource "
					"information.");
					(void) sleep(1);
					goto retry_rs;
				}
				if (rc == HAIP_ERROR) {
					if (print_errors) {
					    (void) fprintf(stderr, dgettext(
					    TEXT_DOMAIN, "Failed to retrieve "
					    "the resource property %s.\n"),
					    SCHA_IS_LOGICAL_HOSTNAME);
					}
					rc = -1;
					goto finished;
				}
				if (is_log != B_TRUE) {
					rc1 = haip_is_shared_address(rs_handle,
						rsname, &is_shared);
					if (rc1 == HAIP_RETRY) {
						scds_syslog(LOG_INFO,
						"Retrying to retrieve the "
						"resource information.");
						(void) sleep(1);
						goto retry_rs;
					}
					if (rc1 == HAIP_ERROR) {
						if (print_errors) {
						    (void) fprintf(stderr,
						    dgettext(TEXT_DOMAIN,
						    "Failed to retrieve the "
						    "resource property %s.\n"),
						    SCHA_IS_SHARED_ADDRESS);
						}
						rc = -1;
						goto finished;
					}
				}
				if (is_log == B_TRUE || is_shared ==
						B_TRUE) {
					rc = haip_rs_get(NULL, rs_handle,
						HAIP_IPALIST, &extprop);
					if (rc == HAIP_RETRY) {
						scds_syslog(LOG_INFO,
						"Retrying to retrieve the "
						"resource information.");
						(void) sleep(1);
						goto retry_rs;
					}
					if (extprop == NULL ||
					extprop->val.val_strarray == NULL) {
						if (print_errors) {
						(void) fprintf(stderr,
						dgettext(TEXT_DOMAIN, "NULL "
						"value returned for the "
						"extension property %s.\n"),
						HAIP_IPALIST);
						}
						rc = -1;
						goto finished;
					}
					hostlist = extprop->val.val_strarray;
				/*
				 * Iterate through the list of hostnames
				 * in the HostNameList property and check
				 * if the hostname matches with the current
				 * resource's hostname.
				 */
				for (i = 0; i < hostlist->array_cnt;
					i++) {
					if (strcmp(hostlist->
						str_array[i],
						hostname) == 0) {
						if (print_errors) {
						(void) fprintf(stderr,
						dgettext(TEXT_DOMAIN,
						"Cannot create resource."
						"This resource contains "
						"hostname(s) that exist "
						"in the hostlist of "
						"another resource.\n"));
						rc = -1;
						goto finished;
						}
					}
				}
				}
			}
		}
	}
finished:
	if (rg_handle != NULL) {
		(void) scha_resourcegroup_close(rg_handle);
		rg_handle = NULL;
	}

	if (clust_handle != NULL) {
		(void) scha_cluster_close(clust_handle);
		clust_handle = NULL;
	}

	if (rs_handle != NULL) {
		(void) scha_resource_close(rs_handle);
		rs_handle = NULL;
	}
	return (rc);
}
