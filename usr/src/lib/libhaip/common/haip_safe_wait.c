/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * haip_safe_wait.c - Contains utilities to make sure that
 * a given ipaddress in not online on some other cluster node.
 * First checks with CMM via the safe_to_takeover utility and
 * returns immediately if that returns true. Otherwise sends
 * ICMP echos every 200 milliseconds and returns after 3
 * consecutive timeouts. If the ipaddress continues to be
 * pingable, it waits for 120 seconds (2 minutes).
 */

#pragma ident	"@(#)haip_safe_wait.c	1.19	09/04/23 SMI"

#include <sys/os.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <signal.h>
#include <poll.h>
#include <sys/types.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/in_systm.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <netinet/icmp6.h>

#include <rgm/libdsdev.h>
#include <rgm/haip.h>

/*
 * On Europa Farm nodes, the safe_to_takeover utiity can't be use.
 */
#define	FRGMD "/usr/cluster/lib/sc/frgmd"

boolean_t isfarmnode;

void
nodetype_init(void)
{
	struct stat filestat;

	if (stat(FRGMD, &filestat) < 0)
		isfarmnode = B_FALSE;
	else
		isfarmnode = B_TRUE;
}


#define	PING_INTERVAL_MS	200
#define	NUM_TIMEOUTS		  3

/*
 * The only data we send out with our ping request is a timestamp.
 * That fits in 56 bytes.
 */
#define	DATALEN	56

/* total icmp packet size is header + the size of the data that we send */
#define	ICMP_PACKET_LENGTH (ICMP_MINLEN + DATALEN)

/*
 *			I N _ C K S U M
 *
 * Checksum routine for Internet Protocol family headers (C Version)
 *
 */
static ushort_t
in_cksum(ushort_t *addr, int len)
{
	register int nleft = len;
	register ushort_t *w = addr;
	register ushort_t answer;
	ushort_t odd_byte = 0;
	register uint_t sum = 0;

	/*
	 *  Our algorithm is simple, using a 32 bit accumulator (sum),
	 *  we add sequential 16 bit words to it, and at the end, fold
	 *  back all the carry bits from the top 16 bits into the lower
	 *  16 bits.
	 */
	while (nleft > 1)  {
		sum += *w++;
		nleft -= 2;
	}

	/* mop up an odd byte, if necessary */
	if (nleft == 1) {
		*(uchar_t *)(&odd_byte) = *(uchar_t *)w;
		sum += odd_byte;
	}

	/*
	 * add back carry outs from top 16 bits to low 16 bits
	 */
	sum = (sum >> 16) + (sum & 0xffff);	/* add hi 16 to low 16 */
	sum += (sum >> 16);			/* add carry */
	answer = (ushort_t)~sum;		/* truncate to 16 bits */
	return (answer);
}

/*
 * haip_ping_ip()
 * pings the ip address specified. echo requests are sent every 'interval'
 * milliseconds. If 'num_timeouts' consecutive requests go unanswered, 1 is
 * returned immediately. If replies (intermittent or every time) continue to be
 * received for 'total_time' milliseconds, 0 is returned.
 * On an error, -1 is returned, *no* error message is logged
 */
int haip_ping_ip(struct in6_addr target, int total_time, int interval,
    int num_timeouts)
{
	/* icmp_packet contents are the same for v4 and v6 */
	char icmp_packet[ICMP_PACKET_LENGTH];
	struct icmp6_hdr *v6_packet = (struct icmp6_hdr *)icmp_packet;
	struct icmp *v4_packet = (struct icmp *)icmp_packet;

	/* same storage for both v4 and v6, different pointers to access it */
	struct sockaddr_storage ss;
	struct sockaddr_in6 *sin6 = (struct sockaddr_in6 *)&ss;
	struct sockaddr_in *sin = (struct sockaddr_in *)&ss;
	struct in6_addr *v6_target = &(sin6->sin6_addr);
	struct in_addr *v4_target = &(sin->sin_addr);

	struct pollfd pfd[1];
	int s = -1;
	int rc, i, recvlen, consecutive_timeouts = 0;
	hrtime_t wait_end_time, wait_remaining, time_to_go_away;
	char recvbuf[SCDS_ARRAY_SIZE];
	boolean_t v4mode = B_FALSE;

	/* we'll use this as our packet id */
	pid_t mypid = getpid();

	/*
	 * This is the hrtime at which we should stop sending pings.
	 * Convert the total_time which is in secs to nanosecs, since
	 * the gethrtime() gives the value in nanosecs.
	 */
	time_to_go_away = gethrtime() + (hrtime_t)(total_time * 1e9);
	bzero(&ss, sizeof (ss));

	/*
	 * figure out whether we're supposed to ping a v4 or v6 address and
	 * setup the target sockaddr accordingly. Also, open a raw socket of
	 * the right type. Then establish the target address as its default
	 * destination. This makes sure that the kernel delivers icmp packets
	 * to our raw socket from this host _only_ - less spurious traffic to
	 * deal with.
	 */
	v4mode = IN6_IS_ADDR_V4MAPPED(&target) ? B_TRUE : B_FALSE;
	if (v4mode) {
		/* target has an IPv4 address */
		sin->sin_family = AF_INET;
		IN6_V4MAPPED_TO_INADDR(&target, v4_target);

		s = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
		if (s < 0) {
			scds_syslog(LOG_ERR,
			    "Failed to create socket: %s.", /* CSTYLED */
			    strerror(errno)); /*lint !e746 */
			rc = -1;
			goto finished;
		}
		/* no actual connection taking place here */
		if (connect(s, (struct sockaddr *)sin,
		    sizeof (struct sockaddr_in)) < 0) {
			/*
			 * SCMSGS
			 * @explanation
			 * Default destination for a raw icmp socket could not
			 * be established.
			 * @user_action
			 * If the error message string thats logged in this
			 * message does not offer any hint as to what the
			 * problem could be, contact your authorized Sun
			 * service provider.
			 */
			scds_syslog(LOG_ERR, "Failed to establish socket's "
			    "default destination: %s", strerror(errno));
			rc = -1;
			goto finished;
		}
	} else {
		struct icmp6_filter f;

		/* target has an IPv6 address */
		sin6->sin6_family = AF_INET6;
		*v6_target = target;

		s = socket(AF_INET6, SOCK_RAW, IPPROTO_ICMPV6);
		if (s < 0) {
			scds_syslog(LOG_ERR, "Failed to create socket: %s.",
			    strerror(errno));
			rc = -1;
			goto finished;
		}
		/* no actual connection taking place here */
		if (connect(s, (struct sockaddr *)sin6,
		    sizeof (struct sockaddr_in6)) < 0) {
			scds_syslog(LOG_ERR, "Failed to establish socket's "
			    "default destination: %s", strerror(errno));
			rc = -1;
			goto finished;
		}

		/*
		 * We are interested in echo replies only. Set a filter for
		 * this. If the setsockopt fails, no big deal - we tried
		 * our best.
		 */
		ICMP6_FILTER_SETBLOCKALL(&f);
		ICMP6_FILTER_SETPASS(ICMP6_ECHO_REPLY, &f);
		(void) setsockopt(s, IPPROTO_ICMPV6, ICMP6_FILTER, &f,
		    sizeof (f));
	}

	/* send pings as long as there's time remaining */
	for (i = 0; gethrtime() < time_to_go_away; i++) {
		if (v4mode) {
			/* build a v4 icmp packet */
			v4_packet->icmp_type = ICMP_ECHO;
			v4_packet->icmp_code = (unsigned short)0;
			v4_packet->icmp_seq = (unsigned short)i;
			v4_packet->icmp_id = (unsigned short)mypid;
			*(hrtime_t *)v4_packet->icmp_data = gethrtime();
			v4_packet->icmp_cksum = 0l;
			v4_packet->icmp_cksum = in_cksum((unsigned short *)
			    v4_packet, ICMP_PACKET_LENGTH);
		} else {
			/* build a v6 icmp packet */
			v6_packet->icmp6_type = ICMP6_ECHO_REQUEST;
			v6_packet->icmp6_code = (unsigned short)0;
			v6_packet->icmp6_seq = (unsigned short)i;
			v6_packet->icmp6_id = (unsigned short)mypid;
			*(hrtime_t *)(v6_packet + 1) = gethrtime();
		}

		/* send out the packet */
		if (send(s, icmp_packet, ICMP_PACKET_LENGTH, 0)
		    != (ssize_t)ICMP_PACKET_LENGTH) {
			/*
			 * SCMSGS
			 * @explanation
			 * There was a problem in sending an ICMP packet over
			 * a raw socket.
			 * @user_action
			 * If the error message string thats logged in this
			 * message does not offer any hint as to what the
			 * problem could be, contact your authorized Sun
			 * service provider.
			 */
			scds_syslog(LOG_ERR, "Failed to send ICMP packet: %s",
			    strerror(errno));
			rc = -1;
			goto finished;
		}

		/*
		 * wait atleast 'interval' millisecs for a reply. If we
		 * get spurious packets, we come back and poll() for the
		 * remaining duration. If a proper reply is received, we
		 * sleep for the remaining time - this makes sure that
		 * echo requests are not sent more frequently than once
		 * every 'interval' millisecs.
		 */

		/*
		 * Convert the interval which is defined in millisecs into
		 * nanosecs, since gethrtime() gives the value in nanosecs
		 */
		wait_end_time = gethrtime() + (hrtime_t)(1e6 * interval);
		while ((wait_remaining = wait_end_time - gethrtime()) >= 0) {
			pfd[0].fd = s;
			pfd[0].events = POLLIN;
			pfd[0].revents = 0;
			rc = poll(pfd, 1, (int)(wait_remaining / 1e6));
			if (rc < 0) {
				rc = -1;
				goto finished;
			} else if (rc == 0) {
				/* no response within the timeout interval */
				if (++consecutive_timeouts >= num_timeouts) {
					rc = 1;
					goto finished;
				}
				break;
			}

			/* make sure there wasnt any error during poll() */
			if (pfd[0].revents & (POLLERR | POLLHUP | POLLNVAL)) {
				rc = -1;
				goto finished;
			}

			/* Response received - make sure its an echo reply */
			recvlen = recv(s, recvbuf, sizeof (recvbuf), 0);
			if (recvlen < 0) {
				rc = -1;
				goto finished;
			}

			if (v4mode) {
				struct ip *ip = (struct ip *)recvbuf;
				struct icmp *icmp;
				int hl;

				hl = ip->ip_hl << 2;
				icmp = (struct icmp *)(recvbuf + hl);

				if ((icmp->icmp_type == ICMP_ECHOREPLY) &&
				    (icmp->icmp_id == (unsigned short)mypid)) {
					/* got a reply, ip address is alive */
					consecutive_timeouts = 0;

					/*
					 * sleep the remaining time, so that
					 * we dont send out the next echo
					 * request the moment we get a reply
					 *
					 * usleep() takes the input time in
					 * microsecs, convert the wait_end_time
					 * which is in nanosecs to microsecs.
					 */
					(void) usleep((uint_t)((wait_end_time -
					    gethrtime()) / 1e3));
					break;
				}
			} else {
				struct icmp6_hdr *icmp6;

				icmp6 = (struct icmp6_hdr *)recvbuf;

				if ((icmp6->icmp6_type == ICMP6_ECHO_REPLY) &&
				    (icmp6->icmp6_id == mypid)) {
					/* got a reply, ip address is alive */
					consecutive_timeouts = 0;

					/*
					 * sleep the remaining time, so that
					 * we dont send out the next echo
					 * request the moment we get a reply
					 *
					 * usleep() takes the input time in
					 * microsecs, convert the wait_end_time
					 * which is in nanosecs to microsecs.
					 */
					(void) usleep((uint_t)((wait_end_time -
					    gethrtime()) / 1e3));
					break;
				}
			}
		}
	}

	/* still receiving replies, return 0 */
	rc = 0;

finished:
	if (s >= 0)
		(void) close(s);

	return (rc);
}

/*
 * haip_safe_wait()
 * The interface here. First run /usr/cluster/lib/sc/safe_to_takeover
 * if that returns OK, return immediately.
 * Otherwise call wait_for_ip().
 * Error conditions are handled conservatively, i.e.
 * fall back on explicit ping checks if there is a problem.
 * Return Values:
 * 0 - If safe_to_takeover returns 0 or if there are no ping responses
 * 1 - If there are ping responses
 */
int
haip_safe_wait(struct in6_addr theip, int timeout)
{
	/*
	 * Call safe_to_takeover to determine if
	 * there was a CMM reconfiguration recently. If not,
	 * we don't need to check if ipaddresses are down
	 * on other nodes.
	 */
	int		rc;

	nodetype_init();

	if (isfarmnode) {
		/*
		 * We set rc to 1. This means will test that the
		 * IP address is effectively unconfigured on the
		 * other node using ping before taking over the
		 * primary role
		 */
		rc = 1;
	} else {
		rc = system("/usr/cluster/lib/sc/safe_to_takeover");
	}
	/*
	 * In order to skip the ping checks via wait_for_ip()
	 * the system() call must have run successfully and
	 * safe_to_takeover must return 0. Hence we don't specifically
	 * look for WEXITSTATUS(rc), rc itself must be zero.
	 */
	if (rc == 0) {
		scds_syslog_debug(1, "safe_to_takeover returned OK");
		return (0);
	}

	scds_syslog_debug(1, "Calling haip_ping_ip()");

	/*
	 * haip_ping_ip() returns
	 * 0 - if responses are received for the ping requests
	 * 1 - if responses are *not* received for the ping requests
	 * -1 - if there is an error
	 */
	rc = haip_ping_ip(theip, timeout, PING_INTERVAL_MS, NUM_TIMEOUTS);
	if (rc == 0) {
		/*
		 * There are responses for the ping requests, so the
		 * address may be already up somewhere.
		 */
		scds_syslog_debug(1, "Responses received for the ping "
		    "requests.");
		return (1);
	}
	return (0);
}
