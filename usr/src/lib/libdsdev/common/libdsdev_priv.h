/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_LIBDSDEV_PRIV_H
#define	_LIBDSDEV_PRIV_H

#pragma ident	"@(#)libdsdev_priv.h	1.72	09/01/09 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <scha.h>
#include <string.h>
#include <thread.h>
#include <synch.h>

#include <rgm/libdsdev.h>

#ifdef	__cplusplus
extern "C" {
#endif

/* SDK Common Extension Properties */
#define	SCDS_EXTPROP_CONFIGDIR			"Confdir_list"
#define	SCDS_EXTPROP_MONITOR_RETRY_COUNT	"Monitor_retry_count"
#define	SCDS_EXTPROP_MONITOR_RETRY_INTERVAL	"Monitor_retry_interval"

#define	SCDS_EXTPROP_PROBE_TIMEOUT		"Probe_timeout"
#define	SCDS_EXTPROP_CHILD_MON_LEVEL		"Child_mon_level"

#define	SCDS_EXTPROP_FAILOVER_ENABLED		"Failover_enabled"

#define	SCDS_EXTPROP_PARAMTABLEVERSION 		"Paramtable_version"
#define	SCDS_EXTPROP_HOSTNAME			"HostnameList"

/* resource type for an HAStoragePlus resource */
#define	HASP_RT_NAME	"SUNW.HAStoragePlus"

/* file system mount points extension property of an HAStoragePlus resource */
#define	HASP_FSMP_PROPERTY	"FilesystemMountPoints"

/* Zpools extension property of an HAStoragePlus resource */
#define	HASP_ZPOOLS_PROPERTY	"Zpools"

/* Array size constants get defined here */
#define	SCDS_ARRAY_SIZE			1024
#define	SCDS_PMFARRAY_SIZE		2048
#define	SCDS_PMFTAG_SIZE		1024
#define	INT_ERR_MSG_SIZE		1024
#define	SCDS_SYSLOG_BUF_SIZE		2048

/* Location for UNIX sockets used by fault monitor */
#define	SCDS_TMPDIR			"/tmp/.cluster"

/* Percentage of timeout for pmf_stop */
#define	SCDS_SMOOTH_PCT			80	/* for SIGTERM */
#define	SCDS_HARD_PCT			15	/* for SIGKILL */

/*
 * Definitions to support parsing of sys prop Port_list
 */

#define	SCDS_CTAB_CODE_INVALID	-1

#define	SCDS_PROTO_STRING_TCP	"tcp"
#define	SCDS_PROTO_STRING_UDP	"udp"
#define	SCDS_PROTO_STRING_SCTP	"sctp"
#define	SCDS_PROTO_STRING_TCP6	"tcp6"
#define	SCDS_PROTO_STRING_UDP6	"udp6"

/*
 * Delimiter characters between:
 *   - IP address and port
 *   - port and protocol
 *
 * The general form of a Port_list entry is
 *   [<IP address>SCDS_IP_PORT_DELIMITER]<port>SCDS_PORT_PROTO_DELIMITER<proto>
 *
 * SCDS_IP_PORT_DELIMITER is '/' instead of the more IPv4-standard ':'
 * to allow IPv6 which uses ':' within an address.
 */
#define	SCDS_IP_PORT_DELIMITER		'/'
#define	SCDS_PORT_PROTO_DELIMITER	'/'

#define	NS_SECURITY	"Security"

typedef struct scds_code_table {
	char    *c_string;
	int	c_code;
} scds_code_table_t;

/* Debug levels for syslog messages */
#define	SCDS_DBG_0		0
#define	SCDS_DBG_1		1	/* start of debugging level */
#define	SCDS_DBG_2		2
#define	SCDS_DBG_3		3
#define	SCDS_DBG_4		4
#define	SCDS_DBG_5		5
#define	SCDS_DBG_6		6
#define	SCDS_DBG_7		7
#define	SCDS_DBG_8		8
#define	SCDS_DBG_9		9	/* end of debugging level */

#define	SCDS_DBG_LOW		SCDS_DBG_1
#define	SCDS_DBG_MID		SCDS_DBG_5
#define	SCDS_DBG_HIGH		SCDS_DBG_9

/* Number to represent an invalid nodeid */
#define	SCDS_INVALID_NODEID		-1

/* Number to represent an invalid file descriptor */
#define	SCDS_INVALID_FD			-1

/* flag to indicate the property value is unset in RTR file and cmd line */
#define	SCDS_PROP_UNSET			-1

/* Base directory for loglevel parameter */
#define	SCDS_VAROPT			"/var/cluster/rgm/rt"

/* Binary for running scripts under pmf */
#define	SCDS_PMFADM			"/usr/cluster/bin/pmfadm"

/* Binary for running scripts under hatimerun */
#define	SCDS_HATIMERUN			"/usr/cluster/bin/hatimerun"

/* Character for in pmftags. ',' cannot be in an RS, RT, or RG name */
#define	SCDS_PMFTAG_CHAR		','

/* for resources that use the default probe name of <basedir>/<svc>_probe */
#define	SCDS_PMF_DEFAULT_PROBE		NULL

/* For use when stopping monitoring by pmf without killing the process */
#define	SCDS_PMF_NOSIGNAL		-1
#define	SCDS_PMF_NOTIMEOUT		0

#define	SCDS_NO_TIMEOUT	-1

/* Return codes from scds_fm_history() */
#define	HISTORY_NOACTION	0		/* Resource is healthy */
#define	HISTORY_RESTART		1		/* Restart resource */
#define	HISTORY_FAILOVER	2		/* Failover RG */
#define	HISTORY_ERROR		3		/* No memory perhaps */
#define	HISTORY_NOACTION_DEG	4		/* Degraded; no action needed */


/*
 * Flag to indicate that the validate method is called for resource creation,
 * resource update, or resource group update.
 */
typedef enum scds_method_opcode
{
	SCDS_OP_NONE = 0,
	SCDS_OP_CREATE_R,
	SCDS_OP_UPDATE_R,
	SCDS_OP_UPDATE_RG
} scds_method_opcode_t;

/*
 * This struct holds all info pertaining to an RT's Registration info
 * which is available through the API call: scha_resourcetype_get()
 */
typedef struct scds_rt
{
	char			*rt_svc_name;
	boolean_t		rt_single_inst;	/* SCHA_SINGLE_INSTANCE */
	scha_initnodes_flag_t	rt_init_nodes;	/* SCHA_INIT_NODES */
	scha_str_array_t	*rt_install_nodes; /* SCHA_INSTALLED_NODES */
	boolean_t		rt_failover;	/* SCHA_FAILOVER */
	int			rt_api_version;	/* SCHA_API_VERSION */
	char			*rt_version;	/* SCHA_RT_VERSION */

	/* SCHA_METHOD */
	char			*rt_basedir;
	char			*rt_start_method;
	char			*rt_stop_method;
} scds_rt_t;


/*
 * This struct holds all resource system-defined properties info pertaining
 * to an RT's Paramtable and everything else available through the API call:
 * scha_resource_get()
 */
typedef struct scds_sysdef_props
{
	scha_str_array_t	*p_strong_dep;
				/* SCHA_RESOURCE_DEPENDENCIES */
	scha_str_array_t	*p_weak_dep;
				/* SCHA_RESOURCE_DEPENDENCIES_WEAK */
	scha_str_array_t	*p_restart_dep;
				/* SCHA_RESOURCE_DEPENDENCIES_RESTART */
	scha_str_array_t	*p_offline_restart_dep;
				/* SCHA_RESOURCE_DEPENDENCIES_OFFLINE_RESTART */
	/*
	 * the below four fields are same as above four fields but with
	 * qualifiers
	 */

	scha_str_array_t	*p_strong_dep_q;
	scha_str_array_t	*p_weak_dep_q;
	scha_str_array_t	*p_restart_dep_q;
	scha_str_array_t	*p_offline_restart_dep_q;
	scha_str_array_t	*p_network_resources_used;
				/* SCHA_NETWORK_RESOURCES_USED */
	scha_failover_mode_t	p_failover_mode;
				/* SCHA_FAILOVER_MODE */
	char			*p_project_name;
				/* SCHA_RESOURCE_PROJECT_NAME */

	int 	p_cprobe_interval;	/* SCHA_CHEAP_PROBE_INTERVAL */
	int 	p_tprobe_interval;	/* SCHA_THOROUGH_PROBE_INTERVAL */
	int 	p_retry_count;		/* SCHA_RETRY_COUNT */
	int 	p_retry_interval;	/* SCHA_RETRY_INTERVAL */
	boolean_t	p_scalable;	/* SCHA_SCALABLE */
	boolean_t	p_gz_override;	/* SCHA_GLOBAL_ZONE_OVERRIDE */

	scha_str_array_t	*p_port_list;    /* SCHA_PORT_LIST */
	scds_netaddr_list_t	*p_netaddr_list;

	/* Method Timeout */
	int	p_start_timeout;
	int	p_stop_timeout;
	int	p_monitor_stop_timeout;
} scds_sysdef_props_t;


/*
 * Struct used for common Extension Properties found in most Resources
 */
typedef struct scds_common_extprops
{
	scha_str_array_t *x_config_dirs;	/* SCDS_EXTPROP_CONFIGDIR */
	char		*x_paramtable_version;
					/* SCDS_EXTPROP_PARAMTABLEVERSION */
	int		x_pmf_arg_n;
					/* SCDS_EXTPROP_MONITOR_RETRY_COUNT */
	int		x_pmf_arg_t;
				/* SCDS_EXTPROP_MONITOR_RETRY_INTERVAL */
	int		x_probe_timeout;	/* SCDS_EXTPROP_PROBE_TIMEOUT */
	int		x_child_mon_level;	/* pmfadm -C <int> */
	boolean_t	x_failover_enabled; /* SCDS_EXTPROP_FAILOVER_ENABLED */
} scds_common_extprops_t;


/*
 * This struct holds all info pertaining to an Resource's properties info
 * which is available through the API call: scha_resource_get().
 */
typedef struct scds_r
{
	scha_switch_t		r_on_off_switch; /* SCHA_ON_OFF_SWITCH */
	scha_switch_t 		r_fm_switch;	/* SCHA_MONITORED_SWITCH */
	scha_rsstatus_t		r_status;	/* SCHA_STATUS */
	scha_rsstatus_t		r_status_node;	/* SCHA_STATUS_NODE */
	scha_rsstate_t 		r_state;	/* SCHA_RESOURCE_STATE */
	scha_rsstate_t 		r_state_node;	/* SCHA_RESOURCE_STATE_NODE */
	scds_sysdef_props_t	*r_sys_props;
	scds_common_extprops_t	*r_ext_props;
} scds_r_t;


/*
 * This struct holds all info pertaining to an RG's properties info
 * which is available through the API call: scha_resourcegroup_get().
 */
typedef struct scds_rg
{
	scha_str_array_t	*rg_nodelist;
	int			rg_max_primaries;
	int			rg_desired_primaries;
	scha_str_array_t	*rg_glb_rs_used;
	scha_str_array_t	*rg_rlist;
	scha_rgmode_t		rg_mode;
	boolean_t		rg_impl_net_depend;
	char			*rg_pathprefix;
	int			rg_ppinterval;
	char			*rg_project_name;
	scha_str_array_t	*rg_affinities;
	/* SC SLM addon start */
	char			*rg_slm_type;
	char			*rg_slm_pset_type;
	int			rg_slm_cpu;
	int			rg_slm_cpu_min;
	/* SC SLM addon end */
} scds_rg_t;

/*
 * Comprehensive struct for the current Resource and its RT, and the RG which
 * the Resource resides.
 */
typedef struct scds_prop_table
{
	char			*pt_rname;
	char			*pt_rtname;
	char			*pt_rgname;
	scds_r_t		*pt_r;
	scds_rg_t		*pt_rg;
	scds_rt_t		*pt_rt;
	char			*pt_current_method;
	scds_method_opcode_t	pt_opcode;	/* for validate only */
	char		*pt_zonename;	/* for RTs with GLOBAL_ZONE=true */
} scds_prop_table_t;

/*
 * This structure keeps a record of each probe. Performance values are
 * not kept because they are kept track of "on the fly" in the
 * probe_summary structure;
 */
struct probe_record {
	struct	probe_record		*p_next;	/* Linked list */
	hrtime_t	p_tstamp;	/* Timestamp for this probe result */
	/* Result for this probe: 0=success;100=failed;0<->100=partial */
	int		p_status;
};

/*
 * This structure keeps summary of a list of probe records kept in a
 * linked list. The linked list is kept of entrys in last "s_interval" seconds
 */
typedef struct scds_probe_summary {
	/* List of past probe results */
	struct	probe_record		*s_list;
	/* Current count of failures */
	int				s_current;
	/* Smoothed performance value */
	ulong_t				s_elapsed_msec;
} scds_probe_summary_t;

/*
 * This struct holds all info that gets initialized in the
 * scds_initialize function. This single structure will used
 * in all the get routines etc. to retrieve the data
 */
typedef struct scds_priv_handle
{
	int scds_argc;
	char **scds_argv;
	mutex_t		scds_mutex;
	boolean_t	is_zone_cluster;
	scds_prop_table_t ptable;
	scha_resource_t rs_handle;
	scha_resourcetype_t rt_handle;
	scha_resourcegroup_t rg_handle;
	scds_probe_summary_t *probe_summary;
	int		unix_sock;	/* Socket for PMF action script */
	int		new_sock;	/* Recieve request on this socket */
} scds_priv_handle_t;


/*
 * This part of the library provides an interface between client applications
 * and the RGMD for SSM services.
 */

/* Extension propertyname of net resources */

#define	SCDS_EXTPROP_HOSTNAMELIST "HostnameList"

/*
 * Some definitions for extended resource properties.
 * These must EXACTLY match with the defn in the RTR file.
 * Most of these relate to scalable mode.
 * XXX Move these to common code. Useful for SWS, Apache etc.
 */

#define	SCDS_EXTPROP_SCNET_R_USED	"Shared_addresses_used"
#define	SCDS_EXTPROP_LB_POLICY		"Load_balancing_policy"
#define	SCDS_EXTPROP_LB_STRING		"Load_balancing_string"
#define	SCDS_EXTPROP_PROTO		"Protocol_family"
#define	SCDS_EXTPROP_PORTLIST		"Port_list"
#define	SCDS_EXTPROP_SECURE		"Secure"

/*
 * The ProtocolFamily extended property is actually an enum. The
 * scha_resource_get() API returns a "symbolic" string for
 * these.
 * We need to map those to the ssm_proto_t defined in ssm.h
 */

#define	SCDS_PROTO_TCP	"TCP"
#define	SCDS_PROTO_UDP	"UDP"
#define	SCDS_PROTO_NONE	"NONE"



/*
 * This function allocates the memory associated with the scds_prop_table_t
 * structure and fills in the value of rname, rgname and rtname from the
 * argument list.
 */
scha_err_t
scds_init_ptable(scds_priv_handle_t *h);

void
scds_syslog_initialize(char *resourceName, char *resourceGroupName,
    char *resourceTypeName, char *methodName);
void
scds_syslog_initialize_zone(char *zname,
    char *resourceName, char *resourceGroupName,
    char *resourceTypeName, char *methodName);

/*
 * Free all memory associated with the given "handle" structure
 */
void
scds_free_handle(scds_priv_handle_t **h);

/*
 * This procedure frees the elements in ptable but does not free up
 * ptable itself.
 */
void
scds_free_ptable_elements(scds_prop_table_t *ptable);


/*
 * scds_validate_probe_values()
 *
 * SUMMARY:
 *    Check if the probing values make sense. Uses the following
 *    formula to check for valid probe values:
 *
 *    probe_timeout * retry_count <= retry_interval
 *
 * PARAMETERS:
 *    void
 *
 * RETURN TYPE:   scha_err_t
 *
 * RETURN VALUES:
 *         SCHA_ERR_NOERR
 *
 */
scha_err_t
scds_validate_probe_values(scds_priv_handle_t *handle,
    scds_method_opcode_t pt_opcode);

/*
 * This function parses the arguments and based on the options it calls
 * scds_process_xyz() function to set the new property value into ptable:
 *	-r	scds_process_sysdef_prop()
 *	-x	scds_process_common_extprop()
 *	-g	scds_process_rg_prop()
 *
 * For updating Resource or RG, only the updating arguments are passed to
 * validate method.  In this case, scds_get_r_info() and scds_get_rg_info()
 * will be called first to fill the original property values into ptable
 * before parsing the arguments.
 */
scha_err_t
scds_process_ptable(scds_priv_handle_t *h);

/*
 * Parse the input string which is in "keyword=value,value,..." format and
 * set the property value in the corresponding field of the output structure.
 */
scha_err_t
scds_process_sysdef_prop(char *r_prop, scds_sysdef_props_t **props);

scha_err_t
scds_process_common_extprop(char *x_prop, scds_common_extprops_t **props);

scha_err_t
scds_process_rg_prop(char *rg_prop, scds_rg_t **rg_info);

scha_err_t
scds_fm_init(scds_priv_handle_t *handle);

/*
 * Get the r, rg or rt information from CCR
 */
scha_err_t
scds_get_r_info(char *zname,
    char *r_name, scha_resource_t rs_handle, scds_r_t **r_info,
    boolean_t is_zone_cluster);

scha_err_t
scds_get_rg_info(char *zname, char *rg_name, scha_resourcegroup_t rg_handle,
    scds_rg_t **rg_info);

scha_err_t
scds_get_rt_info(char *zname, char *rt_name, scha_resourcetype_t rt_handle,
    scds_rt_t **rt_info);

scha_err_t
scds_retrieve_ext_props(const char *zname, scha_resource_t rs_handle,
    const char *prop_name, scha_extprop_value_t **xval);

/*
 * Sub-functions for getting network resources
 */

/*
 * Get the IP addresses from multiple resources.
 *
 * Parameters:
 * 	r_array		(IN)  - resources to get IP addresses from. These
 * 				can be a mixture of network and non-network
 * 				resources.
 * 	snrl		(OUT) - the struct scds_net_resource_list will be
 * 				created and filled in with the network
 * 				resource information for each network
 * 				resource in the r_array. The pointer to the
 * 				scds_net_resource_list pointer  will be
 * 				set to point to the scds_net_resource_list
 * 				that the library will allocate.
 */
scha_err_t
scds_get_ipaddrs_from_rlist(char *zname, scha_str_array_t *r_array,
	scds_net_resource_list_t **snrl);

/*
 * Get the IP addresses from one network resource.
 *
 * Parameters:
 * 	net_rame	(IN)  - name of the network resource
 * 	snr		(OUT) - scds_net_resource struct filled in with
 * 				the network data for this resource (caller
 * 				allocated)
 */
scha_err_t
scds_get_ipaddrs_from_r(char *zname, scha_resource_t net_rs_handle,
	scds_net_resource_t *snr);

/*
 * Free the memory associated with one scds_net_resource.
 *
 * Parameters:
 * 	snr		(IN)  - pointer to scds_net_resource
 *
 * Assumptions:
 * 	snr		The pointer passed in is a valid pointer to a
 * 			scds_net_resource.
 */
scha_err_t
scds_free_net_resource(scds_net_resource_t *snr);

/*
 * Get the nodeid of a cluster node.
 *
 * Parameters:
 * 	nodename	(IN)  - the name of the cluster node
 * 	nodeid		(OUT) - pointer will be filled in with the nodeid
 * 				that corresponds to the nodename (caller
 * 				allocated)
 */
static scha_err_t
scds_get_nodeid(const char *zonename, char *nodename, int *nodeid);

/*
 * Set scds_syslog_debuglevel to the number specified in
 * 	/var/opt/cluster/rt/<rtname>/loglevel file
 */
void
scds_syslog_set_debug_level(char *rtname);

/*
 * Parse an sys prop value of Port_list, validate it, and pass out
 * a 3-tuple structure.
 *
 * Input Arguments
 *   - p_port_list       : denormalized list of string elements of the form
 *                         "<ip>/<port num>/<protocol>".
 *   - valid_proto_table : table of acceptable protocol families  that maps
 *                       valid string label to netinet/in.h-defined macro.
 *   - is_scalable       : whether or not the underlying Port_list is for
 *                       a scalable resource.  If so, the port_list can
 *                       not be empty.  Otherwise it can be.
 * Output Arguments
 *   - netaddr_list      : the parsed and format-validated list of netaddrs.
 *
 *
 * Note: the caller is expected to:
 *       - allocate mem for the output netaddr_list because the
 *         list size of this is known
 *       - later ensure that free_normal_port_list() is called to
 *         free this allocated mem.
 *
 * Return Values:
 *   - SCHA_ERR_NOERR
 *   - SCHA_ERR_INVAL
 */
int scds_parse_port_list(scha_str_array_t   *port_list,
    boolean_t		   is_scalable,
    scds_netaddr_list_t    **netaddr_list);

/*
 * This function checks whether the current portnumber/proto is already
 * existed in the port_list structure or not.
 *
 * Parameters:
 *      p		(IN)  - The port_list structure which has
 * 				the number of ports so far filled and the
 *				port/proto entries.
 *      current         (IN)  - The structure which holds the port/proto entries
 * Return Values
 *   IS_EXISTS		- The current port/proto pair is already in
 *                        the port_list structure.
 *   IS_NOT_EXISTS      - The current port/proto pair does not exists
 *			  in the por_list structure.
 */
int scds_is_port_exists(scds_port_list_t **p, scds_port_t current);


/* functions pulled out from libdssdk.h */

/* Debugging function to print the scds_prop_table structure at debug level */
void
scds_print_ptable(scds_priv_handle_t h, int debug_level);


/*
 * Get the value of non-common extension property from CCR by calling the
 * scha_resource_get().
 *
 * Parameters:
 * 	handle		(IN)  - handle that holds all the data
 * 	propname	(IN)  - the name of the given extension property
 * 	xprop		(OUT) - the value of the given extension property
 * 	nname		(IN)  - the nodename on which the value is to be
 * 				retrieved (for per-node extn props)
 *
 * Return Code:
 * 	SCHA_ERR_NOERR	property found in ccr, store the prop value in xprop
 * 	SCHA_ERR_INVAL	property not found in ccr
 */
scha_err_t
scds_get_ext_prop_from_conf(scds_priv_handle_t *h, const char *propname,
    scha_extprop_value_t **xprop, char *nname);


/*
 * Get extension property value from the argument list passed from command
 * line which is not stored in the scds_prop_table_t structure.
 *
 * Parameters:
 * 	handle		(IN)  - handle that holds all the data
 * 	propname	(IN)  - the name of the given extension property
 * 	xprop_type	(IN)  - the date type of the given extension property
 * 	xprop		(OUT) - the value of the given extension property
 * 	nname		(IN)  - the nodename at which the extension property
 * 				is to be retrieved
 *
 * Return Code:
 * 	SCHA_ERR_NOERR	property found in args, store the prop value in xprop
 * 	SCHA_ERR_INVAL	property not found in args
 */
scha_err_t
scds_get_ext_prop_from_args(scds_priv_handle_t *h, const char *propname,
    scha_prop_type_t xprop_type, scha_extprop_value_t **xprop, char *nname);

/*
 * Get the nodeid where an RG is currently mastered.
 *
 * Parameters:
 * 	rgname		(IN)  - the name of the RG
 * 	nodeid		(OUT) - pointer will be filled in with the nodeid
 * 				(caller allocated) that corresponds to the
 * 				nodename. If the RG is not online, the
 * 				nodeid will be set to SCDS_INVALID_NODEID
 */
scha_err_t
scds_get_rg_primary_node(char *zname, char *rgname, int *nodeid);


/*
 * scds_fm_sockname(): The UNIX pathname of the UNIX socket, given
 * the RG name and the R name.
 */
char*
scds_fm_sockname(char *resourceGroupName, char *resourceName);


/*
 * Get the name of the tag for this resource and type.
 *
 * 	Tag format: rg_name,rs_name,index<.svc|.mon|>  (subject to change)
 *
 * Parameters:
 *	handle		(IN)  - returned by earlier call to scds_initialize
 * 	type		(IN)  - what type of program the tag is for.
 * 				This will determine the tag suffix that
 * 				is used.
 * 	index		(IN)  - for resources where there are multiple
 * 				instances in one resource, the index will
 * 				identify the instance uniquely. This should
 * 				correspond to this resource's index into the
 * 				config dir array.
 * 	tagbufsize	(IN)  - size of the tag buffer that is passed in.
 * 	tagbuf		(OUT) - will be filled in with the tag name
 *
 * Return Value:
 *	SCHA_ERR_NOERR on success.
 *	SCHA_ERR_INVAL if the resource group name or resource name was too long.
 *	SCHA_ERR_INTERNAL on other errors.
 */
scha_err_t
priv_pmf_create_tag_name(scds_priv_handle_t *ph, scds_pmf_type_t type,
    int index, size_t tagbufsize, char *tagbuf);

/*
 * Duplicate the contents of the supplied ext property.
 *
 * Return Value:
 * 	NULL on failure
 * 	a pointer to the newly allocate extension property
 *
 * Caller is supposed to free the ext property when its done
 * with it. scds_free_ext_property can be used for this.
 */
scha_extprop_value_t *
dup_ext_property(scha_extprop_value_t *prop);

int scds_fm_opensock(char *rg, char *rs);
scha_err_t scds_fm_sendreply(scds_priv_handle_t *h, int response);

static scha_err_t
scds_get_net_resource_type(char *zname, scha_resourcetype_t net_rs_handle,
	boolean_t *is_net_type);

scha_err_t scds_fm_closesock(scds_priv_handle_t *h);

scha_err_t scds_fm_getmsg(scds_priv_handle_t *h, char *buf, int size);

void scds_fm_close(scds_priv_handle_t *h);

scds_priv_handle_t *check_handle(scds_handle_t handle);

scha_err_t priv_get_ext_property(scds_priv_handle_t *handle,
    const char *propname, scha_prop_type_t xprop_type,
    scha_extprop_value_t **xprop);

scha_err_t priv_get_ext_property_node(scds_priv_handle_t *handle,
    const char *propname, scha_prop_type_t xprop_type,
    scha_extprop_value_t **xprop, char *nname);


#ifdef	__cplusplus
}
#endif

#endif	/* _LIBDSDEV_PRIV_H */
