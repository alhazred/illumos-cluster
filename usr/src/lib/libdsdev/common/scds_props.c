/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * scds_props.c - Various properties for data services which use
 *				the SC API
 */

#pragma ident	"@(#)scds_props.c	1.105	09/03/06 SMI"

/*
 * Suppress lint message: "Info(793) [c:23]: ANSI limit of 511
 * 'external identifiers' exceeded -- processing is unaffected"
 */
/*lint -e793 */

#include <sys/os.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <libgen.h>
#include <sys/wait.h>
#include <errno.h>
#include <scha.h>
#include <rgm/scha_strings.h>
#include <string.h>
#include <libgen.h>
#include <sys/sol_version.h>
#include <rgm/libdsdev.h>
#include "libdsdev_priv.h"

#define	get_log_priority(x)	((x == SCHA_ERR_SEQID) ? LOG_DEBUG : LOG_ERR)

static int str_kv(char *s, char **key, char **val);
static scha_err_t str_to_int(char *s, int *rc);
static boolean_t str_to_bool(char *s);
static scha_rgmode_t str_to_rgmode(char *s);
static scha_err_t str_to_strarray(char *s, scha_str_array_t **);
static char *strarray_to_str(scha_str_array_t *sa);
static void save_getopt_vars(char **o_optarg, int *o_optind, int *o_opterr,
    int *o_optopt);
static void restore_getopt_vars(char *o_optarg, int o_optind, int o_opterr,
    int o_optopt);

static int string_to_code(char *string);
static scha_err_t process_wildcard_value(char *str, scha_str_array_t **);
static char *my_strtok(char **str, char delimiter);
static scha_err_t scds_get_fullname_helper(const char *zname, char **fullname,
    boolean_t is_zone_cluster, boolean_t use_nodeid);

#define	HAFOIP_RT "SUNW.LogicalHostname"
#define	HASCIP_RT "SUNW.SharedAddress"

/*
 * scds_initialize()
 *
 * SUMMARY:
 *	Allocate and initialize DSDL environment.
 *	Initialize logging and the property table for the resource.
 *	This function must be called at the start of each data
 *	service method.
 *
 * PARAMETERS:
 *	handle (OUT) - initialized here and passed to other DSDL functions
 *	argc   (IN)  - the number of arguments passed to the method
 *	argv   (IN)  - the arguments to the method
 *
 * RETURN TYPE:	scha_err_t
 *
 * scds_initialize does not acquire the mutex, because this is the routine
 * responsible for creating the handle. Hence, no other DSDL routines can be
 * called from this or other threads until this routine finishes.
 *
 */
scha_err_t
scds_initialize(scds_handle_t *handle, int argc, char *argv[])
{
	scha_err_t	rc;
	scds_priv_handle_t *h;
	scha_cluster_t clusterhandle;
	char *zonetype;
	size_t size = 1;
	*handle = NULL;
	/*
	 * If scds_initialize is called from validate method then send the
	 * error messages to stderr else send the messages to syslog
	 */
	h = (scds_priv_handle_t *)calloc(size, sizeof (scds_priv_handle_t));
	if (h == NULL) {
		scds_syslog(LOG_ERR, "Out of memory.");
		return (SCHA_ERR_NOMEM);
	}

	h->scds_argc = argc;
	h->scds_argv = argv;

	/*
	 * Parses the arguments and updates the handle with
	 * Resource, RG, RT names and method code.
	 */

	rc = scds_init_ptable(h);


	if (rc != SCHA_ERR_NOERR) {
		if (h->ptable.pt_opcode != SCDS_OP_NONE) {
			fprintf(stderr,
			    "Failed to retrieve the resource information.");
		} else {
			/*
			 * SCMSGS
			 * @explanation
			 * A Sun Cluster data service is unable to retrieve the
			 * resource information. Low memory or API call
			 * failure might be the reasons.
			 * @user_action
			 * In case of low memory, the problem will probably
			 * cured by rebooting. If the problem recurs, you
			 * might need to increase swap space by configuring
			 * additional swap devices. Otherwise, if it is API
			 * call failure, check the syslog messages from other
			 * components.
			 */
			scds_syslog(LOG_ERR,
			    "Failed to retrieve the resource information.");
		}
		scds_free_handle(&h);
		return (rc);
	}

	/*
	 * scds_init_ptable initialises the zone field. It will be set to -Z
	 * if the method is running in global zone while it was supposed to
	 * run in local zone. This could be true both for 1334 zone as well as
	 * zone cluster. All the scha calls will now use the _zone version, and
	 * pass the zonename stored in the handle. This will be used for
	 * namespace scoping for zone cluster, as well
	 * as action scoping for 1334 zones.
	 */
	rc = scha_cluster_open_zone(h->ptable.pt_zonename, &clusterhandle);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the resource information.");
	}
	rc = scha_cluster_get_zone(h->ptable.pt_zonename, clusterhandle,
	    SCHA_ZONE_TYPE, &zonetype);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the resource information.");
	}
	if (strcasecmp(zonetype, "CLUSTER") == 0) {
		h->is_zone_cluster = B_TRUE;
	} else {
		h->is_zone_cluster = B_FALSE;
	} /* should all this be done in init_ptable */

	(void) scha_cluster_close_zone(h->ptable.pt_zonename, clusterhandle);
	/* no need to free zonetype. cluster_close will free it. */

	/*
	 * Creates the syslog tag and does the openlog call for syslog
	 * messages
	 */
	scds_syslog_initialize_zone(h->ptable.pt_zonename, h->ptable.pt_rname,
	    h->ptable.pt_rgname, h->ptable.pt_rtname,
	    h->ptable.pt_current_method);

	/*
	 * Populates the paramtable structure by doing calls to scha API
	 */

	rc = scds_process_ptable(h);

	if (rc != SCHA_ERR_NOERR) {
		if (h->ptable.pt_opcode != SCDS_OP_NONE) {
			fprintf(stderr,
			    "Failed to process the resource information.");
		} else {
			/*
			 * SCMSGS
			 * @explanation
			 * A Sun cluster data service is unable to retrieve the
			 * resource property information. Low memory or API
			 * call failure might be the reasons.
			 * @user_action
			 * In case of low memory, the problem will probably
			 * cured by rebooting. If the problem recurs, you
			 * might need to increase swap space by configuring
			 * additional swap devices. Otherwise, if it is API
			 * call failure, check the syslog messages from other
			 * components.
			 */
			scds_syslog(LOG_ERR,
			    "Failed to process the resource information.");
		}
		scds_free_handle(&h);
		return (rc);
	}

	/*
	 * Verifies whether the Retry_interval is greater than or equal to"
	 * the product of Thorough_probe_interval, and Retry_count.
	 *
	 * This check is not made for haip resource types. They shipped with
	 * values in the RTR file that would cause this test to fail.
	 */

	if ((strncasecmp(h->ptable.pt_rtname, HAFOIP_RT,
	    sizeof (HAFOIP_RT) - 1) != 0) &&
	    (strncasecmp(h->ptable.pt_rtname, HASCIP_RT,
	    sizeof (HASCIP_RT) - 1) != 0)) {
		scds_validate_probe_values(h, h->ptable.pt_opcode);
	}

	/*
	 * Initializes the variables that are used to store the
	 * probe history
	 */
	rc = scds_fm_init(h);

	if (rc != SCHA_ERR_NOERR) {
		if (h->ptable.pt_opcode != SCDS_OP_NONE) {
			fprintf(stderr,
			    "Failed to initialize the probe history.");
		} else {
			/*
			 * SCMSGS
			 * @explanation
			 * A process has failed to allocate memory for the
			 * probe history structure, most likely because the
			 * system has run out of swap space.
			 * @user_action
			 * To solve this problem, increase swap space by
			 * configuring additional swap devices. See swap(1M)
			 * for more information.
			 */
			scds_syslog(LOG_ERR,
			    "Failed to initialize the probe history.");
		}
		scds_free_handle(&h);
		return (rc);
	}

	(void) mutex_init(&(h->scds_mutex), USYNC_THREAD, NULL);

	*handle = (void *)h;

	/* Create the directory for UNIX sockets used by fault monitor */
	if (mkdirp(SCDS_TMPDIR, (mode_t)0777) != 0) {
		/*
		 * errno.h is missing void arg, so suppress lint error.
		 * [should be errno(void) rather than errno()]
		 */
		if (errno != EEXIST) {		/*lint !e746 */
			if (h->ptable.pt_opcode != SCDS_OP_NONE) {
				fprintf(stderr,
				    "Unable to create directory %s: %s.",
				    SCDS_TMPDIR, strerror(errno));
			} else {
				/*
				 * SCMSGS
				 * @explanation
				 * A call to scds_initialize() failed because
				 * it was unable to create a directory required
				 * for fault monitoring. The reason for the
				 * failure is included in the message.
				 * This might cause an RGM-managed service
				 * to fail.
				 * @user_action
				 * Examine other syslog messages occurring
				 * around the same time.
				 * Take corrective action based upon the
				 * contents of the error messages.
				 */
				scds_syslog(LOG_ERR,
				    "Unable to create directory %s: %s.",
				    SCDS_TMPDIR, strerror(errno));
			}
			return (SCHA_ERR_INTERNAL);
		}
	}

	return (rc);
}

/*
 * Return the first token in the input str as delimited
 * by the delimiter or the end of the string (which
 * ever comes first.
 * The delimiter is replaced with a terminating '\0'.
 *
 * The input str is also modified to point to
 * the first character after to delimiter, i.e.
 * str is adjusted to point to the start of
 * the next token after the one just returned.
 *
 * Note that the original input string is destroyed
 * in the course of breaking-off tokens.
 */
char *
my_strtok(char **str, char delimiter)
{
	char *temp, *temp1;
	if (*str == NULL)
		return (NULL);

	temp = (char *)strchr(*str, delimiter);
	if (temp == NULL) {
		temp = *str;
		*str = NULL;
	} else {
		temp1 = temp + sizeof (char);
		*temp = '\0';
		temp = *str;
		*str = temp1;

	}
	return (temp);
}

/*
 * scds_init_ptable()
 *
 * SUMMARY: This function populates the "handle" structure with the
 * value of rname, rgname, rtname and opcode from the argument list.
 *
 * Parameters:
 *	handle			- will be filled in with the rname,
 *				  rgname, rtname, and opcode.
 * Return Value:
 *	SCHA_ERR_NOERR		- Successfully populated the structure
 *	SCHA_ERR_INTERNAL	- Invalid option in the supplied argument list
 *
 * Neither acquires not rquires the scds_mutex to be held, since it's only
 * called from scds_initialize. If it's ever called from elsewhere, the caller
 * must acquire the mutex before calling this function.
 *
 */
scha_err_t
scds_init_ptable(scds_priv_handle_t *handle)
{
	int c;
	char    internal_err_str[INT_ERR_MSG_SIZE];
	char	*o_optarg;
	int	o_optind, o_opterr, o_optopt;

	/* Verify the number of arguments passed in */
	if (handle->scds_argc < 7) {
		/*
		 * SCMSGS
		 * @explanation
		 * Incorrect arguments are passed to the callback method of a
		 * cluster data service agent.
		 * @user_action
		 * This error might occur while developing and testing a new
		 * agent, if a method is manually invoked with incorrect
		 * command line arguments. If seen on a production cluster,
		 * this message indicates an internal error. Contact your
		 * authorized Sun service provider for assistance in
		 * diagnosing and correcting the problem.
		 */
		scds_syslog(LOG_ERR,
		    "Usage: %s [-c|-u] -R <resource-name> "
		    "-T <type-name> -G <group-name> "
		    "[-r <sys_def_prop>=<value> ...] "
		    "[-x <ext_prop>=<value> ...] "
		    "[-Z <zone-name>]",
		    handle->scds_argv[0]);
		return (SCHA_ERR_INVAL);
	}

	handle->ptable.pt_current_method =
	    strdup(basename(handle->scds_argv[0]));
	if (handle->ptable.pt_current_method == NULL) {
		scds_syslog(LOG_ERR, "Out of memory.");
		return (SCHA_ERR_NOMEM);
	}

	handle->ptable.pt_opcode = SCDS_OP_NONE;
	handle->ptable.pt_zonename = NULL;

	/*
	 * Before calling getopt(), save the original values of getopt
	 * related variables.  These values will be restored whenever
	 * exiting the while loop of getopt().
	 */
	save_getopt_vars(&o_optarg, &o_optind, &o_opterr, &o_optopt);

	/*
	 * Traverse the argument list and update the handle structure
	 * Valid options are:
	 * c - create, u - update
	 * R - Resource Name, T - RT Name, G - RG Name, Z - zonename
	 * r - resource properties, g - RG properties
	 * x - Extension properties
	 * X - per node extension properties.(currently ignored)
	 */
	while ((c = getopt(handle->scds_argc,
	    handle->scds_argv, "R:T:G:r:x:g:X:cuZ:")) != EOF) {
		switch (c) {
		case 'c':
			handle->ptable.pt_opcode = SCDS_OP_CREATE_R;
			break;
		case 'u':
			handle->ptable.pt_opcode = SCDS_OP_UPDATE_R;
			break;
		case 'R':
			handle->ptable.pt_rname = optarg;
			break;
		case 'T':
			handle->ptable.pt_rtname = optarg;
			break;
		case 'G':
			handle->ptable.pt_rgname = optarg;
			break;
		case 'r':
		case 'x':
		case 'X':
			break;
		case 'g':
			if (handle->ptable.pt_opcode != SCDS_OP_CREATE_R)
				handle->ptable.pt_opcode = SCDS_OP_UPDATE_RG;
			break;
		case 'Z':
			handle->ptable.pt_zonename = optarg;
			break;
		default:
			(void) snprintf(internal_err_str,
			    sizeof (internal_err_str),
			    "Invalid option -%c is provided", optopt);
			scds_syslog(LOG_ERR,
			    "INTERNAL ERROR: %s.", internal_err_str);
			/* retore the original values */
			restore_getopt_vars(o_optarg, o_optind, o_opterr,
			    o_optopt);
			return (SCHA_ERR_INTERNAL);
		}
	}

	restore_getopt_vars(o_optarg, o_optind, o_opterr, o_optopt);
	return (SCHA_ERR_NOERR);
}

/*
 * scds_process_ptable()
 *
 * SUMMARY: This function parses the arguments and then calls appropriate
 * methods depending on the options. If option is,
 *
 * creation of resource (SCDS_OP_CREATE_R): the argument list will contain
 * the R and RG properties. Just allocates memory for R and RG sturctures
 * here, parsing of these arguments will be done later.
 *
 * Update/validate: calls the scds_process_xyz() function which updates
 * the related structures with new values.
 *	-r	scds_process_sysdef_prop()
 *	-x	scds_process_common_extprop()
 *	-g	scds_process_rg_prop()
 *
 * For updating Resource or RG, only the updating arguments are passed to
 * validate method.  In this case, scds_get_r_info() and scds_get_rg_info()
 * will be called first to fill the original property values into ptable
 * before parsing the arguments.
 *
 * Parameters:
 *	handle			- R, RT and RG structures will be filled.
 * Return Value:
 *	SCHA_ERR_NOERR		- Successfully populated the structure
 *	SCHA_ERR_INTERNAL	- Invalid option in the supplied argument list
 *
 * Neither acquires not rquires the scds_mutex to be held, since it's only
 * called from scds_initialize. If it's ever called from elsewhere, the caller
 * must acquire the mutex before calling this function.
 *
 */
scha_err_t
scds_process_ptable(scds_priv_handle_t *handle)
{
	int c;
	scha_err_t err;
	size_t size = 1;
	char	*o_optarg;
	int	o_optind, o_opterr, o_optopt;

	/*
	 * Open the current RG and RT handle
	 * Note: R handle cannot be opened here for the case of resource
	 * creation.
	 */

retry_again:

	err = scha_resourcegroup_open_zone(handle->ptable.pt_zonename,
	    handle->ptable.pt_rgname,
	    &(handle->rg_handle));
	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the resource group handle: %s.",
		    scds_error_string(err));
		return (SCHA_ERR_HANDLE);
	}

	err = scha_resourcetype_open_zone(handle->ptable.pt_zonename,
	    handle->ptable.pt_rtname, &(handle->rt_handle));
	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the resource type handle: %s.",
		    scds_error_string(err));
		return (SCHA_ERR_HANDLE);
	}

	err = scds_get_rt_info(handle->ptable.pt_zonename,
	    handle->ptable.pt_rtname,
	    handle->rt_handle, &(handle->ptable.pt_rt));
	if (err != SCHA_ERR_NOERR) {
		goto finished;
	}

	scds_syslog_debug(2, "Debug: OP ");

	if (handle->ptable.pt_opcode == SCDS_OP_CREATE_R) {

		scds_syslog_debug(2, "Debug: OP_CRE");
		/*
		 * For resource creation, the argument list will contain
		 * all of the R and RG properties.
		 * Therefore, we only allocate the memory for the R and
		 * RG structures here.  The values will be filled
		 * in later when parsing the argument list.
		 */
		handle->ptable.pt_rg =
		    (scds_rg_t *)calloc(size, sizeof (scds_rg_t));
		if (handle->ptable.pt_rg == NULL) {
			scds_syslog(LOG_ERR,
			    "Out of memory.");
			err = SCHA_ERR_NOMEM;
			goto finished;
		}

		handle->ptable.pt_r =
		    (scds_r_t *)calloc(size, sizeof (scds_r_t));
		if (handle->ptable.pt_r == NULL) {
			scds_syslog(LOG_ERR,
			    "Out of memory.");
			err = SCHA_ERR_NOMEM;
			goto finished;
		}

		handle->ptable.pt_r->r_sys_props = (scds_sysdef_props_t *)
		    calloc(size, sizeof (scds_sysdef_props_t));

		if (handle->ptable.pt_r->r_sys_props == NULL) {
			scds_syslog(LOG_ERR,
			    "Out of memory.");
			err = SCHA_ERR_NOMEM;
			goto finished;
		}

		handle->ptable.pt_r->r_ext_props = (scds_common_extprops_t *)
		    calloc(size, sizeof (scds_common_extprops_t));
		if (handle->ptable.pt_r->r_ext_props == NULL) {
			scds_syslog(LOG_ERR,
			    "Out of memory.");
			err = SCHA_ERR_NOMEM;
			goto finished;
		}
	} else {
		/* Open the current R using the rs_handle */
		err = scha_resource_open_zone(handle->ptable.pt_zonename,
		    handle->ptable.pt_rname,
		    handle->ptable.pt_rgname, &(handle->rs_handle));
		if (err != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR,
			    "Failed to retrieve the resource handle: %s.",
			    scds_error_string(err));
			return (SCHA_ERR_HANDLE);
		}
		err = scds_get_r_info(handle->ptable.pt_zonename,
		    handle->ptable.pt_rname,
		    handle->rs_handle, &(handle->ptable.pt_r),
		    handle->is_zone_cluster);
		if (err != SCHA_ERR_NOERR) {
			goto finished;
		}

		err = scds_get_rg_info(handle->ptable.pt_zonename,
		    handle->ptable.pt_rgname,
		    handle->rg_handle, &(handle->ptable.pt_rg));
		if (err != SCHA_ERR_NOERR) {
			goto finished;
		}

	}

	/* done for any method except update and validate */
	if (handle->ptable.pt_opcode  == SCDS_OP_NONE)
		return (SCHA_ERR_NOERR);

	scds_syslog_debug(2, "Debug: NOT OP_NONE");

	/*
	 * Before calling getopt(), save the original values of getopt
	 * related variables.  These values will be restored whenever
	 * exiting the while loop of getopt().
	 */
	save_getopt_vars(&o_optarg, &o_optind, &o_opterr, &o_optopt);

	/* Parsing the argument list; bypass -c|u -R -G -T -Z -X */

	while ((c = getopt(handle->scds_argc,
	    handle->scds_argv, "R:T:G:r:x:X:g:cuZ:")) != EOF) {
		switch (c) {
		case 'r':
			err = scds_process_sysdef_prop(optarg,
				&(handle->ptable.pt_r->r_sys_props));
			if (err != SCHA_ERR_NOERR) {
				restore_getopt_vars(o_optarg, o_optind,
				    o_opterr, o_optopt);
				goto finished;
			}
			break;

		case 'x':
			/*
			 * The following function fills in only the common
			 * extension properties. scds_get_ext_property is
			 * useful to retrieve the non-extension properties.
			 */
			err = scds_process_common_extprop(optarg,
				&(handle->ptable.pt_r->r_ext_props));
			if (err != SCHA_ERR_NOERR) {
				restore_getopt_vars(o_optarg, o_optind,
				    o_opterr, o_optopt);
				goto finished;
			}
			break;

		case 'g':
			err = scds_process_rg_prop(optarg,
			    &(handle->ptable.pt_rg));
			if (err != SCHA_ERR_NOERR) {
				restore_getopt_vars(o_optarg, o_optind,
				    o_opterr, o_optopt);
				goto finished;
			}
			break;
		case 'R':
		case 'G':
		case 'T':
		case 'c':
		case 'u':
		case 'Z':
		case 'X':
			break;
		default:
			/*
			 * SCMSGS
			 * @explanation
			 * Invalid option is passed to validate call back
			 * method.
			 * @user_action
			 * This is an internal error. Contact your authorized
			 * Sun service provider for assistance in diagnosing
			 * and correcting the problem.
			 */
			scds_syslog(LOG_ERR,
			    "Invalid option -%c for the validate method.",
			    optopt);
			err = SCHA_ERR_INVAL;
			restore_getopt_vars(o_optarg, o_optind, o_opterr,
			    o_optopt);
			goto finished;
		}
	}

	restore_getopt_vars(o_optarg, o_optind, o_opterr, o_optopt);

	scds_syslog_debug(2, "Debug: Done getopt");
	/*
	 * Resource list is not a RG property and it won't appear in arg list
	 * Need to get the value separately through api in the case of
	 * creating resource
	 */
	if (handle->ptable.pt_opcode == SCDS_OP_CREATE_R) {
		err = scha_resourcegroup_get_zone(handle->ptable.pt_zonename,
		    handle->rg_handle, SCHA_RESOURCE_LIST,
		    &(handle->ptable.pt_rg->rg_rlist));
		if (err != SCHA_ERR_NOERR) {
			scds_syslog(get_log_priority(err),
			    "Failed to retrieve the "
			    "resource group property %s: %s.",
			    SCHA_RESOURCE_LIST, scds_error_string(err));
			goto finished;
		}
	}

	return (SCHA_ERR_NOERR);

finished:

	if (err == SCHA_ERR_SEQID) {

		scds_syslog(get_log_priority(err),
		    "Retrying to retrieve the resource information.");

		/* free up the elements in ptable but not the ptable itself */
		scds_free_ptable_elements(&(handle->ptable));

		/* close the resource handle */
		(void) scha_resource_close_zone(handle->ptable.pt_zonename,
		    handle->rs_handle);
		handle->rs_handle = NULL;

		/* close the resource type handle */
		(void) scha_resourcetype_close_zone(handle->ptable.pt_zonename,
		    handle->rt_handle);
		handle->rt_handle = NULL;

		/* close the resource group handle */
		(void) scha_resourcegroup_close_zone(handle->ptable.pt_zonename,
		    handle->rg_handle);
		handle->rg_handle = NULL;

		(void) sleep(1);
		goto retry_again;
	} else {
		scds_syslog(LOG_ERR,
		    "Failed to process the resource information.");
		return (err);
	}
}


/*
 * scds_process_sysdef_prop()
 *
 * SUMMARY:
 * Parse the input system-defined property string which is in
 * "keyword=value,value,..." format and set the property value in the
 * corresponding field of the output structure.  Note that this function
 * overwrites the value in *props which is retrieved from ccr.
 *
 * PARAMETERS:
 *	r_prop (IN)	- Argument that contains list of properties and values
 *			  in the "keyword=value, value, ..." format.
 *	props  (OUT)	- scds_sysdef_props_t struct will be updated with
 *			  new system defined property values
 *
 * RETURN VALUES:
 *	SCHA_ERR_NOERR	- Updating the scds_sysdef_props_t structure is
 *			  successful.
 *	SCHA_ERR_INVAL  - Invalid argument passed
 */
scha_err_t
scds_process_sysdef_prop(
    char *r_prop, scds_sysdef_props_t **props)
{
	char *key, *val;
	scha_err_t err;
	char    internal_err_str[INT_ERR_MSG_SIZE];

	if (r_prop == NULL)
		return (SCHA_ERR_INVAL);

	if (str_kv(r_prop, &key, &val) != SCHA_ERR_NOERR) {
		(void) snprintf(internal_err_str,
		    sizeof (internal_err_str),
		    "Invalid argument %s", r_prop);

		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		    internal_err_str);
		return (SCHA_ERR_INTERNAL);
	}

	if (strcasecmp(key, SCHA_CHEAP_PROBE_INTERVAL) == 0) {
		err = str_to_int(val, &(*props)->p_cprobe_interval);
		if (err != SCHA_ERR_NOERR)
			return (err);

	} else if (strcasecmp(key, SCHA_THOROUGH_PROBE_INTERVAL) == 0) {
		err = str_to_int(val, &(*props)->p_tprobe_interval);
		if (err != SCHA_ERR_NOERR)
			return (err);

	} else if (strcasecmp(key, SCHA_RETRY_COUNT) == 0) {
		err = str_to_int(val, &(*props)->p_retry_count);
		if (err != SCHA_ERR_NOERR)
			return (err);

	} else if (strcasecmp(key, SCHA_RETRY_INTERVAL) == 0) {
		err = str_to_int(val, &(*props)->p_retry_interval);
		if (err != SCHA_ERR_NOERR)
			return (err);

	} else if (strcasecmp(key, SCHA_FAILOVER_MODE) == 0) {
		if (strcasecmp(val, SCHA_NONE) == 0)
			(*props)->p_failover_mode = SCHA_FOMODE_NONE;
		else if (strcasecmp(val, SCHA_HARD) == 0)
			(*props)->p_failover_mode = SCHA_FOMODE_HARD;
		else if (strcasecmp(val, SCHA_SOFT) == 0)
			(*props)->p_failover_mode = SCHA_FOMODE_SOFT;
		else if (strcasecmp(val, SCHA_RESTART_ONLY) == 0)
			(*props)->p_failover_mode = SCHA_FOMODE_RESTART_ONLY;
		else
			(*props)->p_failover_mode = SCHA_FOMODE_LOG_ONLY;

	} else if (strcasecmp(key, SCHA_RESOURCE_DEPENDENCIES) == 0) {
		err = str_to_strarray(val, &(*props)->p_strong_dep);
		if (err != SCHA_ERR_NOERR)
			return (err);

	} else if (strcasecmp(key, SCHA_RESOURCE_DEPENDENCIES_RESTART) == 0) {
		err = str_to_strarray(val, &(*props)->p_restart_dep);
		if (err != SCHA_ERR_NOERR)
			return (err);

	} else if (strcasecmp(key, SCHA_RESOURCE_DEPENDENCIES_OFFLINE_RESTART)
	    == 0) {
		err = str_to_strarray(val, &(*props)->p_offline_restart_dep);
		if (err != SCHA_ERR_NOERR)
			return (err);

	} else if (strcasecmp(key, SCHA_RESOURCE_DEPENDENCIES_WEAK) == 0) {
		err = str_to_strarray(val, &(*props)->p_weak_dep);
		if (err != SCHA_ERR_NOERR)
			return (err);

	} else if (strcasecmp(key, SCHA_RESOURCE_DEPENDENCIES_Q) == 0) {
		err = str_to_strarray(val, &(*props)->p_strong_dep_q);
		if (err != SCHA_ERR_NOERR)
			return (err);

	} else if (strcasecmp(key, SCHA_RESOURCE_DEPENDENCIES_Q_RESTART) == 0) {
		err = str_to_strarray(val, &(*props)->p_restart_dep_q);
		if (err != SCHA_ERR_NOERR)
			return (err);

	} else if (strcasecmp(key, SCHA_RESOURCE_DEPENDENCIES_Q_OFFLINE_RESTART)
	    == 0) {
		err = str_to_strarray(val, &(*props)->p_offline_restart_dep_q);
		if (err != SCHA_ERR_NOERR)
			return (err);

	} else if (strcasecmp(key, SCHA_RESOURCE_DEPENDENCIES_Q_WEAK) == 0) {
		err = str_to_strarray(val, &(*props)->p_weak_dep_q);
		if (err != SCHA_ERR_NOERR)
			return (err);

	} else if (strcasecmp(key, SCHA_NETWORK_RESOURCES_USED) == 0) {
		err = str_to_strarray(val, &(*props)->p_network_resources_used);
		if (err != SCHA_ERR_NOERR)
			return (err);

	} else if (strcasecmp(key, SCHA_START_TIMEOUT) == 0) {
		err = str_to_int(val, &(*props)->p_start_timeout);
		if (err != SCHA_ERR_NOERR)
			return (err);

	} else if (strcasecmp(key, SCHA_STOP_TIMEOUT) == 0) {
		err = str_to_int(val, &(*props)->p_stop_timeout);
		if (err != SCHA_ERR_NOERR)
			return (err);

	} else if (strcasecmp(key, SCHA_MONITOR_STOP_TIMEOUT) == 0) {
		err = str_to_int(val, &(*props)->p_monitor_stop_timeout);
		if (err != SCHA_ERR_NOERR)
			return (err);

	} else if (strcasecmp(key, SCHA_SCALABLE) == 0) {
		(*props)->p_scalable = str_to_bool(val);

	} else if (strcasecmp(key, SCHA_GLOBAL_ZONE_OVERRIDE) == 0) {
		(*props)->p_gz_override = str_to_bool(val);

	} else if (strcasecmp(key, SCHA_PORT_LIST) == 0) {
		err = str_to_strarray(val, &(*props)->p_port_list);
		if (err != SCHA_ERR_NOERR)
			return (err);

		err = scds_parse_port_list((*props)->p_port_list,
		    (*props)->p_scalable, &(*props)->p_netaddr_list);
		if (err != SCHA_ERR_NOERR)
			return (err);

	} else if (strcasecmp(key, SCHA_RESOURCE_PROJECT_NAME) == 0) {
		(*props)->p_project_name = strdup(val);
		if ((*props)->p_project_name == NULL) {
			scds_syslog(LOG_ERR,
			    "Out of memory.");
			return (SCHA_ERR_NOMEM);
		}


	} else {
		scds_syslog_debug(SCDS_DBG_HIGH,
		    "Ignoring system defined property <%s> "
		    "when parsing argument list.", key);
	}

	return (SCHA_ERR_NOERR);
}

/*
 * scds_process_common_extprop()
 *
 * SUMMARY:
 *
 * Parse the common input extension property string which is in
 * "keyword=value,value,..." format and set the property value in the
 * corresponding field of the output structure.  Note that this function
 * overwrites the value in *props which is retrieved from ccr.
 *
 * PARAMETERS:
 *	x_prop (IN)	- Argument that contains list of extension properties
 *			  and values in the "keyword=value, value, ..." format.
 *	props  (OUT)	- scds_common_extprops_t struct will be updated with
 *			  new system defined property values
 *
 * RETURN VALUES:
 *	SCHA_ERR_NOERR	- Updating the scds_sysdef_props_t structure is
 *			  successful.
 *	SCHA_ERR_INVAL  - Invalid argument passed
 */

scha_err_t
scds_process_common_extprop(char *x_prop, scds_common_extprops_t **props)
{
	char *key, *val;
	scha_err_t err;
	char    internal_err_str[INT_ERR_MSG_SIZE];

	if (x_prop == NULL)
		return (SCHA_ERR_INVAL);

	if (str_kv(x_prop, &key, &val) != SCHA_ERR_NOERR) {
		(void) snprintf(internal_err_str,
		    sizeof (internal_err_str),
		    "Invalid argument for extension property: %s", x_prop);

		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		    internal_err_str);
		return (SCHA_ERR_INTERNAL);
	}

	if (strcasecmp(key, SCDS_EXTPROP_CONFIGDIR) == 0) {
		err = str_to_strarray(val, &(*props)->x_config_dirs);
		if (err != SCHA_ERR_NOERR)
			return (err);

	} else if (strcasecmp(key, SCDS_EXTPROP_PARAMTABLEVERSION) == 0) {
		(*props)->x_paramtable_version = strdup(val);
		if ((*props)->x_paramtable_version == NULL) {
			scds_syslog(LOG_ERR,
			    "Out of memory.");
			return (SCHA_ERR_NOMEM);
		}

	} else if (strcasecmp(key, SCDS_EXTPROP_MONITOR_RETRY_COUNT) == 0) {
		err = str_to_int(val, &(*props)->x_pmf_arg_n);
		if (err != SCHA_ERR_NOERR)
			return (err);

	} else if (strcasecmp(key, SCDS_EXTPROP_MONITOR_RETRY_INTERVAL) == 0) {
		err = str_to_int(val, &(*props)->x_pmf_arg_t);
		if (err != SCHA_ERR_NOERR)
			return (err);

	} else if (strcasecmp(key, SCDS_EXTPROP_PROBE_TIMEOUT) == 0) {
		err = str_to_int(val, &(*props)->x_probe_timeout);
		if (err != SCHA_ERR_NOERR)
			return (err);

	} else if (strcasecmp(key, SCDS_EXTPROP_FAILOVER_ENABLED) == 0) {
		(*props)->x_failover_enabled = str_to_bool(val);

	} else {
		scds_syslog_debug(SCDS_DBG_HIGH,
		    "Ignoring extension property <%s> "
		    "when parsing argument list\n", key);
	}

	return (SCHA_ERR_NOERR);
}

/*
 * man page for scds_get_netaddr_list (and others?) says that the
 * value of the proto can be looked up from in.h
 * There are no protocols TCP6 or UDP6, so we need to create our
 * own values for them. libdsdev.h defines these as arbitrary
 * large numbers.
 */
scds_code_table_t proto_names[] = {
	{SCDS_PROTO_STRING_TCP, SCDS_IPPROTO_TCP},
	{SCDS_PROTO_STRING_UDP, SCDS_IPPROTO_UDP},
	{SCDS_PROTO_STRING_SCTP, SCDS_IPPROTO_SCTP},
	{SCDS_PROTO_STRING_TCP6, SCDS_IPPROTO_TCP6},
	{SCDS_PROTO_STRING_UDP6, SCDS_IPPROTO_UDP6},
	{NULL, SCDS_CTAB_CODE_INVALID}
};

scha_err_t
string_to_code(char *string)
{
	int	i;

	for (i = 0; proto_names[i].c_string != NULL; i++) {
		if (strcasecmp(string, proto_names[i].c_string) == 0)
			return (proto_names[i].c_code);
	}

	return (SCDS_CTAB_CODE_INVALID);
}

/*
 * Parse sys prop value of Port_list, validate it, and pass out
 * a 3-tuple structure.
 *
 * Input Arguments
 *   - p_port_list       : denormalized list of string elements of the form
 *			   "<ip>/<port num>/<protocol>".
 *   - valid_proto_table : table of acceptable protocol families  that maps
 *			 valid string label to netinet/in.h-defined macro.
 *   - is_scalable	 : whether or not the underlying Port_list is for
 *			 a scalable resource.  If so, the port_list can
 *			 not be empty.  Otherwise it can be.
 * Output Arguments
 *   - netaddr_list      : the parsed and format-validated list of netaddrs.
 *
 * Return Values:
 *   - SCHA_ERR_NOERR
 *   - SCHA_ERR_INVAL
 */
scha_err_t
scds_parse_port_list(scha_str_array_t   *port_list,
    boolean_t		   is_scalable,
    scds_netaddr_list_t    **netaddr_list)
{

	uint_t	i = 0;
	char    *port_string  = NULL;
	char    *proto_string = NULL;
	char    *port_string_head  = NULL;
	int	proto_int;
	int	port_int;
	char    *endp = NULL;
	char    *ip = NULL;
	size_t	size = 1;
	int	err;

	/*
	 * Port_list for a scalable resource must not be empty.
	 */
	if ((port_list == NULL || port_list->array_cnt == 0)) {
		if (is_scalable == B_TRUE) {
			/*
			 * SCMSGS
			 * @explanation
			 * The value of the specified property must not be
			 * empty for scalable resources.
			 * @user_action
			 * Use clresource to specify a non-empty value for the
			 * property.
			 */
			scds_syslog(LOG_ERR, "Property %s is empty. This "
				"property must be specified for scalable "
				"resources.", SCHA_PORT_LIST);
			return (SCHA_ERR_INVAL);
		}
		/* Otherwise we are fine */
		return (SCHA_ERR_NOERR);
	}

	*netaddr_list = (scds_netaddr_list_t *)
	    calloc(size, sizeof (scds_netaddr_list_t));
	if (*netaddr_list == NULL) {
		scds_syslog(LOG_ERR,
		    "Out of memory.");
		return (SCHA_ERR_NOMEM);
	}
	(*netaddr_list)->netaddrs =
	    (scds_netaddr_t *)calloc((size_t)port_list->array_cnt,
	    sizeof (scds_netaddr_t));
	if ((*netaddr_list)->netaddrs == NULL) {
		scds_syslog(LOG_ERR,
		    "Out of memory.");
		return (SCHA_ERR_NOMEM);
	}
	scds_syslog_debug(2, "Debug: port_cnt = %d", port_list->array_cnt);
	for (i = 0; i < port_list->array_cnt; i++) {
		if (port_list->str_array[i] == NULL) {
			scds_syslog(LOG_ERR, "NULL value returned for the "
			    "resource property %s.", SCHA_PORT_LIST);
			return (SCHA_ERR_INVAL);
		}

		port_string = strdup(port_list->str_array[i]);
		if (port_string == NULL) {
			scds_syslog(LOG_ERR, "Out of memory.");
			return (SCHA_ERR_NOMEM);
		}

		port_string_head = port_string;

		/*
		 * Parse off the end of the string to get the port protocol.
		 */
		proto_string = strrchr(port_string, SCDS_PORT_PROTO_DELIMITER);
		if (proto_string == NULL) {
			/*
			 * SCMSGS
			 * @explanation
			 * The specified system property does not have a valid
			 * format. The value of the property must include a
			 * protocol.
			 * @user_action
			 * Use clresource to specify the property value with
			 * protocol. For example: TCP.
			 */
			scds_syslog(LOG_ERR, "Protocol is missing in "
			    "system defined property %s.", SCHA_PORT_LIST);
			return (SCHA_ERR_INVAL);

		} else {
			/* The user specfied a protocol following the port */
			*proto_string = '\0';
			proto_string = proto_string + 1;
			proto_int = string_to_code(proto_string);
			if (proto_int == SCDS_CTAB_CODE_INVALID) {
				/*
				 * SCMSGS
				 * @explanation
				 * The specified system property does not have
				 * a valid protocol.
				 * @user_action
				 * Using clnode, change the value of the
				 * property to use a valid protocol. For
				 * example: TCP, UDP.
				 */
				scds_syslog(LOG_ERR, "Invalid protocol is "
				    "specified in %s property.",
				    SCHA_PORT_LIST);
				return (SCHA_ERR_INVAL);
			}
		}

		/*
		 * parse the remaining port_string out from the
		 * <IP>SCDS_IP_PORT_DELIMITER<port> string
		 * (<IP>SCDS_IP_PORT_DELIMITER is optional).
		 */
		ip = port_string;
		port_string = strchr(ip, SCDS_IP_PORT_DELIMITER);
		if (port_string != NULL) {
			/* IP is specified in the port_list */
			*port_string = '\0'; /* separate port string from IP */
			port_string++;  /* advance it from the separator */
		} else {
			/* IP is not specified, fill it with null */
			port_string = ip;
			ip = NULL;
		}

		scds_syslog_debug(2,
		    "Debug: IP = %s and port = %s",
		    (ip != NULL) ? ip : "NULL", port_string);

		/*
		 * Convert the parsed port number.
		 */

		/*
		 * Suppress lint complaint due to error in errno.h --
		 * it's missing the "void" argument in the prototype.
		 */
		errno = 0;	/*lint !e746 */
		port_int = (int)strtol(port_string, &endp, 10);
		err = errno;
		scds_syslog_debug(2, "Debug: port_int(%d), errno(%d) "
			"endp(%s)", port_int, err,
			(endp == NULL) ? "NULL" : endp);
		if ((err != 0) || (*endp != '\0') || (port_int <= 0)) {
			/*
			 * SCMSGS
			 * @explanation
			 * The specified system property does not have a valid
			 * port number.
			 * @user_action
			 * Using clresource, specify the positive, valid port
			 * number.
			 */
			scds_syslog(LOG_ERR, "Invalid port number %s "
			    "in the %s property.", port_string, SCHA_PORT_LIST);
			return (SCHA_ERR_INVAL);
		}

		(*netaddr_list)->num_netaddrs++;
		(*netaddr_list)->netaddrs[i].hostname = ip;
		(*netaddr_list)->netaddrs[i].port_proto.port = port_int;
		(*netaddr_list)->netaddrs[i].port_proto.proto = proto_int;

		/* ip points to the memory allocated */
		if (ip == NULL)
			free(port_string_head);
	}

	return (SCHA_ERR_NOERR);
}

/*
 * scds_print_port_list()
 *
 * SUMMARY:
 *      Debugging function to print the scds_port_list_t
 *      structure at debug level
 *
 * PARAMETERS:
 *      handle          (IN) -  the handle returned from scds_initialize()
 *      debug_level     (IN) -  int between 1-9 (9 being the
 *                              most verbose level)
 *      port_list       (IN) -  pointer to a scds_port_list_t
 *                              struct for printing out
 *
 * RETURN TYPE: void
 *
 */
void
scds_print_port_list(scds_handle_t handle, int debug_level,
    const scds_port_list_t *port_list)
{

	scds_priv_handle_t	*h;
	scds_port_t		*p;
	int			i;

	h = (scds_priv_handle_t *)handle;
	if (h == NULL) {
		scds_syslog(LOG_ERR,
		    "Null value is passed for the handle.");
		abort();
	}
	if (port_list == NULL) {
		return;
	}
	(void) mutex_lock(&h->scds_mutex);

	for (i = 0; i < port_list->num_ports; i++) {
		p = &(port_list->ports[i]);
		scds_syslog_debug(debug_level, "Portlist:%d/%d\n",
			p->port, p->proto);
	}

	(void) mutex_unlock(&h->scds_mutex);
}


/*
 * Parse the input rg property string which is in "keyword=value,value,..."
 * format and set the property value in the corresponding field of the
 * output structure.  Note that this function overwrites the value in *props
 * which is retrieved from ccr.
 */
scha_err_t
scds_process_rg_prop(char *rg_prop, scds_rg_t **rg_info)
{
	char *key, *val;
	scha_err_t err;
	char    internal_err_str[INT_ERR_MSG_SIZE];

	if (rg_prop == NULL)
		return (SCHA_ERR_INVAL);

	if (str_kv(rg_prop, &key, &val) != SCHA_ERR_NOERR) {
		(void) snprintf(internal_err_str,
		    sizeof (internal_err_str),
		    "Invalid argument for resource group property: %s",
		    rg_prop);

		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (SCHA_ERR_INTERNAL);
	}

	if (strcasecmp(key, SCHA_NODELIST) == 0) {
		err = str_to_strarray(val, &(*rg_info)->rg_nodelist);
		if (err != SCHA_ERR_NOERR)
			return (err);

	} else if (strcasecmp(key, SCHA_MAXIMUM_PRIMARIES) == 0) {
		err = str_to_int(val, &(*rg_info)->rg_max_primaries);
		if (err != SCHA_ERR_NOERR)
			return (err);

	} else if (strcasecmp(key, SCHA_DESIRED_PRIMARIES) == 0) {
		err = str_to_int(val, &(*rg_info)->rg_desired_primaries);
		if (err != SCHA_ERR_NOERR)
			return (err);

	} else if (strcasecmp(key, SCHA_GLOBAL_RESOURCES_USED) == 0) {
		err = process_wildcard_value(val, &(*rg_info)->rg_glb_rs_used);
		if (err != SCHA_ERR_NOERR)
			return (err);

	} else if (strcasecmp(key, SCHA_RG_MODE) == 0) {
		(*rg_info)->rg_mode = str_to_rgmode(val);

	} else if (strcasecmp(key, SCHA_IMPL_NET_DEPEND) == 0)
		(*rg_info)->rg_impl_net_depend = str_to_bool(val);

	else if (strcasecmp(key, SCHA_PATHPREFIX) == 0) {
		(*rg_info)->rg_pathprefix = strdup(val);
		if ((*rg_info)->rg_pathprefix == NULL) {
			scds_syslog(LOG_ERR,
			    "Out of memory.");
			return (SCHA_ERR_NOMEM);
		}
	} else if (strcasecmp(key, SCHA_PINGPONG_INTERVAL) == 0) {
		err = str_to_int(val, &(*rg_info)->rg_ppinterval);
		if (err != SCHA_ERR_NOERR)
			return (err);
	} else if (strcasecmp(key, SCHA_RG_PROJECT_NAME) == 0) {
		(*rg_info)->rg_project_name = strdup(val);
		if ((*rg_info)->rg_project_name == NULL) {
			scds_syslog(LOG_ERR,
			    "Out of memory.");
			return (SCHA_ERR_NOMEM);
		}
	/* SC SLM addon start */
	} else if (strcasecmp(key, SCHA_RG_SLM_TYPE) == 0) {
		(*rg_info)->rg_slm_type = strdup(val);
		if ((*rg_info)->rg_slm_type == NULL) {
			scds_syslog(LOG_ERR,
			    "Out of memory.");
			return (SCHA_ERR_NOMEM);
		}
	} else if (strcasecmp(key, SCHA_RG_SLM_PSET_TYPE) == 0) {
		(*rg_info)->rg_slm_pset_type = strdup(val);
		if ((*rg_info)->rg_slm_pset_type == NULL) {
			scds_syslog(LOG_ERR,
			    "Out of memory.");
			return (SCHA_ERR_NOMEM);
		}
	} else if (strcasecmp(key, SCHA_RG_SLM_CPU_SHARES) == 0) {
		err = str_to_int(val, &(*rg_info)->rg_slm_cpu);
		if (err != SCHA_ERR_NOERR)
			return (err);

	} else if (strcasecmp(key, SCHA_RG_SLM_PSET_MIN) == 0) {
		err = str_to_int(val, &(*rg_info)->rg_slm_cpu_min);
		if (err != SCHA_ERR_NOERR)
			return (err);

	/* SC SLM addon end */
	} else if (strcasecmp(key, SCHA_RG_AFFINITIES) == 0) {
		err = str_to_strarray(val, &(*rg_info)->rg_affinities);
		if (err != SCHA_ERR_NOERR)
			return (err);
	}

	return (SCHA_ERR_NOERR);
}

/*
 * Allocates memory for resource structure. It fills in the scds_r_t
 * structure by retrieving the resource properties (system defined and
 * common extension) from CCR using SCHA API calls.
 * Incaseof R/RG update, this function will be called first to read the
 * property values from the CCR then scds_process_xxx_prop will be called
 * to update the appropriate (R or RG) properties with the new values.
 */
scha_err_t
scds_get_r_info(char *zname,
    char *r_name, scha_resource_t rs_handle, scds_r_t **r_info,
    boolean_t is_zone_cluster)
{
	scha_err_t		err;
	scds_r_t		*r = NULL;
	char			*fname = NULL;
	scha_extprop_value_t	*xval;
	char hostname[SCDS_ARRAY_SIZE];
	size_t	size = 1;


	if (r_name == NULL)
		return (SCHA_ERR_INVAL);

	r = (scds_r_t *)calloc(size, sizeof (scds_r_t));
	if (r == NULL) {
		return (SCHA_ERR_NOMEM);
	}

	(void) gethostname(hostname, SCDS_ARRAY_SIZE);

	/*
	 * Now we begin filling in the r_info struct by making calls to the API
	 */


	if (zname != NULL) {
		err = scds_get_fullname(zname, &fname, is_zone_cluster);
		if (err != SCHA_ERR_NOERR) {
			goto finished;
		}
		/* Callback Methods ENABLE/DISABLE switch on fname */
		err = scha_resource_get_zone(zname,
		    rs_handle, SCHA_ON_OFF_SWITCH_NODE,
		    fname, &r->r_on_off_switch);
		if (err != SCHA_ERR_NOERR) {
			/*
			 * SCMSGS
			 * @explanation
			 * The query for a property failed. The reason for the
			 * failure is given in the message.
			 * @user_action
			 * Save a copy of the /var/adm/messages files on all
			 * nodes. Contact your authorized Sun service provider
			 * for assistance in diagnosing the problem.
			 */
			scds_syslog(get_log_priority(err),
			    "Failed to retrieve the resource property %s "
			    "on node %s: %s.",
			    SCHA_ON_OFF_SWITCH, fname, scds_error_string(err));
			free(fname);
			goto finished;
		}
		/* Fault Monitoring ENABLE/DISABLE switch on fname */
		err = scha_resource_get_zone(
		    zname, rs_handle, SCHA_MONITORED_SWITCH_NODE,
		    fname, &r->r_on_off_switch);
		if (err != SCHA_ERR_NOERR) {
			scds_syslog(get_log_priority(err),
			    "Failed to retrieve the resource property %s "
			    "on node %s: %s.",
			    SCHA_MONITORED_SWITCH, fname,
			    scds_error_string(err));
			free(fname);
			goto finished;
		}
		free(fname);
	} else {
		/* Callback Methods ENABLE/DISABLE switch */
		err = scha_resource_get_zone(
		    zname, rs_handle, SCHA_ON_OFF_SWITCH,
		    &r->r_on_off_switch);
		if (err != SCHA_ERR_NOERR) {
			scds_syslog(get_log_priority(err),
			    "Failed to retrieve the resource property %s: %s.",
			    SCHA_ON_OFF_SWITCH, scds_error_string(err));
			goto finished;
		}

		/* Fault Monitoring ENABLE/DISABLE switch */
		err = scha_resource_get_zone(zname,
		    rs_handle, SCHA_MONITORED_SWITCH,
		&r->r_fm_switch);
		if (err != SCHA_ERR_NOERR) {
			scds_syslog(get_log_priority(err),
			    "Failed to retrieve the resource property %s: %s.",
			    SCHA_MONITORED_SWITCH, scds_error_string(err));
			goto finished;
		}
	}

	/*
	 * Pull out the system-defined Properties listed in RT paramtable
	 */
	r->r_sys_props =
	    (scds_sysdef_props_t *)calloc(size, sizeof (scds_sysdef_props_t));
	if (r->r_sys_props == NULL) {
		scds_syslog(LOG_ERR,
		    "Out of memory.");
		return (SCHA_ERR_NOMEM);
	}

	/* Cheap Probe Interval - Optional Property */
	err = scha_resource_get_zone(
	    zname, rs_handle, SCHA_CHEAP_PROBE_INTERVAL,
	    &r->r_sys_props->p_cprobe_interval);
	if (err != SCHA_ERR_NOERR && err != SCHA_ERR_PROP) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the resource property %s: %s.",
		    SCHA_CHEAP_PROBE_INTERVAL, scds_error_string(err));
		goto finished;
	}

	/* Through Probe Interval - Optional Property */
	err = scha_resource_get_zone(zname,
	    rs_handle, SCHA_THOROUGH_PROBE_INTERVAL,
	    &r->r_sys_props->p_tprobe_interval);
	if (err != SCHA_ERR_NOERR && err != SCHA_ERR_PROP) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the resource property %s: %s.",
		    SCHA_THOROUGH_PROBE_INTERVAL, scds_error_string(err));
		goto finished;
	}

	/* Retry Count - Optional Property */
	err = scha_resource_get_zone(zname, rs_handle, SCHA_RETRY_COUNT,
	    &r->r_sys_props->p_retry_count);
	if (err != SCHA_ERR_NOERR && err != SCHA_ERR_PROP) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the resource property %s: %s.",
		    SCHA_RETRY_COUNT, scds_error_string(err));
		goto finished;
	}

	/* Retry Interval - Optional Property */
	err = scha_resource_get_zone(zname, rs_handle, SCHA_RETRY_INTERVAL,
	    &r->r_sys_props->p_retry_interval);
	if (err != SCHA_ERR_NOERR && err != SCHA_ERR_PROP) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the resource property %s: %s.",
		    SCHA_RETRY_INTERVAL, scds_error_string(err));
		goto finished;
	}

	/* Failover Mode */
	err = scha_resource_get_zone(zname, rs_handle, SCHA_FAILOVER_MODE,
	    &r->r_sys_props->p_failover_mode);
	if (err != SCHA_ERR_NOERR) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the resource property %s: %s.",
		    SCHA_FAILOVER_MODE, scds_error_string(err));
		goto finished;
	}

	/* RS Strong Dependencies */
	err = scha_resource_get_zone(
	    zname, rs_handle, SCHA_RESOURCE_DEPENDENCIES,
	    &r->r_sys_props->p_strong_dep);
	if (err != SCHA_ERR_NOERR) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the resource property %s: %s.",
		    SCHA_RESOURCE_DEPENDENCIES, scds_error_string(err));
		goto finished;
	}

	/* RS Restart Dependencies */
	err = scha_resource_get_zone(zname,
	    rs_handle, SCHA_RESOURCE_DEPENDENCIES_RESTART,
	    &r->r_sys_props->p_restart_dep);
	if (err != SCHA_ERR_NOERR) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the "
		    "resource property %s: %s.",
		    SCHA_RESOURCE_DEPENDENCIES_RESTART,
		    scds_error_string(err));
		goto finished;
	}

	/* RS Offline restart Dependencies */
	err = scha_resource_get_zone(zname, rs_handle,
	    SCHA_RESOURCE_DEPENDENCIES_OFFLINE_RESTART,
	    &r->r_sys_props->p_offline_restart_dep);
	if (err != SCHA_ERR_NOERR) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the "
		    "resource property %s: %s.",
		    SCHA_RESOURCE_DEPENDENCIES_OFFLINE_RESTART,
		    scds_error_string(err));
		goto finished;
	}

	/* RS Weak Dependencies */
	err = scha_resource_get_zone(zname,
	    rs_handle, SCHA_RESOURCE_DEPENDENCIES_WEAK,
	    &r->r_sys_props->p_weak_dep);
	if (err != SCHA_ERR_NOERR) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the "
		    "resource property %s: %s.",
		    SCHA_RESOURCE_DEPENDENCIES_WEAK,
		    scds_error_string(err));
		goto finished;
	}

	/* RS Strong Dependencies with Qualifier */
	err = scha_resource_get_zone(zname,
	    rs_handle, SCHA_RESOURCE_DEPENDENCIES_Q,
	    &r->r_sys_props->p_strong_dep_q);
	if (err != SCHA_ERR_NOERR) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the resource property %s: %s.",
		    SCHA_RESOURCE_DEPENDENCIES_Q, scds_error_string(err));
		goto finished;
	}

	/* RS Restart Dependencies with Qualifier */
	err = scha_resource_get_zone(zname,
	    rs_handle, SCHA_RESOURCE_DEPENDENCIES_Q_RESTART,
	    &r->r_sys_props->p_restart_dep_q);
	if (err != SCHA_ERR_NOERR) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the "
		    "resource property %s: %s.",
		    SCHA_RESOURCE_DEPENDENCIES_Q_RESTART,
		    scds_error_string(err));
		goto finished;
	}

	/* RS Offline restart Dependencies with Qualifier */
	err = scha_resource_get_zone(zname, rs_handle,
	    SCHA_RESOURCE_DEPENDENCIES_Q_OFFLINE_RESTART,
	    &r->r_sys_props->p_offline_restart_dep_q);
	if (err != SCHA_ERR_NOERR) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the "
		    "resource property %s: %s.",
		    SCHA_RESOURCE_DEPENDENCIES_Q_OFFLINE_RESTART,
		    scds_error_string(err));
		goto finished;
	}

	/* RS Weak Dependencies with Qualifier */
	err = scha_resource_get_zone(
	    zname, rs_handle, SCHA_RESOURCE_DEPENDENCIES_Q_WEAK,
	    &r->r_sys_props->p_weak_dep_q);
	if (err != SCHA_ERR_NOERR) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the "
		    "resource property %s: %s.",
		    SCHA_RESOURCE_DEPENDENCIES_Q_WEAK,
		    scds_error_string(err));
		goto finished;
	}

	/* Network Resources - Optional Property */
	err = scha_resource_get_zone(zname,
	    rs_handle, SCHA_NETWORK_RESOURCES_USED,
	    &r->r_sys_props->p_network_resources_used);
	if (err != SCHA_ERR_NOERR && err != SCHA_ERR_PROP) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the resource property %s: %s.",
		    SCHA_NETWORK_RESOURCES_USED, scds_error_string(err));
		goto finished;
	}

	/* Method Timeouts */
	/* START Method Timeout */
	err = scha_resource_get_zone(zname, rs_handle, SCHA_START_TIMEOUT,
	    &r->r_sys_props->p_start_timeout);
	/*
	 * It's not a fatal error to be missing a START timeout
	 * because we support RTs with only PRENET_START.
	 */
	if (err == SCHA_ERR_PROP) {
		scds_syslog_debug(SCDS_DBG_HIGH,
		    "Failed to retrieve the resource property %s: %s.",
		    SCHA_START_TIMEOUT, scds_error_string(err));
		r->r_sys_props->p_start_timeout = SCDS_NO_TIMEOUT;
	} else if (err != SCHA_ERR_NOERR) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the resource property %s: %s.",
		    SCHA_START_TIMEOUT, scds_error_string(err));
		goto finished;
	}

	/* STOP Method Timeout */
	err = scha_resource_get_zone(zname, rs_handle, SCHA_STOP_TIMEOUT,
	    &r->r_sys_props->p_stop_timeout);
	/*
	 * It's not a fatal error to be missing a STOP timeout
	 * because we support RTs with only POSTNET_STOP.
	 */
	if (err == SCHA_ERR_PROP) {
		scds_syslog_debug(SCDS_DBG_HIGH,
		    "Failed to retrieve the resource property %s: %s.",
		    SCHA_STOP_TIMEOUT, scds_error_string(err));
		r->r_sys_props->p_stop_timeout = SCDS_NO_TIMEOUT;
	} else if (err != SCHA_ERR_NOERR) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the resource property %s: %s.",
		    SCHA_STOP_TIMEOUT, scds_error_string(err));
		goto finished;
	}

	/* MONITOR_STOP Method Timeout */
	err = scha_resource_get_zone(
	    zname, rs_handle, SCHA_MONITOR_STOP_TIMEOUT,
	    &r->r_sys_props->p_monitor_stop_timeout);
	if (err == SCHA_ERR_PROP) {
		/*
		 * We won't require a monitor_stop_timeout because
		 * the only required methods by the SCHA API are
		 * START and STOP. However, since this is used
		 * in data services which do provide FMs we will
		 * keep this here, but won't return an error if it
		 * is not presant.
		 */
		r->r_sys_props->p_monitor_stop_timeout = SCDS_NO_TIMEOUT;
	} else if (err != SCHA_ERR_NOERR) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the resource property %s: %s.",
		    SCHA_MONITOR_STOP_TIMEOUT, scds_error_string(err));
		goto finished;
	}

	/* Global_zone_override - Optional Property */
	err = scha_resource_get_zone(zname, rs_handle,
	    SCHA_GLOBAL_ZONE_OVERRIDE, &r->r_sys_props->p_gz_override);
	if (err != SCHA_ERR_NOERR && err != SCHA_ERR_PROP) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the resource property %s: %s.",
		    SCHA_GLOBAL_ZONE_OVERRIDE, scds_error_string(err));
		goto finished;
	}

	/*
	 *  Note that Scalable essentially defines a subclass of
	 *  "sys props explicitly used by data service code": those which are
	 *  defined automatically by RGM only if Scalable is declared in the
	 *  RTR file. If Scalable is not declared by the RT, then such an R
	 *  prop must be defined explicitly by the RT implementor in the RTR
	 *  file.
	 *
	 *  So the retrieval of such a property can acceptably return either
	 *  SCHA_ERR_NOERR or SCHA_ERR_PROP if p_scalable is FALSE or not
	 *  defined, but can only acceptably return SCHA_ERR_NOERR if
	 *  Scalable is TRUE.
	 *
	 *  The 1 member of this subclass is:
	 *    - Port_list
	 */

	/* Scalable */
	err = scha_resource_get_zone(zname, rs_handle, SCHA_SCALABLE,
		&r->r_sys_props->p_scalable);
	if (err != SCHA_ERR_NOERR) {
		if (err == SCHA_ERR_PROP) {
			r->r_sys_props->p_scalable = B_FALSE;
		} else {
			scds_syslog(get_log_priority(err),
			    "Failed to retrieve the resource property %s: %s.",
			    SCHA_SCALABLE, scds_error_string(err));
			goto finished;
		}
	}

	/* PortList */
	err = scha_resource_get_zone(zname, rs_handle, SCHA_PORT_LIST,
	    &r->r_sys_props->p_port_list);
	if (err == SCHA_ERR_NOERR || err == SCHA_ERR_PROP) {
		err =
		    scds_parse_port_list(r->r_sys_props->p_port_list,
				r->r_sys_props->p_scalable,
				&(r->r_sys_props->p_netaddr_list));
		/*
		 * As coded now, p_port_list should be
		 * entire raw port_list.  Something needs to
		 * change since only RTR parser knew this.  Only
		 * err msgs are affected right now though.
		 */
		if (err != SCHA_ERR_NOERR)
			goto finished;
	} else {
		goto finished;
	}

	/* Project name */
	err = scha_resource_get_zone(
	    zname, rs_handle, SCHA_RESOURCE_PROJECT_NAME,
	    &r->r_sys_props->p_project_name);
	if (err != SCHA_ERR_NOERR) {
		if (err == SCHA_ERR_PROP) {
			r->r_sys_props->p_project_name = NULL;
		} else {
			scds_syslog(get_log_priority(err),
			    "Failed to retrieve the resource property %s: %s.",
			    SCHA_RESOURCE_PROJECT_NAME,
			    scds_error_string(err));
			goto finished;
		}
	}


	/*
	 * Pull out the common Extension Properties
	 */
	r->r_ext_props = (scds_common_extprops_t *)
	    calloc(size, sizeof (scds_common_extprops_t));
	if (r->r_ext_props == NULL) {
		scds_syslog(LOG_ERR,
		    "Out of memory.");
		err = SCHA_ERR_NOMEM;
		goto finished;
	}

	/* ParamtableVersion */

	r->r_ext_props->x_paramtable_version = NULL;
	err = scds_retrieve_ext_props(zname, rs_handle,
	    SCDS_EXTPROP_PARAMTABLEVERSION, &xval);

	if (err == SCHA_ERR_NOERR) {
		r->r_ext_props->x_paramtable_version = xval->val.val_str;
	} else if (err != SCHA_ERR_PROP) {
		goto finished;
	}

	/* ConfDirList */
	r->r_ext_props->x_config_dirs = NULL;

	err = scds_retrieve_ext_props(zname, rs_handle,
	    SCDS_EXTPROP_CONFIGDIR, &xval);

	if (err == SCHA_ERR_NOERR) {
		r->r_ext_props->x_config_dirs = xval->val.val_strarray;
	} else if (err != SCHA_ERR_PROP) {
		goto finished;
	}

	/* SCDS_EXTPROP_MONITOR_RETRY_COUNT */
	r->r_ext_props->x_pmf_arg_n = SCDS_PROP_UNSET;

	err = scds_retrieve_ext_props(zname, rs_handle,
	    SCDS_EXTPROP_MONITOR_RETRY_COUNT, &xval);

	if (err == SCHA_ERR_NOERR) {
		r->r_ext_props->x_pmf_arg_n = xval->val.val_int;
	} else if (err != SCHA_ERR_PROP) {
		goto finished;
	}

	/* SCDS_EXTPROP_MONITOR_RETRY_INTERVAL */
	r->r_ext_props->x_pmf_arg_t = SCDS_PROP_UNSET;

	err = scds_retrieve_ext_props(zname, rs_handle,
	    SCDS_EXTPROP_MONITOR_RETRY_INTERVAL, &xval);

	if (err == SCHA_ERR_NOERR) {
		r->r_ext_props->x_pmf_arg_t = xval->val.val_int;
	} else if (err != SCHA_ERR_PROP) {
		goto finished;
	}

	/* Probe_Timeout */
	r->r_ext_props->x_probe_timeout = SCDS_PROP_UNSET;

	err = scds_retrieve_ext_props(zname, rs_handle,
	    SCDS_EXTPROP_PROBE_TIMEOUT, &xval);

	if (err == SCHA_ERR_NOERR) {
		r->r_ext_props->x_probe_timeout = xval->val.val_int;
	} else if (err != SCHA_ERR_PROP) {
		goto finished;
	}

	/*
	 * Failover_enabled. Note that the default is B_TRUE rather than
	 * SCDS_PROP_UNSET: this is what it will still be in case of
	 * SCHA_ERR_PROP during scds_retrieve_ext_props below
	 */
	r->r_ext_props->x_failover_enabled = B_TRUE;

	err = scds_retrieve_ext_props(zname, rs_handle,
	    SCDS_EXTPROP_FAILOVER_ENABLED, &xval);

	if (err == SCHA_ERR_NOERR) {
		r->r_ext_props->x_failover_enabled = xval->val.val_boolean;
	} else if (err == SCHA_ERR_PROP) {
		r->r_ext_props->x_failover_enabled = B_TRUE;
	} else {
		/* some error other than SCHA_ERR_PROP, scds_initialize fails */
		goto finished;
	}

	/*
	 * Set the original output to the local r struct we were using
	 */
	*r_info = r;

	return (SCHA_ERR_NOERR);

finished:

	if (r != NULL) {
		if (r->r_sys_props != NULL)
			free(r->r_sys_props);

		if (r->r_ext_props != NULL)
			free(r->r_ext_props);

		free(r);
	}
	*r_info = NULL;

	return (err);
}

scha_err_t
scds_retrieve_ext_props(const char *zname, scha_resource_t rs_handle,
    const char *prop_name, scha_extprop_value_t **xval)
{
	scha_err_t err;

	err = scha_resource_get_zone(zname, rs_handle, SCHA_EXTENSION,
	    prop_name, xval);

	if (err != SCHA_ERR_NOERR && err != SCHA_ERR_SEQID &&
	    err != SCHA_ERR_PROP) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the "
		    "resource property %s: %s.",
		    prop_name,
		    scds_error_string(err));
	}

	return (err);
}


/*
 * Allocates memory for resource group structure. It fills in the scds_rg_t
 * structure by retrieving the resource group properties from CCR using
 * SCHA API calls.
 * Incaseof R/RG update, this function will be called first to read the
 * property values from the CCR then scds_process_rg_prop will be called
 * to update the RG properties with the new values.
 */
scha_err_t
scds_get_rg_info(char *zname, char *rg_name, scha_resourcegroup_t rg_handle,
    scds_rg_t **rg_info)
{
	scha_err_t	err;
	scds_rg_t	*rg;
	size_t		size = 1;

	if (rg_name == NULL)
		return (SCHA_ERR_INVAL);

	rg = (scds_rg_t *)calloc(size, sizeof (scds_rg_t));
	if (rg == NULL) {
		scds_syslog(LOG_ERR,
		    "Out of memory.");
		return (SCHA_ERR_NOMEM);
	}

	/* SCHA_NODELIST */
	err = scha_resourcegroup_get_zone(zname, rg_handle, SCHA_NODELIST,
	    &rg->rg_nodelist);
	if (err != SCHA_ERR_NOERR) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the resource group property %s: %s.",
		    SCHA_NODELIST, scds_error_string(err));
		goto finished;
	}

	/* SCHA_MAXIMUM_PRIMARIES */
	err = scha_resourcegroup_get_zone(
	    zname, rg_handle, SCHA_MAXIMUM_PRIMARIES,
	    &rg->rg_max_primaries);
	if (err != SCHA_ERR_NOERR) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the resource group property %s: %s.",
		    SCHA_MAXIMUM_PRIMARIES, scds_error_string(err));
		goto finished;
	}

	/* SCHA_DESIRED_PRIMARIES */
	err = scha_resourcegroup_get_zone(
	    zname, rg_handle, SCHA_DESIRED_PRIMARIES,
	    &rg->rg_desired_primaries);
	if (err != SCHA_ERR_NOERR) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the resource group property %s: %s.",
		    SCHA_DESIRED_PRIMARIES, scds_error_string(err));
		goto finished;
	}

	/* SCHA_GLOBAL_RESOURCES_USED */
	err = scha_resourcegroup_get_zone(
	    zname, rg_handle, SCHA_GLOBAL_RESOURCES_USED,
	    &rg->rg_glb_rs_used);
	if (err != SCHA_ERR_NOERR) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the resource group property %s: %s.",
		    SCHA_GLOBAL_RESOURCES_USED, scds_error_string(err));
		goto finished;
	}

	/* SCHA_RESOURCE_LIST */
	err = scha_resourcegroup_get_zone(zname, rg_handle, SCHA_RESOURCE_LIST,
	    &rg->rg_rlist);
	if (err != SCHA_ERR_NOERR) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the resource group property %s: %s.",
		    SCHA_RESOURCE_LIST, scds_error_string(err));
		goto finished;
	}

	/* SCHA_RG_MODE */
	err = scha_resourcegroup_get_zone(zname, rg_handle, SCHA_RG_MODE,
	    &rg->rg_mode);
	if (err != SCHA_ERR_NOERR) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the resource group property %s: %s.",
		    SCHA_RG_MODE, scds_error_string(err));
		goto finished;
	}

	/* SCHA_IMPL_NET_DEPEND */
	err = scha_resourcegroup_get_zone(
	    zname, rg_handle, SCHA_IMPL_NET_DEPEND,
	    &rg->rg_impl_net_depend);
	if (err != SCHA_ERR_NOERR) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the resource group property %s: %s.",
		    SCHA_IMPL_NET_DEPEND, scds_error_string(err));
		goto finished;
	}

	/* SCHA_PATHPREFIX */
	err = scha_resourcegroup_get_zone(zname, rg_handle, SCHA_PATHPREFIX,
	    &rg->rg_pathprefix);
	if (err != SCHA_ERR_NOERR) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the resource group property %s: %s.",
		    SCHA_PATHPREFIX, scds_error_string(err));
		goto finished;
	}

	/* SCHA_PINGPONG_INTERVAL */
	err = scha_resourcegroup_get_zone(
	    zname, rg_handle, SCHA_PINGPONG_INTERVAL,
	    &rg->rg_ppinterval);
	if (err != SCHA_ERR_NOERR) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the resource group property %s: %s.",
		    SCHA_PINGPONG_INTERVAL, scds_error_string(err));
		goto finished;
	}

	/* SCHA_RG_PROJECT_NAME */
	err = scha_resourcegroup_get_zone(
	    zname, rg_handle, SCHA_RG_PROJECT_NAME,
	    &rg->rg_project_name);
	if (err != SCHA_ERR_NOERR) {
		if (err == SCHA_ERR_PROP) {
			rg->rg_project_name = NULL;
		} else {
			scds_syslog(get_log_priority(err),
			    "Failed to retrieve the resource group "
			    "property %s: %s.",
			    SCHA_RG_PROJECT_NAME,
			    scds_error_string(err));
			goto finished;
		}
	}

	/* SC SLM addon start */
	/* SCHA_RG_SLM_TYPE */
	err = scha_resourcegroup_get_zone(zname, rg_handle, SCHA_RG_SLM_TYPE,
	    &rg->rg_slm_type);
	if (err != SCHA_ERR_NOERR) {
		if (err == SCHA_ERR_PROP) {
			rg->rg_slm_type = NULL;
		} else {
			scds_syslog(get_log_priority(err),
			    "Failed to retrieve the resource group "
			    "property %s: %s.",
			    SCHA_RG_SLM_TYPE,
			    scds_error_string(err));
			goto finished;
		}
	}

	/* SCHA_RG_SLM_PSET_TYPE */
	err = scha_resourcegroup_get_zone(
	    zname, rg_handle, SCHA_RG_SLM_PSET_TYPE,
	    &rg->rg_slm_pset_type);
	if (err != SCHA_ERR_NOERR) {
		if (err == SCHA_ERR_PROP) {
			rg->rg_slm_type = NULL;
		} else {
			scds_syslog(get_log_priority(err),
			    "Failed to retrieve the resource group "
			    "property %s: %s.",
			    SCHA_RG_SLM_PSET_TYPE,
			    scds_error_string(err));
			goto finished;
		}
	}

	/* SCHA_RG_SLM_CPU_SHARES */
	err = scha_resourcegroup_get_zone(
	    zname, rg_handle, SCHA_RG_SLM_CPU_SHARES,
	    &rg->rg_slm_cpu);
	if (err != SCHA_ERR_NOERR) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the resource group property %s: %s.",
		    SCHA_RG_SLM_CPU_SHARES, scds_error_string(err));
		goto finished;
	}

	/* SCHA_RG_SLM_PSET_MIN */
	err = scha_resourcegroup_get_zone(
	    zname, rg_handle, SCHA_RG_SLM_PSET_MIN,
	    &rg->rg_slm_cpu_min);
	if (err != SCHA_ERR_NOERR) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the resource group property %s: %s.",
		    SCHA_RG_SLM_PSET_MIN, scds_error_string(err));
		goto finished;
	}
	/* SC SLM addon end */

	/* SCHA_RG_AFFINITIES */
	err = scha_resourcegroup_get_zone(zname, rg_handle, SCHA_RG_AFFINITIES,
	    &rg->rg_affinities);
	if (err != SCHA_ERR_NOERR) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the resource group property %s: %s.",
		    SCHA_RG_AFFINITIES, scds_error_string(err));
		goto finished;
	}


	*rg_info = rg;

	return (SCHA_ERR_NOERR);

finished:
	free(rg);

	*rg_info = NULL;
	return (err);
}

/*
 * Allocates memory for resource type structure. It fills in the scds_rt_t
 * structure by retrieving the resource type properties from CCR using
 * SCHA API calls.
 */

scha_err_t
scds_get_rt_info(char *zname, char *rt_name, scha_resourcetype_t rt_handle,
    scds_rt_t **rt_info)
{
	scha_err_t	err;
	scds_rt_t	*rt;
	char		*ptr;
	size_t		size = 1;

	if (rt_name == NULL)
		return (SCHA_ERR_INVAL);

	rt = (scds_rt_t *)calloc(size, sizeof (scds_rt_t));
	if (rt == NULL) {
		scds_syslog(LOG_ERR,
		    "Out of memory.");
		return (SCHA_ERR_NOMEM);
	}

	/*
	 * Now we begin filling in the rt struct
	 * by making calls to the API
	 */

	/* date service name (ie. rt_name without VENDOR_ID.) */
	if (strrchr(rt_name, '.') == NULL)
		/* rt_name doesn't contain VENDOR_ID */
		rt->rt_svc_name = strdup(rt_name);
	else {
		ptr = strpbrk(rt_name, ".");
		rt->rt_svc_name = strdup(ptr + 1);
	}

	if (rt->rt_svc_name == NULL) {
		scds_syslog(LOG_ERR,
		    "Out of memory.");
		return (SCHA_ERR_NOMEM);
	}

	/* SINGLE_INSTANCE */
	err = scha_resourcetype_get_zone(zname, rt_handle, SCHA_SINGLE_INSTANCE,
	    &rt->rt_single_inst);
	if (err != SCHA_ERR_NOERR) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the resource type property %s: %s.",
		    SCHA_SINGLE_INSTANCE, scds_error_string(err));
		goto finished;
	}

	/* INIT_NODES */
	err = scha_resourcetype_get_zone(zname, rt_handle, SCHA_INIT_NODES,
	    &rt->rt_init_nodes);
	if (err != SCHA_ERR_NOERR) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the resource type property %s: %s.",
		    SCHA_INIT_NODES, scds_error_string(err));
		goto finished;
	}

	/* INSTALL_NODES */
	err = scha_resourcetype_get_zone(zname, rt_handle, SCHA_INSTALLED_NODES,
	    &rt->rt_install_nodes);
	if (err != SCHA_ERR_NOERR) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the resource type property %s: %s.",
		    SCHA_INSTALLED_NODES, scds_error_string(err));
		goto finished;
	}

	/* FAILOVER */
	err = scha_resourcetype_get_zone(zname,
	    rt_handle, SCHA_FAILOVER, &rt->rt_failover);
	if (err != SCHA_ERR_NOERR) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the resource type property %s: %s.",
		    SCHA_FAILOVER, scds_error_string(err));
		goto finished;
	}

	/* API Version */
	err = scha_resourcetype_get_zone(zname, rt_handle, SCHA_API_VERSION,
	    &rt->rt_api_version);
	if (err != SCHA_ERR_NOERR) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the resource type property %s: %s.",
		    SCHA_API_VERSION, scds_error_string(err));
		goto finished;
	}

	/* RT Version - Optional Property */
	err = scha_resourcetype_get_zone(zname, rt_handle, SCHA_RT_VERSION,
	    &rt->rt_version);
	if (err != SCHA_ERR_NOERR && err != SCHA_ERR_PROP) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the resource type property %s: %s.",
		    SCHA_RT_VERSION, scds_error_string(err));
		goto finished;
	}

	/* Base Dir Path  */
	err = scha_resourcetype_get_zone(zname, rt_handle, SCHA_RT_BASEDIR,
	    &rt->rt_basedir);
	if (err != SCHA_ERR_NOERR) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the resource type property %s: %s.",
		    SCHA_RT_BASEDIR, scds_error_string(err));
		goto finished;
	}

	/* START Method Path  */
	err = scha_resourcetype_get_zone(zname, rt_handle, SCHA_START,
	    &rt->rt_start_method);
	/*
	 * It's not a fatal error to be missing a START method
	 * because we allow RTs with only PRENET_START.
	 */
	if (err == SCHA_ERR_PROP) {
		scds_syslog_debug(SCDS_DBG_HIGH,
		    "Failed to retrieve the resource type property %s: %s.",
		    SCHA_START, scds_error_string(err));
		rt->rt_start_method = NULL;
	} else if (err != SCHA_ERR_NOERR) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the resource type property %s: %s.",
		    SCHA_START, scds_error_string(err));
		goto finished;
	}

	/* STOP Method Path  */
	err = scha_resourcetype_get_zone(zname, rt_handle, SCHA_STOP,
	    &rt->rt_stop_method);
	/*
	 * It's not a fatal error to be missing a STOP method
	 * because we allow RTs with only POSTNET_STOP.
	 */
	if (err == SCHA_ERR_PROP) {
		scds_syslog_debug(SCDS_DBG_HIGH,
		    "Failed to retrieve the resource type property %s: %s.",
		    SCHA_STOP, scds_error_string(err));
		rt->rt_stop_method = NULL;
	} else if (err != SCHA_ERR_NOERR) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve the resource type property %s: %s.",
		    SCHA_STOP, scds_error_string(err));
		goto finished;
	}

	/*
	 * Set the original output to the local rt struct
	 * we were using
	 */

	*rt_info = rt;

	return (SCHA_ERR_NOERR);

finished:

	if (rt != NULL)
		free(rt);

	*rt_info = NULL;
	return (err);
}

/*
 * Get the value of non-common extension property from CCR by calling the
 * scha_resource_get().
 *
 * Parameters:
 *	handle		(IN)  - handle that holds all the data
 * 	propname	(IN)  - the name of the given extension property
 * 	xprop		(OUT) - the value of the given extension property
 *
 * Return Code:
 *	SCHA_ERR_NOERR	property found in ccr, store the prop value in xprop
 *	SCHA_ERR_INVAL	property not found in ccr
 * Assumes the caller holds the mutex
 */
scha_err_t
scds_get_ext_prop_from_conf(scds_priv_handle_t *h, const char *propname,
    scha_extprop_value_t **xprop, char *nname)
{
	scha_err_t err;
	char    internal_err_str[INT_ERR_MSG_SIZE];
	scha_extprop_value_t *tprop = NULL;

	if (xprop == NULL) {
		(void) snprintf(internal_err_str, sizeof (internal_err_str),
		    "Failed to retrieve the "
		    "extension property: Invalid address");

		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (SCHA_ERR_INTERNAL);
	}

	if (nname == NULL)
		err = scha_resource_get_zone(h->ptable.pt_zonename,
		    h->rs_handle, SCHA_EXTENSION,
		    propname, &tprop);
	else
		err = scha_resource_get_zone(h->ptable.pt_zonename,
		    h->rs_handle, SCHA_EXTENSION_NODE,
		    propname, nname, &tprop);


	if (err == SCHA_ERR_PROP) {
		scds_syslog_debug(SCDS_DBG_HIGH,
		    "<%s> is not a resource extension property\n", propname);
		return (err);
	}

	/*
	 * Since the resource handle cannot be closed here as that would free
	 * all memory returned earlier when scds_initialize() opened the
	 * resource handle. We suppress SCHA_ERR_SEQID and document that anyone
	 * who seriously cares about actual updates must implement an update
	 * method. We also document that applications that want to poll for
	 * CCR data changes can use the lower level RMAPI scha_xxx routines.
	 */
	if ((err != SCHA_ERR_NOERR && err != SCHA_ERR_SEQID) || tprop == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * NULL value was specified for the extension property of the
		 * resource.
		 * @user_action
		 * For the property name check the syslog message. Any of the
		 * following situations might have occurred. Different user
		 * action is needed for these different scenarios. 1) If a new
		 * resource is created or updated, check whether the value of
		 * the extension property is empty. If it is, provide a valid
		 * value by using clresource. 2) For all other cases, treat it
		 * as an Internal error. Contact your authorized Sun service
		 * provider.
		 */
		scds_syslog(LOG_ERR,
		    "NULL value returned for the extension property %s.",
		    propname);
		return (SCHA_ERR_INVAL);
	}

	/*
	 * Now duplicate the ext property that was retrieved.
	 * This is for clean memory management purposes. It makes
	 * sure that scds_get_ext_property *always* allocates its
	 * own memory, and that can always be freed by scds_free_ext_property
	 */
	if ((*xprop = dup_ext_property(tprop)) == NULL) {
		/* Ran out of memory */
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", "Out of memory");
		return (SCHA_ERR_NOMEM);
	}

	return (SCHA_ERR_NOERR);
}

/*
 * scds_get_ext_prop_from_args -
 *	Look for the property value from command line arguments.
 *	Return SCHA_ERR_NOERR upon success.  Return SCHA_ERR_INVAL if the given
 *	property name is not found in argument list.
 * Assumes the caller holds the mutex
 */
scha_err_t
scds_get_ext_prop_from_args(scds_priv_handle_t *h, const char *propname,
    scha_prop_type_t xprop_type, scha_extprop_value_t **xprop, char *nname)
{
	int c;
	char	*o_optarg;
	int	o_optind, o_opterr, o_optopt;
	char *key, *xval;
	char *pn_key = NULL, *nodeid = NULL;
	scha_err_t err;
	char    internal_err_str[INT_ERR_MSG_SIZE];
	size_t	size = 1;
	boolean_t being_created = B_FALSE;

	if (xprop == NULL) {
		(void) snprintf(internal_err_str,
		    sizeof (internal_err_str),
		    "Failed to retrieve the extension property: "
		    "Invalid address");

		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (SCHA_ERR_INTERNAL);
	}

	err = SCHA_ERR_INVAL;

	*xprop = NULL;

	/*
	 * Before calling getopt(), save the original values of getopt
	 * related variables.  These values will be restored whenever
	 * exiting the while loop of getopt().
	 */
	save_getopt_vars(&o_optarg, &o_optind, &o_opterr, &o_optopt);

	while ((c = getopt(h->scds_argc, h->scds_argv, "R:T:G:r:x:X:g:cuZ:"))
	    != EOF) {
		switch (c) {
		case 'c':
			being_created = B_TRUE;
			break;
		case 'u':
		case 'R':
		case 'G':
		case 'T':
		case 'r':
		case 'g':
		case 'Z':
			/* skip pre-defined and rg props */
			break;
		case 'X':
			/*
			 * Per-node extension property values are passed to
			 * the validate callback in two ways. The value on the
			 * local-node is passed using the -x argument. The other
			 * per-node values updated are passed using the
			 * -X argument. Hence, "-X" argument typically takes
			 * -X prop1{node1} = val1 -X prop1{node2} = val2.
			 */
			if (nname == NULL)
				break;
			err = str_kv(optarg, &key, &xval);
			if (err != SCHA_ERR_NOERR) {
				restore_getopt_vars(o_optarg, o_optind,
				    o_opterr, o_optopt);
				return (err);
			}
			pn_key = my_strtok(&key, '{');
			nodeid = my_strtok(&key, '}');
			if (pn_key == NULL || nodeid == NULL) {
				restore_getopt_vars(o_optarg, o_optind,
				    o_opterr, o_optopt);
				return (SCHA_ERR_INVAL);
			}
			if (strcasecmp(pn_key, propname) == 0) {
				if (strcmp(nodeid, nname) != 0) {
					break;
				}
				/*
				 * The property name has been found in argument
				 * list.  There is no need to call getopt()
				 * again to continue searching.  It is ok to
				 * restore the values here.
				 */
				restore_getopt_vars(o_optarg, o_optind,
				    o_opterr, o_optopt);

				*xprop = (scha_extprop_value_t *)
				    calloc(size, sizeof (scha_extprop_value_t));
				if (*xprop == NULL) {
					scds_syslog(LOG_ERR,
					    "Out of memory.");
					return (SCHA_ERR_NOMEM);
				}

				(*xprop)->prop_type = xprop_type;
				switch (xprop_type) {
				case SCHA_PTYPE_STRING:
					(*xprop)->val.val_str = strdup(xval);
					if ((*xprop)->val.val_str == NULL) {
						scds_syslog(LOG_ERR,
						    "Out of memory.");
						err = SCHA_ERR_NOMEM;
						goto finished;
					}
					return (SCHA_ERR_NOERR);

				case SCHA_PTYPE_INT:
					if (sscanf(xval, "%d",
					    &((*xprop)->val.val_int)) != 1) {
						goto finished;
					}
					return (SCHA_ERR_NOERR);

				case SCHA_PTYPE_BOOLEAN:
					(*xprop)->val.val_boolean =
					    str_to_bool(xval);
					return (SCHA_ERR_NOERR);

				case SCHA_PTYPE_ENUM:
					(*xprop)->val.val_enum = strdup(xval);
					if ((*xprop)->val.val_enum == NULL) {
						scds_syslog(LOG_ERR,
						    "Out of memory.");
						err = SCHA_ERR_NOMEM;
						goto finished;
					}
					return (SCHA_ERR_NOERR);
				default:
					(void) snprintf(internal_err_str,
					    sizeof (internal_err_str),
					    "Invalid property type");

					scds_syslog(LOG_ERR,
					    "INTERNAL ERROR: %s.",
					    internal_err_str);
					err = SCHA_ERR_INTERNAL;
					goto finished;
				}


			}
			break;
		case 'x':
			/*
			 * When nname != NULL, the call is essentially to fetch
			 * extension property value on the node nname. We
			 * need to only parse the -X options to get the
			 * value.
			 */
			if (nname != NULL)
				break;
			err = str_kv(optarg, &key, &xval);

			if (err != SCHA_ERR_NOERR) {
				restore_getopt_vars(o_optarg, o_optind,
				    o_opterr, o_optopt);
				return (err);
			}

			if (strcasecmp(key, propname) == 0) {
				/*
				 * The property name has been found in argument
				 * list.  There is no need to call getopt()
				 * again to continue searching.  It is ok to
				 * restore the values here.
				 */
				restore_getopt_vars(o_optarg, o_optind,
				    o_opterr, o_optopt);

				*xprop = (scha_extprop_value_t *)
				    calloc(size, sizeof (scha_extprop_value_t));
				if (*xprop == NULL) {
					scds_syslog(LOG_ERR,
					    "Out of memory.");
					return (SCHA_ERR_NOMEM);
				}

				(*xprop)->prop_type = xprop_type;
				switch (xprop_type) {
				case SCHA_PTYPE_STRING:
					(*xprop)->val.val_str = strdup(xval);
					if ((*xprop)->val.val_str == NULL) {
						scds_syslog(LOG_ERR,
						    "Out of memory.");
						err = SCHA_ERR_NOMEM;
						goto finished;
					}
					return (SCHA_ERR_NOERR);

				case SCHA_PTYPE_INT:
					if (sscanf(xval, "%d",
					    &((*xprop)->val.val_int)) != 1) {
						goto finished;
					}
					return (SCHA_ERR_NOERR);

				case SCHA_PTYPE_BOOLEAN:
					(*xprop)->val.val_boolean =
					    str_to_bool(xval);
					return (SCHA_ERR_NOERR);

				case SCHA_PTYPE_ENUM:
					(*xprop)->val.val_enum = strdup(xval);
					if ((*xprop)->val.val_enum == NULL) {
						scds_syslog(LOG_ERR,
						    "Out of memory.");
						err = SCHA_ERR_NOMEM;
						goto finished;
					}
					return (SCHA_ERR_NOERR);

				case SCHA_PTYPE_STRINGARRAY:
					err = str_to_strarray(xval,
					    &((*xprop)->val.val_strarray));
					if (err != SCHA_ERR_NOERR)
						goto finished;
					return (SCHA_ERR_NOERR);

				case SCHA_PTYPE_UINTARRAY:
				case SCHA_PTYPE_UINT:
				default:
					(void) snprintf(internal_err_str,
					    sizeof (internal_err_str),
					    "Invalid property type");

					scds_syslog(LOG_ERR,
					    "INTERNAL ERROR: %s.",
					    internal_err_str);
					err = SCHA_ERR_INTERNAL;
					goto finished;
				}
			}
			break;

		default:
			scds_syslog(LOG_ERR,
			    "Usage: %s [-c|-u] -R <resource-name> "
			    "-T <type-name> -G <group-name> "
			    "[-r <sys_def_prop>=<value> ...] "
			    "[-x <ext_prop>=<value> ...] "
			    "[-Z <zone-name>]",
			    h->scds_argv[0]);
			err = SCHA_ERR_INVAL;
			restore_getopt_vars(o_optarg, o_optind, o_opterr,
			    o_optopt);
			goto finished;
		}
	}


	restore_getopt_vars(o_optarg, o_optind, o_opterr, o_optopt);

	/*
	 * Return SCHA_ERR_PROP if the resource is being created
	 * and any property is missing from the command line.
	 * priv_get_ext_property() will not try and read properties
	 * from ccr if we return anything other than SCHA_ERR_INVAL
	 * here, so it all works out.
	 */
	if (being_created)
		return (SCHA_ERR_PROP);
	else
		return (SCHA_ERR_INVAL);

finished:
	if (*xprop)
		free(*xprop);

	*xprop = NULL;
	return (err);
}

/*
 * Try to get the value of extension property from argument list first.
 * If it is not there, try to look for it in rgm configuration(CCR).
 */
scha_err_t
scds_get_ext_property(scds_handle_t handle, const char *propname,
    scha_prop_type_t xprop_type, scha_extprop_value_t **xprop)
{
	scds_priv_handle_t	*h;
	scha_err_t		e;

	h = check_handle(handle);

	(void) mutex_lock(&h->scds_mutex);

	e = priv_get_ext_property(h, propname, xprop_type, xprop);

	(void) mutex_unlock(&h->scds_mutex);

	return (e);
}

/*
 * Assumes the caller holds the mutex
 */
scha_err_t
priv_get_ext_property(scds_priv_handle_t *handle, const char *propname,
    scha_prop_type_t xprop_type, scha_extprop_value_t **xprop)
{
	scha_err_t err = SCHA_ERR_NOERR;
	char *fname = NULL;
	boolean_t is_pn = B_FALSE;


	/*
	 * Checkout if the property is a per-node property.
	 * If it is, we need to use the EXTENSION_NODE variant
	 */
	err = scha_resourcetype_get_zone(handle->ptable.pt_zonename,
	    handle->rt_handle, SCHA_PER_NODE, propname,
	    &is_pn);
	if (err != SCHA_ERR_NOERR)
		return (err);


	if (!handle->is_zone_cluster && handle->ptable.pt_zonename !=
	    NULL && is_pn) {
		err = scds_get_fullname_nodeid(handle->ptable.pt_zonename,
		    &fname, handle->is_zone_cluster);
		if (err != SCHA_ERR_NOERR)
			return (err);
		err = priv_get_ext_property_node(handle,
		    propname, xprop_type, xprop, fname);
		free(fname);
		return (err);
	}

	err = scds_get_ext_prop_from_args(handle, propname,
	    xprop_type, xprop, NULL);

	if (err == SCHA_ERR_INVAL)
		/* This is the case that propname is not in argument list. */
		/* We will look for it in the rgm configuration then. */
		return (scds_get_ext_prop_from_conf(handle, propname,
		    xprop, NULL));
	else
		return (err);
}

/*
 * Assumes the caller holds the mutex
 */

scha_err_t
priv_get_ext_property_node(scds_priv_handle_t *handle, const char *propname,
    scha_prop_type_t xprop_type, scha_extprop_value_t **xprop, char *nname)
{
	scha_err_t err;

	err = scds_get_ext_prop_from_args(handle, propname, xprop_type, xprop,
	    nname);


	if (err == SCHA_ERR_INVAL)
		/* This is the case that propname is not in argument list. */
		/* We will look for it in the rgm configuration then. */
		return (scds_get_ext_prop_from_conf(handle, propname,
		    xprop, nname));
	else
		return (err);
}


/*
 * scds_close()
 *
 * SUMMARY:
 *	Free DSDL environment resources that were allocated
 *	by scds_initialize().  Call this function once, prior
 *	to termination of the program.
 *
 * PARAMETERS:
 *	handle 	(IN)  - the handle returned from scds_initialize()
 *
 * RETURN TYPE:	void
 */
void
scds_close(scds_handle_t *handle)
{
	scds_priv_handle_t **h;

	h = (scds_priv_handle_t **)handle;

	(void) mutex_destroy(&((*h)->scds_mutex));

	scds_free_handle(h);
}

/*
 * Free all memory associated with the given "handle" structure and then
 * assign NULL value to the handle.
 */
void
scds_free_handle(scds_priv_handle_t **h)
{

	scds_priv_handle_t *temp = *h;

	if (temp == NULL)
		return;

	if (temp->ptable.pt_current_method) {
		free(temp->ptable.pt_current_method);
		temp->ptable.pt_current_method = NULL;
	}

	scds_free_ptable_elements(&(temp->ptable));

	/* close the resource handle */
	if (temp->rs_handle)
		(void) scha_resource_close_zone(h->ptable.pt_zonename,
		    temp->rs_handle);
	temp->rs_handle = NULL;

	/* close the resource type handle */
	if (temp->rt_handle)
		(void) scha_resourcetype_close_zone(h->ptable.pt_zonename,
		    temp->rt_handle);
	temp->rt_handle = NULL;

	/* close the resource group handle */
	if (temp->rg_handle)
		(void) scha_resourcegroup_close_zone(h->ptable.pt_zonename,
		    temp->rg_handle);
	temp->rg_handle = NULL;

	/* Do fault monitor related clean up */
	scds_fm_close(temp);

	*h = NULL;

	free(temp);
}

/*
 * This procedure frees the elements in ptable but does not free up
 * ptable itself.
 */
void
scds_free_ptable_elements(scds_prop_table_t *ptable)
{
	int i;
	scds_netaddr_list_t *net_list = NULL;

	if (ptable == NULL)
		return;

	if (ptable->pt_r) {
		if (ptable->pt_r->r_sys_props) {
			net_list = ptable->pt_r->r_sys_props->p_netaddr_list;
			if (net_list) {
				if (net_list->netaddrs) {
					for (i = 0;
						i < net_list->num_netaddrs;
						i++) {
						if (net_list->
						    netaddrs[i].hostname) {
							free(net_list->
							    netaddrs[i].
							    hostname);
							net_list->
							netaddrs[i].hostname =
							    NULL;
						}
					}
					free(net_list->netaddrs);
					net_list->netaddrs = NULL;
				}
				free(ptable->
				    pt_r->r_sys_props->p_netaddr_list);
				ptable->
				    pt_r->r_sys_props->p_netaddr_list = NULL;
			}
			free(ptable->pt_r->r_sys_props);
			ptable->pt_r->r_sys_props = NULL;
		}

		if (ptable->pt_r->r_ext_props) {
			free(ptable->pt_r->r_ext_props);
			ptable->pt_r->r_ext_props = NULL;
		}

		free(ptable->pt_r);
		ptable->pt_r = NULL;
	}

	if (ptable->pt_rg) {
		free(ptable->pt_rg);
		ptable->pt_rg = NULL;
	}

	if (ptable->pt_rt) {
		free(ptable->pt_rt->rt_svc_name);
		ptable->pt_rt->rt_svc_name = NULL;
		free(ptable->pt_rt);
		ptable->pt_rt = NULL;
	}
}

/*
 * scds_free_ext_property()
 *
 * SUMMARY:
 *      Free the resource extension property memory
 *      allocated by a call to scds_get_ext_property()
 *
 * PARAMETERS:
 *      property_value (IN) - pointer to extension property value
 *
 * RETURN TYPE: void
 */
void
scds_free_ext_property(scha_extprop_value_t *property_value)
{
	uint_t	i;

	if (property_value == NULL)
		return;

	switch (property_value->prop_type) {
	case SCHA_PTYPE_STRING:
		free(property_value->val.val_str);
		break;
	case SCHA_PTYPE_ENUM:
		free(property_value->val.val_enum);
		break;
	case SCHA_PTYPE_STRINGARRAY:
		for (i = 0; i < property_value->val.val_strarray->array_cnt;
		    i++) {
			free(property_value->val.val_strarray->str_array[i]);
		}
		free(property_value->val.val_strarray->str_array);
		free(property_value->val.val_strarray);
		break;
	case SCHA_PTYPE_INT:
	case SCHA_PTYPE_BOOLEAN:
	case SCHA_PTYPE_UINTARRAY:
	case SCHA_PTYPE_UINT:
	default:
		break;
	}
	free(property_value);
}

/* Debugging function to print the scds_prop_table structure at debug level */
void
scds_print_ptable(scds_priv_handle_t h, int dbg_lev)
{
	char *str = NULL;

	if (dbg_lev > SCDS_DBG_HIGH)
		dbg_lev = SCDS_DBG_HIGH;

	if (h.ptable.pt_rname)
		scds_syslog_debug(dbg_lev,
		    "R_Name: %s\n", h.ptable.pt_rname);
	else
		scds_syslog_debug(dbg_lev,
		    "R_Name: None\n");

	if (h.ptable.pt_rtname)
		scds_syslog_debug(dbg_lev,
		    "RT_Name: %s\n", h.ptable.pt_rtname);
	else
		scds_syslog_debug(dbg_lev,
		    "RT_Name: None\n");

	if (h.ptable.pt_rgname)
		scds_syslog_debug(dbg_lev,
		    "RG_Name: %s\n", h.ptable.pt_rgname);
	else
		scds_syslog_debug(dbg_lev,
		    "RG_Name: None\n");

	if (h.ptable.pt_current_method)
		scds_syslog_debug(dbg_lev,
		    "Current_Method: %s\n",
		    h.ptable.pt_current_method);
	else
		scds_syslog_debug(dbg_lev,
		    "Current_Method: None\n");

	switch (h.ptable.pt_opcode) {
	case SCDS_OP_NONE:
		scds_syslog_debug(dbg_lev,
		    "Method_Opcode: None\n");
		break;
	case SCDS_OP_CREATE_R:
		scds_syslog_debug(dbg_lev,
		    "Method_Opcode: Create_R\n");
		break;
	case SCDS_OP_UPDATE_R:
		scds_syslog_debug(dbg_lev,
		    "Method_Opcode: Update_R\n");
		break;
	case SCDS_OP_UPDATE_RG:
		scds_syslog_debug(dbg_lev,
		    "Method_Opcode: Update_RG\n");
		break;
	}

	if (h.ptable.pt_r) {
		scds_syslog_debug(dbg_lev,
		    "=== R properties ===\n");
		str = strarray_to_str(h.ptable.pt_r->
		    r_sys_props->p_strong_dep);
		scds_syslog_debug(dbg_lev, "Strong_Dep: %s\n", str);
		free(str);
		str = strarray_to_str(h.ptable.pt_r->
		    r_sys_props->p_restart_dep);
		scds_syslog_debug(dbg_lev, "Restart_Dep: %s\n", str);
		free(str);
		str = strarray_to_str(h.ptable.pt_r->
		    r_sys_props->p_offline_restart_dep);
		scds_syslog_debug(dbg_lev, "Offline_Restart_Dep: %s\n", str);
		free(str);
		str = strarray_to_str(h.ptable.pt_r->r_sys_props->p_weak_dep);
		scds_syslog_debug(dbg_lev, "Weak_Dep: %s\n", str);
		free(str);
		str = strarray_to_str(h.ptable.pt_r->
		    r_sys_props->p_network_resources_used);
		scds_syslog_debug(dbg_lev, "Net_Resources_Used: %s\n", str);
		free(str);
		scds_syslog_debug(dbg_lev,
		    "Failover_Mode: %d\n",
		    h.ptable.pt_r->r_sys_props->p_failover_mode);
		scds_syslog_debug(dbg_lev,
		    "Project_Name: %s\n",
		    (h.ptable.pt_r->r_sys_props->p_project_name == NULL) ?
			"null" : h.ptable.pt_r->r_sys_props->p_project_name);
		scds_syslog_debug(dbg_lev,
		    "Cheap_Probe_Intvl: %d\n",
		    h.ptable.pt_r->r_sys_props->p_cprobe_interval);
		scds_syslog_debug(dbg_lev,
		    "Thorough_Probe_Intvl: %d\n",
		    h.ptable.pt_r->r_sys_props->p_tprobe_interval);
		scds_syslog_debug(dbg_lev,
		    "Retry_count: %d\n",
		    h.ptable.pt_r->r_sys_props->p_retry_count);
		scds_syslog_debug(dbg_lev,
		    "Retry_Interval: %d\n",
		    h.ptable.pt_r->r_sys_props->p_retry_interval);
		scds_syslog_debug(dbg_lev,
		    "Start_Timeout: %d\n",
		    h.ptable.pt_r->r_sys_props->p_start_timeout);
		scds_syslog_debug(dbg_lev,
		    "Stop_Timeout: %d\n",
		    h.ptable.pt_r->r_sys_props->p_stop_timeout);
		scds_syslog_debug(dbg_lev,
		    "Monitor_Stop_Timeout: %d\n",
		    h.ptable.pt_r->r_sys_props->p_monitor_stop_timeout);
		scds_syslog_debug(dbg_lev,
		    "Scalable: %d\n",
		    h.ptable.pt_r->r_sys_props->p_scalable);
		str = strarray_to_str(h.ptable.pt_r->r_sys_props->p_port_list);
		scds_syslog_debug(dbg_lev, "Port_list: %s\n", str);
		free(str);
		str = strarray_to_str(h.ptable.pt_r->
		    r_ext_props->x_config_dirs);
		scds_syslog_debug(dbg_lev, "ConfDirList: %s\n", str);
		free(str);
		if (h.ptable.pt_r->r_ext_props->x_paramtable_version)
			scds_syslog_debug(dbg_lev,
			    "Paramtable_Vers: %s\n",
			    h.ptable.pt_r->r_ext_props->x_paramtable_version);
		scds_syslog_debug(dbg_lev,
		    "SCDS_EXTPROP_MONITOR_RETRY_COUNT: %d\n",
		    h.ptable.pt_r->r_ext_props->x_pmf_arg_n);
		scds_syslog_debug(dbg_lev,
		    "SCDS_EXTPROP_MONITOR_RETRY_INTERVAL: %d\n",
		    h.ptable.pt_r->r_ext_props->x_pmf_arg_t);
		scds_syslog_debug(dbg_lev,
		    "Probe_Timeout: %d\n",
		    h.ptable.pt_r->r_ext_props->x_probe_timeout);
		scds_syslog_debug(dbg_lev,
		    "Child_mon_level: %d\n",
		    h.ptable.pt_r->r_ext_props->x_child_mon_level);
		scds_syslog_debug(dbg_lev,
		    "Failover_enabled: %s\n",
		    h.ptable.pt_r->r_ext_props->x_failover_enabled == B_TRUE ?
			"TRUE" : "FALSE");
		scds_syslog_debug(dbg_lev, "Global_zone_override: %d\n",
		    h.ptable.pt_r->r_sys_props->p_gz_override ? "TRUE" :
		    "FALSE");
	} else
		scds_syslog_debug(dbg_lev,
		    "R props not available\n");

	if (h.ptable.pt_rt) {
		scds_syslog_debug(dbg_lev,
		    "=== RT properties ===\n");
		if (h.ptable.pt_rt->rt_svc_name != NULL)
			scds_syslog_debug(dbg_lev,
			    "Svc_Name: %s\n",
			    h.ptable.pt_rt->rt_svc_name);
		scds_syslog_debug(dbg_lev,
		    "Single_Instance: %d\n",
		    h.ptable.pt_rt->rt_single_inst);
		scds_syslog_debug(dbg_lev,
		    "Init_Nodes: %d\n",
		    h.ptable.pt_rt->rt_init_nodes);
		str = strarray_to_str(h.ptable.pt_rt->rt_install_nodes);
		scds_syslog_debug(dbg_lev, "Install_Nodes: %s\n", str);
		free(str);
		scds_syslog_debug(dbg_lev,
		    "FailOver: %d\n",
		    h.ptable.pt_rt->rt_failover);
		scds_syslog_debug(dbg_lev,
		    "Api_Version: %d\n",
		    h.ptable.pt_rt->rt_api_version);
		if (h.ptable.pt_rt->rt_version != NULL)
			scds_syslog_debug(dbg_lev,
			    "RT_Version: %s\n",
			    h.ptable.pt_rt->rt_version);
		if (h.ptable.pt_rt->rt_basedir != NULL)
			scds_syslog_debug(dbg_lev,
			    "BaseDir: %s\n",
			    h.ptable.pt_rt->rt_basedir);
		if (h.ptable.pt_rt->rt_start_method != NULL)
			scds_syslog_debug(dbg_lev,
			"Start_Method: %s\n",
			    h.ptable.pt_rt->rt_start_method);
		if (h.ptable.pt_rt->rt_stop_method != NULL)
			scds_syslog_debug(dbg_lev,
			    "Stop_Method: %s\n",
			    h.ptable.pt_rt->rt_stop_method);
	} else
		scds_syslog_debug(dbg_lev,
		    "RT props not available\n");

	if (h.ptable.pt_rg) {
		scds_syslog_debug(dbg_lev,
		    "=== RG properties ===\n");
		str = strarray_to_str(h.ptable.pt_rg->rg_nodelist);
		scds_syslog_debug(dbg_lev, "Nodelist: %s\n", str);
		free(str);
		scds_syslog_debug(dbg_lev,
		    "Max_Primaries: %d\n",
		    h.ptable.pt_rg->rg_max_primaries);
		scds_syslog_debug(dbg_lev,
		    "Desired_Primaries: %d\n",
		    h.ptable.pt_rg->rg_desired_primaries);
		str = strarray_to_str(h.ptable.pt_rg->rg_glb_rs_used);
		scds_syslog_debug(dbg_lev, "Global_R_Used: %s\n", str);
		free(str);
		scds_syslog_debug(dbg_lev,
		    "RG_mode: %d\n",
		    h.ptable.pt_rg->rg_mode);
		scds_syslog_debug(dbg_lev,
		    "RG_Impl_Net_Depend: %d\n",
		    h.ptable.pt_rg->rg_impl_net_depend);
		scds_syslog_debug(dbg_lev,
		    "Path_Prefix: %s\n",
		    h.ptable.pt_rg->rg_pathprefix);
		scds_syslog_debug(dbg_lev,
		    "RG_Project_Name: %s\n",
		    (h.ptable.pt_rg->rg_project_name == NULL) ?
			"null" : h.ptable.pt_rg->rg_project_name);
		/* SC SLM addon start */
		scds_syslog_debug(dbg_lev,
		    "RG_SLM_type: %s\n",
		    (h.ptable.pt_rg->rg_slm_type == NULL) ?
		    "null" : h.ptable.pt_rg->rg_slm_type);
		scds_syslog_debug(dbg_lev,
		    "RG_SLM_pset_type: %s\n",
		    (h.ptable.pt_rg->rg_slm_pset_type == NULL) ?
		    "null" : h.ptable.pt_rg->rg_slm_pset_type);
		scds_syslog_debug(dbg_lev,
		    "RG_SLM_CPU_SHARES: %d\n",
		    h.ptable.pt_rg->rg_slm_cpu);
		scds_syslog_debug(dbg_lev,
		    "RG_SLM_PSET_MIN: %d\n",
		    h.ptable.pt_rg->rg_slm_cpu_min);
		/* SC SLM addon end */
		str = strarray_to_str(h.ptable.pt_rg->rg_affinities);
		scds_syslog_debug(dbg_lev, "RG_Affinities: %s\n", str);
		free(str);
	} else
		scds_syslog_debug(dbg_lev,
		    "RG props not available\n");
/*
 * Suppressing lint error.
 * Info(1746) [c:0]: parameter 'h' could be made const reference
 */
} /*lint !e1746 */



/*
 * Break a string of format "keyword=values" into two substrings, pointed
 * by 'key' and 'val'.  The delimiter is '='.
 */
scha_err_t
str_kv(char *s, char **key, char **val)
{
	char *tmp;

	if (s == NULL)
		return (SCHA_ERR_INVAL);

	tmp = strdup(s);
	if (tmp == NULL)
		return (SCHA_ERR_NOMEM);

	*key = (char *)strchr(tmp, '=');

	if (*key == NULL) {
		return (SCHA_ERR_INVAL);
	} else {
		*val = *key + sizeof (char);
		**key = '\0';
		*key = tmp;
	}

	return (SCHA_ERR_NOERR);
}

/* Convert the string to integer */
scha_err_t
str_to_int(char *s, int *i)
{
	char    internal_err_str[INT_ERR_MSG_SIZE];

	if (s == NULL)
		return (SCHA_ERR_INVAL);

	if (sscanf(s, "%d", i) == 0) {
			(void) snprintf(internal_err_str,
			    sizeof (internal_err_str),
			    "Failed to convert string %s to int", s);

		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (SCHA_ERR_INTERNAL);
	}

	return (SCHA_ERR_NOERR);
}


/*
 * Converts the string "TRUE" to a boolean_t value B_TRUE and
 * B_FALSE for others.
 */
boolean_t
str_to_bool(char *s)
{
	return ((strcasecmp(s, SCHA_TRUE) == 0) ? B_TRUE : B_FALSE);
}

/*
 * Converts the strings into scha_rgmode_t.
 *
 * Return default value "RGMODE_FAILOVER" if meets any error.
 */
scha_rgmode_t
str_to_rgmode(char *s)
{
	if (s != NULL) {
		if (strcasecmp(s, SCHA_FAILOVER) == 0)
			return (RGMODE_FAILOVER);
		else if (strcasecmp(s, SCHA_SCALABLE) == 0)
			return (RGMODE_SCALABLE);
	}
	return (RGMODE_FAILOVER);
}

/*
 * Checks whether the input string contains wildcard value (*).
 * If yes, updates the is_ALL_value to B_TRUE, otherwise calls
 * str_to_strarray function.
 *
 * NOTE: Currently, this function is called only for GLOBAL_RESOURCES_USED
 * property.
 */
scha_err_t
process_wildcard_value(char *str, scha_str_array_t **sa)
{

	scha_err_t err;
	size_t	size = 1;

	/*
	 * Check if the "*" value is specified.
	 * If yes, update the is_ALL_value to be true.
	 * Otherwise, call str_to_strarray.
	 */
	if (strcmp(str, SCHA_ALL_SPECIAL_VALUE) == 0) {

		*sa = (scha_str_array_t *)
		    calloc(size, sizeof (scha_str_array_t));
		if (*sa == NULL) {
			scds_syslog(LOG_ERR,
			    "Out of memory.");
			return (SCHA_ERR_NOMEM);
		}

		(*sa)->is_ALL_value = B_TRUE;
		(*sa)->str_array = NULL;
		(*sa)->array_cnt = 0;

		err = SCHA_ERR_NOERR;

	} else {

		err = str_to_strarray(str, sa);
	}

	return (err);
}

/*
 * Converts the input string of format "s1,s2,..." to scha_str_array_t.
 * If the input string is null, then it returns the structure with
 * the following values:
 *		array_cnt = 0
 *		is_ALL_value = B_FALSE
 *		str_array = NULL
 */
scha_err_t
str_to_strarray(char *str, scha_str_array_t **sa)
{
	size_t	size = 1;
	char *temp_str, *val, **ptr;
	char *token = ",";
	scha_err_t err;
	char *save_ptr;
	uint_t i;

	*sa = NULL;

	*sa = (scha_str_array_t *)calloc(size, sizeof (scha_str_array_t));
	if (*sa == NULL) {
		scds_syslog(LOG_ERR,
		    "Out of memory.");
		return (SCHA_ERR_NOMEM);
	}

	/* Initialize the structure elements */
	(*sa)->is_ALL_value = B_FALSE;
	(*sa)->array_cnt = 0;
	(*sa)->str_array = NULL;

	/* Check whether the input string is NULL */
	if (str == NULL || str[0] == '\0') {
		/* If the input string is NULL, return success */
		scds_syslog_debug(SCDS_DBG_HIGH, "Null value specified.");
		return (SCHA_ERR_NOERR);
	}

	/*
	 * strtok function will change the input string, so save the
	 * input string.
	 */
	val = strdup(str);
	if (val == NULL) {
		scds_syslog(LOG_ERR,
		    "Out of memory.");
		free(*sa);
		return (SCHA_ERR_NOMEM);
	}

	/*
	 * strtok function is not MT-Safe, instead use strtok_r
	 * which is MT-safe
	 */
	temp_str = strtok_r(val, token, &save_ptr);

	while (temp_str != NULL) {

		ptr = (char **)realloc((*sa)->str_array,
		    ((*sa)->array_cnt + 1) * (sizeof (char *)));
		if (ptr == NULL) {
			scds_syslog(LOG_ERR,
			    "Out of memory.");
			err = SCHA_ERR_NOMEM;
			goto finished;
		}

		ptr[(*sa)->array_cnt] = strdup(temp_str);
		if (ptr[(*sa)->array_cnt] == NULL) {
			scds_syslog(LOG_ERR,
			    "Out of memory.");
			err = SCHA_ERR_NOMEM;
			goto finished;
		}

		/* Update the (*sa) structure with new values */
		(*sa)->str_array = ptr;
		(*sa)->array_cnt++;

		/* get the next token(element) from the string */
		temp_str = strtok_r(NULL, token, &save_ptr);
	}

	/* No need of val anymore, free it */
	free(val);

	return (SCHA_ERR_NOERR);

finished:

	if (*sa) {
		if ((*sa)->str_array) {
			for (i = 0; i < (*sa)->array_cnt; i++) {
				free((*sa)->str_array[i]);
			}
			free((*sa)->str_array);
		}
		free(*sa);
	}

	if (val)
		free(val);

	return (err);
}

/*
 * Convert an array of strings to a string and each sub-string is
 * separated by a comma character
 */
char *
strarray_to_str(scha_str_array_t *sa)
{
	char s[SCDS_ARRAY_SIZE];
	char tmp[SCDS_ARRAY_SIZE];
	char *ptr;
	uint_t i;

	if (sa == NULL || sa->array_cnt == 0) {
		ptr = strdup("NULL");
		return (ptr);
	}

	s[0] = '\0';
	for (i = 0; i < sa->array_cnt - 1; i++) {
		(void) sprintf(tmp, "%s,", sa->str_array[i]);
		(void) strcat(s, tmp);
	}

	(void) strcat(s, sa->str_array[i]);

	ptr = strdup(s);
	return (ptr);
}

/* get functions for properties */

/* get functions which return miscallenous information */

const char*
scds_get_resource_name(scds_handle_t handle)
{
	scds_priv_handle_t	*h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}

	h = (scds_priv_handle_t *)handle;

	return (h->ptable.pt_rname);
}

const char*
scds_get_resource_type_name(scds_handle_t handle)
{
	scds_priv_handle_t	*h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_rtname);
}

const char*
scds_get_resource_group_name(scds_handle_t handle)
{
	scds_priv_handle_t	*h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}

	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_rgname);
}

const char*
scds_get_current_method_name(scds_handle_t handle)
{
	scds_priv_handle_t	*h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_current_method);
}

const char*
scds_get_zone_name(scds_handle_t handle)
{
	scds_priv_handle_t	*h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}

	h = (scds_priv_handle_t *)handle;

#if (SOL_VERSION >= __s10)
	if (h->ptable.pt_zonename == NULL)
		return (NULL);
	if (strcmp(h->ptable.pt_zonename, "global") == 0)
		return (NULL);
#endif
	return (h->ptable.pt_zonename);
}

const char *
scds_get_zone_type(scds_handle_t handle) {
	scds_priv_handle_t *h;
	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->is_zone_cluster ? "CLUSTER" : "NATIVE");
}

boolean_t
scds_is_zone_cluster(scds_handle_t handle) {
	scds_priv_handle_t *h;
	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->is_zone_cluster);
}

/* get functions which return Resource properties */

scha_switch_t
scds_get_rs_on_off_switch(scds_handle_t handle)
{
	scds_priv_handle_t	*h;

	if (handle == NULL) {
		scds_syslog(LOG_ERR,
		    "Null value is passed for the handle.");
		abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_r->r_on_off_switch);
}

scha_err_t
scds_get_rs_on_off_switch_node(scds_handle_t handle, char *nodename,
    scha_switch_t *swtch)
{
	scds_priv_handle_t	*h;
	scha_err_t err = SCHA_ERR_NOERR;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	err = scha_resource_get_zone(h->ptable.pt_zonename,
	    h, SCHA_ON_OFF_SWITCH_NODE, nodename, &swtch);
	if (err != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * The query for a property on the specified node failed. The
		 * reason for the failure is given in the message.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve resource property %s on %s: %s",
		    SCHA_ON_OFF_SWITCH, nodename, scds_error_string(err));
	}
	return (err);
}

scha_switch_t
scds_get_rs_monitored_switch(scds_handle_t handle)
{
	scds_priv_handle_t	*h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_r->r_fm_switch);
}

scha_err_t
scds_get_rs_monitored_switch_node(scds_handle_t handle, char *nodename,
    scha_switch_t *swtch)
{
	scds_priv_handle_t	*h;
	scha_err_t err = SCHA_ERR_NOERR;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	err = scha_resource_get_zone(h->ptable.pt_zonename,
	    h, SCHA_MONITORED_SWITCH_NODE,
	    nodename, &swtch);
	if (err != SCHA_ERR_NOERR) {
		scds_syslog(get_log_priority(err),
		    "Failed to retrieve resource property %s on %s: %s",
		    SCHA_MONITORED_SWITCH, nodename, scds_error_string(err));
	}
	return (err);
}

const scha_str_array_t*
scds_get_rs_resource_dependencies(scds_handle_t handle)
{
	scds_priv_handle_t *h;
	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}

	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_r->r_sys_props->p_strong_dep);
}

const scha_str_array_t*
scds_get_rs_resource_dependencies_weak(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_r->r_sys_props->p_weak_dep);
}

const scha_str_array_t*
scds_get_rs_resource_dependencies_restart(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_r->r_sys_props->p_restart_dep);
}

const scha_str_array_t*
scds_get_rs_resource_dependencies_offline_restart(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_r->r_sys_props->p_offline_restart_dep);
}

const scha_str_array_t*
scds_get_rs_resource_dependencies_Q(scds_handle_t handle)
{
	scds_priv_handle_t *h;
	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}

	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_r->r_sys_props->p_strong_dep_q);
}

const scha_str_array_t*
scds_get_rs_resource_dependencies_Q_weak(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_r->r_sys_props->p_weak_dep_q);
}

const scha_str_array_t*
scds_get_rs_resource_dependencies_Q_restart(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_r->r_sys_props->p_restart_dep_q);
}

const scha_str_array_t*
scds_get_rs_resource_dependencies_Q_offline_restart(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_r->r_sys_props->p_offline_restart_dep_q);
}

scha_str_array_t*
scds_get_rs_network_resources_used(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_r->r_sys_props->p_network_resources_used);
}

scha_failover_mode_t
scds_get_rs_failover_mode(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_r->r_sys_props->p_failover_mode);
}

int
scds_get_rs_cheap_probe_interval(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_r->r_sys_props->p_cprobe_interval);
}

int
scds_get_rs_thorough_probe_interval(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_r->r_sys_props->p_tprobe_interval);
}

int
scds_get_rs_retry_count(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_r->r_sys_props->p_retry_count);
}

int
scds_get_rs_retry_interval(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_r->r_sys_props->p_retry_interval);
}

int
scds_get_rs_start_timeout(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_r->r_sys_props->p_start_timeout);
}

int
scds_get_rs_stop_timeout(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_r->r_sys_props->p_stop_timeout);
}

int
scds_get_rs_monitor_stop_timeout(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_r->r_sys_props->p_monitor_stop_timeout);
}

boolean_t
scds_get_rs_scalable(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_r->r_sys_props->p_scalable);
}

boolean_t
scds_get_rs_global_zone_override(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_r->r_sys_props->p_gz_override);
}

const char*
scds_get_rs_resource_project_name(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_r->r_sys_props->p_project_name);
}

/* Caller of this function has to free the memory */
scha_err_t
scds_get_port_list(scds_handle_t handle, scds_port_list_t **port_list)
{

	int num_of_ports;
	int i;
	scds_port_list_t *port_l;
	scds_priv_handle_t *h;
	size_t	size = 1;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;

	*port_list = NULL;
	if (h->ptable.pt_r->r_sys_props->p_netaddr_list == NULL) {
		return (SCHA_ERR_NOERR);
	}
	num_of_ports =
	    h->ptable.pt_r->r_sys_props->p_netaddr_list->num_netaddrs;

	port_l = (scds_port_list_t *)
	    calloc(size, sizeof (scds_port_list_t));

	if (port_l == NULL) {
		scds_syslog(LOG_ERR, "Out of memory.");
		return (SCHA_ERR_NOMEM);
	}

	/* Initialize the values */
	port_l->num_ports = 0;
	port_l->ports = NULL;

	/*
	 * Suppressing 64-bit lint complaint:
	 * Info(747) [c:39]: Significant prototype coercion
	 * (arg. no. 1) unsigned int to unsigned long
	 */
	port_l->ports = (scds_port_t *)
	    calloc((uint_t)num_of_ports, sizeof (scds_port_t));	/*lint !e747 */

	if (port_l->ports == NULL) {
		scds_syslog(LOG_ERR, "Out of memory.");
		return (SCHA_ERR_NOMEM);
	}

	for (i = 0; i < num_of_ports; i++) {
		port_l->num_ports++;
		port_l->ports[i] = h->ptable.pt_r->r_sys_props->
			p_netaddr_list->netaddrs[i].port_proto;
	}

	*port_list = port_l;

	return (SCHA_ERR_NOERR);
}

/* get functions which return common extension properties */

scha_str_array_t*
scds_get_ext_confdir_list(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}

	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_r->r_ext_props->x_config_dirs);
}

int
scds_get_ext_monitor_retry_count(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_r->r_ext_props->x_pmf_arg_n);
}

int
scds_get_ext_monitor_retry_interval(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_r->r_ext_props->x_pmf_arg_t);
}

int
scds_get_ext_probe_timeout(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_r->r_ext_props->x_probe_timeout);
}

int
scds_get_child_mon_level(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_r->r_ext_props->x_child_mon_level);
}


/* get functions which return Resource Group Properties */

const scha_str_array_t*
scds_get_rg_nodelist(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_rg->rg_nodelist);
}


int
scds_get_rg_maximum_primaries(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_rg->rg_max_primaries);
}

int
scds_get_rg_desired_primaries(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_rg->rg_desired_primaries);
}

const scha_str_array_t*
scds_get_rg_global_resources_used(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_rg->rg_glb_rs_used);
}

const scha_str_array_t*
scds_get_rg_resource_list(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_rg->rg_rlist);
}

scha_rgmode_t
scds_get_rg_rg_mode(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_rg->rg_mode);
}

boolean_t
scds_get_rg_implicit_network_dependencies(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_rg->rg_impl_net_depend);
}

const char*
scds_get_rg_pathprefix(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_rg->rg_pathprefix);
}

int
scds_get_rg_pingpong_interval(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_rg->rg_ppinterval);
}

const char*
scds_get_rg_rg_project_name(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_rg->rg_project_name);
}


/* SC SLM addon start */
const char*
scds_get_rg_rg_slm_type(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_rg->rg_slm_type);
}

const char*
scds_get_rg_rg_slm_pset_type(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_rg->rg_slm_pset_type);
}

int
scds_get_rg_rg_slm_cpu_shares(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_rg->rg_slm_cpu);
}

int
scds_get_rg_rg_slm_pset_min(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_rg->rg_slm_cpu_min);
}
/* SC SLM addon end */

const scha_str_array_t*
scds_get_rg_rg_affinities(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_rg->rg_affinities);
}

boolean_t
scds_get_rt_single_instance(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_rt->rt_single_inst);
}

scha_initnodes_flag_t
scds_get_rt_init_nodes(scds_handle_t handle)
{
	scds_priv_handle_t *h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_rt->rt_init_nodes);
}

const scha_str_array_t*
scds_get_rt_installed_nodes(scds_handle_t handle)
{
	scds_priv_handle_t	*h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_rt->rt_install_nodes);
}

boolean_t
scds_get_rt_failover(scds_handle_t handle)
{
	scds_priv_handle_t	*h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_rt->rt_failover);
}

int
scds_get_rt_api_version(scds_handle_t handle)
{
	scds_priv_handle_t	*h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_rt->rt_api_version);
}

/*
 * Caller needs to free the memory allocated in this
 * function. We are ensured that zname is non-null
 */
scha_err_t
scds_get_fullname(const char *zname, char **fullname, boolean_t is_zone_cluster)
{
	return (scds_get_fullname_helper(zname, fullname, is_zone_cluster,
	    B_FALSE));
}


/*
 * Similar to scds_get_fullname() but returns the ascii nodeid number
 * instead of the nodename as the first element of the fullname,.
 */
scha_err_t
scds_get_fullname_nodeid(const char *zname, char **fullname,
    boolean_t is_zone_cluster)
{
	return (scds_get_fullname_helper(zname, fullname, is_zone_cluster,
	    B_TRUE));
}


/*
 * Helper function for scds_get_fullname() and scds_get_fullname_nodeid().
 *
 * If use_nodeid is true, the 'nodename' portion of the returned fullname
 * will contain the numeric nodeid (in ascii); otherwise it contains the
 * nodename.
 */
scha_err_t
scds_get_fullname_helper(const char *zname, char **fullname,
    boolean_t is_zone_cluster, boolean_t use_nodeid)
{
	char *nodename = NULL;
	char *fname = NULL;
	scha_err_t e;
	scha_cluster_t clusterhandle;

	if (zname == NULL) {
		return (SCHA_ERR_INVAL);
	}
	e = scha_cluster_open_zone(zname, &clusterhandle);
	if (e != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the cluster handle : %s.",
		    scds_error_string(e));
	}
	if (use_nodeid) {
		uint_t nodeid = 0;
		char nodeid_string[32];

		e = scha_cluster_get_zone(zname, clusterhandle,
		    SCHA_NODEID_LOCAL, &nodeid);
		if (e == SCHA_ERR_NOERR) {
			(void) snprintf(nodeid_string, 32, "%u", nodeid);
			if ((nodename = strdup(nodeid_string)) == NULL) {
				(void) scha_cluster_close_zone(zname,
				    clusterhandle);
				return (SCHA_ERR_NOMEM);
			}
		}
	} else {
		e = scha_cluster_get_zone(zname, clusterhandle,
		    SCHA_NODENAME_LOCAL, &nodename);
	}
	if (e != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		    scds_error_string(e));
		(void) scha_cluster_close_zone(zname, clusterhandle);
		return (e);
	}
	if (is_zone_cluster) {
		*fullname = strdup(nodename);
		(void) scha_cluster_close_zone(zname, clusterhandle);
		return (SCHA_ERR_NOERR);
	}
	/* it's a native zone */

	if ((fname = (char *)malloc(strlen(nodename) + strlen(zname) +
	    2)) == NULL) {
		(void) scha_cluster_close_zone(zname, clusterhandle);
		return (SCHA_ERR_NOMEM);
	}
	(void) sprintf(fname, "%s:%s", nodename, zname);
	*fullname = fname;
	(void) scha_cluster_close_zone(zname, clusterhandle);
	return (SCHA_ERR_NOERR);
}	

const char*
scds_get_rt_rt_version(scds_handle_t handle)
{
	scds_priv_handle_t	*h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_rt->rt_version);
}

const char*
scds_get_rt_rt_basedir(scds_handle_t handle)
{
	scds_priv_handle_t	*h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_rt->rt_basedir);
}

const char*
scds_get_rt_start_method(scds_handle_t handle)
{
	scds_priv_handle_t	*h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_rt->rt_start_method);
}

const char*
scds_get_rt_stop_method(scds_handle_t handle)
{
	scds_priv_handle_t	*h;

	if (handle == NULL) {
	    scds_syslog(LOG_ERR,
		"Null value is passed for the handle.");
	    abort();
	}
	h = (scds_priv_handle_t *)handle;
	return (h->ptable.pt_rt->rt_stop_method);
}

/*
 * Save the original values of optarg, optind, opterr, and optopt.
 * This function should be called before calling getopt().
 * Since getopt() is MT-unsafe, this routine assumes that the caller is
 * holding the mutex before accessing the getopt related variables.
 * Also reset optind to 1, it could have been changed by DSDL caller,
 * and set opterr to 0 to disable the error messages printed on stderr
 * by getopt().
 */
void
save_getopt_vars(char **o_optarg, int *o_optind, int *o_opterr, int *o_optopt)
{
	*o_optarg = optarg;
	*o_optind = optind;
	*o_opterr = opterr;
	*o_optopt = optopt;

	optind = 1;
	opterr = 0;
}

/*
 * Restore the original values of getopt related variables set by caller
 */
void
restore_getopt_vars(char *o_optarg, int o_optind, int o_opterr, int o_optopt)
{
	optarg = o_optarg;
	optind = o_optind;
	opterr = o_opterr;
	optopt = o_optopt;
}

/*
 * Duplicate the contents of the supplied ext property.
 * Works in all-or-nothing mode: if NULL is returned, the
 * function has cleaned up any memory allocated upto the
 * point at which it failed.
 */
scha_extprop_value_t *
dup_ext_property(scha_extprop_value_t *prop)
{
	scha_extprop_value_t *rv;
	uint_t i, num;

	/* basic check */
	if (prop == NULL)
		return (NULL);

	rv = (scha_extprop_value_t *)calloc((size_t)1,
	    sizeof (scha_extprop_value_t));
	if (rv == NULL)
		return (NULL);

	switch (rv->prop_type = prop->prop_type) {
	case SCHA_PTYPE_STRING:
		/* make a duplicate of the string */
		if ((rv->val.val_str = strdup(prop->val.val_str)) == NULL) {
			free(rv);
			rv = NULL;
		}
		break;
	case SCHA_PTYPE_INT:
		rv->val.val_int = prop->val.val_int;
		break;
	case SCHA_PTYPE_BOOLEAN:
		rv->val.val_boolean = prop->val.val_boolean;
		break;
	case SCHA_PTYPE_ENUM:
		/* make a duplicate of the text */
		if ((rv->val.val_enum = strdup(prop->val.val_enum)) == NULL) {
			free(rv);
			rv = NULL;
		}
		break;
	case SCHA_PTYPE_STRINGARRAY:
		/* allocate space for the strarray container */
		rv->val.val_strarray = (scha_str_array_t *)calloc((size_t)1,
			sizeof (scha_str_array_t));
		if (rv->val.val_strarray == NULL) {
			free(rv);
			rv = NULL;
			break;
		}

		num = rv->val.val_strarray->array_cnt =
		    prop->val.val_strarray->array_cnt;
		rv->val.val_strarray->is_ALL_value =
		    prop->val.val_strarray->is_ALL_value;

		/* space for the string array */
		rv->val.val_strarray->str_array = (char **)calloc((size_t)num,
		    sizeof (char *));
		if (rv->val.val_strarray->str_array == NULL) {
			free(rv->val.val_strarray);
			free(rv);
			rv = NULL;
			break;
		}

		/* so far so good, now populate the string array */
		for (i = 0; i < num; i++) {
			rv->val.val_strarray->str_array[i] =
			    strdup(prop->val.val_strarray->str_array[i]);
			if (rv->val.val_strarray->str_array[i] == NULL) {
				/*
				 * backtrack and free all strings that were
				 * successfully allocated.
				 */
				while (i)
					free(rv->val.val_strarray->
					    str_array[--i]);

				/* ....and the rest of the stuff too */
				free(rv->val.val_strarray->str_array);
				free(rv->val.val_strarray);
				free(rv);
				rv = NULL;

				/*
				 * out of the for loop, not the switch-case,
				 * although it boils down to the same thing
				 */
				break;
			}
		}

		break;

	/*
	 * scds_get_ext_prop_from_args marks these two as invalid,
	 * so we should refuse to deal with them too.
	 */
	case SCHA_PTYPE_UINTARRAY:
	case SCHA_PTYPE_UINT:
	default:
		free(rv);
		rv = NULL;
	}

	return (rv);
}
