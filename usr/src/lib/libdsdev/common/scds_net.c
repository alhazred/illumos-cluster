/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * scds_net.c - Various TCP related utilities for data services
 *	which use the SC3.0 API.
 */

#pragma ident	"@(#)scds_net.c	1.50	09/01/09 SMI"

/*
 * Suppress lint message: "Info(793) [c:23]: ANSI limit of 511
 * 'external identifiers' exceeded -- processing is unaffected"
 */
/* CSTYLED */
/*lint -"esym(793,external identifiers)" */

#include <sys/os.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <sys/time.h>
#include <signal.h>
#include <errno.h>
#include <strings.h>
#include <unistd.h>
#include <fcntl.h>

#ifdef linux
#include <sys/poll.h>
#endif

#include <scha.h>
#include <rgm/scha_strings.h>

#include <rgm/libdsdev.h>
#include "libdsdev_priv.h"

/*
 * Suppress lint message: "Info(793) [c:23]: ANSI limit of 511
 * 'external identifiers' exceeded -- processing is unaffected"
 */
/* CSTYLED */
/*lint -"esym(793,external identifiers)" */

/*
 * Forward declaration of some routines in this
 * file. These are private versions of the public
 * routines. These routines are called after
 * locking scds_mutex in the public version
 * of these routines.
 */

static scha_err_t priv_get_rs_hostnames(
	scds_priv_handle_t *h, scds_net_resource_list_t **snrl);

static scha_err_t priv_get_rg_hostnames(char *zname,
	char *rgname, scds_net_resource_list_t **snrl);

static void priv_print_net_list(
	scds_priv_handle_t *h, int level,
	const scds_net_resource_list_t *netresource_list);

static scha_err_t priv_get_netaddr_list(
	scds_priv_handle_t *h,
	scds_netaddr_list_t **netaddr_list);

static scha_err_t priv_fm_tcp_connect(
	scds_priv_handle_t *h, int *s, const char *host,
	int port, time_t timeout);

static scha_err_t priv_fm_tcp_disconnect(
	scds_priv_handle_t *h, int s, time_t timeout);

static scha_err_t priv_fm_tcp_read(
	scds_priv_handle_t *h, int s, char *buf,
	size_t *size, time_t timeout);

static scha_err_t priv_fm_tcp_write(
	scds_priv_handle_t *h, int s, char *buf,
	size_t *size, time_t timeout);

static scha_err_t priv_fm_net_connect(
	scds_socket_t *socklist, scds_netaddr_t addr,
	time_t timeout);


/*
 * scds_get_rs_hostnames()
 * Get the hostnames used by a resource.
 * External API
 */
scha_err_t
scds_get_rs_hostnames(scds_handle_t handle,
	scds_net_resource_list_t **snrl)
{
	scds_priv_handle_t	*h;
	scha_err_t			e;

	h = check_handle(handle);

	(void) mutex_lock(&h->scds_mutex);

	e = priv_get_rs_hostnames(h, snrl);

	(void) mutex_unlock(&h->scds_mutex);
	return (e);
}

/*
 * scds_get_rg_hostnames()
 * Get the hostnames used by a RG.
 * External API
 */
scha_err_t
scds_get_rg_hostnames(char *resourcegroup_name,
	scds_net_resource_list_t **netresource_list)
{
	scha_err_t			e;

	if (resourcegroup_name == NULL) {
		return (SCHA_ERR_INVAL);
	}

	e = priv_get_rg_hostnames(NULL, resourcegroup_name, netresource_list);

	return (e);
}

scha_err_t
scds_get_rg_hostnames_zone(char *zone_name, char *resourcegroup_name,
	scds_net_resource_list_t **netresource_list)
{
	scha_err_t			e;

	if (resourcegroup_name == NULL) {
		return (SCHA_ERR_INVAL);
	}

	e = priv_get_rg_hostnames(zone_name,
	    resourcegroup_name, netresource_list);

	return (e);
}


/*
 * scds_print_net_list()
 * syslog()s the contents of a scds_net_resource_list_t
 * External API
 */
void
scds_print_net_list(scds_handle_t handle, int debug_level,
	const scds_net_resource_list_t *netresource_list)
{
	scds_priv_handle_t	*h;

	h = check_handle(handle);

	(void) mutex_lock(&h->scds_mutex);

	priv_print_net_list(h, debug_level, netresource_list);

	(void) mutex_unlock(&h->scds_mutex);
}


/*
 * scds_get_netaddr_list()
 * Returns the ip/port/proto 3-tuple
 * External API
 */
scha_err_t
scds_get_netaddr_list(scds_handle_t handle,
	scds_netaddr_list_t **netaddr_list)
{
	scds_priv_handle_t	*h;
	scha_err_t			e;

	h = check_handle(handle);

	if (netaddr_list == NULL) {
		return (SCHA_ERR_INVAL);
	}
	(void) mutex_lock(&h->scds_mutex);

	e = priv_get_netaddr_list(h, netaddr_list);

	(void) mutex_unlock(&h->scds_mutex);
	return (e);
}

/*
 * scds_free_netaddr_list()
 * Frees memory allocated in a scds_netaddr_list_t
 * Note: The array netaddr_list->netaddrs[] is
 * allocated in a single realloc() call in scds_get_netaddr_list.
 * Thus the whole array is free()ed in one single operation.
 * we still need to free the hostname field in each individual
 * scds_netaddr_t in the array.
 * External API
 */
void
scds_free_netaddr_list(scds_netaddr_list_t *netaddr_list)
{
	int		i;
	scds_netaddr_t	*n;

	if (netaddr_list == NULL) {
		return;
	}
	for (i = 0; i < netaddr_list->num_netaddrs; i++) {
		n = &(netaddr_list->netaddrs[i]);
		if (n->hostname)
			free(n->hostname);
	}
	free(netaddr_list->netaddrs);
	free(netaddr_list);
}

/*
 * scds_free_port_list()
 * Frees memory allocated in a scds_port_list_t
 * External API
 */
void
scds_free_port_list(scds_port_list_t *port_list)
{
	if (port_list) {
		if (port_list->ports)
			free(port_list->ports);
		free(port_list);
	}
}

/*
 * scds_print_netaddr_list()
 * Syslog the NetAddr 3-tuples used by a resource.
 * External API
 */
void
scds_print_netaddr_list(scds_handle_t handle, int debug_level,
	const scds_netaddr_list_t *netaddr_list)
{
	scds_priv_handle_t	*h;
	scds_netaddr_t		*n;
	int					i;

	h = check_handle(handle);

	if (netaddr_list == NULL) {
		return;
	}
	(void) mutex_lock(&h->scds_mutex);

	for (i = 0; i < netaddr_list->num_netaddrs; i++) {
		n = &(netaddr_list->netaddrs[i]);
		scds_syslog_debug(debug_level, "NetAddr %s/%d/%d\n",
			n->hostname, n->port_proto.port, n->port_proto.proto);
	}

	(void) mutex_unlock(&h->scds_mutex);
}

/*
 * scds_fm_tcp_connect()
 * Connect to specified host and port with
 * the given timeout
 * External API
 */

scha_err_t
scds_fm_tcp_connect(scds_handle_t handle, int *sock,
	const char *hostname, int port, time_t timeout)
{
	scds_priv_handle_t	*h;
	scha_err_t			e;

	h = check_handle(handle);

	(void) mutex_lock(&h->scds_mutex);

	e = priv_fm_tcp_connect(h, sock, hostname, port, timeout);

	(void) mutex_unlock(&h->scds_mutex);
	return (e);
}

/*
 * scds_fm_tcp_disconnect()
 * Disconnect socket in specified timeout
 * External API
 */

scha_err_t
scds_fm_tcp_disconnect(scds_handle_t handle, int sock, time_t timeout)
{
	scds_priv_handle_t	*h;
	scha_err_t			e;

	h = check_handle(handle);

	(void) mutex_lock(&h->scds_mutex);

	e = priv_fm_tcp_disconnect(h, sock, timeout);

	(void) mutex_unlock(&h->scds_mutex);
	return (e);
}

/*
 * scds_fm_tcp_read()
 * Read specified number of bytes from socket,
 * with the specified timeout.
 * External API
 */

scha_err_t
scds_fm_tcp_read(scds_handle_t handle, int sock,
	char *buffer, size_t *size, time_t timeout)
{
	scds_priv_handle_t	*h;
	scha_err_t			e;

	h = check_handle(handle);

	(void) mutex_lock(&h->scds_mutex);

	e = priv_fm_tcp_read(h, sock, buffer, size, timeout);

	(void) mutex_unlock(&h->scds_mutex);
	return (e);
}


/*
 * scds_fm_tcp_write()
 * Write specified number of bytes to socket,
 * with the specified timeout.
 * External API
 */
scha_err_t
scds_fm_tcp_write(scds_handle_t handle, int sock,
	char *buffer, size_t *size, time_t timeout)
{
	scds_priv_handle_t	*h;
	scha_err_t			e;

	h = check_handle(handle);

	(void) mutex_lock(&h->scds_mutex);

	e = priv_fm_tcp_write(h, sock, buffer, size, timeout);

	(void) mutex_unlock(&h->scds_mutex);
	return (e);
}

/*
 * Use MIN_DISCONNECT_TIME to close a connection, else
 * we would start leaking fds if timeouts occur.
 */
#define	MIN_DISCONNECT_TIME	2

/*
 * scds_simple_probe()
 * Connect and disconnect to the specifed host and port,
 * with the specified timeout.
 * External API
 */
scha_err_t
scds_simple_probe(scds_handle_t handle, const char *hostname, int port,
	time_t timeout)
{
	scds_priv_handle_t	*h;
	scha_err_t			e1, e2;
	int					s;
	hrtime_t			t1, t2;
	long				t;

	h = check_handle(handle);

	(void) mutex_lock(&h->scds_mutex);

	t1 = gethrtime()/((hrtime_t)1E9);
	e1 = priv_fm_tcp_connect(h, &s, hostname, port, timeout);
	t2 = gethrtime()/((hrtime_t)1E9);
	t = (long)(t2 - t1);
	t = timeout - t;

	/*
	 * If all the time has been used, use a hard-coded
	 * value of 2 seconds to close socket. This makes
	 * sure we have no fd leak.
	 */
	if (t <= 0) {
		t = MIN_DISCONNECT_TIME;
	}
	e2 = priv_fm_tcp_disconnect(h, s, t);

	(void) mutex_unlock(&h->scds_mutex);
	return ((e1 != SCHA_ERR_NOERR) ? e1 : e2);
}


/*
 * Get the hostnames that are used by a resource.
 * Can be called for a scalable or failover resource.
 */
static scha_err_t
priv_get_rs_hostnames(scds_priv_handle_t *h, scds_net_resource_list_t **snrl)
{
	int			rc;
	scds_prop_table_t   *ptable;

	if (snrl == NULL)
		return (SCHA_ERR_INVAL);

	ptable = &(h->ptable);

	/* Initialize pointer in case there are errors */
	*snrl = NULL;

	/*
	 * Check if NETWORK_RESOURCES_USED is set for this resource.
	 * If not, we will use all network resources in this RG.
	 *
	 * For scalable resources, NETWORK_RESOURCES_USED should be set
	 * to a list of SharedAddress resources.  This code handles
	 * scalable resources in addition to failover resources.
	 */
	if (ptable->pt_r->r_sys_props->p_network_resources_used != NULL &&
	    ptable->pt_r->r_sys_props->p_network_resources_used->array_cnt
	    > 0) {
		rc = scds_get_ipaddrs_from_rlist(ptable->pt_zonename,
		    ptable->pt_r->r_sys_props->p_network_resources_used, snrl);
	} else if (ptable->pt_r->r_sys_props->p_scalable == B_TRUE) {
	/*
	 * It is mandatory for a scalable resource to have dependency
	 * on at least one SharedAddress resource.
	 *
	 * Specifically, we should NOT default to looking for resources
	 * in the same RG for it to use.  So we conclude failure here.
	 * The SSM validate method should not allow this case
	 * so consider this an internal error.
	 */
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		    "No SharedAddress resources were specified "
		    "as dependencies of a scalable resource.");
		return (SCHA_ERR_INTERNAL);
	} else {
		rc = scds_get_ipaddrs_from_rlist(ptable->pt_zonename,
		    ptable->pt_rg->rg_rlist, snrl);
	}

	scds_syslog_debug(SCDS_DBG_HIGH,
	    "Returning <%d> from get ip address to use\n", rc);

	return (rc);
}

/*
 * Get the IP addresses that are used by a resource group.
 */
scha_err_t
priv_get_rg_hostnames(char *zname,
    char *rgname, scds_net_resource_list_t **snrl)
{
	scha_err_t		e;
	scha_resourcegroup_t	rg_handle;
	scha_str_array_t	*r_array;
	int			rc;

	/* Initialize pointer in case there are errors */
	*snrl = NULL;

	if (rgname == NULL) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		    "Failed to retrieve host names: Null argument passed "
		    "for resource group name");
		return (SCHA_ERR_INVAL);
	}

retry_rg_get:

	e = scha_resourcegroup_open_zone(zname, rgname, &rg_handle);
	if (e != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * An API operation on the resource group has failed.
		 * @user_action
		 * For the resource group name, check the syslog tag. For more
		 * details, check the syslog messages from other components.
		 * If the error persists, reboot the node.
		 */
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the resource group handle: %s.",
			scds_error_string(e));
		return (e);
	}

	e = scha_resourcegroup_get_zone(zname,
	    rg_handle, SCHA_RESOURCE_LIST,
		&r_array);
	if (e == SCHA_ERR_SEQID) {
		scds_syslog(LOG_INFO,
			"Retrying to retrieve the resource group information.");
			/* close the resource group handle */
		(void) scha_resourcegroup_close_zone(h->ptable.pt_zonename,
		    rg_handle);
		rg_handle = NULL;
		(void) sleep(1);
		goto retry_rg_get;
	} else if (e != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the resource group property %s: %s.",
		    SCHA_RESOURCE_LIST, scds_error_string(e));
		return (e);
	}

	rc = scds_get_ipaddrs_from_rlist(zname, r_array, snrl);

	(void) scha_resourcegroup_close_zone(zname, rg_handle);
	scds_syslog_debug(SCDS_DBG_HIGH,
	    "Returning <%d> from get ip from rg <%s>\n", rc, rgname);

	return (rc);
} 

/*
 * copy_netresources()
 * Copies the specified list of network resources (srclist)
 * into the specified list of netaddr_t array (dstlist).
 * The port and protocol info is copied from port input.
 * Called by priv_get_netaddr_list() when a hostname is not
 * supplied for a given port in the Port_list property of the
 * resource. This function expands the netaddr_list by
 * copying all hostnames contained in netlist into
 * netaddr_list. Memory for dstlist is allocated by caller.
 * Port and protocol info in the dstlist is copied
 * from the port argument.
 *
 * Returns SCHA_ERR_NOMEM if strdup() fails, SCHA_ERR_NOERR
 * in all other cases.
 */
static scha_err_t
copy_netresources(scds_net_resource_list_t *srclist,
	scds_netaddr_t *port, scds_netaddr_t *dstlist)
{
	int		i, j, n;
	scds_netaddr_t	*dst;
	scds_net_resource_t	*src;

	/*
	 * If hostname was specified with the port
	 * We copy it into the dstlist[0]. Other
	 * member in the output list are not
	 * touched.
	 */
	if (port->hostname != NULL) {
		dst = &(dstlist[0]);
		/* Struct copy */
		dst->port_proto = port->port_proto;
		dst->hostname = strdup(port->hostname);
		if (dst->hostname == NULL) {
			scds_syslog(LOG_ERR, "Out of memory.");
			return (SCHA_ERR_NOMEM);
		}
		return (SCHA_ERR_NOERR);
	}
	/*
	 * Else copy the port/proto info into all
	 * output structs and pick up hostnames from
	 * srclist.
	 */
	n = 0;
	for (i = 0; i < srclist->num_netresources; i++) {
		src = &(srclist->netresources[i]);
		for (j = 0; j < src->num_hostnames; j++) {
			dst = &(dstlist[n]);
			dst->port_proto = port->port_proto;
			dst->hostname = strdup(src->hostnames[j]);
			if (dst->hostname == NULL) {
				scds_syslog(LOG_ERR, "Out of memory.");
				return (SCHA_ERR_NOMEM);
			}
			n++;
		}
	}
	return (SCHA_ERR_NOERR);
}

/*
 * Private function to get netaddr_list
 */
static scha_err_t
priv_get_netaddr_list(
	scds_priv_handle_t *h,
	scds_netaddr_list_t **netaddr_list)
{
	scds_net_resource_list_t	*netlist;
	scds_netaddr_list_t		*port_nalist;
	scds_netaddr_t			*port_na, *nalist;
	scds_net_resource_t		*netrs;
	ssize_t					totalhosts;
	int						i;
	size_t					size, n;
	void					*ptr;
	scha_err_t				e;

	/*
	 * Allocate the struct and initialize the number
	 * of netaddrs contained to 0. We will return this
	 * struct unless there are errors.
	 */
	size = 1;
	*netaddr_list = (scds_netaddr_list_t *)
		calloc(size, sizeof (scds_netaddr_list_t));
	if (*netaddr_list == NULL) {
		return (SCHA_ERR_NOMEM);
	}
	(*netaddr_list)->num_netaddrs = 0;
	(*netaddr_list)->netaddrs	= NULL;

	/* See if the port_list property has been read in */
	port_nalist = h->ptable.pt_r->r_sys_props->p_netaddr_list;
	if ((port_nalist == NULL) || (port_nalist->num_netaddrs == 0)) {
		/*
		 * The struct we return would have number of elements
		 * set to zero.
		 */
		return (SCHA_ERR_NOERR);
	}

	/* Get the list of hostnames applicable to the resource */
	e = priv_get_rs_hostnames(h, &netlist);
	if (e != SCHA_ERR_NOERR) {
		free(*netaddr_list);
		*netaddr_list = NULL;
		return (e);
	}
	/* Count the total number of hostnames in netlist */
	totalhosts = 0;
	for (i = 0; i < netlist->num_netresources; i++) {
		netrs = &(netlist->netresources[i]);
		totalhosts += netrs->num_hostnames;
	}

	/* Go over all ports */
	for (i = 0; i < port_nalist->num_netaddrs; i++) {
		/* use "nl" as shorthand for "(*netaddr_list)" */
		scds_netaddr_list_t	*nl = (*netaddr_list);

		port_na = &(port_nalist->netaddrs[i]);
		/*
		 * If hostname was not supplied with the Port_list property
		 * we need to expand the output list to contain
		 * all hostnames returned by get_rs_hostnames().
		 * The number of such entries is pre-computed
		 * to be totalhosts.
		 */
		if (port_na->hostname == NULL) {
			/*
			 * Suppressing Suspicious cast
			 * totalhosts would never be negative.
			 */
			n = (size_t)totalhosts;
		} else {
			n = 1;
		}
		/* n is the number of netaddr entries for current port */

		/*
		 * Suppressing Suspicious cast
		 * num_netaddrs would never be negative.
		 */
		size = (size_t)(((uint_t)(nl->num_netaddrs)) + n) /* CSTYLED */
		    * (sizeof (scds_netaddr_t));	/*lint !e571 */
		ptr = realloc(nl->netaddrs, size);
		if (ptr == NULL) {
			goto out_of_mem;
		}
		nl->netaddrs = (scds_netaddr_t *)ptr;
		/* Set "nalist" to point to newly created elts. */
		/*   at end of netaddrs list */
		/* These elts. will be initialized using copy_netresources() */
		nalist = &((nl->netaddrs)[nl->num_netaddrs]);
		nl->num_netaddrs += (int)n;

		/*
		 * Copy the hostname/port/proto 3-tuple
		 * into the output array.
		 */
		e = copy_netresources(netlist, port_na, nalist);
		if (e != SCHA_ERR_NOERR) {
			goto out_of_mem;
		}
	}

	scds_free_net_list(netlist);
	return (SCHA_ERR_NOERR);

out_of_mem:
	scds_free_netaddr_list(*netaddr_list);
	*netaddr_list = NULL;
	scds_free_net_list(netlist);
	return (SCHA_ERR_NOMEM);
}

/*
 * Free the memory allocated by scds_get_rs_hostnames() or
 * scds_get_rg_hostnames().
 */
void
scds_free_net_list(scds_net_resource_list_t *snrl)
{
	int i, j;
	scds_net_resource_t *snrp;

	if (snrl == NULL)
		return;

	if (snrl->netresources) {
		for (i = 0; i < snrl->num_netresources; i++) {
			snrp = &snrl->netresources[i];
			if (snrp == NULL) {
				continue;
			}
			free(snrp->name);
			if (snrp->hostnames) {
				for (j = 0; j < snrp->num_hostnames; j++) {
					free(snrp->hostnames[j]);
				}
				free(snrp->hostnames);
			}
		}
		free(snrl->netresources);
	}
	free(snrl);
}

/*
 * Get the IP addresses from multiple resources.
 * The network resources can be either SharedAddresses or LogicalHostnames.
 */
scha_err_t
scds_get_ipaddrs_from_rlist(char *zname,
    scha_str_array_t *r_array,
    struct scds_net_resource_list **snrl)
{
	ulong_t 		i;
	scha_err_t		rc;
	scha_err_t		e;
	scha_resource_t 	net_rs_handle = NULL;
	scds_net_resource_list_t 	*snrlp;
	boolean_t		is_net_r = B_FALSE;
	scds_net_resource_t		netrs;

	scds_syslog_debug(SCDS_DBG_HIGH,
	    "Gathering network addresses from the resource list\n");

	if (snrl == NULL)
		return (SCHA_ERR_INVAL);

	snrlp = (scds_net_resource_list_t *)
	    calloc((size_t)1, sizeof (scds_net_resource_list_t));

	if (snrlp == NULL) {
		scds_syslog(LOG_ERR,
		    "Out of memory.");
		return (SCHA_ERR_NOMEM);
	}

	/* Initialize the values in the snrl */
	snrlp->num_netresources = 0;
	snrlp->netresources = NULL;

	*snrl = snrlp;

	if (r_array == NULL || r_array->array_cnt == 0) {
		scds_syslog_debug(SCDS_DBG_LOW,
			"There were no resources found\n");
		return (SCHA_ERR_NOERR);
	}

	rc = SCHA_ERR_NOERR;
	/* Cycle through all of the resources and store the network addrs */
	for (i = 0; i < r_array->array_cnt; i++) {

retry_net_r:

		/* NULL RG name means the API will fill it in */
		e = scha_resource_open_zone(zname, r_array->str_array[i], NULL,
			&net_rs_handle);
		if (e != SCHA_ERR_NOERR) {
			if (e != SCHA_ERR_RSRC) {
				/* error not due to rsrc having been removed */
				/*
				 * SCMSGS
				 * @explanation
				 * An API operation on the resource has
				 * failed.
				 * @user_action
				 * For the resource name, check the syslog
				 * tag. For more details, check the syslog
				 * messages from other components. If the
				 * error persists, reboot the node.
				 */
				scds_syslog(LOG_ERR,
				    "Failed to retrieve the resource handle"
				    ": %s.",
				    scds_error_string(e));
			}
			continue; 	/* what else can we do */
		}

		/* Make sure this looks like a right resource type */
		rc = scds_get_net_resource_type(zname,
		    net_rs_handle, &is_net_r);
		if (rc == SCHA_ERR_SEQID) {
			(void) scha_resource_close_zone(zname, net_rs_handle);
			net_rs_handle = NULL;
			(void) sleep(1);
			goto retry_net_r;
		}
		if (rc != SCHA_ERR_NOERR) {
			/*
			 * SCMSGS
			 * @explanation
			 * While retrieving the IP addresses from the network
			 * resources, the attempt to check whether the
			 * resource is a network resource or not has failed.
			 * @user_action
			 * Internal error or API call failure might be the
			 * reasons. Check the error messages that occurred
			 * just before this message. If there is internal
			 * error, contact your authorized Sun service
			 * provider. For API call failure, check the syslog
			 * messages from other components.
			 */
			scds_syslog(LOG_ERR,
			    "Failed to check whether the resource is "
			    "a network address resource.");
			(void) scha_resource_close_zone(zname, net_rs_handle);
			continue;	/* Assume not a net resource */
		}

		/*
		* This check is safe for scalable resources because the SSM
		* validation won't allow LogicalHostnames to be in the list
		* of NETWORK_RESOURCES_USED.
		*/
		if (is_net_r != B_TRUE) {
			scds_syslog_debug(SCDS_DBG_HIGH,
			    "Resource <%s> is not a network resource\n",
			    r_array->str_array[i]);
			(void) scha_resource_close_zone(zname, net_rs_handle);
			continue;	/* All resources */
		}

		scds_syslog_debug(SCDS_DBG_HIGH,
		    "Resource <%s> is a network resource\n",
		    r_array->str_array[i]);

		/* Add the ip addresses from this resource to our list */
		rc = scds_get_ipaddrs_from_r(zname, net_rs_handle, &netrs);

		if (rc == SCHA_ERR_SEQID) {
			(void) scha_resource_close_zone(zname, net_rs_handle);
			net_rs_handle = NULL;
			(void) sleep(1);
			goto retry_net_r;
		}
		if (rc != SCHA_ERR_NOERR) {
			/*
			 * SCMSGS
			 * @explanation
			 * Retrieving the IP addresses from the network
			 * resources from this resource group has failed.
			 * @user_action
			 * Internal error or API call failure might be the
			 * reasons. Check the error messages that occurred
			 * just before this message. If there is internal
			 * error, contact your authorized Sun service
			 * provider. For API call failure, check the syslog
			 * messages from other components. For the resource
			 * name and resource group name, check the syslog tag.
			 */
			scds_syslog(LOG_ERR,
			    "Failed to get host names from the resource.");
			goto finished;
		}

		/*
		 * The current resource is a network resource, so
		 * allocate space for it and increment the counter
		 * of network resources
		 * Suppressing lint warning e571, suspicious cast.
		 * num_netresources would never be negative.
		 */
		snrlp->num_netresources++;
		snrlp->netresources = (scds_net_resource_t *)
		    realloc((void *)snrlp->netresources, /* CSTYLED */
		    (size_t)snrlp->num_netresources /*lint !e571 */ /*CSTYLED*/
		    * sizeof (scds_net_resource_t)); /*lint !e571 */

		if (snrlp->netresources == NULL) {
			scds_syslog(LOG_ERR,
			    "Out of memory.");
			rc = SCHA_ERR_NOMEM;
			goto finished;
		}

		/* Struct copy */
		snrlp->netresources[snrlp->num_netresources - 1] = netrs;
		snrlp->netresources[snrlp->num_netresources - 1].name =
		    strdup(r_array->str_array[i]);
		if (snrlp->netresources[snrlp->num_netresources - 1].name ==
		    NULL) {
			scds_syslog(LOG_ERR,
			    "Out of memory.");
			rc = SCHA_ERR_NOMEM;
			goto finished;
		}
		(void) scha_resource_close_zone(zname, net_rs_handle);
	} /* for all resources */

	*snrl = snrlp;
	scds_syslog_debug(2, "Debug: rlist Returning %d net res",
		snrlp->num_netresources);

	return (SCHA_ERR_NOERR);

finished:
	if (snrlp)
		scds_free_net_list(snrlp);

	if (net_rs_handle)
		(void) scha_resource_close_zone(zname, net_rs_handle);

	*snrl = NULL;
	return (rc);
}

/*
 * priv_print_net_list()
 * Private function to syslog contents of the specified
 * net_addrlist
 */
void
priv_print_net_list(scds_priv_handle_t *h, int dlevel,
	const scds_net_resource_list_t *snrl)
{
	int 		i, j;

	if ((h == NULL) || (snrl == NULL)) {
		scds_syslog_debug(dlevel,
		    "NULL value passed to network resource print\n");
		return;
	}

	scds_syslog_debug(dlevel,
	    "Net Resource List contains <%d> entries:\n",
	    snrl->num_netresources);

	if (snrl->netresources == NULL) {
		scds_syslog_debug(dlevel,
		    "\tThe Net Resource List is NULL");

		if (snrl->num_netresources != 0) {
			scds_syslog_debug(dlevel,
			    "ERROR: The number of Net Resources is <%d>"
			    "but list is NULL!\n", snrl->num_netresources);
			return;
		}
	}

	for (i = 0; i < snrl->num_netresources; i++) {
		scds_syslog_debug(dlevel,
		    "[%d]\tName <%s>\tNumHostnames <%d>:\n",
		    i,
		    (snrl->netresources[i].name != NULL ?
		    snrl->netresources[i].name : "NULL"),
		    snrl->netresources[i].num_hostnames);

		if (snrl->netresources[i].hostnames == NULL) {
			scds_syslog_debug(dlevel,
			    "\t\t\tHostnames list is NULL\n");
			continue;
		}

		for (j = 0; j < snrl->netresources[i].num_hostnames; j++) {
			scds_syslog_debug(dlevel,
			    "\t\t\t<%s>\n",
			    snrl->netresources[i].hostnames[j]);
		}
	}
}

/*
 * Get the IP addresses from one network resource.
 * The network resource can be a SharedAddress or a LogicalHostname.
 * Assume the memory pointed by snr is pre-allocated
 * The name field in the output is set to NULL.
 */
scha_err_t
scds_get_ipaddrs_from_r(char *zname, scha_resource_t net_rs_handle,
	scds_net_resource_t *snr)
{
	scha_err_t		e;
	scha_extprop_value_t	*extprop;
	scha_str_array_t	*strarray;
	uint_t			i;
	char			*rgname;

	if (snr == NULL) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		    "No memory allocated for the network resource");
		return (SCHA_ERR_INTERNAL);
	}

	/* Initialize the network resource data */
	snr->name = NULL;
	snr->num_hostnames = 0;
	snr->hostnames = NULL;

	if (net_rs_handle == NULL) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		    "Cannot retrieve hostnames: NULL resource handle ");
		return (SCHA_ERR_INTERNAL);
	}

	e = scha_resource_get_zone(zname, net_rs_handle, SCHA_EXTENSION,
		SCDS_EXTPROP_HOSTNAME, &extprop);

	if (e != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the resource property %s: %s.",
		    SCDS_EXTPROP_HOSTNAME, scds_error_string(e));
		return (e);
	}

	/*
	 * presumably, the network address RT implementation
	 * wouldn't allow this.
	 */
	if (extprop == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * NULL value was specified for the extension property of the
		 * resource.
		 * @user_action
		 * For the property name check the syslog message. Any of the
		 * following situations might have occurred. Different user
		 * action is needed for these different scenarios.
		 *
		 * 1) If a new resource is created or updated, check whether
		 * the value of the extension property is empty. If it is,
		 * provide a valid value by using clresource. 2) For all other
		 * cases, treat it as an Internal error. Contact your
		 * authorized Sun service provider.
		 */
		scds_syslog(LOG_ERR,
		    "NULL value returned for the resource property %s.",
		    SCDS_EXTPROP_HOSTNAME);
		return (SCHA_ERR_PROP);
	}

	strarray = extprop->val.val_strarray;
	snr->hostnames =
	    (char **)calloc((size_t)strarray->array_cnt, sizeof (char *));
	if (snr->hostnames == NULL) {
		scds_syslog(LOG_ERR,
		    "Out of memory.");
		return (SCHA_ERR_NOMEM);
	}

	for (i = 0; i < strarray->array_cnt; i++) {
		snr->hostnames[i] = strdup(strarray->str_array[i]);
		if (snr->hostnames[i] == NULL) {
			scds_syslog(LOG_ERR, "Out of memory.");
			return (SCHA_ERR_NOMEM);
		}
		snr->num_hostnames++;
	} /* for all ip addresses for the resource */

	/* Get the current primary of this RG */
	e = scha_resource_get_zone(zname, net_rs_handle, SCHA_GROUP, &rgname);
	if (e != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the resource property %s: %s.",
			SCHA_GROUP, scds_error_string(e));
			return (e);
	}

	return (SCHA_ERR_NOERR);
}

/*
 * scds_get_net_resource_type(): Looks at the RT of the specified resource
 * to make sure it is of logical host type or shared address type.
 */
static scha_err_t
scds_get_net_resource_type(char *zname, scha_resourcetype_t net_rs_handle,
	boolean_t *is_net_type)
{
	scha_err_t		e;
	scha_resourcetype_t	rt_handle;
	char			*rtname;
	boolean_t		bool_val;

	if (net_rs_handle == NULL) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		    "Failed to retrieve the resource type name: "
		    "Network resource handle is null");
		return (SCHA_ERR_INTERNAL);
	}

	*is_net_type = B_FALSE;
	e = scha_resource_get_zone(zname, net_rs_handle, SCHA_TYPE, &rtname);
	if (e != SCHA_ERR_NOERR) {
		if (e != SCHA_ERR_SEQID) {
		/*
		 * in case of seqid error, no error message is required
		 * caller scds_get_ipaddrs_from_rlist() will retry
		 */
			/*
			 * SCMSGS
			 * @explanation
			 * While retrieving the resource information, API
			 * operation has failed to retrieve the resource type
			 * name.
			 * @user_action
			 * This is internal error. Contact your authorized Sun
			 * service provider. For more error description, check
			 * the syslog messages.
			 */
			scds_syslog(LOG_ERR,
			    "Failed to get the resource type name: %s.",
			    scds_error_string(e));
		}
		return (e);
	}

	if (rtname == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * This is an internal error. While attempting to retrieve the
		 * resource information, null value was retrieved for the
		 * resource type name.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		scds_syslog(LOG_ERR,
		    "Resource type name is null.");
		return (SCHA_ERR_INTERNAL);
	}

retry_net_open:

	scds_syslog_debug(2, "Debug: RT open <%s>", rtname);
	e = scha_resourcetype_open_zone(zname, rtname, &rt_handle);
	if (e != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * An API operation on the resource type has failed.
		 * @user_action
		 * For the resource type name, check the syslog tag. For more
		 * details, check the syslog messages from other components.
		 * If the error persists, reboot the node.
		 */
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the resource type handle: "
		    "%s.",
		    scds_error_string(e));
		return (e);
	}

	e = scha_resourcetype_get_zone(
	    zname, rt_handle, SCHA_IS_LOGICAL_HOSTNAME,
	    &bool_val);
	if (e == SCHA_ERR_SEQID) {
		(void) scha_resource_close_zone(zname, rt_handle);
		rt_handle = NULL;
		(void) sleep(1);
		goto retry_net_open;
	}
	if (e != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the resource type property %s: %s.",
		    SCHA_IS_LOGICAL_HOSTNAME, scds_error_string(e));
		return (e);
	}

	scds_syslog_debug(2, "Debug: net RS %s",
		(bool_val == B_TRUE) ? "TRUE" : "FALSE");
	if (bool_val == B_TRUE) {
		(void) scha_resourcetype_close_zone(zname, rt_handle);
		*is_net_type = B_TRUE;
		return (SCHA_ERR_NOERR);
	}

	e = scha_resourcetype_get_zone(zname, rt_handle, SCHA_IS_SHARED_ADDRESS,
	    &bool_val);
	if (e == SCHA_ERR_SEQID) {
		(void) scha_resource_close_zone(zname, rt_handle);
		rt_handle = NULL;
		(void) sleep(1);
		goto retry_net_open;
	}
	if (e != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the resource type property %s: %s.",
		    SCHA_IS_SHARED_ADDRESS, scds_error_string(e));
		(void) scha_resource_close_zone(zname, rt_handle);
		return (e);
	}

	(void) scha_resourcetype_close_zone(zname, rt_handle);

	if (bool_val == B_TRUE) {
		*is_net_type = B_TRUE;
		return (SCHA_ERR_NOERR);
	}

	return (SCHA_ERR_NOERR);
}


/*
 * Convert a nodename to a nodeid.
 */
static scha_err_t
scds_get_nodeid(const char *zname, char *nodename, int *nodeid)
{
	scha_err_t		e;
	scha_cluster_t		clust_handle;
	int			nid;

	if (nodeid == NULL) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		    "Cannot convert the nodename to nodeid; "
		    "No space allocated for the node id");
		return (SCHA_ERR_INTERNAL);
	}

	*nodeid = SCDS_INVALID_NODEID;

	if (nodename == NULL) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		    "Cannot convert nodename to nodeid; "
		    "Nodename is null");
		return (SCHA_ERR_INTERNAL);
	}

retry_again:

	e = scha_cluster_open_zone(zname, &clust_handle);
	if (e != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the cluster handle : %s.",
		    scds_error_string(e));
		return (e);
	}

	e = scha_cluster_get_zone(zname, clust_handle, SCHA_NODEID_NODENAME,
		nodename, &nid);
	if (e == SCHA_ERR_SEQID) {
		/*
		 * SCMSGS
		 * @explanation
		 * An update to cluster configuration occured while cluster
		 * properties were being retrieved
		 * @user_action
		 * Ignore the message.
		 */
		scds_syslog(LOG_INFO,
		    "Retrying to retrieve the cluster information.");
		    /* close the cluster handle */
		    (void) scha_cluster_close(clust_handle);
		    clust_handle = NULL;
		    (void) sleep(1);
		    goto retry_again;
	} else if (e != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * API operation has failed in retrieving the cluster
		 * property.
		 * @user_action
		 * For property name, check the syslog message. For more
		 * details about API call failure, check the syslog messages
		 * from other components.
		 */
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the property %s for %s: %s.",
		    SCHA_NODEID_NODENAME,
		    nodename, scds_error_string(e));
		(void) scha_cluster_close_zone(zname, clust_handle);
		return (e);
	}

	*nodeid = nid;
	(void) scha_cluster_close_zone(zname, clust_handle);

	return (SCHA_ERR_NOERR);
}

/*
 * Given a Failover RG name, returns the
 * current primary node of this RG. exit()s with 1 if there is a
 * serious problem (API call failures), returns -1 if
 * the RG currently has no primaries (does appropiate syslog(WARN)
 * in such cases as well).
 */

scha_err_t
scds_get_rg_primary_node(char *zname, char *rgname, int *nodeid)
{
	scha_err_t		e;
	scha_resourcegroup_t	rg_handle;
	scha_str_array_t	*nodelist;
	uint_t		i;
	int			n, nid, rc;
	scha_rgstate_t		state;

	if (nodeid == NULL) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		    "Failed to retrieve the primary node;"
		    "No space allocated for the node id");
	}

	*nodeid = SCDS_INVALID_NODEID;

	if (rgname == NULL) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		    "Can't get primary node; Resource group name is NULL");
		return (SCHA_ERR_INTERNAL);
	}

retry_rg_primary:
	e = scha_resourcegroup_open_zone(zname, rgname, &rg_handle);
	if (e != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to open the resource group handle"
		    ": %s.", scds_error_string(e));
		return (e);
	}

	/* Maximum primaries and desired primaries must be 1 */
	e = scha_resourcegroup_get_zone(
	    zname, rg_handle, SCHA_MAXIMUM_PRIMARIES,
		&n);
	if (e == SCHA_ERR_SEQID) {
		scds_syslog(LOG_INFO,
			"Retrying to retrieve the resource group information.");
			/* close the resource group handle */
		(void) scha_resourcegroup_close_zone(zname, rg_handle);
		rg_handle = NULL;
		(void) sleep(1);
		goto retry_rg_primary;
	} else if (e != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * Unable to retrieve the resource group property.
		 * @user_action
		 * For the property name and the reason for failure, check the
		 * syslog message. For more details about the api failure,
		 * check the syslog messages from the RGM .
		 */
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the resource group property %s: %s",
		    SCHA_MAXIMUM_PRIMARIES, scds_error_string(e));
		(void) scha_resourcegroup_close_zone(zname, rg_handle);
		return (e);
	}
	if (n > 1) {
		/*
		 * SCMSGS
		 * @explanation
		 * Invalid value has set for Maximum Primaries. The value
		 * should be 1.
		 * @user_action
		 * Reset this value by using clresourcegroup.
		 */
		scds_syslog(LOG_ERR,
		    "Maximum Primaries is "
		    "%d. It should be 1.",
		    n);
		return (SCHA_ERR_INVAL);
	}

	e = scha_resourcegroup_get_zone(
	    zname, rg_handle, SCHA_DESIRED_PRIMARIES,
		&n);
	if (e == SCHA_ERR_SEQID) {
		scds_syslog(LOG_INFO,
			"Retrying to retrieve the resource group information.");
			/* close the resource group handle */
		(void) scha_resourcegroup_close_zone(zname, rg_handle);
		rg_handle = NULL;
		(void) sleep(1);
		goto retry_rg_primary;
	} else if (e != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the resource group property %s: %s.",
		    SCHA_DESIRED_PRIMARIES, scds_error_string(e));
		(void) scha_resourcegroup_close_zone(zname, rg_handle);
		return (e);
	}
	if (n > 1) {
		/*
		 * SCMSGS
		 * @explanation
		 * Invalid value for Desired Primaries property.
		 * @user_action
		 * Invalid value is set for Desired Primaries property. The
		 * value should be 1. Reset the property value by using
		 * clresourcegroup.
		 */
		scds_syslog(LOG_ERR,
		    "Desired Primaries is "
		    "%d. It should be 1.",
		    rgname, n);
		return (SCHA_ERR_INVAL);
	}

	e = scha_resourcegroup_get_zone(zname, rg_handle, SCHA_NODELIST,
		&nodelist);
	if (e == SCHA_ERR_SEQID) {
		scds_syslog(LOG_INFO,
			"Retrying to retrieve the resource group information.");
			/* close the resource group handle */
		(void) scha_resourcegroup_close_zone(zname, rg_handle);
		rg_handle = NULL;
		(void) sleep(1);
		goto retry_rg_primary;
	} else if (e != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the resource group property %s: %s.",
		    SCHA_NODELIST, scds_error_string(e));
		(void) scha_resourcegroup_close_zone(zname, rg_handle);
		return (e);
	}

	if (nodelist == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * Empty value was specified for the nodelist property of the
		 * resource group.
		 * @user_action
		 * Any of the following situations might have occured.
		 * Different user action is required for these different
		 * scenarios. 1) If a resourcegroup is created or updated,
		 * check the value of the nodelist property. If it is empty or
		 * not valid, then provide valid value by using
		 * clresourcegroup command. 2) For all other cases, treat it
		 * as an Internal error. Contact your authorized Sun service
		 * provider.
		 */
		scds_syslog(LOG_ERR,
		    "Resource group nodelist is empty.");
		return (SCHA_ERR_INVAL);
	}

	nid = SCDS_INVALID_NODEID;
	for (i = 0; i < nodelist->array_cnt; i++) {
		e = scha_resourcegroup_get_zone(zname,
		    rg_handle, SCHA_RG_STATE_NODE,
		    nodelist->str_array[i], &state);
		if (e == SCHA_ERR_SEQID) {
			scds_syslog(LOG_INFO,
			    "Retrying to retrieve the resource "
			    "group information.");
			/* close the resource group handle */
			(void) scha_resourcegroup_close_zone(zname, rg_handle);
			rg_handle = NULL;
			(void) sleep(1);
			goto retry_rg_primary;
		} else if (e != SCHA_ERR_NOERR) {
			/*
			 * BugID 4236927: It is ok if the query fails
			 * because the node is not in the cluster. In
			 * this case, we will continue to look for the
			 * master on a different node.
			 */
			if (e == SCHA_ERR_NODE) {
				scds_syslog_debug(SCDS_DBG_HIGH,
				    "node <%s> is not in the cluster\n",
				    nodelist->str_array[i]);
				continue;
			}

			scds_syslog(LOG_ERR,
			    "Failed to retrieve the "
			    "resource group property %s: %s.",
			    SCHA_RG_STATE_NODE,
			    scds_error_string(e));
			(void) scha_resourcegroup_close_zone(zname, rg_handle);
			return (e);
		}
		if (state == SCHA_RGSTATE_ONLINE) {
			if (nid != SCDS_INVALID_NODEID) {
				/* We are online on two different nodes! */
				/*
				 * SCMSGS
				 * @explanation
				 * An internal error has occurred. Resource
				 * group should be online on only one node.
				 * @user_action
				 * Save a copy of the /var/adm/messages files
				 * on all nodes. Contact your authorized Sun
				 * service provider for assistance in
				 * diagnosing the problem.
				 */
				scds_syslog(LOG_ERR,
				    "Resource group is online on more "
				    "than one node.");
			}

			rc = scds_get_nodeid(zname,
			    nodelist->str_array[i], &nid);
			if (rc != SCHA_ERR_NOERR ||
				nid == SCDS_INVALID_NODEID) {
				/*
				 * SCMSGS
				 * @explanation
				 * Data service is failed to retrieve the host
				 * information.
				 * @user_action
				 * If the logical host and shared address
				 * entries are specified in the
				 * /etc/inet/hosts file, check these entries
				 * are correct. If this is not the reason then
				 * check the health of the name server. For
				 * more error information, check the syslog
				 * messages.
				 */
				scds_syslog(LOG_ERR,
				    "Failed to retrieve nodeid.");
			} else {
				scds_syslog_debug(SCDS_DBG_HIGH,
				    "RG <%s> is online on nodeid <%d>\n",
				    rgname, nid);
			}
		}
	} /* for all nodes in the nodelist */

	if (nid == SCDS_INVALID_NODEID) {
		scds_syslog_debug(1,
		    "RG <%s> is not online on any of its primaries\n",
		    rgname);
	}

	*nodeid = nid;
	(void) scha_resourcegroup_close_zone(zname, rg_handle);

	return (SCHA_ERR_NOERR);
}


/*
 * priv_fm_tcp_connect(): Connect to a hostname/port under
 * a given timeout. The implementation here is a bit tricky because
 * we don't have a direct control over the connect() system
 * call. We put socket in non-blocking mode, then
 * use poll() with a timeout to make sure it has connected.
 * After completeing connection, we put socket back
 * into usual blocking mode.
 */

scha_err_t
priv_fm_tcp_connect(scds_priv_handle_t *h, int *sock,
	const char *hostname, int port, time_t timeout)
{
	struct sockaddr_in6 target;
	struct hostent *hp;
	struct pollfd	sockfd;
	int soflags = 0, rc = 0, tlen = 0;
	char errmsg[SCDS_ARRAY_SIZE];
	int lookup_err = 0;
	char **taddr;

	if (h == NULL) {
		return (SCHA_ERR_INVAL);
	}

	/*
	 * We need to figure out whether we should connect
	 * over IPv6 or regular IPv4. To do this, we look up the
	 * hostname and do the following:
	 * a) If the hostname maps to IPv4 address(es) only, connect over v4
	 * b) If the hostname maps to IPv6 address(es) only, connect over v6
	 * c) If the hostname maps to both v4 and v6, connect to v4 only
	 *
	 * In short: if the user wants us to probe over v6, either the hostname
	 * must map to a v6 address only or Portlist=<v6addr/port/tcp6> has to
	 * be used. Portlist=<hostname/port/tcp6> will *not* enable connects
	 * over v6 if hostname maps to both v4 and v6 addresses
	 */
	hp = getipnodebyname(hostname, AF_INET6, AI_ALL | AI_V4MAPPED |
	    AI_ADDRCONFIG, &lookup_err);
	if (hp == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		scds_syslog(LOG_ERR,
		    "Failed to retrieve host information for %s: %s.",
			hostname, hstrerror(lookup_err));
		return (SCHA_ERR_INVAL);
	}

	if ((uint_t)hp->h_length != sizeof (target.sin6_addr)) {
		/* huh? we'd asked for IPv6 sized addresses */
		(void) snprintf(errmsg, sizeof (errmsg), "Address size "
		    "mismatch for hostname %s. Expected size: %d ; Actual "
		    "size: %d", hostname, sizeof (target.sin6_addr),
		    hp->h_length);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", errmsg);
		freehostent(hp);
		return (SCHA_ERR_INTERNAL);
	}

	/* setup the socket structure for the target to which we'll connect */
	bzero(&target, sizeof (target));
	target.sin6_family = AF_INET6;
	target.sin6_port = htons((ushort_t)port);

	/* first address from the lookup result is our default target */
	bcopy(hp->h_addr, &target.sin6_addr, (size_t)(unsigned)hp->h_length);

	/*
	 * If any IPv4 address is found in the list, use it as the target.
	 * If none is found, then target.sin6_addr already has the first
	 * IPv6 address for the host and should be used.
	 */
	scds_syslog_debug(2, "Searching for an IPv4 mapping for %s",
	    hostname);
	for (taddr = hp->h_addr_list; *taddr != NULL; taddr++) {
		if (IN6_IS_ADDR_V4MAPPED((struct in6_addr *)*taddr)) {
			bcopy(*taddr, &target.sin6_addr,
			    (size_t)(unsigned)hp->h_length);
			scds_syslog_debug(2, "Found an IPv4 mapping for %s",
			    hostname);
			break;
		}
	}

	freehostent(hp); /* dont need it any more */

	*sock = socket(AF_INET6, SOCK_STREAM, 0);
	if (*sock < 0) {
		/*
		 * errno.h is missing void arg, so suppress lint error.
		 * [should be errno(void) rather than errno()]
		 */
		scds_syslog(LOG_ERR, "Failed to create socket: %s.", /*CSTYLED*/
		    strerror(errno));			/*lint !e746 */
		return (SCHA_ERR_INTERNAL);
	}
	if ((soflags = fcntl(*sock, F_GETFL, 0)) < 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * Failed to get status flags for the socket used in
		 * communicating with the application.
		 * @user_action
		 * This is an internal error, no user action is required. Also
		 * contact your authorized Sun service provider.
		 */
		scds_syslog(LOG_ERR,
			"Unable to get socket flags: %s.",
			strerror(errno));
		(void) close(*sock);
		*sock = -1;
		return (SCHA_ERR_INTERNAL);
	}
	soflags |= O_NDELAY | O_NONBLOCK;
	if (fcntl(*sock, F_SETFL, soflags) != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * Failed to set the non-blocking flag for the socket used in
		 * communicating with the application.
		 * @user_action
		 * This is an internal error, no user action is required. Also
		 * contact your authorized Sun service provider.
		 */
		scds_syslog(LOG_ERR,
			"Unable to set socket flags: %s.",
			strerror(errno));
		(void) close(*sock);
		*sock = -1;
		return (SCHA_ERR_INTERNAL);
	}

	scds_syslog_debug(2, "Debug: Starting connect()");
	/*
	 * sizeof (target) is never going to be
	 * larger then what an integer can hold.
	 * Hence suppressing lint info 747.
	 * Significant prototype coercion
	 */
	if (connect(*sock, (struct sockaddr *)&target, /* CSTYLED */
	    (uint_t)sizeof (target)) < 0) {	/*lint !e747 */

		/* EINPROGRESS would be set if nothing bad happened */
		if (errno != EINPROGRESS) {
			/*
			 * SCMSGS
			 * @explanation
			 * An error occurred while fault monitor attempted to
			 * probe the health of the data service.
			 * @user_action
			 * Wait for the fault monitor to correct this by doing
			 * restart or failover. For more error description,
			 * look at the syslog messages.
			 */
			scds_syslog(LOG_ERR, "Failed to connect to host %s "
			    "and port %d: %s.",
			    hostname, port, strerror(errno));
			(void) close(*sock);
			*sock = -1;

			/*
			 * Think of SCHA_ERR_STATE here as "not in a connected
			 * state"
			 */
			return (SCHA_ERR_STATE);
		}
	}
	/* Now we poll for connection to complete */
	sockfd.fd = *sock;
	sockfd.events = POLLOUT | POLLPRI;
	sockfd.revents = 0;
	/* Convert timeout to milliseconds */
	timeout *= 1000;

	scds_syslog_debug(2, "Starting poll");
	/*
	 * poll returns 0 for timeout and -ve for errors
	 * Hence we check for return < 1.
	 * However, in case of timeout, it doesn't set
	 * errno. We set errno to ETIMEDOUT here.
	 */
	errno = ETIMEDOUT;
	rc = poll(&sockfd, (ulong_t)1, (int)timeout);
	scds_syslog_debug(2, "Debug: finished poll()");
	if (rc < 1) {
		scds_syslog(LOG_ERR, "Failed to connect to host %s and port "
		    "%d: %s.", hostname, port, strerror(errno));
		(void) close(*sock);
		*sock = -1;
		return (rc == 0 ? SCHA_ERR_TIMEOUT : SCHA_ERR_INTERNAL);
	}

	/*
	 * poll didnt timeout, but did the socket actually connect?
	 * eg. There might be a "connection refused" after the poll
	 * has been started. In cases like that, activity on the
	 * socket fd within the poll timeout can not be taken as
	 * a sign of successful connection.
	 *
	 * Note that we return error only for ENOTCONN, when we are
	 * 100% sure that the socket is not connected. For other
	 * getpeername errors, nothing is done.
	 */
	tlen = sizeof (target);
	if ((getpeername(*sock, (struct sockaddr *)&target, &tlen) < 0) &&
	    (errno == ENOTCONN)) {
		scds_syslog(LOG_ERR, "Failed to connect to host %s and port "
		    "%d: %s.", hostname, port, strerror(errno));
		(void) close(*sock);
		*sock = -1;
		return (SCHA_ERR_STATE); /* not in connected state */
	}

	/* Now we clear the no-delay flag */
	if ((soflags = fcntl(*sock, F_GETFL, 0)) < 0) {
		scds_syslog(LOG_ERR,
			"Unable to get socket flags: %s.",
			strerror(errno));
		(void) close(*sock);
		*sock = -1;
		return (SCHA_ERR_INTERNAL);
	}
	soflags &= ~O_NDELAY;
	soflags &= ~O_NONBLOCK;
	if (fcntl(*sock, F_SETFL, soflags) != 0) {
		scds_syslog(LOG_ERR,
			"Unable to set socket flags: %s.",
			strerror(errno));
		(void) close(*sock);
		*sock = -1;
		return (SCHA_ERR_INTERNAL);
	}
	return (SCHA_ERR_NOERR);
}

/*
 * scds_read(): Read stuff from a socket under a timeout. It is
 * upto the caller to have some mechanism to decide how much to
 * read() and when to stop reading. This functions would block
 * the caller (for the given timeout anyway..).
 */

scha_err_t
priv_fm_tcp_read(scds_priv_handle_t *h, int sock,
	char *buf, size_t *size, time_t timeout)
{
	scha_err_t n = SCHA_ERR_NOERR;
	ssize_t s;
	struct pollfd	sockfd;

	if ((h == NULL) || (sock < 0)) {
		return (SCHA_ERR_INTERNAL);
	}
	sockfd.fd = sock;
	sockfd.events = POLLIN | POLLPRI;
	sockfd.revents = 0;
	/* Convert timeout to milliseconds */
	timeout *= 1000;

	/*
	 * poll returns 0 for timeout and -ve for errors
	 * However, in case of timeout, it doesn't set
	 * errno. We set errno to ETIMEDOUT here.
	 */
	errno = ETIMEDOUT;
	if (poll(&sockfd, (ulong_t)1, (int)timeout) < 1) {
		/*
		 * SCMSGS
		 * @explanation
		 * Failed to read the data from the socket. The reason might
		 * be expiration of timeout, hung application or heavy load.
		 * @user_action
		 * Check if the application is hung. If this is the case,
		 * restart the appilcation.
		 */
		scds_syslog(LOG_ERR, "Failed to read data: %s.",
			strerror(errno));
		(void) close(sock);
		*size = (size_t)0;
		return (SCHA_ERR_TIMEOUT);
	}

	s = recv(sock, buf, *size, 0);
	if (s == -1) {
		s = 0;
		n = SCHA_ERR_TIMEOUT;
	}

	*size = (size_t)s;
	return (n);
}

/*
 * scds_write(): write() stuff into a socket... write()s
 * rarely timeout, if ever. A fault monitor should not
 * be making so much I/O to the application that a send()
 * has to wait (due to flow control maybe).
 * We use the poll() syscall to make sure the socket is
 * ready to be written.
 */

scha_err_t
priv_fm_tcp_write(scds_priv_handle_t *h, int sock,
	char *buf, size_t *size, time_t timeout)
{
	scha_err_t n = SCHA_ERR_NOERR;
	ssize_t s;
	struct pollfd	sockfd;

	if ((h == NULL) || (sock < 0)) {
		return (SCHA_ERR_INTERNAL);
	}
	sockfd.fd = sock;
	sockfd.events = POLLOUT | POLLPRI;
	sockfd.revents = 0;
	/* Convert timeout to milliseconds */
	timeout *= 1000;

	/*
	 * poll returns 0 for timeout and -ve for errors
	 * However, in case of timeout, it doesn't set
	 * errno. We set errno to ETIMEDOUT here.
	 */
	errno = ETIMEDOUT;
	if (poll(&sockfd, (ulong_t)1, (int)timeout) < 1) {
		/*
		 * SCMSGS
		 * @explanation
		 * Failed to write the data to the socket. The reason might be
		 * expiration of timeout, hung application or heavy load.
		 * @user_action
		 * Check if the application is hung. If this is the case,
		 * restart the appilcation.
		 */
		scds_syslog(LOG_ERR, "Unable to write data: %s.",
			strerror(errno));
		(void) close(sock);
		return (SCHA_ERR_TIMEOUT);
	}

	s = send(sock, buf, *size, 0);

	if (s == -1)
		n = SCHA_ERR_TIMEOUT;

	*size = (size_t)s;
	return (n);
}

/*
 * scds_fm_tcp_disconnect(): Disconnect a TCP connection.
 * Calls close() on the socket.
 * XXX Enforce timeout
 */

scha_err_t
priv_fm_tcp_disconnect(scds_priv_handle_t *h, int sock, time_t timeout)
{
	scha_err_t n = SCHA_ERR_NOERR;

	if ((h == NULL) || (sock < 0) || (timeout < 0)) {
		return (SCHA_ERR_INVAL);
	}
	(void) close(sock);

	return (n);
}

/*
 * Connects to upto two IP addresses (for a host) at once - one address is
 * IPv4, other IPv6. Connections take place in parallel, and the timeout
 * specified applies to both the connections.
 */
scha_err_t
scds_fm_net_connect(scds_handle_t handle, scds_socket_t *socklist, int count,
    scds_netaddr_t addr, time_t timeout)
{
	scds_priv_handle_t	*h;
	scha_err_t			e;

	h = check_handle(handle);

	/* very basic checks */
	if ((socklist == NULL) || (count < SCDS_MAX_IPADDR_TYPES) ||
	    (addr.hostname == NULL) ||
	    ((addr.port_proto.proto != SCDS_IPPROTO_TCP) &&
	    (addr.port_proto.proto != SCDS_IPPROTO_TCP6))) {
		/*
		 * SCMSGS
		 * @explanation
		 * The function was called with invalid parameters.
		 * @user_action
		 * Save all resource information (clresource show -v) and
		 * contact the vendor of the resource.
		 */
		scds_syslog(LOG_ERR, "%s called with invalid parameters",
		    "scds_fm_net_connect");
		return (SCHA_ERR_INVAL);
	}

	(void) mutex_lock(&h->scds_mutex);

	e = priv_fm_net_connect(socklist, addr, timeout);

	(void) mutex_unlock(&h->scds_mutex);
	return (e);
}

/*
 * Helper for scds_fm_net_connect(). Infact all the real work is
 * done in this function
 *
 * Implements a parallel socket-pair connector (asynchronous with timeout).
 */
scha_err_t
priv_fm_net_connect(scds_socket_t *sl, scds_netaddr_t addr, time_t timeout)
{
	struct hostent *hp;
	int lookup_err = 0, i, v4found = 0, v6found = 0, pcount = 0, rc;
	int soflags, ptime;
	char errmsg[SCDS_ARRAY_SIZE], **taddr;
	struct sockaddr_in6 target[SCDS_MAX_IPADDR_TYPES];
	struct pollfd sockfd[SCDS_MAX_IPADDR_TYPES];
	hrtime_t end;

	/* this will set status = SCDS_FMSOCK_NA for both sockets */
	bzero(sl, SCDS_MAX_IPADDR_TYPES * sizeof (scds_socket_t));

	scds_syslog_debug(2, "Looking up hostname [%s]", addr.hostname);
	hp = getipnodebyname(addr.hostname, AF_INET6, AI_ALL | AI_V4MAPPED |
	    AI_ADDRCONFIG, &lookup_err);
	if (hp == NULL) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve host information for %s: %s.",
		    addr.hostname, hstrerror(lookup_err));
		return (SCHA_ERR_INVAL);
	}

	if ((uint_t)hp->h_length != sizeof (in6_addr_t)) {
		(void) snprintf(errmsg, sizeof (errmsg), "Address size "
		    "mismatch for hostname %s. Expected size: %d ; Actual "
		    "size: %d", addr.hostname, sizeof (in6_addr_t),
		    hp->h_length);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", errmsg);
		freehostent(hp);
		return (SCHA_ERR_INTERNAL);
	}

	bzero(target, sizeof (target));
	for (i = 0; i < SCDS_MAX_IPADDR_TYPES; i++) {
		target[i].sin6_family = AF_INET6;
		target[i].sin6_port = htons((ushort_t)addr.port_proto.port);
	}

	for (taddr = hp->h_addr_list; *taddr != NULL; taddr++) {
		if (!v4found) {
			/* we are still looking for the first v4 address */
			if (IN6_IS_ADDR_V4MAPPED((struct in6_addr *)*taddr)) {
				bcopy(*taddr, &(target[0].sin6_addr),
				    (size_t)(unsigned)hp->h_length);
				sl[0].status = SCDS_FMSOCK_OK;
				sl[0].err = SCHA_ERR_NOERR;
				v4found = 1; /* stop looking for v4 addresses */
				scds_syslog_debug(2, "Found IPv4 address");
			}
		}
		if (!v6found && addr.port_proto.proto == SCDS_IPPROTO_TCP6) {
			/* we are still looking for the first v6 address */
			if (!IN6_IS_ADDR_V4MAPPED((struct in6_addr *)*taddr)) {
				bcopy(*taddr, &(target[1].sin6_addr),
				    (size_t)(unsigned)hp->h_length);
				sl[1].status = SCDS_FMSOCK_OK;
				sl[1].err = SCHA_ERR_NOERR;
				v6found = 1; /* stop looking for v6 addresses */
				scds_syslog_debug(2, "Found IPv6 address");
			}
		}

		/*
		 * no need to loop if any of these two holds:
		 * a) we already have one v4 and one v6 address
		 * b) user did not specify tcp6 and we have one v4 already
		 */
		if ((v4found && v6found) ||
		    (v4found && (addr.port_proto.proto != SCDS_IPPROTO_TCP6)))
			break;

	}

	freehostent(hp); /* hp is not used after this */

	if (!v4found && !v6found) {
		/*
		 * IPv6 only setup with tcp6 flag not being supplied.
		 * We require that IPv6 be enabled _explicitly_. Even if
		 * the hostname maps to IPv6 addresses only, we are not
		 * willing to be extra clever and ignore the proto part of
		 * Portlist.
		 *
		 * Hopefully, the error message explains the situation precisely
		 */
		/*
		 * SCMSGS
		 * @explanation
		 * The hostname maps to IPv6 address(es) only and tcp6 flag
		 * was not specified in PortList property of the resource. The
		 * IPv6 address for the hostname is being ignored because of
		 * that.
		 * @user_action
		 * Specify tcp6 in PortList property of the resource.
		 */
		scds_syslog(LOG_ERR, "No usable address for hostname %s."
		    " IPv6 addresses were ignored because protocol tcp was "
		    "specified in PortList. Enable IPv6 lookups by using "
		    "tcp6 instead of tcp in PortList.", addr.hostname);
		return (SCHA_ERR_INVAL);
	}

	/*
	 * Create the necessary sockets, set them up for nonblocking mode
	 * and initiate connections
	 */
	for (i = 0; i < SCDS_MAX_IPADDR_TYPES; i++) {
		/*
		 * For simplicity, we call poll (later) with a fixed second
		 * parameter (count). Its important that we default all fds
		 * to -1 so that the ones that are not given a valid value
		 * later are ignored by poll
		 */
		sockfd[i].fd = -1;

		/* ignore the ones that are NA or failed already */
		if (sl[i].status != SCDS_FMSOCK_OK)
			continue;

		/* create an AF_INET6 socket */
		if ((sl[i].fd = socket(AF_INET6, SOCK_STREAM, 0)) < 0) {
			scds_syslog(LOG_ERR, "Failed to create socket: %s.",
			    strerror(errno));
			sl[i].status = SCDS_FMSOCK_ERR;
			sl[i].err = SCHA_ERR_INTERNAL;
			continue;
		}

		/* Fetch the socket options */
		if ((soflags = fcntl(sl[i].fd, F_GETFL, 0)) < 0) {
			scds_syslog(LOG_ERR, "Unable to get socket flags: %s.",
			    strerror(errno));
			(void) close(sl[i].fd);
			sl[i].status = SCDS_FMSOCK_ERR;
			sl[i].err = SCHA_ERR_INTERNAL;
			continue;
		}

		/* We want a nonblocking socket */
		soflags |= O_NDELAY | O_NONBLOCK;

		/* Set the socket options */
		if (fcntl(sl[i].fd, F_SETFL, soflags) != 0) {
			scds_syslog(LOG_ERR, "Unable to set socket flags: %s.",
			    strerror(errno));
			(void) close(sl[i].fd);
			sl[i].status = SCDS_FMSOCK_ERR;
			sl[i].err = SCHA_ERR_INTERNAL;
			continue;
		}

		/*
		 * Initiate connection on this socket. Note that the first
		 * connection request gets a tiny bit extra time because
		 * the loop will run second time (for the second ip) and
		 * only then do we start polling. But this extra time is
		 * insignificant compared to the timeout (or so we hope!)
		 */
		if (connect(sl[i].fd, (struct sockaddr *)(target + i),
		    (uint_t)sizeof (struct sockaddr_in6)) < 0) {

			/* EINPROGRESS would be set if nothing bad happened */
			if (errno != EINPROGRESS) {
				scds_syslog(LOG_ERR, "Failed to connect to host"
				    " %s and port %d: %s.", addr.hostname,
				    addr.port_proto.port, strerror(errno));
				(void) close(sl[i].fd);
				sl[i].status = SCDS_FMSOCK_ERR;
				sl[i].err = SCHA_ERR_STATE;
				continue;
			}
		}

		/* everything worked so far, we'll poll this socket later */
		sockfd[i].fd = sl[i].fd;
		sockfd[i].events = POLLOUT;
		sockfd[i].revents = 0;
		pcount++;
	}

	if (pcount == 0) {
		/*
		 * Complete failure, not even one socket to poll. Error message
		 * has been logged already and saved in each individual
		 * scds_fmsock_status_t. All we need to do is just return.
		 * SCHA_ERR_INTERNAL is probably a reasonable aggregate
		 * value to return.
		 */
		return (SCHA_ERR_INTERNAL);
	}

	/*
	 * When gethrtime() reaches end, give up and return timeout.
	 * Note that gethrtime() deals in nanoseconds, timeout is in
	 * seconds and poll expects milliseconds.
	 */
	end = gethrtime() + (hrtime_t)(timeout * 1e9);
	while (pcount) {
		ptime = (int)((end - gethrtime()) / 1e6); /* in milliseconds */

		/* the last thing we want to do is give a -ve timeout to poll */
		ptime = (ptime < 0) ? 0 : ptime;

		scds_syslog_debug(2, "Starting poll for %d socket(s)", pcount);
		rc = poll(sockfd, (ulong_t)SCDS_MAX_IPADDR_TYPES, ptime);
		if (rc == -1) {
			/* poll itself failed */
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			    strerror(errno));
		}

		for (i = 0; i < SCDS_MAX_IPADDR_TYPES; i++) {
			if (sockfd[i].fd == -1)
				continue;

			switch (rc) {
			case -1:
				/*
				 * poll itself failed. error message has
				 * already been logged.
				 */
				(void) close(sl[i].fd);
				sl[i].status = SCDS_FMSOCK_ERR;
				sl[i].err = SCHA_ERR_INTERNAL;
				pcount--;
				break;
			case 0: /* poll timed out */
				scds_syslog(LOG_ERR, "Failed to connect to "
				    "host %s and port %d: %s.", addr.hostname,
				    addr.port_proto.port, strerror(ETIMEDOUT));
				(void) close(sockfd[i].fd);
				sl[i].status = SCDS_FMSOCK_ERR;
				sl[i].err = SCHA_ERR_TIMEOUT;
				pcount--;
				break;
			default:
				if (sockfd[i].revents & POLLOUT) {
					/*
					 * Great, this one connected. If poll
					 * has to be called one more time (the
					 * other socket is still waiting), we
					 * should set this fd to -1 so that it
					 * is ignored in subsequent calls.
					 */
					sockfd[i].fd = -1;
					scds_syslog_debug(2,
					    "Socket connected");
					pcount--;
				} else if (sockfd[i].revents &
				    (POLLERR | POLLHUP | POLLNVAL)) {
					/* error for this fd */
					scds_syslog(LOG_ERR,
					    "Failed to connect to host %s "
					    "and port %d: %s.", addr.hostname,
					    addr.port_proto.port,
					    "error, hangup or invalid fd");
					(void) close(sl[i].fd);
					sl[i].status = SCDS_FMSOCK_ERR;
					sl[i].err = SCHA_ERR_STATE;
					sockfd[i].fd = -1;
					pcount--;
				} else if (sockfd[i].revents) {
					/*
					 * Some event other than a connect or
					 * an error. This socket should be
					 * polled again, and so pcount is
					 * _not_ decremented here.
					 */
					scds_syslog_debug(2, "Event ignored");
				}
			}
		}
	}

	/*
	 * Set the sockets back to blocking mode, also calculate an
	 * aggregate return value.
	 */
	rc = SCHA_ERR_INTERNAL;
	for (i = 0; i < SCDS_MAX_IPADDR_TYPES; i++) {
		if (sl[i].status != SCDS_FMSOCK_OK)
			continue;

		if ((soflags = fcntl(sl[i].fd, F_GETFL, 0)) < 0) {
			scds_syslog(LOG_ERR, "Unable to get socket flags: %s.",
			    strerror(errno));
			(void) close(sl[i].fd);
			sl[i].status = SCDS_FMSOCK_ERR;
			sl[i].err = SCHA_ERR_INTERNAL;
			continue;
		}

		soflags &= ~(O_NDELAY | O_NONBLOCK);

		if (fcntl(sl[i].fd, F_SETFL, soflags) != 0) {
			scds_syslog(LOG_ERR, "Unable to set socket flags: %s.",
			    strerror(errno));
			(void) close(sl[i].fd);
			sl[i].status = SCDS_FMSOCK_ERR;
			sl[i].err = SCHA_ERR_INTERNAL;
			continue;
		}

		/* it all worked for atleast one socket, return success */
		rc = SCHA_ERR_NOERR;
	}

	/*
	 * This api returns NOERR even for partial failures. eg. v4 connect
	 * works but v6 connect fails. The idea is that even if one valid
	 * socket is returned to the caller, caller is supposed to cleanup using
	 * scds_fm_net_disconnect(). If this api returned failure on partial
	 * success, caller is not likely to call disconnect(), leading to fd
	 * leaks
	 *
	 * If this api returns failure, caller is not expected to perform any
	 * clean up.
	 */
	return (rc);
}

/*
 * Disconnects from all sockets obtained by calling scds_fm_net_connect
 *
 * Timeout is not actually enforced, just like scds_fm_tcp_disconnect().
 * This has never been a problem (not so far that is).
 */
scha_err_t
scds_fm_net_disconnect(scds_handle_t handle, scds_socket_t *socklist,
    int count, time_t timeout)
{
	scds_priv_handle_t *h;
	int i;

	h = check_handle(handle);

	/* very basic checks */
	if ((socklist == NULL) || (count < SCDS_MAX_IPADDR_TYPES) ||
	    (timeout < 0)) {
		scds_syslog(LOG_ERR, "%s called with invalid parameters",
		    "scds_fm_net_disconnect");
		return (SCHA_ERR_INVAL);
	}

	(void) mutex_lock(&h->scds_mutex);

	for (i = 0; i < count; i++) {
		if (socklist[i].status == SCDS_FMSOCK_OK)
			(void) close(socklist[i].fd);
	}

	/* this will also set all status fields to NA */
	bzero(socklist, (uint_t)count * sizeof (scds_socket_t));

	(void) mutex_unlock(&h->scds_mutex);

	return (SCHA_ERR_NOERR);

}

/*
 * IPv6 enabled version of scds_simple_probe. Connects to and disconnects
 * from upto a pair of IP addresses
 *
 * This does _not_take scds_fm_net_connect's philosophical view that the cup
 * is half-full. For partial failures, the aggregate return code will be
 * an ERR. A NOERR is returned iff all attempted operations are successful.
 */
scha_err_t
scds_simple_net_probe(scds_handle_t handle, scds_netaddr_t addr, time_t timeout,
    scds_fmsock_status_t *status, int count)
{
	scds_priv_handle_t *h;
	scha_err_t rc;
	hrtime_t t1, t2;
	long t;
	scds_socket_t socklist[SCDS_MAX_IPADDR_TYPES];

	h = check_handle(handle);

	/* very basic checks */
	if ((addr.hostname == NULL) || (timeout <= 0) || (status == NULL) ||
	    (count < SCDS_MAX_IPADDR_TYPES) ||
	    ((addr.port_proto.proto != SCDS_IPPROTO_TCP) &&
	    (addr.port_proto.proto != SCDS_IPPROTO_TCP6))) {
		scds_syslog(LOG_ERR, "%s called with invalid parameters",
		    "scds_simple_net_probe");
		return (SCHA_ERR_INVAL);
	}

	(void) mutex_lock(&h->scds_mutex);

	t1 = gethrtime()/((hrtime_t)1E9);
	rc = priv_fm_net_connect(socklist, addr, timeout);
	t2 = gethrtime()/((hrtime_t)1E9);

	status[0] = socklist[0].status;
	status[1] = socklist[1].status;

	t = (long)(t2 - t1);
	t = timeout - t;

	/*
	 * If all the time has been used, use a hard-coded
	 * value of 2 seconds to close socket. This makes
	 * sure we have no fd leak.
	 */
	if (t <= 0) {
		t = MIN_DISCONNECT_TIME;
	}

	if (rc == SCHA_ERR_NOERR) {
		/* fails only if given wrong parameters so we ignore it here */
		(void) scds_fm_net_disconnect(handle, socklist,
		    SCDS_MAX_IPADDR_TYPES, t);
	}

	if ((status[0] == SCDS_FMSOCK_ERR) || (status[1] == SCDS_FMSOCK_ERR)) {
		/*
		 * Atleast one socket wasnt connected. Error message has
		 * already been logged by priv_fm_net_connect so there is
		 * no need to log another message
		 */
		rc = SCHA_ERR_INTERNAL;
	}

	(void) mutex_unlock(&h->scds_mutex);
	return (rc);
}

/*
 * this function will be used to verify if the ip is allowed to be assigned
 * to the zone cluster. This contacts rgm which will look through clzonecfg
 * tables to find the information.
 */
scha_err_t
scds_verify_ip(scds_handle_t handle, char *ip)
{
	scds_priv_handle_t *h = check_handle(handle);
	return (scds_verify_ip_adapter(
	    h->ptable.pt_zonename, SCHA_VERIFY_IP, ip));
}

scha_err_t
scds_verify_adapter(scds_handle_t handle, char *ip)
{
	scds_priv_handle_t *h = check_handle(handle);
	return (scds_verify_ip_adapter(
	    h->ptable.pt_zonename, SCHA_VERIFY_ADAPTER, ip));
}

scha_err_t
scds_verify_ip_adapter(char *zname, const char *tag, char *arg)
{
	scha_err_t		e;
	scha_cluster_t		clust_handle;
	int			ret;

	while (1) {
		e = scha_cluster_open_zone(
		    zname, &clust_handle);
		if (e != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR,
			    "Failed to retrieve the cluster handle : %s.",
			    scds_error_string(e));
			return (e);
		}

		e = scha_cluster_get_zone(zname, clust_handle, (char *)tag,
		    arg, &ret);

		if (e == SCHA_ERR_NOERR) {
			break;
		}

		if (e != SCHA_ERR_SEQID) {
			scds_syslog(LOG_ERR,
			    "Failed to retrieve the property %s for %s: %s.",
			    tag,
			    arg, scds_error_string(e));
			(void) scha_cluster_close_zone(zname, clust_handle);
			return (e);
		}
		scds_syslog(LOG_INFO,
		    "Retrying to retrieve the cluster information.");
		    /* close the cluster handle */
		    (void) scha_cluster_close_zone(zname, clust_handle);
		    clust_handle = NULL;
		    (void) sleep(1);
	}
	(void) scha_cluster_close_zone(zname, clust_handle);

	return (ret == 0 ? SCHA_ERR_NOERR : SCHA_ERR_ACCESS);
}
