/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * Various utilities for data services which use
 *				the SC3.0 API
 */

#pragma ident	"@(#)scds_util.c	1.68	09/03/06 SMI"

#include <sys/os.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <libgen.h>
#include <stdarg.h>
#include <errno.h>
#include <limits.h>
#ifndef linux
#include <sys/mnttab.h>
#endif
#include <sys/sol_version.h>
#if SOL_VERSION >= __s10
#include <zone.h>
#endif

#include <scha.h>
#include <rgm/libdsdev.h>
#include "libdsdev_priv.h"

#define	SCDS_LOGLEVEL_FILE			"loglevel"
#define	FMP_ENTRY_DELIM				":"
static int scds_syslog_debuglevel = 0;		/* initialized in scds_init */

/*
 * Logs message to syslog using the priority field.
 * Priority field may be LOG_INFO, LOG_ERR, etc.
 */
void
scds_syslog(int pri, const char *fmt, ...)
{
	va_list		ap;

	/*
	 * override FlexeLint error 40;
	 * our lint doesn't understand compiler
	 * builtin variable __builtin_va_alist
	 */
				/* CSTYLED */
	va_start(ap, fmt);	/*lint !e26 !e50 !e10 */
	vsyslog(pri, fmt, ap);
	va_end(ap);

}

/*
 * Logs debug messages to syslog
 */
void
scds_syslog_debug(int level, const char *fmt, ...)
{
	va_list		ap;

	if (level <= scds_syslog_debuglevel) {
					/* CSTYLED */
		va_start(ap, fmt);	/*lint !e26 !e50 !e10 */
		vsyslog(LOG_DEBUG, fmt, ap);
		va_end(ap);

		return;
	}
}

void
scds_syslog_initialize(char *rname, char *rgname, char *rtname,
	char *curr_method)
{
	scds_syslog_initialize_zone(NULL, rname, rgname, rtname, curr_method);
}

void
scds_syslog_initialize_zone(char *zname, char *rname, char *rgname,
    char *rtname, char *curr_method)
{
	static char	scds_syslog_tag[SCDS_SYSLOG_BUF_SIZE];
	scha_err_t	err;
	int		logfacility;

		if (zname == NULL)
			zname = "";
		if (rname == NULL)
			rname = "";

		if (curr_method == NULL)
			curr_method = "";

		if (rgname == NULL)
			rgname = "";

		if (rtname == NULL)
			rtname = "";

		scds_syslog_set_debug_level(rtname);

		(void) snprintf(scds_syslog_tag, sizeof (scds_syslog_tag),
		    "SC[%s%c%s%c%s%c%s%c%s]", zname, SCDS_PMFTAG_CHAR,
		    rtname, SCDS_PMFTAG_CHAR, rgname,
		    SCDS_PMFTAG_CHAR, rname, SCDS_PMFTAG_CHAR, curr_method);

		/* Get the system logfacility number */
		err = scha_cluster_getlogfacility(&logfacility);

		if (err != SCHA_ERR_NOERR) {
			/* Use LOG_LOCAL7 */
			scds_syslog_debug(SCDS_DBG_LOW,
			    "Failed to retrieve the logfacility from API. "
			    "Using the LOG_LOCAL7 as logfacility for "
			    "logging messages.");
			openlog(scds_syslog_tag, LOG_CONS, LOG_LOCAL7);
		} else {
			scds_syslog_debug(SCDS_DBG_LOW,
			    "logfacility = %d", logfacility);
			openlog(scds_syslog_tag, LOG_CONS, logfacility);
		}

		scds_syslog_debug(SCDS_DBG_LOW,
		    "Debug Level is scds_syslog_debuglevel <%d>\n",
		    scds_syslog_debuglevel);
}

void
scds_syslog_set_debug_level(char *rtname)
{
	FILE	*fp;
	char	buf[SCDS_ARRAY_SIZE];

	scds_syslog_debuglevel = 0;
	if (rtname == NULL) {
		return;
	}

	(void) snprintf(buf, sizeof (buf), "%s/%s/%s",
	    SCDS_VAROPT, rtname, SCDS_LOGLEVEL_FILE);

	fp = fopen(buf, "r");
	if (fp == NULL) {
		/*
		 * The default loglevel file doesn't exist.
		 * If the given rtname contains a version number, the resource
		 * may have been updated from a previous rt version.
		 * In this case, the debug level should be set to the value
		 * specified in the loglevel file for rt basename of rtname.
		 */
		char rt_basename[SCDS_ARRAY_SIZE];
		char *rt_version, *ptr;

		(void) strcpy(rt_basename, rtname);
		rt_version = strchr(rt_basename, ':');
		if (rt_version == NULL)
			/* rtname doesn't have a version number */
			return;

		(void) strtok_r(rt_basename, ":", &ptr);
		(void) snprintf(buf, sizeof (buf), "%s/%s/%s",
		    SCDS_VAROPT, rt_basename, SCDS_LOGLEVEL_FILE);
		fp = fopen(buf, "r");

		if (fp == NULL)
			return;
	}
	(void) fscanf(fp, "%d", &scds_syslog_debuglevel);

	(void) fclose(fp);
	if (scds_syslog_debuglevel < 0)
		scds_syslog_debuglevel = SCDS_DBG_0;
}


scha_err_t
scds_validate_probe_values(scds_priv_handle_t *h,
    scds_method_opcode_t pt_opcode)
{
	if (((h->ptable.pt_r->r_sys_props->p_tprobe_interval +
	    h->ptable.pt_r->r_ext_props->x_probe_timeout) *
	    h->ptable.pt_r->r_sys_props->p_retry_count * 2) >
	    h->ptable.pt_r->r_sys_props->p_retry_interval) {
		if (pt_opcode != SCDS_OP_NONE) {
			(void) fprintf(stderr, "Current setting of "
			    "Retry_interval = %d, might prevent failover on "
			    "repeated probe failures. It is recommended that "
			    "Retry_interval be greater than or equal to "
			    "[(Thorough_probe_interval + Probe_timeout) * 2 *"
			    " Retry_count]. Current values are "
			    "(Thorough_probe_interval = %d,Retry_count = %d,"
			    "Probe_timeout = %d).",
			    h->ptable.pt_r->r_sys_props->p_retry_interval,
			    h->ptable.pt_r->r_sys_props->p_tprobe_interval,
			    h->ptable.pt_r->r_sys_props->p_retry_count,
			    h->ptable.pt_r->r_ext_props->x_probe_timeout);
		} else {
			/*
			 * SCMSGS
			 * @explanation
			 * Validation of the probe related parameters suggest
			 * that invalid values might have been specified.
			 * @user_action
			 * This is a warning message only. However, the current
			 * settings might prevent the resource from failing
			 * over successfully in the event of a failure.
			 * Use clresource to modify the values of
			 * these parameters so that they will hold the correct
			 * relationship as suggested in the warning message.
			 */
			scds_syslog(LOG_WARNING, "Current setting of "
			    "Retry_interval= %d, might prevent failover on "
			    "repeated probe failures. It is recommended that "
			    "Retry_interval be greater than or equal to "
			    "[(Thorough_probe_interval + Probe_timeout) * 2 *"
			    " Retry_count]. Current values are "
			    "(Thorough_probe_interval = %d,Retry_count = %d,"
			    "Probe_timeout = %d).",
			    h->ptable.pt_r->r_sys_props->p_retry_interval,
			    h->ptable.pt_r->r_sys_props->p_tprobe_interval,
			    h->ptable.pt_r->r_sys_props->p_retry_count,
			    h->ptable.pt_r->r_ext_props->x_probe_timeout);
		}
	}
	return (SCHA_ERR_NOERR);
}

/*
 * scds_error_string()
 * Convert a error code to error string.
 * Just a wrapper for scha_strerror.
 */
const char *
scds_error_string(scha_err_t err_no)
{
	return (scha_strerror(err_no));
}

/*
 * scds_error_string_i18n()
 * Convert a error code to an internationalized error string.
 * Just a wrapper around scha_strerror_i18n.
 */
const char *
scds_error_string_i18n(int err_no)
{
	return (scha_strerror_i18n(err_no));
}

/*
 * Called by scds_hasp_check to check whether all
 * specified filesystems are actually mounted or not.
 * Returns 0 if all are mounted, -1 if any of them is
 * not mounted and >0 if there's an error.
 * Does not log anything
 */
#ifndef linux
static int
#if SOL_VERSION >= __s10
are_all_fs_mounted(const scha_str_array_t *mp_list, zoneid_t zoneid)
#else
are_all_fs_mounted(const scha_str_array_t *mp_list)
#endif
{
	FILE 		*fp;
	char 		*mnttab = "/etc/mnttab";
	struct mnttab 	mp, mpref;
	int 		rc;
	uint_t		i;
	char		errmsg[1024];
	char		rpath[PATH_MAX];
#if SOL_VERSION >= __s10
	char		*localzonepath = NULL;
	char		*globalzonepath = NULL;
	char		*delimiter = FMP_ENTRY_DELIM;
	char		*mnt_point;
	char		*lasts;
#endif

	rc = 0;
	fp = fopen(mnttab, "r");
	if (fp == NULL) {
		(void) snprintf(errmsg, sizeof (errmsg),	/* CSTYLED */
		    "fopen : %s", strerror(errno));		/*lint !e746 */
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", errmsg);
		return (1);
	}

	for (i = 0; (i < mp_list->array_cnt) && (rc == 0); i++) {
		mpref.mnt_special = NULL;
		mpref.mnt_mountp = mp_list->str_array[i];
#if SOL_VERSION >= __s10
		if ((mnt_point = strdup(mp_list->str_array[i])) == NULL) {
			int	err = errno;
			(void) snprintf(errmsg, sizeof (errmsg),
			    "Failed to allocate memory : %s.",
			    strerror(err));	/*lint !e746 */
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", errmsg);
			rc = 1;
			continue;
		}
		localzonepath = strtok_r(mnt_point, delimiter, &lasts);
		globalzonepath = strtok_r(NULL, delimiter, &lasts);
		if (zoneid == GLOBAL_ZONEID) {
			/*
			 * scds_hasp_check() called in global zone.
			 */
			if (globalzonepath == NULL) {
				mpref.mnt_mountp = localzonepath;
			} else {
				mpref.mnt_mountp = globalzonepath;
			}
		} else {
			/*
			 * scds_hasp_check() called in non global zone.
			 */
			mpref.mnt_mountp = localzonepath;
		}
#endif
		mpref.mnt_fstype = NULL;
		mpref.mnt_mntopts = NULL;
		mpref.mnt_time = NULL;
		/*
		 * Use the resolved path if successful.
		 */
		if (realpath(mpref.mnt_mountp, rpath) != NULL) {
			mpref.mnt_mountp = rpath;
		}
		rc = getmntany(fp, &mp, &mpref);
		if (rc > 0) {
			/* getmntany failed */
			(void) snprintf(errmsg, sizeof (errmsg),
			    "getmntany : %s", strerror(errno));
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", errmsg);
		} else if (rc == -1) {
			/*
			 * SCMSGS
			 * @explanation
			 * There is no filesystem mounted on the mount point.
			 * @user_action
			 * None. This is a debug message.
			 */
			scds_syslog(LOG_DEBUG, "No filesystem mounted on %s.",
			    mpref.mnt_mountp);
		}

#if SOL_VERSION >= __s10
		if (mnt_point) {
			free(mnt_point);
		}
#endif
		rewind(fp);
	}

	(void) fclose(fp);
	return (rc);
}
#endif

/*
 * This macro is meant to be used *only* by scds_hasp_check
 * Its primary purpose is to make the code easier to read
 * and understand. It's to be called with double brackets as it
 * has to take care of variable number of args. log_args
 * is what would have been passed on to scds_syslog
 */
#define	CATCH_SEQID(log_args) if (e == SCHA_ERR_SEQID) { \
			(void) scha_resource_close(rs_handle); \
			(void) sleep(1); \
			goto restart; \
		} else if (e != SCHA_ERR_NOERR) { \
			scds_syslog log_args; \
			rc = SCHA_ERR_INTERNAL; \
			(void) scha_resource_close(rs_handle); \
			goto cleanup; \
		}

/*
 * This define is for scds_hasp_check only as well.
 */
#define	NUM_DEP_TYPES 4

/*
 * scds_hasp_check()
 * Returns status information about HAStoragePlus resources
 * that a resource depends on
 *
 */

scha_err_t
scds_hasp_check(scds_handle_t handle, scds_hasp_status_t *hasp_status)
{
#ifdef linux
	/*
	 * SCMSGS
	 * @explanation
	 * Need explanation of this message!
	 * @user_action
	 * Need a user action for this message.
	 */
	scds_syslog(LOG_DEBUG, " %s.",
	    "scds_hasp_check API returns HASP_NO_RESOURCE on Linux");
	return (SCDS_HASP_NO_RESOURCE);
#else
	scha_err_t		e, rc;
	char 			*nodename, *rs_name, *rt_name;
	scha_str_array_t	*deps, *nodelist;
	uint_t			i, j, k;
	scha_resource_t		rs_handle;
	int			hasp_rs_count, onl_count;
	scha_extprop_value_t	*fsmp = NULL;
	scha_extprop_value_t	*zpools = NULL;
	scha_rsstate_t		rs_state;
	scds_priv_handle_t	*ph;
	scha_str_array_t	*deplist[NUM_DEP_TYPES];
	char			buf[SCDS_ARRAY_SIZE];
	char 			*zonename;

#if SOL_VERSION >= __s10
	zoneid_t		zoneid;

	if ((zoneid = getzoneid()) < 0) {
		int	err = errno;
		(void) snprintf(buf, sizeof (buf),
		    "getzoneid() failed : %s.", strerror(err));
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", buf);
		return (SCHA_ERR_INTERNAL);
	}
#endif

	/*
	 * 'nodename' is the node in which the resource is configured.
	 * It is in the form "nodename:zonename" for native non-global zones,
	 * otherwise "nodename".
	 *
	 * 'zonename' is NULL unless we are running in the global zone on
	 * behalf of a resource in a non-global zone, in which case it
	 * contains the zonename in which the resource is configured.
	 */
	nodename = NULL;
	ph = (scds_priv_handle_t *)handle;
	zonename = ph->ptable.pt_zonename;
	if (zonename != NULL) {
		e = scds_get_fullname(zonename, &nodename,
		    ph->is_zone_cluster);
	} else {
		e = scha_cluster_getzone(&nodename);
	}
	if (e != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		    scds_error_string(e));
		free(nodename);
		return (SCHA_ERR_INTERNAL);
	}

	(void) mutex_lock(&ph->scds_mutex);

	deplist[0] = ph->ptable.pt_r->r_sys_props->p_strong_dep;
	deplist[1] = ph->ptable.pt_r->r_sys_props->p_weak_dep;
	deplist[2] = ph->ptable.pt_r->r_sys_props->p_restart_dep;
	deplist[3] = ph->ptable.pt_r->r_sys_props->p_offline_restart_dep;

restart:
	hasp_rs_count = 0;
	onl_count = 0;


	/* loop runs for each dependency */
	deps = deplist[0];
	for (i = 0; i < NUM_DEP_TYPES; i++, deps = deplist[i])
	for (j = 0; (deps != NULL) && (j < deps->array_cnt); j++) {

		fsmp = zpools = NULL;
		rs_name = deps->str_array[j];

		/*
		 * For speed, we could specify ptable.pt_rgname
		 * rather than NULL here but then the code would
		 * break for dependencies across RGs
		 */
		e = scha_resource_open_zone(zonename,
		    rs_name, NULL, &rs_handle);

		if (e != SCHA_ERR_NOERR) {
			rc = SCHA_ERR_INTERNAL;
			scds_syslog(LOG_ERR, "Failed to retrieve the resource "
			    "handle: %s.", scds_error_string(e));
			goto cleanup; /*lint !e801 */
		}

		/* get resource type */
		e = scha_resource_get_zone(zonename,
		    rs_handle, SCHA_TYPE,
		    &rt_name);

		CATCH_SEQID((LOG_ERR, "Failed to get the resource type name: "
		    " %s.", scds_error_string(e))); /*lint !e801 */

		/*
		 * Only hasp resources are of interest to us.
		 * In this case, rt_name is of the form:
		 *	HASP_RT_NAME -or- HASP_RT_NAME:#
		 * where # is the RT version number.
		 */
		if (strncmp(rt_name, HASP_RT_NAME, strlen(HASP_RT_NAME)) ||
		    ((strlen(rt_name) > strlen(HASP_RT_NAME)) &&
		    rt_name[strlen(HASP_RT_NAME)] != ':')) {
			(void) scha_resource_close_zone(zonename,
			    rs_handle);
			continue;
		}

		/*
		 * we ignore resources where both FilesystemMountPoints and
		 * Zpools has not been set or is blank
		 */
		e = scha_resource_get_zone(zonename,
		    rs_handle, SCHA_EXTENSION,
		    HASP_FSMP_PROPERTY, &fsmp);

		CATCH_SEQID((LOG_ERR, "Failed to retrieve the resource "
		    "property %s: %s.", HASP_FSMP_PROPERTY,
		    scds_error_string(e))); /*lint !e801 */

		e = scha_resource_get_zone(zonename,
		    rs_handle, SCHA_EXTENSION,
		    HASP_ZPOOLS_PROPERTY, &zpools);

		if (e == SCHA_ERR_PROP) {
			/*
			 * This can be case where the HAS+ resource cannot have
			 * Zpools extension property. We need to handle this
			 * gracefully by assuming the Zpools extension property
			 * value is NULL.
			 */
			zpools = NULL;
		} else {
			CATCH_SEQID((LOG_ERR, "Failed to retrieve the resource "
			    "property %s: %s.", HASP_FSMP_PROPERTY,
			    scds_error_string(e))); /*lint !e801 */
		}

		if (((fsmp == NULL) ||
		    (fsmp->val.val_strarray->array_cnt == 0)) &&
		    ((zpools == NULL) ||
		    (zpools->val.val_strarray->array_cnt == 0))) {
			(void) scha_resource_close_zone(zonename,
			    rs_handle);
			continue; /* move onto next resource */
		}

		/*
		 * we are interested in this resource
		 * Either the zpools value or fsmp value is not NULL.
		 */
		hasp_rs_count++;

		if ((zpools != NULL) &&
		    (zpools->val.val_strarray->array_cnt > 0)) {

			/*
			 * Check for the resource status
			 * - If this resource is online on this node continue
			 *   to check for status of file systems in fsmp value.
			 * - If this resource is not online on this node , find
			 *   the status of the resource on other nodes.
			 *   It is okay to skip finding status of file systems
			 *   in fsmp at this point, as the effective status
			 *   of the resource remains same.
			 */
			e = scha_resource_get_zone(zonename,
			    rs_handle, SCHA_RESOURCE_STATE_NODE,
			    nodename, &rs_state);

			CATCH_SEQID((LOG_ERR, "Failed to retrieve the resource "
			    "property %s: %s.", SCHA_RESOURCE_STATE,
			    scds_error_string(e))); /*lint !e801 */

			if (rs_state != SCHA_RSSTATE_ONLINE &&
			    rs_state != SCHA_RSSTATE_ONLINE_NOT_MONITORED) {
				/*
				 * resource is not online on this node.
				 */
				goto check_other_nodes; /*lint !e801 */
			}
		}

		if ((fsmp == NULL) ||
		    (fsmp->val.val_strarray->array_cnt == 0)) {
			(void) scha_resource_close_zone(zonename,
			    rs_handle);
			continue; /* move onto next resource */
		}

		/*
		 * If all filesystems are mounted here, we'll assume
		 * the resource is online here and skip the rest of
		 * the tests. This extra check is required because
		 * scha_resource_get may provide slightly stale status.
		 * See RFE 4344345 for details.
		 * Note: Even if some fs is not mounted here, we still
		 * call scha_resource_get and let the RGM tell us the
		 * state.
		 */
#if SOL_VERSION >= __s10
		e = are_all_fs_mounted(fsmp->val.val_strarray, zoneid);
#else
		e = are_all_fs_mounted(fsmp->val.val_strarray);
#endif
		if (e == 0) {
			(void) scha_resource_close_zone(zonename,
			    rs_handle);
			continue;
		} else if (e > 0) {
			/* are_all_fs_mounted logs if it returns > 0 */
			(void) scha_resource_close_zone(zonename,
			    rs_handle);
			rc = SCHA_ERR_INTERNAL;
			goto cleanup; /*lint !e801 */
		}

		/* is the resource online here? */
		e = scha_resource_get_zone(zonename,
		    rs_handle, SCHA_RESOURCE_STATE_NODE,
		    nodename, &rs_state);

		CATCH_SEQID((LOG_ERR, "Failed to retrieve the resource "
		    "property %s: %s.", SCHA_RESOURCE_STATE,
		    scds_error_string(e))); /*lint !e801 */

		/*
		 * We get here only if are_all_fs_mounted() told us that
		 * not all filesystems are mounted on this node. Make sure
		 * the resource status does not contradict that. Fail with
		 * SCHA_ERR_INTERNAL if it does.
		 */
		if (rs_state == SCHA_RSSTATE_ONLINE ||
		    rs_state == SCHA_RSSTATE_ONLINE_NOT_MONITORED) {
			(void) snprintf(buf, sizeof (buf), "Resource %s is "
			    "online on this node but some (or all) of the "
			    "filesystems that it manages are not mounted here. "
			    "This is an invalid state",
			    rs_name);
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", buf);
			(void) scha_resource_close_zone(zonename,
			    rs_handle);
			rc = SCHA_ERR_INTERNAL;
			goto cleanup; /*lint !e801 */
		}

check_other_nodes:

		/*
		 * Find out all nodes where our resource group
		 * is allowed to be (this is basically rg_nodelist)
		 */
		if ((nodelist = ph->ptable.pt_rg->rg_nodelist) == NULL) {
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			    "nodelist is null");
			(void) scha_resource_close_zone(zonename,
			    rs_handle);
			rc = SCHA_ERR_INTERNAL;
			goto cleanup; /*lint !e801 */
		}

		/*
		 * Cycle through this list of nodes, skip the node on
		 * which we are running because we have already determined
		 * the resource status on this node
		 */
		for (k = 0; k < nodelist->array_cnt; k++) {
			if (strcasecmp(nodename, nodelist->str_array[k]) == 0)
				continue;
			e = scha_resource_get_zone(zonename,
			    rs_handle,
			    SCHA_RESOURCE_STATE_NODE,
			    nodelist->str_array[k], &rs_state);

			CATCH_SEQID((LOG_ERR, "Failed to retrieve the "
			    "resource property %s: %s.",
			    SCHA_RESOURCE_STATE_NODE,
			    scds_error_string(e))); /*lint !e801 */

			if ((rs_state == SCHA_RSSTATE_ONLINE) ||
			(rs_state == SCHA_RSSTATE_ONLINE_NOT_MONITORED)) {
				onl_count++;
				break;
			}
		}

		(void) scha_resource_close_zone(zonename, rs_handle);

		/* did we break out of the loop or fall through? */
		if (k == nodelist->array_cnt) {
			/* resource is not online on any of our primaries */
			/*
			 * SCMSGS
			 * @explanation
			 * The named resource is not online on any cluster
			 * node.
			 * @user_action
			 * None. This is an informational message.
			 */
			scds_syslog(LOG_INFO, "Resource %s is not online "
			    "anywhere.", rs_name);
			rc = SCHA_ERR_NOERR;
			*hasp_status = SCDS_HASP_NOT_ONLINE;
			goto cleanup; /*lint !e801 */
		}

	} /* both for loops end with this */

	/* no error anywhere */
	/*
	 * exiting the loop, i = NUM_DEP_TYPES and deps = deplist[i]
	 * becomes 'out of bound'. Ignore it because deps is not used
	 * any longer.
	 */
	rc = SCHA_ERR_NOERR; /*lint !e661 */

	if (onl_count > 0)
		*hasp_status = SCDS_HASP_ONLINE_NOT_LOCAL;
	else {
		/*
		 * either all hasp resources are here or
		 * there arent any hasp resources
		 */
		if (hasp_rs_count == 0)
			*hasp_status = SCDS_HASP_NO_RESOURCE;
		else
			*hasp_status = SCDS_HASP_ONLINE_LOCAL;
	}

cleanup:
	if (nodename != NULL) /* wont be NULL but no harm in making sure */
		free(nodename);

	(void) mutex_unlock(&ph->scds_mutex);

	return (rc);
#endif /* linux */
}
