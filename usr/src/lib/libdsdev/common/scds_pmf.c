/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scds_pmf.c	1.56	09/01/09 SMI"

/*
 * scds_pmf.c - libdsdev routines that encapsulate PMF functionality
 */

#include <sys/os.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h>
#include <sys/stat.h>

#include "libdsdev_priv.h"
#if DOOR_IMPL
#include <rgm/pmf.h>
#include <rgm/door/pmf_door.h>
#else
#include "pmf.h"
#endif

#ifdef SC_SRM
#include <project.h>
#endif

#define	PMF_ACTION_SCRIPT	"/usr/cluster/lib/sc/scds_pmf_action_script"

/* PMF tag suffixes */
static char *scds_pmf_tag_suffix[] = { ".svc", ".mon", "" };

/*
 * Note: internal error messages are formatted using snprintf()
 */

/* XXX needs mutex stuff for accessing handle and doing fork1/execv */


/*
 * map_pmf_code
 *
 * Map libpmf errors to SCHA_ERR errors.
 */
static
scha_err_t
map_pmf_code(pmf_error code)
{
	switch (code.type) {
	case PMF_OKAY:
		return (SCHA_ERR_NOERR);
	case PMF_SYSERRNO:
		switch (code.sys_errno) {
		case ENOENT:
			/* tag doesn't exist */
			return (SCHA_ERR_INVAL);
		case ETIME:
			/* request timed out */
			return (SCHA_ERR_TIMEOUT);
		default:
			return (SCHA_ERR_INTERNAL);
		}
	case PMF_DUP:
	case PMF_ACCESS:
	case PMF_CONNECT:
	default:
		return (SCHA_ERR_INTERNAL);
	}
/* Supressing lint error. This is C++ related informational message */
} /*lint !e1746 */


/*
 * scds_pmf_get_status
 *
 * Returns SCHA_ERR_NOERR on success.  Sets *status to SCDS_PMF_MONITORED
 * if the specified instance is being monitored.  Sets *status to
 * SCDS_PMF_NOT_MONITORED if the specified instance is NOT being monitored.
 *
 * Returns SCHA_ERR_INVAL if the resource group name or resource name was
 * too long, or if an invalid parameter was passed to the routine.
 * Returns SCHA_ERR_INTERNAL on other errors.
 *
 * The caller should ignore *status if something other than SCHA_ERR_NOERR
 * is returned.
 */
scha_err_t
scds_pmf_get_status(scds_handle_t handle, scds_pmf_type_t type, int index,
    scds_pmf_status_t *status)
{
	char			pmftag[SCDS_PMFTAG_SIZE];
	scha_err_t		err;
	pmf_status_result	statusres;
	pmf_cmd			pmfcmd;
	scds_priv_handle_t	*ph;

	ph = check_handle(handle);

	if (type != SCDS_PMF_TYPE_SVC &&
	    type != SCDS_PMF_TYPE_MON &&
	    type != SCDS_PMF_TYPE_OTHER) {
		/*
		 * SCMSGS
		 * @explanation
		 * An invalid value was passed for the program_type argument
		 * in the pmf routines.
		 * @user_action
		 * This is a programming error. Verify the value specified for
		 * program_type argument and correct it. The valid types are:
		 * SCDS_PMF_TYPE_SVC: data service application
		 * SCDS_PMF_TYPE_MON: fault monitor SCDS_PMF_TYPE_OTHER: other
		 */
		scds_syslog(LOG_ERR, "Invalid type %d passed.", type);
		return (SCHA_ERR_INVAL);
	}

	/* get the pmftag for this instance */
	if ((err = priv_pmf_create_tag_name(ph, type, index,
	    (size_t)SCDS_PMFTAG_SIZE, pmftag)) != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * Failed to create the tag that has used to register with the
		 * process monitor facility.
		 * @user_action
		 * Check the syslog messages that occurred just before this
		 * message. In case of internal error, save the
		 * /var/adm/messages file and contact authorized Sun service
		 * provider.
		 */
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the process monitor facility tag.");
		return (err);
	}

	/*
	 * Query for pmf status:
	 * equivalent of 'pmfadm -q <tag>'
	 */

	(void) memset(&pmfcmd, 0, sizeof (pmf_cmd));
	(void) memset(&statusres, 0, sizeof (pmf_status_result));
	pmfcmd.security = PMF_SECURITY_DEF;
	if (pmf_status(pmftag, &pmfcmd, &statusres) < 0) {
		free_rpc_args(&statusres);
		return (SCHA_ERR_INTERNAL);
	}
	free_rpc_args(&statusres);

	switch (statusres.code.type) {
	case PMF_OKAY:
		*status = SCDS_PMF_MONITORED;
		return (SCHA_ERR_NOERR);
	case PMF_SYSERRNO:
		switch (statusres.code.sys_errno) {
		case ENOENT:
			*status = SCDS_PMF_NOT_MONITORED;
			return (SCHA_ERR_NOERR);
		case ETIME:
			return (SCHA_ERR_TIMEOUT);
		default:
			return (SCHA_ERR_INTERNAL);
		}
	case PMF_ACCESS:
	case PMF_CONNECT:
	case PMF_DUP:
	default:
		return (SCHA_ERR_INTERNAL);
	}
}


/*
 * scds_pmf_restart_fm
 *
 * Returns SCHA_ERR_NOERR on success.
 * Returns SCHA_ERR_INVAL if the resource group name or resource name was
 * too long, or if an invalid parameter was passed to the routine.
 * Returns SCHA_ERR_TIMEOUT if pmf_kill timed out.
 * Returns SCHA_ERR_INTERNAL on other errors.
 */
scha_err_t
scds_pmf_restart_fm(scds_handle_t handle, int index)
{
	char			pmftag[SCDS_PMFTAG_SIZE];
	scha_err_t		err;
	scds_priv_handle_t	*ph;
	time_t			timeout;
	pmf_result		result;
	pmf_cmd			pmfcmd;

	ph = check_handle(handle);

	if (ph->ptable.pt_r->r_fm_switch != SCHA_SWITCH_ENABLED) {
		/* log informational message and return success (idempotent) */
		/*
		 * SCMSGS
		 * @explanation
		 * An update operation on the resource would have been
		 * restarted the fault monitor. But, the monitor is currently
		 * disabled for the resource.
		 * @user_action
		 * This is informational message. Check whether the monitor is
		 * disabled for the resource. If not, consider it as an
		 * internal error and contact your authorised Sun service
		 * provider.
		 */
		scds_syslog(LOG_INFO,
		    "Cannot restart monitor: Monitor is not enabled.");
		return (SCHA_ERR_NOERR);
	}

	/* get the pmftag for this instance */
	if ((err = priv_pmf_create_tag_name(ph, SCDS_PMF_TYPE_MON,
	    index, (size_t)SCDS_PMFTAG_SIZE, pmftag)) != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the process monitor facility tag.");
		return (err);
	}

	if ((timeout = ph->ptable.pt_r->r_sys_props->p_monitor_stop_timeout) ==
	    SCDS_PROP_UNSET) {
		scds_syslog(LOG_ERR, "Property %s is not set.",
		    SCHA_MONITOR_STOP_TIMEOUT);
		/* XXX do we want to wait forever?  SCDS_PROP_UNSET == -1 */
	}

	/*
	 * equivalent of 'pmfadm -k <tag> -w <timeout>'
	 */

	(void) memset(&pmfcmd, 0, sizeof (pmf_cmd));
	(void) memset(&result, 0, sizeof (pmf_result));
	pmfcmd.security = PMF_SECURITY_DEF;
	pmfcmd.signal = SIGKILL;
	pmfcmd.timewait = (int)timeout;

	if (pmf_kill(pmftag, &pmfcmd, &result) < 0)
		return (SCHA_ERR_INTERNAL);

	/*
	 * pmf will automatically restart the fault monitor
	 * if there are retries remaining.
	 */

	return (map_pmf_code(result.code));
}


/*
 * scds_pmf_signal
 *
 * Sends the signal 'sig' to the process (tree) under PMF control;
 * equivalent of 'pmfadm -k <tag> -w <timeout> <signal>'
 *
 * Returns SCHA_ERR_NOERR on success.
 * Returns SCHA_ERR_INVAL if the resource group name or resource name was
 * too long, or if an invalid parameter was passed to the routine.
 * Returns SCHA_ERR_TIMEOUT if pmf_kill timed out.
 * Returns SCHA_ERR_INTERNAL on other errors.
 */
scha_err_t
scds_pmf_signal(scds_handle_t handle, scds_pmf_type_t type, int index,
    int sig, time_t timeout)
{
	char			pmftag[SCDS_PMFTAG_SIZE];
	scha_err_t		err;
	scds_priv_handle_t	*ph;
	pmf_result		result;
	pmf_cmd			pmfcmd;

	ph = check_handle(handle);

	if (type != SCDS_PMF_TYPE_SVC &&
	    type != SCDS_PMF_TYPE_MON &&
	    type != SCDS_PMF_TYPE_OTHER) {
		scds_syslog(LOG_ERR, "Invalid type %d passed.", type);
		return (SCHA_ERR_INVAL);
	}

	/* get the pmftag for this instance */
	if ((err = priv_pmf_create_tag_name(ph, type, index,
	    (size_t)SCDS_PMFTAG_SIZE, pmftag)) != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the process monitor facility tag.");
		return (err);
	}

	/*
	 * Send a signal via pmf:
	 * equivalent of 'pmfadm -k <tag> -w <timeout> <signal>'
	 */
	(void) memset(&pmfcmd, 0, sizeof (pmf_cmd));
	(void) memset(&result, 0, sizeof (pmf_result));
	pmfcmd.security = PMF_SECURITY_DEF;
	pmfcmd.signal = sig;
	pmfcmd.timewait = (int)timeout;
	if (pmf_kill(pmftag, &pmfcmd, &result) < 0)
		return (SCHA_ERR_INTERNAL);

	return (map_pmf_code(result.code));
}


#define	MAX_ARGV	16


/*
 * scds_pmf_start
 *
 * Calls scds_pmf_start_env() without any environment.
 * Returns SCHA_ERR_NOERR on success.
 * Returns SCHA_ERR_INVAL if the resource group name or resource name was
 * too long, or if an invalid parameter was passed to the routine.
 * Returns SCHA_ERR_TIMEOUT if pmf_start timed out.
 * Returns SCHA_ERR_NOMEM if out of memory.
 * The RGM state machine guarantees that START will never
 * be called "twice in a row" without an intervening call
 * to STOP; therefore if process (tree) is already running, this routine
 * returns SCHA_ERR_INTERNAL.
 * Returns SCHA_ERR_INTERNAL on other errors.
 */
scha_err_t
scds_pmf_start(scds_handle_t handle, scds_pmf_type_t type, int index,
    const char *cmd, int childMonitorLevel)
{
	return (scds_pmf_start_env(handle, type, index, cmd, childMonitorLevel,
	    NULL));
}


/*
 * Return TRUE in result parameter 'xresult' if 'cmd' exists,
 * is a regular file, and is executable by the real user id.
 * Otherwise return FALSE and syslog a useful error message.
 * 'cmd' may have leading whitespace and may be followed by args.
 * Function returns SCHA_ERR_NOERR unless the system runs out
 * of memory, in which case the function returns SCHA_ERR_NOMEM.
 */
static
scha_err_t
cmd_exists_and_executable(const char *cmd, boolean_t *xresult)
{
	char		*lasts, *justcmdp, *b, *cmdcopy;
	struct stat	statbuf;

	*xresult = B_TRUE;
	/*
	 * Make a copy of the string passed in because we are not allowed
	 * to modify it.  strtok_r will insert a NULL into the copied string.
	 */
	if ((cmdcopy = malloc(strlen(cmd) + 1)) == NULL) {
		scds_syslog(LOG_ERR, "Out of memory.");
		return (SCHA_ERR_NOMEM);
	}
	(void) strcpy(cmdcopy, cmd);

	b = cmdcopy;

	/*
	 * Get the name of the file to stat(2)/access(2):
	 * Strip off leading whitespace using isspace,
	 * then strip off trailing args using strtok_r.
	 */
	while (isspace(*b))
		b++;
	if ((justcmdp = (char *)strtok_r(b, " \t", &lasts)) == NULL) {
		/*
		 * print full 'cmd' because 'justcmdp' is NULL;
		 * we know 'cmd' isn't NULL because we checked before
		 * calling this routine
		 */
		/*
		 * SCMSGS
		 * @explanation
		 * An invalid pathname, displayed within the angle brackets,
		 * was passed to a libdsdev routine such as scds_timerun or
		 * scds_pmf_start. This could be the result of mis-configuring
		 * the name of a START or MONITOR_START method or other
		 * property, or a programming error made by the resource type
		 * developer.
		 * @user_action
		 * Supply a valid pathname to a regular, executable file.
		 */
		scds_syslog(LOG_ERR,
		    "Cannot determine command passed in: <%s>.", cmd);
		*xresult = B_FALSE;
		free(cmdcopy);
		return (SCHA_ERR_NOERR);
	}

	/* Determine whether the file exists and is a regular file. */
	if (stat(justcmdp, &statbuf) != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * The stat(2) system call failed on the specified pathname,
		 * which was passed to a libdsdev routine such as scds_timerun
		 * or scds_pmf_start. The reason for the failure is stated in
		 * the message. The error could be the result of 1)
		 * mis-configuring the name of a START or MONITOR_START method
		 * or other property, 2) a programming error made by the
		 * resource type developer, or 3) a problem with the specified
		 * pathname in the file system itself.
		 * @user_action
		 * Ensure that the pathname refers to a regular, executable
		 * file.
		 */
		scds_syslog(LOG_ERR, "Cannot stat %s: %s.", justcmdp,
		    strerror(errno));			/*lint !e746 */
		*xresult = B_FALSE;
		free(cmdcopy);
		return (SCHA_ERR_NOERR);
	}
	if ((statbuf.st_mode & S_IFMT) != S_IFREG) {
		/*
		 * SCMSGS
		 * @explanation
		 * The specified pathname, which was passed to a libdsdev
		 * routine such as scds_timerun or scds_pmf_start, does not
		 * refer to a regular file. This could be the result of 1)
		 * mis-configuring the name of a START or MONITOR_START method
		 * or other property, 2) a programming error made by the
		 * resource type developer, or 3) a problem with the specified
		 * pathname in the file system itself.
		 * @user_action
		 * Ensure that the pathname refers to a regular, executable
		 * file.
		 */
		scds_syslog(LOG_ERR, "Command %s is not a regular file.",
		    justcmdp);
		*xresult = B_FALSE;
		free(cmdcopy);
		return (SCHA_ERR_NOERR);
	}

	/*
	 * Determine whether file is executable by the real user id.
	 * If real user is root, we'll also need to check execute permissions
	 * because access(2) will return OK to execute for root even when
	 * execute permissions are not set.
	 */
	if (access(justcmdp, X_OK) != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * The specified pathname, which was passed to a libdsdev
		 * routine such as scds_timerun or scds_pmf_start, does not
		 * refer to an executable file. This could be the result of 1)
		 * mis-configuring the name of a START or MONITOR_START method
		 * or other property, 2) a programming error made by the
		 * resource type developer, or 3) a problem with the specified
		 * pathname in the file system itself.
		 * @user_action
		 * Ensure that the pathname refers to a regular, executable
		 * file.
		 */
		scds_syslog(LOG_ERR, "Command %s is not executable.", justcmdp);
		*xresult = B_FALSE;
		free(cmdcopy);
		return (SCHA_ERR_NOERR);
	}

	if (getuid() == 0) {
		/* for root, make sure file is executable by someone */
		if ((statbuf.st_mode & (S_IXUSR|S_IXGRP|S_IXOTH)) == 0) {
			/*
			 * SCMSGS
			 * @explanation
			 * The specified pathname, which was passed to a
			 * libdsdev routine such as scds_timerun or
			 * scds_pmf_start, refers to a file that does not have
			 * execute permission set. This could be the result of
			 * 1) mis-configuring the name of a START or
			 * MONITOR_START method or other property, 2) a
			 * programming error made by the resource type
			 * developer, or 3) a problem with the specified
			 * pathname in the file system itself.
			 * @user_action
			 * Ensure that the pathname refers to a regular,
			 * executable file.
			 */
			scds_syslog(LOG_ERR,
			    "Command %s does not have execute permission set.",
			    justcmdp);
			*xresult = B_FALSE;
			free(cmdcopy);
			return (SCHA_ERR_NOERR);
		}
	}

	free(cmdcopy);
	return (SCHA_ERR_NOERR);
}


#define	BINSH	"/bin/sh"
#define	MINUSc	"-c"

/*
 * scds_pmf_start_env
 *
 * The same as the spec for scds_pmf_start, but passes environment to
 * pmfadm (-E option).
 * Returns SCHA_ERR_NOERR on success.
 * Returns SCHA_ERR_INVAL if the resource group name or resource name was
 * too long, or if an invalid parameter was passed to the routine.
 * Returns SCHA_ERR_TIMEOUT if pmf_start timed out.
 * Returns SCHA_ERR_NOMEM if out of memory.
 * The RGM state machine guarantees that START will never
 * be called "twice in a row" without an intervening call
 * to STOP; therefore if process (tree) is already running, this routine
 * returns SCHA_ERR_INTERNAL.
 * Returns SCHA_ERR_INTERNAL on other errors.
 */
scha_err_t
scds_pmf_start_env(scds_handle_t handle, scds_pmf_type_t type, int index,
    const char *cmd, int childMonitorLevel, char **env)
{
	char			cmdcpy[SCDS_PMFARRAY_SIZE];
	char			pmftag[SCDS_PMFTAG_SIZE];
	char    		internal_err_str[INT_ERR_MSG_SIZE];
	char			*pmfargv[MAX_ARGV];
	int			pmfargc;
	int			rc;
	int			i;
	scds_priv_handle_t	*ph;
	scds_pmf_status_t	status;
	scha_err_t		err;
	pmf_result		result;
	pmf_cmd			pmfcmd;
	boolean_t		xresult;

#ifdef SC_SRM
	projid_t		projid = getprojid();
	struct  project		proj;
	char			buf[PROJECT_BUFSZ];
#endif

	ph = check_handle(handle);

	if (type != SCDS_PMF_TYPE_SVC &&
	    type != SCDS_PMF_TYPE_MON &&
	    type != SCDS_PMF_TYPE_OTHER) {
		scds_syslog(LOG_ERR, "Invalid type %d passed.", type);
		return (SCHA_ERR_INVAL);
	}

	if (cmd == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * A NULL value was specified for the command argument.
		 * @user_action
		 * Specify a non-NULL value for the command string.
		 */
		scds_syslog(LOG_ERR, "NULL command string passed.");
		return (SCHA_ERR_INVAL);
	}

	if ((scds_pmf_get_status(handle, type, index, &status) ==
	    SCHA_ERR_NOERR) && (status == SCDS_PMF_MONITORED)) {
		if (type == SCDS_PMF_TYPE_SVC)
			/*
			 * SCMSGS
			 * @explanation
			 * While attempting to restart the resource, error has
			 * occurred. The resource is already online.
			 * @user_action
			 * This is an internal error. Save the
			 * /var/adm/messages file from all the nodes. Contact
			 * your authorized Sun service provider.
			 */
			scds_syslog(LOG_ERR,
			    "Resource is already online.");
		else
			/*
			 * SCMSGS
			 * @explanation
			 * The resource's fault monitor is already running.
			 * @user_action
			 * This is an internal error. Save the
			 * /var/adm/messages file from all the nodes. Contact
			 * your authorized Sun service provider.
			 */
			scds_syslog(LOG_ERR,
			    "Fault monitor is already running.");

		/*
		 * The RGM state machine guarantees that START will never
		 * be called "twice in a row" without an intervening call
		 * to STOP; therefore we return an error here.
		 */
		return (SCHA_ERR_INTERNAL);
	}

	/* get the pmftag for this instance */
	if ((err = priv_pmf_create_tag_name(ph, type, index,
	    (size_t)SCDS_PMFTAG_SIZE, pmftag)) != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the process monitor facility tag.");
		return (err);
	}

	/*
	 * determine args to pmf_start routine;
	 * variations of 'pmfadm -c'
	 */
	(void) memset(&pmfcmd, 0, sizeof (pmf_cmd));
	(void) memset(&result, 0, sizeof (pmf_result));
	(void) memset(cmdcpy, 0, sizeof (cmdcpy));
	pmfcmd.security = PMF_SECURITY_DEF;

	/* set non-zero defaults */
	pmfcmd.period = -1;
	pmfcmd.monitor_children = PMF_ALL;

#ifdef SC_SRM
	/*
	 * Get the project name of the method
	 */
	if (projid == -1 ||
	    getprojbyid(projid, &proj, buf, (size_t)PROJECT_BUFSZ) == NULL)
		/* No such project */
		return (SCHA_ERR_INTERNAL);

	pmfcmd.project_name = strdup(proj.pj_name);
	if (pmfcmd.project_name == NULL) {
		scds_syslog(LOG_ERR, "Out of memory.");
		return (SCHA_ERR_NOMEM);
	}
#endif

	/* compute size of environment, if any */
	if (env) {
		for (i = 0; env[i] != NULL; i++)
			scds_syslog_debug(SCDS_DBG_LOW, "env is <%s>\n",
			    env[i]);
		/*
		 * In the following, +1 is for the NULL (char *) pointer, which
		 * PMF requires for terminating the list of env. variables.
		 */
		pmfcmd.num_env = i + 1;
		scds_syslog_debug(SCDS_DBG_LOW, "num_env is <%d>\n", i);
		pmfcmd.env = env;
		pmfcmd.env_passed = TRUE;
	}

	switch (type) {
	case SCDS_PMF_TYPE_SVC:
		/*
		 * Start cmd and register action script:
		 * equivalent of
		 * 'pmfadm -c <tag> -a PMF_ACTION_SCRIPT -C <level> cmd'
		 */

		if (cmd_exists_and_executable(cmd, &xresult) == SCHA_ERR_NOMEM)
			return (SCHA_ERR_NOMEM);
		if (xresult == B_FALSE)
			return (SCHA_ERR_INVAL);

		pmfcmd.action = PMF_ACTION_SCRIPT;

		if (childMonitorLevel >= 0) {
			pmfcmd.monitor_children = PMF_LEVEL;
			/* add one because we wrap with "/bin/sh -c" */
			pmfcmd.monitor_level = childMonitorLevel + 1;
		}

		/* Ensure cmd string will fit in our cmdcpy buffer */
		if (strlen(cmd) + 1 > sizeof (cmdcpy)) {
			/*
			 * SCMSGS
			 * @explanation
			 * The command string passed to the function is too
			 * long.
			 * @user_action
			 * Use a shorter command name or shorter path to the
			 * command.
			 */
			scds_syslog(LOG_ERR, "Command %s is too long.", cmd);
			return (SCHA_ERR_INVAL);
		}

		(void) strcpy(cmdcpy, cmd);

		/*
		 * wrap the command with "/bin/sh -c"
		 * to enable I/O redirection
		 */
		pmfargv[0] = BINSH;
		pmfargv[1] = MINUSc;
		pmfargv[2] = cmdcpy;
		pmfargv[3] = NULL;
		pmfargc = 3;

		/*
		 * SCMSGS
		 * @explanation
		 * The function is going to request the PMF to start the data
		 * service. If the request fails, refer to the syslog messages
		 * that appear after this message.
		 * @user_action
		 * This is an informational message, no user action is
		 * required.
		 */
		scds_syslog(LOG_INFO, "Attempting to start the data service "
		    "under process monitor facility.");
		break;

	case SCDS_PMF_TYPE_MON:

		if (childMonitorLevel >= 0) {
			pmfcmd.monitor_children = PMF_LEVEL;
			/* add one because we wrap with "/bin/sh -c" */
			pmfcmd.monitor_level = childMonitorLevel + 1;
		}

		pmfcmd.retries = ph->ptable.pt_r->r_ext_props->x_pmf_arg_n;
		pmfcmd.period = ph->ptable.pt_r->r_ext_props->x_pmf_arg_t;

		if (pmfcmd.retries == SCDS_PROP_UNSET ||
		    pmfcmd.period == SCDS_PROP_UNSET) {
			scds_syslog(LOG_ERR,
			    "Monitor_retry_count or Monitor_retry_interval "
			    "is not set.");
			/* we regard this to be an internal error, not EINVAL */
			return (SCHA_ERR_INVAL);
		}

		/*
		 * Modify the cmd that we were passed by appending
		 * 3 additional switches and args; equivalent of
		 *  pmfadm -c tag -C <level> -n <PMF_ARG_N> -t <PMF_ARG_T> \
		 *  path/cmd -R <rname> -T <rtname> -G <rgname>
		 *  (-Z <zonename> for RTs with global_zone = TRUE)
		 */
		if ((ph->ptable.pt_zonename) && (strcmp(
		    ph->ptable.pt_zonename, "global") != 0)) {
			if ((rc = snprintf(cmdcpy, sizeof (cmdcpy),
			    "%s/%s -R %s -T %s -G %s -Z %s",
			    ph->ptable.pt_rt->rt_basedir, cmd,
			    ph->ptable.pt_rname, ph->ptable.pt_rtname,
			    ph->ptable.pt_rgname,
			    ph->ptable.pt_zonename)) == -1) {
				(void) snprintf(internal_err_str,
				    sizeof (internal_err_str),
				    "Failed to form call to pmf_start: "
				    "Output error encountered");

				scds_syslog(LOG_ERR,
				    "INTERNAL ERROR: %s.", internal_err_str);
				return (SCHA_ERR_INTERNAL);
			}
		} else {
			if ((rc = snprintf(cmdcpy, sizeof (cmdcpy),
			    "%s/%s -R %s -T %s -G %s",
			    ph->ptable.pt_rt->rt_basedir, cmd,
			    ph->ptable.pt_rname, ph->ptable.pt_rtname,
			    ph->ptable.pt_rgname)) == -1) {
				(void) snprintf(internal_err_str,
				    sizeof (internal_err_str),
				    "Failed to form call to pmf_start: "
				    "Output error encountered");

				scds_syslog(LOG_ERR,
				    "INTERNAL ERROR: %s.", internal_err_str);
				return (SCHA_ERR_INTERNAL);
			}
		}
		if (rc > (SCDS_PMFARRAY_SIZE - 1)) {
			scds_syslog(LOG_ERR, "Command %s is too long.", cmd);
			return (SCHA_ERR_INVAL);
		}

		/* we check this down here because we need the basedir prefix */
		if (cmd_exists_and_executable(cmdcpy, &xresult)
		    == SCHA_ERR_NOMEM)
			return (SCHA_ERR_NOMEM);
		if (xresult == B_FALSE)
			return (SCHA_ERR_INVAL);

		/*
		 * wrap the command with "/bin/sh -c"
		 * to enable I/O redirection
		 */
		pmfargv[0] = BINSH;
		pmfargv[1] = MINUSc;
		pmfargv[2] = cmdcpy;
		pmfargv[3] = NULL;
		pmfargc = 3;

		/*
		 * SCMSGS
		 * @explanation
		 * The function is going to request the PMF to start the fault
		 * monitor. If the request fails, refer to the syslog messages
		 * that appear after this message.
		 * @user_action
		 * This is an informational message, no user action is
		 * required.
		 */
		scds_syslog(LOG_INFO, "Attempting to start the fault monitor"
		    " under process monitor facility.");
		break;

	case SCDS_PMF_TYPE_OTHER:
		/*
		 * Start the cmd:
		 * equivalent of 'pmfadm -c <tag> -C <level> cmd'
		 */

		if (cmd_exists_and_executable(cmd, &xresult) == SCHA_ERR_NOMEM)
			return (SCHA_ERR_NOMEM);
		if (xresult == B_FALSE)
			return (SCHA_ERR_INVAL);

		if (childMonitorLevel >= 0) {
			pmfcmd.monitor_children = PMF_LEVEL;
			/* add one because we wrap with "/bin/sh -c" */
			pmfcmd.monitor_level = childMonitorLevel + 1;
		}

		/* Ensure cmd string will fit in our cmdcpy buffer */
		if (strlen(cmd) + 1 > sizeof (cmdcpy)) {
			scds_syslog(LOG_ERR, "Command %s is too long.", cmd);
			return (SCHA_ERR_INVAL);
		}

		(void) strcpy(cmdcpy, cmd);

		/*
		 * wrap the command with "/bin/sh -c"
		 * to enable I/O redirection
		 */
		pmfargv[0] = BINSH;
		pmfargv[1] = MINUSc;
		pmfargv[2] = cmdcpy;
		pmfargv[3] = NULL;
		pmfargc = 3;

		break;

	default:
		scds_syslog(LOG_ERR, "Invalid type %d passed.", type);
		return (SCHA_ERR_INVAL);
	}

	rc = pmf_start(pmftag, &pmfcmd, pmfargc, pmfargv, &result);

	if (pmfcmd.project_name)
		free(pmfcmd.project_name);

	if (rc < 0)
		return (SCHA_ERR_INTERNAL);
	else
		return (map_pmf_code(result.code));
}


/*
 * scds_pmf_stop
 *
 * Returns SCHA_ERR_NOERR on success.
 * Returns SCHA_ERR_INVAL if the resource group name or resource name was
 * too long, or if an invalid parameter was passed to the routine.
 * Returns SCHA_ERR_TIMEOUT if pmf_stop timed out.
 * Returns SCHA_ERR_INTERNAL on other errors.
 */
scha_err_t
scds_pmf_stop(scds_handle_t handle, scds_pmf_type_t type, int index,
    int sig, time_t timeout)
{
	char			pmftag[SCDS_PMFTAG_SIZE];
	scds_pmf_status_t	status;
	scds_priv_handle_t	*ph;
	scha_err_t		err;
	pmf_result		result;
	pmf_cmd			pmfcmd;

	ph = check_handle(handle);

	if (type != SCDS_PMF_TYPE_SVC &&
	    type != SCDS_PMF_TYPE_MON &&
	    type != SCDS_PMF_TYPE_OTHER) {
		scds_syslog(LOG_ERR, "Invalid type %d passed.", type);
		return (SCHA_ERR_INVAL);
	}

	if ((scds_pmf_get_status(handle, type, index, &status) ==
	    SCHA_ERR_NOERR) && (status == SCDS_PMF_NOT_MONITORED)) {
		if (type == SCDS_PMF_TYPE_SVC)
			scds_syslog_debug(SCDS_DBG_LOW,
			    "Resource %s is not running\n",
			    ph->ptable.pt_rname);
		else
			scds_syslog_debug(SCDS_DBG_LOW,
			    "Fault monitor for resource %s is not running\n",
			    ph->ptable.pt_rname);
		return (SCHA_ERR_NOERR);
	}

	/* get the pmftag for this instance */
	if ((err = priv_pmf_create_tag_name(ph, type, index,
	    (size_t)SCDS_PMFTAG_SIZE, pmftag)) != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the process monitor facility tag.");
		return (err);
	}

	/*
	 * Send a SIGCONT to the process(es). That helps
	 * in faster recovery if the processes are SIGSTOPPED:
	 * equivalent of 'pmfadm -k <tag> SIGCONT'
	 */
	(void) memset(&pmfcmd, 0, sizeof (pmf_cmd));
	(void) memset(&result, 0, sizeof (pmf_result));
	pmfcmd.security = PMF_SECURITY_DEF;
	pmfcmd.signal = SIGCONT;
	(void) pmf_kill(pmftag, &pmfcmd, &result);

	/*
	 * Ignore the return code from sending SIGCONT;
	 * we want to send SIGTERM regardless.
	 */

	/*
	 * Stop monitoring the process under pmf:
	 * equivalent of 'pmfadm -s <tag> [-w <timeout> <signal>]'
	 */
	(void) memset(&pmfcmd, 0, sizeof (pmf_cmd));
	(void) memset(&result, 0, sizeof (pmf_result));
	pmfcmd.security = PMF_SECURITY_DEF;
	if (sig != SCDS_PMF_NOSIGNAL) {
		pmfcmd.signal = sig;
		if ((int)timeout == -1)
			pmfcmd.timewait = -1;
		else
			pmfcmd.timewait = (int)((timeout * SCDS_SMOOTH_PCT)
			    / 100);
	}

	/*
	 * SCMSGS
	 * @explanation
	 * The function is going to request the PMF to stop the data service.
	 * If the request fails, refer to the syslog messages that appear
	 * after this message.
	 * @user_action
	 * This is an informational message, no user action is required.
	 */
	scds_syslog(LOG_INFO, "Attempting to stop the data service "
	    "running under process monitor facility.");
	(void) pmf_stop(pmftag, &pmfcmd, &result);

	/*
	 * Ignore the return value from pmf_stop and just check
	 * whether the process is still running.
	 * Return OK if process is not running.
	 */
	err = scds_pmf_get_status(handle, type, index, &status);
	if ((err == SCHA_ERR_NOERR) && (status == SCDS_PMF_NOT_MONITORED))
		/* process is not running; return OK */
		return (SCHA_ERR_NOERR);

	if ((sig == SCDS_PMF_NOSIGNAL) || ((int)timeout == -1)) {
		/*
		 * (sig == SCDS_PMF_NOSIGNAL):
		 * No signal was specified.  Either the process is
		 * still running, or some other error was encountered
		 * while trying to determine whether the process was
		 * still running.  Return now.
		 *
		 * ((int)timeout == -1):
		 * We were given an infinite timeout but the pmf_stop
		 * call has returned.  If the process-tree died out,
		 * we would have returned with a SCHA_ERR_NOERR already.
		 * Getting to this point means something is wrong.
		 */
		return (err);
	}

	/*
	 * Try to stop the process with SIGKILL again:
	 * equivalent of 'pmfadm -s <tag> -w <timeout> SIGKILL'
	 * Note that we never reach here if the timeout specified
	 * was infinite. Either the first pmf_stop worked in that
	 * case or there was an error.
	 */
	(void) memset(&pmfcmd, 0, sizeof (pmf_cmd));
	(void) memset(&result, 0, sizeof (pmf_result));
	pmfcmd.security = PMF_SECURITY_DEF;
	pmfcmd.signal = SIGKILL;
	pmfcmd.timewait = (int)((timeout * SCDS_HARD_PCT) / 100);
	(void) pmf_stop(pmftag, &pmfcmd, &result);

	/*
	 * Check whether the pmf tag is still around.
	 */
	err = scds_pmf_get_status(handle, type, index, &status);
	if (err == SCHA_ERR_NOERR) {
		if (status == SCDS_PMF_NOT_MONITORED)
			/* process is not running; return OK */
			return (SCHA_ERR_NOERR);
		else
			/* process is still running */
			return (SCHA_ERR_INTERNAL);
	} else {
		/* some other error occurred */
		return (err);
	}
}


/*
 * scds_pmf_stop_monitoring
 *
 * Stop PMF monitoring of the process (tree);
 * equivalent of 'pmfadm -s <tag>'
 * Nuke the tag only; do not send a signal to the process (tree).
 * If the tag doesn't exist, just return success.
 *
 * Returns SCHA_ERR_NOERR on success.
 * Returns SCHA_ERR_INVAL if the resource group name or resource name was
 * too long, or if an invalid parameter was passed to the routine.
 * Returns SCHA_ERR_INTERNAL on other errors.
 */
scha_err_t
scds_pmf_stop_monitoring(scds_handle_t handle, scds_pmf_type_t type,
    int index)
{
	char			pmftag[SCDS_PMFTAG_SIZE];
	pmf_cmd			pmfcmd;
	pmf_result		result;
	scha_err_t		err;
	scds_priv_handle_t	*ph;

	ph = check_handle(handle);

	if (type != SCDS_PMF_TYPE_SVC &&
	    type != SCDS_PMF_TYPE_MON &&
	    type != SCDS_PMF_TYPE_OTHER) {
		scds_syslog(LOG_ERR, "Invalid type %d passed.", type);
		return (SCHA_ERR_INVAL);
	}

	/* get the pmftag for this instance */
	if ((err = priv_pmf_create_tag_name(ph, type, index,
	    (size_t)SCDS_PMFTAG_SIZE, pmftag)) != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the process monitor facility tag.");
		return (err);
	}

	/*
	 * Stop monitoring the process under pmf:
	 * equivalent of 'pmfadm -s <tag>'
	 */
	(void) memset(&pmfcmd, 0, sizeof (pmf_cmd));
	(void) memset(&result, 0, sizeof (pmf_result));
	pmfcmd.security = PMF_SECURITY_DEF;
	if (pmf_stop(pmftag, &pmfcmd, &result) < 0)
		return (SCHA_ERR_INTERNAL);

	err = map_pmf_code(result.code);
	/* ignore case where tag doesn't exist */
	if (err == SCHA_ERR_INVAL)
		return (SCHA_ERR_NOERR);

	return (err);
}


/*
 * priv_pmf_create_tag_name -- SUNW private function
 *
 * Returns SCHA_ERR_NOERR on success.
 * Returns SCHA_ERR_INVAL if the resource group name or resource name was
 * too long.
 * Returns SCHA_ERR_INTERNAL on other errors.
 */
scha_err_t
priv_pmf_create_tag_name(scds_priv_handle_t *ph, scds_pmf_type_t type,
    int index, size_t tagbufsize, char *tagbuf)
{
	int			rc;
	char    		internal_err_str[INT_ERR_MSG_SIZE];

	if (tagbuf == NULL) {
		(void) snprintf(internal_err_str,
		    sizeof (internal_err_str),
		    "Cannot create the tag: "
		    "NULL buffer passed");

		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (SCHA_ERR_INTERNAL);
	}

	if (type != SCDS_PMF_TYPE_SVC &&
	    type != SCDS_PMF_TYPE_MON &&
	    type != SCDS_PMF_TYPE_OTHER) {
		(void) snprintf(internal_err_str,
		    sizeof (internal_err_str),
		    "Cannot create the tag: "
		    "Invalid type passed");

		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (SCHA_ERR_INTERNAL);
	}

	/*
	 * Since the zc_name is optional, we put it at the end of the tag
	 * so that the scds PMF action script can parse it more easily.
	 */
	if ((ph->ptable.pt_zonename != NULL) &&
	    (strcmp(ph->ptable.pt_zonename, "global") != 0)) {
		/* Tag format: rg_name,rs_name,index<.svc|.mon|>,zc_name */
		rc = snprintf(tagbuf, tagbufsize,
		    "%s%c%s%c%d%s%c%s",
		    ph->ptable.pt_rgname,
		    SCDS_PMFTAG_CHAR,
		    ph->ptable.pt_rname,
		    SCDS_PMFTAG_CHAR,
		    index,
		    scds_pmf_tag_suffix[type],
		    SCDS_PMFTAG_CHAR,
		    ph->ptable.pt_zonename);
	} else {
		/* Tag format: rg_name,rs_name,index<.svc|.mon|> */
		rc = snprintf(tagbuf, tagbufsize,
		    "%s%c%s%c%d%s",
		    ph->ptable.pt_rgname,
		    SCDS_PMFTAG_CHAR,
		    ph->ptable.pt_rname,
		    SCDS_PMFTAG_CHAR,
		    index,
		    scds_pmf_tag_suffix[type]);
	}

	/* Check the return code of snprintf */
	if (rc < 0) {
		(void) snprintf(internal_err_str,
		    sizeof (internal_err_str),
		    "Failed to create tag: Output error encountered");

		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (SCHA_ERR_INTERNAL);
	}

	/*
	 * Check whether the string plus the NULL char fit in the buffer.
	 * This is not an internal error; it's a configuration error.
	 */
	if (rc > (SCDS_PMFARRAY_SIZE - 1)) {
		/*
		 * SCMSGS
		 * @explanation
		 * Process monitor facility failed to execute the command.
		 * Resource group name or resource name is too long for the
		 * process monitor facility command.
		 * @user_action
		 * Check the resource group name and resource name. Give short
		 * name for resource group or resource .
		 */
		scds_syslog(LOG_ERR,
		    "Resource group name or resource name is too long.");
		return (SCHA_ERR_INVAL);
	}

	return (SCHA_ERR_NOERR);
}


/* wide enough to print string values for timeout, signal */
#define	LONG_INT_PRINT_WIDTH	15

#define	MINUSt	"-t"
#define	MINUSk	"-k"

/*
 * XXX move this into scds_util.c?
 * scds_timerun: This function executes the given command under
 * the given timeout using hatimerun(1M).
 * Input arguments are the command to run, timeout and signal. hatimerun
 * will run the program with arguments as a child process under timeout. If
 * timeout expires, then hatimerun kills the child process with the supplied
 * signal. If signal is not provided then SIGKILL is used as the default signal
 * to kill the child process.
 */
scha_err_t
scds_timerun(scds_handle_t handle, const char *cmd, time_t timeout, int sig,
    int *commandReturnCode)
{
	int			rc;
	int			pid;
	int			wstat;
	char			signalstr[LONG_INT_PRINT_WIDTH];
	char			timeoutstr[LONG_INT_PRINT_WIDTH];
	char			*execvargv[MAX_ARGV];
	char			cmdcpy[SCDS_PMFARRAY_SIZE];
	boolean_t		xresult;

	/*
	 * handle isn't used now, but will be in the future
	 * for locking during fork1
	 */
	(void) check_handle(handle);

	if (cmd == NULL) {
		scds_syslog(LOG_ERR, "NULL command string passed.");
		return (SCHA_ERR_INVAL);
	}

	if (cmd_exists_and_executable(cmd, &xresult) == SCHA_ERR_NOMEM)
		return (SCHA_ERR_NOMEM);
	if (xresult == B_FALSE)
		return (SCHA_ERR_INVAL);

	/* hatimerun will not accept a zero or negative value for timeout */
	if (timeout < 1) {
		/*
		 * SCMSGS
		 * @explanation
		 * Failed to execute the command under the specified timeout.
		 * The specified timeout is invalid.
		 * @user_action
		 * Respecify a positive, non-zero timeout value.
		 */
		scds_syslog(LOG_ERR,
		    "Invalid timeout value %d passed.", timeout);
		return (SCHA_ERR_INVAL);
	}

	if (sig == -1) {
		/* No signal was specified */
		(void) sprintf(signalstr, "%d", SIGKILL);
	} else {
		(void) sprintf(signalstr, "%d", sig);
	}

	(void) sprintf(timeoutstr, "%ld", timeout);

	/* Ensure cmd string will fit in our cmdcpy buffer */
	if (strlen(cmd) + 1 > sizeof (cmdcpy)) {
		scds_syslog(LOG_ERR, "Command %s is too long.", cmd);
		return (SCHA_ERR_INVAL);
	}
	(void) strcpy(cmdcpy, cmd);

	/* create argv arguments to execv */
	execvargv[0] = SCDS_HATIMERUN;
	execvargv[1] = MINUSt;
	execvargv[2] = timeoutstr;
	execvargv[3] = MINUSk;
	execvargv[4] = signalstr;
	execvargv[5] = "--";
	/* wrap the command with "/bin/sh -c" to enable I/O redirection */
	execvargv[6] = BINSH;
	execvargv[7] = MINUSc;
	execvargv[8] = cmdcpy;
	execvargv[9] = NULL;

	/* Execute the hatimerun command via fork1/exec */

	/*
	 * In most cases, the simple
	 * strategy of calling exec() immediately in the child of the fork()
	 * should be sufficient for dealing with fork-related problems.  That's
	 * exactly what we do here.
	 */
#ifndef linux
	if ((pid = fork1()) == -1)
#else
	if ((pid = fork()) == -1)
#endif
	{
		/* parent */
		/*
		 * SCMSGS
		 * @explanation
		 * The fork(1) system call failed.
		 * @user_action
		 * Some system resource has been exceeded. Install more
		 * memory, increase swap space or reduce peak memory
		 * consumption.
		 */
		scds_syslog(LOG_ERR, "Couldn't fork1.");
		return (SCHA_ERR_INTERNAL);
	}

	if (pid == 0) {
		/* child */
		if (execv(SCDS_HATIMERUN, execvargv) != 0) {
			/*
			 * suppress lint complaint
			 * "call to ___errno() not made in the presence of
			 * a prototype" which is due to error in errno.h --
			 * prototype is missing "void" argument.
			 */
			scds_syslog(LOG_ERR, "Cannot execute %s: %s.",
			    cmd, strerror(errno));	/*lint !e746 */
			/* XXX exit() or _exit() ? */
			_exit(1);
		}
	}

	/* wait for hatimerun to complete */
	do {
		rc = waitpid(pid, &wstat, 0);
	} while (rc == -1 && errno == EINTR);

	/* Now check the return code from hatimerun */
	*commandReturnCode = WEXITSTATUS((uint_t)wstat);

	if (*commandReturnCode == 99)
		return (SCHA_ERR_TIMEOUT);

	return (SCHA_ERR_NOERR);
}
