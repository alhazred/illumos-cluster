/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * scds_fm.c - Various fault monitor related utilities for data services
 *			which use the SC3.0 API
 */

#pragma ident	"@(#)scds_fm.c	1.78	09/01/09 SMI"

#include <sys/os.h>
#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <signal.h>
#include <errno.h>
#include <strings.h>
#include <unistd.h>

#include <rgm/libdsdev.h>
#include "libdsdev_priv.h"

/* Private version of public utilities implemented in this file */

static scha_err_t
priv_failover_rg(scds_priv_handle_t *h);

static scha_err_t
priv_restart_rg(scds_priv_handle_t *h);

static scha_err_t
priv_restart_resource(scds_priv_handle_t *h);

static scha_err_t
priv_fm_sleep(scds_priv_handle_t *h, time_t timeout);

static scha_err_t
priv_fm_action(scds_priv_handle_t *h, int probe_status,
    long elapsed_milliseconds);

static scha_err_t
exit_probe_with_failure_status(scds_priv_handle_t *h);

/*
 * Maximum number of times a scha_resource_get call should be made
 * with the same handle - used by scds_fm_history
 */
#define	MAX_HANDLE_USAGE 500

/*
 * check_handle()
 * Converts scds_handle_t into scds_priv_handle_t *
 * syslog()s an error and abort()s if bad handle is
 * passed.
 */
scds_priv_handle_t *
check_handle(scds_handle_t handle)
{
	scds_priv_handle_t		*h;

	h = (scds_priv_handle_t *)handle;
	if (h == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * A null handle was passed for the function parameter. No
		 * further processing can be done without a proper handle.
		 * @user_action
		 * It's a programming error, core is generated. Specify a
		 * non-null handle in the function call.
		 */
		scds_syslog(LOG_ERR,
			"Null value is passed for the handle.");
		abort();
	}
	return (h);
}
/*
 * scds_failover_rg()
 * Failover resource group.
 * External API
 */
scha_err_t
scds_failover_rg(scds_handle_t handle)
{
	scds_priv_handle_t		*h;
	scha_err_t				e;

	h = check_handle(handle);

	(void) mutex_lock(&h->scds_mutex);

	/*
	 * SCMSGS
	 * @explanation
	 * This message indicates that the fault monitor is about to make a
	 * failover request to the RGM. If the request fails, refer to the
	 * syslog messages that appear after this message.
	 * @user_action
	 * This is an informational message, no user action is required.
	 */
	scds_syslog(LOG_NOTICE, "Issuing a failover request.");

	e = priv_failover_rg(h);

	(void) mutex_unlock(&h->scds_mutex);

	return (e);
}

/*
 * scds_restart_rg()
 * Restart resource group.
 * External API
 */
scha_err_t
scds_restart_rg(scds_handle_t handle)
{
	scds_priv_handle_t		*h;
	scha_err_t				e;

	h = check_handle(handle);

	(void) mutex_lock(&h->scds_mutex);

	/*
	 * SCMSGS
	 * @explanation
	 * This is informational message. We are above to call API function to
	 * request for restart. In case of failure, follow the syslog messages
	 * after this message.
	 * @user_action
	 * No user action is needed.
	 */
	scds_syslog(LOG_NOTICE, "Issuing a restart request.");

	e = priv_restart_rg(h);

	(void) mutex_unlock(&h->scds_mutex);

	return (e);
}


/*
 * scds_restart_resource()
 * Restart a resource by calling STOP and START methods.
 * External API
 */
scha_err_t
scds_restart_resource(scds_handle_t handle)
{
	scds_priv_handle_t		*h;
	scha_err_t				e;

	h = check_handle(handle);

	(void) mutex_lock(&h->scds_mutex);

	/*
	 * SCMSGS
	 * @explanation
	 * The process monitoring facility tried to send a message to the
	 * fault monitor noting that the data service application died. It was
	 * unable to do so.
	 * @user_action
	 * Since some part (daemon) of the application has failed, it would be
	 * restarted. If fault monitor is not yet started, wait for it to be
	 * started by Sun Cluster framework. If fault monitor has been
	 * disabled, enable it by using clresource.
	 */
	scds_syslog(LOG_NOTICE, "Issuing a resource restart request.");

	e = priv_restart_resource(h);

	(void) mutex_unlock(&h->scds_mutex);

	return (e);
}


/*
 * scds_fm_sleep()
 * Wait for the specified interval (timeout). If
 * process failures are detected during this
 * period, action is taken by restarting or
 * failing over the resource.
 * External API
 */
scha_err_t
scds_fm_sleep(scds_handle_t handle, time_t timeout)
{
	scds_priv_handle_t	*h;
	scha_err_t		e;

	h = check_handle(handle);

	(void) mutex_lock(&h->scds_mutex);

	e = priv_fm_sleep(h, timeout);

	(void) mutex_unlock(&h->scds_mutex);

	return (e);
}

/*
 * scds_fm_action()
 * Called after a probe of the service. Computes
 * the failure history and takes appropiate action,
 * which are no action, restart resource or failover RG.
 * External API.
 */
scha_err_t
scds_fm_action(scds_handle_t handle, int probe_status,
    long elapsed_milliseconds)
{
	scds_priv_handle_t	*h;
	scha_err_t		e;

	h = check_handle(handle);

	(void) mutex_lock(&h->scds_mutex);

	e = priv_fm_action(h, probe_status, elapsed_milliseconds);

	(void) mutex_unlock(&h->scds_mutex);

	return (e);
}



/*
 * scds_fm_timesec():
 * Time in seconds since reboot
 * C callable "fdl_timesecs"
 */
hrtime_t
scds_fm_timesec()
{
	hrtime_t conv = (hrtime_t)1e9;

	return (gethrtime() / conv);
}

/*
 * Performance smoothing algorithm is lifted directly from RTT smoothing
 * done by TCP implementation in Solaris. See tcp_set_rto() in tcp.c
 */
/* XXX going from 48-bit to 32-bit precision on assignment? */
#define	SMOOTH(x, y) ((x) = ((x) * 7 + (y)) / 8)

/*
 * create_record(): Allocate a probe_record struct and
 * initialize it properly.
 */
static struct probe_record *
create_record(int status)
{
	struct probe_record *r;

	r = (struct probe_record *)
	    calloc((size_t)1, sizeof (struct probe_record));
	if (r == NULL)
		return (NULL);

	r->p_next = NULL;
	r->p_tstamp = scds_fm_timesec();
	r->p_status = status;
	return (r);
}

/*
 * scds_fm_init(): Allocate a probe_summary struct and properly
 * initialize it. Called by scds_initialize()
 */
scha_err_t
scds_fm_init(scds_priv_handle_t *h)
{
	scds_probe_summary_t	*p;

	p = (scds_probe_summary_t *)
	    calloc((size_t)1, sizeof (scds_probe_summary_t));
	if (p == NULL)
		return (SCHA_ERR_NOMEM);

	p->s_list = NULL;
	p->s_current = 0;
	p->s_elapsed_msec = 0;

	h->probe_summary = p;

	/* Set unix sockets to invalid */
	h->unix_sock = -1;
	h->new_sock = -1;	/* No connection pending */
	return (SCHA_ERR_NOERR);
}

/*
 * scds_fm_close(): Free handle and associated memory
 */
void
scds_fm_close(scds_priv_handle_t *h)
{
	struct probe_record *r = NULL, *q = NULL;
	scds_probe_summary_t *s;

	s = h->probe_summary;

	if (s)
		r = s->s_list;
	while (r) {
		q = r->p_next;
		free(r);
		r = q;
	}
	if (s)
		free(s);
	h->probe_summary = NULL;
}

/*
 * scds_forget_history()
 * Forget the probe history
 * Frees all memory associated with the probes, but
 * keeps the probe summary structure around.
 * Called by fm_action() when a failover attempt
 * fails.
 */
void
scds_forget_history(scds_priv_handle_t *h)
{
	scds_probe_summary_t	*p;
	struct probe_record *r = NULL, *q = NULL;

	p = h->probe_summary;
	if (p)
		r = p->s_list;
	while (r) {
		q = r->p_next;
		free(r);
		r = q;
	}

	p->s_list = NULL;
	p->s_current = 0;
	p->s_elapsed_msec = 0;
}

/*
 * scds_fm_print_probe_record()
 * Debugging utility. syslog() at the specified debug level.
 * Dump the specified probe history record.
 */
static void
scds_fm_print_probe_record(struct probe_record *p, int dlevel)
{
	if (p == NULL) {
		scds_syslog_debug(dlevel,
		    "\tProbe Record is NULL\n");
		return;
	}

	scds_syslog_debug(dlevel, "Timestamp<%u>\tStatus<%u>",
	    p->p_tstamp, p->p_status);
}

/*
 * scds_fm_print_probes()
 * Debugging utility. syslog() at the specified debug level.
 * Dump all the failure history information.
 */
void
scds_fm_print_probes(scds_handle_t handle, int dlevel)
{
	scds_priv_handle_t	*h;
	scds_probe_summary_t *q;
	struct probe_record		*s;

	h = check_handle(handle);

	q = h->probe_summary;
	if (q == NULL) {
		scds_syslog_debug(dlevel, "Probe Summary is NULL\n");
		return;
	}

	scds_syslog_debug(dlevel, "Printing Probe Summary for <%p>\n", q);

	s = q->s_list;
	while (s != NULL) {
		scds_fm_print_probe_record(s, dlevel);
		s = s->p_next;
	}

	scds_syslog_debug(dlevel, "Accumulated status <%u>", q->s_current);
	scds_syslog_debug(dlevel, "Avg probe time <%u>", q->s_elapsed_msec);
}

/*
 * insert_record(): Inserts a record in a linked list at the front.
 * Also walks the list to see if there are old entries and gets
 * rid of them. Updates the summary structure accordingly.
 */
static void
insert_record(scds_priv_handle_t *h, struct probe_record *r)
{
	scds_probe_summary_t	*s;
	struct probe_record *q, *n, *l;

	s = h->probe_summary;

	/* Insert at the head */
	l = s->s_list;
	s->s_list = r;
	r->p_next = l;

	/*
	 * Now walk the list, computing the new sum, delete anything
	 * which is old. This also processes the record we
	 * just added at the front.
	 */
	q = s->s_list;
	s->s_current = 0;
	while (q) {
		n = q->p_next;
		if (n) {
			if ((r->p_tstamp - n->p_tstamp) >
				h->ptable.pt_r->r_sys_props->p_retry_interval) {
				/* Free this entry */
				q->p_next = n->p_next;
				free(n);
				/* q doesn't change */
				continue;
			}
		}
		s->s_current += q->p_status;
		q = n;
	} /* While there are entries on the list */

	scds_syslog_debug(2, "Debug: new %u curr %u",
		s->s_current, r->p_status);
}

/*
 * scds_fm_history(): Look to see if the number of failures has crossed
 * the threshold, and if so, return HISTORY_FAILOVER. If
 * failures have accumulated to SCDS_PROBE_COMPLETE_FAILURE,
 * returns HISTORY_RESTART.
 */
int
scds_fm_history(scds_priv_handle_t *h, int status, ulong_t msec)
{
	struct probe_record *r;
	scds_probe_summary_t	*p;
	static scha_resource_t scha_handle = NULL;
	static int handle_use_count = 0;
	static int restart_count = 0;
	int rc;
	int retry_interval, retry_count;

	p = h->probe_summary;

	r = create_record(status);
	if (r == NULL)
		return (HISTORY_ERROR);

	/*
	 * Now insert this record in front.
	 */
	insert_record(h, r);

	/*
	 * Update the smoothed performance estimate
	 * Use it only if it is a successful probe
	 */
	if (r->p_status == 0)
		if (p->s_elapsed_msec > 0)
			SMOOTH(p->s_elapsed_msec, msec);

	/*
	 * Find out how many times the resource has already restarted
	 * within the last Retry_interval seconds.
	 *
	 * Probes are long lived programs: everytime we do a scha_resource_get,
	 * some memory will be allocated which is freed only when
	 * scha_resource_close is called. If we use our own dsdl handle
	 * and call scha_resource_open with it (after translating it into
	 * the correct scha handle), we run the risk of exausting memory.
	 * Under ideal conditions the probe will run forever - if it
	 * allocates memory (that is never freed) in every iteration, there
	 * is going to be trouble.
	 *
	 * Also note that we cant cache the value of SCHA_NUM_RESOURCE_RESTARTS.
	 * It has to be queried everytime because it is the restart count
	 * in a time window. Its value can change over time.
	 *
	 * So we open a new handle every MAX_HANDLE_USAGE times. If scha_handle
	 * has been set to NULL, it means its time to open a new handle.
	 */
	if (scha_handle == NULL) {
		if (scha_resource_open_zone(h->ptable.pt_zonename,
		    h->ptable.pt_rname, h->ptable.pt_rgname,
		    &scha_handle) != SCHA_ERR_NOERR) {
			scha_handle = NULL;
			handle_use_count = 0;

			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			    "Resource handle could not be obtained. Last known "
			    "value of Num_resource_restarts will be reused");
		}
	}

	/* if we got the resource handle, get the restart count */
	if (scha_handle != NULL) {
		/* Make a temporary copy of the restart count */
		int restart_count_sav = restart_count;

		/*
		 * SCHA_NUM_RESOURCE_RESTARTS is stored in memory and
		 * not the CCR, we wont get a SCHA_ERR_SEQID here
		 *
		 * If pt_zonename is non-NULL and identifies a native zone,
		 * we are dealing with a Global_zone=TRUE resource type
		 * configured in a non-global zone in the base cluster.
		 * In this case, we use the SCHA_NUM_RESOURCE_RESTARTS_ZONE
		 * query tag and pass pt_zonename as the extra ('zonename')
		 * argument.
		 */
		if ((h->ptable.pt_zonename != NULL) && !h->is_zone_cluster) {
			rc = scha_resource_get_zone(h->ptable.pt_zonename,
			    scha_handle, SCHA_NUM_RESOURCE_RESTARTS_ZONE,
			    h->ptable.pt_zonename, &restart_count);
		} else {
			rc = scha_resource_get_zone(h->ptable.pt_zonename,
			    scha_handle, SCHA_NUM_RESOURCE_RESTARTS,
			    &restart_count);
		}
		if ((rc != SCHA_ERR_NOERR) && (rc != SCHA_ERR_SEQID)) {
			/*
			 * Restore the value of restart_count in case
			 * scha_resource_get clobbered it
			 */
			restart_count = restart_count_sav;

			/* Just to be extra safe: reopen the handle next time */
			(void) scha_resource_close_zone(h->ptable.pt_zonename,
			    scha_handle);
			scha_handle = NULL;
			handle_use_count = 0;

			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			    "Value of Num_resource_restarts could not be "
			    "retrieved. Last known value will be reused");
		} else {
			if (++handle_use_count >= MAX_HANDLE_USAGE) {
				/* A new handle should be opened next time */
				(void) scha_resource_close_zone(
				    h->ptable.pt_zonename, scha_handle);
				scha_handle = NULL;
				handle_use_count = 0;
			}
		}
	}
	retry_count = h->ptable.pt_r->r_sys_props->p_retry_count;
	retry_interval = h->ptable.pt_r->r_sys_props->p_retry_interval;
	/*
	 * Now apply some simple rules to compute
	 * NOACTION/RESTART/FAILOVER
	 */

	/*
	 * Remember, p->s_current was computed by adding 'status' to
	 * the sliding window and removing the 'status' from probes
	 * (if any) that are older than Retry_interval.
	 *
	 * Just figure out if this probe's 'status' pushed us
	 * across an 'SCDS_PROBE_COMPLETE_FAILURE' boundary.
	 * With SCDS_PROBE_COMPLETE_FAILURE set to 100, this
	 * check will pass everytime we cross or reach a century
	 * boundary - 100, 200, ...
	 */
	if ((p->s_current % SCDS_PROBE_COMPLETE_FAILURE) < status) {
		/*
		 * Should we fail over?
		 * Negetive values of retry count indicates that an
		 * unlimited number of resource restarts should be
		 * performed.
		 */
		if ((restart_count >= retry_count) &&
		    (retry_count >= 0)) {
			/*
			 * Leave the history alone. If failover succeeds,
			 * the probe will be dead here. If it fails and this
			 * probe is still around, it should call
			 * scds_forget_history()
			 */
			/*
			 * SCMSGS
			 * @explanation
			 * A request to restart a resource is denied because
			 * the resource has already been restarted Retry_count
			 * times within the past Retry_interval seconds
			 * @user_action
			 * No action required. If desired, use clresourcegroup
			 * to change the Failover_mode setting.
			 */
			scds_syslog(LOG_ERR,
			    "A resource restart attempt on resource %s "
			    "in resource group %s has been blocked "
			    "because the number of restarts within "
			    "the past Retry_interval (%d seconds) "
			    "would exceed Retry_count (%d)",
			    h->ptable.pt_rname, h->ptable.pt_rgname,
			    retry_interval, retry_count);
			return (HISTORY_FAILOVER);
		}

		/*
		 * If the caller restarts the resource successfully,
		 * probe would have been stopped/restarted by the RGM.
		 * So everytime after a restart, we start with a clean
		 * history. The RGM increments Num_resource_restarts for
		 * us in that case.
		 */
		return (HISTORY_RESTART);
	}

	/*
	 * Failover/restart criteria was not met. It is guaranteed that
	 * 0 <= status < SCDS_PROBE_COMPLETE_FAILURE.
	 * Take no action. But inform the caller if the resource is not
	 * 100% fit; it should be marked degraded.
	 */
	return (status ? HISTORY_NOACTION_DEG : HISTORY_NOACTION);
}

/*
 * exit_probe_with_failure_status:
 * This is a utility routine, which is called from priv_fm_action and
 * scds_fm_proc_failed. It sets the status of the resource, and makes the
 * probe quit by calling scds_pmf_stop.
 */
static scha_err_t
exit_probe_with_failure_status(scds_priv_handle_t *h)
{
	scha_err_t err = SCHA_ERR_NOERR;
	scds_prop_table_t  *ptable;

	ptable = &(h->ptable);

	ptable->pt_r->r_status = SCHA_RSSTATUS_FAULTED;
	(void) scha_resource_setstatus_zone(ptable->pt_rname,
	    ptable->pt_rgname, ptable->pt_zonename, ptable->pt_r->r_status,
	    "Application faulted, but not restarted. Probe quitting ...");

	err = scds_pmf_stop((scds_handle_t)h, SCDS_PMF_TYPE_MON,
	    SCDS_PMF_SINGLE_INSTANCE, SIGKILL,
	    (long)scds_get_rs_monitor_stop_timeout((scds_handle_t)h));

	if (err != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * The failover_enabled is set to false. Therefore, an attempt
		 * was made to make the probe quit using PMF, but the attempt
		 * failed.
		 * @user_action
		 * This is an informational message, no user action is needed.
		 */
		scds_syslog(LOG_ERR,
		    "The attempt to kill the probe failed. "
		    "The probe left as-is.");
		return (err);
	}

	/* Should not reach here */
	return (err);
}

/*
 * priv_fm_action()
 * Calls scds_fm_history() to decide what action to take
 * on behalf of the data service.  This method should be
 * called after a normal probe completes, regardless if the probe
 * is successful or not.
 * Calls priv_failover_rg() to perform failover and
 * priv_restart_resource() to restart the resource.
 * Calls scha_resource_setstatus() to set resource status
 * appropriately.
 */
static scha_err_t
priv_fm_action(scds_priv_handle_t *h, int probe_status, long elapsed_msec)
{
	int		rc;
	scha_err_t	evalaction = SCHA_ERR_NOERR;	/* return code */
	int		probe_timeout;
	int		action;
	scds_prop_table_t  *ptable;
	boolean_t failover_enabled = B_TRUE;
	boolean_t is_frozen = B_FALSE;
	scha_failover_mode_t fom = SCHA_FOMODE_NONE;
	static boolean_t is_fm_restart = B_TRUE;
	char *error_string;
	uint_t num_nodes_in_nodelist;

	if (h == NULL)
		return (SCHA_ERR_INVAL);
	ptable = &(h->ptable);

	failover_enabled = ptable->pt_r->r_ext_props->x_failover_enabled;
	fom = ptable->pt_r->r_sys_props->p_failover_mode;
	num_nodes_in_nodelist = ptable->pt_rg->rg_nodelist->array_cnt;
	/* RG nodelist will not have is_ALL_VAL set */

	probe_timeout = h->ptable.pt_r->r_ext_props->x_probe_timeout;
	if ((probe_timeout != SCDS_PROP_UNSET) &&
	    ((elapsed_msec / 1000.0) > (0.5 * probe_timeout))) {
		/*
		 * SCMSGS
		 * @explanation
		 * This message indicates that the probe time of the monitor
		 * exceeded the probe timeout threshold(50%) for the resource.
		 * @user_action
		 * This message suggests that the Probe_timeout extension
		 * property for this resource might be set too low. If the
		 * indicated probe execution time is typical for your level of
		 * system load, consider increasing the value of Probe_timeout
		 * to avoid extraneous restarts or failovers that would be
		 * triggered by exceeding the configured probe timeout.
		 */
		scds_syslog(LOG_NOTICE,
		    "Monitor probe time of %0.2f seconds is "
		    "%0.2f percent of Probe timeout.",
		    (elapsed_msec / 1000.0),
		    ((elapsed_msec / (probe_timeout * 1000.0)) * 100));
	}

	if (probe_status > SCDS_PROBE_COMPLETE_FAILURE &&
	    probe_status != SCDS_PROBE_IMMEDIATE_FAILOVER) {
		probe_status = SCDS_PROBE_COMPLETE_FAILURE;
	}

	/*
	 * No restart of data service should be attempted if
	 * RGM has suspended timeout. The whole reason the monitor
	 * has timedout is because the disks are being switched
	 * over. In that case, attempting to restart the application
	 * is not going to help. It is going to make matters worse.
	 *
	 * Check if the RG is frozen. If it is,
	 * no further action is taken. We do it even before
	 * calling scds_fm_history() so that the failure
	 * history is not updated. Also, we do NOT make this
	 * check if the probe was successful (probe_status == 0),
	 * that serves two purposes.
	 * 1. We don't keep calling scha_resourcegroup_get() even if
	 *	the probe is successful.
	 * 2. If the probe IS successful, we want to remember
	 * that happy outcome regardless of disk status.
	 */

	if (probe_status != 0) {
		evalaction = scha_resourcegroup_get_zone(h->ptable.pt_zonename,
		    h->rg_handle, SCHA_RG_IS_FROZEN, &is_frozen);
		if (evalaction != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR, "Failed to retrieve "
			    "the resource group property %s: %s.",
			    SCHA_RG_IS_FROZEN, scds_error_string(evalaction));
		} else if (is_frozen == B_TRUE) {
			return (SCHA_ERR_NOERR);
		}
	}

	if (probe_status == SCDS_PROBE_IMMEDIATE_FAILOVER) {
		/*
		 * If the probe_status is SCDS_PROBE_IMMEDIATE_FAILOVER,
		 * the probe wants us to failover immediately. Therefore,
		 * we set action to HISTORY_FAILOVER. However, we still
		 * record this failure in the failure history so that if
		 * failover fails, or if failover_enabled is set to false,
		 * this failure is properly recorded. Further, the value
		 * recorded in the failure history is
		 * SCDS_PROBE_COMPLETE_FAILURE.
		 */

		(void) scds_fm_history(h, SCDS_PROBE_COMPLETE_FAILURE,
		    (ulong_t)elapsed_msec);
		action = HISTORY_FAILOVER;
		/*
		 * SCMSGS
		 * @explanation
		 * An immediate failover will be performed.
		 * @user_action
		 * This is an informational message, no user action is needed.
		 */
		scds_syslog(LOG_ERR,
		    "The probe has requested an immediate failover. "
		    "Attempting to failover this resource group subject "
		    "to other property settings.");
	} else {
		action = scds_fm_history(h, probe_status,
		    (ulong_t)elapsed_msec);
	}

	if (probe_status == 0) {
		/*
		 * If the probe_status is 0, we still want to record this
		 * value of the "failure" in the failure history along with
		 * its time stamp, so that it can be used in future restart/
		 * failover computations.
		 * However, if failover_enabled is set to false, an
		 * interesting scenario may arise in which the application
		 * would be restarted even if the current value of probe_status
		 * is 0.  This can happen, for example, if the application
		 * process tree dies twice (i.e. SCDS_PROBE_COMPLETE_FAILURE)
		 * and then comes up OK the third time. If the retry_count is
		 * set to 2, and retry_interval is set to 5 minutes (the
		 * default settings), then without this check, DSDL will keep
		 * restarting the resource until the retry_interval window
		 * has passed over those two intial failures.
		 * Notice that such a situation would never occur if
		 * failover_enabled was set to true. This is because once
		 * retry_count is exceeded, the application is failed over
		 * and all previous failure history is forgotten. For that
		 * reason, this "if" statement only checks for probe_status
		 * being zero.
		 */
		action = HISTORY_NOACTION;
	}

	if (action == HISTORY_FAILOVER) {
		/* if configurations don't allow failover, exit probe */

		if (fom == SCHA_FOMODE_RESTART_ONLY) {
			error_string =
			    "the Failover mode property is set to RESTART_ONLY";
		} else if (fom == SCHA_FOMODE_LOG_ONLY) {
			error_string =
			    "the Failover mode property is set to LOG_ONLY";
		} else if (failover_enabled == B_FALSE) {
			error_string =
			    "the Failover_enabled extension property is FALSE";
		} else if (num_nodes_in_nodelist == 1) {
			error_string =
			    "Nodelist doesn't have any other node for failover";
		} else {
			error_string = NULL;
		}

		if (error_string != NULL) {
			/*
			 * SCMSGS
			 * @explanation
			 * Based on the restart history, the fault monitor
			 * decided for application failover. However the
			 * application is not being failed over because of the
			 * specified reason. The probe is quitting because it
			 * does not have any application to monitor.
			 * @user_action
			 * The application or the monitor is not working
			 * properly. Correct the problem and restart the
			 * application and/or its monitor.
			 */
			scds_syslog(LOG_ERR, "The action to be taken as "
			    "determined by scds_fm_action is failover. However "
			    "the application is not being failed over because "
			    "%s. The application is left as-is. "
			    "Probe quitting ...", error_string);
			evalaction = exit_probe_with_failure_status(h);
			return (evalaction);
		}
	}

	switch (action) {
	case HISTORY_NOACTION:
		if (ptable->pt_r->r_status != SCHA_RSSTATUS_OK ||
		    is_fm_restart) {
			ptable->pt_r->r_status = SCHA_RSSTATUS_OK;
			(void) scha_resource_setstatus_zone(ptable->pt_rname,
				ptable->pt_rgname, ptable->pt_zonename,
				ptable->pt_r->r_status,
				"Service is online.");
			is_fm_restart = B_FALSE;
		}

		evalaction = SCHA_ERR_NOERR;
		break;

	case HISTORY_NOACTION_DEG:
		/* Just set the status and go away */
		if (ptable->pt_r->r_status != SCHA_RSSTATUS_DEGRADED) {
			ptable->pt_r->r_status = SCHA_RSSTATUS_DEGRADED;
			(void) scha_resource_setstatus_zone(ptable->pt_rname,
				ptable->pt_rgname, ptable->pt_zonename,
				ptable->pt_r->r_status,
				"Service is degraded.");
		}

		evalaction = SCHA_ERR_NOERR;
		break;

	case HISTORY_RESTART:
		if (ptable->pt_r->r_status != SCHA_RSSTATUS_DEGRADED) {
			ptable->pt_r->r_status = SCHA_RSSTATUS_DEGRADED;
			(void) scha_resource_setstatus_zone(ptable->pt_rname,
				ptable->pt_rgname, ptable->pt_zonename,
				ptable->pt_r->r_status,
				"Service is degraded.");
		}

		/*
		 * SCMSGS
		 * @explanation
		 * This message indicates that the fault monitor is about to
		 * request a resource restart because of probe failures. If
		 * the request fails, refer to the syslog messages that appear
		 * after this message.
		 * @user_action
		 * This is an informational message; no user action is
		 * required.
		 */
		scds_syslog(LOG_NOTICE,
		    "Issuing a resource restart request "
		    "because of probe failures.");

		rc = priv_restart_resource(h);
		if ((rc == SCHA_ERR_NOERR) || (rc == SCHA_ERR_CHECKS)) {
			/*
			 * SCHA_ERR_CHECKS means the RG froze up sometime
			 * after the scha_control(CHECK_RESTART) call.
			 * In this case, we have already updated our
			 * failure history. There's not much else we can
			 * do for now. Also, priv_restart_resource will
			 * syslog for this case.
			 */
			evalaction = SCHA_ERR_NOERR;
			break;
		}
		/* Unable to restart resource. */
		/*
		 * SCMSGS
		 * @explanation
		 * Restart attempt of the dataservice has failed.
		 * @user_action
		 * Check the sylog messages that are occurred just before this
		 * message to check whether there is any internal error. In
		 * case of internal error, contact your Sun service provider.
		 * Otherwise, any of the following situations may have
		 * happened. 1) Check the Start_timeout and Stop_timeout
		 * values and adjust them if they are not appropriate. 2) This
		 * might be the result of lack of the system resources. Check
		 * whether the system is low in memory or the process table is
		 * full and take appropriate action.
		 */
		scds_syslog(LOG_ERR, "Failed to restart the service.");

		/* do not attempt failover in following cases */
		if (!failover_enabled || num_nodes_in_nodelist == 1) {
			evalaction = SCHA_ERR_FAIL;
			break;
		}

		/*lint -fallthrough */ /* attempt giveover if restart fails */
	case HISTORY_FAILOVER:
		if (ptable->pt_r->r_status != SCHA_RSSTATUS_FAULTED) {
			ptable->pt_r->r_status = SCHA_RSSTATUS_FAULTED;
			(void) scha_resource_setstatus_zone(ptable->pt_rname,
				ptable->pt_rgname, ptable->pt_zonename,
				ptable->pt_r->r_status,
				"Service has failed.");
		}

		/*
		 * SCMSGS
		 * @explanation
		 * This message indicates that the fault monitor is about to
		 * make a failover request because of probe failures. If the
		 * request fails, refer to the syslog messages that appear
		 * after this message.
		 * @user_action
		 * This is an informational message; no user action is
		 * required.
		 */
		scds_syslog(LOG_NOTICE,
		    "Issuing a failover request "
		    "because of probe failures.");

		rc = priv_failover_rg(h);
		if (rc != SCHA_ERR_NOERR) {
			/*
			 * SCMSGS
			 * @explanation
			 * LogicalHostname resource was unable to register
			 * with IPMP for status updates.
			 * @user_action
			 * Most likely it is result of lack of system
			 * resources. Check for memory availability on the
			 * node. Reboot the node if problem persists.
			 */
			scds_syslog(LOG_ERR, "Failover attempt has failed.");
			scds_forget_history(h);
			evalaction = SCHA_ERR_FAIL;
		} else {
			/* should never get here! */
			scds_syslog(LOG_ERR,
			    "INTERNAL ERROR: %s.",
			    "Failover succeeded. Should never get here");
			evalaction = SCHA_ERR_INTERNAL;

			/* Exit Now: PMF would restart us */
			exit(2);
		}
		break;

	default:
		evalaction = SCHA_ERR_INVAL;
		break;

	}

	scds_syslog_debug(SCDS_DBG_HIGH,
	    "Returning <%d> from evaluate after probe\n", evalaction);

	return (evalaction);
}

/*
 * scds_fm_proc_failed()
 * Handle a process failure event. Consults
 * scds_fm_history() to decide on failover vs restart
 * action.
 */
static scha_err_t
scds_fm_proc_failed(scds_priv_handle_t *h)
{
	int			action;
	scha_err_t		rc = SCHA_ERR_NOERR;
	scds_prop_table_t	*ptable;
	boolean_t		failover_enabled = B_TRUE;
	scha_failover_mode_t	fom = SCHA_FOMODE_NONE;
	static scha_resourcegroup_t scha_rg_handle = NULL;
	boolean_t		rg_suspended;

	ptable = &(h->ptable);

	/* The entire process group has died, so update the status */
	(void) scha_resource_setstatus_zone(ptable->pt_rname, ptable->pt_rgname,
	    ptable->pt_zonename,
	    SCHA_RSSTATUS_FAULTED, "Service daemon not running.");
	ptable->pt_r->r_status = SCHA_RSSTATUS_FAULTED;

	action = scds_fm_history(h, SCDS_PROBE_COMPLETE_FAILURE, (ulong_t)0);

	failover_enabled = ptable->pt_r->r_ext_props->x_failover_enabled;

	if (failover_enabled == B_FALSE && action == HISTORY_FAILOVER) {
		/*
		 * SCMSGS
		 * @explanation
		 * The application is not being restarted because
		 * failover_enabled is set to false and the number of restarts
		 * has exceeded the retry_count. The probe is quitting because
		 * it does not have any application to monitor.
		 * @user_action
		 * This is an informational message, no user action is needed.
		 */
		scds_syslog(LOG_ERR,
		    "The application process tree has died and the action "
		    "to be taken as determined by scds_fm_history is to "
		    "failover. However the application is not being "
		    "failed over because the failover_enabled extension "
		    "property is set to false. The application is left "
		    "as-is. Probe quitting ...");
		rc = exit_probe_with_failure_status(h);
		return (rc);
	}

	fom = ptable->pt_r->r_sys_props->p_failover_mode;

open_rg:
	/* Open a handle to get the RG Property */
	rc = scha_resourcegroup_open_zone(ptable->pt_zonename,
	    ptable->pt_rgname, &scha_rg_handle);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to open the resource group <%s> "
		    "handle: <%s>.", ptable->pt_rgname, scds_error_string(rc));
		return (1);
	}

	rc = scha_resourcegroup_get_zone(ptable->pt_zonename,
	    scha_rg_handle, SCHA_RG_SUSP_AUTO_RECOVERY,
	    &rg_suspended);
	if (rc == SCHA_ERR_SEQID) {	/* retry */
		(void) scha_resourcegroup_close_zone(ptable->pt_zonename,
		    scha_rg_handle);
		scha_rg_handle = NULL;
		goto open_rg;
	}
	if (rc != SCHA_ERR_NOERR) {
		/*
		 * if we fail to retrieve the value of the RG property
		 * SCHA_RG_SUSP_AUTO_RECOVERY we assume it to be B_FALSE
		 */
		scds_syslog(LOG_ERR, "Failed to retrieve the resource group "
		    "property %s: %s.", SCHA_RG_SUSP_AUTO_RECOVERY,
		    scds_error_string(rc));
		rg_suspended = B_FALSE;
	}
	if (rg_suspended == B_TRUE) {
		/*
		 * If the RG property SCHA_RG_SUSP_AUTO_RECOVERY is set to TRUE
		 * which means that the RG is Suspended (not being monitored)
		 * then the failover mode of the resource will be overridden to
		 * Log only.
		 */
		fom = SCHA_FOMODE_LOG_ONLY;
	}

	(void) scha_resourcegroup_close_zone(
	    ptable->pt_zonename, scha_rg_handle);

	if (action == HISTORY_FAILOVER) {
		/* Attempt a failover */

		/*
		 * SCMSGS
		 * @explanation
		 * This message indicates that the fault monitor is about to
		 * make a failover request because the application exited. If
		 * the request fails, refer to the syslog messages that appear
		 * after this message.
		 * @user_action
		 * This is an informational message.
		 */
		scds_syslog(LOG_NOTICE,
		    "Issuing a failover request "
		    "because the application exited.");

		rc = priv_failover_rg(h);
		if (rc != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR, "Failover attempt has failed.");
			/*
			 * if the failure is due to the Failover_mode setting
			 * for the resource, update the status message of the
			 * resource and quit, as staying around will not
			 * change anything
			 */
			if ((rc == SCHA_ERR_CHECKS) &&
			    ((fom == SCHA_FOMODE_RESTART_ONLY) ||
			    (fom == SCHA_FOMODE_LOG_ONLY))) {
				rc = exit_probe_with_failure_status(h);
				return (rc);
			} else {
				scds_forget_history(h);
			}
		} else {
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				"Failover succeeded. Should never get here");
			/* Exit now. PMF would restart us */
			exit(2);
		}
	}
	/*
	 * Either the failover has failed or not ready to failover yet
	 * in any case, do restart the application.
	 */

	/*
	 * SCMSGS
	 * @explanation
	 * This message indicates that the fault monitor is about to request a
	 * resource restart because the application exited. If the request
	 * fails, refer to the syslog messages that appear after this message.
	 * @user_action
	 * This is an informational message.
	 */
	scds_syslog(LOG_NOTICE,
	    "Issuing a resource restart request "
	    "because the application exited.");

	rc = priv_restart_resource(h);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to restart the service.");
		/*
		 * if the failure is due to the Failover_mode setting
		 * for the resource, update the status message of the resource
		 * and quit, as staying around will not change anything
		 */
		if ((rc == SCHA_ERR_CHECKS) &&
		    ((fom == SCHA_FOMODE_RESTART_ONLY) ||
		    (fom == SCHA_FOMODE_LOG_ONLY))) {
			rc = exit_probe_with_failure_status(h);
		}
	}

	scds_syslog_debug(SCDS_DBG_HIGH,
	    "Returning <%d> from evaluate after pmf action\n", rc);

	return (rc);
}


/*
 * scds_fm_perf(): Returns the smoothed performance value, suitable
 * for passing it to a Response Time Monitor.
 */

ulong_t
scds_fm_get_perfval(scds_priv_handle_t *h)
{
	return (h->probe_summary->s_elapsed_msec);
}


/*
 * Control socket related utilities. This facility provides a way to
 * communicate with the running fault monitor and get response from it.
 * From the client side, all is needed is a scds_fm_sendmsg() call which
 * sends a message to the monitor and gets an integer response from it.
 * On the server (fault monitor) side, it is supposed to do a
 * scds_fm_waitsock(), which returns 0 if there is a message pending, it
 * then needs to do a scds_fm_getmsg() to get the messages and then do
 * a scds_fm_sendreply() to send an integer to the client.
 */

/*
 * scds_fm_sockname(): The UNIX pathname of the UNIX socket, given
 * the RG name and the R name.
 */
char *
scds_fm_sockname(char *rg_name, char *rs_name)
{
	static char sockname[SCDS_ARRAY_SIZE];
	int err;

	err = snprintf(sockname, sizeof (sockname), "%s/.%s_%s.sock",
	    SCDS_TMPDIR, rg_name, rs_name);
	if (err == -1) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		    "Failed to create socket name for fault monitor: "
		    "Output error encountered");
		return (NULL);
	}

	return (sockname);
}

/*
 * scds_fm_opensock(): Opens a UNIX domain socket on which the
 * fault monitor waits for UNIX domain connections. The entities
 * that could potentially communicate with it are, pmf_action script,
 * start method (if some processing is backgrounded, fault monitoring should
 * not start until a proper startup of the service has taken place), and
 * fault monitor stop method (for orderly shutdown).
 * returns a UNIX domain socket descriptor.
 */

int
scds_fm_opensock(char *rg, char *rs)
{
	int sd;
	int n;
	char *sockname;
	struct sockaddr_un target;

	sockname = scds_fm_sockname(rg, rs);

	sd = socket(AF_UNIX, SOCK_STREAM, 0);
	if (sd < 0) {
		/*
		 * suppress lint error because errno.h prototype
		 * is missing void arg
		 */
		scds_syslog(LOG_ERR,
		    "Failed to create socket: %s.",
		    strerror(errno));	/*lint !e746 */
		return (-1);
	}

	bzero((char *)&target, sizeof (target));
	target.sun_family = AF_UNIX;
	(void) strncpy(target.sun_path, sockname, sizeof (target.sun_path));
	(void) unlink(sockname);

	n = 1;
	(void) setsockopt(sd, SOL_SOCKET, SO_REUSEADDR,
		(char *)&n, (int)sizeof (int));
	if (bind(sd, (struct sockaddr *)&target, (int)sizeof (target)) < 0) {
		scds_syslog_debug(SCDS_DBG_HIGH,
		    "Failed to bind socket to %s: %s.",
		    sockname, strerror(errno));
		(void) close(sd);
		return (-1);
	}
	(void) listen(sd, 5);
	return (sd);
}

/*
 * scds_fm_closesock(): Closes the fault monitor control socket and
 * removes the socket file from the file system.
 * Called by the fault monitor before exiting cleanly.
 *
 * This function used to be called from priv_restart_resource
 * but is no longer called by anyone. It has been left alone in case
 * someday the probe actually has to exit cleanly.
 */

scha_err_t
scds_fm_closesock(scds_priv_handle_t *h)
{
	char *sockname;

	(void) close(h->unix_sock);
	h->unix_sock = -1;
	(void) close(h->new_sock);
	h->new_sock = -1;
	sockname = scds_fm_sockname(h->ptable.pt_rgname, h->ptable.pt_rname);
	(void) unlink(sockname);
	return (SCHA_ERR_NOERR);
}

/*
 * priv_fm_sleep(): Wait for the specified period of time on the UNIX
 * domain socket for connections to arrive. If a connections does arrive,
 * process death is handled by calling scds_fm_proc_failed().
 */

static scha_err_t
priv_fm_sleep(scds_priv_handle_t *h, time_t timeout)
{
	fd_set fdset;
	struct timeval t;
	char	msg[2048];
	scha_err_t	rc = SCHA_ERR_NOERR;

	if (h->unix_sock < 0) {
		h->unix_sock = scds_fm_opensock(h->ptable.pt_rgname,
	    h->ptable.pt_rname);
		if (h->unix_sock < 0) {
			return (SCHA_ERR_INTERNAL);
		}
	}
	scds_syslog_debug(2, "unix_sock = %d", h->unix_sock);
	/*
	 * The lint exception is added for the usage of FD_ZERO for the
	 * following reason. On s8u6, the usage of FD_ZERO below is
	 * correct. But, for s9, the usage results in a lint error.
	 * The lint error Info(792) is described as "void cast
	 * of void expression". This lint error shows up because of
	 * a change to the definition of the FD_ZERO macro in s9 to
	 * the following.
	 * #define FD_ZERO(__p) (void) memset((__p), 0, sizeof (*(__p)))
	 */
	FD_ZERO(&fdset);	/*lint !e792 */
	/*
	 * FD_SET() macro is broken w.r.t. lint
	 */
	FD_SET((uint_t)h->unix_sock, &fdset); /*lint !e737 !e713 */

	t.tv_sec = timeout;
	t.tv_usec = 0;

	if (select(h->unix_sock+1, &fdset, NULL, NULL, &t) > 0) {
		/*
		 * Read the message from action script.
		 * fm_getmsg() fails only for Solaris reasons.
		 * Return to caller if error. Presumably, the
		 * fault monitor would discover that the
		 * process is dead and restart.
		 * XXX If the monitor is not doing anything else, we
		 * cannot depend upon it. Perhaps try to failover the RG?
		 */
		rc = scds_fm_getmsg(h, msg, (int)sizeof (msg));
		if (rc != SCHA_ERR_NOERR) {
			return (rc);
		}

		/* scds_fm_proc_failed() handles process failures */
		rc = scds_fm_proc_failed(h);
		if (rc != SCHA_ERR_NOERR) {
			return (rc);
		}
	}
	return (SCHA_ERR_NOERR);
}

/*
 * scds_fm_getmsg(): Get a message from the control socket. The caller is
 * supposed to have already done a waitsock() which returned 0 because there
 * was a connection pending on the socket. The message is returned in a
 * static buffer (thus we can handle only one active message at a time).
 */

scha_err_t
scds_fm_getmsg(scds_priv_handle_t *h, char *buf, int size)
{
	struct sockaddr_un from;
	ssize_t n;
	int fromlen;
	size_t sz;

	fromlen = sizeof (from);
	h->new_sock = accept(h->unix_sock, (struct sockaddr *)&from,
	    &fromlen);
	if (h->new_sock < 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * While determining the health of the data service, fault
		 * monitor is failed to communicate with the process monitor
		 * facility.
		 * @user_action
		 * This is internal error. Save /var/adm/messages file and
		 * contact your authorized Sun service provider. For more
		 * details about error, check the syslog messges.
		 */
		scds_syslog(LOG_ERR,
		    "Failed to accept connection on socket: %s.",
		    strerror(errno));

		h->new_sock = -1;
		return (SCHA_ERR_INVAL);
	}

	sz = (uint_t)(size - 1);
	if ((n = recv(h->new_sock, buf, sz, 0)) < 1) {
		/*
		 * SCMSGS
		 * @explanation
		 * While determining the health of the data service, fault
		 * monitor is failed to communicate with the process monitor
		 * facility.
		 * @user_action
		 * This is internal error. Save /var/adm/messages file and
		 * contact your authorized Sun service provider. For more
		 * details about error, check the syslog messges.
		 */
		scds_syslog(LOG_ERR,
		    "Failed to communicate: %s.",
		    strerror(errno));
		(void) close(h->new_sock);
		h->new_sock = -1;
		return (SCHA_ERR_TIMEOUT);
	}
	buf[n] = 0;

	return (SCHA_ERR_NOERR);
}

/*
 * scds_fm_sendreply(): Send an integer value to the client
 * which sent us the message.
 */

scha_err_t
scds_fm_sendreply(scds_priv_handle_t *h, int response)
{
	ssize_t n;
	char reply_buf[32];

	if (h->new_sock < 0) {
		return (SCHA_ERR_INVAL);
	}
	(void) sprintf(reply_buf, "%d", response);
	n = send(h->new_sock, reply_buf, strlen(reply_buf) + 1, 0);
	(void) close(h->new_sock);
	h->new_sock = -1;

	if (n == -1)
		return (SCHA_ERR_TIMEOUT);
	else
		return (SCHA_ERR_NOERR);
}

/*
 * scds_fm_sendmsg(): Client side function to communicate with the
 * fault monitor of a resource. Send a message to the FM, which
 * replies with a integer return code. This function returns that
 * code to client. -1 for any errors.
 */
int
scds_fm_sendmsg(char *rg_name, char *rs_name, char *msg)
{
	int rc = -1, sd;
	size_t size;
	ssize_t n;
	char *sockname, buf[32];
	struct sockaddr_un target;

	size = strlen(msg);

	sockname = scds_fm_sockname(rg_name, rs_name);

	sd = socket(AF_UNIX, SOCK_STREAM, 0);
	if (sd < 0) {
		scds_syslog(LOG_ERR,
		    "Failed to create socket: %s.", strerror(errno));
		return (rc);
	}
	target.sun_family = AF_UNIX;
	if (strcpy(target.sun_path, sockname) == NULL) {
		scds_syslog(LOG_ERR, "Out of memory.");
		return (-1);
	}

	if (connect(sd, (struct sockaddr *)&target,
	    (int)sizeof (target)) < 0) {
		goto finished;
	}
	if (send(sd, msg, size, 0) != (int)size) {
		if (errno != ETIMEDOUT && errno != EINTR)
			rc = 0;
		goto finished;
	}
	/*
	 * Wait for a return value from monitor. If the
	 * monitor is stopped because we just triggered
	 * a failover, then recv() syscall would return
	 * without reading anything. We should NOT
	 * restart the application in that case.
	 */
	if ((n = recv(sd, buf, sizeof (buf), 0)) < 1) {
		rc = 1;
		goto finished;
	}

	/*
	 * We recieved something. It would most likely be
	 * a 0, telling us to restart. Parse the returned
	 * value and convert it into an integer.
	 */
	buf[n] = 0;
	rc = atoi(buf);

finished:
	(void) close(sd);
	return (rc);
}

/*
 * priv_restart_resource()
 * Calls scha_control with SCHA_RESOURCE_RESTART tag to request the rgmd to
 * execute STOP followed by START method.
 * Called by fm_action() when it needs to restart
 * a resource.
 */
static scha_err_t
priv_restart_resource(scds_priv_handle_t *h)
{
	scds_prop_table_t	*ptable;
	scha_err_t	e = SCHA_ERR_NOERR;

	ptable = &(h->ptable);

	/* Request RGM to restart this resource */
	e = scha_control_zone(SCHA_RESOURCE_RESTART, ptable->pt_rgname,
	    ptable->pt_rname, ptable->pt_zonename);

	if (e == SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * This message indicates that the RGM successfully restarted
		 * the resource.
		 * @user_action
		 * This is an informational message, no user action is
		 * required.
		 */
		scds_syslog(LOG_INFO, "Successfully restarted service.");
	} else {
		if (e != SCHA_ERR_CHECKS) {
			/*
			 * don't log a trace if the error is SCHA_ERR_CHECKS
			 * Probe may fail repeatedly with Failover_mode setting
			 * preventing giveover [or restart], and we don't want
			 * to fill up syslog
			 */
			/*
			 * SCMSGS
			 * @explanation
			 * This message indicated that the rgm didn't process
			 * a restart request, most likely due to the
			 * configuration settings.
			 * @user_action
			 * This is an informational message.
			 */
			scds_syslog(LOG_ERR, "Restart operation failed: %s.",
			    scds_error_string(e));
		}
	}

	return (e);
}

/*
 * scds_svc_wait()
 * Wait for a application daemon to die for the
 * specified period. If ANY application (started via
 * scds_pmf_start()) dies during this period, we
 * return SCHA_ERR_FAIL, else it returns SCHA_ERR_NOERR.
 * External API
 */
scha_err_t
scds_svc_wait(scds_handle_t handle, time_t timeout)
{
	scds_priv_handle_t	*h;
	scds_prop_table_t	*pt;
	fd_set	fdset;
	struct timeval	t;
	char    msg[2048];
	int		action;
	scha_err_t	e = SCHA_ERR_NOERR;

	h = check_handle(handle);
	pt = &(h->ptable);
	(void) mutex_lock(&(h->scds_mutex));
	if (h->unix_sock < 0) {
		h->unix_sock = scds_fm_opensock(pt->pt_rgname, pt->pt_rname);
		if (h->unix_sock < 0) {
			scds_syslog(LOG_ERR, "Socket creation failed: %s.",
				strerror(errno));
			e = SCHA_ERR_INTERNAL;
			goto finish;
		}
	}
wait_again:
	/*
	 * The lint exception is added for the usage of FD_ZERO for the
	 * following reason. On s8u6, the usage of FD_ZERO below is
	 * correct. But, for s9, the usage results in a lint error.
	 * The lint error Info(792) is described as "void cast
	 * of void expression". This lint error shows up because of
	 * a change to the definition of the FD_ZERO macro in s9 to
	 * the following.
	 * #define FD_ZERO(__p) (void) memset((__p), 0, sizeof (*(__p)))
	 */
	FD_ZERO(&fdset);	/*lint !e792 */
	/*
	 * FD_SET() macro is broken w.r.t. lint
	 */
	FD_SET((uint_t)h->unix_sock, &fdset); /*lint !e737 !e713 */

	t.tv_sec = timeout;
	t.tv_usec = 0;
	if (select(h->unix_sock+1, &fdset, NULL, NULL, &t) > 0) {
		e = scds_fm_getmsg(h, msg, (int)sizeof (msg));
		if (e != SCHA_ERR_NOERR) {
			goto finish;
		}
		action = scds_fm_history(h, SCDS_PROBE_COMPLETE_FAILURE,
			(ulong_t)0);
		if (action == HISTORY_FAILOVER) {
			(void) scds_fm_sendreply(h, 1);
			e = SCHA_ERR_FAIL;
			goto finish;
		}
		(void) scds_fm_sendreply(h, 0);
		goto wait_again;
	}

finish:
	(void) mutex_unlock(&(h->scds_mutex));
	return (e);
}
/*
 * priv_restart_rg()
 * Wrapper around scha_control(RESTART)
 * returns error code from scha_control()
 */
static scha_err_t
priv_restart_rg(scds_priv_handle_t *h)
{
	scha_err_t e;
	scds_prop_table_t	*ptable;

	ptable = &(h->ptable);
	if (ptable == NULL) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		    "NULL property table passed to scha-api restart");
		return (SCHA_ERR_INTERNAL);
	}

	if (ptable->pt_rgname == NULL) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		    "Unable to restart the service: resource group "
		    "name is NULL ");
		return (SCHA_ERR_INTERNAL);
	}

	if (ptable->pt_rname == NULL) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		    "Unable to restart the service: resource "
		    "name is NULL ");
		return (SCHA_ERR_INTERNAL);
	}

	e = scha_control_zone(SCHA_RESTART,
	    ptable->pt_rgname, ptable->pt_rname, ptable->pt_zonename);

	if (e == SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * This message indicates that the rgm restarted a resource
		 * group, as expected.
		 * @user_action
		 * This is an informational message.
		 */
		scds_syslog(LOG_INFO, "Successfully restarted "
		    "resource group.");
	} else {
		if (e != SCHA_ERR_CHECKS) {
			/*
			 * don't log a trace if the error is SCHA_ERR_CHECKS
			 * Probe may fail repeatedly with Failover_mode setting
			 * preventing giveover [or restart], and we don't want
			 * to fill up syslog
			 */
			/*
			 * SCMSGS
			 * @explanation
			 * Restart attempt of the dataservice has failed.
			 * @user_action
			 * Check the sylog messages that are occurred just
			 * before this message to check whether there is any
			 * internal error. In case of internal error, contact
			 * your Sun service provider. Otherwise, any of the
			 * following situations may have happened.
			 *
			 * 1) Check the Start_timeout and Stop_timeout values
			 * and adjust them if they are not appropriate. 2)
			 * This might be the result of lack of the system
			 * resources. Check whether the system is low in
			 * memory or the process table is full and take
			 * appropriate action.
			 */
			scds_syslog(LOG_ERR,
			    "Failed to restart the service: %s.",
			    scds_error_string(e));
		}
	}

	return (e);
}

/*
 * priv_failover_rg()
 * Wrapper around scha_control(GIVEOVER)
 * returns error code from scha_control()
 */
static scha_err_t
priv_failover_rg(scds_priv_handle_t *h)
{
	scha_err_t e;
	scds_prop_table_t	*ptable;

	ptable = &(h->ptable);
	if (ptable == NULL) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		    "NULL property table passed to scha-api failover");
		return (SCHA_ERR_INTERNAL);
	}

	if (ptable->pt_rgname == NULL) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		    "Unable to failover the service: resource group "
		    "name is NULL ");
		return (SCHA_ERR_INTERNAL);
	}

	if (ptable->pt_rname == NULL) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		    "Unable to failover the service: resource "
		    "name is NULL ");
		return (SCHA_ERR_INTERNAL);
	}

	e = scha_control_zone(SCHA_GIVEOVER, ptable->pt_rgname,
	    ptable->pt_rname, ptable->pt_zonename);

	if (e == SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * This message indicates that the rgm successfully failed
		 * over a resource group, as expected.
		 * @user_action
		 * This is an informational message.
		 */
		scds_syslog(LOG_INFO, "Successfully failed-over "
		    "resource group.");
	} else {
		if (e != SCHA_ERR_CHECKS) {
			/*
			 * don't log a trace if the error is SCHA_ERR_CHECKS
			 * Probe may fail repeatedly with Failover_mode setting
			 * preventing giveover [or restart], and we don't want
			 * to fill up syslog
			 */
			scds_syslog(LOG_ERR,
			    "Failover attempt failed: %s.",
			    scds_error_string(e));
		}
	}

	return (e);
}
