#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.28	08/05/20 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# lib/libdsdev/Makefile.com
#

LIBRARY = libdsdev.a
VERS = .1

OBJECTS = scds_props.o scds_net.o scds_pmf.o scds_fm.o scds_util.o

# include library definitions, do not change order of include and DYNLIB
include ../../Makefile.lib

# include the macros for doors or rpc implementation
include $(SRC)/Makefile.ipc

MAPFILE=	../common/mapfile-vers
PMAP =		-M $(MAPFILE)
SRCS =		$(OBJECTS:%.o=../common/%.c)

LIBS = $(DYNLIB) $(SPROLINTLIB)

$(SPROLINTLIB):= SRCS+=../common/llib-ldsdev

CLEANFILES =	$(SPROLINTOUT) $(SPROLINTLIB)

CHECKHDRS = libdsdev_priv.h

# definitions for lint
LINTFLAGS += -I. -I$(ROOTCLUSTHDRDIR) -I$(ROOTCLUSTHDRDIR)/rgm/rpc
LINTFLAGS += -I$(TOP)/sys -I$(TOP)/src
LINTFLAGS += -DPMF_SECURITY_DEF=SEC_UNIX_STRONG
LINTFILES += $(OBJECTS:%.o=%.ln)

LDLIBS += -lscha -lc -lsocket -lnsl -lresolv -lpmf -lgen
LDLIBS += -lproject

MTFLAG = -mt

CPPFLAGS += $(MTFLAG)
CPPFLAGS += -I. -I$(ROOTCLUSTHDRDIR)/rgm/rpc
CPPFLAGS += -I$(TOP)/sys -I$(TOP)/src
CPPFLAGS += -DPMF_SECURITY_DEF=SEC_UNIX_STRONG
CPPFLAGS += -DSC_SRM

.KEEP_STATE:

all:  $(LIBS)

$(DYNLIB): 	$(MAPFILE)

# include library targets
include ../../Makefile.targ

objs/%.o pics/%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<
