/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scadmin_dlcommon.c 1.4	08/05/20 SMI"

/*
 * Common (shared) DLPI test routines.
 * These can be split into individual library routines later.
 *
 * Some of libraries has not been implemented yet(QOS,
 * Acknowledged Connectionless)
 */

#include	<scadmin/scadmin.h>
#include	"scadmin_private_dltest.h"

#include	<sys/types.h>
#include	<sys/stream.h>
#include	<stropts.h>
#include	<sys/dlpi.h>
#include	<sys/signal.h>
#include	<stdio.h>
#include	<string.h>
#include	<strings.h>
#include	<stdarg.h>
#include	<errno.h>

/* dlcommon.c */
static uint_t strgetmsg(int fd, struct strbuf *ctlp, struct strbuf *datap,
    int *flagsp, char *caller, scadmin_namelist_t **errsp);
static void expecting(int prim, union DL_primitives *dlp,
    scadmin_namelist_t **errsp);
static uint_t syserr(char *s, scadmin_namelist_t **errsp);
static void err(scadmin_namelist_t **errsp, char *fmt, ...);
static char *dlprim(ulong_t prim);

/* DL_INFO_REQ */
uint_t
dlinforeq(int fd, scadmin_namelist_t **errsp)
{
	dl_info_req_t	info_req;
	struct	strbuf	ctl;
	int	flags;

	info_req.dl_primitive = DL_INFO_REQ;

	ctl.maxlen = 0;
	ctl.len = sizeof (dl_info_req_t);
	ctl.buf = (char *)&info_req;

	flags = RS_HIPRI;

	if (putmsg(fd, &ctl, (struct strbuf *)0, flags) < 0)
		return (syserr("dlinforeq:  putmsg", errsp));

	return (0);
}

/* DL_INFO_ACK */
uint_t
dlinfoack(int fd, char *ctlbuf, scadmin_namelist_t **errsp)
{
	union	DL_primitives	*dlp;
	struct	strbuf	ctl;
	int	flags;

	ctl.maxlen = MAXDLBUF;
	ctl.len = 0;
	ctl.buf = ctlbuf;

	if (strgetmsg(fd, &ctl, (struct strbuf *)0, &flags, "dlinfoack", errsp))
		return (1);

	dlp = (union DL_primitives *)ctl.buf;

	expecting(DL_INFO_ACK, dlp, errsp);

	if (ctl.len < (int)sizeof (dl_info_ack_t))
		err(errsp, "dlinfoack:  response ctl.len too short:  %d",
		    ctl.len);

	if (flags != RS_HIPRI)
		err(errsp, "dlinfoack:  DL_INFO_ACK was not M_PCPROTO");

	return (0);
}

/* DL_ATTACH_REQ */
uint_t
dlattachreq(int fd, ulong_t ppa, scadmin_namelist_t **errsp)
{
	dl_attach_req_t	attach_req;
	struct	strbuf	ctl;
	int	flags;

	attach_req.dl_primitive = DL_ATTACH_REQ;
	attach_req.dl_ppa = ppa;

	ctl.maxlen = 0;
	ctl.len = sizeof (dl_attach_req_t);
	ctl.buf = (char *)&attach_req;

	flags = 0;

	if (putmsg(fd, &ctl, (struct strbuf *)0, flags) < 0)
		return (syserr("dlattachreq:  putmsg", errsp));

	return (0);
}

/* DL_PROMISCON_REQ */
uint_t
dlpromisconreq(int fd, ulong_t level, scadmin_namelist_t **errsp)
{
	dl_promiscon_req_t	promiscon_req;
	struct	strbuf	ctl;
	int	flags;

	promiscon_req.dl_primitive = DL_PROMISCON_REQ;
	promiscon_req.dl_level = level;

	ctl.maxlen = 0;
	ctl.len = sizeof (dl_promiscon_req_t);
	ctl.buf = (char *)&promiscon_req;

	flags = 0;

	if (putmsg(fd, &ctl, (struct strbuf *)0, flags) < 0)
		return (syserr("dlpromiscon:  putmsg", errsp));

	return (0);
}

/* DL_BIND_REQ */
uint_t
dlbindreq(int fd, ulong_t sap, ulong_t max_conind, ulong_t service_mode,
    ulong_t conn_mgmt, ulong_t xidtest, scadmin_namelist_t **errsp)
{
	dl_bind_req_t	bind_req;
	struct	strbuf	ctl;
	int	flags;

	bind_req.dl_primitive = DL_BIND_REQ;
	bind_req.dl_sap = sap;
	bind_req.dl_max_conind = (t_uscalar_t)max_conind;
	bind_req.dl_service_mode = (uint16_t)service_mode;
	bind_req.dl_conn_mgmt = (uint16_t)conn_mgmt;
	bind_req.dl_xidtest_flg = xidtest;

	ctl.maxlen = 0;
	ctl.len = sizeof (dl_bind_req_t);
	ctl.buf = (char *)&bind_req;

	flags = 0;

	if (putmsg(fd, &ctl, (struct strbuf *)0, flags) < 0)
		return (syserr("dlbindreq:  putmsg", errsp));

	return (0);
}

/* DL_UNITDATA_REQ */
uint_t
dlunitdatareq(int fd, uchar_t *addrp, int addrlen, ulong_t minpri,
    ulong_t maxpri, uchar_t *datap, int datalen, scadmin_namelist_t **errsp)
{
	long	buf[MAXDLBUF];
	union   DL_primitives   *dlp;
	struct	strbuf	data, ctl;
	int	flags = 0;

	dlp = (union DL_primitives *)buf;
	dlp->unitdata_req.dl_primitive = DL_UNITDATA_REQ;
	dlp->unitdata_req.dl_dest_addr_length = (t_uscalar_t)addrlen;
	dlp->unitdata_req.dl_dest_addr_offset = sizeof (dl_unitdata_req_t);
	dlp->unitdata_req.dl_priority.dl_min = (t_scalar_t)minpri;
	dlp->unitdata_req.dl_priority.dl_max = (t_scalar_t)maxpri;

	(void) memcpy(OFFADDR(dlp, sizeof (dl_unitdata_req_t)), addrp,
	    (size_t)addrlen);

	ctl.maxlen = 0;
	ctl.len = (int)((int)sizeof (dl_unitdata_req_t) + addrlen);
	ctl.buf = (char *)buf;

	data.maxlen = 0;
	data.len = datalen;
	data.buf = (char *)datap;

	if (putmsg(fd, &ctl, &data, flags) < 0)
		return (syserr("dlunitdatareq:  putmsg", errsp));

	return (0);
}

/* DL_UNITDATA_IND */
uint_t
dlunitdataind(int fd, char *ctlbuf, char *databuf, int *datalen,
    scadmin_namelist_t **errsp)
{
	union   DL_primitives   *dlp;
	struct  strbuf  ctl, data;
	int	flags;

	ctl.maxlen = MAXDLBUF;
	ctl.len = 0;
	ctl.buf = ctlbuf;

	data.maxlen = MAXDLBUF;
	data.len = 0;
	data.buf = databuf;

	if (strgetmsg(fd, &ctl, &data, &flags, "dlunitdataind", errsp))
		return (1);

	*datalen = data.len;
	dlp = (union DL_primitives *)ctl.buf;

	expecting(DL_UNITDATA_IND, dlp, errsp);

	return (0);
}

/* DL_OK_ACK */
uint_t
dlokack(int fd, char *ctlbuf, scadmin_namelist_t **errsp)
{
	union	DL_primitives	*dlp;
	struct	strbuf	ctl;
	int	flags;

	ctl.maxlen = MAXDLBUF;
	ctl.len = 0;
	ctl.buf = ctlbuf;

	if (strgetmsg(fd, &ctl, (struct strbuf *)0, &flags, "dlokack", errsp))
		return (1);

	dlp = (union DL_primitives *)ctl.buf;

	expecting(DL_OK_ACK, dlp, errsp);

	if (ctl.len < (int)sizeof (dl_ok_ack_t))
		err(errsp, "dlokack:  response ctl.len too short:  %d",
		    ctl.len);

	return (0);
}

/* DL_BIND_ACK */
uint_t
dlbindack(int fd, char *ctlbuf, scadmin_namelist_t **errsp)
{
	union	DL_primitives	*dlp;
	struct	strbuf	ctl;
	int	flags;

	ctl.maxlen = MAXDLBUF;
	ctl.len = 0;
	ctl.buf = ctlbuf;

	if (strgetmsg(fd, &ctl, (struct strbuf *)0, &flags, "dlbindack", errsp))
		return (1);

	dlp = (union DL_primitives *)ctl.buf;

	expecting(DL_BIND_ACK, dlp, errsp);

	if (ctl.len < (int)sizeof (dl_bind_ack_t))
		err(errsp, "dlbindack:  short response ctl.len:  %d", ctl.len);

	return (0);
}

/* Wrapper of getmsg() */
static uint_t
strgetmsg(int fd, struct strbuf *ctlp, struct strbuf *datap, int *flagsp,
    char *caller, scadmin_namelist_t **errsp)
{
	int	rc;
	static	char	errmsg[BUFSIZ];

	/*
	 * Set flags argument and issue getmsg().
	 */
	*flagsp = 0;
	if ((rc = getmsg(fd, ctlp, datap, flagsp)) < 0) {
		(void) sprintf(errmsg, "%s:  getmsg", caller);
		err(errsp, errmsg);
	}

	/*
	 * Check for MOREDATA and/or MORECTL.
	 */
	if ((rc & (MORECTL | MOREDATA)) == (MORECTL | MOREDATA))
		err(errsp, "%s:  MORECTL|MOREDATA", caller);
	if (rc & MORECTL)
		err(errsp, "%s:  MORECTL", caller);
	if (rc & MOREDATA)
		err(errsp, "%s:  MOREDATA", caller);

	/*
	 * Check for at least sizeof (long) control data portion.
	 */
	if (ctlp->len < (int)sizeof (long))
		err(errsp, "getmsg:  control portion length < "
		    "sizeof (long):  %d", ctlp->len);

	return (0);
}

/* Compare primitives */
static void
expecting(int prim, union DL_primitives *dlp, scadmin_namelist_t **errsp)
{
	if (dlp->dl_primitive != (ulong_t)prim) {
		err(errsp, "expected %s got %s", dlprim((ulong_t)prim),
			dlprim((ulong_t)dlp->dl_primitive));
	}
}

static char *
dlprim(ulong_t prim)
{
	static	char	primbuf[80];

	switch (prim) {
		case DL_INFO_REQ:	return ("DL_INFO_REQ");
		case DL_INFO_ACK:	return ("DL_INFO_ACK");
		case DL_ATTACH_REQ:	return ("DL_ATTACH_REQ");
		case DL_DETACH_REQ:	return ("DL_DETACH_REQ");
		case DL_BIND_REQ:	return ("DL_BIND_REQ");
		case DL_BIND_ACK:	return ("DL_BIND_ACK");
		case DL_UNBIND_REQ:	return ("DL_UNBIND_REQ");
		case DL_OK_ACK:	return ("DL_OK_ACK");
		case DL_ERROR_ACK:	return ("DL_ERROR_ACK");
		case DL_SUBS_BIND_REQ: return ("DL_SUBS_BIND_REQ");
		case DL_SUBS_BIND_ACK: return ("DL_SUBS_BIND_ACK");
		case DL_SUBS_UNBIND_REQ: return ("DL_SUBS_UNBIND_REQ");
		case DL_ENABMULTI_REQ: return ("DL_ENABMULTI_REQ");
		case DL_DISABMULTI_REQ: return ("DL_DISABMULTI_REQ");
		case DL_PROMISCON_REQ: return ("DL_PROMISCON_REQ");
		case DL_PROMISCOFF_REQ: return ("DL_PROMISCOFF_REQ");
		case DL_UNITDATA_REQ:	return ("DL_UNITDATA_REQ");
		case DL_UNITDATA_IND:	return ("DL_UNITDATA_IND");
		case DL_UDERROR_IND:	return ("DL_UDERROR_IND");
		case DL_UDQOS_REQ:	return ("DL_UDQOS_REQ");
		case DL_CONNECT_REQ:	return ("DL_CONNECT_REQ");
		case DL_CONNECT_IND:	return ("DL_CONNECT_IND");
		case DL_CONNECT_RES:	return ("DL_CONNECT_RES");
		case DL_CONNECT_CON:	return ("DL_CONNECT_CON");
		case DL_TOKEN_REQ:	return ("DL_TOKEN_REQ");
		case DL_TOKEN_ACK:	return ("DL_TOKEN_ACK");
		case DL_DISCONNECT_REQ:return ("DL_DISCONNECT_REQ");
		case DL_DISCONNECT_IND:return ("DL_DISCONNECT_IND");
		case DL_RESET_REQ:	return ("DL_RESET_REQ");
		case DL_RESET_IND:	return ("DL_RESET_IND");
		case DL_RESET_RES:	return ("DL_RESET_RES");
		case DL_RESET_CON:	return ("DL_RESET_CON");
		case DL_PHYS_ADDR_REQ: return ("DL_PHYS_ADDR_REQ");
		case DL_PHYS_ADDR_ACK: return ("DL_PHYS_ADDR_ACK");
		case DL_SET_PHYS_ADDR_REQ: return ("DL_SET_PHYS_ADDR_REQ");
		case DL_GET_STATISTICS_REQ: return ("DL_GET_STATISTICS_REQ");
		case DL_GET_STATISTICS_ACK: return ("DL_GET_STATISTICS_ACK");
		default:
			(void) sprintf(primbuf, "unknown primitive 0x%x", prim);
			return (primbuf);
	}
}

char *
dlmactype(ulong_t media)
{
	static	char	mediabuf[80];

	switch (media) {
		case DL_CSMACD:return ("DL_CSMACD");
		case DL_TPB:   return ("DL_TPB");
		case DL_TPR:   return ("DL_TPR");
		case DL_METRO: return ("DL_METRO");
		case DL_ETHER: return ("DL_ETHER");
		case DL_HDLC:  return ("DL_HDLC");
		case DL_CHAR:  return ("DL_CHAR");
		case DL_CTCA:  return ("DL_CTCA");
		case DL_FDDI:  return ("DL_FDDI");
		case DL_FC:  return ("DL_FC");
		case DL_ATM:  return ("DL_ATM");
		case DL_IPATM:  return ("DL_IPATM");
		case DL_X25:  return ("DL_X25");
		case DL_ISDN:  return ("DL_ISDN");
		case DL_HIPPI:  return ("DL_HIPPI");
		case DL_100VG:  return ("DL_100VG");
		case DL_100VGTPR:  return ("DL_100VGTPR");
		case DL_ETH_CSMA:  return ("DL_ETH_CSMA");
		case DL_100BT:  return ("DL_100BT");
		case DL_FRAME:  return ("DL_FRAME");
		case DL_MPFRAME:  return ("DL_MPFRAME");
		case DL_ASYNC:  return ("DL_ASYNC");
		case DL_IPX25:  return ("DL_IPX25");
		case DL_LOOP:  return ("DL_LOOP");
		case DL_OTHER:  return ("DL_OTHER");
		default:
			(void) sprintf(mediabuf, "unknown media type 0x%x",
				media);
			return (mediabuf);
	}
}

/*VARARGS1*/
static void
err(scadmin_namelist_t **errsp, char *fmt, ...)
{
	va_list ap;
	char errmsg[BUFSIZ];

	if (!errsp)
		return;

	/*lint -e40 */
	va_start(ap, fmt);
	(void) vsprintf(errmsg, fmt, ap);
	va_end(ap);

	(void) scadmin_append_namelist(errsp, errmsg);
}

static uint_t
syserr(char *s, scadmin_namelist_t **errsp)
{
	char errmsg[BUFSIZ];

	if (errsp) {
		/*lint -e746 */
		(void) sprintf(errmsg, "%s: %s", s, strerror(errno));
		(void) scadmin_append_namelist(errsp, errmsg);
	}

	return (1);
}
