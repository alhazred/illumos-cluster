/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * Copyright (c) 1983, 1984, 1985, 1986, 1987, 1988, 1989 AT&T
 * All Rights Reserved.
 */

/*
 * University Copyright- Copyright (c) 1982, 1986, 1988
 * The Regents of the University of California.
 * All Rights Reserved.
 *
 * University Acknowledgment- Portions of this document are derived from
 * software developed by the University of California, Berkeley, and its
 * contributors.
 */

#pragma ident	"@(#)scadmin_autodiscover.c	1.12	08/05/20 SMI"

#include	<scadmin/scadmin.h>
#include	"scadmin_private_dltest.h"

#include	<stdio.h>
#include	<unistd.h>
#include	<stdlib.h>
#include	<stdio.h>
#include	<strings.h>
#include	<ctype.h>
#include	<netdb.h>
#include	<fcntl.h>
#include	<libintl.h>
#include	<locale.h>
#include	<assert.h>
#include	<errno.h>
#include	<stropts.h>

#include	<netinet/in_systm.h>
#include	<netinet/in.h>
#include	<netinet/ip.h>
#include	<netinet/ip_icmp.h>

#include	<sys/time.h>
#include	<sys/types.h>
#include	<sys/dlpi.h>

/* Sap is one more than transport sap */
#define	SAP	0x834

#define	ABS(x)		((x) < 0 ? -(x) : (x))

#define	PREFIX "/dev/"
#define	PREFIXLEN 5

#define	DESTADDR_MAXLEN	32	/* at least macaddr length + sap size */

#define	MAXDATA	(32 * 1024)
#define	MAXRECV 100

#define	SEND_INTERVAL 1

#define	DEFAULT_SEND_CNT	30	/* default send count */
#define	DEFAULT_RECV_TO		30	/* default receive timeout (seconds) */

static void admin_autodiscover_output(char *buffer, FILE *outputf,
    scadmin_namelist_t **outputp);
static ushort_t in_cksum(ushort_t *addr, int len);

/*
 * admin_autodiscover_output
 *
 * Output a line of autodiscovery results to the given file pointer
 * and/or the "outputp" list.
 */
static void
admin_autodiscover_output(char *buffer, FILE *outputf,
    scadmin_namelist_t **outputp)
{
	if (!buffer)
		return;

	/* Print to file */
	if (outputf) {
		(void) fputs(buffer, outputf);
		(void) putc('\n', outputf);
		(void) fflush(outputf);
	}

	/* Add to namelist */
	if (outputp)
		(void) scadmin_append_namelist(outputp, buffer);
}

/*
 * scadmin_autodiscover
 *
 * Send and/or recieve dlpi packets to and/or from the specified list
 * of "adapters" in order to discover connectivety.   The arguments to
 * this function control behavior in the following ways:
 *
 *	adapters	This string is a comma-separated list of adapters
 *			to use for sending and/or receiving packtets.
 *
 *	vlans		This string is a comma-separated list of vlanid's
 *			to use for sending and/or receiving packtets.
 *
 *	filename	If not NULL, results of the discovery attempt
 *			are printed to this file.
 *
 *	outputp		If not NULL, results are added to this list.
 *
 *	flags		The following flags may be or-ed together:
 *
 *	    SCADMIN_AUTODISCOVER_SEND		Send packets
 *	    SCADMIN_AUTODISCOVER_RECV		Receive packets
 *	    SCADMIN_AUTODISCOVER_PING		Finish up w/ broadcast ping
 *
 *	    SCADMIN_AUTODISCOVER_NOWAIT		Stop receiving before timeout
 *	    SCADMIN_AUTODISCOVER_SNOOP		Just snoop
 *
 *			If SCADMIN_AUTODISCOVER_NOWAIT is given, we
 *			quit as soon as at least one packet has been
 *			received on each adapters, without waiting
 *			for the timeout.   Or, if wait_cnt is non-zero,
 *			quit as soon as at least one packet has been
 *			received on wait_cnt adapters.
 *
 *			Setting "flags" to zero results in the same behavior
 *			as or-ing the FIRST THREE flags together.
 *
 *	send_cnt	If sending, send the given number of packets on
 *			each of the adapters at 1 second intervals.  If
 *			zero, packets are sent at 1 second intervals until
 *			the receive timeout is exhausted.
 *
 *	recv_to		If receiving, look for packets on each of the adapters
 *			until "recv_to" seconds are exhasted.   If zero,
 *			a default of 30 seconds is used.
 *
 *	wait_cnt	If SCADMIN_AUTODISCOVER_NOWAIT is given and wait_cnt
 *			is non-zero, quit as soon as at least one packet
 *			has been received on wait_cnt adapters.
 *
 *	token		If not NULL, it is added to each packet sent
 *			and is used to identify packets intended for
 *			us on the receiving side.
 *
 *	errsp		If not NULL, error messages will be added to this list.
 *
 * Possible return values:
 *
 *	0		No unexpected errors
 *	1		Unexpected errors
 */
uint_t
scadmin_autodiscover(char *adapters, char *vlans, char *filename,
    scadmin_namelist_t **outputp, uint_t flags, uint_t send_cnt,
    uint_t recv_to, uint_t wait_cnt, char *token, scadmin_namelist_t **errsp)
{
	FILE	*outputf = NULL;
	char	hostname[MAXHOSTNAMELEN + 1];
	char	*ptr;
	char	***recvdata = NULL;
	char	**devices = NULL;
	char	tmp_device[64];
	uchar_t	*no_attach = NULL;
	uchar_t	**addr = NULL, *addrbuf = NULL;
	int	*addrlen = NULL;
	ulong_t	*ppas = NULL;
	int	*fds = NULL;
	int	*recvcount = NULL;
	uint_t	nfds = 0;
	ushort_t localsap = SAP;
	uint_t	fdnum;
	uint_t	numdevs;
	uint_t	maxdevs;
	int scan_vlans;
	uint_t	dphyslen;
	uchar_t	dphys[16], dlsap[16] = {0};
	uchar_t	buf[MAXDLBUF];
	union	DL_primitives	*dlp;
	uint_t	i;
	int	dlsaplen, dladdrlen;
	int	datalen = 0;
	t_uscalar_t	mactype;
	struct timeval	tp, oldtp, quittp;
	struct timeval	timeout = {SEND_INTERVAL, 0};
	fd_set	infds, readfds, errorfds;
	char	xmitbuf[MAXDLBUF];
	uchar_t	databuf[MAXDATA];
	uint_t	chunksize = MAXDATA;
	struct strbuf strdata;
	int strflags;
	char	*received;
	char	*xmitdata;
	uint_t	tokenlen;
	uint_t	len;
	char	outputbuf[BUFSIZ];
	char *lasts;
	uint_t rstatus = 1;
	char errbuff[BUFSIZ];
	uint_t	first;
	uint_t *vlanlist = NULL;
	uint_t *uniq_vlanlist = NULL;
	uint_t num_vlans;
	uint_t num_uniqvlans;
	uint_t vnum;
	char svlan[10];
	uint_t uniq_vlan;
	uint_t ivnum;
	ulong_t *vlan_descrip = NULL;
	uint_t *descrip_invalid = NULL;
	uint_t *vlanid = NULL;
	uint_t adp_idx;
	uint_t vlan_idx;
	scadmin_namelist_t *local_errstr = (scadmin_namelist_t *)0;
	char *line;

	/* I18N housekeeping */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/*
	 * Get devices
	 */
	if (adapters == NULL || *adapters == '\0')
		return (0);

	numdevs = 1;
	for (ptr = adapters;  *ptr;  ptr++)
		if (*ptr == ',')
			++numdevs;

	if ((devices = (char **)malloc(numdevs * sizeof (char *))) == NULL) {
		rstatus = 1;
		goto cleanup;
	}
	lasts = NULL;
	ptr = strtok_r(adapters, ",", &lasts);
	for (fdnum = 0; fdnum < numdevs; fdnum++) {
		assert(ptr != NULL);
		/*lint -e668 */
		devices[fdnum] = (char *)malloc(strlen(ptr) + PREFIXLEN + 1);
		if (devices[fdnum] == NULL) {
			rstatus = 1;
			goto cleanup;
		}
		(void) sprintf(devices[fdnum], "%s%s", PREFIX, ptr);
		ptr = strtok_r(NULL, ",", &lasts);
	}
	assert(ptr == NULL);

	/*
	 * Get the vlan id numbers and also the unique vlan ids. For "send"
	 * we will use the vlanids mapped to the devices, but for "receive"
	 * we will try all unique vlans for all devices.
	 */
	num_vlans = 1;
	if (vlans) {
		for (ptr = vlans;  *ptr;  ptr++)
			if (*ptr == ',')
				++num_vlans;
	}

	if ((vlanlist = (uint_t *)calloc(num_vlans, sizeof (uint_t))) == NULL) {
		rstatus = 1;
		goto cleanup;
	}

	if ((uniq_vlanlist = (uint_t *)calloc(num_vlans, sizeof (uint_t)))
	    == NULL) {
		rstatus = 1;
		goto cleanup;
	}

	if (vlans) {
		lasts = NULL;
		ptr = strtok_r(vlans, ",", &lasts);
		num_uniqvlans = 0;
		for (vnum = 0; vnum < num_vlans; vnum++) {

			assert(ptr != NULL);
			(void) sprintf(svlan, "%s", ptr);
			vlanlist[vnum] = (uint_t)atoi(svlan);
			ptr = strtok_r(NULL, ",", &lasts);
			uniq_vlan = 1;
			for (ivnum = 0; ivnum < vnum; ivnum ++) {
				/*lint -e661 */
				if (vlanlist[vnum] == vlanlist[ivnum]) {
					uniq_vlan = 0;
					break;
				}
			}
			if (uniq_vlan) {
				/*lint -e661 */
				uniq_vlanlist[num_uniqvlans] = vlanlist[vnum];
				num_uniqvlans ++;
			}
		}
		assert(ptr == NULL);
	} else {
		num_uniqvlans = 1;
		vlanlist[0] = 0;
		uniq_vlanlist[0] = 0;
	}

	/* If "flags" is zero, set to receive, send, and ping */
	if (flags == 0) {
		flags = (SCADMIN_AUTODISCOVER_RECV |
		    SCADMIN_AUTODISCOVER_SEND |
		    SCADMIN_AUTODISCOVER_PING);
	}

	/* If snoop is set, we are receiving */
	if (flags & SCADMIN_AUTODISCOVER_SNOOP) {
		flags &= (SCADMIN_AUTODISCOVER_NOWAIT);
		flags |= (SCADMIN_AUTODISCOVER_SNOOP |
		    SCADMIN_AUTODISCOVER_RECV);
		filename = NULL;
		token = NULL;
	}

	/* Fix the send count */
	if (flags & SCADMIN_AUTODISCOVER_SEND) {
		if (send_cnt == 0) {
			if (flags & SCADMIN_AUTODISCOVER_RECV) {
				send_cnt = DEFAULT_SEND_CNT;
			} else {
				send_cnt = 1;
			}
		}
	} else {
		send_cnt = 0;
	}

	/* Fix the receive timeout */
	if (flags & SCADMIN_AUTODISCOVER_RECV) {
		if (recv_to == 0)
			recv_to = DEFAULT_RECV_TO;
	} else {
		recv_to = 0;
	}

	/* If autodiscover_receive, will need to scan vlan combinations */
	if ((flags & SCADMIN_AUTODISCOVER_RECV) &&
	    !(flags & SCADMIN_AUTODISCOVER_SNOOP) &&
	    !(flags & SCADMIN_AUTODISCOVER_SEND)) {
		scan_vlans = 1;
		maxdevs = num_uniqvlans * numdevs;
	} else {
		scan_vlans = 0;
		maxdevs = numdevs;
	}

	/* If "filename", open the file */
	if (filename != NULL) {
		outputf = fopen(filename, "a");
		if (outputf == NULL) {
			if (errsp) {
				(void) sprintf(errbuff, dgettext(TEXT_DOMAIN,
				    "Couldn't open %s"), filename);
				(void) scadmin_append_namelist(errsp, errbuff);
			}
			rstatus = 1;
			goto cleanup;
		}
	}

	/* Add the token to the transmit buffer */
	xmitdata = xmitbuf;
	if (token != NULL) {
		tokenlen = strlen(token);
		if (tokenlen >= sizeof (xmitbuf)) {
			rstatus = 1;
			goto cleanup;
		}

		/* Copy, substituting '_' for any ':' we might find */
		ptr = token;
		while (*ptr) {
			*xmitdata++ = (*ptr == ':' ? '_' : *ptr);
			++ptr;
		}

		*xmitdata++ = ':';
	}

	/* Get my unqualified host name */
	hostname[sizeof (hostname) - 1] = '\0';
	(void) gethostname(hostname, sizeof (hostname) - 1);
	if ((ptr = strchr(hostname, '.')) != NULL)
		*ptr = '\0';

	/*
	 * Initialization currently initializes values which
	 * might not actually be used as a result of certain
	 * "flags" settings.
	 */

	/* initialize buf[] */
	for (i = 0; i < MAXDLBUF; i++)
		buf[i] = (unsigned char) i & 0xff;

	/* Allocate and initialize various buffers */
	ppas = (ulong_t *)calloc(numdevs, sizeof (ulong_t));

	no_attach = (uchar_t *)calloc(maxdevs, sizeof (uchar_t));
	addr = (uchar_t **)calloc(maxdevs, sizeof (uchar_t *));
	addrbuf = (uchar_t *)malloc(maxdevs * DESTADDR_MAXLEN);
	addrlen = (int *)calloc(maxdevs, sizeof (int));

	vlan_descrip = (ulong_t *)calloc(maxdevs, sizeof (ulong_t));
	descrip_invalid = (uint_t *)calloc(maxdevs, sizeof (uint_t));
	vlanid = (uint_t *)calloc(maxdevs, sizeof (uint_t));
	fds = (int *)calloc(maxdevs, sizeof (int));
	recvcount = (int *)calloc(maxdevs, sizeof (int));
	recvdata = (char ***)calloc(maxdevs, sizeof (char **));
	if (ppas == NULL || fds == NULL || recvdata == NULL ||
	    vlan_descrip == NULL || descrip_invalid == NULL ||
	    no_attach == NULL || addr == NULL ||
	    addrbuf == NULL || addrlen == NULL ||
	    vlanid == NULL || recvcount == NULL) {
		rstatus = 1;
		goto cleanup;
	}
	for (fdnum = 0; fdnum < maxdevs; fdnum++)
		addr[fdnum] = (uchar_t *)&addrbuf[fdnum * DESTADDR_MAXLEN];

	for (fdnum = 0; fdnum < numdevs; fdnum++) {
		/* ppas */

		/*
		 * Adapter name is of the form <device_name><device_instance>
		 *
		 *  <device_instance> is numeric
		 *  <device_name> in general is alphabetical
		 *  however, in some cases  may contain embedded digits
		 *	(ex: e1000g)
		 */

		char *adaptername = devices[fdnum];
		ptr = &adaptername[strlen(adaptername) - 1];
		while (isdigit(*ptr))
		    ptr--;
		ptr++;

		if (strlen(ptr) == 0) {
			ppas[fdnum] = 0;
		} else {
			ppas[fdnum] = (ulong_t)atoi(ptr);
			ptr[0] = '\0';
		}

	}

	/*
	 * Convert destination address string to address.
	 */
	dphyslen = 6;
	for (i = 0; i < dphyslen; i++) {
	    dphys[i] = 0xff;
	}

	/*
	 * The lint exception is added for the usage of FD_ZERO for the
	 * following reason. On s8u6, the usage of FD_ZERO below is
	 * correct. But, for s9, the usage results in a lint error.
	 * The lint error Info(792) is described as "void cast
	 * of void expression". This lint error shows up because of
	 * a change to the definition of the FD_ZERO macro in s9 to
	 * the following.
	 * #define FD_ZERO(__p) (void) memset((__p), 0, sizeof (*(__p)))
	 */
	(void) FD_ZERO(&infds);	/*lint !e792 */

	for (fdnum = 0; fdnum < maxdevs; fdnum++) {

		/* recvdata */
		recvdata[fdnum] = (char **)calloc(MAXRECV, sizeof (char *));

		/*
		 * Open the device.
		 */
		if ((fds[fdnum] = open(devices[fdnum % numdevs], 2)) < 0) {
			/*
			 * Try style 1 open of the device. Here the
			 * instance number is appended to the device name.
			 */
			(void) sprintf(tmp_device, "%s%d",
			    devices[fdnum], ppas[fdnum]);

			if ((fds[fdnum] = open(tmp_device, 2)) < 0) {
				if (errsp) {
					/*lint -e746 */
					(void) sprintf(errbuff, "%s: %s",
					    devices[fdnum % numdevs],
					    strerror(errno));
					(void) scadmin_append_namelist(errsp,
					    errbuff);
				}
				rstatus = 1;
				goto cleanup;
			}

			/* No need to attach */
			no_attach[fdnum] = 1;
		}

		if (!scan_vlans) {
			if (vlans) {
				vlan_descrip[fdnum] = ppas[fdnum] +
				    1000 * vlanlist[fdnum];
				vlanid[fdnum] = vlanlist[fdnum];
			} else {
				vlan_descrip[fdnum] = ppas[fdnum];
				vlanid[fdnum] = 0;
			}
		} else {
			adp_idx = fdnum % numdevs;
			vlan_idx = fdnum / numdevs;
			vlan_descrip[fdnum] = 1000 *
			    uniq_vlanlist[vlan_idx] + ppas[adp_idx];
			vlanid[fdnum] = uniq_vlanlist[vlan_idx];
		}

		/*
		 * Attach, if needed
		 */
		if (no_attach[fdnum] == 0) {
			if (dlattachreq(fds[fdnum], vlan_descrip[fdnum],
			    errsp)) {
				rstatus = 1;
				goto cleanup;
			}

			local_errstr = (scadmin_namelist_t *)0;
			(void) dlokack(fds[fdnum], (char *)buf, &local_errstr);
			if (local_errstr) {
				line = local_errstr->scadmin_nlist_name;
				if (strstr(line, "expected")) {
					descrip_invalid[fdnum] = 1;
					scadmin_free_namelist(local_errstr);
					local_errstr = (scadmin_namelist_t *)0;
					continue;
				}
				scadmin_free_namelist(local_errstr);
				local_errstr = (scadmin_namelist_t *)0;
			}
		}

		/*lint -e573 -e574 -e737 -e713 */
		FD_SET(fds[fdnum], &infds);
		if (fds[fdnum] >= (int)nfds) {
			nfds = (uint_t)fds[fdnum]+1;
		}

		/*
		 * Enable promiscuous mode.
		 */
		if (dlpromisconreq(fds[fdnum], DL_PROMISC_PHYS, errsp)) {
			rstatus = 1;
			goto cleanup;
		}
		if (dlokack(fds[fdnum], (char *)buf, errsp)) {
			rstatus = 1;
			goto cleanup;
		}

		/*
		 * Bind.
		 */
		if (dlbindreq(fds[fdnum], (ulong_t)localsap, 0,
		    DL_CLDLS, 0, 0, errsp)) {
			rstatus = 1;
			goto cleanup;
		}
		if (dlbindack(fds[fdnum], (char *)buf, errsp)) {
			rstatus = 1;
			goto cleanup;
		}

		/*
		 * Get info.
		 */
		if (dlinforeq(fds[fdnum], errsp)) {
			rstatus = 1;
			goto cleanup;
		}
		if (dlinfoack(fds[fdnum], (char *)buf, errsp)) {
			rstatus = 1;
			goto cleanup;
		}
		dlp = (union DL_primitives *)buf;
		dlsaplen = dlp->info_ack.dl_sap_length;
		dladdrlen = (int)dlp->info_ack.dl_addr_length;
		mactype = dlp->info_ack.dl_mac_type;

		(void) memcpy((char *)dlsap, (char *)&localsap,
				(uint_t)ABS(dlsaplen));

		/* DL_ETHER */
		if (mactype == DL_ETHER || (dlsaplen == -2 && dladdrlen == 8)) {
			dphyslen = dlp->info_ack.dl_brdcst_addr_length;
			if (dphyslen != 6) {
				(void) sprintf(errbuff, dgettext(TEXT_DOMAIN,
				    "destination MAC address must be 6 bytes"));
				(void) scadmin_append_namelist(errsp, errbuff);
				rstatus = 1;
				goto cleanup;
			}
			(void) memcpy((char *)addr[fdnum], (char *)dphys,
				dphyslen);
			(void) memcpy((char *)addr[fdnum] + dphyslen,
				(char *)dlsap, (uint_t)ABS(dlsaplen));
			addrlen[fdnum] = (int)dphyslen + (int)ABS(dlsaplen);
		} /* end of DL_ETHER */
#ifdef DL_IB
		/* DL_IB */
		else if (mactype == DL_IB) {

			dphyslen = dlp->info_ack.dl_brdcst_addr_length;

			(void) memcpy((char *)addr[fdnum],
				((char *)&dlp->info_ack) +
				dlp->info_ack.dl_brdcst_addr_offset,
				dphyslen);
			(void) memcpy((char *)addr[fdnum] + dphyslen,
				(char *)dlsap, (uint_t)ABS(dlsaplen));
			addrlen[fdnum] = (int)dphyslen + (int)ABS(dlsaplen);
		}
#endif
		else if (mactype == DL_OTHER && dladdrlen == ABS(dlsaplen)) {
			(void) memcpy((char *)addr[fdnum], (char *)dlsap,
			    (size_t)dladdrlen);
			addrlen[fdnum] = dladdrlen;
		} /* end of DL_OTHER */

		else {
			(void) sprintf(errbuff, dgettext(TEXT_DOMAIN,
			    "%s media type not implemented yet"),
			    dlmactype(mactype));
			(void) scadmin_append_namelist(errsp, errbuff);
			rstatus = 1;
			goto cleanup;
		} /* end of other protocols */

		/* Additional snoop initialization */
		if (flags & SCADMIN_AUTODISCOVER_SNOOP) {

			if (dlpromisconreq(fds[fdnum], DL_PROMISC_SAP, errsp)) {
				rstatus = 1;
				goto cleanup;
			}
			if (dlokack(fds[fdnum], (char *)buf, errsp)) {
				rstatus = 1;
				goto cleanup;
			}

			if (ioctl(fds[fdnum], DLIOCRAW, 0) < 0) {
				(void) sprintf(errbuff, dgettext(TEXT_DOMAIN,
				    "ioctl: DIOCRAW failed"));
				(void) scadmin_append_namelist(errsp, errbuff);
				rstatus = 1;
				goto cleanup;
			}
		}
	}

	/*
	 * Transmit/receive packets.
	 */
	(void) gettimeofday(&oldtp, NULL);
	quittp = oldtp;
	quittp.tv_sec += recv_to; /* Quit after 30 seconds */
	oldtp.tv_sec -= SEND_INTERVAL+1;

	/*
	 * The lint exception is added for the usage of FD_ZERO for the
	 * following reason. On s8u6, the usage of FD_ZERO below is
	 * correct. But, for s9, the usage results in a lint error.
	 * The lint error Info(792) is described as "void cast
	 * of void expression". This lint error shows up because of
	 * a change to the definition of the FD_ZERO macro in s9 to
	 * the following.
	 * #define FD_ZERO(__p) (void) memset((__p), 0, sizeof (*(__p)))
	 */
	(void) FD_ZERO(&readfds);	/*lint !e792 */
	(void) FD_ZERO(&errorfds);	/*lint !e792 */

	first = 1;
	while (1) {
		int num = 0;
		int found;

		if (!first) {
			errorfds = infds;
			readfds = infds;
			num = select((int)nfds, &readfds, NULL, &errorfds,
			    &timeout);
		} else {
			first = 0;
		}

		/*
		 * Do the send every SEND_INTERVAL seconds
		 */
		(void) gettimeofday(&tp, NULL);

		/*
		 * Time to quit?
		 */
		if (flags & SCADMIN_AUTODISCOVER_RECV) {
			/*CSTYLED*/
			if (timercmp(&tp, &quittp, >)) {
				rstatus = 0;
				goto cleanup;
			}
		} else if (send_cnt) {
			(void) sleep(1);
		} else {
			rstatus = 0;
			goto cleanup;
		}

		/*
		 * Send
		 */
		if ((flags & SCADMIN_AUTODISCOVER_SEND) && send_cnt) {

			/* CSTYLED */
			if (timercmp(&tp, &oldtp, >) ||
			    !(flags & SCADMIN_AUTODISCOVER_RECV)) {
				(void) gettimeofday(&oldtp, NULL);
				oldtp.tv_sec += SEND_INTERVAL;
				for (fdnum = 0; fdnum < numdevs; fdnum++) {
					(void) sprintf(xmitdata, "%s:%s%d:%d",
					    hostname, devices[fdnum]+PREFIXLEN,
					    ppas[fdnum], vlanlist[fdnum]);

					if (dlunitdatareq(fds[fdnum],
					    addr[fdnum], addrlen[fdnum],
					    0, 0, (uchar_t *)xmitbuf,
					    (int)strlen(xmitbuf)+1, errsp)) {
						rstatus = 1;
						goto cleanup;
					}
				}
				--send_cnt;

				/*
				 * If this is our last send, lets see if
				 * we can do one last receive.
				 */
				if (!send_cnt && !num) {
					errorfds = infds;
					readfds = infds;
					num = select((int)nfds, &readfds,
					    NULL, &errorfds, &timeout);
				}
			}
		}

		/*
		 * Receive
		 */
		if (num > 0) {
			for (fdnum = 0; fdnum < maxdevs; fdnum++) {

				if (descrip_invalid[fdnum] == 1)
					continue;

				/*lint -e573 -e737 */
				if (FD_ISSET(fds[fdnum], &errorfds)) {
					(void) sprintf(errbuff,
					    dgettext(TEXT_DOMAIN,
					    "Error on fdnum %d %d"),
					    fdnum, fds[fdnum]);
					(void) scadmin_append_namelist(
					    errsp, errbuff);
				}

				/* If no data to read here, continue */
				/*lint -e573 -e737 */
				if (!FD_ISSET(fds[fdnum], &readfds))
					continue;

				/* Get the data */
				if (flags & SCADMIN_AUTODISCOVER_SNOOP) {
					strdata.len = 0;
					strdata.maxlen = chunksize;
					strdata.buf = (char *)databuf;
					strflags = 0;

					if (getmsg(fds[fdnum], NULL, &strdata,
					    &strflags) < 0)
						continue;

					if (strdata.len <= 0)
						continue;

					datalen = 0;
				} else {
					if (dlunitdataind(fds[fdnum],
					    (char *)buf, (char *)databuf,
					    &datalen, errsp)) {
						rstatus = 1;
						goto cleanup;
					}
				}
				++recvcount[fdnum];

				/* If we don't care about recieves, continue */
				if (!(flags & SCADMIN_AUTODISCOVER_RECV))
					continue;

				/* Zero length, continue */
				if (datalen == 0)
					continue;
				databuf[datalen] = '\0';

				/* Check token */
				received = (char *)databuf;
				if (token != NULL) {

					/* Make sure it matches */
					/*lint -e644 */
					if (strncmp(token, (char *)databuf,
					    tokenlen) ||
					    databuf[tokenlen] != ':')
						continue;

					/* Also, ignore our own echoes */
					received = (char *)databuf+tokenlen+1;
					ptr = strchr(received, ':');
					len = (uint_t)ABS(ptr - received);
					if (strncasecmp(received, hostname,
					    len) == 0 && hostname[len] == '\0')
						continue;
				}

				/* See if we already have this */
				found = 0;
				for (i = 0; recvdata[fdnum][i]; i++) {
					if (strcmp(recvdata[fdnum][i],
					    received) == 0) {
						found = 1;
						break;
					}
				}

				/* Yes, we have it, continue */
				if (found)
					continue;

				/* Too many? */
				if (i >= MAXRECV-1) {
					(void) sprintf(errbuff, dgettext(
					    TEXT_DOMAIN,
					    "Error on fdnum %d %d"),
					    fdnum, fds[fdnum]);
					(void) sprintf(errbuff,
					    dgettext(TEXT_DOMAIN,
					    "Too many recvs"));
					(void) scadmin_append_namelist(errsp,
					    errbuff);

					continue;
				}

				/* Add the data to our list */
				(void) sprintf(outputbuf, "%s%d:%d:%s",
				    devices[fdnum % numdevs]+PREFIXLEN,
				    ppas[fdnum % numdevs], vlanid[fdnum],
				    received);
				admin_autodiscover_output(outputbuf,
				    outputf, outputp);
				recvdata[fdnum][i] = strdup(received);
			}

			/* Time to quit? */
			if ((flags & SCADMIN_AUTODISCOVER_NOWAIT) &&
			    !send_cnt) {

				/* Try to find one that hasn't received */
				i = 0;
				found = 0;
				for (fdnum = 0; fdnum < maxdevs; fdnum++) {
					if (descrip_invalid[fdnum] == 1)
						continue;

					if (!recvcount[fdnum]) {
						++found;
					} else {
						++i;
					}
				}

				if (!found ||
				    (wait_cnt && (i >= wait_cnt))) {
					rstatus = 0;
					goto cleanup;
				}
			}
		}
	}

cleanup:
	/* Normal return */
	if (rstatus == 0) {
		if (flags & SCADMIN_AUTODISCOVER_SNOOP) {
			for (fdnum = 0; fdnum < maxdevs; fdnum++) {
				(void) sprintf(outputbuf, "%s%d:%d",
				    devices[fdnum]+PREFIXLEN,
				    vlan_descrip[fdnum], recvcount[fdnum]);
				admin_autodiscover_output(outputbuf, outputf,
				    outputp);
			}
		}
		admin_autodiscover_output("quit", outputf, outputp);
	}

	/* Close output file */
	if (outputf)
		(void) fclose(outputf);

	/* Close adapter file descriptors */
	if (fds) {
		for (fdnum = 0; fdnum < numdevs; fdnum++)
			(void) close(fds[fdnum]);
	}

	/* Free recvdata */
	if (recvdata) {
		for (fdnum = 0; fdnum < numdevs; fdnum++) {
			if (recvdata[fdnum]) {
				for (i = 0;  i < MAXRECV;  ++i) {
					if (recvdata[fdnum][i] != NULL)
						free(recvdata[fdnum][i]);
				}
				free(recvdata[fdnum]);
			}
		}
		free(recvdata);
	}

	/* Free devices */
	if (devices) {
		for (fdnum = 0; fdnum < numdevs; fdnum++) {
			if (devices[fdnum])
				free(devices[fdnum]);
		}
		free(devices);
	}

	/* Free ppas and fds */
	if (ppas)
		free(ppas);
	if (fds)
		free(fds);
	if (recvcount)
		free(recvcount);
	if (no_attach)
		free(no_attach);
	if (addr)
		free(addr);
	if (addrbuf)
		free(addrbuf);
	if (addrlen)
		free(addrlen);

	/*
	 * Send a message out on each interface so the switches
	 * will know which interfaces are active.
	 * (The switch may have become confused by our probes.)
	 */
	if ((flags & SCADMIN_AUTODISCOVER_PING) &&
	    (flags & SCADMIN_AUTODISCOVER_SEND))
		scadmin_broadcast_ping();

	return (rstatus);
}

/*
 * scadmin_snoop
 *
 * Snoop for all packets on the list of "adapters".
 *
 *	adapters	This string is a comma-separated list of adapters
 *			to use for snooping.
 *
 *	outputp		A line is added to the "outputp" list for each
 *			adapter in the form "<adapter>:<count>", where
 *			<count> is the number of packets seen over the
 *			"recv_to" period.
 *
 *	flags		The following flag may be given:
 *
 *	    SCADMIN_AUTODISCOVER_NOWAIT		Stop receiving before timeout
 *
 *			If SCADMIN_AUTODISCOVER_NOWAIT is given, we
 *			quit as soon as one packet has been received on
 *			each adapters, without waiting for the timeout.
 *
 *	recv_to		Look for packets on each of the adapters
 *			until "recv_to" seconds are exhasted.   If zero,
 *			a default of 30 seconds is used.
 *
 *	errsp		If not NULL, error messages will be added to this list.
 *
 * Possible return values:
 *
 *	0		No unexpected errors
 *	1		Unexpected errors
 */
uint_t
scadmin_snoop(char *adapters, scadmin_namelist_t **outputp, uint_t flags,
    uint_t recv_to, scadmin_namelist_t **errsp)
{
	/* Nowait flag is okay;   add in the snoop and recv flags */
	flags &= (SCADMIN_AUTODISCOVER_NOWAIT);
	flags |= (SCADMIN_AUTODISCOVER_SNOOP | SCADMIN_AUTODISCOVER_RECV);
	return (scadmin_autodiscover(adapters, NULL, NULL, outputp,
	    flags, 0, recv_to, 0, NULL, errsp));
}

/*
 * scadmin_broadcast_ping
 *
 * Send a ping to 255.255.255.255.
 */
void
scadmin_broadcast_ping(void)
{
#define	DATALEN		56
#define	TOTLEN		(ICMP_MINLEN + sizeof (struct timeval) + DATALEN)
	uchar_t out_pkt[TOTLEN];
	struct icmp *icp = (struct icmp *)out_pkt;
	struct timeval *tp = (struct timeval *)&out_pkt[ICMP_MINLEN];
	uchar_t *datap = &out_pkt[ICMP_MINLEN + sizeof (struct timeval)];
	struct sockaddr_in whereto;
	int sock;
	int i;

	/* Open the socket */
	if ((sock = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP)) < 0)
		return;

	/* Set up destination address to broadcast of all 1's */
	bzero(&whereto, sizeof (whereto));
	whereto.sin_family = AF_INET;
	whereto.sin_addr.s_addr = (uint32_t)~0;
	whereto.sin_port = IPPORT_ECHO;

	/* Initialize buffer */
	bzero(out_pkt, sizeof (out_pkt));

	/* Initialize ICMP header */
	icp->icmp_type = ICMP_ECHO;
	icp->icmp_code = 0;
	icp->icmp_cksum = 0;
	icp->icmp_seq = 0;
	icp->icmp_id = htons((int)getpid() & 0xFFFF);

	/* Initialize time stamp */
	(void) gettimeofday(tp, (struct timezone *)NULL);

	/* Initialize data to something */
	for (i = sizeof (struct timeval);  i < DATALEN;  i++)
		*datap++ = (uchar_t)i;

	/* Add checksum */
	icp->icmp_cksum = in_cksum((ushort_t *)icp, TOTLEN);

	/* Send ICMP_ECHO */
	(void) sendto(sock, (char *)out_pkt, TOTLEN, 0,
	    (struct sockaddr *)&whereto, sizeof (struct sockaddr_in));

	/* Close the socket */
	(void) close(sock);
}

/*
 * in_cksum
 *
 * Checksum routine for Internet Protocol family headers (C Version), taken
 * from Solaris ping.c.
 */
static ushort_t
in_cksum(ushort_t *addr, int len)
{
	int nleft = len;
	ushort_t *w = addr;
	ushort_t answer;
	ushort_t odd_byte = 0;
	int sum = 0;

	/*
	 *  Our algorithm is simple, using a 32 bit accumulator (sum),
	 *  we add sequential 16 bit words to it, and at the end, fold
	 *  back all the carry bits from the top 16 bits into the lower
	 *  16 bits.
	 */
	while (nleft > 1) {
		sum += *w++;
		nleft -= 2;
	}

	/* mop up an odd byte, if necessary */
	if (nleft == 1) {
		*(uchar_t *)(&odd_byte) = *(uchar_t *)w;
		sum += odd_byte;
	}

	/*
	 * add back carry outs from top 16 bits to low 16 bits
	 */
	/*lint -e702 */
	sum = (sum >> 16) + (sum & 0xffff);	/* add hi 16 to low 16 */
	/*lint -e702 */
	sum += (sum >> 16);			/* add carry */
	/*lint -e502 -e734 */
	answer = ~sum;				/* truncate to 16 bits */
	return (answer);
}
