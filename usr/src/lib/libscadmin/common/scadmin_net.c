/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * The following notice accompanied the original version of this file:
 *
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */


#pragma ident	"@(#)scadmin_net.c	1.8	08/05/20 SMI"

/*
 * libscadmin network related functions
 */

#include <scadmin/scadmin.h>

#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include <unistd.h>
#include <stropts.h>
#include <libdevinfo.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/sockio.h>
#include <sys/stat.h>

#include <net/if.h>

#define	ADMIN_PATH_TO_INST	"/etc/path_to_inst"

#define	IBD_DEV		"ibd"

static int admin_devfs_entry(di_node_t node, di_minor_t minor, void *arg);
static void admin_check_path_to_inst(char *adaptypes,
    scadmin_namelist_t **namelistp);

/*
 * Check if there is a link under /dev to the device tree for the given
 * interface name.
 */
static int
devtree_entry_exist(const char *ifname)
{
	struct stat buf;
	char devpath[64];

	(void) snprintf(devpath, sizeof (devpath), "/dev/%s", ifname);
	if (stat(devpath, &buf) < 0) {
		return (0);
	}
	return (1);
}

/*
 * scadmin_get_network_adapters
 *
 * Return a list of network adapters in the device tree.  A list of adapter
 * types may also be given.   If "adaptypes" is not NULL, it is a list
 * of known adapter types for which to also search.
 *
 * This function recogizes the following flags:
 *
 *	SCADMIN_ADAPTERS_ALL		- All adapters in device tree
 *	SCADMIN_ADAPTERS_AVAILABLE	- All available adapters
 *	SCADMIN_ADAPTERS_ACTIVE		- All active adapters
 *	SCADMIN_ADAPTERS_LIF		- All logical interfaces
 *
 * It is the caller's responsibility to free the returned namelist
 * by calling scadmin_free_namelist().
 *
 */
scadmin_namelist_t *
scadmin_get_network_adapters(uint_t flag, char *adaptypes)
{
	di_node_t root;
	scadmin_namelist_t *allnames = (scadmin_namelist_t *)0;
	scadmin_namelist_t *activenames = (scadmin_namelist_t *)0;
	scadmin_namelist_t *namelist = (scadmin_namelist_t *)0;
	scadmin_namelist_t *nl;

	/* Switch on flag */
	switch (flag) {
	case SCADMIN_ADAPTERS_ALL:
	case SCADMIN_ADAPTERS_AVAILABLE:

		/*
		 * If no list of adapter types was passed, we need to
		 * traverse the device tree in search of network devices.
		 * If a list is passed, we want to stick to that list only
		 * for supported adapter types.
		 */
		if (adaptypes == NULL) {

			/* Open the devinfo tree */
			if ((root = di_init("/", DINFOSUBTREE | DINFOMINOR))
				== DI_NODE_NIL)
				return (0);

			/* Walk the tree */
			(void) di_walk_minor(root, DDI_NT_NET, DI_CHECK_ALIAS,
				&allnames, admin_devfs_entry);

			/* Close the devinfo tree */
			di_fini(root);
		}

		/*
		 * If we were given an additional list of adapter types
		 * to check, search the /etc/path_to_inst file for any
		 * matching adapters which might be found there.
		 *
		 * We check the device tree in case our list becomes out
		 * of date.   And, we use our list of adapter types because
		 * not all network devices always show up in the device
		 * tree.
		 */
		admin_check_path_to_inst(adaptypes, &allnames);

		/* If only available adapters, weed out the unavailable */
		if (flag == SCADMIN_ADAPTERS_AVAILABLE) {

			/* Get the list of active adapters */
			activenames = scadmin_get_active_adapters(
			    SCADMIN_ADAPTERS_ACTIVE);

			/* Namelist should not include active adapters */
			for (nl = allnames;  nl;  nl = nl->scadmin_nlist_next) {
				if (scadmin_namelist_search(activenames,
				    nl->scadmin_nlist_name)) {
					continue;
				} else if (strncmp(nl->scadmin_nlist_name,
				    IBD_DEV, strlen(IBD_DEV)) == 0 &&
				    !devtree_entry_exist(
				    nl->scadmin_nlist_name)) {
					/*
					 * Infiniband device driver create
					 * admin instances in path_to_inst
					 * that are not actual instances
					 * and should be ignored.
					 */
					continue;
				} else {
					(void) scadmin_append_namelist(
					    &namelist, nl->scadmin_nlist_name);
				}
			}

			/* Free activenames */
			scadmin_free_namelist(activenames);

		} else {
			/* Namelist includes all - just copy */
			for (nl = allnames;  nl;  nl = nl->scadmin_nlist_next) {
				(void) scadmin_append_namelist(&namelist,
				    nl->scadmin_nlist_name);
			}
		}

		/* Get rid of allnames */
		scadmin_free_namelist(allnames);

		/* Done */
		break;

	case SCADMIN_ADAPTERS_ACTIVE:
	case SCADMIN_ADAPTERS_ACTIVE_LIF:

		/* Get the list of active adapters */
		namelist = scadmin_get_active_adapters(flag);

		/* Done */
		break;

	default:
		break;
	}

	return (namelist);
}

/*
 * admin_devfs_entry
 *
 * This is the di_walk_minor callback function.
 *
 * Most of this code originated in Solaris' ifconfig.c.
 */
static int
admin_devfs_entry(di_node_t node, di_minor_t minor, void *arg)
{
	scadmin_namelist_t **namelistp = (scadmin_namelist_t **)arg;
	char *devfstype;
	char *nodename;
	char *dev_ddm_name;
	char last_char;
	char name[MAXPATHLEN];
	char instance[10];

	/*
	 * Look for network devices only
	 */
	devfstype = di_minor_nodetype(minor);
	if ((devfstype == NULL) || (strcmp(devfstype, DDI_NT_NET) != 0))
		return (DI_WALK_CONTINUE);

	/*
	 * If the ddi_minor_data name doesn't match the devinfo node name,
	 * use the ddi_minor_data name. This is needed for devices
	 * that name devinfo names like SUNW,hme...:hme.
	 */
	nodename = di_node_name(node);
	if (di_minor_type(minor) == DDM_ALIAS &&
	    strcmp(di_minor_name(minor), nodename)) {
		(void) strcpy(name, di_minor_name(minor));
	} else {
		(void) strcpy(name, nodename);
	}

	if (di_minor_type(minor) == DDM_MINOR) {
		dev_ddm_name = di_minor_name(minor);
		last_char = dev_ddm_name[strlen(dev_ddm_name) - 1];
		if (last_char < '0' || last_char > '9')
			return (DI_WALK_CONTINUE);
	} else {
		(void) sprintf(instance, "%d", di_instance(node));
		(void) strcat(name, instance);
	}

	/* Okay, add it to the list */
	(void) scadmin_append_namelist(namelistp, name);

	return (DI_WALK_CONTINUE);
}

/*
 * admin_check_path_to_inst
 *
 * Use the "adaptypes" list to search the /etc/path_to_inst file
 * for addtitional devices.
 */
static void
admin_check_path_to_inst(char *adaptypes, scadmin_namelist_t **namelistp)
{
	FILE *fp;
	char buffer[BUFSIZ];
	char field1[BUFSIZ];
	char field3[BUFSIZ];
	char *driver = field3 + 1;		/* skip leading " */
	int instance;
	char **types;
	char **type;
	char *ptr;
	char *last;
	int count;
	uint_t i;

	/* Anything to do? */
	if (adaptypes == NULL)
		return;

	/* Add adaptypes to the "types" array */
	i = 1;
	for (ptr = adaptypes;  *ptr;  ptr++)
		if (*ptr == ',')
			++i;
	if ((types = (char **)calloc(i + 1, sizeof (char *))) == NULL)
		return;
	type = types;
	ptr = strtok_r(adaptypes, ",", &last);
	while (ptr) {
		*type++ = ptr;
		ptr = strtok_r(NULL, ",", &last);
	}

	/* Open path_to_inst */
	if ((fp = fopen(ADMIN_PATH_TO_INST, "r")) == NULL) {
		free(types);
		return;
	}

	/* Search the file for matching adapter types */
	while (fgets(buffer, sizeof (buffer), fp)) {

		/*
		 * There should be 3 fields on each line.
		 * The first and third fields are enclosed in double quotes.
		 */
		count = sscanf(buffer, "%s %d %s", field1, &instance, field3);
		if (count != 3 || *field1 != '\"')
			continue;

		/* Get rid of trailing double quote */
		if ((ptr = strchr(driver, '\"')) != NULL)
			*ptr = '\0';
		for (type = types; *type; ++type) {
			if (strcmp(*type, driver) == 0) {
				(void) sprintf(buffer, "%s%d", driver,
				    instance);
				if (!scadmin_namelist_search(*namelistp,
				    buffer)) {
					(void) scadmin_append_namelist(
					    namelistp, buffer);
				}
				break;
			}
		}
	}

	free(types);
	(void) fclose(fp);
}

/*
 * scadmin_get_active_adapters
 *
 * Return a list of all logical network interfaces which are active.
 *
 * This function recogizes the following flags:
 *
 *	SCADMIN_ADAPTERS_ACTIVE		- All active adapters
 *	SCADMIN_ADAPTERS_LIF		- All logical interfaces
 *
 * It is the caller's responsibility to free the returned namelist
 * by calling scadmin_free_namelist().
 *
 */
scadmin_namelist_t *
scadmin_get_active_adapters(uint_t flag)
{
	scadmin_namelist_t *namelist = (scadmin_namelist_t *)0;
	struct lifnum lifn;
	struct lifconf lifc, lifcall;
	struct lifreq *lifrp;
	struct lifreq lifrflag;
	int sock;
	uint_t numifs, numallifs;
	uint_t n;
	uint_t bufsize, allbufsize;
	char *buf;
	char *allbuf;
	char *name;
	char *ptr;

	/* Check */
	switch (flag) {
	case SCADMIN_ADAPTERS_ACTIVE:
	case SCADMIN_ADAPTERS_ACTIVE_LIF:
		break;

	default:
		return (0);
	}

	/* Initialize */
	bzero(&lifn, sizeof (lifn));

	/* Open a socket */
	if ((sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
		return (0);

	/* Get the number of external interfaces */
	lifn.lifn_family = AF_INET;
	lifn.lifn_flags = LIFC_EXTERNAL_SOURCE;
	if (ioctl(sock, SIOCGLIFNUM, (char *)&lifn) < 0) {
		(void) close(sock);
		return (0);
	}
	numifs = (uint_t)lifn.lifn_count;

	/* Allocate buffers based on number of external interfaces */
	bufsize = numifs * sizeof (struct lifreq);
	if ((buf = malloc(bufsize)) == NULL) {
		(void) close(sock);
		return (0);
	}

	/* Get the external interfaces */
	lifc.lifc_family = AF_INET;
	lifc.lifc_flags = LIFC_EXTERNAL_SOURCE;
	lifc.lifc_len = (int)bufsize;
	lifc.lifc_buf = buf;
	if (ioctl(sock, SIOCGLIFCONF, (char *)&lifc) < 0) {
		(void) close(sock);
		free(buf);
		return (0);
	}

	/* Add each external interface to the name list */
	lifrp = lifc.lifc_req;
	for (n = 0;  n < numifs;  n++, lifrp++) {
		name = lifrp->lifr_name;

		/* Skip any empties */
		if (!name)
			continue;

		/* Don't list logical names? */
		if (flag != SCADMIN_ADAPTERS_ACTIVE_LIF) {
			if ((ptr = strrchr(name, ':')) != NULL)
				*ptr = '\0';
			if (scadmin_namelist_search(namelist, name))
				continue;
		}

		(void) scadmin_append_namelist(&namelist, lifrp->lifr_name);
	}

	/* Get the number of NO_XMIT interfaces */
	lifn.lifn_flags = LIFC_NOXMIT;
	if (ioctl(sock, SIOCGLIFNUM, (char *)&lifn) < 0) {
		(void) close(sock);
		free(buf);
		return (0);
	}
	numallifs = (uint_t)lifn.lifn_count;

	/* Allocate buffers based on number of NO_XMIT interfaces */
	allbufsize = numallifs * sizeof (struct lifreq);
	if ((allbuf = malloc(allbufsize)) == NULL) {
		(void) close(sock);
		free(buf);
		return (0);
	}

	/* Get the NO_XMIT interfaces */
	lifcall.lifc_family = AF_INET;
	lifcall.lifc_flags = LIFC_NOXMIT;
	lifcall.lifc_len = (int)allbufsize;
	lifcall.lifc_buf = allbuf;
	if (ioctl(sock, SIOCGLIFCONF, (char *)&lifcall) < 0) {
		(void) close(sock);
		free(buf);
		free(allbuf);
		return (0);
	}

	lifrp = lifcall.lifc_req;
	for (n = 0;  n < numallifs;  n++, lifrp++) {
		name = lifrp->lifr_name;

		/* Skip any empties */
		if (!name)
			continue;

		(void) memset(&lifrflag, 0, sizeof (lifrflag));
		(void) strncpy(lifrflag.lifr_name, name,
		    sizeof (lifrflag.lifr_name));
		if (ioctl(sock, SIOCGLIFFLAGS, (char *)&lifrflag) < 0)
			continue;

		/*
		 * In this loop we try to add interfaces that may be NO_XMIT
		 * but deprecated as they are not added in the previous loop.
		 * We want to treat deprecated interfaces as active.
		 */
		if (!(lifrflag.lifr_flags & IFF_DEPRECATED))
			continue;

		/* Don't list logical names? */
		if (flag != SCADMIN_ADAPTERS_ACTIVE_LIF) {
			if ((ptr = strrchr(name, ':')) != NULL)
				*ptr = '\0';
			if (scadmin_namelist_search(namelist, name))
				continue;
		}

		(void) scadmin_append_namelist(&namelist, lifrp->lifr_name);
	}

	/* Clean up */
	(void) close(sock);
	free(buf);
	free(allbuf);

	return (namelist);
}
