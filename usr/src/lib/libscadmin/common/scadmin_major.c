/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident   "@(#)scadmin_major.c 1.3     08/05/20 SMI"

#include	<scadmin/scadmin.h>

#include	<stdio.h>
#include	<strings.h>
#include	<limits.h>

#define	ETC_NAME_TO_MAJOR_FILE	"/etc/name_to_major"

/*
 * scadmin_get_major_number
 *	Obtain the major number entry that corresponds with the selected
 *	'driver' as well as the maximum major number entry on this node.
 *
 *	Returns
 *		A pair of integers (driver_entry, max_entry) 	- success
 *		NULL						- failure
 *
 */
scadmin_namelist_t *
scadmin_get_major_number(char *driver)
{
	FILE	*fptr = NULL;
	int	value = 0;
	int	max_value_found = 0;
	int	driver_value = -1;
	char	cand[MAX_INPUT];
	char	buffer[MAX_INPUT];
	scadmin_namelist_t	*namelist = (scadmin_namelist_t *)0;

	/* Open the major number file */
	fptr = fopen(ETC_NAME_TO_MAJOR_FILE, "r");

	if (fptr == NULL)
		return (NULL);

	/*
	 * Read each line of the file and do two things:
	 * 	1) Determine the maximum major entry in the file
	 * 	2) If an entry for our driver exists, obtain its value.
	 *	   If it does not, then the value will be -1.
	 */
	while (fscanf(fptr, "%s %d", cand, &value) == 2) {
		if (strcmp(cand, driver) == 0) {
			/*
			 * If the driver has never been seen before, then let's
			 * set its value.
			 */
			if (driver_value == -1)
				driver_value = value;
			else
			/*
			 * Multiple driver entries are present in the file
			 * which is an unexpected configuration error.
			 */
				return (NULL);
		}
		if (value > max_value_found)
			max_value_found = value;
	}

	/* Close the file pointer */
	(void) fclose(fptr);

	/* Append the information to the namelist structure */
	(void) sprintf(buffer, "%d,%d", driver_value, max_value_found);
	if (scadmin_append_namelist(&namelist, buffer) != 0)
		return (NULL);
	else
		return (namelist);
}
