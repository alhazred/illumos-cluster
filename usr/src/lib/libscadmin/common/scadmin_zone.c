/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident   "@(#)scadmin_zone.c 1.2     08/05/20 SMI"

#include	<scadmin/scadmin.h>

#include	<stdio.h>
#include	<strings.h>
#include	<limits.h>

/*
 * scadmin_get_zonelist
 *
 *	Returns
 *		A list of xclusive IP zones on the node		- success
 *		NULL						- failure
 *
 */
scadmin_namelist_t *
scadmin_get_zonelist()
{
	char	zone_cmd[100];
	FILE	*file_ptr;
	char	buffer[MAX_INPUT];
	scadmin_namelist_t	*namelist = (scadmin_namelist_t *)0;

	(void) sprintf(zone_cmd, "/usr/sbin/zoneadm list -p | cut -d: -f2,7 "
	    "| grep ':excl'| cut -d: -f1");
	if ((file_ptr = popen(zone_cmd, "r")) != NULL) {
		while (fgets(buffer, MAX_INPUT, file_ptr) != NULL) {
			if (scadmin_append_namelist(&namelist, buffer) != 0) {
				(void) pclose(file_ptr);
				scadmin_free_namelist(namelist);
				return (NULL);
			}
		}
		(void) pclose(file_ptr);
		return (namelist);
	} else {
		return (NULL);
	}
}
