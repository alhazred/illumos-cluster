/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scadmin_misc.c	1.3	08/05/20 SMI"

/*
 * libscadmin miscellaneous functions
 */

#include <scadmin/scadmin.h>

#include <stdlib.h>
#include <string.h>

/*
 * scadmin_append_namelist
 *
 * Add a new "name" to the given scadmin namelist.
 *
 * Possible return values:
 *
 *	0		Success
 *	1		Not enough memory
 */
int
scadmin_append_namelist(scadmin_namelist_t **namelistp, char *name)
{
	scadmin_namelist_t **nlp;
	char *n2;

	/* Dup the name */
	if (name == NULL)
		return (0);
	if ((n2 = strdup(name)) == NULL)
		return (1);

	/* Find the end of the list */
	for (nlp = namelistp;  *nlp;  nlp = &(*nlp)->scadmin_nlist_next)
		;

	/* Add it */
	*nlp = (scadmin_namelist_t *)calloc(1, sizeof (scadmin_namelist_t));
	if (*nlp == (scadmin_namelist_t *)0) {
		free(n2);
		return (1);
	}
	(*nlp)->scadmin_nlist_name = n2;

	return (0);
}

/*
 * scadmin_namelist_search
 *
 * Find the given name in the namelist.
 *
 * This function returns NULL if the name is not there.
 *
 */
scadmin_namelist_t *
scadmin_namelist_search(scadmin_namelist_t *namelist, char *name)
{
	scadmin_namelist_t *nl;

	if (!name)
		return (0);

	for (nl = namelist;  nl;  nl = nl->scadmin_nlist_next)
		if (nl->scadmin_nlist_name &&
		    strcmp(name, nl->scadmin_nlist_name) == 0)
			return (nl);

	return (0);
}

/*
 * scadmin_namelist_print
 *
 * Print the namelist.
 *
 */
void
scadmin_namelist_print(char *prefix, scadmin_namelist_t *namelist, FILE *fp)
{
	scadmin_namelist_t *nl;
	char *line;

	if (!fp)
		return;

	for (nl = namelist;  nl;  nl = nl->scadmin_nlist_next) {
		line = nl->scadmin_nlist_name;
		if (line) {
			if (prefix) {
				(void) fprintf(fp, "%s:  %s", prefix, line);
			} else {
				(void) fputs(line, fp);
			}
			if (line[strlen(line) - 1] != '\n')
				(void) putc('\n', fp);
		}
	}
}

/*
 * scadmin_free_namelist
 *
 * Free an scadmin namelist.
 *
 */
void
scadmin_free_namelist(scadmin_namelist_t *namelist)
{
	register scadmin_namelist_t **nlp;
	register scadmin_namelist_t *last;

	/* check arg */
	if (namelist == (scadmin_namelist_t *)0)
		return;

	/* free memory for each member of the list */
	nlp = &namelist;
	while (*nlp) {
		if ((*nlp)->scadmin_nlist_name)
			free((*nlp)->scadmin_nlist_name);
		last = *nlp;

		*nlp = (*nlp)->scadmin_nlist_next;
		free(last);
	}
}
