/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * Common DLPI Test Suite header file
 */

#ifndef _SCADMIN_PRIVATE_DLTEST_H
#define	_SCADMIN_PRIVATE_DLTEST_H

#pragma ident	"@(#)scadmin_private_dltest.h	1.3	08/05/20 SMI"

#ifdef	__cplusplus
extern "C" {
#endif

#include <scadmin/scadmin.h>

#include <sys/types.h>

/*
 * Maximum control/data buffer size (in long's !!) for getmsg().
 */
#define		MAXDLBUF	8192

/*
 * Maximum number of seconds we'll wait for any
 * particular DLPI acknowledgment from the provider
 * after issuing a request.
 */
#define		MAXWAIT		5

/*
 * Maximum address buffer length.
 */
#define		MAXDLADDR	1024

/*
 * Maximum number of threads.
 */
#define		MAXTHREAD	1024


/*
 * Handy macro.
 */
#define		OFFADDR(s, n)	(uchar_t *)((char *)(s) + (int)(n))

/*
 * externs go here
 */
extern uint_t dlokack(int fd, char *ctlbuf, scadmin_namelist_t **errsp);
extern uint_t dlpromisconreq(int fd, ulong_t level, scadmin_namelist_t **errsp);
extern uint_t dlattachreq(int fd, ulong_t ppa, scadmin_namelist_t **errsp);
extern uint_t dlinforeq(int fd, scadmin_namelist_t **errsp);
extern uint_t dlinfoack(int fd, char *ctlbuf, scadmin_namelist_t **errsp);
extern uint_t dlbindreq(int fd, ulong_t sap, ulong_t max_conind,
    ulong_t service_mode, ulong_t conn_mgmt, ulong_t xidtest,
    scadmin_namelist_t **errsp);
extern uint_t dlbindack(int fd, char *ctlbuf, scadmin_namelist_t **errsp);
extern char *dlmactype(ulong_t media);
extern uint_t dlunitdataind(int fd, char *ctlbuf, char *databuf, int *datalen,
    scadmin_namelist_t **errsp);
extern uint_t dlunitdatareq(int fd, uchar_t *addrp, int addrlen, ulong_t minpri,
    ulong_t maxpri, uchar_t *datap, int datalen, scadmin_namelist_t **errsp);

#ifdef	__cplusplus
}
#endif

#endif	/* _SCADMIN_PRIVATE_DLTEST_H */
