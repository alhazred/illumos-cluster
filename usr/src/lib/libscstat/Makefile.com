#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.35	08/05/20 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# lib/libscstat/Makefile.com
#

LIBRARYCCC= libscstat.a
VERS=.1

OBJECTS= \
	scstat_cluster.o \
        scstat_node.o \
        scstat_transport.o \
        scstat_hadev.o \
        scstat_quorum.o \
        scstat_ipmp.o \
        scstat_rg.o

# include library definitions
include ../../Makefile.lib

MAPFILE=	../common/mapfile-vers
SRCS=		$(OBJECTS:%.o=../common/%.c)

LIBS =		$(DYNLIBCCC)

MTFLAG	=	-mt
CPPFLAGS +=	-v -I$(SRC)/common/cl
CPPFLAGS +=	-I..
PMAP =		-M $(MAPFILE)
LINTFLAGS += 	-I..
LINTFILES +=	$(OBJECTS:%.o=%.ln)
LDLIBS +=       -lclconf -lrgm -lpnm -lscha -lscconf -lc

.KEEP_STATE:

$(DYNLIB):	$(MAPFILE)

FRC:

# include library targets
include ../../Makefile.targ

objs/%.o pics/%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<
