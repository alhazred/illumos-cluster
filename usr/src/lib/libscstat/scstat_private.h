/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SCSTAT_PRIVATE_H
#define	_SCSTAT_PRIVATE_H

#pragma ident	"@(#)scstat_private.h	1.18	08/05/20 SMI"

/*
 * libscstat private functions
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <locale.h>
#include <libintl.h>
#include <sys/types.h>
#include <rgm/rgm_scrgadm.h>
#include <scadmin/scconf.h>
#include <scadmin/scstat.h>
#include <scxcfg/scxcfg.h>
#include <dc/libdcs/libdcs.h>
#include <rgm/pnm.h>

#ifdef _KERNEL_ORB
#include <orbtest/trans_tests/unode_trtests.h>
#endif

/* scstat_cluster.c */
/*
 * stat_get_cached_cluster_name
 *
 * Get cluster name from cached cluster configuration. Upon successful,
 * the name of the cluster is returned and it is the caller's
 * responsibility to free it.
 *
 * Possible return value:
 *	SCSTAT_ENOERR		- no error
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EPERM            - not root
 *      SCSTAT_ENOTCLUSTER      - there is no cluster
 *      SCCONF_EINVAL           - invalid argument
 *      SCCONF_EUNEXPECTED      - internal or unexpected error
 */
scstat_errno_t
stat_get_cached_cluster_name(const scconf_cfg_cluster_t *, char **);

/*
 * stat_get_object_type
 *
 * Get object type code from type string.
 *
 * Possible return value:
 *	SCSTAT_TYPE_CLUSTER
 *	SCSTAT_TYPE_RT
 *	SCSTAT_TYPE_RG
 *	SCSTAT_TYPE_RESOURCE
 *	SCSTAT_TYPE_NODE
 *	SCSTAT_TYPE_DEVICE_QUORUM
 *	SCSTAT_TYPE_HA_DEVICE_SERVICE
 *	SCSTAT_TYPE_PATH
 *	SCSTAT_TYPE_UNKNOWN
 */
extern scstat_type_t stat_get_object_type(const char *);

/*
 * stat_get_state_code_from_quorum_state
 *
 * Map quorum state to state code.
 *
 * Possible return values:
 *
 *	ONLINE		- resource is running
 *	OFFLINE		- resource is stopped due to user action
 *	FAULTED		- resource is stopped due to a failure
 *	DEGRADED	- resource is running but has a minor problem
 *	WAIT		- resource is in transition from a state to another
 *	UNKNOWN		- resource is monitored but state of the resource is
 *			  not known because either the monitor went down or
 *			  the monitor cannot report resource state temporarily.
 *	NOT_MONITORED	- There is no monitor to check state of the resource
 */
extern scstat_state_code_t stat_get_state_code_from_quorum_state(
    const quorum_state_t);

/* scstat_hadev.c */
/*
 * stat_convert_dc_error_code
 *
 * convert a dcs error code to scstat error code
 *
 * Possible return values:
 *
 *	SCSTAT_NOERR		- success
 *	SCSTAT_ENOTCLUSTER	- not cluster member
 *	SCSTAT_ESERVICENAME	- invalid service name
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
extern scstat_errno_t stat_convert_dc_error_code(
    const dc_error_t);

/* scstat_node.c */

/* scstat_quorum.c */

/*
 * scstat_convert_quorum_error_code
 *
 * convert a quorum error code to scstat error code
 *
 * Possible return values:
 *
 *	SCSTAT_NOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
extern scstat_errno_t scstat_convert_quorum_error_code(const int quorum_err);

/* scstat_rg.c */

/*
 * stat_convert_scha_error_code
 *
 * convert a scha error code to scstat error code
 *
 * Possible return values:
 *
 *	SCSTAT_ENOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EINVAL		- invalid argument
 *	SCSTAT_ERECONFIG	- cluster is reconfiguring
 *	SCSTAT_EOBSOLETE	- Resource/RG has been updated
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
extern scstat_errno_t stat_convert_scha_error_code(const scha_errmsg_t);

/*
 * stat_convert_scha_rgstate_to_sc_state_code
 *
 * convert a scha_rgstate to sc_state_code_t
 *
 * Possible return values:
 *
 *	ONLINE		- resource is running
 *	OFFLINE		- resource is stopped due to user action
 *	FAULTED		- resource is stopped due to a failure
 *	DEGRADED	- resource is running but has a minor problem
 *	WAIT		- resource is in transition from a state to another
 *	UNKNOWN		- resource is monitored but state of the resource is
 *			  not known because either the monitor went down or
 *			  the monitor cannot report resource state temporarily.
 *	NOT_MONITORED	- There is no monitor to check state of the resource
 */
extern sc_state_code_t stat_convert_scha_rgstate_to_sc_state_code(
    const scha_rgstate_t);

/*
 * stat_convert_scha_rsstatus_to_sc_state_code
 *
 * convert a scha_status_t to sc_state_code_t
 *
 * Possible return values:
 *
 *	ONLINE		- resource is running
 *	OFFLINE		- resource is stopped due to user action
 *	FAULTED		- resource is stopped due to a failure
 *	DEGRADED	- resource is running but has a minor problem
 *	WAIT		- resource is in transition from a state to another
 *	UNKNOWN		- resource is monitored but state of the resource is
 *			  not known because either the monitor went down or
 *			  the monitor cannot report resource state temporarily.
 *	NOT_MONITORED	- There is no monitor to check state of the resource
 */
extern sc_state_code_t stat_convert_scha_rsstatus_to_sc_state_code(
    const scha_rsstatus_t);

/*
 * stat_get_default_rsstatus_string
 *
 * Get resource status string from resource status code. The supplied "text"
 * should be of at least SCSTAT_MAX_STRING_LEN.
 */
extern void stat_get_default_rsstatus_string(const scha_rsstatus_t, char *);

/*
 * stat_get_default_rsstate_string
 *
 * Get resource state string from resource state code. The supplied "text"
 * should be of at least SCSTAT_MAX_STRING_LEN.
 */
extern void stat_get_default_rsstate_string(const scha_rsstate_t, char *);

/* scstat_transport.c */

#ifdef __cplusplus
}
#endif

#endif	/* _SCSTAT_PRIVATE_H */
