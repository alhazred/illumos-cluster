/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scstat_rg.c	1.35	08/07/24 SMI"

/*
 * libscstat cluster ha device service status functions
 */

#include <scstat_private.h>

/*
 * stat_convert_scha_error_code
 *
 * convert a scha error code to scstat error code
 *
 * Possible return values:
 *
 *	SCSTAT_ENOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EINVAL		- invalid argument
 *	SCSTAT_ERECONFIG	- cluster is reconfiguring
 *	SCSTAT_EOBSOLETE	- Resource/RG has been updated
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
/* lint report a C++ error */
/*lint -save -e1746 */
scstat_errno_t
stat_convert_scha_error_code(const scha_errmsg_t scha_error)
{
	scstat_errno_t error = SCSTAT_EUNEXPECTED;

	switch (scha_error.err_code) {
	case SCHA_ERR_NOERR:
	case SCHA_ERR_SEQID:
		error = SCSTAT_ENOERR;
		break;
	case SCHA_ERR_NOMEM:
		error = SCSTAT_ENOMEM;
		break;
	case SCHA_ERR_RT:
	case SCHA_ERR_RG:
	case SCHA_ERR_RSRC:
		error = SCSTAT_EOBSOLETE;
		break;
	case SCHA_ERR_RECONF:
	case SCHA_ERR_CLRECONF:
		error = SCSTAT_ECLUSTERRECONFIG;
		break;
	case SCHA_ERR_RGRECONF:
		error = SCSTAT_ERGRECONFIG;
		break;
	case SCHA_ERR_NODE:
	case SCHA_ERR_MEMBER:
		error = SCSTAT_ENOTCLUSTER;
		break;
	case SCHA_ERR_INVAL:
		error = SCSTAT_EINVAL;
		break;
	case SCHA_ERR_CCR:
	case SCHA_ERR_ACCESS:
	case SCHA_ERR_PROP:
	case SCHA_ERR_TAG:
	case SCHA_ERR_HANDLE:
	case SCHA_ERR_ORB:
	case SCHA_ERR_INTERNAL:
	case SCHA_ERR_DEPEND:
	case SCHA_ERR_STATE:
	case SCHA_ERR_METHOD:
	case SCHA_ERR_CHECKS:
	case SCHA_ERR_RSTATUS:
	default:
		break;
	}
	return (error);
}
/*lint -restore */

/*
 * stat_convert_scha_rgstate_to_sc_state_code
 *
 * convert a scha_rgstate to sc_state_code_t
 *
 * Possible return values:
 *
 *	SC_STATE_ONLINE		- resource is running
 *	SC_STATE_OFFLINE	- resource is stopped due to user action
 *	SC_STATE_FAULTED	- resource is stopped due to a failure
 *	SC_STATE_DEGRADED	- resource is running but has a minor problem
 *	SC_STATE_WAIT		- resource is in transition from a state to
 *				  another
 *	SC_STATE_UNKNOWN	- resource is monitored but state of the
 *				  resource is not known because either the
 *				  monitor went down or the monitor cannot
 *				  report resource state temporarily.
 *	SC_STATE_NOT_MONITORED	- There is no monitor to check state of the
 *				  resource
 */
sc_state_code_t
stat_convert_scha_rgstate_to_sc_state_code(const scha_rgstate_t rgstate)
{
	sc_state_code_t state = SC_STATE_UNKNOWN;

	/* map rg state to status code */
	switch (rgstate) {
	case SCHA_RGSTATE_UNMANAGED:
		state = SC_STATE_NOT_MONITORED;
		break;
	case SCHA_RGSTATE_OFFLINE:
		state = SC_STATE_OFFLINE;
		break;
	case SCHA_RGSTATE_ONLINE:
		state = SC_STATE_ONLINE;
		break;
	case SCHA_RGSTATE_PENDING_ONLINE:
		state = SC_STATE_WAIT;
		break;
	case SCHA_RGSTATE_PENDING_ONLINE_BLOCKED:
		state = SC_STATE_ONLINE;
		break;
	case SCHA_RGSTATE_PENDING_OFFLINE:
		state = SC_STATE_WAIT;
		break;
	case SCHA_RGSTATE_ERROR_STOP_FAILED:
		state = SC_STATE_FAULTED;
		break;
	/* online faulted counts as online */
	case SCHA_RGSTATE_ONLINE_FAULTED:
		state = SC_STATE_ONLINE;
		break;
	default:
		break;
	}

	return (state);
}

/*
 * scstat_get_default_rgstate_string
 *
 * Get rgstate string from rgstate code. The supplied "text" should
 * be of at least SCSTAT_MAX_STRING_LEN.
 */
void
scstat_get_default_rgstate_string(const scha_rgstate_t rgstate,
    char *text)
{
	/* Check arguments */
	if (text == NULL)
		return;

	/* I18N houskeeping */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/* Make sure that the buffer is terminated */
	text[SCSTAT_MAX_STRING_LEN - 1] = '\0';

	switch (rgstate) {
	case SCHA_RGSTATE_UNMANAGED:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Unmanaged"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCHA_RGSTATE_OFFLINE:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Offline"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCHA_RGSTATE_ONLINE:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Online"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCHA_RGSTATE_PENDING_ONLINE:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Pending online"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCHA_RGSTATE_PENDING_ONLINE_BLOCKED:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    "Pending online blocked"), SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCHA_RGSTATE_PENDING_OFFLINE:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Pending offline"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCHA_RGSTATE_ERROR_STOP_FAILED:
		(void) strncpy(text,
		    dgettext(TEXT_DOMAIN, "Error--stop failed"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCHA_RGSTATE_ONLINE_FAULTED:
		(void) strncpy(text,
		    dgettext(TEXT_DOMAIN, "Online faulted"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	default:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Unknown"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	}
}

/*
 * scstat_get_default_rgstate_string_en
 *
 *	Same as scstat_get_default_rgstate_string but don't do any localization
 * 	stuff.
 */
void
stat_get_default_rgstate_string_en(const scha_rgstate_t rgstate,
    char *text)
{
	/* Check arguments */
	if (text == NULL)
		return;

	/* Make sure that the buffer is terminated */
	text[SCSTAT_MAX_STRING_LEN - 1] = '\0';

	switch (rgstate) {
	case SCHA_RGSTATE_UNMANAGED:
		(void) strncpy(text, "Unmanaged", SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCHA_RGSTATE_OFFLINE:
		(void) strncpy(text, "Offline", SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCHA_RGSTATE_ONLINE:
		(void) strncpy(text, "Online", SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCHA_RGSTATE_PENDING_ONLINE:
		(void) strncpy(text, "Pending online",
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCHA_RGSTATE_PENDING_ONLINE_BLOCKED:
		(void) strncpy(text, "Pending online blocked",
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCHA_RGSTATE_PENDING_OFFLINE:
		(void) strncpy(text, "Pending offline",
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCHA_RGSTATE_ERROR_STOP_FAILED:
		(void) strncpy(text, "Error--stop failed",
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCHA_RGSTATE_ONLINE_FAULTED:
		(void) strncpy(text, "Online faulted",
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	default:
		(void) strncpy(text, "Unknown", SCSTAT_MAX_STRING_LEN - 1);
		break;
	}
}

/*
 * stat_convert_scha_rsstatus_to_sc_state_code
 *
 * convert a scha_status_t to sc_state_code_t
 *
 * Possible return values:
 *
 *	SC_STATE_ONLINE		- resource is running
 *	SC_STATE_OFFLINE	- resource is stopped due to user action
 *	SC_STATE_FAULTED	- resource is stopped due to a failure
 *	SC_STATE_DEGRADED	- resource is running but has a minor problem
 *	SC_STATE_WAIT		- resource is in transition from a state to
 *				  another
 *	SC_STATE_UNKNOWN	- resource is monitored but state of the
 *				  resource is not known because either the
 *				  monitor went down or the monitor cannot
 *				  report resource state temporarily.
 *	SC_STATE_NOT_MONITORED	- There is no monitor to check state of the
 *				  resource
 */
sc_state_code_t
stat_convert_scha_rsstatus_to_sc_state_code(const scha_rsstatus_t rsstatus)
{
	sc_state_code_t state = SC_STATE_UNKNOWN;

	/* map resource status to sc status code */
	switch (rsstatus) {
	case SCHA_RSSTATUS_OK:
		state = SC_STATE_ONLINE;
		break;
	case SCHA_RSSTATUS_DEGRADED:
		state = SC_STATE_DEGRADED;
		break;
	case SCHA_RSSTATUS_FAULTED:
		state = SC_STATE_FAULTED;
		break;
	case SCHA_RSSTATUS_OFFLINE:
		state = SC_STATE_OFFLINE;
		break;
	case SCHA_RSSTATUS_UNKNOWN:
	default:
		state = SC_STATE_UNKNOWN;
		break;
	}

	return (state);
}

/*
 * stat_get_default_rsstatus_string
 *
 * Get resource status string from resource status code. The supplied "text"
 * should be of at least SCSTAT_MAX_STRING_LEN.
 */
void
stat_get_default_rsstatus_string(const scha_rsstatus_t rsstatus,
    char *text)
{
	/* Check arguments */
	if (text == NULL)
		return;

	/* I18N houskeeping */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/* Make sure that the buffer is terminated */
	text[SCSTAT_MAX_STRING_LEN - 1] = '\0';

	switch (rsstatus) {
	case SCHA_RSSTATUS_OK:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Online"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCHA_RSSTATUS_DEGRADED:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Degraded"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCHA_RSSTATUS_FAULTED:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Faulted"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCHA_RSSTATUS_OFFLINE:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Offline"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCHA_RSSTATUS_UNKNOWN:
	default:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Unknown"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	}
}

/*
 * stat_get_default_rsstate_string
 *
 * Get resource state string from resource state code. The supplied "text"
 * should be of at least SCSTAT_MAX_STRING_LEN.
 */
void
stat_get_default_rsstate_string(const scha_rsstate_t rsstate,
    char *text)
{
	/* Check arguments */
	if (text == NULL)
		return;

	/* I18N houskeeping */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/* Make sure that the buffer is terminated */
	text[SCSTAT_MAX_STRING_LEN - 1] = '\0';

	switch (rsstate) {
	case SCHA_RSSTATE_OFFLINE:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Offline"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCHA_RSSTATE_ONLINE:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Online"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCHA_RSSTATE_STOP_FAILED:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Stop failed"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCHA_RSSTATE_START_FAILED:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Start failed"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCHA_RSSTATE_MONITOR_FAILED:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Monitor failed"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCHA_RSSTATE_ONLINE_NOT_MONITORED:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    "Online but not monitored"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCHA_RSSTATE_DETACHED:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Detached"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCHA_RSSTATE_STARTING:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Starting"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCHA_RSSTATE_STOPPING:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Stopping"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	default:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Unknown"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	}
}

/*
 * stat_get_default_rsstate_string_en
 *
 *	Same as stat_get_default_rsstate_string but don't do any
 *	localization stuff.
 */
void
stat_get_default_rsstate_string_en(const scha_rsstate_t rsstate,
    char *text)
{
	/* Check arguments */
	if (text == NULL)
		return;

	/* Make sure that the buffer is terminated */
	text[SCSTAT_MAX_STRING_LEN - 1] = '\0';

	switch (rsstate) {
	case SCHA_RSSTATE_OFFLINE:
		(void) strncpy(text, "Offline", SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCHA_RSSTATE_ONLINE:
		(void) strncpy(text, "Online", SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCHA_RSSTATE_STOP_FAILED:
		(void) strncpy(text, "Stop failed", SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCHA_RSSTATE_START_FAILED:
		(void) strncpy(text, "Start failed", SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCHA_RSSTATE_MONITOR_FAILED:
		(void) strncpy(text, "Monitor failed",
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCHA_RSSTATE_ONLINE_NOT_MONITORED:
		(void) strncpy(text, "Online but not monitored",
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCHA_RSSTATE_DETACHED:
		(void) strncpy(text, "Detached", SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCHA_RSSTATE_STARTING:
		(void) strncpy(text, "Starting", SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCHA_RSSTATE_STOPPING:
		(void) strncpy(text, "Stopping", SCSTAT_MAX_STRING_LEN - 1);
		break;
	default:
		(void) strncpy(text, "Unknown", SCSTAT_MAX_STRING_LEN - 1);
		break;
	}
}

/*
 * scstat_get_rgs
 *
 * Upon success, a list of objects of scstat_rg_t are returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_ENOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EINVAL		- invalid argument
 *	SCSTAT_ERECONFIG	- cluster is reconfiguring
 *	SCSTAT_EOBSOLETE	- Resource/RG has been updated
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t
scstat_get_rgs(scstat_rg_t **pplrgs)
{
	scstat_errno_t error = SCSTAT_ENOERR;
	scha_errmsg_t rgm_status = {SCHA_ERR_NOERR, NULL};
	char **managed_rgs = NULL;
	char **unmanaged_rgs = NULL;
	char ** pprgs = NULL;
	char *rg_name;
	scstat_rg_t *prg = NULL;
	scstat_rg_t *prg_current = NULL;

	if (pplrgs == NULL) {
		error = SCSTAT_EINVAL;
		goto cleanup;
	}

	if (rgm_orbinit().err_code != SCHA_ERR_NOERR)
		return (SCSTAT_EUNEXPECTED);

	/* get name for all rgs */
	rgm_status =
	    rgm_scrgadm_getrglist(&managed_rgs, &unmanaged_rgs, B_FALSE, NULL);
	/*
	 * NULL means get for default cluster scope.
	 * we can decide to have a new api _cl which will accept
	 * an extra argument specifying the clustername if a different
	 * scope needs to be chosen. This might be cleaner than adding
	 * a new argument to the same old function.
	 */
	error = stat_convert_scha_error_code(rgm_status);
	if (error != SCSTAT_ENOERR)
			goto cleanup;

	/* get all managed rg */
	if (managed_rgs != NULL) {
		pprgs = managed_rgs;
		rg_name = *pprgs++;
		while (rg_name != NULL) {
			error = scstat_get_rg(rg_name, &prg, B_FALSE);
			if (error == SCSTAT_EOBSOLETE) { /* skip */
				scstat_free_rg(prg);
				prg = NULL;

				error = SCSTAT_ENOERR;
				rg_name = *pprgs++;
				continue;
			}

			if (error != SCSTAT_ENOERR)
				goto cleanup;

			rg_name = *pprgs++;

			/* append rg to list */
			if (prg_current != NULL)
				prg_current->scstat_rg_next = prg;
			else /* first element */
				*pplrgs = prg;

			prg_current = prg;

			prg = NULL;
		}
	}

	/* get all unmanaged rg */
	if (unmanaged_rgs != NULL) {
		pprgs = unmanaged_rgs;
		rg_name = *pprgs++;
		while (rg_name != NULL) {
			error =	scstat_get_rg(rg_name, &prg, B_FALSE);
			if (error == SCSTAT_EOBSOLETE) { /* skip */
				scstat_free_rg(prg);
				prg = NULL;

				error = SCSTAT_ENOERR;
				rg_name = *pprgs++;
				continue;
			}

			if (error != SCSTAT_ENOERR)
				goto cleanup;

			rg_name = *pprgs++;

			/* append rg to list */
			if (prg_current != NULL)
				prg_current->scstat_rg_next = prg;
			else /* first element */
				*pplrgs = prg;

			prg_current = prg;

			prg = NULL;
		}
	}

cleanup:
	/* free managed rg list */
	if (managed_rgs != NULL) {
		pprgs = managed_rgs;
		while (*pprgs != NULL) {
			free(*pprgs);
			pprgs++;
		}
		free(managed_rgs);
	}

	/* free unmanaged rg list */
	if (unmanaged_rgs != NULL) {
		pprgs = unmanaged_rgs;
		while (*pprgs != NULL) {
			free(*pprgs);
			pprgs++;
		}
		free(unmanaged_rgs);
	}

	scstat_free_rg(prg);

	return (error);
}

/*
 * scstat_free_rgs
 *
 * Free all memory associated with a scstat_rg_t structure.
 */
void
scstat_free_rgs(scstat_rg_t *plrgs)
{
	scstat_rg_t *prg = plrgs;
	scstat_rg_t *prg_next;

	while (prg != NULL) {
		prg_next = prg->scstat_rg_next;

		scstat_free_rg(prg);
		prg = prg_next;
	}
}

/*
 * scstat_get_rg
 *
 * Upon success, an object of scstat_rg_t is returned.
 * The caller is responsible for freeing the space.
 * To specify a zone cluster context, the RG name should be
 * scoped with the zone cluster name.
 *
 * Possible return values:
 *
 *	SCSTAT_ENOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EINVAL		- invalid argument
 *	SCSTAT_ERECONFIG	- cluster is reconfiguring
 *	SCSTAT_EOBSOLETE	- Resource/RG has been updated
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t
scstat_get_rg(const scstat_rg_name_t rg_name,
    scstat_rg_t **pprg, boolean_t newcli_caller)
{
	scstat_errno_t error = SCSTAT_ENOERR;
	scha_resourcegroup_t handle = NULL;
	unsigned int free_handle = 0;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	scha_rgstate_t rg_state;
	scstat_rg_t *prg = NULL;
	scha_str_array_t *plnodes = NULL;
	char *node_name;
	scstat_rg_status_t *prg_status = NULL;
	scstat_rg_status_t *prg_status_current = NULL;
	unsigned int i;
	char *rg_part, *zc_part = NULL;
	scconf_errno_t scconf_err = SCCONF_NOERR;

	if (rg_name == NULL) {
		error = SCSTAT_EINVAL;
		goto cleanup;
	}

	if (pprg == NULL) {
		error = SCSTAT_EINVAL;
		goto cleanup;
	}

	/* Get the RG name and the zone cluster name */
	rg_part = zc_part = NULL;
	scconf_err = scconf_parse_obj_name_from_cluster_scope(rg_name,
						&rg_part, &zc_part);

	if (scconf_err != SCCONF_NOERR) {
		error = SCSTAT_EINVAL;
		goto cleanup;
	}

	scha_status.err_code = scha_resourcegroup_open_zone(zc_part,
					rg_part, &handle);
	error = stat_convert_scha_error_code(scha_status);
	if (error != SCSTAT_ENOERR)
		goto cleanup;

	free_handle = 1;

	/* get primary list */
	scha_status.err_code =
	    scha_resourcegroup_get_zone(zc_part, handle, SCHA_NODELIST,
					&plnodes);
	error = stat_convert_scha_error_code(scha_status);
	if (error != SCSTAT_ENOERR)
		goto cleanup;

	prg = (scstat_rg_t *)calloc(1, sizeof (scstat_rg_t));
	if (prg == NULL) {
		error = SCSTAT_ENOMEM;
		goto cleanup;
	}

	/* fill in rg structure */
	prg->scstat_rg_name = strdup(rg_name);
	if (prg->scstat_rg_name == NULL) {
		error = SCSTAT_ENOMEM;
		goto cleanup;
	}

	/* get rg status on every node */
	for (i = 0; i < plnodes->array_cnt; i++) {
		node_name = plnodes->str_array[i];

		/* get rg state on every node */
		scha_status.err_code =
		    scha_resourcegroup_get_zone(zc_part, handle,
			SCHA_RG_STATE_NODE, node_name, &rg_state);
		error = stat_convert_scha_error_code(scha_status);

		/* if node is not a cluster member, ignore it */
		if (error == SCSTAT_ENOTCLUSTER) {
			error = SCSTAT_ENOERR;
			continue;
		}

		/* exit if other error is found */
		if (error != SCSTAT_ENOERR)
			goto cleanup;

		prg_status =
		    (scstat_rg_status_t *)calloc(1,
		    sizeof (scstat_rg_status_t));
		if (prg_status == NULL) {
			error = SCSTAT_ENOMEM;
			goto cleanup;
		}

		prg_status->scstat_rg_statstr =
		    (char *)calloc(1, SCSTAT_MAX_STRING_LEN);
		if (prg_status->scstat_rg_statstr == NULL) {
			error = SCSTAT_ENOMEM;
			goto cleanup;
		}

		prg_status->scstat_node_name = strdup(node_name);
		if (prg_status->scstat_node_name == NULL) {
			error = SCSTAT_ENOMEM;
			goto cleanup;
		}

		/* map rg state to status code */
		prg_status->scstat_rg_status_by_node =
		    stat_convert_scha_rgstate_to_sc_state_code(rg_state);

		if (newcli_caller)
			stat_get_default_rgstate_string_en(rg_state,
			    prg_status->scstat_rg_statstr);
		else
			scstat_get_default_rgstate_string(rg_state,
			    prg_status->scstat_rg_statstr);

		/* append rg status to list */
		if (prg->scstat_rg_status_list == NULL)
			/* first element */
			prg->scstat_rg_status_list = prg_status;
		else
			prg_status_current->scstat_rg_status_by_node_next =
			    prg_status;
		prg_status_current = prg_status;

		prg_status = NULL;
	}

	error =	scstat_get_rss(rg_name, &prg->scstat_rs_list);
	if (error != SCSTAT_ENOERR)
		goto cleanup;

	*pprg = prg;
	prg = NULL; /* so we don't accidently free it during clean up */

cleanup:
	if (free_handle == 1)
		(void) scha_resourcegroup_close(handle);

	scstat_free_rg(prg);

	if (prg_status != NULL) {
		if (prg_status->scstat_node_name != NULL)
			free(prg_status->scstat_node_name);
		if (prg_status->scstat_rg_statstr != NULL)
			free(prg_status->scstat_rg_statstr);
		free(prg_status);
	}
	if (rg_part) {
		free(rg_part);
	}
	if (zc_part) {
		free(zc_part);
	}

	return (error);
}

/*
 * scstat_free_rg
 *
 * Free all memory associated with a scstat_rg_t structure.
 */
void
scstat_free_rg(scstat_rg_t *prg)
{
	scstat_rg_status_t *prg_status;
	scstat_rg_status_t *prg_status_next;

	if (prg != NULL) {
		/* free scstat_rg_t */
		if (prg->scstat_rg_name != NULL)
			free(prg->scstat_rg_name);

		prg_status = prg->scstat_rg_status_list;
		while (prg_status != NULL) {
			prg_status_next = prg_status->
			    scstat_rg_status_by_node_next;

			/* free scstat_rg_status_t */
			if (prg_status->scstat_node_name != NULL)
				free(prg_status->scstat_node_name);

			if (prg_status->scstat_rg_statstr != NULL)
				free(prg_status->scstat_rg_statstr);

			free(prg_status);

			prg_status = prg_status_next;
		}

		scstat_free_rss(prg->scstat_rs_list);

		free(prg);
	}
}

/*
 * scstat_get_rss
 *
 * Upon success, a list of objects of scstat_rs_t are returned.
 * The caller is responsible for freeing the space. To specify
 * a zone cluster context, the RG name should be scoped with a
 * zone cluster context.
 *
 * Possible return values:
 *
 *	SCSTAT_ENOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EINVAL		- invalid argument
 *	SCSTAT_ERECONFIG	- cluster is reconfiguring
 *	SCSTAT_EOBSOLETE	- Resource/RG has been updated
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t scstat_get_rss(const scstat_rg_name_t rg_name,
    scstat_rs_t **pplrss)
{
	scstat_errno_t error = SCSTAT_ENOERR;
	scha_errmsg_t rgm_status = {SCHA_ERR_NOERR, NULL};
	char **rss = NULL;
	char **pprss = NULL;
	char *rs_name;
	scstat_rs_t *prs = NULL;
	scstat_rs_t *prs_current = NULL;
	char *rg_part, *zc_part = NULL;
	scconf_errno_t scconf_err = SCCONF_NOERR;

	if (rg_name == NULL) {
		error = SCSTAT_EINVAL;
		goto cleanup;
	}

	if (pplrss == NULL) {
		error = SCSTAT_EINVAL;
		goto cleanup;
	}

	if (rgm_orbinit().err_code != SCHA_ERR_NOERR)
		return (SCSTAT_EUNEXPECTED);

	/* Get the RG name and the zone cluster name */
	rg_part = zc_part = NULL;
	scconf_err = scconf_parse_obj_name_from_cluster_scope(rg_name,
						&rg_part, &zc_part);

	if (scconf_err != SCCONF_NOERR) {
		error = SCSTAT_EINVAL;
		goto cleanup;
	}

	/* get name for all resources on this rg */
	rgm_status = rgm_scrgadm_getrsrclist(rg_part, &rss, zc_part);
	error = stat_convert_scha_error_code(rgm_status);
	if (error != SCSTAT_ENOERR)
			goto cleanup;

	if (rss != NULL) {
		pprss = rss;
		while (*pprss != NULL) {
			rs_name = *pprss++;
			error = scstat_get_rs(rg_name, rs_name, &prs, B_FALSE);
			if (error == SCSTAT_EOBSOLETE) { /* skip */
				scstat_free_rs(prs);
				prs = NULL;

				error = SCSTAT_ENOERR;
				continue;
			}

			if (error != SCSTAT_ENOERR)
				goto cleanup;

			/* append rs to list */
			if (prs_current != NULL)
				prs_current->scstat_rs_next = prs;
			else /* first element */
				*pplrss = prs;

			prs_current = prs;

			prs = NULL;
		}
	}

cleanup:
	/* free rs list */
	if (rss != NULL) {
		pprss = rss;
		while (*pprss != NULL) {
			free(*pprss);
			pprss++;
		}
		free(rss);
	}
	if (rg_part) {
		free(rg_part);
	}
	if (zc_part) {
		free(zc_part);
	}

	scstat_free_rs(prs);

	return (error);
}

/*
 * scstat_free_rss
 *
 * Free all memory associated with a scstat_rs_t structure.
 */
void
scstat_free_rss(scstat_rs_t *plrss)
{
	scstat_rs_t *prs = plrss;
	scstat_rs_t *prs_next;

	while (prs != NULL) {
		prs_next = prs->scstat_rs_next;

		scstat_free_rs(prs);
		prs = prs_next;
	}
}

/*
 * scstat_get_rs
 *
 * Upon success, an object of scstat_rs_t is returned.
 * The caller is responsible for freeing the space.
 * To specify a zone cluster, the RG name must be scoped
 * to the zone cluster name. Please note that the resource
 * name should not be scoped to a zone cluster name.
 *
 * Possible return values:
 *
 *	SCSTAT_ENOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EINVAL		- invalid argument
 *	SCSTAT_ERECONFIG	- cluster is reconfiguring
 *	SCSTAT_EOBSOLETE	- Resource/RG has been updated
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t scstat_get_rs(const scstat_rg_name_t rg_name,
    const scstat_rs_name_t rs_name,
    scstat_rs_t **pprs, boolean_t newcli_caller)
{
	scstat_errno_t error = SCSTAT_ENOERR;
	scha_resourcegroup_t rghandle = NULL;
	scha_resource_t rshandle = NULL;
	unsigned int free_rg_handle = 0;
	unsigned int free_rs_handle = 0;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	scha_status_value_t *prs_status_value = NULL;
	scha_rsstate_t rs_state;
	scha_str_array_t *plnodes = NULL;
	char *node_name;
	scstat_rs_t *prs = NULL;
	scstat_rs_status_t *prs_status = NULL;
	scstat_rs_status_t *prs_status_current = NULL;
	unsigned int i;
	unsigned int len;
	char *rg_part, *zc_part;
	scconf_errno_t scconf_err = SCCONF_NOERR;

	if (rg_name == NULL) {
		error = SCSTAT_EINVAL;
		goto cleanup;
	}

	if (rs_name == NULL) {
		error = SCSTAT_EINVAL;
		goto cleanup;
	}

	if (pprs == NULL) {
		error = SCSTAT_EINVAL;
		goto cleanup;
	}

	/* Get the RG name and the zone cluster name */
	rg_part = zc_part = NULL;
	scconf_err = scconf_parse_obj_name_from_cluster_scope(rg_name,
						&rg_part, &zc_part);

	if (scconf_err != SCCONF_NOERR) {
		error = SCSTAT_EINVAL;
		goto cleanup;
	}

	/* get resource group handle */
	scha_status.err_code = scha_resourcegroup_open_zone(zc_part,
						rg_part, &rghandle);
	error = stat_convert_scha_error_code(scha_status);
	if (error != SCSTAT_ENOERR)
		goto cleanup;

	free_rg_handle = 1;

	/* get primary list */
	scha_status.err_code =
	    scha_resourcegroup_get_zone(zc_part, rghandle, SCHA_NODELIST,
					&plnodes);
	error = stat_convert_scha_error_code(scha_status);
	if (error != SCSTAT_ENOERR)
		goto cleanup;

	/* get resource handle */
	scha_status.err_code = scha_resource_open_zone(zc_part, rs_name,
					rg_part, &rshandle);
	error = stat_convert_scha_error_code(scha_status);
	if (error != SCSTAT_ENOERR)
		goto cleanup;

	free_rs_handle = 1;

	prs = (scstat_rs_t *)calloc(1, sizeof (scstat_rs_t));
	if (prs == NULL) {
		error = SCSTAT_ENOMEM;
		goto cleanup;
	}

	prs->scstat_rs_name = strdup(rs_name);
	if (prs->scstat_rs_name == NULL) {
		error = SCSTAT_ENOMEM;
		goto cleanup;
	}

	prs->scstat_rs_status_list = NULL;

	/* get resource status on every node */
	for (i = 0; i < plnodes->array_cnt; i++) {
		node_name = plnodes->str_array[i];

		/* get resource status on every node */
		scha_status.err_code =
		    scha_resource_get_zone(zc_part, rshandle, SCHA_STATUS_NODE,
		    node_name, &prs_status_value);
		error = stat_convert_scha_error_code(scha_status);

		/* if node is not a cluster member, ignore it */
		if (error == SCSTAT_ENOTCLUSTER) {
			error = SCSTAT_ENOERR;
			continue;
		}

		/* exit if other error is found */
		if (error != SCSTAT_ENOERR)
				goto cleanup;

		/* get resource state on every node */
		scha_status.err_code = scha_resource_get_zone(zc_part,
					rshandle, SCHA_RESOURCE_STATE_NODE,
					node_name, &rs_state);
		error = stat_convert_scha_error_code(scha_status);

		/* if node is not a cluster member, ignore it */
		if (error == SCSTAT_ENOTCLUSTER) {
			error = SCSTAT_ENOERR;
			continue;
		}

		/* exit if other error is found */
		if (error != SCSTAT_ENOERR)
			goto cleanup;

		/* make resource status */
		prs_status = (scstat_rs_status_t *)
		    calloc(1, sizeof (scstat_rs_status_t));
		if (prs_status == NULL) {
			error = SCSTAT_ENOMEM;
			goto cleanup;
		}

		prs_status->scstat_rs_statstr =
		    (char *)calloc(1, SCSTAT_MAX_STRING_LEN);
		if (prs_status->scstat_rs_statstr == NULL) {
			error = SCSTAT_ENOMEM;
			goto cleanup;
		}

		prs_status->scstat_rs_state_str =
		    (char *)calloc(1, SCSTAT_MAX_STRING_LEN);
		if (prs_status->scstat_rs_state_str == NULL) {
			error = SCSTAT_ENOMEM;
			goto cleanup;
		}

		prs_status->scstat_node_name = strdup(node_name);
		if (prs_status->scstat_node_name == NULL) {
			error = SCSTAT_ENOMEM;
			goto cleanup;
		}

		/* map rs status to status code */
		prs_status->scstat_rs_status_by_node =
		    stat_convert_scha_rsstatus_to_sc_state_code(
		    prs_status_value->status);

		stat_get_default_rsstatus_string(
		    prs_status_value->status,
		    prs_status->scstat_rs_statstr);


		/* concatenate with fault monitor reported message */
		len = strlen(prs_status->scstat_rs_statstr);
		if (prs_status_value->status_msg != NULL &&
			strlen(prs_status_value->status_msg) > 0)
			(void) snprintf(prs_status->scstat_rs_statstr + len,
			    SCSTAT_MAX_STRING_LEN - len, " - %s",
			    prs_status_value->status_msg);

		if (newcli_caller)
			stat_get_default_rsstate_string_en(
			    rs_state, prs_status->scstat_rs_state_str);
		else
			stat_get_default_rsstate_string(
			    rs_state, prs_status->scstat_rs_state_str);

		/* append resource status to list */
		if (prs->scstat_rs_status_list == NULL)
			/* first element */
			prs->scstat_rs_status_list = prs_status;
		else
			prs_status_current->scstat_rs_status_by_node_next =
			    prs_status;
		prs_status_current = prs_status;

		prs_status = NULL;
	}

	*pprs = prs;
	prs = NULL; /* so we don't accidently free it during clean up */

cleanup:
	/* close resource group and resource handle */
	if (free_rs_handle == 1)
		(void) scha_resource_close(rshandle);

	if (free_rg_handle == 1)
		(void) scha_resourcegroup_close(rghandle);

	scstat_free_rs(prs);

	if (prs_status != NULL) {
		if (prs_status->scstat_node_name != NULL)
			free(prs_status->scstat_node_name);
		if (prs_status->scstat_rs_statstr != NULL)
			free(prs_status->scstat_rs_statstr);
		if (prs_status->scstat_rs_state_str != NULL)
			free(prs_status->scstat_rs_state_str);
		free(prs_status);
	}

	if (zc_part)
		free(zc_part);
	if (rg_part)
		free(rg_part);

	return (error);
}

/*
 * scstat_free_rs
 *
 * Free all memory associated with a scstat_rs_t structure.
 */
void
scstat_free_rs(scstat_rs_t *prs)
{
	scstat_rs_status_t *prs_status;
	scstat_rs_status_t *prs_status_next;

	if (prs != NULL) {
		/* free scstat_rs_t */
		if (prs->scstat_rs_name != NULL)
			free(prs->scstat_rs_name);

		prs_status = prs->scstat_rs_status_list;
		while (prs_status != NULL) {
			prs_status_next = prs_status->
			    scstat_rs_status_by_node_next;

			/* free scstat_rs_status_t */
			if (prs_status->scstat_node_name != NULL)
				free(prs_status->scstat_node_name);

			if (prs_status->scstat_rs_statstr != NULL)
				free(prs_status->scstat_rs_statstr);

			if (prs_status->scstat_rs_state_str != NULL)
				free(prs_status->scstat_rs_state_str);

			free(prs_status);

			prs_status = prs_status_next;
		}

		free(prs);
	}
}

/*
 * scstat_get_rg_boolprop
 *
 * Get the value of an boolean RG property.
 *
 * Possible return values:
 *
 *	SCSTAT_ENOERR           - success
 *	SCSTAT_ENOMEM           - not enough memory
 *	SCSTAT_EINVAL           - invalid argument
 *	SCSTAT_ERECONFIG        - cluster is reconfiguring
 *	SCSTAT_EOBSOLETE        - Resource/RG has been updated
 *	SCSTAT_EUNEXPECTED      - internal or unexpected error
 */
scstat_errno_t scstat_get_rg_boolprop(const char *rg_name,
    const char *tag, boolean_t *prop_val)
{
	scha_resourcegroup_t handle = NULL;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	scstat_errno_t error = SCSTAT_ENOERR;
	int free_handle = 0;

	if (!rg_name || !tag) {
		return (SCSTAT_EINVAL);
	}

	scha_status.err_code = scha_resourcegroup_open(rg_name, &handle);
	error = stat_convert_scha_error_code(scha_status);
	if (error != SCSTAT_ENOERR) {
		goto cleanup;
	}

	free_handle = 1;
	scha_status.err_code = scha_resourcegroup_get(handle,
	    tag, prop_val);
	error = stat_convert_scha_error_code(scha_status);

cleanup:
	if (free_handle == 1) {
		(void) scha_resourcegroup_close(handle);
	}

	return (error);
}
