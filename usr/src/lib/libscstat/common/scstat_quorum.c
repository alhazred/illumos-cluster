/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scstat_quorum.c	1.16	08/10/01 SMI"

/*
 * libscstat cluster ha device service status functions
 */

#include <scstat_private.h>

static scstat_errno_t stat_get_quorumdevs(
    const scconf_cfg_cluster_t *pcluster_config,
    const quorum_status_t *pquorum_status, scstat_quorumdev_t **pplquorumdevs);
static void stat_free_quorumdevs(scstat_quorumdev_t *plquorumdevs);
static scstat_errno_t stat_get_node_quorums(
    const scconf_cfg_cluster_t *pcluster_config,
    const quorum_status_t *pquorum_status,
    scstat_node_quorum_t **pplnodequorums);
static void stat_free_node_quorums(scstat_node_quorum_t *plnodequorums);

/*
 * scstat_convert_quorum_error_code
 *
 * convert a quorum error code to scstat error code
 *
 * Possible return values:
 *
 *	SCSTAT_NOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t
scstat_convert_quorum_error_code(const int quorum_err)
{
	scstat_errno_t error = SCSTAT_EUNEXPECTED;

	switch (quorum_err) {
	case 0:
		error = SCSTAT_ENOERR;
		break;
	case ENOMEM:
		error = SCSTAT_ENOMEM;
		break;
	case EINVAL:
	default:
		break;
	}
	return (error);
}

/*
 * scstat_get_quorum
 *
 * Upon success, an object of scstat_quorum_t is returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_NOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EPERM            - not root
 *      SCSTAT_ENOTCLUSTER      - there is no cluster
 *      SCSTAT_EINVAL           - invalid argument
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t
scstat_get_quorum(scstat_quorum_t **ppquorum)
{
	scstat_errno_t error = SCSTAT_ENOERR;
	scconf_errno_t scconf_error = SCCONF_NOERR;
	scconf_cfg_cluster_t *pcluster_config = NULL;

	scconf_error = scconf_get_clusterconfig(&pcluster_config);
	error = scstat_convert_scconf_error_code(scconf_error);
	if (error != SCSTAT_ENOERR) {
		goto cleanup;
	}

	error = scstat_get_quorum_with_cached_config(pcluster_config, ppquorum);

cleanup:
	if (pcluster_config != NULL)
		scconf_free_clusterconfig(pcluster_config);

	return (error);
}

/*
 * scstat_get_quorum_with_cached_config
 *
 * Get the quorum status with cached configuration.
 * Upon success, an object of scstat_quorum_t is returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_NOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EPERM            - not root
 *      SCSTAT_ENOTCLUSTER      - there is no cluster
 *      SCSTAT_EINVAL           - invalid argument
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t
scstat_get_quorum_with_cached_config(
    const scconf_cfg_cluster_t *pcluster_config, scstat_quorum_t **ppquorum)
{
	scstat_errno_t error = SCSTAT_ENOERR;
	scstat_quorum_t *pquorum = NULL;
	quorum_status_t	*pquorum_status = NULL;
	int quorum_error = 0;

	if (ppquorum == NULL) {
		error = SCSTAT_EINVAL;
		goto cleanup;
	}

	/* initialize clconf */
	quorum_error = clconf_lib_init();
	if (quorum_error != 0) {
		error = SCSTAT_EUNEXPECTED;
		goto cleanup;
	}

	/* Get the current quorum status. */
	quorum_error = cluster_get_immediate_quorum_status(&pquorum_status);
	error = scstat_convert_quorum_error_code(quorum_error);
	if (error != SCSTAT_ENOERR)
		goto cleanup;

	/* fill in quorum status */
	pquorum = (scstat_quorum_t *)calloc(1, sizeof (scstat_quorum_t));
	if (pquorum == NULL) {
		error = SCSTAT_ENOMEM;
		goto cleanup;
	} else {
		pquorum->scstat_vote =
		    pquorum_status->current_votes;
		pquorum->scstat_vote_configured =
		    pquorum_status->configured_votes;
		pquorum->scstat_vote_needed =
		    pquorum_status->votes_needed_for_quorum;
	}

	/* get node quorums */
	error = stat_get_node_quorums(pcluster_config, pquorum_status,
	    &pquorum->scstat_node_quorum_list);
	if (error != SCSTAT_ENOERR)
		goto cleanup;

	/* get device quorum */
	error = stat_get_quorumdevs(pcluster_config, pquorum_status,
	    &pquorum->scstat_quorumdev_list);
	if (error != SCSTAT_ENOERR)
		goto cleanup;

	*ppquorum = pquorum;

	pquorum = NULL;

cleanup:
	if (pquorum_status != NULL)
		cluster_release_quorum_status(pquorum_status);

	scstat_free_quorum(pquorum);

	return (error);
}

/*
 * scstat_free_quorum
 *
 * Free all memory associated with a scstat_quorum_t structure.
 */
void
scstat_free_quorum(scstat_quorum_t *pquorum)
{
	if (pquorum != NULL) {
		/* free node quorums */
		if (pquorum->scstat_node_quorum_list != NULL)
			stat_free_node_quorums(\
			    pquorum->scstat_node_quorum_list);

		/* free device quorums */
		if (pquorum->scstat_quorumdev_list != NULL)
			stat_free_quorumdevs(pquorum->scstat_quorumdev_list);

		free(pquorum);
	}
}	

/*
 * stat_get_quorumdevs
 *
 * Upon success, a list of objects of scstat_quorumdev_t are returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_NOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EPERM            - not root
 *      SCSTAT_ENOTCLUSTER      - there is no cluster
 *      SCSTAT_EINVAL           - invalid argument
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
static scstat_errno_t
stat_get_quorumdevs(const scconf_cfg_cluster_t *pcluster_config,
    const quorum_status_t *pquorum_status, scstat_quorumdev_t **pplquorumdevs)
{
	scstat_errno_t error = SCSTAT_ENOERR;
	scconf_errno_t scconf_error = SCCONF_NOERR;
	scstat_quorumdev_t *scstat_qdev, *scstat_qdevs = NULL;
	scstat_quorumdev_t **pscstat_qdev;
	scstat_node_access_t *scstat_qdevnode;
	scstat_node_access_t **pscstat_qdevnode;
	scconf_cfg_qdev_t *scconf_qdev;
	quorum_device_status_t *clconf_qdev_status;
	char buffer[SCSTAT_MAX_STRING_LEN];
	unsigned int i, j;
	unsigned int node_up;
	node_bitmask_t node_bitmask;
	node_bitmask_t current_node_bitmask;

	if (pquorum_status == NULL) {
		error = SCSTAT_EINVAL;
		goto cleanup;
	}

	if (pplquorumdevs == NULL) {
		error = SCSTAT_EINVAL;
		goto cleanup;
	}

	/* Create the list of quorum devices from the configuration */
	pscstat_qdev = &scstat_qdevs;
	for (scconf_qdev = pcluster_config->scconf_cluster_qdevlist;
	    scconf_qdev; scconf_qdev = scconf_qdev->scconf_qdev_next) {
		/* Make sure there is a name */
		if (scconf_qdev->scconf_qdev_name == NULL)
			continue;

		/* Allocate the next structure in the list */
		scstat_qdev = (scstat_quorumdev_t *)
		    calloc(1, sizeof (scstat_quorumdev_t));
		if (scstat_qdev == NULL) {
			error = SCSTAT_ENOMEM;
			goto cleanup;
		}
		*pscstat_qdev = scstat_qdev;
		pscstat_qdev = &scstat_qdev->scstat_quorumdev_next;

		/* Set the name */
		if (scconf_qdev->scconf_qdev_device) {
			scstat_qdev->scstat_quorumdev_name = strdup(
			    scconf_qdev->scconf_qdev_device);
			if (scstat_qdev->scstat_quorumdev_name == NULL) {
				error = SCSTAT_ENOMEM;
				goto cleanup;
			}
		}

		/* Set votes configured */
		scstat_qdev->scstat_vote_configured =
		    scconf_qdev->scconf_qdev_votes;

		/* Get the status information for this device */
		clconf_qdev_status = NULL;
		for (i = 0; i < pquorum_status->num_quorum_devices; i++) {
			clconf_qdev_status =
			    &pquorum_status->quorum_device_list[i];
			if (clconf_qdev_status->gdevname == NULL)
				continue;
			if (strcmp(clconf_qdev_status->gdevname,
			    scstat_qdev->scstat_quorumdev_name) == 0)
				break;
		}

		/* Do not continue, if no status information found */
		if (i == pquorum_status->num_quorum_devices ||
		    clconf_qdev_status == NULL) {
			scstat_get_default_status_string(SCSTAT_OFFLINE,
			    buffer);
			scstat_qdev->scstat_quorumdev_statstr = strdup(buffer);
			if (scstat_qdev->scstat_quorumdev_statstr == NULL) {
				error = SCSTAT_ENOMEM;
				goto cleanup;
			}
			continue;
		}

		/* Fill in node having access to the quorum device */
		node_bitmask = clconf_qdev_status->nodes_with_enabled_paths;
		pscstat_qdevnode = &(scstat_qdev)->scstat_node_access_list;
		for (j = 0; j < pquorum_status->num_nodes; j++) {

			/* Allocate the next structure in the list */
			scstat_qdevnode = (scstat_node_access_t *)
			    calloc(1, sizeof (scstat_node_access_t));
			if (scstat_qdevnode == NULL) {
				error = SCSTAT_ENOMEM;
				goto cleanup;
			}
			*pscstat_qdevnode = scstat_qdevnode;
			pscstat_qdevnode = &(scstat_qdevnode->scstat_node_next);

			/* Get the nodename */
			scconf_error =
			    scconf_get_cached_nodename(pcluster_config,
			    pquorum_status->nodelist[j].nid,
			    &scstat_qdevnode->scstat_node_name);
			error = scstat_convert_scconf_error_code(scconf_error);
			if (error != SCSTAT_ENOERR)
				goto cleanup;

			/*
			 * node_bitmask has one bit for every node in the
			 * cluster. The bit is set if the corresponding node
			 * has access to the quorum device
			 */
			current_node_bitmask =
			    1 << (pquorum_status->nodelist[j].nid - 1);
			node_up = (unsigned int)node_bitmask &
			    current_node_bitmask;
			if (node_up == 0) {
				scstat_qdevnode->scstat_access_status =
				    SCSTAT_ACCESS_DISABLED;
			} else {
				scstat_qdevnode->scstat_access_status =
				    SCSTAT_ACCESS_ENABLED;
			}
		}

		/* Set the status */
		scstat_qdev->scstat_quorumdev_status =
		    stat_get_state_code_from_quorum_state(
		    clconf_qdev_status->state);

		/* Set the status string */
		scstat_get_default_status_string(
		    scstat_qdev->scstat_quorumdev_status, buffer);
		scstat_qdev->scstat_quorumdev_statstr = strdup(buffer);
		if (scstat_qdev->scstat_quorumdev_statstr == NULL) {
			error = SCSTAT_ENOMEM;
			goto cleanup;
		}

		/* Set the votes contributed */
		if (scstat_qdev->scstat_quorumdev_status == SC_STATE_ONLINE) {
			scstat_qdev->scstat_vote_contributed =
			    scstat_qdev->scstat_vote_configured;
		} else {
			scstat_qdev->scstat_vote_contributed = 0;
		}
	}

cleanup:
	if (error != SCSTAT_ENOERR) {
		if (scstat_qdevs != NULL)
			scstat_free_quorumdev(scstat_qdevs);
	} else {
		*pplquorumdevs = scstat_qdevs;
	}

	return (error);
}

/*
 * stat_free_quorumdevs
 *
 * Free all memory associated with a scstat_quorumdev_t structure.
 */
static void
stat_free_quorumdevs(scstat_quorumdev_t *plquorumdevs)
{
	scstat_quorumdev_t *pquorumdev = plquorumdevs;
	scstat_quorumdev_t *pquorumdev_next;

	while (pquorumdev != NULL) {
		pquorumdev_next = pquorumdev->scstat_quorumdev_next;

		scstat_free_quorumdev(pquorumdev);
		pquorumdev = pquorumdev_next;
	}
}

/*
 * scstat_get_quorumdev
 *
 * Upon success, an object of scstat_hadev_t is returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_NOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EPERM            - not root
 *      SCSTAT_ENOTCLUSTER      - there is no cluster
 *      SCSTAT_EINVAL           - invalid argument
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t
scstat_get_quorumdev(const scstat_quorumdev_name_t quorumdev_name,
    scstat_quorumdev_t **ppquorumdev)
{
	scstat_errno_t error = SCSTAT_ENOERR;
	scconf_errno_t scconf_error = SCCONF_NOERR;
	scconf_cfg_cluster_t *pcluster_config = NULL;

	scconf_error = scconf_get_clusterconfig(&pcluster_config);
	error = scstat_convert_scconf_error_code(scconf_error);
	if (error != SCSTAT_ENOERR) {
		goto cleanup;
	}

	error = scstat_get_quorumdev_with_cached_config(pcluster_config,
	    quorumdev_name, ppquorumdev);

cleanup:
	if (pcluster_config != NULL)
		scconf_free_clusterconfig(pcluster_config);

	return (error);
}

/*
 * scstat_get_quorumdev_with_cached_config
 *
 * Get quorum device status with cached configuration.
 * Upon success, an object of scstat_hadev_t is returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_NOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EPERM            - not root
 *      SCSTAT_ENOTCLUSTER      - there is no cluster
 *      SCSTAT_EINVAL           - invalid argument
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t
scstat_get_quorumdev_with_cached_config(
    const scconf_cfg_cluster_t *pcluster_config,
    const scstat_quorumdev_name_t quorumdev_name,
    scstat_quorumdev_t **ppquorumdev)
{
	scstat_errno_t error = SCSTAT_ENOERR;
	scconf_errno_t scconf_error = SCCONF_NOERR;
	quorum_status_t	quorum_status;
	quorum_status_t	*pquorum_status = &quorum_status;
	scstat_quorumdev_t *pquorumdev = NULL;
	scstat_node_access_t *pnode_access = NULL;
	scstat_node_access_t *pnode_access_current = NULL;
	char text[SCSTAT_MAX_STRING_LEN];
	unsigned int i, j;
	unsigned int node_up;
	node_bitmask_t node_bitmask;
	node_bitmask_t current_node_bitmask;
	int quorum_error = 0;

	if (quorumdev_name == NULL) {
		error = SCSTAT_EINVAL;
		goto cleanup;
	}

	if (ppquorumdev == NULL) {
		error = SCSTAT_EINVAL;
		goto cleanup;
	}

	/* initialize clconf */
	quorum_error = clconf_lib_init();
	if (quorum_error != 0) {
		error = SCSTAT_EUNEXPECTED;
		goto cleanup;
	}

	/* Get the current quorum status. */
	quorum_error = cluster_get_immediate_quorum_status(&pquorum_status);
	error = scstat_convert_quorum_error_code(quorum_error);
	if (error != SCSTAT_ENOERR)
		goto cleanup;

	for (i = 0; i < pquorum_status->num_quorum_devices; i++) {
		quorum_device_status_t pquorum_device_status =
		    pquorum_status->quorum_device_list[i];
		if (strcmp(pquorum_device_status.gdevname,
		    quorumdev_name) == 0) {
			/* found match quorum device */
			pquorumdev = (scstat_quorumdev_t *)
			    calloc(1, sizeof (scstat_quorumdev_t));
			if (pquorumdev == NULL) {
				error = SCSTAT_ENOMEM;
				goto cleanup;
			}

			pnode_access_current = NULL;

			/* fill in quorundev structure */
			pquorumdev->scstat_quorumdev_name = strdup(
			    pquorum_device_status.gdevname);
			if (pquorumdev->scstat_quorumdev_name == NULL) {
				error = SCSTAT_ENOMEM;
				goto cleanup;
			}

			pquorumdev->scstat_vote_configured =
			    pquorum_device_status.votes_configured;
			node_bitmask =
			    pquorum_device_status.nodes_with_enabled_paths;

			/* fill in nodes having access to the quorum device */
			for (j = 0; j < pquorum_status->num_nodes; j++) {
				char *node_name = NULL;

				/* if node not belong to cluster, ignore it */
				scconf_error =
				    scconf_get_cached_nodename(pcluster_config,
				    pquorum_status->nodelist[j].nid,
				    &node_name);
				error = scstat_convert_scconf_error_code(\
				    scconf_error);
				if (error != SCSTAT_ENOERR)
					goto cleanup;

				pnode_access = (scstat_node_access_t *)
				    calloc(1, sizeof (scstat_node_access_t));
				if (pnode_access == NULL) {
					error = SCSTAT_ENOMEM;
					goto cleanup;
				}

				pnode_access->scstat_node_name = node_name;

				/*
				 * node_bitmask has one bit for every node in
				 * the cluster. The bit is set if the
				 * corresponding node has access to the
				 * quorum device
				 */

				current_node_bitmask =
				    1 << (pquorum_status->nodelist[j].nid - 1);
				node_up = (unsigned int) node_bitmask &
				    current_node_bitmask;
				pnode_access->scstat_access_status =
				    ((node_up == 0) ? SCSTAT_ACCESS_DISABLED :
				    SCSTAT_ACCESS_ENABLED);
				pnode_access->scstat_node_next = NULL;

				/* append it to node access list */
				if (pnode_access_current != NULL) {
					/* not first element */
					pnode_access_current->scstat_node_next =
					    pnode_access;
				} else	/* first element */
					pquorumdev->scstat_node_access_list =
					    pnode_access;

				pnode_access_current = pnode_access;

				/*
				 * set pnode_access to NULL so we don't free it
				 * once it became part of pquorumdev. Free of
				 * pquorumdev will take care of that.
				 */
				pnode_access = NULL;
			}

			pquorumdev->scstat_quorumdev_status =
			    stat_get_state_code_from_quorum_state(
			    pquorum_device_status.state);
			scstat_get_default_status_string(
			    pquorumdev->scstat_quorumdev_status, text);
			pquorumdev->scstat_quorumdev_statstr =
			    strdup(text);
			if (pquorumdev->scstat_quorumdev_statstr
			    == NULL) {
				error = SCSTAT_ENOMEM;
				goto cleanup;
			}

			pquorumdev->scstat_vote_contributed =
			    ((pquorumdev->scstat_quorumdev_status
			    == SC_STATE_ONLINE) ?
			    pquorumdev->scstat_vote_configured : 0);
			pquorumdev->scstat_quorumdev_next = NULL;

			*ppquorumdev = pquorumdev;

			/*
			 * set pquorumdev to NULL so we don't free it once
			 * it became part of ppquorumdev. Free of ppquorumdev
			 * will take care of that.
			 */
			pquorumdev = NULL;

			break;
		}
	}

cleanup:
	if (pnode_access != NULL) {
		if (pnode_access->scstat_node_name != NULL)
			free(pnode_access->scstat_node_name);

		free(pnode_access);
	}

	scstat_free_quorumdev(pquorumdev);

	if (pquorum_status != NULL)
		cluster_release_quorum_status(pquorum_status);

	return (error);
}

/*
 * scstat_free_quorumdev
 *
 * Free all memory associated with a scstat_quorumdev_t structure.
 */
void
scstat_free_quorumdev(scstat_quorumdev_t *pquorumdev)
{
	scstat_node_access_t *plnode_accesss;
	scstat_node_access_t *pnode_access;
	scstat_node_access_t *pnode_access_next;

	if (pquorumdev != NULL) {
		plnode_accesss = pquorumdev->scstat_node_access_list;

		if (pquorumdev->scstat_quorumdev_name != NULL)
			free(pquorumdev->scstat_quorumdev_name);

		if (pquorumdev->scstat_quorumdev_statstr != NULL)
			free(pquorumdev->scstat_quorumdev_statstr);

		pnode_access = plnode_accesss;

		while (pnode_access != NULL) {
			pnode_access_next = pnode_access->scstat_node_next;

			if (pnode_access->scstat_node_name != NULL)
				free(pnode_access->scstat_node_name);

			free(pnode_access);

			pnode_access = pnode_access_next;
		}

		free(pquorumdev);
	}
}

/*
 * stat_get_node_quorums
 *
 * Upon success, a list of objects of scstat_node_quorum_t are returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_NOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EPERM            - not root
 *      SCSTAT_ENOTCLUSTER      - there is no cluster
 *      SCSTAT_EINVAL           - invalid argument
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
/*ARGSUSED*/
static scstat_errno_t
stat_get_node_quorums(const scconf_cfg_cluster_t *pcluster_config,
    const quorum_status_t *pquorum_status,
    scstat_node_quorum_t **pplnodequorums)
{
	scstat_errno_t error = SCSTAT_ENOERR;
	scconf_errno_t scconf_error = SCCONF_NOERR;
	scstat_node_quorum_t *pnode_quorum = NULL;
	scstat_node_quorum_t *pnode_quorum_current = NULL;
	char text[SCSTAT_MAX_STRING_LEN];
	unsigned int i;

	if (pquorum_status == NULL) {
		error = SCSTAT_EINVAL;
		goto cleanup;
	}

	if (pplnodequorums == NULL) {
		error = SCSTAT_EINVAL;
		goto cleanup;
	}

	/* get node quorums */
	for (i = 0; i < pquorum_status->num_nodes; i++)	{
		char *node_name = NULL;

		/* get node name */
		scconf_error = scconf_get_cached_nodename(pcluster_config,
		    pquorum_status->nodelist[i].nid,
		    &node_name);
		error = scstat_convert_scconf_error_code(scconf_error);
		if (error != SCSTAT_ENOERR)
			goto cleanup;

		pnode_quorum = (scstat_node_quorum_t *)
		    calloc(1, sizeof (scstat_node_quorum_t));
		if (pnode_quorum == NULL) {
			error = SCSTAT_ENOMEM;
			goto cleanup;
		}

		/* fill in node quorum structure */
		pnode_quorum->scstat_node_name = node_name;

		pnode_quorum->scstat_vote_configured =
		    pquorum_status->nodelist[i].votes_configured;
		pnode_quorum->scstat_node_quorum_status =
		    stat_get_state_code_from_quorum_state(
		    pquorum_status->nodelist[i].state);
		scstat_get_default_status_string(
		    pnode_quorum->scstat_node_quorum_status, text);
		pnode_quorum->scstat_node_quorum_statstr = strdup(text);
		if (pnode_quorum->scstat_node_quorum_statstr == NULL) {
			error = SCSTAT_ENOMEM;
			goto cleanup;
		}

		pnode_quorum->scstat_vote_contributed =
		    ((pnode_quorum->scstat_node_quorum_status
		    == SC_STATE_ONLINE) ?
		    pnode_quorum->scstat_vote_configured : 0);
		pnode_quorum->scstat_node_quorum_next = NULL;

		/* append it to node quorum list */
		if (pnode_quorum_current != NULL) /* not first element */
			pnode_quorum_current->scstat_node_quorum_next =
			    pnode_quorum;
		else	/* first element */
			*pplnodequorums = pnode_quorum;

		pnode_quorum_current = pnode_quorum;

		/*
		 * set pnode_quorum to NULL so we don't free it once it became
		 * part of pplnodequorums. Free of pplnodequorums will take
		 * care of that.
		 */
		pnode_quorum = NULL;
	}

cleanup:
	scstat_free_node_quorum(pnode_quorum);

	return (error);
}

/*
 * stat_free_node_quorums
 *
 * Free all memory associated with a scstat_node_quorum_t structure.
 */
static void
stat_free_node_quorums(scstat_node_quorum_t *plnodequorums)
{
	scstat_node_quorum_t *pnode_quorum =
	    (scstat_node_quorum_t *)plnodequorums;
	scstat_node_quorum_t *pnode_quorum_next;

	while (pnode_quorum != NULL) {
		pnode_quorum_next = pnode_quorum->scstat_node_quorum_next;

		scstat_free_node_quorum(pnode_quorum);
		pnode_quorum = pnode_quorum_next;
	}
}

/*
 * scstat_get_node_quorum
 *
 * Upon success, an object of scstat_node_quorum_t is returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_NOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EPERM            - not root
 *      SCSTAT_ENOTCLUSTER      - there is no cluster
 *      SCSTAT_EINVAL           - invalid argument
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t
scstat_get_node_quorum(const scstat_node_name_t node_name,
    scstat_node_quorum_t **ppnodequorum)
{
	scstat_errno_t error = SCSTAT_ENOERR;
	scconf_errno_t scconf_error = SCCONF_NOERR;
	scconf_cfg_cluster_t *pcluster_config = NULL;

	scconf_error = scconf_get_clusterconfig(&pcluster_config);
	error = scstat_convert_scconf_error_code(scconf_error);
	if (error != SCSTAT_ENOERR) {
		goto cleanup;
	}

	error = scstat_get_node_quorum_with_cached_config(pcluster_config,
	    node_name, ppnodequorum);

cleanup:
	if (pcluster_config != NULL)
		scconf_free_clusterconfig(pcluster_config);

	return (error);
}

/*
 * scstat_get_node_quorum_with_cached_config
 *
 * Get node quorum status with cached configuration.
 * Upon success, an object of scstat_node_quorum_t is returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_NOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EPERM            - not root
 *      SCSTAT_ENOTCLUSTER      - there is no cluster
 *      SCSTAT_EINVAL           - invalid argument
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t
scstat_get_node_quorum_with_cached_config(\
    const scconf_cfg_cluster_t *pcluster_config,
    const scstat_node_name_t node_name,
    scstat_node_quorum_t **ppnodequorum)
{
	scstat_errno_t error = SCSTAT_ENOERR;
	scconf_errno_t scconf_error = SCCONF_NOERR;
	quorum_status_t	quorum_status;
	quorum_status_t	*pquorum_status = &quorum_status;
	scstat_node_quorum_t *pnode_quorum = NULL;
	char *quorum_node_name = NULL;
	char text[SCSTAT_MAX_STRING_LEN];
	unsigned int i;
	int quorum_error = 0;

	if (node_name == NULL) {
		error = SCSTAT_EINVAL;
		goto cleanup;
	}

	if (ppnodequorum == NULL) {
		error = SCSTAT_EINVAL;
		goto cleanup;
	}

	/* initialize clconf */
	quorum_error = clconf_lib_init();
	if (quorum_error != 0) {
		error = SCSTAT_EUNEXPECTED;
		goto cleanup;
	}

	/* get quorum status */
	quorum_error = cluster_get_quorum_status(&pquorum_status);
	error = scstat_convert_quorum_error_code(quorum_error);
	if (error != SCSTAT_ENOERR)
		goto cleanup;

	for (i = 0; i < pquorum_status->num_nodes; i++)	{
		/* get node name */
		scconf_error = scconf_get_cached_nodename(pcluster_config,
		    pquorum_status->nodelist[i].nid,
		    &quorum_node_name);
		error = scstat_convert_scconf_error_code(scconf_error);
		if (error != SCSTAT_ENOERR)
			goto cleanup;

		if (strcmp(quorum_node_name, node_name) == 0) {
			/* node found */
			pnode_quorum = (scstat_node_quorum_t *)
			    calloc(1, sizeof (scstat_node_quorum_t));
			if (pnode_quorum == NULL) {
				error = SCSTAT_ENOMEM;
				goto cleanup;
			}

			/* fill in node quorum structure */
			pnode_quorum->scstat_node_name = quorum_node_name;
			quorum_node_name = NULL;

			pnode_quorum->scstat_vote_configured =
			    pquorum_status->nodelist[i].votes_configured;
			pnode_quorum->scstat_node_quorum_status =
			    stat_get_state_code_from_quorum_state(
			    pquorum_status->nodelist[i].state);
			scstat_get_default_status_string(
			    pnode_quorum->scstat_node_quorum_status,
			    text);
			pnode_quorum->scstat_node_quorum_statstr =
			    strdup(text);
			if (pnode_quorum->scstat_node_quorum_statstr
			    == NULL) {
				error = SCSTAT_ENOMEM;
				goto cleanup;
			}

			pnode_quorum->scstat_vote_contributed =
			    ((pnode_quorum->scstat_node_quorum_status
			    == SC_STATE_ONLINE) ?
			    pnode_quorum->scstat_vote_configured : 0);
			pnode_quorum->scstat_node_quorum_next = NULL;

			*ppnodequorum = pnode_quorum;

			/*
			 * set pnode_quorum to NULL so we don't free it
			 * once it became ppnodequorum. Free of ppnodequorum
			 * will take care of that.
			 */
			pnode_quorum = NULL;

			break;
		}

		if (quorum_node_name != NULL) {
			free(quorum_node_name);
			quorum_node_name = NULL;
		}
	}

cleanup:
	scstat_free_node_quorum(pnode_quorum);

	if (quorum_node_name != NULL)
		free(quorum_node_name);

	if (pquorum_status != NULL)
		cluster_release_quorum_status(pquorum_status);

	return (error);
}

/*
 * scstat_free_node_quorum
 *
 * Free all memory associated with a scstat_node_quorum_t structure.
 */
void
scstat_free_node_quorum(scstat_node_quorum_t *pnode_quorum)
{
	if (pnode_quorum != NULL) {
		if (pnode_quorum->scstat_node_name != NULL)
			free(pnode_quorum->scstat_node_name);

		if (pnode_quorum->scstat_node_quorum_statstr != NULL)
			free(pnode_quorum->scstat_node_quorum_statstr);

		if (pnode_quorum != NULL)
			free(pnode_quorum);
	}
}
