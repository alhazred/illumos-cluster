/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scstat_ipmp.c	1.9	08/06/12 SMI"

/*
 * libscstat cluster ipmp service status functions
 */

#include <scadmin/scconf.h>

#include <scstat_private.h>

static void scstat_grp_stat_list_free(scstat_grp_stat_list_t *);
static scstat_ipmp_stat_list_t *scstat_ipmp_stat_list_alloc(void);
static scstat_grp_stat_list_t *scstat_grp_stat_list_alloc(void);
static int get_grp_adp_stat(char *, scstat_grp_stat_list_t *);
static void get_grp_status(char *, scstat_grp_stat_list_t *);
static int get_node_ipmp_state(scstat_grp_stat_list_t **);

/*
 * Frees the scstat_ipmp_stat_list_t structure. The argument is also freed by
 * this routine and is made NULL.
 * INPUT: pointer to scstat_ipmp_stat_list_t struct.
 * RETURN: void.
 */
void
scstat_ipmp_stat_list_free(scstat_ipmp_stat_list_t *isp)
{
	scstat_ipmp_stat_list_t	*ispp, *ispp_next;

	for (ispp = isp; ispp; ispp = ispp_next) {
		ispp_next = ispp->next;
		free(ispp->name);
		scstat_grp_stat_list_free(ispp->pgslt);
		free(ispp);
	}

	isp = NULL;
}

/*
 * Frees the scstat_grp_stat_list_t structure. The argument is also freed by
 * this routine and is made NULL.
 * INPUT: pointer to scstat_grp_stat_list_t struct.
 * RETURN: void.
 */
static void
scstat_grp_stat_list_free(scstat_grp_stat_list_t *ptr)
{
	scstat_grp_stat_list_t	*gslt, *gslt_next;

	for (gslt = ptr; gslt; gslt = gslt_next) {
		gslt_next = gslt->next;
		free(gslt->grp_name);

		/* free adp list */
		pnm_grp_status_free(&(gslt->pgst));
		free(gslt);
	}

	ptr = NULL;
}

/*
 * Allocate memory for scstat_ipmp_stat_list and zero it. Returns a pointer to
 * the allocated memory on success, or on error returns NULL.
 */
static scstat_ipmp_stat_list_t *
scstat_ipmp_stat_list_alloc()
{
	scstat_ipmp_stat_list_t *listptr;

	listptr = (scstat_ipmp_stat_list_t *)malloc(
	    sizeof (scstat_ipmp_stat_list_t));

	if (listptr == NULL)
		return (NULL);

	bzero(listptr, sizeof (scstat_ipmp_stat_list_t));

	return (listptr);
}

/*
 * Allocate memory for scstat_grp_stat_list and zero it. Returns a pointer to
 * the allocated memory on success, or on error returns NULL.
 */
static scstat_grp_stat_list_t *
scstat_grp_stat_list_alloc()
{
	scstat_grp_stat_list_t *listptr;

	listptr = (scstat_grp_stat_list_t *)malloc(
	    sizeof (scstat_grp_stat_list_t));

	if (listptr == NULL)
		return (NULL);

	bzero(listptr, sizeof (scstat_grp_stat_list_t));

	return (listptr);
}

/*
 * Returns SCSTAT_ENOERR on success and on error it returns:
 * SCSTAT_ENOMEM -  out of memory.
 * SCSTAT_EINVAL - given arguments are not valid.
 * SCSTAT_EUNEXPECTED - internal or unexpected error.
 * This routine gets the cluster's IPMP state. It forms a list of
 * ipmp_stat_list structs each of which contains an ipmp group list.
 * Each member of the ipmp group list contains the list of adps with
 * their status. This routine is not MT-safe since it uses libpnm which
 * is not MT-safe.
 * INPUT: pointer to a pointer to scstat_ipmp_stat_list_t - pointer to
 *        ipmp_stat_list list head.
 * RETURN: scstat_errno_t - success or error code.
 */
scstat_errno_t
scstat_get_ipmp_state(scstat_ipmp_stat_list_t **isp)
{
	int scerr;
	scha_cluster_t hdl;
	scha_str_array_t *all_nodenames;
	uint_t ix;
	scha_node_state_t node_state;
	scstat_ipmp_stat_list_t *ilptr, *ihead, *ilast;
	char **zonelist = (char **)0;
	char **zl = (char **)NULL;
	int i;

	if (isp == NULL)
		return (SCSTAT_EINVAL);

	ilptr = ihead = ilast = NULL;

	scerr = scha_cluster_open(&hdl);
	if (scerr != SCHA_ERR_NOERR)
		return (SCSTAT_EUNEXPECTED);

	/* Get the names of all the nodes in this cluster */
	scerr = scha_cluster_get(hdl, SCHA_ALL_NODENAMES, &all_nodenames);
	if (scerr != SCHA_ERR_NOERR) {
		(void) scha_cluster_close(hdl);
		return (SCSTAT_EUNEXPECTED);
	}

	/* For all nodes that are up.... */
	for (ix = 0; ix < all_nodenames->array_cnt; ix++) {
		/* Get the state of this node */
		scerr = scha_cluster_get(hdl, SCHA_NODESTATE_NODE,
		    all_nodenames->str_array[ix], &node_state);
		if (scerr != SCHA_ERR_NOERR) {
			(void) scha_cluster_close(hdl);
			/* free the list - NULL pointer is not a problem */
			scstat_ipmp_stat_list_free(ihead);
			return (SCSTAT_EUNEXPECTED);
		}

		/* Skip this node if it is down */
		if (node_state == SCHA_NODE_DOWN) {
			continue;
		}

		/* Allocate memory for the ipmp_stat_list element */
		if ((ilptr = scstat_ipmp_stat_list_alloc()) == NULL) {
			(void) scha_cluster_close(hdl);
			/* free the list - NULL pointer is not a problem */
			scstat_ipmp_stat_list_free(ihead);
			return (SCSTAT_ENOMEM);
		}

		/* Append this node's struct to the ipmp_stat_list */
		if (ihead == NULL)
			ihead = ilptr;
		if (ilast != NULL)
			ilast->next = ilptr;
		ilast = ilptr;

		/* Store the node name */
		ilptr->name = strdup(all_nodenames->str_array[ix]);
		if (ilptr->name == NULL) {
			scstat_ipmp_stat_list_free(ihead);
			(void) scha_cluster_close(hdl);
			return (SCSTAT_ENOMEM);
		}

		/* Go to the pnmd of the specified node */
		(void) pnm_init(all_nodenames->str_array[ix]);

		/* Get this node's ipmp state */
		if (get_node_ipmp_state(&(ilptr->pgslt)) != 0) {
			scstat_ipmp_stat_list_free(ihead);
			(void) scha_cluster_close(hdl);
			return (SCSTAT_ENOMEM);
		}

		/* Close the connection to the specified node's pnmd */
		pnm_fini();

		/*
		 * Now repeat the same for all the exclusive IP zones hosted
		 * on this node.
		 */
		if (zonelist)
			scconf_free_zones(zonelist);
		zonelist = NULL;

		if (scconf_get_xip_zones(all_nodenames->str_array[ix],
		    &zonelist) != SCCONF_NOERR) {
			if (zonelist)
				scconf_free_zones(zonelist);
			scstat_ipmp_stat_list_free(ihead);
			return (SCSTAT_EUNEXPECTED);
		}
		for (zl = zonelist, i = 0; zl[i]; i++) {
			/* Allocate memory for the ipmp_stat_list element */
			if ((ilptr = scstat_ipmp_stat_list_alloc()) == NULL) {
				(void) scha_cluster_close(hdl);
				/*
				 * free the list - NULL pointer is not a
				 * problem
				 */
				scstat_ipmp_stat_list_free(ihead);
				return (SCSTAT_ENOMEM);
			}

			/* Append this node's struct to the ipmp_stat_list */
			if (ihead == NULL)
				ihead = ilptr;
			if (ilast != NULL)
				ilast->next = ilptr;
			ilast = ilptr;

			/* Store the node name */
			ilptr->name = (char *)calloc(1, strlen(
			    all_nodenames->str_array[ix]) + strlen(zl[i])
			    + 3);
			if (ilptr->name == NULL) {
				scstat_ipmp_stat_list_free(ihead);
				(void) scha_cluster_close(hdl);
				return (SCSTAT_ENOMEM);
			}
			(void) strcat(ilptr->name,
			    all_nodenames->str_array[ix]);
			(void) strcat(ilptr->name, ":");
			(void) strcat(ilptr->name, zl[i]);
			/* Go to the pnmd of the specified node */
			if (pnm_zone_init(all_nodenames->str_array[ix], zl[i]))
				continue;

			/* Get this node's ipmp state */
			if (get_node_ipmp_state(&(ilptr->pgslt)) != 0) {
				scstat_ipmp_stat_list_free(ihead);
				(void) scha_cluster_close(hdl);
				return (SCSTAT_ENOMEM);
			}
			/* Close the connection to the specified node's pnmd */
			pnm_fini();
		}
	}

	(void) scha_cluster_close(hdl);
	*isp = ihead;
	return (SCSTAT_ENOERR);
}

/*
 * Fills a valid scstat_grp_stat_list_t list head pointer on success and on
 * error it fills it with NULL. This routine gets the node's IPMP state.
 * It forms a list of ipmp groups. Each member of the ipmp group list contains
 * the list of adps in that ipmp group with their status.
 * INPUT: pointer to a pointer to scstat_grp_stat_list_t - pointer to
 *        grp_stat_list list head.
 * RETURN: 0 on success and -1 on error.
 */
static int
get_node_ipmp_state(scstat_grp_stat_list_t **ptr)
{
	pnm_group_list_t glist = NULL;
	char *gr_str, *next_gr_str;
	scstat_grp_stat_list_t *glptr, *ghead, *glast;

	/* Initialize the grp_stat_list pointers */
	glptr = ghead = glast = NULL;

	/* Get all the IPMP groups for this node */
	(void) pnm_group_list(&glist);
	next_gr_str = glist;

	/* For all the groups in this node.... */
	while ((gr_str = strtok_r(next_gr_str, ":", &next_gr_str)) != NULL) {

		/* Allocate memory for the grp_stat_list element */
		if ((glptr = scstat_grp_stat_list_alloc()) == NULL) {
			free(glist);
			/* free the list - NULL pointer is not a problem */
			scstat_grp_stat_list_free(ghead);
			*ptr = NULL;
			return (-1);
		}

		/* Append the group struct to the list of groups */
		if (ghead == NULL)
			ghead = glptr;
		if (glast != NULL)
			glast->next = glptr;
		glast = glptr;

		/* Store the group name */
		glptr->grp_name = strdup(gr_str);
		if (glptr->grp_name == NULL) {
			scstat_grp_stat_list_free(ghead);
			free(glist);
			*ptr = NULL;
			return (-1);
		}

		/* Get and store the ipmp group's status */
		get_grp_status(gr_str, glptr);

		/* Get and store the grp's adp level status */
		if (get_grp_adp_stat(gr_str, glptr) != 0) {
			scstat_grp_stat_list_free(ghead);
			free(glist);
			*ptr = NULL;
			return (-1);
		}
	}

	/* Free the glist struct allocated */
	free(glist);

	*ptr = ghead;
	return (0);
}

/*
 * Gets the adp level status for the given ipmp group and stores the status
 * in the given grp_stat_list struct pointer.
 * Returns 0 on success and -1 on error.
 */
static int
get_grp_adp_stat(char *gr_str, scstat_grp_stat_list_t *glptr)
{
	pnm_adp_status_t  *ptr = NULL;
	pnm_grp_status_t grp_status;
	uint_t i;

	/* Get the adp level status for this group from libpnm */
	(void) pnm_grp_adp_status(gr_str, &grp_status);

	/* Allocate storage for the adp array in this group */
	ptr = (pnm_adp_status_t *)
	    malloc(grp_status.num_adps * sizeof (pnm_adp_status_t));
	if (ptr == NULL) {
		pnm_grp_status_free(&grp_status);
		return (-1);
	}

	bzero(ptr, grp_status.num_adps * sizeof (pnm_adp_status_t));

	/* Fill the status of all the adps in this group */
	for (i = 0; i < grp_status.num_adps; i++) {
		ptr[i].adp_name = strdup(
		    grp_status.pnm_adp_stat[i].adp_name);
		ptr[i].status = grp_status.pnm_adp_stat[i].status;
	}

	/* Fill in the grp_stat_list struct */
	glptr->pgst.num_adps = grp_status.num_adps;
	glptr->pgst.pnm_adp_stat = ptr;

	/* Free the grp_status struct */
	pnm_grp_status_free(&grp_status);
	return (0);
}

/*
 * Gets the grp status for the given ipmp group and stores the status
 * in the given grp_stat_list struct pointer.
 * Returns void.
 */
static void
get_grp_status(char *gr_str, scstat_grp_stat_list_t *glptr)
{
	pnm_status_t status;

	/* Get the group's status from libpnm */
	(void) pnm_group_status(gr_str, &status);

	/* Store the status in the grp_stat_list struct */
	glptr->status = status.status;

	/* Free the pnm_status_t struct */
	pnm_status_free(&status);

}
