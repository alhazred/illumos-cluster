/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scstat_hadev.c	1.13	08/05/20 SMI"

/*
 * libscstat cluster ha device service status functions
 */

#include <scstat_private.h>
#include <sys/debug.h>

static scstat_errno_t stat_get_cached_hadev(const scconf_cfg_cluster_t *,
    const char *, scstat_ds_t **);

/*
 * scstat_convert_dc_replica_state_code
 *
 * Convert from a 'dc_replica_state_t' enum to a 'scstat_node_pref_t' enum.
 *
 */
scstat_node_pref_t
scstat_convert_dc_replica_state_code(dc_replica_state_t state)
{
	switch (state) {
	case PRIMARY:
		return (SCSTAT_PRIMARY);
	case SECONDARY:
		return (SCSTAT_SECONDARY);
	case SPARE:
		return (SCSTAT_SPARE);
	case INACTIVE:
		return (SCSTAT_INACTIVE);
	case TRANSITION:
		return (SCSTAT_TRANSITION);
	default:
		return (SCSTAT_INVALID);
	}
}

/*
 * stat_convert_dc_error_code
 *
 * convert a dcs error code to scstat error code
 *
 * Possible return values:
 *
 *	SCSTAT_NOERR		- success
 *	SCSTAT_ENOTCLUSTER	- not cluster member
 *	SCSTAT_ESERVICENAME	- invalid service name
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t
stat_convert_dc_error_code(const dc_error_t dc_err)
{
	scstat_errno_t error = SCSTAT_EUNEXPECTED;

	switch (dc_err)	{
	case DCS_SUCCESS:
		error = SCSTAT_ENOERR;
		break;
	case DCS_ERR_NOT_CLUSTER:
		error = SCSTAT_ENOTCLUSTER;
		break;
	case DCS_ERR_SERVICE_NAME:
		error = SCSTAT_ESERVICENAME;
		break;
	case DCS_ERR_CCR_ACCESS:
	default:
		break;
	}
	return (error);
}

/*
 * scstat_convert_sc_state_code
 *
 * Convert from a 'sc_state_code_t' enum to a 'scstat_state_code_t' enum.
 *
 */
scstat_state_code_t
stat_convert_sc_state_code(sc_state_code_t state)
{
	switch (state) {
	case SC_STATE_ONLINE:
		return (SCSTAT_ONLINE);
	case SC_STATE_OFFLINE:
		return (SCSTAT_OFFLINE);
	case SC_STATE_FAULTED:
		return (SCSTAT_FAULTED);
	case SC_STATE_DEGRADED:
		return (SCSTAT_DEGRADED);
	case SC_STATE_WAIT:
		return (SCSTAT_WAIT);
	case SC_STATE_UNKNOWN:
		return (SCSTAT_UNKNOWN);
	case SC_STATE_NOT_MONITORED:
		return (SCSTAT_NOTMONITORED);
	default:
		break;
	}
	ASSERT(0);
	return (SCSTAT_UNKNOWN);
}

/*
 * scstat_get_ds_status
 *
 * If "dsname" is NULL, information for all device services is returned,
 * otherwise information for the specified device service is returned.
 *
 * The caller is responsible for freeing the space returned in "dsstatus".
 *
 * Possible return values:
 *
 *	SCSTAT_NOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EPERM            - not root
 *	SCSTAT_ENOTCLUSTER	- not a cluster node
 *      SCCONF_EINVAL           - invalid argument
 *	SCSTAT_ESERVICENAME	- invalid device group name
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t
scstat_get_ds_status(const scstat_ds_name_t *dsname, scstat_ds_t **dsstatus)
{
	scstat_errno_t error = SCSTAT_ENOERR;
	scconf_errno_t scconf_error = SCCONF_NOERR;
	scconf_cfg_cluster_t *pcluster_config = NULL;

	scconf_error = scconf_get_clusterconfig(&pcluster_config);
	error = scstat_convert_scconf_error_code(scconf_error);
	if (error != SCSTAT_ENOERR) {
		goto cleanup;
	}

	error = scstat_get_ds_status_with_cached_config(pcluster_config,
	    dsname, dsstatus);

cleanup:
	if (pcluster_config != NULL)
		scconf_free_clusterconfig(pcluster_config);

	return (error);
}

/*
 * scstat_get_ds_status_with_cached_config
 *
 * Get device service status with cached configuration.
 * If "dsname" is NULL, information for all device services is returned,
 * otherwise information for the specified device service is returned.
 *
 * The caller is responsible for freeing the space returned in "dsstatus".
 *
 * Possible return values:
 *
 *	SCSTAT_NOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EPERM            - not root
 *	SCSTAT_ENOTCLUSTER	- not a cluster node
 *      SCCONF_EINVAL           - invalid argument
 *	SCSTAT_ESERVICENAME	- invalid device group name
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t
scstat_get_ds_status_with_cached_config(
    const scconf_cfg_cluster_t *pcluster_config,
    const scstat_ds_name_t *dsname, scstat_ds_t **dsstatus)
{
	scstat_errno_t error = SCSTAT_ENOERR;
	int dcs_error = 0;
	dc_error_t dc_err;
	scstat_ds_t *phadev = NULL;
	scstat_ds_t *phadev_current = NULL;
	dc_string_seq_t *service_names = NULL;
	unsigned int i;

	/* Check for valid arguments */
	if (dsstatus == NULL) {
		error = SCSTAT_EINVAL;
		goto cleanup;
	}

	/* initialize the dcs library */
	dcs_error = dcs_initialize();
	if (dcs_error != 0)
		return (SCSTAT_ENOTCLUSTER);


	if (dsname != NULL) {
		error = stat_get_cached_hadev(pcluster_config,
		    (char *)dsname, dsstatus);
		if (error != SCSTAT_ENOERR)
			goto cleanup;
	} else {
		/* get names of all services */
		dc_err = dcs_get_service_names(&service_names);
		error = stat_convert_dc_error_code(dc_err);
		if (error != SCSTAT_ENOERR)
			goto cleanup;

		/* get service status for each service */
		for (i = 0; i < service_names->count; i++) {
			error = stat_get_cached_hadev(pcluster_config,
			    (service_names->strings)[i], &phadev);

			if (error != SCSTAT_ENOERR)
				goto cleanup;

			/* append it to the list */
			if (phadev_current != NULL) {
				phadev_current->scstat_ds_next = phadev;
			} else {
				/* first element */
				*dsstatus = phadev;
			}

			phadev_current = phadev;

			phadev = NULL;
		}
	}

cleanup:
	if (service_names != NULL)
		dcs_free_string_seq(service_names);
	if (error != SCSTAT_ENOERR) {
		scstat_free_ds_status(*dsstatus);
	}

	return (error);
}

/*
 * scstat_free_ds_status
 *
 * Free all memory associated with a scstat_ds_t structure.
 */
void
scstat_free_ds_status(scstat_ds_t *dsstatus)
{
	scstat_ds_t *curr;

	while (dsstatus != NULL) {
		scstat_ds_node_state_t *plreplicas =
		    dsstatus->scstat_node_state_list;
		scstat_ds_node_state_t *preplica;
		scstat_ds_node_state_t *preplica_next;

		free(dsstatus->scstat_ds_name);
		free(dsstatus->scstat_ds_statstr);

		preplica = plreplicas;

		while (preplica != NULL) {
			preplica_next = preplica->scstat_node_next;

			free(preplica->scstat_node_name);
			free(preplica);

			preplica = preplica_next;
		}

		curr = dsstatus;
		dsstatus = dsstatus->scstat_ds_next;
		free(curr);
	}
}

/*
 * stat_get_cached_hadev
 *
 * Get device group configuration from the cached cluster configuration.
 * Upon success, an object of scstat_ds_t is returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_ENOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EPERM            - not root
 *	SCSTAT_ENOTCLUSTER	- not a cluster node
 *      SCCONF_EINVAL           - invalid argument
 *	SCSTAT_ESERVICENAME	- invalid device group name
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
static scstat_errno_t
stat_get_cached_hadev(const scconf_cfg_cluster_t *pcluster_config,
    const char *dsname, scstat_ds_t **dsstatus)
{
	scstat_errno_t error = SCSTAT_ENOERR;
	scconf_errno_t scconf_error = SCCONF_NOERR;
	scstat_ds_t *phadev = NULL;
	scstat_ds_node_state_t *preplica = NULL;
	scstat_ds_node_state_t *preplica_current = NULL;
	char text[SCSTAT_MAX_STRING_LEN];
	unsigned int j;
	int is_suspended;
	dc_replica_status_seq_t *active_replicas = NULL;
	dc_error_t dc_err;
	sc_state_code_t state;

	ASSERT(dsname != NULL);
	ASSERT(dsstatus != NULL);

	phadev = (scstat_ds_t *)calloc(1, sizeof (scstat_ds_t));
	if (phadev == NULL) {
		error = SCSTAT_ENOMEM;
		goto cleanup;
	}

	/* fill in hadev structure */
	phadev->scstat_ds_name = strdup(dsname);
	if (phadev->scstat_ds_name == NULL) {
		error = SCSTAT_ENOMEM;
		goto cleanup;
	}

	dc_err = dcs_get_service_status(dsname, &is_suspended,
	    &active_replicas, &state, NULL);

	error = stat_convert_dc_error_code(dc_err);
	if (error != SCSTAT_ENOERR)
		goto cleanup;

	phadev->scstat_node_state_list = NULL;
	phadev->scstat_ds_status = stat_convert_sc_state_code(state);
	scstat_get_default_status_string(phadev->scstat_ds_status,
	    text);
	phadev->scstat_ds_statstr = strdup(text);

	/* fill in replica list */
	for (j = 0; j < active_replicas->count; j++) {
		preplica = (scstat_ds_node_state_t *)
		    calloc(1, sizeof (scstat_ds_node_state_t));
		if (preplica == NULL) {
			error = SCSTAT_ENOMEM;
			goto cleanup;
		}

		scconf_error = scconf_get_cached_nodename(pcluster_config,
		    (active_replicas->replicas)[j].id,
		    &preplica->scstat_node_name);
		error = scstat_convert_scconf_error_code(scconf_error);
		if (error != SCSTAT_ENOERR)
			goto cleanup;

		preplica->scstat_node_state =
		    scstat_convert_dc_replica_state_code(
		    (active_replicas->replicas)[j].state);
		preplica->scstat_node_next = NULL;

		/* append it to the list */
		if (preplica_current != NULL) /* not first element */
			preplica_current->scstat_node_next = preplica;
		else	/* first element */
			phadev->scstat_node_state_list = preplica;
		preplica_current = preplica;

		/*
		 * set preplica to NULL so we don't free it once it became
		 * part of phadev. Free of phadev will take care of that.
		 */
		preplica = NULL;
	}

	*dsstatus = phadev;
	phadev = NULL;

cleanup:
	free(preplica);
	scstat_free_ds_status(phadev);

	if (active_replicas != NULL)
		dcs_free_dc_replica_status_seq(active_replicas);

	return (error);
}
