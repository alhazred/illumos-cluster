/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scstat_cluster.c	1.18	08/07/24 SMI"

/*
 * libscstat cluster status functions
 */

#include <sys/rsrc_tag.h>
#include <scstat_private.h>

/*
 * scstat_get_default_status_string
 *
 * Get status string from status code. The supplied "text" should
 * be of at least SCSTAT_MAX_STRING_LEN.
 */
void
scstat_get_default_status_string(const scstat_state_code_t state_code,
    char *text)
{
	/* Check arguments */
	if (text == NULL)
		return;

	/* I18N houskeeping */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/* Make sure that the buffer is terminated */
	text[SCSTAT_MAX_STRING_LEN - 1] = '\0';

	switch (state_code) {
	case SCSTAT_ONLINE:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Online"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCSTAT_OFFLINE:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Offline"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCSTAT_FAULTED:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Faulted"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCSTAT_DEGRADED:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Degraded"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCSTAT_WAIT:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Wait"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCSTAT_NOTMONITORED:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Not Monitored"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCSTAT_UNKNOWN:
	default:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Unknown"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	}
}

/*
 * scstat_get_access_status_string
 *
 * Get access status string. The supplied "text" should
 * be of at least SCSTAT_MAX_STRING_LEN.
 */
void
scstat_get_access_status_string(
    const scstat_access_status_t access_status,
    char *text)
{
	/* Check arguments */
	if (text == NULL)
		return;

	/* I18N houskeeping */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/* Make sure that the buffer is terminated */
	text[SCSTAT_MAX_STRING_LEN - 1] = '\0';

	switch (access_status) {
	case SCSTAT_ACCESS_DISABLED:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Disabled"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCSTAT_ACCESS_ENABLED:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Enabled"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	default:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Invalid"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	}
}

/*
 * scstat_get_preference_level_string
 *
 * Get preference level string. The supplied "text" should
 * be of at least SCSTAT_MAX_STRING_LEN.
 */
void
scstat_get_preference_level_string(const scstat_node_pref_t state,
    char *text)
{
	/* Check arguments */
	if (text == NULL)
		return;

	/* I18N houskeeping */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/* Make sure that the buffer is terminated */
	text[SCSTAT_MAX_STRING_LEN - 1] = '\0';

	switch (state) {
	case SCSTAT_PRIMARY:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Primary"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCSTAT_SECONDARY:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Secondary"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCSTAT_SPARE:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Spare"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCSTAT_INACTIVE:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Inactive"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCSTAT_TRANSITION:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Transition"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	default:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Invalid"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	}
}

/*
 * scstat_strerr
 *
 * Map scstat_errno_t to a string.
 *
 * The supplied "text" should be of at least SCSTAT_MAX_STRING_LEN
 * in length.
 */
void
scstat_strerr(scstat_errno_t err, char *text)
{
	/* Check arguments */
	if (text == NULL)
		return;

	/* I18N houskeeping */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/* Make sure that the buffer is terminated */
	text[SCSTAT_MAX_STRING_LEN - 1] = '\0';

	/* Find the string */
	switch (err) {
	case SCSTAT_ENOERR:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "no error"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCSTAT_ENOMEM:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    "not enough memory"), SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCSTAT_ENOTCLUSTER:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    "not a cluster member"), SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCSTAT_ESERVICENAME:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    "invalid device group name"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCSTAT_EUNEXPECTED:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    "unexpected error"), SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCSTAT_EINVAL:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    "invalid argument"), SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCSTAT_ENOTCONFIGURED:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    "object not configured"), SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCSTAT_EPERM:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    "permission denied"), SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCSTAT_ECLUSTERRECONFIG:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    "cluster is reconfiguring"), SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCSTAT_ERGRECONFIG:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    "Resource group is reconfiguring"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCSTAT_EOBSOLETE:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    "Resource/RG has been updated"), SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCSTAT_ENGZONE:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    "You cannot run this command option from a "
		    "non global zone"), SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCSTAT_ECZONE:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    "You cannot run this command from a "
		    "zone cluster."), SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCSTAT_EUSAGE:
		break;
	default:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    "unknown error"), SCSTAT_MAX_STRING_LEN - 1);
		break;
	}
}

/*
 * scstat_convert_scconf_error_code
 *
 * convert a scconf error code to scstat error code
 *
 * Possible return values:
 *
 *	SCSTAT_NOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_ENOTCLUSTER	- not a cluster node
 *	SCSTAT_EINVAL		- scconf: invalid argument
 *	SCSTAT_EPERM		- not root
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t
scstat_convert_scconf_error_code(const scconf_errno_t scconf_error)
{
	scstat_errno_t error = SCSTAT_EUNEXPECTED;

	switch (scconf_error) {
	case SCCONF_NOERR:
		error = SCSTAT_ENOERR;
		break;
	case SCCONF_EPERM:
		error = SCSTAT_EPERM;
		break;
	case SCCONF_ENOCLUSTER:
		error = SCSTAT_ENOTCLUSTER;
		break;
	case SCCONF_ENOMEM:
		error = SCSTAT_ENOMEM;
		break;
	case SCCONF_EINVAL:
		error = SCSTAT_EINVAL;
		break;
	default:
		break;
	}
	return (error);
}

/*
 * scstat_get_cluster_status
 *
 * Upon success, an object of scstat_cluster_t is returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *      SCSTAT_ENOERR           - success
 *      SCSTAT_ENOMEM           - not enough memory
 *	SCSTAT_ENOTCLUSTER	- not a cluster node
 *	SCSTAT_ESERVICENAME	- invalid device group name
 *	SCSTAT_EINVAL		- scconf: invalid argument
 *	SCSTAT_EPERM		- not root
 *	SCSTAT_ERECONFIG	- cluster is reconfiguring
 *	SCSTAT_EOBSOLETE	- Resource/RG has been updated
 *      SCCONF_EUNEXPECTED      - internal or unexpected error
 */
scstat_errno_t
scstat_get_cluster_status(scstat_cluster_t **ppcluster)
{
	scstat_errno_t error = SCSTAT_ENOERR;
	scconf_errno_t scconf_error = SCCONF_NOERR;
	scconf_cfg_cluster_t *pcluster_config = NULL;

	scconf_error = scconf_get_clusterconfig(&pcluster_config);
	error = scstat_convert_scconf_error_code(scconf_error);
	if (error != SCSTAT_ENOERR) {
		goto cleanup;
	}

	error = scstat_get_cluster_status_with_cached_config(pcluster_config,
	    ppcluster);

cleanup:
	if (pcluster_config != NULL)
		scconf_free_clusterconfig(pcluster_config);

	return (error);
}

/*
 * scstat_get_cluster_status_with_cached_config
 *
 * Get cluster status with cached configuration.
 * Upon success, an object of scstat_cluster_t is returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *      SCSTAT_ENOERR           - success
 *      SCSTAT_ENOMEM           - not enough memory
 *	SCSTAT_ENOTCLUSTER	- not a cluster node
 *	SCSTAT_ESERVICENAME	- invalid device group name
 *	SCSTAT_EINVAL		- scconf: invalid argument
 *	SCSTAT_EPERM		- not root
 *	SCSTAT_ERECONFIG	- cluster is reconfiguring
 *	SCSTAT_EOBSOLETE	- Resource/RG has been updated
 *      SCCONF_EUNEXPECTED      - internal or unexpected error
 */
scstat_errno_t
scstat_get_cluster_status_with_cached_config(
    const scconf_cfg_cluster_t *pcluster_config, scstat_cluster_t **ppcluster)
{
	scstat_errno_t error = SCSTAT_ENOERR;
	scstat_cluster_t *pcluster = NULL;

	if (ppcluster == NULL) {
		error = SCSTAT_EINVAL;
		goto cleanup;
	}

	pcluster = (scstat_cluster_t *)calloc(1, sizeof (scstat_cluster_t));
	if (pcluster == NULL) {
		error = SCSTAT_ENOMEM;
		goto cleanup;
	}

	error = stat_get_cached_cluster_name(pcluster_config,
	    &pcluster->scstat_cluster_name);
	if (error != SCSTAT_ENOERR)
		goto cleanup;

	/* get cluster node s */
	error = scstat_get_nodes_with_cached_config(pcluster_config,
	    &pcluster->scstat_node_list);
	if (error != SCSTAT_ENOERR)
		goto cleanup;

	/* get cluster transport */
	error = scstat_get_transport(&pcluster->scstat_transport);
	if (error != SCSTAT_ENOERR)
		goto cleanup;

	/* get ha device services */
	error = scstat_get_ds_status_with_cached_config(pcluster_config,
	    NULL, &pcluster->scstat_ds_list);
	if (error != SCSTAT_ENOERR)
		goto cleanup;

	/* get cluster quorum */
	error = scstat_get_quorum_with_cached_config(pcluster_config,
	    &pcluster->scstat_quorum);
	if (error != SCSTAT_ENOERR)
		goto cleanup;

	/* get resource groups */
	error = scstat_get_rgs(&pcluster->scstat_rg_list);
	if (error != SCSTAT_ENOERR)
		goto cleanup;

	*ppcluster = pcluster;

	pcluster = NULL; /* don't free it if no error */

cleanup:
	if (pcluster != NULL)
		scstat_free_cluster_status(pcluster);

	return (error);
}

/*
 * scstat_free_cluster_status
 *
 * Free all memory associated with a scstat_cluster_t structure.
 */
void
scstat_free_cluster_status(scstat_cluster_t *pcluster)
{
	if (pcluster != NULL) {
		if (pcluster->scstat_cluster_name != NULL)
			free(pcluster->scstat_cluster_name);

		if (pcluster->scstat_node_list != NULL)
			scstat_free_nodes(pcluster->scstat_node_list);

		if (pcluster->scstat_transport != NULL)
			scstat_free_transport(pcluster->scstat_transport);

		if (pcluster->scstat_ds_list != NULL)
			scstat_free_ds_status(pcluster->scstat_ds_list);

		if (pcluster->scstat_quorum != NULL)
			scstat_free_quorum(pcluster->scstat_quorum);

		if (pcluster->scstat_rg_list != NULL)
			scstat_free_rgs(pcluster->scstat_rg_list);

		free(pcluster);
	}
}

/*
 * stat_get_cached_cluster_name
 *
 * Get cluster name from cached cluster configuration. Upon successful,
 * the name of the cluster is returned and it is the caller's
 * responsibility to free it.
 *
 * Possible return value:
 *	SCSTAT_ENOERR		- no error
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EPERM            - not root
 *      SCSTAT_ENOTCLUSTER      - there is no cluster
 *      SCCONF_EINVAL           - invalid argument
 *      SCCONF_EUNEXPECTED      - internal or unexpected error
 */
scstat_errno_t
stat_get_cached_cluster_name(const scconf_cfg_cluster_t *pcluster_config,
    char ** ppclustername)
{
	scstat_errno_t error = SCSTAT_ENOERR;

	if (ppclustername == NULL) {
		error = SCSTAT_EINVAL;
		goto cleanup;
	}

	/* get name of cluster */
	*ppclustername = strdup(pcluster_config->scconf_cluster_clustername);
	if (*ppclustername == NULL) {
		error = SCSTAT_ENOMEM;
		goto cleanup;
	}

cleanup:
	return (error);
}

/*
 * stat_get_object_type
 *
 * Get object type code from type string.
 *
 * Possible return value:
 *	SCSTAT_TYPE_CLUSTER
 *	SCSTAT_TYPE_RT
 *	SCSTAT_TYPE_RG
 *	SCSTAT_TYPE_RESOURCE
 *	SCSTAT_TYPE_NODE
 *	SCSTAT_TYPE_DEVICE_QUORUM
 *	SCSTAT_TYPE_FRAMEWORK
 *	SCSTAT_TYPE_HA_DEVICE_SERVICE
 *	SCSTAT_TYPE_PATH
 *	SCSTAT_TYPE_UNKNOWN
 */
scstat_type_t
stat_get_object_type(const char *type)
{
	/*
	 * convert type string to enum defined by individual component
	 */
	if (strcmp(type, SC_SYSLOG_CCR_CLCONF_TAG) == 0)
		return (SCSTAT_TYPE_CLUSTER);

	if (strcmp(type, SC_SYSLOG_RGM_RT_TAG) == 0)
		return (SCSTAT_TYPE_RT);

	if (strcmp(type, SC_SYSLOG_RGM_RG_TAG) == 0)
		return (SCSTAT_TYPE_RG);

	if (strcmp(type, SC_SYSLOG_RGM_RS_TAG) == 0)
		return (SCSTAT_TYPE_RESOURCE);

	if (strcmp(type, SC_SYSLOG_CMM_NODE_TAG) == 0)
		return (SCSTAT_TYPE_NODE);

	if (strcmp(type, SC_SYSLOG_CMM_QUORUM_TAG) == 0)
		return (SCSTAT_TYPE_DEVICE_QUORUM);

	if (strcmp(type, SC_SYSLOG_DCS_DEVICE_SERVICE_TAG) == 0)
		return (SCSTAT_TYPE_HA_DEVICE_SERVICE);

	if (strcmp(type, SC_SYSLOG_FRAMEWORK_TAG) == 0)
		return (SCSTAT_TYPE_FRAMEWORK);

	if (strcmp(type, SC_SYSLOG_TRANSPORT_PATH_TAG) == 0)
		return (SCSTAT_TYPE_PATH);

	return (SCSTAT_TYPE_UNKNOWN);
}

/*
 * stat_get_state_code_from_quorum_state
 *
 * Map quorum state tp state code.
 *
 * Possible return values:
 *
 *	ONLINE		- resource is running
 *	OFFLINE		- resource is stopped due to user action
 *	FAULTED		- resource is stopped due to a failure
 *	DEGRADED	- resource is running but has a minor problem
 *	WAIT		- resource is in transition from a state to another
 *	UNKNOWN		- resource is monitored but state of the resource is
 *			  not known because either the monitor went down or
 *			  the monitor cannot report resource state temporarily.
 *	NOT_MONITORED	- There is no monitor to check state of the resource
 */
scstat_state_code_t
stat_get_state_code_from_quorum_state(const quorum_state_t quorum_state)
{
	scstat_state_code_t state_code;

	/* map quorum state UP/DOWN to sc_state_code_t */
	switch (quorum_state) {
	case QUORUM_STATE_ONLINE:
		state_code = SCSTAT_ONLINE;
		break;
	case QUORUM_STATE_OFFLINE:
		state_code = SCSTAT_OFFLINE;
		break;
	default:
		state_code = SCSTAT_UNKNOWN;
		break;
	}
	return (state_code);
}
