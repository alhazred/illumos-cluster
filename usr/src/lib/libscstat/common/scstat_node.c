/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scstat_node.c	1.12	08/05/20 SMI"

/*
 * libscstat cluster node status functions
 */

#include <scstat_private.h>

/*
 * scstat_get_nodes
 *
 * Upon success, a list of objects of scstat_node_t are returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_NOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EPERM            - not root
 *      SCSTAT_ENOTCLUSTER      - there is no cluster
 *      SCCONF_EINVAL           - invalid argument
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t
scstat_get_nodes(scstat_node_t **pplnodes)
{
	scstat_errno_t error = SCSTAT_ENOERR;
	scconf_errno_t scconf_error = SCCONF_NOERR;
	scconf_cfg_cluster_t *pcluster_config = NULL;

	scconf_error = scconf_get_clusterconfig(&pcluster_config);
	error = scstat_convert_scconf_error_code(scconf_error);
	if (error != SCSTAT_ENOERR) {
		goto cleanup;
	}

	error = scstat_get_nodes_with_cached_config(pcluster_config, pplnodes);

cleanup:
	if (pcluster_config != NULL)
		scconf_free_clusterconfig(pcluster_config);

	return (error);
}

/*
 * scstat_get_nodes_with_cached_config
 *
 * Get node status with cached configuration.
 * Upon success, a list of objects of scstat_node_t are returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_NOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EPERM            - not root
 *      SCSTAT_ENOTCLUSTER      - there is no cluster
 *      SCCONF_EINVAL           - invalid argument
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t
scstat_get_nodes_with_cached_config(const scconf_cfg_cluster_t *pcluster_config,
    scstat_node_t **pplnodes)
{
	scstat_errno_t error = SCSTAT_ENOERR;
	scconf_errno_t scconf_error = SCCONF_NOERR;
	scstat_node_t *pnode = NULL;
	scstat_node_t *pnode_current = NULL;
	quorum_status_t	*pquorum_status = NULL;
	char text[SCSTAT_MAX_STRING_LEN];
	int quorum_error = 0;
	unsigned int i;

	if (pplnodes == NULL) {
		error = SCSTAT_EINVAL;
		goto cleanup;
	}

	/* initialize clconf */
	quorum_error = clconf_lib_init();
	if (quorum_error != 0) {
		error = SCSTAT_EUNEXPECTED;
		goto cleanup;
	}

	/* get quorum status */
	quorum_error = cluster_get_quorum_status(&pquorum_status);

	error = scstat_convert_quorum_error_code(quorum_error);
	if (error != SCSTAT_ENOERR)
			goto cleanup;

	/* get status of every node */
	for (i = 0; i < pquorum_status->num_nodes; i++) {
		pnode = (scstat_node_t *)calloc(1, sizeof (scstat_node_t));
		if (pnode == NULL) {
			error = SCSTAT_ENOMEM;
			goto cleanup;
		}

		/* fill in node status */
		scconf_error = scconf_get_cached_nodename(pcluster_config,
		    pquorum_status->nodelist[i].nid,
		    &pnode->scstat_node_name);
		error = scstat_convert_scconf_error_code(scconf_error);
		if (error != SCSTAT_ENOERR)
			goto cleanup;

		pnode->scstat_node_status =
		    stat_get_state_code_from_quorum_state(
		    pquorum_status->nodelist[i].state);

		scstat_get_default_status_string(pnode->scstat_node_status,
		    text);
		pnode->scstat_node_statstr = strdup(text);
		if (pnode->scstat_node_statstr == NULL) {
			error = SCSTAT_ENOMEM;
			goto cleanup;
		}

		/* append it to the list */
		if (pnode_current != NULL)
			pnode_current->scstat_node_next = pnode;
		else /* first element */
			*pplnodes = pnode;

		pnode_current = pnode;

		pnode = NULL; /* so we don't accidentally free it */
	}

cleanup:
	if (pquorum_status != NULL)
		cluster_release_quorum_status(pquorum_status);

	scstat_free_node(pnode);

	return (error);
}

/*
 * scstat_get_farmnodes
 *
 * Get farm node status with cached configuration.
 * Upon success, a list of objects of scstat_node_t are returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_NOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EPERM            - not root
 *      SCSTAT_ENOTCLUSTER      - there is no cluster
 *      SCCONF_EINVAL           - invalid argument
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t
scstat_get_farmnodes(scstat_farm_node_t **fnodes, uint_t *offline_count,
    uint_t *online_count)
{
	scstat_errno_t scstat_error = SCSTAT_ENOERR;
	scconf_errno_t scconf_error = SCCONF_NOERR;
	scxcfg_error scxerr;
	int scxret;
	int num = 0;
	scxcfg scxhdl;
	f_property_t *farm_nodes = NULL;
	f_property_t *farmnodes;
	char property[FCFG_MAX_LEN];
	char value[FCFG_MAX_LEN];
	scconf_nodeid_t nodeid;
	scstat_node_name_t nodename;
	scstat_node_statstr_t nodestate;
	scstat_farm_node_t *farmptr, *listhead, *listtail;	/* Offline */
	scstat_farm_node_t *on_listhead, *on_listtail;		/* Online */

	*offline_count = 0;
	*online_count = 0;

	farmptr = listhead = listtail = NULL;
	on_listhead = on_listtail = NULL;

	/* Check for argument */
	if (fnodes == NULL) {
		return (SCSTAT_EINVAL);
	}

	/* Allocate farm config */
	scconf_error = scconf_scxcfg_open(&scxhdl, &scxerr);
	if (scconf_error != SCCONF_NOERR) {
		scstat_error = SCSTAT_EUNEXPECTED;
		goto cleanup;
	}

	/* Get the list of N nodeids from FARM configuration */
	scconf_error = scconf_scxcfg_getlistproperty_value(&scxhdl,
	    NULL, SCCONF_FARM_NODES, &farm_nodes, &num, &scxerr);
	if (scconf_error != SCCONF_NOERR) {
		scstat_error = SCSTAT_EUNEXPECTED;
		goto cleanup;
	}

	if (num == 0 || farm_nodes == NULL) {
		goto cleanup;
	}

	/* Fill fnodes with node names and their membership status */
	for (farmnodes = farm_nodes; farmnodes; farmnodes = farmnodes->next) {

		/* Get the nodeid */
		nodeid = atoi(farmnodes->value);
		if (nodeid < 0)
			continue;

		/* Allocate memory for scstat_farm_node_t */
		farmptr = (scstat_farm_node_t *)malloc(
		    sizeof (scstat_farm_node_t));
		if (farmptr == NULL) {
			scstat_error = SCSTAT_ENOMEM;
			goto cleanup;
		}

		/* Allocate memory for nodename */
		nodename = (scstat_node_name_t)malloc(SCSTAT_MAX_STRING_LEN);
		if (nodename == NULL) {
			scstat_error = SCSTAT_ENOMEM;
			goto cleanup;
		}

		/* Get the nodename */
		scconf_error = scconf_scxcfg_get_nodename(&scxhdl,
		    nodeid, nodename, &scxerr);
		if (scconf_error != SCCONF_NOERR) {
			scstat_error = SCSTAT_EUNEXPECTED;
			goto cleanup;
		}

		/* Allocate memory for nodestate */
		nodestate = (scstat_node_statstr_t)malloc(
		    SCSTAT_MAX_STRING_LEN);
		if (nodestate == NULL) {
			scstat_error = SCSTAT_ENOMEM;
			goto cleanup;
		}

		/* Get the membership status */
		scconf_error = scconf_get_farmnodestate(nodeid, nodestate);
		if (scconf_error != SCCONF_NOERR) {
			scstat_error = SCSTAT_EUNEXPECTED;
			goto cleanup;
		}

		/* Store the nodename, membership state */
		farmptr->scstat_node_name = nodename;
		farmptr->scstat_node_statstr = nodestate;

		farmptr->scstat_node_next = NULL;

		if (strcmp(nodestate, "Offline") == 0) {
			/* Offline nodes */
			if (listhead == NULL) {
				listhead = farmptr;
			}
			if (listtail != NULL) {
				listtail->scstat_node_next = farmptr;
			}
			listtail = farmptr;

			*offline_count += 1;
		} else {
			/* Online nodes */
			if (on_listhead == NULL) {
				on_listhead = farmptr;
			}
			if (on_listtail != NULL) {
				on_listtail->scstat_node_next = farmptr;
			}
			on_listtail = farmptr;

			*online_count += 1;
		}
	}

	if (*offline_count && listtail != NULL) {
		/* Append the online nodes to offline nodes */
		listtail->scstat_node_next = on_listhead;

		/* Append the (offline, online) nodes to fnodes */
		*fnodes = listhead;
	} else {
		/* Append the (online) nodes to fnodes */
		*fnodes = on_listhead;
	}

cleanup:
	if (scstat_error != SCSTAT_ENOERR) {
		scstat_free_farmnodes(listhead);
	}

	/* Free farm_nodes */
	if (farm_nodes) {
		scconf_scxcfg_freelist(farm_nodes);
	}

	/* Release the farm config */
	scconf_scxcfg_close(&scxhdl);

	return (scstat_error);
}

/*
 * scstat_free_nodes
 *
 * Free all memory associated with a scstat_node_t structure.
 */
void
scstat_free_nodes(scstat_node_t *plnodes)
{
	scstat_node_t *pnode = plnodes;
	scstat_node_t *pnode_next;

	while (pnode != NULL) {
		pnode_next = pnode->scstat_node_next;

		scstat_free_node(pnode);
		pnode = pnode_next;
	}
}

/*
 * scstat_free_farmnodes
 *
 * Free all memory associated with a scstat_node_t structure.
 */
void
scstat_free_farmnodes(scstat_farm_node_t *fnodes)
{
	scstat_farm_node_t *fnode = fnodes;
	scstat_farm_node_t *fnode_next;

	while (fnode != NULL) {
		fnode_next = fnode->scstat_node_next;

		scstat_free_farmnode(fnode);
		fnode = fnode_next;
	}
}

/*
 * scstat_get_node
 *
 * Upon success, an object of scstat_node_t is returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_ENOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 * 	SCSTAT_EPERM            - not root
 *      SCSTAT_ENOTCLUSTER      - there is no cluster
 *      SCCONF_EINVAL           - invalid argument
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t
scstat_get_node(const scstat_node_name_t node_name, scstat_node_t **ppnode)
{
	scstat_errno_t error = SCSTAT_ENOERR;
	scconf_errno_t scconf_error = SCCONF_NOERR;
	scconf_cfg_cluster_t *pcluster_config = NULL;

	scconf_error = scconf_get_clusterconfig(&pcluster_config);
	error = scstat_convert_scconf_error_code(scconf_error);
	if (error != SCSTAT_ENOERR) {
		goto cleanup;
	}

	error = scstat_get_node_with_cached_config(pcluster_config, node_name,
	    ppnode);

cleanup:
	if (pcluster_config != NULL)
		scconf_free_clusterconfig(pcluster_config);

	return (error);
}

/*
 * scstat_get_node_with_cached_config
 *
 * Get node status with cached configuration.
 * Upon success, an object of scstat_node_t is returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_ENOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 * 	SCSTAT_EPERM            - not root
 *      SCSTAT_ENOTCLUSTER      - there is no cluster
 *      SCCONF_EINVAL           - invalid argument
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t
scstat_get_node_with_cached_config(const scconf_cfg_cluster_t *pcluster_config,
    const scstat_node_name_t node_name, scstat_node_t **ppnode)
{
	scstat_errno_t error = SCSTAT_ENOERR;
	scconf_errno_t scconf_error = SCCONF_NOERR;
	scstat_node_t *pnode = NULL;
	quorum_status_t	*pquorum_status = NULL;
	char *quorum_node_name = NULL;
	char text[SCSTAT_MAX_STRING_LEN];
	int quorum_error = 0;
	unsigned int i;

	if (node_name == NULL) {
		error = SCSTAT_EINVAL;
		goto cleanup;
	}

	if (ppnode == NULL) {
		error = SCSTAT_EINVAL;
		goto cleanup;
	}

	/* initialize clconf */
	quorum_error = clconf_lib_init();
	if (quorum_error != 0) {
		error = SCSTAT_EUNEXPECTED;
		goto cleanup;
	}

	/* get quorum status */
	quorum_error = cluster_get_quorum_status(&pquorum_status);
	error = scstat_convert_quorum_error_code(quorum_error);
	if (error != SCSTAT_ENOERR)
		goto cleanup;

	/* get status of specified node */
	for (i = 0; i < pquorum_status->num_nodes; i++) {
		scconf_error = scconf_get_cached_nodename(pcluster_config,
		    pquorum_status->nodelist[i].nid,
		    &quorum_node_name);
		error = scstat_convert_scconf_error_code(scconf_error);
		if (error != SCSTAT_ENOERR)
			goto cleanup;

		if (strcmp(quorum_node_name, node_name) == 0) {
			/* node found */
			pnode = (scstat_node_t *)calloc(1,
			    sizeof (scstat_node_t));
			if (pnode == NULL) {
				error = SCSTAT_ENOMEM;
				goto cleanup;
			}

			/* fill in node status */
			pnode->scstat_node_name = quorum_node_name;
			quorum_node_name = NULL;

			pnode->scstat_node_status =
			    stat_get_state_code_from_quorum_state(
			    pquorum_status->nodelist[i].state);

			scstat_get_default_status_string(
			    pnode->scstat_node_status, text);
			pnode->scstat_node_statstr = strdup(text);
			if (pnode->scstat_node_statstr == NULL) {
				error = SCSTAT_ENOMEM;
				goto cleanup;
			}

			*ppnode = pnode;

			pnode = NULL; /* so we don't accidentally free it */

			break;
		}

		if (quorum_node_name != NULL) {
			free(quorum_node_name);
			quorum_node_name = NULL;
		}
	}

cleanup:
	/* free space */
	if (pquorum_status != NULL)
		cluster_release_quorum_status(pquorum_status);

	if (quorum_node_name != NULL)
		free(quorum_node_name);

	scstat_free_node(pnode);

	return (error);
}

/*
 * scstat_free_node
 *
 * Free all memory associated with a scstat_node_t structure.
 */
void
scstat_free_node(scstat_node_t *pnode)
{
	if (pnode != NULL) {
		if (pnode->scstat_node_name != NULL)
			free(pnode->scstat_node_name);

		if (pnode->scstat_node_statstr != NULL)
			free(pnode->scstat_node_statstr);

		free(pnode);
	}
}


/*
 * scstat_free_farmnode
 *
 * Free all memory associated with a scstat_farm_node_t structure.
 */
void
scstat_free_farmnode(scstat_farm_node_t *fnode)
{
	if (fnode != NULL) {
		if (fnode->scstat_node_name != NULL)
			free(fnode->scstat_node_name);

		if (fnode->scstat_node_statstr != NULL)
			free(fnode->scstat_node_statstr);

		free(fnode);
	}
}
