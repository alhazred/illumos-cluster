/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scstat_transport.c	1.22	08/05/20 SMI"

/*
 * libscstat cluster ha device service status functions
 */

#include <scstat_private.h>

/*
 * scstat_get_transport
 *
 * Upon success, an object of scstat_transport_t is returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_ENOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
/* XXX - incomplete function; remove ARGSUSED when complete */
/* ARGSUSED */
scstat_errno_t
scstat_get_transport(scstat_transport_t **pptransport)
{
	scstat_errno_t error = SCSTAT_ENOERR;
	int clconf_error;
	cladmin_component_state_list_t *plobjects = NULL;
	scstat_path_t *ppath = NULL;
	scstat_path_t *ppath_last = NULL;
	unsigned int i;

	if (pptransport == NULL) {
		error = SCSTAT_EINVAL;
		goto cleanup;
	}

	/* initialize clconf */
	clconf_error = clconf_lib_init();
	if (clconf_error != 0) {
		error = SCSTAT_EUNEXPECTED;
		goto cleanup;
	}

	/* get state for all transport components */
	plobjects = cladmin_get_component_state_list();
	if (plobjects->listlen > 0) { /* not empty list */
		*pptransport = (scstat_transport_t *)
		    calloc(1, sizeof (scstat_transport_t));
		if (*pptransport == NULL) {
			error = SCSTAT_ENOMEM;
			goto cleanup;
		}
	}

	/*
	 * for every transport component, make a scstat object and
	 * add it to the list
	 */
	for (i = 0; i < plobjects->listlen; i++) {
		scstat_type_t type =
		    stat_get_object_type(plobjects->list[i].type);

		switch (type) {
		case SCSTAT_TYPE_PATH:
			ppath = (scstat_path_t *)
			    calloc(1, sizeof (scstat_path_t));
			if (ppath == NULL) {
				error = SCSTAT_ENOMEM;
				goto cleanup;
			}

			/* fill in path structure */
			ppath->scstat_path_name =
			    strdup(plobjects->list[i].int_name);
			if (ppath->scstat_path_name == NULL) {
				error = SCSTAT_ENOMEM;
				goto cleanup;
			}

			ppath->scstat_path_status =
			    plobjects->list[i].state;

			ppath->scstat_path_statstr =
			    strdup(plobjects->list[i].state_string);
			if (ppath->scstat_path_statstr == NULL) {
				error = SCSTAT_ENOMEM;
				goto cleanup;
			}

			/* XXX get path property from query interface */
			/* add it to path list */
			if (ppath_last == NULL) { /* first path */
				(*pptransport)->scstat_path_list = ppath;
				ppath_last = ppath;
			} else { /* append to the end of the list */
				ppath_last->scstat_path_next = ppath;
				ppath_last = ppath;
			}

			ppath = NULL;
			break;
		case SCSTAT_TYPE_UNKNOWN:
		case SCSTAT_TYPE_CLUSTER:
		case SCSTAT_TYPE_NODE:
		case SCSTAT_TYPE_HA_DEVICE_SERVICE:
		case SCSTAT_TYPE_DEVICE_QUORUM:
		case SCSTAT_TYPE_NODE_QUORUM:
		case SCSTAT_TYPE_RT:
		case SCSTAT_TYPE_RG:
		case SCSTAT_TYPE_RESOURCE:
		case SCSTAT_TYPE_FRAMEWORK:
		default:
			break;
		} /*lint !e788 */
	}

cleanup:
	/* free up space */
	if (ppath != NULL)
		scstat_free_path(ppath);

	if (plobjects != NULL)
		cladmin_free_component_state_list(plobjects);

	return (error);
}

/*
 * scstat_free_transport
 *
 * Free all memory associated with a scstat_transport_t structure.
 */
void
scstat_free_transport(scstat_transport_t *ptransport)
{
	scstat_path_t *ppath;
	scstat_path_t *ppath_next;

	if (ptransport != NULL) {
		/* free path list */
		ppath = ptransport->scstat_path_list;
		while (ppath != NULL) {
			ppath_next = ppath->scstat_path_next;
			scstat_free_path(ppath);
			ppath = ppath_next;
		}
		free(ptransport);
	}
}

/*
 * scstat_get_path
 *
 * Upon success, an object of scstat_path_t is returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_NOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EUNEXPECTED	- internal or unexpected error
 */
scstat_errno_t
scstat_get_path(const scstat_path_name_t path_name, scstat_path_t **pppath)
{
	scstat_errno_t error = SCSTAT_ENOERR;
	int clconf_error;
	scstat_path_t *ppath = NULL;
	cladmin_component_state_t *pstate = NULL;

	if (path_name == NULL) {
		error = SCSTAT_EINVAL;
		goto cleanup;
	}

	if (pppath == NULL) {
		error = SCSTAT_EINVAL;
		goto cleanup;
	}

	/* initialize clconf */
	clconf_error = clconf_lib_init();
	if (clconf_error != 0) {
		error = SCSTAT_EUNEXPECTED;
		goto cleanup;
	}

	/* get object from query interface */
	pstate = cladmin_get_component_state(path_name);

	if (pstate == NULL) {
		error = SCSTAT_EUNEXPECTED;
		goto cleanup;
	} else {
		ppath = (scstat_path_t *)
		    calloc(1, sizeof (scstat_path_t));
		if (ppath == NULL) {
			error = SCSTAT_ENOMEM;
			goto cleanup;
		}

		/* fill in path structure */
		ppath->scstat_path_status = pstate->state;
		ppath->scstat_path_statstr =
		    strdup(pstate->state_string);
		if (ppath->scstat_path_statstr == NULL) {
			error = SCSTAT_ENOMEM;
			goto cleanup;
		}

		/* get path property from query interface */
		error = scstat_get_path_endpoints(path_name,
		    &ppath->scstat_path_epoint1.scstat_adapter_name,
		    &ppath->scstat_path_epoint2.scstat_adapter_name);
		if (error != SCSTAT_ENOERR)
			goto cleanup;

		*pppath = ppath;

		ppath = NULL; /* so we don't accidentally free it */
	}

cleanup:
	if (pstate != NULL)
		cladmin_free_component_state(pstate);

	scstat_free_path(ppath);

	return (error);
}

/*
 * scstat_free_path
 *
 * Free all memory associated with a scstat_path_t structure.
 */
void
scstat_free_path(scstat_path_t *ppath)
{
	if (ppath != NULL) {
		if (ppath->scstat_path_name != NULL)
			free(ppath->scstat_path_name);

		if (ppath->scstat_path_statstr != NULL)
			free(ppath->scstat_path_statstr);

		if (ppath->scstat_path_epoint1.scstat_adapter_name != NULL)
			free(ppath->scstat_path_epoint1.scstat_adapter_name);

		if (ppath->scstat_path_epoint2.scstat_adapter_name != NULL)
			free(ppath->scstat_path_epoint2.scstat_adapter_name);

		free(ppath);
	}
}

/*
 * scstat_get_path_endpoints
 *
 * Upon success, two port names are returned.
 * The caller is responsible for freeing the space.
 *
 * Possible return values:
 *
 *	SCSTAT_NOERR		- success
 *	SCSTAT_ENOMEM		- not enough memory
 *	SCSTAT_EINVAL		- invalid argument
 */
scstat_errno_t
scstat_get_path_endpoints(const char *path_name, char **padapter_name1,
    char **padapter_name2)
{
	scstat_errno_t error = SCSTAT_ENOERR;
	char *beginpos = NULL;
	char *endpos = NULL;
	const char *prefix = "cluster";

	if (path_name == NULL) {
		error = SCSTAT_EINVAL;
		goto end;
	}

	if (padapter_name1 == NULL) {
		error = SCSTAT_EINVAL;
		goto end;
	}

	if (padapter_name2 == NULL) {
		error = SCSTAT_EINVAL;
		goto end;
	}

	beginpos = strstr(path_name, ".nodes.");
	if (beginpos == NULL) {
		error = SCSTAT_EINVAL;
		goto end;
	}

	endpos = strstr(beginpos + strlen(".nodes."), ".nodes.");
	if (endpos == NULL) {
		error = SCSTAT_EINVAL;
		goto end;
	}

	*padapter_name1 = (char *)calloc(1,
	    (size_t)(endpos - beginpos + (int)strlen(prefix) + 1));
	if (*padapter_name1 == NULL) {
		error = SCSTAT_ENOMEM;
		goto end;
	}
	(void) strcpy(*padapter_name1, prefix);
	(void) strncat(*padapter_name1, beginpos, (size_t)(endpos - beginpos));

	beginpos = endpos;
	*padapter_name2 = (char *)calloc(1,
	    strlen(beginpos) + strlen(prefix) + 1);
	if (*padapter_name2 == NULL) {
		error = SCSTAT_ENOMEM;
		goto end;
	}
	(void) strcpy(*padapter_name2, prefix);
	(void) strcat(*padapter_name2, beginpos);

end:
	return (error);
}

/*
 * scstat_get_transport_adapter_name
 *
 *	Get name of a transport adapter.
 */
scstat_errno_t
scstat_get_transport_adapter_name(const char *iname, char *name)
{
	scstat_errno_t error = SCSTAT_ENOERR;
	scconf_errno_t scconf_error = SCCONF_NOERR;
	scconf_cfg_cluster_t *pcluster_config = NULL;

	scconf_error = scconf_get_clusterconfig(&pcluster_config);
	error = scstat_convert_scconf_error_code(scconf_error);
	if (error != SCSTAT_ENOERR) {
		goto cleanup;
	}

	error = scstat_get_cached_transport_adapter_name(pcluster_config,
	    iname, name);

cleanup:
	if (pcluster_config != NULL)
		scconf_free_clusterconfig(pcluster_config);

	return (error);
}

/*
 * scstat_get_cached_transport_adapter_name
 *
 *	Get name of a transport adapter from cached configuration.
 */
scstat_errno_t
scstat_get_cached_transport_adapter_name(\
    const scconf_cfg_cluster_t *pcluster_config,
    const char *iname, char *name)
{
	scstat_errno_t	error = SCSTAT_ENOERR;
	scconf_cfg_node_t *pnode_cfg = NULL;
	scconf_cfg_cltr_adap_t *padapter_cfg = NULL;
	boolean_t found = B_FALSE;

	if (iname == NULL) {
		error = SCSTAT_EINVAL;
		goto cleanup;
	}

	/* iterate through the node list */
	pnode_cfg = pcluster_config->scconf_cluster_nodelist;
	while (!found && pnode_cfg != NULL) {
		/* iterate through the adapter list of every node */
		padapter_cfg = pnode_cfg->scconf_node_adapterlist;
		while (!found && padapter_cfg != NULL) {
			if (strcmp(iname, padapter_cfg->scconf_adap_iname)
			    == 0) {
				/* found matching adapter */
				char *node_name;
				char *adapter_name;

				node_name = pnode_cfg->
				    scconf_node_nodename;
				adapter_name = padapter_cfg->
				    scconf_adap_adaptername;

				(void) snprintf(name, SCSTAT_MAX_STRING_LEN,
				    "%s:%s", node_name, adapter_name);

				found = B_TRUE;
			}
			padapter_cfg = padapter_cfg->scconf_adap_next;
		}
		pnode_cfg = pnode_cfg->scconf_node_next;
	}

	if (found == B_FALSE) {
		error = SCSTAT_ENOTCONFIGURED;
		goto cleanup;
	}

cleanup:
	return (error);
}
