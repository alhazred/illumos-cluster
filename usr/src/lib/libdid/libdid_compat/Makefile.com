#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#pragma ident	"@(#)Makefile.com	1.20	09/01/29 SMI"
#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# usr/src/lib/libdid/libdid_compat/Makefile.com
#
LIBRARY = libdid_compat.a 
VERS = .1

OBJECTS = didcommon.o did_shared.o didconf.o

# include library definitions, do not change order of include and DYNLIB
include ../../../Makefile.lib

MAPFILE=
PMAP =
SRCS =		$(OBJECTS:%.o=../../common/%.c)

LIBS = $(DYNLIB)

CLEANFILES =

# definitions for lint
LINTFLAGS += -I../../include
LINTFILES = $(OBJECTS:%.o=%.ln)

LDLIBS += -lc -ldl -lclos -ldevinfo -lclcomm

CPPFLAGS += $(CL_CPPFLAGS)
CPPFLAGS += -I../../include -I$(SRC)/lib/libclcomm/common/ \
	    -I$(SRC)/head/scadmin/rpc

.KEEP_STATE:

all:  $(LIBS)

$(DYNLIB): 	$(MAPFILE)

# include library targets
include ../../../Makefile.targ

objs/%.o pics/%.o: ../../common/%.c
	$(COMPILE.c) -o $@ $<
