/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)didcommon.c	1.66	08/05/21 SMI"

#include <stdio.h>
#include <fcntl.h>
#include <malloc.h>
#include <unistd.h>
#include <stdlib.h>
#include <strings.h>
#include <devid.h>
#include <dlfcn.h>
#include <sys/param.h>
#include <sys/sunddi.h>
#include <sys/ddi_impldefs.h>
#include <sys/dditypes.h>
#include <sys/stat.h>
#include <sys/didio.h>
#include <sys/mkdev.h>
#include <sys/cladm_int.h>
#include <string.h>
#include <dirent.h>
#include <ftw.h>
#include <netdb.h>
#include <libintl.h>
#include <pthread.h>
#include <sys/scsi/impl/uscsi.h>
#include <sys/cl_efi.h>
#ifdef SC_EFI_SUPPORT
#include <sys/dkio.h>
#include <errno.h>
#include <libgen.h>
#endif
#include "libdid.h"
#include "didprivate.h"

/*lint -e611 */
/*
 * From FlexLint book
 * Warning(611) Suspicious cast -- Eithera pointer to a function is
 * being cast to a pointer to an object or vice versa. This is
 * regarded as questionnable by ANSI standard. If this is not a user
 * error, suppress this warning.
 */

int (*didinternal_getnodeid)(char *, nodeid_t *);
int (*didinternal_getnodename)(nodeid_t, char *);
int (*didinternal_getinstlist)(did_device_list_t **, int, did_device_type_t *,
    char *);
int (*didinternal_getrepllist)(did_repl_list_t **);
int (*didinternal_getinst)(did_device_list_t **, int);
int (*didinternal_freeinstlist)(did_device_list_t *);
int (*didinternal_saveinstlist)(did_device_list_t *, char *, char *, int);
int (*didinternal_saveinst)(did_device_list_t *, int);
int (*didinternal_add_repl_entry)(did_repl_list_t *);
int (*didinternal_rm_repl_entry)(int);
int (*didinternal_read_repl_entry)(int, char **);
int (*didinternal_allocinstance)(did_subpath_t *, did_device_list_t **,
    char *);
int (*didinternal_allocinstances)(did_device_list_t *, did_device_list_t **,
    char *);
int (*didinternal_gettypelist)(did_device_type_t **);
int (*didinternal_savetypelist)(did_device_type_t *);
int (*didinternal_mapdidtodev)(char *, did_subpath_t *);
int (*didinternal_mapdevtodid)(char *, did_subpath_t *);
int (*didinternal_exec_program_local)(char *);
int (*didinternal_syncgdevs)(void);
int (*didinternal_syncrepl)(char *, char *);
int (*didinternal_initlibspecific)(void);
int (*didinternal_holdccrlock)(void);
int (*didinternal_releaseccrlock)(int);
int (*didinternal_populate_list_cache)(did_device_list_t *);
int (*didinternal_getglobalfencingstatus)(unsigned short *);
int (*didinternal_setglobalfencingstatus)(unsigned short);
static void did_fork_prepare(void);
static void did_fork_parent(void);
static void did_fork_children(void);
static void did_lib_pthread_init(void);
static void did_lib_init_once(void);
static void did_free_key(void *);

/*
 * To make DID libraries threadsafe, it is necessary to ensure that all DID
 * libraries exported functions (the DID libraries API, see libdid.h) are
 * mutually exclusive. For example, one thread cannot execute did_initlibrary()
 * while another executes did_getinstlist() because did_initlibrary() can
 * change the value of didinternal_getinstlist(), called by did_getinstlist().
 *
 * This lock (did_lock) must be recursive (PTHREAD_MUTEX_RECURSIVE), because
 * some DID libraries API functions are using internaly other DID libraries API
 * functions. For example, did_allocinstance() (which takes did_lock because
 * part of the DID libraries API) calls did_impl_allocinstance() which calls
 * did_ccr_instance_to_str() (in the CCR library form) which calls
 * did_getnodeid() (which takes did_lock also, because part of the DID
 * libraries API). So did_lock must be recursive.
 */
static pthread_mutex_t did_lock;

/*
 * did_error_key is a pthread key for the following thread-specific data: a per
 * thread DID error string. In the previous version of this file, the DID error
 * string was a global value 'char did_error[256]', which was not thread safe.
 * DID libraries API function callers must know use the function
 * did_geterrorstr() to read the per thread DID error string, there is no more
 * did_error[] global string to be thread safe.
 */
static pthread_key_t did_error_key;


/*
 * Extract the did_device_type_t element of value 'dtype' from the
 * did_device_type_t list 'typelist', destroy remainder did_device_type_t list
 * 'typelist'.
 */
static did_device_type_t *
did_device_type_extract(did_device_type_t *typelist, char *dtype)
{
	did_device_type_t *cur_ptyp, *ptyp;

	ptyp = NULL;
	cur_ptyp = typelist;
	while (cur_ptyp != NULL) {
		if (strcmp(cur_ptyp->type_name, dtype) == 0) {
			ptyp = cur_ptyp;
			break;
		}
		cur_ptyp = cur_ptyp->next;
	}
	return (ptyp);
}


/*
 * Remove the did_device_list_t element 'dev' from the did_device_list_t list
*  'pdevlist'.
 */
static void
did_device_list_remove(did_device_list_t **pdevlist, did_device_list_t *dev)
{
	did_device_list_t *cur;
	did_device_list_t *prev = NULL;

	cur = *pdevlist;
	while (cur != NULL) {
		if (cur == dev) {
			if (prev != NULL) {
				prev->next = cur->next;
			} else {
				*pdevlist = (*pdevlist)->next;
			}
			break;
		}
		prev = cur;
		cur = cur->next;
	}
}


/*
 * Implementation of did_init_interface_t vector.
 */
int
did_initlibspecific(void)
{
	int ret;
	/* I18N */
	DID_LOCK(did_lock);
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	if (didinternal_initlibspecific == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: Cannot initialize "
			"library interface.\n"), "did_initlibspecific");
		DID_UNLOCK(did_lock);
		return (-1);
	}
	ret = didinternal_initlibspecific();
	DID_UNLOCK(did_lock);
	return (ret);
}


/*
 * This function initializes the DID library for future use. It
 * expects a version number for the interface, as well as a mode.
 * If the mode is null, the library will determine if is in clustered,
 * non-clustered, or 2.2 compatability mode. Or, the mode can be
 * explicitly set. The tablename argument is not currently used.
 * The function returns DID_INIT_SUCCESS on success. If it failed because
 * we couldn't load the orb, it returns DID_INIT_ORBFAIL, so that we
 * can retry in file mode. If it failed for some other reason, it returns
 * DID_INIT_FAIL, and sets did_geterrorstr().
 *
 * This API is also called during jumpstart which uses a mount point
 * that is different from "/" to mount the auto install directory.
 * It thus reads the SC_BASEDIR envrionement variable and append it
 * to the library paths.
 */
/* ARGSUSED */
int
did_initlibrary(int version, did_if_t mode, char *tablename)
{
	int bootflags;
	static void *dl_handle = NULL;
	static int c_version = -1;
	static did_if_t c_mode = -1;
	char *base_dir;
	char lib_path[BUFSIZ];

	DID_LOCK(did_lock);

	if ((version != -1) && (mode != -1) &&
	    (version == c_version) && (c_mode == mode)) {
		DID_UNLOCK(did_lock);
		return (DID_INIT_SUCCESS);
	}

	if (dl_handle != NULL) {
		(void) dlclose(dl_handle);
		dl_handle = NULL;
	}

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (version != DID_LIB_INTERFACE_VERSION) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: Library version %d "
			"invalid.\n"), "did_initlibrary", version);
		c_version = -1;
		c_mode = -1;
		DID_UNLOCK(did_lock);
		return (DID_INIT_FAIL);
	}

	/* Get the base dir */
	base_dir = getenv("SC_BASEDIR");
	if (!base_dir) {
		base_dir = "/";
	}

	switch (mode) {
		case (DID_CONF_FILE):
			(void) sprintf(lib_path,
			    "%s/usr/cluster/lib/libdid_compat.so.1", base_dir);
			dl_handle = dlopen(lib_path,
			    RTLD_LAZY | RTLD_PARENT);
			break;
		case (DID_CCR_FILE):
			dl_handle = dlopen(
			    "/usr/cluster/lib/libdid_ccrfile.so.1",
			    RTLD_LAZY | RTLD_PARENT);
			break;
		case (DID_CCR):
			dl_handle = dlopen("/usr/cluster/lib/libdid_ccr.so.1",
			    RTLD_LAZY | RTLD_PARENT);
			break;
		case (DID_NONE):
		default:
			break;
	}

	if (dl_handle == NULL) {
		if (cladm(CL_INITIALIZE, CL_GET_BOOTFLAG, &bootflags) != 0) {
			(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN, "Cannot determine cluster "
				"boot mode.\n"));
			c_version = -1;
			c_mode = -1;
			DID_UNLOCK(did_lock);
			return (DID_INIT_FAIL);
		}
		switch (bootflags & (CLUSTER_CONFIGURED | CLUSTER_BOOTED)) {
		case (CLUSTER_CONFIGURED | CLUSTER_BOOTED):
			dl_handle = dlopen("/usr/cluster/lib/libdid_ccr.so.1",
			    RTLD_LAZY | RTLD_PARENT);
			break;
		case (CLUSTER_CONFIGURED):
			dl_handle = dlopen(
			    "/usr/cluster/lib/libdid_ccrfile.so.1",
			    RTLD_LAZY | RTLD_PARENT);
			break;
		default:
			dl_handle = dlopen(
			    "/usr/cluster/lib/libdid_compat.so.1",
			    RTLD_LAZY | RTLD_PARENT);
			break;
		}
	}

	if (dl_handle == NULL) {
		/* Can't dlopen the appropriate library */
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
			dgettext(TEXT_DOMAIN, "%s: Cannot open appropriate "
			"library mode.\n"), "did_initlibrary");
		c_version = -1;
		c_mode = -1;
		DID_UNLOCK(did_lock);
		return (DID_INIT_FAIL);
	}

	didinternal_getnodename = (int (*)(nodeid_t, char *))
		dlsym(dl_handle, "did_impl_getnodename");

	didinternal_getnodeid = (int (*)(char *, nodeid_t *))
		dlsym(dl_handle, "did_impl_getnodeid");

	didinternal_getinstlist = (int (*)(did_device_list_t **, int,
	    did_device_type_t *, char *)) dlsym(dl_handle,
		"did_impl_getinstlist");

	didinternal_getrepllist = (int (*)(did_repl_list_t **)) dlsym(dl_handle,
		"did_impl_getrepllist");

	didinternal_getinst = (int (*)(did_device_list_t **, int))
		dlsym(dl_handle, "did_impl_getinst");

	didinternal_freeinstlist = (int (*)(did_device_list_t *))
		dlsym(dl_handle, "did_impl_freeinstlist");

	didinternal_saveinstlist = (int (*)(did_device_list_t *, char *,
	    char *, int)) dlsym(dl_handle, "did_impl_saveinstlist");

	didinternal_saveinst = (int (*)(did_device_list_t *, int))
		dlsym(dl_handle, "did_impl_saveinst");

	didinternal_add_repl_entry = (int (*)(did_repl_list_t *))
		dlsym(dl_handle, "did_impl_add_repl_entry");

	didinternal_rm_repl_entry = (int (*)(int))
		dlsym(dl_handle, "did_impl_rm_repl_entry");

	didinternal_read_repl_entry = (int (*)(int, char **))
		dlsym(dl_handle, "did_impl_read_repl_entry");

	didinternal_allocinstance = (int (*)(did_subpath_t *,
	    did_device_list_t **, char *)) dlsym(dl_handle,
		"did_impl_allocinstance");

	didinternal_allocinstances = (int (*)(did_device_list_t *,
	    did_device_list_t **, char *)) dlsym(dl_handle,
		"did_impl_allocinstances");

	didinternal_gettypelist = (int (*)(did_device_type_t **))
		dlsym(dl_handle, "did_impl_gettypelist");

	didinternal_savetypelist = (int (*)(did_device_type_t *))
		dlsym(dl_handle, "did_impl_savetypelist");

	didinternal_mapdidtodev = (int (*)(char *, did_subpath_t *))
		dlsym(dl_handle, "did_impl_mapdidtodev");

	didinternal_mapdevtodid = (int (*)(char *, did_subpath_t *))
		dlsym(dl_handle, "did_impl_mapdevtodid");

	didinternal_exec_program_local = (int (*)(char *))
		dlsym(dl_handle, "did_impl_exec_program_local");

	didinternal_syncgdevs = (int (*)(void))
		dlsym(dl_handle, "did_impl_syncgdevs");

	didinternal_syncrepl = (int (*)(char *, char *))
		dlsym(dl_handle, "did_impl_syncrepl");

	didinternal_initlibspecific = (int (*)(void))
		dlsym(dl_handle, "did_impl_initlibspecific");

	didinternal_holdccrlock = (int (*)(void))
		dlsym(dl_handle, "did_impl_holdccrlock");

	didinternal_releaseccrlock = (int (*)(int))
		dlsym(dl_handle, "did_impl_releaseccrlock");

	didinternal_populate_list_cache = (int (*)(did_device_list_t *))
		dlsym(dl_handle, "did_impl_populate_list_cache");

	didinternal_getglobalfencingstatus = (int (*)(unsigned short *))
		dlsym(dl_handle, "did_impl_getglobalfencingstatus");

	didinternal_setglobalfencingstatus = (int (*)(unsigned short))
		dlsym(dl_handle, "did_impl_setglobalfencingstatus");

	/*
	 * Need to return a different error code if we fail in the
	 * library-specific initiation so
	 * that we can retry using the file interfaces.
	 */
	if (did_initlibspecific()) {
		c_version = -1;
		c_mode = -1;
		if (dl_handle != NULL) {
			(void) dlclose(dl_handle);
			dl_handle = NULL;
		}
		DID_UNLOCK(did_lock);
		return (DID_INIT_ORBFAIL);
	}

	if ((version != -1) && (mode != -1)) {
		c_version = version;
		c_mode = mode;
	}

	DID_UNLOCK(did_lock);

	return (DID_INIT_SUCCESS);
}

/* Currently unused. */
int
did_ccr_device_type_to_str(did_device_type_t *tp, char **out)
{
	char	*buf;
	uint_t	maxlen;
	int	i;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (tp == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Invalid input to %s.\n"),
			"did_ccr_device_type_to_str");
		return (-1);
	}

	maxlen = sizeof (did_device_type_t) + DID_CCR_STRING_PAD;
	if ((buf = (char *)malloc(maxlen)) == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Out of memory.\n"));
		return (-1);
	}
	(void) memset(buf, 0, maxlen);

	(void) sprintf(buf, "%d||%s|%s|%d|%d|%d|%d",
		tp->type_num, tp->primary_subdir, tp->discovery_minor,
		tp->name_offset_from_inst, tp->default_nodes,
		tp->start_default_nodes, tp->next_default_node);

	for (i = 0; i < tp->minor_count; i++) {
		(void) sprintf(&buf[strlen(buf)], "|%d:%s:%s:%c",
			tp->minor_num[i], tp->minor_name[i],
			tp->cur_minor_name[i],
			(tp->device_class[i] == S_IFCHR? 'c': 'b'));

	}
	*out = buf;
	return (0);
}

/*
 * Implementation of dlsym'ed internal functions.
 */
/*
 * Given a nodeid_t, return the nodename string it maps to.
 * If no mapping is found, return -1.
 * If the internal implementation does not exist, return -2.
 */
int
did_getnodename(nodeid_t nodeid, char *nodename)
{
	int ret;
	DID_LOCK(did_lock);
	if (didinternal_getnodename == NULL) {
		DID_UNLOCK(did_lock);
		return (-2);
	}
	ret = didinternal_getnodename(nodeid, nodename);
	DID_UNLOCK(did_lock);
	return (ret);
}

/*
 * Given a nodename string, return the nodeid_t value it maps to.
 * If no mapping is found, return -1.
 * If the internal implementation does not exist, return -2.
 */
int
did_getnodeid(char *nodename, nodeid_t *nodeid)
{
	int ret;
	DID_LOCK(did_lock);
	if (didinternal_getnodeid == NULL) {
		DID_UNLOCK(did_lock);
		return (-2);
	}
	ret = didinternal_getnodeid(nodename, nodeid);
	DID_UNLOCK(did_lock);
	return (ret);
}

/*
 * did_getinstlist()
 * This function loads the existng DID instance list from the
 * appropriate table. It expects to be passed a list of DID device
 * types. The fmtfile and mode arguments are not used currently.
 * The function returns 0 on success, -1 on failure, and sets the
 * did_geterrorstr() appropriately.
 */
int
did_getinstlist(did_device_list_t **ptr, int mode,
	did_device_type_t *typelist, char *fmtfile)
{
	int ret;
	DID_LOCK(did_lock);
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	if (didinternal_getinstlist == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: Cannot initialize library "
			"interface.\n"), "did_getinstlist");
		DID_UNLOCK(did_lock);
		return (-1);
	}
	ret = didinternal_getinstlist(ptr, mode, typelist, fmtfile);
	DID_UNLOCK(did_lock);
	return (ret);
}

/*
 * did_getrepllist()
 * This function loads the existng replicated device list from the
 * appropriate table.
 * The function returns 0 on success, 1 if there are no replicated devices,
 * -1 on failure, and sets the did_geterrorstr() appropriately.
 */
int
did_getrepllist(did_repl_list_t **ptr)
{
	int ret;
	DID_LOCK(did_lock);
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	if (didinternal_getrepllist == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: Cannot initialize library "
			"interface.\n"), "did_getrepllist");
		DID_UNLOCK(did_lock);
		return (-1);
	}
	ret = didinternal_getrepllist(ptr);
	DID_UNLOCK(did_lock);
	return (ret);
}

/*
 * did_getinst()
 * This function loads one DID instance from CCR table.
 * It expects to be passed a list of DID device types.
 * The function returns 0 on success, -1 on failure, and sets the
 * did_geterrorstr() appropriately.
 */
int
did_getinst(did_device_list_t **ptr, int inst)
{
	int ret;
	DID_LOCK(did_lock);
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	if (didinternal_getinst == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: Cannot initialize library "
			"interface.\n"), "did_getinstlist");
		DID_UNLOCK(did_lock);
		return (-1);
	}
	ret = didinternal_getinst(ptr, inst);
	DID_UNLOCK(did_lock);
	return (ret);
}

int
did_freeinstlist(did_device_list_t *ptr)
{
	int ret;
	DID_LOCK(did_lock);
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	if (didinternal_freeinstlist == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: Cannot initialize library "
			"interface.\n"), "did_freeinstlist");
		DID_UNLOCK(did_lock);
		return (-1);
	}
	ret = didinternal_freeinstlist(ptr);
	DID_UNLOCK(did_lock);
	return (ret);
}

/*
 * did_saveinstlist()
 * This function saves the list of instances it is passed in DID's
 * configuration table. It has no effect in compatability mode,
 * only works if the current configuration table is empty in
 * non-clustered mode, and writes the CCR table in clustered mode.
 * It returns 0 on success, and -1 on failure, setting
 * did_geterrorstr() appropriately.
 */
int
did_saveinstlist(did_device_list_t *ptr, char *savefile, char *node, int flag)
{
	int ret;
	DID_LOCK(did_lock);
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	if (didinternal_saveinstlist == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: Cannot initialize library "
			"interface.\n"), "did_saveinstlist");
		DID_UNLOCK(did_lock);
		return (-1);
	}
	ret = didinternal_saveinstlist(ptr, savefile, node, flag);
	DID_UNLOCK(did_lock);
	return (ret);
}

/*
 * did_saveinst()
 * This function saves one instance it is passed in DID's
 * configuration table. It has no effect in compatability mode,
 * and only writes the CCR table in clustered mode.
 * It returns 0 on success, and -1 on failure, setting
 * did_geterrorstr() appropriately.
 */
int
did_saveinst(did_device_list_t *ptr, int create)
{
	int ret;
	DID_LOCK(did_lock);
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	if (didinternal_saveinst == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: Cannot initialize library "
			"interface.\n"), "did_saveinst");
		DID_UNLOCK(did_lock);
		return (-1);
	}
	ret = didinternal_saveinst(ptr, create);
	DID_UNLOCK(did_lock);
	return (ret);
}

int
did_read_repl_entry(int key, char **data)
{
	int ret;
	DID_LOCK(did_lock);
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	if (didinternal_read_repl_entry == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: Cannot initialize library "
			"interface.\n"), "did_read_repl_entry");
		DID_UNLOCK(did_lock);
		return (-1);
	}
	ret = didinternal_read_repl_entry(key, data);
	DID_UNLOCK(did_lock);
	return (ret);
}

int
did_rm_repl_entry(int key)
{
	int ret;
	DID_LOCK(did_lock);
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	if (didinternal_rm_repl_entry == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: Cannot initialize library "
			"interface.\n"), "did_rm_repl_entry");
		DID_UNLOCK(did_lock);
		return (-1);
	}
	ret = didinternal_rm_repl_entry(key);
	DID_UNLOCK(did_lock);
	return (ret);
}

int
did_add_repl_entry(did_repl_list_t *repldata)
{
	int ret;
	DID_LOCK(did_lock);
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	if (didinternal_add_repl_entry == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: Cannot initialize library "
			"interface.\n"), "did_add_repl_entry");
		DID_UNLOCK(did_lock);
		return (-1);
	}
	ret = didinternal_add_repl_entry(repldata);
	DID_UNLOCK(did_lock);
	return (ret);
}

/*
 * did_allocinstance()
 * This function allocates a new DID instance. It has no effect in
 * non-clustered or compatability modes. In clustered mode it returns
 * the modified instlist, and also ensures that the new instance is
 * written to the appropriate config table. It returns 0 on success,
 * -1 on failure, and sets did_geterrorstr() appropriately.
 */
int
did_allocinstance(did_subpath_t *subptr, did_device_list_t **instlist,
	char *node)
{
	int ret;
	DID_LOCK(did_lock);
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	if (didinternal_allocinstance == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: Cannot initialize library "
			"interface.\n"), "did_allocinstance");
		DID_UNLOCK(did_lock);
		return (-1);
	}
	ret = didinternal_allocinstance(subptr, instlist, node);
	DID_UNLOCK(did_lock);
	return (ret);
}

/*
 * did_allocinstances()
 * This function allocates new DID instances. It has no effect in
 * non-clustered or compatability modes. In clustered mode it returns
 * the modified instlist, and also ensures that the new instances are
 * written to the appropriate config table. It returns 0 on success,
 * -1 on failure, and sets did_geterrorstr() appropriately.
 */
int
did_allocinstances(did_device_list_t *alloclist, did_device_list_t **instlist,
	char *node)
{
	int ret;
	DID_LOCK(did_lock);
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	if (didinternal_allocinstances == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: Cannot initialize library "
			"interface.\n"), "did_allocinstance");
		DID_UNLOCK(did_lock);
		return (-1);
	}
	ret = didinternal_allocinstances(alloclist, instlist, node);
	DID_UNLOCK(did_lock);
	return (ret);
}

/*
 * did_gettypelist()
 * This function reads in the list of DID types from the configuration
 * table. The data is dynamic in clustered and non-clustered mode, but
 * static, and hard coded into this library in compatability mode. It
 * returns 0 on success, -1 on failure, and sets did_geterrorstr()
 * appropriately.
 */
int
did_gettypelist(did_device_type_t **list)
{
	int ret;
	DID_LOCK(did_lock);
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	if (didinternal_gettypelist == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: Cannot initialize library "
			"interface.\n"), "did_gettypelist");
		DID_UNLOCK(did_lock);
		return (-1);
	}
	ret = didinternal_gettypelist(list);
	DID_UNLOCK(did_lock);
	return (ret);
}

int
did_savetypelist(did_device_type_t *list)
{
	int ret;
	DID_LOCK(did_lock);
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	if (didinternal_savetypelist == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: Cannot initialize library "
			"interface.\n"), "did_savetypelist");
		DID_UNLOCK(did_lock);
		return (-1);
	}
	ret = didinternal_savetypelist(list);
	DID_UNLOCK(did_lock);
	return (ret);
}

int
did_mapdidtodev(char *did_dev, did_subpath_t *dev_path)
{
	int ret;
	DID_LOCK(did_lock);
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	if (didinternal_mapdidtodev == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: Cannot initialize library "
			"interface.\n"), "did_mapdidtodev");
		DID_UNLOCK(did_lock);
		return (-1);
	}
	ret = didinternal_mapdidtodev(did_dev, dev_path);
	DID_UNLOCK(did_lock);
	return (ret);
}

int
did_mapdevtodid(char *did_dev, did_subpath_t *dev_path)
{
	int ret;
	DID_LOCK(did_lock);
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	if (didinternal_mapdevtodid == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: Cannot initialize library "
			"interface.\n"), "did_mapdevtodid");
		DID_UNLOCK(did_lock);
		return (-1);
	}
	ret = didinternal_mapdevtodid(did_dev, dev_path);
	DID_UNLOCK(did_lock);
	return (ret);
}

/*
 * This function executes the specified program on the local node.
 * When booted in clustered mode, it used clexecd to run the program,
 * otherwise it uses system().  Didinternal_exec_program() returns -1
 * if it encounters an error, otherwise it returns the return value for
 * either system() or clconf_do_execution().
 */
int
did_exec_program_local(char *pgm)
{
	int ret;
	DID_LOCK(did_lock);
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	if (didinternal_exec_program_local == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: Cannot initialize library "
			"interface.\n"), "did_exec_program_local");
		DID_UNLOCK(did_lock);
		return (-1);
	}
	ret = didinternal_exec_program_local(pgm);
	DID_UNLOCK(did_lock);
	return (ret);
}

/*
 * did_syncgdevs()
 * This function only does useful work in clustered mode (with the CCR
 * version of the library). In all other cases, it returns -1.
 *
 * When in clustered mode, this function calls the scgdevs programs on
 * all other nodes of the cluster. It returns 0 on success, and -1 on
 * failure, and sets did_geterrorstr().
 */
int
did_syncgdevs()
{
	int ret;
	DID_LOCK(did_lock);
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	if (didinternal_syncgdevs == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: Cannot initialize library "
			"interface.\n"), "did_syngdevs");
		DID_UNLOCK(did_lock);
		return (-1);
	}
	ret = didinternal_syncgdevs();
	DID_UNLOCK(did_lock);
	return (ret);
}

/*
 * did_getglobalfencingstatus()
 */
int
did_getglobalfencingstatus(unsigned short *status)
{
	int ret;
	DID_LOCK(did_lock);
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	if (didinternal_getglobalfencingstatus == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: Cannot initialize library "
		    "interface.\n"), "did_getglobalfencingstatus");
		DID_UNLOCK(did_lock);
		return (-1);
	}
	ret = didinternal_getglobalfencingstatus(status);
	DID_UNLOCK(did_lock);
	return (ret);
}

int
read_global_fencing_status_from_ccr(unsigned short *status)
{
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/* Return if we can't init the library */
	if (did_initlibrary(DID_LIB_INTERFACE_VERSION, NULL, NULL)) {
		/* Errno is set. Don't unset it. */
		return (-1);
	}

	if (did_getglobalfencingstatus(status)) {
		return (-1);
	} else {
		return (0);
	}
}

/*
 * did_syncrepl()
 * This function only does useful work in clustered mode (with the CCR
 * version of the library). In all other cases, it returns -1.
 *
 * When in clustered mode, this function calls the scdidadm -T on
 * all other nodes of the cluster. It returns 0 on success, and -1 on
 * failure, and sets did_geterrorstr().
 */
int
did_syncrepl(char *exec_node, char *command)
{
	int ret;
	DID_LOCK(did_lock);
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	if (didinternal_syncrepl == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: Cannot initialize library "
			"interface.\n"), "did_syncrepl");
		DID_UNLOCK(did_lock);
		return (-1);
	}
	ret = didinternal_syncrepl(exec_node, command);
	DID_UNLOCK(did_lock);
	return (ret);
}

did_device_list_t *
list_all_devices(char *dtype)
{

	did_device_list_t 	*ptr, *cur, *prev, *ret, *tofree;
	did_device_type_t	*typelist;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/* Return if we can't init the library */
	if (did_initlibrary(DID_LIB_INTERFACE_VERSION, NULL, NULL)) {
		/* Errno is set. Don't unset it. */
		return (NULL);
	}

	if (did_gettypelist(&typelist)) {
		/* Errno is set. Don't unset it. */
		return (NULL);
	}
	if (did_getinstlist(&ptr, NULL, typelist, NULL)) {
		/* Errno is set. Don't unset it. */
		/* free() does not change errno */
		return (NULL);
	}

	if (dtype == NULL) {
		/* Return the whole list if a device type isn't specified */
		ret = ptr;
	} else {
		/*
		 * If the device type is specified, walk the
		 * entire list and return any did entries with
		 * that type.
		 */
		did_device_type_t *ptyp =
		    did_device_type_extract(typelist, dtype);
		if (ptyp == NULL) {
			did_device_list_free(ptr);
			return (NULL);
		}
		ret = prev = NULL;
		cur = ptr;
		while (cur != NULL) {
			tofree = NULL;
			if (cur->device_type == ptyp) {
				if (ret == NULL) {
					/* ret is the head of list */
					ret = cur;
				}
				if (prev != NULL) {
					prev->next = cur;
				}
				prev = cur;
			} else {
				tofree = cur;
			}
			cur = cur->next;
			if (tofree != NULL) {
				did_device_list_entry_free(tofree);
			}
		}
		if (prev != NULL) {
			prev->next = NULL;
		}
	}
	return (ret);
}

/*
 * find_did_device()
 * Returns a pointer to a did_device_list_t that refers to the
 * did device specified. Note that since did names give a one
 * to one mapping to did_device_list_t structures, this will
 * not be a linked list.
 */
static int
find_did_device(char *didname, did_device_list_t **ptr, did_device_list_t **out)
{
	did_device_list_t *cur;
	int didinst;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (*didname == 'd') {
		/* Determine the instance number for disks */
		didinst = atoi(didname + 1);
	} else if (*didname >= '0' && *didname <= '9') {
		didinst = (DID_MAX_INSTANCE - atoi(didname) + 1);
	} else {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Invalid input to find_did_device: "
			"%s\n"), didname);
		return (-1);
	}

	/* Return if the instance number doesn't make sense */
	if ((didinst < 1) || (didinst > DID_MAX_INSTANCE)) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: Instance number %d "
			"invalid.\n"), "find_did_device", didinst);
		return (-1);
	}
	for (cur = *ptr; cur; cur = cur->next) {
		if (cur->instance == didinst) {
			*out = cur;
			/* remove 'cur' from '*ptr' list */
			did_device_list_remove(ptr, cur);
			(*out)->next = NULL;
			return (0);
		}
	}
	*out = NULL;
	return (0);
}


/*
 * find_underlying_device()
 * Returns a linked list of did_device_list_t pointers
 * which describe the non-did device name specified.
 * If remote is non-zero, we check for this device on all
 * nodes of the cluster. If it is zero, we only check on
 * the node where the routine was called. Note that due to
 * Solaris namespace collisions, calling this routine with
 * remote set can return multiple DID devices.
 */
static int
find_underlying_device(char *devname, did_device_list_t **ptr, int remote,
	did_device_list_t **out)
{
	did_device_list_t *cur, *next, *prev;
	did_subpath_t *subptr;
	unsigned int instlen = strlen(devname);
	unsigned int pathlen, hnlen = 0;
	char hostname[MAXHOSTNAMELEN + 2];

	prev = *out = NULL;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/*
	 * Need to determine our local hostname if we're not looking on
	 * all nodes for this device.
	 */
	hostname[0] = '\0';
	if (!remote) {
		(void) gethostname(hostname, sizeof (hostname));
		(void) strcat(hostname, ":");
		hnlen = strlen(hostname);
	}

	cur = *ptr;
	while (cur != NULL) {
		next = NULL;
		for (subptr = cur->subpath_listp; subptr;
		    subptr = subptr->next) {
			if ((pathlen = strlen(subptr->device_path)) < instlen) {
				continue;
			}
			if (strcmp(subptr->device_path+pathlen-instlen,
			    devname)) {
				continue;
			}
			if (!remote) {
				/*
				 * Only return paths we're connected to
				 * if remote isn't set.
				 */
				if (strncmp(subptr->device_path, hostname,
				    hnlen)) {
					continue;
				}
			}
			/* Create the '*out' list */
			if (prev != NULL) {
				prev->next = cur;
			} else {
				*out = cur;
			}
			prev = cur;
			next = cur->next;
			did_device_list_remove(ptr, cur);
			prev->next = NULL;
			break;
		}
		if (next == NULL) {
			cur = cur->next;
		} else {
			cur = next;
		}
	}

	if (*out == NULL) {
		return (-1);
	}
	return (0);
}

did_device_list_t *
map_to_did_device(char *devname, int remote)
{
	did_device_list_t	*ptr;
	did_device_type_t	*typelist;
	did_device_list_t	*ret = NULL;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (devname == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Invalid input to %s\n."),
			"map_to_did_device");
		return (NULL);
	}

	if (did_initlibrary(DID_LIB_INTERFACE_VERSION, NULL, NULL)) {
		/* Errno is set. Don't unset it. */
		return (NULL);
	}

	if (did_gettypelist(&typelist)) {
		/* Errno is set. Don't unset it. */
		return (NULL);
	}

	if (did_getinstlist(&ptr, NULL, typelist, NULL)) {
		/* Errno is set. Don't unset it. */
		/* free() doesn't set errno */
		return (NULL);
	}

	if (*devname == 'd') {
		if (find_did_device(devname, &ptr, &ret)) {
			/* Errno is set. Don't unset it. */
			/* free() doesn't set errno */
			did_device_list_free(ptr);
			return (NULL);
		}
		did_device_list_free(ptr);
		if (ret != NULL) {
			if (ret->device_type != NULL) {
				(void) did_device_type_extract(typelist,
				    (ret->device_type)->type_name);
			}
		}
	} else {
		if (find_underlying_device(devname, &ptr, remote, &ret)) {
			/* Errno is set. Don't unset it. */
			/* free() doesn't set errno */
			did_device_list_free(ptr);
			return (NULL);
		}
		did_device_list_free(ptr);
	}

	return (ret);
}

/*
 * Check to see if the specified device is marked as replicated or not
 *
 * Input:
 *   device - DID path name of a device: /dev/did/rdsk/dx or /dev/did/rdsk/dxs2
 *   data - if non-NULL and the device is replicated, the replication info is
 *        placed here
 *
 * Returns:
 *   DID_REPL_TRUE - the specified device is a replicated device
 *   DID_REPL_FALSE - the specified device is not a replciated device
 *   DID_REPL_BAD_DEV - the specified device is not a valid DID device
 *   DID_REPL_ERROR - an internal error occured
 */
did_repl_t
check_for_repl_device(char *device, did_repl_list_t *data)
{
	int rdkey, retval;
	char *key, *indata;

	if ((key = strrchr(device, '/')) == NULL)
		return (DID_REPL_BAD_DEV);
	key++;
	if (key[0] != 'd')
		return (DID_REPL_BAD_DEV);
	key++;
	if (key[0] == '\0')
		return (DID_REPL_BAD_DEV);

	rdkey = atoi(key);
	if ((rdkey < 1) || (rdkey > DID_MAX_INSTANCE))
		return (DID_REPL_BAD_DEV);

	retval = did_read_repl_entry(rdkey, &indata);
	if (retval == 0) {
		if (did_repl_str_to_instance(key, indata, data) != 0)
			return (DID_REPL_ERROR);
		else
			return (DID_REPL_TRUE);
	} else if (retval == 1)
		return (DID_REPL_FALSE);
	else
		return (DID_REPL_ERROR);
}

did_device_list_t *
map_from_did_device(char *didname)
{
	did_device_list_t	*ptr;
	did_device_type_t	*typelist;
	did_device_list_t	*ret = NULL;

	if (didname == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Invalid input to %s\n."),
			"map_from_did_device");
		return (NULL);
	}

	if ((did_initlibrary(DID_LIB_INTERFACE_VERSION, NULL, NULL))
	    != 0) {
		/* did_geterrorstr() is set. Don't unset it. */
		return (NULL);
	}
	if ((did_gettypelist(&typelist)) != 0) {
		/* did_geterrorstr() is set. Don't unset it. */
		return (NULL);
	}
	if ((did_getinstlist(&ptr, NULL, typelist, NULL)) != 0) {
		/* did_geterrorstr() is set. Don't unset it. */
		/* free() doesn't set errno */
		return (NULL);
	}

	if (find_did_device(didname, &ptr, &ret)) {
		/* Errno is set. Don't unset it. */
		/* free() doesn't set errno */
		did_device_list_free(ptr);
		return (NULL);
	}
	did_device_list_free(ptr);
	if (ret != NULL) {
		if (ret->device_type != NULL) {
			(void) did_device_type_extract(typelist,
			    (ret->device_type)->type_name);
		}
	}
	return (ret);
}

int
did_get_instance(did_device_list_t *ptr)
{
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (ptr == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Invalid input to %s.\n"),
			"did_get_instance");
		return (-1);
	}

	return (ptr->instance);
}

short
did_get_devid_type(did_device_list_t *ptr)
{
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (ptr == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Invalid input to %s.\n"),
			"did_get_instance");
		return (-1);
	}

	return ((short)ptr->type);
}

ddi_devid_t
did_get_devid(did_device_list_t *ptr)
{
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (ptr == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Invalid input to %s.\n"),
			"did_get_devid");
		return ((ddi_devid_t)0);
	}

	return (ptr->target_device_id);
}

char *
did_get_path(did_device_list_t *ptr)
{
	did_subpath_t	*subptr;
	size_t		string_len = 2 * MAXPATHLEN * sizeof (char);
	char		*path = (char *)malloc(string_len);

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/*
	 * XXX - We should really enhance this function to
	 * do extension of path's size on the fly.
	 */

	if ((ptr == NULL) || (path == NULL) || (ptr->subpath_listp == NULL)) {
		free(path);
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Invalid input to %s.\n"),
			"did_get_path");
		return (NULL);
	}

	path[0] = '\0';
	for (subptr = ptr->subpath_listp; subptr; subptr = subptr->next) {
		if ((strlen(path) + strlen(subptr->device_path) + 1) >=
		    string_len) {
			free(path);
			(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN, "Out of memory.\n"));
			return (NULL);
		}
		if (sprintf(path, "%s%s ", path, subptr->device_path) <= 0) {
			free(path);
			(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN, "Out of memory.\n"));
			return (NULL);
		}
	}

	return (path);
}

int
did_get_num_paths(did_device_list_t *ptr)
{
	did_subpath_t *subptr;
	int retval = 0;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if ((ptr == NULL) || (ptr->subpath_listp == NULL)) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Invalid input to %s.\n"),
			"did_get_path");
		return (-1);
	}

	for (subptr = ptr->subpath_listp; subptr; subptr = subptr->next)
		retval++;

	return (retval);
}


char *
did_get_did_path(did_device_list_t *ptr)
{
	char *name = malloc(MAXPATHLEN);
	if (name == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Out of memory.\n"));
		return (NULL);
	}

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (ptr == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Invalid input to %s.\n"),
			"did_get_did_path");
		return (NULL);
	}

	if (strcmp(ptr->device_type->type_name, "disk") == 0)
		(void) sprintf(name, "rdsk/d%d", ptr->instance);
	else if (strcmp(ptr->device_type->type_name, "tape") == 0)
		(void) sprintf(name, "rmt/%d",
		    abs(ptr->device_type->start_default_nodes -
		    ptr->instance) + 1);
	return (name);
}

int
did_get_minor_count(did_device_list_t *ptr)
{
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (ptr == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Invalid input to %s.\n"),
			"did_get_minor_count");
		return (-1);
	}

	return (ptr->minor_count);
}

/*
 * did_get_alias()
 * This function is unimplemented until the point when we
 * implement user-defined naming of did devices.
 */
char *
did_get_alias(did_device_list_t *ptr)
{
	if (ptr == NULL)
		return (NULL);

	return (NULL);

}

int
did_delete_devices_by_node(char *nodename)
{
	did_device_list_t	*instlist, *instp, *prev_inst, *del_inst;
	did_device_type_t	*typelist;
	did_subpath_t		*subptr, *prev_subptr, *del_subptr;
	uint_t			nodelen;
	int			ret;

	if (nodename == NULL) {
		return (-1);
	}

	nodelen = strlen(nodename);

	if (did_initlibrary(DID_LIB_INTERFACE_VERSION, NULL, NULL)) {
		return (-1);
	}

	if (did_gettypelist(&typelist)) {
		return (-1);
	}

	if (did_getinstlist(&instlist, NULL, typelist, NULL)) {
		return (-1);
	}

	instp = instlist;
	prev_inst = NULL;

	while (instp) {
		prev_subptr = NULL;
		subptr = instp->subpath_listp;

		while (subptr) {
			if (strncmp(subptr->device_path, nodename, nodelen)) {
				prev_subptr = subptr;
				subptr = subptr->next;
				continue;
			}

			/*
			 * Only delete subpaths which are connected on
			 * the user-specified node.
			 */
			del_subptr = subptr;
			if (prev_subptr)
				prev_subptr->next = subptr->next;
			if (instp->subpath_listp == del_subptr)
				if (prev_subptr)
					instp->subpath_listp = prev_subptr;
				else
					instp->subpath_listp = subptr->next;
			subptr = subptr->next;
			free(del_subptr);
		}

		/*
		 * We should make sure this instance still has
		 * subpaths. If no more subpaths exist, remove
		 * the instance entirely.
		 */

		if (instp->subpath_listp == NULL) {
			/* Remove the instance. There are no more paths. */
			del_inst = instp;
			if (prev_inst)
				prev_inst->next = instp->next;
			/*
			 * Adjust the instlist if the first instance is to
			 * be removed.
			 */
			if (instlist == instp)
				instlist = instp->next;
			/* Remove the instance */
			instp = instp->next;
			free(del_inst);
		} else {
			prev_inst = instp; instp = instp->next;
		}
	}
	ret = did_saveinstlist(instlist, NULL, nodename, REMOVABLE);
	return (ret);
}

int
did_holdccrlock()
{
	int ret;
	DID_LOCK(did_lock);
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	if (didinternal_holdccrlock == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: Cannot initialize library "
			"interface.\n"), "did_holdccrlock");
		DID_UNLOCK(did_lock);
		return (-1);
	}
	ret = didinternal_holdccrlock();
	DID_UNLOCK(did_lock);
	return (ret);
}

int
did_releaseccrlock()
{
	int ret;
	DID_LOCK(did_lock);
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	if (didinternal_releaseccrlock == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: Cannot initialize library "
			"interface.\n"), "did_releaseccrlock");
		DID_UNLOCK(did_lock);
		return (-1);
	}
	ret = didinternal_releaseccrlock(1);
	DID_UNLOCK(did_lock);
	return (ret);
}

int
did_abortccrlock()
{
	int ret;
	DID_LOCK(did_lock);
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	if (didinternal_releaseccrlock == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: Cannot initialize library "
			"interface.\n"), "did_abortccrlock");
		DID_UNLOCK(did_lock);
		return (-1);
	}
	ret = didinternal_releaseccrlock(0);
	DID_UNLOCK(did_lock);
	return (ret);
}

int
did_populate_list_cache(did_device_list_t *ptr)
{
	int ret;
	DID_LOCK(did_lock);
	if (didinternal_populate_list_cache == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: Cannot initialize library "
			"interface.\n"), "did_abortccrlock");
		DID_UNLOCK(did_lock);
		return (-1);
	}
	ret = didinternal_populate_list_cache(ptr);
	DID_UNLOCK(did_lock);
	return (ret);
}


/*
 *  Code for emulating Solaris 8 and Solaris 9 sd / ssd device id algorithms.
 * The main purpose of this code is to allow safe upgrade of clusters between
 * Solaris 8 and Solaris 9, which causes the device ids to change.  The only
 * entry point for this code is through get_solaris_devid().  the following
 * functions are used internally to obtain the requested device id and should
 * not be used for other purposes:
 *     int  inq_page_supported()
 *     void raw_inq_data()
 *     void inq_page_83()
 *     void inq_page_80()
 *     int  inq()
 *     int  is_cdrom()
 *     void s8_sd_evids()
 *     void s8_ssd_devids()
 *     void s9_devids()
 */

/* determine if device supports scsi page 0x80 or 0x83 for getting device id */
int
inq_page_supported(struct vpd_inq_data *vpd_inq)
{
	int counter;
	unsigned char inquirybuffer[MAXNAMELEN];
	struct uscsi_cmd ms_cmd;

	unsigned char ms_cdb[] = {
	    0x12,	/* INQUIRY */
	    0x01,	/* EVPD Bit */
	    0x00,	/* VPD Page 0x00, supported page list */
	    0x00,	/* reserved */
	    0xff,	/* alloc length */
	    0x00	/* control */
	};

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	(void) memset((char *)&ms_cmd, 0, sizeof (ms_cmd));

	ms_cmd.uscsi_flags = USCSI_READ;
	ms_cmd.uscsi_timeout = 60;
	ms_cmd.uscsi_cdb = (caddr_t)&ms_cdb[0];
	ms_cmd.uscsi_cdblen = sizeof (ms_cdb);
	ms_cmd.uscsi_bufaddr = (char *)inquirybuffer;
	ms_cmd.uscsi_buflen = sizeof (inquirybuffer);

	if (ioctl(vpd_inq->fd, USCSICMD, &ms_cmd) == -1) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: ioctl failure"),
		    "inq_page_supported");
		return (-1);
	}

	/* Loop to find one of the 2 pages we need */
	counter = 4;  /* Supported pages start at byte 4, with 0x00 */

	/*
	 * Pages are returned in ascending order, and 0x83 is what we
	 * are hoping for.
	 */
	while ((inquirybuffer[counter] <= 0x83) &&
	    (counter <= (inquirybuffer[VPD_PAGE_LENGTH] + VPD_HEAD_OFFSET))) {
		/*
		 * Add 3 because page_list[3] is the number of
		 * pages minus 3
		 */
		switch (inquirybuffer[counter]) {
		case 0x80:
			vpd_inq->supported_page_mask |= VPD_SERIAL_NUM_PAGE;
			break;
		case 0x83:
			vpd_inq->supported_page_mask |= VPD_DEVICE_ID_PAGE;
			break;
		default:
			break;
		}
		counter++;
	}
	return (0);
}

/* issue scsi inquiry */
void
raw_inq_data(struct vpd_inq_data *vpd_inq)
{
	unsigned char inquirybuffer[MAXNAMELEN];
	struct scsi_inquiry *inq_ptr;
	struct uscsi_cmd ms_cmd;

	unsigned char ms_cdb[] = {
	    0x12,	/* INQUIRY */
	    0x00,	/* EVPD Bit */
	    0x00,	/* VPD Page 0x80, unit serial number page */
	    0x00,	/* reserved */
	    0xff,	/* alloc length */
	    0x00	/* control */
	};

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	(void) memset((char *)&ms_cmd, 0, sizeof (ms_cmd));

	ms_cmd.uscsi_flags = USCSI_READ;
	ms_cmd.uscsi_timeout = 60;
	ms_cmd.uscsi_cdb = (caddr_t)&ms_cdb[0];
	ms_cmd.uscsi_cdblen = sizeof (ms_cdb);
	ms_cmd.uscsi_bufaddr = (char *)inquirybuffer;
	ms_cmd.uscsi_buflen = sizeof (inquirybuffer);

	if (ioctl(vpd_inq->fd, USCSICMD, &ms_cmd) == -1) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: ioctl failure"),
		    "raw_inq_data");
		return;
	}

	inq_ptr = (struct scsi_inquiry *)inquirybuffer;
	(void) memcpy(vpd_inq->inq_data->inq_vid, inq_ptr->inq_vid,
	    sizeof (inq_ptr->inq_vid));
	(void) memcpy(vpd_inq->inq_data->inq_pid, inq_ptr->inq_pid,
	    sizeof (inq_ptr->inq_pid));
	(void) memcpy(vpd_inq->inq_data->inq_revision, inq_ptr->inq_revision,
	    sizeof (inq_ptr->inq_revision));
	(void) memcpy(vpd_inq->inq_data->inq_serial, inq_ptr->inq_serial,
	    sizeof (inq_ptr->inq_serial));
}

/* see if page 0x83 has a device id we can use */
void
inq_page_83(struct vpd_inq_data *vpd_inq)
{
	struct uscsi_cmd ms_cmd;
	unsigned char dev_id_page[MAXNAMELEN];
	int descriptor_bytes_left;
	int offset;
	int offset_id_type[4];
	int found = 0;
	unsigned int len;
	unsigned int i;

	unsigned char ms_cdb[] = {
	    0x12,	/* INQUIRY */
	    0x01,	/* EVPD Bit */
	    0x83,	/* VPD Page 0x83, device id number page */
	    0x00,	/* reserved */
	    0xff,	/* alloc length */
	    0x00	/* control */
	};

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	(void) memset((char *)&ms_cmd, 0, sizeof (ms_cmd));

	/*lint -e545 */
	(void) memset((char *)&dev_id_page, 0xBB, sizeof (dev_id_page));
	/*lint +e545 */

	ms_cmd.uscsi_flags = USCSI_READ;
	ms_cmd.uscsi_timeout = 60;
	ms_cmd.uscsi_cdb = (caddr_t)&ms_cdb[0];
	ms_cmd.uscsi_cdblen = sizeof (ms_cdb);
	ms_cmd.uscsi_bufaddr = (char *)dev_id_page;
	ms_cmd.uscsi_buflen = sizeof (dev_id_page);

	if (ioctl(vpd_inq->fd, USCSICMD, &ms_cmd) == -1) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: ioctl failure"), "inq_page_83");
		return;
	}

	/*
	 * Attempt to validate the page data.  Once validated, we'll walk
	 * the descriptors, looking for certain identifier types that will
	 * mark this device with a unique id/wwn.  Note the comment below
	 * for what we really want to receive.
	 */

	/*
	 * Set the offset to the beginning byte of each
	 * descriptor.  We'll index everything from there.
	 */
	offset = 4;
	descriptor_bytes_left = dev_id_page[3];

	/* Zero out our offset array */
	bzero(offset_id_type, sizeof (offset_id_type));

	/*
	 * According to the scsi spec 8.4.3 SPC-2, there could be several
	 * descriptors associated with each lun.  Some we care about and some
	 * we don't.  This loop is set up to iterate through the descriptors.
	 * We want the 0x03 case which represents an FC-PH, FC-PH3 or FC-FS
	 * Name_Identifier.  The spec mentions nothing about ordering, so we
	 * don't assume any.
	 */
	while ((descriptor_bytes_left > 0) && (offset_id_type[3] == 0)) {
		/*
		 * Inspect the Identification descriptor list. Store the
		 * offsets in the devid page separately for 0x03, 0x01 and
		 * 0x02.  Identifiers 0x00 and 0x04 are not useful as they
		 * don't represent unique identifiers for a lun.  We also
		 * check the association by masking with 0x3f because we want
		 * an association of 0x0 - indicating the identifier field is
		 * associated with the addressed physical or logical device
		 * and not the port.
		 */
		switch ((dev_id_page[offset + 1] & 0x3f)) {
		case 0x01:
			offset_id_type[1] = offset;
			found = 1;
			break;
		case 0x02:
			offset_id_type[2] = offset;
			found = 1;
			break;
		case 0x03:
			offset_id_type[3] = offset;
			found = 1;
			break;
		default:
			(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN, "%s: Devid page "
			    "undesired id type\n"), "inq_page_83");
			break;
		}

		/*
		 * Calculate the descriptor bytes left and move to
		 * the beginning byte of the next id descriptor.
		 */
		descriptor_bytes_left -= (int)(dev_id_page[offset + 3] + 4);
		offset += (4 + (int)dev_id_page[offset + 3]);
	}

	/*
	 * First we ensure we found a valid descriptor, if none
	 * were found we need to clear VPD_DEVICE_ID_PAGE out of
	 * supported_page_mask and return.
	 */
	if (!found) {
		vpd_inq->supported_page_mask ^= VPD_DEVICE_ID_PAGE;
		return;
	}
	/*
	 * We can't depend on an order from a device by identifier type, but
	 * once we have them, we'll walk them in the same order to prevent a
	 * firmware upgrade from breaking our algorithm.  Start with the one
	 * we want the most: id_offset_type[3]. As a sanity check, we set the
	 * offset back to 0
	 */
	offset = 0;
	for (i = 3; i > 0; i--) {
		if (offset_id_type[i] > 0) {
			offset = offset_id_type[i];
			break;
		}
	}
	/*
	 * We have a valid Device ID page, set the length of the
	 * identifier and copy the value into the wwn.
	 */
	if (offset > 0) {
		len = dev_id_page[offset + 3];
		if (len > MAXNAMELEN) {
			vpd_inq->vpd_wwn_len = (unsigned int)-1;
		} else {
			vpd_inq->vpd_wwn_len = len;
			bcopy(&dev_id_page[offset + 4], vpd_inq->vpd_wwn, len);
		}
	} else {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: Devid page no desired id "
		    "type\n"), "inq_page_83");
		vpd_inq->supported_page_mask ^= VPD_DEVICE_ID_PAGE;
	}
}

/* see if page 0x80 has a device id we can use */
void
inq_page_80(struct vpd_inq_data *vpd_inq)
{
	unsigned char inquirybuffer[MAXNAMELEN];
	struct uscsi_cmd ms_cmd;

	unsigned char ms_cdb[] = {
	    0x12,	/* INQUIRY */
	    0x01,	/* EVPD Bit */
	    0x80,	/* VPD Page 0x80, unit serial number page */
	    0x00,	/* reserved */
	    0xff,	/* alloc length */
	    0x00	/* control */
	};

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	(void) memset((char *)&ms_cmd, 0, sizeof (ms_cmd));

	ms_cmd.uscsi_flags = USCSI_READ;
	ms_cmd.uscsi_timeout = 60;
	ms_cmd.uscsi_cdb = (caddr_t)&ms_cdb[0];
	ms_cmd.uscsi_cdblen = sizeof (ms_cdb);
	ms_cmd.uscsi_bufaddr = (char *)inquirybuffer;
	ms_cmd.uscsi_buflen = sizeof (inquirybuffer);

	if (ioctl(vpd_inq->fd, USCSICMD, &ms_cmd) == -1) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: ioctl failure"), "inq_page_80");
		return;
	}

	if (inquirybuffer[3] > MAXNAMELEN) {
		vpd_inq->vpd_serial_num_len = (unsigned int)-1;
	} else {
		vpd_inq->vpd_serial_num_len = inquirybuffer[3];
		bcopy(&inquirybuffer[4], vpd_inq->vpd_serial_num,
		    vpd_inq->vpd_serial_num_len);
	}
}

/* issue sscsi inquiry and see which page (0x 80 or 0x83) we can use */
int
inq(struct vpd_inq_data *vpd_inq)
{
	/*
	 * Return error if we can not find out which pages are supported.
	 */
	if (inq_page_supported(vpd_inq) == -1)
		return (-1);

	raw_inq_data(vpd_inq);

	if (vpd_inq->supported_page_mask & VPD_DEVICE_ID_PAGE) {
		inq_page_83(vpd_inq);
	}

	if (vpd_inq->supported_page_mask & VPD_SERIAL_NUM_PAGE) {
		inq_page_80(vpd_inq);
	}
	return (0);
}

/*
 * Issue a SCSI INQUIRY and check the peripheral device type. A value
 * of 0x05 indicates this is a CD-ROM device. If the INQUIRY fails just
 * punt and say this is not a CD-ROM.
*/
int
is_cdrom(struct vpd_inq_data *vpd_inq)
{
	struct uscsi_cmd cmd;
	char buf[MAXNAMELEN];
	unsigned char cdb[6] = {0x12, 0, 0, 0, 0xff, 0};

	cmd.uscsi_flags = USCSI_READ;
	cmd.uscsi_timeout = 6;
	cmd.uscsi_cdb = (caddr_t)&cdb[0];
	cmd.uscsi_cdblen = sizeof (cdb);
	cmd.uscsi_bufaddr = buf;
	cmd.uscsi_buflen = sizeof (buf);
	cmd.uscsi_rqbuf = NULL;
	cmd.uscsi_rqlen = 0;

	if (ioctl(vpd_inq->fd, USCSICMD, &cmd) < 0) {
		return (0);
	}

	if ((0x1f & buf[0]) != 5) {
		return (0);
	}

	return (1);
}

/*
 * Solaris 8 sd Algorithm:
 * =======================
 * 1) Issue an INQUIRY command for VPD page 0x80.
 *       1a) If page 0x80 is available inspect the VPD page 0x80 contents to
 *          determine if they are valid.
 *       1c) Create a device id by concatenating the contents of VPD page 0x80
 *           with the INQUIRY vid:   <vid><VPD page 0x80 serial #>
 *
 * 2) If VPD page 0x80 is not available with valid data, determine if the disk
 *    has been qualified by Sun as indicated by the string "SUN" in the contents
 *    of the INQUIRY pid contents.
 *       2a) If the disk is Sun qualified create a device id by concatenating
 *           the contents of the INQUIRY vid, and serial number:
 *           <vid><INQUIRY serial #>
 *       2b) If the disk is not Sun qualified fabricate a device id using the
 *           Solaris operating system device id fabrication routine. This OS
 *           routine uses the machine's host id and a timestamp to create a
 *           device id.
 *
 *     NOTES: DID will not attempt to fabricate a device id ala 2b) above and
 *            all vpd_inq fields will retain their originally initialized
 *            values if the conditions in 2a) above is not satisfied.
 *            vpd_inq->devid_type value at enter time is DEVID_INVALID, it is
 *	      modified in case of success only, and just before returning.
 */
void
s8_sd_devids(struct vpd_inq_data *vpd_inq, unsigned char *devid_str)
{
	unsigned int vid_len = 0;
	unsigned int ser_len = 0;

	if (vpd_inq->vpd_serial_num_len == (unsigned int)-1) {
		/* vpd_serial_num_len was too big in inquiry */
		return;
	}

	(void) memset(devid_str, 0, MAXNAMELEN);

	if (vpd_inq->supported_page_mask & VPD_SERIAL_NUM_PAGE) {
		vid_len = sizeof (vpd_inq->inq_data->inq_vid);
		ser_len = vpd_inq->vpd_serial_num_len;
		if ((vid_len + ser_len) > MAXNAMELEN) {
			/* would overflow devid_str */
			return;
		}
		bcopy(vpd_inq->inq_data->inq_vid, devid_str, vid_len);
		bcopy(vpd_inq->vpd_serial_num, &devid_str[vid_len], ser_len);
		vpd_inq->devid_len = vid_len + KSTAT_DATA_CHAR_LEN;
		vpd_inq->devid_type = DEVID_SCSI_SERIAL;
	} else if (bcmp(&vpd_inq->inq_data->inq_pid[9], "SUN", 3) == 0) {
		vid_len = sizeof (vpd_inq->inq_data->inq_vid);
		ser_len = vpd_inq->vpd_serial_num_len;
		if ((vid_len + ser_len) > MAXNAMELEN) {
			/* would overflow devid_str */
			return;
		}
		bcopy(vpd_inq->inq_data->inq_vid, devid_str, vid_len);
		bcopy(vpd_inq->inq_data->inq_serial, &devid_str[vid_len],
		    ser_len);
		vpd_inq->devid_len = vid_len + KSTAT_DATA_CHAR_LEN;
		vpd_inq->devid_type = DEVID_SCSI_SERIAL;
	}
}

/*
 * Solaris 8 ssd Algorithm:
 * ========================
 * 1) Issue an INQUIRY command for VPD page 0x83.
 *       1a) If page 0x83 is available inspect the returned identification
 *           descriptor list to determine if descriptor type 0x03 (FC-PH, FC-PH3
 *           or FC-FS Name_Identifier) is available. If descriptor type 0x03 is
 *           available it is used as the device id
 *
 * 2) If VPD page 0x83 with descriptor type 0x03 is not available, issue an
 *    INQUIRY command for VPD page 0x80.
 *       2a) If page 0x80 is available inspect the VPD page 0x80 contents to
 *           determine if they are valid.
 *       2c) Create a device id by concatenating the contents of VPD page 0x80
 *           with the INQUIRY vid:   <vid><VPD page 0x80 serial #>
 *
 * 3) If VPD pages 0x83 and 0x80 are not available with valid data, create a
 *    device id by concatenating the contents of the INQUIRY pid, and serial
 *    number:   <pid><INQUIRY serial #>
 *
 * NOTES: All vpd_inq fields will retain their originally initializes values
 *        if none of the above conditions is satisfied.
 *        vpd_inq->devid_type value at enter time is DEVID_INVALID, it is
 *	  modified in case of success only, and just before returning.
 *
 */
void
s8_ssd_devids(struct vpd_inq_data *vpd_inq, unsigned char *devid_str,
    int *converted)
{
	unsigned int vid_len = 0;
	unsigned int pid_len = 0;
	unsigned int ser_len = 0;
	unsigned int i;

	(void) memset(devid_str, 0, MAXNAMELEN);

	if (vpd_inq->supported_page_mask & VPD_DEVICE_ID_PAGE) {
		if (vpd_inq->vpd_wwn_len == (unsigned int)-1) {
			/* vpd_wwn_len was too big in inquiry */
			return;
		}
		if (vpd_inq->vpd_wwn_len > (MAXNAMELEN / 2)) {
			/* would overflow devid_str */
			return;
		}
		for (i = 0; i < vpd_inq->vpd_wwn_len; i++) {
			(void) sprintf((char *)&devid_str[i * 2], "%02x",
			    vpd_inq->vpd_wwn[i]);
		}
		*converted = 1;
		vpd_inq->devid_len = vpd_inq->vpd_wwn_len;
		vpd_inq->devid_type = DEVID_SCSI3_WWN;
	} else if (vpd_inq->supported_page_mask & VPD_SERIAL_NUM_PAGE) {
		if (vpd_inq->vpd_serial_num_len == (unsigned int)-1) {
			/* vpd_serial_num_len was too big in inquiry */
			return;
		}
		vid_len = sizeof (vpd_inq->inq_data->inq_vid);
		ser_len = vpd_inq->vpd_serial_num_len;
		if ((vid_len + ser_len) > MAXNAMELEN) {
			/* would overflow devid_str */
			return;
		}
		bcopy(vpd_inq->inq_data->inq_vid, devid_str, vid_len);
		bcopy(vpd_inq->vpd_serial_num, &devid_str[vid_len], ser_len);
		vpd_inq->devid_len = vid_len + ser_len;
		vpd_inq->devid_type = DEVID_SCSI_SERIAL;
	} else if (bcmp(&vpd_inq->inq_data->inq_pid[9], "SUN", 3) == 0) {
		pid_len = sizeof (vpd_inq->inq_data->inq_pid);
		ser_len = sizeof (vpd_inq->inq_data->inq_serial);
		if ((vid_len + ser_len) > MAXNAMELEN) {
			/* would overflow devid_str */
			return;
		}
		bcopy(vpd_inq->inq_data->inq_pid, devid_str, pid_len);
		bcopy(vpd_inq->inq_data->inq_serial, &devid_str[pid_len],
		    ser_len);
		vpd_inq->devid_len = pid_len + ser_len;
		vpd_inq->devid_type = DEVID_SCSI_SERIAL;
	}
}

/*
 * Solaris 9 sd & ssd Algorithm:
 * =============================
 * 1) Issue an INQUIRY command for the list of supported VPD pages.
 *
 * 2) Determine from the VPD page list if the device identification VPD page
 *    (0x83) is supported.
 *       2a) If page 0x83 is available issue an INQUIRY command for VPD page
 *           0x83.
 *       2b) Inspect the returned identification descriptor list to determine if
 *           descriptor type 0x03 (FC-PH, FC-PH3 or FC-FS Name_Identifier) is
 *           available. If descriptor type 0x03 is available it is used as the
 *           device id.
 *
 * 3) If VPD page 0x83 with descriptor type 0x03 is not available, determine
 *    from the VPD page list if the unit serial number VPD page (0x80) is
 *    available.
 *       3a) If page 0x80 is available issue an INQUIRY command for VPD page
 *           0x80.
 *       3b) Inspect the VPD page 0x80 contents to determine if they are valid.
 *       3c) Create a device id by concatenating the contents of VPD page 0x80
 *           with the INQUIRY vid and pid:   <vid><pid><VPD page 0x80 serial #>
 *
 * 4) If VPD pages 0x83 and 0x80 are not available with valid data, determine if
 *    the disk has been qualified by Sun as indicated by the string "SUN" in the
 *    contents of the INQUIRY pid contents.
 *       4a) If the disk is Sun qualified create a device id by concatenating
 *           the contents of the INQUIRY vid, pid, and serial number:
 *           <vid><pid><INQUIRY serial #>
 *       4b) If the disk is not Sun qualified fabricate a device id using the
 *           Solaris operating system device id fabrication routine. This OS
 *           routine uses the machine's host id and a timestamp to create a
 *           device id.
 *
 * NOTES: All vpd_inq fields will retain their originally initializes values
 *        if none of the above conditions is satisfied.
 *        vpd_inq->devid_type value at enter time is DEVID_INVALID, it is
 *	  modified in case of success only, and just before returning.
 */
void
s9_devids(struct vpd_inq_data *vpd_inq, unsigned char *devid_str,
    int *converted)
{
	unsigned int vid_len = 0;
	unsigned int pid_len = 0;
	unsigned int ser_len = 0;
	unsigned int i;

	(void) memset(devid_str, 0, MAXNAMELEN);

	if (vpd_inq->supported_page_mask & VPD_DEVICE_ID_PAGE) {
		if (vpd_inq->vpd_wwn_len == (unsigned int)-1) {
			/* vpd_inq->vpd_wwn_len was too big in inquiry */
			return;
		}
		if (vpd_inq->vpd_wwn_len > (MAXNAMELEN / 2)) {
			/* would overflow devid_str */
			return;
		}
		for (i = 0; i < vpd_inq->vpd_wwn_len; i++)
			(void) sprintf((char *)&devid_str[i * 2], "%02x",
			    vpd_inq->vpd_wwn[i]);
		*converted = 1;
		vpd_inq->devid_len = vpd_inq->vpd_wwn_len;
		vpd_inq->devid_type = DEVID_SCSI3_WWN;
	} else if (vpd_inq->supported_page_mask & VPD_SERIAL_NUM_PAGE) {
		if (vpd_inq->vpd_serial_num_len == (unsigned int)-1) {
			/* vpd_serial_num_len was too big in inquiry */
			return;
		}
		vid_len = sizeof (vpd_inq->inq_data->inq_vid);
		pid_len = sizeof (vpd_inq->inq_data->inq_pid);
		ser_len = vpd_inq->vpd_serial_num_len;
		if ((vid_len + ser_len + pid_len) > MAXNAMELEN) {
			/* would overflow devid_str */
			return;
		}
		bcopy(vpd_inq->inq_data->inq_vid, devid_str, vid_len);
		bcopy(vpd_inq->inq_data->inq_pid, &devid_str[vid_len], pid_len);
		bcopy(vpd_inq->vpd_serial_num, &devid_str[vid_len + pid_len],
		    ser_len);
		vpd_inq->devid_len = vid_len + pid_len + ser_len;
		vpd_inq->devid_type = DEVID_SCSI_SERIAL;
	} else if (bcmp(&vpd_inq->inq_data->inq_pid[9], "SUN", 3) == 0) {
		vid_len = sizeof (vpd_inq->inq_data->inq_vid);
		pid_len = sizeof (vpd_inq->inq_data->inq_pid);
		ser_len = sizeof (vpd_inq->inq_data->inq_serial);
		if ((vid_len + ser_len + pid_len) > MAXNAMELEN) {
			/* would overflow devid_str */
			return;
		}
		bcopy(vpd_inq->inq_data->inq_vid, devid_str, vid_len);
		bcopy(vpd_inq->inq_data->inq_pid, &devid_str[vid_len], pid_len);
		bcopy(vpd_inq->inq_data->inq_serial,
		    &devid_str[vid_len + pid_len], ser_len);
		vpd_inq->devid_len = vid_len + pid_len + ser_len;
		vpd_inq->devid_type = DEVID_SCSI3_WWN;
	}
}


/*
 * Entry point for code which emulates the Solaris 8 and Solaris 9 sd / ssd
 * device id algorithms.  The main purpose of this code is to allow device ids
 * stored persistnetly in the CCR to be compared with new device ids being
 * reported by Solaris after an event which has caused Solaris to report
 * different device ids than it did previsously (generally an O/S upgrade or
 * O/S patch affecting sd or ssd).  Specifically, we will be using it during the
 * transition from Solaris 8 to Solaris 9, which is known to cause device ids to
 * change.
 * Input:
 *        char *device - the full path name of the device whose devid is being
 *                       requested.  This must be the full Solaris path to the
 *			 device (eg.  /dev/rdsk/c1t1d0s2).  s2 must be specified
 *			 and it must be the Solaris path, not an alternate path
 *			 such as the DID path.  The code examines the device
 *			 path in order to determine if the device is managed by
 *			 by sd or ssd (since Solaris 8 devid algorithms are
 *			 different for these two drivers). Use of the wrong path
 *			 can cause this code to choose the wrong Solaris driver
 *			 and hence return an incorrect device id.
 *        int version - The Solaris release to emulate when computing the device
 *	                 id.  Current acceptable values are 8 and 9.
 *        char * devidbuff - buffer to store the hexa dump of the device id in
 *	  did_device_list_t *instptr - A DID device structure.  The comnputed
 *	                 device id will be stored in the target_device_id field,
 *			 this is the only field of the structure which will be
 *			 filled in.
 * Return Values:
 *       0 - No error, device id was successfully computed.
 *	-1 - An internal error prevented computation of the device id.
 *	-2 - An illegal parameter was passed in.
 *	 1 - It is not possible to compute the device for this device.  It is
 *	     either a cd-rom device or some other device not under sd or ssd
 *	     control.
 */
int
get_solaris_devid(char *device, int version, unsigned char *devidbuff,
    did_device_list_t *instptr)
{
	struct vpd_inq_data vpd_inq;
	char link_buf[MAXNAMELEN];
	unsigned char *devconv;
	int converted = 0;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/* verify arguments */
	if ((version != 8) && (version != 9)) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: Illegal Solaris version "
		    "requested\n"), "get_solaris_devid");
		return (-2);
	}
	if ((device == NULL) || (devidbuff == NULL)) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: Null pointer delivered\n"),
		    "get_solaris_devid");
		return (-2);
	}

	/* initialize data */
	vpd_inq.devid_len = 0;
	vpd_inq.vpd_serial_num_len = 0;
	vpd_inq.vpd_wwn_len = 0;

	/* allocate memory */
	vpd_inq.dev_path = malloc(MAXNAMELEN);
	if (!vpd_inq.dev_path) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: Error, malloc() failed\n"),
		    "get_solaris_devid");
		return (-1);
	}
	vpd_inq.inq_data = malloc(sizeof (struct scsi_inquiry));
	if (!vpd_inq.inq_data) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: Error, malloc() failed\n"),
		    "get_solaris_devid");
		return (-1);
	}
	vpd_inq.vpd_serial_num = malloc(MAXNAMELEN);
	if (!vpd_inq.vpd_serial_num) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: Error, malloc() failed\n"),
		    "get_solaris_devid");
		return (-1);
	}
	vpd_inq.vpd_wwn = malloc(MAXNAMELEN);
	if (!vpd_inq.vpd_wwn) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: Error, malloc() failed\n"),
		    "get_solaris_devid");
		return (-1);
	}

	/*
	    XXX Should we verify the device path is Solaris path?
	    The check to determine sd vs ssd relies on this heavily.
	*/
	if (strlen(device) < MAXNAMELEN) {
		(void) strcpy(vpd_inq.dev_path, device);
	} else {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "too big device name %s\n"),
		    "get_solaris_devid", device);
		return (1);
	}
	vpd_inq.dev_type = OTHER_DEV_TYPE;
	vpd_inq.supported_page_mask = 0x00;

	vpd_inq.fd = open(vpd_inq.dev_path, O_RDONLY | O_NDELAY);
	if (vpd_inq.fd == -1) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: Unable to open %s\n"),
		    "get_solaris_devid", vpd_inq.dev_path);
		return (1);
	}

	/* Don't test this device if it is a CD-ROM */
	if (is_cdrom(&vpd_inq)) {
		(void) close(vpd_inq.fd);
		return (1);
	}

	/* sd or ssd? */
	if (readlink(vpd_inq.dev_path, link_buf, sizeof (link_buf)) == -1) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s:Unable to read device link %s\n"),
		    "get_solaris_devid", vpd_inq.dev_path);
		return (-1);
	}
	if (strstr(link_buf, "ide")) {
		/* (s)sd not bound so just return */
		return (1);
	} else if (strstr(link_buf, "ssd")) {
		vpd_inq.dev_type = SSD_DEV_TYPE;
	} else {
		vpd_inq.dev_type = SD_DEV_TYPE;
	}

	/* get all information required to construct devids */
	if (inq(&vpd_inq) == -1) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN,
		    "invalid vpd inquiry data - "
		    "no device id available for %s\n"),
		    device);
		return (-1);
	}

	/* As vpd_inq comes from stack, setup invalid devid type */
	vpd_inq.devid_type = (unsigned short) DEVID_INVALID;

	/* get the requested devid */
	if (version == 9) {
		s9_devids(&vpd_inq, devidbuff, &converted);
	} else {
		if (vpd_inq.dev_type == SSD_DEV_TYPE) {
			s8_ssd_devids(&vpd_inq, devidbuff, &converted);
		} else if (vpd_inq.dev_type == SD_DEV_TYPE) {
			s8_sd_devids(&vpd_inq, devidbuff);
		}
	}

	/* Check that a known devid type has been found */
	if (vpd_inq.devid_type == DEVID_INVALID) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN,
		    "invalid vpd inquiry data - "
		    "no device id available for %s\n"),
		    device);
		return (-1);
	}

	/*
	 * WWN type devids are already formatted correctly for comparison with
	 * field number 5 of the did_instances CCR file.  Other devid types
	 * need to be converted.
	 */

	if (converted == 0) {
		if (did_diskidtostr((char *)devidbuff,
		    (int)vpd_inq.devid_len, (char **)&devconv) != 0) {
			(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN, "%s: Unable to convert "
			    "devid"), "get_solaris_devid");
			return (-1);
		}
		(void) strcpy((char *)devidbuff, (char *)devconv);
		free(devconv);
	}

	did_strtodiskid((char *)devidbuff, instptr);
	instptr->type = vpd_inq.devid_type;
	if (instptr->type && instptr->target_device_id)
		did_set_type(instptr->target_device_id, instptr->type);

	(void) close(vpd_inq.fd);
	return (0);
}

/*
 * did_setglobalfencingstatus()
 */
int
did_setglobalfencingstatus(unsigned short newstatus)
{
	int ret;
	DID_LOCK(did_lock);
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	if (didinternal_setglobalfencingstatus == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: Cannot initialize library "
		    "interface.\n"), "did_setglobalfencingstatus");
		DID_UNLOCK(did_lock);
		return (-1);
	}
	ret = didinternal_setglobalfencingstatus(newstatus);
	DID_UNLOCK(did_lock);
	return (ret);
}

/*
 * did_geterrorstr() function.
 * Part of DID libraries API. Use to read the per thread DID error string
 * (replaces 'did_error' global string in previous version of this file,
 * not thread safe).
 * Returns the per thread DID error string.
 * Until a thread specific data value is associated with did_error_key by
 * the calling thread, this function returns NULL. Return NULL occurs only the
 * first time a thread calls a DID libraries API function. See
 * did_lib_pthread_init() comments and man pthread_getspecific.
 */
char *
did_geterrorstr(void)
{
	return ((char *)pthread_getspecific(did_error_key));
}


/*
 * did_free_key() function.
 * Release the per thread DID error string.
 * Automatically called when a thread exits, see did_geterrorstr() function
 * comments.
 */
static void
did_free_key(void *addr)
{
	free(addr);
}


/*
 * did_fork_prepare() function.
 * The DID libraries thread fork handler. Ensure that before a fork,
 * did_lock is taken. See man pthread_atfork().
 */
static void
did_fork_prepare(void)
{
	DID_LOCK(did_lock);
}


/*
 * did_fork_parent() function.
 * The DID libraries thread fork handler. Ensure that after a fork, in the
 * parent, did_lock is released. See man pthread_atfork().
 */
static void
did_fork_parent(void)
{
	DID_UNLOCK(did_lock);
}


/*
 * did_fork_children() function.
 * The DID libraries thread fork handler. Ensure that after a fork, in the
 * child, did_lock is released. See man pthread_atfork().
 */
static void
did_fork_children(void)
{
	DID_UNLOCK(did_lock);
}


/*
 * did_lib_init_once is called only once by did_lib_pthread_init(), even if
 * did_lib_pthread_init() is executed many times.
 * This function initialize the did_lock recursive mutex (see comments about
 * did_lock at the begining of this file and in did_lib_pthread_init()).
 * This function creates the DID pthread specific key did_error_key (see
 * comments about did_error_key at the begining of this file).
 * This function also creates the DID pthread fork handler. The pthread fork
 * handler is used to avoid dealock on did_lock when fork is performed by a
 * thread. See man pthread_atfork() for details.
 */
static void
did_lib_init_once(void) {
	pthread_mutexattr_t attr;
	(void) pthread_mutexattr_init(&attr);
	(void) pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
	(void) pthread_mutex_init(&did_lock, &attr);
	(void) pthread_mutexattr_destroy(&attr);
	(void) pthread_atfork(did_fork_prepare,
	    did_fork_parent, did_fork_children);
	(void) pthread_key_create(&did_error_key, did_free_key);
}


/*
 * did_lib_pthread_init() function.
 *
 * This function is called by each DID librairies function API (through the
 * DID_LOCK macro). did_lib_pthread_init() calls only one time the
 * did_lib_init_once() function, that creates the global did_lock recursive
 * mutex and the pthread DID ibraries fork handler (see comments in the
 * lid_lib_init_once() function and about did_lock at the begin of this file)
 * and the did_error_key create, used to handle to per thread DID error string
 * (see comments about did_error_key at the begining of this file).
 *
 * In other words, the first thread that calls a DID libraries
 * API function initializes once the global did_lock recursive mutes and
 * the DID libraries pthread fork handler (that avoids did_lock
 * deaklocks during fork).
 *
 * Also when a thread calls a DID librairies function API (through
 * the DID_LOCK macro) for the first time, did_geterrorstr() will return
 * NULL because no data is associated to did_error_key (see comments in
 * did_geterrorstr()). In this case, a did error string for this thread
 * is allocated and associated to this thread through did_error_key. So next
 * time this thread calls a DID librairies function API, a did error error
 * string specific to this thread exists, and did_geterrorstr() does no more
 * returns NULL. This ensure that a thread calling DID librairies function API
 * will has a specific DID error string, that can be read by did_geterrorstr().
 *
 */
static void
did_lib_pthread_init(void)
{
	static pthread_once_t did_init_once = PTHREAD_ONCE_INIT;
	(void) pthread_once(&did_init_once, did_lib_init_once);
	if (did_geterrorstr() == NULL) {
		char *pdid_error;
		pdid_error = malloc(DID_ERRORSTR_SIZE);
		(void) pthread_setspecific(did_error_key, pdid_error);
	}
}


#ifdef SC_EFI_SUPPORT
/*
 *
 * Determine if a disk is EFI or not,
 * if disk is reserved, type is unknown.
 */
int
did_efi_disk(const char *name)
{
	int err;
	int fd;
	struct dk_geom geom;
	struct stat sb;
	char *ptr;
	char *rpath1;
	char *rpath2;
	char *pdir;
	char *pbase;
	/*
	 * did device path, build form 'name' argument
	 */
	char dname[MAXPATHLEN];
	/*
	 * rname is a copy of 'name' arg if "dsk" is not found in 'name',
	 * otherwise rname is a copy of 'name' with "dsk" replaced by "rdsk"
	 */
	char rname[MAXPATHLEN];
	int error = 0;

	if (name == NULL) {
		return (DID_DISK_OPEN_ERROR);
	}

	ptr = strstr(name, "/dsk/");
	if (ptr != NULL) {
		/* rname is 'name' with "dsk" replaced by "rdsk" */
		(void) memcpy(rname, name, (size_t)(ptr - name + 1));
		rname[ptr - name + 1] = 'r';
		(void) strcpy(&(rname[ptr - name + 2]), ptr + 1);
	} else {
		/* rname is just a copy of name */
		(void) strcpy(rname, name);
	}

	/* For dirname() and basename() use */
	rpath1 = strdup(rname);
	if (rpath1 == NULL) {
		return (DID_DISK_MEM_ERROR);
	}
	rpath2 = strdup(rname);
	if (rpath2 == NULL) {
		return (DID_DISK_MEM_ERROR);
	}

	pdir = dirname(rpath1);
	pbase = basename(rpath2);

	/* Compute DID name */
	(void) snprintf(dname, MAXPATHLEN, "%s", rname);
	if (pdir != NULL && pbase != NULL && pbase[0] == 'd') {
		if (strstr((const char *)pdir, "/global/") != NULL) {
			(void) snprintf(dname, MAXPATHLEN,
			    "/dev/did/rdsk/%s", pbase);
		}
	}
	if (pdir && (strcmp(pdir, ".") == 0) && pbase != NULL &&
	    pbase[0] == 'd') {
		(void) sprintf(dname, "/dev/did/rdsk/%s", pbase);
	}

	free(rpath1);
	free(rpath2);

	fd = open(dname, O_RDONLY|O_NDELAY|O_NONBLOCK);
	error = errno;
	if (fd < 0) {
		if (error == EBUSY) {
			/* A CDROM disk */
			return (DID_DISK_TYPE_CDROM);
		}
		if (strcmp(dname, rname) != 0) {
			/* Try with rname */
			fd = open(rname, O_RDONLY|O_NDELAY|O_NONBLOCK);
			error = errno;
			if (fd < 0) {
				if (error == EBUSY) {
					/* A CDROM disk */
					return (DID_DISK_TYPE_CDROM);
				}
				return (DID_DISK_OPEN_ERROR);
			}
		} else {
			return (DID_DISK_OPEN_ERROR);
		}
	}

	if (fstat(fd, &sb) < 0) {
		(void) close(fd);
		return (DID_DISK_TYPE_ERROR);
	}

	if ((sb.st_mode & S_IFMT) != S_IFCHR) {
		return (DID_DISK_TYPE_ERROR);
	}

	err = ioctl(fd, DKIOCGGEOM, &geom);
	error = errno;
	if (err == 0) {
		(void) close(fd);
		/* A VTOC disk */
		return (DID_DISK_TYPE_VTOC);
	}

	if (err == -1 && (error == ENOTSUP)) {
		(void) close(fd);
		/* An EFI disk */
		return (DID_DISK_TYPE_EFI);
	}
	(void) close(fd);
	/*
	 * That if the disk is shared and is reserved,
	 * ioctl DKIOCGGEOM will fail but does not return ENOTSUP.
	 */
	return (DID_DISK_TYPE_UNKNOWN);
}
#endif	/* SC_EFI_SUPPORT */
