/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident   "@(#)did_threads.cc 1.3     08/12/19 SMI"

#include "did_threads.h"
#include <sys/threadpool.h>

#define	DID_MAX_THREADS	100

/*
 * Class definition for the did_task.
 * The did_task represent the task required to be performed on
 * a single thread.
 */


class did_task : public defer_task {
public:
	did_task(task_func task_handler, void *task_arg);
	~did_task();

	void execute();

private:
	task_func	method;
	void		*arg;

	/*
	 * Disallow assignment and pass by value.
	 */

						/* CSTYLED */
	did_task(const did_task &);		/*lint -e754 */
						/* CSTYLED */
	did_task &operator = (did_task &);	/*lint -e754 */

};

did_task::did_task(task_func task_handler, void *task_arg)
{
	method = task_handler;
	arg = task_arg;
}

void did_task::execute() {

		(*method)(arg);
		delete this;

}

threadpool	*DID_Threadpoolp = NULL;

extern "C" int
initDIDthreadpool()
{
	int ret = 0;
	DID_Threadpoolp = new threadpool(true, 1,
		"DID Threadpool", DID_MAX_THREADS);
	if (!DID_Threadpoolp) {
	    ret = -1;
	}
	return (ret);
}

extern "C" void
finiDIDthreadpool()
{
	delete DID_Threadpoolp;
	DID_Threadpoolp = NULL;
}


/*
 * Lint complains about neither zero'ing nor freeing the pointer
 * members : method, arg.
 */
/* CSTYLED */
/*lint -e1540 */
did_task::~did_task()
{
}
/* CSTYLED */
/*lint +e1540 */

/*
 * API implementation published in did_threads.h
 */

extern "C" int
addTasktoThreadPool(task_func task_handler, void *task_arg)
{
	did_task *taskp = new did_task(task_handler, task_arg);
	if (taskp == NULL || DID_Threadpoolp == NULL) {

		/*
		 * Object creation failed, execute the task with current thread.
		 */
		(*task_handler)(task_arg);

	} else {
		DID_Threadpoolp->defer_processing(taskp);
	}
	return (0);

}

extern "C" void
wait4tasksinThreadPool(void)
{

	if (DID_Threadpoolp)
	    DID_Threadpoolp->quiesce(threadpool::QEMPTY);

}
