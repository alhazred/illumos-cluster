/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)didconf.c	1.32	08/09/22 SMI"

#include <stdio.h>
#include <fcntl.h>
#include <malloc.h>
#include <unistd.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <sys/sunddi.h>
#include <sys/ddi_impldefs.h>
#include <sys/dditypes.h>
#include <sys/param.h>
#include <sys/stat.h>
#include <sys/didio.h>
#include <sys/dklabel.h>
#include <libintl.h>
#include "libdid.h"
#include "didprivate.h"

#define	DEFAULT_DIDCONF "/etc/did.conf"

static char	*tapeminors[] = {
	"",
	"b",
	"bn",
	"c",
	"cb",
	"cbn",
	"cn",
	"h",
	"hb",
	"hbn",
	"hn",
	"l",
	"lb",
	"lbn",
	"ln",
	"m",
	"mb",
	"mbn",
	"mn",
	"n",
	"u",
	"ub",
	"ubn",
	"un"
	};


static did_device_type_t *did_typelist = NULL;


void
did_load_device_types(did_device_type_t **typelist)
{
	did_device_type_t	*tapedev, *diskdev;
	uint_t			count, index = 0;

	tapedev = malloc(sizeof (*tapedev));
	if (tapedev == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Out of memory.\n"));
		return;
	}
	tapedev->type_num = 1;
	(void) strcpy(tapedev->type_name, "tape");
	(void) strcpy(tapedev->primary_subdir, "rmt");
	tapedev->minor_count = 24;
	for (count = 0; (int)count < tapedev->minor_count; count++) {
		(void) sprintf(tapedev->minor_name[count], "%s,tp",
		    tapeminors[count]);
		(void) strcpy(tapedev->cur_minor_name[count],
		    tapeminors[count]);
		tapedev->minor_num[count] = (minor_t)count;
		tapedev->device_class[count] = S_IFCHR;
	}
	(void) strcpy(tapedev->discovery_minor, "l");
	tapedev->default_nodes = 10;
	tapedev->start_default_nodes = DID_MAX_INSTANCE;
	tapedev->next_default_node = -1;
	tapedev->next = NULL;
	*typelist = tapedev;

	diskdev = malloc(sizeof (*diskdev));
	if (diskdev == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Out of memory.\n"));
		return;
	}
	diskdev->type_num = 2;
	(void) strcpy(diskdev->type_name, "disk");
	(void) strcpy(diskdev->primary_subdir, "rdsk");
	diskdev->minor_count = NPMAP*2;
	for (count = 0; (int)count < (diskdev->minor_count / 2); count++) {
		diskdev->minor_num[index] = count;
		diskdev->device_class[index] = S_IFCHR;
		(void) sprintf(diskdev->minor_name[index], "s%d,raw", count);
		(void) sprintf(diskdev->cur_minor_name[index], "%c,raw",
		    97+count);

		index++;
	}
	for (count = 0; (int)count < (diskdev->minor_count / 2); count++) {
		diskdev->minor_num[index] = count;
		diskdev->device_class[index] = S_IFBLK;
		(void) sprintf(diskdev->minor_name[index], "s%d,blk", count);
		(void) sprintf(diskdev->cur_minor_name[index], "%c", 97+count);

		index++;
	}
	(void) strcpy(diskdev->discovery_minor, "s2");
	diskdev->default_nodes = 100;
	diskdev->start_default_nodes = 1;
	diskdev->next_default_node = 1;

	diskdev->next = NULL;
	(*typelist)->next = diskdev;

}

/*
 * called from save_instances()
 */
static int
writeRecordDescription(FILE *fp, did_device_list_t *instlist)
{
	did_device_list_t	*dlistp;
	did_subpath_t		*subptr;
	int			i;
	char			typestr[MAXNAMELEN];
	char			*id;

	for (dlistp = instlist; dlistp; dlistp = dlistp->next) {
		for (i = 0; i < dlistp->diskid_len; i++)
			if (!isprint(dlistp->diskid[i]))
				break;
		if (dlistp->diskid)
			(void) fprintf(fp, "#\n# %-*.*s\n#\n", i, i,
			    dlistp->diskid);
		else
			(void) fprintf(fp, "#\n#\n#\n");
		(void) fprintf(fp, "instance\t%d\n", dlistp->instance);

		if (did_typetostr(dlistp->type, typestr, sizeof (typestr)-1)) {
			/* Errno is set. Don't unset it. */
			return (-1);
		}
		(void) fprintf(fp, "\ttype %s\n", typestr);

		if (did_diskidtostr(dlistp->diskid, dlistp->diskid_len, &id)) {
			/* Errno is set. Don't unset it. */
			return (-1);
		}
		(void) fprintf(fp, "\tdiskid %s\n", id);
		free(id);

		for (subptr = dlistp->subpath_listp; subptr;
		    subptr = subptr->next) {
			(void) fprintf(fp, "\tsubpath %s\n",
				subptr->device_path);
		}
	}
	return (0);
}


static int
parseInstance(FILE *fp, int instNumber, did_device_list_t **instlist,
	did_device_type_t *typelist)
{
	char			*s, sbuf[256];
	long			curpos;
	char			arg[80];
	did_device_list_t	*instptr;
	did_device_type_t	*devtypeptr;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (did_findInstance(instNumber, *instlist, &instptr)) {
		/* Errno is set. Don't unset it. */
		return (-1);
	}

	if (instptr != NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Duplicate instance %d.\n"),
			instNumber);
		return (-1);
	}

	instptr = malloc(sizeof (*instptr));
	if (instptr != NULL)
		(void) memset(instptr, 0, sizeof (*instptr));
	else {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Out of memory.\n"));
		return (-1);
	}

	instptr->instance = instNumber;
	if ((!(*instlist)) || instNumber < (*instlist)->instance) {
		instptr->next = *instlist;
		*instlist = instptr;
	} else {
		did_device_list_t	*ninst = *instlist;

		while (ninst->next) {
			if (instNumber < ninst->next->instance)
				break;
			ninst = ninst->next;
		}
		instptr->next = ninst->next;
		ninst->next = instptr;
	}

	while (curpos = ftell(fp),
	    (s = fgets(sbuf, sizeof (sbuf)-1, fp)) != NULL) {
		if (*s == '\0' || *s == '#')
			continue;
		if (*s != '\t' && *s != ' ') {
			(void) fseek(fp, curpos, SEEK_SET);
			return (0);
		}

		if (sscanf(s, " diskid %s", arg) == 1) {
			did_strtodiskid(arg, instptr);
			if (instptr->type && instptr->target_device_id)
				did_set_type(instptr->target_device_id,
				    instptr->type);
			continue;
		}
		if (sscanf(s, " type %s", arg) == 1) {
			instptr->type = did_strtotype(arg);
			if (instptr->target_device_id)
				did_set_type(instptr->target_device_id,
				    instptr->type);
			continue;
		}
		if (sscanf(s, " subpath %s", arg) == 1) {
			did_subpath_t		*subptr = NULL;

			subptr = malloc(sizeof (*subptr));
			if (subptr != NULL)
				(void) memset(subptr, 0, sizeof (*subptr));
			else {
				(void) snprintf(did_geterrorstr(),
				    DID_ERRORSTR_SIZE,
				    dgettext(TEXT_DOMAIN, "Out of memory.\n"));
				return (-1);
			}
			(void) strcpy(subptr->device_path, arg);
			subptr->next = instptr->subpath_listp;
			subptr->instptr = instptr;
			instptr->subpath_listp = subptr;


			for (devtypeptr = typelist; devtypeptr != NULL;
			    devtypeptr = devtypeptr->next) {
				/*
				 * XXX
				 * Actually want to just grab from
				 * CCR table
				 */
				if (strstr(instptr->subpath_listp->device_path,
				    devtypeptr->primary_subdir) != 0) {
					instptr->device_type = devtypeptr;
					break;
				}
			}
			continue;
		} else {
			(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN, "Invalid entry for instance "
				"%d.\n"), instNumber);
			return (-1);
		}
	}
	return (0);
}

static int
save_instances(char *savefile, did_device_list_t *instlist)
{
	FILE	*fp;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if ((fp = fopen(savefile, "w")) == 0) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Cannot open file %s.\n"), savefile);
		return (-1);
	}
	if (writeRecordDescription(fp, instlist)) {
		/* Errno is set. Don't unset it. */
		return (-1);
	}
	(void) fclose(fp);
	return (0);
}

static int
getfmts(char *fmtfile, did_device_list_t **instlist,
				did_device_type_t *typelist)
{
	char    *s, sbuf[256];
	int	icommand;
	FILE	*fp;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if ((fp = fopen(fmtfile, "r")) == 0) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Cannot open file %s.\n"), fmtfile);
		return (-1);
	}

	while ((s = fgets(sbuf, sizeof (sbuf)-1, fp)) != NULL) {
		if ((*s == '\0') || (*s == '#'))
			continue;
		if (sscanf(s, "instance %d", &icommand) == 1) {
			if (parseInstance(fp, icommand, instlist, typelist))
				return (-1);
			continue;
		}
	}
	(void) fclose(fp);
	did_sort_instances(*instlist);

	if (did_checkRecordDescription(*instlist)) {
		/* Errno is set. Don't unset it. */
		return (-1);
	}
	return (0);
}

/*
 * Implementation of the getinstlist method.
 */
/* ARGSUSED */
int
did_impl_getinstlist(did_device_list_t **ptr, int mode,
	did_device_type_t *typelist, char *fmtfile)
{
	did_device_list_t	*instlist = NULL;

	if (fmtfile == NULL) {
		if (getfmts(DEFAULT_DIDCONF, &instlist, typelist) != 0) {
			/* Errno is set. Don't unset it. */
			return (-1);
		}
	} else {
		if (getfmts(fmtfile, &instlist, typelist) != 0) {
			/* Errno is set. Don't unset it. */
			return (-1);
		}
	}
	*ptr = instlist;
	return (0);
}

int
did_impl_freeinstlist(did_device_list_t *ptr)
{
	if (ptr == NULL) {
		return (0);
	}
	did_device_list_free(ptr);
	return (0);
}

/*
 * This interface may need to be enhanced if the old didadm option of an
 * alternate configuration file is to be supported.
 */
/* ARGSUSED */
int
did_impl_saveinstlist(did_device_list_t *ptr, char *savefile, char *node,
	int flag)
{
	flag = flag;

	if (savefile == NULL)
		return (save_instances(DEFAULT_DIDCONF, ptr));
	else
		return (save_instances(savefile, ptr));
}

/* ARGSUSED */
int
did_impl_saveinst(did_device_list_t *ptr, int create)
{
	/* Nothing to do here */

	return (0);
}

/* ARGSUSED */
int
did_impl_read_repl_entry(int key, char **data)
{
	/* Nothing to do here */

	return (0);
}

/* ARGSUSED */
int
did_impl_getrepllist(did_repl_list_t **ptr)
{
	/* Nothing to do here */

	return (0);
}

/* ARGSUSED */
int
did_impl_rm_repl_entry(int key)
{
	/* Nothing to do here */

	return (0);
}

/* ARGSUSED */
int
did_impl_add_repl_entry(did_repl_list_t *repldata)
{
	/* Nothing to do here */

	return (0);
}

/* ARGSUSED */
int
did_impl_allocinstance(did_subpath_t *subptr, did_device_list_t **instlist,
	char *node)
{
	int			instance;
	did_device_list_t	*instp = *instlist;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (strcmp(subptr->instptr->device_type->type_name, "disk") == 0) {

		instance = 1;
		while (instp) {
			if (instp->device_type ==
			    subptr->instptr->device_type) {
				if (instance <= instp->instance)
					instance = instp->instance + 1;
			}
			instp = instp->next;
		}

	} else if (strcmp(subptr->instptr->device_type->type_name, "tape") ==
	    0) {
		instance = DID_MAX_INSTANCE;
		while (instp) {
			if (instp->device_type ==
			    subptr->instptr->device_type) {
					if (instance >= instp->instance)
						instance = instp->instance - 1;
			}
			instp = instp->next;
		}

	} else {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Device type %s invalid.\n"),
		    subptr->instptr->device_type->type_name);
		return (-1);
	}

	subptr->instptr->instance = instance;
	subptr->instptr->next = *instlist;
	*instlist = subptr->instptr;
	return (0);
}

int
did_impl_gettypelist(did_device_type_t **list)
{
	did_device_type_t *typelist;

	if (did_typelist != NULL) {
		*list = did_typelist;
		return (0);
	}

	did_load_device_types(&typelist);
	*list = typelist;
	did_typelist = typelist;
	return (0);
}

/* ARGSUSED */
int
did_impl_savetypelist(did_device_type_t *list)
{

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
	    dgettext(TEXT_DOMAIN, "%s: Operation invalid.\n"),
		"did_impl_savetypelist");
	return (-1);
}

/* ARGSUSED */
int
did_impl_mapdidtodev(char *did_dev, did_subpath_t *dev_path)
{
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
	    dgettext(TEXT_DOMAIN, "%s: Operation invalid.\n"),
		"did_impl_mapdidtodev");
	return (-1);
}

/* ARGSUSED */
int
did_impl_mapdevtodid(char *did_dev, did_subpath_t *dev_path)
{
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
	    dgettext(TEXT_DOMAIN, "%s: Operation invalid.\n"),
		"did_impl_mapdevtodid");
	return (-1);
}

int
did_impl_exec_program_local(char *pgm)
{
	if (pgm == NULL)
		return (-1);

	return (system(pgm));
}

int
did_impl_syncgdevs()
{
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
	    dgettext(TEXT_DOMAIN, "%s: Operation invalid.\n"),
		"did_impl_syncgdevs");
	return (-1);
}

/* ARGSUSED */
int
did_impl_syncrepl(char *exec_node, char *command)
{
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
	    dgettext(TEXT_DOMAIN, "%s: Operation invalid.\n"),
		"did_impl_syncrepl");
	return (-1);
}

/* ARGSUSED */
int
did_impl_initlibspecific()
{
	/* Nothing to do to specifically init the conf file. */

	return (0);
}

int
did_impl_holdccrlock(void)
{
	/* Nothing to do to specifically init the conf file. */

	return (0);
}

int
did_impl_releaseccrlock(int ret)
{
	/* Nothing to do to specifically init the conf file. */
	ret = ret;
	return (0);
}

int
did_impl_getinst(did_device_list_t **p, int ret)
{
	/* Nothing to do to specifically init the conf file. */
	p = p;
	ret = ret;
	return (0);
}

int
did_impl_populate_list_cache(did_device_list_t *dlist)
{
	/* Nothing to do here */
	dlist = dlist;
	return (0);
}

int
did_impl_getglobalfencingstatus(unsigned short *status)
{
	/* Nothing to do here */
	*status = *status;
	return (0);
}

int
did_impl_setglobalfencingstatus(unsigned short newstatus)
{
	FILE *fp;
	char *base_dir;
	char lib_path[BUFSIZ];

	/* Get the base dir */
	base_dir = getenv("SC_BASEDIR");
	if (!base_dir) {
		base_dir = "/";
	}
	(void) sprintf(lib_path, "%s/etc/cluster/ccr/global/global_fencing",
	    base_dir);

	/*
	 * Manually create the global_fencing CCR file.
	 * This should only be called through scinstall.
	 */

	if ((fp = fopen(lib_path, "w")) == 0) {
		(void) printf("error on open\n");
		return (-1);
	}

	(void) fprintf(fp, "global_fencing_protocol %d\n", newstatus);

	(void) fclose(fp);
	return (0);
}
