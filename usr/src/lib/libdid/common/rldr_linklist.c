/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident   "@(#)rldr_linklist.c 1.4     08/12/15 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include "rldr_linklist.h"

/*
*/
int
initRldrList(rldr_devlist_t **xList) {

	rldr_devlist_t *zlist = NULL;
	*xList = (rldr_devlist_t *)malloc(sizeof (struct _list));
	if (!xList)
	    return (-1);
	zlist = *xList;
	zlist->head = (iscsi_dev *)malloc(sizeof (struct _iscsi_dev));
	zlist->tail = (iscsi_dev *)malloc(sizeof (struct _iscsi_dev));
	if (!zlist->head || !zlist->tail)
	    return (-1);
	zlist->head->next = zlist->tail;
	zlist->tail->next = zlist->tail;
	zlist->head->dev_path = NULL;
	zlist->tail->dev_path = NULL;
	zlist->size = 0;

	return (0);
}

/*
 * If "element" parameter is NULL, the function adds the element
 * to the head of the list
 */
int
insertAfter(rldr_devlist_t *xList, iscsi_dev *element, char *device_path) {

	iscsi_dev *newElement = NULL;

	if (!device_path)
	    return (-1);
	newElement = (iscsi_dev *)malloc(sizeof (struct _iscsi_dev));
	if (!newElement)
	    return (-1);
	newElement->dev_path = strdup(device_path);
	if (!newElement->dev_path) {
	    freeElem(newElement);
	    return (-1);
	}
	newElement->attach_complete = 0;
	newElement->didload_complete = 0;
	if (element == NULL) {
	    element = xList->head;
	}
	newElement->next = element->next;
	element->next = newElement;
	xList->size++;

	return (0);
}

/*
*/
void
deleteNext(rldr_devlist_t *xList, iscsi_dev *element) {

	iscsi_dev *deleteElem = NULL;

	deleteElem = element->next;
	element->next = deleteElem->next;

	freeElem(deleteElem);
	xList->size--;
}

/*
 * returns the next element in the source list after the moved element
 */
void
moveElementAfter(rldr_devlist_t *xSourceList, iscsi_dev *element,
	rldr_devlist_t *xDestList) {

	iscsi_dev *toMove = NULL;

	toMove = element->next;
	element->next = element->next->next;
	xSourceList->size--;

	toMove->next = xDestList->head->next;
	xDestList->head->next = toMove;
	xDestList->size++;
}

/*
*/
static void
freeElem(iscsi_dev *element) {
	if (!element)
	    return;
	if (element->dev_path)
	    free(element->dev_path);
	element->next = NULL;
	free(element);
	element = NULL;
}

/*
*/
void emptyRldrList(rldr_devlist_t *xList) {

	iscsi_dev *dev = NULL;

	if (!xList)
	    return;
	dev = xList->head;
	while (dev->next != dev->next->next) {
	    deleteNext(xList, dev);
	}
}

void freeRldrList(rldr_devlist_t *xList) {

	emptyRldrList(xList);
	freeElem(xList->head);
	freeElem(xList->tail);
	free(xList);
}
