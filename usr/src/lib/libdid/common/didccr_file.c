/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)didccr_file.c	1.35	08/08/01 SMI"

#include <stdio.h>
#include <fcntl.h>
#include <malloc.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/sunddi.h>
#include <sys/ddi_impldefs.h>
#include <sys/dditypes.h>
#include <sys/param.h>
#include <sys/stat.h>
#include <sys/didio.h>
#include <sys/clconf.h>
#include <sys/clconf_int.h>
#include <libintl.h>
#include "libdid.h"
#include "didprivate.h"

#define	DEFAULT_CCR_INSTFILE	"/etc/cluster/ccr/global/did_instances"
#define	DEFAULT_CCR_TYPEFILE	"/etc/cluster/ccr/global/did_types"
#define	DEFAULT_CCR_INFRAFILE	"/etc/cluster/ccr/global/infrastructure"
#define	DEFAULT_CCR_REPLFILE	"/etc/cluster/ccr/global/replicated_devices"
#define	DIDCCR_MAX_ELEM_SZ	8192
#define	CCR_INIT		"/usr/cluster/lib/sc/ccradm -i"
#define	CCR_DIRECTORY		"/etc/cluster/ccr/global/directory"

static did_device_type_t *did_typelist = NULL;



/*
 * find the whitespace-separated key value pair in the passed string. Insert a
 * '\0' where the tab was in the passed string.
 */
static int
didccr_file_parse_element(char *s, char **key, char **value)
{
	char	*separator;
	int	nchars = 1;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	*key = s;
	separator = strchr(s, '\t');
	if (separator == NULL) {
		char	*end_separator;

		if ((separator = strchr(s, ' ')) == NULL) {
			(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN, "Invalid format: %s\n"), s);
			return (-1);
		}
		end_separator = &separator[1];
		while (*end_separator == ' ') {
			end_separator++;
			nchars++;
		}
	}
	*separator = '\0';
	*value = &separator[nchars];

	/* Ignore the line if key is a checksum or gennum */
	if ((strncmp(*key, "ccr_checksum", strlen("ccr_checksum")) == 0) ||
	    (strncmp(*key, "ccr_gennum", strlen("ccr_gennum")) == 0)) {
		*key = NULL;
		*value = NULL;
	}

	return (0);
}

/*
 * Get the next element from an opened CCR file. Store it in a buffer and
 * return a pointer to same. It is the caller's responsibility to free the
 * returned storage.
 */
static int
didccr_file_next_element(FILE *fp, char **out)
{
	char			*sbuf, *s;
	uint_t			size = DIDCCR_MAX_ELEM_SZ;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if ((sbuf = (char *)malloc(size)) == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Out of memory.\n"));
		return (-1);
	}
	if ((s = fgets(sbuf, (int)size, fp)) != NULL) {
		uint_t len;

		len = strlen(s);
		/*
		 * Make sure we got the whole element. We verify this
		 * by making sure a '\n' is the last character in the string.
		 * If not, double the buffer size and realloc until the whole
		 * element is retrieved.
		 */
		while (sbuf[len - 1] != '\n') {
			size *= 2;
			if ((sbuf = (char *)realloc(sbuf, size)) == NULL) {
				(void) snprintf(did_geterrorstr(),
				    DID_ERRORSTR_SIZE,
				    dgettext(TEXT_DOMAIN, "Out of memory.\n"));
				return (-1);
			}
			(void) fgets(&sbuf[len], (int)size/2, fp);
			s = sbuf;
			len = strlen(s);
		}
		/*
		 * remove the newline.
		 */
		sbuf[strlen(sbuf) - 1] = '\0';
	}
	*out = s;
	return (0);
}

static int
didccr_file_read_device_types(FILE *fp, did_device_type_t **out)
{
	did_device_type_t	*typelist = NULL;
	char			*buf = NULL;
	char			*key, *data;

	if (didccr_file_next_element(fp, &buf)) {
		/* Errno is set. Don't unset it. */
		return (-1);
	}
	while (buf != NULL) {
		if (didccr_file_parse_element(buf, &key, &data)) {
			free(buf);
			return (-1);
		}
		if ((key != NULL) && (data != NULL)) {

			if (did_ccr_str_to_device_type(key, data, &typelist)) {
				free(buf);
				return (-1);
			}
		}
		free(buf);
		if (didccr_file_next_element(fp, &buf)) {
			/* Errno is set. Don't unset it. */
			return (-1);
		}
	}
	*out = typelist;
	return (0);
}

static int
didccr_file_read_nodename(FILE *fp, nodeid_t nodeid, char *nodename)
{
	char			*buf;
	char			*key, *data;
	char			nodename_key[MAXNAMELEN];

	if (didccr_file_next_element(fp, &buf)) {
		/* Errno is set. Don't unset it. */
		return (-1);
	}
	(void) sprintf(nodename_key, "cluster.nodes.%d.name", nodeid);
	while (buf != NULL) {
		if (didccr_file_parse_element(buf, &key, &data)) {
			free(buf);
			return (-1);
		}
		if ((key != NULL) && (data != NULL)) {
			if (strcmp(key, nodename_key) == 0) {
				(void) strcpy(nodename, data);
				free(buf);
				return (0);
			}
		}
		free(buf);
		if (didccr_file_next_element(fp, &buf)) {
			/* Errno is set. Don't unset it. */
			return (-1);
		}
	}
	return (-1);
}

static int
didccr_file_read_nodeid(FILE *fp, char *nodename, nodeid_t nodeid)
{
	char			*buf;
	char			*key, *data;
	char			nodename_key[MAXNAMELEN];

	if (didccr_file_next_element(fp, &buf)) {
		/* Errno is set. Don't unset it. */
		return (-1);
	}
	(void) sprintf(nodename_key, "cluster.nodes.%d.name", nodeid);
	while (buf != NULL) {
		if (didccr_file_parse_element(buf, &key, &data)) {
			free(buf);
			return (-1);
		}
		if ((key != NULL) && (data != NULL)) {
			if ((strcmp(nodename_key, key) == 0) &&
			    (strcmp(data, nodename) == 0)) {
				free(buf);
				return (0);
			}
		}
		free(buf);
		if (didccr_file_next_element(fp, &buf)) {
			/* Errno is set. Don't unset it. */
			return (-1);
		}
	}
	return (1);
}

static int
didccr_file_read_repls(FILE *fp, did_repl_list_t **ptr)
{
	did_repl_list_t	*new_repl = NULL;
	char			*buf;
	char			*key, *data;

	if (didccr_file_next_element(fp, &buf)) {
		/* Errno is set. Don't unset it. */
		return (-1);
	}
	while (buf != NULL) {
		if (didccr_file_parse_element(buf, &key, &data)) {
			free(buf);
			did_repl_list_free(*ptr);
			return (-1);
		}
		if ((key != NULL) && (data != NULL)) {
			new_repl = (did_repl_list_t *)calloc(1,
			    sizeof (did_repl_list_t));
			if (new_repl == NULL) {
				free(buf);
				did_repl_list_free(*ptr);
				return (-1);
			}
			new_repl->next = *ptr;
			*ptr = new_repl;
			if (did_repl_str_to_instance(key, data, new_repl)
			    != 0) {
				free(buf);
				did_repl_list_free(*ptr);
				return (-1);
			}
		}
		free(buf);
		if (didccr_file_next_element(fp, &buf)) {
			/* Errno is set. Don't unset it. */
			return (-1);
		}
	}

	return (0);
}

static int
didccr_file_read_instances(FILE *fp, did_device_type_t *typelist,
	did_device_list_t **out)
{
	did_device_list_t	*instlist = NULL;
	char			*buf;
	char			*key, *data;

	if (didccr_file_next_element(fp, &buf)) {
		/* Errno is set. Don't unset it. */
		return (-1);
	}
	while (buf != NULL) {
		if (didccr_file_parse_element(buf, &key, &data)) {
			free(buf);
			did_device_list_free(instlist);
			return (-1);
		}
		if ((key != NULL) && (data != NULL)) {
			if (did_ccr_str_to_instance(key, data, &instlist,
			    typelist)) {
				free(buf);
				did_device_list_free(instlist);
				return (-1);
			}
		}
		free(buf);
		if (didccr_file_next_element(fp, &buf)) {
			/* Errno is set. Don't unset it. */
			return (-1);
		}
	}
	*out = instlist;
	return (0);
}

static int
didccr_file_write_instance(FILE *fp, did_device_list_t *instptr)
{
	char	key[128];
	char	*data;

	(void) sprintf(key, "%d", instptr->instance);
	if (did_ccr_instance_to_str(instptr, &data)) {
		/* Errno is set. Don't unset it. */
		return (-1);
	}
	(void) fprintf(fp, "%s\t%s\n", key, data);

	return (0);
}

static int
didccr_file_write_instances(FILE *fp, did_device_list_t *instlist)
{
	did_device_list_t	*instptr;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (instlist == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Invalid input to %s.\n"),
			"didccr_file_write_instances");
		return (0);
	}

	for (instptr = instlist; instptr; instptr = instptr->next) {
		if (didccr_file_write_instance(fp, instptr)) {
			/* Errno is set. Don't unset it. */
			return (-1);
		}
	}

	return (0);
}

/*
 * Implementation of the didccr_file_interface methods.
 */

int
did_impl_getnodename(nodeid_t nodeid, char *nodename)
{
	FILE	*fp;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if ((fp = fopen(DEFAULT_CCR_INFRAFILE, "r")) == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Cannot open %s\n"),
			DEFAULT_CCR_INFRAFILE);
		return (-1);
	}
	if (didccr_file_read_nodename(fp, nodeid, nodename) != 0) {
		(void) fclose(fp);
		return (-1);
	}
	(void) fclose(fp);
	return (0);
}

/*
 * Map a nodename to a nodeid by looking in the ccr infrastructure file.
 * If the file cannot be opened - return '-2'.
 * If no match is found - return '-2'.
 * If an error occurred during parsing, return '-1'.
 * Return '0' for success. Filling-in the mapped nodeid.
 */
int
did_impl_getnodeid(char *nodename, nodeid_t *nodeid)
{
	FILE		*fp;
	nodeid_t	nodenum;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	for (nodenum = 1; nodenum <= NODEID_MAX; nodenum++) {
		if ((fp = fopen(DEFAULT_CCR_INFRAFILE, "r")) == NULL) {
			(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN, "Cannot open %s\n"),
				DEFAULT_CCR_INFRAFILE);
			return (-2);
		}
		switch (didccr_file_read_nodeid(fp, nodename, nodenum)) {
		case 0:
			/*
			 * Match was found.
			 */
			(void) fclose(fp);
			*nodeid = nodenum;
			return (0);

		case 1:
			/*
			 * No match. Try next nodeid.
			 */
			break;

		case -1:
		default:
			/*
			 * Error.
			 */
			(void) fclose(fp);
			return (-1);

		}
		(void) fclose(fp);
	}
	/*
	 * No Match.
	 */
	return (-2);
}

/* ARGSUSED */
int
did_impl_getinstlist(did_device_list_t **ptr, int mode,
	did_device_type_t *typelist, char *fmtfile)
{
	FILE	*fp;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (fmtfile == NULL) {
		fmtfile = (char *)malloc(MAXPATHLEN * sizeof (char));
		if (fmtfile != NULL)
			(void) strcpy(fmtfile, DEFAULT_CCR_INSTFILE);
		else {
			(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN, "Out of memory.\n"));
			return (-1);
		}
	}
	if ((fp = fopen(fmtfile, "r")) == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Cannot open %s.\n"), fmtfile);
		return (-1);
	}
	if (didccr_file_read_instances(fp, typelist, ptr)) {
		(void) fclose(fp);
		return (-1);
	}
	(void) fclose(fp);

	return (0);
}

int
did_impl_freeinstlist(did_device_list_t *ptr)
{
	if (ptr == NULL) {
		return (0);
	}
	did_device_list_free(ptr);
	return (0);
}

/*
 * This interface may need to be enhanced if the old didadm option of an
 * alternate configuration file is to be supported.
 */
/* ARGSUSED */
int
did_impl_saveinstlist(did_device_list_t *ptr, char *savefile, char *node,
	int flag)
{
	FILE		*fp;
	struct stat	buf;
	char		command[MAXPATHLEN];

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	flag = flag;

	if (savefile == NULL) {
		savefile = (char *)malloc(MAXPATHLEN * sizeof (char));
		if (savefile != NULL)
			(void) strcpy(savefile, DEFAULT_CCR_INSTFILE);
		else {
			(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN, "Out of memory.\n"));
			return (-1);
		}
	}

	/*
	 * We only want to save if the ccr file doesn't currently
	 * exist. This is because existing clusters shouldn't be
	 * reconfiguring devices in non-clustered mode. However, we
	 * do need this ability for upgrade's purposes.
	 */
	if (stat(savefile, &buf) == 0) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "File %s exists.\n"), savefile);

		/*
		 * Return a different error code so that upgrade
		 * can distinguish between a failure, and the fact
		 * that it has already been run.
		 */
		return (-2);
	}

	if ((fp = fopen(savefile, "w")) == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Cannot open %s\n"), savefile);
		return (-1);
	}

	if (didccr_file_write_instances(fp, ptr)) {
		(void) fclose(fp);
		return (-1);
	}

	if (fclose(fp)) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s: Could not close %s.\n"),
			"did_impl_saveinstlist", savefile);
		return (-1);
	}

	/* Create the CCR checksum. */
	(void) snprintf(command, sizeof (command), "%s %s -o", CCR_INIT,
	    savefile);
	if (system(command)) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Cannot initialize CCR instances "
			"file %s.\n"), savefile);
		return (-1);
	}

	/*
	 * XXX - Make sure that did_instances isn't already
	 * in the directory file.
	 */

	/* Add did_instances to directory file */
	fp = fopen(CCR_DIRECTORY, "a");
	if (fp == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Cannot open CCR directory file.\n"));
		return (-1);
	}

	(void) fprintf(fp, "did_instances\n");

	(void) fclose(fp);
	(void) snprintf(command, sizeof (command), "%s %s -o", CCR_INIT,
	    CCR_DIRECTORY);
	if (system(command)) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Cannot initialize CCR directory "
			"file %s.\n"), savefile);
		return (-1);
	}

	return (0);
}

/* ARGSUSED */
int
did_impl_saveinst(did_device_list_t *ptr, int create)
{
	/* Nothing to do */

	return (0);
}

/* ARGSUSED */
int
did_impl_read_repl_entry(int key, char **data)
{
	/* Nothing to do */

	return (0);
}

/*
 * Read the replicated_devices CCR table.
 *
 * Return 0 - success
 *        1 - no table or empty table
 *       -1 - error
 */
/* ARGSUSED */
int
did_impl_getrepllist(did_repl_list_t **ptr)
{
	FILE	*fp;
	int	retval;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if ((fp = fopen(DEFAULT_CCR_REPLFILE, "r")) == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Cannot open %s.\n"),
		    DEFAULT_CCR_REPLFILE);
		return (1);
	}
	retval = didccr_file_read_repls(fp, ptr);
	(void) fclose(fp);

	return (retval);
}

/* ARGSUSED */
int
did_impl_rm_repl_entry(int key)
{
	/* Nothing to do */

	return (0);
}

/* ARGSUSED */
int
did_impl_add_repl_entry(did_repl_list_t *repldata)
{
	/* Nothing to do */

	return (0);
}

/* ARGSUSED */
int
did_impl_allocinstance(did_subpath_t *subptr,
			did_device_list_t **instlist, char *node)
{
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
	    dgettext(TEXT_DOMAIN, "%s: Operation invalid.\n"),
		"did_impl_allocinstance");
	return (-1);
}

int
did_impl_gettypelist(did_device_type_t **list)
{
	FILE	*fp;
	char	*fmtfile = DEFAULT_CCR_TYPEFILE;

	if (did_typelist != NULL) {
		*list = did_typelist;
		return (0);
	}

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if ((fp = fopen(fmtfile, "r")) == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Cannot open %s.\n"), fmtfile);
		return (-1);
	}
	if (didccr_file_read_device_types(fp, list)) {
		(void) fclose(fp);
		return (-1);
	}
	(void) fclose(fp);
	did_typelist = *list;
	return (0);
}

/* ARGSUSED */
int
did_impl_savetypelist(did_device_type_t *list)
{
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
	    dgettext(TEXT_DOMAIN, "%s: Operation invalid.\n"),
		"did_impl_savetypelist");
	return (-1);
}

/* ARGSUSED */
int
did_impl_mapdidtodev(char *did_dev, did_subpath_t *dev_path)
{
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
	    dgettext(TEXT_DOMAIN, "%s: Operation invalid.\n"),
		"did_impl_mapdidtodev");
	return (-1);
}

/* ARGSUSED */
int
did_impl_mapdevtodid(char *did_dev, did_subpath_t *dev_path)
{
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
	    dgettext(TEXT_DOMAIN, "%s: Operation invalid.\n"),
		"did_impl_mapdevtodid");

	return (-1);
}

int
did_impl_exec_program_local(char *pgm)
{
	if (pgm == NULL)
		return (-1);

	return (system(pgm));
}

int
did_impl_syncgdevs()
{
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
	    dgettext(TEXT_DOMAIN, "%s: Operation invalid.\n"),
		"did_impl_syncgdevs");
	return (-1);
}

/* ARGSUSED */
int
did_impl_syncrepl(char *exec_node, char *command)
{
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
	    dgettext(TEXT_DOMAIN, "%s: Operation invalid.\n"),
		"did_impl_syncrepl");
	return (-1);
}

int
did_impl_initlibspecific()
{
	/* Nothing to do to specifically init the conf file. */

	return (0);
}

int
did_impl_holdccrlock(void)
{
	/* Nothing to do to specifically init the conf file. */

	return (0);
}

int
did_impl_releaseccrlock(int ret)
{
	/* Nothing to do to specifically init the conf file. */
	ret = ret;

	return (0);
}

int
did_impl_getinst(did_device_list_t **p, int ret)
{
	/* Nothing to do here */
	p = p;
	ret = ret;
	return (0);
}

int
did_impl_populate_list_cache(did_device_list_t *dlist)
{
	/* Nothing to do here */
	dlist = dlist;
	return (0);
}

int
did_impl_getglobalfencingstatus(unsigned short *status)
{
	/* Nothing to do here */
	*status = *status;
	return (0);
}

int
did_impl_setglobalfencingstatus(unsigned short newstatus)
{
	/* Nothing to do here */
	newstatus = newstatus;
	return (0);
}
