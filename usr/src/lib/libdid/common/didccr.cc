//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//


#pragma ident	"@(#)didccr.cc	1.65	08/05/21 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <nslib/ns.h>
#include <nslib/data_container_impl.h>
#include <nslib/ns_interface.h>
#include <h/naming.h>
#include <h/ccr.h>
#include <h/data_container.h>
#include <sys/stat.h>
#include <sys/didio.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/infrastructure/orb.h>
#include <h/quorum.h>
#include <sys/cladm_int.h>
#include <h/repl_pxfs.h>
#include <libintl.h>
#include "libdid.h"
#include "didprivate.h"
#include <sys/cladm_debug.h>

#define	DID_TYPE_TABLE			"did_types"

#define	SYNC_PROGRAM			"/usr/cluster/bin/scgdevs -S"

static int			didccr_open(void);
static void			didccr_close(void);
static int			get_nodeid_by_nodename(char *nodename);
static int			didccr_create_data_table(char *tablename);
static did_device_type_t	*didccr_read_device_types(void);
static int			didccr_read_instances(did_device_type_t *,
					did_device_list_t **out);
static int			didccr_write_instances(did_device_list_t *,
					char *node, int flag);
static int			didccr_begin_write_operation(char *tablename);
static int			didccr_end_write_operation(char *tablename);
static int			didccr_remove_instance(did_device_list_t
					*instptr);
static int			didccr_write_instance(did_device_list_t
					*instlist, int create_inst);
static int			didccr_merge_subpaths(did_device_list_t *from,
					did_device_list_t *to, char *node);
static int			didccr_new_instnum(did_device_type_t *type,
					did_device_list_t *instlist,
					did_device_list_t *batchlist);
static int			didccr_abort_write_operation(char *tablename);
static int			didccr_add_inst_to_seq(
					did_device_list_t *instptr,
					int create_inst, void *seq, int *len);
static int			didccr_remove_ns_entry(did_device_list_t *ptr);
static int			didccr_write_ns_entry(did_device_list_t *ptr);
static int			didccr_write_ns_entry(int newstatus);
static int			didccr_init_ns_map(const char *mapname);
static char			*didccr_devpath_to_device(char *);
static char			*didccr_devpath_to_nodename(char *);
static bool			didccr_ns_map_exists(const char *);

static	naming::naming_context_ptr 	ctxp = naming::naming_context::_nil();
static	ccr::directory_ptr		ccr_dir = ccr::directory::_nil();
static	ccr::updatable_table_ptr	ccr_writetable =
					    ccr::updatable_table::_nil();
static	ccr::updatable_table_ptr	ccr_fencing_writetable =
					    ccr::updatable_table::_nil();
static	did_device_type_t		*did_typelist = NULL;
static	bool				did_instance_cache_enabled = false;

static int
didccr_open(void)
{
	Environment		e;
	CORBA::Object_var	obj;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (!CORBA::is_nil(ctxp) && !CORBA::is_nil(ccr_dir)) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "CCR references are not NULL.\n"));
		return (-1);
	}

	didccr_close();

	ctxp = ns::local_nameserver();
	if (CORBA::is_nil(ctxp)) {
		/* Failed to bind to the nameserver */
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Cannot bind to nameserver.\n"));
		return (-1);
	}
	obj = ctxp->resolve("ccr_directory", e);
	if (e.exception()) {
		/* Failed to resolve ccr directory */
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Cannot resolve ccr_directory.\n"));
		e.clear();
		return (-1);
	}
	ccr_dir = ccr::directory::_narrow(obj);

	// Make sure instance table exists.
	if (didccr_create_data_table(DID_INST_TABLE) ||
	    didccr_create_data_table(GLOBAL_FENCING_TABLE)) {
		/* did_geterrorstr() is set. Don't unset it. */
		didccr_close();
		return (-1);
	}

	return (0);
}

static void
didccr_close(void)
{
	if (!CORBA::is_nil(ccr_writetable)) {
		CORBA::release(ccr_writetable);
	}
	ccr_writetable = ccr::updatable_table::_nil();
	if (!CORBA::is_nil(ccr_fencing_writetable)) {
		CORBA::release(ccr_fencing_writetable);
	}
	ccr_fencing_writetable = ccr::updatable_table::_nil();
	if (!CORBA::is_nil(ctxp)) {
		CORBA::release(ctxp);
	}
	ctxp = naming::naming_context::_nil();
	if (!CORBA::is_nil(ccr_dir)) {
		CORBA::release(ccr_dir);
	}
	ccr_dir = ccr::directory::_nil();
}

static int
didccr_create_data_table(char *tablename)
{
	Environment		e;
	CORBA::StringSeq_var	tnames;
	ccr::readonly_table_var	table;
	int			result = 0;
	CORBA::Exception	*ex;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (tablename == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
				dgettext(TEXT_DOMAIN, "NULL table name"));
			return (-1);
	}

	table = ccr_dir->lookup(tablename, e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::no_such_table::_exnarrow(ex)) {
			e.clear();
			result = 1;
		} else {
			e.clear();
			(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
				dgettext(TEXT_DOMAIN, "Cannot lookup %s.\n"),
				tablename);
			return (-1);
		}
	}

	if (result == 1) {
		// If it hasn't been created, then add the new table
		ccr_dir->create_table(tablename, e);
		if ((ex = e.exception()) != NULL) {
			if (ccr::table_exists::_exnarrow(ex)) {
				e.clear();
				// If someone else created the table already
				// this is good and not an error, so just fall
				// through.
			} else {
				e.clear();
				(void) snprintf(did_geterrorstr(),
				    DID_ERRORSTR_SIZE,
				    dgettext(TEXT_DOMAIN, "Cannot create %s."
					"\n"), tablename);
				return (-1);
			}
		}
	}
	return (0);
}

static did_device_type_t *
didccr_read_device_types(void)
{
	Environment		e;
	ccr::readonly_table_var	table;
	ccr::table_element_var	elem;
	did_device_type_t	*typelist = NULL;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	table = ccr_dir->lookup(DID_TYPE_TABLE, e);
	if (e.exception()) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Cannot lookup %s.\n"),
			DID_TYPE_TABLE);
		e.clear();
		return (NULL);
	}

	e.clear();
	table->atfirst(e);
	if (e.exception()) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Cannot read %s.\n"), DID_TYPE_TABLE);
		e.clear();
		return (NULL);
	}
	for (;;) {
		table->next_element(elem, e);
		if (e.exception()) {
			if (ccr::NoMoreElements::_exnarrow(e.exception())) {
				e.clear();
				// End of the table
				break;
			} else {
				(void) snprintf(did_geterrorstr(),
				    DID_ERRORSTR_SIZE,
				    dgettext(TEXT_DOMAIN, "Cannot read %s.\n"),
				    DID_TYPE_TABLE);
				e.clear();
				return (NULL);
			}
		}

		if (did_ccr_str_to_device_type((char *)elem->key,
			(char *)elem->data, &typelist)) {
			/* did_geterrorstr() is set. Don't unset it. */
			return (NULL);
		}
	}
	did_typelist = typelist;
	return (typelist);
}

static int
didccr_read_instances(did_device_type_t *typelist, did_device_list_t **out)
{
	Environment		e;
	ccr::readonly_table_var	table;
	ccr::table_element_var	elem;
	did_device_list_t	*instlist = NULL;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	table = ccr_dir->lookup(DID_INST_TABLE, e);
	if (e.exception()) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Cannot lookup %s.\n"),
			DID_INST_TABLE);
		e.clear();
		return (-1);
	}

	table->atfirst(e);
	if (e.exception()) {
		/* The table is empty. Return success. */
		e.clear();
		return (0);
	}

	for (;;) {
		table->next_element(elem, e);
		if (e.exception()) {
			if (ccr::NoMoreElements::_exnarrow(e.exception())) {
				// No more element in the table
				e.clear();
				break;
			} else {
				(void) snprintf(did_geterrorstr(),
				    DID_ERRORSTR_SIZE,
				    dgettext(TEXT_DOMAIN, "Cannot read table "
					"%s.\n"), DID_INST_TABLE);
				e.clear();
				return (-1);
			}
		}

		if (did_ccr_str_to_instance((char *)elem->key,
			(char *)elem->data, &instlist, typelist)) {
			/* did_geterrorstr() is set. Don't unset it. */
			did_device_list_free(instlist);
			return (-1);
		}
	}
	*out = instlist;
	return (0);
}

static int
didccr_read_globalfencingstatus(unsigned short *status)
{
	Environment		e;
	ccr::readonly_table_var	table;
	ccr::table_element_var	elem;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	table = ccr_dir->lookup(GLOBAL_FENCING_TABLE, e);
	if (e.exception()) {
		if (ccr::no_such_table::_exnarrow(e.exception())) {
			// Table doesn't exist.
			(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN, "Table %s doesn't exist.\n"),
			    GLOBAL_FENCING_TABLE);
			e.clear();
			return (-1);
		} else {
			(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN, "Cannot lookup %s.\n"),
			    GLOBAL_FENCING_TABLE);
			e.clear();
			return (-1);
		}
	}

	table->atfirst(e);
	if (e.exception()) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Can't read %s.\n"),
		    GLOBAL_FENCING_TABLE);
		e.clear();
		return (-1);
	}

	table->next_element(elem, e);
	if (e.exception()) {
		if (ccr::NoMoreElements::_exnarrow(e.exception())) {
			// Default to pathcount
			*status = GLOBAL_FENCING_PATHCOUNT;
			e.clear();
			return (0);
		} else {
			(void) snprintf(did_geterrorstr(),
			    DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN, "Cannot read %s.\n"),
			    GLOBAL_FENCING_TABLE);
			e.clear();
			return (-1);
		}
	}

	int data = atoi((char *)elem->data);
	*status = (unsigned short)data;
	if (data < 0 || *status > GLOBAL_NO_FENCING) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Invalid entry in %s.\n"),
		    GLOBAL_FENCING_TABLE);
		return (-1);
	}

	return (0);
}

static int
didccr_write_globalfencingstatus(unsigned short newstatus)
{
	Environment		e;
	char			*data = NULL, *key = NULL;
	ccr::element_seq	seq(1, 1);

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (didccr_begin_write_operation(GLOBAL_FENCING_TABLE) != 0) {
		return (-1);
	}

	ccr_fencing_writetable->remove_all_elements(e);
	if (e.exception()) {
		e.clear();
		/* Release our lock on the CCR */
		(void) didccr_abort_write_operation(GLOBAL_FENCING_TABLE);
		return (-1);
	}

	data = new char[5];
	key = os::strdup(GLOBAL_FENCING_KEY);
	if (data == NULL || key == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Out of memory.\n"));
		/* Release our lock on the CCR */
		delete [] data;
		delete [] key;
		(void) didccr_abort_write_operation(GLOBAL_FENCING_TABLE);
		return (-1);
	} else {
		os::sprintf(data, "%d", newstatus);
	}
	// The seq's destructer will delete key and data
	seq[0].key = key;
	seq[0].data = data;

	ccr_fencing_writetable->add_elements(seq, e);
	if (e.exception()) {
		e.clear();
		/* Release our lock on the CCR */
		(void) didccr_abort_write_operation(GLOBAL_FENCING_TABLE);
		return (-1);
	}

	if (didccr_write_ns_entry(newstatus) != 0) {
		(void) didccr_abort_write_operation(GLOBAL_FENCING_TABLE);
		return (-1);
	}

	if (didccr_end_write_operation(GLOBAL_FENCING_TABLE) != 0) {
		return (-1);
	}

	return (0);
}

static int
didccr_write_ns_entry(int newstatus)
{
	if (didccr_init_ns_map(GLOBAL_FENCING_MAP) == -1) {
		return (-1);
	}

	char	statusstr[32];
	(void) os::itoa(newstatus, statusstr);
	// Bind the new status with ns.
	if (cl_rebind(GLOBAL_FENCING_MAP, statusstr)) {
		(void) cl_unbind(GLOBAL_FENCING_MAP);
		(void) snprintf(did_geterrorstr(),
		    DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s failed to populate "
		    "the global fencing algorithm.\n"), "didccr_write_entry");
		return (-1);
	}
	return (0);
}

static int
didccr_write_instances(did_device_list_t *instlist, char *node, int flag)
{
	Environment			e;
	did_device_list_t		*instptr, *tmpinst, *newinst = NULL;
	did_device_list_t		*ccrinstlist = NULL, *ip;
	did_subpath_t			*subp, *prev_subp, *del_subp;
	did_subpath_t			*new_subp;
	CORBA::String_var		ccr_data;
	ccr::readonly_table_var		ccr_readtable;
	int				retval = 0;
	int				dirty = 0;	// table update required
	ccr::element_seq		*seq = new ccr::element_seq(0);
	int				len = 0;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (instlist == NULL) {
		delete seq;
		return (0);
	}

	if (did_typelist == NULL) {
		delete seq;
		return (-1);
	}

	/* Lock down the CCR. */
	if (didccr_begin_write_operation(DID_INST_TABLE) != 0) {
		delete seq;
		/* Errno is set. Don't unset it. */
		return (-1);
	}

	/* Take a copy of the current CCR */
	if (didccr_read_instances(did_typelist, &ccrinstlist)) {
		delete seq;
		/* Errno is set. Don't unset it. */
		(void) didccr_abort_write_operation(DID_INST_TABLE);
		return (-1);
	}

	/*
	 * If FORCE_UPDATE is set in flag, we will rewrite the CCR even
	 * if nothing appears to have changed.
	 */
	if (flag & FORCE_UPDATE) {
		dirty++;
	}
	for (instptr = ccrinstlist; instptr; instptr = instptr->next) {
		tmpinst = did_ccr_find_instance(instptr->instance, instlist);
		if (tmpinst != NULL) {
			int merged;

			if ((merged = didccr_merge_subpaths(instptr, tmpinst,
			    node)) < 0) {
				retval = -1;
			} else if (merged || instptr->default_fencing !=
			    tmpinst->default_fencing ||
			    instptr->detected_fencing !=
			    tmpinst->detected_fencing) {
				dirty++;
			}
		} else {
			/*
			 * In the following, if we find the CCR instance
			 * entry which isn't in our "instlist", and "node"
			 * is specified, we will remove it only the flag
			 * is set to REMOVABLE. Otherwise, leave it there.
			 */
			int changed = 0;
			for (subp = instptr->subpath_listp; subp;
			    subp = subp->next) {
				if (node == NULL) {
					if (changed == 0) {
						/* Add instance to instlist */
						changed = 1;
						newinst = (did_device_list_t *)
						    malloc(
						    sizeof (did_device_list_t));
						if (newinst == NULL) {
							(void) snprintf(
							    did_geterrorstr(),
							    DID_ERRORSTR_SIZE,
							    dgettext(
								TEXT_DOMAIN,
								"Out of "
								"memory.\n"));
							delete seq;
					/*lint -e525 */
					(void) didccr_abort_write_operation(
					    DID_INST_TABLE);
					/*lint +e525 */
							// Remove DID cache
							(void) cl_unbind(
							    DID_INST_TABLE);
							return (-1);
						}

						(void) memcpy(newinst, instptr,
						    sizeof (newinst));
						newinst->subpath_listp = NULL;

						newinst->diskid = (char *)
						    malloc(strlen(
						    instptr->diskid) + 1);
						if (newinst->diskid == NULL) {
							(void) snprintf(
							    did_geterrorstr(),
							    DID_ERRORSTR_SIZE,
							    dgettext(
								TEXT_DOMAIN,
							"Out of memory.\n"));
							delete seq;
					/*lint -e525 */
					(void) didccr_abort_write_operation(
					    DID_INST_TABLE);
					/*lint +e525 */
							// Remove DID cache
							(void) cl_unbind(
							    DID_INST_TABLE);
							return (-1);
						}

						(void) strcpy(newinst->diskid,
						    instptr->diskid);
						/* add to end of instlist */
						for (ip = instlist; ip->next;
							ip = ip->next) {};
						newinst->next = NULL;
						ip->next = newinst;
					}
					/* Add subpath to instlist */
					new_subp = (did_subpath_t *)malloc(
					    sizeof (did_subpath_t));
					if (new_subp == NULL) {
						(void) snprintf(
						    did_geterrorstr(),
						    DID_ERRORSTR_SIZE,
						    dgettext(TEXT_DOMAIN,
							"Out of memory.\n"));
						delete seq;
					/*lint -e725 */
					(void) didccr_abort_write_operation(
					    DID_INST_TABLE);
					/*lint +e725 */
						// Remove DID cache
						(void) cl_unbind(
						    DID_INST_TABLE);
						return (-1);
					}

					(void) memcpy(new_subp, subp,
					    sizeof (new_subp));

					new_subp->instptr = newinst;
					new_subp->next = newinst->subpath_listp;
					newinst->subpath_listp = new_subp;
				}

			}	// for loop
			//
			// If flag has REMOVABLE set, not adding
			// this instance to the seq is
			// equivalent to removing it since
			// we're re-writing the entire table.
			//
			if (!(flag & REMOVABLE)) {
				if (didccr_add_inst_to_seq(instptr, 1,
				    (void *) seq, &len))
					retval = -1;
			} else {
				//
				// We remove the entry from the did
				// instances cache so that it stays
				// consistent with the CCR tables
				//
				(void) didccr_remove_ns_entry(instptr);
				dirty++;
			}
		}
	}
	if (retval == -1) {
		delete seq;
		(void) didccr_abort_write_operation(DID_INST_TABLE);
		//
		// We delete the DID instance cache so that
		// it stays consistent with the CCR tables
		//
		(void) cl_unbind(DID_INST_TABLE);
		return (-1);
	}
	instptr = instlist;

	while (instptr) {
		tmpinst = did_ccr_find_instance(instptr->instance, ccrinstlist);

		if (tmpinst != NULL) {
			/*
			 * Continue if we found the instance in the CCR table,
			 * since we already merged the subpaths in the previous
			 * loop.
			 */
			if (didccr_add_inst_to_seq(instptr, 0,
			    (void *) seq, &len))
				retval = -1;
			instptr = instptr->next;
			continue;
		} else {
			/*
			 * Here we find didn't find an entry in the
			 * CCR table matching an entry in our instlist.
			 */
			subp = instptr->subpath_listp;
			prev_subp = NULL;

			while (subp) {
				if (node == NULL) {
					/* Remove the subpath */
					del_subp = subp;
					if (prev_subp)
						prev_subp->next = subp->next;
					if (instptr->subpath_listp == subp) {
						if (prev_subp)
							instptr->subpath_listp =
							    prev_subp;
						else
							instptr->subpath_listp =
							    subp->next;
					}
					subp = subp->next;
					free(del_subp);
				} else {
					/*
					 * If we find the inst isn't in CCR, we
					 * have to remove all non-local subpath
					 * since they are obsoleted.
					 */
					if (strncmp(subp->device_path, node,
					    strlen(node)) == 0) {
						prev_subp = subp;
						subp = subp->next;
						continue;
					}
					del_subp = subp;
					if (prev_subp)
						prev_subp->next = subp->next;
					if (instptr->subpath_listp == subp) {
						if (prev_subp)
							instptr->subpath_listp =
							    prev_subp;
						else
							instptr->subpath_listp =
							    subp->next;
					}
					subp = subp->next;
					free(del_subp);
				}
			}

			/*
			 * No need to worry about the instptr with no subpath
			 * since we are dealing with instptr not existing
			 * in CCR.
			 */
			if (instptr->subpath_listp != NULL) {
				dirty++;
				if (didccr_add_inst_to_seq(instptr, 1,
				    (void *) seq, &len))
					retval = -1;
			}
			instptr = instptr->next;
		}
	}
	if (retval == -1) {
		delete seq;
		(void) didccr_abort_write_operation(DID_INST_TABLE);
		//
		// We delete the DID instance cache so that
		// it stays consistent with the CCR tables
		//
		(void) cl_unbind(DID_INST_TABLE);
		return (-1);
	}
	//
	// Only (re)write the table if the dirty flag is set.
	//
	if (dirty) {
		//
		// First clear the table since we're writing the
		// entire list again.
		//
		ccr_writetable->remove_all_elements(e);
		if (e.exception()) {
			e.clear();
			delete seq;
			/* Release our lock on the CCR */
			(void) didccr_abort_write_operation(DID_INST_TABLE);
			//
			// We delete the DID instance cache so that
			// it stays consistent with the CCR tables
			//
			(void) cl_unbind(DID_INST_TABLE);
			return (-1);
		}
		ccr_writetable->add_elements(*seq, e);
		if (e.exception()) {
			e.clear();
			delete seq;
			/* Release our lock on the CCR */
			(void) didccr_abort_write_operation(DID_INST_TABLE);
			//
			// We delete the DID instance cache so that
			// it stays consistent with the CCR tables
			//
			(void) cl_unbind(DID_INST_TABLE);
			return (-1);
		}
	}
	delete seq;
	/* Release our lock on the CCR */
	if (didccr_end_write_operation(DID_INST_TABLE) != 0) {
		//
		// We delete the DID instance cache so that
		// it stays consistent with the CCR tables
		//
		(void) cl_unbind(DID_INST_TABLE);
		return (-1);
	}
	return (retval);
}

//
// Wrapper routines for use by libdid.
//

/*
 * Given a nodeid_t, return the nodename string it maps to.
 * If no mapping is found, return -1.
 *
 * Assumes cl_conf_lib_init() has previously been called in either
 * did_impl_saveinstlist() or did_impl_getinstlist().
 */
int
did_impl_getnodename(nodeid_t nodeid, char *nodename)
{
	clconf_get_nodename(nodeid, nodename);
	if (nodename[0] == '\0')
		return (-1);
	return (0);
}

/*
 * Given a nodename string, return the nodeid_t value it maps to.
 * If no mapping is found, return -1.
 *
 * Assumes cl_conf_lib_init() has previously been called in either
 * did_impl_saveinstlist() or did_impl_getinstlist().
 */
int
did_impl_getnodeid(char *nodename, nodeid_t *nodeid)
{
	clconf_cluster_t *clconf;
	clconf_iter_t *currnodesi, *nodesi = (clconf_iter_t *)0;
	clconf_obj_t *node;
	int id;
	const char *name;
	int retval = 0;

	clconf = clconf_cluster_get_current();
	if (clconf == NULL)
		return (-1);

	/*
	 * Iterate through the list of configured nodes
	 */
	nodesi = currnodesi = clconf_cluster_get_nodes(clconf);
	while ((node = clconf_iter_get_current(currnodesi)) != NULL) {

		id = clconf_obj_get_id(node);
		if (id == -1) {
			retval = -1;
			goto cleanup;
		}
		name = clconf_obj_get_name(node);
		/*
		 * search for a nodename match
		 */
		if (name != NULL && strcmp(nodename, name) == 0) {
			*nodeid = (nodeid_t)id;
			goto cleanup;
		}
		clconf_iter_advance(currnodesi);
	}

	/* The nodeid is not found */
	retval = -1;

cleanup:
	/* Release iterator */
	if (nodesi != (clconf_iter_t *)0)
		clconf_iter_release(nodesi);

	/* Release the clconf */
	clconf_obj_release((clconf_obj_t *)clconf);
	return (retval);
}

int
did_impl_getinstlist(did_device_list_t **ptr, int mode,
	did_device_type_t *typelist, char *fmtfile)
{
	did_device_list_t	*instlist = NULL;
	int			error;
	int			did_init = 0;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (fmtfile || mode)
		fmtfile = fmtfile;

	if (typelist == NULL) {
		(void) snprintf(did_geterrorstr(),
		    DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Invalid input to %s.\n"),
			"did_impl_getinstlist");
		return (-1);
	}

	if (CORBA::is_nil(ccr_dir)) {
		did_init = 1;
		if (didccr_open()) {
			/* Errno is set. Don't unset it. */
			return (-1);
		}
	}

	if ((error = clconf_lib_init()) != 0) {
		(void) snprintf(did_geterrorstr(),
		    DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s failed with %d.\n"),
			"did_initlibrary: clconf_lib_init", error);
		didccr_close();
		return (-1);
	}

	if (didccr_read_instances(typelist, &instlist)) {
		didccr_close();
		return (-1);
	}

	*ptr = instlist;
	if (did_init == 1)
		didccr_close();
	return (0);
}

/*
 * Read the replicated_devices CCR table.
 *
 * Return 0 - success
 *        1 - no table or empty table
 *       -1 - error
 */
int
did_impl_getrepllist(did_repl_list_t **ptr)
{
	Environment e;
	static	ccr::readonly_table_ptr ccr_repltable =
	    ccr::readonly_table::_nil();
	ccr::table_element_var repl_elem;
	did_repl_list_t *new_repl;
	int retval = 0;

	if (didccr_open()) {
		/* Errno is set. Don't unset it. */
		return (-1);
	}

	ccr_repltable = ccr_dir->lookup(DID_REPL_TABLE, e);
	if (e.exception()) {
		if (ccr::no_such_table::_exnarrow(e.exception())) {
			e.clear();
			didccr_close();
			return (1);
		} else {
			e.clear();
			(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN, "Cannot lookup %s.\n"),
			    DID_REPL_TABLE);
			didccr_close();
			return (-1);
		}
	}

	ccr_repltable->atfirst(e);
	if (e.exception()) {
		// the table is empty
		e.clear();
		CORBA::release(ccr_repltable);
		didccr_close();
		return (1);
	}

	for (;;) {
		ccr_repltable->next_element(repl_elem, e);
		if (e.exception()) {
			if (ccr::NoMoreElements::_exnarrow(e.exception())) {
				// No more element in the table
				e.clear();
				break;
			} else {
				(void) snprintf(did_geterrorstr(),
				    DID_ERRORSTR_SIZE, dgettext(TEXT_DOMAIN,
				    "Cannot read %s.\n"), DID_REPL_TABLE);
				e.clear();
				retval = -1;
				break;
			}
		}

		new_repl = (did_repl_list_t *)calloc(1,
		    sizeof (did_repl_list_t));
		if (new_repl == NULL) {
			(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN, "Out of memory.\n"));
			retval = -1;
			break;
		}

		new_repl->next = *ptr;
		*ptr = new_repl;
		if (did_repl_str_to_instance((char *)repl_elem->key,
		    (char *)repl_elem->data, new_repl) != 0) {
			retval = -1;
			break;
		}
	}

	if ((retval == -1) && (*ptr != NULL))
		did_repl_list_free(*ptr);

	CORBA::release(ccr_repltable);
	didccr_close();
	return (retval);
}

int
did_impl_freeinstlist(did_device_list_t *ptr)
{
	if (ptr == NULL) {
		return (0);
	}
	did_device_list_free(ptr);
	return (0);
}


int
did_impl_saveinstlist(did_device_list_t *ptr, char *savefile, char *node,
	int flag)
{
	int error;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (savefile)
		savefile = savefile;

	if (didccr_open()) {
		/* Errno is set. Don't unset it. */
		return (-1);
	}
	if ((error = clconf_lib_init()) != 0) {
		(void) snprintf(did_geterrorstr(),
		    DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s failed with %d.\n"),
			"did_initlibrary: clconf_lib_init", error);
		didccr_close();
		return (-1);
	}
	if (didccr_write_instances(ptr, node, flag)) {
		didccr_close();
		return (-1);
	}

	didccr_close();
	return (0);
}

int
didccr_merge_subpaths(did_device_list_t *from, did_device_list_t *to,
	char *node)
{
	did_subpath_t	*spf, *spt, *sp_tmp, *sp_prev;
	int merged = 0;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (from == NULL || to == NULL)
		return (0);

	for (spf = from->subpath_listp; spf; spf = spf->next) {
		for (spt = to->subpath_listp; spt; spt = spt->next) {
			if (strcmp(spf->device_path, spt->device_path) == 0)
				break;
		}

		/* If this is null, we didn't find this subpath in to */
		if (spt == NULL) {
			merged++;
			/*
			 * Check to see that this subpath isn't for the
			 * specified node. We should know better about the
			 * status of this node. This check is needed
			 * in order to allow for deletion of paths and nodes.
			 */
			if (node != NULL) {
				if (strncmp(spf->device_path, node,
				    strlen(node)) == 0) {
					continue;
				}
			}

			sp_tmp = (did_subpath_t *)
				malloc(sizeof (did_subpath_t));
			if ((spf != NULL) && (sp_tmp != NULL)) {
				(void) memcpy(sp_tmp, spf,
				    sizeof (did_subpath_t));
			} else {
				/*
				 * Fail, because we're either confused, or
				 * failed to malloc.
				 */
				(void) snprintf(did_geterrorstr(),
				    DID_ERRORSTR_SIZE,
				    dgettext(TEXT_DOMAIN, "Out of memory.\n"));
				return (-1);
			}
			sp_tmp->next = to->subpath_listp;
			sp_tmp->instptr = to;
			to->subpath_listp = sp_tmp;
		}
	}

	spt = to->subpath_listp;
	sp_prev = NULL;
	while (spt) {
		for (spf = from->subpath_listp; spf; spf = spf->next) {
			if (strcmp(spf->device_path, spt->device_path) == 0)
				break;
		}

		/* If this is null, we didn't find this subpath in from */
		if (spf == NULL) {
			merged++;
			/* Leave subpaths for specified node alone. */
			if (node != NULL) {
				if (strncmp(spt->device_path, node,
				    strlen(node)) == 0) {
					sp_prev = spt;
					spt = spt->next;
					continue;
				}
			}

			/* Remove subpath from to, since it no longer exists. */
			sp_tmp = spt;
			if (sp_prev)
				sp_prev->next = spt->next;
			if (to->subpath_listp == spt)
				if (sp_prev)
					to->subpath_listp = sp_prev;
				else
					to->subpath_listp = spt->next;
			spt = spt->next;
			free(sp_tmp);

		} else {
			sp_prev = spt;
			spt = spt->next;
		}
	}

	return (merged);
}

static int
didccr_begin_write_operation(char *tablename)
{
	Environment	e;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (strcmp(tablename, DID_INST_TABLE) == 0) {
		if (CORBA::is_nil(ccr_writetable)) {
			ccr_writetable = ccr_dir->begin_transaction(
			    tablename, e);
		}
	} else {
		ASSERT(strcmp(tablename, GLOBAL_FENCING_TABLE) == 0);
		if (CORBA::is_nil(ccr_fencing_writetable)) {
			ccr_fencing_writetable = ccr_dir->begin_transaction(
			    tablename, e);
		}
	}
	if (e.exception()) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE, dgettext(
		    TEXT_DOMAIN, "Cannot write to table %s.\n"), tablename);
		e.clear();
		return (-1);
	}
	return (0);
}

static int
didccr_end_write_operation(char *tablename)
{
	Environment	e;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (strcmp(tablename, DID_INST_TABLE) == 0) {
		if (!CORBA::is_nil(ccr_writetable)) {
			ccr_writetable->commit_transaction(e);
		}
	} else {
		ASSERT(strcmp(tablename, GLOBAL_FENCING_TABLE) == 0);
		if (!CORBA::is_nil(ccr_fencing_writetable)) {
			ccr_fencing_writetable->commit_transaction(e);
		}
	}

	if (e.exception()) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE, dgettext(
		    TEXT_DOMAIN, "Cannot complete write to table %s.\n"),
		    tablename);
		e.clear();
		return (-1);
	}
	return (0);
}

static int
intcompare(const void *p1, const void *p2)
{
	int i = *((int *)p1);
	int j = *((int *)p2);

	if (i > j)
		return (1);
	if (i < j)
		return (-1);
	return (0);
}

static int
didccr_new_instnum(did_device_type_t *type,
    did_device_list_t *instlist, did_device_list_t *batchlist)
{
	int			instance;
	did_device_list_t	*instp = instlist;
	did_device_list_t	*batchp = batchlist;
	int			sortlist[DID_MAX_INSTANCE];
	int			count = 0, listsize = 0;
	int			*location;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (type == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Device type not specified "
			"(didccr_new_instnum).\n"), type->type_name);
		return (-1);
	}

	instance = type->start_default_nodes;

	/*
	 * No guarantee that the list is ordered. So, we need to
	 * figure out the gaps manually. We'll use quicksort.
	 * First, put instances into an array.
	 */
	while (instp) {
		sortlist[listsize] = instp->instance;

		instp = instp->next;
		listsize++;
	}
	/*
	 * Now check the batchlist for already allocated instance numbers
	 * of batched instances waiting to be written out.
	 */
	while (batchp) {
		if (batchp->instance > 0) {
			sortlist[listsize] = batchp->instance;
			listsize++;
		}
		batchp = batchp->next;
	}
	/* Now quicksort the array */
	qsort((void *)sortlist, (size_t)listsize, sizeof (int), intcompare);

	/* Search for the starting instance number. */
	location = (int *)bsearch(&type->start_default_nodes,
	    (void *)sortlist, (size_t)listsize, sizeof (int), intcompare);

	/* Didn't find the starting instance. Use that. */
	if (location == NULL)
		return (instance);

	count = location - sortlist;

	/*
	 * We know we found the starting instance. Now look
	 * for instance number holes in the appropriate direction.
	 */
	if (type->next_default_node > 0) {

		while (count < listsize && count >= 0) {
			if (instance < sortlist[count])
				return (instance);
			instance = instance + type->next_default_node;
			count++;
		}

		if (count > 0) {
			if ((sortlist[count-1] + type->next_default_node) <=
			    DID_MAX_INSTANCE) {
				instance = sortlist[count-1] +
				    type->next_default_node;
				return (instance);
			}
		}
	} else if (type->next_default_node < 0) {
		while (count < listsize && count >= 0) {
			if (instance > sortlist[count])
				return (instance);
			instance = instance + type->next_default_node;
			count--;
		}
		if ((sortlist[count+1] + type->next_default_node) >=
		    DID_MIN_INSTANCE) {
			instance = sortlist[count+1] + type->next_default_node;
			return (instance);
		}
	} else {
		/* type->next_default node is 0! Return an error. */
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Device type %s has next default "
			"node value of %d.\n"), type->type_name,
			type->next_default_node);
		return (-1);
	}

	/* Error if we got here. This means we couldn't find an instance. */
	(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
	    dgettext(TEXT_DOMAIN, "Could not allocate a new instance. "
		"Out of instance numbers.\n"));
	return (-1);
}

static void
didccr_add_element(const char *key, const char *data,
    ccr::element_seq *seq, uint_t len)
{
	seq->length(len+1);
	(*seq)[len].key = key;
	(*seq)[len].data = data;
}

static int
didccr_write_instance(did_device_list_t *instptr, int create_inst)
{
	Environment	e;
	char		key[128];
	char		*data = NULL;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/*
	 * If there is no subpath to this instance, remove it from CCR.
	 */
	if (!create_inst && instptr->subpath_listp == NULL)
		if (didccr_remove_instance(instptr))
			return (-1);
		else
			return (0);

	(void) sprintf(key, "%d", instptr->instance);
	if (did_ccr_instance_to_str(instptr, &data)) {
		/* Errno is set. Don't unset it. */
		return (-1);
	}

	if (create_inst)
		ccr_writetable->add_element(key, data, e);
	else
		ccr_writetable->update_element(key, data, e);
	if (e.exception()) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Cannot write to table %s\n"),
			DID_INST_TABLE);
		e.clear();
		return (-1);
	}

	//
	// Update the did instance cache for the specified
	// instance. We ignore the error condition since
	// didccr_write_ns_entry will have already removed
	// the DID instance cache.
	//
	(void) didccr_write_ns_entry(instptr);
	return (0);
}

static int
didccr_add_inst_to_seq(did_device_list_t *instptr, int create_inst,
    void *seq, int *len)
{
	Environment	e;
	char		key[128];
	char		*data = NULL;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/*
	 * If there is no subpath to this instance, remove it from CCR.
	 */
	if (!create_inst && instptr->subpath_listp == NULL) {
		//
		// Not adding it to seq is effectively removing it.
		// We only need to update the DID instance cache to
		// reflect the change and maintain consistency
		//
		(void) didccr_remove_ns_entry(instptr);
		return (0);
	}

	(void) sprintf(key, "%d", instptr->instance);
	if (did_ccr_instance_to_str(instptr, &data)) {
		/* Errno is set. Don't unset it. */
		return (-1);
	}
	didccr_add_element(key, data, (ccr::element_seq *)seq, (uint_t)*len);
	(*len)++;
	//
	// Update the did instance cache for the specified
	// instance. We ignore the error condition since
	// didccr_write_ns_entry will have already removed
	// the DID instance cache.
	//
	(void) didccr_write_ns_entry(instptr);
	return (0);
}

static int
didccr_remove_instance(did_device_list_t *instptr)
{
	Environment	e;
	char		key[128];

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	(void) sprintf(key, "%d", instptr->instance);

	ccr_writetable->remove_element(key, e);
	if (e.exception()) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Cannot write to table %s %s\n"),
			"(didccr_remove_instance)", DID_INST_TABLE);
		e.clear();
		return (-1);
	}

	//
	// Remove the instance information from the DID instance cache
	//
	(void) didccr_remove_ns_entry(instptr);
	return (0);
}

int
did_impl_allocinstance(did_subpath_t *subptr, did_device_list_t **instlist,
	char *node)
{
	int			instnum;
	char			*newdiskid = NULL;
	did_device_list_t	*ccrinstlist = NULL, *ccrinstptr, *ip;
	did_device_list_t	*instptr = subptr->instptr;
	Environment		e;
	int			retval = 0;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (instptr == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Invalid parameter to %s.\n"),
			"did_impl_allocinstance");
		return (-1);
	}

	if (did_typelist == NULL) {
		return (-1);
	}

	if (didccr_open()) {
		/* Errno is set. Don't unset it. */
		return (-1);
	}

	if (didccr_begin_write_operation(DID_INST_TABLE) != 0) {
		didccr_close();
		return (-1);
	}

	//
	// Check if an instance for this disk is already allocated.
	//
	ccrinstptr = NULL;
	if (didccr_read_instances(did_typelist, &ccrinstlist)) {
		/* Errno is set. Don't unset it. */
		(void) didccr_abort_write_operation(DID_INST_TABLE);
		didccr_close();
		return (-1);
	}

	if (did_diskidtostr(instptr->diskid, instptr->diskid_len, &newdiskid)) {
		/* Errno is set. Don't unset it. */
		(void) didccr_abort_write_operation(DID_INST_TABLE);
		didccr_close();
		return (-1);
	}
	/* From here, newdiskid is not NULL */

	if (strcmp(newdiskid, "0") != 0) {
		for (ip = ccrinstlist; ip; ip = ip->next) {
			char *id = NULL;
			if (did_diskidtostr(ip->diskid, ip->diskid_len, &id)) {
				/* Errno is set. Don't unset it. */
				(void) didccr_abort_write_operation(
				    DID_INST_TABLE);
				didccr_close();
				free(newdiskid);
				return (-1);
			}
			/* From here, id is not NULL */
			if (strcmp(newdiskid, id) == 0) {
				ccrinstptr = ip;
				free(id);
				break;
			}
			free(id);
		}
	}
	free(newdiskid);

	if (ccrinstptr == NULL) {
		instnum = didccr_new_instnum(instptr->device_type,
			ccrinstlist, NULL);
		if (instnum < 0) {
			did_device_list_free(ccrinstlist);
			(void) didccr_abort_write_operation(DID_INST_TABLE);
			didccr_close();
			return (-1);
		}
		instptr->instance = instnum;
		if (didccr_write_instance(instptr, 1)) {
			retval = -1;
		}
	} else {
		if (didccr_merge_subpaths(ccrinstptr, instptr, node) < 0) {
			retval = -1;
		} else {
			instptr->instance = ccrinstptr->instance;
			if (didccr_write_instance(instptr, 0))
				retval = -1;
		}
	}
	did_device_list_free(ccrinstlist);

	if (didccr_end_write_operation(DID_INST_TABLE) != 0) {
		/* Errno is set. Don't unset it. */
		return (-1);
	}
	instptr->next = *instlist;
	*instlist = instptr;
	didccr_close();
	return (retval);
}

int
did_impl_allocinstances(did_device_list_t *alloclist,
	did_device_list_t **instlist, char *node)
{
	int			instnum;
	char			*newdiskid = NULL;
	did_device_list_t	*ccrinstlist = NULL, *ccrinstptr, *ip;
	did_device_list_t	*instptr;
	Environment		e;
	ccr::element_seq	*seq = new ccr::element_seq(0);
	int			len = 0;
	int			retval = 0;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (alloclist == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Invalid parameter to %s.\n"),
			"did_impl_allocinstances");
		delete seq;
		return (-1);
	}

	if (did_typelist == NULL) {
		delete seq;
		return (-1);
	}

	if (didccr_open()) {
		/* Errno is set. Don't unset it. */
		delete seq;
		return (-1);
	}

	if (didccr_begin_write_operation(DID_INST_TABLE) != 0) {
		didccr_close();
		delete seq;
		return (-1);
	}

	//
	// Check if an instance for each disk is already allocated.
	//
	if (didccr_read_instances(did_typelist, &ccrinstlist)) {
		/* Errno is set. Don't unset it. */
		(void) didccr_abort_write_operation(DID_INST_TABLE);
		didccr_close();
		delete seq;
		return (-1);
	}
	for (instptr = alloclist; instptr; instptr = instptr->next) {
		ccrinstptr = NULL;
		if (did_diskidtostr(instptr->diskid, instptr->diskid_len,
		    &newdiskid)) {
			/* Errno is set. Don't unset it. */
			(void) didccr_abort_write_operation(DID_INST_TABLE);
			didccr_close();
			delete seq;
			return (-1);
		}
		/* From here, newdiskid is not NULL */
		if (strcmp(newdiskid, "0") != 0) {
			for (ip = ccrinstlist; ip; ip = ip->next) {
				char *id = NULL;
				if (did_diskidtostr(ip->diskid,
				    ip->diskid_len, &id)) {
					/* Errno is set. Don't unset it. */
					(void) didccr_abort_write_operation(
					    DID_INST_TABLE);
					didccr_close();
					delete seq;
					free(newdiskid);
					return (-1);
				}
				if (strcmp(newdiskid, id) == 0) {
					ccrinstptr = ip;
					free(id);
					break;
				}
				free(id);
			}
		}
		free(newdiskid);

		if (ccrinstptr == NULL) {
			instnum = didccr_new_instnum(instptr->device_type,
			    ccrinstlist, alloclist);
			if (instnum < 0) {
				(void) didccr_abort_write_operation(
				    DID_INST_TABLE);
				did_device_list_free(ccrinstlist);
				didccr_close();
				delete seq;
				return (-1);
			}
			instptr->instance = instnum;
			if (didccr_add_inst_to_seq(instptr, 1,
			    (void *) seq, &len))
				retval = -1;
		} else {
			//
			// merge the subpaths into alloclist entry.
			//
			if (didccr_merge_subpaths(ccrinstptr, instptr,
			    node) < 0) {
				retval = -1;
			} else {
				//
				// Mark this alloclist entry as having
				// an instance already allocated.
				//
				instptr->instance = (ccrinstptr->instance * -1);
			}
		}
	}
	//
	// Now add the entire ccrinstlist to what will be written, since
	// we're about to clear and re-write the entire table.
	//
	for (instptr = ccrinstlist; instptr; instptr = instptr->next) {
		if (didccr_add_inst_to_seq(instptr, 1,
		    (void *) seq, &len))
			retval = -1;
	}
	did_device_list_free(ccrinstlist);
	if (retval == -1) {
		/* Release our lock on the CCR */
		(void) didccr_abort_write_operation(DID_INST_TABLE);
		didccr_close();
		// Remove DID cache
		(void) cl_unbind(DID_INST_TABLE);
		delete seq;
		return (-1);
	}
	//
	// First clear the table since we're writing the entire list again.
	//
	ccr_writetable->remove_all_elements(e);
	if (e.exception()) {
		e.clear();
		/* Release our lock on the CCR */
		(void) didccr_abort_write_operation(DID_INST_TABLE);
		didccr_close();
		// Remove DID cache
		(void) cl_unbind(DID_INST_TABLE);
		delete seq;
		return (-1);
	}
	ccr_writetable->add_elements(*seq, e);
	if (e.exception()) {
		e.clear();
		(void) didccr_abort_write_operation(DID_INST_TABLE);
		didccr_close();
		delete seq;
		// Remove DID cache
		(void) cl_unbind(DID_INST_TABLE);
		return (-1);
	}
	delete seq;
	if (didccr_end_write_operation(DID_INST_TABLE) != 0) {
		/* Errno is set. Don't unset it. */
		didccr_close();
		// Remove DID cache
		(void) cl_unbind(DID_INST_TABLE);
		return (-1);
	}
	//
	// append the instlist to the alloclist.
	//
	instptr = alloclist;
	while (instptr->next != NULL)
		instptr = instptr->next;
	instptr->next = *instlist;
	*instlist = alloclist;
	didccr_close();
	return (0);
}

int
did_impl_gettypelist(did_device_type_t **list)
{
	did_device_type_t	*typelist;

	if (did_typelist != NULL) {
		*list = did_typelist;
		return (0);
	}

	if (didccr_open()) {
		/* Errno is set. Don't unset it. */
		return (-1);
	}

	if ((typelist = didccr_read_device_types()) == NULL) {
		didccr_close();
		return (-1);
	}

	*list = typelist;
	didccr_close();
	return (0);
}

int
did_impl_savetypelist(did_device_type_t *)
{

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
	    dgettext(TEXT_DOMAIN, "%s: Operation invalid.\n"),
		"did_impl_savetypelist");
	return (-1);
}

int
did_impl_mapdidtodev(char *, did_subpath_t *)
{
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
	    dgettext(TEXT_DOMAIN, "%s: Operation invalid.\n"),
		"did_impl_mapdidtodev");
	return (-1);
}

int
did_impl_mapdevtodid(char *, did_subpath_t *)
{
	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
	    dgettext(TEXT_DOMAIN, "%s: Operation invalid.\n"),
		"did_impl_mapdevtodid");
	return (-1);
}

int
did_impl_exec_program_local(char *pgm)
{
	nodeid_t myid;

	if (pgm == NULL)
		return (-1);

	// get my node id
	if (os::cladm(CL_CONFIG, CL_NODEID, &myid) != 0)
		return (-1);

	return (clconf_do_execution(pgm, myid, NULL, B_FALSE, B_TRUE, B_TRUE));
}

//
// This routine will execute SYNC_PROGRAM on all nodes currently in the cluster.
//
int
did_impl_syncgdevs()
{
	quorum_status_t			*qp;
	CORBA::Object_var		obj;
	repl_pxfs::ha_mounter_var	mounter;
	Environment			e;
	char				name[20];
	int				err;
	int				bootflags;
	cladmin_log_t			*tolog;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if ((err = os::cladm(CL_INITIALIZE, CL_GET_BOOTFLAG, &bootflags))
	    != 0) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Cannot determine cluster boot mode."
			"\n"));
		return (-1);
	}

	if ((bootflags & (CLUSTER_CONFIGURED | CLUSTER_BOOTED)) !=
	    (CLUSTER_CONFIGURED | CLUSTER_BOOTED)) {
		// We are not booted as a cluster - leave.
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Not in cluster mode. Operation "
			"invalid.\n"));
		return (-1);
	}

	// Get a handle to the nameserver.
	naming::naming_context_var context = ns::root_nameserver();
	if (CORBA::is_nil(context)) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Cannot communicate with other "
			"cluster nodes.\n"));
		return (-1);
	}

	//
	// Get the list of active nodes.
	// XXX Note that this call should be substituted by a more appropriate
	// call in orb_conf:: when it becomes available.
	//
	err = cluster_get_quorum_status(&qp);
	if (err != 0) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Cannot communicate with other "
			"cluster nodes.\n"));
		return (-1);
	}

	tolog = (cladmin_log_t *)malloc(sizeof (cladmin_log_t));
	// tolog NULL is handled below

	// Iterate through the list of nodes.
	for (uint_t i = 0; i < qp->num_nodes; i++) {
		// Skip over the nodes that are not configured.
		if (!orb_conf::node_configured(qp->nodelist[i].nid))
			continue;

		// Skip this node.
		if (qp->nodelist[i].nid == orb_conf::node_number())
			continue;

		// Skip over the nodes that are down.
		if (qp->nodelist[i].state != quorum::QUORUM_STATE_ONLINE)
			continue;

		//
		// If we got to here, then 'gp->nodelist[i].nid' represents a
		// node that is up.
		//

		//
		// Get the 'ha_mounter' object for the node.
		// XXX Note that the ha_mounter object for the node could have
		// not started up yet, or could be in the process of being
		// restarted by the process monitor - hence, we should handle
		// not finding the 'ha_mounter' object for the node much more
		// carefully than we do below.
		//

		(void) sprintf(name, "ha_mounter.%d", qp->nodelist[i].nid);
		obj = context->resolve(name, e);
		if ((CORBA::is_nil(obj)) || (e.exception() != NULL)) {
			if (tolog != NULL) {
				tolog->level = CLADM_RED;
				(void) snprintf(tolog->str,
				    CLADMIN_LOG_SIZE - 1,
				    "did_impl_syncgdevs:%s not resolved\n",
				    name);
				(void) os::cladm(CL_CONFIG, CL_ADMIN_LOG,
				    (void *)tolog);
			}
			if (e.exception() != NULL) {
				CLEXEC_EXCEPTION(e, "did_impl_syncgdevs",
				    "resolve");
				e.clear();
			}
			continue;
		}
		mounter = repl_pxfs::ha_mounter::_narrow(obj);

		if (CORBA::is_nil(mounter)) {
			if (tolog != NULL) {
				tolog->level = CLADM_RED;
				(void) snprintf(tolog->str,
				    CLADMIN_LOG_SIZE - 1,
				    "did_impl_syncgdevs:%s not narrowed\n",
				    name);
				(void) os::cladm(CL_CONFIG, CL_ADMIN_LOG,
				    (void *)tolog);
			}
			continue;
		}

		if (tolog != NULL) {
			tolog->level = CLADM_GREEN;
			(void) snprintf(tolog->str, CLADMIN_LOG_SIZE - 1,
			    "did_impl_syncgdevs :%s exec_program %s\n",
			    name, SYNC_PROGRAM);
			(void) os::cladm(CL_CONFIG,
			    CL_ADMIN_LOG, (void *)tolog);
		}

		mounter->exec_program(SYNC_PROGRAM, e);

		if (tolog != NULL) {
			tolog->level = CLADM_GREEN;
			(void) snprintf(tolog->str, CLADMIN_LOG_SIZE - 1,
			    "did_impl_syncgdevs:%s exec_program %s excep %p\n",
			    name, SYNC_PROGRAM, e.exception());
			(void) os::cladm(CL_CONFIG, CL_ADMIN_LOG,
			    (void *)tolog);
		}

		if (e.exception()) {
			CLEXEC_EXCEPTION(e, "did_impl_syncgdevs", name);
			e.clear();
			continue;
		}
	}

	if (tolog != NULL) {
		free(tolog);
	}

	return (0);

}

static int
get_nodeid_by_nodename(char *nodename)
{
	int i;
	const char *tempname;
	clconf_cluster_t *cl;

	cl = clconf_cluster_get_current();

	for (i = 0; i < NODEID_MAX + 1; i++) {
		tempname = clconf_cluster_get_nodename_by_nodeid(cl,
		    (nodeid_t)i);

		if (tempname == NULL)
			continue;

		if (strcmp(tempname, nodename) == 0)
			return (i);
	}

	// didn't find match
	return (-1);
}

//
// This routine will execute REPL_SYNC_PROGRAM on all nodes in the cluster.
//
int
did_impl_syncrepl(char *exec_node, char *command)
{
	CORBA::Object_var		obj;
	repl_pxfs::ha_mounter_var	mounter;
	Environment			e;
	char				name[20];
	cladmin_log_t			*tolog;
	int				exec_node_id;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	// Get a handle to the nameserver.
	naming::naming_context_var context = ns::root_nameserver();
	if (CORBA::is_nil(context)) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Cannot communicate with other "
			"cluster nodes.\n"));
		return (-1);
	}

	tolog = (cladmin_log_t *)malloc(sizeof (cladmin_log_t));
	// tolog NULL is handled below

	//
	// Get the 'ha_mounter' object for the node.
	// XXX Note that the ha_mounter object for the node could have
	// not started up yet, or could be in the process of being
	// restarted by the process monitor - hence, we should handle
	// not finding the 'ha_mounter' object for the node much more
	// carefully than we do below.
	//
	exec_node_id = get_nodeid_by_nodename(exec_node);
	if (exec_node_id == -1) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Cannot get nodeid for %s\n"),
		    exec_node);
		return (-1);
	}
	(void) sprintf(name, "ha_mounter.%d", exec_node_id);
	obj = context->resolve(name, e);
	if ((CORBA::is_nil(obj)) || (e.exception() != NULL)) {
		if (tolog != NULL) {
			tolog->level = CLADM_RED;
			(void) snprintf(tolog->str, CLADMIN_LOG_SIZE - 1,
			    "did_impl_syncrepl:%s not resolved\n", name);
			(void) os::cladm(CL_CONFIG, CL_ADMIN_LOG,
			    (void *)tolog);
		}
		if (e.exception() != NULL) {
			CLEXEC_EXCEPTION(e, "did_impl_syncrepl", "resolve");
			e.clear();
		}
		return (-1);
	}
	mounter = repl_pxfs::ha_mounter::_narrow(obj);

	if (CORBA::is_nil(mounter)) {
		if (tolog != NULL) {
			tolog->level = CLADM_RED;
			(void) snprintf(tolog->str, CLADMIN_LOG_SIZE - 1,
			    "did_impl_syncrepl:%s not narrowed\n", name);
			(void) os::cladm(CL_CONFIG, CL_ADMIN_LOG,
			    (void *)tolog);
		}
		return (-1);
	}

	if (tolog != NULL) {
		tolog->level = CLADM_GREEN;
		(void) snprintf(tolog->str, CLADMIN_LOG_SIZE - 1,
		    "did_impl_syncrepl :%s exec_program %s\n", name,
		    command);
		(void) os::cladm(CL_CONFIG, CL_ADMIN_LOG, (void *)tolog);
	}

	mounter->exec_program(command, e);

	if (tolog != NULL) {
		tolog->level = CLADM_GREEN;
		(void) snprintf(tolog->str, CLADMIN_LOG_SIZE - 1,
		    "did_impl_syncrepl:%s exec_program %s excep %p\n", name,
		    command, e.exception());
		(void) os::cladm(CL_CONFIG, CL_ADMIN_LOG, (void *)tolog);
	}

	if (e.exception()) {
		CLEXEC_EXCEPTION(e, "did_impl_syncrepl", name);
		e.clear();
	}

	if (tolog != NULL) {
		free(tolog);
	}

	return (0);

}

int
did_impl_initlibspecific()
{

	if (ORB::initialize() != 0) {
		return (-1);
	}

	(void) didccr_init_ns_map(GLOBAL_FENCING_MAP);

	//
	// Create the top level context for the DID instance
	// cache. If it fails then the cache will remain
	// disabled
	//
	if (didccr_init_ns_map(DID_INST_TABLE) == 0)
		did_instance_cache_enabled = true;

	return (0);
}

/*
 * Used to grab DID CCR lock.
 */
int
did_impl_holdccrlock(void)
{
	int	error = 0;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (didccr_open())
		return (-1);

	if ((error = clconf_lib_init()) != 0) {
		(void) snprintf(did_geterrorstr(),
		    DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s failed with %d.\n"),
			"did_initlibrary: clconf_lib_init", error);
		didccr_close();
		return (-1);
	}

	/* Lock down the CCR. */
	if (didccr_begin_write_operation(DID_INST_TABLE) != 0) {
		/* Errno is set. Don't unset it. */
		didccr_close();
		return (-1);
	}

	return (0);
}

/*
 * If ret != 0, it will commit the write operation and release CCR lock.
 * Otherwise, it abort the write operation and release the lock.
 */
int
did_impl_releaseccrlock(int ret)
{

	if (ret) {
		/* Release our lock on the CCR */
		if (didccr_end_write_operation(DID_INST_TABLE) != 0) {
			didccr_close();
			return (-1);
		}
	} else {
		if (didccr_abort_write_operation(DID_INST_TABLE) != 0) {
			didccr_close();
			return (-1);
		}
	}
	didccr_close();
	return (0);
}

/*
 * This interface will be used to update one did instance entry
 */
int
did_impl_saveinst(did_device_list_t *ptr, int creates)
{
	/*
	 * Since the caller of the function is supposed to grap
	 * the CCR lock, we don't bother to do the lock.
	 * Nor does the CCR open/close.
	 */
	if (didccr_write_instance(ptr, creates)) {
		return (-1);
	}
	return (0);
}

/*
 * This interface will be used to read an entry from replicated_devices.
 * Data for the key is returned in 'data' if 'data' is non-NULL.
 *
 * Return values:
 *			1  - no repl table or no matching key
 *			0  - successfully read the key
 *			-1 - error
 *
 */
int
did_impl_read_repl_entry(int inkey, char **data)
{
	Environment e;
	char key[128];
	static	ccr::readonly_table_ptr ccr_repltable =
	    ccr::readonly_table::_nil();
	int did_init = 0;
	int retval = 0;

	(void) sprintf(key, "%d", inkey);

	if (CORBA::is_nil(ccr_dir)) {
		did_init = 1;
		if (didccr_open()) {
			/* Errno is set. Don't unset it. */
			retval = -1;
			goto out;
		}
	}

	ccr_repltable = ccr_dir->lookup(DID_REPL_TABLE, e);
	if (e.exception()) {
		if (ccr::no_such_table::_exnarrow(e.exception())) {
			e.clear();
			retval = 1;
			goto out;
		} else {
			e.clear();
			(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN, "Cannot lookup %s.\n"),
			    DID_REPL_TABLE);
			retval = 1;
			goto out;
		}
	}

	if (data != NULL)
		*data = ccr_repltable->query_element(key, e);
	else
		(void) ccr_repltable->query_element(key, e);
	if (e.exception()) {
		if (ccr::no_such_key::_exnarrow(e.exception())) {
			e.clear();
			CORBA::release(ccr_repltable);
			retval = 1;
			goto out;
		} else {
			e.clear();
			(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN, "Cannot lookup key %d.\n"),
			    inkey);
			CORBA::release(ccr_repltable);
			retval = 1;
			goto out;
		}
	}

	CORBA::release(ccr_repltable);

out:	if (did_init == 1)
		didccr_close();

	return (retval);
}

/*
 * This interface will be used to remove an entry from replicated_devices
 * Return values:
 *			0  - success
 *			-1 - failure
 */
int
did_impl_rm_repl_entry(int rmkey)
{
	Environment e;
	char key[128];
	static	ccr::updatable_table_ptr ccr_repltable =
	    ccr::updatable_table::_nil();

	(void) sprintf(key, "%d", rmkey);

	ccr_repltable = ccr_dir->begin_transaction(DID_REPL_TABLE, e);
	if (e.exception()) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Cannot write to table %s.\n"),
		    DID_REPL_TABLE);
		e.clear();
		CORBA::release(ccr_repltable);
		return (-1);
	}

	ccr_repltable->remove_element(key, e);
	if (e.exception()) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Cannot remove entry %d from table "
		    "%s\n"), rmkey, DID_REPL_TABLE);
		e.clear();
		ccr_repltable->abort_transaction(e);
		if (e.exception())
			// This exception isn't important.
			e.clear();
		CORBA::release(ccr_repltable);
		return (-1);
	}

	ccr_repltable->commit_transaction(e);
	if (e.exception()) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Cannot complete removal of %d from "
		    "table %s.\n"), rmkey, DID_REPL_TABLE);
		e.clear();
		ccr_repltable->abort_transaction(e);
		if (e.exception())
			// This exception isn't important.
			e.clear();
		CORBA::release(ccr_repltable);
		return (-1);
	}

	CORBA::release(ccr_repltable);

	return (0);
}

/*
 * This interface will be used to add an entry to replicated_devices
 *
 * Return values:
 *			0  - success
 * 			-1 - failure
 *			-2 - device is already marked as replicated
 */
int
did_impl_add_repl_entry(did_repl_list_t *repldata)
{
	int retval;
	Environment e;
	char key[MAXNAMELEN];
	char data[MAXNAMELEN];
	static	ccr::updatable_table_ptr ccr_repltable =
	    ccr::updatable_table::_nil();
	did_repl_path_t *path;
	int first_path = 1;
	char *paths_buff;
	int size = 0;

	// Make sure replicated_devices table exists.
	if (didccr_create_data_table(DID_REPL_TABLE))
		return (-1);

	for (path = repldata->device_path_list; path; path = path->next)
		size += strlen(path->device_path) + 1;
	paths_buff = (char *)malloc(sizeof (char) * (size + 1));
	for (path = repldata->device_path_list; path; path = path->next)
		if (first_path == 1) {
			(void) strcpy(paths_buff, path->device_path);
			first_path = 0;
		} else {
			(void) strcat(paths_buff, "|");
			(void) strcat(paths_buff, path->device_path);
		}

	(void) sprintf(key, "%d", repldata->instance);
	(void) sprintf(data, "%d:%s:%s:%s:%s", repldata->orig_inst,
	    repldata->repl_type, repldata->diskid, repldata->dev_group,
	    paths_buff);

	ccr_repltable = ccr_dir->begin_transaction(DID_REPL_TABLE, e);
	if (e.exception()) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Cannot write to table %s.\n"),
		    DID_REPL_TABLE);
		e.clear();
		CORBA::release(ccr_repltable);
		return (-1);
	}

	ccr_repltable->add_element(key, data, e);
	if (e.exception()) {
		if (ccr::key_exists::_exnarrow(e.exception()))
			retval = -2;
		else {
			(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN, "Cannot write to table %s\n"),
			    DID_REPL_TABLE);
			retval = -1;
		}
		e.clear();
		ccr_repltable->abort_transaction(e);
		if (e.exception())
			// This exception isn't important.
			e.clear();
		CORBA::release(ccr_repltable);
		return (retval);
	}

	ccr_repltable->commit_transaction(e);
	if (e.exception()) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Cannot complete write to "
		    "table %s.\n"), DID_REPL_TABLE);
		e.clear();
		ccr_repltable->abort_transaction(e);
		if (e.exception())
			// This exception isn't important.
			e.clear();
		CORBA::release(ccr_repltable);
		return (-1);
	}

	CORBA::release(ccr_repltable);
	return (0);
}

/*
 * Read in the specific instance.
 */
int
did_impl_getinst(did_device_list_t **p, int inst)
{
	Environment		e;
	CORBA::Exception	*ex;
	char			key[128];
	ccr::readonly_table_var	table;
	ccr::table_element_var	elem;

	did_device_list_t	*instptr = NULL;
	char			*buffer;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	(void) sprintf(key, "%d", inst);

	table = ccr_dir->lookup(DID_INST_TABLE, e);
	if (e.exception()) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Cannot lookup %s.\n"),
			DID_INST_TABLE);
		e.clear();
		return (-1);
	}

	table->atfirst(e);
	if (e.exception()) {
		/* The table is empty. Return success. */
		e.clear();
		return (0);
	}

retry_read:

	buffer = table->query_element(key, e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::no_such_key::_exnarrow(ex)) {
			e.clear();
			delete [] buffer;
			return (-1);
		} else if (ccr::table_modified::_exnarrow(ex)) {
			/*
			 * Someone just update the CCR table.
			 * Retry it again later.
			 */
			e.clear();
			delete [] buffer;
			(void) sleep(1 /* second */);
			goto retry_read;
		} else {
			e.clear();
			delete [] buffer;
			return (-1);
		}
	}


	if (did_ccr_str_to_instance(key, buffer, &instptr, did_typelist)) {
		/* did_geterrorstr() is set. Don't unset it. */
		delete [] buffer;
		did_device_list_free(instptr);
		return (-1);
	}

	delete [] buffer;

	e.clear();
	*p = instptr;

	return (0);
}

static int
didccr_abort_write_operation(char *tablename)
{
	Environment	e;

	if (os::strcmp(tablename, DID_INST_TABLE) == 0) {
		if (!CORBA::is_nil(ccr_writetable)) {
			ccr_writetable->abort_transaction(e);
		}
	} else {
		ASSERT(strcmp(tablename, GLOBAL_FENCING_TABLE) == 0);
		if (!CORBA::is_nil(ccr_fencing_writetable)) {
			ccr_fencing_writetable->abort_transaction(e);
		}
	}

	if (e.exception()) {
		// This exception isn't important.
		e.clear();
		return (-1);
	}
	return (0);
}

//
// Initialize the nameserver to hold the did instance cache.
// The did instance cache is comprised of a DID_MAP
// which provide a mechanism to quickly obtain did_instances
// information.
//
// Hierarchy of the did instance cache:
//	did_instances ---- did_map		---- d<instance>
//
// DID_MAP (did_instances/did_map) -
// 	contains binding for each of the DID instance devices and stores
// 	string of nodeids separated by a space
//

//
// This function checks for the existance of mapname in the
// nameserver. The mapname should be a fully qualified name.
// If the mapname is not found then a new context is created.
//
static int
didccr_init_ns_map(const char *mapname)
{
	Environment e;
	naming::naming_context_var rctx_v;

	//
	// Check to see if the map already exists.
	//
	if (!didccr_ns_map_exists(mapname)) {
		//
		// The  map was not found in the nameserver so
		// we attempt to create it here.
		//
		rctx_v = ns::root_nameserver();
		if (CORBA::is_nil(rctx_v)) {
			(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN, "Could not obtain root "
				"nameserver\n"));
			return (-1);
		}

		(void) rctx_v->bind_new_context(mapname, e);
		if (e.exception() != NULL) {
			//
			// A failure occurred trying to create
			// one of the maps. Delete all the maps
			// so that we don't present an
			// inconsistent view of the CCR tables
			//
			e.clear();
			rctx_v->unbind(DID_INST_TABLE, e);
			e.clear();
			(void) snprintf(did_geterrorstr(),
			    DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN, "%s failed to initialize "
				"map %s.\n"), "didccr_init_ns_map", mapname);
			return (-1);
		}
	}
	return (0);
}

//
// The function checks the nameserver for the specified map
// and returns true if it exists
//
static bool
didccr_ns_map_exists(const char *mapname)
{
	Environment e;
	naming::naming_context_var rctx_v;
	rctx_v = ns::root_nameserver();
	if (CORBA::is_nil(rctx_v)) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Could not obtain root "
			"nameserver\n"));
		return (false);
	}

	//
	// Check to see if the map already exists.
	//
	(void) rctx_v->resolve(mapname, e);
	if (e.exception() != NULL) {
		e.clear();
		return (false);
	}
	return (true);
}


//
// Remove an entry from the DID instance cache.
// If an error is encountered then we remove the
// entire cache to prevent an inconsistent view
// of the CCR tables
//
static int
didccr_remove_ns_entry(did_device_list_t *ptr)
{
	char nspath[MAXPATHLEN];

	//
	// Errors from cl_unbind() are ignored since it more than
	// likely indicates that the binding did not exist in
	// the first place.
	//

	(void) sprintf(nspath, "%s/d%d", DID_MAP, ptr->instance);
	(void) cl_unbind(nspath);
	return (0);
}


//
// Given a did_device_list_t entry populate
// the appropriate components in the did instance cache.
// If an error is encountered we remove the entire DID instance
// cache from the nameserver so that we don't present an
// inconsistent view of the CCR tables.
//
static int
didccr_write_ns_entry(did_device_list_t *ptr)
{
	did_subpath_t *sptr;
	char didname[MAXNAMELEN];
	char data[MAXNAMELEN];
	char nspath[MAXPATHLEN];
	char *nodename;
	char *devname;
	nodeid_t nid;

	//
	// If the DID instance cache is not enabled then there
	// is no work to be done.
	//
	if (!did_instance_cache_enabled) {
		return (0);
	} else {
		// Make sure that the DID_MAP exists
		if (didccr_init_ns_map(DID_MAP) == -1) {
			(void) cl_unbind(DID_INST_TABLE);
			return (-1);
		}
	}

	// Only store disk information in the DID instance cache
	if (did_get_devid_type(ptr) == DEVID_NONE)
		return (0);


	//
	// Store the DID instance for this device
	//
	(void) sprintf(didname, "d%d", ptr->instance);

	data[0] = '\0';
	for (sptr = ptr->subpath_listp; sptr; sptr = sptr->next) {

		nodename = didccr_devpath_to_nodename(sptr->device_path);
		devname = didccr_devpath_to_device(sptr->device_path);

		//
		// Determine the nodeids associated with this
		// DID instance. We track all the nodeid for
		// this device in the DID_MAP. We fail
		// if either the nodename is NULL,
		// the device name (devname) is NULL, or
		// we are unable to obtain the nodeid.
		//
		if ((nodename == NULL) || (devname == NULL) ||
		    did_getnodeid(nodename, &nid) < 0) {
			//
			// We were unable to determine the nodeid
			// for this entry. We disable the entire
			// cache to ensure that the data is consistent.
			//
			(void) cl_unbind(DID_INST_TABLE);
			return (-1);
		}

		//
		// Store all the nodeids associated with this device
		// in the data string. This will be written out to the
		// DID_MAP once we have looked at all the subpaths for
		// this instance.
		//
		(void) sprintf(data, "%s%d ", data, nid);

		free(devname);
		free(nodename);
	}

	//
	// the get_disconnected_nodes() call return as soon as it get a value
	// that is less than 1. So we use '|' here to separate the node list
	// and the default fencing info.
	//
	(void) sprintf(data, "%s| %d|%d", data, ptr->default_fencing,
	    ptr->detected_fencing);

	//
	// Populate the DID_MAP entry for this device with
	// the nodeid list obtained from the subpaths.
	//
	(void) sprintf(nspath, "%s/%s", DID_MAP, didname);
	if (cl_rebind(nspath, data)) {
		//
		// We were unable to store the data for this
		// binding. We disable the entire cache
		// to ensure that the data is consistent
		//
		(void) cl_unbind(DID_INST_TABLE);
		return (-1);
	}
	return (0);
}

//
// This function populates the DID instance cache
int
did_impl_populate_list_cache(did_device_list_t *dlist)
{
	Environment e;
	did_device_list_t *cur;

	if (dlist == NULL) {
		return (-1);
	}

	//
	// We hold the ccr lock so that we don't miss any
	// updates while we are populating the DID instance
	// cache
	//
	if (did_holdccrlock()) {
		// did_geterrorstr() is already set so don't override it
		(void) did_abortccrlock();
		return (-1);
	}

	//
	// Traverse the entire did_device_list and populate the
	// DID instance cache.
	//
	for (cur = dlist; cur; cur = cur->next) {
		if (didccr_write_ns_entry(cur) == -1) {
			(void) snprintf(did_geterrorstr(),
			    DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN, "%s failed to populate "
				"the DID instance cache.\n"),
				"did_imp_populate_list_cache");
			(void) did_abortccrlock();
			return (-1);
		}
	}

	// Populate the global fencing status
	unsigned short status;
	if (didccr_read_globalfencingstatus(&status) != 0 ||
	    didccr_write_ns_entry(status) != 0) {
		(void) snprintf(did_geterrorstr(),
		    DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "%s failed to populate "
			"the SCSI3 default fencing status cache.\n"),
			"did_imp_populate_list_cache");
	}

	(void) did_abortccrlock();
	return (0);
}	

// This function get the status of global fencing
int
did_impl_getglobalfencingstatus(unsigned short *status)
{
	*status = GLOBAL_FENCING_PATHCOUNT;

	if (didccr_open()) {
		// Errno is set. Don't unset it. */
		return (-1);
	}

	if (didccr_read_globalfencingstatus(status)) {
		didccr_close();
		return (-1);
	}
	didccr_close();
	return (0);
}

//
// This function sets the global algorithm that determines fencing protocol
// to use.
//
int
did_impl_setglobalfencingstatus(unsigned short newstatus)
{
	//
	// No need to call didccr_open() because this is always called after
	// did_holdccrlock().
	//

	// Assert didccr_open has already been called
	ASSERT(!CORBA::is_nil(ccr_dir));

	if (didccr_write_globalfencingstatus(newstatus)) {
		return (-1);
	} else {
		return (0);
	}
}

//
// Helper functions for DID Instance routines
//

//
// This routine returns back the nodename associated with
// a particular device_path. The caller is responsible for
// freeing the allocated memory.
//
static char *
didccr_devpath_to_nodename(char *device_path)
{
	char *nodename;
	size_t len;

	if ((len = strcspn(device_path, ":")) == 0) {
		return (NULL);
	}

	if ((nodename = (char *)calloc(len + 1, sizeof (char))) == NULL) {
		return (NULL);
	}
	(void) strlcpy(nodename, device_path, len + 1);
	return (nodename);
}

//
// This routine returns back the device associated with
// a particular device_path. The caller is responsible for
// freeing the allocated memory.
//
static char *
didccr_devpath_to_device(char *device_path)
{
	char *devname;

	if ((devname = strrchr(device_path, '/')) == NULL) {
		return (NULL);
	}
	devname++;
	return (strdup(devname));
}
