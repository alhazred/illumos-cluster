/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)did_shared.c	1.31	09/01/29 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <assert.h>
#include <ctype.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <sys/errno.h>
#include <sys/sunddi.h>
#include <sys/clconf.h>
#include <sys/didio.h>
#include <sys/ddi_impldefs.h>
#include <sys/varargs.h>
#include <sys/sc_syslog_msg.h>
#include <sys/sol_version.h>
#include <libintl.h>
#include <libdevinfo.h>
#include <clcomm_cluster_vm.h>
#include "libdid.h"
#include "didprivate.h"

/* Fixed constants used in did_strtodiskid, for conversion */
#define	PROTOLEN	5	/* lenght of the proto string "0xXY" */
#define	PROTOID2	2	/* index of X in the proto string  */
#define	PROTOID3	3	/* index of Y in the proto string  */

#define	LOG_LINE_MAX	512	/* Maximum length of a log message  */
#define	ARR_INCR	50	/* Units by which a dynamic array grows */

struct	devidtype {
	char		*type_name;
	unsigned short	type;
};

struct devidtype diskidtypes[] = {
	{ "DEVID_NONE",		DEVID_NONE },
	{ "DEVID_SCSI3_WWN",	DEVID_SCSI3_WWN },
	{ "DEVID_SCSI_SERIAL",	DEVID_SCSI_SERIAL },
	{ "DEVID_FAB",		DEVID_FAB },
	{ "DEVID_ENCAP",	DEVID_ENCAP },
#ifdef DEVID_ATA_SERIAL
	{ "DEVID_ATA_SERIAL",	DEVID_ATA_SERIAL },
#endif /* DEVID_ATA_SERIAL */
#ifdef DEVID_SCSI3_VPD_T10
	{ "DEVID_SCSI3_VPD_T10",	DEVID_SCSI3_VPD_T10 },
#endif /* DEVID_SCSI3_VPD_T10 */
#ifdef DEVID_SCSI3_VPD_EUI
	{ "DEVID_SCSI3_VPD_EUI",	DEVID_SCSI3_VPD_EUI },
#endif /* DEVID_SCSI3_VPD_EUI */
#ifdef DEVID_SCSI3_VPD_NAA
	{ "DEVID_SCSI3_VPD_NAA",	DEVID_SCSI3_VPD_NAA }
#endif /* DEVID_SCSI3_VPD_NAA */
	};

static char *did_ccr_next_token(char **, const char *);
static int did_ccr_create_minor_map(char *, did_device_type_t *);
static int did_strtoFlags(char *strid, did_device_list_t *insptr);
static int add2Array(char ***array, char *element, int index, int *array_size);
extern int errno;
int fmtline = 0;


/*
 * This function adds a device type, specified by type and record,
 * into the 'typelist' list. The format of 'record' is expected to be:
 * 	<type_num>|<instance_encoding>|<discovery_subdir>|<discovery_minor>|
 *	<name_offset_from_inst>|<default_nodes>|<start_default_nodes>|
 *	<next_default_node>|<minorencoding1>|<minorencoding2>|...
 *		type_num = An integer mapping to the device type.
 *		instance_encoding = Not used in this release.
 *		directory_subdir = Subdir under /dev to look for devices
 *			of this type.
 *		discovery_minor = Minor component of the logical name to
 *			search for. Many devices have multiple minors for
 *			a physical device. This one is the one we should
 *			search for to guarantee existance of that device.
 *		name_offset_from_inst = Not used in this release.
 *		default_nodes = How many minor nodes should DID create at
 *			a time for this type of device.
 *		start_default_nodes = Integer representing the instance to
 *			start creating DID nodes at.
 *		next_default_node = This integer defines how to increment
 *			the previous instance assigned to get the next one
 *			for this device.
 *		minor_encoding = A colon separated list of details to define
 *			minor encoding. These are the minor number, minor
 *			name, minor name of original device, device type
 *			(block and character, read as 'b' or 'c').
 */
int
did_ccr_str_to_device_type(const char *type, char *record,
	did_device_type_t **typelist)
{
	did_device_type_t	*tp;
	char			*rec = record;
	char			*tok;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (!type || !record || !typelist) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Invalid input to %s.\n"),
			"did_ccr_str_to_device_type");
		return (-1);
	}
	if (did_ccr_find_device_type(type, *typelist)) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Duplicate instance of "
			"device type %s.\n"), type);
		return (-1);
	}

	if ((tp = (did_device_type_t *)calloc(1, sizeof (did_device_type_t)))
	    == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Out of memory\n"));
		return (-1);
	}

	(void) strncat(tp->type_name, type, MAXNAMELEN - 1);
	if ((tok = did_ccr_next_token(&rec, "|")) != NULL && *tok)
		tp->type_num = atoi(tok);
	if ((tok = did_ccr_next_token(&rec, "|")) != NULL && *tok) {
		/*
		* Ignore instance encoding for now.
		*/
	}
	if ((tok = did_ccr_next_token(&rec, "|")) != NULL && *tok)
		(void) strncat(tp->primary_subdir, tok, MAXPATHLEN - 1);
	if ((tok = did_ccr_next_token(&rec, "|")) != NULL && *tok)
		(void) strncat(tp->discovery_minor, tok, MAXPATHLEN - 1);
	if ((tok = did_ccr_next_token(&rec, "|")) != NULL && *tok)
		tp->name_offset_from_inst = atoi(tok);
	if ((tok = did_ccr_next_token(&rec, "|")) != NULL && *tok)
		tp->default_nodes = atoi(tok);
	if ((tok = did_ccr_next_token(&rec, "|")) != NULL && *tok)
		tp->start_default_nodes = atoi(tok);
	if ((tok = did_ccr_next_token(&rec, "|")) != NULL && *tok)
		tp->next_default_node = atoi(tok);

	/*
	 * Read descriptions of minor nodes.
	 */
	while ((tok = did_ccr_next_token(&rec, "|")) != NULL) {
		if (did_ccr_create_minor_map(tok, tp) != 0) {
			/* Errno is set. Don't unset it */
			/* did_device_type_free is done in callers */
			return (-1);
		}
	}

	if (*typelist != NULL) {
		tp->next = *typelist;
	}
	*typelist = tp;

	return (0);
}

void
did_repl_list_free(did_repl_list_t *list)
{
	did_repl_list_t *ip, *ip_next;

	for (ip = list; ip; ip = ip_next) {
		ip_next = ip->next;
		did_repl_list_entry_free(ip);
	}
}

void
did_device_list_free(did_device_list_t *list)
{
	did_device_list_t *ip, *ip_next;

	for (ip = list; ip; ip = ip_next) {
		ip_next = ip->next;
		did_device_list_entry_free(ip);
	}
}

void
did_repl_list_entry_free(did_repl_list_t *entry)
{
	assert(entry != NULL);
	if (entry->diskid != NULL) {
		free(entry->diskid);
	}
	free(entry);
}

void
did_device_list_entry_free(did_device_list_t *entry)
{
	did_subpath_t *subp, *subp_next;

	assert(entry != NULL);
	if (entry->diskid != NULL) {
		free(entry->diskid);
	}
	if (entry->target_device_id != NULL) {
		free(entry->target_device_id);
	}
	for (subp = entry->subpath_listp; subp; subp = subp_next) {
		subp_next = subp->next;
		free(subp);
	}
	free(entry);
}

/*
 * This function extracts the replication info from a replicated device entry.
 * 'record' is assumed to be of the form:
 * 	<did_instance>:<repl_type>:<repl_devid>:<dev_group>:
 *	<repl_device_path1|<repl_device_path2>...
 *		did_instance = the did instance that was merged to make a
 *                  replicated device
 *		repl_type = the type of replication for the device
 *		repl_devid = the device id of the merged replicated device
 *		dev_group = the replication dev_group the device is in
 *		repl_device_path = path information for the replicated device,
 *		    including the node name
 */
int
did_repl_str_to_instance(char *key, char *record, did_repl_list_t *repl)
{
	char *rec, *tok;
	did_repl_path_t *newpath;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (!key || !record || !repl) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Invalid input to %s.\n"),
				"did_repl_str_to_instance");
		return (-1);
	}

	repl->instance = atoi(key);
	repl->diskid = NULL;
	repl->remove = 0;

	rec = record;
	/* get src instance */
	tok = did_ccr_next_token(&rec, ":");
	repl->orig_inst = atoi(tok);
	/* get repl_type */
	if ((tok = did_ccr_next_token(&rec, ":")) != NULL && *tok)
		(void) strcpy(repl->repl_type, tok);
	else {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Unable to get replication type "
		    "for instance %s\n"), key);
		return (-1);
	}
	/* get repl_devid */
	if ((tok = did_ccr_next_token(&rec, ":")) != NULL) {
		repl->diskid = (char *)calloc(1,
		    strlen(tok) + 1);
		if (repl->diskid == NULL) {
			(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN, "Out of memory.\n"));
			return (-1);
		}
		(void) strcpy(repl->diskid, tok);
	} else {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Unable to get replicated device id "
		    "for instance %s\n"), key);
		return (-1);
	}
	/* get dev_group */
	if ((tok = did_ccr_next_token(&rec, ":")) != NULL && *tok) {
		(void) strcpy(repl->dev_group, tok);
	} else {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Unable to get replication dev_group "
		    "for instance %s\n"), key);
		return (-1);
	}
	/* get device path */
	repl->device_path_list = NULL;
	while ((tok = did_ccr_next_token(&rec, "|")) != NULL && *tok) {
		newpath = (did_repl_path_t *)malloc(sizeof (*newpath));
		if (newpath == NULL) {
			(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN, "Out of memory.\n"));
			return (-1);
		}
		(void) strcpy(newpath->device_path, tok);
		newpath->next = repl->device_path_list;
		repl->device_path_list = newpath;
	}

	return (0);
}

/*
 * This function adds a new DID instance to instlist. 'record' is
 * assumed to be of the form:
 * 	<device_type>|<devid_type>|<ascii_devid>;<fencing protocol>|<devid>
 *	|<flags>|<subpath1>|<subpath2>|...
 *		device_type = Character device type, as found in typelist.
 *		devid_type = A string representation of the device type
 *				expected by Solaris device ids.
 *		ascii_devid = ASCII conversion of the Solaris devid.
 *		fencing_protocol = A string describing the default fencing
 *				protocol for this device (scsi2 or scsi3)
 *		devid = The Solaris devid, in hex.
 *		flags = See libdid.h for valid values.
 *		subpath = A subpath consists of the cluster nodename, a ':',
 *			then the path to the underlying device. This path
 *			is usually in /dev, and does not contain the minor
 *			portion of the name.
 */
int
did_ccr_str_to_instance(const char *inum, char *record,
	did_device_list_t **instlist, did_device_type_t *typelist)
{
	did_device_list_t	*instptr = NULL;
	char			*rec, *tok;
	int			instnum;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (!record || !typelist) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Invalid input to %s.\n"),
				"did_ccr_str_to_instance");
		return (-1);
	}

	if ((instnum = atoi(inum)) <= 0) {
		/* Invalid instance number */
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Invalid input to %s. %s not "
			"an instance number.\n"),
			"did_ccr_str_to_instance", inum);
		return (-1);
	}
	if (instlist && did_ccr_find_instance(instnum, *instlist)) {
		/* Duplicate instance */
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Duplicate instance %d.\n"), instnum);
		return (-1);
	}

	instptr = (did_device_list_t *)calloc(1, sizeof (did_device_list_t));
	if (instptr == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Out of memory.\n"));
		return (-1);
	}

	rec = record;
	instptr->instance = instnum;
	if ((tok = did_ccr_next_token(&rec, "|")) != NULL) {
		instptr->device_type = did_ccr_find_device_type(tok, typelist);
		if (instptr->device_type == NULL) {
			/* Invalid type */
			(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN, "Invalid input to %s.\n"),
				"did_ccr_str_to_instance");
			did_device_list_entry_free(instptr);
			return (-1);
		}
	}
	if ((tok = did_ccr_next_token(&rec, "|")) != NULL && *tok) {
		instptr->type = did_strtotype(tok);
		if (instptr->target_device_id) {
			did_set_type(instptr->target_device_id, instptr->type);
		}
	}
	/*
	 * Ignore textual disk id.
	 */
	tok = did_ccr_next_token(&rec, "|");

	/* Get the default fencing protocol */
	if ((tok = did_ccr_next_token(&rec, "|")) != NULL && *tok) {
		if (did_strtodiskfencing(tok, instptr) == 0) {
			if ((tok = did_ccr_next_token(&rec, "|")) == NULL ||
			    !*tok || did_strtodiskfencing(tok, instptr) != 0) {
				/* Invalid fencing string */
				(void) snprintf(did_geterrorstr(),
				    DID_ERRORSTR_SIZE, dgettext(TEXT_DOMAIN,
				    "Invalid input to %s.\n"),
				    "did_strtodiskfencing");
				did_device_list_entry_free(instptr);
				return (-1);
			}
			tok = NULL;
		} else {
			/*
			 * Else:
			 * This is not a valid fencing string. We are running
			 * an old version of software without the SCSI3
			 * default fencing feature. We will pass it on and
			 * process it as the manufacturer id string.
			 */
			instptr->default_fencing = FENCING_USE_GLOBAL;
			instptr->detected_fencing = FENCING_NA;
		}
	}

	if (tok || ((tok = did_ccr_next_token(&rec, "|")) != NULL && *tok)) {
		did_strtodiskid(tok, instptr);
		if (instptr->type) {
			did_set_type(instptr->target_device_id, instptr->type);
		}
	}

	if (is_flags_available() == B_TRUE) {
	    if (((tok = did_ccr_next_token(&rec, "|")) != NULL && *tok)) {
		if (did_strtoFlags(tok, instptr) < 0) {
			(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN,
				"Invalid input to %s. Incorrect flags\n"),
				"did_ccr_str_to_instance");
			did_device_list_entry_free(instptr);
			return (-1);
		}
	    }
	}

	while ((tok = did_ccr_next_token(&rec, "|")) != NULL && *tok) {
		did_subpath_t	*subptr = NULL;
		char		dpath_copy[MAXPATHLEN];
		char		*nodeidstr, *pathname;
		char		nodename[MAXNAMELEN];
		nodeid_t	nodeid;

		if ((subptr = (did_subpath_t *)malloc(sizeof (did_subpath_t)))
		    == NULL) {
			(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN, "Out of memory.\n"));
			did_device_list_entry_free(instptr);
			return (-1);
		}
		/*
		 * copy the tok string to our local one since
		 * strtok() leaves '\0' in its wake. Then assign pointers
		 * to the nodeidstr and pathname portions.
		 */
		dpath_copy[0] = '\0';
		(void) strncat(dpath_copy, tok, MAXPATHLEN - 1);
		if ((nodeidstr = strtok(dpath_copy, ":")) == NULL) {
			(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN, "Invalid subpath string, "
				"missing ':'.\n"));
				free(subptr);
				did_device_list_entry_free(instptr);
				errno = EINVAL;
				return (-1);
		}
		pathname = (char *)&(tok[strlen(nodeidstr) + 1]);
		(void) memset(subptr, 0, sizeof (did_subpath_t));
		/*
		 * Sanity check on the nodeid string. If not an integer
		 *  attempt the following mapping.
		 *
		 * If we find a hostname instead of a nodeid id in the string,
		 * still allow reading of the hostname, presuming an
		 * upgrade context. 'Lazy' conversion will occur on the
		 * first boot after upgrade.
		 *
		 * If we find a nodeid in the string, but cannot perform a
		 * mapping to a hostname - return with an error.
		 */
		if (sscanf(nodeidstr, "%d", &nodeid) != 1) {
			/*
			 * Didn't find a nodeid in the path prefix. Assume
			 * old-style ccr encoding and simply read the (assumed)
			 * nodename.
			 */
			(void) strncat(subptr->device_path,
				tok, MAXPATHLEN - 1);
		} else {
			/*
			 * A nodeid was found at the begining of the device
			 * path - conversion is required.
			 */
			if (did_getnodename(nodeid, nodename) != 0) {
				(void) snprintf(did_geterrorstr(),
				DID_ERRORSTR_SIZE,
				dgettext(TEXT_DOMAIN, "No hostname mapping "
				"for nodeid %d.\n"), nodeid);
				free(subptr);
				did_device_list_entry_free(instptr);
				errno = EINVAL;
				return (-1);
			}
			(void) sprintf(dpath_copy, "%s:%s",
			    nodename, pathname);
			(void) strncat(subptr->device_path,
				dpath_copy, MAXPATHLEN - 1);
		}
		subptr->next = instptr->subpath_listp;
		subptr->instptr = instptr;
		instptr->subpath_listp = subptr;
	}

	if (instlist) {
		if (*instlist == NULL || instnum < (*instlist)->instance) {
			instptr->next = *instlist;
			*instlist = instptr;
		} else {
			did_device_list_t	*ninst = *instlist;

			while (ninst->next) {
				if (instnum < ninst->next->instance)
					break;
				ninst = ninst->next;
			}
			instptr->next = ninst->next;
			ninst->next = instptr;
		}
	}
	return (0);
}

int
did_diskidtostr(char *id, int idlen, char **out)
{
	int	i;
	char	*sp, *s;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (idlen == 0) {
		sp = malloc(2);
		if (sp == NULL) {
			(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN, "Out of memory.\n"));
			return (-1);
		}
		(void) strcpy(sp, "0");
		*out = sp;
		return (0);
	}

	sp = malloc((uint_t)idlen*2+1);
	if (sp == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Out of memory.\n"));
		*out = NULL;
		return (-1);
	}
	s = sp;

	for (i = 0; i < idlen; i++) {
		(void) sprintf(sp, "%02x", id[i] & 0xff);
		sp += 2;
	}
	*out = s;
	return (0);
}

int
did_globalfencingtostr(unsigned short fencingid, char **out)
{
	char	*sp = NULL;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	sp = malloc(30);
	if (sp == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Out of memory.\n"));
		return (-1);
	}

	switch (fencingid) {
	case GLOBAL_FENCING_UNKNOWN:
		(void) sprintf(sp, "UNKNOWN algorithm");
		break;
	case GLOBAL_FENCING_PATHCOUNT:
		(void) sprintf(sp, "DID path count algorithm");
		break;
	case GLOBAL_FENCING_PREFER3:
		(void) sprintf(sp, "prefer SCSI3 algorithm");
		break;
	case GLOBAL_NO_FENCING:
		(void) sprintf(sp, "no fencing");
		break;
	default:
		return (-1);
	}
	*out = sp;
	return (0);
}

int
did_diskfencingtostr(unsigned short fencingid, char **out)
{
	char	*sp = NULL;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	sp = malloc(30);
	if (sp == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Out of memory.\n"));
		return (-1);
	}

	switch (fencingid) {
	case FENCING_NA:
		(void) sprintf(sp, "N/A");
		break;
	case SCSI2_FENCING:
		(void) sprintf(sp, "SCSI2");
		break;
	case SCSI3_FENCING:
		(void) sprintf(sp, "SCSI3");
		break;
	case FENCING_USE_GLOBAL:
		(void) sprintf(sp, "USE_GLOBAL");
		break;
	case FENCING_USE_PATHCOUNT:
		(void) sprintf(sp, "USE_PATHCOUNT");
		break;
	case FENCING_USE_SCSI3:
		(void) sprintf(sp, "USE_SCSI3");
		break;
	case NO_FENCING:
		(void) sprintf(sp, "NO_FENCING");
		break;
	default:
		(void) sprintf(sp, "N/A");
		break;
	}

	*out = sp;
	return (0);
}

/*
 * Common code for DID CCR implementation.
 */
static char *
did_ccr_next_token(char **strp, const char *separators)
{
	char	*tok, *eotok;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (strp == NULL || *strp == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Invalid input to %s.\n"),
				"did_ccr_next_token");
		return (NULL);
	}

	tok = *strp;
	if ((eotok = strpbrk(tok, separators)) != NULL)
		*eotok = 0;

	*strp = (eotok? eotok + 1: NULL);
	return (tok);
}

/*
 * Read in minor info of the form:
 *	minor_num:minor_name:cur_minor_name:device_class
 * Returns -1 if minor_num is not a number, or device_list != 'c' or 'b'.
 */
static int
did_ccr_create_minor_map(char *record, did_device_type_t *tp)
{
	char		*tok, *p;
	char		*rec, *saved_rec;
	int		ind = tp->minor_count;

	saved_rec = rec = strdup(record);

	if (rec == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Out of memory\n"));
		return (-1);
	}

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if ((tok = did_ccr_next_token(&rec, ":")) != NULL) {
		tp->minor_num[ind] = strtoul(tok, &p, 10);
		if (*tok == 0 || *p != 0) {
			(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN, "Invalid input to %s.\n"),
				"did_ccr_create_minor_map");
			free(saved_rec);
			return (-1);
		}
	}
	if ((tok = did_ccr_next_token(&rec, ":")) != NULL && *tok)
		(void) strncat(tp->minor_name[ind], tok, MAXPATHLEN - 1);
	if ((tok = did_ccr_next_token(&rec, ":")) != NULL && *tok)
		(void) strncat(tp->cur_minor_name[ind], tok, MAXPATHLEN - 1);
	tp->device_class[ind] = S_IFCHR;
	if ((tok = did_ccr_next_token(&rec, ":")) != NULL && *tok) {
		if (*tok == 'b')
			tp->device_class[ind] = S_IFBLK;
		else if (*tok != 'c') {
			(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN, "Unknown device class %c.\n"),
					*tok);
			free(saved_rec);
			return (-1);
		}
	}
	tp->minor_count++;
	free(saved_rec);
	return (0);
}

did_device_type_t *
did_ccr_find_device_type(const char *type, did_device_type_t *typelist)
{
	did_device_type_t *tp;

	for (tp = typelist; tp; tp = tp->next) {
		if (strcmp(tp->type_name, type) == 0)
			return (tp);
	}
	return (NULL);
}

did_device_list_t *
did_ccr_find_instance(int instnum, did_device_list_t *instlist)
{
	did_device_list_t *ip;

	for (ip = instlist; ip; ip = ip->next) {
		if (ip->instance == instnum)
			return (ip);
	}
	return (NULL);
}

unsigned short
did_strtotype(char *strtype)
{
	uint_t	i;

	for (i = 0; i < (sizeof (diskidtypes) / sizeof (diskidtypes[0])); i++) {
		if (strcasecmp(strtype, diskidtypes[i].type_name) == 0)
			return (diskidtypes[i].type);
	}
	return (DEVID_NONE);
}

/*
 * called from parseInstance
 */
void
did_set_type(ddi_devid_t devid, unsigned short type)
{
	impl_devid_t *idevid = (impl_devid_t *)devid;

	assert(devid);
	DEVID_FORMTYPE(idevid, type);
}

/*
 * Called from parseInstance to convert a string to fencing.
 */
int
did_strtodiskfencing(char *fencingstr, did_device_list_t *insptr)
{
	if (strcmp(fencingstr, "N/A") == 0) {
		insptr->detected_fencing = FENCING_NA;
		return (0);
	}
	if (strcmp(fencingstr, "SCSI2") == 0) {
		insptr->detected_fencing = SCSI2_FENCING;
		return (0);
	}
	if (strcmp(fencingstr, "SCSI3") == 0) {
		insptr->detected_fencing = SCSI3_FENCING;
		return (0);
	}
	if (strcmp(fencingstr, "USE_GLOBAL") == 0) {
		insptr->default_fencing = FENCING_USE_GLOBAL;
		return (0);
	}
	if (strcmp(fencingstr, "USE_PATHCOUNT") == 0) {
		insptr->default_fencing = FENCING_USE_PATHCOUNT;
		return (0);
	}
	if (strcmp(fencingstr, "USE_SCSI3") == 0) {
		insptr->default_fencing = FENCING_USE_SCSI3;
		return (0);
	}
	if (strcmp(fencingstr, "NO_FENCING") == 0) {
		insptr->default_fencing = NO_FENCING;
		return (0);
	}

	/* This is not a valid fencing string */
	return (-1);
}

/*
 * Called from parseInstance to convert a string to flags.
 */
int
did_strtoFlags(char *strid, did_device_list_t *insptr)
{
	unsigned long zFlag = 0;
	errno = 0;
	zFlag = strtoul(strid, (char **)NULL, 16);
	if (errno != 0) {
	    return (-1);
	}
	insptr->flags  = zFlag;
	return (0);
}

/*
 * called from parseInstance
 */
void
did_strtodiskid(char *strid, did_device_list_t *insptr)
{
	impl_devid_t *idevid;
	char proto[PROTOLEN] = "0x00";
	char *idp;
	unsigned short idlen;

	idevid = malloc(strlen(strid) + sizeof (impl_devid_t));
	if (idevid == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Out of memory\n"));
		return;
	}
	idp = idevid->did_id;

	if (strcmp(strid, "0") == 0) {
		insptr->target_device_id = NULL;
		insptr->diskid_len = 0;
		insptr->diskid = NULL;
		return;
	}

	while (*strid) {
		proto[PROTOID2] = *strid++;
		proto[PROTOID3] = *strid++;
		*idp++ = (char)strtoul(proto, (char **)NULL, 16);
	}
	idlen = (unsigned short)(idp - idevid->did_id);
	idevid->did_magic_hi = DEVID_MAGIC_MSB;
	idevid->did_magic_lo = DEVID_MAGIC_LSB;
	idevid->did_rev_hi = DEVID_REV_MSB;
	idevid->did_rev_lo = DEVID_REV_LSB;
	DEVID_FORMLEN(idevid, idlen);
	insptr->target_device_id = (ddi_devid_t)idevid;
	insptr->diskid = (char *)malloc(idlen);
	if (insptr->diskid != NULL) {
		insptr->diskid_len = idlen;
		(void) memcpy(insptr->diskid, idevid->did_id, idlen);
	} else {
		insptr->diskid_len = 0;
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Out of memory\n"));
	}
}

int
did_ccr_instance_to_str(did_device_list_t *instptr, char **out)
{
	char		*diskidstr = NULL;
	char		*asciidiskidstr = NULL;
	char		*defaultfencingstr = NULL;
	char		*detectedfencingstr = NULL;
	char		typestr[MAXNAMELEN];
	char		*buf;
	uint_t		maxlen;
	did_subpath_t	*subptr;
	char		*pipechar;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (instptr == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Invalid input to %s.\n"),
				"did_ccr_instance_to_str");
		return (-1);
	}

	bzero(typestr, sizeof (typestr));

	/*
	 * Allocate a large enough buffer for output.
	 */
	maxlen = sizeof (did_device_list_t) + DID_CCR_STRING_PAD;
	if (is_flags_available()) {
	/*
	 * This extra size is to account for storing flags in Hex
	 * A Hex value is 2 character long for every byte. One
	 * character is accounted for as part of
	 * sizeof (did_device_list_t).
	 */
	    maxlen += sizeof (unsigned int);
	}
	for (subptr = instptr->subpath_listp; subptr; subptr = subptr->next)
		maxlen += sizeof (did_subpath_t);
	if ((buf = (char *)malloc(maxlen)) == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Out of memory.\n"));
		return (-1);
	}

	/*
	 * Fill in output buffer.
	 */

	if (instptr->diskid_len) {
		if (did_diskidtostr(instptr->diskid, instptr->diskid_len,
		    &diskidstr)) {
			/* Errno is set. Don't unset it. */
			return (-1);
		}
		/* From here, diskidstr is not NULL */
		asciidiskidstr = did_diskidtoascii(instptr->diskid,
		    instptr->diskid_len);
		if (asciidiskidstr == NULL) {
			/* Errno is set. Don't unset it. */
			free(diskidstr);
			return (-1);
		}
	}

	if (instptr->type != DEVID_NONE)
		if (did_typetostr(instptr->type, typestr, sizeof (typestr)-1)) {
			/* Errno is set. Don't unset it. */
			if (asciidiskidstr != NULL) {
				free(asciidiskidstr);
			}
			if (diskidstr != NULL) {
				free(diskidstr);
			}
			return (-1);
		}

	if (asciidiskidstr == NULL) {
		asciidiskidstr = (char *)malloc(1);
		if (asciidiskidstr == NULL) {
			(void) snprintf(did_geterrorstr(),
			    DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN, "Out of memory.\n"));
			/* Errno is set. Don't unset it. */
			if (diskidstr != NULL) {
				free(diskidstr);
			}
			return (-1);
		}
		asciidiskidstr[0] = 0; /* NULL string */
	}

	if (diskidstr == NULL) {
		diskidstr = (char *)malloc(1);
		if (diskidstr == NULL) {
			(void) snprintf(did_geterrorstr(),
			    DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN, "Out of memory.\n"));
			/* Errno is set. Don't unset it. */
			free(asciidiskidstr);
			return (-1);
		}
		diskidstr[0] = 0; /* NULL string */
	}

	/* Assigned the default fencing str. */
	if (did_diskfencingtostr(instptr->default_fencing,
	    &defaultfencingstr)) {
		/* Errno is set. Don't unset it */
		return (-1);
	}

	/* Assigned the detected fencing str. */
	if (did_diskfencingtostr(instptr->detected_fencing,
	    &detectedfencingstr)) {
		/* Errno is set. Don't unset it */
		return (-1);
	}

	/*
	 * The string 'asciidiskidstr' represents the human readable devid field
	 * in the CCR entry for this DID device. The vertical bar character '|'
	 * is used to separate fields in CCR entries. Having a vertical bar
	 * within a field makes in difficult, if not impossible to later
	 * separate the fields. So we replace all vertical bar characters in
	 * this string with periods before writing out the CCR entry. Period is
	 * used in place of the vertical bar as it causes no such conflict. This
	 * field is only used for human readability and altering it has no
	 * impact on devid processing.
	 */
	pipechar = asciidiskidstr;
	while ((pipechar = strchr(pipechar, '|')) != NULL) {
		pipechar[0] = '.';
	}

	if (is_flags_available()) {
	    (void) sprintf(buf, "%s|%s|%s|%s|%s|%s|%x",
		instptr->device_type->type_name, typestr, asciidiskidstr,
		defaultfencingstr, detectedfencingstr, diskidstr,
		instptr->flags);
	} else {
	    (void) sprintf(buf, "%s|%s|%s|%s|%s|%s",
		instptr->device_type->type_name, typestr, asciidiskidstr,
		defaultfencingstr, detectedfencingstr, diskidstr);
	}

	for (subptr = instptr->subpath_listp; subptr; subptr = subptr->next) {
		nodeid_t	nodeid;
		char		dpath_copy[MAXPATHLEN];
		char		*nodename, *pathname;

		(void) strcat(buf, "|");
		/*
		 * copy the device_path string to our local one since
		 * strtok() leaves '\0' in its wake. Then assign pointers
		 * to the nodename and pathname portions.
		 */
		dpath_copy[0] = '\0';
		(void) strncat(dpath_copy, subptr->device_path, MAXPATHLEN - 1);
		if ((nodename = strtok(dpath_copy, ":")) == NULL) {
			(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN, "Invalid subpath string, "
					"missing ':'.\n"));
				errno = EINVAL;
				free(asciidiskidstr);
				free(diskidstr);
				free(defaultfencingstr);
				free(detectedfencingstr);
				return (-1);
		}
		pathname = (char *)&(dpath_copy[strlen(nodename) + 1]);
		/*
		 * Need to convert the hostname in the device_path string
		 * to a nodeid value. If the mapping fails with -2, assume the
		 * upgrade context and allow the hostname to be written - flag
		 * the event. 'Lazy' conversion will occur on the
		 * first boot after upgrade.
		 * If the mapping fails with '-1' return with an error.
		 */
		switch (did_getnodeid(nodename, &nodeid)) {

		case 0:
			/*
			 * Success.
			 */
			(void) sprintf(dpath_copy, "%d:%s", nodeid, pathname);
			(void) strcat(buf, dpath_copy);
			break;

		case -2:
			/*
			 * Failed, but we could have been in upgrade context.
			 * If that's the case, we allow writing the hostname
			 * to the CCR table.
			 */
			(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN, "No mapping from %s to a "
				"nodeid, unless this "
			    "is in an upgrade context there could be "
			    "cluster configuration problems.\n"), nodename);
			(void) strcat(buf, subptr->device_path);
			break;
		case -1:
		default:
			(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN, "No mapping from %s to a "
				"nodeid.\n"), nodename);
			errno = EINVAL;
			free(asciidiskidstr);
			free(diskidstr);
			free(defaultfencingstr);
			free(detectedfencingstr);
			return (-1);
		}
	}

	*out = buf;
	free(asciidiskidstr);
	free(diskidstr);
	free(defaultfencingstr);
	free(detectedfencingstr);
	return (0);
}

int
did_typetostr(unsigned short type, char *out, int strsize)
{
	uint_t	i;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	for (i = 0; i < sizeof (diskidtypes) / sizeof (diskidtypes[0]); i++) {
		if (type == diskidtypes[i].type) {
			if ((int)strlen(diskidtypes[i].type_name) > strsize) {
				(void) snprintf(did_geterrorstr(),
				    DID_ERRORSTR_SIZE,
				    dgettext(TEXT_DOMAIN, "Out of memory.\n"));
				return (-1);
			}
			(void) strcpy(out, diskidtypes[i].type_name);
			return (0);
		}
	}
	if ((int)strlen("DEVID_NONE") > strsize) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Not enough memory to convert "
			"type %d.\n"), type);
		return (-1);
	}
	(void) strcpy(out, "DEVID_NONE");
	return (0);
}

char *
did_diskidtoascii(char *diskid, int idlen)
{
	int		i;
	char *ascii = malloc(MAXNAMELEN + 1);

	if (ascii == NULL) {
		(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
		    dgettext(TEXT_DOMAIN, "Out of memory.\n"));
		return (NULL);
	}

	ascii[0] = 0;
	for (i = 0; i < idlen; i++) {
		if (!isprint(diskid[i]))
			break;
		ascii[i] = diskid[i];
	}
	ascii[i] = 0;
	return (ascii);
}

/* ARGSUSED */
void
did_sort_instances(did_device_list_t *instlist)
{
	/*
	 * Use qsort, canonical form into an array.
	 */
}

/*
 * called from get_fmts
 */
int
did_checkRecordDescription(did_device_list_t *instlist)
{
	did_device_list_t	*dlistp;
	did_subpath_t		*subptr;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	for (dlistp = instlist; dlistp; dlistp = dlistp->next) {
		for (subptr = dlistp->subpath_listp; subptr;
		    subptr = subptr->next) {
			/*
			 * stat(path);
			 */
		}
		if ((instlist->target_device_id == 0) &&
		    (instlist->type != DEVID_NONE)) {
			(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN, "Device id invalid for "
				"instance %d.\n"),
			    dlistp->instance);
			return (-1);
		}

		if (instlist->device_type == NULL) {
			(void) snprintf(did_geterrorstr(), DID_ERRORSTR_SIZE,
			    dgettext(TEXT_DOMAIN, "Device type invalid for "
				"instance %d.\n"),
			    dlistp->instance);
			return (-1);
		}
	}
	return (0);

}

/* See libdid.h for comment-description of function. */
int
did_findInstance(int instnumber, did_device_list_t *instlist,
    did_device_list_t **out)
{
	did_device_list_t	*dlistp = NULL;

	for (dlistp = instlist; dlistp; dlistp = dlistp->next)
		if (dlistp->instance == instnumber) {
			*out =  dlistp;
			return (0);
		}

	*out = NULL;
	return (0);
}

/*
 * This function logs a message to the syslog facility.
 *  priority: should be same as first argument passed to syslog(3C)
 *  prefix: This argument is appended to the SC_SYSLOG_DID_TAG to create
 *        the syslog message tag. This should not be NULL. see
 *        sc_syslog_msg.h for a more detailed explanation of this.
 *  format: a string specifying format of the message.
 */
void
did_logmsg(int priority, const char *prefix, const char *format, ...)
{
	sc_syslog_msg_handle_t handle;
	va_list ap;
	char log_msg[LOG_LINE_MAX];

	(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_DID_TAG, prefix);
	va_start(ap, format);
	vsnprintf(log_msg, LOG_LINE_MAX, format, ap);
	va_end(ap);
	(void) sc_syslog_msg_log(handle, priority, MESSAGE, log_msg);
	sc_syslog_msg_done(&handle);
}

/*
 * This function returns the true if the version of the did sub-system in the
 * cluster supports the extra "flag" value in the did_instances file of
 * the CCR.
 */
boolean_t
is_flags_available(void)
{
	clcomm_vp_version_t vp_version;

	return (B_TRUE);
	/*
	if (clcomm_get_running_version("did", &vp_version) != 0) {

	    clcomm_get_running_version return 2, because ORB cannot be
	    initialised. This happens only during early in the boot process.
	    DID must be available pretty early in the boot process.

	    return (B_FALSE);
	}

	return ((boolean_t)(vp_version.major_num >= 2));
	*/
}

/*
 * This is a private function used to add a string to a array
 * of string pointers dynamically. The array grows in 50 element increments.
 * Using a array this way allows for easier sorting and searching at the
 * cost of the some extra memory.
 * Arguments:
 *     array - A pointer to the array of strings
 *     element - The string element to be added to the "array"
 *     index - at which the element is to be added.
 *     array_size - The current size of the "array". If the size is modified
 *                  the modified value is returned in the variable.
 */
static int
add2Array(char ***array, char *element, int index, int *array_size)
{
	int i = index;
	int sz = *array_size;
	char **arr = *array;

	if (i >= sz) {
	    sz += ARR_INCR;
	    arr = (char **)realloc(arr, sz * sizeof (char *));
	}
	if (arr == NULL) {
	    did_logmsg(LOG_ERR, "", "Not enough memory \n");
	    return (ENOMEM);
	}
	arr[i] = strdup(element);
	if (arr[i] == NULL) {
	    did_logmsg(LOG_ERR, "", "Not enough memory \n");
	    return (ENOMEM);
	}

	*array_size = sz;
	*array = arr;
	return (0);
}

/*
 * This is a private function that walks the list of iSCSI device nodes
 * and populates a array with the devices path. The function handles
 * cases when multi-pathing is configured and when it is not.
 * node - Root node from which the walk starts
 * xDevIdArray - The array containing the list of iSCSI device paths.
 * The caller is responsible to free the memory allocated to the array
 * and its elements.
 * xSize - The number of elements of the array.
 */
static int
walk_disknodes(di_node_t node, char ***xDevIdArray, int *xSize)
{
#if	SOL_VERSION >= __s10
	char *phys_path = NULL;
	di_node_t phci_node, child;
	di_path_t phci_path;
	char **pathArray = NULL;
	int index = 0, arr_sz = ARR_INCR, i;

	node = di_drv_first_node(ISCSI_DRIVER, node);
	while (node != DI_NODE_NIL) {
	/*
	 * If the device node exports no minor nodes,
	 * there is no physical disk.
	 */
	    if (di_minor_next(node, DI_MINOR_NIL) == DI_MINOR_NIL) {
		node = di_drv_next_node(node);
		continue;
	    }

	    pathArray = malloc(arr_sz * sizeof (char *));
	    /* Get Normal iSCSI Nodes */
	    child = di_child_node(node);
	    while (child != DI_NODE_NIL) {
		phys_path = di_devfs_path(child);
		if (phys_path != NULL) {
		    if (add2Array(&pathArray, phys_path, index,
			&arr_sz) != 0) {
			goto error;
		    }
		    index++;
		    di_devfs_path_free(phys_path);
		    phys_path = NULL;
		}
		child = di_sibling_node(child);
	    }

	    /* Get iSCSI Nodes used in MPxIO Configurations */
	    phci_path = di_path_next_client(node, DI_PATH_NIL);
	    while (phci_path != NULL) {
		phci_node = di_path_client_node(phci_path);
		if (phci_node != DI_NODE_NIL) {
		    phys_path = di_devfs_path(phci_node);
		    if (phys_path != NULL) {
			if (add2Array(&pathArray, phys_path, index,
			    &arr_sz) != 0) {
			    goto error;
			}
			index++;
			di_devfs_path_free(phys_path);
			phys_path = NULL;
		    }
		}
		phci_path = di_path_next_client(node, phci_path);
	    }


	    node = di_drv_next_node(node);
	}
	qsort(pathArray, index, sizeof (char*), string_compare);
	error:
	if (phys_path != NULL) {
	    di_devfs_path_free(phys_path);
	    for (i = 0; i < index; i++) {
		free(pathArray[i]);
	    }
	    free(pathArray);
	    index = 0;
	    pathArray = NULL;
	}
	*xDevIdArray = pathArray;
	*xSize = index;
#endif
	return (0);
}

/*
 * This is the compare function that can be used to sort a array of strings.
 * This can be used to do a binary search of a sorted array as well.
 */
int
string_compare(const void *arg1, const void *arg2) {
	const char **carg1, **carg2;
	carg1 = (const char**)arg1;
	carg2 = (const char**)arg2;
	return (strcmp(*carg1, *carg2));
}

/*
 * This function is used to get a sorted array of all the iSCSI device
 * paths in the system.
 * Inputs
 *  iscsi_dev_paths - A pointer to a string array(char **)
 *  count - A pointer to a integer, that can be used to store the count
 *  of the array elements.
 * Outputs
 *   iscsi_dev_paths - The array is filled with the iSCSI device paths
 *   in sorted manner. The caller is responsible for freeing the array
 *   and the elements of the array
 *   count - Contains the number of array elements returned.
 */
int
get_iscsi_devices(char ***iscsi_dev_paths, int *count) {
	int ret;
#if	SOL_VERSION >= __s10
	di_node_t root_node;

	if ((root_node = di_init("/", DINFOCPYALL|DINFOPATH)) == DI_NODE_NIL) {
	    return (NULL);
	}
	ret = walk_disknodes(root_node, iscsi_dev_paths, count);
	di_fini(root_node);
#endif
	return (ret);
}

/*
 * This function can be used to free the array that is returned  as a
 * result of the call to get_iscsi_devices() above.
 * iscsi_dev_paths - A array containing strings to be freed
 * count - Number of elements of the array, iscsi_dev_paths
 */
void
free_iscsi_devices(char **iscsi_dev_paths, int count) {
	int i;

	if (iscsi_dev_paths == NULL) {
	    return;
	}
	for (i = 0; i < count; i++) {
	    if (iscsi_dev_paths[i] != NULL) {
		free(iscsi_dev_paths[i]);
	    }
	}
	free(iscsi_dev_paths);
}
