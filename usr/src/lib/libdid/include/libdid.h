/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_LIBDID_H
#define	_LIBDID_H

#pragma ident	"@(#)libdid.h	1.56	09/01/29 SMI"

#include <sys/dditypes.h>
#include <sys/scsi/generic/inquiry.h>
#include <sys/cl_efi.h>
#ifdef SC_EFI_SUPPORT
#include <sys/didio.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

#define	NPMAP (NDKMAP)

#ifdef SC_EFI_SUPPORT
#define	DID_DISK_TYPE_VTOC	1
#define	DID_DISK_TYPE_EFI	2
#define	DID_DISK_TYPE_CDROM	3
#define	DID_DISK_TYPE_UNKNOWN	4
#define	DID_DISK_TYPE_ERROR	5
#define	DID_DISK_OPEN_ERROR	6
#define	DID_DISK_MEM_ERROR	7
#endif

/*
 * Return values for init.
 */
#define	DID_INIT_SUCCESS	0
#define	DID_INIT_FAIL		-1
#define	DID_INIT_ORBFAIL	-2

/*
 * Used for flag in did_saveinstlist().
 */
#define	REMOVABLE			0x1
#define	NON_REMOVABLE			0x2
#define	FORCE_UPDATE			0x4

#define	GLOBAL_FENCING_KEY		"global_fencing_protocol"

/*
 * Global setting of the algorithm that determines the fencing protocol
 */
#define	GLOBAL_FENCING_UNKNOWN		1
#define	GLOBAL_FENCING_PATHCOUNT	2
#define	GLOBAL_FENCING_PREFER3		3
#define	GLOBAL_NO_FENCING		4

/*
 * Algorithm that determines the fencing protocol for each disk.
 * Note: the per disk values must map to the global values defined above.
 *    FENCING_USE_GLOBAL -> GLOBAL_FENCING_UNKNOWN
 *    FENCING_USE_PATHCOUNT -> GLOBAL_FENCING_PATHCOUNT
 *    FENCING_USE_SCSI3 -> GLOBAL_FENCING_PREFER3
 * It is necessary to make sure that the code in set_disk_default_fencing() in
 * didadm.c works properly.
 */
#define	FENCING_USE_GLOBAL	1	/* Use the global setting */
#define	FENCING_USE_PATHCOUNT	2
#define	FENCING_USE_SCSI3	3
#define	NO_FENCING		7

/* Fencing protocol for individual disk detected by autodiscovery. */
#define	FENCING_NA		4
#define	SCSI2_FENCING		5
#define	SCSI3_FENCING		6

#ifndef DEVID_NONE
/*
 * Device id types
 *
 * These types are actually defined in sys/dditypes.h but before Solaris 9
 * were contingent on #ifdef _KERNEL hence copies were needed in this header
 * file. For Solaris 9 the #ifdef contingency was removed. Rather than make
 * these defines dependent on the specific Solaris version - check for
 * DEVID_NONE being defined. This will eliminate the possibility of future
 * breakage should a subsequent update of Solaris 8 also remove the #ifdef.
 */
#define	DEVID_NONE	0
#define	DEVID_SCSI3_WWN 1
#define	DEVID_SCSI_SERIAL	2
#define	DEVID_FAB	3
#define	DEVID_ENCAP	4
#define	DEVID_MAXTYPE	4
#endif /* DEVID_NONE */

/*
 * These types provide the mapping names to the DID instance
 * cache. Each map provides an interface to resolve the necessary
 * data for common operations.
 */
#define	DID_INST_TABLE		"did_instances"
#define	DID_MAP			DID_INST_TABLE"/did_map"
#define	DID_REPL_TABLE		"replicated_devices"

/* CCR table indicates that the cluster is currently using Prefer3 algorithm */
#define	GLOBAL_FENCING_TABLE	"global_fencing"
#define	GLOBAL_FENCING_MAP	"global_fencing_map"

/*
 * Used for emulating Solaris 8 device id algorithm.
 */
#define	VPD_HEAD_OFFSET		3	/* size of head for vpd page */
#define	VPD_PAGE_LENGTH		3	/* offset for pge length data */
#define	VPD_SERIAL_NUM_PAGE	1	/* offset for pge length data */
#define	VPD_DEVICE_ID_PAGE	2	/* offset for pge length data */
#define	OTHER_DEV_TYPE		0
#define	SSD_DEV_TYPE		1
#define	SD_DEV_TYPE		2
#define	KSTAT_DATA_CHAR_LEN	16

/*
 * Used as a prefix to syslog messages from the DID subsystem
 */
#define	SC_SYSLOG_DID_TAG	"Cluster.DID"

/*
 * Used by the did reloader daemon and scgdevs
 */
#define	RUNNING_DIR	"/var/run"
#define	LOCK_FILE	"did_reloader.lock"

#define	ISCSI_DRIVER    "iscsi" /* iSCSI Initiator driver name */

/*
 * These are the valid values for the flags field in
 * the did_device_list_t structure.
 */
/* Default transport protocol to device */
#define	DEFAULT_T_PROTOCOL	0x00000001
/* Device connected via iSCSI protocol */
#define	ISCSI_T_PROTOCOL	0x00000002

typedef struct did_device_type {
	int			minor_count;
	char			type_name[MAXNAMELEN];
	int			type_num;
	char			primary_subdir[MAXPATHLEN];
	char			discovery_minor[MAXPATHLEN];
	int			name_offset_from_inst;
	int			default_nodes;
	int			start_default_nodes;
	int			next_default_node;
	minor_t			minor_num[DID_MAX_MINORS];
	char			minor_name[DID_MAX_MINORS][MAXPATHLEN];
	char			cur_minor_name[DID_MAX_MINORS][MAXPATHLEN];
	int			device_class[DID_MAX_MINORS];
	struct did_device_type	*next;
} did_device_type_t;

/*
 * Admin instance data structure: a linked list of mulitpathed disks. This
 * data structure is built during did initialization by the admin instance
 * in reponse to DMP_INIT IOCTL calls.  Once complete the information it
 * contains is supplied to each instance as they attach
 */

typedef struct did_subpath {
	char	device_path[MAXPATHLEN];
	dev_t   device[DID_MAX_MINORS];
	int	state;
	struct did_subpath *next;
	struct did_device_list *instptr;
} did_subpath_t;

/*
 * Used when building device list
 */
typedef struct did_device_list {
	ddi_devid_t		target_device_id;
	char			*diskid;
	unsigned short		diskid_len;
	unsigned short		type;
	/* algorithm to use to determine the fencing  protocol */
	unsigned short		default_fencing;
	/* Fencing protocol detected by auto discovery */
	unsigned short		detected_fencing;
	int			instance;
	int			minor_count;
	/*
	 * See #define above for valid values.
	 */
	unsigned int		flags;
	did_subpath_t		*subpath_listp;
	struct did_device_type	*device_type;
	struct did_device_list	*next;
} did_device_list_t;

typedef struct did_repl_path {
	char			device_path[MAXPATHLEN];
	struct did_repl_path	*next;
} did_repl_path_t;

typedef struct did_repl_list {
	char			*diskid;
	int			instance;
	int			orig_inst;
	did_repl_path_t		*device_path_list;
	char			dev_group[MAXNAMELEN];
	char			repl_type[MAXNAMELEN];
	int			remove;
	struct did_repl_list	*next;
} did_repl_list_t;

typedef enum {
	DID_NONE,
	DID_CONF_FILE,
	DID_CCR_FILE,
	DID_CCR
} did_if_t;

/*
 * Used for emulating Solaris 8 device id algorithm.
 */
struct vpd_inq_data {
	int fd;
	int dev_type;
	unsigned short devid_type;
	unsigned char supported_page_mask;
	unsigned int devid_len;
	unsigned int vpd_serial_num_len;
	unsigned int vpd_wwn_len;
	char *dev_path;
	unsigned char *vpd_wwn;
	unsigned char *vpd_serial_num;
	struct scsi_inquiry *inq_data;
};


/*
 * Used to check the replication status of a device.
 */
typedef enum {
	DID_REPL_FALSE,
	DID_REPL_BAD_DEV,
	DID_REPL_ERROR,
	DID_REPL_TRUE
} did_repl_t;

/*
 * Internal did/didadm functions. These should not be used
 * by any consumers except for didadm.
 */

/*
 * This function returns an array that is set with an error value when
 * an error is * encountered in the internal did/didadm functions. It
 * functions precisely like errno, and the same cautions should be applied as
 * when using errno.  Returns the per thread did error string.
 */
extern char *did_geterrorstr(void);
#define	DID_ERRORSTR_SIZE	256

/*
 * did_findInstance()
 * This function searches for an instance in the did device list
 * passed to it. It will only return the first matching instance
 * number (there should never be more than one). This will be done
 * via the inst argument. If no instance is found, inst will be NULL.
 * This function returns 0 on success, -1 on failure, and sets
 * did_geterrorstr().
 */
extern int did_findInstance(int instnumber,
			did_device_list_t *instlist, did_device_list_t **out);

/*
 * did_typetostr()
 * This function searches for the type string that matches the
 * short type passed in. It is assumed that the typestr variable
 * will contain allocated memory. It will use typestr to return only the
 * first matching type since there should never be more than one. If no
 * type is found, typestr is given a value of DEVID_NONE.
 * The return value of the function is 0 on success, -1 on failure, and
 * did_geterrorstr() is set appropriately.
 */
extern int did_typetostr(unsigned short type, char *out, int strsize);

/*
 * did_sort_instances()
 * This function currently is a no-op.
 */
extern void did_sort_instances(did_device_list_t *instlist);

/*
 * did_diskidtostr ()
 * This function takes a character-based device id and converts it into
 * a string of hex characters.
 * If the length of the input array is 0, "out" is set to a string of "0".
 * If the length of the array is not 0, each character is converted to
 * a hexadecimal value, and concatenated into the "out" variable.
 * On error this function returns -1, and sets did_geterrorstr().
 * It returns 0 on success.
 */
extern int did_diskidtostr(char *id, int idlen, char **out);

/*
 * did_diskfencingtostr ()
 */
extern int did_diskfencingtostr(unsigned short default_fencing, char **out);

/*
 * did_globalfencingtostr ()
 */
extern int did_globalfencingtostr(unsigned short globalfencing, char **out);

/*
 * did_diskidtoascii()
 * This function takes a character-based device id and converts it to a
 * printable string. If no printable characters are found, it returns
 * an empty (first character set to NULL) static array.
 */
extern char *did_diskidtoascii(char *id, int idlen);

extern int did_initlibrary(int version, did_if_t mode, char *tablename);
extern int did_getinstlist(did_device_list_t **ptr, int mode,
	did_device_type_t *typelist, char *fmtfile);
extern int did_getrepllist(did_repl_list_t **ptr);
extern int did_getinst(did_device_list_t **ptr, int inst);
extern int did_saveinstlist(did_device_list_t *ptr, char *savefile, char *node,
	int flag);
extern int did_saveinst(did_device_list_t *ptr, int create);
extern int did_add_repl_entry(did_repl_list_t *repldata);
extern int did_rm_repl_entry(int key);
extern int did_read_repl_entry(int key, char **data);
extern int did_freeinstlist(did_device_list_t *ptr);
int did_allocinstance(did_subpath_t *subptr, did_device_list_t **instlist,
	char *node);
int did_allocinstances(did_device_list_t *alloclist,
	did_device_list_t **instlist, char *node);
int did_gettypelist(did_device_type_t **list);
int did_getglobalfencingstatus(unsigned short *curstatus);
int did_setglobalfencingstatus(unsigned short newstatus);
int did_exec_program_local(char *pgm);
int did_syncgdevs(void);
int did_syncrepl(char *exec_node, char *command);
int did_holdccrlock(void);
int did_releaseccrlock(void);
int did_abortccrlock(void);
int did_populate_list_cache(did_device_list_t *ptr);

/*
 * All functions below this point are publically available DID
 * functions. These can be used by other cluster consumers.
 */

/*
 * list_all_devices()
 * This function returns a linked list of all devices that DID is
 * aware of in the cluster with the device type that matches the
 * argument dtype. Currently, the valid arguments for dtype are
 * "disk" and "tape". The disk type currently covers both cdroms
 * and disks. If dtype is NULL, all DID devices in the cluster are
 * returned. The data is returned in the form of a linked list of
 * devices. On error, or if no devices are found, this function will
 * return NULL, and set the did_geterrorstr() if an error was found.
 *
 * The memory returned by this function must be released by the
 * caller after use via the free_did_list() function.
 */
extern did_device_list_t *list_all_devices(char *dtype);

extern int read_global_fencing_status_from_ccr(unsigned short *status);

/*
 * map_to_did_device()
 * This function returns a linked list of all DID devices that map
 * to the devname supplied. The devname may be in one of two formats.
 * One acceptable format is the logical name of the device. For disks,
 * this would be cXtXdX. Currently, the cXtXdXsX form is not accepted.
 * Additionally, this interface will take a DID name (e.g. d1) and
 * return a pointer to the DID structure representing this device.
 * This exists so that a user may confirm that a given DID name is
 * valid.
 * The remote argument exists so the user may specify whether they wish
 * to look just on the local node for a given device name (remote == 0),
 * or on all nodes of the cluster (remote != 0). If remote is set,
 * note that it is likely that multiple did devices will be returned.
 * Local node, in this definition, means the node that the library
 * has been invoked on. The remote argument is unused if a DID name is
 * provided as the devname.
 * On error, this function returns NULL and sets did_geterrorstr().
 *
 * The memory returned by this function must be released by the
 * caller after use via the free_did_list() function.
 */
extern did_device_list_t *map_to_did_device(char *devname, int remote);

/*
 * map_from_did_device()
 * This function returns a pointer to the did_device_list_t structure
 * representing the DID device supplied by didname. If there is no such
 * device, the function returns NULL.
 * On error, this function returns NULL and sets did_geterrorstr().
 *
 * The memory returned by this function must be released by the
 * caller after use via the free_did_list() function.
 */
extern did_device_list_t *map_from_did_device(char *didname);

/*
 * check_for_repl_device()
 * This function checks to see if the specified device has been marked
 * as a replicated device.
 */
extern did_repl_t check_for_repl_device(char *device, did_repl_list_t *data);

/*
 * did_get_instance()
 * This function returns the instance value for the DID structure
 * it is passed. On error, it returns -1 and sets did_geterrorstr().
 */
extern int did_get_instance(did_device_list_t *ptr);

/*
 * did_get_devid_type()
 * This function returns the instance value for the DID structure
 * it is passed. On error, it returns -1 and sets did_geterrorstr().
 */
extern short did_get_devid_type(did_device_list_t *ptr);

/*
 * did_get_devid()
 * This function returns the device id for the DID structure it is
 * passed. On error, it returns NULL, and sets did_geterrorstr().
 */
extern ddi_devid_t did_get_devid(did_device_list_t *ptr);

/*
 * did_get_path()
 * This function takes a DID structure and returns a list of logical
 * device paths associated with that DID device. These paths are
 * space delimited, and do not contain the minor portion of the device.
 * So, the return array for a disk would look something like:
 *	"node-1:/dev/rdsk/c0t0d0 node-2:/dev/rdsk/c1t0d2"
 * On error, this function returns NULL and sets did_geterrorstr().
 */
extern char *did_get_path(did_device_list_t *ptr);

/*
 * did_get_num_paths()
 * This function takes a DID structure and returns the number of subpaths
 * which exist for that device.
 * On error, this function returns -1.
 */
extern int did_get_num_paths(did_device_list_t *ptr);

/*
 * did_get_did_path()
 * This function returns the relative did path for the device specified.
 * For a disk, this looks like "rdsk/d1", and for a tape, this looks like
 * "rmt/1". This function returns NULL on error and sets did_geterrorstr().
 */
extern char *did_get_did_path(did_device_list_t *ptr);

/*
 * did_get_minor_count()
 * This function returns the number of minors for this DID device.
 * It returns -1 on error and sets did_geterrorstr().
 */
extern int did_get_minor_count(did_device_list_t *ptr);

/*
 * did_get_alias()
 * This function currently always returns NULL.
 */
extern char *did_get_alias(did_device_list_t *ptr);

/*
 * did_delete_devices_by_node()
 * This function takes a node name as input.
 * It updates the appropriate did CCR table, removing all references
 * to the node name it was given. It is strongly advised that the node
 * being deleted is not in the cluster during deletion, otherwise
 * certain system events can cause DID paths to be re-added.
 * This function returns 0 on success and -1 on failure.
 */
extern int did_delete_devices_by_node(char *nodename);

extern int get_solaris_devid(char *device, int version,
    unsigned char *devidbuff, did_device_list_t *instptr);

/*
 * This function logs a message to the syslog facility.
 *  priority: should be same as first argument passed to syslog(3C)
 *  prefix:This argument is appended to the SC_SYSLOG_DID_TAG to create
 *        the syslog message tag. This argument should not be NULL.
 *  format: a string specifying format of the message.
 */
extern void did_logmsg(int priority, const char *prefix,
	const char *format, ...);

/*
 * This function returns the true if the version of the did sub-system in the
 * cluster supports the extra "flag" value in the did_instances file of
 * the CCR.
 */
extern boolean_t is_flags_available(void);

/*
 * This function is used to get a sorted array of all the iSCSI device
 * paths in the system.
 * Inputs
 *  iscsi_dev_paths - A pointer to a string array(char **)
 *  count - A pointer to a integer, that can be used to store the count
 *  of the array elements.
 * Outputs
 *   iscsi_dev_paths - The array is filled with the iSCSI device paths
 *   in sorted manner. The caller is responsible for freeing the array
 *   and the elements of the array
 *   count - Contains the number of array elements returned.
 */
extern int get_iscsi_devices(char ***iscsi_dev_paths, int *count);

/*
 * This is the compare function that can be used to sort a array of strings.
 * This can be used to do a binary search of a sorted array as well.
 */
extern int string_compare(const void *arg1, const void *arg2);

/*
 * This function can be used to free the array that is returned  as a
 * result of the call to get_iscsi_devices() above.
 * iscsi_dev_paths - A array containing strings to be freed
 * count - Number of elements of the array, iscsi_dev_paths
 */
extern void free_iscsi_devices(char **iscsi_dev_paths, int count);

#ifdef SC_EFI_SUPPORT
/*
 * did_efi_disk(const char *name)
 * This functions returns the disk format
 * EFI (DID_DISK_TYPE_EFI)
 * VTOC (DID_DISK_TYPE_VTOC)
 * CDROM (DID_DISK_TYPE_CDROM)
 * If the disk type cannot be determined (because, for example,
 * disk has a reservation) UNKNOWN is returned (DID_DISK_TYPE_UNKNOWN).
 * If the device name cannot be opened or ioctl fails,
 * DID_DISK_TYPE_ERROR is returned.
 */
extern int did_efi_disk(const char *name);
#endif

#ifdef __cplusplus
}
#endif

#endif /* _LIBDID_H */
