/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
#ifndef _RLDR_LINKLIST_H
#define	_RLDR_LINKLIST_H

typedef struct _iscsi_dev {
    char *dev_path;
    int attach_complete;
    int didload_complete;
    struct _iscsi_dev *next;
} iscsi_dev;

typedef struct _list {
    iscsi_dev *head;
    iscsi_dev *tail;
    int size;
} rldr_devlist_t;

int initRldrList(rldr_devlist_t **xList);
int insertAfter(rldr_devlist_t *xList, iscsi_dev *element, char *device_path);
void deleteNext(rldr_devlist_t *xList, iscsi_dev *element);
void moveElementAfter(rldr_devlist_t *xSourceList, iscsi_dev *element,
	rldr_devlist_t *xDestList);
static void freeElem(iscsi_dev *element);
void emptyRldrList(rldr_devlist_t *xList);
void freeRldrList(rldr_devlist_t *xList);
#endif // _RLDR_LINKLIST_H
