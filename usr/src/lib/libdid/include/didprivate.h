/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_DIDPRIVATE_H
#define	_DIDPRIVATE_H

#pragma ident	"@(#)didprivate.h	1.33	08/05/20 SMI"

#include <sys/clconf.h>
#include <assert.h>

#ifdef __cplusplus
extern "C" {
#endif

#define	DID_LIB_INTERFACE_VERSION	1

#define	DID_CCR_STRING_PAD		1024


/* libdid interfaces */
int did_impl_populate_list_cache(did_device_list_t *ptr);
int did_impl_getglobalfencingstatus(unsigned short *status);
int did_impl_setglobalfencingstatus(unsigned short newstatus);
int did_impl_holdccrlock(void);
int did_impl_releaseccrlock(int);
int did_impl_abortccrlock(void);
int did_impl_getnodeid(char *nodename, nodeid_t *nodeid);
int did_impl_getnodename(nodeid_t nodeid, char *nodename);
int did_impl_getinstlist(did_device_list_t **ptr, int mode,
	did_device_type_t *typelist, char *fmtfile);
int did_impl_getrepllist(did_repl_list_t **ptr);
int did_impl_getinst(did_device_list_t **ptr, int inst);
int did_impl_freeinstlist(did_device_list_t *ptr);
int did_impl_saveinstlist(did_device_list_t *ptr, char *savefile, char *node,
	int flag);
int did_impl_saveinst(did_device_list_t *ptr, int create);
int did_impl_add_repl_entry(did_repl_list_t *repldata);
int did_impl_rm_repl_entry(int key);
int did_impl_read_repl_entry(int key, char **data);
int did_impl_allocinstance(did_subpath_t *subptr,
	did_device_list_t **instlist, char *node);
int did_impl_allocinstances(did_device_list_t *alloclist,
	did_device_list_t **instlist, char *node);
int did_impl_gettypelist(did_device_type_t **list);
int did_impl_savetypelist(did_device_type_t *list);
int did_impl_mapdidtodev(char *did_dev, did_subpath_t *dev_path);
int did_impl_mapdevtodid(char *did_dev, did_subpath_t *dev_path);
int did_impl_exec_program_local(char *pgm);
int did_impl_syncgdevs();
int did_impl_syncrepl(char *exec_node, char *command);
int did_impl_initlibspecific();

/*
 * libdid internal prototypes:
 */

extern int did_getnodeid(char *nodename, nodeid_t *nodeid);
extern int did_getnodename(nodeid_t nodeid, char *nodename);
extern int did_checkRecordDescription(did_device_list_t *instlist);
extern void did_strtodiskid(char *strid, did_device_list_t *insptr);
extern int did_strtodiskfencing(char *strid, did_device_list_t *insptr);
extern void did_set_type(ddi_devid_t devid, unsigned short type);
extern unsigned short did_strtotype(char *strtype);
extern void did_device_list_free(did_device_list_t *list);
extern void did_repl_list_free(did_repl_list_t *list);
extern void did_device_list_entry_free(did_device_list_t *entry);
extern void did_repl_list_entry_free(did_repl_list_t *entry);
extern did_device_list_t *did_ccr_find_instance(int instnum,
	did_device_list_t *instlist);
extern did_device_type_t *did_ccr_find_device_type(const char *,
	did_device_type_t *);


/*
 * Create a device id out of a replicated_device entry and add it to the
 * given instance.
 *
 * If no error happens when parsing the string, we return 0. Otherwise,
 * -1 is returned and did_geterrorstr() is set.
 */
extern int did_repl_str_to_instance(char *key, char *record,
    did_repl_list_t *repl);

/*
 * Create a did_device_list_t struct from a CCR string representation given
 * by ccrstr, with instance number instnum. If instlist is non-NULL, insert
 * the newly created instance into the list in an ascending order of instance
 * number. Note that the instance number shouldn't be part of the CCR
 * string. typelist is used to determine if the instance device type is
 * valid or not, based on whether it exists in typelist or not.
 *
 * If no error happens when parsing the string, we return 0. Otherwise,
 * -1 is returned and did_geterrorstr() is set.
 */
extern int did_ccr_str_to_instance(const char *instnum,
				char *ccrstr, did_device_list_t **instlist,
				did_device_type_t *typelist);

/*
 * Create a CCR string representation of the given instance. The instance
 * number  is not included in the created string. The callee is reponsible
 * for freeing the string after its use.
 */
extern int did_ccr_instance_to_str(did_device_list_t *instp, char **out);

/*
 * Create a did_device_type_t structure from a CCR string representation
 * (the key shouldn't be part of this). 'type' refers to the key to the
 * strin gbeing passed. If 'typelist' is non-NULL, insert the newly
 * created structure to typelist, detecting any pre-existence.
 *
 * The new did_device_type_t is returned in rettype if the above succeeds,
 * and the function returns 0.  On failure, the function returns -1 and
 * sets the did_geterrorstr().
 */
extern int did_ccr_str_to_device_type(const char *type,
				char *record, did_device_type_t **typelist);

/*
 * Create a CCR string representation of a did_device_type_t. The key
 * (either "disk" or "tape") will not be included in the string. The
 *  callee is responsible for freeing the string after its use.
 */
extern int did_ccr_device_type_to_str(did_device_type_t *type, char **out);

extern char *did_geterrorstr(void);


/*
 * DID_LOCK is called by all DID libraries API functions
 */
#define	DID_LOCK(x) \
{ \
	int lck_ret; \
	did_lib_pthread_init(); \
	lck_ret = pthread_mutex_lock(&x); \
	assert(lck_ret == 0); \
}


/*
 * DID_UNLOCK is called by all DID libraries API functions
 */
#define	DID_UNLOCK(x) \
{ \
	int lck_ret; \
	lck_ret = pthread_mutex_unlock(&x); \
	assert(lck_ret == 0); \
}

/*
 * scsi devid invalid type definition
 */
#define	DEVID_INVALID	-1


#ifdef __cplusplus
}
#endif

#endif /* _DIDPRIVATE_H */
