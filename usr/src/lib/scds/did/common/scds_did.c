/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scds_did.c	1.49	08/05/20 SMI"

/*
 * scds_did.so Raw Disk configuration functions
 */

#include "scds_did.h"
#include <unistd.h>

#define	SCDG_DCS_SERVICE_TYPE "DISK"
#define	SCDG_SCCONF_SERVICE_TYPE "rawdisk"
#define	SCDG_MAX_NODE_PREFERENCE 64
#define	SCDG_NUM_DEV_SLICES 8
#define	SCDG_RAWDISK_DEVICE_PATH "/dev/did"
#define	SCDG_LOCAL_ONLY "local_only"
#define	SCDG_GDEV "gdev"

static scconf_errno_t ds_get_globaldev_list(char **globaldev_list[],
    uint_t *num_gdev, char *gdev_list, scconf_state_t *local_only,
    scconf_state_t *autogen);
static scconf_errno_t ds_get_globaldev_range_seq(char **globaldev_list,
    uint_t num_of_gdev, dc_gdev_range_seq_t **gdevs_range_seq);
static uint_t ds_get_num_of_nodes(char **dsnodes);
static scconf_errno_t ds_get_node_seq(char **dsnodes, dc_replica_seq_t *
    dsnodes_seq, dc_replica_seq_t **nodes_seq, scconf_state_t nodepreference,
    uint_t num_nodes, scconf_state_t local_only);
static scconf_errno_t ds_set_node_seq(char **dsnodes, dc_replica_seq_t *
    dsnodes_seq, dc_replica_seq_t **nodes_seq, scconf_state_t nodepreference,
    uint_t num_nodes, scconf_state_t local_only, char **messages);
static scconf_errno_t ds_set_nodes_preference(char ** dsnodes,
    dc_replica_t **nodes, scconf_state_t nodepreference);
static scconf_errno_t ds_confirm_device_conn_to_host(char **globaldev_list,
    char **dsnodes, char **messages);
static boolean_t ds_confirm_is_local_only_dg(
    dc_property_seq_t *dsproperty_seq);
static char *ds_get_property(char *name, dc_property_seq_t *dsproperty_seq);
static dc_error_t ds_get_local_property_seq(dc_property_seq_t **property_seq,
    scconf_nodeid_t nodeid);
static dc_error_t ds_get_global_property_seq(dc_property_seq_t **property_seq,
    char **globaldev_list);
static dc_error_t ds_remove_property_seq(char *dsname,
    dc_property_seq_t *dsproperty_seq);
static dc_error_t ds_change_global_property_seq(char *dsname,
    char **globaldev_list, dc_property_seq_t *dsproperty_seq,
    boolean_t isremove);
static scconf_errno_t ds_get_gdevnames(char *gdevlist, char ***out_gdevlist);
static void ds_free_gdevlist(char **gdev_list);
static void ds_free_gdevs_range_seq(dc_gdev_range_seq_t *gdevs_range_seq);
static void ds_free_nodes_seq(dc_replica_seq_t *nodes_seq);
static scconf_errno_t ds_dcs_create_service_to_scconf_err_status(dc_error_t);
static scconf_errno_t ds_dcs_to_scconf_err_status(dc_error_t dc_err);
static dc_error_t ds_change_service_parameters(char *service_name,
    int failback_enabled, dc_replica_seq_t *nodes_seq,
    dc_gdev_range_seq_t *gdevs_range_seq, char **globaldev_list,
    dc_property_seq_t *dsproperty_seq, scconf_state_t local_only,
    boolean_t is_local_only_dg, scconf_state_t autogen,
    unsigned int dsnumsecondaries, char *repl_type, int set_repl);
static dc_error_t ds_add_nodes(char *service_name,
    dc_replica_seq_t *nodes_seq);
static void ds_get_failback_value(scconf_state_t dsfailback, int *
    failback_value, int enable_failback);
static scconf_errno_t ds_get_target_nodes(dc_replica_seq_t *dsnodes_seq,
    char **dsnodes[], uint_t num_of_nodes);
static scconf_errno_t ds_get_target_gdevs(
    dc_gdev_range_seq_t *dsgdevs_range_seq, char **globaldev_list[],
    uint_t num_of_gdevs);
static scconf_errno_t ds_get_globaldevname(minor_t start_gmin,
    char *devnamep);
static scconf_errno_t ds_get_device_list(char *searchpath,
    scconf_namelist_t *current_ptr, const uint_t iflag,
    scconf_namelist_t **ds_devicenames);
static void ds_free_namelist_local(scconf_namelist_t *namelist);
static scconf_errno_t ds_get_nodeids(char *dsnodes[],
    scconf_nodeid_t **dsnodeids);
static scconf_errno_t add_repl_property(dc_property_seq_t **property_seq,
    char *repl_type);
void add_replication(dc_property_seq_t *dsproperty_seq, boolean_t cur_repl,
    int *new_repl_val);

/*
 * scds_add_ds_local for raw disk
 *
 * Add a new raw disk device group(local or non local) or change the
 * configuration of the existing raw disk group in the DCS.
 * First check if device group already exist in DCS system, if it exists
 * then make sure if all the nodes in the device group have physical
 * connection to all the devices, if not physically connected then return
 * error otherwise add new nodes or devices to the device group. If device
 * group doesn't exist then check the connection again and create the new
 * device group in the DCS.
 *
 *  This "local" function will only operate on the local node.  In cases
 *  where RPC is needed to send the request to the node where the disk group
 *  is imported, the non-local function should be used.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EEXIST		- device group already exists
 *	SCCONF_ENOEXIST		- one or more devices or nodes not found
 *	SCCONF_EINUSE		- the device is already in use
 *	SCCONF_EBUSY		- busy, try again later
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EUNKNOWN		- unknown type
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_ERANGE		- operation not allowed because of out of range
 *				  numsecondaries
 *	SCCONF_DS_EINVAL	- inconsistencies detected in device
 *				  configuration
 */
/*lint -e715 */
/*ARGSUSED*/
scconf_errno_t
scds_add_ds_local(char *dsname, char *dsnodes[], scconf_state_t dspreference,
    scconf_gdev_range_t *dsdevices, scconf_state_t dsfailback,
    scconf_cfg_prop_t *dspropertylist, char *dsoptions,
    unsigned int dsnumsecondaries, char **messages)
{
	dc_error_t dc_err = DCS_SUCCESS;
	scconf_errno_t rstatus = SCCONF_NOERR;
	uint_t num_of_nodes = 0;
	uint_t num_of_gdevs = 0;
	int enable_failback = 0;
	int failback_value = 0;
	char **globaldev_list = NULL;
	char *service_class_name = NULL;
	char *repl_type = NULL;
	dc_gdev_range_seq_t *gdevs_range_seq = (dc_gdev_range_seq_t *)0;
	dc_replica_seq_t *nodes_seq = (dc_replica_seq_t *)0;
	dc_gdev_range_seq_t *dsgdevs_range_seq = (dc_gdev_range_seq_t *)0;
	dc_replica_seq_t *dsnodes_seq = (dc_replica_seq_t *)0;
	dc_property_seq_t *dsproperty_seq = (dc_property_seq_t *)0;
	scconf_state_t local_only = SCCONF_STATE_UNCHANGED;
	scconf_state_t autogen = SCCONF_STATE_UNCHANGED;
	boolean_t is_local_only_dg = B_FALSE;
	boolean_t repl_val = B_FALSE;
	int set_replication;

	/* Initialize */
	dsdevices = NULL;
	dspropertylist = NULL;

	/* Check agruments */
	if (dsname == NULL)
		return (SCCONF_EINVAL);

	/* Determine if device group exist */
	dc_err = dcs_get_service_parameters(dsname, &service_class_name,
	    &enable_failback, &dsnodes_seq, NULL, &dsgdevs_range_seq,
	    &dsproperty_seq);

	/*
	 * If the add command is issued with anything other then
	 * rawdisk device group.
	 */
	if ((dc_err == DCS_SUCCESS) &&
	    (strcmp(service_class_name, SCDG_DCS_SERVICE_TYPE) != 0)) {
		rstatus = SCCONF_EINVAL;
		goto cleanup;
	}

	/* Check if  disk group exist or doesn't exist */
	if ((dc_err != DCS_SUCCESS) && (dc_err != DCS_ERR_SERVICE_NAME)) {
		rstatus = ds_dcs_to_scconf_err_status(dc_err);
		goto cleanup;
	}

	/*
	 * If the device group exists, change in numsecondaries not allowed.
	 * We could silently ignore it, or allow the command to go through
	 * if the new value is the same as the old value. But to be
	 * consistent, less confusing, and theoretically correct during a
	 * race, we fail the command.
	 */
	if ((dc_err == DCS_SUCCESS) &&
	    (dsnumsecondaries != SCCONF_NUMSECONDARIES_UNSET)) {
		rstatus = SCCONF_EINVAL;
		goto cleanup;
	}

	/* Check agruments */
	if ((dsoptions != NULL) && (*dsoptions)) {

		/* Get list of all devices */
		rstatus = ds_get_globaldev_list(&globaldev_list, &num_of_gdevs,
		    dsoptions, &local_only, &autogen);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		if ((autogen == SCCONF_STATE_ENABLED) || (autogen ==
		    SCCONF_STATE_DISABLED)) {
			rstatus = SCCONF_EINVAL;
			goto cleanup;
		}

		/* Populate gdevs_range_seq structure */
		rstatus = ds_get_globaldev_range_seq(globaldev_list,
		    num_of_gdevs, &gdevs_range_seq);

		if (rstatus != SCCONF_NOERR)
			goto cleanup;
	}

	/* If disk group doesn't exist */
	if (dc_err == DCS_ERR_SERVICE_NAME) {
		/* If no global devices are specified */
		if ((num_of_gdevs == 0) ||(dsnodes == NULL)) {
			rstatus = SCCONF_EINVAL;
			goto cleanup;
		}
		dsnodes_seq = NULL;
		if (dspreference == SCCONF_STATE_UNCHANGED)
			dspreference = SCCONF_STATE_DISABLED;

		/* Verify replication */
		rstatus = scconf_verify_replication(globaldev_list, dsname,
		    &repl_val, &repl_type, messages);

		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}
	}

	/* Get the failback value */
	ds_get_failback_value(dsfailback, &failback_value, enable_failback);

	if (dsnodes) {
		num_of_nodes = ds_get_num_of_nodes(dsnodes);
		if (local_only == SCCONF_STATE_ENABLED) {
			/*
			 * If more than one node is specified then
			 * report error.
			 */
			if (num_of_nodes > 1) {
				rstatus = SCCONF_EINVAL;
				goto cleanup;
			}
		}

		/*
		 * If numsecondaries is specified, this must be a new device
		 * group. We fail if the value is too large.
		 */
		if (dsnumsecondaries != SCCONF_NUMSECONDARIES_UNSET) {
			if (dsnumsecondaries > (num_of_nodes - 1)) {
				rstatus = SCCONF_ERANGE;
				goto cleanup;
			}
		}


		/* Populate nodes_seq structure */
		rstatus = ds_get_node_seq(dsnodes, dsnodes_seq,
		    &nodes_seq, dspreference, num_of_nodes, local_only);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

	}

	/* If device group already exist */
	if (dc_err == DCS_SUCCESS) {
		/* If device group is local only device group */
		is_local_only_dg = ds_confirm_is_local_only_dg(dsproperty_seq);
		if (is_local_only_dg) {
			/* Report error if nodes are specified */
			if (dsnodes != NULL) {
				rstatus = SCCONF_EINVAL;
				goto cleanup;
			}
		}

		/* Local to non local and non local to local is not allowed */
		if ((is_local_only_dg) && (local_only ==
		    SCCONF_STATE_DISABLED)) {
			rstatus = SCCONF_EINVAL;
			goto cleanup;
		} else if ((!is_local_only_dg) && (local_only ==
		    SCCONF_STATE_ENABLED)) {
			rstatus = SCCONF_EINVAL;
			goto cleanup;
		}

		if (dsnodes_seq != (dc_replica_seq_t *)0) {
			/* Get list of all the previous and current nodes */
			rstatus = ds_get_target_nodes(dsnodes_seq, &dsnodes,
			    num_of_nodes);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
		}

		if (dsgdevs_range_seq != (dc_gdev_range_seq_t *)0) {

			/* Get list of all the previous and current devices */
			rstatus = ds_get_target_gdevs(dsgdevs_range_seq,
			    &globaldev_list, num_of_gdevs);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
		}

		/* Verify replication */
		rstatus = scconf_verify_replication(globaldev_list, dsname,
		    &repl_val, &repl_type, messages);
		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}

		/* Find out if replication need to be added or not */
		add_replication(dsproperty_seq, repl_val, &set_replication);

		/* Confirm the physical connection between nodes and devices */
		rstatus = ds_confirm_device_conn_to_host(globaldev_list,
		    dsnodes, messages);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;
		/* Change the configuration for existing device group */
		dc_err = ds_change_service_parameters(dsname, failback_value,
		    nodes_seq, gdevs_range_seq, globaldev_list, dsproperty_seq,
		    local_only, is_local_only_dg, autogen,
		    SCCONF_NUMSECONDARIES_UNSET, repl_type, set_replication);
		if (dc_err != DCS_SUCCESS) {
			rstatus = ds_dcs_to_scconf_err_status(dc_err);
		}
	}

	/* If device group name doesn't exist */
	else if (dc_err == DCS_ERR_SERVICE_NAME) {

		/*
		 * Confirm the physical connection between nodes
		 *   and devices
		 */
		rstatus = ds_confirm_device_conn_to_host(globaldev_list,
		    dsnodes, messages);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* If device group is local only device group. */
		if (local_only == SCCONF_STATE_ENABLED) {
			dc_err = ds_get_local_property_seq(&dsproperty_seq,
			    nodes_seq->replicas[0].id);
			if (dc_err != DCS_SUCCESS) {
				rstatus = ds_dcs_to_scconf_err_status(dc_err);
				if (rstatus != SCCONF_NOERR)
					goto cleanup;
			}
		} else {
			dc_err = ds_get_global_property_seq(&dsproperty_seq,
			    globaldev_list);
			if (dc_err != DCS_SUCCESS) {
				rstatus = ds_dcs_to_scconf_err_status(dc_err);
				if (rstatus != SCCONF_NOERR)
					goto cleanup;
			}
		}

		/* Add replication property if repl_val is B_TRUE */
		/*lint -save -e644 : repl_val would have some value */
		if (repl_val == B_TRUE) {
			rstatus = add_repl_property(&dsproperty_seq, repl_type);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
		}
		/*lint -restore */

		/*
		 * We should use the default value if the user didn't
		 * specify the numsec value during for a service creation.
		 */
		if (dsnumsecondaries == SCCONF_NUMSECONDARIES_UNSET) {
			dsnumsecondaries = SCCONF_NUMSECONDARIES_SET_DEFAULT;
		}

		/* Create new device group */
		dc_err = dcs_create_service(dsname, SCDG_DCS_SERVICE_TYPE,
			failback_value, nodes_seq, dsnumsecondaries,
			gdevs_range_seq, dsproperty_seq);
		if (dc_err != DCS_SUCCESS) {
			rstatus =
			    ds_dcs_create_service_to_scconf_err_status(dc_err);
		}
	} else
		rstatus = ds_dcs_to_scconf_err_status(dc_err);

cleanup:
	dcs_free_dc_replica_seq(dsnodes_seq);
	dcs_free_dc_gdev_range_seq(dsgdevs_range_seq);
	ds_free_gdevlist(globaldev_list);
	ds_free_gdevs_range_seq(gdevs_range_seq);
	ds_free_nodes_seq(nodes_seq);
	dcs_free_properties(dsproperty_seq);
	if (repl_type != NULL)
		free(repl_type);
	return (rstatus);
}
/*lint +e715 */

/*
 * scds_add_ds for raw disk
 *
 * Add a new raw disk device group(local or non local) or change the
 * configuration of the existing raw disk group in the DCS.
 * First check if device group already exist in DCS system, if it exists
 * then make sure if all the nodes in the device group have physical
 * connection to all the devices, if not physically connected then return
 * error otherwise add new nodes or devices to the device group. If device
 * group doesn't exist then check the connection again and create the new
 * device group in the DCS.
 *
 * This function is designed to be called for adding a ds locally or remotely.
 * For some volume managers it may use RPC if necessary to process the request.
 * For DID it always calls the local function.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EEXIST		- device group already exists
 *	SCCONF_ENOEXIST		- one or more devices or nodes not found
 *	SCCONF_EINUSE		- the device is already in use
 *	SCCONF_EBUSY		- busy, try again later
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EUNKNOWN		- unknown type
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_ERANGE		- operation not allowed because of out of range
 *				  numsecondaries
 */
scconf_errno_t
scds_add_ds(char *dsname, char *dsnodes[], scconf_state_t dspreference,
    scconf_gdev_range_t *dsdevices, scconf_state_t dsfailback,
    scconf_cfg_prop_t *dspropertylist, char *dsoptions,
    unsigned int dsnumsecondaries, char **messages)
{
	return (scds_add_ds_local(dsname, dsnodes, dspreference, dsdevices,
	    dsfailback, dspropertylist, dsoptions, dsnumsecondaries,
	    messages));
}

/*
 * scds_get_ds_type for raw disk
 *
 * Confirm if device type is DISK. This check is to make sure that right
 * .so is opened.
 *
 * Possible return values:
 *
 *	less than or greater than zero		- error
 *	equal to zero				- success
 *
 */
boolean_t
scds_get_ds_type(char *scconf_type, char *dcs_type)
{
	/* Check to make sure if device type is "DISK" or "rawdisk" */
	if (((dcs_type) && (strcmp(dcs_type, SCDG_DCS_SERVICE_TYPE) == 0)) ||
	    ((scconf_type) && (strcmp(scconf_type, SCDG_SCCONF_SERVICE_TYPE)
	    == 0)))
		return (B_TRUE);
	else
		return (B_FALSE);

}

/*
 * scds_change_ds_local for raw disk
 *
 * Change the configuration of the existing device group in DCS.
 * First checks if device group already exist in DCS system, if it exists
 * then make sure if all the nodes in the device group has the physical
 * connection to all the devices, if not physically connected then return
 * error or add new global devices to the device group. This function also
 * change the node preference order and the failback mode for a device group.
 * No new nodes can be added with this function. To change the order of the
 * node preference, all the nodes in the device group should be specified by
 * the user.
 *
 *  This "local" function will only operate on the local node.  In cases
 *  where RPC is needed to send the request to the node where the disk group
 *  is imported, the non-local function should be used.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOEXIST		- device group or node is not found
 *	SCCONF_EINUSE		- an object is already in use
 *	SCCONF_EBUSY		- busy state, try again later
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EUNKNOWN		- unknown type
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_ERANGE		- operation not allowed because of out of range
 *				  numsecondaries
 *	SCCONF_DS_EINVAL	- inconsistencies detected in device
 *				  configuration
 */
/*lint -e715 */
/*ARGSUSED*/
scconf_errno_t scds_change_ds_local(char *dsname, char *dsnodes[],
    scconf_state_t dspreference, scconf_gdev_range_t *dsdevices,
    scconf_state_t dsfailback, scconf_cfg_prop_t *dspropertylist,
    char *dsoptions, unsigned int dsnumsecondaries, char **messages)
{
	dc_error_t dc_err = DCS_SUCCESS;
	scconf_errno_t rstatus = SCCONF_NOERR;
	uint_t num_of_nodes = 0;
	uint_t num_of_gdevs = 0;
	int enable_failback = 0;
	int failback_value = 0;
	char **globaldev_list = NULL;
	dc_gdev_range_seq_t *gdevs_range_seq = (dc_gdev_range_seq_t *)0;
	dc_replica_seq_t *nodes_seq = (dc_replica_seq_t *)0;
	dc_gdev_range_seq_t *dsgdevs_range_seq = (dc_gdev_range_seq_t *)0;
	dc_replica_seq_t *dsnodes_seq = (dc_replica_seq_t *)0;
	dc_property_seq_t *dsproperty_seq = (dc_property_seq_t *)0;
	scconf_state_t local_only = SCCONF_STATE_UNCHANGED;
	scconf_state_t autogen = SCCONF_STATE_UNCHANGED;
	boolean_t is_local_only_dg = B_FALSE;
	dc_string_seq_t string_seq;
	char *autogen_prop = SCCONF_DS_PROP_AUTOGENERATED;
	char *localonly_prop = SCDG_LOCAL_ONLY;
	char *repl_type = NULL;
	boolean_t repl_val;
	int set_replication;

	/* Initialize */
	dsdevices = NULL;
	dspropertylist = NULL;
	string_seq.strings = NULL;
	string_seq.count = 0;

	/* Determine if device group exist */
	dc_err = dcs_get_service_parameters(dsname, NULL, &enable_failback,
	    &dsnodes_seq, NULL, &dsgdevs_range_seq, &dsproperty_seq);

	is_local_only_dg = ds_confirm_is_local_only_dg(dsproperty_seq);

	if (dc_err != DCS_SUCCESS) {
		rstatus = ds_dcs_to_scconf_err_status(dc_err);
		goto cleanup;
	}

	/* Check agruments */
	if ((dsoptions != NULL) && (*dsoptions)) {
		/* Get list of all devices */
		rstatus = ds_get_globaldev_list(&globaldev_list, &num_of_gdevs,
		    dsoptions, &local_only, &autogen);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Populate gdevs_range_seq structure */
		if (num_of_gdevs != 0) {
			rstatus = ds_get_globaldev_range_seq(globaldev_list,
			    num_of_gdevs, &gdevs_range_seq);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
		}
	}

	if (dsnodes) {
		/* If local only option is specified */
		num_of_nodes = ds_get_num_of_nodes(dsnodes);
		if (local_only == SCCONF_STATE_ENABLED) {

			/*
			 * If more than one node is specified then report
			 * error.
			 */
			if (num_of_nodes > 1) {
				rstatus = SCCONF_EINVAL;
				goto cleanup;
			}
		}
	}

	/* Populate nodes_seq structure */
	rstatus = ds_set_node_seq(dsnodes, dsnodes_seq,
	    &nodes_seq, dspreference, num_of_nodes, local_only, messages);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/* Check that the desired number of secondaries is not too large. */
	if (dsnumsecondaries != SCCONF_NUMSECONDARIES_UNSET) {
		if (dsnumsecondaries > (dsnodes_seq->count - 1)) {
			rstatus = SCCONF_ERANGE;
			goto cleanup;
		}
	}

	if (dsgdevs_range_seq != (dc_gdev_range_seq_t *)0) {

			/* Get list of all the previous and current devices */
			rstatus = ds_get_target_gdevs(dsgdevs_range_seq,
			    &globaldev_list, num_of_gdevs);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
	}

	rstatus = scconf_verify_replication(globaldev_list, dsname,
	    &repl_val, &repl_type, messages);
	if (rstatus != SCCONF_NOERR) {
		goto cleanup;
	}

	/* Find out if replication need to be added or not */
	add_replication(dsproperty_seq, repl_val, &set_replication);

	/*
	 * If want to change to non local device group from the localonly
	 * device group, then get all the devices listed in the
	 * Devices section of the device group configuraion and set the
	 * Property value with the new 'gdev' property.
	 */
	if ((local_only == SCCONF_STATE_DISABLED) && (is_local_only_dg)) {
		/* Remove only local_only property */
		string_seq.strings = &localonly_prop;
		string_seq.count = 1;
		(void) dcs_remove_properties(dsname, &string_seq);

		/*
		 * Set count to zero, so that it will be treated as
		 * setting a new property value for the device
		 * group.
		 */
		dsproperty_seq->count = 0;
		if ((dc_err = ds_change_global_property_seq(
		    dsname, globaldev_list, dsproperty_seq,
		    B_FALSE)) != DCS_SUCCESS) {
			goto cleanup;
		}
	}

	/* Remove the autogen property from the device group */
	if (autogen != SCCONF_STATE_ENABLED) {
		string_seq.strings = &autogen_prop;
		string_seq.count = 1;

		/*
		 * remove the autogen 'property_seq' value for device group
		 * configuration
		 */
		(void) dcs_remove_properties(dsname, &string_seq);
	}

	/* Get failback value */
	ds_get_failback_value(dsfailback, &failback_value, enable_failback);

	/* Change the configuration for existing device group */
	dc_err = ds_change_service_parameters(dsname, failback_value, nodes_seq,
	    gdevs_range_seq, globaldev_list, dsproperty_seq, local_only,
	    is_local_only_dg, autogen, dsnumsecondaries, repl_type,
	    set_replication);

	if (dc_err != DCS_SUCCESS) {
		rstatus = ds_dcs_to_scconf_err_status(dc_err);
	}

cleanup:
	dcs_free_dc_replica_seq(dsnodes_seq);
	dcs_free_dc_gdev_range_seq(dsgdevs_range_seq);
	ds_free_gdevlist(globaldev_list);
	ds_free_gdevs_range_seq(gdevs_range_seq);
	ds_free_nodes_seq(nodes_seq);
	dcs_free_properties(dsproperty_seq);
	if (repl_type != NULL)
		free(repl_type);
	return (rstatus);
}
/*lint +e715 */

/*
 * scds_change_ds for raw disk
 *
 * Change the configuration of the existing device group in DCS.
 * First checks if device group already exist in DCS system, if it exists
 * then make sure if all the nodes in the device group has the physical
 * connection to all the devices, if not physically connected then return
 * error or add new global devices to the device group. This function also
 * change the node preference order and the failback mode for a device group.
 * No new nodes can be added with this function. To change the order of the
 * node preference, all the nodes in the device group should be specied by
 * the user.
 *
 * This function is designed to be called for changing a ds locally or
 * remotely. For some volume managers it may use RPC if necessary to process
 * the request.
 *
 * For DID it always calls the local function.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOEXIST		- device group or node is not found
 *	SCCONF_EINUSE		- an object is already in use
 *	SCCONF_EBUSY		- busy state, try again later
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EUNKNOWN		- unknown type
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_ERANGE		- operation not allowed because of out of range
 *				  numsecondaries
 */
scconf_errno_t scds_change_ds(char *dsname, char *dsnodes[],
    scconf_state_t dspreference, scconf_gdev_range_t *dsdevices,
    scconf_state_t dsfailback, scconf_cfg_prop_t *dspropertylist,
    char *dsoptions, unsigned int dsnumsecondaries, char **messages)
{
	return (scds_change_ds_local(dsname, dsnodes, dspreference, dsdevices,
	    dsfailback, dspropertylist, dsoptions, dsnumsecondaries,
	    messages));
}

/*
 * scds_remove_ds for raw disk
 *
 * Remove the device group from the DCS configuration.
 * If nodelist and global devices are also specified then those
 * nodes and devices are removed from the device group and if there
 * are no more nodes and devices in the device group then device
 * group is also removed from DCS configuraion.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOEXIST		- device group or node is not found
 *	SCCONF_EINUSE		- an object is already in use
 *	SCCONF_EBUSY		- busy state, try again later
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EUNKNOWN		- unknown type
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_ERANGE		- operation not allowed because of out of range
 *				  numsecondaries
 */
/*lint -e715 */
/*ARGSUSED*/
scconf_errno_t scds_remove_ds(char *dsname, char *dsnodes[],
    scconf_gdev_range_t *dsdevices, scconf_cfg_prop_t *dspropertylist,
    char *dsoptions, char **messages)
{
	int i = 0;
	unsigned int j = 0;
	unsigned int k = 0;
	dc_error_t dc_err = DCS_SUCCESS;
	scconf_errno_t rstatus = SCCONF_NOERR;
	uint_t num_of_gdevs = 0;
	int enable_failback = 0;
	char **globaldev_list = NULL;
	dc_gdev_range_seq_t *gdevs_range_seq = (dc_gdev_range_seq_t *)0;
	dc_gdev_range_seq_t *dsgdevs_range_seq = (dc_gdev_range_seq_t *)0;
	dc_replica_seq_t *dsnodes_seq = (dc_replica_seq_t *)0;
	dc_property_seq_t *dsproperty_seq = (dc_property_seq_t *)0;
	scconf_state_t local_only;
	scconf_state_t autogen = SCCONF_STATE_UNCHANGED;
	boolean_t is_local_only_dg = B_FALSE;
	scconf_nodeid_t *dsnodeids = NULL;
	unsigned int node_count = 0;
	unsigned int remaining_nodes = 0;
	unsigned int num_secs = 0;
	boolean_t node_found = B_FALSE;

	/* Initialize */
	dsdevices = NULL;
	dspropertylist = NULL;

	/* Determine if device group exist */
	dc_err = dcs_get_service_parameters(dsname, NULL, &enable_failback,
	    &dsnodes_seq, &num_secs, &dsgdevs_range_seq, &dsproperty_seq);

	/* Check agruments */
	if ((dsnodes == NULL) && (!*dsoptions)) {

		/* Remove the disk group */
		dc_err = dcs_remove_service(dsname);
		if (dc_err != DCS_SUCCESS) {
			rstatus = ds_dcs_to_scconf_err_status(dc_err);
			return (rstatus);
		}
		return (rstatus);
	}

	is_local_only_dg = ds_confirm_is_local_only_dg(dsproperty_seq);
	if (is_local_only_dg) {
		/* Report error if nodes are specified */
		if (dsnodes != NULL) {
			rstatus = SCCONF_EINVAL;
			goto cleanup;
		}
	}

	/* Check agruments */
	if (dsnodes != NULL) {

		/*
		 * If the device service had a non-default numsecondaries
		 * property set, and the node removal operation, if
		 * successful, can result in lower than the numsecondaries
		 * number of secondaries, we fail the operation.
		 */

		/* Convert the nodenames to nodeids */
		rstatus = ds_get_nodeids(dsnodes, &dsnodeids);
		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}

		while (dsnodeids[node_count] != 0) {
			node_count++;
		}

		if (dsnodes_seq != (dc_replica_seq_t *)NULL) {

			remaining_nodes = 0;
			for (j = 0; j < dsnodes_seq->count; j++) {
				node_found = B_FALSE;
				for (k = 0; k < node_count; k++) {
					if (dsnodeids[k] ==
					    dsnodes_seq->replicas[j].id) {
						node_found = B_TRUE;
						break;
					}
				}
				if (node_found == B_FALSE) {
					remaining_nodes++;
				}
			}

			if (num_secs > DCS_DEFAULT_DESIRED_NUM_SECONDARIES) {
				if ((remaining_nodes != 0) &&
				    (remaining_nodes < (num_secs + 1))) {
					rstatus = SCCONF_ERANGE;
					goto cleanup;
				}
			}
		}

		while (dsnodeids[i] != 0) {

			/* Remove node from device group */
			dc_err = dcs_remove_node(dsname, dsnodeids[i]);

			if (dc_err != DCS_SUCCESS) {
				rstatus = ds_dcs_to_scconf_err_status(dc_err);
				goto cleanup;
			}
			i++;
		}

	}

	/* Check agruments */
	if ((dsoptions != NULL) && (*dsoptions)) {

		/* Get list of all devices */
		rstatus = ds_get_globaldev_list(&globaldev_list, &num_of_gdevs,
		    dsoptions, &local_only, &autogen);

		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		if ((autogen == SCCONF_STATE_ENABLED) || (autogen ==
		    SCCONF_STATE_DISABLED)) {
			rstatus = SCCONF_EINVAL;
			goto cleanup;
		}

		/* Populate gdevs_range_seq structure */
		rstatus = ds_get_globaldev_range_seq(globaldev_list,
		    num_of_gdevs, &gdevs_range_seq);

		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Remove devices from device group */
		dc_err = dcs_remove_devices(dsname, gdevs_range_seq);

		if (dc_err != DCS_SUCCESS) {
			rstatus = ds_dcs_to_scconf_err_status(dc_err);
			goto cleanup;
		}

		dc_err = ds_remove_property_seq(dsname, dsproperty_seq);
		if (dc_err != DCS_SUCCESS) {
			rstatus = ds_dcs_to_scconf_err_status(dc_err);
			goto cleanup;
		}

		/* Change dsproperty_seq structure */
		dc_err = ds_change_global_property_seq(dsname, globaldev_list,
		    dsproperty_seq, B_TRUE);
		if (dc_err != DCS_SUCCESS) {
			rstatus = ds_dcs_to_scconf_err_status(dc_err);
			goto cleanup;
		}
	}

	/* Get the device group parameters */
	dc_err = dcs_get_service_parameters(dsname, NULL, &enable_failback,
		&dsnodes_seq, NULL, &dsgdevs_range_seq, NULL);

	if (dc_err != DCS_SUCCESS) {
		rstatus = ds_dcs_to_scconf_err_status(dc_err);
		goto cleanup;
	}

	/* Check if no nodes exist in the device group */
	if (dsnodes_seq->count == 0) {

			/* Remove the device group */
			dc_err = dcs_remove_service(dsname);

			if (dc_err != DCS_SUCCESS) {
				rstatus = ds_dcs_to_scconf_err_status(dc_err);
				goto cleanup;
			}
	}

cleanup:
	dcs_free_dc_replica_seq(dsnodes_seq);
	dcs_free_dc_gdev_range_seq(dsgdevs_range_seq);
	ds_free_gdevlist(globaldev_list);
	ds_free_gdevs_range_seq(gdevs_range_seq);
	dcs_free_properties(dsproperty_seq);
	if (dsnodeids != NULL)
		free(dsnodeids);
	return (rstatus);
}
/*lint +e715 */

/*
 * scds_get_ds_config for raw disk
 *
 * Upon success a list of all the raw device group parameters are
 * returned in dsconfigp.
 * The caller is responsible for freeing the memory for dsconfigp.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOEXIST		- device group is not found
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scds_get_ds_config(char *dsname, const uint_t iflag,
    scconf_cfg_ds_t **dsconfigp)
{
	dc_error_t dc_err = DCS_SUCCESS;
	scconf_errno_t rstatus = SCCONF_NOERR;
	uint_t i = 0;
	boolean_t is_local_only_dg = B_FALSE;
	int enable_failback = 0;
	unsigned int num_secs = 0;
	char devname[MAXNAMELEN];
	char searchpath[MAXPATHLEN];
	char bsearchpath[MAXPATHLEN];
	dc_gdev_range_seq_t *dsgdevs_range_seq = (dc_gdev_range_seq_t *)0;
	dc_replica_seq_t *dsnodes_seq = (dc_replica_seq_t *)0;
	dc_property_seq_t *dsproperty_seq = (dc_property_seq_t *)0;
	scconf_namelist_t *ds_devicenames = NULL;
	scconf_namelist_t *ds_dvdevicenames = NULL;
	scconf_namelist_t *ds_cdevicenames = NULL;
	scconf_namelist_t *ds_bdevicenames = NULL;
	scconf_namelist_t *current_ptr = NULL;
	scconf_nodeid_t *nodelist = NULL;
	char *value;
	char *repl_value;
	scconf_cfg_prop_t **propp;
	unsigned int num_nodes_found;

	/* Get parameters for the specified device group name */
	dc_err = dcs_get_service_parameters(dsname, NULL, &enable_failback,
		&dsnodes_seq, &num_secs, &dsgdevs_range_seq, &dsproperty_seq);

	if (dc_err != DCS_SUCCESS) {
		rstatus = ds_dcs_to_scconf_err_status(dc_err);
		goto cleanup;
	}

	if (dsname != NULL) {
		if (((*dsconfigp)->scconf_ds_name =
		    strdup(dsname)) == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}
	}

	is_local_only_dg = ds_confirm_is_local_only_dg(dsproperty_seq);
	if (is_local_only_dg) {
		if (((*dsconfigp)->scconf_ds_type =
		    strdup("Local_Disk")) == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}
	} else {
		if (((*dsconfigp)->scconf_ds_type =
		    strdup("Disk")) == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}
	}

	/* Set properties of interest */
	propp = &(*dsconfigp)->scconf_ds_propertylist;
	while (*propp != NULL)
		propp = &(*propp)->scconf_prop_next;

	/* SCCONF_DS_PROP_AUTOGENERATED */
	value = ds_get_property(SCCONF_DS_PROP_AUTOGENERATED, dsproperty_seq);
	if (value) {

		/* allocate the property struct */
		*propp = (scconf_cfg_prop_t *)calloc(1,
		    sizeof (scconf_cfg_prop_t));
		if (*propp == NULL) {
			if (value != NULL)
				free(value);
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* add the property */
		(*propp)->scconf_prop_key =
		    strdup(SCCONF_DS_PROP_AUTOGENERATED);
		if ((*propp)->scconf_prop_key == NULL) {
			if (value != NULL)
				free(value);
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* add the value */
		(*propp)->scconf_prop_value = value;

		/* next */
		propp = &(*propp)->scconf_prop_next;
	}

	/* SCCONF_DS_PROP_REPLICATED_DEVICE */
	repl_value = ds_get_property(SCCONF_DS_PROP_REPLICATED_DEVICE,
	    dsproperty_seq);
	if (repl_value) {

		/* Allocate the property struct */
		*propp = (scconf_cfg_prop_t *)calloc(1,
		    sizeof (scconf_cfg_prop_t));
		if (*propp == NULL) {
			if (repl_value != NULL)
				free(repl_value);
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* Add the property */
		(*propp)->scconf_prop_key =
		    strdup(SCCONF_DS_PROP_REPLICATED_DEVICE);
		if ((*propp)->scconf_prop_key == NULL) {
			if (repl_value != NULL)
				free(repl_value);
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* Add the value */
		(*propp)->scconf_prop_value = repl_value;

		/* Next */
		propp = &(*propp)->scconf_prop_next;
	}

	if (((*dsconfigp)->scconf_ds_label =
	    strdup(gettext("device names"))) == NULL) {
		rstatus = SCCONF_ENOMEM;
		goto cleanup;
	}

	(*dsconfigp)->scconf_ds_failback = enable_failback;

	(*dsconfigp)->scconf_ds_num_secs = num_secs;

	/* Get the list of nodes */
	(*dsconfigp)->scconf_ds_nodelist = NULL;
	if (dsnodes_seq->count > 0) {

		switch ((dsnodes_seq->replicas)[0].preference) {
		case 0:
		case SCDG_MAX_NODE_PREFERENCE:
			(*dsconfigp)->scconf_ds_preference =
			    SCCONF_STATE_DISABLED;
			break;
		default:
			(*dsconfigp)->scconf_ds_preference =
			    SCCONF_STATE_ENABLED;
			break;
		}

		nodelist = (scconf_nodeid_t *)calloc(dsnodes_seq->count+1,
		    sizeof (scconf_nodeid_t));
		if (nodelist == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}
		/*
		 * if SCCONF_STATE_ENABLED nodelist has to be sorted
		 * by preference.
		 */
		if ((*dsconfigp)->scconf_ds_preference ==
		    SCCONF_STATE_ENABLED) {
			scconf_nodeid_t tmp_nodeid[SCDG_MAX_NODE_PREFERENCE];

			bzero(tmp_nodeid,
			    sizeof (scconf_nodeid_t) *
			    SCDG_MAX_NODE_PREFERENCE);
			for (i = 0; i < dsnodes_seq->count &&
			    i < SCDG_MAX_NODE_PREFERENCE; i++) {
				tmp_nodeid[dsnodes_seq->replicas[i].preference]=
				    dsnodes_seq->replicas[i].id;
			}

			i = 0;
			num_nodes_found = 0;
			while ((num_nodes_found < dsnodes_seq->count) &&
			    (i < SCDG_MAX_NODE_PREFERENCE)) {
				if (tmp_nodeid[i] == (scconf_nodeid_t)0) {
					i++;
					continue;
				}
				nodelist[num_nodes_found++] = tmp_nodeid[i++];
			}
		} else {
			for (i = 0; i < dsnodes_seq->count; i++)
			    nodelist[i] = dsnodes_seq->replicas[i].id;
		}
		nodelist[dsnodes_seq->count] = 0;
		(*dsconfigp)->scconf_ds_nodelist = nodelist;
	}

	current_ptr = (scconf_namelist_t *)0;
	for (i = 0; i < dsgdevs_range_seq->count; i++) {
		*devname = '\0';

		rstatus = ds_get_globaldevname(
		    dsgdevs_range_seq->ranges[i].start_gmin, devname);

		if (rstatus != SCCONF_NOERR) {
			rstatus = SCCONF_ENOEXIST;
			goto cleanup;
		}

		if ((ds_devicenames = (scconf_namelist_t *)
			calloc(1, sizeof (scconf_namelist_t))) == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		if (*devname) {
			if ((ds_devicenames->scconf_namelist_name =
			    strdup(devname)) == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}
		}

		if (current_ptr != NULL) {
			ds_devicenames->scconf_namelist_next = current_ptr;
		} else {
			ds_devicenames->scconf_namelist_next = NULL;
		}
		current_ptr = ds_devicenames;
	}

	(void) sprintf(searchpath, "%s/", SCDG_RAWDISK_DEVICE_PATH);

	if (!(iflag & SCCONF_CDEVICE_FLAG) && !(iflag & SCCONF_BDEVICE_FLAG)) {
		ds_cdevicenames = NULL;
		ds_bdevicenames = NULL;
	} else if ((iflag & SCCONF_CDEVICE_FLAG) &&
	    (iflag & SCCONF_BDEVICE_FLAG)) {
		(void) strcpy(bsearchpath, searchpath);
		(void) strcat(bsearchpath, "dsk");
		rstatus = ds_get_device_list(bsearchpath, current_ptr, iflag,
		    &ds_bdevicenames);

		(void) strcat(searchpath, "rdsk");
		rstatus = ds_get_device_list(searchpath, current_ptr, iflag,
		    &ds_cdevicenames);
	} else if (iflag & SCCONF_CDEVICE_FLAG) {
		(void) strcat(searchpath, "rdsk");
		rstatus = ds_get_device_list(searchpath, current_ptr, iflag,
		    &ds_cdevicenames);
		ds_bdevicenames = NULL;
	} else if (iflag & SCCONF_BDEVICE_FLAG) {
		(void) strcat(searchpath, "dsk");
		rstatus = ds_get_device_list(searchpath, current_ptr, iflag,
		    &ds_bdevicenames);
		ds_cdevicenames = NULL;
	}

	(*dsconfigp)->scconf_ds_cdevicenames = ds_cdevicenames;
	(*dsconfigp)->scconf_ds_bdevicenames = ds_bdevicenames;

	(void) sprintf(searchpath, "%s/rdsk", SCDG_RAWDISK_DEVICE_PATH);
	rstatus = ds_get_device_list(searchpath, current_ptr,
	    iflag | SCCONF_SLICE2_ONLY_PFLAG,
	    &ds_dvdevicenames);
	(*dsconfigp)->scconf_ds_devvalues = ds_dvdevicenames;

cleanup:
	dcs_free_dc_replica_seq(dsnodes_seq);
	dcs_free_dc_gdev_range_seq(dsgdevs_range_seq);
	dcs_free_properties(dsproperty_seq);
	ds_free_namelist_local(current_ptr);
	if (rstatus != SCCONF_NOERR) {
		if (nodelist != NULL)
			free(nodelist);
		(*dsconfigp)->scconf_ds_nodelist = NULL;
	}
	return (rstatus);
}

/*
 * ds_get_device_list
 *
 * Upon success a list of all the raw devices are returned in ds_devicenames.
 * The caller is responsible for freeing the memory for ds_devicenames.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOEXIST		- the nodeid is not found
 *	SCCONF_ESETUP		- setup attempt failed
 */
static scconf_errno_t
ds_get_device_list(char *searchpath, scconf_namelist_t *current_list,
    const uint_t iflag, scconf_namelist_t **ds_devicenames)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	int error = 0;
	int i = 0;
	struct stat file_stat;
	scconf_namelist_t *namelist;
	scconf_namelist_t *ds_devicename = (scconf_namelist_t *)0;
	scconf_namelist_t *current_ptr = (scconf_namelist_t *)0;
	char dev_searchpath[MAXPATHLEN];
	int first_slice = 0;
	int last_slice = first_slice + SCDG_NUM_DEV_SLICES - 1;

	if (iflag & SCCONF_SLICE2_ONLY_PFLAG) {
		first_slice = 2;
		last_slice = 2;
	}

	for (namelist = current_list; namelist;
	    namelist = namelist->scconf_namelist_next) {

		for (i = first_slice; i <= last_slice; i++) {
			bzero(dev_searchpath, sizeof (dev_searchpath));
			(void) sprintf(dev_searchpath, "%s/%ss%d", searchpath,
			    namelist->scconf_namelist_name, i);

			error = stat(dev_searchpath, &file_stat);
			if ((error != 0) &&
			    (iflag & SCCONF_NODEVICE_ERROR_FLAG))  {
				rstatus = SCCONF_ENOEXIST;
				goto cleanup;
			}

			if ((ds_devicename = (scconf_namelist_t *)
			    calloc(1, sizeof (scconf_namelist_t))) == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}

			if ((ds_devicename->scconf_namelist_name =
			    strdup(dev_searchpath)) == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}

			if (current_ptr != NULL) {
				ds_devicename->scconf_namelist_next =
				    current_ptr;
			} else {
				ds_devicename->scconf_namelist_next = NULL;
			}
			current_ptr = ds_devicename;
		}
	}
	*ds_devicenames = current_ptr;

cleanup:
	return (rstatus);
}

/*
 * ds_get_failback_value
 *
 * Get the appropritae failback value for DCS from the user
 * specified failback value.
 *
 * Possible return values: NONE
 *
 */
static void
ds_get_failback_value(scconf_state_t dsfailback, int *failback_value,
    int enable_failback)
{
	switch (dsfailback) {

	case SCCONF_STATE_UNCHANGED:
		*failback_value =  enable_failback;
		break;

	case SCCONF_STATE_ENABLED:
		*failback_value = 1;
		break;

	case SCCONF_STATE_DISABLED:
		*failback_value = 0;
		break;

	default:
		break;
	}
}

/*
 * ds_change_service_parameters
 *
 * Change configuration for a existing device group.
 *
 * Possible return values:
 *
 *	DCS_SUCCESS
 *	DCS_ERR_NOT_CLUSTER
 *	DCS_ERR_SERVICE_NAME
 *	DCS_ERR_DEVICE_INVAL
 *	DCS_ERR_DEVICE_BUSY
 *	DCS_ERR_CCR_ACCESS
 *
 */
static dc_error_t
ds_change_service_parameters(char *service_name, int failback_enabled,
    dc_replica_seq_t *nodes_seq, dc_gdev_range_seq_t *gdevs_range_seq,
    char **globaldev_list, dc_property_seq_t *dsproperty_seq,
    scconf_state_t local_only, boolean_t is_local_only_dg, scconf_state_t
    autogen, unsigned int dsnumsecondaries, char *repl_type, int set_repl)
{
	dc_error_t dc_err = DCS_SUCCESS;
	dc_property_t property;
	dc_property_seq_t properties;
	char value[MAXNAMELEN];
	dc_string_seq_t string_seq;

	/* Initialise */
	string_seq.strings = NULL;
	string_seq.count = 0;

	/* Set the failback mode */
	dc_err = dcs_set_failback_mode(service_name, failback_enabled);
	if (dc_err != DCS_SUCCESS) {
		return (dc_err);
	}

	/* Set the desired number of secondaries */
	if (dsnumsecondaries != SCCONF_NUMSECONDARIES_UNSET) {
		dc_err = dcs_set_active_secondaries(service_name,
		    dsnumsecondaries);
		if (dc_err != DCS_SUCCESS) {
			return (dc_err);
		}
	}

	if (nodes_seq != (dc_replica_seq_t *)0) {
		dc_err = ds_add_nodes(service_name, nodes_seq);
		if (dc_err != DCS_SUCCESS) {
			return (dc_err);
		}
	}

	if (gdevs_range_seq != (dc_gdev_range_seq_t *)0) {
		if ((dc_err = dcs_add_devices(service_name, gdevs_range_seq))
		    != DCS_SUCCESS)
			return (dc_err);
		/* If not a localonly device group */
		if ((local_only != SCCONF_STATE_ENABLED) &&
		    (!is_local_only_dg)) {
			if ((dc_err = ds_change_global_property_seq(
			    service_name, globaldev_list, dsproperty_seq,
			    B_FALSE)) != DCS_SUCCESS) {
				goto cleanup;
			}
		}
	}

	if (local_only == SCCONF_STATE_ENABLED) {

		properties.properties = &property;
		properties.count = 1;

		property.name = SCDG_LOCAL_ONLY;
		(void) sprintf(value, "%d", nodes_seq->replicas[0].id);
		property.value = value;
		if ((dc_err = dcs_set_properties(service_name,
		    &properties)) != DCS_SUCCESS) {
			goto cleanup;
		}
	}

	if (autogen == SCCONF_STATE_ENABLED) {
		properties.properties = &property;
		properties.count = 1;

		property.name = SCCONF_DS_PROP_AUTOGENERATED;
		(void) sprintf(value, "%d", 1);
		property.value = value;
		if ((dc_err = dcs_set_properties(service_name,
		    &properties)) != DCS_SUCCESS) {
			goto cleanup;
		}
	}

	/* Set replication propery if set_repl is 1 */
	if (set_repl == 1) {
		properties.properties = &property;
		properties.count = 1;

		property.name = SCCONF_DS_PROP_REPLICATED_DEVICE;
		property.value = strdup(repl_type);
		if ((dc_err = dcs_set_properties(service_name,
		    &properties)) != DCS_SUCCESS) {
			goto cleanup;
		}
	}

	/* Removes replication if set_repl is 2 */
	if (set_repl == 2) {
		string_seq.strings = (char **)malloc(sizeof (char *));
		if (string_seq.strings == NULL) {
			return (DCS_ERR_NOMEM);
		}
		string_seq.strings[0] =
		    strdup(SCCONF_DS_PROP_REPLICATED_DEVICE);
		if (string_seq.strings[0] == NULL) {
			free(string_seq.strings);
			return (DCS_ERR_NOMEM);
		}
		string_seq.count = 1;
		dc_err = dcs_remove_properties(service_name, &string_seq);
		free(string_seq.strings[0]);
	}

cleanup:
	return (dc_err);
}

/*
 * ds_add_nodes
 *
 * Add nodes to the existing device group.
 *
 * Possible return values:
 *
 *	DCS_SUCCESS
 *	DCS_ERR_NOT_CLUSTER
 *	DCS_ERR_SERVICE_NAME
 *	DCS_ERR_DEVICE_INVAL
 *	DCS_ERR_DEVICE_BUSY
 *	DCS_ERR_CCR_ACCESS
 *
 */
/*lint -e793 */
static dc_error_t
ds_add_nodes(char *service_name, dc_replica_seq_t *nodes_seq)
{
	dc_error_t dc_err = DCS_SUCCESS;
	uint_t i = 0;

	for (i = 0; i < nodes_seq->count; i++) {
		if ((dc_err = dcs_add_node(service_name,
		    &nodes_seq->replicas[i])) != DCS_SUCCESS) {
			return (dc_err);
		}
	}

	if ((dc_err = dcs_change_node_priority(service_name, nodes_seq)) !=
	    DCS_SUCCESS) {
		return (dc_err);
	} else {
		return (DCS_SUCCESS);
	}
}

/*
 * ds_get_target_nodes
 *
 * Get list of all the previous and current nodes and returned in
 * dsnodes.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EEXIST		- the given "nodename" is already configured
 *	SCCONF_ENOMEM		- not enough memory
 */
static scconf_errno_t
ds_get_target_nodes(dc_replica_seq_t *dsnodes_seq, char **dsnodes[],
    uint_t num_of_nodes)
{
	uint_t i = 0;
	scconf_errno_t rstatus = SCCONF_NOERR;
	char *nodenamep = NULL;

	if ((*dsnodes = (char **)realloc(*dsnodes, (dsnodes_seq->count +
		num_of_nodes + 1) * sizeof (char *))) == NULL)
		return (SCCONF_ENOMEM);

	for (i = 0; i < dsnodes_seq->count; i++) {
		rstatus = scconf_get_nodename(
			(scconf_nodeid_t)dsnodes_seq->replicas[i].id,
			&nodenamep);

		if (rstatus != SCCONF_NOERR)
			return (SCCONF_ENOEXIST);

		if (nodenamep != NULL) {
			if (((*dsnodes)[num_of_nodes++] =
			    strdup(nodenamep)) == NULL)
				return (SCCONF_ENOMEM);
			}
		}
	(*dsnodes)[num_of_nodes] = NULL;
	return (SCCONF_NOERR);
}

/*
 * ds_get_target_gdevs
 *
 * Get list of all the previous and current devices and returned in
 * globaldev_list.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EEXIST		- the given "nodename" is already configured
 *	SCCONF_ENOMEM		- not enough memory
 */
static scconf_errno_t
ds_get_target_gdevs(dc_gdev_range_seq_t *dsgdevs_range_seq,
    char **globaldev_list[], uint_t num_of_gdevs)
{
	uint_t i = 0;
	scconf_errno_t rstatus = SCCONF_NOERR;
	char devname[5];

	if ((*globaldev_list = (char **)realloc(*globaldev_list,
	    (dsgdevs_range_seq->count + num_of_gdevs + 1) *
	    sizeof (char *)))
		== NULL)
		return (SCCONF_ENOMEM);

	for (i = 0; i < dsgdevs_range_seq->count; i++) {
		(void) memset(devname, '\0', sizeof (devname));

		rstatus = ds_get_globaldevname(
		    dsgdevs_range_seq->ranges[i].start_gmin, devname);

		if (rstatus != SCCONF_NOERR)
			return (SCCONF_ENOEXIST);

		if (devname != NULL) {
			if (((*globaldev_list)[num_of_gdevs++] =
			    strdup(devname)) == NULL)
				return (SCCONF_ENOMEM);
		}
	}
	(*globaldev_list)[num_of_gdevs] = NULL;
	return (SCCONF_NOERR);
}

/*
 * ds_get_globaldevname
 *
 * Get the name of global device from maj,min number.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EEXIST		- the given "nodename" is already configured
 *	SCCONF_ENOMEM		- not enough memory
 */
static scconf_errno_t
ds_get_globaldevname(minor_t start_gmin, char *devnamep)
{
	uint_t result = 0;

	if ((start_gmin % 32) != 0)
		return (SCCONF_EUNKNOWN);
	else
		result = start_gmin / 32;

	(void) sprintf(devnamep, "d%d", result);
	return (SCCONF_NOERR);
}

/*
 * ds_get_globaldev_list
 *
 * Get the list of global devices.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 */
static scconf_errno_t
ds_get_globaldev_list(char **globaldev_list[], uint_t *num_gdev,
    char *gdev_list, scconf_state_t *local_only, scconf_state_t *autogen)
{
	int i = 0;
	char *current = NULL;
	char *value = NULL;
	char *gdev = NULL;
	char **temp_listp;
	char *knownopts[] = {
		"globaldev",			/* 0 - globaldev type */
		"localonly",			/* 1 - local type */
		"autogen",			/* 2 - autogen type */
		"sync",				/* 3 - sync */
		(char *)0
	};

	/* Get the number of devices */
	for (current = gdev_list;  current; current = strchr(current+1, ',')) {
		++(*num_gdev);
	}

	/* Allocate memory */
	if ((*globaldev_list = temp_listp =
	    (char **)calloc(((*num_gdev)) + 1,
	    sizeof (char *))) == NULL) {
		return (SCCONF_ENOMEM);
	}

	while (*gdev_list) {
		switch (getsubopt(&gdev_list, knownopts, &value)) {
		/* 0 - globaldev type */
		case 0:
			if (value == NULL) {
				return (SCCONF_EINVAL);
			}
			gdev = value;
			if ((temp_listp[i++] = strdup(gdev)) == NULL)
				return (SCCONF_ENOMEM);
			break;
		/* 1 - local type */
		case 1:
			if ((value == NULL) ||((strcmp(value, "true") != 0) &&
			    (strcmp(value, "false") != 0))) {
				return (SCCONF_EINVAL);
			}
			if (strcmp(value, "true") == 0)
				*local_only = SCCONF_STATE_ENABLED;
			else if (strcmp(value, "false") == 0)
				*local_only = SCCONF_STATE_DISABLED;
			/* localonly has been also counted as global device */
			(*num_gdev)--;
			break;
		/* 2 - autogen type */
		case 2:
			if ((value == NULL) ||((strcmp(value, "true") != 0) &&
			    (strcmp(value, "false") != 0))) {
				return (SCCONF_EINVAL);
			}
			if (strcmp(value, "true") == 0)
				*autogen = SCCONF_STATE_ENABLED;
			else if (strcmp(value, "false") == 0)
				*autogen = SCCONF_STATE_DISABLED;
			/* autogen has been also counted as global device */
			(*num_gdev)--;
			break;
		/* 3 - sync */
		case 3:
			/*
			 * sync is also valid suboption for rawdisks.
			 * It makes sure that replication property is
			 * in sync with underlying replication configuration
			 *
			 */
			if (value != NULL) {
				return (SCCONF_EINVAL);
			}
			/* sync has also been counted as globaldev */
			(*num_gdev)--;
			break;
		/* internal error */
		default:
			return (SCCONF_EINVAL);
		}
	}
	if (i == 0)
		*num_gdev = 0;
	temp_listp[i] = NULL;
	return (SCCONF_NOERR);
}

/*
 * ds_get_globaldev_range_seq
 *
 * Get the devices in dc_gdev_range_seq_t structure.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_NOEXIST		- object does not exist
 *	SCCONF_ENOMEM		- not enough memory
 */
static scconf_errno_t
ds_get_globaldev_range_seq(char **globaldev_list, uint_t num_of_gdev,
    dc_gdev_range_seq_t **gdevs_range_seq)
{
	int i = 0;
	int error = 0;
	dc_gdev_range_t *gdev_ranges;
	struct stat file_stat;
	dev_t curr_dev = (dev_t)0;
	major_t maj;
	minor_t start_min, end_min;
	char path[MAXPATHLEN];
	char **globaldev_listp;

	/* Check arguments */
	if (globaldev_list == NULL || num_of_gdev == 0)
		return (SCCONF_EINVAL);

	if ((gdev_ranges = (dc_gdev_range_t *)calloc(num_of_gdev,
		sizeof (dc_gdev_range_t))) == NULL)
			return (SCCONF_ENOMEM);

	for (globaldev_listp = globaldev_list; *globaldev_listp;
	    ++globaldev_listp) {
		/* get device path */
		(void) sprintf(path, "/dev/did/rdsk/%ss0", *globaldev_listp);

		error = stat(path, &file_stat);

		if (error != 0) {
			return (SCCONF_ENOEXIST);
		}

		if (((file_stat.st_mode & S_IFMT) != S_IFCHR) &&
		    ((file_stat.st_mode & S_IFMT) != S_IFBLK)) {
			return (SCCONF_EINVAL);
		}

		curr_dev = file_stat.st_rdev;
		maj = getemajor(curr_dev);
		start_min = geteminor(curr_dev);
		end_min = start_min + 7;

		gdev_ranges[i].maj = maj;
		gdev_ranges[i].start_gmin = start_min;
		gdev_ranges[i].end_gmin = end_min;
		i++;
	}

	if (((*gdevs_range_seq) = (dc_gdev_range_seq_t *)
	    calloc(1, sizeof (dc_gdev_range_seq_t))) == NULL)
		return (SCCONF_ENOMEM);

	(*gdevs_range_seq)->ranges = gdev_ranges;
	(*gdevs_range_seq)->count = num_of_gdev;

	return (SCCONF_NOERR);
}

/*
 * ds_confirm_is_local_only_dg
 *
 * Confirm if global device is local only device.
 *
 * Possible return values:
 *
 *	B_TRUE
 *	B_FALSE
 */
static boolean_t
ds_confirm_is_local_only_dg(dc_property_seq_t *dsproperty_seq)
{
	char *value;
	boolean_t local_only = B_FALSE;

	value = ds_get_property(SCDG_LOCAL_ONLY, dsproperty_seq);
	if (value != NULL) {
		free(value);
		local_only = B_TRUE;
	}

	return (local_only);
}

/*
 * ds_get_property
 *
 *	Find the property named "name" in the given "dsproperty_seq".
 *
 *	If the property is not found, NULL is returned.   If the property
 *	does not have a key, a pointer to a NULL string is returned;
 *	otherwise, a pointer to the key is returned.
 *
 *	It is the callers responsibility to free any memory associated
 *	with the returned key.
 */
static char *
ds_get_property(char *name, dc_property_seq_t *dsproperty_seq)
{
	int i, count;
	dc_property_t *prop;

	/* Check arguments */
	if (name == NULL ||
	    dsproperty_seq == NULL || dsproperty_seq->properties == NULL)
		return (NULL);

	/* Search for property */
	prop = dsproperty_seq->properties;
	count = (int)dsproperty_seq->count;
	for (i = 0; i < count; i++) {

		/* A match? */
		if (prop->name && strcmp(name, prop->name) == 0) {
			if (prop->value == NULL || *prop->value == '\0') {
				return (strdup(""));
			} else {
				return (strdup(prop->value));
			}
		}

		++prop;
	}

	/* No match */
	return (NULL);
}

/*
 * ds_confirm_device_conn_to_host
 *
 * Confirm if all the global devices are physically connected to nodes.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EINVAL		- connectivity problem with given nodeset
 *	SCCONF_ENOMEM		- not enough memory
 */
static scconf_errno_t
ds_confirm_device_conn_to_host(char **globaldev_list, char **dsnodes,
    char **messages)
{
	int j;
	int k;
	uint_t num_dev_node;
	boolean_t found;
	char **temp_listp = NULL;
	char **globaldev_listp;
	did_device_list_t *did_device_listp;
	did_subpath_t *subptr;
	char *node_listp;
	char *token;
	char *p;
	char buffer[SCCONF_MAXSTRINGLEN];

	for (globaldev_listp = globaldev_list; *globaldev_listp;
	    ++globaldev_listp) {
		/* Get the 'did_device_list_t' for corresponding device */
		num_dev_node = 0;
		did_device_listp = map_from_did_device(*globaldev_listp);
		/* Cannot find it */
		if (!did_device_listp) {
			return (SCCONF_ENOEXIST);
		}

		for (subptr = did_device_listp->subpath_listp; subptr;
		    subptr = subptr->next) {
			num_dev_node++;
		}
		if ((temp_listp = (char **)calloc(num_dev_node + 1,
		    sizeof (char *))) == NULL) {
			return (SCCONF_ENOMEM);
		}

		/* Get the list of nodes a device should be connected to */
		node_listp = did_get_path(did_device_listp);
		if (node_listp == NULL) {
			if (did_geterrorstr() != NULL) {
				(void) fprintf(stderr, "%s", did_geterrorstr());
			}
			continue;
		}
		j = 0;
		for (token = strtok(node_listp, " "); token != NULL;
		    token = strtok(NULL, " ")) {
			if ((p = strchr(token, ':')) != NULL) {
				*p = '\0';
				temp_listp[j++] = token;
			}
		}

		temp_listp[j] = NULL;

		/* Check if the device is connected to all the nodes */
		k = 0;
		found = B_FALSE;
		while (dsnodes[k] != NULL) {
			j = 0;
			found = B_FALSE;
			while (temp_listp[j] != NULL) {
				/* Is device connected to the node */
				if (strncmp(dsnodes[k], temp_listp[j],
				    strlen(dsnodes[k])) == 0) {
					found = B_TRUE;
					break;
				}
				j++;
			}
			/*
			 * It appears lint has a problem with the
			 * above loop and indexing beyond array
			 * bounds.
			 */
			if (found == B_FALSE)	/*lint !e796 */
				break;
			k++;
		}
		if (temp_listp != NULL)
			free(temp_listp);
		if (found == B_FALSE) {
			(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
			    "Node \"%.256s\" is not connected to the disk "
			    "\"%.256s\".\n"), dsnodes[k], *globaldev_listp);
			scconf_addmessage(buffer, messages);
			if (node_listp != NULL) {
				free(node_listp);
			}
			return (SCCONF_EINVAL);
		}
		if (node_listp != NULL) {
			free(node_listp);
		}
	}
	return (SCCONF_NOERR);
}

/*
 * ds_get_num_of_nodes
 *
 * Get the number of nodes specified by the user.
 *
 * Possible return values: NONE
 *
 */
static uint_t
ds_get_num_of_nodes(char **dsnodes)
{
	uint_t count = 0;
	char **ptr;

	for (ptr = dsnodes; *ptr; ++ptr)
		count++;

	return (count);
}

/*
 * ds_get_local_property_seq
 *
 * populate the dc_property_seq_t structure.
 *
 * Possible return values:
 *
 *	DCS_SUCCESS		- success
 *	DCS_ERR_NOMEM		- not enough memory
 */
static dc_error_t
ds_get_local_property_seq(dc_property_seq_t **property_seq,
    scconf_nodeid_t nodeid)
{
	dc_error_t dc_err = DCS_SUCCESS;
	char value[MAXNAMELEN];

	/* Allocate property_seq */
	if ((*property_seq = (dc_property_seq_t *)
	    calloc(1, sizeof (dc_property_seq_t))) == NULL) {
		dc_err = DCS_ERR_NOMEM;
		goto cleanup;
	}

	(*property_seq)->count = 1;
	(*property_seq)->properties =
	    (dc_property_t *)malloc(sizeof (dc_property_t));
	if ((*property_seq)->properties == NULL) {
		dc_err = DCS_ERR_NOMEM;
		goto cleanup;
	}

	((*property_seq)->properties)[0].name = (char *)malloc(
	    strlen("local_only") + 1);
	if (((*property_seq)->properties)[0].name == NULL) {
		dc_err = DCS_ERR_NOMEM;
		goto cleanup;
	}
	(void) strcpy(((*property_seq)->properties)[0].name, SCDG_LOCAL_ONLY);

	((*property_seq)->properties)[0].value = (char *)malloc(
	    MAXNAMELEN);
	if (((*property_seq)->properties)[0].value == NULL) {
		dc_err = DCS_ERR_NOMEM;
		goto cleanup;
	}
	(void) sprintf(value, "%d", nodeid);
	(void) strcpy(((*property_seq)->properties)[0].value, value);

cleanup:
	return (dc_err);
}

/*
 * ds_get_global_property_seq
 *
 * populate the dc_property_seq_t structure.
 * Caller is resposible for freeing up property_seq.
 *
 * Possible return values:
 *
 *      DCS_SUCCESS            - success
 *      DCS_ERR_NOMEM           - not enough memory
 */
static dc_error_t
ds_get_global_property_seq(dc_property_seq_t **property_seq,
    char **globaldev_list)
{
	dc_error_t dc_err = DCS_SUCCESS;
	char **globaldev_listp;
	char gdev_names[BUFSIZ];

	/* Allocate property_seq */
	if ((*property_seq = (dc_property_seq_t *)
	    calloc(1, sizeof (dc_property_seq_t))) == NULL) {
		dc_err = DCS_ERR_NOMEM;
		goto cleanup;
	}

	(*property_seq)->count = 1;
	(*property_seq)->properties =
	    (dc_property_t *)calloc(1, sizeof (dc_property_t));
	if ((*property_seq)->properties == NULL) {
		dc_err = DCS_ERR_NOMEM;
		goto cleanup;
	}

	((*property_seq)->properties)[0].name = (char *)malloc(
	    strlen(SCDG_GDEV) + 1);
	if (((*property_seq)->properties)[0].name == NULL) {
		dc_err = DCS_ERR_NOMEM;
		goto cleanup;
	}
	(void) strcpy(((*property_seq)->properties)[0].name, SCDG_GDEV);

	((*property_seq)->properties)[0].value = (char *)malloc(
	    BUFSIZ);
	if (((*property_seq)->properties)[0].value == NULL) {
		dc_err = DCS_ERR_NOMEM;
		goto cleanup;
	}

	gdev_names[0] = '\0';
	(void) strcpy(gdev_names, *globaldev_list);
	++globaldev_list;

	for (globaldev_listp = globaldev_list; *globaldev_listp;
	    ++globaldev_listp) {
		(void) strcat(gdev_names, ":");
		(void) strcat(gdev_names, *globaldev_listp);
	}

	(void) strcpy(((*property_seq)->properties)[0].value, gdev_names);

cleanup:
	return (dc_err);
}

/*
 * ds_remove_property_seq
 *
 * remove the dc_property_seq_t structure from given device group.
 *
 * Possible return values:
 *
 *	DCS_SUCCESS		- success
 *	DCS_ERR_NOMEM		- not enough memory
 */
static dc_error_t
ds_remove_property_seq(char *dsname, dc_property_seq_t *dsproperty_seq)
{
	dc_error_t dc_err = DCS_SUCCESS;
	dc_string_seq_t string_seq;
	char **curr_string;
	uint_t i = 0;

	string_seq.strings = NULL;
	string_seq.count = 0;

	if ((string_seq.strings = (char **)
	    calloc(dsproperty_seq->count, sizeof (char *))) == NULL) {
		return (DCS_ERR_NOMEM);
	}

	curr_string = string_seq.strings;
	for (i = 0; i < dsproperty_seq->count; i++) {
		*curr_string = dsproperty_seq->properties[i].name;
		curr_string++;
		string_seq.count++;
	}

	/*
	 * remove the 'property_seq' value for device group
	 * configuration
	 */
	dc_err = dcs_remove_properties(dsname, &string_seq);
	if (dc_err != DCS_SUCCESS) {
		return (dc_err);
	}

	return (dc_err);
}

/*
 * ds_change_global_property_seq
 *
 * In remove case, add the property the device group, which excludes
 * the global devices being removed from the device group.
 * In change case add the new devices to the property structure.
 *
 * Possible return values:
 *
 *      DCS_SUCCESS            - success
 *      DCS_ERR_NOMEM           - not enough memory
 */
static dc_error_t
ds_change_global_property_seq(char *dsname, char **globaldev_list,
    dc_property_seq_t *dsproperty_seq, boolean_t isremove)
{
	dc_error_t dc_err = DCS_SUCCESS;
	scconf_errno_t scconferr;
	int i = 0;
	boolean_t is_device_exist = B_FALSE;
	int num_of_gdevs = 0;
	char **globaldev_listp;
	char gdev_names[BUFSIZ];
	char **tmp_gdev_names = NULL;
	dc_property_seq_t *property_seq;

	/* If there are no proerties set fot the device group */
	if (dsproperty_seq->count == 0) {
		/*
		 * Populate the property_seq structure from the global
		 * devices.
		 */
		dc_err = ds_get_global_property_seq(&property_seq,
		    globaldev_list);
		if (dc_err != DCS_SUCCESS)
			goto cleanup;
		if ((dc_err = dcs_set_properties(dsname, property_seq))
		    != DCS_SUCCESS) {
			goto cleanup;
		}
		goto cleanup;
	}

	/* Allocate property_seq */
	if ((property_seq = (dc_property_seq_t *)
	    calloc(1, sizeof (dc_property_seq_t))) == NULL) {
		dc_err = DCS_ERR_NOMEM;
		goto cleanup;
	}

	property_seq->count = 1;
	property_seq->properties =
	    (dc_property_t *)malloc(sizeof (dc_property_t));
	if (property_seq->properties == NULL) {
		dc_err = DCS_ERR_NOMEM;
		goto cleanup;
	}

	(property_seq->properties)[0].name = (char *)malloc(
	    strlen("gdev") + 1);
	if ((property_seq->properties)[0].name == NULL) {
		dc_err = DCS_ERR_NOMEM;
		goto cleanup;
	}

	(void) strcpy((property_seq->properties)[0].name, SCDG_GDEV);

	(property_seq->properties)[0].value = (char *)malloc(
	    BUFSIZ);
	if ((property_seq->properties)[0].value == NULL) {
		dc_err = DCS_ERR_NOMEM;
		goto cleanup;
	}

	if (dsproperty_seq != NULL) {
		/* Convert ":" seperated values to a char pointer array */
		scconferr = ds_get_gdevnames(
		    dsproperty_seq->properties[0].value, &tmp_gdev_names);
		if (scconferr != SCCONF_NOERR)
			goto cleanup;
	}

	gdev_names[0] = '\0';

	/* Check to see if remove command is issued */
	if (isremove) {
		/*
		 * Compare the name of all the devices currently exists
		 * in the property section of device group with the devices
		 * listed with the globaldev option. The devices which are
		 * listed with the globaldev option are removed from the device
		 * group.
		 */
		while (tmp_gdev_names[i] != NULL) {
			for (globaldev_listp = globaldev_list;
			    *globaldev_listp; ++globaldev_listp) {
				if (strcmp(tmp_gdev_names[i],
				    *globaldev_listp) == 0) {
					is_device_exist = B_TRUE;
					break;
				}
			}

			/*
			 * Create a new list which consists of all the
			 * devices existed in the properties, which excludes
			 * devices listed with the globaldev option.
			 */
			if (!is_device_exist) {
				if (num_of_gdevs == 0) {
					(void) strcpy(gdev_names,
					    tmp_gdev_names[i]);
				} else {
					(void) strcat(gdev_names, ":");
					(void) strcpy(gdev_names,
					    tmp_gdev_names[i]);
				}
				num_of_gdevs++;
			}
			is_device_exist = B_FALSE;
			i++;
		}
	}

	/* Check to see if add or change command is issued */
	if (!isremove) {
		/*
		 * Recreate a list which includes all the devices currently
		 * exists in the property section of the device group
		 * configuration.
		 */
		while (tmp_gdev_names[i] != NULL) {
			if (num_of_gdevs == 0) {
				(void) strcpy(gdev_names,
				    tmp_gdev_names[i]);
				num_of_gdevs++;
			} else {
				(void) strcat(gdev_names, ":");
				(void) strcat(gdev_names,
				    tmp_gdev_names[i]);
			}
			i++;
		}

		i = 0;
		/*
		 * Compare if the new devices listed with the globaldev
		 * option is already listed in the property section of
		 * the device group. If a device is found in the device
		 * group then set the flag.
		 */
		for (globaldev_listp = globaldev_list;
		    *globaldev_listp; ++globaldev_listp) {
			i = 0;
			while (tmp_gdev_names[i] != NULL) {
				if (strcmp(tmp_gdev_names[i],
				    *globaldev_listp) == 0) {
					is_device_exist = B_TRUE;
					break;
				}
				i++;
			}
			/* If the device doesn't exist */
			if (!is_device_exist) {
				(void) strcat(gdev_names, ":");
				(void) strcat(gdev_names,
				    *globaldev_listp);
			}
			is_device_exist = B_FALSE;
		}
	}

	if ((gdev_names != NULL) && (*gdev_names != NULL)) {
		(void) strcpy((property_seq->properties)[0].value, gdev_names);
		if ((dc_err = dcs_set_properties(dsname, property_seq))
		    != DCS_SUCCESS) {
			goto cleanup;
		}
	}

cleanup:
	if (tmp_gdev_names != NULL)
		free(tmp_gdev_names);
	dcs_free_properties(property_seq);
	return (dc_err);
}

/*
 * Convert an array of nodeids to an array of nodenames.
 */

static scconf_errno_t
ds_get_gdevnames(char *gdevlist, char ***out_gdevlist)
{
	char *tmp;
	uint_t numgdevs;
	char **dsgdevs = NULL;
	char **curr;

	if (gdevlist == NULL) {
		*out_gdevlist = dsgdevs;
		return (SCCONF_NOERR);
	}

	/* count the number of gdevs and allocate memory for dsgdevs */
	numgdevs = 0;
	for (tmp = gdevlist; tmp; tmp = strchr(tmp + 1, ':'))
		++numgdevs;

	dsgdevs = (char **)calloc(numgdevs + 1, sizeof (char *));
	if (dsgdevs == (char **)0) {
		return (SCCONF_ENOMEM);
	}

	/* Terminate the nodelist with a NULL */
	dsgdevs[numgdevs] = NULL;

	/* Fill out dsgdevs from the gdevlist */
	curr = dsgdevs;
	*curr = strtok(gdevlist, ":");
	/*
	 * lint has a problem with this while loop re. conceivable
	 * access out-of-bounds.
	 */
	/*lint -e796 */
	while (*curr)
		*(++curr) = strtok(NULL, ":");

	*out_gdevlist = dsgdevs;
	/*lint +e796 */

	return (SCCONF_NOERR);
}

/*
 * ds_get_node_seq for add option of scconf
 *
 * Set the node id and preference.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOEXIST		- the nodename(or, id) is not found
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
static scconf_errno_t
ds_get_node_seq(char **dsnodes, dc_replica_seq_t *dsnodes_seq,
    dc_replica_seq_t **nodes_seq, scconf_state_t nodepreference,
    uint_t num_nodes, scconf_state_t local_only)
{
	uint_t i = 0;
	uint_t j = 0;
	uint_t k = 0;
	scconf_errno_t rstatus = SCCONF_NOERR;
	scconf_nodeid_t nodeidp;
	dc_replica_t *nodes = (dc_replica_t *)0;
	boolean_t found = B_FALSE;
	boolean_t preference = B_FALSE;
	uint_t num_existing_nodes = 0;
	uint_t tmp1 = 0;
	uint_t tmp2 = 0;
	nodeid_t tmp_node;
	uint_t tmp_pref;

	/* Check arguments */
	if (dsnodes == NULL)
		return (SCCONF_EINVAL);

	/* If device group already exist */
	if (dsnodes_seq != NULL) {
		/* if nodepreference option is specified */
		if (nodepreference != SCCONF_STATE_UNCHANGED)
			return (SCCONF_EINVAL);
	} else {
		if (nodepreference == SCCONF_STATE_UNCHANGED)
			return (SCCONF_EINVAL);
	}


	/* If device group already exist */
	if (dsnodes_seq != NULL) {
		if (dsnodes_seq->replicas[i].preference
		    != SCDG_MAX_NODE_PREFERENCE)
			preference = B_TRUE;
		num_existing_nodes = dsnodes_seq->count;
	}

	num_nodes += num_existing_nodes;

	/* Allocate nodes */
	if ((nodes = (dc_replica_t *)calloc(num_nodes + 1,
	    sizeof (dc_replica_t))) == NULL)
		return (SCCONF_ENOMEM);

	/* Allocate nodes_seq */
	if (((*nodes_seq) = (dc_replica_seq_t *)
	    calloc(1, sizeof (dc_replica_seq_t))) == NULL)
		return (SCCONF_ENOMEM);

	if (local_only == SCCONF_STATE_ENABLED) {
		while (dsnodes[i] != NULL) {
			rstatus = scconf_get_nodeid(
			    dsnodes[i], &nodeidp);
			if (rstatus != SCCONF_NOERR)
				return (rstatus);

			nodes[i].id = (nodeid_t)nodeidp;
			nodes[i].preference = 0;
			i++;
		}
		(*nodes_seq)->replicas = nodes;
		(*nodes_seq)->count = num_nodes;
		return (rstatus);
	}

	switch (nodepreference) {
	case SCCONF_STATE_UNCHANGED:

		/*
		 * First add the existing nodes, compressing any holes in
		 * the preference ordering. Sorting is necessary as the
		 * nodelist obtained from dcs is not guaranteed to be
		 * sorted.
		 */

		for (tmp1 = 0; tmp1 < num_existing_nodes; tmp1++) {
			nodes[tmp1].id = dsnodes_seq->replicas[tmp1].id;
			if (preference == B_TRUE) {
				nodes[tmp1].preference =
				    dsnodes_seq->replicas[tmp1].preference;
			} else {
				nodes[tmp1].preference =
				    SCDG_MAX_NODE_PREFERENCE;
			}
		}

		if (preference == B_TRUE) {
			for (tmp1 = 0; (tmp1 + 1) < num_existing_nodes;
			    tmp1++) {
				for (tmp2 = tmp1 + 1;
				    tmp2 < num_existing_nodes; tmp2++) {
					if (nodes[tmp2].preference <
					    nodes[tmp1].preference) {
						tmp_node = nodes[tmp1].id;
						tmp_pref =
						    nodes[tmp1].preference;
						nodes[tmp1].id = nodes[tmp2].id;
						nodes[tmp1].preference =
						    nodes[tmp2].preference;
						nodes[tmp2].id = tmp_node;
						nodes[tmp2].preference =
						    tmp_pref;
					}
				}
			}
			for (tmp1 = 0; tmp1 < num_existing_nodes; tmp1++) {
				nodes[tmp1].preference = tmp1 + 1;
			}
		}

		k = num_existing_nodes;

		while (dsnodes[i] != NULL) {
			rstatus = scconf_get_nodeid(
			    dsnodes[i], &nodeidp);
			if (rstatus != SCCONF_NOERR)
				return (rstatus);

			for (j = 0; j < dsnodes_seq->count;
			    j++) {

				/*
				 * Check if node already
				 * exist
				 */
				if (dsnodes_seq->replicas[j].id
				    == nodeidp) {
					found = B_TRUE;
					break;
				}
			}
			if (found == B_TRUE) {
				found = B_FALSE;
				i++;
				continue;
			}

			/* Set node id */
			nodes[k].id = (nodeid_t)nodeidp;

			/* Set node preference */
			if (preference == B_TRUE) {
				nodes[k].preference = k + 1;
			} else {
				nodes[k].preference =
				    SCDG_MAX_NODE_PREFERENCE;
			}
			k++;
			i++;
		}
		break;

	case SCCONF_STATE_ENABLED:
		while (dsnodes[i] != NULL) {
			rstatus = scconf_get_nodeid(
			    dsnodes[i], &nodeidp);
			if (rstatus != SCCONF_NOERR)
				return (rstatus);

			nodes[i].id = (nodeid_t)nodeidp;
			nodes[i].preference = i + 1;
			i++;
		}
		break;

	case SCCONF_STATE_DISABLED:
		while (dsnodes[i] != NULL) {
			rstatus = scconf_get_nodeid(
			    dsnodes[i], &nodeidp);
			if (rstatus != SCCONF_NOERR)
				return (rstatus);

			nodes[i].id = (nodeid_t)nodeidp;
			nodes[i].preference =
			    SCDG_MAX_NODE_PREFERENCE;
			i++;
		}
		break;

	default:
			break;
	}
	/* because some nodes are alreay in the device group */
	if (nodepreference == SCCONF_STATE_UNCHANGED)
		num_nodes = k;


	(*nodes_seq)->replicas = nodes;
	(*nodes_seq)->count = num_nodes;

	return (rstatus);
}

/*
 * ds_set_node_seq for change option of scconf
 *
 * Set the node id and preference.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOEXIST		- the nodename(or, id) is not found
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
static scconf_errno_t
ds_set_node_seq(char **dsnodes, dc_replica_seq_t *dsnodes_seq,
    dc_replica_seq_t **nodes_seq, scconf_state_t nodepreference,
    uint_t num_nodes, scconf_state_t local_only, char **messages)

{
	uint_t i = 0;
	uint_t j = 0;
	scconf_errno_t rstatus = SCCONF_NOERR;
	scconf_nodeid_t nodeidp;
	boolean_t found = B_FALSE;
	boolean_t same_node = B_TRUE;
	char buffer[SCCONF_MAXSTRINGLEN];
	dc_replica_t *nodes = (dc_replica_t *)0;

	/*
	 * If no nodes are specified on the command line and want to
	 * change the node preference.
	 */
	if ((nodepreference == SCCONF_STATE_ENABLED) && (dsnodes == NULL)) {
		(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
		    "If the preferenced property is "
		    "set to true, you must specify "
		    "the list of nodes"));
		scconf_addmessage(buffer, messages);
		return (SCCONF_EINVAL);
	}

	if (((*nodes_seq) = (dc_replica_seq_t *)
	    calloc(1, sizeof (dc_replica_seq_t))) == NULL)
		return (SCCONF_ENOMEM);

	/* If want to change device group local only */
	if (local_only == SCCONF_STATE_ENABLED) {
		/* If a device group has more then one node */
		if (dsnodes_seq->count > 1)
			return (SCCONF_EINVAL);

		/* If nodes are specified on command line */
		if (dsnodes != NULL) {
			/* Get the node id */
			rstatus = scconf_get_nodeid(dsnodes[0],
			    &nodeidp);
			if (rstatus != SCCONF_NOERR)
				return (rstatus);
			/*
			 * If node id specified on the command line is
			 * different from the one in the device group.
			 */
			if (dsnodes_seq->replicas[0].id != nodeidp)
				same_node = B_FALSE;
		}

		/*
		 * If specified node is different from node registered with
		 * DCS
		 */
		if (same_node == B_FALSE)
			return (SCCONF_EINVAL);

		/* Set node preference for local only device group */
		dsnodes_seq->replicas[0].preference = 0;
		(*nodes_seq)->replicas = dsnodes_seq->replicas;
		(*nodes_seq)->count = 1;
		return (rstatus);
	/* If want to change the device group to non local only */
	} else if ((local_only == SCCONF_STATE_DISABLED) && (nodepreference ==
	    SCCONF_STATE_UNCHANGED)) {
		dsnodes_seq->replicas[0].preference = SCDG_MAX_NODE_PREFERENCE;
		(*nodes_seq)->replicas = dsnodes_seq->replicas;
		(*nodes_seq)->count = 1;
		return (rstatus);
	}

	/* If no preference is specified */
	if (nodepreference == SCCONF_STATE_UNCHANGED)
		return (SCCONF_NOERR);

	/* If want to change the node preference order for the device group */
	if (nodepreference == SCCONF_STATE_ENABLED) {
		/* Check if all the existing nodes are specified */
		if (dsnodes_seq != NULL) {
			if (num_nodes != dsnodes_seq->count) {
				(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
				    "If the preferenced property is "
				    "set to true, you must specify "
				    "the list of all nodes present "
				    "in the device group configuration"));
				scconf_addmessage(buffer, messages);
				return (SCCONF_EINVAL);
			}
		}

		/* Check if all the nodes are same as existing ones */
		while (dsnodes[i] != NULL) {
			rstatus = scconf_get_nodeid(dsnodes[i], &nodeidp);
			if (rstatus != SCCONF_NOERR)
				return (rstatus);

			for (j = 0; j < dsnodes_seq->count; j++) {
				if (dsnodes_seq->replicas[j].id ==
				    nodeidp) {
					found = B_TRUE;
					break;
				}
			}
			/* If any specified nodes are different */
			if (found == B_FALSE) {
				(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
				    "If the preferenced property is "
				    "set to true, you must specify "
				    "only the list of all nodes present "
				    "in the device group configuration"));
				scconf_addmessage(buffer, messages);
				return (SCCONF_ENOEXIST);
			} else {
				found = B_FALSE;
				i++;
			}
		}
	/* Get the nodelist from existing nodes */
	} else if (nodepreference == SCCONF_STATE_DISABLED) {
		for (j = 0; j < dsnodes_seq->count; j++)
			dsnodes_seq->replicas[j].preference =
			    SCDG_MAX_NODE_PREFERENCE;
		(*nodes_seq)->replicas = dsnodes_seq->replicas;
		(*nodes_seq)->count = dsnodes_seq->count;
		return (rstatus);
	}

	/* Allocate nodes */
	if ((nodes = (dc_replica_t *)calloc(num_nodes + 1,
	    sizeof (dc_replica_t))) == NULL)
		return (SCCONF_ENOMEM);

	/* Set the node preference and id's */
	rstatus = ds_set_nodes_preference(
	    dsnodes, &nodes, nodepreference);
	if (rstatus != SCCONF_NOERR)
		return (rstatus);

	(*nodes_seq)->replicas = nodes;
	(*nodes_seq)->count = num_nodes;

	return (rstatus);
}

/*
 * ds_set_nodes_preference
 *
 * Set the node id and node preference for change option of scconf.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOEXIST		- the nodename(or, id) is not found
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
static scconf_errno_t
ds_set_nodes_preference(char ** dsnodes, dc_replica_t **nodes,
    scconf_state_t nodepreference)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	scconf_nodeid_t nodeidp;
	uint_t i = 0;

	switch (nodepreference) {

		case SCCONF_STATE_ENABLED:
			while (dsnodes[i] != NULL) {
				rstatus = scconf_get_nodeid(
				    dsnodes[i], &nodeidp);
				if (rstatus != SCCONF_NOERR)
					return (rstatus);

				(*nodes)[i].id = (nodeid_t)nodeidp;
				(*nodes)[i].preference = i + 1;
				i++;
			}
			break;

		case SCCONF_STATE_DISABLED:
			while (dsnodes[i] != NULL) {
				rstatus = scconf_get_nodeid(
				    dsnodes[i], &nodeidp);
				if (rstatus != SCCONF_NOERR)
					return (rstatus);

				(*nodes)[i].id = (nodeid_t)nodeidp;
				(*nodes)[i].preference =
				    SCDG_MAX_NODE_PREFERENCE;
				i++;
			}
			break;

		default:
			break;
		}
	return (rstatus);
}

/*
 * ds_free_gdevlist
 * free all  memory for a gdev_list.
 */
static void
ds_free_gdevlist(char **gdev_list)
{
	int i = 0;

	if (!gdev_list) {
		return;
	}

	while (gdev_list[i] != NULL) {
		free(gdev_list[i]);
		i++;
	}
	free(gdev_list);
}

/*
 * ds_free_gdevs_range_seq
 * free all  memory for a "dc_gdev_range_seq_t" structure.
 */
static void
ds_free_gdevs_range_seq(dc_gdev_range_seq_t *gdevs_range_seq)
{

	if (gdevs_range_seq == NULL)
		return;

	if (gdevs_range_seq->ranges != NULL)
		free(gdevs_range_seq->ranges);

	free(gdevs_range_seq);
}

/*
 * ds_free_nodes_seq
 * free all  memory for a "dc_replica_seq_t" structure.
 */
static void
ds_free_nodes_seq(dc_replica_seq_t *nodes_seq)
{

	if (nodes_seq == NULL)
		return;

	if (nodes_seq->replicas != NULL)
		free(nodes_seq->replicas);

	free(nodes_seq);
}

/*
 * ds_dcs_create_service_to_scconf_err_status
 *
 * Convert dcs_create_service error codes to scconf error codes.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EEXIST		- device group already exists
 *	SCCONF_ENOEXIST		- an object does not exist
 *	SCCONF_EINUSE		- an object is already in use
 *	SCCONF_EBUSY		- busy state, try again later
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EUNKNOWN		- unknown type
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
static scconf_errno_t
ds_dcs_create_service_to_scconf_err_status(dc_error_t dc_err)
{
	if (dc_err == DCS_ERR_SERVICE_NAME)
		return (SCCONF_EEXIST);
	else
		return (ds_dcs_to_scconf_err_status(dc_err));
}

/*
 * ds_dcs_to_scconf_err_status
 *
 * Convert dcs error codes to scconf error codes.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOEXIST		- an object does not exist
 *	SCCONF_EINUSE		- an object is already in use
 *	SCCONF_EBUSY		- busy state, try again later
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EMAJOR		- driver major number is not same across nodes
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EUNKNOWN		- unknown type
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
static scconf_errno_t
ds_dcs_to_scconf_err_status(dc_error_t dc_err)
{
	scconf_errno_t rstatus = SCCONF_NOERR;

	switch (dc_err) {
		case DCS_SUCCESS:
			rstatus = SCCONF_NOERR;
			break;

		case DCS_ERR_SERVICE_NAME:
			rstatus = SCCONF_ENOEXIST;
			break;

		case DCS_ERR_SERVICE_CLASS_NAME:
			rstatus = SCCONF_EUNKNOWN;
			break;

		case DCS_ERR_DEVICE_INVAL:
		case DCS_ERR_SERVICE_INACTIVE:
			rstatus = SCCONF_ENOEXIST;
			break;

		case DCS_ERR_DEVICE_BUSY:
		case DCS_ERR_SERVICE_CLASS_BUSY:
			rstatus = SCCONF_EINUSE;
			break;

		case DCS_ERR_SERVICE_BUSY:
		case DCS_ERR_INVALID_STATE:
			rstatus = SCCONF_EBUSY;
			break;

		case DCS_ERR_NOMEM:
			rstatus = SCCONF_ENOMEM;
			break;

		case DCS_ERR_MAJOR_NUM:
			rstatus = SCCONF_EMAJOR;
			break;

		default:
			rstatus = SCCONF_EUNEXPECTED;
			break;
	}
	return (rstatus);
}

/*
 * Helper function to free a 'scconf_namelist_t' structure.
 */
static void
ds_free_namelist_local(scconf_namelist_t *namelist)
{
	scconf_namelist_t *last, *next;

	if (namelist == (scconf_namelist_t *)0)
		return;

	/* free memory for each member of the list */
	last = (scconf_namelist_t *)0;
	next = namelist;
	while (next != NULL) {
		if (next->scconf_namelist_name != NULL)
			free(next->scconf_namelist_name);

		last = next;
		next = next->scconf_namelist_next;
		free(last);
	}
}

/*
 * Given a list of nodenames, convert them to nodeids.
 */
static scconf_errno_t
ds_get_nodeids(char *dsnodes[], scconf_nodeid_t **dsnodeids)
{
	int i = 0, j;
	scconf_errno_t err = SCCONF_NOERR;

	if (dsnodes != NULL) {
		for (i = 0; dsnodes[i] != NULL; i++)
			;
	}

	*dsnodeids = (scconf_nodeid_t *)calloc((size_t)i + 1,
	    sizeof (scconf_nodeid_t));
	if (*dsnodeids == NULL) {
		return (SCCONF_ENOMEM);
	}

	(*dsnodeids)[i] = 0;

	for (j = 0; j < i; j++) {
		err = scconf_get_nodeid(dsnodes[j], &((*dsnodeids)[j]));
		if (err != SCCONF_NOERR) {
			free(*dsnodeids);
			return (err);
		}
	}

	return (err);
}

/*
 * add_replication
 *
 * Verifies current setup and previous setup and sets set_repl
 * variable to 0, 1 or 2.  Replication property is added
 * or removed accordingly.
 *
 *	No return value.
 */
void
add_replication(dc_property_seq_t *dsproperty_seq, boolean_t cur_repl,
    int *new_repl_val)
{
	char *value;
	*new_repl_val = 0; /* By default no removal or addition */

	value = ds_get_property(SCCONF_DS_PROP_REPLICATED_DEVICE,
	    dsproperty_seq);
	if (value == NULL && cur_repl == B_TRUE) {
		/* Currently not set and configuration is ok. */
		*new_repl_val = 1; /* will be added */
	}
	if (value != NULL && cur_repl == B_FALSE) {
		/* Currently set and configuration is not ok. */
		*new_repl_val = 2; /* will be removed */
	}
}

/*
 * add_repl_property
 *
 * Adds replication property to property_seq
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM		- not enough memory
 *
 */
scconf_errno_t
add_repl_property(dc_property_seq_t **property_seq, char *repl_type)
{

	uint_t property_count;
	uint_t i;
	int found = 0;
	int rstatus = SCCONF_NOERR;

	if (*property_seq != NULL)
		property_count = (*property_seq)->count;
	else {
		/* Allocate memory for new property_seq */
		property_count = 0;
		(*property_seq) = (dc_property_seq_t *)malloc(
		    sizeof (dc_property_seq_t));
		if ((*property_seq) == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		} else {
			(*property_seq)->properties = NULL;
			(*property_seq)->count = 0;
		}
	}

	for (i = 0; i < property_count; i++) {
		if (strcmp((*property_seq)->properties[i].name,
		    SCCONF_DS_PROP_REPLICATED_DEVICE) == 0) {
			found = 1;
			break;
		}
	}
	if (found == 1)
		return (SCCONF_NOERR);

	(*property_seq)->properties = (dc_property_t *)realloc(
	    (*property_seq)->properties,
	    (property_count+1) * sizeof (dc_property_t));
	if ((*property_seq)->properties == NULL) {
		rstatus = SCCONF_ENOMEM;
		goto cleanup;
	}

	if (((*property_seq)->properties[property_count].name =
	    strdup(SCCONF_DS_PROP_REPLICATED_DEVICE)) == NULL) {
		rstatus = SCCONF_ENOMEM;
		goto cleanup;
	}

	if (((*property_seq)->properties[property_count].value =
	    strdup(repl_type)) == NULL) {
		rstatus = SCCONF_ENOMEM;
		goto cleanup;
	}

	(*property_seq)->count = (uint_t)property_count + 1;

cleanup:
	return (rstatus);
}
