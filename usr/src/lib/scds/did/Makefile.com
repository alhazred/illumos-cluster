#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.24	08/05/20 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# lib/scds/did/Makefile.com
#

LIBRARY= scconf_rawdisk.a
VERS=.1

OBJECTS= scds_did.o

# include library definitions
include ../../../Makefile.lib

# override value of ROOTCLUSTLIBDIR set in Makefile.lib
ROOTCLUSTLIBDIR=        $(VROOT)/usr/cluster/lib/dcs

MAPFILE=	../common/mapfile-vers
SRCS=		$(OBJECTS:%.o=../common/%.c)
CLOBBERFILES += $(PIFILES)
CHECKHDRS=	scds_did.h

LIBS=		$(DYNLIB)

LINTFLAGS += -I..
LINTFILES += $(OBJECTS:%.o=%.ln)

MTFLAG	=	-mt
CPPFLAGS +=	-I.. -I$(SRC)/common/cl -I$(SRC)/lib/libdid/include
PMAP =	-M $(MAPFILE)
LDLIBS +=       -lc -ldid -lscconf -lclconf -lc

.KEEP_STATE:

$(DYNLIB):	$(MAPFILE)

# include library targets
include ../../../Makefile.targ

objs/%.o pics/%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<
