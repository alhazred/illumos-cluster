/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scds_local.c	1.3	08/05/20 SMI"

/*
 * scds_local.so Multi-owner SVM config functions
 */

#include <scds_local.h>
#include <sys/debug.h>

#define	SCDG_DCS_SERVICE_TYPE "SUNWlocal"
#define	SCDG_SCCONF_SERVICE_TYPE "multi-owner-svm"
#define	SCDG_MAX_NODE_PREFERENCE 64
#define	SCDG_SDS_DEVICE_PATH "/dev/md"

static scconf_errno_t ds_set_node_seq(scconf_nodeid_t *dsnodeids,
    dc_replica_seq_t *dsnodes_seq, dc_replica_seq_t **nodes_seq,
    scconf_state_t nodepreference, int num_nodes);
static scconf_errno_t ds_set_nodes_preference(scconf_nodeid_t *dsnodeids,
    dc_replica_t **nodes, scconf_state_t nodepreference);
static void ds_free_nodes_seq(dc_replica_seq_t *nodes_seq);
static scconf_errno_t ds_dcs_create_service_to_scconf_err_status(dc_error_t);
static scconf_errno_t ds_dcs_to_scconf_err_status(dc_error_t dc_err);
static dc_error_t ds_change_service_parameters(char *service_name,
    int failback_enabled, dc_replica_seq_t *nodes_seq,
    dc_property_seq_t *property_seq, unsigned int dsnumsecondaries);
static dc_error_t ds_remove_service_parameters(char *service_name,
    scconf_nodeid_t *dsnodeids, dc_string_seq_t *property_seq);
static dc_error_t ds_add_nodes(char *service_name, dc_replica_seq_t *nodes_seq);
static void ds_get_failback_value(scconf_state_t dsfailback, int *
    failback_value, int enable_failback);
static scconf_errno_t ds_get_device_list(char *searchpath, const uint_t iflag,
    scconf_namelist_t **ds_devicenames);
static scconf_errno_t ds_get_nodeids(char *dsnodes[],
    scconf_nodeid_t **dsnodeids);
static int ds_get_num_of_nodes(scconf_nodeid_t *dsnodeids);
static scconf_errno_t ds_get_nodelist(char *dsnodes[],
    scconf_state_t dspreference, dc_replica_seq_t **dsnodes_seq);
static void ds_free_nodelist(dc_replica_seq_t *dsnodes_seq);
static scconf_errno_t ds_get_gdevlist(scconf_gdev_range_t *dsdevices,
    dc_gdev_range_seq_t **dsgdevs_range_seq);
static void ds_free_gdevlist(dc_gdev_range_seq_t *dsgdevs_range_seq);
static scconf_errno_t ds_get_propertylist(scconf_cfg_prop_t *dsproperties,
    dc_property_seq_t **dsproperty_seq);
static void ds_free_propertylist(dc_property_seq_t *dsproperty_seq);
static scconf_errno_t ds_get_propertynames(scconf_cfg_prop_t *dsproperties,
    dc_string_seq_t **dsproperty_seq);
static void ds_free_string_seq(dc_string_seq_t *dsstring_seq);
static scconf_errno_t ds_convert_parameters(const char *service_name,
    int failback_enabled, unsigned int num_secs, dc_replica_seq_t *nodes_seq,
    dc_gdev_range_seq_t *gdev_range_seq, dc_property_seq_t *property_seq,
    const uint_t options, scconf_cfg_ds_t **dsconfigp);

/*
 * Add a Multi-owner SVM device service(local only).
 *
 * The "dsnodes" argument is a NULL terminated array of character
 * pointers each pointing to a nodename.  The order in which the nodes
 * are given establishes preference.  Some device types provide a method for
 * defaulting the nodes preference list, allowing "dsnodes" to be
 * NULL.
 *
 * If dsfailback is set to SCCONF_STATE_UNCHANGED, the default is
 * to disable failback.
 *
 *  This "local" function will only operate on the local node.  In cases
 *  where RPC is needed to send the request to the node where the disk group
 *  is imported, the non-local function should be used.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EEXIST		- device group already exists
 *	SCCONF_ENOEXIST		- diskset or node is not found
 *	SCCONF_EINUSE		- the diskset is already in use
 *	SCCONF_EBUSY		- busy, try again later
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EUNKNOWN		- unknown type
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_ERANGE		- operation not allowed because of out of range
 *				  numsecondaries
 *	SCCONF_DS_EINVAL	- inconsistencies detected in device
 *				  configuration
 */
/*lint -e715 */
scconf_errno_t
scds_add_ds_local(char *dsname, char *dsnodes[], scconf_state_t dspreference,
    scconf_gdev_range_t *dsdevices, scconf_state_t dsfailback,
    scconf_cfg_prop_t *dsproperties, char *dsoptions,
    unsigned int dsnumsecondaries, char **messages)
{
	dc_error_t dc_err = DCS_SUCCESS;
	scconf_errno_t rstatus = SCCONF_NOERR;
	int enable_failback = 0;
	int failback_value = 0;
	dc_gdev_range_seq_t *dsgdevs_range_seq = NULL;
	dc_replica_seq_t *dsnodes_seq = NULL;
	dc_property_seq_t *dsproperty_seq = NULL;


	/* Check agruments */
	if (dsname == NULL)
		return (SCCONF_EINVAL);

	if (dsoptions != NULL)
		return (SCCONF_EINVAL);

	if (dspreference == SCCONF_STATE_UNCHANGED)
		dspreference = SCCONF_STATE_DISABLED;

	/* Determine if device group exist */
	dc_err = dcs_get_service_parameters(dsname, NULL, NULL, NULL, NULL,
	    NULL, NULL);

	if (dc_err != DCS_ERR_SERVICE_NAME) {
		/*
		 * A successful return from dcs_get_service_parameters means
		 * that a device service with the same name already exists.
		 */
		if (dc_err == DCS_SUCCESS) {
			rstatus = SCCONF_EEXIST;
		} else {
			rstatus = ds_dcs_to_scconf_err_status(dc_err);
		}
		goto cleanup;
	}

	/* Get the failback value */
	ds_get_failback_value(dsfailback, &failback_value, enable_failback);

	/* Get the list of nodes */
	rstatus = ds_get_nodelist(dsnodes, dspreference, &dsnodes_seq);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/* Get the list of devices */
	rstatus = ds_get_gdevlist(dsdevices, &dsgdevs_range_seq);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/* Get the list of properties */
	rstatus = ds_get_propertylist(dsproperties, &dsproperty_seq);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/*
	 * We should use the default value if the user didn't specify
	 * the numsec value during a service creation.
	 */
	if (dsnumsecondaries == SCCONF_NUMSECONDARIES_UNSET) {
		dsnumsecondaries = SCCONF_NUMSECONDARIES_SET_DEFAULT;
	} else {
		if (dsnumsecondaries > (dsnodes_seq->count - 1)) {
			rstatus = SCCONF_ERANGE;
			goto cleanup;
		}
	}

	/* Create new device group */
	dc_err = dcs_create_service(dsname, SCDG_DCS_SERVICE_TYPE,
	    failback_value, dsnodes_seq, dsnumsecondaries, dsgdevs_range_seq,
	    dsproperty_seq);
	if (dc_err != DCS_SUCCESS) {
		rstatus = ds_dcs_create_service_to_scconf_err_status(dc_err);
	}

cleanup:
	ds_free_nodelist(dsnodes_seq);
	ds_free_gdevlist(dsgdevs_range_seq);
	ds_free_propertylist(dsproperty_seq);

	return (rstatus);
}
/*lint +e715 */

/*
 * Add a Multi-owner SVM device service.
 *
 * The "dsnodes" argument is a NULL terminated array of character
 * pointers each pointing to a nodename.  The order in which the nodes
 * are given establishes preference.  Some device types provide a method for
 * defaulting the nodes preference list, allowing "dsnodes" to be
 * NULL.
 *
 * If dsfailback is set to SCCONF_STATE_UNCHANGED, the default is
 * to disable failback.
 *
 * This function will work for both local and remote invocation.
 * For Multi-owner SVM this function always calls the local function.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EEXIST		- device group already exists
 *	SCCONF_ENOEXIST		- diskset or node is not found
 *	SCCONF_EINUSE		- the diskset is already in use
 *	SCCONF_EBUSY		- busy, try again later
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EUNKNOWN		- unknown type
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_ERANGE		- operation not allowed because of out of range
 *				  numsecondaries
 */
scconf_errno_t
scds_add_ds(char *dsname, char *dsnodes[], scconf_state_t dspreference,
    scconf_gdev_range_t *dsdevices, scconf_state_t dsfailback,
    scconf_cfg_prop_t *dsproperties, char *dsoptions,
    unsigned int dsnumsecondaries, char **messages)
{
	return (scds_add_ds_local(dsname, dsnodes, dspreference, dsdevices,
	    dsfailback, dsproperties, dsoptions, dsnumsecondaries, messages));
}

/*
 * scds_get_ds_type for multi-owner svm disk
 *
 * Confirm if device type is "SUNWlocal" or "multi-owner-svm". This check
 * is to make sure that right .so is opened.
 *
 * Possible return values:
 *
 *	less than or greater than zero		- error
 *	equal to zero				- success
 */
boolean_t
scds_get_ds_type(char *scconf_type, char *dcs_type)
{
	/*
	 * Check to make sure if device type is "SUNWlocal"
	 * or "multi-owner-svm"
	 */
	if (((dcs_type) && (strcmp(dcs_type, SCDG_DCS_SERVICE_TYPE) == 0)) ||
	    ((scconf_type) && (strcmp(scconf_type, SCDG_SCCONF_SERVICE_TYPE)
	    == 0)))
		return (B_TRUE);
	else
		return (B_FALSE);

}

/*
 * scds_change_ds_local for multi-owner svm device group(local only)
 *
 * Change the configuration of the existing device group in DCS.
 * First checks if device group already exist in DCS system, if it exists,
 * change the node preference order and the failback mode for a device group.
 * No new nodes can be added with this function. To change the order of the
 * node preference, all the nodes in the device group should be specied by
 * the user.
 *
 *  This "local" function will only operate on the local node.  In cases
 *  where RPC is needed to send the request to the node where the disk group
 *  is imported, the non-local function should be used.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOEXIST		- device group or node is not found
 *	SCCONF_EINUSE		- an object is already in use
 *	SCCONF_EBUSY		- busy state, try again later
 *	SCCONF_EINVAL           - invalid argument
 *	SCCONF_ENOMEM           - not enough memory
 *	SCCONF_EUNKNOWN         - unknown type
 *	SCCONF_EUNEXPECTED      - internal or unexpected error
 *	SCCONF_ERANGE		- operation not allowed because of out of range
 *				  numsecondaries
 *	SCCONF_DS_EINVAL	- inconsistencies detected in device
 *				  configuration
 */
/*lint -e715 */
scconf_errno_t
scds_change_ds_local(char *dsname, char *dsnodes[],
    scconf_state_t dspreference,
    scconf_gdev_range_t *dsdevices, scconf_state_t dsfailback,
    scconf_cfg_prop_t *dspropertylist, char *dsoptions,
    unsigned int dsnumsecondaries, char **messages)
{
	dc_error_t dc_err = DCS_SUCCESS;
	scconf_errno_t rstatus = SCCONF_NOERR;
	int num_of_nodes = 0;
	int enable_failback = 0;
	int failback_value = 0;
	dc_replica_seq_t *nodes_seq = NULL;
	dc_replica_seq_t *dsnodes_seq = NULL;
	scconf_nodeid_t *dsnodeids = NULL;
	dc_property_seq_t *properties = NULL;

	/* Initialize */
	dsdevices = NULL;

	/* to make lint happy */
	dsdevices = dsdevices;

	if (dsoptions != NULL)
		return (SCCONF_EINVAL);

	/* Convert the nodenames to nodeids */
	if (dsnodes) {
		rstatus = ds_get_nodeids(dsnodes, &dsnodeids);
		if (rstatus != SCCONF_NOERR) {
			return (rstatus);
		}
	}

	/* Determine if device group exist */
	dc_err = dcs_get_service_parameters(dsname, NULL, &enable_failback,
	    &dsnodes_seq, NULL, NULL, NULL);
	if (dc_err != DCS_SUCCESS) {
		rstatus = ds_dcs_to_scconf_err_status(dc_err);
		goto cleanup;
	}

	/* Check agruments */
	if (dsnodeids != NULL) {
		num_of_nodes = ds_get_num_of_nodes(dsnodeids);

		/* Populate nodes_seq structure */
		rstatus = ds_set_node_seq(dsnodeids, dsnodes_seq,
		    &nodes_seq, dspreference, num_of_nodes);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		if ((dsnumsecondaries != SCCONF_NUMSECONDARIES_UNSET) &&
		    (dsnumsecondaries != SCCONF_NUMSECONDARIES_SET_DEFAULT) &&
		    (dsnumsecondaries > (nodes_seq->count - 1))) {
			rstatus = SCCONF_ERANGE;
			goto cleanup;
		}
	} else {
		if ((dsnumsecondaries != SCCONF_NUMSECONDARIES_UNSET) &&
		    (dsnumsecondaries != SCCONF_NUMSECONDARIES_SET_DEFAULT) &&
		    (dsnumsecondaries > (dsnodes_seq->count - 1))) {
			rstatus = SCCONF_ERANGE;
			goto cleanup;
		}
	}

	/* Get failback value */
	ds_get_failback_value(dsfailback, &failback_value, enable_failback);

	/* Get properties */
	rstatus = ds_get_propertylist(dspropertylist, &properties);
	if (rstatus != SCCONF_NOERR) {
		goto cleanup;
	}

	/* Change the configuration for existing device group */
	dc_err = ds_change_service_parameters(dsname, failback_value,
	    nodes_seq, properties, dsnumsecondaries);
	if (dc_err != DCS_SUCCESS) {
		rstatus = ds_dcs_to_scconf_err_status(dc_err);
	}

cleanup:
	dcs_free_dc_replica_seq(dsnodes_seq);
	ds_free_nodes_seq(nodes_seq);
	ds_free_propertylist(properties);
	if (dsnodeids != NULL)
		free(dsnodeids);

	return (rstatus);
}
/*lint +e715 */

/*
 * scds_change_ds for multi-owner svm device group
 *
 * Change the configuration of the existing device group in DCS.
 * First checks if device group already exist in DCS system, if it exists,
 * change the node preference order and the failback mode for a device group.
 * No new nodes can be added with this function. To change the order of the
 * node preference, all the nodes in the device group should be specied by
 * the user.
 *
 * This function will work for both local and remote invocation.
 * For Multi-owner SVM this function always calls the local function.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOEXIST		- device group or node is not found
 *	SCCONF_EINUSE		- an object is already in use
 *	SCCONF_EBUSY		- busy state, try again later
 *	SCCONF_EINVAL           - invalid argument
 *	SCCONF_ENOMEM           - not enough memory
 *	SCCONF_EUNKNOWN         - unknown type
 *	SCCONF_EUNEXPECTED      - internal or unexpected error
 *	SCCONF_ERANGE		- operation not allowed because of out of range
 *				  numsecondaries
 */
scconf_errno_t
scds_change_ds(char *dsname, char *dsnodes[], scconf_state_t dspreference,
    scconf_gdev_range_t *dsdevices, scconf_state_t dsfailback,
    scconf_cfg_prop_t *dspropertylist, char *dsoptions,
    unsigned int dsnumsecondaries, char **messages)
{
	return (scds_change_ds_local(dsname, dsnodes, dspreference, dsdevices,
	    dsfailback, dspropertylist, dsoptions, dsnumsecondaries,
	    messages));
}

/*
 * Remove a Multi-owner SVM device service.
 *
 * The "dsnodes" argument is a NULL terminated array of character
 * pointers each pointing to a nodename.  The order in which the nodes
 * are given establishes preference.  Some device types provide a method for
 * defaulting the nodes preference list, allowing "dsnodes" to be
 * NULL.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOEXIST		- device group or node is not found
 *	SCCONF_EINUSE		- an object is already in use
 *	SCCONF_EBUSY		- busy state, try again later
 *	SCCONF_EINVAL           - invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EUNKNOWN         - unknown type
 *	SCCONF_EUNEXPECTED      - internal or unexpected error
 *	SCCONF_ERANGE		- operation not allowed because of out of range
 *				  numsecondaries
 */
/*lint -e715 */
scconf_errno_t
scds_remove_ds(char *dsname, char *dsnodes[], scconf_gdev_range_t *dsdevices,
    scconf_cfg_prop_t *dspropertylist, char *dsoptions, char **messages)
{
	unsigned int j = 0;
	unsigned int k = 0;
	dc_error_t dc_err = DCS_SUCCESS;
	scconf_errno_t rstatus = SCCONF_NOERR;
	scconf_nodeid_t *dsnodeids = NULL;
	dc_string_seq_t *properties = NULL;
	dc_replica_seq_t *dsnodes_seq = (dc_replica_seq_t *)0;
	unsigned int node_count = 0;
	unsigned int remaining_nodes = 0;
	unsigned int num_secs = 0;
	boolean_t node_found = B_FALSE;

	/* Initialize */
	dsdevices = NULL;

	/* to make lint happy */
	dsdevices = dsdevices;

	if (dsoptions != NULL)
		return (SCCONF_EINVAL);

	/* Determine if device group exist */
	dc_err = dcs_get_service_parameters(dsname, NULL, NULL, &dsnodes_seq,
	    &num_secs, NULL, NULL);
	if (dc_err != DCS_SUCCESS) {
		rstatus = ds_dcs_to_scconf_err_status(dc_err);
		goto cleanup;
	}

	/* Get properties */
	rstatus = ds_get_propertynames(dspropertylist, &properties);
	if (rstatus != SCCONF_NOERR) {
		goto cleanup;
	}

	/* Get nodes */
	rstatus = ds_get_nodeids(dsnodes, &dsnodeids);
	if (rstatus != SCCONF_NOERR) {
		goto cleanup;
	}

	/*
	 * If the device service had a non-default numsecondaries property
	 * set, and the node removal operation, if successful, can result in
	 * lower than the numsecondaries number of secondaries, we fail the
	 * operation.
	 */
	while (dsnodeids[node_count] != 0) {
		node_count++;
	}

	if (dsnodes_seq != (dc_replica_seq_t *)NULL) {

		remaining_nodes = 0;
		for (j = 0; j < dsnodes_seq->count; j++) {
			node_found = B_FALSE;
			for (k = 0; k < node_count; k++) {
				if (dsnodeids[k] ==
				    dsnodes_seq->replicas[j].id) {
					node_found = B_TRUE;
					break;
				}
			}
			if (node_found == B_FALSE) {
				remaining_nodes++;
			}
		}

		if (num_secs > DCS_DEFAULT_DESIRED_NUM_SECONDARIES) {
			if ((remaining_nodes != 0) &&
			    (remaining_nodes < (num_secs + 1))) {
				rstatus = SCCONF_ERANGE;
				goto cleanup;
			}
		}
	}

	/* Change the configuration for existing device group */
	dc_err = ds_remove_service_parameters(dsname, dsnodeids, properties);
	rstatus = ds_dcs_to_scconf_err_status(dc_err);

cleanup:
	ds_free_string_seq(properties);
	if (dsnodeids != NULL)
		free(dsnodeids);

	return (rstatus);
}
/*lint +e715 */

/*
 * scds_get_ds_config for multi-owner svm device group
 *
 * Upon success a list of all the multi-owner svm device group parameters are
 * returned in dsconfigp.
 * The caller is responsible for freeing the memory for dsconfigp.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR            - success
 *      SCCONF_ENOMEM           - not enough memory
 *      SCCONF_ENOEXIST         - the nodeid is not found
 *      SCCONF_EINVAL           - invalid argument
 *      SCCONF_EUNEXPECTED      - internal or unexpected error
 */
scconf_errno_t
scds_get_ds_config(char *dsname, const uint_t options,
    scconf_cfg_ds_t **dsconfigp)
{
	unsigned int i = 0;
	dc_error_t dc_err = DCS_SUCCESS;
	scconf_errno_t rstatus = SCCONF_NOERR;
	int enable_failback = 0;
	unsigned int num_secs = 0;
	dc_replica_seq_t *dsnodes_seq = NULL;
	dc_property_seq_t *dsproperty_seq = NULL;
	dc_gdev_range_seq_t *dsgdevrange_seq = NULL;

	if (options & SCCONF_BY_NAME_FLAG) {
		ASSERT(dsname != NULL);

		/* Get parameters for the specified device group name */
		dc_err = dcs_get_service_parameters(dsname, NULL,
		    &enable_failback, &dsnodes_seq, &num_secs, &dsgdevrange_seq,
		    &dsproperty_seq);
		if (dc_err != DCS_SUCCESS) {
			rstatus = ds_dcs_to_scconf_err_status(dc_err);
			goto cleanup;
		}

		/* Place the parameters in 'dsconfigp'... */
		rstatus = ds_convert_parameters(dsname, enable_failback,
		    num_secs, dsnodes_seq, dsgdevrange_seq, dsproperty_seq,
		    options, dsconfigp);
		/* ... and cleanup */
		dcs_free_dc_replica_seq(dsnodes_seq);
		dcs_free_dc_gdev_range_seq(dsgdevrange_seq);
		dcs_free_properties(dsproperty_seq);
	} else {
		dc_string_seq_t *dsnames;

		ASSERT(options & SCCONF_BY_TYPE_FLAG);
		ASSERT(dsname == NULL);

		/* Get the list of Multi-owner SVM devices */
		dc_err = dcs_get_service_names_of_class(SCDG_DCS_SERVICE_TYPE,
		    &dsnames);
		if (dc_err != DCS_SUCCESS) {
			rstatus = ds_dcs_to_scconf_err_status(dc_err);
			goto cleanup;
		}

		/* Iterate through the list */
		for (i = 0; i < dsnames->count; i++) {
			num_secs = 0;
			enable_failback = 0;

			/* Get the parameters for a specific device service */
			dc_err = dcs_get_service_parameters(dsname, NULL,
			    &enable_failback, &dsnodes_seq, &num_secs,
			    &dsgdevrange_seq, &dsproperty_seq);
			if (dc_err != DCS_SUCCESS) {
				rstatus = SCCONF_EUNEXPECTED;
				break;
			}

			/* Place the parameters in 'dsconfigp'... */
			rstatus = ds_convert_parameters(dsname, enable_failback,
			    num_secs, dsnodes_seq, dsgdevrange_seq,
			    dsproperty_seq, options, dsconfigp);
			/* ... and cleanup */
			dcs_free_dc_replica_seq(dsnodes_seq);
			dcs_free_dc_gdev_range_seq(dsgdevrange_seq);
			dcs_free_properties(dsproperty_seq);

			/* Break out on error */
			if (rstatus != SCCONF_NOERR) {
				break;
			}
		}
		dcs_free_string_seq(dsnames);
	}

cleanup:
	return (rstatus);
}

/*
 * Get from the libdcs-defined structures to an scconf-defined structure.
 */
static scconf_errno_t
ds_convert_parameters(const char *dsname, int enable_failback,
    unsigned int num_secs, dc_replica_seq_t *dsnodes_seq,
    dc_gdev_range_seq_t *dsgdev_seq, dc_property_seq_t *dsproperty_seq,
    const uint_t options, scconf_cfg_ds_t **dsconfp)
{
	scconf_namelist_t *ds_devicenames;
	scconf_namelist_t *ds_cdevicenames = NULL;
	scconf_namelist_t *ds_bdevicenames = NULL;
	scconf_cfg_prop_t *property = NULL;
	char searchpath[MAXPATHLEN];
	char bsearchpath[MAXPATHLEN];
	unsigned int i = 0;
	scconf_errno_t rstatus = SCCONF_NOERR;
	scconf_cfg_ds_t *curr;
	scconf_nodeid_t *nodelist;

	curr = *dsconfp;

	if ((curr->scconf_ds_name = strdup(dsname)) == NULL) {
		rstatus = SCCONF_ENOMEM;
		goto cleanup;
	}

	/* Local device class appears as a "Multi-owner_SVM" */
	if ((curr->scconf_ds_type = strdup("Multi-owner_SVM")) == NULL) {
		rstatus = SCCONF_ENOMEM;
		goto cleanup;
	}

	if ((curr->scconf_ds_label = strdup(gettext("diskset name")))
	    == NULL) {
		rstatus = SCCONF_ENOMEM;
		goto cleanup;
	}

	/* Get the failback value */
	if (enable_failback == 0) {
		curr->scconf_ds_failback = SCCONF_STATE_DISABLED;
	} else {
		curr->scconf_ds_failback = SCCONF_STATE_ENABLED;
	}

	/* Get the desired number of secondaries */
	curr->scconf_ds_num_secs = num_secs;

	/* Get the list of nodes */
	curr->scconf_ds_nodelist = NULL;

	/*
	 * This service class does not contain replicas so just set
	 * ordered node list preference to DISABLED
	 */
	curr->scconf_ds_preference = SCCONF_STATE_DISABLED;
	if (dsnodes_seq->count > 0) {

		nodelist = (scconf_nodeid_t *)calloc(dsnodes_seq->count+1,
		    sizeof (scconf_nodeid_t));
		if (nodelist == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		for (i = 0; i < dsnodes_seq->count; i++)
			    nodelist[i] = dsnodes_seq->replicas[i].id;
		nodelist[dsnodes_seq->count] = 0;
		curr->scconf_ds_nodelist = nodelist;
	}
	/* Get the list of properties */
	for (i = 0; i < dsproperty_seq->count; i++) {
		property = (scconf_cfg_prop_t *)malloc(
			sizeof (scconf_cfg_prop_t));
		if (property == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}
		property->scconf_prop_key =
		    strdup(dsproperty_seq->properties[i].name);
		if (property->scconf_prop_key == NULL) {
			free(property);
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}
		property->scconf_prop_value =
		    strdup(dsproperty_seq->properties[i].value);
		if (property->scconf_prop_value == NULL) {
			if (property->scconf_prop_key != NULL)
				free(property->scconf_prop_key);
			free(property);
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}
		property->scconf_prop_next =
		    curr->scconf_ds_propertylist;
		curr->scconf_ds_propertylist = property;
	}

	/* Get the list of devices */
	curr->scconf_ds_devlist = NULL;
	if (dsgdev_seq->count > 0) {
		scconf_gdev_range_t *devrange = NULL;
		scconf_gdev_range_t *last = NULL;

		for (i = 0; i < dsgdev_seq->count; i++) {
			devrange = (scconf_gdev_range_t *)calloc(1,
			    sizeof (scconf_gdev_range_t));
			if (devrange == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}

			devrange->major_no = (dsgdev_seq->ranges)[i].maj;
			devrange->start_minor =
			    (dsgdev_seq->ranges)[i].start_gmin;
			devrange->end_minor = (dsgdev_seq->ranges)[i].end_gmin;
			devrange->scconf_gdev_next = NULL;

			if (last == NULL) {
				curr->scconf_ds_devlist = devrange;
			} else {
				last->scconf_gdev_next = devrange;
			}
			last = devrange;
		}
	}

	if ((ds_devicenames = (scconf_namelist_t *)
	    calloc(1, sizeof (scconf_namelist_t))) == NULL) {
		rstatus = SCCONF_ENOMEM;
		goto cleanup;
	}

	if (dsname) {
		if ((ds_devicenames->scconf_namelist_name =
		    strdup(dsname)) == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}
	}

	ds_devicenames->scconf_namelist_next = NULL;
	curr->scconf_ds_devvalues = ds_devicenames;

	(void) sprintf(searchpath, "%s/%s/", SCDG_SDS_DEVICE_PATH, dsname);

	if (!(options & SCCONF_CDEVICE_FLAG) &&
	    !(options & SCCONF_BDEVICE_FLAG)) {
		ds_cdevicenames = NULL;
		ds_bdevicenames = NULL;
	} else if ((options & SCCONF_CDEVICE_FLAG) &&
	    (options & SCCONF_BDEVICE_FLAG)) {
		(void) strcpy(bsearchpath, searchpath);
		(void) strcat(bsearchpath, "dsk");
		rstatus = ds_get_device_list(bsearchpath, options,
		    &ds_bdevicenames);

		(void) strcat(searchpath, "rdsk");
		rstatus = ds_get_device_list(searchpath, options,
		    &ds_cdevicenames);
	} else if (options & SCCONF_CDEVICE_FLAG) {
		(void) strcat(searchpath, "rdsk");
		rstatus = ds_get_device_list(searchpath, options,
		    &ds_cdevicenames);
		ds_bdevicenames = NULL;
	} else if (options & SCCONF_BDEVICE_FLAG) {
		(void) strcat(searchpath, "dsk");
		rstatus = ds_get_device_list(searchpath, options,
		    &ds_bdevicenames);
		ds_cdevicenames = NULL;
	}

	curr->scconf_ds_cdevicenames = ds_cdevicenames;
	curr->scconf_ds_bdevicenames = ds_bdevicenames;

cleanup:
	return (rstatus);
}

/*
 * Helper function to get from a list of nodes to a 'dc_replica_seq_t' structure
 */
static scconf_errno_t
ds_get_nodelist(char *dsnodes[], scconf_state_t dspreference,
    dc_replica_seq_t **dsnodes_seq)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	scconf_nodeid_t *dsnodeids;
	unsigned int i;
	dc_replica_seq_t *dc_nodes;

	ASSERT((dspreference == SCCONF_STATE_ENABLED) ||
	    (dspreference == SCCONF_STATE_DISABLED));

	*dsnodes_seq = (dc_replica_seq_t *)calloc(1, sizeof (dc_replica_seq_t));
	if (*dsnodes_seq == NULL) {
		rstatus = SCCONF_ENOMEM;
		goto done;
	}
	(*dsnodes_seq)->replicas = NULL;

	rstatus = ds_get_nodeids(dsnodes, &dsnodeids);
	if (rstatus != SCCONF_NOERR) {
		goto done;
	}

	/* Count the nodes */
	for (i = 0; dsnodeids[i] != 0; i++)
		;

	(*dsnodes_seq)->count = i;
	if (i == 0) {
		goto done;
	}

	dc_nodes = *dsnodes_seq;
	dc_nodes->replicas = (dc_replica_t *)calloc(i, sizeof (dc_replica_t));
	if (dc_nodes->replicas == NULL) {
		rstatus = SCCONF_ENOMEM;
		goto done;
	}

	for (i = 0; dsnodeids[i] != 0; i++) {
		(dc_nodes->replicas)[i].id = dsnodeids[i];
		if (dspreference == SCCONF_STATE_ENABLED) {
			(dc_nodes->replicas)[i].preference = i + 1;
		} else {
			(dc_nodes->replicas)[i].preference =
			    SCDG_MAX_NODE_PREFERENCE;
		}
	}

done:
	if ((rstatus != SCCONF_NOERR) && (*dsnodes_seq)) {
		if ((*dsnodes_seq)->replicas != NULL)
			free((*dsnodes_seq)->replicas);
		free(*dsnodes_seq);
		*dsnodes_seq = NULL;
	}
	return (rstatus);
}

/*
 * Helper function to free a 'dc_replica_seq_t' structure.
 */
static void
ds_free_nodelist(dc_replica_seq_t *dsnodes_seq)
{
	if (dsnodes_seq != NULL) {
		if (dsnodes_seq->replicas != NULL)
			free(dsnodes_seq->replicas);
		free(dsnodes_seq);
	}
}

/*
 * Helper function to get from 'scconf_gdev_range_t' to 'dc_gdev_range_seq_t'.
 */
static scconf_errno_t
ds_get_gdevlist(scconf_gdev_range_t *dsdevices,
    dc_gdev_range_seq_t **dsgdevs_range_seq)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	scconf_gdev_range_t *tmp;
	unsigned int i = 0;
	dc_gdev_range_seq_t *dc_gdevs = NULL;

	*dsgdevs_range_seq = (dc_gdev_range_seq_t *)calloc(1,
	    sizeof (dc_gdev_range_seq_t));
	if (*dsgdevs_range_seq == NULL) {
		rstatus = SCCONF_ENOMEM;
		goto done;
	}
	(*dsgdevs_range_seq)->ranges = NULL;

	for (i = 0, tmp = dsdevices; tmp; i++, tmp = tmp->scconf_gdev_next)
		/* Empty */;

	dc_gdevs = *dsgdevs_range_seq;
	dc_gdevs->count = i;
	if (i == 0) {
		goto done;
	}

	dc_gdevs->ranges = (dc_gdev_range_t *)calloc(i,
	    sizeof (dc_gdev_range_t));
	if (dc_gdevs->ranges == NULL) {
		rstatus = SCCONF_ENOMEM;
		goto done;
	}

	for (i = 0, tmp = dsdevices; i < dc_gdevs->count;
	    i++, tmp = tmp->scconf_gdev_next) {
		(dc_gdevs->ranges)[i].maj = tmp->major_no;
		(dc_gdevs->ranges)[i].start_gmin = tmp->start_minor;
		(dc_gdevs->ranges)[i].end_gmin = tmp->end_minor;
	}

done:
	if ((rstatus != SCCONF_NOERR) && (*dsgdevs_range_seq)) {
		if ((*dsgdevs_range_seq)->ranges != NULL)
			free((*dsgdevs_range_seq)->ranges);
		free(*dsgdevs_range_seq);
		*dsgdevs_range_seq = NULL;
	}
	return (rstatus);
}

/*
 * Helper function to free a 'dc_gdev_range_seq_t' structure.
 */
static void
ds_free_gdevlist(dc_gdev_range_seq_t *dsgdevs_range_seq)
{
	if (dsgdevs_range_seq != NULL) {
		if (dsgdevs_range_seq->ranges != NULL)
			free(dsgdevs_range_seq->ranges);
		free(dsgdevs_range_seq);
	}
}

/*
 * Helper function to get from a 'scconf_cfg_prop_t' structure to a
 * 'dc_property_seq_t' structure.
 */
static scconf_errno_t
ds_get_propertylist(scconf_cfg_prop_t *dsproperties,
    dc_property_seq_t **dsproperty_seq)
{
	unsigned int i;
	scconf_cfg_prop_t *tmp;

	/* Get a count of the properties */
	for (i = 0, tmp = dsproperties; tmp; i++, tmp = tmp->scconf_prop_next)
		;

	if (i == 0) {
		*dsproperty_seq = NULL;
		return (SCCONF_NOERR);
	}

	*dsproperty_seq = (dc_property_seq_t *)malloc(
		sizeof (dc_property_seq_t));
	if (*dsproperty_seq == NULL) {
		return (SCCONF_ENOMEM);
	}

	(*dsproperty_seq)->count = i;
	(*dsproperty_seq)->properties = (dc_property_t *)calloc(i,
	    sizeof (dc_property_t));
	if (((*dsproperty_seq)->properties) == NULL) {
		free(*dsproperty_seq);
		*dsproperty_seq = NULL;
		return (SCCONF_ENOMEM);
	}

	for (i = 0, tmp = dsproperties; i < ((*dsproperty_seq)->count);
	    i++, tmp = tmp->scconf_prop_next) {
		((*dsproperty_seq)->properties)[i].name =
		    strdup(tmp->scconf_prop_key);
		((*dsproperty_seq)->properties)[i].value =
		    strdup(tmp->scconf_prop_value);
		if ((((*dsproperty_seq)->properties)[i].name == NULL) ||
		    (((*dsproperty_seq)->properties)[i].value == NULL)) {
			unsigned int j;
			if (((*dsproperty_seq)->properties)[i].name != NULL)
				free(((*dsproperty_seq)->properties)[i].name);
			for (j = 0; j < i; j++) {
				if (((*dsproperty_seq)->properties)[j].name
				    != NULL)
					free(((*dsproperty_seq)->\
					    properties)[j].name);
				if (((*dsproperty_seq)->properties)[j].value
				    != NULL)
					free(((*dsproperty_seq)->\
					    properties)[j].value);
			}
			free((*dsproperty_seq)->properties);
			free(*dsproperty_seq);
			*dsproperty_seq = NULL;
			return (SCCONF_ENOMEM);
		}
	}

	return (SCCONF_NOERR);
}

/*
 * Helper function to free a 'dc_property_seq_t' structure.
 */
static void
ds_free_propertylist(dc_property_seq_t *dsproperty_seq)
{
	if (dsproperty_seq != NULL) {
		if (dsproperty_seq->properties != NULL)
			free(dsproperty_seq->properties);
		free(dsproperty_seq);
	}
}

/*
 * Helper function to get all the property names from a 'scconf_cfg_prop_t'
 * structure to a 'dc_string_seq_t' structure.
 */
static scconf_errno_t
ds_get_propertynames(scconf_cfg_prop_t *dsproperties,
    dc_string_seq_t **dsproperty_seq)
{
	unsigned int i;
	scconf_cfg_prop_t *tmp;

	/* Get a count of the properties */
	for (i = 0, tmp = dsproperties; tmp; i++, tmp = tmp->scconf_prop_next)
		;

	*dsproperty_seq = (dc_string_seq_t *)malloc(sizeof (dc_string_seq_t));
	if (*dsproperty_seq == NULL) {
		return (SCCONF_ENOMEM);
	}

	(*dsproperty_seq)->count = i;
	(*dsproperty_seq)->strings = (char **)calloc(i, sizeof (char *));
	if (((*dsproperty_seq)->strings) == NULL) {
		free(*dsproperty_seq);
		*dsproperty_seq = NULL;
		return (SCCONF_ENOMEM);
	}

	for (i = 0, tmp = dsproperties; i < ((*dsproperty_seq)->count);
	    i++, tmp = tmp->scconf_prop_next) {
		((*dsproperty_seq)->strings)[i] = strdup(tmp->scconf_prop_key);
		if (((*dsproperty_seq)->strings)[i] == NULL) {
			unsigned int j;
			for (j = 0; j < i; j++) {
				if (((*dsproperty_seq)->strings)[j] != NULL)
					free(((*dsproperty_seq)->strings)[j]);
			}
			free((*dsproperty_seq)->strings);
			free(*dsproperty_seq);
			*dsproperty_seq = NULL;
			return (SCCONF_ENOMEM);
		}
	}

	return (SCCONF_NOERR);
}

/*
 * Helper function to free a 'dc_string_seq_t' structure.
 */
static void
ds_free_string_seq(dc_string_seq_t *dsstring_seq)
{
	if (dsstring_seq != NULL) {
		int i;
		if (dsstring_seq->strings != NULL) {
			for (i = 0; i < (int)dsstring_seq->count; i++) {
				if ((dsstring_seq->strings)[i] != NULL)
					free((dsstring_seq->strings)[i]);
			}
			free(dsstring_seq->strings);
		}
		free(dsstring_seq);
	}
}

/*
 * Given a list of nodenames, convert them to nodeids.
 */
static scconf_errno_t
ds_get_nodeids(char *dsnodes[], scconf_nodeid_t **dsnodeids)
{
	int i = 0, j;
	scconf_errno_t err = SCCONF_NOERR;

	if (dsnodes != NULL) {
		for (i = 0; dsnodes[i] != NULL; i++)
			;
	}

	*dsnodeids = (scconf_nodeid_t *)calloc((size_t)i+1,
	    sizeof (scconf_nodeid_t));
	if (*dsnodeids == NULL) {
		return (SCCONF_ENOMEM);
	}

	(*dsnodeids)[i] = 0;

	for (j = 0; j < i; j++) {
		err = scconf_get_nodeid(dsnodes[j], &((*dsnodeids)[j]));
		if (err != SCCONF_NOERR) {
			free(*dsnodeids);
			return (err);
		}
	}

	return (err);
}

/*
 * ds_get_device_list
 *
 * Upon success a list of all the raw devices are returned in ds_devicenames.
 * The caller is responsible for freeing the memory for ds_devicenames.
 *
 * Possible return values:
 *
 *      SCCONF_NOERR            - success
 *      SCCONF_ENOMEM           - not enough memory
 *      SCCONF_ENOEXIST         - the nodeid is not found
 *      SCCONF_EUNEXPECTED      - unexpected error
 */

static scconf_errno_t
ds_get_device_list(char *searchpath, const uint_t iflag,
    scconf_namelist_t **ds_devicenames)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	int error = 0;
	DIR *dirf;
	struct dirent *entp = NULL;
	struct dirent *retp;
	struct stat file_stat;
	scconf_namelist_t *ds_devicename = NULL;
	scconf_namelist_t *current_ptr = NULL;
	char dev_searchpath[MAXPATHLEN];

	if ((dirf = opendir(searchpath)) == NULL) {
		return (SCCONF_EUNEXPECTED);
	}

	entp = (struct dirent *)calloc((size_t)1,
	    MAXPATHLEN + 1 + sizeof (struct  dirent));

	while ((error = readdir_r(dirf, entp, &retp)) == 0) {
		if (retp == NULL) {
			break;
		}
		if (strcmp(entp->d_name, ".") == 0 ||
		    strcmp(entp->d_name, "..") == 0) {
			continue;
		}
		bzero(dev_searchpath, sizeof (dev_searchpath));
		(void) sprintf(dev_searchpath, "%s/%s", searchpath,
		    entp->d_name);

		error = stat(searchpath, &file_stat);
		if ((error != 0) && (iflag & SCCONF_NODEVICE_ERROR_FLAG))  {
			rstatus = SCCONF_ENOEXIST;
			goto cleanup;
		}

		if ((ds_devicename = (scconf_namelist_t *)
		    calloc(1, sizeof (scconf_namelist_t))) == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		if ((ds_devicename->scconf_namelist_name =
		    strdup(dev_searchpath)) == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		if (current_ptr != NULL) {
			ds_devicename->scconf_namelist_next = current_ptr;
		} else {
			ds_devicename->scconf_namelist_next = NULL;
		}
		current_ptr = ds_devicename;
	}

	*ds_devicenames = current_ptr;

cleanup:
	if (entp)
		free(entp);
	(void) closedir(dirf);
	return (rstatus);
}

/*
 * ds_get_failback_value
 *
 * Get the appropritae failback value for DCS from the user
 * specified failback value.
 *
 * Possible return values: NONE
 *
 */
static void
ds_get_failback_value(scconf_state_t dsfailback, int *failback_value,
    int enable_failback)
{
	switch (dsfailback) {

	case SCCONF_STATE_UNCHANGED:
		*failback_value =  enable_failback;
		break;

	case SCCONF_STATE_ENABLED:
		*failback_value = 1;
		break;

	case SCCONF_STATE_DISABLED:
		*failback_value = 0;
		break;

	default:
		break;
	}
}

/*
 * ds_change_service_parameters
 *
 * Change configuration for a existing device group.
 *
 * Possible return values:
 *
 *	DCS_SUCCESS
 *	DCS_ERR_NOT_CLUSTER
 *	DCS_ERR_SERVICE_NAME
 *	DCS_ERR_DEVICE_INVAL
 *	DCS_ERR_DEVICE_BUSY
 *	DCS_ERR_CCR_ACCESS
 *
 */
static dc_error_t
ds_change_service_parameters(char *service_name, int failback_enabled,
    dc_replica_seq_t *nodes_seq, dc_property_seq_t *property_seq,
    unsigned int dsnumsecondaries)
{
	dc_error_t dc_err = DCS_SUCCESS;

	/* Set the failback mode */
	dc_err = dcs_set_failback_mode(service_name, failback_enabled);
	if (dc_err != DCS_SUCCESS) {
		return (dc_err);
	}

	/* Set the desired number of secondaries */
	if (dsnumsecondaries != SCCONF_NUMSECONDARIES_UNSET) {
		dc_err = dcs_set_active_secondaries(service_name,
		    dsnumsecondaries);

		if (dc_err != DCS_SUCCESS) {
			return (dc_err);
		}
	}

	if (nodes_seq != (dc_replica_seq_t *)0) {
		dc_err = ds_add_nodes(service_name, nodes_seq);
		if (dc_err != DCS_SUCCESS) {
			return (dc_err);
		}
	}

	if (property_seq != NULL) {
		dc_err = dcs_set_properties(service_name, property_seq);
		if (dc_err != DCS_SUCCESS) {
			return (dc_err);
		}
	}

	return (DCS_SUCCESS);
}

static dc_error_t
ds_remove_service_parameters(char *service_name, scconf_nodeid_t *dsnodeids,
    dc_string_seq_t *property_seq)
{
	dc_error_t dc_err = DCS_SUCCESS;
	unsigned int i;

	/* Are we being asked to remove the service? */
	if ((dsnodeids[0] == 0) && (property_seq->count == 0)) {
		dc_err = dcs_remove_service(service_name);
		return (dc_err);
	}

	for (i = 0; dsnodeids[i] != 0; i++) {
		dc_err = dcs_remove_node(service_name, dsnodeids[i]);
		if (dc_err != DCS_SUCCESS) {
			return (dc_err);
		}
	}

	if (property_seq->count != 0) {
		dc_err = dcs_remove_properties(service_name, property_seq);
		if (dc_err != DCS_SUCCESS) {
			return (dc_err);
		}
	}

	return (DCS_SUCCESS);
}

/*
 * ds_add_nodes
 *
 * Add nodes to the existing device group.
 *
 * Possible return values:
 *
 *	DCS_SUCCESS
 *	DCS_ERR_NOT_CLUSTER
 *	DCS_ERR_SERVICE_NAME
 *	DCS_ERR_DEVICE_INVAL
 *	DCS_ERR_DEVICE_BUSY
 *	DCS_ERR_CCR_ACCESS
 *
 */
static dc_error_t
ds_add_nodes(char *service_name, dc_replica_seq_t *nodes_seq)
{
	dc_error_t dc_err = DCS_SUCCESS;
	int i = 0;

	for (i = 0; i < (int)nodes_seq->count; i++) {
		if ((dc_err = dcs_add_node(service_name,
		    &nodes_seq->replicas[i])) != DCS_SUCCESS) {
			return (dc_err);
		}
	}

	if ((dc_err = dcs_change_node_priority(service_name, nodes_seq)) !=
	    DCS_SUCCESS) {
		return (dc_err);
	} else {
		return (DCS_SUCCESS);
	}
}


/*
 * ds_get_num_of_nodes
 *
 * Get the number of nodes specified by the user.
 *
 * Possible return values: NONE
 *
 */
static int
ds_get_num_of_nodes(scconf_nodeid_t *dsnodeids)
{
	int count = 0;

	while (*dsnodeids++)
		count++;

	return (count);
}

/*
 * ds_set_node_seq for change option of scconf
 *
 * Set the node id and preference.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR            - success
 *      SCCONF_ENOMEM           - not enough memory
 *      SCCONF_EINVAL           - invalid argument
 */
static scconf_errno_t
ds_set_node_seq(scconf_nodeid_t *dsnodeids, dc_replica_seq_t *dsnodes_seq,
    dc_replica_seq_t **nodes_seq, scconf_state_t nodepreference, int num_nodes)
{

	int i = 0;
	int j = 0;
	scconf_errno_t rstatus = SCCONF_NOERR;
	dc_replica_t *repls = (dc_replica_t *)0;
	boolean_t found = B_FALSE;
	scconf_nodeid_t *allnodes = NULL;
	int curr;

	if (dsnodeids == NULL)
		return (SCCONF_EINVAL);

	/* Check if all the existing nodes are specified */
	if (dsnodes_seq != NULL) {
		if ((num_nodes != (int)dsnodes_seq->count) &&
		    (nodepreference == SCCONF_STATE_ENABLED)) {
			return (SCCONF_EINVAL);
		}
	}

	/* Allocate nodes */
	repls = (dc_replica_t *)calloc(
	    dsnodes_seq->count + (uint_t)num_nodes,
	    sizeof (dc_replica_t));
	allnodes = (scconf_nodeid_t *)calloc(
	    dsnodes_seq->count + (uint_t)num_nodes + 1,
	    sizeof (scconf_nodeid_t));

	if ((allnodes == NULL) ||(repls == NULL)) {
		rstatus = SCCONF_ENOMEM;
		goto out;
	}

	for (j = 0; j < num_nodes; j++) {
		allnodes[j] = dsnodeids[j];
	}

	curr = num_nodes;
	for (i = 0; i < (int)dsnodes_seq->count; i++) {
		found = B_FALSE;
		for (j = 0; j < num_nodes; j++) {
			if ((dsnodes_seq->replicas)[i].id == allnodes[j]) {
				found = B_TRUE;
				break;
			}
		}
		if (found == B_FALSE) {
			if (nodepreference == SCCONF_STATE_ENABLED) {
				rstatus = SCCONF_EINVAL;
				goto out;
			}
			allnodes[curr++] = (dsnodes_seq->replicas)[i].id;
		}
	}
	allnodes[curr++] = 0;

	/* If no preference is specified */
	if (nodepreference == SCCONF_STATE_UNCHANGED) {
		if (dsnodes_seq->count == 0) {
			nodepreference = SCCONF_STATE_DISABLED;
		} else if (dsnodes_seq->replicas[0].preference
		    == SCDG_MAX_NODE_PREFERENCE) {
			nodepreference = SCCONF_STATE_DISABLED;
		} else {
			nodepreference = SCCONF_STATE_ENABLED;
		}
	}

	/* Set the node preference and id's */
	rstatus = ds_set_nodes_preference(allnodes, &repls, nodepreference);
	if (rstatus != SCCONF_NOERR)
		goto out;

	if (((*nodes_seq) = (dc_replica_seq_t *)
	    calloc(1, sizeof (dc_replica_seq_t))) == NULL)
		return (SCCONF_ENOMEM);
	(*nodes_seq)->replicas = repls;
	(*nodes_seq)->count = (uint_t)curr-1;

	if (allnodes != NULL)
		free(allnodes);
	return (0);

out:
	if (allnodes != NULL)
		free(allnodes);
	if (repls != NULL)
		free(repls);
	return (rstatus);
}

/*
 * ds_set_nodes_preference
 *
 * Set the node id and node preference for change option of scconf.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 */
static scconf_errno_t
ds_set_nodes_preference(scconf_nodeid_t *dsnodeids, dc_replica_t **nodes,
    scconf_state_t nodepreference)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	uint_t i = 0;

	for (i = 0; dsnodeids[i] != 0; i++) {
		(*nodes)[i].id = (nodeid_t)(dsnodeids[i]);
		if (nodepreference == SCCONF_STATE_ENABLED) {
			(*nodes)[i].preference = i + 1;
		} else {
			(*nodes)[i].preference = SCDG_MAX_NODE_PREFERENCE;
		}
	}

	return (rstatus);
}

/*
 * ds_free_nodes_seq
 * free all  memory for a "dc_replica_seq_t" structure.
 */
static void
ds_free_nodes_seq(dc_replica_seq_t *nodes_seq)
{
	if (nodes_seq == NULL)
		return;

	if (nodes_seq->replicas != NULL)
		free(nodes_seq->replicas);

	free(nodes_seq);
}

/*
 * ds_dcs_create_service_to_scconf_err_status
 *
 * Convert dcs_create_service error codes to scconf error codes.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EEXIST		- device group already exists
 *	SCCONF_ENOEXIST		- an object does not exist
 *	SCCONF_EINUSE		- an object is already in use
 *	SCCONF_EBUSY		- busy state, try again later
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EUNKNOWN		- unknown type
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
static scconf_errno_t
ds_dcs_create_service_to_scconf_err_status(dc_error_t dc_err)
{
	if (dc_err == DCS_ERR_SERVICE_NAME)
		return (SCCONF_EEXIST);
	else
		return (ds_dcs_to_scconf_err_status(dc_err));
}

/*
 * ds_dcs_to_scconf_err_status
 *
 * Convert dcs error codes to scconf error codes.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR            - success
 *      SCCONF_ENOEXIST         - an object does not exist
 *	SCCONF_EINUSE		- an object is already in use
 *	SCCONF_EBUSY		- busy state, try again later
 *      SCCONF_EINVAL           - invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EUNKNOWN		- unknown type
 *      SCCONF_EUNEXPECTED      - internal or unexpected error
 *      SCCONF_DS_ENODEINVAL    - Node not in cluster
 *      SCCONF_EIO              - (Comm) I/O error
 */
static scconf_errno_t
ds_dcs_to_scconf_err_status(dc_error_t dc_err)
{
	scconf_errno_t rstatus = SCCONF_NOERR;

	switch (dc_err) {
	case DCS_SUCCESS:
		rstatus = SCCONF_NOERR;
		break;

	case DCS_ERR_SERVICE_NAME:
		rstatus = SCCONF_ENOEXIST;
		break;

	case DCS_ERR_SERVICE_CLASS_NAME:
		rstatus = SCCONF_EUNKNOWN;
		break;

	case DCS_ERR_DEVICE_INVAL:
	case DCS_ERR_SERVICE_INACTIVE:
		rstatus = SCCONF_ENOEXIST;
		break;

	case DCS_ERR_DEVICE_BUSY:
	case DCS_ERR_SERVICE_CLASS_BUSY:
	case DCS_ERR_NODE_BUSY:
		rstatus = SCCONF_EINUSE;
		break;

	case DCS_ERR_SERVICE_BUSY:
	case DCS_ERR_INVALID_STATE:
		rstatus = SCCONF_EBUSY;
		break;

	case DCS_ERR_NOMEM:
		rstatus = SCCONF_ENOMEM;
		break;

	case DCS_ERR_NOT_CLUSTER:
		rstatus = SCCONF_DS_ENODEINVAL;
		break;

	case DCS_ERR_COMM:
		rstatus = SCCONF_EIO;
		break;

	default:
		rstatus = SCCONF_EUNEXPECTED;
		break;
	}
	return (rstatus);
}
