/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SCDS_SDS_H
#define	_SCDS_SDS_H

#pragma ident	"@(#)scds_sds.h	1.13	08/05/20 SMI"

/*
 * This is the header file for scds_sds.so.
 */

#ifdef __cplusplus
extern "C" {
#endif


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include <sys/ddi.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <sys/types.h>

#include <dirent.h>
#include <libintl.h>
#include <dc/libdcs/libdcs.h>
#include <scadmin/scconf.h>

/*
 * Add a SDS device service.
 *
 * The "dsnodes" argument is a NULL terminated array of character
 * pointers each pointing to a nodename.  The order in which the nodes
 * are given establishes preference.  Some device types provide a method for
 * defaulting the nodes preference list, allowing "dsnodes" to be
 * NULL.
 *
 * If dsfailback is set to SCCONF_STATE_UNCHANGED, the default is
 * to disable failback.
 *
 * This function will work for both local and remote invocation.
 * For SDS this function always calls the local function.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EEXIST		- device group already exists
 *	SCCONF_ENOEXIST		- diskset or node is not found
 *	SCCONF_EINUSE		- the diskset is already in use
 *	SCCONF_EBUSY		- busy, try again later
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EUNKNOWN		- unknown type
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_DS_EINVAL	- inconsistencies detected in device
 *				  configuration
 */
extern scconf_errno_t scds_add_ds(char *dsname, char *dsnodes[],
    scconf_state_t dspreference, scconf_gdev_range_t *dsdevices,
    scconf_state_t dsfailback, scconf_cfg_prop_t *dspropertylist,
    char *dsoptions, unsigned int dsnumsecondaries, char **messages);

/*
 * Add a SDS device service (local only).
 *
 * The "dsnodes" argument is a NULL terminated array of character
 * pointers each pointing to a nodename.  The order in which the nodes
 * are given establishes preference.  Some device types provide a method for
 * defaulting the nodes preference list, allowing "dsnodes" to be
 * NULL.
 *
 * If dsfailback is set to SCCONF_STATE_UNCHANGED, the default is
 * to disable failback.
 *
 *  This "local" function will only operate on the local node.  In cases
 *  where RPC is needed to send the request to the node where the disk group
 *  is imported, the non-local function should be used.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EEXIST		- device group already exists
 *	SCCONF_ENOEXIST		- diskset or node is not found
 *	SCCONF_EINUSE		- the diskset is already in use
 *	SCCONF_EBUSY		- busy, try again later
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EUNKNOWN		- unknown type
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
extern scconf_errno_t scds_add_ds_local(char *dsname, char *dsnodes[],
    scconf_state_t dspreference, scconf_gdev_range_t *dsdevices,
    scconf_state_t dsfailback, scconf_cfg_prop_t *dspropertylist,
    char *dsoptions, unsigned int dsnumsecondaries, char **messages);

/*
 * Change a SDS device service.
 *
 * The "dsnodes" argument is a NULL terminated array of character
 * pointers each pointing to a nodename.  The order in which the nodes
 * are given establishes preference.  Some device types provide a method for
 * defaulting the nodes preference list, allowing "dsnodes" to be
 * NULL.
 *
 * This function will work for both local and remote invocation.
 * For SDS this function always calls the local function.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOEXIST		- device group or node is not found
 *	SCCONF_EINUSE		- an object is already in use
 *	SCCONF_EBUSY		- busy state, try again later
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EUNKNOWN		- unknown type
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_DS_EINVAL	- inconsistencies detected in device
 *				  configuration
 */
extern scconf_errno_t scds_change_ds(char *dsname, char *dsnodes[],
    scconf_state_t dspreference, scconf_gdev_range_t *dsdevices,
    scconf_state_t dsfailback, scconf_cfg_prop_t *dspropertylist,
    char *dsoptions, unsigned int dsnumsecondaries, char **messages);

/*
 * Change a SDS device service (local only).
 *
 * The "dsnodes" argument is a NULL terminated array of character
 * pointers each pointing to a nodename.  The order in which the nodes
 * are given establishes preference.  Some device types provide a method for
 * defaulting the nodes preference list, allowing "dsnodes" to be
 * NULL.
 *
 *  This "local" function will only operate on the local node.  In cases
 *  where RPC is needed to send the request to the node where the disk group
 *  is imported, the non-local function should be used.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOEXIST		- device group or node is not found
 *	SCCONF_EINUSE		- an object is already in use
 *	SCCONF_EBUSY		- busy state, try again later
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EUNKNOWN		- unknown type
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
extern scconf_errno_t scds_change_ds_local(char *dsname, char *dsnodes[],
    scconf_state_t dspreference, scconf_gdev_range_t *dsdevices,
    scconf_state_t dsfailback, scconf_cfg_prop_t *dspropertylist,
    char *dsoptions, unsigned int dsnumsecondaries, char **messages);

/*
 * Remove a SDS device service.
 *
 * The "dsnodes" argument is a NULL terminated array of character
 * pointers each pointing to a nodename.  The order in which the nodes
 * are given establishes preference.  Some device types provide a method for
 * defaulting the nodes preference list, allowing "dsnodes" to be
 * NULL.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOEXIST		- device group or node is not found
 *	SCCONF_EINUSE		- an object is already in use
 *	SCCONF_EBUSY		- busy state, try again later
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EUNKNOWN		- unknown type
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
extern scconf_errno_t scds_remove_ds(char *dsname, char *dsnodes[],
    scconf_gdev_range_t *dsdevices, scconf_cfg_prop_t *dspropertylist,
    char *dsoptions, char **messages);

/*
 * get device service type.
 *
 * The "service_class_name" argument is a NULL terminated array.
 * This function checks to see if the right device group type
 * .so are being called.
 *
 * Possible return values:
 *
 *      less than or greater then zero          - error
 *      equal to zero                           - success
 */
extern boolean_t scds_get_ds_type(char *, char *);

/*
 * get device group configuration.
 *
 * Upon success a list of all the sds device group parameters are
 * returned in dsconfigp.
 * The caller is responsible for freeing the memory for dsconfigp.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOEXIST		- the nodeid is not found
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
extern scconf_errno_t scds_get_ds_config(char *dsname, const uint_t iflag,
    scconf_cfg_ds_t **dsconfigp);

#ifdef __cplusplus
}
#endif

#endif /* _SCDS_SDS_H */
