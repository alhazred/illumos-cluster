/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)sczones.c	1.14	08/07/21 SMI"

/*
 * This file provides the complete implementation of the libsczones
 * library, with which sun cluster components can link in order to
 * register for and receive zone state change events.
 */

/* SC includes */
#include <rgm/sczones.h>

/* system includes */
#include <sys/sol_version.h>

#if SOL_VERSION >= __s10
/* S10 (and later)-only includes */
#include <rgm/sczones_common.h>
#include <sys/rsrc_tag.h>
#include <rgm/rgm_msg.h>
#include <libzonecfg.h>

#include <unistd.h>
#include <door.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <zone.h>
#include <libintl.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/sc_syslog_msg.h>

#define	NUM_CB_PTRS 5

#include <syslog.h>
/*
 * for pre s10, we have only zonescheck functionality implemented to support
 * these checks on all platform; else all other functions make sense only on
 * post-s10 versions
 */

static zone_state_callback_t cb_ptrs[NUM_CB_PTRS] = {
    NULL, NULL, NULL, NULL, NULL};

static int myfd;


/* non verbose zonescheck */
int
sc_nv_zonescheck() {
	zoneid_t zoneid;
	if ((zoneid = getzoneid()) < 0) {
		return (1);
	}
	if (zoneid != 0) {
		return (1);
	}
	return (0);
}

int
sc_zonescheck() {

	zoneid_t zoneid;

	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	if ((zoneid = getzoneid()) < 0) {
		(void) fprintf(stderr, dgettext(TEXT_DOMAIN,
		    "getzoneid() failed. Unable to verify privilege.\n"));
		return (1);
	}
	if (zoneid != 0) {
		(void) fprintf(stderr, dgettext(TEXT_DOMAIN,
		    "You cannot run this command from a non-global zone\n"));
		return (1);
	}
	return (0);
}

boolean_t
sc_zone_configured_on_cur_node(const char *zname) {

#if SOL_VERSION < __s10
	/*
	 * No zones in S9 and below
	 */
	return (B_FALSE);
#else

	boolean_t zone_exists = B_FALSE;
	zone_dochandle_t tmp_handle;
	int err;

	if (zname == NULL) {
		/*
		 * Invalid zone name
		 */
		return (B_FALSE);
	}

	if (strcmp(zname, "global") == 0) {
		/*
		 * global zone exists always.
		 * otherwise we cant call this method
		 */
		return (B_TRUE);
	}

	/*
	 * If we are here, we have to check whether
	 * config information is available for this zone
	 */

	if ((tmp_handle = zonecfg_init_handle()) == NULL) {
		/*
		 * Internal error.
		 */
		return (zone_exists);
	}

	err = zonecfg_get_handle(zname, tmp_handle);
	zonecfg_fini_handle(tmp_handle);

	if (err == Z_NO_ZONE) {
		/*
		 * Zone does not exist.
		 */
		zone_exists = B_FALSE;
	} else if (err == Z_OK) {
		/*
		 * Zone exists.
		 */
		zone_exists = B_TRUE;
	}

	return (zone_exists);
#endif
}

/*
 * The format of the data pointed to by dataptr is:
 * |callback_code|zonename|
 * The callback_code is always sizeof(sc_zone_state_t) bytes. The zonename
 * is assumed to be NULL-terminated and to be the rest of the data.
 */
static void
/* ARGSUSED */
door_callback(void *cookie, char *dataptr, size_t datasize,
    door_desc_t *descptr, uint_t ndesc)
{
	sc_syslog_msg_handle_t handle;
	sc_zone_state_t callback_code;
	char *zonename;

	/* Check the size (mostly to be sure it's not 0) */
	if (datasize < sizeof (sc_zone_state_t) + 1) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The zone state change callback from the sc_zonesd daemon
		 * was improperly formatted. The callback will be ignored.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes. If
		 * the problem persists, contact your authorized Sun service
		 * provider for assistance in diagnosing the problem.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("door_callback: invalid argument size"));
		sc_syslog_msg_done(&handle);

		/* nothing much we can do if it fails */
		(void) door_return(NULL, (size_t)0, NULL, (uint_t)0);
	}

	/*
	 * Save temp callback code for easy access and sanity check
	 * its value.
	 */
	callback_code = *(sc_zone_state_t *)dataptr;

	/*
	 * suppres lint error. We want to check for negative value
	 * in case there's some weird value sent from the sc_zonesd.
	 */
	if (callback_code > (NUM_CB_PTRS - 1) ||
	    callback_code < 0) { /*lint !e568 */
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The zone state change callback from the sc_zonesd was
		 * improperly formatted. The callback will be ignored.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes. If
		 * the problem persists, contact your authorized Sun service
		 * provider for assistance in diagnosing the problem.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("door_callback: invalid argument"));
		sc_syslog_msg_done(&handle);
		/* nothing much we can do if it fails */
		(void) door_return(NULL, (size_t)0, NULL, 0);
	}
	/* get the ptr to the zonename */
	zonename = dataptr + sizeof (sc_zone_state_t);

	/*
	 * Make the actual callback to the client.
	 * The trick here is that the callback_code can be used
	 * directly to index the array of callback ptrs, because they're
	 * stored in the locations matching the order of the callback
	 * codes.
	 *
	 * Check for NULL before deferencing the function ptr in
	 * case the client didn't register for this state transition.
	 */
	/*
	 * suppress lint error: we know callback_code is in range
	 * because we checked it above
	 */
	if (cb_ptrs[callback_code]) { /*lint !e661 */
		cb_ptrs[callback_code](zonename); /*lint !e661 */
	}

	(void) door_return(NULL, (size_t)0, NULL, 0);
}

/*
 * Creates a door for the callbacks.
 * Sends the descriptor for that door to the server, via
 * the server door (at the well-known location).
 */
int
register_zone_state_callbacks(zone_state_callback_t callback_ptrs[],
    int num_callback_ptrs)
{
	int serverfd, i;
	sc_syslog_msg_handle_t handle;
	door_arg_t arg;
	door_desc_t door_desc;
	int res, err;
	int data;

	/*
	 * save the ptrs before we make the door call, in case we start
	 * receiving callbacks right away.
	 */
	for (i = 0; i < NUM_CB_PTRS && i < num_callback_ptrs; i++) {
		cb_ptrs[i] = callback_ptrs[i];
	}

	/* create the door for the server to call us on */
	if ((myfd = door_create(door_callback, NULL, 0)) == -1) {
		err = errno; /*lint !e746 */
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("door_create: %s"), strerror(err));
		sc_syslog_msg_done(&handle);

		return (-1);
	}

	/* open the server door */
	if ((serverfd = open(SCZONES_LIB_SERVER_PATH, O_RDWR)) == -1) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("open: %s"), strerror(err));
		sc_syslog_msg_done(&handle);

		return (-1);
	}

	/* set up the arguments to the door call. */
	door_desc.d_attributes = DOOR_DESCRIPTOR;
	door_desc.d_data.d_desc.d_descriptor = myfd;

	/* 1 means "register" */
	data = 1;
	arg.data_ptr = (char *)&data;
	arg.data_size = sizeof (int);
	arg.desc_ptr = &door_desc;
	arg.desc_num = 1;
	arg.rbuf = (char *)&res;
	arg.rsize = sizeof (int);


	err = do_door_call(serverfd, &arg);
	if (err != 0) {
		return (-1);
	}

	/* Check the return code passed back from the server */
	if (res != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The zone state change registration with sc_zonesd failed
		 * for an unknown reason.
		 * @user_action
		 * Search for messages from sc_zonesd to determine the source
		 * of the error. Save a copy of the /var/adm/messages files on
		 * all nodes. If the problem persists, contact your authorized
		 * Sun service provider for assistance in diagnosing the
		 * problem.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("Unknown error on door server: %s"), res);
		sc_syslog_msg_done(&handle);

		return (-1);
	}

	(void) close(serverfd);

	/* leave myfd open */

	return (0);
}

int
unregister_zone_state_callbacks(void)
{
	int serverfd;
	sc_syslog_msg_handle_t handle;
	door_arg_t arg;
	int res, err;
	int data;

	/* open the server door */
	if ((serverfd = open(SCZONES_LIB_SERVER_PATH, O_RDWR)) == -1) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("open: %s"), strerror(err));
		sc_syslog_msg_done(&handle);

		return (-1);
	}

	/* 0 means "unregister" */
	data = 0;
	arg.data_ptr = (char *)&data;
	arg.data_size = sizeof (int);
	arg.desc_ptr = NULL;
	arg.desc_num = 0;
	arg.rbuf = (char *)&res;
	arg.rsize = sizeof (int);

	err = do_door_call(serverfd, &arg);
	if (err != 0) {
		return (-1);
	}

	/* Check the return code passed back from the server */
	if (res != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("Unknown error on door server: %s"), res);
		sc_syslog_msg_done(&handle);

		return (-1);
	}

	(void) close(serverfd);
	(void) close(myfd);

	return (0);
}

int
do_door_call(int serverfd, door_arg_t *arg)
{
	sc_syslog_msg_handle_t handle;
	sigset_t sig_set, orig_set;
	int err, i;

	/* block all signals during the door call */
	if (sigfillset(&sig_set) != 0) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sigfillset: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(err);
	}

	if ((err = pthread_sigmask(SIG_SETMASK, &sig_set, &orig_set)) != 0) {
		char *err_str = strerror(err);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pthread_sigmask: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		exit(err);
	}


	/*
	 * We retry at most twice for EAGAIN or EINTR before giving up.
	 */
	for (i = 0; i < 3; i++) {
		if (door_call(serverfd, arg) != 0) {
			err = errno;
			if (i < 2 && (err == EAGAIN || err == EINTR)) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGMPMF_TAG, "");
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, SYSTEXT(
				    "door_call: %s; will retry"),
				    strerror(err));
				sc_syslog_msg_done(&handle);

				/* sleep, then retry */
				(void) sleep((uint_t)i + 1);
			} else {

				/* give up */

				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGMPMF_TAG, "");
				/*
				 * SCMSGS
				 * @explanation
				 * The door_call failed with the specified
				 * reason. The libsczones was unable to
				 * register with the sc_zonesd for zone state
				 * change callbacks.
				 * @user_action
				 * Search for messages from sc_zonesd to
				 * determine the source of the error. Save a
				 * copy of the /var/adm/messages files on all
				 * nodes. If the problem persists, contact
				 * your authorized Sun service provider for
				 * assistance in diagnosing the problem.
				 */
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    SYSTEXT("door_call: %s"), strerror(err));
				sc_syslog_msg_done(&handle);

				return (-1);
			}
		} else {
			/* successful call */
			break;
		}
	}

	/* restore the original signal mask */
	if ((err = pthread_sigmask(SIG_SETMASK, &orig_set, NULL)) != 0) {
		char *err_str = strerror(err);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pthread_sigmask: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		exit(err);
	}

	return (0);
}

boolean_t
xdr_zoneup_args(register XDR *xdrs, zoneup_args_t *objp)
{
	if (!xdr_int(xdrs, (int *)&objp->pid))
		return (B_FALSE);
	if (!xdr_opaque(xdrs, (char *)&objp->parms, sizeof (pcparms_t)))
		return (B_FALSE);
	if (!xdr_string(xdrs, &objp->zonename, ~0))
		return (B_FALSE);
	return (B_TRUE);
}

#else

int
sc_zonescheck() {
	return (0);
}

int
sc_nv_zonescheck() {
	return (0);
}

#endif
