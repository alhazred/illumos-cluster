/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scpriority.c	1.8	08/08/01 SMI"


#include <sys/cladm_int.h>
#include <sys/cladm.h>
#include <sys/cl_rgm.h>
#include <door.h>
#include <sys/types.h>
#include <sys/os.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <locale.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <pthread.h>
#include <synch.h>
#include <sys/sc_syslog_msg.h>
#include <limits.h>
/* Used to define __s9, __s10, etc. */
#include <sys/sol_version.h>
#include<rgm/sczones_common.h>
#include <rgm/security.h>
#include <sys/priocntl.h>
#include <sys/rtpriocntl.h>
#include <sys/tspriocntl.h>
#include <sys/cl_assert.h>

#if SOL_VERSION >= __s10
#include <zone.h>
#endif

static int proxy_priocntl(pid_t pid, pcparms_t *parms);

/* useful for setting priority from sun cluster infrastructure */
int
sc_priocntl(pid_t pid, pcparms_t *schedparms) {
	sc_syslog_msg_handle_t handle;
	int error;
#if SOL_VERSION >= __s10
	if (getzoneid() == GLOBAL_ZONEID) {
		error = priocntl(P_PID, pid, PC_SETPARMS, (caddr_t)schedparms);
		if (error != 0) {
			error = errno;
		}
	} else {
		if (pid == P_MYID)
			pid = getpid();
		error = proxy_priocntl(pid, schedparms);
	}
#else
	error = priocntl(P_PID, pid, PC_SETPARMS, (caddr_t)schedparms);
	if (error != 0)
		error = errno;
#endif
	if (error == 0)
		return (error);

	(void) sc_syslog_msg_initialize(&handle,
	    SC_SYSLOG_RGMPMF_TAG, "");
	/*
	 * SCMSGS
	 * @explanation
	 * The server was not able to set the scheduling mode
	 * parameters, and the system error is shown. An error message
	 * is output to syslog.
	 * @user_action
	 * Save the /var/adm/messages file. Contact your authorized
	 * Sun service provider to determine whether a workaround or
	 * patch is available.
	 */
	(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
	    "Could not setup scheduling parameters: %s",
	    strerror(error));
	sc_syslog_msg_done(&handle);
	return (error);
}

static int
get_pcinfo(pcinfo_t *pc_info, const char *classname) {
	int err;
	sc_syslog_msg_handle_t handle;

	(void) strcpy(pc_info->pc_clname, classname);
	if (priocntl(P_PID, P_MYID, PC_GETCID, (char *)pc_info) == -1) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The server was not able to determine the scheduling mode
		 * info, and the system error is shown. An error message is
		 * output to syslog.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Could not lookup %s scheduling class info: %s",
		    pc_info->pc_clname, strerror(err));
		sc_syslog_msg_done(&handle);
		return (1);
	}
	return (0);
}

int
sc_get_rtparameters(pcparms_t *pc_parms) {
	pcinfo_t pc_info;
	rtparms_t *rtp = (rtparms_t *)pc_parms->pc_clparms;
	if (get_pcinfo(&pc_info, "RT") != 0)
		return (1);

	pc_parms->pc_cid = pc_info.pc_cid;
	/*
	 * Initialize realtime scheduling parameters - setting the
	 * timeslice to 100ms, and mnimizing the priority.
	 */
	rtp->rt_pri = 0;
	rtp->rt_tqnsecs = 100000000;
	rtp->rt_tqsecs = 0;
	return (0);
}


#if SOL_VERSION >= __s10
static int
proxy_priocntl(pid_t pid, pcparms_t *parms)
{
	int err;
	int serverfd = -1;
	door_arg_t door_arg;
	char *arg_buf = NULL;
	size_t arg_size;
	XDR xdrs;
	int errcode = 0;
	zoneup_args_t zoneup_args;
	char door_path[PATH_MAX];
	int door_code = RGM_ZONEUP_CODE;
	char arg[MAXPATHLEN];

	CL_PANIC(getzoneid() != GLOBAL_ZONEID);

	memcpy(arg, &door_code, sizeof (int));
	strcpy(arg + sizeof (int), GLOBAL_ZONENAME);

	if (_cladm(CL_CONFIG, CL_RGM_GET_DOOR, &arg) != 0) {
		errcode = errno;
		goto bailout;
	}
	memcpy(&serverfd, &arg, sizeof (int));

	memset(&xdrs, 0, sizeof (xdrs));

	zoneup_args.pid = pid;
	zoneup_args.parms = *parms;
	zoneup_args.zonename = NULL;

	arg_size = zoneup_xdr_sizeof(zoneup_args);
	arg_buf = (char *)calloc(1, arg_size);
	if (arg_buf == NULL) {
		errcode = ENOMEM;
		goto bailout;
	}

	/* Initialize the XDR Stream for Encoding data */
	xdrmem_create(&xdrs, arg_buf, (uint_t)arg_size, XDR_ENCODE);

	/*
	 * Marshall the two arguments. The order is the pmf_pid
	 * (as four bytes) followed immediately by the pcparms
	 * and then the zonename
	 */
	if (!xdr_zoneup_args(&xdrs, &zoneup_args)) {
		errcode = -1;
		goto bailout;
	}

	/*
	 * The data_ptr points to buffer storing the xdr-serialized input
	 * arguments. data_size contains input size. The same buffer is also
	 * used for storing the return output. Hence rbuf points to arg_buf.
	 * rsize stores maximum size of buffer that can be used. On return from
	 * the door_call, data_ptr and data_size conain information about result
	 */
	door_arg.data_ptr =  arg_buf;
	door_arg.data_size = xdr_getpos(&xdrs);
	door_arg.desc_ptr = NULL;
	door_arg.desc_num = 0;
	door_arg.rbuf = arg_buf;
	door_arg.rsize = arg_size;

	/* Call the door (zoneup_call) in sc_zonesd */
	if ((errcode = do_door_call(serverfd, &door_arg)) != 0) {
		goto bailout;
	}
	/* decode result */
	if (door_arg.data_ptr == NULL || door_arg.data_size == 0) {
		errcode = -1;
	} else {
		memcpy(&errcode, door_arg.rbuf, sizeof (int));
	}
bailout:
	free(arg_buf);
	if (xdrs.x_ops != NULL) {
		xdr_destroy(&xdrs);
	}
	if (serverfd >= 0)
		(void) close(serverfd);
	return (errcode);
}
#endif
