#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.10	08/08/06 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# lib/libsczones/Makefile.com
#

LIBRARY= libsczones.a
VERS= .1

COBJECTS = sczones.o scpriority.o
OBJECTS = $(COBJECTS)
LIBZONECFG_32=$(REF_PROTO)/usr/lib/libzonecfg.so.1
LIBZONECFG_64=$(REF_PROTO)/usr/lib/64/libzonecfg.so.1

# include library definitions, do not change order of include and DYNLIB
include ../../Makefile.lib
include $(SRC)/Makefile.ipc

LINTFILES = $(COBJECTS:%.o=%.ln)
CHECK_FILES = $(COBJECTS:%.o=%.c_check)

MAPFILE=	../common/mapfile-vers
PMAP=		-M $(MAPFILE)
SRCS =		$(COBJECTS:%.o=../common/%.c)

LIBS = $(DYNLIB) $(LIBRARY)

# definitions for lint
LINTFLAGS += -Dlint

MTFLAG	= -mt
CPPFLAGS += $(CL_CPPFLAGS)
CPPFLAGS += -I. -D_REENTRANT
CPPFLAGS += -I$(REF_PROTO)/usr/src/head
CPPFLAGS += -I$(REF_PROTO)/usr/src/lib/libbrand/common
CPPFLAGS += -I$(REF_PROTO)/usr/src/lib/libuutil/common

LDLIBS += -lc -ldoor -lclos -lnsl

AS_CPPFLAGS     += -D_ASM
# need to set this to NULL for lint
lint:= AS_CPPFLAGS     =
ASFLAGS         += -P

.KEEP_STATE:

.NO_PARALLEL:

all:  $(LIBS)

$(DYNLIB): 	$(MAPFILE)

# include library targets
include ../../Makefile.targ

objs/%.o pics/%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<

objs/%.o pics/%.o: %.s
	$(COMPILE.s) -o $@ $<
