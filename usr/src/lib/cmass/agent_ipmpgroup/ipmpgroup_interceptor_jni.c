/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * This file contains native library functions used for
 * the IPMP group mbean interceptor
 */

#pragma	ident	"@(#)ipmpgroup_interceptor_jni.c 1.3	08/05/20 SMI"

#include "com/sun/cluster/agent/ipmpgroup/IpmpGroupInterceptor.h"

#include <jniutil.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <cl_query/cl_query_types.h>

/*
 * A couple of helper macros for things we do a lot,
 * they contain references to local variables in the
 * function below
 */
#define	PUT_ATTR(MAP, NAME, VALUE)	      \
	PUT(MAP, NAME, (*env)->NewObject(env, \
	    attribute_class,	              \
	    attribute_constructor,	      \
	    (*env)->NewStringUTF(env, NAME),  \
	    VALUE))

#define	PUT(MAP, NAME, VALUE)                \
	(*env)->CallObjectMethod(env,        \
	    MAP,                             \
	    map_put,                         \
	    (*env)->NewStringUTF(env, NAME), \
	    VALUE)

/*
 * Name attribute delimiter: between node name and adapter name
 */
const char separator = ':';

/*
 * Class:     com_sun_cluster_agent_ipmpgroup_IpmpGroupInterceptor
 * Method:    fillCache
 * Signature: ()Ljava/util/Map;
 */
JNIEXPORT jobject JNICALL
Java_com_sun_cluster_agent_ipmpgroup_IpmpGroupInterceptor_fillCache
	(JNIEnv * env,
	jobject this) {

	/*
	 * Return a Map of Maps containing all instances
	 * key = instanceName
	 * value = Map:
	 * - key=attributeName
	 * - value=corresponding Attribute object
	 */

	jobject cache_map = NULL;
	jobject instance_map = NULL;
	jclass map_class = NULL;
	jmethodID map_put;

	jclass attribute_class = NULL;
	jmethodID attribute_constructor = NULL;

	cl_query_result_t result;
	cl_query_ipmp_info_t *ipmpgroup = NULL;

	cl_query_error_t error = NULL;

	/*
	 * get our references to 'attribute_class'
	 */

	attribute_class = (*env)->FindClass(env, "javax/management/Attribute");
	if (attribute_class == NULL)
		goto error;

	/*
	 * get constructor
	 */
	attribute_constructor =
	    (*env)->GetMethodID(env,
	    attribute_class,
	    "<init>",
	    "(Ljava/lang/String;Ljava/lang/Object;)V");
	if (attribute_constructor == NULL)
		goto error;


	/*
	 * Create the map we are going to return
	 */
	cache_map = newObjNullParam(env, "java/util/HashMap");
	if (cache_map == NULL)
		goto error;

	/*
	 * get our references to map_class and the put method
	 */
	map_class = (*env)->FindClass(env, "java/util/Map");
	if (map_class == NULL)
		goto error;
	map_put =
	    (*env)->GetMethodID(env,
	    map_class,
	    "put",
	    "(Ljava/lang/Object;Ljava/lang/Object;)"
	    "Ljava/lang/Object;");
	if (map_put == NULL)
		goto error;

	/*
	 * Call SCHA_API with to get all data
	 */

	error = cl_query_get_info(CL_QUERY_IPMP_INFO, NULL, &result);

	if (error != CL_QUERY_OK)
		goto error;

	for (ipmpgroup = (cl_query_ipmp_info_t *)result.value_list;
	    ipmpgroup != NULL;
	    ipmpgroup = ipmpgroup->next) {

		char *instance_name = NULL;
		/*
		 * We are going to create a map for this instance and
		 * populate it with its attributes
		 */

		instance_map = newObjNullParam(env, "java/util/HashMap");
		if (instance_map == NULL)
			goto error;

		if ((ipmpgroup->group_name != NULL) &&
		    (ipmpgroup->node_name != NULL)) {

			/*
			 * Instance name is built from group_name and
			 * node_name
			 */
			int retval;

			instance_name = (char *)malloc(sizeof (char) *
			    (strlen(ipmpgroup->group_name) +
				strlen(ipmpgroup->node_name) + 2));
			/*
			 * Set instance_name_utf to this instance name
			 */
			if (instance_name == NULL)
				goto error;
			retval = sprintf(instance_name,
			    "%s%c%s", ipmpgroup->group_name,
			    separator, ipmpgroup->node_name);
			if (retval < 0) {
				free(instance_name);
				goto error;
			}
			PUT_ATTR(instance_map,
			    "Name",
			    newObjStringParam(env,
				"java/lang/String", instance_name));
		} else {
			goto error;
		}

		PUT_ATTR(instance_map,
		    "GroupName",
		    newObjStringParam(env,
			"java/lang/String", ipmpgroup->group_name));

		PUT_ATTR(instance_map,
		    "Online",
		    newBoolean(env,
			ipmpgroup->status == CL_QUERY_ONLINE));

		PUT_ATTR(instance_map,
		    "NodeName",
		    newObjStringParam(env,
			"java/lang/String", ipmpgroup->node_name));

		PUT_ATTR(instance_map,
		    "Adapters",
		    getClQueryList(env,
			&ipmpgroup->adapter_list));
		/*
		 * Now put this map into the returned map under this
		 * instance
		 */

		PUT(cache_map,
		    instance_name,
		    instance_map);

		free(instance_name);
	}

    error:

	if (error == CL_QUERY_OK)
		error = cl_query_free_result(CL_QUERY_IPMP_INFO, &result);

	return (cache_map);
} /*lint !e715 */
