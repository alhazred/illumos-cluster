/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)IpmpGroupInterceptor.java 1.10     09/03/27 SMI"
 */
package com.sun.cluster.agent.ipmpgroup;

// JDK
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

// JMX
import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServer;
import javax.management.MBeanServerNotification;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.management.ReflectionException;

// JDMK
import com.sun.jdmk.JdmkMBeanServer;
import com.sun.jdmk.ServiceName;
import com.sun.jdmk.interceptor.MBeanServerInterceptor;

// Cacao
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.agent.CachedVirtualMBeanInterceptor;
import com.sun.cacao.agent.VirtualMBeanDomainDispatcher;
import com.sun.cacao.invocation.InvocationException;
import com.sun.cacao.invocation.InvocationStatus;
import com.sun.cacao.invocation.InvokeCommand;

// Local
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.event.ClEventDefs;
import com.sun.cluster.agent.event.SysEventNotification;
import com.sun.cluster.agent.event.SysEventNotifierMBean;
import com.sun.cluster.agent.node.Node;


/**
 * CMASS interceptor for the IPMP Group module. All instance and attribute
 * values are fetched through JNI, and all operations are performed through an
 * invocation of a CLI. This interceptor also listen for the events triggered by
 * the cluster for dynamic handle of virtual mbeans creation and deletion.
 */
public class IpmpGroupInterceptor extends CachedVirtualMBeanInterceptor
    implements NotificationListener {

    /* Tracing utility. */
    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.ipmpgroup");
    private final static String logTag = "IpmpGroupInterceptor";

    /* System commands */
    private final static String IFCONFIG = "/usr/sbin/ifconfig";

    /* Internal reference to the SysEvent Notifier. */
    private ObjectName sysEventNotifier;

    /* The MBeanServer in which this interceptor is inserted. */
    private MBeanServer mBeanServer;

    /**
     * Default Constructor.
     *
     * @param  mBeanServer  The MBeanServer in which to insert this interceptor.
     * @param  dispatcher  DomainDispatcher that this virtual mbean interceptor
     * should register into for dispatching.
     * @param  onf  ObjectNameFactory implementation to be used by this
     * VirtualMBeanInterceptor
     */
    public IpmpGroupInterceptor(MBeanServer mBeanServer,
        VirtualMBeanDomainDispatcher dispatcher, ObjectNameFactory onf) {

        super(mBeanServer, dispatcher, onf, IpmpGroupMBean.class, null);
        logger.entering(logTag, "<init>",
            new Object[] { mBeanServer, dispatcher, onf });
        this.mBeanServer = mBeanServer;
        this.sysEventNotifier =
            new ObjectNameFactory("com.sun.cluster.agent.event").getObjectName(
                SysEventNotifierMBean.class, null);
        logger.exiting(logTag, "<init>");
    }

    /**
     * Called when the module is unlocked
     */
    public void unlock() throws Exception {
        super.unlock();
        logger.entering(logTag, "unlock");
        mBeanServer.addNotificationListener(sysEventNotifier, this, null, null);
        logger.exiting(logTag, "unlock");
    }

    /**
     * Called when the module is locked
     */
    public void lock() throws Exception {
        super.lock();
        logger.entering(logTag, "lock");
        mBeanServer.removeNotificationListener(sysEventNotifier, this, null,
            null);
        invalidateCache();
        logger.exiting(logTag, "lock");
    }

    /**
     * The interceptor should monitor create/destroy/attribute-change events and
     * emit appropriate notifications from the mbean server (for create/destroy)
     * or from the virtual mbean (attribute change).
     *
     * <p>This method implements the NotificationListener interface.
     *
     * @param  notification  a <code>Notification</code> value
     * @param  handback  an <code>Object</code> value
     */
    public void handleNotification(Notification notification, Object handback) {

        logger.entering(logTag, "handleNotification",
            new Object[] { notification, handback });

        if (!(notification instanceof SysEventNotification))
            return;

        SysEventNotification clEvent = (SysEventNotification) notification;

        invalidateCache();

        // XXX Not sure about Events to support, not unit tested
        if (
            clEvent.getSubclass().equals(
                    ClEventDefs.ESC_CLUSTER_IPMP_GROUP_CHANGE) ||
                clEvent.getSubclass().equals(
                    ClEventDefs.ESC_CLUSTER_IPMP_GROUP_MEMBER_CHANGE)) {

            Map clEventAttrs = clEvent.getAttrs();

            String name = (String) clEventAttrs.get(
                    ClEventDefs.IPMP_GROUP_NAME);

            int clConfigAction =
                ((Long) (clEventAttrs.get(ClEventDefs.IPMP_GROUP_OPERATION)))
                .intValue();

            // workaround for JDK1.5/1.6 to make sure the Enum is referenced.
            ClEventDefs.ClEventConfigActionEnum configAction =
                ClEventDefs.ClEventConfigActionEnum.CL_EVENT_CONFIG_ADDED;
            configAction = (ClEventDefs.ClEventConfigActionEnum)
                (ClEventDefs.ClEventConfigActionEnum.getEnum(
                        ClEventDefs.ClEventConfigActionEnum.class,
                        clConfigAction));

            if (configAction ==
                    ClEventDefs.ClEventConfigActionEnum.CL_EVENT_CONFIG_ADDED) {

                // Send an MBean registration event
                if (logger.isLoggable(Level.FINEST))
                    logger.finest("Register MBean " + name);

                try {
                    ObjectName obj = getObjectNameFactory().getObjectName(
                            IpmpGroupMBean.class, name);
                    ObjectName delegateName = new ObjectName(
                            ServiceName.DELEGATE);
                    Notification notif = new MBeanServerNotification(
                            MBeanServerNotification.REGISTRATION_NOTIFICATION,
                            delegateName, 0, obj);
                    ((JdmkMBeanServer) mBeanServer).getMBeanServerDelegate()
                    .sendNotification(notif);
                } catch (Exception e) {
                    logger.log(Level.WARNING, "caught exception", e);
                }
            } else if (configAction ==
                    ClEventDefs.ClEventConfigActionEnum.
                    CL_EVENT_CONFIG_REMOVED) {

                // Send an MBean registration event
                if (logger.isLoggable(Level.FINEST))
                    logger.finest("Register MBean " + name);

                try {
                    ObjectName obj = getObjectNameFactory().getObjectName(
                            IpmpGroupMBean.class, name);
                    ObjectName delegateName = new ObjectName(
                            ServiceName.DELEGATE);
                    Notification notif = new MBeanServerNotification(
                            MBeanServerNotification.UNREGISTRATION_NOTIFICATION,
                            delegateName, 0, obj);
                    ((JdmkMBeanServer) mBeanServer).getMBeanServerDelegate()
                    .sendNotification(notif);
                } catch (Exception e) {
                    logger.log(Level.WARNING, "caught exception", e);
                }
            } else // Unexpected, should never happen
                logger.fine("Ignoring config change " + configAction);

        } else if (clEvent.getSubclass().equals(
                    ClEventDefs.ESC_CLUSTER_IPMP_GROUP_STATE)) {

            // XXX ESC_CLUSTER_IPMP_GROUP_STATE
            logger.fine("Ignoring state change");

        } else if (clEvent.getSubclass().equals(
                    ClEventDefs.ESC_CLUSTER_IPMP_IF_CHANGE)) {

            // XXX ESC_CLUSTER_IPMP_IF_CHANGE
            logger.fine("Ignoring if change");
        }

        logger.exiting(logTag, "handleNotification");
    }

    /**
     * Called by the parent interceptor class
     *
     * @param  instanceName  a <code>String</code> value
     * @param  operationName  a <code>String</code> value
     * @param  params  an <code>Object[]</code> value
     * @param  signature  a <code>String[]</code> value
     *
     * @return  an <code>Object</code> value
     *
     * @exception  InstanceNotFoundException  if an error occurs
     * @exception  MBeanException  if an error occurs
     * @exception  ReflectionException  if an error occurs
     * @exception  IOException  if an error occurs
     */
    public Object invoke(String instanceName, String operationName,
        Object params[], String signature[]) throws InstanceNotFoundException,
        MBeanException, ReflectionException, IOException {


        logger.entering(logTag, "invoke: " + operationName, signature);


        if (!isRegistered(instanceName)) {
            throw new InstanceNotFoundException("Cannot find the instance " +
                instanceName);
        }

        // Check that node in instanceName is the local node
        String instanceNodeName =
            (instanceName.split(IpmpGroupMBean.SEPARATOR))[1];
        String localNode = Node.getNodeName();

        if (!instanceNodeName.equals(localNode)) {
            logger.warning("Operation " + operationName + " for instance " +
                instanceName + " cannot be run from local node " + localNode +
                " but should be run from node " + instanceNodeName);
            throw new ReflectionException(new IllegalArgumentException(
                    operationName));
        }

        // Construct the command string from the operation name.
        String cmds[][] = null;

        // Add an adapter to this IPMP group
        if (operationName.equals("addAdapter")) {

            // Get the group name from the instance
            AttributeList attributeList = getAttributes(instanceName,
                    new String[] { "GroupName" });
            String group = (String) ((Attribute) attributeList.get(0))
                .getValue();

            // Retrieve parameters
            String adapter = null;
            boolean ipv4 = false;
            boolean ipv6 = false;
            String test = null;
            boolean standby = false;
            boolean persistent = false;

            if ((signature != null) && (signature.length == 6) &&
                    (signature[0].equals("java.lang.String")) &&
                    (signature[1].equals("boolean")) &&
                    (signature[2].equals("boolean")) &&
                    (signature[3].equals("java.lang.String")) &&
                    (signature[4].equals("boolean")) &&
                    (signature[5].equals("boolean"))) {

                adapter = (String) params[0];
                ipv4 = ((Boolean) params[1]).booleanValue();
                ipv6 = ((Boolean) params[2]).booleanValue();
                test = (String) params[3];
                standby = ((Boolean) params[4]).booleanValue();
                persistent = ((Boolean) params[5]).booleanValue();

                logger.finest("addAdapter " + group + " " + adapter + " " +
                    ipv4 + " " + ipv6 + " " + test + " " + standby + " " +
                    persistent);
            } else {
                throw new IOException("Invalid signature");
            }

            try {
                IpmpGroupManager ipmpGroupManager = new IpmpGroupManager(
                        mBeanServer);

                return (ipmpGroupManager.ipmpGroupAdd(group, adapter, ipv4,
                            ipv6, test, standby, persistent));
            } catch (CommandExecutionException cee) {
                throw new MBeanException(cee);
            }

            // Unexpected operation, should never happen
        } else {
            throw new ReflectionException(new IllegalArgumentException(
                    operationName));
        }
    }

    /**
     * Fill the cache
     *
     * @return  a <code>Map</code>, key is instance name, value is a map of
     * attribute name to attribute value.
     */
    protected native Map fillCache();
}
