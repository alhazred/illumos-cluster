/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)IpmpGroupMBean.java 1.7     08/05/20 SMI"
 */
package com.sun.cluster.agent.ipmpgroup;

import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;


/**
 * This interface defines the management operation available on a remote Cluster
 * IPMP group.
 */
public interface IpmpGroupMBean {

    /**
     * This field is used internally by the CMASS agent to identify the TYPE of
     * the module.
     */
    public static final String TYPE = "ipmpgroup";

    /**
     * String used inside IPMP Group's MBean instance name for separating the
     * group's name from the node's name.
     */
    public static final String SEPARATOR = ":";

    /**
     * Get the IPMP group's key name.
     *
     * @return  IPMP group's key name which is also the MBean instance name and
     * is composed of the IPMP group's name and node's name separated by the
     * character ":".
     */
    public String getName();


    /**
     * Get the IPMP group name.
     *
     * @return  IPMP group's key name.
     */
    public String getGroupName();

    /**
     * Indicates if the IPMP group is online or not.
     *
     * @return  <code>true</code> if the IPMP group is online, <code>
     * false</code> otherwise.
     */
    public boolean isOnline();

    /**
     * Get the node name linked to this IPMP group
     *
     * @return  the node name.
     *
     * @see  com.sun.cluster.agent.node.NodeMBean
     */
    public String getNodeName();

    /**
     * Get the array of adapter's names linked to this IPMP group.
     *
     * @return  an array of adapter's name.
     *
     * @see  com.sun.cluster.agent.transport.AdapterMBean
     */
    public String[] getAdapters();

    /**
     * Add an adapter to this IPMP group. The adapter is plumbed if not already
     * done. Set standby flag if specified. Create test address and logical
     * address. This operation is done on local node (node wide operation).
     *
     * @param  adapter  the adapter's name.
     * @param  ipv4  <code>true</code> for an IPV4 configuration, <code>
     * false</code> otherwise.
     * @param  ipv6  <code>true</code> for an IPv6 configuration, <code>
     * false</code> otherwise.
     * @param  test  the test address string to configure upon this adapter.
     * IPv4: mandatory, if the adapter is not already plumbed it will be the
     * physical interface's address, otherwise it will be the address of a new
     * logical address. IPv6: optional, if the adapter is not already plumbed
     * and test address is not null or empty, it will be the physical
     * interface's address, otherwise it will be the address of a new logical
     * address.
     * @param  standby  <code>true</code> to set the standby flag, <code>
     * false</code> otherwise.
     * @param  persistent  <code>true</code> to make the change persistent,
     * <code>false</code> otherwise. RFU, must be set to false.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     *
     * @see  com.sun.cluster.agent.transport.AdapterMBean
     */
    public ExitStatus[] addAdapter(String adapter, boolean ipv4, boolean ipv6,
        String test, boolean standby, boolean persistent)
        throws CommandExecutionException;
}
