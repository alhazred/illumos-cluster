/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)IpmpGroupManagerMBean.java 1.7     08/05/20 SMI"
 */

package com.sun.cluster.agent.ipmpgroup;

import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;


/**
 * This interface defines the management operation available to handle Ipmp
 * Groups.
 */
public interface IpmpGroupManagerMBean {

    /**
     * This field is used internally by the CMASS agent to identify the TYPE of
     * the module.
     */
    public final static String TYPE = "ipmpgroupManager";

    /**
     * Create an IPMP group for the adapter specified. The adapter is plumbed if
     * not already done, group is set to the given name. Create the test address
     * with flags DEPRECATED and NOFAILOVER set but no netmask and broadcast
     * settings, turn it UP. Set standby flag if specified. This operation is
     * done on local node (node wide operation).
     *
     * @param  adapter  the adapter's name.
     * @param  ipv4  <code>true</code> for IPv4 configutation, <code>
     * false</code> otherwise.
     * @param  ipv6  <code>true</code> for an IPv6 configuration, <code>
     * false</code> for otherwise.
     * @param  test  the test address string to configure upon this adapter.
     * IPv4: mandatory, if the adapter is not already plumbed it will be the
     * physical interface's address, otherwise it will be the address of a new
     * logical address. IPv6: optional, if the adapter is not already plumbed
     * and test address is not null or empty, it will be the physical
     * interface's address, otherwise it will be the address of a new logical
     * address.
     * @param  standby  <code>true</code> to set the standby flag, <code>
     * false</code> otherwise.
     * @param  persistent  <code>true</code> to make the change persistent after
     * reboot, <code>false</code> otherwise.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     *
     * @see  com.sun.cluster.agent.transport.AdapterMBean
     */
    public ExitStatus[] addIpmpGroup(String group, String adapter, boolean ipv4,
        boolean ipv6, String test, boolean standby, boolean persistent)
        throws CommandExecutionException;

    /**
     * Remove the IPMP Group by removing the Group name for all the adapters of
     * the group. This operation is done on local node (node wide operation).
     *
     * @param  group  the IPMP Group's name.
     * @param  persistent  <code>true</code> to make the change persistent,
     * <code>false</code> otherwise. RFU, must be set to false.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] removeIpmpGroup(String group, boolean persistent)
        throws CommandExecutionException;

    /**
     * Discover if the adapter is IPV4 configured.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */

    public ExitStatus[] isFlagIPV4(String adapterName)
        throws CommandExecutionException;

    /**
     * Discover if the adapter is IPV6 configured.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */

    public ExitStatus[] isFlagIPV6(String adapterName)
        throws CommandExecutionException;

}
