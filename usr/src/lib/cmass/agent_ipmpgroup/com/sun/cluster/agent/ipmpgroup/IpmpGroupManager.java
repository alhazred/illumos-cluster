/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)IpmpGroupManager.java 1.12     08/05/20 SMI"
 */

package com.sun.cluster.agent.ipmpgroup;

// JDK
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

// JMX
import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.ReflectionException;

// Cacao
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.invocation.InvocationException;
import com.sun.cacao.invocation.InvocationStatus;
import com.sun.cacao.invocation.InvokeCommand;

// Local
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.node.Node;
import com.sun.cluster.agent.transport.AdapterMBean;
import com.sun.cluster.agent.transport.NodeAdapterInterceptor;
import com.sun.cluster.agent.transport.NodeAdapterMBean;


/**
 * This class implements the {@link IpmpGroupManagerMBean} interface. This MBean
 * is created and register into the CMAS agent in the same way as the
 * {@link IpmpGroupModule} MBean.
 */
public class IpmpGroupManager implements IpmpGroupManagerMBean {

    /* Tracing utility. */
    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.ipmpgroup");
    private final static String logTag = "IpmpGroupManager";

    /* Commands */
    private final static String IFCONFIG = "/usr/sbin/ifconfig";
    private final static String IPMPSCRIPT =
        "/usr/cluster/lib/cmass/ipmpgroupmanager.sh";

    /* The MBeanServer in which this MBean is inserted. */
    private MBeanServer mBeanServer;

    /**
     * Default constructor.
     *
     * @param  mBeanServer  The MBeanServer in which to insert this MBean.
     */
    public IpmpGroupManager(MBeanServer mBeanServer) {
        this.mBeanServer = mBeanServer;
    }

    /*
     * Helper for doing the commands common to addIpmpGroup() and
     * IpmpGroupInterceptor.addAdapter().
     */
    public ExitStatus[] ipmpGroupAdd(String group, String adapter, boolean ipv4,
        boolean ipv6, String test, boolean standby, boolean persistent)
        throws CommandExecutionException {

        // Check if the interface is already plumbed
        String name = Node.getNodeName() + AdapterMBean.SEPARATOR + adapter;
        ObjectNameFactory transportONF = new ObjectNameFactory(
                "com.sun.cluster.agent.transport");
        ObjectName objectName = transportONF.getObjectName(
                NodeAdapterMBean.class, name);
        AttributeList attributeList = null;

        try {
            attributeList = mBeanServer.getAttributes(objectName,
                    new String[] { "Plumbed" });
        } catch (InstanceNotFoundException ie) {
            throw new CommandExecutionException("Node Adapter instance " +
                name + "not found");
        } catch (ReflectionException re) {
            throw new CommandExecutionException("Reflection Exception " +
                "when trying to invoke the " + "getAttributes method of " +
                "NodeAdapterMBean for " + "instance " + name);
        }

        Attribute attribute = (Attribute) attributeList.get(0);
        boolean plumbed = ((Boolean) attribute.getValue()).booleanValue();

        // Start building the command line using an ArrayList
        ArrayList list_ipv4 = new ArrayList();
        ArrayList list_ipv6 = new ArrayList();

        // Set standby flag
        String flag = "standby";

        if (ipv6) {
            list_ipv6.add(IFCONFIG);
            list_ipv6.add(adapter);
            list_ipv6.add("inet6");


            // Interface not plumbed
            if (!plumbed) {
                list_ipv6.add("plumb");
                list_ipv6.add("up");
            }

            // Test address always on group's physical interface for IPv6
            list_ipv6.add("group");
            list_ipv6.add(group);

            if (standby) {
                list_ipv6.add("standby");
            } else {
                list_ipv6.add("-standby");
                flag = "-standby";
            }
        }

        // IPv4
        if (ipv4) {
            list_ipv4.add(IFCONFIG);
            list_ipv4.add(adapter);
            list_ipv4.add("inet");

            // Interface not plumbed
            if (!plumbed) {
                list_ipv4.add("plumb");
            }

            // Create group and logical interface
            list_ipv4.add("group");
            list_ipv4.add(group);

            if ((test != null) && (test.length() > 0)) {
                list_ipv4.add("addif");
                list_ipv4.add(test);
                list_ipv4.add("deprecated");
            }

            list_ipv4.add("netmask");
            list_ipv4.add("+");
            list_ipv4.add("broadcast");
            list_ipv4.add("+");
            list_ipv4.add("up");
            list_ipv4.add("-failover");

            if (standby) {
                list_ipv4.add("standby");
            } else {
                list_ipv4.add("-standby");
                flag = "-standby";
            }
        }

        // Build the commands from the ArrayList
        Object objs_ipv4[] = null;
        Object objs_ipv6[] = null;
        String cmd1_ipv4[] = null;
        String cmd1_ipv6[] = null;

        if (ipv4) {
            objs_ipv4 = list_ipv4.toArray();
            cmd1_ipv4 = new String[objs_ipv4.length];

            for (int i = 0; i < objs_ipv4.length; i++) {
                cmd1_ipv4[i] = (String) objs_ipv4[i];
            }
        }

        if (ipv6) {
            objs_ipv6 = list_ipv6.toArray();
            cmd1_ipv6 = new String[objs_ipv6.length];

            for (int i = 0; i < objs_ipv6.length; i++) {
                cmd1_ipv6[i] = (String) objs_ipv6[i];
            }
        }

        // Build persistency command if any
        String cmds[][] = null;
        String cmd2_ipv4[] = null;
        String cmd2_ipv6[] = null;

        if (persistent) {

            if (ipv4) {
                cmd2_ipv4 = new String[] {
                        IPMPSCRIPT, "add", group, adapter, "inet", test, flag
                    };
            }

            if (ipv6) {
                cmd2_ipv6 = new String[] {
                        IPMPSCRIPT, "add", group, adapter, "inet6", test, flag
                    };
            }

            if (ipv4) {

                if (ipv6) {
                    cmds = new String[][] {
                            cmd1_ipv4, cmd2_ipv4, cmd1_ipv6, cmd2_ipv6
                        };
                } else {
                    cmds = new String[][] { cmd1_ipv4, cmd2_ipv4 };
                }
            } else {

                if (ipv6) {
                    cmds = new String[][] { cmd1_ipv6, cmd2_ipv6 };
                }
            }

        } else {

            if (ipv4) {

                if (ipv6) {
                    cmds = new String[][] { cmd1_ipv4, cmd1_ipv6 };
                } else {
                    cmds = new String[][] { cmd1_ipv4 };
                }
            } else {

                if (ipv6) {
                    cmds = new String[][] { cmd1_ipv6 };
                }
            }
        }

        // Run the command(s)
        InvocationStatus exits[];

        try {
            exits = InvokeCommand.execute(cmds, null);
        } catch (InvocationException ie) {

            // Rollback if command execution fails
            String cmd[][];
            String cmd_ipv4[] = null;
            String cmd_ipv6[] = null;

            if (plumbed) {

                if (ipv4) {
                    cmd_ipv4 = new String[] {
                            IFCONFIG, adapter, "inet", "group", ""
                        };
                }

                if (ipv6) {
                    cmd_ipv6 = new String[] {
                            IFCONFIG, adapter, "inet6", "group", ""
                        };
                }
            } else {

                if (ipv4) {
                    cmd_ipv4 = new String[] {
                            IFCONFIG, adapter, "inet", "unplumb"
                        };
                }

                if (ipv6) {
                    cmd_ipv6 = new String[] {
                            IFCONFIG, adapter, "inet6", "unplumb"
                        };
                }
            }

            if (ipv4) {

                if (ipv6) {
                    cmd = new String[][] { cmd_ipv4, cmd_ipv6 };
                } else {
                    cmd = new String[][] { cmd_ipv4 };
                }
            } else {

                if (ipv6) {
                    cmd = new String[][] { cmd_ipv6 };
                }
            }

            try {

                // No rollback ExitStatus as SPM don't support it
                InvokeCommand.execute(cmds, null);
            } catch (InvocationException ie2) {

                // Hide rollback failed command execution
                exits = ie.getInvocationStatusArray();
                throw new CommandExecutionException(ie.getMessage(),
                    ExitStatus.createArray(exits));
            }

            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        // Script command replaced by its stdout
        ExitStatus exitsWrapper[] = ExitStatus.createArray(exits);

        if ((exitsWrapper.length > 0) && persistent) {
            exitsWrapper[1] = NodeAdapterInterceptor.scriptStderrToCmd(
                    exitsWrapper[1]);
        }

        return exitsWrapper;
    }

    /**
     * Create an IPMP group for the adapter specified. The adapter is plumbed,
     * not already done. Set standby flag if specified. Create test address and
     * logical address. This operation is done on local node (node wide
     * operation).
     *
     * @param  adapter  the adapter's name.
     * @param  ipv4  <code>true</code> for IPv4 configuration, <code>
     * false</code> otherwise.
     * @param  ipv6  <code>true</code> for an IPv6 configuration, <code>
     * false</code> for otherwise.
     * @param  test  the test address string to configure upon this adapter.
     * IPv4: mandatory, if the adapter is not already plumbed it will be the
     * physical interface's address, otherwise it will be the address of a new
     * logical address. IPv6: optional, if the adapter is not already plumbed
     * and test address is not null or empty, it will be the physical
     * interface's address, otherwise it will be the address of a new logical
     * address.
     * @param  standby  <code>true</code> to set the standby flag, <code>
     * false</code> otherwise.
     * @param  persistent  <code>true</code> to make the change persistent,
     * after reboot <code>false</code> otherwise.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     *
     * @see  com.sun.cluster.agent.transport.AdapterMBean
     */
    public ExitStatus[] addIpmpGroup(String group, String adapter, boolean ipv4,
        boolean ipv6, String test, boolean standby, boolean persistent)
        throws CommandExecutionException {

        logger.entering(logTag,
            "addIpmpGroup " + group + " " + adapter + " " + ipv4 + " " + ipv6 +
            " " + test + " " + standby + " " + persistent);

        return ipmpGroupAdd(group, adapter, ipv4, ipv6, test, standby,
                persistent);
    }

    /**
     * Remove the IPMP Group by reseting the group option of all its adapters.
     *
     * @param  group  the IPMP Group's name, like ipmp0.
     * @param  persistent  <code>true</code> to make the change persistent,
     * after reboot <code>false</code> otherwise.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] removeIpmpGroup(String group, boolean persistent)
        throws CommandExecutionException {

        logger.entering(logTag, "removeIpmpGroup " + group + " " + persistent);

        // Construct the command string from the operation name.

        // Retrieve the adapters
        String name = group + IpmpGroupMBean.SEPARATOR + Node.getNodeName();
        ObjectNameFactory ipmpONF = new ObjectNameFactory(
                "com.sun.cluster.agent.ipmpgroup");
        ObjectName objectName = ipmpONF.getObjectName(IpmpGroupMBean.class,
                name);
        AttributeList attributeList = null;

        try {
            attributeList = mBeanServer.getAttributes(objectName,
                    new String[] { "Adapters" });
        } catch (InstanceNotFoundException ie) {
            throw new CommandExecutionException("IPMP Group instance " + name +
                "not found");
        } catch (ReflectionException re) {
            throw new CommandExecutionException("Reflection Exception " +
                "when trying to invoke the " + "getAttributes method of " +
                "IpmpGroupMBean for instance " + name);
        }

        ArrayList list_cmds = new ArrayList();
        String adapters[] = (String[]) ((Attribute) attributeList.get(0))
            .getValue();
        int size = adapters.length;
        String flag = "0";

        if (persistent) {
            flag = "1";
        }

        boolean ipv4;
        boolean ipv6;
        ExitStatus es[] = null;

        for (int i = 0; i < size; i++) {
            ipv4 = false;
            ipv6 = false;

            try {
                es = isFlagIPV4(adapters[i]);

                if (es[0].getReturnCode().intValue() != 0) {

                    // the command failed.
                    ipv4 = false;
                } else {
                    ipv4 = true;
                }
            } catch (CommandExecutionException cee) {
            }

            try {
                es = isFlagIPV6(adapters[i]);

                if (es[0].getReturnCode().intValue() != 0) {

                    // the command failed.
                    ipv6 = false;
                } else {
                    ipv6 = true;
                }
            } catch (CommandExecutionException cee) {
            }

            // logger.warning("removeIpmpGroup : adaptersName = "+adapters[i]+"
            // ipv4 = " +ipv4 + " ipv6 =  " +  ipv6);


            // if it came this far it probably succeeded
            ArrayList list1_ipv4 = new ArrayList();
            ArrayList list2_ipv4 = new ArrayList();
            ArrayList list1_ipv6 = new ArrayList();
            ArrayList list2_ipv6 = new ArrayList();
            list1_ipv4.add(IFCONFIG);
            list1_ipv4.add(adapters[i]);
            list1_ipv4.add("inet");
            list1_ipv4.add("group");
            list1_ipv4.add("");

            list2_ipv4.add(IPMPSCRIPT);
            list2_ipv4.add("del");
            list2_ipv4.add(adapters[i]);
            list2_ipv4.add("inet");
            list2_ipv4.add(flag);

            list1_ipv6.add(IFCONFIG);
            list1_ipv6.add(adapters[i]);
            list1_ipv6.add("inet6");
            list1_ipv6.add("group");
            list1_ipv6.add("");

            list2_ipv6.add(IPMPSCRIPT);
            list2_ipv6.add("del");
            list2_ipv6.add(adapters[i]);
            list2_ipv6.add("inet6");
            list2_ipv6.add(flag);

            if (ipv4) {

                if (ipv6) {
                    list_cmds.add(list1_ipv4);
                    list_cmds.add(list1_ipv6);

                    if (persistent) {
                        list_cmds.add(list2_ipv4);
                        list_cmds.add(list2_ipv6);
                    }
                } else {
                    list_cmds.add(list1_ipv4);

                    if (persistent) {
                        list_cmds.add(list2_ipv4);
                    }
                }
            } else {

                if (ipv6) {
                    list_cmds.add(list1_ipv6);

                    if (persistent) {
                        list_cmds.add(list2_ipv6);
                    }
                }
            }
        }

        // Build the commands from the ArrayList
        Object objs[] = list_cmds.toArray();
        String cmds[][] = new String[objs.length][5];
        objs = list_cmds.toArray();

        for (int i = 0; i < objs.length; i++) {
            Object objs2[] = ((ArrayList) objs[i]).toArray();

            for (int j = 0; j < objs2.length; j++) {
                cmds[i][j] = (String) objs2[j];
            }
        }

        int arraysize = persistent ? (size * 2) : size;

        // Run the command(s)
        InvocationStatus exits[];
        /*
         * int index = 0;
         * int arraysize = persistent ? (size * 2) : size;
         * ExitStatus[] exitsWrapper = new ExitStatus[arraysize] ;
         * try {
         *  for (index = 0; index < arraysize ; index++) {
         *      exits = InvokeCommand.execute(cmds[index], null);
         *      exitsWrapper = ExitStatus.createArray(exits);
         *      exitsWrapper[index] =
         * NodeAdapterInterceptor.scriptStderrToCmd(exitsWrapper[index]);
         *  }
         * } catch(InvocationException ie) {
         *  exits = ie.getInvocationStatusArray();
         *  throw new CommandExecutionException(ie.getMessage(),
         *                                  ExitStatus.createArray(exits));
         * }
         */

        try {
            exits = InvokeCommand.execute(cmds, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        // Script command replaced by its stdout
        ExitStatus exitsWrapper[] = ExitStatus.createArray(exits);

        return exitsWrapper;
    }

    /**
     * Discover if the adapter is IPV4 configured.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */

    public ExitStatus[] isFlagIPV4(String adapterName)
        throws CommandExecutionException {

        // Run the command
        InvocationStatus es[] = null;

        try {
            String c[][] = new String[][] {
                    { IFCONFIG, adapterName }
                };
            es = InvokeCommand.execute(c, null);
        } catch (InvocationException ie) {
            es = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(es));

        }

        // Script command replaced by its stdout
        ExitStatus exitsWrapper[] = ExitStatus.createArray(es);

        return exitsWrapper;
    }

    /**
     * Discover if the adapter is IPV6 configured.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] isFlagIPV6(String adapterName)
        throws CommandExecutionException {
        InvocationStatus es[] = null;

        try {
            String c[][] = new String[][] {
                    { IFCONFIG, adapterName, "inet6" }
                };
            es = InvokeCommand.execute(c, null);
        } catch (InvocationException ie) {
            es = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(es));
        }

        // Script command replaced by its stdout
        ExitStatus exitsWrapper[] = ExitStatus.createArray(es);

        return exitsWrapper;
    }

}
