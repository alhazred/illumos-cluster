/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_GAN_SENSOR_INTERFACE_H
#define	_GAN_SENSOR_INTERFACE_H

#pragma ident	"@(#)gan_sensor_interface.h	1.2	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Generic sensor interface.
 */

#include "sensors.h"

extern int gan_register(const char *, const char *, char *);
extern int gan_getvalues(const char *, const char **, const int,
				sensor_result_t **, int *, char *);
extern int gan_unregister(const char *, const char *, char *);

#ifdef __cplusplus
}
#endif

#endif	/* _GAN_SENSOR_INTERFACE_H */
