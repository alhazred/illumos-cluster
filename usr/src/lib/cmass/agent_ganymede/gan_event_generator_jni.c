/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
#pragma ident	"@(#)gan_event_generator_jni.c	1.3	08/05/20 SMI"

#include <stdlib.h>
#include <stdio.h>
#include <dlfcn.h>
#include <jni.h>
#include <sys/cl_events.h>

#include "gan_jni.h"	/* THROW_INTERNAL_EXCEPTION */

JNIEXPORT jint JNICALL
Java_com_sun_cluster_agent_ganymede_ThresholdBase_generateEvent(JNIEnv *env,
	jobject obj, jobject jdividendBigInteger, jint jdivisor,
	jstring jmot, jstring jki, jstring jmo,
	jstring jprevstatus, jstring jcurrstatus, jstring jdirection)
{
	cl_event_error_t err;
	jlong jdividend;	/* signed 64 bits */
	uint64_t dividend;
	int32_t divisor;
	const char *mot;
	const char *ki;
	const char *mo;
	const char *prevstatus;
	const char *currstatus;
	const char *direction;
	static jclass bigIntegerClass = NULL;
	static jmethodID bigIntegerLongValueMethod = NULL;

	if (bigIntegerClass == NULL) {
		jclass localRefClass = (*env)->FindClass(env,
			"java/math/BigInteger");

		if ((*env)->ExceptionCheck(env) == JNI_TRUE)
			return (1);

		/* create a global reference */
		bigIntegerClass = (*env)->NewGlobalRef(env, localRefClass);

		/* delete the local reference */
		(*env)->DeleteLocalRef(env, localRefClass);

		/* NewGlobalRef doesn't throw exception */
		if (bigIntegerClass == NULL)
			/*
			 * We don't throw a GanymedeInternalException here
			 * because NewGlobalRef() most probably failed
			 * because of an out-of-memory condition, and
			 * we don't want to make things worse...
			 */
			return (1);
	}

	if (bigIntegerLongValueMethod == NULL) {
		bigIntegerLongValueMethod = (*env)->GetMethodID(env,
			bigIntegerClass, "longValue", "()J");

		if ((*env)->ExceptionCheck(env) == JNI_TRUE)
			return (1);
	}

	jdividend = (*env)->CallLongMethod(env, jdividendBigInteger,
		bigIntegerLongValueMethod);
	if ((*env)->ExceptionCheck(env) == JNI_TRUE)
		return (1);

	dividend = (uint64_t)jdividend;

	divisor = (int32_t)jdivisor;

	mot = (*env)->GetStringUTFChars(env, jmot, 0);
	if ((*env)->ExceptionCheck(env) == JNI_TRUE) {
		return (1);
	}

	ki = (*env)->GetStringUTFChars(env, jki, 0);
	if ((*env)->ExceptionCheck(env) == JNI_TRUE) {
		(*env)->ReleaseStringUTFChars(env, jmot, mot);
		return (1);
	}

	mo = (*env)->GetStringUTFChars(env, jmo, 0);
	if ((*env)->ExceptionCheck(env) == JNI_TRUE) {
		(*env)->ReleaseStringUTFChars(env, jki, ki);
		(*env)->ReleaseStringUTFChars(env, jmot, mot);
		return (1);
	}

	prevstatus = (*env)->GetStringUTFChars(env, jprevstatus, 0);
	if ((*env)->ExceptionCheck(env) == JNI_TRUE) {
		(*env)->ReleaseStringUTFChars(env, jmo, mo);
		(*env)->ReleaseStringUTFChars(env, jki, ki);
		(*env)->ReleaseStringUTFChars(env, jmot, mot);
		return (1);
	}

	currstatus = (*env)->GetStringUTFChars(env, jcurrstatus, 0);
	if ((*env)->ExceptionCheck(env) == JNI_TRUE) {
		(*env)->ReleaseStringUTFChars(env, jprevstatus, prevstatus);
		(*env)->ReleaseStringUTFChars(env, jmo, mo);
		(*env)->ReleaseStringUTFChars(env, jki, ki);
		(*env)->ReleaseStringUTFChars(env, jmot, mot);
		return (1);
	}

	direction = (*env)->GetStringUTFChars(env, jdirection, 0);
	if ((*env)->ExceptionCheck(env) == JNI_TRUE) {
		(*env)->ReleaseStringUTFChars(env, jcurrstatus, currstatus);
		(*env)->ReleaseStringUTFChars(env, jprevstatus, prevstatus);
		(*env)->ReleaseStringUTFChars(env, jmo, mo);
		(*env)->ReleaseStringUTFChars(env, jki, ki);
		(*env)->ReleaseStringUTFChars(env, jmot, mot);
		return (1);
	}

#if 0
	(void) fprintf(stderr, "%llu, %d, %s, %s, %s, %s, %s, %s\n",
		dividend, divisor, mot, ki, mo, prevstatus,
		currstatus, direction);
#endif


	if ((err = sc_publish_event(
		ESC_CLUSTER_SLM_THR_STATE, CL_EVENT_PUB_SLM,
		CL_EVENT_SEV_INFO, CL_EVENT_INIT_AGENT, 0, 0, 0,
		CL_DIVIDEND, SE_DATA_TYPE_UINT64, dividend,
		CL_DIVISOR, SE_DATA_TYPE_INT32, divisor,
		CL_OBJECT_TYPE, SE_DATA_TYPE_STRING, mot,
		CL_TELEMETRY_ATTRIBUTE, SE_DATA_TYPE_STRING, ki,
		CL_OBJECT_INSTANCE, SE_DATA_TYPE_STRING, mo,
		CL_OLD_STATUS, SE_DATA_TYPE_STRING, prevstatus,
		CL_NEW_STATUS, SE_DATA_TYPE_STRING, currstatus,
		CL_DIRECTION, SE_DATA_TYPE_STRING, direction,
		NULL)) != (cl_event_error_t)0) {

		switch (err) {
		case CL_EVENT_EATTR:
		case CL_EVENT_EINVAL:
			THROW_INTERNAL_EXCEPTION(env,
				"threshold event generator error");
			break;
		default:
			THROW_INTERNAL_EXCEPTION(env,
				"event generator internal error");
			break;
		}
	}

	(*env)->ReleaseStringUTFChars(env, jdirection, direction);
	(*env)->ReleaseStringUTFChars(env, jcurrstatus, currstatus);
	(*env)->ReleaseStringUTFChars(env, jprevstatus, prevstatus);
	(*env)->ReleaseStringUTFChars(env, jmo, mo);
	(*env)->ReleaseStringUTFChars(env, jki, ki);
	(*env)->ReleaseStringUTFChars(env, jmot, mot);

	return ((int)err);
}
