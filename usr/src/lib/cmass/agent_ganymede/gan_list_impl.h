/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_GAN_LIST_IMPL_H
#define	_GAN_LIST_IMPL_H

#pragma ident	"@(#)gan_list_impl.h	1.2	08/05/20 SMI"

#include <sys/types.h>

struct gan_list_node {
	struct gan_list_node *list_next;
	struct gan_list_node *list_prev;
};

struct gan_list {
	size_t	list_size;
	size_t	list_offset;
	struct	gan_list_node list_head;
};

#endif	/* _GAN_LIST_IMPL_H */
