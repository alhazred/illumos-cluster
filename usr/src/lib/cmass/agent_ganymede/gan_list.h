/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_GAN_LIST_H
#define	_GAN_LIST_H

#pragma ident	"@(#)gan_list.h	1.3	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include "gan_list_impl.h"

typedef struct gan_list_node gan_list_node_t;
typedef struct gan_list gan_list_t;

void gan_list_create(gan_list_t *, size_t, size_t);
void gan_list_destroy(gan_list_t *);

void gan_list_insert_after(gan_list_t *, void *, void *);
void gan_list_insert_before(gan_list_t *, void *, void *);
void gan_list_insert_head(gan_list_t *, void *);
void gan_list_insert_tail(gan_list_t *, void *);
void gan_list_remove(gan_list_t *, void *);
void gan_list_move_tail(gan_list_t *, gan_list_t *);

void *gan_list_head(gan_list_t *);
void *gan_list_tail(gan_list_t *);
void *gan_list_next(gan_list_t *, void *);
void *gan_list_prev(gan_list_t *, void *);

#define	gan_list_for_each(it, list)\
	for (\
		it = gan_list_head((list));\
		it != NULL;\
		it = gan_list_next((list), it))

#ifdef __cplusplus
}
#endif

#endif	/* _GAN_LIST_H */
