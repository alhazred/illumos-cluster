/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
#pragma ident	"@(#)gan_sensor_interface_jni.c	1.3	08/05/20 SMI"

/*
 * The functions register, unregister and getvalues implemented
 * herein are the entry points to the sensor API. These functions
 * deal with the Java-to-C and C-to-Java conversions, the
 * remaining logic being implemented in ganymede.c.
 */

#include <string.h>		/* strcmp, strncpy */
#include <stdlib.h>
#include <stdio.h>
#include <dlfcn.h>
#include <jni.h>

#include "sensors.h"
#include "gan_jni.h"
#include "gan_sensor_interface.h"

#define	UINT64BUFFERSIZE 255

/* ARGSUSED */
JNIEXPORT jint JNICALL
Java_com_sun_cluster_agent_ganymede_SensorLib_registerJNI(JNIEnv *env,
	jobject obj, jstring jlib, jstring jki)
{
	int error_code;
	char error_string[ERROR_STRING_MAX];
	const char *lib;
	const char *ki;

	lib = (*env)->GetStringUTFChars(env, jlib, NULL);

	if ((*env)->ExceptionCheck(env) == JNI_TRUE)
		return (1);

	ki = (*env)->GetStringUTFChars(env, jki, NULL);

	if ((*env)->ExceptionCheck(env) == JNI_TRUE) {
		(*env)->ReleaseStringUTFChars(env, jlib, lib);
		return (1);
	}

	/*
	 * Call into generic library.
	 */
	error_code = gan_register(lib, ki, error_string);

	switch (error_code) {
	case 0: /* success */
		break;

	case 1: /* internal error */
		THROW_INTERNAL_EXCEPTION(env, error_string); /*lint !e717 */
		break;

	default: /* sensor error */
		THROW_SENSOR_EXCEPTION(env, error_string); /*lint !e717 */
		break;
	}

	(*env)->ReleaseStringUTFChars(env, jlib, lib);
	(*env)->ReleaseStringUTFChars(env, jki, ki);

	return (error_code);
}

JNIEXPORT jint JNICALL
Java_com_sun_cluster_agent_ganymede_SensorLib_getValuesJNI(JNIEnv *env,
	jobject obj, jstring jlib, jobjectArray jmo, jobject vector)
{
	sensor_result_t *results;
	jclass cls;
	jmethodID mid;
	int n_results;
	int error_code;
	char error_string[ERROR_STRING_MAX];
	char **monitored_objects = NULL;
	jstring *j_monitored_objects = NULL;
	const char *lib;
	int len = 0;
	int i = 0;
	static jclass vectorClass = 0;
	static jclass entryClass = 0;
	static jclass dateClass = 0;
	static jclass bigIntegerClass = 0;
	static jmethodID vectorAddMethod = 0;
	static jmethodID entryConstructor = 0;
	static jmethodID dateConstructor = 0;
	static jmethodID dateSetNanoMethod = 0;
	static jmethodID bigIntegerConstructor = 0;
	static char uint64buffer[UINT64BUFFERSIZE];

	/*
	 * Get a reference to the getMOType method
	 */
	cls = (*env)->GetObjectClass(env, obj);

	mid = (*env)->GetMethodID(env, cls, "getMOType",
		"(Ljava/lang/String;)Ljava/lang/String;");

	if ((*env)->ExceptionCheck(env) == JNI_TRUE)
		return (1);

	/*
	 * Allocate an array of char * and an array of jstring
	 */
	if ((jmo != NULL) && ((len = (*env)->GetArrayLength(env, jmo)) != 0)) {

		if ((monitored_objects = (char **)calloc((size_t)len,
		    sizeof (char *)))
			== NULL) {

			THROW_INTERNAL_EXCEPTION(env,
			    "calloc() failed\n"); /*lint !e717 */
			return (1);
		}

		if ((j_monitored_objects = (jstring *)calloc((size_t)len,
			sizeof (jstring))) == NULL) {

			free(monitored_objects);
			THROW_INTERNAL_EXCEPTION(env,
			    "calloc() failed\n"); /*lint !e717 */
			return (1);
		}
	}

	/*
	 * Set the values for jstring and char *
	 */
	for (i = 0; i < len; i++) {
		j_monitored_objects[i] =
			(*env)->GetObjectArrayElement(env, jmo, i);

		if ((*env)->ExceptionCheck(env) == JNI_TRUE) {
			free(j_monitored_objects);
			free(monitored_objects);
			return (1);
		}

		monitored_objects[i] = (char *)(*env)->GetStringUTFChars(env,
				j_monitored_objects[i], NULL);

		if ((*env)->ExceptionCheck(env) == JNI_TRUE) {
			free(j_monitored_objects);
			free(monitored_objects);
			return (1);
		}
	}

	/*
	 * Get the sensor library path.
	 */
	lib = (*env)->GetStringUTFChars(env, jlib, NULL);

	if ((*env)->ExceptionCheck(env) == JNI_TRUE)
		return (1);

	/*
	 * Call into the generic library.
	 */
	error_code = gan_getvalues(lib, (const char **)monitored_objects, len,
		&results, &n_results, error_string);

	/*
	 * Free the sensor library path.
	 */
	(*env)->ReleaseStringUTFChars(env, jlib, lib);

	/*
	 * Free the managed objects
	 */
	for (i = 0; i < len; i++) {
		(*env)->ReleaseStringUTFChars(env, j_monitored_objects[i],
			monitored_objects[i]);

		if ((*env)->ExceptionCheck(env) == JNI_TRUE) {
			free(j_monitored_objects);
			free(monitored_objects);
			return (1);
		}
	}

	/*
	 *  Free both tables
	 */
	free(j_monitored_objects);
	free(monitored_objects);

	switch (error_code) {
	case 0: /* success */
		break;

	case 1: /* internal error */
		THROW_INTERNAL_EXCEPTION(env,
		    error_string); /*lint !e717 */
		return (1);

	default: /* sensor error */
		THROW_SENSOR_EXCEPTION(env,
		    error_string); /*lint !e717 */
		return (1);
	}

	if (vectorClass == NULL) {
		jclass localRefClass = (*env)->FindClass(env,
			"java/util/Vector");

		if ((*env)->ExceptionCheck(env) == JNI_TRUE)
			return (1);

		/* create a global reference */
		vectorClass = (*env)->NewGlobalRef(env, localRefClass);

		/* delete the local reference */
		(*env)->DeleteLocalRef(env, localRefClass);

		/* NewGlobalRef doesn't throw exception */
		if (vectorClass == NULL)
			/*
			 * We don't throw a GanymedeInternalException here
			 * because NewGlobalRef() most probably failed
			 * because of an out-of-memory condition, and
			 * we don't want to make things worse...
			 */
			return (1);
	}

	if (entryClass == NULL) {
		jclass localRefClass = (*env)->FindClass(env,
			"com/sun/cluster/agent/ganymede/SensorResult");

		if ((*env)->ExceptionCheck(env) == JNI_TRUE)
			return (1);

		/* create a global reference */
		entryClass = (*env)->NewGlobalRef(env, localRefClass);

		/* delete the local reference */
		(*env)->DeleteLocalRef(env, localRefClass);

		/* NewGlobalRef doesn't throw exception */
		if (entryClass == NULL)
			/*
			 * We don't throw a GanymedeInternalException here
			 * because NewGlobalRef() most probably failed
			 * because of an out-of-memory condition, and
			 * we don't want to make things worse...
			 */
			return (1);
	}

	if (dateClass == NULL) {
		jclass localRefClass = (*env)->FindClass(env,
			"java/sql/Timestamp");

		if ((*env)->ExceptionCheck(env) == JNI_TRUE)
			return (1);

		/* create a global reference */
		dateClass = (*env)->NewGlobalRef(env, localRefClass);

		/* delete the local reference */
		(*env)->DeleteLocalRef(env, localRefClass);

		/* NewGlobalRef doesn't throw exception */
		if (dateClass == NULL)
			/*
			 * We don't throw a GanymedeInternalException here
			 * because NewGlobalRef() most probably failed
			 * because of an out-of-memory condition, and
			 * we don't want to make things worse...
			 */
			return (1);
	}

	if (bigIntegerClass == NULL) {
		jclass localRefClass = (*env)->FindClass(env,
			"java/math/BigInteger");

		if ((*env)->ExceptionCheck(env) == JNI_TRUE)
			return (1);

		/* create a global reference */
		bigIntegerClass = (*env)->NewGlobalRef(env, localRefClass);

		/* delete the local reference */
		(*env)->DeleteLocalRef(env, localRefClass);

		/* NewGlobalRef doesn't throw exception */
		if (bigIntegerClass == NULL)
			/*
			 * We don't throw a GanymedeInternalException here
			 * because NewGlobalRef() most probably failed
			 * because of an out-of-memory condition, and
			 * we don't want to make things worse...
			 */
			return (1);
	}

	if (entryConstructor == NULL) {
		entryConstructor = (*env)->GetMethodID(env, entryClass,
			"<init>",
			"(Ljava/lang/String;"
			"Ljava/lang/String;"
			"Ljava/lang/String;"
			"Ljava/sql/Timestamp;"
			"Ljava/math/BigInteger;"
			"Ljava/math/BigInteger;)V");

		if ((*env)->ExceptionCheck(env) == JNI_TRUE)
			return (1);
	}

	if (dateConstructor == NULL) {
		dateConstructor = (*env)->GetMethodID(env, dateClass, "<init>",
			"(J)V");

		if ((*env)->ExceptionCheck(env) == JNI_TRUE)
			return (1);
	}

	if (vectorAddMethod == NULL) {
		vectorAddMethod = (*env)->GetMethodID(env, vectorClass, "add",
			"(Ljava/lang/Object;)Z");

		if ((*env)->ExceptionCheck(env) == JNI_TRUE)
			return (1);
	}

	if (dateSetNanoMethod == NULL) {
		dateSetNanoMethod = (*env)->GetMethodID(env, dateClass,
			"setNanos", "(I)V");

		if ((*env)->ExceptionCheck(env) == JNI_TRUE)
			return (1);
	}

	if (bigIntegerConstructor == NULL) {
		bigIntegerConstructor = (*env)->GetMethodID(env,
			bigIntegerClass, "<init>", "(Ljava/lang/String;)V");

		if ((*env)->ExceptionCheck(env) == JNI_TRUE)
			return (1);
	}

	for (i = 0; i < n_results; i++) {
		jstring monitored_entity;
		jstring key_indicator_name;
		jstring monitored_entity_type;
		jstring value_str;
		jstring upperbound_str;
		jlong date;
		jint nano;
		jobject entryObject;
		jobject dateObject;
		jobject valueBigIntegerObject;
		jobject upperboundBigIntegerObject;
		jboolean bool_res = JNI_FALSE;

		/*
		 * Monitored entity
		 */
		monitored_entity = (*env)->NewStringUTF(env,
			results[i].managed_object);

		if ((*env)->ExceptionCheck(env) == JNI_TRUE)
			return (1);

		/*
		 * Key Indicator
		 */
		key_indicator_name = (*env)->NewStringUTF(env,
			results[i].ki_type);

		if ((*env)->ExceptionCheck(env) == JNI_TRUE)
			return (1);

		/*
		 * Monitored Entity Type
		 */
		monitored_entity_type = (*env)->CallObjectMethod(env, obj, mid,
			key_indicator_name);

		if ((*env)->ExceptionCheck(env) == JNI_TRUE)
			return (1);

		/*
		 * Date
		 */
		date = results[i].time_current.tv_sec;
		date *= 1000L;
		nano = results[i].time_current.tv_nsec;

		dateObject = (*env)->NewObject(env, dateClass,
			dateConstructor, date);

		if ((*env)->ExceptionCheck(env) == JNI_TRUE)
			return (1);

		(*env)->CallVoidMethod(env, dateObject,
			dateSetNanoMethod, nano);

		if ((*env)->ExceptionCheck(env) == JNI_TRUE)
			return (1);

		/*
		 * Value
		 */
		(void) snprintf(uint64buffer, UINT64BUFFERSIZE, "%llu",
			results[i].usage);

		value_str = (*env)->NewStringUTF(env, uint64buffer);

		if ((*env)->ExceptionCheck(env) == JNI_TRUE)
			return (1);

		valueBigIntegerObject = (*env)->NewObject(env,
		    bigIntegerClass,
		    bigIntegerConstructor,
		    value_str);

		if ((*env)->ExceptionCheck(env) == JNI_TRUE)
			return (1);

		/*
		 * Upper Bound
		 */
		(void) snprintf(uint64buffer, UINT64BUFFERSIZE, "%llu",
			results[i].upperbound);

		upperbound_str = (*env)->NewStringUTF(env, uint64buffer);

		if ((*env)->ExceptionCheck(env) == JNI_TRUE)
			return (1);

		upperboundBigIntegerObject = (*env)->NewObject(env,
		    bigIntegerClass,
		    bigIntegerConstructor,
		    upperbound_str);

		if ((*env)->ExceptionCheck(env) == JNI_TRUE)
			return (1);

		/*
		 * Result
		 */

		entryObject = (*env)->NewObject(env,
			entryClass, entryConstructor,
			monitored_entity,
			monitored_entity_type,
			key_indicator_name,
			dateObject,
			valueBigIntegerObject,
			upperboundBigIntegerObject);

		if ((*env)->ExceptionCheck(env) == JNI_TRUE)
			return (1);

		bool_res = (*env)->CallBooleanMethod(env, vector,
			vectorAddMethod, entryObject);

		if (bool_res != JNI_TRUE) {
			THROW_INTERNAL_EXCEPTION(env, "unexpected returned"
				" value from vectorAddMethod"); /*lint !e717 */
			return (1);
		}

		(*env)->DeleteLocalRef(env, monitored_entity);
		(*env)->DeleteLocalRef(env, key_indicator_name);
		(*env)->DeleteLocalRef(env, value_str);
		(*env)->DeleteLocalRef(env, upperbound_str);
		(*env)->DeleteLocalRef(env, dateObject);
		(*env)->DeleteLocalRef(env, valueBigIntegerObject);
		(*env)->DeleteLocalRef(env, upperboundBigIntegerObject);
		(*env)->DeleteLocalRef(env, entryObject);
	}

	return (0);
}

/* ARGSUSED */
JNIEXPORT jint JNICALL
Java_com_sun_cluster_agent_ganymede_SensorLib_unregisterJNI(JNIEnv *env,
	jobject obj, jstring jlib, jstring jki)
{
	int error_code;
	char error_string[ERROR_STRING_MAX];
	const char *lib;
	const char *ki;



	lib = (*env)->GetStringUTFChars(env, jlib, NULL);

	if ((*env)->ExceptionCheck(env) == JNI_TRUE)
		return (1);

	ki = (*env)->GetStringUTFChars(env, jki, NULL);

	if ((*env)->ExceptionCheck(env) == JNI_TRUE) {
		(*env)->ReleaseStringUTFChars(env, jlib, lib);
		return (1);
	}

	/*
	 * Call into generic library.
	 */
	error_code = gan_unregister(lib, ki, error_string);

	switch (error_code) {
	case 0: /* success */
		break;

	case 1: /* internal error */
		THROW_INTERNAL_EXCEPTION(env,
		    error_string); /*lint !e717 */
		break;

	default: /* sensor error */
		THROW_SENSOR_EXCEPTION(env,
		    error_string); /*lint !e717 */
		break;
	}

	(*env)->ReleaseStringUTFChars(env, jlib, lib);
	(*env)->ReleaseStringUTFChars(env, jki, ki);

	return (error_code);
}
