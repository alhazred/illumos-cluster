/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	__GAN_JNI_H
#define	__GAN_JNI_H

#pragma ident	"@(#)gan_jni.h	1.2	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <jni.h>

extern jclass internalExceptionClass; /* GanymedeInternalException ref */
extern jclass sensorExceptionClass; /* TelemetrySensorException ref */

/*
 * Macro definitions to throw Ganymede-defined Exceptions.  Notes: (a) we're
 * assured that FindClass has thrown an Exception if its return value is NULL
 * (b) NewGlobalRef does not throw Exception.
 */

#define	__THROW_EXCEPTION(__env, __exc_cl, __exc_path, __msg) do { \
	if (__exc_cl == NULL) { \
		jclass __loc_exc_cl = (*__env)->FindClass(__env, __exc_path); \
		if (__loc_exc_cl == NULL) /* exception thrown */ \
			break; \
		__exc_cl = (*__env)->NewGlobalRef(__env, __loc_exc_cl); \
		(*__env)->DeleteLocalRef(__env, __loc_exc_cl); \
		if (__exc_cl == NULL) { /* no exception thrown, too bad */ \
			break; \
		} \
	} \
	(*__env)->ThrowNew(__env, __exc_cl, __msg); \
	/* exception thrown */ \
} while (0)

#define	THROW_INTERNAL_EXCEPTION(__env, __msg) \
	__THROW_EXCEPTION( \
		__env, \
		internalExceptionClass, \
		"com/sun/cluster/agent/ganymede/GanymedeInternalException", \
		__msg);

#define	THROW_SENSOR_EXCEPTION(__env, __msg) \
	__THROW_EXCEPTION( \
		__env, \
		sensorExceptionClass, \
		"com/sun/cluster/agent/ganymede/TelemetrySensorException", \
		__msg)

#ifdef __cplusplus
}
#endif

#endif	/* __GAN_JNI_H */
