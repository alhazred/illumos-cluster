/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
#pragma ident	"@(#)gan_list.c	1.4	08/05/20 SMI"

/*
 * Generic doubly-linked list implementation
 *
 * Note the implementation is from the one used in Open Solaris with a few minor
 * modifications.
 */

#include <sys/types.h>
#include <sys/sysmacros.h>
#include <sys/debug.h>

#include "gan_list.h"
#include "gan_list_impl.h"

#define	gan_list_d2l(a, obj)\
	((gan_list_node_t *)(((char *)obj) + (a)->list_offset))
#define	gan_list_object(a, node) ((void *)(((char *)node) - (a)->list_offset))
#define	gan_list_empty(a) ((a)->list_head.list_next == &(a)->list_head)

#define	gan_list_insert_after_node(list, node, object) {	\
	gan_list_node_t *lnew = gan_list_d2l(list, object);	\
	lnew->list_prev = node;				\
	lnew->list_next = node->list_next;		\
	node->list_next->list_prev = lnew;		\
	node->list_next = lnew;				\
}

#define	gan_list_insert_before_node(list, node, object) {	\
	gan_list_node_t *lnew = gan_list_d2l(list, object);	\
	lnew->list_next = node;				\
	lnew->list_prev = node->list_prev;		\
	node->list_prev->list_next = lnew;		\
	node->list_prev = lnew;				\
}

void
gan_list_create(gan_list_t *list, size_t size, size_t offset)
{
	ASSERT(list);
	ASSERT(size > 0);
	ASSERT(size >= offset + sizeof (gan_list_node_t));

	list->list_size = size;
	list->list_offset = offset;
	list->list_head.list_next = list->list_head.list_prev =
	    &list->list_head;
}

void
gan_list_destroy(gan_list_t *list)
{
	gan_list_node_t *node = &list->list_head;

	ASSERT(list);
	ASSERT(list->list_head.list_next == node);
	ASSERT(list->list_head.list_prev == node);

	node->list_next = node->list_prev = NULL;
}

void
gan_list_insert_after(gan_list_t *list, void *object, void *nobject)
{
	gan_list_node_t *lold = gan_list_d2l(list, object);
	gan_list_insert_after_node(list, lold, nobject);
}

void
gan_list_insert_before(gan_list_t *list, void *object, void *nobject)
{
	gan_list_node_t *lold = gan_list_d2l(list, object);
	gan_list_insert_before_node(list, lold, nobject)
}

void
gan_list_insert_head(gan_list_t *list, void *object)
{
	gan_list_node_t *lold = &list->list_head;
	gan_list_insert_after_node(list, lold, object);
}

void
gan_list_insert_tail(gan_list_t *list, void *object)
{
	gan_list_node_t *lold = &list->list_head;
	gan_list_insert_before_node(list, lold, object);
}

void
gan_list_remove(gan_list_t *list, void *object)
{
	gan_list_node_t *lold = gan_list_d2l(list, object);
	ASSERT(!gan_list_empty(list));
	lold->list_prev->list_next = lold->list_next;
	lold->list_next->list_prev = lold->list_prev;
	lold->list_next = lold->list_prev = NULL;
}

void *
gan_list_head(gan_list_t *list)
{
	if (gan_list_empty(list))
		return (NULL);
	return (gan_list_object(list, list->list_head.list_next));
}

void *
gan_list_tail(gan_list_t *list)
{
	if (gan_list_empty(list))
		return (NULL);
	return (gan_list_object(list, list->list_head.list_prev));
}

void *
gan_list_next(gan_list_t *list, void *object)
{
	gan_list_node_t *node = gan_list_d2l(list, object);

	if (node->list_next != &list->list_head)
		return (gan_list_object(list, node->list_next));

	return (NULL);
}

void *
gan_list_prev(gan_list_t *list, void *object)
{
	gan_list_node_t *node = gan_list_d2l(list, object);

	if (node->list_prev != &list->list_head)
		return (gan_list_object(list, node->list_prev));

	return (NULL);
}

/*
 *  Insert src list after dst list. Empty src list thereafter.
 */
void
gan_list_move_tail(gan_list_t *dst, gan_list_t *src)
{
	gan_list_node_t *dstnode = &dst->list_head;
	gan_list_node_t *srcnode = &src->list_head;

	ASSERT(dst->list_size == src->list_size);
	ASSERT(dst->list_offset == src->list_offset);

	if (gan_list_empty(src))
		return;

	dstnode->list_prev->list_next = srcnode->list_next;
	srcnode->list_next->list_prev = dstnode->list_prev;
	dstnode->list_prev = srcnode->list_prev;
	srcnode->list_prev->list_next = dstnode;

	/* empty src list */
	srcnode->list_next = srcnode->list_prev = srcnode;
}
