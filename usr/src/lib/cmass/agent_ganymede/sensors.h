/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef __SENSORS_H
#define	__SENSORS_H

#pragma ident	"@(#)sensors.h	1.2	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <time.h>

#define	SENSOR_NAME_MAX		128

/*
 * The maximum length of a managed object name is 1032. In that way, a managed
 * object name can contain a path name (MAXPATHLEN = 64), and it is a multiple
 * of 8.
 */

#define	SENSOR_MNG_OBJ_MAX	1032
#define	SENSOR_KI_TYPE_MAX	64
#define	ERROR_STRING_MAX	256
#define	SENSOR_MO_TYPE_MAX	64

typedef struct {
	timespec_t	time_current;
	char		ki_type[SENSOR_KI_TYPE_MAX];
	char		managed_object[SENSOR_MNG_OBJ_MAX];
	uint64_t	usage;
	uint64_t	upperbound;
} sensor_result_t;

#ifdef __cplusplus
}
#endif

#endif /* __SENSORS_H */
