/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)Threshold.java 1.14     08/05/20 SMI"
 */

package com.sun.cluster.agent.ganymede;

import java.io.Serializable;


public class Threshold implements Serializable, Cloneable {

    private Double values[][][] = new Double[2][2][2];

    private Double currentValue;
    private int seqNum;
    private ThresholdStatus fallingStatus;
    private ThresholdStatus risingStatus;

    private String MOType;
    private String MOName;
    private String KIName;
    private String nodeName;

    /*
     * Constructor method, construct with null initial values
     */

    public Threshold() {
        seqNum = -1;
        MOType = null;
        MOName = null;
        KIName = null;
        nodeName = null;
        currentValue = null;
        fallingStatus = ThresholdStatus.UNKNOWN;
        risingStatus = ThresholdStatus.UNKNOWN;

        for (int direction = 0; direction < 2; direction++) {

            for (int severity = 0; severity < 2; severity++) {

                for (int field = 0; field < 2; field++) {
                    values[direction][severity][field] = null;
                }
            }
        }
    }


    public Object clone() {

        /*
         * String and Double as weel as ThresholdStatus
         * are immutables. So there is no need to call clone()
         */
        Threshold t = new Threshold();
        t.MOType = this.MOType;
        t.MOName = this.MOName;
        t.KIName = this.KIName;
        t.nodeName = this.nodeName;
        t.seqNum = this.seqNum;

        t.fallingStatus = this.fallingStatus;
        t.risingStatus = this.risingStatus;

        t.currentValue = currentValue;

        for (int direction = 0; direction < 2; direction++) {

            for (int severity = 0; severity < 2; severity++) {

                for (int field = 0; field < 2; field++) {
                    t.values[direction][severity][field] =
                        this.values[direction][severity][field];
                }
            }
        }

        return (t);
    }


    public String getKIName() {
        return (KIName);
    }

    public void setKIName(String ki) {
        KIName = ki;
    }

    public String getMOType() {
        return (MOType);
    }

    public void setMOType(String mot) {
        MOType = mot;
    }

    public String getMOName() {
        return (MOName);
    }

    public boolean isClusterWide() {

        // a threshold is clusterwide if it doesn't have an associated node
        // name
        return ((nodeName == null) || (nodeName.length() == 0));
    }


    public void setMOName(String mo) {
        MOName = mo;
    }

    public String getNodeName() {
        return (nodeName);
    }

    public void setNodeName(String node) {
        nodeName = node;
    }

    public void setThresholdProperty(ThresholdDirection direction,
        ThresholdSeverity severity, ThresholdFields field, Double val) {

        if ((val != null) && (direction == ThresholdDirection.FALLING)) {
            val = new Double(val.doubleValue() * -1);
        }

        values[direction.getValue()][severity.getValue()][field.getValue()] =
            val;
    }

    public Double getThresholdProperty(ThresholdDirection direction,
        ThresholdSeverity severity, ThresholdFields field) {

        Double val = values[direction.getValue()][severity.getValue()][field
                .getValue()];

        if ((val != null) && (direction == ThresholdDirection.FALLING)) {
            val = new Double(val.doubleValue() * -1);
        }

        return (val);
    }

    public double getCurrentValue() {
        return (currentValue.doubleValue());
    }

    public Double getCurrentDoubleValue() {
        return (currentValue);
    }

    public void setCurrentValue(double value, int seqNum) {
        setCurrentValue(ThresholdDirection.RISING, value, seqNum);
        setCurrentValue(ThresholdDirection.FALLING, value, seqNum);
    }

    public ThresholdStatus getCurrentRisingStatus() {
        return (risingStatus);
    }

    public ThresholdStatus getCurrentFallingStatus() {
        return (fallingStatus);
    }

    public void setCurrentStatuses(ThresholdStatus rising,
        ThresholdStatus falling) {
        risingStatus = rising;
        fallingStatus = falling;
    }

    public int getCurrentSeqNum() {
        return (seqNum);
    }

    public ThresholdStatus[] reset(ThresholdDirection direction, int seqNum) {

        ThresholdStatus statusTab[] = new ThresholdStatus[2];

        this.seqNum = seqNum;
        this.currentValue = null;

        statusTab[1] = ThresholdStatus.UNKNOWN; // new status

        if (direction == ThresholdDirection.FALLING) {
            statusTab[0] = this.fallingStatus; // old status
            this.fallingStatus = ThresholdStatus.UNKNOWN;
        } else {
            statusTab[0] = this.risingStatus; // old status
            this.risingStatus = ThresholdStatus.UNKNOWN;
        }

        return (statusTab);
    }

    public ThresholdStatus[] setCurrentValue(ThresholdDirection direction,
        double cur, int newSeqNum) {

        /*
         * Set the current values.
         */

        seqNum = newSeqNum;
        currentValue = new Double(cur);

        /*
         * Update threshold status.
         */

        ThresholdStatus prevStatus = null;
        ThresholdStatus currStatus = null;
        ThresholdStatus statusTab[] = new ThresholdStatus[2];

        if (direction == ThresholdDirection.FALLING) {
            cur *= -1.;
            prevStatus = fallingStatus;
        } else {
            prevStatus = risingStatus;
        }

        // init current status to previous status
        currStatus = prevStatus;

        Double thresh;
        int i = direction.getValue();

        if ((prevStatus == ThresholdStatus.UNKNOWN) ||
                (prevStatus == ThresholdStatus.OK)) {

            /*
             * Previous status is either OK or UNKNOWN. Threshold status can
             * transition to FATAL, WARNING, or OK. It can remain OK, but cannot
             * remain UNKNOWN.
             */

            int j1 = ThresholdSeverity.FATAL.getValue();
            int j2 = ThresholdSeverity.WARNING.getValue();
            int k = ThresholdFields.VALUELIMIT.getValue();

            if (((thresh = values[i][j1][k]) != null) &&
                    (cur > thresh.doubleValue())) {

                // the fatal value limit has been crossed
                currStatus = ThresholdStatus.FATAL;
            } else if (((thresh = values[i][j2][k]) != null) &&
                    (cur > thresh.doubleValue())) {

                // the warning value limit has been crossed
                currStatus = ThresholdStatus.WARNING;
            } else {
                currStatus = ThresholdStatus.OK;
            }

        } else if (prevStatus == ThresholdStatus.WARNING) {

            /*
             * Previous status is WARNING. Threshold status can transition to
             * FATAL or OK. It can also remain WARNING.
             */

            int j1 = ThresholdSeverity.FATAL.getValue();
            int j2 = ThresholdSeverity.WARNING.getValue();
            int k1 = ThresholdFields.VALUELIMIT.getValue();
            int k2 = ThresholdFields.RESETVALUE.getValue();

            if (((thresh = values[i][j1][k1]) != null) &&
                    (cur > thresh.doubleValue())) {

                // there's a fatal value limit, and it's been crossed
                currStatus = ThresholdStatus.FATAL;
            } else {

                if ((thresh = values[i][j2][k2]) == null)
                    thresh = values[i][j2][k1];

                if ((thresh == null) || (cur < thresh.doubleValue()))
                    currStatus = ThresholdStatus.OK;
            }

        } else if (prevStatus == ThresholdStatus.FATAL) {

            /*
             * Previous status is FATAL. Threshold status can transition to
             * WARNING or OK. It can also remain FATAL.
             */

            int j1 = ThresholdSeverity.FATAL.getValue();
            int j2 = ThresholdSeverity.WARNING.getValue();
            int k1 = ThresholdFields.VALUELIMIT.getValue();
            int k2 = ThresholdFields.RESETVALUE.getValue();

            if ((thresh = values[i][j1][k2]) == null)
                thresh = values[i][j1][k1];

            if ((thresh == null) || (cur < thresh.doubleValue())) {
                currStatus = ThresholdStatus.WARNING;

                if ((thresh = values[i][j2][k2]) == null)
                    thresh = values[i][j2][k1];

                if ((thresh == null) || (cur < thresh.doubleValue()))
                    currStatus = ThresholdStatus.OK;
            }
        }

        if (direction == ThresholdDirection.FALLING) {
            fallingStatus = currStatus;
        } else {
            risingStatus = currStatus;
        }

        statusTab[0] = prevStatus;
        statusTab[1] = currStatus;

        return (statusTab);
    }

    public ThresholdStatus getCurrentStatus() {
        return ((risingStatus.getValue() > fallingStatus.getValue())
                ? risingStatus : fallingStatus);
    }

    public ThresholdStatus getCurrentStatus(ThresholdDirection direction) {

        if (direction == ThresholdDirection.FALLING) {
            return (fallingStatus);
        } else {
            return (risingStatus);
        }
    }

    public Double getWarningFallingThreshold() {
        Double val =
            values[ThresholdDirection.FALLING.getValue()][
                ThresholdSeverity.WARNING.getValue()][ThresholdFields.VALUELIMIT
                .getValue()];

        if (val != null) {
            return (new Double(val.doubleValue() * -1));
        }

        return (null);
    }

    public Double getWarningFallingReset() {
        Double val =
            values[ThresholdDirection.FALLING.getValue()][
                ThresholdSeverity.WARNING.getValue()][ThresholdFields.RESETVALUE
                .getValue()];

        if (val != null) {
            return (new Double(val.doubleValue() * -1));
        }

        return (null);
    }

    public Double getWarningRisingThreshold() {
        return
            (values[ThresholdDirection.RISING.getValue()][
                    ThresholdSeverity.WARNING.getValue()][
                    ThresholdFields.VALUELIMIT.getValue()]);
    }

    public Double getWarningRisingReset() {
        return
            (values[ThresholdDirection.RISING.getValue()][
                    ThresholdSeverity.WARNING.getValue()][
                    ThresholdFields.RESETVALUE.getValue()]);
    }

    public Double getFatalFallingThreshold() {
        Double val =
            values[ThresholdDirection.FALLING.getValue()][
                ThresholdSeverity.FATAL.getValue()][ThresholdFields.VALUELIMIT
                .getValue()];

        if (val != null) {
            return (new Double(val.doubleValue() * -1));
        }

        return (null);
    }

    public Double getFatalFallingReset() {
        Double val =
            values[ThresholdDirection.FALLING.getValue()][
                ThresholdSeverity.FATAL.getValue()][ThresholdFields.RESETVALUE
                .getValue()];

        if (val != null) {
            return (new Double(val.doubleValue() * -1));
        }

        return (null);
    }

    public Double getFatalRisingThreshold() {
        return
            (values[ThresholdDirection.RISING.getValue()][
                    ThresholdSeverity.FATAL.getValue()][
                    ThresholdFields.VALUELIMIT.getValue()]);
    }

    public Double getFatalRisingReset() {
        return
            (values[ThresholdDirection.RISING.getValue()][
                    ThresholdSeverity.FATAL.getValue()][
                    ThresholdFields.RESETVALUE.getValue()]);
    }

    public String toString() {
        return ("[" + MOType + ", " + MOName + ", " + KIName + ", " + nodeName +
                "]");
    }
}
