/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)PermanentStorage.java 1.9     08/12/03 SMI"
 *
 */

package com.sun.cluster.agent.ganymede;

import java.io.*;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.logging.Logger;


/**
 * This class is a singleton holding objects that needs to be saved and
 * recovered.  Objects are registered and then, at finalization, they are saved
 * in a file.  At restart the file is read. Object are stored in an hashtable
 * indexed by a string. If a string is used several time, the new object
 * replaces the old one
 */

public class PermanentStorage {

    /* singleton implementation */
    private static PermanentStorage _instance;

    /* logging facility */
    private static final String logTag = "PermanentStorage";
    private static Logger logger = Logger.getLogger(GanymedeModule.DOMAIN +
            "." + logTag);

    /* Hashtable containing the objects indexed by a string */
    private Hashtable data;

    /**
     * This empty function aims to show the constructor is private. This is
     * mandatory for a singleton
     */
    private PermanentStorage() {
    }


    /**
     * Initialize the object by creating the singleton
     */
    public static void _initialize() throws Exception {
        logger.entering(logTag, "_initialize");

        _instance = new PermanentStorage();
        _instance.initializeInternal();

        logger.exiting(logTag, "_initialize");
    }

    /**
     * Finalize the object (call to store to file)
     */
    public static void _finalize() throws Exception {
        logger.entering(logTag, "_finalize");

        _instance.finalizeInternal();
        _instance = null;

        logger.exiting(logTag, "_finalize");
    }


    /**
     * Return an object previously stored in this repository This is a class
     * method as the object is a singleton
     *
     * @param  theName  The name of the object to retrieve
     *
     * @return  the retrieved object
     */
    public static Object getObject(String theName) {
        return _instance.getObjectInternal(theName);
    }

    /**
     * Return an object previously stored in this repository This is a class
     * method as the object is a singleton
     *
     * @param  theName  The name of the object to retrieve
     * @param  theObject  the associated object
     */
    public static void putObject(String theName, Object theObject) {
        logger.finer("PermanentStorage : add entry " + theName);
        _instance.putObjectInternal(theName, theObject);
    }

    /**
     * remove the reference to a given object This is usefull to let the Garbage
     * COllector do its job
     *
     * @param  theName  The name of the object to remove
     */
    public static void removeObject(String theName) {
        _instance.removeObjectInternal(theName);
    }


    /**
     * Initialize the internal of this object
     */
    private void initializeInternal() {
        logger.entering(logTag, "initializeInternal");
        permanentlyRestore();
        logger.exiting(logTag, "initializeInternal");
    }

    /**
     * Finalize the internal of this object
     */
    private void finalizeInternal() {
        permanentlySave();
    }

    /**
     * Initialize the contents of this object. i.e read it from file.
     */
    private synchronized void permanentlyRestore() {
        logger.entering(logTag, "permanentlyRestore");

        String fileName = Config.getPermanentStorageFileName();
        logger.fine("permanentStorage filename is " + fileName);

        File file = new File(fileName);
        FileInputStream fis = null;

        data = null;

        if (!file.exists()) {
            logger.fine(fileName + " !found => no object to recover");
            data = new Hashtable();

            return;
        }

        if (!file.canRead()) {
            logger.warning(fileName +
                " exist but is !readable: no object restored");
            data = new Hashtable();

            return;
        }

        /* look if the file exist */
        try {
            fis = new FileInputStream(file);
        } catch (Throwable e) {

            /* File !found only */
            logger.fine(fileName +
                " problem whereas readable => no object restored");
            data = new Hashtable();

            return;
        }

        /* restore the content */
        try {
            ObjectInputStream ois = new ObjectInputStream(fis);
            data = (Hashtable) ois.readObject();
            file.delete();
            fis.close();
        } catch (Throwable e) {
            logger.warning(e.toString());
        }

        if (data == null)
            data = new Hashtable();

        for (Enumeration l = data.keys(); l.hasMoreElements();) {
            logger.finer("Restored key : " + l.nextElement());
        }

        logger.exiting(logTag, "permanentlyRestore");
    }

    /**
     * Save the content of the object to file.
     */
    private synchronized void permanentlySave() {
        String fileName = Config.getPermanentStorageFileName();
        File file = new File(fileName);
        FileOutputStream fos = null;

        if (data == null)
            return;

        for (Enumeration l = data.keys(); l.hasMoreElements();) {
            logger.finer("saved key : " + l.nextElement());
        }

        try {
            fos = new FileOutputStream(file);
        } catch (SecurityException e) {
            logger.severe("SECURITY EXCEPTION: Failed to create " + fileName +
                " => backlog measurements are lost");
            logger.throwing("PermanentStorage.java", "permanentlySave", e);

            return;

        } catch (FileNotFoundException e) {
            logger.severe("File Not Found EXCEPTION: Failed to create " +
                fileName + " => backlog measurements are lost");
            logger.throwing("PermanentStorage.java", "permanentlySave", e);

            return;

        } catch (Exception e) {

            /* File not found only */
            logger.severe("Failed to create " + fileName +
                " => backlog measurements are lost");

            return;
        }

        try {
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(data);
            oos.flush();
            fos.close();
        } catch (Exception e) {
            logger.throwing("PermanentStorage.java", "permanentlySave", e);
            logger.warning(e.toString());
        }
    }


    /**
     * [INTERNAL] effectively get an object associated with the parameter
     *
     * @param  theName  the name of the object to retrieve
     */
    private synchronized Object getObjectInternal(String theName) {

        if (data == null) {
            logger.severe("PermanentStorage : no internal data to get " +
                theName);

            return null;
        }

        logger.finer("PermanentStorage : get object for " + theName);

        return data.get(theName);
    }

    /**
     * [INTERNAL] effectively store an object associated with the parameter
     *
     * @param  theName  The name of the object to retrieve
     * @param  theObject  the associated object
     */
    private synchronized void putObjectInternal(String theName,
        Object theObject) {

        if (data == null) {
            logger.severe("PermanentStorage : no internal data to put " +
                theName);

            return;
        }

        if (data.containsKey(theName)) {
            logger.finer(theName +
                " is already present id Permanent Storage, replace it");
        }

        data.put(theName, theObject);
    }


    /**
     * [INTERNAL] remove the ref to the object associated with the parameter
     *
     * @param  theName  The name of the object to remove
     */
    private synchronized void removeObjectInternal(String theName) {

        if (data == null) {
            logger.severe("PermanentStorage : no internal data to remove " +
                theName);

            return;
        }

        if (!data.containsKey(theName)) {
            logger.finer(theName + " is not present. no remove needed");
        }

        data.remove(theName);
    }
}
