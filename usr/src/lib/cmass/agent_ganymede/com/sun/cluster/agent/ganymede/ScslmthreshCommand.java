/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ScslmthreshCommand.java 1.15     08/05/20 SMI"
 *
 */

package com.sun.cluster.agent.ganymede;

// JDK
import java.lang.Boolean;
import java.net.ConnectException;
import java.text.ParseException;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// JMX
import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;

// Cacao
import com.sun.cacao.commandstream.Command;
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.agent.JmxClient;

// CMASS
import com.sun.cluster.agent.node.NodeMBean;
import com.sun.cluster.common.TrustedMBeanModel;


/**
 * This class is the entry point from "cacaocsc". It is used to receive request
 * from a shell and returning answer (status of thresholds).
 */
public class ScslmthreshCommand implements Command {

    /* logging facility */
    private static final String logTag = "ScslmthreshCommand";
    private static Logger logger = Logger.getLogger(GanymedeModule.DOMAIN +
            "." + logTag);

    /*
     * This string is prefixing errors sent back to the CLI. This string
     * is detected when received and managed.
     */
    private static final String CLI_ERROR = "ERROR: ";
    private static final String PROTOCOL_ERROR = CLI_ERROR + "1";
    private static final String INTERNAL_ERROR = CLI_ERROR + "2";

    private MBeanServer mbeanServer;

    public ScslmthreshCommand(MBeanServer mbs) {
        mbeanServer = mbs;
    }


    /**
     * return the node part of a string This is usefull to return the nodename
     * when given a logical nodename in paramater logical nodename = "node:zone"
     */
    private String extractNodeName(String node) {
        int index = node.lastIndexOf(":");

        if (index == -1) {
            return (node);
        }

        return (node.substring(0, index));
    }


    private Vector getNodeList() {
        Vector nodes = new Vector();

        // Get the ObjectName Factory for Cluster CMASS Module
        ObjectNameFactory nodeOnf = new ObjectNameFactory(NodeMBean.class
                .getPackage().getName());

        // Get the Objectname Pattern For ClusterMBean Instances
        ObjectName nodeObjectName = nodeOnf.getObjectNamePattern(
                NodeMBean.class);

        Set nodeMBeanObjNames = mbeanServer.queryNames(nodeObjectName, null);

        // Iterate through the list and get the Nodenames
        Iterator i = nodeMBeanObjNames.iterator();

        while (i.hasNext()) {
            ObjectName nodeMBeanObjectName = (ObjectName) i.next();

            String nodeName = null;
            Boolean isOnline = Boolean.FALSE;

            try {
                nodeName = (String) mbeanServer.getAttribute(
                        nodeMBeanObjectName, "Name");

                isOnline = (Boolean) mbeanServer.getAttribute(
                        nodeMBeanObjectName, "Online");
            } catch (MBeanException e) {

                // ignore any error
                continue;
            } catch (AttributeNotFoundException e) {

                // ignore any error
                continue;
            } catch (InstanceNotFoundException e) {

                // ignore any error
                continue;
            } catch (ReflectionException e) {

                // ignore any error
                continue;
            }

            if (isOnline.equals(Boolean.TRUE)) {
                nodes.add(nodeName);
            }
        }

        return (nodes);
    }


    public int execute(java.lang.String arguments, java.io.InputStream in,
        java.io.OutputStream out, java.io.OutputStream err, java.util.Map env)
        throws java.lang.Exception {

        String option = null;
        String suboption = null;
        String kiname = null;
        String node = null;
        String moname = null;
        String motype = null;

        Vector monameFilter = new Vector();
        Vector motypeFilter = new Vector();
        Vector nodenameFilter = new Vector();
        Vector kinameFilter = new Vector();


        logger.config(env.toString());
        logger.config("ScslmthreshCommand arguments: " + arguments);


        StringTokenizer st = new StringTokenizer(arguments);

        try {

            while (st.hasMoreTokens()) {
                String current = st.nextToken();

                if (current.equals("-moname")) {

                    if (!st.hasMoreTokens()) {
                        err.write(PROTOCOL_ERROR.getBytes());
                        logger.severe("Missing argument for -moname option.");

                        return (Command.ERROR_CODE);
                    }

                    monameFilter.add(st.nextToken());

                    continue;
                }

                if (current.equals("-motype")) {

                    if (!st.hasMoreTokens()) {
                        err.write(PROTOCOL_ERROR.getBytes());
                        logger.severe("Missing argument for -motype option.");

                        return (Command.ERROR_CODE);
                    }

                    motypeFilter.add(st.nextToken());

                    continue;
                }

                if (current.equals("-nodename")) {

                    if (!st.hasMoreTokens()) {
                        err.write(PROTOCOL_ERROR.getBytes());
                        logger.severe("Missing argument for -nodename option.");

                        return (Command.ERROR_CODE);
                    }

                    nodenameFilter.add(st.nextToken());

                    continue;
                }

                if (current.equals("-kiname")) {

                    if (!st.hasMoreTokens()) {
                        err.write(PROTOCOL_ERROR.getBytes());
                        logger.severe("Missing argument for -kiname option.");

                        return (Command.ERROR_CODE);
                    }

                    kinameFilter.add(st.nextToken());

                    continue;
                }
            }
        } catch (Exception e) {
            err.write(INTERNAL_ERROR.getBytes());
            logger.warning(e.toString());
            logger.throwing(getClass().getName(), "execute", e);

            return (Command.ERROR_CODE);
        }

        // retrieving the current Classloader
        // We store it to be able to revert to that one at the end of the method
        ClassLoader savedClassLoader = Thread.currentThread()
            .getContextClassLoader();

        // Overloading the Classloader as a workaround for a JMX bug
        // By default, JMX uses the default classLoader...
        // That's not what we want.
        Thread.currentThread().setContextClassLoader(GanymedeModule.class
            .getClassLoader());

        // for each ONLINE node in the cluster

        Vector results = new Vector();
        Vector nodes = getNodeList();
        HashMap envProps = new HashMap();
        Iterator ite;

        ite = nodes.iterator();
        envProps.put(JMXConnectorFactory.DEFAULT_CLASS_LOADER,
            GanymedeModule.class.getClassLoader());
        envProps.put(JMXConnectorFactory.PROTOCOL_PROVIDER_CLASS_LOADER,
            GanymedeModule.class.getClassLoader());

        while (ite.hasNext()) {
            Vector result = new Vector();
            String nodeName = (String) ite.next();
            JMXConnector jmxConn = null;
            MBeanServerConnection mbsc = null;


            // Do not connect to nodes that are filtered out
            // Some logic is needed to still connect to nodes
            // even if we need info only related to zones on that node
            if (!nodenameFilter.isEmpty()) {
                boolean found = false;

                for (Enumeration e = nodenameFilter.elements();
                        e.hasMoreElements(); /* */) {
                    String n = extractNodeName((String) e.nextElement());

                    if (n.equals(nodeName)) {
                        found = true;

                        break;
                    }

                }

                if (found == false) {

                    // Skipping that node, not of interest for the user
                    logger.fine("Will not contact node " + nodeName);

                    continue;
                }
            }


            try {
                jmxConn = TrustedMBeanModel.getWellKnownConnector(nodeName,
                        envProps);
                mbsc = jmxConn.getMBeanServerConnection();

                ObjectNameFactory onf = new ObjectNameFactory(
                        GanymedeMBean.class.getPackage().getName());

                GanymedeMBean mbean = (GanymedeMBean) JmxClient.getMBeanProxy(
                        mbsc, onf, GanymedeMBean.class, null, false);

                if (mbean == null) {

                    // skip that node
                    continue;
                }

                logger.fine("start retrieving Thresholds for node " + nodeName);

                /* fetch all Thresholds (crossed or not) */
                result.addAll(mbean.fetchLocalCrossedThresholds(true));
            } catch (ConnectException e) {

                // CACAO might not be up
                logger.info("Unable to reach node " + nodeName + ", continue");

                continue;
            } catch (Exception e) {
                logger.warning(e.toString());
                logger.throwing(getClass().getName(), "execute", e);

                // reverting back to the classLoader we saved at
                // the beginning of the method
                Thread.currentThread().setContextClassLoader(savedClassLoader);

                return (Command.ERROR_CODE);
            }

            Iterator iterator = result.iterator();

            while (iterator.hasNext()) {
                Threshold thresh = (Threshold) iterator.next();

                if (thresh.isClusterWide()) {
                    thresh.setNodeName(nodeName);
                }
            }

            results.addAll(result);

        }

        try {

            ite = results.iterator();

            while (ite.hasNext()) {
                Threshold thresh = (Threshold) ite.next();
                StringBuffer oneLine = new StringBuffer();

                if (!monameFilter.isEmpty() &&
                        !monameFilter.contains(thresh.getMOName())) {

                    // that threshold is filtered out
                    logger.fine("Filtering out a threshold with moname = " +
                        thresh.getMOName());

                    continue;
                }

                if (!motypeFilter.isEmpty() &&
                        !motypeFilter.contains(thresh.getMOType())) {

                    // that threshold is filtered out
                    logger.fine("Filtering out a threshold with motype = " +
                        thresh.getMOType());

                    continue;
                }

                if (!kinameFilter.isEmpty() &&
                        !kinameFilter.contains(thresh.getKIName())) {

                    // that threshold is filtered out
                    logger.fine("Filtering out a threshold with kiname = " +
                        thresh.getKIName());

                    continue;
                }

                // need to check node and node:zone
                if (!nodenameFilter.isEmpty()) {
                    boolean found = false;

                    if (nodenameFilter.contains(thresh.getNodeName())) {

                        // easy case where filter is set on the
                        // same node[:zone] than the threshold
                        found = true;
                    } else {

                        // filter is set on node while threshold
                        // returns node:zone
                        String tn = extractNodeName(thresh.getNodeName());

                        for (Enumeration e = nodenameFilter.elements();
                                e.hasMoreElements(); /* */) {
                            String n = (String) e.nextElement();

                            if (tn.equals(n)) {
                                found = true;

                                break;
                            }
                        }
                    }

                    if (found == false) {
                        logger.fine("Filtering out a threshold with node = " +
                            thresh.getNodeName());

                        continue;
                    }
                }

                oneLine.append(thresh.getMOType()).append(" ");
                oneLine.append(thresh.getKIName()).append(" ");

                // we display "-" for the node field of thresholds associated to
                // objects of type MOTYPE_NODE
                if (thresh.getMOType().equals(Config.MOTYPE_NODE) ||
                        (thresh.getNodeName() == null))
                    oneLine.append("- ");
                else
                    oneLine.append(thresh.getNodeName()).append(" ");

                oneLine.append(thresh.getCurrentStatus()).append(" ");

                if (thresh.getCurrentDoubleValue() == null)
                    oneLine.append("- ");
                else
                    oneLine.append(thresh.getCurrentDoubleValue()).append(" ");

                if (thresh.getMOName() == null)
                    oneLine.append("-\n");
                else
                    oneLine.append(thresh.getMOName()).append("\n");

                out.write(oneLine.toString().getBytes());
            }

        } catch (Exception e) {
            err.write(INTERNAL_ERROR.getBytes());
            logger.warning(e.toString());
            logger.throwing(getClass().getName(), "execute", e);
            Thread.currentThread().setContextClassLoader(savedClassLoader);

            return (Command.ERROR_CODE);
        }

        // reverting back to the classLoader we saved at
        // the beginning of the method
        Thread.currentThread().setContextClassLoader(savedClassLoader);

        return (Command.SUCCESS_CODE);
    }
}
