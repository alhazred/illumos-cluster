/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)InsertWeeklyRequest.java 1.6     08/05/20 SMI"
 *
 */

package com.sun.cluster.agent.ganymede;


import java.io.Serializable; // needed to save backlog
import java.util.Vector;


/**
 * This class is defining a request to insert data in the DataBase.
 */
public class InsertWeeklyRequest extends WorkRequest {
    public Vector samples;
    public int seqNum;

    /**
     * Overload the constructor to force the definition of the fields
     *
     * @param  theSamples  data to be put in the DataBase
     * @param  theSeqNum  seqnum associated with the data
     */
    public InsertWeeklyRequest(Vector theSamples, int theSeqNum) {
        samples = theSamples;
        seqNum = theSeqNum;
    }

    public String getType() {
        return "INSERT_WEEKLY";
    }

}
