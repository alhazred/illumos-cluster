/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)GanymedeMBean.java 1.10     08/05/20 SMI"
 *
 */

package com.sun.cluster.agent.ganymede;

// JDK
import java.sql.Time;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

// Agent
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;


/**
 * This interface defines the management operations available for ganymede
 * modules.
 */
public interface GanymedeMBean {

    /**
     * This field is used internally by the CMASS agent to identify the TYPE of
     * the module.
     */
    public static final String TYPE = "ganymede";

    /*
     * KI related interfaces
     */

    public LinkedList fetchKIList(String moType, Boolean activated);

    public LinkedList fetchKIList(String moType);

    /*
     * Measures related interfaces
     */

    public MeasureSet[] fetchMeasures(MeasureId ids[], MeasureLevel level,
        Long begin, Long end, Long timeout) throws Exception,
        GanymedeTimeoutException;

    public MeasureSet[] fetchMeasures(MeasureId ids[], MeasureLevel level,
        Long begin, Long end) throws Exception, GanymedeTimeoutException;

    public MeasureSet[] fetchMeasures(MeasureId ids[], MeasureLevel level,
        Long timeout) throws Exception, GanymedeTimeoutException;

    public MeasureSet[] fetchMeasures(MeasureId ids[], MeasureLevel level)
        throws Exception, GanymedeTimeoutException;

    public MeasureSet[] fetchLastMeasures(MeasureId ids[], MeasureLevel level)
        throws Exception, GanymedeTimeoutException;

    public MeasureSet[] fetchLastMeasures(MeasureId ids[], MeasureLevel level,
        Long timeout) throws Exception, GanymedeTimeoutException;

    /*
     * Threshold related interfaces
     */

    public Threshold fetchThreshold(String mot, String ki, String mo,
        String node);

    public ExitStatus[] updateThreshold(String mot, String mo, String ki,
        String node, ThresholdDirection direction, ThresholdSeverity severity,
        Double valueLimit, Double resetValue) throws CommandExecutionException;

    public ExitStatus[] updateThreshold(String mot, String mo, String ki,
        String node, Double fallingWarningValueLimit,
        Double fallingWarningResetValue, Double fallingFatalValueLimit,
        Double fallingFatalResetValue, Double risingWarningValueLimit,
        Double risingWarningResetValue, Double risingFatalValueLimit,
        Double risingFatalResetValue) throws CommandExecutionException;

    public Vector fetchLocalCrossedThresholds(boolean all) throws Exception;
}
