/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)Ganymede.java 1.15     08/05/20 SMI"
 *
 */

package com.sun.cluster.agent.ganymede;

// JDK
import java.lang.Boolean;
import java.lang.IllegalArgumentException;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

// JMX
import javax.management.MBeanServerInvocationHandler;
import javax.management.MBeanException;

// cacao
import com.sun.cacao.ModuleMBean;
import com.sun.cacao.agent.JmxClient;
import com.sun.cacao.element.AdministrativeStateEnum;
import com.sun.cacao.element.OperationalStateEnum;
import com.sun.cacao.invocation.InvocationException;
import com.sun.cacao.invocation.InvocationStatus;
import com.sun.cacao.invocation.InvokeCommand;

// Agent
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.common.ClusterPaths;



/**
 * Ganymede module.
 */
public class Ganymede implements GanymedeMBean {

    private static final String logTag = "Ganymede";
    private static Logger logger = Logger.getLogger(GanymedeModule.DOMAIN +
            "." + logTag);

    private static final String STR_VALUE = "value";
    private static final String STR_REARM = "rearm";
    private static final String STR_DIRECTION = "direction";
    private static final String STR_SEVERITY = "severity";
    private static final String STR_SETTHRESHOLD = "set-threshold";

    public LinkedList fetchKIList(String moType, Boolean enabled) {
        return SensorBase.fetchKIList(moType, enabled);
    }

    public LinkedList fetchKIList(String moType) {
        return SensorBase.fetchKIList(moType, null);
    }

    public MeasureSet[] fetchMeasures(MeasureId ids[], MeasureLevel level,
        Long begin, Long end, Long timeout) throws Exception,
        GanymedeTimeoutException {

        return (DataBase.getData(ids, level, begin, end, timeout));
    }

    public MeasureSet[] fetchMeasures(MeasureId ids[], MeasureLevel level,
        Long begin, Long end) throws Exception, GanymedeTimeoutException {

        return (DataBase.getData(ids, level, begin, end, null));
    }

    public MeasureSet[] fetchMeasures(MeasureId ids[], MeasureLevel level,
        Long timeout) throws Exception, GanymedeTimeoutException {

        return (DataBase.getData(ids, level, null, null, timeout));
    }

    public MeasureSet[] fetchMeasures(MeasureId ids[], MeasureLevel level)
        throws Exception, GanymedeTimeoutException {

        return (DataBase.getData(ids, level, null, null, null));
    }

    public MeasureSet[] fetchLastMeasures(MeasureId ids[], MeasureLevel level,
        Long timeout) throws Exception, GanymedeTimeoutException {

        return (DataBase.getLatestData(ids, level, timeout));
    }

    public MeasureSet[] fetchLastMeasures(MeasureId ids[], MeasureLevel level)
        throws Exception, GanymedeTimeoutException {

        return (DataBase.getLatestData(ids, level, null));
    }

    public Vector fetchLocalCrossedThresholds(boolean all) throws Exception {
        return (ThresholdBase.fetchLocalCrossedThresholds(all));
    }

    public Threshold fetchThreshold(String mot, String ki, String mo,
        String node) {
        return (ThresholdBase.getThreshold(mot, ki, mo, node));
    }

    private String[] generateUpdateThresholdCommand(String mot, String mo,
        String ki, String node, ThresholdDirection direction,
        ThresholdSeverity severity, Double valueLimit, Double resetValue) {

        String commands[] = null;
        StringBuffer nodeOpt = new StringBuffer();
        StringBuffer directionOpt = new StringBuffer();
        StringBuffer severityOpt = new StringBuffer();
        StringBuffer valueOpt = new StringBuffer();
        StringBuffer rearmOpt = new StringBuffer();

        try {
            directionOpt = directionOpt.append(STR_DIRECTION).append("=")
                .append(ThresholdDirection.getEnumString(direction));

            severityOpt = severityOpt.append(STR_SEVERITY).append("=").append(
                    ThresholdSeverity.getEnumString(severity));
        } catch (Exception e) {
            // ignore exception related to getEnumString
        }

        valueOpt = valueOpt.append(STR_VALUE).append("=");

        if (valueLimit != null) {
            valueOpt = valueOpt.append(valueLimit.toString());
        }

        rearmOpt = rearmOpt.append(STR_REARM).append("=");

        if (resetValue != null) {
            rearmOpt = rearmOpt.append(resetValue.toString());
        }

        if ((node == null) || (node.length() == 0)) {
            commands = new String[] {
                    ClusterPaths.CL_TELEMETRY_ATTR_CMD, STR_SETTHRESHOLD, "-t",
                    mot, "-b", mo, "-p", directionOpt.toString(), "-p",
                    severityOpt.toString(), "-p", valueOpt.toString(), "-p",
                    rearmOpt.toString(), ki
                };
        } else {

            // Case where node is provided
            commands = new String[] {
                    ClusterPaths.CL_TELEMETRY_ATTR_CMD, STR_SETTHRESHOLD, "-t",
                    mot, "-b", mo, "-p", directionOpt.toString(), "-p",
                    severityOpt.toString(), "-p", valueOpt.toString(), "-p",
                    rearmOpt.toString(), "-n", node, // Node param here
                    ki
                };
        }

        return (commands);
    }

    private boolean needUpdate(Double oldVal, Double newVal, Double oldRes,
        Double newRes) {

        if (((newVal != null) && (oldVal == null)) ||
                ((newVal == null) && (oldVal != null)) ||
                ((newVal != null) && (oldVal != null) &&
                    (newVal.doubleValue() != oldVal.doubleValue()))) {

            return true;
        }

        return (((newRes != null) && (oldRes == null)) ||
                ((newRes == null) && (oldRes != null)) ||
                ((newRes != null) && (oldRes != null) &&
                    (newRes.doubleValue() != oldRes.doubleValue())));
    }

    public ExitStatus[] updateThreshold(String mot, String mo, String ki,
        String node, ThresholdDirection direction, ThresholdSeverity severity,
        Double valueLimit, Double resetValue) throws CommandExecutionException {

        // get the threshold associated with (mot, ki, mo, node)
        Threshold thresh = ThresholdBase.getThreshold(mot, ki, mo, node);

        Double oldVal = null;
        Double oldRes = null;

        if (thresh != null) {
            oldVal = thresh.getThresholdProperty(direction, severity,
                    ThresholdFields.VALUELIMIT);
            oldRes = thresh.getThresholdProperty(direction, severity,
                    ThresholdFields.RESETVALUE);
        }

        if (!needUpdate(oldVal, valueLimit, oldRes, resetValue)) {
            IllegalArgumentException e = new IllegalArgumentException(
                    "No threshold update needed");
            logger.throwing(getClass().getName(), "updateThreshold", e);
            throw e;
        }

        String commands[][] = new String[1][];
        InvocationStatus exits[];

        commands[0] = generateUpdateThresholdCommand(mot, mo, ki, node,
                direction, severity, valueLimit, resetValue);

        // Run the command(s)
        try {
            exits = InvokeCommand.execute(commands, null);
        } catch (InvocationException ie) {

            exits = ie.getInvocationStatusArray();

            String strStderr = exits[0].getStderr();
            CommandExecutionException ce = new CommandExecutionException(ie
                    .getMessage() + "---\n" + strStderr,
                    ExitStatus.createArray(exits));
            logger.throwing(getClass().getName(), "updateThreshold", ce);
            throw ce;
        }

        return ExitStatus.createArray(exits);
    }

    public ExitStatus[] updateThreshold(String mot, String mo, String ki,
        String node, Double fallingWarningValueLimit,
        Double fallingWarningResetValue, Double fallingFatalValueLimit,
        Double fallingFatalResetValue, Double risingWarningValueLimit,
        Double risingWarningResetValue, Double risingFatalValueLimit,
        Double risingFatalResetValue) throws CommandExecutionException {

        LinkedList commandList = new LinkedList();
        Double oldVal;
        Double newVal;
        Double oldRes;
        Double newRes;

        // get the threshold associated with (mot, ki, mo, node)
        Threshold thresh = ThresholdBase.getThreshold(mot, ki, mo, node);

        // check falling warning
        oldVal = null;
        oldRes = null;

        if (thresh != null) {
            oldVal = thresh.getWarningFallingThreshold();
            oldRes = thresh.getWarningFallingReset();
        }

        newVal = fallingWarningValueLimit;
        newRes = fallingWarningResetValue;

        if (needUpdate(oldVal, newVal, oldRes, newRes)) {
            String command[] = generateUpdateThresholdCommand(mot, mo, ki, node,
                    ThresholdDirection.FALLING, ThresholdSeverity.WARNING,
                    newVal, newRes);

            commandList.add(command);
        }

        // check falling fatal
        oldVal = null;
        oldRes = null;

        if (thresh != null) {
            oldVal = thresh.getFatalFallingThreshold();
            oldRes = thresh.getFatalFallingReset();
        }

        newVal = fallingFatalValueLimit;
        newRes = fallingFatalResetValue;

        if (needUpdate(oldVal, newVal, oldRes, newRes)) {
            String command[] = generateUpdateThresholdCommand(mot, mo, ki, node,
                    ThresholdDirection.FALLING, ThresholdSeverity.FATAL, newVal,
                    newRes);

            commandList.add(command);
        }

        // check rising warning
        oldVal = null;
        oldRes = null;

        if (thresh != null) {
            oldVal = thresh.getWarningRisingThreshold();
            oldRes = thresh.getWarningRisingReset();
        }

        newVal = risingWarningValueLimit;
        newRes = risingWarningResetValue;

        if (needUpdate(oldVal, newVal, oldRes, newRes)) {
            String command[] = generateUpdateThresholdCommand(mot, mo, ki, node,
                    ThresholdDirection.RISING, ThresholdSeverity.WARNING,
                    newVal, newRes);

            commandList.add(command);
        }

        // check rising fatal
        oldVal = null;
        oldRes = null;

        if (thresh != null) {
            oldVal = thresh.getFatalRisingThreshold();
            oldRes = thresh.getFatalRisingReset();
        }

        newVal = risingFatalValueLimit;
        newRes = risingFatalResetValue;

        if (needUpdate(oldVal, newVal, oldRes, newRes)) {
            String command[] = generateUpdateThresholdCommand(mot, mo, ki, node,
                    ThresholdDirection.RISING, ThresholdSeverity.FATAL, newVal,
                    newRes);

            commandList.add(command);
        }

        if (commandList.isEmpty()) {
            return null;
        }

        // convert the list of commands to an array
        String commands[][] = new String[commandList.size()][];
        ListIterator iter = commandList.listIterator(0);

        while (iter.hasNext()) {
            int index = iter.nextIndex();
            String command[] = (String[]) iter.next();
            commands[index] = command;
        }

        InvocationStatus exits[];

        // Run the command(s)
        try {
            exits = InvokeCommand.execute(commands, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();

            StringBuffer strStderr = new StringBuffer("");

            for (int i = 0; i < exits.length; i++) {
                strStderr.append(exits[i].getStderr() + "\n");
            }

            CommandExecutionException ce = new CommandExecutionException(ie
                    .getMessage() + "---\n" + strStderr,
                    ExitStatus.createArray(exits));
            logger.throwing(getClass().getName(), "updateThreshold", ce);
            throw ce;
        }

        return ExitStatus.createArray(exits);
    }
}
