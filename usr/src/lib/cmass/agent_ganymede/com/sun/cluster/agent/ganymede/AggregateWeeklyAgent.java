/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)AggregateWeeklyAgent.java 1.10     08/05/20 SMI"
 *
 */

package com.sun.cluster.agent.ganymede;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import java.util.logging.Logger;


/**
 * This class is implementing the abstraction of data (hourly -> weekly)
 */
public class AggregateWeeklyAgent extends WorkAgent {

    private static AggregateWeeklyAgent _instance;
    private static final int NB_SLICES_PER_DAY = 8;
    private static final int NB_SECONDS_PER_DAY = 86400; /* 24 * 60 * 60 */

    private static final String logTag = "AggregateWeeklyAgent";
    private static Logger logger = Logger.getLogger(GanymedeModule.DOMAIN +
            "." + logTag);

    private Hashtable uniqueIds;
    private int lastSlice = 0; // slice number of last samples


    private AggregateWeeklyAgent() throws Exception {
        super("AggregateWeeklydata");

        int seqNumInBacklog;
        int seqNumInDataBase;

        Integer value = (Integer) PermanentStorage.getObject("WeeklySeqNum");
        seqNumInBacklog = (value == null) ? 0 : value.intValue();

        logger.finer("Weekly SeqNum from BackLog is " + seqNumInBacklog);

        seqNumInDataBase = DataBase.getSeqNum(MeasureLevel.WEEKLY);
        logger.finer("Weekly SeqNum from DataBase is " + seqNumInDataBase);

        lastSlice = ((seqNumInDataBase > seqNumInBacklog) ? seqNumInDataBase
                                                          : seqNumInBacklog);
    }

    public static void passRequest(WorkRequest wr) {
        _instance.insertRequest(wr);
    }

    /*
     * Users interact with the AggregateWeeklyAgent singleton by means of the
     * following class methods.
     *
     * These class methods are executed outside the AggregateWeeklyAgent thread,
     * so special care must taken to nicely managed the concurrency. The class
     * methods must not do update/delete/insert type transactions to the DB
     * because two threads could end up "commiting" at the same time which would
     * lead to unpredictable results. The class methods must not try to
     * reconnect to the DB on connection loss because two threads could end up
     * reconnecting at the same time, which would result in reconnecting twice.
     * And the class methods must handle the connection = null case which may
     * occur if the AggregateWeeklyAgent thread has terminated.
     */

    public static void _initialize() throws Exception {
        logger.finer("Entering _initialize");

        _instance = new AggregateWeeklyAgent();
        _instance.initializeInternal();
        _instance.start(); // start thread

        logger.finer("Exiting _initialize");
    }

    public static void _finalize() {
        logger.finer("Entering _finalize");

        // Check if already finalized, this can happen if the Ganymede module is
        // locked as a result of failure at unlock time
        if (_instance == null)
            return;

        // Currently the AggregateWeeklyAgent class doesn't call
        // Config.lockModule(), so the following comment doesn't apply as of
        // today. Leave it there as it may well apply in a near future.

        // We may have come in here as the result of calling lockModule(), we
        // may therefore be executing in the Agent thread. In that case, we
        // don't need to request a stop and we must not try to join ourself.
        if (Thread.currentThread() != _instance)
            _instance.finalizeInternal();

        _instance = null;

        logger.finer("Exiting _finalize");
    }


    /**
     * this number is used to give a slice number to a sample. if as new slice
     * is started by a sample, the weekly data must be flushed The slice number
     * is going to be used as the seqnum for the averaged data
     */
    private int getTimeSlice(long time) {
        return (int) (NB_SLICES_PER_DAY * time / NB_SECONDS_PER_DAY);
    }


    private boolean isToBeFlushed(long minTimeStamp, long maxTimeStamp) {
        int sliceOfMin = getTimeSlice(minTimeStamp);
        boolean result = false;

        if (sliceOfMin != lastSlice) {
            lastSlice = sliceOfMin;
            result = true;
        }

        return (result);
    }


    private void flush() {
        InsertWeeklyRequest wr;
        Vector result = new Vector();

        for (Enumeration l = uniqueIds.keys(); l.hasMoreElements();) {
            String Id = (String) l.nextElement();
            AggregateData data = (AggregateData) uniqueIds.get(Id); // proj list

            if (data.nbSamples == 0)
                continue;

            AggregateResult newRes = new AggregateResult();

            newRes.mo = data.mo;
            newRes.mot = data.mot;
            newRes.ki = data.ki;
            newRes.lnode = data.lnode;
            newRes.time_sec = data.time_sec;
            newRes.value = data.value;
            newRes.upperBound = data.upperBound;
            newRes.min = data.min;
            newRes.max = data.max;
            newRes.avg = (long) (data.sumX / data.nbSamples);
            newRes.dev = (long) Math.sqrt((data.sumX2 / data.nbSamples) -
                    ((data.sumX / data.nbSamples) *
                        (data.sumX / data.nbSamples)));
            data.clean();

            result.add(newRes);
        }

        wr = new InsertWeeklyRequest(result, lastSlice);
        PermanentStorage.putObject("WeeklySeqNum", new Integer(lastSlice));

        // pass the weekly request to the DB agent
        DataBase.passRequest(wr);
    }


    /**
     * Run thread
     */
    public void run() {

        logger.fine("AggregateWeeklyAgent thread starts");

        /*
         * Main loop.
         */
        while (true) {
            WorkRequest w = null;

            try {
                waitForWork();
            } catch (Exception e) {
            }

            if (isStopPending()) {
                logger.fine("AggregateWeeklyAgent thread terminated");

                return;
            }

            try {
                w = popRequest();

                if (w.getType().equals("INSERT_HOURLY"))
                    doJob((InsertHourlyRequest) w);

            } catch (Exception e) {
            }

            if (w == null) {
                logger.warning("AggregateWeeklyAgent woken up though " +
                    "workQ is empty!");

                continue;
            }

            // pass the weekly request to the DB agent
            DataBase.passRequest(w);
        }

        // we never get here
    }


    private AggregateData getUniqueId(String ki, String mot, String mo,
        String lnode) {

        AggregateData returnedValue;

        // build the unique string (lnode may be null)
        String uniqueString = ki + "##" + mot + "##" + mo + "##";

        if (lnode != null)
            uniqueString += lnode;

        returnedValue = (AggregateData) uniqueIds.get(uniqueString);

        if (returnedValue == null) {
            returnedValue = new AggregateData(ki, mot, mo, lnode);
            uniqueIds.put(uniqueString, returnedValue);
        }

        return returnedValue;
    }


    private void doJob(InsertHourlyRequest w) {
        Vector samples = w.samples;
        long minTimeStamp = 0;
        long maxTimeStamp = 0;

        for (Enumeration e = samples.elements(); e.hasMoreElements();) {
            SensorResult sr = (SensorResult) e.nextElement();

            String kiName = sr.getKeyIndicatorName();
            String moTypeName = sr.getMOType();
            String moName = sr.getMonitoredEntity();
            String logicalNodeName = sr.getLogicalNodeName();
            AggregateData ad = getUniqueId(kiName, moTypeName, moName,
                    logicalNodeName);
            long sampleTime = sr.getDate().getTime() / 1000;


            ad.addSample(sampleTime, sr.getValue().longValue(),
                sr.getUpperBound().longValue());

            if (minTimeStamp == 0) {
                maxTimeStamp = minTimeStamp = sampleTime;
            } else {

                if (sampleTime < minTimeStamp)
                    minTimeStamp = sampleTime;

                if (sampleTime > maxTimeStamp)
                    maxTimeStamp = sampleTime;
            }
        }

        if (isToBeFlushed(minTimeStamp, maxTimeStamp))
            flush();
    }


    private void initializeInternal() throws Exception {
        uniqueIds = new Hashtable();
    }

    private void finalizeInternal() {

        // request the Agent thread to stop
        requestStop();

        // wait for thread termination
        try {
            join();
        } catch (InterruptedException e) {
            logger.warning(e.toString());
        }

        logger.fine("AggregateWeeklyAgent thread joined");
    }

    private class AggregateData {
        String ki; // key indicator name
        String mot; // managed object type name
        String mo; // managed object name
        String lnode; // logical node name
        public long time_sec; // entry time
        public long value; // entry vlue
        public long upperBound;
        public long min;
        public long max;
        public double sumX;
        public double sumX2;
        public int nbSamples;

        public AggregateData(String theKI, String theMOT, String theMO,
            String theLNode) {

            ki = theKI;
            mo = theMO;
            mot = theMOT;
            lnode = theLNode;

            clean();
        }

        public void clean() {
            value = min = max = upperBound = 0;
            sumX = sumX2 = 0;
            time_sec = nbSamples = 0;
        }

        public void addSample(long theTime, long theValue, long theUpperBound) {

            if (time_sec == 0) {
                time_sec = theTime;
                min = max = value = theValue;
                upperBound = theUpperBound;
            } else {

                if (theValue < min)
                    min = theValue;
                else if (theValue > max)
                    max = theValue;

                if (theUpperBound > upperBound)
                    upperBound = theUpperBound;
            }

            sumX += theValue;
            sumX2 += theValue * theValue;
            nbSamples++;
        }
    }

}
