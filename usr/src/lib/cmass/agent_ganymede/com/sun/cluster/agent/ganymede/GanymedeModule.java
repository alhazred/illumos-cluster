/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)GanymedeModule.java 1.11     08/12/03 SMI"
 *
 */

package com.sun.cluster.agent.ganymede;

// JDK
import java.sql.Time;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

// JMX
import javax.management.ObjectName;
import javax.management.MBeanServer;

// CACAO
import com.sun.cacao.DeploymentDescriptor;
import com.sun.cacao.Module;
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.commandstream.CommandStreamAdaptorModule;
import com.sun.cacao.element.AdministrativeStateEnum;
import com.sun.cacao.element.AvailabilityStatusEnum;
import com.sun.cacao.element.OperationalStateEnum;

// Local
import com.sun.cluster.common.ClusterRuntimeException;


/**
 * Ganymede module using the interceptor and JNI design patterns
 */
public class GanymedeModule extends Module {

    // Tracing utility
    public static final String DOMAIN = "com.sun.cluster.agent.ganymede";
    private static final String logTag = "GanymedeModule";
    private static Logger logger = Logger.getLogger(DOMAIN + "." + logTag);
    private static final String scslmadmCommandStream = "scslmadmCmdStream";
    private static final String scslmthreshCommandStream =
        "scslmthreshCmdStream";

    private boolean scslmadmCommandRegistered;

    private boolean scslmthreshCommandRegistered;

    private ObjectName ganymedeObjectName = null;
    private boolean ganymedeMBeanRegistered;

    /**
     * Constructor called by the container.
     *
     * @param  descriptor
     */
    public GanymedeModule(DeploymentDescriptor descriptor) {
        super(descriptor);

        logger.entering(logTag, "GanymedeModule");

        Map parameters = descriptor.getParameters();
        String library = null;

        try {

            // Load the agent JNI library
            library = (String) parameters.get("library");
            Runtime.getRuntime().load(library);
            logger.fine("library " + library + " loaded");

        } catch (UnsatisfiedLinkError ule) {
            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            availabilityStatusSetAdd(AvailabilityStatusEnum.NOT_INSTALLED);
            setOperationalState(OperationalStateEnum.DISABLED);
            logger.warning("Unable to load native runtime library" + ule);
            throw ule;
        }

        logger.exiting(logTag, "GanymedeModule");
    }

    /**
     * Start the module
     */
    protected void start() throws ClusterRuntimeException {

        // set initial state of the scslm commands and ganymede mbean
        scslmadmCommandRegistered = false;
        scslmthreshCommandRegistered = false;
        ganymedeMBeanRegistered = false;

        try {

            // Note: initilization order matters

            // intialize permanent storage (must be first)
            logger.fine("initialize Permanent Storage");
            PermanentStorage._initialize();

            // initialize sensor base
            logger.fine("initialize sensor base");
            SensorBase._initialize();

            // initialize threshold base
            logger.fine("initialize threshold base");
            ThresholdBase._initialize();

            // intialize config
            logger.fine("initialize config");
            Config._initialize(this);

            // init database
            logger.fine("initialize database");
            DataBase._initialize();

            // init aggregate agent
            logger.fine("initialize AggregateWeeklyAgent");
            AggregateWeeklyAgent._initialize();

            // start scheduling
            logger.fine("initialize scheduler");
            GanSched._initialize();

            // create and register scslmadm command
            logger.fine("register scslmadm Ganymede Command");

            ScslmadmCommand scslmadmCommand = new ScslmadmCommand(getMbs());
            CommandStreamAdaptorModule.registerCommand(getDomainName(),
                scslmadmCommandStream, scslmadmCommand);
            scslmadmCommandRegistered = true;

            // create and register scslmthresh command
            logger.fine("register scslmthresh Ganymede Command");

            ScslmthreshCommand scslmthreshCommand = new ScslmthreshCommand(
                    getMbs());
            CommandStreamAdaptorModule.registerCommand(getDomainName(),
                scslmthreshCommandStream, scslmthreshCommand);
            scslmthreshCommandRegistered = true;

            // create and register Ganymede MBean
            logger.fine("create and register GanymedeMBean");

            ObjectNameFactory objectNameFactory = new ObjectNameFactory(
                    getDomainName());
            ganymedeObjectName = objectNameFactory.getObjectName(
                    GanymedeMBean.class, null);

            GanymedeMBean ganymedeMBean = new Ganymede();
            getMbs().registerMBean(ganymedeMBean, ganymedeObjectName);
            ganymedeMBeanRegistered = true;

        } catch (Exception e) {

            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            setOperationalState(OperationalStateEnum.DISABLED);

            // other cmas modules generate ClusterRuntimeException on
            // start()/stop() failure, although any Exception will be caught by
            // the CACAO framework, we do the same here
            ClusterRuntimeException cre = new ClusterRuntimeException(e
                    .getMessage());
            logger.throwing(getClass().getName(), "start", cre);
            throw cre;
        }
    }

    /**
     * Stop the module
     */
    protected void stop() {

        try {

            // Note: finalization order matters

            // unregister Ganymede MBean
            if (ganymedeMBeanRegistered) {
                logger.fine("unregister GanymedeMBean");
                getMbs().unregisterMBean(ganymedeObjectName);
            }

            // unregister scslmthresh Ganymede command
            if (scslmthreshCommandRegistered) {
                logger.fine("unregister scslmthresh Ganymede command");
                CommandStreamAdaptorModule.unregisterCommand(getDomainName(),
                    scslmthreshCommandStream);
            }

            // unregister scslmadm Ganymede command
            if (scslmadmCommandRegistered) {
                logger.fine("unregister scslmadm Ganymede command");
                CommandStreamAdaptorModule.unregisterCommand(getDomainName(),
                    scslmadmCommandStream);
            }

            // finalize scheduler
            logger.fine("finalize scheduler");
            GanSched._finalize();

            // finalize abstract agent
            logger.fine("finalize AggregateWeeklyAgent");
            AggregateWeeklyAgent._finalize();

            // finalize database
            logger.fine("finalize database");
            DataBase._finalize();

            // finalize sensor base
            logger.fine("finalize threshold base");
            ThresholdBase._finalize();

            // finalize sensor base
            logger.fine("finalize sensor base");
            SensorBase._finalize();

            // finalize config
            logger.fine("finalize config");
            Config._finalize();

            // finalize Permanent Storage
            logger.fine("finalize permanent storage");
            PermanentStorage._finalize();

            logger.fine("finalize Ganymede => DONE");

        } catch (Exception e) {

            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            setOperationalState(OperationalStateEnum.DISABLED);

            // other cmas modules generate ClusterRuntimeException on
            // start()/stop() failure, although any Exception will be caught by
            // the CACAO framework, we do the same here
            ClusterRuntimeException cre = new ClusterRuntimeException(e
                    .getMessage());
            logger.throwing(getClass().getName(), "stop", cre);
            throw cre;
        }
    }

    /**
     * Set the administrative state of the Ganymede module to LOCKED. This
     * method is called from the method Config.java:lockModule() which is to be
     * called in case of internal and/or fatal error.
     */
    public void lockModule() {
        setAdministrativeState(AdministrativeStateEnum.LOCKED);
    }

    /**
     * Returns the MBeanServer
     */
    public MBeanServer getMBeanServer() {
        return getMbs();
    }
}
