/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)WorkQueue.java 1.6     08/05/20 SMI"
 *
 */

package com.sun.cluster.agent.ganymede;

import com.sun.cluster.agent.ganymede.PermanentStorage;
import com.sun.cluster.agent.ganymede.WorkRequest;

import java.util.LinkedList;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.logging.Logger;


/**
 * This class is defining a queue receiving WorkRequest. This queue is member of
 * a WorkAgent.
 */

public class WorkQueue {

    // logging facility
    private static final String logTag = "WorkQueue";
    private static Logger logger = Logger.getLogger(GanymedeModule.DOMAIN +
            "." + logTag);

    /**
     * limit of the queue size. In fact, the size of the queue can be WQLIMIT+1,
     * because WorkAgent may requeue a WorkRequest that has not be correctly
     * processed. Obviously, in case of requeue, the limit is not tested
     */
    private static final int WQLIMIT = 20;

    /* This queue is holding the request to send to database */
    private LinkedList queue;

    /**
     * Constructor of the queue. The constructor restores the contents of the
     * queue from permanent storage
     *
     * @param  theName  The name identifying the queue uniquely
     */
    public WorkQueue(String theName) {
        queue = (LinkedList) PermanentStorage.getObject(theName);

        if (queue == null)
            queue = new LinkedList();
        else {
            logger.fine("restored queue size for " + theName + " is :" +
                queue.size());
        }

        PermanentStorage.putObject(theName, queue);
    }

    /**
     * returns if the queue is empty.
     */
    public synchronized boolean isEmpty() {
        return (queue.isEmpty());
    }


    /**
     * returns a pending request, or null if none
     */
    public synchronized WorkRequest popRequest() {
        WorkRequest toBeReturned;

        try {
            toBeReturned = (WorkRequest) queue.removeLast();
        } catch (NoSuchElementException e) {
            toBeReturned = null;
        }

        return toBeReturned;
    }


    /**
     * requeue a WorkeRequest that can not be processed. The WorkRequest will be
     * the first one to be poped
     *
     * @param  w  the request to be requeued
     */
    public synchronized void requeueRequest(WorkRequest w) {
        queue.addLast(w);
        logger.fine("requeue one elem - new size is :" + queue.size());
    }

    /**
     * put a new request in the queue. the new request is inserted at the tail
     * of the queue
     *
     * @param  w  the request to be queued
     */
    public synchronized void pushRequest(WorkRequest w) {

        if (queue.size() < WQLIMIT) {
            queue.addFirst(w);
        } else {
            logger.warning("Work Request Queue limit reached");
        }
    }
}
