/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)AggregateResult.java 1.6     08/05/20 SMI"
 *
 */

package com.sun.cluster.agent.ganymede;

/**
 * This class is construct by AggregateAgent and inserted in database
 */
class AggregateResult {
    String ki; // key indicator name
    String mot; // managed object type name
    String mo; // managed object name
    String lnode; // logical node name
    public long time_sec; // entry time
    public long value; // entry value
    public long upperBound;
    public long min;
    public long max;
    public long avg;
    public long dev;

    public String getMonitoredEntity() {
        return (mo);
    }

    public String getMOType() {
        return (mot);
    }

    public String getKeyIndicatorName() {
        return (ki);
    }

    public String getLogicalNodeName() {
        return (lnode);
    }

    public long getDate() {
        return (time_sec);
    }

    public long getValue() {
        return (value);
    }

    public long getUpperBound() {
        return (upperBound);
    }

    public long getMin() {
        return (min);
    }

    public long getMax() {
        return (max);
    }

    public long getAverage() {
        return (avg);
    }

    public long getDeviation() {
        return (dev);
    }

}
