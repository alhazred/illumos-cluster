/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)FlexibleTimer.java 1.5     08/05/20 SMI"
 *
 */

package com.sun.cluster.agent.ganymede;

// JDK
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Timer;
import java.util.TimerTask;


/*
 * The FlexibleTimer class extends the java.util.Timer class
 * to allow a user to update dynamically the periodicity of an
 * already schedule TimerTask
 */

class FlexibleTimer extends Timer {

    private Hashtable tasks;

    public FlexibleTimer() {
        super();
        tasks = new Hashtable();
    }

    public void cancel() {

        for (Enumeration e = tasks.elements(); e.hasMoreElements();) {
            GanProxyTask proxy = (GanProxyTask) e.nextElement();
            proxy.cancel();
        }

        super.cancel();
    }

    // For each Task we create a "proxyTask" that will be inserted in the
    // timer and will execute the real task run method (from the user)
    public void scheduleAtFixedRate(TimerTask task, Date firstTime,
        long period) {
        GanProxyTask proxy = new GanProxyTask(task);
        tasks.put(task, proxy);
        super.scheduleAtFixedRate(proxy, firstTime, period);
    }

    public void scheduleAtFixedRate(TimerTask task, long delay, long period) {
        GanProxyTask proxy = new GanProxyTask(task);
        tasks.put(task, proxy);
        super.scheduleAtFixedRate(proxy, delay, period);
    }

    /*
     * When Updating the period of a task, The first task to be executed after
     * the call to setPeriod will be executed at :
     * lastScheduleTime of the TimerTask + the new period
     * If that date is in the past, the task will be executed immediately.
     */
    public void setPeriod(TimerTask userTask, long period) throws Exception {
        GanProxyTask proxy = (GanProxyTask) tasks.remove(userTask);

        if (proxy == null) {
            throw (new Exception("Failed to set Period - no proxy available"));
        }

        proxy.cancel();

        scheduleAtFixedRate(userTask,
            new Date(proxy.getLastScheduledTime() + period), period);
    }

    private class GanProxyTask extends TimerTask {
        private TimerTask userTask;
        private long lastTime = 0;
        private volatile boolean canceled = false;

        public GanProxyTask(TimerTask userTask) {
            this.userTask = userTask;
        }

        public long getLastScheduledTime() {
            return (lastTime);
        }

        // locks are reentrant so GanProxyTask's can call cancel() in their
        // run() methods.
        public synchronized boolean cancel() {
            canceled = true;

            return (super.cancel());
        }

        public synchronized void run() {

            if (canceled == true)
                return;

            lastTime = System.currentTimeMillis();
            userTask.run();
        }
    }
}
