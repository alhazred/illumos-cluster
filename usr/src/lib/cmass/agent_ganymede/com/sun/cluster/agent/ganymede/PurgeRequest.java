/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)PurgeRequest.java 1.5     08/05/20 SMI"
 *
 */

package com.sun.cluster.agent.ganymede;

import com.sun.cluster.agent.ganymede.WorkRequest;

import java.io.Serializable; // needed to save backlog

import java.util.Vector;


/**
 * A PurgeRequest is a WorkRequest triggering a purge in the database. The purge
 * consists in deleting the samples older than a given time
 */
public class PurgeRequest extends WorkRequest {
    public String getType() {
        return "PURGE";
    }
}
