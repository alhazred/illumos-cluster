/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)MeasureSet.java 1.8     08/05/20 SMI"
 *
 */

package com.sun.cluster.agent.ganymede;

import java.io.*;
import java.io.Serializable;

import java.lang.Math;

import java.util.Vector;
import java.util.logging.Logger;


/*
 * This class is containing a set of measures. It contains a MeasureId which
 * inidicates from what objects the measures come from. This object is returned
 * when data is requested through MBean
 */
public final class MeasureSet implements Serializable {

    private static final String logTag = "MeasureSet";
    private static Logger logger = Logger.getLogger(GanymedeModule.DOMAIN +
            "." + logTag);

    private MeasureLevel level;
    private MeasureId id;

    private int time_sec[];
    private int seqNum[];
    private double values[];
    private double upperBounds[];
    private double minValues[]; // WEEKLY
    private double maxValues[]; // standard deviation WEEKLY
    private double avgValues[]; // WEEKLY
    private double devValues[]; // standard deviation WEEKLY
    private String unit; // when returning data from DB

    private int inUse = 0;
    private int allocated = 0;

    public boolean hasMinValues() {
        return (minValues != null);
    }

    public boolean hasMaxValues() {
        return (maxValues != null);
    }

    public boolean hasAvgValues() {
        return (avgValues != null);
    }

    public boolean hasDevValues() {
        return (avgValues != null);
    }

    public int[] getTimes() {

        if (level == MeasureLevel.HOURLY) {
            int tmp[] = new int[inUse];
            System.arraycopy(time_sec, 0, tmp, 0, inUse);

            return tmp;
        }

        return time_sec;
    }


    public int[] getSeqNum() {

        if (level == MeasureLevel.HOURLY) {
            int tmp[] = new int[inUse];
            System.arraycopy(seqNum, 0, tmp, 0, inUse);

            return tmp;
        }

        return seqNum;
    }


    public double[] getValues() {

        if (level == MeasureLevel.HOURLY) {
            double tmp[] = new double[inUse];
            System.arraycopy(values, 0, tmp, 0, inUse);

            return tmp;
        }

        return values;
    }

    public double[] getUpperBounds() {

        if (level == MeasureLevel.HOURLY) {
            double tmp[] = new double[inUse];
            System.arraycopy(upperBounds, 0, tmp, 0, inUse);

            return tmp;
        }

        return upperBounds;
    }

    public double[] getminValues() {
        return minValues;
    }

    public double[] getmaxValues() {
        return maxValues;
    }

    public double[] getavgValues() {
        return avgValues;
    }

    public double[] getdevValues() {
        return devValues;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String theUnit) {
        unit = theUnit;
    }

    public void add(int seqnum, int date, double value, double upperBound) {

        if ((time_sec == null) || (inUse == allocated)) {
            int tmp[];
            double dtmp[];
            tmp = new int[inUse + 10];

            if (time_sec != null) {
                System.arraycopy(time_sec, 0, tmp, 0, inUse);
            }

            time_sec = tmp;

            tmp = new int[inUse + 10];

            if (seqNum != null) {
                System.arraycopy(seqNum, 0, tmp, 0, inUse);
            }

            seqNum = tmp;

            dtmp = new double[inUse + 10];

            if (values != null) {
                System.arraycopy(values, 0, dtmp, 0, inUse);
            }

            values = dtmp;

            dtmp = new double[inUse + 10];

            if (upperBounds != null) {
                System.arraycopy(upperBounds, 0, dtmp, 0, inUse);
            }

            upperBounds = dtmp;

            allocated = allocated + 10;
        }

        time_sec[inUse] = date;
        values[inUse] = value;
        upperBounds[inUse] = upperBound;
        seqNum[inUse] = seqnum;

        inUse = inUse + 1;
    }

    public MeasureId getId() {
        return id;
    }

    public void setId(MeasureId anId) {
        id = anId;
    }


    public MeasureLevel getLevel() {
        return level;
    }

    public void setLevel(MeasureLevel aLevel) {
        level = aLevel;
    }


    public int getLength() {

        if (time_sec == null)
            return 0;
        else
            return time_sec.length;
    }


    public int getTimeOneHourAgo() {
        return (int) (System.currentTimeMillis() / 1000) - (60 * 60);
    }

    public int getTimeOneDayAgo() {
        return (int) (System.currentTimeMillis() / 1000) - (60 * 60 * 24);
    }


    public int getTimeOneWeekAgo() {
        return (int) (System.currentTimeMillis() / 1000) - (60 * 60 * 24 * 7);
    }


    // ------------------------------------------------------------------
    // ------------------------------------------------------------------
    // ------------------------------------------------------------------
    public void saveToFile(String aFileName) {
        StringBuffer toWrite = new StringBuffer();
        FileOutputStream out = null; // declare a file output object
        PrintStream p = null; // declare a print stream object

        if (values == null) {
            logger.fine("No data to save");

            return;
        }

        logger.fine("Created file is: " + aFileName);

        toWrite.append(id.toString() + " - " + level + " - unit = " + unit +
            "\n");

        for (int ii = 0; ii < values.length; ii++) {
            toWrite.append(time_sec[ii] + "\t");
            toWrite.append(seqNum[ii] + "\t");
            toWrite.append(values[ii] + "\t");
            toWrite.append(upperBounds[ii] + "\t");

            if (minValues != null)
                toWrite.append(minValues[ii] + "\t");

            if (maxValues != null)
                toWrite.append(maxValues[ii] + "\t");

            if (avgValues != null)
                toWrite.append(avgValues[ii] + "\t");

            if (devValues != null)
                toWrite.append(devValues[ii] + "\t");

            toWrite.append("\n");
        }

        try {

            // If file is currently sent to the Database. Then create
            // a new one. With a new name to avoid NFS issues.
            // Create a new file output stream
            out = new FileOutputStream(aFileName);
        } catch (java.io.FileNotFoundException e) {
            logger.warning("Error writing measure set to file \"" + aFileName +
                "\"");

            return;
        }


        // Connect print stream to the output stream
        p = new PrintStream(out);

        // Print the string to write
        p.print(toWrite);

        try {
            out.flush();
            out.close();
        } catch (java.io.IOException e) {
            logger.warning("Error writing measure set to file \"" + aFileName +
                "\"");
        }

        p.flush();
        p.close();
    }

}
