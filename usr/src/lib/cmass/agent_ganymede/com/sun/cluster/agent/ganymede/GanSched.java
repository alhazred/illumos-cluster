/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)GanSched.java 1.8     08/05/20 SMI"
 *
 */

package com.sun.cluster.agent.ganymede;

// JDK
import java.util.Enumeration;
import java.util.Random;
import java.util.TimerTask;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

// JMX
import javax.management.MBeanServer;


/**
 * GanSched class.
 */

public class GanSched {

    private static GanSched _instance = null;

    private static final String logTag = "GanSched";
    private static Logger logger = Logger.getLogger(GanymedeModule.DOMAIN +
            "." + logTag);

    private static final int FIRST_PERIOD_MS = 3000;
    private static final int PURGE_PERIOD_S = 60 * 60 / 4; // every quarter
    private long interval = FIRST_PERIOD_MS;

    private FlexibleTimer ti;

    private TimerTask instrumentation = null;
    private TimerTask purge = null;

    public static void _initialize() {
        logger.entering(logTag, "_initialize");

        _instance = new GanSched();
        _instance.initializeInternal();

        logger.exiting(logTag, "_initialize");
    }

    public static void _finalize() {
        logger.entering(logTag, "_finalize");

        // check if already finalized, this can happen if the
        // Ganymede module is locked as a result of failure
        // at unlock time
        if (_instance == null)
            return;

        _instance.finalizeInternal();
        _instance = null;

        logger.exiting(logTag, "_finalize");
    }

    public static void updateSamplingInterval(long period) throws Exception {
        logger.warning("changing instrum period to " + period);
        _instance.updateSamplingIntervalInternal(period);
    }

    public static void updatePurgePeriod(long period) throws Exception {
        logger.warning("changing purge period to " + period);
        _instance.updatePurgePeriodInternal(period);
    }

    private void initializeInternal() {
        ti = new FlexibleTimer();

        interval = Config.getSamplingInterval();
        logger.config("Sampling interval is " + interval);

        instrumentation = new GanInstrum();
        purge = new TimerTask() {
                public void run() {
                    DataBase.passRequest(new PurgeRequest());
                }
            };

        ti.scheduleAtFixedRate(instrumentation, FIRST_PERIOD_MS, // delay
            interval);

        ti.scheduleAtFixedRate(purge,

            // first purge is randomized
            new Random().nextInt(PURGE_PERIOD_S * 1000), PURGE_PERIOD_S * 1000);
    }

    private void finalizeInternal() {
        ti.cancel();
        ti = null;
        instrumentation = null;
        purge = null;
    }

    private void updatePeriod(TimerTask task, long period) throws Exception {
        ti.setPeriod(task, period);
    }

    private void updateSamplingIntervalInternal(long period) throws Exception {
        updatePeriod(instrumentation, period);
    }

    private void updatePurgePeriodInternal(long period) throws Exception {
        updatePeriod(purge, period);
    }
}
