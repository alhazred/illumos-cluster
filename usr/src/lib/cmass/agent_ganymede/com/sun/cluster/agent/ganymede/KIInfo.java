/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)KIInfo.java 1.5     08/05/20 SMI"
 *
 */

package com.sun.cluster.agent.ganymede;

import java.io.*;


public final class KIInfo implements Serializable {
    private String name;
    private String unit;
    private String moType;
    private Integer divisor;
    private Boolean enabled;
    private Object userData; // user data

    public KIInfo(String _name, String _unit, String _moType, Integer _divisor,
        boolean _enabled) {
        name = _name;
        unit = _unit;
        moType = _moType;
        divisor = _divisor;
        enabled = (_enabled == true) ? Boolean.TRUE : Boolean.FALSE;
    }

    public String getName() {
        return name;
    }

    public String getUnit() {
        return unit;
    }

    public String getMOType() {
        return moType;
    }

    public Integer getDivisor() {
        return divisor;
    }

    public boolean isEnabled() {
        return enabled.booleanValue();
    }

    public String isEnabledString() {

        if (isEnabled())
            return "true";

        return "false";
    }

    public void enable() {
        enabled = Boolean.TRUE;
    }

    public void disable() {
        enabled = Boolean.FALSE;
    }

    public void changeState(boolean _enabled) {
        enabled = (_enabled == true) ? Boolean.TRUE : Boolean.FALSE;
    }

    public void setUserData(Object object) {
        userData = object;
    }

    public Object getUserData() {
        return userData;
    }
}
