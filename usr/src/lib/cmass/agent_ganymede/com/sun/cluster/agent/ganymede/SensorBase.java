/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)SensorBase.java 1.12     08/05/20 SMI"
 *
 */

package com.sun.cluster.agent.ganymede;

// JDK
import java.lang.Boolean;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

// JMX
import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.JMException;

// CACAO
import com.sun.cacao.ObjectNameFactory;

// CMASS
import com.sun.cluster.agent.rgm.ResourceGroupMBean;


/**
 * This class stores the list of sensors. It eases the operations performed on
 * "all sensors": getValues, ...
 */
public class SensorBase {
    private static SensorBase _instance = null;

    /* logging facility */
    private static final String logTag = "SensorBase";
    private static Logger logger = Logger.getLogger(GanymedeModule.DOMAIN +
            "." + logTag);

    private static final String CPU_DEVICE_SENSOR = "CPU Device Sensor";

    private static final String MEM_DEVICE_SENSOR = "Memory Device Sensor";

    private static final String MEMCPU_PROCESS_SENSOR =
        "Memory/CPU Process Sensor";

    private LinkedList sensorList;

    /**
     * Constructor
     */
    private SensorBase() {
        sensorList = new LinkedList();
    }


    public static void _initialize() {
        _instance = new SensorBase();
        // init is done is Config.java
    }

    public static void _finalize() {

        if (_instance == null)
            return;

        _instance.finalizeInternal();
        _instance = null;
    }

    public static void refresh(Properties props) {
        _instance.refreshInternal(props);
    }

    public static LinkedList fetchKIList(String moType, Boolean enabled) {
        return _instance.fetchKIListInternal(moType, enabled);
    }

    public static KIInfo lookupKIInfo(String moType, String kiName) {
        return _instance.lookupKIInfoInternal(moType, kiName);
    }

    public static boolean getValuesFromSensors(Vector results)
        throws Exception {
        return _instance.getValuesFromSensorsInternal(results);
    }

    private void finalizeInternal() {

        try {
            ListIterator li = sensorList.listIterator(0);

            while (li.hasNext()) {
                Sensor sensor = (Sensor) li.next();
                sensor.destroy();
            }
        } catch (Exception e) {
            logger.throwing(getClass().getName(), "finalizeInternal", e);
        }
    }

    /**
     * This method walks through all sensors and instructs them to refresh
     * their config Note: this method is called from Config's sysevent handler
     * and could potentially be called at the same time as
     * fetchKIListInternal(), making the refresh non-atomic from the perspective
     * of fetchKIListInternal(); we prevent this by making both methods
     * synchronized.
     */
    synchronized private void refreshInternal(Properties props) {
        logger.entering(logTag, "refreshInternal");

        try {

            if (sensorList.size() == 0) { // First time

                String dir = Config.getSensorDirectory();

                for (int i = 1; true; i++) {
                    String name = props.getProperty("sensor." + i + ".name");

                    if (name == null)
                        break;

                    String file = props.getProperty("sensor." + i + ".file");
                    String type = props.getProperty("sensor." + i + ".type");

                    // construct a sensor object
                    Sensor sensor;

                    if (type.equals("lib")) {
                        sensor = new SensorLib(name, dir + file, i, props);
                    } else if (type.equals("cmd")) {
                        sensor = new SensorCmd(name, dir + file, i, props);
                    } else {
                        logger.warning(logTag + " unknown sensor type");

                        continue;
                    }

                    // insert sensor into list only if it has KIs
                    if (sensor.KIListIsEmpty() == false) {
                        logger.finer("adding configuration for " +
                            sensor.getName());
                        sensorList.add(sensor);
                    } else
                        logger.config("sensor " + name + " has no KI");
                }
            } else { // Refreshing

                ListIterator li = sensorList.listIterator(0);

                while (li.hasNext()) {
                    Sensor sensor = (Sensor) li.next();
                    sensor.refresh(props);
                }
            }
        } catch (Exception e) {
            logger.throwing(getClass().getName(), "refreshInternal", e);
        }

        logger.exiting(logTag, "refreshInternal");
    }

    synchronized private LinkedList fetchKIListInternal(String moType,
        Boolean enabled) {

        LinkedList kiList = new LinkedList();
        ListIterator it = sensorList.listIterator(0);

        while (it.hasNext()) {
            Sensor sensor = (Sensor) it.next();

            LinkedList list = sensor.fetchKIList(moType, enabled);
            kiList.addAll(list);
        }

        return (kiList);
    }

    private KIInfo lookupKIInfoInternal(String moType, String kiName) {

        ListIterator it = sensorList.listIterator(0);

        while (it.hasNext()) {
            Sensor sensor = (Sensor) it.next();

            LinkedList list = sensor.fetchKIList(moType, null);

            if (list.size() == 0)
                continue;

            ListIterator i = list.listIterator(0);

            while (i.hasNext()) {
                KIInfo k = (KIInfo) i.next();

                if (k.getName().equals(kiName)) {

                    // got it
                    return k;
                }
            }
        }

        // KI wasn't found
        return null;
    }

    private boolean getValuesFromSensorsInternal(Vector results)
        throws Exception {

        ListIterator sensorIter = sensorList.listIterator(0);

        logger.entering(getClass().getName(), "getValuesFromSensorsInternal");

        // loop over the sensors
        while (sensorIter.hasNext()) {
            Sensor sensor = (Sensor) sensorIter.next();
            String name = sensor.getName();

            logger.finer("getting values from " + name);

            /*
             * Warning: the sensor names are used here to differentiate the
             * sensors. Changes made in the sensor names in scslmadm_config.h
             * must be reflected here.
             */

            /*
             * The CPU and Memory Device Sensors are passed one managed object
             * name: the node name.
             */

            if (name.equals(CPU_DEVICE_SENSOR) ||
                    name.equals(MEM_DEVICE_SENSOR)) {
                String managedObjectNames[] = new String[1];
                managedObjectNames[0] = Config.getLocalNode();

                sensor.getValues(managedObjectNames, results);

                continue;
            }

            /*
             * Except the Memory/CPU Process Sensor the other sensors aren't
             * passed any managed objects.
             */

            if (!name.equals(MEMCPU_PROCESS_SENSOR)) {
                sensor.getValues(null, results);

                continue;
            }

            /*
             * The Memory/CPU Process Sensor is passed managed object names of
             * this form: "zone:project".
             */

            ObjectNameFactory onf = new ObjectNameFactory(
                    ResourceGroupMBean.class.getPackage().getName());

            ObjectName objName = onf.getObjectNamePattern(
                    ResourceGroupMBean.class);

            MBeanServer mBeanServer = Config.getMBeanServer();

            Set s = mBeanServer.queryNames(objName, null);

            if (s == null) // no RG, skip that sensor
                continue;

            HashMap managedObjectRGNameMap = new HashMap();
            Iterator rgIter = s.iterator();

            // loop over all RGs
            while (rgIter.hasNext()) {
                ObjectName rgObj = (ObjectName) rgIter.next();

                try {

                    // skip non-automated RG
                    String slmType = (String) mBeanServer.getAttribute(rgObj,
                            "SLMType");

                    if (!slmType.equals("automated"))
                        continue;

                    // get the RG nodelist (contains nodes and zones)
                    String nodeNames[] = (String[]) mBeanServer.getAttribute(
                            rgObj, "NodeNames");

                    boolean rgNodelistContainsGlobalZone = false;
                    LinkedList zoneList = new LinkedList();

                    for (int i = 0; i < nodeNames.length; i++) {

                        // tmp[0] contains the node name, tmp[1] the zone name
                        String tmp[] = nodeNames[i].split(":");

                        String node = tmp[0];

                        // filter out remote nodes
                        if (!Config.getLocalNode().equals(node))
                            continue;

                        if (tmp.length > 1) {

                            // RG nodelist contains this local zone
                            zoneList.add(tmp[1]);
                        } else {

                            // RG nodelist contains global zone
                            // (is always the case on Solaris 9)
                            rgNodelistContainsGlobalZone = true;
                        }
                    }

                    // get RG name
                    String rgName = (String) mBeanServer.getAttribute(rgObj,
                            "Name");

                    // build associated managed object names. The project name
                    // is derived from the RG name using the following: "SCSLM_"
                    // is appended to the RG name, and all occurences of "-" are
                    // replaced by "_" (because SRM projects cannot contain "-"
                    // in their names on Solaris 10).

                    String projectName = "SCSLM_" + rgName.replace('-', '_');

                    if (rgNodelistContainsGlobalZone)
                        managedObjectRGNameMap.put(projectName, rgName);

                    ListIterator li = zoneList.listIterator(0);

                    while (li.hasNext()) {
                        String zone = (String) li.next();
                        String managedObjectName = zone + ":" + projectName;

                        managedObjectRGNameMap.put(managedObjectName, rgName);
                    }

                } catch (JMException e) {
                    logger.fine("caught JMException in getValuesFromSensors, " +
                        "continue...");

                    continue;
                } catch (Exception e) {
                    GanymedeInternalException ge =
                        new GanymedeInternalException(e.getMessage());
                    logger.throwing(getClass().getName(),
                        "getValueFromSensorsInternal", ge);
                    throw ge;
                }
            }

            int numManagedObjects = managedObjectRGNameMap.size();

            String managedObjectNames[] = null;

            if (numManagedObjects != 0) {
                managedObjectNames = new String[numManagedObjects];

                Iterator managedObjectIter = managedObjectRGNameMap.keySet()
                    .iterator();

                int n = 0;

                while (managedObjectIter.hasNext()) {
                    managedObjectNames[n++] = (String) managedObjectIter.next();
                }
            }

            // now get the values from the sensor
            Vector tempResults = new Vector();
            boolean res = sensor.getValues(managedObjectNames, tempResults);

            if ((res == false) || (managedObjectNames == null))
                continue;

            for (Enumeration e = tempResults.elements(); e.hasMoreElements();
                    /* */) {

                SensorResult result = (SensorResult) e.nextElement();
                String managedObjectName = result.getMonitoredEntity();

                // get zone name from the MO ("zone:project")
                String zoneName = null;
                String tmp[] = managedObjectName.split(":");

                // build the logical nodename ("node:zone")
                if (tmp.length > 1) {
                    zoneName = Config.getLocalNode() + ":" + tmp[0];
                }

                // get the stored RG name for that MO ("zone:project")
                String rgName = (String) managedObjectRGNameMap.get(
                        managedObjectName);

                SensorResult newRes = new SensorResult(rgName,
                        result.getMOType(), zoneName,
                        result.getKeyIndicatorName(), result.getDate(),
                        result.getValue(), result.getUpperBound());

                results.add(newRes);
            }
        }

        logger.exiting(getClass().getName(), "getValuesFromSensorsInternal");

        return true;
    }
}
