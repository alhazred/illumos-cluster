/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)MeasureId.java 1.7     08/05/20 SMI"
 *
 */

package com.sun.cluster.agent.ganymede;

import java.io.Serializable;


/**
 * This class is used when requesting/returning data. It identifies a set of
 * measures.  Some fields may be empty at request if the user is not filtering
 * on them.
 */
public final class MeasureId implements Serializable {
    private String managedObjectType;
    private String managedObject;
    private String keyIndicator;
    private String nodeName;

    public MeasureId(String theManagedObjectType, String theManagedObject,
        String theKeyIndicator, String theNodeName) {
        managedObjectType = theManagedObjectType;
        managedObject = theManagedObject;
        keyIndicator = theKeyIndicator;
        nodeName = theNodeName;
    }

    public boolean equals(Object o) {
        return (o instanceof MeasureId) &&
            nodeName.equals(((MeasureId) o).nodeName) &&
            keyIndicator.equals(((MeasureId) o).keyIndicator) &&
            managedObjectType.equals(((MeasureId) o).managedObjectType) &&
            managedObject.equals(((MeasureId) o).managedObject);
    }

    /*
     * A good practice is to override hashCode when we override equlas.
     * However, we can keep the Object definition
     */

    public boolean equals(String mId) {
        return (getMangle().equals(mId));
    }

    public String getManagedObjectType() {
        return (managedObjectType);
    }

    public String getManagedObject() {
        return (managedObject);
    }

    public String getNodeName() {
        return (nodeName);
    }

    public String getKeyIndicator() {
        return (keyIndicator);
    }

    /**
     * This method is used to create a unique String from the different fields.
     * This is usefull to create a Key for an HashTable
     */
    public String getMangle() {
        return (nodeName + "&" + managedObject + "&" + keyIndicator + "&" +
                managedObjectType);
    }
}
