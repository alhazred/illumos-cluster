/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ThresholdBase.java 1.16     08/05/20 SMI"
 *
 */
package com.sun.cluster.agent.ganymede;

// JDK
import java.lang.Boolean;
import java.math.BigInteger;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

// JMX
import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.OperationsException;

// CACAO
import com.sun.cacao.ObjectNameFactory;

// Agent
import com.sun.cluster.agent.ganymede.Config;
import com.sun.cluster.agent.ganymede.KIInfo;
import com.sun.cluster.agent.rgm.ResourceGroupMBean;


/*
 * This class is a singleton containing the defined thresholds for all
 * objects.
 */
public class ThresholdBase {
    private static ThresholdBase _instance = null;

    /* logging facility */
    private static final String logTag = "ThresholdBase";
    private static Logger logger = Logger.getLogger(GanymedeModule.DOMAIN +
            "." + logTag);

    private Hashtable thresholdList;


    static native int generateEvent(BigInteger dividend, int divisor,
        String moType, String kiName, String moName, String prevStatus,
        String currStatus, String dir);

    synchronized public static void _initialize() {
        _instance = new ThresholdBase();
        _instance.initializeInternal();
    }

    synchronized public static void _finalize() {

        if (_instance == null)
            return;

        _instance.finalizeInternal();
        _instance = null;
    }

    synchronized public static void updateThresholds(Hashtable v) {
        logger.entering(logTag, "updateThresholds");
        _instance.updateThresholdsInternal(v);
        logger.exiting(logTag, "updateThresholds");
    }

    synchronized public static Threshold getThreshold(String mot, String ki,
        String mo, String node) {
        return (_instance.getThresholdInternal(mot, ki, mo, node));
    }

    synchronized public static void verifyThresholds(Vector results,
        int seqNum) {
        _instance.verifyThresholdsInternal(results, seqNum);
    }

    synchronized public static Vector fetchLocalCrossedThresholds(boolean all)
        throws Exception {
        return (_instance.fetchLocalCrossedThresholdsInternal(all));
    }

    /**
     * Internals
     */

    private void initializeInternal() {
        thresholdList = new Hashtable();
    }

    private void finalizeInternal() {
        thresholdList.clear();
        thresholdList = null;
    }

    private boolean isNodeLocal(String node) {

        if (node == null) {
            return (true);
        }

        if (node.equals("")) {
            return (true);
        }

        if (node.equals(Config.getLocalNode())) {
            return (true);
        }

        if (node.startsWith(Config.getLocalNode() + ":")) {
            return (true);
        }

        return (false);
    }

    /**
     * return the node part of a string This is usefull to return the nodename
     * when given a logical nodename in paramater logical nodename = "node:zone"
     */
    private String extractNodeName(String node) {
        int index = node.lastIndexOf(":");

        if (index == -1) {
            return (node);
        }

        return (node.substring(0, index));
    }

    private Threshold getThresholdInternal(String motype, String kiname,
        String moname, String node) {
        return (getThresholdInternal(motype, kiname, moname, node, false));
    }


    /**
     * if returnOnlyInstances=true then clusterwide thresholds are not returned
     */
    private Threshold getThresholdInternal(String motype, String kiname,
        String moname, String nodename, boolean returnOnlyInstances) {

        logger.entering(logTag, "getThresholdInternal");

        // override nodename for thresholds on mot.sunw.node objects
        String node = nodename;

        if (motype.equals(Config.MOTYPE_NODE)) {
            node = moname;
        }

        Threshold clusterWideThresh = null;
        Threshold specifiedThresh = null;

        Vector list = (Vector) getThresholdListInternal(motype, kiname, moname,
                node);

        if (list == null) {
            logger.fine("List is NULL");

            return (null);
        }

        logger.fine("List size : " + list.size());

        for (Enumeration e = list.elements(); e.hasMoreElements(); /* */) {
            Threshold t = (Threshold) e.nextElement();

            if (t.isClusterWide()) {

                // this is a clusterwide threshold
                clusterWideThresh = t;
            } else if (t.getNodeName().equals(node)) {
                specifiedThresh = t;
            }
        }

        if (returnOnlyInstances == true) {

            if (specifiedThresh != null) {
                logger.fine("return Specified threshold");

                return (specifiedThresh);
            } else if (clusterWideThresh != null) {
                logger.fine("return newly created Specified threshold");

                Threshold newThresh = (Threshold) clusterWideThresh.clone();
                newThresh.setNodeName(node);
                addThresholdInternal(newThresh);

                return (newThresh);
            }
        } else { // (returnOnlyInstances == false)

            if (clusterWideThresh != null) {
                logger.fine("return Clusterwide threshold");

                return (clusterWideThresh);
            } else if (specifiedThresh != null) {
                logger.fine("return Specified threshold");

                return (specifiedThresh);
            }
        }

        logger.fine("return NULL threshold");
        logger.exiting(logTag, "getThresholdInternal");

        return (null);
    }

    private void addThresholdInternal(Threshold t) {

        logger.entering(logTag, "addThresholdInternal");

        try {

            // only the thresholds for the local node or all the nodes will be
            // kept on the local node
            if (t == null) {
                logger.fine("provided threshold is NULL");

                return;
            }

            String node;

            if (t.isClusterWide()) {

                // clusterwide threshold, we use the local node as the hash key
                node = Config.getLocalNode();
            } else {

                // all other thresholds have an associated node name
                node = extractNodeName(t.getNodeName());
            }

            Hashtable aNodeHash = (Hashtable) thresholdList.get(node);

            if (aNodeHash == null) {
                logger.fine("Create aNodeHash for " + node);
                aNodeHash = new Hashtable();
                thresholdList.put(node, aNodeHash);
            }

            Hashtable myMotypeHash = (Hashtable) aNodeHash.get(t.getMOType());

            if (myMotypeHash == null) {
                logger.fine("Create myMotypeHash for " + t.getMOType());
                myMotypeHash = new Hashtable();
                aNodeHash.put(t.getMOType(), myMotypeHash);
            }

            Hashtable myKinameHashtable = (Hashtable) myMotypeHash.get(t
                    .getKIName());

            if (myKinameHashtable == null) {
                logger.fine("Create myKinameHash for " + t.getKIName());

                myKinameHashtable = new Hashtable();
                myMotypeHash.put(t.getKIName(), myKinameHashtable);
            }

            // List contains the thresholds that have a node value of : node =
            // "n1" or "n1:z1" or "" ("" means all of the nodes/zones)
            Vector list = (Vector) myKinameHashtable.get(t.getMOName());

            if (list == null) {
                logger.fine("Create threshold List for " + t.getMOName());
                list = new Vector();
                myKinameHashtable.put(t.getMOName(), list);
            }

            // In all cases, add the threshold to the list
            list.add(t);

        } catch (Exception e) {
            logger.throwing(getClass().getName(), "addThreshold", e);
        }

        logger.exiting(logTag, "addThresholdInternal");
    }

    private Vector getThresholdListInternal(String motype, String kiname,
        String moname, String node) {

        logger.entering(logTag, "getThresholdListInternal");

        Hashtable myNodeHash = null;
        Hashtable myMotypeHash = null;
        Hashtable myKinameHashtable = null;
        String aNode = null;
        Vector list = null;

        // Cluster-wide thresholds are stored under the local node
        if ((node == null) || node.equals("")) {
            aNode = Config.getLocalNode();
        } else {
            aNode = extractNodeName(node);
        }

        myNodeHash = (Hashtable) thresholdList.get(aNode);

        if (myNodeHash != null) {
            myMotypeHash = (Hashtable) myNodeHash.get(motype);
        } else {
            logger.fine("getThresholdListInternal " + "myNodeHash is null");

            return (null);
        }

        if (myMotypeHash != null) {
            myKinameHashtable = (Hashtable) myMotypeHash.get(kiname);
        } else {
            logger.fine("getThresholdListInternal " + "myMotypeHash is null");

            return (null);
        }

        if (myKinameHashtable != null) {
            list = (Vector) myKinameHashtable.get(moname);
        } else {
            logger.fine("getThresholdListInternal " +
                "myKinameHashtable is null");

            return (null);
        }

        logger.exiting(logTag, "getThresholdListInternal");

        return (list);
    }


    private Vector fetchLocalCrossedThresholdsInternal(boolean all)
        throws Exception {

        logger.entering(logTag, "fetchLocalCrossedThresholdsInternal");

        Vector output = new Vector();

        Hashtable myNodeHash = null;

        // retrieve only local thresholds
        myNodeHash = (Hashtable) thresholdList.get(Config.getLocalNode());

        if (myNodeHash == null) {
            logger.fine("No thresholds configured on that node");

            return (output);
        }

        try {
            Enumeration localNodeEnum = myNodeHash.elements();

            while (localNodeEnum.hasMoreElements()) {
                Enumeration MOTypeEnum =
                    ((Hashtable) localNodeEnum.nextElement()).elements();

                while (MOTypeEnum.hasMoreElements()) {
                    Enumeration KINameEnum =
                        ((Hashtable) MOTypeEnum.nextElement()).elements();

                    while (KINameEnum.hasMoreElements()) {
                        Vector list = (Vector) KINameEnum.nextElement();

                        for (Enumeration e = list.elements();
                                e.hasMoreElements(); /* */) {

                            Threshold thresh = (Threshold) e.nextElement();

                            if (thresh == null) {
                                logger.warning("null threshold! " +
                                    "should never occur");

                                continue;
                            }

                            logger.fine("node : " + thresh.getNodeName() +
                                " MOT : " + thresh.getMOType() + " KI : " +
                                thresh.getKIName() + " MO : " +
                                thresh.getMOName() + " Status : " +
                                thresh.getCurrentStatus() + " Value : " +
                                thresh.getCurrentDoubleValue());

                            // We skip the cluster-wide threshold because the
                            // clusterwide threshold is a configuration
                            // information only and is never updated
                            if (thresh.isClusterWide()) {
                                logger.fine("Skipping clusterwide threshold");

                                continue;
                            }

                            if (((all == false) &&
                                        (thresh.getCurrentStatus() !=
                                            ThresholdStatus.OK)) ||
                                    (all == true)) {
                                logger.fine("Adding threshold");
                                output.add(thresh);
                            } else {
                                logger.fine("Status Ok, skipping");
                            }
                        }
                    }
                }
            }

            logger.fine("Total threshold found = " + output.size());

        } catch (Exception e) {
            logger.throwing(getClass().getName(),
                "fetchLocalCrossedThresholdsInternal", e);
        }

        logger.exiting(logTag, "fetchLocalCrossedThresholdsInternal");

        return (output);
    }

    private void sendNotification(String moType, String kiName, String moName,
        BigInteger dividend, ThresholdDirection direction,
        ThresholdStatus status[]) {

        KIInfo ki = SensorBase.lookupKIInfo(moType, kiName);

        if (ki == null) {
            logger.severe("lookupKIInfo failed");

            return;
        }

        sendNotification(moType, kiName, moName, dividend, ki.getDivisor(),
            direction, status);
    }

    private void sendNotification(String moType, String kiName, String moName,
        BigInteger dividend, Integer divisor, ThresholdDirection direction,
        ThresholdStatus status[]) {

        if (status[0] != status[1]) { // status changed

            String prevStatus = "";
            String currStatus = "";
            String dir = "";

            try {
                prevStatus = ThresholdStatus.getEnumString(status[0]);
                currStatus = ThresholdStatus.getEnumString(status[1]);
                dir = ThresholdDirection.getEnumString(direction);
            } catch (Exception e) {

                // should never happen
                logger.severe("Unable to retrieve string associated to enum");
                logger.throwing(getClass().getName(), "sendNotification", e);

                return;
            }

            if (
                generateEvent(dividend, divisor.intValue(), moType, kiName,
                        moName, prevStatus, currStatus, dir) != 0) {

                logger.severe("Error: generateEvent failed");
            }
        }
    }

    private void resetLocalOldThresholds(int seqNum) {
        logger.entering(logTag, "resetLocalOldThresholds");

        Vector output = new Vector();

        Hashtable myNodeHash = null;

        // retrieve only local thresholds
        myNodeHash = (Hashtable) thresholdList.get(Config.getLocalNode());

        if (myNodeHash == null) {
            logger.fine("No thresholds configured on that node");

            return;
        }

        Enumeration localNodeEnum = myNodeHash.elements();

        while (localNodeEnum.hasMoreElements()) {
            Enumeration MOTypeEnum = ((Hashtable) localNodeEnum.nextElement())
                .elements();

            while (MOTypeEnum.hasMoreElements()) {
                Enumeration KINameEnum = ((Hashtable) MOTypeEnum.nextElement())
                    .elements();

                while (KINameEnum.hasMoreElements()) {
                    Vector list = (Vector) KINameEnum.nextElement();

                    for (Enumeration e = list.elements();
                            e.hasMoreElements(); /* */) {

                        Threshold thresh = (Threshold) e.nextElement();

                        if (thresh == null) {
                            continue;
                        }

                        // We skip the cluster-wide threshold because the
                        // clusterwide threshold is a configuration information
                        // only and is never updated
                        if (thresh.isClusterWide()) {
                            logger.fine("Skipping clusterwide threshold");

                            continue;
                        }

                        logger.fine("node : " + thresh.getNodeName() +
                            " MOT : " + thresh.getMOType() + " KI : " +
                            thresh.getKIName() + " MO : " + thresh.getMOName() +
                            " Status : " + thresh.getCurrentStatus() +
                            " Value : " + thresh.getCurrentDoubleValue() +
                            " SeqNum : " + thresh.getCurrentSeqNum() + " vs " +
                            seqNum);

                        if (thresh.getCurrentSeqNum() != seqNum) {
                            ThresholdStatus status[];

                            logger.fine("RESETING");

                            // reset RISING
                            status = thresh.reset(ThresholdDirection.RISING,
                                    seqNum);

                            sendNotification(thresh.getMOType(),
                                thresh.getKIName(), thresh.getMOName(),
                                BigInteger.ZERO, ThresholdDirection.RISING,
                                status);

                            // reset FALLING
                            status = thresh.reset(ThresholdDirection.FALLING,
                                    seqNum);

                            sendNotification(thresh.getMOType(),
                                thresh.getKIName(), thresh.getMOName(),
                                BigInteger.ZERO, ThresholdDirection.FALLING,
                                status);
                            output.add(thresh);
                        }
                    }
                }
            }
        }

        logger.exiting(logTag, "resetLocalOldThresholds");
    }

    private void verifyThresholdsInternal(Vector results, int seqNum) {

        logger.entering(logTag, "verifyThresholdsInternal");

        Iterator ite = results.iterator();

        while (ite.hasNext()) {
            SensorResult sr = (SensorResult) ite.next();
            BigInteger dividend = sr.getValue();
            String moType = sr.getMOType();
            String kiName = sr.getKeyIndicatorName();
            String moName = sr.getMonitoredEntity();
            String node = sr.getLogicalNodeName();

            ThresholdStatus status[];

            if (node == null) {
                node = Config.getLocalNode();
            }

            logger.fine("Looking for threshold  " + sr);

            /*
             * using true to be able to return new Instances
             * of thresholds if the threshold is clusterwide
             */
            Threshold thresh = getThresholdInternal(moType, kiName, moName,
                    node, true);

            if (thresh == null) {

                // No threshold for that SensorResult, continue
                continue;
            }

            KIInfo ki = SensorBase.lookupKIInfo(moType, kiName);

            if (ki == null) {
                logger.severe("lookupKIInfo failed");

                return;
            }

            Integer divisor = ki.getDivisor();

            // Dont want to divide by 0
            double val = (divisor.doubleValue() <= 0.)
                ? dividend.doubleValue()
                : (dividend.doubleValue() / divisor.doubleValue());


            // check RISING
            status = thresh.setCurrentValue(ThresholdDirection.RISING, val,
                    seqNum);

            sendNotification(moType, kiName, moName, dividend, divisor,
                ThresholdDirection.RISING, status);

            // check FALLING
            status = thresh.setCurrentValue(ThresholdDirection.FALLING, val,
                    seqNum);

            sendNotification(moType, kiName, moName, dividend, divisor,
                ThresholdDirection.FALLING, status);
        }

        // Now we need to reset all of the thresholds on objects that disapeared
        resetLocalOldThresholds(seqNum);

        logger.exiting(logTag, "verifyThresholdsInternal");
    }


    private void updateThresholdsInternal(Hashtable v) {
        logger.entering(logTag, "updateThresholdsInternal");

        Vector additionalThresholds = new Vector();


        if (thresholdList.size() != 0) {
            // update new ones with previous values/states

            for (Enumeration e = v.elements(); e.hasMoreElements(); /* */) {
                boolean containsClusterWide = false;
                boolean replace = false;

                Threshold newThresh = (Threshold) e.nextElement();

                Vector thresholdList = getThresholdListInternal(newThresh
                        .getMOType(), newThresh.getKIName(),
                        newThresh.getMOName(), newThresh.getNodeName());

                // This is a completely new threshold
                if (thresholdList == null) {
                    continue;
                }

                /*
                 * 4 cases can happen :
                 *
                 * 1- if the new threshold is cluster wide
                 *    and there is already a clusterwide threshold
                 *    we update all the instances
                 * 2- if the new Threshold is cluster wide
                 *    but there is no clusterwide thresholds in the list
                 *    we delete all the existing instances
                 * 3- if the new threshold is NOT clusterwide
                 *    and there is a clusterwide threshold in the list
                 *    we delete all the existing instances
                 * 4- if the new threshold is NOT clusterwide
                 *    and there is no clusterwide threshold in the list,
                 *    the new threshold is added/updated in the list
                 */

                Iterator ite = thresholdList.iterator();

                while (ite.hasNext()) {
                    Threshold tmpThresh = (Threshold) ite.next();

                    if (tmpThresh.isClusterWide()) {
                        containsClusterWide = true;

                        break;
                    }
                }

                if (containsClusterWide != newThresh.isClusterWide()) {
                    replace = true;
                }

                // cases 2 and 3
                if (replace == true) {
                    // we skip all of the existing thresholds
                } else {

                    for (Enumeration ee = thresholdList.elements();
                            ee.hasMoreElements(); /* */) {

                        Threshold tmpThresh = (Threshold) ee.nextElement();

                        if (tmpThresh.isClusterWide()) {

                            // we skip the clusterwide threshold definition
                            continue;
                        } else if (containsClusterWide == true) {
                            // updating all (case 1)

                            // creating a new instance for the local node
                            // or for a specific zone
                            Threshold newInstance = (Threshold) newThresh
                                .clone();

                            // copy the value
                            newInstance.setCurrentValue(tmpThresh
                                .getCurrentValue(),
                                tmpThresh.getCurrentSeqNum());

                            // copy the statuses
                            newInstance.setCurrentStatuses(tmpThresh
                                .getCurrentRisingStatus(),
                                tmpThresh.getCurrentFallingStatus());

                            // copy the nodename/zonename
                            newInstance.setNodeName(tmpThresh.getNodeName());

                            // Insert it in a temporary Vector
                            additionalThresholds.add(newInstance);

                        } else {

                            // updating the right one (case 4)
                            if (tmpThresh.getNodeName().equals(
                                        newThresh.getNodeName())) {

                                Double value = tmpThresh
                                    .getCurrentDoubleValue();

                                // copy the value if it is not null, if it is
                                // null we don't have anything to do, the values
                                // set in the threshold constructor are fine
                                if (value != null) {
                                    newThresh.setCurrentValue(value
                                        .doubleValue(),
                                        tmpThresh.getCurrentSeqNum());
                                }

                                // copy the statuses
                                newThresh.setCurrentStatuses(tmpThresh
                                    .getCurrentRisingStatus(),
                                    tmpThresh.getCurrentFallingStatus());

                            }
                        }
                    }
                }
            }

            // erase previous thresholds
            thresholdList.clear();
        }

        // Insert the new thresholds
        for (Enumeration e = v.elements(); e.hasMoreElements(); /* */) {

            logger.fine("adding threshold from new config");
            addThresholdInternal((Threshold) e.nextElement());
        }

        for (Enumeration e = additionalThresholds.elements();
                e.hasMoreElements(); /* */) {

            logger.fine("adding old instances threshold");
            addThresholdInternal((Threshold) e.nextElement());
        }

        logger.exiting(logTag, "updateThresholdsInternal");
    }

}
