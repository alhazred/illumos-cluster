/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)Sensor.java 1.11     08/05/20 SMI"
 *
 */
package com.sun.cluster.agent.ganymede;

// JDK
import java.lang.Boolean;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Properties;
import java.util.Vector;
import java.util.logging.Logger;


/**
 * Abstract class implementing methods that are common to all sensor types.
 */
abstract class Sensor {

    /* logging facility */
    protected static final String logTag = "Sensor";
    protected static Logger logger = Logger.getLogger(GanymedeModule.DOMAIN +
            "." + logTag);
    private LinkedList keyIndicatorList;

    protected String name;

    // file to load
    protected String file;

    // sensor id in the config
    private int idInConf;

    public Sensor(String _name, String _file, int _idInConf, Properties props) {

        this.name = _name;
        this.file = _file;
        this.idInConf = _idInConf;

        String pfx = "sensor." + idInConf + ".KI.";

        keyIndicatorList = new LinkedList();

        /* get sensor KIs from config */
        for (int i = 1; true; i++) {
            String kiname = props.getProperty(pfx + i + ".name");

            if (kiname == null)
                break;

            String moType = props.getProperty(pfx + i + ".motype");
            String unit = props.getProperty(pfx + i + ".unit");
            Integer divisor = Integer.valueOf(props.getProperty(
                        pfx + i + ".divisor"));
            boolean enabled = props.getProperty(pfx + i + ".enabled", "true")
                .matches("([yY][eE][sS]|[tT][rR][uU][eE])");

            KIInfo KI = new KIInfo(kiname, unit, moType, divisor, enabled);

            /* disabled KIs are also inserted into list */
            keyIndicatorList.add(KI);
        }

        if (KIListIsEmpty() == true)
            logger.info("sensor " + name + " has no KI");
    }

    public void refresh(Properties props) {

        for (int i = 1; true; i++) {
            String enabled = props.getProperty("sensor." + idInConf + ".KI." +
                    i + ".enabled");
            String name = props.getProperty("sensor." + idInConf + ".KI." + i +
                    ".name");

            if (enabled == null)
                break;

            try {
                ListIterator li = keyIndicatorList.listIterator(0);

                while (li.hasNext()) {
                    KIInfo ki = (KIInfo) li.next();

                    if (ki.getName().equals(name)) {

                        if (ki.isEnabledString().equals(enabled)) {
                            logger.fine("Already the right value for " + name);
                        } else {
                            logger.fine("updates activation state for " + name +
                                " was " + ki.isEnabledString() + " now " +
                                enabled);

                            // update the ki state
                            ki.changeState(enabled.matches(
                                    "([yY][eE][sS]|[tT][rR][uU][eE])"));

                            // start/stop the telemetry for that KI
                            if (ki.isEnabled())
                                registerKI(name);
                            else
                                unregisterKI(name);
                        }
                    }
                }
            } catch (Exception e) {
                logger.warning(e.toString());
            }
        }
    }

    public boolean KIListIsEmpty() {
        return (keyIndicatorList.isEmpty());
    }

    public int getId() {
        return (idInConf);
    }

    public String getName() {
        return (name);
    }

    public LinkedList fetchKIList(String moType, Boolean enabled) {
        LinkedList kiList = new LinkedList();
        ListIterator it = keyIndicatorList.listIterator(0);

        while (it.hasNext()) {
            KIInfo ki = (KIInfo) it.next();

            if (ki.getMOType().equals(moType) &&
                    ((enabled == null) ||
                        (enabled.booleanValue() == ki.isEnabled()))) {
                kiList.add(ki);
            }
        }

        return (kiList);
    }

    private KIInfo lookupKIInfo(String kiName) {
        ListIterator it = keyIndicatorList.listIterator(0);

        while (it.hasNext()) {
            KIInfo ki = (KIInfo) it.next();

            if (ki.getName().equals(kiName))
                return (ki);
        }

        return (null);
    }

    /*
     * Cautious: this function is called from within the sensor interface jni
     * code so do not modify the signature of that function without modifying
     * gan_sensor_interface.c. As a side note, it's also called from within
     * SensorCmd.java, hence the protected keyword.
     */
    protected String getMOType(String kiName) {
        KIInfo ki = lookupKIInfo(kiName);

        if (ki != null)
            return ki.getMOType();

        return (null);
    }


    /**
     * To be called in subclasses' constructor.
     */
    protected void registerKeyIndicators() {
        ListIterator it = keyIndicatorList.listIterator();

        while (it.hasNext()) {
            KIInfo ki = (KIInfo) it.next();

            if (ki.isEnabled())
                registerKI(ki.getName());
        }
    }

    /**
     * To be called in subclasses' destroy() method.
     */
    protected void unregisterKeyIndicators() {
        ListIterator it = keyIndicatorList.listIterator();

        while (it.hasNext()) {
            KIInfo ki = (KIInfo) it.next();

            if (ki.isEnabled())
                unregisterKI(ki.getName());
        }
    }

    public abstract boolean registerKI(String kiName);

    public abstract boolean unregisterKI(String kiName);

    public abstract boolean getValues(String objectNames[], Vector results);

    public abstract void destroy();

}
