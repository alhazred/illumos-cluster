/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)SensorLib.java 1.6     08/05/20 SMI"
 *
 */
package com.sun.cluster.agent.ganymede;

// JDK
import java.lang.String;

import java.util.Properties;
import java.util.Vector;


/**
 * This class provides specific code for sensors of type library.
 */
public class SensorLib extends Sensor {

    public SensorLib(String _name, String _file, int _idInConf,
        Properties props) {

        super(_name, _file, _idInConf, props);

        if (KIListIsEmpty() == false) {
            registerKeyIndicators();
        }
    }

    private native int registerJNI(String file, String KIType);

    private native int getValuesJNI(String file, String mo[], Vector result);

    private native int unregisterJNI(String file, String KIType);

    public synchronized boolean registerKI(String kiName) {
        int retval;

        try {
            retval = registerJNI(file, kiName);
        } catch (Exception e) {
            logger.warning(e.getMessage());
            retval = 1;
        }

        return ((retval == 0) ? true : false);
    }

    public synchronized boolean unregisterKI(String kiName) {
        int retval;

        try {
            retval = unregisterJNI(file, kiName);
        } catch (Exception e) {
            logger.warning(e.getMessage());
            retval = 1;
        }

        return ((retval == 0) ? true : false);
    }

    public synchronized boolean getValues(String objectNames[],
        Vector results) {
        int retval;

        try {
            Vector v = new Vector();
            retval = getValuesJNI(file, objectNames, v);
            results.addAll(v);
        } catch (Exception e) {
            logger.warning(e.getMessage());
            retval = 1;
        }

        return ((retval == 0) ? true : false);
    }

    /**
     * destroy()'s caller must make sure that neither registerKI(), nor
     * unregisterKI(), nor getValues() can be called at the same time and after
     * destroy() is called.
     */
    public void destroy() {
        unregisterKeyIndicators();
    }
}
