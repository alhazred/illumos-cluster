/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)DataBase.java 1.23     08/09/08 SMI"
 *
 */

package com.sun.cluster.agent.ganymede;

// JDK
import java.io.*;
import java.lang.Integer;
import java.lang.String;
import java.lang.Thread;
import java.net.UnknownHostException;
import java.sql.*;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Logger;

// JMX
import javax.management.JMException;

// cmass agent
import com.sun.cluster.agent.rgm.RgmModule;


/**
 * This class is implementing the access to the Database
 */
public class DataBase extends WorkAgent {

    public static final int MEASURE_ID_SIZE_LIMIT = 80;
    private static DataBase _instance;

    private static final String DATA_NOT_RETRIEVABLE = 
        "Telemetry data can not be retrieved";  // only for log, no i18n
    private static final String logTag = "DataBase";
    private static Logger logger = Logger.getLogger(GanymedeModule.DOMAIN +
            "." + logTag);

    // number of millisecs before attempting a DB reconnection
    private static final long sleepTimeBeforeReconnection = 1000;

    // node, KI, MO, and MOT id caches
    private DataBaseCache cache;

    // KIInfo cache
    private Hashtable KIInfoCache;

    // database connection handle
    private Connection connection;

    /**
     * Constructor
     */
    private DataBase() throws Exception {
        super("DataBase");

        // Load JDBC driver. The
        // -Djdbc.drivers=org.apache.derby.jdbc.ClientDriver statement could
        // be used in the XLM property file instead
        Class.forName("org.apache.derby.jdbc.ClientDriver");

        // initialize attributes
        connection = null;
        KIInfoCache = new Hashtable();
        cache = new DataBaseCache("Main");

    }

    /*
     * Static methods
     */

    /*
     * Users interact with the DataBase singleton by means of the following
     * class (static) methods.
     *
     * These class methods are executed outside the DataBase thread, so special
     * care must taken to nicely managed the concurrency. The class methods must
     * not do update/delete/insert type transactions to the DB because two
     * threads could end up "commiting" at the same time which would lead to
     * unpredictable results. The class methods must not try to reconnect to the
     * DB on connection loss because two threads could end up reconnecting at
     * the same time, which would result in reconnecting twice. And the class
     * methods must handle the connection = null case which may occur if the
     * DataBase thread has terminated.
     */

    public static void _initialize() throws Exception {
        logger.finer("Entering _initialize");

        _instance = new DataBase();
        _instance.initializeInternal();
        _instance.start(); // start thread

        logger.finer("Exiting _initialize");
    }

    public static void _finalize() {
        logger.finer("Entering _finalize");

        // Check if already finalized, this can happen if the Ganymede module is
        // locked as a result of failure at unlock time
        if (_instance == null)
            return;

        // We may have come in here as the result of calling lockModule(), we
        // may therefore be executing in the Agent thread. In that case, we
        // don't need to request a stop and we must not try to join ourself.
        if (Thread.currentThread() != _instance)
            _instance.finalizeInternal();

        _instance = null;

        logger.finer("Exiting _finalize");
    }

    public static void passRequest(WorkRequest wr) {
        _instance.insertRequest(wr);
    }

    public static int getSeqNum(MeasureLevel theLevel) {
        return _instance.getSeqNumInternal(theLevel);
    }

    /*
     * The remaining static methods below are called through the Ganymede MBean
     * interface by the CACAO framework. It is to be note that we're guaranteed
     * that _instance isn't null because CACAO protects its modules from being
     * reentered while they are stopped or being stopped.
     */

    public static MeasureSet[] getData(MeasureId ids[], MeasureLevel level,
        Long begin, Long end, Long timeout) throws Exception,
        GanymedeTimeoutException {

        if ((ids == null) || (level == null)) {
            IllegalArgumentException e = new IllegalArgumentException();
            logger.throwing(DataBase.class.getName(), "getData", e);
            throw e;
        }

        SelectRequest sr = new SelectRequest(ids, level, false, /* not latest */
                begin, end);

        _instance.getDataInternal(sr, timeout);

        if (sr.result == null) {
            // when result is null, DB is not accessible
            throw new Exception(DATA_NOT_RETRIEVABLE);
        } 
        return sr.result;
    }


    public static MeasureSet[] getLatestData(MeasureId ids[],
        MeasureLevel level, Long timeout) throws Exception,
        GanymedeTimeoutException {

        if ((ids == null) || (level == null)) {
            IllegalArgumentException e = new IllegalArgumentException();
            logger.throwing(DataBase.class.getName(), "getLatestData", e);
            throw e;
        }

        if (ids.length > MEASURE_ID_SIZE_LIMIT) {
            IllegalArgumentException e = new IllegalArgumentException(
                "The size of MeasureId exceeded limitation");
            logger.throwing(DataBase.class.getName(), "getLatestData", e);
            throw e;
        }

        SelectRequest sr = new SelectRequest(ids, level, true, /* latest */
                null, null);

        _instance.getDataInternal(sr, timeout);
 
        if (sr.result == null) {
            // when result is null, DB is not accessible
            throw new Exception(DATA_NOT_RETRIEVABLE);
        } 
        return sr.result;
    }

    /*
     * Object methods
     */

    private void initializeInternal() throws Exception {

        /*
         * establishConnection() calls Config.getURLToDatabase(), which
         * internally calls Config.getDatabaseHostName() to get the hostname of
         * the database RG's current primary. At this point it may be that the
         * database RG MBean's cache is not valid and the database RG has no
         * primary. This can occur if the cluster event hasn't been received
         * yet, and the MBean cache hasn't been invalidated yet. We need to take
         * this into account, and retry.  We retry during 110 seconds, which
         * corresponds to the time after which the MBean cache is automatically
         * invalidated by CACAO (100 seconds, see CachedVirtualMBeanInterceptor
         * javadoc) plus 10 seconds.
         *
         * Note that we also retry establishConnection() if an SQLException
         * occurs at connection time. This can happen in the case where the
         * database RG is switching/failing over while we are starting.
         */

        long connectRetries = 110 / (sleepTimeBeforeReconnection / 1000);

        boolean connect = true;

        /*
         * This loop establishes the connection to the DB server, reads the
         * necessary information from the DB, and tries again if the connection
         * is lost.
         */

        while (true) {

            try {

                // establish connection and create schema
                if (connect) {
                    connect = false;
                    establishConnection();
                }

                // fill node, KI, MO and MOT id caches
                fillCaches();

                initSeqNum();

                break; // ok, done

            } catch (SQLException e) {

                // handleSQLException throws e if it cannot deal with it
                connect = handleSQLException(e);

                try {
                    connection.rollback();
                } catch (Exception ee) {
                    // may be NullPointerException
                }

                logger.fine("Number of retries to do before giving up: " +
                    connectRetries);

                if (connect && (--connectRetries < 0))
                    throw e;

                try {
                    Thread.sleep(sleepTimeBeforeReconnection);
                } catch (InterruptedException ee) {
                }

            } catch (GanymedeNoPrimaryException e) {

                logger.fine("Number of retries to do before giving up: " +
                    connectRetries);

                if (--connectRetries < 0) {
                    throw e;
                }

                try {
                    Thread.sleep(sleepTimeBeforeReconnection);
                } catch (InterruptedException ee) {
                }

                connect = true;
            } // any other exception will be thrown
        }
    }

    private void finalizeInternal() {

        // request the Agent thread to stop
        requestStop();

        // wait for thread termination
        try {
            join();
        } catch (InterruptedException e) {
            logger.warning(e.toString());
        }

        logger.fine("Database thread joined");
    }

    /**
     * get the current seqnum in the database (for the current host)
     *
     * @param  none
     *
     * @return  the seqnum of this node
     */
    private int getSeqNumInternal(MeasureLevel theLevel) {
        return (theLevel == MeasureLevel.HOURLY) ? cache.hourlySeqNum
                                                 : cache.weeklySeqNum;
    }

    private void getDataInternal(SelectRequest sr, Long timeout)
        throws Exception, GanymedeTimeoutException {

        insertRequest(sr);

        /*
         * We set the wait timeout in such a way that the database RG has
         * time to restart.
         */
        long to;

        if (timeout != null) {
            to = timeout.longValue();
        } else {
            to = (Config.getDatabaseProbeInterval() +
                    Config.getDatabaseProbeTimeout() +
                    Config.getDatabaseStartTimeout() +
                    Config.getDatabaseStopTimeout()) / 1000;
        }

        logger.fine("Wait for data readiness");

        sr.waitForDataReadiness(to);
    }

    /**
     * Run thread
     */
    public void run() {

        logger.fine("database thread starts");

        long connectRetries = (Config.getDatabaseProbeInterval() +
                Config.getDatabaseProbeTimeout() +
                Config.getDatabaseStartTimeout() +
                Config.getDatabaseStopTimeout()) /
            (sleepTimeBeforeReconnection / 1000);

        boolean connect = false;

        /*
         * Main loop.
         */

        while (true) {
            WorkRequest w = null;

            try {
                waitForWork();

                if (isStopPending()) {
                    connection = null;
                    logger.fine("Database thread terminated");

                    return;
                }

                w = popRequest();

                if (w == null) {
                    logger.warning("woken up though workQ is empty!");

                    continue;
                }

                if (connect) {
                    establishConnection();
                    connect = false;
                }

                if (w.getType().equals("INSERT_HOURLY"))
                    insertHourlyMeasuresInDB((InsertHourlyRequest) w);

                if (w.getType().equals("INSERT_WEEKLY"))
                    insertWeeklyMeasuresInDB((InsertWeeklyRequest) w);

                if (w.getType().equals("PURGE"))
                    purgeDB();

                if (w.getType().equals("SELECT"))
                    getDataFromDB((SelectRequest) w);

                /*
                 * Update connectRetries, possibly using up-to-date values from
                 * Config.
                 */
                connectRetries = (Config.getDatabaseProbeInterval() +
                        Config.getDatabaseProbeTimeout() +
                        Config.getDatabaseStartTimeout() +
                        Config.getDatabaseStopTimeout()) /
                    (sleepTimeBeforeReconnection / 1000);

            } catch (SQLException e) {

                // requeue
                if (w != null)
                    requeueRequest(w);

                boolean goAway = false;

                try {
                    connect = handleSQLException(e);
                } catch (SQLException ee) {
                    goAway = true;
                }

                try {
                    connection.rollback();
                } catch (Exception ee) {
                    // may be a NullPointerException
                }

                logger.fine("Number of retries to do before giving up: " +
                    connectRetries);

                if (goAway || (connect && (--connectRetries < 0))) {
                    connection = null;
                    Config.lockModule();

                    return;
                }

                try {
                    Thread.sleep(sleepTimeBeforeReconnection);
                } catch (InterruptedException ee) {
                }

            } catch (GanymedeNoPrimaryException e) {

                /*
                 * getURLToDatabase(), called from establishConnection(), cannot
                 * retrieve the database host name. The database RG has no
                 * primary.
                 */

                // requeue
                if (w != null)
                    requeueRequest(w);

                logger.fine("Number of retries to do before giving up: " +
                    connectRetries);

                if (--connectRetries < 0) {
                    connection = null;
                    Config.lockModule();

                    return;
                }

                try {
                    Thread.sleep(sleepTimeBeforeReconnection);
                } catch (InterruptedException ee) {
                }

                connect = true;
            } catch (Exception e) {
                connection = null;
                Config.lockModule();

                return;
            }
        }

        // we never get here
    }

    /*
     * return the oldest time we want to keep data on (hourly).  This
     * value is dependent on the range of data we want to display (it
     * must be earlier than the oldest time displayed in the graph
     */
    private long getOldestTimeForHourlyTable() {

        /*
         * As displayed data is three full hours plus the current hour,
         * We must keep 4 full hours.
         */
        return ((System.currentTimeMillis() / 1000) - (4 * (60 * 60)));
    }

    /* return the oldest time we want to display data (hourly). */
    private long getOldestTimeForHourlyDisplay() {

        /* this integer is expressing current time in hours */
        long hours = System.currentTimeMillis() / (1000 * 60 * 60);

        /* let's go 3 full hours in the past */
        return (60 * 60 * (hours - 3));
    }

    /* return the oldest time we want to keep data on (weekly). */
    private long getOldestTimeForWeeklyTable() {

        /*
         * As displayed data is 7 full days plus the current day,
         * We must keep 8 full days.
         */
        return ((System.currentTimeMillis() / 1000) - (60 * 60 * 24 * 8));
    }


    /* return the oldest time we want to display data (weekly). */
    private long getOldestTimeForWeeklyDisplay() {

        /* this integer is expressing current time in days */
        long days = System.currentTimeMillis() / (1000 * 60 * 60 * 24);

        /* let's go 7 full days in the past */
        return (60 * 60 * 24 * (days - 7));
    }

    /**
     * Establish a connection to the DataBase server, and places the connection
     * handle in the connection attribute
     */
    private void establishConnection() throws SQLException, Exception {
        Connection conn = null;

        String dbURL = Config.getURLToDatabase(); // can throw Exception
        logger.fine("establishing connection to DB (" + dbURL + ") [START]");

        conn = DriverManager.getConnection(dbURL);
        logger.finer("establishing connection to DB [getConnection] done");

        // do not use autocommit mode
        conn.setAutoCommit(false);

        // use uncommitted read transaction mode
        conn.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED);
        logger.finest("establishing connection to DB [setIsolation] done");

        // try to rollback on some previous transaction
        conn.rollback();
        logger.finest("establishing connection to DB [rollback] done");

        connection = conn;

        logger.info("connection to DB established");
    }

    /**
     * Handle an SQLException
     *
     * @param  e  the SQLException to handle
     *
     * @return  boolean If the SQLException can be handled, true is returned if
     * the caller can retry the operation that caused the exception, and false
     * otherwise. If the SQLException cannot be handled, it is thrown and the
     * caller gets responsible for it; in practise the caller would call
     * lockModule() in such cases.
     */
    private boolean handleSQLException(SQLException e) throws SQLException {
        SQLException iter = e;

        logger.fine("handling SQLException...");
        logger.throwing(getClass().getName(), "handleSQLException", e);

        while (iter != null) {
            String msg = iter.getMessage();
            String state = iter.getSQLState();

            String format = (iter == e) ? "ExceptionMessage: "
                                        : "NextExceptionMessage: ";

            logger.fine(format + msg);
            logger.fine("ExceptionSQLState: " + state);

            if (state != null) {

                /*
                 * Exception has associated SQL State. Good.
                 */

                // detect connection exception
                if (state.matches("^08")) {
                    logger.fine("ConnectionException detected");

                    return true;
                }

                // detect DisConnectException
                if (state.matches("58017")) {
                    logger.fine("DisconnectException detected");

                    return true;
                }

                // detect lock wait timeout
                if (state.equals("40XL1") || state.equals("40XL2")) {
                    logger.fine("lock wait timed out");

                    return false;
                }

            } else {

                /*
                 * no SQL State available. This code is mostly
                 * Derby-specific then.
                 *
                 * Note: when reconnecting to the Derby server, a
                 * PriviledgedActionException ("Error opening socket...")
                 * sometimes triggers, immediately reconnecting often
                 * succeeds. 
                 */

                if (msg.matches(".*communication error.*") ||
                        msg.matches(".*connection closed.*") ||
                        msg.matches(".*Error opening socket.*")) {
                    logger.fine("ConnectionException detected");

                    return true;
                }
            }

            iter = iter.getNextException();
        }

        /*
         * Don't know what to do with that SQLException. Throw the exception.
         * But rollback before to release locks possibly hold in the database
         * server.
         */

        try {
            connection.rollback();
        } catch (Exception se) {
            // can be NullPointerException
        }

        logger.throwing(getClass().getName(), "handleSQLException", e);
        throw e;
    }

    /**
     * Some SQL string to ease modularity
     *
     * @param  none
     *
     * @return  the seqnum of this node
     */

    private String getSelectNodeIdSQL(String name) {
        return "SELECT Id FROM Nodes WHERE (name='" + name + "')";
    }

    private String getSelectNodeSQL() {

        String localPhysNodeName = Config.getLocalNode();

        return "SELECT id, name FROM Nodes" + " WHERE (nodes.name='" +
            localPhysNodeName + "'" + " OR nodes.name LIKE '" +
            localPhysNodeName + ":%')";
    }

    private String getInsertNodeSQL(String name, int physicalNodeId) {
        return "INSERT INTO Nodes (name, physId, description)" + " VALUES ('" +
            name + "'," + physicalNodeId + ",'no desc')";
    }

    private String getSelectMOIdSQL(String name, int MOTypeId, int nodeId) {
        return "SELECT Id FROM ManagedObjects" + " WHERE (name='" + name +
            "')" + " AND (type=" + MOTypeId + ")" + " AND (node=" + nodeId +
            ")";
    }

    private String getSelectMOSQL() {

        String localPhysNodeName = Config.getLocalNode();

        return "SELECT managedobjects.id, managedobjects.name," +
            " motypes.name, nodes.name" +
            " FROM ManagedObjects, MOTypes, Nodes" +
            " WHERE (nodes.id=managedobjects.node)" +
            " AND (motypes.id=managedobjects.type)" + " AND (nodes.name='" +
            localPhysNodeName + "'" + " OR nodes.name LIKE '" +
            localPhysNodeName + ":%')";
    }

    private String getInsertMOSQL(String name, int MOTypeId, int nodeId) {
        return "INSERT INTO ManagedObjects " +
            "(name, node, description, type) VALUES ('" + name + "'," + nodeId +
            ",'NO DESCRIPTION'," + MOTypeId + ")";
    }

    private String getSelectKIIdSQL(String name) {
        return "SELECT id FROM KeyIndicators WHERE (name='" + name + "')";
    }

    private String getSelectKISQL() {
        return "SELECT id, name FROM KeyIndicators";
    }

    private String getInsertKISQL(String name, String desc) {
        return ("INSERT INTO KeyIndicators (name, description) VALUES ('" +
                name + "','" + desc + "')");
    }

    private String getSelectMOTypeIdSQL(String name) {
        return "SELECT id from MoTypes WHERE (name='" + name + "')";
    }

    private String getSelectMOTypeSQL() {
        return "SELECT id, name from MOTypes";
    }

    private String getInsertMOTypeSQL(String name) {
        return ("INSERT INTO MOTypes (name) VALUES ('" + name + "')");
    }

    /**
     * Cache management
     */
    private void cacheKIInfo(String moType, String name, KIInfo ki) {
        KIInfoCache.put(moType + "##" + name, ki);
        logger.finer("caching: " + moType + "##" + name);
    }

    private KIInfo getKIInfoFromCache(String moType, String name) {
        return ((KIInfo) KIInfoCache.get(moType + "##" + name));
    }

    /**
     * This function fills the identifier caches for the nodes, key indicators,
     * managed objects, and managed object types.
     */
    private void fillCaches() throws JMException, SQLException {

        /*
         * Creating one statement and reusing it for every
         * excuteQuery results in prepatedStatement leaks
         * in the Derby server, so be careful and use
         * Derby's runtimeinfo tool if you're to do some
         * tweaking in here.
         */

        // Fill node cache
        Statement stmt = connection.createStatement();
        ResultSet result = stmt.executeQuery(getSelectNodeSQL());

        while (result.next()) {
            cache.cacheNodeId(/* node name */result.getString(2),
                /* node id */ result.getInt(1));
        }

        result.close();
        stmt.close();

        // Fill KI cache
        stmt = connection.createStatement();
        result = stmt.executeQuery(getSelectKISQL());

        while (result.next()) {
            cache.cacheKIId(/* KI name */result.getString(2),
                /* KI id */ result.getInt(1));
        }

        result.close();
        stmt.close();

        // Fill MO cache
        stmt = connection.createStatement();
        result = stmt.executeQuery(getSelectMOSQL());

        while (result.next()) {
            cache.cacheMOId(/* MO name */result.getString(2),
                /* MOT name */ result.getString(3),
                /* node name */ result.getString(4),
                /* MO id */ result.getInt(1));
        }

        result.close();
        stmt.close();

        // Fill MOType cache
        stmt = connection.createStatement();
        result = stmt.executeQuery(getSelectMOTypeSQL());

        while (result.next()) {
            cache.cacheMOTypeId(/* MOT name */result.getString(2),
                /* MOT id */ result.getInt(1));
        }

        result.close();
        stmt.close();
    }

    /**
     * insert a new seqNum in the database (for the current host)
     */
    private void insertSeqNum(int physicalNodeId, int hourlySeqNum,
        int weeklySeqNum) throws SQLException {

        Statement stmt = connection.createStatement();
        StringBuffer query = new StringBuffer(
                "INSERT INTO LatestSeqNums (NODE, Hourly, Weekly) VALUES(")
            .append(physicalNodeId).append(", ").append(hourlySeqNum).append(
                ", ").append(weeklySeqNum).append(")");
        stmt.executeUpdate(query.toString());
        stmt.close();

        connection.commit();
    }

    /**
     * This method is called at initialization
     *
     * @param  none
     */
    private void initSeqNum() throws SQLException {

        logger.fine("initializing the sequence numbers");

        // get physical node id
        String physicalNodeName = Config.getLocalNode();
        int physicalNodeId = cache.getNodeId(physicalNodeName);

        if (physicalNodeId == -1) {

            // physical node is not in the DB, insert it
            physicalNodeId = insertPhysicalNodeIntoDB(physicalNodeName);

            // insert into cache also
            cache.cacheNodeId(physicalNodeName, physicalNodeId);
        }

        connection.commit(); // insertPhysicalNodeIntoDB did not commit

        Statement stmt = connection.createStatement();
        ResultSet result = stmt.executeQuery(
                "SELECT Hourly, Weekly FROM LatestSeqNums " + "WHERE (node=" +
                physicalNodeId + ")");

        if (result.next() == false) {
            insertSeqNum(physicalNodeId, 1000, 1000);
            cache.weeklySeqNum = 1000;
            cache.hourlySeqNum = 1000;
        } else {
            cache.hourlySeqNum = result.getInt(1);
            cache.weeklySeqNum = result.getInt(2);
        }

        result.close();
        stmt.close();
    }

    /**
     * Insert the new set of measures from one sequence number, also update
     * sequence number in the DB
     */
    private void insertHourlyMeasuresInDB(InsertHourlyRequest ir)
        throws SQLException {

        /*
         * Column |  Type   | Modifiers
         * -------+---------+-----------
         * node   | integer | not null
         * mo     | integer | not null
         * ki     | integer | not null
         * seqnum | integer |
         * time   | integer |
         * value  | longint |
         */

        PreparedStatement ps = null;

        try {

            logger.finest("pushing Hourly Data to DB [START]");

            /*
             * Get physical node identifier.
             */
            String physicalNodeName = Config.getLocalNode();
            int physicalNodeId = cache.getNodeId(physicalNodeName);

            if (physicalNodeId == -1) {

                // physical node is not in the DB, insert it
                physicalNodeId = insertPhysicalNodeIntoDB(physicalNodeName);

                // insert into cache also
                cache.cacheNodeId(physicalNodeName, physicalNodeId);
            }

            // Update sequence number
            logger.finest("pushing " + ir.seqNum + " to DB [START]");

            Statement stmt = connection.createStatement();
            stmt.executeUpdate("UPDATE LatestSeqNums SET Hourly = " +
                ir.seqNum + " WHERE (node = " + physicalNodeId + ")");
            stmt.close();

            /*
             *
             * Insert the Sensor results.
             *
             */

            // create object identifier temporary cache
            DataBaseCache cacheTemp = new DataBaseCache("Temporary");

            ps = connection.prepareStatement(
                    "INSERT INTO hourlymeasures VALUES (?, ?, " + ir.seqNum +
                    ", ?, ?, ?)");

            Enumeration e = ir.samples.elements();

            while (e.hasMoreElements()) {
                SensorResult sr = (SensorResult) e.nextElement();

                /*
                 * Get Key Indicator Identifier.
                 */
                String kiName = sr.getKeyIndicatorName();
                int kiId = cache.getKIId(kiName);

                if (kiId == -1)
                    kiId = cacheTemp.getKIId(kiName);

                if (kiId == -1) {

                    // Cache miss. KI can be either in the DB, if
                    // another node already inserted it, or not,
                    // if we are the first to encounter it
                    kiId = insertKIIntoDB(kiName);
                    cacheTemp.cacheKIId(kiName, kiId);
                }

                /*
                 * Get Managed Object Type Identifier.
                 */
                String moTypeName = sr.getMOType();
                int moTypeId = cache.getMOTypeId(moTypeName);

                if (moTypeId == -1)
                    moTypeId = cacheTemp.getMOTypeId(moTypeName);

                if (moTypeId == -1) {

                    // Cache miss. MOT can be either in the DB, if
                    // another node already inserted it, or not,
                    // if we are the first to encounter it
                    moTypeId = insertMOTypeIntoDB(moTypeName);
                    cacheTemp.cacheMOTypeId(moTypeName, moTypeId);
                }

                /*
                 * Get Node Identifier.
                 */

                // initialize nodeName and nodeId
                String nodeName = physicalNodeName;
                int nodeId = physicalNodeId;

                // get logical node identifier, if any
                String logicalNodeName = sr.getLogicalNodeName();

                if (logicalNodeName != null) {
                    int logicalNodeId = cache.getNodeId(logicalNodeName);

                    if (logicalNodeId == -1)
                        logicalNodeId = cacheTemp.getNodeId(logicalNodeName);

                    if (logicalNodeId == -1) {

                        // Cache miss. Logical node isn't in DB, insert it
                        logicalNodeId = insertLogicalNodeIntoDB(logicalNodeName,
                                physicalNodeId);
                        cacheTemp.cacheNodeId(logicalNodeName, logicalNodeId);
                    }

                    nodeName = logicalNodeName;
                    nodeId = logicalNodeId;
                }

                /*
                 * Get Managed Object Identifier.
                 */
                String moName = sr.getMonitoredEntity();
                int moId = cache.getMOId(moName, moTypeName, nodeName);

                if (moId == -1)
                    moId = cacheTemp.getMOId(moName, moTypeName, nodeName);

                if (moId == -1) {

                    // Cache miss. MO isn't in DB, insert it
                    moId = insertMOIntoDB(moName, moTypeId, nodeId);
                    cacheTemp.cacheMOId(moName, moTypeName, nodeName, moId);
                }

                /*
                 * At this point if either moId or kiId is equal to -1 it
                 * means there's a bug here in DataBase,java. The bug
                 * will be detected thanks to the SQLException triggered
                 * as a result of the -1 value.
                 */
                ps.setInt(1, moId);
                ps.setInt(2, kiId);
                ps.setInt(3, (int) (sr.getDate().getTime() / 1000));
                ps.setLong(4, sr.getValue().longValue());
                ps.setLong(5, sr.getUpperBound().longValue());
                ps.addBatch();
            }

            /* result should be tested here */

            ps.executeBatch();
            ps.close();

            connection.commit();
            logger.finest("pushing " + ir.seqNum + " to DB [DONE]");

            cache.integrate(cacheTemp);

        } catch (SQLException e) {

            // trying to circumvent 
            // http://issues.apache.org/jira/browse/DERBY-210 
            try {
                ps.close();
            } catch (NullPointerException npe) {
                // occurs if the SQLException occured
                // before preparing Statement ps
            } catch (SQLException se) {
                // that's ok, if we lost the connection
                // we'll reconnect later
            }

            throw e;
        }
    }

    /**
     * Insert the new set of measures from one sequence number, also update
     * sequence number in the DB
     */
    private void insertWeeklyMeasuresInDB(InsertWeeklyRequest wr)
        throws SQLException {

        /**
         * CREATE TABLE WeeklyMeasures (
         * MO   INTEGER NOT NULL,       -- MO is instanciated for as node
         * KI   INTEGER NOT NULL,
         * Time  INTEGER,
         * Value BIGINT,
         * UpperBound BIGINT DEFAULT 0,         -- UPPER BOUND
         * minValue BIGINT,
         * maxValue BIGINT,
         * avgValue BIGINT
         * varValue BIGINT) ;
         */


        PreparedStatement ps = null;

        try {

            logger.finest("pushing Weekly Data to DB [START]");

            /*
             * Get physical node identifier.
             */
            String physicalNodeName = Config.getLocalNode();
            int physicalNodeId = cache.getNodeId(physicalNodeName);

            if (physicalNodeId == -1) {

                // physical node is not in the DB, insert it
                physicalNodeId = insertPhysicalNodeIntoDB(physicalNodeName);
                cache.cacheNodeId(physicalNodeName, physicalNodeId);
            }

            /*
             * Update sequence number.
             */
            Statement stmt = connection.createStatement();
            stmt.executeUpdate("UPDATE LatestSeqNums SET Weekly = " +
                wr.seqNum + " WHERE (node = " + physicalNodeId + ")");
            stmt.close();

            /*
             * Insert the Aggregate results.
             */

            // create object identifier temporary cache
            DataBaseCache cacheTemp = new DataBaseCache("Temporary");

            ps = connection.prepareStatement(
                    "INSERT INTO weeklymeasures (MO, KI, time, Seqnum, Value," +
                    " UpperBound, minValue, maxValue, avgValue, devValue)" +
                    " VALUES (?, ?, ?," + wr.seqNum + ", ?, ?, ?, ?, ?, ?) ");

            Enumeration e = wr.samples.elements();

            while (e.hasMoreElements()) {
                AggregateResult ar = (AggregateResult) e.nextElement();

                /*
                 * Get Key Indicator Identifier.
                 */
                String kiName = ar.getKeyIndicatorName();
                int kiId = cache.getKIId(kiName);

                if (kiId == -1)
                    kiId = cacheTemp.getKIId(kiName);

                if (kiId == -1) {

                    // Cache miss. KI can be either in the DB, if
                    // another node already inserted it, or not,
                    // if we are the first to encounter it
                    kiId = insertKIIntoDB(kiName);
                    cacheTemp.cacheKIId(kiName, kiId);
                }

                /*
                 * Get Managed Object Type Identifier.
                 */
                String moTypeName = ar.getMOType();
                int moTypeId = cache.getMOTypeId(moTypeName);

                if (moTypeId == -1)
                    moTypeId = cacheTemp.getMOTypeId(moTypeName);

                if (moTypeId == -1) {

                    // Cache miss. MOT can be either in the DB, if
                    // another node already inserted it, or not,
                    // if we are the first to encounter it
                    moTypeId = insertMOTypeIntoDB(moTypeName);
                    cacheTemp.cacheMOTypeId(moTypeName, moTypeId);
                }

                /*
                 * Get Node Identifier.
                 */
                String nodeName = physicalNodeName;
                int nodeId = physicalNodeId;

                String logicalNodeName = ar.getLogicalNodeName();

                if (logicalNodeName != null) {
                    int logicalNodeId = cache.getNodeId(logicalNodeName);

                    if (logicalNodeId == -1)
                        logicalNodeId = cacheTemp.getNodeId(logicalNodeName);

                    if (logicalNodeId == -1) {

                        // Cache miss. Logical node isn't in DB, insert it
                        logicalNodeId = insertLogicalNodeIntoDB(logicalNodeName,
                                physicalNodeId);
                        cacheTemp.cacheNodeId(logicalNodeName, logicalNodeId);
                    }

                    nodeName = logicalNodeName;
                    nodeId = logicalNodeId;
                }

                /*
                 * Get Managed Object Identifier.
                 */
                String moName = ar.getMonitoredEntity();
                int moId = cache.getMOId(moName, moTypeName, nodeName);

                if (moId == -1)
                    moId = cacheTemp.getMOId(moName, moTypeName, nodeName);

                if (moId == -1) {

                    // Cache miss. MO isn't in DB, insert it
                    moId = insertMOIntoDB(moName, moTypeId, nodeId);
                    cacheTemp.cacheMOId(moName, moTypeName, nodeName, moId);
                }

                /*
                 * At this point if either moId or kiId is equal to -1 it
                 * means there's a bug here in DataBase,java. The bug
                 * will be detected thanks to the SQLException triggered
                 * as a result of the -1 value.
                 *
                 * MO   INTEGER NOT NULL,   -- MO is instanciated for as node
                 * KI   INTEGER NOT NULL,
                 * Time  INTEGER,
                 * Value BIGINT,
                 * UpperBound BIGINT DEFAULT 0,         -- UPPER BOUND
                 * minValue BIGINT,
                 * maxValue BIGINT,
                 * avgValue BIGINT
                 * varValue BIGINT)
                 */

                ps.setInt(1, moId);
                ps.setInt(2, kiId);
                ps.setInt(3, (int) ar.getDate());
                ps.setLong(4, ar.getValue());
                ps.setLong(5, ar.getUpperBound());
                ps.setLong(6, ar.getMin());
                ps.setLong(7, ar.getMax());
                ps.setLong(8, ar.getAverage());
                ps.setLong(9, ar.getDeviation());
                ps.addBatch();
            }

            /* result should be tested here */

            ps.executeBatch();
            ps.close();

            connection.commit();
            logger.finest("pushing Weekly Data to DB [DONE]");

            cache.integrate(cacheTemp);

        } catch (SQLException e) {
            try {
                ps.close();
            } catch (NullPointerException npe) {
                // occurs if the SQLException occured
                // before preparing Statement ps
            } catch (SQLException se) {
                // that's ok, if we lost the connection
                // we'll reconnect later
            }

            throw e;
        }
    }

    /**
     * Insert physical local in the DB, and return its id
     *
     * @param  name  the name of the physical node to insert
     *
     * @return  the id of the inserted node, -1 on error This method does not
     * commit the transaction, so it is the caller's respnsability to commit.
     */
    private int insertPhysicalNodeIntoDB(String name) throws SQLException {

        logger.fine("insert physical node " + name + " into DB");

        Statement stmt = connection.createStatement();
        stmt.executeUpdate(getInsertNodeSQL(name, /* dont know it yet */
                -1));

        ResultSet result = stmt.executeQuery(getSelectNodeIdSQL(name));

        if (result.next() == false) {
            result.close();
            stmt.close();

            return -1;
        }

        int physicalNodeId = result.getInt(1);

        // update the physId attribute
        stmt.executeUpdate("UPDATE Nodes SET physid=" + physicalNodeId +
            " WHERE (id=" + physicalNodeId + ")");

        result.close();
        stmt.close();

        return physicalNodeId;
    }

    /**
     * Insert a logical node in the DB, and return its id
     *
     * @param  name  the name of the logical node to insert
     * @param  physicalNodeId  the id of the physical node
     *
     * @return  the id of the inserted node, -1 on error This method does not
     * commit the transaction, so it is the caller's respnsability to commit.
     */
    private int insertLogicalNodeIntoDB(String name, int physicalNodeId)
        throws SQLException {

        logger.fine("insert logical node " + name + " into DB");

        Statement stmt = connection.createStatement();
        stmt.executeUpdate(getInsertNodeSQL(name, physicalNodeId));

        ResultSet result = stmt.executeQuery(getSelectNodeIdSQL(name));

        if (result.next() == false) {
            result.close();
            stmt.close();

            return -1;
        }

        int logicalNodeId = result.getInt(1);
        result.close();
        stmt.close();

        return logicalNodeId;
    }

    /**
     * Acquire exclusive DB locks in the tables keyindicators amd motypes.
     */
    private void acquireDBLocks() throws SQLException {

        /*
         * This method is called in the methods insertKIIntoDB() and
         * insertMOTIntoDB() to avoid the following deadlock situation in
         * insertHourlyMeasuresInDB() and insertWeeklyMeasuresInDB().
         *
         * ---
         * XID1 XID2
         * X lock ki (granted)
         * X lock mot (granted)
         * S lock ki  (wait)
         * S lock mot (wait)
         * ---
         *
         * Note: the shared lock (S lock) are acquired as a result of the
         * executeBatch() call.
         *
         */

        Statement stmt = connection.createStatement();

        stmt.execute("LOCK TABLE keyindicators IN EXCLUSIVE MODE");
        stmt.execute("LOCK TABLE motypes IN EXCLUSIVE MODE");
        stmt.close();
    }

    /**
     * Insert a KI into the DB if not already inserted
     *
     * @param  name  name of the KI to be inserted in the DB
     *
     * @return  KI identifier, or -1 in case of error This method does not
     * commit the transaction, so it is the caller's respnsability to commit.
     */
    private int insertKIIntoDB(String name) throws SQLException {

        acquireDBLocks();

        Statement stmt = connection.createStatement();

        ResultSet result = stmt.executeQuery(getSelectKIIdSQL(name));

        if (result.next() == false) {
            stmt.executeUpdate(getInsertKISQL(name, "no desc"));

            result.close();
            result = stmt.executeQuery(getSelectKIIdSQL(name));

            if (result.next() == false) {
                result.close();
                stmt.close();

                return -1;
            }
        }

        int id = result.getInt(1);
        result.close();
        stmt.close();

        return id;
    }

    /**
     * Insert a MO Type into the DB if not already inserted
     *
     * @param  name  name of the MO Type to be inserted in the DB
     *
     * @return  MO Type identifier, or -1 in case of error This method does not
     * commit the transaction, so it is the caller's respnsability to commit.
     */
    private int insertMOTypeIntoDB(String name) throws SQLException {

        acquireDBLocks();

        Statement stmt = connection.createStatement();

        ResultSet result = stmt.executeQuery(getSelectMOTypeIdSQL(name));

        if (result.next() == false) {

            // MOType isn't in DB, insert it
            stmt.executeUpdate(getInsertMOTypeSQL(name));

            result.close();
            result = stmt.executeQuery(getSelectMOTypeIdSQL(name));

            if (result.next() == false) {
                result.close();
                stmt.close();

                return -1;
            }
        }

        int id = result.getInt(1);
        result.close();
        stmt.close();

        return id;
    }

    /**
     * Insert a MO into the DB
     *
     * @param  name  name of the MO to be inserted in the DB
     * @param  moTypeId  the id of the managed object type
     * @param  nodeId  the id of the node
     *
     * @return  MO identifier, or -1 in case of error This method does not
     * commit the transaction, so it is the caller's responsability to commit.
     */
    private int insertMOIntoDB(String name, int moTypeId, int nodeId)
        throws SQLException {

        logger.fine("insert MO " + name + " into DB");

        Statement stmt = connection.createStatement();
        stmt.executeUpdate(getInsertMOSQL(name, moTypeId, nodeId));

        ResultSet result = stmt.executeQuery(getSelectMOIdSQL(name, moTypeId,
                    nodeId));

        if (result.next() == false) {
            result.close();
            stmt.close();

            return -1;
        }

        int id = result.getInt(1);
        result.close();
        stmt.close();

        return id;
    }

    /**
     * Purge old data in the DB.
     */
    private void purgeDB() throws SQLException {

        /*
         * the selection is on the time in the DB and not the time
         * *OF* the DB host. we delete value that are olderf than 65 minutes
         */

        // TODO in derby: the following cleaning may be done call
        // SYSCS_UTIL.SYSCS_COMPRESS_TABLE('GANYMEDE', 'HOURLYMEASURES', 0);

        logger.fine("purging...");

        Statement stmt;
        int nbPurges;

        stmt = null;
        nbPurges = -1;

        try {
            stmt = connection.createStatement();
            nbPurges = stmt.executeUpdate("DELETE FROM hourlymeasures" +
                    " WHERE time < " + getOldestTimeForHourlyTable());
            stmt.close();
            connection.commit();

        } catch (SQLException e) {

            // be paranoid, close statement
            try {
                stmt.close();
            } catch (NullPointerException npe) {
                // occurs if the SQLException occured
                // in createStatement
            } catch (SQLException se) {
                // that's ok, if we lost the connection
                // we'll reconnect later
            }

            throw e;
        }

        logger.fine(nbPurges + " entries purged in table hourlymeasures");

        stmt = null;
        nbPurges = -1;

        try {
            stmt = connection.createStatement();
            nbPurges = stmt.executeUpdate("DELETE FROM weeklymeasures" +
                    " WHERE time < " + getOldestTimeForWeeklyTable());
            stmt.close();
            connection.commit();

        } catch (SQLException e) {

            // be paranoid, close statement
            try {
                stmt.close();
            } catch (NullPointerException npe) {
                // occurs if the SQLException occured
                // in createStatement
            } catch (SQLException se) {
                // that's ok, if we lost the connection
                // we'll reconnect later
            }

            throw e;
        }

        logger.fine(nbPurges + " entries purged in weeklymeasures");
    }

    private MeasureSet getMeasureSet(Vector vect, String node, String KI,
        String MOType, String MO) {
        MeasureId lookedMeasureId = new MeasureId(MOType, MO, KI, node);

        for (int i = 0; i < vect.size(); i++) {
            MeasureSet mset = (MeasureSet) vect.elementAt(i);

            if (lookedMeasureId.equals(mset.getId()))
                return (mset);
        }

        return (null);
    }

    /**
     * Create a SQL filter on given measureids returned string is null if
     * something is invalid
     */
    private String getDataRequestFilter(MeasureId mids[]) {
        boolean isRequestValid = false;

        String n; // node name
        String k; // key indicator name
        String t; // managed object type
        String m; // managed object name
        String fullFilter = "";

        /*
         * Convert MeasureSets to HashSet to remove doubles
         */

        for (int i = 0; i < mids.length && i < MEASURE_ID_SIZE_LIMIT; i++) {
            n = mids[i].getNodeName();
            t = mids[i].getManagedObjectType();
            m = mids[i].getManagedObject();
            k = mids[i].getKeyIndicator();

            boolean isValid = false;
            String filter = ""; // TODO: could be a StringBuffer

            if (n != null) {
                isValid = true;
                filter += "nodes.name = '" + n + "' ";
            }

            if (t != null) {

                if (isValid == true)
                    filter += "AND ";

                isValid = true;
                filter += "motypes.name = '" + t + "' ";
            }

            if (m != null) {

                if (isValid == true)
                    filter += "AND ";

                isValid = true;
                filter += "managedobjects.name = '" + m + "' ";
            }

            if (k != null) {

                if (isValid == true)
                    filter += "AND ";

                isValid = true;
                filter += "keyindicators.name = '" + k + "'";
            }

            /* if this is not the first criterion, add a OR */
            if (i != 0) {
                fullFilter += " OR ";
            }

            if (filter.length() != 0) {
                fullFilter += "(" + filter + ")";
            }
        }

        return fullFilter;
    }

    /**
     * This method is redirecting to Hourly or weekly request
     */
    private void getDataFromDB(SelectRequest sr) throws SQLException {

        try {

            if (sr.level.equals(MeasureLevel.HOURLY))
                getHourlyDataFromDB(sr);
            else
                getWeeklyDataFromDB(sr);

            logger.fine("getDataFromDB notify result is ready");
        } finally {
            sr.notifyDataIsReady();
        }

    }

    /*
     * TODO: This is assuming that the request will ask
     * TODO: for the same KIs for all nodes and all MO
     */
    private void getHourlyDataFromDB(SelectRequest sr) throws SQLException {
        MeasureSet results[] = null;

        String n = null; // node name
        String k = null; // key indicator name
        String t = null; // managed object type
        String m = null; // managed object name

        String columns = null;
        String tables = null;
        String conditions = null;
        String order = null;

        String filter = getDataRequestFilter(sr.mids);

        columns = "nodes.name, keyindicators.name, " +
            "motypes.name, managedobjects.name, seqnum, " +
            "time, value, upperbound ";

        tables = "hourlymeasures, motypes, managedobjects, " +
            "nodes, keyindicators ";

        conditions = "hourlymeasures.mo = managedobjects.id " +
            "AND nodes.id = managedobjects.node " +
            "AND managedobjects.type = motypes.id " +
            "AND keyindicators.id = hourlymeasures.ki ";

        sr.result = null;

        if (sr.latest == true) {
            tables = tables + ", latestseqnums ";
            conditions = conditions + "AND nodes.physid = latestseqnums.node " +
                "AND hourlymeasures.seqnum = latestseqnums.hourly ";
        }

        if (!((filter == null) || (filter.length() == 0))) {
            conditions += "AND (" + filter + ")";
        }

        // Add time filtering if needed
        if (sr.latest == false) {

            if ((sr.begin == null) && (sr.end == null)) {
                conditions += " AND time > " + getOldestTimeForHourlyDisplay();
            } else {

                if (sr.begin != null) {
                    conditions += " AND time > " + sr.begin.toString();
                }

                if (sr.end != null) {
                    conditions += " AND time < " + sr.end.toString();
                }
            }

            order = "time";
        }

        String request = "SELECT " + columns + "FROM " + tables + "WHERE " +
            conditions;

        if (order != null) {
            request += " ORDER BY " + order;
        }

        Iterator ite;
        Vector vect = new Vector();

        logger.fine("getDataFromDB called");

        logger.finer(request);

        Statement stmt = null;
        ResultSet result = null;

        try {
            stmt = connection.createStatement();
            logger.finer("getDataFromDB execute Request [START]");
            result = stmt.executeQuery(request);
            logger.finer("getDataFromDB execute Request [DONE]");

            while (result.next()) {
                n = result.getString(1); // node name
                k = result.getString(2); // key indicator name
                t = result.getString(3); // managed object type
                m = result.getString(4); // managed object name

                KIInfo ki = getKIInfoFromCache(t, k);

                if (ki == null) {

                    // cache miss
                    ki = SensorBase.lookupKIInfo(t, k);

                    if (ki == null) {

                        // internal bug
                        logger.warning("KIInfo lookup failed!");

                        continue;
                    }

                    cacheKIInfo(t, k, ki);
                }

                MeasureSet mset = getMeasureSet(vect, n, k, t, m);

                if (mset == null) {
                    mset = new MeasureSet();
                    mset.setId(new MeasureId(t, m, k, n));
                    mset.setLevel(MeasureLevel.HOURLY);

                    vect.add(mset);
                }

                int seqnum = result.getInt(5); // seqnum
                int date = result.getInt(6); // date
                long value = result.getLong(7); // value
                long upperBound = result.getLong(8); // upperBound

                double divisor = ki.getDivisor().doubleValue();

                if (divisor == 0.)
                    divisor = 1.;

                mset.add(seqnum, date, (double) value / divisor,
                    (double) upperBound / divisor);
            }

            result.close();
            stmt.close();
            logger.finer("getDataFromDB gather results [DONE]");
        } catch (SQLException e) {

            logger.warning(e.toString());
            logger.warning("Request was : " + request);
            logger.throwing(getClass().getName(), "getHourlyDataFromDB", e);

            // protect from Derby's memory leak
            try {
                result.close();
                stmt.close();
            } catch (NullPointerException npe) {
                // occurs if the SQLException occured
                // before preparing Statement ps
            } catch (SQLException se) {
                // that's ok, if we lost the connection
                // we'll reconnect later
            }

            throw e;
        }

        try {
            results = new MeasureSet[vect.size()];

            for (int i = 0; i < vect.size(); i++)
                results[i] = (MeasureSet) vect.elementAt(i);
        } catch (Exception e) {
            logger.warning(e.toString());
        }

        sr.result = results;

        return;
    }

    private void getWeeklyDataFromDB(SelectRequest sr) throws SQLException {
        MeasureSet results[] = null;

        String n = null; // node name
        String k = null; // key indicator name
        String t = null; // managed object type
        String m = null; // managed object name

        String filter = getDataRequestFilter(sr.mids);
        String columns = null;
        String tables = null;
        String conditions = null;
        String order = null;


        Iterator ite = null;
        Vector vect = new Vector();
        Statement stmt = null;
        ResultSet result = null;

        columns = "nodes.name, keyindicators.name, " +
            "motypes.name, managedobjects.name, seqnum, " +
            "time, value, upperbound ";

        tables = "weeklymeasures, motypes, managedobjects, " +
            "nodes, keyindicators ";

        conditions = "weeklymeasures.mo = managedobjects.id " +
            "AND nodes.id = managedobjects.node " +
            "AND managedobjects.type = motypes.id " +
            "AND keyindicators.id = weeklymeasures.ki ";

        sr.result = null;

        if (sr.latest == true) {
            tables = tables + ", latestseqnums ";
            conditions = conditions + "AND nodes.physid = latestseqnums.node " +
                "AND weeklymeasures.seqnum = latestseqnums.weekly ";
        }

        if (!((filter == null) || (filter.length() == 0))) {
            conditions += "AND (" + filter + ")";
        }

        logger.fine("getDataFromDB called");

        /* Add time filtering id needed */
        if (sr.latest == false) {

            if ((sr.begin == null) && (sr.end == null)) {
                conditions += " AND time > " + getOldestTimeForWeeklyDisplay();
            } else {

                if (sr.begin != null) {
                    conditions += " AND time > " + sr.begin.toString();
                }

                if (sr.end != null) {
                    conditions += " AND time < " + sr.end.toString();
                }
            }

            order = "time";
        }

        String request = "SELECT " + columns + "FROM " + tables + "WHERE " +
            conditions;

        if (order != null) {
            request += " ORDER BY " + order;
        }

        logger.finer(request);

        try {
            stmt = connection.createStatement();
            logger.finer("getDataFromDB execute Request [START]");
            result = stmt.executeQuery(request);
            logger.finer("getDataFromDB execute Request [DONE]");

            while (result.next()) {
                n = result.getString(1); // node name
                k = result.getString(2); // key indicator name
                t = result.getString(3); // managed object type
                m = result.getString(4); // managed object name

                KIInfo ki = getKIInfoFromCache(t, k);

                if (ki == null) {

                    // cache miss
                    ki = SensorBase.lookupKIInfo(t, k);

                    if (ki == null) {

                        // internal bug
                        logger.warning("KIInfo lookup failed!");

                        continue;
                    }

                    cacheKIInfo(t, k, ki);
                }

                MeasureSet mset = getMeasureSet(vect, n, k, t, m);

                if (mset == null) {
                    mset = new MeasureSet();
                    mset.setId(new MeasureId(t, m, k, n));
                    mset.setLevel(MeasureLevel.HOURLY);

                    vect.add(mset);
                }

                int seqnum = result.getInt(5); // seqnum
                int date = result.getInt(6); // date
                long value = result.getLong(7); // value
                long upperBound = result.getLong(8); // upperBound

                double divisor = ki.getDivisor().doubleValue();

                if (divisor == 0.)
                    divisor = 1.;

                mset.add(seqnum, date, (double) value / divisor,
                    (double) upperBound / divisor);
            }

            result.close();
            stmt.close();
            logger.finer("getDataFromDB gather results [DONE]");
        } catch (SQLException e) {

            logger.warning(e.toString());
            logger.warning("Request was : " + request);
            logger.throwing(getClass().getName(), "getWeeklyDataFromDB", e);

            // protect from Derby's memory leak
            try {
                result.close();
                stmt.close();
            } catch (NullPointerException npe) {
                // occurs if the SQLException occured
                // before preparing Statement ps
            } catch (SQLException se) {
                // that's ok, if we lost the connection
                // we'll reconnect later
            }

            throw e;
        }

        try {
            results = new MeasureSet[vect.size()];

            for (int i = 0; i < vect.size(); i++)
                results[i] = (MeasureSet) vect.elementAt(i);
        } catch (Exception e) {
            logger.warning(e.toString());
        }

        sr.result = results;

        return;
    }
}
