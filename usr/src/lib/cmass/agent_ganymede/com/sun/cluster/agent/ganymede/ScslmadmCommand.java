/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ScslmadmCommand.java 1.11     08/05/20 SMI"
 *
 */

package com.sun.cluster.agent.ganymede;

import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.agent.JmxClient;

// Cacao
import com.sun.cacao.commandstream.Command;

import java.lang.Boolean;

import java.text.ParseException;

import java.util.StringTokenizer;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// JMX
import javax.management.MBeanServer;


/**
 * This class is the entry point from "cacaocsc". It is used to receive request
 * from a shell and returning answer (for instance data from DB).
 */
public class ScslmadmCommand implements Command {

    /* logging facility */
    private static final String logTag = "ScslmadmCommand";
    private static Logger logger = Logger.getLogger(GanymedeModule.DOMAIN +
            "." + logTag);

    /*
     * This string is prefixing errors sent back to the CLI. This string
     * is detected when received and managed.
     */
    private static final String CLI_ERROR = "ERROR: ";
    private static final String PROTOCOL_ERROR = CLI_ERROR + "1";
    private static final String INTERNAL_ERROR = CLI_ERROR + "2";

    private MBeanServer mbeanServer;

    public ScslmadmCommand(MBeanServer mbs) {
        mbeanServer = mbs;
    }

    public int execute(java.lang.String arguments, java.io.InputStream in,
        java.io.OutputStream out, java.io.OutputStream err, java.util.Map env)
        throws java.lang.Exception {

        String option = null;
        String suboption = null;
        String kiname = null;
        String node = null;
        String moname = null;
        String motype = null;
        Long startTime = null;
        Long stopTime = null;
        MeasureLevel level = MeasureLevel.HOURLY;


        logger.config(env.toString());
        logger.config("Command arguments: " + arguments);

        StringTokenizer st = new StringTokenizer(arguments);

        try {

            while (st.hasMoreTokens()) {
                String current = st.nextToken();

                if (current.equals("-start")) {

                    if (!st.hasMoreTokens()) {
                        err.write(PROTOCOL_ERROR.getBytes());
                        logger.severe("Missing argument for -start option.");

                        return (Command.ERROR_CODE);
                    }

                    startTime = Long.valueOf(st.nextToken());

                    continue;
                }

                if (current.equals("-stop")) {

                    if (!st.hasMoreTokens()) {
                        err.write(PROTOCOL_ERROR.getBytes());
                        logger.severe("Missing argument for -stop option.");

                        return (Command.ERROR_CODE);
                    }

                    stopTime = Long.valueOf(st.nextToken());

                    continue;
                }

                if (current.equals("-k")) {

                    if (!st.hasMoreTokens()) {
                        err.write(PROTOCOL_ERROR.getBytes());
                        logger.severe("Missing argument for -k option.");

                        return (Command.ERROR_CODE);
                    }

                    kiname = st.nextToken();

                    continue;
                }

                if (current.equals("-y")) {

                    if (!st.hasMoreTokens()) {
                        err.write(PROTOCOL_ERROR.getBytes());
                        logger.severe("Missing argument for -y option.");

                        return (Command.ERROR_CODE);
                    }

                    level = (st.nextToken().equals("latest"))
                        ? MeasureLevel.HOURLY : MeasureLevel.WEEKLY;

                    continue;
                }

                if (current.equals("-m")) {

                    if (!st.hasMoreTokens()) {
                        err.write(PROTOCOL_ERROR.getBytes());
                        logger.severe("Missing argument for -m option.");

                        return (Command.ERROR_CODE);
                    }

                    current = st.nextToken();

                    StringTokenizer st2 = new StringTokenizer(current, ",",
                            true);

                    while (st2.hasMoreTokens()) {
                        String current2 = st2.nextToken();

                        if (current2.equals(","))
                            continue;

                        // Type=Value
                        String token[] = current2.split("=");
                        String type = token[0];
                        String value = token[1];

                        if ((type.equals("object-instance")) ||
                                (type.equals("oi"))) {

                            if (moname == null)
                                moname = value;
                            else
                                moname = moname + "," + value;
                        } else if (type.equals("node")) {

                            if (node == null)
                                node = value;
                            else
                                node = node + "," + value;
                        } else if ((type.equals("object-type")) ||
                                (type.equals("ot"))) {

                            if (motype == null)
                                motype = value;
                            else {
                                err.write(PROTOCOL_ERROR.getBytes());

                                logger.severe("Only one object-type " +
                                    "can be specified\n");

                                return (Command.ERROR_CODE);
                            }

                        } else {
                            String oneWritenLine = CLI_ERROR + type +
                                " is not valid\n";

                            err.write(oneWritenLine.getBytes());

                            return (Command.ERROR_CODE);
                        }
                    }
                }
            }
        } catch (Exception e) {
            err.write(INTERNAL_ERROR.getBytes());
            logger.warning(e.toString());
            logger.throwing(getClass().getName(), "execute", e);

            return (Command.ERROR_CODE);
        }


        GanymedeMBean mbean = (GanymedeMBean) JmxClient.getMBeanProxy(
                mbeanServer,
                new ObjectNameFactory(
                    GanymedeMBean.class.getPackage().getName()),
                GanymedeMBean.class, null, false);

        /*
         * CREATE THE SET OF MEASURE_ID
         */

        logger.fine("start generating ids");

        MeasureId ids[];

        try {
            String kiList[] = null;
            String motypeList[] = null;
            String nodeList[] = null;
            String moList[] = null;

            /*
             * Get the number of given KI and given MO and create an array of
             * the mo and the ki. the management of null value forces us to
             * manage all in one loop and compute the values od the indices.
             */
            int length = 1;

            if (kiname != null) {
                kiList = kiname.split(",");
                length *= kiList.length;
            }

            if (motype != null) {
                motypeList = motype.split(",");
                length *= motypeList.length;
            }

            if (moname != null) {
                moList = moname.split(",");
                length *= moList.length;
            }

            if (node != null) {
                nodeList = node.split(",");
                length *= nodeList.length;
            }

            int oneMeasureId = 0;
            int oneKI = 0;
            int oneMO = 0;
            int oneNode = 0;
            int oneMOT = 0;

            ids = new MeasureId[length];

            for (oneMeasureId = 0; oneMeasureId < length; oneMeasureId++) {
                ids[oneMeasureId] = new MeasureId(((motype == null)
                            ? null : motypeList[oneMOT]),
                        ((moname == null) ? null : moList[oneMO]),
                        ((kiname == null) ? null : kiList[oneKI]),
                        ((node == null) ? null : nodeList[oneNode]));

                if (kiname != null) {
                    oneKI++;

                    if (oneKI == kiList.length)
                        oneKI = 0;
                }

                if (moname != null) {

                    if (oneKI == 0)
                        oneMO++;

                    if (oneMO == moList.length)
                        oneMO = 0;

                }

                if (motype != null) {

                    if (oneMO == 0)
                        oneMOT++;

                    if (oneMOT == motypeList.length)
                        oneMOT = 0;

                }

                if (node != null) {

                    if (oneNode == 0)
                        oneNode++;

                    /* FOLLOWING TEST SHOULD NEVER BE TRUE */
                    if (oneNode == nodeList.length)
                        oneNode = 0;

                }

            }
        } catch (Exception e) {
            logger.warning(e.toString());
            logger.throwing(getClass().getName(), "execute", e);

            return (Command.ERROR_CODE);
        }

        logger.fine("Measure Ids are created");

        MeasureSet ms[] = null;

        if ((startTime == null) && (stopTime == null)) {
            ms = mbean.fetchLastMeasures(ids, level);
        } else {
            ms = mbean.fetchMeasures(ids, level, startTime, stopTime);
        }

        try {

            StringBuffer sb = new StringBuffer();

            for (int i = 0; i < ms.length; i++) {
                int times[] = ms[i].getTimes();

                if (times != null) {

                    for (int j = 0; j < times.length; j++) {

                        /* clear string */
                        sb.setLength(0);

                        sb.append(times[j]).append(" ").append(ms[i].getId()
                            .getNodeName()).append(" ").append(ms[i].getId()
                            .getManagedObjectType()).append(" ").append(
                            ms[i].getId().getKeyIndicator()).append(" ").append(
                            ms[i].getId().getManagedObject()).append(" ")
                        .append(ms[i].getValues()[j]).append("\n");
                        out.write(sb.toString().getBytes());
                    }
                }
            }
        } catch (Exception e) {
            err.write(INTERNAL_ERROR.getBytes());
            logger.warning(e.toString());
            logger.throwing(getClass().getName(), "execute", e);

            return (Command.ERROR_CODE);
        }

        return (Command.SUCCESS_CODE);
    }
}
