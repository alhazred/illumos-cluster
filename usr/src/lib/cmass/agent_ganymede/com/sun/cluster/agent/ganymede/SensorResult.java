/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)SensorResult.java 1.7     08/05/20 SMI"
 *
 */
package com.sun.cluster.agent.ganymede;

import java.io.Serializable;

import java.math.BigInteger;

import java.sql.Timestamp;

import java.util.*;
// to be saved to file

public class SensorResult implements Serializable {

    private String monitoredEntity;
    private String keyIndicatorName;
    private String MOType;
    private String logicalNodeName;
    private Timestamp usageDate;
    private BigInteger value;
    private BigInteger upperBound;

    public SensorResult(String me, String mot, String lnn, String kin,
        Timestamp c, BigInteger v, BigInteger ub) {
        monitoredEntity = me;
        MOType = mot;
        logicalNodeName = lnn;
        keyIndicatorName = kin;
        usageDate = c;
        value = v;
        upperBound = ub;
    }

    public SensorResult(String me, String mot, String kin, Timestamp c,
        BigInteger v, BigInteger ub) {
        this(me, mot, null, kin, c, v, ub);
    }

    public String getMonitoredEntity() {
        return (monitoredEntity);
    }

    public String getMOType() {
        return (MOType);
    }

    public String getLogicalNodeName() {
        return (logicalNodeName);
    }

    public String getKeyIndicatorName() {
        return (keyIndicatorName);
    }

    public Timestamp getDate() {
        return (usageDate);
    }

    public BigInteger getValue() {
        return (value);
    }

    public BigInteger getUpperBound() {
        return (upperBound);
    }

    public String toString() {
        return (usageDate + " " + monitoredEntity + " " + MOType + " " +
                keyIndicatorName + " " + logicalNodeName + " " + value + " " +
                upperBound);
    }

}
