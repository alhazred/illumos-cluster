/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)GanInstrum.java 1.10     08/05/20 SMI"
 *
 */

package com.sun.cluster.agent.ganymede;

// JDK
import java.util.Enumeration;
import java.util.TimerTask;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * This class is triggering instrumentations from the sensors. It then sends it
 * to the processing chain.
 */
public class GanInstrum extends TimerTask {

    private static final String logTag = "GanInstrum";
    private static Logger logger = Logger.getLogger(GanymedeModule.DOMAIN +
            "." + logTag);

    private int nextInsertSeqNum;


    /**
     * Warning - This constructor needs database up and running
     */
    public GanInstrum() {
        int seqNumInBacklog;
        int seqNumInDataBase;
        Integer value = (Integer) PermanentStorage.getObject("HourlySeqNum");
        seqNumInBacklog = (value == null) ? 0 : value.intValue();

        logger.finer("HourlySeqNum from BackLog is " + seqNumInBacklog);

        seqNumInDataBase = DataBase.getSeqNum(MeasureLevel.HOURLY);
        logger.finer("HourlySeqNum from DataBase is " + seqNumInDataBase);

        // 10 increment is here to create a leap with old data
        nextInsertSeqNum = 10 +
            ((seqNumInDataBase > seqNumInBacklog) ? seqNumInDataBase
                                                  : seqNumInBacklog);

        logger.finer("HourlySeqNum from future insert is " + nextInsertSeqNum);
    }

    /**
     * Sampling method.
     */
    public void run() {

        Vector results = new Vector();

        boolean ok;
        int tmpSeqNum = nextInsertSeqNum + 1;

        // get instrum values from the instrum object
        try {
            ok = SensorBase.getValuesFromSensors(results);

            // Updates the thesholds
            ThresholdBase.verifyThresholds(results, tmpSeqNum);

        } catch (Exception e) {
            // fatal error, lock module

            // Note: locking module here causes the current thread to stop() the
            // Ganymede module, and cancel()'s this TimerTask. That's fine
            // because (a) locks are reentrant, and (b) we're about to return.

            logger.throwing(getClass().getName(), "run", e);
            Config.lockModule();

            return;
        }

        if (!ok) {
            logger.severe("Instrumenatation failed");
            Config.lockModule();

            return;
        }

        // insert the values in the DB
        logger.finest("size of vector received from sensors = " +
            results.size());

        if (results.size() != 0) {
            InsertHourlyRequest ir;

            nextInsertSeqNum = tmpSeqNum;
            ir = new InsertHourlyRequest(results, nextInsertSeqNum);
            AggregateWeeklyAgent.passRequest(ir);
            PermanentStorage.putObject("HourlySeqNum",
                new Integer(nextInsertSeqNum));

            // At this point we gave the AggregateWeeklyAgent thread a
            // reference to the results Vector. So it's important not to
            // fiddle with the results anymore.

        }
    }
}
