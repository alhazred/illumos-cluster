/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)Config.java 1.22     08/05/20 SMI"
 *
 */

package com.sun.cluster.agent.ganymede;

// JDK
import java.io.*;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

// JMX
import javax.management.InstanceNotFoundException;
import javax.management.JMException;
import javax.management.MBeanServer;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;

// CACAO
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.agent.JmxClient;
import com.sun.cacao.element.AdministrativeStateEnum;

// CMASS 
import com.sun.cluster.agent.configaccess.CCRManagerMBean;
import com.sun.cluster.agent.event.ClEventDefs;
import com.sun.cluster.agent.event.SysEventNotification;
import com.sun.cluster.agent.event.SysEventNotifierMBean;
import com.sun.cluster.agent.node.Node;
import com.sun.cluster.agent.node.NodeMBean;
import com.sun.cluster.agent.node.NodeModule;
import com.sun.cluster.agent.rgm.ResourceGroupMBean;
import com.sun.cluster.agent.rgm.ResourceMBean;
import com.sun.cluster.agent.rgm.RgmModule;
import com.sun.cluster.agent.rgm.property.ResourceProperty;
import com.sun.cluster.agent.rgm.property.ResourcePropertyBoolean;
import com.sun.cluster.agent.rgm.property.ResourcePropertyInteger;
import com.sun.cluster.agent.rgm.property.ResourcePropertyString;
import com.sun.cluster.common.ClusterPaths;


public class Config implements NotificationListener {

    // constants
    private static Config _instance = null;

    // logging utility
    private static final String logTag = "Config";
    private static Logger logger = Logger.getLogger(GanymedeModule.DOMAIN +
            "." + logTag);

    private static final String SCTELEMETRY_RT = "SUNW.sctelemetry";
    private static final String HADERBY_RT = "SUNW.derby";

    private static final String URL_FORMAT =
        "jdbc:derby://%HOST%:%PORT%/APP;user=APP";

    private static final String EXACCT_CLEANUP_FILE =
        "/var/cluster/run/SCTelemetry.exacct_cleanup_activated";

    private static final String PERMANENT_STORAGE_FILE_NAME =
        "/var/tmp/telemetry_backlog";

    private static final String CCR_SENSOR_TABLE_NAME = "scslmki";
    private static final String CCR_THRESHOLD_TABLE_NAME = "scslmthresh";

    private static final String DEFAULT_SENSOR_DIRECTORY = ClusterPaths.lib +
        "cmass/sensors/";

    private static final String CCR_NULL_VALUE = "<NULL>";
    public static final String MOTYPE_RG = "mot.sunw.cluster.resourcegroup";
    public static final String MOTYPE_NODE = "mot.sunw.node";

    // CACAO objects
    private GanymedeModule ganymedeModule;
    private MBeanServer mBeanServer;

    // RG and R names
    private String hadbRGName;
    private String hadbRName;
    private String telemetryRGName;
    private String telemetryRName;

    // Telemetry RG properties
    private String telemetryProject;
    private long samplingInterval;
    private String sensorDirectory;
    private boolean exacctCleanup = false;

    // Database RG properties
    private long databasePort;
    private long databaseStartTimeout;
    private long databaseStopTimeout;
    private long databaseProbeTimeout;
    private long databaseProbeInterval;

    /*
     * Class methods.
     */

    /**
     * Constructor of the Config. This constructor is private as we do not want
     * multiple instances. This is a singleton.
     */
    private Config() {

        sensorDirectory = DEFAULT_SENSOR_DIRECTORY;

        databasePort = -1; // invalid port number

        hadbRGName = null;
        hadbRName = null;
        telemetryRGName = null;
        telemetryRName = null;
    }

    /**
     * Method called at Ganymede module startup (unlock)
     *
     * @param  gm  ganymede module calling this method
     */
    public static void _initialize(GanymedeModule gm) throws Exception {

        logger.entering(logTag, "_initialize");

        _instance = new Config();
        _instance.initializeInternal(gm);

        logger.exiting(logTag, "_initialize");
    }

    /**
     * Method called at Ganymede module stop (lock)
     */
    public static void _finalize() throws Exception {
        logger.entering(logTag, "_finalize");

        // check if already finalized, this can happen if the
        // Ganymede module is locked as a result of failure
        // at unlock time
        if (_instance == null)
            return;

        _instance.finalizeInternal();
        _instance = null; // otherwise wouldn't be GC'ed

        logger.exiting(logTag, "_finalize");
    }

    public static MBeanServer getMBeanServer() {
        return (_instance.getMBeanServerInternal());
    }

    public static boolean isModuleLocked() {
        return (_instance.isModuleLockedInternal());
    }

    public static void lockModule() {
        _instance.lockModuleInternal();
    }

    public static String getLocalNode() {
        return (_instance.getLocalNodeInternal());
    }

    public static long getSamplingInterval() {
        return (_instance.getSamplingIntervalInternal());
    }

    public static void setSamplingInterval(long v) {
        _instance.setSamplingIntervalInternal(v);
    }

    public static boolean getExacctCleanup() {
        return (_instance.getExacctCleanupInternal());
    }

    public static String getPermanentStorageFileName() {
        return (PERMANENT_STORAGE_FILE_NAME);
    }

    public static void setExacctCleanup(boolean b) throws Exception {
        _instance.setExacctCleanupInternal(b);
    }

    public static String getSensorDirectory() {
        return (_instance.getSensorDirectoryInternal());
    }

    public static String getURLToDatabase() throws Exception {
        return (_instance.getURLToDatabaseInternal());
    }

    public static String getTelemetryProject() {
        return (_instance.getTelemetryProjectInternal());
    }

    public static long getDatabaseStartTimeout() {
        return (_instance.getDatabaseStartTimeoutInternal());
    }

    public static long getDatabaseStopTimeout() {
        return (_instance.getDatabaseStopTimeoutInternal());
    }

    public static long getDatabaseProbeTimeout() {
        return (_instance.getDatabaseProbeTimeoutInternal());
    }

    public static long getDatabaseProbeInterval() {
        return (_instance.getDatabaseProbeIntervalInternal());
    }

    /*
     * Object methods.
     */

    private void initializeInternal(GanymedeModule gm) throws Exception {

        logger.entering(logTag, "initializeInternal");

        ganymedeModule = gm;
        mBeanServer = gm.getMBeanServer();

        /*
         * Retrieve DB R and RG names, and telemetry R and RG names.
         */
        logger.fine("calling retrieveGanymedeRGInfo");
        retrieveGanymedeRGInfo();

        /*
         * Read sensor config.
         */
        logger.fine("calling loadSensorConfig");
        loadSensorConfig();

        /*
         * Read threshold config.
         */
        logger.fine("calling loadThresholdConfig");
        loadThresholdConfig();

        /*
         * Read properties of the Telemetry RG.
         */
        logger.fine("calling loadTelemetryConfig");
        loadTelemetryConfig(false);

        logger.fine("calling loadHADatabaseConfig");
        loadHADatabaseConfig(false);

        /*
         * Register config change handler.
         */
        logger.fine("retrieving ObjectNameFactory");

        ObjectNameFactory onf = new ObjectNameFactory(
                SysEventNotifierMBean.class.getPackage().getName());

        logger.fine("retrieving sysEventNotifierMBean");

        ObjectName sysEventNotifier = onf.getObjectName(
                SysEventNotifierMBean.class, null);

        // generates InstanceNotFoundException
        logger.fine("adding NotificationListener");
        mBeanServer.addNotificationListener(sysEventNotifier, this, null, null);

        logger.exiting(logTag, "initializeInternal");

    }

    private void finalizeInternal() throws Exception {

        /*
         * Unregister config change handler.
         */
        ObjectNameFactory onf = new ObjectNameFactory(
                SysEventNotifierMBean.class.getPackage().getName());

        ObjectName sysEventNotifier = onf.getObjectName(
                SysEventNotifierMBean.class, null);

        mBeanServer.removeNotificationListener(sysEventNotifier, this, null,
            null);
    }

    /**
     * Retrieve the names of the Telemetry Resource Group, Resource, and
     * project, and those of the HA Database Resource Group and Resource. Store
     * them in the attributes telemetryRGName, telemetryRName, telemetryProject,
     * hadbRGName, and hadbRName.
     */
    private void retrieveGanymedeRGInfo() throws Exception {
        logger.entering(logTag, "retrieveGanymedeRGInfo");

        try {
            logger.fine("retrieve ObjectNameFactory");

            ObjectNameFactory onf = new ObjectNameFactory(RgmModule.class);

            /*
             * Step1: look for the SCTELEMETRY_RT Resource, and its container
             * Resource Group. Store the Resource name, the Resource Group name,
             * and the project name.
             */

            logger.fine("retrieve ObjectName");

            ObjectName objName = onf.getObjectNamePattern(
                    ResourceGroupMBean.class);

            logger.fine("query Names");

            Set s = mBeanServer.queryNames(objName, null);

            logger.fine("create Iterator on names");

            Iterator iter = s.iterator();

            Hashtable hTab = new Hashtable();
            ObjectName telemetryRGObj = null;

            // iterate over RGs
            while (iter.hasNext()) {
                ObjectName rgObj = (ObjectName) iter.next();

                String rNames[] = (String[]) mBeanServer.getAttribute(rgObj,
                        "ResourceNames");

                for (int i = 0; i < rNames.length; i++) {
                    hTab.clear();
                    hTab.put("type", "resource");
                    hTab.put("instance", "\"" + rNames[i] + "\"");

                    ObjectName rObj = new ObjectName(
                            "com.sun.cluster.agent.rgm", hTab);
                    String rType = (String) mBeanServer.getAttribute(rObj,
                            "Type");

                    if (rType.equals(SCTELEMETRY_RT) ||
                            rType.equals(SCTELEMETRY_RT + ":1")) {
                        String name = (String) mBeanServer.getAttribute(rgObj,
                                "Name");

                        // got it!

                        telemetryRGObj = rgObj;
                        telemetryRGName = name;
                        telemetryRName = rNames[i];

                        // now get the associated Solaris project

                        String slmType = (String) mBeanServer.getAttribute(
                                rgObj, "SLMType");

                        if (slmType.equals("automated")) {

                            /*
                             * The rule to generate the projectname from the
                             * RG name is as follow :
                             * 1- add a prefix "SCSLM_" to the RG_name
                             * 2- replace all instances of "-" by "_"
                             *    (because SRM projects can't contain "-" in
                             *    their names on S10)
                             */
                            telemetryProject = "SCSLM_" +
                                telemetryRGName.replace('-', '_');

                            break; // done
                        }

                        telemetryProject = (String) mBeanServer.getAttribute(
                                rgObj, "ProjectName");

                        if (telemetryProject.length() != 0)
                            break; // done

                        telemetryProject = "default";

                        break;
                    }
                }
            }


            /*
             * Step2: get the list of RGs telemetry RG depends on. Store that
             * list in the variable dependencies.
             */
            String dependencies[] = (String[]) mBeanServer.getAttribute(
                    telemetryRGObj, "Dependencies");

            /*
             * Step3: among the RG dependencies, look for the RG that contains a
             * Resource of type HADERBY_RT. Store the Resource and the Resource
             * Group names.
             */

            for (int i = 0; i < dependencies.length; i++) {
                ObjectName rgObj = null;

                hTab.clear();
                hTab.put("type", "resourcegroup");
                hTab.put("instance", "\"" + dependencies[i] + "\"");

                rgObj = new ObjectName("com.sun.cluster.agent.rgm", hTab);

                String rNames[] = (String[]) mBeanServer.getAttribute(rgObj,
                        "ResourceNames");

                for (int j = 0; j < rNames.length; j++) {
                    hTab.clear();
                    hTab.put("type", "resource");
                    hTab.put("instance", "\"" + rNames[j] + "\"");

                    ObjectName rObj = new ObjectName(
                            "com.sun.cluster.agent.rgm", hTab);
                    String rType = (String) mBeanServer.getAttribute(rObj,
                            "Type");

                    if (rType.equals(HADERBY_RT) ||
                            rType.equals(HADERBY_RT + ":1")) {
                        String name = (String) mBeanServer.getAttribute(rgObj,
                                "Name");

                        hadbRGName = name;
                        hadbRName = rNames[j];

                        break;
                    }
                }
            }
        } catch (Exception e) {
            logger.throwing(getClass().getName(), "retrieveGanymedeRGInfo", e);
            throw e;
        }

        logger.fine("Database Resource Group: " + hadbRGName);
        logger.fine("Database Resource: " + hadbRName);
        logger.fine("Telemetry Resource Group: " + telemetryRGName);
        logger.fine("Telemetry Resource: " + telemetryRName);

        logger.exiting(logTag, "retrieveGanymedeRGInfo");
    }

    /**
     * Return the private hostname of the cluster node currently running the DB
     * server. Called from within the Database class when (re)establishing a
     * connection to the DB server.
     */
    private String getDatabaseHostName() throws Exception {
        String privateHostname = null;

        Hashtable hTab = new Hashtable();
        hTab.put("type", "resourcegroup");
        hTab.put("instance", "\"" + hadbRGName + "\"");

        ObjectName obj = new ObjectName("com.sun.cluster.agent.rgm", hTab);

        logger.fine("retrieving Current primaries for " + hadbRGName + " RG (" +
            obj.getCanonicalName() + ")");

        String hosts[] = null;
        hosts = (String[]) mBeanServer.getAttribute(obj, "CurrentPrimaries");

        switch (hosts.length) {

        case 1:
            break;

        default:

            GanymedeNoPrimaryException ge = new GanymedeNoPrimaryException(
                    "Database RG has " + hosts.length + " CurrentPrimaries");
            logger.throwing(getClass().getName(), "getDatabaseHostName", ge);
            throw ge;
        }

        String host = hosts[0];

        logger.fine(hadbRGName + " primary node name is: " + host);

        ObjectNameFactory onf = new ObjectNameFactory(NodeModule.class);
        obj = onf.getObjectName(NodeMBean.class, host);

        privateHostname = (String) mBeanServer.getAttribute(obj,
                "PrivateHostname");

        logger.fine(hadbRGName + " primary priv node name is" +
            privateHostname);

        return privateHostname;
    }

    private MBeanServer getMBeanServerInternal() {
        return mBeanServer;
    }

    private boolean isModuleLockedInternal() {
        return (ganymedeModule.getAdministrativeState() ==
                AdministrativeStateEnum.LOCKED);
    }

    private void lockModuleInternal() {
        ganymedeModule.lockModule();
    }

    private String getLocalNodeInternal() {
        return Node.getNodeName();
    }

    private long getSamplingIntervalInternal() {
        return samplingInterval;
    }

    private void setSamplingIntervalInternal(long v) {
        samplingInterval = v;
    }

    private boolean getExacctCleanupInternal() {
        return exacctCleanup;
    }

    private void setExacctCleanupInternal(boolean b) throws Exception {

        if (exacctCleanup == b)
            return;

        File f = new File(EXACCT_CLEANUP_FILE);

        if (b) {
            f.createNewFile();
        } else {
            f.delete();
        }

        exacctCleanup = b;
    }

    private String getSensorDirectoryInternal() {
        return sensorDirectory;
    }

    private String getURLToDatabaseInternal() throws Exception {
        String host = getDatabaseHostName(); // can throw exception
        String url = URL_FORMAT.replaceAll("%HOST%", host).replaceAll("%PORT%",
                String.valueOf(databasePort));

        return url;
    }

    private String getTelemetryProjectInternal() {
        return telemetryProject;
    }

    private long getDatabaseStartTimeoutInternal() {
        return databaseStartTimeout;
    }

    private long getDatabaseStopTimeoutInternal() {
        return databaseStopTimeout;
    }

    private long getDatabaseProbeTimeoutInternal() {
        return databaseProbeTimeout;
    }

    private long getDatabaseProbeIntervalInternal() {
        return databaseProbeInterval;
    }

    /**
     * Config change notification handler.
     */
    public void handleNotification(Notification notification, Object handback) {

        /*
         * All exceptions that may occur in handleNotification() are swallowed
         * as they cannot be propagated to the notification publisher. A warning
         * message is logged though.
         */

        try {

            if (!(notification instanceof SysEventNotification))
                return;

            SysEventNotification clEvent = (SysEventNotification) notification;

            if (clEvent.getSubclass().equals(
                        ClEventDefs.ESC_CLUSTER_SLM_CONFIG_CHANGE)) {

                logger.fine("received cluster event " + notification);

                Map clEventAttrs = clEvent.getAttrs();
                String tableName = (String) clEventAttrs.get(
                        ClEventDefs.CL_CCR_TABLE_NAME);

                if (tableName != null) {

                    if (tableName.equals(CCR_SENSOR_TABLE_NAME)) {
                        loadSensorConfig();
                    }

                    if (tableName.equals(CCR_THRESHOLD_TABLE_NAME)) {
                        loadThresholdConfig();
                    }
                }
            } else if (clEvent.getSubclass().equals(
                        ClEventDefs.ESC_CLUSTER_R_CONFIG_CHANGE)) {

                logger.fine("received cluster event " + notification);

                Map clEventAttrs = clEvent.getAttrs();
                String rgName = (String) clEventAttrs.get(
                        ClEventDefs.CL_RG_NAME);
                String rName = (String) clEventAttrs.get(ClEventDefs.CL_R_NAME);

                if (rgName.equals(telemetryRGName) &&
                        rName.equals(telemetryRName)) {

                    loadTelemetryConfig(true);

                } else if (rgName.equals(hadbRGName) &&
                        rName.equals(hadbRName)) {

                    loadHADatabaseConfig(true);
                }
            }
        } catch (Exception e) {

            // log and swallow all exceptions
            logger.severe(e.getMessage());
        }
    }

    /**
     * Load the sensor configuration. Can be called from the following execution
     * contexts: - Ganymede module start, trough _initialize() - Cluster event
     * notification handler, through handleNotification()
     */
    private void loadSensorConfig() throws Exception {

        logger.entering(getClass().getName(), "loadSensorConfig");

        Properties props = new Properties();

        // read CCR table
        ObjectNameFactory onf = new ObjectNameFactory(CCRManagerMBean.class
                .getPackage().getName());

        CCRManagerMBean mbean = (CCRManagerMBean) JmxClient.getMBeanProxy(
                mBeanServer, onf, CCRManagerMBean.class, null, false);

        Map tableContent = mbean.readTable(CCR_SENSOR_TABLE_NAME);

        if (tableContent == null) {

            /*
             * We do not know the sensor config, most probably the
             * CCR table does not exist. No instrumentation can be started.
             */
            logger.severe("loadSensorConfig : " + CCR_SENSOR_TABLE_NAME +
                " is not accessible");

            throw (new Exception(
                    CCR_SENSOR_TABLE_NAME +
                    " is not accessible. Unable to read " +
                    "the sensor configuration"));
        } else {
            Iterator ite = tableContent.entrySet().iterator();

            while (ite.hasNext()) {
                Map.Entry entry = (Map.Entry) ite.next();
                props.setProperty((String) entry.getKey(),
                    (String) entry.getValue());
            }

            SensorBase.refresh(props);
        }

        logger.exiting(getClass().getName(), "loadSensorConfig");
    }

    /**
     * Load the Threshold configuration. Can be called from the following
     * execution contexts: - Ganymede module start, trough _initialize() -
     * Cluster event notification handler, through handleNotification()
     */
    private void loadThresholdConfig() throws Exception {

        logger.entering(getClass().getName(), "loadThresholdConfig");

        // read CCR table
        Hashtable localTable = new Hashtable();

        ObjectNameFactory onf = new ObjectNameFactory(CCRManagerMBean.class
                .getPackage().getName());

        CCRManagerMBean mbean = (CCRManagerMBean) JmxClient.getMBeanProxy(
                mBeanServer, onf, CCRManagerMBean.class, null, false);

        Map tableContent = mbean.readTable(CCR_THRESHOLD_TABLE_NAME);

        if (tableContent == null) {

            // if the table is empty, then remove all the thresholds
            // by adding an empty vector
            ThresholdBase.updateThresholds(localTable);

            return;
        }

        Iterator ite = tableContent.entrySet().iterator();

        while (ite.hasNext()) {

            int field = 1;
            Integer ccrId = null;
            Map.Entry entry = (Map.Entry) ite.next();
            String entryValue = null;
            String valueType = null;

            // Process Value to transform "<NULL>" to a real empty string
            logger.fine("parsing CCR key " + (String) entry.getKey());

            if (entry.getValue() != null) {
                entryValue = ((String) entry.getValue()).trim();

                if ((entryValue.equals(CCR_NULL_VALUE)) ||
                        (entryValue.equals("")))
                    entryValue = null;
            }

            if (entryValue != null)
                logger.fine("parsing CCR value [" + entryValue + "]");
            else
                logger.fine("parsing CCR value is null");

            StringTokenizer st = new StringTokenizer((String) entry.getKey(),
                    ".", false);

            while (st.hasMoreTokens()) {
                String current = st.nextToken();

                switch (field) {

                case 1:

                    if (!current.equals("threshold")) {
                        GanymedeInternalException ge =
                            new GanymedeInternalException(
                                "CCR content isn't valid");
                        logger.throwing(getClass().getName(),
                            "loadThresholdConfig", ge);
                        throw ge;
                    }

                    break;

                case 2:
                    ccrId = Integer.valueOf(current);

                    break;

                case 3:
                    valueType = current;

                    break;

                default:

                    GanymedeInternalException ge =
                        new GanymedeInternalException(
                            "CCR content isn't valid");
                    logger.throwing(getClass().getName(), "loadThresholdConfig",
                        ge);
                    throw ge;
                }

                field++;
            }

            Threshold t = (Threshold) localTable.get(ccrId);

            if (t == null) {
                t = new Threshold();
                localTable.put(ccrId, t);
            }


            if (valueType.startsWith("rising") ||
                    valueType.startsWith("falling")) {
                StringTokenizer st2 = new StringTokenizer(valueType, "_",
                        false);

                if (!st2.hasMoreTokens()) {
                    GanymedeInternalException ge =
                        new GanymedeInternalException(
                            "CCR content isn't valid");
                    logger.throwing(getClass().getName(), "loadThresholdConfig",
                        ge);
                    throw ge;
                }

                ThresholdDirection d = ThresholdDirection.getEnumValue(st2
                        .nextToken());

                if (!st2.hasMoreTokens()) {
                    GanymedeInternalException ge =
                        new GanymedeInternalException(
                            "CCR content isn't valid");
                    logger.throwing(getClass().getName(), "loadThresholdConfig",
                        ge);
                    throw ge;
                }

                ThresholdSeverity s = ThresholdSeverity.getEnumValue(st2
                        .nextToken());

                if (!st2.hasMoreTokens()) {
                    GanymedeInternalException ge =
                        new GanymedeInternalException(
                            "CCR content isn't valid");
                    logger.throwing(getClass().getName(), "loadThresholdConfig",
                        ge);
                    throw ge;
                }


                if (entryValue != null) {
                    ThresholdFields f = ThresholdFields.getEnumValue(st2
                            .nextToken());
                    Double doubleValue = null;

                    doubleValue = new Double(entryValue);
                    t.setThresholdProperty(d, s, f, doubleValue);
                }

            } else if (valueType.equals("object-type")) {
                t.setMOType(entryValue);

            } else if (valueType.equals("object-instance")) {
                t.setMOName(entryValue);

            } else if (valueType.equals("telemetry-attribute")) {
                t.setKIName(entryValue);

            } else if (valueType.equals("node")) {
                t.setNodeName(entryValue);
            }
        }

        logger.finer("Number of Thresholds : " + localTable.size());

        // Thresholds associated with mot.sunw.node objects do not have an
        // associated node. Let's associate one artificially.
        for (Enumeration e = localTable.elements(); e.hasMoreElements();
                /* */) {

            Threshold t = (Threshold) e.nextElement();

            if (t.getMOType().equals(MOTYPE_NODE)) {
                t.setNodeName(t.getMOName());
            }
        }

        ThresholdBase.updateThresholds(localTable);

        logger.exiting(getClass().getName(), "loadThresholdConfig");
    }

    /**
     * Load the HA Database configuration. Can be called from the following
     * execution contexts: - Ganymede module start, trough _initialize() -
     * Cluster event notification handler, through handleNotification()
     */

    private void loadHADatabaseConfig(boolean apply) throws Exception {

        logger.entering(getClass().getName(), "loadHADatabaseConfig");

        boolean foundStartTimeout = false;
        boolean foundStopTimeout = false;
        boolean foundProbeInterval = false;
        boolean foundProbeTimeout = false;
        boolean foundDbPort = false;
        Iterator i = null;

        ObjectNameFactory onf = new ObjectNameFactory(RgmModule.class);

        // Get the MBean for Database Resource Group module
        ResourceMBean mbean = (ResourceMBean) JmxClient.getMBeanProxy(
                mBeanServer, onf, ResourceMBean.class, hadbRName, false);

        if (mbean == null) {
            String msg = "Unable to access " + hadbRName + " MBean";
            logger.severe(msg);
            throw (new Exception(msg));
        }

        List sysProperties = mbean.getSystemProperties();

        if (sysProperties == null) {
            String msg = "Unable to retrieve " + hadbRName +
                " system properties";
            logger.severe(msg);
            throw (new Exception(msg));
        }

        logger.fine("Number of system properties : " + sysProperties.size());

        i = sysProperties.iterator();

        while (i.hasNext()) {
            ResourceProperty property = (ResourceProperty) i.next();
            String propertyName = property.getName();

            logger.fine("Analysing system property : " + propertyName);

            if (propertyName.equalsIgnoreCase("START_TIMEOUT")) {

                // Start timeout
                logger.fine("retrieve START_TIMEOUT");

                if (property instanceof ResourcePropertyInteger) {
                    databaseStartTimeout = ((ResourcePropertyInteger) property)
                        .getValue().longValue();
                    foundStartTimeout = true;
                }
            } else if (propertyName.equalsIgnoreCase("STOP_TIMEOUT")) {

                // Stop timeout
                logger.fine("retrieve STOP_TIMEOUT");

                if (property instanceof ResourcePropertyInteger) {
                    databaseStopTimeout = ((ResourcePropertyInteger) property)
                        .getValue().longValue();
                    foundStopTimeout = true;
                }
            } else if (propertyName.equalsIgnoreCase(
                        "THOROUGH_PROBE_INTERVAL")) {

                // Manage THOROUGH_PROBE_INTERVAL
                logger.fine("retrieve THOROUGH_PROBE_INTERVAL");

                if (property instanceof ResourcePropertyInteger) {
                    databaseProbeInterval = ((ResourcePropertyInteger) property)
                        .getValue().longValue();
                    foundProbeInterval = true;
                }
            }
        }


        List extProperties = mbean.getExtProperties();

        if (extProperties == null) {
            String msg = "Unable to retrieve " + hadbRName +
                " extended properties";
            logger.severe(msg);
            throw (new Exception(msg));
        }

        logger.fine("Number of extended properties : " + extProperties.size());

        i = extProperties.iterator();

        while (i.hasNext()) {
            ResourceProperty property = (ResourceProperty) i.next();
            String propertyName = property.getName();

            logger.fine("Analysing extended property : " + propertyName);

            if (propertyName.equalsIgnoreCase("DB_PORT")) {

                // Database port
                logger.fine("retrieve DB_PORT");

                if (property instanceof ResourcePropertyInteger) {
                    databasePort = ((ResourcePropertyInteger) property)
                        .getValue().longValue();
                    foundDbPort = true;
                }

            }

            if (propertyName.equalsIgnoreCase("PROBE_TIMEOUT")) {

                // Probe timeout
                logger.fine("retrieve PROBE_TIMEOUT");

                if (property instanceof ResourcePropertyInteger) {
                    databaseProbeTimeout = ((ResourcePropertyInteger) property)
                        .getValue().longValue();
                    foundProbeTimeout = true;
                }
            }
        }


        if (foundProbeInterval == false) {
            String msg = "Unable to retrieve THOROUGH_PROBE_INTERVAL value";
            logger.severe(msg);
            throw (new Exception(msg));
        }

        if (foundProbeTimeout == false) {
            String msg = "Unable to retrieve PROBE_TIMEOUT value";
            logger.severe(msg);
            throw (new Exception(msg));
        }

        if (foundStartTimeout == false) {
            String msg = "Unable to retrieve START_TIMEOUT value";
            logger.severe(msg);
            throw (new Exception(msg));
        }

        if (foundStopTimeout == false) {
            String msg = "Unable to retrieve STOP_TIMEOUT value";
            logger.severe(msg);
            throw (new Exception(msg));
        }

        if (foundDbPort == false) {
            String msg = "Unable to retrieve DB_PORT value";
            logger.severe(msg);
            throw (new Exception(msg));
        }

        logger.fine("START_TIMEOUT = " + databaseStartTimeout);
        logger.fine("STOP_TIMEOUT = " + databaseStopTimeout);
        logger.fine("PROBE_TIMEOUT = " + databaseProbeTimeout);
        logger.fine("THOROUGH_PROBE_INTERVAL = " + databaseProbeInterval);
        logger.fine("DB_PORT = " + databasePort);

        logger.exiting(getClass().getName(), "loadHADatabaseConfig");
    }


    /**
     * Load the Telemetry configuration. Can be called from two different
     * execution contexts: - module start(), in _initialize() - notification
     * handler, in handleNotification()
     */
    private void loadTelemetryConfig(boolean apply) throws Exception {

        logger.entering(getClass().getName(), "loadTelemetryConfig");

        boolean foundExacctCleanup = false;
        boolean foundSamplingInterval = false;

        ObjectNameFactory onf = new ObjectNameFactory(RgmModule.class);

        ObjectName objName = onf.getObjectName(ResourceMBean.class,
                telemetryRName);

        if (objName == null) {
            String msg = "Unable to access " + telemetryRName + " MBean";
            logger.severe(msg);
            throw (new Exception(msg));
        }

        List extProperties = null;
        extProperties = (List) mBeanServer.getAttribute(objName,
                "ExtProperties");

        if (extProperties == null) {
            String msg = "Unable to retrieve " + telemetryRName +
                " extended properties";
            logger.severe(msg);
            throw (new Exception(msg));
        }

        Iterator i = extProperties.iterator();

        while (i.hasNext()) {
            ResourceProperty property = (ResourceProperty) i.next();
            String propertyName = property.getName();

            if (propertyName.equalsIgnoreCase("EXTENDED_ACCOUNTING_CLEANUP")) {

                // EXTENDED_ACCOUNTING_CLEANUP property
                logger.fine("updating EXTENDED_ACCOUNTING_CLEANUP");

                Boolean cleanup = Boolean.FALSE;

                if (property instanceof ResourcePropertyBoolean) {
                    cleanup = ((ResourcePropertyBoolean) property).getValue();

                    setExacctCleanupInternal(cleanup.booleanValue());
                    foundExacctCleanup = true;
                }
            } else if (propertyName.equalsIgnoreCase("SAMPLING_INTERVAL")) {

                // SAMPLING INTERVAL property
                logger.fine("updating SAMPLING_INTERVAL");

                long interval = 0;
                long previousInterval = 0;

                if (property instanceof ResourcePropertyInteger) {
                    interval = ((ResourcePropertyInteger) property).getValue()
                        .longValue() * 1000;

                    previousInterval = getSamplingInterval();

                    if (interval != previousInterval) {
                        setSamplingIntervalInternal(interval);

                        if (apply == true) {
                            logger.config("[config] Sensor " +
                                "sampling interval = " + interval);
                            GanSched.updateSamplingInterval(interval);
                        }
                    }

                    foundSamplingInterval = true;
                }
            }
        }


        if (foundExacctCleanup == false) {
            String msg = "Unable to retrieve " +
                "EXTENDED_ACCOUNTING_CLEANUP value";
            logger.severe(msg);
            throw (new Exception(msg));
        }

        if (foundSamplingInterval == false) {
            String msg = "Unable to retrieve SAMPLING_INTERVAL value";
            logger.severe(msg);
            throw (new Exception(msg));
        }

        logger.fine("EXTENDED_ACCOUNTING_CLEANUP = " + exacctCleanup);
        logger.fine("SAMPLING_INTERVAL = " + samplingInterval);

        logger.exiting(getClass().getName(), "loadTelemetryConfig");
    }

}
