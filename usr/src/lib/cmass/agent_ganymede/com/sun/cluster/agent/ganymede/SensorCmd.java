/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)SensorCmd.java 1.11     08/05/20 SMI"
 *
 */
package com.sun.cluster.agent.ganymede;

// JDK
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.String;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Properties;
import java.util.Vector;
import java.util.logging.Logger;

// CACAO
import com.sun.cacao.invocation.UserProcess;

// CMASS
import com.sun.cluster.common.ClusterPaths;


/**
 * This class provides specific code for sensors of type library.
 */
public class SensorCmd extends Sensor {

    /* logging facility */
    private static final String logTag = "SensorCmd";
    private static Logger logger = Logger.getLogger(GanymedeModule.DOMAIN +
            "." + logTag);

    /* command tags */
    private final static String TAG_START = "__START__\n";
    private final static String TAG_STOP = "__STOP__\n";
    private final static String TAG_GETVALUES = "__GETVALUES__\n";
    private final static String TAG_END = "__END__\n";

    /* response tags */
    private final static String TAG_ERROR = "__ERROR__\n";

    private UserProcess process;

    public SensorCmd(String _name, String _file, int _idInConf,
        Properties props) {

        super(_name, _file, _idInConf, props);

        if (KIListIsEmpty() == false) {
            String argv0 = ClusterPaths.NEWTASK_CMD;
            String argv1 = "-p";
            String argv2 = Config.getTelemetryProject();
            String argv3 = file;

            /* start process */
            String argv[] = new String[] { argv0, argv1, argv2, argv3 };

            try {
                process = new UserProcess(argv, null);
                registerKeyIndicators();
            } catch (java.io.IOException e) {
                logger.warning(e.getMessage());
                logger.throwing(getClass().getName(), "SensorCmd.<init>", e);
            }
        }
    }

    /* helper function for reading one line */
    private static String readOneLine(InputStream i) {
        String line = "";

        try {
            int last_idx = -1;

            while (true) {
                int in = 0;
                byte b[] = new byte[1];

                in = i.read(b);

                if (in != 1) {
                    line = null;
                    throw (new IOException("Unable to read stdin from sensor"));
                }

                line += new String(b);
                last_idx++;

                if (line.charAt(last_idx) == '\n')
                    break;
            }
        } catch (IOException e) {
            logger.warning(e.toString());
        }

        return (line);
    }

    public synchronized boolean registerKI(String kiName) {
        boolean retval;

        try {
            OutputStream oStream = process.getOutputStream();
            InputStream iStream = process.getInputStream();

            /* write command tag */
            oStream.write(TAG_START.getBytes());

            /* write command args */
            String kiLine = kiName + "\n";
            oStream.write(kiLine.getBytes());
            oStream.flush();

            /* read response */
            String line = readOneLine(iStream);

            if (line.equals(TAG_ERROR)) {
                logger.warning(readOneLine(iStream));
                retval = false;
            } else if (line.charAt(0) != '\n') {
                logger.warning("registerKI: got unexpected response " +
                    "from sensor " + name + ": " + line);
                retval = false;
            } else {

                /* command succeeded */
                retval = true;
            }
        } catch (IOException e) {
            logger.warning(e.toString());
            logger.throwing(getClass().getName(),
                "registerKI for sensor " + name + " failed", e);
            retval = false;
        }

        return (retval);
    }

    public synchronized boolean unregisterKI(String kiName) {
        boolean retval;

        try {
            OutputStream oStream = process.getOutputStream();
            InputStream iStream = process.getInputStream();

            /* write command tag */
            oStream.write(TAG_STOP.getBytes());

            /* write command args */
            String kiLine = kiName + "\n";
            oStream.write(kiLine.getBytes());
            oStream.flush();

            /* read response */
            String line = readOneLine(iStream);

            if (line.equals(TAG_ERROR)) {
                logger.warning(readOneLine(iStream));
                retval = false;
            } else if (line.charAt(0) != '\n') {
                logger.warning("unregisterKI: got unexpected response " +
                    "from sensor " + name + ": " + line);
                retval = false;
            } else {

                /* command succeeded */
                retval = true;
            }
        } catch (IOException e) {
            logger.warning(e.toString());
            retval = false;
        }

        return (retval);
    }

    public synchronized boolean getValues(String objectNames[],
        Vector results) {
        boolean retval;

        try {
            Vector tmp = new Vector();

            OutputStream oStream = process.getOutputStream();
            InputStream iStream = process.getInputStream();

            /* write command tag */
            oStream.write(TAG_GETVALUES.getBytes());

            /* write command args */
            if (objectNames != null) {

                for (int i = 0; i < objectNames.length; i++) {
                    String moLine = objectNames[i] + "\n";
                    oStream.write(moLine.getBytes());
                    oStream.flush();
                }
            }

            /* write end of command tag */
            oStream.write(TAG_END.getBytes());
            oStream.flush();

            /* read response */
            int resCnt = 0;

            while (true) {
                String line = readOneLine(iStream);

                if (line.equals(TAG_ERROR)) {
                    logger.warning(readOneLine(iStream));
                    retval = (resCnt == 0) ? false : true;

                    break;
                }

                if (line.charAt(0) == '\n') {
                    retval = true;

                    break;
                }

                resCnt++;

                /* output is given in semi colom-separated values format */
                String fields[] = line.split(";");

                /* convert to SensorResult fields */
                Double sec_D = new Double(fields[0]);
                Double nsec_D = new Double(fields[1]);
                String ki = fields[2];
                String mo = fields[3];
                BigInteger v = new BigInteger(fields[4]);
                BigInteger u = new BigInteger(fields[5]);

                double msec_d = sec_D.doubleValue() * 1e3;
                msec_d += nsec_D.doubleValue() * 1e-6;

                Double msec_D = new Double(msec_d);
                long msec_l = msec_D.longValue();
                Timestamp ts = new Timestamp(msec_l);

                /* create a SensorResult object and add it to results */
                tmp.add(new SensorResult(mo, getMOType(ki), ki, ts, v, u));
            }

            results.addAll(tmp);

        } catch (IOException e) {
            logger.warning(e.toString());
            retval = false;
        }

        return (retval);
    }

    /**
     * destroy()'s caller must make sure that neither registerKI(), nor
     * unregisterKI(), nor getValues() can be called at the same time and after
     * destroy() is called.
     */
    public void destroy() {
        unregisterKeyIndicators();
        process.destroy();
    }
}
