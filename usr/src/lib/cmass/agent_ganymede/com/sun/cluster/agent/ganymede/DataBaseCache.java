/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)DataBaseCache.java 1.7     08/05/20 SMI"
 *
 */

package com.sun.cluster.agent.ganymede;

import java.util.Hashtable;
import java.util.logging.Logger;


/**
 * This class is implementing the cache to the DB CONSIDER IT AS A PRIVATE
 * MEMBER OF Database.java
 */
public class DataBaseCache {

    private static final String logTag = "DataBaseCache";
    private static Logger logger = Logger.getLogger(GanymedeModule.DOMAIN +
            "." + logTag);

    // Caches to optimize read-access to DB

    // The Managed Object hashtable's key is MO+MOType+NodeName
    private Hashtable KIId;
    private Hashtable MOTypeId;
    private Hashtable MOId;
    private Hashtable nodeId;
    private String cacheName;

    public int hourlySeqNum;
    public int weeklySeqNum;


    /**
     * Constructor
     */
    public DataBaseCache(String theName) {

        cacheName = theName;
        KIId = new Hashtable();
        MOId = new Hashtable();
        MOTypeId = new Hashtable();
        nodeId = new Hashtable();
    }

    /**
     * CACHE MANAGEMENT
     */

    public void cacheMOId(String name, String MOType, String nodeName, int id) {
        MOId.put(name + "##" + MOType + "##" + nodeName, new Integer(id));
        logger.fine("MO Caching (id=" + id + ", " + name + "##" + MOType +
            "##" + nodeName + ") in " + cacheName);
    }

    public int getMOId(String name, String MOType, String nodeName) {
        Integer id = (Integer) MOId.get(name + "##" + MOType + "##" + nodeName);

        if (id != null)
            return (id.intValue());

        return (-1);
    }

    public void cacheKIId(String name, int id) {
        KIId.put(name, new Integer(id));
        logger.finer("KI Caching " + name + " with Id " + id + " in " +
            cacheName);
    }

    public int getKIId(String name) {
        Integer id = (Integer) KIId.get(name);

        if (id != null)
            return (id.intValue());

        return (-1);
    }

    public void cacheMOTypeId(String name, int id) {
        MOTypeId.put(name, new Integer(id));
        logger.finer("MOType Caching " + name + " with Id " + id + " in " +
            cacheName);
    }

    public int getMOTypeId(String name) {
        Integer id = (Integer) MOTypeId.get(name);

        if (id != null)
            return (id.intValue());

        return (-1);
    }

    public void cacheNodeId(String name, int id) {
        nodeId.put(name, new Integer(id));
        logger.finer("caching node " + name + " with Id " + id + " in " +
            cacheName);
    }

    public int getNodeId(String name) {
        Integer id = (Integer) nodeId.get(name);

        if (id != null)
            return (id.intValue());

        return (-1);
    }

    public void integrate(DataBaseCache dbc) {
        KIId.putAll(dbc.KIId);
        MOTypeId.putAll(dbc.MOTypeId);
        MOId.putAll(dbc.MOId);
        nodeId.putAll(dbc.nodeId);
    }
}
