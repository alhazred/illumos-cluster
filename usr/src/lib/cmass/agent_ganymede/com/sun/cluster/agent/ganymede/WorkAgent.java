/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)WorkAgent.java 1.6     08/05/20 SMI"
 *
 */

package com.sun.cluster.agent.ganymede;

import com.sun.cluster.agent.ganymede.WorkQueue;

import java.lang.String;
import java.lang.Thread;

import java.util.ListIterator;
import java.util.logging.Logger;


/**
 * This class is managing the processing associated with the WorkQueue. It
 * extends the thread and manages the sleep/wake status.
 */
public class WorkAgent extends Thread {

    // logging facility
    private static final String logTag = "WorkAgent";
    private static Logger logger = Logger.getLogger(GanymedeModule.DOMAIN +
            "." + logTag);

    // queue holding requests
    private WorkQueue workRequestQueue;

    // thread must return if set to true
    private volatile boolean stopPending;

    // unique name for this object
    private String name;


    /**
     * Constructor of the Agent. The name must be unique as it is used to define
     * a name for the queue which must be unique to be correctly saved in the
     * persistent storage
     *
     * @param  theName  a unique name for this agent
     */
    protected WorkAgent(String theName) {
        stopPending = false;
        name = theName;
        workRequestQueue = new WorkQueue("Queue of " + theName);
    }

    /**
     * Request the thread to return.
     */
    protected void requestStop() {
        stopPending = true;

        synchronized (this) {
            notify();
        }
    }

    /**
     * Check if there's a stop pending.
     */
    protected boolean isStopPending() {
        return stopPending;
    }

    /**
     * Reinsert a not processed request in the queue, the reinsertion is done as
     * the request has never been popped
     *
     * @param  theRequest  the request to reinsert
     */
    protected void requeueRequest(WorkRequest theRequest) {
        workRequestQueue.requeueRequest(theRequest);
    }


    /**
     * get the first (oldest) pending request
     */
    protected WorkRequest popRequest() throws Exception {

        // assume there is no concurrency
        if (workRequestQueue.isEmpty())
            return null;
        else
            return workRequestQueue.popRequest();
    }

    /**
     * returns true if a request pending.
     */
    protected boolean isWorkPending() {
        return (!workRequestQueue.isEmpty());
    }

    /**
     * makes the WorkAgent waits for a WorkRequest
     */
    protected void waitForWork() throws Exception {

        while (workRequestQueue.isEmpty() && (stopPending == false)) {

            synchronized (this) {
                logger.finest(name + " is now sleeping");
                wait();
                logger.finest(name + " is now waken up");
            }
        }
    }

    /**
     * insert a request for next agent (called by predecessors)
     *
     * @param  theRequest  the request to reinsert
     */
    public void insertRequest(WorkRequest theRequest) {
        workRequestQueue.pushRequest(theRequest);

        synchronized (this) {
            logger.finest(name + " is notified ");
            notify();
        }
    }

}
