/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident    "@(#)ThresholdFields.java    1.5    08/05/20 SMI"
 */
package com.sun.cluster.agent.ganymede;

import javax.print.attribute.EnumSyntax;


public class ThresholdFields extends EnumSyntax {
    public static final ThresholdFields VALUELIMIT = new ThresholdFields(0);
    public static final ThresholdFields RESETVALUE = new ThresholdFields(1);


    private static final String stringTable[] = { "valuelimit", "resetvalue" };

    private static final ThresholdFields enumValueTable[] = {
            VALUELIMIT, RESETVALUE
        };

    private ThresholdFields(int i) {
        super(i);
    }

    protected String[] getStringTable() {
        return stringTable;
    }

    protected EnumSyntax[] getEnumValueTable() {
        return enumValueTable;
    }

    protected static final ThresholdFields getEnumValue(String input)
        throws NullPointerException, Exception {

        if (input == null) {
            throw (new NullPointerException());
        }

        for (int i = 0; i < stringTable.length; i++) {

            if (input.equals(stringTable[i])) {
                return (enumValueTable[i]);
            }
        }

        throw (new Exception("Unknown field : " + input));
    }

    protected static final String getEnumString(ThresholdFields input)
        throws NullPointerException, Exception {

        if (input == null) {
            throw (new NullPointerException());
        }

        for (int i = 0; i < stringTable.length; i++) {

            if (input == enumValueTable[i]) {
                return (stringTable[i]);
            }
        }

        throw (new Exception("Unknown field"));
    }
}
