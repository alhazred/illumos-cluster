/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
#pragma ident	"@(#)gan_sensor_interface.c	1.3	08/05/20 SMI"

/*
 * register, unregister and getvalues generic functions
 * implementation.
 */

#include <string.h>		/* strcmp, strncpy */
#include <stdlib.h>		/* malloc, */
#include <stdio.h>		/* fprintf, */
#include <string.h>		/* strerror, */
#include <strings.h>		/* strncpy */
#include <assert.h>		/* assert */
#include <errno.h>		/* errno */
#include <stddef.h>		/* offsetof */
#include <dlfcn.h>		/* dlopen, */
#include <link.h>

#include "sensors.h"		/* SENSOR_NAME_MAX, */

#include "gan_list.h"	/* gan_list_t, gan_list_node_t, */

#define	GETV	"get_values"
#define	START	"start"
#define	STOP	"stop"

typedef int (*get_values_func_t)(char **, int, sensor_result_t **, int *,
	char *);
typedef int (*start_func_t)(const char *ki, char error_string[]);
typedef int (*stop_func_t)(const char *ki, char error_string[]);

typedef struct ki_handle {
	gan_list_node_t node;
	char name[SENSOR_KI_TYPE_MAX];
} ki_handle_t;

typedef struct lib_handle {
	gan_list_node_t node;
	char name[SENSOR_NAME_MAX];
	void *handle;
	gan_list_t ki_list;
	sensor_result_t *res;
	int n_res;
	int (*get_values_func)(char **, int, sensor_result_t **, int *, char *);
	int (*start_func)(const char *, char *);
	int (*stop_func)(const char *, char *);
} lib_handle_t;

static int list_created;
static gan_list_t lib_list;

/*
 * gan_register(), gan_getvalues() and gan_unregister() return codes are:
 *  - 0 in case of success
 *  - 1 in case of an internal error
 *  - 2 in case of a sensor error
 */

int
gan_register(const char *lib_name, const char *ki_name, char *error_string)
{
	lib_handle_t *lib;
	ki_handle_t *ki;
	char *error;

	assert(lib_name != NULL && ki_name != NULL);

	/*
	 * Flush error from JVM
	 * "ld.so.1: java: fatal: JNI_OnLoad: can't find symbol"
	 */
	error = dlerror();

	/*
	 * Create library list if not already created.
	 */
	if (!list_created) {
		gan_list_create(&lib_list, sizeof (lib_handle_t),
			offsetof(lib_handle_t, node));
		list_created = 1;
	}

	gan_list_for_each(lib, &lib_list)
		if (strncmp(lib->name, lib_name, SENSOR_NAME_MAX) == 0)
			break;

	if (lib == NULL) {
		void *handle;

		/*
		 * There's no lib named lib_name in list, we
		 * need to that library.
		 */

		if ((handle = dlopen(lib_name, RTLD_LAZY)) == NULL) {
			if ((error = dlerror()) != NULL) {
				(void) snprintf(error_string, ERROR_STRING_MAX,
					"dlopen %s: %s\n", lib_name, error);
			} else {
				(void) snprintf(error_string, ERROR_STRING_MAX,
					"dlopen %s: failed for unknown"
					" reason\n", lib_name);
			}
			return (1);
		}

		lib = (lib_handle_t *)malloc(sizeof (lib_handle_t));
		if (lib == NULL) {
			(void) snprintf(error_string, ERROR_STRING_MAX,
				"malloc failed (%s)\n", strerror(errno));
			(void) dlclose(handle);
			return (1);
		}
		(void) memset(lib, 0, sizeof (lib_handle_t));

		lib->handle = handle;
		(void) strncpy(lib->name, lib_name, SENSOR_NAME_MAX);

		gan_list_create(&lib->ki_list, sizeof (ki_handle_t),
			offsetof(ki_handle_t, node));

		if ((lib->start_func = (start_func_t)dlsym(handle, START))
			== NULL) {

			if ((error = dlerror()) != NULL) {
				(void) snprintf(error_string, ERROR_STRING_MAX,
					"dlsym %s, %s: %s\n",
					lib_name, START, error);
			} else {
				(void) snprintf(error_string, ERROR_STRING_MAX,
					"dlsym %s, %s: failed for unknown"
					" reason\n", lib_name, START);
			}
			gan_list_destroy(&lib->ki_list);
			(void) dlclose(lib->handle);
			free(lib);
			return (1);
		}

		if ((lib->get_values_func = (get_values_func_t)dlsym(handle,
			GETV)) == NULL) {

			if ((error = dlerror()) != NULL) {
				(void) snprintf(error_string, ERROR_STRING_MAX,
					"dlsym %s, %s: %s\n",
					lib_name, GETV, error);
			} else {
				(void) snprintf(error_string, ERROR_STRING_MAX,
					"dlsym %s: %s: failed for unknown"
					" reason\n", lib_name, GETV);
			}
			gan_list_destroy(&lib->ki_list);
			(void) dlclose(lib->handle);
			free(lib);
			return (1);
		}

		if ((lib->stop_func = (stop_func_t)dlsym(handle, STOP)) ==
			NULL) {

			if ((error = dlerror()) != NULL) {
				(void) snprintf(error_string, ERROR_STRING_MAX,
					"dlsym %s, %s: %s\n",
					lib_name, STOP, error);
			} else {
				(void) snprintf(error_string, ERROR_STRING_MAX,
					"dlsym %s, %s: failed for unknown"
					" reason\n", lib_name, STOP);
			}
			gan_list_destroy(&lib->ki_list);
			(void) dlclose(lib->handle);
			free(lib);
			return (1);
		}

		/*
		 * Insert lib into list.
		 */
		gan_list_insert_tail(&lib_list, lib);
	}

	gan_list_for_each(ki, &lib->ki_list)
		if (strncmp(ki->name, ki_name, SENSOR_KI_TYPE_MAX) == 0)
			break;

	if (ki != NULL)
		/*
		 * KI is in list so already active in sensor. Calling
		 * the sensor's start would not hurt but let's not
		 * call it as an optimization.
		 */
		return (0);

	/*
	 * ki isn't active.
	 */

	ki = (ki_handle_t *)malloc(sizeof (ki_handle_t));
	if (ki == NULL) {
		(void) snprintf(error_string, ERROR_STRING_MAX,
			"malloc failed (%s)\n", strerror(errno));
		return (1);
	}
	(void) memset(ki, 0, sizeof (ki_handle_t));
	(void) strncpy(ki->name, ki_name, SENSOR_KI_TYPE_MAX);

	/*
	 * Call sensor's start.
	 */

	assert(lib->start_func != NULL);

	if (lib->start_func(ki->name, error_string) != 0) {
		free(ki);
		return (2); /* sensor error */
	}

	/*
	 * Insert KI into list.
	 */
	gan_list_insert_tail(&lib->ki_list, ki);

	return (0);
}

int
gan_getvalues(const char *lib_name, const char **mo, const int n_mo,
	sensor_result_t **res, int *n_res, char *error)
{
	int ret;
	lib_handle_t *lib;

	/* init result pointer */
	*res = NULL;
	*n_res = 0;

	gan_list_for_each(lib, &lib_list)
		if (strncmp(lib->name, lib_name, SENSOR_NAME_MAX) == 0)
			break;

	if (lib == NULL) {
		(void) snprintf(error, ERROR_STRING_MAX,
			"sensor %s not found\n", lib_name);
		return (1);
	}

	if (gan_list_head(&lib->ki_list) == NULL) {

		/*
		 * KI list is empty. The sensors should handle the case where
		 * their get_values func is called while there's no active KI,
		 * and they should return 1 in such a case. As an optimization,
		 * let's not call the sensor's get_values.
		 */

		(void) snprintf(error, ERROR_STRING_MAX,
			"sensor %s has no active KI\n", lib_name);
		return (1);
	}

	assert(lib->get_values_func != NULL);

	if ((ret = lib->get_values_func((char **)mo, n_mo, &lib->res,
		&lib->n_res, error)) != 0)
		return (2); /* sensor error */

	*res = lib->res;
	*n_res = lib->n_res;

	return (0);
}

int
gan_unregister(const char *lib_name, const char *ki_name, char *error_string)
{
	lib_handle_t *lib;
	ki_handle_t *ki;

	assert(lib_name != NULL && ki_name != NULL);

	gan_list_for_each(lib, &lib_list)
		if (strncmp(lib->name, lib_name, SENSOR_NAME_MAX) == 0)
			break;

	if (lib == NULL) {
		(void) snprintf(error_string, ERROR_STRING_MAX,
			"sensor %s not found\n", lib_name);
		return (1);
	}

	gan_list_for_each(ki, &lib->ki_list)
		if (strncmp(ki->name, ki_name, SENSOR_KI_TYPE_MAX) == 0)
			break;

	if (ki == NULL)
		/*
		 * KI isn't in list so already inactive in sensor. Calling
		 * the sensor's stop would not hurt but let's not  call it
		 * as an optimization.
		 */
		return (0);

	assert(lib->stop_func != NULL);

	if (lib->stop_func(ki->name, error_string) != 0)
		return (2); /* sensor error */

	gan_list_remove(&lib->ki_list, ki);
	free(ki);

	if (gan_list_head(&lib->ki_list) == NULL) {

		/*
		 * KI list is empty, unload sensor.
		 */

		/* destroy KI list */
		gan_list_destroy(&lib->ki_list);

		/* close sensor library */
		assert(lib->handle != NULL);
		if (dlclose(lib->handle) != 0) {
			char *error;

			if ((error = dlerror()) != NULL) {
				(void) snprintf(error_string, ERROR_STRING_MAX,
					"dlclose %s: %s\n",
					lib->name, error);
			} else {
				(void) snprintf(error_string, ERROR_STRING_MAX,
					"dlclose %s: failed for unknown"
					" reason\n", lib->name);
			}
			return (1);
		}

		gan_list_remove(&lib_list, lib);
		free(lib->res);
		free(lib);

		if (gan_list_head(&lib_list) == NULL) {
			/*
			 * lib list is empty.
			 */
			gan_list_destroy(&lib_list);
			list_created = 0;
		}
	}

	return (0);
}
