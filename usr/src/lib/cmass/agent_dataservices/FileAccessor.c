/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident	"@(#)FileAccessor.c	1.5	08/05/20 SMI"

#include <stdlib.h>
#include <stdio.h>
#include <jniutil.h>
#include <sys/stat.h>
#include "com/sun/cluster/agent/dataservices/utils/FileAccessor.h"

/*
 * Class:     com_sun_cluster_agent_dataservices_utils_FileAccessor
 * Method:    getFileMode
 * Signature: (Ljava/lang/String;)J
 */
JNIEXPORT jlong
JNICALL Java_com_sun_cluster_agent_dataservices_utils_\
FileAccessor_getFileMode
	(JNIEnv * env, jobject this, jstring filePathName) {

	struct stat file_info;
	int ret_val;
	int64_t fileMode;

	const char *fName = (*env)->GetStringUTFChars(env, filePathName, NULL);
	ret_val = stat(fName, &file_info);
	(*env)->ReleaseStringUTFChars(env, filePathName, fName);
	fileMode = file_info.st_mode;

	return (jlong) (fileMode);
}

/*
 * Class:     com_sun_cluster_agent_dataservices_utils_FileAccessor
 * Method:    getFileOwner
 * Signature: (Ljava/lang/String;)J
 */
JNIEXPORT jlong
JNICALL Java_com_sun_cluster_agent_dataservices_utils_FileAccessor_getFileOwner
	(JNIEnv * env, jobject this, jstring filePathName) {

	struct stat file_info;
	int ret_val;
	int64_t fileOwner;

	const char *fName = (*env)->GetStringUTFChars(env, filePathName, NULL);
	ret_val = stat(fName, &file_info);
	(*env)->ReleaseStringUTFChars(env, filePathName, fName);
	fileOwner = file_info.st_uid;

	return (jlong) (fileOwner);
}
