/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident	"@(#)VfstabFileAccessor.c	1.13	08/05/20 SMI"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <jniutil.h>
#include <sys/vfstab.h>
#include "com/sun/cluster/agent/dataservices/utils/VfstabFileAccessor.h"

#define	xstrdup(x)	(x?strdup(x):NULL)

void add_to_list(JNIEnv *env, jobject *this, struct vfstab *pVfs);
void convertVfsStruct(JNIEnv *env, jobject *this, struct vfstab *pVfs,
			jobject *obj_vfsStruct, int cflag);

/*
 * Class:     com_sun_cluster_agent_dataservices_utils_VfstabFileAccessor
 * Method:    executeOperation
 * Signature: (Ljava/lang/String;Ljava/lang/String;
 *		Lcom/sun/cluster/agent/dataservices/utils/VfsStruct;)V
 */
JNIEXPORT void JNICALL
Java_com_sun_cluster_agent_dataservices_\
utils_VfstabFileAccessor_executeOperation
(JNIEnv *env, jobject this, jstring operation,
	jstring jData, jobject jVfsStruct) {

    FILE *fp_vfstab;
    struct vfstab *pVfs, *cVfsStruct;
    const char *cOperation;
    const char *cData;
    int ret_val = 0;
	/*
	* Get Operation names from Util.java
	*/

	jstring jStr;
	const char *vfstab_op_readFully;
	const char *vfstab_op_appendLine;
	const char *vfstab_op_getvfsfile;
	const char *vfstab_op_getvfsspec;
	const char *vfstab_op_getvfsany;

	/* Get Util class */
	jclass class_Util = (*env)->FindClass(env,
			"com/sun/cluster/agent/dataservices/utils/Util");

	/* Get Operation field Ids */
	jfieldID op_readFully_id = (*env)->GetStaticFieldID(env,
					class_Util,
					"vfstab_op_readFully",
					"Ljava/lang/String;");
	jfieldID op_appendLine_id = (*env)->GetStaticFieldID(env,
					class_Util,
					"vfstab_op_appendLine",
					"Ljava/lang/String;");
	jfieldID op_getvfsfile_id = (*env)->GetStaticFieldID(env,
					class_Util,
					"vfstab_op_getvfsfile",
					"Ljava/lang/String;");
	jfieldID op_getvfsspec_id = (*env)->GetStaticFieldID(env,
					class_Util,
					"vfstab_op_getvfsspec",
					"Ljava/lang/String;");
	jfieldID op_getvfsany_id = (*env)->GetStaticFieldID(env,
					class_Util,
					"vfstab_op_getvfsany",
					"Ljava/lang/String;");
	/* Get operation field values */
	jStr = (jstring)(*env)->GetStaticObjectField(env,
			class_Util, op_readFully_id);
	vfstab_op_readFully = (*env)->GetStringUTFChars(env, jStr, NULL);

	jStr = (jstring)(*env)->GetStaticObjectField(env,
			class_Util, op_appendLine_id);
	vfstab_op_appendLine = (*env)->GetStringUTFChars(env, jStr, NULL);

	jStr = (jstring)(*env)->GetStaticObjectField(env,
			class_Util, op_getvfsfile_id);
	vfstab_op_getvfsfile = (*env)->GetStringUTFChars(env, jStr, NULL);

	jStr = (jstring)(*env)->GetStaticObjectField(env,
			class_Util, op_getvfsspec_id);
	vfstab_op_getvfsspec = (*env)->GetStringUTFChars(env, jStr, NULL);

	jStr = (jstring)(*env)->GetStaticObjectField(env,
			class_Util, op_getvfsany_id);
	vfstab_op_getvfsany = (*env)->GetStringUTFChars(env, jStr, NULL);

	/* open vfstab file in read/write mode */
	if ((fp_vfstab = fopen(VFSTAB, "r+")) == NULL)
		return;

	/* Get the given operation name */
	cOperation = (*env)->GetStringUTFChars(env, operation, NULL);


	do {
	pVfs = (struct vfstab*) malloc(sizeof (struct vfstab));

	if (strcmp(cOperation, vfstab_op_readFully) == 0) {
		ret_val = getvfsent(fp_vfstab, pVfs);
		if (ret_val != 0) {
			(*env)->ReleaseStringUTFChars(env,
				operation, cOperation);
			free(pVfs);
			break;
		}
	} else if (strcmp(cOperation, vfstab_op_getvfsfile) == 0) {
		cData = (*env)->GetStringUTFChars(env, jData, NULL);
		ret_val = getvfsfile(fp_vfstab, pVfs, xstrdup(cData));
		if (ret_val != 0) {
			(*env)->ReleaseStringUTFChars(env,
				operation, cOperation);
			(*env)->ReleaseStringUTFChars(env,
				jData, cData);
			free(pVfs);
			break;
		}
	} else if (strcmp(cOperation, vfstab_op_getvfsspec) == 0) {
		cData = (*env)->GetStringUTFChars(env, jData, NULL);
		ret_val = getvfsspec(fp_vfstab, pVfs, xstrdup(cData));
		if (ret_val != 0) {
			(*env)->ReleaseStringUTFChars(env,
				operation, cOperation);
			(*env)->ReleaseStringUTFChars(env,
				jData, cData);
			free(pVfs);
			break;
		}
	} else if (strcmp(cOperation, vfstab_op_getvfsany) == 0) {
		cVfsStruct = (struct vfstab*) malloc(sizeof (struct vfstab));
		convertVfsStruct(env, &this, cVfsStruct, &jVfsStruct, 0);
		ret_val = getvfsany(fp_vfstab, pVfs, cVfsStruct);
		if (ret_val != 0) {
			(*env)->ReleaseStringUTFChars(env,
				operation, cOperation);
			(*env)->DeleteLocalRef(env, jVfsStruct);
			free(cVfsStruct);
			free(pVfs);
			break;
		}
	} else if (strcmp(cOperation, vfstab_op_appendLine) == 0) {
		cVfsStruct = (struct vfstab*) malloc(sizeof (struct vfstab));
		convertVfsStruct(env, &this, cVfsStruct, &jVfsStruct, 0);
		fseek(fp_vfstab, 0, SEEK_END);
		putvfsent(fp_vfstab, cVfsStruct);
		(*env)->ReleaseStringUTFChars(env, operation, cOperation);
		(*env)->DeleteLocalRef(env, jVfsStruct);
		free(cVfsStruct);
		free(pVfs);
		break;
	}

	/* Add the vfstab struct to the list */
	add_to_list(env, &this, pVfs);

	/* Free memory */
	free(pVfs);
	} while (1);

	/* Free memory */
	(*env)->DeleteLocalRef(env, jStr);

	/* Close file */
	fclose(fp_vfstab);
}


/*
 * Utility function to add given vfstab struct to the vfstab sruct list as
 * maintained by the VfstabFileAccessor MBean
 */
void add_to_list(JNIEnv *env, jobject *this, struct vfstab *pVfs) {


	/* Get VfsStruct class constructor */
	jclass class_VfsStruct = (*env)->FindClass(env,
			"com/sun/cluster/agent/dataservices/utils/VfsStruct");
	jmethodID id_VfsStruct = (*env)->GetMethodID(env,
					class_VfsStruct,
					"<init>",
					"()V");

	/* Get VfsTabFileAccessor class - addToList method */
	jclass class_this = (*env)->GetObjectClass(env, *this);
	jmethodID id_addToList = (*env)->GetMethodID(env,
		class_this,
		"addToList",
		"(Lcom/sun/cluster/agent/dataservices/utils/VfsStruct;)V");

	jobject obj_vfsStruct = NULL;

	/* Create new VfsStruct object */
	obj_vfsStruct = (*env)->NewObject(env,
			class_VfsStruct, id_VfsStruct);

	convertVfsStruct(env, this, pVfs, &obj_vfsStruct, 1);

	/* Add this filled vfsStruct object to the list of vfsStruct objects */
	(*env)->CallVoidMethod(env, *this, id_addToList,
				obj_vfsStruct);

	/* Free memory */
	(*env)->DeleteLocalRef(env, obj_vfsStruct);
}



/*
 * Utility function for conversion between java/c vfsStruct.
 * cFlag = 1 : convert c vfstruct to java vfsStruct
 * cFlag = 0 : convert java vfstruct to c vfsStruct
 */
void convertVfsStruct(JNIEnv *env,
			jobject *this,
			struct vfstab *pVfs,
			jobject *obj_vfsStruct,
			int cFlag) {

	/* Get VfsStruct class */
	jclass class_VfsStruct = (*env)->FindClass(env,
			"com/sun/cluster/agent/dataservices/utils/VfsStruct");

	/* Get vfstab field Ids */
	jfieldID vfs_special_id = (*env)->GetFieldID(env,
					class_VfsStruct,
					"vfs_special",
					"Ljava/lang/String;");
	jfieldID vfs_fsckdev_id = (*env)->GetFieldID(env,
					class_VfsStruct,
					"vfs_fsckdev",
					"Ljava/lang/String;");
	jfieldID vfs_mountp_id = (*env)->GetFieldID(env,
					class_VfsStruct,
					"vfs_mountp",
					"Ljava/lang/String;");
	jfieldID vfs_fstype_id = (*env)->GetFieldID(env,
					class_VfsStruct,
					"vfs_fstype",
					"Ljava/lang/String;");
	jfieldID vfs_fsckpass_id = (*env)->GetFieldID(env,
					class_VfsStruct,
					"vfs_fsckpass",
					"Ljava/lang/String;");
	jfieldID vfs_automnt_id = (*env)->GetFieldID(env,
					class_VfsStruct,
					"vfs_automnt",
					"Ljava/lang/String;");
	jfieldID vfs_mntopts_id = (*env)->GetFieldID(env,
					class_VfsStruct,
					"vfs_mntopts",
					"Ljava/lang/String;");
	jstring jStr;
	const char *cStr;

	if (cFlag) {
		/* Copy values from c vfsStruct to java vfsStruct */

		jStr = (*env)->NewStringUTF(env, pVfs->vfs_special);
		(*env)->SetObjectField(env, *obj_vfsStruct,
				vfs_special_id, jStr);
		(*env)->DeleteLocalRef(env, jStr);

		jStr = (*env)->NewStringUTF(env, pVfs->vfs_fsckdev);
		(*env)->SetObjectField(env, *obj_vfsStruct,
				vfs_fsckdev_id, jStr);
		(*env)->DeleteLocalRef(env, jStr);

		jStr = (*env)->NewStringUTF(env, pVfs->vfs_mountp);
		(*env)->SetObjectField(env, *obj_vfsStruct,
				vfs_mountp_id, jStr);
		(*env)->DeleteLocalRef(env, jStr);

		jStr = (*env)->NewStringUTF(env, pVfs->vfs_fstype);
		(*env)->SetObjectField(env, *obj_vfsStruct,
				vfs_fstype_id, jStr);
		(*env)->DeleteLocalRef(env, jStr);

		jStr = (*env)->NewStringUTF(env, pVfs->vfs_fsckpass);
		(*env)->SetObjectField(env, *obj_vfsStruct,
				vfs_fsckpass_id, jStr);
		(*env)->DeleteLocalRef(env, jStr);

		jStr = (*env)->NewStringUTF(env, pVfs->vfs_automnt);
		(*env)->SetObjectField(env, *obj_vfsStruct,
				vfs_automnt_id, jStr);
		(*env)->DeleteLocalRef(env, jStr);

		jStr = (*env)->NewStringUTF(env, pVfs->vfs_mntopts);
		(*env)->SetObjectField(env, *obj_vfsStruct,
				vfs_mntopts_id, jStr);
		(*env)->DeleteLocalRef(env, jStr);

	} else {
	/* Copy values from java vfsStruct to c vfsStruct */

	jStr = (jstring)(*env)->GetObjectField(env,
				*obj_vfsStruct, vfs_special_id);
	if (jStr == NULL) pVfs->vfs_special = NULL;
	else {
		cStr = (*env)->GetStringUTFChars(env, jStr, NULL);
		pVfs->vfs_special = xstrdup(cStr);
		(*env)->ReleaseStringUTFChars(env, jStr, cStr);
	}
	(*env)->DeleteLocalRef(env, jStr);

	jStr = (jstring)(*env)->GetObjectField(env,
				*obj_vfsStruct, vfs_fsckdev_id);
	if (jStr == NULL) pVfs->vfs_fsckdev = NULL;
	else {
		cStr = (*env)->GetStringUTFChars(env, jStr, NULL);
		pVfs->vfs_fsckdev = xstrdup(cStr);
		(*env)->ReleaseStringUTFChars(env, jStr, cStr);
	}
	(*env)->DeleteLocalRef(env, jStr);

	jStr = (jstring)(*env)->GetObjectField(env,
				*obj_vfsStruct, vfs_mountp_id);
	if (jStr == NULL) pVfs->vfs_mountp = NULL;
	else {
		cStr = (*env)->GetStringUTFChars(env, jStr, NULL);
		pVfs->vfs_mountp = xstrdup(cStr);
		(*env)->ReleaseStringUTFChars(env, jStr, cStr);
	}
	(*env)->DeleteLocalRef(env, jStr);

	jStr = (jstring)(*env)->GetObjectField(env,
				*obj_vfsStruct, vfs_fstype_id);
	if (jStr == NULL) pVfs->vfs_fstype = NULL;
	else {
		cStr = (*env)->GetStringUTFChars(env, jStr, NULL);
		pVfs->vfs_fstype = xstrdup(cStr);
		(*env)->ReleaseStringUTFChars(env, jStr, cStr);
	}
	(*env)->DeleteLocalRef(env, jStr);

	jStr = (jstring)(*env)->GetObjectField(env,
				*obj_vfsStruct, vfs_fsckpass_id);
	if (jStr == NULL) pVfs->vfs_fsckpass = NULL;
	else {
		cStr = (*env)->GetStringUTFChars(env, jStr, NULL);
		pVfs->vfs_fsckpass = xstrdup(cStr);
		(*env)->ReleaseStringUTFChars(env, jStr, cStr);
	}
	(*env)->DeleteLocalRef(env, jStr);

	jStr = (jstring)(*env)->GetObjectField(env,
				*obj_vfsStruct, vfs_automnt_id);
	if (jStr == NULL) pVfs->vfs_automnt = NULL;
	else {
		cStr = (*env)->GetStringUTFChars(env, jStr, NULL);
		pVfs->vfs_automnt = xstrdup(cStr);
		(*env)->ReleaseStringUTFChars(env, jStr, cStr);
	}
	(*env)->DeleteLocalRef(env, jStr);

	jStr = (jstring)(*env)->GetObjectField(env,
				*obj_vfsStruct, vfs_mntopts_id);
	if (jStr == NULL) pVfs->vfs_mntopts = NULL;
	else {
		cStr = (*env)->GetStringUTFChars(env, jStr, NULL);
		pVfs->vfs_mntopts = xstrdup(cStr);
		(*env)->ReleaseStringUTFChars(env, jStr, cStr);
	}
	(*env)->DeleteLocalRef(env, jStr);
	}
}

/*
 * Class:	com_sun_cluster_agent_dataservices_utils_VfstabFileAccessor
 * Method:	runCommand
 * Signature:	(Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL
Java_com_sun_cluster_agent_dataservices_utils_VfstabFileAccessor_runCommand
(JNIEnv *env, jclass this, jstring cmdname) {
	const char *cmd = (*env)->GetStringUTFChars(env, cmdname, NULL);
	system(cmd);
}
