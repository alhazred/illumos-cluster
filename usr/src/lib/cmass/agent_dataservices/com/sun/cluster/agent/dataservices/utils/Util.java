/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)Util.java	1.78	08/11/13 SMI"
 */

package com.sun.cluster.agent.dataservices.utils;

// J2SE
import java.io.IOException;
import java.io.Serializable;

// JMX
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;

// Cacao
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.agent.MBeanServerInvocationHandler;

// Agent
import com.sun.cluster.agent.cluster.ClusterMBean;
import com.sun.cluster.agent.dataservices.DataServicesModule;
import com.sun.cluster.agent.node.NodeMBean;
import com.sun.cluster.common.TrustedMBeanModel;


public class Util {

    private static String clusterName;

    public static final int NOTCONFIGURED = -1; // Initial state before
                                                // configuration
    public static final int CONFIGURED = 0; // Successful configuration
    public static final int FAILED_ROLLBACKED = 1; // Configuration failed but
                                                   // rollback succeeded
    public static final int ROLLBACK_FAILED = 2; // Rollback failed
    public static final int FAILED_NOTROLLBACKED = 3; // Intentionally not
                                                      // rollbacked

    public static final String ROLLBACK_SUCCESS = "ROLLBACK_SUCCESS";
    public static final String ROLLBACK_FAIL = "ROLLBACK_FAIL";
    public static final String IO_ERROR = "IO_ERROR";

    /** Used as a prefix in the session attributes for our map */
    private static final String RBAC_AUTHS = "RBAC_AUTHS : ";

    /** Used as a prefix in the session attributes for boolean isRoot */
    private static final String RBAC_ROOT = "RBAC_ROOT : ";

    public static final String WIZARD_STATE = "WIZARD_STATE";

    public static final String NODE_TO_CONNECT = "NODE_TO_CONNECT";

    public static final String ROOT_PATH = "/root/";
    public static final String ZONE_ROOTPATH = "ZONE_ROOTPATH";
    public static final String ZONE_ENABLE = "ZONE_ENABLE";
    public static final String PLUS_PLUS = "++";

    public static final String FAILBACK_PROP = "failback";

    // The following two constants are used in the GUI wizards
    // to display warning messages to the user when validation fails
    public static final String ALERT_DISPLAYED = "DisplayAlertBox";
    public static final String VALUE_CHANGED = "ValueChanged";

    // Constants shared between all logical hostname selection panel
    // and hastorageplus selection panels in all the wizards
    public static final String CREATE_NEW = "CreateNewOption";
    public static final String SEL_RS = "SelectedResources";
    public static final String SEL_RG = "SelectedRG";

    // Constants shared between all logical hostname selection panel
    // in all the wizards
    public static final String SEL_HOSTNAMES = "SelectedHostnames";

    // Used to send a RG value to be used for discovery of logical hostname
    // resources
    public static final String RG_PARAMETER = "RGParameter";

    // Constants shared between all storage selection panel
    // in all the wizards
    public static final String SEL_FILESYSTEMS = "SelectedFilesystems";
    public static final String SEL_DEVICES = "SelecetedDevices";

    // HASP constants shared between HAStoragePlusMBean and the the HASP wizards
    // tags used by ServiceInfo
    public static final String NAME_TAG = "_Name";
    public static final String SERVER_GROUP = "ServerGroup";
    public static final String RGPROP_TAG = "_rgProperties";
    public static final String SYSPROP_TAG = "_sysProperties";
    public static final String EXTPROP_TAG = "_extProperties";
    public static final String ZC_TAG = "_zoneCluster";
    public static final String BC_TAG = "_baseCluster";
    public static final String NODELIST = "nodelist";
    public static final String AVAIL_NODELIST = "available_nodelist";
    public static final String PATHPREFIX = "Pathprefix";
    public static final String HASP_RES_ID = "HAStoragePlus";
    public static final String QFS_RES_ID = "SharedQFS";
    public static final String LH_RES_ID = "LogicalHost";
    public static final String RG_AFFINITY = "RG_affinities";
    public static final String POSITIVE = "+";
    public static final String NEGATIVE = "-";

    // Prefix used when specifying inter-cluster dependencies
    public static final String GLOBAL_PREFIX = "global:";

    // Keyword used to identify all volumes in a ZC
    public static final String ALL = "--All--";

    // Constants for accessing vfstab file
    public static final String VFSTAB_FILE = "/etc/vfstab";
    public static final String vfstab_op_readFully = "readFully";
    public static final String vfstab_op_appendLine = "appendLine";
    public static final String vfstab_op_getvfsfile = "getvfsfile";
    public static final String vfstab_op_getvfsspec = "getvfsspec";
    public static final String vfstab_op_getvfsany = "getvfsany";

    // Constants for Command Generator
    public static final String RESOURCE_DEPENDENCIES = "Resource_dependencies";

    public static final String RG_POSTFIX = "-rg";
    public static final String RS_POSTFIX = "-rs";

    public static final String RG_PREFIX_KEY = "RG_Prefix_Key";
    public static final String RS_PREFIX_KEY = "RS_Prefix_Key";

    public static final String RTNAME = "rtname";

    // Constant used in RS & RG Name generation
    public static final String LHNAME_PREFIX = "lh";
    public static final String SANAME_PREFIX = "sa";

    // NFS constants shared between NFSMBean, NFS wizards and NFSServiceConfig
    // class
    public static final String LH_NAMES = "LH_Names";
    public static final String LH_RTNAME = "SUNW.LogicalHostname";
    public static final String SH_RTNAME = "SUNW.SharedAddress";
    public static final String LH_HOSTLIST_EXT_PROP = "HostnameList";
    public static final String HASP_NAMES = "HASP_NAMES";
    public static final String HASP_RTNAME = "SUNW.HAStoragePlus";
    public static final String QFS_RTNAME = "SUNW.qfs";
    public static final String NFS_HASP_MTPTS = "nfsHaspMountPoints";
    public static final String NFS_SHARE_OPT = "NFS_SHARE_OPT";
    public static final String NFS_SHARE_OPT_DATA = "NFS_SHARE_OPT_DATA";
    public static final String NFS_PATHPREFIX_DATA = "NFS_PATHPREFIX_DATA";
    public static final String NFS_DIR_DATA = "data";
    public static final String NFS_DIR_ADMIN = "admin";
    public static final String NFS_RESOURCE_GROUP = "HANFSGroup";
    public static final String NFS_LH_RID = "LogicalHostResource";
    public static final String NFS_LH_NAME = "LogicalHostResourceName";
    public static final String NFS_HASP_RID = "StorageResource";
    public static final String NFS_RID = "NFSResource";
    public static final String NFS_SHARE_CMDS = "NFSShareCmds";
    public static final String NFS_HASPWIZSEL1 = "haspwizselection1";
    public static final String NFS_HASPWIZSEL2 = "haspwizselection2";
    public static final String NFS_FILEPREFIX = "dfstab.";
    public static final String NFS_DIRPOSTFIX = "SUNW.nfs";
    public static final String NFS_APPCONFDATA = "NFS_APPCONFDATA";
    public static final String NFS_OP_MSG = "NFS_OP_MSG";
    public static final String NFS_DFSTAB_FILE = "NFS_dfstab_file";
    public static final String SHARE_OPT_SECURITY = "share_opt_security";
    public static final String SHARE_OPT_ACCESS = "share_opt_access";
    public static final String SHARE_OPT_NOSUID = "share_opt_nosuid";
    public static final String SHARE_OPT_DESC = "share_opt_desc";
    public static final String SHARE_OPT_PATH = "share_opt_path";
    public static final String SHARE_OPT_MTPT = "share_opt_mtpt";
    public static final String SHARE_OPT_NOSUID_YVAL = "yes";
    public static final String SHARE_OPT_NOSUID_NVAL = "no";

    // Permissions given to share directory if created through nfs wizard
    public static final String NFS_SHAREDIR_PERMISSION = "777";

    // Permissions given to PathPrefix directory
    public static final String NFS_PATHPREFIXDIR_PERMISSION = "755";

    // Permissions given to dfstab file for nfs resource
    public static final String NFS_DFSTABFILE_PERMISSION = "600";

    // Oracle RAC constants shared between OracleRACMBean, OracleRAC wizards
    // and OracleRAC Server Config class.
    public static final String ORF_STORAGE_MGMT_SCHEMES =
        "ORF_STORAGE_MGMT_SCHEMES";
    public static final String SVM = "SVM";
    public static final String VXVM = "VXVM";
    public static final String QFS_SVM = "QFS+SVM";
    public static final String QFS_HWRAID = "QFS+HWRAID";
    public static final String HWRAID = "HWRAID";
    public static final String NAS = "NAS";
    public static final String SUN_NAS = "SUN_NAS";
    public static final String CVM = "CVM";
    public static final String SCMD = "SCMD";
    public static final String ORF_RESOURCE_GROUP = "OracleRACFrameworkGroup";
    public static final String ORF_RID = "FrameworkResource";
    public static final String ORF_UDLM_RID = "UDLMResource";
    public static final String ORF_SVM_RID = "SVMResource";
    public static final String ORF_VXVM_RID = "CVMResource";
    public static final String FRMWRK_RT = "FRMWRK_RT";


    // Prefix used to differentiate between Server and Listener properties
    public static final String SERVER_PREFIX = "Server:";
    public static final String LISTENER_PREFIX = "Listener:";

    // Properties used by Oracle Server and Oracle Listener
    public static final String ORACLE_HOME = "ORACLE_HOME";
    public static final String SERVER_ORACLE_HOME = SERVER_PREFIX + ORACLE_HOME;
    public static final String LISTENER_ORACLE_HOME = LISTENER_PREFIX +
        ORACLE_HOME;
    public static final String ORACLE_SID = "ORACLE_SID";
    public static final String CONNECT_STRING = "Connect_string";
    public static final String SERVER_CONNECT_STRING = SERVER_PREFIX +
        CONNECT_STRING;
    public static final String ALERT_LOG_FILE = "Alert_log_file";
    public static final String SERVER_ALERT_LOG_FILE = SERVER_PREFIX +
        ALERT_LOG_FILE;
    public static final String PARAMETER_FILE = "Parameter_file";
    public static final String SERVER_PARAMETER_FILE = SERVER_PREFIX +
        PARAMETER_FILE;
    public static final String DEBUG_LEVEL = "Debug_level";
    public static final String SERVER_DEBUG_LEVEL = SERVER_PREFIX + DEBUG_LEVEL;
    public static final String CUSTOM_ACTION_FILE = "Custom_action_file";
    public static final String SERVER_CUSTOM_ACTION_FILE = SERVER_PREFIX +
        CUSTOM_ACTION_FILE;
    public static final String LISTENER_DEBUG_LEVEL = LISTENER_PREFIX +
        DEBUG_LEVEL;
    public static final String USER_ENV = "User_env";
    public static final String SERVER_USER_ENV = SERVER_PREFIX + USER_ENV;
    public static final String LISTENER_USER_ENV = LISTENER_PREFIX + USER_ENV;
    public static final String LISTENER_NAME = "Listener_name";

    public static final String STORAGE_RSLIST = "Storage_Resource_List";
    public static final String HASP_RSLIST = "HAStoragePlus_Resource_List";
    public static final String LH_RSLIST = "LogicalHost_Resource_List";

    public static final String STORAGE_RGLIST = "Storage_RGList";
    public static final String LH_RGLIST = "LH_RGList";

    public static final String ORAHOME_HASP_RSLIST =
        "ORAHOME_HastoragePlus_RS_List";

    public static String ORA_SERVER_RTNAME = "SUNW.oracle_server";
    public static String ORA_LISTENER_RTNAME = "SUNW.oracle_listener";
    public static String ORA_SERVER_RES_ID = "OracleServer";
    public static String ORA_LISTENER_RES_ID = "OracleListener";

    public static final String NEW_HASP_RS = "NewHastorageResource";
    public static final String NEW_HASP_RSNAME = "NewHastorageResourceName";
    public static final String NEW_FSMOUNTPOINTS = "NewFilesystemMountPoints";

    public static final String NEW_LH_RS = "NewLogicalHostResource";
    public static final String NEW_LH_RSNAME = "NewLogicalHostResourceName";

    public static final String ZONE_SEPARATOR = ":";

    /* COLON delimiter */
    public static String COLON = ":";

    /* DOT */
    public static String DOT = ".";

    /* COMMA */
    public static String COMMA = ",";

    /* Separator - space   */
    public static String SEPARATOR = "     ";

    /* SPACE */
    public static String SPACE = " ";

    public static String DOUBLEQUOTE = "\"";

    public static String SINGLEQUOTE = "\'";

    /* SLASH */
    public static String SLASH = "/";

    public static String BACKSLASH = "\\";

    /* TWO */
    public static String TWO = "2";

    /* 'At' SYMBOL */
    public static String SYMB_AT = "@";

    /* RG_SUFFIX */
    public static String RG_SUFFIX = "-rg";

    /* RS_SUFFIX */
    public static String RS_SUFFIX = "-rs";

    /* UNDERSCORE */
    public static String UNDERSCORE = "_";

    /* DASH */
    public static String DASH = "-";

    /* NEWLINE */
    public static String NEWLINE = "\n";

    /* NEW */
    public static String NEW = "NEW";

    /* Domain name */
    public static String DOMAIN = "com.sun.cluster.agent.dataservices";
    public static String DOMAIN_NODE = "com.sun.cluster.agent.node";

    /* Strings used for Results panel */
    public static String CMDLIST = "commandList";
    public static String EXITSTATUS = "exitStatus";

    public static String GLOBAL_DISK = "Disk";
    public static String LOCAL_DISK = "Local_Disk";
    public static String STRING_DELIMITER = ", ";

    // Function names constants for ServiceConfig Interface
    public static final String AGGREGATE_VALUES = "aggregateValues";
    public static final String APPLICATION_CONFIGURATION =
        "applicationConfiguration";
    public static final String DISCOVER_POSSIBILITIES = "discoverPossibilities";
    public static final String GET_ALL_PROPERTIES = "getAllProperties";
    public static final String GET_DISCOVERABLE_PROPERTIES =
        "getDiscoverableProperties";
    public static final String VALIDATE_INPUT = "validateInput";


    /**
     * Constants for individual wizards and MBeans These are mostly Property
     * Names that are used between MBeans and the Wizards
     */

    // HASP constants shared between HAStoragePlusMBean and the the HASP wizards
    public static final String PROPERTY_NAMES = "PropertyNames";
    public static final String RG_NAME = "RG_Name";
    public static final String RG_NAME_W_EXCEPTIONS = "RG_Name_With_exceptions";
    public static final String RS_NAME = "RS_Name";
    public static final String RS_NAME_W_EXCEPTIONS = "RS_Name_With_exceptions";
    public static final String RG_NODELIST = "RG_NodeList";
    public static final String HASP_ALL_FILESYSTEMS = "FilesystemMountPoints";
    public static final String QFS_FILESYSTEM = "QFSFileSystem";
    public static final String ZPOOLS_PROPERTY = "Zpools";
    public static final String AFFINITY_ON = "AffinityOn";
    public static final String HASP_ALL_DEVICES = "GlobalDevicePaths";
    public static final String HASP_FSTYPE = "FileSystemType";
    public static final String MOUNT_POINT = "MountPoint";
    public static final String DEVICE_PATH = "DevicePath";
    public static final String FILE_SYSTEMS = "FileSystems";
    public static final String VFSTAB_ENTRIES = "VfstabEntries";
    public static final String ERROR_VFSTAB_VECTOR = "ErrorVfstabVector";
    public static final String VFSTAB_MAP_TO_ADD = "VfstabMapToAdd";
    public static final String HASP_DEVICE_GROUPS = "HASPDeviceGroups";
    public static final String HASP_RAW_DEVICES = "HASPRawDevices";
    public static final String DEVICE_GROUPS = "DeviceGroups";
    public static final String RAW_DEVICES = "RawDevices";
    public static final String GLOBAL = ",global,";
    public static final String GLOBAL_OPTION = "global";
    public static final String SHARED = "shared";
    public static final String M = ",m,";
    public static final String NO = "no";
    public static final String DEVPATH = "devpath";
    public static final String DSK = "/dsk/";
    public static final String RDSK = "/rdsk/";
    public static final String FSTYPE_VXFS = "vxfs";
    public static final String FSTYPE_SAMFS = "samfs";
    public static final String UFS_LOGGING = "logging";
    public static final String VXFS_LOGGING = "log";
    public static final String FORCEIO = "forcedirectio";

    // Constants for LogicalHost and SharedAddress MBeans and Wizards
    public static final String LOGICALHOST = "LogicalHost";
    public static final String SHAREDADDRESS = "SharedAddress";
    public static final String NETIFLIST = "NetIfList";
    public static final String HOSTNAMELIST = "HostnameList";
    public static final String RESOURCENAME = "Resource_Name";
    public static final String RESOURCEGROUPNAME = "ResourceGroup_Name";
    public static final String CHECKNAMESERVICE = "CheckNameService";
    public static final String AUXNODELIST = "AuxNodeList";
    public static final String EXCLUSIVEIP = "ExclusiveIP";
    public static final String IPMPGROUPMAP = "IPMPGroupMap";

    // Constants for Apache MBean and Wizard

    // Property Names that are specific to Apache
    // These names should be the same in the ApacheConfiguration Class
    public static final String APACHE_INSTALLATION_TYPE =
        "APACHE_INSTALLATION_TYPE";
    public static final String APACHE_HOME = "APACHE_HOME";
    public static final String APACHE_DOC = "APACHE_DOC";
    public static final String APACHE_CONF_FILE = "APACHE_CONF_FILE";
    public static final String APACHE_PORT = "APACHE_PORT";
    public static final String APACHE_VERSION = "APACHE_VERSION";
    public static final String PIDFILE_DIR = "PIDFILE_DIR";
    public static final String PIDFILE = "PIDFILE";
    public static final String CONF_FILE = "Conf_File";
    public static final String PORT_LIST = "Port_List";
    public static final String HOST_NAME = "Host_Name";
    public static final String BIN_DIR = "Bin_dir";
    public static final String PORT_PROP = "Port_list";
    public static final String APACHERESOURCE = "ApacheResource";
    public static final String STORAGERESOURCE = "StorageResource";
    public static final String SHAREDADDRESSGROUP = "SharedAddressGroup";

    // Property Names for Apache and Other Resources and their ResourceGroups
    public static final String SH_NAMES = "SH_NAMES";
    public static final String SH_RSLIST = "SharedAddress_Resource_List";
    public static final String SH_RGLIST = "SharedAddress_ResourceGroup_List";
    public static final String APACHE_MODE = "APACHE_MODE";
    public static final String APACHE_LISTEN = "APACHE_LISTEN";
    public static final String APACHE_MOUNT = "APACHE_MOUNT";
    public static final String APACHE_ZONE = "APACHE_ZONE";
    public static final String APACHE_RESOURCE_NAME = "APACHE_RESOURCE_NAME";
    public static final String APACHE_RESOURCE_GROUP = "APACHE_RESOURCE_GROUP";
    public static final String APACHE_SHAREDADDRESS_GROUP =
        "APACHE_SHAREDADDRESS_GROUP";
    public static final String LH_RESOURCE_NAME = "LH_RESOURCE_NAM";
    public static final String SH_RESOURCE_NAME = "SH_RESOURCE_NAME";
    public static final String STORAGE_RESOURCES = "STORAGE_RESOURCES";
    public static final String MAX_PRIMARIES = "Maximum_primaries";
    public static final String DESIRED_PRIMARIES = "Desired_primaries";
    public static final String RG_MODE = "RG_mode";
    public static final String SCALABLE_RG_MODE = "Scalable";
    public static final String ZONE_CLUSTER_NAME = "ZoneName";

    // Constants for SAP WebAs Agent Discovery in DataService Code
    public static final String SID = "SAP_SID";
    public static final String ENQ_PROF = "Enqueue_Profile";
    public static final String ENQ_LOC = "Enqueue_Server";
    public static final String SAP_USER = "SAP_User";
    public static final String ENQ_I_NO = "Enqueue_Instance_Number";
    public static final String SCS_I_NO = "SAP_Instance_Number";
    public static final String SCS_NAME = "SAP_Instance_Name";
    public static final String REP_PROF = "Replica_Profile";
    public static final String REP_LOC = "Replica_Server";

    // Constants for SAP WebAs Agent Discovery in Wizard Code
    public static final String CURRENT_HASP_RESOURCES =
        "configured_hasp_resources";
    public static final String CURRENT_LH_RESOURCES = "configured_lh_resources";
    public static final String SAP_CS_RG_NAME = "rg_name_for_cs";
    public static final String SAP_REP_RG_NAME = "rg_name_for_rep";
    public static final String ENQ_R_NAME = "r_name_for_enq";
    public static final String SCS_R_NAME = "r_name_for_scs";
    public static final String REP_R_NAME = "r_name_for_rep";
    public static final String ORADB_RTNAME = "SUNW.oracle_server";
    public static final String SAPDB_RTNAME = "SUNW.sapdb";
    public static final String SCS_RTNAME = "SUNW.sapscs";
    public static final String ENQ_RTNAME = "SUNW.sapenq";
    public static final String REP_RTNAME = "SUNW.saprepl";
    public static final String DB_RSLIST = "database_resources_for_sap";

    // IDs from the SAP Service Definition File
    public static final String CS_GRP = "HASAPENQGroup";
    public static final String ENQ_RS = "SAPENQResource";
    public static final String SCS_RS = "SAPSCSResource";
    public static final String CS_HASP_RS = "StorageResource";
    public static final String CS_LH_RS = "LogicalHostResource";
    public static final String REP_GRP = "HASAPREPGroup";
    public static final String REP_RS = "SAPREPResource";
    public static final String REP_HASP_RS = "ReplicaStorageResource";
    public static final String REP_QFS_RS = "ReplicaSharedQFS";
    public static final String REP_LH_RS = "ReplicaLogicalHostResource";

    // IDs for the SAP Wizard Values
    public static final String SAP_SID = "sap_sid";
    public static final String REP_COMPONENT = "replica_server";

    public static final String ENQ_INSTANCE_NUM = "enq_instance_number";
    public static final String ENQ_SERVER_LOC = "enq_server_location";
    public static final String ENQ_SERVER_PROFILE_LOC =
        "enq_server_profile_location";

    public static final String SCS_INSTANCE_NUM = "scs_instance_number";
    public static final String SCS_INSTANCE_NAME = "scs_instance_name";

    public static final String REP_SERVER_LOC = "rep_server_location";
    public static final String REP_SERVER_PROFILE_LOC =
        "rep_server_profile_location";

    public static final String ENQ_RES_NAME = "enq_resource_name";
    public static final String SCS_RES_NAME = "scs_resource_name";
    public static final String REP_RES_NAME = "rep_resource_name";
    public static final String REP_RG_NAME = "rep_rg";
    public static final String CS_RG_NAME = "rg_name_for_central_services";

    public static final String REP_SELECTED_FS = "selected_new_fs_for_rep";
    public static final String REP_SELECTED_GD = "selected_new_device_for_rep";
    public static final String REP_SELECTED_STORAGE =
        "selected_storage_resources_for_replica";
    public static final String REP_SELECTED_LH = "selected_lh_res_for replica";
    public static final String REP_NEW_HOSTS = "new_hosts_for_replica";

    public static final String SELECTED_LH = "selected_lh_resources";
    public static final String CS_NEW_HOSTS = "new_hosts_for_central_sevices";
    public static final String CS_SELECTED_FS =
        "selected_new_fs_for_central_sevices";
    public static final String CS_SELECTED_GD =
        "selected_new_device_for_central_sevices";
    public static final String CS_SELECTED_STORAGE =
        "selected_storage_resources_for_central_sevices";
    public static final String SELECTED_DB = "selected_database_res";

    public static final String REP_VFSTAB_ENTRIES = "vfstab_entries_for_rep";
    public static final String CS_VFSTAB_ENTRIES = "vfstab_entries_for_cs";
    public static final String CS_STORAGE_NAME = "new_storage_res_name_for_cs";
    public static final String CS_LH_NAME = "new_lh_name_for_cs";
    public static final String REP_STORAGE_NAME = "new_storage_res_name_for_rep";
    public static final String REP_LH_NAME = "new_lh_name_for_rep";
    public static final String CS_NETIFLIST = "ipmpGrp_for_cs";
    public static final String REP_NETIFLIST = "ipmpGrp_for_rep";
    public static final String SAPRESULT = "results_from_command";


    public static final String PREFIX = "prefix_for_name_generation";
    public static final String EXEMPT = "names_exempt";

    // Oracle RAC Wizard Model Keys
    public static final String KEY_PREFIX = "prefix_for_key";
    public static final String CLUSTER_SEL = "cluster_selection";
    public static final String BASE_CLUSTER = "0";
    public static final String ZONE_CLUSTER = "1";
    public static final String BASE_CLUSTER_KEY = "BaseClusterKey";
    public static final String ZONE_CLUSTER_LIST = "zone_cluster_list";
    public static final String SEL_ZONE_CLUSTER = "selected_zone_cluster";
    public static final String HOSTNODEMAP = "ZonePublicHostNameAndNodeMap";
    public static final String ZONE_CLUSTER_PROPS = "ZoneClusterProperties";
    public static final String ZONE_CLUSTER_NODELIST = "ZoneClusterNodeList";
    public static final String ZONE_CLUSTER_HOSTLIST = "ZoneClusterHostList";
    public static final String ZONE_HOSTNAME = "ZoneHostName";
    public static final String NODEHOSTMAP = "ZoneClusterNodeHostMap";
    public static final String BASE_CLUSTER_NODELIST = "BaseClusterNodeList";
    public static final String ZONE_CLUSTER_FSTAB_LIST = "ZoneClusterFSTabList";
    public static final String ZONE_CLUSTER_DEVTAB_LIST = "ZoneClusterDevTabList";
    public static final String ZONE_PATH = "ZonePath";

    public static final String NOTAVAIL_VOLUMES = "ListOfVolumesNotAvailable";
    public static final String AVAIL_VOLUMES = "ListOfVolumesAvailable";
    public static final String CONFMAP = "configuration_map_data";
    public static final String NODENAME = "NodeName";

    // Oracle RAC Wizard IDs
    public static final String SEL_SCAL_RS = "SelScalableResources";
    public static final String RAC_PROXY_RGNAME = "RAC_Proxy_RGName";
    public static final String RAC_PROXY_RSNAME = "RAC_Proxy_ResourceName";
    public static final String RAC_10G_RGID = "OracleRAC10GDatabaseGroup";
    public static final String SEL_STORAGE = "SELECTED_STORAGE";
    public static final String SEL_STORAGE_RG = "SELECTED_STORAGE_RG";
    public static String RAC_SERVER_RTNAME = "SUNW.scalable_rac_server";
    public static String RAC_LISTENER_RTNAME = "SUNW.scalable_rac_listener";
    public static String RAC_FRAMEWORK_RTNAME = "SUNW.rac_framework";
    public static String RAC_UDLM_RTNAME = "SUNW.rac_udlm";
    public static String RAC_RG_NODES = "nodelist_of_framework_resource";
    public static final String NINE = "9";
    public static final String TEN = "10";
    public static final int SERVER = 1;
    public static final int LISTENER = 2;
    public static final int BOTH_S_L = 3;

    public static final String RAC_VER = "oracle_version";
    public static final String DB_COMPONENTS = "oracle_components";
    public static final String SEL_ORACLE_SID = "sel_oracle_sid";
    public static final String SEL_ORACLE_HOME = "sel_oracle_home";
    public static final String RAC_NODELIST_COMPLETE = "is_all_nodes_complete";
    public static final String RAC_NODEIDX = "current_node_index";
    public static final String RAC_CURNODENAME = "current_nodename";


    public static final String ORACLE_ZONE_ENABLE = "ORACLE_ZONE_ENABLE";
    public static final String RAC_ZONE_ENABLE = "RAC_ZONE_ENABLE";

    // Oracle RAC Strings from Service Definition File
    public static final String RAC_STORAGE_GROUP_ID = "OracleRACStorageGroup";
    public static final String RAC9I_DBGROUP_ID = "Oracle9iDatabaseGroup";
    public static final String RAC9I_SCAL_SERVER_ID = "ScalableOracleServer";
    public static final String RAC9I_SCAL_LISTENER_ID =
        "ScalableOracleListener";
    public static final String RAC9I_LHGROUP_ID = "Oracle9iHostnamesGroup";
    public static final String RAC9I_LH_ID = "Oracle9iLogicalHost";


    // RAC 10G Wizard model keys
    public static final String CRS_HOME = "CRS_HOME";
    public static final String DB_NAME = "DB_NAME";
    public static final String ORA_VER = "ORA_VER";
    public static final String PRE_102 = "pre102";
    public static final String POST_102 = "post102";
    public static final String RAC10G_CURNODENAME = "RAC10G_CURNODENAME";
    public static final String RAC10G_NODEIDX = "RAC10G_NODEIDX";

    public static final String RAC10G_NODELIST_COMPLETE = "NodeListComplete";
    public static final String RAC_SERVER_PROXY_RTNAME =
        "SUNW.scalable_rac_server_proxy";
    public static final String SCAL_DEVGROUP_RTNAME = "SUNW.ScalDeviceGroup";
    public static final String SCAL_MOUNTPOINT_RTNAME = "SUNW.ScalMountPoint";

    public static final String WAIT_ZC_BOOT_INFO = "WaitZCBootInfo";
    public static final String WAIT_ZC_BOOT_RESOURCE = "WaitForZCBootResource";
    public static final String WAIT_ZC_BOOT_GROUP = "WaitForZCBootGroup";
    public static final String WAIT_ZC_BOOT_RTNAME = "SUNW.wait_zc_boot";
    public static final String ZCNAME_PROP = "ZCName";

    public static final String SCAL_DEVGROUP_RSLIST =
        "ScalableDeviceGroup_RSList";
    public static final String SCAL_MOUNTPOINT_RSLIST =
        "ScalableMountPoint_RSList";
    public static final String SCAL_MOUNTPOINTS = "ScalableMountPointDirs";
    public static final String SCAL_DEVGROUPS = "ScalableDeviceGroupNames";
    public static final String DISCOVERED_MOUNTPOINTS = "DiscoveredMountPoints";
    public static final String DISCOVERED_DEVGROUPS = "DiscoveredDeviceGroups";
    public static final String SC_SCALMOUNTPOINTS = "SCScalableMountPoints";
    public static final String SC_SCALDEVGROUPS = "SCScalableDeviceGroups";
    public static final String CRS_STORAGE_CHOICE = "SelStorageChoice";
    public static final String SELECT_EXISTING = "0";
    public static final String SELECT_NONE = "1";
    public static final String ALL_SEL_CRS_FILES = "SelCrsOcrVotingDiskLoc";
    public static final String CRS_OCR_VOTING_FILES = "CRS_OCR_VOTING_FILES";
    public static final String GDD_RSLIST = "GDD_ResourceList";
    public static final String SEL_CRS_HOME = "SELECTED_CRS_HOME";
    public static final String SEL_DB_NAME = "SELECTED_DB_NAME";
    public static final String CRS_FRAMEWORK_RTNAME = "SUNW.crs_framework";
    public static final String CRS_FRAMEWORK_RSNAME =
        "CRS_Framework_ResourceName";
    public static final String RAC_FRAMEWORK_RSNAME =
        "RAC_Framework_ResourceName";
    public static final String RAC_FRAMEWORK_RGNAME = "RAC_Framework_RGName";
    public static final String CRS_FRAMEWORK_PRESENT = "CRS_Framework_Present";

    public static final String CRS_FRAMEWORK_RSID = "CRSFrameworkResource";
    public static final String STORAGE_GROUP_RSID1 = "OracleRACStorageGroup1";
    public static final String STORAGE_GROUP_RSID2 = "OracleRACStorageGroup2";
    public static final String SCAL_MOUNTPOINT_RSID = "ScalableMountPoint";
    public static final String SCAL_DEVGROUP_RSID = "ScalableDeviceGroup";
    public static final String RAC_10G_PROXY_RSID = "ScalableRacServerProxy";
    public static final String CONFIGURATION_TYPE = "ConfigurationType";
    public static final String RAC_10G = "RAC_10G";
    public static final String RAC_9I = "RAC_9I";
    public static final String RAC_SERVER_RS_NAME = "rac_server_rs_name";
    public static final String RAC_LISTENER_RS_NAME = "rac_listener_rs_name";
    public static final String RAC_RG_NAME = "rac_rg_name";
    public static final String RAC_LH_RG_PREFIX = "rac-lh";
    public static final String RAC_LISTERNER_PREFIX = "rac-listener1";

    // Oracle RAC Discovery Keys
    public static final String SCALABLE_SERVER_PROPS =
        "rac_scalable_server_props";
    public static final String SCALABLE_LISTENER_PROPS =
        "rac_scalable_listener_props";

    public static final String UNDO = "undo";
    public static final String YES_OP = "YES";
    public static final String NO_OP = "NO";
    public static final String VALUES = "values";
    public static final String LOCAL_NODE = "{local_node}";
    public static final String RS_DEPEND_OFFLINE_RESTART =
        "resource_dependencies_offline_restart";
    public static final String RS_WEAK_DEPENDENCIES =
        "Resource_dependencies_weak";

    public static final String SEL_LH_RS = "Selected_LH_Resource";
    public static final String SEL_LH_RSNAMES = "Selected_LH_Resource_Names";
    public static final String SEL_LH_RG = "Selected_LH_ResourceGroup";
    public static final String NEW_LH_RG = "new_rgfor_lh";


    // Constants for RAC Storage Wizard
    public static final String RAC_STATE_FILE_ABSENT = "state_file_absent";
    public static final String SVMDEVICEGROUP = "Multi-owner_SVM";
    public static final String RGM_SVMDEVICEGROUP = "RGM_OBAN";
    public static final String NONRGM_SVMDEVICEGROUP = "NONRGM_OBAN";
    public static final String VxVMDEVICEGROUP = "CVM";
    public static final String RGM_VxVMDEVICEGROUP = "RGM_VxVM";
    public static final String NONRGM_VxVMDEVICEGROUP = "NONRGM_VxVM";
    public static final String SELECTED_DGS = "Selected_DiskGroups";
    public static final String SELECTED_DG_RESOURCES = "Selected_DG_Resources";
    public static final String NEW_DG_RESOURCES = "New_DG_Resources";
    public static final String SELECTED_FS_RESOURCES = "Seletecd_FS_Resources";
    public static final String NEW_FS_RESOURCES = "New_FS_Resources";
    public static final String SQFS = "s-qfs";
    public static final String ALL_SQFS = "AllQFSFilesystems";
    public static final String RGM_SQFS = "RGM_SharedQFS";
    public static final String NONRGM_SQFS = "NONRGM_SQFS";
    public static final String RGM_NAS = "RGM_NAS";
    public static final String NONRGM_NAS = "NONRGM_NAS";
    public static final String SCAL_FS_RESOURCES = "ScalFSResources";
    public static final String SCAL_DG_RESOURCES = "ScalDGResources";
    public static final String STORAGEGROUP = "StorageGroup";
    public static final String DG_STORAGEGROUP = "StorageGroupForDevicegroup";
    public static final String FS_STORAGEGROUP = "StorageGroupForFilesystems";
    public static final String QFS_INFO = "QFSInformation";
    public static final String METASERVER_RESOURCE = "MetaserverResource";
    public static final String METASERVER_NODES = "MetaserverNodes";
    public static final String OBAN_UNDER_QFS = "ObanUnderQFS";
    public static final String DISKGROUP_NAME = "DiskGroupName";
    public static final String DISKGROUP_TYPE = "DiskGroupType";
    public static final String LOGICAL_DEVLIST = "LogicalDeviceList";
    public static final String METASERVER_GROUP = "MetaserverGroup";
    public static final String FILE_SYSTEM_TYPE = "FileSystemType";
    public static final String FILE_SYSTEM_NAME = "FileSystemName";
    public static final String QFS_FILE_SYSTEM = "QFSFileSystem";
    public static final String SCALABLE_SVM_RES = "ScalableSVMResource";
    public static final String SCALABLE_CVM_RES = "ScalableCVMResource";
    public static final String SCALABLE_QFS_RES = "ScalQFSResource";
    public static final String SCALABLE_NFS_RES = "ScalNASResource";
    public static final String SCALABLE_QFS_SVM_RES = "ScalQFSUnderSVMResource";
    public static final String SCALABLE_NAS_RES = "ScalNASResource";
    public static final String MOUNTPOINT_DIR = "MountPointDir";
    public static final String TARGET_FILE_SYSTEM = "TargetFileSystem";
    public static final String SCAL = "scal";
    public static final String PRIMARY_NODENAME = "PrimaryNodeName";
    public static final String SEL_DG_ENTRY_LIST = "SelectedDGEntryList";

    // to support zc:R/RG name format
    public static final int ZC_INDEX = 0;
    public static final int R_INDEX = 1;

    // these values correspond to the value returned by function
    // System.getProperty("os.arch")
    public static final String SPARC = "sparc";
    public static final String X86 = "x86";


    /**
     * Returns the name of the endpoint used to access the cluster.
     *
     * <p>For now this returns 'localhost' since we don't have a floating IP
     * address and we presume that the SPM is running on the cluster.
     *
     * @return  a <code>String</code> value
     */
    static public String getClusterEndpoint() {

        return "localhost";
    }

    /**
     * Returns the name of the endpoint used to access a give node
     *
     * <p>We cannot use the node name as an endpoint since it does not provide a
     * fully qualified name.
     *
     * @return  a <code>String</code> value
     */
    static public String getNodeEndpoint(String nodeName) {

        try {
            String split_str[] = nodeName.split(COLON);
            NodeMBean mbean = (NodeMBean) TrustedMBeanModel.getMBeanProxy(
                    DOMAIN_NODE, getClusterEndpoint(), NodeMBean.class,
                    split_str[0], false);

            return mbean.getPublicInetAddress();
        } catch (Exception e) {
            String split_str[] = nodeName.split(COLON);

            return split_str[0];
        }

    }

    /**
     * Returns the name of the endpoint used to access a give node
     *
     * <p>We cannot use the node name as an endpoint since it does not provide a
     * fully qualified name.
     *
     * @return  a <code>String</code> value
     */
    static public String getNodeEndpoint(JMXConnector connector,
        String nodeName) {

        try {
            String split_str[] = nodeName.split(COLON);
            NodeMBean mbean = (NodeMBean) TrustedMBeanModel.getMBeanProxy(
                    DOMAIN_NODE, connector, NodeMBean.class, split_str[0],
                    false);

            return mbean.getPublicInetAddress();
        } catch (Exception e) {
            String split_str[] = nodeName.split(COLON);

            return split_str[0];
        }

    }

    /**
     * Tells if the node is online. This method has to be normally called before
     * getNodeEndpoint to avoid timeout in case of other node reboot.
     *
     * @return  true if the node is online.
     */
    static public boolean isNodeOnline(String nodeName) {
        // NOTE : we can do getNodeEndpoint and isNodeOnline in one request

        try {
            String split_str[] = nodeName.split(COLON);
            NodeMBean mbean = (NodeMBean) TrustedMBeanModel.getMBeanProxy(
                    DOMAIN_NODE, getClusterEndpoint(), NodeMBean.class,
                    split_str[0], false);

            return mbean.isOnline();
        } catch (Exception e) {
            return false;
        }
    }

    public static String convertArraytoString(String xStrArray[]) {
	return (convertArraytoStringBuffer(xStrArray)).toString();
    }

    public static StringBuffer convertArraytoStringBuffer(String xStrArray[]) {
        StringBuffer strBuf = new StringBuffer();

        if ((xStrArray != null) && (xStrArray.length > 0)) {

            for (int i = 0; i < xStrArray.length; i++) {
                strBuf.append(xStrArray[i]);

                if (i != (xStrArray.length - 1)) {
                    strBuf.append(Util.COMMA);
                }
            }
        }

        return strBuf;
    }
}
