/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ClusterDiskGroupInfo.java	1.5	08/07/23 SMI"
 */


package com.sun.cluster.agent.dataservices.utils;


import java.util.List;
import java.util.Map;
import java.util.Iterator;


/**
 * The ClusterDiskGroupInfo class wraps the ClusterDiskGroup information for a
 * ClusterDiskGroup for a DataService
 *
 * <p>This class provides set of member variables and methods for configuring a
 * ClusterDiskGroup.
 */
public class ClusterDiskGroupInfo implements java.io.Serializable {

    private String clusterName = null; // Name of the cluster for which the disk group info is
				       // being stored
    private List diskGroupInfoList = null; // List of diskgroups in this

    /**
     * Creates a new instance of ClusterDiskGroupInfo
     */
    public ClusterDiskGroupInfo() {
    }

    /**
     * Creates a new instance of ClusterDiskGroupInfo with the required parameters
     */
    public ClusterDiskGroupInfo(String clusterName, List diskGroupInfoList) {
	this.clusterName = clusterName;
	this.diskGroupInfoList = diskGroupInfoList;
    }

    /**
     * Setter methods
     */
    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    public void setDiskGroupInfoList(List diskGroupInfoList) {
	this.diskGroupInfoList = diskGroupInfoList;
    }

    /**
     * Getter methods
     */
    public String getClusterName() {
        return this.clusterName;
    }

    public List getDiskGroupInfoList() {
	return this.diskGroupInfoList;
    }

    /**
     * toString method.
     */
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("\n\t");

        buffer.append("clusterName	 :" + clusterName + "\n\t");
        buffer.append("diskGroupInfoList :" + "\n\t\t");

        Iterator i = diskGroupInfoList.iterator();

        while (i.hasNext()) {
            buffer.append(i.next() + "\n\t\t");
        }

        buffer.append("\n");

        return buffer.toString();
    }
}
