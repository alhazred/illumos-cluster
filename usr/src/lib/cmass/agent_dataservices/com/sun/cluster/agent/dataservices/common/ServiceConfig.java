/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ServiceConfig.java 1.8     08/05/20 SMI"
 */

package com.sun.cluster.agent.dataservices.common;

// J2SE
import java.util.HashMap;
import java.util.Map;


/**
 * The ServiceConfig Interface specifies a set of operations to configure a
 * Dataservice. Every DataserviceMBean interface should extend this interface
 * and thus expose these operations.
 */
public interface ServiceConfig {

    /**
     * Gets the names of those properties for a Dataservice which could be
     * discovered without user intervention
     *
     * @return  String Array of Property Names
     */
    public String[] getDiscoverableProperties();

    /**
     * Gets the names of all properties to be set for configuring a Dataservice.
     *
     * @return  String Array of Property Names
     */
    String[] getAllProperties();

    /**
     * Discovers the possible values for the given set of properties and Helper
     * Data
     *
     * @param  propertyNames  String array of names of properties whose values
     * are to be discovered.
     * @param  helperData  The HelperData that would be used for discovering the
     * property values.
     *
     * @return  A Map containing propertyNames as Key with their discovered
     * values as Values.
     */
    HashMap discoverPossibilities(String propertyNames[], Object helperData);

    /**
     * Discovers the possible values for the given set of properties and Helper
     * Data
     *
     * @param  nodeList  String array of nodes on which the discovery has to be
     * run.
     * @param  propertyNames  String array of names of properties whose values
     * are to be discovered.
     * @param  helperData  The HelperData that would be used for discovering the
     * property values.
     *
     * @return  A Map containing propertyNames as Key with their discovered
     * values as Values.
     */
    HashMap discoverPossibilities(String nodeList[], String propertyNames[],
        Object helperData);

    /**
     * Validates the values for a set of properties.
     *
     * @param  propertyNames  Names of Properties whose values should be
     * validated
     * @param  userInputs  Map containing the data to be validated
     *
     * @return  An ErrorValue object containing the validation result.
     */
    ErrorValue validateInput(String propertyNames[], HashMap userInputs,
        Object helperData);

    /**
     * Aggregates the values for a given PropertyName.
     *
     * @param  propertyName  Name of the Property whose values are to be
     * aggregated
     * @param  unfilteredValues  Map of the nodenames and a array of discovered
     * values on that particular node.
     * @param  helperData  Optional HelperData
     *
     * @return  Array of String containing all the possible values of the
     * property in all of cluster.
     */
    String[] aggregateValues(String propertyName, Map unfilteredValues,
        Object helperData);

    /**
     * Perform the data service specific configuration operation on the cluster.
     *
     * @param  helperData  HelperData to do the Configuration
     *
     * @return  ErrorValue object representing any configuration errors.
     */
    ErrorValue applicationConfiguration(Object helperData);
}
