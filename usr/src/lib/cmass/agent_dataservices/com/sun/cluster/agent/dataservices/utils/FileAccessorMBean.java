/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)FileAccessorMBean.java 1.13     08/12/03 SMI"
 */

package com.sun.cluster.agent.dataservices.utils;

/**
 * This MBean interface defines File Accessor MBean.
 */
public interface FileAccessorMBean {

    /**
     * Read the entire contents of the file. This function would return an
     * exception if the size of the file specified exceeds FILE_SIZE_READ_LIMIT.
     * This parameter could be specified in Deployment descriptor of this
     * module.
     *
     * @param  filePathName  The name of the file prefixed with its absolute
     * path.
     *
     * @return  Returns an array of strings containing the file contents. Each
     * element of the array represents each line of the file. The tab in each
     * line of the file would be replaced by ":".
     */
    public String[] readFully(String filePathName);

    /**
     * Writes the given string array to the given file at the specified path
     * name. Each string in the array would be written as a new line in the end
     * of the file with each occurrence of ':' replaced by tab.
     *
     * @param  filePathName  The name of the file prefixed with its absolute
     * path.
     * @param  objToWrite  String array to be written to the file.
     */
    public void appendLine(String filePathName, String objToWrite[]);

    /**
     * Writes the given string array to the given file at the specified path
     * name. Each string in the array would be written as a new line in the end
     * of the file with the occurrences of ':' retained if replaceColon is
     * "false".
     *
     * @param  filePathName  The name of the file prefixed with its absolute
     * path.
     * @param  objToWrite  String array to be written to the file.
     * @param  replaceColon  boolean whether or not to replace the ":" with
     * "\t".
     */
    public boolean appendLine(String filePathName, String objToWrite[],
        boolean replaceColon);

    /**
     * Returns size of the file specified in bytes.
     *
     * @param  filePathName  The name of the file prefixed with its absolute
     * path.
     *
     * @return  Size of the specified file in bytes.
     */
    public long getFileSize(String filePathName);

    /**
     * Checks for the existence of the file.
     *
     * @param  filePathName  The name of the file prefixed with its absolute
     * path.
     *
     * @return  Boolean specifying the existence of the file.
     */
    public boolean ifFileExists(String filePathName);

    /**
     * Returns mode of the specified file.
     *
     * @param  filePathName  The name of the file prefixed with its absolute
     * path.
     *
     * @return  Mode of the specified file as long.
     */
    public long getFileMode(String filePathName);

    /**
     * Returns owner of the specified file.
     *
     * @param  filePathName  The name of the file prefixed with its absolute
     * path.
     *
     * @return  Owner of the file specified as long.
     */
    public long getFileOwner(String filePathName);

    /**
     * Creates the directory named by this abstract pathname, including any
     * necessary but nonexistent parent directories.
     *
     * @param  pathName  The pathname of the directory to be created
     *
     * @return  true if the command succeeds.
     */
    public boolean createDir(String pathName);

    /**
     * Creates the file named by this abstract pathname even if the file exists.
     *
     * @param  pathName  The pathname of the directory to be created
     *
     * @return  true if the command succeeds.
     */
    public boolean createAndOverwriteFile(String pathName);

    /**
     * @deprecated
     */
    public void appendLine(String filePathName, String stringToWrite);
}
