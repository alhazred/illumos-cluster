/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ErrorValue.java 1.11     08/05/20 SMI"
 */

package com.sun.cluster.agent.dataservices.common;

import java.io.Serializable;


/**
 * This Class encapsulates the result of any operation done while Configuring
 * the Dataservice.
 */
public class ErrorValue implements Serializable {

    private Boolean returnVal;
    private String errorStr;

    public ErrorValue() {

        // return Val false implies an error
        this(null, null);
    }

    public ErrorValue(Boolean returnVal, String errorStr) {
        this.returnVal = returnVal;
        this.errorStr = errorStr;
    }

    public Boolean getReturnValue() {
        return this.returnVal;
    }

    public void setReturnVal(Boolean returnVal) {
        this.returnVal = returnVal;
    }

    public String getErrorString() {
        return this.errorStr;

    }

    public void setErrorString(String errorStr) {
        this.errorStr = errorStr;
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("Return Value :" + returnVal);
        buffer.append("Error String :" + errorStr);

        return buffer.toString();
    }
}
