/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)ResourceInfo.java	1.31	09/04/22 SMI"
 */


package com.sun.cluster.agent.dataservices.utils;


// J2SE
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

// JMX
import javax.management.Attribute;
import javax.management.AttributeValueExp;
import javax.management.MBeanServer;
import javax.management.MBeanServerInvocationHandler;
import javax.management.ObjectName;
import javax.management.Query;
import javax.management.QueryExp;
import javax.management.StringValueExp;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;

// CACAO
import com.sun.cacao.ObjectNameFactory;

// CMASS
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.rgm.ResourceData;
import com.sun.cluster.agent.rgm.ResourceGroupData;
import com.sun.cluster.agent.rgm.ResourceTypeData;
import com.sun.cluster.agent.rgm.RgGroupMBean;
import com.sun.cluster.agent.rgm.RGroupMBean;
import com.sun.cluster.agent.rgm.RtGroupMBean;
import com.sun.cluster.agent.rgm.property.ResourcePropertyString;
import com.sun.cluster.common.TrustedMBeanModel;

/**
 * The ResourceInfo class wraps the resource information for a resource that is
 * to be configured for a DataService.
 */
public class ResourceInfo {

    private String resourceId = null; // Logical ID for this Resource
    private String rtName = null; // ResourceType for this Resource
    private String resourceName = null; // Name of the Resource in Cluster
    private List dependency = null; // List of Resources this Resource
                                    // depends on
    private List dependent = null; // List of resources that depend on
                                   // this resource
    private boolean optional; // is this Resource optional
    private boolean multiple; // is multiple instance allowed
    private ResourceGroupInfo resourceGroup = null; // ResourceGroup of this
                                                    // Resource
    private boolean resourceConfigured; // Whether this Resource is

    // already configured.
    private List systemProperties = null; // Resource System Properties
    private List extensionProperties = null; // Resource Extension Properties

    private boolean bringOnline; // Specifies whether to bring the

    // Resource online after creating
    // the commands for the resource

    private Map customConfigureMap = null; // Map that holds the information

    // to configure while this resource
    // is created

    private boolean isNewResource = true; // By Default create a New Resource
    private boolean resourceRollbacked; // Whether this resource has been

    // the name of the zone cluster or null for base cluster
    private String zoneClusterName = null;

    // mark runCmd as true
    private boolean runCmd = true; 

    // Rollbacked

    /**
     * Creates a new instance of ResourceInfo and associates it with a
     * ResourceGroup
     *
     * @param  resourceGroup  ResourceGroup of this Resource
     */
    public ResourceInfo(ResourceGroupInfo resourceGroup) {
        this.resourceGroup = resourceGroup;
    }

    /**
     * Getter method for the Resource's RTName as specified in the ServiceConfig
     * XML
     *
     * @return  RTName of the Resource as specified in the ServiceConfig XML
     */
    public String getRtName() {
        return rtName;
    }

    /**
     * Setter method for the Resource's RTName as specified in the ServiceConfig
     * XML
     *
     * @param  rtName  RTName of the Resource as specified in the ServiceConfig
     * XML
     */
    public void setRtName(String rtName) {
        this.rtName = rtName;
    }

    /**
     * Getter method for the Resource's Name as it is in SunCluster
     *
     * @return  Name of the Resource as specified or created in SunCluster
     */
    public String getResourceName() {
        return resourceName;
    }

    /**
     * Setter method for the Resource's Name as it is in SunCluster
     *
     * @param  resourceName  Name of the Resource as specified or created in
     * SunCluster
     */
    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    /**
     * Getter method for the Resource's Dependency
     *
     * @return  Dependencies of the Resource in comma seperated format
     */
    public List getDependency() {
        return dependency;
    }

    /**
     * Setter method for the Resource's Dependency
     *
     * @param  dependency  Dependencies of the Resource in comma seperated
     * format
     */
    public void setDependency(List dependency) {
        this.dependency = dependency;
    }

    /**
     * Getter method for the Resource's Dependent list
     *
     * @return  List of Resources dependent on this resource in comma seperated
     * format
     */
    public List getDependent() {
        return dependent;
    }

    /**
     * Setter method for the Resource's Dependent list
     *
     * @param  dependent  List of resources dependent on the Resource in comma
     * seperated format
     */
    public void setDependent(List dependent) {
        this.dependent = dependent;
    }

    /**
     * Checks whether Resource is optional for the DataService
     *
     * @return  <code>"true"</code> if it is optional <code>"false"</code> if it
     * is not optional
     */
    public boolean isOptional() {
        return optional;
    }

    /**
     * Set whether Resource is optional for the DataService
     *
     * @param  optional  <code>"true"</code> if it is optional <code>
     * "false"</code> if it is not optional
     */
    public void setOptional(boolean optional) {
        this.optional = optional;
    }

    /**
     * Returns the name of the zone cluster or null for base cluster
     *
     * @return zoneClusterName name of the zone cluster or null for base cluster.
     */
    public String getZoneClusterName() {
	return this.zoneClusterName;
    }

    /**
     * Sets the name of the zone cluster to the name specified.
     *
     * @param zoneClusterName name of the zone cluster or null for base cluster
     */
    public void setZoneClusterName(String zoneClusterName) {
	this.zoneClusterName = zoneClusterName;
    }    

    /**
     * toString()
     */
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("\n\t\t");
        buffer.append("id            :" + resourceId + "\n\t\t");
        buffer.append("rtName        :" + rtName + "\n\t\t");
        buffer.append("resourceName  :" + resourceName + "\n\t\t");
        buffer.append("dependency    :" + getDependencies() + "\n\t\t");
        buffer.append("dependent     :" + getDependents() + "\n\t\t");
        buffer.append("optional      :" + optional + "\n\t\t");
        buffer.append("multiple      :" + multiple + "\n\t\t");
        buffer.append("zoneClusterName      :" + zoneClusterName + "\n\t\t");
        buffer.append("resourceConfigured   :" + resourceConfigured + "\n\t\t");
        buffer.append("bringOnline          :" + bringOnline + "\n\t\t");
        buffer.append("customConfigureMap   :" + customConfigureMap + "\n\t\t");
        buffer.append("systemProperties     :" + systemProperties + "\n\t\t");
        buffer.append("extensionProperties  :" + extensionProperties +
            "\n\t\t");

        return buffer.toString();
    }

    /**
     * Returns the ResourceGroup Object that contains this Resource
     *
     * @return  the ResourceGroup object that contains this resource.
     */
    public ResourceGroupInfo getResourceGroup() {
        return resourceGroup;
    }

    /**
     * Sets the ResourceGroup Object that should contain this Resource
     *
     * @param  resourceGroup  the ResourceGroup object that contains this
     * resource as <code>ResourceGroupInfo</code>
     */
    public void setResourceGroup(ResourceGroupInfo resourceGroup) {
        this.resourceGroup = resourceGroup;
    }

    /**
     * Generates the command set for creating the Resource in the ResourceGroup
     * with the specified Properties and stores it in the commandSet list.
     *
     * @param  commandSet  The List where the generated Commands should be
     * stored.
     *
     * @throws  Exception
     */
    public void generateCommand(MBeanServer mbs, ObjectNameFactory rgmOnf,
        List commandSet) throws Exception {

        ExitStatus status[] = null;

        // If Resource is already configured do nothing
        if (resourceConfigured)
            return;

        // Check if Name is set,
        // if not and if Resource is Mandatory throw Exception
        if ((resourceName == null) && (optional == false)) {
            throw new Exception();
        } else if (resourceName == null) { // Resource is not configured,

            // do not generate commands
            return;
        }

        // Configure the Resource's dependencies first
        if (dependency != null) {
            Iterator i = dependency.iterator();

            while (i.hasNext()) {
                ResourceInfo resource = (ResourceInfo) i.next();

                List dependentList = resource.getDependent();

                if (dependentList == null) {
                    dependentList = new ArrayList();
                }

                if (!dependentList.contains(this)) {
                    dependentList.add(this);
                }

                resource.setDependent(dependentList);

                // If the resource is in the Same RG Configure it first.
                if (resource.getResourceGroup().getGroupName().equals(
                            resourceGroup.getGroupName())) {

                    try {
                        resource.generateCommand(mbs, rgmOnf, commandSet);
                    } catch (Exception e) {
                        throw e;
                    }

                    resource.setResourceConfigured(true);
                } else {

                    // Check whether the Resource is configured
                    if (!resource.isResourceConfigured()) {

                        // Check if Resource's Group is configured
                        if (!resource.getResourceGroup().isGroupConfigured()) {

                            // Generate Commands for that ResourceGroup
                            try {
                                resource.getResourceGroup().generateCommandSet(
                                    mbs, rgmOnf, commandSet);
                            } catch (Exception e) {
                                throw e;
                            }

                            resource.getResourceGroup().setGroupConfigured(
                                true);
                        }

                        // Configure the Resource.
                        try {
                            resource.generateCommand(mbs, rgmOnf, commandSet);
                        } catch (Exception e) {
                            throw e;
                        }

                        resource.setResourceConfigured(true);
                    }
                }
            }
        }

        // Generate the Commands

        // Mbean Call : Check whether resource with name exists,accordingly set
        // the isNewResource Flag
	

	RGroupMBean rGroupMBean = (RGroupMBean)
	    MBeanServerInvocationHandler.newProxyInstance(mbs,
		rgmOnf.getObjectName(RGroupMBean.class, null),
		RGroupMBean.class, false);

        if (rGroupMBean.isResourceRegistered(resourceName, null, zoneClusterName)) {
            isNewResource = false;
        }

        // Mbean Call : If new Resource, Generate Command to create the Resource
        // in its ResourceGroup
        try {

            if (isNewResource) {

                // Check whether the ResourceType is Registered

		RtGroupMBean rtGroupMBean = (RtGroupMBean)
		    MBeanServerInvocationHandler.newProxyInstance(mbs,
			rgmOnf.getObjectName(RtGroupMBean.class, null),
			RtGroupMBean.class, false);

		Attribute tmpAttr = new Attribute("Name", rtName);

		// read the RTR file information from the global zone
		HashMap retMap = rtGroupMBean.readData(null, tmpAttr);

		// get the keys and see the one that matches
		Set keySet = retMap.keySet();
		Iterator iterator = keySet.iterator();
		boolean found = false;
		ResourceTypeData rtData = null;
		String fqrtName = rtName;

		while (iterator.hasNext() && !found) {
		    fqrtName = (String)iterator.next();

		    if (fqrtName.startsWith(rtName)) {

			rtData = (ResourceTypeData)retMap.get(fqrtName);
			found = true;
		    }
		}

		if (rtData == null) {
		    throw new Exception("Resource Type not found in CMASS : " +
			rtName);
                }

		if (!rtGroupMBean.isRegistered(fqrtName, zoneClusterName)) {

		    status = rtGroupMBean.registerResourceType(fqrtName, zoneClusterName, runCmd);
		    commandSet.add(DataServicesUtil.convertArrayToString(
			    status[0].getCmdExecuted()));
                }

                // Store the dependencies in the System Property.
                if (dependency != null) {
                    String dependArray[] = getDependencyArray();

                    if (dependArray.length > 0) {
                        StringBuffer dependencies = null;

                        if (dependArray[0] != null) {
                            dependencies = new StringBuffer();
                            dependencies.append(dependArray[0]);
                        }

                        for (int i = 1; i < dependArray.length; i++) {

                            if (dependArray[i] != null) {

                                if (dependencies == null) {
                                    dependencies = new StringBuffer();
                                    dependencies.append(dependArray[i]);
                                } else {
                                    dependencies.append(",");
                                    dependencies.append(dependArray[i]);
                                }
                            }
                        }

                        if (dependencies != null) {
                            ResourcePropertyString rp =
                                new ResourcePropertyString(
                                    Util.RESOURCE_DEPENDENCIES,
                                    dependencies.toString());

                            if (systemProperties == null) {
                                systemProperties = new ArrayList();
                            }

                            systemProperties.add(rp);
                        }
                    }
                }

                // Create Resource with System and Extension Properties.
                status = rGroupMBean.addResource(resourceName,
                        getResourceGroup().getName(), fqrtName,
			zoneClusterName, systemProperties,
                        extensionProperties, runCmd);

                commandSet.add(DataServicesUtil.convertArrayToString(
                        status[0].getCmdExecuted()));

            } else { // Existing Resource

                if ((systemProperties != null) &&
                        (systemProperties.size() > 0)) {
                    status = rGroupMBean.changeSystemProperties(
			resourceName, systemProperties, zoneClusterName, runCmd);
                    commandSet.add(DataServicesUtil.convertArrayToString(
                            status[0].getCmdExecuted()));
                }

                if ((extensionProperties != null) &&
                        (extensionProperties.size() > 0)) {
                    status = rGroupMBean.changeExtProperties(
			resourceName, extensionProperties, zoneClusterName, runCmd);
                    commandSet.add(DataServicesUtil.convertArrayToString(
                            status[0].getCmdExecuted()));
                }

                // Generate Command to set the appropriate dependencies for
                // this resource.
                if (dependency != null) {

		    // Get the resourceData
		    ResourceData rData = rGroupMBean.getResourceData(
			resourceName, zoneClusterName);
		    // Get the Resource's Existing Dependencies
		    String existingDependencies[] = rData
                            .getStrongDependencies();

		    String finalDependencies[] = DataServicesUtil.concatenateArrays(
			existingDependencies, getDependencyArray());

		    // Remove duplicates from the array
		    List finalList = Arrays.asList(finalDependencies);

		    SortedSet dependencySet = new TreeSet();

		    for (Iterator i = finalList.iterator(); i.hasNext();) {
			dependencySet.add((String) i.next());
		    }

		    finalDependencies = new String[dependencySet.size()];
		    finalDependencies = (String[]) dependencySet.toArray(
			finalDependencies);
		    status = rGroupMBean.changeStrongDependencies(
			resourceName, finalDependencies, zoneClusterName, runCmd);

		    commandSet.add(DataServicesUtil.convertArrayToString(
			status[0].getCmdExecuted()));
		}
	    }
	} catch (CommandExecutionException e) {
	    e.printStackTrace();
	    status = e.getExitStatusArray();
	    System.out.println("Command Tried to Execute : \n" +
		DataServicesUtil.convertArrayToString(
		    status[0].getCmdExecuted()));
	    System.out.println(status[0].getErrStrings());
	    throw e;
	} catch (Exception e) {
	    e.printStackTrace();
	    throw e;
	}

        // If the bringOnline is set to true, the resource's resource group
        // would be brought online (if the resource group is already online
        // it would be restarted)

        // This step is done only if we need this resource to be online for
        // further resources to be created)
        if (bringOnline) {

            try {

		RgGroupMBean rgGroupMBean = (RgGroupMBean)
		    MBeanServerInvocationHandler.newProxyInstance(mbs,
			rgmOnf.getObjectName(RgGroupMBean.class, null),
			RgGroupMBean.class, false);

                status = rgGroupMBean.bringOnline(
		    getResourceGroup().getName(),
		    zoneClusterName, runCmd);
                commandSet.add(DataServicesUtil.convertArrayToString(
                        status[0].getCmdExecuted()));

            } catch (CommandExecutionException e) {
                status = e.getExitStatusArray();
                System.out.println("Command Tried to Execute : \n" +
                    DataServicesUtil.convertArrayToString(
                        status[0].getCmdExecuted()));
                System.out.println(status[0].getErrStrings());
                throw e;
            } catch (Exception e) {
                throw e;
            }
        }

        // Custom Configure
        if (customConfigureMap != null) {

            try {
                status = customConfigure(mbs, rgmOnf, customConfigureMap);

                if (status != null) {
                    commandSet.add(DataServicesUtil.convertArrayToString(
                            status[0].getCmdExecuted()));
                }
            } catch (CommandExecutionException cee) {
                status = cee.getExitStatusArray();

                if ((status != null) && (status.length > 0)) {
                    System.out.println("Command Tried to Execute : \n" +
                        DataServicesUtil.convertArrayToString(
                            status[0].getCmdExecuted()));
                    System.out.println(status[0].getErrStrings());
                }

                throw cee;
            } catch (Exception e) {
                throw e;
            }
        }
    }

    /**
     * Executes the commands or invokes the Dataservice MBean operation based on
     * the customConfiguration Map. If the resource's bringOnline is true, the
     * commands and the operation would be executed on the node that it is
     * online
     *
     * @param  customConfigureMap  Map containing the custom configuration
     * information.
     *
     * @return  commandsExecuted if any
     */
    private ExitStatus[] customConfigure(MBeanServer mbs,
        ObjectNameFactory rgmOnf, Map customConfigureMap) throws Exception {
        String onlineNode = "";
        ExitStatus status[] = null;

	RgGroupMBean rgGroupMBean = (RgGroupMBean)
	    MBeanServerInvocationHandler.newProxyInstance(mbs,
		rgmOnf.getObjectName(RgGroupMBean.class, null),
		RgGroupMBean.class, false);

	ResourceGroupData rgData = rgGroupMBean.getResourceGroup(
	    this.resourceGroup.getName(), zoneClusterName);

        try {

            // If bringOnline is set to true
            // Get the node on which the resource's RG is online
            if (isBringOnline()) {
                String onlineNodes[] = (String[]) rgData.getCurrentPrimaries();

                // If its scalable, it shouldnt matter to get any online node
                onlineNode = onlineNodes[0];
            }
            // Get the current node
            else {
                onlineNode = Util.getClusterEndpoint();
            }

            // Get the handler to the infrastructure MBean on the online node
            Map env = new HashMap();
            env.put(JMXConnectorFactory.DEFAULT_CLASS_LOADER,
                InfrastructureMBean.class.getClassLoader());
            env.put(JMXConnectorFactory.PROTOCOL_PROVIDER_CLASS_LOADER,
                InfrastructureMBean.class.getClassLoader());

            JMXConnector localConnection = TrustedMBeanModel
                .getWellKnownConnector(Util.getClusterEndpoint(), env);
            JMXConnector connector = TrustedMBeanModel.getWellKnownConnector(
                    Util.getNodeEndpoint(localConnection, onlineNode), env);
            InfrastructureMBean infrastructureMBean = (InfrastructureMBean)
                TrustedMBeanModel.getMBeanProxy(Util.DOMAIN, connector,
                    InfrastructureMBean.class, null, false);

            if (customConfigureMap != null) {
                Set keys = customConfigureMap.keySet();

                for (Iterator keysIter = keys.iterator(); keysIter.hasNext();) {
                    String key = (String) keysIter.next();

                    // Execute a command
                    if (key.equals("command")) {
                        String commands[] = (String[]) customConfigureMap.get(
                                "command");
                        status = infrastructureMBean.executeCommand(commands);
                    }
                    // Execute an operation on the MBean
                    else if (key.equals("operation")) {
                        Map opMap = (Map) customConfigureMap.get(key);
                        Class mBean = (Class) opMap.get("MBeanName");
                        String opName = (String) opMap.get("OperationName");
                        String types[] = (String[]) opMap.get("ParamType");
                        Object values[] = (Object[]) opMap.get("ParamValue");

                        // Operation Message
                        Object tmpObj = opMap.get("OperationMessage");

                        if (tmpObj != null) {
                            ExitStatus tmpStatus = new ExitStatus();
                            tmpStatus.setCmdExecuted(
                                new String[] { (String) tmpObj });
                            status = new ExitStatus[] { tmpStatus };
                        }

                        Object errObj = infrastructureMBean.executeOperation(
                                mBean, opName, types, values);

                        if (errObj instanceof ErrorValue) {
                            ErrorValue err = (ErrorValue) errObj;

                            if (!err.getReturnValue().booleanValue()) {
                                throw new CommandExecutionException(err
                                    .getErrorString());
                            }
                        }
                    }
                }
            }

            localConnection.close();
            connector.close();
        } catch (CommandExecutionException e) {
            e.printStackTrace();
            System.out.println("End of E");
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("End of E");
            throw e;
        }

        return status;
    }

    /**
     * Creates a new ResourceInfo object with the same configuration details as
     * this ResourceInfo Object.
     *
     * @return  ResourceInfo object with the same configuration details as this
     * ResourceInfo object.
     */
    public ResourceInfo copyResource() {

        // Create a new ResourceInfo object
        ResourceInfo resource = new ResourceInfo(this.resourceGroup);

        // Copy the attributes
        resource.setRtName(rtName);
        resource.setResourceId(resourceId);
        resource.setMultiple(multiple);
        resource.setOptional(optional);
        resource.setDependency(dependency);
        resource.setDependent(dependent);

        // add this reosurce to the existing set of resources
        this.resourceGroup.getResources().add(resource);

        return resource;
    }


    /**
     * Creates a new ResourceInfo object with the same configuration details as
     * this ResourceInfo Object but attaches it to the specified ResourceGroup
     *
     * @param  group  ResourceGroup to which this resource should be attached to
     *
     * @return  ResourceInfo object with the same configuration details as this
     * ResourceInfo object.
     */
    public ResourceInfo copyResource(ResourceGroupInfo group) {

        // Create a new ResourceInfo object
        ResourceInfo resource = new ResourceInfo(group);

        // Copy the attributes
        resource.setRtName(rtName);
        resource.setResourceId(resourceId);
        resource.setMultiple(multiple);
        resource.setOptional(optional);
        resource.setDependency(dependency);
        resource.setDependent(dependent);

        // add this reosurce to the existing set of resources of that group
        List groupResourceList = group.getResources();

        if (groupResourceList == null) {
            groupResourceList = new ArrayList();
            groupResourceList.add(resource);
            group.setResources(groupResourceList);
        } else {
            groupResourceList.add(resource);
        }

        return resource;
    }

    /**
     * Removes this ResourceInfo object from the existing resources list.
     *
     * @return  boolean value indicating the success of remove operation
     */
    public boolean removeResource() {
        return this.resourceGroup.getResources().remove(this);
    }


    /**
     * Checks whether the Resource is present in the ResourceGroup with the same
     * Name
     *
     * @return  <code>"true"</code> if Resource with the same name Exists.
     * <code>"false"</code> Resource with the same name doesnt Exists.
     */
    public boolean isResourcePresent() {

        // Will implement Later
        return false;
    }

    /**
     * Getter method for the Resource's Id
     *
     * @return  ID of the Resource
     */
    public String getResourceId() {
        return resourceId;
    }

    /**
     * Setter method for the Resource's Id
     *
     * @param  resourceId ID  of the Resource
     */
    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    /**
     * Returns a space separated list of IDs of Resources this Resource depends
     * on.
     *
     * @return  space separated list of IDs of Resources this Resource depends
     * on or return
     *
     * <p>"No Dependency"</p>
     *
     * if there is no dependency
     */
    public String getDependencies() {

        StringBuffer buffer;

        if (dependency == null) {
            return "No Dependency";
        } else {
            buffer = new StringBuffer();

            Iterator i = dependency.iterator();

            while (i.hasNext()) {
                ResourceInfo resource = (ResourceInfo) i.next();
                buffer.append(resource.getResourceId() + " ");
            }

            return buffer.toString();
        }
    }


    /**
     * Returns an String array of Names of Resources this Resource depends on
     *
     * @return  String array of names of Resources this Resource depends on
     */
    public String[] getDependencyArray() {

        String buffer[];
        int j = 0;

        if (dependency == null) {
            return null;
        } else {
            buffer = new String[dependency.size()];

            Iterator i = dependency.iterator();

            while (i.hasNext()) {
                ResourceInfo resource = (ResourceInfo) i.next();
                buffer[j++] = resource.getResourceName();
            }

            return buffer;
        }
    }

    /**
     * Returns a space separated list of IDs of Resources that depend on this
     * Resource.
     *
     * @return  space separated list of IDs of Resources that depend on this
     * Resource or return
     *
     * <p>"No Dependents"</p>
     *
     * if there are no dependent resources
     */
    public String getDependents() {

        StringBuffer buffer;

        if (dependent == null) {
            return "No Dependents";
        } else {
            buffer = new StringBuffer();

            Iterator i = dependent.iterator();

            while (i.hasNext()) {
                ResourceInfo resource = (ResourceInfo) i.next();
                buffer.append(resource.getResourceId() + " ");
            }

            return buffer.toString();
        }
    }


    /**
     * Returns an String array of Names of Resources that depend on this
     * Resource.
     *
     * @return  String array of names of Resources that depend on this Resource.
     */
    public String[] getDependentArray() {

        String buffer[];
        int j = 0;

        if (dependent == null) {
            return null;
        } else {
            buffer = new String[dependent.size()];

            Iterator i = dependent.iterator();

            while (i.hasNext()) {
                ResourceInfo resource = (ResourceInfo) i.next();
                buffer[j++] = resource.getResourceName();
            }

            return buffer;
        }
    }

    /**
     * Checks whether the Commands Have been generated for this Resource
     *
     * @return  whether Commands haveb been generated for this Resource
     */
    public boolean isResourceConfigured() {
        return resourceConfigured;
    }

    /**
     * Sets whether the Commands Have been generated for this Resource
     *
     * @param  resourceConfigured  true if Commands have been generated false if
     * commands have not been generated.
     */
    public void setResourceConfigured(boolean resourceConfigured) {
        this.resourceConfigured = resourceConfigured;
    }

    /**
     * Gets the SystemProperties for the Resource
     *
     * @return  The System Properties for this Resource
     */
    public List getSysProperties() {
        return systemProperties;
    }

    /**
     * Sets the System Properties for this Resource
     *
     * @param  systemProperties  The System Properties for this Resource
     */
    public void setSysProperties(List systemProperties) {
        this.systemProperties = systemProperties;
    }

    /**
     * Gets the ExtensionProperties for the Resource
     *
     * @return  The Extension Properties for this Resource
     */
    public List getExtProperties() {
        return extensionProperties;
    }

    /**
     * Sets the Extension Properties for this Resource
     *
     * @param  extensionProperties  The Extension Properties for this Resource
     */
    public void setExtProperties(List extensionProperties) {
        this.extensionProperties = extensionProperties;
    }

    /**
     * Checks if Multiple Instances of this Resource is allowed
     *
     * @return  <code>true</code> if multiple instances of this Resource is
     * allowed. <code>false</code> if multiple instances of this Resource is not
     * allowed.
     */
    public boolean isMultiple() {
        return this.multiple;
    }

    /**
     * Sets if Multiple Instances of this Resource is allowed
     *
     * @param  multiple  <code>true</code> if multiple instances of this
     * Resource is allowed. <code>false</code> if multiple instances of this
     * Resource is not allowed.
     */
    public void setMultiple(boolean multiple) {
        this.multiple = multiple;
    }

    /**
     * This method removes any session specific information that was used to
     * generate the commands
     */
    public void reset() {

        // Reset all instance specific information
        this.resourceName = null;
        this.systemProperties = null;
        this.extensionProperties = null;
        this.resourceConfigured = false;
        this.bringOnline = false;
        this.customConfigureMap = null;
        this.resourceRollbacked = false;
        this.isNewResource = true;
	this.zoneClusterName = null;
    }


    /**
     * Specifies whether to bring the resource online after creating the
     * resource
     *
     * @return  true if the resource is to be brought online false if not
     */
    public boolean isBringOnline() {
        return bringOnline;
    }

    /**
     * Sets the bringOnline flag for this resource if true, the resource would
     * be brought online NOTE: This method would attempt to bring only the
     * Resource's ResourceGroup. The dependent ResourceGroups should be brought
     * online.
     *
     * @param  bringOnline  true if the resource is to be brought online
     */
    public void setBringOnline(boolean bringOnline) {
        this.bringOnline = bringOnline;
    }

    /**
     * Gets the customConfiguration Map for this resource
     */
    public Map getCustomConfigureMap() {
        return customConfigureMap;
    }

    /**
     * Set the customConfiguration Map for this resource The customConfiguration
     * Map is of the following structure : Key        -    Value command    -
     * String [] commands to be executed operation  -    HashMap of the below
     * structure : {MBeanName= <Class MBeanInterface>, OperationName=<String
     * OperationName>, ParamType=<String []ParamType>, ParamValue=<Object
     * []ParamValue>}
     */
    public void setCustomConfigureMap(Map customConfigureMap) {
        this.customConfigureMap = customConfigureMap;
    }

    /**
     * Rollbacks the command generated for this resource
     */
    public void rollBack(MBeanServer mbs, ObjectNameFactory rgmOnf,
        List commandSet) throws Exception {

        ExitStatus status[] = null;


        if (resourceName == null) {
            return;
        }

        if (resourceRollbacked || !resourceConfigured) {
            return;
        }

        // Rollback the Resources dependent on this resource
        if (dependent != null) {
            Iterator i = dependent.iterator();

            while (i.hasNext()) {
                ResourceInfo resource = (ResourceInfo) i.next();

                // If the resource is in the Same RG Rollback it first
                if (resource.getResourceGroup().getGroupName().equals(
                            resourceGroup.getGroupName())) {

                    try {
                        resource.rollBack(mbs, rgmOnf, commandSet);
                    } catch (Exception e) {
                        throw e;
                    }

                    resource.setResourceRollbacked(true);
                } else {

                    // Check whether the Resource is already Rollbacked
                    if (!resource.isResourceRollbacked()) {

                        // Check if Resource's Group is Rollbacked
                        if (!resource.getResourceGroup().isGroupRollbacked()) {

                            // Rollback Commands for that ResourceGroup
                            try {
                                resource.getResourceGroup().rollBack(mbs,
                                    rgmOnf, commandSet);
                            } catch (Exception e) {
                                throw e;
                            }

                            resource.getResourceGroup().setGroupRollbacked(
                                true);
                        }

                        // Rollback the resource
                        try {
                            resource.rollBack(mbs, rgmOnf, commandSet);
                        } catch (Exception e) {
                            throw e;
                        }

                        resource.setResourceRollbacked(true);
                    }
                }
            }
        }

	RGroupMBean rGroupMBean = (RGroupMBean)
	    MBeanServerInvocationHandler.newProxyInstance(mbs,
		rgmOnf.getObjectName(RGroupMBean.class, null),
		RGroupMBean.class, false);

        // Rollback this Resource
        try {
            // Resource was configured as new Resource
            if (isNewResource) {

		// Disable the Resource
		status = rGroupMBean.disable(resourceName, zoneClusterName);

                commandSet.add(DataServicesUtil.convertArrayToString(
                        status[0].getCmdExecuted()));

                // Delete the Resource
                status = rGroupMBean.remove(resourceName, zoneClusterName);
                commandSet.add(DataServicesUtil.convertArrayToString(
                        status[0].getCmdExecuted()));
            }
            // Resource was configured as Existing Resource
            else {
                // Rollback
                if (dependency != null) {
		    // Get the resourceData
		    ResourceData rData = rGroupMBean.getResourceData(
			resourceName, zoneClusterName);

		    // Get the Resource's Existing Dependencies
		    String existingDependencies[] = rData
                            .getStrongDependencies();

                    String addedDependencies[] = getDependencyArray();
                    List existing = Arrays.asList(existingDependencies);
                    List added = Arrays.asList(addedDependencies);
                    existing.removeAll(added);

                    String finalDependencies[] = new String[existing.size()];
                    finalDependencies = (String[]) existing.toArray(
                            finalDependencies);
                    rGroupMBean.changeStrongDependencies(
			resourceName, finalDependencies, zoneClusterName, runCmd);
                }
            }
        } catch (CommandExecutionException e) {
            status = e.getExitStatusArray();
            System.out.println("Command Tried to Execute : \n" +
                DataServicesUtil.convertArrayToString(
                    status[0].getCmdExecuted()));
            System.out.println(status[0].getErrStrings());
            throw e;
        }
    }

    /**
     * Returns true if the Resource configuration has been Rollbacked
     */
    public boolean isResourceRollbacked() {
        return resourceRollbacked;
    }

    /**
     * Sets the Rollbacked flag for this Resource
     */
    public void setResourceRollbacked(boolean resourceRollbacked) {
        this.resourceRollbacked = resourceRollbacked;
    }
}
