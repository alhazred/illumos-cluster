/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)FileAccessorWrapper.java 1.13     08/05/20 SMI"
 */

package com.sun.cluster.agent.dataservices.utils;

// J2SE
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// JMX
import javax.management.MBeanServerConnection;
import javax.management.MBeanServerInvocationHandler;
import javax.management.ObjectName;

// Cacao
import com.sun.cacao.ObjectNameFactory;

// CMAS
import com.sun.cluster.agent.dataservices.utils.FileAccessorMBean;
import com.sun.cluster.agent.dataservices.utils.VfstabFileAccessorMBean;
import com.sun.cluster.agent.node.NodeMBean;
import com.sun.cluster.common.TrustedMBeanModel;


/**
 * File Accessor utility class to provide general file accessing functionality.
 */
public class FileAccessorWrapper {

    private String filePathName;
    private MBeanServerConnection mbsConnections[];
    private ObjectNameFactory onf;

    /**
     * @deprecated
     */
    private String nodeList[];


    /**
     * Constructor of this utility class.
     *
     * @param  pathName  The name of the file prefixed with its absolute path.
     * @param  mbsConnections  Array of MBeanServiceConnection objects. These
     * objects specify to which nodes FileAccessorWrapper would connect to.
     */
    public FileAccessorWrapper(String pathName,
        MBeanServerConnection mbsConnections[]) {
        this.filePathName = pathName;
        this.mbsConnections = mbsConnections;
        this.onf = new ObjectNameFactory(Util.DOMAIN);
    }


    /**
     * @deprecated
     */
    public FileAccessorWrapper(String pathName) {
        this.filePathName = pathName;

        List nodeMBeans = getNodeMBeans();
        List nodeListBuffer = new ArrayList();

        Iterator i = nodeMBeans.iterator();

        while (i.hasNext()) {
            NodeMBean mbean = (NodeMBean) i.next();
            nodeListBuffer.add(mbean.getName());
        }

        nodeList = (String[]) nodeListBuffer.toArray(
                new String[nodeListBuffer.size()]);
    }

    /**
     * @deprecated
     */
    public FileAccessorWrapper(String pathName, String nodeList[]) {
        this.filePathName = pathName;
        this.nodeList = nodeList;
    }


    /**
     * Read the entire contents of given file as specified in pathname parameter
     * in the constructor.
     *
     * @param  mbsConnection  The file present in the node specified by this
     * MBeanServerConnection would be read. If null is specified the
     * mbeanServerConnection appearing first in the mbsConnections would be
     * used.
     *
     * @return  Object This could be an array of Strings or a list of vfsStruct
     * objects - based on file being accessed. Each element of the string array
     * would represent each line of the file. The tab in each line of the file
     * would be replaced by ":".
     */
    public Object readFully(MBeanServerConnection mbsConnection) {

        if (mbsConnection == null)
            mbsConnection = mbsConnections[0];

        Object fileData = null;

        if (filePathName.equalsIgnoreCase(Util.VFSTAB_FILE)) {
            VfstabFileAccessorMBean vfstabFile = getVfstabFileAccesor(
                    mbsConnection);
            fileData = vfstabFile.readFully();
        } else {
            FileAccessorMBean file = getFileAccesor(mbsConnection);
            fileData = file.readFully(filePathName);
        }

        return fileData;

    }


    /**
     * Write the given Object to the given file as specified in pathname
     * parameter in the constructor. The object could be a list of vfsStruct
     * objects or String array. If the object is a string array each string
     * would be written as a new line in the end of the file with each
     * occurrence of ':' replaced by tab.
     *
     * @param  objToWrite  Object to be written to the file.
     * @param  mbsConnection  The file in the node specified through this
     * mbsConnection would be updated. If this parameter is null, the files
     * corresponding to all MBeanServerConnections corresponding to
     * mbsConnecions - as specified in the constructor of this class - would be
     * updated.
     */
    public void appendLine(Object objToWrite,
        MBeanServerConnection mbsConnection) {

        if (mbsConnection != null) {

            if (filePathName.equalsIgnoreCase(Util.VFSTAB_FILE)) {
                VfstabFileAccessorMBean vfstabFile = getVfstabFileAccesor(
                        mbsConnection);
                vfstabFile.appendLine((ArrayList) objToWrite);
            } else {
                FileAccessorMBean file = getFileAccesor(mbsConnection);
                file.appendLine(filePathName, (String[]) objToWrite);
            }
        } else {

            for (int i = 0; i < mbsConnections.length; i++) {
                mbsConnection = mbsConnections[i];

                if (filePathName.equalsIgnoreCase(Util.VFSTAB_FILE)) {
                    VfstabFileAccessorMBean vfstabFile = getVfstabFileAccesor(
                            mbsConnection);
                    vfstabFile.appendLine((ArrayList) objToWrite);
                } else {
                    FileAccessorMBean file = getFileAccesor(mbsConnection);
                    file.appendLine(filePathName, (String[]) objToWrite);
                }
            }
        }
    }

    /**
     * Write the given Object to the given file as specified in pathname
     * parameter in the constructor. The object could be a list of vfsStruct
     * objects or String array. If the object is a string array each string
     * would be written as a new line in the end of the file with occurrences of
     * ':' retained if replaceColon is "false".
     *
     * @param  objToWrite  Object to be written to the file.
     * @param  mbsConnection  The file in the node specified through this
     * mbsConnection would be updated. If this parameter is null, the files
     * corresponding to all MBeanServerConnections corresponding to
     * mbsConnecions - as specified in the constructor of this class - would be
     * updated.
     * @param  replaceColon  boolean whether or not to replace the ":" with
     * "\t".
     */
    public boolean appendLine(Object objToWrite,
        MBeanServerConnection mbsConnection, boolean replaceColon) {

        boolean retVal = true;

        if (replaceColon == true) {
            appendLine(objToWrite, mbsConnection);
        } else {

            if (mbsConnection != null) {

                if (filePathName.equalsIgnoreCase(Util.VFSTAB_FILE)) {
                    VfstabFileAccessorMBean vfstabFile = getVfstabFileAccesor(
                            mbsConnection);
                    vfstabFile.appendLine((ArrayList) objToWrite);
                } else {
                    FileAccessorMBean file = getFileAccesor(mbsConnection);
                    retVal = file.appendLine(filePathName,
                            (String[]) objToWrite, replaceColon);
                }
            } else {

                for (int i = 0; i < mbsConnections.length; i++) {
                    mbsConnection = mbsConnections[i];

                    if (filePathName.equalsIgnoreCase(Util.VFSTAB_FILE)) {
                        VfstabFileAccessorMBean vfstabFile =
                            getVfstabFileAccesor(mbsConnection);
                        vfstabFile.appendLine((ArrayList) objToWrite);
                    } else {
                        FileAccessorMBean file = getFileAccesor(mbsConnection);
                        retVal = file.appendLine(filePathName,
                                (String[]) objToWrite, replaceColon);
                    }
                }
            }
        }

        return retVal;
    }

    /**
     * This function returns the size of file as specified in pathname parameter
     * to this class constructor. If the mbsConnection is not specified, the
     * first mbsConnection from mbsConnections would be used.
     *
     * @param  mbsConnection  The file in the node specified through this
     * mbsConnection would be used.
     *
     * @return  File Size of type long.
     */
    public long getFileSize(MBeanServerConnection mbsConnection) {

        if (mbsConnection == null)
            mbsConnection = mbsConnections[0];

        long fileSize = 0;

        fileSize = (getFileAccesor(mbsConnection)).getFileSize(filePathName);

        return fileSize;

    }


    /**
     * This function checks for the existence of the file as specified in
     * pathname parameter to this class constructor. If the mbsConnection is not
     * specified, the first mbsConnection from mbsConnections would be used.
     *
     * @param  mbsConnection  The file in the node specified through this
     * mbsConnection would be used.
     *
     * @return  boolean literal which specifies the existence of the file.
     */
    public boolean ifFileExists(MBeanServerConnection mbsConnection) {

        if (mbsConnection == null)
            mbsConnection = mbsConnections[0];

        boolean fileExists = false;

        fileExists = (getFileAccesor(mbsConnection)).ifFileExists(filePathName);

        return fileExists;
    }


    /**
     * This function returns the mode of file as specified in pathname parameter
     * to this class constructor. If the mbsConnection is not specified, the
     * first mbsConnection from mbsConnections would be used.
     *
     * @param  mbsConnection  The file in the node specified through this
     * mbsConnection would be used.
     *
     * @return  The mode of file in type long.
     */
    public long getFileMode(MBeanServerConnection mbsConnection) {

        if (mbsConnection == null)
            mbsConnection = mbsConnections[0];

        long fileMode = 0;

        fileMode = (getFileAccesor(mbsConnection)).getFileMode(filePathName);

        return fileMode;
    }


    /**
     * This function returns the owner of file as specified in pathname
     * parameter to this class constructor. If the mbsConnection is not
     * specified, the first mbsConnection from mbsConnections would be used.
     *
     * @param  mbsConnection  The file in the node specified through this
     * mbsConnection would be used.
     *
     * @return  The owner of file in type long.
     */
    public long getFileOwner(MBeanServerConnection mbsConnection) {

        if (mbsConnection == null)
            mbsConnection = mbsConnections[0];

        long fileOwner = 0;

        fileOwner = (getFileAccesor(mbsConnection)).getFileOwner(filePathName);

        return fileOwner;
    }

    /**
     * Creates the directory named by this abstract pathname, including any
     * necessary but nonexistent parent directories.
     *
     * @param  pathName  The pathname of the directory to be created
     * @param  mbsConnection  The command would be executed only on the node
     * specified by this parameter. If this is null the command would be
     * executed on all nodes as specified in the constructor of this class -
     * mbsConnections parameter.
     *
     * @return  true if the command succeeds.
     */
    public boolean createDir(String pathName,
        MBeanServerConnection mbsConnection) {

        if (mbsConnection != null) {
            FileAccessorMBean file = getFileAccesor(mbsConnection);

            return file.createDir(pathName);
        } else {

            for (int i = 0; i < mbsConnections.length; i++) {
                mbsConnection = mbsConnections[i];

                FileAccessorMBean file = getFileAccesor(mbsConnection);

                if (!file.createDir(pathName))
                    return false;
            }
        }

        return true;

    }

    /**
     * Creates the file named by this abstract pathname even if the file exists
     *
     * @param  pathName  The pathname of the file to be created
     * @param  mbsConnection  The command would be executed only on the node
     * specified by this parameter. If this is null the command would be
     * executed on all nodes as specified in the constructor of this class -
     * mbsConnections parameter.
     *
     * @return  true if the command succeeds.
     */
    public boolean createAndOverwriteFile(String pathName,
        MBeanServerConnection mbsConnection) {

        if (mbsConnection != null) {
            FileAccessorMBean file = getFileAccesor(mbsConnection);

            return file.createAndOverwriteFile(pathName);
        } else {

            for (int i = 0; i < mbsConnections.length; i++) {
                mbsConnection = mbsConnections[i];

                FileAccessorMBean file = getFileAccesor(mbsConnection);

                if (!file.createAndOverwriteFile(pathName))
                    return false;
            }
        }

        return true;

    }


    /**
     * This function searches the vfstab file until a mount point matching data
     * is found and prepares a corresponding VfsStruct with the fields from the
     * line in the vfstab file. This function internally uses getvfsfile() API.
     *
     * @param  data
     * @param  mbsConnection  The command would be executed only on the node
     * specified by this parameter. If the mbsConnection is null, the first
     * mbsConnection from mbsConnections would be used.
     *
     * @return  Returns a list of thus found VfsStruct objects.
     */
    public Object getvfsfile(String data, MBeanServerConnection mbsConnection) {

        if (mbsConnection == null)
            mbsConnection = mbsConnections[0];

        Object fileData = null;
        VfstabFileAccessorMBean vfstabFile = getVfstabFileAccesor(
                mbsConnection);
        fileData = vfstabFile.getvfsfile(data);

        return fileData;
    }


    /**
     * This function searches the vfstab file until a special device matching
     * data is found and prepares a corresponding VfsStruct with the fields from
     * the line in the vfstab file.  The data argument will try to match on
     * device type (block or character special) and major and minor device
     * numbers.  If  it  cannot match in this manner, then it compares the
     * strings. This function internally uses getvfsspec() API.
     *
     * @param  data
     * @param  mbsConnection  The command would be executed only on the node
     * specified by this parameter. If the mbsConnection is null, the first
     * mbsConnection from mbsConnections would be used.
     *
     * @return  Returns a list of thus found VfsStruct objects.
     */
    public Object getvfsspec(String data, MBeanServerConnection mbsConnection) {

        if (mbsConnection == null)
            mbsConnection = mbsConnections[0];

        Object fileData = null;
        VfstabFileAccessorMBean vfstabFile = getVfstabFileAccesor(
                mbsConnection);
        fileData = vfstabFile.getvfsspec(data);

        return fileData;
    }


    /**
     * The getvfsany() function searches the vfstab file until a match is found
     * between a line in the file and given VfsStruct object. A match occurrs if
     * all non-null entries in the given VfsStruct object match the
     * corresponding fields in the file. This function internally uses
     * getvfsany() API.
     *
     * @param  vfsStruct  Object of type VfsStruct
     * @param  mbsConnection  The command would be executed only on the node
     * specified by this parameter. If the mbsConnection is null, the first
     * mbsConnection from mbsConnections would be used.
     *
     * @return  Returns a list of thus found VfsStruct objects.
     */
    public Object getvfsany(VfsStruct vfsStruct,
        MBeanServerConnection mbsConnection) {

        if (mbsConnection == null)
            mbsConnection = mbsConnections[0];

        Object fileData = null;
        VfstabFileAccessorMBean vfstabFile = getVfstabFileAccesor(
                mbsConnection);
        fileData = vfstabFile.getvfsany(vfsStruct);

        return fileData;
    }


    private FileAccessorMBean getFileAccesor(
        MBeanServerConnection mbsConnection) {

        FileAccessorMBean file = null;

        try {
            ObjectName fileMBeanObjName = onf.getObjectName(
                    FileAccessorMBean.class, null);
            file = (FileAccessorMBean) MBeanServerInvocationHandler
                .newProxyInstance(mbsConnection, fileMBeanObjName,
                    FileAccessorMBean.class, false);
        } catch (Exception ex) {
            System.out.println("Could not get connection to cacao agent");
            ex.printStackTrace();
        }

        return file;
    }

    private VfstabFileAccessorMBean getVfstabFileAccesor(
        MBeanServerConnection mbsConnection) {

        VfstabFileAccessorMBean vfstab = null;

        try {
            ObjectName fileMBeanObjName = onf.getObjectName(
                    VfstabFileAccessorMBean.class, null);
            vfstab = (VfstabFileAccessorMBean) MBeanServerInvocationHandler
                .newProxyInstance(mbsConnection, fileMBeanObjName,
                    VfstabFileAccessorMBean.class, false);
        } catch (Exception ex) {
            System.out.println("Could not get connection to cacao agent");
            ex.printStackTrace();
        }

        return vfstab;
    }


    /**
     * @deprecated
     */
    public String[] readFully(String nodeName) {

        if (nodeName == null)
            nodeName = nodeList[0];

        String fileData[] = null;

        try {

            if (filePathName.equalsIgnoreCase("vfstab")) {
                VfstabFileAccessorMBean vfstabFile = (VfstabFileAccessorMBean)
                    TrustedMBeanModel.getMBeanProxy(Util.DOMAIN,
                        Util.getNodeEndpoint(nodeName),
                        VfstabFileAccessorMBean.class, null, false);
// fileData = vfstabFile.readFully(filePathName);
            } else {
                FileAccessorMBean file = (FileAccessorMBean) TrustedMBeanModel
                    .getMBeanProxy(Util.DOMAIN, Util.getNodeEndpoint(nodeName),
                        FileAccessorMBean.class, null, false);
                fileData = file.readFully(filePathName);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return fileData;

    }


    /**
     * @deprecated
     */
    public void appendLine(String stringToWrite, String nodeName) {

        // Also check if given nodename is valid i.e. exist in nodeList
        try {

            if (nodeName != null) {

                if (filePathName.equalsIgnoreCase("vfstab")) {
                    VfstabFileAccessorMBean vfstabFile =
                        (VfstabFileAccessorMBean) TrustedMBeanModel
                        .getMBeanProxy(Util.DOMAIN,
                            Util.getNodeEndpoint(nodeName),
                            VfstabFileAccessorMBean.class, null, false);
// vfstabFile.appendLine(filePathName, stringToWrite);
                } else {
                    FileAccessorMBean file = (FileAccessorMBean)
                        TrustedMBeanModel.getMBeanProxy(Util.DOMAIN,
                            Util.getNodeEndpoint(nodeName),
                            FileAccessorMBean.class, null, false);
                    file.appendLine(filePathName, stringToWrite);
                }
            } else {

                for (int i = 0; i < nodeList.length; i++) {
                    nodeName = nodeList[i];

                    if (filePathName.equalsIgnoreCase("vfstab")) {
                        VfstabFileAccessorMBean vfstabFile =
                            (VfstabFileAccessorMBean) TrustedMBeanModel
                            .getMBeanProxy(Util.DOMAIN,
                                Util.getNodeEndpoint(nodeName),
                                VfstabFileAccessorMBean.class, null, false);
// vfstabFile.appendLine(filePathName, stringToWrite);
                    } else {
                        FileAccessorMBean file = (FileAccessorMBean)
                            TrustedMBeanModel.getMBeanProxy(Util.DOMAIN,
                                Util.getNodeEndpoint(nodeName),
                                FileAccessorMBean.class, null, false);
                        file.appendLine(filePathName, stringToWrite);
                    }
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }


    /**
     * @deprecated
     */
    private List getNodeMBeans() {
        List nodeMBeans = null;

        try {
            nodeMBeans = TrustedMBeanModel.getMBeanProxies(Util
                    .getClusterEndpoint(), NodeMBean.class, false, null);
        } catch (IOException ex) {
            System.out.println("Unable to get Node List :");
            ex.printStackTrace();
        }

        return nodeMBeans;
    }


}
